/*************************************************************************
   *   $Header XXWCBinInquiryPage.java $
   *   Module Name: XXWCBinInquiryPage
   *   
   *   Package: package xxwc.oracle.apps.inv.bins.server;
   *   
   *   Imports Classes:
   *   
   *   import java.sql.*;
   *   import oracle.apps.inv.utilities.server.UtilFns;
   *   import oracle.apps.mwa.beans.*;
   *   import oracle.apps.mwa.container.MWALib;
   *   import oracle.apps.mwa.container.Session;
   *   import oracle.apps.mwa.eventmodel.*;
   *   import oracle.apps.mwa.container.FileLogger;
   *   import oracle.apps.fnd.flexj.FlexException;
   *   import oracle.apps.inv.lov.server.ItemLOV;
   *   import oracle.apps.inv.lov.server.LocatorKFF;
   *   import oracle.apps.inv.lov.server.SubinventoryLOV;
   *   import xxwc.oracle.apps.inv.lov.server.*;
   *
   *   PURPOSE:   Java Class for XXWCBinInquiryPage Page Layout.
   *    
   *   REVISIONS:
   *   Ver        Date        Author                     Description
   *   ---------  ----------  ---------------         -------------------------
        1.0       09-FEB-2015   Lee Spitzer             Initial Version - 
                                                          TMS Ticket 20150302-00164 - RF - Bin maintenance form   
        1.1       29-MAR-2016   Lee Spitzer             TMS Ticket Number 20151103-00182 - RF - UI Improvements to reduce number of clicks
**************************************************************************/

package xxwc.oracle.apps.inv.bins.server;

import java.sql.SQLException;

import oracle.apps.mwa.beans.*;
import oracle.apps.mwa.container.MWALib;
import oracle.apps.mwa.container.Session;
import oracle.apps.mwa.eventmodel.*;
import oracle.apps.mwa.container.FileLogger;
import oracle.apps.fnd.flexj.FlexException;
import oracle.apps.fnd.flexj.KeyFlexfield;
import oracle.apps.inv.invinq.server.ItemOnhandIterator;
import oracle.apps.inv.utilities.server.*;
import oracle.apps.inv.lov.server.ItemLOV;
import oracle.apps.inv.lov.server.LocatorKFF;
import oracle.apps.inv.lov.server.SubinventoryLOV;
import xxwc.oracle.apps.inv.lov.server.*;

/*************************************************************************
 *   NAME: XXWCBinInquiryPage extends PageBean
 *
 *   PURPOSE:   Main class for XXWCBinInquiryPage
 *
 *   REVISIONS:
 *   Ver        Date        Author                     Description
 *   ---------  ----------  ---------------         -------------------------
        1.0       09-FEB-2015   Lee Spitzer             Initial Version - 
                                                          TMS Ticket 20150302-00164 - RF - Bin maintenance form   
 **************************************************************************/


public class XXWCBinInquiryPage extends PageBean{
    /*Define objects in the class*/
    public static String WMS_TXN_SUCCESS = "Txn Successful";
    public static String WMS_TXN_ERROR = "Txn Failed. Please check log";
    public static String WMS_TXN_CANCEL = "Txn Cancelled";
    public static String WMS_LOT_INS_FAIL = "Lot Insert Failed";
    public static String WMS_SER_INS_FAIL = "Serial Insert Failed";
    public static String savenextPrompt = "";
    public static String donePrompt = "";
    public static String cancelPrompt = "";
    private boolean areThePromptsSet;
    protected XXWCItemLOV mItemFld;
    TextFieldBean mItemDescFld;
    XXWCBinsLOV mBinFld;
    protected ButtonFieldBean mCancel;
    protected ButtonFieldBean mDone;
    protected String m_userid;


    /*************************************************************************
     *   NAME: private static String gPackage = "xxwc.oracle.apps.inv.bins.server";
     *
     *   PURPOSE:   private method to default gPackage value for debugging
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
            1.0       09-FEB-2015   Lee Spitzer             Initial Version - 
                                                                TMS Ticket 20150302-00164 - RF - Bin maintenance form   
     **************************************************************************/

    private static String gPackage = "xxwc.oracle.apps.inv.bins.server";


    /*************************************************************************
     *   NAME: private static String gCallFrom = "XXWCBinInquiryPage";
     *
     *   PURPOSE:   private method to pass gCallFrom from methods to return class name CustomListener
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
            1.0       09-FEB-2015   Lee Spitzer             Initial Version - 
                                                                TMS Ticket 20150302-00164 - RF - Bin maintenance form   
     **************************************************************************/

    private static String gCallFrom = "XXWCBinInquiryPage";



    /*************************************************************************
     *   NAME: public XXWCBinInquiryPage(Session session)
     *
     *   PURPOSE:   Inherits session from XXWCPrimaryBinAssignmentPage
     *
     *   Parameters: Session session
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
            1.0       09-FEB-2015   Lee Spitzer             Initial Version - 
                                                               TMS Ticket 20150302-00164 - RF - Bin maintenance form   
     **************************************************************************/

    public XXWCBinInquiryPage(Session session) {
        initLayout(session);
    }

    
    /*************************************************************************
     *   NAME: private void initLayout(Session session)
     *
     *   PURPOSE:   Sets the initial layout of the page
     *
     *   Parameters: Session session
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
          1.0       09-FEB-2015   Lee Spitzer             Initial Version - 
                                                             TMS Ticket 20150302-00164 - RF - Bin maintenance form   
     **************************************************************************/

    private void initLayout(Session session) {
        m_userid = "";
        WMS_TXN_SUCCESS = UtilFns.getMessage(session, "WMS", "WMS_TXN_SUCCESS");
        WMS_TXN_ERROR = UtilFns.getMessage(session, "WMS", "WMS_TXN_ERROR");
        WMS_TXN_CANCEL = UtilFns.getMessage(session, "WMS", "WMS_TXN_CANCEL");
        WMS_LOT_INS_FAIL = UtilFns.getMessage(session, "WMS", "WMS_LOT_INS_FAIL");
        WMS_SER_INS_FAIL = UtilFns.getMessage(session, "WMS", "WMS_SER_INS_FAIL");
        /*Item Field*/
        // mItemFld = new LOVFieldBean();
        mItemFld = new XXWCItemLOV("ALL");
        mItemFld.setName("XXWC.ITEM");
        mItemFld.setRequired(true);
        /*Item Description*/
        mItemDescFld = new TextFieldBean();
        mItemDescFld.setName("XXWC.DESC");
        mItemDescFld.setPrompt("Item Desc");
        mItemDescFld.setEditable(false);
        /*Subinventory*/
        //mSubFld = new SubinventoryLOV("INVINQ");
        //mSubFld.setName("XXWC.SUB");
        //mSubFld.setRequired(false);
        /*Bin*/
        mBinFld = new XXWCBinsLOV();
        mBinFld.setName("XXWC.BIN_LIST");
        mBinFld.setHidden(true);
        mBinFld.setEditable(true);
        /*Save Properties*/
        mDone = new ButtonFieldBean();
        mDone.setName("XXWC.DONE");
        mDone.setPrompt("Done");
        mDone.setEnableAcceleratorKey(true);
        mDone.setHidden(true);
        /*Cancel Properties*/
        mCancel = new ButtonFieldBean();
        mCancel.setName("XXWC.CANCEL");
        mCancel.setPrompt("Cancel");
        mCancel.setEnableAcceleratorKey(true);
        mCancel.setHidden(false);
        /*set the properties for each to the addFieldBean*/
        addFieldBean(mItemFld);
        addFieldBean(mBinFld);
        addFieldBean(mDone);
        addFieldBean(mCancel);
        /*Point Page to the XXWCPrimaryBinAssignmentFListener Listener*/
        XXWCBinInquiryFListener fieldListener = new XXWCBinInquiryFListener();
        /*Add Listener to each property*/
        mItemFld.addListener(fieldListener);
        mItemDescFld.addListener(fieldListener);
        mBinFld.addListener(fieldListener);
        mDone.addListener(fieldListener);
        mCancel.addListener(fieldListener);
        try {
            setPrompts(session);
        } catch (Exception exception) {
            session.setStatusMessage(UtilFns.getMessage(session, "WMS", "WMS_PROMPT_NOTSET"));
        }
    }
    
    /*************************************************************************
     *   NAME: private void initPrompts(Session session) throws SQLException
     *
     *   PURPOSE:   Sets the initial prompts of the session
     *
     *   Parameters: Session session
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
          1.0       09-FEB-2015   Lee Spitzer             Initial Version - 
                                                             TMS Ticket 20150302-00164 - RF - Bin maintenance form   
     **************************************************************************/

    private void initPrompts(Session session) throws SQLException {
        try {
            String gMethod = "initPrompts";
            String gCallPoint = "Start";
            FileLogger.getSystemLogger().trace(gPackage + "." + gCallFrom + " " + gMethod + " " + gCallPoint);
            String s = (String)session.getObject("ORGCODE");
            setPrompt((new StringBuilder()).append("XXWC Bin Assignment (").append(s).append(")").toString());

            mItemFld.setPrompt(MWALib.getAKPrompt(session, "oracle.apps.inv.utilities.InvResourceTable",
                                                  "INV_ITEM_PROMPT"));
            mItemFld.retrieveAttributes("INV_ITEM_PROMPT");
            mItemDescFld.setPrompt(MWALib.getAKPrompt(session, "oracle.apps.inv.utilities.InvResourceTable",
                                                      "INV_DESCRIPTION_PROMPT"));
            mItemDescFld.retrieveAttributes("INV_DESCRIPTION_PROMPT");
            mBinFld.setPrompt("Bins");
            donePrompt = MWALib.getAKPrompt(session, "oracle.apps.inv.utilities.InvResourceTable", "INV_DONE_PROMPT");
            cancelPrompt = MWALib.getAKPrompt(session, "oracle.apps.inv.utilities.InvResourceTable", "INV_CANCEL_PROMPT");
            areThePromptsSet = true;
            gCallPoint = "End";
            FileLogger.getSystemLogger().trace(gPackage + "." + gCallFrom + " " + gMethod + " " + gCallPoint);
        } catch (SQLException sqlexception) {
            session.setStatusMessage(UtilFns.getMessage(session, "WMS", "WMS_TXN_ERROR"));
        }
    }

    /*************************************************************************
     *   NAME: private void setPrompts(Session session)
     *
     *   PURPOSE:   Sets the set prompts of the session
     *
     *   Parameters: Session session
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
          1.0       09-FEB-2015   Lee Spitzer             Initial Version - 
                                                             TMS Ticket 20150302-00164 - RF - Bin maintenance form   
          1.1       29-MAR-2016   Lee Spitzer             TMS Ticket Number 20151103-00182 - RF - UI Improvements to reduce number of clicks
     **************************************************************************/

    private void setPrompts(Session session) {
            String gMethod = "setPrompts";
            String gCallPoint = "Start";
            if (!areThePromptsSet) {
                try {
                    initPrompts(session);
                } catch (SQLException sqlexception) {
                }
            }
            FileLogger.getSystemLogger().trace(gPackage + "." + gCallFrom + " " + gMethod + " " + gCallPoint);
            String s = (String)session.getObject("ORGCODE");
            setPrompt((new StringBuilder()).append("XXWC Bin Inquiry (").append(s).append(")").toString());

            mDone.setPrompt(donePrompt);
            mCancel.setPrompt(cancelPrompt);
            mDone.retrieveAttributes("INV_DONE_PROMPT");
            mCancel.retrieveAttributes("INV_CANCEL_PROMPT");
            //mCancel.setPrompt("Menu"); //Removed 03/29/2016 TMS Ticket 20151103-00182
            gCallPoint = "End Prompts";
            FileLogger.getSystemLogger().trace(gPackage + "." + gCallFrom + " " + gMethod + " " + gCallPoint);
            gCallPoint = "setting field values";
    }


    /*************************************************************************
     *   NAME: public ItemLOV  getmItemFld()
     *
     *   PURPOSE:   return the value of mItemFld
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
             1.0       09-FEB-2015   Lee Spitzer             Initial Version - 
                                                                TMS Ticket 20150302-00164 - RF - Bin maintenance form   
      **************************************************************************/

    public XXWCItemLOV getmItemFld()
        //public LOVFieldBean getmItemFld()
    {
        return mItemFld;
    }

    /*************************************************************************
     *   NAME: public TextFieldBean getmItemDescFld()
     *
     *   PURPOSE:   return the value of mItemDescFld
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
            1.0       09-FEB-2015   Lee Spitzer             Initial Version - 
                                                                TMS Ticket 20150302-00164 - RF - Bin maintenance form   
     **************************************************************************/

    public TextFieldBean getmItemDescFld() {
        return mItemDescFld;
    }

    /*************************************************************************
     *   NAME: public MultiListFieldBean getmBinFld()
     *
     *   PURPOSE:   return the value of mBinFld
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
            1.0       09-FEB-2015   Lee Spitzer             Initial Version -
                                                                TMS Ticket 20150302-00164 - RF - Bin maintenance form   
     **************************************************************************/

    public XXWCBinsLOV getmBinFld() {
        return mBinFld;
    }

     /*************************************************************************
      *   NAME: public ButtonFieldBean getmCancel
      *
      *   PURPOSE:   return the value of mCancel
      *
      *   REVISIONS:
      *   Ver        Date        Author                     Description
      *   ---------  ----------  ---------------         -------------------------
             1.0       09-FEB-2015   Lee Spitzer             Initial Version - 
                                                                 TMS Ticket 20150302-00164 - RF - Bin maintenance form   
      **************************************************************************/

    public ButtonFieldBean getmCancel() {
        return mCancel;
    }

    /*************************************************************************
     *   NAME: public ButtonFieldBean getmDone()
     *
     *   PURPOSE:   return the value of mDone
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
             1.0       09-FEB-2015   Lee Spitzer             Initial Version - 
                                                                TMS Ticket 20150302-00164 - RF - Bin maintenance form   
     **************************************************************************/

    public ButtonFieldBean getmDone() {
        return mDone;
    }


    /*************************************************************************
     *   NAME: public String getUserId()
     *
     *   PURPOSE:   return the value of m_userid
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
             1.0       09-FEB-2015   Lee Spitzer             Initial Version - 
                                                                TMS Ticket 20150302-00164 - RF - Bin maintenance form   
     **************************************************************************/

    public String getUserId() {
        return m_userid;
    }

    /*************************************************************************
     *   NAME: void pageEntered(MWAEvent e) throws AbortHandlerException, InterruptedHandlerException,
                                               DefaultOnlyHandlerException {
     *
     *   PURPOSE:   pageEntered method
     *
     *   Parameters: MWAEvent mwaevent
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
             1.0       09-FEB-2015   Lee Spitzer             Initial Version - 
                                                                TMS Ticket 20150302-00164 - RF - Bin maintenance form   
     **************************************************************************/

    public void pageEntered(MWAEvent e) throws AbortHandlerException, InterruptedHandlerException,
                                               DefaultOnlyHandlerException {
        Session session = e.getSession();
    }

    /*************************************************************************
     *   NAME: void pageExited(MWAEvent e) throws AbortHandlerException, InterruptedHandlerException,
                                               DefaultOnlyHandlerException {
     *
     *   PURPOSE:   pageExited method
     *
     *
     *   Parameters: MWAEvent   mwaevent
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
             1.0       09-FEB-2015   Lee Spitzer             Initial Version - 
                                                                TMS Ticket 20150302-00164 - RF - Bin maintenance form   

     **************************************************************************/

    public void pageExited(MWAEvent e) throws AbortHandlerException, InterruptedHandlerException,
                                              DefaultOnlyHandlerException {
        Session session = e.getSession();
    }
}
