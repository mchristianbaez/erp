/*************************************************************************
   *   $Header XXWCSubXferPage.java $
   *   Module Name: XXWCSubXferPage
   *   
   *   Package: package xxwc.oracle.apps.inv.invtxn.server;
   *   
   *   Imports Classes:
   *   
   *   import java.sql.*;
   *   import oracle.apps.inv.utilities.server.UtilFns;
   *   import oracle.apps.mwa.beans.*;
   *   import oracle.apps.mwa.container.MWALib;
   *   import oracle.apps.mwa.container.Session;
   *   import oracle.apps.mwa.eventmodel.*;
   *   import oracle.apps.mwa.container.FileLogger;
   *   import oracle.apps.fnd.flexj.FlexException;
   *   import oracle.apps.inv.lov.server.ItemLOV;
   *   import oracle.apps.inv.lov.server.LocatorKFF;
   *   import oracle.apps.inv.lov.server.SubinventoryLOV;
   *   import xxwc.oracle.apps.inv.lov.server.*;
   *
   *   PURPOSE:   Java Class for XXWC Cycle Count Page Layout.
   *    
   *   REVISIONS:
   *   Ver        Date        Author                     Description
   *   ---------  ----------  ---------------         -------------------------
        1.0       20-JUN-2015   Lee Spitzer             Initial Version - 
                                                            TMS Ticket Number 20150622-00003 RF - Miscellaneous Transactions
        1.1       29-MAR-2016   Lee Spitzer             TMS Ticket Number 20151103-00182 - RF - UI Improvements to reduce number of clicks
**************************************************************************/
package xxwc.oracle.apps.inv.invtxn.server;

import java.sql.*;

import oracle.apps.mwa.beans.*;
import oracle.apps.mwa.container.MWALib;
import oracle.apps.mwa.container.Session;
import oracle.apps.mwa.eventmodel.*;
import oracle.apps.mwa.container.FileLogger;
import oracle.apps.fnd.flexj.FlexException;
import oracle.apps.fnd.flexj.KeyFlexfield;
import oracle.apps.inv.invinq.server.ItemOnhandIterator;
import oracle.apps.inv.lov.server.CycleCountLOV;
import oracle.apps.inv.utilities.server.*;
import oracle.apps.inv.lov.server.ItemLOV;
import oracle.apps.inv.lov.server.LocatorKFF;
import oracle.apps.inv.lov.server.SubinventoryLOV;

import oracle.apps.mwa.presentation.telnet.TelnetSession;

import xxwc.oracle.apps.inv.lov.server.*;


/*************************************************************************
 *   NAME: public class XXWCSubXferPage extends PageBean
 *
 *   PURPOSE:   Main class for XXWCPrimaryBinAssignmentFunction
 *
 *   REVISIONS:
 *   Ver        Date        Author                     Description
 *   ---------  ----------  ---------------         -------------------------
      1.0       20-JUN-2015   Lee Spitzer             Initial Version -
                                                      TMS Ticket Number 20150622-00003 RF - Miscellaneous Transactions
 **************************************************************************/

public class XXWCSubXferPage extends PageBean{
    /*Define objects in the class*/
    public static String WMS_TXN_SUCCESS = "Txn Successful";
    public static String WMS_TXN_ERROR = "Txn Failed. Please check log";
    public static String WMS_TXN_CANCEL = "Txn Cancelled";
    public static String WMS_LOT_INS_FAIL = "Lot Insert Failed";
    public static String WMS_SER_INS_FAIL = "Serial Insert Failed";
    public static String saveNextPrompt = "";
    public static String donePrompt = "";
    public static String cancelPrompt = "";
    private boolean areThePromptsSet;
    protected XXWCItemLOV mItemFld;
    TextFieldBean mItemDescFld;
    protected XXWCSubLOV mSubFld;
    protected XXWCBinsLOV mLocFld;
    TextFieldBean mOnHandFld;
    TextFieldBean mAvailableFld;
    protected XXWCSubLOV mToSubFld;
    protected XXWCBinsLOV mToLocFld;
    TextFieldBean mUOMFld;
    TextFieldBean mQtyFld;
    protected ButtonFieldBean mSaveNext;
    protected ButtonFieldBean mDone;
    protected ButtonFieldBean mMenu;
    protected String m_userid;
    /*************************************************************************
     *   NAME: private static String gPackage = "xxwc.oracle.apps.inv.rcv.server";
     *
     *   PURPOSE:   private method to default gPackage value for debugging
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
            1.0       20-JUN-2015   Lee Spitzer             Initial Version - 
                                                            TMS Ticket Number 20150622-00003 RF - Miscellaneous Transactions
     **************************************************************************/

    private static String gPackage = "xxwc.oracle.apps.inv.invtxn.server";


    /*************************************************************************
     *   NAME: private static String gCallFrom = "XXWCSubXferPage";
     *
     *   PURPOSE:   private method to pass gCallFrom from methods to return class name CustomListener
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
            1.0       20-JUN-2015   Lee Spitzer             Initial Version - 
                                                            TMS Ticket Number 20150622-00003 RF - Miscellaneous Transactions
     **************************************************************************/

    private static String gCallFrom = "XXWCSubXferPage";



    
    /*************************************************************************
    *   NAME: public XXWCSubXferPage(Session session)
    *
    *   PURPOSE:   Main method and calls initLayout Method
    *
    *   REVISIONS:
    *   Ver        Date        Author                     Description
    *   ---------  ----------  ---------------         -------------------------
         1.0       20-JUN-2015   Lee Spitzer             Initial Version - 
                                                         TMS Ticket Number 20150622-00003 RF - Miscellaneous Transactions
     **************************************************************************/

    public XXWCSubXferPage(Session session) {
        initLayout(session);
    }
    
    /*************************************************************************
    *   NAME: private void initLayout(Session session)
    *
    *   PURPOSE:   Sets the initial layout of the paged
    *
    *   REVISIONS:
    *   Ver        Date        Author                     Description
    *   ---------  ----------  ---------------         -------------------------
         1.0       20-JUN-2015   Lee Spitzer             Initial Version - 
                                                         TMS Ticket Number 20150622-00003 RF - Miscellaneous Transactions
         1.1       29-MAR-2016   Lee Spitzer             TMS Ticket Number 20151103-00182 - RF - UI Improvements to reduce number of clicks
     **************************************************************************/

    private void initLayout(Session session) {
        String gMethod = "setPrompts";
        String gCallPoint = "Start";
        m_userid = "";
        WMS_TXN_SUCCESS = UtilFns.getMessage(session, "WMS", "WMS_TXN_SUCCESS");
        WMS_TXN_ERROR = UtilFns.getMessage(session, "WMS", "WMS_TXN_ERROR");
        WMS_TXN_CANCEL = UtilFns.getMessage(session, "WMS", "WMS_TXN_CANCEL");
        WMS_LOT_INS_FAIL = UtilFns.getMessage(session, "WMS", "WMS_LOT_INS_FAIL");
        WMS_SER_INS_FAIL = UtilFns.getMessage(session, "WMS", "WMS_SER_INS_FAIL");
        /*Item*/        
        mItemFld = new XXWCItemLOV("ALL");
        mItemFld.setName("XXWC.ITEM");
        mItemFld.setPrompt("Item");
        mItemFld.setRequired(true);
        mItemFld.setEditable(true);
        /*Item Desc*/
        mItemDescFld = new TextFieldBean();
        mItemDescFld.setName("XXWC.DESC");
        mItemDescFld.setPrompt("Item Desc");
        mItemDescFld.setEditable(false);
        mItemDescFld.setHidden(true);
        /*Subinventory*/
        mSubFld = new XXWCSubLOV();
        mSubFld.setName("XXWC.SUB");
        mSubFld.setPrompt("Sub");
        mSubFld.setRequired(true);
        mSubFld.setHidden(true);
        /*Bin*/
        try {
            mLocFld = new XXWCBinsLOV();
            mLocFld.setName("XXWC.LOC");
            mLocFld.setPrompt("Loc");
            mLocFld.setRequired(true);
            mLocFld.setEditable(false);
            mLocFld.setHidden(true);
        } catch (Exception e) {
            UtilFns.trace("Error instantiating rcvtxn locator kff");
        }
        /*On-Hand Field*/
        mOnHandFld = new TextFieldBean();
        mOnHandFld.setName("XXWC.ONHAND");
        mOnHandFld.setPrompt("On Hand");
        mOnHandFld.setEditable(false);
        mOnHandFld.setHidden(true);
        /*Available Field*/
        mAvailableFld = new TextFieldBean();
        mAvailableFld.setName("XXWC.AVAILABLE");
        mAvailableFld.setPrompt("Available");
        mAvailableFld.setEditable(false);
        mAvailableFld.setHidden(true);
        /*To Subinventory*/
        mToSubFld = new XXWCSubLOV();
        mToSubFld.setName("XXWC.TO_SUB");
        mToSubFld.setPrompt("To Sub");
        mToSubFld.setRequired(true);
        mToSubFld.setHidden(true);
        /*Bin*/
        try {
            mToLocFld = new XXWCBinsLOV();
            mToLocFld.setName("XXWC.TO_LOC");
            mToLocFld.setPrompt("To Loc");
            mToLocFld.setRequired(true);
            mToLocFld.setEditable(false);
            mToLocFld.setHidden(true);
        } catch (Exception e) {
            UtilFns.trace("Error instantiating rcvtxn locator kff");
        }
        /*UOM Properties*/
        mUOMFld = new TextFieldBean();
        mUOMFld.setName("XXWC.UOM");
        mUOMFld.setPrompt("UOM");
        mUOMFld.setEditable(false);
        mUOMFld.setHidden(true);
        /*Count Qty*/
        mQtyFld = new TextFieldBean();
        mQtyFld.setName("XXWC.QTY");
        mQtyFld.setPrompt("Qty");
        mQtyFld.setRequired(true);
        mQtyFld.setHidden(true);
        /*SaveNext*/
        mSaveNext = new ButtonFieldBean();
        mSaveNext.setName("XXWC.SAVENEXT");
        mSaveNext.setPrompt("Save/Next");
        mSaveNext.setEnableAcceleratorKey(true);
        mSaveNext.setHidden(true);
        /*Next Properties*/
        mDone = new ButtonFieldBean();
        mDone.setName("XXWC.DONE");
        mDone.setPrompt("Done");
        mDone.setEnableAcceleratorKey(true);
        mDone.setHidden(true);
        /*Close Properties*/
        mMenu = new ButtonFieldBean();
        mMenu.setName("XXWC.MENU");
        //mMenu.setPrompt("Menu"); //Removed 03/29/2016 TMS Ticket 20151103-00182
        mMenu.setPrompt("Cancel"); //Added 03/29/2016 TMS Ticket 20151103-00182
        mMenu.setEnableAcceleratorKey(true);
        mMenu.setHidden(false);
        /*set the properties for each to the addFieldBean*/
        addFieldBean(mItemFld);
        addFieldBean(mItemDescFld);
        addFieldBean(mSubFld);
        addFieldBean(mLocFld);
        addFieldBean(mOnHandFld);
        addFieldBean(mAvailableFld);
        addFieldBean(mToSubFld);
        addFieldBean(mToLocFld);
        addFieldBean(mUOMFld);
        addFieldBean(mQtyFld);
        addFieldBean(mSaveNext);
        addFieldBean(mDone);
        addFieldBean(mMenu);
        /*Point Page to the XXWCPrimaryBinAssignmentFListener Listener*/
        XXWCSubXferFListener fieldListener = new XXWCSubXferFListener();
        /*Add Listener to each property*/
        mItemFld.addListener(fieldListener);
        mItemDescFld.addListener(fieldListener);
        mSubFld.addListener(fieldListener);
        mLocFld.addListener(fieldListener);
        mOnHandFld.addListener(fieldListener);
        mAvailableFld.addListener(fieldListener);
        mToSubFld.addListener(fieldListener);
        mToLocFld.addListener(fieldListener);
        mUOMFld.addListener(fieldListener);
        mQtyFld.addListener(fieldListener);
        mSaveNext.addListener(fieldListener);
        mDone.addListener(fieldListener);
        mMenu.addListener(fieldListener);
        try {
            setPrompts(session);
        } catch (Exception exception) {
            session.setStatusMessage(UtilFns.getMessage(session, "WMS", "WMS_PROMPT_NOTSET"));
        }
    }
    
    /*************************************************************************
    *   NAME: private void setPrompts(Session session)
    *
    *   PURPOSE:   Sets Prompts for the Page
    *
    *   REVISIONS:
    *   Ver        Date        Author                     Description
    *   ---------  ----------  ---------------         -------------------------
         1.0       20-JUN-2015   Lee Spitzer             Initial Version - 
                                                         TMS Ticket Number 20150622-00003 RF - Miscellaneous Transactions
         1.1       29-MAR-2016   Lee Spitzer             TMS Ticket Number 20151103-00182 - RF - UI Improvements to reduce number of clicks
     **************************************************************************/

    private void setPrompts(Session session) {
        if (!areThePromptsSet) {
            try {
                initPrompts(session);
            } catch (SQLException sqlexception) {
                session.setStatusMessage(UtilFns.getMessage(session, "WMS", "WMS_TXN_ERROR"));
            }
        }
        mSaveNext.setPrompt(saveNextPrompt);
        mDone.setPrompt(donePrompt);
        mMenu.setPrompt(cancelPrompt);
        mSaveNext.retrieveAttributes("INV_SAVE_NEXT_PROMPT");
        mDone.retrieveAttributes("INV_DONE_PROMPT");
        mMenu.retrieveAttributes("INV_CANCEL_PROMPT");
        //mMenu.setPrompt("Menu"); //Removed 03/29/2016 TMS Ticket 20151103-00182
        
    }
   
    /*************************************************************************
    *   NAME: private void initPrompts(Session session) throws SQLException
    *
    *   PURPOSE:   Sets the initial prompts for the page
    *
    *   REVISIONS:
    *   Ver        Date        Author                     Description
    *   ---------  ----------  ---------------         -------------------------
         1.0       20-JUN-2015   Lee Spitzer             Initial Version - 
                                                         TMS Ticket Number 20150622-00003 RF - Miscellaneous Transactions
     **************************************************************************/
 
    private void initPrompts(Session session) throws SQLException {
        try {
            String s = (String)session.getObject("ORGCODE");
            setPrompt((new StringBuilder()).append("XXWC Subinventory Transfer (").append(s).append(")").toString());
            cancelPrompt =
                    MWALib.getAKPrompt(session, "oracle.apps.inv.utilities.InvResourceTable", "INV_CANCEL_PROMPT");
            saveNextPrompt =
                    MWALib.getAKPrompt(session, "oracle.apps.inv.utilities.InvResourceTable", "INV_SAVE_NEXT_PROMPT");
            donePrompt = MWALib.getAKPrompt(session, "oracle.apps.inv.utilities.InvResourceTable", "INV_DONE_PROMPT");
            areThePromptsSet = true;
        } catch (SQLException sqlexception) {
            session.setStatusMessage(UtilFns.getMessage(session, "WMS", "WMS_TXN_ERROR"));
        }
    }


    /*************************************************************************
     *   NAME: public XXWCItemLOV  getmItemFld()
     *
     *   PURPOSE:   return the value of mItemFld
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
        1.0       20-JUN-2015   Lee Spitzer             Initial Version -
                                                                TMS Ticket Number 20150622-00003 RF - Miscellaneous Transactions
     **************************************************************************/

    public XXWCItemLOV getmItemFld() 
    {
        return mItemFld;    
    }


    /*************************************************************************
     *   NAME: public TextFieldBean  getmItemDescFld()
     *
     *   PURPOSE:   return the value of mItemDescFld
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
        1.0       20-JUN-2015   Lee Spitzer             Initial Version -
                                                                TMS Ticket Number 20150622-00003 RF - Miscellaneous Transactions
     **************************************************************************/

    public TextFieldBean getmItemDescFld() 
    {
        return mItemDescFld;    
    }
        
    /*************************************************************************
     *   NAME: public TextFieldBean  getmSubFld()
     *
     *   PURPOSE:   return the value of mSubFld
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
        1.0       20-JUN-2015   Lee Spitzer             Initial Version -
                                                                TMS Ticket Number 20150622-00003 RF - Miscellaneous Transactions
     **************************************************************************/

    public XXWCSubLOV getmSubFld()
    {
        return mSubFld;
    }
    

    /*************************************************************************
     *   NAME: public XXWCBinsLOV getmLocFld()
     *
     *   PURPOSE:   return the value of mLocFld
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
             1.0       20-JUN-2015   Lee Spitzer             Initial Version -
                                                             TMS Ticket Number 20150622-00003 RF - Miscellaneous Transactions
          **************************************************************************/

    public XXWCBinsLOV getmLocFld() {
        return mLocFld;
    }


    /*************************************************************************
     *   NAME: public TextFieldBean getmOnHandFld()
     *
     *   PURPOSE:   return the value of mOnHandFld
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
        1.0       20-JUN-2015   Lee Spitzer             Initial Version -
                                                                TMS Ticket Number 20150622-00003 RF - Miscellaneous Transactions
     **************************************************************************/

    public TextFieldBean getmOnHandFld() {
        return mOnHandFld;
    }
    
    
    /*************************************************************************
     *   NAME: public TextFieldBean getmAvailableFld()
     *
     *   PURPOSE:   return the value of mAvailableFld
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
        1.0       20-JUN-2015   Lee Spitzer             Initial Version -
                                                                TMS Ticket Number 20150622-00003 RF - Miscellaneous Transactions
     **************************************************************************/

    public TextFieldBean getmAvailableFld() {
        return mAvailableFld;
    }

    /*************************************************************************
     *   NAME: public TextFieldBean  getmToSubFld()
     *
     *   PURPOSE:   return the value of mToSubFld
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
        1.0       20-JUN-2015   Lee Spitzer             Initial Version -
                                                                TMS Ticket Number 20150622-00003 RF - Miscellaneous Transactions
     **************************************************************************/

    public XXWCSubLOV getmToSubFld()
    {
        return mToSubFld;
    }
    
    /*************************************************************************
     *   NAME: public XXWCBinsLOV getmToLocFld()
     *
     *   PURPOSE:   return the value of mToLocFld
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
             1.0       20-JUN-2015   Lee Spitzer             Initial Version -
                                                             TMS Ticket Number 20150622-00003 RF - Miscellaneous Transactions
          **************************************************************************/

    public XXWCBinsLOV getmToLocFld() {
        return mToLocFld;
    }

        
    /*************************************************************************
     *   NAME: public TextFieldBean getmUOMFld()
     *
     *   PURPOSE:   return the value of mUOMFld
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
        1.0       20-JUN-2015   Lee Spitzer             Initial Version -
                                                                TMS Ticket Number 20150622-00003 RF - Miscellaneous Transactions
     **************************************************************************/

    public TextFieldBean getmUOMFld() {
        return mUOMFld;
    }
    
    /*************************************************************************
     *   NAME: public TextFieldBean mQtyFld()
     *
     *   PURPOSE:   return the value of mQtyFld
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
        1.0       20-JUN-2015   Lee Spitzer             Initial Version -
                                                                TMS Ticket Number 20150622-00003 RF - Miscellaneous Transactions
     **************************************************************************/

    public TextFieldBean getmQtyFld() {
        return mQtyFld;
    }

    /*************************************************************************
    *   NAME: public ButtonFieldBean getmSaveNext()
    *
    *   PURPOSE:   return the value of mSaveNext
    *
    *   REVISIONS:
    *   Ver        Date        Author                     Description
    *   ---------  ----------  ---------------         -------------------------
        1.0       20-JUN-2015   Lee Spitzer             Initial Version -
                                                                    TMS Ticket Number 20150622-00003 RF - Miscellaneous Transactions
    **************************************************************************/

    public ButtonFieldBean getmSaveNext() {
        return mSaveNext;
    }

    /*************************************************************************
    *   NAME: public ButtonFieldBean getmDone()
    *
    *   PURPOSE:   return the value of mDone
    *
    *   REVISIONS:
    *   Ver        Date        Author                     Description
    *   ---------  ----------  ---------------         -------------------------
        1.0       20-JUN-2015   Lee Spitzer             Initial Version -
                                                                    TMS Ticket Number 20150622-00003 RF - Miscellaneous Transactions
    **************************************************************************/

    public ButtonFieldBean getmDone() {
        return mDone;
    }
    
    /*************************************************************************
    *   NAME: public ButtonFieldBean getmMenu()
    *
    *   PURPOSE:   return the value of mMenu
    *
    *   REVISIONS:
    *   Ver        Date        Author                     Description
    *   ---------  ----------  ---------------         -------------------------
        1.0       20-JUN-2015   Lee Spitzer             Initial Version -
                                                                    TMS Ticket Number 20150622-00003 RF - Miscellaneous Transactions
    **************************************************************************/

    public ButtonFieldBean getmMenu() {
        return mMenu;
    }


    /*************************************************************************
     *   NAME: public String getUserId() getUserId()
     *
     *   PURPOSE:   return the value of m_userid
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
        1.0       20-JUN-2015   Lee Spitzer             Initial Version -
                                                                TMS Ticket Number 20150622-00003 RF - Miscellaneous Transactions
     **************************************************************************/

    public String getUserId() {
        return m_userid;
    }

    /*************************************************************************
    *   NAME: void pageEntered(MWAEvent e) throws AbortHandlerException, InterruptedHandlerException,
                                                   DefaultOnlyHandlerException {
    *
    *   PURPOSE:   pageEntered method
    *
    *   Parameters: MWAEvent mwaevent
    *
    *   REVISIONS:
    *   Ver        Date        Author                     Description
    *   ---------  ----------  ---------------         -------------------------
        1.0       20-JUN-2015   Lee Spitzer             Initial Version -
                                                                    TMS Ticket Number 20150622-00003 RF - Miscellaneous Transactions
    **************************************************************************/

    public void pageEntered(MWAEvent e) throws AbortHandlerException, InterruptedHandlerException,
                                               DefaultOnlyHandlerException {
        
        Session session = e.getSession();
        
    }

    /*************************************************************************
    *   NAME: void pageEntered(MWAEvent e) throws AbortHandlerException, InterruptedHandlerException,
                                                       DefaultOnlyHandlerException {
    *
    *   PURPOSE:   pageEntered method
    *
    *   Parameters: MWAEvent mwaevent
    *
    *   REVISIONS:
    *   Ver        Date        Author                     Description
    *   ---------  ----------  ---------------         -------------------------
        1.0       20-JUN-2015   Lee Spitzer             Initial Version -
                                                                        TMS Ticket Number 20150622-00003 RF - Miscellaneous Transactions
    **************************************************************************/

    public void pageExited(MWAEvent e) throws AbortHandlerException, InterruptedHandlerException,
                                               DefaultOnlyHandlerException {
        Session session = e.getSession();
    
    }
    
}
