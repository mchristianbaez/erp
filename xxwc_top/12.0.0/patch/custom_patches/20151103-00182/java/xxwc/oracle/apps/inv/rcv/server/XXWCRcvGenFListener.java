/*************************************************************************
   *   $Header XXWCRcvGenFListener.java $
   *   Module Name: XXWCRcvGenFListener
   *   
   *   Package: package xxwc.oracle.apps.inv.rcv.server;
   *   
   *   Imports Classes:
   *   
   *   import java.sql.*;
   *   import oracle.apps.inv.utilities.server.UtilFns;
   *   import oracle.apps.mwa.beans.*;
   *   import oracle.apps.mwa.container.MWALib;
   *   import oracle.apps.mwa.container.Session;
   *   import oracle.apps.mwa.eventmodel.*;
   *   import oracle.apps.mwa.container.FileLogger;
   *   import oracle.apps.fnd.flexj.FlexException;
   *   import oracle.apps.inv.lov.server.ItemLOV;
   *   import oracle.apps.inv.lov.server.LocatorKFF;
   *   import oracle.apps.inv.lov.server.SubinventoryLOV;
   *   import xxwc.oracle.apps.inv.lov.server.*;
   *
   *   PURPOSE:   Java Class for XXWCRcvGenFListener Page Layout.
   *    
   *   REVISIONS:
   *   Ver        Date        Author                     Description
   *   ---------  ----------  ---------------         -------------------------
        1.0       09-FEB-2015   Lee Spitzer             Initial Version - 
                                                          TMS Ticket 20150302-00152 RF - Receiving
        1.1       29-MAR-2016   Lee Spitzer             TMS Ticket Number 20151103-00182 - RF - UI Improvements to reduce number of clicks
**************************************************************************/
 package xxwc.oracle.apps.inv.rcv.server;

import java.sql.*;
import java.util.Hashtable;
import java.util.Vector;
import oracle.apps.fnd.common.VersionInfo;
import oracle.apps.inv.lov.server.*;
import oracle.apps.inv.utilities.server.*;
import oracle.apps.mwa.beans.*;
import oracle.apps.mwa.container.FileLogger;
import oracle.apps.mwa.container.Session;
import oracle.apps.mwa.eventmodel.*;
import oracle.apps.mwa.presentation.telnet.TelnetSession;
import oracle.jdbc.OraclePreparedStatement;
import oracle.sql.NUMBER;
import oracle.apps.mwa.container.MWALib;
import oracle.apps.fnd.flexj.KeyFlexfield;

import oracle.apps.inv.rcv.server.RcptGenPage;
import oracle.apps.inv.rcv.server.RcvTxnPage;

import xxwc.oracle.apps.inv.labels.server.XXWCUBDFListener;
import xxwc.oracle.apps.inv.labels.server.XXWCUBDPage;
import xxwc.oracle.apps.inv.lov.server.*;
import xxwc.oracle.apps.inv.utilities.server.*;


/*************************************************************************
 *   NAME: XXWCRcvGenFListener implements MWAFieldListener
 *
 *   PURPOSE:   Main class for XXWCRcvGenFListener
 *
 *   REVISIONS:
 *   Ver        Date        Author                     Description
 *   ---------  ----------  ---------------         -------------------------
        1.0       09-FEB-2015   Lee Spitzer             Initial Version - 
                                                          TMS Ticket 20150302-00152 RF - Receiving
 **************************************************************************/


public class XXWCRcvGenFListener implements MWAFieldListener{
    XXWCRcvGenPage pg;
    XXWCUBDPage pgUBD;
    Session ses;
    String dialogPageButtons[];
    Integer g_return = -1;
    String g_message = "";
    Integer g_cancel = 0; //added 01-JUN-2015
    //XXWCErrorHandler XXWCErrorHandler;
    
    /*************************************************************************
     *   NAME: private static String gPackage = "xxwc.oracle.apps.inv.rcv.server";
     *
     *   PURPOSE:   private method to default gPackage value for debugging
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
             1.0       09-FEB-2015   Lee Spitzer             Initial Version - 
                                                                TMS Ticket 20150302-00152 RF - Receiving
     **************************************************************************/

    private static String gPackage = "xxwc.oracle.apps.inv.rcv.server";


    /*************************************************************************
     *   NAME: private static String gCallFrom = "XXWCRcvGenFListener";
     *
     *   PURPOSE:   private method to pass gCallFrom from methods to return class
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
            1.0       09-FEB-2015   Lee Spitzer             Initial Version - 
                                                                 TMS Ticket 20150302-00152 RF - Receiving
     **************************************************************************/

    private static String gCallFrom = "XXWCRcvGenFListener";

    /*************************************************************************
     *   NAME: public XXWCRcvGenFListener()
     *
     *   PURPOSE:   public method XXWCRcvGenFListener
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
            1.0       09-FEB-2015   Lee Spitzer             Initial Version - 
                                                               TMS Ticket 20150302-00152 RF - Receiving
     **************************************************************************/

    public XXWCRcvGenFListener() {
        $init$();
    }

    /*************************************************************************
     *   NAME:  public void fieldEntered(MWAEvent mwaevent) throws AbortHandlerException, InterruptedHandlerException,
                                                       DefaultOnlyHandlerException
     *
     *   PURPOSE:   field entered for XXWCRcvGenFListener
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
            1.0       09-FEB-2015   Lee Spitzer             Initial Version - 
                                                                 TMS Ticket 20150302-00152 RF - Receiving
     **************************************************************************/
    
    public void fieldEntered(MWAEvent mwaevent) throws AbortHandlerException, InterruptedHandlerException,
                                                       DefaultOnlyHandlerException {
        ses = mwaevent.getSession();
        pg = (XXWCRcvGenPage)ses.getCurrentPage();
        String s = UtilFns.fieldEnterSource(ses);
        /*PO*/
        if (s.equals("XXWC.PO")) {
                enteredPO(mwaevent);    
                return;
        }
        /*Req*/
        if (s.equals("XXWC.REQ")) {
            enteredREQ(mwaevent);
            return;
        }
        /*RMA*/
        if (s.equals("XXWC.RMA")) {
            enteredRMA(mwaevent);
            return;
        }
        /*PO*/
        if (s.equals("XXWC.ACCEPT_DEFER")) {
            enteredAcceptanceDefer(mwaevent);
            return;
        }
        /*Item*/
        if (s.equals("XXWC.ITEM")) {
            enteredItem(mwaevent);
            return;
        }
        /*Sub*/
        if (s.equals("XXWC.SUB")) {
            enteredSub(mwaevent);
            return;
        }
        /*Internal RTV*/
        if (s.equals("XXWC.RTV")) {
            enteredInternalRTV(mwaevent);
        }
         
    }

    /*************************************************************************
     *   NAME:      public void fieldExited(MWAEvent mwaevent) throws AbortHandlerException, InterruptedHandlerException,
                                                      DefaultOnlyHandlerException
     *
     *   PURPOSE:   field exited for XXWCRcvGenFListener
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
            1.0       09-FEB-2015   Lee Spitzer             Initial Version - 
                                                                 TMS Ticket 20150302-00152 RF - Receiving
            1.1       01-JUN-2015   Lee Spitzer             Initial Version - 
                                                                  TMS Ticket 20150601-00248 - RF Break/fix - Timeout during receiving causes stuck transactions
            1.2       29-MAR-2016   Lee Spitzer             TMS Ticket Number 20151103-00182 - RF - UI Improvements to reduce number of clicks
     **************************************************************************/

    public void fieldExited(MWAEvent mwaevent) throws AbortHandlerException, InterruptedHandlerException,
                                                      DefaultOnlyHandlerException {
        String gMethod = "fieldExited";
        String gCallPoint = "Start";
        String s = ((FieldBean)mwaevent.getSource()).getName();
        String vField = mwaevent.getAction();
        boolean flag = false;
        boolean flag1 = false;
        boolean flag3 = false;
        if (mwaevent.getAction().equals("MWA_SUBMIT")) {
            flag = true;
        } else if (mwaevent.getAction().equals("MWA_PREVIOUSFIELD")) {
            flag1 = true;
        } else if (mwaevent.getAction().equals("MWA_NEXTFIELD")) {
            flag3 = true;
        }
        /*PO*/
        //if (s.equals("XXWC.PO")) { //Removed 03/29/2016 TMS Ticket 20151103-00182
        if ((flag && s.equals("XXWC.PO")) || (flag3 && s.equals("XXWC.PO"))) { //Added 03/29/2016 TMS Ticket 20151103-00182
            FileLogger.getSystemLogger().trace("mwaevent getAction for exitedPO " + mwaevent.getAction());
            exitedPO(mwaevent, ses);
            
            if (g_cancel == 1) { //Added 01-JUN-2015
                return;
            }
            else {
                if (pg.getmPOFld().getValue().equals(null) || pg.getmPOFld().getValue().equals("")) {  
                }
                else {
                    execPrintExpectedReceipts(mwaevent, ses);
                }
            }
            
            //XXWCErrorHandler XXWCErrorHandler = new XXWCErrorHandler();
            //XXWCErrorHandler.XXWCCallAPI(gPackage+"."+gCallFrom + " " + gMethod, gCallPoint , pg.getmPOFld().getValue(), "Testing Error", ses);
            
            return;
        }
        /*REQ*/
        //if (s.equals("XXWC.REQ")) { //Removed 03/29/2016 TMS Ticket 20151103-00182
        if ((flag && s.equals("XXWC.REQ")) || (flag3 && s.equals("XXWC.REQ"))) { //Added 03/29/2016 TMS Ticket 20151103-00182
            exitedREQ(mwaevent, ses);

            if (g_cancel == 1) { //Added 01-JUN-2015
                return;
            }
            else {            
                if (pg.getmReqFld().getValue().equals(null) || pg.getmReqFld().getValue().equals("")) {  
                }
                else {
                    execPrintExpectedReceipts(mwaevent, ses);
                }
            }
            
            return;
        }
        /*RMA*/
        //if (s.equals("XXWC.RMA")) { //Removed 03/29/2016 TMS Ticket 20151103-00182
        if ((flag && s.equals("XXWC.RMA")) || (flag3 && s.equals("XXWC.RMA"))) { //Added 03/29/2016 TMS Ticket 20151103-00182
            exitedRMA(mwaevent, ses);
            
            if (g_cancel == 1) { //Added 01-JUN-2015
                return;
            }
            else {
                if (pg.getmRMAFld().getValue().equals(null) || pg.getmRMAFld().getValue().equals("")) {  
                }
                else {
                    execPrintExpectedReceipts(mwaevent, ses);
                }
                return;
            }
        }
        /*Acceptance Defer*/
        if (s.equals("XXWC.ACCEPT_DEFER")) {
            exitedAcceptanceDefer(mwaevent, ses);
            return;
        }
        /*Item*/
        if (s.equals("XXWC.ITEM")) {
            exitedItem(mwaevent, ses);
            return;
        }
        /*Qty*/
        //if ((!flag1) && s.equals("XXWC.QTY")) { removed 3/9/2015
        if ((flag3 || flag) && s.equals("XXWC.QTY")) {
            exitedQty(mwaevent, ses);
            return;
        }
        /*Sub*/
        if (s.equals("XXWC.SUB")) {
            //exitedSub(mwaevent, ses);
            return;
        }
        /*Cancel*/
        if (s.equals("XXWC.RTV")) {
            exitedInternalRTV(mwaevent, ses);
            return;
        }
        /*Cancel*/
        if (flag && s.equals("XXWC.PRINT_ITEM_LABEL")) {
            exitedPrintItemLabel(mwaevent, ses);
            return;
        }
        /*Cancel*/
        if (flag && s.equals("INV.CANCEL")) {
            execDeleteGroupId(mwaevent, ses);
            clearSessionValues(mwaevent, ses);
            exitedCancel(mwaevent, ses);
            return;
        }
        
        /*SaveNext*/
        if (flag && s.equals("INV.SAVENEXT")) {
            
            String l_doc_type = "";
            String l_transaction_type = "";
            
            try {
                l_doc_type = (String)pg.getmDocType().getValue();;
                l_transaction_type = (String)pg.getmTransactionType().getValue();
            }
            catch (Exception e){
                l_doc_type = "";
                l_transaction_type = "";
            }

            /* removed 4/28/2015            
            if (l_transaction_type.equals("RTV")){
                execRTVDFFUpdate(mwaevent, ses);    
            }
            else {
                getGroupId(mwaevent, ses);
                execInsertIntoRcvInterface(mwaevent, ses);
            }
            */

            if (pg.getmInternalRTVFld().getValue().equals(null) || pg.getmInternalRTVFld().getValue().equals("")){
            }
            else {
                execRTVDFFUpdate(mwaevent, ses);
            }
            
            getGroupId(mwaevent, ses);
            execInsertIntoRcvInterface(mwaevent, ses);
                        
            exitedSave(mwaevent, ses);
            ses.setNextFieldName("XXWC.ITEM");
            pg.getmItemFld().setRequired(false);
            pg.getmDone().setHidden(false);
            //pg.getmQty().setHidden(false);
            //pg.getmQty().setRequired(false);
            return;
        }
        /*Done*/
        if (flag && s.equals("INV.DONE")) {
            gCallPoint = "INV.DONE";
            FileLogger.getSystemLogger().trace(gPackage + "." + gCallFrom + " " + gMethod + " " + gCallPoint);
            
            String l_doc_type = "";
            String l_transaction_type = "";
            
            try {
                l_doc_type = (String)pg.getmDocType().getValue();
                l_transaction_type = (String)pg.getmTransactionType().getValue();
            }
            catch (Exception e){
                l_doc_type = "";
                l_transaction_type = "";
            }
            
            FileLogger.getSystemLogger().trace(gPackage + "." + gCallFrom + " " + gMethod + " " + gCallPoint + " l_doc_type " + l_doc_type);
            FileLogger.getSystemLogger().trace(gPackage + "." + gCallFrom + " " + gMethod + " " + gCallPoint + " l_transaction_type " + l_transaction_type);
            
            /* removed 4/28/2015            
            if (l_transaction_type.equals("RTV")){
                execRTVDFFUpdate(mwaevent, ses);    
            }
            else {
                getGroupId(mwaevent, ses);
                execInsertIntoRcvInterface(mwaevent, ses);
            }
            */
            
            gCallPoint = "InternalRTV Value";
            if (pg.getmInternalRTVFld().getValue().equals(null) || pg.getmInternalRTVFld().getValue().equals("")){
                FileLogger.getSystemLogger().trace(gPackage + "." + gCallFrom + " " + gMethod + " " + gCallPoint + " is blank");
                
            }
            else {
                FileLogger.getSystemLogger().trace(gPackage + "." + gCallFrom + " " + gMethod + " " + gCallPoint + " is not blank");
                execRTVDFFUpdate(mwaevent, ses);
            }
            
            gCallPoint = "RTV";
            if (l_transaction_type.equals("RTV")){
                FileLogger.getSystemLogger().trace(gPackage + "." + gCallFrom + " " + gMethod + " " + gCallPoint + " transaction type is RTV");
                
            //    execRTVDFFUpdate(mwaevent, ses);
                ses.setStatusMessage(pg.WMS_TXN_SUCCESS);
                ses.clearAllApplicationScopeObjects();
                pg.getmDone().setNextPageName("|END_OF_TRANSACTION|");
                   
            }
            else {
                
                FileLogger.getSystemLogger().trace(gPackage + "." + gCallFrom + " " + gMethod + " " + gCallPoint + " transaction type is not RTV");
                //setPrinter(mwaevent, ses);
                //setLabelPrinter(mwaevent, ses);
                getGroupId(mwaevent, ses);
                getDefaultCarrier(mwaevent, ses);
                
                gCallPoint = "Getting Item and Qty Field ";
                if ((pg.getmItemFld().getValue().equals(null) || pg.getmItemFld().getValue().equals("")) && (pg.getmQty().getValue().equals("") || pg.getmQty().getValue().equals(null))){   
                    FileLogger.getSystemLogger().trace(gPackage + "." + gCallFrom + " " + gMethod + " " + gCallPoint + " item and qty field is null");
                    execInsertIntoRcvInterface(mwaevent, ses);
                }
                else {
                    FileLogger.getSystemLogger().trace(gPackage + "." + gCallFrom + " " + gMethod + " " + gCallPoint + " item and qty field is not null");
                    execInsertIntoRcvInterface(mwaevent, ses);
                }
                
                FileLogger.getSystemLogger().trace(gPackage + "." + gCallFrom + " " + gMethod + " " + gCallPoint + " calling exitedDone ");
                exitedDone(mwaevent, ses);
            }

            
            return;
        }
        else {
            return;
        }
    }

    /*************************************************************************
     *   NAME:      private void $init$()
     *
     *   PURPOSE:   $init$ sets the diaglogButtons button for user prompt messages to OK
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
         1.0       09-FEB-2015   Lee Spitzer             Initial Version - 
                                                             TMS Ticket 20150302-00152 RF - Receiving
     **************************************************************************/

    private void $init$() {
        dialogPageButtons = (new String[] { "OK" });
    }



    /*************************************************************************
    *   NAME: public void enteredPO(MWAEvent mwaevent) throws AbortHandlerException, InterruptedHandlerException,
                                                     DefaultOnlyHandlerException
    *
    *   PURPOSE:  Entered Item Field Listener Process to set the List of Values for the Subinventory Field
    *
    *   REVISIONS:
    *   Ver        Date        Author                     Description
    *   ---------  ----------  ---------------         -------------------------
             1.0       09-FEB-2015   Lee Spitzer             Initial Version - 
                                                                  TMS Ticket 20150302-00152 RF - Receiving
    **************************************************************************/

    public void enteredPO(MWAEvent mwaevent) throws AbortHandlerException, InterruptedHandlerException,
                                                    DefaultOnlyHandlerException {

        
       try {
           String gMethod = "enteredPO";
           String gCallPoint = "Start";
           String l_doc_type;
           String l_transaction_type;
           String l_group_id;
           try {
               l_doc_type = (String)ses.getObject("TXN.DOCTYPE");
               l_transaction_type = (String)ses.getObject("TXN.TRANSACTION");
           }
           catch (Exception e){
               FileLogger.getSystemLogger().trace(gPackage + "." + gCallFrom + " " + gMethod + " " + gCallPoint + " " + e);
               l_doc_type = "";
               l_transaction_type = "";
           }
           
           ses.putSessionObject("sessXXWC.DOCTYPE", l_doc_type);
           ses.putSessionObject("sessXXWC.TRANSACTION_TYPE",l_transaction_type);
           
           pg.getmDocType().setValue(l_doc_type);
           pg.getmTransactionType().setValue(l_transaction_type);
           
           pg.getmDocType().setHidden(true);
           pg.getmTransactionType().setHidden(true);
           
           XXWCPOLOV poLOV = pg.getmPOFld();
           poLOV.setValidateFromLOV(true);
           poLOV.setlovStatement("XXWC_RCV_MOB_PKG.GET_PO_LOV");
           String paramType[] = { "C", "N", "S" };
           String parameters[] =
           { " ", "ORGID", "xxwc.oracle.apps.inv.rcv.server.XXWCRcvGenPage.XXWC.PO" };
           poLOV.setInputParameterTypes(paramType);
           poLOV.setInputParameters(parameters);
       } catch (Exception e) {
           UtilFns.error("Error in calling entered PO " + e);
       }
    }


    /*************************************************************************
    *   NAME: public void enteredREQ(MWAEvent mwaevent) throws AbortHandlerException, InterruptedHandlerException,
                                                     DefaultOnlyHandlerException
    *
    *   PURPOSE:  Entered Item Field Listener Process to set the List of Values for the Subinventory Field
    *
    *   REVISIONS:
    *   Ver        Date        Author                     Description
    *   ---------  ----------  ---------------         -------------------------
             1.0       09-FEB-2015   Lee Spitzer             Initial Version - 
                                                                  TMS Ticket 20150302-00152 RF - Receiving
    **************************************************************************/

    public void enteredREQ(MWAEvent mwaevent) throws AbortHandlerException, InterruptedHandlerException,
                                                    DefaultOnlyHandlerException {

        
       try {
           String gMethod = "enteredREQ";
           String gCallPoint = "Start";
           String l_doc_type;
           String l_transaction_type;
           String l_group_id;
           try {
               l_doc_type = (String)ses.getObject("TXN.DOCTYPE");
               l_transaction_type = (String)ses.getObject("TXN.TRANSACTION");
           }
           catch (Exception e){
               FileLogger.getSystemLogger().trace(gPackage + "." + gCallFrom + " " + gMethod + " " + gCallPoint + " " + e);
               l_doc_type = "";
               l_transaction_type = "";
           }
                 
           ses.putSessionObject("sessXXWC.DOC_TYPE",l_doc_type);
           ses.putSessionObject("sessXXWC.TRANSACTION_TYPE",l_transaction_type);
           
           pg.getmDocType().setValue(l_doc_type);
           pg.getmTransactionType().setValue(l_transaction_type);
           
           pg.getmDocType().setHidden(true);
           pg.getmTransactionType().setHidden(true);
           
           XXWCREQLOV reqLOV = pg.getmReqFld();
           reqLOV.setValidateFromLOV(true);
           reqLOV.setlovStatement("XXWC_RCV_MOB_PKG.GET_REQ_LOV");
           String paramType[] = { "C", "N", "S" };
           String parameters[] =
           { " ", "ORGID", "xxwc.oracle.apps.inv.rcv.server.XXWCRcvGenPage.XXWC.REQ" };
           reqLOV.setInputParameterTypes(paramType);
           reqLOV.setInputParameters(parameters);
       } catch (Exception e) {
           UtilFns.error("Error in calling enteredREQ " + e);
       }
    }


    /*************************************************************************
    *   NAME: public void enteredRMA(MWAEvent mwaevent) throws AbortHandlerException, InterruptedHandlerException,
                                                     DefaultOnlyHandlerException
    *
    *   PURPOSE:  Entered Item Field Listener Process to set the List of Values for the Subinventory Field
    *
    *   REVISIONS:
    *   Ver        Date        Author                     Description
    *   ---------  ----------  ---------------         -------------------------
             1.0       09-FEB-2015   Lee Spitzer             Initial Version - 
                                                                  TMS Ticket 20150302-00152 RF - Receiving
    **************************************************************************/

    public void enteredRMA(MWAEvent mwaevent) throws AbortHandlerException, InterruptedHandlerException,
                                                    DefaultOnlyHandlerException {

        
       try {
           String gMethod = "enteredRMA";
           String gCallPoint = "Start";
           String l_doc_type;
           String l_transaction_type;
           String l_group_id;
           try {
               l_doc_type = (String)ses.getObject("TXN.DOCTYPE");
               l_transaction_type = (String)ses.getObject("TXN.TRANSACTION");
           }
           catch (Exception e){
               FileLogger.getSystemLogger().trace(gPackage + "." + gCallFrom + " " + gMethod + " " + gCallPoint + " " + e);
               l_doc_type = "";
               l_transaction_type = "";
           }
                     
           ses.putSessionObject("sessXXWC.DOC_TYPE",l_doc_type);
           ses.putSessionObject("sessXXWC.TRANSACTION_TYPE",l_transaction_type);
           
           pg.getmDocType().setValue(l_doc_type);
           pg.getmTransactionType().setValue(l_transaction_type);
           
           pg.getmDocType().setHidden(true);
           pg.getmTransactionType().setHidden(true);
           
           XXWCRMALOV rmaLOV = pg.getmRMAFld();
           rmaLOV.setValidateFromLOV(true);
           rmaLOV.setlovStatement("XXWC_RCV_MOB_PKG.GET_RMA_LOV");
           String paramType[] = { "C", "N", "S" };
           String parameters[] =
           { " ", "ORGID", "xxwc.oracle.apps.inv.rcv.server.XXWCRcvGenPage.XXWC.RMA" };
           rmaLOV.setInputParameterTypes(paramType);
           rmaLOV.setInputParameters(parameters);
       } catch (Exception e) {
           UtilFns.error("Error in calling enteredREQ " + e);
       }
    }


    /*************************************************************************
    *   NAME: public void enteredInternalRTV(MWAEvent mwaevent) throws AbortHandlerException, InterruptedHandlerException,
                                                     DefaultOnlyHandlerException
    *
    *   PURPOSE:  Entered Item Field Listener Process to set the List of Values for the Subinventory Field
    *
    *   REVISIONS:
    *   Ver        Date        Author                     Description
    *   ---------  ----------  ---------------         -------------------------
             1.0       09-FEB-2015   Lee Spitzer             Initial Version - 
                                                                  TMS Ticket 20150302-00152 RF - Receiving
    **************************************************************************/

    public void enteredInternalRTV (MWAEvent mwaevent) throws AbortHandlerException, InterruptedHandlerException,
                                                    DefaultOnlyHandlerException {

        
       try {
           XXWCInternalRTVLOV internalRTVLOV = pg.getmInternalRTVFld();
           internalRTVLOV.setValidateFromLOV(true);
           internalRTVLOV.setlovStatement("XXWC_RCV_MOB_PKG.GET_RTV_LOV");
           String paramType[] = { "C", "S"};;
           String parameters[] =
           { " ", "xxwc.oracle.apps.inv.rcv.server.XXWCRcvGenPage.XXWC.RTV" };
           internalRTVLOV.setInputParameterTypes(paramType);
           internalRTVLOV.setInputParameters(parameters);
       } catch (Exception e) {
           UtilFns.error("Error in calling enteredREQ " + e);
       }
    }


    /*************************************************************************
    *   NAME: public void exitedInternalRTV(MWAEvent mwaevent, Session ses) throws AbortHandlerException, InterruptedHandlerException,
                                                    DefaultOnlyHandlerException
    *
    *   PURPOSE:  Exited Subinventory Field Listener Process to determine if the Locator Field should be displayed
    *
    *   REVISIONS:
    *   Ver        Date        Author                     Description
    *   ---------  ----------  ---------------         -------------------------
            1.0       09-FEB-2015   Lee Spitzer             Initial Version - 
                                                                 TMS Ticket 20150302-00152 RF - Receiving
    **************************************************************************/

    public void exitedInternalRTV(MWAEvent mwaevent, Session ses) throws AbortHandlerException, InterruptedHandlerException,
                                                                DefaultOnlyHandlerException {
       try {
            pg.getmSaveNext().setHidden(false);
            pg.getmDone().setHidden(false);
       } catch (Exception e) {
           UtilFns.error("Error in calling exitedInternalRTV " + e);
       }
    }


    
    /*************************************************************************
    *   NAME: public void enteredItem(MWAEvent mwaevent) throws AbortHandlerException, InterruptedHandlerException,
                                                     DefaultOnlyHandlerException
    *
    *   PURPOSE:  Entered Item Field Listener Process to set the List of Values for the Subinventory Field
    *
    *   REVISIONS:
    *   Ver        Date        Author                     Description
    *   ---------  ----------  ---------------         -------------------------
             1.0       09-FEB-2015   Lee Spitzer             Initial Version - 
                                                                  TMS Ticket 20150302-00152 RF - Receiving
    **************************************************************************/

    public void enteredItem(MWAEvent mwaevent) throws AbortHandlerException, InterruptedHandlerException,
                                                    DefaultOnlyHandlerException {

        
        String gMethod = "enteredItem";
        String gCallPoint = "Start";
        
        String l_doc_type = "";
        String l_transaction_type = "";
        
        try {
            l_doc_type = (String)pg.getmDocType().getValue();;
            l_transaction_type = (String)pg.getmTransactionType().getValue();
        }
        catch (Exception e){
            FileLogger.getSystemLogger().trace(gPackage + "." + gCallFrom + " " + gMethod + " " + gCallPoint + " " + e);
            l_doc_type = "";
            l_transaction_type = "";
        }
       
       try {
           if (l_doc_type.equals("PO")) {
               ses.putSessionObject("sessPOHeaderId", pg.getmPOFld().getmPOHeaderId());
               ses.putSessionObject("sessReqHeaderId", "");
               ses.putSessionObject("sessShipmentHeaderId", "");
               ses.putSessionObject("sessRMAHeaderId", "");
               ses.putSessionObject("sessShipFromOrgId", "");
               ses.putSessionObject("sessGroupId", pg.getmGroupId().getValue());
           }
           if (l_doc_type.equals("REQ")) {
               ses.putSessionObject("sessPOHeaderId", "");  
               ses.putSessionObject("sessReqHeaderId", pg.getmReqFld().getmREQHeaderId());
               ses.putSessionObject("sessShipmentHeaderId", pg.getmReqFld().getmShipmentHeaderId());
               ses.putSessionObject("sessRMAHeaderId", "");
               ses.putSessionObject("sessShipFromOrgId", pg.getmReqFld().getmShipFromOrgId());
               ses.putSessionObject("sessGroupId", pg.getmGroupId().getValue());
           }
           if (l_doc_type.equals("RMA")) {
               ses.putSessionObject("sessPOHeaderId", "");  
               ses.putSessionObject("sessReqHeaderId", "");
               ses.putSessionObject("sessShipmentHeaderId", "");
               ses.putSessionObject("sessRMAHeaderId", pg.getmRMAFld().getmRMAHeaderId());
               ses.putSessionObject("sessShipFromOrgId", "");
               ses.putSessionObject("sessGroupId", pg.getmGroupId().getValue());
           }
           XXWCItemLOV itemLOV = pg.getmItemFld();
           itemLOV.setValidateFromLOV(true);
           itemLOV.setlovStatement("XXWC_RCV_MOB_PKG.GET_ITEM_LOV");
           //                      0    1    2    3    4    5    6    7    8    9
           String paramType[] = { "C", "N", "S", "S", "S", "S", "S", "S", "S", "S"};
           //                                     0       1       2              3     4              5                    6                      7                  8
           String prompts[] = { "INVENTORY_ITEM_ID" , "ITEM", "QTY", "RUNNING_QTY", "UOM", "DESCRIPTION", "LOT_CONTROL_CODE", "SERIAL_CONTROL_CODE", "SHELF_LIFE_DAYS" };
           //                    0     1     2     3     4     5     6       7      8
           boolean flag[] = {false, true, true, true, true, true, false, false, false};
           //                        0        1                                                           2                            3                 4                 5                       6                   7             8                  9
           String parameters [] = { " ", "ORGID", "xxwc.oracle.apps.inv.rcv.server.XXWCRcvGenPage.XXWC.ITEM", "sessXXWC.TRANSACTION_TYPE", "sessPOHeaderId", "sessReqHeaderId", "sessShipmentHeaderId", "sessRMAHeaderId", "sessShipFromOrgId", "sessGroupId" };
           //String parameters[] = { " ", "ORGID", "xxwc.oracle.apps.inv.rcv.server.XXWCRcvGenPage.XXWC.ITEM", "TXN.TRANSACTION", "", "sessReqHeaderId", "", "" };
           itemLOV.setInputParameterTypes(paramType);
           itemLOV.setInputParameters(parameters);
           itemLOV.setSubfieldPrompts(prompts);
           itemLOV.setSubfieldDisplays(flag);
       } catch (Exception e) {
           UtilFns.error("Error in calling enteredItem " + e);
       }
    }
    
    /*************************************************************************
    *   NAME: public void exitedPO(MWAEvent mwaevent, Session ses) throws AbortHandlerException, InterruptedHandlerException,
                                                    DefaultOnlyHandlerException
    *
    *   PURPOSE:  Exited Subinventory Field Listener Process to determine if the Locator Field should be displayed
    *
    *   REVISIONS:
    *   Ver        Date        Author                     Description
    *   ---------  ----------  ---------------         -------------------------
            1.0       09-FEB-2015   Lee Spitzer             Initial Version - 
                                                                 TMS Ticket 20150302-00152 RF - Receiving
            1.1       29-MAR-2016   Lee Spitzer             TMS Ticket Number 20151103-00182 - RF - UI Improvements to reduce number of clicks
    **************************************************************************/

    public void exitedPO(MWAEvent mwaevent, Session ses) throws AbortHandlerException, InterruptedHandlerException,
                                                                DefaultOnlyHandlerException {
       String gMethod = "exitedPO";
       String gCallPoint = "Start";
       int x_return = 0;
       String p_po_header_id = "";
       String l_note_to_receiver = "";
       
       checkStuckGroupId(mwaevent, ses); //Added 01-JUN-2015
        
       if (g_cancel == 1) { //Added 01-JUN-2015
           XXWCRcvGenPage _tmp = pg;
           ses.setStatusMessage(pg.WMS_TXN_CANCEL);
           ses.clearAllApplicationScopeObjects();
           ses.setStatusMessage(XXWCRcvGenPage.WMS_TXN_CANCEL);
           ses.setNextFieldName("");
           pg.getmPOFld().setNextPageName("|END_OF_TRANSACTION|");
           FileLogger.getSystemLogger().trace(gPackage + "." + gCallFrom + " " + gMethod + " " + gCallPoint);
       }
       else {
       try {
            gCallPoint = "Entering Try";
            pg.getmVendorName().setValue(pg.getmPOFld().getmVendorName());
            pg.getmNoteToReceiver().setValue(pg.getmPOFld().getmNoteToReceiver());
            pg.mVendorName.setHidden(false);
            pg.mNoteToReceiver.setHidden(false);
       
            p_po_header_id = pg.getmPOFld().getmPOHeaderId();
            l_note_to_receiver = pg.getmNoteToReceiver().getValue();
           
            FileLogger.getSystemLogger().trace(gPackage + "." + gCallFrom + " " + gMethod + " " + gCallPoint + " p_po_header_id " + p_po_header_id );
           
            //Calling PO Acceptance Check; if the value is 0 then trigger acceptance message
            CallableStatement cstmt = null;
            Connection con = ses.getConnection();
            cstmt = con.prepareCall("{? = call XXWC_RCV_MOB_PKG.get_po_acceptance_notes_cnt(?)");
            cstmt.registerOutParameter(1, Types.INTEGER);
            cstmt.setString(2, p_po_header_id);
            cstmt.execute();
            x_return = cstmt.getInt(1);
            cstmt.close();
            
           FileLogger.getSystemLogger().trace(gPackage + "." + gCallFrom + " " + gMethod + " " + gCallPoint + " x_return " + x_return );
           
           if (x_return == 0) {
               TelnetSession telnetsessionX = (TelnetSession)ses;
               Integer k =
                   telnetsessionX.showPromptPage("Warning!", "PO Confirmation from this Supplier is not processed on this PO, please contact buyer before Receiving. Note to receiver is " + l_note_to_receiver + "."  , (new String[] { "Continue", "Exit" }) );
               if (k == 0) {
                   pg.getmPOAcceptanceDeferReasonsFld().setEditable(true);
                   pg.getmPOAcceptanceDeferReasonsFld().setRequired(true);
                   pg.getmPOAcceptanceDeferReasonsFld().setHidden(false);
                   return;
               } else if (k == 1) {
                   ses.putSessionObject("sessXXWC.ACCEPT_DEFER", "");
                   //exitedCancel(mwaevent, ses);
                   XXWCRcvGenPage _tmp = pg;
                   ses.setStatusMessage(pg.WMS_TXN_CANCEL);
                   ses.clearAllApplicationScopeObjects();
                   ses.setStatusMessage(XXWCRcvGenPage.WMS_TXN_CANCEL);
                   ses.setNextFieldName("");
                   pg.getmPOFld().setNextPageName("|END_OF_TRANSACTION|");
                   FileLogger.getSystemLogger().trace(gPackage + "." + gCallFrom + " " + gMethod + " " + gCallPoint);
                   g_cancel = 1; //Added 03/29/2015 TMS Ticket 20151103-00182
               }
               else {
                   ses.putSessionObject("sessXXWC.ACCEPT_DEFER", "");
                   pg.getmPOAcceptanceDeferReasonsFld().setEditable(false);
                   pg.getmPOAcceptanceDeferReasonsFld().setRequired(false);
                   pg.getmPOAcceptanceDeferReasonsFld().setHidden(true);
                   g_cancel = 1; //Added 03/29/2015 TMS Ticket 20151103-00182
                   return;
               }
           }
           else {
                  ses.putSessionObject("sessXXWC.ACCEPT_DEFER", "");
           }
       } catch (Exception e) {
           FileLogger.getSystemLogger().trace(gPackage + "." + gCallFrom + " " + gMethod + " " + gCallPoint + " Error in calling exitedPO " + e );
           UtilFns.error("Error in calling exitedPO " + e);
       }
       }
    }

    
    /*************************************************************************
    *   NAME: public void exitedREQ(MWAEvent mwaevent, Session ses) throws AbortHandlerException, InterruptedHandlerException,
                                                    DefaultOnlyHandlerException
    *
    *   PURPOSE:  Exited Subinventory Field Listener Process to determine if the Locator Field should be displayed
    *
    *   REVISIONS:
    *   Ver        Date        Author                     Description
    *   ---------  ----------  ---------------         -------------------------
            1.0       09-FEB-2015   Lee Spitzer             Initial Version - 
                                                                 TMS Ticket 20150302-00152 RF - Receiving
    **************************************************************************/

    public void exitedREQ(MWAEvent mwaevent, Session ses) throws AbortHandlerException, InterruptedHandlerException,
                                                                DefaultOnlyHandlerException {
        String gMethod = "exitedREQ";
        String gCallPoint = "Start";
        try {
            
            checkStuckGroupId(mwaevent, ses); //Added 01-JUN-2015
            
            if (g_cancel == 1) { //Added 01-JUN-2015
                XXWCRcvGenPage _tmp = pg;
                ses.setStatusMessage(pg.WMS_TXN_CANCEL);
                ses.clearAllApplicationScopeObjects();
                ses.setStatusMessage(XXWCRcvGenPage.WMS_TXN_CANCEL);
                ses.setNextFieldName("");
                pg.getmReqFld().setNextPageName("|END_OF_TRANSACTION|");
                FileLogger.getSystemLogger().trace(gPackage + "." + gCallFrom + " " + gMethod + " " + gCallPoint);
                }
            else {
                pg.getmVendorName().setValue(pg.getmReqFld().getmShipFromOrg());
                //pg.getmNoteToReceiver().setValue(pg.getmPOFld().getmNoteToReceiver());
                pg.mVendorName.setHidden(false);
                pg.mVendorName.setPrompt("Ship Org");
                //pg.mNoteToReceiver.setHidden(false);
            }
       } catch (Exception e) {
           UtilFns.error("Error in calling  exitedREQ " + e);
       }
    }


    /*************************************************************************
    *   NAME: public void exitedRMA(MWAEvent mwaevent, Session ses) throws AbortHandlerException, InterruptedHandlerException,
                                                    DefaultOnlyHandlerException
    *
    *   PURPOSE:  Exited Subinventory Field Listener Process to determine if the Locator Field should be displayed
    *
    *   REVISIONS:
    *   Ver        Date        Author                     Description
    *   ---------  ----------  ---------------         -------------------------
            1.0       09-FEB-2015   Lee Spitzer             Initial Version - 
                                                                 TMS Ticket 20150302-00152 RF - Receiving
    **************************************************************************/

    public void exitedRMA(MWAEvent mwaevent, Session ses) throws AbortHandlerException, InterruptedHandlerException,
                                                                DefaultOnlyHandlerException {
        String gMethod = "exitedRMA";
        String gCallPoint = "Start";
        
        try {
            
            checkStuckGroupId(mwaevent, ses); //Added 01-JUN-2015
           
            if (g_cancel == 1) { //Added 01-JUN-2015
                XXWCRcvGenPage _tmp = pg;
                ses.setStatusMessage(pg.WMS_TXN_CANCEL);
                ses.clearAllApplicationScopeObjects();
                ses.setStatusMessage(XXWCRcvGenPage.WMS_TXN_CANCEL);
                ses.setNextFieldName("");
                pg.getmRMAFld().setNextPageName("|END_OF_TRANSACTION|");
                FileLogger.getSystemLogger().trace(gPackage + "." + gCallFrom + " " + gMethod + " " + gCallPoint);
            }
            else {
                pg.getmVendorName().setValue(pg.getmRMAFld().getmPartyName());
                //pg.getmNoteToReceiver().setValue(pg.getmPOFld().getmNoteToReceiver());
                pg.mVendorName.setHidden(false);
                pg.mVendorName.setPrompt("Cust");
                //pg.mNoteToReceiver.setHidden(false);
            }
       } catch (Exception e) {
           UtilFns.error("Error in calling exitedRMA " + e);
       }
    }


    /*************************************************************************
    *   NAME: public void exitedItem(MWAEvent mwaevent, Session ses) throws AbortHandlerException, InterruptedHandlerException,
                                                    DefaultOnlyHandlerException
    *
    *   PURPOSE:  Exited Subinventory Field Listener Process to determine if the Locator Field should be displayed
    *
    *   REVISIONS:
    *   Ver        Date        Author                     Description
    *   ---------  ----------  ---------------         -------------------------
            1.0       09-FEB-2015   Lee Spitzer             Initial Version - 
                                                                 TMS Ticket 20150302-00152 RF - Receiving
    **************************************************************************/

    public void exitedItem(MWAEvent mwaevent, Session ses) throws AbortHandlerException, InterruptedHandlerException,
                                                                 DefaultOnlyHandlerException {
         
       
        String gMethod = "exitedItem";
        String gCallPoint = "Start";
        
        String l_doc_type = "";
        String l_transaction_type = "";
         
        try {
            l_doc_type = (String)pg.getmDocType().getValue();
            l_transaction_type = (String)pg.getmTransactionType().getValue();
        }
        catch (Exception e){
            FileLogger.getSystemLogger().trace(gPackage + "." + gCallFrom + " " + gMethod + " " + gCallPoint + " " + e);
            l_doc_type = "";
            l_transaction_type = "";
        }
       
        gCallPoint = "Getting Item Field Value";
        if (pg.getmItemFld().getValue().equals(null) || pg.getmItemFld().getValue().equals("")) {
            FileLogger.getSystemLogger().trace(gPackage + "." + gCallFrom + " " + gMethod + " " + gCallPoint + " is null ");
             pg.getmQty().setHidden(false);
             pg.getmQty().setEditable(false);
             pg.getmSubFld().setHidden(false);
             pg.getmSubFld().setEditable(false);
             pg.getmSubFld().setValue("");
             //ses.setNextFieldName("INV.DONE");
             return;
        }
        else {
            FileLogger.getSystemLogger().trace(gPackage + "." + gCallFrom + " " + gMethod + " " + gCallPoint + " is not null ");
                                                                                                
            if (!l_transaction_type.equals("RTV")) {
               try {
                    pg.getmItemFld().getValue().toUpperCase();
                    pg.getmItemFld().setValue(pg.getmItemFld().getmItemNumber());
                    pg.mItemDescription.setHidden(false);
                    pg.mUOMFld.setHidden(false);
                    pg.mQtyFld.setHidden(false);
                    pg.mQtyFld.setEditable(true);
                    pg.mRunningQtyFld.setHidden(false);
                    pg.mSubFld.setHidden(false);
                    pg.getmSubFld().setEditable(true);
                    pg.getmItemDescriptionFld().setValue(pg.getmItemFld().getmItemDescription());
                    pg.getmUOMFld().setValue(pg.getmItemFld().getmPrimaryUOMCode());
                    pg.getmQty().setValue(pg.getmItemFld().getmQuantityOpen());
                    pg.getmRunningQty().setValue(pg.getmItemFld().getmRunningQty());
                    pg.getmSubFld().setValue("General");
                   
               } catch (Exception e) {
                   UtilFns.error("Error in calling exitedItem" + e);
               }
            }
            else {
                try {
                    pg.getmItemFld().getValue().toUpperCase();
                    pg.getmItemFld().setValue(pg.getmItemFld().getmItemNumber());
                    pg.mItemDescription.setHidden(false);
                    pg.mUOMFld.setHidden(false);
                    pg.mQtyFld.setHidden(false);
                    pg.mQtyFld.setEditable(false);
                    pg.mRunningQtyFld.setHidden(false);
                    pg.mSubFld.setHidden(true);
                    pg.getmItemDescriptionFld().setValue(pg.getmItemFld().getmItemDescription());
                    pg.getmUOMFld().setValue(pg.getmItemFld().getmPrimaryUOMCode());
                    pg.getmQty().setValue(pg.getmItemFld().getmQuantityOpen());
                    pg.getmRunningQty().setValue(pg.getmItemFld().getmRunningQty());
                    pg.getmInternalRTVFld().setHidden(false);
                } catch (Exception e) {
                    UtilFns.error("Error in calling exitedItem" + e);
                }
            }
            
            if(l_transaction_type.equals("RTV") && l_doc_type.equals("REQ")) {
                pg.getmInternalRTVFld().setHidden(false);
            }
            else if (l_transaction_type.equals("RECEIVE") && l_doc_type.equals("REQ")) {
                pg.getmInternalRTVFld().setHidden(false);
            }
            else {
                pg.getmInternalRTVFld().setHidden(true);
            }
          
        }
    }


    /*************************************************************************
    *   NAME: public void enteredSub(MWAEvent mwaevent) throws AbortHandlerException, InterruptedHandlerException,
                                                     DefaultOnlyHandlerException
    *
    *   PURPOSE:  Entered Item Field Listener Process to set the List of Values for the Subinventory Field
    *
    *   REVISIONS:
    *   Ver        Date        Author                     Description
    *   ---------  ----------  ---------------         -------------------------
             1.0       09-FEB-2015   Lee Spitzer             Initial Version - 
                                                                  TMS Ticket 20150302-00152 RF - Receiving
    **************************************************************************/

    public void enteredSub(MWAEvent mwaevent) throws AbortHandlerException, InterruptedHandlerException,
                                                    DefaultOnlyHandlerException {

        
       try {
           XXWCSubLOV subLOV = pg.getmSubFld();
           subLOV.setValidateFromLOV(true);
           subLOV.setlovStatement("XXWC_MWA_ROUTINES_PKG.GET_WC_SUB_LOV");
           //                      0    1    2    3
           String paramType[] = { "C", "N", "S", "S"};
           //                                     0               1               2                 3              4                   5                      6      7    
           String prompts[] = { "SUBINVENTORY_CODE" , "DESCRIPTION", "LOCATOR_TYPE", "ASSET_INVENTORY"};
           //                    0     1      2     3 
           boolean flag[] = {true, true, false, false};
           //                        0        1                                                          2    3 
           String parameters[] = { " ", "ORGID", "xxwc.oracle.apps.inv.rcv.server.XXWCRcvGenPage.XXWC.SUB",  ""};
           subLOV.setInputParameterTypes(paramType);
           subLOV.setInputParameters(parameters);
       } catch (Exception e) {
           UtilFns.error("Error in calling enteredSub " + e);
       }
    }


    /*************************************************************************
    *   NAME: public void exitedQty(MWAEvent mwaevent, Session ses) throws AbortHandlerException, InterruptedHandlerException,
                                                    DefaultOnlyHandlerException
    *
    *   PURPOSE:  Exited Subinventory Field Listener Process to determine if the Locator Field should be displayed
    *
    *   REVISIONS:
    *   Ver        Date        Author                     Description
    *   ---------  ----------  ---------------         -------------------------
            1.0       09-FEB-2015   Lee Spitzer             Initial Version - 
                                                                 TMS Ticket 20150302-00152 RF - Receiving
    **************************************************************************/

    public void exitedQty(MWAEvent mwaevent, Session ses) throws AbortHandlerException, InterruptedHandlerException,
                                                                    DefaultOnlyHandlerException {
        
    String gMethod = "exitedQty";
    String gCallPoint = "Start";
    FileLogger.getSystemLogger().trace(gPackage + "." + gCallFrom + " " + gMethod + " " + gCallPoint);
    String p_organization_id;
    String p_item;
    String l_time_sensitive = "N";
    
    p_organization_id = (String)ses.getObject("ORGID");
    
    pg.getmQty().setValue(pg.getmQty().getValue().trim());
    if (pg.getmQty().getValue().equals(null)) {
        return;
    } 
    else {
        gCallPoint = "Calling XXWC_RCV_MOB_PKG.validate_rcv_qty_val ";
        try {
            CallableStatement cstmt = null;
            Connection con = ses.getConnection();
            cstmt = con.prepareCall("{call XXWC_RCV_MOB_PKG.validate_rcv_qty_val(?,?,?,?)}");
            cstmt.setString(1, pg.getmQty().getValue());
            cstmt.setString(2, pg.getmRunningQty().getValue());
            cstmt.registerOutParameter(3, Types.INTEGER); //x_return
            cstmt.registerOutParameter(4, Types.VARCHAR); //x_message
            cstmt.execute();
            Integer x_return = cstmt.getInt(3);
            String  x_message = cstmt.getString(4);
            cstmt.close();
            boolean showSubError = true;
            if (x_return > 0) {
                TelnetSession telnetsessionX = (TelnetSession)ses;
                Integer k =
                    telnetsessionX.showPromptPage("Error!", x_message,
                                                  dialogPageButtons);
                if (k == 0) {
                    pg.getmQty().setValue(null);
                    ses.setNextFieldName("XXWC.QTY");
                } else {
                    return;
                }
            }
            if (x_return == 0) {
                pg.getmPrintItemLabel().setHidden(false);
                pg.getmCancel().setHidden(false);
                pg.getmSaveNext().setHidden(false);
                pg.getmDone().setHidden(false);
                ses.setNextFieldName("INV.SAVENEXT");
            }
            p_item = pg.getmItemFld().getValue();
            gCallPoint = "Calling checkItemIsTS ";
            l_time_sensitive = checkItemIsTS(p_organization_id, p_item, ses);
            if(l_time_sensitive.equals("Y")){
                ses.setStatusMessage("Item requires UBD Label.  Press Enter");
                String l_current_page = ses.getCurrentPageName();
                ses.putSessionObject("sessPreviousPage", l_current_page);
                ses.putSessionObject("sessPreviousField", "INV.SAVENEXT");
                ses.putSessionObject("sessXXWC.SHELF_LIFE", pg.getmItemFld().getmShelfLifeDays());
                ses.putSessionObject("sessXXWC.COPIES",pg.getmQty().getValue());
                ses.putSessionObject("sessXXWC.UBD_STATUS", "4X1");
                pg.getmQty().setNextPageName("xxwc.oracle.apps.inv.labels.server.XXWCUBDPage");
                ses.setNextFieldName("XXWC.UBD");
            }
            else {
                pg.getmQty().setNextPageName("xxwc.oracle.apps.inv.rcv.server.XXWCRcvGenPage");
                ses.putSessionObject("sessPreviousPage", "");
                ses.putSessionObject("sessPreviousField", "");
                ses.putSessionObject("sessXXWC.SHELF_LIFE", "");
                ses.putSessionObject("sessXXWC.COPIES","");
                ses.putSessionObject("sessXXWC.UBD_STATUS", "");
                
            }
        }
        catch (Exception e) {
            UtilFns.error("Error in validating Quantity" + e);
            }
        gCallPoint = "Calling XXWC_RCV_MOB_PKG.GET_RCV_TOLERANCE ";
        try {
            String p_doctype;
            String p_source_header_id;
            p_doctype = pg.getmDocType().getValue();
            p_source_header_id = "";
            if (p_doctype.equals("PO")) {
                    p_source_header_id = pg.getmPOFld().getmPOHeaderId();
            }
            if (p_doctype.equals("REQ")) {
                    p_source_header_id = pg.getmReqFld().getmShipmentHeaderId();
            }

            if (p_doctype.equals("RMA")) {
                    p_source_header_id = pg.getmRMAFld().getmRMAHeaderId();
            }
            
            Integer l_qty = 0;
            l_qty = Integer.parseInt(pg.getmQty().getValue());
            FileLogger.getSystemLogger().trace(gPackage + "." + gCallFrom + " " + gMethod + " " + gCallPoint + " l_qty " + l_qty);
            
            Integer l_running_qty = 0;
            l_running_qty = Integer.parseInt(pg.getmRunningQty().getValue());
            FileLogger.getSystemLogger().trace(gPackage + "." + gCallFrom + " " + gMethod + " " + gCallPoint + " l_running_qty " + l_running_qty);
            
            
            Integer l_total_qty = 0;
            l_total_qty = l_qty + l_running_qty;
            FileLogger.getSystemLogger().trace(gPackage + "." + gCallFrom + " " + gMethod + " " + gCallPoint + " l_total_qty " + l_total_qty);
            
            CallableStatement cstmt = null;
            Connection con = ses.getConnection();                           //1 2 3 4 5 6 7 8 
            cstmt = con.prepareCall("{call XXWC_RCV_MOB_PKG.GET_RCV_TOLERANCE(?,?,?,?,?,?,?,?)}");
            cstmt.setString(1, p_organization_id);
            cstmt.setString(2, pg.getmItemFld().getmInventoryItemId());
            cstmt.setString(3, p_source_header_id);
            cstmt.setString(4, pg.getmTransactionType().getValue());
            cstmt.setString(5, pg.getmDocType().getValue());
            cstmt.setInt(6, l_total_qty);
            cstmt.registerOutParameter(7, Types.INTEGER); //x_return
            cstmt.registerOutParameter(8, Types.VARCHAR); //x_message
            cstmt.execute();
            Integer x_return = cstmt.getInt(7);
            String  x_message = cstmt.getString(8);
            cstmt.close();
            boolean showSubError = true;
            if (x_return > 0) {
                if (x_return == 1) {
                        TelnetSession telnetsessionX = (TelnetSession)ses;
                        Integer k =
                            telnetsessionX.showPromptPage("Warning!", x_message ,
                                                          dialogPageButtons);
                        if (k == 0) {
                            return;
                        } else {
                            return;
                        }
                }
                else if (x_return == 2) {
                        TelnetSession telnetsessionX = (TelnetSession)ses;
                        Integer k =
                            telnetsessionX.showPromptPage("Reject!", x_message ,
                                                          dialogPageButtons);
                        if (k == 0) {
                            pg.getmQty().setValue(null);
                            ses.setNextFieldName("XXWC.QTY");
                        } else {
                            return;
                        }
                }
            }
        }
        catch (Exception e) {
            UtilFns.error("Error in validating Quantity" + e);
            }
        
    }
    
    }


 

    /*************************************************************************
    *   NAME: public void exitedCancel(MWAEvent mwaevent, Session ses) throws AbortHandlerException, InterruptedHandlerException,
                                                     DefaultOnlyHandlerException
    *
    *   PURPOSE:  Exited Close For Field Listener Process to proces the Close Button
    *
    *   REVISIONS:
    *   Ver        Date        Author                     Description
    *   ---------  ----------  ---------------         -------------------------
            1.0       09-FEB-2015   Lee Spitzer             Initial Version - 
                                                               TMS Ticket 20150302-00152 RF - Receiving
    **************************************************************************/

    public void exitedCancel(MWAEvent mwaevent, Session ses) throws AbortHandlerException, InterruptedHandlerException,
                                                                   DefaultOnlyHandlerException {
        String gMethod = "exitedCancel";
        String gCallPoint = "Start";
        FileLogger.getSystemLogger().trace(gPackage + "." + gCallFrom + " " + gMethod + " " + gCallPoint);
        try {
            
            gCallPoint = "Starting rollback";
            FileLogger.getSystemLogger().trace(gPackage + "." + gCallFrom + " " + gMethod + " " + gCallPoint);
            CallableStatement cstmt = null;
            Connection con = ses.getConnection();
            cstmt = con.prepareCall("{call XXWC_RCV_MOB_PKG.EXECUTE_ROLLBACK}");
            cstmt.execute();
            cstmt.close();
            FileLogger.getSystemLogger().trace(gPackage + "." + gCallFrom + " " + gMethod + " " + gCallPoint);
            XXWCRcvGenPage _tmp = pg;
            ses.setStatusMessage(pg.WMS_TXN_CANCEL);
            ses.clearAllApplicationScopeObjects();
            ses.setStatusMessage(XXWCRcvGenPage.WMS_TXN_CANCEL);
            pg.getmCancel().setNextPageName("|END_OF_TRANSACTION|");
            FileLogger.getSystemLogger().trace(gPackage + "." + gCallFrom + " " + gMethod + " " + gCallPoint);
        } catch (Exception e) {
            FileLogger.getSystemLogger().trace(gPackage + "." + gCallFrom + " " + gMethod + " " + gCallPoint + "Error in calling exitedCancel " + e);
            UtilFns.error("Error in calling exitedCancel " + e);
        }

    }
    

    /*************************************************************************
    *   NAME:     public void exitedSave(MWAEvent mwaevent, Session ses) throws AbortHandlerException,
                                                                                 InterruptedHandlerException,
                                                                                 DefaultOnlyHandlerException
    *
    *   PURPOSE:  Clears the values in the form
    *
    *   REVISIONS:
    *   Ver        Date        Author                     Description
    *   ---------  ----------  ---------------         -------------------------
        1.0       09-FEB-2015   Lee Spitzer             Initial Version - 
                                                                TMS Ticket 20150302-00152 RF - Receiving

    **************************************************************************/
    
    protected void exitedSave(MWAEvent mwaevent, Session ses) throws AbortHandlerException, InterruptedHandlerException,
                                                        DefaultOnlyHandlerException {
        try {
                XXWCRcvGenPage _tmp = pg;
                //ses.setStatusMessage(pg.WMS_TXN_SUCCESS);
                pg.getmItemFld().setValue("");
                pg.getmItemFld().setRequired(true);
                pg.getmItemDescriptionFld().setValue("");
                pg.getmUOMFld().setValue("");
                pg.getmQty().setValue("");
                pg.mRunningQtyFld.setValue("");
                //pg.getmSubFld().setValue("General"); //Removed 5/18/2015
                pg.getmSubFld().setValue(""); //Added 5/18/2015
                pg.getmInternalRTVFld().setValue("");
                pg.getmPOFld().setEditable(false);
                pg.getmReqFld().setEditable(false);
                pg.getmRMAFld().setEditable(false);
                pg.getmPOAcceptanceDeferReasonsFld().setEditable(false);
                /*removed 5/18/2015
                pg.getmItemDescriptionFld().setHidden(true);
                pg.getmUOMFld().setHidden(true);
                pg.getmQty().setHidden(true);
                pg.mRunningQtyFld.setHidden(true);
                pg.getmSubFld().setHidden(true);
                pg.getmInternalRTVFld().setHidden(true);
                pg.getmGroupId().setHidden(true);
                pg.getmPrintItemLabel().setHidden(true);
                pg.getmSaveNext().setHidden(true);
                pg.getmDone().setHidden(true);
                //pg.getmCancel().setHidden(false);
                */
                pg.getmItemDescriptionFld().setHidden(false);
                pg.getmUOMFld().setHidden(false);
                pg.getmQty().setHidden(false);
                pg.mRunningQtyFld.setHidden(false);
                pg.getmSubFld().setHidden(false);
                pg.getmSubFld().setEditable(false);
                pg.getmQty().setEditable(false);
                
            } 
        catch (Exception e) {
            UtilFns.error("Error in calling exitedDone " + e);
        }
    
    }
    
    /*************************************************************************
    *   NAME:     public void exitedDone(MWAEvent mwaevent, Session ses) throws AbortHandlerException,
                                                                                 InterruptedHandlerException,
                                                                                 DefaultOnlyHandlerException
    *
    *   PURPOSE:  Clears the values in the form
    *
    *   REVISIONS:
    *   Ver        Date        Author                     Description
    *   ---------  ----------  ---------------         -------------------------
        1.0       09-FEB-2015   Lee Spitzer             Initial Version - 
                                                                TMS Ticket 20150302-00152 RF - Receiving

    **************************************************************************/
    
    protected void exitedDone(MWAEvent mwaevent, Session ses) throws AbortHandlerException, InterruptedHandlerException,
                                                        DefaultOnlyHandlerException {
        try {
                String gCallMethod = "exitedDone";
                String gCallPoint = "Start";
                FileLogger.getSystemLogger().trace(gPackage + " " + gCallFrom + " " + gCallPoint);
                XXWCRcvGenPage _tmp = pg;
                
                
                //ses.setStatusMessage(pg.WMS_TXN_SUCCESS);
                ses.putSessionObject("sessXXWC.GROUP_ID", pg.getmGroupId().getValue());
                ses.putSessionObject("sessXXWC.DOC_TYPE",pg.getmDocType().getValue());
                ses.putSessionObject("sessXXWC.TRANSACTION_TYPE",pg.getmTransactionType().getValue());
                
                FileLogger.getSystemLogger().trace(gPackage + " " + gCallFrom + " " + gCallPoint + " sessXXWC.GROUP_ID " + (String)ses.getObject("sessXXWC.GROUP_ID"));
                FileLogger.getSystemLogger().trace(gPackage + " " + gCallFrom + " " + gCallPoint + " sessXXWC.DOC_TYPE " + (String)ses.getObject("sessXXWC.DOC_TYPE"));
                FileLogger.getSystemLogger().trace(gPackage + " " + gCallFrom + " " + gCallPoint + " sessXXWC.TRANSACTION_TYPE  "+ (String)ses.getObject("sessXXWC.TRANSACTION_TYPE"));
            
                pg.getmDone().setNextPageName("xxwc.oracle.apps.inv.rcv.server.XXWCRcptInfoPage");
                ses.setNextFieldName("XXWC.CARRIER");
        }
        catch (Exception e) {
            UtilFns.error("Error in calling exitedDone " + e);
        }
    
    }

    /*************************************************************************
    *   NAME:     public void checkItemIsTSString organizationId, String inventoryItemId, Session ses) throws AbortHandlerException, InterruptedHandlerException,
                                                                                DefaultOnlyHandlerException
    *
    *   PURPOSE:  Clears the values in the form
    *
    *   REVISIONS:
    *   Ver        Date        Author                     Description
    *   ---------  ----------  ---------------         -------------------------
        1.0       09-FEB-2015   Lee Spitzer             Initial Version - 
                                                                TMS Ticket 20150302-00152 RF - Receiving

    **************************************************************************/
    
    public String checkItemIsTS (String organizationId, String inventoryItemId, Session ses) throws AbortHandlerException, InterruptedHandlerException,
                                                                                DefaultOnlyHandlerException{
        //Going to check if item is time sensitive
        
        String gMethod = "checkItemIsTS";
        String gCallPoint = "Start";
        String x_return = "N";
        
        try {
            gCallPoint = "Calling XXWC_MWA_ROUTINES_PKG.CHECK_ITEM_IS_TS";
            CallableStatement cstmt = null;
            Connection con = ses.getConnection();
            //                                                                        1 2 
            cstmt = con.prepareCall("{? = call XXWC_MWA_ROUTINES_PKG.CHECK_ITEM_IS_TS(?,?)}");
            cstmt.registerOutParameter(1, Types.VARCHAR);
            cstmt.setString(2, organizationId);
            cstmt.setString(3, inventoryItemId);
            FileLogger.getSystemLogger().trace(gPackage + " " + gCallFrom + " " + gCallPoint + " cstm " + cstmt.toString());
            cstmt.execute();
            x_return = cstmt.getString(1);
            FileLogger.getSystemLogger().trace(gPackage + " " + gCallFrom + " " + gCallPoint + " return value" + x_return);
            cstmt.close();
        }
        catch (Exception e){
            UtilFns.error("Error in calling default checkItemIsTS " + e);
        }
        
        return x_return;

    }
    
    
    /*************************************************************************
    *   NAME:     public void getGroupId throws AbortHandlerException, InterruptedHandlerException,
                                                                       DefaultOnlyHandlerException
    *
    *   PURPOSE:  Clears the values in the form
    *
    *   REVISIONS:
    *   Ver        Date        Author                     Description
    *   ---------  ----------  ---------------         -------------------------
        1.0       09-FEB-2015   Lee Spitzer             Initial Version - 
                                                                TMS Ticket 20150302-00152 RF - Receiving

    **************************************************************************/
    
    public void getGroupId (MWAEvent mwaevent, Session ses)  throws AbortHandlerException, InterruptedHandlerException,
                                                                                DefaultOnlyHandlerException{
        //Going to check if item is time sensitive
        
        String gMethod = "getGroupId";
        String gCallPoint = "Start";
        String x_return = "-1";
        
        try {
            if (pg.getmGroupId().getValue().equals(null) || pg.getmGroupId().getValue().equals("")) {
                try {
                    CallableStatement cstmt = null;
                    Connection con = ses.getConnection();
                    cstmt = con.prepareCall("{? = call XXWC_RCV_MOB_PKG.get_next_group_id}");
                    cstmt.registerOutParameter(1, Types.VARCHAR);
                    cstmt.execute();
                    x_return = cstmt.getString(1);
                    cstmt.close();
                }
                catch (Exception e){
                    FileLogger.getSystemLogger().trace(gPackage + "." + gCallFrom + " " + gMethod + " " + gCallPoint + " " + e);
                    x_return = "-1";
                }
                pg.getmGroupId().setValue(x_return);
                pg.getmGroupId().setHidden(true);
            }
            else {
                x_return = pg.getmGroupId().getValue();
                pg.getmGroupId().setHidden(true);
            }
        }
        catch (Exception e){
            FileLogger.getSystemLogger().trace(gPackage + "." + gCallFrom + " " + gMethod + " " + gCallPoint + " " + e);
            x_return = "-1";
        }
        
        ses.putSessionObject("sessXXWC.GROUP_ID",x_return);
        
    }


  
  
    /*************************************************************************
    *   NAME: public void execInsertIntoRcvInterface(MWAEvent mwaevent, Session ses) throws AbortHandlerException, InterruptedHandlerException,
                                                    DefaultOnlyHandlerException
    *
    *   PURPOSE:  Exited Subinventory Field Listener Process to determine if the Locator Field should be displayed
    *
    *   REVISIONS:
    *   Ver        Date        Author                     Description
    *   ---------  ----------  ---------------         -------------------------
            1.0       09-FEB-2015   Lee Spitzer             Initial Version - 
                                                                 TMS Ticket 20150302-00152 RF - Receiving
    **************************************************************************/

    public void execInsertIntoRcvInterface(MWAEvent mwaevent, Session ses) throws AbortHandlerException, InterruptedHandlerException,
                                                                    DefaultOnlyHandlerException {
        
        String gMethod = "execInsertIntoRcvInterface";
        String gCallPoint = "Start";
        String p_group_id;
        String p_organization_id;
        String p_inventory_item_id;
        String p_item_description;
        String p_uom_code;
        String p_quantity;
        String p_source_header_id = "";
        String p_subinventory_code;
        String p_locator_id;
        String p_project_id;
        String p_task_id;
        String p_transaction_type;
        String p_doctype;
        String p_suppress_traveler;
        String p_user_id;
        String p_vendor_id;
        String p_vendor_site_id;
        Integer x_return;
        String x_message;
        
        p_group_id = pg.getmGroupId().getValue();
        p_organization_id = (String)ses.getObject("ORGID");
        p_inventory_item_id = pg.getmItemFld().getmInventoryItemId();
        p_item_description = pg.getmItemDescriptionFld().getValue();
        p_uom_code = pg.getmUOMFld().getValue();
        p_quantity = pg.getmQty().getValue();
        p_subinventory_code = pg.getmSubFld().getValue();
        p_locator_id = "";
        p_project_id = "";
        p_task_id = "";
        p_transaction_type = pg.getmTransactionType().getValue();
        p_doctype = pg.getmDocType().getValue();
        p_suppress_traveler = "N";
        p_user_id = (String)ses.getObject("USERID");
        p_vendor_id = pg.getmPOFld().getmVendorId();
        p_vendor_site_id = pg.getmPOFld().getmVendorSiteId();
        x_return = -1;
        x_message = "";
        
    
        if (p_doctype.equals("PO")) {
                p_source_header_id = pg.getmPOFld().getmPOHeaderId();
        }
        if (p_doctype.equals("REQ")) {
                p_source_header_id = pg.getmReqFld().getmShipmentHeaderId();
        }

        if (p_doctype.equals("RMA")) {
                    p_source_header_id = pg.getmRMAFld().getmRMAHeaderId();
        }
    
          try {
                CallableStatement cstmt = null;
                Connection con = ses.getConnection();
                //                                                                        1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9     
                cstmt = con.prepareCall("{call XXWC_RCV_MOB_PKG.INSERT_INTO_RCV_INTERFACE(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)}");
                cstmt.setString(1, p_group_id);
                cstmt.setString(2, p_organization_id);
                cstmt.setString(3, p_inventory_item_id);
                cstmt.setString(4, p_item_description);
                cstmt.setString(5, p_uom_code);
                cstmt.setString(6, p_quantity);
                cstmt.setString(7, p_source_header_id);
                cstmt.setString(8, p_subinventory_code);
                cstmt.setString(9, p_locator_id);
                cstmt.setString(10, p_project_id);
                cstmt.setString(11, p_task_id);
                cstmt.setString(12, p_transaction_type);
                cstmt.setString(13, p_doctype);
                cstmt.setString(14, p_suppress_traveler);
                cstmt.setString(15, p_user_id);
                cstmt.setString(16, p_vendor_id);
                cstmt.setString(17, p_vendor_site_id);
                cstmt.registerOutParameter(18, Types.INTEGER); //x_return
                cstmt.registerOutParameter(19, Types.VARCHAR); //x_message
                cstmt.execute();
                x_return = cstmt.getInt(18);
                x_message = cstmt.getString(19);
                cstmt.close();
                boolean showSubError = true;
                if (x_return > 1) {
                    TelnetSession telnetsessionX = (TelnetSession)ses;
                    Integer k =
                        telnetsessionX.showPromptPage("Error!", x_message,
                                                      dialogPageButtons);
                    if (k == 0) {
                        pg.getmQty().setValue(null);
                        if (pg.getmQty().isHidden()) {
                            ses.setNextFieldName("XXWC.INV");
                        }
                        else {
                            ses.setNextFieldName("XXWC.QTY");
                        }
                            
                    } else {
                        return;
                    }
                }
            }
            catch (Exception e) {
                UtilFns.error("Error in validating Quantity" + e);
            }
            
          g_return = x_return;
          g_message = x_message;
          ses.setStatusMessage(x_message);
        }
        
        
    /*************************************************************************
    *   NAME:     public void getDefaultCarrier throws AbortHandlerException, InterruptedHandlerException,
                                                                       DefaultOnlyHandlerException
    *
    *   PURPOSE:  Clears the values in the form
    *
    *   REVISIONS:
    *   Ver        Date        Author                     Description
    *   ---------  ----------  ---------------         -------------------------
        1.0       09-FEB-2015   Lee Spitzer             Initial Version - 
                                                                TMS Ticket 20150302-00152 RF - Receiving

    **************************************************************************/
    
    public void getDefaultCarrier (MWAEvent mwaevent, Session ses)  throws AbortHandlerException, InterruptedHandlerException,
                                                                                DefaultOnlyHandlerException{
        //Going to check if item is time sensitive
        
        String gMethod = "getDefaultCarrier";
        String gCallPoint = "Start";
        String x_return = "";
        String p_source_header_id = "";
        String p_transaction_type;
        String p_doctype;
        
        p_transaction_type = pg.getmTransactionType().getValue();
        p_doctype = pg.getmDocType().getValue();

        if (p_doctype.equals("PO")) {
            p_source_header_id = pg.getmPOFld().getmPOHeaderId();
        }
        
        if (p_doctype.equals("REQ")) {
            p_source_header_id = pg.getmReqFld().getmShipmentHeaderId();
        }
        if (p_doctype.equals("RMA")) {
            p_source_header_id = pg.getmRMAFld().getmRMAHeaderId();
        }
        
        try {
            if (!p_source_header_id.equals(null) || !p_source_header_id.equals("")) {
                try {
                    CallableStatement cstmt = null;
                    Connection con = ses.getConnection();
                    cstmt = con.prepareCall("{? = call XXWC_RCV_MOB_PKG.get_default_carrier(?,?,?)}");
                    cstmt.registerOutParameter(1, Types.VARCHAR);
                    cstmt.setString(2, p_transaction_type);
                    cstmt.setString(3, p_doctype);
                    cstmt.setString(4, p_source_header_id);
                    cstmt.execute();
                    x_return = cstmt.getString(1);
                    cstmt.close();
                }
                catch (Exception e){
                    FileLogger.getSystemLogger().trace(gPackage + "." + gCallFrom + " " + gMethod + " " + gCallPoint + " " + e);
                    x_return = "-1";
                }
                ses.putSessionObject("sessXXWC.CARRIER",x_return);
            }
            else {
                ses.putSessionObject("sessXXWC.CARRIER","");
             }
        }
        catch (Exception e){
            FileLogger.getSystemLogger().trace(gPackage + "." + gCallFrom + " " + gMethod + " " + gCallPoint + " " + e);
            x_return = "";
        }
        
    }


    /*************************************************************************
    *   NAME: public void exitedPrintItemLabel(MWAEvent mwaevent, Session ses) throws AbortHandlerException, InterruptedHandlerException,
                                                    DefaultOnlyHandlerException
    *
    *   PURPOSE:  Exited Subinventory Field Listener Process to determine if the Locator Field should be displayed
    *
    *   REVISIONS:
    *   Ver        Date        Author                     Description
    *   ---------  ----------  ---------------         -------------------------
            1.0       09-FEB-2015   Lee Spitzer             Initial Version - 
                                                                 TMS Ticket 20150302-00152 RF - Receiving
    **************************************************************************/

    public void exitedPrintItemLabel(MWAEvent mwaevent, Session ses) throws AbortHandlerException, InterruptedHandlerException,
                                                                    DefaultOnlyHandlerException {
        
        try{
            String l_current_page = ses.getCurrentPageName();
            ses.putSessionObject("sessPreviousPage", l_current_page);
            ses.putSessionObject("sessPreviousField", "INV.SAVENEXT");
            ses.putSessionObject("sessXXWC.ITEM", pg.getmItemFld().getmItemNumber());
            ses.putSessionObject("sessXXWC.BIN", "");
            ses.putSessionObject("sessXXWC.PRICE", "");
            ses.putSessionObject("sessXXWC.ITEM_STATUS","4X1");
            ses.putSessionObject("sessXXWC.PRINT_LOCATION", "No");
            ses.putSessionObject("sessXXWC.SHELF_PRICE","");
            ses.putSessionObject("sessXXWC.COPIES", "1");
            pg.getmPrintItemLabel().setNextPageName("xxwc.oracle.apps.inv.labels.server.XXWCItemLabelPage");
            ses.setNextFieldName("XXWC.ITEM");
            }
            catch (Exception e) {
                UtilFns.error("Error in validating Quantity" + e);
                     ses.putSessionObject("sessXXWC.ITEM", "");
                     ses.putSessionObject("sessXXWC.BIN", "");
                     ses.putSessionObject("sessXXWC.PRICE", "");
                     ses.putSessionObject("sessXXWC.ITEM_STATUS","");
                     ses.putSessionObject("sessXXWC.PRINT_LOCATION", "");
                     ses.putSessionObject("sessXXWC.SHELF_PRICE","");
                     ses.putSessionObject("sessXXWC.COPIES", "");
            }
        }
    
    /*************************************************************************
         *   NAME: public void execRTVDFFUpdate(MWAEvent mwaevent, Session ses) throws AbortHandlerException,
                                                                      InterruptedHandlerException,
                                                                      DefaultOnlyHandlerException 
    
         *   PURPOSE: Retrieves page values and executes the print material label API, xxwc_mwa_routines_pkg.material_label
         *    
         *   In: MWAEvent mwaevent
         *    
         *    
         *   REVISIONS:
         *   Ver        Date        Author                     Description
         *   ---------  ----------  ---------------         -------------------------
              1.0       01-NOV-2014   Lee Spitzer             Initial Version - 
    **************************************************************************/
      
    public void execRTVDFFUpdate (MWAEvent mwaevent, Session ses) throws AbortHandlerException,
                                                                      InterruptedHandlerException,
                                                                      DefaultOnlyHandlerException 
    { 
        String gMethod = "execRTVDFFUpdate";
        String gCallPoint = "Start";
         FileLogger.getSystemLogger().trace(gPackage + "." + gCallFrom + " " + gMethod + " " + gCallPoint );
        String p_organization_id = "";
        String p_inventory_item_id = "";
        String p_shipment_header_id = "";
        String p_user_id = "";
        String p_value = "";
        Integer x_return = -1;
        String x_message = "";
        
        p_user_id = (String)ses.getObject("USERID");
        p_organization_id = (String)ses.getObject("ORGID");
        p_inventory_item_id = (String)pg.getmItemFld().getmInventoryItemId();
        p_shipment_header_id = (String)pg.getmReqFld().getmShipmentHeaderId();
        p_value = (String)pg.getmInternalRTVFld().getValue();

    
    
        FileLogger.getSystemLogger().trace(gPackage + "." + gCallFrom + " " + gMethod + " " + gCallPoint + " p_organization_id " + p_organization_id );
        FileLogger.getSystemLogger().trace(gPackage + "." + gCallFrom + " " + gMethod + " " + gCallPoint + " p_inventory_item_id " + p_inventory_item_id );
        FileLogger.getSystemLogger().trace(gPackage + "." + gCallFrom + " " + gMethod + " " + gCallPoint + " p_shipment_header_id " + p_shipment_header_id);
        FileLogger.getSystemLogger().trace(gPackage + "." + gCallFrom + " " + gMethod + " " + gCallPoint + " p_value " + p_value);
        FileLogger.getSystemLogger().trace(gPackage + "." + gCallFrom + " " + gMethod + " " + gCallPoint + " p_user_id " + p_user_id);
        
        
        try {
            gCallPoint = "Calling XXWC_RCV_MOB_PKG.PROCESS_UPDATE_RTV_DFF ";
            CallableStatement cstmt = null;
            Connection con = ses.getConnection();
            //                                                                     1 2 3 4 5 6 7
            cstmt = con.prepareCall("{call XXWC_RCV_MOB_PKG.PROCESS_UPDATE_RTV_DFF(?,?,?,?,?,?,?)}");
            cstmt.setString(1, p_shipment_header_id); //p_shipment_header_id
            cstmt.setString(2, p_inventory_item_id); //p_inventory_item_id
            cstmt.setString(3, p_organization_id); //p_organization_id
            cstmt.setString(4, p_value); //p_value
            cstmt.setString(5, p_user_id); //p_user_id
            cstmt.registerOutParameter(6, Types.INTEGER); //x_result
            cstmt.registerOutParameter(7, Types.VARCHAR); //x_message 
            cstmt.execute();
            x_return = cstmt.getInt(6);
            x_message = cstmt.getString(7);
            cstmt.close();
            FileLogger.getSystemLogger().trace(gPackage + "." + gCallFrom + " " + gMethod + " " + gCallPoint + " x_return " + x_return);
            FileLogger.getSystemLogger().trace(gPackage + "." + gCallFrom + " " + gMethod + " " + gCallPoint + " x_message " + x_message);
            boolean showSubError = true;
                if (x_return == 0) {
                }
                
                if (x_return > 0) {
                    dialogPageButtons = new String[] { "OK" };
                    TelnetSession telnetsessionX = (TelnetSession)ses;
                    int k = telnetsessionX.showPromptPage("Error!", x_message, dialogPageButtons);
                    if (k == 0) {
                        ses.setRefreshScreen(true);
                     }
                 }
        }
        catch (Exception e){
            UtilFns.error("Error in calling default execRTVDFFUpdate " + e);
        }
    
        g_return = x_return;
        g_message = x_message;
    }
    
    /*************************************************************************
    *   NAME: public void clearSessionValues(MWAEvent mwaevent, Session ses) throws AbortHandlerException, InterruptedHandlerException,
                                                    DefaultOnlyHandlerException
    *
    *   PURPOSE:  Exited Subinventory Field Listener Process to determine if the Locator Field should be displayed
    *
    *   REVISIONS:
    *   Ver        Date        Author                     Description
    *   ---------  ----------  ---------------         -------------------------
            1.0       09-FEB-2015   Lee Spitzer             Initial Version - 
                                                                 TMS Ticket 20150302-00152 RF - Receiving
    **************************************************************************/


    public void clearSessionValues(MWAEvent mwaevent, Session ses) throws AbortHandlerException, InterruptedHandlerException,
                                                                    DefaultOnlyHandlerException {
        ses.putSessionObject("sessXXWC.CARRIER", "");
        ses.putSessionObject("sessXXWC.GROUP_ID", "");
        ses.putSessionObject("sessXXWC.DOC_TYPE","");
        ses.putSessionObject("sessXXWC.TRANSACTION_TYPE","");
        ses.putSessionObject("sessXXWC.ACCEPT_DEFER",""); 
        ses.putSessionObject("sessPreviousPage", "");
        ses.putSessionObject("sessPreviousField", "");
        ses.putSessionObject("sessXXWC.SHELF_LIFE", "");
        ses.putSessionObject("sessXXWC.COPIES","");
        ses.putSessionObject("sessXXWC.UBD_STATUS", "");
        ses.putSessionObject("sessPOHeaderId", "");  
        ses.putSessionObject("sessReqHeaderId", "");
        ses.putSessionObject("sessShipmentHeaderId", "");
        ses.putSessionObject("sessRMAHeaderId", "");
        ses.putSessionObject("sessShipFromOrgId", "");
    }
    
    /*************************************************************************
    *   NAME: public void enteredAcceptanceDefer(MWAEvent mwaevent) throws AbortHandlerException, InterruptedHandlerException,
                                                     DefaultOnlyHandlerException
    *
    *   PURPOSE:  Entered Item Field Listener Process to set the List of Values for the Subinventory Field
    *
    *   REVISIONS:
    *   Ver        Date        Author                     Description
    *   ---------  ----------  ---------------         -------------------------
             1.0       09-FEB-2015   Lee Spitzer             Initial Version - 
                                                                  TMS Ticket 20150302-00152 RF - Receiving
    **************************************************************************/

    public void enteredAcceptanceDefer(MWAEvent mwaevent) throws AbortHandlerException, InterruptedHandlerException,
                                                    DefaultOnlyHandlerException {

        try{     
           XXWCPOAcceptanceDeferReasonsLOV  poAcceptDeferLOV = pg.getmPOAcceptanceDeferReasonsFld();
           poAcceptDeferLOV.setValidateFromLOV(true);
           poAcceptDeferLOV.setlovStatement("XXWC_RCV_MOB_PKG.GET_PO_DEFER_ACCEPTANCE_LOV");
           String paramType[] = { "C", "S" };
           String parameters[] =
           { " ", "xxwc.oracle.apps.inv.rcv.server.XXWCRcvGenPage.XXWC.ACCEPT_DEFER" };
           poAcceptDeferLOV.setInputParameterTypes(paramType);
           poAcceptDeferLOV.setInputParameters(parameters);
       } catch (Exception e) {
           UtilFns.error("Error in calling enteredCarrier " + e);
       }
    }


    /*************************************************************************
    *   NAME: public void exitedAcceptanceDefer(MWAEvent mwaevent, Session ses) throws AbortHandlerException, InterruptedHandlerException,
                                                    DefaultOnlyHandlerException
    *
    *   PURPOSE:  Exited Acceptance Defer Listener Process to determine if the Locator Field should be displayed
    *
    *   REVISIONS:
    *   Ver        Date        Author                     Description
    *   ---------  ----------  ---------------         -------------------------
            1.0       09-FEB-2015   Lee Spitzer             Initial Version - 
                                                                 TMS Ticket 20150302-00152 RF - Receiving
    **************************************************************************/

    public void exitedAcceptanceDefer(MWAEvent mwaevent, Session ses) throws AbortHandlerException, InterruptedHandlerException,
                                                                DefaultOnlyHandlerException {
       try {
           String l_po_acceptance = pg.getmPOAcceptanceDeferReasonsFld().getmLookupCode();
           ses.putSessionObject("sessXXWC.ACCEPT_DEFER", l_po_acceptance);
       } catch (Exception e) {
           UtilFns.error("Error in calling exitedAcceptanceDefer" + e);
       }
    }


    /*************************************************************************
    *   NAME:   public void execPrintExpectedReceipts(MWAEvent mwaevent, Session ses) throws AbortHandlerException,
                                                                                             InterruptedHandlerException,
                                                                                             DefaultOnlyHandlerException 
    
    *   PURPOSE: Retrieves page values and executes the print material label API, xxwc_mwa_routines_pkg.material_label
    *    
    *   In: MWAEvent mwaevent
    *    
    *    
    *   REVISIONS:
    *   Ver        Date        Author                     Description
    *   ---------  ----------  ---------------         -------------------------
           1.0       06-FEB-2015   Lee Spitzer             Initial Version - 
    **************************************************************************/
      
    public String execPrintExpectedReceipts(MWAEvent mwaevent, Session ses) throws AbortHandlerException,
                                                                      InterruptedHandlerException,
                                                                      DefaultOnlyHandlerException 
    { 
        String gMethod = "execPrintExpectedReceipts";
        String gCallPoint = "Start";
        FileLogger.getSystemLogger().trace(gPackage + "." + gCallFrom + " " + gMethod + " " + gCallPoint );
        
        TelnetSession telnetsessionX = (TelnetSession)ses;
        int k = telnetsessionX.showPromptPage("Print Expected Receipts", "Print expected receipts report?", new String [] {"Yes", "No"});
        if (k == 0) {
             
        
            String p_organization_id = "";
            String p_po_number = "";
            String p_req_number = "";
            String p_vendor = "";
            String p_structure_id = "";
            String p_rma_number = "";
            Integer x_return = null;
            String x_message = "";
                    
            gCallPoint = "Getting values from page";   
            
            p_organization_id = (String)ses.getObject("ORGID");
            p_po_number = pg.getmPOFld().getmPONumber();
            p_req_number = pg.getmReqFld().getmREQNumber();
            p_rma_number = pg.getmRMAFld().getmRMANumber();
            p_vendor = pg.getmPOFld().getmVendorName();
            p_structure_id = "101";
            
            
            FileLogger.getSystemLogger().trace(gPackage + "." + gCallFrom + " " + gMethod + " " + gCallPoint + " p_organization_id " + p_organization_id);
            FileLogger.getSystemLogger().trace(gPackage + "." + gCallFrom + " " + gMethod + " " + gCallPoint + " p_po_number " + p_po_number);
            FileLogger.getSystemLogger().trace(gPackage + "." + gCallFrom + " " + gMethod + " " + gCallPoint + " p_req_number " + p_req_number);
            FileLogger.getSystemLogger().trace(gPackage + "." + gCallFrom + " " + gMethod + " " + gCallPoint + " p_rma_number " + p_rma_number);
            FileLogger.getSystemLogger().trace(gPackage + "." + gCallFrom + " " + gMethod + " " + gCallPoint + " p_vendor " + p_vendor);
            FileLogger.getSystemLogger().trace(gPackage + "." + gCallFrom + " " + gMethod + " " + gCallPoint + " p_structure_id " + p_structure_id);
            
            
            try {
                gCallPoint = "Calling XXWC_LABEL_PRINTERS_PKG.PROCESS_EXPECTED_RECEIPTS_CCR";
                CallableStatement cstmt = null;
                Connection con = ses.getConnection();
                //                                                                                   1 2 3 4 5 6 7 8 
                cstmt = con.prepareCall("{call XXWC_LABEL_PRINTERS_PKG.PROCESS_EXPECTED_RECEIPTS_CCR(?,?,?,?,?,?,?,?)}");
                cstmt.setString(1, p_organization_id); //p_organization_id
                cstmt.setString(2, p_po_number); //p_po_number
                cstmt.setString(3, p_req_number); //p_req_number
                cstmt.setString(4, p_rma_number); //p_rma_number
                cstmt.setString(5, p_vendor); //p_vendor
                cstmt.setString(6, p_structure_id); //p_structure_id
                cstmt.registerOutParameter(7, Types.INTEGER); //x_result
                cstmt.registerOutParameter(8, Types.VARCHAR); //x_message 
                cstmt.execute();
                x_return = cstmt.getInt(7);
                x_message = cstmt.getString(8);
                cstmt.close();
                FileLogger.getSystemLogger().trace(gPackage + "." + gCallFrom + " " + gMethod + " " + gCallPoint + " x_return " + x_return);
                FileLogger.getSystemLogger().trace(gPackage + "." + gCallFrom + " " + gMethod + " " + gCallPoint + " x_message " + x_message);
                boolean showSubError = true;
                    if (x_return == 0) {
                            ses.setStatusMessage(x_message);
                    }
                    if (x_return > 0) {
                        dialogPageButtons = new String[] { "OK" };
                        k = telnetsessionX.showPromptPage("Error!", x_message, dialogPageButtons);
                        if (k == 0) {
                             return x_message;
                         } 
                    }
            }
            catch (Exception e){
                UtilFns.error("Error in calling default execPrintExpectedReceipts " + e);
            }
        
            return x_message;
        }
        if (k == 1) {
            
            return "";
        }
        else {
            return "";
        }
            
    }
    

    /*************************************************************************
    *   NAME:   public execDeleteGroupId(MWAEvent mwaevent, Session ses) throws AbortHandlerException,
                                                                                             InterruptedHandlerException,
                                                                                             DefaultOnlyHandlerException 
    
    *   PURPOSE: Retrieves page values and executes the print material label API, xxwc_mwa_routines_pkg.material_label
    *    
    *   In: MWAEvent mwaevent
    *    
    *    
    *   REVISIONS:
    *   Ver        Date        Author                     Description
    *   ---------  ----------  ---------------         -------------------------
           1.0       06-FEB-2015   Lee Spitzer             Initial Version - 
    **************************************************************************/
      
    public void execDeleteGroupId(MWAEvent mwaevent, Session ses) throws AbortHandlerException,
                                                                      InterruptedHandlerException,
                                                                      DefaultOnlyHandlerException 
    { 
        String gMethod = "execDeleteGroupId";
        String gCallPoint = "Start";
        FileLogger.getSystemLogger().trace(gPackage + "." + gCallFrom + " " + gMethod + " " + gCallPoint );
        
        String p_group_id = "";
        Integer x_return = null;
        String x_message = "";
                    
        gCallPoint = "Getting values from page";   
            
        p_group_id = pg.getmGroupId().getValue();
            
        FileLogger.getSystemLogger().trace(gPackage + "." + gCallFrom + " " + gMethod + " " + gCallPoint + " p_group_id " + p_group_id);
            
            
            try {
                gCallPoint = "Calling XXWC_RCV_MOB_PKG.DELETE_RCV_IFACE";
                CallableStatement cstmt = null;
                Connection con = ses.getConnection();
                //                                                               1 2 3
                cstmt = con.prepareCall("{call XXWC_RCV_MOB_PKG.DELETE_RCV_IFACE(?,?,?)}");
                cstmt.setString(1, p_group_id); //p_group_id
                cstmt.registerOutParameter(2, Types.INTEGER); //x_result
                cstmt.registerOutParameter(3, Types.VARCHAR); //x_message 
                cstmt.execute();
                x_return = cstmt.getInt(2);
                x_message = cstmt.getString(3);
                cstmt.close();
                FileLogger.getSystemLogger().trace(gPackage + "." + gCallFrom + " " + gMethod + " " + gCallPoint + " x_return " + x_return);
                FileLogger.getSystemLogger().trace(gPackage + "." + gCallFrom + " " + gMethod + " " + gCallPoint + " x_message " + x_message);
                boolean showSubError = true;
                    if (x_return > 0) {
                        dialogPageButtons = new String[] { "OK" };
                        TelnetSession telnetsessionX = (TelnetSession)ses;
                        int k = telnetsessionX.showPromptPage("Error!", x_message, dialogPageButtons);
                        if (k == 0) {
                         } 
                    }
            }
            catch (Exception e){
                UtilFns.error("Error in calling default execDeleteGroupId " + e);
            }
            
    }
    
    /*************************************************************************
    *   NAME: public void checkStuckGroupId (MWAEvent mwaevent, Session ses) throws AbortHandlerException, InterruptedHandlerException,
                                                    DefaultOnlyHandlerException
    *
    *   PURPOSE:  Exited Subinventory Field Listener Process to determine if the Locator Field should be displayed
    *
    *   REVISIONS:
    *   Ver        Date        Author                     Description
    *   ---------  ----------  ---------------         -------------------------
            1.0      01-JUN-2015   Lee Spitzer             Initial Version - 
                                                            TMS Ticket 20150601-00248 - RF Break/fix - Timeout during receiving causes stuck transactions
    **************************************************************************/

    public void checkStuckGroupId(MWAEvent mwaevent, Session ses) throws AbortHandlerException, InterruptedHandlerException,
                                                                    DefaultOnlyHandlerException {
        
        String gMethod = "checkStuckGroupId";
        String gCallPoint = "Start";
        String p_organization_id;
        String p_source_header_id = "";
        String p_transaction_type;
        String p_doctype;
        String p_user_id;
        Integer x_return;
        String x_message;
        
        p_organization_id = (String)ses.getObject("ORGID");
        p_transaction_type = pg.getmTransactionType().getValue();
        p_doctype = pg.getmDocType().getValue();
        p_user_id = (String)ses.getObject("USERID");
        x_return = -1;
        x_message = "";
        
    
        if (p_doctype.equals("PO")) {
                p_source_header_id = pg.getmPOFld().getmPOHeaderId();
        }
        if (p_doctype.equals("REQ")) {
                p_source_header_id = pg.getmReqFld().getmShipmentHeaderId();
        }

        if (p_doctype.equals("RMA")) {
                    p_source_header_id = pg.getmRMAFld().getmRMAHeaderId();
        }
    
          try {
                CallableStatement cstmt = null;
                Connection con = ses.getConnection();
                //                                                                   1 2 3 4 5 6 7
                cstmt = con.prepareCall("{call XXWC_RCV_MOB_PKG.CHECK_STUCK_GROUP_ID(?,?,?,?,?,?,?)}");
                cstmt.setString(1, p_organization_id);
                cstmt.setString(2, p_source_header_id);
                cstmt.setString(3, p_doctype);
                cstmt.setString(4, p_transaction_type);
                cstmt.setString(5, p_user_id);
                cstmt.registerOutParameter(6, Types.INTEGER); //x_return
                cstmt.registerOutParameter(7, Types.VARCHAR); //x_message
                cstmt.execute();
                x_return = cstmt.getInt(6);
                x_message = cstmt.getString(7);
                cstmt.close();
                boolean showSubError = true;
                if (x_return < 0) {
                    TelnetSession telnetsessionX = (TelnetSession)ses;
                    Integer k =
                        telnetsessionX.showPromptPage("Error!", x_message,
                                                      dialogPageButtons);
                    if (k == 0) {
                        g_cancel = 1;
                        return;
                    } else {
                        return;
                    }
                }
              if (x_return > 0) {
                  TelnetSession telnetsessionX = (TelnetSession)ses;
                  Integer k =
                      telnetsessionX.showPromptPage("Warning!", x_message,
                                                    (new String[] { "Continue" , "Delete" , "Menu" }));
                  if (k == 0) {
                      pg.getmGroupId().setValue(x_return.toString());  
                      return;
                  } 
                  else if (k ==1) {
                      pg.getmGroupId().setValue(x_return.toString()); 
                      execDeleteGroupId(mwaevent, ses);
                      pg.getmGroupId().setValue(""); 
                      return;
                  }
                  else if (k ==2) {
                      g_cancel = 1;
                      clearSessionValues(mwaevent, ses);
                      exitedCancel(mwaevent, ses);
                      return;
                  }
                  else
                       return;
              }
              if (x_return == 0) {
                  pg.getmGroupId().setValue("");
              }
            }
            catch (Exception e) {
                UtilFns.error("Error in validating Quantity" + e);
            }
            
          g_return = x_return;
          g_message = x_message;
        }

}
