/*************************************************************************
   *   $Header XXWCAddUPCPage.java $
   *   Module Name: XXWCAddUPCPage
   *   
   *   Package: package xxwc.oracle.apps.inv.invtxn.server;
   *   
   *   Imports Classes:
   *   
   *   import java.sql.*;
   *   import oracle.apps.inv.utilities.server.UtilFns;
   *   import oracle.apps.mwa.beans.*;
   *   import oracle.apps.mwa.container.MWALib;
   *   import oracle.apps.mwa.container.Session;
   *   import oracle.apps.mwa.eventmodel.*;
   *   import oracle.apps.mwa.container.FileLogger;
   *   import oracle.apps.fnd.flexj.FlexException;
   *   import oracle.apps.inv.lov.server.ItemLOV;
   *   import oracle.apps.inv.lov.server.LocatorKFF;
   *   import oracle.apps.inv.lov.server.SubinventoryLOV;
   *   import xxwc.oracle.apps.inv.lov.server.*;
   *
   *   PURPOSE:   Java Class for XXWC Cycle Count Page Layout.
   *    
   *   REVISIONS:
   *   Ver        Date        Author                     Description
   *   ---------  ----------  ---------------         -------------------------
        1.0       20-JUN-2015   Lee Spitzer             Initial Version - 
                                                            TMS Ticket Number 20150622-00004 RF - UPC Update
        1.1       29-MAR-2016   Lee Spitzer             TMS Ticket Number 20151103-00182 - RF - UI Improvements to reduce number of clicks
**************************************************************************/
package xxwc.oracle.apps.inv.invtxn.server;

import java.sql.*;

import oracle.apps.mwa.beans.*;
import oracle.apps.mwa.container.MWALib;
import oracle.apps.mwa.container.Session;
import oracle.apps.mwa.eventmodel.*;
import oracle.apps.mwa.container.FileLogger;
import oracle.apps.fnd.flexj.FlexException;
import oracle.apps.fnd.flexj.KeyFlexfield;
import oracle.apps.inv.invinq.server.ItemOnhandIterator;
import oracle.apps.inv.lov.server.CycleCountLOV;
import oracle.apps.inv.utilities.server.*;
import oracle.apps.inv.lov.server.ItemLOV;
import oracle.apps.inv.lov.server.LocatorKFF;
import oracle.apps.inv.lov.server.SubinventoryLOV;

import oracle.apps.inv.lov.server.VendorLOV;
import oracle.apps.mwa.presentation.telnet.TelnetSession;

import xxwc.oracle.apps.inv.lov.server.*;


/*************************************************************************
 *   NAME: public class XXWCAddUPCPage extends PageBean
 *
 *   PURPOSE:   Main class for XXWCPrimaryBinAssignmentFunction
 *
 *   REVISIONS:
 *   Ver        Date        Author                     Description
 *   ---------  ----------  ---------------         -------------------------
      1.0       20-JUN-2015   Lee Spitzer             Initial Version -
                                                      TMS Ticket Number 20150622-00004 RF - UPC Update
 **************************************************************************/

public class XXWCAddUPCPage extends PageBean{
    /*Define objects in the class*/
    public static String WMS_TXN_SUCCESS = "Txn Successful";
    public static String WMS_TXN_ERROR = "Txn Failed. Please check log";
    public static String WMS_TXN_CANCEL = "Txn Cancelled";
    public static String WMS_LOT_INS_FAIL = "Lot Insert Failed";
    public static String WMS_SER_INS_FAIL = "Serial Insert Failed";
    public static String saveNextPrompt = "";
    public static String donePrompt = "";
    public static String cancelPrompt = "";
    private boolean areThePromptsSet;
    protected XXWCItemLOV mItemFld;
    TextFieldBean mItemDescFld;
    TextFieldBean mUPCFld;
    protected XXWCVendorLOV mVendorFld; 
    protected ButtonFieldBean mSaveNext;
    protected ButtonFieldBean mDone;
    protected ButtonFieldBean mMenu;
    protected String m_userid;
    /*************************************************************************
     *   NAME: private static String gPackage = "xxwc.oracle.apps.inv.invtxn.server";
     *
     *   PURPOSE:   private method to default gPackage value for debugging
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
            1.0       20-JUN-2015   Lee Spitzer             Initial Version - 
                                                            TMS Ticket Number 20150622-00004 RF - UPC Update
     **************************************************************************/

    private static String gPackage = "xxwc.oracle.apps.inv.invtxn.server";


    /*************************************************************************
     *   NAME: private static String gCallFrom = "XXWCAddUPCPage";
     *
     *   PURPOSE:   private method to pass gCallFrom from methods to return class name CustomListener
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
            1.0       20-JUN-2015   Lee Spitzer             Initial Version - 
                                                            TMS Ticket Number 20150622-00004 RF - UPC Update
     **************************************************************************/

    private static String gCallFrom = "XXWCAddUPCPage";



    
    /*************************************************************************
    *   NAME: public XXWCAddUPCPage(Session session)
    *
    *   PURPOSE:   Main method and calls initLayout Method
    *
    *   REVISIONS:
    *   Ver        Date        Author                     Description
    *   ---------  ----------  ---------------         -------------------------
         1.0       20-JUN-2015   Lee Spitzer             Initial Version - 
                                                         TMS Ticket Number 20150622-00004 RF - UPC Update
     **************************************************************************/

    public XXWCAddUPCPage(Session session) {
        initLayout(session);
    }
    
    /*************************************************************************
    *   NAME: private void initLayout(Session session)
    *
    *   PURPOSE:   Sets the initial layout of the paged
    *
    *   REVISIONS:
    *   Ver        Date        Author                     Description
    *   ---------  ----------  ---------------         -------------------------
         1.0       20-JUN-2015   Lee Spitzer             Initial Version - 
                                                         TMS Ticket Number 20150622-00004 RF - UPC Update
         1.1       29-MAR-2016   Lee Spitzer             TMS Ticket Number 20151103-00182 - RF - UI Improvements to reduce number of clicks
     **************************************************************************/

    private void initLayout(Session session) {
        String gMethod = "setPrompts";
        String gCallPoint = "Start";
        m_userid = "";
        WMS_TXN_SUCCESS = UtilFns.getMessage(session, "WMS", "WMS_TXN_SUCCESS");
        WMS_TXN_ERROR = UtilFns.getMessage(session, "WMS", "WMS_TXN_ERROR");
        WMS_TXN_CANCEL = UtilFns.getMessage(session, "WMS", "WMS_TXN_CANCEL");
        WMS_LOT_INS_FAIL = UtilFns.getMessage(session, "WMS", "WMS_LOT_INS_FAIL");
        WMS_SER_INS_FAIL = UtilFns.getMessage(session, "WMS", "WMS_SER_INS_FAIL");
         /*Item*/        
        mItemFld = new XXWCItemLOV("ALL");
        mItemFld.setName("XXWC.ITEM");
        mItemFld.setPrompt("Item");
        mItemFld.setRequired(true);
        mItemFld.setEditable(true);
        mItemFld.setHidden(false);
        /*Item Desc*/
        mItemDescFld = new TextFieldBean();
        mItemDescFld.setName("XXWC.DESC");
        mItemDescFld.setPrompt("Item Desc");
        mItemDescFld.setEditable(false);
        mItemDescFld.setHidden(true);
        /*UPC Properties*/
        mUPCFld = new TextFieldBean();
        mUPCFld.setName("XXWC.UPC");
        mUPCFld.setPrompt("Barcode");
        mUPCFld.setRequired(true);
        mUPCFld.setHidden(true);
        /*Vendor*/
        mVendorFld = new XXWCVendorLOV();
        mVendorFld.setName("XXWC.VENDOR");
        mVendorFld.setPrompt("Vendor");
        mVendorFld.setRequired(false);
        mVendorFld.setHidden(true);
        /*SaveNext*/
        mSaveNext = new ButtonFieldBean();
        mSaveNext.setName("XXWC.SAVENEXT");
        mSaveNext.setPrompt("Save/Next");
        mSaveNext.setEnableAcceleratorKey(true);
        mSaveNext.setHidden(true);
        /*Next Properties*/
        mDone = new ButtonFieldBean();
        mDone.setName("XXWC.DONE");
        mDone.setPrompt("Done");
        mDone.setEnableAcceleratorKey(true);
        mDone.setHidden(true);
        /*Close Properties*/
        mMenu = new ButtonFieldBean();
        mMenu.setName("XXWC.MENU");
        //mMenu.setPrompt("Menu"); //Removed 03/29/2016 TMS Ticket 20151103-00182
        mMenu.setPrompt("Cancel"); //Added 03/29/2016 TMS Ticket 20151103-00182
        mMenu.setEnableAcceleratorKey(true);
        mMenu.setHidden(false);
        /*set the properties for each to the addFieldBean*/
        addFieldBean(mItemFld);
        addFieldBean(mItemDescFld);
        addFieldBean(mUPCFld);
        addFieldBean(mVendorFld);
        addFieldBean(mSaveNext);
        addFieldBean(mDone);
        addFieldBean(mMenu);
        /*Point Page to the XXWCPrimaryBinAssignmentFListener Listener*/
        XXWCAddUPCFListener fieldListener = new XXWCAddUPCFListener();
        /*Add Listener to each property*/
        mItemFld.addListener(fieldListener);
        mItemDescFld.addListener(fieldListener);
        mUPCFld.addListener(fieldListener);
        mVendorFld.addListener(fieldListener);
        mSaveNext.addListener(fieldListener);
        mDone.addListener(fieldListener);
        mMenu.addListener(fieldListener);
        try {
            setPrompts(session);
        } catch (Exception exception) {
            session.setStatusMessage(UtilFns.getMessage(session, "WMS", "WMS_PROMPT_NOTSET"));
        }
    }
    
    /*************************************************************************
    *   NAME: private void setPrompts(Session session)
    *
    *   PURPOSE:   Sets Prompts for the Page
    *
    *   REVISIONS:
    *   Ver        Date        Author                     Description
    *   ---------  ----------  ---------------         -------------------------
         1.0       20-JUN-2015   Lee Spitzer             Initial Version - 
                                                         TMS Ticket Number 20150622-00004 RF - UPC Update
         1.1       29-MAR-2016   Lee Spitzer             TMS Ticket Number 20151103-00182 - RF - UI Improvements to reduce number of clicks
     **************************************************************************/

    private void setPrompts(Session session) {
        if (!areThePromptsSet) {
            try {
                initPrompts(session);
            } catch (SQLException sqlexception) {
                session.setStatusMessage(UtilFns.getMessage(session, "WMS", "WMS_TXN_ERROR"));
            }
        }
        mSaveNext.setPrompt(saveNextPrompt);
        mDone.setPrompt(donePrompt);
        mMenu.setPrompt(cancelPrompt);
        mSaveNext.retrieveAttributes("INV_SAVE_NEXT_PROMPT");
        mDone.retrieveAttributes("INV_DONE_PROMPT");
        mMenu.retrieveAttributes("INV_CANCEL_PROMPT");
        //mMenu.setPrompt("Menu"); //Removed 03/29/2016 TMS Ticket 20151103-00182
        
    }
   
    /*************************************************************************
    *   NAME: private void initPrompts(Session session) throws SQLException
    *
    *   PURPOSE:   Sets the initial prompts for the page
    *
    *   REVISIONS:
    *   Ver        Date        Author                     Description
    *   ---------  ----------  ---------------         -------------------------
         1.0       20-JUN-2015   Lee Spitzer             Initial Version - 
                                                         TMS Ticket Number 20150622-00004 RF - UPC Update
     **************************************************************************/
 
    private void initPrompts(Session session) throws SQLException {
        try {
            String s = (String)session.getObject("ORGCODE");
            setPrompt((new StringBuilder()).append("XXWC Add Barcode (").append(s).append(")").toString());
            cancelPrompt =
                    MWALib.getAKPrompt(session, "oracle.apps.inv.utilities.InvResourceTable", "INV_CANCEL_PROMPT");
            saveNextPrompt =
                    MWALib.getAKPrompt(session, "oracle.apps.inv.utilities.InvResourceTable", "INV_SAVE_NEXT_PROMPT");
            donePrompt = MWALib.getAKPrompt(session, "oracle.apps.inv.utilities.InvResourceTable", "INV_DONE_PROMPT");
            areThePromptsSet = true;
        } catch (SQLException sqlexception) {
            session.setStatusMessage(UtilFns.getMessage(session, "WMS", "WMS_TXN_ERROR"));
        }
    }


    /*************************************************************************
     *   NAME: public XXWCItemLOV  getmItemFld()
     *
     *   PURPOSE:   return the value of mItemFld
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
        1.0       20-JUN-2015   Lee Spitzer             Initial Version -
                                                                TMS Ticket Number 20150622-00004 RF - UPC Update
     **************************************************************************/

    public XXWCItemLOV getmItemFld() 
    {
        return mItemFld;    
    }


    /*************************************************************************
     *   NAME: public TextFieldBean  getmItemDescFld()
     *
     *   PURPOSE:   return the value of mItemDescFld
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
        1.0       20-JUN-2015   Lee Spitzer             Initial Version -
                                                                TMS Ticket Number 20150622-00004 RF - UPC Update
     **************************************************************************/

    public TextFieldBean getmItemDescFld() 
    {
        return mItemDescFld;    
    }
        
    /*************************************************************************
     *   NAME: public TextFieldBean  getmUPCFld()
     *
     *   PURPOSE:   return the value of mUPCFld
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
        1.0       20-JUN-2015   Lee Spitzer             Initial Version -
                                                                TMS Ticket Number 20150622-00004 RF - UPC Update
     **************************************************************************/

    public TextFieldBean getmUPCFld()
    {
        return mUPCFld;
    }
    
    /*************************************************************************
     *   NAME: public XXWCVendorLOV getmVendorFld()
     *
     *   PURPOSE:   return the value of mVendorFld
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
             1.0       20-JUN-2015   Lee Spitzer             Initial Version -
                                                             TMS Ticket Number 20150622-00004 RF - UPC Update
     **************************************************************************/

    public XXWCVendorLOV getmVendorFld() {
        return mVendorFld;
    }

    /*************************************************************************
    *   NAME: public ButtonFieldBean getmSaveNext()
    *
    *   PURPOSE:   return the value of mSaveNext
    *
    *   REVISIONS:
    *   Ver        Date        Author                     Description
    *   ---------  ----------  ---------------         -------------------------
        1.0       20-JUN-2015   Lee Spitzer             Initial Version -
                                                                    TMS Ticket Number 20150622-00004 RF - UPC Update
    **************************************************************************/

    public ButtonFieldBean getmSaveNext() {
        return mSaveNext;
    }

    /*************************************************************************
    *   NAME: public ButtonFieldBean getmDone()
    *
    *   PURPOSE:   return the value of mDone
    *
    *   REVISIONS:
    *   Ver        Date        Author                     Description
    *   ---------  ----------  ---------------         -------------------------
        1.0       20-JUN-2015   Lee Spitzer             Initial Version -
                                                                    TMS Ticket Number 20150622-00004 RF - UPC Update
    **************************************************************************/

    public ButtonFieldBean getmDone() {
        return mDone;
    }
    
    /*************************************************************************
    *   NAME: public ButtonFieldBean getmMenu()
    *
    *   PURPOSE:   return the value of mMenu
    *
    *   REVISIONS:
    *   Ver        Date        Author                     Description
    *   ---------  ----------  ---------------         -------------------------
        1.0       20-JUN-2015   Lee Spitzer             Initial Version -
                                                                    TMS Ticket Number 20150622-00004 RF - UPC Update
    **************************************************************************/

    public ButtonFieldBean getmMenu() {
        return mMenu;
    }


    /*************************************************************************
     *   NAME: public String getUserId() getUserId()
     *
     *   PURPOSE:   return the value of m_userid
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
        1.0       20-JUN-2015   Lee Spitzer             Initial Version -
                                                                TMS Ticket Number 20150622-00004 RF - UPC Update
     **************************************************************************/

    public String getUserId() {
        return m_userid;
    }

    /*************************************************************************
    *   NAME: void pageEntered(MWAEvent e) throws AbortHandlerException, InterruptedHandlerException,
                                                   DefaultOnlyHandlerException {
    *
    *   PURPOSE:   pageEntered method
    *
    *   Parameters: MWAEvent mwaevent
    *
    *   REVISIONS:
    *   Ver        Date        Author                     Description
    *   ---------  ----------  ---------------         -------------------------
        1.0       20-JUN-2015   Lee Spitzer             Initial Version -
                                                                    TMS Ticket Number 20150622-00004 RF - UPC Update
    **************************************************************************/

    public void pageEntered(MWAEvent e) throws AbortHandlerException, InterruptedHandlerException,
                                               DefaultOnlyHandlerException {
        
        Session session = e.getSession();
        
    }

    /*************************************************************************
    *   NAME: void pageEntered(MWAEvent e) throws AbortHandlerException, InterruptedHandlerException,
                                                       DefaultOnlyHandlerException {
    *
    *   PURPOSE:   pageEntered method
    *
    *   Parameters: MWAEvent mwaevent
    *
    *   REVISIONS:
    *   Ver        Date        Author                     Description
    *   ---------  ----------  ---------------         -------------------------
        1.0       20-JUN-2015   Lee Spitzer             Initial Version -
                                                                        TMS Ticket Number 20150622-00004 RF - UPC Update
    **************************************************************************/

    public void pageExited(MWAEvent e) throws AbortHandlerException, InterruptedHandlerException,
                                               DefaultOnlyHandlerException {
        Session session = e.getSession();
    
    }
    
}
