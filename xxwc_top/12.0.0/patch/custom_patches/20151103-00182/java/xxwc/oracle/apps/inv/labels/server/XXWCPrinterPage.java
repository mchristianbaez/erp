/*************************************************************************
   *   $Header XXWCPrinterPage.java $
   *   Module Name: XXWCPrinterPage
   *   
   *   Package: package xxwc.oracle.apps.inv.TMS Ticket 20150302-00152 - RF Receiving.server;
   *   
   *   Imports Classes:
   *   
   *   import java.sql.*;
   *   import oracle.apps.inv.utilities.server.UtilFns;
   *   import oracle.apps.mwa.beans.*;
   *   import oracle.apps.mwa.container.MWALib;
   *   import oracle.apps.mwa.container.Session;
   *   import oracle.apps.mwa.eventmodel.*;
   *   import oracle.apps.mwa.container.FileLogger;
   *   import oracle.apps.fnd.flexj.FlexException;
   *   import oracle.apps.inv.lov.server.ItemLOV;
   *   import oracle.apps.inv.lov.server.LocatorKFF;
   *   import oracle.apps.inv.lov.server.SubinventoryLOV;
   *   import xxwc.oracle.apps.inv.lov.server.*;
   *
   *   PURPOSE:   Java Class for XXWCPrinterPage Page Layout.
   *    
   *   REVISIONS:
   *   Ver        Date        Author                     Description
   *   ---------  ----------  ---------------         -------------------------
        1.0       09-FEB-2015   Lee Spitzer             Initial Version - 
                                                          TMS Ticket 20150302-00152 - RF Receiving
        1.1       07-DEC-2015   P.Vamshidhar           TMS#20151103-00188 - RF Enhancements
        1.2       29-MAR-2016   Lee Spitzer            TMS Ticket 20151103-00182 - RF - UI Improvements to reduce number of clicks
**************************************************************************/

package xxwc.oracle.apps.inv.labels.server;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.SQLException;

import oracle.apps.mwa.beans.*;
import oracle.apps.mwa.container.MWALib;
import oracle.apps.mwa.container.Session;
import oracle.apps.mwa.eventmodel.*;
import oracle.apps.mwa.container.FileLogger;
import oracle.apps.fnd.flexj.FlexException;
import oracle.apps.fnd.flexj.KeyFlexfield;
import oracle.apps.inv.invinq.server.ItemOnhandIterator;
import oracle.apps.inv.utilities.server.*;
import xxwc.oracle.apps.inv.lov.server.*;

/*************************************************************************
 *   NAME: XXWCPrinterPage extends PageBean
 *
 *   PURPOSE:   Main class for XXWCPrinterPage
 *
 *   REVISIONS:
 *   Ver        Date        Author                     Description
 *   ---------  ----------  ---------------         -------------------------
 *   1.0       09-FEB-2015   Lee Spitzer             Initial Version - 
 *                                                   TMS Ticket 20150302-00152 - RF Receiving
 *   1.1       07-DEC-2015   P.Vamshidhar           TMS#20151103-00188 - RF Enhancements
 **************************************************************************/


public class XXWCPrinterPage extends PageBean{
    /*Define objects in the class*/
    public static String WMS_TXN_SUCCESS = "Txn Successful";
    public static String WMS_TXN_ERROR = "Txn Failed. Please check log";
    public static String WMS_TXN_CANCEL = "Txn Cancelled";
    public static String WMS_LOT_INS_FAIL = "Lot Insert Failed";
    public static String WMS_SER_INS_FAIL = "Serial Insert Failed";
    public static String savenextPrompt = "";
    public static String donePrompt = "";
    public static String cancelPrompt = "";
    private boolean areThePromptsSet;
    protected XXWCPrintersLOV mPrinterFld;
    protected XXWCPrintersLOV mLabelPrinterFld;
    /* Added Below code in Rev 1.1 - Begin*/
    protected XXWCItemLabelStatusFlagLOV mItemLabelStatusFlag;
    /*protected XXWCPrintersLOV mItemLabelStatusFlag;*/
    protected XXWCYesNoLOV mPrintLocationFld;
    protected TextFieldBean mCopies;    
    /* Added above code in Rev 1.1 - End*/    
    protected ButtonFieldBean mCancel;
    protected ButtonFieldBean mSaveNext;
    protected ButtonFieldBean mDone;
    protected String m_userid;


    /*************************************************************************
     *   NAME: private static String gPackage = "xxwc.oracle.apps.inv.TMS Ticket 20150302-00152 - RF Receiving.server";
     *
     *   PURPOSE:   private method to default gPackage value for debugging
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
            1.0       09-FEB-2015   Lee Spitzer             Initial Version - 
                                                                TMS Ticket 20150302-00152 - RF Receiving
     **************************************************************************/

    private static String gPackage = "xxwc.oracle.apps.inv.TMS Ticket 20150302-00152 - RF Receiving.server";


    /*************************************************************************
     *   NAME: private static String gCallFrom = "XXWCPrinterPage";
     *
     *   PURPOSE:   private method to pass gCallFrom from methods to return class name CustomListener
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
            1.0       09-FEB-2015   Lee Spitzer             Initial Version - 
                                                                TMS Ticket 20150302-00152 - RF Receiving
     **************************************************************************/

    private static String gCallFrom = "XXWCPrinterPage";


    /*************************************************************************
     *   NAME: public XXWCPrinterPage(Session session)
     *
     *   PURPOSE:   Inherits session from XXWCPrimaryBinAssignmentPage
     *
     *   Parameters: Session session
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
            1.0       09-FEB-2015   Lee Spitzer             Initial Version - 
                                                               TMS Ticket 20150302-00152 - RF Receiving
     **************************************************************************/

    public XXWCPrinterPage(Session session) {
        initLayout(session);
    }

    
    /**************************************************************************************************
     *   NAME: private void initLayout(Session session)
     *
     *   PURPOSE:   Sets the initial layout of the page
     *
     *   Parameters: Session session
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -----------------------------------------------
     *    1.0       09-FEB-2015   Lee Spitzer             Initial Version - 
     *                                                        TMS Ticket 20150302-00152 - RF Receiving
     *    1.1       07-DEC-2015   P.Vamshidhar            TMS#20151103-00188 
     ***************************************************************************************************/

    private void initLayout(Session session) {
        m_userid = "";
        WMS_TXN_SUCCESS = UtilFns.getMessage(session, "WMS", "WMS_TXN_SUCCESS");
        WMS_TXN_ERROR = UtilFns.getMessage(session, "WMS", "WMS_TXN_ERROR");
        WMS_TXN_CANCEL = UtilFns.getMessage(session, "WMS", "WMS_TXN_CANCEL");
        WMS_LOT_INS_FAIL = UtilFns.getMessage(session, "WMS", "WMS_LOT_INS_FAIL");
        WMS_SER_INS_FAIL = UtilFns.getMessage(session, "WMS", "WMS_SER_INS_FAIL");
        
        /*Printer*/
        mPrinterFld = new XXWCPrintersLOV();
        mPrinterFld.setName("XXWC.PRINTER");
        mPrinterFld.setRequired(true);
        /*Label Printer*/
        mLabelPrinterFld = new XXWCPrintersLOV();
        mLabelPrinterFld.setName("XXWC.LABEL_PRINTER");
        mLabelPrinterFld.setRequired(true);
        /*Item Label Status -- Added by Vamshi in TMS#20151103-00188*/
        mItemLabelStatusFlag = new XXWCItemLabelStatusFlagLOV();
        /*mItemLabelStatusFlag = new XXWCPrintersLOV();*/
        mItemLabelStatusFlag.setName("XXWC.ITEM_STATUS");
        mItemLabelStatusFlag.setRequired(true);       
        /*Print Location Fld -- Added by Vamshi in TMS#20151103-00188*/
        mPrintLocationFld = new XXWCYesNoLOV();
        mPrintLocationFld.setName("XXWC.PRINT_LOCATION");
        mPrintLocationFld.setRequired(false);   
        mPrintLocationFld.setValue("No");
        /*Copies -- Added by Vamshi in TMS#20151103-00188*/
        mCopies = new TextFieldBean();
        mCopies.setName("XXWC.COPIES");
        mCopies.setRequired(true);
        mCopies.setValue("1");
        /*Save Properties*/
        mSaveNext = new ButtonFieldBean();
        mSaveNext.setName("INV.SAVENEXT");
        mSaveNext.setPrompt("Save/Next");
        mSaveNext.setEnableAcceleratorKey(true);
        mSaveNext.setHidden(true);
        /*Save Properties*/
        mDone = new ButtonFieldBean();
        mDone.setName("INV.DONE");
        mDone.setPrompt("Done");
        mDone.setEnableAcceleratorKey(true);
        mDone.setHidden(false);
        /*Cancel Properties*/
        mCancel = new ButtonFieldBean();
        mCancel.setName("INV.CANCEL");
        mCancel.setPrompt("Cancel");
        mCancel.setEnableAcceleratorKey(true);
        mCancel.setHidden(false);
        /*set the properties for each to the addFieldBean*/
        addFieldBean(mPrinterFld);
        addFieldBean(mLabelPrinterFld);
        /* Below field added by Vamshi in TMS#20151103-00188 - Begin*/
        addFieldBean(mItemLabelStatusFlag);
        addFieldBean(mPrintLocationFld);
        addFieldBean(mCopies);
        /* Above fields added by Vamshi in TMS#20151103-00188 - End*/
        addFieldBean(mSaveNext);
        addFieldBean(mDone);
        addFieldBean(mCancel);
        XXWCPrinterFListener fieldListener = new XXWCPrinterFListener();
        /*Add Listener to each property*/
        mPrinterFld.addListener(fieldListener);
        mLabelPrinterFld.addListener(fieldListener);
        /* Below Code added by Vamshi in TMS#20151103-00188 - Begin*/        
        mItemLabelStatusFlag.addListener(fieldListener);        
        mPrintLocationFld.addListener(fieldListener);
        mCopies.addListener(fieldListener);
        /* Above Code added by Vamshi in TMS#20151103-00188 - End*/        
        mSaveNext.addListener(fieldListener);
        mDone.addListener(fieldListener);
        mCancel.addListener(fieldListener);
        try {
            setPrompts(session);
        } catch (Exception exception) {
            session.setStatusMessage(UtilFns.getMessage(session, "WMS", "WMS_PROMPT_NOTSET"));
        }
    }
    
    /*******************************************************************************************
     *   NAME: private void initPrompts(Session session) throws SQLException
     *
     *   PURPOSE:   Sets the initial prompts of the session
     *
     *   Parameters: Session session
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
     *    1.0       09-FEB-2015   Lee Spitzer             Initial Version - 
     *                                                    TMS Ticket 20150302-00152 - RF Receiving
     *    1.1       07-DEC-2015   P.Vamshidhar            TMS#20151103-00188 
     ***********************************************************************************************/

    private void initPrompts(Session session) throws SQLException {
        try {
            String gMethod = "initPrompts";
            String gCallPoint = "Start";
            FileLogger.getSystemLogger().trace(gPackage + "." + gCallFrom + " " + gMethod + " " + gCallPoint);
            String s = (String)session.getObject("ORGCODE");
            setPrompt((new StringBuilder()).append("XXWC Set Printers (").append(s).append(")").toString());

            mPrinterFld.setPrompt("Printer");
            mLabelPrinterFld.setPrompt("Label Printer");
            /* Below code added by Vamshi in TMS#20151103-00188 - Begin*/            
            mItemLabelStatusFlag.setPrompt("Label Size");            
            mPrintLocationFld.setPrompt("Location?");
            mCopies.setPrompt("Copies");
            /* Above Code added by Vamshi in TMS#20151103-00188 - Begin*/                        
            savenextPrompt = MWALib.getAKPrompt(session, "oracle.apps.inv.utilities.InvResourceTable", "INV_SAVE_NEXT_PROMPT");
            donePrompt = MWALib.getAKPrompt(session, "oracle.apps.inv.utilities.InvResourceTable", "INV_DONE_PROMPT");
            cancelPrompt = MWALib.getAKPrompt(session, "oracle.apps.inv.utilities.InvResourceTable", "INV_CANCEL_PROMPT");
            areThePromptsSet = true;
            gCallPoint = "End";
            FileLogger.getSystemLogger().trace(gPackage + "." + gCallFrom + " " + gMethod + " " + gCallPoint);
            session.putObject("sessEnterPage", "Y"); /*Added TMS#20151103-00188*/
        } catch (SQLException sqlexception) {
            session.setStatusMessage(UtilFns.getMessage(session, "WMS", "WMS_TXN_ERROR"));
        }
    }

    /*************************************************************************
     *   NAME: private void setPrompts(Session session)
     *
     *   PURPOSE:   Sets the set prompts of the session
     *
     *   Parameters: Session session
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
          1.0       09-FEB-2015   Lee Spitzer             Initial Version - 
                                                             TMS Ticket 20150302-00152 - RF Receiving
          1.1       29-MAR-2016   Lee Spitzer             TMS Ticket Number 20151103-00182 - RF - UI Improvements to reduce number of clicks
     **************************************************************************/

    private void setPrompts(Session session) {
            String gMethod = "setPrompts";
            String gCallPoint = "Start";
            if (!areThePromptsSet) {
                try {
                    initPrompts(session);
                } catch (SQLException sqlexception) {
                }
            }
            FileLogger.getSystemLogger().trace(gPackage + "." + gCallFrom + " " + gMethod + " " + gCallPoint);
            String s = (String)session.getObject("ORGCODE");
            setPrompt((new StringBuilder()).append("XXWC Set Printers (").append(s).append(")").toString());

            mSaveNext.setPrompt(savenextPrompt);
            mDone.setPrompt(donePrompt);
            mCancel.setPrompt(cancelPrompt);
            mSaveNext.retrieveAttributes("INVE_SAVE_NEXT_PROMPT");
            mDone.retrieveAttributes("INV_DONE_PROMPT");
            mCancel.retrieveAttributes("INV_CANCEL_PROMPT");
            //mCancel.setPrompt("Menu"); //Removed 03/29/2016 TMS Ticket 20151103-00182
            gCallPoint = "End Prompts";
            FileLogger.getSystemLogger().trace(gPackage + "." + gCallFrom + " " + gMethod + " " + gCallPoint);
            /*setFieldValues(session);*/
    }


     /*************************************************************************
     *   NAME: public XXWCPrintersLOV getmPrinterFld(
     *
     *   PURPOSE:   return the value of getmPrinterFld
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
            1.0       09-FEB-2015   Lee Spitzer             Initial Version - 
                                                                TMS Ticket 20150302-00152 - RF Receiving
     **************************************************************************/

    public XXWCPrintersLOV getmPrinterFld() {
        return mPrinterFld;
    }

    /*************************************************************************
    *   NAME: public XXWCPrintersLOV getmLabelPrinterFld()
    *
    *   PURPOSE:   return the value of getmLabelPrinterFld
    *
    *   REVISIONS:
    *   Ver        Date        Author                     Description
    *   ---------  ----------  ---------------         -------------------------
           1.0       09-FEB-2015   Lee Spitzer             Initial Version - 
                                                               TMS Ticket 20150302-00152 - RF Receiving
    **************************************************************************/

    public XXWCPrintersLOV getmLabelPrinterFld() {
        return mLabelPrinterFld;
    }
    
    /*************************************************************************
     *   NAME: public ButtonFieldBean getmCancel
     *
     *   PURPOSE:   return the value of mCancel
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
            1.0       09-FEB-2015   Lee Spitzer             Initial Version - 
                                                                TMS Ticket 20150302-00152 - RF Receiving
     **************************************************************************/

    public ButtonFieldBean getmCancel() {
        return mCancel;
    }
    
    /*************************************************************************
     *   NAME: public XXWCItemLabelStatusFlagLOV getmItemLabelStatusFlag()
     *
     *   PURPOSE:   return the value of Label Size
     *
     *   REVISIONS:
     *   Ver        Date           Author                     Description
     *   ---------  ----------    ---------------         -------------------------
            1.1     07-Dec-2015   P.Vamshidhar             Initial Version - 
                                                            TMS#20151103-00188 
     **************************************************************************/

     public XXWCItemLabelStatusFlagLOV getmItemLabelStatusFlag() {
         return mItemLabelStatusFlag;
     }

    /****************************************************************************************
    *   NAME: public XXWCYesNoLOV mPrintLocationFld()
    *
    *   PURPOSE:   return the value of Print location Yes/No
    *
    *   REVISIONS:
    *   Ver        Date        Author                     Description
    *   ---------  ----------  ---------------         -------------------------
    *  1.1     07-Dec-2015   P.Vamshidhar             Initial Version - TMS#20151103-00188 
    *****************************************************************************************/

    public XXWCYesNoLOV getmPrintLocationFld() {
        return mPrintLocationFld;
    }


    /****************************************************************************************
    *   NAME: public TextFieldBean getmCopies()
    *
    *   PURPOSE:   return the value of Print location Yes/No
    *
    *   REVISIONS:
    *   Ver        Date        Author                     Description
    *   ---------  ----------  ---------------         -------------------------
    *  1.1     11-FEB-2016      Lee Spitzer             Initial Version - TMS#20151103-00188 
    *****************************************************************************************/

    public TextFieldBean getmCopies() {
        return mCopies;
    }
    /*************************************************************************
     *   NAME: public ButtonFieldBean getmSaveNext()
     *
     *   PURPOSE:   return the value of mSaveNext
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
             1.0       09-FEB-2015   Lee Spitzer             Initial Version - 
                                                                TMS Ticket 20150302-00152 - RF Receiving
     **************************************************************************/

    public ButtonFieldBean getmSaveNext() {
        return mSaveNext;
    }

    /*************************************************************************
     *   NAME: public ButtonFieldBean getmDone()
     *
     *   PURPOSE:   return the value of mDone
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
             1.0       09-FEB-2015   Lee Spitzer             Initial Version - 
                                                                TMS Ticket 20150302-00152 - RF Receiving
     **************************************************************************/

    public ButtonFieldBean getmDone() {
        return mDone;
    }


    /*************************************************************************
     *   NAME: public String getUserId()
     *
     *   PURPOSE:   return the value of m_userid
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
             1.0       09-FEB-2015   Lee Spitzer             Initial Version - 
                                                                TMS Ticket 20150302-00152 - RF Receiving
     **************************************************************************/

    public String getUserId() {
        return m_userid;
    }

    /*************************************************************************
     *   NAME: void pageEntered(MWAEvent e) throws AbortHandlerException, InterruptedHandlerException,
                                               DefaultOnlyHandlerException {
     *
     *   PURPOSE:   pageEntered method
     *
     *   Parameters: MWAEvent mwaevent
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
             1.0       09-FEB-2015   Lee Spitzer             Initial Version - 
                                                                TMS Ticket 20150302-00152 - RF Receiving
     **************************************************************************/

    public void pageEntered(MWAEvent e) throws AbortHandlerException, InterruptedHandlerException,
                                               DefaultOnlyHandlerException {
        Session session = e.getSession();
    }

    /*************************************************************************
     *   NAME: void pageExited(MWAEvent e) throws AbortHandlerException, InterruptedHandlerException,
                                               DefaultOnlyHandlerException {
     *
     *   PURPOSE:   pageExited method
     *
     *
     *   Parameters: MWAEvent   mwaevent
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
             1.0       09-FEB-2015   Lee Spitzer             Initial Version - 
                                                                TMS Ticket 20150302-00152 - RF Receiving

     **************************************************************************/

    public void pageExited(MWAEvent e) throws AbortHandlerException, InterruptedHandlerException,
                                              DefaultOnlyHandlerException {
        Session session = e.getSession();
    }
   
}
