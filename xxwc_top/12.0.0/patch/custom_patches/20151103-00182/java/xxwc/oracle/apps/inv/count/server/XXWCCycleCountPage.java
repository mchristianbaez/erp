/*************************************************************************
   *   $Header XXWCCycleCountPage.java $
   *   Module Name: XXWCCycleCountPage
   *   
   *   Package: package xxwc.oracle.apps.inv.count.server;
   *   
   *   Imports Classes:
   *   
   *   import java.sql.*;
   *   import oracle.apps.inv.utilities.server.UtilFns;
   *   import oracle.apps.mwa.beans.*;
   *   import oracle.apps.mwa.container.MWALib;
   *   import oracle.apps.mwa.container.Session;
   *   import oracle.apps.mwa.eventmodel.*;
   *   import oracle.apps.mwa.container.FileLogger;
   *   import oracle.apps.fnd.flexj.FlexException;
   *   import oracle.apps.inv.lov.server.ItemLOV;
   *   import oracle.apps.inv.lov.server.LocatorKFF;
   *   import oracle.apps.inv.lov.server.SubinventoryLOV;
   *   import xxwc.oracle.apps.inv.lov.server.*;
   *
   *   PURPOSE:   Java Class for XXWC Cycle Count Page Layout.
   *    
   *   REVISIONS:
   *   Ver        Date        Author                     Description
   *   ---------  ----------  ---------------         -------------------------
        1.0       01-JUN-2015   Lee Spitzer             Initial Version - 
                                                            TMS Ticket Number 20150302-00173 - RF - Cyclecount
        1.1       17-MAR-2016   Lee Spitzer             TMS Ticket Number 20150810-00022 - RF - Cyclecount Research Report

        1.2       29-MAR-2016   Lee Spitzer             TMS Ticket Number 20151103-00182 - RF  - UI Improvements to reduce number of clicks

**************************************************************************/
package xxwc.oracle.apps.inv.count.server;

import java.sql.*;

import oracle.apps.mwa.beans.*;
import oracle.apps.mwa.container.MWALib;
import oracle.apps.mwa.container.Session;
import oracle.apps.mwa.eventmodel.*;
import oracle.apps.mwa.container.FileLogger;
import oracle.apps.fnd.flexj.FlexException;
import oracle.apps.fnd.flexj.KeyFlexfield;
import oracle.apps.inv.invinq.server.ItemOnhandIterator;
import oracle.apps.inv.lov.server.CycleCountLOV;
import oracle.apps.inv.utilities.server.*;
import oracle.apps.inv.lov.server.ItemLOV;
import oracle.apps.inv.lov.server.LocatorKFF;
import oracle.apps.inv.lov.server.SubinventoryLOV;

import oracle.apps.mwa.presentation.telnet.TelnetSession;

import xxwc.oracle.apps.inv.invinq.server.XXWCItemOnHand;
import xxwc.oracle.apps.inv.invinq.server.XXWCItemOnHandIterator;
import xxwc.oracle.apps.inv.lov.server.*;
import xxwc.oracle.apps.inv.rcv.server.XXWCRcvGenPage;

/*************************************************************************
 *   NAME: public class XXWCCycleCountPage extends PageBean
 *
 *   PURPOSE:   Main class for XXWCPrimaryBinAssignmentFunction
 *
 *   REVISIONS:
 *   Ver        Date        Author                     Description
 *   ---------  ----------  ---------------         -------------------------
      1.0       01-JUN-2015   Lee Spitzer             Initial Version -
                                                      TMS Ticket Number 20150302-00173 - RF - Cyclecount
      1.1       17-MAR-2016   Lee Spitzer             TMS Ticket Number 20150810-00022 - RF - Cyclecount Research Report

      1.2       29-MAR-2016   Lee Spitzer             TMS Ticket Number 20151103-00182 - RF  - UI Improvements to reduce number of clicks
 **************************************************************************/

public class XXWCCycleCountPage extends PageBean{
    /*Define objects in the class*/
    public static String WMS_TXN_SUCCESS = "Txn Successful";
    public static String WMS_TXN_ERROR = "Txn Failed. Please check log";
    public static String WMS_TXN_CANCEL = "Txn Cancelled";
    public static String WMS_LOT_INS_FAIL = "Lot Insert Failed";
    public static String WMS_SER_INS_FAIL = "Serial Insert Failed";
    public static String saveNextPrompt = "";
    public static String donePrompt = "";
    public static String cancelPrompt = "";
    private boolean areThePromptsSet;
    TextFieldBean mCycleCountHeaderFld;
    TextFieldBean mCycleCountEntryId;
    TextFieldBean mWCCycleCountSequence;
    TextFieldBean mCycleCountSequence;
    TextFieldBean mCostGroupId;
    TextFieldBean mStockLocator;
    TextFieldBean mInventoryLocationId;
    protected XXWCBinsLOV mLocFld;
    TextFieldBean mItemReadOnly;
    TextFieldBean mItemDescFld;
    TextFieldBean mItemIdFld;
    protected XXWCItemLOV mItemFld;
    TextFieldBean mSubFld;
    TextFieldBean mLotFld;
    TextFieldBean mMinFld;
    TextFieldBean mMaxFld;
    TextFieldBean mSVFld;
    TextFieldBean mUOMFld;
    TextFieldBean mCountQtyFld;
    TextFieldBean mSSQty;
    TextFieldBean mDueDateFld;
    TextFieldBean mPriorCountQuantityFld;
    TextFieldBean mTotalCountQuantityFld;
    protected ButtonFieldBean mPrevious;
    protected ButtonFieldBean mNext;
    protected ButtonFieldBean mClose;
    protected ButtonFieldBean mProcess;
    protected ButtonFieldBean mCreateBin;
    protected ButtonFieldBean mAddItem;
    protected ButtonFieldBean mCCReport; //Added 03/17/2016 TMS Ticket 20150810-00022
    protected String m_userid;
    public XXWCCycleCountIterator mCycleCountSeq;
    public XXWCCycleCount mCurrentCycleCount;
    /*************************************************************************
     *   NAME: private static String gPackage = "xxwc.oracle.apps.inv.rcv.server";
     *
     *   PURPOSE:   private method to default gPackage value for debugging
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
            1.0       01-JUN-2015   Lee Spitzer             Initial Version - 
                                                            TMS Ticket Number 20150302-00173 - RF - Cyclecount
     **************************************************************************/

    private static String gPackage = "xxwc.oracle.apps.inv.count.server";


    /*************************************************************************
     *   NAME: private static String gCallFrom = "XXWCCycleCountPage";
     *
     *   PURPOSE:   private method to pass gCallFrom from methods to return class name CustomListener
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
            1.0       01-JUN-2015   Lee Spitzer             Initial Version - 
                                                            TMS Ticket Number 20150302-00173 - RF - Cyclecount
     **************************************************************************/

    private static String gCallFrom = "XXWCCycleCountPage";



    
    /*************************************************************************
    *   NAME: public XXWCCycleCountPage(Session session)
    *
    *   PURPOSE:   Main method and calls initLayout Method
    *
    *   REVISIONS:
    *   Ver        Date        Author                     Description
    *   ---------  ----------  ---------------         -------------------------
         1.0       01-JUN-2015   Lee Spitzer             Initial Version - 
                                                         TMS Ticket Number 20150302-00173 - RF - Cyclecount
     **************************************************************************/

    public XXWCCycleCountPage(Session session) {
        initLayout(session);
    }
    
    /*************************************************************************
    *   NAME: private void initLayout(Session session)
    *
    *   PURPOSE:   Sets the initial layout of the paged
    *
    *   REVISIONS:
    *   Ver        Date        Author                     Description
    *   ---------  ----------  ---------------         -------------------------
         1.0       01-JUN-2015   Lee Spitzer             Initial Version - 
                                                         TMS Ticket Number 20150302-00173 - RF - Cyclecount
         1.1       17-MAR-2016   Lee Spitzer             TMS Ticket Number 20150810-00022 - RF - Cyclecount Research Report

         1.2       29-MAR-2016   Lee Spitzer             TMS Ticket Number 20151103-00182 - RF  - UI Improvements to reduce number of clicks
     **************************************************************************/

    private void initLayout(Session session) {
        String gMethod = "setPrompts";
        String gCallPoint = "Start";
        m_userid = "";
        WMS_TXN_SUCCESS = UtilFns.getMessage(session, "WMS", "WMS_TXN_SUCCESS");
        WMS_TXN_ERROR = UtilFns.getMessage(session, "WMS", "WMS_TXN_ERROR");
        WMS_TXN_CANCEL = UtilFns.getMessage(session, "WMS", "WMS_TXN_CANCEL");
        WMS_LOT_INS_FAIL = UtilFns.getMessage(session, "WMS", "WMS_LOT_INS_FAIL");
        WMS_SER_INS_FAIL = UtilFns.getMessage(session, "WMS", "WMS_SER_INS_FAIL");
        
        
        String l_doc_type;
        String l_transaction_type;
        String l_entry_status_code;
        try {
            l_doc_type = (String)session.getObject("TXN.DOCTYPE");
            l_transaction_type = (String)session.getObject("TXN.TRANSACTION");
            l_entry_status_code = (String)session.getObject("TXN.ENTRY_STATUS_CODE");
        }
        catch (Exception e){
            FileLogger.getSystemLogger().error(gPackage + "." + gCallFrom + " " + gMethod  + " " + gCallPoint + " error getting l_doc_type and l_transaction_type " + e);
            l_doc_type = "";
            l_transaction_type = "";
            l_entry_status_code = "";
        }
        
        /*Cycle Count Header*/
        mCycleCountHeaderFld = new TextFieldBean();
        mCycleCountHeaderFld.setName("XXWC.CYCLECOUNT_HEADER");
        mCycleCountHeaderFld.setPrompt("CycleHdrId");
        mCycleCountHeaderFld.setRequired(false);
        mCycleCountHeaderFld.setHidden(false);
        mCycleCountHeaderFld.setEditable(true);
        /*Cycle Count Entry Id*/
        mCycleCountEntryId = new TextFieldBean();
        mCycleCountEntryId.setName("XXWC.CYCLECOUNT_ENTRY_ID");
        mCycleCountEntryId.setPrompt("CycleEntryId");
        mCycleCountEntryId.setRequired(false);
        mCycleCountEntryId.setHidden(true);
        mCycleCountEntryId.setEditable(false);
        /*WC Count List Sequence*/
        mWCCycleCountSequence = new TextFieldBean();
        mWCCycleCountSequence.setName("XXWC.WC_COUNT_SEQUENCE");
        mWCCycleCountSequence.setPrompt("WC Seq");
        mWCCycleCountSequence.setRequired(false);
        mWCCycleCountSequence.setHidden(true);
        mWCCycleCountSequence.setEditable(false);
        /*Count List Sequence*/
        mCycleCountSequence = new TextFieldBean();
        mCycleCountSequence.setName("XXWC.COUNT_SEQUENCE");
        mCycleCountSequence.setPrompt("Seq");
        mCycleCountSequence.setRequired(false);
        mCycleCountSequence.setHidden(true);
        mCycleCountSequence.setEditable(false);
        /*Cost Group Id*/
        mCostGroupId = new TextFieldBean();
        mCostGroupId.setName("XXWC.COST_GROUP_ID");
        mCostGroupId.setPrompt("CostGroupId");
        mCostGroupId.setRequired(false);
        mCostGroupId.setHidden(true);
        mCostGroupId.setEditable(false);
        /*Stock Locator*/
        mStockLocator = new TextFieldBean();
        mStockLocator.setName("XXWC.STOCK_LOCATOR");
        mStockLocator.setPrompt("StockLocator");
        mStockLocator.setRequired(false);
        mStockLocator.setHidden(true);
        mStockLocator.setEditable(false);
        /*Stock Locator Id*/
        mInventoryLocationId = new TextFieldBean();
        mInventoryLocationId.setName("XXWC.LOCATOR_ID");
        mInventoryLocationId.setPrompt("LocatorId");
        mInventoryLocationId.setRequired(false);
        mInventoryLocationId.setHidden(true);
        mInventoryLocationId.setEditable(false);
        /*Bin*/
        mLocFld = new XXWCBinsLOV();
        mLocFld.setName("XXWC.LOC");
        mLocFld.setPrompt("Bin");
        mLocFld.setRequired(false);
        mLocFld.setEditable(false);
        /*Item Read Only*/;
        mItemReadOnly = new TextFieldBean();
        mItemReadOnly.setName("XXWC.ITEM_RO");
        mItemReadOnly.setPrompt("Item");
        mItemReadOnly.setRequired(false);
        mItemReadOnly.setEditable(false);
        /*Item Description*/
        mItemIdFld = new TextFieldBean();
        mItemIdFld.setName("XXWC.ITEM_ID");
        mItemIdFld.setPrompt("Item ID");
        mItemIdFld.setEditable(false);
        mItemIdFld.setHidden(true);
        /*Item Description*/
        mItemDescFld = new TextFieldBean();
        mItemDescFld.setName("XXWC.DESC");
        mItemDescFld.setPrompt("Item Desc");
        mItemDescFld.setEditable(false);
        mItemDescFld.setHidden(false);
        /*Item*/        
        mItemFld = new XXWCItemLOV("CYCLE");
        mItemFld.setName("XXWC.ITEM");
        mItemFld.setPrompt("Item");
        mItemFld.setRequired(false);
        mItemFld.setEditable(true);
        /*Subinventory*/
        mSubFld = new TextFieldBean();
        mSubFld.setName("XXWC.SUB");
        mSubFld.setPrompt("Sub");
        mSubFld.setRequired(false);
        mSubFld.setEditable(false);
        mSubFld.setHidden(false);
        /*Due Date*/
        mDueDateFld = new TextFieldBean();
        mDueDateFld.setName("XXWC.DUE_DATE");
        mDueDateFld.setPrompt("Due Date");
        mDueDateFld.setRequired(false);
        mDueDateFld.setEditable(false);
        mDueDateFld.setHidden(true);
        /*Snap Shot On-Hand Qty*/
        mSSQty = new TextFieldBean();
        mSSQty.setName("XXWC.SS_QTY");
        mSSQty.setPrompt("SS Qty");
        mSSQty.setRequired(false);
        mSSQty.setEditable(false);
        mSSQty.setHidden(true);
        /*Snap Shot On-Hand Qty*/
        mPriorCountQuantityFld = new TextFieldBean();
        mPriorCountQuantityFld.setName("XXWC.PRIOR_COUNT_QTY");
        mPriorCountQuantityFld.setPrompt("Prior Count");
        mPriorCountQuantityFld.setRequired(false);
        mPriorCountQuantityFld.setEditable(false);
        mPriorCountQuantityFld.setHidden(true);
        /*Lot*/
        mLotFld = new TextFieldBean();
        mLotFld.setName("XXWC.SUB");
        mLotFld.setPrompt("Sub");
        mLotFld.setRequired(false);
        mLotFld.setEditable(false);
        mLotFld.setHidden(true);
        /*Min*/
        mMinFld = new TextFieldBean();
        mMinFld.setName("XXWC.MIN");
        mMinFld.setPrompt("MIN");
        mMinFld.setEditable(false);
        mMinFld.setHidden(true);
        /*Max*/
        mMaxFld = new TextFieldBean();
        mMaxFld.setName("XXWC.MAX");
        mMaxFld.setPrompt("Max");
        mMaxFld.setEditable(false);
        mMaxFld.setHidden(true);
        /*SV*/
        mSVFld = new TextFieldBean();
        mSVFld.setName("XXWC.SV");
        mSVFld.setPrompt("SV");
        mSVFld.setEditable(false);
        mSVFld.setHidden(true);
        /*UOM Properties*/
        mUOMFld = new TextFieldBean();
        mUOMFld.setName("XXWC.UOM");
        mUOMFld.setPrompt("UOM");
        mUOMFld.setEditable(false);
        mUOMFld.setHidden(false);
        /*Count Qty*/
        mCountQtyFld = new TextFieldBean();
        mCountQtyFld.setName("XXWC.QTY");
        mCountQtyFld.setPrompt("Qty");
        mCountQtyFld.setEditable(true);
        mCountQtyFld.setRequired(false);
        /*Total Qty*/
        mTotalCountQuantityFld = new TextFieldBean();
        mTotalCountQuantityFld.setName("XXWC.TOTAL_QTY");
        mTotalCountQuantityFld.setPrompt("Total Qty");
        mTotalCountQuantityFld.setEditable(false);
        mTotalCountQuantityFld.setRequired(false);
        mTotalCountQuantityFld.setHidden(false);
        /*Previous Properties*/
        mPrevious = new ButtonFieldBean();
        mPrevious.setName("XXWC.PREVIOUS");
        mPrevious.setPrompt("Previous");
        mPrevious.setEnableAcceleratorKey(true);
        mPrevious.setHidden(true);
        /*Next Properties*/
        mNext = new ButtonFieldBean();
        mNext.setName("XXWC.NEXT");
        mNext.setPrompt("Next");
        mNext.setEnableAcceleratorKey(true);
        mNext.setHidden(true);
        /*Close Properties*/
        mClose = new ButtonFieldBean();
        mClose.setName("XXWC.CLOSE");
        mClose.setPrompt("Close");
        mClose.setEnableAcceleratorKey(true);
        mClose.setHidden(false);
        /*Process Properties*/
        mProcess = new ButtonFieldBean();
        mProcess.setName("XXWC.PROCESS");
        mProcess.setPrompt("Process");
        mProcess.setEnableAcceleratorKey(true);
        //mProcess.setHidden(false); //Removed 03/29/2016 TMS Ticket 20151103-00182
        mProcess.setHidden(true); //Added 03/29/2016 TMS Ticket 20151103-00182
        /*Create Bin Properties*/
        mCreateBin = new ButtonFieldBean();
        mCreateBin.setName("XXWC.CREATE_BIN");
        mCreateBin.setPrompt("Create Bin");
        mCreateBin.setEnableAcceleratorKey(true);
        mCreateBin.setHidden(false);
        /*Add Item Properties*/
        mAddItem = new ButtonFieldBean();
        mAddItem.setName("XXWC.ADD_ITEM");
        //mAddItem.setPrompt("Add Item");//Removed 03/29/2016 TMS Ticket 20151103-00182
        mAddItem.setPrompt("Add New Item"); //Added 03/29/2016 TMS Ticket 20151103-00182
        mAddItem.setEnableAcceleratorKey(true);
        mAddItem.setHidden(false);
        /*Cycle Count Research Report*/ //Added 03/17/2016 TMS Ticket 20150810-00022
        mCCReport = new ButtonFieldBean(); //Added 03/17/2016 TMS Ticket 20150810-00022
        mCCReport.setName("XXWC.CC_REPORT"); //Added 03/17/2016 TMS Ticket 20150810-00022
        mCCReport.setPrompt("CC Report"); //Added 03/17/2016 TMS Ticket 20150810-00022
        mCCReport.setEnableAcceleratorKey(true); //Added 03/17/2016 TMS Ticket 20150810-00022
        mCCReport.setHidden(true); //Added 03/17/2016 TMS Ticket 20150810-00022
        /*set the properties for each to the addFieldBean*/
        addFieldBean(mCycleCountHeaderFld);
        addFieldBean(mCycleCountEntryId);
        addFieldBean(mWCCycleCountSequence);
        addFieldBean(mCycleCountSequence);
        addFieldBean(mCostGroupId);
        addFieldBean(mStockLocator);
        addFieldBean(mInventoryLocationId);                               
        addFieldBean(mLocFld);
        addFieldBean(mItemReadOnly);
        addFieldBean(mItemDescFld);
        addFieldBean(mItemIdFld);
        addFieldBean(mItemFld);
        addFieldBean(mSubFld);
        addFieldBean(mDueDateFld);
        addFieldBean(mSSQty);
        addFieldBean(mPriorCountQuantityFld);
        addFieldBean(mLotFld);
        addFieldBean(mMinFld);
        addFieldBean(mMaxFld);
        addFieldBean(mSVFld);
        addFieldBean(mUOMFld);
        addFieldBean(mTotalCountQuantityFld);
        addFieldBean(mCountQtyFld);
        //addFieldBean(mPrevious); //Removed 03/29/2016 TMS Ticket 20151103-00182
        addFieldBean(mNext);
        addFieldBean(mPrevious); //Added 03/29/2016 TMS Ticket 20151103-00182
        addFieldBean(mClose);
        addFieldBean(mProcess);
        //addFieldBean(mCreateBin); //Removed 03/29/2016 TMS Ticket 20151103-00182
        addFieldBean(mAddItem);
        addFieldBean(mCreateBin);//Added 03/29/2016 TMS Ticket 20151103-00182
        addFieldBean(mCCReport); //Added 03/17/2016 TMS Ticket 20150810-00022
        /*Point Page to the XXWCPrimaryBinAssignmentFListener Listener*/
        XXWCCycleCountFListener fieldListener = new XXWCCycleCountFListener();
        /*Add Listener to each property*/
        mCycleCountHeaderFld.addListener(fieldListener);
        mCycleCountEntryId.addListener(fieldListener);
        mWCCycleCountSequence.addListener(fieldListener);
        mCycleCountSequence.addListener(fieldListener);
        mCostGroupId.addListener(fieldListener);
        mStockLocator.addListener(fieldListener);
        mInventoryLocationId.addListener(fieldListener);
        mLocFld.addListener(fieldListener);
        mItemReadOnly.addListener(fieldListener);
        mItemDescFld.addListener(fieldListener);
        mItemIdFld.addListener(fieldListener);
        mItemFld.addListener(fieldListener);
        mSubFld.addListener(fieldListener);
        mDueDateFld.addListener(fieldListener);
        mSSQty.addListener(fieldListener);
        mPriorCountQuantityFld.addListener(fieldListener);
        mLotFld.addListener(fieldListener);
        mMinFld.addListener(fieldListener);
        mMaxFld.addListener(fieldListener);
        mSVFld.addListener(fieldListener);
        mUOMFld.addListener(fieldListener);
        mTotalCountQuantityFld.addListener(fieldListener);
        mCountQtyFld.addListener(fieldListener);
        //mPrevious.addListener(fieldListener);  //Removed 03/29/2016 TMS Ticket 20151103-00182
        mNext.addListener(fieldListener);
        mPrevious.addListener(fieldListener);  //Added 03/29/2016 TMS Ticket 20151103-00182
        mClose.addListener(fieldListener);
        mProcess.addListener(fieldListener);
        //mCreateBin.addListener(fieldListener); //Removed 03/29/2016 TMS Ticket 20151103-00182
        mAddItem.addListener(fieldListener);
        mCreateBin.addListener(fieldListener); //Added 03/29/2016 TMS Ticket 20151103-00182
        mCCReport.addListener(fieldListener); //Added 03/17/2016 TMS Ticket 20150810-00022
        try {
            setPrompts(session);
        } catch (Exception exception) {
            session.setStatusMessage(UtilFns.getMessage(session, "WMS", "WMS_PROMPT_NOTSET"));
        }
    }
    
    /*************************************************************************
    *   NAME: private void setPrompts(Session session)
    *
    *   PURPOSE:   Sets Prompts for the Page
    *
    *   REVISIONS:
    *   Ver        Date        Author                     Description
    *   ---------  ----------  ---------------         -------------------------
         1.0       01-JUN-2015   Lee Spitzer             Initial Version - 
                                                         TMS Ticket Number 20150302-00173 - RF - Cyclecount
     **************************************************************************/

    private void setPrompts(Session session) {
        if (!areThePromptsSet) {
            try {
                initPrompts(session);
            } catch (SQLException sqlexception) {
                session.setStatusMessage(UtilFns.getMessage(session, "WMS", "WMS_TXN_ERROR"));
            }
        }
        //mClose.setPrompt(saveNextPrompt);
        //mProcess.setPrompt(donePrompt);
        mClose.retrieveAttributes("INV_SAVE_NEXT_PROMPT");
        mProcess.retrieveAttributes("INV_DONE_PROMPT");
    }
   
    /*************************************************************************
    *   NAME: private void initPrompts(Session session) throws SQLException
    *
    *   PURPOSE:   Sets the initial prompts for the page
    *
    *   REVISIONS:
    *   Ver        Date        Author                     Description
    *   ---------  ----------  ---------------         -------------------------
         1.0       01-JUN-2015   Lee Spitzer             Initial Version - 
                                                         TMS Ticket Number 20150302-00173 - RF - Cyclecount
         1.1       29-MAR-2016   Lee Spitzer             TMS Ticket Number 20151103-00182 - RF - UI Improvements to reduce number of clicks
     **************************************************************************/
 
    private void initPrompts(Session session) throws SQLException {
        try {
            String s = (String)session.getObject("ORGCODE");
            if (session.getObject("TXN.DOCTYPE").equals("PHYSICAL")) { //Added 03/29/2016 TMS Ticket 20151103-00182
                setPrompt((new StringBuilder()).append("XXWC Physical Inventory Entry (").append(s).append(")").toString()); //Added 03/29/2016 TMS Ticket 20151103-00182
                //Added 03/29/2016 TMS Ticket 20151103-00182
            } //Added 03/29/2016 TMS Ticket 20151103-00182
            else { //Added 03/29/2016 TMS Ticket 20151103-00182
                setPrompt((new StringBuilder()).append("XXWC Cycle Count Entry (").append(s).append(")").toString());
            }//Added 03/29/2016 TMS Ticket 20151103-00182
            saveNextPrompt =
                    MWALib.getAKPrompt(session, "oracle.apps.inv.utilities.InvResourceTable", "INV_SAVE_NEXT_PROMPT");
            donePrompt = MWALib.getAKPrompt(session, "oracle.apps.inv.utilities.InvResourceTable", "INV_DONE_PROMPT");
            areThePromptsSet = true;
        } catch (SQLException sqlexception) {
            session.setStatusMessage(UtilFns.getMessage(session, "WMS", "WMS_TXN_ERROR"));
        }
    }


    /*************************************************************************
     *   NAME: public TextFieldBean  getmCycleCount()
     *
     *   PURPOSE:   return the value of mCycleCountHeaderFld
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
        1.0       01-JUN-2015   Lee Spitzer             Initial Version -
                                                                TMS Ticket Number 20150302-00173 - RF - Cyclecount
     **************************************************************************/

    public TextFieldBean getmCycleCountHeaderFld() 
    {
        return mCycleCountHeaderFld;    
    }

    /*************************************************************************
     *   NAME: public TextFieldBean  getmCycleCountEntryId()
     *
     *   PURPOSE:   return the value of mCycleCountEntryId
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
        1.0       01-JUN-2015   Lee Spitzer             Initial Version -
                                                                TMS Ticket Number 20150302-00173 - RF - Cyclecount
     **************************************************************************/

     public TextFieldBean getmCycleCountEntryId() 
     {
         return mCycleCountEntryId;    
     }

    /*************************************************************************
     *   NAME: public TextFieldBean  getmWCCycleCountSequence()
     *
     *   PURPOSE:   return the value of mWCCycleCountSequence
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
        1.0       01-JUN-2015   Lee Spitzer             Initial Version -
                                                                TMS Ticket Number 20150302-00173 - RF - Cyclecount
     **************************************************************************/

     public TextFieldBean getmWCCycleCountSequence() 
     {
         return mWCCycleCountSequence;    
     }
    
    
    /*************************************************************************
     *   NAME: public TextFieldBean  getmWCCycleCountSequence()
     *
     *   PURPOSE:   return the value of mWCCycleCountSequence
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
        1.0       01-JUN-2015   Lee Spitzer             Initial Version -
                                                                TMS Ticket Number 20150302-00173 - RF - Cyclecount
     **************************************************************************/

     public TextFieldBean getmCycleCountSequence() 
     {
         return mCycleCountSequence;    
     }


    /*************************************************************************
     *   NAME: public TextFieldBean  getmCostGroupId()
     *
     *   PURPOSE:   return the value of mCostGroupId
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
        1.0       01-JUN-2015   Lee Spitzer             Initial Version -
                                                                TMS Ticket Number 20150302-00173 - RF - Cyclecount
     **************************************************************************/

     public TextFieldBean getmCostGroupId() 
     {
         return mCostGroupId;    
     }

    
    
    /*************************************************************************
     *   NAME: public TextFieldBean  getmStockLocator()
     *
     *   PURPOSE:   return the value of mStockLocator
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
        1.0       01-JUN-2015   Lee Spitzer             Initial Version -
                                                                TMS Ticket Number 20150302-00173 - RF - Cyclecount
     **************************************************************************/

     public TextFieldBean getmStockLocator() 
     {
         return mStockLocator;    
     }


    /*************************************************************************
     *   NAME: public TextFieldBean  getmInventoryLocationId()
     *
     *   PURPOSE:   return the value of mInventoryLocationId
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
        1.0       01-JUN-2015   Lee Spitzer             Initial Version -
                                                                TMS Ticket Number 20150302-00173 - RF - Cyclecount
     **************************************************************************/

     public TextFieldBean getmInventoryLocationId() 
     {
         return mInventoryLocationId;    
     }


     /*************************************************************************
      *   NAME: public XXWCBinsLOV  getmLocFld()
      *
      *   PURPOSE:   return the value of mLocFld
      *
      *   REVISIONS:
      *   Ver        Date        Author                     Description
      *   ---------  ----------  ---------------         -------------------------
         1.0       01-JUN-2015   Lee Spitzer             Initial Version -
                                                                 TMS Ticket Number 20150302-00173 - RF - Cyclecount
      **************************************************************************/
     


    public XXWCBinsLOV getmLocFld() 
    {
        return mLocFld;
    }

    /*************************************************************************
     *   NAME: public XXWCItemLOV getmItemReadOnly() 
     *
     *   PURPOSE:   return the value of mItemReadOnly
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
        1.0       01-JUN-2015   Lee Spitzer             Initial Version -
                                                                TMS Ticket Number 20150302-00173 - RF - Cyclecount
     **************************************************************************/

    public TextFieldBean getmItemReadOnly() 
    {
        return mItemReadOnly;    
    }

    /*************************************************************************
     *   NAME: public TextFieldBean  getmItemDescFld()
     *
     *   PURPOSE:   return the value of mItemDescFld
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
        1.0       01-JUN-2015   Lee Spitzer             Initial Version -
                                                                TMS Ticket Number 20150302-00173 - RF - Cyclecount
     **************************************************************************/

    public TextFieldBean getmItemDescFld() 
    {
        return mItemDescFld;    
    }
    
    /*************************************************************************
     *   NAME: public TextFieldBean  mItemIdFld()
     *
     *   PURPOSE:   return the value of mItemDescFld
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
        1.0       01-JUN-2015   Lee Spitzer             Initial Version -
                                                                TMS Ticket Number 20150302-00173 - RF - Cyclecount
     **************************************************************************/

    public TextFieldBean getmItemIdFld() 
    {
        return mItemIdFld;    
    }    
    
    
    
    /*************************************************************************
     *   NAME: public XXWCItemLOV  getmItemFld()
     *
     *   PURPOSE:   return the value of mItemFld
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
        1.0       01-JUN-2015   Lee Spitzer             Initial Version -
                                                                TMS Ticket Number 20150302-00173 - RF - Cyclecount
     **************************************************************************/

    public XXWCItemLOV getmItemFld() 
    {
        return mItemFld;    
    }
    
    /*************************************************************************
     *   NAME: public TextFieldBean  getmSubFld()
     *
     *   PURPOSE:   return the value of mSubFld
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
        1.0       01-JUN-2015   Lee Spitzer             Initial Version -
                                                                TMS Ticket Number 20150302-00173 - RF - Cyclecount
     **************************************************************************/

    public TextFieldBean getmSubFld()
    {
        return mSubFld;
    }
    
    
    /*************************************************************************
     *   NAME: public TextFieldBean  getmDueDateFld()
     *
     *   PURPOSE:   return the value of mDueDateFld
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
        1.0       01-JUN-2015   Lee Spitzer             Initial Version -
                                                                TMS Ticket Number 20150302-00173 - RF - Cyclecount
     **************************************************************************/

    public TextFieldBean getmDueDateFld()
    {
        return mDueDateFld;
    }
    
    
    /*************************************************************************
     *   NAME: public TextFieldBean  getmSSQty()
     *
     *   PURPOSE:   return the value of mSSQty
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
        1.0       01-JUN-2015   Lee Spitzer             Initial Version -
                                                                TMS Ticket Number 20150302-00173 - RF - Cyclecount
     **************************************************************************/

    public TextFieldBean getmSSQty()
    {
        return mSSQty;
    }


    /*************************************************************************
     *   NAME: public TextFieldBean  getmPriorCountQuantityFld()
     *
     *   PURPOSE:   return the value of mPriorCountQuantityFld
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
        1.0       01-JUN-2015   Lee Spitzer             Initial Version -
                                                                TMS Ticket Number 20150302-00173 - RF - Cyclecount
     **************************************************************************/

    public TextFieldBean getmPriorCountQuantityFld()
    {
        return mPriorCountQuantityFld;
    }
    
    /*************************************************************************
     *   NAME: public TextFieldBean  getmLotFld()
     *
     *   PURPOSE:   return the value of mLotFld
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
        1.0       01-JUN-2015   Lee Spitzer             Initial Version -
                                                                TMS Ticket Number 20150302-00173 - RF - Cyclecount
     **************************************************************************/

    public TextFieldBean getmLotFld()
    {
        return mLotFld;
    }

    
    /*************************************************************************
     *   NAME: public TextFieldBean getmMinFld()
     *
     *   PURPOSE:   return the value of mMinFld
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
        1.0       01-JUN-2015   Lee Spitzer             Initial Version -
                                                                TMS Ticket Number 20150302-00173 - RF - Cyclecount
     **************************************************************************/

    public TextFieldBean getmMinFld() {
        return mMinFld;
    }

    /*************************************************************************
     *   NAME: public TextFieldBean getmMaxFld()
     *
     *   PURPOSE:   return the value of mMaxFld
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
        1.0       01-JUN-2015   Lee Spitzer             Initial Version -
                                                                TMS Ticket Number 20150302-00173 - RF - Cyclecount
     **************************************************************************/

    public TextFieldBean getmMaxFld() {
        return mMaxFld;
    }

    /*************************************************************************
     *   NAME: public TextFieldBean getmSVFld()
     *
     *   PURPOSE:   return the value of mSVFld
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
        1.0       01-JUN-2015   Lee Spitzer             Initial Version -
                                                                TMS Ticket Number 20150302-00173 - RF - Cyclecount
     **************************************************************************/

    public TextFieldBean getmSVFld() {
        return mSVFld;
    }
    
    /*************************************************************************
     *   NAME: public TextFieldBean getmUOMFld()
     *
     *   PURPOSE:   return the value of mUOMFld
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
        1.0       01-JUN-2015   Lee Spitzer             Initial Version -
                                                                TMS Ticket Number 20150302-00173 - RF - Cyclecount
     **************************************************************************/

    public TextFieldBean getmUOMFld() {
        return mUOMFld;
    }

    /*************************************************************************
     *   NAME: public TextFieldBean getmTotalCountQuantityFld()
     *
     *   PURPOSE:   return the value of mTotalCountQuantityFld
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
        1.0       01-JUN-2015   Lee Spitzer             Initial Version -
                                                                TMS Ticket Number 20150302-00173 - RF - Cyclecount
     **************************************************************************/

    public TextFieldBean getmTotalCountQuantityFld() {
        return mTotalCountQuantityFld;
    }
    
    /*************************************************************************
     *   NAME: public TextFieldBean mCountQtyFld()
     *
     *   PURPOSE:   return the value of mCountQtyFld
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
        1.0       01-JUN-2015   Lee Spitzer             Initial Version -
                                                                TMS Ticket Number 20150302-00173 - RF - Cyclecount
     **************************************************************************/

    public TextFieldBean getmCountQtyFld() {
        return mCountQtyFld;
    }

    /*************************************************************************
    *   NAME: public ButtonFieldBean getmPrevious()
    *
    *   PURPOSE:   return the value of mPrevious
    *
    *   REVISIONS:
    *   Ver        Date        Author                     Description
    *   ---------  ----------  ---------------         -------------------------
        1.0       01-JUN-2015   Lee Spitzer             Initial Version -
                                                                    TMS Ticket Number 20150302-00173 - RF - Cyclecount
    **************************************************************************/

    public ButtonFieldBean getmPrevious() {
        return mPrevious;
    }

    /*************************************************************************
    *   NAME: public ButtonFieldBean getmNext()
    *
    *   PURPOSE:   return the value of mNext
    *
    *   REVISIONS:
    *   Ver        Date        Author                     Description
    *   ---------  ----------  ---------------         -------------------------
        1.0       01-JUN-2015   Lee Spitzer             Initial Version -
                                                                    TMS Ticket Number 20150302-00173 - RF - Cyclecount
    **************************************************************************/

    public ButtonFieldBean getmNext() {
        return mNext;
    }
    
    /*************************************************************************
    *   NAME: public ButtonFieldBean getmClose()
    *
    *   PURPOSE:   return the value of mClose
    *
    *   REVISIONS:
    *   Ver        Date        Author                     Description
    *   ---------  ----------  ---------------         -------------------------
        1.0       01-JUN-2015   Lee Spitzer             Initial Version -
                                                                    TMS Ticket Number 20150302-00173 - RF - Cyclecount
    **************************************************************************/

    public ButtonFieldBean getmClose() {
        return mClose;
    }

    /*************************************************************************
    *   NAME: public ButtonFieldBean getmProcess()
    *
    *   PURPOSE:   return the value of mProcess
    *
    *   REVISIONS:
    *   Ver        Date        Author                     Description
    *   ---------  ----------  ---------------         -------------------------
        1.0       01-JUN-2015   Lee Spitzer             Initial Version -
                                                                    TMS Ticket Number 20150302-00173 - RF - Cyclecount
    **************************************************************************/

    public ButtonFieldBean getmProcess() {
        return mProcess;
    }

    /*************************************************************************
    *   NAME: public ButtonFieldBean getmCreateBin()
    *
    *   PURPOSE:   return the value of mCreateBin
    *
    *   REVISIONS:
    *   Ver        Date        Author                     Description
    *   ---------  ----------  ---------------         -------------------------
        1.0       01-JUN-2015   Lee Spitzer             Initial Version -
                                                                    TMS Ticket Number 20150302-00173 - RF - Cyclecount
    **************************************************************************/

    public ButtonFieldBean getmCreateBin() {
        return mCreateBin;
    }

    /*************************************************************************
    *   NAME: public ButtonFieldBean getmAddItem()
    *
    *   PURPOSE:   return the value of mAddItem
    *
    *   REVISIONS:
    *   Ver        Date        Author                     Description
    *   ---------  ----------  ---------------         -------------------------
        1.0       01-JUN-2015   Lee Spitzer             Initial Version -
                                                                    TMS Ticket Number 20150302-00173 - RF - Cyclecount
    **************************************************************************/

    public ButtonFieldBean getmAddItem() {
        return mAddItem;
    }
    
    /*************************************************************************
    *   NAME: public ButtonFieldBean getmCCReport()
    *
    *   PURPOSE:   return the value of mCCReport
    *
    *   REVISIONS:
    *   Ver        Date        Author                     Description
    *   ---------  ----------  ---------------         -------------------------
        1.0       17-MAR-2016   Lee Spitzer             Initial Version -
                                                            TMS Ticket Number 20150810-00022 - RF - Cyclecount Research Report
    **************************************************************************/

    public ButtonFieldBean getmCCReport() {
        return mCCReport;
    }
    
    /*************************************************************************
     *   NAME: public String getUserId() getUserId()
     *
     *   PURPOSE:   return the value of m_userid
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
        1.0       01-JUN-2015   Lee Spitzer             Initial Version -
                                                                TMS Ticket Number 20150302-00173 - RF - Cyclecount
     **************************************************************************/

    public String getUserId() {
        return m_userid;
    }

    /*************************************************************************
    *   NAME: void pageEntered(MWAEvent e) throws AbortHandlerException, InterruptedHandlerException,
                                                   DefaultOnlyHandlerException {
    *
    *   PURPOSE:   pageEntered method
    *
    *   Parameters: MWAEvent mwaevent
    *
    *   REVISIONS:
    *   Ver        Date        Author                     Description
    *   ---------  ----------  ---------------         -------------------------
        1.0       01-JUN-2015   Lee Spitzer             Initial Version -
                                                                    TMS Ticket Number 20150302-00173 - RF - Cyclecount
    **************************************************************************/

    public void pageEntered(MWAEvent e) throws AbortHandlerException, InterruptedHandlerException,
                                               DefaultOnlyHandlerException {
        
        Session session = e.getSession();
        
    }

    /*************************************************************************
    *   NAME: void pageEntered(MWAEvent e) throws AbortHandlerException, InterruptedHandlerException,
                                                       DefaultOnlyHandlerException {
    *
    *   PURPOSE:   pageEntered method
    *
    *   Parameters: MWAEvent mwaevent
    *
    *   REVISIONS:
    *   Ver        Date        Author                     Description
    *   ---------  ----------  ---------------         -------------------------
        1.0       01-JUN-2015   Lee Spitzer             Initial Version -
                                                                        TMS Ticket Number 20150302-00173 - RF - Cyclecount
    **************************************************************************/

    public void pageExited(MWAEvent e) throws AbortHandlerException, InterruptedHandlerException,
                                               DefaultOnlyHandlerException {
        Session session = e.getSession();
    
    }

    /*************************************************************************
     *   NAME: setXXWCCycleCountSeq(XXWCCycleCountIterator XXWCCycleCountIterator)
     *
     *   PURPOSE:   sets the vector sequence number on the XXWCCycleCountIterator class
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
        1.0       01-JUN-2015   Lee Spitzer             Initial Version -
                                                                TMS Ticket 20150302-00173 - RF - Cyclecount   
     **************************************************************************/

    public void setXXWCCycleCountSeq(XXWCCycleCountIterator XXWCCycleCountIterator) {
        mCycleCountSeq = XXWCCycleCountIterator;
    }

    /*************************************************************************
     *   NAME: XXWCCycleCountIterator getmXXWCCycleCountSeq()
     *
     *   PURPOSE:   gets the vector sequence number on the XXWCCycleCountIterator class
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
        1.0       01-JUN-2015   Lee Spitzer             Initial Version -
                                                                TMS Ticket 20150302-00173 - RF - Cyclecount   
     **************************************************************************/

    public XXWCCycleCountIterator getmXXWCCycleCountSeq() {
        return mCycleCountSeq;
    }

    /*************************************************************************
     *   NAME: XXWCBinPutAway getmCurrentXXWCCycleCount()
     *
     *   PURPOSE:   gets the current sequence number on the XXWCCycleCountIterator class
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
        1.0       01-JUN-2015   Lee Spitzer             Initial Version -
                                                                TMS Ticket 20150302-00173 - RF - Cyclecount   
     **************************************************************************/

    public XXWCCycleCount getmCurrentXXWCCycleCount() {
        return mCurrentCycleCount;
    }

    /*************************************************************************
     *   NAME: setCurrentXXWCCycleCount(XXWCItemOnHand XXWCItemOnHand)
     *
     *   PURPOSE:   sets the current sequence number on the XXWCCycleCountIterator class
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
        1.0       01-JUN-2015   Lee Spitzer             Initial Version -
                                                                TMS Ticket 20150302-00173 - RF - Cyclecount   
     **************************************************************************/

    public void setCurrentXXWCCycleCount(XXWCCycleCount XXWCCycleCount) {
        mCurrentCycleCount = XXWCCycleCount;
    }
    
}
