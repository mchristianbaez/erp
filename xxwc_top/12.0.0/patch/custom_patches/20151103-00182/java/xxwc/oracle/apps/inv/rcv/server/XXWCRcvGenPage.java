/*************************************************************************
   *   $Header XXWCRcvGenPage.java $
   *   Module Name: XXWCRcvGenPage
   *   
   *   Package: package xxwc.oracle.apps.inv.rcv.server;
   *   
   *   Imports Classes:
   *   
   *   import java.sql.*;
   *   import oracle.apps.inv.utilities.server.UtilFns;
   *   import oracle.apps.mwa.beans.*;
   *   import oracle.apps.mwa.container.MWALib;
   *   import oracle.apps.mwa.container.Session;
   *   import oracle.apps.mwa.eventmodel.*;
   *   import oracle.apps.mwa.container.FileLogger;
   *   import oracle.apps.fnd.flexj.FlexException;
   *   import oracle.apps.inv.lov.server.ItemLOV;
   *   import oracle.apps.inv.lov.server.LocatorKFF;
   *   import oracle.apps.inv.lov.server.SubinventoryLOV;
   *   import xxwc.oracle.apps.inv.lov.server.*;
   *
   *   PURPOSE:   Java Class for XXWCRcvGenPage Page Layout.
   *    
   *   REVISIONS:
   *   Ver        Date        Author                     Description
   *   ---------  ----------  ---------------         -------------------------
        1.0       09-FEB-2015   Lee Spitzer             Initial Version - 
                                                          TMS Ticket 20150302-00152 RF - Receiving
        1.1       29-MAR-2016   Lee Spitzer             TMS Ticket Number 20151103-00182 - RF - UI Improvements to reduce number of clicks
**************************************************************************/

package xxwc.oracle.apps.inv.rcv.server;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.SQLException;

import oracle.apps.mwa.beans.*;
import oracle.apps.mwa.container.MWALib;
import oracle.apps.mwa.container.Session;
import oracle.apps.mwa.eventmodel.*;
import oracle.apps.mwa.container.FileLogger;
import oracle.apps.fnd.flexj.FlexException;
import oracle.apps.fnd.flexj.KeyFlexfield;
import oracle.apps.inv.utilities.server.*;
import xxwc.oracle.apps.inv.lov.server.*;

/*************************************************************************
 *   NAME: XXWCRcvGenPage extends PageBean
 *
 *   PURPOSE:   Main class for XXWCRcvGenPage
 *
 *   REVISIONS:
 *   Ver        Date        Author                     Description
 *   ---------  ----------  ---------------         -------------------------
        1.0       09-FEB-2015   Lee Spitzer             Initial Version - 
                                                          TMS Ticket 20150302-00152 RF - Receiving
 **************************************************************************/


public class XXWCRcvGenPage extends PageBean{
    /*Define objects in the class*/
    public static String WMS_TXN_SUCCESS = "Txn Successful";
    public static String WMS_TXN_ERROR = "Txn Failed. Please check log";
    public static String WMS_TXN_CANCEL = "Txn Cancelled";
    public static String WMS_LOT_INS_FAIL = "Lot Insert Failed";
    public static String WMS_SER_INS_FAIL = "Serial Insert Failed";
    public static String savenextPrompt = "";
    public static String donePrompt = "";
    public static String cancelPrompt = "";
    private boolean areThePromptsSet;
    protected XXWCPOLOV mPOFld;
    protected XXWCREQLOV mReqFld;
    protected XXWCRMALOV mRMAFld;
    protected TextFieldBean mVendorName;
    protected TextFieldBean mNoteToReceiver;
    protected XXWCPOAcceptanceDeferReasonsLOV mPOAcceptanceDeferReasonsFld;
    protected XXWCItemLOV mItemFld;
    protected TextFieldBean mItemDescription;
    protected TextFieldBean mUOMFld;
    protected TextFieldBean mQtyFld;
    protected XXWCSubLOV    mSubFld;
    protected TextFieldBean mDocType;
    protected TextFieldBean mTransactionType;
    protected TextFieldBean mGroupId;
    protected ButtonFieldBean mPrintItemLabel;
    protected XXWCInternalRTVLOV mInternalRTVFld;
    protected ButtonFieldBean mCancel;
    protected ButtonFieldBean mSaveNext;
    protected ButtonFieldBean mDone;
    protected TextFieldBean mRunningQtyFld;
    protected String m_userid;


    /*************************************************************************
     *   NAME: private static String gPackage = "xxwc.oracle.apps.inv.rcv.server";
     *
     *   PURPOSE:   private method to default gPackage value for debugging
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
            1.0       09-FEB-2015   Lee Spitzer             Initial Version - 
                                                                TMS Ticket 20150302-00152 RF - Receiving
     **************************************************************************/

    private static String gPackage = "xxwc.oracle.apps.inv.rcv.server";


    /*************************************************************************
     *   NAME: private static String gCallFrom = "XXWCRcvGenPage";
     *
     *   PURPOSE:   private method to pass gCallFrom from methods to return class name CustomListener
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
            1.0       09-FEB-2015   Lee Spitzer             Initial Version - 
                                                                TMS Ticket 20150302-00152 RF - Receiving
     **************************************************************************/

    private static String gCallFrom = "XXWCRcvGenPage";



    /*************************************************************************
     *   NAME: public XXWCRcvGenPage(Session session)
     *
     *   PURPOSE:   Inherits session from XXWCPrimaryBinAssignmentPage
     *
     *   Parameters: Session session
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
            1.0       09-FEB-2015   Lee Spitzer             Initial Version - 
                                                               TMS Ticket 20150302-00152 RF - Receiving
     **************************************************************************/

    public XXWCRcvGenPage(Session session) {
        initLayout(session);
    }

    
    /*************************************************************************
     *   NAME: private void initLayout(Session session)
     *
     *   PURPOSE:   Sets the initial layout of the page
     *
     *   Parameters: Session session
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
          1.0       09-FEB-2015   Lee Spitzer             Initial Version - 
                                                             TMS Ticket 20150302-00152 RF - Receiving
     **************************************************************************/

    private void initLayout(Session session) {
        String gMethod = "setPrompts";
        String gCallPoint = "Start";
        m_userid = "";
        WMS_TXN_SUCCESS = UtilFns.getMessage(session, "WMS", "WMS_TXN_SUCCESS");
        WMS_TXN_ERROR = UtilFns.getMessage(session, "WMS", "WMS_TXN_ERROR");
        WMS_TXN_CANCEL = UtilFns.getMessage(session, "WMS", "WMS_TXN_CANCEL");
        WMS_LOT_INS_FAIL = UtilFns.getMessage(session, "WMS", "WMS_LOT_INS_FAIL");
        WMS_SER_INS_FAIL = UtilFns.getMessage(session, "WMS", "WMS_SER_INS_FAIL");
        /*Doc Type*/
        String l_doc_type;
        String l_transaction_type;
        try {
            l_doc_type = (String)session.getObject("TXN.DOCTYPE");
            l_transaction_type = (String)session.getObject("TXN.TRANSACTION");
        }
        catch (Exception e){
            FileLogger.getSystemLogger().error(gPackage + "." + gCallFrom + " " + gMethod  + " " + gCallPoint + " error getting l_doc_type and l_transaction_type " + e);
            l_doc_type = "";
            l_transaction_type = "";
        }
        
        mPOFld = new XXWCPOLOV ();
        mPOFld.setName("XXWC.PO");
        
        mReqFld = new XXWCREQLOV ();
        mReqFld.setName("XXWC.REQ");
        
        mRMAFld = new XXWCRMALOV ();
        mRMAFld.setName("XXWC.RMA");
       
        mPOAcceptanceDeferReasonsFld = new XXWCPOAcceptanceDeferReasonsLOV();
        mPOAcceptanceDeferReasonsFld.setName("XXWC.ACCEPT_DEFER");
        mPOAcceptanceDeferReasonsFld.setRequired(true);
        mPOAcceptanceDeferReasonsFld.setHidden(true);
        
        if (l_doc_type.equals("PO")){
            mPOFld.setHidden(false);
            mReqFld.setHidden(true);
            mRMAFld.setHidden(true);
            mPOFld.setRequired(true);
            
        }
       
        if (l_doc_type.equals("REQ")){
            mPOFld.setHidden(true);
            mReqFld.setHidden(false);
            mRMAFld.setHidden(true);
            mReqFld.setRequired(true);
        }
       
        if (l_doc_type.equals("RMA")){
            mPOFld.setHidden(true);
            mReqFld.setHidden(true);
            mRMAFld.setHidden(false);
            mRMAFld.setRequired(true);
        }
        
        mVendorName = new TextFieldBean ();
        mVendorName.setName("XXWC.VENDOR_NAME");
        mVendorName.setEditable(false);
        mVendorName.setHidden(true);
        
        
        mNoteToReceiver = new TextFieldBean ();
        mNoteToReceiver.setName("XXWC.NOTE_TO_RECEIVER");
        mNoteToReceiver.setEditable(false);
        mNoteToReceiver.setHidden(true);
        
        
        /*Item*/
        mItemFld = new XXWCItemLOV("RCV");
        mItemFld.setName("XXWC.ITEM");
        mItemFld.setRequired(true);
        /*Item Description*/
        mItemDescription = new TextFieldBean();
        mItemDescription.setName("XXWC.ITEM_DESCRIPTION");
        mItemDescription.setEditable(false);
        mItemDescription.setHidden(true);
        
        mDocType = new TextFieldBean();
        mDocType.setName("XXWC.DOC_TYPE");
        mDocType.setEditable(false);
        mDocType.setHidden(true);
       
        mGroupId = new TextFieldBean();
        mGroupId.setName("XXWC.GROUP_ID");
        mGroupId.setEditable(false);
        mGroupId.setHidden(true);
         
        mTransactionType = new TextFieldBean();
        mTransactionType.setName("XXWC.TRANSACTION_TYPE");
        mTransactionType.setEditable(false);
        mTransactionType.setHidden(true);
        
        /*UOM*/
        mUOMFld = new TextFieldBean();
        mUOMFld.setName("XXWC.UOM");
        mUOMFld.setEditable(false);
        mUOMFld.setHidden(true);
        mUOMFld.setRequired(true);
        /*Qty*/
        mQtyFld = new TextFieldBean();
        mQtyFld.setName("XXWC.QTY");
        mQtyFld.setEditable(true);
        mQtyFld.setHidden(true);
        mQtyFld.setRequired(true);
        /*Total Running Qty*/
        mRunningQtyFld = new TextFieldBean();
        mRunningQtyFld.setName("XXWC.RUNNING_QTY");
        mRunningQtyFld.setEditable(false);
        mRunningQtyFld.setHidden(true);
        mRunningQtyFld.setValue("0");
        /*Sub*/
        mSubFld = new XXWCSubLOV();
        mSubFld.setName("XXWC.SUB");
        mSubFld.setRequired(true);
        mSubFld.setHidden(true);
        
        mPrintItemLabel = new ButtonFieldBean();
        mPrintItemLabel.setName("XXWC.PRINT_ITEM_LABEL");
        mPrintItemLabel.setHidden(true);
        
        /*Internal RTV*/
        mInternalRTVFld = new XXWCInternalRTVLOV ();
        mInternalRTVFld.setName("XXWC.RTV");
        mInternalRTVFld.setRequired(false);
        mInternalRTVFld.setHidden(true);
        
        /*Save Properties*/
        mSaveNext = new ButtonFieldBean();
        mSaveNext.setName("INV.SAVENEXT");
        mSaveNext.setPrompt("Next Item");
        mSaveNext.setEnableAcceleratorKey(true);
        mSaveNext.setHidden(true);
        /*Save Properties*/
        mDone = new ButtonFieldBean();
        mDone.setName("INV.DONE");
        mDone.setPrompt("Done");
        mDone.setEnableAcceleratorKey(true);
        mDone.setHidden(true);
        /*Cancel Properties*/
        mCancel = new ButtonFieldBean();
        mCancel.setName("INV.CANCEL");
        mCancel.setPrompt("Cancel");
        mCancel.setEnableAcceleratorKey(true);
        mCancel.setHidden(false);
        /*set the properties for each to the addFieldBean*/
        addFieldBean(mPOFld);
        addFieldBean(mReqFld);
        addFieldBean(mRMAFld);
        addFieldBean(mPOAcceptanceDeferReasonsFld);
        addFieldBean(mVendorName);
        addFieldBean(mNoteToReceiver);
        addFieldBean(mItemFld);
        addFieldBean(mItemDescription);
        addFieldBean(mUOMFld);
        addFieldBean(mQtyFld);
        addFieldBean(mRunningQtyFld);
        addFieldBean(mInternalRTVFld);
        addFieldBean(mSubFld);
        addFieldBean(mDocType);
        addFieldBean(mTransactionType);
        addFieldBean(mGroupId);
        addFieldBean(mPrintItemLabel);
        addFieldBean(mSaveNext);
        addFieldBean(mDone);
        addFieldBean(mCancel);
        XXWCRcvGenFListener fieldListener = new XXWCRcvGenFListener();
        /*Add Listener to each property*/
        mPOFld.addListener(fieldListener);
        mReqFld.addListener(fieldListener);
        mRMAFld.addListener(fieldListener);
        mPOAcceptanceDeferReasonsFld.addListener(fieldListener);
        mVendorName.addListener(fieldListener);
        mNoteToReceiver.addListener(fieldListener);
        mItemFld.addListener(fieldListener);
        mItemDescription.addListener(fieldListener);
        mUOMFld.addListener(fieldListener);
        mQtyFld.addListener(fieldListener);
        mRunningQtyFld.addListener(fieldListener);
        mSubFld.addListener(fieldListener);
        mDocType.addListener(fieldListener);
        mTransactionType.addListener(fieldListener);
        mGroupId.addListener(fieldListener);
        mPrintItemLabel.addListener(fieldListener);
        mInternalRTVFld.addListener(fieldListener);
        mSaveNext.addListener(fieldListener);
        mDone.addListener(fieldListener);
        mCancel.addListener(fieldListener);
        try {
            setPrompts(session);
        } catch (Exception exception) {
            session.setStatusMessage(UtilFns.getMessage(session, "WMS", "WMS_PROMPT_NOTSET"));
            FileLogger.getSystemLogger().error(gPackage + "." + gCallFrom + " " + gMethod  + " " + gCallPoint + " exception " + exception);
            
        }
    }
    
    /*************************************************************************
     *   NAME: private void initPrompts(Session session) throws SQLException
     *
     *   PURPOSE:   Sets the initial prompts of the session
     *
     *   Parameters: Session session
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
          1.0       09-FEB-2015   Lee Spitzer             Initial Version - 
                                                             TMS Ticket 20150302-00152 RF - Receiving
     **************************************************************************/

    private void initPrompts(Session session) throws SQLException {
        String gMethod = "initPrompts";
        String gCallPoint = "Start";
        try {
            FileLogger.getSystemLogger().trace(gPackage + "." + gCallFrom + " " + gMethod  + " " + gCallPoint);
            String s = (String)session.getObject("ORGCODE");
            setPrompt((new StringBuilder()).append("XXWC Receipts (").append(s).append(")").toString());
            mPOFld.setPrompt("PO Num");
            mReqFld.setPrompt("Req Num");
            mRMAFld.setPrompt("RMA Num");
            mPOAcceptanceDeferReasonsFld.setPrompt("Accpt Overide");
            mVendorName.setPrompt("Supplier");
            mNoteToReceiver.setPrompt("Notes");
            mDocType.setPrompt("DOCTYPE");
            mTransactionType.setPrompt("TRANSACTION");
            mGroupId.setPrompt("GROUP_ID");
            mItemFld.setPrompt("Item");
            mItemDescription.setPrompt("Desc");
            mUOMFld.setPrompt("UOM");
            mQtyFld.setPrompt("Qty");
            mRunningQtyFld.setPrompt("RunningQty");
            mSubFld.setPrompt("Sub");
            mPrintItemLabel.setPrompt("Item Label");
            mInternalRTVFld.setPrompt("RTV");
            mSaveNext.setPrompt("Next Item");
            //savenextPrompt = MWALib.getAKPrompt(session, "oracle.apps.inv.utilities.InvResourceTable", "INV_SAVE_NEXT_PROMPT");
            donePrompt = MWALib.getAKPrompt(session, "oracle.apps.inv.utilities.InvResourceTable", "INV_DONE_PROMPT");
            cancelPrompt = MWALib.getAKPrompt(session, "oracle.apps.inv.utilities.InvResourceTable", "INV_CANCEL_PROMPT");
            areThePromptsSet = true;
            gCallPoint = "End";
            FileLogger.getSystemLogger().trace(gPackage + "." + gCallFrom + " " + gMethod  + " " + gCallPoint);
        } catch (SQLException sqlexception) {
            session.setStatusMessage(UtilFns.getMessage(session, "WMS", "WMS_TXN_ERROR"));
            FileLogger.getSystemLogger().error(gPackage + "." + gCallFrom + " " + gMethod  + " " + gCallPoint + " sqlexception " + sqlexception);
        }
    }

    /*************************************************************************
     *   NAME: private void setPrompts(Session session)
     *
     *   PURPOSE:   Sets the set prompts of the session
     *
     *   Parameters: Session session
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
          1.0       09-FEB-2015   Lee Spitzer             Initial Version - 
                                                             TMS Ticket 20150302-00152 RF - Receiving
          1.1       29-MAR-2016   Lee Spitzer             TMS Ticket Number 20151103-00182 - RF - UI Improvements to reduce number of clicks
     **************************************************************************/

    private void setPrompts(Session session) {
            String gMethod = "setPrompts";
            String gCallPoint = "Start";
            if (!areThePromptsSet) {
                try {
                    initPrompts(session);
                } catch (SQLException sqlexception) {
                    FileLogger.getSystemLogger().error(gPackage + "." + gCallFrom + " " + gMethod  + " " + gCallPoint + " sqlexception " + sqlexception);
                }
            }
            FileLogger.getSystemLogger().trace(gPackage + "." + gCallFrom + " " + gMethod  + " " + gCallPoint);
            String s = (String)session.getObject("ORGCODE");
            setPrompt((new StringBuilder()).append("XXWC Receipts (").append(s).append(")").toString());

            //mSaveNext.setPrompt(savenextPrompt);
            mSaveNext.setPrompt("Next Item");
            mDone.setPrompt(donePrompt);
            mCancel.setPrompt(cancelPrompt);
            mSaveNext.retrieveAttributes("INVE_SAVE_NEXT_PROMPT");
            mDone.retrieveAttributes("INV_DONE_PROMPT");
            mCancel.retrieveAttributes("INV_CANCEL_PROMPT");
            //mCancel.setPrompt("Menu"); //Removed 03/29/2016 TMS Ticket 20151103-00182
            gCallPoint = "End Prompts";
            FileLogger.getSystemLogger().trace(gPackage + "." + gCallFrom + " " + gMethod  + " " + gCallPoint);
    }


    /*************************************************************************
    *   NAME: public XXWCPOLOV getmPOFld(
    *
    *   PURPOSE:   return the value of getmPrinterFld
    *
    *   REVISIONS:
    *   Ver        Date        Author                     Description
    *   ---------  ----------  ---------------         -------------------------
           1.0       09-FEB-2015   Lee Spitzer             Initial Version - 
                                                               TMS Ticket 20150302-00152 RF - Receiving
    **************************************************************************/

    public XXWCPOLOV getmPOFld() {
       return mPOFld;
    }


    /*************************************************************************
    *   NAME: public XXWCREQLOV getmPOFld(
    *
    *   PURPOSE:   return the value of getmPrinterFld
    *
    *   REVISIONS:
    *   Ver        Date        Author                     Description
    *   ---------  ----------  ---------------         -------------------------
           1.0       09-FEB-2015   Lee Spitzer             Initial Version - 
                                                               TMS Ticket 20150302-00152 RF - Receiving
    **************************************************************************/

    public XXWCREQLOV getmReqFld() {
       return mReqFld;
    }

    /*************************************************************************
    *   NAME: public XXWCRMALOV getmPOFld(
    *
    *   PURPOSE:   return the value of getmPrinterFld
    *
    *   REVISIONS:
    *   Ver        Date        Author                     Description
    *   ---------  ----------  ---------------         -------------------------
           1.0       09-FEB-2015   Lee Spitzer             Initial Version - 
                                                               TMS Ticket 20150302-00152 RF - Receiving
    **************************************************************************/

    public XXWCRMALOV getmRMAFld() {
       return mRMAFld;
    }

    /*************************************************************************
    *   NAME: public TextFieldBean getmVendorName(
    *
    *   PURPOSE:   return the value of getmPrinterFld
    *
    *   REVISIONS:
    *   Ver        Date        Author                     Description
    *   ---------  ----------  ---------------         -------------------------
           1.0       09-FEB-2015   Lee Spitzer             Initial Version - 
                                                               TMS Ticket 20150302-00152 RF - Receiving
    **************************************************************************/

    public TextFieldBean getmVendorName() {
       return mVendorName;
    }

    /*************************************************************************
    *   NAME: public TextFieldBean getmNoteToReceiver(
    *
    *   PURPOSE:   return the value of getmPrinterFld
    *
    *   REVISIONS:
    *   Ver        Date        Author                     Description
    *   ---------  ----------  ---------------         -------------------------
           1.0       09-FEB-2015   Lee Spitzer             Initial Version - 
                                                               TMS Ticket 20150302-00152 RF - Receiving
    **************************************************************************/

    public TextFieldBean getmNoteToReceiver() {
       return mNoteToReceiver;
    }

     /*************************************************************************
     *   NAME: public XXWCItemLOV getmItemFld(
     *
     *   PURPOSE:   return the value of getmPrinterFld
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
            1.0       09-FEB-2015   Lee Spitzer             Initial Version - 
                                                                TMS Ticket 20150302-00152 RF - Receiving
     **************************************************************************/

    public XXWCItemLOV getmItemFld() {
        return mItemFld;
    }

    /*************************************************************************
    *   NAME: public TextFieldBean getmItemDescription(
    *
    *   PURPOSE:   return the value of getmPrinterFld
    *
    *   REVISIONS:
    *   Ver        Date        Author                     Description
    *   ---------  ----------  ---------------         -------------------------
           1.0       09-FEB-2015   Lee Spitzer             Initial Version - 
                                                               TMS Ticket 20150302-00152 RF - Receiving
    **************************************************************************/

    public TextFieldBean getmItemDescriptionFld() {
       return mItemDescription;
    }

    /*************************************************************************
    *   NAME: public TextFieldBean getmUOMFld(
    *
    *   PURPOSE:   return the value of mUOMFld
    *
    *   REVISIONS:
    *   Ver        Date        Author                     Description
    *   ---------  ----------  ---------------         -------------------------
           1.0       09-FEB-2015   Lee Spitzer             Initial Version - 
                                                               TMS Ticket 20150302-00152 RF - Receiving
    **************************************************************************/

    public TextFieldBean getmUOMFld() {
        return mUOMFld;
    }

    /*************************************************************************
    *   NAME: public TextFieldBean getmQty(
    *
    *   PURPOSE:   return the value of mQtyFlds
    *
    *   REVISIONS:
    *   Ver        Date        Author                     Description
    *   ---------  ----------  ---------------         -------------------------
           1.0       09-FEB-2015   Lee Spitzer             Initial Version - 
                                                               TMS Ticket 20150302-00152 RF - Receiving
    **************************************************************************/
    
    public TextFieldBean getmQty() {
        return mQtyFld;
    }

    /*************************************************************************
    *   NAME: public TextFieldBean getmRunningQty(
    *
    *   PURPOSE:   return the value of mQtyFlds
    *
    *   REVISIONS:
    *   Ver        Date        Author                     Description
    *   ---------  ----------  ---------------         -------------------------
           1.0       09-FEB-2015   Lee Spitzer             Initial Version - 
                                                               TMS Ticket 20150302-00152 RF - Receiving
    **************************************************************************/
    
    public TextFieldBean getmRunningQty() {
        return mRunningQtyFld;
    }


    /*************************************************************************
    *   NAME: public XXWCSubLOV XXWCSubLOV(
    *
    *   PURPOSE:   return the value of mSubFld
    *
    *   REVISIONS:
    *   Ver        Date        Author                     Description
    *   ---------  ----------  ---------------         -------------------------
           1.0       09-FEB-2015   Lee Spitzer             Initial Version - 
                                                               TMS Ticket 20150302-00152 RF - Receiving
    **************************************************************************/
    
    public XXWCSubLOV getmSubFld() {
        return mSubFld;
    }

    /*************************************************************************
    *   NAME: public TextFieldBean getmDocType(
    *
    *   PURPOSE:   return the value of mDocType
    *
    *   REVISIONS:
    *   Ver        Date        Author                     Description
    *   ---------  ----------  ---------------         -------------------------
           1.0       09-FEB-2015   Lee Spitzer             Initial Version - 
                                                               TMS Ticket 20150302-00152 RF - Receiving
    **************************************************************************/
    
    public TextFieldBean getmDocType() {
        return mDocType;
    }

    /*************************************************************************
    *   NAME: public TextFieldBean getmTransactionType(
    *
    *   PURPOSE:   return the value of mTransactionType
    *
    *   REVISIONS:
    *   Ver        Date        Author                     Description
    *   ---------  ----------  ---------------         -------------------------
           1.0       09-FEB-2015   Lee Spitzer             Initial Version - 
                                                               TMS Ticket 20150302-00152 RF - Receiving
    **************************************************************************/
    
    public TextFieldBean getmTransactionType() {
        return mTransactionType;
    }

    /*************************************************************************
    *   NAME: public TextFieldBean getmGroupId(
    *
    *   PURPOSE:   return the value of mGroupId
    *
    *   REVISIONS:
    *   Ver        Date        Author                     Description
    *   ---------  ----------  ---------------         -------------------------
           1.0       09-FEB-2015   Lee Spitzer             Initial Version - 
                                                               TMS Ticket 20150302-00152 RF - Receiving
    **************************************************************************/

    public TextFieldBean getmGroupId() {
        return mGroupId;
    }
    

    /*************************************************************************
    *   NAME: public XXWCPOAcceptanceDeferReasonsLOV getmPOAcceptanceDeferReasonsFld(
    *
    *   PURPOSE:   return the value of mPOAcceptanceDeferReasonsFld
    *
    *   REVISIONS:
    *   Ver        Date        Author                     Description
    *   ---------  ----------  ---------------         -------------------------
           1.0       09-FEB-2015   Lee Spitzer             Initial Version - 
                                                               TMS Ticket 20150302-00152 RF - Receiving
    **************************************************************************/

    public XXWCPOAcceptanceDeferReasonsLOV  getmPOAcceptanceDeferReasonsFld() {
        return mPOAcceptanceDeferReasonsFld;
    }

    /*************************************************************************
    *   NAME: public ButtonFieldBean getmPrintItemLabel(
    *
    *   PURPOSE:   return the value of mPrintItemLabel
    *
    *   REVISIONS:
    *   Ver        Date        Author                     Description
    *   ---------  ----------  ---------------         -------------------------
           1.0       09-FEB-2015   Lee Spitzer             Initial Version - 
                                                               TMS Ticket 20150302-00152 RF - Receiving
    **************************************************************************/

    public ButtonFieldBean getmPrintItemLabel() {
        return mPrintItemLabel;
    }
    
    /*************************************************************************
     *   NAME: public ButtonFieldBean getmCancel
     *
     *   PURPOSE:   return the value of mCancel
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
            1.0       09-FEB-2015   Lee Spitzer             Initial Version - 
                                                                TMS Ticket 20150302-00152 RF - Receiving
     **************************************************************************/

    public ButtonFieldBean getmCancel() {
        return mCancel;
    }

    /*************************************************************************
     *   NAME: public ButtonFieldBean getmSaveNext()
     *
     *   PURPOSE:   return the value of mSaveNext
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
             1.0       09-FEB-2015   Lee Spitzer             Initial Version - 
                                                                TMS Ticket 20150302-00152 RF - Receiving
     **************************************************************************/

    public ButtonFieldBean getmSaveNext() {
        return mSaveNext;
    }

    /*************************************************************************
     *   NAME: public ButtonFieldBean getmDone()
     *
     *   PURPOSE:   return the value of mDone
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
             1.0       09-FEB-2015   Lee Spitzer             Initial Version - 
                                                                TMS Ticket 20150302-00152 RF - Receiving
     **************************************************************************/

    public ButtonFieldBean getmDone() {
        return mDone;
    }

    /*************************************************************************
     *   NAME: public ButtonFieldBean getmInternalRTVFld()
     *
     *   PURPOSE:   return the value of mDone
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
             1.0       09-FEB-2015   Lee Spitzer             Initial Version - 
                                                                TMS Ticket 20150302-00152 RF - Receiving
     **************************************************************************/

    public XXWCInternalRTVLOV getmInternalRTVFld() {
        return mInternalRTVFld;
    }


    /*************************************************************************
     *   NAME: public String getUserId()
     *
     *   PURPOSE:   return the value of m_userid
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
             1.0       09-FEB-2015   Lee Spitzer             Initial Version - 
                                                                TMS Ticket 20150302-00152 RF - Receiving
     **************************************************************************/

    public String getUserId() {
        return m_userid;
    }

    /*************************************************************************
     *   NAME: void pageEntered(MWAEvent e) throws AbortHandlerException, InterruptedHandlerException,
                                               DefaultOnlyHandlerException {
     *
     *   PURPOSE:   pageEntered method
     *
     *   Parameters: MWAEvent mwaevent
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
             1.0       09-FEB-2015   Lee Spitzer             Initial Version - 
                                                                TMS Ticket 20150302-00152 RF - Receiving
     **************************************************************************/

    public void pageEntered(MWAEvent e) throws AbortHandlerException, InterruptedHandlerException,
                                               DefaultOnlyHandlerException {
        Session session = e.getSession();
        
        
    }

    /*************************************************************************
     *   NAME: void pageExited(MWAEvent e) throws AbortHandlerException, InterruptedHandlerException,
                                               DefaultOnlyHandlerException {
     *
     *   PURPOSE:   pageExited method
     *
     *
     *   Parameters: MWAEvent   mwaevent
     *
     *   REVISIONS:
     *   Ver        Date        Author                     Description
     *   ---------  ----------  ---------------         -------------------------
             1.0       09-FEB-2015   Lee Spitzer             Initial Version - 
                                                                TMS Ticket 20150302-00152 RF - Receiving

     **************************************************************************/

    public void pageExited(MWAEvent e) throws AbortHandlerException, InterruptedHandlerException,
                                              DefaultOnlyHandlerException {
        Session session = e.getSession();
        
    }
   
}
