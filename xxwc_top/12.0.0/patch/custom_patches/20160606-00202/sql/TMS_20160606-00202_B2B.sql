/*************************************************************************
  $Header TMS_20160606-00202_B2B.sql $
  Module Name: TMS_20160606-00202  Data Fix script for B2B 

  PURPOSE: Data Fix script for B2B

  REVISIONS:
  Ver        Date         Author                Description
  ---------  -----------  ------------------    --------------------------
  1.0        06-JUN-2016  Gopi Damuluri         TMS#20160606-00202 
**************************************************************************/ 
SET SERVEROUTPUT ON SIZE 1000000
DECLARE
BEGIN
   DBMS_OUTPUT.put_line ('TMS: 20160606-00202    , Before Update');

UPDATE xxwc.xxwc_b2b_cust_info_tbl
   SET notify_account_mgr = 'N'
 WHERE notify_account_mgr = 'Y';

   DBMS_OUTPUT.put_line ('TMS: 20160606-00202  Sales order lines updated (Expected:57): '|| SQL%ROWCOUNT);

   COMMIT;

   DBMS_OUTPUT.put_line ('TMS: 20160606-00202    , End Update');
EXCEPTION
   WHEN OTHERS
   THEN
      ROLLBACK;
      DBMS_OUTPUT.put_line ('TMS: 20160606-00202 , Errors : ' || SQLERRM);
END;
/