  set serveroutput on;
 /*************************************************************************
      PURPOSE:  Receiving Error Message on LT Rental order

      REVISIONS:
      Ver        Date          Author             Description
      ---------  ------------  ------------       ------------------
      1.0        27-Nov-2017  Sundaramoorthy     Initial Version - TMS #20170810-00186 
	************************************************************************/ 
 BEGIN
 dbms_output.put_line ('Start Update ');

 UPDATE oe_order_lines_all
  SET  flow_status_code ='CLOSED'
  , open_flag ='N'
  WHERE line_id = 91932115
  AND header_id =51272481;  
  COMMIT;
  
  dbms_output.put_line ('Update count '||SQL%ROWCOUNT);
  EXCEPTION
  WHEN OTHERS THEN
    ROLLBACK;
	dbms_output.put_line ('Inside Exception '|| SUBSTR(SQLERRM,1,2000));	
END;
/	