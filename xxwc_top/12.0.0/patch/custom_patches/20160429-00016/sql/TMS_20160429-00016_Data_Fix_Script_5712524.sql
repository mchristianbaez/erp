/*
 TMS: 20160225-00024       
 Date: 02/25/2016
 Notes: data fix script to process month end transactions
*/

SET SERVEROUTPUT ON SIZE 100000;
SET VERIFY OFF;

begin 

UPDATE wsh_delivery_details 
SET released_status = 'D', 
src_requested_quantity = 0, 
requested_quantity = 0, 
shipped_quantity = 0, 
cycle_count_quantity = 0, 
cancelled_quantity = 0, 
subinventory = null, 
locator_id = null, 
lot_number = null, 
revision = null, 
inv_interfaced_flag = 'X', 
oe_interfaced_flag = 'X' 
WHERE delivery_detail_id =15595791 ; 

--1 row expected to be updated

update apps.wsh_delivery_assignments
set delivery_id = null,
parent_delivery_detail_id = null
where delivery_detail_id = 15595791;


--1 row expected to be updated

update apps.oe_order_lines_all
set flow_status_code='CANCELLED',
cancelled_flag='Y'
where line_id=69471513
and headeR_id=42427117;

--1 row expected to be updated
end;
/