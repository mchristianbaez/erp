create or replace
PACKAGE BODY       XXEIS.EIS_RS_XXWC_COM_UTIL_PKG
AS
   /************************************************************************
     $Header eis_rs_xxwc_com_util_pkg $
     Module Name: EIS_RS_XXWC_COM_UTIL_PKG.pkb

     PURPOSE:   This package holds the utilities for EIS Reports

     REVISIONS:
     Ver        Date        Author             Description
     ---------  ----------  ---------------    -------------------------
     1.0        01/11/2012  EIS                Initial Version
     1.1        02/28/2014  Gopi Damuluri      TMS# 20140228-00050 Tuning of White Cap Direct Processing Report- slow running in EBIZPRD
                                               Added the procedure - WCD_PRE_TRIG
     1.2        03/10/2014  Harsha Yedla       TMS#20130920-00347
                                               Added the procedures - get_doc_date_modified,xxwc_open_sales_order_pre_trig
     1.3        03/10/2014  Harsha Yedla       TMS#20131219-00105
                                 Added the procedure - xxwc_order_exception_pre_trig
     1.4        03/17/2014  Harsha Yedla       TMS#20140317-00281 Error in Open Sales Orders Report
     1.5        03/19/2014  Gopi Damuluri      TMS#20140317-00281 SalesRep Issue in Open Sales Orders Report
     1.6        03/26/2014  Gopi Damuluri      TMS# 20140326-00106
                                               Commented the procedure xxwc_open_sales_order_pre_trig
     1.7        03/27/2014   Anil Mulagiri      Added the funtion - get_item_acc
     1.9        05/20/2014   Mahender Reddy     Added the funtion - get_item_onhand_qty
     1.10       11/09/2014   Kathy Poling       Added column names print log for insert procedure xxwc_order_exception_pre_trig
                                                ESMS 269621
     1.11       03/11/2015   Mahesh Kudle       Modified for performance issues TMS#20150226-00193
     1.12       06/29/2015  Mahender Reddy      Added the function - xxwc_inv_get_born_date for TMS#20150227-00171
	 1.13       09/25/2015  Mahender Reddy		Added Inventory_Item_id in wcd_pre_trig for TMS#20150827-00075 
	 1.14       03/04/2016  Mahender Reddy		Modified Function get_ytd_sales for TMS# 20160301-00067
     1.15       09/25/2015  Manjula Chellappan  TMS #20150622-00156 - Shipping Extension modification for PUBD	
   **************************************************************************/
   FUNCTION get_period_name (p_ledger_id NUMBER, p_gl_date DATE)
      RETURN VARCHAR2
   IS
      l_period_name   VARCHAR2 (20);
   BEGIN
      SELECT period_name
        INTO l_period_name
        FROM gl_periods gp, gl_ledgers gl
       WHERE     gp.period_set_name = gl.period_set_name
             AND gl.ledger_id = p_ledger_id
             AND p_gl_date BETWEEN gp.start_date AND gp.end_date;

      RETURN l_period_name;
   EXCEPTION
      WHEN OTHERS
      THEN
         RETURN NULL;
   END;

   FUNCTION get_vendor_name (p_inventory_item_id    NUMBER,
                             p_organization_id      NUMBER)
      RETURN VARCHAR2
   IS
      l_vendor_name   VARCHAR2 (240);
   BEGIN
      SELECT MAX (pov.vendor_name)
        INTO l_vendor_name
        FROM mrp_sr_assignments ass,
             mrp_sr_receipt_org rco,
             mrp_sr_source_org sso,
             po_vendors pov
       WHERE     1 = 1
             AND ass.inventory_item_id(+) = p_inventory_item_id
             AND ass.organization_id(+) = p_organization_id
             AND rco.sourcing_rule_id(+) = ass.sourcing_rule_id
             AND sso.sr_receipt_id(+) = rco.sr_receipt_id
             AND pov.vendor_id(+) = sso.vendor_id;

      RETURN l_vendor_name;
   EXCEPTION
      WHEN OTHERS
      THEN
         RETURN NULL;
   END;


   FUNCTION get_vendor_number (p_inventory_item_id    NUMBER,
                               p_organization_id      NUMBER)
      RETURN VARCHAR2
   IS
      l_vendor_number   VARCHAR2 (240);
   BEGIN
      SELECT MAX (pov.segment1)
        INTO l_vendor_number
        FROM mrp_sr_assignments ass,
             mrp_sr_receipt_org rco,
             mrp_sr_source_org sso,
             po_vendors pov
       WHERE     1 = 1
             AND ass.inventory_item_id(+) = p_inventory_item_id
             AND ass.organization_id(+) = p_organization_id
             AND rco.sourcing_rule_id(+) = ass.sourcing_rule_id
             AND sso.sr_receipt_id(+) = rco.sr_receipt_id
             AND pov.vendor_id(+) = sso.vendor_id;

      RETURN l_vendor_number;
   EXCEPTION
      WHEN OTHERS
      THEN
         RETURN NULL;
   END;

   FUNCTION get_vendor_name (p_line_id NUMBER)
      RETURN VARCHAR2
   IS
      l_vendor_name   VARCHAR2 (240);
   BEGIN
      SELECT MAX (pov.vendor_name)
        INTO l_vendor_name
        FROM mrp_sr_assignments ass,
             mrp_sr_receipt_org rco,
             mrp_sr_source_org sso,
             po_vendors pov,
             oe_order_lines ol
       WHERE     1 = 1
             AND ol.line_id = p_line_id
             AND ass.inventory_item_id = ol.inventory_item_id
             AND ass.organization_id = ol.ship_from_org_id
             AND rco.sourcing_rule_id(+) = ass.sourcing_rule_id
             AND sso.sr_receipt_id(+) = rco.sr_receipt_id
             AND pov.vendor_id(+) = sso.vendor_id;

      RETURN l_vendor_name;
   EXCEPTION
      WHEN OTHERS
      THEN
         RETURN NULL;
   END;


   FUNCTION get_vendor_number (p_line_id NUMBER)
      RETURN VARCHAR2
   IS
      l_vendor_number   VARCHAR2 (240);
   BEGIN
      SELECT MAX (pov.segment1)
        INTO l_vendor_number
        FROM mrp_sr_assignments ass,
             mrp_sr_receipt_org rco,
             mrp_sr_source_org sso,
             po_vendors pov,
             oe_order_lines ol
       WHERE     1 = 1
             AND ol.line_id = p_line_id
             AND ass.inventory_item_id = ol.inventory_item_id
             AND ass.organization_id = ol.ship_from_org_id
             AND rco.sourcing_rule_id(+) = ass.sourcing_rule_id
             AND sso.sr_receipt_id(+) = rco.sr_receipt_id
             AND pov.vendor_id(+) = sso.vendor_id;

      RETURN l_vendor_number;
   EXCEPTION
      WHEN OTHERS
      THEN
         RETURN NULL;
   END;


   FUNCTION get_beg_inv (p_inventory_item_id    NUMBER,
                         p_organization_id      NUMBER,
                         p_date                 DATE)
      RETURN NUMBER
   IS
      l_beg_inv   NUMBER;
   BEGIN
      SELECT SUM (moq.transaction_quantity)
        INTO l_beg_inv
        FROM mtl_onhand_quantities_detail moq
       WHERE     TRUNC (moq.date_received) <= p_date
             AND inventory_item_id = p_inventory_item_id
             AND organization_id = p_organization_id
             AND moq.owning_tp_type = 1;

      RETURN l_beg_inv;
   EXCEPTION
      WHEN OTHERS
      THEN
         RETURN NULL;
   END;


   FUNCTION get_end_inv (p_inventory_item_id    NUMBER,
                         p_organization_id      NUMBER,
                         p_date                 DATE)
      RETURN NUMBER
   IS
      l_end_inv   NUMBER;
   BEGIN
      SELECT SUM (moq.transaction_quantity)
        INTO l_end_inv
        FROM mtl_onhand_quantities_detail moq
       WHERE     1 = 1
             AND TRUNC (moq.date_received) <= p_date
             AND inventory_item_id = p_inventory_item_id
             AND organization_id = p_organization_id
             AND moq.owning_tp_type = 1;

      RETURN l_end_inv;
   EXCEPTION
      WHEN OTHERS
      THEN
         RETURN NULL;
   END;


   FUNCTION get_turns (p_inventory_item_id    NUMBER,
                       p_organization_id      NUMBER,
                       p_start_date           DATE,
                       p_end_date             DATE)
      RETURN NUMBER
   IS
      l_cogs_amt   NUMBER;
      l_min_date   DATE;
      l_end_date   DATE;
      l_turns      NUMBER;
      l_avg_inv    NUMBER;
   BEGIN
      BEGIN
         IF g_vdate IS NULL
         THEN
            SELECT MIN (vdate)
              INTO g_vdate
              FROM xxeis.eis_xxwc_inv_val_dm_tab;
         END IF;
      EXCEPTION
         WHEN OTHERS
         THEN
            g_vdate := NULL;
      END;

      IF g_vdate > p_end_date
      THEN
         l_end_date := g_vdate;
      END IF;

      BEGIN
         SELECT   NVL (
                     SUM (
                          (  (  ol.ordered_quantity
                              * DECODE (ol.line_category_code,
                                        'RETURN', -1,
                                        1))
                           - NVL (ol.cancelled_quantity, 0))
                        * NVL (ol.unit_cost, 0)),
                     0)
                * 4
           INTO l_cogs_amt
           FROM oe_order_headers oh, oe_order_lines ol
          WHERE     oh.header_id = ol.header_id
                AND TRUNC (oh.booked_date) BETWEEN p_start_date
                                               AND NVL (l_end_date,
                                                        p_end_date)
                AND ol.inventory_item_id = p_inventory_item_id
                AND ol.ship_from_org_id = p_organization_id
                AND oh.flow_status_code IN ('BOOKED', 'CLOSED', 'CANCELLED');
      EXCEPTION
         WHEN OTHERS
         THEN
            l_cogs_amt := 0;
      END;

      BEGIN
         SELECT AVG (NVL (ohqty, 0) * NVL (unit_cost, 0))
           INTO l_avg_inv
           FROM xxeis.eis_xxwc_inv_val_dm_tab
          WHERE vdate BETWEEN p_start_date AND NVL (l_end_date, p_end_date);

         l_turns := l_cogs_amt / l_avg_inv;
      EXCEPTION
         WHEN OTHERS
         THEN
            l_turns := 0;
      END;


      RETURN NVL (l_turns, 0);
   EXCEPTION
      WHEN OTHERS
      THEN
         RETURN 0;
   END;

   FUNCTION get_turns (p_inventory_item_id NUMBER, p_organization_id NUMBER)
      RETURN NUMBER
   IS
      l_turns   NUMBER;
   BEGIN
      SELECT CASE
                WHEN exitd.avg_inv != 0
                THEN
                   TO_NUMBER (exitd.cogs_amt / exitd.avg_inv)
                ELSE
                   0
             END
                turns
        INTO l_turns
        FROM xxeis.eis_xxwc_items_turns_data exitd
       WHERE     exitd.inventory_item_id = p_inventory_item_id
             AND exitd.organization_id = p_organization_id;

      RETURN NVL (l_turns, 0);
   EXCEPTION
      WHEN OTHERS
      THEN
         RETURN 0;
   END;


   FUNCTION get_cogs (p_inventory_item_id    NUMBER,
                      p_organization_id      NUMBER,
                      p_start_date           DATE,
                      p_end_date             DATE)
      RETURN NUMBER
   IS
      l_cogs_amt   NUMBER;
   BEGIN
      SELECT NVL (
                SUM (
                     (  (  ol.ordered_quantity
                         * DECODE (ol.line_category_code, 'RETURN', -1, 1))
                      - NVL (ol.cancelled_quantity, 0))
                   * NVL (ol.unit_cost, 0)),
                0)
        INTO l_cogs_amt
        FROM oe_order_headers oh, oe_order_lines ol
       WHERE     oh.header_id = ol.header_id
             AND TRUNC (oh.booked_date) BETWEEN p_start_date AND p_end_date
             AND ol.inventory_item_id = p_inventory_item_id
             AND ol.ship_from_org_id = p_organization_id
             AND oh.flow_status_code NOT IN ('ENTERED');

      RETURN l_cogs_amt;
   EXCEPTION
      WHEN OTHERS
      THEN
         RETURN 0;
   END;


   FUNCTION get_cogs_amt (p_inventory_item_id    NUMBER,
                          p_organization_id      NUMBER,
                          p_start_date           DATE,
                          p_end_date             DATE)
      RETURN NUMBER
   IS
      l_cogs_amt   NUMBER;
   BEGIN
      SELECT NVL (
                SUM (
                     ol.ordered_quantity
                   * DECODE (ol.line_category_code, 'RETURN', -1, 1)
                   * NVL (ol.unit_selling_price, 0)),
                0)
        INTO l_cogs_amt
        FROM oe_order_headers oh, oe_order_lines ol
       WHERE     oh.header_id = ol.header_id
             AND TRUNC (oh.ordered_date) BETWEEN p_start_date AND p_end_date
             AND ol.inventory_item_id = p_inventory_item_id
             AND ol.ship_from_org_id = p_organization_id
             AND oh.flow_status_code NOT IN ('CANCELLED')
             AND ol.flow_status_code NOT IN ('CANCELLED');


      RETURN l_cogs_amt;
   EXCEPTION
      WHEN OTHERS
      THEN
         RETURN 0;
   END;

   FUNCTION get_item_selling_price (p_inventory_item_id    NUMBER,
                                    p_org_id               NUMBER)
      RETURN NUMBER
   IS
      l_item_price   NUMBER;
   BEGIN
      SELECT NVL (unit_selling_price, 0)
        INTO l_item_price
        FROM ra_customer_trx_lines_all
       WHERE customer_trx_line_id IN (SELECT MAX (customer_trx_line_id)
                                        FROM ra_customer_trx_lines_all rctl
                                       WHERE     rctl.inventory_item_id =
                                                    p_inventory_item_id
                                             AND org_id = p_org_id);

      RETURN l_item_price;
   EXCEPTION
      WHEN OTHERS
      THEN
         RETURN NULL;
   END;



   FUNCTION get_last_order_num (p_inventory_item_id    NUMBER,
                                p_customer_id          NUMBER)
      RETURN NUMBER
   IS
      l_order_num   NUMBER;
   BEGIN
      SELECT interface_header_attribute1
        INTO l_order_num
        FROM ra_customer_trx
       WHERE customer_trx_id IN (SELECT MAX (rct.customer_trx_id)
                                   FROM ra_customer_trx rct,
                                        ra_customer_trx_lines rctl,
                                        oe_order_lines ol,
                                        oe_order_lines oll,
                                        mtl_system_items_b msi
                                  WHERE     bill_to_customer_id =
                                               p_customer_id
                                        AND rct.interface_header_attribute1
                                               IS NOT NULL
                                        AND rct.interface_header_context =
                                               'ORDER ENTRY'
                                        AND TO_CHAR (oll.line_id) =
                                               rctl.interface_line_attribute6
                                        AND oll.link_to_line_id = ol.line_id
                                        AND ol.inventory_item_id =
                                               p_inventory_item_id
                                        AND oll.inventory_item_id =
                                               msi.inventory_item_id
                                        AND oll.ship_from_org_id =
                                               msi.organization_id
                                        AND msi.segment1 = 'Rental Charge'
                                        AND rctl.customer_trx_id =
                                               rct.customer_trx_id --and oll.inventory_item_id             = p_inventory_item_id
                                                                  );


      RETURN l_order_num;
   EXCEPTION
      WHEN OTHERS
      THEN
         RETURN NULL;
   END;


   FUNCTION get_dist_invoice_num (p_customer_trx_id    NUMBER,
                                  p_invoice_number     VARCHAR2,
                                  p_order_type         VARCHAR2)
      RETURN VARCHAR2
   IS
      l_invoice_num   VARCHAR2 (240);
      l_amount        NUMBER;
   BEGIN
      IF p_order_type IN ('WC LONG TERM RENTAL', 'WC SHORT TERM RENTAL')
      THEN
         SELECT SUM (extended_amount)
           INTO l_amount
           FROM ra_customer_trx_lines_all rctl
          WHERE rctl.customer_trx_id = p_customer_trx_id;

         IF l_amount <> 0
         THEN
            l_invoice_num := p_invoice_number;
         ELSE
            l_invoice_num := NULL;
         END IF;
      ELSE
         l_invoice_num := p_invoice_number;
      END IF;

      RETURN l_invoice_num;
   EXCEPTION
      WHEN OTHERS
      THEN
         RETURN NULL;
   END;

   FUNCTION get_account_type_code (p_transaction_id    NUMBER,
                                   p_account_id        NUMBER)
      RETURN VARCHAR2
   IS
      l_account_line_code   VARCHAR2 (240);
   BEGIN
      SELECT MAX (lu1.meaning)
        INTO l_account_line_code
        FROM mtl_transaction_accounts mta, mfg_lookups lu1
       WHERE     lu1.lookup_type = 'CST_ACCOUNTING_LINE_TYPE'
             AND lu1.lookup_code = mta.accounting_line_type
             AND mta.transaction_id = p_transaction_id
             AND mta.accounting_line_type IN ('35', '36');

      --AND mta.reference_account = p_account_id;


      RETURN l_account_line_code;
   EXCEPTION
      WHEN OTHERS
      THEN
         RETURN NULL;
   END;


   FUNCTION get_last_invoice_num (p_inventory_item_id    NUMBER,
                                  p_customer_id          NUMBER,
                                  p_header_id            NUMBER)
      RETURN VARCHAR2
   IS
      l_trx_number   VARCHAR2 (240);
      l_trx_date     DATE;
   BEGIN
      IF    g_inv_rental_tab.inventory_item_id IS NULL
         OR (    g_inv_rental_tab.inventory_item_id != p_inventory_item_id
             AND g_inv_rental_tab.header_id != p_header_id)
      THEN
         SELECT TRUNC (trx_date), trx_number
           INTO l_trx_date, l_trx_number
           FROM ra_customer_trx
          WHERE customer_trx_id IN (SELECT MAX (rct.customer_trx_id)
                                      FROM ra_customer_trx rct,
                                           ra_customer_trx_lines rctl,
                                           oe_order_lines ol,
                                           oe_order_lines oll,
                                           mtl_system_items_b msi
                                     WHERE     bill_to_customer_id =
                                                  p_customer_id
                                           AND rct.interface_header_attribute1
                                                  IS NOT NULL
                                           AND rct.interface_header_context =
                                                  'ORDER ENTRY' --and rctl.interface_line_context      ='ORDER ENTRY'
                                           AND TO_CHAR (oll.line_id) =
                                                  rctl.interface_line_attribute6
                                           AND oll.link_to_line_id =
                                                  ol.line_id
                                           AND ol.header_id = p_header_id
                                           AND ol.inventory_item_id =
                                                  p_inventory_item_id
                                           AND oll.inventory_item_id =
                                                  msi.inventory_item_id
                                           AND oll.ship_from_org_id =
                                                  msi.organization_id
                                           AND msi.segment1 = 'Rental Charge'
                                           AND rctl.customer_trx_id =
                                                  rct.customer_trx_id --and oll.inventory_item_id             = p_inventory_item_id
                                                                     );

         g_inv_rental_tab.trx_date := l_trx_date;
         g_inv_rental_tab.trx_number := l_trx_number;
         g_inv_rental_tab.inventory_item_id := p_inventory_item_id;
         g_inv_rental_tab.header_id := p_header_id;
         g_inv_rental_tab.customer_id := p_customer_id;
      ELSE
         l_trx_number := g_inv_rental_tab.trx_number;
      END IF;


      RETURN l_trx_number;
   EXCEPTION
      WHEN OTHERS
      THEN
         RETURN NULL;
   END;

   FUNCTION get_rental_item (p_line_id NUMBER)
      RETURN VARCHAR2
   IS
      l_item   VARCHAR2 (240);
   BEGIN
      SELECT ordered_item
        INTO l_item
        FROM oe_order_lines
       WHERE line_id = p_line_id;


      RETURN l_item;
   EXCEPTION
      WHEN OTHERS
      THEN
         RETURN NULL;
   END;


   FUNCTION get_rental_item_desc (p_line_id NUMBER)
      RETURN VARCHAR2
   IS
      l_item_desc   VARCHAR2 (240);
   BEGIN
      SELECT msi.description
        INTO l_item_desc
        FROM oe_order_lines ol, mtl_system_items_b msi
       WHERE     line_id = p_line_id
             AND msi.inventory_item_id = ol.inventory_item_id
             AND msi.organization_id = ol.ship_from_org_id;

      RETURN l_item_desc;
   EXCEPTION
      WHEN OTHERS
      THEN
         RETURN NULL;
   END;



   FUNCTION get_last_billed_date (p_inventory_item_id    NUMBER,
                                  p_customer_id          NUMBER,
                                  p_header_id            NUMBER)
      RETURN DATE
   IS
      l_trx_date     DATE;
      l_trx_number   VARCHAR2 (30);
   BEGIN
      IF    g_inv_rental_tab.inventory_item_id IS NULL
         OR (    g_inv_rental_tab.inventory_item_id != p_inventory_item_id
             AND g_inv_rental_tab.header_id != p_header_id)
      THEN
         SELECT TRUNC (trx_date), trx_number
           INTO l_trx_date, l_trx_number
           FROM ra_customer_trx
          WHERE customer_trx_id IN (SELECT MAX (rct.customer_trx_id)
                                      FROM ra_customer_trx rct,
                                           ra_customer_trx_lines rctl,
                                           oe_order_lines ol,
                                           oe_order_lines oll,
                                           mtl_system_items_b msi
                                     WHERE     bill_to_customer_id =
                                                  p_customer_id
                                           AND rct.interface_header_attribute1
                                                  IS NOT NULL
                                           AND rct.interface_header_context =
                                                  'ORDER ENTRY'
                                           AND TO_CHAR (oll.line_id) =
                                                  rctl.interface_line_attribute6
                                           AND oll.link_to_line_id =
                                                  ol.line_id
                                           AND ol.header_id = p_header_id
                                           AND ol.inventory_item_id =
                                                  p_inventory_item_id
                                           AND oll.inventory_item_id =
                                                  msi.inventory_item_id
                                           AND oll.ship_from_org_id =
                                                  msi.organization_id
                                           AND msi.segment1 = 'Rental Charge'
                                           AND rctl.customer_trx_id =
                                                  rct.customer_trx_id --and oll.inventory_item_id             = p_inventory_item_id
                                                                     );

         g_inv_rental_tab.trx_date := l_trx_date;
         g_inv_rental_tab.trx_number := l_trx_number;
         g_inv_rental_tab.inventory_item_id := p_inventory_item_id;
         g_inv_rental_tab.header_id := p_header_id;
         g_inv_rental_tab.customer_id := p_customer_id;
      ELSE
         l_trx_date := g_inv_rental_tab.trx_date;
      END IF;


      RETURN l_trx_date;
   EXCEPTION
      WHEN OTHERS
      THEN
         RETURN NULL;
   END;


   FUNCTION get_new_selling_price (p_inventory_item_id    NUMBER,
                                   p_organization_id      NUMBER,
                                   p_customer_id          NUMBER)
      RETURN NUMBER
   IS
      l_selling_price   NUMBER;
   BEGIN
      SELECT NVL (unit_selling_price, 0)
        INTO l_selling_price
        FROM ra_customer_trx_lines
       WHERE customer_trx_line_id IN (SELECT MAX (header_id)
                                        FROM oe_order_lines oel
                                       WHERE     oel.inventory_item_id =
                                                    p_inventory_item_id
                                             AND oel.ship_from_org_id =
                                                    p_organization_id
                                             AND oel.sold_to_org_id =
                                                    p_customer_id);


      RETURN l_selling_price;
   EXCEPTION
      WHEN OTHERS
      THEN
         RETURN NULL;
   END;

   FUNCTION get_list_price (p_inventory_item_id    NUMBER,
                            p_organization_id      NUMBER)
      RETURN NUMBER
   IS
      l_list_price   NUMBER;
   BEGIN
      SELECT NVL (unit_list_price, 0)
        INTO l_list_price
        FROM oe_order_lines
       WHERE line_id IN (SELECT MAX (oel.line_id)
                           FROM oe_order_lines oel
                          WHERE     oel.inventory_item_id =
                                       p_inventory_item_id
                                AND oel.ship_from_org_id = p_organization_id);


      RETURN l_list_price;
   EXCEPTION
      WHEN OTHERS
      THEN
         RETURN NULL;
   END;

   FUNCTION get_last_list_price (p_inventory_item_id    NUMBER,
                                 p_organization_id      NUMBER)
      RETURN NUMBER
   IS
      l_list_price   NUMBER;
   BEGIN
      SELECT NVL (unit_list_price, 0)
        INTO l_list_price
        FROM oe_order_lines
       WHERE line_id IN (SELECT MIN (oel.line_id)
                           FROM oe_order_lines oel
                          WHERE     oel.inventory_item_id =
                                       p_inventory_item_id
                                AND oel.ship_from_org_id = p_organization_id);


      RETURN l_list_price;
   EXCEPTION
      WHEN OTHERS
      THEN
         RETURN NULL;
   END;

   FUNCTION get_curr_lp_start_date (p_list_header_id    NUMBER,
                                    p_list_line_id      NUMBER)
      RETURN DATE
   IS
      l_curr_lp_start_date   DATE;
   BEGIN
      SELECT start_date_active
        INTO l_curr_lp_start_date
        FROM qp_list_lines
       WHERE list_line_id IN (SELECT MAX (qll.list_line_id)
                                FROM qp_list_lines qll
                               WHERE     qll.list_header_id =
                                            p_list_header_id
                                     AND qll.list_line_id = p_list_line_id);


      RETURN l_curr_lp_start_date;
   EXCEPTION
      WHEN OTHERS
      THEN
         RETURN NULL;
   END;


   FUNCTION get_curr_lp_end_date (p_list_header_id    NUMBER,
                                  p_list_line_id      NUMBER)
      RETURN DATE
   IS
      l_curr_lp_end_date   DATE;
   BEGIN
      SELECT end_date_active
        INTO l_curr_lp_end_date
        FROM qp_list_lines
       WHERE list_line_id IN (SELECT MAX (qll.list_line_id)
                                FROM qp_list_lines qll
                               WHERE     qll.list_header_id =
                                            p_list_header_id
                                     AND qll.list_line_id = p_list_line_id);


      RETURN l_curr_lp_end_date;
   EXCEPTION
      WHEN OTHERS
      THEN
         RETURN NULL;
   END;

   FUNCTION get_last_lp_start_date (p_list_header_id    NUMBER,
                                    p_list_line_id      NUMBER)
      RETURN DATE
   IS
      l_last_lp_start_date   DATE;
   BEGIN
      SELECT start_date_active
        INTO l_last_lp_start_date
        FROM qp_list_lines
       WHERE list_line_id IN (SELECT MIN (qll.list_line_id)
                                FROM qp_list_lines qll
                               WHERE     qll.list_header_id =
                                            p_list_header_id
                                     AND qll.list_line_id = p_list_line_id);


      RETURN l_last_lp_start_date;
   EXCEPTION
      WHEN OTHERS
      THEN
         RETURN NULL;
   END;



   FUNCTION get_mtd_sales (p_inventory_item_id    NUMBER,
                           p_organization_id      NUMBER,
                           p_start_date           DATE DEFAULT NULL,
                           p_end_date             DATE DEFAULT NULL)
      RETURN NUMBER
   IS
      l_mtd_sales   NUMBER;
   BEGIN
      SELECT NVL (SUM (oel.ordered_quantity), 0)
        INTO l_mtd_sales
        FROM oe_order_headers oeh, oe_order_lines oel
       WHERE     TRUNC (ordered_date) BETWEEN NVL (p_start_date, g_date_from)
                                          AND NVL (p_end_date, g_date_to)
             AND oeh.header_id = oel.header_id
             AND oel.inventory_item_id = p_inventory_item_id
             AND oel.ship_from_org_id = p_organization_id
             AND oeh.flow_status_code NOT IN ('CANCELLED')
             AND oel.flow_status_code NOT IN ('CANCELLED');

      RETURN l_mtd_sales;
   EXCEPTION
      WHEN OTHERS
      THEN
         RETURN 0;
   END;

   FUNCTION get_tot_sales (p_inventory_item_id    NUMBER,
                           p_organization_id      NUMBER,
                           p_start_date           DATE DEFAULT NULL,
                           p_end_date             DATE DEFAULT NULL)
      RETURN NUMBER
   IS
      l_mtd_sales   NUMBER;
   BEGIN
      SELECT NVL (SUM (oel.ordered_quantity), 0)
        INTO l_mtd_sales
        FROM oe_order_headers oeh, oe_order_lines oel
       WHERE     TRUNC (ordered_date) BETWEEN NVL (p_start_date, g_date_from)
                                          AND NVL (p_end_date, g_date_to)
             AND oeh.header_id = oel.header_id
             AND oel.flow_status_code <> 'CANCELLED'
             AND oel.inventory_item_id = p_inventory_item_id
             AND oel.ship_from_org_id = p_organization_id;

      RETURN l_mtd_sales;
   EXCEPTION
      WHEN OTHERS
      THEN
         RETURN 0;
   END;

   FUNCTION get_tot_sales_dollors (p_inventory_item_id    NUMBER,
                                   p_organization_id      NUMBER,
                                   p_start_date           DATE DEFAULT NULL,
                                   p_end_date             DATE DEFAULT NULL)
      RETURN NUMBER
   IS
      l_mtd_sales   NUMBER;
   BEGIN
      SELECT   NVL (
                  SUM (
                     DECODE (otl.order_category_code,
                             'RETURN', (oel.ordered_quantity * -1),
                             oel.ordered_quantity)),
                  0)
             * NVL (SUM (unit_selling_price), 0)
        INTO l_mtd_sales
        FROM oe_order_headers oeh,
             oe_order_lines oel,
             oe_transaction_types_vl otl,
             wsh_delivery_details wdd
       WHERE     1 = 1
             AND TRUNC (ordered_date) BETWEEN NVL (p_start_date, g_date_from)
                                          AND NVL (p_end_date, g_date_to)
             AND oeh.header_id = oel.header_id
             AND otl.org_id = oel.org_id
             AND otl.transaction_type_id = oel.line_type_id
             AND oel.line_id = wdd.source_line_id
             AND (   wdd.released_status IN ('C')
                  OR oel.flow_status_code = 'CLOSED')
             AND oel.flow_status_code <> 'CANCELLED'
             AND oel.inventory_item_id = p_inventory_item_id
             AND oel.ship_from_org_id = p_organization_id;

      RETURN l_mtd_sales;
   EXCEPTION
      WHEN OTHERS
      THEN
         RETURN 0;
   END;


   FUNCTION get_tot_sales_dlr (p_line_id       NUMBER,
                               p_start_date    DATE DEFAULT NULL,
                               p_end_date      DATE DEFAULT NULL)
      RETURN NUMBER
   IS
      l_mtd_sales   NUMBER;
      l_cnt         NUMBER;
   BEGIN
      /*SELECT COUNT(*)
      into l_cnt
      FROM WSH_DELIVERY_DETAILS WDD
      WHERE  WDD.SOURCE_LINE_ID=P_LINE_ID
      and released_status='C';

      if l_cnt > 0 then*/

      SELECT NVL (oel.ordered_quantity, 0) * NVL (unit_selling_price, 0)
        INTO l_mtd_sales
        FROM oe_order_headers oeh,
             oe_order_lines oel,
             oe_transaction_types_vl otl
       WHERE     1 = 1
             AND TRUNC (ordered_date) BETWEEN NVL (p_start_date, g_date_from)
                                          AND NVL (p_end_date, g_date_to)
             AND oeh.header_id = oel.header_id
             AND otl.org_id = oel.org_id
             AND otl.transaction_type_id = oel.line_type_id
             AND oel.flow_status_code IN ('PRE-BILLING_ACCEPTANCE', 'CLOSED')
             AND oel.line_id = p_line_id;

      --end if;
      RETURN l_mtd_sales;
   EXCEPTION
      WHEN OTHERS
      THEN
         RETURN 0;
   END;


   FUNCTION get_tot_prizingzone_sales_dlr (
      p_inventory_item_id    NUMBER,
      p_organization_id      NUMBER,
      p_zone                 VARCHAR2,
      p_start_date           DATE DEFAULT NULL,
      p_end_date             DATE DEFAULT NULL)
      RETURN NUMBER
   IS
      l_price_sales   NUMBER;
   BEGIN
      SELECT /*+ index(OEL OE_ORDER_LINES_ALL_HOT00) */ 
             NVL (
                SUM (
                     NVL (oel.ordered_quantity, 0)
                   * NVL (unit_selling_price, 0)),
                0)
        INTO l_price_sales
        FROM oe_order_headers oeh,
             oe_order_lines oel,
             oe_transaction_types_vl otl,
             mtl_parameters mtp
       WHERE     1 = 1
             --AND TRUNC (ordered_date) BETWEEN NVL (p_start_date, g_date_from)                              -- Version 1.11
             --                             AND NVL (p_end_date, g_date_to)                                  -- Version 1.11
             AND (oel.creation_date) >= NVL (p_start_date, TRUNC (g_date_from))                              -- Version 1.11
             AND (oel.creation_date) <= TO_DATE (NVL (p_end_date, TRUNC (g_date_to)), 'dd-mon-yy') + 0.9999  -- Version 1.11             
             AND oeh.header_id = oel.header_id
             AND otl.org_id = oel.org_id
             AND otl.transaction_type_id = oel.line_type_id
             AND oel.flow_status_code IN ('PRE-BILLING_ACCEPTANCE', 'CLOSED')
             AND oel.ship_from_org_id = mtp.organization_id
             AND oel.inventory_item_id = p_inventory_item_id --AND OEL.SHIP_FROM_ORG_ID    = P_ORGANIZATION_ID
             AND mtp.attribute6 = p_zone;


      --end if;
      RETURN l_price_sales;
   EXCEPTION
      WHEN OTHERS
      THEN
         RETURN 0;
   END;


   FUNCTION get_ytd_sales (p_inventory_item_id    NUMBER,
                           p_organization_id      NUMBER,
                           p_year_start_date      DATE)
      RETURN NUMBER
   IS
      l_ytd_sales   NUMBER;
   BEGIN
      SELECT NVL (SUM (oel.ordered_quantity), 0)
        INTO l_ytd_sales
        FROM oe_order_headers oeh, oe_order_lines oel
       WHERE   --TRUNC (ordered_date) BETWEEN p_year_start_date -- Commeneted for Ver 1.14
	         TRUNC (ordered_date) BETWEEN add_months(trunc(ordered_date, 'MM'), -12)  --Added for Ver 1.14
                                          AND TRUNC (SYSDATE)
             AND oeh.header_id = oel.header_id
             AND oel.inventory_item_id = p_inventory_item_id
             AND oel.ship_from_org_id = p_organization_id;

      RETURN l_ytd_sales;
   EXCEPTION
      WHEN OTHERS
      THEN
         RETURN NULL;
   END;



   FUNCTION get_header_hold (p_order_header_id NUMBER, p_line_id NUMBER)
      RETURN VARCHAR2
   IS
      l_hold_name   VARCHAR2 (400);
      l_cnt         NUMBER;
   BEGIN
      SELECT COUNT (*)
        INTO l_cnt
        FROM oe_order_holds hold,
             oe_hold_sources hold_source,
             oe_hold_definitions hold_definition
       WHERE     hold.header_id = p_order_header_id
             AND hold.line_id IS NULL
             AND hold.hold_source_id = hold_source.hold_source_id
             AND hold_source.hold_id = hold_definition.hold_id
             AND hold.hold_release_id IS NULL;

      IF l_cnt > 1
      THEN
         RETURN 'Multiple Holds Exist';
      END IF;

      SELECT hold_definition.name
        INTO l_hold_name
        FROM oe_order_holds hold,
             oe_hold_sources hold_source,
             oe_hold_definitions hold_definition
       WHERE     hold.header_id = p_order_header_id
             AND hold.line_id IS NULL
             AND hold.hold_source_id = hold_source.hold_source_id
             AND hold_source.hold_id = hold_definition.hold_id
             AND hold.hold_release_id IS NULL;



      RETURN l_hold_name;
   EXCEPTION
      WHEN OTHERS
      THEN
         RETURN NULL;
   END;


   FUNCTION get_line_hold (p_order_header_id NUMBER, p_line_id NUMBER)
      RETURN VARCHAR2
   IS
      l_hold_name   VARCHAR2 (400);
      l_cnt         NUMBER;
   BEGIN
      SELECT COUNT (*)
        INTO l_cnt
        FROM oe_order_holds hold,
             oe_hold_sources hold_source,
             oe_hold_definitions hold_definition
       WHERE     hold.header_id = p_order_header_id
             AND hold.line_id = p_line_id
             AND hold.hold_source_id = hold_source.hold_source_id
             AND hold_source.hold_id = hold_definition.hold_id
             AND hold.hold_release_id IS NULL;


      IF l_cnt > 1
      THEN
         RETURN 'Multiple Holds Exist';
      END IF;


      SELECT hold_definition.name
        INTO l_hold_name
        FROM oe_order_holds hold,
             oe_hold_sources hold_source,
             oe_hold_definitions hold_definition
       WHERE     hold.header_id = p_order_header_id
             AND hold.line_id = p_line_id
             AND hold.hold_source_id = hold_source.hold_source_id
             AND hold_source.hold_id = hold_definition.hold_id
             AND hold.hold_release_id IS NULL;


      RETURN l_hold_name;
   EXCEPTION
      WHEN OTHERS
      THEN
         RETURN NULL;
   END;

   FUNCTION get_header_hold_date (p_order_header_id NUMBER, p_line_id NUMBER)
      RETURN DATE
   IS
      l_hold_date   DATE;
      l_cnt         NUMBER;
   BEGIN
      /*select count(*)
      Into l_cnt
          from    Oe_Order_Holds Hold,
          Oe_Hold_Sources hold_Source,
          Oe_Hold_Definitions Hold_Definition
          Where Hold.Header_Id=P_Order_Header_Id
          and hold.line_id  is null
           And Hold.Hold_Source_Id            = Hold_Source.Hold_Source_Id
        and hold_source.hold_id                   = hold_definition.hold_id
         and Hold.hold_release_id is null;

           if l_cnt >1 then
           return 'Multiple Holds Exist';
           end if;*/

      SELECT MAX (hold.creation_date)
        --Hold_Definition.creation_date commented by srini
        INTO l_hold_date
        FROM oe_order_holds hold,
             oe_hold_sources hold_source,
             oe_hold_definitions hold_definition
       WHERE     hold.header_id = p_order_header_id
             AND hold.line_id IS NULL
             AND hold.hold_source_id = hold_source.hold_source_id
             AND hold_source.hold_id = hold_definition.hold_id
             AND hold.hold_release_id IS NULL;



      RETURN l_hold_date;
   EXCEPTION
      WHEN OTHERS
      THEN
         RETURN NULL;
   END;


   FUNCTION get_line_hold_date (p_order_header_id NUMBER, p_line_id NUMBER)
      RETURN DATE
   IS
      l_hold_date   DATE;
      l_cnt         NUMBER;
   BEGIN
      /*select count(*)
      Into l_cnt
          from    Oe_Order_Holds Hold,
          Oe_Hold_Sources hold_Source,
          Oe_Hold_Definitions Hold_Definition
          Where Hold.Header_Id=P_Order_Header_Id
           and hold.line_id  =p_line_id
           and hold.hold_source_id            = hold_source.hold_source_id
           and hold_source.hold_id                   = hold_definition.hold_id
             and Hold.hold_release_id is null;


           if l_cnt >1 then
           return 'Multiple Holds Exist';
           end if;*/


      SELECT MAX (hold.creation_date)
        --Hold_Definition.creation_date commented by srini
        INTO l_hold_date
        FROM oe_order_holds hold,
             oe_hold_sources hold_source,
             oe_hold_definitions hold_definition
       WHERE     hold.header_id = p_order_header_id
             AND hold.line_id = p_line_id
             AND hold.hold_source_id = hold_source.hold_source_id
             AND hold_source.hold_id = hold_definition.hold_id
             AND hold.hold_release_id IS NULL;


      RETURN l_hold_date;
   EXCEPTION
      WHEN OTHERS
      THEN
         RETURN NULL;
   END;

   FUNCTION get_discount_amt (p_order_header_id    NUMBER,
                              p_line_id            NUMBER DEFAULT NULL)
      RETURN NUMBER
   IS
      l_adj_amt   NUMBER := 0;
   BEGIN
      SELECT SUM (NVL (adjusted_amount, 0))
        INTO l_adj_amt
        FROM oe_price_adjustments_v
       WHERE     list_line_type_code = 'DIS'
             AND header_id = p_order_header_id
             AND line_id = NVL (p_line_id, line_id);


      RETURN l_adj_amt;
   EXCEPTION
      WHEN OTHERS
      THEN
         RETURN 0;
   END;

   FUNCTION get_special_cost (p_header_id            NUMBER,
                              p_line_id              NUMBER,
                              p_inventory_item_id    NUMBER)
      RETURN NUMBER
   IS
      l_special_cost   NUMBER := 0;
   BEGIN
      /*select discount_value
        INTO l_disc_amt
        from ozf_sd_batch_lines_all
       where order_header_id              = p_header_id
         and order_line_id                = p_line_id
         AND discount_type = 'NEWPRICE';*/
      /*select min(opa.operand)
       INTO l_special_cost
       from oe_price_adjustments_v opa,
            ozf_sd_request_headers_all_b  osh,
            ozf_sd_request_lines_all osl
      where opa.adjustment_name   = osh.request_number
        AND osh.request_header_id = osl.request_header_id
        AND osl.inventory_item_id = p_inventory_item_id
        AND opa.header_id         = p_header_id
        and opa.line_id           = p_line_id;*/

      SELECT MAX (TO_NUMBER (NVL (qpl.attribute4, 0)))
        INTO l_special_cost
        FROM qp_list_lines qpl, oe_price_adjustments_v opa
       WHERE     opa.header_id = p_header_id
             AND opa.line_id = p_line_id
             AND opa.list_line_type_code = 'DIS'
             AND opa.applied_flag = 'Y'
             AND qpl.list_header_id = opa.list_header_id
             AND qpl.list_line_id = opa.list_line_id;

      RETURN l_special_cost;
   EXCEPTION
      WHEN OTHERS
      THEN
         RETURN 0;
   END;

   FUNCTION get_ssd_req_number (p_header_id            NUMBER,
                                p_line_id              NUMBER,
                                p_inventory_item_id    NUMBER)
      RETURN VARCHAR2
   IS
      l_ssd_number   VARCHAR2 (100);
   BEGIN
      /*select discount_value
        INTO l_disc_amt
        from ozf_sd_batch_lines_all
       where order_header_id              = p_header_id
         and order_line_id                = p_line_id
         AND discount_type = 'NEWPRICE';*/
      SELECT MIN (osh.request_number)
        INTO l_ssd_number
        FROM oe_price_adjustments_v opa,
             ozf_sd_request_headers_all_b osh,
             ozf_sd_request_lines_all osl
       WHERE     opa.adjustment_name = osh.request_number
             AND osh.request_header_id = osl.request_header_id
             AND osl.inventory_item_id = p_inventory_item_id
             AND opa.header_id = p_header_id
             AND opa.line_id = p_line_id;

      RETURN l_ssd_number;
   EXCEPTION
      WHEN OTHERS
      THEN
         RETURN 0;
   END;

   FUNCTION get_freight_amt (p_order_header_id    NUMBER,
                             p_line_id            NUMBER DEFAULT NULL)
      RETURN NUMBER
   IS
      l_adj_amt   NUMBER;
   BEGIN
      SELECT SUM (NVL (adjusted_amount, 0))
        INTO l_adj_amt
        FROM oe_price_adjustments_v
       WHERE     list_line_type_code = 'FREIGHT_CHARGE'
             AND header_id = p_order_header_id
             AND line_id = NVL (p_line_id, line_id);


      RETURN l_adj_amt;
   EXCEPTION
      WHEN OTHERS
      THEN
         RETURN 0;
   END;


   PROCEDURE set_date_from (p_date_from DATE)
   IS
   BEGIN
      g_date_from := p_date_from;
   END;

   PROCEDURE set_date_to (p_date_to DATE)
   IS
   BEGIN
      g_date_to := p_date_to;
   END;


   FUNCTION get_period
      RETURN VARCHAR2
   IS
   BEGIN
      RETURN g_period_name;
   --FND_FILE.PUT_LINE(FND_FILE.LOG,'GET PERIOD NAME'||G_PERIOD_NAME);
   EXCEPTION
      WHEN OTHERS
      THEN
         --FND_FILE.PUT_LINE(FND_FILE.LOG,'Error Message'||SQLCODE||SQLERRM);
         RETURN NULL;
   END;

   FUNCTION get_date_from
      RETURN DATE
   IS
   BEGIN
      RETURN g_date_from;
   --FND_FILE.PUT_LINE(FND_FILE.LOG,'Get date'||G_DATE_FROM);
   EXCEPTION
      WHEN OTHERS
      THEN
         --FND_FILE.PUT_LINE(FND_FILE.LOG,'Error Message'||SQLCODE||SQLERRM);
         RETURN NULL;
   END;



   FUNCTION get_date_to
      RETURN DATE
   IS
   BEGIN
      RETURN g_date_to;
   END;


   FUNCTION get_cancelled_qty (p_inventory_item_id    NUMBER,
                               p_organization_id      NUMBER)
      RETURN NUMBER
   IS
      l_cancelled_qty   NUMBER;
   BEGIN
      SELECT SUM (oel.ordered_quantity)
        INTO l_cancelled_qty
        FROM oe_order_headers oeh, oe_order_lines oel
       WHERE     TRUNC (ordered_date) BETWEEN g_date_from AND g_date_to
             AND oeh.header_id = oel.header_id
             AND oel.inventory_item_id = p_inventory_item_id
             AND oel.ship_from_org_id = p_organization_id
             AND oel.flow_status_code IN ('CANCELLED');

      RETURN l_cancelled_qty;
   EXCEPTION
      WHEN OTHERS
      THEN
         RETURN NULL;
   END;

   FUNCTION get_line_disp_qty (p_inventory_item_id    NUMBER,
                               p_organization_id      NUMBER)
      RETURN NUMBER
   IS
      l_disp_qty   NUMBER;
   BEGIN
      SELECT SUM (oel.ordered_quantity)
        INTO l_disp_qty
        FROM oe_order_headers oeh, oe_order_lines oel
       WHERE     TRUNC (ordered_date) BETWEEN g_date_from AND g_date_to
             AND oeh.header_id = oel.header_id
             AND oel.inventory_item_id = p_inventory_item_id
             AND oel.ship_from_org_id = p_organization_id
             AND oel.ship_from_org_id <> oeh.ship_from_org_id;

      RETURN l_disp_qty;
   EXCEPTION
      WHEN OTHERS
      THEN
         RETURN NULL;
   END;



   FUNCTION get_cancelled_count (p_inventory_item_id    NUMBER,
                                 p_organization_id      NUMBER)
      RETURN NUMBER
   IS
      l_cancelled_count   NUMBER;
   BEGIN
      SELECT COUNT (oel.line_id)
        INTO l_cancelled_count
        FROM oe_order_headers oeh, oe_order_lines oel
       WHERE     TRUNC (ordered_date) BETWEEN g_date_from AND g_date_to
             AND oeh.header_id = oel.header_id
             AND oel.inventory_item_id = p_inventory_item_id
             AND oel.ship_from_org_id = p_organization_id
             AND oel.flow_status_code IN ('CANCELLED');

      RETURN l_cancelled_count;
   EXCEPTION
      WHEN OTHERS
      THEN
         RETURN NULL;
   END;

   FUNCTION get_line_disp_count (p_inventory_item_id    NUMBER,
                                 p_organization_id      NUMBER)
      RETURN NUMBER
   IS
      l_disp_count   NUMBER;
   BEGIN
      SELECT COUNT (oel.line_id)
        INTO l_disp_count
        FROM oe_order_headers oeh, oe_order_lines oel
       WHERE     TRUNC (ordered_date) BETWEEN g_date_from AND g_date_to
             AND oeh.header_id = oel.header_id
             AND oel.inventory_item_id = p_inventory_item_id
             AND oel.ship_from_org_id = p_organization_id
             AND oel.ship_from_org_id <> oeh.ship_from_org_id;

      RETURN l_disp_count;
   EXCEPTION
      WHEN OTHERS
      THEN
         RETURN NULL;
   END;


   PROCEDURE set_period_name_from (p_period_name_from VARCHAR)
   IS
   BEGIN
      g_period_name_from := p_period_name_from;
   END;

   PROCEDURE set_period_name_to (p_period_name_to VARCHAR)
   IS
   BEGIN
      g_period_name_to := p_period_name_to;
   END;


   FUNCTION get_period_name_from
      RETURN VARCHAR
   IS
   BEGIN
      RETURN g_period_name_from;
   END;

   FUNCTION get_period_name_to
      RETURN VARCHAR
   IS
   BEGIN
      RETURN g_period_name_to;
   END;

   FUNCTION get_last_year_period_name (p_ledger_id NUMBER)
      RETURN VARCHAR
   IS
      l_period_year       NUMBER;
      l_period_num        NUMBER;
      l_period_name       VARCHAR2 (50);
      l_set_of_books_id   NUMBER;
   BEGIN
      SELECT gp.period_year, gp.period_num
        INTO l_period_year, l_period_num
        FROM gl_periods gp, gl_ledgers gl
       WHERE     gp.period_set_name = gl.period_set_name
             AND period_name = g_period_name
             AND gl.ledger_id = p_ledger_id;

      SELECT gps.period_name
        INTO l_period_name
        FROM gl_period_statuses gps
       WHERE     gps.application_id = 101
             AND gps.set_of_books_id = p_ledger_id
             AND period_year = (l_period_year - 1)
             AND period_num = l_period_num;

      RETURN l_period_name;
   END;



   FUNCTION get_period_end_date (p_period_name VARCHAR2, p_ledger_id NUMBER)
      RETURN DATE
   IS
      l_period_date   DATE;
   BEGIN
      SELECT end_date
        INTO l_period_date
        FROM gl_periods gp, gl_ledgers gl
       WHERE     gp.period_set_name = gl.period_set_name
             AND gl.ledger_id = p_ledger_id
             AND period_name = p_period_name;

      RETURN l_period_date;
   EXCEPTION
      WHEN OTHERS
      THEN
         RETURN NULL;
   END;


   FUNCTION get_period_start_date (p_period_name    VARCHAR2,
                                   p_ledger_id      NUMBER)
      RETURN DATE
   IS
      l_period_date   DATE;
   BEGIN
      SELECT start_date
        INTO l_period_date
        FROM gl_periods gp, gl_ledgers gl
       WHERE     gp.period_set_name = gl.period_set_name
             AND gl.ledger_id = p_ledger_id
             AND period_name = p_period_name;

      RETURN l_period_date;
   EXCEPTION
      WHEN OTHERS
      THEN
         RETURN NULL;
   END;

   FUNCTION get_year_start_date (p_period_name VARCHAR2, p_ledger_id NUMBER)
      RETURN DATE
   IS
      l_period_date   DATE;
   BEGIN
      SELECT gps.year_start_date
        INTO l_period_date
        FROM gl_period_statuses gps
       WHERE     gps.application_id = 101
             AND set_of_books_id = p_ledger_id
             AND period_name = p_period_name;

      RETURN l_period_date;
   EXCEPTION
      WHEN OTHERS
      THEN
         RETURN NULL;
   END;

   FUNCTION get_tax_jurdisjiction_code (p_tax_line_id NUMBER)
      RETURN VARCHAR2
   IS
      l_jurisdiction_code   VARCHAR2 (600);
   BEGIN
      SELECT tax_jurisdiction_code
        INTO l_jurisdiction_code
        FROM zx_lines_v
       WHERE tax_line_id = p_tax_line_id;

      RETURN l_jurisdiction_code;
   END;

   FUNCTION get_shipping_mthd (p_freight_code VARCHAR2)
      RETURN VARCHAR2
   IS
      l_description   VARCHAR2 (600);
   BEGIN
      SELECT DISTINCT of_ship_via.description
        INTO l_description
        FROM org_freight of_ship_via
       WHERE of_ship_via.freight_code = p_freight_code;

      RETURN l_description;
   END;

   PROCEDURE set_getpaid_data (p_custno VARCHAR2, p_trx_number VARCHAR2)
   IS
      l_shipvia   VARCHAR2 (600);
   BEGIN
      SELECT shipvia,
             warehse,
             ordnum,
             invno
        INTO g_cnv_inv_tab.shipvia,
             g_cnv_inv_tab.warehse,
             g_cnv_inv_tab.ordnum,
             g_cnv_inv_tab.invno
        FROM xxwc.xxwc_arexti_getpaid_dump_tbl
       WHERE custno = p_custno AND invno = p_trx_number;
   END;

   FUNCTION get_shipvia (p_custno VARCHAR2, p_trx_number VARCHAR2)
      RETURN VARCHAR2
   IS
      l_shipvia   VARCHAR2 (600);
   BEGIN
      IF     g_cnv_inv_tab.custno = p_custno
         AND g_cnv_inv_tab.invno = p_trx_number
      THEN
         RETURN g_cnv_inv_tab.shipvia;
      ELSE
         set_getpaid_data (p_custno, p_trx_number);
      END IF;

      RETURN g_cnv_inv_tab.shipvia;
   END;

   FUNCTION get_warehse (p_custno VARCHAR2, p_trx_number VARCHAR2)
      RETURN VARCHAR2
   IS
      l_shipvia   VARCHAR2 (600);
   BEGIN
      IF     g_cnv_inv_tab.custno = p_custno
         AND g_cnv_inv_tab.invno = p_trx_number
      THEN
         RETURN g_cnv_inv_tab.warehse;
      ELSE
         set_getpaid_data (p_custno, p_trx_number);
      END IF;

      RETURN g_cnv_inv_tab.warehse;
   END;


   FUNCTION get_ordnum (p_custno VARCHAR2, p_trx_number VARCHAR2)
      RETURN VARCHAR2
   IS
      l_shipvia   VARCHAR2 (600);
   BEGIN
      IF     g_cnv_inv_tab.custno = p_custno
         AND g_cnv_inv_tab.invno = p_trx_number
      THEN
         RETURN g_cnv_inv_tab.ordnum;
      ELSE
         set_getpaid_data (p_custno, p_trx_number);
      END IF;

      RETURN g_cnv_inv_tab.ordnum;
   END;



   FUNCTION get_po_cost (p_requisition_header_id    NUMBER,
                         p_po_header_id             NUMBER)
      RETURN NUMBER
   IS
      l_po_cost   NUMBER;
   BEGIN
      SELECT ROUND (SUM (quantity * unit_price), 2)
        INTO l_po_cost
        FROM po_lines pl2, oe_drop_ship_sources od1
       WHERE     pl2.po_line_id = od1.po_line_id
             AND od1.requisition_header_id = p_requisition_header_id
             AND od1.po_header_id = p_po_header_id;

      RETURN l_po_cost;
   END;

   FUNCTION get_shipping_mthd2 (p_party_id NUMBER)
      RETURN VARCHAR2
   IS
      l_description   VARCHAR2 (600);
   BEGIN
      SELECT DISTINCT of_ship_via.description
        INTO l_description
        FROM org_freight of_ship_via
       WHERE p_party_id = of_ship_via.party_id;

      RETURN l_description;
   END;


   FUNCTION get_shipping_mthd3 (p_freight_code VARCHAR2)
      RETURN VARCHAR2
   IS
      l_description   VARCHAR2 (600);
   BEGIN
      SELECT DISTINCT meaning
        INTO l_description
        FROM fnd_lookup_values_vl
       WHERE     lookup_type = 'SHIP_METHOD'
             AND end_date_active IS NULL
             AND lookup_code = p_freight_code;


      IF    UPPER (l_description) LIKE UPPER ('%Will%Call%')
         OR UPPER (l_description) LIKE UPPER ('%Walk%In%')
      THEN
         RETURN 'Will Call';
      ELSIF    UPPER (l_description) LIKE UPPER ('%Our%Truck%')
            OR UPPER (l_description) LIKE UPPER ('%Salesperson%Delivery%')
      THEN
         RETURN 'Our Truck';
      ELSIF    UPPER (l_description) LIKE UPPER ('%UPS')
            OR UPPER (l_description) LIKE UPPER ('%FEDEX EAST-Parcel-Ground')
            OR UPPER (l_description) LIKE
                  UPPER ('%FEDEX NATIONAL-LTL-Ground')
      THEN
         RETURN '3rd Party Deliveries';
      ELSE
         RETURN '3rd Party Deliveries';
      END IF;
   EXCEPTION
      WHEN OTHERS
      THEN
         RETURN NULL;
   END;


   FUNCTION get_shipping_mthd4 (p_freight_code VARCHAR2)
      RETURN VARCHAR2
   IS
      l_description   VARCHAR2 (600);
   BEGIN
      SELECT DISTINCT meaning
        INTO l_description
        FROM fnd_lookup_values_vl
       WHERE     lookup_type = 'SHIP_METHOD'
             AND end_date_active IS NULL
             AND lookup_code = p_freight_code;

      RETURN l_description;
   EXCEPTION
      WHEN OTHERS
      THEN
         RETURN NULL;
   END;

   FUNCTION get_rental_start_date (p_line_id NUMBER)
      RETURN DATE
   IS
      l_date   DATE;
   BEGIN
      SELECT TRUNC (oll.actual_shipment_date)
        INTO l_date
        FROM oe_order_lines oll, oe_transaction_types_vl ott1
       WHERE     oll.line_type_id = ott1.transaction_type_id
             AND ott1.name = 'RENTAL LINE'
             AND oll.line_id = p_line_id;

      RETURN l_date;
   END;

   FUNCTION get_rental_days (p_header_id          NUMBER,
                             p_link_to_line_id    NUMBER,
                             p_line_id            NUMBER)
      RETURN NUMBER
   IS
      l_days               NUMBER;
      l_shipment_date      DATE;
      l_flow_status_code   VARCHAR2 (50);
      l_creation_date      DATE;
   BEGIN
      /* Select trunc(Date_Scheduled)
       Into  L_Shipment_Date
       From  Wsh_Delivery_Details
       Where Source_Header_Id=P_Header_Id
       and   source_line_id=P_line_id;*/

      SELECT oll.actual_shipment_date
        INTO l_shipment_date
        FROM oe_order_lines oll, oe_transaction_types_vl ott1
       WHERE     oll.line_type_id = ott1.transaction_type_id
             AND ott1.name = 'RENTAL LINE'
             AND oll.line_id = p_link_to_line_id;

      SELECT CASE
                WHEN shipment_line_status_code = 'FULLY RECEIVED'
                THEN
                   creation_date - l_shipment_date
                ELSE
                   SYSDATE - l_shipment_date
             END
        INTO l_days
        FROM rcv_shipment_lines rsl
       WHERE rsl.oe_order_line_id = p_line_id;

      -- (trunc(creation_date - add_months(l_shipment_date,trunc(months_between(creation_date,l_shipment_date) ))))
      -- Else  round((Sysdate)-L_Shipment_Date)
      --  end

      RETURN l_days;
   EXCEPTION
      WHEN OTHERS
      THEN
         --  return null;
         RETURN ROUND ( (SYSDATE) - l_shipment_date);
   END;

   FUNCTION get_onhand_inv (p_inventory_item_id    NUMBER,
                            p_organization_id      NUMBER)
      RETURN NUMBER
   IS
      l_onhand_inv   NUMBER;
   BEGIN
      SELECT NVL (SUM (moq.transaction_quantity), 0)
        INTO l_onhand_inv
        FROM mtl_onhand_quantities_detail moq
       WHERE     inventory_item_id = p_inventory_item_id
             AND organization_id = p_organization_id;

      RETURN l_onhand_inv;
   EXCEPTION
      WHEN OTHERS
      THEN
         RETURN 0;
   END;

   FUNCTION get_oldest_born_date (p_inventory_item_id    NUMBER,
                                  p_organization_id      NUMBER)
      RETURN DATE
   IS
      l_born_date   DATE;
   BEGIN
      SELECT TRUNC (MIN (orig_date_received))
        INTO l_born_date
        FROM mtl_onhand_quantities_detail moq
       WHERE     inventory_item_id = p_inventory_item_id
             AND organization_id = p_organization_id;

      RETURN l_born_date;
   EXCEPTION
      WHEN OTHERS
      THEN
         RETURN NULL;
   END;


   FUNCTION get_onhand_date (p_inventory_item_id    NUMBER,
                             p_organization_id      NUMBER)
      RETURN DATE
   IS
      l_onhand_date   DATE;
   BEGIN
      SELECT MAX (date_received)
        INTO l_onhand_date
        FROM mtl_onhand_quantities_detail moq
       WHERE     inventory_item_id = p_inventory_item_id
             AND organization_id = p_organization_id;

      RETURN l_onhand_date;
   EXCEPTION
      WHEN OTHERS
      THEN
         RETURN NULL;
   END;

   FUNCTION get_onhand_qty (p_inventory_item_id    NUMBER,
                            p_organization_id      NUMBER,
                            p_date                 DATE DEFAULT NULL)
      RETURN NUMBER
   IS
      l_onhand_qty   NUMBER;
   BEGIN
      SELECT NVL (SUM (moq.transaction_quantity), 0)
        INTO l_onhand_qty
        FROM mtl_material_transactions moq
       WHERE     inventory_item_id = p_inventory_item_id
             AND organization_id = p_organization_id
             AND TRUNC (transaction_date) <= NVL (p_date, g_date_to);

      RETURN l_onhand_qty;
   EXCEPTION
      WHEN OTHERS
      THEN
         RETURN 0;
   END;


   FUNCTION get_sub_onhand_qty (p_inventory_item_id    NUMBER,
                                p_organization_id      NUMBER,
                                p_date                 DATE DEFAULT NULL)
      RETURN NUMBER
   IS
      l_sub_onhand_qty   NUMBER;
   BEGIN
      SELECT NVL (SUM (moq.transaction_quantity), 0)
        INTO l_sub_onhand_qty
        FROM mtl_item_sub_inventories mis,
             mtl_material_transactions moq,
             mtl_secondary_inventories msci
       WHERE     moq.inventory_item_id = mis.inventory_item_id
             AND moq.organization_id = mis.organization_id
             AND mis.secondary_inventory = msci.secondary_inventory_name
             AND mis.organization_id = msci.organization_id
             AND UPPER (msci.secondary_inventory_name) IN ('DBRENTAL',
                                                           'DRRENTAL',
                                                           'LOSTRENTAL',
                                                           'RENTAL',
                                                           'RENTAL N/R',
                                                           'RENTALHLD')
             AND moq.inventory_item_id = p_inventory_item_id
             AND moq.organization_id = p_organization_id
             AND TRUNC (transaction_date) <= NVL (p_date, g_date_to);

      RETURN l_sub_onhand_qty;
   EXCEPTION
      WHEN OTHERS
      THEN
         RETURN 0;
   END;

   FUNCTION get_item_cost (p_inventory_item_id    NUMBER,
                           p_organization_id      NUMBER)
      RETURN NUMBER
   IS
      l_item_cost   NUMBER;
   BEGIN
      SELECT actual_cost
        INTO l_item_cost
        FROM mtl_material_transactions
       WHERE transaction_id =
                (SELECT MAX (transaction_id)
                   FROM mtl_material_transactions moq
                  WHERE     inventory_item_id = p_inventory_item_id
                        AND organization_id = p_organization_id
                        AND TRUNC (transaction_date) <= g_date_to);

      RETURN l_item_cost;
   EXCEPTION
      WHEN OTHERS
      THEN
         RETURN NULL;
   END;

   FUNCTION get_item_demand_qty (p_inventory_item_id    NUMBER,
                                 p_organization_id      NUMBER)
      RETURN NUMBER
   IS
      l_demand_qty   NUMBER;
   BEGIN
      SELECT SUM (ol.ordered_quantity)
        INTO l_demand_qty
        FROM oe_order_lines ol
       WHERE     ol.inventory_item_id = p_inventory_item_id
             AND ol.ship_from_org_id = p_organization_id
             AND flow_status_code NOT IN ('CLOSED', 'CANCELLED');

      RETURN l_demand_qty;
   EXCEPTION
      WHEN OTHERS
      THEN
         RETURN NULL;
   END;


   FUNCTION get_item_crossref (p_inventory_item_id    NUMBER,
                               p_organization_id      NUMBER)
      RETURN VARCHAR2
   IS
      l_cross_ref   VARCHAR2 (255);
   BEGIN
      SELECT cross_reference
        INTO l_cross_ref
        FROM mtl_cross_references_v mcr
       WHERE     mcr.inventory_item_id = p_inventory_item_id
             AND mcr.cross_reference_type = 'VENDOR'
             AND NVL (mcr.organization_id, p_organization_id) =
                    p_organization_id;

      RETURN l_cross_ref;
   EXCEPTION
      WHEN OTHERS
      THEN
         RETURN NULL;
   END;

   /*FUNCTION Get_primary_bin_loc (P_INVENTORY_ITEM_ID NUMBER, P_ORGANIZATION_ID NUMBER)
   RETURN VARCHAR2
   IS
    l_seg VARCHAR2(5000);
   BEGIN

       Select Segment1
       Into  L_Seg
       From Mtl_Item_Locations_Kfv
       Where Inventory_Item_Id  = P_Inventory_Item_Id
       AND ORGANIZATION_ID      =P_ORGANIZATION_ID
       and segment1  like '1%';


     return l_seg;
    exception
    WHEN OTHERS THEN
      RETURN NULL;
   END;*/

   FUNCTION get_primary_bin_loc (p_inventory_item_id    NUMBER,
                                 p_organization_id      NUMBER)
      RETURN VARCHAR2
   IS
      l_seg   VARCHAR2 (5000);
   BEGIN
      SELECT mil.segment1
        INTO l_seg
        FROM mtl_item_locations_kfv mil, mtl_secondary_locators msl
       WHERE     msl.inventory_item_id = p_inventory_item_id
             AND msl.organization_id = p_organization_id
             AND msl.secondary_locator = mil.inventory_location_id
             AND mil.segment1 LIKE '1-%';


      RETURN l_seg;
   EXCEPTION
      WHEN OTHERS
      THEN
         RETURN NULL;
   END;

   /*FUNCTION GET_ALTERNATE_BIN_LOC (P_INVENTORY_ITEM_ID NUMBER, P_ORGANIZATION_ID NUMBER)
   RETURN VARCHAR2
   IS
    l_str VARCHAR2(5000);
   BEGIN

      FOR GET_BIN_LOC IN( SELECT CONCATENATED_SEGMENTS
     --  INTO  l_str
       From Mtl_Item_Locations_Kfv
       Where Inventory_Item_Id  = P_Inventory_Item_Id
       AND ORGANIZATION_ID      = P_ORGANIZATION_ID
       and segment1 not like '1%')
       LOOP
       l_str:=l_str||','||GET_BIN_LOC.CONCATENATED_SEGMENTS;
       END LOOP;

     return substr(l_str,2);
    exception
    WHEN OTHERS THEN
      RETURN NULL;
   END;*/

   FUNCTION get_alternate_bin_loc (p_inventory_item_id    NUMBER,
                                   p_organization_id      NUMBER)
      RETURN VARCHAR2
   IS
      l_str   VARCHAR2 (5000);
   BEGIN
      FOR get_bin_loc
         IN (SELECT mil.concatenated_segments
               --  INTO  l_str
               FROM mtl_item_locations_kfv mil, mtl_secondary_locators msl
              WHERE     msl.inventory_item_id = p_inventory_item_id
                    AND msl.organization_id = p_organization_id
                    AND msl.secondary_locator = mil.inventory_location_id
                    AND mil.segment1 NOT LIKE '1-%')
      LOOP
         l_str := l_str || ',' || get_bin_loc.concatenated_segments;
      END LOOP;

      RETURN SUBSTR (l_str, 2);
   EXCEPTION
      WHEN OTHERS
      THEN
         RETURN NULL;
   END;

   /* FUNCTION get_bin_loc_flag (p_inventory_item_id NUMBER, p_organization_id NUMBER)
       RETURN VARCHAR2
    IS
       l_return_flag   VARCHAR2 (1);
    BEGIN
       IF g_start_bin IS NULL AND g_end_bin IS NULL
       THEN
          RETURN 'Y';
       ELSIF g_start_bin IS NOT NULL AND g_end_bin IS NOT NULL
       THEN
          SELECT 'Y'
            INTO l_return_flag
            FROM mtl_item_locations_kfv mil, mtl_secondary_locators msl
           WHERE msl.inventory_item_id = p_inventory_item_id AND msl.organization_id = p_organization_id AND msl.secondary_locator = mil.inventory_location_id AND SUBSTR (mil.segment1, 3) BETWEEN g_start_bin AND g_end_bin;

          RETURN l_return_flag;
       ELSIF g_start_bin IS NOT NULL AND g_end_bin IS NULL
       THEN
          SELECT 'Y'
            INTO l_return_flag
            FROM mtl_item_locations_kfv mil, mtl_secondary_locators msl
           WHERE msl.inventory_item_id = p_inventory_item_id AND msl.organization_id = p_organization_id AND msl.secondary_locator = mil.inventory_location_id AND SUBSTR (mil.segment1, 3) >= g_start_bin;

          RETURN l_return_flag;
       ELSIF g_start_bin IS NULL AND g_end_bin IS NOT NULL
       THEN
          SELECT 'Y'
            INTO l_return_flag
            FROM mtl_item_locations_kfv mil, mtl_secondary_locators msl
           WHERE msl.inventory_item_id = p_inventory_item_id AND msl.organization_id = p_organization_id AND msl.secondary_locator = mil.inventory_location_id AND SUBSTR (mil.segment1, 3) <= g_end_bin;

          RETURN l_return_flag;
       END IF;
    EXCEPTION
       WHEN OTHERS
       THEN
          RETURN 'N';
    END; */


   FUNCTION get_bin_loc_flag (p_inventory_item_id    NUMBER,
                              p_organization_id      NUMBER)
      RETURN VARCHAR2
   IS
      l_return_flag   VARCHAR2 (1);
   BEGIN
      IF g_start_bin IS NULL AND g_end_bin IS NULL
      THEN
         RETURN 'Y';
      ELSIF g_start_bin IS NOT NULL AND g_end_bin IS NOT NULL
      THEN
         SELECT 'Y'
           INTO l_return_flag
           FROM MTL_ITEM_LOCATIONS_KFV MIL, MTL_SECONDARY_LOCATORS MSL
          WHERE     msl.inventory_item_id = p_inventory_item_id
                AND MSL.ORGANIZATION_ID = P_ORGANIZATION_ID
                AND msl.secondary_locator = mil.inventory_location_id
                AND SUBSTR (mil.segment1, 3) BETWEEN g_start_bin
                                                 AND g_end_bin
                AND ROWNUM <= 1;

         RETURN l_return_flag;
      ELSIF g_start_bin IS NOT NULL AND g_end_bin IS NULL
      THEN
         SELECT 'Y'
           INTO l_return_flag
           FROM MTL_ITEM_LOCATIONS_KFV MIL, MTL_SECONDARY_LOCATORS MSL
          WHERE     msl.inventory_item_id = p_inventory_item_id
                AND MSL.ORGANIZATION_ID = P_ORGANIZATION_ID
                AND msl.secondary_locator = mil.inventory_location_id
                AND SUBSTR (mil.segment1, 3) >= g_start_bin
                AND ROWNUM <= 1;

         RETURN l_return_flag;
      ELSIF g_start_bin IS NULL AND g_end_bin IS NOT NULL
      THEN
         SELECT 'Y'
           INTO l_return_flag
           FROM MTL_ITEM_LOCATIONS_KFV MIL, MTL_SECONDARY_LOCATORS MSL
          WHERE     msl.inventory_item_id = p_inventory_item_id
                AND MSL.ORGANIZATION_ID = P_ORGANIZATION_ID
                AND msl.secondary_locator = mil.inventory_location_id
                AND SUBSTR (mil.segment1, 3) <= g_end_bin
                AND ROWNUM <= 1;

         RETURN l_return_flag;
      END IF;
   EXCEPTION
      WHEN OTHERS
      THEN
         RETURN 'N';
   END;


   FUNCTION get_final_end_qty (p_inventory_item_id    NUMBER,
                               p_organization_id      NUMBER,
                               p_date                 DATE)
      RETURN NUMBER
   IS
      l_final_end_qty   NUMBER;
   BEGIN
      SELECT SUM (mmt.transaction_quantity)
        INTO l_final_end_qty
        FROM mtl_material_transactions mmt
       WHERE     1 = 1
             AND TRUNC (mmt.transaction_date) <= p_date
             AND inventory_item_id = p_inventory_item_id
             AND organization_id = p_organization_id;

      RETURN l_final_end_qty;
   EXCEPTION
      WHEN OTHERS
      THEN
         RETURN NULL;
   END;


   FUNCTION get_schedule_part_flag (p_header_id NUMBER, p_line_id NUMBER)
      RETURN VARCHAR2
   IS
      l_return   VARCHAR2 (10);
   BEGIN
      SELECT 'Yes'
        INTO l_return
        FROM oe_price_adjustments_v
       WHERE     UPPER (adjustment_name) LIKE 'GSA%SCHEDULE%'
             AND header_id = p_header_id
             AND line_id = p_line_id;

      RETURN l_return;
   EXCEPTION
      WHEN OTHERS
      THEN
         RETURN 'No';
   END;


   FUNCTION get_sale_cost (p_customer_trx_id NUMBER)
      RETURN NUMBER
   IS
      l_amount   NUMBER;
   BEGIN
      SELECT SUM (ol.ordered_quantity * NVL (ol.unit_cost, 0))
        INTO l_amount
        FROM ra_customer_trx rct,
             ra_customer_trx_lines rctl,
             oe_order_headers oh,
             oe_order_lines ol,
             oe_payments oep
       WHERE     rctl.customer_trx_id = rct.customer_trx_id
             AND rctl.sales_order_line = ol.line_number
             AND rctl.inventory_item_id = ol.inventory_item_id
             AND ol.header_id = oh.header_id
             AND TO_CHAR (oh.order_number) =
                    TO_CHAR (rct.interface_header_attribute1)
             AND rctl.customer_trx_id = p_customer_trx_id
             AND oep.header_id = ol.header_id
             AND oep.line_id = ol.line_id;

      RETURN l_amount;
   EXCEPTION
      WHEN OTHERS
      THEN
         RETURN 0;
   END;

   FUNCTION get_open_sales_orders (p_inventory_item_id    NUMBER,
                                   p_organization_id      NUMBER)
      RETURN NUMBER
   IS
      l_order_numbers   NUMBER;
   BEGIN
      SELECT COUNT (oh.order_number)
        INTO l_order_numbers
        FROM oe_order_headers oh, oe_order_lines ol
       WHERE     oh.header_id = ol.header_id
             AND ol.inventory_item_id = p_inventory_item_id
             AND ol.ship_from_org_id = p_organization_id
             AND ol.flow_status_code NOT IN ('CLOSED', 'CANCELLED');

      RETURN l_order_numbers;
   EXCEPTION
      WHEN OTHERS
      THEN
         RETURN NULL;
   END;


   FUNCTION get_effective_period_num (p_period         VARCHAR2,
                                      p_ledger_id   IN NUMBER)
      RETURN NUMBER
   IS
      l_effective_period_num   gl_period_statuses.effective_period_num%TYPE;
   BEGIN
      SELECT gp.effective_period_num
        INTO l_effective_period_num
        FROM gl_period_statuses gp
       WHERE     gp.ledger_id = p_ledger_id
             AND gp.period_name = p_period
             AND gp.application_id = 101;

      RETURN l_effective_period_num;
   EXCEPTION
      WHEN OTHERS
      THEN
         RETURN NULL;
   END;

   FUNCTION get_inv_cat_class (p_inventory_item_id    NUMBER,
                               p_organization_id      NUMBER)
      RETURN VARCHAR2
   IS
      l_cat_class   VARCHAR2 (400);
   BEGIN
      SELECT mcv.concatenated_segments
        INTO l_cat_class
        FROM mtl_categories_kfv mcv,
             mtl_category_sets mcs,
             mtl_item_categories mic
       WHERE     mcs.category_set_name = 'Inventory Category'
             AND mcs.structure_id = mcv.structure_id
             AND mic.inventory_item_id = p_inventory_item_id
             AND mic.organization_id = p_organization_id
             AND mic.category_set_id = mcs.category_set_id
             AND mic.category_id = mcv.category_id;

      RETURN l_cat_class;
   EXCEPTION
      WHEN OTHERS
      THEN
         RETURN NULL;
   END;

   FUNCTION get_inv_gm (p_inventory_item_id    NUMBER,
                        p_organization_id      NUMBER,
                        p_item_category_id     NUMBER)
      RETURN NUMBER
   IS
      l_min_margin   NUMBER;
   BEGIN
      SELECT MAX (xopg.min_margin)
        INTO l_min_margin
        FROM mtl_parameters mtp, xxwc.xxwc_om_pricing_guardrail xopg
       WHERE     NVL (xopg.item_category_id, p_item_category_id) =
                    p_item_category_id
             AND (   p_inventory_item_id IS NULL
                  OR NVL (xopg.inventory_item_id, p_inventory_item_id) =
                        p_inventory_item_id)
             AND xopg.price_zone = mtp.attribute6
             AND mtp.organization_id = p_organization_id
             AND xopg.status = 'CURRENT';

      RETURN l_min_margin;
   EXCEPTION
      WHEN OTHERS
      THEN
         RETURN NULL;
   END;



   FUNCTION get_inv_cat_seg1 (p_inventory_item_id    NUMBER,
                              p_organization_id      NUMBER)
      RETURN VARCHAR2
   IS
      l_cat_class   VARCHAR2 (400);
   BEGIN
      SELECT mcv.segment1
        INTO l_cat_class
        FROM mtl_categories_kfv mcv,
             mtl_category_sets mcs,
             mtl_item_categories mic
       WHERE     mcs.category_set_name = 'Inventory Category'
             AND mcs.structure_id = mcv.structure_id
             AND mic.inventory_item_id = p_inventory_item_id
             AND mic.organization_id = p_organization_id
             AND mic.category_set_id = mcs.category_set_id
             AND mic.category_id = mcv.category_id;

      RETURN l_cat_class;
   EXCEPTION
      WHEN OTHERS
      THEN
         RETURN NULL;
   END;


   FUNCTION get_inv_prod_cat_class (p_inventory_item_id    NUMBER,
                                    p_organization_id      NUMBER)
      RETURN VARCHAR2
   IS
      l_cat_class   VARCHAR2 (400);
   BEGIN
      SELECT mcv.segment2
        INTO l_cat_class
        FROM mtl_categories_kfv mcv,
             mtl_category_sets mcs,
             mtl_item_categories mic
       WHERE     mcs.category_set_name = 'Product'
             AND mcs.structure_id = mcv.structure_id
             AND mic.inventory_item_id = p_inventory_item_id
             AND mic.organization_id = p_organization_id
             AND mic.category_set_id = mcs.category_set_id
             AND mic.category_id = mcv.category_id;

      RETURN l_cat_class;
   EXCEPTION
      WHEN OTHERS
      THEN
         RETURN NULL;
   END;


   FUNCTION get_inv_vel_cat_class (p_inventory_item_id    NUMBER,
                                   p_organization_id      NUMBER)
      RETURN VARCHAR2
   IS
      l_cat_class   VARCHAR2 (400);
   BEGIN
      SELECT mcv.segment1
        INTO l_cat_class
        FROM mtl_categories_kfv mcv,
             mtl_category_sets mcs,
             mtl_item_categories mic
       WHERE     mcs.category_set_name = 'Sales Velocity'
             AND mcs.structure_id = mcv.structure_id
             AND mic.inventory_item_id = p_inventory_item_id
             AND mic.organization_id = p_organization_id
             AND mic.category_set_id = mcs.category_set_id
             AND mic.category_id = mcv.category_id;

      RETURN l_cat_class;
   EXCEPTION
      WHEN OTHERS
      THEN
         RETURN NULL;
   END;


   FUNCTION get_inv_cat_class_desc (p_inventory_item_id    NUMBER,
                                    p_organization_id      NUMBER)
      RETURN VARCHAR2
   IS
      l_cat_class_desc   VARCHAR2 (400);
   BEGIN
      SELECT flex_val.description
        INTO l_cat_class_desc
        FROM mtl_categories_kfv mcv,
             mtl_category_sets mcs,
             mtl_item_categories mic,
             fnd_flex_values_vl flex_val,
             fnd_flex_value_sets flex_name
       WHERE     mcs.category_set_name = 'Inventory Category'
             AND mcs.structure_id = mcv.structure_id
             AND mic.category_set_id = mcs.category_set_id
             AND mic.category_id = mcv.category_id
             AND flex_value = mcv.segment2
             AND mic.inventory_item_id = p_inventory_item_id
             AND mic.organization_id = p_organization_id
             AND flex_name.flex_value_set_id = flex_val.flex_value_set_id
             AND flex_name.flex_value_set_name = 'XXWC_CATEGORY_CLASS';

      RETURN l_cat_class_desc;
   EXCEPTION
      WHEN OTHERS
      THEN
         RETURN NULL;
   END;

   FUNCTION get_reason_code (p_header_id NUMBER, p_line_id NUMBER)
      RETURN VARCHAR2
   IS
      l_reason_code   VARCHAR2 (500);
   BEGIN
      SELECT reason_code
        INTO l_reason_code
        FROM oe_reasons
       WHERE     header_id = p_header_id
             AND entity_code = 'LINE'
             AND entity_id = p_line_id;

      RETURN l_reason_code;
   EXCEPTION
      WHEN OTHERS
      THEN
         RETURN NULL;
   END;

   FUNCTION get_sales_profit (p_header_id NUMBER)
      RETURN NUMBER
   IS
      l_gross_profit   NUMBER;
   BEGIN
      SELECT ROUND (
                  SUM (ordered_quantity * NVL (unit_selling_price, 0))
                - SUM (ordered_quantity * NVL (unit_cost, 0)),
                2)
        INTO l_gross_profit
        FROM oe_order_lines
       WHERE header_id = p_header_id AND flow_status_code <> 'CANCELLED';

      RETURN l_gross_profit;
   EXCEPTION
      WHEN OTHERS
      THEN
         RETURN NULL;
   END;

   FUNCTION get_sales_profit_percentage (p_header_id NUMBER)
      RETURN NUMBER
   IS
      l_gross_profit_per   NUMBER;
      l_order_sales        NUMBER;
      l_sales_cost         NUMBER;
   BEGIN
      SELECT SUM (ordered_quantity * NVL (unit_selling_price, 0)),
             SUM (ordered_quantity * NVL (unit_cost, 0))
        INTO l_order_sales, l_sales_cost
        FROM oe_order_lines
       WHERE header_id = p_header_id AND flow_status_code <> 'CANCELLED';

      IF l_order_sales > 0
      THEN
         l_gross_profit_per :=
            ( (l_order_sales - l_sales_cost) / l_order_sales) * 100;
      ELSE
         l_gross_profit_per := 0;
      END IF;

      RETURN l_gross_profit_per;
   EXCEPTION
      WHEN OTHERS
      THEN
         RETURN NULL;
   END;

   PROCEDURE set_payment_type (p_payment_type VARCHAR2)
   IS
   BEGIN
      g_payment_type := p_payment_type;
   END;

   FUNCTION get_payment_type
      RETURN VARCHAR2
   IS
   BEGIN
      RETURN g_payment_type;
   END;

   FUNCTION get_gsa_modifier_amt (p_product_val       VARCHAR2,
                                  p_list_price_amt    NUMBER)
      RETURN NUMBER
   IS
      l_arithmetic_operator   VARCHAR2 (20);
      l_value                 NUMBER;
      l_gsa_modifier_amt      NUMBER;
   BEGIN
      SELECT ql.arithmetic_operator, ql.operand
        INTO l_arithmetic_operator, l_value
        FROM qp_list_headers qh, qp_modifier_summary_v ql
       WHERE     qh.list_header_id = ql.list_header_id
             AND qh.name = 'GSA PRICING'
             AND ql.product_attr_value = p_product_val;

      -- AND     nvl(trunc(ql.end_date_active),trunc(SYSDATE))>=trunc(SYSDATE);

      IF l_arithmetic_operator = '%'
      THEN
         l_gsa_modifier_amt := ( (p_list_price_amt) * (l_value / 100));

         IF NVL (p_list_price_amt, 0) = 0
         THEN
            RETURN l_gsa_modifier_amt;
         ELSE
            RETURN (p_list_price_amt - l_gsa_modifier_amt);
         END IF;
      ELSIF l_arithmetic_operator = 'NEWPRICE'
      THEN
         RETURN l_value;
      ELSE
         RETURN 0;
      END IF;
   EXCEPTION
      WHEN OTHERS
      THEN
         RETURN 0;
   END;



   FUNCTION get_gsa_disc_amt (p_product_val       VARCHAR2,
                              p_list_price_amt    NUMBER)
      RETURN NUMBER
   IS
      l_arithmetic_operator   VARCHAR2 (20);
      l_value                 NUMBER;
      l_gsa_modifier_amt      NUMBER;
   BEGIN
      SELECT ql.arithmetic_operator, ql.operand
        INTO l_arithmetic_operator, l_value
        FROM qp_list_headers qh, qp_modifier_summary_v ql
       WHERE     qh.list_header_id = ql.list_header_id
             AND qh.name = 'GSA PRICING'
             AND ql.product_attr_value = p_product_val
             AND NVL (TRUNC (ql.end_date_active), TRUNC (SYSDATE)) >=
                    TRUNC (SYSDATE);

      IF l_arithmetic_operator = '%'
      THEN
         l_gsa_modifier_amt := ( (p_list_price_amt) * (l_value / 100));
      ELSIF l_arithmetic_operator = 'NEWPRICE'
      THEN
         l_gsa_modifier_amt := l_value;
      ELSE
         RETURN 0;
      END IF;

      RETURN l_gsa_modifier_amt;
   EXCEPTION
      WHEN OTHERS
      THEN
         RETURN NULL;
   END;

   FUNCTION get_adj_auto_modifier_amt (p_header_id NUMBER, p_line_id NUMBER)
      RETURN NUMBER
   IS
      l_adj_amt   NUMBER;
   BEGIN
      SELECT NVL ( (SUM (adj_auto.adjusted_amount) * (-1)), 0)
        INTO l_adj_amt
        FROM oe_price_adjustments_v adj_auto
       WHERE     adj_auto.header_id = p_header_id
             AND adj_auto.line_id = p_line_id
             AND adj_auto.automatic_flag = 'Y';

      RETURN l_adj_amt;
   EXCEPTION
      WHEN OTHERS
      THEN
         RETURN 0;
   END;

   FUNCTION get_line_modifier_name (p_header_id NUMBER, p_line_id NUMBER)
      RETURN VARCHAR2
   IS
      l_mod_name   VARCHAR2 (240);
   BEGIN
      SELECT MAX (adjustment_name)
        INTO l_mod_name
        FROM oe_price_adjustments_v adj
       WHERE     adj.header_id = p_header_id
             AND adj.line_id = p_line_id
             AND price_adjustment_id IN (SELECT MAX (price_adjustment_id)
                                           FROM oe_price_adjustments_v adj1
                                          WHERE     adj1.header_id =
                                                       p_header_id
                                                AND adj1.line_id = p_line_id
                                                AND adj1.list_line_type_code =
                                                       'DIS'
                                                AND adj1.applied_flag = 'Y');

      RETURN l_mod_name;
   EXCEPTION
      WHEN OTHERS
      THEN
         RETURN 0;
   END;


   FUNCTION get_adj_auto_modifier_name (p_header_id NUMBER, p_line_id NUMBER)
      RETURN VARCHAR2
   IS
      l_adj_name   VARCHAR2 (240);
   BEGIN
      SELECT MAX (adjustment_name)
        INTO l_adj_name
        FROM oe_price_adjustments_v adj_auto
       WHERE     adj_auto.header_id = p_header_id
             AND adj_auto.line_id = p_line_id
             AND adj_auto.automatic_flag = 'Y';

      RETURN l_adj_name;
   EXCEPTION
      WHEN OTHERS
      THEN
         RETURN 0;
   END;

   FUNCTION get_po_om_order_number (p_header_id          NUMBER,
                                    p_line_id            NUMBER,
                                    p_trx_src_type_id    NUMBER)
      RETURN VARCHAR2
   IS
      l_order_number   VARCHAR2 (240);
   BEGIN
      IF p_trx_src_type_id IN (2, 12)
      THEN
         BEGIN
            SELECT order_number
              INTO l_order_number
              FROM oe_order_headers_all oh, oe_order_lines_all ol
             WHERE ol.header_id = oh.header_id AND ol.line_id = p_line_id;
         EXCEPTION
            WHEN OTHERS
            THEN
               l_order_number := NULL;
         END;
      ELSIF p_trx_src_type_id = 1
      THEN
         BEGIN
            SELECT poh.segment1
              INTO l_order_number
              FROM po_headers poh, rcv_transactions rcv
             WHERE     rcv.transaction_id = p_header_id
                   AND rcv.po_header_id = poh.po_header_id;
         EXCEPTION
            WHEN OTHERS
            THEN
               l_order_number := NULL;
         END;
      ELSE
         NULL;
      END IF;

      RETURN l_order_number;
   EXCEPTION
      WHEN OTHERS
      THEN
         RETURN NULL;
   END;


   FUNCTION get_item_avg_cost (p_inventory_item_id    NUMBER,
                               p_organization_id      NUMBER)
      RETURN NUMBER
   IS
      l_avg_cost   NUMBER;
   BEGIN
      SELECT AVG (item_cost)
        INTO l_avg_cost
        FROM cst_item_costs
       WHERE     cost_type_id = 2
             AND inventory_item_id IN (SELECT component_item_id
                                         FROM bom_bill_of_materials bbm,
                                              bom_inventory_components bic
                                        WHERE     bbm.bill_sequence_id =
                                                     bic.bill_sequence_id
                                              AND bbm.assembly_item_id =
                                                     p_inventory_item_id -- and   nvl(bbm.organization_id     = P_Organization_Id
                                                                        );

      RETURN l_avg_cost;
   EXCEPTION
      WHEN OTHERS
      THEN
         RETURN 0;
   END;

   FUNCTION get_fly_item_selling_price (p_header_id            NUMBER,
                                        p_inventory_item_id    NUMBER)
      RETURN NUMBER
   IS
      l_selling_price   NUMBER;
   BEGIN
      SELECT SUM (NVL (ol.unit_selling_price, 0))
        INTO l_selling_price
        FROM oe_order_lines ol
       WHERE     ol.header_id = p_header_id
             AND ol.inventory_item_id IN (SELECT component_item_id
                                            FROM bom_bill_of_materials bbm,
                                                 bom_inventory_components bic
                                           WHERE     bbm.bill_sequence_id =
                                                        bic.bill_sequence_id
                                                 AND bbm.assembly_item_id =
                                                        p_inventory_item_id -- and   bbm.organization_id     = ol.ship_from_org_id
                                                                           );

      RETURN l_selling_price;
   EXCEPTION
      WHEN OTHERS
      THEN
         RETURN 0;
   END;

   FUNCTION get_line_ship_confirm_flag (p_header_id NUMBER, p_line_id NUMBER)
      RETURN VARCHAR2
   IS
      l_flag   VARCHAR2 (2);
   BEGIN
      SELECT MAX ('Y')
        INTO l_flag
        FROM wsh_delivery_details
       WHERE     source_header_id = p_header_id
             AND source_line_id = p_line_id
             AND released_status IN ('C');

      RETURN l_flag;
   EXCEPTION
      WHEN OTHERS
      THEN
         RETURN 'N';
   END;

   FUNCTION get_rental_unit_selling_price (p_line_id NUMBER)
      RETURN NUMBER
   IS
      l_price   NUMBER;
   BEGIN
      SELECT MAX (unit_selling_price)
        INTO l_price
        FROM oe_order_lines
       WHERE ordered_item = 'Rental Charge' AND link_to_line_id = p_line_id;

      RETURN l_price;
   EXCEPTION
      WHEN OTHERS
      THEN
         RETURN 0;
   END;


   FUNCTION get_line_shipment_date (p_line_id NUMBER)
      RETURN DATE
   IS
      l_ship_date   DATE;
   BEGIN
      SELECT wnd.initial_pickup_date
        INTO l_ship_date
        FROM wsh_delivery_assignments wda,
             wsh_delivery_details wdd,
             wsh_new_deliveries wnd
       WHERE     wda.delivery_detail_id = wdd.delivery_detail_id
             AND wnd.delivery_id = wda.delivery_id
             AND source_line_id = p_line_id
             AND source_code = 'OE';


      RETURN l_ship_date;
   EXCEPTION
      WHEN OTHERS
      THEN
         RETURN NULL;
   END;

   FUNCTION get_trader (p_header_id NUMBER, p_line_id NUMBER)
      RETURN NUMBER
   IS
      l_trader   NUMBER := 0;
   BEGIN
      SELECT COUNT (*)
        INTO l_trader
        FROM oe_price_adjustments_v
       WHERE     header_id = p_header_id
             AND line_id = p_line_id
             AND adjustment_name LIKE '%TRADER%';


      RETURN l_trader;
   EXCEPTION
      WHEN OTHERS
      THEN
         RETURN 0;
   END;

   FUNCTION get_mfgadjust (p_header_id NUMBER, p_line_id NUMBER)
      RETURN NUMBER
   IS
      l_mfgadj   NUMBER := 0;
   BEGIN
      SELECT -1 * adjusted_amount                                    --operand
        INTO l_mfgadj
        FROM oe_price_adjustments_v oe, qp_list_headers qph
       WHERE     oe.header_id = p_header_id
             AND oe.line_id = p_line_id
             AND oe.list_header_id = qph.list_header_id
             AND (   adjustment_name LIKE '%MFG DISC%'
                  OR (qph.attribute10 = 'Manufacturer''s Promotion'));


      RETURN l_mfgadj;
   EXCEPTION
      WHEN OTHERS
      THEN
         RETURN 0;
   END;

   FUNCTION get_subinventory (p_organization_id NUMBER)
      RETURN VARCHAR2
   IS
      l_sub_inv   VARCHAR2 (50);
   BEGIN
      SELECT secondary_inventory_name
        INTO l_sub_inv
        FROM mtl_secondary_inventories
       WHERE organization_id = p_organization_id;


      RETURN l_sub_inv;
   EXCEPTION
      WHEN OTHERS
      THEN
         RETURN NULL;
   END;


   FUNCTION get_last_updt_by (p_line_id NUMBER)
      RETURN VARCHAR2
   IS
      l_name   VARCHAR2 (100);
   BEGIN
      SELECT ppf.full_name
        INTO l_name
        FROM oe_order_lines ol, per_all_people_f ppf, fnd_user fu
       WHERE     ol.last_updated_by = fu.user_id --   and ol.flow_status_code  = 'CANCELLED'
             AND fu.employee_id = ppf.person_id(+)
             AND ol.creation_date BETWEEN ppf.effective_start_date
                                      AND NVL (ppf.effective_end_date,
                                               ol.creation_date)
             AND ol.line_id = p_line_id;

      RETURN l_name;
   EXCEPTION
      WHEN OTHERS
      THEN
         RETURN NULL;
   END;

   FUNCTION get_cash_amount (p_header_id NUMBER)
      RETURN NUMBER
   IS
      l_cash_amount   NUMBER;
   BEGIN
      SELECT rctl.extended_amount
        INTO l_cash_amount
        FROM oe_order_headers oh,
             oe_order_lines ol,
             oe_payments oe,
             ra_customer_trx_lines rctl
       WHERE     1 = 1
             AND oh.header_id = ol.header_id
             AND oe.header_id = oh.header_id
             AND TO_CHAR (oh.order_number) = rctl.interface_line_attribute1
             AND TO_CHAR (ol.line_id) = rctl.interface_line_attribute6
             AND oe.header_id = p_header_id;



      RETURN l_cash_amount;
   EXCEPTION
      WHEN OTHERS
      THEN
         RETURN 0;
   END;

   FUNCTION get_isr_rpt_dc_mod_sub
      RETURN VARCHAR2
   IS
   BEGIN
      RETURN g_isr_rpt_dc_mod_sub;
   END;


   FUNCTION get_item_purchased (p_inventory_item_id    NUMBER,
                                p_organization_id      NUMBER)
      RETURN NUMBER
   IS
      l_purchased_item   NUMBER;
   BEGIN
      SELECT COUNT (DISTINCT poh.po_header_id)
        INTO l_purchased_item
        FROM po_headers poh,
             po_lines pol,
             po_line_locations poll,
             mtl_system_items_kfv msi
       WHERE     poh.po_header_id = pol.po_header_id
             AND pol.po_line_id = poll.po_line_id
             AND msi.inventory_item_id = pol.item_id
             AND msi.organization_id = poll.ship_to_organization_id
             AND msi.inventory_item_id = p_inventory_item_id
             AND msi.organization_id = p_organization_id
             AND TRUNC (poh.creation_date) >= SYSDATE - 365;

      --and trunc(poh.creation_date) <=nvl(g_date_from,trunc(poh.creation_date))
      -- and trunc(poh.creation_date) >=nvl(g_date_to,trunc(poh.creation_date)) ;


      RETURN l_purchased_item;
   EXCEPTION
      WHEN OTHERS
      THEN
         RETURN 0;
   END;

   FUNCTION get_isr_item_cost (p_inventory_item_id    NUMBER,
                               p_organization_id      NUMBER)
      RETURN NUMBER
   IS
      l_item_cost   NUMBER;
   BEGIN
      SELECT unit_price
        INTO l_item_cost
        FROM po_lines_all
       WHERE po_line_id IN (SELECT MAX (pol.po_line_id)
                              FROM po_headers poh,
                                   po_line_locations poll,
                                   po_lines pol,
                                   po_vendors pov
                             WHERE     poh.type_lookup_code = 'BLANKET'
                                   AND poh.po_header_id = pol.po_header_id
                                   AND pol.po_line_id = poll.po_line_id(+)
                                   AND NVL (poh.cancel_flag, 'N') = 'N' --and  poll.quantity_received > 0
                                   AND pol.item_id = p_inventory_item_id
                                   AND (   poh.global_agreement_flag = 'Y'
                                        OR poll.ship_to_organization_id =
                                              p_organization_id)
                                   AND poh.vendor_id = pov.vendor_id
                                   AND pov.segment1 =
                                          get_vendor_number (
                                             p_inventory_item_id,
                                             p_organization_id));

      RETURN l_item_cost;
   EXCEPTION
      WHEN OTHERS
      THEN
         RETURN NULL;
   END;

   FUNCTION get_isr_bpa_doc (p_inventory_item_id    NUMBER,
                             p_organization_id      NUMBER)
      RETURN NUMBER
   IS
      l_bpa_doc   NUMBER;
   BEGIN
      SELECT segment1
        INTO l_bpa_doc
        FROM po_headers_all
       WHERE po_header_id IN (SELECT MAX (poh.po_header_id)
                                FROM po_headers poh,
                                     po_line_locations poll,
                                     po_lines_all pol,
                                     po_vendors pov
                               WHERE     poh.type_lookup_code = 'BLANKET'
                                     AND poh.po_header_id = pol.po_header_id
                                     AND NVL (poh.cancel_flag, 'N') = 'N'
                                     AND pol.po_line_id = poll.po_line_id(+) --and  poll.quantity_received > 0
                                     AND pol.item_id = p_inventory_item_id
                                     AND (   poh.global_agreement_flag = 'Y'
                                          OR poll.ship_to_organization_id =
                                                p_organization_id)
                                     AND poh.vendor_id = pov.vendor_id
                                     AND pov.segment1 =
                                            get_vendor_number (
                                               p_inventory_item_id,
                                               p_organization_id));

      RETURN l_bpa_doc;
   EXCEPTION
      WHEN OTHERS
      THEN
         RETURN NULL;
   END;

   FUNCTION get_isr_open_po_qty (p_inventory_item_id    NUMBER,
                                 p_organization_id      NUMBER)
      RETURN NUMBER
   IS
      l_open_qty               NUMBER;
      l_internal_receipt_qty   NUMBER;
      l_onorder_qty            NUMBER;
      l_component_qty          NUMBER;
      l_assembly_qy            NUMBER;
   BEGIN
      SELECT SUM (qty)
        INTO l_open_qty
        FROM (SELECT SUM (
                          poll.quantity
                        - NVL (poll.quantity_received, 0)
                        - NVL (poll.quantity_cancelled, 0))
                        qty
                FROM po_headers poh,
                     po_line_locations poll,
                     po_lines pol,
                     po_vendors pov
               WHERE     1 = 1
                     AND NVL (poh.cancel_flag, 'N') = 'N'
                     AND NVL (pol.cancel_flag, 'N') = 'N'
                     AND NVL (poll.cancel_flag, 'N') = 'N'
                     AND NVL (poh.closed_code, 'OPEN') NOT IN ('FINALLY CLOSED',
                                                               'CLOSED')
                     AND NVL (poll.closed_code, 'OPEN') NOT IN ('CLOSED',
                                                                'FINALLY CLOSED')
                     AND NVL (pol.closed_code, 'OPEN') NOT IN ('CLOSED',
                                                               'FINALLY CLOSED')
                     AND poh.type_lookup_code IN ('STANDARD',
                                                  'BLANKET',
                                                  'PLANNED')
                     AND poh.po_header_id = pol.po_header_id
                     AND pol.po_line_id = poll.po_line_id
                     AND poll.quantity - NVL (poll.quantity_received, 0) > 0
                     AND pol.item_id = p_inventory_item_id
                     AND poll.ship_to_organization_id = p_organization_id
                     AND poh.vendor_id = pov.vendor_id);

      --query to get internal receipt qty---

      SELECT SUM (oel.ordered_quantity - NVL (oel.fulfilled_quantity, 0))
        INTO l_internal_receipt_qty
        FROM apps.oe_order_lines_all oel,
             apps.oe_order_headers_all oeh,
             apps.po_requisition_headers_all prh
       WHERE     oeh.header_id = oel.header_id
             AND oeh.source_document_id = prh.requisition_header_id
             AND oel.source_type_code = 'INTERNAL'
             AND oel.flow_status_code = 'CLOSED'
             AND oel.inventory_item_id = p_inventory_item_id
             AND oel.ship_from_org_id = p_organization_id
             AND NOT EXISTS
                        (SELECT 1
                           FROM apps.rcv_transactions rt,
                                apps.po_requisition_lines_all prl
                          WHERE     rt.source_document_code = 'REQ'
                                AND rt.requisition_line_id =
                                       prl.requisition_line_id
                                AND prl.requisition_header_id =
                                       prh.requisition_header_id);

      --how ROP and Min-Max get the WIP Demand (If the item is a component on a work order).

      SELECT SUM (o.required_quantity - o.quantity_issued)
        INTO l_component_qty
        FROM wip_discrete_jobs d, wip_requirement_operations o
       WHERE     o.wip_entity_id = d.wip_entity_id
             AND o.organization_id = d.organization_id
             AND d.organization_id = p_organization_id
             AND o.inventory_item_id = p_inventory_item_id
             AND o.date_required <= SYSDATE
             AND o.required_quantity >= o.quantity_issued
             AND o.operation_seq_num > 0
             AND d.status_type IN (1,
                                   3,
                                   4,
                                   6)
             AND o.wip_supply_type NOT IN (5, 6)
             --  AND nvl(o.supply_subinventory,1) = decode(:subinv,NULL,nvl(o.supply_subinventory,1),:subinv)
             AND NOT EXISTS
                        (SELECT /*+ index(mtl MTL_DEMAND_N12)*/
                                wip.wip_entity_id
                           FROM wip_so_allocations wip, mtl_demand mtl
                          WHERE     wip_entity_id = o.wip_entity_id
                                AND wip.organization_id = p_organization_id
                                AND wip.organization_id = mtl.organization_id
                                AND wip.demand_source_header_id =
                                       mtl.demand_source_header_id
                                AND wip.demand_source_line =
                                       mtl.demand_source_line
                                AND wip.demand_source_delivery =
                                       mtl.demand_source_delivery
                                AND mtl.inventory_item_id =
                                       p_inventory_item_id);

      --how ROP and Min-Max get WIP Supply (If it's an assembly item on a work order).

      SELECT SUM (
                  NVL (start_quantity, 0)
                - NVL (quantity_completed, 0)
                - NVL (quantity_scrapped, 0))
        INTO l_assembly_qy
        FROM wip_discrete_jobs
       WHERE     organization_id = p_organization_id
             AND primary_item_id = p_inventory_item_id
             AND status_type IN (1,
                                 3,
                                 4,
                                 6)
             AND job_type IN (1, 3)
             AND scheduled_completion_date <=
                    TO_DATE (TO_CHAR (SYSDATE), 'DD-MON-RR');


      l_onorder_qty :=
           NVL (l_open_qty, 0)
         + NVL (l_internal_receipt_qty, 0)
         + NVL (l_component_qty, 0)
         + NVL (l_assembly_qy, 0);

      RETURN l_onorder_qty;
   EXCEPTION
      WHEN OTHERS
      THEN
         RETURN NULL;
   END;


   FUNCTION get_isr_open_req_qty (p_inventory_item_id    NUMBER,
                                  p_organization_id      NUMBER)
      RETURN NUMBER
   IS
      l_open_qty   NUMBER;
   BEGIN
      SELECT SUM (prdl.req_line_quantity) qty
        INTO l_open_qty
        FROM po_requisition_headers prh,
             po_req_distributions prdl,
             po_requisition_lines prl
       WHERE     prh.requisition_header_id = prl.requisition_header_id
             AND prdl.requisition_line_id = prl.requisition_line_id
             AND item_id = p_inventory_item_id
             AND NVL (prh.cancel_flag, 'N') = 'N'
             AND NVL (prl.cancel_flag, 'N') = 'N'
             AND NVL (prl.closed_code, 'OPEN') = 'OPEN'
             AND prdl.gl_encumbered_date IS NOT NULL       --added by srinivas
             AND destination_organization_id = p_organization_id
             AND NOT EXISTS
                        (SELECT 1
                           FROM po_action_history pah
                          WHERE     pah.object_id = prh.requisition_header_id
                                AND pah.object_type_code = 'REQUISITION'
                                AND pah.action_code = 'CANCEL')
             AND NOT EXISTS
                    (SELECT 1
                       FROM po_distributions pod
                      WHERE pod.req_distribution_id = prdl.distribution_id)
             AND NOT EXISTS
                        (SELECT 1
                           FROM oe_order_headers oh
                          WHERE     order_type_id = 1011
                                AND oh.source_document_id =
                                       prl.requisition_header_id);

      --   and pov.segment1                 = Get_Vendor_Number(p_inventory_item_id,p_organization_id)


      RETURN l_open_qty;
   EXCEPTION
      WHEN OTHERS
      THEN
         RETURN NULL;
   END;

   FUNCTION get_isr_avail_qty (p_inventory_item_id    NUMBER,
                               p_organization_id      NUMBER)
      RETURN NUMBER
   IS
      l_demand_qty   NUMBER := 0;
      l_avail_qty    NUMBER;
   BEGIN
      --This is code is commneted by srinivas--
      /* select sum(ol.ordered_quantity-nvl(ol.fulfilled_quantity,0))
       into  l_demand_qty
       from  oe_order_lines_all ol
       where OL.INVENTORY_ITEM_ID     = P_INVENTORY_ITEM_ID
       and   OL.SHIP_FROM_ORG_ID      = P_ORGANIZATION_ID
       and   (ol.ordered_quantity-nvl(ol.fulfilled_quantity,0))>0
       and   ol.flow_status_code not  in ('CANCELLED','CLOSED');*/

      --The following code added by srinivas---

      SELECT NVL (SUM (mr.reservation_quantity), 0)
        INTO l_demand_qty
        FROM oe_order_lines ol, mtl_sales_orders mso, mtl_reservations mr
       WHERE     mr.inventory_item_id = p_inventory_item_id
             AND mr.organization_id = p_organization_id
             AND mr.demand_source_line_id = ol.line_id --AND  ol.flow_status_CODE not in ('CLOSED')
             AND mr.demand_source_header_id = mso.sales_order_id;

      l_avail_qty :=
         (  get_onhand_inv (p_inventory_item_id, p_organization_id)
          - NVL (l_demand_qty, 0));
      RETURN l_avail_qty;
   EXCEPTION
      WHEN NO_DATA_FOUND
      THEN
         RETURN l_avail_qty;
   END;

   FUNCTION get_isr_ss_cnt (p_inventory_item_id    NUMBER,
                            p_organization_id      NUMBER)
      RETURN NUMBER
   IS
      l_ss_item_cnt   NUMBER;
   BEGIN
      SELECT COUNT (*)
        INTO l_ss_item_cnt
        FROM mtl_system_items_kfv msi
       WHERE     msi.inventory_item_id = p_inventory_item_id
             AND msi.source_organization_id = p_organization_id;

      RETURN l_ss_item_cnt;
   EXCEPTION
      WHEN OTHERS
      THEN
         RETURN NULL;
   END;

   FUNCTION get_order_line_status (p_line_id IN NUMBER)
      RETURN VARCHAR2
   IS
      l_line_status   VARCHAR2 (50);
      l_status        VARCHAR2 (50);
   BEGIN
      SELECT ol.flow_status_code
        INTO l_line_status
        FROM oe_order_lines ol
       WHERE ol.line_id = p_line_id;

      l_status :=
         apps.oe_line_status_pub.get_line_status (p_line_id, l_line_status);

      RETURN l_status;
   EXCEPTION
      WHEN OTHERS
      THEN
         RETURN NULL;
   END;

   FUNCTION get_order_line_status (p_line_id            IN NUMBER,
                                   p_flow_status_code   IN VARCHAR2)
      RETURN VARCHAR2
   IS
      l_line_status   VARCHAR2 (50);
      l_status        VARCHAR2 (50);
   BEGIN
      IF p_flow_status_code = 'AWAITING_SHIPPING'
      THEN
         BEGIN
            SELECT MAX (released_status)
              INTO l_line_status
              FROM wsh_delivery_details
             WHERE source_line_id = p_line_id;

            IF l_line_status = 'B'
            THEN
               RETURN 'Backordered';
            END IF;
         EXCEPTION
            WHEN OTHERS
            THEN
               NULL;
         END;
      END IF;

      SELECT ol.flow_status_code
        INTO l_line_status
        FROM oe_order_lines ol
       WHERE ol.line_id = p_line_id;

      l_status :=
         apps.oe_line_status_pub.get_line_status (p_line_id,
                                                  p_flow_status_code);

      RETURN l_status;
   EXCEPTION
      WHEN OTHERS
      THEN
         RETURN NULL;
   END;


   FUNCTION get_grossmgr_rpt_sale_rep_flag (p_salerep_number VARCHAR2)
      RETURN VARCHAR2
   IS
      l_return_flag   VARCHAR2 (2);
   BEGIN
      IF g_manger_type IS NULL
      THEN
         BEGIN
            SELECT 'M'
              INTO g_manger_type
              FROM xxwc.dw_salesreps ds
             WHERE 1 = 1 AND ntid = fnd_global.user_name;
         EXCEPTION
            WHEN NO_DATA_FOUND
            THEN
               NULL;
         END;

         BEGIN
            SELECT DISTINCT 'DM', districtmanager_hremployeeid
              INTO g_manger_type, g_salerep_num
              FROM xxwc.dw_salesreps ds
             WHERE 1 = 1 AND districtmanager_ntid = fnd_global.user_name;
         EXCEPTION
            WHEN NO_DATA_FOUND
            THEN
               NULL;
         END;

         BEGIN
            SELECT DISTINCT 'RM', regionalmanager_hremployeeid
              INTO g_manger_type, g_salerep_num
              FROM xxwc.dw_salesreps ds
             WHERE 1 = 1 AND regionalmanager_ntid = fnd_global.user_name;
         EXCEPTION
            WHEN NO_DATA_FOUND
            THEN
               NULL;
         END;

         BEGIN
            SELECT DISTINCT 'NM', nationalmanager_hremployeeid
              INTO g_manger_type, g_salerep_num
              FROM xxwc.dw_salesreps ds
             WHERE 1 = 1 AND nationalmanager_ntid = fnd_global.user_name;
         EXCEPTION
            WHEN NO_DATA_FOUND
            THEN
               NULL;
         END;
      ELSE
         IF g_manger_type = 'M'
         THEN
            RETURN 'Y';
         ELSIF g_manger_type = 'DM'
         THEN
            BEGIN
               SELECT MAX ('Y')
                 INTO l_return_flag
                 FROM xxwc.dw_salesreps ds
                WHERE     districtmanager_hremployeeid = g_salerep_num
                      AND hremployeeid = p_salerep_number;
            EXCEPTION
               WHEN NO_DATA_FOUND
               THEN
                  NULL;
            END;
         ELSIF g_manger_type = 'RM'
         THEN
            BEGIN
               SELECT MAX ('Y')
                 INTO l_return_flag
                 FROM xxwc.dw_salesreps ds
                WHERE     regionalmanager_hremployeeid = g_salerep_num
                      AND (   hremployeeid = p_salerep_number
                           OR districtmanager_hremployeeid = p_salerep_number);
            EXCEPTION
               WHEN NO_DATA_FOUND
               THEN
                  NULL;
            END;
         ELSIF g_manger_type = 'NM'
         THEN
            BEGIN
               SELECT MAX ('Y')
                 INTO l_return_flag
                 FROM xxwc.dw_salesreps ds
                WHERE     nationalmanager_hremployeeid = g_salerep_num
                      AND (   hremployeeid = p_salerep_number
                           OR districtmanager_hremployeeid = p_salerep_number
                           OR regionalmanager_hremployeeid = p_salerep_number);
            EXCEPTION
               WHEN NO_DATA_FOUND
               THEN
                  NULL;
            END;
         END IF;
      END IF;

      RETURN NVL (l_return_flag, 'N');
   END;

   FUNCTION get_invoice_amt (p_header_id IN NUMBER)
      RETURN NUMBER
   IS
      l_inv_amt   NUMBER;
   BEGIN
      SELECT SUM (ps.amount_due_original)
        INTO l_inv_amt
        FROM oe_order_headers oh,
             ra_customer_trx rct,
             ar_payment_schedules ps
       WHERE     1 = 1
             AND ps.customer_trx_id = rct.customer_trx_id
             AND TO_CHAR (oh.order_number) = rct.interface_header_attribute1
             AND rct.interface_header_context = 'ORDER ENTRY'
             AND oh.header_id = p_header_id;

      RETURN l_inv_amt;
   EXCEPTION
      WHEN OTHERS
      THEN
         RETURN 0;
   END;

   FUNCTION get_cust_invoice_amt (p_trx_id IN NUMBER)
      RETURN NUMBER
   IS
      l_inv_amt   NUMBER;
   BEGIN
      SELECT SUM (ps.amount_due_original)
        INTO l_inv_amt
        FROM ar_payment_schedules ps
       WHERE 1 = 1 AND ps.customer_trx_id = p_trx_id;

      RETURN l_inv_amt;
   EXCEPTION
      WHEN OTHERS
      THEN
         RETURN 0;
   END;

   FUNCTION get_unit_cost (p_inventory_item_id    NUMBER,
                           p_organization_id      NUMBER,
                           p_segment1             VARCHAR2)
      RETURN NUMBER
   IS
      l_unit_cost           NUMBER;
      l_line_id             NUMBER;
      l_inventory_item_id   NUMBER;
   BEGIN
      IF INSTR (UPPER (p_segment1), 'RR') <> 0
      THEN
         SELECT MAX (inventory_item_id)
           INTO l_inventory_item_id
           FROM mtl_system_items_kfv
          WHERE     segment1 LIKE SUBSTR (p_segment1, 3)
                AND organization_id = p_organization_id;
      ELSE
         SELECT MAX (inventory_item_id)
           INTO l_inventory_item_id
           FROM mtl_system_items_kfv
          WHERE     segment1 LIKE SUBSTR (p_segment1, 2)
                AND organization_id = p_organization_id;
      END IF;


      /* Select Max(Line_Id)
       into l_line_id
          From Oe_Order_Lines  Ol
          Where Ol.Inventory_Item_Id         =L_Inventory_Item_Id
          And Ol.Ship_From_Org_Id            =P_Organization_Id ;

          If L_Line_Id Is Null Then
           Select Oll.unit_cost
          INTO  l_unit_cost
          From Oe_Order_Lines  Oll
          Where Line_Id In
          (Select Max(Line_Id) From
          Oe_Order_Lines  Ol
           Where Ol.Inventory_Item_Id         =P_Inventory_Item_Id
           And Ol.Ship_From_Org_Id            =P_Organization_Id
           );
       Else
          Select Ol1.Unit_Cost
          Into  L_Unit_Cost
          From Oe_Order_Lines Ol1 Where
          Line_Id =L_Line_Id;
      end if;
      */

      l_unit_cost :=
         apps.cst_cost_api.get_item_cost (1,
                                          l_inventory_item_id,
                                          p_organization_id);
      RETURN l_unit_cost;
   EXCEPTION
      WHEN OTHERS
      THEN
         l_unit_cost := 2;
         RETURN l_unit_cost;
   END;

   FUNCTION get_subinv_onhand_qty (p_inventory_item_id    NUMBER,
                                   p_organization_id      NUMBER,
                                   p_date                 DATE DEFAULT NULL)
      RETURN NUMBER
   IS
      l_subinv_onhand_qty   NUMBER;
   BEGIN
      SELECT NVL (SUM (moq.transaction_quantity), 0)
        INTO l_subinv_onhand_qty
        FROM mtl_material_transactions moq, mtl_secondary_inventories msinv
       WHERE     moq.inventory_item_id = p_inventory_item_id
             AND moq.organization_id = p_organization_id
             AND moq.organization_id = msinv.organization_id(+)
             AND moq.subinventory_code = msinv.secondary_inventory_name
             AND UPPER (msinv.secondary_inventory_name) IN ('DBRENTAL',
                                                            'DRRENTAL',
                                                            'LOSTRENTAL',
                                                            'RENTAL',
                                                            'RENTAL N/R',
                                                            'RENTALHLD')
             AND TRUNC (moq.transaction_date) <= NVL (p_date, g_date_to);

      RETURN l_subinv_onhand_qty;
   EXCEPTION
      WHEN OTHERS
      THEN
         RETURN 0;
   END;

   FUNCTION get_fixed_assets_cnt (p_organization_id IN NUMBER)
      RETURN NUMBER
   IS
      l_cnt   NUMBER;
   BEGIN
      SELECT COUNT (*)
        INTO l_cnt
        FROM mtl_system_items_kfv
       WHERE segment1 LIKE 'R%' AND organization_id = p_organization_id;

      RETURN l_cnt;
   EXCEPTION
      WHEN OTHERS
      THEN
         RETURN 0;
   END;

   FUNCTION get_bo_flag (p_line_id IN NUMBER)
      RETURN VARCHAR2
   IS
      l_flag   NUMBER;
   BEGIN
      SELECT MAX ('Y')
        INTO l_flag
        FROM wsh_delivery_details
       WHERE released_status = 'B' AND source_line_id = p_line_id;

      RETURN l_flag;
   EXCEPTION
      WHEN OTHERS
      THEN
         RETURN 'N';
   END;

   FUNCTION get_backorder_qty (p_header_id IN NUMBER, p_line_id IN NUMBER)
      RETURN VARCHAR2
   IS
      l_qty   NUMBER;
   BEGIN
      SELECT NVL (ol.ordered_quantity, 0) ordered_quantity
        INTO l_qty
        FROM oe_order_lines ol, wsh_delivery_details wdd
       WHERE     wdd.released_status = 'B'
             AND wdd.source_header_id = ol.header_id
             AND wdd.source_line_id = ol.line_id
             AND wdd.source_header_id = p_header_id
             AND wdd.source_line_id = p_line_id;

      RETURN l_qty;
   EXCEPTION
      WHEN OTHERS
      THEN
         RETURN 0;
   END;


   FUNCTION get_line_rma_flag (p_line_id NUMBER)
      RETURN VARCHAR2
   IS
      l_return_flag   VARCHAR2 (5);
   BEGIN
      SELECT 'Y'
        INTO l_return_flag
        FROM rcv_shipment_lines
       WHERE     source_document_code = 'RMA'
             AND oe_order_line_id = p_line_id
             AND shipment_line_status_code = 'FULLY RECEIVED';

      RETURN l_return_flag;
   EXCEPTION
      WHEN OTHERS
      THEN
         RETURN 'N';
   END;

   FUNCTION get_po_vendor_name (p_inventory_item_id    NUMBER,
                                p_organization_id      NUMBER)
      RETURN VARCHAR2
   IS
      l_vendor   VARCHAR2 (500);
   BEGIN
      SELECT MAX (pv.vendor_name)
        INTO l_vendor
        FROM po_vendors pv,
             po_lines_all pol,
             po_line_locations_all poll,
             po_headers_all poh
       WHERE     pol.item_id = p_inventory_item_id
             AND poll.ship_to_organization_id = p_organization_id
             AND pol.po_line_id = poll.po_line_id
             AND poh.po_header_id = pol.po_header_id
             AND poh.vendor_id = pv.vendor_id;

      RETURN l_vendor;
   EXCEPTION
      WHEN OTHERS
      THEN
         RETURN NULL;
   END;

   FUNCTION get_po_vendor_number (p_inventory_item_id    NUMBER,
                                  p_organization_id      NUMBER)
      RETURN VARCHAR2
   IS
      l_vendor   VARCHAR2 (500);
   BEGIN
      SELECT MAX (pv.segment1)
        INTO l_vendor
        FROM po_vendors pv,
             po_lines_all pol,
             po_line_locations_all poll,
             po_headers_all poh
       WHERE     pol.item_id = p_inventory_item_id
             AND poll.ship_to_organization_id = p_organization_id
             AND pol.po_line_id = poll.po_line_id
             AND poh.po_header_id = pol.po_header_id
             AND poh.vendor_id = pv.vendor_id;

      RETURN l_vendor;
   EXCEPTION
      WHEN OTHERS
      THEN
         RETURN NULL;
   END;

   FUNCTION get_lot_status (p_inventory_item_id    NUMBER,
                            p_organization_id      NUMBER,
                            p_line_id              NUMBER)
      RETURN VARCHAR2
   IS
      l_reason   VARCHAR2 (50);
   BEGIN
      SELECT 'Lot Control'
        INTO l_reason
        FROM mtl_system_items_b
       WHERE     inventory_item_id = p_inventory_item_id
             AND organization_id = p_organization_id
             AND lot_control_code = 2
             AND NOT EXISTS
                        (SELECT 1
                           FROM mtl_reservations
                          WHERE     demand_source_line_id = p_line_id
                                AND organization_id = p_organization_id
                                AND lot_number IS NOT NULL);

      RETURN l_reason;
   EXCEPTION
      WHEN OTHERS
      THEN
         RETURN NULL;
   END;

   FUNCTION get_isr_sourcing_rule (p_inventory_item_id    NUMBER,
                                   p_organization_id      NUMBER)
      RETURN VARCHAR2
   IS
      l_sr_name   VARCHAR2 (500);
   BEGIN
      SELECT MAX (msr.sourcing_rule_name)
        INTO l_sr_name
        FROM mrp_sr_assignments ass,
             mrp_sr_receipt_org rco,
             mrp_sr_source_org sso,
             mrp_sourcing_rules msr,
             po_vendors pov
       WHERE     1 = 1
             AND ass.inventory_item_id(+) = p_inventory_item_id
             AND ass.organization_id(+) = p_organization_id
             AND rco.sourcing_rule_id(+) = ass.sourcing_rule_id
             AND sso.sr_receipt_id(+) = rco.sr_receipt_id
             AND msr.sourcing_rule_id = ass.sourcing_rule_id
             AND pov.vendor_id(+) = sso.vendor_id;

      RETURN l_sr_name;
   EXCEPTION
      WHEN OTHERS
      THEN
         RETURN NULL;
   END;

   FUNCTION get_isr_ss (p_inventory_item_id NUMBER, p_organization_id NUMBER)
      RETURN NUMBER
   IS
      l_ss   NUMBER;
   BEGIN
      SELECT mst.safety_stock_quantity
        INTO l_ss
        FROM mtl_safety_stocks_view mst,
             org_acct_periods oap,
             org_organization_definitions ood
       WHERE     ood.organization_id = oap.organization_id
             AND inventory_item_id = p_inventory_item_id
             AND mst.organization_id = p_organization_id
             AND mst.organization_id = oap.organization_id
             AND TRUNC (mst.effectivity_date) BETWEEN period_start_date
                                                  AND NVL (period_close_date,
                                                           SYSDATE)
             AND TRUNC (SYSDATE) BETWEEN period_start_date
                                     AND NVL (period_close_date, SYSDATE);

      RETURN l_ss;
   EXCEPTION
      WHEN OTHERS
      THEN
         RETURN NULL;
   END;

   FUNCTION get_charger_amt (p_header_id NUMBER)
      RETURN NUMBER
   IS
      l_charge_amt   NUMBER;
   BEGIN
      IF g_pre_header_id = p_header_id
      THEN
         RETURN 0;
      ELSE
         g_pre_header_id := p_header_id;

         BEGIN
            SELECT NVL (oclv.charge_amount, 0)
              INTO l_charge_amt
              FROM oe_charge_lines_v oclv
             WHERE oclv.header_id = p_header_id;

            RETURN l_charge_amt;
         EXCEPTION
            WHEN NO_DATA_FOUND
            THEN
               l_charge_amt := 0;
               RETURN l_charge_amt;
         END;
      END IF;

      RETURN l_charge_amt;
   END;

   FUNCTION get_cash_drawer_amt (p_header_id NUMBER)
      RETURN NUMBER
   IS
      l_order_amount      NUMBER;
      g_insert_recd_cnt   NUMBER := 0;
      l_count             NUMBER;
   BEGIN
      IF g_prev_header_id = p_header_id
      THEN
         RETURN 0;
      ELSE
         g_prev_header_id := p_header_id;

         SELECT ROUND (
                   SUM (
                        (  (ol.ordered_quantity * ol.unit_selling_price)
                         + ol.tax_value)
                      * DECODE (ol.line_category_code, 'RETURN', -1, 1)),
                   2)
                   order_amount
           INTO l_order_amount
           FROM oe_order_lines ol
          WHERE ol.header_id = p_header_id;

         RETURN l_order_amount;
      END IF;
   /*select COUNT(*)
   INTO L_COUNT from
   Xxwc_Om_Cash_Refund_Tbl
   WHERE HEADER_ID=P_HEADER_ID;

     select SUM((OL.ORDERED_QUANTITY*OL.UNIT_SELLING_PRICE)+ OL.TAX_VALUE) ORDER_AMOUNT
       INTO L_ORDER_AMOUNT
       from oe_order_lines ol
       where OL.HEADER_ID=P_HEADER_ID;


   If G_Insert_Recd_Cnt=0  Then
   G_Insert_Recd_Cnt     :=G_Insert_Recd_Cnt+1;
   G_TB_DATA_TAB(G_INSERT_RECD_CNT):=P_HEADER_ID;
    Return L_Order_Amount;
    End If;

   if L_COUNT =1 then
       return L_ORDER_AMOUNT;
   else
    for I in G_TB_DATA_TAB.first .. G_TB_DATA_TAB.last LOOP
    if G_TB_DATA_TAB(I)= P_HEADER_ID then
     return 0;
     end if;
   end LOOP;
   end if;
   G_INSERT_RECD_CNT     :=G_INSERT_RECD_CNT+1;
   G_TB_DATA_TAB(G_INSERT_RECD_CNT):=P_HEADER_ID;
   return L_ORDER_AMOUNT;
   */
   END;



   FUNCTION get_customer_total (p_cust_account_id NUMBER)
      RETURN NUMBER
   IS
      l_cust_total   NUMBER;
   BEGIN
      /*If g_prev_cust_id= P_Cust_Account_Id Then
      Return L_Cust_Total;
      Else
      g_prev_cust_id := P_Cust_Account_Id;*/

      SELECT --Sum(Xxeis.Eis_Rs_Ar_Fin_Com_Util_Pkg.Amount_Remaining ( Ps.Payment_Schedule_Id, Ps.Due_Date, Null, Null, (Ps.Amount_Due_Original)  *Nvl(Ps.Exchange_Rate,1), Ps.Amount_Applied, Ps.Amount_Credited, Ps.Amount_Adjusted, Ps.Class ))
             SUM (ps.amount_due_remaining)
        INTO l_cust_total
        FROM ra_cust_trx_types ctt,
             ra_customer_trx trx,
             hz_cust_accounts cust,
             hz_parties party,
             ar_payment_schedules ps,
             ra_cust_trx_line_gl_dist gld
       WHERE     TRUNC (ps.gl_date) <=
                    xxeis.eis_rs_ar_fin_com_util_pkg.get_asof_date
             AND trx.cust_trx_type_id = ctt.cust_trx_type_id
             AND trx.bill_to_customer_id = cust.cust_account_id
             AND cust.party_id = party.party_id
             AND ps.customer_trx_id = trx.customer_trx_id
             AND trx.customer_trx_id = gld.customer_trx_id
             AND gld.account_class = 'REC'
             AND gld.latest_rec_flag = 'Y'
             AND ctt.org_id = trx.org_id
             AND cust.cust_account_id = p_cust_account_id;



      RETURN NVL (l_cust_total, 0);
   -- end if;

   END;


   FUNCTION get_customer_amt_due (p_cust_account_id NUMBER)
      RETURN NUMBER
   IS
      l_cust_total   NUMBER;
   BEGIN
      /*If g_prev_cust_id= P_Cust_Account_Id Then
      Return L_Cust_Total;
      Else
      g_prev_cust_id := P_Cust_Account_Id;*/

      SELECT SUM (xxeis.eis_rs_ar_fin_com_util_pkg.amount_remaining (
                     ps.payment_schedule_id,
                     ps.due_date,
                     NULL,
                     NULL,
                     (ps.amount_due_original) * NVL (ps.exchange_rate, 1),
                     ps.amount_applied,
                     ps.amount_credited,
                     ps.amount_adjusted,
                     ps.class))
        INTO l_cust_total
        FROM ra_cust_trx_types ctt,
             ra_customer_trx trx,
             hz_cust_accounts cust,
             hz_parties party,
             ar_payment_schedules ps,
             ra_cust_trx_line_gl_dist gld
       WHERE     TRUNC (ps.gl_date) <=
                    xxeis.eis_rs_ar_fin_com_util_pkg.get_asof_date
             AND trx.bill_to_customer_id = cust.cust_account_id
             AND cust.party_id = party.party_id
             AND ps.customer_trx_id = trx.customer_trx_id
             AND trx.customer_trx_id = gld.customer_trx_id
             AND gld.account_class = 'REC'
             AND gld.latest_rec_flag = 'Y'
             AND cust.cust_account_id = p_cust_account_id;



      RETURN l_cust_total;
   -- end if;

   END;


   FUNCTION get_customer_receipt_total (p_cust_account_id NUMBER)
      RETURN NUMBER
   IS
      l_cust_total   NUMBER;
   BEGIN
      IF p_cust_account_id IS NOT NULL
      THEN
         SELECT SUM (ps.amount_due_remaining)
           INTO l_cust_total
           FROM ar_payment_schedules ps,
                ar_cash_receipts cr,
                hz_cust_accounts cust,
                hz_parties party
          WHERE     TRUNC (ps.gl_date) <=
                       xxeis.eis_rs_ar_fin_com_util_pkg.get_asof_date
                AND ps.cash_receipt_id = cr.cash_receipt_id
                AND ps.customer_id = cust.cust_account_id(+)
                AND cust.party_id = party.party_id(+)
                AND cust.cust_account_id = p_cust_account_id
                AND NOT EXISTS
                           (SELECT 1
                              FROM ar_cash_receipt_history crh1
                             WHERE     crh1.cash_receipt_id =
                                          cr.cash_receipt_id
                                   AND TRUNC (crh1.gl_date) <=
                                          xxeis.eis_rs_ar_fin_com_util_pkg.get_asof_date
                                   AND status = 'REVERSED');

         RETURN NVL (l_cust_total, 0);
      ELSE
         RETURN 0;
      END IF;
   END;

   FUNCTION get_rental_bill_days (p_shp_date      DATE,
                                  p_start_date    DATE,
                                  p_end_date      DATE)
      RETURN NUMBER
   IS
   BEGIN
      IF ( (TRUNC (p_end_date) - TRUNC (p_shp_date)) + 1) >
            ( (TRUNC (p_end_date) - TRUNC (p_start_date)) + 1)
      THEN
         RETURN ROUND ( (TRUNC (p_end_date) - TRUNC (p_start_date)) + 1);
      ELSE
         RETURN ROUND ( (TRUNC (p_end_date) - TRUNC (p_shp_date)) + 1, 0);
      END IF;
   END;

   FUNCTION get_accrued_amt (p_days          NUMBER,
                             p_dff_price     NUMBER,
                             p_list_price    NUMBER)
      RETURN NUMBER
   IS
      l_amt   NUMBER := 0;
   BEGIN
      l_amt := FLOOR (p_days / 7) * 4 * NVL (p_dff_price, p_list_price); ----get weeks

      IF MOD (p_days, 7) > 3
      THEN                                  -----remaining days  if grt>4 then
         l_amt := l_amt + 4 * NVL (p_dff_price, p_list_price);
      ELSIF MOD (p_days, 7) < 4
      THEN                                  ----remaining days  if less<3 then
         l_amt := l_amt + MOD (p_days, 7) * NVL (p_dff_price, p_list_price);
      END IF;


      RETURN l_amt;
   END;

   FUNCTION get_rental_long_bill_days (p_shp_date DATE, p_end_date DATE)
      RETURN NUMBER
   IS
      l_days         NUMBER;
      l_minus_days   NUMBER;
   BEGIN
      SELECT CASE
                WHEN MOD ( (TRUNC (p_end_date) - TRUNC (p_shp_date)) + 1, 28) <>
                        0
                THEN
                     28
                   * (FLOOR (
                           ( (TRUNC (p_end_date) - TRUNC (p_shp_date)) + 1)
                         / 28))
                ELSE
                     28
                   * (  FLOOR (
                             ( (TRUNC (p_end_date) - TRUNC (p_shp_date)) + 1)
                           / 28)
                      - 1)
             END
        INTO l_minus_days
        FROM DUAL;


      RETURN ROUND (
                  ( (TRUNC (p_end_date) - TRUNC (p_shp_date)) + 1)
                - l_minus_days);
   /*   select
    case
     WHEN p_end_date -p_shp_date BETWEEN 29 AND 56
     THEN ((p_end_date-p_shp_date)-28)
     when (p_end_date -p_shp_date) between 57 and 84
     THEN ((p_end_date-p_shp_date)-56)
     when (p_end_date -p_shp_date) between 85 and 112
     then ((p_end_date-p_shp_date)-84)
     when (p_end_date -p_shp_date) between 113 and 140
     then ((p_end_date-p_shp_date)-112)
     when (p_end_date -p_shp_date) between 141 and 168
     then ((p_end_date-p_shp_date)-140)
     when (p_end_date -p_shp_date) between 169 and 196
     then ((p_end_date-p_shp_date)-168)
     when (p_end_date -p_shp_date) between 197 and 224
     then ((p_end_date-p_shp_date)-196)
     when (p_end_date -p_shp_date) between 225 and 252
     then ((p_end_date-p_shp_date)-224)
     when (p_end_date -p_shp_date) between 253 and 280
     then ((p_end_date-p_shp_date)-252)
     when (p_end_date -p_shp_date) between 281 and 308
     THEN ((p_end_date-p_shp_date)-280) END
     into l_days
     FROM    dual;
     return l_days;*/
   END;

   /*FUNCTION Get_Curr_list_Price (P_SEGMENT VARCHAR2)
   Return Number
   is
    l_list_price Number;
   BEGIN

           SELECT operand
              into l_list_price
             FROM qp_list_lines_v
               WHERE list_line_id IN
                     (SELECT MAX(qllv.list_line_id)
                       FROM qp_list_lines_v qllv,
                            qp_pricing_attributes qpa
                       WHERE 1                           =1
                        and qllv.list_line_id             = qpa.list_line_id
                        and qpa.product_attribute_context = 'ITEM'
                        and qpa.product_attribute         = 'PRICING_ATTRIBUTE1'
                        and qllv.product_attr_val_disp =p_segment
                        and trunc(qllv.start_date_active) >=nvl(g_date_from,trunc(qllv.start_date_active))
                        and trunc(qllv.end_date_active) <=nvl(g_date_to,trunc(qllv.end_date_active))
                      );

     return l_list_price;
    exception
    WHEN OTHERS THEN
      return null;
   END;       */


   --FUNCTION GET_CURR_LIST_PRICE  (P_SEGMENT1 VARCHAR2) RETURN NUMBER
   FUNCTION get_curr_list_price (p_item_id NUMBER)
      RETURN NUMBER
   IS
      l_list_price   NUMBER;
      l_cnt          NUMBER;
   BEGIN
      /*  select count(*)
        into l_cnt
        from qp_list_lines_v qllv,
              qp_pricing_attributes qpa,
              qp_list_headers       qslv
        where 1                           =1
        and qslv.list_header_id            =qllv.list_header_id
        and upper(qslv.name)             like 'CATEGORY%'
        and qllv.list_line_id             = qpa.list_line_id
        and qpa.product_attribute_context = 'ITEM'
        and qpa.product_attribute         = 'PRICING_ATTRIBUTE1'
        and qllv.product_attr_val_disp =p_segment1;*/

      -- if l_cnt =1 then
      /*  SELECT operand
           into l_list_price
          FROM qp_list_lines_v
            WHERE list_line_id IN
                  (SELECT MAX(qllv.list_line_id)
                    from qp_list_lines_v qllv,
                       --  qp_pricing_attributes qpa,
                         qp_list_headers       qslv
                    where 1                           =1
                    and qslv.list_header_id            =qllv.list_header_id
                     and upper(qslv.name)             like 'CATEGORY%'
                     --and qllv.list_line_id             = qpa.list_line_id
                     and qllv.product_attribute_context = 'ITEM'
                    -- and qpa.product_attribute         = 'PRICING_ATTRIBUTE1'
                     and qllv.product_attr_val_disp =p_segment1
                   --  and  trunc(sysdate)>= nvl(trunc(qllv.START_DATE_ACTIVE),trunc(sysdate))
                  --    and  trunc(sysdate)<= nvl(trunc(qllv.END_DATE_ACTIVE),trunc(sysdate))
                   );*/
      ---Added by Srinivas --

      SELECT operand
        INTO l_list_price
        FROM qp_list_lines_v
       WHERE list_line_id IN (SELECT MAX (qllv.list_line_id)
                                FROM qp_list_lines_v qllv,
                                     qp_list_headers qslv
                               WHERE     1 = 1
                                     AND qslv.list_header_id =
                                            qllv.list_header_id
                                     AND qslv.list_type_code = 'PRL' --and upper(qslv.name)             like 'CATEGORY%'
                                     AND qllv.product_attribute_context =
                                            'ITEM'
                                     AND qllv.product_id = p_item_id);

      RETURN l_list_price;
     /* else

              SELECT operand
           into l_list_price
          FROM qp_list_lines_v
            WHERE list_line_id IN
                  (SELECT MAX(qllv.list_line_id)
                    from qp_list_lines_v qllv,
                         qp_pricing_attributes qpa,
                         qp_list_headers       qslv
                    where 1                           =1
                    and qslv.list_header_id            =qllv.list_header_id
                     and upper(qslv.name)             like 'CATEGORY%'
                     and qllv.list_line_id             = qpa.list_line_id
                     and qpa.product_attribute_context = 'ITEM'
                     and qpa.product_attribute         = 'PRICING_ATTRIBUTE1'
                     and qllv.product_attr_val_disp =p_segment1
                  --   and  nvl(trunc(qllv.start_date_active),trunc(g_date_from))>= nvl(trunc(qllv.start_date_active),trunc(sysdate))
                   --   and  nvl(trunc(qllv.end_date_active)trunc(G_DATE_TO)<= nvl(trunc(qllv.end_date_active),trunc(sysdate))
                   );

      end if;

*/

   EXCEPTION
      WHEN OTHERS
      THEN
         RETURN NULL;
   END;

   FUNCTION get_curr_list_price (p_item_id              NUMBER,
                                 p_pricing_attribute    VARCHAR2)
      RETURN NUMBER
   IS
      l_list_price   NUMBER;
      l_cnt          NUMBER;
   BEGIN
      SELECT operand
        INTO l_list_price
        FROM qp_list_lines_v
       WHERE list_line_id IN (SELECT MAX (qpa.list_line_id)
                                FROM qp_pricing_attributes qpa,
                                     qp_list_lines qll,
                                     qp_list_headers qslv
                               WHERE     1 = 1
                                     AND qslv.list_header_id =
                                            qll.list_header_id
                                     AND qslv.list_type_code = 'PRL'
                                     AND qll.list_line_id = qpa.list_line_id --and upper(qslv.name)             like 'CATEGORY%'
                                     AND qpa.product_attribute_context =
                                            'ITEM'
                                     AND qpa.product_attribute =
                                            p_pricing_attribute
                                     AND qpa.product_attr_value = p_item_id);

      RETURN l_list_price;
   EXCEPTION
      WHEN OTHERS
      THEN
         RETURN NULL;
   END;


   PROCEDURE get_curr_sold_sales (p_item_id        IN     NUMBER,
                                  p_cat_id         IN     NUMBER,
                                  p_cat            IN     VARCHAR2,
                                  p_cust_acct_id   IN     NUMBER,
                                  p_start_date     IN     DATE,
                                  p_end_date       IN     DATE,
                                  p_units_sold        OUT NUMBER,
                                  p_actual_sales      OUT NUMBER,
                                  p_unit_cost         OUT NUMBER,
                                  p_process_id     IN     NUMBER)
   IS
      l_start_date   VARCHAR2 (32000);
      l_end_date     VARCHAR2 (32000);
   BEGIN
      /*IF p_start_date IS NULL THEN
         L_start_date := '1=1';
      ELSE
        L_START_DATE:=  'AND (DECODE(OTT.NAME,''STANDARD ORDER'' ,TRUNC(OL.ACTUAL_SHIPMENT_DATE),
                                             ,NVL(TRUNC(OL.ACTUAL_SHIPMENT_DATE),TRUNC(OL.SCHEDULE_SHIP_DATE))))>='''||P_START_DATE||'''';
      END IF;

      IF p_end_date IS NULL THEN
         l_end_date := '1=1';
      ELSE
         L_END_DATE :='AND (DECODE(OTT.NAME,''STANDARD ORDER'', TRUNC(OL.ACTUAL_SHIPMENT_DATE),
                                             ,NVL(TRUNC(OL.ACTUAL_SHIPMENT_DATE),TRUNC(OL.SCHEDULE_SHIP_DATE))))<='''||P_END_DATE||'''';
     END IF;*/


      IF p_item_id != 0
      THEN
         IF p_start_date IS NULL AND p_end_date IS NULL
         THEN
            --fnd_file.put_line(fnd_file.log,' Start and End Date  and item is zero' || p_start_date || ' ' || p_end_date );
            BEGIN
               SELECT exusd.unit_sold, exusd.actual_sales, exusd.unit_cost
                 INTO p_units_sold, p_actual_sales, p_unit_cost
                 FROM xxeis.eis_xxwc_unit_sales_data exusd
                WHERE     exusd.inventory_item_id = p_item_id
                      AND exusd.cust_account_id = p_cust_acct_id;
            EXCEPTION
               WHEN OTHERS
               THEN
                  --fnd_file.put_line(fnd_file.log,' In Exception  item iz  zero :' || substr(sqlerrm,1,250) );
                  p_units_sold := 0;
                  p_actual_sales := 0;
                  p_unit_cost := 0;
            END;
         ELSE
            BEGIN
               --fnd_file.put_line(fnd_file.log,' Start and End Date  and item is not zero' || p_start_date || ' ' || p_end_date );
               --fnd_file.put_line(fnd_file.log,' Customer Id ' || p_cust_acct_id || '  Item Id ' || p_item_id );
               SELECT exusd.unit_sold, exusd.actual_sales, exusd.unit_cost
                 INTO p_units_sold, p_actual_sales, p_unit_cost
                 FROM xxeis.eis_xxwc_unit_sales_data_gt exusd
                WHERE     exusd.inventory_item_id = p_item_id
                      AND exusd.cust_account_id = p_cust_acct_id
                      AND exusd.process_id = p_process_id;
            EXCEPTION
               WHEN OTHERS
               THEN
                  --fnd_file.put_line(fnd_file.log,' In Exception  item iz not zero :' || substr(sqlerrm,1,250) );
                  p_units_sold := 0;
                  p_actual_sales := 0;
                  p_unit_cost := 0;
            END;
         END IF;
      /*
      select NVL(SUM(DECODE(LINE_CATEGORY_CODE, 'RETURN',NVL(OL.SHIPPED_QUANTITY,NVL(OL.FULFILLED_QUANTITY,OL.ORDERED_QUANTITY))*-1,NVL(OL.SHIPPED_QUANTITY,NVL(OL.FULFILLED_QUANTITY,OL.ORDERED_QUANTITY)))),0) ,
             NVL(SUM((NVL(DECODE (LINE_CATEGORY_CODE, 'RETURN',NVL(OL.SHIPPED_QUANTITY,NVL(OL.FULFILLED_QUANTITY,OL.ORDERED_QUANTITY))*-1,NVL(OL.SHIPPED_QUANTITY,NVL(OL.FULFILLED_QUANTITY,OL.ORDERED_QUANTITY))),0)*OL.UNIT_SELLING_PRICE)),0)
        INTO p_units_sold,p_actual_sales
        from OE_ORDER_LINES              OL,
             OE_ORDER_HEADERS            OH,
             OE_TRANSACTION_TYPES_TL     OTT,
             --QP_MODIFIER_SUMMARY_V       QLL,
             MTL_CATEGORIES_KFV          MCK,
             MTL_CATEGORY_SETS           MCS,
             MTL_ITEM_CATEGORIES         MIC,
             MTL_SYSTEM_ITEMS_B_KFV      MSI
       where OL.HEADER_ID                    = OH.HEADER_ID
         AND OTT.transaction_TYPE_ID         = OH.ORDER_TYPE_ID
         and OL.FLOW_STATUS_CODE             = 'CLOSED'
         and MCS.STRUCTURE_ID                = MCK.STRUCTURE_ID
         and MIC.CATEGORY_SET_ID             = MCS.CATEGORY_SET_ID
         and MIC.CATEGORY_ID                 = MCK.CATEGORY_ID
         and MIC.INVENTORY_ITEM_ID           = MSI.INVENTORY_ITEM_ID
         and MIC.ORGANIZATION_ID             = MSI.ORGANIZATION_ID
         and MSI.INVENTORY_ITEM_ID           = OL.INVENTORY_ITEM_ID
         AND OL.SOLD_TO_ORG_ID               = NVL(p_cust_acct_id,OL.SOLD_TO_ORG_ID)
         and MSI.ORGANIZATION_ID             = OL.SHIP_FROM_ORG_ID
         AND MCK.CATEGORY_ID                 =  p_cat_id
         || L_START_DATE ||' '|| L_END_DATE
         ;
        */
      ELSIF p_cat_id = 0
      THEN
         SELECT NVL (
                   SUM (
                      DECODE (
                         line_category_code,
                         'RETURN',   NVL (
                                        ol.shipped_quantity,
                                        NVL (ol.fulfilled_quantity,
                                             ol.ordered_quantity))
                                   * -1,
                         NVL (
                            ol.shipped_quantity,
                            NVL (ol.fulfilled_quantity, ol.ordered_quantity)))),
                   0),
                NVL (
                   SUM (
                      (  NVL (
                            DECODE (
                               line_category_code,
                               'RETURN',   NVL (
                                              ol.shipped_quantity,
                                              NVL (ol.fulfilled_quantity,
                                                   ol.ordered_quantity))
                                         * -1,
                               NVL (
                                  ol.shipped_quantity,
                                  NVL (ol.fulfilled_quantity,
                                       ol.ordered_quantity))),
                            0)
                       * ol.unit_selling_price)),
                   0),
                AVG (0)
           INTO p_units_sold, p_actual_sales, p_unit_cost
           FROM oe_order_lines ol,
                oe_order_headers oh,
                oe_transaction_types_tl ott, --QP_MODIFIER_SUMMARY_V       QLL,
                mtl_categories_kfv mck,
                mtl_category_sets mcs,
                mtl_item_categories mic,
                mtl_system_items_b_kfv msi
          WHERE     ol.header_id = oh.header_id
                AND ott.transaction_type_id = oh.order_type_id
                AND ol.flow_status_code = 'CLOSED'
                AND mcs.structure_id = mck.structure_id
                AND mic.category_set_id = mcs.category_set_id
                AND mic.category_id = mck.category_id
                AND mic.inventory_item_id = msi.inventory_item_id
                AND mic.organization_id = msi.organization_id
                AND msi.inventory_item_id = ol.inventory_item_id
                AND ol.sold_to_org_id = p_cust_acct_id
                AND msi.organization_id = ol.ship_from_org_id
                AND mck.segment1 = p_cat
                AND (   p_start_date IS NULL
                     OR (DECODE (
                            ott.name,
                            'STANDARD ORDER', TRUNC (ol.actual_shipment_date),
                            NVL (TRUNC (ol.actual_shipment_date),
                                 TRUNC (ol.schedule_ship_date))) >=
                            p_start_date))
                AND (   p_end_date IS NULL
                     OR (DECODE (
                            ott.name,
                            'STANDARD ORDER', TRUNC (ol.actual_shipment_date),
                            NVL (TRUNC (ol.actual_shipment_date),
                                 TRUNC (ol.schedule_ship_date))) <=
                            p_end_date));
      ELSE
         SELECT NVL (
                   SUM (
                      DECODE (
                         line_category_code,
                         'RETURN',   NVL (
                                        ol.shipped_quantity,
                                        NVL (ol.fulfilled_quantity,
                                             ol.ordered_quantity))
                                   * -1,
                         NVL (
                            ol.shipped_quantity,
                            NVL (ol.fulfilled_quantity, ol.ordered_quantity)))),
                   0),
                NVL (
                   SUM (
                      (  NVL (
                            DECODE (
                               line_category_code,
                               'RETURN',   NVL (
                                              ol.shipped_quantity,
                                              NVL (ol.fulfilled_quantity,
                                                   ol.ordered_quantity))
                                         * -1,
                               NVL (
                                  ol.shipped_quantity,
                                  NVL (ol.fulfilled_quantity,
                                       ol.ordered_quantity))),
                            0)
                       * ol.unit_selling_price)),
                   0),
                AVG (0)
           INTO p_units_sold, p_actual_sales, p_unit_cost
           FROM oe_order_lines ol,
                oe_order_headers oh,
                oe_transaction_types_tl ott, --QP_MODIFIER_SUMMARY_V       QLL,
                mtl_categories_kfv mck,
                mtl_category_sets mcs,
                mtl_item_categories mic,
                mtl_system_items_b_kfv msi
          WHERE     ol.header_id = oh.header_id
                AND ott.transaction_type_id = oh.order_type_id
                AND ol.flow_status_code = 'CLOSED'
                AND mcs.structure_id = mck.structure_id
                AND mic.category_set_id = mcs.category_set_id
                AND mic.category_id = mck.category_id
                AND mic.inventory_item_id = msi.inventory_item_id
                AND mic.organization_id = msi.organization_id
                AND msi.inventory_item_id = ol.inventory_item_id
                AND ol.sold_to_org_id =
                       NVL (p_cust_acct_id, ol.sold_to_org_id)
                AND msi.organization_id = ol.ship_from_org_id
                AND mck.category_id = p_cat_id
                AND (   p_start_date IS NULL
                     OR (DECODE (
                            ott.name,
                            'STANDARD ORDER', TRUNC (ol.actual_shipment_date),
                            NVL (TRUNC (ol.actual_shipment_date),
                                 TRUNC (ol.schedule_ship_date))) >=
                            p_start_date))
                AND (   p_end_date IS NULL
                     OR (DECODE (
                            ott.name,
                            'STANDARD ORDER', TRUNC (ol.actual_shipment_date),
                            NVL (TRUNC (ol.actual_shipment_date),
                                 TRUNC (ol.schedule_ship_date))) <=
                            p_end_date));
      END IF;
   EXCEPTION
      WHEN OTHERS
      THEN
         p_units_sold := 0;
         p_actual_sales := 0;
         p_unit_cost := 0;
   END get_curr_sold_sales;

   PROCEDURE get_curr_sold_sales (p_item_id          IN     NUMBER,
                                  p_cat_id           IN     NUMBER,
                                  p_cat              IN     VARCHAR2,
                                  p_cust_acct_id     IN     NUMBER,
                                  p_list_header_id   IN     NUMBER,
                                  p_units_sold          OUT NUMBER,
                                  p_actual_sales        OUT NUMBER,
                                  p_unit_cost           OUT NUMBER,
                                  p_process_id       IN     NUMBER)
   IS
      l_start_date   VARCHAR2 (32000);
      l_end_date     VARCHAR2 (32000);
   BEGIN
      IF p_item_id != 0
      THEN
         BEGIN
            --fnd_file.put_line(fnd_file.log,' Start and End Date  and item is not zero' || p_start_date || ' ' || p_end_date );
            --fnd_file.put_line(fnd_file.log,' Customer Id ' || p_cust_acct_id || '  Item Id ' || p_item_id );
            SELECT exusd.unit_sold, exusd.actual_sales, exusd.unit_cost
              INTO p_units_sold, p_actual_sales, p_unit_cost
              FROM xxeis.eis_xxwc_unit_sales_data_gt exusd
             WHERE     exusd.inventory_item_id = p_item_id
                   AND exusd.cust_account_id = p_cust_acct_id
                   AND exusd.list_header_id = p_list_header_id
                   AND exusd.process_id = p_process_id;
         EXCEPTION
            WHEN OTHERS
            THEN
               -- fnd_file.put_line(fnd_file.log,' In Exception  item id not zero :' || substr(sqlerrm,1,250) );
               p_units_sold := 0;
               p_actual_sales := 0;
               p_unit_cost := 0;
         END;
      ELSIF p_cat_id = 0
      THEN
         BEGIN
              --fnd_file.put_line(fnd_file.log,' Start and End Date  and item is not zero' || p_start_date || ' ' || p_end_date );
              --fnd_file.put_line(fnd_file.log,' Customer Id ' || p_cust_acct_id || '  Item Id ' || p_item_id );
              SELECT SUM (exusd.unit_sold), SUM (exusd.actual_sales), AVG (0)
                INTO p_units_sold, p_actual_sales, p_unit_cost
                FROM xxeis.eis_xxwc_unit_sales_data_gt exusd
               WHERE     exusd.cat = p_cat
                     AND exusd.cust_account_id = p_cust_acct_id
                     AND exusd.list_header_id = p_list_header_id
                     AND exusd.process_id = p_process_id
            GROUP BY exusd.cust_account_id,
                     exusd.list_header_id,
                     exusd.cat,
                     exusd.process_id;
         EXCEPTION
            WHEN OTHERS
            THEN
               --fnd_file.put_line(fnd_file.log,' In Exception  Cat :' || substr(sqlerrm,1,250) );
               p_units_sold := 0;
               p_actual_sales := 0;
               p_unit_cost := 0;
         END;
      ELSE
         BEGIN
              --fnd_file.put_line(fnd_file.log,' Start and End Date  and item is not zero' || p_start_date || ' ' || p_end_date );
              --fnd_file.put_line(fnd_file.log,' Customer Id ' || p_cust_acct_id || '  Item Id ' || p_item_id );
              SELECT SUM (exusd.unit_sold), SUM (exusd.actual_sales), AVG (0)
                INTO p_units_sold, p_actual_sales, p_unit_cost
                FROM xxeis.eis_xxwc_unit_sales_data_gt exusd
               WHERE     exusd.category_id = p_cat_id
                     AND exusd.cust_account_id = p_cust_acct_id
                     AND exusd.list_header_id = p_list_header_id
                     AND exusd.process_id = p_process_id
            GROUP BY exusd.cust_account_id,
                     exusd.list_header_id,
                     exusd.category_id,
                     exusd.process_id;
         EXCEPTION
            WHEN OTHERS
            THEN
               --fnd_file.put_line(fnd_file.log,' In Exception category_id  :' || substr(sqlerrm,1,250) );
               p_units_sold := 0;
               p_actual_sales := 0;
               p_unit_cost := 0;
         END;
      END IF;
   EXCEPTION
      WHEN OTHERS
      THEN
         p_units_sold := 0;
         p_actual_sales := 0;
         p_unit_cost := 0;
   END get_curr_sold_sales;

   /*Function get_application_method (P_Inventory_Item_Id Number, P_Organization_Id Number)
   Return varchar2
   is
    l_application_method varchar2(100);
   BEGIN

      /* Select apps.qp_qp_form_pricing_attr.get_meaning(qll.arithmetic_operator, 'ARITHMETIC_OPERATOR')
       into l_application_method
       from  qp_secu_list_headers_vl qsl,
             qp_modifier_summary_v qll,
             mtl_system_items_kfv msi
       where 1                      =1
        and qsl.list_header_id            =qll.list_header_id
        and qll.product_attribute_context ='ITEM'
        and msi.segment1                  =to_char(qll.product_attr_value)
        and msi.inventory_item_id        = p_inventory_item_id
        and msi.organization_id          = p_organization_id
        and upper(qsl.name) not like '%BRANCH%';*/

   /*   select apps.qp_qp_form_pricing_attr.get_meaning(oea.arithmetic_operator, 'ARITHMETIC_OPERATOR')
        into l_application_method
       from oe_price_adjustments_v oea,
            oe_order_lines ol
         where  ol.header_id                      = oea.header_id(+)
           and  ol.line_id                        = oea.line_id(+)
           and upper(oea.adjustment_name) not like '%BRANCH%'
           and oea.list_line_type_code='DIS'
           --and oea.header_id=10764;
           and ol.inventory_item_id=p_inventory_item_id
           and ol.ship_from_org_id=p_organization_id;


   return l_application_method;
  exception
  WHEN OTHERS THEN
    return null;
 END;*/

   FUNCTION get_application_method (p_header_id NUMBER, p_line_id NUMBER)
      RETURN VARCHAR2
   IS
      l_application_method   VARCHAR2 (100);
   BEGIN
      /* Select apps.qp_qp_form_pricing_attr.get_meaning(qll.arithmetic_operator, 'ARITHMETIC_OPERATOR')
       into l_application_method
       from  qp_secu_list_headers_vl qsl,
             qp_modifier_summary_v qll,
             mtl_system_items_kfv msi
       where 1                      =1
        and qsl.list_header_id            =qll.list_header_id
        and qll.product_attribute_context ='ITEM'
        and msi.segment1                  =to_char(qll.product_attr_value)
        and msi.inventory_item_id        = p_inventory_item_id
        and msi.organization_id          = p_organization_id
        and upper(qsl.name) not like '%BRANCH%';*/

      SELECT apps.qp_qp_form_pricing_attr.get_meaning (
                oea.arithmetic_operator,
                'ARITHMETIC_OPERATOR')
        INTO l_application_method
        FROM oe_price_adjustments_v oea,
             qp_list_headers_vl qlh,
             qp_qualifiers qual
       --  hz_cust_site_uses hcsu
       WHERE     1 = 1    --    and oea.list_line_type_code            = 'DIS'
             AND oea.list_header_id = qlh.list_header_id
             AND qual.list_header_id = qlh.list_header_id
             AND qual.qualifier_context = 'CUSTOMER'
             AND qual.qualifier_attribute IN ('QUALIFIER_ATTRIBUTE11',
                                              'QUALIFIER_ATTRIBUTE14') --  and qual.orig_sys_qualifier_ref        = hcsu.site_use_id
     --   and hcsu.site_use_code = 'BILL_TO' or hcsu.site_use_code = 'SHIP_TO'
             AND UPPER (oea.adjustment_name) NOT LIKE '%BRANCH%'
             AND oea.header_id = p_header_id
             AND oea.line_id = p_line_id;



      RETURN l_application_method;
   EXCEPTION
      WHEN OTHERS
      THEN
         RETURN NULL;
   END;


   FUNCTION get_application_value (p_header_id NUMBER, p_line_id NUMBER)
      RETURN NUMBER
   IS
      l_application_val   NUMBER;
   BEGIN
      /* Select apps.qp_qp_form_pricing_attr.get_meaning(qll.arithmetic_operator, 'ARITHMETIC_OPERATOR')
       into l_application_method
       from  qp_secu_list_headers_vl qsl,
             qp_modifier_summary_v qll,
             mtl_system_items_kfv msi
       where 1                      =1
        and qsl.list_header_id            =qll.list_header_id
        and qll.product_attribute_context ='ITEM'
        and msi.segment1                  =to_char(qll.product_attr_value)
        and msi.inventory_item_id        = p_inventory_item_id
        and msi.organization_id          = p_organization_id
        and upper(qsl.name) not like '%BRANCH%';*/

      SELECT operand
        INTO l_application_val
        FROM oe_price_adjustments_v oea,
             qp_list_headers_vl qlh,
             qp_qualifiers qual
       WHERE     1 = 1
             AND oea.list_header_id = qlh.list_header_id
             AND qual.list_header_id = qlh.list_header_id
             AND qual.qualifier_context = 'CUSTOMER'
             AND qual.qualifier_attribute IN ('QUALIFIER_ATTRIBUTE11',
                                              'QUALIFIER_ATTRIBUTE14')
             AND UPPER (oea.adjustment_name) NOT LIKE '%BRANCH%'
             AND oea.header_id = p_header_id
             AND oea.line_id = p_line_id;


      RETURN l_application_val;
   EXCEPTION
      WHEN OTHERS
      THEN
         RETURN NULL;
   END;


   FUNCTION get_shipped_qty (p_header_id NUMBER, p_line_id NUMBER)
      RETURN NUMBER
   IS
      l_shipped_qty    NUMBER;
      l_returned_qty   NUMBER;
   BEGIN
      /*  select sum(NVL(ol.shipped_quantity,OL.FULFILLED_QUANTITY))
          into l_shipped_qty
         from oe_order_lines ol
          where  ol.flow_status_code='CLOSED'
           and  ol.header_id=p_header_id
           and  ol.line_id  =p_line_id
          /* and exists (select 1
                       from  oe_price_adjustments_v oea,
                            qp_list_headers_vl qlh,
                            qp_qualifiers qual
                         where  1=1
                           and oea.list_header_id                 = qlh.list_header_id
                           and qual.list_header_id                = qlh.list_header_id
                           and qual.qualifier_context             = 'CUSTOMER'
                           and qual.QUALIFIER_ATTRIBUTE in ('QUALIFIER_ATTRIBUTE11' ,'QUALIFIER_ATTRIBUTE14')
                           and upper(oea.adjustment_name) not like '%BRANCH%'
                           and oea.header_id                      = p_header_id
                           and oea.line_id                        = p_line_id
                         );*/


      /*  select sum(NVL(orlr.shipped_quantity,orlr.FULFILLED_QUANTITY))
        into l_returned_qty
        from oe_order_lines orlr
         where orlr.line_category_code= 'RETURN'
          and  orlr.reference_header_id=p_header_id
          and  orlr.reference_line_id=p_line_id
          and  orlr.flow_status_code='CLOSED';   */

      /*  begin
              select NVL(OL.SHIPPED_QUANTITY,NVL(OL.FULFILLED_QUANTITY,OL.ORDERED_QUANTITY))
              into l_shipped_qty
              from oe_order_lines ol
              where  ol.flow_status_code='CLOSED'
              and  OL.HEADER_ID=P_HEADER_ID
              and  ol.line_id  =p_line_id ;
         exception
            when others then
              l_shipped_qty := 0;
         end;
         begin
              select sum(NVL(orlr.shipped_quantity,orlr.FULFILLED_QUANTITY))
                into l_returned_qty
                from oe_order_lines orlr
               where orlr.line_category_code= 'RETURN'
                and  orlr.reference_header_id=p_header_id
                and  orlr.reference_line_id=p_line_id
                and  orlr.flow_status_code='CLOSED';
          exception
            when others then
               l_returned_qty := 0;
         end;      */

      SELECT DECODE (
                line_category_code,
                'RETURN',   NVL (
                               ol.shipped_quantity,
                               NVL (ol.fulfilled_quantity,
                                    ol.ordered_quantity))
                          * -1,
                NVL (ol.shipped_quantity,
                     NVL (ol.fulfilled_quantity, ol.ordered_quantity)))
        INTO l_shipped_qty
        FROM oe_order_lines ol
       WHERE     ol.flow_status_code = 'CLOSED'
             AND ol.header_id = p_header_id
             AND ol.line_id = p_line_id;


      RETURN (NVL (l_shipped_qty, 0));
   EXCEPTION
      WHEN OTHERS
      THEN
         RETURN NULL;
   END;


   FUNCTION get_returned_qty (p_header_id NUMBER, p_line_id NUMBER)
      RETURN NUMBER
   IS
      l_returned_qty   NUMBER;
   BEGIN
      SELECT SUM (orlr.shipped_quantity)
        INTO l_returned_qty
        FROM oe_order_lines orlr
       WHERE     orlr.line_category_code = 'RETURN'
             AND orlr.header_id = p_header_id
             AND orlr.line_id = p_line_id
             AND orlr.flow_status_code = 'CLOSED';

      RETURN l_returned_qty;
   EXCEPTION
      WHEN OTHERS
      THEN
         RETURN NULL;
   END;


   FUNCTION get_sales_dollars (p_header_id NUMBER, p_line_id NUMBER)
      RETURN NUMBER
   IS
      l_sales_dollars   NUMBER;
   BEGIN
      SELECT SUM (ol.unit_selling_price * ol.ordered_quantity)
        INTO l_sales_dollars
        FROM oe_order_lines ol, oe_price_adjustments_v oel
       WHERE     ol.header_id = oel.header_id(+)
             AND ol.line_id = oel.line_id(+) --and  oel.adjustment_name ='CSP'
             AND ol.flow_status_code = 'CLOSED'
             AND ol.header_id = p_header_id
             AND ol.line_id = p_line_id --and  ol.actual_shipment_date between EIS_RS_XXWC_COM_UTIL_PKG.GET_DATE_FROM AND EIS_RS_XXWC_COM_UTIL_PKG.GET_DATE_TO
                                       ;

      RETURN l_sales_dollars;
   EXCEPTION
      WHEN OTHERS
      THEN
         RETURN NULL;
   END;


   FUNCTION get_return_dollars (p_header_id NUMBER, p_line_id NUMBER)
      RETURN NUMBER
   IS
      l_return_dollars   NUMBER;
   BEGIN
        SELECT (SUM (orlr.unit_selling_price * orlr.ordered_quantity) * -1)
          INTO l_return_dollars
          FROM oe_order_lines orlr, oe_price_adjustments_v oel
         WHERE     orlr.line_category_code = 'RETURN'
               AND orlr.header_id = oel.header_id(+)
               AND orlr.line_id = oel.line_id(+) --and  oel.adjustment_name ='CSP'
               AND orlr.flow_status_code = 'CLOSED'
               AND orlr.header_id = p_header_id
               AND orlr.line_id = p_line_id
               AND orlr.reference_line_id IS NOT NULL
      --and  orlr.actual_shipment_date between EIS_RS_XXWC_COM_UTIL_PKG.GET_DATE_FROM AND EIS_RS_XXWC_COM_UTIL_PKG.GET_DATE_TO
      GROUP BY orlr.ordered_item, oel.adjustment_name;


      RETURN l_return_dollars;
   EXCEPTION
      WHEN OTHERS
      THEN
         RETURN NULL;
   END;

   /*function GET_AVERAGE_COST (p_header_id number, p_line_id number)
   RETURN NUMBER
   is
    l_avg_cost number;
   BEGIN

               select QRY.ACTUAL_COST
                     into l_avg_cost
                 FROM
                   (SELECT d.trx_number,
                     c.line_number,
                     c.sales_order,
                     b.ship_from_org_id,
                     a.inventory_item_id,
                     c.sales_order_line,
                     b.ordered_item,
                     b.ordered_quantity,
                     b.unit_selling_price,
                     B.UNIT_COST,
                     SUM(a.actual_cost) actual_cost,
                     a.transaction_costed_date
                   FROM MTL_CST_ACTUAL_COST_DETAILS a,
                     OE_ORDER_LINES b,
                     RA_CUSTOMER_TRX_LINES c,
                     RA_CUSTOMER_TRX D
                   WHERE A.INVENTORY_ITEM_ID    =B.INVENTORY_ITEM_ID
                   AND A.ORGANIZATION_ID        =B.SHIP_FROM_ORG_ID
                   AND B.HEADER_ID=P_HEADER_ID
                   and b.line_id  =p_line_id
                   --AND c.sales_order            = '10004691'
                   AND b.line_id                =c.interface_line_attribute6
                   AND c.interface_line_context ='ORDER ENTRY'
                   AND a.transaction_costed_date=
                     (SELECT MIN(transaction_costed_date)
                     FROM INV.MTL_CST_ACTUAL_COST_DETAILS d
                     WHERE a.inventory_item_id    =d.inventory_item_id
                     AND a.organization_id        =d.organization_id
                     AND d.transaction_costed_date>b.fulfillment_date
                     )
                   AND d.customer_trx_id=c.customer_trx_id
                   AND d.creation_date  > sysdate-2
                   group by d.trx_number, c.line_number, c.sales_order, b.ship_from_org_id, a.inventory_item_id, c.sales_order_line, b.ordered_item, b.ordered_quantity, b.unit_selling_price, B.UNIT_COST, a.transaction_costed_date
                   ) qry;

     return l_avg_cost;
    exception
    WHEN OTHERS THEN
      return null;
   END;*/

   FUNCTION get_average_cost (p_header_id NUMBER, p_line_id NUMBER)
      RETURN NUMBER
   IS
      l_avg_cost   NUMBER;
   BEGIN
        SELECT SUM (a.actual_cost) actual_cost
          INTO l_avg_cost
          FROM mtl_cst_actual_cost_details a,
               oe_order_lines b,
               ra_customer_trx_lines c,
               ra_customer_trx d
         WHERE     a.inventory_item_id = b.inventory_item_id
               AND a.organization_id = b.ship_from_org_id
               AND b.header_id = p_header_id
               AND b.line_id = p_line_id
               --AND c.sales_order            = '10004691'
               AND b.line_id = c.interface_line_attribute6
               AND c.interface_line_context = 'ORDER ENTRY'
               AND a.transaction_costed_date =
                      (SELECT MIN (transaction_costed_date)
                         FROM inv.mtl_cst_actual_cost_details d
                        WHERE     a.inventory_item_id = d.inventory_item_id
                              AND a.organization_id = d.organization_id
                              AND d.transaction_costed_date >
                                     b.fulfillment_date)
               AND d.customer_trx_id = c.customer_trx_id
               AND d.creation_date > SYSDATE - 2
      GROUP BY d.trx_number,
               c.line_number,
               c.sales_order,
               b.ship_from_org_id,
               a.inventory_item_id,
               c.sales_order_line,
               b.ordered_item,
               b.ordered_quantity,
               b.unit_selling_price,
               b.unit_cost,
               a.transaction_costed_date;

      RETURN l_avg_cost;
   EXCEPTION
      WHEN OTHERS
      THEN
         RETURN NULL;
   END;

   FUNCTION get_average_cost (p_inventory_item_id    NUMBER,
                              p_organization_id      NUMBER,
                              p_shipment_date        DATE,
                              p_line_id              NUMBER)
      RETURN NUMBER
   IS
      l_avg_cost   NUMBER;
   BEGIN
      SELECT MIN (actual_cost)
        INTO l_avg_cost
        FROM (  SELECT SUM (a.actual_cost) actual_cost
                  FROM mtl_cst_actual_cost_details a
                 WHERE     a.inventory_item_id = p_inventory_item_id
                       AND a.organization_id = p_organization_id
                       --and e.trx_source_line_id   = p_line_id
                       --  and e.transaction_id=a.transaction_id
                       AND a.transaction_id =
                              (SELECT MIN (transaction_id)
                                 FROM mtl_cst_actual_cost_details d
                                WHERE     a.inventory_item_id =
                                             d.inventory_item_id
                                      AND a.organization_id = d.organization_id
                                      AND d.transaction_costed_date >
                                             p_shipment_date)
                       AND a.transaction_costed_date =
                              (SELECT MIN (transaction_costed_date)
                                 FROM mtl_cst_actual_cost_details d
                                WHERE     a.inventory_item_id =
                                             d.inventory_item_id
                                      AND a.organization_id = d.organization_id
                                      AND d.transaction_costed_date >
                                             p_shipment_date)
              GROUP BY a.inventory_item_id,
                       a.organization_id,
                       a.transaction_id,
                       a.transaction_costed_date) qry;


      RETURN NVL (l_avg_cost, 0);
   EXCEPTION
      WHEN OTHERS
      THEN
         RETURN NULL;
   END;

   ----Credit Meemo---

   FUNCTION xxwc_cal_restock_per (l_header_id IN NUMBER)
      RETURN NUMBER
   IS
      l_restock_amount       NUMBER;
      l_non_restock_amount   NUMBER;
      l_restock_per          NUMBER;
   BEGIN
      l_restock_amount := 0;
      l_non_restock_amount := 0;
      l_restock_per := 0;

      SELECT SUM (ordered_quantity * unit_selling_price)
        INTO l_restock_amount
        FROM oe_order_lines
       WHERE header_id = l_header_id AND ordered_item = 'RESTOCKING';

      IF l_restock_amount <> 0
      THEN
         SELECT NVL (SUM (ordered_quantity * unit_selling_price), 1)
           INTO l_non_restock_amount
           FROM oe_order_lines
          WHERE header_id = l_header_id;

         -- AND ORDERED_ITEM !='RESTOCKING';

         -- L_RESTOCK_PER :=ROUND((L_RESTOCK_AMOUNT/NVL(L_NON_RESTOCK_AMOUNT,1))*100,2)/L_RESTOCK_AMOUNT;
         l_restock_per :=
            ROUND ( (l_restock_amount / NVL (l_non_restock_amount, 1)) * 100,
                   2);

         RETURN l_restock_per;
      ELSE
         RETURN l_restock_per;
      END IF;
   EXCEPTION
      WHEN OTHERS
      THEN
         DBMS_OUTPUT.put_line ('THE ERROR IS ' || SQLCODE || SQLERRM);
         l_restock_per := 0;
         RETURN l_restock_per;
   END;

   FUNCTION xxwc_get_fiscal_year (p_org_id IN NUMBER, p_date IN DATE)
      RETURN NUMBER
   IS
      l_period_year   NUMBER;
   BEGIN
      SELECT gps.period_year
        INTO l_period_year
        FROM hr_operating_units hou, gl_ledgers gl, gl_period_statuses gps
       WHERE     hou.organization_id = p_org_id
             AND gl.ledger_id = hou.set_of_books_id
             AND gl.ledger_id = gps.set_of_books_id
             AND gl.accounted_period_type = gps.period_type
             AND TRUNC (p_date) BETWEEN gps.start_date AND gps.end_date
             AND gps.application_id = 101;

      RETURN l_period_year;
   EXCEPTION
      WHEN OTHERS
      THEN
         RETURN NULL;
   END;

   FUNCTION xxwc_get_fiscal_month (p_org_id IN NUMBER, p_date IN DATE)
      RETURN VARCHAR2
   IS
      l_period_month   VARCHAR2 (20);
   BEGIN
      SELECT gps.period_name
        INTO l_period_month
        FROM hr_operating_units hou, gl_ledgers gl, gl_period_statuses gps
       WHERE     hou.organization_id = p_org_id
             AND gl.ledger_id = hou.set_of_books_id
             AND gl.ledger_id = gps.set_of_books_id
             AND gl.accounted_period_type = gps.period_type
             AND TRUNC (p_date) BETWEEN gps.start_date AND gps.end_date
             AND gps.application_id = 101;

      RETURN l_period_month;
   EXCEPTION
      WHEN OTHERS
      THEN
         RETURN NULL;
   END;

   FUNCTION calc_ordered_amount (p_header_id IN NUMBER)
      RETURN NUMBER
   IS
      l_extended_amt   NUMBER;
      l_tax_amt        NUMBER;
      l_charge_amt     NUMBER;
      l_total_amt      NUMBER;
   BEGIN
      SELECT NVL (SUM (ordered_quantity * unit_selling_price), 0)
        INTO l_extended_amt
        FROM oe_order_lines
       WHERE header_id = p_header_id;

      SELECT NVL (SUM (tax_value), 0)
        INTO l_tax_amt
        FROM oe_order_lines
       WHERE header_id = p_header_id;

      SELECT NVL (SUM (charge_amount), 0)
        INTO l_charge_amt
        FROM oe_charge_lines_v
       WHERE header_id = p_header_id;

      l_total_amt := (l_extended_amt + l_tax_amt + l_charge_amt) * -1;

      RETURN l_total_amt;
   EXCEPTION
      WHEN OTHERS
      THEN
         l_total_amt := 0;
         RETURN l_total_amt;
   END;

   --Credit Memo---

   --ap balance---
   FUNCTION get_ap_balance (p_vendor_id NUMBER, p_org_id NUMBER)
      RETURN NUMBER
   IS
      l_balance   NUMBER;
   BEGIN
      SELECT SUM (aps.amount_remaining)
        INTO l_balance
        FROM ap_invoices api, ap_payment_schedules aps
       WHERE     1 = 1
             AND api.vendor_id = p_vendor_id
             AND api.org_id = p_org_id
             AND api.invoice_id = aps.invoice_id
             AND api.org_id = aps.org_id
             AND api.cancelled_date IS NULL;


      RETURN l_balance;
   EXCEPTION
      WHEN OTHERS
      THEN
         RETURN NULL;
   END;

   ---ap balance----

   --ar balance---
   FUNCTION get_ar_balance (p_bill_to_customer_id NUMBER)
      RETURN NUMBER
   IS
      l_balance   NUMBER;
   BEGIN
      SELECT SUM (ps.amount_due_remaining)
        INTO l_balance
        FROM ra_customer_trx trx, ar_payment_schedules ps
       WHERE     trx.customer_trx_id = ps.customer_trx_id
             AND trx.bill_to_customer_id = p_bill_to_customer_id;


      RETURN l_balance;
   EXCEPTION
      WHEN OTHERS
      THEN
         RETURN NULL;
   END;

   ---ar balance----

   --Receipt Date Period---
   FUNCTION get_receipt_date_period (p_trx_id NUMBER)
      RETURN DATE
   IS
      l_period_name    VARCHAR2 (20);
      l_receipt_date   DATE;
   BEGIN
      SELECT MAX (acr.receipt_date)
        INTO l_receipt_date
        FROM ar_receivable_applications ara, ar_cash_receipts acr
       WHERE     ara.applied_customer_trx_id = p_trx_id
             AND ara.display = 'Y'
             AND ara.status = 'APP'
             AND ara.application_type = 'CASH'
             AND ara.cash_receipt_id = acr.cash_receipt_id;


      RETURN l_receipt_date;
   EXCEPTION
      WHEN OTHERS
      THEN
         l_receipt_date := NULL;
         RETURN l_receipt_date;
   END;

   --Receipt Date Period---

   --Receipt Date Period---
   FUNCTION get_trx_period_name (p_trx_id NUMBER)
      RETURN VARCHAR2
   IS
      l_period_name   VARCHAR2 (20);
      l_ledgre_id     NUMBER;
      l_gl_date       DATE;
   BEGIN
      SELECT MAX (set_of_books_id), MAX (ara.gl_date)
        INTO l_ledgre_id, l_gl_date
        FROM ar_receivable_applications ara
       WHERE     ara.applied_customer_trx_id = p_trx_id
             AND ara.display = 'Y'
             AND ara.status = 'APP'
             AND ara.application_type = 'CASH';

      SELECT period_name
        INTO l_period_name
        FROM gl_periods gp, gl_ledgers gl
       WHERE     gp.period_set_name = gl.period_set_name
             AND gl.ledger_id = l_ledgre_id
             AND l_gl_date BETWEEN gp.start_date AND gp.end_date;


      RETURN l_period_name;
   EXCEPTION
      WHEN OTHERS
      THEN
         RETURN NULL;
   END;

   --Receipt Date Period---

   FUNCTION get_usetax_amt (p_amount NUMBER, p_post_code NUMBER)
      RETURN NUMBER
   IS
      l_state_tax_rate    NUMBER;
      l_county_tax_rate   NUMBER;
      l_city_tax_rate     NUMBER;
      l_tax_amount        NUMBER;
      l_stcode            NUMBER;
      l_cntycode          NUMBER;
      l_total_tax_rate    NUMBER;
   BEGIN
      BEGIN
         SELECT stcode, cntycode, NVL (curuserate, 0)
           INTO l_stcode, l_cntycode, l_city_tax_rate
           FROM taxware.taxlocltax
          WHERE zipcode = p_post_code;
      EXCEPTION
         WHEN OTHERS
         THEN
            l_city_tax_rate := 0;
      END;

      BEGIN
         SELECT NVL (curuserate, 0)
           INTO l_county_tax_rate
           FROM taxware.taxcntytax
          WHERE cntycode = l_cntycode;
      EXCEPTION
         WHEN OTHERS
         THEN
            l_county_tax_rate := 0;
      END;

      BEGIN
         SELECT NVL (curuserate, 0)
           INTO l_state_tax_rate
           FROM taxware.taxsttax
          WHERE stcode = l_stcode;
      EXCEPTION
         WHEN OTHERS
         THEN
            l_state_tax_rate := 0;
      END;

      l_total_tax_rate :=
         (l_state_tax_rate + l_county_tax_rate + l_city_tax_rate);
      l_tax_amount := (p_amount * l_total_tax_rate);
      RETURN l_tax_amount;
   EXCEPTION
      WHEN OTHERS
      THEN
         l_tax_amount := 0;
         RETURN l_tax_amount;
   END;

   FUNCTION get_usetax_amt (p_amount NUMBER, p_post_code VARCHAR2)
      RETURN NUMBER
   IS
      l_state_tax_rate    NUMBER;
      l_county_tax_rate   NUMBER;
      l_city_tax_rate     NUMBER;
      l_tax_amount        NUMBER;
      l_stcode            NUMBER;
      l_cntycode          NUMBER;
      l_total_tax_rate    NUMBER;
      l_zip_code          NUMBER;
   BEGIN
      BEGIN
         l_zip_code :=
            TO_NUMBER (
               CASE
                  WHEN INSTR (p_post_code, '-') > 0
                  THEN
                     SUBSTR (p_post_code, 1, INSTR (p_post_code, '-') - 1)
                  ELSE
                     p_post_code
               END);

         SELECT stcode, cntycode, NVL (curuserate, 0)
           INTO l_stcode, l_cntycode, l_city_tax_rate
           FROM taxware.taxlocltax
          WHERE zipcode = l_zip_code;
      EXCEPTION
         WHEN OTHERS
         THEN
            l_city_tax_rate := 0;
      END;

      BEGIN
         SELECT NVL (curuserate, 0)
           INTO l_county_tax_rate
           FROM taxware.taxcntytax
          WHERE cntycode = l_cntycode;
      EXCEPTION
         WHEN OTHERS
         THEN
            l_county_tax_rate := 0;
      END;

      BEGIN
         SELECT NVL (curuserate, 0)
           INTO l_state_tax_rate
           FROM taxware.taxsttax
          WHERE stcode = l_stcode;
      EXCEPTION
         WHEN OTHERS
         THEN
            l_state_tax_rate := 0;
      END;

      l_total_tax_rate :=
         (l_state_tax_rate + l_county_tax_rate + l_city_tax_rate);
      l_tax_amount := (p_amount * l_total_tax_rate);
      RETURN l_tax_amount;
   EXCEPTION
      WHEN OTHERS
      THEN
         l_tax_amount := 0;
         RETURN l_tax_amount;
   END;


   FUNCTION get_usetax_rate (p_post_code VARCHAR2)
      RETURN NUMBER
   IS
      l_state_tax_rate    NUMBER;
      l_county_tax_rate   NUMBER;
      l_city_tax_rate     NUMBER;
      l_tax_amount        NUMBER;
      l_stcode            NUMBER;
      l_cntycode          NUMBER;
      l_total_tax_rate    NUMBER := 0;
      l_zip_code          NUMBER := 0;
   BEGIN
      BEGIN
         l_zip_code :=
            TO_NUMBER (
               CASE
                  WHEN INSTR (p_post_code, '-') > 0
                  THEN
                     SUBSTR (p_post_code, 1, INSTR (p_post_code, '-') - 1)
                  ELSE
                     p_post_code
               END);

         SELECT stcode, cntycode, NVL (curuserate, 0)
           INTO l_stcode, l_cntycode, l_city_tax_rate
           FROM taxware.taxlocltax
          WHERE zipcode = l_zip_code;
      EXCEPTION
         WHEN OTHERS
         THEN
            l_city_tax_rate := 0;
      END;

      BEGIN
         SELECT NVL (curuserate, 0)
           INTO l_county_tax_rate
           FROM taxware.taxcntytax
          WHERE cntycode = l_cntycode;
      EXCEPTION
         WHEN OTHERS
         THEN
            l_county_tax_rate := 0;
      END;

      BEGIN
         SELECT NVL (curuserate, 0)
           INTO l_state_tax_rate
           FROM taxware.taxsttax
          WHERE stcode = l_stcode;
      EXCEPTION
         WHEN OTHERS
         THEN
            l_state_tax_rate := 0;
      END;

      l_total_tax_rate :=
         (l_state_tax_rate + l_county_tax_rate + l_city_tax_rate);


      RETURN l_total_tax_rate;
   EXCEPTION
      WHEN OTHERS
      THEN
         l_total_tax_rate := 0;
         RETURN l_total_tax_rate;
   END;

   FUNCTION get_usetax_rate (p_post_code NUMBER)
      RETURN NUMBER
   IS
      l_state_tax_rate    NUMBER;
      l_county_tax_rate   NUMBER;
      l_city_tax_rate     NUMBER;
      l_tax_amount        NUMBER;
      l_stcode            NUMBER;
      l_cntycode          NUMBER;
      l_total_tax_rate    NUMBER := 0;
   BEGIN
      BEGIN
         SELECT stcode, cntycode, NVL (curuserate, 0)
           INTO l_stcode, l_cntycode, l_city_tax_rate
           FROM taxware.taxlocltax
          WHERE zipcode = p_post_code;
      EXCEPTION
         WHEN OTHERS
         THEN
            l_city_tax_rate := 0;
      END;

      BEGIN
         SELECT NVL (curuserate, 0)
           INTO l_county_tax_rate
           FROM taxware.taxcntytax
          WHERE cntycode = l_cntycode;
      EXCEPTION
         WHEN OTHERS
         THEN
            l_county_tax_rate := 0;
      END;

      BEGIN
         SELECT NVL (curuserate, 0)
           INTO l_state_tax_rate
           FROM taxware.taxsttax
          WHERE stcode = l_stcode;
      EXCEPTION
         WHEN OTHERS
         THEN
            l_state_tax_rate := 0;
      END;

      l_total_tax_rate :=
         (l_state_tax_rate + l_county_tax_rate + l_city_tax_rate);


      RETURN l_total_tax_rate;
   EXCEPTION
      WHEN OTHERS
      THEN
         l_total_tax_rate := 0;
         RETURN l_total_tax_rate;
   END;



   --Vendor Quote Batch
   FUNCTION get_venquote_glstring (p_order_number   IN VARCHAR2,
                                   p_type           IN VARCHAR2,
                                   p_line_id        IN NUMBER)
      RETURN VARCHAR2
   IS
      l_location     gl_code_combinations_kfv.segment2%TYPE;
      l_string       gl_code_combinations_kfv.concatenated_segments%TYPE;
      l_trx_number   ra_customer_trx.trx_number%TYPE;
      l_trx_date     VARCHAR2 (20);
   BEGIN
      SELECT gcc.segment2,
             gcc.concatenated_segments,
             rct.trx_number,
             TO_CHAR (rct.trx_date)
        INTO l_location,
             l_string,
             l_trx_number,
             l_trx_date
        FROM gl_code_combinations_kfv gcc,
             ra_customer_trx rct,
             ra_customer_trx_lines rctl,
             ra_cust_trx_line_gl_dist rctlgl,
             oe_order_headers oeh,
             oe_order_lines oel
       WHERE     oeh.header_id = oel.header_id
             AND oeh.order_number = p_order_number
             AND oel.line_id = p_line_id
             AND TO_CHAR (oeh.order_number) = rctl.interface_line_attribute1
             AND TO_CHAR (oel.line_id) = rctl.interface_line_attribute6
             AND rctl.line_type = 'LINE'
             AND rct.customer_trx_id = rctl.customer_trx_id
             AND rctlgl.customer_trx_id = rctl.customer_trx_id
             AND rctlgl.customer_trx_line_id = rctl.customer_trx_line_id
             AND gcc.code_combination_id = rctlgl.code_combination_id;

      IF UPPER (p_type) = 'LOCATION'
      THEN
         RETURN l_location;
      ELSIF UPPER (p_type) = 'STRING'
      THEN
         RETURN l_string;
      ELSIF UPPER (p_type) = 'TRXNUM'
      THEN
         RETURN l_trx_number;
      ELSIF UPPER (p_type) = 'TRXDATE'
      THEN
         RETURN l_trx_date;
      END IF;
   EXCEPTION
      WHEN OTHERS
      THEN
         RETURN NULL;
         DBMS_OUTPUT.put_line ('the error is ' || SQLCODE || SQLERRM);
   END;

   --Vendor Quote Batch

   FUNCTION get_int_req_so_qty (p_inventory_item_id    NUMBER,
                                p_organization_id      NUMBER)
      RETURN NUMBER
   IS
      l_inter_sales_order_qty   NUMBER;
      l_internal_req_qty        NUMBER;
      l_booked_qty              NUMBER;
      l_avialble_qty            NUMBER;
      l_avialble_qty2           NUMBER;
   BEGIN
      --query to get internal sales order qty--

      /*   SELECT   SUM(OEL.ORDERED_QUANTITY-NVL(OL.FULFILLED_QUANTITY,0))
         into L_INTER_SALES_ORDER_QTY
   FROM  OE_ORDER_LINES         OEL,
         OE_ORDER_HEADERS        OEH
        -- PO_REQUISITION_HEADERS  PORH,
        -- PO_REQUISITION_LINES    PORL
     where OEH.HEADER_ID               = OEL.HEADER_ID
    -- AND OEL.SOURCE_DOCUMENT_ID        = PORH.REQUISITION_HEADER_ID
   --  AND OEL.SOURCE_DOCUMENT_LINE_ID   = PORL.REQUISITION_LINE_ID
    -- AND PORH.REQUISITION_HEADER_ID    = PORL.REQUISITION_HEADER_ID
     and OEL.SOURCE_TYPE_CODE          ='INTERNAL'
     and oeh. order_type_id = 1011
    -- and PORL.SOURCE_TYPE_CODE         ='INVENTORY'
   --  AND OEL.ORDER_SOURCE_ID           = 10             --order_source_id for 'Internal'
   --AND oel.orig_sys_document_ref = Int_Req_num'
    -- and OEL.ORG_ID                    = PORH.ORG_ID
     and OEL.INVENTORY_ITEM_ID          = P_INVENTORY_ITEM_ID
     and OEL.SHIP_FROM_ORG_ID           = P_ORGANIZATION_ID
   --  and PORH.TRANSFERRED_TO_OE_FLAG        = 'Y'
     and OL.FLOW_STATUS_CODE not  in ('CANCELLED','CLOSED')
    and  exists(select 1
                         from PO_REQUISITION_LINES prl
                          where  oh.source_document_id = prl.REQUISITION_HEADER_ID);*/
      SELECT SUM (
                  oel.ordered_quantity
                - NVL (oel.fulfilled_quantity, 0)
                - NVL (mr.reservation_quantity, 0))
        INTO l_inter_sales_order_qty
        FROM oe_order_lines_all oel,
             oe_order_headers_all oeh,
             mtl_sales_orders mso,
             mtl_reservations mr
       WHERE     oeh.header_id = oel.header_id
             AND oel.source_type_code = 'INTERNAL'
             AND oeh.order_type_id = 1011
             AND mr.inventory_item_id(+) = oel.inventory_item_id
             AND mr.organization_id(+) = oel.ship_from_org_id
             AND mr.demand_source_line_id(+) = oel.line_id
             AND mso.sales_order_id(+) = mr.demand_source_header_id
             -- and OEL.ORDER_SOURCE_ID           = 10
             AND oel.inventory_item_id = p_inventory_item_id
             AND oel.ship_from_org_id = p_organization_id
             AND oel.flow_status_code NOT IN ('CANCELLED', 'CLOSED')
             AND EXISTS
                    (SELECT 1
                       FROM po_requisition_lines_all prl
                      WHERE oeh.source_document_id =
                               prl.requisition_header_id);

      --query to get internal req which are not interfaced for sales orders ---


      SELECT SUM (prl.quantity)
        INTO l_internal_req_qty
        FROM po_requisition_lines prl, po_requisition_headers prh
       WHERE     prh.requisition_header_id = prl.requisition_header_id
             AND prh.type_lookup_code = 'INTERNAL'
             AND prl.source_type_code = 'INVENTORY'
             AND prl.item_id = p_inventory_item_id
             AND prl.source_organization_id = p_organization_id
             AND prh.transferred_to_oe_flag = 'N';

      --Query to get all booked sales order qty's exculding the return orders and open quotes and internale sales orders

      SELECT SUM (
                  ol.ordered_quantity
                - NVL (ol.fulfilled_quantity, 0)
                - NVL (mr.reservation_quantity, 0))
        INTO l_booked_qty
        FROM oe_order_lines ol,
             oe_order_headers oh,
             mtl_sales_orders mso,
             mtl_reservations mr
       WHERE     oh.header_id = ol.header_id
             AND ol.inventory_item_id = p_inventory_item_id
             AND ol.ship_from_org_id = p_organization_id
             AND mr.inventory_item_id(+) = ol.inventory_item_id
             AND mr.organization_id(+) = ol.ship_from_org_id
             AND mr.demand_source_line_id(+) = ol.line_id
             AND mso.sales_order_id(+) = mr.demand_source_header_id
             AND (ol.ordered_quantity - NVL (ol.fulfilled_quantity, 0)) > 0
             AND oh.flow_status_code IN ('BOOKED')
             AND oh.transaction_phase_code <> 'N' --NOT ALLOWED TO OPEN QUOTES
             AND ol.line_category_code <> 'RETURN' --NOT ALLOWED TO RETURN oRDERS
             AND ol.source_type_code <> 'INTERNAL' --Not allowed to Internale Sales Orders
             --and OH. ORDER_TYPE_ID            <> 1012   --Not allowed to Internale Sales Orders
             --and OL.ORDER_SOURCE_ID           <> 10 --Not allowed to Internale Sales Orders
             AND ol.flow_status_code NOT IN ('CANCELLED', 'CLOSED');

      l_avialble_qty :=
         get_isr_avail_qty (p_inventory_item_id, p_organization_id);

      l_avialble_qty2 :=
           l_avialble_qty
         - (  NVL (l_booked_qty, 0)
            + NVL (l_inter_sales_order_qty, 0)
            + NVL (l_internal_req_qty, 0));

      RETURN l_avialble_qty2;
   EXCEPTION
      WHEN OTHERS
      THEN
         l_avialble_qty2 := 0;
   END;

   FUNCTION get_cash_from_date
      RETURN DATE
   IS
   BEGIN
      RETURN g_cash_from_date;
   END;

   FUNCTION get_cash_to_date
      RETURN DATE
   IS
   BEGIN
      RETURN g_cash_to_date;
   END;

   FUNCTION get_org_item_reserve_qty (p_inventory_item_id    NUMBER,
                                      p_organization_id      NUMBER)
      RETURN NUMBER
   IS
      l_reserve_qty   NUMBER;
   BEGIN
      SELECT NVL (SUM (mr.reservation_quantity), 0)
        INTO l_reserve_qty
        FROM oe_order_lines ol, mtl_sales_orders mso, mtl_reservations mr
       WHERE     mr.inventory_item_id = p_inventory_item_id
             AND mr.organization_id = p_organization_id
             AND mr.demand_source_line_id = ol.line_id --AND  ol.flow_status_CODE not in ('CLOSED')
             AND mr.demand_source_header_id = mso.sales_order_id;

      RETURN l_reserve_qty;
   END;


   FUNCTION get_credit_memo_tax_val (p_type                   IN VARCHAR2,
                                     p_customer_trx_id        IN NUMBER,
                                     p_customer_trx_line_id   IN NUMBER)
      RETURN NUMBER
   IS
      l_tax_value   NUMBER;
      l_tax_rate    NUMBER;
   BEGIN
      SELECT NVL (SUM (zl.tax_amt), 0) tax_val,
             NVL (SUM (zl.tax_rate), 0) tax_rate
        INTO l_tax_value, l_tax_rate
        FROM zx_lines zl
       WHERE     zl.trx_id = p_customer_trx_id
             AND zl.trx_line_id = p_customer_trx_line_id
             AND zl.application_id = 222;

      IF p_type = 'TAXAMT'
      THEN
         RETURN l_tax_value;
      ELSIF p_type = 'TAXRATE'
      THEN
         RETURN l_tax_rate;
      END IF;
   EXCEPTION
      WHEN OTHERS
      THEN
         l_tax_value := 0;
         RETURN l_tax_value;
   END;



   FUNCTION get_best_buy (p_item_id            NUMBER,
                          p_organization_id    NUMBER,
                          p_vendor_number      VARCHAR2)
      RETURN NUMBER
   IS
      l_best_price   NUMBER;
   BEGIN
      SELECT MAX (NVL (pol.unit_price, 0))
        INTO l_best_price
        FROM po_headers poh,
             po_lines pol,
             po_line_locations poll,
             po_vendors pv
       WHERE     poh.po_header_id = pol.po_header_id
             AND poh.type_lookup_code = 'BLANKET'
             AND poh.enabled_flag = 'Y'
             AND NVL (poh.cancel_flag, 'N') = 'N'
             AND poll.po_header_id(+) = pol.po_header_id
             AND poll.po_line_id(+) = pol.po_line_id
             AND pol.item_id = p_item_id
             AND NVL (poll.ship_to_organization_id, p_organization_id) =
                    p_organization_id
             AND poh.vendor_id = pv.vendor_id
             AND pv.segment1 = p_vendor_number;

      RETURN l_best_price;
   EXCEPTION
      WHEN NO_DATA_FOUND
      THEN
         l_best_price := 0;
         RETURN l_best_price;
      WHEN OTHERS
      THEN
         l_best_price := 0;
         RETURN l_best_price;
   END;

   FUNCTION get_contract_customer_name (p_site_use_code    IN VARCHAR2,
                                        p_qual_attribute   IN VARCHAR2)
      RETURN VARCHAR2
   IS
      l_customer_name   VARCHAR2 (2000);
   BEGIN
      IF p_site_use_code IN ('Bill To', 'Bill To Customer')
      THEN
         SELECT DISTINCT hp.party_name
           INTO l_customer_name
           FROM hz_parties hp, hz_cust_accounts hca
          WHERE     hp.party_id = hca.party_id
                AND hca.cust_account_id = p_qual_attribute;

         RETURN l_customer_name;
      -- elsif P_SITE_USE_CODE   IN ('Ship To','Ship To Customer') THEN
      ELSE
         SELECT DISTINCT hp.party_name
           INTO l_customer_name
           FROM hz_parties hp,
                hz_cust_accounts hca,
                hz_cust_acct_sites hcas,
                hz_cust_site_uses_all hcsu
          WHERE     hcsu.cust_acct_site_id = hcas.cust_acct_site_id
                AND hcas.cust_account_id = hca.cust_account_id
                AND hp.party_id = hca.party_id
                AND hcsu.site_use_id = p_qual_attribute;

         RETURN l_customer_name;
      END IF;
   -- RETURN L_CUSTOMER_NAME;
   EXCEPTION
      WHEN OTHERS
      THEN
         l_customer_name := NULL;

         RETURN l_customer_name;
   END;

   PROCEDURE set_cost_location (p_organization_code VARCHAR2)
   IS
   BEGIN
      SELECT organization_id
        INTO g_organization_id
        FROM mtl_parameters
       WHERE organization_code = p_organization_code;
   EXCEPTION
      WHEN OTHERS
      THEN
         g_organization_id := NULL;
   END;


   FUNCTION get_cost_location
      RETURN NUMBER
   IS
   BEGIN
      RETURN g_organization_id;
   END;

   FUNCTION get_req_source (p_organization_id NUMBER)
      RETURN VARCHAR2
   IS
      l_org_code   VARCHAR2 (20);
   BEGIN
      SELECT organization_code
        INTO l_org_code
        FROM mtl_parameters mp
       WHERE mp.organization_id = p_organization_id;

      RETURN l_org_code;
   EXCEPTION
      WHEN OTHERS
      THEN
         RETURN NULL;
   END;


   FUNCTION get_order_line_po_cost (p_inventory_item_id    NUMBER,
                                    p_organization_id      NUMBER,
                                    p_order_date           DATE)
      RETURN NUMBER
   IS
      l_po_cost   NUMBER;
   BEGIN
      /*select max(actual_material)
       into l_po_cost
       from CST_CG_COST_HISTORY_V
      WHERE organization_id = p_organization_id
        AND inventory_item_id = p_inventory_item_id
        and  transaction_id =(
        select max(transaction_id) FROM CST_CG_COST_HISTORY_V
        WHERE organization_id = p_organization_id
        AND inventory_item_id = p_inventory_item_id
       -- AND transaction_type IN ('PO Receipt','Account receipt','Account alias receipt')
        AND transaction_type = 'PO Receipt'
        AND transaction_costed_date <= P_Order_Date);*/

      SELECT SUM (DECODE (cost_element_id, 1, actual_cost, 0))
        INTO l_po_cost
        FROM mtl_cst_actual_cost_details mcd
       WHERE     transaction_id =
                    (SELECT MAX (transaction_id)
                       FROM mtl_cst_actual_cost_details mcd1
                      WHERE     mcd1.inventory_item_id = p_inventory_item_id
                            AND mcd1.organization_id = p_organization_id
                            AND TRUNC (transaction_costed_date) <=
                                   p_order_date)
             AND mcd.inventory_item_id = p_inventory_item_id
             AND mcd.organization_id = p_organization_id
             AND TRUNC (transaction_costed_date) <= p_order_date;


      RETURN l_po_cost;
   EXCEPTION
      WHEN OTHERS
      THEN
         RETURN NULL;
   END;

   FUNCTION get_cash_date (p_payment_set_id NUMBER)
      RETURN DATE
   IS
      l_cash_date   DATE;
   BEGIN
      IF p_payment_set_id IS NULL
      THEN
         RETURN NULL;
      END IF;

      IF g_payment_set_id != p_payment_set_id
      THEN
         BEGIN
            SELECT TRUNC (MIN (creation_date))
              INTO l_cash_date
              FROM ar_receivable_applications_all ra
             WHERE     ra.payment_set_id = p_payment_set_id
                   AND ra.applied_customer_trx_id = -1;
         EXCEPTION
            WHEN OTHERS
            THEN
               l_cash_date := NULL;
         END;

         g_cash_date := l_cash_date;
         g_payment_set_id := p_payment_set_id;
      ELSE
         l_cash_date := g_cash_date;
      END IF;

      RETURN l_cash_date;
   EXCEPTION
      WHEN OTHERS
      THEN
         RETURN NULL;
   END;

   PROCEDURE init_item_details (p_inventory_item_id NUMBER)
   IS
      l_cat_class   VARCHAR2 (400);
   BEGIN
      SELECT MAX (msi.segment1),
             MAX (msi.description),
             MAX (mcv.segment1),
             MAX (ffv.description),
             MAX (mcv.concatenated_segments)
        INTO g_item_rec.item,
             g_item_rec.item_desc,
             g_item_rec.cat,
             g_item_rec.cat_desc,
             g_item_rec.cat_class
        FROM mtl_categories_kfv mcv,
             mtl_item_categories mic,
             mtl_system_items_b msi,
             fnd_flex_values_vl ffv,
             fnd_flex_value_sets ffvs
       WHERE     mcv.structure_id = 101
             AND mic.category_set_id = 1100000062
             AND msi.inventory_item_id = 2927452
             AND mic.organization_id = msi.organization_id
             AND mic.inventory_item_id = msi.inventory_item_id
             AND mic.category_id = mcv.category_id
             AND ffv.flex_value = mcv.segment1
             AND ffvs.flex_value_set_id = ffv.flex_value_set_id
             AND ffvs.flex_value_set_name = 'XXWC_CATEGORY_NUMBER';
   EXCEPTION
      WHEN OTHERS
      THEN
         NULL;
   END;

   FUNCTION get_order_count (p_order_number IN NUMBER)
      RETURN NUMBER
   IS
   BEGIN
      FOR j IN 1 .. g_order_count
      LOOP
         IF p_order_number = g_order_tab (j)
         THEN
            RETURN 0;
         END IF;
      END LOOP;

      g_order_count := g_order_count + 1;
      g_order_tab (g_order_count) := p_order_number;

      RETURN 1;
   EXCEPTION
      WHEN OTHERS
      THEN
         RETURN 0;
   END;


   PROCEDURE get_order_count_status (p_header_id      IN     NUMBER,
                                     p_order_status      OUT NUMBER,
                                     p_order_count       OUT NUMBER) --Added for Product Fill Rate Report by Order and by Line report use
   IS
      CURSOR xx_ordered_qty (p_header_id NUMBER)
      IS
         SELECT ool.line_id
           FROM oe_order_lines_all ool
          WHERE ool.header_id = p_header_id;

      CURSOR xx_shipped_qty (p_line_id NUMBER)
      IS
         SELECT ool.shipped_quantity,
                ool.fulfilled_quantity,
                ool.ordered_quantity,
                ool.line_id,
                ool.line_type_id
           FROM oe_order_lines_all ool
          WHERE ool.line_id = p_line_id;
   BEGIN
      p_order_count := 0;

      IF g_order_id <> p_header_id
      THEN
         p_order_count := 1;
         g_order_id := p_header_id;
         p_order_status := 1;

         FOR ordered_qty IN xx_ordered_qty (p_header_id)
         LOOP
            FOR shipped_qty IN xx_shipped_qty (ordered_qty.line_id)
            LOOP
               IF shipped_qty.line_type_id <> 1005
               THEN
                  IF shipped_qty.ordered_quantity <>
                        shipped_qty.shipped_quantity
                  THEN
                     p_order_status := 0;
                  END IF;
               ELSE
                  IF shipped_qty.ordered_quantity <>
                        shipped_qty.fulfilled_quantity
                  THEN
                     p_order_status := 0;
                  END IF;
               END IF;
            END LOOP;
         END LOOP;
      ELSE
         p_order_status := 0;
      END IF;
   EXCEPTION
      WHEN OTHERS
      THEN
         p_order_status := 0;
         p_order_count := 0;
   END get_order_count_status;

   PROCEDURE get_line_status (p_line_id         IN     NUMBER,
                              p_closed_status      OUT NUMBER,
                              p_bo_status          OUT NUMBER) -- Added to find status of line in Product Fill Rate Report by Order and by Line report
   IS
      l_release_status   VARCHAR2 (1);

      CURSOR xx_shipped_qty
      IS
         SELECT ool.shipped_quantity,
                ool.fulfilled_quantity,
                ool.ordered_quantity,
                ool.line_type_id
           FROM oe_order_lines ool
          WHERE ool.line_id = p_line_id;

      shipped_qty        xx_shipped_qty%ROWTYPE;
   BEGIN
      OPEN xx_shipped_qty;

      FETCH xx_shipped_qty INTO shipped_qty;

      IF shipped_qty.line_type_id <> 1005 -- Added if condition is order is COUNTER ORDER or not
      THEN
         IF shipped_qty.ordered_quantity = shipped_qty.shipped_quantity
         THEN
            p_closed_status := 1;
            p_bo_status := 0;
         ELSE
            p_closed_status := 0;
            p_bo_status := 1;
         END IF;
      ELSE
         IF shipped_qty.ordered_quantity = shipped_qty.fulfilled_quantity
         THEN
            p_closed_status := 1;
            p_bo_status := 0;
         ELSE
            p_closed_status := 0;
            p_bo_status := 1;
         END IF;
      END IF;

      CLOSE xx_shipped_qty;
   EXCEPTION
      WHEN OTHERS
      THEN
         p_closed_status := 0;
         p_bo_status := 1;

         CLOSE xx_shipped_qty;
   END;

   /*PROCEDURE PARSE_PARAM_LIST(P_PROCESS_ID IN NUMBER,P_LIST_NAME IN VARCHAR2, P_LIST_TYPE IN VARCHAR2)
   IS
     lv_Str_List           LONG ;
     lb_cnt                binary_integer;
     la_tab_str            dbms_utility.uncl_array;
     l_list_id             NUMBER;
     l_list_name           VARCHAR2(240);
     l_list_type           VARCHAR2(240);
     lv_str_list_quote     LONG;
   BEGIN
   begin
           select  LIST_ID,
                   LIST_NAME,
                   LIST_VALUES ,
                   list_type
           into
                   L_LIST_ID,
                   L_LIST_NAME,
                   LV_STR_LIST,
                   l_list_type
           FROM APPS.XXWC_PARAM_LIST
           WHERE LIST_NAME = P_LIST_NAME
             AND LIST_TYPE = P_LIST_TYPE;
         EXCEPTION WHEN OTHERS THEN
         FND_FILE.PUT_LINE(FND_FILE.LOG,'The Error is '||SQLCODE||SQLERRM);
         end;
       -- put all the strings in double quotes to avoid special character problems
     -- while parsing with comma_to_table procedure
     lv_Str_List_Quote := '"' || REPLACE( lv_Str_List, ',', '","' ) || '"';

     -- parse the string into comma separated table
     DBMS_UTILITY.COMMA_TO_TABLE(lv_Str_List_Quote, lb_cnt, la_Tab_Str);

     FOR i IN 1 .. la_Tab_Str.COUNT LOOP
       -- remove double quotes added earlier and trim to fetch the actual string
      -- DBMS_OUTPUT.PUT_LINE(TRIM(replace( LA_TAB_STR(I), '"', '' )));
      --TRIM(replace( LA_TAB_STR(I), '"', '' ))
      --REGEXP_REPLACE(LA_TAB_STR(I),'[^[a-z,A-Z,0-9]]*')
       if REGEXP_REPLACE(TRIM(replace( LA_TAB_STR(I), '"', '' )),'[^[a-z,A-Z,0-9]]*') is not null then

       insert into xxeis.EIS_XXWC_PARAM_PARSE_LIST
       ( PROCESS_ID,
         LIST_ID,
         LIST_NAME,
         LIST_VALUE,
         LIST_TYPE
       )
       values
       ( P_PROCESS_ID,
         L_LIST_ID,
         L_LIST_NAME,
         trim(replace(LA_TAB_STR(I),'"','')),
         L_LIST_TYPE
       );

       COMMIT;


       END IF;
     END LOOP;
   END; */

   PROCEDURE parse_param_list (p_process_id   IN NUMBER,
                               p_list_name    IN VARCHAR2,
                               p_list_type    IN VARCHAR2)
   IS
      lv_str_list                    CLOB;
      lb_cnt                         BINARY_INTEGER;
      l_comma_index                  PLS_INTEGER;
      l_index                        PLS_INTEGER := 1;
      la_tab_str                     DBMS_UTILITY.uncl_array;
      non_occuring_prefix   CONSTANT VARCHAR2 (4) := 'zzzz';
      l_list_id                      NUMBER;
      l_list_name                    VARCHAR2 (240);
      l_list_type                    VARCHAR2 (240);
      lv_str_list_quote              LONG;
      lv_value                       VARCHAR2 (200);
      lvchr                          VARCHAR2 (20) := 'CHR(10)';
   BEGIN
      --execute immediate ' Set define off ';
      --insert into xxeis.log values (P_LIST_NAME ); commit;
      --insert into xxeis.log values (P_LIST_TYPE ); commit;
      BEGIN
         SELECT list_id,
                list_name,
                LIST_VALUES,
                list_type
           INTO l_list_id,
                l_list_name,
                lv_str_list,
                l_list_type
           FROM apps.xxwc_param_list
          WHERE     list_name = REPLACE (p_list_name, '''', '')
                AND list_type = p_list_type
                AND ROWNUM <= 1;
      EXCEPTION
         WHEN OTHERS
         THEN
            fnd_file.put_line (fnd_file.LOG,
                               'The Error is ' || SQLCODE || SQLERRM);
      END;

      lv_str_list := TO_CLOB (REPLACE (lv_str_list, CHR (10)));
      lv_str_list := lv_str_list || ',';

      --insert into xxeis.log values (lv_Str_List ); commit;

      IF lv_str_list IS NOT NULL
      THEN
         LOOP
            l_comma_index := INSTR (lv_str_list, ',', l_index);
            EXIT WHEN l_comma_index = 0;
            lv_value := SUBSTR (lv_str_list, l_index, l_comma_index - l_index);
            -- FND_FILE.PUT_LINE(FND_FILE.LOG,lv_value);
            --   insert into xxeis.log values ('comma_index' ||l_comma_index ||'Value , '|| lv_value ); commit;
            l_index := l_comma_index + 1;

            IF REGEXP_REPLACE (TRIM (lv_value), '[^[a-z,A-Z,0-9]]*')
                  IS NOT NULL
            THEN
               INSERT INTO xxeis.eis_xxwc_param_parse_list (process_id,
                                                            list_id,
                                                            list_name,
                                                            list_value,
                                                            list_type)
                       VALUES (
                                 p_process_id,
                                 l_list_id,
                                 l_list_name, -- TRIM(substr(LA_TAB_STR(i),length(non_occuring_prefix)+1)),
                                 TRIM (
                                    LTRIM (
                                       REGEXP_REPLACE (lv_value,
                                                       '([[:cntrl:]])|(^\t)',
                                                       NULL))),
                                 l_list_type);
            END IF;
         END LOOP;
      END IF;

      COMMIT;
   END;

   /*
   PROCEDURE PARSE_PARAM_LIST(P_PROCESS_ID IN NUMBER,P_LIST_NAME IN VARCHAR2, P_LIST_TYPE IN VARCHAR2)
   IS
     lv_Str_List           LONG ;
     lb_cnt                binary_integer;
     la_tab_str            dbms_utility.uncl_array;
     non_occuring_prefix   constant varchar2(4) := 'zzzz';
     l_list_id             NUMBER;
     l_list_name           VARCHAR2(240);
     l_list_type           VARCHAR2(240);
     lv_str_list_quote     LONG;
   BEGIN
   begin
           select  LIST_ID,
                   LIST_NAME,
                   LIST_VALUES ,
                   list_type
           into
                   L_LIST_ID,
                   L_LIST_NAME,
                   LV_STR_LIST,
                   l_list_type
           FROM APPS.XXWC_PARAM_LIST
          WHERE LIST_NAME = P_LIST_NAME
            AND LIST_TYPE = P_LIST_TYPE
            AND ROWNUM <=1;
         EXCEPTION WHEN OTHERS THEN
         FND_FILE.PUT_LINE(FND_FILE.LOG,'The Error is '||SQLCODE||SQLERRM);
         end;


     FOR i IN (select regexp_substr(LV_STR_LIST,'[^,]+',1,level) value
        from dual
      connect by level <= length(regexp_replace(LV_STR_LIST,'[^,]+')) + 1) LOOP
       -- remove double quotes added earlier and trim to fetch the actual string
      -- DBMS_OUTPUT.PUT_LINE(TRIM(replace( LA_TAB_STR(I), '"', '' )));
      if TRIM(i.value) is not null then

       insert into xxeis.EIS_XXWC_PARAM_PARSE_LIST
       ( PROCESS_ID,
         LIST_ID,
         LIST_NAME,
         LIST_VALUE,
         LIST_TYPE
       )
       values
       ( P_PROCESS_ID,
         L_LIST_ID,
         L_LIST_NAME,
        -- TRIM(substr(LA_TAB_STR(i),length(non_occuring_prefix)+1)),
         TRIM(i.value),
         L_LIST_TYPE
       );

     END IF;
     END LOOP;
        COMMIT;
   END;
   */
   PROCEDURE parse_cleanup_table (p_process_id IN NUMBER)
   IS
   BEGIN
      DELETE FROM xxeis.eis_xxwc_param_parse_list
            WHERE process_id = p_process_id;

      COMMIT;
   END;

   FUNCTION get_flag_value
      RETURN VARCHAR2
   IS
   BEGIN
      RETURN g_flag_value;
   END get_flag_value;

   FUNCTION get_item_flag_value
      RETURN VARCHAR2
   IS
   BEGIN
      RETURN g_item_flag;
   END get_item_flag_value;

   FUNCTION get_cat_flag_value
      RETURN VARCHAR2
   IS
   BEGIN
      RETURN g_cat_flag;
   END get_cat_flag_value;


   FUNCTION get_inv_cat_segments (p_inventory_item_id    NUMBER,
                                  p_organization_id      NUMBER)
      RETURN VARCHAR2
   IS
      l_cat_class   VARCHAR2 (400);
   BEGIN
      SELECT mcv.concatenated_segments
        INTO l_cat_class
        FROM mtl_categories_kfv mcv,
             mtl_category_sets mcs,
             mtl_item_categories mic
       WHERE     mcs.category_set_name = 'Inventory Category'
             AND mcs.structure_id = mcv.structure_id
             AND mic.inventory_item_id = p_inventory_item_id
             AND mic.organization_id = p_organization_id
             AND mic.category_set_id = mcs.category_set_id
             AND mic.category_id = mcv.category_id;

      RETURN l_cat_class;
   EXCEPTION
      WHEN OTHERS
      THEN
         RETURN NULL;
   END;

   FUNCTION get_empolyee_name (p_employee_id IN NUMBER, p_date IN DATE)
      RETURN VARCHAR2
   IS
      l_full_name     VARCHAR2 (240);
      l_employee_id   NUMBER;
   BEGIN
      SELECT employee_id
        INTO l_employee_id
        FROM fnd_user
       WHERE user_id = p_employee_id;

      SELECT ppf.full_name
        INTO l_full_name
        FROM per_all_people_f ppf
       WHERE     1 = 1
             AND ppf.person_id = l_employee_id
             AND (TRUNC (p_date) BETWEEN TRUNC (ppf.effective_start_date)
                                     AND NVL (ppf.effective_end_date, p_date));

      RETURN l_full_name;
   EXCEPTION
      WHEN OTHERS
      THEN
         RETURN NULL;
   END;

   FUNCTION get_bill_line_cost (p_header_id           IN NUMBER,
                                p_inventory_item_id   IN NUMBER)
      RETURN NUMBER
   IS
      l_cost      NUMBER := 0;
      l_line_id   NUMBER;
   BEGIN
      SELECT MIN (ol.reference_line_id)
        INTO l_line_id
        FROM oe_order_lines ol
       WHERE     ol.header_id = p_header_id
             AND ol.inventory_item_id = p_inventory_item_id
             AND ol.line_type_id = 1008;

      /*
       if  l_line_id is null THEN
         select min(ol.reference_line_id)
          INTO l_line_id
          from oe_order_lines ol
         where ol.header_id    = p_header_id
           AND ol.line_type_id = 1008;
       END IF;*/

      l_cost :=
         NVL (apps.xxwc_mv_routines_pkg.get_order_line_cost (l_line_id), 0);

      RETURN l_cost;
   EXCEPTION
      WHEN OTHERS
      THEN
         RETURN 0;
   END;

   FUNCTION get_payment_amount (p_order_number     IN NUMBER,
                                p_payment_set_id   IN NUMBER,
                                p_header_id        IN NUMBER)
      RETURN NUMBER
   IS
      l_receipt_adj      NUMBER := 0;
      l_invoice_amount   NUMBER := 0;
      l_amount           NUMBER := 0;
      l_order_amount     NUMBER := 0;
   BEGIN
      BEGIN
         SELECT SUM (
                     NVL (aps.amount_due_original, 0)
                   - NVL (aps.amount_due_remaining, 0))
                   amount
           INTO l_invoice_amount
           FROM ar_payment_schedules_all aps, ra_customer_trx_all rct
          WHERE     rct.interface_header_context = 'ORDER ENTRY'
                AND rct.customer_trx_id = aps.customer_trx_id
                AND TO_CHAR (p_order_number) =
                       rct.interface_header_attribute1;
      EXCEPTION
         WHEN OTHERS
         THEN
            l_invoice_amount := 0;
      END;

      BEGIN
         SELECT CASE
                   WHEN (   oh.order_type_id != 1001
                         OR EXISTS
                               (SELECT 1
                                  FROM oe_order_lines ol1
                                 WHERE     ol1.header_id = oh.header_id
                                       AND ol1.ordered_item LIKE '%DEPOSIT%'))
                   THEN
                      (SELECT NVL (
                                 ROUND (
                                    SUM (
                                         ROUND (
                                              DECODE (ol.line_category_code,
                                                      'RETURN', -1,
                                                      1)
                                            * ol.ordered_quantity
                                            * ol.unit_selling_price,
                                            2)
                                       + ol.tax_value),
                                    2),
                                 0)
                                 order_amount
                         FROM oe_order_lines ol
                        WHERE ol.header_id = oh.header_id)
                   WHEN oh.order_type_id = 1001
                   THEN
                      (SELECT NVL (
                                 ROUND (
                                    SUM (
                                         ROUND (
                                              DECODE (ol.line_category_code,
                                                      'RETURN', -1,
                                                      1)
                                            * NVL (ol.shipped_quantity, 0)
                                            * ol.unit_selling_price,
                                            2)
                                       + ol.tax_value),
                                    2),
                                 0)
                                 order_amount
                         FROM oe_order_lines ol
                        WHERE     ol.header_id = oh.header_id
                              AND NVL (shipped_quantity, 0) <> 0
                              AND TRUNC (ol.actual_shipment_date) <=
                                     xxeis.eis_rs_xxwc_com_util_pkg.get_cash_to_date
                              AND ol.ordered_item NOT LIKE '%DEPOSIT%')
                   ELSE
                      0
                END
                   order_amount
           INTO l_order_amount
           FROM oe_order_headers oh
          WHERE header_id = p_header_id;
      EXCEPTION
         WHEN OTHERS
         THEN
            l_order_amount := 0;
      END;

      IF l_invoice_amount != l_order_amount
      THEN
         BEGIN
            SELECT SUM (NVL (amount_applied, 0)) amount
              INTO l_receipt_adj
              FROM ar_receivable_applications_all rap, ar_receivables_trx rt
             WHERE     SIGN (rap.applied_payment_schedule_id) < 0
                   AND rap.receivables_trx_id = rt.receivables_trx_id
                   AND rt.TYPE = 'WRITEOFF'
                   AND rap.status = 'ACTIVITY'
                   AND rap.cash_receipt_id IN (SELECT cash_receipt_id
                                                 FROM ar_receivable_applications_all rap1
                                                WHERE rap1.payment_set_id =
                                                         p_payment_set_id);
         EXCEPTION
            WHEN OTHERS
            THEN
               l_receipt_adj := 0;
         END;
      END IF;

      DBMS_OUTPUT.put_line (
            'Invoice Amount :'
         || l_invoice_amount
         || 'Receipt Adj := '
         || l_receipt_adj);
      l_amount := NVL (l_invoice_amount, 0) + NVL (l_receipt_adj, 0);

      RETURN l_amount;
   EXCEPTION
      WHEN OTHERS
      THEN
         RETURN 0;
   END;

   FUNCTION get_order_freight (p_header_id IN NUMBER)
      RETURN NUMBER
   IS
      l_adj_amt   NUMBER := 0;
   BEGIN
      SELECT SUM (NVL (adjusted_amount, 0))
        INTO l_adj_amt
        FROM oe_price_adjustments_v
       WHERE     list_line_type_code = 'FREIGHT_CHARGE'
             AND applied_flag = 'Y'
             AND header_id = p_header_id;

      RETURN l_adj_amt;
   EXCEPTION
      WHEN OTHERS
      THEN
         RETURN 0;
   END;

   PROCEDURE set_period_name (p_period_name_from VARCHAR)
   IS
   BEGIN
      IF p_period_name_from IS NOT NULL
      THEN
         g_period_name_from := p_period_name_from;
      ELSE
         SELECT period_name
           INTO g_period_name_from
           FROM gl_periods gp
          WHERE     gp.period_set_name = '4-4-QTR'
                AND TRUNC (SYSDATE) BETWEEN TRUNC (gp.start_date)
                                        AND TRUNC (gp.end_date);
      END IF;
   EXCEPTION
      WHEN OTHERS
      THEN
         g_period_name_from := NULL;
   END set_period_name;

   FUNCTION get_open_order_status (p_item_id           IN NUMBER,
                                   p_organization_id   IN NUMBER)
      RETURN VARCHAR2
   IS
      l_flag   VARCHAR2 (20);
   BEGIN
      SELECT DISTINCT 'Y'
        INTO l_flag
        FROM oe_order_headers h, oe_order_lines l, oe_transaction_types_vl t
       WHERE     h.header_id = l.header_id
             AND t.transaction_type_id = h.order_type_id
             AND t.name IN ('STANDARD ORDER',
                            'COUNTER ORDER',
                            'RETURN ORDER',
                            'REPAIR ORDER')
             AND l.inventory_item_id = p_item_id
             AND l.ship_from_org_id = p_organization_id
             AND l.flow_status_code NOT IN ('CLOSED', 'CANCELLED');

      RETURN l_flag;
   EXCEPTION
      WHEN NO_DATA_FOUND
      THEN
         l_flag := 'N';
         RETURN l_flag;
      WHEN OTHERS
      THEN
         l_flag := 'N';
         RETURN l_flag;
   END;

   FUNCTION get_doc_date (p_header_id       IN NUMBER,
                          p_line_id         IN NUMBER,
                          p_item_id         IN NUMBER,
                          p_org_id          IN NUMBER,
                          p_header_org_id   IN NUMBER)
      RETURN DATE
   IS
      l_doc_date   DATE;
   BEGIN
      SELECT MIN (doc_date)
        INTO l_doc_date
        FROM (SELECT xpl.actual_completion_date doc_date
                FROM xxwc.xxwc_print_log_tbl xpl,
                     xxwc.xxwc_wsh_shipping_stg xws
               WHERE     1 = 1
                     AND xpl.header_id = p_header_id
                     AND xpl.organization_id = p_header_org_id
                     AND xpl.concurrent_program_id IN (70401, 75404) --packing slip and internal packslip
                     AND xpl.program_application_id = 20005
                     AND xpl.argument10 = TO_CHAR (xws.delivery_id)
                     AND xpl.header_id = xws.header_id
                     AND xws.inventory_item_id = p_item_id
                     AND xws.ship_from_org_id = p_org_id
                     AND xws.line_id = p_line_id
                     AND xpl.request_id =
                            (SELECT MIN (xpl2.request_id)
                               FROM xxwc.xxwc_print_log_tbl xpl2
                              WHERE     1 = 1
                                    AND xpl2.concurrent_program_id IN (70401,
                                                                       75404)
                                    AND xpl2.program_application_id = 20005
                                    AND xpl2.header_id = xpl.header_id)
              UNION
              SELECT fcr.actual_completion_date
                FROM fnd_concurrent_requests fcr,
                     xxwc.xxwc_wsh_shipping_stg xws,
                     fnd_user fu,
                     fnd_concurrent_programs_vl fcp
               WHERE     1 = 1
                     AND fcr.argument2 = TO_CHAR (p_header_id)
                     AND fcr.argument1 = TO_CHAR (p_header_org_id)
                     AND fcr.concurrent_program_id =
                            fcp.concurrent_program_id
                     AND fcr.requested_by = fu.user_id
                     AND fcr.program_application_id = 20005
                     AND fcr.concurrent_program_id IN (70401, 75404) --packing slip and internal packslip
                     AND fcr.argument10 = TO_CHAR (xws.delivery_id)
                     AND fcr.argument2 = TO_CHAR (xws.header_id)
                     AND xws.inventory_item_id = p_item_id
                     AND xws.ship_from_org_id = p_org_id
                     AND xws.line_id = p_line_id
                     AND fcr.request_id =
                            (SELECT MIN (fcr1.request_id)
                               FROM fnd_concurrent_requests fcr1
                              WHERE     1 = 1
                                    AND fcr1.concurrent_program_id IN (70401,
                                                                       75404)
                                    AND fcr1.program_application_id = 20005
                                    AND fcr1.argument2 = fcr.argument2
                                    AND fcr1.argument10 = fcr.argument10));

      RETURN l_doc_date;
   EXCEPTION
      WHEN OTHERS
      THEN
         RETURN NULL;
   END;

   FUNCTION get_line_id (p_line_id IN NUMBER)
      RETURN NUMBER
   IS
   BEGIN
      FOR j IN 1 .. g_order_count
      LOOP
         IF p_line_id = g_order_tab (j)
         THEN
            RETURN 0;
         END IF;
      END LOOP;

      g_order_count := g_order_count + 1;
      g_order_tab (g_order_count) := p_line_id;

      RETURN 1;
   EXCEPTION
      WHEN OTHERS
      THEN
         RETURN 0;
   END;

   FUNCTION get_promo_item (p_item_id IN NUMBER, p_org_id IN NUMBER)
      RETURN VARCHAR2
   IS
      l_cat_type   VARCHAR2 (240);
   BEGIN
      BEGIN
         SELECT 'Y'
           INTO l_cat_type
           FROM                                      --mtl_system_items_b msi,
               mtl_item_categories_v mic
          WHERE     mic.segment2 = 'PRMO' --    AND msi.inventory_item_id = mic.inventory_item_id
                           --    AND msi.organization_id = mic.organization_id
                AND mic.inventory_item_id = p_item_id
                AND mic.organization_id = p_org_id;
      EXCEPTION
         WHEN NO_DATA_FOUND
         THEN
            --dbms_output.PUT_LINE('Stage1');
            l_cat_type := 'N';
            RETURN l_cat_type;
         WHEN OTHERS
         THEN
            --dbms_output.PUT_LINE('Stage1');
            l_cat_type := 'N';
            RETURN l_cat_type;
      END;

      RETURN l_cat_type;
   END;

   FUNCTION get_delivery_charge_amt (p_trx_id IN NUMBER)
      RETURN NUMBER
   IS
      l_charge_amt   NUMBER;
   BEGIN
      IF g_pre_header_id = p_trx_id
      THEN
         RETURN 0;
      ELSE
         g_pre_header_id := p_trx_id;

         BEGIN
            SELECT SUM (extended_amount)
              INTO l_charge_amt
              FROM apps.ra_customer_trx_lines
             WHERE     customer_trx_id = p_trx_id
                   AND line_type = 'LINE'
                   AND interface_line_context = 'ORDER ENTRY'
                   AND description = 'Delivery Charge';

            RETURN l_charge_amt;
         EXCEPTION
            WHEN NO_DATA_FOUND
            THEN
               l_charge_amt := 0;
               RETURN l_charge_amt;
            WHEN OTHERS
            THEN
               l_charge_amt := 0;
               RETURN l_charge_amt;
         END;
      END IF;

      RETURN l_charge_amt;
   END;

   FUNCTION yy_get_doc_date (p_header_id       IN NUMBER,
                             p_line_id         IN NUMBER,
                             p_item_id         IN NUMBER,
                             p_org_id          IN NUMBER,
                             p_header_org_id   IN NUMBER)
      RETURN DATE
   IS
      l_doc_date   DATE;
   BEGIN
      SELECT MIN (doc_date)
        INTO l_doc_date
        FROM (SELECT xpl.actual_completion_date doc_date
                FROM xxwc.xxwc_print_log_tbl xpl,
                     xxwc.xxwc_wsh_shipping_stg xws
               WHERE     1 = 1
                     AND xpl.header_id = p_header_id
                     AND xpl.organization_id = p_header_org_id
                     AND xpl.concurrent_program_id IN (70401, 75404) --packing slip and internal packslip
                     AND xpl.argument10 = TO_CHAR (xws.delivery_id)
                     AND xpl.header_id = xws.header_id
                     AND xws.inventory_item_id = p_item_id
                     AND xws.ship_from_org_id = p_org_id
                     AND xws.line_id = p_line_id
                     AND xpl.request_id =
                            (SELECT MIN (xpl2.request_id)
                               FROM xxwc.xxwc_print_log_tbl xpl2
                              WHERE     1 = 1
                                    AND xpl2.concurrent_program_id IN (70401,
                                                                       75404)
                                    AND xpl2.header_id = xpl.header_id));

      RETURN l_doc_date;
   EXCEPTION
      WHEN OTHERS
      THEN
         RETURN NULL;
   END;

   FUNCTION GET_PERSON_NAME (P_EMPLOYEE_ID IN NUMBER, P_DATE IN DATE)
      RETURN VARCHAR2
   IS
      l_full_name     VARCHAR2 (240);
      L_employee_id   NUMBER;
   BEGIN
      SELECT ppf.full_name
        INTO l_full_name
        FROM per_all_people_f ppf
       WHERE     1 = 1
             AND ppf.person_id = P_EMPLOYEE_ID
             AND (TRUNC (P_DATE) BETWEEN TRUNC (ppf.effective_start_date)
                                     AND NVL (ppf.effective_end_date, P_DATE));

      RETURN l_full_name;
   EXCEPTION
      WHEN OTHERS
      THEN
         RETURN NULL;
   END;

   /*procedure POPULATE_LOCATIONS(P_PROCESS_ID IN NUMBER) is

   BEGIN

   INSERT INTO APPS.XXWC_HR_LOCATIONS(PROCESS_ID,LOCATION_ID,LOCATION_CODE,INVENTORY_ORGANIZATION_ID)
         ( SELECT  P_PROCESS_ID,
                   LOCATION_ID,
                   SUBSTR(LOCATION_CODE,1,3),
                   INVENTORY_ORGANIZATION_ID
           FROM APPS.HR_LOCATIONS_ALL
         );
   COMMIT;
   END;

   procedure CLEANUP_POPULATE_LOCATIONS(P_PROCESS_ID IN NUMBER) is

   BEGIN

     DELETE from APPS.XXWC_HR_LOCATIONS WHERE PROCESS_ID =P_PROCESS_ID;
     COMMIT;

   END; */

   FUNCTION GET_BALANCE_AMOUNT (p_order_number     IN NUMBER,
                                p_payment_set_id   IN NUMBER,
                                p_header_id        IN NUMBER)
      RETURN NUMBER
   IS
      l_receipt_adj      NUMBER := 0;
      L_INVOICE_AMOUNT   NUMBER := 0;
      l_amount           NUMBER := 0;
      l_order_amount     NUMBER := 0;
   BEGIN
      BEGIN
         SELECT --SUM(NVL(aps.amount_due_original,0)-NVL(aps.amount_due_remaining,0)) amount
                SUM (NVL (APS.AMOUNT_DUE_REMAINING, 0)) amount
           INTO l_invoice_amount
           FROM ar_payment_schedules_all aps, RA_CUSTOMER_TRX_all RCT
          WHERE     rct.INTERFACE_header_CONTEXT = 'ORDER ENTRY'
                AND RCT.CUSTOMER_TRX_ID = aps.CUSTOMER_TRX_ID
                AND TO_CHAR (p_order_number) =
                       RCT.INTERFACE_HEADER_ATTRIBUTE1;
      EXCEPTION
         WHEN OTHERS
         THEN
            l_invoice_amount := 0;
      END;

      /* BEGIN
         select
          CASE
            WHEN (oh.order_type_id != 1001
            OR EXISTS
              (SELECT 1
              FROM OE_ORDER_LINES OL1
              WHERE OL1.header_id = OH.HEADER_ID
              AND OL1.ORDERED_ITEM LIKE '%DEPOSIT%'
              ))
            THEN
              (SELECT NVL(ROUND(SUM(ROUND(DECODE(ol.line_category_code,'RETURN', -1,1)* OL.ORDERED_QUANTITY*OL.UNIT_SELLING_PRICE,2)+ OL.TAX_VALUE),2),0) ORDER_AMOUNT
              FROM OE_ORDER_LINES OL
              WHERE OL.HEADER_ID=OH.HEADER_ID
              )
            WHEN oh.order_type_id = 1001
            THEN
              (SELECT NVL (ROUND(SUM(ROUND(DECODE(ol.line_category_code,'RETURN', -1,1)* NVL(OL.SHIPPED_QUANTITY,0)*OL.UNIT_SELLING_PRICE,2)+ OL.TAX_VALUE),2),0) ORDER_AMOUNT
              FROM OE_ORDER_LINES OL
              WHERE OL.HEADER_ID                  =OH.HEADER_ID
              AND NVL(SHIPPED_QUANTITY,0)        <> 0
              AND TRUNC(OL.ACTUAL_SHIPMENT_DATE) <=XXEIS.EIS_RS_XXWC_COM_UTIL_PKG.GET_CASH_TO_DATE
              AND OL.ORDERED_ITEM NOT LIKE '%DEPOSIT%'
              )
            ELSE 0
          END ORDER_AMOUNT
          into l_order_amount
          FROM oe_order_headers oh
           Where header_id = p_header_id;
      EXCEPTION
        WHEN OTHERS THEN
          l_order_amount := 0;
      END;
      */
      -- IF L_INVOICE_AMOUNT != L_ORDER_AMOUNT THEN
      RETURN L_INVOICE_AMOUNT;
   -- ELSE
   --  return 0;
   /* BEGIN
      SELECT SUM(NVL(amount_applied,0)) amount
        INTO l_receipt_adj
        FROM ar_receivable_applications_all rap,
             ar_receivables_trx rt
       WHERE SIGN(rap.APPLIED_PAYMENT_SCHEDULE_ID) < 0
         AND rap.receivables_trx_id  = rt.receivables_trx_id
         AND rt.type ='WRITEOFF'
         AND rap.STATUS          = 'ACTIVITY'
             AND rap.cash_receipt_id IN
         (SELECT cash_receipt_id
             FROM ar_receivable_applications_all rap1
             WHERE RAP1.PAYMENT_SET_ID = P_PAYMENT_SET_ID
         );
   EXCEPTION
     WHEN OTHERS THEN
       L_RECEIPT_ADJ := 0;
   END;*/
   -- END IF;
   --DBMS_OUTPUT.PUT_LINE('Invoice Amount :' ||L_INVOICE_AMOUNT ||'Receipt Adj := '||L_RECEIPT_ADJ);
   --l_amount  := nvl(l_invoice_amount,0) + nvl(l_receipt_adj,0);

   --  return l_amount;
   EXCEPTION
      WHEN OTHERS
      THEN
         RETURN 0;
   END;

   FUNCTION get_pricing_resp (p_user_id IN NUMBER)
      RETURN VARCHAR2
   IS
      l_resp_name   VARCHAR2 (2000);
   BEGIN
      EXECUTE IMMEDIATE
         ' SELECT  MAX(FRV.RESPONSIBILITY_NAME)
FROM  FND_RESPONSIBILITY_VL FRV,
FND_USER_RESP_GROUPS FURG
WHERE FRV.RESPONSIBILITY_ID = FURG.RESPONSIBILITY_ID
AND   FRV.APPLICATION_ID    = FURG.RESPONSIBILITY_APPLICATION_ID
AND   RESPONSIBILITY_NAME LIKE ''%Prici%''
AND   USER_ID               = :1'
         INTO l_resp_name
         USING p_user_id;

      RETURN l_resp_name;
   EXCEPTION
      WHEN OTHERS
      THEN
         RETURN NULL;
   END;

   FUNCTION get_low_high_f (p_item IN VARCHAR2, p_qty IN NUMBER)
      RETURN VARCHAR2
   IS
      l_value   VARCHAR2 (20);
      l_qty     NUMBER;
   BEGIN
      SELECT TO_NUMBER (description)
        INTO l_qty
        FROM fnd_lookup_values
       WHERE     lookup_type = 'XXWC_QP_ITEM_LOW_QTY_LOOKUP'
             AND lookup_code = p_item;

      DBMS_OUTPUT.put_line ('L_QTY' || l_qty);

      IF p_qty > l_qty
      THEN
         l_value := 'HIGH';
         DBMS_OUTPUT.put_line ('High if');
         RETURN l_value;
      ELSE
         l_value := 'LOW';
         DBMS_OUTPUT.put_line ('Low if');
         RETURN l_value;
      END IF;
   EXCEPTION
      WHEN NO_DATA_FOUND
      THEN
         IF p_qty > 3
         THEN
            l_value := 'HIGH';
            DBMS_OUTPUT.put_line ('No data Low if');
         ELSE
            l_value := 'LOW';
         END IF;

         RETURN l_value;
      WHEN OTHERS
      THEN
         l_value := 'HIGH';
         DBMS_OUTPUT.put_line ('when others Low if');
         RETURN l_value;
   END;

   -- Version# 1.1 > Start
   /********************************************************************************
   ProcedureName : wcd_pre_trig
   Purpose       : Used by "White Cap Direct Processing Report"

   HISTORY
   ===============================================================================
   VERSION DATE          AUTHOR(S)       DESCRIPTION
   ------- -----------   --------------- -----------------------------------------
   1.0     02/28/2014    Gopi Damuluri   Initial Creation
   ********************************************************************************/
   PROCEDURE wcd_pre_trig (p_ship_date_from   IN DATE,
                           p_ship_date_to     IN DATE,
                           p_warehouse        IN VARCHAR2)
   IS
   BEGIN
      --------------------------------------------------------------------------
      -- Insert into staging table XXWC_OEL_WCD_GTT_TBL
      --------------------------------------------------------------------------
      INSERT INTO xxwc.xxwc_oel_wcd_gtt_tbl (header_id,       --creation_date,
                                             line_category_code,
                                             ordered_quantity,
                                             unit_selling_price,
                                             tax_value,
                                             line_id,
                                             actual_shipment_date,
                                             shipping_method_code,
                                             ship_from_org_id,
											 inventory_item_id  --Added by Mahender for TMS#20150827-00075 on 09/25/2015
											 )
         SELECT header_id,                                          --sysdate,
                line_category_code,
                ordered_quantity,
                unit_selling_price,
                tax_value,
                line_id,
                actual_shipment_date,
                shipping_method_code,
                ship_from_org_id,
				inventory_item_id  --Added by Mahender for TMS#20150827-00075 on 09/25/2015
           FROM oe_order_lines_all
          WHERE     1 = 1
                AND shipping_method_code LIKE '%WCD%'
                AND TRUNC (actual_shipment_date) >= p_ship_date_from
                AND TRUNC (actual_shipment_date) <= p_ship_date_to;

      --------------------------------------------------------------------------
      -- Insert into staging table XXWC_OEH_WCD_GTT_TBL
      --------------------------------------------------------------------------
      INSERT INTO xxwc.xxwc_oeh_wcd_gtt_tbl (order_number,   -- creation_date,
                                             header_id,
                                             attribute2,
                                             ship_from_org_id)
         SELECT DISTINCT ooh.order_number,                          --sysdate,
                         ooh.header_id,
                         ooh.attribute2,
                         ooh.ship_from_org_id
           FROM oe_order_headers_all ooh
          WHERE     1 = 1
                AND EXISTS
                       (SELECT '1'
                          FROM xxwc.xxwc_oel_wcd_gtt_tbl oel
                         WHERE 1 = 1 AND oel.header_id = ooh.header_id);

      DBMS_STATS.gather_index_stats (ownname   => 'XXWC',
                                     indname   => 'XXWC_OEH_WCD_GTT_TBL_N1');
      DBMS_STATS.gather_index_stats (ownname   => 'XXWC',
                                     indname   => 'XXWC_OEL_WCD_GTT_TBL_N1');
   EXCEPTION
      WHEN OTHERS
      THEN
         NULL;
   END wcd_pre_trig;

   -- Version# 1.1 < End

   -- Version# 1.2 > Start
   /********************************************************************************
   ProcedureName : xxwc_order_exception_pre_trig
   Purpose       : Used by "White Cap Order Exception Report"

   HISTORY
   ===============================================================================
   VERSION DATE          AUTHOR(S)       DESCRIPTION
   ------- -----------   --------------- -----------------------------------------
   1.0     03/05/2014    Harsha Yedla   Initial Creation
   1.15    09/25/2015    Manjula Chellappan  TMS #20150622-00156 - Shipping Extension modification for PUBD   
   ********************************************************************************/
   PROCEDURE xxwc_order_exception_pre_trig
   IS
      v_cnt   NUMBER;
   BEGIN
      --------------------------------------------------------------------------
      -- Insert into staging table XXEIS.XXWC_GTT_OE_ORDER_HOLDS_ALL
      --------------------------------------------------------------------------
      --------------added by harsha for performance issue TMS#20130920-00347-----------



      INSERT INTO XXEIS.XXWC_GTT_OE_ORDER_HOLDS_ALL VALUE
         (SELECT *
            FROM APPS.OE_ORDER_HOLDS_ALL
           WHERE hold_release_id IS NULL);



      INSERT INTO XXEIS.XXWC_GTT_WSH_SHIPPING_TEMP_STG VALUE
--         (SELECT DISTINCT XXWSS.* -- Commented for Ver 1.15
--Added for Ver 1.15 Begin
           (SELECT DISTINCT 
		          xxwss.header_id,
				  xxwss.line_id,
				  xxwss.delivery_id,
				  xxwss.delivery_detail_id,
				  xxwss.inventory_item_id,
				  xxwss.ship_from_org_id,
				  xxwss.ordered_quantity,
				  xxwss.force_ship_qty,
				  xxwss.transaction_qty,
				  xxwss.lot_number,
				  xxwss.reservation_id,
				  xxwss.status,
				  xxwss.created_by,
				  xxwss.creation_date,
				  xxwss.last_updated_by,
				  xxwss.last_update_date,
				  xxwss.last_update_login,
				  xxwss.request_id,
				  xxwss.org_id
--Added for Ver 1.15 End				  
            FROM XXEIS.XXWC_GTT_OE_ORDER_HOLDS_ALL XXWCOL,
                 xxwc.xxwc_wsh_shipping_stg XXWSS
           WHERE     XXWCOL.HEADER_ID = XXWSS.HEADER_ID
                 AND XXWCOL.LINE_ID = XXWSS.LINE_ID -- AND XXWCOL.INVENTORY_ITEM_ID=XXWSS.INVENTORY_ITEM_ID
                                                   -- AND XXWCOL.ship_from_org_id =XXWSS.ship_from_org_id
         );



      --------------------------------------------------------------------------
      -- Insert into staging table XXEIS.XXWC_GTT_OE_ORDER_HOLDS_ALL
      --------------------------------------------------------------------------

      INSERT INTO XXEIS.XXWC_GTT_PRINT_LOG_TBL VALUE
         (SELECT DISTINCT --1.10 added column names
                       xpl.request_id
                      ,xpl.concurrent_program_id
                      ,xpl.program_application_id
                      ,xpl.concurrent_program_name
                      ,xpl.user_concurrent_program_name
                      ,xpl.printer
                      ,xpl.printer_description
                      ,xpl.requested_by
                      ,xpl.user_name
                      ,xpl.user_description
                      ,xpl.header_id
                      ,xpl.organization_id
                      ,xpl.table_name
                      ,xpl.actual_completion_date
                      ,xpl.phase_code
                      ,xpl.status_code
                      ,xpl.number_of_copies
                      ,xpl.phase
                      ,xpl.status
                      ,xpl.argument1
                      ,xpl.argument2
                      ,xpl.argument3
                      ,xpl.argument4
                      ,xpl.argument5
                      ,xpl.argument6
                      ,xpl.argument7
                      ,xpl.argument8
                      ,xpl.argument9
                      ,xpl.argument10
                      ,xpl.argument11
                      ,xpl.argument12
                      ,xpl.argument13
                      ,xpl.argument14
                      ,xpl.argument15
                      ,xpl.argument16
                      ,xpl.argument17
                      ,xpl.argument18
                      ,xpl.argument19
                      ,xpl.argument20
                      ,xpl.argument21
                      ,xpl.argument22
                      ,xpl.argument23
                      ,xpl.argument24
                      ,xpl.argument25  
            FROM xxwc.xxwc_print_log_tbl xpl,
                 XXEIS.XXWC_GTT_OE_ORDER_HOLDS_ALL oh
           WHERE     xpl.concurrent_program_id IN (70401, 75404) --packing slip and internal packslip
                 AND xpl.program_application_id = 20005
                 --  AND xpl.organization_id =oh.ship_from_org_id
                 AND xpl.header_id = oh.header_id);
   EXCEPTION
      WHEN OTHERS
      THEN
         v_errbuf :=
               'Error_Stack...'
            || CHR (10)
            || DBMS_UTILITY.format_error_stack ()
            || CHR (10)
            || ' Error_Backtrace...'
            || CHR (10)
            || DBMS_UTILITY.format_error_backtrace ();
   END xxwc_order_exception_pre_trig;



   FUNCTION get_doc_date_modified (p_header_id       IN NUMBER,
                                   p_line_id         IN NUMBER,
                                   p_item_id         IN NUMBER,
                                   p_org_id          IN NUMBER,
                                   p_header_org_id   IN NUMBER)
      RETURN DATE
   IS
      l_doc_date   DATE;
   BEGIN
      SELECT MIN (xpl.actual_completion_date)
        INTO l_doc_date
        FROM XXEIS.XXWC_GTT_PRINT_LOG_TBL xpl
       WHERE     1 = 1
             AND xpl.header_id = p_header_id
             AND xpl.organization_id = p_header_org_id
             AND EXISTS
                    (SELECT '1'
                       FROM XXEIS.XXWC_GTT_WSH_SHIPPING_TEMP_STG xws
                      WHERE     1 = 1
                            AND xws.header_id = p_header_id
                            AND xws.line_id = p_line_id
                            AND xws.inventory_item_id = p_item_id
                            AND xws.ship_from_org_id = p_org_id
                            AND xpl.argument10 = TO_CHAR (xws.delivery_id));

      /*
            SELECT MIN (doc_date)
              INTO l_doc_date
              FROM (SELECT xpl.actual_completion_date doc_date
                      FROM XXEIS.XXWC_GTT_PRINT_LOG_TBL xpl, XXEIS.XXWC_GTT_WSH_SHIPPING_TEMP_STG xws
                     WHERE     1 = 1
                           AND xpl.header_id = p_header_id
                           AND xpl.organization_id = p_header_org_id
      --                     AND xpl.concurrent_program_id IN (70401, 75404)                                                                                                                                                                                                                                                                                                        --packing slip and internal packslip
      --                     AND xpl.program_application_id = 20005
                           AND xpl.argument10 = TO_CHAR (xws.delivery_id)
                           AND xpl.header_id = xws.header_id
                           AND xws.inventory_item_id = p_item_id
                           AND xws.ship_from_org_id = p_org_id
                           AND xws.line_id = p_line_id
      --                     AND xpl.request_id = (SELECT MIN (xpl2.request_id)
      --                                             FROM xxwc.xxwc_print_log_tbl xpl2
      --                                            WHERE 1 = 1 AND xpl2.concurrent_program_id IN (70401, 75404) AND xpl2.program_application_id = 20005 AND xpl2.header_id = xpl.header_id)
      --              UNION
      */
      --              SELECT /*+parallel(auto)*/ fcr.actual_completion_date
      /*                FROM fnd_concurrent_requests fcr
                          ,XXEIS.XXWC_GTT_WSH_SHIPPING_TEMP_STG xws
                          ,fnd_user fu
                          ,fnd_concurrent_programs_vl fcp
                     WHERE     1 = 1
                           AND fcr.argument2 = TO_CHAR (p_header_id)
                           AND fcr.argument1 = TO_CHAR (p_header_org_id)
                           AND fcr.concurrent_program_id = fcp.concurrent_program_id
                           AND fcr.requested_by = fu.user_id
                           AND fcr.program_application_id = 20005
                           AND fcr.concurrent_program_id IN (70401, 75404)                                                                                                                                                                                                                                                                                                        --packing slip and internal packslip
                           AND fcr.argument10 = TO_CHAR (xws.delivery_id)
                           AND fcr.argument2 = TO_CHAR (xws.header_id)
                           AND xws.inventory_item_id = p_item_id
                           AND xws.ship_from_org_id = p_org_id
                           AND xws.line_id = p_line_id
                           AND fcr.request_id = (SELECT MIN (fcr1.request_id)
                                                   FROM fnd_concurrent_requests fcr1
                                                  WHERE 1 = 1 AND fcr1.concurrent_program_id IN (70401, 75404) AND fcr1.program_application_id = 20005 AND fcr1.argument2 = fcr.argument2 AND fcr1.argument10 = fcr.argument10)
      */
      --);

      RETURN l_doc_date;
   EXCEPTION
      WHEN OTHERS
      THEN
         v_errbuf :=
               'Error_Stack...'
            || CHR (10)
            || DBMS_UTILITY.format_error_stack ()
            || CHR (10)
            || ' Error_Backtrace...'
            || CHR (10)
            || DBMS_UTILITY.format_error_backtrace ();
   END;

   -- Version# 1.2 < End

   -- Version# 1.3 > Start
   /********************************************************************************
   ProcedureName : xxwc_open_sales_order_pre_trig
   Purpose       : Used by "White Cap Sales Order Report"

   HISTORY
   ===============================================================================
   VERSION DATE          AUTHOR(S)       DESCRIPTION
   ------- -----------   --------------- -----------------------------------------
   1.0     03/05/2014    Harsha Yedla   Initial Creation
   ********************************************************************************/

   -- Version# 1.4 > Start
   --   PROCEDURE xxwc_open_sales_order_pre_trig(p_org_id varchar2)
   --   IS
   --      v_cnt        NUMBER;
   --      l_doc_date   DATE;
   --      V_ORG_ID     varchar2(2000);
   --      v_val        number;

   --      CURSOR C1
   --      IS
   --   SELECT A.ROWID RID, A.*
   --     FROM XXEIS.XXWC_GTT_OPEN_ORDERS_TMP A;
   --   BEGIN
   --      DBMS_OUTPUT.put_line ('before inserts xxwc_open_sales_order_pre_trig');

   --      --------------------------------------------------------------------------
   --      -- Insert into staging table XXEIS.XXWC_GTT_OE_ORDER_HEADERS
   --      --------------------------------------------------------------------------
   --      --------------added by harsha for performance issue TMS#20130920-00347 -----------

   --       begin

   --        INSERT INTO XXEIS.XXWC_GTT_OE_ORDER_HEADERS (HEADER_ID
   --      ,ORG_ID
   --      ,ORDER_TYPE_ID
   --      ,ORDER_NUMBER
   --      ,ORDERED_DATE
   --      ,REQUEST_DATE
   --      ,CREATION_DATE
   --      ,CREATED_BY
   --      ,CREATED_BY_NAME
   --      ,QUOTE_NUMBER
   --      ,SOLD_TO_ORG_ID
   --      ,PARTY_ID
   --      ,CUST_ACCOUNT_ID
   --      ,CUSTOMER_NUMBER
   --      ,CUSTOMER_NAME
   --      ,CUSTOMER_JOB_NAME
   --      ,CUST_ACCT_SITE_ID
   --      ,SHIP_TO_CITY
   --      ,SHIP_TO_STE_ID
   --      ,SHP_TO_PRT_ID
   --      ,ZIP_CODE
   --      ,FLOW_STATUS_CODE
   --      ,TRANSACTION_PHASE_CODE
   --      ,PAYMENT_TERM_ID
   --      ,PAYMENT_TERMS
   --      ,SALESREP_ID
   --      ,ORG_SALESREP_ID
   --      ,SALES_PERSON_NAME
   --      ,SHIP_FROM_ORG_ID
   --      ,SHIP_TO_ORG_ID)
   --   (SELECT /*+rule*/
   --    Oh.Header_Id,
   --     OH.ORG_ID,
   --     OH.ORDER_TYPE_ID,
   --     OH.ORDER_NUMBER,
   --     OH.ORDERED_DATE,
   --     OH.REQUEST_DATE,
   --     oh.creation_date,
   --     oh.CREATED_BY,
   --     (SELECT ppf.full_name
   --        FROM per_all_people_f ppf, fnd_user fu
   --       WHERE     1 = 1
   --       AND ppf.person_id = fu.employee_id
   --       AND user_id = oh.CREATED_BY
   --       AND (TRUNC (oh.creation_date) BETWEEN TRUNC (
   --                  ppf.effective_start_date)
   --                 AND NVL (
   --                  ppf.effective_end_date,
   --                  oh.creation_date))) CREATED_BY_NAME,
   --     OH.QUOTE_NUMBER,
   --     OH.SOLD_TO_ORG_ID,
   --     (SELECT HZP.PARTY_ID
   --        FROM HZ_CUST_ACCOUNTS HCA, HZ_PARTIES HZP
   --       WHERE     oh.sold_to_org_id = hca.cust_account_id
   --       AND HCA.PARTY_ID = HZP.PARTY_ID),
   --     (SELECT hca.cust_account_id
   --        FROM HZ_CUST_ACCOUNTS HCA
   --       WHERE oh.sold_to_org_id = hca.cust_account_id),
   --     (SELECT HCA.ACCOUNT_NUMBER
   --        FROM HZ_CUST_ACCOUNTS HCA
   --       WHERE oh.sold_to_org_id = hca.cust_account_id),
   --     (SELECT NVL (HCA.ACCOUNT_NAME, hzp.party_name)
   --        FROM HZ_CUST_ACCOUNTS HCA, HZ_PARTIES HZP
   --       WHERE     oh.sold_to_org_id = hca.cust_account_id
   --       AND HCA.PARTY_ID = HZP.PARTY_ID),
   --     (SELECT HZCS_SHIP_TO.LOCATION
   --        FROM HZ_CUST_SITE_USES_ALL HZCS_SHIP_TO,
   --       HZ_CUST_ACCT_SITES HCAS_SHIP_TO,
   --       HZ_PARTY_SITES HZPS_SHIP_TO,
   --       HZ_LOCATIONS HZL_SHIP_TO
   --       WHERE     oh.ship_to_org_id = hzcs_ship_to.site_use_id
   --       AND hzcs_ship_to.cust_acct_site_id =
   --        hcas_ship_to.cust_acct_site_id
   --       AND hcas_ship_to.party_site_id =
   --        hzps_ship_to.party_site_id
   --       AND hzl_ship_to.location_id =
   --        hzps_ship_to.location_id),
   --     (SELECT Hcas_Ship_To.Cust_Acct_Site_Id
   --        FROM HZ_CUST_SITE_USES_ALL HZCS_SHIP_TO,
   --       HZ_CUST_ACCT_SITES HCAS_SHIP_TO,
   --       HZ_PARTY_SITES HZPS_SHIP_TO,
   --       HZ_LOCATIONS HZL_SHIP_TO
   --       WHERE     oh.ship_to_org_id = hzcs_ship_to.site_use_id
   --       AND hzcs_ship_to.cust_acct_site_id =
   --        hcas_ship_to.cust_acct_site_id
   --       AND hcas_ship_to.party_site_id =
   --        hzps_ship_to.party_site_id
   --       AND hzl_ship_to.location_id =
   --        hzps_ship_to.location_id),
   --     (SELECT hzl_ship_to.city
   --        FROM HZ_CUST_SITE_USES_ALL HZCS_SHIP_TO,
   --       HZ_CUST_ACCT_SITES HCAS_SHIP_TO,
   --       HZ_PARTY_SITES HZPS_SHIP_TO,
   --       HZ_LOCATIONS HZL_SHIP_TO
   --       WHERE     oh.ship_to_org_id = hzcs_ship_to.site_use_id
   --       AND hzcs_ship_to.cust_acct_site_id =
   --        hcas_ship_to.cust_acct_site_id
   --       AND hcas_ship_to.party_site_id =
   --        hzps_ship_to.party_site_id
   --       AND hzl_ship_to.location_id =
   --        hzps_ship_to.location_id),
   --     (SELECT Hzcs_Ship_To.Site_Use_Id
   --        FROM HZ_CUST_SITE_USES_ALL HZCS_SHIP_TO,
   --       HZ_CUST_ACCT_SITES HCAS_SHIP_TO,
   --       HZ_PARTY_SITES HZPS_SHIP_TO,
   --       HZ_LOCATIONS HZL_SHIP_TO
   --       WHERE     oh.ship_to_org_id = hzcs_ship_to.site_use_id
   --       AND hzcs_ship_to.cust_acct_site_id =
   --        hcas_ship_to.cust_acct_site_id
   --       AND hcas_ship_to.party_site_id =
   --        hzps_ship_to.party_site_id
   --       AND hzl_ship_to.location_id =
   --        hzps_ship_to.location_id),
   --     (SELECT HZPS_SHIP_TO.PARTY_SITE_ID
   --        FROM HZ_CUST_SITE_USES_ALL HZCS_SHIP_TO,
   --       HZ_CUST_ACCT_SITES HCAS_SHIP_TO,
   --       HZ_PARTY_SITES HZPS_SHIP_TO,
   --       HZ_LOCATIONS HZL_SHIP_TO
   --       WHERE     oh.ship_to_org_id = hzcs_ship_to.site_use_id
   --       AND hzcs_ship_to.cust_acct_site_id =
   --        hcas_ship_to.cust_acct_site_id
   --       AND hcas_ship_to.party_site_id =
   --        hzps_ship_to.party_site_id
   --       AND hzl_ship_to.location_id =
   --        hzps_ship_to.location_id),
   --     (SELECT hzl_ship_to.postal_code
   --        FROM HZ_CUST_SITE_USES_ALL HZCS_SHIP_TO,
   --       HZ_CUST_ACCT_SITES HCAS_SHIP_TO,
   --       HZ_PARTY_SITES HZPS_SHIP_TO,
   --       HZ_LOCATIONS HZL_SHIP_TO
   --       WHERE     oh.ship_to_org_id = hzcs_ship_to.site_use_id
   --       AND hzcs_ship_to.cust_acct_site_id =
   --        hcas_ship_to.cust_acct_site_id
   --       AND hcas_ship_to.party_site_id =
   --        hzps_ship_to.party_site_id
   --       AND hzl_ship_to.location_id =
   --        hzps_ship_to.location_id),
   --     OH.FLOW_STATUS_CODE,
   --     OH.TRANSACTION_PHASE_CODE,
   --     OH.PAYMENT_TERM_ID,
   --     (SELECT /*+index(RAB RA_TERMS_B_U1)*/
   --       NAME
   --        FROM apps.RA_TERMS_B RAB, apps.RA_TERMS_TL RAT
   --       WHERE     RAB.TERM_ID = RAT.TERM_ID
   --       AND RAB.TERM_ID = oh.payment_term_id),
   --     OH.SALESREP_ID,
   --     (SELECT Rep.Salesrep_Id
   --        FROM ra_salesreps REP -- Version# 1.5
   --       WHERE     OH.SALESREP_ID = REP.SALESREP_ID
   --       AND OH.ORG_ID = REP.ORG_ID),
   --     (SELECT REP.name
   --        FROM ra_salesreps REP -- Version# 1.5
   --       WHERE     OH.SALESREP_ID = REP.SALESREP_ID
   --       AND OH.ORG_ID = REP.ORG_ID),
   --     OH.SHIP_FROM_ORG_ID,
   --     OH.SHIP_TO_ORG_ID
   --      FROM OE_ORDER_HEADERS_ALL OH
   --     WHERE OH.FLOW_STATUS_CODE IN ('BOOKED',
   --           'DRAFT',
   --           'ENTERED',
   --           'PENDING_CUSTOMER_ACCEPTANCE'));


   --exception
   --when others then
   -- FND_FILE.put_line (FND_FILE.LOG,'In others of xxwc_open_sales_order_pre_trig for headers' || SQLERRM);
   --end;


   --  select instr(p_org_id,',') into v_val from dual;


   --    if p_org_id !='All' THEN

   --  if v_val!=0 then

   --   v_org_id:=p_org_id;
   --       else
   --    v_org_id:= ''''||p_org_id||'''';
   --    end if;

   --      EXECUTE IMMEDIATE 'insert into XXEIS.XXWC_GTT_ORGANIZATIONS_TEMP value (SELECT organization_id  FROM MTL_PARAMETERS  WHERE organization_code IN ('||v_org_id||'))';

   --       else
   --         insert into XXEIS.XXWC_GTT_ORGANIZATIONS_TEMP value (SELECT organization_id  FROM MTL_PARAMETERS  WHERE 1=1);
   --        end if;

   --      --------------------------------------------------------------------------
   --      -- Insert into staging table XXEIS.XXWC_GTT_OE_ORDER_LINES
   --      --------------------------------------------------------------------------
   --begin

   --        INSERT INTO XXEIS.XXWC_GTT_OE_ORDER_LINES  (LINE_ID
   --      ,HEADER_ID
   --      ,SOLD_FROM_ORG_ID
   --      ,SHIP_FROM_ORG_ID
   --      ,WAREHOUSE
   --      ,SHIPPING_METHOD_CODE
   --      ,ORDERED_QUANTITY
   --      ,CREATION_DATE
   --      ,FLOW_STATUS_CODE
   --      ,UNIT_SELLING_PRICE
   --      ,LINE_CATEGORY_CODE
   --      ,SCHEDULE_SHIP_DATE
   --      ,INVENTORY_ITEM_ID
   --      ,ITEM_NUMBER
   --      ,USER_ITEM_DESCRIPTION)
   --   (SELECT /*+rule*/
   --    OL.LINE_ID,
   --     OL.HEADER_ID,
   --     OL.SOLD_FROM_ORG_ID,
   --     ol.ship_from_org_id,
   --     (SELECT mtp.organization_code
   --        FROM mtl_parameters mtp
   --       WHERE mtp.organization_id = ol.ship_from_org_id),
   --     OL.SHIPPING_METHOD_CODE,
   --     OL.ORDERED_QUANTITY,
   --     OL.CREATION_DATE,
   --     OL.FLOW_STATUS_CODE,
   --     OL.UNIT_SELLING_PRICE,
   --     OL.LINE_CATEGORY_CODE,
   --     OL.SCHEDULE_SHIP_DATE,
   --     OL.INVENTORY_ITEM_ID,
   --     (SELECT MSI.SEGMENT1
   --        FROM MTL_SYSTEM_ITEMS_B MSI
   --       WHERE     MSI.ORGANIZATION_ID = OL.SHIP_FROM_ORG_ID
   --       AND MSI.INVENTORY_ITEM_ID = OL.INVENTORY_ITEM_ID),
   --     (SELECT MSI.DESCRIPTION
   --        FROM MTL_SYSTEM_ITEMS_B MSI
   --       WHERE     MSI.ORGANIZATION_ID = OL.SHIP_FROM_ORG_ID
   --       AND MSI.INVENTORY_ITEM_ID = OL.INVENTORY_ITEM_ID)
   --      FROM OE_ORDER_LINES_ALL OL, XXEIS.XXWC_GTT_OE_ORDER_HEADERS OH
   --     WHERE     OL.HEADER_ID = OH.HEADER_ID
   --     AND exists (SELECT 1 FROM MTL_PARAMETERS mp,XXEIS.XXWC_GTT_ORGANIZATIONS_TEMP a  WHERE ol.ship_from_org_id =mp.organization_id and mp.organization_id=a.ORGANIZATION_ID )
   --     AND OL.FLOW_STATUS_CODE NOT IN ('CANCELLED', 'CLOSED'));

   --exception
   --when others then
   -- FND_FILE.put_line (FND_FILE.LOG,
   --      'In others of xxwc_open_sales_order_pre_trig for lines' || SQLERRM);
   --end;



   --      --------------------------------------------------------------------------
   --      -- Insert into staging table XXEIS.XXWC_GTT_WSH_SHIPPING_TEMP_STG
   --      --------------------------------------------------------------------------
   --    begin

   --        INSERT INTO XXEIS.XXWC_GTT_WSH_SHIPPING_TEMP_STG (
   --  HEADER_ID
   --  ,LINE_ID
   --  ,DELIVERY_ID
   --  ,DELIVERY_DETAIL_ID
   --  ,INVENTORY_ITEM_ID
   --  ,SHIP_FROM_ORG_ID
   --  ,ORDERED_QUANTITY
   --  ,FORCE_SHIP_QTY
   --  ,TRANSACTION_QTY
   --  ,LOT_NUMBER
   --  ,RESERVATION_ID
   --  ,STATUS
   --  ,CREATED_BY
   --  ,CREATION_DATE
   --  ,LAST_UPDATED_BY
   --  ,LAST_UPDATE_DATE
   --  ,LAST_UPDATE_LOGIN
   --  ,REQUEST_ID)
   --     (SELECT /*+rule*/
   --       XXWSS.HEADER_ID
   --  ,XXWSS.LINE_ID
   --  ,XXWSS.DELIVERY_ID
   --  ,XXWSS.DELIVERY_DETAIL_ID
   --  ,XXWSS.INVENTORY_ITEM_ID
   --  ,XXWSS.SHIP_FROM_ORG_ID
   --  ,XXWSS.ORDERED_QUANTITY
   --  ,XXWSS.FORCE_SHIP_QTY
   --  ,XXWSS.TRANSACTION_QTY
   --  ,XXWSS.LOT_NUMBER
   --  ,XXWSS.RESERVATION_ID
   --  ,XXWSS.STATUS
   --  ,XXWSS.CREATED_BY
   --  ,XXWSS.CREATION_DATE
   --  ,XXWSS.LAST_UPDATED_BY
   --  ,XXWSS.LAST_UPDATE_DATE
   --  ,XXWSS.LAST_UPDATE_LOGIN
   --  ,XXWSS.REQUEST_ID
   --      FROM xxeis.XXWC_GTT_OE_ORDER_LINES XXWCOL,
   --     xxwc.xxwc_wsh_shipping_stg XXWSS
   --     WHERE     XXWCOL.HEADER_ID = XXWSS.HEADER_ID
   --     AND XXWCOL.LINE_ID = XXWSS.LINE_ID
   --     AND XXWCOL.INVENTORY_ITEM_ID = XXWSS.INVENTORY_ITEM_ID
   --     AND XXWCOL.ship_from_org_id = XXWSS.ship_from_org_id);
   --    exception
   --when others then
   -- FND_FILE.put_line (FND_FILE.LOG,
   --      'In others of xxwc_open_sales_order_pre_trig for SHIPPING_TEMP_STG' || SQLERRM);
   -- end;


   --      --------------------------------------------------------------------------
   --      -- Insert into staging table XXEIS.XXWC_GTT_PRINT_LOG_TBL
   --      --------------------------------------------------------------------------
   -- begin

   --        INSERT INTO XXEIS.XXWC_GTT_PRINT_LOG_TBL (REQUEST_ID
   --      ,CONCURRENT_PROGRAM_ID
   --      ,PROGRAM_APPLICATION_ID
   --      ,CONCURRENT_PROGRAM_NAME
   --      ,USER_CONCURRENT_PROGRAM_NAME
   --      ,PRINTER
   --      ,PRINTER_DESCRIPTION
   --      ,REQUESTED_BY
   --      ,USER_NAME
   --      ,USER_DESCRIPTION
   --      ,HEADER_ID
   --      ,ORGANIZATION_ID
   --      ,TABLE_NAME
   --      ,ACTUAL_COMPLETION_DATE
   --      ,PHASE_CODE
   --      ,STATUS_CODE
   --      ,NUMBER_OF_COPIES
   --      ,PHASE
   --      ,STATUS
   --      ,ARGUMENT1
   --      ,ARGUMENT2
   --      ,ARGUMENT3
   --      ,ARGUMENT4
   --      ,ARGUMENT5
   --      ,ARGUMENT6
   --      ,ARGUMENT7
   --      ,ARGUMENT8
   --      ,ARGUMENT9
   --      ,ARGUMENT10
   --      ,ARGUMENT11
   --      ,ARGUMENT12
   --      ,ARGUMENT13
   --      ,ARGUMENT14
   --      ,ARGUMENT15
   --      ,ARGUMENT16
   --      ,ARGUMENT17
   --      ,ARGUMENT18
   --      ,ARGUMENT19
   --      ,ARGUMENT20
   --      ,ARGUMENT21
   --      ,ARGUMENT22
   --      ,ARGUMENT23
   --      ,ARGUMENT24
   --      ,ARGUMENT25)
   --     (SELECT /*+rule*/
   --       distinct xpl.REQUEST_ID
   --      ,xpl.CONCURRENT_PROGRAM_ID
   --      ,xpl.PROGRAM_APPLICATION_ID
   --      ,xpl.CONCURRENT_PROGRAM_NAME
   --      ,xpl.USER_CONCURRENT_PROGRAM_NAME
   --      ,xpl.PRINTER
   --      ,xpl.PRINTER_DESCRIPTION
   --      ,xpl.REQUESTED_BY
   --      ,xpl.USER_NAME
   --      ,xpl.USER_DESCRIPTION
   --      ,xpl.HEADER_ID
   --      ,xpl.ORGANIZATION_ID
   --      ,xpl.TABLE_NAME
   --      ,xpl.ACTUAL_COMPLETION_DATE
   --      ,xpl.PHASE_CODE
   --      ,xpl.STATUS_CODE
   --      ,xpl.NUMBER_OF_COPIES
   --      ,xpl.PHASE
   --      ,xpl.STATUS
   --      ,xpl.ARGUMENT1
   --      ,xpl.ARGUMENT2
   --      ,xpl.ARGUMENT3
   --      ,xpl.ARGUMENT4
   --      ,xpl.ARGUMENT5
   --      ,xpl.ARGUMENT6
   --      ,xpl.ARGUMENT7
   --      ,xpl.ARGUMENT8
   --      ,xpl.ARGUMENT9
   --      ,xpl.ARGUMENT10
   --      ,xpl.ARGUMENT11
   --      ,xpl.ARGUMENT12
   --      ,xpl.ARGUMENT13
   --      ,xpl.ARGUMENT14
   --      ,xpl.ARGUMENT15
   --      ,xpl.ARGUMENT16
   --      ,xpl.ARGUMENT17
   --      ,xpl.ARGUMENT18
   --      ,xpl.ARGUMENT19
   --      ,xpl.ARGUMENT20
   --      ,xpl.ARGUMENT21
   --      ,xpl.ARGUMENT22
   --      ,xpl.ARGUMENT23
   --      ,xpl.ARGUMENT24
   --      ,xpl.ARGUMENT25
   --      FROM xxwc.xxwc_print_log_tbl xpl,
   --     XXEIS.XXWC_GTT_OE_ORDER_HEADERS oh
   --     WHERE     xpl.concurrent_program_id IN (70401, 75404) --packing slip and internal packslip
   --     AND xpl.program_application_id = 20005
   --     AND xpl.organization_id = oh.ship_from_org_id
   --     AND xpl.header_id = oh.header_id);
   --exception
   --when others then
   --FND_FILE.put_line (FND_FILE.LOG,'In others of xxwc_open_sales_order_pre_trig for log' || SQLERRM);
   -- end;

   --      --------------------------------------------------------------------------
   --      -- Insert into staging table XXEIS.XXWC_GTT_OPEN_ORDERS_TMP
   --      --------------------------------------------------------------------------
   --begin

   --      INSERT INTO XXEIS.XXWC_GTT_OPEN_ORDERS_TMP (WAREHOUSE
   --    ,SALES_PERSON_NAME
   --    ,CREATED_BY
   --    ,ORDER_NUMBER
   --    ,ORDERED_DATE
   --    ,LINE_CREATION_DATE
   --    ,ORDER_TYPE
   --    ,QUOTE_NUMBER
   --    ,ORDER_LINE_STATUS
   --    ,ORDER_HEADER_STATUS
   --    ,CUSTOMER_NUMBER
   --    ,CUSTOMER_NAME
   --    ,CUSTOMER_JOB_NAME
   --    ,ORDER_AMOUNT
   --    ,SHIPPING_METHOD
   --    ,SHIP_TO_CITY
   --    ,ZIP_CODE
   --    ,SCHEDULE_SHIP_DATE
   --    ,HEADER_STATUS
   --    ,TRANSACTION_PHASE_CODE
   --    ,ITEM_NUMBER
   --    ,ITEM_DESCRIPTION
   --    ,QTY
   --    ,PAYMENT_TERMS
   --    ,REQUEST_DATE
   --    ,ORDER_HEADER_ID
   --    ,ORDER_LINE_ID
   --    ,CUST_ACCOUNT_ID
   --    ,CUST_ACCT_SITE_ID
   --    ,SALESREP_ID
   --    ,PARTY_ID
   --    ,TRANSACTION_TYPE_ID
   --    ,MTP_ORGANIZATION_ID
   --    ,HZCS_SHIP_TO_STE_ID
   --    ,HZPS_SHP_TO_PRT_ID
   --    ,USER_ITEM_DESCRIPTION
   --    ,INVENTORY_ITEM_ID
   --    ,HDR_SHIP_FROM_ORG_ID
   --    ,LIN_SHIP_FROM_ORG_ID
   --    ,HEADER_ID
   --    ,LINE_ID
   --    ,DELIVERY_DOC_DATE)
   --   (SELECT /*+rule*/
   --    OL.warehouse,
   --  --(SELECT organization_code FROM MTL_PARAMETERS  WHERE organization_id=OL.warehouse and rownum<2 ) warehouse,
   --     OH.SALES_PERSON_NAME,
   --     OH.CREATED_BY_NAME,
   --     OH.ORDER_NUMBER,
   --     TRUNC (OH.ORDERED_DATE),
   --     TRUNC (ol.creation_date),
   --     OTT.NAME,
   --     OH.QUOTE_NUMBER,
   --     apps.OE_LINE_STATUS_PUB.get_line_status (
   --        ol.line_id,
   --        ol.flow_status_code),
   --     flv_order_status.meaning,
   --     OH.CUSTOMER_NUMBER,
   --     OH.customer_name,
   --     OH.CUSTOMER_JOB_NAME,
   --     ROUND (
   --        (  OL.UNIT_SELLING_PRICE
   --         * DECODE (OL.LINE_CATEGORY_CODE,
   --             'RETURN', OL.ORDERED_QUANTITY * -1,
   --             OL.ORDERED_QUANTITY)),
   --        2),
   --     flv_ship_mtd.meaning,
   --     OH.ship_to_city,
   --     OH.zip_code,
   --     OL.SCHEDULE_SHIP_DATE,
   --     OH.FLOW_STATUS_CODE,
   --     OH.TRANSACTION_PHASE_CODE,
   --     OL.Item_number,
   --     OL.USER_ITEM_DESCRIPTION,
   --     DECODE (ol.line_category_code,
   --       'RETURN', (NVL (Ol.ordered_quantity, 0) * -1),
   --       NVL (Ol.ordered_quantity, 0)),
   --     OH.Payment_terms,
   --     oh.request_date,
   --     Oh.Header_Id,
   --     Ol.Line_Id,
   --     OH.cust_account_id,
   --     OH.Cust_Acct_Site_Id,
   --     OH.ORG_SALESREP_ID,
   --     OH.PARTY_ID,
   --     ott.transaction_type_id,
   --     (SELECT Mtp.Organization_Id
   --        FROM mtl_parameters mtp
   --       WHERE mtp.organization_id = ol.ship_from_org_id),
   --     OH.SHIP_TO_STE_ID,
   --     OH.SHP_TO_PRT_ID,
   --     OL.USER_ITEM_DESCRIPTION,
   --     ol.INVENTORY_ITEM_ID,
   --     oh.ship_from_org_id,
   --     ol.ship_from_org_id,
   --     oh.header_id,
   --     ol.line_id,
   --     NULL
   --      -- XXEIS.eis_rs_process_reports.GET_DOC_DATE (
   --      --    oh.header_id,
   --      --    ol.line_id,
   --      --    ol.INVENTORY_ITEM_ID,
   --      ----    ol.ship_from_org_id,
   --      --    oh.ship_from_org_id)
   --      FROM XXEIS.XXWC_GTT_OE_ORDER_HEADERS OH,
   --     XXEIS.XXWC_GTT_OE_ORDER_LINES ol,
   --     OE_TRANSACTION_TYPES_VL OTT,
   --     FND_LOOKUP_VALUES_VL FLV_SHIP_MTD,
   --     OE_LOOKUPS FLV_ORDER_STATUS
   --     WHERE     ol.header_id = oh.header_id
   --     AND OH.ORDER_TYPE_ID = OTT.TRANSACTION_TYPE_ID
   --     AND OL.FLOW_STATUS_CODE NOT IN ('CLOSED', 'CANCELLED')
   --     AND FLV_SHIP_MTD.LOOKUP_TYPE(+) = 'SHIP_METHOD'
   --     AND FLV_SHIP_MTD.LOOKUP_CODE(+) = OL.SHIPPING_METHOD_CODE
   --     AND flv_order_status.lookup_type = 'FLOW_STATUS'
   --     AND flv_order_status.lookup_code = Oh.flow_status_code
   --     AND ott.name IN ('STANDARD ORDER',
   --          'COUNTER ORDER',
   --          'RETURN ORDER',
   --          'REPAIR ORDER')
   --     AND (   (    ol.FLOW_STATUS_CODE = 'ENTERED'
   --        AND TRUNC (oh.creation_date) < TRUNC (SYSDATE))
   --          OR OL.FLOW_STATUS_CODE <> 'ENTERED'));

   --     exception
   --   when others then
   --   FND_FILE.put_line (FND_FILE.LOG,'In others of xxwc_open_sales_order_pre_trig for tmp' || SQLERRM);
   --   end;



   --      FOR i IN c1
   --      LOOP

   --    begin
   --   SELECT MIN (xpl.actual_completion_date)
   --     INTO l_doc_date
   --     FROM XXEIS.XXWC_GTT_PRINT_LOG_TBL xpl
   --    WHERE     1 = 1
   --    AND xpl.header_id = i.header_id
   --    AND xpl.organization_id = i.HDR_SHIP_FROM_ORG_ID
   --    AND EXISTS
   --           (SELECT distinct '1'
   --        FROM XXEIS.XXWC_GTT_WSH_SHIPPING_TEMP_STG xws
   --       WHERE     1 = 1
   --             AND xws.header_id = i.header_id
   --             AND xws.line_id = i.line_id
   --           --AND xws.inventory_item_id = i.ITEM_NUMBER
   --             AND xws.inventory_item_id = i.inventory_item_id  --TMS#20140317-00281 Error in Open Sales Orders Report
   --             AND xws.ship_from_org_id =
   --              i.LIN_SHIP_FROM_ORG_ID
   --             AND xpl.argument10 = TO_CHAR (xws.delivery_id));
   --      exception
   --when others then
   -- FND_FILE.put_line (FND_FILE.LOG,'In others of xxwc_open_sales_order_pre_trig xpl.actual_completion_date i.header_id...i.line_id...i.ITEM_NUMBER...i.LIN_SHIP_FROM_ORG_ID...i.inventory_item_id..' || SQLERRM||i.header_id||'...'||i.line_id||'...'||i.ITEM_NUMBER||'...'||i.LIN_SHIP_FROM_ORG_ID||i.inventory_item_id);
   -- end;



   --   UPDATE XXEIS.XXWC_GTT_OPEN_ORDERS_TMP
   --      SET DELIVERY_DOC_DATE = l_doc_date
   --    WHERE ROWID = i.rid;
   --      END LOOP;

   --      COMMIT;
   --   EXCEPTION
   --      WHEN OTHERS
   --      THEN


   --  v_errbuf :=
   --         'Error_Stack...'
   --      || CHR (10)
   --      || DBMS_UTILITY.format_error_stack ()
   --      || CHR (10)
   --      || ' Error_Backtrace...'
   --      || CHR (10)
   --      || DBMS_UTILITY.format_error_backtrace ();
   --     END xxwc_open_sales_order_pre_trig;
   -- Version# 1.3 < End
   -- Version# 1.4 < End

   -- Version# 1.7 > Start
   FUNCTION get_item_acc (p_ccid IN NUMBER)
      RETURN VARCHAR2
   AS
      l_account   VARCHAR2 (181);
   BEGIN
      SELECT concatenated_segments
        INTO l_account
        FROM gl_code_combinations_kfv gcc
       WHERE code_combination_id = p_ccid;

      RETURN l_account;
   EXCEPTION
      WHEN OTHERS
      THEN
         RETURN NULL;
   END GET_ITEM_ACC;

   -- Version# 1.7 > end

   -- Version# 1.9 > start
   FUNCTION get_item_onhand_qty (p_inventory_item_id    NUMBER,
                                 p_organization_id      NUMBER)
      RETURN NUMBER
   IS
      l_item_onhand_qty   NUMBER;
   BEGIN
      SELECT NVL (SUM (moq.transaction_quantity), 0)
        INTO l_item_onhand_qty
        FROM mtl_onhand_quantities_detail moq
       WHERE     inventory_item_id = p_inventory_item_id
             AND organization_id = p_organization_id;

      RETURN l_item_onhand_qty;
   EXCEPTION
      WHEN OTHERS
      THEN
         RETURN 0;
   END;
-- Version# 1.9 > end
-- Version# 1.12 > start
FUNCTION xxwc_inv_get_born_date (
  p_inventory_item_id       IN NUMBER,
   p_organization_id         IN NUMBER,
   p_create_transaction_id   IN NUMBER,
   p_init_flag               IN NUMBER)
   RETURN DATE
AS
   l_oldest_born_date           DATE;
   l_transaction_id             NUMBER;
   l_transfer_organization_id   NUMBER;
BEGIN
   IF p_init_flag = 1
   THEN
      SELECT transaction_id, transfer_organization_id, transaction_date
        INTO l_transaction_id, l_transfer_organization_id, l_oldest_born_date
        FROM mtl_material_transactions mmt
       WHERE transaction_id = p_create_transaction_id;

      IF NVL (l_transfer_organization_id, p_organization_id) =
            p_organization_id
      THEN
         SELECT transaction_id, transfer_organization_id, transaction_date
           INTO l_transaction_id,
                l_transfer_organization_id,
                l_oldest_born_date
           FROM mtl_material_transactions mmt
          WHERE transaction_id =
                   (SELECT MAX (transaction_id)
                      FROM mtl_material_transactions
                     WHERE     inventory_item_id = p_inventory_item_id
                           AND organization_id = p_organization_id
                           AND source_code = 'RCV'
                           AND transaction_id < p_create_transaction_id);
      END IF;
   ELSE
    SELECT transaction_id, transfer_organization_id, transaction_date
        INTO l_transaction_id, l_transfer_organization_id, l_oldest_born_date
        FROM mtl_material_transactions mmt
       WHERE transaction_id =
                (SELECT MAX (transaction_id)
                   FROM mtl_material_transactions
                  WHERE     inventory_item_id = p_inventory_item_id
                        AND organization_id = p_organization_id
                        AND source_code = 'RCV'
                        AND transaction_id < p_create_transaction_id);
   END IF;

--RETURN (l_oldest_born_date);

   IF NVL (l_transfer_organization_id, p_organization_id) = p_organization_id
   THEN
      IF p_init_flag = 1
      THEN
         RETURN (l_oldest_born_date);
      ELSE
         RETURN (l_oldest_born_date);
      END IF;
   ELSE
      RETURN (xxwc_inv_get_born_date (p_inventory_item_id,
                                      l_transfer_organization_id,
                                      l_transaction_id,
                                      0));
   END IF;
EXCEPTION
   WHEN OTHERS
   THEN
      RETURN (l_oldest_born_date);
END;
-- Version# 1.12 > End
END EIS_RS_XXWC_COM_UTIL_PKG;
/