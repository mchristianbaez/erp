/*************************************************************************
  $Header ALTER_XXWC_WSH_SHIPPING_STG.sql $
  Module Name: ALTER_XXWC_WSH_SHIPPING_STG.sql

  PURPOSE:   To Add the column subinventory

  REVISIONS:
  Ver        Date        Author                     Description
  ---------  ----------  ---------------         -------------------------
  1.0        03/09/2016  Manjula Chellappan      TMS 20150622-00156 - Shipping Extension modification for PUBD 
**************************************************************************/
ALTER TABLE XXWC.XXWC_WSH_SHIPPING_STG ADD (subinventory VARCHAR2(10));