/*************************************************************************************************************************************************************
   -- File Name:  Category_Assignment_Delete.sql
   --
   -- PROGRAM TYPE: Table
   --
   -- PURPOSE:  To Delete item category assignment
   -- HISTORY
   -- ===========================================================================================================================================================
   -- ===========================================================================================================================================================
   -- VERSION DATE          AUTHOR(S)       DESCRIPTION
   -- ------- -----------   --------------- ---------------------------------------------------------------------------------------------------------------------
   -- 1.0     09-NOV-2015   P.Vamshidhar    TMS#20151029-00071

  ***************************************************************************************************************************************************************/
set define off;
CREATE TABLE XXWC.XXWC_MTL_ITEM_CATE_BKP
(
  INVENTORY_ITEM_ID  NUMBER                     NOT NULL,
  ORGANIZATION_ID    NUMBER                     NOT NULL,
  CATEGORY_SET_ID    NUMBER                     NOT NULL,
  CATEGORY_ID        NUMBER                     NOT NULL,
  CREATION_DATE      DATE                       NOT NULL,
  CREATED_BY         NUMBER                     NOT NULL,
  PROCESS_STATUS     VARCHAR2(20 BYTE),
  ERROR_MESS         VARCHAR2(2000 BYTE)
)
/
set define off;
DECLARE
   CURSOR C1
   IS
SELECT mic.inventory_item_id,
       mic.organization_id,
       mic.category_set_id,
       mic.category_id
  FROM apps.mtl_item_categories_v mic,
       apps.mtl_system_items_b msib,
       apps.mtl_parameters mp
 WHERE     mic.category_id = 1123
       AND mic.organization_id = msib.organization_id
       AND mic.inventory_item_id = msib.inventory_item_id
       AND msib.organization_id = mp.organization_id
       AND mp.master_organization_id = 222;

   v_return_status   VARCHAR2 (100);
   v_errorcode       VARCHAR2 (100);
   v_msg_count       NUMBER;
   v_msg_data        VARCHAR2 (1000);

   l_user_id         NUMBER := 33710;
   l_resp_id         NUMBER := 50884;
   l_resp_appl_id    NUMBER := 407;
   l_status          VARCHAR2 (20);
   l_err_mess        VARCHAR2 (2000);
   l_count           NUMBER := 0;
BEGIN
   SELECT COUNT (1)
     INTO l_count
     FROM dba_objects
    WHERE object_name = 'XXWC_MTL_ITEM_CATE_BKP';

   IF l_count > 0
   THEN
      mo_global.set_policY_context ('S', 162);

      fnd_global.APPS_INITIALIZE (l_user_id, l_resp_id, l_resp_appl_id);

      FOR C2 IN C1
      LOOP
         INV_ITEM_CATEGORY_PUB.DELETE_CATEGORY_ASSIGNMENT (
            p_api_version         => 1.0,
            p_init_msg_list       => FND_API.G_TRUE,
            p_commit              => FND_API.G_FALSE,
            x_return_status       => v_return_status,
            x_errorcode           => v_errorcode,
            x_msg_count           => v_msg_count,
            x_msg_data            => v_msg_data,
            p_category_id         => c2.category_id,
            p_category_set_id     => c2.category_set_id,
            p_inventory_item_id   => c2.inventory_item_id,
            p_organization_id     => c2.organization_id);

         IF v_return_status = fnd_api.g_ret_sts_success
         THEN
            COMMIT;
            l_status := 'Success';
         ELSE
            l_status := 'Failed';
            ROLLBACK;
            l_err_mess := v_msg_data;
         END IF;

         INSERT INTO XXWC.XXWC_MTL_ITEM_CATE_BKP (INVENTORY_ITEM_ID,
                                                  ORGANIZATION_ID,
                                                  CATEGORY_SET_ID,
                                                  CATEGORY_ID,
                                                  CREATION_DATE,
                                                  CREATED_BY,
                                                  PROCESS_STATUS,
                                                  ERROR_MESS)
              VALUES (C2.INVENTORY_ITEM_ID,
                      C2.ORGANIZATION_ID,
                      C2.CATEGORY_ID,
                      C2.CATEGORY_SET_ID,
                      SYSDATE,
                      l_user_id,
                      l_status,
                      l_err_mess);

         COMMIT;
      END LOOP;
   END IF;
   
EXCEPTION
   WHEN OTHERS
   THEN
      ROLLBACK;
END;
/