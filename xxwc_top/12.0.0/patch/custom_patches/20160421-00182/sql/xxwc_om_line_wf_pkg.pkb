CREATE OR REPLACE PACKAGE BODY      xxwc_om_line_wf_pkg 
AS
   /*************************************************************************
      $Header XXWC_OM_LINE_WF_PKG.PKG $
      Module Name: XXWC_OM_LINE_WF_PKG.PKG

      PURPOSE:   This package is used for data script automation through workflow

      REVISIONS:
      Ver        Date        Author                     Description
      ---------  ----------  ---------------         -------------------------
      1.0        26-Apr-2016  Niraj                   Initial Version
   ****************************************************************************/

   --Email Defaults
   g_dflt_email   fnd_user.email_address%TYPE    := 'WC-ITDEVALERTS-U1@HDSupply.com';
   g_pkg_name  VARCHAR2(50)                      := 'xxwc_om_line_wf_pkg';

   /*************************************************************************
      PROCEDURE Name: insert_api_log

      PURPOSE:   Insert log into table XXWC_OM_WF_LOG_TBL
      REVISIONS:
      Ver        Date        Author                     Description
      ---------  ----------  ---------------         -------------------------
      1.0        26-Apr-2016    Niraj             Initial Version 
   ****************************************************************************/
   PROCEDURE insert_api_log( p_order_line_id  IN   NUMBER,
                             p_api_name       IN   VARCHAR2,
                             p_api_section    IN   VARCHAR2
                           )
   IS
   --PRAGMA AUTONOMOUS_TRANSACTION;
      l_log_enabled_flag VARCHAR2(3);
   BEGIN
      SELECT NVL(fnd_profile.value('XXWC_OEL_WF_API_LOG_ENABLED'),'N') 
      INTO l_log_enabled_flag
      FROM dual;
      IF l_log_enabled_flag = 'Y' THEN
         INSERT INTO xxwc.xxwc_om_wf_log_tbl
                           (ORDER_LINE_ID      ,
                            API_NAME           ,
                            API_SECTION        ,
                            CREATION_DATE      ,
                            CREATED_BY         ,
                            LAST_UPDATE_DATE   ,
                            LAST_UPDATED_BY
                           )
                     VALUES(p_order_line_id    ,
                            p_api_name         ,
                            p_api_section      ,
                            SYSDATE            ,
                            FND_GLOBAL.USER_ID ,
                            SYSDATE            ,
                            FND_GLOBAL.USER_ID
                          );
         -- COMMIT;
      END IF;
   END insert_api_log;
   
   /*************************************************************************
      PROCEDURE Name: Correct_awaiting_invoice

      PURPOSE:   Line is shipped, invoiced and the workflow is closed, 
                 however the line status is not closed  

      REVISIONS:
      Ver        Date          Author             Description
      ---------  ------------  ------------       ------------------
      1.0        06-May-2016    Niraj             Initial Version 
   ****************************************************************************/
   PROCEDURE Correct_awaiting_invoice(P_itemtype  IN VARCHAR2
                                     ,P_itemkey   IN VARCHAR2
                                     ,p_actid     IN NUMBER
                                     ,p_funcmode  IN VARCHAR2
                                     ,resultout   IN OUT VARCHAR2
                                    )
   IS
      l_tax_line_count NUMBER;
      l_line_count     NUMBER;
      l_sec               VARCHAR2 (200);
      l_proc_name         VARCHAR2(50) := 'Correct_awaiting_invoice';
   BEGIN
      l_sec := 'Start';
      insert_api_log(p_order_line_id  => P_itemkey,
                     p_api_name       => l_proc_name,
                     p_api_section    => l_sec
                    );
      -- RUN mode - normal process execution
      IF (p_funcmode = 'RUN') THEN
         ------------------------------------------------------------------------------------
         -- Correct_awaiting_invoice
         ------------------------------------------------------------------------------------
         l_sec := 'If Invoice exists for line';
         SELECT /*+ INDEX(rctl XXWC_RA_CUST_TRX_LINES_N14)*/ COUNT(1) 
         INTO l_tax_line_count
         FROM ra_customer_trx_lines_all rctl
         WHERE rctl.interface_line_attribute6 = p_itemkey;
         IF l_tax_line_count > 0 THEN
            l_sec := 'UPDATE apps.oe_order_lines_all 3';
            UPDATE apps.oe_order_lines_all ool
            SET ool.invoice_interface_status_code ='YES'
               ,ool.open_flag = 'N'
               ,ool.flow_status_code = 'CLOSED'
               ,ool.invoiced_quantity = ool.shipped_quantity
            WHERE line_id = p_itemkey
            AND   ool.flow_status_code = 'INVOICE_HOLD';
            
            l_sec := '1.If Invoice exists for line - Total records updated in table oe_order_lines_all:'||sql%rowcount;
            insert_api_log(p_order_line_id  => P_itemkey,
                           p_api_name       => l_proc_name,
                           p_api_section    => l_sec
                          );
         END IF;

         ------------------------------------------------------------------------------------
         -- Correct_awaiting_invoice
         ------------------------------------------------------------------------------------
         ----------------------------------------------------------------------------------------------------------------
         --1. Line stuck in Picked status and the associated delivery detaild id has the OE_INTERFACED_FLAG set to N
         ----------------------------------------------------------------------------------------------------------------
         l_line_count := 0;
         l_sec := 'Line stuck in Picked status';
         SELECT COUNT(1)
           INTO l_line_count
           FROM oe_order_lines_all
          WHERE line_id = P_itemkey
            AND flow_status_code NOT IN ('AWAITING_SHIPPING' , 'BOOKED' , 'SHIPPED');
         
         IF l_line_count > 0 THEN
           l_sec := 'UPDATE apps.wsh_delivery_Details';
           UPDATE apps.wsh_delivery_Details
              SET OE_INTERFACED_FLAG = 'Y'
            WHERE source_line_id     = P_itemkey
              AND oe_interfaced_flag = 'N'
              AND released_status    = 'C';
           
		   l_sec := '2.Line stuck in Picked status - Total records updated in table wsh_delivery_Details:'||sql%rowcount;
           insert_api_log(p_order_line_id  => P_itemkey,
                          p_api_name       => l_proc_name,
                          p_api_section    => l_sec
                         );
         END IF;

      END IF;
      -- CANCEL mode is run in the event that the activity must be undone,
      -- for example when a process is reset to an earlier point
      IF (p_funcmode = 'CANCEL') THEN
         -- no result needed
          resultout := wf_engine.eng_completed||':'||wf_engine.eng_null;
         RETURN;
      END IF;
         l_sec := 'End';
         insert_api_log(p_order_line_id  => P_itemkey,
                        p_api_name       => l_proc_name,
                        p_api_section    => l_sec
                       );
         resultout := 'COMPLETE';
         OE_STANDARD_WF.Clear_Msg_Context;
         return;

      EXCEPTION
         WHEN OTHERS THEN
            xxcus_error_pkg.xxcus_error_main_api (
                                                   p_called_from         => 'XXWC_OM_LINE_WF_PKG.CORRECT_AWAITING_INVOICE'
                                                  ,p_calling             => l_sec
                                                  ,p_request_id          => fnd_global.conc_request_id
                                                  ,p_ora_error_msg       => SUBSTR(SQLERRM,1,2000)
                                                  ,p_error_desc          => 'Error in procedure correct_awaiting_invoice for line id '||P_itemkey
                                                  ,p_distribution_list   => g_dflt_email
                                                  ,p_module              => 'XXWC'
                                                 );
            wf_core.context( 'correct_awaiting_invoice'
                             ,'CorrectAwaitingInvoice'
                             ,P_itemtype
                             ,P_itemkey
                             ,TO_CHAR(p_actid)
                             ,p_funcmode
                           );
            RAISE;
   END correct_awaiting_invoice;
   
   /*************************************************************************
      PROCEDURE Name: Correct_awaiting_Order

      PURPOSE:   Update line attributes to process stucked order line 

      REVISIONS:
      Ver        Date          Author             Description
      ---------  ------------  ------------       ------------------
      1.0        18-May-2016    Niraj             Initial Version 
   ****************************************************************************/
   PROCEDURE Correct_awaiting_Order(P_itemtype  IN VARCHAR2
                                   ,P_itemkey   IN VARCHAR2
                                   ,p_actid     IN NUMBER
                                   ,p_funcmode  IN VARCHAR2
                                   ,resultout   IN OUT VARCHAR2
                                   )
   IS
      l_sec                VARCHAR2 (200);
      l_proc_name          VARCHAR2(50) := 'Correct_awaiting_Order';
      l_delivery_detail_id NUMBER;
      l_header_id          NUMBER;
      l_updated_rowcount   NUMBER;
      l_line_count         NUMBER;
      l_return_status      BOOLEAN := false;
      v_org_id             NUMBER;
      v_line_id            NUMBER := P_itemkey; --60463535;
      l_line_cnt           NUMBER;
      CURSOR wsh_dlv_dtl_id(p_line_id NUMBER)
      IS
         SELECT delivery_detail_id,source_header_id
         FROM wsh_delivery_details wdd
         WHERE wdd.source_line_id = p_line_id;
      CURSOR pending_receipts 
      IS
       SELECT rcv.transaction_id
       FROM   rcv_transactions rcv,
              po_line_locations_all poll,
              oe_drop_ship_sources oed,
              oe_order_lines_all oel
       WHERE  oel.source_type_code = 'EXTERNAL'
       AND    nvl(oel.shipped_quantity,0) = 0
       AND    oed.line_id = oel.line_id
       AND    oed.line_location_id is not null
       AND    poll.line_location_id = oed.line_location_id
       AND    rcv.po_line_location_id = poll.line_location_id
       AND    oel.line_id = v_line_id
       AND    rcv.TRANSACTION_TYPE = 'DELIVER';
     CURSOR organization (v_order_line_id NUMBER) 
     IS 
       SELECT org_id
       FROM   oe_order_lines_all
       WHERE  line_id = v_order_line_id;
   BEGIN
      l_sec := 'Start';
      insert_api_log(p_order_line_id  => P_itemkey,
                     p_api_name       => l_proc_name,
                     p_api_section    => l_sec
                    );
      -- RUN mode - normal process execution
      IF (p_funcmode = 'RUN') THEN

         ----------------------------------------------------------------------------------------------------------------
         --2. Line has been cancelled, however the delivery has been created and trying to ship for ZERO quantity, line status in Picked status
         ----------------------------------------------------------------------------------------------------------------
         l_line_count := 0;
         l_sec := 'If line quantity is zero and canceled/awaiting shipping';
         SELECT COUNT(1) INTO l_line_count
         FROM oe_order_lines_all ool
         WHERE ool.line_id = P_itemkey
         AND   ool.ordered_quantity = 0
         AND   ool.flow_status_code IN ('CANCELLED','AWAITING_SHIPPING');
         IF l_line_count > 0 THEN
            l_sec := 'Fetch cursor wsh_dlv_dtl_id';
            FOR rec IN wsh_dlv_dtl_id(P_itemkey)
            LOOP
               l_delivery_detail_id := rec.delivery_detail_id;
               l_header_id          := rec.source_header_id;
            END LOOP; 
            l_sec := 'UPDATE apps.wsh_delivery_details2';
            UPDATE apps.wsh_delivery_details
            SET released_status = 'D',
                src_requested_quantity = 0,
                requested_quantity = 0,
                shipped_quantity = 0,
                cycle_count_quantity = 0,
                cancelled_quantity = 0,
                subinventory = null,
                locator_id = null,
                lot_number = null,
                revision = null,
                inv_interfaced_flag = 'X',
                oe_interfaced_flag = 'X'
            WHERE delivery_detail_id =l_delivery_detail_id;
            
            l_updated_rowcount := SQL%ROWCOUNT;
            l_sec := '2.Total records updated in table wsh_delivery_details:'||l_updated_rowcount;
            insert_api_log(p_order_line_id  => P_itemkey,
                           p_api_name       => l_proc_name,
                           p_api_section    => l_sec
                          );
            --1 row expected to be updated
            l_sec := 'UPDATE apps.wsh_delivery_assignments';
            UPDATE apps.wsh_delivery_assignments
            SET delivery_id = null,
                parent_delivery_detail_id = null
            WHERE delivery_detail_id = l_delivery_detail_id;
          
           l_updated_rowcount := SQL%ROWCOUNT;
           l_sec := '3.Total records updated in table wsh_delivery_assignments:'||l_updated_rowcount;
           insert_api_log(p_order_line_id  => P_itemkey,
                           p_api_name       => l_proc_name,
                           p_api_section    => l_sec
                          );
            --1 row expected to be updated
            IF l_updated_rowcount > 0 THEN
               l_sec := 'UPDATE apps.oe_order_lines_all';
               UPDATE apps.oe_order_lines_all
               SET flow_status_code='CANCELLED',
                   cancelled_flag='Y'
               WHERE line_id=P_itemkey
               AND header_id=l_header_id
               AND ordered_quantity = 0
               AND flow_status_code <> 'CANCELLED';
               l_sec := '4.Total records updated in table oe_order_lines_all:'||SQL%ROWCOUNT;
               insert_api_log(p_order_line_id  => P_itemkey,
                           p_api_name       => l_proc_name,
                           p_api_section    => l_sec
                          );
            END IF;
         END IF;

         ----------------------------------------------------------------------------------------------------------------
         --3. Line has been received, however the associated sales order line has been stuck in BOOKED status
         ----------------------------------------------------------------------------------------------------------------
         l_sec := 'Check if line is Booked';
         SELECT COUNT(1) INTO l_line_cnt
         FROM oe_order_lines_all
         WHERE line_id = v_line_id
         AND   flow_status_code = 'BOOKED';
         IF l_line_cnt > 0 THEN
            l_sec := 'UPDATE oe_order_lines_all 2';
            UPDATE oe_order_lines_all oeol
            SET oeol.flow_status_code = 'AWAITING_RECEIPT'
            WHERE oeol.line_id = v_line_id
             AND oeol.flow_status_code <> 'AWAITING_RECEIPT';
            l_sec := '5.Total record updated in table oe_order_lines_all:'||SQL%ROWCOUNT;
            insert_api_log(p_order_line_id  => P_itemkey,
                           p_api_name       => l_proc_name,
                           p_api_section    => l_sec
                          );
            l_sec := 'Fetch cursor organization';
            OPEN organization(v_line_id);
            FETCH organization INTO v_org_id;
            CLOSE organization;
            
            mo_global.init('ONT');
            mo_global.set_policy_context('S', v_org_id);
            l_sec := 'Fetch cursor pending_receipts';
            FOR all_lines IN pending_receipts 
            LOOP
              l_return_status := OE_DS_PVT.DROPSHIPRECEIVE(all_lines.transaction_id,'INV');
            END LOOP;
            --COMMIT;
            -- no result, we return 'COMPLETE: ' string
            --resultout := wf_engine.eng_completed||':'||wf_engine.eng_null;
            --RETURN;
         END IF; 
         -- no result, we return 'COMPLETE: ' string
         resultout := wf_engine.eng_completed||':'||wf_engine.eng_null;
         --RETURN;
      END IF;
      
      ----------------------------------------------------------------------------------------------------------------
      -- CANCEL mode is run in the event that the activity must be undone,
      -- for example when a process is reset to an earlier point
      ----------------------------------------------------------------------------------------------------------------
      IF (p_funcmode = 'CANCEL') THEN
         -- no result needed
         l_sec := 'p_funcmode = CANCEL';
         insert_api_log(p_order_line_id  => P_itemkey,
                        p_api_name       => l_proc_name,
                        p_api_section    => l_sec
                       );
         resultout := wf_engine.eng_completed||':'||wf_engine.eng_null;
         --RETURN;
      END IF;
      l_sec := 'End';
      insert_api_log(p_order_line_id  => P_itemkey,
                     p_api_name       => l_proc_name,
                     p_api_section    => l_sec
                    );
      IF resultout IS NULL THEN
         resultout := wf_engine.eng_null;
      END IF;

         resultout := 'COMPLETE';
         OE_STANDARD_WF.Clear_Msg_Context;
         return;

   EXCEPTION
      WHEN OTHERS THEN
         insert_api_log(p_order_line_id  => P_itemkey,
                        p_api_name       => l_proc_name,
                        p_api_section    => l_sec
                       );
         xxcus_error_pkg.xxcus_error_main_api (
                                                p_called_from         => UPPER(g_pkg_name)||'.'||UPPER(l_proc_name)
                                               ,p_calling             => l_sec
                                               ,p_request_id          => fnd_global.conc_request_id
                                               ,p_ora_error_msg       => SUBSTR(SQLERRM,1,2000)
                                               ,p_error_desc          => 'Error in procedure '||l_proc_name||' for line id '||P_itemkey
                                               ,p_distribution_list   => g_dflt_email
                                               ,p_module              => 'XXWC'
                                              );
         wf_core.context( l_proc_name
                          ,'CorrectAwaitingInvoice'
                          ,P_itemtype
                          ,P_itemkey
                          ,TO_CHAR(p_actid)
                          ,p_funcmode
                        );
         RAISE;
   END Correct_awaiting_Order;
END xxwc_om_line_wf_pkg;
/
