/******************************************************************************
  $Header TMS_20180926-00001_ORDER_LINE_STUCK_IN_FULFILLED.sql $
  Module Name:Data Fix script for 20180926-00001

  PURPOSE: Data fix script for 20180926-00001 Order stuck in Booked

  REVISIONS:
  Ver        Date         Author                Description
  ---------  -----------  ------------------    -------------------------------
  1.0        26-SEP-2018  Pattabhi Avula        TMS#20180926-00001 - Return Order 30057912 line 8.1 is stuck in the Fulfilled line status

*******************************************************************************/
SET SERVEROUTPUT ON SIZE 1000000

BEGIN
   DBMS_OUTPUT.put_line ('Before Update');

   UPDATE apps.oe_order_lines_all
      SET open_flag = 'N',
          flow_status_Code = 'CLOSED'
    WHERE line_id = 134599873
	  AND header_id = 78347765;


   DBMS_OUTPUT.put_line ('Records update-' || SQL%ROWCOUNT);
   COMMIT;
EXCEPTION
   WHEN OTHERS
   THEN
      DBMS_OUTPUT.put_line ('Unable to update record ' || SQLERRM);
END;
/