/*
Ticket#                                                                 Date                 Author                          Notes
-------------------------------------------------------------------------------------------------------------------------------------------------------------------
TMS 20170821-00206 / RITM0011184   09/15/2017   Balaguru Seshadri   HDS Concur Actuals - Personal journals summary table 
*/
--
drop table xxcus.xxcus_gl_intf_control_b;
--
create table xxcus.xxcus_gl_intf_control_b
(
  concur_erp varchar2(48),
  user_je_source_name varchar2(25),
  je_source_name        varchar2(25),
  status                varchar2(60),
  interface_run_id      number,
  group_id              number,
  ledger_id       number,
  concur_batch_id number,
  hds_run_id number,  
  request_id            number,
  set_req_id number,
  wrapper_req_id number,
  created_by number,
  creation_date date,
  last_updated_by number,
  last_update_date date,
  import_status_request_id number
);
--
comment on table xxcus.xxcus_gl_intf_control_b is 'TMS 20170821-00206 / RITM0011184';
--