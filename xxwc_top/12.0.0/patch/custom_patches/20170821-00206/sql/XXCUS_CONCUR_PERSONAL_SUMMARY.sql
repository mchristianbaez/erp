/*
Ticket#                                                                 Date                 Author                          Notes
-------------------------------------------------------------------------------------------------------------------------------------------------------------------
TMS 20170821-00206 / RITM0011184   09/15/2017   Balaguru Seshadri   HDS Concur Actuals - Personal journals summary table 
*/
--
drop table xxcus.xxcus_concur_personal_summary;
--
create table xxcus.xxcus_concur_personal_summary
(
emp_org_erp    varchar2(48),
emp_org_company    varchar2(48),
emp_org_branch    varchar2(48), 
journal_payee_pmt_type_name     varchar2(64),
concur_batch_date date, 
currency    varchar2(3),
emp_id    varchar2(48),
emp_full_name    varchar2(80),
concur_batch_id    number default 0,
rpt_key    number default 0,
personal_charges_original    number default 0,
emp_oop_amount    number default 0,
net_cash_expenses    number default 0,
personal_charges_final    number default 0,
personal_recaptured    number default 0,
ledger_id number,
gl_interfaced_flag varchar2(1),
created_by number,
creation_date date,
run_id number
);
--
comment on table xxcus.xxcus_concur_personal_summary is 'TMS 20170821-00206 / RITM0011184 ';
--