/***********************************************************************************************************************************************
   NAME:     TMS_20180912-00002_Datafix.sql
   PURPOSE:  Data fix

   REVISIONS:
   Ver        Date        Author           Description
   ---------  ----------  ---------------  ----------------------------------------------------------------------------------------------------
   1.0        09/12/2018  Rakesh Patel     TMS#20180912-00002 -TMS# 20180912-00002-Pricing records update start date and end date today
************************************************************************************************************************************************/
SET SERVEROUTPUT ON SIZE 1000000

BEGIN
   DBMS_OUTPUT.put_line ('Before Updated');

   update apps.qp_list_lines
      set start_Date_Active=trunc (sysdate) -1,
          end_Date_Active=trunc (sysdate) -1
    where created_by=49908
      and trunc (creation_Date)=trunc (sysdate);

   DBMS_OUTPUT.put_line ('Records Updated -' || SQL%ROWCOUNT);

   COMMIT;

   EXCEPTION
   WHEN OTHERS
   THEN
      DBMS_OUTPUT.put_line ('Unable to Updated record ' || SQLERRM);
	  ROLLBACK;
END;
/