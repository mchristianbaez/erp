DECLARE
	l_line_id      NUMBER := 42316910;
	l_activity_id  NUMBER ;
	l_status       VARCHAR2(60);
	l_result       VARCHAR2(1000);
	l_file_name    VARCHAR2(1000);
	l_db_name      VARCHAR2(1000);
BEGIN
	-- Setup debugging
	oe_debug_pub.debug_on;
	oe_debug_pub.initialize;
	oe_debug_pub.setdebuglevel(5);
	
	l_file_name := oe_debug_pub.set_debug_mode('FILE');
	dbms_output.put_line('Debug log is located at: ' || Oe_debug_pub.g_dir || '/' ||Oe_debug_pub.g_file);
	
	SELECT name 
	INTO l_db_name
	FROM V$DATABASE;
	oe_debug_pub.add('Running for databse: ' || l_db_name);
			

    Oe_standard_wf.OEOL_Selector(p_itemtype => 'OEOL'
                                 ,p_itemkey => to_char(l_line_id)
                                 ,p_actid => 12345
                                 ,p_funcmode => 'SET_CTX'
                                 ,p_result => l_result
								 );
								 
	oe_debug_pub.add('Result: '||l_result );

    oe_debug_pub.add('Calling handleerror for line...');
	
	wf_engine.handleerror( itemtype    =>    'OEOL'
						  ,itemkey     =>    To_Char(l_line_id)
						  ,activity    =>    'FULFILL_LINE'
						  ,command     =>    'RETRY'
						  ,result      =>    NULL
						);
	
	oe_debug_pub.add('FULFILL_LINE Retried...');
	dbms_output.put_line('FULFILL_LINE Retried...');
	
	-- Finishing script with success
	oe_debug_pub.add('Script succesfully executed.');
	dbms_output.put_line('Script succesfully executed.');
	oe_debug_pub.debug_off;
	
EXCEPTION
	WHEN Others THEN
		oe_debug_pub.ADD('Error: ...'||SQLERRM);
        Dbms_Output.Put_Line('Error: ...'||SQLERRM);
        ROLLBACK;
	
END;
/
COMMIT;
/