/*
   Ticket#                               Date         Author             Notes
   -------------------------------------------------------------------------------------------------------------------------------------------------------
   TMS 20170412-00112 / ESMS 554216      02/17/2017   Balaguru Seshadri  Concur process related tables
*/
--
CREATE OR REPLACE SYNONYM APPS.XXCUS_SRC_CONCUR_SAE_OOP FOR XXCUS.XXCUS_SRC_CONCUR_SAE_OOP;
--