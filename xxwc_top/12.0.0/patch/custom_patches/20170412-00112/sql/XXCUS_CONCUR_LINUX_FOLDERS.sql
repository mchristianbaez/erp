   /*
      ************************************************************************
    HD Supply
    All rights reserved.
   **************************************************************************
     PURPOSE:   GSC Concur SAE file process 

     REVISIONS:
     Ver        Date                Author            Description
     ---------  ----------         ---------------    -------------------------------------------------------------------------------------------------------------
     1.0       04/20/2016  Balaguru Seshadri          TMS: 20170412-00112 / ESMS 554216 - Create folder xx_iface/ebsqa/outbound/concur and sub folders
   ************************************************************************* 
  */
DECLARE
   l_db_name    VARCHAR2 (20) := NULL;
   l_path_inb   VARCHAR2 (240) := NULL;
   l_path_oub   VARCHAR2 (240) := NULL;
   l_sql        VARCHAR2 (240) := NULL;
   g_command    VARCHAR2 (2000) := NULL; 
   l_result     VARCHAR2 (2000) := NULL;    
--
BEGIN
   --
   SELECT LOWER (name) INTO l_db_name FROM v$database;
   --
	begin   
     --	
      l_path_oub := '/xx_iface/' || l_db_name || '/outbound/concur';	
	 --
				 g_command := 'mkdir '
							 ||' '
							 ||l_path_oub                      
							 ; 
				 --
				 dbms_output.put_line('MKDIR command:');
				 dbms_output.put_line('============');
				 dbms_output.put_line(g_command);
				 --
				 select xxwc_edi_iface_pkg.xxwc_oscommand_run (g_command)
				 into   l_result
				 from   dual;
				 dbms_output.put_line('Create folder '||l_path_oub||', Status: '||nvl(l_result, 'Success'));
				 --
                 if (l_result is null) then --folder created just make sure it has 777 permission				    
					 --
				     g_command := 'chmod 777 '
							 ||l_path_oub
							 ; 
					 --
					 dbms_output.put_line('CHMOD command:');
					 dbms_output.put_line('============');
					 dbms_output.put_line(g_command);
					 --
					 select xxwc_edi_iface_pkg.xxwc_oscommand_run (g_command)
					 into   l_result
					 from   dual;
					 dbms_output.put_line('Set 777 for folder '||l_path_oub||', Status: '||nvl(l_result, 'Success'));
					 --	
                     if l_result is null then --concur folder is setup and good to go
					   --
						begin  --create concur/actuals folder 
						 --	
						  l_path_oub := '/xx_iface/' || l_db_name || '/outbound/concur/actuals';	
						 --
									 g_command := 'mkdir '
												 ||' '
												 ||l_path_oub                      
												 ; 
									 --
									 dbms_output.put_line('MKDIR command:');
									 dbms_output.put_line('============');
									 dbms_output.put_line(g_command);
									 --
									 select xxwc_edi_iface_pkg.xxwc_oscommand_run (g_command)
									 into   l_result
									 from   dual;
									 dbms_output.put_line('Create folder '||l_path_oub||', Status: '||nvl(l_result, 'Success'));
									 --
									 if (l_result is null) then --folder created just make sure it has 777 permission				    
										 --
										 g_command := 'chmod 777 '
												 ||l_path_oub
												 ; 
										 --
										 dbms_output.put_line('CHMOD command:');
										 dbms_output.put_line('============');
										 dbms_output.put_line(g_command);
										 --
										 select xxwc_edi_iface_pkg.xxwc_oscommand_run (g_command)
										 into   l_result
										 from   dual;
										 dbms_output.put_line('Set 777 for folder '||l_path_oub||', Status: '||nvl(l_result, 'Success'));
										 -- Create concur/actuals/ and all sub folders.
										    --
											begin  --create concur/actuals/ww_mincron folder 
											 --	
											  l_path_oub := '/xx_iface/' || l_db_name || '/outbound/concur/actuals/ww_mincron';	
											 --
														 g_command := 'mkdir '
																	 ||' '
																	 ||l_path_oub                      
																	 ; 
														 --
														 dbms_output.put_line('MKDIR command:');
														 dbms_output.put_line('============');
														 dbms_output.put_line(g_command);
														 --
														 select xxwc_edi_iface_pkg.xxwc_oscommand_run (g_command)
														 into   l_result
														 from   dual;
														 dbms_output.put_line('Create folder '||l_path_oub||', Status: '||nvl(l_result, 'Success'));
														 --
														 if (l_result is null) then --folder created just make sure it has 777 permission				    
															 --
															 g_command := 'chmod 777 '
																	 ||l_path_oub
																	 ; 
															 --
															 dbms_output.put_line('CHMOD command:');
															 dbms_output.put_line('============');
															 dbms_output.put_line(g_command);
															 --
															 select xxwc_edi_iface_pkg.xxwc_oscommand_run (g_command)
															 into   l_result
															 from   dual;
															 dbms_output.put_line('Set 777 for folder '||l_path_oub||', Status: '||nvl(l_result, 'Success'));
															 --					 
														 end if;
											 --
											exception
												when others then
												 dbms_output.put_line('Error in creation of concur folder, message =>'||sqlerrm);
												 raise program_error;
											end; --create concur/actuals/ww_mincron folder
										    --	
											begin  --create concur/actuals/ci_oracle folder 
											 --	
											  l_path_oub := '/xx_iface/' || l_db_name || '/outbound/concur/actuals/ci_oracle';	
											 --
														 g_command := 'mkdir '
																	 ||' '
																	 ||l_path_oub                      
																	 ; 
														 --
														 dbms_output.put_line('MKDIR command:');
														 dbms_output.put_line('============');
														 dbms_output.put_line(g_command);
														 --
														 select xxwc_edi_iface_pkg.xxwc_oscommand_run (g_command)
														 into   l_result
														 from   dual;
														 dbms_output.put_line('Create folder '||l_path_oub||', Status: '||nvl(l_result, 'Success'));
														 --
														 if (l_result is null) then --folder created just make sure it has 777 permission				    
															 --
															 g_command := 'chmod 777 '
																	 ||l_path_oub
																	 ; 
															 --
															 dbms_output.put_line('CHMOD command:');
															 dbms_output.put_line('============');
															 dbms_output.put_line(g_command);
															 --
															 select xxwc_edi_iface_pkg.xxwc_oscommand_run (g_command)
															 into   l_result
															 from   dual;
															 dbms_output.put_line('Set 777 for folder '||l_path_oub||', Status: '||nvl(l_result, 'Success'));
															 --					 
														 end if;
											 --
											exception
												when others then
												 dbms_output.put_line('Error in creation of concur folder, message =>'||sqlerrm);
												 raise program_error;
											end; --create concur/actuals/ci_oracle folder
										    --
											begin  --create concur/actuals/gsc_oracle folder 
											 --	
											  l_path_oub := '/xx_iface/' || l_db_name || '/outbound/concur/actuals/gsc_oracle';	
											 --
														 g_command := 'mkdir '
																	 ||' '
																	 ||l_path_oub                      
																	 ; 
														 --
														 dbms_output.put_line('MKDIR command:');
														 dbms_output.put_line('============');
														 dbms_output.put_line(g_command);
														 --
														 select xxwc_edi_iface_pkg.xxwc_oscommand_run (g_command)
														 into   l_result
														 from   dual;
														 dbms_output.put_line('Create folder '||l_path_oub||', Status: '||nvl(l_result, 'Success'));
														 --
														 if (l_result is null) then --folder created just make sure it has 777 permission				    
															 --
															 g_command := 'chmod 777 '
																	 ||l_path_oub
																	 ; 
															 --
															 dbms_output.put_line('CHMOD command:');
															 dbms_output.put_line('============');
															 dbms_output.put_line(g_command);
															 --
															 select xxwc_edi_iface_pkg.xxwc_oscommand_run (g_command)
															 into   l_result
															 from   dual;
															 dbms_output.put_line('Set 777 for folder '||l_path_oub||', Status: '||nvl(l_result, 'Success'));
															 --					 
														 end if;
											 --
											exception
												when others then
												 dbms_output.put_line('Error in creation of concur folder, message =>'||sqlerrm);
												 raise program_error;
											end; --create concur/actuals/gsc_oracle folder
										    --
											begin  --create concur/actuals/ci_cad_gp folder 
											 --	
											  l_path_oub := '/xx_iface/' || l_db_name || '/outbound/concur/actuals/ci_cad_gp';	
											 --
														 g_command := 'mkdir '
																	 ||' '
																	 ||l_path_oub                      
																	 ; 
														 --
														 dbms_output.put_line('MKDIR command:');
														 dbms_output.put_line('============');
														 dbms_output.put_line(g_command);
														 --
														 select xxwc_edi_iface_pkg.xxwc_oscommand_run (g_command)
														 into   l_result
														 from   dual;
														 dbms_output.put_line('Create folder '||l_path_oub||', Status: '||nvl(l_result, 'Success'));
														 --
														 if (l_result is null) then --folder created just make sure it has 777 permission				    
															 --
															 g_command := 'chmod 777 '
																	 ||l_path_oub
																	 ; 
															 --
															 dbms_output.put_line('CHMOD command:');
															 dbms_output.put_line('============');
															 dbms_output.put_line(g_command);
															 --
															 select xxwc_edi_iface_pkg.xxwc_oscommand_run (g_command)
															 into   l_result
															 from   dual;
															 dbms_output.put_line('Set 777 for folder '||l_path_oub||', Status: '||nvl(l_result, 'Success'));
															 --					 
														 end if;
											 --
											exception
												when others then
												 dbms_output.put_line('Error in creation of concur folder, message =>'||sqlerrm);
												 raise program_error;
											end; --create concur/actuals/ci_cad_gp folder
										    --	
											begin  --create concur/actuals/ci_his_dynamics folder 
											 --	
											  l_path_oub := '/xx_iface/' || l_db_name || '/outbound/concur/actuals/ci_his_dynamics';	
											 --
														 g_command := 'mkdir '
																	 ||' '
																	 ||l_path_oub                      
																	 ; 
														 --
														 dbms_output.put_line('MKDIR command:');
														 dbms_output.put_line('============');
														 dbms_output.put_line(g_command);
														 --
														 select xxwc_edi_iface_pkg.xxwc_oscommand_run (g_command)
														 into   l_result
														 from   dual;
														 dbms_output.put_line('Create folder '||l_path_oub||', Status: '||nvl(l_result, 'Success'));
														 --
														 if (l_result is null) then --folder created just make sure it has 777 permission				    
															 --
															 g_command := 'chmod 777 '
																	 ||l_path_oub
																	 ; 
															 --
															 dbms_output.put_line('CHMOD command:');
															 dbms_output.put_line('============');
															 dbms_output.put_line(g_command);
															 --
															 select xxwc_edi_iface_pkg.xxwc_oscommand_run (g_command)
															 into   l_result
															 from   dual;
															 dbms_output.put_line('Set 777 for folder '||l_path_oub||', Status: '||nvl(l_result, 'Success'));
															 --					 
														 end if;
											 --
											exception
												when others then
												 dbms_output.put_line('Error in creation of concur folder, message =>'||sqlerrm);
												 raise program_error;
											end; --create concur/actuals/ci_his_dynamics folder
										    --
											begin  --create concur/actuals/fm_cad_infor folder 
											 --	
											  l_path_oub := '/xx_iface/' || l_db_name || '/outbound/concur/actuals/fm_cad_infor';	
											 --
														 g_command := 'mkdir '
																	 ||' '
																	 ||l_path_oub                      
																	 ; 
														 --
														 dbms_output.put_line('MKDIR command:');
														 dbms_output.put_line('============');
														 dbms_output.put_line(g_command);
														 --
														 select xxwc_edi_iface_pkg.xxwc_oscommand_run (g_command)
														 into   l_result
														 from   dual;
														 dbms_output.put_line('Create folder '||l_path_oub||', Status: '||nvl(l_result, 'Success'));
														 --
														 if (l_result is null) then --folder created just make sure it has 777 permission				    
															 --
															 g_command := 'chmod 777 '
																	 ||l_path_oub
																	 ; 
															 --
															 dbms_output.put_line('CHMOD command:');
															 dbms_output.put_line('============');
															 dbms_output.put_line(g_command);
															 --
															 select xxwc_edi_iface_pkg.xxwc_oscommand_run (g_command)
															 into   l_result
															 from   dual;
															 dbms_output.put_line('Set 777 for folder '||l_path_oub||', Status: '||nvl(l_result, 'Success'));
															 --					 
														 end if;
											 --
											exception
												when others then
												 dbms_output.put_line('Error in creation of concur folder, message =>'||sqlerrm);
												 raise program_error;
											end; --create concur/actuals/fm_cad_infor folder
										    --	
											begin  --create concur/actuals/fm_sap folder 
											 --	
											  l_path_oub := '/xx_iface/' || l_db_name || '/outbound/concur/actuals/fm_sap';	
											 --
														 g_command := 'mkdir '
																	 ||' '
																	 ||l_path_oub                      
																	 ; 
														 --
														 dbms_output.put_line('MKDIR command:');
														 dbms_output.put_line('============');
														 dbms_output.put_line(g_command);
														 --
														 select xxwc_edi_iface_pkg.xxwc_oscommand_run (g_command)
														 into   l_result
														 from   dual;
														 dbms_output.put_line('Create folder '||l_path_oub||', Status: '||nvl(l_result, 'Success'));
														 --
														 if (l_result is null) then --folder created just make sure it has 777 permission				    
															 --
															 g_command := 'chmod 777 '
																	 ||l_path_oub
																	 ; 
															 --
															 dbms_output.put_line('CHMOD command:');
															 dbms_output.put_line('============');
															 dbms_output.put_line(g_command);
															 --
															 select xxwc_edi_iface_pkg.xxwc_oscommand_run (g_command)
															 into   l_result
															 from   dual;
															 dbms_output.put_line('Set 777 for folder '||l_path_oub||', Status: '||nvl(l_result, 'Success'));
															 --					 
														 end if;
											 --
											exception
												when others then
												 dbms_output.put_line('Error in creation of concur folder, message =>'||sqlerrm);
												 raise program_error;
											end; --create concur/actuals/fm_sap folder
										    --											
                                         -- Create concur/actuals/ and all sub folders										 
									 end if;
						 --
						exception
							when others then
							 dbms_output.put_line('Error in creation of concur folder, message =>'||sqlerrm);
							 raise program_error;
						end; --create concur/actuals folder
					   -- Create concur/accruals folder
					   --
						begin  --create concur/accruals folder 
						 --	
						  l_path_oub := '/xx_iface/' || l_db_name || '/outbound/concur/accruals';	
						 --
									 g_command := 'mkdir '
												 ||' '
												 ||l_path_oub                      
												 ; 
									 --
									 dbms_output.put_line('MKDIR command:');
									 dbms_output.put_line('============');
									 dbms_output.put_line(g_command);
									 --
									 select xxwc_edi_iface_pkg.xxwc_oscommand_run (g_command)
									 into   l_result
									 from   dual;
									 dbms_output.put_line('Create folder '||l_path_oub||', Status: '||nvl(l_result, 'Success'));
									 --
									 if (l_result is null) then --folder created just make sure it has 777 permission				    
										 --
										 g_command := 'chmod 777 '
												 ||l_path_oub
												 ; 
										 --
										 dbms_output.put_line('CHMOD command:');
										 dbms_output.put_line('============');
										 dbms_output.put_line(g_command);
										 --
										 select xxwc_edi_iface_pkg.xxwc_oscommand_run (g_command)
										 into   l_result
										 from   dual;
										 dbms_output.put_line('Set 777 for folder '||l_path_oub||', Status: '||nvl(l_result, 'Success'));
										 -- Create concur/accruals/ and all sub folders.
										    --
											begin  --create concur/accruals/ww_mincron folder 
											 --	
											  l_path_oub := '/xx_iface/' || l_db_name || '/outbound/concur/accruals/ww_mincron';	
											 --
														 g_command := 'mkdir '
																	 ||' '
																	 ||l_path_oub                      
																	 ; 
														 --
														 dbms_output.put_line('MKDIR command:');
														 dbms_output.put_line('============');
														 dbms_output.put_line(g_command);
														 --
														 select xxwc_edi_iface_pkg.xxwc_oscommand_run (g_command)
														 into   l_result
														 from   dual;
														 dbms_output.put_line('Create folder '||l_path_oub||', Status: '||nvl(l_result, 'Success'));
														 --
														 if (l_result is null) then --folder created just make sure it has 777 permission				    
															 --
															 g_command := 'chmod 777 '
																	 ||l_path_oub
																	 ; 
															 --
															 dbms_output.put_line('CHMOD command:');
															 dbms_output.put_line('============');
															 dbms_output.put_line(g_command);
															 --
															 select xxwc_edi_iface_pkg.xxwc_oscommand_run (g_command)
															 into   l_result
															 from   dual;
															 dbms_output.put_line('Set 777 for folder '||l_path_oub||', Status: '||nvl(l_result, 'Success'));
															 --					 
														 end if;
											 --
											exception
												when others then
												 dbms_output.put_line('Error in creation of concur folder, message =>'||sqlerrm);
												 raise program_error;
											end; --create concur/accruals/ww_mincron folder
										    --	
											begin  --create concur/accruals/ci_oracle folder 
											 --	
											  l_path_oub := '/xx_iface/' || l_db_name || '/outbound/concur/accruals/ci_oracle';	
											 --
														 g_command := 'mkdir '
																	 ||' '
																	 ||l_path_oub                      
																	 ; 
														 --
														 dbms_output.put_line('MKDIR command:');
														 dbms_output.put_line('============');
														 dbms_output.put_line(g_command);
														 --
														 select xxwc_edi_iface_pkg.xxwc_oscommand_run (g_command)
														 into   l_result
														 from   dual;
														 dbms_output.put_line('Create folder '||l_path_oub||', Status: '||nvl(l_result, 'Success'));
														 --
														 if (l_result is null) then --folder created just make sure it has 777 permission				    
															 --
															 g_command := 'chmod 777 '
																	 ||l_path_oub
																	 ; 
															 --
															 dbms_output.put_line('CHMOD command:');
															 dbms_output.put_line('============');
															 dbms_output.put_line(g_command);
															 --
															 select xxwc_edi_iface_pkg.xxwc_oscommand_run (g_command)
															 into   l_result
															 from   dual;
															 dbms_output.put_line('Set 777 for folder '||l_path_oub||', Status: '||nvl(l_result, 'Success'));
															 --					 
														 end if;
											 --
											exception
												when others then
												 dbms_output.put_line('Error in creation of concur folder, message =>'||sqlerrm);
												 raise program_error;
											end; --create concur/accruals/ci_oracle folder
										    --
											begin  --create concur/accruals/gsc_oracle folder 
											 --	
											  l_path_oub := '/xx_iface/' || l_db_name || '/outbound/concur/accruals/gsc_oracle';	
											 --
														 g_command := 'mkdir '
																	 ||' '
																	 ||l_path_oub                      
																	 ; 
														 --
														 dbms_output.put_line('MKDIR command:');
														 dbms_output.put_line('============');
														 dbms_output.put_line(g_command);
														 --
														 select xxwc_edi_iface_pkg.xxwc_oscommand_run (g_command)
														 into   l_result
														 from   dual;
														 dbms_output.put_line('Create folder '||l_path_oub||', Status: '||nvl(l_result, 'Success'));
														 --
														 if (l_result is null) then --folder created just make sure it has 777 permission				    
															 --
															 g_command := 'chmod 777 '
																	 ||l_path_oub
																	 ; 
															 --
															 dbms_output.put_line('CHMOD command:');
															 dbms_output.put_line('============');
															 dbms_output.put_line(g_command);
															 --
															 select xxwc_edi_iface_pkg.xxwc_oscommand_run (g_command)
															 into   l_result
															 from   dual;
															 dbms_output.put_line('Set 777 for folder '||l_path_oub||', Status: '||nvl(l_result, 'Success'));
															 --					 
														 end if;
											 --
											exception
												when others then
												 dbms_output.put_line('Error in creation of concur folder, message =>'||sqlerrm);
												 raise program_error;
											end; --create concur/accruals/gsc_oracle folder
										    --
											begin  --create concur/accruals/ci_cad_gp folder 
											 --	
											  l_path_oub := '/xx_iface/' || l_db_name || '/outbound/concur/accruals/ci_cad_gp';	
											 --
														 g_command := 'mkdir '
																	 ||' '
																	 ||l_path_oub                      
																	 ; 
														 --
														 dbms_output.put_line('MKDIR command:');
														 dbms_output.put_line('============');
														 dbms_output.put_line(g_command);
														 --
														 select xxwc_edi_iface_pkg.xxwc_oscommand_run (g_command)
														 into   l_result
														 from   dual;
														 dbms_output.put_line('Create folder '||l_path_oub||', Status: '||nvl(l_result, 'Success'));
														 --
														 if (l_result is null) then --folder created just make sure it has 777 permission				    
															 --
															 g_command := 'chmod 777 '
																	 ||l_path_oub
																	 ; 
															 --
															 dbms_output.put_line('CHMOD command:');
															 dbms_output.put_line('============');
															 dbms_output.put_line(g_command);
															 --
															 select xxwc_edi_iface_pkg.xxwc_oscommand_run (g_command)
															 into   l_result
															 from   dual;
															 dbms_output.put_line('Set 777 for folder '||l_path_oub||', Status: '||nvl(l_result, 'Success'));
															 --					 
														 end if;
											 --
											exception
												when others then
												 dbms_output.put_line('Error in creation of concur folder, message =>'||sqlerrm);
												 raise program_error;
											end; --create concur/accruals/ci_cad_gp folder
										    --	
											begin  --create concur/accruals/ci_his_dynamics folder 
											 --	
											  l_path_oub := '/xx_iface/' || l_db_name || '/outbound/concur/accruals/ci_his_dynamics';	
											 --
														 g_command := 'mkdir '
																	 ||' '
																	 ||l_path_oub                      
																	 ; 
														 --
														 dbms_output.put_line('MKDIR command:');
														 dbms_output.put_line('============');
														 dbms_output.put_line(g_command);
														 --
														 select xxwc_edi_iface_pkg.xxwc_oscommand_run (g_command)
														 into   l_result
														 from   dual;
														 dbms_output.put_line('Create folder '||l_path_oub||', Status: '||nvl(l_result, 'Success'));
														 --
														 if (l_result is null) then --folder created just make sure it has 777 permission				    
															 --
															 g_command := 'chmod 777 '
																	 ||l_path_oub
																	 ; 
															 --
															 dbms_output.put_line('CHMOD command:');
															 dbms_output.put_line('============');
															 dbms_output.put_line(g_command);
															 --
															 select xxwc_edi_iface_pkg.xxwc_oscommand_run (g_command)
															 into   l_result
															 from   dual;
															 dbms_output.put_line('Set 777 for folder '||l_path_oub||', Status: '||nvl(l_result, 'Success'));
															 --					 
														 end if;
											 --
											exception
												when others then
												 dbms_output.put_line('Error in creation of concur folder, message =>'||sqlerrm);
												 raise program_error;
											end; --create concur/accruals/ci_his_dynamics folder
										    --
											begin  --create concur/accruals/fm_cad_infor folder 
											 --	
											  l_path_oub := '/xx_iface/' || l_db_name || '/outbound/concur/accruals/fm_cad_infor';	
											 --
														 g_command := 'mkdir '
																	 ||' '
																	 ||l_path_oub                      
																	 ; 
														 --
														 dbms_output.put_line('MKDIR command:');
														 dbms_output.put_line('============');
														 dbms_output.put_line(g_command);
														 --
														 select xxwc_edi_iface_pkg.xxwc_oscommand_run (g_command)
														 into   l_result
														 from   dual;
														 dbms_output.put_line('Create folder '||l_path_oub||', Status: '||nvl(l_result, 'Success'));
														 --
														 if (l_result is null) then --folder created just make sure it has 777 permission				    
															 --
															 g_command := 'chmod 777 '
																	 ||l_path_oub
																	 ; 
															 --
															 dbms_output.put_line('CHMOD command:');
															 dbms_output.put_line('============');
															 dbms_output.put_line(g_command);
															 --
															 select xxwc_edi_iface_pkg.xxwc_oscommand_run (g_command)
															 into   l_result
															 from   dual;
															 dbms_output.put_line('Set 777 for folder '||l_path_oub||', Status: '||nvl(l_result, 'Success'));
															 --					 
														 end if;
											 --
											exception
												when others then
												 dbms_output.put_line('Error in creation of concur folder, message =>'||sqlerrm);
												 raise program_error;
											end; --create concur/accruals/fm_cad_infor folder
										    --	
											begin  --create concur/accruals/fm_sap folder 
											 --	
											  l_path_oub := '/xx_iface/' || l_db_name || '/outbound/concur/accruals/fm_sap';	
											 --
														 g_command := 'mkdir '
																	 ||' '
																	 ||l_path_oub                      
																	 ; 
														 --
														 dbms_output.put_line('MKDIR command:');
														 dbms_output.put_line('============');
														 dbms_output.put_line(g_command);
														 --
														 select xxwc_edi_iface_pkg.xxwc_oscommand_run (g_command)
														 into   l_result
														 from   dual;
														 dbms_output.put_line('Create folder '||l_path_oub||', Status: '||nvl(l_result, 'Success'));
														 --
														 if (l_result is null) then --folder created just make sure it has 777 permission				    
															 --
															 g_command := 'chmod 777 '
																	 ||l_path_oub
																	 ; 
															 --
															 dbms_output.put_line('CHMOD command:');
															 dbms_output.put_line('============');
															 dbms_output.put_line(g_command);
															 --
															 select xxwc_edi_iface_pkg.xxwc_oscommand_run (g_command)
															 into   l_result
															 from   dual;
															 dbms_output.put_line('Set 777 for folder '||l_path_oub||', Status: '||nvl(l_result, 'Success'));
															 --					 
														 end if;
											 --
											exception
												when others then
												 dbms_output.put_line('Error in creation of concur folder, message =>'||sqlerrm);
												 raise program_error;
											end; --create concur/accruals/fm_sap folder
										    --											
                                         -- Create concur/accruals/ and all sub folders										 
									 end if;
						 --
						exception
							when others then
							 dbms_output.put_line('Error in creation of concur folder, message =>'||sqlerrm);
							 raise program_error;
						end; --create concur/accruals folder
					   --  					   
                       -- Create concur/accruals folder					   
			         end if; -- concur folder is setup and good to go
				 end if; -- concur folder is setup and good to go
     --
	exception
		when others then
		 dbms_output.put_line('Error in creation of concur folder, message =>'||sqlerrm);
		 raise program_error;
	end;
	-- 
EXCEPTION
 WHEN OTHERS THEN
  DBMS_OUTPUT.PUT_LINE('OUTER BLOCK : '||SQLERRM);
END;
/