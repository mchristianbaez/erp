CREATE OR REPLACE PACKAGE BODY APPS.xxcus_gsc_po_import_locn as
/*
 -- Author: Balaguru Seshadri
 -- Concurrent Job: HDS GSC PO: Import OPN Locations.
 -- Author: Balaguru Seshadri
 -- Scope: This package imports all Oracle Property Manager active locations that belong to a building, floor or office as hr_locations for use
 --        by the HDS GSC Requisition process using iPricurement application. 
 -- Modification History
 -- ESMS         RFC       Date           Version   Comments
 -- =========== =========  ============   ========  =====================================================================
 -- 264411                 10-DEC-2014    1.0       Initial version
 -- 264411                 11-DEC-2014    1.1       Add parameter l_org_id to crete_locn routine and pass it on to attribute_category
 --                                                 to fix the global dff context issue
 -- 553794                 14-MAR-2017  1.2       TMS 20170314-00175, OPN Locations missing in HR Locations setup. can't select receiving location TX194 
*/
 --
 g_locn_rec hr_locations_all%rowtype;
 --
 /*
 *************************************************************************
 *
 * procedure: print_log
 * DESCRIPTION: Procedure to display debug log messages
 * PARAMETERS
 * ==========
 * NAME              TYPE     DESCRIPTION
.* ----------------- -------- ---------------------------------------------
 * p_message         IN      Input message
 * RETURN VALUE: None
 * History
 * =======
 -- ESMS         RFC       Date           Version   Comments
 -- =========== =========  ============   ========  =====================================================================
 -- 264411                 10-DEC-2014    1.0       Initial version
 */  
 procedure print_output(p_message in varchar2) is
 begin
     if  fnd_global.conc_request_id >0 then
      fnd_file.put_line(fnd_file.output, p_message);
     else
      dbms_output.put_line(p_message);
     end if;
 end print_output;
 --  
 /*
 *************************************************************************
 *
 * procedure: print_log
 * DESCRIPTION: Procedure to display debug log messages
 * PARAMETERS
 * ==========
 * NAME              TYPE     DESCRIPTION
.* ----------------- -------- ---------------------------------------------
 * p_message         IN      Input message
 * RETURN VALUE: None
 * History
 * =======
 -- ESMS         RFC       Date           Version   Comments
 -- =========== =========  ============   ========  =====================================================================
 -- 264411                 10-DEC-2014    1.0       Initial version
 */  
 procedure print_log(p_message in varchar2) is
 begin
     if  fnd_global.conc_request_id >0 then
      fnd_file.put_line(fnd_file.log, p_message);
     else
      dbms_output.put_line(p_message);
     end if;
 end print_log;
 --
 /*
 *************************************************************************
 *
 * FUNCTION: loc_exists
 * DESCRIPTION: Procedure to display debug log messages
 * PARAMETERS
 * ==========
 * NAME              TYPE     DESCRIPTION
.* ----------------- -------- ---------------------------------------------
 * p_locn_code       IN      Input location code
 * RETURN VALUE: NA or OK. to indicate if it's an existing location or a new one
 * History
 * =======
 -- ESMS         RFC       Date           Version   Comments
 -- =========== =========  ============   ========  =====================================================================
 -- 264411                 10-DEC-2014    1.0       Initial version
 */  
 function loc_exists
   (
     p_locn_code in varchar2
    ,p_opn_id    in varchar2
    ,p_loc_id    in varchar2
    ,p_fru       in varchar2
    ,p_loc_start in varchar2
    ,p_loc_end   in varchar2
   ) return varchar2 is
  --
 begin 
  --
  select *
  into   g_locn_rec
  from   hr_locations_all
  where  1 =1
    and  location_code =p_locn_code --Ver 1.2
    -- Begin Ver 1.2
--    and  attribute1 =p_opn_id 
--    and  attribute2 =p_loc_id
--    and  attribute3 =p_fru
--    and  attribute4 =p_loc_start
--    and  attribute5 =p_loc_end
    -- End Ver 1.2
    ;
    --and  location_code =p_locn_code;
  --   
    return 'EXISTING';
  -- 
 exception
  when no_data_found then
   return 'NEW';
  when too_many_rows then --Ver 1.2
   print_log('@loc_exists, too many rows error, location_code ='||p_locn_code||' ,msg ='||sqlerrm); --Ver 1.2
   return 'ERR';  --Ver 1.2
  when others then
   print_log('@loc_exists, other error, location_code ='||p_locn_code||' ,msg ='||sqlerrm);
   return 'ERR';
 end loc_exists;
 --
 /*
 *************************************************************************
 *
 * procedure: inactivate_locn
 * DESCRIPTION: Procedure to just inactivate a location
 * PARAMETERS: Multiple
 * ==========
 * RETURN VALUE: None
 * History
 * =======
 -- ESMS         RFC       Date           Version   Comments
 -- =========== =========  ============   ========  =====================================================================
 -- 264411                 10-DEC-2014    1.0       Initial version
 */  
 procedure inactivate_locn 
   (
     p_locn_id     in number
    ,p_locn_code   in varchar2
    ,p_obj_ver_num in number                
   ) is
  --
  l_object_version_number NUMBER :=p_obj_ver_num;
  --
  l_eff_date date :=trunc(sysdate);
  l_inactive_date date :=Null;
  --
 begin 
  --
  SAVEPOINT init_here;
  --
   l_inactive_date :=trunc(sysdate);
  --
  apps.hr_location_api.update_location
    (
        p_effective_date        =>l_eff_date,
        p_location_id           =>p_locn_id,
        p_inactive_date         =>l_inactive_date,
        p_object_version_number =>l_object_version_number
    );  
  --
  if nvl(l_object_version_number, 0) >p_obj_ver_num then 
   print_output('Successfully inactivated location: '||p_locn_code);
  else
   print_output('Failed to inactivate location: '||p_locn_code);  
  end if;
  --
 exception
  when others then
   print_log('@inactivate_locn, location_code ='||p_locn_code||', location id ='||p_locn_id||', msg ='||sqlerrm);
   rollback to init_here;
 end inactivate_locn;
 --  
 /*
 *************************************************************************
 *
 * procedure: modify_locn
 * DESCRIPTION: Procedure to display debug log messages
 * PARAMETERS: Multiple
 * ==========
 * RETURN VALUE: None
 * History
 * =======
 -- ESMS         RFC       Date           Version   Comments
 -- =========== =========  ============   ========  =====================================================================
 -- 264411                 10-DEC-2014    1.0       Initial version
 */  
 procedure modify_locn 
   (
     p_locn_id    in number
    ,p_locn_code  in varchar2
    ,p_locn_desc  in varchar2 
    ,p_addr_line1 in varchar2
    ,p_addr_line2 in varchar2
    ,p_city       in varchar2
    ,p_state_prv  in varchar2
    ,p_county     in varchar2
    ,p_zipcode    in varchar2
    ,p_country    in varchar2
    ,p_opn_id     in varchar2
    ,p_opn_loc_id in varchar2
    ,p_fru        in varchar2
    ,p_loc_start  in varchar2           
    ,p_loc_end    in varchar2
    ,p_inactive   in varchar2
    ,p_obj_ver_num in number                
   ) is
  --
  l_location_id NUMBER;
  --
  l_object_version_number NUMBER :=p_obj_ver_num;
  --
  l_eff_date date :=trunc(sysdate);
  l_inactive_date date :=Null;
  --
  l_style    hr_locations_all.style%type :=Null;
  --
  l_region_1 hr_locations_all.region_1%type :=Null;
  l_region_2 hr_locations_all.region_2%type :=Null;  
  --
 begin 
  --
  SAVEPOINT init_here; 
  --
  if p_country ='US' then 
   l_region_1 :=p_county; 
   l_region_2 :=p_state_prv;   
  elsif p_country ='CA' then 
   l_region_1 :=p_state_prv;
   l_region_2 :=Null;      
  else
   l_region_1 :=Null;
   l_region_2 :=Null;  
  end if;
  --
  if (p_inactive ='Y') then
   l_inactive_date :=trunc(sysdate);
  else
   l_inactive_date :=Null;  
  end if;
  --
  apps.hr_location_api.update_location
    (
        p_effective_date        =>l_eff_date,
        p_location_id           =>p_locn_id,
        p_location_code         =>p_locn_code,
        p_description           =>p_locn_desc,        
        p_inactive_date         =>l_inactive_date,
        p_address_line_1        =>p_addr_line1,
        p_address_line_2        =>p_addr_line2,
        p_town_or_city          =>p_city,   
        p_region_1              =>l_region_1,        
        p_region_2              =>l_region_2,        
        p_postal_code           =>p_zipcode,
        p_country               =>p_country,
        p_attribute1            =>p_opn_id,
        p_attribute2            =>p_opn_loc_id,
        p_attribute3            =>p_fru,
        p_attribute4            =>p_loc_start,
        p_attribute5            =>p_loc_end,
        p_object_version_number =>l_object_version_number
    );  
  --
  if nvl(l_object_version_number, 0) >p_obj_ver_num then 
   print_output('Updated location :'||p_locn_code||' and its attributes successfully.');
  else
   print_output('Failed to update location :'||p_locn_code);  
  end if;
  --
 exception
  when others then
   print_log('@modify_locn, location_code ='||p_locn_code||', location id ='||p_locn_id||', msg ='||sqlerrm);
   rollback to init_here;
 end modify_locn;
 --  
 /*
 *************************************************************************
 *
 * procedure: create_locn
 * DESCRIPTION: Procedure to display debug log messages
 * PARAMETERS: Multiple
 * ==========
 * RETURN VALUE: None
 * History
 * =======
 -- ESMS         RFC       Date           Version   Comments
 -- =========== =========  ============   ========  =====================================================================
 -- 264411                 10-DEC-2014    1.0       Initial version
 */  
 procedure create_locn 
   (
     p_locn_code  in varchar2
    ,p_locn_desc  in varchar2 
    ,p_addr_line1 in varchar2
    ,p_addr_line2 in varchar2
    ,p_city       in varchar2
    ,p_state_prv  in varchar2
    ,p_county     in varchar2
    ,p_zipcode    in varchar2
    ,p_country    in varchar2
    ,p_opn_id     in varchar2
    ,p_locn_id    in varchar2
    ,p_fru        in varchar2
    ,p_loc_start  in varchar2           
    ,p_loc_end    in varchar2 
    ,p_hr_locn_id       out number
    ,p_hr_locn_obj_ver  out number               
   ) is
  --
  l_location_id NUMBER;
  --
  l_eff_date date :=trunc(sysdate);
  l_object_version_number NUMBER;
  --
  l_org_id number :=mo_global.get_current_org_id;  
  --
  l_style    hr_locations_all.style%type :=Null;
  --
  l_region_1 hr_locations_all.region_1%type :=Null;
  l_region_2 hr_locations_all.region_2%type :=Null;  
  --
 begin 
  --
  SAVEPOINT init_here; 
  --
  if p_country ='US' then
   l_style :='US_GLB'; 
   l_region_1 :=p_county; 
   l_region_2 :=p_state_prv;   
  elsif p_country ='CA' then 
   l_style :='CA_GLB';
   l_region_1 :=p_state_prv;
   l_region_2 :=Null;      
  else
   l_region_1 :=Null;
   l_region_2 :=Null;  
   l_style :=Null;
  end if;
  --
  --print_log('Pass1');
  --
  apps.hr_location_api.create_location
    (
        p_effective_date        =>l_eff_date,
        p_location_code         =>p_locn_code,
        p_description           =>p_locn_desc,
        p_address_line_1        =>p_addr_line1,
        p_address_line_2        =>p_addr_line2,
        p_town_or_city          =>p_city,   
        p_region_1              =>l_region_1,        
        p_region_2              =>l_region_2,        
        p_postal_code           =>p_zipcode,
        p_country               =>p_country,
        p_style                 =>l_style,
        p_attribute_category    =>l_org_id,
        p_attribute1            =>p_opn_id,
        p_attribute2            =>p_locn_id,
        p_attribute3            =>p_fru,
        p_attribute4            =>p_loc_start,
        p_attribute5            =>p_loc_end,                                
        p_location_id           =>l_location_id,
        p_object_version_number =>l_object_version_number
    );
  --
  --print_log('Pass2');        
  --
  p_hr_locn_id       :=l_location_id;
  p_hr_locn_obj_ver  :=l_object_version_number;  
  --
  --print_log('Pass3');
  --  
  if nvl(l_location_id, 0) >0 then 
   print_output('Created location :'||p_locn_code||' successfully');
  else
   print_output('Failed to create location :'||p_locn_code);  
  end if;
  --
  --print_log('Pass4');
  --
 exception
  when others then
   print_log('@create_locn, location_code ='||p_locn_code||', msg ='||sqlerrm);
   rollback to init_here;
 end create_locn;
 --  
 /*
 *************************************************************************
 *
 * procedure: main
 * DESCRIPTION: Procedure to import OPN locations as HR Locations.
 * PARAMETERS
 * ==========
 * NAME              TYPE     DESCRIPTION
.* ----------------- -------- ---------------------------------------------
 * retcode           OUT    standard concurrent program return variable
 * errbuf            OUT    standard concurrent program return variable
 * RETURN VALUE: Mentioned above
 * History
 * =======
 -- ESMS         RFC       Date           Version   Comments
 -- =========== =========  ============   ========  =====================================================================
 -- 264411                 10-DEC-2014    1.0       Initial version
 */ 
 procedure main 
   (
      retcode          out varchar2
     ,errbuf           out varchar2
   ) as
  --  
  cursor all_opn_locn is
    select a.opn_code              opn_code
          ,a.lc_fru                lc_fru
          ,a.location_id           location_id
          ,a.location_code         location_code
          ,a.opn_id                opn_id
          ,a.loc_active_start_date loc_start
          ,a.loc_active_end_date   loc_end
          ,b.address_line1         address_line1
          ,b.address_line2         address_line2
          ,b.property_name         property_name
          ,b.city                  city
          ,b.county                county
          ,b.state_prv             state_prv
          ,b.postal_code           zipcode
          ,b.country_code          country_code
    from xxcus.xxcus_opn_fru_loc_all a
        ,( 
            select address_line1
                  ,address_line2
                  ,city
                  ,county
                  ,hds_st_prv state_prv
                  ,postal_code
                  ,property_name
                  ,opn_id
                  ,location_id
                  ,loc_active_start_date
                  ,loc_active_end_date
                  ,(
                    select territory_code
                    from   fnd_territories_tl
                    where  1 =1
                      and  territory_short_name =address_country
                   )                       country_code                  
            from   xxcus.xxcus_opn_properties_all
            where  1 =1
              and  opn_type ='PROPERTY'
              and  location_type_lookup_code NOT IN ('LAND', 'PARCEL', 'SECTION')
              --and  loc_active_end_date >=trunc(sysdate) --Ver1.2
              and  nvl(loc_active_end_date, trunc(sysdate)) >=trunc(sysdate) --Ver 1.2 
              and  hds_current_flag ='Y'   
         ) b
    where 1 =1
    and a.loc_active_end_date >=trunc(sysdate)
    and a.hds_fru_section_current_flag ='Y'
    and a.hds_pri_fru_flag ='Y'
    and a.bldg_land_type ='BUILDING'
    and a.floor_parcel_type ='FLOOR'
    and b.opn_id =a.opn_id
    and b.location_id =a.location_id
    and b.loc_active_start_date =a.loc_active_start_date
    --and b.loc_active_end_date =a.loc_active_end_date --Ver 1.2 
    and nvl(b.loc_active_end_date, to_date('31-DEC-4712','DD-MON-YYYY')) =a.loc_active_end_date --Ver 1.2    
    and not exists -- Begin Ver 1.2
       (
          select '1'
          from   apps.hr_locations_all  c
          where  1 =1
               and
                   substr(a.opn_code||'-'||a.lc_fru|| '-' || b.address_line1 || '-' ||
                   substr(b.property_name, 1, 2) || '-' || b.city || '-' ||b.postal_code  ,1 ,60) =c.location_code    --we are pulling only the first 60 characters bcoz hr_locations.location_code has a max of 60 chars.
                 and c.business_group_id is null    --Check scope at "Global" level   
       )    --End Ver 1.2
    group by
           a.opn_code
          ,a.lc_fru
          ,a.location_id
          ,a.location_code
          ,a.opn_id
          ,a.loc_active_start_date
          ,a.loc_active_end_date
          ,b.address_line1
          ,b.address_line2
          ,b.property_name
          ,b.city
          ,b.county
          ,b.state_prv
          ,b.postal_code
          ,b.country_code       
    order by opn_code, location_code;                  
  --
  v_loc_exists varchar2(10) :=Null;
  --
  v_hds_name hr_locations.location_code%type :=Null;
  --
  n_obj_ver_num number :=Null; 
  --
  l_hr_locn_id       NUMBER;
  l_hr_locn_obj_ver  NUMBER;
  l_inactive_flag    VARCHAR2(1) :=Null;
  --
  l_module        VARCHAR2(24) :='GSC PO';
  l_err_msg       CLOB;
  l_sec           VARCHAR2(255);
  --
  l_err_callfrom  VARCHAR2(75) DEFAULT 'apps.xxcus_gsc_po_import_locn.main';
  l_err_callpoint VARCHAR2(75) DEFAULT 'START';
  l_distro_list   VARCHAR2(75) DEFAULT 'HDSOracleDevelopers@hdsupply.com';     
  -- end of variable declarations and cursor definitions    
 begin 
  --
  for rec in all_opn_locn loop 
   --
   -- reset the hr_locations_all row type variable to store the next record information
   --
   v_hds_name   :=Null;
   --
   v_loc_exists :=Null;
   --
   g_locn_rec   :=Null; 
   --
   begin 
    --
    v_hds_name :=substr
                 (
                   rec.opn_code||'-'||rec.lc_fru|| '-' || rec.address_line1 || '-' ||
                   substr(rec.property_name, 1, 2) || '-' || rec.city || '-' ||rec.zipcode
                  ,1
                  ,60 --we are pulling only the first 60 characters bcoz hr_locations.location_code has a max of 60 chars.
                 );
    --
    v_loc_exists :=loc_exists
                       (
                         p_locn_code =>v_hds_name --in varchar2
                        ,p_opn_id    =>to_char(rec.opn_id) --in varchar2
                        ,p_loc_id    =>to_char(rec.location_id) --in varchar2
                        ,p_fru       =>rec.lc_fru --in varchar2
                        ,p_loc_start =>to_char(rec.loc_start, 'mm/dd/yyyy')  --in varchar2
                        ,p_loc_end   =>to_char(rec.loc_end, 'mm/dd/yyyy')  --in varchar2
                       );    
    --
    if (v_loc_exists ='NEW') then
     --
     create_locn 
       (
         p_locn_code        =>v_hds_name --in varchar2
        ,p_locn_desc        =>v_hds_name --in varchar2
        ,p_addr_line1       =>rec.address_line1 --in varchar2
        ,p_addr_line2       =>rec.address_line2 --in varchar2
        ,p_city             =>rec.city --in varchar2
        ,p_state_prv        =>rec.state_prv --in varchar2
        ,p_county           =>rec.county --in varchar2
        ,p_zipcode          =>rec.zipcode --in varchar2
        ,p_country          =>rec.country_code --in varchar2
        ,p_opn_id           =>rec.opn_id --in varchar2
        ,p_locn_id          =>rec.location_id --in varchar2
        ,p_fru              =>rec.lc_fru --in varchar2
        ,p_loc_start        =>to_char(rec.loc_start, 'mm/dd/yyyy') --in varchar2            
        ,p_loc_end          =>to_char(rec.loc_end, 'mm/dd/yyyy')  --in varchar2
        ,p_hr_locn_id       =>l_hr_locn_id
        ,p_hr_locn_obj_ver  =>l_hr_locn_obj_ver
       );
     --         
    elsif (v_loc_exists ='EXISTING') then
     --
     -- g_locn_rec.object_version_number already has the latest object version number as we  have queried the record into a rowtype variable
     -- g_locn_rec.location_id already has the location_id as we as we  have queried the record into a rowtype variable
     --
     n_obj_ver_num :=g_locn_rec.object_version_number;
     --
     -- check if the location is not already inactive
     --
     if (g_locn_rec.inactive_date is not null) then 
      --
      print_output('Location :'||v_hds_name||' is already inactive. Skip.');       
      --
     else
      --
      if rec.loc_end <=trunc(sysdate) then
       --
       l_inactive_flag :='Y';
       --
      else
       --
       l_inactive_flag :=Null;
       --
      end if;
      --
        modify_locn 
           (
             p_locn_id     =>g_locn_rec.location_id --in number
            ,p_locn_code   =>v_hds_name --in varchar2 
            ,p_locn_desc   =>v_hds_name --in varchar2
            ,p_addr_line1  =>rec.address_line1 --in varchar2
            ,p_addr_line2  =>rec.address_line2 --in varchar2
            ,p_city        =>rec.city --in varchar2
            ,p_state_prv   =>rec.state_prv --in varchar2
            ,p_county      =>rec.county --in varchar2
            ,p_zipcode     =>rec.zipcode --in varchar2
            ,p_country     =>rec.country_code --in varchar2
            ,p_opn_id      =>rec.opn_id --in varchar2
            ,p_opn_loc_id  =>rec.location_id --in varchar2
            ,p_fru         =>rec.lc_fru --in varchar2
            ,p_loc_start   =>to_char(rec.loc_start, 'mm/dd/yyyy') --in varchar2           
            ,p_loc_end     =>to_char(rec.loc_end, 'mm/dd/yyyy') --in varchar2
            ,p_inactive    =>l_inactive_flag --in varchar2 either Y or N 
            ,p_obj_ver_num =>n_obj_ver_num --in number                
           );      
      --
     end if;
     --
    else
     print_log('Unknown Action: Not a New or Existing, v_loc_exists ='||v_loc_exists||', locn_code ='||v_hds_name);
    end if; 
    --
   exception
    when others then
     print_log('in block, routine: loc_exists, location_code ='||v_hds_name||', msg ='||sqlerrm);
   end;
   --
  end loop;
  --
 exception
  when others then
   print_log('Error in xxcus_gsc_po_import_locn.main, message ='||sqlerrm);
    l_err_msg := l_sec;
        l_err_msg := l_err_msg || ' ...Error_Stack...' ||
                     dbms_utility.format_error_stack() || ' Error_Backtrace...' ||
                     dbms_utility.format_error_backtrace();
        fnd_file.put_line(fnd_file.log, l_err_msg);
        l_err_callpoint := l_sec;
        xxcus_error_pkg.xxcus_error_main_api(p_called_from       => l_err_callfrom
                                            ,p_calling           => l_err_callpoint
                                            ,p_request_id        => fnd_global.conc_request_id
                                            ,p_ora_error_msg     => substr(' Error_Stack...' ||
                                                                           dbms_utility.format_error_stack() ||
                                                                           ' Error_Backtrace...' ||
                                                                           dbms_utility.format_error_backtrace()
                                                                          ,1
                                                                          ,2000)
                                            ,p_error_desc        => substr(l_err_msg
                                                                          ,1
                                                                          ,240)
                                            ,p_distribution_list => l_distro_list
                                            ,p_module            => l_module);
 end main;
 --
 --
end xxcus_gsc_po_import_locn;
/ 