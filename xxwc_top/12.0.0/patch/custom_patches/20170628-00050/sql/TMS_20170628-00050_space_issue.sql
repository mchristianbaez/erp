/* ************************************************************************
  $Header TMS_20170628-00050_space_issue.sql $
  Module Name: TMS_20170628-00050 Data Fix script

  PURPOSE: Data Fix script 

  REVISIONS:
  Ver        Date         Author                Description
  ---------  -----------  ------------------    --------------------------
  1.0        07-JUL-2017  Pattabhi Avula        TMS#20170628-00050

************************************************************************* */ 
SET SERVEROUTPUT ON SIZE 1000000;
DECLARE
BEGIN
   DBMS_OUTPUT.put_line ('TMS: 20170628-00050   , Before Update');
   
UPDATE po_lines_all
SET vendor_product_num = TRIM (VENDOR_PRODUCT_NUM)
WHERE po_line_id IN (7860807, 7860808);

 DBMS_OUTPUT.put_line (
         'TMS: 20170628-00050 Sales order lines updated (Expected:2): '
      || SQL%ROWCOUNT);

   COMMIT;

   DBMS_OUTPUT.put_line ('TMS: 20170628-00050   , End Update');
EXCEPTION
   WHEN OTHERS
   THEN
      ROLLBACK;
      DBMS_OUTPUT.put_line ('TMS: 20170628-00050, Errors : ' || SQLERRM);
END;
/