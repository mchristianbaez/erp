drop materialized view APPS.XXWC_OB_CT_ORD_LN_MV;

CREATE MATERIALIZED VIEW APPS.XXWC_OB_CT_ORD_LN_MV (ACTUAL_SHIPMENT_DATE,ATTRIBUTE5,ATTRIBUTE7,CUST_PO_NUMBER,HEADER_ID,INVENTORY_ITEM_ID,LAST_UPDATE_DATE,LINE_CATEGORY_CODE,LINE_ID,LINK_TO_LINE_ID,REFERENCE_LINE_ID,ORDERED_QUANTITY,PRICING_DATE,SHIP_FROM_ORG_ID,SHIPPING_METHOD_CODE,SOURCE_TYPE_CODE,UNIT_COST,UNIT_LIST_PRICE,HAS_DROPSHIP_CNT,ORDER_TYPE_ID,MSIB_ITEM_NUMBER,MSIB_ITEM_DESCRIPTION,MSIB_INVENTORY_ITEM_ID)
TABLESPACE APPS_TS_TX_DATA
NOCACHE
NOCOMPRESS
BUILD IMMEDIATE
REFRESH COMPLETE ON DEMAND
WITH PRIMARY KEY
AS 
/*************************************************************************
 Copyright (c) 2016 HD Supply
 All rights reserved.
**************************************************************************
   $Header XXWC_OB_CT_ORD_LN_MV$
  Module Name: XXWC_OB_CT_ORD_LN_MV

  PURPOSE:
 
  sub-MV used by APPS.XXWC_OB_CUST_TRX_INCR_MV
  
   This MV will be refreshed before the main query in
   XXWC_OB_CUST_TRX_INCR_MV
    
         

  REVISIONS:
  
   VERSION  DATE        AUTHOR(S)        TMS TASK          DESCRIPTION
   -------  ----------  ---------------  ---------------   ----------------------------------------------
   1.0      2016-07-26  Steve Moffa      20160720-00055    Initially Created 
 
**************************************************************************/
WITH order_lines
     AS 
       (SELECT /*+ qb_name(ord_ln) materialize */
              l1.actual_shipment_date
              ,l1.attribute5
              ,l1.attribute7
              ,l1.cust_po_number
              ,l1.header_id
              ,l1.inventory_item_id
              ,l1.last_update_date
              ,l1.line_category_code
              ,l1.line_id
              ,NVL (l1.link_to_line_id, l2.link_to_line_id) link_to_line_id
              ,l1.reference_line_id
              ,l1.ordered_quantity
              ,l1.org_id
              ,l1.pricing_date
              ,l1.ship_from_org_id
              ,l1.shipping_method_code
              ,l1.source_type_code
              ,l1.unit_cost
              ,l1.unit_list_price
              ,CASE
                 WHEN     NVL (l1.booked_flag, 'N') = 'Y' 
                      AND NVL (l1.cancelled_flag, 'N') = 'N'
                      AND l1.source_type_code = 'EXTERNAL'
                 THEN
                   'Y'
                 ELSE
                   'N'
               END
                 has_dropship_cnt
          FROM ont.oe_order_lines_all l1
               LEFT OUTER JOIN (SELECT t.line_id
                                      ,t.link_to_line_id
                                  FROM ont.oe_order_lines_all t
                                 WHERE TRUNC (t.creation_date) > TRUNC (SYSDATE)  - 180) l2 -- For rental returns 
                 ON l1.reference_line_id = l2.line_id
where l1.line_id in (select INTERFACE_LINE_ATTRIBUTE6 from APPS.XXWC_OB_CT_CTL_MV WHERE trim(TRANSLATE(INTERFACE_LINE_ATTRIBUTE6,'0123456789', ' ')) is null) 
       )
SELECT l.actual_shipment_date
      ,l.attribute5
      ,l.attribute7
      ,l.cust_po_number
      ,l.header_id
      ,l.inventory_item_id
      ,l.last_update_date
      ,l.line_category_code
      ,l.line_id
      ,l.link_to_line_id
      ,l.reference_line_id
      ,l.ordered_quantity
      ,l.pricing_date
      ,l.ship_from_org_id
      ,l.shipping_method_code
      ,l.source_type_code
      ,l.unit_cost
      ,l.unit_list_price
      ,l.has_dropship_cnt
      ,h.order_type_id
      ,msib.segment1 AS msib_item_number
      ,msib.description AS msib_item_description
      ,msib.inventory_item_id AS msib_inventory_item_id
  FROM order_lines l
       INNER JOIN ont.oe_order_headers_all h
         ON     l.header_id = h.header_id
            AND l.org_id = h.org_id
       LEFT OUTER JOIN ont.oe_order_lines_all rma_ln
         ON     l.link_to_line_id = rma_ln.line_id
            AND rma_ln.line_category_code = 'RETURN'
       LEFT OUTER JOIN ont.oe_order_lines_all shp_ln
         ON     rma_ln.link_to_line_id = shp_ln.line_id
            AND shp_ln.line_category_code = 'ORDER'
       LEFT OUTER JOIN inv.mtl_system_items_b msib
         ON     shp_ln.inventory_item_id = msib.inventory_item_id
            AND shp_ln.ship_from_org_id = msib.organization_id;

CREATE INDEX APPS.XXWC_OB_CT_ORD_LN_MV_N1 ON APPS.XXWC_OB_CT_ORD_LN_MV
(LINE_ID)
TABLESPACE APPS_TS_TX_IDX;

COMMENT ON MATERIALIZED VIEW APPS.XXWC_OB_CT_ORD_LN_MV IS 'snapshot table for snapshot APPS.XXWC_OB_CUST_TRX_INCR_MV';

BEGIN
  SYS.DBMS_STATS.GATHER_TABLE_STATS (
     OwnName           => 'APPS'
    ,TabName           => 'XXWC_OB_CT_ORD_LN_MV'
    ,Estimate_Percent  => 100
    ,Method_Opt        => 'FOR ALL COLUMNS SIZE SKEWONLY'
    ,Degree            => NULL
    ,Cascade           => TRUE
    ,No_Invalidate  => FALSE);
END;
/