DROP MATERIALIZED VIEW   APPS.XXWC_OB_CT_PO_PRICE_MV;

CREATE MATERIALIZED VIEW APPS.XXWC_OB_CT_PO_PRICE_MV (LAST_PO_PRICE,ITEM_ID,TO_ORGANIZATION_ID,SHIPMENT_HEADER_ID,SHIPMENT_LINE_ID,PO_HEADER_ID,PO_LINE_ID,PO_LINE_LOCATION_ID,TRANSACTION_DATE)
TABLESPACE APPS_TS_TX_DATA
NOCACHE
NOCOMPRESS
BUILD IMMEDIATE
REFRESH COMPLETE ON DEMAND
WITH PRIMARY KEY
AS 
/*************************************************************************
 Copyright (c) 2016 HD Supply
 All rights reserved.
**************************************************************************
   $Header XXWC_OB_CT_PO_PRICE_MV$
  Module Name: XXWC_OB_CT_PO_PRICE_MV

  PURPOSE:
 
  sub-MV used by APPS.XXWC_OB_CUST_TRX_INCR_MV
    -- replacement for call to
    --  apps.xxwc_mv_routines_pkg.get_last_po_price
    --
    -- This MV will be refreshed before the main query in
    -- XXWC_OB_CUST_TRX_INCR_MV
    --
    -- 10/02/14: CG: Updated to sort and select by last transaction_id instead of transaction_date
                    The Sub MV created to calculate the last PO price for all items was partitioning
                    and ordering by transaction_date and selecting the last one. We need to change
                    that to use the receiving transaction_id instead, since there is a chance of
                    two POs for the same item/org being received simultaneously and the values on
                    the PO are different (yes, I ran into that issue while testing the MV and the
                    function). By sorting and using the receiving transaction_id we guarantee
                    pulling the very last entry for that item/org combination
 
  

  REVISIONS:
  
   VERSION  DATE        AUTHOR(S)        TMS TASK          DESCRIPTION
   -------  ----------  ---------------  ---------------   ----------------------------------------------
   1.0      2016-07-26  Steve Moffa      20160720-00055    Initially Created 
   
 
**************************************************************************/
SELECT
      last_po_price
      ,item_id
      ,to_organization_id
      ,shipment_header_id
      ,shipment_line_id
      ,po_header_id
      ,po_line_id
      ,po_line_location_id
      ,transaction_date
  FROM (SELECT pol.unit_price AS last_po_price
              ,rsl.item_id
              ,rsl.to_organization_id
              ,rt.shipment_header_id
              ,rt.shipment_line_id
              ,rt.po_header_id
              ,rt.po_line_id
              ,rt.po_line_location_id
              ,rt.transaction_date
              ,ROW_NUMBER ()
                 OVER (PARTITION BY rsl.item_id, rsl.to_organization_id
                                   ORDER BY rt.transaction_id DESC)
                 AS rn
          FROM po.rcv_transactions rt
              ,po.rcv_shipment_headers rsh
              ,po.rcv_shipment_lines rsl
              ,po.po_headers_all poh
              ,po.po_lines_all pol
              ,po.po_line_locations_all poll
         WHERE     rsh.shipment_header_id = rt.shipment_header_id
               AND rsl.shipment_line_id = rt.shipment_line_id
               AND rt.transaction_type = 'DELIVER'
               AND poh.po_header_id = rt.po_header_id
               AND pol.po_line_id = rt.po_line_id
               AND poll.line_location_id = rt.po_line_location_id)
 WHERE rn = 1;

COMMENT ON MATERIALIZED VIEW APPS.XXWC_OB_CT_PO_PRICE_MV IS 'snapshot table for snapshot APPS.XXWC_OB_CT_PO_PRICE_MV';

BEGIN
  SYS.DBMS_STATS.GATHER_TABLE_STATS (
     OwnName           => 'APPS'
    ,TabName           => 'XXWC_OB_CT_PO_PRICE_MV'
    ,Estimate_Percent  => 100
    ,Method_Opt        => 'FOR ALL COLUMNS SIZE SKEWONLY'
    ,Degree            => NULL
    ,Cascade           => TRUE
    ,No_Invalidate  => FALSE);
END;
/