/***********************************************************************************************************************************************
   NAME:       TMS_20180207-00091_Datafix.sql
   PURPOSE:  Data fix

   REVISIONS:
   Ver        Date        Author           Description
   ---------  ----------  ---------------  ----------------------------------------------------------------------------------------------------
   1.0        02/17/2018  Rakesh Patel     TMS#20180207-00091- reprocess the error records
************************************************************************************************************************************************/
SET SERVEROUTPUT ON SIZE 1000000

BEGIN
   DBMS_OUTPUT.put_line ('Before LOOP');
   
   UPDATE xxwc.XXWC_OM_DMS2_SIGN_CAPTURE_TBL
      SET SHIP_CONFIRM_STATUS = NULL 
	      ,LAST_UPDATED_BY     = -1
	      ,LAST_UPDATE_DATE    = SYSDATE
	 WHERE ORDER_NUMBER IN ( '27395266' , '27385208', '27378465', '27390752', '27389749', '27396209', '27362640', '27387435'
	 ,'27395971','27395718','27395201','27380152'
	 );
   
   DBMS_OUTPUT.put_line ('Records Updated -' || SQL%ROWCOUNT);
   COMMIT;
   
  EXCEPTION
   WHEN OTHERS
   THEN
      DBMS_OUTPUT.put_line ('Unable to update record ' || SQLERRM);
	  ROLLBACK;
END;
/