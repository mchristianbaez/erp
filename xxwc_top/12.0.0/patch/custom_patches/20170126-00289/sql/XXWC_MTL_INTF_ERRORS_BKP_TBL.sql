      /**********************************************************************************************
      -- PROCEDURE: XXWC_MTL_INTF_ERRORS_BKP_TBL
      --
      -- PURPOSE: Inserting the data into XXWC_MTL_INTF_ERRORS_BKP_TBL table while purging the data
	              from mtl_interface_errors table.
      -- HISTORY
      -- ============================================================================================
      -- ============================================================================================
      -- VERSION DATE          AUTHOR(S)       DESCRIPTION
      -- ------- -----------   --------------- ------------------------------------------------------
      -- 1.0     09-Feb-2017   Pattabhi Avula    Created this Table - TMS#20170126-00289
      
      ***********************************************************************************************/
	  CREATE TABLE XXWC.XXWC_MTL_INTF_ERRORS_BKP_TBL(
            ORGANIZATION_ID                 NUMBER(20),       
            TRANSACTION_ID                  NUMBER(20),        
            UNIQUE_ID                       NUMBER(20),     
            LAST_UPDATE_DATE                DATE,          
            LAST_UPDATED_BY                 NUMBER(20),         
            CREATION_DATE                   DATE,           
            CREATED_BY                      NUMBER(20),         
            LAST_UPDATE_LOGIN               NUMBER(20),         
            TABLE_NAME                      VARCHAR2(30),   
            MESSAGE_NAME                    VARCHAR2(30),   
            COLUMN_NAME                     VARCHAR2(32),   
            REQUEST_ID                      NUMBER(20),         
            PROGRAM_APPLICATION_ID          NUMBER(20),         
            PROGRAM_ID                      NUMBER(20),         
            PROGRAM_UPDATE_DATE             DATE,           
            ERROR_MESSAGE                   VARCHAR2(2000), 
            ENTITY_IDENTIFIER               VARCHAR2(30),   
            BO_IDENTIFIER                   VARCHAR2(30),   
            MESSAGE_TYPE                    VARCHAR2(1),
            INSERT_DATE                     DATE)
/