--Report Name            : Invoice Pre-Register report
--Import Instructions    : Run this file in XXEIS user from SQL*Plus or any tool like Toad, SQL Navigator. 
prompt Creating View Data for Invoice Pre-Register report
set scan off define off
DECLARE
BEGIN 
--Inserting View EIS_XXWC_OM_INV_PRE_REG_V
xxeis.eis_rs_ins.v( 'EIS_XXWC_OM_INV_PRE_REG_V',660,'','','','','SA059956','XXEIS','Eis Xxwc Om Inv Pre Reg V','EXOIPRV','','');
--Delete View Columns for EIS_XXWC_OM_INV_PRE_REG_V
xxeis.eis_rs_utility.delete_view_rows('EIS_XXWC_OM_INV_PRE_REG_V',660,FALSE);
--Inserting View Columns for EIS_XXWC_OM_INV_PRE_REG_V
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_INV_PRE_REG_V','DISCOUNT_AMT',660,'Discount Amt','DISCOUNT_AMT','','','','SA059956','NUMBER','','','Discount Amt','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_INV_PRE_REG_V','TAX_VALUE',660,'Tax Value','TAX_VALUE','','','','SA059956','NUMBER','','','Tax Value','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_INV_PRE_REG_V','GROSS_AMOUNT',660,'Gross Amount','GROSS_AMOUNT','','','','SA059956','NUMBER','','','Gross Amount','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_INV_PRE_REG_V','CUSTOMER_NUMBER',660,'Customer Number','CUSTOMER_NUMBER','','','','SA059956','VARCHAR2','','','Customer Number','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_INV_PRE_REG_V','CUSTOMER_NAME',660,'Customer Name','CUSTOMER_NAME','','','','SA059956','VARCHAR2','','','Customer Name','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_INV_PRE_REG_V','ORDER_STATUS',660,'Order Status','ORDER_STATUS','','','','SA059956','VARCHAR2','','','Order Status','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_INV_PRE_REG_V','ORDERED_DATE',660,'Ordered Date','ORDERED_DATE','','','','SA059956','DATE','','','Ordered Date','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_INV_PRE_REG_V','ORDER_NUMBER',660,'Order Number','ORDER_NUMBER','','','','SA059956','NUMBER','','','Order Number','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_INV_PRE_REG_V','WAREHOUSE',660,'Warehouse','WAREHOUSE','','','','SA059956','VARCHAR2','','','Warehouse','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_INV_PRE_REG_V','AVERAGE_COST',660,'Average Cost','AVERAGE_COST','','','','SA059956','NUMBER','','','Average Cost','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_INV_PRE_REG_V','PROFIT',660,'Profit','PROFIT','','','','SA059956','NUMBER','','','Profit','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_INV_PRE_REG_V','ORDER_TYPE',660,'Order Type','ORDER_TYPE','','','','SA059956','VARCHAR2','','','Order Type','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_INV_PRE_REG_V','CREATED_BY',660,'Created By','CREATED_BY','','','','SA059956','VARCHAR2','','','Created By','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_INV_PRE_REG_V','SALESREP_NAME',660,'Salesrep Name','SALESREP_NAME','','','','SA059956','VARCHAR2','','','Salesrep Name','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_INV_PRE_REG_V','TRX_NUMBER',660,'Trx Number','TRX_NUMBER','','','','SA059956','VARCHAR2','','','Trx Number','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_INV_PRE_REG_V','PAYMENT_TERM',660,'Payment Term','PAYMENT_TERM','','','','SA059956','VARCHAR2','','','Payment Term','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_INV_PRE_REG_V','SALE_COST',660,'Sale Cost','SALE_COST','','','','SA059956','NUMBER','','','Sale Cost','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_INV_PRE_REG_V','ITEM',660,'Item','ITEM','','','','SA059956','VARCHAR2','','','Item','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_INV_PRE_REG_V','ITEM_DESC',660,'Item Desc','ITEM_DESC','','','','SA059956','VARCHAR2','','','Item Desc','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_INV_PRE_REG_V','QTY',660,'Qty','QTY','','','','SA059956','NUMBER','','','Qty','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_INV_PRE_REG_V','UNIT_SELLING_PRICE',660,'Unit Selling Price','UNIT_SELLING_PRICE','','','','SA059956','NUMBER','','','Unit Selling Price','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_INV_PRE_REG_V','ORDER_LINE',660,'Order Line','ORDER_LINE','','','','SA059956','VARCHAR2','','','Order Line','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_INV_PRE_REG_V','INVOICE_DATE',660,'Invoice Date','INVOICE_DATE','','','','SA059956','DATE','','','Invoice Date','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_INV_PRE_REG_V','SALES',660,'Sales','SALES','','','','SA059956','NUMBER','','','Sales','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_INV_PRE_REG_V','JOB_NUMBER',660,'Job Number','JOB_NUMBER','','','','SA059956','VARCHAR2','','','Job Number','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_INV_PRE_REG_V','ORDER_SOURCE',660,'Order Source','ORDER_SOURCE','','','','SA059956','VARCHAR2','','','Order Source','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_INV_PRE_REG_V','MODIFIER_NAME',660,'Modifier Name','MODIFIER_NAME','','','','SA059956','VARCHAR2','','','Modifier Name','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_INV_PRE_REG_V','FREIGHT_AMT',660,'Freight Amt','FREIGHT_AMT','','','','SA059956','NUMBER','','','Freight Amt','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_INV_PRE_REG_V','INV_AMOUNT',660,'Inv Amount','INV_AMOUNT','','','','SA059956','NUMBER','','','Inv Amount','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_INV_PRE_REG_V','SALESREP_NUMBER',660,'Salesrep Number','SALESREP_NUMBER','','','','SA059956','VARCHAR2','','','Salesrep Number','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_INV_PRE_REG_V','HEADER_ID',660,'Header Id','HEADER_ID','','','','SA059956','NUMBER','','','Header Id','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_INV_PRE_REG_V','ORGANIZATION_ID',660,'Organization Id','ORGANIZATION_ID','','','','SA059956','NUMBER','','','Organization Id','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_INV_PRE_REG_V','PAYMENT_DESC',660,'Payment Desc','PAYMENT_DESC','','','','SA059956','VARCHAR2','','','Payment Desc','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_INV_PRE_REG_V','UOM',660,'Uom','UOM','','','','SA059956','VARCHAR2','','','Uom','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_INV_PRE_REG_V','CAT',660,'Cat','CAT','','','','SA059956','VARCHAR2','','','Cat','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_INV_PRE_REG_V','LINE_FLOW_STATUS_CODE',660,'Line Flow Status Code','LINE_FLOW_STATUS_CODE','','','','SA059956','VARCHAR2','','','Line Flow Status Code','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_INV_PRE_REG_V','VQN_MODIFIER_NAME',660,'Vqn Modifier Name','VQN_MODIFIER_NAME','','','','SA059956','VARCHAR2','','','Vqn Modifier Name','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_INV_PRE_REG_V','LINE_TYPE',660,'Line Type','LINE_TYPE','','','','SA059956','VARCHAR2','','','Line Type','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_INV_PRE_REG_V','PARTY_ID',660,'Party Id','PARTY_ID','','','','SA059956','VARCHAR2','','','Party Id','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_INV_PRE_REG_V','SPECIAL_COST',660,'Special Cost','SPECIAL_COST','','','','SA059956','VARCHAR2','','','Special Cost','','','');
--Inserting View Components for EIS_XXWC_OM_INV_PRE_REG_V
xxeis.eis_rs_ins.vcomp( 'EIS_XXWC_OM_INV_PRE_REG_V','MTL_PARAMETERS',660,'MTL_PARAMETERS','SHIP_FROM_ORG','SHIP_FROM_ORG','SA059956','SA059956','-1','Inventory Control Options And Defaults','','','','');
xxeis.eis_rs_ins.vcomp( 'EIS_XXWC_OM_INV_PRE_REG_V','HZ_PARTIES',660,'HZ_PARTIES','PARTY','PARTY','SA059956','SA059956','-1','Information About Parties Such As Organizations, P','','','','');
xxeis.eis_rs_ins.vcomp( 'EIS_XXWC_OM_INV_PRE_REG_V','OE_ORDER_HEADERS',660,'OE_ORDER_HEADERS_ALL','OH','OH','SA059956','SA059956','-1','Oe Order Headers All Stores Header Information For','','','','');
xxeis.eis_rs_ins.vcomp( 'EIS_XXWC_OM_INV_PRE_REG_V','OE_ORDER_LINES',660,'OE_ORDER_LINES_ALL','OL','OL','SA059956','SA059956','-1','Oe Order Lines All Stores Information For All Orde','','','','');
xxeis.eis_rs_ins.vcomp( 'EIS_XXWC_OM_INV_PRE_REG_V','MTL_SYSTEM_ITEMS_KFV',660,'MTL_SYSTEM_ITEMS_B','MSI','MSI','SA059956','SA059956','-1','Inventory Item Definitions','','','','');
--Inserting View Component Joins for EIS_XXWC_OM_INV_PRE_REG_V
xxeis.eis_rs_ins.vcj( 'EIS_XXWC_OM_INV_PRE_REG_V','MTL_PARAMETERS','SHIP_FROM_ORG',660,'EXOIPRV.ORGANIZATION_ID','=','SHIP_FROM_ORG.ORGANIZATION_ID(+)','','','','Y','SA059956','','');
xxeis.eis_rs_ins.vcj( 'EIS_XXWC_OM_INV_PRE_REG_V','HZ_PARTIES','PARTY',660,'EXOIPRV.PARTY_ID','=','PARTY.PARTY_ID(+)','','','','Y','SA059956','','');
xxeis.eis_rs_ins.vcj( 'EIS_XXWC_OM_INV_PRE_REG_V','OE_ORDER_HEADERS','OH',660,'EXOIPRV.HEADER_ID','=','OH.HEADER_ID(+)','','','','Y','SA059956','','');
xxeis.eis_rs_ins.vcj( 'EIS_XXWC_OM_INV_PRE_REG_V','OE_ORDER_LINES','OL',660,'EXOIPRV.LINE_ID','=','OL.LINE_ID(+)','','','','Y','SA059956','','');
xxeis.eis_rs_ins.vcj( 'EIS_XXWC_OM_INV_PRE_REG_V','MTL_SYSTEM_ITEMS_KFV','MSI',660,'EXOIPRV.INVENTORY_ITEM_ID','=','MSI.INVENTORY_ITEM_ID(+)','','','','Y','SA059956','','');
xxeis.eis_rs_ins.vcj( 'EIS_XXWC_OM_INV_PRE_REG_V','MTL_SYSTEM_ITEMS_KFV','MSI',660,'EXOIPRV.MSI_ORGANIZATION_ID','=','MSI.ORGANIZATION_ID(+)','','','','Y','SA059956','','');
END;
/
set scan on define on
prompt Creating Report LOV Data for Invoice Pre-Register report
set scan off define off
DECLARE
BEGIN 
--Inserting Report LOVs - Invoice Pre-Register report
xxeis.eis_rs_ins.lov( 660,'select  RS.Name,SALESREP_ID  from  RA_SALESREPS RS
WHERE  RS.NAME is not null','','OM SALES REP','This gives the sales representative name','XXEIS_RS_ADMIN',NULL,'N','','');
xxeis.eis_rs_ins.lov( 660,'select nvl( party.party_name,CUST_ACCT.account_name) customer_name,CUST_ACCT.ACCOUNT_NUMBER
from  HZ_PARTIES PARTY, HZ_CUST_ACCOUNTS CUST_ACCT where CUST_ACCT.PARTY_ID = PARTY.PARTY_ID AND  PARTY.status=''A''
','','OM CUSTOMER NAME','This gives the Customer Name','XXEIS_RS_ADMIN',NULL,'N','','');
xxeis.eis_rs_ins.lov( 660,'Select  name, description from ra_terms_vl','','WC OM Payment Terms','','XXEIS_RS_ADMIN',NULL,'N','','');
xxeis.eis_rs_ins.lov( 660,'SELECT ORGANIZATION_CODE WAREHOUSE,ORGANIZATION_NAME FROM ORG_ORGANIZATION_DEFINITIONS OOD WHERE SYSDATE < NVL(OOD.DISABLE_DATE,SYSDATE+1) AND EXISTS (SELECT 1 FROM XXEIS.EIS_ORG_ACCESS_V  WHERE organization_id = ood.organization_id )
UNION
SELECT ''All'', ''All Organizations'' from Dual','','OM Warehouse All','','ANONYMOUS',NULL,'N','','');
xxeis.eis_rs_ins.lov( 660,'SELECT DISTINCT attribute7 created_by
FROM apps.oe_order_headers_all','','Order Created By Lov','','ANONYMOUS',NULL,'N','','');
END;
/
set scan on define on
prompt Creating Report Data for Invoice Pre-Register report
set scan off define off
DECLARE
BEGIN 
--Deleting Report data - Invoice Pre-Register report
xxeis.eis_rs_utility.delete_report_rows( 'Invoice Pre-Register report' );
--Inserting Report - Invoice Pre-Register report
xxeis.eis_rs_ins.r( 660,'Invoice Pre-Register report','','Real-time branch sales; current day?s sales ? at a detail and summary level.','','','','SA059956','EIS_XXWC_OM_INV_PRE_REG_V','Y','','','SA059956','','N','White Cap Reports','','CSV,Pivot Excel,EXCEL,','N');
--Inserting Report Columns - Invoice Pre-Register report
xxeis.eis_rs_ins.rc( 'Invoice Pre-Register report',660,'CUSTOMER_NAME','Customer Name','Customer Name','','','default','','11','N','','','','','','','','SA059956','N','N','','EIS_XXWC_OM_INV_PRE_REG_V','','');
xxeis.eis_rs_ins.rc( 'Invoice Pre-Register report',660,'CUSTOMER_NUMBER','Customer Number','Customer Number','','','default','','10','N','','','','','','','','SA059956','N','N','','EIS_XXWC_OM_INV_PRE_REG_V','','');
xxeis.eis_rs_ins.rc( 'Invoice Pre-Register report',660,'DISCOUNT_AMT','Discount Amt','Discount Amt','','~T~D~2','default','','20','N','','','','','','','','SA059956','N','N','','EIS_XXWC_OM_INV_PRE_REG_V','','');
xxeis.eis_rs_ins.rc( 'Invoice Pre-Register report',660,'ORDERED_DATE','Ordered Date','Ordered Date','','','default','','2','N','','','','','','','','SA059956','N','N','','EIS_XXWC_OM_INV_PRE_REG_V','','');
xxeis.eis_rs_ins.rc( 'Invoice Pre-Register report',660,'ORDER_NUMBER','Order Number','Order Number','','~~~','default','','3','N','','','','','','','','SA059956','N','N','','EIS_XXWC_OM_INV_PRE_REG_V','','');
xxeis.eis_rs_ins.rc( 'Invoice Pre-Register report',660,'ORDER_STATUS','Order Status','Order Status','','','default','','6','N','','','','','','','','SA059956','N','N','','EIS_XXWC_OM_INV_PRE_REG_V','','');
xxeis.eis_rs_ins.rc( 'Invoice Pre-Register report',660,'TAX_VALUE','Sales Tax','Tax Value','','~T~D~2','default','','21','N','','','','','','','','SA059956','N','N','','EIS_XXWC_OM_INV_PRE_REG_V','','');
xxeis.eis_rs_ins.rc( 'Invoice Pre-Register report',660,'AVERAGE_COST','Unit Cost','Average Cost','','~T~D~2','default','','18','N','','','','','','','','SA059956','N','N','','EIS_XXWC_OM_INV_PRE_REG_V','','');
xxeis.eis_rs_ins.rc( 'Invoice Pre-Register report',660,'ORDER_TYPE','Order Type','Order Type','','','default','','4','N','','','','','','','','SA059956','N','N','','EIS_XXWC_OM_INV_PRE_REG_V','','');
xxeis.eis_rs_ins.rc( 'Invoice Pre-Register report',660,'GM_PER','GM%','Order Type','NUMBER','~T~D~2','default','','25','Y','','','','','','','case when (NVL(EXOIPRV.sales,0) !=0  AND NVL(EXOIPRV.sale_cost,0) !=0) then  decode(EXOIPRV.line_type,''CREDIT ONLY'',(-1*(((EXOIPRV.sales-EXOIPRV.sale_cost)/(EXOIPRV.sales))*100)),(((EXOIPRV.sales-EXOIPRV.sale_cost)/(EXOIPRV.sales))*100)) WHEN NVL(EXOIPRV.sale_cost,0) =0 THEN decode(EXOIPRV.line_type,''CREDIT ONLY'',(-1*100),100) WHEN nvl(EXOIPRV.sales,0) != 0 THEN decode(EXOIPRV.line_type,''CREDIT ONLY'',(-1*(((EXOIPRV.sales-EXOIPRV.sale_cost)/(EXOIPRV.sales))*100)),(((EXOIPRV.sales-EXOIPRV.sale_cost)/(EXOIPRV.sales))*100)) else 0 end','SA059956','N','N','','EIS_XXWC_OM_INV_PRE_REG_V','','');
xxeis.eis_rs_ins.rc( 'Invoice Pre-Register report',660,'CREATED_BY','Created By','Created By','','','default','','7','N','','','','','','','','SA059956','N','N','','EIS_XXWC_OM_INV_PRE_REG_V','','');
xxeis.eis_rs_ins.rc( 'Invoice Pre-Register report',660,'TRX_NUMBER','Invoice Number','Trx Number','','','default','','9','N','','','','','','','','SA059956','N','N','','EIS_XXWC_OM_INV_PRE_REG_V','','');
xxeis.eis_rs_ins.rc( 'Invoice Pre-Register report',660,'WAREHOUSE','Warehouse','Warehouse','','','default','','1','N','','','','','','','','SA059956','N','N','','EIS_XXWC_OM_INV_PRE_REG_V','','');
xxeis.eis_rs_ins.rc( 'Invoice Pre-Register report',660,'PAYMENT_TERM','Payment Term','Payment Term','','','default','','28','N','','','','','','','','SA059956','N','N','','EIS_XXWC_OM_INV_PRE_REG_V','','');
xxeis.eis_rs_ins.rc( 'Invoice Pre-Register report',660,'SALESREP_NAME','Salesrep Name','Salesrep Name','','','default','','29','N','','','','','','','','SA059956','N','N','','EIS_XXWC_OM_INV_PRE_REG_V','','');
xxeis.eis_rs_ins.rc( 'Invoice Pre-Register report',660,'ITEM','SKU','Item','','','default','','13','N','','','','','','','','SA059956','N','N','','EIS_XXWC_OM_INV_PRE_REG_V','','');
xxeis.eis_rs_ins.rc( 'Invoice Pre-Register report',660,'ITEM_DESC','SKU Description','Item Desc','','','default','','14','N','','','','','','','','SA059956','N','N','','EIS_XXWC_OM_INV_PRE_REG_V','','');
xxeis.eis_rs_ins.rc( 'Invoice Pre-Register report',660,'ORDER_LINE','Sales Order Line Number','Order Line','','','default','','12','N','','','','','','','','SA059956','N','N','','EIS_XXWC_OM_INV_PRE_REG_V','','');
xxeis.eis_rs_ins.rc( 'Invoice Pre-Register report',660,'UNIT_SELLING_PRICE','Unit Sell Price','Unit Selling Price','','~T~D~2','default','','16','N','','','','','','','','SA059956','N','N','','EIS_XXWC_OM_INV_PRE_REG_V','','');
xxeis.eis_rs_ins.rc( 'Invoice Pre-Register report',660,'QTY','Order Qty','Qty','','~~~','default','','15','N','','','','','','','','SA059956','N','N','','EIS_XXWC_OM_INV_PRE_REG_V','','');
xxeis.eis_rs_ins.rc( 'Invoice Pre-Register report',660,'INVOICE_DATE','Invoice Date','Invoice Date','','','default','','8','N','','','','','','','','SA059956','N','N','','EIS_XXWC_OM_INV_PRE_REG_V','','');
xxeis.eis_rs_ins.rc( 'Invoice Pre-Register report',660,'SALE_COST','Extended Cost','Sale Cost','','~T~D~2','default','','19','N','','','','','','','','SA059956','N','N','','EIS_XXWC_OM_INV_PRE_REG_V','','');
xxeis.eis_rs_ins.rc( 'Invoice Pre-Register report',660,'LINE_TOTAL','Line Total','Sale Cost','NUMBER','~T~D~2','default','','22','Y','','','','','','','(nvl(EXOIPRV.sales,0) + nvl(EXOIPRV.tax_value,0))','SA059956','N','N','','EIS_XXWC_OM_INV_PRE_REG_V','','');
xxeis.eis_rs_ins.rc( 'Invoice Pre-Register report',660,'ORDER_COUNT','Order Count','Sale Cost','NUMBER','~~~','default','','31','Y','','','','','','','XXEIS.EIS_RS_XXWC_COM_UTIL_PKG.get_order_count(EXOIPRV.order_number)','SA059956','N','N','','EIS_XXWC_OM_INV_PRE_REG_V','','');
xxeis.eis_rs_ins.rc( 'Invoice Pre-Register report',660,'PROFIT','Profit','Profit','NUMBER','~T~D~2','default','','24','Y','','','','','','','(NVL(EXOIPRV.sales,0) - NVL(EXOIPRV.sale_cost,0))','SA059956','N','N','','EIS_XXWC_OM_INV_PRE_REG_V','','');
xxeis.eis_rs_ins.rc( 'Invoice Pre-Register report',660,'SALES','Extended Sell Price','Sales','','~T~D~2','default','','17','N','','','','','','','','SA059956','N','N','','EIS_XXWC_OM_INV_PRE_REG_V','','');
xxeis.eis_rs_ins.rc( 'Invoice Pre-Register report',660,'JOB_NUMBER','Job Number','Job Number','','','default','','32','N','','','','','','','','SA059956','N','N','','EIS_XXWC_OM_INV_PRE_REG_V','','');
xxeis.eis_rs_ins.rc( 'Invoice Pre-Register report',660,'ORDER_SOURCE','Order Source','Order Source','','','default','','5','N','','','','','','','','SA059956','N','N','','EIS_XXWC_OM_INV_PRE_REG_V','','');
xxeis.eis_rs_ins.rc( 'Invoice Pre-Register report',660,'ADJ_GM_PER','Adjust GM%','Order Source','NUMBER','~T~D~2','default','','27','Y','','','','','','','case WHEN EXOIPRV.SPECIAL_COST IS NULL THEN NULL when (NVL(EXOIPRV.sales,0) !=0  AND NVL(EXOIPRV.SPECIAL_COST,0) !=0) then (((EXOIPRV.sales-(EXOIPRV.SPECIAL_COST * EXOIPRV.QTY))/(EXOIPRV.sales))*100) WHEN NVL(EXOIPRV.SPECIAL_COST,0) =0  THEN 100 WHEN nvl(EXOIPRV.sales,0) != 0 THEN (((EXOIPRV.sales-(EXOIPRV.SPECIAL_COST * EXOIPRV.QTY))/(EXOIPRV.sales))*100) else 0 end','SA059956','N','N','','EIS_XXWC_OM_INV_PRE_REG_V','','');
xxeis.eis_rs_ins.rc( 'Invoice Pre-Register report',660,'VQN_MOD_NAME','Modifier Name','Order Source','VARCHAR2','','default','','30','Y','','','','','','','NVL(EXOIPRV.VQN_MODIFIER_NAME,EXOIPRV.MODIFIER_NAME)','SA059956','N','N','','EIS_XXWC_OM_INV_PRE_REG_V','','');
xxeis.eis_rs_ins.rc( 'Invoice Pre-Register report',660,'SPECIAL_COST','Special VQN Cost','Special Cost','','','default','','26','N','','','','','','','','SA059956','N','N','','EIS_XXWC_OM_INV_PRE_REG_V','','');
--Inserting Report Parameters - Invoice Pre-Register report
xxeis.eis_rs_ins.rp( 'Invoice Pre-Register report',660,'Warehouse','Warehouse','WAREHOUSE','IN','OM Warehouse All','','VARCHAR2','Y','Y','1','','N','CONSTANT','SA059956','Y','N','','','');
xxeis.eis_rs_ins.rp( 'Invoice Pre-Register report',660,'Created By','Created By','CREATED_BY','IN','Order Created By Lov','','VARCHAR2','N','Y','3','','Y','CONSTANT','SA059956','Y','N','','','');
xxeis.eis_rs_ins.rp( 'Invoice Pre-Register report',660,'Salesrep Name','Salesrep Name','SALESREP_NAME','IN','OM SALES REP','','VARCHAR2','N','Y','4','','Y','CONSTANT','SA059956','Y','N','','','');
xxeis.eis_rs_ins.rp( 'Invoice Pre-Register report',660,'Payment Term','Payment Term','PAYMENT_TERM','IN','WC OM Payment Terms','','VARCHAR2','N','Y','5','','Y','CONSTANT','SA059956','Y','N','','','');
xxeis.eis_rs_ins.rp( 'Invoice Pre-Register report',660,'Customer Name','Customer Name','CUSTOMER_NAME','IN','OM CUSTOMER NAME','','VARCHAR2','N','Y','6','','Y','CONSTANT','SA059956','Y','N','','','');
xxeis.eis_rs_ins.rp( 'Invoice Pre-Register report',660,'As Of Date','As Of Date','ORDERED_DATE','>=','','','DATE','Y','Y','2','','N','CONSTANT','SA059956','Y','N','','','');
--Inserting Report Conditions - Invoice Pre-Register report
xxeis.eis_rs_ins.rcn( 'Invoice Pre-Register report',660,'CREATED_BY','IN',':Created By','','','Y','3','Y','SA059956');
xxeis.eis_rs_ins.rcn( 'Invoice Pre-Register report',660,'SALESREP_NAME','IN',':Salesrep Name','','','Y','4','Y','SA059956');
xxeis.eis_rs_ins.rcn( 'Invoice Pre-Register report',660,'PAYMENT_TERM','IN',':Payment Term','','','Y','5','Y','SA059956');
xxeis.eis_rs_ins.rcn( 'Invoice Pre-Register report',660,'CUSTOMER_NAME','IN',':Customer Name','','','Y','6','Y','SA059956');
xxeis.eis_rs_ins.rcn( 'Invoice Pre-Register report',660,'','','','','AND ( ''All'' IN (:Warehouse) OR (WAREHOUSE IN (:Warehouse)))
AND (EXOIPRV.ORDER_TYPE != ''STANDARD ORDER'' OR  (EXOIPRV.ORDER_TYPE = ''STANDARD ORDER''  AND EXOIPRV.ORDERED_DATE < =:As Of Date) OR (EXOIPRV.ORDER_TYPE = ''STANDARD ORDER''  AND EXOIPRV.LINE_FLOW_STATUS_CODE IN (''CLOSED'',''INVOICE_DELIVERY'',''INVOICE_HOLD'',
''INVOICE_INCOMPLETE'',''INVOICE_NOT_APPLICABLE'',''INVOICE_RFR'',''INVOICE_UNEXPECTED_ERROR'',
''PARTIAL_INVOICE_RFR'')))
','Y','0','','SA059956');
--Inserting Report Sorts - Invoice Pre-Register report
xxeis.eis_rs_ins.rs( 'Invoice Pre-Register report',660,'CREATED_BY','ASC','SA059956','','');
xxeis.eis_rs_ins.rs( 'Invoice Pre-Register report',660,'ORDER_NUMBER','ASC','SA059956','','');
--Inserting Report Triggers - Invoice Pre-Register report
xxeis.eis_rs_ins.rt( 'Invoice Pre-Register report',660,'begin
XXEIS.EIS_XXWC_INV_PRE_REG_RPT_PKG.set_date_from(:As Of Date);
end;','B','Y','SA059956');
--Inserting Report Templates - Invoice Pre-Register report
--Inserting Report Portals - Invoice Pre-Register report
--Inserting Report Dashboards - Invoice Pre-Register report
--Inserting Report Security - Invoice Pre-Register report
xxeis.eis_rs_ins.rsec( 'Invoice Pre-Register report','660','','50926',660,'SA059956','','');
xxeis.eis_rs_ins.rsec( 'Invoice Pre-Register report','660','','50927',660,'SA059956','','');
xxeis.eis_rs_ins.rsec( 'Invoice Pre-Register report','660','','50928',660,'SA059956','','');
xxeis.eis_rs_ins.rsec( 'Invoice Pre-Register report','660','','50929',660,'SA059956','','');
xxeis.eis_rs_ins.rsec( 'Invoice Pre-Register report','660','','50931',660,'SA059956','','');
xxeis.eis_rs_ins.rsec( 'Invoice Pre-Register report','660','','50930',660,'SA059956','','');
xxeis.eis_rs_ins.rsec( 'Invoice Pre-Register report','660','','50856',660,'SA059956','','');
xxeis.eis_rs_ins.rsec( 'Invoice Pre-Register report','660','','50857',660,'SA059956','','');
xxeis.eis_rs_ins.rsec( 'Invoice Pre-Register report','660','','50858',660,'SA059956','','');
xxeis.eis_rs_ins.rsec( 'Invoice Pre-Register report','660','','50859',660,'SA059956','','');
xxeis.eis_rs_ins.rsec( 'Invoice Pre-Register report','660','','50860',660,'SA059956','','');
xxeis.eis_rs_ins.rsec( 'Invoice Pre-Register report','660','','50861',660,'SA059956','','');
xxeis.eis_rs_ins.rsec( 'Invoice Pre-Register report','20005','','50880',660,'SA059956','','');
xxeis.eis_rs_ins.rsec( 'Invoice Pre-Register report','','LC053655','',660,'SA059956','','');
xxeis.eis_rs_ins.rsec( 'Invoice Pre-Register report','','10010432','',660,'SA059956','','');
xxeis.eis_rs_ins.rsec( 'Invoice Pre-Register report','','RB054040','',660,'SA059956','','');
xxeis.eis_rs_ins.rsec( 'Invoice Pre-Register report','','RV003897','',660,'SA059956','','');
xxeis.eis_rs_ins.rsec( 'Invoice Pre-Register report','','SS084202','',660,'SA059956','','');
xxeis.eis_rs_ins.rsec( 'Invoice Pre-Register report','660','','21623',660,'SA059956','','');
xxeis.eis_rs_ins.rsec( 'Invoice Pre-Register report','','SO004816','',660,'SA059956','','');
xxeis.eis_rs_ins.rsec( 'Invoice Pre-Register report','660','','50886',660,'SA059956','','');
xxeis.eis_rs_ins.rsec( 'Invoice Pre-Register report','660','','50901',660,'SA059956','','');
xxeis.eis_rs_ins.rsec( 'Invoice Pre-Register report','660','','50870',660,'SA059956','','');
xxeis.eis_rs_ins.rsec( 'Invoice Pre-Register report','660','','50871',660,'SA059956','','');
xxeis.eis_rs_ins.rsec( 'Invoice Pre-Register report','660','','50869',660,'SA059956','','');
xxeis.eis_rs_ins.rsec( 'Invoice Pre-Register report','20005','','50900',660,'SA059956','','');
xxeis.eis_rs_ins.rsec( 'Invoice Pre-Register report','660','','51044',660,'SA059956','','');
xxeis.eis_rs_ins.rsec( 'Invoice Pre-Register report','20005','','51024',660,'SA059956','','');
--Inserting Report Pivots - Invoice Pre-Register report
xxeis.eis_rs_ins.rpivot( 'Invoice Pre-Register report',660,'Pivot','1','1,0|1,2,1','1,1,0,0|None|2');
--Inserting Report Pivot Details For Pivot - Pivot
xxeis.eis_rs_ins.rpivot_dtls( 'Invoice Pre-Register report',660,'Pivot','CUSTOMER_NAME','PAGE_FIELD','','','1','','');
xxeis.eis_rs_ins.rpivot_dtls( 'Invoice Pre-Register report',660,'Pivot','DISCOUNT_AMT','DATA_FIELD','SUM','','2','','xlNormal');
xxeis.eis_rs_ins.rpivot_dtls( 'Invoice Pre-Register report',660,'Pivot','CREATED_BY','ROW_FIELD','','','2','','');
xxeis.eis_rs_ins.rpivot_dtls( 'Invoice Pre-Register report',660,'Pivot','ORDER_NUMBER','ROW_FIELD','','','3','','');
xxeis.eis_rs_ins.rpivot_dtls( 'Invoice Pre-Register report',660,'Pivot','LINE_TOTAL','DATA_FIELD','SUM','','3','','xlNormal');
xxeis.eis_rs_ins.rpivot_dtls( 'Invoice Pre-Register report',660,'Pivot','WAREHOUSE','ROW_FIELD','','','1','','');
xxeis.eis_rs_ins.rpivot_dtls( 'Invoice Pre-Register report',660,'Pivot','ORDER_COUNT','DATA_FIELD','SUM','Order Count','5','','xlNormal');
xxeis.eis_rs_ins.rpivot_dtls( 'Invoice Pre-Register report',660,'Pivot','PROFIT','DATA_FIELD','SUM','','4','','');
xxeis.eis_rs_ins.rpivot_dtls( 'Invoice Pre-Register report',660,'Pivot','SALES','DATA_FIELD','SUM','','1','','');
--Inserting Report Summary Calculation Columns For Pivot- Pivot
END;
/
set scan on define on
