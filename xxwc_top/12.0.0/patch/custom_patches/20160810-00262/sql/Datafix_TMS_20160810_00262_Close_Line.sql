/*************************************************************************
  $Header Datafix_TMS_20160810_00262_Close_Line.sql $
  Module Name: TMS_20160810_00262


  REVISIONS:
  Ver        Date         Author                Description
  ---------  -----------  ------------------    --------------------------
   1.0       17-JAN-2016  Niraj K Ranjan        TMS#20160810_00262   ORDER 20871187
**************************************************************************/ 
SET SERVEROUTPUT ON SIZE 1000000
BEGIN
   DBMS_OUTPUT.put_line ('TMS: 20160810_00262    , Before Update');  
		  
	UPDATE apps.oe_order_lines_all 
      SET 
      shipping_quantity = 5, 
      actual_shipment_date = TO_DATE('10/25/2016' ,'MM/DD/YYYY'), 
      fulfilled_flag = 'Y', 
      fulfillment_date = TO_DATE('10/25/2016','MM/DD/YYYY'), 
      fulfilled_quantity = 5, 
      last_updated_by = -1, 
      last_update_date = SYSDATE, 
      open_flag = 'N', 
      FLOW_STATUS_CODE='CLOSED' 
    WHERE line_id = 58464469 
    AND header_id= 35623886; 


   DBMS_OUTPUT.put_line (
         'TMS: 20160810_00262  Sales order lines updated (Expected:1): '
      || SQL%ROWCOUNT);

   COMMIT;

   DBMS_OUTPUT.put_line ('TMS: 20160810_00262    , End Update');
EXCEPTION
   WHEN OTHERS
   THEN
      ROLLBACK;
      DBMS_OUTPUT.put_line ('TMS: 20160715_00210 , Errors : ' || SQLERRM);
END;
/
