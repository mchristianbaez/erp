BEGIN
   mo_global.set_policy_context ('S', '162');
END;
/
DECLARE
   CURSOR cur_sgn
   IS
      SELECT DISTINCT
             SHIP_TO_ORGANIZATION_ID,
             pha.segment1,
             '''' || pha.VENDOR_NAME || '''' supplier
        FROM po_headers_v pha, po_line_locations_all plla
       WHERE     pha.po_header_id = plla.po_header_id
             AND pha.creation_date BETWEEN SYSDATE - 500 AND SYSDATE
             AND   plla.QUANTITY
                 - (plla.QUANTITY_RECEIVED + plla.QUANTITY_CANCELLED) > 1
             AND pha.authorization_status = 'APPROVED'
             AND NVL (pha.cancel_flag, 'N') = 'N'
             AND NVL (pha.closed_code, 'OPEN') = 'OPEN'
             AND NVL (plla.cancel_flag, 'N') = 'N'
             AND pha.org_id = 162
             AND ROWNUM < 101;

   l_batch_id        NUMBER;
   l_group_id        NUMBER;
   l_return          NUMBER;
   l_template_code   VARCHAR2 (200);

   l_exception       EXCEPTION;
BEGIN
   FOR rec_sign IN cur_sgn
   LOOP
      -- l_group_id := xxwc_print_request_groups_s.nextval;
      l_batch_id := xxwc_print_requests_s.NEXTVAL;
      l_group_id := xxwc_print_request_groups_s.NEXTVAL;

      l_template_code := 'XXWC_POXRVXRV_PRINT';

      INSERT INTO XXWC.XXWC_PRINT_REQUESTS_TEMP (CREATED_BY,
                                                 CREATION_DATE,
                                                 LAST_UPDATED_BY,
                                                 LAST_UPDATE_DATE,
                                                 BATCH_ID,
                                                 PROCESS_FLAG,
                                                 GROUP_ID,
                                                 APPLICATION,
                                                 PROGRAM,
                                                 DESCRIPTION,
                                                 START_TIME,
                                                 SUB_REQUEST,
                                                 PRINTER,
                                                 STYLE,
                                                 COPIES,
                                                 SAVE_OUTPUT,
                                                 PRINT_TOGETHER,
                                                 VALIDATE_PRINTER,
                                                 TEMPLATE_APPL_NAME,
                                                 TEMPLATE_CODE,
                                                 TEMPLATE_LANGUAGE,
                                                 TEMPLATE_TERRITORY,
                                                 OUTPUT_FORMAT,
                                                 NLS_LANGUAGE)
           VALUES (16991                                          --CREATED_BY
                        ,
                   SYSDATE                                    -- CREATION_DATE
                          ,
                   16991                                      --LAST_UDATED_BY
                        ,
                   SYSDATE                                 -- LAST_UPDATE_DATE
                          ,
                   l_batch_id                                       --BATCH_ID
                             ,
                   '1'                                          --PROCESS_FLAG
                      ,
                   l_group_id                                       --GROUP_ID
                             ,
                   'XXWC'                                        --APPLICATION
                         ,
                   'XXWC_POXRVXRV_PRINT'                             --PROGRAM
                                        ,
                   'XXWC Expected Receipts Report - Print'       --DESCRIPTION
                                                          ,
                   NULL                                           --START_TIME
                       ,
                   'FALSE'                                       --SUB_REQUEST
                          ,
                   'orlando_test_eval'                                         --PRINTER
                            ,
                   NULL                                                --STYLE
                       ,
                   1                                                  --COPIES
                    ,
                   'TRUE'                                        --SAVE_OUTPUT
                         ,
                   'N'                                        --PRINT_TOGETHER
                      ,
                   'RESOLVE'                                --VALIDATE_PRINTER
                            ,
                   'XXWC'                                 --TEMPLATE_APPL_NAME
                         ,
                   l_template_code                             --TEMPLATE_CODE
                                  ,
                   'en'                                    --TEMPLATE_LANGUAGE
                       ,
                   'US'                                   --TEMPLATE_TERRITORY
                       ,
                   'PDF'                                       --OUTPUT_FORMAT
                        --,'1'
                   ,
                   'en'                                         --NLS_LANGUAGE
                       );

      COMMIT;

      INSERT INTO XXWC_PRINT_REQUESTS_ARG_TEMP
           VALUES (l_batch_id,
                   1,
                   l_group_id,
                   1,
                   rec_sign.SHIP_TO_ORGANIZATION_ID);

      INSERT INTO XXWC_PRINT_REQUESTS_ARG_TEMP
           VALUES (l_batch_id,
                   1,
                   l_group_id,
                   2,
                   NULL);

      INSERT INTO XXWC_PRINT_REQUESTS_ARG_TEMP
           VALUES (l_batch_id,
                   1,
                   l_group_id,
                   3,
                   NULL);

      INSERT INTO XXWC_PRINT_REQUESTS_ARG_TEMP
           VALUES (l_batch_id,
                   1,
                   l_group_id,
                   4,
                   NULL);

      INSERT INTO XXWC_PRINT_REQUESTS_ARG_TEMP
           VALUES (l_batch_id,
                   1,
                   l_group_id,
                   5,
                   rec_sign.segment1);

      INSERT INTO XXWC_PRINT_REQUESTS_ARG_TEMP
           VALUES (l_batch_id,
                   1,
                   l_group_id,
                   6,
                   NULL);

      INSERT INTO XXWC_PRINT_REQUESTS_ARG_TEMP
           VALUES (l_batch_id,
                   1,
                   l_group_id,
                   7,
                   NULL);

      INSERT INTO XXWC_PRINT_REQUESTS_ARG_TEMP
           VALUES (l_batch_id,
                   1,
                   l_group_id,
                   8,
                   NULL);

      INSERT INTO XXWC_PRINT_REQUESTS_ARG_TEMP
           VALUES (l_batch_id,
                   1,
                   l_group_id,
                   9,
                   rec_sign.supplier);

      INSERT INTO XXWC_PRINT_REQUESTS_ARG_TEMP
           VALUES (l_batch_id,
                   1,
                   l_group_id,
                   10,
                   NULL);

      INSERT INTO XXWC_PRINT_REQUESTS_ARG_TEMP
           VALUES (l_batch_id,
                   1,
                   l_group_id,
                   11,
                   NULL);

      INSERT INTO XXWC_PRINT_REQUESTS_ARG_TEMP
           VALUES (l_batch_id,
                   1,
                   l_group_id,
                   12,
                   101);

      INSERT INTO XXWC_PRINT_REQUESTS_ARG_TEMP
           VALUES (l_batch_id,
                   1,
                   l_group_id,
                   13,
                   NULL);

      INSERT INTO XXWC_PRINT_REQUESTS_ARG_TEMP
           VALUES (l_batch_id,
                   1,
                   l_group_id,
                   14,
                   NULL);

      INSERT INTO XXWC_PRINT_REQUESTS_ARG_TEMP
           VALUES (l_batch_id,
                   1,
                   l_group_id,
                   15,
                   NULL);

      INSERT INTO XXWC_PRINT_REQUESTS_ARG_TEMP
           VALUES (l_batch_id,
                   1,
                   l_group_id,
                   16,
                   NULL);

      INSERT INTO XXWC_PRINT_REQUESTS_ARG_TEMP
           VALUES (l_batch_id,
                   1,
                   l_group_id,
                   17,
                   NULL);

      INSERT INTO XXWC_PRINT_REQUESTS_ARG_TEMP
           VALUES (l_batch_id,
                   1,
                   l_group_id,
                   18,
                   2);

      INSERT INTO XXWC_PRINT_REQUESTS_ARG_TEMP
           VALUES (l_batch_id,
                   1,
                   l_group_id,
                   19,
                   NULL);

      INSERT INTO XXWC_PRINT_REQUESTS_ARG_TEMP
           VALUES (l_batch_id,
                   1,
                   l_group_id,
                   20,
                   NULL);

      INSERT INTO XXWC_PRINT_REQUESTS_ARG_TEMP
           VALUES (l_batch_id,
                   1,
                   l_group_id,
                   21,
                   NULL);

      INSERT INTO XXWC_PRINT_REQUESTS_ARG_TEMP
           VALUES (l_batch_id,
                   1,
                   l_group_id,
                   22,
                   NULL);


      COMMIT;

      l_return :=
         XXWC_ONT_ROUTINES_PKG.SUBMIT_PRINT_BATCH (l_batch_id,
                                                   l_group_id,
                                                   1,           -- ProcessFlag
                                                   16991, --rec_sign.REQUESTED_BY,
                                                   50884,
                                                   401);

      DBMS_OUTPUT.put_line (' l_return ' || l_return);
      COMMIT;
   END LOOP;
EXCEPTION
   WHEN l_exception
   THEN
      DBMS_OUTPUT.put_line (' l_Exception ');
   WHEN OTHERS
   THEN
      DBMS_OUTPUT.put_line ('Error - ' || SQLERRM);
END;
/