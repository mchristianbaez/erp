DECLARE

CURSOR cur_sgn
    IS
SELECT *
  FROM xxwc.xxwc_print_log_tbl xpl
 WHERE 1=1 --concurrent_program_name = 'XXWC_OM_SRECEIPT' 
   AND user_concurrent_program_name='XXWC Internal Order Pack Slip'
   AND exists (select 1 from oe_order_headers_all a , oe_order_lines_all b
    where a.header_id=b.header_id
    and a.header_id=xpl.argument2 
    and b.ship_from_org_id=xpl.argument1 and b.flow_status_code='AWAITING_SHIPPING')
    AND user_name<>'RT008057'
    AND rownum<101;

l_batch_id NUMBER;
l_group_id NUMBER;
l_return NUMBER;
l_template_code VARCHAR2(200);

l_exception EXCEPTION;

BEGIN

fnd_global.apps_initialize (user_id        => 16991,
                               resp_id        => 50857,
                               resp_appl_id   => 660);

   mo_global.set_policy_context ('S', 162);
   


FOR rec_sign IN cur_sgn LOOP

-- l_group_id := xxwc_print_request_groups_s.nextval;
l_batch_id := xxwc_print_requests_s.nextval;
l_group_id := xxwc_print_request_groups_s.nextval;

l_template_code := 'XXWC_INT_ORD_PACK';

INSERT INTO XXWC.XXWC_PRINT_REQUESTS_TEMP
                 (CREATED_BY
                 ,CREATION_DATE
                 ,LAST_UPDATED_BY
                 ,LAST_UPDATE_DATE
                 ,BATCH_ID
                 ,PROCESS_FLAG
                 ,GROUP_ID
                 ,APPLICATION
                 ,PROGRAM
                 ,DESCRIPTION
                 ,START_TIME
                 ,SUB_REQUEST
                 ,PRINTER
                 ,STYLE
                 ,COPIES
                 ,SAVE_OUTPUT
                 ,PRINT_TOGETHER
                 ,VALIDATE_PRINTER
                 ,TEMPLATE_APPL_NAME
                 ,TEMPLATE_CODE
                 ,TEMPLATE_LANGUAGE
                 ,TEMPLATE_TERRITORY
                 ,OUTPUT_FORMAT
                 --,DMS_SIGN_FLAG
                 ,NLS_LANGUAGE)
                 values
                 (rec_sign.REQUESTED_BY                            --CREATED_BY
                 ,rec_sign.ACTUAL_COMPLETION_DATE                         -- CREATION_DATE
                 ,rec_sign.REQUESTED_BY                            --LAST_UDATED_BY
                 ,rec_sign.ACTUAL_COMPLETION_DATE                         -- LAST_UPDATE_DATE
                 ,l_batch_id                           --BATCH_ID
                 ,'1'                                  --PROCESS_FLAG
                 ,l_group_id                           --GROUP_ID
                 ,'XXWC'                               --APPLICATION
                 , rec_sign.concurrent_program_name                              --PROGRAM
                 , rec_sign.USER_CONCURRENT_PROGRAM_NAME                         --DESCRIPTION
                 ,NULL                                 --START_TIME 
                 ,'FALSE'                              --SUB_REQUEST
                 , 'orlando_test_eval'                          --PRINTER
                 , NULL                                 --STYLE
                 , 1                                 --COPIES
                 ,'TRUE'                               --SAVE_OUTPUT
                 ,'N'                                  --PRINT_TOGETHER
                 ,'RESOLVE'                            --VALIDATE_PRINTER
                 ,'XXWC'                               --TEMPLATE_APPL_NAME
                 , l_template_code              --TEMPLATE_CODE
                 ,'en'                                 --TEMPLATE_LANGUAGE
                 ,'US'                                 --TEMPLATE_TERRITORY
                 ,'PDF'                                --OUTPUT_FORMAT
                 --,'1'
                 ,'en'                                 --NLS_LANGUAGE
                 );
                 
                 COMMIT;

                 INSERT INTO XXWC_PRINT_REQUESTS_ARG_TEMP values (l_batch_id,1,l_group_id,1,rec_sign.argument1);

                 INSERT INTO XXWC_PRINT_REQUESTS_ARG_TEMP values (l_batch_id,1,l_group_id,2,rec_sign.argument2);
                 
                 INSERT INTO XXWC_PRINT_REQUESTS_ARG_TEMP values (l_batch_id,1,l_group_id,3,2);
              
                 INSERT INTO XXWC_PRINT_REQUESTS_ARG_TEMP values (l_batch_id,1,l_group_id,4,null);

                 INSERT INTO XXWC_PRINT_REQUESTS_ARG_TEMP values (l_batch_id,1,l_group_id,5,null);

                 INSERT INTO XXWC_PRINT_REQUESTS_ARG_TEMP values (l_batch_id,1,l_group_id,6,2);

                 INSERT INTO XXWC_PRINT_REQUESTS_ARG_TEMP values (l_batch_id,1,l_group_id,7,null);

                 INSERT INTO XXWC_PRINT_REQUESTS_ARG_TEMP values (l_batch_id,1,l_group_id,8,null);

                 INSERT INTO XXWC_PRINT_REQUESTS_ARG_TEMP values (l_batch_id,1,l_group_id,9,1);
                 
                 INSERT INTO XXWC_PRINT_REQUESTS_ARG_TEMP values (l_batch_id,1,l_group_id,10,null);
                 
                 INSERT INTO XXWC_PRINT_REQUESTS_ARG_TEMP values (l_batch_id,1,l_group_id,11,2);


             COMMIT;
             
             dbms_output.put_line(' l_batch_id-'||l_batch_id);
             dbms_output.put_line(' l_group_id-'||l_group_id);

              l_return := XXWC_ONT_ROUTINES_PKG.SUBMIT_PRINT_BATCH(l_batch_id, 
                                                                   l_group_id,
                                                                   1,                              -- ProcessFlag
                                                                   16991, --rec_sign.REQUESTED_BY,
                                                                   50857,
                                                                   660);
                                                                   
dbms_output.put_line(' l_return '||l_return);
             COMMIT;

END LOOP;

EXCEPTION
WHEN l_exception THEN
  dbms_output.put_line(' l_Exception ');
WHEN OTHERS THEN
dbms_output.put_line('Error - '||SQLERRM);
END;
/