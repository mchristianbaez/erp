create or replace package body APPS.XXWC_PERF_SQL_ID_P as
  /*************************************************************************
       $Header XXWC_PERF_SQL_ID_P $
       Module Name: XXWC_PERF_SQL_ID_P

       REVISIONS:
       Ver        Date        Author                     Description
       ---------  ----------  ---------------         -------------------------
       1.0        05/02/2017  Nancy Pahwa            TMS#20170425-00251 -Function to debug Grid Info
       1.1        07/31/2017  Nancy Pahwa            TMS #20170731-00409 -FIx clob to char error
       1.2        04/03/2018  Nancy Pahwa            TMS #20180301-00106 - Fix clob error
  **************************************************************************/
  function PERF_SQL_ID(p_sql_id varchar2) return t_result_tab
    pipelined as
    l_ret_rec t_result_rec;
  BEGIN
    for i in (select distinct x.inst_id,
                              x.sid,
                              --  x.serial#,
                              r.request_id concurrent_request_id,
                              x.username,
                              x.client_identifier,
                              x.osuser,
                              x.status,
                              to_char(x.logon_time, 'mm/dd/yyyy hh24:mi') logon_time,
                              v.spid spid_dbnode,
                              x.machine,
                              x.MODULE,
                              x.action,
                              x.program,
                              x.SQL_ID,
                              sqlarea.optimizer_mode,
                              sqlarea.hash_value,
                              sqlarea.address,
                              sqlarea.sql_text,
                              p.concurrent_program_name,
                              r.argument_text,
                              t.user_concurrent_program_name
                from gv$process                         v,
                     sys.gv_$sqlarea                    sqlarea,
                     gv$session                         x,
                     applsys.fnd_concurrent_requests    r,
                     applsys.FND_CONCURRENT_PROGRAMS    p,
                     applsys.FND_CONCURRENT_PROGRAMS_TL t
               where 1 = 1
                 and x.paddr = v.addr
                 and r.oracle_process_id = v.spid
                 and x.sql_hash_value = sqlarea.hash_value
                 and x.sql_address = sqlarea.address
                 and x.username is not null
                 and r.concurrent_program_id = p.concurrent_program_id
                 and p.concurrent_program_id = t.concurrent_program_id
                 and x.SQL_ID = p_sql_id
              union all
              select distinct x.inst_id,
                              x.sid,
                              -- x.serial#,
                              null request_id,
                              x.username,
                              x.client_identifier,
                              x.osuser,
                              x.status,
                              to_char(x.logon_time, 'mm/dd/yyyy hh24:mi') logon_time,
                              v.spid spid_dbnode,
                              x.machine,
                              x.MODULE,
                              x.action,
                              x.program,
                              x.SQL_ID,
                              sqlarea.optimizer_mode,
                              sqlarea.hash_value,
                              sqlarea.address,
                              sqlarea.sql_text,
                              null concurrent_program_name,
                              null argument_text,
                              null user_concurrent_program_name
                from gv$process v, sys.gv_$sqlarea sqlarea, gv$session x
               where 1 = 1
                 and x.paddr = v.addr
                 and x.sql_hash_value = sqlarea.hash_value
                 and x.sql_address = sqlarea.address
                 and x.username is not null
                 and x.SQL_ID = p_sql_id
                 and x.SQL_ID not in
                     (select x.sql_id
                        from gv$process                         v,
                             sys.gv_$sqlarea                    sqlarea,
                             gv$session                         x,
                             applsys.fnd_concurrent_requests    r,
                             applsys.FND_CONCURRENT_PROGRAMS    p,
                             applsys.FND_CONCURRENT_PROGRAMS_TL t
                       where 1 = 1
                         and x.paddr = v.addr
                         and r.oracle_process_id = v.spid
                         and x.sql_hash_value = sqlarea.hash_value
                         and x.sql_address = sqlarea.address
                         and x.username is not null
                         and r.concurrent_program_id =
                             p.concurrent_program_id
                         and p.concurrent_program_id =
                             t.concurrent_program_id
                         and x.SQL_ID = p_sql_id)
              union all
            select distinct null inst_id,
                              null sid,
                              -- x.SESSION_SERIAL#,
                              null concurrent_request_id,
                              null user_id,
                              x.client_id client_identifier,
                              null osuser,
                              null status,
                              null logon_time,
                              null spid_dbnode,
                              x.machine,
                              x.MODULE,
                              x.action,
                              x.program,
                              x.SQL_ID,
                              null optimizer_mode,
                              null hash_value,
                              null address,
                              to_Char(dbms_lob.substr(d.SQL_TEXT,1000,1)) sql_text, --1.2
                             -- dbms_lob.substr(d.sql_text, 1000, 1 ) sql_text,--1.1V start
                           -- to_char(d.sql_text) sql_text,  --1.1 end
                              null concurrent_program_name,
                              null argument_text,
                              null user_concurrent_program_name
                from sys.DBA_HIST_ACTIVE_SESS_HISTORY x,
                     sys.DBA_HIST_SQLTEXT             d
               where 1=1
                 and x.sql_id = d.sql_id
                 and x.sample_time between to_date(sysdate - 7) and
                     to_date(sysdate)
                 and x.SQL_ID = p_sql_id) loop
      l_ret_rec.inst_id                      := i.inst_id;
      l_ret_rec.sid                          := i.sid;
      l_ret_rec.concurrent_request_id        := i.concurrent_request_id;
      l_ret_rec.username                     := i.username;
      l_ret_rec.Client_Identifier            := i.client_identifier;
      l_ret_rec.Osuser                       := i.osuser;
      l_ret_rec.Status                       := i.status;
      l_ret_rec.logon_time                   := i.logon_time;
      l_ret_rec.spid_dbnode                  := i.spid_dbnode;
      l_ret_rec.Machine                      := i.machine;
      l_ret_rec.Module                       := i.module;
      l_ret_rec.Action                       := i.action;
      l_ret_rec.Program                      := i.program;
      l_ret_rec.Sql_Id                       := i.sql_id;
      l_ret_rec.Optimizer_Mode               := i.optimizer_mode;
      l_ret_rec.Hash_Value                   := i.hash_value;
      l_ret_rec.Address                      := i.address;
      l_ret_rec.Sql_Text                     := i.sql_text;
      l_ret_rec.concurrent_program_name      := i.concurrent_program_name;
      l_ret_rec.user_concurrent_program_name := i.user_concurrent_program_name;
      l_ret_rec.argument_text                := i.argument_text;
      pipe row(l_ret_rec);
    end loop;

  END;
  FUNCTION GET_CON_REQUEST_INFO(p_conc_request_id IN NUMBER DEFAULT NULL)
    return t_result_tab2
    pipelined as
    l_ret_rec t_result_rec2;
  BEGIN
    for i in (SELECT a.request_id,
                     a.phase_code,
                     a.status_code,
                     d.sid         as oracle_sid,
                     d.serial#,
                     d.osuser,
                     d.process,
                     c.spid        as os_process_id
                FROM apps.fnd_concurrent_requests  a,
                     apps.fnd_concurrent_processes b,
                     gv$process                    c,
                     gv$session                    d
               WHERE a.controlling_manager = b.concurrent_process_id
                 AND c.pid = b.oracle_process_id
                 AND b.session_id = d.audsid
                 AND a.phase_code = 'R'
                 AND a.status_code = 'R'
                 AND a.request_id = NVL(p_conc_request_id, a.request_id)) loop
      l_ret_rec.request_id    := i.request_id;
      l_ret_rec.phase_code    := i.phase_code;
      l_ret_rec.status_code   := i.status_code;
      l_ret_rec.oracle_sid    := i.oracle_sid;
      l_ret_rec.serial#       := i.serial#;
      l_ret_rec.osuser        := i.osuser;
      l_ret_rec.process       := i.process;
      l_ret_rec.os_process_id := i.os_process_id;
      pipe row(l_ret_rec);
    end loop;
  END;
  FUNCTION GET_SQL_INFO(p_oracle_sid IN NUMBER) return t_result_tab3
    pipelined as
    l_ret_rec t_result_rec3;
  BEGIN
    for i in (SELECT sid, sql_text
                FROM gv$session ses, gv$sqlarea sql
               WHERE ses.sql_hash_value = sql.hash_value(+)
                 AND ses.sql_address = sql.address(+)
                 AND ses.sid = p_oracle_sid) loop
      l_ret_rec.sid      := i.sid;
      l_ret_rec.sql_text := i.sql_text;
      pipe row(l_ret_rec);
    end loop;
  END;
  FUNCTION GET_CON_REQ_SQL_INFO(p_conc_request_id IN NUMBER)
    return t_result_tab4
    pipelined as
    l_ret_rec t_result_rec4;
  BEGIN
    for i in (SELECT DISTINCT sql.sql_text,
                     fcr.request_id,
                     fcr.phase_code,
                     fcr.status_code,
                     NULL as oracle_sid,
                     NULL as serial#,
                     NULL as osuser,
                     NULL as process,
                     NULL as os_process_id
                FROM apps.fnd_concurrent_requests  fcr,
                     apps.fnd_concurrent_processes fcp,
                     gv$process                    p,
                     gv$session                    ses,
                     gv$sqlarea                    sql
               WHERE fcr.controlling_manager = fcp.concurrent_process_id
                 AND p.pid = fcp.oracle_process_id
                 AND fcp.session_id = ses.audsid
                 AND fcr.phase_code = 'R'
                 AND fcr.status_code = 'R'
                 AND ses.sql_hash_value = sql.hash_value(+)
                 AND ses.sql_address = sql.address(+)
                 AND fcr.request_id = p_conc_request_id) loop
      l_ret_rec.sql_text      := i.sql_text;
      l_ret_rec.request_id    := i.request_id;
      l_ret_rec.phase_code    := i.phase_code;
      l_ret_rec.status_code   := i.status_code;
      l_ret_rec.oracle_sid    := i.oracle_sid;
      l_ret_rec.serial#       := i.serial#;
      l_ret_rec.osuser        := i.osuser;
      l_ret_rec.process       := i.process;
      l_ret_rec.os_process_id := i.os_process_id;
      pipe row(l_ret_rec);
    end loop;
  END; --GET_CON_REQ_SQL_INFO
end;
/