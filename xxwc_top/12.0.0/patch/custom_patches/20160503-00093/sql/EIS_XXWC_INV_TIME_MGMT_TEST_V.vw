---------------------------------------------------------------------------------------------------------------
/**************************************************************************************************************
  $Header XXEIS.EIS_XXWC_INV_TIME_MGMT_TEST_V $
  Module Name : Inventory
  PURPOSE	  : Time Sensitive Mgmt - WC
  TMS Task Id : 20160503-00093	 
  REVISIONS   :
  VERSION DATE               AUTHOR(S)       DESCRIPTION
  ------- -----------------  --------------- -----------------------------------------
  1.0     16-May-2016        Siva   		 TMS#20160503-00093	   Performance Tuning
**************************************************************************************************************/
DROP VIEW XXEIS.EIS_XXWC_INV_TIME_MGMT_TEST_V;

CREATE OR REPLACE VIEW XXEIS.EIS_XXWC_INV_TIME_MGMT_TEST_V (PROCESS_ID, INV_ORG_NAME, ORGANIZATION_ID, ORGANIZATION_CODE, BUYER, MASTER_ORG_CODE, SUBINVENTORY_CODE, LOCATOR_ID, LOCATOR, INVENTORY_ITEM_ID, ITEM, ITEM_DESCRIPTION, REVISION, UNIT_OF_MEASURE, ON_HAND, PRIMARY_BIN_LOC, ALTERNATE_BIN_LOC, OLDEST_BORN_DATE, SHELF_LIFE_DAYS, VENDOR_NAME, VENDOR_NUMBER, UNPACKED, PACKED, COST_GROUP_ID, LOT_NUMBER, SUBINVENTORY_STATUS_ID, LOCATOR_STATUS_ID, LOT_STATUS_ID, PLANNING_TP_TYPE, PLANNING_ORGANIZATION_ID, PLANNING_ORGANIZATION, OWNING_TP_TYPE, OWNING_ORGANIZATION_ID, OWNING_ORGANIZATION, LOT_EXPIRY_DATE, LPN, LPN_ID, STATUS_CODE, UNIT_NUMBER, SUBINVENTORY_TYPE, SECONDARY_INVENTORY_NAME, DATE_RECEIVED, ONHAND_QUANTITIES_ID, INVENTORY_LOCATION_ID, LANGUAGE, MIN_MINMAX_QUANTITY, MAX_MINMAX_QUANTITY, LIST_PRICE_PER_UNIT, MP_ORGANIZATION_ID, MIL_ORGANIZATION_ID, MSI_ORGANIZATION_ID , MSIV_INVENTORY_ITEM_ID, MSIV_ORGANIZATION_ID, MMS_STATUS_ID, OOD_ORGANIZATION_ID, OODW_ORGANIZATION_ID,
  OODP_ORGANIZATION_ID, HAOU_ORGANIZATION_ID, MLN_INVENTORY_ITEM_ID, MLN_ORGANIZATION_ID, MLN_LOT_NUMBER, REGION, DAYS_AGED, AT_RISK, AVGCOST, UBD, AT_RISK_N, SHORT_DATE)
AS
  SELECT PROCESS_ID,
    inv_org_name,
    organization_id,
    organization_code,
    buyer,
    master_org_code,
    subinventory_code,
    locator_id,
    LOCATOR,
    inventory_item_id,
    item,
    item_description,
    revision,
    unit_of_measure,
    on_hand,
    primary_bin_loc,
    alternate_bin_loc,
    oldest_born_date,
    shelf_life_days,
    vendor_name,
    vendor_number,
    UNPACKED,
    packed,
    cost_group_id,
    lot_number,
    subinventory_status_id,
    locator_status_id,
    lot_status_id,
    planning_tp_type,
    planning_organization_id,
    planning_organization,
    owning_tp_type,
    owning_organization_id,
    owning_organization,
    lot_expiry_date,
    lpn,
    lpn_id,
    status_code,
    unit_number,
    subinventory_type,
    secondary_inventory_name,
    date_received,
    onhand_quantities_id,
    inventory_location_id,
    language,
    min_minmax_quantity,
    max_minmax_quantity,
    list_price_per_unit,
    mp_organization_id,
    mil_organization_id,
    msi_organization_id,
    msiv_inventory_item_id,
    msiv_organization_id,
    mms_status_id,
    ood_organization_id,
    oodw_organization_id,
    oodp_organization_id,
    haou_organization_id,
    mln_inventory_item_id,
    mln_organization_id,
    mln_lot_number,
    region,
    days_aged,
    at_risk,
    avgcost,
    ubd,
    at_risk_n,
    short_date
  FROM XXEIS.eis_xxwc_inv_time_mgmt_tbl
/
