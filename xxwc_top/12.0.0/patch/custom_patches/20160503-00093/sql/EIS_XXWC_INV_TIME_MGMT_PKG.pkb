CREATE OR REPLACE 
PACKAGE BODY XXEIS.EIS_XXWC_INV_TIME_MGMT_PKG
AS
 --//============================================================================
--//  
--// Change Request 			:: Performance Issue  
--//
--// Object Usage 				:: This Object Referred by "Time Sensitive Mgmt - WC"
--//
--// Object Name         		:: XXEIS.EIS_XXWC_INV_TIME_MGMT_PKG
--//
--// Object Type         		:: Package Specification
--//
--// Object Description  		:: This Package will trigger in before report and insert the values into Table
--//
--// Version Control
--//============================================================================
--// Version    Author           Date          Description
--//----------------------------------------------------------------------------
--// 1.0        Siva  	      05-may-2016       Initial Build TMS#20160503-00093
--//============================================================================
  g_inventory_item_id NUMBER;
  g_organization_id   NUMBER;
  g_vendor_name       VARCHAR2(1000);
  g_vendor_number     VARCHAR2(1000);

FUNCTION get_person (p_person_id NUMBER) RETURN VARCHAR2
IS
--//============================================================================
--//
--// Object Name         :: get_person  
--//
--// Object Usage 		 :: This Object Referred by "Time Sensitive Mgmt - WC"
--//
--// Object Type         :: FUNCTION
--//
--// Object Description  :: This function gets the record based on parameter and insert into table.
--//
--// Version Control
--//============================================================================
--// Version    Author           Date          Description
--//----------------------------------------------------------------------------
--// 1.0         Venu        	05-May-2016       Initial Build 
--//============================================================================
l_sql VARCHAR2(1000);
l_person VARCHAR2(1000);
BEGIN 
l_sql := 'SELECT 
              max(ppf.full_name) full_name
            FROM per_people_f ppf
              WHERE ppf.person_id = :1
              AND (sysdate) between effective_start_date 
              and nvl(effective_end_date,sysdate)
              '
              ;
   EXECUTE IMMEDIATE l_sql
    INTO l_person
    USING p_person_id; 
    
 RETURN   l_person;
 
 EXCEPTION
 WHEN others THEN 
 RETURN NULL;
 END get_person;
              
PROCEDURE MAIN(
    p_process_id    NUMBER,
    p_organization  VARCHAR2,
    p_item          VARCHAR2,
    p_subinventory  VARCHAR2,
    p_region        VARCHAR2,
    p_supplier      VARCHAR2,
    p_location_list VARCHAR2,
    p_item_list     VARCHAR2,
    p_supplier_list VARCHAR2,
    p_at_risk       VARCHAR2)
AS
--//============================================================================
--//
--// Object Name         :: MAIN  
--//
--// Object Usage 		 :: This Object Referred by "Time Sensitive Mgmt - WC"
--//
--// Object Type         :: Procedure
--//
--// Object Description  :: This Procedure will call in before report and insert the values into Table
--//
--// Version Control
--//============================================================================
--// Version    Author           Date          Description
--//----------------------------------------------------------------------------
--// 1.0     Venu  			05-May-2016 		Initial Build   
--//============================================================================ 
TYPE tb_data_tab
IS
  TABLE OF XXEIS.eis_xxwc_inv_time_mgmt_tbl%rowtype INDEX BY binary_integer;
  l_tb_data_tab tb_data_tab;
  l_main_sql     VARCHAR2(32000);
  l_where_cond_1 VARCHAR2(10000);
TYPE cursor_type
IS
  REF
  CURSOR;
    refcursor_1 cursor_type ;
  BEGIN
    IF p_organization ='All' THEN
      l_where_cond_1 := l_where_cond_1 || ' AND 1=1';
    ELSE
      l_where_cond_1 := l_where_cond_1 || ' AND MP.organization_code IN (' || REPLACE (xxeis.eis_rs_utility.get_param_values (p_param_value => trim (p_organization ) ), '%%', ', ' ) || ')';
    END IF;
    IF p_item        IS NOT NULL THEN
      l_where_cond_1 := l_where_cond_1 || ' AND MSIV.concatenated_segments IN (' || REPLACE (xxeis.eis_rs_utility.get_param_values (p_param_value => trim (p_item ) ), '%%', ', ' ) || ')';
    END IF;
    IF p_subinventory IS NOT NULL THEN
      l_where_cond_1  := l_where_cond_1 || ' AND MOQ.subinventory_code IN (' || REPLACE (xxeis.eis_rs_utility.get_param_values (p_param_value => trim (p_subinventory ) ), '%%', ', ' ) || ')';
    END IF;
    IF p_region      IS NOT NULL THEN
      l_where_cond_1 := l_where_cond_1 || ' AND MP.ATTRIBUTE9  IN (' || REPLACE (xxeis.eis_rs_utility.get_param_values (p_param_value => trim (p_region ) ), '%%', ', ' ) || ')';
    END IF;
    IF p_at_risk      ='All' THEN
      l_where_cond_1 := l_where_cond_1 || ' AND 1=1';
    elsif p_at_risk   ='Y' THEN
      l_where_cond_1 := l_where_cond_1 || ' AND TO_CHAR(TO_DATE(MOQ.orig_date_received)+MSIV.shelf_life_days,''DD-Mon-YYYY'')<SYSDATE';
    ELSE
      l_where_cond_1 := l_where_cond_1 || ' AND TO_CHAR(TO_DATE(MOQ.orig_date_received)+MSIV.shelf_life_days,''DD-Mon-YYYY'')>=SYSDATE';
    END IF;
    
    IF p_supplier    IS NOT NULL THEN
      l_where_cond_1 := l_where_cond_1 || ' AND EXISTS
                        (SELECT 1
                      FROM mrp_sr_assignments ass,
                          mrp_sr_receipt_org rco,
                          mrp_sr_source_org sso,
                          po_vendors pov
                    WHERE 1                  =1
                    AND ass.inventory_item_id=MSIV.inventory_item_id
                    AND ass.organization_id  =MSIV.organization_id
                    AND rco.sourcing_rule_id =ass.sourcing_rule_id
                    AND sso.sr_receipt_id    =rco.sr_receipt_id
                    AND pov.vendor_id        =sso.vendor_id     
                    AND POV.vendor_name   IN (' || REPLACE (xxeis.eis_rs_utility.get_param_values (p_param_value => trim (p_supplier ) ), '%%', ', ' ) || '))';
    END IF;
    IF p_location_list IS NOT NULL THEN
      l_where_cond_1   := l_where_cond_1 || ' AND EXISTS
                          (SELECT
                            /*+INDEX(XXWC_PARAM_LIST XXWC_PARAM_LIST_N1)*/
                               1
                          FROM APPS.xxwc_param_list
                          WHERE 1=1
                          AND LIST_TYPE=''Org''
                          AND DBMS_LOB.INSTR(list_values,MP.organization_code, 1, 1)<>0                        
                          AND LIST_NAME  IN (' || REPLACE (xxeis.eis_rs_utility.get_param_values (p_param_value => trim (p_location_list ) ), '%%', ', ' ) || '))';
    END IF;
    
    IF p_item_list   IS NOT NULL THEN
      l_where_cond_1 := l_where_cond_1 || 'AND EXISTS
                      (SELECT
                      /*+INDEX(XXWC_PARAM_LIST XXWC_PARAM_LIST_N1)*/
                          1
                      FROM APPS.xxwc_param_list
                      WHERE 1=1
                      AND LIST_TYPE=''Item''
                      AND DBMS_LOB.INSTR(LIST_VALUES,MSIV.concatenated_segments, 1, 1)<>0
                      AND LIST_NAME IN (' || REPLACE (xxeis.eis_rs_utility.get_param_values (p_param_value => trim (p_item_list ) ), '%%', ', ' ) || '))';
    END IF;
   
    IF p_supplier_list IS NOT NULL THEN
        l_where_cond_1   := l_where_cond_1 || ' AND EXISTS
                    (SELECT 1
                    FROM mrp_sr_assignments ass,
                      mrp_sr_receipt_org rco,
                      mrp_sr_source_org SSO,
                      po_vendors POV
                    WHERE 1                  =1
                    AND ass.inventory_item_id=MSIV.inventory_item_id
                    AND ass.organization_id  =MSIV.organization_id
                    AND rco.sourcing_rule_id =ass.sourcing_rule_id
                    AND SSO.SR_RECEIPT_ID    =RCO.SR_RECEIPT_ID
                    AND POV.VENDOR_ID        =SSO.VENDOR_ID
                    AND EXISTS
                      (SELECT
                        /*+INDEX(XXWC_PARAM_LIST XXWC_PARAM_LIST_N1)*/
                        1
                      FROM APPS.xxwc_param_list,
                        po_vendors VEND
                      WHERE list_name                                     ='''||p_supplier_list||'''
                      AND VEND.vendor_id                                  =POV.vendor_id
                      AND list_type                                       =''Supplier''
                      AND DBMS_LOB.INSTR(list_values,VEND.segment1, 1, 1)<>0
                      )
                    )';
    END IF;
    
    l_main_sql :='SELECT ' ||p_process_id||',
            HAOU.name inv_org_name,
            MOQ.organization_id organization_id,
            MP.organization_code organization_code,
            xxeis.eis_xxwc_inv_time_mgmt_pkg.get_person(MSIV.buyer_id)  buyer,
            OOD.organization_code master_org_code,
            MOQ.subinventory_code subinventory_code,
            MOQ.locator_id locator_id,
            MIL.concatenated_segments LOCATOR,
            MOQ.inventory_item_id inventory_item_id,
            MSIV.concatenated_segments item,
            MSIV.description item_description,
            MOQ.revision revision,
            MUOM.unit_of_measure,
            MOQ.primary_transaction_quantity on_hand,
            xxeis.eis_rs_xxwc_com_util_pkg.get_primary_bin_loc ( MSIV.inventory_item_id, MSIV.organization_id) primary_bin_loc,
            XXEIS.EIS_RS_XXWC_COM_UTIL_PKG.GET_ALTERNATE_BIN_LOC ( MSIV.INVENTORY_ITEM_ID, MSIV.ORGANIZATION_ID) ALTERNATE_BIN_LOC,
            MOQ.orig_date_received oldest_born_date,
            MSIV.shelf_life_days shelf_life_days,
            xxeis.eis_po_xxwc_isr_util_qa_pkg.get_vendor_name ( MSIV.inventory_item_id, MSIV.organization_id) vendor_name,
            xxeis.eis_po_xxwc_isr_util_qa_pkg.get_vendor_number ( MSIV.inventory_item_id, MSIV.organization_id) vendor_number,
            DECODE (MOQ.containerized_flag, 1, 0, MOQ.primary_transaction_quantity) unpacked,
            DECODE (MOQ.containerized_flag, 1, MOQ.primary_transaction_quantity, 0) packed,
            MOQ.cost_group_id cost_group_id,
            MOQ.lot_number lot_number,
            -- TO_CHAR (NULL) serial_number,
            --MIL.project_id project_id,
            --MIL.task_id task_id,
            MSI.status_id subinventory_status_id,
            MIL.status_id locator_status_id,
            MLN.status_id lot_status_id,
            MOQ.planning_tp_type planning_tp_type,
            MOQ.planning_organization_id planning_organization_id,
            OODP.organization_name planning_organization,
            MOQ.owning_tp_type owning_tp_type,
            MOQ.owning_organization_id owning_organization_id,
            OODW.organization_name owning_organization,
            MLN.expiration_date lot_expiry_date,
            MLV.lpn,
            MOQ.lpn_id,
            MMS.status_code,
            PUN.unit_number,
            MSI.subinventory_type,
            MSI.secondary_inventory_name,
            MOQ.date_received,
            MOQ.onhand_quantities_id,
            MIL.inventory_location_id,
            MUOM.language,
            MSIV.MIN_MINMAX_QUANTITY,
            MSIV.MAX_MINMAX_QUANTITY,
            MSIV.LIST_PRICE_PER_UNIT,
            -- primary keys added for component_joins
            MP.organization_id mp_organization_id,
            MIL.organization_id mil_organization_id,
            MSI.organization_id msi_organization_id,
            MSIV.inventory_item_id msiv_inventory_item_id,
            MSIV.organization_id msiv_organization_id,
            MMS.status_id mms_status_id,
            OOD.organization_id ood_organization_id,
            OODW.organization_id oodw_organization_id,
            OODP.ORGANIZATION_ID OODP_ORGANIZATION_ID,
            HAOU.ORGANIZATION_ID HAOU_ORGANIZATION_ID,
            MLN.INVENTORY_ITEM_ID MLN_INVENTORY_ITEM_ID,
            MLN.ORGANIZATION_ID MLN_ORGANIZATION_ID,
            MLN.LOT_NUMBER MLN_LOT_NUMBER,
            MP.ATTRIBUTE9 REGION,
            ROUND (TRUNC (SYSDATE) - TRUNC (MOQ.orig_date_received)) Days_Aged,
            CASE
              WHEN (NVL (MSIV.shelf_life_days, 0) * 0.75) > ROUND (TRUNC (SYSDATE) - TRUNC (MOQ.orig_date_received))
              THEN ''N''
              ELSE ''Y''
            END At_risk,
            NVL ( apps.cst_cost_api.get_item_cost (1, MSIV.inventory_item_id, MSIV.organization_id), 0) avgcost
            --Start of Ver 1.2
            ,
            TO_DATE(TO_CHAR(TO_DATE(MOQ.orig_date_received)+MSIV.shelf_life_days,''DD-Mon-YYYY'')) UBD ,
            CASE
              WHEN TO_CHAR(TO_DATE(MOQ.orig_date_received)+MSIV.shelf_life_days,''DD-Mon-YYYY'')<SYSDATE
              THEN ''Y''
              ELSE ''N''
            END AT_RISK_N ,
            CASE
              WHEN TO_CHAR(TO_DATE(MOQ.orig_date_received)+MSIV.shelf_life_days,''DD-Mon-YYYY'') BETWEEN sysdate-.046875 AND SYSDATE+90
              THEN ''Y''
              ELSE ''N''
            END SHORT_DATE
          FROM mtl_parameters MP,
            mtl_item_locations_kfv MIL,
            mtl_secondary_inventories MSI,
            mtl_lot_numbers MLN,
            mtl_system_items_kfv MSIV,
            mtl_onhand_quantities_detail MOQ,
            mtl_units_of_measure_tl MUOM,
            mtl_onhand_lpn_v MLV,
            mtl_material_statuses_vl MMS,
            pjm_unit_numbers PUN,
            org_organization_definitions OOD,
            org_organization_definitions OODW,
            org_organization_definitions OODP,
            hr_all_organization_units HAOU            
          WHERE MOQ.organization_id         = MP.organization_id
          AND MOQ.organization_id           = MIL.organization_id(+)
          AND MOQ.locator_id                = MIL.inventory_location_id(+)
          AND MOQ.organization_id           = MSI.organization_id
          AND MOQ.subinventory_code         = MSI.secondary_inventory_name
          AND MOQ.organization_id           = MLN.organization_id(+)
          AND MOQ.inventory_item_id         = MLN.inventory_item_id(+)
          AND MOQ.lot_number                = MLN.lot_number(+)
          --AND MSIV.buyer_id                 = ppf.person_id(+)
          AND MOQ.organization_id           = MSIV.organization_id
          AND MOQ.inventory_item_id         = MSIV.inventory_item_id
          AND MUOM.uom_code                 = MSIV.primary_uom_code
          AND MUOM.language                 = USERENV (''LANG'')
          AND MLV.lpn_id(+)                 = MOQ.lpn_id
          AND MMS.status_id(+)              = MIL.status_id
          AND PUN.MASTER_ORGANIZATION_ID(+) = MOQ.ORGANIZATION_ID
          AND MP.master_organization_id     = OOD.organization_id
          AND MOQ.planning_organization_id  = OODP.organization_id
          AND MOQ.owning_organization_id    = OODW.organization_id
          AND HAOU.organization_id          = MSI.organization_id
--          AND ( (MSIV.shelf_life_days       > 0  
--          AND MSIV.shelf_life_days          < 10000)
--          OR MOQ.lot_number                IS NOT NULL)  --TMS#20160315-00168 Commented by siva
		  AND EXISTS  --TMS#20160315-00168  added by siva
          (SELECT 1
          FROM mtl_categories_kfv Mcv,
            mtl_category_sets MCS,
            mtl_item_categories MIC
          WHERE Mcs.Category_Set_Name   =''Time Sensitive''
          AND Mcs.Structure_Id          = Mcv.Structure_Id
          AND Mic.Inventory_Item_Id     = MSIV.inventory_item_id
          AND Mic.Organization_Id       = MSIV.organization_id
          AND MIC.category_set_id       = MCS.category_set_id
          and MIC.category_id           = MCV.category_id
          AND MCV.concatenated_segments =''Yes'' )'  
    ;
    
    
    l_main_sql :=l_main_sql||' '||l_where_cond_1;
    
    fnd_file.put_line(fnd_file.LOG,'L_MAIN_SQL '||l_main_sql);
    
    OPEN refcursor_1 FOR l_main_sql;
    loop
      fetch refcursor_1 BULK COLLECT INTO l_tb_data_tab LIMIT 10000;
    IF l_tb_data_tab.count>0 THEN
      forall i IN 1..l_tb_data_tab.count
      INSERT INTO xxeis.EIS_XXWC_INV_TIME_MGMT_TBL VALUES l_tb_data_tab(i);
    END IF;
    exit
  WHEN refcursor_1%notfound;
  END loop;
  
  CLOSE refcursor_1;
EXCEPTION
WHEN others THEN
  fnd_file.put_line(fnd_file.LOG,' error in main procedure'||sqlerrm);
END MAIN;

PROCEDURE clear_table
  (
    p_process_id NUMBER
  )
AS
--//============================================================================
--//
--// Object Name         :: clear_table  
--//
--// Object Usage 		 :: This Object Referred by "Time Sensitive Mgmt - WC"
--//
--// Object Type         :: Procedure
--//
--// Object Description  :: This Procedure will call in Afteer report and delete data from Table
--//
--// Version Control
--//============================================================================
--// Version    Author           Date          Description
--//----------------------------------------------------------------------------
--// 1.0     Venu  				05-May-2016 	Initial Build   
--//============================================================================
  BEGIN  
  DELETE FROM XXEIS.EIS_XXWC_INV_TIME_MGMT_TBL WHERE PROCESS_ID=P_PROCESS_ID;
  COMMIT; 
END clear_table;

END eis_xxwc_inv_time_mgmt_pkg;
/
