/*******************************************************************
script name: Task ID: TMS_20180612-00104.sql
purpose: datascript for fnd_lob sequence
revision history:
Version Date Aurthor Description
----------------------------------------------------------------
1.0 13-JUN-2018 Pattabhi Avula Task ID: 20180612-00104
*******************************************************************/
SET serveroutput ON SIZE 1000000;
DECLARE 
l_last_number          NUMBER;
l_fnd_lob_file_id      NUMBER;
l_next_val             NUMBER;
BEGIN
-- Alter the increment:
EXECUTE IMMEDIATE 'alter sequence APPLSYS.FND_LOBS_S increment by 5000';
-- Engage the increment:
--select FND_LOBS_S.NEXTVAL from DUAL;
-- Run the following 2 queries again to verify last_number column in dba_sequences is greater than max(file_id) in fnd_lobs table
 BEGIN 
   SELECT FND_LOBS_S.NEXTVAL 
     INTO l_next_val 
	 FROM DUAL;
	 
   SELECT last_number 
     INTO l_last_number 
	 FROM dba_sequences
    WHERE sequence_name = 'FND_LOBS_S';
	
    dbms_output.put_line('last_number' || l_last_number);

    SELECT MAX(FILE_ID) 
	  INTO l_fnd_lob_file_id 
	  FROM fnd_lobs; 
    dbms_output.put_line('file id' || l_fnd_lob_file_id);

 END;
--If yes now reverse the increment of sequence 
  IF l_last_number > l_fnd_lob_file_id 
   THEN 
--Alter the increment back to only go by 1
    EXECUTE IMMEDIATE'alter sequence APPLSYS.FND_LOBS_S increment by 1';
  ELSE
   dbms_output.put_line('l_last_number NOT > l_fnd_lob_file_id, Please check the script output for values');
  END IF;
END;
/