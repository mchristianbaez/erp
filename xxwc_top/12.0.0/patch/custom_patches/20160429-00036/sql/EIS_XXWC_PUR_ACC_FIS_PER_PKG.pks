create or replace package       XXEIS.EIS_XXWC_PUR_ACC_FIS_PER_PKG as
 --//============================================================================
--//  
--// Change Request 			:: Performance Issue  
--//
--// Object Usage 				:: This Object Referred by "PURCHASES AND ACCRUALS - By VENDOR, LOB, BU,AGREEMENT YEAR, FISCAL PERIOD"
--//
--// Object Name         		:: EIS_XXWC_PUR_ACC_FIS_PER_PKG
--//
--// Object Type         		:: Package Spec
--//
--// Object Description  		:: This Package will trigger in before report and insert the values into Table
--//
--// Version Control
--//============================================================================
--// Version    Author           Date          Description
--//----------------------------------------------------------------------------
--// 1.0        PRAMOD  	   04/29/2016      Initial Build  --TMS#20160429-00036  --Performance Tuning
--//============================================================================
procedure GET_PUR_ACC_FIS_PER_DTLS (	p_process_id 	in number,
								P_Vendor 	in varchar2,
								p_Lob			in varchar2,
								P_MVID			in varchar2,
								P_Fiscal_Period in varchar2,
								P_Agreement_Year	in number
							);
--//============================================================================
--//
--// Object Name         :: get_pur_acc_fis_per_dtls  
--//
--// Object Usage 		 :: This Object Referred by "PURCHASES AND ACCRUALS - By VENDOR, LOB, BU,AGREEMENT YEAR, FISCAL PERIOD"
--//
--// Object Type         :: Procedure
--//
--// Object Description  :: This  procedure is created based on the EIS_XXWC_PUR_PETSQO_V View.Due to the 
--// 						performance issue ,spilt the view definition based on the driving tables and driving data.
--//
--// Version Control
--//============================================================================
--// Version    Author           Date          Description
--//----------------------------------------------------------------------------
--// 1.0        PRAMOD  	  04/29/2016       Initial Build  --TMS#20160429-00036  --Performance Tuning
--//============================================================================
 type CURSOR_TYPE4 is ref cursor;
 
type T_ARRAY is table of varchar2(32000)
   INDEX BY BINARY_INTEGER;
   
procedure clear_temp_tables ( p_process_id in number); 
	 --//============================================================================
--//
--// Object Name         :: clear_temp_tables
--//
--// Object Type         :: Procedure
--//
--// Object Description  :: This procedure will trigger in After Report level and delete all the records from the table based on that particular process_id.
--//
--// Version Control
--//============================================================================
--// Version    Author           Date          Description
--//----------------------------------------------------------------------------
--// 1.0        PRAMOD  	   04/29/2016      Initial Build --TMS#20160429-00036  --Performance Tuning
--//============================================================================
END;
/
