CREATE OR REPLACE PACKAGE BODY APPS.XXWC_EGO_WORKFLOW_PKG
AS
   /*******************************************************************************************************************
   -- File Name: XXWC_EGO_WORKFLOW_PKG.pkb
   --
   -- PROGRAM TYPE: PL/SQL Script   <API>
   --
   -- PURPOSE:
   -- HISTORY
   -- =================================================================================================================
   -- =================================================================================================================
   -- VERSION DATE          AUTHOR(S)       DESCRIPTION
   -- ------- -----------   --------------- ---------------------------------------------------------------------------
   -- 1.0     01-MAR-2015   Shankar Vanga   TMS#20150126-00044  - Create Single Item Workflow Tool
   -- 2.0     26-May-2015   P.vamshidhar    TMS#20150519-00091 - Single Item UI Enhancements
                                            Added new Procedure SET_STEP_ACT_OPTIONS to show workflow
                                            notification subject with vendor name.

   --3.0      09-Jun-2015   P.Vamshidhar    TMS#20150608-00031 -- Added new Procedures/function
                                            XXWC_WF_ASSIGNEE_FUNC
                                            SELECT_STD_REVIEWERS
                                            Update_Change_line

   --4.0      14-Aug-2015   P.Vamshidhar    TMS#20150708-00061 - PCAM enhancements part 3

   --5.0      19-OCT-2015   P.vamshidhar    TMS#20150817-00043 - EHS role correction.
                                            PROCEDURE : SELECT_STEP_PEOPLE
                                            TMS#20150901-00146 - PCAM add delete functionality for Cross References
                                            Removed un-wanted messages.

   --6.0      18-Mar-2016   P.Vamshidhar    TMS#20160317-00046 New item requests not routing to some CMs
   ********************************************************************************************************************/


   g_debug_file       UTL_FILE.FILE_TYPE;
   g_debug_flag       BOOLEAN := FALSE;                 -- For Debug, set TRUE
   g_output_dir       VARCHAR2 (240) := NULL;
   g_debug_filename   VARCHAR2 (30) := 'EngChangeWorkflowUtil.log';
   g_debug_errmesg    VARCHAR2 (400);

   g_err_callfrom     VARCHAR2 (100) := 'XXWC_EGO_WORKFLOW_PKG';
   g_distro_list      VARCHAR2 (100) := 'HDSOracleDevelopers@hdsupply.com';



   PROCEDURE get_rpm (itemtype    IN            VARCHAR2,
                      itemkey     IN            VARCHAR2,
                      actid       IN            NUMBER,
                      funcmode    IN            VARCHAR2,
                      resultout   IN OUT NOCOPY VARCHAR2)
   IS
      /*******************************************************************************************************************************
               PROCEDURE : get_rpm

                 REVISIONS:
                 Ver        Date        Author                     Description
                 ---------  ----------  ---------------    ----------------------------------------------------------------------------------
                 1.0        01-MAR-2015   Shankar Vanga     TMS#20150126-00044  - Create Single Item Workflow Tool
                                                            Initial Version
              -- 2.0        26-May-2015   P.vamshidhar     TMS#20150519-00091 - Single Item UI Enhancements
                                                           Added new Procedure SET_STEP_ACT_OPTIONS to show workflow
                                                           notification subject with vendor name.

      *********************************************************************************************************************************/

      l_result_out      VARCHAR2 (30);
      l_return_status   VARCHAR2 (30);
      l_line_id         NUMBER;
      l_user_name       VARCHAR2 (250);
      l_change_name     VARCHAR2 (200);
      l_status          VARCHAR2 (50);

      l_msg_count       NUMBER;
      l_msg_data        VARCHAR2 (200);
      l_requestor       VARCHAR2 (200);
      l_rpm_approver    VARCHAR2 (200);
   BEGIN
      --
      -- RUN mode - normal process execution
      --
      IF (funcmode = 'RUN')
      THEN
         l_status :=
            wf_engine.GetItemAttrText (itemtype   => itemtype,
                                       itemkey    => itemkey,
                                       aname      => 'STATUS');

         l_change_name :=
            wf_engine.GetItemAttrText (itemtype   => itemtype,
                                       itemkey    => itemkey,
                                       aname      => 'CHANGE_NAME');

         -- Added below code by vamshi in TMS#20150519-00091 -- START

         xxwc_pim_chg_item_mgmt.xxwc_ego_vendor_insert (
            FND_PROFILE.VALUE ('LOGIN_ID'),
            NULL,
            l_change_name,
            'U');

         -- Added above code by vamshi in TMS#20150519-00091 -- END


         IF l_status = 'RPM Approval'
         THEN
            BEGIN
               ---Code changes  here..Ram
               SELECT a1.user_name
                 INTO l_rpm_approver
                 FROM fnd_user A1, apps.PER_ALL_PEOPLE_F F1
                WHERE     F1.person_id = A1.employee_id
                      --- added by Ram to check the full name, from the db..expecting the setups to be correct as well
                      AND f1.full_name =
                             (SELECT b_cat.attribute3
                                FROM apps.FND_FLEX_VALUES B_cat,
                                     mtl_parameters mp
                               WHERE     b_cat.value_category =
                                            'XXWC_DISTRICT_DFF'
                                     AND b_cat.flex_value = mp.attribute8
                                     AND mp.organization_id =
                                            (SELECT orgs.organization_id
                                               FROM apps.PER_PEOPLE_F ppf,
                                                    apps.PER_ASSIGNMENTS_V7 per,
                                                    apps.HR_LOCATIONS_ALL HRL,
                                                    apps.FND_USER FUSE,
                                                    apps.mtl_parameters orgs
                                              WHERE     fuse.employee_id =
                                                           ppf.person_id(+)
                                                    AND ppf.person_id =
                                                           per.person_id(+)
                                                    AND ppf.effective_end_date >
                                                           SYSDATE
                                                    AND per.location_id =
                                                           hrl.location_id(+)
                                                    AND hrl.inventory_organization_id =
                                                           orgs.organization_id(+)
                                                    AND fuse.user_id =
                                                           (SELECT created_by
                                                              FROM eng_engineering_changes
                                                             WHERE change_notice =
                                                                      l_change_name)));
            EXCEPTION
               WHEN OTHERS
               THEN
                  ---Need to send the same to a group..rownum =1,suparna gets for now based on relationship status

                  SELECT member_user_name
                    INTO l_rpm_approver
                    FROM APPS.EGO_GROUP_MEMBERS_V
                   WHERE     group_name LIKE 'WC Regional Purchasing Manager'
                         AND ROWNUM = 1;
            END;


            wf_engine.SetItemAttrText (itemtype   => itemtype,
                                       itemkey    => itemkey,
                                       aname      => 'XXHDS_RPM_APPROVER',
                                       avalue     => l_rpm_approver);

            resultout := 'COMPLETE';
            RETURN;
         END IF;
      END IF;
   EXCEPTION
      WHEN OTHERS
      THEN
         -- The line below records this function call in the error system
         -- in the case of an exception.
         wf_core.context ('XXWC_EGO_WORKFLOW_PKG',
                          'GET_RPM',
                          itemtype,
                          itemkey,
                          TO_CHAR (actid),
                          funcmode);

         RAISE;
   END get_rpm;

   PROCEDURE update_status (itemtype    IN            VARCHAR2,
                            itemkey     IN            VARCHAR2,
                            actid       IN            NUMBER,
                            funcmode    IN            VARCHAR2,
                            resultout   IN OUT NOCOPY /* file.sql.39 change */
                                                      VARCHAR2)
   IS
      /*******************************************************************************************************************************
               PROCEDURE : update_status

                 REVISIONS:
                 Ver        Date        Author                     Description
                 ---------  ----------  ---------------    ----------------------------------------------------------------------------------
                 1.0        01-MAR-2015   Shankar Vanga     TMS#20150126-00044  - Create Single Item Workflow Tool
                                                            Initial Version

      *********************************************************************************************************************************/

      l_user_id     NUMBER;
      l_user_name   VARCHAR2 (100);
      l_action_id   NUMBER;
      l_noti_id     NUMBER;
      l_status      VARCHAR2 (50);
   BEGIN
      IF (funcmode = 'RUN')
      THEN
         l_status :=
            wf_engine.GetItemAttrText (itemtype   => itemtype,
                                       itemkey    => itemkey,
                                       aname      => 'STATUS');


         IF (l_status = 'RPM Approval')
         THEN
            resultout := 'COMPLETE:Y';
         ELSIF (l_status = 'CM Approval')
         THEN
            resultout := 'COMPLETE:N';
         END IF;
      END IF;
   EXCEPTION
      WHEN OTHERS
      THEN
         -- The line below records this function call in the error system
         -- in the case of an exception.
         wf_core.context ('XXWC_EGO_WORKFLOW_PKG',
                          'get_rpm',
                          itemtype,
                          itemkey,
                          TO_CHAR (actid),
                          funcmode);

         RAISE;
   END update_status;

   PROCEDURE skip_worklfow_process (itemtype    IN            VARCHAR2,
                                    itemkey     IN            VARCHAR2,
                                    actid       IN            NUMBER,
                                    funcmode    IN            VARCHAR2,
                                    resultout   IN OUT NOCOPY /* file.sql.39 change */
                                                              VARCHAR2)
   IS
      /*******************************************************************************************************************************
               PROCEDURE : skip_worklfow_process

                 REVISIONS:
                 Ver        Date        Author                     Description
                 ---------  ----------  ---------------    ----------------------------------------------------------------------------------
                 1.0        01-MAR-2015   Shankar Vanga     TMS#20150126-00044  - Create Single Item Workflow Tool
                                                            Initial Version

                 6.0      18-Mar-2016   P.Vamshidhar    TMS#20160317-00046 New item requests not routing to some CMs

      *********************************************************************************************************************************/

      l_result_out      VARCHAR2 (30);
      l_return_status   VARCHAR2 (30);
      l_line_id         NUMBER;
      l_user_name       VARCHAR2 (250);
      l_change_id       NUMBER;
      l_status          VARCHAR2 (50);
      l_change_name     VARCHAR2 (200);
      l_tier            VARCHAR2 (200) DEFAULT NULL;
      l_cat_mgr         VARCHAR2 (200) DEFAULT NULL;
   BEGIN
      --
      -- RUN mode - normal process execution
      --
      IF (funcmode = 'RUN')
      THEN
         BEGIN
            l_status :=
               wf_engine.GetItemAttrText (itemtype   => itemtype,
                                          itemkey    => itemkey,
                                          aname      => 'STATUS');

            l_change_name :=
               wf_engine.GetItemAttrText (itemtype   => itemtype,
                                          itemkey    => itemkey,
                                          aname      => 'CHANGE_NAME');

            IF l_status = 'CM Approval'
            THEN
               -- Find Category Manager, id changed from description, assumption all users are set as emplpoyee..
               SELECT user_name
                 INTO l_cat_mgr
                 FROM fnd_user
                WHERE employee_id =
                         (SELECT cat_mgr.person_id
                            FROM apps.MTL_CATEGORIES_V mcv,
                                 apps.FND_FLEX_VALUES B_cat,
                                 apps.FND_FLEX_VALUES_TL T_cat,
                                 apps.FND_FLEX_VALUE_SETS V_cat,
                                 apps.FND_FLEX_VALUES B_sub_cat,
                                 apps.FND_FLEX_VALUES_TL T_sub_cat,
                                 apps.FND_FLEX_VALUE_SETS V_sub_cat,
                                 apps.FND_FLEX_VALUES B_prod_line,
                                 apps.FND_FLEX_VALUES_TL T_prod_line,
                                 apps.FND_FLEX_VALUE_SETS V_prod_line,
                                 --apps.PER_ALL_PEOPLE_F cat_dir,  Commented in 6.0
                                 --apps.PER_ALL_PEOPLE_F cat_mgr Commented in 6.0
                                 -- v6.0 Start
                                 (SELECT person_id
                                    FROM apps.PER_ALL_PEOPLE_F
                                   WHERE SYSDATE BETWEEN EFFECTIVE_START_DATE
                                                     AND EFFECTIVE_END_DATE)
                                 cat_dir,
                                 (SELECT person_id
                                    FROM apps.PER_ALL_PEOPLE_F
                                   WHERE SYSDATE BETWEEN EFFECTIVE_START_DATE
                                                     AND EFFECTIVE_END_DATE)
                                 cat_mgr                           -- V6.0 End
                           WHERE     mcv.structure_name = 'Item Categories'
                                 AND b_cat.FLEX_VALUE_ID =
                                        t_cat.FLEX_VALUE_ID
                                 AND b_cat.FLEX_VALUE_SET_ID =
                                        v_cat.flex_value_set_id
                                 AND v_cat.flex_value_set_name IN ('XXWC_CATMGT_CATEGORIES')
                                 AND b_cat.flex_value = mcv.attribute5
                                 AND b_sub_cat.FLEX_VALUE_ID =
                                        t_sub_cat.FLEX_VALUE_ID
                                 AND b_sub_cat.FLEX_VALUE_SET_ID =
                                        v_sub_cat.flex_value_set_id
                                 AND v_sub_cat.flex_value_set_name IN ('XXWC_CATMGT_SUBCATEGORIES')
                                 AND b_sub_cat.flex_value = mcv.attribute6
                                 AND b_prod_line.FLEX_VALUE_ID =
                                        t_prod_line.FLEX_VALUE_ID
                                 AND b_prod_line.FLEX_VALUE_SET_ID =
                                        v_prod_line.flex_value_set_id
                                 AND v_prod_line.flex_value_set_name IN ('XXWC_CATMGT_PRODUCT_LINES')
                                 AND b_prod_line.flex_value = mcv.attribute7
                                 AND B_sub_cat.attribute1 =
                                        cat_dir.person_id(+)
                                 AND B_sub_cat.attribute2 =
                                        cat_mgr.person_id(+)
                                 AND MCV.CATEGORY_CONCAT_SEGS =
                                        (SELECT global_attribute1
                                           FROM XXWC_MTL_SY_ITEMS_CHG_B item,
                                                eng_engineering_changes chg
                                          WHERE     item.oracle_change_id =
                                                       chg.change_id
                                                AND chg.change_notice =
                                                       l_change_name));

               BEGIN
                  --Changes requried here not to check for tier

                  wf_engine.SetItemAttrText (itemtype   => itemtype,
                                             itemkey    => itemkey,
                                             aname      => 'XXWC_CM_APPROVER',
                                             avalue     => l_cat_mgr);

                  resultout := 'COMPLETE:Y';
                  RETURN;
               EXCEPTION
                  WHEN OTHERS
                  THEN
                     wf_engine.SetItemAttrText (
                        itemtype   => itemtype,
                        itemkey    => itemkey,
                        aname      => 'XXWC_CM_APPROVER',
                        avalue     => l_cat_mgr);

                     resultout := 'COMPLETE:N';
                     RETURN;
               END;
            END IF;
         EXCEPTION
            WHEN OTHERS
            THEN
               resultout := 'COMPLETE:N';
               RETURN;
         END;
      END IF;
   END skip_worklfow_process;

   PROCEDURE SELECT_HAZMAT_ITEM (itemtype   IN            VARCHAR2,
                                 itemkey    IN            VARCHAR2,
                                 actid      IN            NUMBER,
                                 funcmode   IN            VARCHAR2,
                                 result     IN OUT NOCOPY VARCHAR2)
   IS
      /*******************************************************************************************************************************
               PROCEDURE : SELECT_HAZMAT_ITEM

                 REVISIONS:
                 Ver        Date        Author                     Description
                 ---------  ----------  ---------------    ----------------------------------------------------------------------------------
                 1.0        01-MAR-2015   Shankar Vanga     TMS#20150126-00044  - Create Single Item Workflow Tool
                                                            Initial Version
                 2.0        26-May-2015   P.vamshidhar      TMS#20150519-00091 - Single Item UI Enhancements
                                                            Added new Procedure SET_STEP_ACT_OPTIONS to show workflow
                                                            notification subject with vendor name.

                 5.0        19-OCT-2015   P.vamshidhar      TMS#20150817-00043 - EHS role correction.
                                                            TMS#20150901-00146 - PCAM add delete functionality for Cross References
                                                            Commented un-wanted variables.
      *********************************************************************************************************************************/

      l_return_status   VARCHAR2 (1);
      l_msg_count       NUMBER;
      l_msg_data        VARCHAR2 (200);
      l_change_name     VARCHAR2 (200);
      l_status          VARCHAR2 (200);
      l_hazmat_flag     VARCHAR2 (1) DEFAULT NULL;
      L_COUNT           NUMBER;           --For checking base and custom table
      --L_CHG_COUNT       NUMBER;  -- Commented in 5.0 Rev
      L_ERRO_MSG        VARCHAR2 (4000);
      --L_CHANGE_ID       NUMBER; -- Commented in 5.0 Rev
      --L_ITEM_COUNT      NUMBER; -- Commented in 5.0 Rev
      l_item_name       VARCHAR2 (50);
   BEGIN
      --
      -- RUN mode - normal process execution
      --
      IF (funcmode = 'RUN')
      THEN
         BEGIN
            l_status :=
               wf_engine.GetItemAttrText (itemtype   => itemtype,
                                          itemkey    => itemkey,
                                          aname      => 'STATUS');

            l_change_name :=
               wf_engine.GetItemAttrText (itemtype   => itemtype,
                                          itemkey    => itemkey,
                                          aname      => 'CHANGE_NAME');


            -- Added below code by Vamshi in TMS#20150519-00091

            xxwc_pim_chg_item_mgmt.xxwc_ego_vendor_insert (
               FND_PROFILE.VALUE ('LOGIN_ID'),
               NULL,
               l_change_name,
               'U');

            -- Added above code by Vamshi in TMS#20150519-00091

            L_COUNT := 0;

            IF (l_status = 'EHS Approval')
            THEN
               SELECT COUNT (item.hazardous_material_flag)
                 INTO l_count
                 FROM xxwc_mtl_sy_items_chg_b item,
                      eng_engineering_changes chg
                WHERE     item.oracle_change_id = chg.change_id
                      AND chg.change_notice = l_change_name;

               IF (L_COUNT > 0)
               THEN
                  SELECT item.hazardous_material_flag
                    INTO l_hazmat_flag
                    FROM xxwc_mtl_sy_items_chg_b item,
                         eng_engineering_changes chg
                   WHERE     item.oracle_change_id = chg.change_id
                         AND chg.change_notice = l_change_name;
               ELSE
                  l_count := 0;

                  SELECT COUNT (1)             -- Changed by Vamshi in Rev 5.0
                    INTO l_count
                    FROM MTL_SYSTEM_ITEMS_B ITEM,
                         ENG_ENGINEERING_CHANGES CHG,
                         eng_change_lines_vl ecl
                   WHERE     ECL.CHANGE_ID = CHG.CHANGE_ID
                         AND ITEM.segment1 = ECL.name
                         AND ITEM.ORGANIZATION_ID = '222'
                         AND CHG.CHANGE_NOTICE = l_change_name;

                  IF (l_count > 0)
                  THEN
                     SELECT ITEM.HAZARDOUS_MATERIAL_FLAG
                       INTO l_hazmat_flag
                       FROM MTL_SYSTEM_ITEMS_B ITEM,
                            ENG_ENGINEERING_CHANGES CHG,
                            eng_change_lines_vl ecl
                      WHERE     ECL.CHANGE_ID = CHG.CHANGE_ID
                            AND ITEM.segment1 = ECL.name
                            AND ITEM.ORGANIZATION_ID = '222'
                            AND CHG.CHANGE_NOTICE = l_change_name;
                  ELSE
                     -- Added below code by Vamshi in 5.0 -- Start
                     l_count := 0;

                     SELECT COUNT (1)
                       INTO l_count
                       FROM APPS.ENG_REVISED_ITEMS ERI,
                            APPS.MTL_SYSTEM_ITEMS_B MSIB
                      WHERE     ERI.REVISED_ITEM_ID = MSIB.INVENTORY_ITEM_ID
                            AND MSIB.ORGANIZATION_ID = 222
                            AND MSIB.HAZARDOUS_MATERIAL_FLAG = 'Y'
                            AND ERI.CHANGE_NOTICE = l_change_name;

                     IF l_count > 0
                     THEN
                        l_hazmat_flag := 'Y';
                     ELSE
                        -- Added above code by Vamshi in 5.0 -- End

                        l_hazmat_flag := NULL;
                     END IF;
                  END IF;                        -- Base Table Hazard Material
               END IF;                                -----IF (L_COUNT>0) THEN

               IF (l_hazmat_flag = 'Y')
               THEN
                  result := 'COMPLETE:Y';
                  RETURN;
               ELSE
                  result := 'COMPLETE:N';
                  RETURN;
               END IF;
            END IF;                    ---IF (l_status = 'EHS Approval')  THEN
         EXCEPTION                             -----if (funcmode = 'RUN') then
            WHEN OTHERS
            THEN
               RESULT := 'COMPLETE:Y'; ---Manual approval required on exception Ram added
               RETURN;
         END;
      END IF;                                                 --funcmode : RUN
   EXCEPTION
      WHEN OTHERS
      THEN
         -- The line below records this function call in the error system
         -- in the case of an exception.
         wf_core.context ('Eng_Workflow_Pub',
                          'SELECT_STEP_PEOPLE',
                          itemtype,
                          itemkey,
                          TO_CHAR (actid),
                          funcmode);

         result := 'COMPLETE:Y';
         RAISE;
   END SELECT_HAZMAT_ITEM;

   PROCEDURE SELECT_STEP_PEOPLE (itemtype   IN            VARCHAR2,
                                 itemkey    IN            VARCHAR2,
                                 actid      IN            NUMBER,
                                 funcmode   IN            VARCHAR2,
                                 result     IN OUT NOCOPY VARCHAR2)
   IS
      /*******************************************************************************************************************************
               PROCEDURE : SELECT_STEP_PEOPLE

                 REVISIONS:
                 Ver        Date        Author                     Description
                 ---------  ----------  ---------------    ----------------------------------------------------------------------------------
                 1.0        01-MAR-2015   Shankar Vanga     TMS#20150126-00044  - Create Single Item Workflow Tool
                                                            Initial Version

                 5.0        19-OCT-2015   P.vamshidhar      TMS#20150817-00043 - EHS role correction.
                                                            PROCEDURE : SELECT_STEP_PEOPLE

      *********************************************************************************************************************************/


      l_return_status            VARCHAR2 (1);
      l_msg_count                NUMBER;
      l_msg_data                 VARCHAR2 (200);
      l_change_name              VARCHAR2 (200);
      l_status                   VARCHAR2 (200);
      l_hazmat_flag              VARCHAR2 (1) DEFAULT NULL;

      -- Added changes in 5.0 -- Start
      l_route_step_id            ENG_CHANGE_ROUTE_STEPS.STEP_ID%TYPE;
      l_condition_type_code      VARCHAR2 (30);
      l_timeout_min              NUMBER;
      l_required_relative_days   NUMBER;
      l_response_by_date         DATE;

      CURSOR c_step_act (
         p_step_id    NUMBER)
      IS
         SELECT TRUNC (required_date) response_by_date,
                condition_type_code,
                required_relative_days required_relative_days
           FROM ENG_CHANGE_ROUTE_STEPS
          WHERE step_id = p_step_id;
   -- Added changes in 5.0 -- End

   BEGIN
      --
      -- RUN mode - normal process execution
      --
      IF (funcmode = 'RUN')
      THEN
         BEGIN
            l_status :=
               wf_engine.GetItemAttrText (itemtype   => itemtype,
                                          itemkey    => itemkey,
                                          aname      => 'STATUS');

            l_change_name :=
               wf_engine.GetItemAttrText (itemtype   => itemtype,
                                          itemkey    => itemkey,
                                          aname      => 'CHANGE_NAME');

            -- Added below changes in 5.0 -- Start

            -- Get Route Step Id
            Eng_Workflow_Util.GetRouteStepId (
               p_item_type       => itemtype,
               p_item_key        => itemkey,
               x_route_step_id   => l_route_step_id);

            -- Get Step Activity Options
            FOR i IN c_step_act (p_step_id => l_route_step_id)
            LOOP
               l_response_by_date := i.response_by_date;
               l_condition_type_code := i.condition_type_code;
               l_required_relative_days := i.required_relative_days;
            END LOOP;

            -- Set Step Action Voting Option based on step condition type
            -- code
            Eng_Workflow_Util.SetStepActVotingOption (
               p_item_type             => itemtype,
               p_item_key              => itemkey,
               p_condition_type_code   => l_condition_type_code);

            -- Get Response Timeout Min
            Eng_Workflow_Util.GetNtfResponseTimeOut (
               p_item_type     => itemtype,
               p_item_key      => itemkey,
               x_timeout_min   => l_timeout_min);

            IF l_timeout_min IS NULL
            THEN
               -- Use the number of days to set Response TimeOut
               -- Set Response Timeout Min
               Eng_Workflow_Util.SetNtfResponseTimeOut (
                  p_item_type                => itemtype,
                  p_item_key                 => itemkey,
                  p_required_relative_days   => l_required_relative_days);
            END IF;

            -- Added Above changes in 5.0 -- End

            -- Set Adhoc Party Role
            SetStepPeopleRole (x_return_status   => l_return_status,
                               x_msg_count       => l_msg_count,
                               x_msg_data        => l_msg_data,
                               p_item_type       => itemtype,
                               p_item_key        => itemkey);

            IF l_return_status = FND_API.G_RET_STS_SUCCESS
            THEN
               -- set result
               result := 'COMPLETE';
               RETURN;
            ELSE
               -- Unexpected Exception
               RAISE FND_API.G_EXC_UNEXPECTED_ERROR;
            END IF;
         EXCEPTION
            WHEN OTHERS
            THEN
               NULL;
         END;
      END IF;                                                 --funcmode : RUN
   EXCEPTION
      WHEN FND_API.G_EXC_UNEXPECTED_ERROR
      THEN
         -- The line below records this function call in the error system
         -- in the case of an exception.
         wf_core.context ('Eng_Workflow_Pub',
                          'SELECT_STEP_PEOPLE',
                          itemtype,
                          itemkey,
                          TO_CHAR (actid),
                          funcmode);

         result := 'COMPLETE';

         RAISE;
      WHEN OTHERS
      THEN
         -- The line below records this function call in the error system
         -- in the case of an exception.
         wf_core.context ('Eng_Workflow_Pub',
                          'SELECT_STEP_PEOPLE',
                          itemtype,
                          itemkey,
                          TO_CHAR (actid),
                          funcmode);

         result := 'COMPLETE';

         RAISE;
   END SELECT_STEP_PEOPLE;

   PROCEDURE SetStepPeopleRole (x_return_status      OUT NOCOPY VARCHAR2,
                                x_msg_count          OUT NOCOPY NUMBER,
                                x_msg_data           OUT NOCOPY VARCHAR2,
                                p_item_type       IN            VARCHAR2,
                                p_item_key        IN            VARCHAR2)
   IS
      /*******************************************************************************************************************************
               PROCEDURE : SetStepPeopleRole

                 REVISIONS:
                 Ver        Date        Author                     Description
                 ---------  ----------  ---------------    ----------------------------------------------------------------------------------
                 1.0        01-MAR-2015   Shankar Vanga     TMS#20150126-00044  - Create Single Item Workflow Tool
                                                            Initial Version

      *********************************************************************************************************************************/


      l_api_name    CONSTANT VARCHAR2 (30) := 'SetStepPeopleRole';

      l_route_step_id        NUMBER;
      l_change_id            NUMBER;
      l_assignee_party_id    NUMBER;
      l_requestor_party_id   NUMBER;
      l_creator_user_id      NUMBER;
      l_role_name            VARCHAR2 (320);
      l_role_display_name    VARCHAR2 (320);
      -- Bug4532263
      -- l_role_users       VARCHAR2(2000) ;
      l_role_users           WF_DIRECTORY.UserTable;
   BEGIN
      --  Initialize API return status to success
      x_return_status := FND_API.G_RET_STS_SUCCESS;

      -- Set Step People to Role Users
      SetStepPeopleToRoleUsers2 (x_role_users => l_role_users);


      -- Create adhoc role and add users to role
      IF (l_role_users IS NOT NULL AND l_role_users.COUNT > 0)
      THEN
         l_role_name := 'XXWCEHS' || TO_CHAR (SYSDATE, 'YYYYMMSSHHMISS'); -- need the right role name

         l_role_display_name := l_role_name;

         -- Set Adhoc Role and Users in WF Directory Adhoc Role
         SetWFAdhocRole2 (p_role_name           => l_role_name,
                          p_role_display_name   => l_role_display_name --l_role_display_name --Renganathan, Ram
                                                                      ,
                          p_role_users          => l_role_users,
                          p_expiration_date     => NULL);

         WF_ENGINE.SetItemAttrText (p_item_type,
                                    p_item_key,
                                    'XXWC_EHS_ROLE',
                                    l_role_name);
      ELSE
         -- Unexpected Exception
         RAISE FND_API.G_EXC_UNEXPECTED_ERROR;
      END IF;
   EXCEPTION
      WHEN OTHERS
      THEN
         x_return_status := FND_API.G_RET_STS_UNEXP_ERROR;

         RAISE;
   END SetStepPeopleRole;

   PROCEDURE SetStepPeopleToRoleUsers2 (
      x_role_users   IN OUT NOCOPY WF_DIRECTORY.UserTable)
   IS
      /*******************************************************************************************************************************
               PROCEDURE : SetStepPeopleToRoleUsers2

                 REVISIONS:
                 Ver        Date        Author                     Description
                 ---------  ----------  ---------------    ----------------------------------------------------------------------------------
                 1.0        01-MAR-2015   Shankar Vanga     TMS#20150126-00044  - Create Single Item Workflow Tool
                                                            Initial Version

      *********************************************************************************************************************************/


      l_user_role   VARCHAR2 (320);


      -- In case of Instance Route, Assignee Id is always person's party id
      CURSOR c_step_people
      IS
         SELECT member_user_name user_role
           FROM APPS.EGO_GROUP_MEMBERS_V
          WHERE group_name = 'WC EHS Team';
   --         and member_user_name ='RR054396';

   BEGIN
      FOR l_rec IN c_step_people
      LOOP
         AddRoleToRoleUserTable (l_rec.user_role, x_role_users);
      END LOOP;
   END SetStepPeopleToRoleUsers2;

   PROCEDURE SetWFAdhocRole2 (
      p_role_name           IN OUT NOCOPY VARCHAR2,
      p_role_display_name   IN OUT NOCOPY VARCHAR2,
      p_role_users          IN            WF_DIRECTORY.UserTable,
      p_expiration_date     IN            DATE DEFAULT SYSDATE)
   IS
   /*******************************************************************************************************************************
            PROCEDURE : SetWFAdhocRole2

              REVISIONS:
              Ver        Date        Author                     Description
              ---------  ----------  ---------------    ----------------------------------------------------------------------------------
              1.0        01-MAR-2015   Shankar Vanga     TMS#20150126-00044  - Create Single Item Workflow Tool
                                                         Initial Version

   *********************************************************************************************************************************/

   BEGIN
      WF_DIRECTORY.CreateAdHocRole2 (
         role_name           => p_role_name,
         role_display_name   => p_role_display_name,
         role_users          => p_role_users,
         expiration_date     => p_expiration_date);
   END SetWFAdhocRole2;

   PROCEDURE AddRoleToRoleUserTable (
      p_role_name    IN            VARCHAR2,
      x_role_users   IN OUT NOCOPY WF_DIRECTORY.UserTable)
   IS
      /*******************************************************************************************************************************
               PROCEDURE : AddRoleToRoleUserTable

                 REVISIONS:
                 Ver        Date        Author                     Description
                 ---------  ----------  ---------------    ----------------------------------------------------------------------------------
                 1.0        01-MAR-2015   Shankar Vanga     TMS#20150126-00044  - Create Single Item Workflow Tool
                                                            Initial Version

      *********************************************************************************************************************************/

      l_index       NUMBER;
      l_dup_flag    BOOLEAN;
      l_new_index   NUMBER;
   BEGIN
      -- First, check the user role is Active
      IF (WF_DIRECTORY.UserActive (p_role_name))
      THEN
         l_dup_flag := FALSE;
         l_new_index := 0;

         -- Second, check the user role is duplicate
         IF (x_role_users IS NOT NULL AND x_role_users.COUNT > 0)
         THEN
            l_index := x_role_users.FIRST;
            l_new_index := x_role_users.LAST + 1;

            WHILE (l_index IS NOT NULL AND NOT l_dup_flag)
            LOOP
               IF p_role_name = x_role_users (l_index)
               THEN
                  l_dup_flag := TRUE;
               END IF;

               l_index := x_role_users.NEXT (l_index);
            END LOOP;
         END IF;


         IF NOT l_dup_flag
         THEN
            x_role_users (l_new_index) := p_role_name;
         END IF;
      END IF;
   END AddRoleToRoleUserTable;


   PROCEDURE SET_STEP_ACT_OPTIONS (itemtype   IN            VARCHAR2,
                                   itemkey    IN            VARCHAR2,
                                   actid      IN            NUMBER,
                                   funcmode   IN            VARCHAR2,
                                   result     IN OUT NOCOPY VARCHAR2)
   /*******************************************************************************************************************************
            PROCEDURE : SET_STEP_ACT_OPTIONS

              REVISIONS:
              Ver        Date              Author                     Description
              ---------  ----------    ---------------   ----------------------------------------------------------------------------------
              1.0        01-MAR-2015   Shankar Vanga     TMS#20150126-00044  - Create Single Item Workflow Tool
                                                         Initial Version

              5.0        19-OCT-2015   P.vamshidhar      TMS#20150817-00043 - EHS role correction.
                                                         TMS#20150901-00146 - PCAM add delete functionality for Cross References


 *********************************************************************************************************************************/
   IS
      l_route_step_id            NUMBER;
      l_timeout_min              NUMBER;
      l_required_relative_days   NUMBER;
      l_response_by_date         DATE;
      l_condition_type_code      VARCHAR2 (30);

      l_change_name              VARCHAR2 (20);
      l_vendor_name              VARCHAR2 (60);
      l_vendor_number            VARCHAR2 (50);
      l_count                    NUMBER;
      l_change_type              VARCHAR2 (100);      -- Added in Revision 5.0

      CURSOR c_step_act (
         p_step_id    NUMBER)
      IS
         SELECT TRUNC (required_date) response_by_date,
                condition_type_code,
                required_relative_days required_relative_days
           FROM ENG_CHANGE_ROUTE_STEPS
          WHERE step_id = p_step_id;

      CURSOR C1 (p_change_name VARCHAR2)
      IS
           SELECT VENDOR_NUMBER
             FROM XXWC.XXWC_EGO_CHANGE_VENDOR
            WHERE change_name = p_change_name
         ORDER BY ROWID DESC;
   BEGIN
      --
      -- RUN mode - normal process execution
      --
      IF (funcmode = 'RUN')
      THEN
         -- Added below code to show notification subject include vendor name.
         l_change_name :=
            wf_engine.GetItemAttrText (itemtype   => itemtype,
                                       itemkey    => itemkey,
                                       aname      => 'CHANGE_NAME');

         -------------------------------------------------------------------------------------
         BEGIN
            l_count := 0;

            SELECT COUNT (1)
              INTO l_count
              FROM (SELECT VENDOR_NAME
                      FROM APPS.XXWC_EGO_INTRNAL_AT_ATTR_AGV A,
                           ENG_REVISED_ITEMS B,
                           APPS.ENG_ENGINEERING_CHANGES C,
                           APPS.AP_SUPPLIERS D
                     WHERE     A.INVENTORY_ITEM_ID = B.REVISED_ITEM_ID
                           AND A.ORGANIZATION_ID = 222
                           AND A.ORGANIZATION_ID = B.ORGANIZATION_ID
                           AND B.CHANGE_ID = C.CHANGE_ID
                           AND A.XXWC_VENDOR_NUMBER_ATTR = D.SEGMENT1
                           AND C.CHANGE_NOTICE = l_change_name
                    UNION
                    SELECT VENDOR_NAME
                      FROM xxwc.XXWC_CHG_EGO_ITEM_ATTRS A,
                           APPS.ENG_ENGINEERING_CHANGES B,
                           APPS.AP_SUPPLIERS C
                     WHERE     A.CHANGE_ID = B.CHANGE_ID
                           AND B.CHANGE_NOTICE = l_change_name
                           AND C.vendor_type_lookup_code = 'VENDOR'
                           AND C.segment1 = a.c_ext_attr1);

            IF l_count > 0
            THEN
               BEGIN
                  SELECT 'for' || ' ' || SUBSTR (VENDOR_NAME, 1, 45)
                    INTO l_vendor_name
                    FROM (SELECT VENDOR_NAME
                            FROM APPS.XXWC_EGO_INTRNAL_AT_ATTR_AGV A,
                                 ENG_REVISED_ITEMS B,
                                 APPS.ENG_ENGINEERING_CHANGES C,
                                 APPS.AP_SUPPLIERS D
                           WHERE     A.INVENTORY_ITEM_ID = B.REVISED_ITEM_ID
                                 AND A.ORGANIZATION_ID = 222
                                 AND A.ORGANIZATION_ID = B.ORGANIZATION_ID
                                 AND B.CHANGE_ID = C.CHANGE_ID
                                 AND A.XXWC_VENDOR_NUMBER_ATTR = D.SEGMENT1
                                 AND C.CHANGE_NOTICE = l_change_name
                          UNION
                          SELECT VENDOR_NAME
                            FROM xxwc.XXWC_CHG_EGO_ITEM_ATTRS A,
                                 APPS.ENG_ENGINEERING_CHANGES B,
                                 APPS.AP_SUPPLIERS C
                           WHERE     A.CHANGE_ID = B.CHANGE_ID
                                 AND B.CHANGE_NOTICE = l_change_name
                                 AND C.vendor_type_lookup_code = 'VENDOR'
                                 AND C.segment1 = a.c_ext_attr1)
                   WHERE ROWNUM < 2;

                  wf_engine.SetItemAttrText (itemtype   => itemtype,
                                             itemkey    => itemkey,
                                             aname      => 'XXWC_VENDOR_NAME',
                                             avalue     => l_vendor_name);
               EXCEPTION
                  WHEN OTHERS
                  THEN
                     l_vendor_name := NULL;
               END;
            ELSE
               BEGIN
                  OPEN C1 (l_change_name);

                  FETCH C1 INTO l_vendor_number;

                  CLOSE C1;

                  SELECT vendor_name
                    INTO l_vendor_name
                    FROM APPS.AP_SUPPLIERS
                   WHERE SEGMENT1 = l_vendor_number;

                  wf_engine.SetItemAttrText (itemtype   => itemtype,
                                             itemkey    => itemkey,
                                             aname      => 'XXWC_VENDOR_NAME',
                                             avalue     => l_vendor_name);
               EXCEPTION
                  WHEN OTHERS
                  THEN
                     l_vendor_name := NULL;
               END;
            END IF;
         EXCEPTION
            WHEN OTHERS
            THEN
               l_vendor_name := NULL;
         END;

         --- Below change added in TMS#20150901-00146 Revision - 5.0 -- start
         l_change_type :=
            wf_engine.GetItemAttrText (itemtype   => itemtype,
                                       itemkey    => itemkey,
                                       aname      => 'CHANGE_MANAGEMENT_TYPE');

         IF UPPER (l_change_type) = 'CHANGE ORDER'
         THEN
            -- if any thing wrong regarding add/delete/update cross references, DC team needs to be rejct.
            Cross_ref_populate (l_change_name);
         END IF;


         --- Above change added in TMS#20150901-00146 Revision - 5.0 -- end;

         -------------------------------------------------------------------------------------

         -- Get Route Step Id
         Eng_Workflow_Util.GetRouteStepId (
            p_item_type       => itemtype,
            p_item_key        => itemkey,
            x_route_step_id   => l_route_step_id);

         -- Get Step Activity Options
         FOR i IN c_step_act (p_step_id => l_route_step_id)
         LOOP
            l_response_by_date := i.response_by_date;
            l_condition_type_code := i.condition_type_code;
            l_required_relative_days := i.required_relative_days;
         END LOOP;

         -- Set Step Action Voting Option based on step condition type
         -- code
         Eng_Workflow_Util.SetStepActVotingOption (
            p_item_type             => itemtype,
            p_item_key              => itemkey,
            p_condition_type_code   => l_condition_type_code);

         -- Get Response Timeout Min
         Eng_Workflow_Util.GetNtfResponseTimeOut (
            p_item_type     => itemtype,
            p_item_key      => itemkey,
            x_timeout_min   => l_timeout_min);


         IF l_timeout_min IS NULL
         THEN
            --
            -- Comment out for reminder notification
            -- We disabled time out functionality
            --
            -- Set Response Timeout Min
            -- Eng_Workflow_Util.SetNtfResponseTimeOut
            -- (   p_item_type         => itemtype
            -- ,  p_item_key          => itemkey
            -- ,  p_response_by_date  => l_response_by_date
            -- ) ;
            --

            -- Use the number of days to set Response TimeOut
            -- Set Response Timeout Min
            Eng_Workflow_Util.SetNtfResponseTimeOut (
               p_item_type                => itemtype,
               p_item_key                 => itemkey,
               p_required_relative_days   => l_required_relative_days);
         END IF;
      END IF;                                                -- funcmode : RUN

      --
      -- CANCEL mode - activity 'compensation'
      --
      -- This is in the event that the activity must be undone,
      -- for example when a process is reset to an earlier point
      -- due to a loop back.
      --
      IF (funcmode = 'CANCEL')
      THEN
         -- your cancel code goes here
         NULL;

         -- no result needed
         result := 'COMPLETE';
         RETURN;
      END IF;


      --
      -- Other execution modes may be created in the future.  Your
      -- activity will indicate that it does not implement a mode
      -- by returning null
      --
      result := '';
      RETURN;
   EXCEPTION
      WHEN FND_API.G_EXC_UNEXPECTED_ERROR
      THEN
         -- The line below records this function call in the error system
         -- in the case of an exception.
         wf_core.context ('Eng_Workflow_Pub',
                          'SET_STEP_ACT_OPTIONS',
                          itemtype,
                          itemkey,
                          TO_CHAR (actid),
                          funcmode);
         RAISE;
      WHEN OTHERS
      THEN
         -- The line below records this function call in the error system
         -- in the case of an exception.
         wf_core.context ('Eng_Workflow_Pub',
                          'SET_STEP_ACT_OPTIONS',
                          itemtype,
                          itemkey,
                          TO_CHAR (actid),
                          funcmode);
         RAISE;
   END SET_STEP_ACT_OPTIONS;

   -- Added below function for single item UI extract report @ TMS#20150608-00031 by Vamshi.

   FUNCTION XXWC_WF_ASSIGNEE_FUNC (p_change_notice     IN VARCHAR2,
                                   p_wf_key            IN VARCHAR2,
                                   p_status_name       IN VARCHAR2,
                                   p_approval_status   IN VARCHAR2)
      RETURN VARCHAR2
   IS
      /*******************************************************************************************************************************
               FUNCTION : XXWC_WF_ASSIGNEE_FUNC

   -- VERSION DATE          AUTHOR(S)       DESCRIPTION
   -- ------- -----------   --------------- ------------------------------------------------

   --3.0      09-Jun-2015   P.Vamshidhar    TMS#20150608-00031 -- Added new Procedures/function
                                            XXWC_WF_ASSIGNEE_FUNC

   --4.0      14-Aug-2015   P.Vamshidhar    TMS#20150708-00061 - PCAM enhancements part 3


      *********************************************************************************************************************************/

      lvc_full_name   VARCHAR2 (100);

      CURSOR c1 (
         p_change_notice1    VARCHAR2,
         p_wf_key1           VARCHAR2)
      IS
           SELECT c.full_name
             FROM apps.wf_notifications A,
                  APPS.FND_USER B,
                  APPS.PER_ALL_PEOPLE_F C
            WHERE     a.user_key LIKE (p_change_notice1 || '%')
                  AND A.ITEM_KEY = p_wf_key1
                  AND A.RECIPIENT_ROLE = B.USER_NAME
                  AND A.MESSAGE_TYPE = 'ENGCSTEP' -- Added by Vamshi TMS#20150708-00061
                  AND B.EMPLOYEE_ID = C.PERSON_ID
         ORDER BY responder;
   BEGIN
      -- Moved cursor part below to improve performance - TMS#20150708-00061

      IF     (   UPPER (p_status_name) LIKE '%DC%'
              OR UPPER (p_status_name) LIKE '%EHS%')
         AND p_approval_status = 'IN_PROGRESS'
      THEN
         RETURN NULL;
      END IF;

      OPEN c1 (p_change_notice, p_wf_key);

      FETCH C1 INTO lvc_full_name;

      CLOSE c1;

      RETURN lvc_full_name;
   EXCEPTION
      WHEN OTHERS
      THEN
         IF C1%ISOPEN
         THEN
            CLOSE C1;
         END IF;

         RETURN NULL;
   END;

   /*******************************************************************************************************************************
    PROCEDURE : SELECT_STD_REVIEWERS

    REVISIONS:
    Ver        Date        Author                     Description
    ---------  ----------  ---------------    --------------------------------------------------------------------------
    3.0        09-JUN-2015 P.Vamshidhar        TMS#20150608-00031 - Initial Version
    *******************************************************************************************************************************/

   PROCEDURE SELECT_STD_REVIEWERS (itemtype   IN            VARCHAR2,
                                   itemkey    IN            VARCHAR2,
                                   actid      IN            NUMBER,
                                   funcmode   IN            VARCHAR2,
                                   result     IN OUT NOCOPY VARCHAR2)
   IS
      l_return_status   VARCHAR2 (1);
      l_msg_count       NUMBER;
      l_msg_data        VARCHAR2 (200);


      l_change_name     VARCHAR2 (100);
      l_change_type     VARCHAR2 (100);
      l_change_status   VARCHAR2 (100);
      l_change_id       VARCHAR2 (100);
   BEGIN
      --
      -- RUN mode - normal process execution
      --
      IF (funcmode = 'RUN')
      THEN
         -- Set Reviewers Role
         Eng_Workflow_Util.SetReviewersRole (
            x_return_status   => l_return_status,
            x_msg_count       => l_msg_count,
            x_msg_data        => l_msg_data,
            p_item_type       => itemtype,
            p_item_key        => itemkey,
            p_reviewer_type   => 'STD');



         l_change_name :=
            wf_engine.GetItemAttrText (itemtype   => itemtype,
                                       itemkey    => itemkey,
                                       aname      => 'CHANGE_NAME');


         l_change_type :=
            wf_engine.GetItemAttrText (itemtype   => itemtype,
                                       itemkey    => itemkey,
                                       aname      => 'CHANGE_MANAGEMENT_TYPE');

         l_change_status :=
            wf_engine.GetItemAttrText (itemtype   => itemtype,
                                       itemkey    => itemkey,
                                       aname      => 'STATUS');

         IF     l_change_type = 'Change Request'
            AND l_change_status IN ('Released')
         THEN
            Update_Change_line (l_change_name);
         END IF;



         IF     l_change_type IN ('Change Order', 'Change Request')
            AND l_change_status IN ('Released', 'Scheduled')
            AND l_return_status = FND_API.G_RET_STS_SUCCESS
         THEN
            result := 'COMPLETE:NONE';
            RETURN;
         END IF;


         IF l_return_status = FND_API.G_RET_STS_SUCCESS
         THEN
            -- set result
            result := 'COMPLETE';
            RETURN;
         -- None
         ELSIF l_return_status = Eng_Workflow_Util.G_RET_STS_NONE
         THEN
            -- set result
            result := 'COMPLETE:NONE';
            RETURN;
         ELSE
            -- Unexpected Exception
            RAISE FND_API.G_EXC_UNEXPECTED_ERROR;
         END IF;
      END IF;                                                -- funcmode : RUN


      --
      -- CANCEL mode - activity 'compensation'
      --
      -- This is in the event that the activity must be undone,
      -- for example when a process is reset to an earlier point
      -- due to a loop back.
      --
      IF (funcmode = 'CANCEL')
      THEN
         -- your cancel code goes here
         NULL;

         -- no result needed
         result := 'COMPLETE';
         RETURN;
      END IF;


      --
      -- Other execution modes may be created in the future.  Your
      -- activity will indicate that it does not implement a mode
      -- by returning null
      --
      result := '';
      RETURN;
   EXCEPTION
      WHEN FND_API.G_EXC_UNEXPECTED_ERROR
      THEN
         -- The line below records this function call in the error system
         -- in the case of an exception.
         wf_core.context ('Eng_Workflow_Pub',
                          'SELECT_STD_REVIEWERS',
                          itemtype,
                          itemkey,
                          TO_CHAR (actid),
                          funcmode);
         RAISE;
      WHEN OTHERS
      THEN
         -- The line below records this function call in the error system
         -- in the case of an exception.
         wf_core.context ('Eng_Workflow_Pub',
                          'SELECT_STD_REVIEWERS',
                          itemtype,
                          itemkey,
                          TO_CHAR (actid),
                          funcmode);
         RAISE;
   END SELECT_STD_REVIEWERS;


   /*******************************************************************************************************************************
    PROCEDURE : Update_Change_line

    REVISIONS:
    Ver        Date        Author                     Description
    ---------  ----------  ---------------    --------------------------------------------------------------------------
    3.0        09-JUN-2015 P.Vamshidhar        TMS#20150608-00031 - Initial Version
    *******************************************************************************************************************************/

   PROCEDURE Update_Change_line (p_change_name IN VARCHAR2)
   IS
      PRAGMA AUTONOMOUS_TRANSACTION;
      l_change_id   ENG_ENGINEERING_CHANGES.CHANGE_ID%TYPE;
      l_count       NUMBER;
   BEGIN
      SELECT change_id
        INTO l_change_id
        FROM apps.eng_engineering_changes
       WHERE change_notice = p_change_name;

      IF l_change_id IS NOT NULL
      THEN
         SELECT COUNT (1)
           INTO l_count
           FROM ENG_CHANGE_LINES
          WHERE CHANGE_ID = l_change_id;

         IF l_count > 0
         THEN
            UPDATE ENG_CHANGE_LINES
               SET STATUS_CODE = 11                        -- Complete status.
             WHERE CHANGE_ID = l_change_id;

            COMMIT;
         END IF;
      END IF;
   EXCEPTION
      WHEN OTHERS
      THEN
         NULL;
   END;

   -- Added below procedure in Rev 5.0

   PROCEDURE Cross_ref_populate (p_change_name IN VARCHAR2)
   /*******************************************************************************************************************************
     PROCEDURE : Cross_ref_populate

     REVISIONS:
     Ver        Date              Author                     Description
    ---------  ----------    ---------------   -------------------------------------------------------------------------
     5.0        19-OCT-2015   P.vamshidhar      TMS#20150817-00043 - EHS role correction.
                                                TMS#20150901-00146 - PCAM add delete functionality for Cross References

 *********************************************************************************************************************************/
   IS
      PRAGMA AUTONOMOUS_TRANSACTION;
      ln_change_id   ENG_ENGINEERING_CHANGES.CHANGE_ID%TYPE;
      tcount         NUMBER := 0;
   BEGIN
      BEGIN
         SELECT change_id
           INTO ln_change_id
           FROM APPS.ENG_ENGINEERING_CHANGES B
          WHERE b.change_notice = p_change_name;

         SELECT COUNT (1)
           INTO tcount
           FROM XXWC.XXWC_PCAM_CREFF_DATA_PURGE A
          WHERE USER_LOGIN_ID = FND_GLOBAL.LOGIN_ID;

         IF NVL (tcount, 0) >= 1
         THEN
            UPDATE XXWC.XXWC_PCAM_CREFF_DATA_PURGE
               SET CHANGE_ID = ln_change_id, change_notice = p_change_name
             WHERE USER_LOGIN_ID = FND_GLOBAL.LOGIN_ID;
         END IF;
      EXCEPTION
         WHEN OTHERS
         THEN
            ROLLBACK;
      END;

      COMMIT;
   EXCEPTION
      WHEN OTHERS
      THEN
         ROLLBACK;
   END;
END XXWC_EGO_WORKFLOW_PKG;
/