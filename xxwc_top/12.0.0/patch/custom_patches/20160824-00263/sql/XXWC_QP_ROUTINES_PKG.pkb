CREATE OR REPLACE PACKAGE BODY APPS.XXWC_QP_ROUTINES_PKG
AS
/*********************************************************************************************************************
    Package   Body : APPS.XXWC_QP_ROUTINES_PKG
                     This package body has Procedures and Functions related to Pricing routines.
    Change History
Ver    Resource             Date              TMS Ticket #      Procedure /Function Name
1.0    Raghav Velichetti    4/3/2013         20130121-00460                   GET_ORDER_SUB_TOTAL
2.0   Ram Talluri          11/4/2013         20131008-00446         write_log,write_error,is_row_locked,upd_calc_price_flag,upd_calc_price_flag_proc
3.0   Ram Talluri          5/9/2014          20140225-00208         webber_product_catalog
4.0   Ram Talluri         6/9/2014         20140528-00018         get_price_break
5.0   Ram Talluri         6/9/2014         20140619-00072        minor changes get_price_break  
6.0   Mahesh Kudle        4/21/2015        TMS 20150416-00086 minor change to add new branch
7.0   Hari Prasad M          8/4/2015         TMS 20150708-00048 --Webber eCatalog Program Error,Replace comma with space.
8.0   Pattabhi Avula      01/22/2016       TMS#20160111-00035 - Webber Interface Missing Column -- Added comma in the UTL file
9.0   P.Vamshidhar        09/01/2016       TMS#20160824-00263 - Webber File
********************************************************************************************************************/
/*************************************************************************
        *   Function : GET_ORDER_SUB_TOTAL
        *
        *   PURPOSE:   This procedure return order sub total excluding Freight and Tzxes
        *  Author: Raghav Velicheti (TMS # 20130121-00460)
        * Creation_date 4/3/2013
        *Last update Date 4/3/2013
        *   Parameter:
        *          IN
        *              p_Header_id
        * ************************************************************************/
   FUNCTION XXWC_GET_TOT_MINUS (p_header_id IN NUMBER)
      RETURN NUMBER
   IS
      p_order_sum   NUMBER;

      CURSOR get_sum_order_lines_cur (
         l_header_id NUMBER)
      IS
         SELECT SUM (
                     (unit_selling_price * ordered_quantity)
                   * DECODE (line_category_code, 'ORDER', 1, -1))
           FROM apps.oe_order_lines_all
          WHERE header_id = l_header_id;
   BEGIN
      OPEN get_sum_order_lines_cur (p_header_id);

      FETCH get_sum_order_lines_cur INTO p_order_sum;

      CLOSE get_sum_order_lines_cur;

      RETURN p_order_sum;
   EXCEPTION
      WHEN OTHERS
      THEN
         RETURN NULL;
   END XXWC_GET_TOT_MINUS;

   /*************************************************************************
       *   Procedure : write_log
       *
       *   PURPOSE:   This procedure prints the string in log file.
       *  Author: Ram Talluri- TMS #20131008-00446
       * Creation_date 11/4/2013
       * Last update Date 11/4/2013
       *   Parameter:
       *          IN p_debug_msg
       *
       * ************************************************************************/
   PROCEDURE write_log (p_debug_msg IN VARCHAR2)
   IS
   BEGIN
      fnd_file.put_line (fnd_file.LOG, p_debug_msg);
      DBMS_OUTPUT.put_line (p_debug_msg);
   END write_log;

   /*************************************************************************
        *   Procedure : write_error
        *
        *   PURPOSE:   This procedure reports the error.
        *  Author: Ram Talluri- TMS #20131008-00446
        * Creation_date 11/4/2013
        * Last update Date 11/4/2013
        *   Parameter:
        *          IN p_debug_msg
        *
        * ************************************************************************/
   PROCEDURE write_error (p_debug_msg IN VARCHAR2, p_call_point IN VARCHAR2)
   IS
      l_req_id          NUMBER := fnd_global.conc_request_id;
      l_err_callfrom    VARCHAR2 (75) DEFAULT 'XXWC_QP_ROUTINES_PKG';
      l_err_callpoint   VARCHAR2 (75) DEFAULT 'START';
      l_distro_list     VARCHAR2 (75)
                           DEFAULT 'HDSOracleDevelopers@hdsupply.com';
   BEGIN
      xxcus_error_pkg.xxcus_error_main_api (
         p_called_from         => l_err_callfrom,
         p_calling             => l_err_callpoint,
         p_request_id          => l_req_id,
         p_ora_error_msg       => SUBSTR (p_debug_msg, 1, 2000),
         p_error_desc          => 'Error running XXWC_QP_ROUTINES_PKG with PROGRAM ERROR',
         p_distribution_list   => l_distro_list,
         p_module              => 'XXWC');
   END write_error;

   /*************************************************************************
       *   Function : is_row_locked
       *
       *   PURPOSE:   This function identifies if a record has ben locked by user
       *  Author: Ram Talluri- TMS #20131008-00446
       * Creation_date 11/4/2013
       * Last update Date 11/4/2013
       *   Parameter:
       *          IN rowid, table_name
       *
       * ************************************************************************/
   FUNCTION is_row_locked (v_rowid ROWID, table_name VARCHAR2)
      RETURN VARCHAR2
   IS
      x   NUMBER;
      PRAGMA AUTONOMOUS_TRANSACTION;
   BEGIN
      EXECUTE IMMEDIATE
            'Begin
                           Select 1 into :x from '
         || table_name
         || ' where rowid =:v_rowid for update nowait;
                         Exception
                            When Others Then
                              :x:=null;
                         End;'
         USING OUT x, v_rowid;

      -- now release the lock if we got it.
      ROLLBACK;

      IF x = 1
      THEN
         RETURN 'N';
      ELSIF x IS NULL
      THEN
         RETURN 'Y';
      END IF;
   END;

   /*************************************************************************
        *   Procedure : upd_calc_price_flag
        *
        *   PURPOSE:   This procedure sets calculate freeze flag of orde_line to Freeze Price.
        *  Author: Ram Talluri- TMS #20131008-00446
        * Creation_date 11/4/2013
        * Last update Date 11/20/2013
        *   Parameter:
        *          IN  p_delta_date
        *          IN  p_header_id
        *
        * Version   Date     Name        TMS
        * 1.0      1/8/2014  Ram Talluri  TMS 20140108-00146       
        * ************************************************************************/
   PROCEDURE upd_calc_price_flag (errbuf              OUT VARCHAR2,
                                  retcode             OUT NUMBER,
                                  p_last_delta_date       VARCHAR2,
                                  p_header_id             NUMBER)
   IS
      l_header_rec               oe_order_pub.header_rec_type;
      o_header_rec               oe_order_pub.header_rec_type;
      o_header_val_rec           oe_order_pub.header_val_rec_type;
      o_header_adj_tbl           oe_order_pub.header_adj_tbl_type;
      o_header_adj_val_tbl       oe_order_pub.header_adj_val_tbl_type;
      o_header_price_att_tbl     oe_order_pub.header_price_att_tbl_type;
      o_header_adj_att_tbl       oe_order_pub.header_adj_att_tbl_type;
      o_header_adj_assoc_tbl     oe_order_pub.header_adj_assoc_tbl_type;
      o_header_scredit_tbl       oe_order_pub.header_scredit_tbl_type;
      o_header_scredit_val_tbl   oe_order_pub.header_scredit_val_tbl_type;
      l_line_tbl                 oe_order_pub.line_tbl_type;
      o_line_tbl                 oe_order_pub.line_tbl_type;
      o_line_val_tbl             oe_order_pub.line_val_tbl_type;
      o_line_adj_tbl             oe_order_pub.line_adj_tbl_type;
      o_line_adj_val_tbl         oe_order_pub.line_adj_val_tbl_type;
      o_line_price_att_tbl       oe_order_pub.line_price_att_tbl_type;
      o_line_adj_att_tbl         oe_order_pub.line_adj_att_tbl_type;
      o_line_adj_assoc_tbl       oe_order_pub.line_adj_assoc_tbl_type;
      o_line_scredit_tbl         oe_order_pub.line_scredit_tbl_type;
      o_line_scredit_val_tbl     oe_order_pub.line_scredit_val_tbl_type;
      o_lot_serial_tbl           oe_order_pub.lot_serial_tbl_type;
      o_lot_serial_val_tbl       oe_order_pub.lot_serial_val_tbl_type;
      l_action_request_tbl       oe_order_pub.request_tbl_type;
      o_action_request_tbl       oe_order_pub.request_tbl_type;
      l_return_status            VARCHAR2 (240);
      l_msg_count                NUMBER;
      l_msg_data                 VARCHAR2 (4000);
      v_exception                EXCEPTION;

      v_locked_line              VARCHAR2 (1) := 'N';
      v_locked_header            VARCHAR2 (1) := 'N';
      v_running_message1         VARCHAR2 (2048);
      l_msg_dummy                VARCHAR2 (1024);
      v_call_point               VARCHAR2 (1000);
   BEGIN
      
      v_call_point := 'STAGE1';

      FOR c1_rec
         IN (SELECT /*+ index(ol XXWC_OE_ORDER_LN_LUD1) */--Ram Talluri 7/30/2014 TMS #20140609-00203 index hint updated to reflect the latest index name changes.
                   a.line_id,
                    h.order_number,
                    a.line_number,
                    a.header_id,
                    a.ROWID line_rowid,
                    h.ROWID header_rowid
               FROM apps.oe_order_lines_all a, apps.oe_order_headers_all h
              WHERE     a.flow_status_code NOT IN
                           ('ENTERED', 'CLOSED', 'CANCELLED', 'OFFER_EXPIRED','DRAFT')
                    AND a.CALCULATE_PRICE_FLAG NOT IN('N','P')
                    AND h.order_type_id IN
                           (1001, 1004, 1009, 1006, 1013, 1014) --std, counter,Repair orders,return orders, ST rental, LT Rental
                    AND h.header_id = a.header_id
                    AND h.header_id = NVL (p_header_id, h.header_id)
                    AND TRUNC (a.last_update_date) BETWEEN TRUNC (
                                                                NVL (
                                                                   TO_DATE (
                                                                      p_last_delta_date,
                                                                      'YYYY/MM/DD HH24:MI:SS'),
                                                                   SYSDATE)
                                                              - 1.01)
                                                       AND   TRUNC (
                                                                NVL (
                                                                   TO_DATE (
                                                                      p_last_delta_date,
                                                                      'YYYY/MM/DD HH24:MI:SS'),
                                                                   SYSDATE))
                                                           + 0.9)
      LOOP
         v_call_point := 'STAGE2';

         BEGIN
            v_locked_line :=
               is_row_locked (c1_rec.line_rowid, 'OE_ORDER_LINES_ALL');
            v_locked_header :=
               is_row_locked (c1_rec.header_rowid, 'OE_ORDER_HEADERS_ALL');

            v_call_point := 'STAGE3';
         EXCEPTION
            WHEN OTHERS
            THEN
               v_locked_line := 'Y';
               v_locked_header := 'Y';
         END;

         IF v_locked_line = 'N' AND v_locked_header = 'N'
         THEN
            v_call_point := 'STAGE4';
            oe_msg_pub.initialize;
            
            l_line_tbl (1) := oe_order_pub.g_miss_line_rec;
            l_line_tbl (1).line_id := c1_rec.line_id;
            l_line_tbl (1).header_id := c1_rec.header_id;
            l_line_tbl (1).calculate_price_flag := 'P'; --set to partial price.
            l_line_tbl (1).operation := oe_globals.g_opr_update;
            oe_order_pub.process_order (
               p_api_version_number       => 1.0,
               p_header_rec               => l_header_rec,
               p_line_tbl                 => l_line_tbl,
               p_action_request_tbl       => l_action_request_tbl,
               x_return_status            => l_return_status,
               x_msg_count                => l_msg_count,
               x_msg_data                 => l_msg_data,
               x_header_rec               => o_header_rec,
               x_header_val_rec           => o_header_val_rec,
               x_header_adj_tbl           => o_header_adj_tbl,
               x_header_adj_val_tbl       => o_header_adj_val_tbl,
               x_header_price_att_tbl     => o_header_price_att_tbl,
               x_header_adj_att_tbl       => o_header_adj_att_tbl,
               x_header_adj_assoc_tbl     => o_header_adj_assoc_tbl,
               x_header_scredit_tbl       => o_header_scredit_tbl,
               x_header_scredit_val_tbl   => o_header_scredit_val_tbl,
               x_line_tbl                 => o_line_tbl,
               x_line_val_tbl             => o_line_val_tbl,
               x_line_adj_tbl             => o_line_adj_tbl,
               x_line_adj_val_tbl         => o_line_adj_val_tbl,
               x_line_price_att_tbl       => o_line_price_att_tbl,
               x_line_adj_att_tbl         => o_line_adj_att_tbl,
               x_line_adj_assoc_tbl       => o_line_adj_assoc_tbl,
               x_line_scredit_tbl         => o_line_scredit_tbl,
               x_line_scredit_val_tbl     => o_line_scredit_val_tbl,
               x_lot_serial_tbl           => o_lot_serial_tbl,
               x_lot_serial_val_tbl       => o_lot_serial_val_tbl,
               x_action_request_tbl       => o_action_request_tbl);

            IF l_return_status = 'S'
            THEN
               write_log (
                     ' Calculate Price Flag Update Success for order number ='
                  || c1_rec.order_number
                  || ', '
                  || ' LINE_NUMBER='
                  || c1_rec.line_number
                  || ', '
                  || 'LINE_ID='
                  || c1_rec.line_id
                  || l_msg_data);
            ELSE
               IF l_msg_count > 0
               THEN
                  v_call_point := 'STAGE5';
                  v_running_message1 := '';

                  FOR i IN 1 .. l_msg_count
                  LOOP
                     fnd_msg_pub.get (i,
                                      fnd_api.g_false,
                                      l_msg_data,
                                      l_msg_dummy);

                     IF NVL (v_running_message1, 'null') <> l_msg_data
                     THEN
                        write_log (
                              'Calculate Price Flag Update Error for order number ='
                           || c1_rec.order_number
                           || ', '
                           || ' LINE_NUMBER='
                           || c1_rec.line_number
                           || ', '
                           || 'LINE_ID='
                           || c1_rec.line_id
                           || (SUBSTR (
                                  'Msg' || TO_CHAR (i) || ': ' || l_msg_data,
                                  1,
                                  255)));
                     END IF;

                     v_running_message1 := l_msg_data;
                  END LOOP;
               END IF;
            END IF;

            DBMS_OUTPUT.put_line (
               l_return_status || '=' || l_msg_count || '=' || l_msg_data);
            COMMIT;
         END IF;
      END LOOP;
   EXCEPTION
      WHEN NO_DATA_FOUND THEN
      NULL;
      WHEN OTHERS
      THEN
         NULL;
         write_error ('Error in updating calculate price_flag procedure',
                      v_call_point);
   END upd_calc_price_flag;

   /*************************************************************************
        *   Procedure : upd_calc_price_flag_proc
        *
        *   PURPOSE:   This procedure sets calculate freeze flag of orde_line to Freeze Price.
        *  Author: Ram Talluri- TMS #20131008-00446
        * Creation_date 11/4/2013
        * Last update Date 11/20/2013
        *   Parameter:
        *          IN  p_header_id
        *          IN p_order_type_id
        * Version   Date     Name        TMS
        * 1.0      1/8/2014  Ram Talluri  TMS 20140108-00146      
        *
        * ************************************************************************/
   PROCEDURE upd_calc_price_flag_proc (p_header_id        NUMBER,
                                       p_order_type_id    NUMBER)
   IS
      l_header_rec               oe_order_pub.header_rec_type;
      o_header_rec               oe_order_pub.header_rec_type;
      o_header_val_rec           oe_order_pub.header_val_rec_type;
      o_header_adj_tbl           oe_order_pub.header_adj_tbl_type;
      o_header_adj_val_tbl       oe_order_pub.header_adj_val_tbl_type;
      o_header_price_att_tbl     oe_order_pub.header_price_att_tbl_type;
      o_header_adj_att_tbl       oe_order_pub.header_adj_att_tbl_type;
      o_header_adj_assoc_tbl     oe_order_pub.header_adj_assoc_tbl_type;
      o_header_scredit_tbl       oe_order_pub.header_scredit_tbl_type;
      o_header_scredit_val_tbl   oe_order_pub.header_scredit_val_tbl_type;
      l_line_tbl                 oe_order_pub.line_tbl_type;
      o_line_tbl                 oe_order_pub.line_tbl_type;
      o_line_val_tbl             oe_order_pub.line_val_tbl_type;
      o_line_adj_tbl             oe_order_pub.line_adj_tbl_type;
      o_line_adj_val_tbl         oe_order_pub.line_adj_val_tbl_type;
      o_line_price_att_tbl       oe_order_pub.line_price_att_tbl_type;
      o_line_adj_att_tbl         oe_order_pub.line_adj_att_tbl_type;
      o_line_adj_assoc_tbl       oe_order_pub.line_adj_assoc_tbl_type;
      o_line_scredit_tbl         oe_order_pub.line_scredit_tbl_type;
      o_line_scredit_val_tbl     oe_order_pub.line_scredit_val_tbl_type;
      o_lot_serial_tbl           oe_order_pub.lot_serial_tbl_type;
      o_lot_serial_val_tbl       oe_order_pub.lot_serial_val_tbl_type;
      l_action_request_tbl       oe_order_pub.request_tbl_type;
      o_action_request_tbl       oe_order_pub.request_tbl_type;
      l_return_status            VARCHAR2 (240);
      l_msg_count                NUMBER;
      l_msg_data                 VARCHAR2 (4000);
      v_exception                EXCEPTION;

      v_locked_line              VARCHAR2 (1) := 'N';
      v_locked_header            VARCHAR2 (1) := 'N';
      v_running_message1         VARCHAR2 (2048);
      l_msg_dummy                VARCHAR2 (1024);
      v_call_point               VARCHAR2 (1000);
   BEGIN
      
      v_call_point := 'STAGE1';

      FOR c1_rec
         IN (SELECT /*+ index(ol XXWC_OE_ORDER_LN_LUD1) */--Ram Talluri 7/30/2014 TMS #20140609-00203 index hint updated to reflect the latest index name changes.
                   a.line_id,
                    h.order_number,
                    a.line_number,
                    a.header_id,
                    a.ROWID line_rowid,
                    h.ROWID header_rowid,
                    a.last_update_date
               FROM apps.oe_order_lines_all a, apps.oe_order_headers_all h
              WHERE     a.flow_status_code IN
                           ('BOOKED',
                            'AWAITING_SHIPPING',
                            'AWAITING_RETURN',
                            'INVOICE_HOLD',
                            'FULFILLED',
                            'DRAFT',
                            'PENDING_CUSTOMER_ACCEPTANCE') 
                    AND a.CALCULATE_PRICE_FLAG NOT IN ('N','P')
                    AND h.order_type_id =
                           NVL (p_order_type_id, h.order_type_id)
                    AND h.header_id = a.header_id
                    AND h.header_id = NVL (p_header_id, h.header_id)
                    AND TRUNC (a.last_update_date) = TRUNC (SYSDATE))
      LOOP
         v_call_point := 'STAGE2';

         BEGIN
            v_locked_line :=
               is_row_locked (c1_rec.line_rowid, 'OE_ORDER_LINES_ALL');
            v_locked_header :=
               is_row_locked (c1_rec.header_rowid, 'OE_ORDER_HEADERS_ALL');

            v_call_point := 'STAGE3';
         EXCEPTION
            WHEN OTHERS
            THEN
               v_locked_line := 'Y';
               v_locked_header := 'Y';
         END;

         IF v_locked_line = 'N' AND v_locked_header = 'N'
         THEN
            v_call_point := 'STAGE4';
            oe_msg_pub.initialize;
            
            l_line_tbl (1) := oe_order_pub.g_miss_line_rec;
            l_line_tbl (1).line_id := c1_rec.line_id;
            l_line_tbl (1).header_id := c1_rec.header_id;
            l_line_tbl (1).calculate_price_flag := 'P'; --set to Partial price.
            l_line_tbl (1).operation := oe_globals.g_opr_update;
            oe_order_pub.process_order (
               p_api_version_number       => 1.0,
               p_header_rec               => l_header_rec,
               p_line_tbl                 => l_line_tbl,
               p_action_request_tbl       => l_action_request_tbl,
               x_return_status            => l_return_status,
               x_msg_count                => l_msg_count,
               x_msg_data                 => l_msg_data,
               x_header_rec               => o_header_rec,
               x_header_val_rec           => o_header_val_rec,
               x_header_adj_tbl           => o_header_adj_tbl,
               x_header_adj_val_tbl       => o_header_adj_val_tbl,
               x_header_price_att_tbl     => o_header_price_att_tbl,
               x_header_adj_att_tbl       => o_header_adj_att_tbl,
               x_header_adj_assoc_tbl     => o_header_adj_assoc_tbl,
               x_header_scredit_tbl       => o_header_scredit_tbl,
               x_header_scredit_val_tbl   => o_header_scredit_val_tbl,
               x_line_tbl                 => o_line_tbl,
               x_line_val_tbl             => o_line_val_tbl,
               x_line_adj_tbl             => o_line_adj_tbl,
               x_line_adj_val_tbl         => o_line_adj_val_tbl,
               x_line_price_att_tbl       => o_line_price_att_tbl,
               x_line_adj_att_tbl         => o_line_adj_att_tbl,
               x_line_adj_assoc_tbl       => o_line_adj_assoc_tbl,
               x_line_scredit_tbl         => o_line_scredit_tbl,
               x_line_scredit_val_tbl     => o_line_scredit_val_tbl,
               x_lot_serial_tbl           => o_lot_serial_tbl,
               x_lot_serial_val_tbl       => o_lot_serial_val_tbl,
               x_action_request_tbl       => o_action_request_tbl);

            IF l_return_status = 'S'
            THEN
               COMMIT;
            ELSE
               IF l_msg_count > 0
               THEN
                  v_call_point := 'STAGE5';
                  v_running_message1 := '';

                  FOR i IN 1 .. l_msg_count
                  LOOP
                     fnd_msg_pub.get (i,
                                      fnd_api.g_false,
                                      l_msg_data,
                                      l_msg_dummy);

                     IF NVL (v_running_message1, 'null') <> l_msg_data
                     THEN
                        write_log (
                              'Calculate Price Flag Update Error for order number ='
                           || c1_rec.order_number
                           || ', '
                           || ' LINE_NUMBER='
                           || c1_rec.line_number
                           || ', '
                           || 'LINE_ID='
                           || c1_rec.line_id
                           || (SUBSTR (
                                  'Msg' || TO_CHAR (i) || ': ' || l_msg_data,
                                  1,
                                  255)));
                     END IF;

                     v_running_message1 := l_msg_data;
                  END LOOP;
               END IF;
            END IF;

            DBMS_OUTPUT.put_line (
               l_return_status || '=' || l_msg_count || '=' || l_msg_data);
            COMMIT;
         END IF;
      END LOOP;
   EXCEPTION
      WHEN NO_DATA_FOUND THEN
      NULL;
      WHEN OTHERS
      THEN
         NULL;
         write_error ('Error in updating calculate price_flag procedure',
                      v_call_point);
   END upd_calc_price_flag_proc;
/*************************************************************************
        *   Procedure : webber_product_catalog
        *
        *   PURPOSE:   This procedure generates Product catalog for customer Webber. .
        *   Author: Ram Talluri- TMS #20140225-00208
        *   Creation_date 5/9/2014
        *   Last update Date 6/23/2014
        *
        * Version   Date         Name                TMS
        * 1.0     5/9/2014       Ram Talluri         TMS 20140225-00208   
        * 2.0     6/23/2014      Ram Talluri         TMS 20140620-00236 
        * 7.0     8/4/2015       Hari Prasad M       TMS 20150708-00048 --Webber eCatalog Program Error,Replace comma with space.
        * 8.0     01/22/2016     Pattabhi Avula      TMS#20160111-00035 - Webber Interface Missing Column -- Added comma in the UTL file        
        * 9.0   P.Vamshidhar        09/01/2016       TMS#20160824-00263 - Webber File
        * ************************************************************************/

PROCEDURE webber_product_catalog (errbuf OUT VARCHAR2, retcode OUT NUMBER)
IS
   p_line_tbl                 qp_preq_grp.line_tbl_type;
   p_qual_tbl                 qp_preq_grp.qual_tbl_type;
   p_line_attr_tbl            qp_preq_grp.line_attr_tbl_type;
   p_line_detail_tbl          qp_preq_grp.line_detail_tbl_type;
   p_line_detail_qual_tbl     qp_preq_grp.line_detail_qual_tbl_type;
   p_line_detail_attr_tbl     qp_preq_grp.line_detail_attr_tbl_type;
   p_related_lines_tbl        qp_preq_grp.related_lines_tbl_type;
   p_control_rec              qp_preq_grp.control_record_type;
   x_line_tbl                 qp_preq_grp.line_tbl_type;
   x_line_qual                qp_preq_grp.qual_tbl_type;
   x_line_attr_tbl            qp_preq_grp.line_attr_tbl_type;
   x_line_detail_tbl          qp_preq_grp.line_detail_tbl_type;
   x_line_detail_qual_tbl     qp_preq_grp.line_detail_qual_tbl_type;
   x_line_detail_attr_tbl     qp_preq_grp.line_detail_attr_tbl_type;
   x_related_lines_tbl        qp_preq_grp.related_lines_tbl_type;
   x_return_status            VARCHAR2 (240);
   x_return_status_text       VARCHAR2 (240);
   qual_rec                   qp_preq_grp.qual_rec_type;
   line_attr_rec              qp_preq_grp.line_attr_rec_type;
   line_rec                   qp_preq_grp.line_rec_type;
   detail_rec                 qp_preq_grp.line_detail_rec_type;
   ldet_rec                   qp_preq_grp.line_detail_rec_type;
   rltd_rec                   qp_preq_grp.related_lines_rec_type;
   l_pricing_contexts_tbl     qp_attr_mapping_pub.contexts_result_tbl_type;
   l_qualifier_contexts_tbl   qp_attr_mapping_pub.contexts_result_tbl_type;
   l_line_tbl_cnt             NUMBER := 0;

   i                          BINARY_INTEGER;
   l_version                  VARCHAR2 (240);
   l_file_val                 VARCHAR2 (60);
   l_modifier_name            VARCHAR2 (240);
   l_list_header_id           NUMBER;
   l_item_category            NUMBER;
   l_gm_selling_price         NUMBER;
   l_message_level            NUMBER;

   --variables for webber product catalog logic
   l_cust_account_id          NUMBER := 11078; --WW WEBBER LLC NO HOUSTON, account_number=113526000
   --:= apps.fnd_profile.VALUE ('XXWC_CSP_DUMMY_ACCOUNT'); --Dummy Customer for Pricing Reports
   l_cust_site_use_id         NUMBER := NULL;
   l_organization_id          NUMBER;
   l_list_line_id             NUMBER;
   l_manufacturer             VARCHAR2 (200);
   l_web_keywords             VARCHAR2 (200);
   l_uom_code                 VARCHAR2 (10);
   l_mfg_partnumber           VARCHAR2 (200);

   -- global file name variables
   --l_directory                VARCHAR2 (80) := 'XXWC_WCS_QP_EXTRACTS_DIR';--Commented by Ram Talluri for TMS #20140620-00236 6/23/2014
   l_directory                VARCHAR2 (80) := 'XXWC_QP_WEBBER_SKU_CATG_DIR';--Added by Ram Talluri for TMS #20140620-00236 6/23/2014
   l_webber_outfile           UTL_FILE.file_type;
   l_webber_file_name         VARCHAR2 (80) := 'wc_webber_prod_catalog.csv';
   l_webber_tmp_file_name     VARCHAR2 (80)
                                 := 'wc_webber_prod_catalog_tmp.csv';

   -- local error handling variables
   l_err_callfrom             VARCHAR2 (75)
      := 'XXWC_QP_ROUTINES_PKG.WEBBER_PRODUCT_CATALOG';
   l_err_callpoint            VARCHAR2 (75) := 'START';
   l_distro_list              VARCHAR2 (75)
                                 DEFAULT 'HDSOracleDevelopers@hdsupply.com';
   l_message                  VARCHAR2 (2000);
BEGIN
   fnd_file.put_line (fnd_file.LOG,
                      'Start of Procedure-Webber Product Catalog');
   l_webber_outfile :=
      UTL_FILE.fopen (l_directory, l_webber_tmp_file_name, 'w');

   --UTL_FILE.put_line (l_webber_outfile,'Barcode1|Barcode2/Part Number|Barcode3|Description|Manufacturer|Package Size|UOM|Price|Key Words');
-- Commented below code in Rev 9.0 by Vamshi 
/*   FOR r   
      IN (SELECT DISTINCT ms.inventory_item_id, ms.segment1, ms.Description --,mcs.category_set_name, mic.segment1
            FROM apps.mtl_item_categories_v mic,
                 apps.mtl_category_sets mcs,
                 apps.mtl_system_items_b ms
           WHERE     1 = 1
                 AND mic.CATEGORY_SET_ID = mcs.CATEGORY_SET_ID
                 AND mic.organization_id = ms.organization_id
                 AND mic.inventory_item_id = ms.inventory_item_id
                 AND mic.SEGMENT1 NOT IN ('N', 'Z', 'E')
                 AND mcs.CATEGORY_SET_NAME = 'Sales Velocity'
                 --AND ms.organization_id IN (223, 224, 367, 366, 276)       -- Commented by Mahesh for TMS#20150416-00086  on 04/21/2015
                 AND ms.organization_id IN (SELECT lookup_code
                                            FROM fnd_lookup_values_vl
                                            WHERE     lookup_type = 'XXWC_WEBBER_PRD_CTG_BRN'
                                                  AND enabled_flag = 'Y'       )   -- Added by Mahesh for TMS#20150416-00086  on 04/21/2015
                 AND ms.item_type NOT IN
                        ('RE_RENT',
                         'REPAIR',
                         'RENTAL',
                         'INTANGIBLE',
                         'SERVICE ITEM',
                         'SPECIAL',
                         'DISCONTINUE')
                 )
*/        
-- Added below code in Rev 9.0 by Vamshi
-- Executing gather stats for custom table 
FND_FILE.PUT_LINE(FND_FILE.log,' EIS_DATA_MANAGMNT_DTLS Gather stats start');

DBMS_STATS.gather_table_stats('XXEIS', 'EIS_DATA_MANAGMNT_DTLS');

FND_FILE.PUT_LINE(FND_FILE.log,' EIS_DATA_MANAGMNT_DTLS Gather stats Completed');

 FOR r   
      IN (SELECT msib.inventory_item_id, msib.segment1, msib.Description
  FROM apps.mtl_system_items_b msib
 WHERE     EXISTS
              (SELECT 1
                 FROM apps.mtl_system_items_b ms1,
                      (SELECT LOOKUP_CODE item_type
                         FROM apps.fnd_COMMON_lookups
                        WHERE     LOOKUP_type = 'ITEM_TYPE'
                              AND application_id = 401
                              AND LOOKUP_CODE NOT IN ('RE_RENT',
                                                      'REPAIR',
                                                      'RENTAL',
                                                      'INTANGIBLE',
                                                      'SERVICE ITEM',
                                                      'SPECIAL',
                                                      'DISCONTINUE')) fcl,
                      (SELECT TO_CHAR (lookup_code) organization_id
                         FROM APPS.fnd_lookup_values_vl
                        WHERE     lookup_type = 'XXWC_WEBBER_PRD_CTG_BRN'
                              AND enabled_flag = 'Y') flv,
                      (SELECT CATEGORY_ID
                         FROM APPS.MTL_CATEGORIES_B
                        WHERE segment1 NOT IN ('N', 'Z', 'E')) mc,
                      apps.mtl_item_categories mic,
                      apps.MTL_CATEGORY_SETS_TL MCS
                WHERE     1 = 1
                      AND ms1.item_type = fcl.item_type
                      AND ms1.organization_id = flv.organization_id
                      AND mic.organization_id = ms1.organization_id
                      AND mic.inventory_item_id = ms1.inventory_item_id
                      AND MCS.CATEGORY_SET_NAME = 'Sales Velocity'
                      AND mic.category_set_id = mcs.category_set_id
                      AND mic.category_id = mc.category_id
                      AND ms1.inventory_item_id = msib.inventory_item_id)
       AND msib.organization_id = 222
                 )         
   LOOP
      l_item_category := NULL;

     --check to see if item exists in branch 711, if not use the first one available.
     l_err_callpoint:='Check for item-orgn assignment';
      BEGIN
         SELECT ORGANIZATION_ID
           INTO l_organization_id
           FROM apps.mtl_system_items_b
          WHERE     1 = 1
                AND organization_id = 224
                AND inventory_item_id = r.inventory_item_id;
      EXCEPTION
         WHEN NO_DATA_FOUND
         THEN
            SELECT ORGANIZATION_ID
              INTO l_organization_id
              FROM apps.mtl_system_items_b
             WHERE     1 = 1
                   --AND organization_id IN (223, 367, 366, 276)      -- Commented by Mahesh for TMS#20150416-00086  on 04/21/2015
                   AND organization_id IN (SELECT lookup_code
                                           FROM fnd_lookup_values_vl
                                           WHERE     lookup_type = 'XXWC_WEBBER_PRD_CTG_BRN'
                                                 AND enabled_flag = 'Y' )  -- Added by Mahesh for TMS#20150416-00086  on 04/21/2015
                   AND inventory_item_id = r.inventory_item_id
                   AND ROWNUM = 1;
         WHEN OTHERS
         THEN
            l_organization_id := 222;
      END;
      
      --Added by Ram Talluri for TMS #20140620-00236 6/23/2014
      --XXWC.XXWC_PA_WAREHOUSE_GT table needs to be populated to return accurate price values from qp_preq_pub.price_request API
      
        BEGIN
            COMMIT; --This commit is required to delete previously populated records from temp table
            
            INSERT INTO XXWC.XXWC_PA_WAREHOUSE_GT
                VALUES (l_organization_id);
                
        EXCEPTION
        WHEN OTHERS
        THEN
            l_err_callpoint := 'Failed to insert temp table';
            NULL;
        END;
        
      --Changes end for TMS #20140620-00236 6/23/2014



     --Derive the item category id
     l_err_callpoint:='Deriving category id';
      BEGIN
         SELECT category_id
           INTO l_item_category
           FROM apps.mtl_item_categories_v
          WHERE     inventory_item_id = r.inventory_item_id
                AND category_set_id = 1100000062
                AND organization_id = NVL (l_organization_id, 222);
      EXCEPTION
         WHEN OTHERS
         THEN
            l_item_category := NULL;
      END;

      qp_attr_mapping_pub.build_contexts (
         p_request_type_code           => 'ONT',
         p_pricing_type                => 'L',
         x_price_contexts_result_tbl   => l_pricing_contexts_tbl,
         x_qual_contexts_result_tbl    => l_qualifier_contexts_tbl);

      l_line_tbl_cnt := l_line_tbl_cnt + 1;

      ---- Control Record
      p_control_rec.pricing_event := 'LINE';
      p_control_rec.calculate_flag := 'Y';
      p_control_rec.simulation_flag := 'Y';
      p_control_rec.rounding_flag := 'Q';
      p_control_rec.manual_discount_flag := 'Y';
      p_control_rec.request_type_code := 'ONT';
      p_control_rec.temp_table_insert_flag := 'Y';

      ---- Line Records ---------
      line_rec.request_type_code := 'ONT';
      line_rec.line_id := -1; -- Order Line Id. This can be any thing for this script
      line_rec.line_index := '1';                        -- Request Line Index
      line_rec.line_type_code := 'LINE';        -- LINE or ORDER(Summary Line)
      line_rec.pricing_effective_date := SYSDATE; -- Pricing as of what date ?
      line_rec.active_date_first := SYSDATE; -- Can be Ordered Date or Ship Date
      line_rec.active_date_second := SYSDATE; -- Can be Ordered Date or Ship Date
      line_rec.active_date_first_type := 'NO TYPE';                -- ORD/SHIP
      line_rec.active_date_second_type := 'NO TYPE';               -- ORD/SHIP
      line_rec.line_quantity := 1;                         -- Ordered Quantity

     --Derive the item's primary UOM
     l_err_callpoint:='Deriving item primary UOM';
      BEGIN
         SELECT msib.primary_uom_code
           INTO line_rec.line_uom_code
           FROM apps.mtl_system_items_b msib
          WHERE     msib.inventory_item_id = r.inventory_item_id
                AND msib.organization_id = NVL (l_organization_id, 222);
      EXCEPTION
         WHEN OTHERS
         THEN
            line_rec.line_uom_code := 'EA';
      END;


      line_rec.currency_code := 'USD';                        -- Currency Code
      line_rec.price_flag := 'Y'; -- Price Flag can have 'Y' , 'N'(No pricing) , 'P'(Phase)
      p_line_tbl (1) := line_rec;

      ---- Line Attribute Record
      line_attr_rec.line_index := 1;
      line_attr_rec.pricing_context := 'ITEM';
      line_attr_rec.pricing_attribute := 'PRICING_ATTRIBUTE1';
      line_attr_rec.pricing_attr_value_from := TO_CHAR (r.inventory_item_id); -- INVENTORY ITEM ID
      line_attr_rec.validated_flag := 'N';
      p_line_attr_tbl (1) := line_attr_rec;



      IF l_item_category IS NOT NULL
      THEN
         line_attr_rec.line_index := 1;
         line_attr_rec.pricing_context := 'ITEM';                           --
         line_attr_rec.pricing_attribute := 'PRICING_ATTRIBUTE2';
         line_attr_rec.pricing_attr_value_from := TO_CHAR (l_item_category); -- Category ID
         line_attr_rec.validated_flag := 'N';
         p_line_attr_tbl (2) := line_attr_rec;
      END IF;


      qual_rec.line_index := 1;
      qual_rec.qualifier_context := 'ORDER';
      qual_rec.qualifier_attribute := 'QUALIFIER_ATTRIBUTE18';
      qual_rec.qualifier_attr_value_from :=
         TO_CHAR (NVL (l_organization_id, 224));          -- SHIP_FROM_ORG_ID;
      qual_rec.comparison_operator_code := '=';
      qual_rec.validated_flag := 'Y';
      p_qual_tbl (1) := qual_rec;


      qual_rec.line_index := 1;
      qual_rec.qualifier_context := 'CUSTOMER';
      qual_rec.qualifier_attribute := 'QUALIFIER_ATTRIBUTE32';
      qual_rec.qualifier_attr_value_from := TO_CHAR (l_cust_account_id); -- CUSTOMER ID;
      qual_rec.comparison_operator_code := '=';
      qual_rec.validated_flag := 'Y';
      p_qual_tbl (2) := qual_rec;
      
      l_err_callpoint:='Prior to API Call';

      qp_preq_pub.price_request (p_line_tbl,
                                 p_qual_tbl,
                                 p_line_attr_tbl,
                                 p_line_detail_tbl,
                                 p_line_detail_qual_tbl,
                                 p_line_detail_attr_tbl,
                                 p_related_lines_tbl,
                                 p_control_rec,
                                 x_line_tbl,
                                 x_line_qual,
                                 x_line_attr_tbl,
                                 x_line_detail_tbl,
                                 x_line_detail_qual_tbl,
                                 x_line_detail_attr_tbl,
                                 x_related_lines_tbl,
                                 x_return_status,
                                 x_return_status_text);

      -- Getting new selling price
      l_err_callpoint:='After to API Call';
      i := x_line_tbl.FIRST;

      IF i IS NOT NULL
      THEN
         LOOP
            l_gm_selling_price := x_line_tbl (i).adjusted_unit_price;
            EXIT WHEN i = x_line_tbl.LAST;
            i := x_line_tbl.NEXT (i);
         END LOOP;
      END IF;


      i := x_line_detail_tbl.FIRST;
     --derive the modifier/price list name
     l_err_callpoint:='Deriving Modifier name';
      IF i IS NOT NULL
      THEN
         LOOP
            IF x_line_detail_tbl (i).automatic_flag = 'Y'
            THEN
               l_modifier_name := NULL;
               l_list_header_id := NULL;

               BEGIN
                  SELECT name
                    INTO l_modifier_name
                    FROM apps.qp_list_headers_vl
                   WHERE list_header_id =
                            x_line_detail_tbl (i).list_header_id;

                  l_list_header_id := x_line_detail_tbl (i).list_header_id;
                  l_list_line_id := x_line_detail_tbl (i).list_line_id;
               EXCEPTION
                  WHEN OTHERS
                  THEN
                     l_modifier_name := NULL;
                     l_list_header_id := NULL;
               END;
            END IF;

            EXIT WHEN i = x_line_detail_tbl.LAST;
            i := x_line_detail_tbl.NEXT (i);
         END LOOP;
      END IF;

     --below code is used to derive most commonly used vendor as manufacturer
     l_err_callpoint:='Deriving Manufaturer name';
      BEGIN
         l_manufacturer := NULL;

         SELECT SUBSTR (sourcing_vendor_name, 1, 200)
           INTO l_manufacturer
           FROM (  SELECT COUNT (1), sourcing_vendor_name
                     FROM xxeis.EIS_XXWC_DATA_MGMT_AUDIT_V
                    WHERE item = r.segment1
                 GROUP BY sourcing_vendor_name
                 ORDER BY COUNT (1) DESC)
          WHERE ROWNUM = 1;
      EXCEPTION
         WHEN OTHERS
         THEN
            l_manufacturer := NULL;
      END;
     
     --Below code used to derive item's key words from PDH
     l_err_callpoint:='Deriving Keywords';
      BEGIN
         l_web_keywords := NULL;

         SELECT SUBSTR (TL_EXT_ATTR3, 1, 200)
           INTO l_web_keywords
           FROM apps.EGO_MTL_SY_ITEMS_EXT_TL a
          WHERE     ATTR_GROUP_ID = 479
                AND TL_EXT_ATTR3 IS NOT NULL
                AND organization_id = 222
                AND inventory_item_id = r.inventory_item_id;
      EXCEPTION
         WHEN NO_DATA_FOUND
         THEN
            SELECT SUBSTR (mct.description, 1, 200)
              INTO l_web_keywords
              FROM apps.mtl_item_categories mic, apps.mtl_categories_tl mct
             WHERE     1 = 1
                   AND mic.category_id = mct.category_id
                   AND mic.inventory_item_id = r.inventory_item_id
                   AND mic.organization_id = 222
                   AND mic.category_set_id = 1100000062
                   AND ROWNUM = 1;
         WHEN OTHERS
         THEN
            l_web_keywords := NULL;
      END;
      
      --Below code used to derive Webber UOM from UOM mapping lookup
      l_err_callpoint:='Deriving Webber UOM';
      BEGIN
            
        SELECT meaning
          INTO l_uom_code
          FROM apps.fnd_lookup_values
         WHERE     1 = 1
           AND lookup_type = 'XXWC_INV_UOM_MAPPING_WEBBER'
           AND enabled_flag = 'Y'
           AND NVL (end_date_active, TRUNC (SYSDATE)) >= TRUNC (SYSDATE)
           AND lookup_code = line_rec.line_uom_code;
          
      EXCEPTION
      WHEN OTHERS THEN
      l_uom_code:=line_rec.line_uom_code;
      END;
      
      --Below code used to derive manufacturer part number from item cross references table
      l_err_callpoint:='Deriving vendor product number';
      BEGIN
      
      SELECT cross_reference
        INTO l_mfg_partnumber
        FROM apps.mtl_cross_references
       WHERE cross_reference_type='VENDOR'
         AND inventory_item_id=r.inventory_item_id
         AND ROWNUM=1;
      
      EXCEPTION
      WHEN OTHERS THEN
      l_mfg_partnumber:=NULL;
      END;
      
      l_err_callpoint:='Generating utl file';
      
      
      ----- Commented for Rev 7.0 Begin <<

     /* UTL_FILE.put_line (
         l_webber_outfile,
            r.segment1                                              --Barcode1
         || ','
         || l_mfg_partnumber                                        --Barcode2 OEM Item number
         || ','
         || NULL                                                    --Barcode3
         || ','
         || r.segment1                                           --Part number
         || ','
         || REPLACE (r.Description, ',', '_')                    --Description
         || ','
         || REPLACE (l_manufacturer, ',', '_')                  --Manufacturer
         || ','
         || '1'                                                 --Package Size
         || ','
         || l_uom_code                                       --UOM
         || ','
         || l_gm_selling_price                                         --Price
         || ','
         || REPLACE (l_web_keywords, ',', '_'));                   --Key words
         */
         --- Commented for Rev 7.0 End >>
         -- Added fro Rev 7.0 Begin <<
         UTL_FILE.put_line (
         l_webber_outfile,
            replace(r.segment1 ,',',' ')                                            --Barcode1
         || ','
         || replace(l_mfg_partnumber ,',',' ')                                       --Barcode2 OEM Item number
         || ','
         || NULL                                                    --Barcode3
         || ','
         || replace(r.segment1 ,',',' ')                                         --Part number
         || ','
         || REPLACE (r.Description, ',', '_')                    --Description
         || ','                                                 -- Version# 8.0
         || REPLACE (l_manufacturer, ',', '_')                  --Manufacturer
         || ','
         || '1'                                                 --Package Size
         || ','
         || replace(l_uom_code ,',', ' ')                                      --UOM
         || ','
         || replace(l_gm_selling_price  ,',',' ')                                       --Price
         || ','
         || REPLACE (l_web_keywords, ',', '_'));                   --Key words
         -- Added fro Rev 7.0 End >>
   END LOOP;

   UTL_FILE.fclose (l_webber_outfile);


   fnd_file.put_line (
      fnd_file.LOG,
         'Renaming price list file '
      || l_webber_tmp_file_name
      || ' => '
      || l_webber_file_name);
   UTL_FILE.frename (src_location    => l_directory,
                     src_filename    => l_webber_tmp_file_name,
                     dest_location   => l_directory,
                     dest_filename   => l_webber_file_name,
                     overwrite       => TRUE);

   fnd_file.put_line (fnd_file.LOG,
                      'Webber product catalog file is now generated');
   fnd_file.put_line (fnd_file.LOG,
                      'Number of items in file is ' || l_line_tbl_cnt);
   fnd_file.put_line (
      fnd_file.LOG,
      'File can be copied from XXWC_WCS_QP_EXTRACTS_DIR directory');
EXCEPTION
   WHEN OTHERS
   THEN
      errbuf := 'Error';
      retcode := 2;
      fnd_file.put_line (fnd_file.LOG, SQLERRM);

      IF UTL_FILE.is_open (l_webber_outfile)
      THEN
         UTL_FILE.fclose (l_webber_outfile);
      END IF;

      xxcus_error_pkg.xxcus_error_main_api (
         p_called_from         => l_err_callfrom,
         p_calling             => l_err_callpoint,
         p_ora_error_msg       => SQLERRM,
         p_error_desc          => l_message,
         p_distribution_list   => l_distro_list,
         p_module              => 'XXWC');
END webber_product_catalog;
   /*************************************************************************
        *   Procedure : get_price_break
        *
        *   PURPOSE:   This procedure gets the quantity price break information for order line.
        *  Author: Ram Talluri- TMS #20140528-00018
        * Creation_date 6/9/2014
        * Last update Date 6/9/2014
        * Version   Date     Name        TMS
        * 1.0     6/9/2014  Ram Talluri  TMS 20140528-00018  
        * 5.0     7/1/2014  Ram Talluri TMS 20140619-00072 
        *
        * ************************************************************************/
FUNCTION get_price_break (p_line_id IN NUMBER)
   RETURN VARCHAR2
IS
   l_return_value    VARCHAR2 (3000);
   l_count           NUMBER := 0;

   -- local error handling variables
   l_err_callfrom    VARCHAR2 (75) := 'XXWC_QP_ROUTINES_PKG.GET_PRICE_BREAK';
   l_err_callpoint   VARCHAR2 (75) := 'START';
   l_distro_list     VARCHAR2 (75) DEFAULT 'HDSOracleDevelopers@hdsupply.com';
   l_message         VARCHAR2 (2000);
BEGIN
   FOR c1_rec
      IN (  SELECT qlh.name,
                   qms.pricing_attr_value_from,
                   qms.pricing_attr_value_to,
                   qms.arithmetic_operator_type,
                   qms.formula,
                   qms.operand,
                   qms.attribute5,--modifier_type
                   qms.attribute4--special cost
              FROM apps.qp_list_headers qlh,
                   apps.qp_modifier_summary_v ql,
                   apps.oe_price_adjustments opj,
                   apps.qp_modifier_summary_v qms
             WHERE     1 = 1
                   AND opj.list_header_id = qlh.list_header_id
                   AND opj.list_line_id = ql.list_line_id
                   AND qlh.list_header_id = ql.list_header_id
                   AND opj.automatic_flag = 'Y'
                   AND ql.excluder_flag='N'
                   AND opj.line_id = p_line_id
                   AND opj.list_header_id = qms.list_header_id
                   AND qms.LIST_LINE_TYPE_CODE = 'DIS'
                   AND qms.product_attr_val = ql.product_attr_val
                   AND NVL (qms.end_date_active, TRUNC (SYSDATE)) >=
                          TRUNC (SYSDATE)
          ORDER BY TO_NUMBER (qms.pricing_attr_value_from) ASC)
   LOOP
      l_err_callpoint := 'IN LOOP';

      IF     l_count = 0
         AND c1_rec.pricing_attr_value_from IS NULL
         AND c1_rec.pricing_attr_value_to IS NULL --for all non price break records         
      THEN
         l_err_callpoint := 'for all non price break records';

         IF     c1_rec.arithmetic_operator_type = 'New Price'
            AND c1_rec.formula IS NULL
            AND c1_rec.attribute5='Contract Pricing'  --of CSP new price modifier only
         THEN
         l_err_callpoint := 'CSP new price modifier only';
            l_return_value :=
                  l_return_value
               || 'CSP - '
               || c1_rec.ARITHMETIC_OPERATOR_TYPE
               || ' - $'
               || c1_rec.operand
               || CHR (10);
         ELSIF c1_rec.arithmetic_operator_type = 'New Price'
            AND c1_rec.formula IS NULL
            AND c1_rec.attribute5='Vendor Quote'  --of VQN new price modifier only
         THEN
         l_err_callpoint := 'VQN new price modifier only';
            l_return_value :=
                  l_return_value
               || 'VQN - '
               || c1_rec.ARITHMETIC_OPERATOR_TYPE
               || ' - $'
               || c1_rec.operand
               || CHR (10)
               || 'SPECIAL COST - $'
               || c1_rec.attribute4
               || CHR (10);
         ELSIF     c1_rec.arithmetic_operator_type = 'New Price'
               AND c1_rec.formula = 'Percent Discount'
               AND c1_rec.attribute5='Contract Pricing' --CSP percent discount only
         THEN
         l_err_callpoint := 'CSP percent discount modifier only';
            l_return_value :=
                  l_return_value
               || 'CSP - '
               || 'PERCENT DISCOUNT - '
               || (1 - c1_rec.operand) * 100
               || '%'
               || CHR (10);
         ELSIF     c1_rec.arithmetic_operator_type = 'New Price'
               AND c1_rec.formula = 'Percent Discount'
               AND c1_rec.attribute5='Vendor Quote' --VQN percent discount only
         THEN
         l_err_callpoint := 'VQN percent discount modifier only';
            l_return_value :=
                  l_return_value
               || 'VQN - '
               || 'PERCENT DISCOUNT - '
               || (1 - c1_rec.operand) * 100
               || '%'
               || CHR (10)
               || 'SPECIAL COST - $'
               || c1_rec.attribute4
               || CHR (10);      
         ELSIF     c1_rec.arithmetic_operator_type = 'New Price'
               AND c1_rec.formula = 'Cost Escalator'
               AND c1_rec.attribute5='Contract Pricing' --CSP cost plus modifier only
         THEN
         l_err_callpoint := 'CSP cost plus modifier only';
            l_return_value :=
                  l_return_value
               || 'CSP - '
               || 'COST PLUS - '
               || c1_rec.operand * 100
               || '%GM'
               || CHR (10);
         ELSIF     c1_rec.arithmetic_operator_type = 'New Price'
               AND c1_rec.formula = 'Cost Escalator'
               AND c1_rec.attribute5='Vendor Quote' --VQN cost plus modifier only
         THEN
         l_err_callpoint := 'VQN cost plus modifier only';
            l_return_value :=
                  l_return_value
               || 'VQN - '
               || 'COST PLUS - '
               || c1_rec.operand * 100
               || '%GM'
               || CHR (10)
               || 'SPECIAL COST - $'
               || c1_rec.attribute4
               || CHR (10);
         ELSIF     c1_rec.arithmetic_operator_type = 'Amount'
               AND c1_rec.formula IS NULL
               AND c1_rec.attribute5='Contract Pricing' --CSP Amount discount only
         THEN
         l_err_callpoint := 'CSP amount discount modifier only';
            l_return_value :=
                  l_return_value
               || 'CSP - '
               || 'AMOUNT OFF - $'
               || c1_rec.operand
               || CHR (10);
         ELSIF     c1_rec.arithmetic_operator_type = 'Amount'
               AND c1_rec.formula IS NULL
               AND c1_rec.attribute5='Vendor Quote' --VQN Amount discount only
         THEN
         l_err_callpoint := 'VQN Amount discount modifier only';
            l_return_value :=
                  l_return_value
               || 'VQN - '
               || 'AMOUNT OFF - $'
               || c1_rec.operand
               || CHR (10)
               || 'SPECIAL COST - $'
               || c1_rec.attribute4
               || CHR (10);
         ELSIF c1_rec.arithmetic_operator_type = 'New Price'
            AND c1_rec.formula IS NULL
            AND c1_rec.attribute5 IS NULL  --NON CSP/VQN new price modifier only
         THEN
         l_err_callpoint := 'NON CSP/VQN New price modifier only';
            l_return_value :=
                  l_return_value
               --|| c1_rec.name--TMS #20140619-00072
               || CASE SUBSTR(c1_rec.NAME,-6,6) WHEN 'TRADER' THEN  'TRADER ' ELSE c1_rec.NAME END--TMS #20140619-00072
               || ' - '
               || c1_rec.ARITHMETIC_OPERATOR_TYPE
               || ' - $'
               || c1_rec.operand
               || CHR (10);
         ELSIF     c1_rec.arithmetic_operator_type = 'New Price'
               AND c1_rec.formula = 'Percent Discount'
               AND c1_rec.attribute5 IS NULL --NON CSP/VQN percent discount only
         THEN
         l_err_callpoint := 'NON CSP/VQN percent discount modifier only';
            l_return_value :=
                  l_return_value
               --|| c1_rec.name--TMS #20140619-00072
               || CASE SUBSTR(c1_rec.NAME,-6,6) WHEN 'TRADER' THEN  'TRADER ' ELSE c1_rec.NAME END--TMS #20140619-00072
               || ' - '
               || 'PERCENT DISCOUNT - '
               || (1 - c1_rec.operand) * 100
               || '%'
               || CHR (10);      
         ELSIF     c1_rec.arithmetic_operator_type = 'New Price'
               AND c1_rec.formula = 'Cost Escalator'
               AND c1_rec.attribute5 IS NULL --NON CSP/VQN cost plus modifier only
         THEN
         l_err_callpoint := 'NON CSP/VQN cost plus modifier only';
            l_return_value :=
                  l_return_value
               --|| c1_rec.name--TMS #20140619-00072
               || CASE SUBSTR(c1_rec.NAME,-6,6) WHEN 'TRADER' THEN  'TRADER ' ELSE c1_rec.NAME END--TMS #20140619-00072
               || ' - '
               || 'COST PLUS - '
               || c1_rec.operand * 100
               || '%GM'
               || CHR (10);
         ELSIF     c1_rec.arithmetic_operator_type = 'Amount'
               AND c1_rec.formula IS NULL
               AND c1_rec.attribute5 IS NULL --NON CSP/VQN Amount discount only
         THEN
         l_err_callpoint := 'NON CSP/VQN Amount modifier only';
            l_return_value :=
                  l_return_value
               --|| c1_rec.name--TMS #20140619-00072
               || CASE SUBSTR(c1_rec.NAME,-6,6) WHEN 'TRADER' THEN  'TRADER ' ELSE c1_rec.NAME END--TMS #20140619-00072
               || ' - '
               || 'AMOUNT OFF - $'
               || c1_rec.operand
               || CHR (10);
                        
         END IF;
      
      --Added by Ram Talluri for TMS #20140619-00072 6/19/2014 
      ELSIF c1_rec.arithmetic_operator_type = 'New Price'
            AND (c1_rec.pricing_attr_value_from IS NOT NULL OR c1_rec.pricing_attr_value_to IS NOT NULL)
            AND c1_rec.formula = 'Percent Discount'
            AND c1_rec.operand = 1 --Price breaks with no discount
      THEN
      l_err_callpoint := 'Qty Price break with no discount only';
               l_return_value :=
               l_return_value
            || 'QTY BREAK '
            || '    '
            || 'MARKET PRICE'
            || '    '
            || '    '
            || 'QTY: '
            || c1_rec.PRICING_ATTR_VALUE_FROM
            || ' - '
            || c1_rec.PRICING_ATTR_VALUE_TO
            || CHR (10);   
      --end by Ram Talluri for TMS #20140619-00072 6/19/2014         
         
      ELSIF c1_rec.arithmetic_operator_type = 'New Price'
            AND (c1_rec.pricing_attr_value_from IS NOT NULL OR c1_rec.pricing_attr_value_to IS NOT NULL)
            AND c1_rec.formula IS NULL --Price breaks with new price scenario only
      THEN
      l_err_callpoint := 'Price break New price modifier only';
               l_return_value :=
               l_return_value
            --|| c1_rec.name--TMS #20140619-00072
            || 'QTY BREAK ' --TMS #20140619-00072           
            || '    '
            || c1_rec.ARITHMETIC_OPERATOR_TYPE
            || ' $'
            || c1_rec.operand
            || '    '
            --|| 'QUANTITY: '--TMS #20140619-00072
            || 'QTY: '--TMS #20140619-00072
            || c1_rec.PRICING_ATTR_VALUE_FROM
            || ' - '
            || c1_rec.PRICING_ATTR_VALUE_TO
            || CHR (10);
      ELSIF     (c1_rec.pricing_attr_value_from IS NOT NULL OR c1_rec.pricing_attr_value_to IS NOT NULL)
            AND c1_rec.arithmetic_operator_type = 'New Price'
            AND c1_rec.formula = 'Percent Discount'--Price breaks with percent discount scenario only
      THEN
      l_err_callpoint := 'Price break percent discount modifier only';
               l_return_value :=
               l_return_value
            --|| c1_rec.name--TMS #20140619-00072
            || 'QTY BREAK ' --TMS #20140619-00072
            || '    '
            || 'PERCENT DISCOUNT - '
            || (1 - c1_rec.operand) * 100
            || '%'
            || '    '
            --|| 'QUANTITY: '--TMS #20140619-00072
            || 'QTY: '--TMS #20140619-00072
            || c1_rec.PRICING_ATTR_VALUE_FROM
            || ' - '
            || c1_rec.PRICING_ATTR_VALUE_TO
            || CHR (10);
      ELSIF    (c1_rec.pricing_attr_value_from IS NOT NULL OR c1_rec.pricing_attr_value_to IS NOT NULL)
            AND c1_rec.arithmetic_operator_type = 'New Price'
            AND c1_rec.formula = 'Cost Escalator'--Price breaks with cost plus scenario only
      THEN
      l_err_callpoint := 'Price break cost plus modifier only';
               l_return_value :=
               l_return_value
            --|| c1_rec.name--TMS #20140619-00072
            || 'QTY BREAK ' --TMS #20140619-00072
            || '    '
            || 'COST PLUS - '
            || c1_rec.operand * 100
            || '%GM'
            || '    '
            --|| 'QUANTITY: '--TMS #20140619-00072
            || 'QTY: '--TMS #20140619-00072
            || c1_rec.PRICING_ATTR_VALUE_FROM
            || ' - '
            || c1_rec.PRICING_ATTR_VALUE_TO
            || CHR (10);
      ELSIF     c1_rec.arithmetic_operator_type = 'Amount'
            AND (c1_rec.pricing_attr_value_from IS NOT NULL OR c1_rec.pricing_attr_value_to IS NOT NULL)--Price breaks with amount discount scenario only
      THEN
         l_err_callpoint := 'Price break Amount discount modifier only';
               l_return_value :=
               l_return_value
            --|| c1_rec.name--TMS #20140619-00072
            || 'QTY BREAK ' --TMS #20140619-00072
            || '    '
            || 'AMOUNT OFF - $'
            || c1_rec.operand
            || '    '
            --|| 'QUANTITY: '--TMS #20140619-00072
            || 'QTY: '--TMS #20140619-00072
            || c1_rec.PRICING_ATTR_VALUE_FROM
            || ' - '
            || c1_rec.PRICING_ATTR_VALUE_TO
            || CHR (10);
      END IF;

      l_count := l_count + 1;
   END LOOP;

   l_err_callpoint := 'After loop';

   RETURN UPPER(l_return_value);
EXCEPTION
   WHEN OTHERS
   THEN
      NULL;
      xxcus_error_pkg.xxcus_error_main_api (
         p_called_from         => l_err_callfrom,
         p_calling             => l_err_callpoint,
         p_ora_error_msg       => SQLERRM,
         p_error_desc          => l_message,
         p_distribution_list   => l_distro_list,
         p_module              => 'XXWC');
END get_price_break;
    
END XXWC_QP_ROUTINES_PKG;
/