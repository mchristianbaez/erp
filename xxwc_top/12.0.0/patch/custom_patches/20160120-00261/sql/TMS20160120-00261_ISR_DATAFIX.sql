/*************************************************************************
  $Header TMS20160120-00261_ISR_DATAFIX.sql $
  Module Name: TMS#20160120-00261 - Flip Date issue.

  PURPOSE: Data fix to update flip date as null for non-stock flag 'N'

  REVISIONS:
  Ver        Date         Author                Description
  ---------  -----------  ------------------    --------------------------
  1.0        22-Jan-2016  P.Vamshidhar          TMS#20160120-00261

**************************************************************************/ 
SET SERVEROUTPUT ON SIZE 1000000
CREATE TABLE XXWC.XXWC_ISR_20160120_00261 AS (SELECT * FROM xxwc.xxwc_isr_details_all)
/
DECLARE
BEGIN
   DBMS_OUTPUT.put_line ('TMS: 20160120-00261 - Before Update');

   UPDATE xxwc.xxwc_isr_details_all A
      SET A.FLIP_DATE = NULL
    WHERE     NVL (A.STK_FLAG, 'N') = 'N'
          AND A.FLIP_DATE =
                 (SELECT MIC.CREATION_DATE
                    FROM APPS.MTL_ITEM_CATEGORIES MIC,
                         APPS.MTL_CATEGORY_SETS MCS
                   WHERE     MIC.CATEGORY_SET_ID = MCS.CATEGORY_SET_ID
                         AND MCS.CATEGORY_SET_NAME LIKE 'Sales Velocity'
                         AND MIC.INVENTORY_ITEM_ID = a.inventory_item_id
                         AND mic.organization_id = a.organization_id);

   DBMS_OUTPUT.put_line (
         'TMS: 20160120-00261 ISR Data fix - Number of records updated: '
      || SQL%ROWCOUNT);

   COMMIT;

   DBMS_OUTPUT.put_line ('TMS: 20160120-00261 -  End Update');
EXCEPTION
   WHEN OTHERS
   THEN
      ROLLBACK;
      DBMS_OUTPUT.put_line ('TMS: 20160120-00261 -  Errors : ' || SQLERRM);
END;
/