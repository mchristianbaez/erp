/*
Task ID: 220180118-00136  - Delete invalid records from RCV interfaces
Created by - Vamshi
 */
SET SERVEROUTPUT ON SIZE 1000000

BEGIN
   DBMS_OUTPUT.put_line ('Before delete1');

   DELETE FROM apps.rcv_headers_interface
         WHERE GROUP_ID IN (SELECT GROUP_ID
                              FROM rcv_transactions_interface
                             WHERE requisition_line_id = 49312027);

   DBMS_OUTPUT.put_line ('Records deleted1-' || SQL%ROWCOUNT);
   COMMIT;

   DBMS_OUTPUT.put_line ('Before delete2');

   DELETE FROM apps.rcv_transactions_interface
         WHERE requisition_line_id = 49312027;

   DBMS_OUTPUT.put_line ('Records deleted2-' || SQL%ROWCOUNT);
   COMMIT;
EXCEPTION
   WHEN OTHERS
   THEN
      DBMS_OUTPUT.put_line ('Unable to delete ' || SQLERRM);
END;
/