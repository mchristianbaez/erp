/*************************************************************************
Copyright (c) 2012 HD Supply
All rights reserved.
**************************************************************************
  $Header XXWC_OE_ORDER_LINES_ALL_N3
  Module Name: XXWC_OE_ORDER_LINES_ALL_N3
  PURPOSE:

  REVISIONS:
  -- VERSION   DATE            AUTHOR(S)          DESCRIPTION
  -- -------   -----------     ------------       ---------      -------------------------
  -- 1.0       14-DEC-2016     Ashwin Sridhar     Initially Created TMS# 20161206-00247 
**************************************************************************/
CREATE INDEX XXWC_OE_ORDER_LINES_ALL_N3 ON ONT.OE_ORDER_LINES_ALL(SHIP_FROM_ORG_ID, SCHEDULE_SHIP_DATE);
/