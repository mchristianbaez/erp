create or replace 
PACKAGE BODY     XXEIS.EIS_XXWC_OM_LINE_DISP_PKG AS
 --//============================================================================
--//  
--// Change Request 			:: Performance Issue  
--//
--// Object Usage 				:: This Object Referred by "LINES DISPOSITION REPORT"
--//
--// Object Name         		:: EIS_XXWC_OM_LINE_DISP_PKG
--//
--// Object Type         		:: Package Body
--//
--// Object Description  		:: This Package will trigger in before report and insert the values into Table
--//
--// Version Control
--//============================================================================
--// Version    Author           Date          Description
--//----------------------------------------------------------------------------
--// 1.0        PRAMOD  	    04/18/2016      Initial Build  --TMS#20160426-00102  --Performance Tuuning
--//============================================================================

    procedure OM_LINE_DISP (P_PROCESS_ID            in number
                           ,p_Ordered_Date_From     IN date
                           ,p_Ordered_Date_To       IN date
                           ,p_location              in varchar2
                           ,p_pricing_zone          in varchar2
                           ,p_item_number           in varchar2
                           ,p_Item_Description      in varchar2
                          )
   IS
      --//============================================================================
--//
--// Object Name         :: om_line_disp  
--//
--// Object Usage 		 :: This Object Referred by "LINES DISPOSITION REPORT"
--//
--// Object Type         :: Procedure
--//
--// Object Description  :: This  procedure is created based on the EIS_XXWC_OM_LINE_DISP_V View.Due to the 
--// 						performance issue ,spilt the view definition based on the driving tables and driving data.
--//
--// Version Control
--//============================================================================
--// Version    Author           Date          Description
--//----------------------------------------------------------------------------
--// 1.0        PRAMOD  	  04/22/2016       Initial Build  --TMS#20160426-00102  --Performance Tuuning
--//============================================================================
      L_QUERY               varchar2 (32000);
      L_QUERY2              varchar2 (32000);
      l_query3              VARCHAR2 (32000);
      lv_program_location   VARCHAR2 (2000);
      L_REF_CURSOR          CURSOR_TYPE;
      L_REF_CURSOR2         CURSOR_TYPE;
      L_REF_CURSOR3         CURSOR_TYPE;
      L_REF_CURSOR4         cursor_type;
      l_insert_recd_cnt     NUMBER;
      L_PRICING_ZONE        varchar2(1000);
      L_item_number         varchar2(1000);
      L_Item_Description    varchar2(1000);

   BEGIN
    

      IF p_pricing_zone IS NOT NULL THEN    
       L_PRICING_ZONE:= L_PRICING_ZONE||' and mp.attribute6 in  ('||xxeis.eis_rs_utility.get_param_values(p_pricing_zone)||' )';
        end if;
        
        
        IF p_item_number IS NOT NULL THEN    
       L_item_number:= L_item_number||' and  msi.segment1 in  ('||xxeis.eis_rs_utility.get_param_values(p_item_number)||' )';
        end if;
        
        
         IF p_Item_Description IS NOT NULL THEN    
       L_Item_Description:= L_Item_Description||' and MSI.DESCRIPTION in  ('||xxeis.eis_rs_utility.get_param_values(p_Item_Description)||' )';
        end if;

--fnd_file.put_line (fnd_file.LOG,'Start');

      L_QUERY :=
'  select '||P_PROCESS_ID||' process_id,h.header_id,l.line_id
     from OE_ORDER_HEADERS H,OE_ORDER_LINES L
        where H.HEADER_ID =L.HEADER_ID
    and L.CREATION_DATE >= '''||p_Ordered_Date_From||'''
    AND L.CREATION_DATE <=TO_DATE ('''||p_Ordered_Date_To||''', ''dd-mon-yy'') + 0.9999
    and   (  L.SHIP_FROM_ORG_ID <> H.SHIP_FROM_ORG_ID
    and L.FLOW_STATUS_CODE in (''PRE-BILLING_ACCEPTANCE'', ''CLOSED''))
    and not exists (  SELECT 1
                         FROM 
                            MTL_ITEM_CATEGORIES MIC
                                 WHERE     h.ship_from_org_id = mic.organization_id
                                       AND l.inventory_item_id = mic.inventory_item_id
                                       AND MIC.CATEGORY_SET_ID =1100000062
                                       
and MIC.CATEGORY_ID in (4278,4279)
           --            mic.category_concat_segs IN (''99.99RT'', ''99.99RR'')                                  
           --            AND mic.category_set_id = mdcs.category_set_id
           --            AND mdcs.category_set_name = ''Inventory Category''

and h.order_type_id IN (1009,1013,1014)
           --            (UPPER (ott.name) IN (''WC LONG TERM RENTAL''
           --             ,''WC SHORT TERM RENTAL''
           --             ,''REPAIR ORDER''))
                                    )';
                                    
                                    
            L_QUERY2 :=
'    select '||P_PROCESS_ID||' process_id,h.header_id,l.line_id
         from OE_ORDER_HEADERS H,OE_ORDER_LINES L
            where H.HEADER_ID =L.HEADER_ID
    and L.CREATION_DATE >= '''||p_Ordered_Date_From||'''
    AND L.CREATION_DATE <=TO_DATE ('''||p_Ordered_Date_To||''', ''dd-mon-yy'') + 0.9999
    and L.FLOW_STATUS_CODE = ''CANCELLED''
    and not exists (  SELECT 1
                            FROM 
                                MTL_ITEM_CATEGORIES MIC
                                  WHERE      h.order_type_id IN (1009,1013,1014)
                                       AND h.ship_from_org_id = mic.organization_id
                                       AND l.inventory_item_id = mic.inventory_item_id
                                       AND MIC.CATEGORY_SET_ID =1100000062
                                       AND MIC.CATEGORY_ID IN (4278,4279)
                                    )';

      
      OPEN l_ref_cursor FOR l_query;

      LOOP
         FETCH L_REF_CURSOR bulk collect into G_VIEW_TAB limit 10000;

                  FORALL S in 1 .. G_VIEW_TAB.COUNT    
                     insert into XXEIS.EIS_HEADER_LINE_TAB
                          VALUES G_VIEW_TAB (S); 

EXIT when L_REF_CURSOR%NOTFOUND;
  end LOOP;
      CLOSE l_ref_cursor;
         commit;
         
               OPEN l_ref_cursor2 FOR l_query2;

      LOOP
         FETCH L_REF_CURSOR2 bulk collect into G_VIEW_TAB2 limit 10000;

                  FORALL S in 1 .. G_VIEW_TAB2.COUNT    
                     insert into XXEIS.EIS_HEADER_LINE_TAB   --TMS#20160426-00102    by PRAMOD  on 04-18-2016
                          VALUES G_VIEW_TAB2 (S); 

EXIT when L_REF_CURSOR2%NOTFOUND;
  end LOOP;
      close L_REF_CURSOR2;
         commit;

         G_VIEW_TAB2.delete;
         
         OPEN l_ref_cursor3 FOR 'select * from XXEIS.EIS_HEADER_LINE_TAB where process_id='||p_process_id||'';

              FETCH L_REF_CURSOR3 bulk collect into G_VIEW_TAB3;
         
         close L_REF_CURSOR3;
         
         fnd_file.put_line (fnd_file.LOG,'main L_QUERY3');
         
         for S in 1..G_VIEW_TAB3.COUNT
         LOOP          
         L_QUERY3:='SELECT 
                   '||p_process_id||' process_id,
                   msi.segment1 item_number
                   ,MSI.DESCRIPTION ITEM_DESCRIPTION
                   ,mp.organization_code branch
                   ,mp.attribute6 pricing_zone
                   ,  NVL (ol.cancelled_quantity, 0) qty_of_items_cancelled
                   , (DECODE (ol.flow_status_code
                             ,''CANCELLED'', 0
                             ,DECODE (ol.ship_from_org_id, oh.ship_from_org_id, 0, ol.ordered_quantity)))
                       qty_of_items_dispositioned
                   , (DECODE (ol.flow_status_code, ''CANCELLED'', 1, 0)) times_line_cancelled
                   , (DECODE (ol.flow_status_code
                             ,''CANCELLED'', 0
                             ,DECODE (ol.ship_from_org_id, oh.ship_from_org_id, 0, 1)))
                       times_line_dispositioned
                   ,xxeis.eis_rs_xxwc_com_util_pkg.get_tot_sales_dlr (ol.line_id) total_sales
                   ,xxeis.eis_rs_xxwc_com_util_pkg.get_inv_vel_cat_class (msi.inventory_item_id
                                                                         ,msi.organization_id)
                       velocity
                   ,min_minmax_quantity MIN
                   ,MAX_MINMAX_QUANTITY max
                   ,min_minmax_quantity reorder_point
                   ,mp.organization_code header_branch
                   ,msi.inventory_item_id
                   ,msi.organization_id
               --descr#flexfield#start
               --descr#flexfield#end
               --gl#accountff#start
               --gl#accountff#end
               FROM mtl_system_items_kfv msi
                   ,oe_order_headers oh
                   ,oe_order_lines ol
                   ,mtl_parameters mp
              where     1 = 1
                    AND msi.organization_id = mp.organization_id
                    AND msi.organization_id = oh.ship_from_org_id
                    AND msi.inventory_item_id = ol.inventory_item_id
                    and OL.HEADER_ID = OH.HEADER_ID
                    and mp.organization_code='''||p_location||'''
                    and OH.HEADER_ID = :1
                    and OL.LINE_ID   = :2
                    '||L_Item_Description||'
                    '||L_item_number||'
                    '||L_Item_Description||'
                    ';
                    
--                    fnd_file.put_line (fnd_file.LOG,'main L_QUERY3'||L_QUERY3);
                    
              open L_REF_CURSOR4  for l_query3 using G_VIEW_TAB3(S).HEADER_ID,G_VIEW_TAB3(S).LINE_ID;
    LOOP      
  --  fnd_file.put_line (fnd_file.LOG,'entered');
    FETCH  L_REF_CURSOR4 Bulk Collect  INTO G_MAIN_VIEW_TAB limit 10000;
                    
                 FORALL J in 1 .. G_MAIN_VIEW_TAB.COUNT    
                     insert into XXEIS.EIS_XXWC_OM_LINE_DISP_TAB    --TMS#20160426-00102    by PRAMOD  on 04-18-2016
                          values G_MAIN_VIEW_TAB(J); 
                          
                          EXIT when L_REF_CURSOR4%NOTFOUND;
  end LOOP;
 -- fnd_file.put_line (fnd_file.LOG,'entered 1');
      close L_REF_CURSOR4;
 --     fnd_file.put_line (fnd_file.LOG,'entered 2');
      end LOOP;
 --     fnd_file.put_line (fnd_file.LOG,'entered 3');
         commit;

      EXCEPTION
         WHEN OTHERS
         THEN
            fnd_file.put_line (
               fnd_file.LOG,
               'Inside Exception==>' || SUBSTR (SQLERRM, 1, 240));
   END;
   
   PROCEDURE CLEAR_TEMP_TABLES ( P_PROCESS_ID IN NUMBER)
AS
	--//============================================================================
--//
--// Object Name         :: clear_temp_tables
--//
--// Object Type         :: Procedure
--//
--// Object Description  :: This procedure will trigger in After Report level and delete all the records from the table based on that particular process_id.
--//
--// Version Control
--//============================================================================
--// Version    Author           Date          Description
--//----------------------------------------------------------------------------
--// 1.0        PRAMOD  	   04/22/2016      Initial Build  --TMS#20160426-00102 --Performance Tuuning
--//============================================================================  
  BEGIN  
  DELETE FROM XXEIS.EIS_XXWC_OM_LINE_DISP_TAB WHERE PROCESS_ID=P_PROCESS_ID;
  DELETE FROM XXEIS.EIS_HEADER_LINE_TAB WHERE PROCESS_ID=P_PROCESS_ID; 
  COMMIT;
END ;    

END;
/
