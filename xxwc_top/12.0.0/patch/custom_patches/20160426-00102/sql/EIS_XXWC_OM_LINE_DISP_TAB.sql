--------------------------------------------------------------------------------------
/*******************************************************************************
  Table: EIS_XXWC_OM_LINE_DISP_TAB
  Description: This table is used to get data from XXEIS.EIS_XXWC_OM_LINE_DISP_PKG Package.
  HISTORY
  ===============================================================================
  VERSION DATE               AUTHOR(S)       DESCRIPTION
  ------- -----------------  --------------- -----------------------------------------
  1.0     18-Apr-2016        PRAMOD   			TMS#20160426-00102  Performance Tuning
********************************************************************************/
DROP TABLE XXEIS.EIS_XXWC_OM_LINE_DISP_TAB CASCADE CONSTRAINTS;

CREATE TABLE XXEIS.EIS_XXWC_OM_LINE_DISP_TAB 
   (	PROCESS_ID NUMBER, 
	ITEM_NUMBER VARCHAR2(240 BYTE), 
	ITEM_DESCRIPTION VARCHAR2(240 BYTE), 
	BRANCH VARCHAR2(240 BYTE), 
	PRICING_ZONE VARCHAR2(240 BYTE), 
	QTY_OF_ITEMS_CANCELLED NUMBER, 
	QTY_OF_ITEMS_DISPOSITIONED NUMBER, 
	TIMES_LINE_CANCELLED NUMBER, 
	TIMES_LINE_DISPOSITIONED NUMBER, 
	TOTAL_SALES NUMBER, 
	VELOCITY VARCHAR2(240 BYTE), 
	MIN NUMBER, 
	MAX NUMBER, 
	REORDER_POINT NUMBER, 
	HEADER_BRANCH VARCHAR2(240 BYTE), 
	INVENTORY_ITEM_ID NUMBER, 
	ORGANIZATION_ID NUMBER
   )
/
