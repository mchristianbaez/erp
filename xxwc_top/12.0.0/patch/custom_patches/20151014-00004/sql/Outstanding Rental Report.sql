--Report Name            : Outstanding Rental Report
--Import Instructions : 1. If RTF template needs to be uploaded to report automatically then move the RTF template to physical path of APPS_DATA_FILE_DIR oracle directory. Check dba_directories view to get the physical path of oracle directory. 
--                         (If you want to use any other oracle directory other than APPS_DATA_FILE_DIR then replace APPS_DATA_FILE_DIR with your oracle directory name below.)
--                      2. Make sure XXEIS schema has read/write permissions to above used oracle directory and give full permissions to the directory at OS level.
--                      3. Run this file in XXEIS user from SQL*Plus or any tool like Toad, SQL Navigator. 
exec xxeis.eis_rs_ins.g_rtf_dir := 'APPS_DATA_FILE_DIR' ;
prompt Creating View Data for Outstanding Rental Report
set scan off define off
DECLARE
BEGIN 
--Inserting View EIS_XXWC_OM_OUTSTG_RTL_V
xxeis.eis_rs_ins.v( 'EIS_XXWC_OM_OUTSTG_RTL_V',660,'','','','','MR020532','XXEIS','Eis Xxwc Om Outstg Rtl V','EXOORV','','');
--Delete View Columns for EIS_XXWC_OM_OUTSTG_RTL_V
xxeis.eis_rs_utility.delete_view_rows('EIS_XXWC_OM_OUTSTG_RTL_V',660,FALSE);
--Inserting View Columns for EIS_XXWC_OM_OUTSTG_RTL_V
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_OUTSTG_RTL_V','RENTAL_LOCATION',660,'Rental Location','RENTAL_LOCATION','','','','MR020532','VARCHAR2','','','Rental Location','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_OUTSTG_RTL_V','RENTAL_START_DATE',660,'Rental Start Date','RENTAL_START_DATE','','','','MR020532','DATE','','','Rental Start Date','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_OUTSTG_RTL_V','QUANTITY_ON_RENT',660,'Quantity On Rent','QUANTITY_ON_RENT','','','','MR020532','NUMBER','','','Quantity On Rent','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_OUTSTG_RTL_V','ITEM_DESCRIPTION',660,'Item Description','ITEM_DESCRIPTION','','','','MR020532','VARCHAR2','','','Item Description','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_OUTSTG_RTL_V','ITEM_NUMBER',660,'Item Number','ITEM_NUMBER','','','','MR020532','VARCHAR2','','','Item Number','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_OUTSTG_RTL_V','SALES_PERSON_NAME',660,'Sales Person Name','SALES_PERSON_NAME','','','','MR020532','VARCHAR2','','','Sales Person Name','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_OUTSTG_RTL_V','CUSTOMER_NAME',660,'Customer Name','CUSTOMER_NAME','','','','MR020532','VARCHAR2','','','Customer Name','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_OUTSTG_RTL_V','ORDER_TYPE',660,'Order Type','ORDER_TYPE','','','','MR020532','VARCHAR2','','','Order Type','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_OUTSTG_RTL_V','DAYS_ON_RENT',660,'Days On Rent','DAYS_ON_RENT','','','','MR020532','NUMBER','','','Days On Rent','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_OUTSTG_RTL_V','CUSTOMER_JOB_NAME',660,'Customer Job Name','CUSTOMER_JOB_NAME','','','','MR020532','VARCHAR2','','','Customer Job Name','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_OUTSTG_RTL_V','ORDER_NUMBER',660,'Order Number','ORDER_NUMBER','','','','MR020532','NUMBER','','','Order Number','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_OUTSTG_RTL_V','EST_RENTAL_RETURN_DATE',660,'Est Rental Return Date','EST_RENTAL_RETURN_DATE','','','','MR020532','VARCHAR2','','','Est Rental Return Date','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_OUTSTG_RTL_V','CREATED_BY',660,'Created By','CREATED_BY','','','','MR020532','VARCHAR2','','','Created By','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_OUTSTG_RTL_V','LINE_NUMBER',660,'Line Number','LINE_NUMBER','','','','MR020532','VARCHAR2','','','Line Number','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_OUTSTG_RTL_V','MSI_ORGANIZATION_ID',660,'Msi Organization Id','MSI_ORGANIZATION_ID','','','','MR020532','NUMBER','','','Msi Organization Id','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_OUTSTG_RTL_V','INVENTORY_ITEM_ID',660,'Inventory Item Id','INVENTORY_ITEM_ID','','','','MR020532','NUMBER','','','Inventory Item Id','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_OUTSTG_RTL_V','ORGANIZATION_ID',660,'Organization Id','ORGANIZATION_ID','','','','MR020532','NUMBER','','','Organization Id','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_OUTSTG_RTL_V','PARTY_ID',660,'Party Id','PARTY_ID','','','','MR020532','NUMBER','','','Party Id','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_OUTSTG_RTL_V','LINE_ID',660,'Line Id','LINE_ID','','','','MR020532','NUMBER','','','Line Id','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_OUTSTG_RTL_V','HEADER_ID',660,'Header Id','HEADER_ID','','','','MR020532','NUMBER','','','Header Id','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_OUTSTG_RTL_V','PRI_BILL_TO_PH',660,'Pri Bill To Ph','PRI_BILL_TO_PH','','','','MR020532','VARCHAR2','','','Pri Bill To Ph','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_OUTSTG_RTL_V','BILL_TO_CUST_NAME',660,'Bill To Cust Name','BILL_TO_CUST_NAME','','','','MR020532','VARCHAR2','','','Bill To Cust Name','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_OUTSTG_RTL_V','BILL_TO_CUST_PH_NO',660,'Bill To Cust Ph No','BILL_TO_CUST_PH_NO','','','','MR020532','VARCHAR2','','','Bill To Cust Ph No','','','');
--Inserting View Components for EIS_XXWC_OM_OUTSTG_RTL_V
--Inserting View Component Joins for EIS_XXWC_OM_OUTSTG_RTL_V
END;
/
set scan on define on
prompt Creating Report LOV Data for Outstanding Rental Report
set scan off define off
DECLARE
BEGIN 
--Inserting Report LOVs - Outstanding Rental Report
xxeis.eis_rs_ins.lov( 660,'select nvl( party.party_name,CUST_ACCT.account_name) customer_name,CUST_ACCT.ACCOUNT_NUMBER
from  HZ_PARTIES PARTY, HZ_CUST_ACCOUNTS CUST_ACCT where CUST_ACCT.PARTY_ID = PARTY.PARTY_ID AND  PARTY.status=''A''
','','OM CUSTOMER NAME','This gives the Customer Name','XXEIS_RS_ADMIN',NULL,'N','','');
xxeis.eis_rs_ins.lov( 660,'SELECT C.LOCATION  SHIP_TO_LOCATION ,A.ACCOUNT_NUMBER
CUSTOMER_NUMBER,A.ACCOUNT_NAME CUSTOMER_NAME ,HOU.NAME OPERATING_UNIT
FROM HZ_CUST_ACCOUNTS A,HZ_CUST_ACCT_SITES B,
HZ_CUST_SITE_USES C,
HR_OPERATING_UNITS HOU
 WHERE A.CUST_ACCOUNT_ID=B.CUST_ACCOUNT_ID
 AND B.CUST_ACCT_SITE_ID=C.CUST_ACCT_SITE_ID
 AND SITE_USE_CODE=''SHIP_TO''
 AND B.ORG_ID=HOU.ORGANIZATION_ID
','','OM SHIP TO LOCATION','This gives ship to locations','XXEIS_RS_ADMIN',NULL,'N','','');
xxeis.eis_rs_ins.lov( 660,'SELECT ORGANIZATION_CODE WAREHOUSE,ORGANIZATION_NAME FROM ORG_ORGANIZATION_DEFINITIONS OOD WHERE SYSDATE < NVL(OOD.DISABLE_DATE,SYSDATE+1) AND EXISTS (SELECT 1 FROM XXEIS.EIS_ORG_ACCESS_V  WHERE organization_id = ood.organization_id )','','OM WAREHOUSE','','XXEIS_RS_ADMIN',NULL,'N','','');
xxeis.eis_rs_ins.lov( 660,'SELECT DISTINCT MSI.SEGMENT1   ITEM_NUMBER,
MSI.DESCRIPTION  ITEM_DESCRIPTION from mtl_system_items_b msi','','OM Item Number LOV','Order Item numbers','XXEIS_RS_ADMIN',NULL,'N','','');
xxeis.eis_rs_ins.lov( 660,'select distinct jrse.source_name  from
jtf_rs_salesreps jrs,
jtf_rs_resource_extns jrse
where    jrs.resource_id                    = jrse.resource_id','','XXWC Sales Manager Lov','','ANONYMOUS',NULL,'N','','');
END;
/
set scan on define on
prompt Creating Report Data for Outstanding Rental Report
set scan off define off
DECLARE
BEGIN 
--Deleting Report data - Outstanding Rental Report
xxeis.eis_rs_utility.delete_report_rows( 'Outstanding Rental Report' );
--Inserting Report - Outstanding Rental Report
xxeis.eis_rs_ins.r( 660,'Outstanding Rental Report','','Identifies all Order Types Rental and returns Customer Name, Customer Project, Salesman, Part Numbers and Days on Rent to help support and track duration of project and rentals adverting overdue issues and credits.','','','','MR020532','EIS_XXWC_OM_OUTSTG_RTL_V','Y','','','MR020532','','N','White Cap Reports','','CSV,Pivot Excel,EXCEL,','N');
--Inserting Report Columns - Outstanding Rental Report
xxeis.eis_rs_ins.rc( 'Outstanding Rental Report',660,'CUSTOMER_NAME','Customer Name','Customer Name','','','default','','3','N','','','','','','','','MR020532','N','N','','EIS_XXWC_OM_OUTSTG_RTL_V','','');
xxeis.eis_rs_ins.rc( 'Outstanding Rental Report',660,'ITEM_DESCRIPTION','Item Description','Item Description','','','default','','11','N','','','','','','','','MR020532','N','N','','EIS_XXWC_OM_OUTSTG_RTL_V','','');
xxeis.eis_rs_ins.rc( 'Outstanding Rental Report',660,'ITEM_NUMBER','Item Number','Item Number','','','default','','10','N','','','','','','','','MR020532','N','N','','EIS_XXWC_OM_OUTSTG_RTL_V','','');
xxeis.eis_rs_ins.rc( 'Outstanding Rental Report',660,'ORDER_TYPE','Order Type','Order Type','','','default','','2','N','','','','','','','','MR020532','N','N','','EIS_XXWC_OM_OUTSTG_RTL_V','','');
xxeis.eis_rs_ins.rc( 'Outstanding Rental Report',660,'QUANTITY_ON_RENT','Quantity On Rent','Quantity On Rent','','~~~','default','','12','N','','','','','','','','MR020532','N','N','','EIS_XXWC_OM_OUTSTG_RTL_V','','');
xxeis.eis_rs_ins.rc( 'Outstanding Rental Report',660,'RENTAL_LOCATION','Rental Loc','Rental Location','','','default','','1','N','','','','','','','','MR020532','N','N','','EIS_XXWC_OM_OUTSTG_RTL_V','','');
xxeis.eis_rs_ins.rc( 'Outstanding Rental Report',660,'RENTAL_START_DATE','Rental Start Date','Rental Start Date','','','default','','13','N','','','','','','','','MR020532','N','N','','EIS_XXWC_OM_OUTSTG_RTL_V','','');
xxeis.eis_rs_ins.rc( 'Outstanding Rental Report',660,'SALES_PERSON_NAME','Sales Person Name','Sales Person Name','','','default','','8','N','','','','','','','','MR020532','N','N','','EIS_XXWC_OM_OUTSTG_RTL_V','','');
xxeis.eis_rs_ins.rc( 'Outstanding Rental Report',660,'DAYS_ON_RENT','Days On Rent','Days On Rent','','~~~','default','','14','N','','','','','','','','MR020532','N','N','','EIS_XXWC_OM_OUTSTG_RTL_V','','');
xxeis.eis_rs_ins.rc( 'Outstanding Rental Report',660,'CUSTOMER_JOB_NAME','Job Site Name-Number','Customer Job Name','','','default','','4','N','','','','','','','','MR020532','N','N','','EIS_XXWC_OM_OUTSTG_RTL_V','','');
xxeis.eis_rs_ins.rc( 'Outstanding Rental Report',660,'ORDER_NUMBER','Order Number','Order Number','','~~~','default','','9','N','','','','','','','','MR020532','N','N','','EIS_XXWC_OM_OUTSTG_RTL_V','','');
xxeis.eis_rs_ins.rc( 'Outstanding Rental Report',660,'CREATED_BY','Created By','Created By','','','default','','15','N','','','','','','','','MR020532','N','N','','EIS_XXWC_OM_OUTSTG_RTL_V','','');
xxeis.eis_rs_ins.rc( 'Outstanding Rental Report',660,'EST_RENTAL_RETURN_DATE','Est Rental Return Date','Est Rental Return Date','','','default','','16','N','','','','','','','','MR020532','N','N','','EIS_XXWC_OM_OUTSTG_RTL_V','','');
xxeis.eis_rs_ins.rc( 'Outstanding Rental Report',660,'PRI_BILL_TO_PH','Primary Bill To Phone Num','Pri Bill To Ph','','','default','','5','N','','','','','','','','MR020532','N','N','','EIS_XXWC_OM_OUTSTG_RTL_V','','');
xxeis.eis_rs_ins.rc( 'Outstanding Rental Report',660,'BILL_TO_CUST_NAME','Bill To Contact Name','Bill To Cust Name','','','default','','6','N','','','','','','','','MR020532','N','N','','EIS_XXWC_OM_OUTSTG_RTL_V','','');
xxeis.eis_rs_ins.rc( 'Outstanding Rental Report',660,'BILL_TO_CUST_PH_NO','Bill To Contact Phone Num','Bill To Cust Ph No','','','default','','7','N','','','','','','','','MR020532','N','N','','EIS_XXWC_OM_OUTSTG_RTL_V','','');
--Inserting Report Parameters - Outstanding Rental Report
xxeis.eis_rs_ins.rp( 'Outstanding Rental Report',660,'Sales Person','Sales Person','SALES_PERSON_NAME','IN','XXWC Sales Manager Lov','','VARCHAR2','N','Y','6','','Y','CONSTANT','MR020532','Y','N','','','');
xxeis.eis_rs_ins.rp( 'Outstanding Rental Report',660,'Start Date','Start Date','RENTAL_START_DATE','>=','','','DATE','N','Y','1','','Y','CONSTANT','MR020532','Y','N','','Start Date','');
xxeis.eis_rs_ins.rp( 'Outstanding Rental Report',660,'End Date','End Date','RENTAL_START_DATE','<=','','','DATE','N','Y','2','','Y','CONSTANT','MR020532','Y','N','','End Date','');
xxeis.eis_rs_ins.rp( 'Outstanding Rental Report',660,'Warehouse','Warehouse','RENTAL_LOCATION','IN','OM WAREHOUSE','','VARCHAR2','N','Y','3','','Y','CONSTANT','MR020532','Y','N','','','');
xxeis.eis_rs_ins.rp( 'Outstanding Rental Report',660,'Customer Name','Customer Name','CUSTOMER_NAME','IN','OM CUSTOMER NAME','','VARCHAR2','N','Y','4','','Y','CONSTANT','MR020532','Y','N','','','');
xxeis.eis_rs_ins.rp( 'Outstanding Rental Report',660,'Customer Job Name','Customer Job Name','CUSTOMER_JOB_NAME','IN','OM SHIP TO LOCATION','','VARCHAR2','N','Y','5','','Y','CONSTANT','MR020532','Y','N','','','');
xxeis.eis_rs_ins.rp( 'Outstanding Rental Report',660,'Item Number','Item Number','ITEM_NUMBER','IN','OM Item Number LOV','','VARCHAR2','N','Y','7','','Y','CONSTANT','MR020532','Y','N','','','');
--Inserting Report Conditions - Outstanding Rental Report
xxeis.eis_rs_ins.rcn( 'Outstanding Rental Report',660,'SALES_PERSON_NAME','IN',':Sales Person','','','Y','6','Y','MR020532');
xxeis.eis_rs_ins.rcn( 'Outstanding Rental Report',660,'TRUNC(RENTAL_START_DATE)','>=',':Start Date','','','Y','1','Y','MR020532');
xxeis.eis_rs_ins.rcn( 'Outstanding Rental Report',660,'TRUNC(RENTAL_START_DATE)','<=',':End Date','','','Y','2','Y','MR020532');
xxeis.eis_rs_ins.rcn( 'Outstanding Rental Report',660,'RENTAL_LOCATION','IN',':Warehouse','','','Y','3','Y','MR020532');
xxeis.eis_rs_ins.rcn( 'Outstanding Rental Report',660,'CUSTOMER_NAME','IN',':Customer Name','','','Y','4','Y','MR020532');
xxeis.eis_rs_ins.rcn( 'Outstanding Rental Report',660,'CUSTOMER_JOB_NAME','IN',':Customer Job Name','','','Y','5','Y','MR020532');
xxeis.eis_rs_ins.rcn( 'Outstanding Rental Report',660,'ITEM_NUMBER','IN',':Item Number','','','Y','7','Y','MR020532');
--Inserting Report Sorts - Outstanding Rental Report
xxeis.eis_rs_ins.rs( 'Outstanding Rental Report',660,'CUSTOMER_NAME','ASC','MR020532','','');
xxeis.eis_rs_ins.rs( 'Outstanding Rental Report',660,'CUSTOMER_JOB_NAME','ASC','MR020532','','');
--Inserting Report Triggers - Outstanding Rental Report
--Inserting Report Templates - Outstanding Rental Report
xxeis.eis_rs_ins.R_Tem( 'Outstanding Rental Report','Outstanding Rental Report','Seeded template for Outstanding Rental Report','','','','','','','','','','','','MR020532');
--Inserting Report Portals - Outstanding Rental Report
--Inserting Report Dashboards - Outstanding Rental Report
--Inserting Report Security - Outstanding Rental Report
xxeis.eis_rs_ins.rsec( 'Outstanding Rental Report','660','','50926',660,'MR020532','','');
xxeis.eis_rs_ins.rsec( 'Outstanding Rental Report','660','','50927',660,'MR020532','','');
xxeis.eis_rs_ins.rsec( 'Outstanding Rental Report','660','','50928',660,'MR020532','','');
xxeis.eis_rs_ins.rsec( 'Outstanding Rental Report','660','','50929',660,'MR020532','','');
xxeis.eis_rs_ins.rsec( 'Outstanding Rental Report','660','','50931',660,'MR020532','','');
xxeis.eis_rs_ins.rsec( 'Outstanding Rental Report','660','','50930',660,'MR020532','','');
xxeis.eis_rs_ins.rsec( 'Outstanding Rental Report','660','','50856',660,'MR020532','','');
xxeis.eis_rs_ins.rsec( 'Outstanding Rental Report','660','','50857',660,'MR020532','','');
xxeis.eis_rs_ins.rsec( 'Outstanding Rental Report','660','','50858',660,'MR020532','','');
xxeis.eis_rs_ins.rsec( 'Outstanding Rental Report','660','','50859',660,'MR020532','','');
xxeis.eis_rs_ins.rsec( 'Outstanding Rental Report','660','','50860',660,'MR020532','','');
xxeis.eis_rs_ins.rsec( 'Outstanding Rental Report','660','','50861',660,'MR020532','','');
xxeis.eis_rs_ins.rsec( 'Outstanding Rental Report','20005','','50880',660,'MR020532','','');
xxeis.eis_rs_ins.rsec( 'Outstanding Rental Report','','LC053655','',660,'MR020532','','');
xxeis.eis_rs_ins.rsec( 'Outstanding Rental Report','','10010432','',660,'MR020532','','');
xxeis.eis_rs_ins.rsec( 'Outstanding Rental Report','','RB054040','',660,'MR020532','','');
xxeis.eis_rs_ins.rsec( 'Outstanding Rental Report','','RV003897','',660,'MR020532','','');
xxeis.eis_rs_ins.rsec( 'Outstanding Rental Report','','SS084202','',660,'MR020532','','');
xxeis.eis_rs_ins.rsec( 'Outstanding Rental Report','660','','21623',660,'MR020532','','');
xxeis.eis_rs_ins.rsec( 'Outstanding Rental Report','','SO004816','',660,'MR020532','','');
xxeis.eis_rs_ins.rsec( 'Outstanding Rental Report','660','','50886',660,'MR020532','','');
xxeis.eis_rs_ins.rsec( 'Outstanding Rental Report','660','','50901',660,'MR020532','','');
xxeis.eis_rs_ins.rsec( 'Outstanding Rental Report','660','','50870',660,'MR020532','','');
xxeis.eis_rs_ins.rsec( 'Outstanding Rental Report','660','','50871',660,'MR020532','','');
xxeis.eis_rs_ins.rsec( 'Outstanding Rental Report','660','','50869',660,'MR020532','','');
xxeis.eis_rs_ins.rsec( 'Outstanding Rental Report','20005','','50900',660,'MR020532','','');
xxeis.eis_rs_ins.rsec( 'Outstanding Rental Report','660','','51044',660,'MR020532','','');
xxeis.eis_rs_ins.rsec( 'Outstanding Rental Report','201','','50892',660,'MR020532','','');
--Inserting Report Pivots - Outstanding Rental Report
END;
/
set scan on define on
