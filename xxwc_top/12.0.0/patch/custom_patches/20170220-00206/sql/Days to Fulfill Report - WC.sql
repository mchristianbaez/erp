--Report Name            : Days to Fulfill Report - WC
--Import Instructions    : Run this file in XXEIS user from SQL*Plus or any tool like Toad, SQL Navigator. 
prompt Creating Object Data XXWC_EIS_ORD_DAYS_FULFILL_V
set scan off define off
DECLARE
mod_exist VARCHAR2(1);
BEGIN 
mod_exist := XXEIS.EIS_RSC_INSERT.check_if_module_exist(660);
IF mod_exist = 'Y' THEN 
--Inserting Object XXWC_EIS_ORD_DAYS_FULFILL_V
xxeis.eis_rsc_ins.v( 'XXWC_EIS_ORD_DAYS_FULFILL_V',660,'','','','','ANONYMOUS','XXEIS','Eis Ord Days FULFILL V','EODFV','','','VIEW','US','','');
--Delete Object Columns for XXWC_EIS_ORD_DAYS_FULFILL_V
xxeis.eis_rsc_utility.delete_view_rows('XXWC_EIS_ORD_DAYS_FULFILL_V',660,FALSE);
--Inserting Object Columns for XXWC_EIS_ORD_DAYS_FULFILL_V
xxeis.eis_rsc_ins.vc( 'XXWC_EIS_ORD_DAYS_FULFILL_V','AVG_COST',660,'Avg Cost','AVG_COST','','~T~D~2','','ANONYMOUS','NUMBER','','','Avg Cost','','','','US');
xxeis.eis_rsc_ins.vc( 'XXWC_EIS_ORD_DAYS_FULFILL_V','EXTENDED_PRICE',660,'Extended Price','EXTENDED_PRICE','','~T~D~2','','ANONYMOUS','NUMBER','','','Extended Price','','','','US');
xxeis.eis_rsc_ins.vc( 'XXWC_EIS_ORD_DAYS_FULFILL_V','SALESREP',660,'Salesrep','SALESREP','','','','ANONYMOUS','VARCHAR2','','','Salesrep','','','','US');
xxeis.eis_rsc_ins.vc( 'XXWC_EIS_ORD_DAYS_FULFILL_V','CREATED_BY',660,'Created By','CREATED_BY','','','','ANONYMOUS','VARCHAR2','','','Created By','','','','US');
xxeis.eis_rsc_ins.vc( 'XXWC_EIS_ORD_DAYS_FULFILL_V','UNIT_SELL_PRICE',660,'Unit Sell Price','UNIT_SELL_PRICE','','~T~D~2','','ANONYMOUS','NUMBER','','','Unit Sell Price','','','','US');
xxeis.eis_rsc_ins.vc( 'XXWC_EIS_ORD_DAYS_FULFILL_V','ITEM_DESCRIPTION',660,'Item Description','ITEM_DESCRIPTION','','','','ANONYMOUS','VARCHAR2','','','Item Description','','','','US');
xxeis.eis_rsc_ins.vc( 'XXWC_EIS_ORD_DAYS_FULFILL_V','ORDERED_ITEM',660,'Ordered Item','ORDERED_ITEM','','','','ANONYMOUS','VARCHAR2','','','Ordered Item','','','','US');
xxeis.eis_rsc_ins.vc( 'XXWC_EIS_ORD_DAYS_FULFILL_V','ORDER_QTY',660,'Order Qty','ORDER_QTY','','~T~D~2','','ANONYMOUS','NUMBER','','','Order Qty','','','','US');
xxeis.eis_rsc_ins.vc( 'XXWC_EIS_ORD_DAYS_FULFILL_V','INVOICE_NUMBER',660,'Invoice Number','INVOICE_NUMBER','','','','ANONYMOUS','VARCHAR2','','','Invoice Number','','','','US');
xxeis.eis_rsc_ins.vc( 'XXWC_EIS_ORD_DAYS_FULFILL_V','CLOSED_DATE',660,'Closed Date','CLOSED_DATE','','','','ANONYMOUS','DATE','','','Closed Date','','','','US');
xxeis.eis_rsc_ins.vc( 'XXWC_EIS_ORD_DAYS_FULFILL_V','REQUEST_DATE',660,'Request Date','REQUEST_DATE','','','','ANONYMOUS','DATE','','','Request Date','','','','US');
xxeis.eis_rsc_ins.vc( 'XXWC_EIS_ORD_DAYS_FULFILL_V','SCHEDULE_SHIP_DATE',660,'Schedule Ship Date','SCHEDULE_SHIP_DATE','','','','ANONYMOUS','DATE','','','Schedule Ship Date','','','','US');
xxeis.eis_rsc_ins.vc( 'XXWC_EIS_ORD_DAYS_FULFILL_V','ORDER_CHANNEL',660,'Order Channel','ORDER_CHANNEL','','','','ANONYMOUS','VARCHAR2','','','Order Channel','','','','US');
xxeis.eis_rsc_ins.vc( 'XXWC_EIS_ORD_DAYS_FULFILL_V','ORDER_NUMBER',660,'Order Number','ORDER_NUMBER','','','','ANONYMOUS','NUMBER','','','Order Number','','','','US');
xxeis.eis_rsc_ins.vc( 'XXWC_EIS_ORD_DAYS_FULFILL_V','CUSTOMER_NUMBER',660,'Customer Number','CUSTOMER_NUMBER','','','','ANONYMOUS','VARCHAR2','','','Customer Number','','','','US');
xxeis.eis_rsc_ins.vc( 'XXWC_EIS_ORD_DAYS_FULFILL_V','ACCOUNT_DESCRIPTION',660,'Account Description','ACCOUNT_DESCRIPTION','','','','ANONYMOUS','VARCHAR2','','','Account Description','','','','US');
xxeis.eis_rsc_ins.vc( 'XXWC_EIS_ORD_DAYS_FULFILL_V','STATUS',660,'Status','STATUS','','','','ANONYMOUS','VARCHAR2','','','Status','','','','US');
xxeis.eis_rsc_ins.vc( 'XXWC_EIS_ORD_DAYS_FULFILL_V','DAY_TO_FULFILL',660,'Day To Fulfill','DAY_TO_FULFILL','','','','ANONYMOUS','NUMBER','','','Day To Fulfill','','','','US');
xxeis.eis_rsc_ins.vc( 'XXWC_EIS_ORD_DAYS_FULFILL_V','LINE_NUMBER',660,'Line Number','LINE_NUMBER','','','','ANONYMOUS','VARCHAR2','','','Line Number','','','','US');
xxeis.eis_rsc_ins.vc( 'XXWC_EIS_ORD_DAYS_FULFILL_V','BOOKED_DATE',660,'Booked Date','BOOKED_DATE','','','','ANONYMOUS','DATE','','','Booked Date','','','','US');
xxeis.eis_rsc_ins.vc( 'XXWC_EIS_ORD_DAYS_FULFILL_V','SHIP_ORG',660,'Ship Org','SHIP_ORG','','','','ANONYMOUS','VARCHAR2','','','Ship Org','','','','US');
xxeis.eis_rsc_ins.vc( 'XXWC_EIS_ORD_DAYS_FULFILL_V','TRACKING_NUM',660,'Tracking Num','TRACKING_NUM','','','','ANONYMOUS','VARCHAR2','','','Tracking Num','','','','US');
xxeis.eis_rsc_ins.vc( 'XXWC_EIS_ORD_DAYS_FULFILL_V','LINE_STATUS',660,'Line Status','LINE_STATUS','','','','ANONYMOUS','VARCHAR2','','','Line Status','','','','US');
xxeis.eis_rsc_ins.vc( 'XXWC_EIS_ORD_DAYS_FULFILL_V','CUST_PO_NUMBER',660,'Cust Po Number','CUST_PO_NUMBER','','','','ANONYMOUS','VARCHAR2','','','Cust Po Number','','','','US');
xxeis.eis_rsc_ins.vc( 'XXWC_EIS_ORD_DAYS_FULFILL_V','PARTY_NUMBER',660,'Party Number','PARTY_NUMBER','','','','ANONYMOUS','VARCHAR2','','','Party Number','','','','US');
xxeis.eis_rsc_ins.vc( 'XXWC_EIS_ORD_DAYS_FULFILL_V','GM',660,'Gm','GM','','','','ANONYMOUS','VARCHAR2','','','Gm','','','','US');
xxeis.eis_rsc_ins.vc( 'XXWC_EIS_ORD_DAYS_FULFILL_V','HEADER_ID',660,'Header Id','HEADER_ID','','','','ANONYMOUS','NUMBER','','','Header Id','','','','US');
xxeis.eis_rsc_ins.vc( 'XXWC_EIS_ORD_DAYS_FULFILL_V','PARTY_ID',660,'Party Id','PARTY_ID','','','','ANONYMOUS','NUMBER','','','Party Id','','','','US');
xxeis.eis_rsc_ins.vc( 'XXWC_EIS_ORD_DAYS_FULFILL_V','ACCEPTANCE_DATE',660,'Acceptance Date','ACCEPTANCE_DATE','','','','ANONYMOUS','DATE','','','Acceptance Date','','','','');
xxeis.eis_rsc_ins.vc( 'XXWC_EIS_ORD_DAYS_FULFILL_V','MET_REQUEST_DATE',660,'Met Request Date','MET_REQUEST_DATE','','','','ANONYMOUS','NUMBER','','','Met Request Date','','','','');
--Inserting Object Components for XXWC_EIS_ORD_DAYS_FULFILL_V
--Inserting Object Component Joins for XXWC_EIS_ORD_DAYS_FULFILL_V
ELSE
RAISE_APPLICATION_ERROR (-20001,'Module does not exist. Import/Create the module with id 660');
END IF;
END;
/
prompt Creating Report LOV Data for Days to Fulfill Report - WC
set scan off define off
DECLARE
mod_exist VARCHAR2(1);
BEGIN 
mod_exist := XXEIS.EIS_RSC_INSERT.check_if_module_exist(660);
IF mod_exist = 'Y' THEN 
--Inserting Report LOVs - Days to Fulfill Report - WC
xxeis.eis_rsc_ins.lov( 660,'select  cust_acct.account_number Customer_Number,cust_acct.account_name customer_name,party.party_name
from  HZ_PARTIES PARTY, HZ_CUST_ACCOUNTS CUST_ACCT
 where CUST_ACCT.PARTY_ID = PARTY.PARTY_ID','','OM CUSTOMER NUMBER','This gives the Customer Number','XXEIS_RS_ADMIN',NULL,'N','','','','','','','US');
xxeis.eis_rsc_ins.lov( 660,'select distinct flow_status_code Order_Status from oe_order_headers_all','','OM Order Header Status','This Lov picks the  Order Status from Order Headers.','XXEIS_RS_ADMIN',NULL,'N','','','','','','','US');
xxeis.eis_rsc_ins.lov( 660,'SELECT ppf.full_name  created_by, fu.user_name user_name
  FROM FND_USER fu, per_people_f ppf
 WHERE TRUNC(SYSDATE) BETWEEN fu.START_DATE AND NVL(fu.end_date, hr_general.end_of_time)
   and TRUNC(SYSDATE) BETWEEN ppf.effective_start_date AND ppf.effective_end_date
   and fu.employee_id = ppf.person_id (+)
   and exists
          (select 1
             from fnd_user_resp_groups furg,
                  fnd_responsibility_vl fr
            where furg.responsibility_id   = fr.responsibility_id
              and fu.user_id = furg.user_id
              and fr.responsibility_name like ''XXEIS%'')','','Created By Lov','','XXEIS_RS_ADMIN',NULL,'N','','','','','','','US');
xxeis.eis_rsc_ins.lov( 660,'SELECT ORGANIZATION_CODE WAREHOUSE,ORGANIZATION_NAME FROM ORG_ORGANIZATION_DEFINITIONS OOD WHERE SYSDATE < NVL(OOD.DISABLE_DATE,SYSDATE+1) AND EXISTS (SELECT 1 FROM XXEIS.EIS_ORG_ACCESS_V  WHERE organization_id = ood.organization_id )
UNION
SELECT ''All'', ''All Organizations'' from Dual','','OM Warehouse All','','ANONYMOUS',NULL,'N','','','','','','','US');
xxeis.eis_rsc_ins.lov( '','select distinct attribute2 Order_Channel from oe_order_headers_all','','EIS XXWC Order Channel','Display the list of order channels','ANONYMOUS',NULL,'N','','','','','','','US');
xxeis.eis_rsc_ins.lov( '','select distinct papf.full_name from JTF_RS_SALESREPS jrs,per_all_people_f papf
where jrs.person_id = papf.person_id','','EIS XXWC Sales Rep','TO Display the list of sales rep employess','ANONYMOUS',NULL,'N','','','','','','','US');
xxeis.eis_rsc_ins.lov( '','select distinct p.party_number party_number ,p.party_name Customer_Name
from hz_parties p, hz_cust_accounts c
where p.party_id = c.party_id','','EIS XXWC PARTY ID','To Display Party id for the customer','ANONYMOUS',NULL,'N','','','','','','','US');
xxeis.eis_rsc_ins.lov( '','select list_name from XXWC_PARAM_LIST where list_type = ''Item'' and (PUBLIC_FLAG =''Y'' OR USER_ID = fnd_global.user_id)','','XXWC Item List','','ANONYMOUS',NULL,'N','','','','','','','US');
null;
ELSE
RAISE_APPLICATION_ERROR (-20001,'Module does not exist. Import/Create the module with id 660');
END IF;
END;
/
prompt Creating Report Data for Days to Fulfill Report - WC
set scan off define off
DECLARE
mod_exist varchar2(1);
BEGIN 
mod_exist := XXEIS.EIS_RSC_INSERT.check_if_module_exist(660);
IF mod_exist = 'Y' THEN 
--Deleting Report data - Days to Fulfill Report - WC
xxeis.eis_rsc_utility.delete_report_rows( 'Days to Fulfill Report - WC' );
--Inserting Report - Days to Fulfill Report - WC
xxeis.eis_rsc_ins.r( 660,'Days to Fulfill Report - WC','','Show the running total of open orders, as well as the days it took to fulfill orders in the past.  The report will be used by National Accounts Teams to manage open orders and analyze past orders, as some National Acct Customers are beginning to implement lead time scorecards.','','','','MR020532','XXWC_EIS_ORD_DAYS_FULFILL_V','Y','','','MR020532','','Y','White Cap Reports','','CSV,Pivot Excel,EXCEL,','N','','','','','','','','US','','','','');
--Inserting Report Columns - Days to Fulfill Report - WC
xxeis.eis_rsc_ins.rc( 'Days to Fulfill Report - WC',660,'AVG_COST','Avg Cost','Avg Cost','','$~T~D~2','default','','21','N','Y','','','','','','','MR020532','N','N','','XXWC_EIS_ORD_DAYS_FULFILL_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Days to Fulfill Report - WC',660,'EXTENDED_PRICE','Extended Price','Extended Price','','$~T~D~2','default','','23','N','Y','','','','','','','MR020532','N','N','','XXWC_EIS_ORD_DAYS_FULFILL_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Days to Fulfill Report - WC',660,'ACCOUNT_DESCRIPTION','Account Description','Account Description','','','default','','5','N','Y','','','','','','','MR020532','N','N','','XXWC_EIS_ORD_DAYS_FULFILL_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Days to Fulfill Report - WC',660,'CLOSED_DATE','Closed Date','Closed Date','','','default','','14','N','Y','','','','','','','MR020532','N','N','','XXWC_EIS_ORD_DAYS_FULFILL_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Days to Fulfill Report - WC',660,'CREATED_BY','Created By','Created By','','','default','','26','N','Y','','','','','','','MR020532','N','N','','XXWC_EIS_ORD_DAYS_FULFILL_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Days to Fulfill Report - WC',660,'CUSTOMER_NUMBER','Customer Number','Customer Number','','','default','','6','N','Y','','','','','','','MR020532','N','N','','XXWC_EIS_ORD_DAYS_FULFILL_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Days to Fulfill Report - WC',660,'DAY_TO_FULFILL','Day to Invoice','Day To Fulfill','','~~~','default','','1','N','Y','','','','','','','MR020532','N','N','','XXWC_EIS_ORD_DAYS_FULFILL_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Days to Fulfill Report - WC',660,'INVOICE_NUMBER','Invoice Number','Invoice Number','','','default','','15','N','Y','','','','','','','MR020532','N','N','','XXWC_EIS_ORD_DAYS_FULFILL_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Days to Fulfill Report - WC',660,'ITEM_DESCRIPTION','Item Description','Item Description','','','default','','20','N','Y','','','','','','','MR020532','N','N','','XXWC_EIS_ORD_DAYS_FULFILL_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Days to Fulfill Report - WC',660,'ORDERED_ITEM','Ordered Item','Ordered Item','','','default','','19','N','Y','','','','','','','MR020532','N','N','','XXWC_EIS_ORD_DAYS_FULFILL_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Days to Fulfill Report - WC',660,'SCHEDULE_SHIP_DATE','Schedule Ship Date','Schedule Ship Date','','','default','','11','N','Y','','','','','','','MR020532','N','N','','XXWC_EIS_ORD_DAYS_FULFILL_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Days to Fulfill Report - WC',660,'STATUS','Status','Status','','','default','','3','N','Y','','','','','','','MR020532','N','N','','XXWC_EIS_ORD_DAYS_FULFILL_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Days to Fulfill Report - WC',660,'UNIT_SELL_PRICE','Unit Sell Price','Unit Sell Price','','$~T~D~2','default','','22','N','Y','','','','','','','MR020532','N','N','','XXWC_EIS_ORD_DAYS_FULFILL_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Days to Fulfill Report - WC',660,'GM%','GM%','Unit Sell Price','NUMBER','~,~.~2','default','','24','Y','Y','','','','','','(EODFV.EXTENDED_PRICE - EODFV.AVG_COST*EODFV.ORDER_QTY)/decode(nvl(EODFV.EXTENDED_PRICE,0),0,1,EODFV.EXTENDED_PRICE)*100','MR020532','N','N','','XXWC_EIS_ORD_DAYS_FULFILL_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Days to Fulfill Report - WC',660,'ORDER_CHANNEL','Order Channel','Order Channel','','','default','','9','N','Y','','','','','','','MR020532','N','N','','XXWC_EIS_ORD_DAYS_FULFILL_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Days to Fulfill Report - WC',660,'ORDER_NUMBER','Order Number','Order Number','','~~~','default','','7','N','Y','','','','','','','MR020532','N','N','','XXWC_EIS_ORD_DAYS_FULFILL_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Days to Fulfill Report - WC',660,'ORDER_QTY','Order Qty','Order Qty','','~T~D~0','default','','18','N','Y','','','','','','','MR020532','N','N','','XXWC_EIS_ORD_DAYS_FULFILL_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Days to Fulfill Report - WC',660,'REQUEST_DATE','Request Date','Request Date','','','default','','12','N','Y','','','','','','','MR020532','N','N','','XXWC_EIS_ORD_DAYS_FULFILL_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Days to Fulfill Report - WC',660,'SALESREP','Salesrep','Salesrep','','','default','','27','N','Y','','','','','','','MR020532','N','N','','XXWC_EIS_ORD_DAYS_FULFILL_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Days to Fulfill Report - WC',660,'LINE_NUMBER','Line Number','Line Number','','','default','','17','N','Y','','','','','','','MR020532','N','N','','XXWC_EIS_ORD_DAYS_FULFILL_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Days to Fulfill Report - WC',660,'CUST_PO_NUMBER','Cust Po Number','Cust Po Number','','','default','','8','N','Y','','','','','','','MR020532','N','N','','XXWC_EIS_ORD_DAYS_FULFILL_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Days to Fulfill Report - WC',660,'BOOKED_DATE','Booked Date','Booked Date','','','default','','10','N','Y','','','','','','','MR020532','N','N','','XXWC_EIS_ORD_DAYS_FULFILL_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Days to Fulfill Report - WC',660,'SHIP_ORG','Ship Org','Ship Org','','','','','16','N','Y','','','','','','','MR020532','N','N','','XXWC_EIS_ORD_DAYS_FULFILL_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Days to Fulfill Report - WC',660,'TRACKING_NUM','Tracking Num','Tracking Num','','','','','25','N','Y','','','','','','','MR020532','N','N','','XXWC_EIS_ORD_DAYS_FULFILL_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Days to Fulfill Report - WC',660,'LINE_STATUS','Line Status','Line Status','','','default','','4','N','Y','','','','','','','MR020532','N','N','','XXWC_EIS_ORD_DAYS_FULFILL_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Days to Fulfill Report - WC',660,'ACCEPTANCE_DATE','Acceptance Date','Acceptance Date','','','','','13','','Y','','','','','','','MR020532','N','N','','XXWC_EIS_ORD_DAYS_FULFILL_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Days to Fulfill Report - WC',660,'MET_REQUEST_DATE','Met Request Date','Met Request Date','','','','','2','','Y','','','','','','','MR020532','N','N','','XXWC_EIS_ORD_DAYS_FULFILL_V','','','','US','');
--Inserting Report Parameters - Days to Fulfill Report - WC
xxeis.eis_rsc_ins.rp( 'Days to Fulfill Report - WC',660,'Status','Status','STATUS','IN','OM Order Header Status','','VARCHAR2','N','Y','3','Y','Y','CONSTANT','MR020532','Y','N','','','','XXWC_EIS_ORD_DAYS_FULFILL_V','','','US','');
xxeis.eis_rsc_ins.rp( 'Days to Fulfill Report - WC',660,'Created By','Created By','CREATED_BY','IN','Created By Lov','','VARCHAR2','N','Y','9','Y','Y','CONSTANT','MR020532','Y','N','','','','XXWC_EIS_ORD_DAYS_FULFILL_V','','','US','');
xxeis.eis_rsc_ins.rp( 'Days to Fulfill Report - WC',660,'Customer Number','Customer Number','CUSTOMER_NUMBER','IN','OM CUSTOMER NUMBER','','VARCHAR2','N','Y','5','Y','Y','CONSTANT','MR020532','Y','N','','','','XXWC_EIS_ORD_DAYS_FULFILL_V','','','US','');
xxeis.eis_rsc_ins.rp( 'Days to Fulfill Report - WC',660,'Order Channel','Order Channel','ORDER_CHANNEL','IN','EIS XXWC Order Channel','','VARCHAR2','N','Y','7','Y','Y','CONSTANT','MR020532','Y','N','','','','XXWC_EIS_ORD_DAYS_FULFILL_V','','','US','');
xxeis.eis_rsc_ins.rp( 'Days to Fulfill Report - WC',660,'SalesRep','SalesRep','SALESREP','IN','EIS XXWC Sales Rep','','VARCHAR2','N','Y','10','Y','Y','CONSTANT','MR020532','Y','N','','','','XXWC_EIS_ORD_DAYS_FULFILL_V','','','US','');
xxeis.eis_rsc_ins.rp( 'Days to Fulfill Report - WC',660,'As Of Date','As Of Date','','<=','','','DATE','N','Y','11','','N','CURRENT_DATE','MR020532','N','N','','Start Date','','XXWC_EIS_ORD_DAYS_FULFILL_V','','','US','');
xxeis.eis_rsc_ins.rp( 'Days to Fulfill Report - WC',660,'Booked Start Date From','Booked Start Date From','BOOKED_DATE','>=','','','DATE','Y','Y','1','','Y','CONSTANT','MR020532','Y','N','','Start Date','','XXWC_EIS_ORD_DAYS_FULFILL_V','','','US','');
xxeis.eis_rsc_ins.rp( 'Days to Fulfill Report - WC',660,'Booked Start Date TO','Booked Start Date TO','BOOKED_DATE','<=','','','DATE','Y','Y','2','','Y','CONSTANT','MR020532','Y','N','','End Date','','XXWC_EIS_ORD_DAYS_FULFILL_V','','','US','');
xxeis.eis_rsc_ins.rp( 'Days to Fulfill Report - WC',660,'Ship From Org ID','Ship From Org ID','SHIP_ORG','IN','OM Warehouse All','','NUMERIC','N','Y','8','Y','Y','CONSTANT','MR020532','Y','N','','','','XXWC_EIS_ORD_DAYS_FULFILL_V','','','US','');
xxeis.eis_rsc_ins.rp( 'Days to Fulfill Report - WC',660,'Party Number','Party Number','PARTY_NUMBER','IN','EIS XXWC PARTY ID','','NUMERIC','N','Y','4','Y','Y','CONSTANT','MR020532','Y','N','','','','XXWC_EIS_ORD_DAYS_FULFILL_V','','','US','');
xxeis.eis_rsc_ins.rp( 'Days to Fulfill Report - WC',660,'ItemList','Item List','','IN','XXWC Item List','','VARCHAR2','N','Y','6','','N','CONSTANT','MR020532','Y','N','','','','XXWC_EIS_ORD_DAYS_FULFILL_V','','','US','');
--Inserting Dependent Parameters - Days to Fulfill Report - WC
--Inserting Report Conditions - Days to Fulfill Report - WC
xxeis.eis_rsc_ins.rcnh( 'Days to Fulfill Report - WC',660,'BOOKED_DATE >= :Booked Start Date From ','SIMPLE','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','BOOKED_DATE','','Booked Start Date From','','','','','XXWC_EIS_ORD_DAYS_FULFILL_V','','','','','','GREATER_THAN_EQUALS','Y','Y','','','','','1',660,'Days to Fulfill Report - WC','BOOKED_DATE >= :Booked Start Date From ');
xxeis.eis_rsc_ins.rcnh( 'Days to Fulfill Report - WC',660,'BOOKED_DATE <= :Booked Start Date TO ','SIMPLE','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','BOOKED_DATE','','Booked Start Date TO','','','','','XXWC_EIS_ORD_DAYS_FULFILL_V','','','','','','LESS_THAN_EQUALS','Y','Y','','','','','1',660,'Days to Fulfill Report - WC','BOOKED_DATE <= :Booked Start Date TO ');
xxeis.eis_rsc_ins.rcnh( 'Days to Fulfill Report - WC',660,'CREATED_BY IN :Created By ','SIMPLE','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','CREATED_BY','','Created By','','','','','XXWC_EIS_ORD_DAYS_FULFILL_V','','','','','','IN','Y','Y','','','','','1',660,'Days to Fulfill Report - WC','CREATED_BY IN :Created By ');
xxeis.eis_rsc_ins.rcnh( 'Days to Fulfill Report - WC',660,'CUSTOMER_NUMBER IN :Customer Number ','SIMPLE','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','CUSTOMER_NUMBER','','Customer Number','','','','','XXWC_EIS_ORD_DAYS_FULFILL_V','','','','','','IN','Y','Y','','','','','1',660,'Days to Fulfill Report - WC','CUSTOMER_NUMBER IN :Customer Number ');
xxeis.eis_rsc_ins.rcnh( 'Days to Fulfill Report - WC',660,'ORDER_CHANNEL IN :Order Channel ','SIMPLE','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','ORDER_CHANNEL','','Order Channel','','','','','XXWC_EIS_ORD_DAYS_FULFILL_V','','','','','','IN','Y','Y','','','','','1',660,'Days to Fulfill Report - WC','ORDER_CHANNEL IN :Order Channel ');
xxeis.eis_rsc_ins.rcnh( 'Days to Fulfill Report - WC',660,'PARTY_NUMBER IN :Party Number ','SIMPLE','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','PARTY_NUMBER','','Party Number','','','','','XXWC_EIS_ORD_DAYS_FULFILL_V','','','','','','IN','Y','Y','','','','','1',660,'Days to Fulfill Report - WC','PARTY_NUMBER IN :Party Number ');
xxeis.eis_rsc_ins.rcnh( 'Days to Fulfill Report - WC',660,'SALESREP IN :SalesRep ','SIMPLE','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','SALESREP','','SalesRep','','','','','XXWC_EIS_ORD_DAYS_FULFILL_V','','','','','','IN','Y','Y','','','','','1',660,'Days to Fulfill Report - WC','SALESREP IN :SalesRep ');
xxeis.eis_rsc_ins.rcnh( 'Days to Fulfill Report - WC',660,'SHIP_ORG IN :Ship From Org ID ','SIMPLE','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','SHIP_ORG','','Ship From Org ID','','','','','XXWC_EIS_ORD_DAYS_FULFILL_V','','','','','','IN','Y','Y','','','','','1',660,'Days to Fulfill Report - WC','SHIP_ORG IN :Ship From Org ID ');
xxeis.eis_rsc_ins.rcnh( 'Days to Fulfill Report - WC',660,'STATUS IN :Status ','SIMPLE','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','STATUS','','Status','','','','','XXWC_EIS_ORD_DAYS_FULFILL_V','','','','','','IN','Y','Y','','','','','1',660,'Days to Fulfill Report - WC','STATUS IN :Status ');
xxeis.eis_rsc_ins.rcnh( 'Days to Fulfill Report - WC',660,'Free Text ','FREE_TEXT','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','','','','','','','','','','','','','','','Y','','','','','AND ( :ItemList is NULL  OR
          EXISTS  (SELECT 1
  from xxeis.eis_xxwc_param_parse_list x
  where x.list_name     =:ItemList
  and x.list_type     =''Item''
 and x.process_id    = :system.process_id
and  EODFV.ORDERED_ITEM =regexp_replace(x.list_value,''[^[a-z,A-Z,0-9]]*'')))','1',660,'Days to Fulfill Report - WC','Free Text ');
--Inserting Report Sorts - Days to Fulfill Report - WC
xxeis.eis_rsc_ins.rs( 'Days to Fulfill Report - WC',660,'','DESC','MR020532','1','2');
xxeis.eis_rsc_ins.rs( 'Days to Fulfill Report - WC',660,'','DESC','MR020532','2','1');
--Inserting Report Triggers - Days to Fulfill Report - WC
xxeis.eis_rsc_ins.rt( 'Days to Fulfill Report - WC',660,'BEGIN
xxeis.EIS_RS_XXWC_COM_UTIL_PKG.parse_cleanup_table(P_PROCESS_ID    => :SYSTEM.PROCESS_ID);
END;','A','Y','MR020532','AQ');
xxeis.eis_rsc_ins.rt( 'Days to Fulfill Report - WC',660,'Declare
L_ITEM_LIST     varchar2(240);
begin
L_ITEM_LIST    :=:ItemList;
if L_ITEM_LIST is not null then
XXEIS.EIS_RS_XXWC_COM_UTIL_PKG.PARSE_PARAM_LIST(P_PROCESS_ID    => :SYSTEM.PROCESS_ID,
                        P_LIST_NAME    => L_ITEM_LIST,
                        P_LIST_TYPE    => ''Item''
                        );
xxeis.EIS_RS_XXWC_COM_UTIL_PKG.G_ITEM_FLAG:=''Y'';
else
xxeis.EIS_RS_XXWC_COM_UTIL_PKG.G_ITEM_FLAG:=''N'';
end if;
xxeis.eis_rs_xxwc_com_util_pkg.set_date_from(:As Of Date);
end;','B','Y','MR020532','AQ');
--inserting report templates - Days to Fulfill Report - WC
--Inserting Report Portals - Days to Fulfill Report - WC
--inserting report dashboards - Days to Fulfill Report - WC
--Exporting report views table information -  
xxeis.eis_rsc_ins.rviews( 'Days to Fulfill Report - WC','660','XXWC_EIS_ORD_DAYS_FULFILL_V','XXWC_EIS_ORD_DAYS_FULFILL_V','N','');
--inserting report security - Days to Fulfill Report - WC
xxeis.eis_rsc_ins.rsec( 'Days to Fulfill Report - WC','660','','XXWC_ORDER_MGMT_READ_ONLY_COST',660,'MR020532','','','');
xxeis.eis_rsc_ins.rsec( 'Days to Fulfill Report - WC','660','','XXWC_ORDER_MGMT_READ_ONLY',660,'MR020532','','','');
xxeis.eis_rsc_ins.rsec( 'Days to Fulfill Report - WC','660','','XXWC_ORDER_MGMT_PRICING_SUPER',660,'MR020532','','','');
xxeis.eis_rsc_ins.rsec( 'Days to Fulfill Report - WC','660','','XXWC_ORDER_MGMT_PRICING_STD',660,'MR020532','','','');
xxeis.eis_rsc_ins.rsec( 'Days to Fulfill Report - WC','660','','XXWC_ORDER_MGMT_PRICING_LTD',660,'MR020532','','','');
xxeis.eis_rsc_ins.rsec( 'Days to Fulfill Report - WC','660','','XXWC_ORDER_MGMT_PRICING_FULL',660,'MR020532','','','');
xxeis.eis_rsc_ins.rsec( 'Days to Fulfill Report - WC','20005','','XXWC_VIEW_ALL_EIS_REPORTS',660,'MR020532','','','');
--Inserting Report Pivots - Days to Fulfill Report - WC
--Inserting Report   Version details- Days to Fulfill Report - WC
xxeis.eis_rsc_ins.rv( 'Days to Fulfill Report - WC','','Days to Fulfill Report - WC','SA059956');
ELSE
RAISE_APPLICATION_ERROR (-20001,'Module does not exist. Import/Create the module with id 660');
END IF;
END;
/
