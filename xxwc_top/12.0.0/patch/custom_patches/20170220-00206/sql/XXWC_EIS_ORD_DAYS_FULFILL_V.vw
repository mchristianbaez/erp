------------------------------------------------------------------------------------
/******************************************************************************
   NAME       :  xxeis.xxwc_eis_ord_days_fulfill_v
   REVISIONS:
	Ver         Date            Author             Description
   ---------  ----------    ---------------  ------------------------------------
    1.0       	nil      		 nil	            Initial Version 
	1.1		  28-Mar-2017       Siva				TMS#20170220-00206 
******************************************************************************/
DROP VIEW XXEIS.XXWC_EIS_ORD_DAYS_FULFILL_V;  

CREATE OR REPLACE VIEW xxeis.xxwc_eis_ord_days_fulfill_v
(
   order_number
  ,day_to_fulfill
  ,status
  ,line_status
  ,account_description
  ,customer_number
  ,header_id
  ,cust_po_number
  ,order_channel
  ,booked_date
  ,schedule_ship_date
  ,request_date
  ,closed_date
  ,invoice_number
  ,ship_org
  ,line_number
  ,order_qty
  ,ordered_item
  ,item_description
  ,avg_cost
  ,unit_sell_price
  ,extended_price
  ,gm
  ,created_by
  ,salesrep
  ,tracking_num
  ,party_id
  ,party_number
  ,acceptance_date  --added for version 1.1
  ,met_request_date --added for version 1.1
)
AS
   SELECT ooh.order_number order_number
         ,DECODE (ooh.flow_status_code
                 ,'CLOSED', (TO_DATE (ooh.last_update_date) - TO_DATE (ooh.ordered_date))
                 , (TO_DATE (SYSDATE) - TO_DATE (ooh.booked_date)))
             day_to_fulfill
         ,ooh.flow_status_code status
         ,ool.flow_status_code line_status
         ,hca.account_name account_description
         ,hca.account_number customer_number
         ,ooh.header_id header_id
         ,ooh.cust_po_number cust_po_number
         ,ooh.attribute2 order_channel
         ,TRUNC (ooh.booked_date) booked_date
         ,ool.schedule_ship_date schedule_ship_date
         ,ooh.request_date request_date
         ,CASE WHEN ool.flow_status_code IN ('CLOSED', 'CANCELLED') THEN ct.trx_date ELSE NULL END
             closed_date
         ,CASE WHEN ool.flow_status_code IN ('CLOSED') THEN ct.trx_number ELSE NULL END invoice_number
         ,ood.organization_code ship_org
         , (ool.line_number || '.' || ool.shipment_number) line_number
         ,ool.ordered_quantity order_qty
         ,ool.ordered_item
         ,msi.description item_description
         , (CASE
               WHEN ool.flow_status_code IN ('CLOSED'
                                            ,'INVOICED'
                                            ,'INVOICED_PARTIAL'
                                            ,'INVOICE_DELIVERY'
                                            ,'INVOICE_HOLD'
                                            ,'INVOICE_INCOMPLETE')
               THEN
                  apps.xxwc_mv_routines_pkg.get_order_line_cost (ool.line_id)
               WHEN ott.name IN ('BILL ONLY', 'CREDIT ONLY')
               THEN
                  0
               ELSE
                  NVL (apps.cst_cost_api.get_item_cost (1, msi.inventory_item_id, msi.organization_id), 0)
            END)
             avg_cost
         ,ool.unit_selling_price unit_sell_price
         , (ool.ordered_quantity * ool.unit_selling_price) extended_price
         ,NULL gm
         ,fu.description created_by
         ,papf.full_name salesrep
         , (SELECT a.tracking_num
              FROM xxwc.xxwc_b2b_so_delivery_info_tbl a,                               --oe_order_lines_all c,
                                                        xxwc.xxwc_wsh_shipping_stg b
             WHERE /*c.header_id = ooh.header_id
               AND c.line_id = ool.line_id
               AND c.line_id = b.line_id*/
                   b.line_id = ool.line_id AND a.delivery_id = b.delivery_id AND ROWNUM = 1)
             tracking_num
         ,hp.party_id party_id
         ,hp.party_number party_number
    	 ,ool.revrec_signature_date acceptance_date  --added for version 1.1
    	 ,(CASE
      		WHEN TRUNC(ooh.request_date)< TRUNC(ool.revrec_signature_date)
      		THEN xxeis.eis_xxwc_inv_util_pkg.get_working_days(ooh.request_date,ool.revrec_signature_date)
			WHEN TRUNC(OOH.REQUEST_DATE) > TRUNC(OOL.REVREC_SIGNATURE_DATE)
            THEN -1*(xxeis.eis_xxwc_inv_util_pkg.get_working_days(ool.revrec_signature_date,ooh.request_date))
      		ELSE 0
    	   END)met_request_date  --added for version 1.1
     FROM oe_order_headers_all ooh
         ,oe_order_lines_all ool
         ,oe_transaction_types_tl ott
         ,hz_parties hp
         ,hz_cust_accounts hca
         ,jtf_rs_salesreps jrs
         ,per_all_people_f papf
         ,mtl_system_items_b msi
         ,ra_customer_trx_all ct
         ,ra_customer_trx_lines_all ctl 
         ,org_organization_definitions ood
         ,fnd_user fu
    WHERE     1 = 1
          AND ooh.header_id = ool.header_id
          AND ooh.order_type_id = ott.transaction_type_id
		  and ott.LANGUAGE(+) = userenv('LANG') --added for version 1.1
          AND ott.name IN ('STANDARD ORDER')
          AND ooh.sold_to_org_id = hca.cust_account_id
          AND hca.party_id = hp.party_id
          AND jrs.salesrep_id = ool.salesrep_id
		  and jrs.org_id      = ool.org_id   --added for version 1.1
          AND jrs.person_id = papf.person_id
		  and trunc(sysdate) between papf.effective_start_date (+) and papf.effective_end_date (+)--added for version 1.1
          AND msi.segment1 = ool.ordered_item
          AND msi.organization_id = ool.ship_from_org_id
		  -- AND TO_CHAR (ooh.order_number) = ct.interface_header_attribute1(+) --commented for version 1.1
          AND TO_CHAR (ool.line_id) = ctl.interface_line_attribute6(+)	 
          AND ctl.interface_line_context(+)= 'ORDER ENTRY'  --added for version 1.1
          AND ctl.customer_trx_id = ct.customer_trx_id(+)   --added for version 1.1  
          AND ood.organization_id = ool.ship_from_org_id
          AND fu.user_id = ooh.created_by;
