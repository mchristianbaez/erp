/* Ver        Date         Author           Description
   ---------  -----------  ---------------  ------------------------------------
   1.0        14-Jan-2016  Kishorebabu V    TMS#20160114-00018 ata fix script to delete the records from the custom table
                                            Initial Version   */

SET SERVEROUTPUT ON SIZE 100000;
SET VERIFY OFF;

DELETE FROM xxwc.xxwc_pricing_segment_tbl
WHERE account_number IN ('101996000',
'101829000',
'102939000',
'113871000',
'115949000',
'10000063535',
'104566000',
'153283000',
'10000008434');

COMMIT;
/