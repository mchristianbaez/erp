/*
   Ticket#                               Date         Author             Notes
   -------------------------------------------------------------------------------------------------------------------------------------------------------
   TMS 20170412-00117 / ESMS 554216      02/17/2017   Balaguru Seshadri  Concur process related tables
*/
DROP INDEX XXCUS.XXCUS_CONCUR_WW_BR_PROJ_U2;
--
CREATE UNIQUE INDEX XXCUS.XXCUS_CONCUR_WW_BR_PROJ_U2 ON XXCUS.XXCUS_CONCUR_WW_BR_PROJ_REF
(ID)
NOLOGGING
TABLESPACE XXCUS_DATA
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
            FLASH_CACHE      DEFAULT
            CELL_FLASH_CACHE DEFAULT
           )
NOPARALLEL
;
--
