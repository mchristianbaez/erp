/*
TMS: 20160304-00176 / ESMS 316840
AUTHOR: BALAGURU SESHADRI
DATE: 07/07/2016
SCOPE: NEW WC REBATES RATE OF RETURN VIEW
*/
CREATE OR REPLACE VIEW APPS.XXCUS_OZF_WC_REBATE_ROR_V AS
SELECT AGREEMENT_YEAR CURRENT_AGREEMENT_YEAR,
       MVID,
       VENDOR,
       PLAN_ID,
       OFFER_NAME,
       PURCHASES,
       TOTAL_ACCRUALS,
       SYSTEM_ACCRUALS,
       MANUAL_ADJUSTMENTS,
       ROUND (
            TOTAL_ACCRUALS
          / CASE WHEN PURCHASES = 0 THEN 1 ELSE PURCHASES END
          * 100,
          2)
          TOTAL_ROR,
       ROUND (
            SYSTEM_ACCRUALS
          / CASE WHEN PURCHASES = 0 THEN 1 ELSE PURCHASES END
          * 100,
          2)
          SYSTEM_ROR,
       ROUND (
            MANUAL_ADJUSTMENTS
          / CASE WHEN PURCHASES = 0 THEN 1 ELSE PURCHASES END
          * 100,
          2)
          MANUAL_ROR
  FROM (SELECT AGREEMENT_YEAR,
               MVID,
               VENDOR,
               PLAN_ID,
               OFFER_NAME,
               (
                    SELECT SUM(SELLING_PRICE*QUANTITY) 
                    FROM   XXCUS.XXCUS_OZF_XLA_ACCRUALS_B XLA
                    WHERE 1 =1
                    AND XLA.QP_LIST_HEADER_ID =PLAN_ID  --FROM MAIN QRY                       
                    AND CALENDAR_YEAR =AGREEMENT_YEAR --FROM MAIN QRY
                    AND XLA.BILL_TO_PARTY_ID =2060 --ALWAYS 2060 FOR WC
                    AND XLA.OFU_MVID =MVID--FROM MAIN QRY
                    AND XLA.COOP_YES_NO ='N'           
                    ) PURCHASES,
               TOTAL_ACCRUALS,
               SYSTEM_ACCRUALS,
               MANUAL_ADJUSTMENTS
          FROM (  SELECT B.AGREEMENT_YEAR,
                         B.MVID,
                         B.LOB,
                         C.PARTY_NAME VENDOR,
                         B.PLAN_ID,
                         QLHV.DESCRIPTION OFFER_NAME,
                         SUM (ACCRUAL_AMOUNT) TOTAL_ACCRUALS,
                         SUM (
                            CASE
                               WHEN B.OFU_ATTRIBUTE10 IS NULL
                               THEN
                                  B.ACCRUAL_AMOUNT
                               ELSE
                                  0
                            END)
                            SYSTEM_ACCRUALS,
                         SUM (
                            CASE
                               WHEN B.OFU_ATTRIBUTE10 IS NOT NULL
                               THEN
                                  B.ACCRUAL_AMOUNT
                               ELSE
                                  0
                            END)
                            MANUAL_ADJUSTMENTS
                    FROM XXCUS.XXCUS_YTD_INCOME_B B,
                         XXCUS.XXCUS_REBATE_CUSTOMERS C,
                         OZF.OZF_OFFERS OO,
                         APPS.QP_LIST_HEADERS_VL QLHV
                   WHERE     1 = 1
                         AND B.OFU_CUST_ACCOUNT_ID = C.CUSTOMER_ID
                         AND B.PLAN_ID = OO.QP_LIST_HEADER_ID
                         AND B.LOB_ID = 2060
                         AND B.REBATE_TYPE = 'REBATE'
                         AND B.AGREEMENT_YEAR = TO_CHAR (SYSDATE, 'YYYY') -- ALWAYS PULL FOR CURRENT YEAR AGREEMENTS ONLY
                         AND C.CUSTOMER_ATTRIBUTE1 = 'HDS_MVID'
                         AND OO.STATUS_CODE ='ACTIVE'
                         AND OO.QP_LIST_HEADER_ID  = QLHV.LIST_HEADER_ID 
                GROUP BY B.AGREEMENT_YEAR,
                         B.MVID,
                         C.PARTY_NAME,
                         B.PLAN_ID,
                         B.LOB,
                         QLHV.DESCRIPTION) D);
--
COMMENT ON TABLE XXCUS.XXCUS_OZF_WC_REBATE_ROR_V IS 'TMS 20160304-00176 / ESMS 316840';
--						 