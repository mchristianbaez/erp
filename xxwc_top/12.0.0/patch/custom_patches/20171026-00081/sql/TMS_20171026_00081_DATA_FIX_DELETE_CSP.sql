/*******************************************************************
  script name: TMS_20171026_00081_DATA_FIX_DELETE_CSP.sql
  purpose: To delete specified csp after taking their backup
  revision history:
  Version   Date          Aurthor           Description
  ----------------------------------------------------------------
  1.0       27-OCT-2017   Niraj K Ranjan    TMS#20171026-00081   CSP - Approved in Oracle - but not working
*******************************************************************/
SET serveroutput on size 1000000;
CREATE TABLE xxwc.xxwc_om_contract_prc_hdr_bkp
   AS (SELECT * FROM xxwc_om_contract_pricing_hdr 
       WHERE agreement_id IN (19144 , 24109 , 271535 , 330508 , 333522 , 343641));
       
CREATE TABLE xxwc.xxwc_om_contract_prc_lines_bkp
   AS (SELECT * FROM xxwc_om_contract_pricing_lines 
       WHERE agreement_id IN (19144 , 24109 , 271535 , 330508 , 333522 , 343641));    
       
CREATE TABLE xxwc.xxwc_om_csp_notifications_bkp
   AS (SELECT * FROM xxwc_om_csp_notifications_tbl 
       WHERE agreement_id IN (19144 , 24109 , 271535 , 330508 , 333522 , 343641));
BEGIN
   dbms_output.put_line('TMS_20171026-00081 : '||'Start script');    
       
   DELETE FROM xxwc_om_csp_notifications_tbl
   WHERE agreement_id IN (19144 , 24109 , 271535 , 330508 , 333522 , 343641);
   
   dbms_output.put_line('TMS_20171026-00081 : '||'Total records deleted from table xxwc_om_csp_notifications_tbl : '||SQL%ROWCOUNT);
   
   DELETE FROM xxwc_om_contract_pricing_lines 
   WHERE agreement_id IN (19144 , 24109 , 271535 , 330508 , 333522 , 343641);
   
   dbms_output.put_line('TMS_20171026-00081 : '||'Total records deleted from table xxwc_om_contract_pricing_lines : '||SQL%ROWCOUNT);
   
   DELETE FROM xxwc_om_contract_pricing_hdr 
   WHERE agreement_id IN (19144 , 24109 , 271535 , 330508 , 333522 , 343641);
   
   dbms_output.put_line('TMS_20171026-00081 : '||'Total records deleted from table xxwc_om_contract_pricing_hdr : '||SQL%ROWCOUNT);
   
   dbms_output.put_line('TMS_20171026-00081 : '||'End script');
   COMMIT;
EXCEPTION
   WHEN OTHERS THEN
      dbms_output.put_line('TMS_20171026-00081 : '||'ERROR - '||SQLERRM);
	  ROLLBACK;
END;
/