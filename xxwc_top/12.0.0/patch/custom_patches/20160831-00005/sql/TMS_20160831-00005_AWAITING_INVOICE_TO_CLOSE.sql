/*************************************************************************
  $Header TMS_20160831-00005_AWAITING_INVOICE_TO_CLOSE.sql $
  Module Name: TMS_20160831-00005  Data Fix script for I675908

  PURPOSE: Data fix script for I675908--No permanent fix in process (But patch available per oracle)

  REVISIONS:
  Ver        Date         Author                Description
  ---------  -----------  ------------------    --------------------------
  1.0        02-AUG-2016  Raghav Velichetti         TMS#20160831-00005 

**************************************************************************/ 
SET SERVEROUTPUT ON SIZE 1000000;
DECLARE
BEGIN
   DBMS_OUTPUT.put_line ('TMS: 20160831-00005    , Before Update');

update apps.oe_order_lines_all
set FLOW_STATUS_CODE='CANCELLED',
CANCELLED_FLAG='Y'
where line_id = 76681509
and header_id= 46873664;
   DBMS_OUTPUT.put_line (
         'TMS: 20160831-00005  Sales order lines updated (Expected:1): '
      || SQL%ROWCOUNT);

   COMMIT;

   DBMS_OUTPUT.put_line ('TMS: 20160831-00005    , End Update');
EXCEPTION
   WHEN OTHERS
   THEN
      ROLLBACK;
      DBMS_OUTPUT.put_line ('TMS: 20160831-00005 , Errors : ' || SQLERRM);
END;
/