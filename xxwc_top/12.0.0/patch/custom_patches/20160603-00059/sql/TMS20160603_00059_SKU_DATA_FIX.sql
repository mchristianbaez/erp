/****************************************************************************************************************************   $Header TMS20160603_00059_SKU_DATA_FIX.sql $
  Module Name TMS20160603_00059 - Needs to be delete null record from XXWC_UMT_SKU_BRANCH_DISP_TBL

  PURPOSE Data fix to remove null record from custom table. 

  REVISIONS
  Ver        Date         Author                Description
  ---------  -----------  ------------------    --------------------------
  1.0        03-Jun-2016  P.Vamshidhar          TMS20160603_00059 - Needs to be delete null record from XXWC_UMT_SKU_BRANCH_DISP_TBL

****************************************************************************************************************************/
SET SERVEROUTPUT ON SIZE 100000

BEGIN
   DBMS_OUTPUT.put_line ('Before Delete');

   DELETE FROM XXWC.XXWC_UMT_SKU_BRANCH_DISP_TBL
         WHERE ID = 743965;

   DBMS_OUTPUT.PUT_LINE ('Number of rows deleted ' || SQL%ROWCOUNT);


   DBMS_OUTPUT.put_line ('After Delete');
END;
/