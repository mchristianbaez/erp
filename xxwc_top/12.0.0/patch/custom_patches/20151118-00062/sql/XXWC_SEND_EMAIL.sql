--TMS#20151113-00038 To test the email server
BEGIN
              apps.xxcus_misc_pkg.html_email (p_to       => 'WC-ITFINANCESUPPORT-U1@HDSUPPLY.COM'
                                      ,p_from            => 'donotreply@hdsupply.com'             
                                      ,p_text            => 'test pls delete'
                                      ,p_subject         => 'Testing'
                                      ,                                                 
                                       p_html            => 'test pls delete'
                                      ,p_smtp_hostname   => 'mailoutrelay.hdsupply.net'
                                      ,p_smtp_portnum    => '25');
END;
/