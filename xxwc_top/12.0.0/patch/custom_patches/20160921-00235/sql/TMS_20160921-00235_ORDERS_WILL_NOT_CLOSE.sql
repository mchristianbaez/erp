/*************************************************************************
  $Header TMS_20160921-00235_ORDERS_WILL_NOT_CLOSE.sql $
  Module Name: TMS_20160921-00235  Data Fix script 

  PURPOSE: Data fix script for Stuck awaiting receipt

  REVISIONS:
  Ver        Date         Author                Description
  ---------  -----------  ------------------    --------------------------
  1.0        03-OCT-2016  Pattabhi Avula        TMS#20160921-00235

**************************************************************************/ 
SET SERVEROUTPUT ON SIZE 1000000;
DECLARE
BEGIN
   DBMS_OUTPUT.put_line ('TMS: 20160921-00235    , Before Update');

update apps.oe_order_lines_all
set FLOW_STATUS_CODE='CANCELLED',
CANCELLED_FLAG='Y'
where line_id = 78372216
and header_id= 47938682;

   DBMS_OUTPUT.put_line (
         'TMS: 20160921-00235  Sales order lines updated (Expected:1): '
      || SQL%ROWCOUNT);

   COMMIT;

   DBMS_OUTPUT.put_line ('TMS: 20160921-00235    , End Update');
EXCEPTION
   WHEN OTHERS
   THEN
      ROLLBACK;
      DBMS_OUTPUT.put_line ('TMS: 20160921-00235 , Errors : ' || SQLERRM);
END;
/