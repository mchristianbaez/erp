/******************************************************************************
  $Header TMS_20170110-00315_Order_stuck_in_Close.sql $
  Module Name:Data Fix script for 20170110-00315

  PURPOSE: Data fix script for 20170110-00315 Order stuck in Close

  REVISIONS:
  Ver        Date         Author                Description
  ---------  -----------  ------------------    -------------------------------
  1.0        27-APR-2017  Krishna Kumar        TMS#20170110-00315

*******************************************************************************/
SET SERVEROUTPUT ON SIZE 1000000

BEGIN
   DBMS_OUTPUT.put_line ('Before Update');

   UPDATE apps.oe_order_headers_all
      SET booked_flag='Y',
          flow_status_Code='CLOSED'
    WHERE header_id=22381157;


   DBMS_OUTPUT.put_line ('Records update-' || SQL%ROWCOUNT);
   COMMIT;
EXCEPTION
   WHEN OTHERS
   THEN
      DBMS_OUTPUT.put_line ('Unable to update record ' || SQLERRM);
END;
/