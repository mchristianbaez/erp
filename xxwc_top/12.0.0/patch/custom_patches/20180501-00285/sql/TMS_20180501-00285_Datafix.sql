/***********************************************************************************************************************************************
   NAME:     TMS_20180501-00285_Datafix.sql
   PURPOSE:  Data fix

   REVISIONS:
   Ver        Date        Author           Description
   ---------  ----------  ---------------  ----------------------------------------------------------------------------------------------------
   1.0        05/01/2018  Rakesh Patel     TMS#20180501-00285 - purge orphan records
************************************************************************************************************************************************/
SET SERVEROUTPUT ON SIZE 1000000

BEGIN

   DBMS_OUTPUT.put_line ('Before deleting records');  
     
   DELETE xxwc.xxwc_om_quote_lines wc 
   WHERE NOT EXISTS (SELECT 1 
                       FROM xxwc.xxwc_om_quote_headers hc 
                       WHERE hc.quote_number=wc.quote_number
                  );
	   
   DBMS_OUTPUT.put_line ('Records Deleted from xxwc_om_quote_lines - ' || SQL%ROWCOUNT);

   COMMIT;

   EXCEPTION
   WHEN OTHERS
   THEN
      DBMS_OUTPUT.put_line ('Unable to delete record ' || SQLERRM);
	  ROLLBACK;
END;
/