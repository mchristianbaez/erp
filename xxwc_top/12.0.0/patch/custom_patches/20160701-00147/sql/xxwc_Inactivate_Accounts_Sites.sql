/*************************************************************************
      Script Name: xxwc_Inactivate_Accounts_Sites.sql

      PURPOSE:   To inactivate accounts and sites

      REVISIONS:
      Ver        Date        Author                     Description
      ---------  ----------  ---------------         -------------------------
      1.0        20/10/2017  Niraj K Ranjan          TMS#20160701-00147   Credit - one time data script to inactivate accountsites with Prism data only
****************************************************************************/
CREATE TABLE XXWC.XXWC_INACTIVE_SITES_TBL
AS
(SELECT *
  FROM (SELECT PARTY_SITE_ID,
               PARTY_SITE_NAME,
               ACCOUNT_NUMBER,
               ACCOUNT_NAME,
               CUST_ACCOUNT_ID,
               PARTY_SITE_NUMBER,
               CREATION_DATE party_site_creation_date,
               SITE_USE_ID,
               LOCATION,
               COLLECTOR,
               PROFILE_CLASS,
               OBJECT_VERSION_NUMBER,
               SITE_CLASSIFICATION,
               SITE_USE_PRIMARY_FLAG,
               NAME,
               (SELECT NVL (SUM (AMOUNT_DUE_REMAINING), 0)
                  FROM apps.ar_payment_schedules_all
                 WHERE customer_site_use_id = v.site_use_id)
                  payment_activity,
               (SELECT NVL (COUNT (1), 0)
                  FROM apps.oe_order_headers_all ooh
                 WHERE    ooh.ship_to_org_id = v.site_use_id
                       OR ooh.invoice_to_org_id = v.site_use_id)
                  order_activity,
               (SELECT MAX (creation_date)
                  FROM apps.ar_payment_schedules_all
                 WHERE customer_site_use_id = v.site_use_id)
                  last_payment_date,
               (SELECT MAX (a.trx_date)
                  FROM apps.ra_customer_trx_all a,
                       apps.ar_payment_schedules_all b,
                       apps.ar_receivable_applications c
                 WHERE     1 = 1
                       AND a.CUSTOMER_TRX_ID = b.CUSTOMER_TRX_ID
                       AND b.payment_schedule_id = c.payment_schedule_id
                       AND b.customer_site_use_id = v.site_use_id)
                  last_trx_date,
               v.creation_date site_creation_date
          FROM ((SELECT /*+leading(hca)*/
                       acct_site.party_site_id,
                        cust.account_number,
                        cust.account_name,
                        cust.cust_account_id,
                        party_site.creation_date,
                        party_site.party_site_number,
                        site_uses.site_use_id,
                        site_uses.location,
                        coll.name collector,
                        pc.name profile_class,
                        NVL (party_site.object_version_number, 1)
                           object_version_number,
                        site_uses.attribute1 site_classification,
                           site_uses.SITE_USE_CODE
                        || '-'
                        || site_uses.primary_flag
                           site_use_primary_flag,
                        pc.name,
                        party_site.party_site_name
                   FROM AR.hz_cust_accounts cust,
                        AR.hz_parties party,
                        AR.hz_party_sites party_site,
                        apps.hz_cust_acct_sites_all acct_site,
                        apps.hz_cust_site_uses_all site_uses,
                        AR.hz_locations loc,
                        AR.hz_customer_profiles prof,
                        AR.hz_cust_profile_classes pc,
                        AR.ar_collectors coll
                  WHERE     cust.party_id = party.party_id
                        AND party.party_id = party_site.party_id
                        AND cust.cust_account_id = acct_site.cust_account_id
                        AND acct_site.party_site_id =
                               party_site.party_site_id
                        AND acct_site.cust_acct_site_id =
                               site_uses.cust_acct_site_id
                        AND loc.location_id = party_site.location_id
                        AND prof.site_use_id = site_uses.site_use_id
                        AND prof.profile_class_id = pc.profile_class_id
                        AND prof.collector_id = coll.collector_id(+)
                        AND site_uses.org_id = acct_site.org_id
                        AND site_uses.org_id = 162
                        AND NVL (acct_site.status, 'A') = 'A'
                        AND NVL (site_uses.status, 'A') = 'A'
                        AND NVL (cust.status, 'A') = 'A'
                        AND pc.name NOT IN ('DEFAULT',
                                            'Intercompany Customers',
                                            'WC Branches')
                        AND site_uses.primary_flag = 'N')) v
         WHERE    1 = 1
               OR (v.cust_account_id, v.site_use_id) IN (SELECT b.customer_id,
                                                                b.customer_site_use_id
                                                           FROM apps.ra_customer_trx_all a,
                                                                apps.ar_payment_schedules_all b,
                                                                apps.ar_receivable_applications_all c
                                                          WHERE     1 = 1
                                                                AND a.CUSTOMER_TRX_ID =
                                                                       b.CUSTOMER_TRX_ID
                                                                AND b.payment_schedule_id =
                                                                       c.payment_schedule_id
                                                                AND EXTRACT (
                                                                       YEAR FROM (b.creation_date)) <
                                                                       EXTRACT (
                                                                          YEAR FROM (  SYSDATE
                                                                                     -   365
                                                                                       * 3))
                                                                AND (    EXTRACT (
                                                                            YEAR FROM (c.creation_date)) <
                                                                            EXTRACT (
                                                                               YEAR FROM (  SYSDATE
                                                                                          -   365
                                                                                            * 3))
                                                                     AND a.INTERFACE_HEADER_CONTEXT IN ('PRISM INVOICES',
                                                                                                        'CONVERSION'))))
       v1
 WHERE     1 = 1
       AND v1.payment_activity = 0
       AND v1.order_activity = 0
       AND SITE_CREATION_DATE < TO_DATE ('15-NOV-2014', 'DD-MON-RRRR')
);
/
ALTER TABLE XXWC.XXWC_INACTIVE_SITES_TBL
ADD  (RECORD_STATUS VARCHAR2(50),ERROR_MESSAGE VARCHAR2(2000));
/
set serveroutput on
/
/*Account Site Script */
DECLARE
   l_errmsg VARCHAR2(2000);
   l_rec_count NUMBER;
   l_rec_success NUMBER;
   l_rec_error NUMBER;
   l_party_site_rec   hz_party_site_v2pub.PARTY_SITE_REC_TYPE;
   l_obj_num          NUMBER := 2;
   l_return_status    VARCHAR2 (1);
   l_msg_count        NUMBER;
   l_msg_data         VARCHAR2 (2000);
   CURSOR cr_site
   IS
      (SELECT ROWID REC_ROWID,
              PARTY_SITE_ID,
			  PARTY_SITE_NAME,
			  ACCOUNT_NUMBER,
			  ACCOUNT_NAME,
			  CUST_ACCOUNT_ID,
			  PARTY_SITE_NUMBER,
			  PARTY_SITE_CREATION_DATE,
			  SITE_USE_ID,
			  LOCATION,
			  COLLECTOR,
			  PROFILE_CLASS,
			  OBJECT_VERSION_NUMBER,
			  SITE_CLASSIFICATION,
			  SITE_USE_PRIMARY_FLAG,
			  NAME,
			  PAYMENT_ACTIVITY,
			  ORDER_ACTIVITY,
			  LAST_PAYMENT_DATE,
			  LAST_TRX_DATE,
			  SITE_CREATION_DATE,
              RECORD_STATUS,
              ERROR_MESSAGE
       FROM XXWC.XXWC_INACTIVE_SITES_TBL 
       WHERE 1=1
	   AND RECORD_STATUS IS NULL OR RECORD_STATUS = 'ERROR');
BEGIN
   dbms_output.put_line('Start of Site Loop');
   l_rec_count := 0;
   l_rec_success := 0;
   l_rec_error := 0;
   FOR rec_site IN cr_site
   LOOP
      l_rec_count := l_rec_count + 1;
      BEGIN
	     l_errmsg := NULL;
         --CALL API TO INACTIVATE SITE
		 mo_global.init ( 'AR' ) ;
         --mo_global.set_org_context ( 204, NULL, 'AR' ) ;
         --fnd_global.set_nls_context ( 'AMERICAN' ) ;
         mo_global.set_policy_context ( 'S', 162) ;
         l_party_site_rec.party_site_id := rec_site.party_site_id;
		 l_obj_num := rec_site.object_version_number;
         l_party_site_rec.status := 'I';
         hz_party_site_v2pub.update_party_site (
            p_init_msg_list           => FND_API.G_FALSE,
            p_party_site_rec          => l_party_site_rec,
            p_object_version_number   => l_obj_num,
            x_return_status           => l_return_status,
            x_msg_count               => l_msg_count,
            x_msg_data                => l_msg_data);
         IF l_return_status <> 'S' and l_msg_count > 0 THEN
            FOR i IN 1 .. l_msg_count
            LOOP
               l_errmsg := SUBSTR(l_errmsg||' '||i||'.'|| SUBSTR (FND_MSG_PUB.Get (p_encoded => FND_API.G_FALSE), 1, 255),1,2000);
            END LOOP;
			ROLLBACK;
			UPDATE XXWC.XXWC_INACTIVE_SITES_TBL
			SET RECORD_STATUS = 'ERROR',
			    ERROR_MESSAGE = l_errmsg
            WHERE ROWID = rec_site.REC_ROWID;
			l_rec_error := l_rec_error + 1;
         ELSE
            UPDATE XXWC.XXWC_INACTIVE_SITES_TBL
			SET RECORD_STATUS = 'SUCCESS',
			    ERROR_MESSAGE = l_errmsg
            WHERE ROWID = rec_site.REC_ROWID;
			l_rec_success := l_rec_success + 1;
         END IF;
	     --END CALL OF API
		 COMMIT;
	  EXCEPTION
	     WHEN OTHERS THEN
		    l_errmsg := SUBSTR(SQLERRM,1,2000);
			ROLLBACK;
			UPDATE XXWC.XXWC_INACTIVE_SITES_TBL
			SET RECORD_STATUS = 'ERROR',
			    ERROR_MESSAGE = l_errmsg
            WHERE ROWID = rec_site.REC_ROWID;
	        COMMIT;
			l_rec_error := l_rec_error + 1;
	  END;
   END LOOP;
   dbms_output.put_line('End of Site Loop');
   dbms_output.put_line('Total site records fetched = '||l_rec_count);
   dbms_output.put_line('Total site records inactivated successfully = '||l_rec_success);
   dbms_output.put_line('Total site records Error out = '||l_rec_error);
EXCEPTION
   WHEN OTHERS THEN
      l_errmsg := SUBSTR(SQLERRM,1,2000);
	  dbms_output.put_line('Unexpected error during site inactivation: '||l_errmsg);
END;
/
/*End Account Site Script*/
