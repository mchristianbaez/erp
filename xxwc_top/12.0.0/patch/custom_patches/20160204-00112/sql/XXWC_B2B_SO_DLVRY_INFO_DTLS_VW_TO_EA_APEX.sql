/********************************************************************************
   $Header XXWC_B2B_SO_DLVRY_INFO_DTLS_VW_TO_EA_APEX.sql $
   Module Name: XXWC_B2B_SO_DLVRY_INFO_DTLS_VW

   PURPOSE:  Grant to Apex on view - XXWC_B2B_SO_DLVRY_INFO_DTLS_VW.

   REVISIONS:
   Ver        Date        Author                     Description
   ---------  ----------  ---------------         -------------------------
   1.0        08/09/2015  Gopi Damuluri           TMS# 20150615-00088
                                                  Initial Version
   1.1        01/10/2016  Gopi Damuluri           TMS# 20160204-00112 - B2B POD Enhancement
********************************************************************************/

GRANT SELECT ON APPS.XXWC_B2B_SO_DLVRY_INFO_DTLS_VW TO EA_APEX WITH GRANT OPTION;

/