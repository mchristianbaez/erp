  /*******************************************************************************
  Table: XXWC.XXWC_B2B_POD_FILES_TBL
  Description: This table is used to maintain POD File information for B2B
  HISTORY
  ===============================================================================
  VERSION DATE               AUTHOR(S)       DESCRIPTION
  ------- -----------------  --------------- -----------------------------------------
  1.0     11-Dec-2015        Gopi Damuluri   TMS# 20160120-00169 - B2B POD Enhancement
                                             TMS# 20160204-00112 - B2B POD Enhancement  
  ********************************************************************************/
DROP TABLE XXWC.XXWC_B2B_POD_FILES_TBL CASCADE CONSTRAINTS;
/

CREATE TABLE XXWC.XXWC_B2B_POD_FILES_TBL
(
  FILE_PERMISSIONS  VARCHAR2(50 BYTE),
  FILE_TYPE         VARCHAR2(64 BYTE),
  FILE_OWNER        VARCHAR2(64 BYTE),
  FILE_GROUP        VARCHAR2(64 BYTE),
  FILE_SIZE         VARCHAR2(64 BYTE),
  FILE_MONTH        VARCHAR2(64 BYTE),
  FILE_DAY          VARCHAR2(64 BYTE),
  FILE_TIME         VARCHAR2(64 BYTE),
  FILE_NAME         VARCHAR2(612 BYTE)
)
ORGANIZATION EXTERNAL
  (  TYPE ORACLE_LOADER
     DEFAULT DIRECTORY XXWC_OM_DMS_IB_POD_DIR
     ACCESS PARAMETERS 
       ( RECORDS DELIMITED BY NEWLINE
     LOAD WHEN file_permissions != 'total'
     PREPROCESSOR XXWC_OM_DMS_IB_POD_DIR: 'list_files.sh'
     NOBADFILE
     NODISCARDFILE
     NOLOGFILE
     FIELDS TERMINATED BY WHITESPACE
    )
     LOCATION (XXWC_OM_DMS_IB_POD_DIR:'list_files_dummy_source.txt')
  )
REJECT LIMIT UNLIMITED
NOPARALLEL
NOMONITORING;
/

grant ALL on directory XXWC_OM_DMS_IB_POD_DIR to TD002849;