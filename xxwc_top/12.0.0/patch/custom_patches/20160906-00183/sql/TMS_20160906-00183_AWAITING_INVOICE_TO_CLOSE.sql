/*************************************************************************
  $Header TMS_20160906-00183_AWAITING_INVOICE_TO_CLOSE.sql $
  Module Name: TMS_20160906-00183  Data Fix script for I675908

  PURPOSE: Data fix script for I675908--No permanent fix in process (But patch available per oracle)

  REVISIONS:
  Ver        Date         Author                Description
  ---------  -----------  ------------------    --------------------------
  1.0        02-AUG-2016  Raghav Velichetti         TMS#20160906-00183   Order line has been cancelled. Line status is stuck in awaiting shipping 

**************************************************************************/ 
SET SERVEROUTPUT ON SIZE 1000000
DECLARE
BEGIN
   DBMS_OUTPUT.put_line ('TMS: 20160906-00183    , Before Update');

   update apps.oe_order_lines_all
   set FLOW_STATUS_CODE='CANCELLED',
   CANCELLED_FLAG='Y'
   where line_id = 78312215
   and header_id= 47865915;
   
   DBMS_OUTPUT.put_line (
         'TMS: 20160906-00183  Sales order lines updated (Expected:1): '
      || SQL%ROWCOUNT);

   COMMIT;

   DBMS_OUTPUT.put_line ('TMS: 20160906-00183    , End Update');
EXCEPTION
   WHEN OTHERS
   THEN
      ROLLBACK;
      DBMS_OUTPUT.put_line ('TMS: 20160906-00183 , Errors : ' || SQLERRM);
END;
/
