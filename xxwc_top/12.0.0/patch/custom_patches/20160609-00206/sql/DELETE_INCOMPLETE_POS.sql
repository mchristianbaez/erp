/*
 TMS: 20160609-00206 Delete Incomplete POs.
 Created by: Ram Talluri
 Date: 6/10/2016
 */
SET SERVEROUTPUT ON SIZE 1000000

DECLARE
   l_result       BOOLEAN;
   l_validation   VARCHAR2 (1) := 'N';
   l_po_number    NUMBER;
   l_count        NUMBER := 0;

   CURSOR c_pos
   IS
      SELECT po_header_id, segment1
        FROM po_headers_all pha
       WHERE     1 = 1
             AND pha.type_lookup_code = 'STANDARD'
             AND pha.org_id = 162
             AND creation_date <
                    TO_DATE ('01-JAN-2016 00:00:00',
                             'DD-MON-YYYY HH24:MI:SS')
             AND NVL (pha.authorization_status, 'INCOMPLETE') = 'INCOMPLETE'--AND ROWNUM < 4
;

   CURSOR c_po_details (
      p_header_id    NUMBER)
   IS
      SELECT pha.po_header_id,
             pha.org_id,
             pha.segment1,
             pha.agent_id,
             pdt.document_subtype,
             pdt.document_type_code,
             pha.closed_code,
             pha.closed_date,
             pha.type_lookup_code
        FROM apps.po_headers_all pha, apps.po_document_types_all pdt
       WHERE     pha.type_lookup_code = pdt.document_subtype
             AND pha.org_id = pdt.org_id
             AND pha.org_id = 162
             AND pdt.DOCUMENT_TYPE_CODE='PO'   
             AND pha.po_header_id = p_header_id;
BEGIN
   fnd_global.apps_initialize (user_id        => 16991,
                               resp_id        => 50983,
                               resp_appl_id   => 201);

   FOR i IN c_pos
   LOOP
      FOR j IN c_po_details (i.po_header_id)
      LOOP
         l_result :=
            po_headers_sv1.delete_po (
               X_po_header_id       => j.po_header_id,
               X_type_lookup_code   => j.type_lookup_code,
               p_skip_validation    => l_validation);

         /* IF l_result = TRUE
          THEN
             COMMIT;
             DBMS_OUTPUT.put_line (
                   'PO: '
                || j.segment1
                || ',Deleted Successfully');
          ELSE
             ROLLBACK;
             DBMS_OUTPUT.put_line (
                'PO : ' || j.segment1 || ',Failed to Delete');
          END IF;

          DBMS_OUTPUT.put_line ('Deletion Process Over');*/
         COMMIT;
      END LOOP;

      l_po_number := i.segment1;
      l_count := l_count + 1;
   END LOOP;

   DBMS_OUTPUT.PUT_LINE ('Number of POs deleted-' || l_count);
EXCEPTION
   WHEN OTHERS
   THEN
      DBMS_OUTPUT.put_line (
         'Error in deleting PO #: ' || l_po_number || SQLERRM);
END;
/