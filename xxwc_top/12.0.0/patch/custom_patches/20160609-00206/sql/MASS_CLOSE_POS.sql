/*
 TMS: 20160609-00206 Mass Close POs.
 Created by: Ram Talluri
 Date: 6/10/2016
 */
SET SERVEROUTPUT ON SIZE 1000000

DECLARE
   l_po_header_id   NUMBER;

   lv_result        BOOLEAN;
   lv_return_code   VARCHAR2 (20);
   lv_header_id     NUMBER;
   l_count          NUMBER := 0;

   CURSOR c_pos
   IS
      SELECT po_header_id
        FROM po_headers_all pha
       WHERE     creation_date <
                    TO_DATE ('31-DEC-2015 23:59:59',
                             'DD-MON-YYYY HH24:MI:SS')
             AND org_id = 162
             AND type_lookup_code = 'STANDARD'
             AND authorization_status = 'APPROVED'
             AND closed_code = 'OPEN'
             AND NVL (cancel_flag, 'N') = 'N'
             AND NOT EXISTS
                    (SELECT 1
                       FROM po_headers_all pha1,
                            po_lines_all pla,
                            mtl_system_items_b msi
                      WHERE     pha1.po_header_id = pla.po_header_id
                            AND pla.item_id = msi.inventory_item_id
                            AND msi.organization_id = 222
                            AND msi.segment1 LIKE 'R%'
                            AND EXISTS
                                   (SELECT category_id
                                      FROM mtl_categories_v
                                     WHERE     UPPER(description) LIKE '%RENT%'
                                           AND category_id = pla.category_id)
                            AND pha1.po_header_id = pha.po_header_id
);

   CURSOR c_po_details (
      p_header_id    NUMBER)
   IS
      SELECT pha.po_header_id,
             pha.org_id,
             pha.segment1,
             pha.agent_id,
             pdt.document_subtype,
             pdt.document_type_code,
             pha.closed_code,
             pha.closed_date
        FROM apps.po_headers_all pha, apps.po_document_types_all pdt
       WHERE     pha.type_lookup_code = pdt.document_subtype
             AND pha.org_id = pdt.org_id
             AND pha.org_id = 162
             AND pdt.DOCUMENT_TYPE_CODE='PO'   
             AND authorization_status = 'APPROVED'
             AND pha.closed_code <> 'FINALLY CLOSED'
             AND pha.po_header_id = p_header_id;
BEGIN
   fnd_global.apps_initialize (user_id        => 16991,
                               resp_id        => 50983,
                               resp_appl_id   => 201);

   FOR i IN c_pos
   LOOP
      FOR j IN c_po_details (i.po_header_id)
      LOOP
         BEGIN
            lv_result :=
               PO_ACTIONS.CLOSE_PO (
                  P_DOCID           => j.po_header_id,
                  P_DOCTYP          => 'PO',
                  P_DOCSUBTYP       => 'STANDARD', -- Can be STANDARD, BLANKET, RELEASE
                  P_LINEID          => NULL,          -- If want to close Line
                  P_SHIPID          => NULL,      -- If want to close Shipment
                  P_ACTION          => 'CLOSE',
                  P_REASON          => 'PO CLOSE FOR TMS 20160609-00206',
                  P_CALLING_MODE    => j.document_type_code,
                  P_CONC_FLAG       => 'N',
                  P_RETURN_CODE     => lv_return_code,
                  P_AUTO_CLOSE      => 'N',
                  P_ACTION_DATE     => SYSDATE,
                  P_ORIGIN_DOC_ID   => NULL);
         /*  IF lv_result = TRUE
           THEN
              DBMS_OUTPUT.PUT_LINE ('Successfully closed PO#' || j.segment1);
           ELSE
              DBMS_OUTPUT.PUT_LINE ('Cannot close PO#' || j.segment1);
           END IF;*/
         END;
      END LOOP;

      COMMIT;
      l_po_header_id := i.po_header_id;
      l_count := l_count + 1;
   END LOOP;

   DBMS_OUTPUT.PUT_LINE ('Number of POs closed-' || l_count);
EXCEPTION
   WHEN OTHERS
   THEN
      DBMS_OUTPUT.put_line (
            'Finally close falied for po_header_id-'
         || l_po_header_id
         || ' Error is '
         || SQLERRM);
END;
/