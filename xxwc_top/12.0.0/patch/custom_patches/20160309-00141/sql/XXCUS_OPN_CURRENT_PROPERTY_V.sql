CREATE OR REPLACE FORCE VIEW XXCUS.XXCUS_OPN_CURRENT_PROPERTY_V AS
/* 
-- *****************************************************************************************
-- Scope: This view will be the source for the APEX COMMON systems and the Web Application
--               Location Finder Plus tool.
-- Grants:  Oracle EBIZPRD database user XXCUS has granted "SELECT" access to user INTERFACE_DSTAGE
-- $Header XXCUS_OPN_CURRENT_PROPERTY_V.sql $
-- Module Name: HDS Property Manager
-- REVISIONS:
-- Ver        Date        Author                       Description
-- ---------  ----------  ----------                   ----------------
-- 1.0        10/14/2015  Bala Seshadri                Created.     
-- 1.1        01/14/2016  Bala Seshadri                TMS: 20160114-00042 / ESMS: 313314, Recompile view as we've added new field 
--                                                     hds_prop_current_flag to base table XXCUS.XXCUS_OPN_PROPERTIES_ALL      
-- ***************************************************************************************** 
*/
SELECT *
  FROM XXCUS.XXCUS_OPN_PROPERTIES_ALL
 WHERE 1 = 1 
       AND OPN_TYPE = 'PROPERTY' 
       AND HDS_CURRENT_FLAG = 'Y';
--
COMMENT ON TABLE XXCUS.XXCUS_OPN_CURRENT_PROPERTY_V IS 'TMS: 20160114-00042 / ESMS: 313314';
--
GRANT SELECT ON XXCUS.XXCUS_OPN_CURRENT_PROPERTY_V TO INTERFACE_DSTAGE;
--