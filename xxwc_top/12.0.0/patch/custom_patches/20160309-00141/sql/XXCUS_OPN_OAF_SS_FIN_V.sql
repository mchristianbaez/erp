  /*
  ===========================================================================
    Module: Oracle Property Manager
    Type: HDS GSC custom view
    PURPOSE: Used by HDS OPN Property Manager -Oracle Application Form SuperSearch UI TAB  "Finance"
    HISTORY
  ==============================================================================================================================
      VERSION DATE                  AUTHOR(S)                 DESCRIPTION                       TICKET
      -------        -----------           ---------------                ------------------------------    ---------------                          
      1.0            30-Dec-2015   Balaguru Seshadri  Created.                                 TMS 20160209-00169   
  */  
CREATE OR REPLACE FORCE VIEW APPS.XXCUS_OPN_OAF_SS_FIN_V AS
SELECT A.PAYMENT_TERM_TYPE PAYMENT_TYPE
             ,A.FREQUENCY_TYPE FREQUENCY
             ,TRUNC(A.START_DATE) START_DATE
             ,TRUNC(A.END_DATE) END_DATE
             ,A.ESTIMATED_AMOUNT ESTIMATED_AMOUNT
             ,regexp_replace(a.term_comments, '[[:cntrl:]]', '') term_comments             
             ,Initcap(A.ATTRIBUTE1) fixed_var
             ,A.ATTRIBUTE2 pays_tax
             ,A.LEASE_ID
             ,A.LEASE_CHANGE_ID
FROM APPS.PN_PAYMENT_TERMS_V a
WHERE 1 =1;
--
COMMENT ON TABLE APPS.XXCUS_OPN_OAF_SS_FIN_V IS  'TMS 20160209-00169 OR ESMS: 195667';
--