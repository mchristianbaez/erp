CREATE OR REPLACE FORCE VIEW XXCUS.XXCUS_OPN_CURRENT_FRU_LOC_V AS
/* 
-- **************************************************************************************************
-- Scope: This view will be the source for the APEX COMMON systems and the Web Application
--               Location Finder Plus tool.
-- Grants:  Oracle EBIZPRD database user XXCUS has granted "SELECT" access to user INTERFACE_DSTAGE [See below]
-- $Header XXCUS_OPN_CURRENT_FRU_LOC_V.sql $
-- Module Name: HDS Property Manager
-- REVISIONS:
-- Ver        Date        Author                       Description
-- ---------  ----------  ----------                   ----------------
-- 1.0        10/14/2015  Bala Seshadri                Created.  
-- 1.1        01/14/2016  Bala Seshadri                TMS: 20160114-00042 / ESMS: 313314, Recompile view as we've added new fields 
--                                                     HDS_FRU_LOC, HDS_FEED_TO_EXTENDED, HDS_FEED_TO_EXTERNAL and FAX_NUMBER to base table 
--                                                     XXCUS.XXCUS_OPN_FRU_LOC_ALL
-- 1.2        03/04/2016  Bala Seshadri                TMS 20160302-00017 / ESMS 318695  --Need to recreate to include hds_active_bldg_sf_fru_prp and extract date fields plus six other fields	
-- ************************************************************************************************** 
*/
SELECT *
  FROM XXCUS.XXCUS_OPN_FRU_LOC_ALL
 WHERE 1 = 1 
       AND OPN_TYPE = 'PROPERTY' 
       AND HDS_FRU_SECTION_CURRENT_FLAG = 'Y';
--
COMMENT ON TABLE XXCUS.XXCUS_OPN_CURRENT_FRU_LOC_V IS 'TMS: 20160302-00017 / ESMS: 318695';
--
GRANT SELECT ON XXCUS.XXCUS_OPN_CURRENT_FRU_LOC_V TO INTERFACE_DSTAGE;
--