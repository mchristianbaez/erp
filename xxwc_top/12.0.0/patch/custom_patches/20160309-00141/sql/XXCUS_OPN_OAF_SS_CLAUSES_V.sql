  /*
  ===========================================================================
    Module: Oracle Property Manager
    Type: HDS GSC custom view
    PURPOSE: Used by HDS OPN Property Manager -Oracle Application Form SuperSearch UI TAB  "Clauses"
    HISTORY
  ==============================================================================================================================
      VERSION DATE                  AUTHOR(S)                 DESCRIPTION                       TICKET
      -------        -----------           ---------------                ------------------------------    ---------------                          
      1.0            30-Dec-2015   Balaguru Seshadri  Created.                                 TMS 20160209-00169        
  */  
CREATE OR REPLACE FORCE VIEW APPS.XXCUS_OPN_OAF_SS_CLAUSES_V AS
SELECT a.right_type
             ,a.right_reference
             ,regexp_replace(a.right_comments, '[[:cntrl:]]', '') rights_comments
             ,a.right_status
             ,a.lease_id
             ,a.lease_change_id
             ,a.right_id
             ,a.right_num
FROM APPS.PN_RIGHTS_V a
WHERE 1 =1;
--
COMMENT ON TABLE APPS.XXCUS_OPN_OAF_SS_CLAUSES_V IS  'TMS 20160209-00169 OR ESMS: 195667';
--