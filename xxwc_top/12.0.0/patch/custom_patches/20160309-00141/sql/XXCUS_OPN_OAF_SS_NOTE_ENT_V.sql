  /*
  ===========================================================================
    Module: Oracle Property Manager
    Type: HDS GSC custom view
    PURPOSE: Used by HDS OPN Property Manager -Oracle Application Form SuperSearch UI TAB  "Notes and Entities"
    HISTORY
  ==============================================================================================================================
      VERSION DATE                  AUTHOR(S)                 DESCRIPTION                       TICKET
      -------        -----------           ---------------                ------------------------------    ---------------                          
      1.0            30-Dec-2015   Balaguru Seshadri  Created.                                 TMS 20160209-00169       
  */  
CREATE OR REPLACE FORCE VIEW APPS.XXCUS_OPN_OAF_SS_NOTE_ENT_V AS
SELECT A.NOTE_TYPE
             ,TRUNC(A.NOTE_DATE) NOTE_DATE
             ,REGEXP_REPLACE(B.TEXT, '[[:cntrl:]]', '') NOTE_TEXT
             ,(
                  select related_party_name
                  from  xxcus.xxcus_opn_rer_locations_all
                  where 1 =1
                  and lease_id =a.lease_id 
                  and rownum <2
              ) related_party_lease
             ,(
                  select guarantor
                  from  xxcus.xxcus_opn_rer_locations_all
                  where 1 =1
                  and lease_id =a.lease_id 
                  and rownum <2
              ) guarantor              
             ,A.LEASE_ID
             ,B.NOTE_DETAIL_ID
             ,A.NOTE_HEADER_ID
FROM PN_NOTE_HEADERS_V  A
,PN_NOTE_DETAILS_VL B
WHERE 1 =1
AND B.NOTE_HEADER_ID(+) =A.NOTE_HEADER_ID;
--
COMMENT ON TABLE APPS.XXCUS_OPN_OAF_SS_NOTE_ENT_V IS 'TMS 20160209-00169 OR ESMS: 195667';
--