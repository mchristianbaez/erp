   /***************************************************************************************************************************************************
        $Header XXWC_AR_AHH_CASH_RCPTS_S $
        Module Name: XXWC_AR_AHH_CASH_RCPTS_S.sql

        PURPOSE:AHH AR cash receipts import. 

        REVISIONS:
        Ver        Date        Author           Description
        ---------  ----------  ---------------  ------------------------------------------------------------------------------------------------
        1.0        08/16/2018  P.Vamshidhar     20180816-00001_AHH Debit Memo and Cash Receipts Staging table Change
   ******************************************************************************************************************************************************/
   
CREATE SEQUENCE XXWC.XXWC_AR_AHH_CASH_RCPTS_S
  START WITH 1
  MAXVALUE 99999999999
  MINVALUE 1
  NOCYCLE
  NOCACHE
  NOORDER
/  
  


