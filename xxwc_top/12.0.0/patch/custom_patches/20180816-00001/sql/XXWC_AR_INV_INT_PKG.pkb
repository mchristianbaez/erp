CREATE OR REPLACE PACKAGE BODY APPS.XXWC_AR_INV_INT_PKG
IS
   /******************************************************************************************************************************************************
        $Header XXWC_AR_INV_INT_PKG $
        Module Name: XXWC_AR_INV_INT_PKG.pkb

        PURPOSE:   AHH Customer Conversion

        REVISIONS:
        Ver        Date        Author           Description
        ---------  ----------  ---------------  ------------------------------------------------------------------------------------------------
        1.0        06/06/2018  P.Vamshidhar     TMS#20180708-00002 - AH HARRIS AR Debit Memo/Invoice Interface
   ******************************************************************************************************************************************************/
   ----------------------------------------------------------------------------------
   -- GLOBAL VARIABLES
   ----------------------------------------------------------------------------------
   g_request_id                 NUMBER;
   g_batch_source               ra_batch_sources_all.name%TYPE := 'HARRIS';
   g_context                    VARCHAR2 (50) := 'PRISM INVOICES';
   g_org_name                   hr_operating_units.name%TYPE := 'HDS White Cap - Org';
   g_currency                   VARCHAR2 (15) := 'USD';
   g_amount_includes_tax_flag   VARCHAR2 (1) := 'Y';
   g_taxable_flag               VARCHAR2 (1) := 'Y';
   g_conversion_type            VARCHAR2 (30) := 'User';
   g_conversion_rate            NUMBER := 1;
   g_org_id                     NUMBER;
   g_dflt_tax_acct              VARCHAR2 (20);
   g_frt_acct                   VARCHAR2 (20);
   g_rec_acct                   VARCHAR2 (20);
   g_segment1                   VARCHAR2 (20) := '0W';
   g_segment3                   VARCHAR2 (20) := '0000';
   g_segment5                   VARCHAR2 (20) := '00000';
   g_segment6                   VARCHAR2 (20) := '00000';
   g_segment7                   VARCHAR2 (20) := '00000';
   g_retcode                    NUMBER := 0;
   g_err_msg                    VARCHAR2 (2000);
   g_threshold_value            NUMBER;
   g_dflt_cat_class             VARCHAR2 (30) := 'DFLT';

   ----------------------------------------------------------------------------------
   -- DEBIT MEMO - GLOBAL VARIABLES
   ----------------------------------------------------------------------------------
   g_dm_rec_acct                VARCHAR2 (20);
   g_dm_rev_acct                VARCHAR2 (20);
   g_dm_rev_acct_fh             VARCHAR2 (20);
   g_dm_description             mtl_system_items_b.description%TYPE
                                   := 'WC : Debit Memos';
   g_dm_term_name               VARCHAR2 (30) := 'IMMEDIATE';
   g_dm_sales_person            VARCHAR2 (30) := 'No Sales Credit';
   g_dm_batch_source            ra_batch_sources_all.name%TYPE
                                   := 'HARRIS REFUND'; --'PRISM REFUND'; -- Vamshi

   --Email Defaults
   ppl_dflt_email               fnd_user.email_address%TYPE
                                   := 'HDSOracleDevelopers@hdsupply.com';

   /********************************************************************************
   ProcedureName : LOAD_INTERFACE
   Purpose       : API to validate and process PRISM Sales Invoice data from Oracle
                   Staging tables, load them into Oracle Interface tables.

   HISTORY
   ===============================================================================
   VERSION DATE          AUTHOR(S)       DESCRIPTION
   ------- -----------   --------------- -----------------------------------------
   1.0     12/13/2011    Gopi Damuluri    Initial version.
   ********************************************************************************/
   PROCEDURE load_interface
   AS
      l_error_code            NUMBER;
      l_recordcnt             NUMBER (6) := 0;
      l_error_message         VARCHAR2 (2000);
      l_err_flag              VARCHAR2 (1) := 'N';
      l_ship_to_found         VARCHAR2 (1) := 'N';
      l_org_id                NUMBER;
      l_inv_line_num          NUMBER;
      l_bill_to_address_id    NUMBER;
      l_ship_to_address_id    NUMBER;
      l_bill_to_site_use_id   NUMBER;
      l_bill_to_cust_id       NUMBER;
      l_ship_to_cust_id       NUMBER;
      l_party_site_id         NUMBER;
      l_cust_acct_id          NUMBER;
      l_salesrep_id           NUMBER;
      l_branch_code           VARCHAR2 (5);
      l_inv_org_id            NUMBER;
      l_inv_item_id           NUMBER;
      l_terms_name            xxwc.XXWC_AHH_PAYMENT_TERMS_T.AHH_TERM_NAME%TYPE;
      l_terms_id              NUMBER;
      l_rev_ccid              NUMBER;
      l_rec_ccid              NUMBER;
      l_cus_trx_type_id       NUMBER;
      l_trx_name              ra_cust_trx_types_all.name%TYPE;
      l_dup_trx_val_flag      VARCHAR2 (1);
      l_uom_code              mtl_system_items_b.primary_uom_code%TYPE;
      l_uom_name              mtl_system_items_b.primary_unit_of_measure%TYPE;
      l_description           mtl_system_items_b.description%TYPE
         := 'WC : Line Memo For Legacy System Data Conversion';
      l_inv_num               ra_customer_trx_all.trx_number%TYPE;
      l_trx_type_id           NUMBER;
      l_tax_rev_ccid          NUMBER;
      l_rev_acct              VARCHAR2 (20);
      l_direct_acct           VARCHAR2 (20);
      l_int_comp_acct         VARCHAR2 (20);
      l_rental_acct           VARCHAR2 (20);
      l_tax_acct              VARCHAR2 (20);
      l_ship_to_state         VARCHAR2 (3);
      l_frt_rev_ccid          NUMBER;
      l_freight_cnt           NUMBER := 0;
      l_freight_amt           NUMBER := 0;
      l_rec_dist_line_cnt     NUMBER := 0;
      l_rec_frt_amt           NUMBER := 0;
      l_rec_amt               NUMBER := 0;
      l_line_tax_amt          NUMBER := 0;
      l_diff_tax_amt          NUMBER := 0;
      l_zero_tax_line_cnt     NUMBER := 0;
      l_frt_tax_amt           NUMBER := 0;
      l_sum_tax_amt           NUMBER := 0;
      l_frt_line_exists       VARCHAR2 (1);
      l_inv_rev_amt           NUMBER := 0;
      l_inv_freight_amt       NUMBER := 0;
      l_inv_amt               NUMBER := 0;
      lvc_reason_code         RA_CUSTOMER_TRX_LINES_ALL.REASON_CODE%TYPE;
      lvc_ship_via            VARCHAR2 (1000);
      l_intan_rev_acct        gl_code_combinations.segment4%TYPE;
      l_program_error         EXCEPTION;
      ln_batch_source_id      RA_BATCH_SOURCES.BATCH_SOURCE_ID%TYPE;

      ---------------------------------------------------------------------------------
      -- Cursor to extract Invoice Header Data from Staging Table
      ----------------------------------------------------------------------------------
      CURSOR cur_hdr
      IS
           SELECT xaist.ROWID r_id,
                  xaist.trx_number,
                  xaist.cust_trx_type_id,
                  xaist.revenue_amount,
                  xaist.amount,
                  xaist.paying_customer_id,
                  xaist.stg_attribute3,
                  xaist.orig_system_bill_customer_ref,
                  xaist.primary_salesrep_number,
                  xaist.warehouse_id,
                  xaist.term_id term_name,
                  DECODE (xaist.legacy_party_site_number,
                          '0', TO_CHAR (xaist.paying_customer_id),
                          xaist.legacy_party_site_number)
                     legacy_party_site_number,
                  xaist.stg_attribute2,
                  'I' inv_type_flag,
                  UPPER (xaist.PAYING_SITE_USE_ID) PAYING_SITE_USE_ID,
                  UPPER (xaist.reason_code) reason_code,
                  UPPER (xaist.ship_via) ship_via
             FROM xxwc.xxwc_ar_inv_stg_tbl xaist
            WHERE     1 = 1
                  AND NVL (xaist.status, 'XXXXX') <> 'PROCESSED'
                  AND NVL (xaist.revenue_amount, 0) != 0
                  AND NVL (xaist.freight_amount, 0) = 0             -- IS NULL
         GROUP BY xaist.ROWID,
                  'I',
                  xaist.trx_number,
                  xaist.cust_trx_type_id,
                  xaist.revenue_amount,
                  xaist.amount,
                  xaist.paying_customer_id,
                  xaist.stg_attribute3,
                  xaist.orig_system_bill_customer_ref,
                  xaist.primary_salesrep_number,
                  xaist.warehouse_id,
                  xaist.term_id,
                  xaist.stg_attribute2,
                  xaist.legacy_party_site_number,
                  UPPER (xaist.PAYING_SITE_USE_ID),
                  UPPER (xaist.reason_code),
                  UPPER (xaist.ship_via)
         UNION
           SELECT xaist.ROWID r_id,
                  xaist.trx_number,
                  xaist.cust_trx_type_id,
                  xaist.revenue_amount,
                  xaist.amount,
                  xaist.paying_customer_id,
                  xaist.stg_attribute3,
                  xaist.orig_system_bill_customer_ref,
                  xaist.primary_salesrep_number,
                  xaist.warehouse_id,
                  xaist.term_id term_name,
                  DECODE (xaist.legacy_party_site_number,
                          '0', TO_CHAR (xaist.paying_customer_id),
                          xaist.legacy_party_site_number)
                     legacy_party_site_number,
                  xaist.stg_attribute2,
                  'F' inv_type_flag,
                  UPPER (xaist.PAYING_SITE_USE_ID) PAYING_SITE_USE_ID,
                  UPPER (xaist.reason_code) reason_code,
                  UPPER (xaist.ship_via) ship_via
             FROM xxwc.xxwc_ar_inv_stg_tbl xaist
            WHERE     1 = 1
                  AND NVL (xaist.status, 'XXXXX') <> 'PROCESSED'
                  AND xaist.freight_amount IS NOT NULL
                  AND NVL (xaist.revenue_amount, 0) = 0
                  AND NOT EXISTS
                         (SELECT '1'
                            FROM xxwc.xxwc_ar_inv_stg_tbl xaist_f
                           WHERE     xaist_f.freight_amount IS NULL
                                 AND NVL (xaist_f.revenue_amount, 0) != 0
                                 AND xaist_f.trx_number = xaist.trx_number)
         GROUP BY xaist.ROWID,
                  'F',
                  xaist.trx_number,
                  xaist.cust_trx_type_id,
                  xaist.revenue_amount,
                  xaist.amount,
                  xaist.paying_customer_id,
                  xaist.stg_attribute3,
                  xaist.orig_system_bill_customer_ref,
                  xaist.primary_salesrep_number,
                  xaist.warehouse_id,
                  xaist.term_id,
                  xaist.legacy_party_site_number,
                  xaist.stg_attribute2,
                  UPPER (xaist.PAYING_SITE_USE_ID),
                  UPPER (xaist.reason_code),
                  UPPER (xaist.ship_via)
         ORDER BY trx_number;

      ----------------------------------------------------------------------------------
      -- Cursor to extract Invoice Line Data from Staging Table
      ----------------------------------------------------------------------------------
      CURSOR cur_line (
         p_trx_number     VARCHAR2,
         p_trx_type_id    NUMBER)
      IS
           SELECT xaist.ROWID r_id, xaist.*
             FROM xxwc.xxwc_ar_inv_stg_tbl xaist
            WHERE     1 = 1
                  AND NVL (xaist.status, 'XXXXX') <> 'PROCESSED'
                  AND xaist.trx_number = p_trx_number
                  --AND xaist.cust_trx_type_id = p_trx_type_id
         ORDER BY xaist.trx_number;

      ----------------------------------------------------------------------------------
      -- Cursor to extract Invoice Freight Data from Staging Table
      ----------------------------------------------------------------------------------
      CURSOR cur_freight (
         p_trx_number     VARCHAR2,
         p_trx_type_id    NUMBER)
      IS
           SELECT xaist.ROWID r_id, xaist.*
             FROM xxwc.xxwc_ar_inv_stg_tbl xaist
            WHERE     1 = 1
                  AND NVL (xaist.status, 'XXXXX') <> 'PROCESSED'
                  AND xaist.trx_number = p_trx_number
                  --AND xaist.cust_trx_type_id = p_trx_type_id
                  --AND freight_amount IS NOT NULL
                  AND NVL (freight_amount, 0) <> 0
         ORDER BY xaist.trx_number;
   BEGIN
      fnd_file.put_line (
         fnd_file.LOG,
         '----------------------------------------------------------------------------------');
      fnd_file.put_line (fnd_file.LOG, 'Start of Procedure : LOAD_INTERFACE');
      fnd_file.put_line (
         fnd_file.LOG,
         '----------------------------------------------------------------------------------');

      ----------------------------------------------------------------------------------
      -- Derive OperatingUnitId.
      ----------------------------------------------------------------------------------
      BEGIN
         SELECT organization_id
           INTO g_org_id
           FROM hr_operating_units
          WHERE NAME = g_org_name;

         fnd_file.put_line (fnd_file.LOG, 'g_org_id - ' || g_org_id);
      EXCEPTION
         WHEN OTHERS
         THEN
            fnd_file.put_line (fnd_file.LOG,
                               'Error deriving Operating Unit details');
      END;

      BEGIN
         SELECT batch_source_id
           INTO ln_batch_source_id
           FROM apps.ra_batch_sources_all
          WHERE name = g_batch_source;
      EXCEPTION
         WHEN OTHERS
         THEN
            ln_batch_source_id := NULL;
            fnd_file.put_line (fnd_file.LOG,
                               'Error deriving batch Source name');
      END;

      fnd_file.put_line (fnd_file.LOG,
                         'ln_batch_source_id:' || ln_batch_source_id);

      fnd_file.put_line (fnd_file.LOG, 'Before Main Loop');

      FOR rec_hdr IN cur_hdr
      LOOP
         -- rec_hdr loop start>
         fnd_file.put_line (fnd_file.LOG, 'Inside Main loop');
         -- Initialize Loop Variables
         l_err_flag := 'N';
         l_ship_to_found := 'N';
         l_bill_to_address_id := NULL;
         l_ship_to_address_id := NULL;
         l_bill_to_cust_id := NULL;
         l_ship_to_cust_id := NULL;
         l_party_site_id := NULL;
         l_cust_acct_id := NULL;
         l_salesrep_id := NULL;
         l_branch_code := NULL;
         l_inv_org_id := NULL;
         l_terms_name := NULL;
         l_terms_id := NULL;
         l_cus_trx_type_id := NULL;
         l_trx_name := NULL;
         l_dup_trx_val_flag := NULL;
         l_error_message := NULL;
         l_trx_type_id := NULL;
         l_inv_num := NULL;
         l_diff_tax_amt := NULL;
         l_inv_rev_amt := NULL;
         l_inv_freight_amt := NULL;
         l_inv_amt := NULL;
         lvc_ship_via := NULL;
         lvc_reason_code := NULL;

         fnd_file.put_line (
            fnd_file.LOG,
            ' --------------------------------------------------------------------------- ');
         fnd_file.put_line (fnd_file.LOG,
                            'Invoice Number - ' || rec_hdr.trx_number);

         -- Invoice Line#
         --       IF l_inv_num  = rec_hdr.trx_number THEN
         --         NULL;
         --       ELSE
         l_inv_num := rec_hdr.trx_number;
         l_trx_type_id := rec_hdr.cust_trx_type_id;
         --       END IF;

         ----------------------------------------------------------------------------------
         -- COMMIT for every 1000 records
         ----------------------------------------------------------------------------------
         l_recordcnt := l_recordcnt + 1;

         IF l_recordcnt = 1000
         THEN
            COMMIT;
            l_recordcnt := 0;
         END IF;

         BEGIN
            -- Invoice Amount Validation > Start
            ----------------------------------------------------------------------------------
            -- Validate if InvoiceAmount matches sum of Revenue, Tax and Freight Amounts
            ----------------------------------------------------------------------------------
            -- Derive Revenue Amount
            BEGIN
               SELECT SUM (NVL (revenue_amount, 0))
                 INTO l_inv_rev_amt
                 FROM xxwc.xxwc_ar_inv_stg_tbl
                WHERE 1 = 1 AND trx_number = rec_hdr.trx_number;
            EXCEPTION
               WHEN OTHERS
               THEN
                  l_err_flag := 'Y';
                  l_error_message :=
                     l_error_message || '-- Error deriving Revenue Amount';
            END;

            -- Derive Freight Amount
            BEGIN
               SELECT SUM (NVL (freight_amount, 0))
                 INTO l_inv_freight_amt
                 FROM xxwc.xxwc_ar_inv_stg_tbl
                WHERE 1 = 1 AND trx_number = rec_hdr.trx_number;
            EXCEPTION
               WHEN OTHERS
               THEN
                  l_err_flag := 'Y';
                  l_error_message :=
                     l_error_message || '-- Error deriving Freight Amount';
            END;

            l_inv_amt :=
               l_inv_rev_amt + l_inv_freight_amt + rec_hdr.stg_attribute2;

            IF rec_hdr.amount != l_inv_amt
            THEN
               l_err_flag := 'Y';
               l_error_message :=
                     l_error_message
                  || '-- InvoiceAmount does not match sum of Revenue, Tax and Freight Amounts';
               RAISE l_program_error;
            END IF;

            ----------------------------------------------------------------------------------
            -- Derive Invoice Type
            ----------------------------------------------------------------------------------

            IF rec_hdr.amount >= 0
            THEN
               l_trx_name := 'Invoice';
            ELSIF rec_hdr.amount < 0
            THEN
               l_trx_name := 'Credit Memo';
            ELSE
               l_trx_name := 'Invoice';
            END IF;
            ----------------------------------------------------------------------------------
            -- Derive InvoiceTypeId
            ----------------------------------------------------------------------------------
            BEGIN
               SELECT cust_trx_type_id
                 INTO l_cus_trx_type_id
                 FROM ra_cust_trx_types_all
                WHERE     NAME = l_trx_name
                      AND SYSDATE BETWEEN start_date
                                      AND NVL (end_date, SYSDATE + 1)
                      AND org_id = g_org_id;
            EXCEPTION
               WHEN OTHERS
               THEN
                  l_err_flag := 'Y';
                  l_error_message :=
                     l_error_message || '--Invalid Transaction Type';
            END;

            -- OE SHIP-TO LOGIC > START
            IF NVL (rec_hdr.stg_attribute3, 0) > 0
            THEN
               ----------------------------------------------------------------------------------
               -- Derive Sold To Account Details
               ----------------------------------------------------------------------------------
               -- Deriving Customer account from Cross Over table.
               BEGIN
                  SELECT hca.cust_account_id
                    INTO l_cust_acct_id
                    FROM XXWC.XXWC_AHH_AR_CUST_CROSS_OVER_T xaac,
                         hz_cust_accounts hca
                   WHERE     xaac.CUST_NUM =
                                TRIM (TO_CHAR (rec_hdr.paying_customer_id))
                         AND TRIM (
                                REPLACE (
                                   REPLACE (xaac.oracle_cust_num,
                                            CHR (13),
                                            ''),
                                   CHR (10),
                                   '')) = hca.account_number
                         AND hca.status = 'A'
                         AND ROWNUM = 1;
               EXCEPTION
                  WHEN OTHERS
                  THEN
                     BEGIN
                        SELECT hca.cust_account_id
                          INTO l_cust_acct_id
                          FROM hz_cust_accounts hca,
                               HZ_CUST_ACCT_SITES_ALL hcas
                         WHERE     hca.cust_account_id = hcas.cust_account_id
                               AND NVL (hcas.attribute17, '123') =
                                      TRIM (
                                         TO_CHAR (rec_hdr.paying_customer_id))
                               AND hca.status = 'A'
                               AND hcas.org_id = 162
                               AND hca.attribute4 = 'AHH'
                               AND hcas.bill_to_flag = 'P';
                     EXCEPTION
                        WHEN NO_DATA_FOUND
                        THEN
                           BEGIN
                              SELECT CUST_ACCOUNT_ID
                                INTO l_cust_acct_id
                                FROM APPS.HZ_CUST_ACCT_SITES_ALL
                               WHERE NVL (ATTRIBUTE17, '123') =
                                        TRIM (
                                              TO_CHAR (
                                                 rec_hdr.paying_customer_id)
                                           || '-'
                                           || TO_CHAR (
                                                 rec_hdr.paying_site_use_id));
                           EXCEPTION
                              WHEN OTHERS
                              THEN
                                 l_err_flag := 'Y';
                                 l_error_message :=
                                       l_error_message
                                    || '-- Customer not available in Oracle1';
                           END;
                        WHEN OTHERS
                        THEN
                           l_err_flag := 'Y';
                           l_error_message :=
                                 l_error_message
                              || '-- Customer not available in Oracle2';
                     END;
               END;

               ----------------------------------------------------------------------------------
               -- Derive Ship-To Address using Customer Id
               ----------------------------------------------------------------------------------
               BEGIN
                  SELECT hcas.cust_acct_site_id,
                         hcas.cust_account_id,
                         hcsu.bill_to_site_use_id,
                         hcas.party_site_id
                    INTO l_ship_to_address_id,
                         l_ship_to_cust_id,
                         l_bill_to_site_use_id,
                         l_party_site_id
                    FROM hz_cust_acct_sites_all hcas,
                         hz_cust_site_uses_all hcsu
                   WHERE     1 = 1
                         AND hcas.status = 'A'
                         AND hcsu.status = 'A'
                         AND hcas.ship_to_flag = 'P'
                         AND hcas.cust_account_id = l_cust_acct_id
                         AND hcas.cust_acct_site_id = hcsu.cust_acct_site_id
                         AND hcsu.site_use_code = 'SHIP_TO'
                         AND hcas.org_id = g_org_id;

                  l_ship_to_found := 'Y';
               EXCEPTION
                  WHEN NO_DATA_FOUND
                  THEN
                     l_ship_to_found := 'N';
                     l_err_flag := 'Y';
                     l_error_message :=
                        l_error_message || '-- Ship-To Site not in Oracle';
                  WHEN TOO_MANY_ROWS
                  THEN
                     l_err_flag := 'Y';
                     l_error_message :=
                           l_error_message
                        || '-- Multiple Oracle Sites have same PRISM Party Site1';
                  WHEN OTHERS
                  THEN
                     l_err_flag := 'Y';
                     l_error_message :=
                           l_error_message
                        || '-- Error deriving SHIP-TO Site details';
               END;
            ELSE
               -- IF rec_hdr.orig_system_bill_customer_id > 0

               ----------------------------------------------------------------------------------
               -- Derive Sold To Account Details
               ----------------------------------------------------------------------------------
               BEGIN
                  SELECT hca.cust_account_id
                    INTO l_cust_acct_id
                    FROM XXWC.XXWC_AHH_AR_CUST_CROSS_OVER_T xaac,
                         hz_cust_accounts hca
                   WHERE     xaac.CUST_NUM =
                                TRIM (TO_CHAR (rec_hdr.paying_customer_id))
                         AND TRIM (
                                REPLACE (
                                   REPLACE (xaac.oracle_cust_num,
                                            CHR (13),
                                            ''),
                                   CHR (10),
                                   '')) = hca.account_number
                         AND hca.status = 'A'
                         AND ROWNUM = 1;
               EXCEPTION
                  WHEN OTHERS
                  THEN
                     BEGIN
                        SELECT hca.cust_account_id
                          INTO l_cust_acct_id
                          FROM hz_cust_accounts hca,
                               HZ_CUST_ACCT_SITES_ALL hcas
                         WHERE     hca.cust_account_id = hcas.cust_account_id
                               AND NVL (hcas.attribute17, '123') =
                                      TRIM (
                                         TO_CHAR (rec_hdr.paying_customer_id))
                               AND hca.status = 'A'
                               AND hcas.org_id = 162
                               AND hca.attribute4 = 'AHH'
                               AND hcas.bill_to_flag = 'P';
                     EXCEPTION
                        WHEN NO_DATA_FOUND
                        THEN
                           BEGIN
                              SELECT CUST_ACCOUNT_ID
                                INTO l_cust_acct_id
                                FROM APPS.HZ_CUST_ACCT_SITES_ALL
                               WHERE NVL (ATTRIBUTE17, '123') =
                                        TRIM (
                                              TO_CHAR (
                                                 rec_hdr.paying_customer_id)
                                           || '-'
                                           || TO_CHAR (
                                                 rec_hdr.paying_site_use_id));
                           EXCEPTION
                              WHEN OTHERS
                              THEN
                                 l_err_flag := 'Y';
                                 l_error_message :=
                                       l_error_message
                                    || '-- Customer not available in Oracle3';
                           END;
                        WHEN OTHERS
                        THEN
                           l_err_flag := 'Y';
                           l_error_message :=
                                 l_error_message
                              || '-- Customer not available in Oracle4';
                     END;
               END;

               IF l_cust_acct_id IS NOT NULL
               THEN
                  ----------------------------------------------------------------------------------
                  -- Derive Ship-To Address using Prism PartySiteNumber - Validate Customer Site
                  ----------------------------------------------------------------------------------
                  BEGIN
                     SELECT hcas.cust_acct_site_id,
                            hcas.cust_account_id,
                            hcsu.bill_to_site_use_id,
                            hcas.party_site_id
                       INTO l_ship_to_address_id,
                            l_ship_to_cust_id,
                            l_bill_to_site_use_id,
                            l_party_site_id
                       FROM hz_cust_acct_sites_all hcas,
                            hz_cust_site_uses_all hcsu
                      WHERE     1 = 1
                            AND hcas.status = 'A'
                            AND hcsu.status = 'A'
                            --AND hcas.attribute17 = rec_hdr.legacy_party_site_number  -- by vamshi
                            AND hcas.attribute17 =
                                   TRIM (
                                         TO_CHAR (rec_hdr.paying_customer_id)
                                      || '-'
                                      || TO_CHAR (rec_hdr.paying_site_use_id))
                            AND hcas.cust_acct_site_id =
                                   hcsu.cust_acct_site_id
                            AND hcsu.site_use_code = 'SHIP_TO'
                            AND hcas.org_id = g_org_id
                            AND ROWNUM = 1;

                     l_ship_to_found := 'Y';
                  EXCEPTION
                     WHEN NO_DATA_FOUND
                     THEN
                        -- Check for the case with an appended Ship-To Site
                        BEGIN
                           SELECT hcas.cust_acct_site_id,
                                  hcas.cust_account_id,
                                  hcsu.bill_to_site_use_id,
                                  hcas.party_site_id
                             INTO l_ship_to_address_id,
                                  l_ship_to_cust_id,
                                  l_bill_to_site_use_id,
                                  l_party_site_id
                             FROM hz_cust_acct_sites_all hcas,
                                  hz_cust_site_uses_all hcsu
                            WHERE     1 = 1
                                  AND hcas.status = 'A'
                                  AND hcsu.status = 'A'
                                  AND SUBSTR (
                                         hcas.attribute17,
                                         1,
                                         INSTR (hcas.attribute17, '-') - 1) =
                                         rec_hdr.legacy_party_site_number
                                  AND hcas.cust_acct_site_id =
                                         hcsu.cust_acct_site_id
                                  AND hcsu.site_use_code = 'SHIP_TO'
                                  AND hcas.org_id = g_org_id
                                  AND ROWNUM = 1;


                           l_ship_to_found := 'Y';
                        EXCEPTION
                           WHEN NO_DATA_FOUND
                           THEN
                              l_ship_to_found := 'N';
                           WHEN TOO_MANY_ROWS
                           THEN
                              l_err_flag := 'Y';
                              l_error_message :=
                                    l_error_message
                                 || '-- Multiple Oracle Sites have same PRISM Party Site2';
                           WHEN OTHERS
                           THEN
                              l_err_flag := 'Y';
                              l_error_message :=
                                    l_error_message
                                 || '-- Error deriving SHIP-TO Site details';
                        END;
                     WHEN TOO_MANY_ROWS
                     THEN
                        l_err_flag := 'Y';
                        l_error_message :=
                              l_error_message
                           || '-- Multiple Oracle Sites have same PRISM Party Site3';
                     WHEN OTHERS
                     THEN
                        l_err_flag := 'Y';
                        l_error_message :=
                              l_error_message
                           || '-- Error deriving SHIP-TO Site details';
                  END;

                  ----------------------------------------------------------------------------------
                  -- Derive Ship-To Address using Prism PartySiteNumber - Validate Party Site
                  ----------------------------------------------------------------------------------
                  IF l_ship_to_found = 'N'
                  THEN
                     BEGIN
                        SELECT hcas.cust_acct_site_id,
                               hcas.cust_account_id,
                               hcsu.bill_to_site_use_id,
                               hcas.party_site_id
                          INTO l_ship_to_address_id,
                               l_ship_to_cust_id,
                               l_bill_to_site_use_id,
                               l_party_site_id
                          FROM hz_cust_acct_sites_all hcas,
                               hz_cust_site_uses_all hcsu,
                               hz_party_sites hps
                         WHERE     1 = 1
                               AND hcas.status = 'A'
                               AND hcsu.status = 'A'
                               AND hps.status = 'A'
                               --AND hps.party_site_number = rec_hdr.legacy_party_site_number -- Vamshi
                               AND hcas.attribute17 =
                                      TRIM (
                                            TO_CHAR (
                                               rec_hdr.paying_customer_id)
                                         || '-'
                                         || TO_CHAR (
                                               rec_hdr.paying_site_use_id))
                               AND hcas.cust_acct_site_id =
                                      hcsu.cust_acct_site_id
                               AND hcas.party_site_id = hps.party_site_id
                               AND hcsu.site_use_code = 'SHIP_TO'
                               AND hcas.org_id = g_org_id
                               AND ROWNUM = 1;

                        l_ship_to_found := 'Y';
                     EXCEPTION
                        WHEN NO_DATA_FOUND
                        THEN
                           -- Check for the case with an appended Ship-To Site
                           BEGIN
                              SELECT hcas.cust_acct_site_id,
                                     hcas.cust_account_id,
                                     hcsu.bill_to_site_use_id,
                                     hcas.party_site_id
                                INTO l_ship_to_address_id,
                                     l_ship_to_cust_id,
                                     l_bill_to_site_use_id,
                                     l_party_site_id
                                FROM hz_cust_acct_sites_all hcas,
                                     hz_cust_site_uses_all hcsu,
                                     hz_party_sites hps
                               WHERE     1 = 1
                                     AND hcas.status = 'A'
                                     AND hcsu.status = 'A'
                                     AND hps.status = 'A'
                                     AND SUBSTR (
                                            hcas.attribute17,
                                            1,
                                              INSTR (hcas.attribute17,
                                                     '-',
                                                     1)
                                            - 1) =
                                            TRIM (
                                               TO_CHAR (
                                                  rec_hdr.paying_customer_id))
                                     AND hcas.cust_acct_site_id =
                                            hcsu.cust_acct_site_id
                                     AND hcas.party_site_id =
                                            hps.party_site_id
                                     AND hcsu.site_use_code = 'SHIP_TO'
                                     AND hcas.org_id = g_org_id
                                     AND ROWNUM = 1;

                              l_ship_to_found := 'Y';
                           EXCEPTION
                              WHEN NO_DATA_FOUND
                              THEN
                                 l_err_flag := 'Y';
                                 l_error_message :=
                                       l_error_message
                                    || '-- PRISM Party Site not in Oracle1';
                              WHEN TOO_MANY_ROWS
                              THEN
                                 l_err_flag := 'Y';
                                 l_error_message :=
                                       l_error_message
                                    || '-- Multiple Oracle Sites have same PRISM Party Site4';
                              WHEN OTHERS
                              THEN
                                 l_err_flag := 'Y';
                                 l_error_message :=
                                       l_error_message
                                    || '-- Error deriving SHIP-TO Site details';
                           END;
                        WHEN TOO_MANY_ROWS
                        THEN
                           l_err_flag := 'Y';
                           l_error_message :=
                                 l_error_message
                              || '-- Multiple Oracle Sites have same PRISM Party Site5';
                        WHEN OTHERS
                        THEN
                           l_err_flag := 'Y';
                           l_error_message :=
                                 l_error_message
                              || '-- Error deriving SHIP-TO Site details';
                     END;
                  END IF;
               END IF;                          -- l_cust_acct_id  IS NOT NULL
            END IF;             -- IF rec_hdr.orig_system_bill_customer_id > 0

            IF l_cust_acct_id IS NOT NULL
            THEN
               -- l_cust_acct_id  IS NOT NULL
               -- OE SHIP-TO LOGIC < END

               fnd_file.put_line (fnd_file.LOG,
                                  ' END -- OE SHIP-TO LOGIC < END ');

               -------------------------------------------------------------------------------
               -- Deriving ShipTo State
               -------------------------------------------------------------------------------
               BEGIN
                  SELECT hl.state
                    INTO l_ship_to_state
                    FROM hz_party_sites hps, hz_locations hl
                   WHERE     1 = 1
                         AND hps.status = 'A'
                         AND hps.party_site_id = l_party_site_id
                         AND hps.location_id = hl.location_id;
               EXCEPTION
                  WHEN OTHERS
                  THEN
                     l_err_flag := 'Y';
                     l_error_message :=
                        l_error_message || '-- Error deriving SHIP-TO State';
               END;

               -------------------------------------------------------------------------------
               -- Derive Tax Account
               -------------------------------------------------------------------------------
               l_tax_acct := NULL;

               BEGIN
                  SELECT description
                    INTO l_tax_acct
                    FROM ar_lookups
                   WHERE     1 = 1
                         AND lookup_type = 'XXWC_TAX_ACCOUNT'
                         AND enabled_flag = 'Y'
                         AND SYSDATE BETWEEN start_date_active
                                         AND NVL (end_date_active,
                                                  SYSDATE + 1)
                         AND lookup_code = l_ship_to_state;

                  fnd_file.put_line (fnd_file.LOG,
                                     'l_tax_acct - ' || l_tax_acct);
               EXCEPTION
                  WHEN NO_DATA_FOUND
                  THEN
                     l_tax_acct := g_dflt_tax_acct;
                  WHEN OTHERS
                  THEN
                     l_err_flag := 'Y';
                     l_error_message :=
                           l_error_message
                        || 'Error deriving Tax Account for the state - '
                        || l_ship_to_state;
               END;

               fnd_file.put_line (
                  fnd_file.LOG,
                  'l_bill_to_site_use_id - ' || l_bill_to_site_use_id);

               BEGIN
                  IF l_bill_to_site_use_id IS NOT NULL
                  THEN
                     ----------------------------------------------------------------------------------
                     -- Derive Bill-To Address details using BillTo SiteUseId associated with ShipTo
                     ----------------------------------------------------------------------------------
                     SELECT hcas.cust_acct_site_id, hcas.cust_account_id
                       INTO l_bill_to_address_id, l_bill_to_cust_id
                       FROM hz_cust_acct_sites_all hcas,
                            hz_cust_site_uses_all hcsu
                      WHERE     1 = 1
                            AND hcas.status = 'A'
                            AND hcsu.status = 'A'
                            AND hcsu.site_use_id = l_bill_to_site_use_id
                            AND hcas.cust_acct_site_id =
                                   hcsu.cust_acct_site_id
                            AND hcsu.site_use_code = 'BILL_TO'
                            AND hcas.org_id = g_org_id;
                  ELSE
                     ---------------------------------------
                     -- Derive Primary Bill-To Site
                     ---------------------------------------
                     SELECT hcas.cust_acct_site_id, hcas.cust_account_id
                       INTO l_bill_to_address_id, l_bill_to_cust_id
                       FROM hz_cust_acct_sites_all hcas
                      WHERE     1 = 1
                            AND hcas.status = 'A'
                            AND hcas.cust_account_id = l_cust_acct_id
                            AND NVL (hcas.bill_to_flag, 'N') = 'P'
                            AND hcas.org_id = g_org_id;
                  END IF;
               EXCEPTION
                  WHEN NO_DATA_FOUND
                  THEN
                     ---------------------------------------
                     -- Derive Primary Bill-To Site
                     ---------------------------------------
                     BEGIN
                        SELECT hcas.cust_acct_site_id, hcas.cust_account_id
                          INTO l_bill_to_address_id, l_bill_to_cust_id
                          FROM hz_cust_acct_sites_all hcas
                         WHERE     1 = 1
                               AND hcas.status = 'A'
                               AND hcas.cust_account_id = l_cust_acct_id
                               AND NVL (hcas.bill_to_flag, 'N') = 'P'
                               AND hcas.org_id = g_org_id;
                     EXCEPTION
                        WHEN OTHERS
                        THEN
                           l_err_flag := 'Y';
                           l_error_message :=
                                 l_error_message
                              || '-- Customer has no BILL-TO Site';
                     END;
                  WHEN OTHERS
                  THEN
                     l_err_flag := 'Y';
                     l_error_message :=
                        l_error_message || '-- Customer has no BILL-TO Site';
               END;
            END IF;                             -- l_cust_acct_id  IS NOT NULL

            ----------------------------------------------------------------------------------
            -- Derive Salesrep Details
            ----------------------------------------------------------------------------------
            BEGIN
               SELECT A.SALESREP_ID
                 INTO l_salesrep_id
                 FROM RA_SALESREPS_ALL A, XXWC.XXWC_AHH_AM_T B
                WHERE     A.ORG_ID = g_org_id
                      AND UPPER (TRIM (B.SLSREPOUT)) =
                             UPPER (TRIM (rec_hdr.primary_salesrep_number))
                      AND A.SALESREP_NUMBER = B.SALESREP_ID
                      AND ROWNUM = 1;
            EXCEPTION
               WHEN OTHERS
               THEN
                  BEGIN
                     SELECT salesrep_id
                       INTO l_salesrep_id
                       FROM ra_salesreps_all
                      WHERE     NAME = 'No Sales Credit'
                            AND status = 'A'
                            AND SYSDATE BETWEEN start_date_active
                                            AND NVL (end_date_active,
                                                     SYSDATE + 1)
                            AND org_id = g_org_id;
                  EXCEPTION
                     WHEN OTHERS
                     THEN
                        l_error_message :=
                           l_error_message || '--Defaulting Sales Rep';
                  END;
            END;

            ----------------------------------------------------------------------------------
            -- Validate for Duplicate Invoice Number
            ----------------------------------------------------------------------------------
            BEGIN
               SELECT trx_number
                 INTO l_dup_trx_val_flag
                 FROM ra_customer_trx_all
                WHERE     1 = 1
                      AND interface_header_context = g_context
                      AND trx_number = rec_hdr.trx_number
                      AND BATCH_SOURCE_ID = ln_batch_source_id
                      AND org_id = g_org_id;

               l_err_flag := 'Y';
               l_error_message :=
                  l_error_message || '-- Duplicate Invoice Number';
            EXCEPTION
               WHEN OTHERS
               THEN
                  NULL;
            END;

            ----------------------------------------------------------------------------------
            -- Derive revenue account id
            ----------------------------------------------------------------------------------
            BEGIN
               SELECT ORACLE_BW_NUMBER
                 INTO l_branch_code
                 FROM XXWC.XXWC_AP_BRANCH_LIST_STG_TBL
                WHERE UPPER (AHH_WAREHOUSE_NUMBER) =
                         UPPER (rec_hdr.warehouse_id);

               FND_FILE.PUT_LINE (FND_FILE.LOG,
                                  'l_branch_code:' || l_branch_code);
            /*
            SELECT entrp_loc
              INTO l_branch_code
              FROM xxcus_location_code_vw
             WHERE     inactive = 'N'
                   AND entrp_entity = '0W'
                   AND LTRIM (lob_branch, '0') =
                          LTRIM (TO_CHAR (rec_hdr.warehouse_id), '0');
            */
            EXCEPTION
               WHEN TOO_MANY_ROWS
               THEN
                  l_err_flag := 'Y';
                  l_error_message :=
                        l_error_message
                     || '-- Too many rows returned during Branch Code Validation';
               WHEN OTHERS
               THEN
                  l_err_flag := 'Y';
                  l_error_message :=
                        l_error_message
                     || '-- Branch Code not defined for : '
                     || rec_hdr.warehouse_id;
            END;

            ----------------------------------------------------------------------------------
            -- Derive Inventory Org Id
            ----------------------------------------------------------------------------------
            BEGIN
               SELECT organization_id
                 INTO l_inv_org_id
                 FROM org_organization_definitions ood
                WHERE organization_code = LPAD (rec_hdr.warehouse_id, 3, '0');
            EXCEPTION
               WHEN OTHERS
               THEN
                  l_inv_org_id := NULL;
            END;

            ----------------------------------------------------------------------------------
            -- Derive Reason Code
            ----------------------------------------------------------------------------------
            lvc_reason_code := NULL;
            /*
             IF REC_HDR.REASON_CODE IS NOT NULL
             THEN
                BEGIN
                   SELECT oracle_return_reason
                     INTO lvc_reason_code
                     FROM XXWC.XXWC_RETURN_REASON_TBL
                    WHERE description = REC_HDR.REASON_CODE;
                EXCEPTION
                   WHEN TOO_MANY_ROWS
                   THEN
                      l_err_flag := 'Y';
                      l_error_message :=
                            l_error_message
                         || '-- Too many rows returned during Reason code Validation';
                   WHEN OTHERS
                   THEN
                      l_err_flag := 'Y';
                      l_error_message :=
                            l_error_message
                         || '-- Reason Code not defined in reference table for : '
                         || REC_HDR.REASON_CODE;
                END;
             END IF;
             */

            ----------------------------------------------------------------------------------
            -- Derive Ship Via
            ----------------------------------------------------------------------------------
            lvc_ship_via := NULL;

            IF REC_HDR.SHIP_VIA IS NOT NULL
            THEN
               BEGIN
                  SELECT ORACLE_SHIP_METHOD_CODE
                    INTO lvc_ship_via
                    FROM XXWC.XXWC_SHIP_VIA_TBL
                   WHERE UPPER (description) = UPPER (REC_HDR.SHIP_VIA);
               EXCEPTION
                  WHEN NO_DATA_FOUND
                  THEN
                     SELECT ORACLE_SHIP_METHOD_CODE
                       INTO lvc_ship_via
                       FROM XXWC.XXWC_SHIP_VIA_TBL
                      WHERE UPPER (description) = 'DEFAULT';
                  WHEN TOO_MANY_ROWS
                  THEN
                     l_err_flag := 'Y';
                     l_error_message :=
                           l_error_message
                        || '-- Too many rows returned during Ship_via code Validation';
                  WHEN OTHERS
                  THEN
                     l_err_flag := 'Y';
                     l_error_message :=
                           l_error_message
                        || '-- Ship Via not defined in reference table for : '
                        || REC_HDR.ship_via;
               END;
            END IF;

            ----------------------------------------------------------------------------------
            -- Derive PaymentTerm Name
            ----------------------------------------------------------------------------------
            IF l_trx_name = 'Credit Memo'
            THEN
               l_terms_id := NULL;
            ELSE
                -------------------
 
                 BEGIN
                     SELECT TERM_NAME
                      INTO l_terms_name
                      FROM XXWC.XXWC_AHH_PAYMENT_TERMS_T
                     WHERE UPPER (AHH_TERM_NAME) = TRIM (UPPER (rec_hdr.term_name))
					 AND ROWNUM=1;

                    SELECT term_id
                      INTO l_terms_id
                      FROM ra_terms rt
                     WHERE     TRIM (UPPER (name)) = TRIM (UPPER (l_terms_name))
                         AND SYSDATE BETWEEN NVL (rt.start_date_active, SYSDATE -1)
                                         AND NVL (rt.end_date_active,
                                                  SYSDATE + 1)
                           AND ROWNUM = 1;
                 EXCEPTION
                    WHEN OTHERS
                    THEN
                     l_err_flag := 'Y';
                     l_error_message :=
                           l_error_message
                        || '-- Invalid Payment Term';
                 END;
                
                -------------------
/*
               SELECT rt.term_id
                    INTO l_terms_id
                    FROM xxwc_payment_terms_xref xpt, apps.ra_terms rt
                   WHERE     UPPER (xpt.prism_description) =
                                TRIM (UPPER (rec_hdr.term_name))
                         AND TRIM (UPPER (rt.NAME)) =
                                UPPER (TRIM (xpt.oracle_term))
                         AND SYSDATE BETWEEN rt.start_date_active
                                         AND NVL (rt.end_date_active,
                                                  SYSDATE + 1)
                         AND ROWNUM = 1;
                         */
 
            END IF;

            -- Check for Header Freight (No Invoice Lines will have Tax Amount)
            IF NVL (rec_hdr.stg_attribute2, 0) != 0
            THEN
               l_zero_tax_line_cnt := 0;

               BEGIN
                  SELECT COUNT (1)
                    INTO l_zero_tax_line_cnt
                    FROM xxwc.xxwc_ar_inv_stg_tbl
                   WHERE     1 = 1
                         AND trx_number = rec_hdr.trx_number
                         AND tax_amount != 0;
               EXCEPTION
                  WHEN OTHERS
                  THEN
                     l_err_flag := 'Y';
                     l_error_message :=
                           l_error_message
                        || '-- Error deriving Header Freight only.';
               END;
            END IF;

            l_inv_line_num := 1;

            FOR rec_line IN cur_line (l_inv_num, l_trx_type_id)
            LOOP
               -- rec_line loop start>
               l_inv_item_id := NULL;
               l_rev_ccid := NULL;
               l_rec_ccid := NULL;
               l_uom_code := NULL;
               l_uom_name := NULL;
               l_direct_acct := NULL;
               l_rental_acct := NULL;
               l_rev_acct := NULL;
               l_int_comp_acct := NULL;
               l_intan_rev_acct := NULL;                    --Added by Vamshi.

               fnd_file.put_line (
                  fnd_file.LOG,
                  '************ l_inv_line_num *** ' || l_inv_line_num);

               ----------------------------------------------------------------------------------
               -- If line# is 1 then derive the Tax Difference
               ----------------------------------------------------------------------------------
               IF l_inv_line_num = 1
               THEN
                  l_line_tax_amt := 0;
                  l_diff_tax_amt := 0;
                  l_sum_tax_amt := 0;
                  l_frt_tax_amt := 0;

                  BEGIN
                     SELECT SUM (NVL (tax_amount, 0))
                       INTO l_sum_tax_amt
                       FROM xxwc.xxwc_ar_inv_stg_tbl stg
                      WHERE trx_number = rec_line.trx_number -- AND freight_amount IS NULL
                                                            ;

                     fnd_file.put_line (fnd_file.LOG,
                                        'l_sum_tax_amt - ' || l_sum_tax_amt);
                  EXCEPTION
                     WHEN OTHERS
                     THEN
                        l_sum_tax_amt := TO_NUMBER (rec_line.stg_attribute2);
                  END;

                  l_diff_tax_amt :=
                     (TO_NUMBER (rec_line.stg_attribute2) - l_sum_tax_amt);

                  ----------------------------------------------------------------------------------
                  -- Check if freight line exists
                  ----------------------------------------------------------------------------------
                  l_frt_line_exists := 'N';

                  FOR rec_freight IN cur_freight (l_inv_num, l_trx_type_id)
                  LOOP
                     l_frt_line_exists := 'Y';
                  END LOOP;

                  fnd_file.put_line (
                     fnd_file.LOG,
                     'l_frt_line_exists -  ' || l_frt_line_exists);
               END IF;                                -- IF l_inv_line_num = 1

               fnd_file.put_line (fnd_file.LOG,
                                  'l_diff_tax_amt -  ' || l_diff_tax_amt);

               ----------------------------------------------------------------------------------
               -- Derive Inventory Item Id
               ----------------------------------------------------------------------------------
               fnd_file.put_line (
                  fnd_file.LOG,
                     'rec_line.inventory_item_num - '
                  || rec_line.inventory_item_num);
               fnd_file.put_line (fnd_file.LOG,
                                  'l_inv_org_id - ' || l_inv_org_id);

               IF     rec_line.inventory_item_num IS NOT NULL
                  AND l_inv_org_id IS NOT NULL
               THEN
                  BEGIN
                     SELECT inventory_item_id,
                            primary_uom_code,
                            primary_unit_of_measure
                       INTO l_inv_item_id, l_uom_code, l_uom_name
                       FROM mtl_system_items_b msib
                      WHERE     organization_id = l_inv_org_id
                            AND enabled_flag = 'Y'
                            AND segment1 = rec_line.inventory_item_num;

                     fnd_file.put_line (fnd_file.LOG,
                                        'l_inv_item_id - ' || l_inv_item_id);
                  EXCEPTION
                     WHEN NO_DATA_FOUND
                     THEN
                        -- Use memo line if item is not defined in Oracle
                        l_inv_item_id := NULL;
                        l_uom_code := NULL;
                        l_uom_name := NULL;
                     WHEN OTHERS
                     THEN
                        fnd_file.put_line (fnd_file.LOG,
                                           'Error - ' || SQLERRM);
                        l_inv_item_id := NULL;
                        l_uom_code := NULL;
                        l_uom_name := NULL;
                  END;
               END IF;

               l_description := NVL(rec_line.item_description,'SP/'||rec_line.inventory_item_num);

               fnd_file.put_line (
                  fnd_file.LOG,
                  'rec_line.item_description:' || rec_line.item_description);

               fnd_file.put_line (fnd_file.LOG,
                                  'l_branch_code:' || l_branch_code);

               IF l_branch_code IS NOT NULL
               THEN
                  BEGIN
                     ----------------------------------------------------------------------------------
                     -- Check if the Customer is an Intercompany Customer
                     ----------------------------------------------------------------------------------
                     SELECT description
                       INTO l_int_comp_acct
                       FROM ar_lookups
                      WHERE     lookup_type = 'XXWC_INTERCOMPANY_ACCOUNT'
                            AND enabled_flag = 'Y'
                            AND SYSDATE BETWEEN start_date_active
                                            AND NVL (end_date_active,
                                                     SYSDATE + 1)
                            AND lookup_code =
                                   TO_CHAR (rec_hdr.paying_customer_id);
                  EXCEPTION
                     WHEN OTHERS
                     THEN
                        l_int_comp_acct := NULL;
                  END;

                  IF l_int_comp_acct IS NOT NULL
                  THEN
                     l_rev_acct := l_int_comp_acct;
                  END IF;

                  IF l_int_comp_acct IS NULL
                  THEN
                     ----------------------------------------------------------------------------------
                     -- Derive Revenue Account
                     ----------------------------------------------------------------------------------
                     BEGIN
                        SELECT externally_visible_flag, description
                          INTO l_direct_acct, l_rental_acct
                          FROM ar_lookups
                         WHERE     lookup_type = 'XXWC_REVENUE_ACCOUNT'
                               AND lookup_code =
                                      RPAD (TRIM (rec_line.category_class),
                                            4,
                                            '0')
                               AND SYSDATE BETWEEN start_date_active
                                               AND NVL (end_date_active,
                                                        SYSDATE + 1)
                               AND NVL (enabled_flag, 'Y') = 'Y';
                     EXCEPTION
                        WHEN NO_DATA_FOUND
                        THEN
                           ----------------------------------------------------------------------------------
                           -- Use default Category Class - 'DFLT'
                           ----------------------------------------------------------------------------------
                           BEGIN
                              SELECT externally_visible_flag, description
                                INTO l_direct_acct, l_rental_acct
                                FROM ar_lookups
                               WHERE     lookup_type = 'XXWC_REVENUE_ACCOUNT'
                                     AND lookup_code = g_dflt_cat_class
                                     AND SYSDATE BETWEEN start_date_active
                                                     AND NVL (
                                                            end_date_active,
                                                            SYSDATE + 1)
                                     AND NVL (enabled_flag, 'Y') = 'Y';
                           EXCEPTION
                              WHEN NO_DATA_FOUND
                              THEN
                                 l_err_flag := 'Y';
                                 l_error_message :=
                                       l_error_message
                                    || '-- DFLT Category Class not defined in Oracle';
                              WHEN OTHERS
                              THEN
                                 l_err_flag := 'Y';
                                 l_error_message :=
                                       l_error_message
                                    || '-- Error deriving mapping for Category Class - DFLT';
                           END;
                        WHEN OTHERS
                        THEN
                           l_err_flag := 'Y';
                           l_error_message :=
                                 l_error_message
                              || '-- Error deriving mapping for Category Class - '
                              || rec_line.category_class;
                     END;

                     IF rec_line.direct_flag = 1
                     THEN
                        l_rev_acct := l_direct_acct;
                     ELSE
                        l_rev_acct := l_rental_acct;
                     END IF;
                  END IF;                        -- IF l_int_comp_acct IS NULL
               ELSE
                  fnd_file.put_line (fnd_file.LOG, 'Branch Code is null. ');
                  l_err_flag := 'Y';
                  l_error_message :=
                     l_error_message || '-- Branch Code is null.';
               END IF;                               -- if branch_code IS NULL

               -- Added below code by Vamshi. - Begin
               ---------------------------------------------------------------------
               -- Deriving Intangible item Reveue Account
               ---------------------------------------------------------------------
               BEGIN
                  l_intan_rev_acct := NULL;

                  SELECT TAG
                    INTO l_intan_rev_acct
                    FROM APPS.FND_LOOKUP_VALUES
                   WHERE     LOOKUP_TYPE = 'XXWC_INTANGIBLES_GL_VALUES'
                         AND ENABLED_FLAG = 'Y'
                         AND DESCRIPTION = rec_line.item_description
                         AND SYSDATE BETWEEN NVL (START_DATE_ACTIVE,
                                                  SYSDATE - 1)
                                         AND NVL (END_DATE_ACTIVE,
                                                  SYSDATE + 1)
                         AND tag IS NOT NULL;
               EXCEPTION
                  WHEN OTHERS
                  THEN
                     l_intan_rev_acct := NULL;
               END;

               FND_FILE.PUT_LINE (FND_FILE.LOG,
                                  'l_intan_rev_acct: ' || l_intan_rev_acct);
               -- Added above code by Vamshi. -End


               fnd_file.put_line (
                  fnd_file.LOG,
                     ' B4 inserting into interface table l_err_flag -'
                  || l_err_flag);


               fnd_file.put_line (
                  fnd_file.LOG,
                     ' B4 inserting into interface table l_err_flag -'
                  || l_err_flag);


               IF NVL (l_err_flag, 'N') = 'N'
               THEN
                  IF 1 = 1                    --   rec_hdr.inv_type_flag = 'I'
                          AND NVL (rec_line.freight_amount, 0) = 0 --changed by  vamshi
                  THEN
                     fnd_file.put_line (fnd_file.LOG, ' insert 1');

                     BEGIN
                        INSERT
                          INTO ra_interface_lines_all (
                                  interface_line_context,
                                  interface_line_attribute1,
                                  interface_line_attribute2,
                                  interface_line_attribute3,
                                  interface_line_attribute4,
                                  interface_line_attribute5,
                                  interface_line_attribute6,
                                  interface_line_attribute7,
                                  interface_line_attribute8,
                                  interface_line_attribute9,
                                  interface_line_attribute10,
                                  interface_line_attribute11,
                                  interface_line_attribute12,
                                  interface_line_attribute13,
                                  interface_line_attribute15,
                                  batch_source_name,
                                  line_type,
                                  description,
                                  currency_code,
                                  amount,
                                  cust_trx_type_id,
                                  trx_date,
                                  quantity,
                                  warehouse_id,
                                  purchase_order,
                                  sales_order,
                                  sales_order_date,
                                  primary_salesrep_id,
                                  trx_number,
                                  term_id,
                                  org_id,
                                  gl_date,
                                  uom_code,
                                  uom_name,
                                  line_number,
                                  inventory_item_id,
                                  paying_customer_id,
                                  orig_system_bill_customer_id,
                                  orig_system_ship_customer_id,
                                  orig_system_sold_customer_id,
                                  orig_system_bill_address_id,
                                  orig_system_ship_address_id,
                                  conversion_type,
                                  conversion_rate,
                                  amount_includes_tax_flag,
                                  tax_code,
                                  tax_rate_code,
                                  tax_jurisdiction_code,
                                  tax_status_code,
                                  tax,
                                  tax_regime_code,
                                  creation_date,
                                  created_by,
                                  last_update_date,
                                  last_updated_by,
                                  reason_code)
                        VALUES (g_context,
                                rec_line.trx_number -- interface_line_attribute1
                                                   ,
                                rec_line.quantity_ordered -- interface_line_attribute2
                                                         ,
                                rec_line.quantity_back_ordered -- interface_line_attribute3
                                                              ,
                                rec_line.inventory_item_num -- interface_line_attribute4
                                                           ,
                                rec_line.revenue_amount -- interface_line_attribute5
                                                       ,
                                'LINE_' || l_inv_line_num -- interface_line_attribute6
                                                         ,
                                rec_line.warehouse_id -- interface_line_attribute7
                                                     ,
                                rec_line.prism_inv_type -- interface_line_attribute8
                                                       ,
                                rec_line.ordered_by -- interface_line_attribute9
                                                   ,
                                rec_line.taken_by -- interface_line_attribute10
                                                 ,
                                rec_line.received_by -- interface_line_attribute11
                                                    ,
                                -- rec_line.ship_via -- interface_line_attribute12
                                lvc_ship_via,
                                rec_line.line_number -- interface_line_attribute13
                                                    ,
                                rec_line.sales_order -- interface_line_attribute15
                                                    ,
                                g_batch_source            -- batch_source_name
                                              ,
                                'LINE'                            -- line_type
                                      ,
                                l_description                   -- description
                                             ,
                                g_currency                    -- currency_code
                                          ,
                                (rec_line.revenue_amount)            -- amount
                                                         ,
                                l_cus_trx_type_id          -- cust_trx_type_id
                                                 ,
                                rec_line.trx_date                  -- trx_date
                                                 ,
                                rec_line.quantity_shipped          -- quantity
                                                         ,
                                l_inv_org_id                   -- warehouse_id
                                            ,
                                rec_line.purchase_order      -- purchase_order
                                                       ,
                                rec_line.sales_order            -- sales_order
                                                    ,
                                rec_line.sales_order_date  -- sales_order_date
                                                         ,
                                l_salesrep_id           -- primary_salesrep_id
                                             ,
                                rec_line.trx_number              -- trx_number
                                                   ,
                                l_terms_id                          -- term_id
                                          ,
                                g_org_id                             -- org_id
                                        ,
                                rec_line.creation_date              -- gl_date
                                                      ,
                                l_uom_code                         -- uom_code
                                          ,
                                l_uom_name                         -- uom_name
                                          ,
                                l_inv_line_num                  -- line_number
                                              ,
                                l_inv_item_id             -- inventory_item_id
                                             ,
                                l_cust_acct_id           -- paying_customer_id
                                              ,
                                l_bill_to_cust_id -- orig_system_bill_customer_id
                                                 ,
                                l_ship_to_cust_id -- orig_system_ship_customer_id
                                                 ,
                                l_cust_acct_id -- orig_system_sold_customer_id
                                              ,
                                l_bill_to_address_id -- orig_system_bill_address_id
                                                    ,
                                l_ship_to_address_id -- orig_system_ship_address_id
                                                    ,
                                g_conversion_type           -- conversion_type
                                                 ,
                                g_conversion_rate           -- conversion_rate
                                                 ,
                                'N'                -- amount_includes_tax_flag
                                   ,
                                'HDS-PRISM'                        -- tax_code
                                           ,
                                'HDS-PRISM'                   -- tax_rate_code
                                           ,
                                'HDS-PRISM'           -- tax_jurisdiction_code
                                           ,
                                'HDS-PRISM'                 -- tax_status_code
                                           ,
                                'HDS-PRISM'                             -- tax
                                           ,
                                'SALES TAX REGIME'          -- tax_regime_code
                                                  ,
                                SYSDATE                       -- creation_date
                                       ,
                                -1                               -- created_by
                                  ,
                                SYSDATE                    -- last_update_date
                                       ,
                                -1                          -- last_updated_by
                                  ,
                                lvc_reason_code);
                     EXCEPTION
                        WHEN OTHERS
                        THEN
                           l_err_flag := 'Y';
                           fnd_file.put_line (
                              fnd_file.LOG,
                                 'Error inserting ITEM record into RA_INTERFACE_LINES_ALL1 '
                              || SQLERRM);
                     END;


                     ----------------------------------------------
                     -- TAX Lines and Distribution
                     ----------------------------------------------

                     --------------------------------------------------------------------------------------------
                     -- If Freight Line exists and TaxDifference is less than or equal to 1, then add the
                     -- TaxDifference to first Item Tax Line.
                     -- *
                     -- If Freight Line exists and TaxDifference is greater than 1, then add the TaxDifference to
                     -- Freight Tax Line
                     -- *
                     -- If Freight Line does not exist then add the TaxDifference to Freight Line
                     --------------------------------------------------------------------------------------------

                     l_line_tax_amt := rec_line.tax_amount;

                     fnd_file.put_line (fnd_file.LOG,
                                        '================== B4 ');
                     fnd_file.put_line (
                        fnd_file.LOG,
                        'l_diff_tax_amt - ' || l_diff_tax_amt);
                     fnd_file.put_line (
                        fnd_file.LOG,
                        'l_frt_line_exists - ' || l_frt_line_exists);
                     fnd_file.put_line (
                        fnd_file.LOG,
                        'l_line_tax_amt - ' || l_line_tax_amt);

                     IF l_diff_tax_amt != 0
                     THEN
                        IF l_frt_line_exists = 'Y'
                        THEN
                           IF ABS (l_diff_tax_amt) <= 1
                           THEN
                              IF    l_line_tax_amt != 0
                                 OR (    l_zero_tax_line_cnt = 0
                                     AND rec_line.item_description =
                                            'HEADER FREIGHT')
                              THEN
                                 l_line_tax_amt :=
                                    l_line_tax_amt + l_diff_tax_amt;
                                 l_diff_tax_amt := 0;
                              END IF;                   -- l_line_tax_amt != 0
                           END IF;                 -- ABS(l_diff_tax_amt) <= 1
                        ELSE
                           -- l_frt_line_exists = 'Y'
                           IF    l_line_tax_amt != 0
                              OR (    l_zero_tax_line_cnt = 0
                                  AND rec_line.item_description =
                                         'HEADER FREIGHT')
                           THEN
                              l_line_tax_amt :=
                                 l_line_tax_amt + l_diff_tax_amt;
                              l_diff_tax_amt := 0;
                           END IF;                      -- l_line_tax_amt != 0
                        END IF;                     -- l_frt_line_exists = 'Y'
                     END IF;                            -- l_diff_tax_amt != 0

                     fnd_file.put_line (fnd_file.LOG,
                                        '================== AFTER ');
                     fnd_file.put_line (
                        fnd_file.LOG,
                        'l_diff_tax_amt - ' || l_diff_tax_amt);
                     fnd_file.put_line (
                        fnd_file.LOG,
                        'l_frt_line_exists - ' || l_frt_line_exists);
                     fnd_file.put_line (
                        fnd_file.LOG,
                        'l_line_tax_amt - ' || l_line_tax_amt);

                     IF l_line_tax_amt IS NULL
                     THEN
                        l_line_tax_amt := 0;
                     END IF;

                     BEGIN
                        IF NVL (l_err_flag, 'N') = 'N'
                        THEN
                           --if NVL(l_line_tax_amt,0)>0 THEN -- Added by Vamshi
                           fnd_file.put_line (fnd_file.LOG, ' insert 2');

                           INSERT
                             INTO ra_interface_lines_all (
                                     interface_line_context,
                                     interface_line_attribute1,
                                     interface_line_attribute2,
                                     interface_line_attribute3,
                                     interface_line_attribute4,
                                     interface_line_attribute5,
                                     interface_line_attribute6,
                                     interface_line_attribute7,
                                     interface_line_attribute8,
                                     interface_line_attribute9,
                                     interface_line_attribute10,
                                     interface_line_attribute11,
                                     interface_line_attribute12,
                                     interface_line_attribute13,
                                     interface_line_attribute15,
                                     batch_source_name,
                                     line_type,
                                     description,
                                     currency_code,
                                     amount,
                                     cust_trx_type_id,
                                     trx_date,
                                     quantity,
                                     warehouse_id,
                                     purchase_order,
                                     sales_order,
                                     sales_order_date,
                                     primary_salesrep_id,
                                     trx_number,
                                     term_id,
                                     org_id,
                                     gl_date,
                                     uom_code,
                                     uom_name,
                                     line_number,
                                     inventory_item_id,
                                     paying_customer_id,
                                     orig_system_bill_customer_id,
                                     orig_system_ship_customer_id,
                                     orig_system_sold_customer_id,
                                     orig_system_bill_address_id,
                                     orig_system_ship_address_id,
                                     conversion_type,
                                     conversion_rate,
                                     amount_includes_tax_flag,
                                     link_to_line_context,
                                     link_to_line_attribute1,
                                     link_to_line_attribute2,
                                     link_to_line_attribute3,
                                     link_to_line_attribute4,
                                     link_to_line_attribute5,
                                     link_to_line_attribute6,
                                     tax_code,
                                     tax_rate_code,
                                     tax_jurisdiction_code,
                                     tax_status_code,
                                     tax,
                                     tax_regime_code,
                                     creation_date,
                                     created_by,
                                     last_update_date,
                                     last_updated_by,
                                     reason_code)
                           VALUES (g_context,
                                   rec_line.trx_number -- interface_line_attribute1
                                                      ,
                                   rec_line.quantity_ordered -- interface_line_attribute2
                                                            ,
                                   rec_line.quantity_back_ordered -- interface_line_attribute3
                                                                 ,
                                   rec_line.inventory_item_num -- interface_line_attribute4
                                                              ,
                                   l_line_tax_amt -- interface_line_attribute5
                                                 ,
                                   'TAX_' || l_inv_line_num -- interface_line_attribute6
                                                           ,
                                   rec_line.warehouse_id -- interface_line_attribute7
                                                        ,
                                   rec_line.prism_inv_type -- interface_line_attribute8
                                                          ,
                                   rec_line.ordered_by -- interface_line_attribute9
                                                      ,
                                   rec_line.taken_by -- interface_line_attribute10
                                                    ,
                                   rec_line.received_by -- interface_line_attribute11
                                                       ,
                                   rec_line.ship_via -- interface_line_attribute12
                                                    ,
                                   rec_line.line_number -- interface_line_attribute13
                                                       ,
                                   rec_line.sales_order -- interface_line_attribute15
                                                       ,
                                   g_batch_source         -- batch_source_name
                                                 ,
                                   'TAX'                          -- line_type
                                        ,
                                   l_description                -- description
                                                ,
                                   g_currency                 -- currency_code
                                             ,
                                   l_line_tax_amt                    -- amount
                                                 ,
                                   l_cus_trx_type_id       -- cust_trx_type_id
                                                    ,
                                   rec_line.trx_date               -- trx_date
                                                    ,
                                   rec_line.quantity_shipped       -- quantity
                                                            ,
                                   l_inv_org_id                -- warehouse_id
                                               ,
                                   rec_line.purchase_order   -- purchase_order
                                                          ,
                                   rec_line.sales_order         -- sales_order
                                                       ,
                                   rec_line.sales_order_date -- sales_order_date
                                                            ,
                                   l_salesrep_id        -- primary_salesrep_id
                                                ,
                                   rec_line.trx_number           -- trx_number
                                                      ,
                                   l_terms_id                       -- term_id
                                             ,
                                   g_org_id                          -- org_id
                                           ,
                                   rec_line.creation_date           -- gl_date
                                                         ,
                                   l_uom_code                      -- uom_code
                                             ,
                                   l_uom_name                      -- uom_name
                                             ,
                                   l_inv_line_num               -- line_number
                                                 ,
                                   l_inv_item_id          -- inventory_item_id
                                                ,
                                   l_cust_acct_id        -- paying_customer_id
                                                 ,
                                   l_bill_to_cust_id -- orig_system_bill_customer_id
                                                    ,
                                   l_ship_to_cust_id -- orig_system_ship_customer_id
                                                    ,
                                   l_cust_acct_id -- orig_system_sold_customer_id
                                                 ,
                                   l_bill_to_address_id -- orig_system_bill_address_id
                                                       ,
                                   l_ship_to_address_id -- orig_system_ship_address_id
                                                       ,
                                   g_conversion_type        -- conversion_type
                                                    ,
                                   g_conversion_rate        -- conversion_rate
                                                    ,
                                   'N'             -- amount_includes_tax_flag
                                      ,
                                   g_context           -- link_to_line_context
                                            ,
                                   rec_line.trx_number -- link_to_line_attribute1
                                                      ,
                                   rec_line.quantity_ordered -- link_to_line_attribute2
                                                            ,
                                   rec_line.quantity_back_ordered -- link_to_line_attribute3
                                                                 ,
                                   rec_line.inventory_item_num -- link_to_line_attribute4
                                                              ,
                                   rec_line.revenue_amount -- link_to_line_attribute5
                                                          ,
                                   'LINE_' || l_inv_line_num -- link_to_line_attribute6
                                                            ,
                                   'HDS-PRISM'                     -- tax_code
                                              ,
                                   'HDS-PRISM'                -- tax_rate_code
                                              ,
                                   'HDS-PRISM'        -- tax_jurisdiction_code
                                              ,
                                   'HDS-PRISM'              -- tax_status_code
                                              ,
                                   'HDS-PRISM'                          -- tax
                                              ,
                                   'SALES TAX REGIME'       -- tax_regime_code
                                                     ,
                                   SYSDATE                    -- creation_date
                                          ,
                                   -1                            -- created_by
                                     ,
                                   SYSDATE                 -- last_update_date
                                          ,
                                   -1                       -- last_updated_by
                                     ,
                                   lvc_reason_code);

                           fnd_file.put_line (fnd_file.LOG, ' insert 3');


                           INSERT
                             INTO ra_interface_distributions_all (
                                     interface_line_context,
                                     interface_line_attribute1,
                                     interface_line_attribute2,
                                     interface_line_attribute3,
                                     interface_line_attribute4,
                                     interface_line_attribute5,
                                     interface_line_attribute6,
                                     account_class,
                                     amount,
                                     percent,
                                     segment1,
                                     segment2,
                                     segment3,
                                     segment4,
                                     segment5,
                                     segment6,
                                     segment7,
                                     acctd_amount,
                                     org_id,
                                     creation_date,
                                     created_by,
                                     last_update_date,
                                     last_updated_by)
                           VALUES (g_context,
                                   rec_line.trx_number, -- interface_line_attribute1
                                   rec_line.quantity_ordered, -- interface_line_attribute2
                                   rec_line.quantity_back_ordered, -- interface_line_attribute3
                                   rec_line.inventory_item_num, -- interface_line_attribute4
                                   l_line_tax_amt, -- interface_line_attribute5
                                   'TAX_' || l_inv_line_num, -- interface_line_attribute6
                                   'TAX',                     -- account_class
                                   l_line_tax_amt,                   -- amount
                                   100,                             -- percent
                                   g_segment1,                     -- segment1
                                   l_branch_code,                  -- segment2
                                   g_segment3,                     -- segment3
                                   l_tax_acct,                     -- segment4
                                   g_segment5,                     -- segment5
                                   g_segment6,                     -- segment6
                                   g_segment7,                     -- segment7
                                   l_line_tax_amt,             -- acctd_amount
                                   g_org_id,                         -- org_id
                                   SYSDATE,                   -- creation_date
                                   -1,                           -- created_by
                                   SYSDATE,                -- last_update_date
                                   -1                       -- last_updated_by
                                     );
                        END IF;
                     -- END IF; -- Added Vamshi
                     EXCEPTION
                        WHEN OTHERS
                        THEN
                           fnd_file.put_line (
                              fnd_file.LOG,
                              'Error processing the TAX record.');
                     END;
                  END IF;                       -- rec_hdr.inv_type_flag = 'I'

                  IF l_inv_line_num = 1
                  THEN
                     --------------------------------------------------------------------------------------------
                     -- FREIGHT Lines and Distribution
                     --------------------------------------------------------------------------------------------
                     FOR rec_freight
                        IN cur_freight (l_inv_num, l_trx_type_id)
                     LOOP
                        -- rec_freight loop start>

                        l_freight_cnt := 0;
                        l_freight_amt := 0;
                        l_frt_tax_amt := 0;

                        --------------------------------------------------------------------------------------------
                        -- Check if there are any feight lines inserted into the interface table
                        --------------------------------------------------------------------------------------------
                        BEGIN
                           SELECT COUNT (1)
                             INTO l_freight_cnt
                             FROM ra_interface_lines_all
                            WHERE     trx_number = l_inv_num
                                  AND line_type = 'LINE'
                                  AND description = 'FREIGHT';
                        EXCEPTION
                           WHEN OTHERS
                           THEN
                              l_err_flag := 'Y';
                              l_error_message :=
                                    l_error_message
                                 || '-- Error validating if Freight Line exists in InterfaceTable ';
                        END;

                        --------------------------------------------------------------------------------------------
                        -- Derive Freight Amount
                        --------------------------------------------------------------------------------------------
                        BEGIN
                           SELECT SUM (freight_amount)
                             INTO l_freight_amt
                             FROM xxwc.xxwc_ar_inv_stg_tbl
                            WHERE     trx_number = l_inv_num
                                  AND freight_amount IS NOT NULL;
                        EXCEPTION
                           WHEN OTHERS
                           THEN
                              l_err_flag := 'Y';
                              l_error_message :=
                                    l_error_message
                                 || '-- Error deriving Freight Amount ';
                        END;

                        --------------------------------------------------------------------------------------------
                        -- Derive Tax Amount
                        --------------------------------------------------------------------------------------------
                        BEGIN
                           SELECT SUM (NVL (tax_amount, 0))
                             INTO l_frt_tax_amt
                             FROM xxwc.xxwc_ar_inv_stg_tbl
                            WHERE     trx_number = l_inv_num
                                  AND freight_amount IS NOT NULL;
                        EXCEPTION
                           WHEN OTHERS
                           THEN
                              l_err_flag := 'Y';
                              l_error_message :=
                                    l_error_message
                                 || '-- Error deriving Freight Tax Amount ';
                        END;


                        fnd_file.put_line (
                           fnd_file.LOG,
                              ' l_freight_cnt:'
                           || l_freight_cnt
                           || ' l_err_flag:'
                           || l_err_flag);

                        IF l_freight_cnt = 0 AND l_err_flag = 'N'
                        THEN
                           BEGIN
                              --------------------------------------------------------------------------------------------
                              -- Insert Freight Line into Interface Table
                              --------------------------------------------------------------------------------------------

                              fnd_file.put_line (fnd_file.LOG, ' insert 4');

                              BEGIN
                                 INSERT
                                   INTO ra_interface_lines_all (
                                           interface_line_context,
                                           interface_line_attribute1,
                                           interface_line_attribute2,
                                           interface_line_attribute3,
                                           interface_line_attribute4,
                                           interface_line_attribute5,
                                           interface_line_attribute6,
                                           interface_line_attribute7,
                                           interface_line_attribute8,
                                           interface_line_attribute9,
                                           interface_line_attribute10,
                                           interface_line_attribute11,
                                           interface_line_attribute12,
                                           interface_line_attribute13,
                                           interface_line_attribute15,
                                           batch_source_name,
                                           line_type,
                                           description,
                                           currency_code,
                                           amount,
                                           cust_trx_type_id,
                                           trx_date,
                                           quantity,
                                           warehouse_id,
                                           purchase_order,
                                           sales_order,
                                           sales_order_date,
                                           primary_salesrep_id,
                                           trx_number,
                                           term_id,
                                           org_id,
                                           gl_date,
                                           uom_code,
                                           uom_name,
                                           line_number,
                                           paying_customer_id,
                                           orig_system_bill_customer_id,
                                           orig_system_ship_customer_id,
                                           orig_system_sold_customer_id,
                                           orig_system_bill_address_id,
                                           orig_system_ship_address_id,
                                           conversion_type,
                                           conversion_rate,
                                           amount_includes_tax_flag,
                                           tax_code,
                                           tax_rate_code,
                                           tax_jurisdiction_code,
                                           tax_status_code,
                                           tax,
                                           tax_regime_code,
                                           creation_date,
                                           created_by,
                                           last_update_date,
                                           last_updated_by,
                                           reason_code)
                                    VALUES (
                                              g_context,
                                              rec_freight.trx_number -- interface_line_attribute1
                                                                    ,
                                              rec_freight.quantity_ordered -- interface_line_attribute2
                                                                          ,
                                              rec_freight.quantity_back_ordered -- interface_line_attribute3
                                                                               ,
                                              rec_freight.inventory_item_num -- interface_line_attribute4
                                                                            ,
                                              l_freight_amt -- interface_line_attribute5
                                                           ,
                                              'FREIGHT_1' -- interface_line_attribute6
                                                         ,
                                              rec_line.warehouse_id -- interface_line_attribute7
                                                                   ,
                                              rec_line.prism_inv_type -- interface_line_attribute8
                                                                     ,
                                              rec_line.ordered_by -- interface_line_attribute9
                                                                 ,
                                              rec_line.taken_by -- interface_line_attribute10
                                                               ,
                                              rec_line.received_by -- interface_line_attribute11
                                                                  ,
                                              rec_line.ship_via -- interface_line_attribute12
                                                               ,
                                              rec_freight.line_number -- interface_line_attribute13
                                                                     ,
                                              rec_line.sales_order -- interface_line_attribute15
                                                                  ,
                                              g_batch_source -- batch_source_name
                                                            ,
                                              'LINE'              -- line_type
                                                    ,
                                              'FREIGHT'         -- description
                                                       ,
                                              g_currency      -- currency_code
                                                        ,
                                              l_freight_amt          -- amount
                                                           ,
                                              l_cus_trx_type_id -- cust_trx_type_id
                                                               ,
                                              rec_freight.trx_date -- trx_date
                                                                  ,
                                              DECODE (
                                                 rec_freight.quantity_shipped,
                                                 0, 1,
                                                 rec_freight.quantity_shipped) -- quantity
                                                                              ,
                                              l_inv_org_id     -- warehouse_id
                                                          ,
                                              rec_freight.purchase_order -- purchase_order
                                                                        ,
                                              rec_freight.sales_order -- sales_order
                                                                     ,
                                              rec_freight.sales_order_date -- sales_order_date
                                                                          ,
                                              l_salesrep_id -- primary_salesrep_id
                                                           ,
                                              rec_freight.trx_number -- trx_number
                                                                    ,
                                              l_terms_id            -- term_id
                                                        ,
                                              g_org_id               -- org_id
                                                      ,
                                              rec_line.creation_date -- gl_date
                                                                    ,
                                              l_uom_code         -- l_uom_code
                                                        ,
                                              l_uom_name           -- uom_name
                                                        ,
                                              '1'               -- line_number
                                                 ,
                                              l_cust_acct_id -- paying_customer_id
                                                            ,
                                              l_bill_to_cust_id -- orig_system_bill_customer_id
                                                               ,
                                              l_ship_to_cust_id -- orig_system_ship_customer_id
                                                               ,
                                              l_cust_acct_id -- orig_system_sold_customer_id
                                                            ,
                                              l_bill_to_address_id -- orig_system_bill_address_id
                                                                  ,
                                              l_ship_to_address_id -- orig_system_ship_address_id
                                                                  ,
                                              g_conversion_type -- conversion_type
                                                               ,
                                              g_conversion_rate -- conversion_rate
                                                               ,
                                              'N'  -- amount_includes_tax_flag
                                                 ,
                                              'HDS-PRISM'          -- tax_code
                                                         ,
                                              'HDS-PRISM'     -- tax_rate_code
                                                         ,
                                              'HDS-PRISM' -- tax_jurisdiction_code
                                                         ,
                                              'HDS-PRISM'   -- tax_status_code
                                                         ,
                                              'HDS-PRISM'               -- tax
                                                         ,
                                              'SALES TAX REGIME' -- tax_regime_code
                                                                ,
                                              SYSDATE         -- creation_date
                                                     ,
                                              -1                 -- created_by
                                                ,
                                              SYSDATE      -- last_update_date
                                                     ,
                                              -1            -- last_updated_by
                                                ,
                                              lvc_reason_code);
                              EXCEPTION
                                 WHEN OTHERS
                                 THEN
                                    l_err_flag := 'Y';
                                    l_error_message :=
                                          l_error_message
                                       || '-- Error inserting Freight record';
                              END;

                              /*
                                     BEGIN
                                     INSERT INTO ra_interface_salescredits_all (interface_line_context,
                                                                                interface_line_attribute1,
                                                                                interface_line_attribute2,
                                                                                interface_line_attribute3,
                                                                                interface_line_attribute4,
                                                                                interface_line_attribute5,
                                                                                interface_line_attribute6,
                                                                                salesrep_id,
                                                                                sales_credit_amount_split,
                                                                                sales_credit_percent_split,
                                                                                sales_credit_type_id,
                                                                                org_id,
                                                                                creation_date,
                                                                                created_by,
                                                                                last_update_date,
                                                                                last_updated_by
                                                                                )
                                                                        VALUES (g_context,
                                                                                rec_freight.trx_number,              -- interface_line_attribute1
                                                                                rec_freight.quantity_ordered,        -- interface_line_attribute2
                                                                                rec_freight.quantity_back_ordered,   -- interface_line_attribute3
                                                                                rec_freight.inventory_item_num,      -- interface_line_attribute4
                                                                                l_freight_amt,                       -- interface_line_attribute5
                                                                                'FREIGHT_1',                         -- interface_line_attribute6
                                                                                l_salesrep_id,                       -- salesrep_id
                                                                                l_freight_amt,                       -- sales_credit_amount_split
                                                                                100,                                 -- sales_credit_percent_split
                                                                                1,                                   -- sales_credit_type_id
                                                                                g_org_id,                            -- org_id
                                                                                SYSDATE,                             -- creation_date
                                                                                -1,                                  -- created_by
                                                                                SYSDATE,                             -- last_update_date
                                                                                -1                                   -- last_updated_by
                                                                                );
                                     EXCEPTION
                                     WHEN OTHERS THEN
                                       l_err_flag := 'Y';
                                       l_error_message := l_error_message || '-- Error inserting Freight SalesCredit record';
                                     END;
                              */

                              --------------------------------------------------------------------------------------------
                              -- Insert Freight Line into Distribution Table
                              --------------------------------------------------------------------------------------------
                              BEGIN
                                 fnd_file.put_line (fnd_file.LOG,
                                                    ' insert 5');

                                 INSERT
                                   INTO ra_interface_distributions_all (
                                           interface_line_context,
                                           interface_line_attribute1,
                                           interface_line_attribute2,
                                           interface_line_attribute3,
                                           interface_line_attribute4,
                                           interface_line_attribute5,
                                           interface_line_attribute6,
                                           account_class,
                                           amount,
                                           percent,
                                           segment1,
                                           segment2,
                                           segment3,
                                           segment4,
                                           segment5,
                                           segment6,
                                           segment7,
                                           acctd_amount,
                                           org_id,
                                           creation_date,
                                           created_by,
                                           last_update_date,
                                           last_updated_by)
                                 VALUES (g_context,
                                         rec_freight.trx_number, -- interface_line_attribute1
                                         rec_freight.quantity_ordered, -- interface_line_attribute2
                                         rec_freight.quantity_back_ordered, -- interface_line_attribute3
                                         rec_freight.inventory_item_num, -- interface_line_attribute4
                                         l_freight_amt, -- interface_line_attribute5
                                         'FREIGHT_1', -- interface_line_attribute6
                                         'REV',               -- account_class
                                         l_freight_amt,              -- amount
                                         100,                       -- percent
                                         g_segment1,               -- segment1
                                         l_branch_code,            -- segment2
                                         g_segment3,               -- segment3
                                         NVL (l_intan_rev_acct, g_frt_acct), -- segment4 -- Changed by Vamshi.
                                         g_segment5,               -- segment5
                                         g_segment6,               -- segment6
                                         g_segment7,               -- segment7
                                         l_freight_amt,        -- acctd_amount
                                         g_org_id,                   -- org_id
                                         SYSDATE,             -- creation_date
                                         -1,                     -- created_by
                                         SYSDATE,          -- last_update_date
                                         -1                 -- last_updated_by
                                           );
                              EXCEPTION
                                 WHEN OTHERS
                                 THEN
                                    l_err_flag := 'Y';
                                    l_error_message :=
                                          l_error_message
                                       || '-- Error inserting Freight Distribution record';
                              END;

                              IF l_diff_tax_amt != 0
                              THEN
                                 IF rec_hdr.inv_type_flag = 'I'
                                 THEN
                                    IF ABS (l_diff_tax_amt) > 1
                                    THEN
                                       l_frt_tax_amt :=
                                          l_frt_tax_amt + l_diff_tax_amt;
                                       l_diff_tax_amt := 0;
                                    ELSIF     ABS (l_diff_tax_amt) <= 1
                                          AND l_zero_tax_line_cnt = 0
                                    THEN
                                       l_frt_tax_amt :=
                                          l_frt_tax_amt + l_diff_tax_amt;
                                       l_diff_tax_amt := 0;
                                    END IF;
                                 ELSE
                                    -- rec_hdr.inv_type_flag = 'I'
                                    l_frt_tax_amt :=
                                       rec_freight.stg_attribute2;
                                    l_diff_tax_amt := 0;
                                 END IF;        -- rec_hdr.inv_type_flag = 'I'
                              END IF;                -- IF l_diff_tax_amt != 0

                              fnd_file.put_line (
                                 fnd_file.LOG,
                                    'b4 inserting l_frt_tax_amt - '
                                 || l_frt_tax_amt);

                              --------------------------------------------------------------------------------------------
                              -- Insert Freight Tax Line into Interface Table
                              --------------------------------------------------------------------------------------------
                              BEGIN
                                 fnd_file.put_line (
                                    fnd_file.LOG,
                                    'l_description:' || l_description);
                                 fnd_file.put_line (fnd_file.LOG,
                                                    ' insert 6');


                                 INSERT
                                   INTO ra_interface_lines_all (
                                           interface_line_context,
                                           interface_line_attribute1,
                                           interface_line_attribute2,
                                           interface_line_attribute3,
                                           interface_line_attribute4,
                                           interface_line_attribute5,
                                           interface_line_attribute6,
                                           interface_line_attribute7,
                                           interface_line_attribute8,
                                           interface_line_attribute9,
                                           interface_line_attribute10,
                                           interface_line_attribute11,
                                           interface_line_attribute12,
                                           interface_line_attribute13,
                                           interface_line_attribute15,
                                           batch_source_name,
                                           line_type,
                                           description,
                                           currency_code,
                                           amount,
                                           cust_trx_type_id,
                                           trx_date,
                                           quantity,
                                           warehouse_id,
                                           purchase_order,
                                           sales_order,
                                           sales_order_date,
                                           primary_salesrep_id,
                                           trx_number,
                                           term_id,
                                           org_id,
                                           gl_date,
                                           uom_code,
                                           uom_name,
                                           line_number,
                                           paying_customer_id,
                                           orig_system_bill_customer_id,
                                           orig_system_ship_customer_id,
                                           orig_system_sold_customer_id,
                                           orig_system_bill_address_id,
                                           orig_system_ship_address_id,
                                           conversion_type,
                                           conversion_rate,
                                           amount_includes_tax_flag,
                                           link_to_line_context,
                                           link_to_line_attribute1,
                                           link_to_line_attribute2,
                                           link_to_line_attribute3,
                                           link_to_line_attribute4,
                                           link_to_line_attribute5,
                                           link_to_line_attribute6,
                                           tax_code,
                                           tax_rate_code,
                                           tax_jurisdiction_code,
                                           tax_status_code,
                                           tax,
                                           tax_regime_code,
                                           creation_date,
                                           created_by,
                                           last_update_date,
                                           last_updated_by,
                                           reason_code)
                                    VALUES (
                                              g_context,
                                              rec_freight.trx_number -- interface_line_attribute1
                                                                    ,
                                              rec_freight.quantity_ordered -- interface_line_attribute2
                                                                          ,
                                              rec_freight.quantity_back_ordered -- interface_line_attribute3
                                                                               ,
                                              'FREIGHT' -- interface_line_attribute4
                                                       ,
                                              l_frt_tax_amt -- interface_line_attribute5
                                                           ,
                                              'FREIGHT_TAX' -- interface_line_attribute6
                                                           ,
                                              rec_freight.warehouse_id -- interface_line_attribute7
                                                                      ,
                                              rec_freight.prism_inv_type -- interface_line_attribute8
                                                                        ,
                                              rec_freight.ordered_by -- interface_line_attribute9
                                                                    ,
                                              rec_freight.taken_by -- interface_line_attribute10
                                                                  ,
                                              rec_freight.received_by -- interface_line_attribute11
                                                                     ,
                                              --rec_line.ship_via -- interface_line_attribute12   commented by Vamshi
                                              lvc_ship_via,
                                              rec_freight.line_number -- interface_line_attribute13
                                                                     ,
                                              rec_freight.sales_order -- interface_line_attribute15
                                                                     ,
                                              g_batch_source -- batch_source_name
                                                            ,
                                              'TAX'               -- line_type
                                                   ,
                                              l_description     -- description
                                                           ,
                                              g_currency      -- currency_code
                                                        ,
                                              l_frt_tax_amt          -- amount
                                                           ,
                                              l_cus_trx_type_id -- cust_trx_type_id
                                                               ,
                                              rec_freight.trx_date -- trx_date
                                                                  ,
                                              DECODE (
                                                 rec_freight.quantity_shipped,
                                                 0, 1,
                                                 rec_freight.quantity_shipped) -- quantity
                                                                              ,
                                              l_inv_org_id     -- warehouse_id
                                                          ,
                                              rec_freight.purchase_order -- purchase_order
                                                                        ,
                                              rec_freight.sales_order -- sales_order
                                                                     ,
                                              rec_freight.sales_order_date -- sales_order_date
                                                                          ,
                                              l_salesrep_id -- primary_salesrep_id
                                                           ,
                                              rec_freight.trx_number -- trx_number
                                                                    ,
                                              l_terms_id            -- term_id
                                                        ,
                                              g_org_id               -- org_id
                                                      ,
                                              rec_freight.creation_date -- gl_date
                                                                       ,
                                              l_uom_code           -- uom_code
                                                        ,
                                              l_uom_name           -- uom_name
                                                        ,
                                              l_inv_line_num    -- line_number
                                                            ,
                                              l_cust_acct_id -- paying_customer_id
                                                            ,
                                              l_bill_to_cust_id -- orig_system_bill_customer_id
                                                               ,
                                              l_ship_to_cust_id -- orig_system_ship_customer_id
                                                               ,
                                              l_cust_acct_id -- orig_system_sold_customer_id
                                                            ,
                                              l_bill_to_address_id -- orig_system_bill_address_id
                                                                  ,
                                              l_ship_to_address_id -- orig_system_ship_address_id
                                                                  ,
                                              g_conversion_type -- conversion_type
                                                               ,
                                              g_conversion_rate -- conversion_rate
                                                               ,
                                              'N'  -- amount_includes_tax_flag
                                                 ,
                                              g_context -- link_to_line_context
                                                       ,
                                              rec_freight.trx_number -- link_to_line_attribute1
                                                                    ,
                                              rec_freight.quantity_ordered -- link_to_line_attribute2
                                                                          ,
                                              rec_freight.quantity_back_ordered -- link_to_line_attribute3
                                                                               ,
                                              rec_freight.inventory_item_num -- link_to_line_attribute4
                                                                            ,
                                              l_freight_amt -- link_to_line_attribute5
                                                           ,
                                              'FREIGHT_1' -- link_to_line_attribute6
                                                         ,
                                              'HDS-PRISM'          -- tax_code
                                                         ,
                                              'HDS-PRISM'     -- tax_rate_code
                                                         ,
                                              'HDS-PRISM' -- tax_jurisdiction_code
                                                         ,
                                              'HDS-PRISM'   -- tax_status_code
                                                         ,
                                              'HDS-PRISM'               -- tax
                                                         ,
                                              'SALES TAX REGIME' -- tax_regime_code
                                                                ,
                                              SYSDATE         -- creation_date
                                                     ,
                                              -1                 -- created_by
                                                ,
                                              SYSDATE      -- last_update_date
                                                     ,
                                              -1            -- last_updated_by
                                                ,
                                              lvc_reason_code);
                              EXCEPTION
                                 WHEN OTHERS
                                 THEN
                                    l_err_flag := 'Y';
                                    fnd_file.put_line (
                                       fnd_File.LOG,
                                          '-- Error inserting Freight Tax record:'
                                       || l_description
                                       || '  '
                                       || SQLERRM);
                                    l_error_message :=
                                          l_error_message
                                       || '-- Error inserting Freight Tax record';
                              END;

                              /*
                                     BEGIN
                                     INSERT INTO ra_interface_salescredits_all (interface_line_context,
                                                                                interface_line_attribute1,
                                                                                interface_line_attribute2,
                                                                                interface_line_attribute3,
                                                                                interface_line_attribute4,
                                                                                interface_line_attribute5,
                                                                                interface_line_attribute6,
                                                                                salesrep_id,
                                                                                sales_credit_amount_split,
                                                                                sales_credit_percent_split,
                                                                                sales_credit_type_id,
                                                                                org_id,
                                                                                creation_date,
                                                                                created_by,
                                                                                last_update_date,
                                                                                last_updated_by
                                                                                )
                                                                        VALUES (g_context,
                                                                                rec_freight.trx_number,              -- interface_line_attribute1
                                                                                rec_freight.quantity_ordered,        -- interface_line_attribute2
                                                                                rec_freight.quantity_back_ordered,   -- interface_line_attribute3
                                                                                'FREIGHT',                           -- interface_line_attribute4
                                                                                l_frt_tax_amt,                       -- interface_line_attribute5
                                                                                'FREIGHT_TAX',                       -- interface_line_attribute6
                                                                                l_salesrep_id,                       -- salesrep_id
                                                                                l_frt_tax_amt,                       -- sales_credit_amount_split
                                                                                100,                                 -- sales_credit_percent_split
                                                                                1,                                   -- sales_credit_type_id
                                                                                g_org_id,                            -- org_id
                                                                                SYSDATE,                             -- creation_date
                                                                                -1,                                  -- created_by
                                                                                SYSDATE,                             -- last_update_date
                                                                                -1                                   -- last_updated_by
                                                                                );
                                     EXCEPTION
                                     WHEN OTHERS THEN
                                       l_err_flag := 'Y';
                                       l_error_message := l_error_message || '-- Error inserting Freight Tax SalesCredit record';
                                     END;
                              */

                              --------------------------------------------------------------------------------------------
                              -- Insert Freight Tax Line into Distribution Table
                              --------------------------------------------------------------------------------------------
                              BEGIN
                                 fnd_file.put_line (fnd_file.LOG,
                                                    ' insert 7');


                                 INSERT
                                   INTO ra_interface_distributions_all (
                                           interface_line_context,
                                           interface_line_attribute1,
                                           interface_line_attribute2,
                                           interface_line_attribute3,
                                           interface_line_attribute4,
                                           interface_line_attribute5,
                                           interface_line_attribute6,
                                           account_class,
                                           amount,
                                           percent,
                                           segment1,
                                           segment2,
                                           segment3,
                                           segment4,
                                           segment5,
                                           segment6,
                                           segment7,
                                           acctd_amount,
                                           org_id,
                                           creation_date,
                                           created_by,
                                           last_update_date,
                                           last_updated_by)
                                 VALUES (g_context,
                                         rec_freight.trx_number, -- interface_line_attribute1
                                         rec_freight.quantity_ordered, -- interface_line_attribute2
                                         rec_freight.quantity_back_ordered, -- interface_line_attribute3
                                         'FREIGHT', -- interface_line_attribute4
                                         l_frt_tax_amt, -- interface_line_attribute5
                                         'FREIGHT_TAX', -- interface_line_attribute6
                                         'TAX',               -- account_class
                                         l_frt_tax_amt,              -- amount
                                         100,                       -- percent
                                         g_segment1,               -- segment1
                                         l_branch_code,            -- segment2
                                         g_segment3,               -- segment3
                                         l_tax_acct,               -- segment4
                                         g_segment5,               -- segment5
                                         g_segment6,               -- segment6
                                         g_segment7,               -- segment7
                                         l_frt_tax_amt,        -- acctd_amount
                                         g_org_id,                   -- org_id
                                         SYSDATE,             -- creation_date
                                         -1,                     -- created_by
                                         SYSDATE,          -- last_update_date
                                         -1                 -- last_updated_by
                                           );
                              EXCEPTION
                                 WHEN OTHERS
                                 THEN
                                    l_err_flag := 'Y';
                                    l_error_message :=
                                          l_error_message
                                       || '-- Error inserting Freight Tax Distribution record';
                              END;

                              IF SQL%FOUND
                              THEN
                                 fnd_file.put_line (
                                    fnd_file.LOG,
                                    'FREIGHT line inserted successfully');
                              END IF;

                              BEGIN
                                 UPDATE xxwc.xxwc_ar_inv_stg_tbl
                                    SET status = 'PROCESSED',
                                        error_message = NULL
                                  WHERE     trx_number = l_inv_num
                                        AND freight_amount IS NOT NULL;
                              EXCEPTION
                                 WHEN OTHERS
                                 THEN
                                    l_err_flag := 'Y';
                                    l_error_message :=
                                          l_error_message
                                       || '-- Error updating staging record to PROCESSED';
                              END;
                           EXCEPTION
                              WHEN OTHERS
                              THEN
                                 fnd_file.put_line (
                                    fnd_file.LOG,
                                       'Error processing FREIGHT Line for invoice - '
                                    || l_inv_num);
                           END;
                        END IF;
                     END LOOP;                       -- rec_freight loop end <
                  END IF;

                  -- IF rec_hdr.inv_type_flag = 'I' THEN ---?
                  --------------------------------------------------------------------------------------------
                  -- Insert REV Item Line into Distribution Table
                  --------------------------------------------------------------------------------------------
                  BEGIN
                     fnd_file.put_line (fnd_file.LOG, ' insert 8');

                     INSERT
                       INTO ra_interface_distributions_all (
                               interface_line_context,
                               interface_line_attribute1,
                               interface_line_attribute2,
                               interface_line_attribute3,
                               interface_line_attribute4,
                               interface_line_attribute5,
                               interface_line_attribute6,
                               account_class,
                               amount,
                               percent,
                               segment1,
                               segment2,
                               segment3,
                               segment4,
                               segment5,
                               segment6,
                               segment7,
                               acctd_amount,
                               org_id,
                               creation_date,
                               created_by,
                               last_update_date,
                               last_updated_by)
                     VALUES (g_context,
                             rec_line.trx_number, -- interface_line_attribute1
                             rec_line.quantity_ordered, -- interface_line_attribute2
                             rec_line.quantity_back_ordered, -- interface_line_attribute3
                             rec_line.inventory_item_num, -- interface_line_attribute4
                             rec_line.revenue_amount, -- interface_line_attribute5
                             'LINE_' || l_inv_line_num, -- interface_line_attribute6
                             'REV',                           -- account_class
                             rec_line.revenue_amount,                -- amount
                             100,                                   -- percent
                             g_segment1,                           -- segment1
                             l_branch_code,                        -- segment2
                             g_segment3,                           -- segment3
                             NVL (l_intan_rev_acct, l_rev_acct), -- segment4 -- Changed by Vamshi.
                             g_segment5,                           -- segment5
                             g_segment6,                           -- segment6
                             g_segment7,                           -- segment7
                             rec_line.revenue_amount,          -- acctd_amount
                             g_org_id,                               -- org_id
                             SYSDATE,                         -- creation_date
                             -1,                                 -- created_by
                             SYSDATE,                      -- last_update_date
                             -1                             -- last_updated_by
                               );
                  EXCEPTION
                     WHEN OTHERS
                     THEN
                        l_err_flag := 'Y';
                        l_error_message :=
                              l_error_message
                           || '-- Error inserting REV Distribution Item record';
                  END;

                  -- END IF;    ---?                   -- rec_hdr.inv_type_flag = 'I'

                  l_rec_dist_line_cnt := 0;

                  -- Check if Receivable Distribution Line is entered for this Invoice.
                  BEGIN
                     SELECT COUNT (1)
                       INTO l_rec_dist_line_cnt
                       FROM ra_interface_distributions_all
                      WHERE     interface_line_context = g_context
                            AND interface_line_attribute1 =
                                   rec_line.trx_number
                            AND account_class = 'REC';
                  EXCEPTION
                     WHEN OTHERS
                     THEN
                        l_rec_dist_line_cnt := 0;
                  END;

                  IF l_rec_dist_line_cnt = 0 AND rec_line.revenue_amount != 0
                  THEN
                     l_rec_frt_amt := 0;
                     l_rec_amt := 0;

                     BEGIN
                        SELECT SUM (NVL (revenue_amount, 0))
                          INTO l_rec_amt
                          FROM xxwc.xxwc_ar_inv_stg_tbl
                         WHERE     trx_number = rec_line.trx_number
                               AND freight_amount IS NULL;
                     EXCEPTION
                        WHEN OTHERS
                        THEN
                           l_rec_dist_line_cnt := 0;
                           l_rec_amt := 0;
                     END;

                     BEGIN
                        SELECT SUM (NVL (freight_amount, 0))
                          INTO l_rec_frt_amt
                          FROM xxwc.xxwc_ar_inv_stg_tbl
                         WHERE     trx_number = rec_line.trx_number
                               AND freight_amount IS NOT NULL;
                     EXCEPTION
                        WHEN OTHERS
                        THEN
                           l_rec_dist_line_cnt := 0;
                           l_rec_frt_amt := 0;
                     END;

                     l_rec_amt :=
                          NVL (l_rec_amt, 0)
                        + NVL (l_rec_frt_amt, 0)
                        + NVL (rec_line.stg_attribute2, 0);

                     --------------------------------------------------------------------------------------------
                     -- Insert REC Line into Distribution Table
                     --------------------------------------------------------------------------------------------
                     BEGIN
                        fnd_file.put_line (fnd_file.LOG, ' insert 9');

                        INSERT
                          INTO ra_interface_distributions_all (
                                  interface_line_context,
                                  interface_line_attribute1,
                                  interface_line_attribute2,
                                  interface_line_attribute3,
                                  interface_line_attribute4,
                                  interface_line_attribute5,
                                  interface_line_attribute6,
                                  account_class,
                                  amount,
                                  percent,
                                  segment1,
                                  segment2,
                                  segment3,
                                  segment4,
                                  segment5,
                                  segment6,
                                  segment7,
                                  acctd_amount,
                                  org_id,
                                  creation_date,
                                  created_by,
                                  last_update_date,
                                  last_updated_by)
                        VALUES (g_context,
                                rec_line.trx_number, -- interface_line_attribute1
                                rec_line.quantity_ordered, -- interface_line_attribute2
                                rec_line.quantity_back_ordered, -- interface_line_attribute3
                                rec_line.inventory_item_num, -- interface_line_attribute4
                                rec_line.revenue_amount, -- interface_line_attribute5
                                'LINE_' || l_inv_line_num, -- interface_line_attribute6
                                'REC',                        -- account_class
                                l_rec_amt,                           -- amount
                                100,                                -- percent
                                g_segment1,                        -- segment1
                                l_branch_code,                     -- segment2
                                g_segment3,                        -- segment3
                                g_rec_acct,                        -- segment4
                                g_segment5,                        -- segment5
                                g_segment6,                        -- segment6
                                g_segment7,                        -- segment7
                                l_rec_amt,                     -- acctd_amount
                                g_org_id,                            -- org_id
                                SYSDATE,                      -- creation_date
                                -1,                              -- created_by
                                SYSDATE,                   -- last_update_date
                                -1                          -- last_updated_by
                                  );
                     EXCEPTION
                        WHEN OTHERS
                        THEN
                           l_err_flag := 'Y';
                           l_error_message :=
                                 l_error_message
                              || '-- Error inserting REC Distribution record';
                     END;
                  END IF;                        -- IF l_rec_dist_line_cnt = 0

                  IF SQL%FOUND
                  THEN
                     fnd_file.put_line (fnd_file.LOG,
                                        'Inserted successfully');
                  END IF;

                  BEGIN
                     UPDATE xxwc.xxwc_ar_inv_stg_tbl
                        SET status = 'PROCESSED',
                            error_message = SUBSTR (l_error_message, 1, 250)
                      WHERE trx_number = rec_hdr.trx_number;
                  EXCEPTION
                     WHEN OTHERS
                     THEN
                        l_err_flag := 'Y';
                        l_error_message :=
                              l_error_message
                           || '-- Error updating staging record status to PROCESSED';
                  END;
               ELSE
                  -- IF l_err_flag =N
                  BEGIN
                     UPDATE xxwc.xxwc_ar_inv_stg_tbl
                        SET status = 'REJECTED',
                            error_message = SUBSTR (l_error_message, 1, 250)
                      WHERE trx_number = rec_hdr.trx_number;
                  EXCEPTION
                     WHEN OTHERS
                     THEN
                        l_err_flag := 'Y';
                        l_error_message :=
                              l_error_message
                           || '-- Error updating staging record status to REJECTED';
                  END;
               END IF;                                     -- IF l_err_flag =N

               l_inv_line_num := l_inv_line_num + 1;
            END LOOP;                                   -- rec_line loop end <

            fnd_file.put_line (fnd_file.LOG, 'ERROR_FLAG:' || l_err_flag);

            IF l_err_flag = 'Y'
            THEN
               BEGIN
                  DELETE FROM ra_interface_lines_all
                        WHERE trx_number = rec_hdr.trx_number;

                  DELETE FROM ra_interface_distributions_all
                        WHERE interface_line_attribute1 = rec_hdr.trx_number;

                  DELETE FROM ra_interface_salescredits_all
                        WHERE interface_line_attribute1 = rec_hdr.trx_number;
               EXCEPTION
                  WHEN OTHERS
                  THEN
                     l_err_flag := 'Y';
                     l_error_message :=
                           l_error_message
                        || '-- Error deleting records inserted into interface table';
               END;

               BEGIN
                  UPDATE xxwc.xxwc_ar_inv_stg_tbl
                     SET status = 'REJECTED'
                   WHERE trx_number = rec_hdr.trx_number;
               EXCEPTION
                  WHEN OTHERS
                  THEN
                     l_err_flag := 'Y';
                     l_error_message :=
                           l_error_message
                        || '-- Error deleting records inserted into interface table';
               END;
            END IF;
         EXCEPTION
            WHEN l_program_error
            THEN
               fnd_file.put_line (fnd_file.LOG, l_error_message);

               BEGIN
                  UPDATE xxwc.xxwc_ar_inv_stg_tbl
                     SET status = 'REJECTED',
                         error_message = SUBSTR (l_error_message, 1, 250)
                   WHERE trx_number = rec_hdr.trx_number;
               EXCEPTION
                  WHEN OTHERS
                  THEN
                     l_err_flag := 'Y';
                     l_error_message :=
                           l_error_message
                        || '-- Error updating staging record status to REJECTED';
               END;
            WHEN OTHERS
            THEN
               fnd_file.put_line (fnd_file.LOG, l_error_message);
               fnd_file.put_line (fnd_file.output, l_error_message);

               xxcus_error_pkg.xxcus_error_main_api (
                  p_called_from         => 'load_interface',
                  p_calling             => 'load_interface',
                  p_request_id          => fnd_global.conc_request_id,
                  p_ora_error_msg       => l_error_message,
                  p_error_desc          => 'Error running P2E Sales Invoice Interface with Program Error Exception',
                  p_distribution_list   => ppl_dflt_email,
                  p_module              => 'AR');
         END;                               -- Invoice Amount Validation < End
      END LOOP;                                          -- rec_hdr loop end <

      COMMIT;
      
      -- $$$$$
      BEGIN
      UPDATE RA_INTERFACE_LINES_ALL
      SET TERM_ID = NULL, TERM_NAME=NULL
      WHERE CUST_TRX_TYPE_ID= 2;
      
      COMMIT;
      EXCEPTION
      WHEN OTHERS THEN 
      NULL;
      END;

      fnd_file.put_line (
         fnd_file.LOG,
         '----------------------------------------------------------------------------------');
      fnd_file.put_line (fnd_file.LOG, 'End of Procedure : LOAD_INTERFACE');
      fnd_file.put_line (
         fnd_file.LOG,
         '----------------------------------------------------------------------------------');
   EXCEPTION
      WHEN OTHERS
      THEN
         ROLLBACK;
         l_error_code := SQLCODE;
         l_error_message := SUBSTR (SQLERRM, 1, 200);
         fnd_file.put_line (
            fnd_file.LOG,
            'Exception Block of Stage to Interface Import Procedure');
         fnd_file.put_line (fnd_file.LOG,
                            'The Error Code Traced is : ' || l_error_code);
         fnd_file.put_line (
            fnd_file.LOG,
            'The Error Message Traced is : ' || l_error_message);

         g_retcode := 2;
         g_err_msg := 'Error loading AR Sales Invoice interface table';
   END load_interface;

   /********************************************************************************
   ProcedureName : SUBMIT_INTERFACE
   Purpose       : API to submit Auto Invoice Master Program.

   HISTORY
   ===============================================================================
   VERSION DATE          AUTHOR(S)       DESCRIPTION
   ------- -----------   --------------- -----------------------------------------
   1.0     12/13/2011    Gopi Damuluri    Initial version.
   ********************************************************************************/
   PROCEDURE submit_interface (p_batch_source IN VARCHAR2)
   AS
      l_conc_request_id   NUMBER;
      l_request_id        NUMBER;

      v_phase             VARCHAR2 (100);
      v_status            VARCHAR2 (100);
      v_dev_phase         VARCHAR2 (100);
      v_dev_status        VARCHAR2 (100);
      v_message           VARCHAR2 (2000);
      v_wait_outcome      BOOLEAN;

      l_status            VARCHAR2 (100);
      l_del_phase         VARCHAR2 (100);
      l_del_status        VARCHAR2 (100);

      l_message           VARCHAR2 (2000);
      l_error_code        NUMBER;
      l_error_message     VARCHAR2 (240);
      l_batch_source_id   NUMBER;
   BEGIN
      fnd_file.put_line (
         fnd_file.LOG,
         '----------------------------------------------------------------------------------');
      fnd_file.put_line (fnd_file.LOG,
                         'Start of Procedure : SUBMIT_INTERFACE');
      fnd_file.put_line (
         fnd_file.LOG,
         '----------------------------------------------------------------------------------');

      BEGIN
         SELECT batch_source_id
           INTO l_batch_source_id
           FROM ra_batch_sources_all
          WHERE NAME = p_batch_source;
      EXCEPTION
         WHEN OTHERS
         THEN
            l_error_message :=
                  l_error_message
               || '-- Invoice Source '
               || g_batch_source
               || ' is not defined in Oracle';
      END;

      IF l_batch_source_id IS NOT NULL
      THEN
         l_request_id :=
            fnd_request.submit_request (
               application   => 'AR',
               program       => 'RAXMTR',
               description   => 'Autoinvoice Master Program',
               start_time    => SYSDATE,
               sub_request   => FALSE,
               argument1     => 9,
               argument2     => g_org_id,
               argument3     => l_batch_source_id,
               argument4     => p_batch_source,
               argument5     => TO_CHAR (SYSDATE, 'DD-MON-YYYY'),
               argument6     => NULL,
               argument7     => NULL,
               argument8     => NULL,
               argument9     => NULL,
               argument10    => NULL,
               argument11    => NULL,
               argument12    => NULL,
               argument13    => NULL,
               argument14    => NULL,
               argument15    => NULL,
               argument16    => NULL,
               argument17    => NULL,
               argument18    => NULL,
               argument19    => NULL,
               argument20    => NULL,
               argument21    => NULL,
               argument22    => NULL,
               argument23    => NULL,
               argument24    => NULL,
               argument25    => NULL,
               argument26    => 'Y',
               argument27    => NULL);
         COMMIT;
      END IF;

      g_request_id := l_request_id;

      v_wait_outcome :=
         fnd_concurrent.wait_for_request (l_request_id,
                                          30,
                                          15000,
                                          v_phase,
                                          v_status,
                                          v_dev_phase,
                                          v_dev_status,
                                          v_message);

      ----------------------------------------------------------------------------------
      -- Call to Document attachment Procedure If Standard program completed Normal
      ----------------------------------------------------------------------------------
      IF UPPER (v_dev_phase) = 'COMPLETE' AND UPPER (v_dev_status) = 'NORMAL'
      THEN
         fnd_file.put_line (
            fnd_file.LOG,
            'Auto Invoice Master Process Completed Successfully');
      END IF;

      fnd_file.put_line (
         fnd_file.LOG,
         '----------------------------------------------------------------------------------');
      fnd_file.put_line (fnd_file.LOG, 'End of Procedure : SUBMIT_INTERFACE');
      fnd_file.put_line (
         fnd_file.LOG,
         '----------------------------------------------------------------------------------');
   EXCEPTION
      WHEN OTHERS
      THEN
         l_error_code := SQLCODE;
         l_error_message := SUBSTR (SQLERRM, 1, 200);
         fnd_file.put_line (
            fnd_file.LOG,
            'Exception Block of Standard Program Call Procedure');
         fnd_file.put_line (fnd_file.LOG,
                            'The Error Code Traced is : ' || l_error_code);
         fnd_file.put_line (
            fnd_file.LOG,
            'The Error Message Traced is : ' || l_error_message);

         g_retcode := 2;
         g_err_msg := 'Error submitting Auto Invoice Master Program';
   END submit_interface;

   /********************************************************************************
   ProcedureName : MAIN
   Purpose       : API which internally calls LOAD_INTERFACE and SUBMIT_INTERFACE
                   programs.

   HISTORY
   ===============================================================================
   VERSION DATE          AUTHOR(S)       DESCRIPTION
   ------- -----------   --------------- -----------------------------------------
   1.0     12/13/2011    Gopi Damuluri    Initial version.
   ********************************************************************************/
   ----------------------------------------------------------------------------------
   -- Main Procedure
   ----------------------------------------------------------------------------------
   PROCEDURE main (errbuf               OUT VARCHAR2,
                   retcode              OUT NUMBER,
                   p_conc_prg_name   IN     VARCHAR2)
   AS
      l_flag            VARCHAR2 (10) DEFAULT 'Y';
      l_error_code      NUMBER;
      l_error_message   VARCHAR2 (2000);
   BEGIN
      fnd_file.put_line (
         fnd_file.LOG,
         '----------------------------------------------------------------------------------');
      fnd_file.put_line (fnd_file.LOG, 'Start of Procedure : MAIN');
      fnd_file.put_line (
         fnd_file.LOG,
         '----------------------------------------------------------------------------------');
      fnd_file.put_line (fnd_file.LOG, 'Input Parameters     :');
      fnd_file.put_line (fnd_file.LOG,
                         'p_conc_prg_name        :' || p_conc_prg_name);
      fnd_file.put_line (
         fnd_file.LOG,
         '----------------------------------------------------------------------------------');

      ----------------------------------------------------------------------------------
      -- Calling the procedure to update Global Variables.
      ----------------------------------------------------------------------------------
      update_acct_global_var;

      ----------------------------------------------------------------------------------
      -- Calling the procedure to validate the staging data and load Interface Tables.
      ----------------------------------------------------------------------------------
      load_interface;

      ----------------------------------------------------------------------------------
      -- Calling the procedure to submit "Auto Invoice Master Program"
      ----------------------------------------------------------------------------------
      IF p_conc_prg_name = 'Y'
      THEN
         submit_interface (g_batch_source);
         fnd_file.put_line (
            fnd_file.LOG,
            'Request ID for AR Invoice Import Program Is :' || g_request_id);
      END IF;

      retcode := g_retcode;
      errbuf := g_err_msg;

      fnd_file.put_line (
         fnd_file.LOG,
         '----------------------------------------------------------------------------------');
      fnd_file.put_line (fnd_file.LOG, 'End of Procedure : MAIN');
      fnd_file.put_line (
         fnd_file.LOG,
         '----------------------------------------------------------------------------------');
   EXCEPTION
      WHEN OTHERS
      THEN
         l_error_code := SQLCODE;
         l_error_message :=
            'Error in the main procedure of Sales Invoice Interface';

         retcode := 2;
         errbuf := l_error_message;

         fnd_file.put_line (fnd_file.LOG,
                            'Exception Block of Main Procedure');
         fnd_file.put_line (fnd_file.LOG,
                            'The Error Code Traced is : ' || l_error_code);
         fnd_file.put_line (
            fnd_file.LOG,
            'The Error Message Traced is : ' || l_error_message);
   END main;

   /********************************************************************************
   ProcedureName : UC4_CALL
   Purpose       : API which is called from UC4 job. It submits the Concurrent
                   Program - "XXWC Prism To EBS AR Invoice Interface"

   HISTORY
   ===============================================================================
   VERSION DATE          AUTHOR(S)       DESCRIPTION
   ------- -----------   --------------- -----------------------------------------
   1.0     12/13/2011    Gopi Damuluri    Initial version.
   ********************************************************************************/
   PROCEDURE uc4_call (p_errbuf                   OUT VARCHAR2,
                       p_retcode                  OUT NUMBER,
                       p_conc_prg_name         IN     VARCHAR2,
                       p_conc_prg_arg1         IN     VARCHAR2,
                       p_user_name             IN     VARCHAR2,
                       p_responsibility_name   IN     VARCHAR2,
                       p_org_name              IN     VARCHAR2,
                       p_threshold_value       IN     NUMBER)
   IS
      --
      -- Package Variables
      --
      l_package               VARCHAR2 (50) := 'XXWC_AR_INV_INT_PKG';
      l_email                 fnd_user.email_address%TYPE;

      l_req_id                NUMBER NULL;
      v_phase                 VARCHAR2 (50);
      v_status                VARCHAR2 (50);
      v_dev_status            VARCHAR2 (50);
      v_dev_phase             VARCHAR2 (50);
      v_message               VARCHAR2 (250);
      v_error_message         VARCHAR2 (3000);
      v_supplier_id           NUMBER;
      v_rec_cnt               NUMBER := 0;
      l_message               VARCHAR2 (150);
      l_errormessage          VARCHAR2 (3000);
      pl_errorstatus          NUMBER;
      l_can_submit_request    BOOLEAN := TRUE;
      l_globalset             VARCHAR2 (100);
      l_err_msg               VARCHAR2 (3000);
      l_err_code              NUMBER;
      l_sec                   VARCHAR2 (255);
      l_statement             VARCHAR2 (9000);
      l_user                  fnd_user.user_id%TYPE;
      l_user_id               NUMBER;
      l_responsibility_id     NUMBER;
      l_resp_application_id   NUMBER;
      l_rej_rec_cnt           NUMBER;
      l_totl_rec_cnt          NUMBER;
      l_rej_percent           NUMBER;
      l_prc_inv_amt           NUMBER;
      l_prc_tax_amt           NUMBER;
      l_prc_frt_amt           NUMBER;
      l_rej_inv_amt           NUMBER;
      l_rej_tax_amt           NUMBER;
      l_rej_frt_amt           NUMBER;
      l_totl_inv_amt          NUMBER;
      l_totl_tax_amt          NUMBER;
      l_totl_frt_amt          NUMBER;
      l_conc_prg_arg1         VARCHAR2 (900);

      -- Error DEBUG
      l_err_callfrom          VARCHAR2 (75)
                                 DEFAULT 'XXWC_AR_INV_INT_PKG.UC4_CALL';
      l_err_callpoint         VARCHAR2 (75) DEFAULT 'START';
   BEGIN
      p_retcode := 0;

      --------------------------------------------------------------------------
      -- Deriving UserId
      --------------------------------------------------------------------------
      BEGIN
         SELECT user_id
           INTO l_user_id
           FROM fnd_user
          WHERE     1 = 1
                AND user_name = UPPER (p_user_name)
                AND SYSDATE BETWEEN start_date
                                AND NVL (end_date, TRUNC (SYSDATE) + 1);
      EXCEPTION
         WHEN NO_DATA_FOUND
         THEN
            l_err_msg :=
               'UserName - ' || p_user_name || ' not defined in Oracle';
            RAISE PROGRAM_ERROR;
         WHEN OTHERS
         THEN
            l_err_msg :=
               'Error deriving user_id for UserName - ' || p_user_name;
            RAISE PROGRAM_ERROR;
      END;

      --------------------------------------------------------------------------
      -- Deriving ResponsibilityId and ResponsibilityApplicationId
      --------------------------------------------------------------------------
      BEGIN
         SELECT responsibility_id, application_id
           INTO l_responsibility_id, l_resp_application_id
           FROM fnd_responsibility_vl
          WHERE     responsibility_name = p_responsibility_name
                AND SYSDATE BETWEEN start_date
                                AND NVL (end_date, TRUNC (SYSDATE) + 1);
      EXCEPTION
         WHEN NO_DATA_FOUND
         THEN
            l_err_msg :=
                  'Responsibility - '
               || p_responsibility_name
               || ' not defined in Oracle';
            RAISE PROGRAM_ERROR;
         WHEN OTHERS
         THEN
            l_err_msg :=
                  'Error deriving Responsibility_id for ResponsibilityName - '
               || p_responsibility_name;
            RAISE PROGRAM_ERROR;
      END;

      fnd_file.put_line (fnd_file.LOG,
                         'Concurrent Program Name - ' || p_conc_prg_name);
      l_sec :=
         'UC4 call to run concurrent request - PRISM2EBS AR Invoice Interface';
      l_conc_prg_arg1 := p_conc_prg_arg1;

      --------------------------------------------------------------------------
      -- Apps Initialize
      --------------------------------------------------------------------------
      fnd_global.apps_initialize (l_user_id,
                                  l_responsibility_id,
                                  l_resp_application_id);

      l_err_callpoint := 'Before submitting Concurrent request';
      --------------------------------------------------------------------------
      -- Submit "XXWC Create Outbound File Common Program"
      --------------------------------------------------------------------------
      l_req_id :=
         fnd_request.submit_request (application   => 'XXWC',
                                     program       => p_conc_prg_name,
                                     description   => NULL,
                                     start_time    => SYSDATE,
                                     sub_request   => FALSE,
                                     argument1     => l_conc_prg_arg1);
      l_err_callpoint := 'After submitting Concurrent request';

      COMMIT;

      DBMS_OUTPUT.put_line ('After fnd_request');

      IF (l_req_id != 0)
      THEN
         IF fnd_concurrent.wait_for_request (l_req_id,
                                             30,
                                             15000,
                                             v_phase,
                                             v_status,
                                             v_dev_phase,
                                             v_dev_status,
                                             v_message)
         THEN
            v_error_message :=
                  CHR (10)
               || 'ReqID='
               || l_req_id
               || ' DPhase '
               || v_dev_phase
               || ' DStatus '
               || v_dev_status
               || CHR (10)
               || ' MSG - '
               || v_message;

            -- Error Returned
            IF v_dev_phase != 'COMPLETE' OR v_dev_status != 'NORMAL'
            THEN
               l_statement :=
                     'An error occured running the XXWC_AR_INV_INT_PKG '
                  || v_error_message
                  || '.';
               fnd_file.put_line (fnd_file.LOG, l_statement);
               fnd_file.put_line (fnd_file.output, l_statement);
               RAISE PROGRAM_ERROR;
            END IF;
         -- Then Success!
         ELSE
            l_statement :=
                  'An error occured running the XXWC_AR_INV_INT_PKG '
               || v_error_message
               || '.';
            fnd_file.put_line (fnd_file.LOG, l_statement);
            fnd_file.put_line (fnd_file.output, l_statement);
            RAISE PROGRAM_ERROR;
         END IF;
      ELSE
         l_statement := 'An error occured running the XXWC_AR_INV_INT_PKG ';
         fnd_file.put_line (fnd_file.LOG, l_statement);
         fnd_file.put_line (fnd_file.output, l_statement);
         RAISE PROGRAM_ERROR;
      END IF;

      l_prc_inv_amt := 0;
      l_prc_tax_amt := 0;
      l_rej_inv_amt := 0;
      l_rej_tax_amt := 0;
      l_totl_inv_amt := 0;
      l_totl_tax_amt := 0;
      l_prc_frt_amt := 0;
      l_rej_frt_amt := 0;
      l_totl_frt_amt := 0;

      ----------------------------------------------------------------------------------
      -- Validate ERROR RECORD COUNT against THRESHOLD VALUE.
      ----------------------------------------------------------------------------------
      BEGIN
         SELECT COUNT (DISTINCT trx_number)
           INTO l_totl_rec_cnt
           FROM xxwc.xxwc_ar_inv_stg_tbl xaist
          WHERE     1 = 1
                AND NVL (status, 'XXXXX') IN ('PROCESSED', 'REJECTED')
                AND creation_date IN (SELECT MAX (creation_date)
                                        FROM xxwc.xxwc_ar_inv_stg_tbl);

         SELECT COUNT (DISTINCT trx_number)
           INTO l_rej_rec_cnt
           FROM xxwc.xxwc_ar_inv_stg_tbl xaist
          WHERE     1 = 1
                AND NVL (status, 'XXXXX') = 'REJECTED'
                AND creation_date IN (SELECT MAX (creation_date)
                                        FROM xxwc.xxwc_ar_inv_stg_tbl);

         ----------------------------------------------------------------------------------
         -- Total Invoice Amount
         ----------------------------------------------------------------------------------
         SELECT SUM (NVL (revenue_amount, 0))
           INTO l_totl_inv_amt
           FROM xxwc.xxwc_ar_inv_stg_tbl xaist
          WHERE     1 = 1
                AND NVL (status, 'XXXXX') IN ('PROCESSED', 'REJECTED')
                AND creation_date IN (SELECT MAX (creation_date)
                                        FROM xxwc.xxwc_ar_inv_stg_tbl);

         SELECT SUM (NVL (tax_amt, 0))
           INTO l_totl_tax_amt
           FROM (SELECT DISTINCT
                        trx_number, TO_NUMBER (stg_attribute2) tax_amt
                   FROM xxwc.xxwc_ar_inv_stg_tbl xaist
                  WHERE     1 = 1
                        AND NVL (status, 'XXXXX') IN ('PROCESSED', 'REJECTED')
                        AND creation_date IN (SELECT MAX (creation_date)
                                                FROM xxwc.xxwc_ar_inv_stg_tbl));

         SELECT SUM (NVL (freight_amount, 0))
           INTO l_totl_frt_amt
           FROM xxwc.xxwc_ar_inv_stg_tbl xaist
          WHERE     1 = 1
                AND NVL (status, 'XXXXX') IN ('PROCESSED', 'REJECTED')
                AND creation_date IN (SELECT MAX (creation_date)
                                        FROM xxwc.xxwc_ar_inv_stg_tbl);

         l_totl_inv_amt := l_totl_inv_amt + l_totl_tax_amt + l_totl_frt_amt;

         ----------------------------------------------------------------------------------
         -- Rejected Invoice Amount
         ----------------------------------------------------------------------------------
         SELECT SUM (NVL (revenue_amount, 0))
           INTO l_rej_inv_amt
           FROM xxwc.xxwc_ar_inv_stg_tbl xaist
          WHERE     1 = 1
                AND NVL (status, 'XXXXX') = 'REJECTED'
                AND creation_date IN (SELECT MAX (creation_date)
                                        FROM xxwc.xxwc_ar_inv_stg_tbl);

         SELECT SUM (NVL (freight_amount, 0))
           INTO l_rej_frt_amt
           FROM xxwc.xxwc_ar_inv_stg_tbl xaist
          WHERE     1 = 1
                AND NVL (status, 'XXXXX') = 'REJECTED'
                AND creation_date IN (SELECT MAX (creation_date)
                                        FROM xxwc.xxwc_ar_inv_stg_tbl);

         SELECT SUM (NVL (tax_amt, 0))
           INTO l_rej_tax_amt
           FROM (SELECT DISTINCT
                        trx_number, TO_NUMBER (stg_attribute2) tax_amt
                   FROM xxwc.xxwc_ar_inv_stg_tbl xaist
                  WHERE     1 = 1
                        AND NVL (status, 'XXXXX') = 'REJECTED'
                        AND creation_date IN (SELECT MAX (creation_date)
                                                FROM xxwc.xxwc_ar_inv_stg_tbl));

         l_rej_inv_amt := l_rej_inv_amt + l_rej_tax_amt + l_rej_frt_amt;

         ----------------------------------------------------------------------------------
         -- Processed Invoice Amount
         ----------------------------------------------------------------------------------
         SELECT SUM (NVL (revenue_amount, 0))
           INTO l_prc_inv_amt
           FROM xxwc.xxwc_ar_inv_stg_tbl xaist
          WHERE     1 = 1
                AND NVL (status, 'XXXXX') = 'PROCESSED'
                AND creation_date IN (SELECT MAX (creation_date)
                                        FROM xxwc.xxwc_ar_inv_stg_tbl);

         SELECT SUM (NVL (freight_amount, 0))
           INTO l_prc_frt_amt
           FROM xxwc.xxwc_ar_inv_stg_tbl xaist
          WHERE     1 = 1
                AND NVL (status, 'XXXXX') = 'PROCESSED'
                AND creation_date IN (SELECT MAX (creation_date)
                                        FROM xxwc.xxwc_ar_inv_stg_tbl);

         SELECT SUM (NVL (tax_amt, 0))
           INTO l_prc_tax_amt
           FROM (SELECT DISTINCT
                        trx_number, TO_NUMBER (stg_attribute2) tax_amt
                   FROM xxwc.xxwc_ar_inv_stg_tbl xaist
                  WHERE     1 = 1
                        AND NVL (status, 'XXXXX') = 'PROCESSED'
                        AND creation_date IN (SELECT MAX (creation_date)
                                                FROM xxwc.xxwc_ar_inv_stg_tbl));

         l_prc_inv_amt := l_prc_inv_amt + l_prc_tax_amt + l_prc_frt_amt;

         ----------------------------------------------------------------------------------
         -- Calculate Reject Percent
         ----------------------------------------------------------------------------------
         l_rej_percent := (l_rej_rec_cnt * 100) / l_totl_rec_cnt;

         IF l_rej_percent = 0
         THEN
            p_retcode := 0;
         ELSIF l_rej_percent > 0 AND l_rej_percent <= p_threshold_value
         THEN
            p_retcode := 1;
         ELSIF l_rej_percent > p_threshold_value
         THEN
            p_retcode := 2;
         END IF;

         p_errbuf :=
               'TotalRecordCount = '
            || l_totl_rec_cnt
            || '      RejectedRecordCount = '
            || l_rej_rec_cnt;
      EXCEPTION
         WHEN OTHERS
         THEN
            p_retcode := 2;
            l_statement := 'Error while validating the Error Record Counts';
            fnd_file.put_line (
               fnd_file.LOG,
               'Error while validating the Error Record Counts - ' || SQLERRM);
      END;

      DBMS_OUTPUT.put_line (l_sec);
      DBMS_OUTPUT.put_line ('Request ID:  ' || l_req_id);
   EXCEPTION
      WHEN PROGRAM_ERROR
      THEN
         ROLLBACK;
         l_err_code := 2;
         l_err_msg := l_sec;
         l_err_msg := l_err_msg || ' ERROR ' || SUBSTR (SQLERRM, 1, 2000);
         DBMS_OUTPUT.put_line (l_err_msg);
         fnd_file.put_line (fnd_file.LOG, l_err_msg);
         fnd_file.put_line (fnd_file.output, l_err_msg);
         --        l_err_callpoint := l_message;

         xxcus_error_pkg.xxcus_error_main_api (
            p_called_from         => l_err_callfrom,
            p_calling             => l_err_callpoint,
            p_request_id          => l_req_id,
            p_ora_error_msg       => SQLERRM,
            p_error_desc          => 'Error running XXWC_AR_INV_INT_PKG package with PROGRAM ERROR',
            p_distribution_list   => ppl_dflt_email,
            p_module              => 'AR');

         p_retcode := 2;
      WHEN OTHERS
      THEN
         fnd_file.put_line (fnd_file.LOG, l_sec);
         fnd_file.put_line (fnd_file.output, l_sec);
         l_err_code := 2;
         l_err_msg := l_sec;
         l_err_msg :=
               l_sec
            || l_err_msg
            || ' ERROR '
            || SQLCODE
            || SUBSTR (SQLERRM, 1, 2000);
         DBMS_OUTPUT.put_line (l_err_msg);
         fnd_file.put_line (fnd_file.LOG, l_err_msg);
         fnd_file.put_line (fnd_file.output, l_err_msg);
         --        l_err_callpoint := l_message;

         p_retcode := 2;

         xxcus_error_pkg.xxcus_error_main_api (
            p_called_from         => l_err_callfrom,
            p_calling             => l_err_callpoint,
            p_request_id          => l_req_id,
            p_ora_error_msg       => SUBSTR (l_err_msg, 1, 2000),
            p_error_desc          => 'Error running XXWC_AR_INV_INT_PKG package with OTHERS Exception',
            p_distribution_list   => ppl_dflt_email,
            p_module              => 'AR');
   END uc4_call;

   /********************************************************************************
   ProcedureName : LOAD_DEBIT_MEMOS
   Purpose       : API used to validate and import PRISM Debit Memos.

   HISTORY
   ===============================================================================
   VERSION DATE          AUTHOR(S)       DESCRIPTION
   ------- -----------   --------------- -----------------------------------------
   1.0     12/13/2011    Gopi Damuluri    Initial version.
   1.1     06/29/2012    Gopi Damuluri    Derive Revenue Account for First Hawaiin
   ********************************************************************************/
   PROCEDURE load_debit_memos (errbuf               OUT VARCHAR2,
                               retcode              OUT NUMBER,
                               p_conc_prg_name   IN     VARCHAR2)
   AS
      l_error_code            NUMBER;
      l_recordcnt             NUMBER (6) := 0;
      l_error_message         VARCHAR2 (2000);
      l_err_flag              VARCHAR2 (1) := 'N';

      l_ship_to_address_id    NUMBER;
      l_bill_to_site_use_id   NUMBER;
      l_party_site_id         NUMBER;
      l_bill_to_address_id    NUMBER;
      l_cust_acct_id          NUMBER;
      l_salesrep_id           NUMBER;
      l_branch_code           VARCHAR2 (5);
      l_dm_rev_acct           VARCHAR2 (20);
      l_fh_acct               NUMBER;
      l_inv_org_id            NUMBER;
      l_terms_name            XXWC.XXWC_AHH_PAYMENT_TERMS_T.AHH_TERM_NAME%TYPE;
      l_terms_id              NUMBER;
      l_cus_trx_type_id       NUMBER;
      g_dm_trx_type           ra_cust_trx_types_all.name%TYPE := 'Debit Memo';
      l_dup_trx_val_flag      VARCHAR2 (1);
      l_trx_type_id           NUMBER;
      l_ship_to_found         VARCHAR2 (1);

      l_cm_batch_source_id    NUMBER;
      l_dm_batch_source_id    NUMBER;
      l_receipt_method_id     NUMBER;

      ---------------------------------------------------------------------------------
      -- Cursor to extract Debit Memo Data from Staging Table
      ----------------------------------------------------------------------------------
      CURSOR cur_dm
      IS
         SELECT xcr.ROWID r_id,
                   xcr.invoice_nbr
                || '_'
                || TO_CHAR (xcr.deposit_date, 'MMDDYY')
                   invoice_nbr_dd,
                xcr.CONTROL_NBR,
                xcr.DEPOSIT_DATE,
                xcr.OPERATOR_CODE,
                xcr.CHECK_NBR,
                ABS (xcr.CHECK_AMT) CHECK_AMT,
                xcr.URL_LINK,
                xcr.CUSTOMER_NBR,
                xcr.INVOICE_NBR,
                ABS (xcr.CHECK_AMT) INVOICE_AMT,
                xcr.DISC_AMT,
                xcr.GL_ACCT_NBR,
                xcr.SHORT_PAY_CODE,
                xcr.CREATION_DATE,
                xcr.UPDATED_DATE,
                xcr.STATUS,
                xcr.BILL_TO_LOCATION,
                xcr.COMMENTS,
                xcr.SHORT_CODE_INTERFACED,
                xcr.SHORT_CODE_INT_DATE,
                xcr.RECEIPT_TYPE,
                xcr.ORACLE_ACCOUNT_NBR,
                xcr.CUST_ACCOUNT_ID,
                xcr.ORACLE_FLAG,
                xcr.RECEIPT_ID,
                xcr.ABA_NUMBER,
                xcr.BANK_ACCOUNT_NUM,
                xcr.ORG_ID
           FROM xxwc.XXWC_AR_AHH_DM_STG_TBL xcr
          WHERE     1 = 1
                AND status != 'PROCESSED'
                AND NVL (status, 'XXX') NOT LIKE 'REV%'    -- already reversed
                AND receipt_type IN ('DEBIT', 'DEBIT MEMO')
                AND operator_code IN ('X',
                                      'Y',
                                      'H',
                                      'J');
   BEGIN
      fnd_file.put_line (
         fnd_file.LOG,
         '----------------------------------------------------------------------------------');
      fnd_file.put_line (fnd_file.LOG,
                         'Start of Procedure : LOAD_DEBIT_MEMOS');
      fnd_file.put_line (
         fnd_file.LOG,
         '----------------------------------------------------------------------------------');

      ----------------------------------------------------------------------------------
      -- Calling the procedure to update Debit Memo Global Variables.
      ----------------------------------------------------------------------------------
      update_dm_acct_global_var;

      ----------------------------------------------------------------------------------
      -- Derive OperatingUnitId.
      ----------------------------------------------------------------------------------
      BEGIN
         SELECT organization_id
           INTO g_org_id
           FROM hr_operating_units
          WHERE NAME = g_org_name;

         fnd_file.put_line (fnd_file.LOG, 'g_org_id - ' || g_org_id);
      EXCEPTION
         WHEN OTHERS
         THEN
            fnd_file.put_line (fnd_file.LOG,
                               'Error deriving Operating Unit details');
      END;

      FOR rec_dm IN cur_dm
      LOOP
         -- rec_dm loop start>

         -- Initialize Loop Variables
         l_err_flag := 'N';
         l_bill_to_address_id := NULL;
         l_cust_acct_id := NULL;
         l_salesrep_id := NULL;
         l_branch_code := NULL;
         l_dm_rev_acct := NULL;
         l_fh_acct := NULL;
         l_inv_org_id := NULL;
         l_terms_name := NULL;
         l_terms_id := NULL;
         l_cus_trx_type_id := NULL;
         l_dup_trx_val_flag := NULL;
         l_error_message := NULL;
         l_trx_type_id := NULL;

         fnd_file.put_line (
            fnd_file.LOG,
            ' --------------------------------------------------------------------------- ');
         fnd_file.put_line (fnd_file.LOG,
                            'Debit Memo Number - ' || rec_dm.invoice_nbr_dd);

         ----------------------------------------------------------------------------------
         -- COMMIT for every 1000 records
         ----------------------------------------------------------------------------------
         IF l_recordcnt = 1000
         THEN
            COMMIT;
            l_recordcnt := 0;
         END IF;

         ----------------------------------------------------------------------------------
         -- Derive InvoiceTypeId
         ----------------------------------------------------------------------------------
         BEGIN
            SELECT cust_trx_type_id
              INTO l_cus_trx_type_id
              FROM ra_cust_trx_types_all
             WHERE     NAME = g_dm_trx_type
                   AND SYSDATE BETWEEN start_date
                                   AND NVL (end_date, SYSDATE + 1)
                   AND org_id = g_org_id;
         EXCEPTION
            WHEN OTHERS
            THEN
               l_err_flag := 'Y';
               l_error_message :=
                     l_error_message
                  || '--Invalid Transaction Type : '
                  || g_dm_trx_type;
         END;

         ----------------------------------------------------------------------------------
         -- Derive Ship-To Address using Prism PartySiteNumber
         ----------------------------------------------------------------------------------
         l_ship_to_found := 'N';

         BEGIN
            SELECT hcas.cust_acct_site_id,
                   hcas.cust_account_id,
                   hcsu.bill_to_site_use_id,
                   hcas.party_site_id
              INTO l_ship_to_address_id,
                   l_cust_acct_id,
                   l_bill_to_site_use_id,
                   l_party_site_id
              FROM hz_cust_acct_sites_all hcas, hz_cust_site_uses_all hcsu
             WHERE     1 = 1
                   AND hcas.status = 'A'
                   AND hcas.attribute17 =
                             rec_dm.customer_nbr
                          || '-'
                          || UPPER (rec_dm.BILL_TO_LOCATION)
                   AND hcas.cust_acct_site_id = hcsu.cust_acct_site_id
                   AND hcsu.site_use_code = 'SHIP_TO'
                   AND hcas.org_id = g_org_id;

            l_ship_to_found := 'Y';
         EXCEPTION
            WHEN NO_DATA_FOUND
            THEN
               -- Check for the case with an appended Ship-To Site
               BEGIN
                  SELECT hcas.cust_acct_site_id,
                         hcas.cust_account_id,
                         hcsu.bill_to_site_use_id,
                         hcas.party_site_id
                    INTO l_ship_to_address_id,
                         l_cust_acct_id,
                         l_bill_to_site_use_id,
                         l_party_site_id
                    FROM hz_cust_acct_sites_all hcas,
                         hz_cust_site_uses_all hcsu,
                         apps.hz_cust_accounts hca
                   WHERE     1 = 1
                         AND hcas.status = 'A'
                         AND SUBSTR (hcas.attribute17,
                                     1,
                                     INSTR (hcas.attribute17, '-') - 1) =
                                rec_dm.customer_nbr
                         AND hcas.cust_acct_site_id = hcsu.cust_acct_site_id
                         AND hcsu.site_use_code = 'SHIP_TO'
                         AND hcas.cust_account_id = hca.cust_account_id
                         AND hca.attribute4 = 'AHH'
                         AND hcas.org_id = g_org_id;

                  l_ship_to_found := 'Y';
               EXCEPTION
                  WHEN NO_DATA_FOUND
                  THEN
                     l_err_flag := 'Y';
                     l_error_message :=
                           l_error_message
                        || '-- PRISM Party Site not in Oracle2';
                  WHEN TOO_MANY_ROWS
                  THEN
                     l_err_flag := 'Y';
                     l_error_message :=
                           l_error_message
                        || '-- Multiple Oracle Sites have same PRISM Party Site6';
                  WHEN OTHERS
                  THEN
                     l_err_flag := 'Y';
                     l_error_message :=
                           l_error_message
                        || '-- Error deriving SHIP-TO Site details';
               END;
            WHEN TOO_MANY_ROWS
            THEN
               l_err_flag := 'Y';
               l_error_message :=
                     l_error_message
                  || '-- Multiple Oracle Sites have same PRISM Party Site7';
            WHEN OTHERS
            THEN
               l_err_flag := 'Y';
               l_error_message :=
                  l_error_message || '-- Error deriving SHIP-TO Site details';
         END;

         ----------------------------------------------------------------------------------
         -- Derive Ship-To Address using Prism PartySiteNumber - Validate Party Site
         ----------------------------------------------------------------------------------
         IF l_ship_to_found = 'N'
         THEN
            BEGIN
               SELECT hcas.cust_acct_site_id,
                      hcas.cust_account_id,
                      hcsu.bill_to_site_use_id,
                      hcas.party_site_id
                 INTO l_ship_to_address_id,
                      l_cust_acct_id,
                      l_bill_to_site_use_id,
                      l_party_site_id
                 FROM hz_cust_acct_sites_all hcas,
                      hz_cust_site_uses_all hcsu,
                      hz_party_sites hps,
                      hz_cust_accounts hca
                WHERE     1 = 1
                      AND hcas.status = 'A'
                      AND hcsu.status = 'A'
                      AND hps.status = 'A'
                      AND hps.party_site_number = rec_dm.customer_nbr
                      AND hcas.cust_acct_site_id = hcsu.cust_acct_site_id
                      AND hcas.party_site_id = hps.party_site_id
                      AND hcsu.site_use_code = 'SHIP_TO'
                      AND hcas.cust_account_id = hca.cust_account_id
                      AND hca.attribute4 = 'AHH'
                      AND hcas.org_id = g_org_id;

               l_ship_to_found := 'Y';
            EXCEPTION
               WHEN NO_DATA_FOUND
               THEN
                  -- Check for the case with an appended Ship-To Site
                  BEGIN
                     SELECT hcas.cust_acct_site_id,
                            hcas.cust_account_id,
                            hcsu.bill_to_site_use_id,
                            hcas.party_site_id
                       INTO l_ship_to_address_id,
                            l_cust_acct_id,
                            l_bill_to_site_use_id,
                            l_party_site_id
                       FROM hz_cust_acct_sites_all hcas,
                            hz_cust_site_uses_all hcsu,
                            hz_party_sites hps,
                            hz_cust_accounts hca
                      WHERE     1 = 1
                            AND hcas.status = 'A'
                            AND hcsu.status = 'A'
                            AND hps.status = 'A'
                            AND SUBSTR (
                                   hps.party_site_number,
                                   1,
                                   INSTR (hps.party_site_number, '-') - 1) =
                                   rec_dm.customer_nbr
                            AND hcas.cust_acct_site_id =
                                   hcsu.cust_acct_site_id
                            AND hcas.party_site_id = hps.party_site_id
                            AND hcsu.site_use_code = 'SHIP_TO'
                            AND hcas.cust_account_id = hca.cust_account_id
                            AND hca.attribute4 = 'AHH'
                            AND hcas.org_id = g_org_id;

                     l_ship_to_found := 'Y';
                  EXCEPTION
                     WHEN NO_DATA_FOUND
                     THEN
                        l_err_flag := 'Y';
                        l_error_message :=
                              l_error_message
                           || '-- PRISM Party Site not in Oracle3';
                     WHEN TOO_MANY_ROWS
                     THEN
                        l_err_flag := 'Y';
                        l_error_message :=
                              l_error_message
                           || '-- Multiple Oracle Sites have same PRISM Party Site8';
                     WHEN OTHERS
                     THEN
                        l_err_flag := 'Y';
                        l_error_message :=
                              l_error_message
                           || '-- Error deriving SHIP-TO Site details';
                  END;
               WHEN TOO_MANY_ROWS
               THEN
                  l_err_flag := 'Y';
                  l_error_message :=
                        l_error_message
                     || '-- Multiple Oracle Sites have same PRISM Party Site9';
               WHEN OTHERS
               THEN
                  l_err_flag := 'Y';
                  l_error_message :=
                        l_error_message
                     || '-- Error deriving SHIP-TO Site details';
            END;
         END IF;

         BEGIN
            IF l_bill_to_site_use_id IS NOT NULL
            THEN
               ----------------------------------------------------------------------------------
               -- Derive Bill-To Address details using BillTo SiteUseId associated with ShipTo
               ----------------------------------------------------------------------------------
               SELECT hcsu.cust_acct_site_id
                 INTO l_bill_to_address_id
                 FROM hz_cust_site_uses_all hcsu
                WHERE     1 = 1
                      AND hcsu.site_use_id = l_bill_to_site_use_id
                      AND hcsu.site_use_code = 'BILL_TO'
                      AND hcsu.status = 'A'
                      AND hcsu.org_id = g_org_id;
            ELSE
               ---------------------------------------
               -- Derive Primary Bill-To Site
               ---------------------------------------
               SELECT hcas.cust_acct_site_id
                 INTO l_bill_to_address_id
                 FROM hz_cust_acct_sites_all hcas
                WHERE     1 = 1
                      AND hcas.status = 'A'
                      AND hcas.cust_account_id = l_cust_acct_id
                      AND NVL (hcas.bill_to_flag, 'N') = 'P'
                      AND hcas.org_id = g_org_id;
            END IF;
         EXCEPTION
            WHEN NO_DATA_FOUND
            THEN
               ---------------------------------------
               -- Derive Primary Bill-To Site
               ---------------------------------------
               BEGIN
                  SELECT hcas.cust_acct_site_id
                    INTO l_bill_to_address_id
                    FROM hz_cust_acct_sites_all hcas
                   WHERE     1 = 1
                         AND hcas.status = 'A'
                         AND hcas.cust_account_id = l_cust_acct_id
                         AND NVL (hcas.bill_to_flag, 'N') = 'P'
                         AND hcas.org_id = g_org_id;
               EXCEPTION
                  WHEN OTHERS
                  THEN
                     l_err_flag := 'Y';
                     l_error_message :=
                           l_error_message
                        || '-- Customer has no Primary BILL-TO Site';
               END;
            WHEN OTHERS
            THEN
               l_err_flag := 'Y';
               l_error_message :=
                     l_error_message
                  || '-- Customer has no Primary BILL-TO Site';
         END;

         ----------------------------------------------------------------------------------
         -- Derive Salesrep Details
         ----------------------------------------------------------------------------------
         BEGIN
            SELECT salesrep_id
              INTO l_salesrep_id
              FROM ra_salesreps_all
             WHERE     NAME = g_dm_sales_person
                   AND status = 'A'
                   AND SYSDATE BETWEEN start_date_active
                                   AND NVL (end_date_active, SYSDATE + 1)
                   AND org_id = g_org_id;
         EXCEPTION
            WHEN OTHERS
            THEN
               l_err_flag := 'Y';
               l_error_message := l_error_message || '-- No SalesRep Found';
         END;

         ----------------------------------------------------------------------------------
         -- Derive Branch Code
         ----------------------------------------------------------------------------------
         BEGIN
            l_branch_code := 'BW080';                   -- hardcoded by Vamshi
         --        SELECT entrp_loc
         --          INTO l_branch_code
         --          FROM xxcus_location_code_vw
         --         WHERE inactive = 'N'
         --           AND entrp_entity = '0W'
         --           AND ltrim(lob_branch, '0') =
         --               ltrim(to_char(rec_dm.bill_to_location), '0');
         EXCEPTION
            WHEN NO_DATA_FOUND
            THEN
               l_err_flag := 'Y';
               l_error_message :=
                     l_error_message
                  || '-- Branch Code not defined for : '
                  || rec_dm.bill_to_location;
            WHEN TOO_MANY_ROWS
            THEN
               l_err_flag := 'Y';
               l_error_message :=
                     l_error_message
                  || '-- Too many rows returned during Branch Code Validation';
            WHEN OTHERS
            THEN
               l_err_flag := 'Y';
               l_error_message :=
                     l_error_message
                  || '-- Error deriving Branch Code for : '
                  || rec_dm.bill_to_location;
         END;

         ----------------------------------------------------------------------------------
         -- Derive Revenue Account
         ----------------------------------------------------------------------------------
         BEGIN
            l_fh_acct := '251100';
         --        SELECT COUNT(1)
         --          INTO l_fh_acct
         --          FROM fnd_lookups
         --         WHERE 1 = 1
         --           AND lookup_type = 'XXWC_AR_LOCKBOX_HAWAII'
         --           AND lookup_code = ltrim(to_char(rec_dm.bill_to_location), '0')
         --           AND SYSDATE BETWEEN start_date_active AND
         --               nvl(end_date_active, SYSDATE + 1)
         --           AND nvl(enabled_flag, 'Y') = 'Y';
         EXCEPTION
            WHEN NO_DATA_FOUND
            THEN
               l_fh_acct := 0;
            WHEN OTHERS
            THEN
               l_fh_acct := 0;
               l_err_flag := 'Y';
               l_error_message :=
                     l_error_message
                  || '-- Error deriving Revenue Account for BillToLocation : '
                  || rec_dm.bill_to_location;
         END;

         IF l_fh_acct > 0
         THEN
            l_dm_rev_acct := g_dm_rev_acct_fh;
         ELSE
            l_dm_rev_acct := g_dm_rev_acct;
         END IF;

         ----------------------------------------------------------------------------------
         -- Derive Inventory Org Id
         ----------------------------------------------------------------------------------
         BEGIN
            SELECT organization_id
              INTO l_inv_org_id
              FROM org_organization_definitions ood
             WHERE organization_code =
                      LPAD (UPPER (rec_dm.bill_to_location), 3, '0');
         EXCEPTION
            WHEN OTHERS
            THEN
               l_inv_org_id := NULL;
         END;

         ----------------------------------------------------------------------------------
         -- Derive PaymentTerm Name
         ----------------------------------------------------------------------------------
         BEGIN
            SELECT term_id
              INTO l_terms_id
              FROM ra_terms
             WHERE     UPPER (NAME) = g_dm_term_name
                   AND TRUNC (SYSDATE) BETWEEN start_date_active
                                           AND NVL (end_date_active,
                                                    SYSDATE + 1);
         EXCEPTION
            WHEN OTHERS
            THEN
               ----------------------------------------------------------------------------------
               -- Defaulting PaymnetTerms to '30 NET'
               ----------------------------------------------------------------------------------
               BEGIN
                  SELECT term_id
                    INTO l_terms_id
                    FROM ra_terms
                   WHERE     UPPER (NAME) = '30 NET'
                         AND TRUNC (SYSDATE) BETWEEN start_date_active
                                                 AND NVL (end_date_active,
                                                          SYSDATE + 1);
               EXCEPTION
                  WHEN OTHERS
                  THEN
                     l_err_flag := 'Y';
                     l_error_message :=
                        l_error_message || '-- Error in Defaulting terms';
               END;
         END;

         ----------------------------------------------------------------------------------
         -- Inserting into interface table
         ----------------------------------------------------------------------------------
         fnd_file.put_line (
            fnd_file.LOG,
            ' B4 inserting into interface table l_err_flag -' || l_err_flag);
         fnd_file.put_line (fnd_file.LOG,
                            ' l_error_message -' || l_error_message);

         IF NVL (l_err_flag, 'N') = 'N'
         THEN
            BEGIN
               INSERT
                 INTO ra_interface_lines_all (interface_line_context,
                                              interface_line_attribute1,
                                              interface_line_attribute2,
                                              interface_line_attribute3,
                                              interface_line_attribute4,
                                              interface_line_attribute5,
                                              interface_line_attribute6,
                                              interface_line_attribute15,
                                              batch_source_name,
                                              line_type,
                                              description,
                                              currency_code,
                                              amount,
                                              cust_trx_type_id,
                                              trx_date,
                                              quantity,
                                              unit_selling_price,
                                              warehouse_id,
                                              trx_number,
                                              term_id,
                                              org_id,
                                              gl_date,
                                              line_number,
                                              paying_customer_id,
                                              orig_system_bill_customer_id,
                                              orig_system_ship_customer_id,
                                              orig_system_sold_customer_id,
                                              orig_system_bill_address_id,
                                              orig_system_ship_address_id,
                                              conversion_type,
                                              conversion_rate,
                                              amount_includes_tax_flag,
                                              taxable_flag,
                                              creation_date,
                                              created_by,
                                              last_update_date,
                                              last_updated_by,
                                              attribute_category,
                                              attribute15                                              
                                              )
               VALUES (g_context                     -- interface_line_context
                                ,
                       rec_dm.invoice_nbr_dd      -- interface_line_attribute1
                                            ,
                       0                          -- interface_line_attribute2
                        ,
                       0                          -- interface_line_attribute3
                        ,
                       'REFUND'                   -- interface_line_attribute4
                               ,
                       rec_dm.check_amt           -- interface_line_attribute5
                                       ,
                       'LINE'                     -- interface_line_attribute6
                             ,
                       rec_dm.check_nbr          -- interface_line_attribute15
                                       ,                                       
                       g_dm_batch_source                  -- batch_source_name
                                        ,
                       'LINE'                                     -- line_type
                             ,
                       g_dm_description                         -- description
                                       ,
                       g_currency                             -- currency_code
                                 ,
                       rec_dm.invoice_amt                 -- amount  -- vamshi
                                         ,
                       l_cus_trx_type_id                   -- cust_trx_type_id
                                        ,
                       rec_dm.deposit_date                         -- trx_date
                                          ,
                       1                                           -- quantity
                        ,
                       ROUND (rec_dm.invoice_amt, 2)     -- unit_selling_price
                                                    ,
                       l_inv_org_id                            -- warehouse_id
                                   ,
                       rec_dm.invoice_nbr_dd                     -- trx_number
                                            ,
                       l_terms_id                                   -- term_id
                                 ,
                       g_org_id                                      -- org_id
                               ,
                       rec_dm.creation_date                         -- gl_date
                                           ,
                       1                                        -- line_number
                        ,
                       l_cust_acct_id                    -- paying_customer_id
                                     ,
                       l_cust_acct_id          -- orig_system_bill_customer_id
                                     ,
                       l_cust_acct_id          -- orig_system_ship_customer_id
                                     ,
                       l_cust_acct_id          -- orig_system_sold_customer_id
                                     ,
                       l_bill_to_address_id     -- orig_system_bill_address_id
                                           ,
                       l_ship_to_address_id     -- orig_system_ship_address_id
                                           ,
                       g_conversion_type                    -- conversion_type
                                        ,
                       g_conversion_rate                    -- conversion_rate
                                        ,
                       'Y'                         -- amount_includes_tax_flag
                          ,
                       'N'                                     -- taxable_flag
                          ,
                       SYSDATE                                -- creation_date
                              ,
                       -1                                        -- created_by
                         ,
                       SYSDATE                             -- last_update_date
                              ,
                       -1   ,                                -- last_updated_by
                       162, --                       attribute_category,
                        TO_CHAR(SYSDATE,'MM/DD/YYYY') --                   attribute15                                              
                       
                         );
            EXCEPTION
               WHEN OTHERS
               THEN
                  l_err_flag := 'Y';
                  fnd_file.put_line (
                     fnd_file.LOG,
                        'Error inserting ITEM record into RA_INTERFACE_LINES_ALL2 '
                     || SQLERRM);
            END;

            /*
                     INSERT INTO ra_interface_salescredits_all (interface_line_context,
                                                                 interface_line_attribute1,
                                                                 interface_line_attribute2,
                                                                 interface_line_attribute3,
                                                                 interface_line_attribute4,
                                                                 interface_line_attribute5,
                                                                 interface_line_attribute6,
                                                                 salesrep_id,
                                                                 sales_credit_amount_split,
                                                                 sales_credit_percent_split,
                                                                 sales_credit_type_id,
                                                                 org_id,
                                                               creation_date,
                                                               created_by,
                                                               last_update_date,
                                                               last_updated_by
                                                                 )
                                                         VALUES (g_context,
                                                                 rec_dm.invoice_nbr_dd,      -- interface_line_attribute1
                                                                 rec_dm.quantity_ordered,      -- interface_line_attribute2
                                                                 rec_dm.quantity_back_ordered,    -- interface_line_attribute3
                                                                 rec_dm.inventory_item_num,    -- interface_line_attribute4
                                                                 rec_dm.revenue_amount,      -- interface_line_attribute5
                                                          'LINE_1',   --      'LINE',  -- interface_line_attribute6
                                                                 l_salesrep_id,
                                                                 rec_dm.invoice_amt,
                                                                 100,
                                                                 1,
                                                                 g_org_id,
                                                           SYSDATE,
                                                           -1,
                                                           SYSDATE,
                                                           -1
                                                                 );
            */

            BEGIN
               INSERT
                 INTO ra_interface_distributions_all (
                         interface_line_context,
                         interface_line_attribute1,
                         interface_line_attribute2,
                         interface_line_attribute3,
                         interface_line_attribute4,
                         interface_line_attribute5,
                         interface_line_attribute6,
                         account_class,
                         amount,
                         percent,
                         segment1,
                         segment2,
                         segment3,
                         segment4,
                         segment5,
                         segment6,
                         segment7,
                         acctd_amount,
                         org_id,
                         creation_date,
                         created_by,
                         last_update_date,
                         last_updated_by)
               VALUES (g_context,
                       rec_dm.invoice_nbr_dd,     -- interface_line_attribute1
                       0,                         -- interface_line_attribute2
                       0,                         -- interface_line_attribute3
                       'REFUND',                  -- interface_line_attribute4
                       rec_dm.check_amt,          -- interface_line_attribute5
                       'LINE',                    -- interface_line_attribute6
                       'REV',                                 -- account_class
                       rec_dm.invoice_amt,                           -- amount
                       100,                                         -- percent
                       g_segment1,                                 -- segment1
                       l_branch_code,                              -- segment2
                       g_segment3,                                 -- segment3
                       l_dm_rev_acct,                              -- segment4
                       g_segment5,                                 -- segment5
                       g_segment6,                                 -- segment6
                       g_segment7,                                 -- segment7
                       rec_dm.invoice_amt,                     -- acctd_amount
                       g_org_id,                                     -- org_id
                       SYSDATE,                               -- creation_date
                       -1,                                       -- created_by
                       SYSDATE,                            -- last_update_date
                       -1                                   -- last_updated_by
                         );
            EXCEPTION
               WHEN OTHERS
               THEN
                  l_err_flag := 'Y';
                  l_error_message :=
                        l_error_message
                     || '-- Error inserting REV REFUND Distribution record';
            END;

            BEGIN
               INSERT
                 INTO ra_interface_distributions_all (
                         interface_line_context,
                         interface_line_attribute1,
                         interface_line_attribute2,
                         interface_line_attribute3,
                         interface_line_attribute4,
                         interface_line_attribute5,
                         interface_line_attribute6,
                         account_class,
                         amount,
                         percent,
                         segment1,
                         segment2,
                         segment3,
                         segment4,
                         segment5,
                         segment6,
                         segment7,
                         acctd_amount,
                         org_id,
                         creation_date,
                         created_by,
                         last_update_date,
                         last_updated_by)
               VALUES (g_context,
                       rec_dm.invoice_nbr_dd,     -- interface_line_attribute1
                       0,                         -- interface_line_attribute2
                       0,                         -- interface_line_attribute3
                       'REFUND',                  -- interface_line_attribute4
                       rec_dm.check_amt,          -- interface_line_attribute5
                       'LINE',                    -- interface_line_attribute6
                       'REC',                                 -- account_class
                       rec_dm.invoice_amt,                           -- amount
                       100,                                         -- percent
                       g_segment1,                                 -- segment1
                       l_branch_code,                              -- segment2
                       g_segment3,                                 -- segment3
                       g_dm_rec_acct,                              -- segment4
                       g_segment5,                                 -- segment5
                       g_segment6,                                 -- segment6
                       g_segment7,                                 -- segment7
                       rec_dm.invoice_amt,                     -- acctd_amount
                       g_org_id,                                     -- org_id
                       SYSDATE,                               -- creation_date
                       -1,                                       -- created_by
                       SYSDATE,                            -- last_update_date
                       -1                                   -- last_updated_by
                         );
            EXCEPTION
               WHEN OTHERS
               THEN
                  l_err_flag := 'Y';
                  l_error_message :=
                        l_error_message
                     || '-- Error inserting REC REFUND Distribution record';
            END;

            IF SQL%FOUND
            THEN
               fnd_file.put_line (fnd_file.LOG, 'Inserted successfully');
            END IF;

            BEGIN
               ----------------------------------------------------------------------------------
               -- Updating status of records to PROCESSED
               ----------------------------------------------------------------------------------
               UPDATE xxwc.XXWC_AR_AHH_DM_STG_TBL
                  SET status = 'PROCESSED',
                      receipt_type = 'DEBIT MEMO',
                      comments = NULL
                WHERE ROWID = rec_dm.r_id;
            EXCEPTION
               WHEN OTHERS
               THEN
                  l_err_flag := 'Y';
                  l_error_message :=
                        l_error_message
                     || '-- Error updating staging record status to PROCESSED';
            END;
         ELSE
            -- IF l_err_flag =N
            ----------------------------------------------------------------------------------
            -- Updating status of error records to REJECTED.
            ----------------------------------------------------------------------------------
            BEGIN
               UPDATE xxwc.XXWC_AR_AHH_DM_STG_TBL
                  SET status = 'REJECTED'    -- , receipt_type  = 'DEBIT MEMO'
                                         , comments = l_error_message
                WHERE ROWID = rec_dm.r_id;
            EXCEPTION
               WHEN OTHERS
               THEN
                  l_err_flag := 'Y';
                  l_error_message :=
                        l_error_message
                     || '-- Error updating staging record status to REJECTED';
            END;
         END IF;                                           -- IF l_err_flag =N

         l_recordcnt := l_recordcnt + 1;

         IF l_err_flag = 'Y'
         THEN
            ----------------------------------------------------------------------------------
            -- Delete interface table records
            ----------------------------------------------------------------------------------
            BEGIN
               DELETE FROM ra_interface_lines_all
                     WHERE trx_number = rec_dm.invoice_nbr_dd;

               DELETE FROM ra_interface_distributions_all
                     WHERE interface_line_attribute1 = rec_dm.invoice_nbr_dd;

               DELETE FROM ra_interface_salescredits_all
                     WHERE interface_line_attribute1 = rec_dm.invoice_nbr_dd;
            EXCEPTION
               WHEN OTHERS
               THEN
                  l_err_flag := 'Y';
                  l_error_message :=
                        l_error_message
                     || '-- Error deleting records inserted into interface table';
            END;

            BEGIN
               UPDATE xxwc.XXWC_AR_AHH_DM_STG_TBL xcr
                  SET status = 'REJECTED'
                WHERE invoice_nbr = rec_dm.invoice_nbr;
            EXCEPTION
               WHEN OTHERS
               THEN
                  l_err_flag := 'Y';
                  l_error_message :=
                        l_error_message
                     || '-- Error updating staging record status to REJECTED';
            END;
         END IF;
      END LOOP;                                           -- rec_dm loop end <

      COMMIT;

      ----------------------------------------------------------------------------------
      -- Calling the procedure to submit "Auto Invoice Master Program"
      ----------------------------------------------------------------------------------
      IF p_conc_prg_name = 'Y'
      THEN
         submit_interface (g_dm_batch_source);
         
         -----------------------------------
         --Updating attribute15 
         -----------------------------------
       BEGIN         
        UPDATE ra_customer_trx_all
           SET attribute_Category = 162,
               attribute15 = TO_CHAR (SYSDATE, 'MM/DD/YYYY')
         WHERE     cust_trx_type_id = 1001
               AND batch_source_id IN (SELECT batch_source_id
                                         FROM apps.ra_batch_sources_all
                                        WHERE     UPPER (name) = 'HARRIS REFUND'
                                              AND org_id = 162)
               AND attribute15 IS NULL;
               COMMIT;
        EXCEPTION
        WHEN OTHERS THEN
        FND_FILE.PUT_LINE(FND_FILE.LOG,'Error occured while updataing attribute15 '||SQLERRM);
        END;
        
         

         ----------------------------------------------------------------------------------
         -- Derive Debit Memo Batch SourceId
         ----------------------------------------------------------------------------------
         BEGIN
            SELECT batch_source_id
              INTO l_dm_batch_source_id
              FROM ra_batch_sources_all
             WHERE NAME = g_dm_batch_source;
         EXCEPTION
            WHEN OTHERS
            THEN
               l_err_flag := 'Y';
               l_error_message :=
                     l_error_message
                  || '-- Invoice Source '
                  || g_dm_batch_source
                  || ' is not defined in Oracle';
         END;

         ----------------------------------------------------------------------------------
         -- Derive Credit Memo Batch SourceId
         ----------------------------------------------------------------------------------
         BEGIN
            SELECT batch_source_id
              INTO l_cm_batch_source_id
              FROM ra_batch_sources_all
             WHERE NAME = g_batch_source;
         EXCEPTION
            WHEN OTHERS
            THEN
               retcode := 2;
               l_err_flag := 'Y';
               l_error_message :=
                     l_error_message
                  || '-- Invoice Source '
                  || g_batch_source
                  || ' is not defined in Oracle';
         END;

         ----------------------------------------------------------------------------------
         -- Derive Receipt Method Id
         ----------------------------------------------------------------------------------
         BEGIN
            SELECT receipt_method_id
              INTO l_receipt_method_id
              FROM ar_receipt_methods
             WHERE NAME = 'WC-Corporate Receipt Method';
         EXCEPTION
            WHEN OTHERS
            THEN
               retcode := 2;
               l_err_flag := 'Y';
               l_error_message :=
                     l_error_message
                  || '-- Receipt Method "WC-Corporate Receipt Method" is not defined in Oracle';
         END;

         IF l_err_flag != 'Y'
         THEN
            ----------------------------------------------------------------------------------
            -- Calling the procedure to apply Refund Transactions
            ----------------------------------------------------------------------------------
            xxwc_ar_prism_refund_pkg.process_refund (l_error_message,
                                                     l_error_code,
                                                     -99,
                                                     l_dm_batch_source_id,
                                                     l_cm_batch_source_id,
                                                     l_receipt_method_id);
            fnd_file.put_line (
               fnd_file.LOG,
                  'Request ID for AR Invoice Import Program Is :'
               || g_request_id);
         END IF;                                  -- IF l_err_flag != 'Y' THEN
      END IF;                                      -- IF p_conc_prg_name = 'Y'

      fnd_file.put_line (
         fnd_file.LOG,
         '----------------------------------------------------------------------------------');
      fnd_file.put_line (fnd_file.LOG, 'End of Procedure : LOAD_DEBIT_MEMOS');
      fnd_file.put_line (
         fnd_file.LOG,
         '----------------------------------------------------------------------------------');
   EXCEPTION
      WHEN OTHERS
      THEN
         ROLLBACK;
         l_error_code := SQLCODE;
         l_error_message := SUBSTR (SQLERRM, 1, 200);
         fnd_file.put_line (fnd_file.LOG,
                            'Exception Block Debit Memos Import Procedure');
         fnd_file.put_line (fnd_file.LOG,
                            'The Error Code Traced is : ' || l_error_code);
         fnd_file.put_line (
            fnd_file.LOG,
            'The Error Message Traced is : ' || l_error_message);

         g_retcode := 2;
         g_err_msg := 'Error loading AR Debit Memos interface table';
   END load_debit_memos;

   /********************************************************************************
   ProcedureName : update_acct_global_var
   Purpose       : API used to update Global Variables with Account Details.

   HISTORY
   ===============================================================================
   VERSION DATE          AUTHOR(S)       DESCRIPTION
   ------- -----------   --------------- -----------------------------------------
   1.0     06/18/2012    Gopi Damuluri    Initial version.
   ********************************************************************************/
   PROCEDURE update_acct_global_var
   IS
      l_program_error   EXCEPTION;

      l_err_msg         VARCHAR2 (3000);
      l_err_code        NUMBER;
      l_sec             VARCHAR2 (255);
      -- Error DEBUG
      l_err_callfrom    VARCHAR2 (75)
         DEFAULT 'XXWC_AR_INV_INT_PKG.UC4_CALL.UPDATE_ACCT_GLOBAL_VAR';
      l_err_callpoint   VARCHAR2 (75) DEFAULT 'START';
   BEGIN
      BEGIN
         SELECT description
           INTO g_rec_acct
           FROM ar_lookups
          WHERE     1 = 1
                AND lookup_type = 'XXWC_PRISM_SALES_ACCOUNTS'
                AND lookup_code = 'RECEIVABLE';
      EXCEPTION
         WHEN OTHERS
         THEN
            l_err_msg :=
               'Error deriving RECEIVABLE Account from lookup XXWC_PRISM_SALES_ACCOUNTS';
            RAISE l_program_error;
      END;

      BEGIN
         SELECT description
           INTO g_frt_acct
           FROM ar_lookups
          WHERE     1 = 1
                AND lookup_type = 'XXWC_PRISM_SALES_ACCOUNTS'
                AND lookup_code = 'FREIGHT';
      EXCEPTION
         WHEN OTHERS
         THEN
            l_err_msg :=
               'Error deriving FREIGHT Account from lookup XXWC_PRISM_SALES_ACCOUNTS';
            RAISE l_program_error;
      END;

      BEGIN
         SELECT description
           INTO g_dflt_tax_acct
           FROM ar_lookups
          WHERE     1 = 1
                AND lookup_type = 'XXWC_PRISM_SALES_ACCOUNTS'
                AND lookup_code = 'TAX';
      EXCEPTION
         WHEN OTHERS
         THEN
            l_err_msg :=
               'Error deriving TAX Account from lookup XXWC_PRISM_SALES_ACCOUNTS';
            RAISE l_program_error;
      END;
   EXCEPTION
      WHEN l_program_error
      THEN
         ROLLBACK;
         DBMS_OUTPUT.put_line (l_err_msg);
         fnd_file.put_line (fnd_file.LOG, l_err_msg);
         fnd_file.put_line (fnd_file.output, l_err_msg);

         xxcus_error_pkg.xxcus_error_main_api (
            p_called_from         => l_err_callfrom,
            p_calling             => l_err_callpoint,
            p_request_id          => fnd_global.conc_request_id,
            p_ora_error_msg       => SQLERRM,
            p_error_desc          => 'Error running XXWC_AR_INV_INT_PKG package with PROGRAM ERROR',
            p_distribution_list   => ppl_dflt_email,
            p_module              => 'AR');
      WHEN OTHERS
      THEN
         l_err_msg :=
               l_sec
            || l_err_msg
            || ' ERROR '
            || SQLCODE
            || SUBSTR (SQLERRM, 1, 2000);
         DBMS_OUTPUT.put_line (l_err_msg);
         fnd_file.put_line (fnd_file.LOG, l_err_msg);
         fnd_file.put_line (fnd_file.output, l_err_msg);

         xxcus_error_pkg.xxcus_error_main_api (
            p_called_from         => l_err_callfrom,
            p_calling             => l_err_callpoint,
            p_request_id          => fnd_global.conc_request_id,
            p_ora_error_msg       => SUBSTR (l_err_msg, 1, 2000),
            p_error_desc          => 'Error running XXWC_AR_INV_INT_PKG package with OTHERS Exception',
            p_distribution_list   => ppl_dflt_email,
            p_module              => 'AR');
   END update_acct_global_var;

   /********************************************************************************
   ProcedureName : update_dm_acct_global_var
   Purpose       : API used to update Global Variables with Account Details.

   HISTORY
   ===============================================================================
   VERSION DATE          AUTHOR(S)       DESCRIPTION
   ------- -----------   --------------- -----------------------------------------
   1.0     06/18/2012    Gopi Damuluri    Initial version.
   1.1     06/29/2012    Gopi Damuluri    Derive Revenue Account for First Hawaiin
   ********************************************************************************/
   PROCEDURE update_dm_acct_global_var
   IS
      l_program_error   EXCEPTION;

      l_err_msg         VARCHAR2 (3000);
      l_err_code        NUMBER;
      l_sec             VARCHAR2 (255);
      -- Error DEBUG
      l_err_callfrom    VARCHAR2 (75)
         DEFAULT 'XXWC_AR_INV_INT_PKG.UC4_CALL.UPDATE_DM_ACCT_GLOBAL_VAR';
      l_err_callpoint   VARCHAR2 (75) DEFAULT 'START';
   BEGIN
      BEGIN
         SELECT description
           INTO g_dm_rec_acct
           FROM ar_lookups
          WHERE     1 = 1
                AND lookup_type = 'XXWC_PRISM_SALES_ACCOUNTS'
                AND lookup_code = 'DM_RECEIVABLE';
      EXCEPTION
         WHEN OTHERS
         THEN
            l_err_msg :=
               'Error deriving DM RECEIVABLE Account from lookup XXWC_PRISM_SALES_ACCOUNTS';
            RAISE l_program_error;
      END;

      BEGIN
         SELECT description
           INTO g_dm_rev_acct
           FROM ar_lookups
          WHERE     1 = 1
                AND lookup_type = 'XXWC_PRISM_SALES_ACCOUNTS'
                AND lookup_code = 'DM_REVENUE';
      EXCEPTION
         WHEN OTHERS
         THEN
            l_err_msg :=
               'Error deriving DM REVENUE Account from lookup XXWC_PRISM_SALES_ACCOUNTS';
            RAISE l_program_error;
      END;

      BEGIN
         SELECT description
           INTO g_dm_rev_acct_fh
           FROM ar_lookups
          WHERE     1 = 1
                AND lookup_type = 'XXWC_PRISM_SALES_ACCOUNTS'
                AND lookup_code = 'DM_REVENUE_FH';
      EXCEPTION
         WHEN OTHERS
         THEN
            l_err_msg :=
               'Error deriving DM FH REVENUE Account from lookup XXWC_PRISM_SALES_ACCOUNTS';
            RAISE l_program_error;
      END;
   EXCEPTION
      WHEN l_program_error
      THEN
         ROLLBACK;
         DBMS_OUTPUT.put_line (l_err_msg);
         fnd_file.put_line (fnd_file.LOG, l_err_msg);
         fnd_file.put_line (fnd_file.output, l_err_msg);

         xxcus_error_pkg.xxcus_error_main_api (
            p_called_from         => l_err_callfrom,
            p_calling             => l_err_callpoint,
            p_request_id          => fnd_global.conc_request_id,
            p_ora_error_msg       => SQLERRM,
            p_error_desc          => 'Error running XXWC_AR_INV_INT_PKG package with PROGRAM ERROR',
            p_distribution_list   => ppl_dflt_email,
            p_module              => 'AR');
      WHEN OTHERS
      THEN
         l_err_msg :=
               l_sec
            || l_err_msg
            || ' ERROR '
            || SQLCODE
            || SUBSTR (SQLERRM, 1, 2000);
         DBMS_OUTPUT.put_line (l_err_msg);
         fnd_file.put_line (fnd_file.LOG, l_err_msg);
         fnd_file.put_line (fnd_file.output, l_err_msg);

         xxcus_error_pkg.xxcus_error_main_api (
            p_called_from         => l_err_callfrom,
            p_calling             => l_err_callpoint,
            p_request_id          => fnd_global.conc_request_id,
            p_ora_error_msg       => SUBSTR (l_err_msg, 1, 2000),
            p_error_desc          => 'Error running XXWC_AR_INV_INT_PKG package with OTHERS Exception',
            p_distribution_list   => ppl_dflt_email,
            p_module              => 'AR');
   END update_dm_acct_global_var;

   PROCEDURE update_rev_amt (p_errbuf          OUT VARCHAR2,
                             p_retcode         OUT NUMBER,
                             p_trx_number   IN     VARCHAR2)
   IS
      CURSOR cur_inv
      IS
           SELECT trx_number,
                  amount,
                  MIN (line_number) min_line_number,
                  SUM (NVL (revenue_amount, 0)) revenue_amount,
                  SUM (NVL (freight_amount, 0)) freight_amount,
                  stg_attribute2 tax_amount,
                  (  amount
                   - (  SUM (NVL (revenue_amount, 0))
                      + SUM (NVL (freight_amount, 0))
                      + stg_attribute2))
                     diff
             FROM xxwc.xxwc_ar_inv_stg_tbl
            WHERE     error_message LIKE
                         '%InvoiceAmount does not match sum of Revenue, Tax and Freight Amounts%'
                  AND status = 'REJECTED'
                  AND trx_number = NVL (p_trx_number, trx_number)
         GROUP BY trx_number, stg_attribute2, amount;

      l_err_msg         VARCHAR2 (3000);
      l_sec             VARCHAR2 (255);
      l_err_callfrom    VARCHAR2 (255) := 'UPDATE_REV_AMT ';
      l_err_callpoint   VARCHAR2 (75) DEFAULT 'START';
   BEGIN
      FOR rec_inv IN cur_inv
      LOOP
         IF ABS (rec_inv.diff) <= 1
         THEN
            l_sec := 'Update Revenue Amount';

            UPDATE xxwc.xxwc_ar_inv_stg_tbl
               SET revenue_amount = revenue_amount + rec_inv.diff
             WHERE     1 = 1
                   AND trx_number = rec_inv.trx_number
                   AND line_number = rec_inv.min_line_number;
         END IF;
      END LOOP;
   EXCEPTION
      WHEN OTHERS
      THEN
         l_err_msg :=
               l_sec
            || l_err_msg
            || ' ERROR '
            || SQLCODE
            || SUBSTR (SQLERRM, 1, 2000);
         DBMS_OUTPUT.put_line (l_err_msg);
         fnd_file.put_line (fnd_file.LOG, l_err_msg);
         fnd_file.put_line (fnd_file.output, l_err_msg);

         xxcus_error_pkg.xxcus_error_main_api (
            p_called_from         => l_err_callfrom,
            p_calling             => l_err_callpoint,
            p_request_id          => fnd_global.conc_request_id,
            p_ora_error_msg       => SUBSTR (l_err_msg, 1, 2000),
            p_error_desc          => 'Error running XXWC_AR_INV_INT_PKG package with OTHERS Exception',
            p_distribution_list   => ppl_dflt_email,
            p_module              => 'AR');
   END update_rev_amt;

   /********************************************************************************
   ProcedureName : UC4_UPDATE_REV
   Purpose       : UC4 submits the Concurrent Program
                   "XXWC Prism To EBS AR Invoice Interface - Update Revenue Amount"

   HISTORY
   ===============================================================================
   VERSION DATE          AUTHOR(S)       DESCRIPTION
   ------- -----------   --------------- -----------------------------------------
   1.0     03/28/2013    Kathy Poling    Initial version. Task ID: 20130325-00749
                                         Service Ticket 190048
   ********************************************************************************/
   PROCEDURE uc4_update_rev (p_errbuf                   OUT VARCHAR2,
                             p_retcode                  OUT NUMBER,
                             p_conc_prg_name         IN     VARCHAR2,
                             p_user_name             IN     VARCHAR2,
                             p_responsibility_name   IN     VARCHAR2,
                             p_org_name              IN     VARCHAR2)
   IS
      --
      -- Package Variables
      --

      l_req_id                NUMBER NULL;
      v_phase                 VARCHAR2 (50);
      v_status                VARCHAR2 (50);
      v_dev_status            VARCHAR2 (50);
      v_dev_phase             VARCHAR2 (50);
      v_message               VARCHAR2 (250);
      v_error_message         VARCHAR2 (3000);
      l_err_msg               VARCHAR2 (3000);
      l_err_code              NUMBER;
      l_sec                   VARCHAR2 (255);
      l_statement             VARCHAR2 (9000);
      l_user_id               fnd_user.user_id%TYPE;
      l_responsibility_id     NUMBER;
      l_resp_application_id   NUMBER;

      -- Error DEBUG
      l_err_callfrom          VARCHAR2 (75)
                                 DEFAULT 'XXWC_AR_INV_INT_PKG.UC4_UPDATE_REV';
      l_err_callpoint         VARCHAR2 (75) DEFAULT 'START';
   BEGIN
      p_retcode := 0;

      --------------------------------------------------------------------------
      -- Deriving UserId
      --------------------------------------------------------------------------
      BEGIN
         SELECT user_id
           INTO l_user_id
           FROM fnd_user
          WHERE     1 = 1
                AND user_name = UPPER (p_user_name)
                AND SYSDATE BETWEEN start_date
                                AND NVL (end_date, TRUNC (SYSDATE) + 1);
      EXCEPTION
         WHEN NO_DATA_FOUND
         THEN
            l_err_msg :=
               'UserName - ' || p_user_name || ' not defined in Oracle';
            RAISE PROGRAM_ERROR;
         WHEN OTHERS
         THEN
            l_err_msg :=
               'Error deriving user_id for UserName - ' || p_user_name;
            RAISE PROGRAM_ERROR;
      END;

      --------------------------------------------------------------------------
      -- Deriving ResponsibilityId and ResponsibilityApplicationId
      --------------------------------------------------------------------------
      BEGIN
         SELECT responsibility_id, application_id
           INTO l_responsibility_id, l_resp_application_id
           FROM fnd_responsibility_vl
          WHERE     responsibility_name = p_responsibility_name
                AND SYSDATE BETWEEN start_date
                                AND NVL (end_date, TRUNC (SYSDATE) + 1);
      EXCEPTION
         WHEN NO_DATA_FOUND
         THEN
            l_err_msg :=
                  'Responsibility - '
               || p_responsibility_name
               || ' not defined in Oracle';
            RAISE PROGRAM_ERROR;
         WHEN OTHERS
         THEN
            l_err_msg :=
                  'Error deriving Responsibility_id for ResponsibilityName - '
               || p_responsibility_name;
            RAISE PROGRAM_ERROR;
      END;

      fnd_file.put_line (fnd_file.LOG,
                         'Concurrent Program Name - ' || p_conc_prg_name);
      l_sec :=
         'UC4 call to run concurrent request - Prism To EBS AR Invoice Interface - Update Revenue Amount';

      --------------------------------------------------------------------------
      -- Apps Initialize
      --------------------------------------------------------------------------
      fnd_global.apps_initialize (l_user_id,
                                  l_responsibility_id,
                                  l_resp_application_id);

      l_err_callpoint := 'Before submitting Concurrent request';
      --------------------------------------------------------------------------
      -- Submit "XXWC Create Outbound File Common Program"
      --------------------------------------------------------------------------
      l_req_id :=
         fnd_request.submit_request (application   => 'XXWC',
                                     program       => p_conc_prg_name,
                                     description   => NULL,
                                     start_time    => SYSDATE,
                                     sub_request   => FALSE,
                                     argument1     => NULL);
      l_err_callpoint := 'After submitting Concurrent request';

      COMMIT;

      DBMS_OUTPUT.put_line ('After fnd_request');

      IF (l_req_id != 0)
      THEN
         IF fnd_concurrent.wait_for_request (l_req_id,
                                             30,
                                             15000,
                                             v_phase,
                                             v_status,
                                             v_dev_phase,
                                             v_dev_status,
                                             v_message)
         THEN
            v_error_message :=
                  CHR (10)
               || 'ReqID='
               || l_req_id
               || ' DPhase '
               || v_dev_phase
               || ' DStatus '
               || v_dev_status
               || CHR (10)
               || ' MSG - '
               || v_message;

            -- Error Returned
            IF v_dev_phase != 'COMPLETE' OR v_dev_status != 'NORMAL'
            THEN
               l_statement :=
                     'An error occured running the XXWC_AR_INV_INT_PKG '
                  || v_error_message
                  || '.';
               fnd_file.put_line (fnd_file.LOG, l_statement);
               fnd_file.put_line (fnd_file.output, l_statement);
               RAISE PROGRAM_ERROR;
            END IF;
         -- Then Success!
         ELSE
            l_statement :=
                  'An error occured running the XXWC_AR_INV_INT_PKG '
               || v_error_message
               || '.';
            fnd_file.put_line (fnd_file.LOG, l_statement);
            fnd_file.put_line (fnd_file.output, l_statement);
            RAISE PROGRAM_ERROR;
         END IF;
      ELSE
         l_statement := 'An error occured running the XXWC_AR_INV_INT_PKG ';
         fnd_file.put_line (fnd_file.LOG, l_statement);
         fnd_file.put_line (fnd_file.output, l_statement);
         RAISE PROGRAM_ERROR;
      END IF;

      DBMS_OUTPUT.put_line (l_sec);
      DBMS_OUTPUT.put_line ('Request ID:  ' || l_req_id);
   EXCEPTION
      WHEN PROGRAM_ERROR
      THEN
         ROLLBACK;
         l_err_code := 2;
         l_err_msg := l_sec;
         l_err_msg := l_err_msg || ' ERROR ' || SUBSTR (SQLERRM, 1, 2000);
         DBMS_OUTPUT.put_line (l_err_msg);
         fnd_file.put_line (fnd_file.LOG, l_err_msg);
         fnd_file.put_line (fnd_file.output, l_err_msg);

         xxcus_error_pkg.xxcus_error_main_api (
            p_called_from         => l_err_callfrom,
            p_calling             => l_err_callpoint,
            p_request_id          => l_req_id,
            p_ora_error_msg       => SQLERRM,
            p_error_desc          => 'Error running XXWC_AR_INV_INT_PKG package with PROGRAM ERROR',
            p_distribution_list   => ppl_dflt_email,
            p_module              => 'AR');

         p_retcode := 2;
      WHEN OTHERS
      THEN
         fnd_file.put_line (fnd_file.LOG, l_sec);
         fnd_file.put_line (fnd_file.output, l_sec);
         l_err_code := 2;
         l_err_msg := l_sec;
         l_err_msg :=
               l_sec
            || l_err_msg
            || ' ERROR '
            || SQLCODE
            || SUBSTR (SQLERRM, 1, 2000);
         DBMS_OUTPUT.put_line (l_err_msg);
         fnd_file.put_line (fnd_file.LOG, l_err_msg);
         fnd_file.put_line (fnd_file.output, l_err_msg);

         p_retcode := 2;

         xxcus_error_pkg.xxcus_error_main_api (
            p_called_from         => l_err_callfrom,
            p_calling             => l_err_callpoint,
            p_request_id          => l_req_id,
            p_ora_error_msg       => SUBSTR (l_err_msg, 1, 2000),
            p_error_desc          => 'Error running XXWC_AR_INV_INT_PKG package with OTHERS Exception',
            p_distribution_list   => ppl_dflt_email,
            p_module              => 'AR');
   END uc4_update_rev;
END xxwc_ar_inv_int_pkg;
/
