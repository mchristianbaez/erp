CREATE OR REPLACE PACKAGE BODY XXWC_INV_PEND_TRNS_PROCESS_PKG AS
  /********************************************************************************
  FILE NAME   : XXWC_INV_PEND_TRNS_PROCESS_PKG.pkb

  PROGRAM TYPE: PL/SQL Package Body

  PURPOSE     : Automate processing for Inventory Transactions stuck in mtl transaction interface table

  HISTORY
  ===============================================================================
  VERSION DATE          AUTHOR(S)       DESCRIPTION
  ------- -----------   --------------- -----------------------------------------
  1.0     11/05/2015    Keerthi SM      TMS20150715-00148 Initial version.
  1.1     06/16/2017    Pattabhi Avula  TMS#20160826-00027 - Reprocessing the error
                                        Transaction
  ********************************************************************************/

-- Declaring Package Global Variables

g_req_id       NUMBER(20)    := fnd_global.conc_request_id;
g_distro_list  VARCHAR2(100) := 'HDSOracleDevelopers@hdsupply.com';

PROCEDURE del_inv_int_err_prc(X_ERRORBUF  OUT VARCHAR2
                             ,X_RETCODE   OUT NUMBER) IS

  /********************************************************************************
  FILE NAME   : del_inv_int_err_prc

  PROGRAM TYPE: PL/SQL Procedure

  PURPOSE     : Deleting the Transaction Quantity Cannot be ZeroError Records from 
                interface tables

  HISTORY
  ===============================================================================
  VERSION DATE          AUTHOR(S)       DESCRIPTION
  ------- -----------   --------------- -----------------------------------------
  1.0     11/05/2015    Keerthi SM      TMS20150715-00148 Initial version.
  1.1     06/16/2017    Pattabhi Avula  TMS#20160826-00027 - Reprocessing the error
                                        Transaction
  ********************************************************************************/

  l_lkp_value            VARCHAR2(200);
  l_err_msg              VARCHAR2(200);
  l_sec                  VARCHAR2(100);
  l_ora_msg              VARCHAR2(1000);
  FATAL_ERROR            EXCEPTION;
  EXECUTION_ERROR        EXCEPTION;
  
  -- <START> Vern# 1.1
  l_processing_return    NUMBER;
  l_return_status        VARCHAR2 (240);
  l_msg_count            NUMBER;
  l_msg_data             VARCHAR2 (4000);
  l_trans_count          NUMBER;
  l_log_sequence         NUMBER;
  l_error_msg            VARCHAR2 (4000);
  -- <END> Vern# 1.1

CURSOR cur_inv_int_err(p_error_msg VARCHAR2) IS
  -- SELECT transaction_interface_id  Vern#1.1
  SELECT transaction_interface_id, transaction_header_id --  Vern#1.1
  FROM   mtl_transactions_interface mti
  WHERE  mti.error_explanation = p_error_msg;

BEGIN
  l_sec:='Fetching the Error message value from the lookup';
  fnd_file.put_line(fnd_file.log,'Fetching the Error message value from the lookup');  --  Vern#1.1
   BEGIN
     SELECT  meaning
     INTO    l_lkp_value
     FROM    fnd_lookup_values
     WHERE   lookup_type  = 'XXWC_INV_INT_DELETE'
     AND     lookup_code  = 'ERROR_TRANSACTION'
     AND     language     = 'US'
     AND     enabled_flag = 'Y';
   EXCEPTION
    WHEN NO_DATA_FOUND THEN
      l_err_msg := ' No Data Found' || SUBSTR (SQLERRM, 1, 250);
    RAISE FATAL_ERROR;
    WHEN OTHERS THEN
      l_err_msg := ' Error occured while fetchIing the lookup value' ||SQLERRM;
   END;
    fnd_file.put_line(fnd_file.log,'Fetching the Error message value from the lookup and values is '||l_lkp_value);  --  Vern#1.1
     l_sec:='Before Starting the for loop';

     FOR rec_inv_int_err IN cur_inv_int_err(l_lkp_value) LOOP
       BEGIN
         DELETE
		 FROM  mtl_transactions_interface
		 WHERE source_code='ORDER ENTRY'
		 AND   transaction_interface_id=rec_inv_int_err.transaction_interface_id;
         l_sec:='Deleted the eligible data from the interface';
       EXCEPTION
         WHEN OTHERS THEN 
				l_err_msg := 'Unable to delete the order entry source code interface data';
				l_ora_msg := SUBSTR (SQLERRM, 1, 250);
				RAISE EXECUTION_ERROR;
				ROLLBACK;
       END;
    -- END LOOP;  -- Vern# 1.1
	 fnd_file.put_line(fnd_file.log,'Deleted the eligible data from the Interface table');  --  Vern#1.1
	 COMMIT;
     l_sec:='Before Updating the mtl_transactions_interface table error messages as null';
	-- FOR rec_inv_int_err1 IN cur_inv_int_err(l_lkp_value) LOOP      --  Vern#1.1
		BEGIN
         UPDATE  mtl_transactions_interface
           SET   process_flag        	  = 1
           ,     lock_flag           	  = 2
           ,     transaction_mode    	  = 3
           ,     validation_required 	  = 1
           ,     error_code          	  = NULL
           ,     error_explanation   	  = NULL
           WHERE process_flag        	  = 3
		   AND 	 source_code              = 'ORDER ENTRY'  -- Vern# 1.1
		  -- AND   transaction_interface_id = rec_inv_int_err1.transaction_interface_id; -- Vern# 1.1
		  AND   transaction_header_id = rec_inv_int_err.transaction_header_id;  -- Vern# 1.1
       EXCEPTION
         WHEN OTHERS THEN
				l_err_msg:='Unable to update the Process flag, Lock flag, Transaction mode and validation required';
				l_ora_msg := SUBSTR (SQLERRM, 1, 250);
				RAISE FATAL_ERROR;
				ROLLBACK;
       END;
	   COMMIT;  -- Vern# 1.1
	    -- <START> Vern# 1.1
	  /* fnd_file.put_line(fnd_file.log,'Updated the data in mtl_transactions_interface table');	  
	   l_processing_return := NULL;
       l_return_status := NULL;
       l_msg_count := NULL;
       l_msg_data := NULL;
       l_trans_count := NULL;
       fnd_file.put_line(fnd_file.log,' Calling the API to process the transactions');
       l_processing_return :=
                  inv_txn_manager_pub.process_transactions (
                     p_api_version        => 1.0,
                     p_init_msg_list      => fnd_api.g_true,
                     p_commit             => fnd_api.g_true,
                     p_validation_level   => fnd_api.g_valid_level_full,
                     x_return_status      => l_return_status,
                     x_msg_count          => l_msg_count,
                     x_msg_data           => l_msg_data,
                     x_trans_count        => l_trans_count,
                     p_table              => 1,
                     p_header_id          => rec_inv_int_err.transaction_header_id);
					 
        fnd_file.put_line(fnd_file.log,'API status is '||l_processing_return);
		
		fnd_file.put_line(fnd_file.log,'API return status is '||l_return_status);

               IF l_return_status = fnd_api.g_ret_sts_success
               THEN
                  l_return_status := NULL;
			   ELSE
                RAISE EXECUTION_ERROR;
				ROLLBACK;
               END IF;   */	   
	   -- <END> Vern# 1.1
	   
	 END LOOP;
	   COMMIT;

EXCEPTION
      WHEN FATAL_ERROR THEN
         FND_FILE.PUT_LINE (FND_FILE.LOG, 'Error Message: ' || l_err_msg);
		 X_RETCODE := 2;
	
	  WHEN EXECUTION_ERROR THEN
         xxcus_error_pkg.xxcus_error_main_api (
            p_called_from         => 'XXWC_INV_PEND_TRNS_PROCESS_PKG.del_inv_int_err_prc',
            p_calling             => l_sec,
            p_request_id          => g_req_id,
            p_ora_error_msg       => l_ora_msg,
            p_error_desc          => l_err_msg,
            p_distribution_list   => g_distro_list,
            p_module              => 'INV');
			ROLLBACK;
			X_RETCODE := 2;
 
	  WHEN OTHERS THEN
         l_ora_msg := SUBSTR (SQLERRM, 1, 250);

         xxcus_error_pkg.xxcus_error_main_api (
            p_called_from         => 'XXWC_INV_PEND_TRNS_PROCESS_PKG.del_inv_int_err_prc',
            p_calling             => l_sec,
            p_request_id          => g_req_id,
            p_ora_error_msg       => l_ora_msg,
            p_error_desc          => l_err_msg,
            p_distribution_list   => g_distro_list,
            p_module              => 'INV');
			ROLLBACK;
			X_RETCODE := 2;
END del_inv_int_err_prc;

PROCEDURE upd_inv_int_sysd_prc(X_ERRORBUF  OUT VARCHAR2
                              ,X_RETCODE   OUT NUMBER) IS
							  
	/********************************************************************************
  FILE NAME   : upd_inv_int_sysd_prc

  PROGRAM TYPE: PL/SQL Procedure

  PURPOSE     : Closed period transactions updating to open period date or sysdate

  HISTORY
  ===============================================================================
  VERSION DATE          AUTHOR(S)       DESCRIPTION
  ------- -----------   --------------- -----------------------------------------
  1.0     11/05/2015    Keerthi SM      TMS20150715-00148 Initial version.
  ********************************************************************************/						  
							  

  l_update_msg    VARCHAR2(200);
  l_sec           VARCHAR2(100);
  l_msg           VARCHAR2(1000);
  l_err_msg       VARCHAR2(200);
  l_ora_msg       VARCHAR2(1000);
  FATAL_ERROR     EXCEPTION;
  EXECUTION_ERROR EXCEPTION;
  
  CURSOR cur_inv_int_update(p_update_msg VARCHAR2) IS
  SELECT transaction_interface_id
  FROM   mtl_transactions_interface mti
  WHERE  mti.error_explanation      = p_update_msg;

BEGIN
  l_sec:='Fetching the required message from the lookup to update';
  BEGIN
    SELECT meaning
    INTO   l_update_msg
    FROM   fnd_lookup_values
    WHERE  lookup_type= 'XXWC_INV_INT_DELETE'
    AND    lookup_code  ='ACCOUNT_PERIOD'
    AND    language     ='US'
    AND    enabled_flag ='Y';
  EXCEPTION
	WHEN NO_DATA_FOUND THEN
      l_err_msg := ' No Data Found' || SUBSTR (SQLERRM, 1, 250);
    RAISE FATAL_ERROR;
     WHEN OTHERS THEN
      l_err_msg := ' Error occured while fetch the lookup value' ||SQLERRM;
    ROLLBACK;
  END;

    l_sec:='Before Starting the for loop';
    FOR rec_inv_int_update IN cur_inv_int_update(l_update_msg) LOOP
      BEGIN
        UPDATE mtl_transactions_interface mti
        SET    mti.transaction_date     = SYSDATE
        WHERE  source_code				='ORDER ENTRY'
		AND    transaction_interface_id = rec_inv_int_update.transaction_interface_id;
        l_sec:='Update the eligible data from the interface';		
      EXCEPTION
        WHEN OTHERS THEN
				l_err_msg := 'Unable to update to sysdate for order entry source code'||SQLERRM;
				l_ora_msg := SUBSTR (SQLERRM, 1, 250);
				RAISE EXECUTION_ERROR;
				ROLLBACK;
      END;
    END LOOP;
	COMMIT;
	l_sec:='Before Updating the mtl_transactions_interface table account period as null';
	 FOR rec_inv_int_update1 IN cur_inv_int_update(l_update_msg) LOOP
       BEGIN
         UPDATE  mtl_transactions_interface
           SET   process_flag        = 1
           ,     lock_flag           = 2
           ,     transaction_mode    = 3
           ,     validation_required = 1
           ,     error_code          = NULL
           ,     error_explanation   = NULL
           WHERE process_flag        = 3
		   AND    transaction_interface_id = rec_inv_int_update1.transaction_interface_id;
       EXCEPTION
         WHEN OTHERS THEN
				l_err_msg:='Unable to update the Process flag, Lock flag, Transaction mode and validation required';
				l_ora_msg := SUBSTR (SQLERRM, 1, 250);
				RAISE FATAL_ERROR;
				ROLLBACK;
       END;
	   END LOOP;
	   COMMIT;

EXCEPTION
      WHEN FATAL_ERROR THEN
         FND_FILE.PUT_LINE (FND_FILE.LOG, 'Error Message: ' || l_err_msg);
		 X_RETCODE := 2;
	
	  WHEN EXECUTION_ERROR THEN
         xxcus_error_pkg.xxcus_error_main_api (
            p_called_from         => 'XXWC_INV_PEND_TRNS_PROCESS_PKG.upd_inv_int_sysd_prc',
            p_calling             => l_sec,
            p_request_id          => g_req_id,
            p_ora_error_msg       => l_ora_msg,
            p_error_desc          => l_err_msg,
            p_distribution_list   => g_distro_list,
            p_module              => 'INV');
			ROLLBACK;
			X_RETCODE := 2;
 
	  WHEN OTHERS THEN
         l_ora_msg := SUBSTR (SQLERRM, 1, 250);

         xxcus_error_pkg.xxcus_error_main_api (
            p_called_from         => 'XXWC_INV_PEND_TRNS_PROCESS_PKG.upd_inv_int_sysd_prc',
            p_calling             => l_sec,
            p_request_id          => g_req_id,
            p_ora_error_msg       => l_ora_msg,
            p_error_desc          => l_err_msg,
            p_distribution_list   => g_distro_list,
            p_module              => 'INV');
			ROLLBACK;
			X_RETCODE := 2;

END upd_inv_int_sysd_prc;

END XXWC_INV_PEND_TRNS_PROCESS_PKG;
/