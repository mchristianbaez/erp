/*************************************************************************
   *   $Header XXWC_CUSTOMER_BALANCE_VW
   *   Module Name: XXWC_CUSTOMER_BALANCE_VW
   *
      PURPOSE:   View used for to generate the customer balances
   *
   *   REVISIONS:
   *   Ver        Date        Author              Description
   *   ---------  ----------  ---------------     -------------------------
   *   1.0        10/07/2014  Pattabhi Avula      Initial Version and TMS#20141001-00057 
   *   1.1        06/27/2018  Pattabhi Avula      TMS#20180716-00001 - Customer Balances 
   *                                              Program and View Correction changes
   *   1.2        07/27/2018  Pahwa Nancy         TMS#20180727-00068 Remove GL Profile Date Validation
   * ***************************************************************************/
  CREATE OR REPLACE FORCE EDITIONABLE VIEW APPS.XXWC_CUSTOMER_BALANCE_VW(
         ORG_ID, 
		 ACCOUNT_NUMBER, 
		 PRISM_CUST_NUMBER, 
		 PRISM_LEGACY_PARTY_SITE_NUMBER, 
		 CURRENT_BALANCE, 
		 THIRTY_DAYS_BAL, 
		 SIXTY_DAYS_BAL, 
		 NINETY_DAYS_BAL, 
		 ONETWENTY_DAYS_BAL, 
		 OVER_ONETWENTY_DAYS_BAL, 
		 TOTAL_DUE) AS 
  SELECT x1.org_id,
         x1.account_number,
         x1.prism_cust_number,
         x1.prism_legacy_party_site_number,
         0 current_balance,        --SUM (x1.current_balance) current_balance,
         (SUM (x1.current_balance) + SUM (x1.thirty_days_bal)) thirty_days_bal,
         SUM (x1.sixty_days_bal) sixty_days_bal,
         SUM (x1.ninety_days_bal) ninety_days_bal,
         SUM (x1.onetwenty_days_bal) onetwenty_days_bal,
         SUM (x1.over_onetwenty_days_bal) over_onetwenty_days_bal,
         (  SUM (x1.current_balance)
          + SUM (x1.thirty_days_bal)
          + SUM (x1.sixty_days_bal)
          + SUM (x1.ninety_days_bal)
          + SUM (x1.onetwenty_days_bal)
          + SUM (x1.over_onetwenty_days_bal))
            total_due
    FROM (SELECT hou.NAME operating_unit_name,
                 apsa.org_id,
                 hca.account_number account_number,
                 DECODE(INSTR (hcasa.attribute17, '-'),0,hcasa.attribute17,SUBSTR (hcasa.attribute17, 1, INSTR (hcasa.attribute17, '-')-1))  -- Ver#1.1
                    prism_cust_number, -- Ver#1.1
                 DECODE(INSTR (hcasa.attribute17, '-'),0,'',SUBSTR (hcasa.attribute17,INSTR (hcasa.attribute17, '-') + 1))  -- Ver#1.1
                    prism_legacy_party_site_number,-- Ver#1.1
                 hcsua.attribute1 site_type,
                 hcsua.LOCATION site_use_location,
                 hcpc_site.NAME site_profile_class,
                 hcpc_cust.NAME cust_profile_class,
                 (CASE
                     WHEN NVL (hcpc_cust.NAME, 'Unknown') =
                             'Contractor - No Notice'
                     THEN
                        hca.attribute6
                     WHEN     NVL (hcpc_cust.NAME, 'Unknown') !=
                                 'Contractor - No Notice'
                          AND hcasa.attribute17 IS NOT NULL
                     THEN
                        SUBSTR (hcasa.attribute17,
                                1,
                                DECODE (INSTR (hcasa.attribute17,
                                               '-',
                                               1,
                                               1),
                                        0, LENGTH (hcasa.attribute17),
                                          INSTR (hcasa.attribute17,
                                                 '-',
                                                 1,
                                                 1)
                                        - 1))
                     ELSE
                        hca.attribute6
                  END)
                    job_number,
                 apsa.payment_schedule_id,
                 apsa.trx_number,
                 apsa.trx_date,
                 apsa.due_date,
                 (TRUNC (SYSDATE) - TRUNC (apsa.due_date)) days_late,
                 apsa.amount_due_remaining,
                 (CASE
                     WHEN (TRUNC (SYSDATE) - TRUNC (apsa.due_date)) <= 0
                     THEN
                        apsa.amount_due_remaining
                     ELSE
                        0
                  END)
                    current_balance,
                 (CASE
                     WHEN     (TRUNC (SYSDATE) - TRUNC (apsa.due_date)) >= 1
                          AND (TRUNC (SYSDATE) - TRUNC (apsa.due_date)) <= 30
                     THEN
                        apsa.amount_due_remaining
                     ELSE
                        0
                  END)
                    thirty_days_bal,
                 (CASE
                     WHEN     (TRUNC (SYSDATE) - TRUNC (apsa.due_date)) >= 31
                          AND (TRUNC (SYSDATE) - TRUNC (apsa.due_date)) <= 60
                     THEN
                        apsa.amount_due_remaining
                     ELSE
                        0
                  END)
                    sixty_days_bal,
                 (CASE
                     WHEN     (TRUNC (SYSDATE) - TRUNC (apsa.due_date)) >= 61
                          AND (TRUNC (SYSDATE) - TRUNC (apsa.due_date)) <= 90
                     THEN
                        apsa.amount_due_remaining
                     ELSE
                        0
                  END)
                    ninety_days_bal,
                 (CASE
                     WHEN     (TRUNC (SYSDATE) - TRUNC (apsa.due_date)) >= 91
                          AND (TRUNC (SYSDATE) - TRUNC (apsa.due_date)) <= 120
                     THEN
                        apsa.amount_due_remaining
                     ELSE
                        0
                  END)
                    onetwenty_days_bal,
                 (CASE
                     WHEN (TRUNC (SYSDATE) - TRUNC (apsa.due_date)) > 120
                     THEN
                        apsa.amount_due_remaining
                     ELSE
                        0
                  END)
                    over_onetwenty_days_bal
            FROM apps.ar_payment_schedules apsa,
                 ar.hz_cust_accounts hca,
                 apps.hz_cust_site_uses hcsua,
                 apps.hz_cust_acct_sites hcasa,
                 hr_operating_units hou,
                 hz_customer_profiles hcp_site,
                 hz_cust_profile_classes hcpc_site,
                 hz_customer_profiles hcp_cust,
                 hz_cust_profile_classes hcpc_cust
           WHERE     apsa.status = 'OP'
                 AND apsa.org_id = hou.organization_id
                 -- Account Data
                 AND apsa.customer_id = hca.cust_account_id
                 -- Site Use
                 AND apsa.customer_site_use_id = hcsua.site_use_id(+)
                 AND apsa.org_id = hcsua.org_id(+)
                 AND hcsua.cust_acct_site_id = hcasa.cust_acct_site_id(+)
--                 AND TRUNC (hcasa.creation_date) >=                           -- Ver#1.2
--                        TO_DATE (
--                           fnd_profile.VALUE ('XXWC_AR_CONVERSION_GL_DATE'),
--                           'DD-MON-YYYY')                                      -- Ver#1.2
                 -- Account Site
                 -- AND hca.cust_account_id = hcasa.cust_account_id
                 -- Site Use Profile
                 AND apsa.customer_site_use_id = hcp_site.site_use_id(+)
                 AND hcp_site.profile_class_id = hcpc_site.profile_class_id(+)
                 -- Customer profle
                 AND apsa.customer_id = hcp_cust.cust_account_id(+)
                 AND hcp_cust.site_use_id(+) IS NULL
                 AND hcp_cust.profile_class_id = hcpc_cust.profile_class_id(+)
                 AND hcasa.attribute17 IS NOT NULL                          -- Ver#1.1
          UNION
          SELECT hou.NAME operating_unit_name,
                 hcasa.org_id,
                 hca.account_number account_number -- ,hca.attribute6 prism_cust_number
                          -- ,hcasa.attribute17 prism_legacy_party_site_number
                 ,
                 DECODE(INSTR (hcasa.attribute17, '-'),0,hcasa.attribute17,SUBSTR (hcasa.attribute17, 1, INSTR (hcasa.attribute17, '-')-1))  -- Ver#1.1
                    prism_cust_number,  -- Ver#1.1
                 DECODE(INSTR (hcasa.attribute17, '-'),0,'',SUBSTR (hcasa.attribute17,INSTR (hcasa.attribute17, '-') + 1))   -- Ver#1.1
                    prism_legacy_party_site_number,   -- Ver#1.1
                 hcsua.attribute1 site_type,
                 hcsua.LOCATION site_use_location,
                 hcpc_site.NAME site_profile_class,
                 hcpc_cust.NAME cust_profile_class,
                 (CASE
                     WHEN hcasa.attribute17 IS NOT NULL
                     THEN
                        SUBSTR (hcasa.attribute17,
                                1,
                                DECODE (INSTR (hcasa.attribute17,
                                               '-',
                                               1,
                                               1),
                                        0, LENGTH (hcasa.attribute17),
                                          INSTR (hcasa.attribute17,
                                                 '-',
                                                 1,
                                                 1)
                                        - 1))
                     ELSE
                        hca.attribute6
                  END)
                    job_number,
                 NULL sched_id,
                 NULL trx_number,
                 NULL trx_date,
                 NULL due_date,
                 0 days_late,
                 0 amount_due_remaining,
                 0 current_balance,
                 0 thirty_days_bal,
                 0 sixty_days_bal,
                 0 ninety_days_bal,
                 0 onetwenty_days_bal,
                 0 over_onetwenty_days_bal
            FROM hz_cust_accounts hca,
                 apps.hz_cust_acct_sites hcasa,
                 apps.hz_cust_site_uses hcsua,
                 hr_operating_units hou,
                 hz_customer_profiles hcp_site,
                 hz_cust_profile_classes hcpc_site,
                 hz_customer_profiles hcp_cust,
                 hz_cust_profile_classes hcpc_cust
           WHERE     hca.cust_account_id = hcasa.cust_account_id(+)
--                 AND TRUNC (hcasa.creation_date) >=                              -- Ver#1.2
--                        TO_DATE (
--                           fnd_profile.VALUE ('XXWC_AR_CONVERSION_GL_DATE'),
--                           'DD-MON-YYYY')                                         -- Ver#1.2
                 AND hcasa.cust_acct_site_id = hcsua.cust_acct_site_id(+)
                 AND hcasa.org_id = hou.organization_id(+)
                 AND hcsua.site_use_code(+) = 'BILL_TO'
                 -- Site Use Profile
                 AND hcsua.site_use_id = hcp_site.site_use_id(+)
                 AND hcp_site.profile_class_id = hcpc_site.profile_class_id(+)
                 -- Customer profle
                 AND hca.cust_account_id = hcp_cust.cust_account_id(+)
                 AND hcp_cust.site_use_id(+) IS NULL
                 AND hcp_cust.profile_class_id = hcpc_cust.profile_class_id(+)
                 AND hcasa.attribute17 IS NOT NULL) x1          -- Ver#1.1
GROUP BY x1.org_id,
         x1.account_number,
         x1.prism_cust_number,                           --x1.sub_acct_number,
         x1.prism_legacy_party_site_number                 -- Ver#1.1
ORDER BY x1.org_id,
         x1.account_number,
         x1.prism_cust_number,                           --x1.sub_acct_number,
         x1.prism_legacy_party_site_number;             
/