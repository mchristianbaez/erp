-----------------------------------------------------------------------------------------------------------------------------
/*
-- ********************************************************************************************
-- HISTORY
-- ============================================================================================
-- ============================================================================================
-- VERSION DATE          AUTHOR(S)       DESCRIPTION
-- ------- -----------   --------------- ------------------------------------------------------
-- 1.0     18-Apr-2017   Nancy Pahwa   TMS# 20170206-00257
*/
  GRANT INSERT ON "XXWC"."XXWC_B2B_POD_STAGE_TBL" TO "APPS" WITH GRANT OPTION;
  GRANT SELECT ON "XXWC"."XXWC_B2B_POD_STAGE_TBL" TO "APPS" WITH GRANT OPTION;
  GRANT UPDATE ON "XXWC"."XXWC_B2B_POD_STAGE_TBL" TO "APPS" WITH GRANT OPTION;
  GRANT DELETE ON "XXWC"."XXWC_B2B_POD_STAGE_TBL" TO "APPS" WITH GRANT OPTION;
/