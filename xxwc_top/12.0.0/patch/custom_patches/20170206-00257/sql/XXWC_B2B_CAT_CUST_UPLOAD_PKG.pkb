CREATE OR REPLACE PACKAGE BODY APPS.XXWC_B2B_CAT_CUST_UPLOAD_PKG AS
  -- -----------------------------------------------------------------------------
  -- � Copyright 2008, Nancy Pahwa
  -- All Rights Reserved
  --
  -- Name           : insert_data
  -- Date Written   : 11-May-2016
  -- Author         : Nancy Pahwa
  --
  -- Modification History:
  --
  -- Version When         Who        Did what
  -- ------- -----------  --------   -----------------------------------------------------
  -- 1.1     11-May-2016  nancypahwa   Initially Created TMS# 20160223-00029
  -- 1.2     22-May-2016  nancypahwa   Added the position change TMS# 20160223-00029
  -- 1.3     26-APR-2017  Nancy Pahwa  20170206-00257  Initially Created
  ---------------------------------------------------------------------------------
  procedure customer_catalog_upload(p_file in number /*,
                                                                                                                                                                                                                                                                                                                                                                        p_customer_id       in number,
                                                                                                                                                                                                                                                                                                                                                                        p_hash_key          in number,
                                                                                                                                                                                                                                                                                                                                                                        p_cust_catalog_name varchar2*/) IS
    v_blob_data         BLOB;
    v_blob_len          NUMBER;
    v_position          NUMBER;
    v_raw_chunk         RAW(10000);
    v_char              CHAR(1);
    c_chunk_len         number := 1;
    v_line              VARCHAR2(32767) := NULL;
    v_data_array        wwv_flow_global.vc_arr2;
    v_rows              number;
    v_sr_no             number := 12;
    l_statement         varchar2(255);
    l_upload_id         number := p_file;
    l_PROCESS_STATUS    VARCHAR2(5);
    l_VALIDATED_FLAG    VARCHAR2(5);
    l_customer_name     varchar2(360);
    l_cust_catalog_name varchar2(20);
    l_item_number       varchar2(100);
    l_item_description  varchar2(240);
    l_product_catalog   number;
    l_transaction_type  number;
    l_hash_key          number;
    l_count             number;
  BEGIN
    delete from XXWC.XXWC_B2B_CAT_UPLOAD_STAGE_TBL;
    l_statement := 'read data';
    select blob_content
      into v_blob_data
      from XXWC.XXWC_B2B_CAT_UPLOAD_TBL
     where upload_id = p_file;

    v_blob_len := dbms_lob.getlength(v_blob_data);
    dbms_output.put_line('blob content length' || v_blob_len);
    --1.2 start
     -- v_position := 1;
  v_position := dbms_lob.instr(v_blob_data, '0A' ) + 1;  --1.2 end
    l_statement := 'read and convert binary to char';

    l_statement := 'read and convert binary to char';
    -- Read and convert binary to char</span>
    WHILE (v_position <= v_blob_len) LOOP
      l_statement := 'v_raw_chunk';
      v_raw_chunk := dbms_lob.substr(v_blob_data, c_chunk_len, v_position);
      l_statement := 'v_char';
      v_char      := chr(hex_to_decimal(rawtohex(v_raw_chunk)));
      l_statement := 'v_line';
      v_line      := v_line || v_char;
      l_statement := 'v_position';
      v_position  := v_position + c_chunk_len;
      -- When a whole line is retrieved </span>
      IF v_char = CHR(10) THEN

        l_statement := 'replace , with :';
        -- Convert comma to : to use wwv_flow_utilities </span>
        v_line := REPLACE(v_line, ',', ':');
        dbms_output.put_line('v_line: ' || v_line);

        l_statement := 'create arrays of data';
        -- Convert each column separated by : into array of data </span>

        v_data_array := wwv_flow_utilities.string_to_table(v_line);
        L_STATEMENT  := 'GET CUSTOMER RECORD ID';
        dbms_output.put_line('v_data_array: ' || v_data_array(1));
        dbms_output.put_line('v_data_array: ' || v_data_array(2));
        dbms_output.put_line('v_data_array: ' || v_data_array(3));
        dbms_output.put_line('v_data_array: ' || v_data_array(4));
        dbms_output.put_line('v_data_array: ' || v_data_array(5));
        begin
          insert into XXWC.XXWC_B2B_CAT_UPLOAD_STAGE_TBL
            (CUSTOMER_ID,
             CUSTOMER_NAME,
             CATALOG_GROUP_ID,
             ITEM_NUMBER,
             ITEM_DESCRIPTION,
             CUST_CATALOG_NAME,
             TRANSACTION_FLAG,
             INVENTORY_ITEM_ID,
             TRANSACTION_FLAG_NUM)
          values
            (get_customer_id(v_data_array(1)),
             v_data_array(1),
             get_cat_group_id(v_data_array(3)),
             v_data_array(3),
             v_data_array(4),
             v_data_array(2),
             substr(v_data_array(5), 1, 1),
             get_cat_inventory_item_id(v_data_array(3)),
             case when substr(v_data_array(5), 1, 1) = '0' then 0 when
             substr(v_data_array(5), 1, 1) = '1' then 1 else 0 end);
          v_line := NULL;
        end;
      end if;
    END LOOP;
    l_statement := 'merge data into custom table';

    for i in (select customer_id,
                     customer_name,
                     catalog_group_id,
                     item_number,
                     item_description,
                     cust_catalog_name,
                     transaction_flag,
                     inventory_item_id,
                     transaction_flag_num
                from XXWC.XXWC_B2B_CAT_UPLOAD_STAGE_TBL) loop
      begin
        select count(1)
          into l_count
          from XXWC.XXWC_B2B_CAT_CUSTOMER_TBL a
         where a.CUSTOMER_ID = i.CUSTOMER_ID
           and a.cust_catalog_name = i.cust_catalog_name
           and a.item_number = i.item_number;
        l_statement := 'Count match - ' || l_count;
      end;
      if l_count > 0 and i.transaction_flag_num = 1 then
        delete from XXWC.XXWC_B2B_CAT_CUSTOMER_TBL a
         where a.CUSTOMER_ID = i.CUSTOMER_ID
           and a.cust_catalog_name = i.cust_catalog_name
           and a.item_number = i.item_number;
        commit;
      end if;
      if l_count > 0 and i.transaction_flag_num = 0 then
        null;
      end if;
      if l_count = 0 and i.transaction_flag_num = 0 then
        INSERT into XXWC.XXWC_B2B_CAT_CUSTOMER_TBL
          (customer_id,
           catalog_group_id,
           item_number,
           cust_catalog_name,
           is_active,
           hash_key,
           inventory_item_id)
        VALUES
          (i.customer_id,
           i.CATALOG_GROUP_ID,
           i.item_number,
           i.cust_catalog_name,
           'Y',
           --l_hash_key,
           null,
           i.inventory_item_id);
        commit;
        /* else
        if l_count = 0 and i.transaction_flag_num = 1 then
          l_statement := 'No row to be processed within the file ';
          --  l_statement := l_statement || SQLERRM;
          raise_application_error(-20001,
                                  'customer_catalog_upload: ' ||
                                  l_statement);*/
        --   end if;
      end if;
    end loop;

    /* MERGE INTO XXWC.XXWC_B2B_CAT_CUSTOMER_TBL a
    USING (select * from XXWC.XXWC_B2B_CAT_UPLOAD_STAGE_TBL) b
    ON (a.CUSTOMER_ID = b.CUSTOMER_ID and a.cust_catalog_name = b.cust_catalog_name and a.item_number = b.item_number)
    \*  WHEN MATCHED then
            UPDATE set a.is_active = 'Y' DELETE WHERE b.transaction_flag = 1*\
    WHEN NOT MATCHED THEN
      INSERT
        (customer_id,
         catalog_group_id,
         item_number,
         cust_catalog_name,
         is_active,
         hash_key,
         inventory_item_id)
      VALUES
        (b.customer_id,
         b.CATALOG_GROUP_ID,
         b.item_number,
         b.cust_catalog_name,
         'Y',
         --l_hash_key,
         null,
         b.inventory_item_id);*/
    begin
      for i in (select cust_catalog_name
                  from XXWC.XXWC_B2B_CAT_CUSTOMER_TBL
                 where hash_key is null
                 group by cust_catalog_name) loop
        select xxwc.xxwc_b2b_cat_customer_hash_seq.nextval
          into l_hash_key
          from dual;
        update XXWC.XXWC_B2B_CAT_CUSTOMER_TBL
           set hash_key = l_hash_key
         where cust_catalog_name = i.cust_catalog_name;
        commit;
      end loop;
    end;

  EXCEPTION
    WHEN OTHERS THEN
      l_statement := l_statement || SQLERRM;
      raise_application_error(-20001,
                              'customer_catalog_upload: ' || l_statement);

  END customer_catalog_upload;

  FUNCTION get_customer_id(p_customer_name IN varchar2) return number IS
    -- -----------------------------------------------------------------------------
    -- � Copyright 2010, Nancy Pahwa
    -- All Rights Reserved
    --
    -- Name           : get_customer_id
    -- Date Written   : 11-MAY-2016
    -- Author         : Nancy Pahwa
    --
    -- Description : get_dist_code
    --
    -- Dependencies Tables        : None
    -- Dependencies Views         : None
    -- Dependencies Sequences     : None
    -- Dependencies Procedures    : None
    -- Dependencies Functions     : None
    -- Dependencies Packages      : None
    -- Dependencies Types         : None
    -- Dependencies Database Links: None
    --
    -- Modification History:
    --
    -- When         Who        Did what
    -- -----------  --------   -----------------------------------------------------
    -- 11-MAY-2016  Nancy Pahwa   Initially Created
    ---------------------------------------------------------------------------------
    l_customer_id number;
  BEGIN
    begin
      select cust_account_id
        into l_customer_id
        from apps.xxwc_b2b_cat_customer_mv
       where account_name = trim(upper(p_customer_name));
    exception
      when no_data_found then
        l_customer_id := 0;
    end;
    RETURN l_customer_id;
  EXCEPTION
    WHEN OTHERS THEN
      raise_application_error(-20001,
                              'XXWC_B2B_CAT_CUST_UPLOAD_PKG.get_customer_id: ' ||
                              SQLERRM);
  END get_customer_id;
  FUNCTION get_cat_group_id(p_item_number IN varchar2) return number IS
    -- -----------------------------------------------------------------------------
    -- � Copyright 2010, Nancy Pahwa
    -- All Rights Reserved
    --
    -- Name           : get_cat_group_id
    -- Date Written   : 11-MAY-2016
    -- Author         : Nancy Pahwa
    --
    -- Description : get_dist_code
    --
    -- Dependencies Tables        : None
    -- Dependencies Views         : None
    -- Dependencies Sequences     : None
    -- Dependencies Procedures    : None
    -- Dependencies Functions     : None
    -- Dependencies Packages      : None
    -- Dependencies Types         : None
    -- Dependencies Database Links: None
    --
    -- Modification History:
    --
    -- When         Who        Did what
    -- -----------  --------   -----------------------------------------------------
    -- 11-MAY-2016  Nancy Pahwa   Initially Created
    ---------------------------------------------------------------------------------
    l_cat_group_id number;
  BEGIN
    begin
      SELECT a.item_catalog_group_id
        into l_cat_group_id
        FROM MTL_ITEM_CATALOG_GROUPS_B_KFV A, APPS.MTL_SYSTEM_ITEMS B
       WHERE A.ITEM_CATALOG_GROUP_ID = B.ITEM_CATALOG_GROUP_ID
         AND B.ORGANIZATION_ID = 222
         and inventory_item_status_code = 'Active'
         and b.segment1 = trim(p_item_number);
    exception
      when no_data_found then
        l_cat_group_id := 0;
    end;
    RETURN l_cat_group_id;
  EXCEPTION
    WHEN OTHERS THEN
      raise_application_error(-20001,
                              'XXWC_B2B_CAT_CUST_UPLOAD_PKG.get_cat_group_id: ' ||
                              SQLERRM);
  END get_cat_group_id;
  FUNCTION get_cat_inventory_item_id(p_item_number IN varchar2) return number IS
    -- -----------------------------------------------------------------------------
    -- � Copyright 2010, Nancy Pahwa
    -- All Rights Reserved
    --
    -- Name           : get_cat_inventory_item_id
    -- Date Written   : 11-MAY-2016
    -- Author         : Nancy Pahwa
    --
    -- Description : get_dist_code
    --
    -- Dependencies Tables        : None
    -- Dependencies Views         : None
    -- Dependencies Sequences     : None
    -- Dependencies Procedures    : None
    -- Dependencies Functions     : None
    -- Dependencies Packages      : None
    -- Dependencies Types         : None
    -- Dependencies Database Links: None
    --
    -- Modification History:
    --
    -- When         Who        Did what
    -- -----------  --------   -----------------------------------------------------
    -- 11-MAY-2016  Nancy Pahwa   Initially Created
    ---------------------------------------------------------------------------------
    l_cat_inventory_item_id number;
  BEGIN
    begin
      SELECT b.inventory_item_id
        into l_cat_inventory_item_id
        FROM APPS.MTL_SYSTEM_ITEMS B
       WHERE b.inventory_item_status_code = 'Active'
         AND B.ORGANIZATION_ID = 222
         and b.segment1 = trim(p_item_number);
    exception
      when no_data_found then
        l_cat_inventory_item_id := 0;
    end;
    RETURN l_cat_inventory_item_id;
  EXCEPTION
    WHEN OTHERS THEN
      raise_application_error(-20001,
                              'XXWC_B2B_CAT_CUST_UPLOAD_PKG.get_cat_inventory_item_id: ' ||
                              SQLERRM);
  END get_cat_inventory_item_id;
  function hex_to_decimal(p_hex_str in varchar2) return number as
    -- -----------------------------------------------------------------------------
    -- � Copyright 2008, Nancy Pahwa
    -- All Rights Reserved
    --
    -- Name           : hex_to_decimal
    -- Date Written   : 11-MAY-2016
    -- Author         : Nancy PAhwa
    --
    -- Modification History:
    --
    -- When         Who        Did what
    -- -----------  --------   -----------------------------------------------------
    -- 11-MAY-2016  NancyPahwa Initially Created
    ---------------------------------------------------------------------------------

    v_dec number;
    v_hex varchar2(16) := '0123456789ABCDEF';

  begin
    v_dec := 0;
    for indx in 1 .. length(p_hex_str) loop
      v_dec := v_dec * 16 + instr(v_hex, upper(substr(p_hex_str, indx, 1))) - 1;
    end loop;
    return v_dec;
  end hex_to_decimal;
--1.3 Start
 PROCEDURE XXWC_B2B_POD_FILE_UPLOAD_PRC(p_filename     IN VARCHAR2,
                                     p_directory    IN VARCHAR2,
                                     p_location     IN VARCHAR2 DEFAULT NULL,
                                     p_new_filename OUT VARCHAR2) AS
    -- -----------------------------------------------------------------------------
    -- � Copyright 2008, HD Supply
    -- All Rights Reserved
    --
    -- Name           : XXWC_B2B_POD_FILE_UPLOAD_PRC
    -- Date Written   : 26-APRIL-2017
    -- Author         : Nancy Pahwa
    --
    -- Modification History: PACKAGE FOR UPLOAD OF CSV FILE FOR USE OF EXTERNAL TABLE
    --
    -- When         Who             TMS Task        Did what
    -- -----------  --------        --------        ---------------------------------------------
    -- 26-APR-2017  Nancy Pahwa  20170206-00257  Initially Created
    ---------------------------------------------------------------------------------
    l_file           UTL_FILE.file_type;
    l_blob_len       NUMBER;
    l_pos            INTEGER := 1;
    l_amount         BINARY_INTEGER := 32767;
    l_buffer         RAW(32767);
    l_directory      VARCHAR2(500);
    v_new_filename   VARCHAR2(500);
    l_count          NUMBER;
    l_bfilename      VARCHAR2(500);
    v_bfile          BFILE;
    l_year           NUMBER;
    l_timestamp      NUMBER;
    l_directory_path VARCHAR2(200);
    l_sql            VARCHAR2(200);
  BEGIN
    -- Checking if the Directory exists. Creating a directory if it doesn't exist.
    l_directory    := p_directory;
    v_new_filename := SUBSTR(p_filename,
                             INSTR(p_filename, '/') + 1,
                             LENGTH(p_filename));

    --Dbms_Output.Put_Line(l_directory||'  '||v_new_filename);
    v_bfile := BFILENAME(p_directory, v_new_filename);
    l_file  := UTL_FILE.fopen(p_directory, v_new_filename, 'WB', 32760);
    --      IF DBMS_LOB.FILEEXISTS (v_bfile) = 1
    --      THEN
    FOR rec IN (SELECT blob_content lblob
                  FROM apex_application_temp_files
                 WHERE name = p_filename
                   AND id = id) LOOP
      l_blob_len := DBMS_LOB.getlength(rec.lblob);
      WHILE l_pos < l_blob_len LOOP
        DBMS_LOB.read(rec.lblob, l_amount, l_pos, l_buffer);
        UTL_FILE.put_raw(l_file, l_buffer, FALSE);
        l_pos := l_pos + l_amount;
      END LOOP;
    END LOOP;
    UTL_FILE.fclose(l_file);
    --  END IF;
  EXCEPTION
    WHEN OTHERS THEN
      ROLLBACK;
      -- Close the file if something goes wrong.
      IF UTL_FILE.is_open(l_file) THEN
        UTL_FILE.fclose(l_file);
      END IF;

      RAISE;
  END;
--1.3 End
  PROCEDURE XXWC_B2B_FILE_UPLOAD_PRC(p_filename     IN VARCHAR2,
                                     p_directory    IN VARCHAR2,
                                     p_location     IN VARCHAR2 DEFAULT NULL,
                                     p_new_filename OUT VARCHAR2) AS
    -- -----------------------------------------------------------------------------
    -- � Copyright 2008, HD Supply
    -- All Rights Reserved
    --
    -- Name           : XXWC_B2B_FILE_UPLOAD_PRC
    -- Date Written   : 26-MAY-2015
    -- Author         : Christian Baez
    --
    -- Modification History: PACKAGE FOR UPLOAD OF CSV FILE FOR USE OF EXTERNAL TABLE
    --
    -- When         Who             TMS Task        Did what
    -- -----------  --------        --------        ---------------------------------------------
    -- 26-MAY-2015  Christian Baez  20160223-00029  Initially Created
    ---------------------------------------------------------------------------------
    l_file           UTL_FILE.file_type;
    l_blob_len       NUMBER;
    l_pos            INTEGER := 1;
    l_amount         BINARY_INTEGER := 32767;
    l_buffer         RAW(32767);
    l_directory      VARCHAR2(500);
    v_new_filename   VARCHAR2(500);
    l_count          NUMBER;
    l_bfilename      VARCHAR2(500);
    v_bfile          BFILE;
    l_year           NUMBER;
    l_timestamp      NUMBER;
    l_directory_path VARCHAR2(200);
    l_sql            VARCHAR2(200);
  BEGIN
    -- Checking if the Directory exists. Creating a directory if it doesn't exist.
    l_directory    := p_directory;
    v_new_filename := SUBSTR(p_filename,
                             INSTR(p_filename, '/') + 1,
                             LENGTH(p_filename));

    --Dbms_Output.Put_Line(l_directory||'  '||v_new_filename);
    v_bfile := BFILENAME(p_directory, v_new_filename);
    l_file  := UTL_FILE.fopen(p_directory, v_new_filename, 'WB', 32760);
    --      IF DBMS_LOB.FILEEXISTS (v_bfile) = 1
    --      THEN
    FOR rec IN (SELECT blob_content lblob
                  FROM apex_application_files
                 WHERE name = p_filename
                   AND id = id) LOOP
      l_blob_len := DBMS_LOB.getlength(rec.lblob);
      WHILE l_pos < l_blob_len LOOP
        DBMS_LOB.read(rec.lblob, l_amount, l_pos, l_buffer);
        UTL_FILE.put_raw(l_file, l_buffer, FALSE);
        l_pos := l_pos + l_amount;
      END LOOP;
    END LOOP;
    UTL_FILE.fclose(l_file);
    --  END IF;
  EXCEPTION
    WHEN OTHERS THEN
      ROLLBACK;
      -- Close the file if something goes wrong.
      IF UTL_FILE.is_open(l_file) THEN
        UTL_FILE.fclose(l_file);
      END IF;

      RAISE;
  END;
end;
/