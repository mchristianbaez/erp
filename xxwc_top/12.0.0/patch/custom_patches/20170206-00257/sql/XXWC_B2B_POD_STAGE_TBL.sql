  /*******************************************************************************
  Table: "XXWC"."XXWC_B2B_POD_STAGE_TBL" 
  Description: "XXWC"."XXWC_B2B_POD_STAGE_TBL" 
  HISTORY
  ===============================================================================
  VERSION DATE               AUTHOR(S)       DESCRIPTION
  ------- -----------------  --------------- -----------------------------------------
  1.0     18-Apr-2017        Pahwa Nancy   TMS# 20170206-00257 
  ********************************************************************************/
-- Create table
CREATE TABLE "XXWC"."XXWC_B2B_POD_STAGE_TBL" 
   (	"ACCOUNT_NUMBER" VARCHAR2(30), 
	"POD_EMAIL" VARCHAR2(240), 
	"POD_FREQUENCY" VARCHAR2(50), 
	"DELIVER_POD" VARCHAR2(1), 
	"SITE_USE_ID" VARCHAR2(100)
   );
/