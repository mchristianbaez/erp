CREATE OR REPLACE VIEW APPS.XXWC_BRANCH_TOOL_ITEMS_VW AS
-----------------------------------------------------------------------------
  -- � Copyright 2008, Nancy Pahwa
  -- All Rights Reserved
  --
  -- Name           : APPS.XXWC_BRANCH_TOOL_ITEMS_VW 
  -- Date Written   : 10-AUG-2017
  -- Author         : Nancy Pahwa
  --
  -- Modification History:
  --
  -- Version When         Who        Did what
  -- ------- -----------  --------   -----------------------------------------------------
  -- 1.1    10-AUG-2017  nancypahwa   Initially Created TMS# 20170810-00056
  ---------------------------------------------------------------------------------
/*select m.partnumber || ' - ' || shortdescription d , m.partnumber r from apps.XXWC_MD_SEARCH_PRODUCTS_MV m
where m.partnumber not like '#%'
and m.inventory_item_id in
       (select c.inventory_item_id
          from apps.mtl_item_categories_v c, apps.mtl_categories mc
         where c.CATEGORY_ID = mc.CATEGORY_ID and c.STRUCTURE_ID = mc.STRUCTURE_ID
         and mc.ATTRIBUTE5 = '0007' and c.CATEGORY_SET_NAME = 'Inventory Category');*/
SELECT m.partnumber || ' - ' || shortdescription d, m.partnumber r
  FROM apps.XXWC_MD_SEARCH_PRODUCTS_MV m
 WHERE     1 = 1                                  --m.partnumber NOT LIKE '#%'
       AND m.inventory_item_id IN (SELECT c.inventory_item_id
                                     FROM apps.mtl_item_categories_v c,
                                          apps.mtl_categories mc
                                    WHERE     c.CATEGORY_ID = mc.CATEGORY_ID
                                          AND c.STRUCTURE_ID =
                                                 mc.STRUCTURE_ID
                                          AND mc.ATTRIBUTE5 = '0007'
                                          AND c.organization_id = 222
                                          AND c.CATEGORY_SET_NAME =
                                                 'Inventory Category')
       AND NOT EXISTS
                  (SELECT 1
                     FROM APPS.MTL_SYSTEM_ITEMS_B msib
                    WHERE     msib.ORGANIZATION_ID = 222
                          AND msib.SEGMENT1 LIKE '#%'
                          AND msib.INVENTORY_ITEM_ID = m.inventory_item_id
                          )
                          and m.ITEM_TYPE not like 'SPECIAL'
and m.partnumber not like 'SP/%';