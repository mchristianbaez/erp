/*************************************************************************
 Copyright (c) 2012 HD Supply
 All rights reserved.
**************************************************************************
   $Header GL.XXWC_OBIEE_GL_IMPRT_REFNCES_N5 $
  Module Name: GL.XXWC_OBIEE_GL_IMPRT_REFNCES_N5

  PURPOSE:

  REVISIONS:
 VERSION DATE          AUTHOR(S)   TMS TASK            DESCRIPTION
 ------- -----------   ------------ ---------          -------------------
 1.0     27-JUN-2017   Pattabhi.A  TMS#20170626-00129  Initially Created 
**************************************************************************/
CREATE INDEX GL.XXWC_OBIEE_GL_IMPRT_REFNCES_N5 ON GL.GL_IMPORT_REFERENCES(LAST_UPDATE_DATE)
LOGGING
NOPARALLEL;
/