--Report Name            : Open Sales Orders Report
--Import Instructions    : Run this file in XXEIS user from SQL*Plus or any tool like Toad, SQL Navigator. 
prompt Creating View Data for Open Sales Orders Report
set scan off define off
DECLARE
BEGIN 
--Inserting View EIS_XXWC_OM_OPEN_ORDERS_V
xxeis.eis_rs_ins.v( 'EIS_XXWC_OM_OPEN_ORDERS_V',660,'','','','','SA059956','XXEIS','Eis Xxwc Om Open Orders V','EXOOOV','','');
--Delete View Columns for EIS_XXWC_OM_OPEN_ORDERS_V
xxeis.eis_rs_utility.delete_view_rows('EIS_XXWC_OM_OPEN_ORDERS_V',660,FALSE);
--Inserting View Columns for EIS_XXWC_OM_OPEN_ORDERS_V
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_OPEN_ORDERS_V','ORDER_LINE_STATUS',660,'Order Line Status','ORDER_LINE_STATUS','','','','SA059956','VARCHAR2','','','Order Line Status','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_OPEN_ORDERS_V','ORDER_AMOUNT',660,'Order Amount','ORDER_AMOUNT','','','','SA059956','NUMBER','','','Order Amount','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_OPEN_ORDERS_V','SCHEDULE_SHIP_DATE',660,'Schedule Ship Date','SCHEDULE_SHIP_DATE','','','','SA059956','DATE','','','Schedule Ship Date','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_OPEN_ORDERS_V','SALES_PERSON_NAME',660,'Sales Person Name','SALES_PERSON_NAME','','','','SA059956','VARCHAR2','','','Sales Person Name','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_OPEN_ORDERS_V','ZIP_CODE',660,'Zip Code','ZIP_CODE','','','','SA059956','VARCHAR2','','','Zip Code','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_OPEN_ORDERS_V','SHIP_TO_CITY',660,'Ship To City','SHIP_TO_CITY','','','','SA059956','VARCHAR2','','','Ship To City','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_OPEN_ORDERS_V','CUSTOMER_JOB_NAME',660,'Customer Job Name','CUSTOMER_JOB_NAME','','','','SA059956','VARCHAR2','','','Customer Job Name','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_OPEN_ORDERS_V','WAREHOUSE',660,'Warehouse','WAREHOUSE','','','','SA059956','VARCHAR2','','','Warehouse','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_OPEN_ORDERS_V','ORDER_TYPE',660,'Order Type','ORDER_TYPE','','','','SA059956','VARCHAR2','','','Order Type','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_OPEN_ORDERS_V','CUSTOMER_NAME',660,'Customer Name','CUSTOMER_NAME','','','','SA059956','VARCHAR2','','','Customer Name','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_OPEN_ORDERS_V','CUSTOMER_NUMBER',660,'Customer Number','CUSTOMER_NUMBER','','','','SA059956','VARCHAR2','','','Customer Number','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_OPEN_ORDERS_V','SHIPPING_METHOD',660,'Shipping Method','SHIPPING_METHOD','','','','SA059956','VARCHAR2','','','Shipping Method','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_OPEN_ORDERS_V','ORDERED_DATE',660,'Ordered Date','ORDERED_DATE','','','','SA059956','DATE','','','Ordered Date','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_OPEN_ORDERS_V','ORDER_NUMBER',660,'Order Number','ORDER_NUMBER','','','','SA059956','NUMBER','','','Order Number','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_OPEN_ORDERS_V','CREATED_BY',660,'Created By','CREATED_BY','','','','SA059956','VARCHAR2','','','Created By','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_OPEN_ORDERS_V','QUOTE_NUMBER',660,'Quote Number','QUOTE_NUMBER','','','','SA059956','NUMBER','','','Quote Number','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_OPEN_ORDERS_V','ORDER_HEADER_STATUS',660,'Order Header Status','ORDER_HEADER_STATUS','','','','SA059956','VARCHAR2','','','Order Header Status','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_OPEN_ORDERS_V','PAYMENT_TERMS',660,'Payment Terms','PAYMENT_TERMS','','','','SA059956','VARCHAR2','','','Payment Terms','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_OPEN_ORDERS_V','ITEM_DESCRIPTION',660,'Item Description','ITEM_DESCRIPTION','','','','SA059956','VARCHAR2','','','Item Description','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_OPEN_ORDERS_V','ITEM_NUMBER',660,'Item Number','ITEM_NUMBER','','','','SA059956','VARCHAR2','','','Item Number','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_OPEN_ORDERS_V','QTY',660,'Qty','QTY','','','','SA059956','NUMBER','','','Qty','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_OPEN_ORDERS_V','REQUEST_DATE',660,'Request Date','REQUEST_DATE','','','','SA059956','DATE','','','Request Date','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_OPEN_ORDERS_V','USER_ITEM_DESCRIPTION',660,'User Item Description','USER_ITEM_DESCRIPTION','','','','SA059956','VARCHAR2','','','User Item Description','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_OPEN_ORDERS_V','DELIVERY_DOC_DATE',660,'Delivery Doc Date','DELIVERY_DOC_DATE','','','','SA059956','DATE','','','Delivery Doc Date','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_OPEN_ORDERS_V','HZPS_SHP_TO_PRT_ID',660,'Hzps Shp To Prt Id','HZPS_SHP_TO_PRT_ID','','','','SA059956','NUMBER','','','Hzps Shp To Prt Id','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_OPEN_ORDERS_V','HZCS_SHIP_TO_STE_ID',660,'Hzcs Ship To Ste Id','HZCS_SHIP_TO_STE_ID','','','','SA059956','NUMBER','','','Hzcs Ship To Ste Id','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_OPEN_ORDERS_V','MTP_ORGANIZATION_ID',660,'Mtp Organization Id','MTP_ORGANIZATION_ID','','','','SA059956','NUMBER','','','Mtp Organization Id','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_OPEN_ORDERS_V','TRANSACTION_TYPE_ID',660,'Transaction Type Id','TRANSACTION_TYPE_ID','','','','SA059956','NUMBER','','','Transaction Type Id','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_OPEN_ORDERS_V','PARTY_ID',660,'Party Id','PARTY_ID','','','','SA059956','NUMBER','','','Party Id','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_OPEN_ORDERS_V','SALESREP_ID',660,'Salesrep Id','SALESREP_ID','','','','SA059956','NUMBER','','','Salesrep Id','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_OPEN_ORDERS_V','CUST_ACCT_SITE_ID',660,'Cust Acct Site Id','CUST_ACCT_SITE_ID','','','','SA059956','NUMBER','','','Cust Acct Site Id','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_OPEN_ORDERS_V','CUST_ACCOUNT_ID',660,'Cust Account Id','CUST_ACCOUNT_ID','','','','SA059956','NUMBER','','','Cust Account Id','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_OPEN_ORDERS_V','ORDER_LINE_ID',660,'Order Line Id','ORDER_LINE_ID','','','','SA059956','NUMBER','','','Order Line Id','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_OPEN_ORDERS_V','ORDER_HEADER_ID',660,'Order Header Id','ORDER_HEADER_ID','','','','SA059956','NUMBER','','','Order Header Id','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_OPEN_ORDERS_V','LINE_CREATION_DATE',660,'Line Creation Date','LINE_CREATION_DATE','','','','SA059956','DATE','','','Line Creation Date','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_OPEN_ORDERS_V','HEADER_STATUS',660,'Header Status','HEADER_STATUS','','','','SA059956','VARCHAR2','','','Header Status','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_OPEN_ORDERS_V','PROCESS_ID',660,'Process Id','PROCESS_ID','','','','SA059956','NUMBER','','','Process Id','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_OPEN_ORDERS_V','TRANSACTION_PHASE_CODE',660,'Transaction Phase Code','TRANSACTION_PHASE_CODE','','','','SA059956','VARCHAR2','','','Transaction Phase Code','','','');
--Inserting View Components for EIS_XXWC_OM_OPEN_ORDERS_V
xxeis.eis_rs_ins.vcomp( 'EIS_XXWC_OM_OPEN_ORDERS_V','HZ_CUST_ACCOUNTS',660,'HZ_CUST_ACCOUNTS','HCA','HCA','SA059956','SA059956','-1','Stores Information About Customer Accounts.','','','','');
xxeis.eis_rs_ins.vcomp( 'EIS_XXWC_OM_OPEN_ORDERS_V','HZ_PARTIES',660,'HZ_PARTIES','HZP','HZP','SA059956','SA059956','-1','Information About Parties Such As Organizations, P','','','','');
xxeis.eis_rs_ins.vcomp( 'EIS_XXWC_OM_OPEN_ORDERS_V','MTL_PARAMETERS',660,'MTL_PARAMETERS','MTP','MTP','SA059956','SA059956','-1','Inventory Control Options And Defaults','','','','');
xxeis.eis_rs_ins.vcomp( 'EIS_XXWC_OM_OPEN_ORDERS_V','HZ_CUST_ACCT_SITES',660,'HZ_CUST_ACCT_SITES_ALL','HCAS_SHIP_TO','HCAS_SHIP_TO','SA059956','SA059956','-1','Stores All Customer Account Sites Across All Opera','','','','');
--Inserting View Component Joins for EIS_XXWC_OM_OPEN_ORDERS_V
xxeis.eis_rs_ins.vcj( 'EIS_XXWC_OM_OPEN_ORDERS_V','HZ_CUST_ACCOUNTS','HCA',660,'EXOOOV.CUST_ACCOUNT_ID','=','HCA.CUST_ACCOUNT_ID(+)','','','','Y','SA059956','','');
xxeis.eis_rs_ins.vcj( 'EIS_XXWC_OM_OPEN_ORDERS_V','HZ_PARTIES','HZP',660,'EXOOOV.PARTY_ID','=','HZP.PARTY_ID(+)','','','','Y','SA059956','','');
xxeis.eis_rs_ins.vcj( 'EIS_XXWC_OM_OPEN_ORDERS_V','MTL_PARAMETERS','MTP',660,'EXOOOV.MTP_ORGANIZATION_ID','=','MTP.ORGANIZATION_ID(+)','','','','Y','SA059956','','');
xxeis.eis_rs_ins.vcj( 'EIS_XXWC_OM_OPEN_ORDERS_V','HZ_CUST_ACCT_SITES','HCAS_SHIP_TO',660,'EXOOOV.CUST_ACCT_SITE_ID','=','HCAS_SHIP_TO.CUST_ACCT_SITE_ID(+)','','','','Y','SA059956','','');
END;
/
set scan on define on
prompt Creating Report LOV Data for Open Sales Orders Report
set scan off define off
DECLARE
BEGIN 
--Inserting Report LOVs - Open Sales Orders Report
xxeis.eis_rs_ins.lov( 660,'select  RS.Name,SALESREP_ID  from  RA_SALESREPS RS
WHERE  RS.NAME is not null','','OM SALES REP','This gives the sales representative name','XXEIS_RS_ADMIN',NULL,'N','','');
xxeis.eis_rs_ins.lov( 660,'select distinct ott.name order_type,ott.description description,ott.transaction_type_id order_type_id from oe_transaction_types_tl ott','','OM ORDER TYPE','This gives the Order Type','XXEIS_RS_ADMIN',NULL,'N','','');
xxeis.eis_rs_ins.lov( 660,'select cust_acct.CUST_ACCOUNT_ID,nvl( party.party_name,CUST_ACCT.account_name) customer_name,CUST_ACCT.ACCOUNT_NUMBER
from  HZ_PARTIES PARTY, HZ_CUST_ACCOUNTS CUST_ACCT where CUST_ACCT.PARTY_ID = PARTY.PARTY_ID AND  PARTY.status=''A''','','XXOM CUSTOMER NAME','This gives the Customer Name','XXEIS_RS_ADMIN',NULL,'Y','','');
xxeis.eis_rs_ins.lov( 660,'select  cust_acct.CUST_ACCOUNT_ID,cust_acct.account_number Customer_Number,cust_acct.account_name customer_name,party.party_name
from  HZ_PARTIES PARTY, HZ_CUST_ACCOUNTS CUST_ACCT
 where CUST_ACCT.PARTY_ID = PARTY.PARTY_ID','','XXOM CUSTOMER NUMBER','This gives the Customer Number','XXEIS_RS_ADMIN',NULL,'Y','','');
xxeis.eis_rs_ins.lov( 660,'select meaning ship_method,description from FND_LOOKUP_VALUES_vl where lookup_type=''SHIP_METHOD''','','OM SHIP METHOD','','XXEIS_RS_ADMIN',NULL,'N','','');
xxeis.eis_rs_ins.lov( 660,'SELECT ppf.full_name  created_by, fu.user_name user_name
  FROM FND_USER fu, per_people_f ppf
 WHERE TRUNC(SYSDATE) BETWEEN fu.START_DATE AND NVL(fu.end_date, hr_general.end_of_time)
   and TRUNC(SYSDATE) BETWEEN ppf.effective_start_date AND ppf.effective_end_date
   and fu.employee_id = ppf.person_id (+)
   and exists
          (select 1
             from fnd_user_resp_groups furg,
                  fnd_responsibility_vl fr
            where furg.responsibility_id   = fr.responsibility_id
              and fu.user_id = furg.user_id
              and fr.responsibility_name like ''XXEIS%'')','','Created By Lov','','XXEIS_RS_ADMIN',NULL,'N','','');
xxeis.eis_rs_ins.lov( 660,'Select  name, description from ra_terms_vl','','WC OM Payment Terms','','XXEIS_RS_ADMIN',NULL,'N','','');
xxeis.eis_rs_ins.lov( 660,'SELECT DISTINCT LOCATION FROM hz_cust_site_uses','','OM Customer Job Name LOV','','XXEIS_RS_ADMIN',NULL,'N','','');
xxeis.eis_rs_ins.lov( 660,'SELECT ORGANIZATION_CODE WAREHOUSE,ORGANIZATION_NAME FROM ORG_ORGANIZATION_DEFINITIONS OOD WHERE SYSDATE < NVL(OOD.DISABLE_DATE,SYSDATE+1) AND EXISTS (SELECT 1 FROM XXEIS.EIS_ORG_ACCESS_V  WHERE organization_id = ood.organization_id )
UNION
SELECT ''All'', ''All Organizations'' from Dual','','OM Warehouse All','','ANONYMOUS',NULL,'N','','');
xxeis.eis_rs_ins.lov( 660,'SELECT meaning Status
  FROM OE_LOOKUPS lv
 WHERE lookup_type = ''LINE_FLOW_STATUS''
  AND lookup_code not in(''CLOSED'', ''CANCELLED'')','','WC Open Order Line Status','','ANONYMOUS',NULL,'N','','');
xxeis.eis_rs_ins.lov( 660,'SELECT upper(meaning) Status
  FROM OE_LOOKUPS lv
 WHERE lookup_type = ''FLOW_STATUS''
   AND lookup_code not in(''CANCELLED'', ''CLOSED'')','','XXWC ORDER FLOW STATUS CODE','','PK059658',NULL,'Y','','');
END;
/
set scan on define on
prompt Creating Report Data for Open Sales Orders Report
set scan off define off
DECLARE
BEGIN 
--Deleting Report data - Open Sales Orders Report
xxeis.eis_rs_utility.delete_report_rows( 'Open Sales Orders Report' );
--Inserting Report - Open Sales Orders Report
xxeis.eis_rs_ins.r( 660,'Open Sales Orders Report','','Open orders report by customer, by job, by salesperson, by created by, by shipping method, by promise date.','','','','SA059956','EIS_XXWC_OM_OPEN_ORDERS_V','Y','','','SA059956','','N','White Cap Reports','','CSV,Pivot Excel,EXCEL,','N');
--Inserting Report Columns - Open Sales Orders Report
xxeis.eis_rs_ins.rc( 'Open Sales Orders Report',660,'ORDER_HEADER_STATUS','Order Header Status','Order Header Status','','','default','','10','N','','','','','','','','SA059956','N','N','','EIS_XXWC_OM_OPEN_ORDERS_V','','');
xxeis.eis_rs_ins.rc( 'Open Sales Orders Report',660,'PAYMENT_TERMS','Payment Terms','Payment Terms','','','default','','16','N','','','','','','','','SA059956','N','N','','EIS_XXWC_OM_OPEN_ORDERS_V','','');
xxeis.eis_rs_ins.rc( 'Open Sales Orders Report',660,'ITEM_DESCRIPTION','Item Description','Item Description','','','default','','12','N','','','','','','','','SA059956','N','N','','EIS_XXWC_OM_OPEN_ORDERS_V','','');
xxeis.eis_rs_ins.rc( 'Open Sales Orders Report',660,'ITEM_NUMBER','Item Number','Item Number','','','default','','11','N','','','','','','','','SA059956','N','N','','EIS_XXWC_OM_OPEN_ORDERS_V','','');
xxeis.eis_rs_ins.rc( 'Open Sales Orders Report',660,'QTY','Qty','Qty','','~,~.~0','default','','14','N','','','','','','','','SA059956','N','N','','EIS_XXWC_OM_OPEN_ORDERS_V','','');
xxeis.eis_rs_ins.rc( 'Open Sales Orders Report',660,'REQUEST_DATE','Requested Date','Request Date','','','default','','7','N','','','','','','','','SA059956','N','N','','EIS_XXWC_OM_OPEN_ORDERS_V','','');
xxeis.eis_rs_ins.rc( 'Open Sales Orders Report',660,'CUSTOMER_NUMBER','Customer Number','Customer Number','','','default','','17','N','','','','','','','','SA059956','N','N','','EIS_XXWC_OM_OPEN_ORDERS_V','','');
xxeis.eis_rs_ins.rc( 'Open Sales Orders Report',660,'ORDERED_DATE','Ordered Date','Ordered Date','','','default','','6','N','','','','','','','','SA059956','N','N','','EIS_XXWC_OM_OPEN_ORDERS_V','','');
xxeis.eis_rs_ins.rc( 'Open Sales Orders Report',660,'ORDER_AMOUNT','Order Amount','Order Amount','','~T~D~2','default','','20','N','','','','','','','','SA059956','N','N','','EIS_XXWC_OM_OPEN_ORDERS_V','','');
xxeis.eis_rs_ins.rc( 'Open Sales Orders Report',660,'ORDER_LINE_STATUS','Order Line Status','Order Line Status','','','default','','15','N','','','','','','','','SA059956','N','N','','EIS_XXWC_OM_OPEN_ORDERS_V','','');
xxeis.eis_rs_ins.rc( 'Open Sales Orders Report',660,'ORDER_NUMBER','Order Number','Order Number','','~~~','default','','4','N','','','','','','','','SA059956','N','N','','EIS_XXWC_OM_OPEN_ORDERS_V','','');
xxeis.eis_rs_ins.rc( 'Open Sales Orders Report',660,'ORDER_TYPE','Order Type','Order Type','','','default','','9','N','','','','','','','','SA059956','N','N','','EIS_XXWC_OM_OPEN_ORDERS_V','','');
xxeis.eis_rs_ins.rc( 'Open Sales Orders Report',660,'SCHEDULE_SHIP_DATE','Schedule Ship Date','Schedule Ship Date','','','default','','24','N','','','','','','','','SA059956','N','N','','EIS_XXWC_OM_OPEN_ORDERS_V','','');
xxeis.eis_rs_ins.rc( 'Open Sales Orders Report',660,'SHIPPING_METHOD','Shipping Method','Shipping Method','','','default','','21','N','','','','','','','','SA059956','N','N','','EIS_XXWC_OM_OPEN_ORDERS_V','','');
xxeis.eis_rs_ins.rc( 'Open Sales Orders Report',660,'ZIP_CODE','Zip Code','Zip Code','','','default','','23','N','','','','','','','','SA059956','N','N','','EIS_XXWC_OM_OPEN_ORDERS_V','','');
xxeis.eis_rs_ins.rc( 'Open Sales Orders Report',660,'WAREHOUSE','Warehouse','Warehouse','','','default','','1','N','','','','','','','','SA059956','N','N','','EIS_XXWC_OM_OPEN_ORDERS_V','','');
xxeis.eis_rs_ins.rc( 'Open Sales Orders Report',660,'SHIP_TO_CITY','Ship To City','Ship To City','','','default','','22','N','','','','','','','','SA059956','N','N','','EIS_XXWC_OM_OPEN_ORDERS_V','','');
xxeis.eis_rs_ins.rc( 'Open Sales Orders Report',660,'CUSTOMER_JOB_NAME','Customer Job Name','Customer Job Name','','','default','','19','N','','','','','','','','SA059956','N','N','','EIS_XXWC_OM_OPEN_ORDERS_V','','');
xxeis.eis_rs_ins.rc( 'Open Sales Orders Report',660,'CUSTOMER_NAME','Customer Name','Customer Name','','','default','','18','N','','','','','','','','SA059956','N','N','','EIS_XXWC_OM_OPEN_ORDERS_V','','');
xxeis.eis_rs_ins.rc( 'Open Sales Orders Report',660,'SALES_PERSON_NAME','Sales Person Name','Sales Person Name','','','default','','2','N','','','','','','','','SA059956','N','N','','EIS_XXWC_OM_OPEN_ORDERS_V','','');
xxeis.eis_rs_ins.rc( 'Open Sales Orders Report',660,'QUOTE_NUMBER','Quote Number','Quote Number','','~~~','default','','5','N','','','','','','','','SA059956','N','N','','EIS_XXWC_OM_OPEN_ORDERS_V','','');
xxeis.eis_rs_ins.rc( 'Open Sales Orders Report',660,'CREATED_BY','Created By','Created By','','','default','','3','N','','','','','','','','SA059956','N','N','','EIS_XXWC_OM_OPEN_ORDERS_V','','');
xxeis.eis_rs_ins.rc( 'Open Sales Orders Report',660,'USER_ITEM_DESCRIPTION','User Item Description','User Item Description','','','default','','13','N','','','','','','','','SA059956','N','N','','EIS_XXWC_OM_OPEN_ORDERS_V','','');
xxeis.eis_rs_ins.rc( 'Open Sales Orders Report',660,'DELIVERY_DOC_DATE','Delivery Doc Date','Delivery Doc Date','','','','','8','N','','','','','','','','SA059956','N','N','','EIS_XXWC_OM_OPEN_ORDERS_V','','');
--Inserting Report Parameters - Open Sales Orders Report
xxeis.eis_rs_ins.rp( 'Open Sales Orders Report',660,'Job Name','Job Name','CUSTOMER_JOB_NAME','IN','OM Customer Job Name LOV','','VARCHAR2','N','Y','5','','N','CONSTANT','SA059956','Y','N','','','');
xxeis.eis_rs_ins.rp( 'Open Sales Orders Report',660,'Customer Name','Customer Name','CUSTOMER_NAME','IN','XXOM CUSTOMER NAME','','VARCHAR2','N','Y','3','','N','CONSTANT','SA059956','Y','N','','','');
xxeis.eis_rs_ins.rp( 'Open Sales Orders Report',660,'Customer Number','Customer Number','CUSTOMER_NUMBER','IN','XXOM CUSTOMER NUMBER','','VARCHAR2','N','Y','4','','N','CONSTANT','SA059956','Y','N','','','');
xxeis.eis_rs_ins.rp( 'Open Sales Orders Report',660,'Ordered Date From','Ordered Date From','ORDERED_DATE','>=','','','DATE','N','Y','11','','N','CONSTANT','SA059956','Y','N','','','');
xxeis.eis_rs_ins.rp( 'Open Sales Orders Report',660,'Order Type','Order Type','ORDER_TYPE','IN','OM ORDER TYPE','','VARCHAR2','N','Y','2','','N','CONSTANT','SA059956','Y','N','','','');
xxeis.eis_rs_ins.rp( 'Open Sales Orders Report',660,'Sales Person','Sales Person','SALES_PERSON_NAME','IN','OM SALES REP','','VARCHAR2','N','Y','6','','N','CONSTANT','SA059956','Y','N','','','');
xxeis.eis_rs_ins.rp( 'Open Sales Orders Report',660,'Schedule Ship Date From','Schedule Ship Date From','SCHEDULE_SHIP_DATE','>=','','','DATE','N','Y','9','','N','CONSTANT','SA059956','Y','N','','','');
xxeis.eis_rs_ins.rp( 'Open Sales Orders Report',660,'Shipping Method','Shipping Method','SHIPPING_METHOD','IN','OM SHIP METHOD','','VARCHAR2','N','Y','8','','N','CONSTANT','SA059956','Y','N','','','');
xxeis.eis_rs_ins.rp( 'Open Sales Orders Report',660,'Warehouse','Warehouse','WAREHOUSE','IN','OM Warehouse All','','VARCHAR2','Y','Y','1','','N','CONSTANT','SA059956','Y','N','','','');
xxeis.eis_rs_ins.rp( 'Open Sales Orders Report',660,'Created By','Created By','CREATED_BY','IN','Created By Lov','','VARCHAR2','N','Y','7','','N','CONSTANT','SA059956','Y','N','','','');
xxeis.eis_rs_ins.rp( 'Open Sales Orders Report',660,'Ordered Date To','Ordered Date To','ORDERED_DATE','<=','','','DATE','N','Y','12','','N','CONSTANT','SA059956','Y','N','','','');
xxeis.eis_rs_ins.rp( 'Open Sales Orders Report',660,'Schedule Ship Date To','Schedule Ship Date To','SCHEDULE_SHIP_DATE','<=','','','DATE','N','Y','10','','N','CONSTANT','SA059956','Y','N','','','');
xxeis.eis_rs_ins.rp( 'Open Sales Orders Report',660,'Order Header Status','Order Header Status','ORDER_HEADER_STATUS','IN','XXWC ORDER FLOW STATUS CODE','','VARCHAR2','N','Y','14','','N','CONSTANT','SA059956','Y','N','','','');
xxeis.eis_rs_ins.rp( 'Open Sales Orders Report',660,'Payment Terms','Payment Terms','PAYMENT_TERMS','IN','WC OM Payment Terms','','VARCHAR2','N','Y','15','','Y','CONSTANT','SA059956','Y','N','','','');
xxeis.eis_rs_ins.rp( 'Open Sales Orders Report',660,'Order Line Status','Order Line Status','ORDER_LINE_STATUS','IN','WC Open Order Line Status','','VARCHAR2','N','Y','13','','Y','CONSTANT','SA059956','Y','N','','','');
--Inserting Report Conditions - Open Sales Orders Report
xxeis.eis_rs_ins.rcn( 'Open Sales Orders Report',660,'ORDER_LINE_STATUS','IN',':Order Line Status','','','Y','2','Y','SA059956');
xxeis.eis_rs_ins.rcn( 'Open Sales Orders Report',660,'PAYMENT_TERMS','IN',':Payment Terms','','','Y','3','Y','SA059956');
xxeis.eis_rs_ins.rcn( 'Open Sales Orders Report',660,'','','','','and PROCESS_ID = :SYSTEM.PROCESS_ID','Y','1','','SA059956');
--Inserting Report Sorts - Open Sales Orders Report
xxeis.eis_rs_ins.rs( 'Open Sales Orders Report',660,'WAREHOUSE','ASC','SA059956','','');
xxeis.eis_rs_ins.rs( 'Open Sales Orders Report',660,'ORDER_NUMBER','ASC','SA059956','','');
--Inserting Report Triggers - Open Sales Orders Report
xxeis.eis_rs_ins.rt( 'Open Sales Orders Report',660,'begin
xxeis.eis_xxwc_om_open_sales_ord_pkg.OPEN_SALES_ORDER_PRE_PROC(
p_process_id          => :SYSTEM.PROCESS_ID,
p_org_id                => :Warehouse,
p_order_type          => :Order Type,
p_cust_name          =>   :Customer Name,
p_cust_num            => :Customer Number,
p_cust_job_name    => :Job Name,
p_salesrep_name    =>    :Sales Person,
P_CREATED_BY                =>  :Created By,
p_shipping_method          =>  :Shipping Method,
p_schedule_date_from     => :Schedule Ship Date From,
p_schedule_date_to         => :Schedule Ship Date To,
p_ordered_date_from      =>  :Ordered Date From,
p_ordered_date_to          =>  :Ordered Date To,
p_order_line_status         => :Order Line Status,
p_order_header_status    =>  :Order Header Status,
p_payment_terms      =>   :Payment Terms);
end;','B','Y','SA059956');
xxeis.eis_rs_ins.rt( 'Open Sales Orders Report',660,'begin
xxeis.EIS_XXWC_OM_OPEN_SALES_ORD_PKG.CLEAR_TEMP_TABLES( 
p_process_id =>:SYSTEM.PROCESS_ID);
end;','A','Y','SA059956');
--Inserting Report Templates - Open Sales Orders Report
--Inserting Report Portals - Open Sales Orders Report
--Inserting Report Dashboards - Open Sales Orders Report
--Inserting Report Security - Open Sales Orders Report
xxeis.eis_rs_ins.rsec( 'Open Sales Orders Report','660','','50926',660,'SA059956','','');
xxeis.eis_rs_ins.rsec( 'Open Sales Orders Report','660','','50927',660,'SA059956','','');
xxeis.eis_rs_ins.rsec( 'Open Sales Orders Report','660','','50928',660,'SA059956','','');
xxeis.eis_rs_ins.rsec( 'Open Sales Orders Report','660','','50929',660,'SA059956','','');
xxeis.eis_rs_ins.rsec( 'Open Sales Orders Report','660','','50931',660,'SA059956','','');
xxeis.eis_rs_ins.rsec( 'Open Sales Orders Report','660','','50930',660,'SA059956','','');
xxeis.eis_rs_ins.rsec( 'Open Sales Orders Report','660','','21623',660,'SA059956','','');
xxeis.eis_rs_ins.rsec( 'Open Sales Orders Report','701','','50546',660,'SA059956','','');
xxeis.eis_rs_ins.rsec( 'Open Sales Orders Report','660','','50856',660,'SA059956','','');
xxeis.eis_rs_ins.rsec( 'Open Sales Orders Report','660','','50857',660,'SA059956','','');
xxeis.eis_rs_ins.rsec( 'Open Sales Orders Report','660','','50858',660,'SA059956','','');
xxeis.eis_rs_ins.rsec( 'Open Sales Orders Report','660','','50859',660,'SA059956','','');
xxeis.eis_rs_ins.rsec( 'Open Sales Orders Report','660','','50860',660,'SA059956','','');
xxeis.eis_rs_ins.rsec( 'Open Sales Orders Report','660','','50861',660,'SA059956','','');
xxeis.eis_rs_ins.rsec( 'Open Sales Orders Report','20005','','50880',660,'SA059956','','');
xxeis.eis_rs_ins.rsec( 'Open Sales Orders Report','','LC053655','',660,'SA059956','','');
xxeis.eis_rs_ins.rsec( 'Open Sales Orders Report','','10010432','',660,'SA059956','','');
xxeis.eis_rs_ins.rsec( 'Open Sales Orders Report','','RB054040','',660,'SA059956','','');
xxeis.eis_rs_ins.rsec( 'Open Sales Orders Report','','RV003897','',660,'SA059956','','');
xxeis.eis_rs_ins.rsec( 'Open Sales Orders Report','','SS084202','',660,'SA059956','','');
xxeis.eis_rs_ins.rsec( 'Open Sales Orders Report','','SO004816','',660,'SA059956','','');
xxeis.eis_rs_ins.rsec( 'Open Sales Orders Report','660','','50886',660,'SA059956','','');
xxeis.eis_rs_ins.rsec( 'Open Sales Orders Report','660','','50901',660,'SA059956','','');
xxeis.eis_rs_ins.rsec( 'Open Sales Orders Report','660','','50870',660,'SA059956','','');
xxeis.eis_rs_ins.rsec( 'Open Sales Orders Report','660','','50871',660,'SA059956','','');
xxeis.eis_rs_ins.rsec( 'Open Sales Orders Report','660','','50869',660,'SA059956','','');
xxeis.eis_rs_ins.rsec( 'Open Sales Orders Report','20005','','50900',660,'SA059956','','');
xxeis.eis_rs_ins.rsec( 'Open Sales Orders Report','660','','51044',660,'SA059956','','');
--Inserting Report Pivots - Open Sales Orders Report
xxeis.eis_rs_ins.rpivot( 'Open Sales Orders Report',660,'Pivot','1','1,0|1,2,1','1,1,0,0|None|2');
--Inserting Report Pivot Details For Pivot - Pivot
xxeis.eis_rs_ins.rpivot_dtls( 'Open Sales Orders Report',660,'Pivot','ORDER_AMOUNT','DATA_FIELD','SUM','','1','','');
xxeis.eis_rs_ins.rpivot_dtls( 'Open Sales Orders Report',660,'Pivot','CREATED_BY','ROW_FIELD','','','1','','');
xxeis.eis_rs_ins.rpivot_dtls( 'Open Sales Orders Report',660,'Pivot','CUSTOMER_NAME','ROW_FIELD','','','2','','');
xxeis.eis_rs_ins.rpivot_dtls( 'Open Sales Orders Report',660,'Pivot','ORDER_NUMBER','ROW_FIELD','','','3','','');
--Inserting Report Summary Calculation Columns For Pivot- Pivot
END;
/
set scan on define on
