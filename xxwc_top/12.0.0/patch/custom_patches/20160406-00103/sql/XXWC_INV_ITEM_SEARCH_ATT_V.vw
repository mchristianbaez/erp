CREATE OR REPLACE FORCE VIEW apps.xxwc_inv_item_search_att_v 
/*************************************************************************
   *   $Header xxwc_inv_item_search_att_v.vw $
   *   Module Name: xxwc Advance Item Search ATT view for Form
   *
   *   PURPOSE:   View create script for XXWC Advanced Item Search ATT Form
   *
   *   REVISIONS:
   *   Ver        Date        Author                     Description
   *   ---------  ----------  ---------------         -------------------------
   *   2.0        04-07-2016  Rakesh Patel            TMS#20160406-00101/20160406-00103-PUBD - Modifications to XXWC INV Item Search Form for PUBD
   * ***************************************************************************/
(
   organization_id
 , organization_code
 , organization_name
 , inventory_item_id
 , segment1
 , description
 , unit_weight
 , amu
 , po_cost
 , on_hand_qty
 , item_uom_code
 , minimum_order_quantity
 , fixed_lot_multiplier
 , shelf_life
 , lead_time
 , calc_lead_time
 , velocity_class
 , target_level
 , max_level
 , long_description
 -- 11/26/2013 CG: TMS 20131121-00164: Added new column for vendor part number
 , vendor_part_number
 , PUBD ----Added by Rakesh Patel for TMS#20160406-00101 /20160406-00103(ver 2.0)
)
AS
   (SELECT						 -- Created by Shankar Hariharan on 12-19-2012
			 -- Used in Item Info section of AIS
			 org.organization_id
		   , org.organization_code
		   , org.organization_name
		   , msi.inventory_item_id
		   , msi.segment1
		   , msi.description
		   , msi.unit_weight
		   , msi.attribute20 amu
		   , xxwc_ascp_scwb_pkg.get_po_cost (msi.inventory_item_id
										   , msi.organization_id
										   , NULL
										   , NULL)
				po_cost
		   , xxwc_ascp_scwb_pkg.get_on_hand (msi.inventory_item_id
										   , msi.organization_id
										   , 'RQ') -- Shankar TMS 20130107-00851 17-Jan-2013 changed from H to RQ
				on_hand_qty
		   , msi.primary_uom_code item_uom_code
		   , msi.minimum_order_quantity
		   , msi.fixed_lot_multiplier
		   , msi.shelf_life_days shelf_life
		   , NVL (msi.full_lead_time, 0) lead_time
		   , msi.attribute30 calc_lead_time
		   , xxwc_ascp_scwb_pkg.get_sales_velocity (msi.inventory_item_id
												  , msi.organization_id)
				velocity_class
		   , xxwc_ascp_scwb_pkg.get_inventory_level ('T'
												   , msi.inventory_item_id
												   , msi.organization_id)
				target_level
		   , xxwc_ascp_scwb_pkg.get_inventory_level ('M'
												   , msi.inventory_item_id
												   , msi.organization_id)
				max_level
		   -- 07/23/2013 CG: TMS 20130715-00357: AIS Form Changes. Point #8: Added to pull item long description
		   , msit.long_description
           , SUBSTR(xxwc_mv_routines_pkg.get_item_cross_reference (  msi.inventory_item_id,'VENDOR',1 ),0,300) vendor_part_number
		   , xxwc_ascp_scwb_pkg.get_on_hand (msi.inventory_item_id, org.organization_id, 'PUBD') PUBD --Added by Rakesh Patel for TMS#20160406-00101/20160406-00103
	  FROM	 org_organization_definitions org
		   , mtl_system_items msi
		   , -- 07/23/2013 CG: TMS 20130715-00357: AIS Form Changes. Point #8: Added to pull item long description
			mtl_system_items_tl msit
	 WHERE		 msi.organization_id = org.organization_id
			 -- 07/23/2013 CG: TMS 20130715-00357: AIS Form Changes. Point #8: Added to pull item long description
			 AND msi.inventory_item_id = msit.inventory_item_id
			 AND msi.organization_id = msit.organization_id
			 AND msit.language = USERENV ('LANG'))
/


