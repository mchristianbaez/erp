REM $Header: ap_inv_event_status_code_sel.sql 120.1.12010000.7 2014/09/29 22:36:23 vinaik noship $
REM +=======================================================================+
REM |    Copyright (c) 2001, 2014 Oracle Corporation, Redwood Shores, CA, USA     |
REM |                         All rights reserved.                          |
REM +=======================================================================+
REM | FILENAME                                                              |
REM |     ap_inv_event_status_code_sel.sql                                  |
REM |                                                                       |
REM | DESCRIPTION                                                           |
REM |     This script is used to generate reports for all records in        |
REM |     xla_events that have invalid event_status_code                    |
REM | HISTORY Created by zrehman on 21-April-2008                           |
REM +=======================================================================+
REM dbdrv:none

SET SERVEROUTPUT ON;
SET VERIFY OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK;
WHENEVER OSERROR EXIT FAILURE ROLLBACK;

/*
PROMPT ________________________________________________________
PROMPT
PROMPT Following are the parameters that would be asked for
PROMPT by the script, along with their meanings:
PROMPT
PROMPT Email_id:
PROMPT ========
PROMPT A valid email id to get log output files in email.
PROMPT This is an optional features, to make use of this feature please 
PROMPT Follow the Metalink Note 1582550.1 and pass valid email id. 
PROMPT
PROMPT This feature can be ignored by not passing email id 
PROMPT (just by pressing enter key).
PROMPT
PROMPT ________________________________________________________*/


DECLARE

  l_count		            NUMBER; 
  l_count1		          NUMBER; 
  l_file_location       v$parameter.value%TYPE;   
  l_message             VARCHAR2(500);
  l_exists              NUMBER :=0;
  l_instance_name       VARCHAR2(100);
  l_host_name           VARCHAR2(100);
  l_email_flag          BOOLEAN DEFAULT FALSE;
  l_email_id            VARCHAR2(255) := 'ramabujjayya.talluri@hdsupply.com';
  l_date                VARCHAR2(100);

BEGIN

  AP_Acctg_Data_Fix_PKG.Open_Log_Out_Files
         (6992111||'-diag',l_file_location);
  AP_Acctg_Data_Fix_PKG.Print('<html><body>');
  

  -----------------------------------------------------------------------------
  --               Printing instance details
  -----------------------------------------------------------------------------

  BEGIN
   l_date := to_char(sysdate,'DD-MON-YYYY HH24:MI:SS');
    
    SELECT instance_name, host_name
      INTO l_instance_name, l_host_name
      FROM v$instance;
    AP_Acctg_Data_Fix_PKG.Print('Selection script executed on instance '||
       l_instance_name||' host_name '||l_host_name||' on '|| l_date);
  EXCEPTION
  WHEN OTHERS THEN
    AP_Acctg_Data_Fix_PKG.Print('While querying instance name');
    l_message := 'Exception :: '||SQLERRM||'';
    AP_Acctg_Data_Fix_PKG.Print(l_message);                                                   
    
  END;

  
  --------------------------------------------------------------------------
  -- Step 1: Drop the temporary tables if already exists
  --------------------------------------------------------------------------

 /*********  INCOMPLETE EVENT_STATUS_CODE IN XLA_EVENTS TABLE  **********/

  AP_Acctg_Data_Fix_PKG.Print('Dropping and Creating Driver tables');
  Begin
    Execute Immediate
      'Drop table ap_temp_data_driver_6992111';

  EXCEPTION
    WHEN OTHERS THEN
    AP_Acctg_Data_Fix_PKG.Print('Could not delete ap_temp_data_driver_6992111'
	   ||sqlerrm);
  END;


  Begin
    Execute Immediate
      'CREATE TABLE ap_temp_data_driver_6992111(
        EVENT_ID		NUMBER(15),
	EVENT_DATE              DATE,
        EVENT_STATUS_CODE	VARCHAR2(1),
	PROCESS_STATUS_CODE     VARCHAR2(1),
	EVENT_TYPE_CODE         VARCHAR2(100),
        ENTITY_CODE		VARCHAR2(30),
        SOURCE_ID		NUMBER(15),
	TRANSACTION_NUMBER      VARCHAR2(240),
	BUDGETARY_CONTROL_FLAG  VARCHAR2(1),
	ORG_ID                  NUMBER,
	SET_OF_BOOKS_ID         NUMBER,
	PROCESS_FLAG		VARCHAR2(1)
    )';
  EXCEPTION
     WHEN OTHERS THEN
       fnd_file.put_line(fnd_file.output, 'Could not create '||
                      'ap_temp_data_driver_6992111 '||sqlerrm);
   END;
       
  Begin
    Execute Immediate
     'Insert into ap_temp_data_driver_6992111
          (
           event_id,
	   event_date,
           event_status_code,
	   process_status_code,  
	   event_type_code,
           entity_code,
           source_id,
	   transaction_number,
           budgetary_control_flag,
	   org_id,
	   set_of_books_id,
	   process_flag
           )
     (SELECT xe.event_id,
             xe.event_date,
             xe.event_status_code,
	     xe.process_status_code,
	     xe.event_type_code,
	     xte.entity_code,
             xte.source_id_int_1 source_id,
	     xte.transaction_number,
	     xe.budgetary_control_flag,
	     xte.security_id_int_1,
	     xte.ledger_id,
	     ''Y''
	FROM xla_events xe,
             xla_transaction_entities_upg xte
       WHERE xte.entity_id=xe.entity_id
         AND xte.application_id=200
         AND xe.application_id=200
         AND xe.event_status_code=''I''
         AND NOT exists 
		(SELECT 1 
		 FROM ap_holds_all AH
		 WHERE invoice_id=xte.source_id_int_1 
		 AND xte.ENTITY_CODE=''AP_INVOICES''
                 AND AH.RELEASE_LOOKUP_CODE is null
                 )
         AND NOT EXISTS
		(SELECT 1 
		 FROM ap_holds_all AH
		 WHERE invoice_id IN 
                        (SELECT INVOICE_ID 
			 FROM AP_INVOICE_PAYMENTS_ALL AIP 
                         WHERE AIP.CHECK_ID=xte.source_id_int_1 
			 AND AH.RELEASE_LOOKUP_CODE is null
                         AND xte.ENTITY_CODE=''AP_PAYMENTS''
                          )
		)
         AND NOT EXISTS
                (SELECT 1
                   FROM ap_payment_history_all aph,
                        ap_system_parameters_all asp
                  WHERE asp.org_id = aph.org_id
                    AND aph.pmt_currency_code <> asp.base_currency_code
                    AND aph.pmt_to_base_xrate is null
                    AND aph.check_id = xte.source_id_int_1
                    AND xte.entity_code = ''AP_PAYMENTS'')
     
         AND NOT EXISTS
              (SELECT 1
               FROM ap_invoice_distributions_all aid,
                    financials_system_params_all fsp
               WHERE aid.invoice_id = NVL(XTE.source_id_int_1, -99)
               AND xte.entity_code = ''AP_INVOICES''
               AND xte.ledger_id = aid.set_of_books_id
               AND aid.org_id = fsp.org_id
               AND aid.set_of_books_id = fsp.set_of_books_id 
               AND ( (NVL(fsp.purch_encumbrance_flag,''N'') = ''N''
                      AND aid.match_Status_flag NOT IN (''T'',''A'') )
                      OR ((NVL(fsp.purch_encumbrance_flag,''N'') = ''Y''
                           AND aid.match_Status_flag != ''A''))))
         AND NOT EXISTS
              (SELECT 1
               FROM ap_invoice_distributions_all aid,
                    financials_system_params_all fsp,
                    ap_invoice_payments_all aip
               WHERE aip.check_id = NVL(XTE.source_id_int_1, -99)
               AND aip.invoice_id = aid.invoice_id
               AND xte.entity_code = ''AP_PAYMENTS''
               AND xte.ledger_id = aid.set_of_books_id
               AND aid.org_id = fsp.org_id
               AND aid.set_of_books_id = fsp.set_of_books_id 
               AND ( (NVL(fsp.purch_encumbrance_flag,''N'') = ''N''
                      AND aid.match_Status_flag NOT IN (''T'',''A'') )
                      OR ((NVL(fsp.purch_encumbrance_flag,''N'') = ''Y''
                           AND aid.match_Status_flag != ''A''))))) ';
  EXCEPTION
     WHEN OTHERS THEN
          fnd_file.put_line(fnd_file.output, 'Exception in inserting '||
	  'records into ap_temp_data_driver_6992111 - '||SQLERRM);
  END;  
  
 /*********  FINAL ACCOUNTING ENTRY STATUS IN XLA_AE_HEADERS 
             TABLE  FOR IMCOMPLETE EVENTS                     **********/

  Begin
    Execute Immediate
      'Drop table ap_header_driver_6992111';

  EXCEPTION
    WHEN OTHERS THEN
    AP_Acctg_Data_Fix_PKG.Print('Could not delete ap_header_driver_6992111'
	   ||sqlerrm);
  END;

  Begin
    Execute Immediate
      'CREATE TABLE ap_header_driver_6992111(
        EVENT_ID			                    NUMBER(15),
      	EVENT_DATE                        DATE,
	      AE_HEADER_ID			                NUMBER(15),
	      LEDGER_ID			                    NUMBER(15),
	      BALANCE_TYPE_CODE	                VARCHAR2(1),
	      ACCOUNTING_ENTRY_STATUS_CODE	    VARCHAR2(1),
        EVENT_STATUS_CODE		              VARCHAR2(1),
	      PROCESS_STATUS_CODE		            VARCHAR2(1),
      	EVENT_TYPE_CODE		                VARCHAR2(100),
        ENTITY_CODE			                  VARCHAR2(30),
        SOURCE_ID		                    	NUMBER(15),
	      TRANSACTION_NUMBER                VARCHAR2(240),
	      ORG_ID                            NUMBER,
	      PROCESS_FLAG			                VARCHAR2(1)
        )';
  EXCEPTION
     WHEN OTHERS THEN
     fnd_file.put_line(fnd_file.output, 'Could not create '||
                      'ap_header_driver_6992111 '||sqlerrm);
   END;
       
  Begin
    Execute Immediate
     'Insert into ap_header_driver_6992111
          (
           event_id,
	         event_date,
	         ae_header_id,
	         ledger_id,
	         balance_type_code,
	         accounting_entry_status_code,
           event_status_code,
	         process_status_code,  
	         event_type_code,
           entity_code,
           source_id,
	         transaction_number,
           org_id,
	         process_flag
           )
     (SELECT xah.event_id,
             dr.event_date,
             xah.ae_header_id,
	           xah.ledger_id,
	           xah.balance_type_code,
	           xah.accounting_entry_status_code,
	           dr.event_status_code,
	           dr.process_status_code,
	           dr.event_type_code,
	           dr.entity_code,
	           dr.source_id,
	           dr.transaction_number,
	           dr.org_id,
	           ''Y''
        FROM xla_ae_headers xah,
	           ap_temp_data_driver_6992111 dr 
       WHERE xah.application_id = 200
         AND xah.event_id = dr.event_id    
	 AND xah.upg_batch_id IS NOT NULL  /*Bug 14195137*/
	 AND xah.upg_batch_id <> -9999    /*Bug 14195137*/
	 AND xah.accounting_entry_status_code = ''F''
	 AND nvl(xah.gl_transfer_status_code, ''N'') <> ''Y''
     ) ';

  EXCEPTION
     WHEN OTHERS THEN
     fnd_file.put_line(fnd_file.output, 'Exception in inserting records into '|| 
		                       'ap_header_driver_6992111 - '||SQLERRM);
  END;     

  ------------------------------------------------------------------
  -- Step 3: Report all the affected events in Log file 
  ---------------------------------------------------------------------
  Begin

    Execute Immediate 
    'SELECT count(*) 
       FROM ap_temp_data_driver_6992111' INTO l_count;

  EXCEPTION
     WHEN OTHERS THEN
     fnd_file.put_line(fnd_file.output, 'Exception in getting count from '|| 
		                       'ap_temp_data_driver_6992111 - '||SQLERRM);
  End;  

  IF (l_count > 0) THEN 
    AP_Acctg_Data_Fix_PKG.Print('******* Events with incomplete '||
                                'event status  *******');            
    BEGIN

      AP_Acctg_Data_Fix_PKG.Print_html_table 
        (p_select_list       => 'EVENT_ID,EVENT_DATE,EVENT_STATUS_CODE,PROCESS_STATUS_CODE,'||
				'EVENT_TYPE_CODE,ENTITY_CODE,SOURCE_ID,TRANSACTION_NUMBER,'||         
				'BUDGETARY_CONTROL_FLAG,ORG_ID,SET_OF_BOOKS_ID,PROCESS_FLAG',             
         p_table_in          => 'ap_temp_data_driver_6992111',
         p_where_in          => 'ORDER BY SOURCE_ID,EVENT_ID',
         P_calling_sequence  => 'ap_inv_event_status_code_sel.sql');

    EXCEPTION                                                          
       WHEN OTHERS THEN
       fnd_file.put_line(fnd_file.output, 'Exception in printing to html table->'||
                                    SQLERRM);
    END;
  END IF;
  
  ---------------------------------------------------------------------------

  Begin

    Execute Immediate 
    'SELECT count(*) 
       FROM ap_header_driver_6992111' INTO l_count1;

  EXCEPTION
     WHEN OTHERS THEN
     fnd_file.put_line(fnd_file.output, 'Exception in getting count from '|| 
		                       'ap_header_driver_6992111 - '||SQLERRM);
  End;  

  IF (l_count1 > 0) THEN 
    AP_Acctg_Data_Fix_PKG.Print('******* Headers in Final Status associated '||
                                'to Incomplete events  *******');            
    BEGIN

      AP_Acctg_Data_Fix_PKG.Print_html_table 
        (p_select_list       => 'EVENT_ID,EVENT_DATE,AE_HEADER_ID,LEDGER_ID,'||
	                        'BALANCE_TYPE_CODE,ACCOUNTING_ENTRY_STATUS_CODE,'||
				'EVENT_STATUS_CODE,PROCESS_STATUS_CODE,'||
				'EVENT_TYPE_CODE,ENTITY_CODE,SOURCE_ID,'||
				'TRANSACTION_NUMBER,ORG_ID,PROCESS_FLAG',
         p_table_in          => 'ap_header_driver_6992111',
         p_where_in          => 'ORDER BY SOURCE_ID,EVENT_ID',
         P_calling_sequence  => 'ap_inv_event_status_code_sel.sql');

    EXCEPTION                                                          
       WHEN OTHERS THEN
       fnd_file.put_line(fnd_file.output, 'Exception in printing to html table->'||
                                    SQLERRM);
    END;
  END IF;
  
  ---------------------------------------------------------------------
  -- Step 4: User need to follow the  next  to fix the issue
  ---------------------------------------------------------------------

  IF (l_count > 0 OR l_count1 > 0) THEN

    l_message :=  '_______________________________________'||
                  '_______________________________________';
    AP_Acctg_Data_Fix_PKG.Print(l_message);
    l_message :=  'Following are the next steps to be followed '||
                  'to fix the issue, if any';
    AP_Acctg_Data_Fix_PKG.Print(l_message);
  
    l_message :=  '1. If you don''t want any event'||
                  ' to be fixed then update process_flag in table(s) '||
                  ' ap_temp_data_driver_6992111'||
		              ' to ''N'' for those events. <p>';
    AP_Acctg_Data_Fix_PKG.Print(l_message);
  
    l_message :=  '2. To fix the identified events run '||
                  'ap_inv_event_status_code_fix.sql which is present in '||
  		  '$AP_TOP/patch/115/sql.';
    AP_Acctg_Data_Fix_PKG.Print(l_message);

    l_message :=  '3. If data is not corrected contact'||
                  ' Oracle Support and supply files'||
                  ' 6992111-diag-HH24:MI:SS.html and 6992111-fix-HH24:MI:SS.html.';

    AP_Acctg_Data_Fix_PKG.Print(l_message);
    
      
  ELSE 
    AP_Acctg_Data_Fix_PKG.Print('No Data to be corrected');
  END IF;
  
    AP_Acctg_Data_Fix_PKG.Print('</body></html>');
    AP_Acctg_Data_Fix_PKG.Close_Log_Out_Files;
    IF (l_email_id IS NOT NULL) THEN
        l_email_flag := AP_Acctg_Data_Fix_PKG.send_mail_clob_impl(l_email_id,l_file_location);
    END IF;
    IF (l_email_flag) THEN
       dbms_output.put_line ('Email Sent');
    ELSE
       dbms_output.put_line ('Email Sending Failed');  
    END IF;

  dbms_output.put_line('--------------------------------------------------'||
                         '-----------------------------');
  dbms_output.put_line(l_file_location||' is the log file created');
  dbms_output.put_line('--------------------------------------------------'||
                          '-----------------------------');

EXCEPTION

  WHEN OTHERS THEN
    l_message := 'Exception :: '||SQLERRM;
    AP_Acctg_Data_Fix_PKG.Print(l_message);
    AP_Acctg_Data_Fix_PKG.Print('</body></html>');
    AP_ACCTG_DATA_FIX_PKG.Close_Log_Out_Files;
    
    IF (l_email_id IS NOT NULL) THEN
        l_email_flag := AP_Acctg_Data_Fix_PKG.send_mail_clob_impl(l_email_id,l_file_location);
    END IF;
    IF (l_email_flag) THEN
       dbms_output.put_line ('Email Sent');
    ELSE
       dbms_output.put_line ('Email Sending Failed');  
    END IF;
    
    DBMS_OUTPUT.put_line(l_file_location||' is the log file created');
END;
/
COMMIT;
EXIT;
