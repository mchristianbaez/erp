--TMS#20151002-00007 modified by Mahender Reddy on 11/03/2015
--Report Name            : Orders on Hold Report
--Import Instructions    : Run this file in XXEIS user from SQL*Plus or any tool like Toad, SQL Navigator. 
prompt Creating View Data for Orders on Hold Report
set scan off define off
DECLARE
BEGIN 
--Inserting View EIS_XXWC_OM_IGVP_V
xxeis.eis_rs_ins.v( 'EIS_XXWC_OM_IGVP_V',660,'','','','','MR020532','XXEIS','Eis Xxwc Om Igvp V','EXOIV','','');
--Delete View Columns for EIS_XXWC_OM_IGVP_V
xxeis.eis_rs_utility.delete_view_rows('EIS_XXWC_OM_IGVP_V',660,FALSE);
--Inserting View Columns for EIS_XXWC_OM_IGVP_V
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_IGVP_V','ORDER_LINE_HOLD_NAME',660,'Order Line Hold Name','ORDER_LINE_HOLD_NAME','','','','MR020532','VARCHAR2','','','Order Line Hold Name','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_IGVP_V','ORDER_HEADER_HOLD_NAME',660,'Order Header Hold Name','ORDER_HEADER_HOLD_NAME','','','','MR020532','VARCHAR2','','','Order Header Hold Name','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_IGVP_V','SALES_ORDER_LINE_NUMBER',660,'Sales Order Line Number','SALES_ORDER_LINE_NUMBER','','','','MR020532','VARCHAR2','','','Sales Order Line Number','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_IGVP_V','SALES_ORDER_LINE_STATUS',660,'Sales Order Line Status','SALES_ORDER_LINE_STATUS','','','','MR020532','VARCHAR2','','','Sales Order Line Status','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_IGVP_V','SALES_ORDER_STATUS',660,'Sales Order Status','SALES_ORDER_STATUS','','','','MR020532','VARCHAR2','','','Sales Order Status','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_IGVP_V','SALES_ORDER_NUMBER',660,'Sales Order Number','SALES_ORDER_NUMBER','','','','MR020532','NUMBER','','','Sales Order Number','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_IGVP_V','CUSTOMER_NUMBER',660,'Customer Number','CUSTOMER_NUMBER','','','','MR020532','VARCHAR2','','','Customer Number','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_IGVP_V','CUSTOMER_NAME',660,'Customer Name','CUSTOMER_NAME','','','','MR020532','VARCHAR2','','','Customer Name','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_IGVP_V','INVENTORY_ORGANIZATION',660,'Inventory Organization','INVENTORY_ORGANIZATION','','','','MR020532','VARCHAR2','','','Inventory Organization','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_IGVP_V','ORDERED_DATE',660,'Ordered Date','ORDERED_DATE','','','','MR020532','DATE','','','Ordered Date','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_IGVP_V','UNIT_SELLING_PRICE',660,'Unit Selling Price','UNIT_SELLING_PRICE','','','','MR020532','NUMBER','','','Unit Selling Price','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_IGVP_V','QUANTITY',660,'Quantity','QUANTITY','','','','MR020532','NUMBER','','','Quantity','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_IGVP_V','PART_DESCRIPTION',660,'Part Description','PART_DESCRIPTION','','','','MR020532','VARCHAR2','','','Part Description','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_IGVP_V','PART_NUMBER',660,'Part Number','PART_NUMBER','','','','MR020532','VARCHAR2','','','Part Number','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_IGVP_V','EXTENDED_PRICE',660,'Extended Price','EXTENDED_PRICE','','~T~D~2','','MR020532','NUMBER','','','Extended Price','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_IGVP_V','UNIT_COST',660,'Unit Cost','UNIT_COST','','~T~D~2','','MR020532','NUMBER','','','Unit Cost','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_IGVP_V','ORDER_TYPE',660,'Order Type','ORDER_TYPE','','','','MR020532','VARCHAR2','','','Order Type','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_IGVP_V','CREATED_BY',660,'Created By','CREATED_BY','','','','MR020532','VARCHAR2','','','Created By','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_IGVP_V','EXTENDED_COST',660,'Extended Cost','EXTENDED_COST','','~T~D~2','','MR020532','NUMBER','','','Extended Cost','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_IGVP_V','HOLD_DATE',660,'Hold Date','HOLD_DATE','','','','MR020532','DATE','','','Hold Date','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_IGVP_V','LINE_TYPE',660,'Line Type','LINE_TYPE','','','','MR020532','VARCHAR2','','','Line Type','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_IGVP_V','SPECIAL_COST',660,'Special Cost','SPECIAL_COST','','','','MR020532','NUMBER','','','Special Cost','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_IGVP_V','VQN_MODIFIER_NAME',660,'Vqn Modifier Name','VQN_MODIFIER_NAME','','','','MR020532','VARCHAR2','','','Vqn Modifier Name','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_IGVP_V','MARGIN_PER',660,'Margin Per','MARGIN_PER','','','','MR020532','NUMBER','','','Margin Per','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_IGVP_V','GROSS_PERCENTAGE',660,'Gross Percentage','GROSS_PERCENTAGE','','','','MR020532','NUMBER','','','Gross Percentage','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_IGVP_V','GROSS_PERCENT_DOLLARS',660,'Gross Percent Dollars','GROSS_PERCENT_DOLLARS','','','','MR020532','NUMBER','','','Gross Percent Dollars','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_IGVP_V','AVERAGE_COST',660,'Average Cost','AVERAGE_COST','','~T~D~2','','MR020532','NUMBER','','','Average Cost','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_IGVP_V','ONHAND_LOCATION',660,'Onhand Location','ONHAND_LOCATION','','','','MR020532','VARCHAR2','','','Onhand Location','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_IGVP_V','MARGIN',660,'Margin','MARGIN','','','','MR020532','NUMBER','','','Margin','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_IGVP_V','HEADER_ID',660,'Header Id','HEADER_ID','','','','MR020532','NUMBER','','','Header Id','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_IGVP_V','INVENTORY_ITEM_ID',660,'Inventory Item Id','INVENTORY_ITEM_ID','','','','MR020532','NUMBER','','','Inventory Item Id','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_IGVP_V','LINE_ID',660,'Line Id','LINE_ID','','','','MR020532','NUMBER','','','Line Id','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_IGVP_V','ORGANIZATION_ID',660,'Organization Id','ORGANIZATION_ID','','','','MR020532','NUMBER','','','Organization Id','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_IGVP_V','PARTY_ID',660,'Party Id','PARTY_ID','','','','MR020532','NUMBER','','','Party Id','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_IGVP_V','UNIT_LIST_PRICE',660,'Unit List Price','UNIT_LIST_PRICE','','','','MR020532','NUMBER','','','Unit List Price','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_IGVP_V','HZP#PARTY_TYPE',660,'Descriptive flexfield: Party Information Column Name: Party Type','HZP#Party_Type','','','','MR020532','VARCHAR2','HZ_PARTIES','ATTRIBUTE1','Hzp#Party Type','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_IGVP_V','HZP#GVID_ID',660,'Descriptive flexfield: Party Information Column Name: GVID_ID','HZP#GVID_ID','','','','MR020532','VARCHAR2','HZ_PARTIES','ATTRIBUTE2','Hzp#Gvid Id','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_IGVP_V','MTP#FACTORY_PLANNER_DATA_DIR',660,'Descriptive flexfield: Organization parameters Column Name: Factory Planner Data Directory','MTP#Factory_Planner_Data_Dir','','','','MR020532','VARCHAR2','MTL_PARAMETERS','ATTRIBUTE1','Mtp#Factory Planner Data Directory','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_IGVP_V','MTP#FRU',660,'Descriptive flexfield: Organization parameters Column Name: FRU','MTP#FRU','','','','MR020532','VARCHAR2','MTL_PARAMETERS','ATTRIBUTE10','Mtp#Fru','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_IGVP_V','MTP#LOCATION_NUMBER',660,'Descriptive flexfield: Organization parameters Column Name: Location Number','MTP#Location_Number','','','','MR020532','VARCHAR2','MTL_PARAMETERS','ATTRIBUTE11','Mtp#Location Number','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_IGVP_V','MTP#BRANCH_OPERATIONS_MANAGE',660,'Descriptive flexfield: Organization parameters Column Name: Branch Operations Manager','MTP#Branch_Operations_Manage','','','','MR020532','VARCHAR2','MTL_PARAMETERS','ATTRIBUTE13','Mtp#Branch Operations Manager','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_IGVP_V','MTP#DELIVER_CHARGE',660,'Descriptive flexfield: Organization parameters Column Name: Deliver Charge','MTP#Deliver_Charge','','','','MR020532','VARCHAR2','MTL_PARAMETERS','ATTRIBUTE14','Mtp#Deliver Charge','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_IGVP_V','MTP#FACTORY_PLANNER_EXECUTAB',660,'Descriptive flexfield: Organization parameters Column Name: Factory Planner Executable Directory','MTP#Factory_Planner_Executab','','','','MR020532','VARCHAR2','MTL_PARAMETERS','ATTRIBUTE2','Mtp#Factory Planner Executable Directory','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_IGVP_V','MTP#FACTORY_PLANNER_USER',660,'Descriptive flexfield: Organization parameters Column Name: Factory Planner User','MTP#Factory_Planner_User','','','','MR020532','VARCHAR2','MTL_PARAMETERS','ATTRIBUTE3','Mtp#Factory Planner User','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_IGVP_V','MTP#FACTORY_PLANNER_HOST',660,'Descriptive flexfield: Organization parameters Column Name: Factory Planner Host','MTP#Factory_Planner_Host','','','','MR020532','VARCHAR2','MTL_PARAMETERS','ATTRIBUTE4','Mtp#Factory Planner Host','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_IGVP_V','MTP#FACTORY_PLANNER_PORT_NUM',660,'Descriptive flexfield: Organization parameters Column Name: Factory Planner Port Number','MTP#Factory_Planner_Port_Num','','','','MR020532','VARCHAR2','MTL_PARAMETERS','ATTRIBUTE5','Mtp#Factory Planner Port Number','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_IGVP_V','MTP#PRICING_ZONE',660,'Descriptive flexfield: Organization parameters Column Name: Pricing Zone','MTP#Pricing_Zone','','','','MR020532','VARCHAR2','MTL_PARAMETERS','ATTRIBUTE6','Mtp#Pricing Zone','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_IGVP_V','MTP#ORG_TYPE',660,'Descriptive flexfield: Organization parameters Column Name: Org Type','MTP#Org_Type','','','','MR020532','VARCHAR2','MTL_PARAMETERS','ATTRIBUTE7','Mtp#Org Type','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_IGVP_V','MTP#DISTRICT',660,'Descriptive flexfield: Organization parameters Column Name: District','MTP#District','','','','MR020532','VARCHAR2','MTL_PARAMETERS','ATTRIBUTE8','Mtp#District','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_IGVP_V','MTP#REGION',660,'Descriptive flexfield: Organization parameters Column Name: Region','MTP#Region','','','','MR020532','VARCHAR2','MTL_PARAMETERS','ATTRIBUTE9','Mtp#Region','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_IGVP_V','MSI#HDS#LOB',660,'Descriptive flexfield: Items Column Name: LOB Context: HDS','MSI#HDS#LOB','','','','MR020532','VARCHAR2','MTL_SYSTEM_ITEMS_B','ATTRIBUTE1','Msi#Hds#Lob','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_IGVP_V','MSI#HDS#DROP_SHIPMENT_ELIGAB',660,'Descriptive flexfield: Items Column Name: Drop Shipment Eligable Context: HDS','MSI#HDS#Drop_Shipment_Eligab','','','','MR020532','VARCHAR2','MTL_SYSTEM_ITEMS_B','ATTRIBUTE10','Msi#Hds#Drop Shipment Eligable','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_IGVP_V','MSI#HDS#INVOICE_UOM',660,'Descriptive flexfield: Items Column Name: Invoice UOM Context: HDS','MSI#HDS#Invoice_UOM','','','','MR020532','VARCHAR2','MTL_SYSTEM_ITEMS_B','ATTRIBUTE15','Msi#Hds#Invoice Uom','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_IGVP_V','MSI#HDS#PRODUCT_ID',660,'Descriptive flexfield: Items Column Name: Product ID Context: HDS','MSI#HDS#Product_ID','','','','MR020532','VARCHAR2','MTL_SYSTEM_ITEMS_B','ATTRIBUTE2','Msi#Hds#Product Id','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_IGVP_V','MSI#HDS#VENDOR_PART_NUMBER',660,'Descriptive flexfield: Items Column Name: Vendor Part Number Context: HDS','MSI#HDS#Vendor_Part_Number','','','','MR020532','VARCHAR2','MTL_SYSTEM_ITEMS_B','ATTRIBUTE3','Msi#Hds#Vendor Part Number','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_IGVP_V','MSI#HDS#UNSPSC_CODE',660,'Descriptive flexfield: Items Column Name: UNSPSC Code Context: HDS','MSI#HDS#UNSPSC_Code','','','','MR020532','VARCHAR2','MTL_SYSTEM_ITEMS_B','ATTRIBUTE4','Msi#Hds#Unspsc Code','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_IGVP_V','MSI#HDS#UPC_PRIMARY',660,'Descriptive flexfield: Items Column Name: UPC Primary Context: HDS','MSI#HDS#UPC_Primary','','','','MR020532','VARCHAR2','MTL_SYSTEM_ITEMS_B','ATTRIBUTE5','Msi#Hds#Upc Primary','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_IGVP_V','MSI#HDS#SKU_DESCRIPTION',660,'Descriptive flexfield: Items Column Name: SKU Description Context: HDS','MSI#HDS#SKU_Description','','','','MR020532','VARCHAR2','MTL_SYSTEM_ITEMS_B','ATTRIBUTE6','Msi#Hds#Sku Description','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_IGVP_V','MSI#WC#CA_PROP_65',660,'Descriptive flexfield: Items Column Name: CA Prop 65 Context: WC','MSI#WC#CA_Prop_65','','','','MR020532','VARCHAR2','MTL_SYSTEM_ITEMS_B','ATTRIBUTE1','Msi#Wc#Ca Prop 65','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_IGVP_V','MSI#WC#COUNTRY_OF_ORIGIN',660,'Descriptive flexfield: Items Column Name: Country of Origin Context: WC','MSI#WC#Country_of_Origin','','','','MR020532','VARCHAR2','MTL_SYSTEM_ITEMS_B','ATTRIBUTE10','Msi#Wc#Country Of Origin','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_IGVP_V','MSI#WC#ORM_D_FLAG',660,'Descriptive flexfield: Items Column Name: ORM-D Flag Context: WC','MSI#WC#ORM_D_Flag','','','','MR020532','VARCHAR2','MTL_SYSTEM_ITEMS_B','ATTRIBUTE11','Msi#Wc#Orm-D Flag','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_IGVP_V','MSI#WC#STORE_VELOCITY',660,'Descriptive flexfield: Items Column Name: Store Velocity Context: WC','MSI#WC#Store_Velocity','','','','MR020532','VARCHAR2','MTL_SYSTEM_ITEMS_B','ATTRIBUTE12','Msi#Wc#Store Velocity','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_IGVP_V','MSI#WC#DC_VELOCITY',660,'Descriptive flexfield: Items Column Name: DC Velocity Context: WC','MSI#WC#DC_Velocity','','','','MR020532','VARCHAR2','MTL_SYSTEM_ITEMS_B','ATTRIBUTE13','Msi#Wc#Dc Velocity','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_IGVP_V','MSI#WC#YEARLY_STORE_VELOCITY',660,'Descriptive flexfield: Items Column Name: Yearly Store Velocity Context: WC','MSI#WC#Yearly_Store_Velocity','','','','MR020532','VARCHAR2','MTL_SYSTEM_ITEMS_B','ATTRIBUTE14','Msi#Wc#Yearly Store Velocity','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_IGVP_V','MSI#WC#YEARLY_DC_VELOCITY',660,'Descriptive flexfield: Items Column Name: Yearly DC Velocity Context: WC','MSI#WC#Yearly_DC_Velocity','','','','MR020532','VARCHAR2','MTL_SYSTEM_ITEMS_B','ATTRIBUTE15','Msi#Wc#Yearly Dc Velocity','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_IGVP_V','MSI#WC#PRISM_PART_NUMBER',660,'Descriptive flexfield: Items Column Name: PRISM Part Number Context: WC','MSI#WC#PRISM_Part_Number','','','','MR020532','VARCHAR2','MTL_SYSTEM_ITEMS_B','ATTRIBUTE16','Msi#Wc#Prism Part Number','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_IGVP_V','MSI#WC#HAZMAT_DESCRIPTION',660,'Descriptive flexfield: Items Column Name: Hazmat Description Context: WC','MSI#WC#Hazmat_Description','','','','MR020532','VARCHAR2','MTL_SYSTEM_ITEMS_B','ATTRIBUTE17','Msi#Wc#Hazmat Description','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_IGVP_V','MSI#WC#HAZMAT_CONTAINER',660,'Descriptive flexfield: Items Column Name: Hazmat Container Context: WC','MSI#WC#Hazmat_Container','','','','MR020532','VARCHAR2','MTL_SYSTEM_ITEMS_B','ATTRIBUTE18','Msi#Wc#Hazmat Container','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_IGVP_V','MSI#WC#GTP_INDICATOR',660,'Descriptive flexfield: Items Column Name: GTP Indicator Context: WC','MSI#WC#GTP_Indicator','','','','MR020532','VARCHAR2','MTL_SYSTEM_ITEMS_B','ATTRIBUTE19','Msi#Wc#Gtp Indicator','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_IGVP_V','MSI#WC#LAST_LEAD_TIME',660,'Descriptive flexfield: Items Column Name: Last Lead Time Context: WC','MSI#WC#Last_Lead_Time','','','','MR020532','VARCHAR2','MTL_SYSTEM_ITEMS_B','ATTRIBUTE2','Msi#Wc#Last Lead Time','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_IGVP_V','MSI#WC#AMU',660,'Descriptive flexfield: Items Column Name: AMU Context: WC','MSI#WC#AMU','','','','MR020532','VARCHAR2','MTL_SYSTEM_ITEMS_B','ATTRIBUTE20','Msi#Wc#Amu','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_IGVP_V','MSI#WC#RESERVE_STOCK',660,'Descriptive flexfield: Items Column Name: Reserve Stock Context: WC','MSI#WC#Reserve_Stock','','','','MR020532','VARCHAR2','MTL_SYSTEM_ITEMS_B','ATTRIBUTE21','Msi#Wc#Reserve Stock','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_IGVP_V','MSI#WC#TAXWARE_CODE',660,'Descriptive flexfield: Items Column Name: Taxware Code Context: WC','MSI#WC#Taxware_Code','','','','MR020532','VARCHAR2','MTL_SYSTEM_ITEMS_B','ATTRIBUTE22','Msi#Wc#Taxware Code','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_IGVP_V','MSI#WC#AVERAGE_UNITS',660,'Descriptive flexfield: Items Column Name: Average Units Context: WC','MSI#WC#Average_Units','','','','MR020532','VARCHAR2','MTL_SYSTEM_ITEMS_B','ATTRIBUTE25','Msi#Wc#Average Units','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_IGVP_V','MSI#WC#PRODUCT_CODE',660,'Descriptive flexfield: Items Column Name: Product code Context: WC','MSI#WC#Product_code','','','','MR020532','VARCHAR2','MTL_SYSTEM_ITEMS_B','ATTRIBUTE26','Msi#Wc#Product Code','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_IGVP_V','MSI#WC#IMPORT_DUTY_',660,'Descriptive flexfield: Items Column Name: Import Duty % Context: WC','MSI#WC#Import_Duty_','','','','MR020532','VARCHAR2','MTL_SYSTEM_ITEMS_B','ATTRIBUTE27','Msi#Wc#Import Duty %','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_IGVP_V','MSI#WC#KEEP_ITEM_ACTIVE',660,'Descriptive flexfield: Items Column Name: Keep Item Active Context: WC','MSI#WC#Keep_Item_Active','','','','MR020532','VARCHAR2','MTL_SYSTEM_ITEMS_B','ATTRIBUTE29','Msi#Wc#Keep Item Active','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_IGVP_V','MSI#WC#PESTICIDE_FLAG',660,'Descriptive flexfield: Items Column Name: Pesticide Flag Context: WC','MSI#WC#Pesticide_Flag','','','','MR020532','VARCHAR2','MTL_SYSTEM_ITEMS_B','ATTRIBUTE3','Msi#Wc#Pesticide Flag','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_IGVP_V','MSI#WC#CALC_LEAD_TIME',660,'Descriptive flexfield: Items Column Name: Calc Lead Time Context: WC','MSI#WC#Calc_Lead_Time','','','','MR020532','VARCHAR2','MTL_SYSTEM_ITEMS_B','ATTRIBUTE30','Msi#Wc#Calc Lead Time','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_IGVP_V','MSI#WC#VOC_GL',660,'Descriptive flexfield: Items Column Name: VOC G/L Context: WC','MSI#WC#VOC_GL','','','','MR020532','VARCHAR2','MTL_SYSTEM_ITEMS_B','ATTRIBUTE4','Msi#Wc#Voc G/L','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_IGVP_V','MSI#WC#PESTICIDE_FLAG_STATE',660,'Descriptive flexfield: Items Column Name: Pesticide Flag State Context: WC','MSI#WC#Pesticide_Flag_State','','','','MR020532','VARCHAR2','MTL_SYSTEM_ITEMS_B','ATTRIBUTE5','Msi#Wc#Pesticide Flag State','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_IGVP_V','MSI#WC#VOC_CATEGORY',660,'Descriptive flexfield: Items Column Name: VOC Category Context: WC','MSI#WC#VOC_Category','','','','MR020532','VARCHAR2','MTL_SYSTEM_ITEMS_B','ATTRIBUTE6','Msi#Wc#Voc Category','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_IGVP_V','MSI#WC#VOC_SUB_CATEGORY',660,'Descriptive flexfield: Items Column Name: VOC Sub Category Context: WC','MSI#WC#VOC_Sub_Category','','','','MR020532','VARCHAR2','MTL_SYSTEM_ITEMS_B','ATTRIBUTE7','Msi#Wc#Voc Sub Category','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_IGVP_V','MSI#WC#MSDS_#',660,'Descriptive flexfield: Items Column Name: MSDS # Context: WC','MSI#WC#MSDS_#','','','','MR020532','VARCHAR2','MTL_SYSTEM_ITEMS_B','ATTRIBUTE8','Msi#Wc#Msds #','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_IGVP_V','MSI#WC#HAZMAT_PACKAGING_GROU',660,'Descriptive flexfield: Items Column Name: Hazmat Packaging Group Context: WC','MSI#WC#Hazmat_Packaging_Grou','','','','MR020532','VARCHAR2','MTL_SYSTEM_ITEMS_B','ATTRIBUTE9','Msi#Wc#Hazmat Packaging Group','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_IGVP_V','OH#RENTAL_TERM',660,'Descriptive flexfield: Additional Header Information Column Name: Rental Term','OH#Rental_Term','','','','MR020532','VARCHAR2','OE_ORDER_HEADERS_ALL','ATTRIBUTE1','Oh#Rental Term','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_IGVP_V','OH#ORDER_CHANNEL',660,'Descriptive flexfield: Additional Header Information Column Name: Order Channel','OH#Order_Channel','','','','MR020532','VARCHAR2','OE_ORDER_HEADERS_ALL','ATTRIBUTE2','Oh#Order Channel','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_IGVP_V','OH#PICKED_BY',660,'Descriptive flexfield: Additional Header Information Column Name: Picked By','OH#Picked_By','','','','MR020532','VARCHAR2','OE_ORDER_HEADERS_ALL','ATTRIBUTE3','Oh#Picked By','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_IGVP_V','OH#LOAD_CHECKED_BY',660,'Descriptive flexfield: Additional Header Information Column Name: Load Checked By','OH#Load_Checked_By','','','','MR020532','VARCHAR2','OE_ORDER_HEADERS_ALL','ATTRIBUTE4','Oh#Load Checked By','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_IGVP_V','OH#DELIVERED_BY',660,'Descriptive flexfield: Additional Header Information Column Name: Delivered By','OH#Delivered_By','','','','MR020532','VARCHAR2','OE_ORDER_HEADERS_ALL','ATTRIBUTE5','Oh#Delivered By','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_IGVP_V','OL#ESTIMATED_RETURN_DATE',660,'Descriptive flexfield: Additional Line Attribute Information Column Name: Estimated Return Date','OL#Estimated_Return_Date','','','','MR020532','VARCHAR2','OE_ORDER_LINES_ALL','ATTRIBUTE1','Ol#Estimated Return Date','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_IGVP_V','OL#RERENT_PO',660,'Descriptive flexfield: Additional Line Attribute Information Column Name: ReRent PO','OL#ReRent_PO','','','','MR020532','VARCHAR2','OE_ORDER_LINES_ALL','ATTRIBUTE10','Ol#Rerent Po','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_IGVP_V','OL#FORCE_SHIP',660,'Descriptive flexfield: Additional Line Attribute Information Column Name: Force Ship','OL#Force_Ship','','','','MR020532','VARCHAR2','OE_ORDER_LINES_ALL','ATTRIBUTE11','Ol#Force Ship','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_IGVP_V','OL#APPLICATION_METHOD',660,'Descriptive flexfield: Additional Line Attribute Information Column Name: Application Method','OL#Application_Method','','','','MR020532','VARCHAR2','OE_ORDER_LINES_ALL','ATTRIBUTE13','Ol#Application Method','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_IGVP_V','OL#ITEM_ON_BLOWOUT',660,'Descriptive flexfield: Additional Line Attribute Information Column Name: Item on Blowout?','OL#Item_on_Blowout','','','','MR020532','VARCHAR2','OE_ORDER_LINES_ALL','ATTRIBUTE15','Ol#Item On Blowout?','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_IGVP_V','OL#POF_STD_LINE',660,'Descriptive flexfield: Additional Line Attribute Information Column Name: POF Std Line','OL#POF_Std_Line','','','','MR020532','VARCHAR2','OE_ORDER_LINES_ALL','ATTRIBUTE19','Ol#Pof Std Line','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_IGVP_V','OL#RERENTAL_BILLING_TERMS',660,'Descriptive flexfield: Additional Line Attribute Information Column Name: ReRental Billing Terms','OL#ReRental_Billing_Terms','','','','MR020532','VARCHAR2','OE_ORDER_LINES_ALL','ATTRIBUTE2','Ol#Rerental Billing Terms','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_IGVP_V','OL#PRICING_GUARDRAIL',660,'Descriptive flexfield: Additional Line Attribute Information Column Name: Pricing Guardrail','OL#Pricing_Guardrail','','','','MR020532','VARCHAR2','OE_ORDER_LINES_ALL','ATTRIBUTE20','Ol#Pricing Guardrail','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_IGVP_V','OL#PRINT_EXPIRED_PRODUCT_DIS',660,'Descriptive flexfield: Additional Line Attribute Information Column Name: Print Expired Product Disclaim','OL#Print_Expired_Product_Dis','','','','MR020532','VARCHAR2','OE_ORDER_LINES_ALL','ATTRIBUTE3','Ol#Print Expired Product Disclaim','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_IGVP_V','OL#RENTAL_CHARGE',660,'Descriptive flexfield: Additional Line Attribute Information Column Name: Rental Charge','OL#Rental_Charge','','','','MR020532','VARCHAR2','OE_ORDER_LINES_ALL','ATTRIBUTE4','Ol#Rental Charge','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_IGVP_V','OL#VENDOR_QUOTE_COST',660,'Descriptive flexfield: Additional Line Attribute Information Column Name: Vendor Quote Cost','OL#Vendor_Quote_Cost','','','','MR020532','VARCHAR2','OE_ORDER_LINES_ALL','ATTRIBUTE5','Ol#Vendor Quote Cost','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_IGVP_V','OL#PO_COST_FOR_VENDOR_QUOTE',660,'Descriptive flexfield: Additional Line Attribute Information Column Name: PO Cost for Vendor Quote','OL#PO_Cost_for_Vendor_Quote','','','','MR020532','VARCHAR2','OE_ORDER_LINES_ALL','ATTRIBUTE6','Ol#Po Cost For Vendor Quote','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_IGVP_V','OL#SERIAL_NUMBER',660,'Descriptive flexfield: Additional Line Attribute Information Column Name: Serial Number','OL#Serial_Number','','','','MR020532','VARCHAR2','OE_ORDER_LINES_ALL','ATTRIBUTE7','Ol#Serial Number','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_IGVP_V','OL#ENGINEERING_COST',660,'Descriptive flexfield: Additional Line Attribute Information Column Name: Engineering Cost','OL#Engineering_Cost','','','','MR020532','VARCHAR2','OE_ORDER_LINES_ALL','ATTRIBUTE8','Ol#Engineering Cost','','','');
--Inserting View Components for EIS_XXWC_OM_IGVP_V
xxeis.eis_rs_ins.vcomp( 'EIS_XXWC_OM_IGVP_V','OE_ORDER_HEADERS',660,'OE_ORDER_HEADERS_ALL','OH','OH','MR020532','MR020532','-1','Oe Order Headers All Stores Header Information For','','','','');
xxeis.eis_rs_ins.vcomp( 'EIS_XXWC_OM_IGVP_V','OE_ORDER_LINES',660,'OE_ORDER_LINES_ALL','OL','OL','MR020532','MR020532','-1','Oe Order Lines All Stores Information For All Orde','','','','');
xxeis.eis_rs_ins.vcomp( 'EIS_XXWC_OM_IGVP_V','HZ_PARTIES',660,'HZ_PARTIES','HZP','HZP','MR020532','MR020532','-1','Information About Parties Such As Organizations, P','','','','');
xxeis.eis_rs_ins.vcomp( 'EIS_XXWC_OM_IGVP_V','MTL_PARAMETERS',660,'MTL_PARAMETERS','MTP','MTP','MR020532','MR020532','-1','Inventory Control Options And Defaults','','','','');
xxeis.eis_rs_ins.vcomp( 'EIS_XXWC_OM_IGVP_V','MTL_SYSTEM_ITEMS_KFV',660,'MTL_SYSTEM_ITEMS_B','MSI','MSI','MR020532','MR020532','-1','Inventory Item Definitions','','','','');
--Inserting View Component Joins for EIS_XXWC_OM_IGVP_V
xxeis.eis_rs_ins.vcj( 'EIS_XXWC_OM_IGVP_V','OE_ORDER_HEADERS','OH',660,'EXOIV.HEADER_ID','=','OH.HEADER_ID(+)','','','','Y','MR020532','','');
xxeis.eis_rs_ins.vcj( 'EIS_XXWC_OM_IGVP_V','OE_ORDER_LINES','OL',660,'EXOIV.LINE_ID','=','OL.LINE_ID(+)','','','','Y','MR020532','','');
xxeis.eis_rs_ins.vcj( 'EIS_XXWC_OM_IGVP_V','HZ_PARTIES','HZP',660,'EXOIV.PARTY_ID','=','HZP.PARTY_ID(+)','','','','Y','MR020532','','');
xxeis.eis_rs_ins.vcj( 'EIS_XXWC_OM_IGVP_V','MTL_PARAMETERS','MTP',660,'EXOIV.ORGANIZATION_ID','=','MTP.ORGANIZATION_ID(+)','','','','Y','MR020532','','');
xxeis.eis_rs_ins.vcj( 'EIS_XXWC_OM_IGVP_V','MTL_SYSTEM_ITEMS_KFV','MSI',660,'EXOIV.INVENTORY_ITEM_ID','=','MSI.INVENTORY_ITEM_ID(+)','','','','Y','MR020532','','');
xxeis.eis_rs_ins.vcj( 'EIS_XXWC_OM_IGVP_V','MTL_SYSTEM_ITEMS_KFV','MSI',660,'EXOIV.ORGANIZATION_ID','=','MSI.ORGANIZATION_ID(+)','','','','Y','MR020532','','');
END;
/
set scan on define on
prompt Creating Report LOV Data for Orders on Hold Report
set scan off define off
DECLARE
BEGIN 
--Inserting Report LOVs - Orders on Hold Report
xxeis.eis_rs_ins.lov( 660,'select distinct ott.name order_type,ott.description description,ott.transaction_type_id order_type_id from oe_transaction_types_tl ott','','OM ORDER TYPE','This gives the Order Type','XXEIS_RS_ADMIN',NULL,'N','','');
xxeis.eis_rs_ins.lov( '','SELECT ORGANIZATION_CODE WAREHOUSE,ORGANIZATION_NAME
FROM ORG_ORGANIZATION_DEFINITIONS OOD
WHERE SYSDATE < NVL(OOD.DISABLE_DATE,SYSDATE+1) AND EXISTS (SELECT 1 FROM XXEIS.EIS_ORG_ACCESS_V  WHERE organization_id = ood.organization_id )
UNION
SELECT ''All'', ''All Organizations'' FROM DUAL','','XXWC All ORG LIST','XXWC All ORG LIST','MR020532',NULL,'N','','');
xxeis.eis_rs_ins.lov( '','select name from OE_HOLD_DEFINITIONS','','XXWC OM HOLD TYPE','XXWC OM HOLD TYPE for Order on holds report','MR020532',NULL,'N','','');
END;
/
set scan on define on
prompt Creating Report Data for Orders on Hold Report
set scan off define off
DECLARE
BEGIN 
--Deleting Report data - Orders on Hold Report
xxeis.eis_rs_utility.delete_report_rows( 'Orders on Hold Report' );
--Inserting Report - Orders on Hold Report
xxeis.eis_rs_ins.r( 660,'Orders on Hold Report','','Order Holds and Not Invoiced Report','','','','MR020532','EIS_XXWC_OM_IGVP_V','Y','','','MR020532','','N','White Cap Reports','','CSV,Pivot Excel,EXCEL,','N');
--Inserting Report Columns - Orders on Hold Report
xxeis.eis_rs_ins.rc( 'Orders on Hold Report',660,'CUSTOMER_NAME','Customer Name','Customer Name','','','default','','2','N','','','','','','','','MR020532','N','N','','EIS_XXWC_OM_IGVP_V','','');
xxeis.eis_rs_ins.rc( 'Orders on Hold Report',660,'CUSTOMER_NUMBER','Customer Number','Customer Number','','','default','','1','N','','','','','','','','MR020532','N','N','','EIS_XXWC_OM_IGVP_V','','');
xxeis.eis_rs_ins.rc( 'Orders on Hold Report',660,'INVENTORY_ORGANIZATION','Loc','Inventory Organization','','','default','','3','N','','','','','','','','MR020532','N','N','','EIS_XXWC_OM_IGVP_V','','');
xxeis.eis_rs_ins.rc( 'Orders on Hold Report',660,'ORDER_HEADER_HOLD_NAME','Order Header Hold Name','Order Header Hold Name','','','default','','13','N','','','','','','','','MR020532','N','N','','EIS_XXWC_OM_IGVP_V','','');
xxeis.eis_rs_ins.rc( 'Orders on Hold Report',660,'ORDER_LINE_HOLD_NAME','Order Line Hold Name','Order Line Hold Name','','','default','','15','N','','','','','','','','MR020532','N','N','','EIS_XXWC_OM_IGVP_V','','');
xxeis.eis_rs_ins.rc( 'Orders on Hold Report',660,'PART_DESCRIPTION','Part Description','Part Description','','','default','','18','N','','','','','','','','MR020532','N','N','','EIS_XXWC_OM_IGVP_V','','');
xxeis.eis_rs_ins.rc( 'Orders on Hold Report',660,'PART_NUMBER','Part Number','Part Number','','','default','','17','N','','','','','','','','MR020532','N','N','','EIS_XXWC_OM_IGVP_V','','');
xxeis.eis_rs_ins.rc( 'Orders on Hold Report',660,'QUANTITY','Quantity','Quantity','','~T~D~2','default','','19','N','','','','','','','','MR020532','N','N','','EIS_XXWC_OM_IGVP_V','','');
xxeis.eis_rs_ins.rc( 'Orders on Hold Report',660,'SALES_ORDER_LINE_NUMBER','Order Line Number','Sales Order Line Number','','','default','','10','N','','','','','','','','MR020532','N','N','','EIS_XXWC_OM_IGVP_V','','');
xxeis.eis_rs_ins.rc( 'Orders on Hold Report',660,'SALES_ORDER_LINE_STATUS','Order Line Status','Sales Order Line Status','','','default','','9','N','','','','','','','','MR020532','N','N','','EIS_XXWC_OM_IGVP_V','','');
xxeis.eis_rs_ins.rc( 'Orders on Hold Report',660,'SALES_ORDER_NUMBER','Sales Order Number','Sales Order Number','','~~~','default','','4','N','','','','','','','','MR020532','N','N','','EIS_XXWC_OM_IGVP_V','','');
xxeis.eis_rs_ins.rc( 'Orders on Hold Report',660,'SALES_ORDER_STATUS','Order Header Status','Sales Order Status','','','default','','6','N','','','','','','','','MR020532','N','N','','EIS_XXWC_OM_IGVP_V','','');
xxeis.eis_rs_ins.rc( 'Orders on Hold Report',660,'UNIT_SELLING_PRICE','Unit Selling Price','Unit Selling Price','','~T~D~2','default','','20','N','','','','','','','','MR020532','N','N','','EIS_XXWC_OM_IGVP_V','','');
xxeis.eis_rs_ins.rc( 'Orders on Hold Report',660,'EXTENDED_PRICE','Extended Price','Extended Price','','~T~D~2','default','','21','N','','','','','','','','MR020532','N','N','','EIS_XXWC_OM_IGVP_V','','');
xxeis.eis_rs_ins.rc( 'Orders on Hold Report',660,'UNIT_COST','Unit Cost','Unit Cost','','~T~D~2','default','','24','N','','','','','','','','MR020532','N','N','','EIS_XXWC_OM_IGVP_V','','');
xxeis.eis_rs_ins.rc( 'Orders on Hold Report',660,'ORDER_HEADER_HOLD','Order Header Hold','Unit Cost','VARCHAR2','','default','','12','Y','','','','','','','CASE WHEN EXOIV.ORDER_HEADER_HOLD_NAME IS NULL THEN ''No'' ELSE ''Yes'' END','MR020532','N','N','','EIS_XXWC_OM_IGVP_V','','');
xxeis.eis_rs_ins.rc( 'Orders on Hold Report',660,'ORDER_LINE_HOLD','Order Line Hold','Unit Cost','VARCHAR2','','default','','14','Y','','','','','','','CASE WHEN  EXOIV.ORDER_LINE_HOLD_NAME IS NULL THEN ''No'' ELSE ''Yes'' END','MR020532','N','N','','EIS_XXWC_OM_IGVP_V','','');
xxeis.eis_rs_ins.rc( 'Orders on Hold Report',660,'ORDER_TYPE','Order Type','Order Type','','','default','','7','N','','','','','','','','MR020532','N','N','','EIS_XXWC_OM_IGVP_V','','');
xxeis.eis_rs_ins.rc( 'Orders on Hold Report',660,'CREATED_BY','Created By','Created By','','','default','','5','N','','','','','','','','MR020532','N','N','','EIS_XXWC_OM_IGVP_V','','');
xxeis.eis_rs_ins.rc( 'Orders on Hold Report',660,'EXTENDED_COST','Extended Cost','Extended Cost','','~T~D~2','default','','25','N','','','','','','','','MR020532','N','N','','EIS_XXWC_OM_IGVP_V','','');
xxeis.eis_rs_ins.rc( 'Orders on Hold Report',660,'HOLD_DATE','Hold Date','Hold Date','','','default','','16','N','','','','','','','','MR020532','N','N','','EIS_XXWC_OM_IGVP_V','','');
xxeis.eis_rs_ins.rc( 'Orders on Hold Report',660,'ORDERED_DATE','Ordered Date','Ordered Date','','','default','','8','N','','','','','','','','MR020532','N','N','','EIS_XXWC_OM_IGVP_V','','');
xxeis.eis_rs_ins.rc( 'Orders on Hold Report',660,'GM$','GM$','Ordered Date','NUMBER','~T~D~2','default','','26','Y','','','','','','','(NVL(EXOIV.EXTENDED_PRICE,0)-NVL(EXOIV.EXTENDED_COST,0))','MR020532','N','N','','EIS_XXWC_OM_IGVP_V','','');
xxeis.eis_rs_ins.rc( 'Orders on Hold Report',660,'LINE_TYPE','Line Type','Line Type','','','default','','11','N','','','','','','','','MR020532','N','N','','EIS_XXWC_OM_IGVP_V','','');
xxeis.eis_rs_ins.rc( 'Orders on Hold Report',660,'SPECIAL_COST','Special VQN Cost','Special Cost','','~T~D~2','default','','27','N','','','','','','','','MR020532','N','N','','EIS_XXWC_OM_IGVP_V','','');
xxeis.eis_rs_ins.rc( 'Orders on Hold Report',660,'VQN_MODIFIER_NAME','Modifier Name','Vqn Modifier Name','','','default','','29','N','','','','','','','','MR020532','N','N','','EIS_XXWC_OM_IGVP_V','','');
xxeis.eis_rs_ins.rc( 'Orders on Hold Report',660,'ADJ_GM_PER','Adjust GM%','Vqn Modifier Name','NUMBER','~T~D~2','default','','28','Y','','','','','','','case WHEN EXOIV.SPECIAL_COST is NULL THEN NULL when (NVL(EXOIV.EXTENDED_PRICE,0) !=0  AND NVL(EXOIV.SPECIAL_COST,0) !=0) then (((EXOIV.EXTENDED_PRICE-(EXOIV.SPECIAL_COST * EXOIV.QUANTITY))/(EXOIV.EXTENDED_PRICE))*100) WHEN NVL(EXOIV.SPECIAL_COST,0) =0  THEN 100 WHEN nvl(EXOIV.EXTENDED_PRICE,0) != 0 THEN (((EXOIV.EXTENDED_PRICE-(EXOIV.SPECIAL_COST * EXOIV.QUANTITY))/(EXOIV.EXTENDED_PRICE))*100) else 0 END','MR020532','N','N','','EIS_XXWC_OM_IGVP_V','','');
xxeis.eis_rs_ins.rc( 'Orders on Hold Report',660,'MARG_PER','Margin%','Vqn Modifier Name','NUMBER','~T~D~2','default','','23','Y','','','','','','','case when (NVL(EXOIV.EXTENDED_PRICE,0) !=0  AND NVL(EXOIV.EXTENDED_COST,0) !=0) then (((nvl(EXOIV.EXTENDED_PRICE,0)-nvl(EXOIV.EXTENDED_COST,0))/nvl(EXOIV.EXTENDED_PRICE,0))*100) WHEN NVL(EXOIV.EXTENDED_COST,0) =0  THEN 100 WHEN nvl(EXOIV.EXTENDED_PRICE,0) != 0 THEN (((nvl(EXOIV.EXTENDED_PRICE,0)-nvl(EXOIV.EXTENDED_COST,0))/nvl(EXOIV.EXTENDED_PRICE,0))*100) else 0 end','MR020532','N','N','','EIS_XXWC_OM_IGVP_V','','');
--Inserting Report Parameters - Orders on Hold Report
xxeis.eis_rs_ins.rp( 'Orders on Hold Report',660,'Warehouse','Warehouse','INVENTORY_ORGANIZATION','IN','XXWC All ORG LIST','','VARCHAR2','Y','Y','1','','N','CONSTANT','MR020532','Y','N','','','');
xxeis.eis_rs_ins.rp( 'Orders on Hold Report',660,'Ordered Date From','Ordered Date To','ORDERED_DATE','>=','','','DATE','N','Y','2','','Y','CONSTANT','MR020532','Y','N','','Start Date','');
xxeis.eis_rs_ins.rp( 'Orders on Hold Report',660,'Ordered Date To','Ordered Date To','ORDERED_DATE','<=','','','DATE','N','Y','3','','Y','CONSTANT','MR020532','Y','N','','End Date','');
--Added for TMS#20151002-00007
xxeis.eis_rs_ins.rp( 'Orders on Hold Report',660,'Order Type','Order Type','ORDER_TYPE','IN','OM ORDER TYPE','','VARCHAR2','N','Y','4','','Y','CONSTANT','MR020532','Y','N','','','');
xxeis.eis_rs_ins.rp( 'Orders on Hold Report',660,'Hold Type','Hold Type','ORDER_LINE_HOLD_NAME','IN','XXWC OM HOLD TYPE','','VARCHAR2','N','Y','5','','Y','CONSTANT','MR020532','Y','N','','','');
--Inserting Report Conditions - Orders on Hold Report
xxeis.eis_rs_ins.rcn( 'Orders on Hold Report',660,'INVENTORY_ORGANIZATION','IN',':Warehouse','','','N','3','N','MR020532');
xxeis.eis_rs_ins.rcn( 'Orders on Hold Report',660,'ORDERED_DATE','>=',':Ordered Date From','','','Y','2','Y','MR020532');
xxeis.eis_rs_ins.rcn( 'Orders on Hold Report',660,'ORDERED_DATE','<=',':Ordered Date To','','','Y','3','Y','MR020532');
xxeis.eis_rs_ins.rcn( 'Orders on Hold Report',660,'ORDER_TYPE','IN',':Order Type','','','Y','4','Y','MR020532');
--Added for TMS#20151002-00007
xxeis.eis_rs_ins.rcn( 'Orders on Hold Report',660,'ORDER_LINE_HOLD_NAME','IN',':Hold Type','','','Y','5','Y','MR020532');
xxeis.eis_rs_ins.rcn( 'Orders on Hold Report',660,'','','','','AND (EXOIV.ORDER_HEADER_HOLD_NAME IS NOT NULL OR EXOIV.ORDER_LINE_HOLD_NAME IS NOT NULL)
AND (EXOIV.ORDER_HEADER_HOLD_NAME IS NOT NULL OR NVL(EXOIV.ORDER_LINE_HOLD_NAME,''HOLD'') != ''XXWC_PRICE_CHANGE_HOLD'')
AND ( ''All'' IN (:Warehouse) OR (INVENTORY_ORGANIZATION IN (:Warehouse)))','Y','1','','MR020532');
--Inserting Report Sorts - Orders on Hold Report
xxeis.eis_rs_ins.rs( 'Orders on Hold Report',660,'SALES_ORDER_NUMBER','ASC','MR020532','','');
--Inserting Report Triggers - Orders on Hold Report
--Inserting Report Templates - Orders on Hold Report
--Inserting Report Portals - Orders on Hold Report
--Inserting Report Dashboards - Orders on Hold Report
--Inserting Report Security - Orders on Hold Report
xxeis.eis_rs_ins.rsec( 'Orders on Hold Report','660','','50926',660,'MR020532','','');
xxeis.eis_rs_ins.rsec( 'Orders on Hold Report','660','','50927',660,'MR020532','','');
xxeis.eis_rs_ins.rsec( 'Orders on Hold Report','660','','50928',660,'MR020532','','');
xxeis.eis_rs_ins.rsec( 'Orders on Hold Report','660','','50929',660,'MR020532','','');
xxeis.eis_rs_ins.rsec( 'Orders on Hold Report','660','','50931',660,'MR020532','','');
xxeis.eis_rs_ins.rsec( 'Orders on Hold Report','660','','50930',660,'MR020532','','');
xxeis.eis_rs_ins.rsec( 'Orders on Hold Report','660','','21623',660,'MR020532','','');
xxeis.eis_rs_ins.rsec( 'Orders on Hold Report','701','','50546',660,'MR020532','','');
xxeis.eis_rs_ins.rsec( 'Orders on Hold Report','660','','50856',660,'MR020532','','');
xxeis.eis_rs_ins.rsec( 'Orders on Hold Report','660','','50857',660,'MR020532','','');
xxeis.eis_rs_ins.rsec( 'Orders on Hold Report','660','','50858',660,'MR020532','','');
xxeis.eis_rs_ins.rsec( 'Orders on Hold Report','660','','50859',660,'MR020532','','');
xxeis.eis_rs_ins.rsec( 'Orders on Hold Report','660','','50860',660,'MR020532','','');
xxeis.eis_rs_ins.rsec( 'Orders on Hold Report','660','','50861',660,'MR020532','','');
xxeis.eis_rs_ins.rsec( 'Orders on Hold Report','20005','','50880',660,'MR020532','','');
xxeis.eis_rs_ins.rsec( 'Orders on Hold Report','','LC053655','',660,'MR020532','','');
xxeis.eis_rs_ins.rsec( 'Orders on Hold Report','','10010432','',660,'MR020532','','');
xxeis.eis_rs_ins.rsec( 'Orders on Hold Report','','RB054040','',660,'MR020532','','');
xxeis.eis_rs_ins.rsec( 'Orders on Hold Report','','RV003897','',660,'MR020532','','');
xxeis.eis_rs_ins.rsec( 'Orders on Hold Report','','SS084202','',660,'MR020532','','');
xxeis.eis_rs_ins.rsec( 'Orders on Hold Report','','SO004816','',660,'MR020532','','');
xxeis.eis_rs_ins.rsec( 'Orders on Hold Report','660','','50886',660,'MR020532','','');
xxeis.eis_rs_ins.rsec( 'Orders on Hold Report','660','','50901',660,'MR020532','','');
xxeis.eis_rs_ins.rsec( 'Orders on Hold Report','660','','50870',660,'MR020532','','');
xxeis.eis_rs_ins.rsec( 'Orders on Hold Report','660','','50871',660,'MR020532','','');
xxeis.eis_rs_ins.rsec( 'Orders on Hold Report','660','','50869',660,'MR020532','','');
xxeis.eis_rs_ins.rsec( 'Orders on Hold Report','20005','','50900',660,'MR020532','','');
xxeis.eis_rs_ins.rsec( 'Orders on Hold Report','660','','51044',660,'MR020532','','');
xxeis.eis_rs_ins.rsec( 'Orders on Hold Report','','MS016543','',660,'MR020532','','');
--Inserting Report Pivots - Orders on Hold Report
xxeis.eis_rs_ins.rpivot( 'Orders on Hold Report',660,'Pivot','1','1,0|1,2,1','1,1,0,0|None|2');
--Inserting Report Pivot Details For Pivot - Pivot
xxeis.eis_rs_ins.rpivot_dtls( 'Orders on Hold Report',660,'Pivot','SALES_ORDER_NUMBER','ROW_FIELD','','','1','1','xlNormal');
xxeis.eis_rs_ins.rpivot_dtls( 'Orders on Hold Report',660,'Pivot','EXTENDED_PRICE','DATA_FIELD','SUM','','1','1','xlNormal');
xxeis.eis_rs_ins.rpivot_dtls( 'Orders on Hold Report',660,'Pivot','CUSTOMER_NAME','PAGE_FIELD','','','1','1','xlNormal');
xxeis.eis_rs_ins.rpivot_dtls( 'Orders on Hold Report',660,'Pivot','CUSTOMER_NUMBER','PAGE_FIELD','','','2','1','xlNormal');
xxeis.eis_rs_ins.rpivot_dtls( 'Orders on Hold Report',660,'Pivot','ORDER_HEADER_HOLD_NAME','ROW_FIELD','','','2','1','xlNormal');
xxeis.eis_rs_ins.rpivot_dtls( 'Orders on Hold Report',660,'Pivot','ORDER_LINE_HOLD_NAME','ROW_FIELD','','','3','1','xlNormal');
xxeis.eis_rs_ins.rpivot_dtls( 'Orders on Hold Report',660,'Pivot','PART_DESCRIPTION','PAGE_FIELD','','','4','1','xlNormal');
xxeis.eis_rs_ins.rpivot_dtls( 'Orders on Hold Report',660,'Pivot','PART_NUMBER','PAGE_FIELD','','','3','1','xlNormal');
--Inserting Report Summary Calculation Columns For Pivot- Pivot
END;
/
set scan on define on