/***********************************************************************************************************************************************
   NAME:     TMS_20181024-00002_Datafix.sql
   PURPOSE:  Data fix

   REVISIONS:
   Ver        Date        Author           Description
   ---------  ----------  ---------------  ----------------------------------------------------------------------------------------------------
   1.0        10/24/2018  Rakesh Patel     TMS#20181024-00002-Credit Limit Data Script - Jobs Only- Data fix script
************************************************************************************************************************************************/
SET SERVEROUTPUT ON SIZE 1000000

DECLARE
  l_count              BINARY_INTEGER := 0;
  l_err_count          BINARY_INTEGER := 0;
  l_backupsuffix       VARCHAR2(10)   := TO_CHAR(SYSDATE,'ddhh24miss');
  l_cust_site_use_rec  hz_cust_account_site_v2pub.cust_site_use_rec_type;
  l_version_num        NUMBER;
  l_return_status      VARCHAR(10);
  l_msg_count          NUMBER;    
  l_msg_data           VARCHAR(4000);
  
     CURSOR c_cust_sites 
	                    ( p_account_number    IN VARCHAR2
						 ,p_party_site_number IN VARCHAR2
						 ,p_cust_acct_site_id IN NUMBER
						 )
         IS
       SELECT 		 
            hca.account_number,
             hca.cust_account_id,
             hca.account_name,
             hps.party_site_number,
             hcsa.cust_acct_site_id,
             hcsa.party_site_id,
             hcsu.site_use_id,
             hcsu.site_use_code,
             hcsu.location,
			 SUBSTR('M9'||' '||hcsu.location, 1, 40) new_location, --40 length
             hcsu.created_by_module,
             hcsu.object_version_number
       FROM apps.hz_cust_accounts_all hca,
            apps.hz_party_sites hps,
            apps.hz_cust_acct_sites_All hcsa,
			apps.hz_cust_site_uses_all hcsu
      WHERE  1 = 1
         AND hps.party_id           = hca.party_id
         AND hca.cust_account_id    = hcsa.cust_account_id
         AND hps.party_site_id      = hcsa.party_site_id --party site id
		 AND hcsu.cust_acct_site_id = hcsa.cust_acct_site_id
         AND hca.account_number     = p_account_number
         AND hps.party_site_number  = p_party_site_number  --party site number
         AND hcsa.cust_acct_site_id = p_cust_acct_site_id 
		 AND hcsa.status            = 'A'
		 AND hcsu.status            = 'A';
BEGIN
  dbms_output.put_line( 'Begin Fix TMS_20181024-00002' );
  
  --Apps Initialization
  FND_GLOBAL.APPS_INITIALIZE(USER_ID      => 1290, --XXWC_INT_FINANCE
                             RESP_ID      => 51208,
                             RESP_APPL_ID => 222);
  MO_GLOBAL.INIT('AR');
  MO_GLOBAL.SET_POLICY_CONTEXT('S', 162);
  
  dbms_output.put( 'Begin Backup ... ' );
  EXECUTE IMMEDIATE 'CREATE TABLE xxwc.hz_cust_acct_sites_all' ||l_backupsuffix || ' AS SELECT * FROM AR.hz_cust_acct_sites_all WHERE cust_acct_site_id IN (SELECT cust_acct_site_id FROM XXWC.XXWC_CUST_LOC_UPD_EXT_TBL)';
  EXECUTE IMMEDIATE 'GRANT ALL ON xxwc.hz_cust_acct_sites_all' ||l_backupsuffix || ' TO PUBLIC';
  dbms_output.put_line('Backup hz_cust_acct_sites_all Table - Done, xxwc.hz_cust_acct_sites_all'||l_backupsuffix);
  
  FOR cust_loc_upd_rec IN (SELECT oracle_account_number, oracle_site_number, cust_acct_site_id
                                 FROM xxwc.xxwc_cust_loc_upd_ext_tbl
                                WHERE 1=1
                                 -- AND CUST_ACCT_SITE_ID = 1624431
                                 ) LOOP
     FOR c_cust_sites_rec IN c_cust_sites (  p_account_number    => cust_loc_upd_rec.oracle_account_number
                                            ,p_party_site_number => cust_loc_upd_rec.oracle_site_number  
                                            ,p_cust_acct_site_id => cust_loc_upd_rec.cust_acct_site_id ) LOOP
        l_cust_site_use_rec.site_use_id       := c_cust_sites_rec.site_use_id; 
        l_cust_site_use_rec.cust_acct_site_id := c_cust_sites_rec.cust_acct_site_id; 
        l_cust_site_use_rec.location          := c_cust_sites_rec.new_location; 
        l_cust_site_use_rec.site_use_code     := c_cust_sites_rec.site_use_code;
        l_cust_site_use_rec.created_by_module := c_cust_sites_rec.created_by_module;
  
        hz_cust_account_site_v2pub.update_cust_site_use(p_init_msg_list         => 'T',
                                                        p_cust_site_use_rec     => l_cust_site_use_rec,
                                                        p_object_version_number => c_cust_sites_rec.object_version_number,
                                                        x_return_status         => l_return_status,
                                                        x_msg_count             => l_msg_count,
                                                        x_msg_data              => l_msg_data);
                                                  
        IF l_return_status = 'S' THEN
           l_count := l_count+1;
        ELSE
            l_err_count := l_err_count+1;
            IF NVL(l_msg_count, 0) > 1 THEN
              FOR i IN 1 .. l_msg_count LOOP
                 dbms_output.put_line(' Error Status ' || l_return_status);
                 dbms_output.put_line(' Error message ' || l_msg_data);
              END LOOP;
            ELSE
               dbms_output.put_line(' Error message ' || l_msg_data);
            END IF;
        END IF; 
     END LOOP;      
  END LOOP;

  dbms_output.put_line('Records Updated:   ' || l_count);
  
  dbms_output.put_line('Records Error count :   ' || l_err_count);

  COMMIT;

  dbms_output.put_line( 'End Fix 20181024-00002' );
  
EXCEPTION
  WHEN OTHERS THEN
    dbms_output.put_line('Unable to update records ' || SQLCODE || ': ' ||SQLERRM);
    ROLLBACK;
END;