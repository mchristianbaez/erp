/* Ver        Date         Author           Description
   ---------  -----------  ---------------  ------------------------------------
   1.0        28-Jan-2015  Neha Saini       TMS#20160128-00069  IT - WC Daily Flash Cash report */

SET SERVEROUTPUT ON SIZE 100000;
SET VERIFY OFF;
BEGIN

UPDATE apps.ra_customer_trx_all 
SET INTERFACE_HEADER_CONTEXT = 'ORDER ENTRY'
where trx_number in ('10004681243',
                     '10004681128',
                     '10004682267',
                     '10004681220',
                     '10004682401',
                     '10004682402',
                     '10004682304',
                     '10004682464',
                     '10004682435',
                     '10004681243',
                     '10004681242')
and INTERFACE_HEADER_CONTEXT IS NULL;
--1 row expected to be updated
dbms_output.put_line('No of records updated: '||SQL%ROWCOUNT);
COMMIT;

EXCEPTION
  WHEN others THEN
    dbms_output.put_line('Error occured while updating STATUS: '||SQLERRM);
END;
/