CREATE OR REPLACE PACKAGE xxwc_om_line_wf_pkg
AS
   /**************************************************************************************************************************
   *   $Header XXWC_OM_LINE_WF_PKG.PKG $
   *   Module Name: XXWC_OM_LINE_WF_PKG.PKG
   *
   *   PURPOSE:   This package is used for data script automation through workflow
   *
   *   REVISIONS:
   *   Ver        Date         Author                     Description
   *   ---------  -----------  ---------------         -------------------------
   *   1.0        26-Apr-2016  Niraj                   Initial Version
   *   1.1        13-Jul-2016  P.vamshidhar            TMS#20160708-00173 - Batch improvements for XXWC OM Fulfillment Acceptance
   *   2.0        09-Jun-2016  Rakesh Patel            Changes for TMS#20160511-00208
   * ***************************************************************************************************************************/
   PROCEDURE Correct_awaiting_invoice (P_itemtype   IN     VARCHAR2,
                                       P_itemkey    IN     VARCHAR2,
                                       p_actid      IN     NUMBER,
                                       p_funcmode   IN     VARCHAR2,
                                       resultout    IN OUT VARCHAR2);

   PROCEDURE Correct_awaiting_Order (P_itemtype   IN     VARCHAR2,
                                     P_itemkey    IN     VARCHAR2,
                                     p_actid      IN     NUMBER,
                                     p_funcmode   IN     VARCHAR2,
                                     resultout    IN OUT VARCHAR2);

   -- Added below procedure by Vamshi in Rev 1.1
   PROCEDURE wf_fulfillment_accept (P_itemtype   IN     VARCHAR2,
                                    P_itemkey    IN     VARCHAR2,
                                    p_actid      IN     NUMBER,
                                    p_funcmode   IN     VARCHAR2,
                                    resultout    IN OUT VARCHAR2);

   -- Added below procedure by Rakesh P. in Rev 2.0
   PROCEDURE Start_AR_CMG_Process(
                                    p_itemtype         IN VARCHAR2,
                                    p_itemkey          IN VARCHAR2,
                                    p_actid            IN NUMBER,
                                    p_funcmode         IN VARCHAR2,
                                    resultout      IN OUT VARCHAR2 
                                   );
   -- Added below procedure by Rakesh P. in Rev 2.0                               
   PROCEDURE Sumbit_AR_CMG_WF_Background; 
   
END xxwc_om_line_wf_pkg;
/