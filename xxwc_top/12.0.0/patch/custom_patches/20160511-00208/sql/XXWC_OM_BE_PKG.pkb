CREATE OR REPLACE PACKAGE BODY APPS.XXWC_OM_BE_PKG
AS
   /********************************************************************************************************************************
      $Header XXWC_OM_BE_PKG.PKB $
      Module Name: XXWC_OM_BE_PKG.PKB

      PURPOSE:   This package is used for OM custom business events

      REVISIONS:
      Ver        Date        Author                     Description
      ---------  ----------  ---------------         -------------------------------------------------------------------------------
      1.0        09-Jun-2016  Rakesh Patel           TMS#20160511-00208-IT - Improvements to AR Credit Management backend workflow
   *********************************************************************************************************************************/

   --Email Defaults
   g_dflt_email   fnd_user.email_address%TYPE
                     := 'WC-ITDEVALERTS-U1@HDSupply.com';
   g_pkg_name     VARCHAR2 (50) := 'XXWC_OM_BE_PKG';

   
   /*************************************************************************
      PROCEDURE Name: Start_AR_CMG_Process

      PURPOSE:   This procedure will call wf api to process AR credit managment wf process

      REVISIONS:
      Ver        Date          Author             Description
      ---------  ------------  ------------       ------------------
      1.0        31-Aug-2016   Rakesh Patel       TMS#20160511-00208-IT - Improvements to 
                                                  AR Credit Management backend workflow
   ****************************************************************************/
   FUNCTION Start_AR_CMG_Process(   p_subscription_guid   IN     RAW,
                                    p_event               IN OUT wf_event_t
       ) RETURN VARCHAR2 IS

          l_proc_name               VARCHAR2(2000) := 'XXWC_OM_BE_PKG.Start_AR_CMG_Process';
          l_sec                     VARCHAR2(200);
          l_credit_request_id       NUMBER;
          l_itemkey                 NUMBER;
          l_order_header_id         NUMBER;   
          l_credit_request_id_count NUMBER;    
          l_StartTime            NUMBER;       
          l_CurrentTime          NUMBER;
          l_Elapsed              NUMBER;
     BEGIN
        l_credit_request_id := to_number (p_event.getvalueforparameter ('P_CREDIT_REQUEST_ID'));
						  
        l_sec := 'Before sleep Start_AR_CMG_Process_New';
		DBMS_LOCK.sleep(10);                  
        
        l_sec := 'Start Start_AR_CMG_Process_New';
        xxwc_om_line_wf_pkg.Sumbit_AR_CMG_WF_Background;
        RETURN ('SUCCESS');

     EXCEPTION
          WHEN OTHERS THEN
             xxcus_error_pkg.xxcus_error_main_api (
                                                p_called_from         => UPPER(g_pkg_name)||'.'||UPPER(l_proc_name)
                                               ,p_calling             => l_sec
                                               ,p_request_id          => fnd_global.conc_request_id
                                               ,p_ora_error_msg       => SUBSTR(SQLERRM,1,2000)
                                               ,p_error_desc          => 'Error in procedure '||l_proc_name||' for header id '||l_itemkey
                                               ,p_distribution_list   => g_dflt_email
                                               ,p_module              => 'XXWC'
                                              );
         RETURN 'ERROR';
     END Start_AR_CMG_Process;
     
     PROCEDURE raise_ar_cmg_process(p_credit_request_id  IN NUMBER,
                                    p_review_type IN VARCHAR2,
                                    p_error_msg  OUT VARCHAR2)
									
   /**************************************************************************
     PROCEDURE Name: raise_ar_cmg_process
     
     PURPOSE:   This Procedure is used to raise the business event 
                to start ar credit management process
     REVISIONS:
     Ver        Date        Author                     Description
     ---------  ----------  ---------------         -------------------------
     1.0        12-Aug-2016 Rakesh Patel            TMS#20160511-00208-IT - Improvements to AR Credit Management backend workflow
     **************************************************************************/
   IS PRAGMA AUTONOMOUS_TRANSACTION;
   
      l_credit_request_id       NUMBER;
      l_review_type             VARCHAR2 (4000);
      l_error_msg               VARCHAR2 (4000);
      l_parameter_list          wf_parameter_list_t;
      l_event_data              CLOB;
      l_sec                     VARCHAR2 (100);
   BEGIN
      l_sec := 'Initialize Values';

      l_credit_request_id := p_credit_request_id;      
      l_review_type := p_review_type;
	  

      l_sec := 'Set values for Parameter List';

      l_parameter_list :=
               wf_parameter_list_t (
                  wf_parameter_t ('SEND_DATE',
                                  fnd_date.date_to_canonical (SYSDATE)),
                  wf_parameter_t ('P_CREDIT_REQUEST_ID', NVL(l_credit_request_id, -99)),
                  wf_parameter_t ('P_REVIEW_TYPE', NVL(p_review_type,'XX'))
				                    );

            l_sec := 'Raise business Event';

            wf_event.raise (
               p_event_name   => 'xxwc.oracle.apps.ont.order.arcmgprocess',
               p_event_key    => SYS_GUID (),
               p_event_data   => l_event_data,
               p_parameters   => l_parameter_list);
            
			COMMIT;
			
   EXCEPTION
      WHEN OTHERS
      THEN
         xxcus_error_pkg.xxcus_error_main_api (
            p_called_from         => 'XXWC_OM_BE_PKG.raise_ar_cmg_process',
            p_calling             => l_sec,
            p_request_id          => fnd_global.conc_request_id,
            p_ora_error_msg       => SUBSTR (
                                          ' Error_Stack...'
                                       || DBMS_UTILITY.format_error_stack ()
                                       || ' Error_Backtrace...'
                                       || DBMS_UTILITY.format_error_backtrace (),
                                       1,
                                       2000),
            p_error_desc          => SUBSTR (SQLERRM, 1, 240),
            p_distribution_list   => g_dflt_email,
            p_module              => 'OM');
         
   END raise_ar_cmg_process; 
END xxwc_om_be_pkg;
/