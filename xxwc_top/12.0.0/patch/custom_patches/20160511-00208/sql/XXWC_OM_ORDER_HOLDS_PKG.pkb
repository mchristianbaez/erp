CREATE OR REPLACE PACKAGE BODY APPS.xxwc_om_order_holds_pkg
AS
   /******************************************************************************
      NAME:       apps.xxwc_om_order_holds_pkg
      PURPOSE:    Package to create a case folder whenever a hard hold is placed.

      REVISIONS:
      Ver        Date        Author               Description
      ---------  ----------  ---------------  ------------------------------------
      1.0        6/26/2013     shariharan     	   Initial Version
      1.1        7/16/2013     shariharan     	   Create case folder only for Booked orders
                                              	   Change the source user to be order creator
      1.2        10/01/2014    Gopi Damuluri  	   TMS# 20141002-00060 Multi Org Changes
      1.3        07/29/2015   Maharajan Shunmugam  TMS#20150706-00192 Credit - Case Folders not created for Site and Account Credit Holds
	  1.4        06-Jun-2016  Rakesh Patel         Changes for TMS#20160511-00208 to start wf process for the case folder.  
   ******************************************************************************/

   PROCEDURE create_case_folder (errbuf OUT VARCHAR2, retcode OUT VARCHAR2)
   IS
      l_stage             NUMBER;
      --l_success         VARCHAR2 (1);       --commented for version 1.3
      --l_cf_create       VARCHAR2 (1);       --commented for version 1.3
      l_hold_exist        VARCHAR2 (1);       --Added for version 1.3
      l_folder_exist      VARCHAR2 (1);       --Added for version 1.3
     -- l_exist           VARCHAR2 (1);       --commented for version 1.3
      out_message         VARCHAR2 (2000);
      l_msg_index_OUT     NUMBER;
      l_hold_code         VARCHAR2 (10);
      l_order_number      NUMBER;
      l_total_exposure    NUMBER;
      l_request_id        NUMBER;
      l_msg_count         NUMBER;
      l_msg_data          VARCHAR2 (2000);
      l_return_status     VARCHAR2 (30);
      l_party_id          NUMBER;
      l_customer_id       NUMBER;
      l_site_use_id       NUMBER;

      l_hold_reason_rec   apps.AR_CMGT_CREDIT_REQUEST_API.hold_reason_rec_type
         := apps.AR_CMGT_CREDIT_REQUEST_API.hold_reason_rec_type (NULL);
      i_hld_rec           NUMBER := 0;
      l_review_type       VARCHAR2 (10);
      l_refund_email      VARCHAR2 (100) := fnd_Profile.VALUE ('XXWC_OM_CF_CREATE_EMAIL');
      
      CURSOR c1
      IS
         SELECT DISTINCT a.hold_source_id
                       , a.order_hold_id           --added for version 1.3
                       , b.hold_entity_code
                       , a.header_id
                       , b.hold_id
                       , d.created_by
                      , d.order_number             --added for version 1.3
           FROM apps.xxwc_order_credit_holds_tbl a
              , apps.oe_hold_sources  b
              , apps.oe_order_holds   c
              , apps.oe_order_headers d
          WHERE     a.process_flag = 'N'
                AND a.hold_source_id = b.hold_source_id
                AND a.order_hold_id = c.order_hold_id
                AND a.header_id=d.header_id
                AND d.flow_status_code not in ('ENTERED','CANCELLED');
   BEGIN
      UPDATE apps.xxwc_order_credit_holds_tbl a
         SET process_flag = 'Y'
           , hold_entity_code = 'O'
           , process_msg = 'Hold Entity Code not B or C'
       WHERE process_flag = 'N'
             AND EXISTS
                    (SELECT 1
                       FROM apps.oe_hold_sources b
                      WHERE a.hold_source_id = b.hold_source_id
                            --AND hold_entity_code IN ('O', 'I', 'P', 'OT'));  --commented for ver#1.3
                              AND hold_entity_code NOT IN ('B', 'C'));         --Added for ver#1.3

      COMMIT;

      UPDATE apps.xxwc_order_credit_holds_tbl a
         SET process_flag = 'Y'
           , hold_entity_code = 'O'
           , process_msg = 'Hold ID is not Credit Check Failure 1'
       WHERE process_flag = 'N'
             AND EXISTS
                    (SELECT 1
                       FROM apps.oe_hold_sources b
                      WHERE a.hold_source_id = b.hold_source_id
                            AND hold_id <> 1);

      COMMIT;

      FOR c1_rec IN c1
      LOOP
         l_stage := 3;
--         L_SUCCESS := 'Y';        --commented for version 1.3

 --check if unreleased hold exist

--commented below for version 1.3
 /*BEGIN
            SELECT 'Y'
              INTO l_exist
              FROM DUAL
             WHERE EXISTS
                      (SELECT 1
                         FROM apps.oe_order_holds d
                        WHERE     d.hold_source_id = c1_rec.hold_source_id
                              AND d.header_id = c1_rec.header_id
                              AND d.released_flag = 'N');

            l_cf_create := 'Y';
         EXCEPTION
            WHEN OTHERS
            THEN*/

--Added for version 1.3 <<Start
      BEGIN
       SELECT 'Y'
         INTO l_hold_exist
       FROM apps.oe_order_holds d
       WHERE d.hold_source_id = c1_rec.hold_source_id
         AND d.order_hold_id = c1_rec.order_hold_id        
         AND d.header_id = c1_rec.header_id
         AND d.released_flag = 'N';
      EXCEPTION   
      WHEN NO_DATA_FOUND THEN
         l_hold_exist := 'N';
      WHEN OTHERS THEN
         l_hold_exist := 'N';
     END;

     fnd_file.put_line (FND_FILE.LOG,'Hold Exist for '||c1_rec.order_number||'-'||l_hold_exist); 
        
     IF l_hold_exist = 'N' THEN								--Added for version 1.3 <<End
        UPDATE apps.xxwc_order_credit_holds_tbl a
          SET process_flag = 'Y'
            , hold_entity_code = c1_rec.hold_entity_code
            , process_msg = 'No unreleased holds exist'
        WHERE     hold_source_id = c1_rec.hold_source_id
              AND header_id = c1_rec.header_id
              AND a.order_hold_id = c1_rec.order_hold_id      --added for ver 1.3
              AND process_flag = 'N';
      fnd_file.put_line (FND_FILE.LOG,'Staging table is updated as No unreleased hold Exist for SO '||c1_rec.order_number);-- Added for version 1.3
      END IF;

     IF l_hold_exist = 'Y' THEN         -- Added IF - END IF for version 1.3

    --check if case folder already exist
--commented below for ver 1.3
/* IF l_cf_create = 'Y'
         THEN
            BEGIN
               SELECT 'Y'
                 INTO l_exist
                 FROM DUAL
                WHERE EXISTS
                         (SELECT 1
                            FROM apps.ar_cmgt_credit_requests b
                           WHERE     c1_rec.header_id = b.source_column1
                                 AND b.source_column3 = 'ORDER'
                                 AND ((b.review_type='SITE_HOLD' and c1_rec.hold_entity_code='B') or
                                     (b.review_type='CUST_HOLD' and c1_rec.hold_entity_code='C'))
                                 AND b.status <> 'PROCESSED');

               l_cf_create := 'N';*/

--Added for version 1.3 <<Start
      BEGIN
        SELECT 'Y'
          INTO l_folder_exist
        FROM apps.ar_cmgt_credit_requests b
        WHERE c1_rec.header_id = b.source_column1
          AND b.source_column3 = 'ORDER'
          AND ((b.review_type='SITE_HOLD' and c1_rec.hold_entity_code='B') or
              (b.review_type='CUST_HOLD' and c1_rec.hold_entity_code='C'))
           AND b.status <> 'PROCESSED';
      EXCEPTION
      WHEN NO_DATA_FOUND THEN
         l_folder_exist:= 'N';
      WHEN OTHERS THEN
         l_folder_exist:= 'N';
     END;
    END IF;

     fnd_file.put_line (FND_FILE.LOG,'Unprocessed folder exist for '||c1_rec.order_number||'-'||l_folder_exist); 

     IF l_folder_exist = 'Y' THEN   --Added for version 1.3 <<End

        UPDATE apps.xxwc_order_credit_holds_tbl a
          SET process_flag = 'Y'
            , hold_entity_code = c1_rec.hold_entity_code
            , process_msg = 'Unprocessed Case Folder Already Exists'
        WHERE     hold_source_id = c1_rec.hold_source_id
              AND header_id = c1_rec.header_id
              AND a.order_hold_id = c1_rec.order_hold_id      --added for ver 1.3
              AND process_flag = 'N';
--commented for ver 1.3
    /* EXCEPTION
               WHEN OTHERS
               THEN
                  l_cf_create := 'Y';
            END;
         END IF;


         IF l_cf_create = 'Y'
         THEN*/


 fnd_file.put_line (FND_FILE.LOG,'Staging table is updated as Unprocessed folder already exist for '||c1_rec.order_number); -- Added for version 1.3

     ELSE   -- Added for version 1.3
            SELECT a.order_number
                 , b.cust_account_id
                 , b.party_id
                 , a.invoice_to_org_id
                 , oe_oe_totals_summary.PRT_ORDER_TOTAL (a.header_id)
              INTO l_order_number
                 , l_customer_id
                 , l_party_id
                 , l_site_use_id
                 , l_total_exposure
              FROM apps.oe_order_headers a, apps.hz_cust_accounts b
             WHERE a.header_id = c1_rec.header_id
                   AND a.sold_to_org_id = b.cust_account_id;

            l_stage := 4;


            IF c1_rec.hold_entity_code = 'C'
            THEN
               l_stage := 7;
               l_review_type := 'CUST_HOLD';
               l_site_use_id := NULL;
            ELSIF c1_rec.hold_entity_code = 'B'
            THEN
               l_stage := 8;
               l_review_type := 'SITE_HOLD';
            END IF;

            IF c1_rec.hold_entity_code IN ('C','B')         --Added IF clause for version#1.3 <<Start
	    THEN
           fnd_file.put_line (FND_FILE.LOG,'Hold entity code in '||c1_rec.hold_entity_code||'-Creating FOlder');  --Added for ver 1.3 <<End
            l_stage := 9;
            --i_hld_rec := i_hld_rec + 1;
            l_hold_reason_rec (1) := 'OE_CC_HOLD_ORDER';
            l_stage := 10;
            
            apps.AR_CMGT_CREDIT_REQUEST_API.Create_credit_request (
               p_api_version                => 1.0
             , p_init_msg_list              => FND_API.G_TRUE
             , p_commit                     => FND_API.G_TRUE
             , p_validation_level           => FND_API.G_VALID_LEVEL_FULL
             , x_return_status              => l_return_status
             , x_msg_count                  => l_msg_count
             , x_msg_data                   => l_msg_data
             , p_application_number         => NULL
             , p_application_date           => SYSDATE
             , p_requestor_type             => 'FND_USER' --NULL
             , p_requestor_id               => c1_rec.created_by --fnd_global.employee_id
             , p_review_type                => l_review_type
             , p_credit_classification      => NULL
             , p_requested_amount           => l_total_exposure
             , p_requested_currency         => 'USD'       --l_limit_curr_code
             , p_trx_amount                 => l_total_exposure
             , p_trx_currency               => 'USD' --p_header_rec.transactional_curr_code
             , p_credit_type                => 'TRADE'
             , p_term_length                => NULL --the unit is no of months
             , p_credit_check_rule_id       => 1041
             --p_credit_check_rule_rec.credit_check_rule_id
             , p_credit_request_status      => 'SUBMIT'
             , p_party_id                   => l_party_id --l_review_party_id --bug 5907331
             , p_cust_account_id            => l_customer_id
             , p_cust_acct_site_id          => NULL
             , p_site_use_id                => l_site_use_id
             , p_contact_party_id           => NULL --party_id of the pseudo party
             , p_notes                      => NULL    --contact relationship.
             , p_source_org_id              => 162           --l_source_org_id
             , p_source_user_id             => c1_rec.created_by --fnd_global.user_id --l_source_user_id
             , p_source_resp_id             => fnd_global.resp_id --l_source_resp_id
             , p_source_appln_id            => fnd_global.resp_appl_id --l_source_appln_id
             , p_source_security_group_id   => 0  --l_source_security_group_id
             , p_source_name                => 'OM'
             , p_source_column1             => c1_rec.header_id --p_header_rec.header_id
             , p_source_column2             => l_order_number --p_header_rec.order_number
             , p_source_column3             => 'ORDER'
             , p_credit_request_id          => l_request_id
             , p_hold_reason_rec            => l_hold_reason_rec   --ER8880886
                                                                );
           

            l_stage := 11;

           fnd_file.put_line (FND_FILE.LOG,'Create folder API status '||l_return_status); --Added for ver 1.3
           fnd_file.put_line (FND_FILE.LOG,'Create folder API Message '||l_msg_data); --Added for ver 1.3

            IF l_return_status <> 'S'
            THEN
               l_stage := 12;
           --    l_success := 'N';  --commented for ver 1.3

               FOR i IN 1 .. FND_MSG_PUB.Count_Msg
               LOOP
                  apps.FND_MSG_PUB.Get (p_msg_index       => i
                                 , p_encoded         => 'F'
                                 , p_data            => out_message
                                 , p_msg_index_OUT   => l_msg_index_OUT);
                  l_msg_data := l_msg_data || ':' || out_message;
               END LOOP;

               UTL_MAIL.send (
                  sender       => l_refund_email
                , recipients   => l_refund_email
                , cc           => NULL
                , bcc          => NULL
                , subject      =>   'Case Folder for Order '
                                 || l_order_number
                                 || ':'
                                 || l_msg_count
                                 || ':'
                                 || l_stage
                , MESSAGE      =>   'Case Folder Creation failed for Order '
                                 || l_return_status
                                 || ':'
                                 || l_msg_data
                , mime_type    => 'text/plain; charset=us-ascii'
                , priority     => 3
                , replyto      => NULL);
             fnd_file.put_line (FND_FILE.LOG,'Case Folder Creation failed for Order '||l_order_number); --Added for ver 1.3
           ELSE      --Added for ver 1.3
            --END IF; --commented for ver 1.3

           -- IF l_success = 'Y' --commented for ver 1.3
           -- THEN               --commented for ver 1.3
               UPDATE apps.xxwc_order_credit_holds_tbl
                  SET process_flag = 'Y'
                    , hold_entity_code = c1_rec.hold_entity_code
                    , process_msg = 'Case Folder Created'
                WHERE hold_source_id = c1_rec.hold_source_id
                      AND header_id = c1_rec.header_id
                      AND order_hold_id = c1_rec.order_hold_id;      --added for ver 1.3
             fnd_file.put_line (FND_FILE.LOG,'Case Folder Created for Order '||l_order_number); --commented for ver 1.3

             -- Version 1.4 > Start
			 --Added for TMS#20160511-00208 to start AR Credit Management wf process to create case folder.
             wf_engine.StartProcess (itemtype => 'ARCMGTAP',--itemtype
                                     itemkey  => l_request_id--itemkey  
                                    );
             -- Version 1.4 < End
             COMMIT;
            END IF;
          END IF;         --Added for version#1.3
         END IF;
      END LOOP;
      -- Version 1.4 > Start
      xxwc_om_line_wf_pkg.Sumbit_AR_CMG_WF_Background;
      -- Version 1.4 < End
   EXCEPTION
      WHEN OTHERS
      THEN
         NULL;
         -- l_stage := 13;
         UTL_MAIL.send (
            sender       => l_refund_email
          , recipients   => l_refund_email
          , cc           => NULL
          , bcc          => NULL
          , subject      =>   'Case Folder for Order '
                           || l_order_number
                           || ':'
                           || l_stage
          , MESSAGE      => 'Case Folder Creation failed for Order exception'
          , mime_type    => 'text/plain; charset=us-ascii'
          , priority     => 3
          , replyto      => NULL);
   END create_case_folder;



   PROCEDURE release_order_hold (i_order_number   IN     NUMBER
                               , i_review_type    IN     VARCHAR2
                               , o_ret_msg           OUT VARCHAR2)
   IS
      l_msg_count        NUMBER := 0;
      l_msg_data         VARCHAR2 (2000);
      l_return_status    VARCHAR2 (30);
      l_release_reason   VARCHAR2 (30);
      l_order_tbl        OE_HOLDS_PVT.order_tbl_type;
      l_header_id        NUMBER;
      i                  NUMBER := 0;

      CURSOR c1
      IS
         SELECT header_id, line_id
           FROM apps.oe_holds_history_v
          WHERE     header_id = l_header_id
                AND line_id IS NOT NULL
                AND hold_entity_code = 'B'
                AND released_by IS NULL;
   BEGIN
      --fnd_message.debug('Start');
      apps.mo_global.init('ONT');
      apps.mo_global.set_policy_context('S',162);
      BEGIN
         SELECT header_id
           INTO l_header_id
           FROM apps.oe_order_headers
          WHERE order_number = i_order_number;
      EXCEPTION
         WHEN OTHERS
         THEN
            --fnd_message.debug('Exception Return 1');
            RETURN;
      END;

      IF i_review_type = 'CUST_HOLD'
      THEN
         l_order_tbl (1).header_id := l_header_id;
      --fnd_message.debug('No Line');
      ELSE
         FOR c1_rec IN c1
         LOOP
            i := i + 1;
            l_order_tbl (i).header_id := c1_rec.header_id;
            l_order_tbl (i).line_id := c1_rec.line_id;
         END LOOP;
      --fnd_message.debug('Line Exists');
      END IF;

      --fnd_message.debug('Before Call');
      apps.OE_HOLDS_PUB.RELEASE_HOLDS (
         p_api_version           => 1.0
       , p_order_tbl             => l_order_tbl
       , p_hold_id               => 1
       , p_release_reason_code   => 'AR_APPROVE'
       , p_release_comment       => 'Approved by Credit Manager'
       , x_return_status         => l_return_status
       , x_msg_count             => l_msg_count
       , x_msg_data              => l_msg_data);


      IF l_return_status = 'S'
      THEN
         xxwc_ascp_scwb_pkg.proccommit;
      ELSE
           FOR i IN 1 .. oe_msg_pub.count_msg
           LOOP
              l_msg_data :=
                    l_msg_data
                 || ':'
                 || oe_msg_pub.get (p_msg_index => i, p_encoded => 'F');
           END LOOP;
         o_ret_msg := l_msg_data|| '-' || l_return_status;
      END IF;
   END release_order_hold;
END xxwc_om_order_holds_pkg;
/