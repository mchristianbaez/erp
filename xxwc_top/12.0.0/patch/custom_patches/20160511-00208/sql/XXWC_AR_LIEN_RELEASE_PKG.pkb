CREATE OR REPLACE PACKAGE BODY APPS.xxwc_ar_lien_release_pkg
AS
   /******************************************************************************
      NAME:       apps.xxwc_ar_lien_release_pkg
      PURPOSE:    Package to create a case folder for lien release.

      REVISIONS:
      Ver        Date        Author           Description
      ---------  ----------  ---------------  ------------------------------------
      1.0        11/22/2013  shariharan    Initial Version
	  1.1	     10/10/2014	 Veera         Modified the code for TMS#20141001-00167 	
   ******************************************************************************/
   FUNCTION get_rem_order_total (i_header_id IN NUMBER)
      RETURN NUMBER
   IS
      l_line_total   NUMBER;
   BEGIN
      SELECT --sum(oe_oe_totals_summary.line_total (i_header_id, a.line_id,a.line_number, a.shipment_number)+a.tax_value+oe_oe_totals_summary.line_charges (i_header_id, a.line_id))
             sum( (NVL (Ordered_Quantity, 0) * NVL (unit_selling_price, 0)*decode(a.line_category_code,'RETURN',-1,1)) +a.tax_value+oe_oe_totals_summary.line_charges (i_header_id, a.line_id))
        INTO l_line_total
        FROM apps.oe_order_lines a
       WHERE     header_id = i_header_id
             --AND a.flow_status_Code NOT IN ('CLOSED','DRAFT_CUSTOMER_REJECTED','CANCELLED','DRAFT','LOST','AWAITING_RETURN','AWAITING_RECEIPT');
             AND a.flow_status_Code NOT IN ('CLOSED',
                                         'CANCELLED',
                                         'DRAFT',
                                         'DRAFT_CUSTOMER_REJECTED',
                                         'LOST',
                                         'OFFER_EXPIRED');
             -- ('BOOKED', 'AWAITING_SHIPPING');
      RETURN (NVL (l_line_total, 0));
   END get_rem_order_total;
   
   
   PROCEDURE create_case_folder (i_customer_id in number,i_request_id in number,error_message out varchar2)
   IS
      l_stage             NUMBER;
      l_success           VARCHAR2 (1);
      l_cf_create         VARCHAR2 (1) := 'Y';
      l_exist             VARCHAR2 (1);
      out_message         VARCHAR2 (4000);
      l_msg_index_OUT     NUMBER;
      l_hold_code         VARCHAR2 (10);
      l_order_number      NUMBER;
      l_total_exposure    NUMBER;
      l_request_id        NUMBER;
      l_msg_count         NUMBER;
      l_msg_data          VARCHAR2 (2000);
      l_note_msg_data     VARCHAR2 (2000);
      l_return_status     VARCHAR2 (30);
      l_party_id          NUMBER;
      l_customer_id       NUMBER;
      l_site_use_id       NUMBER;
      l_customer_name     VARCHAR2(240);

      l_hold_reason_rec   apps.AR_CMGT_CREDIT_REQUEST_API.hold_reason_rec_type
         := apps.AR_CMGT_CREDIT_REQUEST_API.hold_reason_rec_type (NULL);
      i_hld_rec           NUMBER := 0;
      l_review_type       VARCHAR2 (10);
      l_refund_email      VARCHAR2 (100) := fnd_Profile.VALUE ('XXWC_OM_CF_CREATE_EMAIL');
      l_jtf_note_id       NUMBER;
	  l_source_org_id     NUMBER;  --10/10/2014 TMS#20141001-00167 Added By Veera 
      
      CURSOR c1
      IS
         SELECT a.rowid, a.*
           FROM apps.xxwc_ar_lien_rel_action_tbl a
          WHERE     a.process_flag = 'N'
                AND a.customer_id = i_customer_id
                AND a.request_id = i_request_id;
   BEGIN
    SELECT b.party_id, a.party_name
      INTO l_party_id, l_customer_name
      FROM apps.hz_cust_accounts b, apps.hz_parties a
     WHERE b.cust_account_id = i_customer_id
       AND a.party_id=b.party_id;
       
    FOR c1_rec IN c1
      LOOP
         l_stage := 1;
         L_SUCCESS := 'Y';

         IF l_cf_create = 'Y'
         THEN
            l_stage := 2;
            IF c1_rec.lien_release_type='CP' then
             l_review_type := 'LIENCP';
            ELSIF c1_rec.lien_release_type='UCP' then
             l_review_type := 'LIENUCP';            
            ELSIF c1_rec.lien_release_type='CF' then
             l_review_type := 'LIENCF';
            ELSIF c1_rec.lien_release_type='UCF' then
             l_review_type := 'LIENUCF';
            END IF;
            l_stage := 3;
          l_source_org_id:=mo_global.get_current_org_id;  --10/10/2014 TMS#20141001-00167 Added By Veera
            apps.AR_CMGT_CREDIT_REQUEST_API.Create_credit_request (
               p_api_version                => 1.0
             , p_init_msg_list              => FND_API.G_TRUE
             , p_commit                     => FND_API.G_FALSE
             , p_validation_level           => FND_API.G_VALID_LEVEL_FULL
             , x_return_status              => l_return_status
             , x_msg_count                  => l_msg_count
             , x_msg_data                   => l_msg_data
             , p_application_number         => NULL
             , p_application_date           => SYSDATE
             , p_requestor_type             => 'EMPLOYEE' 
             , p_requestor_id               => fnd_global.employee_id
             , p_review_type                => l_review_type
             , p_credit_classification      => c1_rec.credit_classification
             , p_requested_amount           => c1_rec.amount_requested
             , p_requested_currency         => 'USD'       --l_limit_curr_code
             , p_trx_amount                 => c1_rec.amount_requested
             , p_trx_currency               => 'USD' --p_header_rec.transactional_curr_code
             , p_credit_type                => 'TRADE'
             , p_term_length                => NULL --the unit is no of months
             , p_credit_check_rule_id       => 1041
             , p_credit_request_status      => 'SUBMIT'
             , p_party_id                   => l_party_id --l_review_party_id --bug 5907331
             , p_cust_account_id            => c1_rec.customer_id
             , p_cust_acct_site_id          => c1_rec.address_id
             , p_site_use_id                => c1_rec.bill_to_site_use_id
             , p_contact_party_id           => NULL --party_id of the pseudo party
             , p_notes                      => c1_rec.notes    --contact relationship.
             --, p_source_org_id              => 162           --l_source_org_id --10/10/2014 TMS#20141001-00167 Commented By Veera
			  , p_source_org_id              => l_source_org_id  --10/10/2014 TMS#20141001-00167 Added By Veera
             , p_source_user_id             => c1_rec.created_by --fnd_global.user_id --l_source_user_id
             , p_source_resp_id             => fnd_global.resp_id --l_source_resp_id
             , p_source_appln_id            => fnd_global.resp_appl_id --l_source_appln_id
             , p_source_security_group_id   => 0  --l_source_security_group_id
             , p_source_name                => 'OCM_UI'
             , p_source_column1             => null
             , p_source_column2             => null
             , p_source_column3             => null
             , p_credit_request_id          => l_request_id
           --  , p_hold_reason_rec            => l_hold_reason_rec   --ER8880886
                                                                );
            l_stage := 6;

            IF l_return_status <> 'S'
            THEN
               l_stage := 7;
               l_success := 'N';

               FOR i IN 1 .. FND_MSG_PUB.Count_Msg
               LOOP
                  apps.FND_MSG_PUB.Get (p_msg_index       => i
                                 , p_encoded         => 'F'
                                 , p_data            => out_message
                                 , p_msg_index_OUT   => l_msg_index_OUT);
                  l_msg_data := l_msg_data || ':' || out_message;
               END LOOP;
               
               UTL_MAIL.send (
                  sender       => l_refund_email
                , recipients   => l_refund_email
                , cc           => NULL
                , bcc          => NULL
                , subject      =>   'Case Folder failed for Lien Release- '
                                 || l_customer_name
                , MESSAGE      =>   'Case Folder failed for Lien Release- '
                                 || l_customer_name||' - '
                                 || l_return_status
                                 || ':'
                                 || l_msg_data
                , mime_type    => 'text/plain; charset=us-ascii'
                , priority     => 3
                , replyto      => NULL);
               UPDATE xxwc_ar_lien_rel_action_tbl
                  SET error_message = substr(l_msg_data,1,2000)
                WHERE rowid=c1_rec.rowid;
               --COMMIT;
            END IF;

            IF l_success = 'Y'
            THEN
               UPDATE xxwc_ar_lien_rel_action_tbl
                  SET process_flag = 'Y'
                WHERE rowid=c1_rec.rowid;
              -- COMMIT;
               
            --Create note in JTF tables
            
              jtf_notes_pub.Create_note
                ( p_parent_note_id     => null
                , p_jtf_note_id        => null
                , p_api_version        => 1.0
                , p_init_msg_list      => FND_API.G_TRUE
                , p_commit             => FND_API.G_FALSE
                , p_validation_level   => 100
                , x_return_status      => l_return_status
                , x_msg_count          => l_msg_count
                , x_msg_data           => l_note_msg_data
                , p_org_id              =>  NULL
                , p_source_object_id    => c1_rec.bill_to_site_use_id
                , p_source_object_code  => 'IEX_BILLTO'
                , p_notes               => c1_rec.notes
                , p_notes_detail        => NULL
                , p_note_status         => 'I'
                , p_entered_by          => FND_GLOBAL.USER_ID
                , p_entered_date        => sysdate 
                , x_jtf_note_id         => l_jtf_note_id 
                , p_last_update_date    => sysdate
                , p_last_updated_by     => FND_GLOBAL.USER_ID
                , p_creation_date       => sysdate
                , p_created_by          => FND_GLOBAL.USER_ID
                , p_last_update_login   => FND_GLOBAL.LOGIN_ID
                , p_attribute1          => NULL
                , p_attribute2          => NULL
                , p_attribute3          => NULL
                , p_attribute4          => NULL
                , p_attribute5          => NULL
                , p_attribute6          => NULL
                , p_attribute7          => NULL
                , p_attribute8          => NULL
                , p_attribute9          => NULL
                , p_attribute10         => NULL
                , p_attribute11         => NULL
                , p_attribute12         => NULL
                , p_attribute13         => NULL
                , p_attribute14         => NULL
                , p_attribute15         => NULL
                , p_context             => NULL
                , p_note_type           => 'ASF_CALLBACK'
               -- , p_jtf_note_contexts_tab => null
                );
            END IF;
         END IF;
      END LOOP;
      error_message := l_msg_data;
      
      IF l_msg_data is null then
       commit;
      ELSE
       rollback; 
      END IF;
   EXCEPTION
      WHEN OTHERS
      THEN
         NULL;
         -- l_stage := 13;
         UTL_MAIL.send (
            sender       => l_refund_email
          , recipients   => l_refund_email
          , cc           => NULL
          , bcc          => NULL
          , subject      =>   'Case Folder failed for Lien Release- '
                                 || l_customer_name
          , MESSAGE      => 'Case Folder failed for Lien Release- '
                                 || l_customer_name
          , mime_type    => 'text/plain; charset=us-ascii'
          , priority     => 3
          , replyto      => NULL);
   END create_case_folder;   
END xxwc_ar_lien_release_pkg;
/