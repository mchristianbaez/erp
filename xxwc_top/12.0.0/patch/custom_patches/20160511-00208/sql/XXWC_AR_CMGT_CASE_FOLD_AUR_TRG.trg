CREATE OR REPLACE TRIGGER APPS.XXWC_AR_CMGT_CASE_FOLD_AUR_TRG
   /**************************************************************************
     $Header XXWC_AR_CMGT_CASE_FOLD_AUR_TRG $
     Module Name: XXWC_AR_CMGT_CASE_FOLD_AUR_TRG
     PURPOSE:   This Trigger is used to raise the business event to submit ar credit management workflow backgroud process
     
	 REVISIONS:
     Ver        Date         Author                  Description
     ---------  -----------  ---------------         -------------------------
     1.0        19-Aug-2016  Rakesh Patel            TMS#20160511-00208-IT - Improvements to AR Credit Management backend workflow
     **************************************************************************/
AFTER UPDATE ON APPS.AR_CMGT_CASE_FOLDERS 
FOR EACH ROW
WHEN ( new.status = 'SUBMITTED' --OR new.status = 'SAVED'
 ) 
DECLARE
      l_error_msg               VARCHAR2 (240);      
      l_sec                     VARCHAR2 (100);
	  l_dflt_email            fnd_user.email_address%TYPE
                          := 'HDSOracleDevelopers@hdsupply.com';	  
BEGIN
    l_sec := 'Before raising business event';
    XXWC_OM_BE_PKG.raise_ar_cmg_process(:old.credit_request_id,NVL(:old.review_type,'XX'),l_error_msg);
EXCEPTION
      WHEN OTHERS
      THEN	 		 
         xxcus_error_pkg.xxcus_error_main_api (
            p_called_from         => 'XXWC_AR_CMGT_CASE_FOLD_AUR_TRG',
            p_calling             => l_sec,
            p_request_id          => fnd_global.conc_request_id,
            p_ora_error_msg       => SUBSTR (
                                          ' Error_Stack...'
                                       || DBMS_UTILITY.format_error_stack ()
                                       || ' Error_Backtrace...'
                                       || DBMS_UTILITY.format_error_backtrace (),
                                       1,
                                       2000),
            p_error_desc          => SUBSTR (SQLERRM, 1, 240),
            p_distribution_list   => l_dflt_email,
            p_module              => 'OM');
END;
/