
/*
Date: 11/22/2016
Author: Balaguru Seshadri
Ticket#                                                                                         Ver       Date                               Notes
============================================    ===== =============          ============================================================== 
TMS 20160927-00093                                                          1.0        11/21/2016               Remove Parallelism from Standard Indexes and Tables
*/
DECLARE
 --
 CURSOR ALL_OZF_INDX IS
 SELECT *
FROM ALL_INDEXES
WHERE 1 =1
AND OWNER ='OZF'
AND DEGREE NOT IN ('0', '1');
 --
 V_INDEX_NAME VARCHAR2(30);
 V_SQL VARCHAR2(250) :=NULL;
 V_DEGREE1 VARCHAR2(40) :=NULL;
 V_DEGREE2 VARCHAR2(40) :=NULL; 
 --
BEGIN
 --
 DBMS_OUTPUT.ENABLE(1000000);
 DBMS_OUTPUT.PUT_LINE(' '); 
 DBMS_OUTPUT.PUT_LINE('Begin disable all ozf indexes');
 --
 FOR REC IN ALL_OZF_INDX
 LOOP
  --
  V_INDEX_NAME :=REC.INDEX_NAME;
  V_DEGREE1 :=REC.DEGREE;
  --
  V_SQL :='ALTER INDEX '||REC.OWNER||'.'||REC.INDEX_NAME||' noparallel';
  --
  EXECUTE IMMEDIATE V_SQL;
  --
  SELECT DEGREE INTO V_DEGREE2 FROM ALL_INDEXES WHERE OWNER =REC.OWNER AND INDEX_NAME =REC.INDEX_NAME;
  --
  DBMS_OUTPUT.PUT_LINE('Index : '||V_INDEX_NAME||' original parallel degree : '||V_DEGREE1||', New parallel degree :'||V_DEGREE2);    
  --
 END LOOP;
 --
 DBMS_OUTPUT.PUT_LINE(' ');   
 DBMS_OUTPUT.PUT_LINE('End disable all ozf indexes');
 DBMS_OUTPUT.PUT_LINE(' ');   
EXCEPTION
 WHEN OTHERS THEN
  DBMS_OUTPUT.PUT_LINE('Index :'||V_INDEX_NAME||' SQL :'||V_SQL||', ERROR :'||SQLERRM);
END;
/