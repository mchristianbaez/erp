/* ************************************************************************
  $Header TMS_20170621-00186_Line_close.sql $
  Module Name: TMS_20170621-00186 Data Fix script

  PURPOSE: Data Fix script 

  REVISIONS:
  Ver        Date         Author                Description
  ---------  -----------  ------------------    --------------------------
  1.0        11-JUL-2017  Pattabhi Avula        TMS#20170621-00186

************************************************************************* */ 
SET SERVEROUTPUT ON SIZE 1000000;
DECLARE
BEGIN
   DBMS_OUTPUT.put_line ('TMS: 20170621-00186   , Before Update');
   
UPDATE oe_order_lines_all
   SET open_flag='N',
       flow_status_code='CLOSED'
 WHERE line_id=98793587
   AND header_id=60303923; 

 DBMS_OUTPUT.put_line (
         'TMS: 20170621-00186 Sales order lines updated (Expected:1): '
      || SQL%ROWCOUNT);

   COMMIT;

   DBMS_OUTPUT.put_line ('TMS: 20170621-00186   , End Update');
EXCEPTION
   WHEN OTHERS
   THEN
      ROLLBACK;
      DBMS_OUTPUT.put_line ('TMS: 20170621-00186, Errors : ' || SQLERRM);
END;
/