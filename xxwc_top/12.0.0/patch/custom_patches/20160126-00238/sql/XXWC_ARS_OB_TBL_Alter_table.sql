  /********************************************************************************
  FILE NAME: XXWC.XXWC_ARS_OB_TBL.sql
  
  PROGRAM TYPE: Table Script
  
  PURPOSE: ARS Outbound Interfaces staging table caloumn addition.
  
  HISTORY
  ===============================================================================
  VERSION DATE          AUTHOR(S)       DESCRIPTION
  ------- -----------   --------------- -----------------------------------------
  1.0     19/12/2016    Neha Saini   Initial creation 
  ********************************************************************************/
  
ALTER TABLE XXWC.XXWC_ARS_OB_TBL ADD  ACCOUNT_STATUS  VARCHAR2(30 BYTE);