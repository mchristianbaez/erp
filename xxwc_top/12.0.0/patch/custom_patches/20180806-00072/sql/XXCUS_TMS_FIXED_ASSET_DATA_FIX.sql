/*
 TMS:   20180806-00072
 Date:  07/19/2018
 Notes: Backup the fixed asset records table with wrong period_name column,copy them into backup table and update the base table with proper period names.
 Author: Ashwin Sridhar
*/
SET SERVEROUTPUT ON SIZE 1000000
DECLARE
 --
 n_loc NUMBER :=0;
 v_db  VARCHAR2(30) :=Null;
 v_ok  BOOLEAN :=Null;
 v_err VARCHAR2(5):='N';
 ln_rec_count NUMBER:=0;
 ln_upd_count NUMBER:=0;
 --
TYPE t_fa_deprn_type IS TABLE OF FA.FA_DEPRN_PERIODS%ROWTYPE INDEX BY BINARY_INTEGER;
c_fa_deprn_type_rec t_fa_deprn_type;
 
--Cursor for FA_DEPRN_PERIODS...
CURSOR c_dprn_per IS
SELECT fdp.*,fdp.rowid row_id
FROM   FA.FA_DEPRN_PERIODS  fdp
WHERE  period_name LIKE'%-18';

--Cursor for FA_CALENDAR_PERIODS...
CURSOR c_cal_per IS
SELECT fcp.*,fcp.rowid row_id
FROM   FA.FA_CALENDAR_PERIODS fcp
WHERE  period_name LIKE'%-18';

BEGIN --Main Processing...
  --
  n_loc :=101;

  --Updating the records for the table FA_DEPRN_PERIODS...
  DBMS_OUTPUT.put_line ('Start Updating the records for the table FA_DEPRN_PERIODS.');
  BEGIN 
    --
    n_loc :=102;
    --
    EXECUTE IMMEDIATE 'CREATE TABLE xxcus.xxcus_tms_fa_dprn_per_bkp as  
    SELECT *
    FROM  FA.FA_DEPRN_PERIODS  
    WHERE PERIOD_NAME like''%-18''';   
    --
    v_ok :=TRUE;
    --    
    n_loc :=103;
    --
    DBMS_OUTPUT.put_line ('Backup table xxcus.xxcus_tms_fa_dprn_per_bkp created for the base table FA_DEPRN_PERIODS.');
    --
  EXCEPTION
  WHEN others THEN 
    --
    v_ok :=FALSE;
    --
    n_loc :=104;
    --
  END;
  --
  IF (v_ok) THEN
  -- 
    n_loc :=105;
    --
    v_ok :=Null;       
    --
    BEGIN
      --
      n_loc :=106;
      --
      FOR rec_dprn_per IN c_dprn_per LOOP
      --
        ln_rec_count:=ln_rec_count+1; 
        v_ok:=Null;
        v_err:='N';
        
        BEGIN
      
          UPDATE fa.fa_deprn_periods 
          SET period_name = SUBSTR(period_name,1,5)||'018' 
          WHERE ROWID=rec_dprn_per.row_id;
      
        EXCEPTION
        WHEN others THEN
      
          v_err:='Y';
          v_ok:=FALSE;
      
        END;
      
        --
        IF v_err='N' THEN
        --
          ln_upd_count:=ln_upd_count+1;
          dbms_output.put_line('Updated Record for book type code:'||rec_dprn_per.BOOK_TYPE_CODE||', for Period Name :'||rec_dprn_per.PERIOD_NAME);
        --
        END IF;
        
      END LOOP;
      --         
       n_loc :=107;
     --
     EXCEPTION
     WHEN others THEN
     --
       n_loc :=108;
     --
       dbms_output.put_line('@ n_loc ='||n_loc||', message ='||SQLERRM);
     --
     END;
  --
  ELSE
   
    DBMS_OUTPUT.put_line ('v_ok is FALSE...check xpatch log files for error messages.');

  END IF;
  --
  COMMIT;
  --
  DBMS_OUTPUT.put_line ('Total Records updated for table FA_DEPRN_PERIODS.'||ln_upd_count);
  
  DBMS_OUTPUT.put_line ('End Updating the records for the table FA_DEPRN_PERIODS.');
  
  --
  --Updating the records for the table FA_CALENDAR_PERIODS...
  DBMS_OUTPUT.put_line ('Start Updating the records for the table FA_CALENDAR_PERIODS.');
  BEGIN 
  
    --Initialising the local variables for next run...
    ln_rec_count:=0;
    ln_upd_count:=0; 
    v_ok        :=NULL;
    v_err       :='N';

    --
    n_loc :=109;
    --
    EXECUTE IMMEDIATE 'CREATE TABLE xxcus.xxcus_tms_fa_cald_per_bkp as  
    SELECT *
    FROM  FA.FA_CALENDAR_PERIODS  
    WHERE PERIOD_NAME like''%-18''';   
    --
    v_ok :=TRUE;
    --    
    n_loc :=110;
    --
    DBMS_OUTPUT.put_line ('Backup table xxcus.xxcus_tms_fa_cald_per_bkp created for the base table FA_CALENDAR_PERIODS.');
    --
  EXCEPTION
  WHEN others THEN 
    --
    v_ok :=FALSE;
    --
    n_loc :=111;
    --
  END;
  --
  IF (v_ok) THEN
  -- 
    n_loc :=112;
    --
    v_ok :=Null;       
    --
    BEGIN
      --
      n_loc :=113;
      --
      FOR rec_cal_per IN c_cal_per LOOP
      --
        ln_rec_count:=ln_rec_count+1; 
        v_ok  :=Null;
        v_err :='N';
        
        BEGIN
      
          UPDATE fa.FA_CALENDAR_PERIODS 
          SET period_name = SUBSTR(period_name,1,5)||'018' 
          WHERE ROWID=rec_cal_per.row_id;
      
        EXCEPTION
        WHEN others THEN
      
          v_err:='Y';
          v_ok:=FALSE;
      
        END;
      
        --
        IF v_err='N' THEN
        --
          ln_upd_count:=ln_upd_count+1;
          dbms_output.put_line('Updated Record for CALENDAR TYPE:'||rec_cal_per.CALENDAR_TYPE||', for Period Name :'||rec_cal_per.PERIOD_NAME);
        --
        END IF;
        
      END LOOP;
      --         
       n_loc :=114;
     --
     EXCEPTION
     WHEN others THEN
     --
       n_loc :=115;
     --
       dbms_output.put_line('@ n_loc ='||n_loc||', message ='||SQLERRM);
     --
     END;
  --
  ELSE
   
    DBMS_OUTPUT.put_line ('v_ok is FALSE...check xpatch log files for error messages.');

  END IF;
  --
  COMMIT;
  --
  DBMS_OUTPUT.put_line ('Total Records updated for table FA_CALENDAR_PERIODS.'||ln_upd_count);
  
  DBMS_OUTPUT.put_line ('End Updating the records for the table FA_CALENDAR_PERIODS.');
  
EXCEPTION
WHEN others THEN
      
  DBMS_OUTPUT.put_line ('TMS: 20180806-00072, Errors =' || SQLERRM);
  
  ROLLBACK;

END;
/