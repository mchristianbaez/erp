CREATE OR REPLACE PACKAGE BODY APPS.XXWC_AHH_INTF_STATS_PKG AS
/************************************************************************************************************************************
   $Header XXWC_AHH_INTF_STATS_PKG $
   Module Name: XXWC_AHH_INTF_STATS_PKG.pks

   PURPOSE:   This package is called by the concurrent programs
              To sed email 
   REVISIONS:
   Ver        Date        Author             Description
   ---------  ----------  ---------------   -------------------------------------------------------------------------------------
    1.0       08/07/2018  P.Vamshidhar       Initial Build - Task ID:TMS#20180803-00026 - AH Harries Interface Stats Automation
    2.0       08/09/2018  Rakesh Patel       TMS#20180808-00128 -Fixs for the AHH daily interface report
	3.0       08/21/2018  P.Vamshidhar       TMS#20180821-00001 - XXWC AHH Interface Stats Automation
	                                         PDF file name change and creating UC4 job for stats.  
  *******************************************************************************************************************************/
g_pkg_name VARCHAR2(1000):='XXWC_AHH_INTF_STATS_PKG';
/*************************************************************************
  Procedure : write_error

  PURPOSE:   This procedure logs debug message in Concurrent Out file
  Parameter: IN  p_debug_msg      -- Debug Message
  REVISIONS:
  Ver        Date        Author             Description
  ---------  ----------  ---------------   -------------------------
    1.0       08/07/2018  P.Vamshidhar       Initial Build - Task ID:TMS#20180803-00026 - AH Harries Interface Stats Automation
************************************************************************/
--Add message to concurrent output file
PROCEDURE write_error (p_debug_msg IN VARCHAR2) IS

l_req_id          NUMBER := fnd_global.conc_request_id;
l_err_callfrom    VARCHAR2 (75) DEFAULT 'XXWC_AHH_INTF_STATS_PKG';
l_err_callpoint   VARCHAR2 (75) DEFAULT 'START';
l_distro_list     VARCHAR2 (75) DEFAULT 'HDSOracleDevelopers@hdsupply.com';

BEGIN

  xxcus_error_pkg.xxcus_error_main_api (
      p_called_from         => l_err_callfrom,
      p_calling             => l_err_callpoint,
      p_request_id          => l_req_id,
      p_ora_error_msg       => SUBSTR (p_debug_msg, 1, 2000),
      p_error_desc          => 'Error running XXWC_AHH_INTF_STATS_PKG with PROGRAM ERROR',
      p_distribution_list   => l_distro_list,
      p_module              => 'XXWC');

END write_error;

/*************************************************************************************************************
  Procedure : afterReport
  REVISIONS:
  Ver        Date        Author             Description
  ---------  ----------  ---------------   -------------------------
	3.0       08/21/2018  P.Vamshidhar       TMS#20180821-00001 - XXWC AHH Interface Stats Automation
	                                         PDF file name change and creating UC4 job for stats.  
**************************************************************************************************************/
FUNCTION afterReport RETURN BOOLEAN AS

l_sub_routine      VARCHAR2(30) :='afterReport';
g_notif_email_from VARCHAR2(240):='no_reply@hdsupply.com';
l_email_subject    VARCHAR2(240):='AHH Interface Stats for '||TO_CHAR(SYSDATE, 'DD-MON-YYYY')||' Files';--TMS#20180808-00128 -Fixs for the AHH daily interface report
g_file_date        VARCHAR2(240) :=TO_CHAR(SYSDATE, 'DD-MON-YYYY'); --TMS#20180808-00128 -Fixs for the AHH daily interface report
l_non_prod_email   VARCHAR2(240):=fnd_profile.value('XXWC_AHH_INTERFACE_STATS_EMAIL_PROF');
ln_request_id      NUMBER:=fnd_global.conc_request_id;
l_file_name        VARCHAR2(250):='XXWC_AHH_INTERFACE_STAT_'||ln_request_id||'_1.PDF';
l_req_id           NUMBER;
--l_new_file         VARCHAR2(250):='XXWC_AHH_INTERFACE_STATS.PDF'; -- Commented in Rev 3.0
l_new_file         VARCHAR2(250);   -- Added in Rev 3.0
lv_arg1            VARCHAR2(100);
lv_arg2            VARCHAR2(100);

BEGIN
  
  FND_FILE.PUT_LINE(FND_FILE.LOG,'Parent Request ID '||ln_request_id);
  
  BEGIN
  
    SELECT ARGUMENT1
    ,      ARGUMENT2
    INTO   lv_arg1
    ,      lv_arg2
    FROM apps.FND_CONCURRENT_REQUESTS
    WHERE request_id=ln_request_id;
  
  EXCEPTION
  WHEN others THEN
  
    lv_arg1:=NULL;
    lv_arg2:=NULL;
  
  END;
  
  -- Added below code in Rev 3.0
  select UPPER(name)||'_FIN_INTERFACES_'||TO_CHAR(SYSDATE, 'DDMONYY')||'_STATS.PDF' INTO l_new_file from  V$DATABASE;
  
  l_req_id := fnd_request.submit_request
                 (
                   'XXWC'
                  ,'XXWC_AHH_INTF_STATS_RPT'
                  ,NULL
                  ,NULL
                  ,FALSE
                  ,l_file_name 
                  ,l_non_prod_email -- non production recipient email id
                  ,g_notif_email_from --sender email id
                  ,l_email_subject --email subject
                  ,l_new_file  
                  ,g_file_date --TMS#20180808-00128 -Fixs for the AHH daily interface report
                 );
 
  COMMIT;

  IF l_req_id>0 THEN
  
    FND_FILE.PUT_LINE(FND_FILE.LOG,'Request Submitted successfully with request ID '||l_req_id);

    COMMIT;

  ELSE
  
    FND_FILE.PUT_LINE(FND_FILE.LOG,'Issue in submitting the request ');
  
  END IF;

  RETURN TRUE;
  
EXCEPTION
WHEN others THEN

 FND_FILE.PUT_LINE(FND_FILE.LOG,'Issue in '||g_pkg_name||'.'||l_sub_routine||' msg ='||sqlerrm);

  RETURN FALSE;

END afterReport;

FUNCTION beforereport RETURN BOOLEAN AS

ln_request_id NUMBER :=0;
l_sub_routine VARCHAR2(30) :='beforereport';
p_run_id      NUMBER;
p_group_id    VARCHAR2(240);

BEGIN
  
  NULL;  
  return true;
  
EXCEPTION
WHEN others THEN

  FND_FILE.PUT_LINE(FND_FILE.LOG,'Issue in '||g_pkg_name||'.'||l_sub_routine||' msg ='||sqlerrm);

  RETURN FALSE;

END beforereport;

END XXWC_AHH_INTF_STATS_PKG;
/