CREATE OR REPLACE PACKAGE BODY APPS.XXCUS_PO_POST_PROCESS AS
   /* ************************************************************************
    HD Supply
    All rights reserved.
   **************************************************************************
     PURPOSE: Rename the oracle pdf output to a different file name and email it to the requisition preparer.
                         Original file is created by Oracle job "HDS PO Output for Communication".
     REVISIONS:
     Ticket                          Ver             Date               Author                            Description
     ----------------------    ----------   ----------          ------------------------      -------------------------
     20171004-00368  1.0           10/04/2017    Bala Seshadri              Created.
   **************************************************************************/
   --
   Function rename_and_email
                     (
                        P_Subscription_Guid In Raw
                       ,P_Event             In Out Nocopy Wf_Event_T
                     )
      Return Varchar2 Is
      --
      L_Api_Name    Constant Varchar2(30) := 'rename_file';
      L_Api_Version Constant Number := 1.0;
      L_Rule           Varchar2(20);
      --
      l_command        varchar2(4000) :=Null;
      l_result                varchar2(4000) :=Null;
      l_msg                   varchar2(2000)   :=Null;
      --
      L_Parameter_List Wf_Parameter_List_T := Wf_Parameter_List_T();
      L_Parameter_T    Wf_Parameter_T      := Wf_Parameter_T(Null, Null);
      L_Parameter_Name Varchar2(2000);
      --
      Param_Index      Pls_Integer;
      L_Event_Key      Varchar2(240);
      L_Event_Name     Varchar2(50);
      --
      l_request_id number :=0;
      l_email_request_id number :=0;      
      l_concurrent_program_id          Number :=0;
      l_outfile_name apps.fnd_concurrent_requests.outfile_name%type :=Null;
      l_temp_file_name varchar2(240) :=Null;
      --
      l_cp_id number :=0;
      l_po_number po.po_headers_all.segment1%type :=Null;
      --
     l_po_header_id NUMBER :=0;
     l_org_id  NUMBER :=0;
     --
     l_preparer_name fnd_user.description%type :=Null;
     l_preparer_email fnd_user.email_address%type :=Null;
     l_email_subject varchar2(200) :=Null;
     l_email_cc varchar2(240) :=Null;
     l_email_from varchar2(240) :='OracleEBS.GSC.PO.Auto@hdsupply.com';
     --
   --
   Begin 
              begin 
                select concurrent_program_id
                into     l_cp_id
                from   fnd_concurrent_programs_vl
                where 1 =1
                and concurrent_program_name ='POXPOPDF1'; -- GSC's job to print PO...HDS PO Output for Communication
              exception
               when no_data_found then
                l_cp_id :=0;
               when others then
                l_cp_id :=0;
              end;

              L_Parameter_List := P_Event.Getparameterlist();
              L_Event_Key      := P_Event.Geteventkey();

              If L_Parameter_List Is Not Null Then

                 Param_Index := L_Parameter_List.First;

                 While (Param_Index <= L_Parameter_List.Last)

                 Loop
                    if ( l_parameter_list(param_index).getname() ='REQUEST_ID') then
                        --
                        l_request_id := l_parameter_list(param_index).getvalue();
                        --
                    elsif ( l_parameter_list(param_index).getname() ='CONCURRENT_PROGRAM_ID') then
                        --
                         l_concurrent_program_id := l_parameter_list(param_index).getvalue();
                        --
                    else
                     Null; --We don't need other parameters...
                    end if;
                    Param_Index :=L_Parameter_List.Next(Param_Index);
                    --
                 End Loop;
                --
              End If;
          --
          -- lets move forward with checking other things if and only if the program was "Format Payment Instructions with Text Output"
          --
          if ( l_concurrent_program_id = l_cp_id ) then
           --
           begin
                --
                 l_temp_file_name :='HDS_PO_'||l_po_number||'.pdf';
                 --           
                select outfile_name 
                           ,argument3 
                           ,substr('/ebizlog/conc/out/o185701924.out', 1, instr('/ebizlog/conc/out/o185701924.out','/',-1))||'HDS_PO_'||argument3||'.pdf'
                into     l_outfile_name
                           ,l_po_number
                           ,l_temp_file_name
                from   fnd_concurrent_requests
                where  1 =1
                    and  request_id =l_request_id;
                 --
                 if ( l_outfile_name is not null ) then
                  --
                      begin 
                                select  distinct  -- DP019342 Pamblanco, David A
                                            (
                                                select description
                                                from fnd_user a
                                                where 1 =1
                                                and a.employee_id =d.preparer_id 
                                            ) preparer_name 
                                           ,(
                                                select email_address
                                                from fnd_user a
                                                where 1 =1
                                                and a.employee_id =d.preparer_id 
                                            ) preparer_email
                                          ,'Purchase order output attached - PO# '||e.segment1 email_subject
                                          ,e.po_header_id
                                          ,e.org_id
                                into    l_preparer_name
                                          ,l_preparer_email
                                          ,l_email_subject
                                          ,l_po_header_id
                                          ,l_org_id
                                from po_headers_all e
                                        ,po_distributions_all a
                                        ,po_req_distributions_all b 
                                        ,po_requisition_lines_all c
                                        ,po_requisition_headers_all d
                                where 1  =1
                                and e.segment1 =l_po_number
                                and a.po_header_id =e.po_header_id
                                and b.distribution_id = a.req_distribution_id
                                and c.requisition_line_id =b.requisition_line_id
                                and d.requisition_header_id =c.requisition_header_id
                                 ;                       
                      exception
                       when no_data_found then
                                          l_preparer_name :=Null;
                                          l_preparer_email :=Null;
                                          l_email_subject :=Null;                      
                       when too_many_rows then
                                          l_preparer_name :=Null;
                                          l_preparer_email :=Null;
                                          l_email_subject :=Null;
                       when others then
                                          l_preparer_name :=Null;
                                          l_preparer_email :=Null;
                                          l_email_subject :=Null;
                      end;
                  --
                    if l_preparer_email is not null then
                         --
                              begin
                                    l_email_request_id :=fnd_request.submit_request
                                          (
                                            application      =>'XXCUS'
                                           ,program          =>'XXCUS_PO_POST_PROCESS'
                                           ,description      =>''
                                           ,start_time       =>''
                                           ,sub_request      =>FALSE
                                           ,argument1        =>l_outfile_name -- original concurrent output file with absolute path
                                           ,argument2        =>l_temp_file_name -- new file with absolute path
                                           ,argument3     => l_preparer_email -- Requisition preparer email
                                           ,argument4     =>l_email_subject -- email subject 
                                           ,argument5     =>l_email_from -- Email from
                                          );
                                          --
                                             --
                                             begin
                                              --
                                                Insert into xxcus.xxcus_po_post_process_log
                                                (
                                                     po_header_id
                                                    ,po_number
                                                    ,org_id
                                                    ,po_print_request_id
                                                    ,po_email_request_id
                                                    ,concurrent_outfile
                                                    ,temp_file
                                                    ,preparer_email
                                                    ,email_subject
                                                    ,email_from
                                                    ,status
                                                    ,creation_date
                                                )   
                                                values
                                                (
                                                     l_po_header_id
                                                    ,l_po_number
                                                    ,l_org_id
                                                    ,l_request_id
                                                    ,l_email_request_id
                                                    ,l_outfile_name
                                                    ,l_temp_file_name
                                                    ,l_preparer_email
                                                    ,l_email_subject
                                                    ,l_email_from
                                                    ,case
                                                      when l_email_request_id >0 then 'Email job kicked off for PO '||l_po_number
                                                      else 'Email job failed to send PO '||l_po_number
                                                     end
                                                    ,sysdate                            
                                                );                       
                                              --
                                             exception
                                              when others then 
                                               Null;
                                             end;
                                             --                                          
                                          --commit;
                                          --
                              exception
                                when no_data_found then
                                      Null;
                                when others then
                                       Null;
                              end;                     
                         --
                    end if;
                  --
                 end if;
                 --
           exception
            when no_data_found then
               l_po_number :='0';
            when others then
               l_po_number :='0';
           end;
           --
          end if;
          --
          Return 'SUCCESS';
          --
      --Else
        --Null; --Other than EBIZPRD, we are not going to monitor the events...
      --End If;
   Exception
      When Others Then
       WF_CORE.CONTEXT('oracle.apps.fnd.concurrent.request.completed',
                             p_event.getEventName( ), p_subscription_guid);
       WF_EVENT.setErrorInfo(p_event, 'ERROR');
         Return 'ERROR';
   End rename_and_email;
   --
END XXCUS_PO_POST_PROCESS;
/