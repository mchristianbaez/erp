CREATE OR REPLACE PACKAGE BODY APPS.xxcus_tm_interface_pkg IS

  l_dflt_email VARCHAR2(200) := 'HDSOracleDevelopers@hdsupply.com'; -- email used for errors within
  l_bu_attr    VARCHAR2(30) := 'HDS_BU';
  l_prod_instance VARCHAR2(10) :='EBSPRD'; --Ver 3.1
  l_email_id_prd VARCHAR2(60) :='HDSOracleRebates@HDSupply.com'; --Ver 3.1
  l_email_id_non_prd VARCHAR2(60) :='HDSRebateManagementSystemNotifications@hdsupply.com'; --Ver 3.1
  l_rebates_audit_job_code varchar2(30) :='XXCUS_REBATES_AUDIT'; --ver 3.2
  l_sleep_seconds number :=300; -- 3 minutes -- Ver 3.3
  --
  /*******************************************************************************
   HISTORY
   ===============================================================================
   VERSION DATE          AUTHOR(S)       DESCRIPTION
   ------- -----------   --------------- -----------------------------------------
   2.0     07/21/2014    Kathy Poling    SR 257808 RFC 41043 Change in procedure
                                         load_rebate_receipts
   2.1     08/12/2014    Kathy Poling    SR 258560 Change in procedure submit_request_set
                                         RFC 41267
  2.2     09/02/2014    Bala Seshadri   Add filter inventory_item_status_code to
                                        procedure receipt_exceptions and comment out
                                       enabled_flag
   2.3    10/14/2014    Bala Seshadri  ESMS 265235
   2.4    10/20/2014     Kathy Poling   ESMS 531490 RFC 42024 change in procedure
                                        load_rebate_product, oneoff_rebate_products
                                        and load_rebate_receipts
   2.5    10/25/2014    Bala Seshadri   RFC 42038 / ESMS 267539
   2.6    12/29/2014    Kathy Poling   ESMS 244996 adding the ability to disable the
                                       weekly request set from procedure load_rebate_receipts
                                       ESMS 240310 made check for trade profile an outer join
                                       in procedure load_rebate_receipts, oneoff_rebate_receipt
                                       and receipt_exceptions
                                       ESMS 275319 added date check for prior to submitting
                                       the weekly request set rom procedure load_rebate_receipts
                                       RFC 42740
   2.7   03/02/2015   Bala Seshadri   RFC 43009 / ESMS 280366.
                                       Changes to routine submit_request_set
                                       a) Comment code that refreshes xxucsozf_accrual_mv
                  b) Invoke Update collector program after PAM refresh
                                       c) Add YTD income extract stage
   2.7   04/07/2015   Bala Seshadri    ESMS 283148. Add  stage EXTRACT_GL_SUMMARY [Sequence 68]
   2.8   04/21/2015   Bala Seshadri    ESMS 285756. Replace edw_common.edw_rebt_pt_vndr_mv@dw_edw_lnk with
                                        EA_APPS.MVID_APEX_REBT_PT_VNDR_V@EAAPXPRD
   2.9   02/10/2016   Bala Seshadri   TMS 20160208-00208 or  ESMS 316266. Comment logic for HDS Rebates: SLA Exceptions Report and HDS Rebates: Generate TM GL Extract for US and CANADA
   3.0  04/08/2016   Balaguru Seshadri TMS 20160407-00172 / ESMS 322629, EBSPRD migration related fixes FOR GSC objects
   3.1   08/22/2016     Balaguru Seshadri TMS 20160711-00227  / ESMS 449491 - Add fixes for rebates data reconciliation.
   3.2   01/17/2016     Balaguru Seshadri TMS ???????????? / ESMS 449491 - Add fixes to set BI Template [RTF] for the last stage of the request set.
   3.3   11/18/2016     Balaguru Seshadri TMS 20161116-00075 / ESMS 486287 -Add % Var in Rebate Audit Report
   3.4   03/29/2017    Balaguru Seshadri  TMS 20170329-00173 - Add debug statement
   3.5   07/13/2017    Balaguru Seshadri  TMS 20170713-00081 - Fixes for adjusment period logic and CT4 total amount calculation
   3.6   10/29/2017    Balaguru Seshadri -- 12C database upgrade..replace parallel auto hint to exact threads 
   3.7   11/28/2017    Balaguru Seshadri -- TMS 20170202-00054
                                                                              Move DB link pointers from xpeprd to eaapxprd. Note Db link name remains same [apxprd_lnk].
                                                                              Just the source of the DB link is changed from xpeprd to eaapxprd.                                                        
   ********************************************************************************/
  --
  --
  PROCEDURE print_log(p_message IN VARCHAR2) IS
  BEGIN
    IF fnd_global.conc_request_id > 0
    THEN
      fnd_file.put_line(fnd_file.log, p_message);
    ELSE
      dbms_output.put_line(p_message);
    END IF;
  END print_log;
  --
  PROCEDURE print_output(p_message IN VARCHAR2) IS
  BEGIN
    IF fnd_global.conc_request_id > 0
    THEN
      fnd_file.put_line(fnd_file.output, p_message);
    ELSE
      dbms_output.put_line(p_message);
    END IF;
  END print_output;
  --
  FUNCTION get_gl_num_of_days RETURN NUMBER IS
    --
    n_num_of_days NUMBER := 0;
    --
  BEGIN
    --
    -- The user profile HDS Rebates: GL Number Of Days gives the number of days
    -- we have to add to the fiscal period start to derive the date_invoiced
    -- which also is the source for gl_date
    --
    SELECT fnd_profile.value('XXCUS_OZF_RECEIPT_GL_NUM_OF_DAYS')
      INTO n_num_of_days
      FROM dual;
    --
    RETURN n_num_of_days;
    --
  EXCEPTION
    WHEN no_data_found THEN
      RETURN n_num_of_days;
    WHEN OTHERS THEN
      RETURN n_num_of_days;
      print_log('Unable to find the number of days. Please check the profile HDS Rebates: GL Number Of Days. ');
  END get_gl_num_of_days;
  --
  PROCEDURE get_gl_segments(p_bu_nm        IN VARCHAR2
                           ,p_brnch_cd     IN VARCHAR2
                           ,x_prod_segment OUT VARCHAR2
                           ,x_loc_segment  OUT VARCHAR2) IS

    CURSOR chk_len_brnch(v_brnch_cd IN VARCHAR2) IS
      SELECT length(v_brnch_cd) FROM dual;

    CURSOR get_segments(p_bu_nm IN VARCHAR, p_brnch_cd IN VARCHAR2) IS
      SELECT entrp_entity, entrp_loc
        FROM apps.xxcus_rebate_branch_vw
       WHERE dw_bu_desc = p_bu_nm
         AND oper_branch = p_brnch_cd;

    CURSOR get_defaults(p_bu_nm IN VARCHAR) IS
      SELECT attribute4, attribute5
        FROM hz_parties
       WHERE party_name = p_bu_nm
         AND attribute1 = l_bu_attr;

    l_prod_segment VARCHAR2(20);
    l_loc_segment  VARCHAR2(20);
    l_len_brnch    VARCHAR2(20);
    l_brnch_cd     VARCHAR2(20);
  BEGIN

    OPEN chk_len_brnch(p_brnch_cd);
    FETCH chk_len_brnch
      INTO l_len_brnch;
    CLOSE chk_len_brnch;

    OPEN get_segments(p_bu_nm, p_brnch_cd);
    FETCH get_segments
      INTO l_prod_segment, l_loc_segment;
    CLOSE get_segments;

    IF p_bu_nm = 'WATERWORKS' OR p_bu_nm = 'WHITECAP CONSTRUCTION'
    THEN

      IF l_len_brnch < '3'
      THEN

        l_brnch_cd := lpad(p_brnch_cd, 3, '0');

      ELSE

        l_brnch_cd := p_brnch_cd;

      END IF;

    ELSE

      l_brnch_cd := p_brnch_cd;

    END IF;

    OPEN get_segments(p_bu_nm, l_brnch_cd);
    FETCH get_segments
      INTO l_prod_segment, l_loc_segment;
    CLOSE get_segments;

    IF l_prod_segment IS NULL OR l_loc_segment IS NULL
    THEN
      OPEN get_defaults(p_bu_nm);
      FETCH get_defaults
        INTO l_prod_segment, l_loc_segment;
      CLOSE get_defaults;
    END IF;

    x_prod_segment := l_prod_segment;
    x_loc_segment  := l_loc_segment;

  EXCEPTION
    WHEN OTHERS THEN
      NULL;
  END get_gl_segments;

  /*******************************************************************************
  * Procedure:   LOAD_BRANCH_LOCATION
  * Description: This will create FRU and BRANCH Locations as Internal Customers
  *              in Oracle.  A Party relationship will be created betwenn the LOB
  *              and the FRU
  HISTORY
  ===============================================================================
  VERSION DATE          AUTHOR(S)       DESCRIPTION
  ------- -----------   --------------- -----------------------------------------
  1.0     01/27/2010    Kathy Poling    Initial creation of the procedure
  1.1     05/19/2012    Kathy Poling    Removed truncate on the interface tables
                                        and added delete to limit to only Rebate
                                        customers.  Changed responsibility had to set
                                        profile to allow manual site numbering
  1.2     07/26/2012    Kathy Poling    Changes made to use location code table for
                                        creating the branches for Phase 2.  Location
                                        Data doesn't have all branches which had more
                                        receipts going to a default "Drop Ship" for
                                        the ship to
  ********************************************************************************/

  PROCEDURE load_branch_location(errbuf OUT VARCHAR2, retcode OUT NUMBER) IS

    --
    -- Package Variables
    --
    l_req_id             NUMBER NULL;
    v_phase              VARCHAR2(50);
    v_status             VARCHAR2(50);
    v_dev_status         VARCHAR2(50);
    v_dev_phase          VARCHAR2(50);
    v_message            VARCHAR2(250);
    v_interval           NUMBER := 30; -- In seconds
    v_max_time           NUMBER := 15000; -- In seconds
    v_error_message      VARCHAR2(3000);
    l_can_submit_request BOOLEAN := TRUE;
    l_err_msg            VARCHAR2(3000);
    l_err_code           NUMBER;
    l_sec                VARCHAR2(255);
    l_count              NUMBER := 0;
    l_user_id            fnd_user.user_id%TYPE; --REBTINTERFACE user
    l_address            VARCHAR2(240);
    l_county             VARCHAR2(60);
    l_city               VARCHAR2(60);
    l_state              VARCHAR2(60);
    l_country            VARCHAR2(60);

    l_module        VARCHAR2(150) := 'XXCUS_INTERFACE';
    l_category_code VARCHAR2(30) := 'FRU_LOCATION';
    l_party_attr    VARCHAR2(30) := 'HDS_LOB';
    l_cust_attr CONSTANT ra_customers_interface_all.customer_attribute1%TYPE := 'HDS_LOB_BRANCH';
    l_party NUMBER;
    l_org   hr_all_organization_units.organization_id%TYPE;
    l_us_org CONSTANT hr_all_organization_units.organization_id%TYPE := 101; --HDS Rebates USA - Org
    l_cn_org CONSTANT hr_all_organization_units.organization_id%TYPE := 102; --HDS Rebates CAN - Org
    l_user   CONSTANT VARCHAR2(100) := 'REBTINTERFACE';
    l_responsibility      VARCHAR2(100) := 'XXCUS_CON'; --'HDS Trade Management Administrator - Rebates'
    l_level               VARCHAR2(15); --Version 1.3 used for address validation
    l_responsibility_name VARCHAR2(100) := 'Receivables Manager'; --Version 1.3 used for address validation

    -- Party API
    p_organization_rec hz_party_v2pub.organization_rec_type;
    x_party_id         NUMBER;
    x_party_number     VARCHAR2(2000);
    x_profile_id       NUMBER;

    --Customer Account API
    p_cust_account_rec      hz_cust_account_v2pub.cust_account_rec_type;
    p_object_version_number NUMBER;

    x_return_status VARCHAR2(2000);
    x_msg_count     NUMBER;
    x_msg_data      VARCHAR2(2000);

    -- Error DEBUG
    l_err_callfrom  VARCHAR2(75) DEFAULT 'XXCUS_TM_INTERFACE_PKG.LOAD_BRANCH';
    l_err_callpoint VARCHAR2(75) DEFAULT 'START';
    l_distro_list   VARCHAR2(75) DEFAULT 'HDSOracleDevelopers@hdsupply.com';

  BEGIN

    BEGIN
      SELECT user_id INTO l_user_id FROM fnd_user WHERE user_name = l_user;

    EXCEPTION
      WHEN no_data_found THEN
        l_err_msg := 'User name ' || l_user || ' not defined in Oracle';
        RAISE program_error;
      WHEN OTHERS THEN
        l_err_msg := 'Error deriving user_id for user name ' || l_user;
        RAISE program_error;
    END;

    --  Setup parameters for running FND JOBS!
    l_can_submit_request := xxcus_misc_pkg.set_responsibility(l_user
                                                             ,l_responsibility);
    IF l_can_submit_request
    THEN
      l_sec := 'Global Variables are set.';

      fnd_file.put_line(fnd_file.log, l_sec);
      fnd_file.put_line(fnd_file.output, l_sec);

    ELSE

      l_sec := 'Global Variables are not set for the XXCUS INTERFACE.';
      fnd_file.put_line(fnd_file.log, l_sec);
      fnd_file.put_line(fnd_file.output, l_sec);
      RAISE program_error;
    END IF;

    --  END OF Setup parameters for running FND JOBS!

    --Clear contents of table
    l_sec := 'Deleting data from customer and profiles interface for Rebates ORG 101 and 102.';
    DELETE FROM ar.ra_customers_interface_all
     WHERE org_id IN (l_us_org, l_cn_org); --Version 1.1
    DELETE FROM ar.ra_customer_profiles_int_all
     WHERE org_id IN (l_us_org, l_cn_org); --Version 1.1

    fnd_file.put_line(fnd_file.log, l_sec);
    fnd_file.put_line(fnd_file.output, l_sec);

    BEGIN
      --EXECUTE IMMEDIATE 'truncate table ar.ra_customers_interface_all';   --Version 1.1
      --EXECUTE IMMEDIATE 'truncate table ar.ra_customer_profiles_int_all'; --Version 1.1
      --Clear contents of the prior run table
      l_sec := 'Truncate the prior table to load data from current for comparison.';
      fnd_file.put_line(fnd_file.log, l_sec);
      fnd_file.put_line(fnd_file.output, l_sec);
      EXECUTE IMMEDIATE 'truncate table xxcus.xxcus_vrm_location_prior';
    EXCEPTION
      WHEN OTHERS THEN
        l_err_msg := 'Failed to Truncate xxcus.xxcus_vrm_location_prior : ' ||
                     SQLERRM;
        fnd_file.put_line(fnd_file.log, l_err_msg);
        RAISE program_error;
    END;

    --Load table to compare data
    BEGIN
      INSERT /*+ APPEND */
      INTO xxcus.xxcus_vrm_location_prior
        SELECT * FROM xxcus.xxcus_vrm_location_curr;

      COMMIT;

      l_sec := 'Load the prior table data from current for comparison.';
      fnd_file.put_line(fnd_file.log, l_sec);
      fnd_file.put_line(fnd_file.output, l_sec);
    END;

    --Clear contents of the current table
    BEGIN
      l_sec := 'Truncate the current table to load data from Property Manager.';
      fnd_file.put_line(fnd_file.log, l_sec);
      fnd_file.put_line(fnd_file.output, l_sec);
      EXECUTE IMMEDIATE 'truncate table xxcus.xxcus_vrm_location_curr';

    EXCEPTION
      WHEN OTHERS THEN
        l_err_msg := 'Failed to Truncate xxcus.xxcus_vrm_location_curr : ' ||
                     SQLERRM;
        fnd_file.put_line(fnd_file.log, l_err_msg);
        RAISE program_error;
    END;

    --create LOB as a customer
    BEGIN
      --Loop to load table to map location code
      FOR c_load IN (SELECT DISTINCT fm.fru
                                    ,fm.lob_branch
                                    ,fm.fru_description
                                    ,fm.bu_description
                                    ,fm.pos_system
                                    ,fm.lob_name
                       FROM apps.xxcus_fruloc_master_vw fm
                      WHERE assignment_type <> 'PPL'
                      ORDER BY fm.fru, fm.lob_branch)
      LOOP

        INSERT INTO xxcus.xxcus_vrm_location_curr
          SELECT fruloc
                ,fru
                ,lob_branch
                ,fru_description
                ,bu_description
                ,pos_system
                ,ops_status
                ,lob_name
                ,loc
                ,location_id
                ,property_id
                ,location_code
                ,address_line1
                ,address_line2
                ,county
                ,city
                ,state
                ,province
                ,country
                ,zip_code
                ,address_id
                ,phone
                ,fax
                ,SYSDATE
                ,-1
                ,NULL
                ,operations_closed_date
            FROM apps.xxcus_fruloc_master_vw
           WHERE assignment_type <> 'PPL'
             AND fru = c_load.fru
             AND lob_branch = c_load.lob_branch
             AND lob_name = c_load.lob_name;

        COMMIT;

      END LOOP;

      --table loaded xxcus_vrm_location_curr

      --Create LOB as a Customer Party
      BEGIN
        BEGIN
          SELECT COUNT(*)
            INTO l_party
            FROM fnd_lookup_values_vl lv
           WHERE lookup_type = 'PN_PORTFOLIO_TYPE'
             AND enabled_flag = 'Y'
             AND lookup_code <> 'MLT'
             AND upper(meaning) NOT IN
                 (SELECT party_name
                    FROM ar.hz_parties hp
                   WHERE hp.party_name = upper(lv.meaning)
                     AND hp.attribute_category = l_us_org
                     AND hp.attribute1 = l_party_attr);

        EXCEPTION
          WHEN no_data_found THEN
            l_party := 0;
        END;

        IF l_party > 0
        THEN
          BEGIN
            FOR c_party IN (SELECT lookup_code lob, upper(meaning) lob_name
                              FROM fnd_lookup_values_vl lv
                             WHERE lookup_type = 'PN_PORTFOLIO_TYPE'
                               AND enabled_flag = 'Y'
                               AND lookup_code <> 'MLT'
                               AND upper(meaning) NOT IN
                                   (SELECT party_name
                                      FROM ar.hz_parties hp
                                     WHERE hp.party_name = upper(lv.meaning)
                                       AND hp.attribute_category = l_us_org
                                       AND hp.attribute1 = l_party_attr))
            LOOP

              p_organization_rec.organization_name := c_party.lob_name;
              p_organization_rec.attribute1        := l_party_attr;
              p_organization_rec.known_as          := c_party.lob;
              p_organization_rec.created_by_module := l_module;
              hz_party_v2pub.create_organization('T'
                                                ,p_organization_rec
                                                ,x_return_status
                                                ,x_msg_count
                                                ,x_msg_data
                                                ,x_party_id
                                                ,x_party_number
                                                ,x_profile_id);

              l_sec := 'LOB Name: ' || c_party.lob_name ||
                       ' Created Party Number: ' || x_party_number;
              fnd_file.put_line(fnd_file.log, l_sec);
              fnd_file.put_line(fnd_file.output, l_sec);

              IF x_msg_count > 1
              THEN
                FOR i IN 1 .. x_msg_count
                LOOP
                  l_sec := 'I.' || substr(fnd_msg_pub.get(p_encoded => fnd_api.g_false)
                                         ,1
                                         ,255);
                  fnd_file.put_line(fnd_file.log, l_sec);
                  fnd_file.put_line(fnd_file.output, l_sec);

                END LOOP;
              END IF;

              UPDATE ar.hz_parties
                 SET attribute1         = l_party_attr
                    ,category_code      = l_category_code
                    ,attribute_category = l_us_org
               WHERE party_name = c_party.lob_name
                 AND created_by_module = l_module
                 AND party_number = x_party_number;

              COMMIT;

            END LOOP;
          END;
        END IF;
      END;
    END;

    BEGIN
      l_sec := 'Starting loop for insert of the customer interface.';
      fnd_file.put_line(fnd_file.log, l_sec);
      fnd_file.put_line(fnd_file.output, l_sec);

      --For Loop for insert customer interface
      FOR c_interface IN (SELECT DISTINCT lc.fru
                                         ,lc.lob_branch
                                         ,lc.fru_descr
                                         ,decode(lc.inactive
                                                ,'N'
                                                ,'ACTIVE'
                                                ,'INACTIVE') close_status
                                         ,'ACTIVE' status
                                         ,lc.state
                                         ,lc.country
                                         ,flv.tag
                                         ,flv.description lob_name
                                         ,p.party_id
                                         ,lc.effdt
                            FROM apps.xxcus_location_code_vw lc
                                ,apps.fnd_lookup_values_vl   flv
                                ,ar.hz_parties               p
                           WHERE lc.business_unit = flv.lookup_code
                             AND flv.lookup_type = 'XXCUS_BUSINESS_UNIT'
                             AND flv.view_application_id = 3
                             AND flv.security_group_id = 0
                             AND flv.description = p.party_name
                             AND p.created_by_module = l_module
                             AND p.attribute_category = l_us_org
                             AND p.attribute1 = l_party_attr
                             AND lc.state IS NOT NULL
                             AND lc.lob_branch IS NOT NULL
                             AND flv.tag || '.' || lc.fru || '.' ||
                                 lc.lob_branch NOT IN
                                 (SELECT party_site_number
                                    FROM ar.hz_party_sites
                                   WHERE party_site_number =
                                         flv.tag || '.' || lc.fru || '.' ||
                                         lc.lob_branch
                                     AND party_id = p.party_id))
      LOOP

        IF c_interface.country = 'CAN'
        THEN
          l_org := l_cn_org;
        ELSE
          l_org := l_us_org;
        END IF;

        BEGIN
          SELECT vlc.address_line1
                ,vlc.county
                ,vlc.city
                ,nvl(vlc.state, vlc.province)
                ,vlc.country
            INTO l_address, l_county, l_city, l_state, l_country
            FROM xxcus.xxcus_vrm_location_curr vlc
           WHERE vlc.fru = c_interface.fru
             AND vlc.state = c_interface.state
             AND vlc.lob_branch = c_interface.lob_branch
             AND rownum = 1;

        EXCEPTION
          WHEN no_data_found THEN
            l_address := 'UNKNOWN';
            l_county  := 'UNKNOWN';
            l_city    := 'UNKNOWN';
            l_state   := c_interface.state;
            l_country := c_interface.country;

        END;

        INSERT INTO ar.ra_customers_interface_all
          (orig_system_customer_ref
          ,site_use_code
          ,orig_system_address_ref
          ,orig_system_party_ref
          ,customer_name
          ,customer_number
          ,customer_type
          ,location
          ,address1
          ,city
          ,state
          ,province
          ,county
          ,country
           --,customer_attribute_category
          ,customer_attribute1
          ,customer_attribute3
          ,primary_site_use_flag
          ,insert_update_flag
          ,last_updated_by
          ,last_update_date
          ,created_by
          ,creation_date
          ,org_id
          ,customer_status
          ,customer_category_code
          ,party_site_number)
        VALUES
          (c_interface.fru || '-' || c_interface.tag || '-' ||
           c_interface.lob_branch
          ,'BILL_TO'
          ,c_interface.fru || '-' || c_interface.tag || '-' ||
           c_interface.lob_branch
          ,c_interface.party_id
          ,c_interface.tag || '-' || c_interface.fru_descr || '-' ||
           c_interface.fru
          ,c_interface.tag || '.' || c_interface.fru || '.' ||
           c_interface.lob_branch
          ,'I'
          ,c_interface.tag || '.' || c_interface.fru || '.' ||
           c_interface.lob_branch
          ,l_address
          ,l_city
          ,CASE WHEN l_org = l_us_org THEN c_interface.state ELSE NULL END
          ,CASE WHEN l_org = l_cn_org THEN c_interface.state ELSE NULL END
          ,l_county
          ,decode(c_interface.country
                 ,'United States'
                 ,'US'
                 ,'USA'
                 ,'US'
                 ,'Canada'
                 ,'CA'
                 ,'CAN'
                 ,'CA')
           --,l_org
          ,l_cust_attr
          ,c_interface.fru_descr
          ,'Y'
          ,'I'
          ,l_user_id
          ,SYSDATE
          ,l_user_id
          ,SYSDATE
          ,l_org
          ,substr(c_interface.status, 1, 1)
          ,l_category_code
          ,c_interface.tag || '.' || c_interface.fru || '.' ||
           c_interface.lob_branch);

        INSERT INTO ra_customer_profiles_int_all
          (insert_update_flag
          ,orig_system_customer_ref
          ,customer_profile_class_name
          ,credit_hold
          ,last_updated_by
          ,last_update_date
          ,created_by
          ,creation_date
          ,org_id)
        VALUES
          ('I'
          ,c_interface.fru || '-' || c_interface.tag || '-' ||
           c_interface.lob_branch
          ,'DEFAULT'
          ,'N'
          ,l_user_id
          ,SYSDATE
          ,l_user_id
          ,SYSDATE
          ,l_org);

      END LOOP;
    END;

    --start customer import
    BEGIN

      --Version 1.3 04/23/2013 need to turn address validation to warning before creating or updating the customers
      l_level := 'WARNING';

      l_sec := 'Changing Address Validation to WARNING.';
      fnd_file.put_line(fnd_file.log
                       ,'Start time:  ' ||
                        to_char(SYSDATE, 'DD-MON-YYYY HH24:MI:SS') ||
                        ' for ' || l_sec);
      fnd_file.put_line(fnd_file.output
                       ,'Start time:  ' ||
                        to_char(SYSDATE, 'DD-MON-YYYY HH24:MI:SS') ||
                        ' for ' || l_sec);

      xxcus_ar_set_geo_val_level.submit_job(l_err_msg
                                           ,l_err_code
                                           ,l_level
                                           ,l_user
                                           ,l_responsibility_name);

      fnd_file.put_line(fnd_file.log
                       ,'End time:  ' ||
                        to_char(SYSDATE, 'DD-MON-YYYY HH24:MI:SS') ||
                        ' for ' || l_sec);
      fnd_file.put_line(fnd_file.output
                       ,'End time:  ' ||
                        to_char(SYSDATE, 'DD-MON-YYYY HH24:MI:SS') ||
                        ' for ' || l_sec);
      -- end 04/23/2013

      BEGIN
        SELECT COUNT(*)
          INTO l_count
          FROM ar.ra_customers_interface_all
         WHERE org_id IN (l_us_org, l_cn_org);

      EXCEPTION
        WHEN no_data_found THEN
          l_count := 0;

          l_sec := 'No Branches to load from the Customer Import, Record count:  ' ||
                   l_count;
          fnd_file.put_line(fnd_file.log, l_sec);
          fnd_file.put_line(fnd_file.output, l_sec);
      END;

      IF l_count >= 1
      THEN

        FOR c_org IN (SELECT DISTINCT org_id
                        FROM ar.ra_customers_interface_all
                       WHERE org_id IN (l_us_org, l_cn_org))
        LOOP

          -- Call Customer Inteface

          fnd_file.put_line(fnd_file.log, v_message);
          l_req_id := apps.fnd_request.submit_request('AR'
                                                     ,'RACUST'
                                                     ,NULL
                                                     ,NULL
                                                     ,FALSE
                                                     ,'N'
                                                     ,c_org.org_id);
          COMMIT;

          IF (l_req_id != 0)
          THEN
            IF fnd_concurrent.wait_for_request(l_req_id
                                              ,v_interval
                                              ,v_max_time
                                              ,v_phase
                                              ,v_status
                                              ,v_dev_phase
                                              ,v_dev_status
                                              ,v_message)
            THEN
              v_error_message := chr(10) || 'ReqID=' || l_req_id ||
                                 ' Org ID=' || c_org.org_id || ' DPhase ' ||
                                 v_dev_phase || ' DStatus ' || v_dev_status ||
                                 chr(10) || ' MSG - ' || v_message;
              -- Error Returned
              IF v_dev_phase != 'COMPLETE' OR v_dev_status != 'NORMAL'
              THEN
                l_sec := 'An error occured in the running Rebates Branch Customer Interface' ||
                         v_error_message || '.';
                fnd_file.put_line(fnd_file.log, l_sec);
                fnd_file.put_line(fnd_file.output, l_sec);
                RAISE program_error;

              END IF;
              -- Then Success!
              l_sec := 'Customer Import for Org ID:  ' || c_org.org_id ||
                       '  Request ID:  ' || l_req_id;

              fnd_file.put_line(fnd_file.log, l_sec);
              fnd_file.put_line(fnd_file.output, l_sec);

            ELSE
              l_sec := 'An error occured running Rebates Branch Customer Interface' ||
                       v_error_message || '.';
              fnd_file.put_line(fnd_file.log, l_sec);
              fnd_file.put_line(fnd_file.output, l_sec);
              RAISE program_error;
            END IF;

          ELSE
            l_sec := 'An error occured when trying to submitting Rebates Branch Customer Interface';
            fnd_file.put_line(fnd_file.log, l_sec);
            fnd_file.put_line(fnd_file.output, l_sec);
            RAISE program_error;
          END IF;
        END LOOP;
      END IF;

      --Version 1.3 04/23/2013 need to turn address validation to error before creating or updating the customers
      l_level := 'ERROR';

      l_sec := 'Changing Address Validation to ERROR.';
      fnd_file.put_line(fnd_file.log
                       ,'Start time:  ' ||
                        to_char(SYSDATE, 'DD-MON-YYYY HH24:MI:SS') ||
                        ' for ' || l_sec);
      fnd_file.put_line(fnd_file.output
                       ,'Start time:  ' ||
                        to_char(SYSDATE, 'DD-MON-YYYY HH24:MI:SS') ||
                        ' for ' || l_sec);

      xxcus_ar_set_geo_val_level.submit_job(l_err_msg
                                           ,l_err_code
                                           ,l_level
                                           ,l_user
                                           ,l_responsibility_name);

      fnd_file.put_line(fnd_file.log
                       ,'End time:  ' ||
                        to_char(SYSDATE, 'DD-MON-YYYY HH24:MI:SS') ||
                        ' for ' || l_sec);
      fnd_file.put_line(fnd_file.output
                       ,'End time:  ' ||
                        to_char(SYSDATE, 'DD-MON-YYYY HH24:MI:SS') ||
                        ' for ' || l_sec);
      -- end 04/23/2013
    END;
    --end customer import

    --FRU is inactive updating cust account with the effective date
    BEGIN
      l_sec := 'Starting loop for API update attribute2 for inactive FRU.';
      fnd_file.put_line(fnd_file.log, l_sec);
      fnd_file.put_line(fnd_file.output, l_sec);

      FOR c_update IN (SELECT lc.fru || '.' || lc.lob_branch fru_branch
                             ,lc.country
                             ,lc.effdt
                             ,ca.cust_account_id
                             ,ca.object_version_number
                             ,ca.orig_system_reference
                         FROM apps.xxcus_location_code_vw lc
                             ,apps.fnd_lookup_values_vl   flv
                             ,ar.hz_parties               p
                             ,ar.hz_cust_accounts         ca
                        WHERE lc.inactive = 'Y'
                          AND lc.business_unit = flv.lookup_code
                          AND flv.lookup_type = 'XXCUS_BUSINESS_UNIT'
                          AND flv.description = p.party_name
                          AND p.created_by_module = l_module
                          AND p.attribute_category = l_us_org
                          AND lc.state IS NOT NULL
                          AND lc.lob_branch IS NOT NULL
                          AND p.attribute1 = l_party_attr
                          AND flv.tag || '.' || lc.fru || '.' ||
                              lc.lob_branch = ca.account_number
                          AND p.party_id = ca.party_id
                          AND lc.effdt <> nvl(ca.attribute2, 'X'))

      LOOP

        IF c_update.country = 'CAN'
        THEN
          l_org := l_cn_org;
        ELSE
          l_org := l_us_org;
        END IF;

        --updating attribute2 with effective end date for FRU on the customer account

        x_return_status := NULL;
        x_msg_count     := NULL;
        x_msg_data      := NULL;

        mo_global.init('AR');
        mo_global.set_policy_context('M', l_org);

        p_cust_account_rec.cust_account_id := c_update.cust_account_id;
        p_cust_account_rec.attribute2      := c_update.effdt;
        p_object_version_number            := c_update.object_version_number;
        hz_cust_account_v2pub.update_cust_account('T'
                                                 ,p_cust_account_rec
                                                 ,p_object_version_number
                                                 ,x_return_status
                                                 ,x_msg_count
                                                 ,x_msg_data);

        l_sec := 'FRU inactive update attribute2 for Cust Account ID:  ' ||
                 c_update.cust_account_id || ' FRU / LOB Branch: ' ||
                 c_update.fru_branch || ' Status = ' || x_return_status;

        fnd_file.put_line(fnd_file.log, l_sec);
        fnd_file.put_line(fnd_file.output, l_sec);

      END LOOP;

      l_sec := 'Done with loop for API update attribute2 for inactive FRU.';
      fnd_file.put_line(fnd_file.log, l_sec);
      fnd_file.put_line(fnd_file.output, l_sec);

    END;

  EXCEPTION
    WHEN program_error THEN
      ROLLBACK;
      l_err_code := 2;
      l_err_msg  := l_err_msg || ' Error_Stack...' ||
                    dbms_utility.format_error_stack() ||
                    ' Error_Backtrace...' ||
                    dbms_utility.format_error_backtrace();
      fnd_file.put_line(fnd_file.log, l_err_msg);
      fnd_file.put_line(fnd_file.output, l_err_msg);
      retcode         := l_err_code;
      errbuf          := l_err_msg;
      l_err_callpoint := l_sec;

      xxcus_error_pkg.xxcus_error_main_api(p_called_from       => l_err_callfrom
                                          ,p_calling           => l_err_callpoint
                                          ,p_request_id        => l_req_id
                                          ,p_ora_error_msg     => substr(regexp_replace(l_err_msg
                                                                                       ,'[[:cntrl:]]'
                                                                                       ,NULL)
                                                                        ,1
                                                                        ,2000)
                                          ,p_error_desc        => substr(l_err_msg
                                                                        ,1
                                                                        ,240)
                                          ,p_distribution_list => l_distro_list
                                          ,p_module            => 'TM');

    WHEN OTHERS THEN
      fnd_file.put_line(fnd_file.log, l_sec);
      fnd_file.put_line(fnd_file.output, l_sec);
      l_err_code := 2;
      l_err_msg  := l_err_msg || ' Error_Stack...' ||
                    dbms_utility.format_error_stack() ||
                    ' Error_Backtrace...' ||
                    dbms_utility.format_error_backtrace();
      fnd_file.put_line(fnd_file.log, l_err_msg);
      fnd_file.put_line(fnd_file.output, l_err_msg);
      retcode         := l_err_code;
      errbuf          := l_err_msg;
      l_err_callpoint := l_sec;

      xxcus_error_pkg.xxcus_error_main_api(p_called_from       => l_err_callfrom
                                          ,p_calling           => l_err_callpoint
                                          ,p_request_id        => l_req_id
                                          ,p_ora_error_msg     => substr(regexp_replace(l_err_msg
                                                                                       ,'[[:cntrl:]]'
                                                                                       ,NULL)
                                                                        ,1
                                                                        ,2000)
                                          ,p_error_desc        => substr(l_err_msg
                                                                        ,1
                                                                        ,240)
                                          ,p_distribution_list => l_distro_list
                                          ,p_module            => 'TM');

  END load_branch_location;

  /*******************************************************************************
  * Procedure:   LOAD_REBATE_VENDOR
  * Description: This will create Rebate Vendors as a Customer with pay to sites
  *              being the LOB pay to vendors in Oracle. Will be using a MV from
  *              Sourcing DM
  HISTORY
  ===============================================================================
  VERSION DATE          AUTHOR(S)       DESCRIPTION
  ------- -----------   --------------- -----------------------------------------
  1.0     07/22/2010    Kathy Poling    Initial creation of the procedure
  1.1     08/30/2011    Kathy Poling    Changes to api's added commits and calling
                                        sequences instead of allowing api to generate
  1.2     05/21/2012    Kathy Poling    Remove truncate on interface tables for White Cap 20/20
                                        Profile change needed to change reponsibility
                                        to allow manual site numbering
  1.3     09/02/2012    Kathy Poling    Changes to pay to vendor and location
  1.4     11/29/2012    Kathy Poling    Changes to PayTo for moving to different MVID
  1.5     04/18/2013    Kathy Poling    Added lookup XXCUS_REBATE_BU_XREF for mapping
                                        SDW BU name and code to hr_business_unit that
                                        is used in the location code table and changing
                                        address validation
  1.6     07/15/2013    Kathy Poling    SR 207447 correcting issues with Party Site Use
                                        and Cust Site Use
  1.7     08/06/2013    Kathy Poling    Change to cursor c_party currency caused duplicate
                                        SR  207447
  2.8     06/29/2015   Bala Seshadri    ESMS 285756. Replace edw_common.edw_rebt_pt_vndr_mv@dw_edw_lnk with
                                        EA_APPS.MVID_APEX_REBT_PT_VNDR_V@EAAPXPRD and add outer join to attribute2 field
  ********************************************************************************/
  PROCEDURE load_rebate_vndr(errbuf OUT VARCHAR2, retcode OUT NUMBER) IS

    --
    -- Package Variables
    --
    l_req_id             NUMBER NULL;
    v_phase              VARCHAR2(50);
    v_status             VARCHAR2(50);
    v_dev_status         VARCHAR2(50);
    v_dev_phase          VARCHAR2(50);
    v_message            VARCHAR2(250);
    v_interval           NUMBER := 30; -- In seconds
    v_max_time           NUMBER := 15000; -- In seconds
    v_error_message      VARCHAR2(3000);
    l_can_submit_request BOOLEAN := TRUE;
    l_err_msg            VARCHAR2(3000);
    l_err_code           NUMBER;
    l_sec                VARCHAR2(500);
    l_statement          VARCHAR2(9000);
    l_count              NUMBER := 0;
    l_count_site         NUMBER := 0;
    l_site               VARCHAR2(3);
    l_primary_flag       VARCHAR2(1); --Version 1.5
    l_org                hr_all_organization_units.organization_id%TYPE;
    l_us_org CONSTANT hr_all_organization_units.organization_id%TYPE := 101; --HDS Rebates USA - Org
    l_cn_org CONSTANT hr_all_organization_units.organization_id%TYPE := 102; --HDS Rebates CAN - Org
    l_user   CONSTANT VARCHAR2(100) := 'REBTINTERFACE';
    l_responsibility      VARCHAR2(100) := 'XXCUS_CON';
    l_module              VARCHAR2(80) := 'XXCUS_INTERFACE';
    l_level               VARCHAR2(15); --Version 1.5 used for address validation
    l_responsibility_name VARCHAR2(100) := 'Receivables Manager'; --Version 1.5 used for address validation

    l_status    VARCHAR2(1);
    l_procedure VARCHAR2(50) := 'LOAD_REBATE_VNDR';
    l_mstr_attr CONSTANT ra_customers_interface_all.customer_attribute1%TYPE := 'HDS_MVID';
    l_user_id fnd_user.user_id%TYPE; --REBTINTERFACE user

    --API
    x_return_status VARCHAR2(2000);
    x_msg_count     NUMBER;
    x_msg_data      VARCHAR2(2000);

    --Party API
    p_organization_rec            hz_party_v2pub.organization_rec_type;
    p_party_object_version_number NUMBER;
    x_profile_id                  NUMBER;

    --Location API
    p_location_rec          hz_location_v2pub.location_rec_type;
    x_location_id           NUMBER;
    p_object_version_number NUMBER;

    --Party Site API
    p_party_site_rec    hz_party_site_v2pub.party_site_rec_type;
    x_party_site_id     NUMBER;
    x_party_site_number VARCHAR2(2000);

    --Party Site Use API
    p_party_site_use_rec hz_party_site_v2pub.party_site_use_rec_type;
    x_party_site_use_id  NUMBER;

    --Customer Account
    p_cust_account_rec hz_cust_account_v2pub.cust_account_rec_type;

    --Customer Account Site
    p_cust_acct_site_rec hz_cust_account_site_v2pub.cust_acct_site_rec_type;
    x_cust_acct_site_id  NUMBER;

    --Customer Account Site Use
    p_cust_site_use_rec    hz_cust_account_site_v2pub.cust_site_use_rec_type;
    p_customer_profile_rec hz_customer_profile_v2pub.customer_profile_rec_type;
    x_site_use_id          NUMBER;

    -- Error DEBUG
    l_err_callfrom  VARCHAR2(75) DEFAULT 'XXCUS_TM_INTERFACE_PKG.LOAD_REBATE_VNDR';
    l_err_callpoint VARCHAR2(75) DEFAULT 'START';
    l_distro_list   VARCHAR2(75) DEFAULT 'HDSOracleDevelopers@hdsupply.com';

  BEGIN

    BEGIN
      SELECT user_id INTO l_user_id FROM fnd_user WHERE user_name = l_user;
    EXCEPTION
      WHEN OTHERS THEN
        l_sec := 'User ' || l_user || ' not in fnd_user';
        fnd_file.put_line(fnd_file.log, l_sec);
        fnd_file.put_line(fnd_file.output, l_sec);
        RAISE program_error;
    END;

    --  Setup parameters for running FND JOBS!
    l_can_submit_request := xxcus_misc_pkg.set_responsibility(l_user
                                                             ,l_responsibility);
    IF l_can_submit_request
    THEN
      l_sec := 'Global Variables are set.';

    ELSE
      l_sec := 'Global Variables are not set for the Responsibility of ' ||
               l_user || ' and the User.';
      fnd_file.put_line(fnd_file.log, l_sec);
      fnd_file.put_line(fnd_file.output, l_sec);
      RAISE program_error;
    END IF;

    fnd_file.put_line(fnd_file.log, l_sec);
    fnd_file.put_line(fnd_file.output, l_sec);
    --  END OF Setup parameters for running FND JOBS!

    --  Check to see if data has been loaded from DW

    SELECT COUNT(*)
      INTO l_count
      FROM EA_APPS.MVID_APEX_REBT_PT_VNDR_V@EAAPXPRD --edw_common.edw_rebt_pt_vndr_mv@dw_edw_lnk --Ver 2.8
     WHERE rebate_flg = 'R';

    l_sec := 'Vendor data loaded from SDW - Count:  ' || l_count;
    fnd_file.put_line(fnd_file.log, l_sec);
    fnd_file.put_line(fnd_file.output, l_sec);

    IF l_count < 100
    THEN
      l_sec := 'Vendor data has not been loaded from SDW';
      fnd_file.put_line(fnd_file.log, l_sec);
      fnd_file.put_line(fnd_file.output, l_sec);

      RAISE program_error;
    END IF;

    INSERT INTO xxcus.xxcus_rebate_aud_tbl
    VALUES
      (fnd_global.conc_request_id()
      ,l_procedure
      ,SYSDATE
      ,nvl(fnd_global.user_name, 'NA')
      ,nvl(fnd_global.resp_name, 'NA')
      ,NULL
      ,0
      ,'Beginning Program - vendor load'
      ,SYSDATE
      ,l_count
      ,NULL
      ,NULL);

    COMMIT;

    BEGIN
      l_sec := 'Truncate the history vendor stagging table to load monthly data.';
      fnd_file.put_line(fnd_file.log, l_sec);
      fnd_file.put_line(fnd_file.output, l_sec);
      EXECUTE IMMEDIATE 'truncate table xxcus.xxcus_rebate_vndr_history_tbl';
    EXCEPTION
      WHEN OTHERS THEN
        l_err_msg := 'Failed to Truncate xxcus_rebate_vndr_history_tbl : ' ||
                     SQLERRM;
        fnd_file.put_line(fnd_file.log, l_err_msg);
        RAISE program_error;
    END;
    --Load table to compare data

    BEGIN
      INSERT /*+ APPEND */
      INTO xxcus.xxcus_rebate_vndr_history_tbl
        SELECT * FROM xxcus.xxcus_rebate_vndr_curr_tbl;

      COMMIT;
    END;

    BEGIN
      l_sec := 'Truncate the current vendor stagging table to load monthly data.';
      fnd_file.put_line(fnd_file.log, l_sec);
      fnd_file.put_line(fnd_file.output, l_sec);
      EXECUTE IMMEDIATE 'truncate table xxcus.xxcus_rebate_vndr_curr_tbl';
    EXCEPTION
      WHEN OTHERS THEN
        l_err_msg := 'Failed to Truncate xxcus_rebate_vndr_curr_tbl : ' ||
                     SQLERRM;
        fnd_file.put_line(fnd_file.log, l_err_msg);
        RAISE program_error;
    END;

    --Clear contents of interface table
    l_sec := 'Deleting data from customer and profiles interface for Rebates ORG 101 and 102.';
    DELETE FROM ar.ra_customers_interface_all
     WHERE org_id IN (l_us_org, l_cn_org); --Version 1.1
    DELETE FROM ar.ra_customer_profiles_int_all
     WHERE org_id IN (l_us_org, l_cn_org); --Version 1.1

    fnd_file.put_line(fnd_file.log, l_sec);
    fnd_file.put_line(fnd_file.output, l_sec);

    l_sec := 'Loading vendor curr table from SDW';

    fnd_file.put_line(fnd_file.output
                     ,'Start time:  ' ||
                      to_char(SYSDATE, 'DD-MON-YYYY HH24:MI:SS') ||
                      ' for ' || l_sec);
    fnd_file.put_line(fnd_file.log
                     ,'Start time:  ' ||
                      to_char(SYSDATE, 'DD-MON-YYYY HH24:MI:SS') ||
                      ' for ' || l_sec);

    INSERT /*+ APPEND */
    INTO xxcus.xxcus_rebate_vndr_curr_tbl
      (SELECT pt_vndr_id
             ,pt_vndr_cd
             ,org_hier_id
             ,flv.attribute3 --Version 1.5
             ,flv.description --Version 1.5
             ,src_sys_nm
             ,pt_vndr_mail_addr_nm
             ,oracle_vndr_cd
             ,pt_vndr_tax_id_nbr
             ,pt_ultmt_duns
             ,pt_hq_duns
             ,pt_duns
             ,pt_mail_addr_ln1
             ,pt_mail_addr_ln2
             ,pt_mail_city
             ,pt_mail_st_prov
             ,pt_mail_pstl_cd
             ,pt_mail_fax
             ,pt_mail_cntry
             ,pt_mail_phn_nbr
             ,pt_phy_addr_nm
             ,pt_phy_addr_ln1
             ,pt_phy_addr_ln2
             ,pt_phy_city
             ,pt_phy_st_prov
             ,pt_phy_pstl_cd
             ,pt_phy_fax
             ,pt_phy_cntry
             ,pt_phy_phn_nbr
             ,pt_pay_trm_disc_pct
             ,pt_pay_days
             ,pt_pay_trm_desc
             ,rebt_vndr_id
             ,rebt_vndr_cd
             ,rebt_vndr_nm
             ,rebate_flg
             ,geo_loc_hier_lob_nm
             ,curnc_flg
             ,as_of_dt
             ,NULL
             ,'N'
             ,NULL
         FROM EA_APPS.MVID_APEX_REBT_PT_VNDR_V@EAAPXPRD --edw_common.edw_rebt_pt_vndr_mv@dw_edw_lnk --Ver 2.8
             ,apps.fnd_lookup_values flv
        WHERE flv.lookup_type(+) = 'XXCUS_REBATE_BU_XREF' --Ver 2.8
          AND flv.enabled_flag(+) = 'Y' --Ver 2.8
          AND nvl(flv.end_date_active(+), SYSDATE) >= SYSDATE --Ver 2.8
          AND flv.meaning(+) = bu_nm --Ver 2.8
          AND flv.attribute2(+) = bu_cd --Ver 2.8
          /*  --Ver 2.8 Begin
          AND (bu_nm, pt_vndr_id) IN
              (SELECT bu_nm, pt_vndr_id
                 FROM edw_common.edw_rebt_pt_vndr_mv@dw_edw_lnk
               HAVING COUNT(*) = 1
                GROUP BY bu_nm, pt_vndr_id)
          */  --Ver 2.8 End
                );

    COMMIT;

    fnd_file.put_line(fnd_file.log
                     ,'End time:  ' ||
                      to_char(SYSDATE, 'DD-MON-YYYY HH24:MI:SS') ||
                      ' for ' || l_sec);

    fnd_file.put_line(fnd_file.output
                     ,'End time:  ' ||
                      to_char(SYSDATE, 'DD-MON-YYYY HH24:MI:SS') ||
                      ' for ' || l_sec);

    UPDATE xxcus.xxcus_rebate_aud_tbl
       SET stage = l_sec, stg_date = SYSDATE --, processed_datetime = sysdate
     WHERE conc_request_id = nvl(fnd_global.conc_request_id, 0)
       AND procedure_name = l_procedure
       AND trunc(processed_datetime) = trunc(SYSDATE);

    COMMIT;

    --Version 1.5 04/23/2013 need to turn address validation to warning before creating or updating the customers
    l_level := 'WARNING';

    l_sec := 'Changing Address Validation to WARNING.';
    fnd_file.put_line(fnd_file.log
                     ,'Start time:  ' ||
                      to_char(SYSDATE, 'DD-MON-YYYY HH24:MI:SS') ||
                      ' for ' || l_sec);
    fnd_file.put_line(fnd_file.output
                     ,'Start time:  ' ||
                      to_char(SYSDATE, 'DD-MON-YYYY HH24:MI:SS') ||
                      ' for ' || l_sec);

    xxcus_ar_set_geo_val_level.submit_job(l_err_msg
                                         ,l_err_code
                                         ,l_level
                                         ,l_user
                                         ,l_responsibility_name);

    fnd_file.put_line(fnd_file.log
                     ,'End time:  ' ||
                      to_char(SYSDATE, 'DD-MON-YYYY HH24:MI:SS') ||
                      ' for ' || l_sec);
    fnd_file.put_line(fnd_file.output
                     ,'End time:  ' ||
                      to_char(SYSDATE, 'DD-MON-YYYY HH24:MI:SS') ||
                      ' for ' || l_sec);
    -- end 04/23/2013

    --starting process for MVID or PayTo status or name change
    BEGIN

      l_sec := 'Changing Status on Rebate Customer or PayTo.';
      fnd_file.put_line(fnd_file.log
                       ,'Start time:  ' ||
                        to_char(SYSDATE, 'DD-MON-YYYY HH24:MI:SS') ||
                        ' for ' || l_sec);
      fnd_file.put_line(fnd_file.output
                       ,'Start time:  ' ||
                        to_char(SYSDATE, 'DD-MON-YYYY HH24:MI:SS') ||
                        ' for ' || l_sec);

      --Rebate vendor no longer in feed from DW or setup in AR as inactive or name change
      FOR c_party IN ( --MVID not in feed from DW will set Oracle as inactive
                      SELECT NULL rebt_vndr_cd
                             ,NULL rebt_vndr_nm
                             ,ca.cust_account_id
                             ,ca.object_version_number
                             ,'I' status
                             ,ps.party_id
                             ,ps.party_site_id
                             ,ps.object_version_number site_version
                             ,ps.party_site_number
                             ,p.party_name
                             ,NULL party_site_use_id
                             ,NULL partysiteuse_ver
                             ,NULL cust_acct_site_id
                             ,NULL custsite_org
                             ,NULL site_use_id
                             ,NULL custsiteuse_ver
                             ,NULL curnc_flg
                        FROM hz_parties          p
                             ,hz_party_sites      ps
                             ,ar.hz_cust_accounts ca
                       WHERE p.attribute1 = l_mstr_attr
                         AND p.party_id = ps.party_id
                         AND ps.status = 'A'
                         AND ps.party_id = ca.party_id
                         AND ca.status = 'A'
                         AND ca.attribute2 IN
                             (SELECT attribute2
                                FROM ar.hz_cust_accounts hca
                               WHERE hca.attribute1 = l_mstr_attr
                                 AND hca.party_id = ps.party_id
                                 AND hca.attribute2 || '~MSTR' =
                                     ps.party_site_number
                                 AND hca.party_id = p.party_id
                              MINUS
                              SELECT DISTINCT rebt_vndr_cd
                                FROM xxcus.xxcus_rebate_vndr_curr_tbl)
                      UNION
                      --MVID in feed from DW but inactive in Oracle will change status to Active in Oracle
                      SELECT DISTINCT rebt_vndr_cd
                                     ,REPLACE(REPLACE(REPLACE(rebt_vndr_nm
                                                             ,'.')
                                                     ,',')
                                             ,'-') rebt_vndr_nm
                                     ,ca.cust_account_id
                                     ,ca.object_version_number
                                     ,'A' status
                                     ,ps.party_id
                                     ,ps.party_site_id
                                     ,ps.object_version_number site_version
                                     ,ps.party_site_number
                                     ,p.party_name
                                     ,hpsu.party_site_use_id
                                     ,hpsu.object_version_number partysiteuse_ver
                                     ,NULL cust_acct_site_id
                                     ,NULL custsite_org
                                     ,NULL site_use_id
                                     ,NULL custsiteuse_ver
                                     ,NULL curnc_flg --Version 1.7 8/6/13
                        FROM xxcus.xxcus_rebate_vndr_curr_tbl v
                            ,hz_parties                       p
                            ,ar.hz_cust_accounts              ca
                            ,hz_party_sites                   ps
                            ,ar.hz_party_site_uses            hpsu
                      --,hz_cust_acct_sites_all           hcas
                       WHERE v.rebt_vndr_cd || '~MSTR' = p.party_number
                         AND p.attribute1 = l_mstr_attr
                         AND p.party_id = ca.party_id
                         AND ca.status = 'I'
                         AND ca.attribute2 IN
                             (SELECT DISTINCT rebt_vndr_cd
                                FROM xxcus.xxcus_rebate_vndr_curr_tbl)
                         AND ca.party_id = ps.party_id
                         AND v.rebt_vndr_cd || '~MSTR' = ps.party_site_number
                         AND ps.party_site_id = hpsu.party_site_id
                         AND hpsu.site_use_type = 'BILL_TO'
                      --AND ps.party_site_id = hcas.party_site_id
                      UNION
                      --MVID name changed in feed from DW will change name in Oracle
                      SELECT DISTINCT rebt_vndr_cd
                                     ,REPLACE(REPLACE(REPLACE(rebt_vndr_nm
                                                             ,'.')
                                                     ,',')
                                             ,'-') rebt_vndr_nm
                                     ,NULL
                                     ,p.object_version_number
                                     ,'C' status
                                     ,p.party_id
                                     ,NULL
                                     ,NULL
                                     ,NULL party_site_number
                                     ,party_name
                                     ,NULL party_site_use_id
                                     ,NULL partysiteuse_ver
                                     ,NULL cust_acct_site_id
                                     ,NULL custsite_org
                                     ,NULL site_use_id
                                     ,NULL custsiteuse_ver
                                     ,NULL curnc_flg --Version 1.7 8/6/13
                        FROM xxcus.xxcus_rebate_vndr_curr_tbl v, hz_parties p
                       WHERE v.rebt_vndr_cd || '~MSTR' = p.party_number
                         AND p.attribute1 = l_mstr_attr
                         AND p.party_name <>
                             REPLACE(REPLACE(REPLACE(rebt_vndr_nm, '.'), ',')
                                    ,'-')
                      UNION
                      --PayTo vendor no longer in feed from DW for MVID will set as inactive in Oracle
                      SELECT NULL rebt_vndr_cd
                            ,NULL rebt_vndr_nm
                            ,NULL cust_account_id
                            ,NULL object_version_number
                            ,'PSI' status
                            ,ps.party_id
                            ,ps.party_site_id
                            ,ps.object_version_number site_version
                            ,ps.party_site_number
                            ,p.party_name
                            ,NULL party_site_use_id
                            ,NULL partysiteuse_ver
                            ,NULL cust_acct_site_id
                            ,NULL custsite_org
                            ,NULL site_use_id
                            ,NULL custsiteuse_ver
                            ,NULL curnc_flg
                        FROM hz_party_sites ps, hz_parties p
                       WHERE p.attribute1 = l_mstr_attr
                         AND ps.party_id = p.party_id
                         AND substr(ps.orig_system_reference, 1, 8) =
                             'PT VNDR~'
                         AND ps.status = 'A'
                         AND (p.attribute2,
                              nvl(substr(party_site_number
                                        ,1
                                        ,instr(party_site_number, '|') - 1)
                                 ,party_site_number)) NOT IN
                             (SELECT rebt_vndr_cd, pt_vndr_cd || '~' || bu_cd
                                FROM xxcus.xxcus_rebate_vndr_curr_tbl
                               WHERE pt_vndr_cd || '~' || bu_cd =
                                     nvl(substr(party_site_number
                                               ,1
                                               ,instr(party_site_number, '|') - 1)
                                        ,party_site_number)
                                 AND rebt_vndr_cd = p.attribute2)
                      UNION
                      --PayTo inactive in Oracle and in feed from DW for that MVID will change status to Active in Oracle
                      SELECT ps.attribute4 rebt_vndr_cd
                            ,NULL rebt_vndr_nm
                            ,NULL cust_account_id
                            ,ps.object_version_number
                            ,'PSA' status
                            ,ps.party_id
                            ,ps.party_site_id
                            ,ps.object_version_number site_version
                            ,ps.party_site_number
                            ,p.party_name
                            ,hpsu.party_site_use_id
                            ,hpsu.object_version_number partysiteuse_ver
                            ,hcas.cust_acct_site_id
                            ,hcas.org_id custsite_org
                            ,hcsu.site_use_id
                            ,hcsu.object_version_number custsiteuse_ver
                            ,NULL curnc_flg
                        FROM hz_party_sites         ps
                            ,hz_parties             p
                            ,ar.hz_party_site_uses  hpsu
                            ,hz_cust_acct_sites_all hcas
                            ,hz_cust_site_uses_all  hcsu
                       WHERE p.attribute1 = l_mstr_attr
                         AND ps.party_id = p.party_id
                         AND ps.created_by_module = l_module
                         AND substr(ps.orig_system_reference, 1, 8) =
                             'PT VNDR~'
                         AND ps.status = 'I'
                         AND ps.party_site_id = hpsu.party_site_id
                         AND hpsu.site_use_type = 'BILL_TO'
                         AND ps.party_site_id = hcas.party_site_id
                         AND hcas.cust_acct_site_id = hcsu.cust_acct_site_id
                         AND hcsu.site_use_code = 'BILL_TO'
                         AND (p.attribute2,
                              nvl(substr(party_site_number
                                        ,1
                                        ,instr(party_site_number, '|.') - 1)
                                 ,party_site_number)) IN
                             (SELECT rebt_vndr_cd, pt_vndr_cd || '~' || bu_cd
                                FROM xxcus.xxcus_rebate_vndr_curr_tbl
                               WHERE pt_vndr_cd || '~' || bu_cd =
                                     nvl(substr(party_site_number
                                               ,1
                                               ,instr(party_site_number, '|') - 1)
                                        ,party_site_number)
                                 AND rebt_vndr_cd = p.attribute2)
                       ORDER BY status)
      LOOP

        x_return_status      := NULL;
        x_msg_count          := NULL;
        x_msg_data           := NULL;
        p_cust_account_rec   := NULL;
        p_party_site_rec     := NULL;
        p_cust_acct_site_rec := NULL;
        p_party_site_use_rec := NULL;
        p_organization_rec   := NULL;

        IF c_party.status = 'PSI'
        THEN
          l_status := 'I';
        ELSE
          l_status := 'A';
        END IF;

        --7/11/13
        IF nvl(c_party.curnc_flg, 'USD') = 'USD'
        THEN
          l_org := l_us_org;
        ELSE
          l_org := l_cn_org;
        END IF;
        --7/11/13

        IF c_party.status IN ('A', 'I')
        THEN

          --changing status for MVID customer account

          x_return_status := NULL;
          x_msg_count     := NULL;
          x_msg_data      := NULL;

          mo_global.init('AR');
          mo_global.set_policy_context('M', 101);
          p_cust_account_rec.cust_account_id := c_party.cust_account_id;
          p_cust_account_rec.status          := c_party.status;
          p_object_version_number            := c_party.object_version_number;
          --p_organization_rec.created_by_module := l_module;
          hz_cust_account_v2pub.update_cust_account('T'
                                                   ,p_cust_account_rec
                                                   ,p_object_version_number
                                                   ,x_return_status
                                                   ,x_msg_count
                                                   ,x_msg_data);

          l_sec := 'MVID status on Cust Account ID:  ' ||
                   c_party.cust_account_id || ' Status = ' ||
                   x_return_status;

          IF x_return_status = 'S'
          THEN
            fnd_file.put_line(fnd_file.output, l_sec);

            ----changing status for MVID party site
            x_return_status := NULL;
            x_msg_count     := NULL;
            x_msg_data      := NULL;

            --p_party_site_rec.party_id          := c_party.party_id;       --Version 1.1
            p_party_site_rec.party_site_id := c_party.party_site_id;
            p_party_site_rec.status        := c_party.status;
            p_object_version_number        := c_party.site_version;
            --p_party_site_rec.created_by_module := l_module;

            hz_party_site_v2pub.update_party_site('T'
                                                 ,p_party_site_rec
                                                 ,p_object_version_number
                                                 ,x_return_status
                                                 ,x_msg_count
                                                 ,x_msg_data);

            l_sec := 'MVID status on party sites ID:  ' ||
                     c_party.party_site_id || ' Status = ' ||
                     x_return_status;

            IF x_return_status = 'S'
            THEN
              fnd_file.put_line(fnd_file.output, l_sec);

              --Version  1.6
              IF c_party.status = 'A'
              THEN

                update_site_use(c_party.party_site_use_id
                               ,l_status
                               ,c_party.party_site_id
                               ,c_party.party_site_number);

              END IF;
              --
            ELSE
              l_sec := 'ERROR Rebate Vendor status on Site ID:  ' ||
                       c_party.party_site_id || ' Message:  ' || x_msg_data ||
                       '  ERROR Rebate Vndr status on party ID:  ' ||
                       c_party.party_site_id || ' Message:  ' || x_msg_data;

              fnd_file.put_line(fnd_file.log, l_sec);
              fnd_file.put_line(fnd_file.output, l_sec);
            END IF;

          ELSE
            l_sec := 'ERROR Rebate Vendor status on Cust Account ID:  ' ||
                     c_party.cust_account_id || ' Message:  ' || x_msg_data ||
                     '  ERROR Rebate Vndr status on party sites ID:  ' ||
                     c_party.party_id || ' Message:  ' || x_msg_data;

            fnd_file.put_line(fnd_file.log, l_sec);
            fnd_file.put_line(fnd_file.output, l_sec);
          END IF;

        END IF;

        --update party site for PayTo to active/inactivate
        IF c_party.status IN ('PSI', 'PSA')
        THEN

          x_return_status := NULL;
          x_msg_count     := NULL;
          x_msg_data      := NULL;

          p_party_site_rec.party_site_id := c_party.party_site_id;
          p_party_site_rec.status        := l_status;
          p_object_version_number        := c_party.site_version;
          --p_party_site_rec.created_by_module := l_module;

          hz_party_site_v2pub.update_party_site('T'
                                               ,p_party_site_rec
                                               ,p_object_version_number
                                               ,x_return_status
                                               ,x_msg_count
                                               ,x_msg_data);

          IF x_return_status = 'S'
          THEN

            l_sec := 'Update PayTo status on party sites ID:  ' ||
                     c_party.party_site_id || ' Status = ' ||
                     x_return_status;

            fnd_file.put_line(fnd_file.output, l_sec);

            --Version  1.6
            IF c_party.status = 'PSA'
            THEN

              update_site_use(c_party.party_site_use_id
                             ,l_status
                             ,c_party.party_site_id
                             ,c_party.party_site_number);

            END IF;
            --
          ELSE
            l_sec := 'ERROR PayTo Status update - Party Site id:  ' ||
                     c_party.party_site_id || ' Status = ' ||
                     x_return_status || ' Reason = ' || x_msg_data;
            fnd_file.put_line(fnd_file.log, l_sec);
            fnd_file.put_line(fnd_file.output, l_sec);
          END IF;
        END IF;

        --update party name for MVID
        IF c_party.status = 'C' AND
           c_party.rebt_vndr_nm <> c_party.party_name
        THEN

          x_return_status := NULL;
          x_msg_count     := NULL;
          x_msg_data      := NULL;

          p_organization_rec.organization_name  := c_party.rebt_vndr_nm;
          p_organization_rec.party_rec.party_id := c_party.party_id;
          p_party_object_version_number         := c_party.object_version_number;
          hz_party_v2pub.update_organization('T'
                                            ,p_organization_rec
                                            ,p_party_object_version_number
                                            ,x_profile_id
                                            ,x_return_status
                                            ,x_msg_count
                                            ,x_msg_data);

          IF x_return_status = 'S'
          THEN
            l_sec := 'Party name updated Rebate Vndr:  ' ||
                     c_party.rebt_vndr_nm || ' Status = ' ||
                     x_return_status;

            fnd_file.put_line(fnd_file.output, l_sec);

            UPDATE xxcus.xxcus_rebate_vndr_curr_tbl
               SET request_id   = nvl(fnd_global.conc_request_id, 0)
                  ,status_flag  = 'A'
                  ,process_date = SYSDATE
             WHERE rebt_vndr_cd = c_party.rebt_vndr_cd;

          ELSE
            l_sec := 'ERROR Party Name change - Rebate Vndr:  ' ||
                     c_party.rebt_vndr_nm || ' Status = ' ||
                     x_return_status || ' Reason = ' || x_msg_data;
            fnd_file.put_line(fnd_file.log, l_sec);
            fnd_file.put_line(fnd_file.output, l_sec);
          END IF;

        END IF;

        --status flag 'A' MVID or PayTo inactive/active or name chage
        UPDATE xxcus.xxcus_rebate_vndr_curr_tbl
           SET request_id   = nvl(fnd_global.conc_request_id, 0)
              ,status_flag  = 'A'
              ,process_date = SYSDATE
         WHERE rebt_vndr_cd = c_party.rebt_vndr_cd;

      END LOOP;

      fnd_file.put_line(fnd_file.log
                       ,'End time:  ' ||
                        to_char(SYSDATE, 'DD-MON-YYYY HH24:MI:SS') ||
                        ' for ' || l_sec);
      fnd_file.put_line(fnd_file.output
                       ,'End time:  ' ||
                        to_char(SYSDATE, 'DD-MON-YYYY HH24:MI:SS') ||
                        ' for ' || l_sec);

    END;

    --start loading new MVID's
    BEGIN
      l_sec := 'Rebate vendor "MVID": customer import Started.';
      fnd_file.put_line(fnd_file.log
                       ,'Start time:  ' ||
                        to_char(SYSDATE, 'DD-MON-YYYY HH24:MI:SS') ||
                        ' for ' || l_sec);
      fnd_file.put_line(fnd_file.output
                       ,'Start time:  ' ||
                        to_char(SYSDATE, 'DD-MON-YYYY HH24:MI:SS') ||
                        ' for ' || l_sec);

      UPDATE xxcus.xxcus_rebate_aud_tbl
         SET stage = l_sec, stg_date = SYSDATE --, processed_datetime = sysdate
       WHERE conc_request_id = nvl(fnd_global.conc_request_id, 0)
         AND procedure_name = l_procedure
         AND trunc(processed_datetime) = trunc(SYSDATE);

      COMMIT;

      --create master vendor as a customer using the customer interface
      BEGIN

        FOR c_mstr IN (SELECT DISTINCT rebt_vndr_cd
                                      ,REPLACE(REPLACE(REPLACE(rebt_vndr_nm
                                                              ,'.')
                                                      ,',')
                                              ,'-') rebt_vndr_nm
                                      ,curnc_flg
                         FROM xxcus.xxcus_rebate_vndr_curr_tbl
                        WHERE nvl(status_flag, 'N') = 'N'
                          AND nvl(bu_nm, 'Z') <> 'UNKNOWN' --Ver 2.8
                          AND rebt_vndr_cd || '~MSTR' NOT IN
                              (SELECT party_number
                                 FROM ar.hz_parties
                                WHERE attribute1 = l_mstr_attr))
        LOOP

          IF c_mstr.curnc_flg = 'USD'
          THEN
            l_org := l_us_org;
          ELSE
            l_org := l_cn_org;
          END IF;

          INSERT INTO ar.ra_customers_interface_all
            (orig_system_customer_ref
            ,site_use_code
            ,orig_system_address_ref
            ,customer_name
            ,customer_number
            ,customer_type
            ,location
            ,address1
            ,city
            ,state
            ,county
            ,postal_code
            ,country
            ,customer_attribute1
            ,customer_attribute2
            ,primary_site_use_flag
            ,party_number
            ,insert_update_flag
            ,last_updated_by
            ,last_update_date
            ,created_by
            ,creation_date
            ,org_id
            ,customer_status
            ,customer_category_code
            ,party_site_number)
          VALUES
            ('MSTR~' || c_mstr.rebt_vndr_cd
            ,'BILL_TO'
            ,'MSTR~' || c_mstr.rebt_vndr_cd || '~' || 'ORL'
            ,c_mstr.rebt_vndr_nm
            ,c_mstr.rebt_vndr_cd || '~MSTR'
            ,'R'
            ,c_mstr.rebt_vndr_cd || '~MSTR'
            ,'501 W CHURCH ST'
            ,'ORLANDO'
            ,'FL'
            ,'ORANGE'
            ,'32805'
            ,'US'
            ,l_mstr_attr
            ,c_mstr.rebt_vndr_cd
            ,'Y'
            ,c_mstr.rebt_vndr_cd || '~MSTR'
            ,'I'
            ,l_user_id
            ,SYSDATE
            ,l_user_id
            ,SYSDATE
            ,l_org
            ,'A'
            ,'MASTER_VENDOR'
            ,c_mstr.rebt_vndr_cd || '~MSTR');

          INSERT INTO ra_customer_profiles_int_all
            (insert_update_flag
            ,orig_system_customer_ref
            ,customer_profile_class_name
            ,credit_hold
            ,last_updated_by
            ,last_update_date
            ,created_by
            ,creation_date
            ,org_id)
          VALUES
            ('I'
            ,'MSTR~' || c_mstr.rebt_vndr_cd
            ,'DEFAULT'
            ,'N'
            ,l_user_id
            ,SYSDATE
            ,l_user_id
            ,SYSDATE
            ,l_org);

        END LOOP;
        COMMIT;
      END;

      --start customer import
      BEGIN

        l_count := 0;

        SELECT COUNT(*)
          INTO l_count
          FROM ar.ra_customers_interface_all
         WHERE org_id IN (l_us_org, l_cn_org);

        IF l_count >= 1
        THEN

          l_sec := 'Data loaded for MVID into Customer Interface-ready to run import.';
          fnd_file.put_line(fnd_file.log, l_sec);
          fnd_file.put_line(fnd_file.output, l_sec);

          --          v_message := 'Running Customer Interface';
          --          l_message := v_message;

          FOR c_interface IN (SELECT DISTINCT org_id
                                FROM ar.ra_customers_interface_all
                               WHERE org_id IN (l_us_org, l_cn_org))
          LOOP

            -- Call Customer Inteface

            fnd_file.put_line(fnd_file.log, v_message);
            l_req_id := apps.fnd_request.submit_request('AR'
                                                       ,'RACUST'
                                                       ,NULL
                                                       ,NULL
                                                       ,FALSE
                                                       ,'N'
                                                       ,c_interface.org_id);
            COMMIT;

          END LOOP;

          IF (l_req_id != 0)
          THEN
            IF fnd_concurrent.wait_for_request(l_req_id
                                              ,v_interval
                                              ,v_max_time
                                              ,v_phase
                                              ,v_status
                                              ,v_dev_phase
                                              ,v_dev_status
                                              ,v_message)
            THEN
              v_error_message := chr(10) || 'ReqID=' || l_req_id ||
                                 ' DPhase ' || v_dev_phase || ' DStatus ' ||
                                 v_dev_status || chr(10) || ' MSG - ' ||
                                 v_message;
              -- Error Returned
              IF v_dev_phase != 'COMPLETE' OR v_dev_status != 'NORMAL'
              THEN
                l_statement := 'An error occured in the running of the XXCUS_TM_INTERFACE_PKG Customer Interface' ||
                               v_error_message || '.';
                fnd_file.put_line(fnd_file.log, l_statement);
                fnd_file.put_line(fnd_file.output, l_statement);
                RAISE program_error;

              END IF;
              -- Then Success!
            ELSE
              l_statement := 'An error occured running the XXCUS_TM_INTERFACE_PKG Customer Interface' ||
                             v_error_message || '.';
              fnd_file.put_line(fnd_file.log, l_statement);
              fnd_file.put_line(fnd_file.output, l_statement);
              RAISE program_error;
            END IF;

          ELSE
            l_statement := 'An error occured when trying to submitting the XXCUS_TM_INTERFACE_PKG Customer Interface';
            fnd_file.put_line(fnd_file.log, l_statement);
            fnd_file.put_line(fnd_file.output, l_statement);
            RAISE program_error;
          END IF;
        END IF;
      END;
      --end customer import

      l_sec := 'Rebate vendor "MVID": customer import done.';

      UPDATE xxcus.xxcus_rebate_aud_tbl
         SET stage = l_sec, stg_date = SYSDATE
       WHERE conc_request_id = nvl(fnd_global.conc_request_id, 0)
         AND procedure_name = l_procedure
         AND trunc(processed_datetime) = trunc(SYSDATE);

      COMMIT;

      fnd_file.put_line(fnd_file.log
                       ,'End time:  ' ||
                        to_char(SYSDATE, 'DD-MON-YYYY HH24:MI:SS') ||
                        ' for ' || l_sec);
      fnd_file.put_line(fnd_file.output
                       ,'End time:  ' ||
                        to_char(SYSDATE, 'DD-MON-YYYY HH24:MI:SS') ||
                        ' for ' || l_sec);
    END;

    --start location, account and sites api for Pay to vendor
    BEGIN

      l_sec := 'PayTo API calls.';

      fnd_file.put_line(fnd_file.log
                       ,'Start time:  ' ||
                        to_char(SYSDATE, 'DD-MON-YYYY HH24:MI:SS') ||
                        ' for ' || l_sec);
      fnd_file.put_line(fnd_file.output
                       ,'Start time:  ' ||
                        to_char(SYSDATE, 'DD-MON-YYYY HH24:MI:SS') ||
                        ' for ' || l_sec);

      FOR c_rebt IN (SELECT p.party_id
                           ,p.party_name
                           ,REPLACE(REPLACE(REPLACE(v.rebt_vndr_nm, '.')
                                           ,',')
                                   ,'-') rebt_vndr_nm
                           ,ca.cust_account_id
                           ,v.geo_loc_hier_lob_nm
                           ,v.bu_nm
                           ,v.bu_cd
                           ,src_sys_nm
                           ,pt_vndr_mail_addr_nm
                           ,pt_vndr_tax_id_nbr
                           ,pt_vndr_cd
                           ,rebt_vndr_cd
                           ,pt_vndr_id
                           ,rebate_flg
                           ,REPLACE(REPLACE(REPLACE(substr(nvl(pt_mail_addr_ln1
                                                              ,'NONE')
                                                          ,1
                                                          ,240)
                                                   ,'UNKNOWN'
                                                   ,'NONE')
                                           ,'*'
                                           ,NULL)
                                   ,'.'
                                   ,NULL) vndr_addr_ln1
                           ,REPLACE(REPLACE(REPLACE(substr(pt_mail_addr_ln2
                                                          ,1
                                                          ,240)
                                                   ,'UNKNOWN'
                                                   ,NULL)
                                           ,'*'
                                           ,NULL)
                                   ,'.'
                                   ,NULL) vndr_addr_ln2
                           ,nvl(pt_mail_city, 'X') city_nm
                           ,nvl(pt_mail_st_prov, 'X') terr_nm
                           ,pt_mail_pstl_cd pstl_cd
                           ,(CASE
                              WHEN pt_mail_cntry IN ('US', 'USA', 'UNKNOWN') THEN
                               'US'
                              WHEN pt_mail_cntry IN
                                   ('CA', 'CAN', 'CANN', 'CANADA') THEN
                               'CA'
                              ELSE
                               'US'
                            END) cntry_nm
                           ,v.curnc_flg
                           ,p.status
                           ,p.object_version_number
                           ,(SELECT ps.party_site_id
                               FROM hz_party_sites ps
                              WHERE nvl(substr(party_site_number
                                              ,1
                                              ,instr(party_site_number, '|') - 1)
                                       ,party_site_number) =
                                    pt_vndr_cd || '~' || bu_cd
                                AND ps.party_id = p.party_id) party_site_id
                           ,(SELECT location_id
                               FROM hz_locations l
                              WHERE l.orig_system_reference =
                                    'PT VNDR~' || v.pt_vndr_cd || '~' ||
                                    v.bu_cd
                                AND l.attribute1 = v.pt_vndr_id
                                AND l.attribute2 = v.geo_loc_hier_lob_nm
                                AND l.attribute3 = v.pt_vndr_cd
                                AND rownum = 1) location_id
                       FROM xxcus.xxcus_rebate_vndr_curr_tbl v
                           ,ar.hz_parties                    p
                           ,ar.hz_cust_accounts              ca
                      WHERE nvl(status_flag, 'N') IN ('N', 'A')
                        AND bu_nm <> 'UNKNOWN'
                        AND rebt_vndr_cd || '~MSTR' = p.party_number
                        AND p.attribute1 = l_mstr_attr
                        AND p.party_number = ca.account_number)
      LOOP

        x_return_status      := NULL;
        x_msg_count          := NULL;
        x_msg_data           := NULL;
        x_location_id        := NULL;
        x_party_site_id      := NULL;
        x_party_site_number  := NULL;
        p_location_rec       := NULL;
        p_party_site_rec     := NULL;
        p_party_site_use_rec := NULL;
        p_cust_acct_site_rec := NULL;
        p_cust_site_use_rec  := NULL;
        l_primary_flag       := 'N'; --Version 1.5     PayTo hz_cust_site_uses should not be set as primary

        IF c_rebt.curnc_flg = 'USD'
        THEN
          l_org := l_us_org;
        ELSE
          l_org := l_cn_org;
        END IF;

        --start Location for Pay to Vendor
        IF c_rebt.party_site_id IS NULL AND c_rebt.location_id IS NULL
        THEN

          x_return_status := NULL;
          x_msg_count     := NULL;
          x_msg_data      := NULL;
          x_location_id   := NULL;

          p_location_rec.country               := c_rebt.cntry_nm;
          p_location_rec.address1              := c_rebt.vndr_addr_ln1;
          p_location_rec.address2              := c_rebt.vndr_addr_ln2;
          p_location_rec.city                  := c_rebt.city_nm;
          p_location_rec.postal_code           := c_rebt.pstl_cd;
          p_location_rec.state                 := c_rebt.terr_nm;
          p_location_rec.county                := NULL; --'X'; changed for validation
          p_location_rec.orig_system_reference := 'PT VNDR~' ||
                                                  c_rebt.pt_vndr_cd || '~' ||
                                                  c_rebt.bu_cd;
          p_location_rec.attribute1            := c_rebt.pt_vndr_id;
          p_location_rec.attribute2            := c_rebt.geo_loc_hier_lob_nm;
          p_location_rec.attribute3            := c_rebt.pt_vndr_cd;
          p_location_rec.created_by_module     := l_module;

          hz_location_v2pub.create_location('T'
                                           ,p_location_rec
                                           ,x_location_id
                                           ,x_return_status
                                           ,x_msg_count
                                           ,x_msg_data);

          IF x_return_status = 'S'
          THEN

            l_sec := 'Data loaded for Pay To Vendor location :  ' ||
                     'PT VNDR~' || c_rebt.pt_vndr_cd || '~' || c_rebt.bu_cd ||
                     ' Status = ' || x_return_status;

            fnd_file.put_line(fnd_file.output, l_sec);

            --Party Site API for the PayTo

            x_return_status     := NULL;
            x_msg_count         := NULL;
            x_msg_data          := NULL;
            x_party_site_id     := NULL;
            x_party_site_number := NULL;

            BEGIN
              l_count_site := NULL;

              SELECT COUNT(*)
                INTO l_count_site
                FROM hz_party_sites
               WHERE nvl(substr(party_site_number
                               ,1
                               ,instr(party_site_number, '|') - 1)
                        ,party_site_number) =
                     c_rebt.pt_vndr_cd || '~' || c_rebt.bu_cd
                 AND party_id <> c_rebt.party_id;
            EXCEPTION
              WHEN OTHERS THEN
                l_count_site := NULL;
            END;

            IF l_count_site > 0
            THEN
              l_site := '|' || lpad(l_count_site + 1, 2, 0);
            ELSE
              l_site := NULL;
            END IF;

            p_party_site_rec.party_id                 := c_rebt.party_id;
            p_party_site_rec.party_site_number        := c_rebt.pt_vndr_cd || '~' ||
                                                         c_rebt.bu_cd ||
                                                         l_site;
            p_party_site_rec.location_id              := x_location_id;
            p_party_site_rec.party_site_name          := c_rebt.pt_vndr_mail_addr_nm;
            p_party_site_rec.orig_system_reference    := 'PT VNDR~' ||
                                                         c_rebt.pt_vndr_cd || '~' ||
                                                         c_rebt.bu_cd;
            p_party_site_rec.identifying_address_flag := 'N';
            p_party_site_rec.attribute2               := c_rebt.pt_vndr_id;
            p_party_site_rec.attribute3               := c_rebt.geo_loc_hier_lob_nm;
            p_party_site_rec.attribute4               := c_rebt.pt_vndr_cd;
            p_party_site_rec.attribute5               := c_rebt.rebate_flg;
            p_party_site_rec.attribute6               := c_rebt.pt_vndr_tax_id_nbr;
            p_party_site_rec.created_by_module        := l_module;

            hz_party_site_v2pub.create_party_site('T'
                                                 ,p_party_site_rec
                                                 ,x_party_site_id
                                                 ,x_party_site_number
                                                 ,x_return_status
                                                 ,x_msg_count
                                                 ,x_msg_data);

            IF x_return_status = 'S'
            THEN

              l_sec := 'Data loaded for Pay To Vendor - Create Party Site Num:  ' ||
                       c_rebt.pt_vndr_cd || '~' || c_rebt.bu_cd || l_site --Version 1.1
                       || ' Status = ' || x_return_status;

              fnd_file.put_line(fnd_file.output, l_sec);

              --start party site use type for PayTo
              x_party_site_use_id := NULL;
              x_return_status     := NULL;
              x_msg_count         := NULL;
              x_msg_data          := NULL;

              p_party_site_use_rec.site_use_type     := 'BILL_TO';
              p_party_site_use_rec.party_site_id     := x_party_site_id;
              p_party_site_use_rec.created_by_module := l_module;
              hz_party_site_v2pub.create_party_site_use('T'
                                                       ,p_party_site_use_rec
                                                       ,x_party_site_use_id
                                                       ,x_return_status
                                                       ,x_msg_count
                                                       ,x_msg_data);

              IF x_return_status = 'S'
              THEN

                l_sec := 'Data loaded for PayTo Vendor - Create Party Site Use:  ' ||
                         c_rebt.pt_vndr_cd || '~' || c_rebt.bu_cd || l_site ||
                         ' Status = ' || x_return_status;

                fnd_file.put_line(fnd_file.output, l_sec);

                --start cust account site for PayTo
                x_cust_acct_site_id := NULL;
                x_return_status     := NULL;
                x_msg_count         := NULL;
                x_msg_data          := NULL;
                --------------------------------------------------------------------------
                -- Apps Initialize
                --------------------------------------------------------------------------
                fnd_global.apps_initialize(1211 --l_user_id
                                          ,50622 --l_responsibility_id
                                          ,222 --l_resp_application_id
                                           );

                mo_global.init('AR');
                mo_global.set_policy_context('S', l_org);

                p_cust_acct_site_rec.cust_account_id       := c_rebt.cust_account_id;
                p_cust_acct_site_rec.party_site_id         := x_party_site_id;
                p_cust_acct_site_rec.language              := 'US';
                p_cust_acct_site_rec.org_id                := l_org;
                p_cust_acct_site_rec.orig_system_reference := 'PT VNDR~' ||
                                                              c_rebt.pt_vndr_cd || '~' ||
                                                              c_rebt.bu_cd;
                p_cust_acct_site_rec.created_by_module     := l_module;
                hz_cust_account_site_v2pub.create_cust_acct_site('T'
                                                                ,p_cust_acct_site_rec
                                                                ,x_cust_acct_site_id
                                                                ,x_return_status
                                                                ,x_msg_count
                                                                ,x_msg_data);

                IF x_return_status = 'S'
                THEN

                  l_sec := 'Data loaded for PayTo  - Create Cust Acct Site:  ' ||
                           c_rebt.pt_vndr_cd || '~' || c_rebt.bu_cd ||
                           l_site || ' Status = ' || x_return_status;

                  fnd_file.put_line(fnd_file.output, l_sec);

                  --start Customer Account Site Use for PayTo
                  x_site_use_id   := NULL;
                  x_return_status := NULL;
                  x_msg_count     := NULL;
                  x_msg_data      := NULL;

                  --------------------------------------------------------------------------
                  -- Apps Initialize
                  --------------------------------------------------------------------------
                  fnd_global.apps_initialize(1211 --l_user_id
                                            ,50622 --l_responsibility_id
                                            ,222 --l_resp_application_id
                                             );

                  mo_global.init('AR');
                  mo_global.set_policy_context('S', l_org);

                  p_cust_site_use_rec.cust_acct_site_id     := x_cust_acct_site_id;
                  p_cust_site_use_rec.site_use_code         := 'BILL_TO';
                  p_cust_site_use_rec.org_id                := l_org;
                  p_cust_site_use_rec.location              := c_rebt.pt_vndr_cd || '~' ||
                                                               c_rebt.bu_cd;
                  p_cust_site_use_rec.orig_system_reference := 'PT VNDR~' ||
                                                               c_rebt.pt_vndr_cd || '~' ||
                                                               c_rebt.bu_cd;
                  p_cust_site_use_rec.primary_flag          := l_primary_flag; --Version 1.5
                  p_cust_site_use_rec.created_by_module     := l_module;
                  hz_cust_account_site_v2pub.create_cust_site_use('T'
                                                                 ,p_cust_site_use_rec
                                                                 ,p_customer_profile_rec
                                                                 ,''
                                                                 ,''
                                                                 ,x_site_use_id
                                                                 ,x_return_status
                                                                 ,x_msg_count
                                                                 ,x_msg_data);

                  IF x_return_status = 'S'
                  THEN

                    l_sec := 'Data loaded for PayTo Vendor - Create Cust Acct Site Use:  ' ||
                             c_rebt.pt_vndr_cd || '~' || c_rebt.bu_cd ||
                             l_site || ' Status = ' || x_return_status;

                    fnd_file.put_line(fnd_file.output, l_sec);

                  ELSE
                    l_sec := 'ERROR Failed to load for PayTo Vendor - Create Cust Acct Site Use:  ' ||
                             c_rebt.pt_vndr_cd || '~' || c_rebt.bu_cd ||
                             l_site || ' Status = ' || x_return_status ||
                             ' Reason = ' || x_msg_data;
                    fnd_file.put_line(fnd_file.log, l_sec);
                    fnd_file.put_line(fnd_file.output, l_sec);

                  END IF;

                ELSE
                  l_sec := 'ERROR Failed to load for Pay To Vendor - Create Cust Acct Site:  ' ||
                           c_rebt.pt_vndr_cd || '~' || c_rebt.bu_cd ||
                           l_site || ' Status = ' || x_return_status ||
                           ' Reason = ' || x_msg_data;
                  fnd_file.put_line(fnd_file.log, l_sec);
                  fnd_file.put_line(fnd_file.output, l_sec);
                END IF;

              ELSE
                l_sec := 'ERROR Failed to load for PayTo - Create Party Site Use:  ' ||
                         c_rebt.pt_vndr_cd || '~' || c_rebt.bu_cd || l_site ||
                         ' Status = ' || x_return_status || ' Reason = ' ||
                         x_msg_data;
                fnd_file.put_line(fnd_file.log, l_sec);
                fnd_file.put_line(fnd_file.output, l_sec);
              END IF;

            ELSE
              l_sec := 'ERROR Failed to load for PayTo Vendor - Create Party Site Num:  ' ||
                       c_rebt.pt_vndr_cd || '~' || c_rebt.bu_cd || l_site ||
                       ' Status = ' || x_return_status || ' Reason = ' ||
                       x_msg_data;
              fnd_file.put_line(fnd_file.log, l_sec);
              fnd_file.put_line(fnd_file.output, l_sec);
            END IF;

          ELSE

            l_sec := 'ERROR - Creation of Location failed:  ' || 'PT VNDR~' ||
                     c_rebt.pt_vndr_cd || '~' || c_rebt.bu_cd ||
                     ' Status = ' || x_return_status || ' Reason = ' ||
                     x_msg_data;

            fnd_file.put_line(fnd_file.log, l_sec);
            fnd_file.put_line(fnd_file.output, l_sec);
          END IF;

        END IF;

        --Location setup but missing party site
        IF c_rebt.party_site_id IS NULL AND c_rebt.location_id IS NOT NULL
        THEN

          x_return_status     := NULL;
          x_msg_count         := NULL;
          x_msg_data          := NULL;
          x_party_site_id     := NULL;
          x_party_site_number := NULL;

          BEGIN
            l_count_site := NULL;

            SELECT COUNT(*)
              INTO l_count_site
              FROM hz_party_sites
             WHERE nvl(substr(party_site_number
                             ,1
                             ,instr(party_site_number, '|') - 1)
                      ,party_site_number) =
                   c_rebt.pt_vndr_cd || '~' || c_rebt.bu_cd
               AND party_id <> c_rebt.party_id;
          EXCEPTION
            WHEN OTHERS THEN
              l_count_site := NULL;
          END;

          IF l_count_site > 0
          THEN
            l_site := '|' || lpad(l_count_site + 1, 2, 0);
          ELSE
            l_site := NULL;
          END IF;

          p_party_site_rec.party_id                 := c_rebt.party_id;
          p_party_site_rec.party_site_number        := c_rebt.pt_vndr_cd || '~' ||
                                                       c_rebt.bu_cd ||
                                                       l_site;
          p_party_site_rec.location_id              := c_rebt.location_id;
          p_party_site_rec.party_site_name          := c_rebt.pt_vndr_mail_addr_nm;
          p_party_site_rec.orig_system_reference    := 'PT VNDR~' ||
                                                       c_rebt.pt_vndr_cd || '~' ||
                                                       c_rebt.bu_cd;
          p_party_site_rec.identifying_address_flag := 'N';
          p_party_site_rec.attribute2               := c_rebt.pt_vndr_id;
          p_party_site_rec.attribute3               := c_rebt.geo_loc_hier_lob_nm;
          p_party_site_rec.attribute4               := c_rebt.pt_vndr_cd;
          p_party_site_rec.attribute5               := c_rebt.rebate_flg;
          p_party_site_rec.attribute6               := c_rebt.pt_vndr_tax_id_nbr;
          p_party_site_rec.created_by_module        := l_module;

          hz_party_site_v2pub.create_party_site('T'
                                               ,p_party_site_rec
                                               ,x_party_site_id
                                               ,x_party_site_number
                                               ,x_return_status
                                               ,x_msg_count
                                               ,x_msg_data);

          IF x_return_status = 'S'
          THEN

            l_sec := 'Data loaded for Pay To Vendor - Create Party Site Num:  ' ||
                     c_rebt.pt_vndr_cd || '~' || c_rebt.bu_cd || l_site ||
                     ' Status = ' || x_return_status;

            fnd_file.put_line(fnd_file.output, l_sec);

            --start party site use type for PayTo vendors sites where location had been create
            x_party_site_use_id := NULL;
            x_return_status     := NULL;
            x_msg_count         := NULL;
            x_msg_data          := NULL;

            p_party_site_use_rec.site_use_type     := 'BILL_TO';
            p_party_site_use_rec.party_site_id     := x_party_site_id;
            p_party_site_use_rec.created_by_module := l_module;
            hz_party_site_v2pub.create_party_site_use('T'
                                                     ,p_party_site_use_rec
                                                     ,x_party_site_use_id
                                                     ,x_return_status
                                                     ,x_msg_count
                                                     ,x_msg_data);

            IF x_return_status = 'S'
            THEN

              l_sec := 'Data loaded for Pay To Vendor - Create Party Site Use:  ' ||
                       c_rebt.pt_vndr_cd || '~' || c_rebt.bu_cd || l_site ||
                       ' Status = ' || x_return_status;

              fnd_file.put_line(fnd_file.output, l_sec);

              --start cust account site for PayTo where location had been create
              x_cust_acct_site_id := NULL;
              x_return_status     := NULL;
              x_msg_count         := NULL;
              x_msg_data          := NULL;

              --------------------------------------------------------------------------
              -- Apps Initialize
              --------------------------------------------------------------------------
              fnd_global.apps_initialize(1211 --l_user_id
                                        ,50622 --l_responsibility_id
                                        ,222 --l_resp_application_id
                                         );

              mo_global.init('AR');
              mo_global.set_policy_context('S', l_org);

              p_cust_acct_site_rec.cust_account_id       := c_rebt.cust_account_id;
              p_cust_acct_site_rec.party_site_id         := x_party_site_id;
              p_cust_acct_site_rec.language              := 'US';
              p_cust_acct_site_rec.org_id                := l_org;
              p_cust_acct_site_rec.orig_system_reference := 'PT VNDR~' ||
                                                            c_rebt.pt_vndr_cd || '~' ||
                                                            c_rebt.bu_cd;
              p_cust_acct_site_rec.created_by_module     := l_module;
              hz_cust_account_site_v2pub.create_cust_acct_site('T'
                                                              ,p_cust_acct_site_rec
                                                              ,x_cust_acct_site_id
                                                              ,x_return_status
                                                              ,x_msg_count
                                                              ,x_msg_data);

              IF x_return_status = 'S'
              THEN

                l_sec := 'Data loaded for Pay To Vendor - Create Cust Acct Site:  ' ||
                         c_rebt.pt_vndr_cd || '~' || c_rebt.bu_cd || l_site ||
                         ' Status = ' || x_return_status;

                fnd_file.put_line(fnd_file.output, l_sec);

                --start Customer Account Site Use for PayTo where location had been create
                x_site_use_id   := NULL;
                x_return_status := NULL;
                x_msg_count     := NULL;
                x_msg_data      := NULL;

                --------------------------------------------------------------------------
                -- Apps Initialize
                --------------------------------------------------------------------------
                fnd_global.apps_initialize(1211 --l_user_id
                                          ,50622 --l_responsibility_id
                                          ,222 --l_resp_application_id
                                           );

                mo_global.init('AR');
                mo_global.set_policy_context('S', l_org);

                p_cust_site_use_rec.cust_acct_site_id     := x_cust_acct_site_id;
                p_cust_site_use_rec.site_use_code         := 'BILL_TO';
                p_cust_site_use_rec.org_id                := l_org;
                p_cust_site_use_rec.location              := c_rebt.pt_vndr_cd || '~' ||
                                                             c_rebt.bu_cd;
                p_cust_site_use_rec.orig_system_reference := 'PT VNDR~' ||
                                                             c_rebt.pt_vndr_cd || '~' ||
                                                             c_rebt.bu_cd;
                p_cust_site_use_rec.primary_flag          := l_primary_flag; --Version 1.5
                p_cust_site_use_rec.created_by_module     := l_module;
                hz_cust_account_site_v2pub.create_cust_site_use('T'
                                                               ,p_cust_site_use_rec
                                                               ,p_customer_profile_rec
                                                               ,''
                                                               ,''
                                                               ,x_site_use_id
                                                               ,x_return_status
                                                               ,x_msg_count
                                                               ,x_msg_data);

                IF x_return_status = 'S'
                THEN

                  l_sec := 'Data loaded for Pay To Vendor - Create Cust Acct Site Use:  ' ||
                           c_rebt.pt_vndr_cd || '~' || c_rebt.bu_cd ||
                           l_site || ' Status = ' || x_return_status;

                  fnd_file.put_line(fnd_file.output, l_sec);

                ELSE
                  l_sec := 'ERROR Failed to load for Pay To Vendor - Create Cust Acct Site Use:  ' ||
                           c_rebt.pt_vndr_cd || '~' || c_rebt.bu_cd ||
                           l_site || ' Status = ' || x_return_status ||
                           ' Reason = ' || x_msg_data;
                  fnd_file.put_line(fnd_file.log, l_sec);
                  fnd_file.put_line(fnd_file.output, l_sec);
                END IF;

              ELSE
                l_sec := 'ERROR Failed to load for Pay To Vendor - Create Cust Acct Site:  ' ||
                         c_rebt.pt_vndr_cd || '~' || c_rebt.bu_cd || l_site ||
                         ' Status = ' || x_return_status || ' Reason = ' ||
                         x_msg_data;
                fnd_file.put_line(fnd_file.log, l_sec);
                fnd_file.put_line(fnd_file.output, l_sec);
              END IF;

            ELSE
              l_sec := 'ERROR Failed to load for Pay To Vendor - Create Party Site Use:  ' ||
                       c_rebt.pt_vndr_cd || '~' || c_rebt.bu_cd || l_site ||
                       ' Status = ' || x_return_status || ' Reason = ' ||
                       x_msg_data;
              fnd_file.put_line(fnd_file.log, l_sec);
              fnd_file.put_line(fnd_file.output, l_sec);
            END IF;

          ELSE
            l_sec := 'ERROR Failed to load for Pay To Vendor - Create Party Site Num:  ' ||
                     'Party Site ID:  ' || x_party_site_id || 'PAY-TO:  ' ||
                     c_rebt.pt_vndr_cd || '~' || c_rebt.bu_cd || l_site ||
                     ' Status = ' || x_return_status || ' Reason = ' ||
                     x_msg_data;
            fnd_file.put_line(fnd_file.log, l_sec);
            fnd_file.put_line(fnd_file.output, l_sec);
          END IF;

          --New PayTo processed
          UPDATE xxcus.xxcus_rebate_vndr_curr_tbl
             SET request_id   = nvl(fnd_global.conc_request_id, 0)
                ,status_flag  = 'P'
                ,process_date = SYSDATE
           WHERE pt_vndr_id = c_rebt.pt_vndr_id;
        END IF;
      END LOOP;
      COMMIT;

      fnd_file.put_line(fnd_file.log
                       ,'End time:  ' ||
                        to_char(SYSDATE, 'DD-MON-YYYY HH24:MI:SS') ||
                        ' for ' || l_sec);
      fnd_file.put_line(fnd_file.output
                       ,'End time:  ' ||
                        to_char(SYSDATE, 'DD-MON-YYYY HH24:MI:SS') ||
                        ' for ' || l_sec);
    END;

    --start updates to pay to vendors
    BEGIN

      l_sec := 'Updates to PayTo address or party site name';

      fnd_file.put_line(fnd_file.log
                       ,'Start time:  ' ||
                        to_char(SYSDATE, 'DD-MON-YYYY HH24:MI:SS') ||
                        ' for ' || l_sec);
      fnd_file.put_line(fnd_file.output
                       ,'Start time:  ' ||
                        to_char(SYSDATE, 'DD-MON-YYYY HH24:MI:SS') ||
                        ' for ' || l_sec);

      FOR c_rebt IN (SELECT l.location_id
                           ,l.object_version_number location_version
                           ,p.party_id
                           ,p.party_name
                           ,REPLACE(REPLACE(REPLACE(v.rebt_vndr_nm, '.')
                                           ,',')
                                   ,'-') rebt_vndr_nm
                           ,ca.cust_account_id
                           ,v.geo_loc_hier_lob_nm
                           ,v.bu_nm
                           ,v.bu_cd
                           ,src_sys_nm
                           ,pt_vndr_mail_addr_nm
                           ,ps.party_site_name
                           ,pt_vndr_tax_id_nbr
                           ,pt_vndr_cd
                           ,rebt_vndr_cd
                           ,pt_vndr_id
                           ,rebate_flg
                           ,REPLACE(REPLACE(REPLACE(substr(nvl(pt_mail_addr_ln1
                                                              ,'NONE')
                                                          ,1
                                                          ,240)
                                                   ,'UNKNOWN'
                                                   ,'NONE')
                                           ,'*'
                                           ,NULL)
                                   ,'.'
                                   ,NULL) vndr_addr_ln1
                           ,REPLACE(REPLACE(REPLACE(substr(pt_mail_addr_ln2
                                                          ,1
                                                          ,240)
                                                   ,'UNKNOWN'
                                                   ,NULL)
                                           ,'*'
                                           ,NULL)
                                   ,'.'
                                   ,NULL) vndr_addr_ln2
                           ,nvl(pt_mail_city, 'X') city_nm
                           ,nvl(pt_mail_st_prov, 'X') terr_nm
                           ,pt_mail_pstl_cd pstl_cd
                           ,(CASE
                              WHEN pt_mail_cntry IN ('US', 'USA', 'UNKNOWN') THEN
                               'US'
                              WHEN pt_mail_cntry IN
                                   ('CA', 'CAN', 'CANN', 'CANADA') THEN
                               'CA'
                              ELSE
                               'US'
                            END) cntry_nm
                           ,v.curnc_flg
                           ,l.address1
                           ,l.address2
                           ,l.city
                           ,l.postal_code
                           ,l.state
                           ,p.status
                           ,p.object_version_number
                           ,ps.party_site_id
                           ,ps.object_version_number site_vers_num
                           ,ps.status site_status
                           ,ps.end_date_active
                       FROM xxcus.xxcus_rebate_vndr_curr_tbl v
                           ,ar.hz_parties                    p
                           ,ar.hz_cust_accounts              ca
                           ,hz_party_sites                   ps
                           ,ar.hz_locations                  l
                      WHERE nvl(status_flag, 'N') IN ('N', 'A')
                        AND bu_nm <> 'UNKNOWN'
                        AND rebt_vndr_cd || '~MSTR' = p.party_number
                        AND p.attribute1 = l_mstr_attr
                        AND p.party_id = ca.party_id
                        AND p.party_number = ca.account_number
                        AND p.party_id = ps.party_id
                        AND nvl(substr(party_site_number
                                      ,1
                                      ,instr(party_site_number, '|') - 1)
                               ,party_site_number) =
                            pt_vndr_cd || '~' || bu_cd
                        AND ps.location_id = l.location_id)
      LOOP

        x_return_status            := NULL;
        x_msg_count                := NULL;
        x_msg_data                 := NULL;
        p_party_site_rec           := NULL;
        p_location_rec             := NULL;
        p_location_rec.location_id := NULL;

        --------update for PayTo vendor name in party sites---------------------
        IF c_rebt.party_site_name <> c_rebt.pt_vndr_mail_addr_nm
        THEN

          x_return_status := NULL;
          x_msg_count     := NULL;
          x_msg_data      := NULL;

          p_party_site_rec.party_site_id     := c_rebt.party_site_id;
          p_party_site_rec.party_site_name   := c_rebt.pt_vndr_mail_addr_nm;
          p_object_version_number            := c_rebt.site_vers_num;
          p_party_site_rec.created_by_module := l_module;

          hz_party_site_v2pub.update_party_site('T'
                                               ,p_party_site_rec
                                               ,p_object_version_number
                                               ,x_return_status
                                               ,x_msg_count
                                               ,x_msg_data);

          IF x_return_status = 'S'
          THEN

            l_sec := 'Pay to vendor update on party sites :  ' ||
                     c_rebt.party_site_id || ' Status = ' ||
                     x_return_status;

            fnd_file.put_line(fnd_file.output, l_sec);
          ELSE
            l_sec := 'Pay to vendor update Failed on party sites :  ' ||
                     c_rebt.party_site_id || ' Status = ' ||
                     x_return_status || ' Reason = ' || x_msg_data;

            fnd_file.put_line(fnd_file.log, l_sec);
            fnd_file.put_line(fnd_file.output, l_sec);
          END IF;
        END IF;
        -- end update to party site name for PayTo

        -- update for PayTo vendor address in hz_locations
        IF c_rebt.site_status = 'A' AND
           (c_rebt.vndr_addr_ln1 IS NOT NULL AND
           c_rebt.vndr_addr_ln1 <> c_rebt.address1) OR
           (c_rebt.city_nm IS NOT NULL AND c_rebt.city_nm <> c_rebt.city) OR
           (c_rebt.terr_nm IS NOT NULL AND c_rebt.terr_nm <> c_rebt.state) OR
           (c_rebt.pstl_cd IS NOT NULL AND
           c_rebt.pstl_cd <> c_rebt.postal_code)
        THEN

          x_return_status := NULL;
          x_msg_count     := NULL;
          x_msg_data      := NULL;

          p_location_rec.location_id := c_rebt.location_id;
          p_location_rec.address2    := c_rebt.vndr_addr_ln2;
          p_location_rec.address1    := c_rebt.vndr_addr_ln1;
          p_location_rec.city        := c_rebt.city_nm;
          p_location_rec.postal_code := c_rebt.pstl_cd;
          p_location_rec.state       := c_rebt.terr_nm;
          p_location_rec.country     := c_rebt.cntry_nm;
          p_object_version_number    := c_rebt.location_version;

          hz_location_v2pub.update_location('T'
                                           ,p_location_rec
                                           ,p_object_version_number
                                           ,x_return_status
                                           ,x_msg_count
                                           ,x_msg_data);

          IF x_return_status = 'S'
          THEN
            l_sec := 'Update address for location ID:  ' ||
                     c_rebt.location_id || ' Status = ' || x_return_status;

            fnd_file.put_line(fnd_file.output, l_sec);

          ELSE
            l_sec := 'ERROR Update Failed address for location ID:  ' ||
                     c_rebt.location_id || ' Status = ' || x_return_status ||
                     ' Reason = ' || x_msg_data;
            fnd_file.put_line(fnd_file.log, l_sec);
            fnd_file.put_line(fnd_file.output, l_sec);
          END IF;
        END IF;
        --End update to PayTo vendor address

        UPDATE xxcus.xxcus_rebate_vndr_curr_tbl
           SET request_id   = nvl(fnd_global.conc_request_id, 0)
              ,status_flag  = 'P'
              ,process_date = SYSDATE
         WHERE pt_vndr_id = c_rebt.pt_vndr_id;

      END LOOP;
      COMMIT;

      fnd_file.put_line(fnd_file.log
                       ,'End time:  ' ||
                        to_char(SYSDATE, 'DD-MON-YYYY HH24:MI:SS') ||
                        ' for ' || l_sec);
      fnd_file.put_line(fnd_file.output
                       ,'End time:  ' ||
                        to_char(SYSDATE, 'DD-MON-YYYY HH24:MI:SS') ||
                        ' for ' || l_sec);
    END;

    --End updates Pay To Vendor

    --Version 1.5 04/23/2013 need to turn address validation to Error after creating or updating the customers
    l_level := 'ERROR';

    l_sec := 'Changing Address Validation to ERROR.';
    fnd_file.put_line(fnd_file.log
                     ,'Start time:  ' ||
                      to_char(SYSDATE, 'DD-MON-YYYY HH24:MI:SS') ||
                      ' for ' || l_sec);
    fnd_file.put_line(fnd_file.output
                     ,'Start time:  ' ||
                      to_char(SYSDATE, 'DD-MON-YYYY HH24:MI:SS') ||
                      ' for ' || l_sec);

    xxcus_ar_set_geo_val_level.submit_job(l_err_msg
                                         ,l_err_code
                                         ,l_level
                                         ,l_user
                                         ,l_responsibility_name);

    fnd_file.put_line(fnd_file.log
                     ,'End time:  ' ||
                      to_char(SYSDATE, 'DD-MON-YYYY HH24:MI:SS') ||
                      ' for ' || l_sec);
    fnd_file.put_line(fnd_file.output
                     ,'End time:  ' ||
                      to_char(SYSDATE, 'DD-MON-YYYY HH24:MI:SS') ||
                      ' for ' || l_sec);
    -- end 04/23/2013

    l_sec := 'Finished.';
    fnd_file.put_line(fnd_file.log
                     ,'End time:  ' ||
                      to_char(SYSDATE, 'DD-MON-YYYY HH24:MI:SS') ||
                      ' for ' || l_sec);
    fnd_file.put_line(fnd_file.output
                     ,'End time:  ' ||
                      to_char(SYSDATE, 'DD-MON-YYYY HH24:MI:SS') ||
                      ' for ' || l_sec);

    UPDATE xxcus.xxcus_rebate_aud_tbl
       SET stage = l_sec, stg_date = SYSDATE
     WHERE conc_request_id = nvl(fnd_global.conc_request_id, 0)
       AND procedure_name = l_procedure
       AND trunc(processed_datetime) = trunc(SYSDATE);

    --call to create segment1 of the value set "HDS_Supplier_Code" being used for items.
    create_valueset;

  EXCEPTION
    WHEN program_error THEN
      ROLLBACK;
      l_err_code := 2;
      l_err_msg  := l_err_msg || ' Error_Stack...' ||
                    dbms_utility.format_error_stack() ||
                    ' Error_Backtrace...' ||
                    dbms_utility.format_error_backtrace();
      fnd_file.put_line(fnd_file.log, l_err_msg);
      fnd_file.put_line(fnd_file.output, l_err_msg);
      l_err_callpoint := l_sec;

      xxcus_error_pkg.xxcus_error_main_api(p_called_from       => l_err_callfrom
                                          ,p_calling           => l_err_callpoint
                                          ,p_request_id        => l_req_id
                                          ,p_ora_error_msg     => substr(regexp_replace(l_err_msg
                                                                                       ,'[[:cntrl:]]'
                                                                                       ,NULL)
                                                                        ,1
                                                                        ,2000)
                                          ,p_error_desc        => substr(l_err_msg
                                                                        ,1
                                                                        ,240)
                                          ,p_distribution_list => l_distro_list
                                          ,p_module            => 'TM');

    WHEN OTHERS THEN
      fnd_file.put_line(fnd_file.log, l_sec);
      fnd_file.put_line(fnd_file.output, l_sec);
      l_err_code := 2;
      l_err_msg  := l_err_msg || ' Error_Stack...' ||
                    dbms_utility.format_error_stack() ||
                    ' Error_Backtrace...' ||
                    dbms_utility.format_error_backtrace();
      fnd_file.put_line(fnd_file.log, l_err_msg);
      fnd_file.put_line(fnd_file.output, l_err_msg);
      l_err_callpoint := l_sec;

      xxcus_error_pkg.xxcus_error_main_api(p_called_from       => l_err_callfrom
                                          ,p_calling           => l_err_callpoint
                                          ,p_request_id        => l_req_id
                                          ,p_ora_error_msg     => substr(regexp_replace(l_err_msg
                                                                                       ,'[[:cntrl:]]'
                                                                                       ,NULL)
                                                                        ,1
                                                                        ,2000)
                                          ,p_error_desc        => substr(l_err_msg
                                                                        ,1
                                                                        ,240)
                                          ,p_distribution_list => l_distro_list
                                          ,p_module            => 'TM');

  END load_rebate_vndr;

  /*******************************************************************************
   * Procedure:   LOAD_REBATE_PRODUCT
   * Description: This will create Products
   *              being items in Oracle. Will be using a MV from
   *              Sourcing DM
   HISTORY
   ===============================================================================
   VERSION DATE          AUTHOR(S)       DESCRIPTION
   ------- -----------   --------------- -----------------------------------------
   1.0     07/22/2010    Kathy Poling    Initial creation of the procedure
   1.1     04/08/2010    Kathy Poling    Removed call to apex classification.  Added truncate history table
   1.2     03/21/2012    Kathy Poling    Change segment1 - item number add suffix '~RBT' for
                                         White Cap project.  RFC 33211
   1.3     05/19/2012    Kathy Poling    Removed truncate of interface tables and changing
                                         to delete for Rebates Org.  Changes for White Cap 20/20
                                         Changes to sku_desc_lng increase column size to allow
                                         data from DW to insert and added substr to attribute6
   1.4     01/15/2013    Kathy Poling    Changed staging table to partition and changed to
                                         using base tables from DW.  Also adding fperiod and
                                         bu as a calling parm
                                         RFC 36582
   1.5     04/18/2013    Kathy Poling    Added lookup XXCUS_REBATE_BU_XREF for mapping
                                         SDW BU name and code to hr_business_unit that
                                         is used in the location code table
   1.6     04/30/2013    Kathy Poling    Product interface deletes errors from Oracles
                                         item and catergory interface prior to starting
                                         new or updates to products
                                         SR 202222
  1.7      8/6/2013      Kathy Poling    Adding additional error comments
                                         Changed truncate to delete of product tbl to
                                         keep items missing UOM setup.  This is needed
                                         to allow business to setup uom and run oneoff
                                         product load.  This has to be completed prior
                                         to the next weeks run or it will have duplicates.
                                         The issue was with the default partition and
                                         the four BU's that map to the same BU in Oracle
                                         SR 202000
  1.8      9/5/2013    Kathy Poling      Adding parm of BU to procedure call load_uom_mapping
                                         RFC 38031
  1.9      9/17/2013   Kathy Poling      Adding source system to attribute field to identify
                                         correct sku's
                                         RFC 38254
  1.10     11/27/2013  Kathy Poling      Added parallel hint for performance.  Change timeout to lookup
                                         Change to category import to follow item import
                                         RFC 38762
   2.4    10/20/2014     Kathy Poling    ESMS 531490 RFC 42024 change parm being passed
                                         to lookup so the Oracle business name is passed
                                         to load_uom_mapping.  Also Changes to make sure only
                                         one sku is created, duplicates where happening
                                         since we don't purge sku's that were not created
                                         for mapping issues.
   ********************************************************************************/

  PROCEDURE load_rebate_product(errbuf    OUT VARCHAR2
                               ,retcode   OUT NUMBER
                               ,p_fperiod IN VARCHAR2
                               ,p_bu_nm   IN VARCHAR2) IS

    --
    -- Package Variables
    --
    l_req_id             NUMBER NULL;
    v_phase              VARCHAR2(50);
    v_status             VARCHAR2(50);
    v_dev_status         VARCHAR2(50);
    v_dev_phase          VARCHAR2(50);
    v_message            VARCHAR2(250);
    v_interval           NUMBER := 30; -- In seconds
    v_max_time           NUMBER := 30000; -- In seconds
    v_error_message      VARCHAR2(3000);
    l_can_submit_request BOOLEAN := TRUE;
    l_err_msg            VARCHAR2(3000);
    l_err_code           NUMBER;
    l_sec                VARCHAR2(255);
    l_count              NUMBER := 0;
    l_procedure          VARCHAR2(50) := 'LOAD_REBATE_PRODUCT';
    l_item_sequence      NUMBER;
    l_category_number    NUMBER := 1123; --Version 1.1   "NEW.NEW"
    l_fiscal_per_id      NUMBER;
    l_set_process_id     NUMBER;
    l_partition          VARCHAR2(30);
    l_partition_sdw      VARCHAR2(30);
    l_oracle_bu_nm       VARCHAR2(80); --Version 1.5
    l_waittime           NUMBER; --Version 1.10
    l_org_id             NUMBER := 84; --Default to HDS Rebates USA - Inv
    l_user_id            fnd_user.user_id%TYPE; --REBTINTERFACE user
    l_user CONSTANT VARCHAR2(100) := 'REBTINTERFACE';
    l_responsibility VARCHAR2(100) := 'XXCUS_CON'; --'HDS Trade Management Administrator - Rebates'
    l_category_set CONSTANT mtl_category_sets_tl.category_set_id%TYPE := 1100000041; --HDS Supplier Category
    l_calendar     CONSTANT gl_periods.period_set_name%TYPE := '4-4-QTR';

    -- Error DEBUG
    l_err_callfrom  VARCHAR2(75) DEFAULT 'XXCUS_TM_INTERFACE_PKG.LOAD_REBATE_PRODUCT';
    l_err_callpoint VARCHAR2(75) DEFAULT 'START';
    l_distro_list   VARCHAR2(75) DEFAULT 'HDSOracleDevelopers@hdsupply.com';

  BEGIN

    SELECT user_id INTO l_user_id FROM fnd_user WHERE user_name = l_user;

    --  Setup parameters for running FND JOBS!
    l_can_submit_request := xxcus_misc_pkg.set_responsibility(l_user
                                                             ,l_responsibility);
    IF l_can_submit_request
    THEN
      l_sec := 'Global Variables are set.';

    ELSE

      l_sec := 'Global Variables are not set for the Responsibility of ' ||
               l_user || ' and the User.';
      fnd_file.put_line(fnd_file.log, l_sec);
      fnd_file.put_line(fnd_file.output, l_sec);
      RAISE program_error;
    END IF;

    --  Check to see if data has been loaded from DW
    SELECT COUNT(*)
      INTO l_count
      FROM xxcus.xxcus_rebate_product_sdw_tbl
     WHERE bu_nm = p_bu_nm;

    IF l_count < 1
    THEN
      -- Product data has not been loaded from DW.
      --Version 1.7  8/6/13
      l_sec := 'Product count for BU:  ' || p_bu_nm || ' = ' || l_count;
      fnd_file.put_line(fnd_file.log, l_sec);
      fnd_file.put_line(fnd_file.output, l_sec);
      RAISE program_error;
    END IF;

    l_sec := 'Getting lookup values';
    fnd_file.put_line(fnd_file.log, l_sec);
    fnd_file.put_line(fnd_file.output, l_sec);

    --Version 1.6
    BEGIN
      fnd_file.put_line(fnd_file.log
                       ,'lookup Oracle BU Name for p_bu_nm = ' || p_bu_nm);

      SELECT oracle_bu_nm
        INTO l_oracle_bu_nm
        FROM apps.xxcus_rebate_bu_nm_v
       WHERE bu_nm = p_bu_nm;
    EXCEPTION
      WHEN no_data_found THEN
        l_err_msg := 'Failed to find Oracle BU Name mapped to SDW BU Name : ' ||
                     p_bu_nm || ' : ' || SQLERRM;
        fnd_file.put_line(fnd_file.log, l_err_msg);
        RAISE program_error;
    END;
    --end 04/1/13

    BEGIN
      --lookup for mapping of bu name to the partition that needs truncate prior to loading receipts
      --to the table for comparison
      SELECT tag --lookup_code     Version 1.6
        INTO l_partition
        FROM fnd_lookup_values_vl
       WHERE lookup_type = 'XXCUS_REBATES_TABLE_PARTITION'
         AND meaning = p_bu_nm
         AND lookup_code NOT LIKE 'SDW.%'
         AND enabled_flag = 'Y'
         AND nvl(end_date_active, SYSDATE) >= SYSDATE;
    EXCEPTION
      WHEN no_data_found THEN
        l_partition := 'OTHER';
    END;

    BEGIN
      --lookup for mapping of bu name to the partition that needs truncate prior to loading receipts
      --to the staging table from SDW
      SELECT meaning
        INTO l_partition_sdw
        FROM fnd_lookup_values_vl
       WHERE lookup_type = 'XXCUS_REBATES_TABLE_PARTITION'
         AND description = p_bu_nm
         AND lookup_code LIKE 'SDW.%'
         AND enabled_flag = 'Y'
         AND nvl(end_date_active, SYSDATE) >= SYSDATE;
    EXCEPTION
      WHEN no_data_found THEN
        l_err_msg := 'BU ' || p_bu_nm ||
                     ' not defined in lookup XXCUS_REBATES_TABLE_PARTITION ' ||
                     SQLERRM;
        fnd_file.put_line(fnd_file.log, l_err_msg);
        RAISE program_error;
      WHEN OTHERS THEN
        l_err_msg := 'Error deriving partition for ' || p_bu_nm;
        RAISE program_error;
    END;

    --lookup for wait time for ddl_lock_timeout
    --Version 1.10
    BEGIN
      SELECT to_number(meaning)
        INTO l_waittime
        FROM fnd_lookup_values_vl
       WHERE lookup_type = 'HDS_TM_RECEIPT_PARAMETERS'
         AND lookup_code = 'WAIT_TIME'
         AND view_application_id = 682 --Trade Management
         AND enabled_flag = 'Y'
         AND nvl(end_date_active, SYSDATE) >= SYSDATE;
    EXCEPTION
      WHEN no_data_found THEN
        l_err_msg := 'Failed to find value for wait time: ' || p_fperiod ||
                     ' : ' || SQLERRM;
        fnd_file.put_line(fnd_file.log, l_err_msg);
        RAISE program_error;
    END;

    BEGIN
      SELECT period_year || lpad(period_num, 2, 0)
        INTO l_fiscal_per_id
        FROM gl.gl_periods
       WHERE period_set_name = l_calendar
         AND period_name = p_fperiod
         AND period_num <> 13;
    EXCEPTION
      WHEN no_data_found THEN
        l_err_msg := 'Failed to get start and end date for period : ' ||
                     p_fperiod || ' : ' || SQLERRM;
        fnd_file.put_line(fnd_file.log, l_err_msg);
        RAISE program_error;
    END;

    --loading new or updated uom from apex
    l_sec := 'Load UOM xref data from Apex.';

    fnd_file.put_line(fnd_file.log
                     ,'Start time:  ' ||
                      to_char(SYSDATE, 'DD-MON-YYYY HH24:MI:SS') ||
                      ' for ' || l_sec);

    fnd_file.put_line(fnd_file.output
                     ,'Start time:  ' ||
                      to_char(SYSDATE, 'DD-MON-YYYY HH24:MI:SS') ||
                      ' for ' || l_sec);
    fnd_file.put_line(fnd_file.log, 'l_oracle_bu_nm = ' || l_oracle_bu_nm);
    --load_uom_mapping(p_bu_nm); --Version 1.8  9/5/13
    load_uom_mapping(l_oracle_bu_nm); --Version 2.4  10/20/14

    fnd_file.put_line(fnd_file.log
                     ,'End time:  ' ||
                      to_char(SYSDATE, 'DD-MON-YYYY HH24:MI:SS') ||
                      ' for ' || l_sec);
    fnd_file.put_line(fnd_file.output
                     ,'End time:  ' ||
                      to_char(SYSDATE, 'DD-MON-YYYY HH24:MI:SS') ||
                      ' for ' || l_sec);

    --Insert audit table
    INSERT INTO xxcus.xxcus_rebate_aud_tbl
    VALUES
      (nvl(fnd_global.conc_request_id, 0)
      ,l_procedure
      ,SYSDATE
      ,nvl(fnd_global.user_name, 'NA')
      ,nvl(fnd_global.resp_name, 'NA')
      ,p_fperiod
      ,l_fiscal_per_id
      ,'Beginning Program - product load'
      ,SYSDATE
      ,NULL --l_count
      ,NULL
      ,p_bu_nm);

    COMMIT;

    --Version 1.7  8/7/13
    --added to stop process from receiving the error
    --ORA-00054: resource busy and acquire with NOWAIT specified or timeout expired
    --Version 1.10
    --EXECUTE IMMEDIATE 'alter session set ddl_lock_timeout=360 ';
    EXECUTE IMMEDIATE 'alter session set ddl_lock_timeout=' || l_waittime;

    --Clear contents of history table only hold last run

    l_sec := 'Truncate History partition for';
    fnd_file.put_line(fnd_file.log
                     ,'Start time:  ' ||
                      to_char(SYSDATE, 'DD-MON-YYYY HH24:MI:SS') ||
                      ' for ' || l_sec);
    fnd_file.put_line(fnd_file.output
                     ,'Start time:  ' ||
                      to_char(SYSDATE, 'DD-MON-YYYY HH24:MI:SS') ||
                      ' for ' || l_sec);
    BEGIN

      EXECUTE IMMEDIATE 'alter table xxcus.xxcus_rebate_product_hist_tbl truncate partition ' ||
                        l_partition;

      fnd_file.put_line(fnd_file.log
                       ,'End time:  ' ||
                        to_char(SYSDATE, 'DD-MON-YYYY HH24:MI:SS') ||
                        ' for ' || l_sec);
      fnd_file.put_line(fnd_file.output
                       ,'End time:  ' ||
                        to_char(SYSDATE, 'DD-MON-YYYY HH24:MI:SS') ||
                        ' for ' || l_sec);
      --Version 1.7 8/7/13
      -- DELETE FROM xxcus.xxcus_rebate_product_hist_tbl
      --   WHERE bu_nm = l_oracle_bu_nm; --Version 1.6   p_bu_nm

    EXCEPTION
      WHEN OTHERS THEN
        l_err_msg := 'Failed to truncate partition ' || l_partition ||
                     ' on xxcus_rebate_product_hist_tbl for BU: ' ||
                     p_bu_nm || ' - ' || SQLERRM;
        fnd_file.put_line(fnd_file.log, l_err_msg);
        RAISE program_error;
    END;

    --Load table to compare data for the next run
    BEGIN
      INSERT /*+ APPEND */
      INTO xxcus.xxcus_rebate_product_hist_tbl
        SELECT *
          FROM xxcus.xxcus_rebate_product_curr_tbl
         WHERE bu_nm = l_oracle_bu_nm; --Version 1.6   p_bu_nm;

      COMMIT;

      l_sec := 'Loaded the backup table data from last run for comparison.';
      fnd_file.put_line(fnd_file.log
                       ,'End time:  ' ||
                        to_char(SYSDATE, 'DD-MON-YYYY HH24:MI:SS') ||
                        ' for ' || l_sec);
      fnd_file.put_line(fnd_file.output
                       ,'End time:  ' ||
                        to_char(SYSDATE, 'DD-MON-YYYY HH24:MI:SS') ||
                        ' for ' || l_sec);

    END;

    --Clear contents of staging table for BU
    BEGIN

      l_sec := 'Tuncate xxcus_rebate_product_curr_tbl table by BU.';
      fnd_file.put_line(fnd_file.log
                       ,'Start time:  ' ||
                        to_char(SYSDATE, 'DD-MON-YYYY HH24:MI:SS') ||
                        ' for ' || l_sec);
      fnd_file.put_line(fnd_file.output
                       ,'Start time:  ' ||
                        to_char(SYSDATE, 'DD-MON-YYYY HH24:MI:SS') ||
                        ' for ' || l_sec);
      --Version 1.7 8/26/13
      -- EXECUTE IMMEDIATE 'alter table xxcus.xxcus_rebate_product_curr_tbl truncate partition ' ||
      --                   l_partition;

      --Version 1.8 8/26/2013
      DELETE FROM xxcus.xxcus_rebate_product_curr_tbl p
       WHERE bu_nm = l_oracle_bu_nm
         AND p.sku_cd || '~' || p.bu_nm || '~' || p.src_sys_nm --Version 1.9  9/17/13 added src_sys_nm
             IN (SELECT description
                   FROM inv.mtl_system_items_b
                  WHERE organization_id = l_org_id
                    AND description =
                        p.sku_cd || '~' || p.bu_nm || '~' || p.src_sys_nm --Version 1.9  9/17/13 added src_sys_nm
                 );

    EXCEPTION
      WHEN OTHERS THEN
        l_err_msg := 'Failed to delete xxcus_rebate_product_curr_tbl for BU ' ||
                     l_oracle_bu_nm || ' - ' || SQLERRM;
        fnd_file.put_line(fnd_file.log, l_err_msg);
        RAISE program_error;
    END;

    fnd_file.put_line(fnd_file.log
                     ,'End time:  ' ||
                      to_char(SYSDATE, 'DD-MON-YYYY HH24:MI:SS') ||
                      ' for ' || l_sec);
    fnd_file.put_line(fnd_file.output
                     ,'End time:  ' ||
                      to_char(SYSDATE, 'DD-MON-YYYY HH24:MI:SS') ||
                      ' for ' || l_sec);

    -- ready to load data into staging table
    l_sec := 'Inserting table xxcus_rebate_product_curr_tbl with data from SDW.';
    fnd_file.put_line(fnd_file.log
                     ,'Start time:  ' ||
                      to_char(SYSDATE, 'DD-MON-YYYY HH24:MI:SS') ||
                      ' for ' || l_sec);
    fnd_file.put_line(fnd_file.output
                     ,'Start time:  ' ||
                      to_char(SYSDATE, 'DD-MON-YYYY HH24:MI:SS') ||
                      ' for ' || l_sec);
    INSERT /*+ APPEND */
    INTO xxcus.xxcus_rebate_product_curr_tbl
      (SELECT geo_loc_hier_lob_nm
             ,flv.description --Version 1.6  bu_nm
             ,src_sys_nm
             ,prod_id
             ,sku_cd
             ,sku_desc_lng
             ,vndr_part_nbr
             ,nvl(prod_purch_uom, 'UNKNOWN')
             ,po_repl_cost
             ,prim_upc
             --,REPLACE(secondary_upc, '?, NULL)  --Ver 2.9 --Nothing wrong with the line. When we compiled last time it was using TOAD. With Xpatch the native sqlplus in linux can't interpret the non US ASCII character
             ,REPLACE(secondary_upc,'([^[:print:]])', NULL) --ver 2.9 --Nothing wrong with the comemnted
             ,tertiary_upc
             ,unspsc_cd
             ,as_of_dt
             ,NULL
             ,'N'
             ,NULL
         FROM xxcus.xxcus_rebate_product_sdw_tbl
             ,apps.fnd_lookup_values flv --Version 1.6
        WHERE bu_nm = p_bu_nm
          AND flv.lookup_type = 'XXCUS_REBATE_BU_XREF' --Version 1.6
          AND flv.enabled_flag = 'Y' --Version 1.6
          AND nvl(flv.end_date_active, SYSDATE) >= SYSDATE --Version 1.6
          AND flv.meaning = bu_nm);

    COMMIT;

    fnd_file.put_line(fnd_file.log
                     ,'End time:  ' ||
                      to_char(SYSDATE, 'DD-MON-YYYY HH24:MI:SS') ||
                      ' for ' || l_sec);
    fnd_file.put_line(fnd_file.output
                     ,'End time:  ' ||
                      to_char(SYSDATE, 'DD-MON-YYYY HH24:MI:SS') ||
                      ' for ' || l_sec);

    --start loading products for DW
    BEGIN

      --Create output file
      l_sec := 'Start new items loading.';
      fnd_file.put_line(fnd_file.log, l_sec);
      fnd_file.put_line(fnd_file.output, l_sec);

      UPDATE xxcus.xxcus_rebate_aud_tbl
         SET stage = l_sec, stg_date = SYSDATE
       WHERE (conc_request_id = nvl(fnd_global.conc_request_id, 0) OR
             trunc(processed_datetime) = trunc(SYSDATE))
         AND procedure_name = l_procedure
         AND fiscal_period = p_fperiod
         AND post_run_id = l_fiscal_per_id
         AND bu_nm = p_bu_nm;
      COMMIT;

      --loading item and category interface for BU
      l_sec := 'Loading new items/category into interface.';
      fnd_file.put_line(fnd_file.log
                       ,'Start time:  ' ||
                        to_char(SYSDATE, 'DD-MON-YYYY HH24:MI:SS') ||
                        ' for ' || l_sec);
      fnd_file.put_line(fnd_file.output
                       ,'Start time:  ' ||
                        to_char(SYSDATE, 'DD-MON-YYYY HH24:MI:SS') ||
                        ' for ' || l_sec);
      --      FOR i IN 1 .. 50
      --      LOOP

      SELECT mtl_system_items_intf_sets_s.nextval
        INTO l_set_process_id
        FROM dual;

      --Version 2.4  10/21/2014 added loop to get only one distict sku for bu
      FOR c_item IN (SELECT DISTINCT bu_nm
                                    ,nvl(p.sku_cd, 'UNKNOWN') sku_cd
                                    ,src_sys_nm
                       FROM xxcus.xxcus_rebate_product_curr_tbl p
                      WHERE p.status_flag = 'N'
                        AND p.bu_nm = l_oracle_bu_nm
                        AND p.sku_cd || '~' || p.bu_nm || '~' ||
                            p.src_sys_nm NOT IN
                            (SELECT description
                               FROM inv.mtl_system_items_b
                              WHERE organization_id = l_org_id
                                AND description = p.sku_cd || '~' || p.bu_nm || '~' ||
                                    p.src_sys_nm))
      LOOP

        FOR c_item_number_rec IN (SELECT p.prod_id
                                        ,nvl(p.sku_cd, 'UNKNOWN') sku_cd
                                        ,p.sku_desc_lng
                                        ,(CASE
                                           WHEN p.vndr_part_nbr IN
                                                ('UNKNOWN', '-') THEN
                                            NULL
                                           ELSE
                                            p.vndr_part_nbr
                                         END) vndr_part_nbr
                                        ,p.po_repl_cost
                                        ,p.prod_purch_uom
                                        ,p.unspsc_cd
                                        ,p.bu_nm
                                        ,p.geo_loc_hier_lob_nm
                                        ,p.status_flag
                                        ,p.src_sys_nm
                                        ,p.prim_upc
                                        ,p.secondary_upc
                                        ,p.tertiary_upc
                                        ,oracle_uom_code
                                        ,p.as_of_dt
                                    FROM xxcus.xxcus_rebate_product_curr_tbl p
                                        ,xxcus.xxcus_rebate_uom_tbl          xref
                                  -- ,hdsoracle.hds_rebate_product_class_upld@apxprd_lnk upld   --Version 1.1
                                   WHERE p.status_flag = 'N'
                                        --AND p.bu_nm = l_oracle_bu_nm --Version 1.6  p_bu_nm   --Version 2.4
                                     AND p.bu_nm = c_item.bu_nm --Version 2.4
                                     AND p.src_sys_nm = c_item.src_sys_nm --Version 2.4
                                     AND p.sku_cd = c_item.sku_cd --Version 2.4
                                     AND p.prod_purch_uom =
                                         xref.purch_uom_cd --only items with a valid uom
                                     AND p.geo_loc_hier_lob_nm = xref.lob_nm
                                     AND p.bu_nm = xref.bu_nm
                                     AND upper(p.src_sys_nm) =
                                         upper(xref.src_sys_nm)
                                     AND p.as_of_dt IN --Version 2.4
                                         (SELECT MAX(as_of_dt)
                                            FROM xxcus.xxcus_rebate_product_curr_tbl x
                                           WHERE x.status_flag = 'N'
                                             AND x.bu_nm = p.bu_nm
                                             AND x.src_sys_nm = p.src_sys_nm
                                             AND x.sku_cd = p.sku_cd)
                                        --AND p.bu_nm = upld.bu_nm(+)            --Version 1.1
                                        --AND p.sku_cd = upld.sku_cd(+)          --Version 1.1
                                        --AND upld.processed(+) = 'N'            --Version 1.1
                                        --AND rownum < 10001
                                     AND p.sku_cd || '~' || p.bu_nm || '~' ||
                                         p.src_sys_nm --Version 1.9 9/17/13 added src_sys_nm
                                         NOT IN
                                         (SELECT description
                                            FROM inv.mtl_system_items_b
                                           WHERE organization_id = l_org_id
                                             AND description =
                                                 p.sku_cd || '~' || p.bu_nm || '~' ||
                                                 p.src_sys_nm --Version 1.9 9/17/13 added src_sys_nm
                                          )
                                     AND rownum = 1) --Version 2.4 needed to add because data pushed from SDW could cause the same date
        LOOP

          SELECT mtl_system_items_b_s.nextval
            INTO l_item_sequence
            FROM dual;

          INSERT INTO inv.mtl_system_items_interface
            (organization_id
            ,set_process_id
            ,transaction_type
            ,process_flag
            ,summary_flag
            ,segment1
            ,description
            ,primary_uom_code
            ,attribute1 --LOB
            ,attribute2 --prod_id from SDW
            ,attribute3 --vndr_part_nbr
            ,attribute4 --unspsc_cd
            ,attribute5 --prim_upc
            ,attribute6 --sku_desc_lng
            ,attribute7 --source_system    --Version 1.9   9/17/13
            ,attribute8 --sku_cd           --Version 1.9   9/17/13
            ,inventory_item_status_code
            ,allowed_units_lookup_code
            ,last_update_date
            ,last_updated_by
            ,inventory_asset_flag
            ,revision_qty_control_code
            ,revision
            ,purchasing_enabled_flag
            ,stock_enabled_flag
            ,mtl_transactions_enabled_flag
            ,must_use_approved_vendor_flag
            ,purchasing_item_flag
            ,customer_order_enabled_flag
            ,inventory_item_flag
            ,reservable_type
            ,bom_item_type
            ,bom_enabled_flag
            ,effectivity_control
            ,costing_enabled_flag
            ,enabled_flag
            ,item_type)
          VALUES
            (l_org_id
            ,l_set_process_id
            ,'CREATE'
            ,1
            ,'N'
            ,l_item_sequence || '~RBT'
            , --c_item_number_rec.sku_cd || '~' || c_item_number_rec.bu_nm,
             c_item_number_rec.sku_cd || '~' || c_item_number_rec.bu_nm || '~' ||
             c_item_number_rec.src_sys_nm --Version 1.9  9/17/13
            , --c_item_number_rec.sku_desc_lng,
             c_item_number_rec.oracle_uom_code
            ,c_item_number_rec.geo_loc_hier_lob_nm
            ,c_item_number_rec.prod_id
            , --|| '~' || c_item_number_rec.bu_nm || '~' ||c_item_number_rec.src_sys_nm,
             c_item_number_rec.vndr_part_nbr
            ,c_item_number_rec.unspsc_cd
            ,c_item_number_rec.prim_upc
            ,substr(c_item_number_rec.sku_desc_lng, 1, 240)
            ,c_item_number_rec.src_sys_nm --Version 1.9  9/17/13
            ,c_item_number_rec.sku_cd --Version 1.9  9/17/13
            ,'Active'
            ,3
            ,SYSDATE
            ,l_user_id
            ,'N'
            ,1
            ,0
            ,'N'
            ,'N'
            ,'N'
            ,'N'
            ,'Y'
            ,'N'
            ,'N'
            ,1
            ,4
            ,'Y'
            ,1
            ,'N'
            ,'Y'
            ,'P' --Purchased
             );

          INSERT INTO mtl_item_categories_interface
            (item_number
            ,organization_id
            ,transaction_type
            ,category_set_id
            ,category_id
            ,process_flag
            ,set_process_id)
          VALUES
            (l_item_sequence || '~RBT' --c_item_number_rec.sku_cd || '~' || c_item_number_rec.bu_nm,
            ,l_org_id
            ,'CREATE'
            ,l_category_set -- HDS Supplier Category
            ,l_category_number --Version 1.1 nvl(c_item_number_rec.category_id, 1123)
            ,1
            ,l_set_process_id);

          UPDATE xxcus.xxcus_rebate_product_curr_tbl
             SET status_flag  = 'P'
                ,request_id   = nvl(fnd_global.conc_request_id, 0)
                ,process_date = SYSDATE
           WHERE --prod_id = c_item_number_rec.prod_id    --Version  10/21/14
           status_flag = 'N' --Version  10/21/14
           AND sku_cd = c_item_number_rec.sku_cd
           AND bu_nm = c_item_number_rec.bu_nm
           AND src_sys_nm = c_item_number_rec.src_sys_nm --Version 1.9  9/17/13
          ;
          COMMIT;
        END LOOP;
      END LOOP;

      fnd_file.put_line(fnd_file.log
                       ,'End time:  ' ||
                        to_char(SYSDATE, 'DD-MON-YYYY HH24:MI:SS') ||
                        ' for ' || l_sec);
      fnd_file.put_line(fnd_file.output
                       ,'End time:  ' ||
                        to_char(SYSDATE, 'DD-MON-YYYY HH24:MI:SS') ||
                        ' for ' || l_sec);
    END; --end inserting new items

    BEGIN
      --start of items to be updated
      l_sec := 'Loading items/category for update into interface.';
      fnd_file.put_line(fnd_file.log
                       ,'Start time:  ' ||
                        to_char(SYSDATE, 'DD-MON-YYYY HH24:MI:SS') ||
                        ' for ' || l_sec);
      fnd_file.put_line(fnd_file.output
                       ,'Start time:  ' ||
                        to_char(SYSDATE, 'DD-MON-YYYY HH24:MI:SS') ||
                        ' for ' || l_sec);

      UPDATE xxcus.xxcus_rebate_aud_tbl
         SET stage = l_sec, stg_date = SYSDATE
       WHERE (conc_request_id = nvl(fnd_global.conc_request_id, 0) OR
             trunc(processed_datetime) = trunc(SYSDATE))
         AND procedure_name = l_procedure
         AND fiscal_period = p_fperiod
         AND post_run_id = l_fiscal_per_id
         AND bu_nm = p_bu_nm;
      COMMIT;

      --      FOR i IN 1 .. 50
      --      LOOP

      SELECT mtl_system_items_intf_sets_s.nextval
        INTO l_set_process_id
        FROM dual;

      FOR c_update IN (SELECT /*+ PARALLEL (AUTO) */ --Version 1.10
                        inventory_item_id
                       ,description
                       ,segment1
                       ,attribute1
                       ,attribute2
                       ,attribute3
                       ,attribute4
                       ,attribute5
                       ,attribute6
                       ,geo_loc_hier_lob_nm
                       ,bu_nm
                       ,prod_id
                       ,sku_cd
                       ,substr(sku_desc_lng, 1, 240) sku_desc_lng
                       ,vndr_part_nbr
                       ,prim_upc
                       ,unspsc_cd
                       ,src_sys_nm --Version 1.9  9/17/13
                         FROM inv.mtl_system_items_b              i
                             ,xxcus.xxcus_rebate_product_curr_tbl p
                        WHERE organization_id = l_org_id
                          AND p.bu_nm = l_oracle_bu_nm --Version 1.6   p_bu_nm
                          AND p.status_flag <> 'P'
                          AND i.description =
                              sku_cd || '~' || bu_nm || '~' || src_sys_nm --Version 1.9   9/17/13
                          AND i.attribute1 = geo_loc_hier_lob_nm
                          AND (i.attribute2 <> prod_id OR
                              i.attribute3 <> vndr_part_nbr OR
                              i.attribute4 <> unspsc_cd OR
                              i.attribute5 <> prim_upc OR
                              i.attribute6 <> substr(sku_desc_lng, 1, 240))
                       --AND rownum < 10001
                       )
      LOOP

        --check if sku, description or vendor part number has been changed then update item

        IF c_update.prod_id <> c_update.attribute2 OR
           c_update.sku_desc_lng <> c_update.attribute6 OR
           c_update.vndr_part_nbr <> c_update.attribute3 OR
           c_update.unspsc_cd <> c_update.attribute4 OR
           c_update.prim_upc <> c_update.attribute5
        THEN

          INSERT INTO inv.mtl_system_items_interface
            (inventory_item_id
            ,organization_id
            ,set_process_id
            ,transaction_type
            ,process_flag
            ,summary_flag
            ,description
            ,attribute2
            ,attribute3
            ,attribute4
            ,attribute5
            ,attribute6
            ,attribute7 --Version 1.9   9/17/13
            ,attribute8 --Version 1.9   9/17/13
            ,inventory_item_status_code
            ,allowed_units_lookup_code
            ,last_update_date
            ,last_updated_by
            ,enabled_flag)
          VALUES
            (c_update.inventory_item_id
            ,l_org_id
            ,l_set_process_id
            ,'UPDATE'
            ,1
            ,'N'
            ,c_update.sku_cd || '~' || c_update.bu_nm || '~' ||
             c_update.src_sys_nm --Version 1.9   9/17/13 added src_sys_nm
            ,c_update.prod_id
            ,c_update.vndr_part_nbr
            ,c_update.unspsc_cd
            ,c_update.prim_upc
            ,c_update.sku_desc_lng
            ,c_update.src_sys_nm --Version 1.9   9/17/13
            ,c_update.sku_cd --Version 1.9   9/17/13
            ,'Active'
            ,3
            ,SYSDATE
            ,l_user_id
            ,'Y');

          UPDATE xxcus.xxcus_rebate_product_curr_tbl
             SET status_flag  = 'P'
                ,request_id   = nvl(fnd_global.conc_request_id, 0)
                ,process_date = SYSDATE
           WHERE prod_id = c_update.prod_id
             AND sku_cd = c_update.sku_cd
             AND bu_nm = c_update.bu_nm
             AND src_sys_nm = c_update.src_sys_nm --Version 1.9   9/17/13
          ;

          COMMIT;
        END IF;

        COMMIT;
      END LOOP;
      --      END LOOP;

      fnd_file.put_line(fnd_file.log
                       ,'End time:  ' ||
                        to_char(SYSDATE, 'DD-MON-YYYY HH24:MI:SS') ||
                        ' for ' || l_sec);
      fnd_file.put_line(fnd_file.output
                       ,'End time:  ' ||
                        to_char(SYSDATE, 'DD-MON-YYYY HH24:MI:SS') ||
                        ' for ' || l_sec);

      UPDATE xxcus.xxcus_rebate_aud_tbl
         SET stage = l_sec, stg_date = SYSDATE
       WHERE (conc_request_id = nvl(fnd_global.conc_request_id, 0) OR
             trunc(processed_datetime) = trunc(SYSDATE))
         AND procedure_name = l_procedure
         AND fiscal_period = p_fperiod
         AND post_run_id = l_fiscal_per_id
         AND bu_nm = p_bu_nm;
      COMMIT;

    END;

    --*******************************************
    --Submit the item import process
    --*******************************************
    BEGIN
      l_sec := 'Start item/category import; ';
      fnd_file.put_line(fnd_file.log
                       ,'Start time:  ' ||
                        to_char(SYSDATE, 'DD-MON-YYYY HH24:MI:SS') ||
                        ' for ' || l_sec);
      fnd_file.put_line(fnd_file.output
                       ,'Start time:  ' ||
                        to_char(SYSDATE, 'DD-MON-YYYY HH24:MI:SS') ||
                        ' for ' || l_sec);

      l_count := 0;

      SELECT COUNT(*)
        INTO l_count
        FROM inv.mtl_system_items_interface
       WHERE organization_id = l_org_id;

      IF l_count > 0
      THEN

        FOR c_interface IN (SELECT DISTINCT set_process_id
                                           ,decode(transaction_type
                                                  ,'CREATE'
                                                  ,1
                                                  ,'UPDATE'
                                                  ,2) transaction_type
                              FROM inv.mtl_system_items_interface
                             WHERE organization_id = l_org_id)
        LOOP

          --Submit Import Process under XXCUS_CON responsitility
          l_sec := 'Submitting Import Items process.';
          fnd_file.put_line(fnd_file.log, l_sec);
          fnd_file.put_line(fnd_file.output, l_sec);

          UPDATE xxcus.xxcus_rebate_aud_tbl
             SET stage = l_sec, stg_date = SYSDATE
           WHERE (conc_request_id = nvl(fnd_global.conc_request_id, 0) OR
                 trunc(processed_datetime) = trunc(SYSDATE))
             AND procedure_name = l_procedure
             AND fiscal_period = p_fperiod
             AND post_run_id = l_fiscal_per_id
             AND bu_nm = p_bu_nm;
          COMMIT;

          --Submite the Concurrent Item Import
          l_req_id := fnd_request.submit_request(application => 'INV'
                                                ,program     => 'INCOIN'
                                                ,description => NULL
                                                ,start_time  => SYSDATE
                                                ,sub_request => FALSE
                                                ,argument1   => l_org_id
                                                ,argument2   => 1
                                                ,argument3   => 1
                                                ,argument4   => 1
                                                ,argument5   => 1
                                                ,argument6   => c_interface.set_process_id
                                                ,argument7   => c_interface.transaction_type);

          COMMIT;

          fnd_file.put_line(fnd_file.log, l_req_id);
          fnd_file.put_line(fnd_file.output, l_req_id);

          --Write detail Line Log rows
          l_sec := 'Waiting for Import Items to finish ' || l_req_id;
          fnd_file.put_line(fnd_file.log, l_sec);
          fnd_file.put_line(fnd_file.output, l_sec);

          UPDATE xxcus.xxcus_rebate_aud_tbl
             SET stage = l_sec, stg_date = SYSDATE
           WHERE (conc_request_id = nvl(fnd_global.conc_request_id, 0) OR
                 trunc(processed_datetime) = trunc(SYSDATE))
             AND procedure_name = l_procedure
             AND fiscal_period = p_fperiod
             AND post_run_id = l_fiscal_per_id
             AND bu_nm = p_bu_nm;
          COMMIT;

          --Wait for Item Import
          IF (l_req_id != 0)
          THEN
            IF fnd_concurrent.wait_for_request(l_req_id
                                              ,v_interval
                                              ,v_max_time
                                              ,v_phase
                                              ,v_status
                                              ,v_dev_phase
                                              ,v_dev_status
                                              ,v_message)
            THEN
              v_error_message := 'ReqID=' || l_req_id || ' DPhase ' ||
                                 v_dev_phase || ' DStatus ' || v_dev_status ||
                                 ' MSG ' || v_message;

              IF v_dev_phase != 'COMPLETE' OR v_dev_status != 'NORMAL'
              THEN
                l_sec := 'An error occured in Import Items for BU Name:  ' ||
                         p_bu_nm ||
                         ' please review the Log for concurrent request ' ||
                         l_req_id || ' - ' || v_error_message || '.';
                fnd_file.put_line(fnd_file.log, l_sec);
                fnd_file.put_line(fnd_file.output, l_sec);
                --RAISE program_error;
                /*
                             --look for items or categories that are in interface with error
                             l_sec := 'Call for item iface errors notification; ';
                             fnd_file.put_line(fnd_file.log
                                              ,'Start time:  ' ||
                                               to_char(SYSDATE
                                                      ,'DD-MON-YYYY HH24:MI:SS') ||
                                               ' for ' || l_sec);
                             fnd_file.put_line(fnd_file.output
                                              ,'Start time:  ' ||
                                               to_char(SYSDATE
                                                      ,'DD-MON-YYYY HH24:MI:SS') ||
                                               ' for ' || l_sec);

                             --call to check if any product wasn't setup because of missing uom xref
                             item_iface_errors(p_bu_nm
                                              ,l_req_id
                                              ,c_interface.set_process_id);

                             fnd_file.put_line(fnd_file.log
                                              ,'End time:  ' ||
                                               to_char(SYSDATE
                                                      ,'DD-MON-YYYY HH24:MI:SS') ||
                                               ' for ' || l_sec);
                             fnd_file.put_line(fnd_file.output
                                              ,'End time:  ' ||
                                               to_char(SYSDATE
                                                      ,'DD-MON-YYYY HH24:MI:SS') ||
                                               ' for ' || l_sec);
                */
              ELSE
                retcode := 0;
              END IF;
            ELSE
              l_sec := 'An error occured in Import Items for BU Name:  ' ||
                       p_bu_nm ||
                       ' please review the Log for concurrent request ' ||
                       l_req_id || ' - ' || v_error_message || '.';
              fnd_file.put_line(fnd_file.log, l_sec);
              fnd_file.put_line(fnd_file.output, l_sec);
              --RAISE program_error;
            END IF;
          ELSE
            l_sec := 'An error occured when trying to submit Import Items for BU Name:  ' ||
                     p_bu_nm ||
                     ' the concurrent request was not submitted.';
            fnd_file.put_line(fnd_file.log, l_sec);
            fnd_file.put_line(fnd_file.output, l_sec);
            RAISE program_error;
          END IF;

          IF c_interface.transaction_type = 1
          THEN
            --Submit Item Category Assignment Open Interface under XXCUS_CON responsitility  10/29/13 moved so import runs following items
            l_sec := 'Submitting Item Category Assignment Open Interface process.';
            fnd_file.put_line(fnd_file.log, l_sec);
            fnd_file.put_line(fnd_file.output, l_sec);

            UPDATE xxcus.xxcus_rebate_aud_tbl
               SET stage = l_sec, stg_date = SYSDATE
             WHERE (conc_request_id = nvl(fnd_global.conc_request_id, 0) OR
                   trunc(processed_datetime) = trunc(SYSDATE))
               AND procedure_name = l_procedure
               AND fiscal_period = p_fperiod
               AND post_run_id = l_fiscal_per_id
               AND bu_nm = p_bu_nm;
            COMMIT;

            --Submite the Concurrent Item Category Import
            l_req_id := fnd_request.submit_request(application => 'INV'
                                                  ,program     => 'INV_ITEM_CAT_ASSIGN_OI'
                                                  ,description => NULL
                                                  ,start_time  => SYSDATE
                                                  ,sub_request => FALSE
                                                  ,argument1   => c_interface.set_process_id
                                                  ,argument2   => 1
                                                  ,argument3   => 1);

            COMMIT;

            fnd_file.put_line(fnd_file.log
                             ,'Item Category Assignment request:  ' ||
                              l_req_id);
            fnd_file.put_line(fnd_file.output
                             ,'Item Category Assignment request:  ' ||
                              l_req_id);

            --Write detail Line Log rows
            l_sec := 'Waiting for Category Import to finish for BU Name:  ' ||
                     p_bu_nm || ' Request ID:  ' || l_req_id;
            fnd_file.put_line(fnd_file.log, l_sec);
            fnd_file.put_line(fnd_file.output, l_sec);

            --Wait for Item Import
            IF (l_req_id != 0)
            THEN
              IF fnd_concurrent.wait_for_request(l_req_id
                                                ,v_interval
                                                ,v_max_time
                                                ,v_phase
                                                ,v_status
                                                ,v_dev_phase
                                                ,v_dev_status
                                                ,v_message)
              THEN
                v_error_message := 'ReqID=' || l_req_id || ' DPhase ' ||
                                   v_dev_phase || ' DStatus ' ||
                                   v_dev_status || ' MSG ' || v_message;

                IF v_dev_phase != 'COMPLETE' OR v_dev_status != 'NORMAL'
                THEN
                  l_sec := 'An error occured in Import Categories for BU Name:  ' ||
                           p_bu_nm ||
                           ' please review the Log for concurrent request ' ||
                           l_req_id || ' - ' || v_error_message || '.';
                  fnd_file.put_line(fnd_file.log, l_sec);
                  fnd_file.put_line(fnd_file.output, l_sec);
                  --RAISE program_error;
                ELSE
                  retcode := 0;
                END IF;
              ELSE
                l_sec := 'An error occured in Import Categories
              for BU Name:  ' || p_bu_nm ||
                         '  please review the Log for concurrent request ' ||
                         l_req_id || ' - ' || v_error_message || '.';
                fnd_file.put_line(fnd_file.log, l_sec);
                fnd_file.put_line(fnd_file.output, l_sec);
                --RAISE program_error;
              END IF;
            ELSE
              l_sec := 'An error occured when trying to submit Import Categories for BU Name:  ' ||
                       p_bu_nm ||
                       ' the concurrent request was not submitted.';
              fnd_file.put_line(fnd_file.log, l_sec);
              fnd_file.put_line(fnd_file.output, l_sec);
              --RAISE program_error;
            END IF;
          END IF;
        END LOOP;

        --*******************************************
        --Submit the category import process
        --*******************************************
        /*
        FOR c_category IN (SELECT DISTINCT set_process_id
                             FROM mtl_item_categories_interface
                            WHERE organization_id = l_org_id)
        LOOP

          --Submit Import Process under XXCUS_CON responsitility
          l_sec := 'Submitting Item Category Assignment Open Interface process.';
          fnd_file.put_line(fnd_file.log, l_sec);
          fnd_file.put_line(fnd_file.output, l_sec);

          UPDATE xxcus.xxcus_rebate_aud_tbl
             SET stage = l_sec, stg_date = SYSDATE
           WHERE (conc_request_id = nvl(fnd_global.conc_request_id, 0) OR
                 trunc(processed_datetime) = trunc(SYSDATE))
             AND procedure_name = l_procedure
             AND fiscal_period = p_fperiod
             AND post_run_id = l_fiscal_per_id
             AND bu_nm = p_bu_nm;
          COMMIT;

          --Submite the Concurrent Item Category Import
          l_req_id := fnd_request.submit_request(application => 'INV'
                                                ,program     => 'INV_ITEM_CAT_ASSIGN_OI'
                                                ,description => NULL
                                                ,start_time  => SYSDATE
                                                ,sub_request => FALSE
                                                ,argument1   => c_category.set_process_id
                                                ,argument2   => 1
                                                ,argument3   => 1);

          COMMIT;

          fnd_file.put_line(fnd_file.log, l_req_id);
          fnd_file.put_line(fnd_file.output, l_req_id);

          --Write detail Line Log rows
          l_sec := 'Waiting for Import Items to finish for BU Name:  ' ||
                   p_bu_nm || ' Request ID:  ' || l_req_id;
          fnd_file.put_line(fnd_file.log, l_sec);
          fnd_file.put_line(fnd_file.output, l_sec);

          --Wait for Item Import
          IF (l_req_id != 0)
          THEN
            IF fnd_concurrent.wait_for_request(l_req_id
                                              ,v_interval
                                              ,v_max_time
                                              ,v_phase
                                              ,v_status
                                              ,v_dev_phase
                                              ,v_dev_status
                                              ,v_message)
            THEN
              v_error_message := 'ReqID=' || l_req_id || ' DPhase ' ||
                                 v_dev_phase || ' DStatus ' || v_dev_status ||
                                 ' MSG ' || v_message;

              IF v_dev_phase != 'COMPLETE' OR v_dev_status != 'NORMAL'
              THEN
                l_sec := 'An error occured in Import Categories for BU Name:  ' ||
                         p_bu_nm ||
                         ' please review the Log for concurrent request ' ||
                         l_req_id || ' - ' || v_error_message || '.';
                fnd_file.put_line(fnd_file.log, l_sec);
                fnd_file.put_line(fnd_file.output, l_sec);
                --RAISE program_error;
              ELSE
                retcode := 0;
              END IF;
            ELSE
              l_sec := 'An error occured in Import Categories
              for BU Name:  ' || p_bu_nm ||
                       '  please review the Log for concurrent request ' ||
                       l_req_id || ' - ' || v_error_message || '.';
              fnd_file.put_line(fnd_file.log, l_sec);
              fnd_file.put_line(fnd_file.output, l_sec);
              --RAISE program_error;
            END IF;
          ELSE
            l_sec := 'An error occured when trying to submit Import Categories for BU Name:  ' ||
                     p_bu_nm ||
                     ' the concurrent request was not submitted.';
            fnd_file.put_line(fnd_file.log, l_sec);
            fnd_file.put_line(fnd_file.output, l_sec);
            RAISE program_error;
          END IF;
        END LOOP;
        */
      END IF;
    END;

    fnd_file.put_line(fnd_file.log
                     ,'End time:  ' ||
                      to_char(SYSDATE, 'DD-MON-YYYY HH24:MI:SS') ||
                      ' for ' || l_sec);
    fnd_file.put_line(fnd_file.output
                     ,'End time:  ' ||
                      to_char(SYSDATE, 'DD-MON-YYYY HH24:MI:SS') ||
                      ' for ' || l_sec);

    UPDATE xxcus.xxcus_rebate_aud_tbl
       SET stage = l_sec, stg_date = SYSDATE
     WHERE (conc_request_id = nvl(fnd_global.conc_request_id, 0) OR
           trunc(processed_datetime) = trunc(SYSDATE))
       AND procedure_name = l_procedure
       AND fiscal_period = p_fperiod
       AND post_run_id = l_fiscal_per_id
       AND bu_nm = p_bu_nm;

    COMMIT;

    l_sec := 'Call for UOM notification; ';
    fnd_file.put_line(fnd_file.log
                     ,'Start time:  ' ||
                      to_char(SYSDATE, 'DD-MON-YYYY HH24:MI:SS') ||
                      ' for ' || l_sec);
    fnd_file.put_line(fnd_file.output
                     ,'Start time:  ' ||
                      to_char(SYSDATE, 'DD-MON-YYYY HH24:MI:SS') ||
                      ' for ' || l_sec);

    --call to check if any product wasn't setup because of missing uom xref
    uom_exceptions(l_oracle_bu_nm);

    fnd_file.put_line(fnd_file.log
                     ,'End time:  ' ||
                      to_char(SYSDATE, 'DD-MON-YYYY HH24:MI:SS') ||
                      ' for ' || l_sec);
    fnd_file.put_line(fnd_file.output
                     ,'End time:  ' ||
                      to_char(SYSDATE, 'DD-MON-YYYY HH24:MI:SS') ||
                      ' for ' || l_sec);

    --need to truncate the partition for the SDW staging table for the next run for the BU
    BEGIN

      l_sec := 'Truncate partition in xxcus_rebate_product_sdw_tbl table by BU.';
      fnd_file.put_line(fnd_file.log
                       ,'Start time:  ' ||
                        to_char(SYSDATE, 'DD-MON-YYYY HH24:MI:SS') ||
                        ' for ' || l_sec);
      fnd_file.put_line(fnd_file.output
                       ,'Start time:  ' ||
                        to_char(SYSDATE, 'DD-MON-YYYY HH24:MI:SS') ||
                        ' for ' || l_sec);

      EXECUTE IMMEDIATE 'alter table xxcus.xxcus_rebate_product_sdw_tbl truncate partition ' ||
                        l_partition_sdw;

      fnd_file.put_line(fnd_file.log
                       ,'End time:  ' ||
                        to_char(SYSDATE, 'DD-MON-YYYY HH24:MI:SS') ||
                        ' for ' || l_sec);
      fnd_file.put_line(fnd_file.output
                       ,'End time:  ' ||
                        to_char(SYSDATE, 'DD-MON-YYYY HH24:MI:SS') ||
                        ' for ' || l_sec);

    EXCEPTION
      WHEN OTHERS THEN
        l_err_msg := 'Failed to truncate partition in xxcus_rebate_product_sdw_tbl for  ' ||
                     l_partition_sdw || ' - ' || SQLERRM;
        fnd_file.put_line(fnd_file.log, l_err_msg);
        RAISE program_error;
    END;

  EXCEPTION
    WHEN program_error THEN
      ROLLBACK;
      l_err_code := 2;
      fnd_file.put_line(fnd_file.log, l_sec);
      fnd_file.put_line(fnd_file.output, l_sec);
      l_err_msg := l_err_msg || ' Error_Stack...' ||
                   dbms_utility.format_error_stack() ||
                   ' Error_Backtrace...' ||
                   dbms_utility.format_error_backtrace();
      fnd_file.put_line(fnd_file.log, l_err_msg);
      fnd_file.put_line(fnd_file.output, l_err_msg);
      retcode         := l_err_code;
      errbuf          := l_err_msg;
      l_err_callpoint := l_sec;

      xxcus_error_pkg.xxcus_error_main_api(p_called_from       => l_err_callfrom
                                          ,p_calling           => l_err_callpoint
                                          ,p_request_id        => l_req_id
                                          ,p_ora_error_msg     => substr(regexp_replace(l_err_msg
                                                                                       ,'[[:cntrl:]]'
                                                                                       ,NULL)
                                                                        ,1
                                                                        ,2000)
                                          ,p_error_desc        => substr(l_err_msg
                                                                        ,1
                                                                        ,240)
                                          ,p_distribution_list => l_distro_list
                                          ,p_module            => 'TM');

    WHEN OTHERS THEN
      fnd_file.put_line(fnd_file.log, l_sec);
      fnd_file.put_line(fnd_file.output, l_sec);
      l_err_code := 2;
      l_err_msg  := l_err_msg || ' Error_Stack...' ||
                    dbms_utility.format_error_stack() ||
                    ' Error_Backtrace...' ||
                    dbms_utility.format_error_backtrace();
      fnd_file.put_line(fnd_file.log, l_err_msg);
      fnd_file.put_line(fnd_file.output, l_err_msg);
      retcode         := l_err_code;
      errbuf          := l_err_msg;
      l_err_callpoint := l_sec;

      xxcus_error_pkg.xxcus_error_main_api(p_called_from       => l_err_callfrom
                                          ,p_calling           => l_err_callpoint
                                          ,p_request_id        => l_req_id
                                          ,p_ora_error_msg     => substr(regexp_replace(l_err_msg
                                                                                       ,'[[:cntrl:]]'
                                                                                       ,NULL)
                                                                        ,1
                                                                        ,2000)
                                          ,p_error_desc        => substr(l_err_msg
                                                                        ,1
                                                                        ,240)
                                          ,p_distribution_list => l_distro_list
                                          ,p_module            => 'TM');
  END load_rebate_product;

  /*******************************************************************************
  * Procedure:   LOAD_CATEGORIES_APEX
  * Description: This will load the classification from Apex creating the category codes
  *               using rebate vender for segment1 and the classification for segment2
  *              after category code has been created any item associated will be
  *              interfaced to use the new category
  HISTORY
  ===============================================================================
  VERSION DATE          AUTHOR(S)       DESCRIPTION
  ------- -----------   --------------- -----------------------------------------
  1.0     09/03/2010    Kathy Poling    Initial creation of the procedure
  1.1     02/25/2011    Kathy Poling    RFC 25039 enhancement to products to allow
                                        multiple categories by rebate vendor
  1.2    10/18/2011     Kathy Poling    RFC  32180 changes to category need to check category
                                        as they load
  1.3    11/08/2011     Kathy Poling    RFC 32296 Change update category to category id
                                        for old category along with changes for pulling
                                        from apex added NVL
  ********************************************************************************/
  --PROCEDURE LOAD_CATEGORIES_APEX(errbuf OUT VARCHAR2, retcode OUT NUMBER) IS
  PROCEDURE load_categories_apex(errbuf OUT VARCHAR2, retcode OUT NUMBER) IS

    l_sec                VARCHAR2(200);
    l_err_msg            VARCHAR2(3000);
    l_err_code           NUMBER;
    l_req_id             NUMBER NULL;
    v_phase              VARCHAR2(50);
    v_status             VARCHAR2(50);
    v_dev_status         VARCHAR2(50);
    v_dev_phase          VARCHAR2(50);
    v_message            VARCHAR2(250);
    v_interval           NUMBER := 30; -- In seconds
    v_max_time           NUMBER := 15000; -- In seconds
    v_error_message      VARCHAR2(3000);
    l_can_submit_request BOOLEAN := TRUE;
    l_org_id             NUMBER := 84; --Default to HDS Rebates USA - Inv
    l_user_id            NUMBER; --REBTINTERFACE user
    l_user CONSTANT VARCHAR2(100) := 'REBTINTERFACE';
    l_responsibility VARCHAR2(100) := 'XXCUS_CON'; --'HDS Trade Management Administrator - Rebates'

    -- Error DEBUG
    l_err_callfrom  VARCHAR2(75) DEFAULT 'XXCUS_TM_INTERFACE_PKG.LOAD_CATEGORIES_APEX';
    l_err_callpoint VARCHAR2(75) DEFAULT 'START Loading Categories Apex';
    l_distro_list   VARCHAR2(75) DEFAULT 'HDSOracleDevelopers@hdsupply.com';

    --category codes
    l_category_rec inv_item_category_pub.category_rec_type;
    l_api_version CONSTANT NUMBER := 1.0;
    x_return_status VARCHAR2(1);
    x_category_id   NUMBER;
    x_msg_count     NUMBER;
    x_msg_data      VARCHAR2(4000);
    x_errorcode     VARCHAR2(4000);

  BEGIN

    SELECT user_id INTO l_user_id FROM fnd_user WHERE user_name = l_user;
    --  Setup parameters for running FND JOBS!
    l_can_submit_request := xxcus_misc_pkg.set_responsibility(l_user
                                                             ,l_responsibility);
    IF l_can_submit_request
    THEN
      l_sec := 'Global Variables are set.';

    ELSE

      l_sec := 'Global Variables are not set for the Responsibility of ' ||
               l_responsibility || ' and the User.';
      fnd_file.put_line(fnd_file.log, l_sec);
      fnd_file.put_line(fnd_file.output, l_sec);
      RAISE program_error;
    END IF;
    /*
        BEGIN
          l_sec := 'Truncate the table to load data into category interface.';
          fnd_file.put_line(fnd_file.log, l_sec);
          fnd_file.put_line(fnd_file.output, l_sec);
          EXECUTE IMMEDIATE 'truncate table inv.mtl_item_categories_interface';
        EXCEPTION
          WHEN OTHERS THEN
            l_err_msg := 'Failed to Truncate mtl_item_categories_interface : ' ||
                         SQLERRM;
            fnd_file.put_line(fnd_file.log, l_err_msg);
            RAISE program_error;
        END;
    */

    BEGIN
      l_sec := 'Insert data into xxcus_rebate_categories_tbl.';

      INSERT /*+ APPEND */
      INTO xxcus.xxcus_rebate_categories_tbl
        (SELECT inventory_item_id
               ,classification
               ,category_id
               ,cat_seg1
               ,cat_seg2
               ,rebt_vndr_nm
               ,rebt_vndr_cd
               ,cust_account_id
               ,processed
               ,new_classification
               ,SYSDATE
               ,updated_by
               ,classify_id
           FROM ea_apps.hds_rebate_classify@apxprd_lnk -- Ver 3.7 TMS 20171128-00198 hdsoracle.hds_rebate_classify@apxprd_lnk
          WHERE processed = 'N'
            AND (classification <> nvl(cat_seg2, 'NEW') OR
                new_classification <> nvl(cat_seg2, 'NEW') OR
                rebt_vndr_cd <> nvl(cat_seg1, 'NEW')) ---- Version 1.1    --Version 1.3 added NVL
            AND (nvl(classification, 'X') <> 'X' OR
                nvl(new_classification, 'X') <> 'X') ---- Version 1.1
            AND inventory_item_id IS NOT NULL ---- Version 1.3
         );

      COMMIT;

      l_sec := 'Updating hds_rebate_classify in apex.';

      UPDATE ea_apps.hds_rebate_classify@apxprd_lnk -- Ver 3.7 TMS 20171128-00198  hdsoracle.hds_rebate_classify@apxprd_lnk
         SET processed = 'Y'
       WHERE processed = 'N'
         AND (classification <> cat_seg2 OR new_classification <> cat_seg2 OR
             rebt_vndr_cd <> cat_seg1) ---- Version 1.1
         AND (nvl(classification, 'X') <> 'X' OR
             nvl(new_classification, 'X') <> 'X');

      COMMIT;
    END;

    --Create category codes in Oracle
    BEGIN
      l_sec := 'Loop thru to create classification.';

      FOR c_category IN (SELECT DISTINCT rebt_vndr_cd
                                        ,(CASE
                                           WHEN new_classification IS NOT NULL THEN
                                            new_classification
                                           ELSE
                                            classification
                                         END) seg2
                           FROM xxcus.xxcus_rebate_categories_tbl
                          WHERE processed = 'N'
                            AND rebt_vndr_cd <> cat_seg1
                            AND (CASE
                                  WHEN new_classification IS NOT NULL THEN
                                   new_classification
                                  ELSE
                                   classification
                                END) <> cat_seg2
                            AND (rebt_vndr_cd, (CASE
                                   WHEN new_classification IS NOT NULL THEN
                                    new_classification
                                   ELSE
                                    classification
                                 END)) NOT IN (SELECT segment1, segment2
                                                 FROM inv.mtl_categories_b
                                                WHERE structure_id = 50348
                                                  AND segment1 = rebt_vndr_cd
                                                  AND segment2 = (CASE
                                                        WHEN new_classification IS NOT NULL THEN
                                                         new_classification
                                                        ELSE
                                                         classification
                                                      END))
                         UNION
                         SELECT DISTINCT rebt_vndr_cd, classification
                           FROM ea_apps.hds_rebate_product_class_upld@apxprd_lnk  -- Ver 3.7 TMS 20171128-00198 hdsoracle.hds_rebate_product_class_upld@apxprd_lnk
                          WHERE (rebt_vndr_cd, classification) NOT IN
                                (SELECT segment1, segment2
                                   FROM inv.mtl_categories_b
                                  WHERE structure_id = 50348
                                    AND segment1 = rebt_vndr_cd
                                    AND segment2 = classification))
      LOOP

        l_category_rec.structure_id   := 50348;
        l_category_rec.structure_code := 'HDS Supplier Category';
        l_category_rec.summary_flag   := 'N';
        l_category_rec.enabled_flag   := 'Y';
        l_category_rec.segment1       := c_category.rebt_vndr_cd;
        l_category_rec.segment2       := c_category.seg2;

        -- create the new mtl_categories record.
        inv_item_category_pub.create_category(p_api_version   => l_api_version
                                             ,p_init_msg_list => fnd_api.g_false
                                             ,p_commit        => fnd_api.g_true
                                             ,x_return_status => x_return_status
                                             ,x_errorcode     => x_errorcode
                                             ,x_msg_count     => x_msg_count
                                             ,x_msg_data      => x_msg_data
                                             ,p_category_rec  => l_category_rec
                                             ,x_category_id   => x_category_id);

        fnd_file.put_line(fnd_file.output
                         ,'Return_status = ' ||
                          substr(x_return_status, 1, 255));
        fnd_file.put_line(fnd_file.log
                         ,'Return_status = ' ||
                          substr(x_return_status, 1, 255));

        fnd_file.put_line(fnd_file.output
                         ,'Category Id = ' || to_char(x_category_id));
        fnd_file.put_line(fnd_file.log
                         ,'Category Id = ' || to_char(x_category_id));

      END LOOP;
    END;

    --Load item categories
    BEGIN

      l_sec := 'Load data into category interface.';

      FOR c_item_category IN (SELECT r.classify_id
                                    ,r.inventory_item_id
                                    ,r.category_id old_category_id
                                    ,r.cat_seg1
                                    ,r.cat_seg2
                                    ,r.rebt_vndr_cd
                                    ,r.classification
                                    ,r.new_classification
                                     --Version 1.2 added oracle_cat
                                    ,(SELECT category_id
                                        FROM apps.mtl_item_categories_v c
                                       WHERE c.category_set_id = 1100000041
                                         AND c.organization_id = 84
                                         AND c.inventory_item_id =
                                             r.inventory_item_id
                                         AND c.segment1 = r.rebt_vndr_cd) oracle_cat
                                    ,(SELECT category_id
                                        FROM apps.mtl_item_categories_v x
                                       WHERE x.category_set_id = 1100000041
                                         AND x.organization_id = 84
                                         AND x.inventory_item_id =
                                             r.inventory_item_id
                                         AND x.category_id = 1123) category
                                    ,c.category_id
                                    ,c.segment1
                                    ,c.segment2
                                FROM xxcus.xxcus_rebate_categories_tbl r
                                    ,inv.mtl_categories_b              c
                               WHERE processed = 'N'
                                 AND r.rebt_vndr_cd = c.segment1
                                 AND (CASE
                                       WHEN new_classification IS NOT NULL THEN
                                        new_classification
                                       ELSE
                                        classification
                                     END) = c.segment2
                                 AND c.structure_id = 50348)
      LOOP

        IF /* c_item_category.old_category_id = 1123
                                                                                                                                                                                                  OR c_item_category.old_category_id <> c_item_category.category_id
                                                                                                                                                                                                  AND  c_item_category.cat_seg1 = c_item_category.segment1 ---- Version 1.1
                                                                                                                                                                                                  */
        -- Version 1.2
         c_item_category.oracle_cat IS NULL AND
         c_item_category.category = 1123 OR
         c_item_category.oracle_cat <> c_item_category.category_id
        THEN

          INSERT INTO mtl_item_categories_interface
            (inventory_item_id
            ,organization_id
            ,transaction_type
            ,category_set_id
            ,category_id
            ,old_category_id
            ,process_flag
            ,set_process_id)
          VALUES
            (c_item_category.inventory_item_id
            ,l_org_id
            ,'UPDATE'
            ,1100000041
            ,c_item_category.category_id
             --,c_item_category.old_category_id                             -- Version 1.2
            ,nvl(c_item_category.oracle_cat, c_item_category.category) -- Version 1.3
            ,1
            ,1);

        END IF;
        /*
        --Version 1.1 add to allow the multiple categories to be assigned to create a new item category
        IF c_item_category.old_category_id <> 1123 AND
           c_item_category.old_category_id <> c_item_category.category_id AND
           c_item_category.cat_seg1 <> c_item_category.segment1
           */ --Version 1.2
        IF c_item_category.oracle_cat IS NULL AND
           c_item_category.category IS NULL
        THEN

          INSERT INTO mtl_item_categories_interface
            (inventory_item_id
            ,organization_id
            ,transaction_type
            ,category_set_id
            ,category_id
             --,old_category_id                         --Version 1.2
            ,process_flag
            ,set_process_id)
          VALUES
            (c_item_category.inventory_item_id
            ,l_org_id
            ,'CREATE'
            ,1100000041
            ,c_item_category.category_id
             --,c_item_category.old_category_id         --Version 1.2
            ,1
            ,1);

        END IF;
        --end version 1.1 to allow creation of new item category

        UPDATE xxcus.xxcus_rebate_categories_tbl
           SET processed = 'Y'
         WHERE classify_id = c_item_category.classify_id;

      END LOOP;

      --Submit Import Process under XXCUS_CON responsitility
      l_sec := 'Submitting Item Category Assignment Open Interface process.';
      fnd_file.put_line(fnd_file.log, l_sec);
      fnd_file.put_line(fnd_file.output, l_sec);

      --Submite the Concurrent Item Category Import
      l_req_id := fnd_request.submit_request(application => 'INV'
                                            ,program     => 'INV_ITEM_CAT_ASSIGN_OI'
                                            ,description => NULL
                                            ,start_time  => SYSDATE
                                            ,sub_request => FALSE
                                            ,argument1   => 1
                                            , --set_process_id
                                             argument2   => 1
                                            ,argument3   => 1);

      COMMIT;

      fnd_file.put_line(fnd_file.log, l_req_id);
      fnd_file.put_line(fnd_file.output, l_req_id);

      --Write detail Line Log rows
      l_sec := 'Waiting for Import Items to finish ' || l_req_id;
      fnd_file.put_line(fnd_file.log, l_sec);
      fnd_file.put_line(fnd_file.output, l_sec);

      --Wait for Item Import
      IF (l_req_id != 0)
      THEN
        IF fnd_concurrent.wait_for_request(l_req_id
                                          ,v_interval
                                          ,v_max_time
                                          ,v_phase
                                          ,v_status
                                          ,v_dev_phase
                                          ,v_dev_status
                                          ,v_message)
        THEN
          v_error_message := 'ReqID=' || l_req_id || ' DPhase ' ||
                             v_dev_phase || ' DStatus ' || v_dev_status ||
                             ' MSG ' || v_message;

          IF v_dev_phase != 'COMPLETE' OR v_dev_status != 'NORMAL'
          THEN
            l_err_msg := 'An error occured in Import Categories, please review the Log for concurrent request ' ||
                         l_req_id || ' - ' || v_error_message || '.';
            fnd_file.put_line(fnd_file.log, l_err_msg);
            fnd_file.put_line(fnd_file.output, l_err_msg);
            RAISE program_error;
            --ELSE
            --  retcode := 0;
          END IF;
        ELSE
          l_err_msg := 'EBS timed out waiting on Import Categories, please review the Log for concurrent request ' ||
                       l_req_id || ' - ' || v_error_message || '.';
          fnd_file.put_line(fnd_file.log, l_err_msg);
          fnd_file.put_line(fnd_file.output, l_err_msg);
          RAISE program_error;
        END IF;
      ELSE
        l_err_msg := 'An error occured when trying to submit Import Categories, the concurrent request was not submitted.';
        fnd_file.put_line(fnd_file.log, l_err_msg);
        fnd_file.put_line(fnd_file.output, l_err_msg);
        RAISE program_error;
      END IF;

    END;

  EXCEPTION
    WHEN program_error THEN
      ROLLBACK;
      l_err_code := 2;
      l_err_msg  := l_err_msg || ' Error_Stack...' ||
                    dbms_utility.format_error_stack() ||
                    ' Error_Backtrace...' ||
                    dbms_utility.format_error_backtrace();
      fnd_file.put_line(fnd_file.log, l_err_msg);
      fnd_file.put_line(fnd_file.output, l_err_msg);
      -- retcode         := l_err_code;
      --errbuf          := l_err_msg;
      l_err_callpoint := l_sec;

      xxcus_error_pkg.xxcus_error_main_api(p_called_from       => l_err_callfrom
                                          ,p_calling           => l_err_callpoint
                                          ,p_request_id        => l_req_id
                                          ,p_ora_error_msg     => substr(regexp_replace(l_err_msg
                                                                                       ,'[[:cntrl:]]'
                                                                                       ,NULL)
                                                                        ,1
                                                                        ,2000)
                                          ,p_error_desc        => substr(l_err_msg
                                                                        ,1
                                                                        ,240)
                                          ,p_distribution_list => l_distro_list
                                          ,p_module            => 'TM');

    WHEN OTHERS THEN
      fnd_file.put_line(fnd_file.log, l_sec);
      fnd_file.put_line(fnd_file.output, l_sec);
      l_err_code := 2;
      l_err_msg  := l_err_msg || ' Error_Stack...' ||
                    dbms_utility.format_error_stack() ||
                    ' Error_Backtrace...' ||
                    dbms_utility.format_error_backtrace();
      fnd_file.put_line(fnd_file.log, l_err_msg);
      fnd_file.put_line(fnd_file.output, l_err_msg);
      --retcode         := l_err_code;
      --errbuf          := l_err_msg;
      l_err_callpoint := l_sec;

      xxcus_error_pkg.xxcus_error_main_api(p_called_from       => l_err_callfrom
                                          ,p_calling           => l_err_callpoint
                                          ,p_request_id        => l_req_id
                                          ,p_ora_error_msg     => substr(regexp_replace(l_err_msg
                                                                                       ,'[[:cntrl:]]'
                                                                                       ,NULL)
                                                                        ,1
                                                                        ,2000)
                                          ,p_error_desc        => substr(l_err_msg
                                                                        ,1
                                                                        ,240)
                                          ,p_distribution_list => l_distro_list
                                          ,p_module            => 'TM');

  END load_categories_apex;

  /*******************************************************************************
  * Procedure:   ONEOFF_REBATE_PRODUCTS
  * Description: This will load products from staging area that were not mapped at
  *              monthend processing.  UOM xref would have been updated to
  *              allow the remaining products to load
  *
  *
  HISTORY
  ===============================================================================
  VERSION DATE          AUTHOR(S)       DESCRIPTION
  ------- -----------   --------------- -----------------------------------------
  1.0     09/07/2010    Kathy Poling    Initial creation of the procedure
  1.1     03/21/2012    Kathy Poling    Change segment1 - item number add suffix '~RBT' for
                                        White Cap project.  RFC 33211
  1.2     05/21/2012    Kathy Poling    Removed truncate on the interface tables for White Cap 20/20
  1.3     03/08/2012    Kathy Poling    Changed to uom table in oracle instead of apex
  1.4     09/05/2013    Kathy Poling    Changed call to procedure oneoff_uom_mapping RFC 38031
  1.5     09/17/2013    Kathy Poling    Adding source system to attribute field to identify
                                        correct sku's.  Changed to get classification to
                                        stop duplicates
                                        RFC 38254
  1.6     10/28/2013    Kathy Poling    Adding parallel hint to query and remove process being
                                        run as REBTINTERFACE     RFC 38762
  2.4    10/20/2014     Kathy Poling    ESMS 531490 RFC 42024 Changes to make sure only
                                        one sku is created, duplicates where happening
                                        since we don't purge sku's that were not created
                                        for mapping issues.
  ********************************************************************************/

  PROCEDURE oneoff_rebate_products(errbuf OUT VARCHAR2, retcode OUT NUMBER) IS

    --
    -- Package Variables
    --
    l_req_id             NUMBER NULL;
    v_phase              VARCHAR2(50);
    v_status             VARCHAR2(50);
    v_dev_status         VARCHAR2(50);
    v_dev_phase          VARCHAR2(50);
    v_message            VARCHAR2(250);
    v_error_message      VARCHAR2(3000);
    v_interval           NUMBER := 30; -- In seconds
    v_max_time           NUMBER := 15000; -- In seconds
    l_can_submit_request BOOLEAN := TRUE;
    l_globalset          VARCHAR2(100);
    l_err_msg            VARCHAR2(3000);
    l_err_code           NUMBER;
    l_sec                VARCHAR2(255);
    l_count              NUMBER := 0;
    l_procedure          VARCHAR2(50) := 'ONEOFF_REBATE_PRODUCTS';
    l_item_sequence      NUMBER;
    l_set_process_id     NUMBER;
    l_org_id             NUMBER := 84; --Default to HDS Rebates USA - Inv
    l_user_id            NUMBER; --REBTINTERFACE user
    l_user               VARCHAR2(100); --Version 1.6
    l_responsibility     VARCHAR2(100); --Version 1.6
    l_bu                 VARCHAR2(50) := NULL;
    l_category_set CONSTANT mtl_category_sets_tl.category_set_id%TYPE := 1100000041; --HDS Supplier Category
    -- Error DEBUG
    l_err_callfrom  VARCHAR2(75) DEFAULT 'XXCUS_TM_INTERFACE_PKG.ONEOFF_REBATE_PRODUCT';
    l_err_callpoint VARCHAR2(75) DEFAULT 'START';
    l_distro_list   VARCHAR2(75) DEFAULT 'HDSOracleDevelopers@hdsupply.com';

  BEGIN

    --Version 1.9  11/3/2013
    -- SELECT user_id INTO l_user_id FROM fnd_user WHERE user_name = l_user;
    SELECT user_id, user_name
      INTO l_user_id, l_user
      FROM fnd_user
     WHERE user_id = fnd_global.user_id();

    SELECT responsibility_name
      INTO l_responsibility
      FROM fnd_responsibility_vl
     WHERE responsibility_id = fnd_global.resp_id();

    fnd_file.put_line(fnd_file.log, 'Resp Name:  ' || l_responsibility);

    --  Setup parameters for running FND JOBS!
    l_can_submit_request := xxcus_misc_pkg.set_responsibility(l_user
                                                             ,l_responsibility);
    IF l_can_submit_request
    THEN
      l_sec := 'Global Variables are set User Name:  ' || l_user ||
               ' Resp Name:  ' || l_responsibility;
      fnd_file.put_line(fnd_file.log, l_sec);
      fnd_file.put_line(fnd_file.output, l_sec);

    ELSE

      l_sec := 'Global Variables are not set for User Name:  ' || l_user ||
               ' Resp Name:  ' || l_responsibility;
      fnd_file.put_line(fnd_file.log, l_sec);
      fnd_file.put_line(fnd_file.output, l_sec);
      RAISE program_error;
    END IF;

    SELECT COUNT(*)
      INTO l_count
      FROM xxcus.xxcus_rebate_product_curr_tbl
     WHERE status_flag = 'N';

    IF l_count >= 1
    THEN
      -- if count is < 1 No Product data to loaded.

      INSERT INTO xxcus.xxcus_rebate_aud_tbl
      VALUES
        (nvl(fnd_global.conc_request_id, 0)
        ,l_procedure
        ,SYSDATE
        ,nvl(fnd_global.user_name, 'NA')
        ,nvl(fnd_global.resp_name, 'NA')
        ,NULL
        ,0
        ,'Beginning Program - oneoff product load'
        ,SYSDATE
        ,l_count
        ,NULL
        ,NULL);

      COMMIT;

      UPDATE xxcus.xxcus_rebate_aud_tbl
         SET stage = l_sec, stg_date = SYSDATE
      --processed_datetime = sysdate
       WHERE conc_request_id = nvl(fnd_global.conc_request_id, 0)
         AND procedure_name = l_procedure
         AND trunc(processed_datetime) = trunc(SYSDATE);

      COMMIT;
      /*   Version 1.2
            --Clear contents of interface tables
            BEGIN
              l_sec := 'Truncate the table to load data into item interface.';
              fnd_file.put_line(fnd_file.log, l_sec);
              fnd_file.put_line(fnd_file.output, l_sec);
              EXECUTE IMMEDIATE 'truncate table inv.mtl_system_items_interface';
            EXCEPTION
              WHEN OTHERS THEN
                l_err_msg := 'Failed to Truncate mtl_system_items_interface : ' ||
                             SQLERRM;
                fnd_file.put_line(fnd_file.log, l_err_msg);
                RAISE program_error;
            END;

            BEGIN
              l_sec := 'Truncate the table to load data into category interface.';
              fnd_file.put_line(fnd_file.log, l_sec);
              fnd_file.put_line(fnd_file.output, l_sec);
              EXECUTE IMMEDIATE 'truncate table inv.mtl_item_categories_interface';
            EXCEPTION
              WHEN OTHERS THEN
                l_err_msg := 'Failed to Truncate mtl_item_categories_interface : ' ||
                             SQLERRM;
                fnd_file.put_line(fnd_file.log, l_err_msg);
                RAISE program_error;
            END;
      */
      --Create output file
      l_sec := 'Start new items loading.';
      fnd_file.put_line(fnd_file.log, l_sec);
      fnd_file.put_line(fnd_file.output, l_sec);

      UPDATE xxcus.xxcus_rebate_aud_tbl
         SET stage = l_sec, stg_date = SYSDATE
      --processed_datetime = sysdate
       WHERE conc_request_id = nvl(fnd_global.conc_request_id, 0)
         AND procedure_name = l_procedure
         AND trunc(processed_datetime) = trunc(SYSDATE);

      --loading new or updated uom from apex
      l_sec := 'Load UOM xref data from Apex.';
      fnd_file.put_line(fnd_file.log, l_sec);
      fnd_file.put_line(fnd_file.output, l_sec);

      fnd_file.put_line(fnd_file.log
                       ,'Start time:  ' ||
                        to_char(SYSDATE, 'DD-MON-YYYY HH24:MI:SS') ||
                        ' for ' || l_sec);
      fnd_file.put_line(fnd_file.output
                       ,'Start time:  ' ||
                        to_char(SYSDATE, 'DD-MON-YYYY HH24:MI:SS') ||
                        ' for ' || l_sec);

      load_uom_mapping(l_bu); --Version 1.4   9/5/13

      fnd_file.put_line(fnd_file.log
                       ,'End time:  ' ||
                        to_char(SYSDATE, 'DD-MON-YYYY HH24:MI:SS') ||
                        ' for ' || l_sec);
      fnd_file.put_line(fnd_file.output
                       ,'End time:  ' ||
                        to_char(SYSDATE, 'DD-MON-YYYY HH24:MI:SS') ||
                        ' for ' || l_sec);

      FOR i IN 1 .. 15
      LOOP

        SELECT mtl_system_items_intf_sets_s.nextval
          INTO l_set_process_id
          FROM dual;

        --Version 2.4  10/21/2014 added loop to get only one distict sku for bu
        FOR c_item IN (SELECT DISTINCT bu_nm
                                      ,nvl(p.sku_cd, 'UNKNOWN') sku_cd
                                      ,src_sys_nm
                         FROM xxcus.xxcus_rebate_product_curr_tbl p
                        WHERE p.status_flag = 'N'
                          AND p.sku_cd || '~' || p.bu_nm || '~' ||
                              p.src_sys_nm NOT IN
                              (SELECT description
                                 FROM inv.mtl_system_items_b
                                WHERE organization_id = l_org_id
                                  AND description =
                                      p.sku_cd || '~' || p.bu_nm || '~' ||
                                      p.src_sys_nm))
        LOOP
          FOR c_item_number_rec IN (SELECT /*+ PARALLEL (AUTO) */ --Version 1.6
                                     p.prod_id
                                    ,nvl(p.sku_cd, 'UNKNOWN') sku_cd
                                    ,p.sku_desc_lng
                                    ,(CASE
                                       WHEN p.vndr_part_nbr IN
                                            ('UNKNOWN', '-') THEN
                                        NULL
                                       ELSE
                                        p.vndr_part_nbr
                                     END) vndr_part_nbr
                                    ,p.po_repl_cost
                                    ,p.prod_purch_uom
                                    ,p.unspsc_cd
                                    ,p.bu_nm
                                    ,p.geo_loc_hier_lob_nm
                                    ,p.status_flag
                                    ,p.src_sys_nm
                                    ,p.prim_upc
                                    ,p.secondary_upc
                                    ,p.tertiary_upc
                                    ,oracle_uom_code
                                    ,upld.rebt_vndr_cd
                                    ,upld.classification
                                    ,(SELECT c.category_id
                                        FROM inv.mtl_categories_b c
                                       WHERE c.structure_id = 50348
                                         AND c.segment1 = upld.rebt_vndr_cd
                                         AND c.segment2 =
                                             upld.classification) category_id
                                      FROM xxcus.xxcus_rebate_product_curr_tbl p
                                          ,xxcus.xxcus_rebate_uom_tbl          xref --Version 1.3
                                           --,hdsoracle.hds_rebate_uom_xref_tbl@apxprd_lnk       xref  Version 1.3
                                           --,hdsoracle.hds_rebate_product_class_upld@apxprd_lnk upld   Version 1.5
                                          ,(SELECT DISTINCT rebt_vndr_cd
                                                           ,classification
                                                           ,bu_nm
                                                           ,sku_cd --Version 1.5
                                              FROM ea_apps.hds_rebate_product_class_upld@apxprd_lnk -- Ver 3.7 TMS 20171128-00198  hdsoracle.hds_rebate_product_class_upld@apxprd_lnk
                                             WHERE processed = 'N'
                                               AND rownum = 1) upld
                                     WHERE p.status_flag = 'N'
                                       AND p.bu_nm = c_item.bu_nm --Version 2.4
                                       AND p.src_sys_nm = c_item.src_sys_nm --Version 2.4
                                       AND p.sku_cd = c_item.sku_cd --Version 2.4
                                       AND p.prod_purch_uom =
                                           xref.purch_uom_cd --only items with a valid uom
                                       AND p.geo_loc_hier_lob_nm =
                                           xref.lob_nm
                                       AND p.bu_nm = xref.bu_nm
                                       AND upper(p.src_sys_nm) =
                                           upper(xref.src_sys_nm)
                                       AND p.bu_nm = upld.bu_nm(+)
                                       AND p.sku_cd = upld.sku_cd(+)
                                          --AND upld.processed(+) = 'N'           Version 1.5
                                          --and rownum < 10001
                                       AND p.as_of_dt IN
                                           (SELECT MAX(as_of_dt)
                                              FROM xxcus.xxcus_rebate_product_curr_tbl x
                                             WHERE x.status_flag = 'N'
                                               AND x.bu_nm = p.bu_nm
                                               AND x.src_sys_nm =
                                                   p.src_sys_nm
                                               AND x.sku_cd = p.sku_cd)
                                       AND p.sku_cd || '~' || p.bu_nm || '~' ||
                                           p.src_sys_nm --Version 1.5 9/17/13  added src_sys_nm
                                           NOT IN
                                           (SELECT description
                                              FROM inv.mtl_system_items_b
                                             WHERE organization_id = l_org_id
                                               AND description =
                                                   p.sku_cd || '~' || p.bu_nm || '~' ||
                                                   p.src_sys_nm --Version 1.5 9/17/13  added src_sys_nm
                                            )
                                       AND rownum = 1) --Version 2.4 needed to add because data pushed from SDW could cause the same date
          LOOP

            SELECT mtl_system_items_b_s.nextval
              INTO l_item_sequence
              FROM dual;

            INSERT INTO inv.mtl_system_items_interface
              (organization_id
              ,set_process_id
              ,transaction_type
              ,process_flag
              ,summary_flag
              ,segment1
              ,description
              ,primary_uom_code
              ,attribute1
              ,attribute2
              ,attribute3
              ,attribute4
              ,attribute5
              ,attribute6
              ,attribute7 --Version 1.5 9/17/13  src_sys_nm
              ,attribute8 --Version 1.5 9/17/13  sku_cd
              ,inventory_item_status_code
              ,allowed_units_lookup_code
              ,last_update_date
              ,last_updated_by
              ,inventory_asset_flag
              ,revision_qty_control_code
              ,revision
              ,purchasing_enabled_flag
              ,stock_enabled_flag
              ,mtl_transactions_enabled_flag
              ,must_use_approved_vendor_flag
              ,purchasing_item_flag
              ,customer_order_enabled_flag
              ,inventory_item_flag
              ,reservable_type
              ,bom_item_type
              ,bom_enabled_flag
              ,effectivity_control
              ,costing_enabled_flag
              ,enabled_flag
              ,item_type)
            VALUES
              (l_org_id
              ,l_set_process_id
              ,'CREATE'
              ,1
              ,'N'
              ,l_item_sequence || '~RBT'
              , --c_item_number_rec.sku_cd || '~' || c_item_number_rec.bu_nm,
               c_item_number_rec.sku_cd || '~' || c_item_number_rec.bu_nm || '~' ||
               c_item_number_rec.src_sys_nm --Version 1.5  9/17/13  c_item_number_rec.src_sys_nm              --Version 1.5  9/17/13
              , --c_item_number_rec.sku_desc_lng,
               c_item_number_rec.oracle_uom_code
              ,c_item_number_rec.geo_loc_hier_lob_nm
              ,c_item_number_rec.prod_id
              , --|| '~' || c_item_number_rec.bu_nm || '~' ||c_item_number_rec.src_sys_nm,
               c_item_number_rec.vndr_part_nbr
              ,c_item_number_rec.unspsc_cd
              ,c_item_number_rec.prim_upc
              ,substr(c_item_number_rec.sku_desc_lng, 1, 240)
              ,c_item_number_rec.src_sys_nm --Version 1.5  9/17/13
              ,c_item_number_rec.sku_cd --Version 1.5  9/17/13
              ,'Active'
              ,3
              ,SYSDATE
              ,l_user_id
              ,'N'
              ,1
              ,0
              ,'N'
              ,'N'
              ,'N'
              ,'N'
              ,'Y'
              ,'N'
              ,'N'
              ,1
              ,4
              ,'Y'
              ,1
              ,'N'
              ,'Y'
              ,'P' --Purchased
               );

            INSERT INTO mtl_item_categories_interface
              (item_number
              ,organization_id
              ,transaction_type
              ,category_set_id
              ,category_id
              ,process_flag
              ,set_process_id)
            VALUES
              (l_item_sequence || '~RBT'
              , --c_item_number_rec.sku_cd || '~' || c_item_number_rec.bu_nm,
               l_org_id
              ,'CREATE'
              ,l_category_set
              , --1 = Inventory, 1100000041 = HDS Supplier Category
               nvl(c_item_number_rec.category_id, 1123)
              , --l_category_number,  NEW.NEW
               1
              ,l_set_process_id);

            UPDATE xxcus.xxcus_rebate_product_curr_tbl
               SET status_flag  = 'P'
                  ,request_id   = nvl(fnd_global.conc_request_id, 0)
                  ,process_date = SYSDATE
             WHERE prod_id = c_item_number_rec.prod_id
               AND sku_cd = c_item_number_rec.sku_cd
               AND bu_nm = c_item_number_rec.bu_nm
               AND src_sys_nm = c_item_number_rec.src_sys_nm --Version 1.5 9/17/13
            ;

          END LOOP;
        END LOOP;
      END LOOP;
    END IF;

    BEGIN
      --start of items to be updated
      l_sec := 'Start items for update loading.';
      fnd_file.put_line(fnd_file.log, l_sec);
      fnd_file.put_line(fnd_file.output, l_sec);

      UPDATE xxcus.xxcus_rebate_aud_tbl
         SET stage = l_sec, stg_date = SYSDATE
      --processed_datetime = sysdate
       WHERE conc_request_id = nvl(fnd_global.conc_request_id, 0)
         AND procedure_name = l_procedure
         AND trunc(processed_datetime) = trunc(SYSDATE);

      SELECT mtl_system_items_intf_sets_s.nextval
        INTO l_set_process_id
        FROM dual;

      FOR c_update IN (SELECT /*+ PARALLEL (AUTO) */ --Version 1.6
                        inventory_item_id
                       ,description
                       ,segment1
                       ,attribute1
                       ,attribute2
                       ,attribute3
                       ,attribute4
                       ,attribute5
                       ,attribute6
                       ,geo_loc_hier_lob_nm
                       ,bu_nm
                       ,prod_id
                       ,sku_cd
                       ,substr(sku_desc_lng, 1, 240) sku_desc_lng
                       ,vndr_part_nbr
                       ,prim_upc
                       ,unspsc_cd
                       ,src_sys_nm --Version 1.5   9/17/13
                         FROM inv.mtl_system_items_b              i
                             ,xxcus.xxcus_rebate_product_curr_tbl p
                        WHERE organization_id = l_org_id
                          AND i.description =
                              sku_cd || '~' || bu_nm || '~' || src_sys_nm --Version 1.5  9/17/13 added src_sys_nm
                          AND i.attribute1 = geo_loc_hier_lob_nm
                          AND (i.attribute2 <> prod_id OR
                              i.attribute3 <> vndr_part_nbr OR
                              i.attribute4 <> unspsc_cd OR
                              i.attribute5 <> prim_upc OR
                              i.attribute6 <> substr(sku_desc_lng, 1, 240)))
      LOOP

        --check if sku, description or vendor part number has been changed then update item

        IF c_update.prod_id <> c_update.attribute2 OR
           c_update.sku_desc_lng <> c_update.attribute6 OR
           c_update.vndr_part_nbr <> c_update.attribute3 OR
           c_update.unspsc_cd <> c_update.attribute4 OR
           c_update.prim_upc <> c_update.attribute5
        THEN

          INSERT INTO inv.mtl_system_items_interface
            (inventory_item_id
            ,organization_id
            ,set_process_id
            ,transaction_type
            ,process_flag
            ,summary_flag
            ,description
            ,attribute2
            ,attribute3
            ,attribute4
            ,attribute5
            ,attribute6
            ,attribute7 --Version 1.5  9/17/13
            ,attribute8 --Version 1.5  9/17/13
            ,inventory_item_status_code
            ,allowed_units_lookup_code
            ,last_update_date
            ,last_updated_by
            ,enabled_flag)
          VALUES
            (c_update.inventory_item_id
            ,l_org_id
            ,l_set_process_id
            ,'UPDATE'
            ,1
            ,'N'
            ,c_update.sku_cd || '~' || c_update.bu_nm || '~' ||
             c_update.src_sys_nm --Version 1.5  9/17/13
            ,c_update.prod_id
            ,c_update.vndr_part_nbr
            ,c_update.unspsc_cd
            ,c_update.prim_upc
            ,c_update.sku_desc_lng
            ,c_update.src_sys_nm --Version 1.5  9/17/13
            ,c_update.sku_cd --Version 1.5  9/17/13
            ,'Active'
            ,3
            ,SYSDATE
            ,l_user_id
            ,'Y');

          UPDATE xxcus.xxcus_rebate_product_curr_tbl
             SET status_flag  = 'P'
                ,request_id   = nvl(fnd_global.conc_request_id, 0)
                ,process_date = SYSDATE
           WHERE prod_id = c_update.prod_id
             AND sku_cd = c_update.sku_cd
             AND bu_nm = c_update.bu_nm
             AND src_sys_nm = c_update.src_sys_nm --Version 1.5  9/17/13
          ;

        END IF;

        COMMIT;
      END LOOP;

      l_sec := 'Products for update inserted into interface.';
      fnd_file.put_line(fnd_file.log, l_sec);
      fnd_file.put_line(fnd_file.output, l_sec);

      UPDATE xxcus.xxcus_rebate_aud_tbl
         SET stage = l_sec, stg_date = SYSDATE
       WHERE conc_request_id = nvl(fnd_global.conc_request_id, 0)
         AND procedure_name = l_procedure
         AND trunc(processed_datetime) = trunc(SYSDATE);
      COMMIT;
    END;
    --*******************************************
    --Submit the item import process
    --*******************************************
    BEGIN
      l_count := 0;

      SELECT COUNT(*)
        INTO l_count
        FROM inv.mtl_system_items_interface
       WHERE organization_id = l_org_id;

      IF l_count > 0
      THEN

        FOR c_interface IN (SELECT DISTINCT set_process_id
                                           ,decode(transaction_type
                                                  ,'CREATE'
                                                  ,1
                                                  ,'UPDATE'
                                                  ,2) transaction_type
                              FROM inv.mtl_system_items_interface
                             WHERE organization_id = l_org_id)
        LOOP

          --Submit Import Process under XXCUS_CON responsitility
          l_sec := 'Submitting Import Items process.';
          fnd_file.put_line(fnd_file.log, l_sec);
          fnd_file.put_line(fnd_file.output, l_sec);

          UPDATE xxcus.xxcus_rebate_aud_tbl
             SET stage = l_sec, stg_date = SYSDATE
           WHERE conc_request_id = nvl(fnd_global.conc_request_id, 0)
             AND procedure_name = l_procedure
             AND trunc(processed_datetime) = trunc(SYSDATE);
          COMMIT;

          --Submite the Concurrent Item Import
          l_req_id := fnd_request.submit_request(application => 'INV'
                                                ,program     => 'INCOIN'
                                                ,description => NULL
                                                ,start_time  => SYSDATE
                                                ,sub_request => FALSE
                                                ,argument1   => l_org_id
                                                ,argument2   => 1
                                                ,argument3   => 1
                                                ,argument4   => 1
                                                ,argument5   => 1
                                                ,argument6   => c_interface.set_process_id
                                                ,argument7   => c_interface.transaction_type);

          COMMIT;

          fnd_file.put_line(fnd_file.log, l_req_id);
          fnd_file.put_line(fnd_file.output, l_req_id);

          --Write detail Line Log rows
          l_sec := 'Waiting for Import Items to finish ' || l_req_id;
          fnd_file.put_line(fnd_file.log, l_sec);
          fnd_file.put_line(fnd_file.output, l_sec);

          UPDATE xxcus.xxcus_rebate_aud_tbl
             SET stage = l_sec, stg_date = SYSDATE
           WHERE conc_request_id = nvl(fnd_global.conc_request_id, 0)
             AND procedure_name = l_procedure
             AND trunc(processed_datetime) = trunc(SYSDATE);
          COMMIT;

          --Wait for Item Import
          IF (l_req_id != 0)
          THEN
            IF fnd_concurrent.wait_for_request(l_req_id
                                              ,v_interval
                                              ,v_max_time
                                              ,v_phase
                                              ,v_status
                                              ,v_dev_phase
                                              ,v_dev_status
                                              ,v_message)
            THEN
              v_error_message := 'ReqID=' || l_req_id || ' DPhase ' ||
                                 v_dev_phase || ' DStatus ' || v_dev_status ||
                                 ' MSG ' || v_message;

              IF v_dev_phase != 'COMPLETE' OR v_dev_status != 'NORMAL'
              THEN
                l_err_msg := 'An error occured in the running of the Import Items request ID: ' ||
                             l_req_id || '   ' || v_error_message || '.';
                fnd_file.put_line(fnd_file.log, l_err_msg);
                fnd_file.put_line(fnd_file.output, l_err_msg);
                --RAISE program_error;
              ELSE
                retcode := 0;
              END IF;
            ELSE
              l_err_msg := 'EBS timed out waiting on Import Items ' ||
                           v_error_message || '.';
              fnd_file.put_line(fnd_file.log, l_err_msg);
              fnd_file.put_line(fnd_file.output, l_err_msg);
              --RAISE program_error;
            END IF;
          ELSE
            l_err_msg := 'An error occured when trying to submit the Import Items';
            fnd_file.put_line(fnd_file.log, l_err_msg);
            fnd_file.put_line(fnd_file.output, l_err_msg);
            RAISE program_error;
          END IF;

          --Version 1.6   11/27/13
          IF c_interface.transaction_type = 1
          THEN
            --Submit Item Category Assignment Open Interface under XXCUS_CON responsitility  11/27/13 moved so import runs following items
            l_sec := 'Submitting Item Category Assignment Open Interface process.';
            fnd_file.put_line(fnd_file.log, l_sec);
            fnd_file.put_line(fnd_file.output, l_sec);

            UPDATE xxcus.xxcus_rebate_aud_tbl
               SET stage = l_sec, stg_date = SYSDATE
             WHERE (conc_request_id = nvl(fnd_global.conc_request_id, 0) OR
                   trunc(processed_datetime) = trunc(SYSDATE))
               AND procedure_name = l_procedure;
            COMMIT;

            --Submite the Concurrent Item Category Import
            l_req_id := fnd_request.submit_request(application => 'INV'
                                                  ,program     => 'INV_ITEM_CAT_ASSIGN_OI'
                                                  ,description => NULL
                                                  ,start_time  => SYSDATE
                                                  ,sub_request => FALSE
                                                  ,argument1   => c_interface.set_process_id
                                                  ,argument2   => 1
                                                  ,argument3   => 1);

            COMMIT;

            fnd_file.put_line(fnd_file.log
                             ,'Item Category Assignment request:  ' ||
                              l_req_id);
            fnd_file.put_line(fnd_file.output
                             ,'Item Category Assignment request:  ' ||
                              l_req_id);

            --Write detail Line Log rows
            l_sec := 'Waiting for Category Import to finish for Request ID:  ' ||
                     l_req_id;
            fnd_file.put_line(fnd_file.log, l_sec);
            fnd_file.put_line(fnd_file.output, l_sec);

            --Wait for Item Import
            IF (l_req_id != 0)
            THEN
              IF fnd_concurrent.wait_for_request(l_req_id
                                                ,v_interval
                                                ,v_max_time
                                                ,v_phase
                                                ,v_status
                                                ,v_dev_phase
                                                ,v_dev_status
                                                ,v_message)
              THEN
                v_error_message := 'ReqID=' || l_req_id || ' DPhase ' ||
                                   v_dev_phase || ' DStatus ' ||
                                   v_dev_status || ' MSG ' || v_message;

                IF v_dev_phase != 'COMPLETE' OR v_dev_status != 'NORMAL'
                THEN
                  l_sec := 'An error occured in Import Categories for please review the Log for concurrent request ' ||
                           l_req_id || ' - ' || v_error_message || '.';
                  fnd_file.put_line(fnd_file.log, l_sec);
                  fnd_file.put_line(fnd_file.output, l_sec);
                ELSE
                  retcode := 0;
                END IF;
              ELSE
                l_sec := 'An error occured in Import Categories please review the Log for concurrent request ' ||
                         l_req_id || ' - ' || v_error_message || '.';
                fnd_file.put_line(fnd_file.log, l_sec);
                fnd_file.put_line(fnd_file.output, l_sec);
              END IF;
            ELSE
              l_sec := 'An error occured when trying to submit Import Categories the concurrent request was not submitted.';
              fnd_file.put_line(fnd_file.log, l_sec);
              fnd_file.put_line(fnd_file.output, l_sec);
            END IF;
          END IF;
          --end version 1.6 11/27/13
        END LOOP;

        --*******************************************
        --Submit the category import process
        --*******************************************
        /*   Version 1.6  11/27/13
        FOR c_category IN (SELECT DISTINCT set_process_id
                             FROM mtl_item_categories_interface
                            WHERE organization_id = l_org_id)
        LOOP

          --Submit Import Process under XXCUS_CON responsitility
          l_sec := 'Submitting Item Category Assignment Open Interface process.';
          fnd_file.put_line(fnd_file.log, l_sec);
          fnd_file.put_line(fnd_file.output, l_sec);

          UPDATE xxcus.xxcus_rebate_aud_tbl
             SET stage = l_sec, stg_date = SYSDATE
           WHERE conc_request_id = nvl(fnd_global.conc_request_id, 0)
             AND procedure_name = l_procedure
             AND trunc(processed_datetime) = trunc(SYSDATE);
          COMMIT;

          --Submite the Concurrent Item Category Import
          l_req_id := fnd_request.submit_request(application => 'INV'
                                                ,program     => 'INV_ITEM_CAT_ASSIGN_OI'
                                                ,description => NULL
                                                ,start_time  => SYSDATE
                                                ,sub_request => FALSE
                                                ,argument1   => c_category.set_process_id
                                                ,argument2   => 1
                                                ,argument3   => 1);

          COMMIT;

          fnd_file.put_line(fnd_file.log, l_req_id);
          fnd_file.put_line(fnd_file.output, l_req_id);

          --Write detail Line Log rows
          l_sec := 'Waiting for Import Items to finish ' || l_req_id;
          fnd_file.put_line(fnd_file.log, l_sec);
          fnd_file.put_line(fnd_file.output, l_sec);

          --Wait for Item Import
          IF (l_req_id != 0)
          THEN
            IF fnd_concurrent.wait_for_request(l_req_id
                                              ,v_interval
                                              ,v_max_time
                                              ,v_phase
                                              ,v_status
                                              ,v_dev_phase
                                              ,v_dev_status
                                              ,v_message)
            THEN
              v_error_message := 'ReqID=' || l_req_id || ' DPhase ' ||
                                 v_dev_phase || ' DStatus ' || v_dev_status ||
                                 ' MSG ' || v_message;

              IF v_dev_phase != 'COMPLETE' OR v_dev_status != 'NORMAL'
              THEN
                l_err_msg := 'An error occured in the running of the Import Categories request ID: ' ||
                             l_req_id || '   ' || v_error_message || '.';
                fnd_file.put_line(fnd_file.log, l_err_msg);
                fnd_file.put_line(fnd_file.output, l_err_msg);
                --RAISE program_error;
              ELSE
                retcode := 0;
              END IF;
            ELSE
              l_err_msg := 'EBS timed out waiting on Import Categories' ||
                           v_error_message || '.';
              fnd_file.put_line(fnd_file.log, l_err_msg);
              fnd_file.put_line(fnd_file.output, l_err_msg);
              --RAISE program_error;
            END IF;
          ELSE
            l_err_msg := 'An error occured when trying to submit the Import Categories';
            fnd_file.put_line(fnd_file.log, l_err_msg);
            fnd_file.put_line(fnd_file.output, l_err_msg);
            RAISE program_error;
          END IF;
        END LOOP;
        */
      END IF;
    END;

    l_sec := 'Finished.';
    fnd_file.put_line(fnd_file.log, l_sec);
    fnd_file.put_line(fnd_file.output, l_sec);

    UPDATE xxcus.xxcus_rebate_aud_tbl
       SET stage = l_sec, stg_date = SYSDATE
     WHERE conc_request_id = nvl(fnd_global.conc_request_id, 0)
       AND procedure_name = l_procedure
       AND trunc(processed_datetime) = trunc(SYSDATE);

    COMMIT;

    --call to check if any product wasn't setup because of missing uom xref
    uom_exceptions(l_bu);

  EXCEPTION
    WHEN program_error THEN
      ROLLBACK;
      l_err_code := 2;
      l_err_msg  := l_err_msg || ' Error_Stack...' ||
                    dbms_utility.format_error_stack() ||
                    ' Error_Backtrace...' ||
                    dbms_utility.format_error_backtrace();
      fnd_file.put_line(fnd_file.log, l_err_msg);
      fnd_file.put_line(fnd_file.output, l_err_msg);
      retcode         := l_err_code;
      errbuf          := l_err_msg;
      l_err_callpoint := l_sec;

      xxcus_error_pkg.xxcus_error_main_api(p_called_from       => l_err_callfrom
                                          ,p_calling           => l_err_callpoint
                                          ,p_request_id        => l_req_id
                                          ,p_ora_error_msg     => substr(regexp_replace(l_err_msg
                                                                                       ,'[[:cntrl:]]'
                                                                                       ,NULL)
                                                                        ,1
                                                                        ,2000)
                                          ,p_error_desc        => substr(l_err_msg
                                                                        ,1
                                                                        ,240)
                                          ,p_distribution_list => l_distro_list
                                          ,p_module            => 'TM');

    WHEN OTHERS THEN
      fnd_file.put_line(fnd_file.log, l_sec);
      fnd_file.put_line(fnd_file.output, l_sec);
      l_err_code := 2;
      l_err_msg  := l_err_msg || ' Error_Stack...' ||
                    dbms_utility.format_error_stack() ||
                    ' Error_Backtrace...' ||
                    dbms_utility.format_error_backtrace();
      fnd_file.put_line(fnd_file.log, l_err_msg);
      fnd_file.put_line(fnd_file.output, l_err_msg);
      retcode         := l_err_code;
      errbuf          := l_err_msg;
      l_err_callpoint := l_sec;

      xxcus_error_pkg.xxcus_error_main_api(p_called_from       => l_err_callfrom
                                          ,p_calling           => l_err_callpoint
                                          ,p_request_id        => l_req_id
                                          ,p_ora_error_msg     => substr(regexp_replace(l_err_msg
                                                                                       ,'[[:cntrl:]]'
                                                                                       ,NULL)
                                                                        ,1
                                                                        ,2000)
                                          ,p_error_desc        => substr(l_err_msg
                                                                        ,1
                                                                        ,240)
                                          ,p_distribution_list => l_distro_list
                                          ,p_module            => 'TM');

  END oneoff_rebate_products;

  /*******************************************************************************
  * Procedure:   ONEOFF_REBATE_RECEIPTS
  * Description: This will load receipts into interface, create a batch bassed on Rebate
  *              vender, process the batch loading into base tables and close the batch
  *              allowing the Third Party Accrual to start running.
  *              Data has been loaded from Sourcing DM but not loaded because of products
  HISTORY
  ===============================================================================
  VERSION DATE          AUTHOR(S)       DESCRIPTION
  ------- -----------   --------------- -----------------------------------------
  1.0     09/08/2010    Kathy Poling    Initial creation of the procedure
  1.2     02/02/2011    Kathy Poling    Changed logic to only load USD receipts until the
                                        business make a decision of how data should be loaded.
  1.3     10/07/2011    Kathy Poling    Changes made to stop duplicates
                                        RFC #32129
  1.4     09/06/2012    Kathy Poling    Changes for branch
  1.5     09/05/2013    Kathy Poling    Changed call to procedure oneoff_uom_mapping RFC 38031
  1.6     09/12/2013    Kathy Poling    Change to map the the LOB Branch
                                        SR 218282
  1.7     09/17/2013    Kathy Poling    Adding source system when looking for item
                                        RFC 38254
  1.8     09/25/2013    Kathy Poling    Change on date invoiced so it more middle of calendar
                                        month.  SR 223933
                                        Change for getting branch calling function to fix WW and WC
                                        Added process to wait for TPA to finish and call to FAE
  1.9     11/28/2013    Kathy Poling    Change to updating the audit table  hint to query
                                        Changed l_sleep to lookup
                                        RFC 38762
  1.10    02/03/2014    Kathy Poling    Remove the call to FAE
                                        SR 236145 RFC 39202
  1.20   10/25/2014   Bala Seshadri     RFC 42038 / ESMS 267539
  2.6     01/02/2015    Kathy Poling    ESMS 240310 made check for trade profile an outer join
  ********************************************************************************/
  PROCEDURE oneoff_rebate_receipts(errbuf      OUT VARCHAR2
                                  ,retcode     OUT NUMBER
                                  ,p_rebt_vndr IN VARCHAR2
                                  ,p_bu_nm     IN VARCHAR2
                                  ,p_fperiod   IN VARCHAR2) IS

    --
    -- Package Variables
    --
    l_fiscal_period_start DATE := NULL;
    n_days                NUMBER := 0;
    --
    --
    v_phase      VARCHAR2(50);
    v_status     VARCHAR2(50);
    v_dev_status VARCHAR2(50);
    --
    v_dev_phase VARCHAR2(50);
    v_message   VARCHAR2(250);
    v_interval  NUMBER := 30; -- In seconds
    --
    v_max_time    NUMBER := 8000; -- In seconds
    l_errorstatus NUMBER;
    l_req_id      NUMBER := NULL;
    v_rec_cnt     NUMBER := 0;
    --
    l_can_submit_request BOOLEAN := TRUE;
    l_globalset          VARCHAR2(100);
    l_err_msg            VARCHAR2(3000);
    l_err_code           NUMBER;
    l_sec                VARCHAR2(255);
    --
    l_count   NUMBER := 0;
    l_success BOOLEAN DEFAULT TRUE;
    --
    l_procedure VARCHAR2(50) := 'ONEOFF_REBATE_RECEIPTS';
    l_receipt   NUMBER;
    --
    l_batch       NUMBER;
    l_high        NUMBER;
    l_low         NUMBER;
    l_months_back NUMBER;
    --
    l_batch_num        VARCHAR2(30);
    l_trnsfr_typ       VARCHAR2(2);
    l_trnsfr_mvmnt_typ VARCHAR2(2);
    l_ordr_ctgry       VARCHAR2(30);
    --
    --
    l_lob_attr  VARCHAR2(30) := 'HDS_LOB';
    l_bu_attr   VARCHAR2(30) := 'HDS_BU';
    l_cust_attr VARCHAR2(30) := 'HDS_LOB_BRANCH';
    --
    l_sleep      NUMBER;
    l_status     NUMBER; --Version 1.8  10/23/13
    l_start_date gl_periods.start_date%TYPE;
    l_end_date   gl_periods.end_date%TYPE;
    --
    l_calendar CONSTANT gl_periods.period_set_name%TYPE := '4-4-QTR';
    l_org NUMBER;
    --
    l_us_org  CONSTANT hr_all_organization_units.organization_id%TYPE := 101; --HDS Rebates USA - Org
    l_cn_org  CONSTANT hr_all_organization_units.organization_id%TYPE := 102; --HDS Rebates CAN - Org
    l_inv_org CONSTANT hr_all_organization_units.organization_id%TYPE := 84; --HDS Rebates USA - Inv
    --
    l_conc_program CONSTANT fnd_concurrent_programs_tl.concurrent_program_id%TYPE := 44848; --Third Party Accrual From Resale Table  --Version 1.8 10/23/2013
    l_category_set CONSTANT mtl_category_sets_tl.category_set_id%TYPE := 1100000041; --HDS Supplier Category
    l_category NUMBER := 1123; --Rebates default item category
    --
    l_fiscal_per_id NUMBER;
    l_user_id       NUMBER;
    --l_user CONSTANT VARCHAR2(100) := 'REBTINTERFACE';
    l_user           VARCHAR2(100);
    l_responsibility VARCHAR2(100); --:= 'XXCUS_CON'; 'HDS Trade Management Administrator - Rebates'
    --
    x_return_status VARCHAR2(1);
    x_msg_count     NUMBER;
    x_msg_data      VARCHAR2(4000);
    l_errorstatus   NUMBER;
    -- Error DEBUG
    l_err_callfrom  VARCHAR2(75) DEFAULT 'XXCUS_TM_INTERFACE_PKG.ONEOFF_REBATE_RECEIPT';
    l_err_callpoint VARCHAR2(75) DEFAULT 'START';
    l_distro_list   VARCHAR2(75) DEFAULT 'HDSOracleDevelopers@hdsupply.com';
    --
    l_selling_price NUMBER;
    l_prod_segment  VARCHAR2(20);
    l_loc_segment   VARCHAR2(20);
    --
    n_tmp_cust_id      NUMBER;
    n_tmp_vndr_unq_nbr VARCHAR2(100);
    --
  BEGIN

    --------------------------------------------------------------------------
    -- Apps Initialize
    --------------------------------------------------------------------------
    l_sec := 'Setting global variables';
    --
    n_days := get_gl_num_of_days;
    --
    l_sec := 'Fetching number of days to calculate date_invoiced /gl_date';
    --
    IF n_days <= 0
    THEN
      --
      print_log('Please update the number of days value in the profile HDS Rebates: GL Number Of Days.');
      RAISE program_error;
    ELSE
     fnd_file.put_line(fnd_file.log, 'Ver 3.4...HDS Rebates: GL Number Of Days [n_days] : '||n_days); --ver 3.4
    END IF;
    --Version 1.9  11/3/2013
    -- SELECT user_id INTO l_user_id FROM fnd_user WHERE user_name = l_user;
    SELECT user_id, user_name
      INTO l_user_id, l_user
      FROM fnd_user
     WHERE user_id = fnd_global.user_id();

    SELECT responsibility_name
      INTO l_responsibility
      FROM fnd_responsibility_vl
     WHERE responsibility_id = fnd_global.resp_id();

    fnd_file.put_line(fnd_file.log, 'Resp Name:  ' || l_responsibility);

    --  Setup parameters for running FND JOBS!
    l_can_submit_request := xxcus_misc_pkg.set_responsibility(l_user
                                                             ,l_responsibility);
    IF l_can_submit_request
    THEN
      l_sec := 'Global Variables are set User Name:  ' || l_user ||
               ' Resp Name:  ' || l_responsibility;
      fnd_file.put_line(fnd_file.log, l_sec);
      fnd_file.put_line(fnd_file.output, l_sec);

    ELSE

      l_sec := 'Global Variables are not set for User Name:  ' || l_user ||
               ' Resp Name:  ' || l_responsibility;
      fnd_file.put_line(fnd_file.log, l_sec);
      fnd_file.put_line(fnd_file.output, l_sec);
      RAISE program_error;
    END IF;

    l_sec := 'Ready to start process';
    fnd_file.put_line(fnd_file.log
                     ,'Start time:  ' ||
                      to_char(SYSDATE, 'DD-MON-YYYY HH24:MI:SS') ||
                      ' for ' || l_sec);
    fnd_file.put_line(fnd_file.output
                     ,'Start time:  ' ||
                      to_char(SYSDATE, 'DD-MON-YYYY HH24:MI:SS') ||
                      ' for ' || l_sec);

    BEGIN
      SELECT to_number(meaning)
        INTO l_high
        FROM fnd_lookup_values_vl
       WHERE lookup_type = 'HDS_TM_RECEIPT_PARAMETERS'
         AND lookup_code = 'RECPT_HIGH'
         AND view_application_id = 682; --Trade Management
     fnd_file.put_line(fnd_file.log, 'Ver 3.4...l_high : '||l_high);    --ver 3.4
    EXCEPTION
      WHEN no_data_found THEN
        l_err_msg := 'Failed to find receipt high value : ' || p_fperiod ||
                     ' : ' || SQLERRM;
        fnd_file.put_line(fnd_file.log, l_err_msg);
        RAISE program_error;
    END;

    BEGIN
      SELECT to_number(meaning)
        INTO l_low
        FROM fnd_lookup_values_vl
       WHERE lookup_type = 'HDS_TM_RECEIPT_PARAMETERS'
         AND lookup_code = 'RECPT_LOW'
         AND view_application_id = 682; --Trade Management
     fnd_file.put_line(fnd_file.log, 'Ver 3.4...l_low : '||l_low);    --ver 3.4
    EXCEPTION
      WHEN no_data_found THEN
        l_err_msg := 'Failed to find receipt low value : ' || p_fperiod ||
                     ' : ' || SQLERRM;
        fnd_file.put_line(fnd_file.log, l_err_msg);
        RAISE program_error;
    END;

    BEGIN
      SELECT to_number(meaning)
        INTO l_months_back
        FROM fnd_lookup_values_vl
       WHERE lookup_type = 'HDS_TM_RECEIPT_PARAMETERS'
         AND lookup_code = 'MONTHS_BACK'
         AND view_application_id = 682; --Trade Management
     fnd_file.put_line(fnd_file.log, 'Ver 3.4...l_months_back : '||l_months_back);   --ver 3.4
    EXCEPTION
      WHEN no_data_found THEN
        l_err_msg := 'Failed to find receipt high value  ' || SQLERRM;
        fnd_file.put_line(fnd_file.log, l_err_msg);
        RAISE program_error;
    END;

    --lookup for sleep time waiting for TPA to complete
    --Version 1.9
    BEGIN
      SELECT to_number(meaning)
        INTO l_sleep
        FROM fnd_lookup_values_vl
       WHERE lookup_type = 'HDS_TM_RECEIPT_PARAMETERS'
         AND lookup_code = 'SLEEP_TIME'
         AND view_application_id = 682 --Trade Management
         AND enabled_flag = 'Y'
         AND nvl(end_date_active, SYSDATE) >= SYSDATE;
     fnd_file.put_line(fnd_file.log, 'Ver 3.4...l_sleep : '||l_sleep); --ver 3.4
    EXCEPTION
      WHEN no_data_found THEN
        l_err_msg := 'Failed to find value for sleep time: ' || p_fperiod ||
                     ' : ' || SQLERRM;
        fnd_file.put_line(fnd_file.log, l_err_msg);
        RAISE program_error;
    END;

    --Version 1.1 changed start date to look back 2 full calendar years
    BEGIN
      SELECT start_date
        INTO l_start_date
        FROM gl.gl_periods
       WHERE period_set_name = l_calendar
         AND period_num <> 13
         AND period_year || lpad(period_num, 2, 0) =
             to_number(to_char(add_months('01-' || p_fperiod
                                         , --SYSDATE,
                                          l_months_back)
                              ,'YYYY') || '11');

      fnd_file.put_line(fnd_file.log, 'Start Date [l_start_date] :  ' || l_start_date); --ver 3.4
    EXCEPTION
      WHEN no_data_found THEN
        l_err_msg := 'Failed on start date: ' || ' : ' || SQLERRM;
        fnd_file.put_line(fnd_file.log, l_err_msg);
        RAISE program_error;
    END;

    BEGIN
      SELECT trunc(end_date)
            ,trunc(start_date)
            ,period_year || lpad(period_num, 2, 0)
        INTO l_end_date, l_fiscal_period_start, l_fiscal_per_id
        FROM gl.gl_periods
       WHERE period_set_name = l_calendar
         AND period_name = p_fperiod
         AND period_num <> 13;

      fnd_file.put_line(fnd_file.log
                       ,'End Date [l_end_date] :  ' || l_end_date || ' Fiscal Period:  ' || --ver 3.4
                        l_fiscal_per_id || ' l_fiscal_period_start =' ||
                        to_char(l_fiscal_period_start, 'mm/dd/yyyy'));
    EXCEPTION
      WHEN no_data_found THEN
        l_err_msg := 'Failed to start and end date for period : ' ||
                     p_fperiod || ' : ' || SQLERRM;
        fnd_file.put_line(fnd_file.log, l_err_msg);
        RAISE program_error;
    END;

    --loading new or updated uom from apex
    l_sec := 'Load UOM xref data from Apex.';
    fnd_file.put_line(fnd_file.log, l_sec);
    fnd_file.put_line(fnd_file.output, l_sec);

    fnd_file.put_line(fnd_file.log
                     ,'Start time:  ' ||
                      to_char(SYSDATE, 'DD-MON-YYYY HH24:MI:SS') ||
                      ' for ' || l_sec);
    fnd_file.put_line(fnd_file.output
                     ,'Start time:  ' ||
                      to_char(SYSDATE, 'DD-MON-YYYY HH24:MI:SS') ||
                      ' for ' || l_sec);

    load_uom_mapping(p_bu_nm); --Version 1.5  9/5/13

    fnd_file.put_line(fnd_file.log
                     ,'End time:  ' ||
                      to_char(SYSDATE, 'DD-MON-YYYY HH24:MI:SS') ||
                      ' for ' || l_sec);
    fnd_file.put_line(fnd_file.output
                     ,'End time:  ' ||
                      to_char(SYSDATE, 'DD-MON-YYYY HH24:MI:SS') ||
                      ' for ' || l_sec);

    INSERT INTO xxcus.xxcus_rebate_aud_tbl
    VALUES
      (nvl(fnd_global.conc_request_id, 0)
      ,l_procedure
      ,SYSDATE
      ,nvl(fnd_global.user_name, 'NA')
      ,nvl(fnd_global.resp_name, 'NA')
      ,p_fperiod || '~' || p_rebt_vndr || '~' || p_bu_nm
      ,l_fiscal_per_id
      ,'Beginning Program - oneoff receipt loads'
      ,SYSDATE
      ,l_count
      ,NULL
      ,p_bu_nm);

    COMMIT;

    --fnd_file.put_line(fnd_file.log, '09/02/2014 -100');

    l_sec := 'Grouping receipts by Rebate Vendor and receipt date for the period; ';
    fnd_file.put_line(fnd_file.log
                     ,'Start time:  ' ||
                      to_char(SYSDATE, 'DD-MON-YYYY HH24:MI:SS') ||
                      ' for ' || l_sec);
    fnd_file.put_line(fnd_file.output
                     ,'Start time:  ' ||
                      to_char(SYSDATE, 'DD-MON-YYYY HH24:MI:SS') ||
                      ' for ' || l_sec);

    UPDATE xxcus.xxcus_rebate_aud_tbl
       SET stage = l_sec, stg_date = SYSDATE
     WHERE (conc_request_id = nvl(fnd_global.conc_request_id, 0) OR
           trunc(processed_datetime) = trunc(SYSDATE)) --Version 1.9
       AND procedure_name = l_procedure
       AND fiscal_period =
           p_fperiod || '~' || p_rebt_vndr || '~' || p_bu_nm
       AND post_run_id = l_fiscal_per_id
       AND nvl(bu_nm, 'x') = nvl(p_bu_nm, 'x') --Version 1.9 added nvl
       AND user_name = nvl(fnd_global.user_name, 'NA') --Version 1.9
       AND resp_name = nvl(fnd_global.resp_name, 'NA'); --Version 1.9

    COMMIT;
    --fnd_file.put_line(fnd_file.log, '09/02/2014 -101');
    FOR c_group IN (SELECT /*+ PARALLEL (AUTO) */ --Version 1.9
                     p.party_id
                    ,p.party_name
                    ,c.cust_account_id
                    ,rebt_vndr_cd
                    ,recpt_crt_dt
                    ,curnc_flg
                    ,COUNT(*)
                      FROM xxcus.xxcus_rebate_receipt_curr_tbl r
                          ,ar.hz_cust_accounts                 c
                          ,ar.hz_parties                       p
                     WHERE bu_nm = nvl(p_bu_nm, bu_nm)
                       AND r.recpt_crt_dt BETWEEN l_start_date AND
                           l_end_date
                          --r.status_flag <> 'P'  --Version 1.3
                          --r.receipt IS NULL
                       AND r.status_flag = 'N'
                       AND r.rebt_vndr_cd || '~MSTR' = c.account_number
                       AND c.party_id = p.party_id
                     GROUP BY rebt_vndr_cd
                             ,recpt_crt_dt
                             ,c.cust_account_id
                             ,p.party_id
                             ,p.party_name
                             ,r.curnc_flg)
    LOOP

      SELECT xxcus_receipt_id_s.nextval INTO l_receipt FROM dual;

      UPDATE xxcus.xxcus_rebate_receipt_curr_tbl
         SET receipt         = l_receipt
            ,cust_account_id = c_group.cust_account_id
            ,party_id        = c_group.party_id
            ,party_name      = c_group.party_name
       WHERE recpt_crt_dt BETWEEN l_start_date AND l_end_date
         AND status_flag = 'N'
            --AND status_flag <> 'P'  Version 1.3
            --AND receipt IS NULL
            --and shp_to_brnch_nbr = c_group.shp_to_brnch_nbr
         AND rebt_vndr_cd = c_group.rebt_vndr_cd
            --and po_nbr = c_group.po_nbr
         AND recpt_crt_dt = c_group.recpt_crt_dt
         AND curnc_flg = c_group.curnc_flg;

    END LOOP;
    COMMIT;
    --fnd_file.put_line(fnd_file.log, '09/02/2014 -102');
    fnd_file.put_line(fnd_file.log
                     ,'End time:  ' ||
                      to_char(SYSDATE, 'DD-MON-YYYY HH24:MI:SS') ||
                      ' for ' || l_sec);
    fnd_file.put_line(fnd_file.output
                     ,'End time:  ' ||
                      to_char(SYSDATE, 'DD-MON-YYYY HH24:MI:SS') ||
                      ' for ' || l_sec);

    --start loading receipts into resale interface

    l_sec := 'Loading receipts into resale iface for the period ' ||
             p_fperiod || '  Report date between ' || l_start_date ||
             ' and ' || l_end_date;
    fnd_file.put_line(fnd_file.log
                     ,'Start time:  ' ||
                      to_char(SYSDATE, 'DD-MON-YYYY HH24:MI:SS') ||
                      ' for ' || l_sec);
    fnd_file.put_line(fnd_file.output
                     ,'Start time:  ' ||
                      to_char(SYSDATE, 'DD-MON-YYYY HH24:MI:SS') ||
                      ' for ' || l_sec);

    UPDATE xxcus.xxcus_rebate_aud_tbl
       SET stage = l_sec, stg_date = SYSDATE --, processed_datetime = sysdate
     WHERE (conc_request_id = nvl(fnd_global.conc_request_id, 0) OR
           trunc(processed_datetime) = trunc(SYSDATE)) --Version 1.9
       AND procedure_name = l_procedure
       AND fiscal_period =
           p_fperiod || '~' || p_rebt_vndr || '~' || p_bu_nm
       AND post_run_id = l_fiscal_per_id
       AND nvl(bu_nm, 'x') = nvl(p_bu_nm, 'x') --Version 1.9 added nvl
       AND user_name = nvl(fnd_global.user_name, 'NA') --Version 1.9
       AND resp_name = nvl(fnd_global.resp_name, 'NA'); --Version 1.9

    COMMIT;
    --fnd_file.put_line(fnd_file.log, '09/02/2014 -103');
    BEGIN
      --
      BEGIN
        FOR c_receipt IN (SELECT /*+ PARALLEL(AUTO) */
                           r.vndr_recpt_unq_nbr
                          ,r.pt_vndr_cd
                          ,r.shp_to_brnch_cd
                          ,r.geo_loc_hier_lob_nm
                          ,r.bu_nm
                          ,r.bu_cd
                          ,r.src_sys_nm
                          ,r.drop_shp_flg
                          ,r.po_nbr
                          ,r.recpt_crt_dt
                          ,r.vndr_part_nbr
                          ,(CASE
                             WHEN r.item_cost_amt < 0 THEN
                              r.item_cost_amt * -1
                             ELSE
                              r.item_cost_amt
                           END) item_cost_amt
                          ,(CASE
                             WHEN r.item_cost_amt < 0 THEN
                              r.recpt_qty * -1
                             ELSE
                              r.recpt_qty
                           END) / (CASE
                             WHEN buy_pkg_unt_cnt IS NULL THEN
                              1
                             WHEN buy_pkg_unt_cnt = 0 THEN
                              1
                             ELSE
                              buy_pkg_unt_cnt
                           END) recpt_qty
                          ,(CASE
                             WHEN buy_pkg_unt_cnt IS NULL THEN
                              1
                             WHEN buy_pkg_unt_cnt = 0 THEN
                              1
                             ELSE
                              buy_pkg_unt_cnt
                           END) buy_pkg_unt_cnt
                          ,(CASE
                             WHEN item_cost_amt < 0 THEN
                              item_cost_amt * -1
                             ELSE
                              item_cost_amt
                           END) * ((CASE
                             WHEN item_cost_amt < 0 THEN
                              recpt_qty * -1
                             ELSE
                              recpt_qty
                           END) / (CASE
                             WHEN buy_pkg_unt_cnt IS NULL THEN
                              1
                             WHEN buy_pkg_unt_cnt = 0 THEN
                              1
                             ELSE
                              buy_pkg_unt_cnt
                           END)) total_cost
                          ,decode(r.curnc_flg, 'CND', 'CAD', 'USD') curnc_flg
                           --,to_char(r.receipt) receipt
                          ,r.receipt receipt
                          ,r.prod_id
                          ,r.sku_cd
                          ,i.inventory_item_id
                          ,i.segment1
                          ,i.description
                          ,r.prod_uom_cd
                          ,i.primary_uom_code
                          ,xxcus_tm_interface_pkg.get_branch(r.bu_nm
                                                            ,r.shp_to_brnch_cd
                                                            ,r.geo_loc_hier_lob_nm) bill_to_cust_acct_id --Version  1.8
                          ,bp.party_id bill_to_party_id
                          ,bp.party_name bill_to_party_name
                          ,ps.party_site_id
                          ,r.cust_account_id
                          ,r.party_id
                          ,r.party_name
                          ,(SELECT category_id --Version 1.3
                              FROM apps.mtl_item_categories_v c
                             WHERE c.category_set_id = l_category_set
                               AND c.organization_id = l_inv_org
                               AND c.inventory_item_id = i.inventory_item_id
                               AND c.segment1 = r.rebt_vndr_cd) category_id
                           --,c.category_id                                  --Version 1.3
                          ,cp.cust_acct_id
                          ,to_number(nvl(TRIM(regexp_replace(octp.attribute2
                                                            ,'([^[:print:]])'
                                                            ,' '))
                                        ,'0')) freight_discount
                          ,to_number(nvl(TRIM(regexp_replace(octp.attribute3
                                                            ,'([^[:print:]])'
                                                            ,' '))
                                        ,'0')) pmt_discount
                           --,to_number(nvl(octp.attribute2, '0')) freight_discount --added by NS
                           --,to_number(nvl(octp.attribute3, '0')) pmt_discount ---added by NS
                          ,xref.oracle_uom_code
                          ,hp1.party_name       bu_name
                          ,hp1.party_id         bu_id
                            FROM xxcus.xxcus_rebate_receipt_curr_tbl r
                                 --,apps.xxcus_rebate_branch_vw         xrbv --Version 1.4    Version 1.8
                                ,inv.mtl_system_items_b i
                                 --,inv.mtl_item_categories                      c --Version 1.3
                                ,apps.xxcus_cust_prod_rebt_v cp
                                ,ar.hz_parties               bp
                                ,ar.hz_parties               hp1 ---added by NS
                                ,ar.hz_relationships         rel ---added by NS
                                ,ar.hz_party_sites           ps
                                ,ozf_cust_trd_prfls_all      octp ---added by NS
                                ,xxcus.xxcus_rebate_uom_tbl  xref
                           WHERE r.sku_cd || '~' || r.bu_nm || '~' ||
                                 r.src_sys_nm --Version 1.7   9/17/13
                                 = i.description --i.segment1
                             AND i.organization_id = l_inv_org
                                --AND i.enabled_flag = 'Y' --commented 09/02/2014
                             AND i.inventory_item_status_code = 'Active' --added 09/20/2014
                                --AND i.inventory_item_id = c.inventory_item_id    --Version 1.3
                                --AND c.organization_id = l_inv_org                --Version 1.3
                                --AND c.category_set_id = 1100000041               --Version 1.3
                             AND r.status_flag = 'N' --<> 'P'   Version 1.3
                             AND r.receipt IS NOT NULL
                             AND nvl(r.item_cost_amt, 0) <> 0
                             AND nvl(r.recpt_qty, 0) <> 0
                                --AND r.shp_to_brnch_cd = xrbv.oper_branch(+) --Version 1.4    --Version 1.8
                                --AND r.bu_nm = xrbv.dw_bu_desc(+) --Version 1.4               --Version 1.8
                             AND r.cust_account_id = octp.cust_account_id(+) ---added by NS  --Version 2.6  1/2/15
                             AND decode(r.curnc_flg, 'CND', 'CAD', 'USD') =
                                 octp.claim_currency(+) --Version 2.6  1/2/15
                             AND r.cust_account_id = cp.cust_acct_id(+)
                             AND r.recpt_crt_dt BETWEEN l_start_date AND
                                 l_end_date
                             AND r.geo_loc_hier_lob_nm = bp.known_as
                             AND bp.attribute1 = l_lob_attr
                             AND bp.party_id = rel.object_id
                             AND rel.relationship_code = 'DIVISION_OF'
                             AND rel.relationship_type =
                                 'HEADQUARTERS/DIVISION'
                             AND rel.subject_id = hp1.party_id
                             AND hp1.attribute1 = l_bu_attr
                             AND hp1.party_name = r.bu_nm --added 10-24
                             AND rel.status = 'A'
                             AND rel.directional_flag IN ('B', 'F')
                             AND r.pt_vndr_cd || '~' || r.bu_cd =
                                 ps.party_site_number
                             AND nvl(r.prod_uom_cd, 'UNKNOWN') =
                                 xref.purch_uom_cd --only items with a valid uom
                             AND r.bu_nm = xref.bu_nm
                             AND upper(r.src_sys_nm) =
                                 upper(xref.src_sys_nm)
                             AND r.bu_nm = nvl(p_bu_nm, r.bu_nm)
                             AND r.rebt_vndr_cd || '~MSTR' =
                                 nvl(p_rebt_vndr, r.rebt_vndr_cd|| '~MSTR') --ver 3.4
                          --AND r.curnc_flg = 'USD'  --Version 1.2 limit receipts to load only USD
                          )
        LOOP
          --
          n_tmp_cust_id      := c_receipt.cust_account_id;
          n_tmp_vndr_unq_nbr := c_receipt.vndr_recpt_unq_nbr;
           fnd_file.put_line(fnd_file.log, 'Ver 3.4...Vendor Unique Receipt# :  ' || n_tmp_vndr_unq_nbr); --ver 3.4
          --
          --
          BEGIN
            --
            IF c_receipt.recpt_qty < 0
            THEN
              l_ordr_ctgry       := 'RETURN';
              l_trnsfr_typ       := 'BN';
              l_trnsfr_mvmnt_typ := 'CD';
            ELSE
              l_ordr_ctgry       := 'ORDER';
              l_trnsfr_typ       := 'SS';
              l_trnsfr_mvmnt_typ := 'DC';
            END IF;
          EXCEPTION
            WHEN OTHERS THEN
              fnd_file.put_line(fnd_file.log
                               ,'@09/02/2014 -103:B1, cust_account_id =' ||
                                n_tmp_cust_id || ', vndr_recpt_unq_nbr =' ||
                                n_tmp_vndr_unq_nbr);
              fnd_file.put_line(fnd_file.log
                               ,'09/02/2014 -103:B1, Msg =' || SQLERRM);
          END;
          --
          BEGIN
            IF c_receipt.curnc_flg = 'USD'
            THEN
              l_org := l_us_org;
            ELSE
              l_org := l_cn_org;
            END IF;
          EXCEPTION
            WHEN OTHERS THEN
              fnd_file.put_line(fnd_file.log
                               ,'@103:B2, cust_account_id =' ||
                                n_tmp_cust_id || ', vndr_recpt_unq_nbr =' ||
                                n_tmp_vndr_unq_nbr);
              fnd_file.put_line(fnd_file.log, '@103:B2, Msg =' || SQLERRM);
          END;
          --
          BEGIN
            IF c_receipt.freight_discount IS NOT NULL OR
               c_receipt.pmt_discount IS NOT NULL
            THEN
              l_selling_price := c_receipt.item_cost_amt -
                                 ((c_receipt.freight_discount +
                                 c_receipt.pmt_discount) *
                                 c_receipt.item_cost_amt) / 100; --- added by NS
            ELSE
              l_selling_price := c_receipt.item_cost_amt;
            END IF;
          EXCEPTION
            WHEN OTHERS THEN
              fnd_file.put_line(fnd_file.log
                               ,'@103:B3, cust_account_id =' ||
                                n_tmp_cust_id || ', vndr_recpt_unq_nbr =' ||
                                n_tmp_vndr_unq_nbr);
              fnd_file.put_line(fnd_file.log, '@103:B3, Msg =' || SQLERRM);
          END;
          --
          ---added by NS
          BEGIN
            get_gl_segments(c_receipt.bu_nm
                           ,c_receipt.shp_to_brnch_cd
                           ,l_prod_segment
                           ,l_loc_segment);
          EXCEPTION
            WHEN OTHERS THEN
              fnd_file.put_line(fnd_file.log
                               ,'103:A, bu_nm =' || c_receipt.bu_nm ||
                                ',shp_to_brnch_cd =' ||
                                c_receipt.shp_to_brnch_cd ||
                                ', l_prod_segment =' || l_prod_segment ||
                                ', l_loc_segment =' || l_loc_segment);
              fnd_file.put_line(fnd_file.log
                               ,'@103:A, cust_account_id =' ||
                                n_tmp_cust_id || ', vndr_recpt_unq_nbr =' ||
                                n_tmp_vndr_unq_nbr);
              fnd_file.put_line(fnd_file.log, '@103:A, Msg =' || SQLERRM);
          END;
          --
          BEGIN
            --
            IF c_receipt.cust_acct_id IS NULL OR
               (c_receipt.cust_acct_id IS NOT NULL AND
               nvl(c_receipt.category_id, 1123) <> l_category)
            THEN
              IF c_receipt.recpt_qty > 0 AND c_receipt.total_cost >= l_high OR
                 c_receipt.recpt_qty < 0 AND c_receipt.total_cost <= l_low
              THEN
                --
                BEGIN
                  --
                   fnd_file.put_line(fnd_file.log, 'Ver 3.4...Before insert into resale lines for receipt  ' ||n_tmp_vndr_unq_nbr); --ver 3.4
                  INSERT INTO ozf_resale_lines_int_all
                    (resale_line_int_id
                    ,object_version_number
                    ,last_update_date
                    ,last_updated_by
                    ,creation_date
                    ,created_by
                    ,resale_batch_id
                    ,status_code
                    ,resale_transfer_type
                    ,product_transfer_movement_type
                    ,tracing_flag
                    ,ship_from_cust_account_id
                    ,ship_from_party_name
                    ,ship_from_site_id
                    ,sold_from_cust_account_id
                    ,sold_from_party_name
                    ,sold_from_site_id
                    ,bill_to_cust_account_id
                    ,bill_to_party_id
                    ,bill_to_party_name
                    ,
                     --BILL_TO_PARTY_SITE_ID,
                     end_cust_party_name
                    ,end_cust_party_id
                    ,direct_customer_flag
                    ,order_category
                    ,currency_code
                    ,claimed_amount
                    ,allowed_amount
                    ,total_allowed_amount
                    ,total_claimed_amount
                    ,uom_code
                    ,quantity
                    ,calculated_price
                    ,calculated_amount
                    ,org_id
                    ,data_source_code
                    ,order_number
                    ,date_ordered
                    ,item_number
                    ,inventory_item_id
                    ,response_code
                    ,acctd_calculated_price
                    ,selling_price
                    ,acctd_selling_price
                    ,purchase_price
                    ,purchase_uom_code
                    ,total_accepted_amount
                    ,accepted_amount
                    ,po_number
                    ,line_attribute5
                    ,line_attribute1 --pay-to vendor code
                    ,line_attribute2
                    ,line_attribute3
                    ,line_attribute6
                    ,line_attribute11
                    ,line_attribute12
                    ,line_attribute13
                    ,line_attribute14
                    ,date_invoiced) --01/30/13  gl date logic added
                  VALUES
                    (ozf_resale_lines_int_all_s.nextval
                    ,1
                    ,SYSDATE
                    ,l_user_id
                    ,SYSDATE
                    ,l_user_id
                    ,l_batch
                    ,'OPEN'
                    ,l_trnsfr_typ
                    ,l_trnsfr_mvmnt_typ
                    ,'T'
                    ,c_receipt.cust_account_id
                    ,c_receipt.party_name
                    ,c_receipt.party_site_id
                    ,c_receipt.cust_account_id
                    ,c_receipt.party_name
                    ,c_receipt.party_site_id
                     --,nvl(c_receipt.bill_to_cust_acct_id ,c_receipt.drpshp_cust_acct_id)    --Version 1.8
                    ,c_receipt.bill_to_cust_acct_id --Version 1.8
                    ,c_receipt.bill_to_party_id
                    ,c_receipt.bill_to_party_name
                    ,
                     --c_receipt.party_site_id,
                     c_receipt.bu_name
                    ,c_receipt.bu_id
                    ,'T'
                    ,l_ordr_ctgry
                    , --'ORDER',
                     c_receipt.curnc_flg
                    ,0
                    ,0
                    ,0
                    ,0
                    ,c_receipt.primary_uom_code
                    ,c_receipt.recpt_qty
                    ,0
                    ,0
                    ,l_org
                    ,'ORCL_DW'
                    ,c_receipt.receipt
                    ,c_receipt.recpt_crt_dt
                    ,c_receipt.segment1
                    ,c_receipt.inventory_item_id
                    ,'Y'
                    ,0
                    ,l_selling_price
                    ,l_selling_price
                    ,c_receipt.item_cost_amt
                    ,nvl(c_receipt.oracle_uom_code, 'UNK')
                    ,0
                    ,0
                    ,c_receipt.po_nbr
                    ,c_receipt.vndr_recpt_unq_nbr
                    ,c_receipt.pt_vndr_cd
                    ,c_receipt.bu_nm
                    ,c_receipt.src_sys_nm
                    ,c_receipt.shp_to_brnch_cd
                    ,l_prod_segment
                    ,l_loc_segment
                    ,c_receipt.freight_discount
                    ,c_receipt.pmt_discount
                     --,l_end_date - 10 --Version 1.8  adding the -10 days --commented as per ESMS 267539
                    ,l_fiscal_period_start + n_days --Added as per ESMS 267539
                     ); --Version 1.8  adding the -10 days
                   fnd_file.put_line(fnd_file.log, 'Ver 3.4...After insert into resale lines for receipt  ' ||n_tmp_vndr_unq_nbr); --ver 3.4
                  --
                EXCEPTION
                  WHEN OTHERS THEN
                    --
                    fnd_file.put_line(fnd_file.log
                                     ,'@104:A, c_receipt.cust_account_id =' ||
                                      c_receipt.cust_account_id);
                    fnd_file.put_line(fnd_file.log
                                     ,'@104:A, Msg =' || SQLERRM);
                    --
                END;
                --
                v_rec_cnt := v_rec_cnt + 1;
                BEGIN
                  UPDATE xxcus.xxcus_rebate_receipt_curr_tbl
                     SET status_flag = 'P', process_date = SYSDATE
                  --,comments     = 'INTERFACED'
                   WHERE vndr_recpt_unq_nbr = c_receipt.vndr_recpt_unq_nbr
                     AND bu_nm = c_receipt.bu_nm
                     AND status_flag = 'N';
                   fnd_file.put_line(fnd_file.log, 'Ver 3.4...After updating status flag to P for receipt  ' ||n_tmp_vndr_unq_nbr); --ver 3.4
                EXCEPTION
                  WHEN OTHERS THEN
                    fnd_file.put_line(fnd_file.log
                                     ,'@105, c_receipt.vndr_recpt_unq_nbr =' ||
                                      c_receipt.vndr_recpt_unq_nbr ||
                                      ',bu_nm =' || c_receipt.bu_nm);
                    fnd_file.put_line(fnd_file.log
                                     ,'@105, Msg =' || SQLERRM);
                END;
                -- --fnd_file.put_line(fnd_file.log, '09/02/2014 -104:B, @vndr_recpt_unq_nbr ='||c_receipt.vndr_recpt_unq_nbr);
              END IF;
            END IF;
            --
          EXCEPTION
            WHEN OTHERS THEN
              fnd_file.put_line(fnd_file.log
                               ,',cust_account_id =' || n_tmp_cust_id ||
                                ', vndr_recpt_unq_nbr =' ||
                                n_tmp_vndr_unq_nbr);
              fnd_file.put_line(fnd_file.log, '@103:B4, Msg =' || SQLERRM);
          END;
          --
        END LOOP;
      EXCEPTION
        WHEN OTHERS THEN
          fnd_file.put_line(fnd_file.log
                           ,',cust_account_id =' || n_tmp_cust_id ||
                            ', vndr_recpt_unq_nbr =' || n_tmp_vndr_unq_nbr);
          fnd_file.put_line(fnd_file.log, '@103:C, Msg =' || SQLERRM);
      END;
      --
      COMMIT;
      --call to get list of sku codes for the email that have not been classified
      -- product_exceptions(p_bu_nm);

      fnd_file.put_line(fnd_file.log
                       ,'End time:  ' ||
                        to_char(SYSDATE, 'DD-MON-YYYY HH24:MI:SS') ||
                        'for ' || l_sec);
      fnd_file.put_line(fnd_file.output
                       ,'End time:  ' ||
                        to_char(SYSDATE, 'DD-MON-YYYY HH24:MI:SS') ||
                        'for ' || l_sec);

      --COMMIT;

      l_sec := 'Receipts Loaded...= ' || v_rec_cnt;
      fnd_file.put_line(fnd_file.log, l_sec);
      fnd_file.put_line(fnd_file.output, l_sec);
      dbms_output.put_line('Receipts created...= ' || v_rec_cnt);
      BEGIN
        UPDATE xxcus.xxcus_rebate_aud_tbl
           SET stage = l_sec, stg_date = SYSDATE
         WHERE (conc_request_id = nvl(fnd_global.conc_request_id, 0) OR
               trunc(processed_datetime) = trunc(SYSDATE)) --Version 1.9
           AND procedure_name = l_procedure
           AND fiscal_period =
               p_fperiod || '~' || p_rebt_vndr || '~' || p_bu_nm --Version 1.9
           AND post_run_id = l_fiscal_per_id
           AND nvl(bu_nm, 'x') = nvl(p_bu_nm, 'x') --Version 1.9 added nvl
           AND user_name = nvl(fnd_global.user_name, 'NA') --Version 1.9
           AND resp_name = nvl(fnd_global.resp_name, 'NA'); --Version 1.9
      EXCEPTION
        WHEN OTHERS THEN
          fnd_file.put_line(fnd_file.log
                           ,'@106:A, fiscal_period =' || p_fperiod || '~' ||
                            p_rebt_vndr || '~' || p_bu_nm ||
                            ',post_run_id =' || l_fiscal_per_id ||
                            ', l_procedure =' || l_procedure);
          fnd_file.put_line(fnd_file.log, '@106:A, Msg =' || SQLERRM);
      END;
      --
      fnd_file.put_line(fnd_file.log, '@106:B');
      --
    EXCEPTION
      WHEN OTHERS THEN
        l_err_msg := 'Unexpected Error Occured in Section :' || '::ERROR :' ||
                     SQLERRM;
        fnd_file.put_line(fnd_file.output, l_err_msg);
        fnd_file.put_line(fnd_file.log, l_err_msg);
        --l_success := FALSE;
        ROLLBACK;
    END;
    --
    fnd_file.put_line(fnd_file.log, '@107');
    --insert batch
    BEGIN

      l_sec := 'Ready to create batches for receipts loaded.';
      fnd_file.put_line(fnd_file.log
                       ,'Start time:  ' ||
                        to_char(SYSDATE, 'DD-MON-YYYY HH24:MI:SS') ||
                        ' for ' || l_sec);
      fnd_file.put_line(fnd_file.output
                       ,'Start time:  ' ||
                        to_char(SYSDATE, 'DD-MON-YYYY HH24:MI:SS') ||
                        ' for ' || l_sec);

      FOR c_batch IN (SELECT /*+ PARALLEL(AUTO) */ --Version 1.9
                      DISTINCT i.sold_from_cust_account_id
                              ,i.object_version_number
                              ,i.org_id
                              ,i.data_source_code
                              ,p.party_id
                              ,i.currency_code
                              ,COUNT(*) batch_count
                              ,i.end_cust_party_name --Version 1.8
                              ,get_offer_type(i.sold_from_cust_account_id) offer_type --Version 1.9  10/28/13
                        FROM ozf_resale_lines_int_all i, hz_parties p
                       WHERE i.status_code = 'OPEN'
                         AND i.data_source_code = 'ORCL_DW'
                         AND i.sold_from_party_name = p.party_name
                         AND p.attribute1 = 'HDS_MVID'
                         AND i.resale_batch_id IS NULL
                       GROUP BY sold_from_cust_account_id
                               ,i.object_version_number
                               ,i.org_id
                               ,data_source_code
                               ,p.party_id
                               ,i.currency_code
                               ,i.end_cust_party_name)
      LOOP

        BEGIN

          l_batch     := NULL;
          l_batch_num := NULL;
          l_success   := TRUE;

          SELECT ozf_resale_batches_all_s.nextval INTO l_batch FROM dual;

          SELECT to_char(ozf_resale_batch_number_s.nextval)
            INTO l_batch_num
            FROM dual;

          l_sec := 'Calling api to insert batch for batch ID: ' || l_batch;

          ozf_resale_batches_pkg.insert_row(px_resale_batch_id           => l_batch
                                           ,px_object_version_number     => c_batch.object_version_number
                                           ,p_last_update_date           => SYSDATE
                                           ,p_last_updated_by            => l_user_id
                                           ,p_creation_date              => SYSDATE
                                           ,p_request_id                 => NULL
                                           ,p_created_by                 => l_user_id
                                           ,p_created_from               => NULL
                                           ,p_last_update_login          => NULL
                                           ,p_program_application_id     => NULL
                                           ,p_program_update_date        => NULL
                                           ,p_program_id                 => NULL
                                           ,p_batch_number               => l_batch_num
                                           ,p_batch_type                 => 'TRACING'
                                           ,p_batch_count                => c_batch.batch_count
                                           ,p_year                       => NULL
                                           ,p_month                      => NULL
                                           ,p_report_date                => trunc(SYSDATE)
                                           ,p_report_start_date          => l_start_date
                                           ,p_report_end_date            => l_end_date
                                           ,p_status_code                => 'OPEN'
                                           ,p_data_source_code           => c_batch.data_source_code
                                           , -- BUG 5077213
                                            p_reference_type             => NULL
                                           ,p_reference_number           => NULL
                                           ,p_comments                   => NULL
                                           ,p_partner_claim_number       => NULL
                                           ,p_transaction_purpose_code   => '00'
                                           ,p_transaction_type_code      => '01'
                                           ,p_partner_type               => 'DS'
                                           ,p_partner_id                 => NULL
                                           ,p_partner_party_id           => c_batch.party_id
                                           ,p_partner_cust_account_id    => c_batch.sold_from_cust_account_id
                                           ,p_partner_site_id            => NULL
                                           ,p_partner_contact_party_id   => NULL
                                           ,p_partner_contact_name       => NULL
                                           ,p_partner_email              => NULL
                                           ,p_partner_phone              => NULL
                                           ,p_partner_fax                => NULL
                                           ,p_header_tolerance_operand   => NULL
                                           ,p_header_tolerance_calc_code => NULL
                                           ,p_line_tolerance_operand     => NULL
                                           ,p_line_tolerance_calc_code   => NULL
                                           ,p_currency_code              => c_batch.currency_code
                                           ,p_claimed_amount             => NULL
                                           ,p_allowed_amount             => NULL
                                           ,p_paid_amount                => NULL
                                           ,p_disputed_amount            => NULL
                                           ,p_accepted_amount            => NULL
                                           ,p_lines_invalid              => NULL
                                           ,p_lines_w_tolerance          => NULL
                                           ,p_lines_disputed             => NULL
                                           ,p_batch_set_id_code          => 'OTHER'
                                           ,p_credit_code                => NULL
                                           ,p_credit_advice_date         => NULL
                                           ,p_purge_flag                 => NULL
                                           ,p_attribute_category         => NULL
                                           ,p_attribute1                 => NULL
                                           ,p_attribute2                 => NULL
                                           ,p_attribute3                 => NULL
                                           ,p_attribute4                 => NULL
                                           ,p_attribute5                 => NULL
                                           ,p_attribute6                 => NULL
                                           ,p_attribute7                 => NULL
                                           ,p_attribute8                 => NULL
                                           ,p_attribute9                 => NULL
                                           ,p_attribute10                => NULL
                                           ,p_attribute11                => NULL
                                           ,p_attribute12                => NULL
                                           ,p_attribute13                => NULL
                                           ,p_attribute14                => NULL
                                           ,p_attribute15                => NULL
                                           ,px_org_id                    => c_batch.org_id
                                           ,p_direct_order_flag          => NULL); --API change with updgrade 12.1.3

          --dbms_output.put_line('Successfully loaded batch id  ' || l_batch);
          l_sec := 'Successfully loaded batch id  ' || l_batch;
          fnd_file.put_line(fnd_file.log, l_sec);
          fnd_file.put_line(fnd_file.output, l_sec);

          --COMMIT;

          UPDATE ozf_resale_lines_int_all
             SET resale_batch_id = l_batch
           WHERE resale_batch_id IS NULL
             AND status_code = 'OPEN'
             AND data_source_code = 'ORCL_DW'
             AND sold_from_cust_account_id =
                 c_batch.sold_from_cust_account_id
             AND org_id = c_batch.org_id
             AND object_version_number = c_batch.object_version_number
             AND currency_code = c_batch.currency_code;

          --COMMIT;
          /*  Version   10/23/2013
                   INSERT INTO xxcus.xxcus_rebt_cust_tbl
                     (rebt_cust_acct_id)
                   VALUES
                     (c_batch.sold_from_cust_account_id);
          */
          --COMMIT;
          --table used for the HDS Funds Accrual to know what rebate customer the FAE needs to run for 10/23/13

          INSERT INTO xxcus.xxcus_rebt_cust_tbl
            (rebt_cust_acct_id
            ,resale_batch_id
            ,org_id
            ,fiscal_per_id
            ,bu_nm
            ,created_date
            ,offer_type) --Version 1.9  10/28/13
          VALUES
            (c_batch.sold_from_cust_account_id
            ,l_batch
            ,c_batch.org_id
            ,l_fiscal_per_id
            ,c_batch.end_cust_party_name
            ,SYSDATE
            ,c_batch.offer_type);
          --10/23/13

        EXCEPTION
          WHEN OTHERS THEN
            l_err_msg := l_sec || ' Unexpected Error Occured in Section :' ||
                         '::ERROR :' || SQLERRM;
            fnd_file.put_line(fnd_file.output, l_err_msg);
            fnd_file.put_line(fnd_file.log, l_err_msg);
            l_success := FALSE;
            ROLLBACK;

        END;
        IF l_success
        THEN

          l_sec := 'Start process of batches; ';

          BEGIN

            ozf_resale_pub.start_process_iface(p_api_version      => 1.0
                                              ,p_init_msg_list    => fnd_api.g_false
                                              ,p_commit           => fnd_api.g_false
                                              ,p_validation_level => fnd_api.g_valid_level_full
                                              ,p_resale_batch_id  => l_batch
                                              ,x_return_status    => x_return_status
                                              ,x_msg_data         => x_msg_data
                                              ,x_msg_count        => x_msg_count);

            l_sec := 'Successfully processed batch id  ' || l_batch ||
                     ' Status: ' || x_return_status;

            IF x_return_status = fnd_api.g_ret_sts_success
            THEN
              COMMIT;

              fnd_file.put_line(fnd_file.output, l_sec);

              ozf_resale_pub.start_payment(p_api_version      => 1.0
                                          ,p_init_msg_list    => fnd_api.g_false
                                          ,p_commit           => fnd_api.g_false
                                          ,p_validation_level => fnd_api.g_valid_level_full
                                          ,p_resale_batch_id  => l_batch
                                          ,x_return_status    => x_return_status
                                          ,x_msg_data         => x_msg_data
                                          ,x_msg_count        => x_msg_count);

              l_sec := 'Successfully closed payment batch id  ' || l_batch ||
                       ' Status: ' || x_return_status;

              IF x_return_status = fnd_api.g_ret_sts_success
              THEN
                fnd_file.put_line(fnd_file.output, l_sec);

                --10/23/13
                UPDATE xxcus.xxcus_rebt_cust_tbl c
                   SET status_code = 'CLOSED'
                 WHERE c.resale_batch_id = l_batch;
                --

              ELSE
                l_sec := 'ERROR closed payment batch id  ' || l_batch ||
                         ' Status: ' || x_return_status || ' Reason = ' ||
                         x_msg_data;
                fnd_file.put_line(fnd_file.output, l_sec);
                fnd_file.put_line(fnd_file.log, l_sec);

                --10/23/13
                UPDATE xxcus.xxcus_rebt_cust_tbl c
                   SET status_code = x_return_status
                 WHERE c.resale_batch_id = l_batch;
                --
              END IF;
            ELSE
              l_sec := 'ERROR processing payment batch id  ' || l_batch ||
                       ' Status: ' || x_return_status || ' Reason = ' ||
                       x_msg_data;
              fnd_file.put_line(fnd_file.output, l_sec);
              fnd_file.put_line(fnd_file.log, l_sec);

              --10/23/13
              UPDATE xxcus.xxcus_rebt_cust_tbl c
                 SET status_code = x_return_status
               WHERE c.resale_batch_id = l_batch;
              --
            END IF;

          EXCEPTION
            WHEN OTHERS THEN
              l_err_msg := 'Error in processing batch ' || l_batch ||
                           ' at ' || l_sec;
              fnd_file.put_line(fnd_file.output, l_err_msg);
              fnd_file.put_line(fnd_file.log, l_err_msg);
              l_success := FALSE;
              ROLLBACK;
          END;

        END IF;
        COMMIT;

      END LOOP;
      fnd_file.put_line(fnd_file.log, '@108');
      fnd_file.put_line(fnd_file.log
                       ,'End time:  ' ||
                        to_char(SYSDATE, 'DD-MON-YYYY HH24:MI:SS') ||
                        ' for ' || l_sec);
      fnd_file.put_line(fnd_file.output
                       ,'End time:  ' ||
                        to_char(SYSDATE, 'DD-MON-YYYY HH24:MI:SS') ||
                        ' for ' || l_sec);

    END;

    --call to put comments in receipts that are not loading, status_flag = 'N'
    l_sec := 'Submit Request for Receipt excepetion - comments.';
    fnd_file.put_line(fnd_file.log
                     ,'Start time:  ' ||
                      to_char(SYSDATE, 'DD-MON-YYYY HH24:MI:SS') ||
                      ' for ' || l_sec);
    fnd_file.put_line(fnd_file.output
                     ,'Start time:  ' ||
                      to_char(SYSDATE, 'DD-MON-YYYY HH24:MI:SS') ||
                      ' for ' || l_sec);

    submit_receipt_exceptions(l_err_msg
                             ,l_err_code
                             ,l_start_date
                             ,l_end_date
                             ,l_low
                             ,l_high
                             ,p_bu_nm
                             ,fnd_global.user_id() --Version1.7
                             ,l_responsibility); --Version1.7

    fnd_file.put_line(fnd_file.log
                     ,'End time:  ' ||
                      to_char(SYSDATE, 'DD-MON-YYYY HH24:MI:SS') ||
                      ' for ' || l_sec);
    fnd_file.put_line(fnd_file.output
                     ,'End time:  ' ||
                      to_char(SYSDATE, 'DD-MON-YYYY HH24:MI:SS') ||
                      ' for ' || l_sec);
    fnd_file.put_line(fnd_file.log, '@109');
    --Version 1.8 updating the batch status to know which batches will have Third Party Accrual run
    --batches must have a "CLOSED" status
    --Wait for Third Party Accrual to run to call FAE
    l_sec := 'Waiting for Third Party Accrual Request To Complete';
    fnd_file.put_line(fnd_file.log, l_sec);
    fnd_file.put_line(fnd_file.output, l_sec);

    fnd_file.put_line(fnd_file.log
                     ,'Start time:  ' ||
                      to_char(SYSDATE, 'DD-MON-YYYY HH24:MI:SS') ||
                      ' for ' || l_sec);
    fnd_file.put_line(fnd_file.output
                     ,'Start time:  ' ||
                      to_char(SYSDATE, 'DD-MON-YYYY HH24:MI:SS') ||
                      ' for ' || l_sec);

    BEGIN

      FOR i IN 1 .. 50
      LOOP

        SELECT COUNT(*)
          INTO l_status
          FROM xxcus.xxcus_rebt_cust_tbl x, ozf_resale_batches_all b --Version 1.4 8/27/13 ignoring rejected batches
         WHERE bu_nm = nvl(p_bu_nm, bu_nm)
           AND x.resale_batch_id = b.resale_batch_id
           AND x.status_code = 'CLOSED'
           AND b.status_code <> 'REJECTED'
              --AND nvl(request_code, 'XX') <> 'C'
           AND nvl(request_code, 'XX') NOT IN ('C', 'E', 'X')
           AND nvl(status_flag, 'X') <> 'P';

        IF l_status <> 0
        THEN
          dbms_lock.sleep(l_sleep);

          FOR c_tpa IN (SELECT request_id
                              ,to_number(argument1) argument1
                              ,phase_code
                          FROM applsys.fnd_concurrent_requests r
                         WHERE r.concurrent_program_id = l_conc_program --Third Party Accrual From Resale Table
                           AND to_number(r.argument1) IN
                               (SELECT resale_batch_id
                                  FROM xxcus.xxcus_rebt_cust_tbl b
                                 WHERE bu_nm = nvl(p_bu_nm, bu_nm)
                                   AND status_code = 'CLOSED'
                                      --AND nvl(request_code, 'XX') <> 'C'
                                   AND nvl(request_code, 'XX') NOT IN
                                       ('C', 'E', 'X')
                                   AND nvl(status_flag, 'X') <> 'P'
                                   AND resale_batch_id =
                                       to_number(r.argument1)))
          LOOP

            IF c_tpa.phase_code <> 'C'
            THEN
              IF (c_tpa.request_id != 0)
              THEN
                IF fnd_concurrent.wait_for_request(c_tpa.request_id
                                                  ,v_interval
                                                  ,v_max_time
                                                  ,v_phase
                                                  ,v_status
                                                  ,v_dev_phase
                                                  ,v_dev_status
                                                  ,v_message)
                THEN
                  l_sec := 'ReqID=' || l_req_id || ' DPhase ' ||
                           v_dev_phase || ' DStatus ' || v_dev_status ||
                           chr(10) || ' MSG - ' || v_message;
                  IF v_dev_phase = 'COMPLETE' AND v_dev_status = 'NORMAL'
                  THEN
                    UPDATE xxcus.xxcus_rebt_cust_tbl
                       SET request_code = c_tpa.phase_code
                          ,request_id   = c_tpa.request_id
                     WHERE resale_batch_id = c_tpa.argument1
                       AND bu_nm = nvl(p_bu_nm, bu_nm);
                    COMMIT;
                  ELSE

                    fnd_file.put_line(fnd_file.log, l_sec);
                    fnd_file.put_line(fnd_file.output, l_sec);

                    UPDATE xxcus.xxcus_rebt_cust_tbl
                       SET request_code = c_tpa.phase_code
                          ,request_id   = c_tpa.request_id
                     WHERE resale_batch_id = c_tpa.argument1
                       AND bu_nm = nvl(p_bu_nm, bu_nm);
                    COMMIT;
                  END IF;
                END IF;
              END IF;

            ELSE
              UPDATE xxcus.xxcus_rebt_cust_tbl
                 SET request_code = c_tpa.phase_code
                    ,request_id   = c_tpa.request_id
               WHERE resale_batch_id = c_tpa.argument1
                 AND bu_nm = nvl(p_bu_nm, bu_nm);
              COMMIT;
            END IF;

          END LOOP;
        END IF;
      END LOOP;
      fnd_file.put_line(fnd_file.log
                       ,'End time:  ' ||
                        to_char(SYSDATE, 'DD-MON-YYYY HH24:MI:SS') ||
                        ' for ' || l_sec);
      fnd_file.put_line(fnd_file.output
                       ,'End time:  ' ||
                        to_char(SYSDATE, 'DD-MON-YYYY HH24:MI:SS') ||
                        ' for ' || l_sec);
    END;

    l_sec := 'Finished';
    fnd_file.put_line(fnd_file.log, l_sec);
    fnd_file.put_line(fnd_file.output, l_sec);

    UPDATE xxcus.xxcus_rebate_aud_tbl
       SET stage = l_sec, stg_date = SYSDATE --, processed_datetime = sysdate
     WHERE (conc_request_id = nvl(fnd_global.conc_request_id, 0) OR
           trunc(processed_datetime) = trunc(SYSDATE)) --Version 1.9
       AND procedure_name = l_procedure
       AND fiscal_period =
           p_fperiod || '~' || p_rebt_vndr || '~' || p_bu_nm
       AND post_run_id = l_fiscal_per_id
       AND nvl(bu_nm, 'x') = nvl(p_bu_nm, 'x') --Version 1.9 added nvl
       AND user_name = nvl(fnd_global.user_name, 'NA') --Version 1.9
       AND resp_name = nvl(fnd_global.resp_name, 'NA'); --Version 1.9
    COMMIT;

    --Version 1.8 Run FAE after TPA has finished processing 10/23/2013
    --Version 1.10 Remove the call to FAE  02/03/2014
    /*
      BEGIN

        l_count := NULL;

        -- Call Funds Accrual Engine - xxcus_tm_misc_pkg.submit_fae
        --check to see if receipt load has finished so FAE will run with distinct cust acct

        SELECT COUNT(*)
          INTO l_count
          FROM xxcus.xxcus_rebate_aud_tbl a
         WHERE nvl(a.bu_nm, 'x') = nvl(p_bu_nm, 'x')
           AND a.fiscal_period =
               p_fperiod || '~' || p_rebt_vndr || '~' || p_bu_nm
           AND a.stage = 'Finished'
           AND a.procedure_name = l_procedure;

        IF l_count > 0
        THEN

          l_sec := 'Submitted Funds Accrual request-HDS Funds Accrual Engine All Vendors.';
          fnd_file.put_line(fnd_file.log, l_sec);
          fnd_file.put_line(fnd_file.output, l_sec);

          l_req_id := fnd_request.submit_request('OZF'
                                                ,'HDS_SUBMIT_FAE_VENDORS'
                                                ,NULL
                                                ,NULL
                                                ,FALSE
                                                ,p_bu_nm);

          COMMIT;
        END IF;
      END;
    */
  EXCEPTION
    WHEN program_error THEN
      ROLLBACK;
      l_err_code := 2;
      l_err_msg  := l_sec;
      l_err_msg  := l_err_msg || ' Error_Stack...' ||
                    dbms_utility.format_error_stack() ||
                    ' Error_Backtrace...' ||
                    dbms_utility.format_error_backtrace();
      fnd_file.put_line(fnd_file.log, l_err_msg);
      fnd_file.put_line(fnd_file.output, l_err_msg);
      retcode         := l_err_code;
      errbuf          := l_err_msg;
      l_err_callpoint := l_sec;

      xxcus_error_pkg.xxcus_error_main_api(p_called_from       => l_err_callfrom
                                          ,p_calling           => l_err_callpoint
                                          ,p_request_id        => l_req_id
                                          ,p_ora_error_msg     => substr(regexp_replace(l_err_msg
                                                                                       ,'[[:cntrl:]]'
                                                                                       ,NULL)
                                                                        ,1
                                                                        ,2000)
                                          ,p_error_desc        => substr(l_err_msg
                                                                        ,1
                                                                        ,240)
                                          ,p_distribution_list => l_distro_list
                                          ,p_module            => 'TM');

    WHEN OTHERS THEN
      fnd_file.put_line(fnd_file.log, l_sec);
      fnd_file.put_line(fnd_file.output, l_sec);
      l_err_code := 2;
      l_err_msg  := l_err_msg || ' Error_Stack...' ||
                    dbms_utility.format_error_stack() ||
                    ' Error_Backtrace...' ||
                    dbms_utility.format_error_backtrace();
      fnd_file.put_line(fnd_file.log, l_err_msg);
      fnd_file.put_line(fnd_file.output, l_err_msg);
      retcode         := l_err_code;
      errbuf          := l_err_msg;
      l_err_callpoint := l_sec;

      xxcus_error_pkg.xxcus_error_main_api(p_called_from       => l_err_callfrom
                                          ,p_calling           => l_err_callpoint
                                          ,p_request_id        => l_req_id
                                          ,p_ora_error_msg     => substr(regexp_replace(l_err_msg
                                                                                       ,'[[:cntrl:]]'
                                                                                       ,NULL)
                                                                        ,1
                                                                        ,2000)
                                          ,p_error_desc        => substr(l_err_msg
                                                                        ,1
                                                                        ,240)
                                          ,p_distribution_list => l_distro_list
                                          ,p_module            => 'TM');

  END oneoff_rebate_receipts;

  /*******************************************************************************
  * Procedure:   CREATE_VALUESET
  * Description: This is called from LOAD_REBATE_VNDR and creates the indepenent valueset
  *              for rebate vender.
  *
  HISTORY
  ===============================================================================
  VERSION DATE          AUTHOR(S)       DESCRIPTION
  ------- -----------   --------------- -----------------------------------------
  1.0     09/13/2010    Kathy Poling    Initial creation of the procedure

  ********************************************************************************/

  PROCEDURE create_valueset IS

    --
    -- Package Variables
    --
    l_req_id   NUMBER NULL;
    l_err_msg  VARCHAR2(3000);
    l_err_code NUMBER;
    l_sec      VARCHAR2(255);
    l_valueset CONSTANT fnd_flex_value_sets.flex_value_set_name%TYPE := 'HDS_SUPPLIER_CODE';
    x_storage_value VARCHAR2(2000);

    -- Error DEBUG
    l_err_callfrom  VARCHAR2(75) DEFAULT 'XXCUS_TM_INTERFACE_PKG.CREATE_VALUESET';
    l_err_callpoint VARCHAR2(75) DEFAULT 'START';
    l_distro_list   VARCHAR2(75) DEFAULT 'HDSOracleDevelopers@hdsupply.com';

  BEGIN

    l_sec := 'Creating indepenent valueset for Rebate Vendor.';
    fnd_file.put_line(fnd_file.output
                     ,'Start time:  ' ||
                      to_char(SYSDATE, 'DD-MON-YYYY HH24:MI:SS') ||
                      ' for ' || l_sec);

    FOR c_rebt IN (SELECT REPLACE(party_number, '~MSTR', '') party_number
                         ,party_name
                     FROM hz_parties
                    WHERE attribute1 = 'HDS_MVID'
                      AND REPLACE(party_number, '~MSTR', '') NOT IN
                          (SELECT fv2.flex_value
                             FROM applsys.fnd_flex_values_tl  fvt2
                                 ,applsys.fnd_flex_values     fv2
                                 ,applsys.fnd_flex_value_sets fvs2
                            WHERE fvt2.flex_value_id = fv2.flex_value_id
                              AND fv2.flex_value_set_id =
                                  fvs2.flex_value_set_id
                              AND fvs2.flex_value_set_name = l_valueset
                              AND fvt2.language = 'US'
                              AND enabled_flag = 'Y'
                              AND fv2.flex_value <> 'NEW'
                              AND fv2.flex_value =
                                  REPLACE(party_number, '~MSTR', '')))
    LOOP

      fnd_flex_val_api.create_independent_vset_value(l_valueset
                                                    ,c_rebt.party_number
                                                    ,c_rebt.party_name
                                                    ,'Y'
                                                    ,NULL
                                                    ,NULL
                                                    ,'N'
                                                    ,NULL
                                                    ,NULL
                                                    ,x_storage_value);

    END LOOP;

    l_sec := 'Finished creating indepenent valueset for Rebate Vendor.';
    fnd_file.put_line(fnd_file.output
                     ,'End time:  ' ||
                      to_char(SYSDATE, 'DD-MON-YYYY HH24:MI:SS') ||
                      ' for ' || l_sec);

  EXCEPTION
    WHEN program_error THEN
      ROLLBACK;
      l_err_code := 2;
      l_err_msg  := l_err_msg || ' Error_Stack...' ||
                    dbms_utility.format_error_stack() ||
                    ' Error_Backtrace...' ||
                    dbms_utility.format_error_backtrace();
      fnd_file.put_line(fnd_file.log, l_err_msg);
      fnd_file.put_line(fnd_file.output, l_err_msg);
      l_err_callpoint := l_sec;

      xxcus_error_pkg.xxcus_error_main_api(p_called_from       => l_err_callfrom
                                          ,p_calling           => l_err_callpoint
                                          ,p_request_id        => l_req_id
                                          ,p_ora_error_msg     => substr(regexp_replace(l_err_msg
                                                                                       ,'[[:cntrl:]]'
                                                                                       ,NULL)
                                                                        ,1
                                                                        ,2000)
                                          ,p_error_desc        => substr(l_err_msg
                                                                        ,1
                                                                        ,240)
                                          ,p_distribution_list => l_distro_list
                                          ,p_module            => 'TM');

    WHEN OTHERS THEN
      fnd_file.put_line(fnd_file.log, l_sec);
      fnd_file.put_line(fnd_file.output, l_sec);
      l_err_code := 2;
      l_err_msg  := l_err_msg || ' Error_Stack...' ||
                    dbms_utility.format_error_stack() ||
                    ' Error_Backtrace...' ||
                    dbms_utility.format_error_backtrace();
      fnd_file.put_line(fnd_file.log, l_err_msg);
      fnd_file.put_line(fnd_file.output, l_err_msg);
      l_err_callpoint := l_sec;

      xxcus_error_pkg.xxcus_error_main_api(p_called_from       => l_err_callfrom
                                          ,p_calling           => l_err_callpoint
                                          ,p_request_id        => l_req_id
                                          ,p_ora_error_msg     => substr(regexp_replace(l_err_msg
                                                                                       ,'[[:cntrl:]]'
                                                                                       ,NULL)
                                                                        ,1
                                                                        ,2000)
                                          ,p_error_desc        => substr(l_err_msg
                                                                        ,1
                                                                        ,240)
                                          ,p_distribution_list => l_distro_list
                                          ,p_module            => 'TM');

  END create_valueset;

  /*******************************************************************************
  * Procedure:   UOM_EXCEPTIONS
  * Description: This is called from LOAD_REBATE_PRODUCTS.  If any product isn't loaded
  *              because the uom xref isn't setup in APEX will mail the team the
  *              notification
  HISTORY
  ===============================================================================
  VERSION DATE          AUTHOR(S)       DESCRIPTION
  ------- -----------   --------------- -----------------------------------------

  1.0     09/19/2010    Kathy Poling    Initial creation of the procedure
  1.1     03/08/2013    Kathy Poling    Changed to not use table in apex.  It's
                                        being staged and update in oracle
                                        RFC 36582
  1.2     08/22/2013    Kathy Poling    Added parm bu_nm
                                        SR 202000
  1.3     11/27/2013    Kathy Poling    Added parallel hint for performance
  ********************************************************************************/

  PROCEDURE uom_exceptions(p_bu_nm IN VARCHAR2) IS
    --
    -- Package Variables
    --
    l_dflt_email VARCHAR2(200) := 'HDSRebateManagementSystemNotifications@hdsupply.com'; --'hdsoracleapsupport@hdsupply.com';
    l_req_id     NUMBER NULL;

    l_err_msg    VARCHAR2(3000);
    l_err_code   NUMBER;
    l_sec        VARCHAR2(255);
    l_count_item NUMBER := 0;
    -- Error DEBUG
    l_err_callfrom  VARCHAR2(75) DEFAULT 'XXCUS_TM_INTERFACE_PKG.UOM_EXCEPTIONS';
    l_err_callpoint VARCHAR2(75) DEFAULT 'START';
    l_distro_list   VARCHAR2(75) DEFAULT 'HDSOracleDevelopers@hdsupply.com';

    -- Mailer
    l_sender      VARCHAR2(100);
    l_sid         VARCHAR2(8);
    l_body        VARCHAR2(32767);
    l_body_header VARCHAR2(32767);
    l_body_detail VARCHAR2(32767);
    l_body_footer VARCHAR2(32767);
    l_host        VARCHAR2(256) := 'mailoutrelay.hdsupply.net';
    l_hostport    VARCHAR2(20) := '25';

    CURSOR c_uom IS
      SELECT /*+ PARALLEL (AUTO) */ --Version 1.3
      DISTINCT p.bu_nm, upper(p.src_sys_nm) src_sys_nm, p.prod_purch_uom
        FROM xxcus.xxcus_rebate_product_curr_tbl p
       WHERE bu_nm = nvl(p_bu_nm, bu_nm) --Version 1.2
         AND (p.bu_nm, upper(p.src_sys_nm), p.prod_purch_uom) NOT IN
             (SELECT xref.bu_nm, upper(xref.src_sys_nm), xref.purch_uom_cd
              --FROM hdsoracle.hds_rebate_uom_xref_tbl@apxprd_lnk xref    Version 1.1
                FROM xxcus.xxcus_rebate_uom_tbl xref --Version 1.1
               WHERE xref.bu_nm = p.bu_nm
                 AND upper(xref.src_sys_nm) = upper(p.src_sys_nm)
                 AND xref.purch_uom_cd = p.prod_purch_uom)
       ORDER BY 1, 2;

  BEGIN

    --DBMS_OUTPUT.ENABLE (1000000);

    SELECT lower(NAME) INTO l_sid FROM v$database;
    l_sender := 'Oracle.Applications_' || l_sid || '@hdsupply.com';

    --dbms_output.put_line('sid = ' ||l_sid);

    SELECT COUNT(*)
      INTO l_count_item
      FROM xxcus.xxcus_rebate_product_curr_tbl p
     WHERE bu_nm = nvl(p_bu_nm, bu_nm) --Version 1.2
       AND (p.bu_nm, upper(p.src_sys_nm), p.prod_purch_uom) NOT IN
           (SELECT xref.bu_nm, upper(xref.src_sys_nm), xref.purch_uom_cd
            --FROM hdsoracle.hds_rebate_uom_xref_tbl@apxprd_lnk xref    Version 1.1
              FROM xxcus.xxcus_rebate_uom_tbl xref --Version 1.1
             WHERE xref.bu_nm = p.bu_nm
               AND upper(xref.src_sys_nm) = upper(p.src_sys_nm)
               AND xref.purch_uom_cd = p.prod_purch_uom);

    --dbms_output.put_line('count = ' ||l_count_item);

    IF l_count_item > 0
    THEN

      l_body_header := '<style type="text/css">
.style1 {
    border-style: solid;
    border-width: 1px;
}
.style2 {
    border: 1px solid #000000;
}
.style3 {
    border: 1px solid #000000;
--    background-color: #FFFF00;
}
.style4 {
    color: #FFFF00;
    border: 1px solid #000000;
    background-color: #000000;
}
.style5 {
    font-size: large;
}
.style6 {
    font-size: xx-large;
}
.style7 {
    color: #FFCC00;
}
</style>
<p><span class="style6"><span class="style7"><strong>HD</strong></span><strong>SUPPLY</strong></span><br />
<span class="style5">REBATES - Not all products loaded</span></p>
     <BR><table style="width: 100%">    <tr>' ||
                       '<td class="style4"><B>BU NAME</B></td><td class="style4"><B>SOURCE SYSTEM</B></td><td class="style4"><B>UOM</B></td><tr>';

      FOR c_email IN c_uom
      LOOP
        --Get list of UOM info for the Email.

        l_body_detail := l_body_detail || '<td class="style3">' ||
                         c_email.bu_nm || '</td><td class="style3">' ||
                         c_email.src_sys_nm || '</td><td class="style3">' ||
                         c_email.prod_purch_uom || '</td><TR>';

        dbms_output.put_line(c_email.bu_nm || ' : ' || c_email.src_sys_nm ||
                             ' : ' || c_email.prod_purch_uom);
      END LOOP;

      l_body_footer := l_body_footer ||
                       '<BR> Number of Products not loaded into Oracle: ' ||
                       l_count_item;
      l_body_footer := l_body_footer ||
                       '<BR>The Unit of Measures are not setup in the cross reference. They need to be setup for all items to load. When xref has been setup run HDS TM Product Load Items';
      l_body        := l_body_header || l_body_detail || l_body_footer;

      -- Send email .
      -- DEBUG NAME
      l_err_callpoint := 'Sending email';
      -- DEBUG NAME

      --email sent to support team to correct uom xref
      xxcus_misc_pkg.html_email(p_to      => l_dflt_email
                               ,p_from    => l_sender
                               ,p_text    => 'test'
                               ,p_subject => '********ALERT****UOM CROSS REFERENCE NOT SETUP****ALERT*********'
                               ,
                                --p_subject       => l_subject -- v1.1,
                                p_html          => l_body
                               ,p_smtp_hostname => l_host
                               ,p_smtp_portnum  => l_hostport);

    END IF;

  EXCEPTION
    WHEN program_error THEN
      ROLLBACK;
      l_err_code := 2;
      l_err_msg  := l_err_msg || ' Error_Stack...' ||
                    dbms_utility.format_error_stack() ||
                    ' Error_Backtrace...' ||
                    dbms_utility.format_error_backtrace();
      fnd_file.put_line(fnd_file.log, l_err_msg);
      fnd_file.put_line(fnd_file.output, l_err_msg);
      l_err_callpoint := l_sec;

      xxcus_error_pkg.xxcus_error_main_api(p_called_from       => l_err_callfrom
                                          ,p_calling           => l_err_callpoint
                                          ,p_request_id        => l_req_id
                                          ,p_ora_error_msg     => substr(regexp_replace(l_err_msg
                                                                                       ,'[[:cntrl:]]'
                                                                                       ,NULL)
                                                                        ,1
                                                                        ,2000)
                                          ,p_error_desc        => substr(l_err_msg
                                                                        ,1
                                                                        ,240)
                                          ,p_distribution_list => l_distro_list
                                          ,p_module            => 'TM');

    WHEN OTHERS THEN
      fnd_file.put_line(fnd_file.log, l_sec);
      fnd_file.put_line(fnd_file.output, l_sec);
      l_err_code := 2;
      l_err_msg  := l_err_msg || ' Error_Stack...' ||
                    dbms_utility.format_error_stack() ||
                    ' Error_Backtrace...' ||
                    dbms_utility.format_error_backtrace();
      fnd_file.put_line(fnd_file.log, l_err_msg);
      fnd_file.put_line(fnd_file.output, l_err_msg);
      l_err_callpoint := l_sec;

      xxcus_error_pkg.xxcus_error_main_api(p_called_from       => l_err_callfrom
                                          ,p_calling           => l_err_callpoint
                                          ,p_request_id        => l_req_id
                                          ,p_ora_error_msg     => substr(regexp_replace(l_err_msg
                                                                                       ,'[[:cntrl:]]'
                                                                                       ,NULL)
                                                                        ,1
                                                                        ,2000)
                                          ,p_error_desc        => substr(l_err_msg
                                                                        ,1
                                                                        ,240)
                                          ,p_distribution_list => l_distro_list
                                          ,p_module            => 'TM');

  END uom_exceptions;

  /*******************************************************************************
   * Procedure:   RECEIPT_EXCEPTIONS
   * Description: This is called from LOAD_REBATE_RECEIPTS.  If any receipt isn't loaded
   *              looking for a reason to allow reporting
   *
   HISTORY
   ===============================================================================
   VERSION DATE          AUTHOR(S)       DESCRIPTION
   ------- -----------   --------------- -----------------------------------------
   1.0     09/04/2012    Kathy Poling    Initial creation of the procedure
   1.1     09/20/2013    Kathy Poling    Change for item description
                                         RFC 38254
   1.2     10/17/2013    Kathy Poling    Change to query
   1.3     11/77/2013    Kathy Poling    Added parallel hint for performance
                                         RFC 38762
  1.4     09/02/2014    Bala Seshadri    RFC 41456
                                        Add new filter inventory_item_status_code ='Active'
                                        to the cursor c1 and commented enabled_flag
   2.6    01/15/2015    Kathy Poling     ESMS 240310 removed check for trade profile
   ****************************************************************************************/

  PROCEDURE receipt_exceptions(errbuf       OUT NOCOPY VARCHAR2
                              ,retcode      OUT NOCOPY NUMBER
                              ,p_start_date IN DATE
                              ,p_end_date   IN DATE
                              ,p_low        IN NUMBER
                              ,p_high       IN NUMBER
                              ,p_bu_nm      IN VARCHAR2) IS
    --
    -- Package Variables
    --
    l_req_id NUMBER := fnd_global.conc_request_id;
    l_us_org       CONSTANT hr_all_organization_units.organization_id%TYPE := 101; --HDS Rebates USA - Org
    l_inv_org      CONSTANT hr_all_organization_units.organization_id%TYPE := 84; --HDS Rebates USA - Inv
    l_category_set CONSTANT mtl_category_sets_tl.category_set_id%TYPE := 1100000041; --HDS Supplier Category
    l_lob_attr VARCHAR2(30) := 'HDS_LOB';
    l_bu_attr  VARCHAR2(30) := 'HDS_BU';

    -- Error DEBUG
    l_err_msg  VARCHAR2(3000);
    l_err_code NUMBER;
    l_sec      VARCHAR2(255);

    --l_package       VARCHAR2(50) := 'XXCUS_TM_INTERFACE_PKG.RECEIPT_EXCEPTIONS';
    l_err_callfrom  VARCHAR2(75) DEFAULT 'XXCUS_TM_INTERFACE_PKG.RECEIPT_EXCEPTIONS';
    l_err_callpoint VARCHAR2(75) DEFAULT 'START';
    l_distro_list   VARCHAR2(75) DEFAULT 'HDSOracleDevelopers@hdsupply.com';

  BEGIN
    fnd_file.put_line(fnd_file.output
                     ,'*---------------------------------------------------------------------------------------------*');

    fnd_file.put_line(fnd_file.output
                     ,rpad('Receipt start date', 40, ' ') || ': ' ||
                      p_start_date);
    fnd_file.put_line(fnd_file.output
                     ,rpad('Receipt end date', 40, ' ') || ': ' ||
                      p_end_date);
    fnd_file.put_line(fnd_file.output
                     ,rpad('Low amount', 40, ' ') || ': ' || p_low);
    fnd_file.put_line(fnd_file.output
                     ,rpad('High amount', 40, ' ') || ': ' || p_high);
    fnd_file.put_line(fnd_file.output
                     ,rpad('BU Name', 40, ' ') || ': ' || p_bu_nm);

    fnd_file.put_line(fnd_file.log
                     ,'*---------------------------------------------------------------------------------------------*');

    fnd_file.put_line(fnd_file.log
                     ,rpad('Receipt start date', 40, ' ') || ': ' ||
                      p_start_date);
    fnd_file.put_line(fnd_file.log
                     ,rpad('Receipt end date', 40, ' ') || ': ' ||
                      p_end_date);
    fnd_file.put_line(fnd_file.log
                     ,rpad('Low amount', 40, ' ') || ': ' || p_low);
    fnd_file.put_line(fnd_file.log
                     ,rpad('High amount', 40, ' ') || ': ' || p_high);
    fnd_file.put_line(fnd_file.log
                     ,rpad('BU Name', 40, ' ') || ': ' || p_bu_nm);

    l_sec := 'Getting receipts to update comments';

    BEGIN

      FOR c1 IN (SELECT /*+ PARALLEL (AUTO) */ --Version 1.3
                  r.rowid
                 ,r.vndr_recpt_unq_nbr
                 ,r.pt_vndr_cd
                 ,r.shp_to_brnch_cd
                 ,r.geo_loc_hier_lob_nm
                 ,r.bu_nm
                 ,r.bu_cd
                 ,r.src_sys_nm
                 ,r.drop_shp_flg
                 ,r.po_nbr
                 ,r.recpt_crt_dt
                 ,r.vndr_part_nbr
                 ,(CASE
                    WHEN r.item_cost_amt < 0 THEN
                     r.item_cost_amt * -1
                    ELSE
                     r.item_cost_amt
                  END) item_cost_amt
                 ,(CASE
                    WHEN r.item_cost_amt < 0 THEN
                     r.recpt_qty * -1
                    ELSE
                     r.recpt_qty
                  END) / (CASE
                    WHEN buy_pkg_unt_cnt IS NULL THEN
                     1
                    WHEN buy_pkg_unt_cnt = 0 THEN
                     1
                    ELSE
                     buy_pkg_unt_cnt
                  END) recpt_qty
                 ,(CASE
                    WHEN buy_pkg_unt_cnt IS NULL THEN
                     1
                    WHEN buy_pkg_unt_cnt = 0 THEN
                     1
                    ELSE
                     buy_pkg_unt_cnt
                  END) buy_pkg_unt_cnt
                 ,(CASE
                    WHEN item_cost_amt < 0 THEN
                     item_cost_amt * -1
                    ELSE
                     item_cost_amt
                  END) * ((CASE
                    WHEN item_cost_amt < 0 THEN
                     recpt_qty * -1
                    ELSE
                     recpt_qty
                  END) / (CASE
                    WHEN buy_pkg_unt_cnt IS NULL THEN
                     1
                    WHEN buy_pkg_unt_cnt = 0 THEN
                     1
                    ELSE
                     buy_pkg_unt_cnt
                  END)) total_cost
                 ,decode(r.curnc_flg, 'CND', 'CAD', 'USD') curnc_flg
                 ,to_char(r.receipt) receipt
                 ,r.prod_id
                 ,r.sku_cd
                 ,r.cust_account_id
                 ,r.party_id
                 ,r.party_name
                 ,r.comments
                 /*,(SELECT trade_profile_id
                     FROM ozf_cust_trd_prfls_all octp
                    WHERE octp.cust_account_id = r.cust_account_id
                      AND decode(r.curnc_flg, 'CND', 'CAD', 'USD') =
                          octp.claim_currency) trade_profile  */    --Version 2.6
                 ,(SELECT inventory_item_id
                     FROM inv.mtl_system_items_b i
                    WHERE i.organization_id = l_inv_org
                         --AND i.enabled_flag = 'Y' --Commented out 09/02/2014
                      AND i.inventory_item_status_code = 'Active' --New 09/02/2014
                      AND i.description =
                          r.sku_cd || '~' || r.bu_nm || '~' || r.src_sys_nm --Version 1.1 9/20/13  added src_sys_nm
                   ) inventory_item_id
                 ,(SELECT category_id
                     FROM apps.mtl_item_categories_v c
                         ,inv.mtl_system_items_b     i
                    WHERE c.category_set_id = l_category_set --1100000041
                      AND c.organization_id = l_inv_org
                      AND c.organization_id = i.organization_id
                      AND c.inventory_item_id = i.inventory_item_id
                      AND c.segment1 = r.rebt_vndr_cd
                         --AND i.enabled_flag = 'Y' --Commented out 09/02/2014
                      AND i.inventory_item_status_code = 'Active' --New 09/02/2014
                      AND i.description =
                          r.sku_cd || '~' || r.bu_nm || '~' || r.src_sys_nm --Version 1.1 9/20/13  added src_sys_nm
                      AND rownum = 1) category_id
                 ,(SELECT cp.cust_acct_id
                     FROM apps.xxcus_cust_prod_rebt_v cp
                    WHERE cp.cust_acct_id = r.cust_account_id) cust_acct_id
                 ,(SELECT bp.party_name
                     FROM ar.hz_parties bp
                    WHERE bp.attribute_category = l_us_org
                      AND bp.attribute1 = l_lob_attr
                      AND bp.known_as = r.geo_loc_hier_lob_nm) bill_to_party_name
                 ,(SELECT ps.party_site_id
                     FROM ar.hz_party_sites ps
                    WHERE ps.party_site_number =
                          r.pt_vndr_cd || '~' || r.bu_cd) party_site_id
                 ,(SELECT hp1.party_id
                     FROM ar.hz_parties       bp
                         ,ar.hz_parties       hp1
                         ,ar.hz_relationships rel
                    WHERE bp.known_as = r.geo_loc_hier_lob_nm
                      AND bp.attribute1 = l_lob_attr
                      AND bp.party_id = rel.object_id
                      AND rel.relationship_code = 'DIVISION_OF'
                      AND rel.relationship_type = 'HEADQUARTERS/DIVISION'
                      AND rel.subject_id = hp1.party_id
                      AND hp1.attribute1 = l_bu_attr
                      AND rel.status = 'A'
                      AND rel.directional_flag IN ('B', 'F')
                      AND hp1.party_name = r.bu_nm) bu_id
                 ,(SELECT xref.oracle_uom_code
                     FROM xxcus.xxcus_rebate_uom_tbl xref
                    WHERE xref.purch_uom_cd = nvl(r.prod_uom_cd, 'UNKNOWN')
                      AND xref.bu_nm = r.bu_nm
                      AND upper(xref.src_sys_nm) = upper(r.src_sys_nm)) oracle_uom_code
                   FROM xxcus.xxcus_rebate_receipt_curr_tbl r
                 --,apps.xxcus_rebate_branch_vw         xrbv            --Version 1.2 10/17/13
                  WHERE r.bu_nm = nvl(p_bu_nm, r.bu_nm)
                    AND r.status_flag = 'N'
                    AND r.recpt_crt_dt BETWEEN p_start_date AND p_end_date
                 --AND r.shp_to_brnch_cd = xrbv.oper_branch(+)             --Version 1.2 10/17/13
                 --AND r.bu_nm = xrbv.dw_bu_desc(+)                        --Version 1.2 10/17/13
                 )
      LOOP

        IF c1.total_cost = 0
        THEN
          UPDATE xxcus.xxcus_rebate_receipt_curr_tbl
             SET comments         = 'Receipt Total is 0'
                ,last_update_date = SYSDATE
           WHERE ROWID = c1.rowid;

        ELSIF c1.total_cost BETWEEN p_low AND p_high
        THEN
          UPDATE xxcus.xxcus_rebate_receipt_curr_tbl
             SET comments         = 'Receipt Total between ' || p_low ||
                                    ' and ' || p_high
                ,last_update_date = SYSDATE
           WHERE ROWID = c1.rowid;

    /*   ELSIF c1.trade_profile IS NULL
        THEN
          UPDATE xxcus.xxcus_rebate_receipt_curr_tbl
             SET comments         = 'Trade profile missing for vendor'
                ,last_update_date = SYSDATE
           WHERE ROWID = c1.rowid;
     */ --Version 2.6
        ELSIF c1.inventory_item_id IS NULL
        THEN
          UPDATE xxcus.xxcus_rebate_receipt_curr_tbl
             SET comments = 'Product not setup', last_update_date = SYSDATE
           WHERE ROWID = c1.rowid;

        ELSIF c1.cust_acct_id IS NOT NULL AND
              nvl(c1.category_id, 1123) = 1123
        THEN
          UPDATE xxcus.xxcus_rebate_receipt_curr_tbl
             SET comments         = 'Category not setup'
                ,last_update_date = SYSDATE
           WHERE ROWID = c1.rowid;

        ELSIF c1.bill_to_party_name IS NULL
        THEN
          UPDATE xxcus.xxcus_rebate_receipt_curr_tbl
             SET comments         = 'LOB not setup as customer'
                ,last_update_date = SYSDATE
           WHERE ROWID = c1.rowid;

        ELSIF c1.party_site_id IS NULL
        THEN
          UPDATE xxcus.xxcus_rebate_receipt_curr_tbl
             SET comments         = 'Payto vendor not setup in party sites'
                ,last_update_date = SYSDATE
           WHERE ROWID = c1.rowid;

        ELSIF c1.bu_id IS NULL
        THEN
          UPDATE xxcus.xxcus_rebate_receipt_curr_tbl
             SET comments         = 'Business Unit not setup as a Party'
                ,last_update_date = SYSDATE
           WHERE ROWID = c1.rowid;

        ELSIF c1.oracle_uom_code IS NULL
        THEN
          UPDATE xxcus.xxcus_rebate_receipt_curr_tbl
             SET comments         = 'UOM mapping not setup'
                ,last_update_date = SYSDATE
           WHERE ROWID = c1.rowid;

        ELSE
          UPDATE xxcus.xxcus_rebate_receipt_curr_tbl
             SET comments = NULL, last_update_date = SYSDATE
           WHERE ROWID = c1.rowid;
        END IF;

      END LOOP;

      COMMIT;
      l_sec := 'Finished updating comments for BU:  ' || p_bu_nm ||
               ' with receipt date range between ' || p_start_date ||
               ' and ' || p_end_date;

    END;

    fnd_file.put_line(fnd_file.log
                     ,'*---------------------------------------------------------------------------------------------*');
    fnd_file.put_line(fnd_file.log
                     ,'End time:  ' ||
                      to_char(SYSDATE, 'DD-MON-YYYY HH24:MI:SS'));

    fnd_file.put_line(fnd_file.output
                     ,'*---------------------------------------------------------------------------------------------*');
    fnd_file.put_line(fnd_file.output
                     ,'End time:  ' ||
                      to_char(SYSDATE, 'DD-MON-YYYY HH24:MI:SS'));

  EXCEPTION
    WHEN program_error THEN
      --ROLLBACK;
      l_err_code := 2;
      l_err_msg  := l_err_msg || ' Error_Stack...' ||
                    dbms_utility.format_error_stack() ||
                    ' Error_Backtrace...' ||
                    dbms_utility.format_error_backtrace();
      fnd_file.put_line(fnd_file.log, l_err_msg);
      fnd_file.put_line(fnd_file.output, l_err_msg);
      l_err_callpoint := l_sec;

      xxcus_error_pkg.xxcus_error_main_api(p_called_from       => l_err_callfrom
                                          ,p_calling           => l_err_callpoint
                                          ,p_request_id        => l_req_id
                                          ,p_ora_error_msg     => substr(regexp_replace(l_err_msg
                                                                                       ,'[[:cntrl:]]'
                                                                                       ,NULL)
                                                                        ,1
                                                                        ,2000)
                                          ,p_error_desc        => substr(l_err_msg
                                                                        ,1
                                                                        ,240)
                                          ,p_distribution_list => l_distro_list
                                          ,p_module            => 'TM');

    WHEN OTHERS THEN
      fnd_file.put_line(fnd_file.log, l_sec);
      fnd_file.put_line(fnd_file.output, l_sec);
      l_err_code := 2;
      l_err_msg  := l_err_msg || ' Error_Stack...' ||
                    dbms_utility.format_error_stack() ||
                    ' Error_Backtrace...' ||
                    dbms_utility.format_error_backtrace();
      fnd_file.put_line(fnd_file.log, l_err_msg);
      fnd_file.put_line(fnd_file.output, l_err_msg);
      l_err_callpoint := l_sec;

      xxcus_error_pkg.xxcus_error_main_api(p_called_from       => l_err_callfrom
                                          ,p_calling           => l_err_callpoint
                                          ,p_request_id        => l_req_id
                                          ,p_ora_error_msg     => substr(regexp_replace(l_err_msg
                                                                                       ,'[[:cntrl:]]'
                                                                                       ,NULL)
                                                                        ,1
                                                                        ,2000)
                                          ,p_error_desc        => substr(l_err_msg
                                                                        ,1
                                                                        ,240)
                                          ,p_distribution_list => l_distro_list
                                          ,p_module            => 'TM');

  END receipt_exceptions;

  /*******************************************************************************
  * Procedure:   PRODUCT_EXCEPTIONS
  * Description: This is called from LOAD_REBATE_RECEIPTS.  If any receipt isn't loaded
  *              because the classification isn't setup will mail the team the
  *              notification
  HISTORY
  ===============================================================================
  VERSION DATE          AUTHOR(S)       DESCRIPTION
  ------- -----------   --------------- -----------------------------------------
  1.0     09/19/2010    Kathy Poling    Initial creation of the procedure
  1.1     12/12/2012    Kathy Poling    Added parm to procedure RFC 36582
  1.2     04/21/2013    Kathy Poling    Added count to verify if notification needs to be sent
  1.3     08/13/2013    Kathy Poling    Added global temp table to remove hz_parties for performance
  ********************************************************************************/

  PROCEDURE product_exceptions(p_bu_nm IN VARCHAR2) IS
    --
    -- Package Variables
    --
    l_dflt_email VARCHAR2(200) := 'HDSRebateManagementSystemNotifications@hdsupply.com'; --'HDSRebateManagementSystemNotifications@hdsupply.com';
    l_req_id     NUMBER NULL;

    l_err_msg  VARCHAR2(3000);
    l_err_code NUMBER;
    l_sec      VARCHAR2(255);
    l_inv_org      CONSTANT hr_all_organization_units.organization_id%TYPE := 84; --HDS Rebates USA - Inv
    l_category_set CONSTANT inv.mtl_category_sets_tl.category_set_id%TYPE := 1100000041; --HDS Supplier Category
    l_category NUMBER := 1123; --Rebates default item category
    l_count    NUMBER; --Version 1.2
    l_lob      VARCHAR(100) := 'HDS_LOB'; --Version 1.3
    -- Error DEBUG
    l_err_callfrom  VARCHAR2(75) DEFAULT 'XXCUS_TM_INTERFACE_PKG.PRODUCT_EXCEPTIONS';
    l_err_callpoint VARCHAR2(75) DEFAULT 'START';
    l_distro_list   VARCHAR2(75) DEFAULT 'HDSOracleDevelopers@hdsupply.com';

    -- Mailer
    l_sender      VARCHAR2(100);
    l_sid         VARCHAR2(8);
    l_body        VARCHAR2(32767);
    l_body_header VARCHAR2(32767);
    l_body_detail VARCHAR2(32767);
    l_body_footer VARCHAR2(32767);
    l_host        VARCHAR2(256) := 'mailoutrelay.hdsupply.net';
    l_hostport    VARCHAR2(20) := '25';

    CURSOR c_sku IS
      SELECT r.party_name, COUNT(*) COUNT
        FROM xxcus.xxcus_rebate_receipt_curr_tbl r
            ,inv.mtl_system_items_b              i
            ,inv.mtl_item_categories             c
            ,apps.xxcus_cust_prod_rebt_v         cp
            ,xxcus.xxcus_rebate_lob_parties_gtt  bp --8/14/13  testing
             --,ar.hz_parties                       bp  --8/14/13  testing
            ,ar.hz_party_sites          ps
            ,xxcus.xxcus_rebate_uom_tbl xref
       WHERE r.bu_nm = nvl(p_bu_nm, r.bu_nm)
         AND r.status_flag = 'N'
         AND nvl(r.receipt, -1) <> -1 --8/14/13 changed to nvl
         AND r.item_cost_amt <> 0
         AND r.recpt_qty <> 0
         AND r.sku_cd || '~' || r.bu_nm = i.description --i.segment1
         AND i.organization_id = l_inv_org
         AND i.enabled_flag = 'Y'
         AND i.inventory_item_id = c.inventory_item_id
         AND c.organization_id = l_inv_org
         AND c.category_set_id = l_category_set
         AND r.cust_account_id = cp.cust_acct_id(+)
         AND r.geo_loc_hier_lob_nm = bp.known_as
         AND r.pt_vndr_cd || '~' || r.bu_cd = ps.party_site_number
         AND r.prod_uom_cd = xref.purch_uom_cd --only items with a valid uom
         AND r.bu_nm = xref.bu_nm
         AND upper(r.src_sys_nm) = upper(xref.src_sys_nm)
         AND c.category_id = l_category
         AND nvl(cp.cust_acct_id, -1) <> -1 --8/14/13 changed to nvl
       GROUP BY r.party_name
       ORDER BY 1;

  BEGIN

    --Version 1.3
    --Insert Global Line Information GTT
    l_sec := 'Loading LOB Party to global temporary table; ';
    fnd_file.put_line(fnd_file.log, l_sec);
    fnd_file.put_line(fnd_file.output, l_sec);

    INSERT INTO xxcus.xxcus_rebate_lob_parties_gtt
      (SELECT party_id
             ,party_number
             ,party_name
             ,party_type
             ,validated_flag
             ,last_updated_by
             ,creation_date
             ,created_by
             ,last_update_date
             ,attribute_category
             ,attribute1
             ,attribute2
             ,attribute3
             ,attribute4
             ,attribute5
             ,attribute6
             ,attribute7
             ,attribute8
             ,attribute9
             ,attribute10
             ,attribute11
             ,attribute12
             ,attribute13
             ,attribute14
             ,attribute15
             ,attribute16
             ,attribute17
             ,attribute18
             ,attribute19
             ,attribute20
             ,attribute21
             ,attribute22
             ,attribute23
             ,attribute24
             ,orig_system_reference
             ,known_as
             ,country
             ,address1
             ,address2
             ,address3
             ,address4
             ,city
             ,postal_code
             ,state
             ,province
             ,status
             ,county
             ,category_code
             ,object_version_number
             ,created_by_module
         FROM ar.hz_parties
        WHERE attribute1 = l_lob);

    --Checking to see if a notification needs to be sent
    --Version 1.2
    BEGIN

      l_sec := 'Checking count to see if notification is sent; ';
      fnd_file.put_line(fnd_file.log, l_sec);
      fnd_file.put_line(fnd_file.output, l_sec);
      SELECT COUNT(*)
        INTO l_count
        FROM xxcus.xxcus_rebate_receipt_curr_tbl r
            ,inv.mtl_system_items_b              i
            ,inv.mtl_item_categories             c
            ,apps.xxcus_cust_prod_rebt_v         cp
            ,xxcus.xxcus_rebate_lob_parties_gtt  bp --8/14/13  testing
             --,ar.hz_parties                       bp  --8/14/13  testing
            ,ar.hz_party_sites          ps
            ,xxcus.xxcus_rebate_uom_tbl xref
       WHERE r.bu_nm = nvl(p_bu_nm, r.bu_nm)
         AND r.status_flag = 'N'
         AND nvl(r.receipt, -1) <> -1 --8/14/13 changed to nvl
         AND r.item_cost_amt <> 0
         AND r.recpt_qty <> 0
         AND r.sku_cd || '~' || r.bu_nm = i.description --i.segment1
         AND i.organization_id = l_inv_org
         AND i.enabled_flag = 'Y'
         AND i.inventory_item_id = c.inventory_item_id
         AND c.organization_id = l_inv_org
         AND c.category_set_id = l_category_set
         AND r.cust_account_id = cp.cust_acct_id(+)
         AND r.geo_loc_hier_lob_nm = bp.known_as
         AND r.pt_vndr_cd || '~' || r.bu_cd = ps.party_site_number
         AND r.prod_uom_cd = xref.purch_uom_cd --only items with a valid uom
         AND r.bu_nm = xref.bu_nm
         AND upper(r.src_sys_nm) = upper(xref.src_sys_nm)
         AND c.category_id = l_category
         AND nvl(cp.cust_acct_id, -1) <> -1 --8/14/13 changed to nvl
      ;
    EXCEPTION
      WHEN OTHERS THEN
        l_count := 0;
    END;

    IF l_count > 0
    THEN
      --Version 1.2

      dbms_output.enable(100000);

      SELECT lower(NAME) INTO l_sid FROM v$database;
      l_sender := 'Oracle.Applications_' || l_sid || '@hdsupply.com';

      --dbms_output.put_line('sid = ' ||l_sid);

      l_body_header := '<style type="text/css">
.style1 {
    border-style: solid;
    border-width: 1px;
}
.style2 {
    border: 1px solid #000000;
}
.style3 {
    border: 1px solid #000000;
--    background-color: #FFFF00;
}
.style4 {
    color: #FFFF00;
    border: 1px solid #000000;
    background-color: #000000;
}
.style5 {
    font-size: large;
}
.style6 {
    font-size: xx-large;
}
.style7 {
    color: #FFCC00;
}
</style>
<p><span class="style6"><span class="style7"><strong>HD</strong></span><strong>SUPPLY</strong></span><br />
<span class="style5">REBATES - Not all product have been classified for</span></p>
     <BR><table style="width: 100%">  <tr>' ||
                       '<td class="style4"><B>REBATE VENDOR</B></td><td class="style4"><B>BUSINESS NAME</B></td><td class="style4"><B>NUMBER OF SKU</B></td><tr>';
      -- '<td class="style4"><B>REBATE VENDOR</B></td><td class="style4"><B>NUMBER OF SKU</B></td><tr>';
      FOR c_email IN c_sku
      LOOP
        --Get list of SKU info for the Email.

        l_body_detail := l_body_detail || '<td class="style3">' ||
                         c_email.party_name || '</td><td class="style3">' ||
                        --p_bu_nm || '</td><td class="style3">' ||
                         c_email.count || '</td><TR>';

        fnd_file.put_line(fnd_file.log
                         ,c_email.party_name || ' | ' || c_email.count);

      END LOOP;

      l_body_footer := l_body_footer ||
                       '<BR>The following Vendors have Product Specific Rebates that SKUs
                                              need a classification prior to loading receipts.';

      l_body_footer := l_body_footer ||
                       '<BR>Use the Rebate UOM and Product Classification Maintenance Tool:';
      l_body        := l_body_header || l_body_detail || l_body_footer;

      -- Send email

      l_sec := 'Sending email';

      --email sent to support team to correct uom xref
      xxcus_misc_pkg.html_email(p_to            => l_dflt_email
                               ,p_from          => l_sender
                               ,p_text          => 'test'
                               ,p_subject       => '***ALERT***Classification For Product Specific Rebates Not Setup***'
                               ,p_html          => l_body
                               ,p_smtp_hostname => l_host
                               ,p_smtp_portnum  => l_hostport);

    END IF; --Version 1.2
  EXCEPTION
    WHEN program_error THEN
      ROLLBACK;
      l_err_code := 2;
      fnd_file.put_line(fnd_file.log, l_sec);
      fnd_file.put_line(fnd_file.output, l_sec);
      l_err_msg := l_err_msg || ' ERROR ' || substr(SQLERRM, 1, 2000);

      fnd_file.put_line(fnd_file.log, l_err_msg);
      fnd_file.put_line(fnd_file.output, l_err_msg);
      l_err_callpoint := l_sec;

      xxcus_error_pkg.xxcus_error_main_api(p_called_from       => l_err_callfrom
                                          ,p_calling           => l_err_callpoint
                                          ,p_request_id        => l_req_id
                                          ,p_ora_error_msg     => substr(SQLERRM
                                                                        ,1
                                                                        ,2000)
                                          ,p_error_desc        => substr(l_err_msg
                                                                        ,1
                                                                        ,2000)
                                          ,p_distribution_list => l_distro_list
                                          ,p_module            => 'TM');

    WHEN OTHERS THEN
      fnd_file.put_line(fnd_file.log, l_sec);
      fnd_file.put_line(fnd_file.output, l_sec);
      l_err_code := 2;
      l_err_msg  := l_sec || l_err_msg || ' ERROR ' || SQLCODE ||
                    substr(SQLERRM, 1, 2000);

      fnd_file.put_line(fnd_file.log, l_err_msg);
      fnd_file.put_line(fnd_file.output, l_err_msg);
      l_err_callpoint := l_sec;

      xxcus_error_pkg.xxcus_error_main_api(p_called_from       => l_err_callfrom
                                          ,p_calling           => l_err_callpoint
                                          ,p_request_id        => l_req_id
                                          ,p_ora_error_msg     => substr(SQLERRM
                                                                        ,1
                                                                        ,2000)
                                          ,p_error_desc        => substr(l_err_msg
                                                                        ,1
                                                                        ,2000)
                                          ,p_distribution_list => l_distro_list
                                          ,p_module            => 'TM');

  END product_exceptions;

  /*******************************************************************************
  * Procedure:   UC4_BRANCH
  * Description: This is for UC4 to start the concurrent request to load Branches
  *              for the Rebate process at month end
  *
  HISTORY
  ===============================================================================
  VERSION DATE          AUTHOR(S)       DESCRIPTION
  ------- -----------   --------------- -----------------------------------------
  1.0     09/24/2010    Kathy Poling    Initial creation of the procedure

  ********************************************************************************/

  PROCEDURE uc4_branch(errbuf OUT VARCHAR2, retcode OUT NUMBER) IS
    --
    -- Package Variables
    --
    l_req_id             NUMBER NULL;
    v_phase              VARCHAR2(50);
    v_status             VARCHAR2(50);
    v_dev_status         VARCHAR2(50);
    v_dev_phase          VARCHAR2(50);
    v_message            VARCHAR2(250);
    v_error_message      VARCHAR2(3000);
    v_interval           NUMBER := 30; -- In seconds
    v_max_time           NUMBER := 15000; -- In seconds
    l_can_submit_request BOOLEAN := TRUE;
    l_err_msg            VARCHAR2(3000);
    l_err_code           NUMBER;
    l_sec                VARCHAR2(255);
    l_statement          VARCHAR2(9000);
    l_user_id            fnd_user.user_id%TYPE; --REBTINTERFACE user
    l_user CONSTANT VARCHAR2(100) := 'REBTINTERFACE';
    l_responsibility VARCHAR2(100) := 'XXCUS_CON'; --'HDS Trade Management Administrator - Rebates'

    -- Error DEBUG
    l_err_callfrom  VARCHAR2(75) DEFAULT 'XXCUS_TM_INTERFACE_PKG.UC4_BRANCH';
    l_err_callpoint VARCHAR2(75) DEFAULT 'START';
    l_distro_list   VARCHAR2(75) DEFAULT 'HDSOracleDevelopers@hdsupply.com';

  BEGIN

    SELECT user_id INTO l_user_id FROM fnd_user WHERE user_name = l_user;

    --  Setup parameters for running FND JOBS!
    l_can_submit_request := xxcus_misc_pkg.set_responsibility(l_user
                                                             ,l_responsibility);
    IF l_can_submit_request
    THEN
      l_sec := 'Global Variables are set.';

    ELSE

      --l_globalset := 'Global Variables are not set.';
      l_sec := 'Global Variables are not set for the ' || l_user || '.';
      fnd_file.put_line(fnd_file.log, l_sec);
      fnd_file.put_line(fnd_file.output, l_sec);
      RAISE program_error;
    END IF;

    l_sec := 'UC4 call to run concurrent request HDS TM Branch Load Process.';

    l_req_id := fnd_request.submit_request('XXCUS'
                                          ,'XXCUSLD'
                                          , --HDS TM Branch Load Process
                                           NULL
                                          ,NULL
                                          ,FALSE);
    COMMIT;

    IF (l_req_id != 0)
    THEN
      IF fnd_concurrent.wait_for_request(l_req_id
                                        ,v_interval
                                        ,v_max_time
                                        ,v_phase
                                        ,v_status
                                        ,v_dev_phase
                                        ,v_dev_status
                                        ,v_message)
      THEN
        v_error_message := chr(10) || 'ReqID=' || l_req_id || ' DPhase ' ||
                           v_dev_phase || ' DStatus ' || v_dev_status ||
                           chr(10) || ' MSG - ' || v_message;
        -- Error Returned
        IF v_dev_phase != 'COMPLETE' OR v_dev_status != 'NORMAL'
        THEN
          l_err_msg := 'An error occured in the running of the XXCUS_TM_INTERFACE_PKG HDS TM Branch Load Process request ID: ' ||
                       l_req_id || '   ' || v_error_message || '.';
          fnd_file.put_line(fnd_file.log, l_err_msg);
          fnd_file.put_line(fnd_file.output, l_err_msg);
          RAISE program_error;

        END IF;
        -- Then Success!
        retcode := 0;
      ELSE
        l_err_msg := 'EBS timed out waiting on XXCUS_TM_INTERFACE_PKG HDS TM Branch Load Process' ||
                     v_error_message || '.';
        fnd_file.put_line(fnd_file.log, l_err_msg);
        fnd_file.put_line(fnd_file.output, l_err_msg);
        RAISE program_error;
      END IF;

    ELSE
      l_err_msg := 'An error occured when trying to submit the XXCUS_TM_INTERFACE_PKG HDS TM Branch Load Process';
      fnd_file.put_line(fnd_file.log, l_err_msg);
      fnd_file.put_line(fnd_file.output, l_err_msg);
      RAISE program_error;
    END IF;

    dbms_output.put_line(l_sec);
    dbms_output.put_line('Request ID:  ' || l_req_id);

  EXCEPTION
    WHEN program_error THEN
      ROLLBACK;
      l_err_code := 2;
      l_err_msg  := l_err_msg || ' Error_Stack...' ||
                    dbms_utility.format_error_stack() ||
                    ' Error_Backtrace...' ||
                    dbms_utility.format_error_backtrace();
      dbms_output.put_line(l_err_msg);
      fnd_file.put_line(fnd_file.log, l_err_msg);
      fnd_file.put_line(fnd_file.output, l_err_msg);
      l_err_callpoint := l_sec;

      xxcus_error_pkg.xxcus_error_main_api(p_called_from       => l_err_callfrom
                                          ,p_calling           => l_err_callpoint
                                          ,p_request_id        => l_req_id
                                          ,p_ora_error_msg     => substr(regexp_replace(l_err_msg
                                                                                       ,'[[:cntrl:]]'
                                                                                       ,NULL)
                                                                        ,1
                                                                        ,2000)
                                          ,p_error_desc        => substr(l_err_msg
                                                                        ,1
                                                                        ,240)
                                          ,p_distribution_list => l_distro_list
                                          ,p_module            => 'TM');

    WHEN OTHERS THEN
      fnd_file.put_line(fnd_file.log, l_sec);
      fnd_file.put_line(fnd_file.output, l_sec);
      l_err_code := 2;
      l_err_msg  := l_err_msg || ' Error_Stack...' ||
                    dbms_utility.format_error_stack() ||
                    ' Error_Backtrace...' ||
                    dbms_utility.format_error_backtrace();
      dbms_output.put_line(l_err_msg);
      fnd_file.put_line(fnd_file.log, l_err_msg);
      fnd_file.put_line(fnd_file.output, l_err_msg);
      l_err_callpoint := l_sec;

      xxcus_error_pkg.xxcus_error_main_api(p_called_from       => l_err_callfrom
                                          ,p_calling           => l_err_callpoint
                                          ,p_request_id        => l_req_id
                                          ,p_ora_error_msg     => substr(regexp_replace(l_err_msg
                                                                                       ,'[[:cntrl:]]'
                                                                                       ,NULL)
                                                                        ,1
                                                                        ,2000)
                                          ,p_error_desc        => substr(l_err_msg
                                                                        ,1
                                                                        ,240)
                                          ,p_distribution_list => l_distro_list
                                          ,p_module            => 'TM');

  END uc4_branch;

  /*******************************************************************************
  * Procedure:   UC4_VENDORS
  * Description: This is for UC4 to start the concurrent request to load Rebate Vendors
  *              for the Rebate process at month end
  *
  HISTORY
  ===============================================================================
  VERSION DATE          AUTHOR(S)       DESCRIPTION
  ------- -----------   --------------- -----------------------------------------
  1.0     09/24/2010    Kathy Poling    Initial creation of the procedure

  ********************************************************************************/

  PROCEDURE uc4_vendors(errbuf OUT VARCHAR2, retcode OUT NUMBER) IS
    --
    -- Package Variables
    --
    l_req_id             NUMBER NULL;
    v_phase              VARCHAR2(50);
    v_status             VARCHAR2(50);
    v_dev_status         VARCHAR2(50);
    v_dev_phase          VARCHAR2(50);
    v_message            VARCHAR2(250);
    v_error_message      VARCHAR2(3000);
    v_interval           NUMBER := 30; -- In seconds
    v_max_time           NUMBER := 15000; -- In seconds
    l_can_submit_request BOOLEAN := TRUE;
    l_err_msg            VARCHAR2(3000);
    l_err_code           NUMBER;
    l_sec                VARCHAR2(255);
    l_statement          VARCHAR2(9000);
    l_user_id            fnd_user.user_id%TYPE; --REBTINTERFACE user
    l_user CONSTANT VARCHAR2(100) := 'REBTINTERFACE';
    l_responsibility VARCHAR2(100) := 'XXCUS_CON'; --'HDS Trade Management Administrator - Rebates'

    -- Error DEBUG
    l_err_callfrom  VARCHAR2(75) DEFAULT 'XXCUS_TM_INTERFACE_PKG.UC4_VENDORS';
    l_err_callpoint VARCHAR2(75) DEFAULT 'START';
    l_distro_list   VARCHAR2(75) DEFAULT 'HDSOracleDevelopers@hdsupply.com';

  BEGIN

    SELECT user_id INTO l_user_id FROM fnd_user WHERE user_name = l_user;

    --  Setup parameters for running FND JOBS!
    l_can_submit_request := xxcus_misc_pkg.set_responsibility(l_user
                                                             ,l_responsibility);
    IF l_can_submit_request
    THEN
      l_sec := 'Global Variables are set.';

    ELSE

      --l_globalset := 'Global Variables are not set.';
      l_sec := 'Global Variables are not set for the ' || l_user || '.';
      fnd_file.put_line(fnd_file.log, l_sec);
      fnd_file.put_line(fnd_file.output, l_sec);
      RAISE program_error;
    END IF;

    l_sec := 'UC4 call to run concurrent request HDS TM Customer Load Process.';

    l_req_id := fnd_request.submit_request('XXCUS'
                                          ,'XXCUSVNDR'
                                          , --HDS TM Customer Load Process
                                           NULL
                                          ,NULL
                                          ,FALSE);
    COMMIT;

    IF (l_req_id != 0)
    THEN
      IF fnd_concurrent.wait_for_request(l_req_id
                                        ,v_interval
                                        ,v_max_time
                                        ,v_phase
                                        ,v_status
                                        ,v_dev_phase
                                        ,v_dev_status
                                        ,v_message)
      THEN
        v_error_message := chr(10) || 'ReqID=' || l_req_id || ' DPhase ' ||
                           v_dev_phase || ' DStatus ' || v_dev_status ||
                           chr(10) || ' MSG - ' || v_message;
        -- Error Returned
        IF v_dev_phase != 'COMPLETE' OR v_dev_status != 'NORMAL'
        THEN
          l_err_msg := 'An error occured in the running of the XXCUS_TM_INTERFACE_PKG HDS TM Customer Load Process request ID: ' ||
                       l_req_id || '   ' || v_error_message || '.';
          fnd_file.put_line(fnd_file.log, l_err_msg);
          fnd_file.put_line(fnd_file.output, l_err_msg);
          RAISE program_error;

        END IF;
        -- Then Success!
        retcode := 0;
      ELSE
        l_err_msg := 'EBS timed out waiting on XXCUS_TM_INTERFACE_PKG HDS TM Customer Load Process' ||
                     v_error_message || '.';
        fnd_file.put_line(fnd_file.log, l_err_msg);
        fnd_file.put_line(fnd_file.output, l_err_msg);
        RAISE program_error;
      END IF;

    ELSE
      l_err_msg := 'An error occured when trying to submit the XXCUS_TM_INTERFACE_PKG HDS TM Customer Load Process';
      fnd_file.put_line(fnd_file.log, l_err_msg);
      fnd_file.put_line(fnd_file.output, l_err_msg);
      RAISE program_error;
    END IF;

    dbms_output.put_line(l_sec);
    dbms_output.put_line('Request ID:  ' || l_req_id);

  EXCEPTION
    WHEN program_error THEN
      ROLLBACK;
      l_err_code := 2;
      l_err_msg  := l_err_msg || ' Error_Stack...' ||
                    dbms_utility.format_error_stack() ||
                    ' Error_Backtrace...' ||
                    dbms_utility.format_error_backtrace();
      dbms_output.put_line(l_err_msg);
      fnd_file.put_line(fnd_file.log, l_err_msg);
      fnd_file.put_line(fnd_file.output, l_err_msg);
      l_err_callpoint := l_sec;

      xxcus_error_pkg.xxcus_error_main_api(p_called_from       => l_err_callfrom
                                          ,p_calling           => l_err_callpoint
                                          ,p_request_id        => l_req_id
                                          ,p_ora_error_msg     => substr(regexp_replace(l_err_msg
                                                                                       ,'[[:cntrl:]]'
                                                                                       ,NULL)
                                                                        ,1
                                                                        ,2000)
                                          ,p_error_desc        => substr(l_err_msg
                                                                        ,1
                                                                        ,240)
                                          ,p_distribution_list => l_distro_list
                                          ,p_module            => 'TM');

    WHEN OTHERS THEN
      fnd_file.put_line(fnd_file.log, l_sec);
      fnd_file.put_line(fnd_file.output, l_sec);
      l_err_code := 2;
      l_err_msg  := l_err_msg || ' Error_Stack...' ||
                    dbms_utility.format_error_stack() ||
                    ' Error_Backtrace...' ||
                    dbms_utility.format_error_backtrace();
      dbms_output.put_line(l_err_msg);
      fnd_file.put_line(fnd_file.log, l_err_msg);
      fnd_file.put_line(fnd_file.output, l_err_msg);
      l_err_callpoint := l_sec;

      xxcus_error_pkg.xxcus_error_main_api(p_called_from       => l_err_callfrom
                                          ,p_calling           => l_err_callpoint
                                          ,p_request_id        => l_req_id
                                          ,p_ora_error_msg     => substr(regexp_replace(l_err_msg
                                                                                       ,'[[:cntrl:]]'
                                                                                       ,NULL)
                                                                        ,1
                                                                        ,2000)
                                          ,p_error_desc        => substr(l_err_msg
                                                                        ,1
                                                                        ,240)
                                          ,p_distribution_list => l_distro_list
                                          ,p_module            => 'TM');

  END uc4_vendors;

  /*******************************************************************************
  * Procedure:   UC4_PRODUCTS
  * Description: This is for UC4 to start the concurrent request to load Products
  *              for the Rebate process at month end
  *
  HISTORY
  ===============================================================================
  VERSION DATE          AUTHOR(S)       DESCRIPTION
  ------- -----------   --------------- -----------------------------------------
  1.0     09/24/2010    Kathy Poling    Initial creation of the procedure
  1.1     01/15/2013    Kathy Poling    Adding parms fperiod and bu for changing to
                                        partitioned tables.  Added a concurrent program
                                        that will allow the default partition to be
                                        incompatible and the others can run in parallel
                                        RFC 36582
  1.2     05/02/2013    Kathy Poling    Added check for BU's the actually rollup to
                                        the same BU in Oracle
                                        SR 200882
  1.3     12/02/2013    Kathy Poling    Added 'RAISE' to error message for SDW
                                        RFC 38762
  ********************************************************************************/

  PROCEDURE uc4_products(errbuf    OUT VARCHAR2
                        ,retcode   OUT NUMBER
                        ,p_fperiod IN VARCHAR2
                        ,p_bu_nm   IN VARCHAR2) IS
    --
    -- Package Variables
    --
    l_req_id             NUMBER NULL;
    v_phase              VARCHAR2(50);
    v_status             VARCHAR2(50);
    v_dev_status         VARCHAR2(50);
    v_dev_phase          VARCHAR2(50);
    v_message            VARCHAR2(250);
    v_error_message      VARCHAR2(3000);
    v_interval           NUMBER := 30; -- In seconds
    v_max_time           NUMBER := 15000; -- In seconds
    l_can_submit_request BOOLEAN := TRUE;
    l_err_msg            VARCHAR2(3000);
    l_err_code           NUMBER;
    l_sec                VARCHAR2(255);
    l_statement          VARCHAR2(9000);
    l_oracle_bu          VARCHAR2(50); --Version 1.2
    l_app_name           VARCHAR2(30) := 'XXCUS'; --Version 1.2
    l_count              NUMBER; --Version 1.2
    l_request_running    NUMBER; --Version 1.2
    l_sleep              NUMBER := 30; --Version 1.2
    l_req_id_wait        NUMBER NULL; --Version 1.2
    l_status             VARCHAR2(1); --Version 1.2
    l_user_id            fnd_user.user_id%TYPE; --REBTINTERFACE user
    l_user CONSTANT VARCHAR2(100) := 'REBTINTERFACE';
    l_responsibility VARCHAR2(100) := 'XXCUS_CON'; --'HDS Trade Management Administrator - Rebates'
    l_partition      VARCHAR2(30);
    l_program        VARCHAR2(30);

    -- Error DEBUG
    l_err_callfrom  VARCHAR2(75) DEFAULT 'XXCUS_TM_INTERFACE_PKG.UC4_PRODUCTS';
    l_err_callpoint VARCHAR2(75) DEFAULT 'START';
    l_distro_list   VARCHAR2(75) DEFAULT 'HDSOracleDevelopers@hdsupply.com';

  BEGIN

    SELECT user_id INTO l_user_id FROM fnd_user WHERE user_name = l_user;

    --  Setup parameters for running FND JOBS!
    l_can_submit_request := xxcus_misc_pkg.set_responsibility(l_user
                                                             ,l_responsibility);
    IF l_can_submit_request
    THEN
      l_sec := 'Global Variables are set.';

    ELSE

      --l_globalset := 'Global Variables are not set.';
      l_sec := 'Global Variables are not set for the ' || l_user || '.';
      fnd_file.put_line(fnd_file.log, l_sec);
      fnd_file.put_line(fnd_file.output, l_sec);
      RAISE program_error;
    END IF;

    BEGIN
      --lookup for mapping of bu name to which concurrent program to run.  If the bu defaults to 'OTHER
      --the request is incompatibile
      SELECT tag --lookup_code   04/22/2013
        INTO l_partition
        FROM fnd_lookup_values_vl
       WHERE lookup_type = 'XXCUS_REBATES_TABLE_PARTITION'
         AND meaning = p_bu_nm
         AND lookup_code NOT LIKE 'SDW.%'
         AND enabled_flag = 'Y'
         AND nvl(end_date_active, SYSDATE) >= SYSDATE;
      dbms_output.put_line('BU: ' || p_bu_nm || '  Will use partition: ' ||
                           l_partition);
    EXCEPTION
      WHEN no_data_found THEN
        l_partition := 'OTHER';
    END;

    IF l_partition = 'OTHER'
    THEN
      l_program := 'XXCUSREBATEITEM_DEFAULT'; --HDS TM Product Misc Load
    ELSE
      l_program := 'XXCUSREBATEITEM'; --HDS TM Product Load Process
    END IF;

    --Version 1.2  5/2/2013
    --adding check for BU's the actually rollup to the same BU in Oracle, need to wait if receipts
    --are processing (example:  FM  and WhiteCap Construction )
    BEGIN
      SELECT oracle_bu_nm
        INTO l_oracle_bu
        FROM apps.xxcus_rebate_bu_nm_v
       WHERE bu_nm = p_bu_nm;
    EXCEPTION
      WHEN no_data_found THEN
        l_err_msg := 'Failed to find Oracle BU mapping : ' || p_bu_nm ||
                     ' : ' || SQLERRM;
        fnd_file.put_line(fnd_file.log, l_err_msg);
        RAISE program_error;
    END;

    SELECT COUNT(*)
      INTO l_count
      FROM apps.xxcus_rebate_bu_nm_v
     WHERE oracle_bu_nm = l_oracle_bu;

    IF l_count > 1
    THEN

      BEGIN

        SELECT r.request_id, r.phase_code
          INTO l_req_id_wait, l_status
          FROM applsys.fnd_concurrent_requests r
              ,applsys.fnd_concurrent_programs p
              ,apps.xxcus_rebate_bu_nm_v       xrbn
         WHERE r.concurrent_program_id = p.concurrent_program_id
           AND p.concurrent_program_name IN
               ('XXCUSREBATEITEM_DEFAULT', 'XXCUSREBATEITEM')
           AND p.application_id = 20003
           AND r.argument1 = p_fperiod
           AND r.argument2 = xrbn.bu_nm
           AND r.phase_code <> 'C'
           AND xrbn.oracle_bu_nm IN
               (SELECT oracle_bu_nm
                  FROM apps.xxcus_rebate_bu_nm_v
                 GROUP BY oracle_bu_nm
                HAVING COUNT(oracle_bu_nm) > 1)
           AND xrbn.oracle_bu_nm IN
               (SELECT oracle_bu_nm
                  FROM apps.xxcus_rebate_bu_nm_v a
                 WHERE a.bu_nm = p_bu_nm);

      EXCEPTION
        WHEN no_data_found THEN
          l_req_id_wait := NULL;
          l_status      := NULL;
      END;

      IF l_req_id_wait IS NOT NULL AND nvl(l_status, 'X') <> 'C'
      THEN
        --dbms_lock.sleep(l_sleep);

        IF fnd_concurrent.wait_for_request(l_req_id_wait
                                          ,v_interval
                                          ,v_max_time
                                          ,v_phase
                                          ,v_status
                                          ,v_dev_phase
                                          ,v_dev_status
                                          ,v_message)
        THEN
          l_sec := 'ReqID=' || l_req_id_wait || ' DPhase ' || v_dev_phase ||
                   ' DStatus ' || v_dev_status || chr(10) || ' MSG - ' ||
                   v_message || ' ProductsProcessing for BU= ' ||
                   l_oracle_bu || ' Time = ' || SYSDATE;
          IF v_dev_phase = 'COMPLETE'
          THEN
            l_sec := 'Oracle BU finished submitting next BU request HDS TM Product Load.';

            l_req_id := fnd_request.submit_request(l_app_name
                                                  ,l_program
                                                  ,NULL
                                                  ,NULL
                                                  ,FALSE
                                                  ,p_fperiod
                                                  ,p_bu_nm);
            COMMIT;

            IF (l_req_id != 0)
            THEN
              IF fnd_concurrent.wait_for_request(l_req_id
                                                ,v_interval
                                                ,v_max_time
                                                ,v_phase
                                                ,v_status
                                                ,v_dev_phase
                                                ,v_dev_status
                                                ,v_message)
              THEN
                v_error_message := chr(10) || 'ReqID=' || l_req_id ||
                                   ' DPhase ' || v_dev_phase || ' DStatus ' ||
                                   v_dev_status || chr(10) || ' MSG - ' ||
                                   v_message;
                -- Error Returned
                IF v_dev_phase != 'COMPLETE' OR v_dev_status != 'NORMAL'
                THEN
                  l_err_msg := 'An error occured in the running of the XXCUS_TM_INTERFACE_PKG HDS TM Product Load Process request ID: ' ||
                               l_req_id || '   ' || v_error_message || '.';
                  fnd_file.put_line(fnd_file.log, l_err_msg);
                  fnd_file.put_line(fnd_file.output, l_err_msg);
                  RAISE program_error;

                END IF;
                -- Then Success!
                retcode := 0;
              ELSE
                l_err_msg := 'EBS timed out waiting on XXCUS_TM_INTERFACE_PKG HDS TM Product Load Process' ||
                             v_error_message || '.';
                fnd_file.put_line(fnd_file.log, l_err_msg);
                fnd_file.put_line(fnd_file.output, l_err_msg);
                RAISE program_error;
              END IF;

            ELSE
              l_err_msg := 'An error occured when trying to submit the XXCUS_TM_INTERFACE_PKG HDS TM Product Load Process';
              fnd_file.put_line(fnd_file.log, l_err_msg);
              fnd_file.put_line(fnd_file.output, l_err_msg);
              RAISE program_error;
            END IF;

          END IF;
        END IF;

      ELSE
        l_sec := 'UC4 call to run concurrent request HDS TM Product Load Process.';

        l_req_id := fnd_request.submit_request(l_app_name
                                              ,l_program
                                              ,NULL
                                              ,NULL
                                              ,FALSE
                                              ,p_fperiod
                                              ,p_bu_nm);
        COMMIT;

        IF (l_req_id != 0)
        THEN
          IF fnd_concurrent.wait_for_request(l_req_id
                                            ,v_interval
                                            ,v_max_time
                                            ,v_phase
                                            ,v_status
                                            ,v_dev_phase
                                            ,v_dev_status
                                            ,v_message)
          THEN
            v_error_message := chr(10) || 'ReqID=' || l_req_id ||
                               ' DPhase ' || v_dev_phase || ' DStatus ' ||
                               v_dev_status || chr(10) || ' MSG - ' ||
                               v_message;
            -- Error Returned
            IF v_dev_phase != 'COMPLETE' OR v_dev_status != 'NORMAL'
            THEN
              l_err_msg := 'An error occured in the running of the XXCUS_TM_INTERFACE_PKG HDS TM Product Load Process request ID: ' ||
                           l_req_id || '   ' || v_error_message || '.';
              fnd_file.put_line(fnd_file.log, l_err_msg);
              fnd_file.put_line(fnd_file.output, l_err_msg);
              RAISE program_error;

            END IF;
            -- Then Success!
            retcode := 0;
          ELSE
            l_err_msg := 'EBS timed out waiting on XXCUS_TM_INTERFACE_PKG HDS TM Product Load Process' ||
                         v_error_message || '.';
            fnd_file.put_line(fnd_file.log, l_err_msg);
            fnd_file.put_line(fnd_file.output, l_err_msg);
            RAISE program_error;
          END IF;

        ELSE
          l_err_msg := 'An error occured when trying to submit the XXCUS_TM_INTERFACE_PKG HDS TM Product Load Process';
          fnd_file.put_line(fnd_file.log, l_err_msg);
          fnd_file.put_line(fnd_file.output, l_err_msg);
          RAISE program_error;
        END IF;

      END IF;

    ELSE
      -- end 5/2/2013

      l_sec := 'UC4 call to run concurrent request HDS TM Product Load Process.';

      l_req_id := fnd_request.submit_request(l_app_name
                                            ,l_program
                                            ,NULL
                                            ,NULL
                                            ,FALSE
                                            ,p_fperiod
                                            ,p_bu_nm);
      COMMIT;

      IF (l_req_id != 0)
      THEN
        IF fnd_concurrent.wait_for_request(l_req_id
                                          ,v_interval
                                          ,v_max_time
                                          ,v_phase
                                          ,v_status
                                          ,v_dev_phase
                                          ,v_dev_status
                                          ,v_message)
        THEN
          v_error_message := chr(10) || 'ReqID=' || l_req_id || ' DPhase ' ||
                             v_dev_phase || ' DStatus ' || v_dev_status ||
                             chr(10) || ' MSG - ' || v_message;
          -- Error Returned
          IF v_dev_phase != 'COMPLETE' OR v_dev_status != 'NORMAL'
          THEN
            l_err_msg := 'An error occured in the running of the XXCUS_TM_INTERFACE_PKG HDS TM Product Load Process request ID: ' ||
                         l_req_id || '   ' || v_error_message || '.';
            fnd_file.put_line(fnd_file.log, l_err_msg);
            fnd_file.put_line(fnd_file.output, l_err_msg);
            RAISE program_error;

          END IF;
          -- Then Success!
          retcode := 0;
        ELSE
          l_err_msg := 'EBS timed out waiting on XXCUS_TM_INTERFACE_PKG HDS TM Product Load Process' ||
                       v_error_message || '.';
          fnd_file.put_line(fnd_file.log, l_err_msg);
          fnd_file.put_line(fnd_file.output, l_err_msg);
          RAISE program_error;
        END IF;

      ELSE
        l_err_msg := 'An error occured when trying to submit the XXCUS_TM_INTERFACE_PKG HDS TM Product Load Process';
        fnd_file.put_line(fnd_file.log, l_err_msg);
        fnd_file.put_line(fnd_file.output, l_err_msg);
        RAISE program_error;
      END IF;

    END IF;

    dbms_output.put_line(l_sec);
    dbms_output.put_line('Request ID:  ' || l_req_id);

  EXCEPTION
    WHEN program_error THEN
      ROLLBACK;
      l_err_code := 2;
      l_err_msg  := l_err_msg || ' Error_Stack...' ||
                    dbms_utility.format_error_stack() ||
                    ' Error_Backtrace...' ||
                    dbms_utility.format_error_backtrace();
      fnd_file.put_line(fnd_file.log, l_err_msg);
      fnd_file.put_line(fnd_file.output, l_err_msg);
      l_err_callpoint := l_sec;

      xxcus_error_pkg.xxcus_error_main_api(p_called_from       => l_err_callfrom
                                          ,p_calling           => l_err_callpoint
                                          ,p_request_id        => l_req_id
                                          ,p_ora_error_msg     => substr(regexp_replace(l_err_msg
                                                                                       ,'[[:cntrl:]]'
                                                                                       ,NULL)
                                                                        ,1
                                                                        ,2000)
                                          ,p_error_desc        => substr(l_err_msg
                                                                        ,1
                                                                        ,240)
                                          ,p_distribution_list => l_distro_list
                                          ,p_module            => 'TM');

      dbms_output.put_line(l_err_code || ' Message code''|' || l_err_code ||
                           '|Exception: ' || l_err_msg);
      RAISE;

    WHEN OTHERS THEN
      fnd_file.put_line(fnd_file.log, l_sec);
      fnd_file.put_line(fnd_file.output, l_sec);
      l_err_code := 2;
      l_err_msg  := l_err_msg || ' Error_Stack...' ||
                    dbms_utility.format_error_stack() ||
                    ' Error_Backtrace...' ||
                    dbms_utility.format_error_backtrace();
      fnd_file.put_line(fnd_file.log, l_err_msg);
      fnd_file.put_line(fnd_file.output, l_err_msg);
      l_err_callpoint := l_sec;

      xxcus_error_pkg.xxcus_error_main_api(p_called_from       => l_err_callfrom
                                          ,p_calling           => l_err_callpoint
                                          ,p_request_id        => l_req_id
                                          ,p_ora_error_msg     => substr(regexp_replace(l_err_msg
                                                                                       ,'[[:cntrl:]]'
                                                                                       ,NULL)
                                                                        ,1
                                                                        ,2000)
                                          ,p_error_desc        => substr(l_err_msg
                                                                        ,1
                                                                        ,240)
                                          ,p_distribution_list => l_distro_list
                                          ,p_module            => 'TM');

      dbms_output.put_line(l_err_code || ' Message code''|' || l_err_code ||
                           '|Exception: ' || l_err_msg);
      RAISE;

  END uc4_products;

  /*******************************************************************************
  * Procedure:   UC4_RECEIPTS
  * Description: This is for UC4 to start the concurrent request to load Receipts
  *              for the Rebate process at month end
  *
  HISTORY
  ===============================================================================
  VERSION DATE          AUTHOR(S)       DESCRIPTION
  ------- -----------   --------------- -----------------------------------------
  1.0     09/24/2010    Kathy Poling    Initial creation of the procedure
  1.1     01/15/2013    Kathy Poling    Adding parm bu for changing to partitioned
                                        tables.  Added a concurrent program that
                                        will allow the default partition to be
                                        incompatible and the others can run in parallel
                                        RFC 36582
  1.2     05/02/2013    Kathy Poling    Added check for BU's the actually rollup to
                                        the same BU in Oracle
                                        SR 200882
  1.3     12/02/2013    Kathy Poling    Added 'RAISE' to error message for SDW
                                        RFC 38762
  ********************************************************************************/
  --
  PROCEDURE uc4_receipts(errbuf    OUT VARCHAR2
                        ,retcode   OUT NUMBER
                        ,p_fperiod IN VARCHAR2
                        ,p_bu_nm   IN VARCHAR2) IS
    --
    -- Package Variables
    --
    l_req_id             NUMBER NULL;
    v_phase              VARCHAR2(50);
    v_status             VARCHAR2(50);
    v_dev_status         VARCHAR2(50);
    v_dev_phase          VARCHAR2(50);
    v_message            VARCHAR2(250);
    v_error_message      VARCHAR2(3000);
    v_interval           NUMBER := 30; -- In seconds
    v_max_time           NUMBER := 150000; -- In seconds
    l_can_submit_request BOOLEAN := TRUE;
    l_err_msg            VARCHAR2(3000);
    l_err_code           NUMBER;
    l_sec                VARCHAR2(255);
    l_statement          VARCHAR2(9000);
    l_partition          VARCHAR2(30);
    l_program            VARCHAR2(30);
    l_oracle_bu          VARCHAR2(50); --Version 1.2
    l_app_name           VARCHAR2(30) := 'XXCUS'; --Version 1.2
    l_count              NUMBER; --Version 1.2
    l_status             VARCHAR2(1); --Version 1.2
    l_request_running    NUMBER; --Version 1.2
    l_sleep              NUMBER := 60; --Version 1.2
    l_req_id_wait        NUMBER NULL; --Version 1.2
    l_user_id            fnd_user.user_id%TYPE; --REBTINTERFACE user
    l_user CONSTANT VARCHAR2(100) := 'REBTINTERFACE';
    l_responsibility VARCHAR2(100) := 'XXCUS_CON'; --'HDS Trade Management Administrator - Rebates'

    -- Error DEBUG
    l_err_callfrom  VARCHAR2(75) DEFAULT 'XXCUS_TM_INTERFACE_PKG.UC4_RECEIPTS';
    l_err_callpoint VARCHAR2(75) DEFAULT 'START';
    l_distro_list   VARCHAR2(75) DEFAULT 'HDSOracleDevelopers@hdsupply.com';

  BEGIN

    SELECT user_id INTO l_user_id FROM fnd_user WHERE user_name = l_user;

    --  Setup parameters for running FND JOBS!
    l_can_submit_request := xxcus_misc_pkg.set_responsibility(l_user
                                                             ,l_responsibility);
    IF l_can_submit_request
    THEN
      l_sec := 'Global Variables are set.';
      dbms_output.put_line(l_sec);
    ELSE

      --l_globalset := 'Global Variables are not set.';
      l_sec := 'Global Variables are not set for the ' || l_user || '.';
      fnd_file.put_line(fnd_file.log, l_sec);
      fnd_file.put_line(fnd_file.output, l_sec);
      dbms_output.put_line(l_sec);
      RAISE program_error;
    END IF;

    BEGIN
      --lookup for mapping of bu name to which concurrent program to run.  If the bu defaults to 'OTHER
      --the request is incompatibile
      SELECT tag --lookup_code  04/22/13
        INTO l_partition
        FROM fnd_lookup_values_vl
       WHERE lookup_type = 'XXCUS_REBATES_TABLE_PARTITION'
         AND meaning = p_bu_nm
         AND lookup_code NOT LIKE 'SDW.%'
         AND enabled_flag = 'Y'
         AND nvl(end_date_active, SYSDATE) >= SYSDATE;
      dbms_output.put_line('BU: ' || p_bu_nm || '  Will use partition: ' ||
                           l_partition);
    EXCEPTION
      WHEN no_data_found THEN
        l_partition := 'OTHER';
        dbms_output.put_line(l_partition);
    END;

    IF l_partition = 'OTHER'
    THEN
      l_program := 'XXCUSRECPT_DEFAULT';
    ELSE
      l_program := 'XXCUSRECPT';
    END IF;

    dbms_output.put_line(l_program);

    --Version 1.2  5/2/2013
    --adding check for BU's the actually rollup to the same BU in Oracle, need to wait if receipts
    --are processing (example:  FM  and WhiteCap Construction )
    BEGIN
      SELECT oracle_bu_nm
        INTO l_oracle_bu
        FROM apps.xxcus_rebate_bu_nm_v
       WHERE bu_nm = p_bu_nm;
    EXCEPTION
      WHEN no_data_found THEN
        l_err_msg := 'Failed to find Oracle BU mapping : ' || p_bu_nm ||
                     ' : ' || SQLERRM;
        fnd_file.put_line(fnd_file.log, l_err_msg);
        RAISE program_error;
    END;

    SELECT COUNT(*)
      INTO l_count
      FROM apps.xxcus_rebate_bu_nm_v
     WHERE oracle_bu_nm = l_oracle_bu;

    IF l_count > 1
    THEN

      BEGIN

        SELECT r.request_id, r.phase_code
          INTO l_req_id_wait, l_status
          FROM applsys.fnd_concurrent_requests r
              ,applsys.fnd_concurrent_programs p
              ,apps.xxcus_rebate_bu_nm_v       xrbn
         WHERE r.concurrent_program_id = p.concurrent_program_id
           AND p.concurrent_program_name IN
               ('XXCUSRECPT_DEFAULT', 'XXCUSRECPT')
           AND p.application_id = 20003
           AND r.argument1 = p_fperiod
           AND r.argument2 = xrbn.bu_nm
           AND r.phase_code <> 'C'
           AND xrbn.oracle_bu_nm IN
               (SELECT oracle_bu_nm
                  FROM apps.xxcus_rebate_bu_nm_v
                 GROUP BY oracle_bu_nm
                HAVING COUNT(oracle_bu_nm) > 1)
           AND xrbn.oracle_bu_nm IN
               (SELECT oracle_bu_nm
                  FROM apps.xxcus_rebate_bu_nm_v a
                 WHERE a.bu_nm = p_bu_nm);

      EXCEPTION
        WHEN no_data_found THEN
          l_req_id_wait := NULL;
          l_status      := NULL;
      END;

      IF l_req_id_wait IS NOT NULL AND nvl(l_status, 'X') <> 'C'
      THEN
        --dbms_lock.sleep(l_sleep);

        IF fnd_concurrent.wait_for_request(l_req_id_wait
                                          ,v_interval
                                          ,v_max_time
                                          ,v_phase
                                          ,v_status
                                          ,v_dev_phase
                                          ,v_dev_status
                                          ,v_message)
        THEN
          l_sec := 'ReqID=' || l_req_id_wait || ' DPhase ' || v_dev_phase ||
                   ' DStatus ' || v_dev_status || chr(10) || ' MSG - ' ||
                   v_message || ' ReceiptsProcessing for BU= ' ||
                   l_oracle_bu || ' Time = ' || SYSDATE;
          IF v_dev_phase = 'COMPLETE'
          THEN
            l_sec := 'Oracle BU finished submitting next BU request HDS TM Receipt Load.';

            l_req_id := fnd_request.submit_request(l_app_name
                                                  ,l_program
                                                  ,NULL
                                                  ,NULL
                                                  ,FALSE
                                                  ,p_fperiod
                                                  ,p_bu_nm);
            COMMIT;

            IF (l_req_id != 0)
            THEN
              IF fnd_concurrent.wait_for_request(l_req_id
                                                ,v_interval
                                                ,v_max_time
                                                ,v_phase
                                                ,v_status
                                                ,v_dev_phase
                                                ,v_dev_status
                                                ,v_message)
              THEN
                v_error_message := chr(10) || 'ReqID=' || l_req_id ||
                                   ' DPhase ' || v_dev_phase || ' DStatus ' ||
                                   v_dev_status || chr(10) || ' MSG - ' ||
                                   v_message;
                -- Error Returned
                IF v_dev_phase != 'COMPLETE' OR v_dev_status != 'NORMAL'
                THEN
                  l_err_msg := 'An error occured running of the XXCUS_TM_INTERFACE_PKG HDS TM Receipt Load Process Request ID: ' ||
                               l_req_id || '  ' || v_error_message || '.';
                  fnd_file.put_line(fnd_file.log, l_err_msg);
                  fnd_file.put_line(fnd_file.output, l_err_msg);
                  RAISE program_error;

                END IF;
                -- Then Success!
                retcode := 0;
              ELSE
                l_err_msg := 'EBS timed out waiting on XXCUS_TM_INTERFACE_PKG HDS TM Receipt Load Process' ||
                             v_error_message || '.';
                fnd_file.put_line(fnd_file.log, l_err_msg);
                fnd_file.put_line(fnd_file.output, l_err_msg);
                RAISE program_error;
              END IF;

            ELSE
              l_err_msg := 'An error occured when trying to submit the XXCUS_TM_INTERFACE_PKG HDS TM Receipt Load Process';
              fnd_file.put_line(fnd_file.log, l_err_msg);
              fnd_file.put_line(fnd_file.output, l_err_msg);
              RAISE program_error;
            END IF;

          END IF;
        END IF;

      ELSE
        l_sec := 'UC4 call to run concurrent request HDS TM Receipt Load Process.';

        l_req_id := fnd_request.submit_request(l_app_name
                                              ,l_program
                                              ,NULL
                                              ,NULL
                                              ,FALSE
                                              ,p_fperiod
                                              ,p_bu_nm);
        COMMIT;

        IF (l_req_id != 0)
        THEN
          IF fnd_concurrent.wait_for_request(l_req_id
                                            ,v_interval
                                            ,v_max_time
                                            ,v_phase
                                            ,v_status
                                            ,v_dev_phase
                                            ,v_dev_status
                                            ,v_message)
          THEN
            v_error_message := chr(10) || 'ReqID=' || l_req_id ||
                               ' DPhase ' || v_dev_phase || ' DStatus ' ||
                               v_dev_status || chr(10) || ' MSG - ' ||
                               v_message;
            -- Error Returned
            IF v_dev_phase != 'COMPLETE' OR v_dev_status != 'NORMAL'
            THEN
              l_err_msg := 'An error occured running of the XXCUS_TM_INTERFACE_PKG HDS TM Receipt Load Process Request ID: ' ||
                           l_req_id || '  ' || v_error_message || '.';
              fnd_file.put_line(fnd_file.log, l_err_msg);
              fnd_file.put_line(fnd_file.output, l_err_msg);
              RAISE program_error;

            END IF;
            -- Then Success!
            retcode := 0;
          ELSE
            l_err_msg := 'EBS timed out waiting on XXCUS_TM_INTERFACE_PKG HDS TM Receipt Load Process' ||
                         v_error_message || '.';
            fnd_file.put_line(fnd_file.log, l_err_msg);
            fnd_file.put_line(fnd_file.output, l_err_msg);
            RAISE program_error;
          END IF;

        ELSE
          l_err_msg := 'An error occured when trying to submit the XXCUS_TM_INTERFACE_PKG HDS TM Receipt Load Process';
          fnd_file.put_line(fnd_file.log, l_err_msg);
          fnd_file.put_line(fnd_file.output, l_err_msg);
          RAISE program_error;
        END IF;

      END IF;

    ELSE
      -- end 5/2/2013

      l_sec := 'UC4 call to run concurrent request HDS TM Receipt Load Process.';

      l_req_id := fnd_request.submit_request(l_app_name
                                            ,l_program
                                            ,NULL
                                            ,NULL
                                            ,FALSE
                                            ,p_fperiod
                                            ,p_bu_nm);
      COMMIT;

      IF (l_req_id != 0)
      THEN
        IF fnd_concurrent.wait_for_request(l_req_id
                                          ,v_interval
                                          ,v_max_time
                                          ,v_phase
                                          ,v_status
                                          ,v_dev_phase
                                          ,v_dev_status
                                          ,v_message)
        THEN
          v_error_message := chr(10) || 'ReqID=' || l_req_id || ' DPhase ' ||
                             v_dev_phase || ' DStatus ' || v_dev_status ||
                             chr(10) || ' MSG - ' || v_message;
          -- Error Returned
          IF v_dev_phase != 'COMPLETE' OR v_dev_status != 'NORMAL'
          THEN
            l_err_msg := 'An error occured in the running of the XXCUS_TM_INTERFACE_PKG HDS TM Receipt Load Process request ID: ' ||
                         l_req_id || '   ' || v_error_message || '.';
            fnd_file.put_line(fnd_file.log, l_err_msg);
            fnd_file.put_line(fnd_file.output, l_err_msg);
            RAISE program_error;

          END IF;
          -- Then Success!
          retcode := 0;
        ELSE
          l_err_msg := 'EBS timed out waiting on XXCUS_TM_INTERFACE_PKG HDS TM Receipt Load Process' ||
                       v_error_message || '.';
          fnd_file.put_line(fnd_file.log, l_err_msg);
          fnd_file.put_line(fnd_file.output, l_err_msg);
          RAISE program_error;
        END IF;

      ELSE
        l_err_msg := 'An error occured when trying to submit the XXCUS_TM_INTERFACE_PKG HDS TM Receipt Load Process';
        fnd_file.put_line(fnd_file.log, l_err_msg);
        fnd_file.put_line(fnd_file.output, l_err_msg);
        RAISE program_error;
      END IF;

    END IF;

    dbms_output.put_line(l_sec);
    dbms_output.put_line('Request ID:  ' || l_req_id);

  EXCEPTION
    WHEN program_error THEN
      ROLLBACK;
      l_err_code := 2;
      l_err_msg  := l_err_msg || ' Error_Stack...' ||
                    dbms_utility.format_error_stack() ||
                    ' Error_Backtrace...' ||
                    dbms_utility.format_error_backtrace();
      fnd_file.put_line(fnd_file.log, l_err_msg);
      fnd_file.put_line(fnd_file.output, l_err_msg);
      l_err_callpoint := l_sec;

      xxcus_error_pkg.xxcus_error_main_api(p_called_from       => l_err_callfrom
                                          ,p_calling           => l_err_callpoint
                                          ,p_request_id        => l_req_id
                                          ,p_ora_error_msg     => substr(regexp_replace(l_err_msg
                                                                                       ,'[[:cntrl:]]'
                                                                                       ,NULL)
                                                                        ,1
                                                                        ,2000)
                                          ,p_error_desc        => substr(l_err_msg
                                                                        ,1
                                                                        ,240)
                                          ,p_distribution_list => l_distro_list
                                          ,p_module            => 'TM');

      --Version 1.3
      dbms_output.put_line(l_err_code || ' Message code''|' || l_err_code ||
                           '|Exception: ' || l_err_msg);
      RAISE;

    WHEN OTHERS THEN
      fnd_file.put_line(fnd_file.log, l_sec);
      fnd_file.put_line(fnd_file.output, l_sec);
      l_err_code := 2;
      l_err_msg  := l_err_msg || ' Error_Stack...' ||
                    dbms_utility.format_error_stack() ||
                    ' Error_Backtrace...' ||
                    dbms_utility.format_error_backtrace();
      fnd_file.put_line(fnd_file.log, l_err_msg);
      fnd_file.put_line(fnd_file.output, l_err_msg);
      l_err_callpoint := l_sec;

      xxcus_error_pkg.xxcus_error_main_api(p_called_from       => l_err_callfrom
                                          ,p_calling           => l_err_callpoint
                                          ,p_request_id        => l_req_id
                                          ,p_ora_error_msg     => substr(regexp_replace(l_err_msg
                                                                                       ,'[[:cntrl:]]'
                                                                                       ,NULL)
                                                                        ,1
                                                                        ,2000)
                                          ,p_error_desc        => substr(l_err_msg
                                                                        ,1
                                                                        ,240)
                                          ,p_distribution_list => l_distro_list
                                          ,p_module            => 'TM');

      --Version 1.3
      dbms_output.put_line(l_err_code || ' Message code''|' || l_err_code ||
                           '|Exception: ' || l_err_msg);
      RAISE;

  END uc4_receipts;
  --
  -- Begin Ver 3.1
  function get_instance return varchar2 as
   l_db_name varchar2(20) :=Null;
   --
   begin
       --
       select upper(name)
       into    l_db_name
       from  v$database;
       --
       return l_db_name;
       --
   exception
     when others then
        print_log('Error in fetch of instance name, message => '||sqlerrm);
        return 'NA';
   end get_instance;
  --
  function beforereport return boolean as
    d_fiscal_wk_begin date :=Null; --Ver 3.3
    d_fiscal_wk_end date :=Null; --Ver 3.3
  begin
         -- Begin Ver 3.3
         begin
           select min(fiscal_week_start), max(fiscal_week_end)
           into    d_fiscal_wk_begin, d_fiscal_wk_end
           from xxcus.xxcus_rebates_recon_audit_b
           where 1 =1
                and stage_code !='EDW-CT2'
                and email_flag ='U'
                and dw_btch_id =(
                                                    select max(b.dw_btch_id)
                                                    from xxcus.xxcus_rebates_recon_audit_b b
                                                    where 1 =1
                                                        and b.stage_code !='EDW-CT2'
                                                        and b.email_flag ='U'
                                                  );
         exception
          when others then
            print_log('Error in fetch of max and min fiscal week dates, error =>'||sqlerrm);
         end;
         -- End Ver 3.3
        SELECT   DISTINCT to_char(a.fiscal_week_start, 'mm/dd/rr')||' TO '||to_char(a.fiscal_week_end, 'mm/dd/rr')
        INTO xxcus_tm_interface_pkg.g_week_dates
        FROM xxcus.xxcus_rebates_recon_audit_b a
        WHERE 1 = 1
        and a.stage_code !='EDW-CT2'
        and a.email_flag ='U'
        and a.fiscal_week_start >=d_fiscal_wk_begin -- Ver 3.3
        and a.fiscal_week_end <=d_fiscal_wk_end; -- Ver 3.3
   --
   xxcus_tm_interface_pkg.g_audit_report_file :='XXCUS_REBATES_AUDIT_'||fnd_global.conc_request_id||'_1.PDF';
   --
   print_log ('xxcus_tm_interface_pkg.g_week_dates : '||xxcus_tm_interface_pkg.g_week_dates);
   print_log(' ');
   print_log ('xxcus_tm_interface_pkg.g_audit_report_file : '||xxcus_tm_interface_pkg.g_audit_report_file);
   --
   return true;
  exception
   when others then
   --
   print_log ('Error in fetch of xxcus_tm_interface_pkg.g_week_dates : '||sqlerrm);
   --
    return false;
  end beforereport;
  --
  function afterreport return boolean as
   n_loc number;
   v_p1 varchar2(150) :=Null;
   v_p2 varchar2(150) :=Null;
   v_p3 varchar2(150) :=Null;
   ln_request_id number :=0;
  begin
   --
   n_loc :=100;
   --
   savepoint s1;
   --
    select xxcus_tm_interface_pkg.g_audit_email_id
               ,xxcus_tm_interface_pkg.g_week_dates
               ,xxcus_tm_interface_pkg.g_audit_report_file
    into v_p1, v_p2, v_p3
    from dual;
   --
   print_log ('v_p1 : '||v_p1);
   print_log ('v_p2 : '||v_p2);
   print_log ('v_p3 : '||v_p3);
   --
   n_loc :=101;
   --
   update xxcus.xxcus_rebates_recon_audit_b
   set email_flag ='Y'
   where email_flag ='U';
   --
   commit; --Ver 3.3
   --
   print_log ('Total rows updated rows for email distribution : '||sql%rowcount);
   --
   n_loc :=102;
   --
           -- Begin Ver 3.3
           begin
            dbms_lock.sleep(seconds =>l_sleep_seconds);
           end;
           -- End Ver 3.3
   --
   ln_request_id :=fnd_request.submit_request
    (
      application =>'XXCUS'
     ,program =>'XXCUS_REBATES_AUDIT_STATUS'
     ,description =>Null
     ,start_time =>Null
     ,sub_request =>FALSE
     ,argument1 =>v_p1
     ,argument2 =>v_p2
     ,argument3 =>v_p3
    );
   --
   commit;
   --
   n_loc :=103;
   --
   return true;
  exception
   when others then
    print_log('loc => '||n_loc||', msg ='||sqlerrm);
    rollback;
    return false;
  end afterreport;
  --
  PROCEDURE recon_audit_pp
                                    (
                                      p_bu_nm          IN VARCHAR2
                                     ,p_dw_btch_id IN NUMBER
                                     ,p_oracle_bu_nm IN VARCHAR2
                                     ,p_fiscal_wk_start IN DATE
                                     ,p_fiscal_wk_end IN DATE
                                    ) IS
    --
    -- Package Variables
    --
    l_req_id             NUMBER NULL;
    v_phase              VARCHAR2(50);
    v_status             VARCHAR2(50);
    v_dev_status         VARCHAR2(50);
    v_dev_phase          VARCHAR2(50);
    v_message            VARCHAR2(250);
    v_error_message      VARCHAR2(3000);
    v_interval           NUMBER := 30; -- In seconds
    v_max_time           NUMBER := 1500; -- In seconds
    l_err_msg            VARCHAR2(3000);
    l_err_code           NUMBER;
    l_sec                VARCHAR2(255);
    l_statement          VARCHAR2(9000);
    l_program            VARCHAR2(30) DEFAULT 'XXCUSOZF_REBATES_RECPT_EXCPT';
    l_app                VARCHAR2(30) DEFAULT 'XXCUS';
    l_responsibility     NUMBER;
    l_user               VARCHAR2(100);
    l_user_id            NUMBER;
    l_can_submit_request BOOLEAN := TRUE;
    -- Error DEBUG
    l_err_callfrom  VARCHAR2(75) DEFAULT 'XXCUS_TM_INTERFACE_PKG.recon_audit_pp'; --rebates recon audit post processor
    l_err_callpoint VARCHAR2(75) DEFAULT 'START';
    l_distro_list   VARCHAR2(75) DEFAULT 'HDSOracleDevelopers@hdsupply.com';
    --
    l_err_count Number :=0;
    l_err_amount Number :=0;
    --
    l_loaded_count Number :=0;
    l_loaded_amount Number :=0;
    --
   type l_resale_line_type is RECORD
    (
        resale_line_id NUMBER
       ,price NUMBER
       ,qty NUMBER
       ,bu VARCHAR2(150)
       ,resale_receipt# VARCHAR2(150)
    );
    type l_resale_line_tbl is table of l_resale_line_type index by binary_integer;
     l_resale_lines_rec l_resale_line_tbl;
    --

  BEGIN
        --If we had successfully pushed the receipts over to the interface table ozf_resale_lines_int_all
        --then we must expect a status of CLOSED. Check if any record has a status of other than CLOSED.
        begin
          --
                select /*+ parallel (a 2) */
                       sum(nvl(a.selling_price,0) * nvl(a.quantity,0))
                      ,count(a.line_attribute5)
                into  l_err_amount
                        ,l_err_count
                from ozf.ozf_resale_lines_int_all a
                where 1 =1
                     and a.line_attribute2 =p_oracle_bu_nm
                     and a.date_ordered between p_fiscal_wk_start and p_fiscal_wk_end
                     and a.org_id in ('101', '102')
                     and a.status_code <> 'CLOSED'
                     ;
                --
                l_err_amount :=nvl(l_err_amount, 0);
                --
          --
        exception
         when no_data_found then
          l_err_count :=-97.1;
          l_err_amount :=-97.1;
         when others then
          print_log('Error in fetch of failed fiscal week receipt records @ ct5, message ='||sqlerrm);
          l_err_count :=-97.2;
          l_err_amount :=-97.2;
        end;
      --
      begin
       savepoint ct4b;
       update xxcus.xxcus_rebates_recon_audit_b
               set  ct5_total_rows =l_err_count
                     ,ct5_total_amount =l_err_amount
                     ,status_code ='IN PROCESS'
                     ,stage_code ='EBS-CT5'
                     ,last_update_date =sysdate
          where 1 =1
               and bu_name =p_bu_nm
               and dw_btch_id =p_dw_btch_id
               and status_code ='IN PROCESS'
               and stage_code ='EBS-CT4';
            print_log (' ');
            print_log ('CT5 Complete, Total rows :'||l_err_count||', Total Amount :'||l_err_amount);
            print_log (' ');
       commit;
      exception
       when others then
        rollback to ct4b;
        print_log('Error in updating recon audit table status code to EBS-CT5, message :'||sqlerrm);
      end;
      --
      COMMIT;
      --
        begin
          --
            select /*+ parallel (b 4) */
                      sum (nvl (b.selling_price, 0) * nvl (b.quantity, 0))
                     ,count (b.line_attribute5)
            into    l_loaded_amount
                      ,l_loaded_count
            from apps.ozf_resale_lines_all b
            where 1 =1
                 and b.date_ordered between p_fiscal_wk_start and p_fiscal_wk_end
                 and b.line_attribute2 =p_oracle_bu_nm
                 ;
          --
          print_log(' ');
          print_log('@CT6,l_loaded_count = '||l_loaded_count||', l_loaded_amount ='||l_loaded_amount);
          print_log(' ');
          --
              begin
               savepoint ct6b;
               update xxcus.xxcus_rebates_recon_audit_b
                       set  ct6_total_rows =l_loaded_count
                             ,ct6_total_amount =l_loaded_amount
                             ,status_code ='COMPLETE'
                             ,stage_code ='EBS-CT6'
                             ,last_update_date =sysdate
                  where 1 =1
                       and bu_name =p_bu_nm
                       and dw_btch_id =p_dw_btch_id
                       and status_code ='IN PROCESS'
                       and stage_code ='EBS-CT5';
                    print_log (' ');
                    print_log ('CT6 Complete, Total rows :'||l_loaded_count||', Total Amount :'||l_loaded_amount);
                    print_log (' ');
               commit;
              exception
               when others then
                rollback to ct6b;
                print_log('Error in updating recon audit table status code to EBS-CT6, message :'||sqlerrm);
              end;
          --
        exception
         when no_data_found then
          l_loaded_count :=-98.1;
          l_loaded_amount :=-98.1;
         when others then
          print_log('Error in fetch of successfully loaded receipt records @ ct6, message ='||sqlerrm);
          l_loaded_count :=-98.2;
          l_loaded_amount :=-98.2;
        end;
      --
  EXCEPTION
    WHEN program_error THEN
      ROLLBACK;
      l_err_code := 2;
      l_err_msg  := l_err_msg || ' Error_Stack...' ||
                    dbms_utility.format_error_stack() ||
                    ' Error_Backtrace...' ||
                    dbms_utility.format_error_backtrace();
      dbms_output.put_line(l_err_msg);
      fnd_file.put_line(fnd_file.log, l_err_msg);
      fnd_file.put_line(fnd_file.output, l_err_msg);
      l_err_callpoint := l_sec;

      xxcus_error_pkg.xxcus_error_main_api(p_called_from       => l_err_callfrom
                                          ,p_calling           => l_err_callpoint
                                          ,p_request_id        => l_req_id
                                          ,p_ora_error_msg     => substr(regexp_replace(l_err_msg
                                                                                       ,'[[:cntrl:]]'
                                                                                       ,NULL)
                                                                        ,1
                                                                        ,2000)
                                          ,p_error_desc        => substr(l_err_msg
                                                                        ,1
                                                                        ,240)
                                          ,p_distribution_list => l_distro_list
                                          ,p_module            => 'TM');

    WHEN OTHERS THEN
      fnd_file.put_line(fnd_file.log, l_sec);
      fnd_file.put_line(fnd_file.output, l_sec);
      l_err_code := 2;
      l_err_msg  := l_err_msg || ' Error_Stack...' ||
                    dbms_utility.format_error_stack() ||
                    ' Error_Backtrace...' ||
                    dbms_utility.format_error_backtrace();
      dbms_output.put_line(l_err_msg);
      fnd_file.put_line(fnd_file.log, l_err_msg);
      fnd_file.put_line(fnd_file.output, l_err_msg);
      l_err_callpoint := l_sec;

      xxcus_error_pkg.xxcus_error_main_api(p_called_from       => l_err_callfrom
                                          ,p_calling           => l_err_callpoint
                                          ,p_request_id        => l_req_id
                                          ,p_ora_error_msg     => substr(regexp_replace(l_err_msg
                                                                                       ,'[[:cntrl:]]'
                                                                                       ,NULL)
                                                                        ,1
                                                                        ,2000)
                                          ,p_error_desc        => substr(l_err_msg
                                                                        ,1
                                                                        ,240)
                                          ,p_distribution_list => l_distro_list
                                          ,p_module            => 'TM');

  END recon_audit_pp;
  -- End Ver 3.1
  /*******************************************************************************
  * Procedure:   LOAD_REBATE_RECEIPTS
  * Description: This is called from UC4_RECEIPTS.  This is receipts loading from
  *              SDW.  SDW pushes receipt to a staging table that has been partitioned
  *              matching to SDW.  Process pulls receipts for period plus prior
  *              periods as needed for loading by BU.  Changes based on phase 2
  HISTORY
  ===============================================================================
  VERSION DATE          AUTHOR(S)       DESCRIPTION
  ------- -----------   --------------- -----------------------------------------
  1.0     01/11/2013    Kathy Poling    Initial creation of the procedure
                                        RFC 36582
  1.2     04/18/2013    Kathy Poling    Added lookup XXCUS_REBATE_BU_XREF for mapping
                                        SDW BU name and code to hr_business_unit that
                                        is used in the location code table.
                                        SR 200882
  1.3     05/14/2013    Kathy Poling    Added check if all BU finished the FAE will
                                        be submitted
  1.4     08/19/2013    Kathy Poling    Added SDW history tbl that will be used for
                                        recon.  Changed moving data from the receipt tbl
                                        using fiscal period.  Change while looping thru
                                        table xxcus_rebt_cust_tbl to ignore rejected batches
                                        SR 202000
  1.5     09/05/2013    Kathy Poling    Changed call to procedure oneoff_uom_mapping RFC 38031
  1.6     09/12/2013    Kathy Poling    Change to map the the LOB Branch
                                        SR 218282
  1.7     09/17/2013    Kathy Poling    Adding source system when looking for item
                                        RFC 38254
  1.8     09/25/2013    Kathy Poling    Change on date invoiced so it more middle of calendar
                                        month.  SR 223933
                                        Change from version 1.4 removing and making that step
                                        for recon SR 207441
                                        Change for getting branch calling function to fix WW and WC
  1.9     10/10/2013    Kathy Poling    Change for running FAE weekly or on month end week
                                        changed to run FAE only for active volume agreements
  1.10    11/27/2013    Kathy Poling    Added parallel hint for performance.  Change timeout to lookup
                                        RFC 38762
  1.11    12/03/2013    Kathy Poling    Change for month end submitting FAE
                                        RFC 38828
  1.12    02/03/2014    Kathy Poling    Change to getting the post_run_id from audit
                                        SR 236145 RFC 39202
  2.0     07/22/2014    Kathy Poling    SR 257808 RFC 41043 Corrected the adjustment logic for UA
  2.1    09/02/2014    Bala Seshadri   RFC 41456 Add regexp_replace function to oneoff_rebate_receipts routine
  2.2    09/10/2014   Bala Seshadri    RFC 41456 Add regexp_replace function to load_rebate_receipts routine
  2.4    10/20/2014     Kathy Poling    ESMS 531490 RFC 42024 change parm being passed
                                        to lookup so the Oracle business name is passed
                                        to load_uom_mapping
  2.5   10/25/2014   Bala Seshadri      RFC 42038 / ESMS 267539
  2.6    12/29/2014    Kathy Poling    ESMS 244996 adding the ability to disable the
                                       weekly request set from procedure load_rebate_receipts
                                       ESMS 240310 made check for trade profile an outer join
                                       in procedure load_rebate_receipts, oneoff_rebate_receipt
                                       and receipt_exceptions
                                       ESMS 275319 added date check for prior to submitting
                                       the weekly request set rom procedure load_rebate_receipts
                                       RFC 42740
  ********************************************************************************/
  PROCEDURE load_rebate_receipts(errbuf    OUT VARCHAR2
                                ,retcode   OUT NUMBER
                                ,p_fperiod IN VARCHAR2
                                ,p_bu_nm   IN VARCHAR2) IS

    --
    -- Package Variables
    --
    l_fiscal_period_start DATE := NULL;
    n_days                NUMBER := 0;
    --
    --
    l_req_id     NUMBER NULL;
    v_phase      VARCHAR2(50);
    v_status     VARCHAR2(50);
    v_dev_status VARCHAR2(50);
    --
    v_dev_phase VARCHAR2(50);
    v_message   VARCHAR2(250);
    v_interval  NUMBER := 30; -- In seconds
    v_max_time  NUMBER := 150000; -- In seconds
    --
    v_rec_cnt            NUMBER := 0;
    l_can_submit_request BOOLEAN := TRUE;
    l_globalset          VARCHAR2(100);
    l_err_msg            VARCHAR2(3000);
    --
    l_err_code  NUMBER;
    l_sec       VARCHAR2(255);
    l_count     NUMBER := 0;
    l_adj_count NUMBER;
    --
    l_success   BOOLEAN DEFAULT TRUE;
    l_procedure VARCHAR2(50) := 'LOAD_REBATE_RECEIPTS';
    l_receipt   NUMBER;
    l_batch     NUMBER;
    l_high      NUMBER;
    --
    l_low           NUMBER;
    l_months_new    NUMBER;
    l_months_adj    NUMBER;
    l_adjustment_id NUMBER;
    --
    l_months_back  NUMBER;
    l_fae          NUMBER;
    l_batch_create VARCHAR2(50);
    l_batch_num    VARCHAR2(30);
    l_trnsfr_typ   VARCHAR2(2);
    --
    l_trnsfr_mvmnt_typ VARCHAR2(2);
    l_ordr_ctgry       VARCHAR2(30);
    l_lob_attr         VARCHAR2(30) := 'HDS_LOB';
    l_bu_attr          VARCHAR2(30) := 'HDS_BU';
    --
    l_cust_attr     VARCHAR2(30) := 'HDS_LOB_BRANCH';
    l_partition     VARCHAR2(30);
    l_partition_sdw VARCHAR2(30);
    --
    l_sleep        NUMBER;
    l_oracle_bu_nm VARCHAR2(80);
    l_runnum       NUMBER := 0; --Version 1.3
    l_monthend     DATE; --Version 1.9
    --
    l_waittime     NUMBER; --Version 1.10
    l_max_runnum   NUMBER; --Version 1.12
    l_wkly_req_set VARCHAR2(10); --Version 2.6

    l_start_date gl_periods.start_date%TYPE;
    l_new_start_date gl_periods.start_date%TYPE;     -- Ver 3.5
    l_end_date   gl_periods.end_date%TYPE;
    l_calendar CONSTANT gl_periods.period_set_name%TYPE := '4-4-QTR';
    --
    l_org NUMBER;
    l_us_org  CONSTANT hr_all_organization_units.organization_id%TYPE := 101; --HDS Rebates USA - Org
    l_cn_org  CONSTANT hr_all_organization_units.organization_id%TYPE := 102; --HDS Rebates CAN - Org
    l_inv_org CONSTANT hr_all_organization_units.organization_id%TYPE := 84; --HDS Rebates USA - Inv
    --
    l_conc_program CONSTANT fnd_concurrent_programs_tl.concurrent_program_id%TYPE := 44848; --Third Party Accrual From Resale Table
    l_category_set CONSTANT mtl_category_sets_tl.category_set_id%TYPE := 1100000041; --HDS Supplier Category
    l_fiscal_per_id NUMBER;
    l_adj_fiscal_per_id NUMBER; -- Ver 3.5
    --
    l_user_id  NUMBER;
    l_status   NUMBER;
    l_category NUMBER := 1123; --Rebates default item category
    --
    l_user CONSTANT VARCHAR2(100) := 'REBTINTERFACE';
    l_responsibility VARCHAR2(100) := 'XXCUS_CON'; --'HDS Trade Management Administrator - Rebates'

    x_return_status VARCHAR2(1);
    x_msg_count     NUMBER;
    x_msg_data      VARCHAR2(4000);

    -- Error DEBUG
    l_err_callfrom  VARCHAR2(75) DEFAULT 'XXCUS_TM_INTERFACE_PKG.LOAD_REBATE_RECEIPTS';
    l_err_callpoint VARCHAR2(75) DEFAULT 'START';
    l_distro_list   VARCHAR2(75) DEFAULT 'HDSOracleDevelopers@hdsupply.com';

    l_selling_price NUMBER;
    l_prod_segment  VARCHAR2(20);
    l_loc_segment   VARCHAR2(20);
    --
    -- Begin Ver 3.1
    --
    l_fiscal_week_start date;
    l_fiscal_week_end date;
    l_edw_fiscal_per_id Number;
    --
    l_recon_ins_delta number :=0;
    l_dw_btch_id number :=0;
    --
    l_ct3_recon_total_amount number :=0;
    l_ct3_recon_total_rows number :=0;
    --
    l_ct4_recon_rows_P Number :=0;
    l_ct4_recon_amount_P Number :=0;
    l_ct4_recon_rows_N    Number :=0;
    l_ct4_recon_amount_N    Number :=0;
    --
    l_resale_line_int_id Number :=0;
    type l_resale_line_id_tbl is table of apps.ozf_resale_lines_int_all.resale_line_int_id%type index by binary_integer;
    l_resale_line_int_rec    l_resale_line_id_tbl;
    --
    l_x_return_status varchar2(1) :=Null;
    l_x_msg_data varchar2(2000) :=Null;
    l_alter_nls_date varchar2(250) :=Null;
    l_sql varchar2(2000) :=Null;
    l_seq number :=0;
    --
    cursor c_new_purchases (p_oracle_bu_nm in varchar2) is
    select vndr_recpt_unq_nbr
    from  xxcus.xxcus_rbt_new_purchases_stg
    where 1 =1
         and bu_nm =p_oracle_bu_nm;
    --
    type t_new_purchases is table of c_new_purchases%rowtype index by binary_integer;
    t_new_purch_rec t_new_purchases;
    --
    cursor c_receipts is
            select /*+ PARALLEL (a 8) */ a.bu_nm, a.vndr_recpt_unq_nbr
              from xxcus.xxcus_rebate_receipt_tbl a
             where 1 =1
                   and a.bu_nm = l_oracle_bu_nm
                   and a.fiscal_per_id >=to_number(to_char(add_months(sysdate,l_months_new),'YYYY') || '11')
                   and not exists
                    (
                        select /*+ PARALLEL (b 8) */
                                    '1'
                          from xxcus.xxcus_rebate_receipt_curr_tbl b
                         where 1 =1
                               and b.bu_nm = a.bu_nm
                               and b.vndr_recpt_unq_nbr =a.vndr_recpt_unq_nbr
                    )
                   and not exists
                    (
                        select /*+ PARALLEL (c 8) index (c XXCUS_REBATE_RECPT_HIST_TBL_N5) */
                                    '1'
                          from xxcus.xxcus_rebate_recpt_history_tbl c
                         where 1 =1
                               and c.bu_nm = a.bu_nm
                               and c.vndr_recpt_unq_nbr =a.vndr_recpt_unq_nbr
                    );
     --
     TYPE t_receipts is TABLE OF  c_receipts%rowtype INDEX BY BINARY_INTEGER;
     t_recpt_rec t_receipts;
     --
     cursor c_history_receipts is
     SELECT /*+ PARALLEL (r 8) */
                         'UH' action --Need an adjustment
                        ,r.bu_nm
                        ,r.fiscal_per_id
                        ,r.vndr_recpt_unq_nbr
                        ,r.sku_cd
                        ,r.prod_uom_cd
                        ,r.item_cost_amt
                        ,r.recpt_qty
                        ,r.buy_pkg_unt_cnt
                          FROM xxcus.xxcus_rebate_receipt_tbl      r
                              ,xxcus.xxcus_rebate_receipt_curr_tbl cr
                         WHERE r.bu_nm = l_oracle_bu_nm --Version 1.2 p_bu_nm
                           AND r.bu_nm = cr.bu_nm
                           AND r.vndr_recpt_unq_nbr = cr.vndr_recpt_unq_nbr
                           AND (r.item_cost_amt <> cr.item_cost_amt OR
                               r.recpt_qty <> cr.recpt_qty OR
                               r.buy_pkg_unt_cnt <> cr.buy_pkg_unt_cnt OR
                               r.pt_vndr_cd <> cr.pt_vndr_cd OR
                               r.rebt_vndr_cd <> cr.rebt_vndr_cd)
                           AND cr.status_flag = 'P'
                           AND r.recpt_crt_dt >=
                               add_months(trunc(SYSDATE), l_months_adj)
                           AND cr.recpt_crt_dt >=
                               add_months(trunc(SYSDATE), l_months_adj)
                           AND cr.adjusted = 'N' --Version 1.8 10/23/2013  removed null
                        UNION
                        SELECT /*+ PARALLEL (r 8) */
                         'UA' action --Need to rev adjustment
                        ,r.bu_nm
                        ,r.fiscal_per_id
                        ,r.vndr_recpt_unq_nbr
                        ,r.sku_cd
                        ,r.prod_uom_cd
                        ,r.item_cost_amt
                        ,r.recpt_qty
                        ,r.buy_pkg_unt_cnt
                          FROM xxcus.xxcus_rebate_receipt_tbl      r
                              ,xxcus.xxcus_rebate_receipt_curr_tbl cr
                              ,xxcus.xxcus_rebate_adjustment_tbl   ra
                         WHERE r.bu_nm = l_oracle_bu_nm --Version 1.2 p_bu_nm
                           AND r.bu_nm = cr.bu_nm
                           AND r.vndr_recpt_unq_nbr = cr.vndr_recpt_unq_nbr
                           AND r.bu_nm = ra.bu_nm
                           AND r.vndr_recpt_unq_nbr = ra.vndr_recpt_unq_nbr
                           AND ra.adjustment_id =
                               (SELECT MAX(adjustment_id)
                                  FROM xxcus.xxcus_rebate_adjustment_tbl x
                                 WHERE r.bu_nm = ra.bu_nm
                                   AND x.vndr_recpt_unq_nbr =
                                       ra.vndr_recpt_unq_nbr)
                           AND (r.item_cost_amt <> ra.item_cost_amt OR
                               r.recpt_qty <> ra.recpt_qty OR
                               r.buy_pkg_unt_cnt <> ra.buy_pkg_unt_cnt OR
                               r.pt_vndr_cd <> ra.pt_vndr_cd OR --Version 2.0
                               r.rebt_vndr_cd <> ra.rebt_vndr_cd) --Version 2.0
                              --AND cr.status_flag <> 'N' --status should it be used!!!!
                           AND r.recpt_crt_dt >=
                               add_months(trunc(SYSDATE), l_months_adj)
                           AND cr.recpt_crt_dt >=
                               add_months(trunc(SYSDATE), l_months_adj)
                           AND cr.adjusted = 'Y'
                        UNION
                        SELECT /*+ PARALLEL (r 8) */
                         'UC' action --Change receipt
                        ,r.bu_nm
                        ,r.fiscal_per_id
                        ,r.vndr_recpt_unq_nbr
                        ,r.sku_cd
                        ,r.prod_uom_cd
                        ,r.item_cost_amt
                        ,r.recpt_qty
                        ,r.buy_pkg_unt_cnt
                          FROM xxcus.xxcus_rebate_receipt_tbl      r
                              ,xxcus.xxcus_rebate_receipt_curr_tbl cr
                         WHERE r.bu_nm = l_oracle_bu_nm --Version 1.2 p_bu_nm
                           AND r.bu_nm = cr.bu_nm
                           AND r.vndr_recpt_unq_nbr = cr.vndr_recpt_unq_nbr
                           AND (r.item_cost_amt <> cr.item_cost_amt OR
                               r.recpt_qty <> cr.recpt_qty OR
                               r.buy_pkg_unt_cnt <> cr.buy_pkg_unt_cnt OR
                               r.pt_vndr_cd <> cr.pt_vndr_cd OR
                               r.rebt_vndr_cd <> cr.rebt_vndr_cd)
                           AND cr.status_flag = 'N'
                           AND r.recpt_crt_dt >=
                               add_months(trunc(SYSDATE), l_months_adj)
                           AND cr.recpt_crt_dt >=
                               add_months(trunc(SYSDATE), l_months_adj);
     --
     TYPE t_hist_type IS RECORD
      (
         action xxcus.xxcus_rebate_receipt_tbl.update_action%type
        ,bu_nm xxcus.xxcus_rebate_receipt_tbl.bu_nm%type
        ,fiscal_per_id xxcus.xxcus_rebate_receipt_tbl.fiscal_per_id%type
        ,vndr_recpt_unq_nbr xxcus.xxcus_rebate_receipt_tbl.vndr_recpt_unq_nbr%type
        ,sku_cd xxcus.xxcus_rebate_receipt_tbl.sku_cd%type
        ,prod_uom_cd xxcus.xxcus_rebate_receipt_tbl.prod_uom_cd%type
        ,item_cost_amt xxcus.xxcus_rebate_receipt_tbl.item_cost_amt%type
        ,recpt_qty xxcus.xxcus_rebate_receipt_tbl.recpt_qty%type
        ,buy_pkg_unt_cnt xxcus.xxcus_rebate_receipt_tbl.buy_pkg_unt_cnt%type
      );
     TYPE t_hist_receipts is TABLE OF  t_hist_type INDEX BY BINARY_INTEGER;
     t_hist_rec t_hist_receipts;
     --
     -- End Ver 3.1
    --
    --
  BEGIN
    --
    -- Begin Ver 3.1
    select 'alter session set nls_date_format='||''''||'DD-MM-RRRR'||'''' into l_alter_nls_date from dual ;
    --
    print_log( ' ');
    print_log('Set NLS_DATE_FORMAT using =>'||l_alter_nls_date);
    print_log( ' ');
    --
    -- End Ver 3.1
    --
    n_days := get_gl_num_of_days;
    --
    l_sec := 'Fetching number of days to calculate date_invoiced /gl_date';
    --
    IF n_days <= 0
    THEN
      --
      print_log('Please update the number of days value in the profile HDS Rebates: GL Number Of Days.');
      RAISE program_error;
      --
    END IF;
    --
    -- Begin Ver 3.1
    begin
       select nvl(max(dw_btch_id), 0)
       into     l_dw_btch_id
       from   xxcus.xxcus_rebates_recon_audit_b
       where 1 =1
             and bu_name =p_bu_nm
             and status_code ='NEW'
             and stage_code ='EDW-CT2';
        --
        if l_dw_btch_id =0 then
          print_log('@check1...Failed to determine the DW BATCH ID for this week''s run for BU : '||p_bu_nm);
          raise program_error;
        else
             print_log('@check1...DW BATCH ID  : '||l_dw_btch_id);
             print_output('@check1...Failed to determine the DW BATCH ID for this week''s run for BU : '||p_bu_nm);
             begin
                 select trunc(fiscal_week_start), trunc(fiscal_week_end)
                 into    l_fiscal_week_start, l_fiscal_week_end
                 from   xxcus.xxcus_rebates_recon_audit_b
                 where 1 =1
                      and  bu_name =p_bu_nm
                      and  dw_btch_id =l_dw_btch_id;
                                   print_log('@check1...DW l_fiscal_week_start : '||l_fiscal_week_start);
                                                print_log('@check1...DW l_fiscal_week_end : '||l_fiscal_week_end);
             exception
              when others then
               print_log('@check3...Failed to determine the fiscal week start and end for BU : '||p_bu_nm||', DW_BTCH_ID :'||l_dw_btch_id);
               l_fiscal_week_start :=Null;
               l_fiscal_week_end :=Null;
               raise program_error;
             end;
        end if;
        --
    exception
     when others then
      print_log('@check2...Failed to determine the DW BATCH ID for this week''s run for BU : '||p_bu_nm);
      raise program_error;
    end;
    -- End Ver 3.1
    --
    SELECT user_id INTO l_user_id FROM fnd_user WHERE user_name = l_user;

    --  Setup parameters for running FND JOBS!
    l_can_submit_request := xxcus_misc_pkg.set_responsibility(l_user
                                                             ,l_responsibility);
    IF l_can_submit_request
    THEN
      l_sec := 'Global Variables are set User Name:  ' || l_user ||
               ' Resp Name:  ' || l_responsibility;
      fnd_file.put_line(fnd_file.log, l_sec);
      fnd_file.put_line(fnd_file.output, l_sec);
    ELSE
      l_sec := 'Global Variables are not set for the Responsibility of ' ||
               l_responsibility || ' and the User ' || l_user;
      fnd_file.put_line(fnd_file.log, l_sec);
      fnd_file.put_line(fnd_file.output, l_sec);
      RAISE program_error;
    END IF;

    l_sec := 'Starting process';
    fnd_file.put_line(fnd_file.log
                     ,'Start time:  ' ||
                      to_char(SYSDATE, 'DD-MON-YYYY HH24:MI:SS') ||
                      ' for ' || l_sec);
    fnd_file.put_line(fnd_file.output
                     ,'Start time:  ' ||
                      to_char(SYSDATE, 'DD-MON-YYYY HH24:MI:SS') ||
                      ' for ' || l_sec);

    --Version 1.2 04/18/2013
    BEGIN
      l_sec := 'lookup Oracle BU Name xref to SDW BU Name : ' || p_bu_nm;
      fnd_file.put_line(fnd_file.log, l_sec);

      SELECT oracle_bu_nm
        INTO l_oracle_bu_nm
        FROM apps.xxcus_rebate_bu_nm_v
       WHERE bu_nm = p_bu_nm;
       print_log ('p_bu_nm ='||p_bu_nm||',  l_oracle_bu_nm ='||l_oracle_bu_nm); --Ver 3.1
    EXCEPTION
      WHEN no_data_found THEN
        l_err_msg := 'Failed to find Oracle BU Name mapped to SDW BU Name : ' ||
                     p_bu_nm || ' : ' || SQLERRM;
        fnd_file.put_line(fnd_file.log, l_err_msg);
        RAISE program_error;
    END;
    --04/18/2013

    BEGIN

      --lookup for mapping of bu name to the partition that needs truncate prior to loading receipts
      --to the table for comparison
      SELECT tag --lookup_code   Version 1.2 04/22/2013
        INTO l_partition
        FROM fnd_lookup_values_vl
       WHERE lookup_type = 'XXCUS_REBATES_TABLE_PARTITION'
         AND meaning = p_bu_nm
         AND lookup_code NOT LIKE 'SDW.%'
         AND enabled_flag = 'Y'
         AND nvl(end_date_active, SYSDATE) >= SYSDATE;
         print_log('Partition used in Oracle EBS staging [Excluding *SDW* table]  :  ' || l_partition); --Ver 3.1
    EXCEPTION
      WHEN no_data_found THEN
        l_partition := 'OTHER';
        fnd_file.put_line(fnd_file.log
                         ,'lookup partition for sdw:  ' || l_partition);
    END;

    BEGIN

      --lookup for mapping of bu name to the partition that needs truncate prior to loading receipts
      --to the table for comparison
      SELECT meaning
        INTO l_partition_sdw
        FROM fnd_lookup_values_vl
       WHERE lookup_type = 'XXCUS_REBATES_TABLE_PARTITION'
         AND description = p_bu_nm
         AND lookup_code LIKE 'SDW.%'
         AND enabled_flag = 'Y'
         AND nvl(end_date_active, SYSDATE) >= SYSDATE;
         print_log('SDW partition for BU : '||p_bu_nm||' is '||l_partition_sdw); --Ver 3.1
    EXCEPTION
      WHEN no_data_found THEN
        l_partition := 'OTHER';
        fnd_file.put_line(fnd_file.log
                         ,'lookup partition for receipt:  ' ||
                          l_partition_sdw);
    END;

    --Version 2.6 12/29/2014
    BEGIN

      SELECT fnd_profile.value('XXCUS_REBATES_RUN_WEEKLY_REQEST_SET')
        INTO l_wkly_req_set
        FROM dual;
      l_sec := 'XXCUS: Rebates Run Weekly Request Set = ' || l_wkly_req_set;
      fnd_file.put_line(fnd_file.log, l_sec);
    EXCEPTION
      WHEN no_data_found THEN
        l_err_msg := 'Failed to find Profile XXCUS: Rebates Run Weekly Request Set - ' ||
                     SQLERRM;
        fnd_file.put_line(fnd_file.log, l_err_msg);
        RAISE program_error;
    END;
    -- 12/29/2014

    BEGIN
      --lookup for receipt $ to stop receipts from loading to interface that is not < 1.00
      SELECT to_number(meaning)
        INTO l_high
        FROM fnd_lookup_values_vl
       WHERE lookup_type = 'HDS_TM_RECEIPT_PARAMETERS'
         AND lookup_code = 'RECPT_HIGH'
         AND view_application_id = 682 --Trade Management
         AND enabled_flag = 'Y'
         AND nvl(end_date_active, SYSDATE) >= SYSDATE;
    EXCEPTION
      WHEN no_data_found THEN
        l_err_msg := 'Failed to find receipt high value : ' || p_fperiod ||
                     ' : ' || SQLERRM;
        fnd_file.put_line(fnd_file.log, l_err_msg);
        RAISE program_error;
    END;

    --lookup for return receipt $ to stop receipts from loading to interface that is not > -1.00
    BEGIN
      SELECT to_number(meaning)
        INTO l_low
        FROM fnd_lookup_values_vl
       WHERE lookup_type = 'HDS_TM_RECEIPT_PARAMETERS'
         AND lookup_code = 'RECPT_LOW'
         AND view_application_id = 682 --Trade Management
         AND enabled_flag = 'Y'
         AND nvl(end_date_active, SYSDATE) >= SYSDATE;
    EXCEPTION
      WHEN no_data_found THEN
        l_err_msg := 'Failed to find receipt low value : ' || p_fperiod ||
                     ' : ' || SQLERRM;
        fnd_file.put_line(fnd_file.log, l_err_msg);
        RAISE program_error;
    END;

    --lookup for searching the number months back for missing receipts
    BEGIN
      SELECT to_number(meaning)
        INTO l_months_new
        FROM fnd_lookup_values_vl
       WHERE lookup_type = 'HDS_TM_RECEIPT_PARAMETERS'
         AND lookup_code = 'NEW_RCPT_MTHS'
         AND view_application_id = 682 --Trade Management
         AND enabled_flag = 'Y'
         AND nvl(end_date_active, SYSDATE) >= SYSDATE;
    EXCEPTION
      WHEN no_data_found THEN
        l_err_msg := 'Failed to find value for number of months to search new receipts: ' ||
                     p_fperiod || ' : ' || SQLERRM;
        fnd_file.put_line(fnd_file.log, l_err_msg);
        RAISE program_error;
    END;

    --number of months back to look for receipt adjustments
    BEGIN
      SELECT to_number(meaning)
        INTO l_months_adj
        FROM fnd_lookup_values_vl
       WHERE lookup_type = 'HDS_TM_RECEIPT_PARAMETERS'
         AND lookup_code = 'ADJ_RCPT_MTHS'
         AND view_application_id = 682 --Trade Management
         AND enabled_flag = 'Y'
         AND nvl(end_date_active, SYSDATE) >= SYSDATE;
    EXCEPTION
      WHEN no_data_found THEN
        l_err_msg := 'Failed to find value for number of months to search : ' ||
                     p_fperiod || ' : ' || SQLERRM;
        fnd_file.put_line(fnd_file.log, l_err_msg);
        RAISE program_error;
    END;

    --lookup for knowing the starting date for the receipt date creating the batches
    BEGIN
      SELECT to_number(meaning)
        INTO l_months_back
        FROM fnd_lookup_values_vl
       WHERE lookup_type = 'HDS_TM_RECEIPT_PARAMETERS'
         AND lookup_code = 'MONTHS_BACK'
         AND view_application_id = 682 --Trade Management
         AND enabled_flag = 'Y'
         AND nvl(end_date_active, SYSDATE) >= SYSDATE;
    EXCEPTION
      WHEN no_data_found THEN
        l_err_msg := 'Failed to find receipt high value  ' || SQLERRM;
        fnd_file.put_line(fnd_file.log, l_err_msg);
        RAISE program_error;
    END;

    --lookup figure out if this will be a weekly run or monthend week
    --Version 1.9  added 10/10/13
    BEGIN
      SELECT SYSDATE + meaning
        INTO l_monthend
        FROM fnd_lookup_values_vl
       WHERE lookup_type = 'HDS_TM_RECEIPT_PARAMETERS'
         AND lookup_code = 'MONTHEND'
         AND view_application_id = 682 --Trade Management
         AND enabled_flag = 'Y'
         AND nvl(end_date_active, SYSDATE) >= SYSDATE;
    EXCEPTION
      WHEN no_data_found THEN
        l_err_msg := 'Failed to find number of days to check if it is a close week  ' ||
                     SQLERRM;
        fnd_file.put_line(fnd_file.log, l_err_msg);
        RAISE program_error;
    END;

    --lookup for wait time for ddl_lock_timeout
    --Version 1.10
    BEGIN
      SELECT to_number(meaning)
        INTO l_waittime
        FROM fnd_lookup_values_vl
       WHERE lookup_type = 'HDS_TM_RECEIPT_PARAMETERS'
         AND lookup_code = 'WAIT_TIME'
         AND view_application_id = 682 --Trade Management
         AND enabled_flag = 'Y'
         AND nvl(end_date_active, SYSDATE) >= SYSDATE;
    EXCEPTION
      WHEN no_data_found THEN
        l_err_msg := 'Failed to find value for wait time: ' || p_fperiod ||
                     ' : ' || SQLERRM;
        fnd_file.put_line(fnd_file.log, l_err_msg);
        RAISE program_error;
    END;

    --lookup for sleep time waiting for TPA to complete
    --Version 1.10
    BEGIN
      SELECT to_number(meaning)
        INTO l_sleep
        FROM fnd_lookup_values_vl
       WHERE lookup_type = 'HDS_TM_RECEIPT_PARAMETERS'
         AND lookup_code = 'SLEEP_TIME'
         AND view_application_id = 682 --Trade Management
         AND enabled_flag = 'Y'
         AND nvl(end_date_active, SYSDATE) >= SYSDATE;
    EXCEPTION
      WHEN no_data_found THEN
        l_err_msg := 'Failed to find value for sleep time: ' || p_fperiod ||
                     ' : ' || SQLERRM;
        fnd_file.put_line(fnd_file.log, l_err_msg);
        RAISE program_error;
    END;

    --start date to look back 2 full calendar years, in lookup so user can contol
    BEGIN
      SELECT start_date
        INTO l_start_date
        FROM gl.gl_periods
       WHERE period_set_name = l_calendar
         AND period_num <> 13
         AND period_year || lpad(period_num, 2, 0) =
             to_number(to_char(add_months('01-' || p_fperiod
                                         , --SYSDATE,
                                          l_months_back)
                              ,'YYYY') || '11');
    EXCEPTION
      WHEN no_data_found THEN
        l_err_msg := 'Failed on start date: ' || ' : ' || SQLERRM;
        fnd_file.put_line(fnd_file.log, l_err_msg);
        RAISE program_error;
    END;

    BEGIN
      SELECT trunc(end_date)
            ,trunc(start_date)
            ,period_year || lpad(period_num, 2, 0)
        INTO l_end_date, l_fiscal_period_start, l_fiscal_per_id
        FROM gl.gl_periods
       WHERE period_set_name = l_calendar
         AND period_name = p_fperiod
         AND period_num <> 13;
         --
         l_edw_fiscal_per_id :=l_fiscal_per_id; --Ver 3.1
         --
    EXCEPTION
      WHEN no_data_found THEN
        l_err_msg := 'Failed to get start and end date for period : ' ||
                     p_fperiod || ' : ' || SQLERRM;
        fnd_file.put_line(fnd_file.log, l_err_msg);
        RAISE program_error;
    END;
    -- Begin Ver 3.5
    BEGIN
      SELECT period_year|| lpad(period_num, 2, 0) , start_date -- Ver 3.5
        INTO l_adj_fiscal_per_id, l_new_start_date -- Ver 3.5
        from gl_period_statuses
        where 1 =1
             AND application_id =101
             AND ledger_id =2021
             AND adjustment_period_flag ='N'
             AND  add_months(to_char(sysdate,'DD') || p_fperiod , l_months_back)  between start_date and end_date;
         --
         fnd_file.put_line(fnd_file.log, 'New variable [l_adj_fiscal_per_id] used for both history table insert and curr table delete where fiscal per id < '||l_adj_fiscal_per_id);
         print_log('New variable [l_adj_fiscal_per_id] used for both history table insert and curr table delete where fiscal per id < '||l_adj_fiscal_per_id);
         --
    EXCEPTION
      WHEN no_data_found THEN
        l_err_msg := 'Failed to get start and end date for period : ' ||
                     p_fperiod || ' : ' || SQLERRM;
        fnd_file.put_line(fnd_file.log, l_err_msg);
        RAISE program_error;
    END;
    -- End Ver 3.5
    -- Begin Ver 3.1
    print_log (' ');
    print_log ('l_start_date ='||l_start_date);
    print_log (' l_end_date ='||l_end_date);
    print_log ('l_fiscal_period_start ='||l_fiscal_period_start);
    print_log ('l_fiscal_per_id ='||l_fiscal_per_id);
    print_log (' ');
    print_log (' ');
    -- End Ver 3.1
    -- Batch number to track what run receipts were loaded
    BEGIN
      SELECT xxcus.xxcus_rebate_batch_s.nextval || '~' || SYSDATE
        INTO l_batch_create
        FROM dual;
      l_sec := 'Batch Number:  ' || l_batch_create;
      fnd_file.put_line(fnd_file.log, l_sec);
      fnd_file.put_line(fnd_file.output, l_sec);
    EXCEPTION
      WHEN OTHERS THEN
        l_err_msg := 'Failed to get the batch sequence for loading data : ' ||
                     p_fperiod || ' : ' || SQLERRM;
        fnd_file.put_line(fnd_file.log, l_err_msg);
        RAISE program_error;
    END;

    --Version 1.3 determin the run number for the period
    BEGIN
      SELECT (MAX(nvl(post_run_id, 0)) + 1)
        INTO l_runnum
        FROM xxcus.xxcus_rebate_aud_tbl
       WHERE procedure_name = l_procedure
         AND fiscal_period = p_fperiod
         AND bu_nm = p_bu_nm;

      l_sec := 'Audit Run Number:  ' || l_runnum;
      fnd_file.put_line(fnd_file.log, l_sec);
      fnd_file.put_line(fnd_file.output, l_sec);
    EXCEPTION
      WHEN no_data_found THEN
        l_runnum := 1;
        l_sec    := 'Audit Run Number "no data found:  ' || l_runnum;
        fnd_file.put_line(fnd_file.log, l_sec);
        fnd_file.put_line(fnd_file.output, l_sec);
      WHEN OTHERS THEN
        l_runnum := 1;
        l_sec    := 'Audit Run Number "others":  ' || l_runnum;
        fnd_file.put_line(fnd_file.log, l_sec);
        fnd_file.put_line(fnd_file.output, l_sec);
    END;

    --  Check to see if data has been loaded from DW
    SELECT COUNT(*)
      INTO l_count
      FROM xxcus.xxcus_rebate_receipt_sdw_tbl
     WHERE fiscal_per_id = l_fiscal_per_id
       AND bu_nm = p_bu_nm
       ;

    IF l_count < 1
    THEN
      -- Receipt data has not been loaded from DW.
      l_err_msg := 'No Receipt data for ' || p_bu_nm ||
                   ' in the table xxcus_rebate_receipt_sdw_tbl for period ' ||
                   p_fperiod;
      RAISE program_error;
    END IF;

    --added to stop process from receiving the deadlock error
    --ORA-00054: resource busy and acquire with NOWAIT specified or timeout expired
    --Version 1.10
    --EXECUTE IMMEDIATE 'alter session set ddl_lock_timeout=360 ';
    EXECUTE IMMEDIATE 'alter session set ddl_lock_timeout=' || l_waittime;

    --Version 1.8 truncate sdw recon table by partition and will insert lastest data by BU
    BEGIN

      l_sec := 'Truncate partition in xxcus_rebate_recon_sdw_tbl table by BU.';
      fnd_file.put_line(fnd_file.log
                       ,'Start time:  ' ||
                        to_char(SYSDATE, 'DD-MON-YYYY HH24:MI:SS') ||
                        ' for ' || l_sec);
      fnd_file.put_line(fnd_file.output
                       ,'Start time:  ' ||
                        to_char(SYSDATE, 'DD-MON-YYYY HH24:MI:SS') ||
                        ' for ' || l_sec);

      EXECUTE IMMEDIATE 'alter table xxcus.xxcus_rebate_recon_sdw_tbl truncate partition ' ||
                        l_partition_sdw;

      fnd_file.put_line(fnd_file.log
                       ,'End time:  ' ||
                        to_char(SYSDATE, 'DD-MON-YYYY HH24:MI:SS') ||
                        ' for ' || l_sec);
      fnd_file.put_line(fnd_file.output
                       ,'End time:  ' ||
                        to_char(SYSDATE, 'DD-MON-YYYY HH24:MI:SS') ||
                        ' for ' || l_sec);

    EXCEPTION
      WHEN OTHERS THEN
        l_err_msg := 'Failed to truncate partition in xxcus_rebate_recon_sdw_tbl for  ' ||
                     l_partition_sdw || ' - ' || SQLERRM;
        fnd_file.put_line(fnd_file.log, l_err_msg);
        RAISE program_error;
    END;

    IF l_runnum IS NULL
    THEN
      l_runnum := 1;
    END IF;

    l_sec := 'Audit Run Number:  ' || l_runnum;
    fnd_file.put_line(fnd_file.log, l_sec);
    fnd_file.put_line(fnd_file.output, l_sec);

    --insert audit table
    INSERT INTO xxcus.xxcus_rebate_aud_tbl
    VALUES
      (nvl(fnd_global.conc_request_id, 0)
      ,l_procedure
      ,SYSDATE
      ,nvl(fnd_global.user_name, 'NA')
      ,nvl(fnd_global.resp_name, 'NA')
      ,p_fperiod
      ,l_runnum --l_fiscal_per_id      Version 1.3
      ,'Beginning Program - loading receipts'
      ,SYSDATE
      ,NULL
      ,l_batch_create
      ,p_bu_nm);

    COMMIT;
    --Load table to backup data prior to monthly processing

    BEGIN
      INSERT /*+ APPEND */
      INTO xxcus.xxcus_rebate_recpt_history_tbl
        SELECT *
          FROM xxcus.xxcus_rebate_receipt_curr_tbl
         WHERE status_flag = 'P'
           AND bu_nm = l_oracle_bu_nm --Version 1.2    p_bu_nm
           AND fiscal_per_id <l_adj_fiscal_per_id --Ver 3.5
           --AND vndr_recpt_unq_nbr like '%5637172969%' -- Ver 3.5
           /*
               to_number(to_char(add_months('01-' || p_fperiod --SYSDATE   8/21/13
                                           ,l_months_back)
                                ,'YYYY') || '11')
                                */
        -- to_number(to_char(add_months(SYSDATE, l_months_back), 'YYYYMM')) --this will give 12 months of processed data

        ;

      COMMIT;

      l_sec := 'Loaded the prior table data from current for comparison.';
      fnd_file.put_line(fnd_file.log, l_sec);
      fnd_file.put_line(fnd_file.output, l_sec);
    END;

    --Clear contents of table ready to load cust acct for funds accrual
    BEGIN

      l_sec := 'Delete completed records from table xxcus_rebt_cust_tbl.';
      fnd_file.put_line(fnd_file.log, l_sec);
      fnd_file.put_line(fnd_file.output, l_sec);

      dbms_output.put_line(l_sec);

      DELETE FROM xxcus.xxcus_rebt_cust_tbl
       WHERE bu_nm = l_oracle_bu_nm --Version 1.2 p_bu_nm
         AND status_code = 'CLOSED'
         AND request_code = 'C'
         AND (status_flag = 'P' OR nvl(offer_type, 'x') = 'x');

    EXCEPTION
      WHEN OTHERS THEN
        l_err_msg := 'Failed to delete xxcus_rebt_cust_tbl: ' || SQLERRM;
        fnd_file.put_line(fnd_file.log, l_err_msg);
        RAISE program_error;
    END;

    --Move processed data to history table, getting ready to load data from DW
    BEGIN

      l_sec := 'Delete processed from xxcus_rebate_receipt_curr_tbl table after being moved to history.';
      fnd_file.put_line(fnd_file.log, l_sec);
      fnd_file.put_line(fnd_file.output, l_sec);
      --Execute Immediate 'truncate table xxcus.xxcus_rebate_receipt_tbl';
      DELETE FROM xxcus.xxcus_rebate_receipt_curr_tbl
       WHERE status_flag = 'P'
         AND bu_nm = l_oracle_bu_nm --Version 1.2     p_bu_nm
         AND fiscal_per_id <l_adj_fiscal_per_id ; --Ver 3.5
            --Version 1.4   8/27
            --to_number(to_char(add_months(SYSDATE, l_months_back), 'YYYY') || '11')
             --to_number(to_char(add_months('01-' || p_fperiod, l_months_back) --Ver 3.5
                              --,'YYYY') || '11'); --Ver 3.5
      COMMIT;

    EXCEPTION
      WHEN OTHERS THEN
        l_err_msg := 'Failed to delete xxcus_rebate_receipt_curr_tbl: ' ||
                     SQLERRM;
        fnd_file.put_line(fnd_file.log, l_err_msg);
        RAISE program_error;
    END;

    --loading new or updated uom from apex
    l_sec := 'Load UOM xref data from Apex.';
    fnd_file.put_line(fnd_file.log, l_sec);
    fnd_file.put_line(fnd_file.output, l_sec);

    fnd_file.put_line(fnd_file.log
                     ,'Start time:  ' ||
                      to_char(SYSDATE, 'DD-MON-YYYY HH24:MI:SS') ||
                      ' for ' || l_sec);
    fnd_file.put_line(fnd_file.output
                     ,'Start time:  ' ||
                      to_char(SYSDATE, 'DD-MON-YYYY HH24:MI:SS') ||
                      ' for ' || l_sec);

    --load_uom_mapping(p_bu_nm); --Version 1.5  9/5/13
    load_uom_mapping(l_oracle_bu_nm); --Version 2.4 10/20/14

    fnd_file.put_line(fnd_file.log
                     ,'End time:  ' ||
                      to_char(SYSDATE, 'DD-MON-YYYY HH24:MI:SS') ||
                      ' for ' || l_sec);
    fnd_file.put_line(fnd_file.output
                     ,'End time:  ' ||
                      to_char(SYSDATE, 'DD-MON-YYYY HH24:MI:SS') ||
                      ' for ' || l_sec);

    --Clear data from staging to reload data pushed from DW
    BEGIN

      l_sec := 'Tuncate xxcus_rebate_receipt_tbl table by BU.';
      fnd_file.put_line(fnd_file.log
                       ,'Start time:  ' ||
                        to_char(SYSDATE, 'DD-MON-YYYY HH24:MI:SS') ||
                        ' for ' || l_sec);
      fnd_file.put_line(fnd_file.output
                       ,'Start time:  ' ||
                        to_char(SYSDATE, 'DD-MON-YYYY HH24:MI:SS') ||
                        ' for ' || l_sec);

      EXECUTE IMMEDIATE 'alter table xxcus.xxcus_rebate_receipt_tbl truncate partition ' ||
                        l_partition;

      fnd_file.put_line(fnd_file.log
                       ,'End time:  ' ||
                        to_char(SYSDATE, 'DD-MON-YYYY HH24:MI:SS') ||
                        ' for ' || l_sec);
      fnd_file.put_line(fnd_file.output
                       ,'End time:  ' ||
                        to_char(SYSDATE, 'DD-MON-YYYY HH24:MI:SS') ||
                        ' for ' || l_sec);

    EXCEPTION
      WHEN OTHERS THEN
        l_err_msg := 'Failed to delete xxcus_rebate_receipt_tbl for BU ' ||
                     l_oracle_bu_nm || ' - ' || SQLERRM;
        fnd_file.put_line(fnd_file.log, l_err_msg);
        RAISE program_error;
    END;

    BEGIN
      l_sec := 'Load Receipt from SDW staging table; ';
      fnd_file.put_line(fnd_file.log
                       ,'Start time:  ' ||
                        to_char(SYSDATE, 'DD-MON-YYYY HH24:MI:SS') ||
                        ' for ' || l_sec);
      fnd_file.put_line(fnd_file.output
                       ,'Start time:  ' ||
                        to_char(SYSDATE, 'DD-MON-YYYY HH24:MI:SS') ||
                        ' for ' || l_sec);

      INSERT /*+ APPEND */
      INTO xxcus.xxcus_rebate_receipt_tbl
        (SELECT /*+ PARALLEL (t 2) */ -- Ver 3.6 
          t.geo_loc_hier_lob_nm
        ,t.src_sys_nm
        ,flv.description
        ,flv.attribute3
        ,t.fiscal_per_id
        ,t.vndr_recpt_unq_nbr
        ,t.pt_vndr_id
        ,t.pt_vndr_cd
        ,t.sf_vndr_id
        ,t.sf_vndr_cd
        ,t.rebt_vndr_id
        ,t.rebt_vndr_cd
        ,t.rebt_vndr_nm
        ,t.prod_id
        ,t.sku_cd
        ,t.vndr_part_nbr
        ,t.shp_to_brnch_id
        ,t.shp_to_brnch_cd
        ,t.shp_to_brnch_nm
        ,t.po_nbr
        ,t.po_crt_dt
        ,t.recpt_crt_dt
        ,t.drop_shp_flg
        ,t.prod_uom_cd
        ,nvl(t.item_cost_amt, 0)
        ,t.curnc_flg
        ,nvl(t.recpt_qty, 0)
        ,t.vndr_shp_qty
        ,nvl(t.buy_pkg_unt_cnt, 1)
        ,t.as_of_dt
        ,'N'
        ,SYSDATE
      --Begin Ver 3.1
       ,case
         when t.recpt_crt_dt between  l_fiscal_week_start and l_fiscal_week_end then 'Y'
         else 'N'
       end current_week_record
      -- End Ver 3.1
           FROM xxcus.xxcus_rebate_receipt_sdw_tbl t
               ,apps.fnd_lookup_values flv
          WHERE 1 =1 --Ver 3.1
            AND t.bu_nm = p_bu_nm --Ver 3.1
            AND t.fiscal_per_id BETWEEN 201112 AND l_fiscal_per_id --8/27/13
               --fiscal_per_id <= l_fiscal_per_id   --8/27/13
            AND flv.lookup_type = 'XXCUS_REBATE_BU_XREF'
            AND flv.enabled_flag = 'Y'
            AND nvl(flv.end_date_active, SYSDATE) >= SYSDATE
            AND flv.meaning = bu_nm);
      fnd_file.put_line(fnd_file.log,'@ Ver 3.5 - Inserted into xxcus.xxcus_rebate_receipt_tbl, total records : '||sql%rowcount); -- Ver 3.5
      COMMIT;

      fnd_file.put_line(fnd_file.log
                       ,'End time:  ' ||
                        to_char(SYSDATE, 'DD-MON-YYYY HH24:MI:SS') ||
                        ' for ' || l_sec);
      fnd_file.put_line(fnd_file.output
                       ,'End time:  ' ||
                        to_char(SYSDATE, 'DD-MON-YYYY HH24:MI:SS') ||
                        ' for ' || l_sec);
    END;

    --Version 1.8   added insert to sdw recon table (replacing version 1.4)
    BEGIN
      l_sec := 'Load Receipt into SDW recon table; ';
      fnd_file.put_line(fnd_file.log
                       ,'Start time:  ' ||
                        to_char(SYSDATE, 'DD-MON-YYYY HH24:MI:SS') ||
                        ' for ' || l_sec);
      fnd_file.put_line(fnd_file.output
                       ,'Start time:  ' ||
                        to_char(SYSDATE, 'DD-MON-YYYY HH24:MI:SS') ||
                        ' for ' || l_sec);

      INSERT /*+ APPEND */
      INTO xxcus.xxcus_rebate_recon_sdw_tbl
        (SELECT /*+ PARALLEL (t 2) */ -- Ver 3.6
          t.geo_loc_hier_lob_nm
        ,t.src_sys_nm
        ,t.bu_nm
        ,t.bu_cd
        ,t.fiscal_per_id
        ,t.vndr_recpt_unq_nbr
        ,t.pt_vndr_id
        ,t.pt_vndr_cd
        ,t.sf_vndr_id
        ,t.sf_vndr_cd
        ,t.rebt_vndr_id
        ,t.rebt_vndr_cd
        ,t.rebt_vndr_nm
        ,t.prod_id
        ,t.sku_cd
        ,t.vndr_part_nbr
        ,t.part_desc
        ,t.shp_to_brnch_id
        ,t.shp_to_brnch_cd
        ,t.shp_to_brnch_nm
        ,t.po_nbr
        ,t.po_crt_dt
        ,t.recpt_crt_dt
        ,t.drop_shp_flg
        ,t.prod_uom_cd
        ,nvl(t.item_cost_amt, 0)
        ,t.curnc_flg
        ,nvl(t.recpt_qty, 0)
        ,t.vndr_shp_qty
        ,nvl(t.buy_pkg_unt_cnt, 1)
        ,t.as_of_dt
           FROM xxcus.xxcus_rebate_receipt_sdw_tbl t -- Ver 3.6
          WHERE t.bu_nm = p_bu_nm -- Ver 3.6
          );
      --
      fnd_file.put_line(fnd_file.log,'@ Ver 3.5 - Inserted into xxcus.xxcus_rebate_recon_sdw_tbl, total records : '||sql%rowcount); -- Ver 3.5
      --
      COMMIT;
      --
      -- Begin Ver 3.1
      --
      begin
            select
                     count (1)
                    ,sum(
                                        ( nvl( recpt_qty, 0 ) / ( case when buy_pkg_unt_cnt is null then 1 when buy_pkg_unt_cnt = 0 then 1 else buy_pkg_unt_cnt end ) )
                                      * nvl( item_cost_amt, 0 )
                             )
            into l_ct3_recon_total_rows
                   ,l_ct3_recon_total_amount
            from xxcus.xxcus_rebate_receipt_sdw_tbl
            where 1 =1
            and bu_nm =p_bu_nm
            and recpt_crt_dt between l_fiscal_week_start and l_fiscal_week_end;
          --
      exception
       when others then
            l_ct3_recon_total_rows :=-95;
            l_ct3_recon_total_amount :=-95;
            print_log('Error in fetch of ct3 totals from xxcus.xxcus_rebate_receipt_sdw_tbl, message :'||sqlerrm);
      end;
      --
      begin
       savepoint ct3;
       update xxcus.xxcus_rebates_recon_audit_b
               set  ct3_recon_total_rows =l_ct3_recon_total_rows
                     ,ct3_recon_total_amount =l_ct3_recon_total_amount
                     ,ebs_batch_code =l_batch_create
                     ,ebs_run_num =l_runnum
                     ,ebs_fiscal_period =p_fperiod
                     ,status_code ='IN PROCESS'
                     ,stage_code ='EBS-CT3'
                     ,ebs_request_id =fnd_global.conc_request_id
                     ,last_updated_by_name =fnd_global.user_name
                     ,last_update_date =sysdate
                     ,email_flag ='U' --we will set it to U so that the Audit report will pickup using the flag of U and update it finally to Y. The audit report is submitted as the last stage of the weekly request set
          where 1 =1
               and bu_name =p_bu_nm
               and dw_btch_id =l_dw_btch_id
               and status_code ='NEW'
               and stage_code ='EDW-CT2';
        print_log (' ');
        print_log ('CT3 Complete, Total rows :'||l_ct3_recon_total_rows||', Total Amount :'||l_ct3_recon_total_amount);
        print_log (' ');
       commit;
      exception
       when others then
        rollback to ct3;
        print_log('Error in updating recon audit table status code to EBS-CT3, BU ='||p_bu_nm||', l_dw_btch_id ='||l_dw_btch_id||', message :'||sqlerrm);
      end;
      --
      -- End Ver 3.1
      fnd_file.put_line(fnd_file.log
                       ,'End time:  ' ||
                        to_char(SYSDATE, 'DD-MON-YYYY HH24:MI:SS') ||
                        ' for ' || l_sec);
      fnd_file.put_line(fnd_file.output
                       ,'End time:  ' ||
                        to_char(SYSDATE, 'DD-MON-YYYY HH24:MI:SS') ||
                        ' for ' || l_sec);
    END;

    BEGIN
      --Get any new receipt that hasn't been loaded
      l_sec := 'Compare receipts for loading new during month end ' ||
               p_fperiod || '.' || '  Going Back from sysdate ' ||
               l_months_new;
      fnd_file.put_line(fnd_file.log
                       ,'Start time:  ' ||
                        to_char(SYSDATE, 'DD-MON-YYYY HH24:MI:SS') ||
                        ' for ' || l_sec);
      fnd_file.put_line(fnd_file.output
                       ,'Start time:  ' ||
                        to_char(SYSDATE, 'DD-MON-YYYY HH24:MI:SS') ||
                        ' for ' || l_sec);

      -- Checking for new receipts maybe a performance issue
      --
      -- Begin Ver 3.1
      --
      begin
        delete xxcus.xxcus_rbt_new_purchases_stg where bu_nm =l_oracle_bu_nm;
        commit;
      exception
       when others then
         print_log('*********************');
         print_log('Error in deleting temp new receipts from table xxcus.xxcus_rebate_new_receipts for bu_nm :'||l_oracle_bu_nm||', msg ='||sqlerrm);
         print_log('*********************');
      end;
      --
      begin
          open c_receipts;
              loop
               fetch c_receipts bulk collect into t_recpt_rec limit 5000;
               exit when t_recpt_rec.count =0;
               print_log ('New receipt count, t_recpt_rec.count ='||t_recpt_rec.count);
                       if t_recpt_rec.count >0 then
                                    --
                                    begin
                                     forall idx in 1 .. t_recpt_rec.count
                                     insert into xxcus.xxcus_rbt_new_purchases_stg values t_recpt_rec(idx);
                                     commit;
                                    exception
                                     when others then
                                      print_log ('*************************');
                                      print_log (' Error in bulk insert of new purchases into table xxcus.xxcus_rbt_new_purchases_stg, msg ='||sqlerrm);
                                      print_log ('*************************');
                                    end;
                                    --
                       end if;
              end loop;
          close c_receipts;
          --
          --
      exception
       when others then
        print_log ('Issue in processing receipts with new status, msg ='||sqlerrm);
      end;
      --
      begin
          open c_new_purchases (p_oracle_bu_nm =>l_oracle_bu_nm);
              loop
               fetch c_new_purchases bulk collect into t_new_purch_rec limit 500;
               exit when t_new_purch_rec.count =0;
               print_log ('New Purchases : t_new_purch_rec.count ='||t_new_purch_rec.count);
                       if t_new_purch_rec.count >0 then
                                    --
                                    begin
                                       --
                                       if l_oracle_bu_nm ='WHITECAP CONSTRUCTION' then -- begin to check with partition we have to use
                                             --
                                            for idx in 1 .. t_new_purch_rec.count loop
                                              update xxcus.xxcus_rebate_receipt_tbl partition (WCC)
                                                      set update_action = 'NEW'
                                               where 1 =1
                                                     and bu_nm = l_oracle_bu_nm
                                                     and vndr_recpt_unq_nbr = t_new_purch_rec(idx).vndr_recpt_unq_nbr;
                                            end loop;
                                            --
                                       elsif l_oracle_bu_nm ='FACILITIES MAINTENANCE' then
                                             --
                                            for idx in 1 .. t_new_purch_rec.count loop
                                              update xxcus.xxcus_rebate_receipt_tbl partition (FM)
                                                      set update_action = 'NEW'
                                               where 1 =1
                                                     and bu_nm = l_oracle_bu_nm
                                                     and vndr_recpt_unq_nbr = t_new_purch_rec(idx).vndr_recpt_unq_nbr;
                                            end loop;
                                            --
                                       elsif l_oracle_bu_nm ='REPAIR_REMODEL' then
                                             --
                                            for idx in 1 .. t_new_purch_rec.count loop
                                              update xxcus.xxcus_rebate_receipt_tbl partition (RR)
                                                      set update_action = 'NEW'
                                               where 1 =1
                                                     and bu_nm = l_oracle_bu_nm
                                                     and vndr_recpt_unq_nbr = t_new_purch_rec(idx).vndr_recpt_unq_nbr;
                                            end loop;
                                            --
                                       elsif l_oracle_bu_nm ='WATERWORKS' then
                                             --
                                            for idx in 1 .. t_new_purch_rec.count loop
                                              update xxcus.xxcus_rebate_receipt_tbl partition (WW)
                                                      set update_action = 'NEW'
                                               where 1 =1
                                                     and bu_nm = l_oracle_bu_nm
                                                     and vndr_recpt_unq_nbr = t_new_purch_rec(idx).vndr_recpt_unq_nbr;
                                            end loop;
                                            --
                                       elsif l_oracle_bu_nm ='USABLUEBOOK' then
                                             --
                                            for idx in 1 .. t_new_purch_rec.count loop
                                              update xxcus.xxcus_rebate_receipt_tbl partition (USABB)
                                                      set update_action = 'NEW'
                                               where 1 =1
                                                     and bu_nm = l_oracle_bu_nm
                                                     and vndr_recpt_unq_nbr = t_new_purch_rec(idx).vndr_recpt_unq_nbr;
                                            end loop;
                                            --
                                       elsif l_oracle_bu_nm ='ELECTRICAL' then
                                             --
                                            for idx in 1 .. t_new_purch_rec.count loop
                                              update xxcus.xxcus_rebate_receipt_tbl partition (ELE)
                                                      set update_action = 'NEW'
                                               where 1 =1
                                                     and bu_nm = l_oracle_bu_nm
                                                     and vndr_recpt_unq_nbr = t_new_purch_rec(idx).vndr_recpt_unq_nbr;
                                            end loop;
                                            --
                                       elsif l_oracle_bu_nm ='UTILISERV' then
                                             --
                                            for idx in 1 .. t_new_purch_rec.count loop
                                              update xxcus.xxcus_rebate_receipt_tbl partition (UTL_USA)
                                                      set update_action = 'NEW'
                                               where 1 =1
                                                     and bu_nm = l_oracle_bu_nm
                                                     and vndr_recpt_unq_nbr = t_new_purch_rec(idx).vndr_recpt_unq_nbr;
                                            end loop;
                                            --
                                       elsif l_oracle_bu_nm ='INTERIORS' then
                                             --
                                            for idx in 1 .. t_new_purch_rec.count loop
                                              update xxcus.xxcus_rebate_receipt_tbl partition (CTI)
                                                      set update_action = 'NEW'
                                               where 1 =1
                                                     and bu_nm = l_oracle_bu_nm
                                                     and vndr_recpt_unq_nbr = t_new_purch_rec(idx).vndr_recpt_unq_nbr;
                                            end loop;
                                            --
                                       elsif l_oracle_bu_nm ='WHITECAP' then
                                             --
                                            for idx in 1 .. t_new_purch_rec.count loop
                                              update xxcus.xxcus_rebate_receipt_tbl partition (WHCP)
                                                      set update_action = 'NEW'
                                               where 1 =1
                                                     and bu_nm = l_oracle_bu_nm
                                                     and vndr_recpt_unq_nbr = t_new_purch_rec(idx).vndr_recpt_unq_nbr;
                                            end loop;
                                            --
                                       else --  All other BU's like Fasteners and Tools, FM Canada etc.,
                                            for idx in 1 .. t_new_purch_rec.count loop
                                              update xxcus.xxcus_rebate_receipt_tbl partition (OTHER)
                                                      set update_action = 'NEW'
                                               where 1 =1
                                                     and bu_nm = l_oracle_bu_nm
                                                     and vndr_recpt_unq_nbr = t_new_purch_rec(idx).vndr_recpt_unq_nbr;
                                            end loop;
                                        --
                                       end if; -- end check with partition we have to use
                                        --
                                      commit;
                                      --
                                    exception
                                     when others then
                                      print_log ('Error in FORALL updating receipts to new status: msg ='||sqlerrm);
                                    end;
                       end if;
              end loop;
          close c_new_purchases;
          --
          commit;
          --
      exception
       when others then
        print_log ('Issue in processing receipts with new status, msg ='||sqlerrm);
      end;
      --
      -- End Ver 3.1
      --
    END;

    BEGIN

      l_sec := 'Checking receipts for changes ' || p_fperiod || '.';
      fnd_file.put_line(fnd_file.log
                       ,'Start time:  ' ||
                        to_char(SYSDATE, 'DD-MON-YYYY HH24:MI:SS') ||
                        ' for ' || l_sec);
      fnd_file.put_line(fnd_file.output
                       ,'Start time:  ' ||
                        to_char(SYSDATE, 'DD-MON-YYYY HH24:MI:SS') ||
                        ' for ' || l_sec);

      dbms_output.put_line('Start time:  ' ||
                           to_char(SYSDATE, 'DD-MON-YYYY HH24:MI:SS') ||
                           ' for ' || l_sec);

      UPDATE xxcus.xxcus_rebate_aud_tbl
         SET stage = l_sec, stg_date = SYSDATE
       WHERE batch_created = l_batch_create
         AND procedure_name = l_procedure
         AND fiscal_period = p_fperiod
         AND post_run_id = l_runnum
         AND bu_nm = p_bu_nm;

      COMMIT;

      --UH = receipt that has been processed need to make adjustment
      --UC = receipt that has changed but still pending in table
      --UA = receipt has been adjusted need reverse adjustment and create new adjustment
      --Checking for 6 months on receipt date for adjustments
      --
      -- Begin Ver 3.1
      --
      begin
          open c_history_receipts;
              loop
               fetch c_history_receipts bulk collect into t_hist_rec limit 5000;
               exit when t_hist_rec.count =0;
               print_log ('History receipt count, t_hist_rec.count ='||t_hist_rec.count);
                       if t_hist_rec.count >0 then
                                    begin
                                     --
                                       if l_oracle_bu_nm ='WHITECAP CONSTRUCTION' then -- begin to check with partition we have to use
                                            --
                                              for idx in 1 .. t_hist_rec.count loop
                                                  update xxcus.xxcus_rebate_receipt_tbl partition (WCC)
                                                          set update_action = t_hist_rec(idx).action
                                                   where 1 =1
                                                         and bu_nm = t_hist_rec(idx).bu_nm
                                                         and vndr_recpt_unq_nbr = t_hist_rec(idx).vndr_recpt_unq_nbr
                                                         and fiscal_per_id = t_hist_rec(idx).fiscal_per_id;
                                              end loop;
                                            --
                                       elsif l_oracle_bu_nm ='FACILITIES MAINTENANCE' then
                                            --
                                              for idx in 1 .. t_hist_rec.count loop
                                                  update xxcus.xxcus_rebate_receipt_tbl partition (FM)
                                                          set update_action = t_hist_rec(idx).action
                                                   where 1 =1
                                                         and bu_nm = t_hist_rec(idx).bu_nm
                                                         and vndr_recpt_unq_nbr = t_hist_rec(idx).vndr_recpt_unq_nbr
                                                         and fiscal_per_id = t_hist_rec(idx).fiscal_per_id;
                                              end loop;
                                            --
                                       elsif l_oracle_bu_nm ='REPAIR_REMODEL' then
                                            --
                                              for idx in 1 .. t_hist_rec.count loop
                                                  update xxcus.xxcus_rebate_receipt_tbl partition (RR)
                                                          set update_action = t_hist_rec(idx).action
                                                   where 1 =1
                                                         and bu_nm = t_hist_rec(idx).bu_nm
                                                         and vndr_recpt_unq_nbr = t_hist_rec(idx).vndr_recpt_unq_nbr
                                                         and fiscal_per_id = t_hist_rec(idx).fiscal_per_id;
                                              end loop;
                                            --
                                       elsif l_oracle_bu_nm ='WATERWORKS' then
                                            --
                                              for idx in 1 .. t_hist_rec.count loop
                                                  update xxcus.xxcus_rebate_receipt_tbl partition (WW)
                                                          set update_action = t_hist_rec(idx).action
                                                   where 1 =1
                                                         and bu_nm = t_hist_rec(idx).bu_nm
                                                         and vndr_recpt_unq_nbr = t_hist_rec(idx).vndr_recpt_unq_nbr
                                                         and fiscal_per_id = t_hist_rec(idx).fiscal_per_id;
                                              end loop;
                                            --
                                       elsif l_oracle_bu_nm ='USABLUEBOOK' then
                                            --
                                              for idx in 1 .. t_hist_rec.count loop
                                                  update xxcus.xxcus_rebate_receipt_tbl partition (USABB)
                                                          set update_action = t_hist_rec(idx).action
                                                   where 1 =1
                                                         and bu_nm = t_hist_rec(idx).bu_nm
                                                         and vndr_recpt_unq_nbr = t_hist_rec(idx).vndr_recpt_unq_nbr
                                                         and fiscal_per_id = t_hist_rec(idx).fiscal_per_id;
                                              end loop;
                                            --
                                       elsif l_oracle_bu_nm ='ELECTRICAL' then
                                            --
                                              for idx in 1 .. t_hist_rec.count loop
                                                  update xxcus.xxcus_rebate_receipt_tbl partition (ELE)
                                                          set update_action = t_hist_rec(idx).action
                                                   where 1 =1
                                                         and bu_nm = t_hist_rec(idx).bu_nm
                                                         and vndr_recpt_unq_nbr = t_hist_rec(idx).vndr_recpt_unq_nbr
                                                         and fiscal_per_id = t_hist_rec(idx).fiscal_per_id;
                                              end loop;
                                            --
                                       elsif l_oracle_bu_nm ='UTILISERV' then
                                            --
                                              for idx in 1 .. t_hist_rec.count loop
                                                  update xxcus.xxcus_rebate_receipt_tbl partition (UTL_USA)
                                                          set update_action = t_hist_rec(idx).action
                                                   where 1 =1
                                                         and bu_nm = t_hist_rec(idx).bu_nm
                                                         and vndr_recpt_unq_nbr = t_hist_rec(idx).vndr_recpt_unq_nbr
                                                         and fiscal_per_id = t_hist_rec(idx).fiscal_per_id;
                                              end loop;
                                            --
                                       elsif l_oracle_bu_nm ='INTERIORS' then
                                            --
                                              for idx in 1 .. t_hist_rec.count loop
                                                  update xxcus.xxcus_rebate_receipt_tbl partition (CTI)
                                                          set update_action = t_hist_rec(idx).action
                                                   where 1 =1
                                                         and bu_nm = t_hist_rec(idx).bu_nm
                                                         and vndr_recpt_unq_nbr = t_hist_rec(idx).vndr_recpt_unq_nbr
                                                         and fiscal_per_id = t_hist_rec(idx).fiscal_per_id;
                                              end loop;
                                            --
                                       elsif l_oracle_bu_nm ='WHITECAP' then
                                            --
                                              for idx in 1 .. t_hist_rec.count loop
                                                  update xxcus.xxcus_rebate_receipt_tbl partition (WHCP)
                                                          set update_action = t_hist_rec(idx).action
                                                   where 1 =1
                                                         and bu_nm = t_hist_rec(idx).bu_nm
                                                         and vndr_recpt_unq_nbr = t_hist_rec(idx).vndr_recpt_unq_nbr
                                                         and fiscal_per_id = t_hist_rec(idx).fiscal_per_id;
                                              end loop;
                                            --
                                       else --  All other BU's like Fasteners and Tools, FM Canada etc.,
                                            --
                                              for idx in 1 .. t_hist_rec.count loop
                                                  update xxcus.xxcus_rebate_receipt_tbl partition (OTHER)
                                                          set update_action = t_hist_rec(idx).action
                                                   where 1 =1
                                                         and bu_nm = t_hist_rec(idx).bu_nm
                                                         and vndr_recpt_unq_nbr = t_hist_rec(idx).vndr_recpt_unq_nbr
                                                         and fiscal_per_id = t_hist_rec(idx).fiscal_per_id;
                                              end loop;
                                            --
                                       end if; -- end check with partition we have to use
                                     --
                                     commit;
                                    exception
                                     when others then
                                      print_log ('Error in updating historical receipts to UD/UC/UA status, message ='||sqlerrm);
                                    end;
                       end if;
              end loop;
          close c_history_receipts;
          --
          commit;
          --
      exception
       when others then
        print_log ('Issue in processing receipts with new status, msg ='||sqlerrm);
      end;
      --
      Null;
      -- End Ver 3.1
      fnd_file.put_line(fnd_file.log
                       ,'End time:  ' ||
                        to_char(SYSDATE, 'DD-MON-YYYY HH24:MI:SS') ||
                        ' for ' || l_sec);
      fnd_file.put_line(fnd_file.output
                       ,'End time:  ' ||
                        to_char(SYSDATE, 'DD-MON-YYYY HH24:MI:SS') ||
                        ' for ' || l_sec);
    END;

    BEGIN

      BEGIN
        l_adj_count := NULL;

        SELECT COUNT(*)
          INTO l_adj_count
          FROM xxcus.xxcus_rebate_receipt_tbl
         WHERE bu_nm = l_oracle_bu_nm --Version 1.2 p_bu_nm
           AND update_action = 'UC'
           ;
          --
          fnd_file.put_line(fnd_file.log,'@ Ver 3.5 - UC count in xxcus.xxcus_rebate_receipt_tbl,   l_adj_count: '||l_adj_count); -- Ver 3.5
          --
      EXCEPTION
        WHEN no_data_found THEN
          l_adj_count := 0;
        WHEN OTHERS THEN
          l_adj_count := 0;

      END;

      l_sec := 'Number of adjusted receipts not processed need to be changed: ' ||
               l_adj_count || '.';
      fnd_file.put_line(fnd_file.log
                       ,'Start time:  ' ||
                        to_char(SYSDATE, 'DD-MON-YYYY HH24:MI:SS') ||
                        ' for ' || l_sec);
      fnd_file.put_line(fnd_file.output
                       ,'Start time:  ' ||
                        to_char(SYSDATE, 'DD-MON-YYYY HH24:MI:SS') ||
                        ' for ' || l_sec);

      dbms_output.put_line('Start time:  ' ||
                           to_char(SYSDATE, 'DD-MON-YYYY HH24:MI:SS') ||
                           ' for ' || l_sec);

      UPDATE xxcus.xxcus_rebate_aud_tbl
         SET stage = l_sec, stg_date = SYSDATE
       WHERE batch_created = l_batch_create
         AND procedure_name = l_procedure
         AND fiscal_period = p_fperiod
         AND post_run_id = l_runnum
         AND bu_nm = p_bu_nm;

      COMMIT;

      IF l_adj_count > 0
      THEN

        --receipt in xxcus_rebate_receipt_curr_tbl has not processed and has been
        --changed need to update data in the current table for processing
        FOR c_update IN (SELECT /*+ PARALLEL (r 2) PARALLEL (cr 4) */ -- Ver 3.6 
                          r.geo_loc_hier_lob_nm
                         ,r.src_sys_nm
                         ,r.bu_nm
                         ,r.vndr_recpt_unq_nbr
                         ,r.sku_cd
                         ,r.recpt_crt_dt
                         ,r.item_cost_amt
                         ,r.recpt_qty
                         ,r.buy_pkg_unt_cnt
                         ,r.rebt_vndr_cd
                         ,r.rebt_vndr_nm
                         ,r.pt_vndr_cd
                         ,cr.geo_loc_hier_lob_nm old_lob
                         ,cr.src_sys_nm          old_src_sys
                         ,cr.bu_nm               old_bu_nm
                         ,cr.vndr_recpt_unq_nbr  old_recpt_nbr
                         ,cr.pt_vndr_cd          old_vndr_cd
                         ,cr.rebt_vndr_cd        old_rebt_cd
                         ,cr.shp_to_brnch_cd     old_brnch
                         ,cr.sku_cd              old_sku
                         ,cr.po_nbr              old_po
                         ,cr.po_crt_dt           old_po_dt
                         ,cr.recpt_crt_dt        old_recpt_dt
                         ,cr.prod_uom_cd         old_uom
                         ,cr.curnc_flg           old_curnc
                         ,cr.item_cost_amt       old_cost
                         ,cr.recpt_qty           old_qty
                         ,cr.buy_pkg_unt_cnt     old_pkg_cnt
                         ,cr.pt_vndr_cd          old_pt_vndr_cd
                         ,cr.rebt_vndr_nm        old_rebt_vndr_nm
                         ,cr.as_of_dt
                           FROM xxcus.xxcus_rebate_receipt_tbl      r
                               ,xxcus.xxcus_rebate_receipt_curr_tbl cr
                          WHERE r.bu_nm = l_oracle_bu_nm --Version 1.2 p_bu_nm
                            AND r.bu_nm = cr.bu_nm
                            AND r.vndr_recpt_unq_nbr = cr.vndr_recpt_unq_nbr
                            AND r.update_action = 'UC'
                            AND cr.status_flag = 'N')
        LOOP

          UPDATE xxcus.xxcus_rebate_receipt_curr_tbl
             SET item_cost_amt    = c_update.item_cost_amt
                ,recpt_qty        = c_update.recpt_qty
                ,buy_pkg_unt_cnt  = c_update.buy_pkg_unt_cnt
                ,pt_vndr_cd       = c_update.pt_vndr_cd
                ,rebt_vndr_cd     = c_update.rebt_vndr_cd
                ,rebt_vndr_nm     = c_update.rebt_vndr_nm
                ,receipt          = NULL
                ,cust_account_id  = NULL
                ,party_id         = NULL
                ,party_name       = NULL
                ,last_update_date = SYSDATE
           WHERE bu_nm = c_update.bu_nm
             AND vndr_recpt_unq_nbr = c_update.vndr_recpt_unq_nbr
             AND status_flag = 'N';
        END LOOP;
        COMMIT;
      END IF;

      fnd_file.put_line(fnd_file.log
                       ,'End time:  ' ||
                        to_char(SYSDATE, 'DD-MON-YYYY HH24:MI:SS') ||
                        ' for ' || l_sec);
      fnd_file.put_line(fnd_file.output
                       ,'End time:  ' ||
                        to_char(SYSDATE, 'DD-MON-YYYY HH24:MI:SS') ||
                        ' for ' || l_sec);
    END;

    BEGIN

      BEGIN
        l_count := NULL;

        SELECT COUNT(*)
          INTO l_count
          FROM xxcus.xxcus_rebate_receipt_tbl
         WHERE bu_nm = l_oracle_bu_nm --Version 1.2 p_bu_nm
           AND update_action = 'NEW'
           ;
          --
          fnd_file.put_line(fnd_file.log,'@ Ver 3.5 -  xxcus.xxcus_rebate_receipt_tbl,   NEW  l_count : '||l_count); -- Ver 3.5
          --
      EXCEPTION
        WHEN no_data_found THEN
          l_count := 0;
        WHEN OTHERS THEN
          l_count := 0;

      END;

      l_sec := 'Load Receipt to process; ';
      fnd_file.put_line(fnd_file.log
                       ,'Start time:  ' ||
                        to_char(SYSDATE, 'DD-MON-YYYY HH24:MI:SS') ||
                        ' for ' || l_sec);
      fnd_file.put_line(fnd_file.output
                       ,'Start time:  ' ||
                        to_char(SYSDATE, 'DD-MON-YYYY HH24:MI:SS') ||
                        ' for ' || l_sec);

      UPDATE xxcus.xxcus_rebate_aud_tbl
         SET stage = l_sec, stg_date = SYSDATE
       WHERE batch_created = l_batch_create
         AND procedure_name = l_procedure
         AND fiscal_period = p_fperiod
         AND post_run_id = l_runnum
         AND bu_nm = p_bu_nm;

      IF l_count > 0
      THEN

        --loading new receipts to process
        INSERT /*+ APPEND */
        INTO xxcus.xxcus_rebate_receipt_curr_tbl
          (SELECT /*+ PARALLEL (a 2) */ -- Ver 3.6
            a.geo_loc_hier_lob_nm
          ,a.src_sys_nm
          ,a.bu_nm
          ,a.bu_cd
          ,a.fiscal_per_id
          ,a.vndr_recpt_unq_nbr
          ,a.pt_vndr_id
          ,a.pt_vndr_cd
          ,a.sf_vndr_id
          ,a.sf_vndr_cd
          ,a.rebt_vndr_id
          ,a.rebt_vndr_cd
          ,a.rebt_vndr_nm
          ,a.prod_id
          ,a.sku_cd
          ,a.vndr_part_nbr
          ,a.shp_to_brnch_id
          ,a.shp_to_brnch_cd
          ,a.shp_to_brnch_nm
          ,a.po_nbr
          ,a.po_crt_dt
          ,a.recpt_crt_dt
          ,a.drop_shp_flg
          ,a.prod_uom_cd
          ,a.item_cost_amt
          ,a.curnc_flg
          ,a.recpt_qty
          ,a.vndr_shp_qty
          ,a.buy_pkg_unt_cnt
          ,a.as_of_dt
          ,NULL
          ,'N'
          ,NULL
          ,NULL
          ,NULL
          ,NULL
          ,NULL
          ,SYSDATE
          ,'N' --adjusted status
          ,l_batch_create
          ,NULL
             FROM xxcus.xxcus_rebate_receipt_tbl a
            WHERE a.bu_nm = l_oracle_bu_nm --Version 1.2 p_bu_nm
              AND a.update_action = 'NEW'
              );
          --
          fnd_file.put_line(fnd_file.log,'@ Ver 3.5 -  insert into xxcus.xxcus_rebate_receipt_curr_tbl,   NEW  total records : '||sql%rowcount); -- Ver 3.5
          --
        COMMIT;

      END IF;
    END;
    --end of loading new receipts into xxcus_rebate_receipt_curr_tbl
    fnd_file.put_line(fnd_file.log
                     ,'End time:  ' ||
                      to_char(SYSDATE, 'DD-MON-YYYY HH24:MI:SS') ||
                      ' for ' || l_sec);
    fnd_file.put_line(fnd_file.output
                     ,'End time:  ' ||
                      to_char(SYSDATE, 'DD-MON-YYYY HH24:MI:SS') ||
                      ' for ' || l_sec);

    --starting first time adjustments (new process Sep 2012)
    BEGIN

      BEGIN
        l_adj_count := NULL;

        SELECT COUNT(*)
          INTO l_adj_count
          FROM xxcus.xxcus_rebate_receipt_tbl
         WHERE bu_nm = l_oracle_bu_nm --Version 1.2 p_bu_nm
           AND update_action = 'UH'
           ;
          --
          fnd_file.put_line(fnd_file.log,'@ Ver 3.5 -  UH count from insert into xxcus.xxcus_rebate_receipt_tbl,  l_adj_count  : '||l_adj_count); -- Ver 3.5
          --
      EXCEPTION
        WHEN no_data_found THEN
          l_adj_count := 0;
        WHEN OTHERS THEN
          l_adj_count := 0;

      END;

      l_sec := 'Starting first time adjustments - Record Count = ' ||
               l_adj_count || '.';
      fnd_file.put_line(fnd_file.log
                       ,'Start time:  ' ||
                        to_char(SYSDATE, 'DD-MON-YYYY HH24:MI:SS') ||
                        ' for ' || l_sec);
      fnd_file.put_line(fnd_file.output
                       ,'Start time:  ' ||
                        to_char(SYSDATE, 'DD-MON-YYYY HH24:MI:SS') ||
                        ' for ' || l_sec);

      UPDATE xxcus.xxcus_rebate_aud_tbl
         SET stage = l_sec, stg_date = SYSDATE
       WHERE batch_created = l_batch_create
         AND procedure_name = l_procedure
         AND fiscal_period = p_fperiod
         AND post_run_id = l_runnum
         AND bu_nm = p_bu_nm;

      COMMIT;

      --First time receipt has an adjustment (new process Sep 2012)
      IF l_adj_count > 0
      THEN

        FOR c_adj IN (
                 SELECT /*+ PARALLEL (r 2) PARALLEL (hr 4) */ -- Ver 3.6
                       r.geo_loc_hier_lob_nm
                      ,r.src_sys_nm
                      ,r.bu_nm
                      ,r.bu_cd
                      ,r.fiscal_per_id
                      ,r.vndr_recpt_unq_nbr
                      ,r.pt_vndr_id
                      ,r.pt_vndr_cd
                      ,r.sf_vndr_id
                      ,r.sf_vndr_cd
                      ,r.rebt_vndr_id
                      ,r.rebt_vndr_cd
                      ,r.rebt_vndr_nm
                      ,r.prod_id
                      ,r.sku_cd
                      ,r.vndr_part_nbr
                      ,r.shp_to_brnch_id
                      ,r.shp_to_brnch_cd
                      ,r.shp_to_brnch_nm
                      ,r.po_nbr
                      ,r.po_crt_dt
                      ,r.recpt_crt_dt
                      ,r.drop_shp_flg
                      ,r.prod_uom_cd
                      ,r.item_cost_amt
                      ,r.curnc_flg
                      ,r.recpt_qty
                      ,r.vndr_shp_qty
                      ,r.buy_pkg_unt_cnt
                      ,r.as_of_dt
                      ,hr.geo_loc_hier_lob_nm old_lob
                      ,hr.src_sys_nm          old_src_sys
                      ,hr.bu_nm               old_bu_nm
                      ,hr.bu_cd               old_bu_cd
                      ,hr.fiscal_per_id       old_fiscal_per_id
                      ,hr.vndr_recpt_unq_nbr  old_recpt_nbr
                      ,hr.pt_vndr_cd          old_pt_vndr_cd
                      ,hr.sf_vndr_id          old_sf_vndr_id
                      ,hr.sf_vndr_cd          old_sf_vndr_cd
                      ,hr.rebt_vndr_cd        old_rebt_cd
                      ,hr.rebt_vndr_nm        old_rebt_nm
                      ,hr.shp_to_brnch_cd     old_brnch
                      ,hr.prod_id             old_prod_id
                      ,hr.sku_cd              old_sku
                      ,hr.po_nbr              old_po
                      ,hr.po_crt_dt           old_po_dt
                      ,hr.recpt_crt_dt        old_recpt_dt
                      ,hr.prod_uom_cd         old_uom
                      ,hr.curnc_flg           old_curnc
                      ,hr.item_cost_amt       old_cost
                      ,hr.recpt_qty           old_qty
                      ,hr.buy_pkg_unt_cnt     old_pkg_cnt
                        FROM xxcus.xxcus_rebate_receipt_curr_tbl hr
                            ,xxcus.xxcus_rebate_receipt_tbl      r
                       WHERE r.bu_nm = l_oracle_bu_nm --Version 1.2 p_bu_nm
                         AND r.bu_nm = hr.bu_nm
                         AND r.vndr_recpt_unq_nbr = hr.vndr_recpt_unq_nbr
                         AND r.update_action = 'UH'
                         AND hr.adjusted = 'N'
                         )
        LOOP

          SELECT xxcus.xxcus_rebate_adjustment_s.nextval
            INTO l_adjustment_id
            FROM dual;

          --creating adjustment receipt
          INSERT INTO xxcus.xxcus_rebate_receipt_curr_tbl
            (geo_loc_hier_lob_nm
            ,src_sys_nm
            ,bu_nm
            ,bu_cd
            ,fiscal_per_id
            ,vndr_recpt_unq_nbr
            ,pt_vndr_id
            ,pt_vndr_cd
            ,sf_vndr_id
            ,sf_vndr_cd
            ,rebt_vndr_id
            ,rebt_vndr_cd
            ,rebt_vndr_nm
            ,prod_id
            ,sku_cd
            ,vndr_part_nbr
            ,shp_to_brnch_id
            ,shp_to_brnch_cd
            ,shp_to_brnch_nm
            ,po_nbr
            ,po_crt_dt
            ,recpt_crt_dt
            ,drop_shp_flg
            ,prod_uom_cd
            ,item_cost_amt
            ,curnc_flg
            ,recpt_qty
            ,vndr_shp_qty
            ,buy_pkg_unt_cnt
            ,as_of_dt
            ,last_update_date
            ,status_flag
            ,adjusted
            ,batch_created)
          VALUES
            (c_adj.geo_loc_hier_lob_nm
            ,c_adj.src_sys_nm
            ,c_adj.bu_nm
            ,c_adj.bu_cd
            ,c_adj.fiscal_per_id
            ,c_adj.vndr_recpt_unq_nbr || '~ADJ~' || l_adjustment_id
            ,c_adj.pt_vndr_id
            ,c_adj.pt_vndr_cd
            ,c_adj.sf_vndr_id
            ,c_adj.sf_vndr_cd
            ,c_adj.rebt_vndr_id
            ,c_adj.rebt_vndr_cd
            ,c_adj.rebt_vndr_nm
            ,c_adj.prod_id
            ,c_adj.sku_cd
            ,c_adj.vndr_part_nbr
            ,c_adj.shp_to_brnch_id
            ,c_adj.shp_to_brnch_cd
            ,c_adj.shp_to_brnch_nm
            ,c_adj.po_nbr
            ,c_adj.po_crt_dt
            ,c_adj.recpt_crt_dt
            ,c_adj.drop_shp_flg
            ,c_adj.prod_uom_cd
            ,c_adj.item_cost_amt
            ,c_adj.curnc_flg
            ,c_adj.recpt_qty
            ,c_adj.vndr_shp_qty
            ,c_adj.buy_pkg_unt_cnt
            ,c_adj.as_of_dt
            ,SYSDATE
            ,'N'
            ,'N'
            ,l_batch_create);

          l_sec := 'Inserted receipt tbl ' || c_adj.vndr_recpt_unq_nbr ||
                   '~ADJ~' || l_adjustment_id;

          --create adjustment record
          INSERT INTO xxcus.xxcus_rebate_adjustment_tbl
            (adjustment_id
            ,bu_nm
            ,vndr_recpt_unq_nbr
            ,fiscal_per_id
            ,creation_date
            ,adj_recpt_unq_nbr
            ,item_cost_amt
            ,recpt_qty
            ,buy_pkg_unt_cnt
            ,pt_vndr_cd
            ,rebt_vndr_cd
            ,shp_to_brnch_cd
            ,batch_created)
          VALUES
            (l_adjustment_id
            ,c_adj.bu_nm
            ,c_adj.vndr_recpt_unq_nbr
            ,c_adj.fiscal_per_id
            ,SYSDATE
            ,c_adj.vndr_recpt_unq_nbr || '~ADJ~' || l_adjustment_id
            ,c_adj.item_cost_amt
            ,c_adj.recpt_qty
            ,c_adj.buy_pkg_unt_cnt
            ,c_adj.pt_vndr_cd
            ,c_adj.rebt_vndr_cd
            ,c_adj.shp_to_brnch_cd
            ,l_batch_create);

          l_sec := 'Inserted adjustment tbl ' || l_adjustment_id;

          UPDATE xxcus.xxcus_rebate_receipt_curr_tbl
             SET adjusted = 'Y'
           WHERE bu_nm = c_adj.bu_nm
             AND vndr_recpt_unq_nbr = c_adj.vndr_recpt_unq_nbr;

          --create reversal receipt
          INSERT INTO xxcus.xxcus_rebate_receipt_curr_tbl
            (geo_loc_hier_lob_nm
            ,src_sys_nm
            ,bu_nm
            ,bu_cd
            ,fiscal_per_id
            ,sku_cd
            ,prod_id
            ,pt_vndr_cd
            ,sf_vndr_id
            ,sf_vndr_cd
            ,rebt_vndr_cd
            ,rebt_vndr_nm
            ,shp_to_brnch_cd
            ,vndr_recpt_unq_nbr
            ,po_nbr
            ,po_crt_dt
            ,recpt_crt_dt
            ,prod_uom_cd
            ,curnc_flg
            ,item_cost_amt
            ,recpt_qty
            ,buy_pkg_unt_cnt
            ,as_of_dt
            ,status_flag
            ,adjusted
            ,batch_created)
          VALUES
            (c_adj.geo_loc_hier_lob_nm
            ,c_adj.src_sys_nm
            ,c_adj.bu_nm
            ,c_adj.bu_cd
            ,c_adj.fiscal_per_id
            ,c_adj.sku_cd
            ,c_adj.prod_id
            ,c_adj.old_pt_vndr_cd
            ,c_adj.sf_vndr_id
            ,c_adj.sf_vndr_cd
            ,c_adj.old_rebt_cd
            ,c_adj.old_rebt_nm
            ,c_adj.old_brnch
            ,c_adj.old_recpt_nbr || '~REV~' || l_adjustment_id
            ,c_adj.old_po
            ,c_adj.old_po_dt
            ,c_adj.old_recpt_dt
            ,c_adj.old_uom
            ,c_adj.old_curnc
            ,c_adj.old_cost
            ,c_adj.old_qty * -1
            ,c_adj.old_pkg_cnt
            ,c_adj.as_of_dt
            ,'N'
            ,'N'
            ,l_batch_create);

          l_sec := 'Inserted receipt tbl with reversal ' ||
                   c_adj.old_recpt_nbr || '~REV~' || l_adjustment_id;
        END LOOP;

      END IF;

      fnd_file.put_line(fnd_file.log
                       ,'End time:  ' ||
                        to_char(SYSDATE, 'DD-MON-YYYY HH24:MI:SS') ||
                        ' for ' || l_sec);
      fnd_file.put_line(fnd_file.output
                       ,'End time:  ' ||
                        to_char(SYSDATE, 'DD-MON-YYYY HH24:MI:SS') ||
                        ' for ' || l_sec);

      dbms_output.put_line('End time:  ' ||
                           to_char(SYSDATE, 'DD-MON-YYYY HH24:MI:SS') ||
                           ' for ' || l_sec);
    END;

    --starting receipts that have been adjusted at least once and in the adjustment table (new process Sep 2012)
    BEGIN

      BEGIN
        l_adj_count := NULL;

        SELECT COUNT(*)
          INTO l_adj_count
          FROM xxcus.xxcus_rebate_receipt_tbl
         WHERE bu_nm = l_oracle_bu_nm --Version 1.2 p_bu_nm
           AND update_action = 'UA'
           ;
            --
            fnd_file.put_line(fnd_file.log,'@ Ver 3.5 - UA receipt in xxcus.xxcus_rebate_receipt_curr_tbl,   l_adj_count '||l_adj_count); -- Ver 3.5
            --
      EXCEPTION
        WHEN no_data_found THEN
          l_adj_count := 0;
        WHEN OTHERS THEN
          l_adj_count := 0;

      END;

      l_sec := 'Insert the receipts that have been adjusted - Record Count = ' ||
               l_adj_count || '.';
      fnd_file.put_line(fnd_file.log
                       ,'Start time:  ' ||
                        to_char(SYSDATE, 'DD-MON-YYYY HH24:MI:SS') ||
                        ' for ' || l_sec);
      fnd_file.put_line(fnd_file.output
                       ,'Start time:  ' ||
                        to_char(SYSDATE, 'DD-MON-YYYY HH24:MI:SS') ||
                        ' for ' || l_sec);

      dbms_output.put_line('Start time:  ' ||
                           to_char(SYSDATE, 'DD-MON-YYYY HH24:MI:SS') ||
                           ' for ' || l_sec);

      UPDATE xxcus.xxcus_rebate_aud_tbl
         SET stage = l_sec, stg_date = SYSDATE
       WHERE batch_created = l_batch_create
         AND procedure_name = l_procedure
         AND fiscal_period = p_fperiod
         AND post_run_id = l_runnum
         AND bu_nm = p_bu_nm;

      COMMIT;

      IF l_adj_count > 0
      THEN

        --get info from processed receipts to reverse (adjustment has qty  from previous receipt data)
        FOR c2_adj IN (SELECT /*+ PARALLEL (r 2) PARALLEL (ra 2) */ -- Ver 3.6
                        r.geo_loc_hier_lob_nm
                       ,r.src_sys_nm
                       ,r.bu_nm
                       ,r.bu_cd
                       ,r.fiscal_per_id
                       ,r.vndr_recpt_unq_nbr
                       ,r.pt_vndr_id
                       ,r.pt_vndr_cd
                       ,r.sf_vndr_id
                       ,r.sf_vndr_cd
                       ,r.rebt_vndr_id
                       ,r.rebt_vndr_cd
                       ,r.rebt_vndr_nm
                       ,r.prod_id
                       ,r.sku_cd
                       ,r.vndr_part_nbr
                       ,r.shp_to_brnch_id
                       ,r.shp_to_brnch_cd
                       ,r.shp_to_brnch_nm
                       ,r.po_nbr
                       ,r.po_crt_dt
                       ,r.recpt_crt_dt
                       ,r.drop_shp_flg
                       ,r.prod_uom_cd
                       ,r.item_cost_amt
                       ,r.curnc_flg
                       ,r.recpt_qty
                       ,r.vndr_shp_qty
                       ,r.buy_pkg_unt_cnt
                       ,r.as_of_dt
                       ,ra.adjustment_id
                       ,ra.fiscal_per_id      adj_fiscal_per_id
                       ,ra.adj_recpt_unq_nbr
                       ,ra.pt_vndr_cd         adj_pt_vndr_cd
                       ,ra.rebt_vndr_cd       adj_rebt_vndr_cd
                       ,ra.shp_to_brnch_cd    adj_brnch
                       ,ra.item_cost_amt      adj_cost
                       ,ra.recpt_qty          adj_qty
                       ,ra.buy_pkg_unt_cnt    adj_pkg_cnt
                         FROM xxcus.xxcus_rebate_receipt_tbl    r
                             ,xxcus.xxcus_rebate_adjustment_tbl ra
                        WHERE r.bu_nm = l_oracle_bu_nm --Version 1.2 p_bu_nm
                          AND r.bu_nm = ra.bu_nm
                          AND r.vndr_recpt_unq_nbr = ra.vndr_recpt_unq_nbr
                          AND r.update_action = 'UA'
                          AND ra.adjustment_id =
                              (SELECT MAX(adjustment_id)
                                 FROM xxcus.xxcus_rebate_adjustment_tbl x
                                WHERE r.bu_nm = ra.bu_nm
                                  AND x.vndr_recpt_unq_nbr =
                                      ra.vndr_recpt_unq_nbr)
                  )
        LOOP

          SELECT xxcus.xxcus_rebate_adjustment_s.nextval
            INTO l_adjustment_id
            FROM dual;

          --creating receipt
          INSERT INTO xxcus.xxcus_rebate_receipt_curr_tbl
            (geo_loc_hier_lob_nm
            ,src_sys_nm
            ,bu_nm
            ,bu_cd
            ,fiscal_per_id
            ,vndr_recpt_unq_nbr
            ,pt_vndr_id
            ,pt_vndr_cd
            ,sf_vndr_id
            ,sf_vndr_cd
            ,rebt_vndr_id
            ,rebt_vndr_cd
            ,rebt_vndr_nm
            ,prod_id
            ,sku_cd
            ,vndr_part_nbr
            ,shp_to_brnch_id
            ,shp_to_brnch_cd
            ,shp_to_brnch_nm
            ,po_nbr
            ,po_crt_dt
            ,recpt_crt_dt
            ,drop_shp_flg
            ,prod_uom_cd
            ,item_cost_amt
            ,curnc_flg
            ,recpt_qty
            ,vndr_shp_qty
            ,buy_pkg_unt_cnt
            ,as_of_dt
            ,last_update_date
            ,status_flag
            ,adjusted
            ,batch_created)
          VALUES
            (c2_adj.geo_loc_hier_lob_nm
            ,c2_adj.src_sys_nm
            ,c2_adj.bu_nm
            ,c2_adj.bu_cd
            ,c2_adj.fiscal_per_id
            ,c2_adj.vndr_recpt_unq_nbr || '~ADJ~' || l_adjustment_id
            ,c2_adj.pt_vndr_id
            ,c2_adj.pt_vndr_cd
            ,c2_adj.sf_vndr_id
            ,c2_adj.sf_vndr_cd
            ,c2_adj.rebt_vndr_id
            ,c2_adj.rebt_vndr_cd
            ,c2_adj.rebt_vndr_nm
            ,c2_adj.prod_id
            ,c2_adj.sku_cd
            ,c2_adj.vndr_part_nbr
            ,c2_adj.shp_to_brnch_id
            ,c2_adj.shp_to_brnch_cd
            ,c2_adj.shp_to_brnch_nm
            ,c2_adj.po_nbr
            ,c2_adj.po_crt_dt
            ,c2_adj.recpt_crt_dt
            ,c2_adj.drop_shp_flg
            ,c2_adj.prod_uom_cd
            ,c2_adj.item_cost_amt
            ,c2_adj.curnc_flg
            ,c2_adj.recpt_qty
            ,c2_adj.vndr_shp_qty
            ,c2_adj.buy_pkg_unt_cnt
            ,c2_adj.as_of_dt
            ,SYSDATE
            ,'N'
            ,'N'
            ,l_batch_create);

          l_sec := 'Inserting receipt tbl that had been adjusted before ' ||
                   c2_adj.vndr_recpt_unq_nbr || '~ADJ~' || l_adjustment_id;

          --creating adjustment record
          INSERT INTO xxcus.xxcus_rebate_adjustment_tbl
            (adjustment_id
            ,bu_nm
            ,vndr_recpt_unq_nbr
            ,fiscal_per_id
            ,creation_date
            ,adj_recpt_unq_nbr
            ,reversal_adj_recpt
            ,item_cost_amt
            ,recpt_qty
            ,buy_pkg_unt_cnt
            ,pt_vndr_cd
            ,rebt_vndr_cd
            ,shp_to_brnch_cd
            ,batch_created)
          VALUES
            (l_adjustment_id
            ,c2_adj.bu_nm
            ,c2_adj.vndr_recpt_unq_nbr
            ,c2_adj.fiscal_per_id
            ,SYSDATE
            ,c2_adj.vndr_recpt_unq_nbr || '~ADJ~' || l_adjustment_id
            ,c2_adj.adj_recpt_unq_nbr
            ,c2_adj.item_cost_amt
            ,c2_adj.recpt_qty
            ,c2_adj.buy_pkg_unt_cnt
            ,c2_adj.pt_vndr_cd
            ,c2_adj.rebt_vndr_cd
            ,c2_adj.shp_to_brnch_cd
            ,l_batch_create);

          l_sec := 'Inserting adjustment tbl that had been adjusted before ' ||
                   c2_adj.vndr_recpt_unq_nbr || '~ADJ~' || l_adjustment_id;

          --create reversal receipt
          INSERT INTO xxcus.xxcus_rebate_receipt_curr_tbl
            (geo_loc_hier_lob_nm
            ,src_sys_nm
            ,bu_nm
            ,bu_cd
            ,fiscal_per_id
            ,sku_cd
            ,prod_id
            ,pt_vndr_cd
            ,sf_vndr_id
            ,sf_vndr_cd
            ,rebt_vndr_cd
            ,shp_to_brnch_cd
            ,vndr_recpt_unq_nbr
            ,po_nbr
            ,po_crt_dt
            ,recpt_crt_dt
            ,prod_uom_cd
            ,curnc_flg
            ,item_cost_amt
            ,recpt_qty
            ,buy_pkg_unt_cnt
            ,as_of_dt
            ,status_flag
            ,adjusted
            ,batch_created)
          VALUES
            (c2_adj.geo_loc_hier_lob_nm
            ,c2_adj.src_sys_nm
            ,c2_adj.bu_nm
            ,c2_adj.bu_cd
            ,c2_adj.adj_fiscal_per_id
            ,c2_adj.sku_cd
            ,c2_adj.prod_id
            ,c2_adj.adj_pt_vndr_cd
            ,c2_adj.sf_vndr_id
            ,c2_adj.sf_vndr_cd
            ,c2_adj.adj_rebt_vndr_cd
            ,c2_adj.adj_brnch
            ,c2_adj.adj_recpt_unq_nbr || '~REV~' || l_adjustment_id
            ,c2_adj.po_nbr
            ,c2_adj.po_crt_dt
            ,c2_adj.recpt_crt_dt
            ,c2_adj.prod_uom_cd
            ,c2_adj.curnc_flg
            ,c2_adj.adj_cost
            ,c2_adj.adj_qty * -1
            ,c2_adj.adj_pkg_cnt
            ,c2_adj.as_of_dt
            ,'N'
            ,'N'
            ,l_batch_create);

          l_sec := 'Reversal inserting receipt tbl that had been adjusted before ' ||
                   c2_adj.adj_recpt_unq_nbr || '~REV~' || l_adjustment_id;

        END LOOP;
      END IF;
      fnd_file.put_line(fnd_file.log
                       ,'End time:  ' ||
                        to_char(SYSDATE, 'DD-MON-YYYY HH24:MI:SS') ||
                        ' for ' || l_sec);
      fnd_file.put_line(fnd_file.output
                       ,'End time:  ' ||
                        to_char(SYSDATE, 'DD-MON-YYYY HH24:MI:SS') ||
                        ' for ' || l_sec);

      dbms_output.put_line('End time:  ' ||
                           to_char(SYSDATE, 'DD-MON-YYYY HH24:MI:SS') ||
                           ' for ' || l_sec);
    END;

    BEGIN
      l_sec := 'Grouping receipts by Rebate Vendor and receipt date for the period ' ||
               p_fperiod || '.';
      fnd_file.put_line(fnd_file.log
                       ,'Start time:  ' ||
                        to_char(SYSDATE, 'DD-MON-YYYY HH24:MI:SS') ||
                        ' for ' || l_sec);
      fnd_file.put_line(fnd_file.output
                       ,'Start time:  ' ||
                        to_char(SYSDATE, 'DD-MON-YYYY HH24:MI:SS') ||
                        ' for ' || l_sec);

      dbms_output.put_line('Start time:  ' ||
                           to_char(SYSDATE, 'DD-MON-YYYY HH24:MI:SS') ||
                           ' for ' || l_sec);

      UPDATE xxcus.xxcus_rebate_aud_tbl
         SET stage = l_sec, stg_date = SYSDATE
       WHERE batch_created = l_batch_create
         AND procedure_name = l_procedure
         AND fiscal_period = p_fperiod
         AND post_run_id = l_runnum
         AND bu_nm = p_bu_nm;

      COMMIT;

      --group is used for creating the batch in ozf_resale_batches_all
      FOR c_group IN (SELECT /*+ PARALLEL (r 4) PARALLEL (c 2) PARALLEL (p 2)  */ -- Ver 3.6
                       p.party_id
                      ,p.party_name
                      ,c.cust_account_id
                      ,rebt_vndr_cd
                      ,recpt_crt_dt
                      ,curnc_flg
                      ,COUNT(*)
                        FROM xxcus.xxcus_rebate_receipt_curr_tbl r
                            ,ar.hz_cust_accounts                 c
                            ,ar.hz_parties                       p
                       WHERE r.bu_nm = l_oracle_bu_nm --Version 1.2 p_bu_nm
                         AND r.recpt_crt_dt BETWEEN l_new_start_date AND --l_start_date AND --  Ver 3.5
                             l_end_date
                         AND r.status_flag = 'N'
                         AND r.rebt_vndr_cd || '~MSTR' = c.account_number
                         AND c.party_id = p.party_id
                       GROUP BY rebt_vndr_cd
                               ,recpt_crt_dt
                               ,c.cust_account_id
                               ,p.party_id
                               ,p.party_name
                               ,r.curnc_flg)
      LOOP

        SELECT xxcus_receipt_id_s.nextval INTO l_receipt FROM dual;

        UPDATE xxcus.xxcus_rebate_receipt_curr_tbl
           SET receipt          = l_receipt
              ,cust_account_id  = c_group.cust_account_id
              ,party_id         = c_group.party_id
              ,party_name       = c_group.party_name
              ,last_update_date = SYSDATE
         WHERE recpt_crt_dt BETWEEN l_new_start_date AND l_end_date -- Ver 3.5
           AND status_flag = 'N'
           AND bu_nm = l_oracle_bu_nm --Version 1.2 p_bu_nm
           AND rebt_vndr_cd = c_group.rebt_vndr_cd
           AND recpt_crt_dt = c_group.recpt_crt_dt
           AND curnc_flg = c_group.curnc_flg;

      END LOOP;
      COMMIT;

      fnd_file.put_line(fnd_file.log
                       ,'End time:  ' ||
                        to_char(SYSDATE, 'DD-MON-YYYY HH24:MI:SS') ||
                        ' for ' || l_sec);
      fnd_file.put_line(fnd_file.output
                       ,'End time:  ' ||
                        to_char(SYSDATE, 'DD-MON-YYYY HH24:MI:SS') ||
                        ' for ' || l_sec);

      dbms_output.put_line('End time:  ' ||
                           to_char(SYSDATE, 'DD-MON-YYYY HH24:MI:SS') ||
                           ' for ' || l_sec);

    END;

    --start loading receipts from stagging to ozf_resale_lines_int_all

    l_sec := 'Loading receipts into resale iface for the period ' ||
             p_fperiod || '  Report date between ' || l_start_date ||
             ' and ' || l_end_date;
    fnd_file.put_line(fnd_file.log
                     ,'Start time:  ' ||
                      to_char(SYSDATE, 'DD-MON-YYYY HH24:MI:SS') ||
                      ' for ' || l_sec);
    fnd_file.put_line(fnd_file.output
                     ,'Start time:  ' ||
                      to_char(SYSDATE, 'DD-MON-YYYY HH24:MI:SS') ||
                      ' for ' || l_sec);

    UPDATE xxcus.xxcus_rebate_aud_tbl
       SET stage = l_sec, stg_date = SYSDATE
     WHERE batch_created = l_batch_create
       AND procedure_name = l_procedure
       AND fiscal_period = p_fperiod
       AND post_run_id = l_runnum
       AND bu_nm = p_bu_nm;

    COMMIT;
    -- loading receipts to the resale lines interface - check for performance
    BEGIN
      FOR c_receipt IN (SELECT /*+ PARALLEL (r 4) PARALLEL (i 4) PARALLEL (cp 4)  */ -- Ver 3.6
                         r.rowid
                        ,r.vndr_recpt_unq_nbr
                        ,r.pt_vndr_cd
                        ,r.shp_to_brnch_cd
                        ,r.geo_loc_hier_lob_nm
                        ,r.bu_nm
                        ,r.bu_cd
                        ,r.src_sys_nm
                        ,r.drop_shp_flg
                        ,r.po_nbr
                        ,r.recpt_crt_dt
                        ,r.vndr_part_nbr
                        ,(CASE
                           WHEN r.item_cost_amt < 0 THEN
                            r.item_cost_amt * -1
                           ELSE
                            r.item_cost_amt
                         END) item_cost_amt
                        ,(CASE
                           WHEN r.item_cost_amt < 0 THEN
                            r.recpt_qty * -1
                           ELSE
                            r.recpt_qty
                         END) / (CASE
                           WHEN buy_pkg_unt_cnt IS NULL THEN
                            1
                           WHEN buy_pkg_unt_cnt = 0 THEN
                            1
                           ELSE
                            buy_pkg_unt_cnt
                         END) recpt_qty
                        ,(CASE
                           WHEN buy_pkg_unt_cnt IS NULL THEN
                            1
                           WHEN buy_pkg_unt_cnt = 0 THEN
                            1
                           ELSE
                            buy_pkg_unt_cnt
                         END) buy_pkg_unt_cnt
                        ,(CASE
                           WHEN item_cost_amt < 0 THEN
                            item_cost_amt * -1
                           ELSE
                            item_cost_amt
                         END) * ((CASE
                           WHEN item_cost_amt < 0 THEN
                            recpt_qty * -1
                           ELSE
                            recpt_qty
                         END) / (CASE
                           WHEN buy_pkg_unt_cnt IS NULL THEN
                            1
                           WHEN buy_pkg_unt_cnt = 0 THEN
                            1
                           ELSE
                            buy_pkg_unt_cnt
                         END)) total_cost
                        ,decode(r.curnc_flg, 'CND', 'CAD', 'USD') curnc_flg
                        ,to_char(r.receipt) receipt
                        ,r.prod_id
                        ,r.sku_cd
                        ,i.inventory_item_id
                        ,i.segment1
                        ,i.description
                        ,r.prod_uom_cd
                        ,i.primary_uom_code
                         --Version 1.8
                        ,xxcus_tm_interface_pkg.get_branch(r.bu_nm
                                                          ,r.shp_to_brnch_cd
                                                          ,r.geo_loc_hier_lob_nm) bill_to_cust_acct_id
                         /*  --Version 1.8
                         ,(SELECT ba.cust_account_id
                             FROM ar.hz_cust_accounts ba
                            WHERE ba.customer_type = 'I'
                              AND ba.status = 'A'
                              AND ba.attribute1 = l_cust_attr
                              AND ba.account_number = r.geo_loc_hier_lob_nm || '.' ||
                                  xrbv.rebates_fru || '.' ||
                                 --r.shp_to_brnch_cd   --Version 1.6  9/12/13
                                  xrbv.lob_branch --Version 1.6  9/12/13
                           ) bill_to_cust_acct_id
                         ,(SELECT cust_account_id
                             FROM ar.hz_cust_accounts hca
                            WHERE hca.customer_type = 'I'
                              AND hca.status = 'A'
                              AND hca.account_number = r.geo_loc_hier_lob_nm || '.' ||
                                  'DRPSHP') drpshp_cust_acct_id
                          */
                        ,bp.party_id bill_to_party_id
                        ,bp.party_name bill_to_party_name
                        ,ps.party_site_id
                        ,r.cust_account_id
                        ,r.party_id
                        ,hprcpt.party_name
                        ,(SELECT category_id
                            FROM apps.mtl_item_categories_v c
                           WHERE c.category_set_id = l_category_set
                             AND c.organization_id = l_inv_org
                             AND c.inventory_item_id = i.inventory_item_id
                             AND c.segment1 = r.rebt_vndr_cd) category_id
                        ,cp.cust_acct_id
                        ,to_number(nvl(TRIM(regexp_replace(octp.attribute2
                                                          ,'([^[:print:]])'
                                                          ,' '))
                                      ,'0')) freight_discount
                        ,to_number(nvl(TRIM(regexp_replace(octp.attribute3
                                                          ,'([^[:print:]])'
                                                          ,' '))
                                      ,'0')) pmt_discount
                         --,octp.attribute2 freight_discount --added by NS -- commented out 09/08/2014
                         --,octp.attribute3 pmt_discount --added by NS -- commented out 09/08/2014
                        ,xref.oracle_uom_code
                        ,hp1.party_name       bu_name
                        ,hp1.party_id         bu_id
                          FROM xxcus.xxcus_rebate_receipt_curr_tbl r
                               --,apps.xxcus_rebate_branch_vw         xrbv    --Version 1.8
                              ,inv.mtl_system_items_b      i
                              ,apps.xxcus_cust_prod_rebt_v cp
                              ,ar.hz_parties               hprcpt --03/17/2013
                              ,ar.hz_parties               bp
                              ,ar.hz_party_sites           ps
                              ,ar.hz_parties               hp1
                              ,ar.hz_relationships         rel
                              ,ozf_cust_trd_prfls_all      octp ---added by NS
                              ,xxcus.xxcus_rebate_uom_tbl  xref
                         WHERE r.bu_nm = l_oracle_bu_nm --Version 1.2  p_bu_nm
                           AND r.recpt_crt_dt BETWEEN l_new_start_date AND -- Ver 3.5
                               l_end_date
                           AND r.status_flag = 'N'
                           AND nvl(r.receipt, -1) <> -1 --8/13/13  IS NOT NULL
                           AND r.sku_cd || '~' || r.bu_nm || '~' ||
                               r.src_sys_nm --Version 1.7  9/17/13
                               = i.description
                           AND i.organization_id = l_inv_org
                              --AND i.enabled_flag = 'Y' --COMMENTED 09/02/2014
                           AND i.inventory_item_status_code = 'Active' --added 09/02/2014
                           AND nvl(r.item_cost_amt, 0) <> 0
                           AND nvl(r.recpt_qty, 0) <> 0
                              --AND r.shp_to_brnch_cd = xrbv.oper_branch(+)   --Version 1.8
                              --AND r.bu_nm = xrbv.dw_bu_desc(+)              --Version 1.8
                           AND r.cust_account_id = cp.cust_acct_id(+)
                           AND r.party_id = hprcpt.party_id
                           AND r.cust_account_id = octp.cust_account_id(+) ---added by NS --Version 2.6  1/2/15
                           AND decode(r.curnc_flg, 'CND', 'CAD', 'USD') =
                               octp.claim_currency(+)  --Version 2.6   1/2/15
                           AND r.geo_loc_hier_lob_nm = bp.known_as
                           AND bp.attribute1 = l_lob_attr
                           AND bp.party_id = rel.object_id
                           AND rel.relationship_code = 'DIVISION_OF'
                           AND rel.relationship_type =
                               'HEADQUARTERS/DIVISION'
                           AND rel.subject_id = hp1.party_id
                           AND hp1.attribute1 = l_bu_attr
                           AND hp1.party_name = r.bu_nm --added 10-24
                           AND rel.status = 'A'
                           AND rel.directional_flag IN ('B', 'F')
                           AND r.pt_vndr_cd || '~' || r.bu_cd =
                               ps.party_site_number
                           AND nvl(r.prod_uom_cd, 'UNKNOWN') =
                               xref.purch_uom_cd --only items with a valid uom
                           AND r.bu_nm = xref.bu_nm
                           AND upper(r.src_sys_nm) = upper(xref.src_sys_nm))
      LOOP

        l_sec := 'Receipt inserting into resale line int ' ||
                 c_receipt.vndr_recpt_unq_nbr;
        IF c_receipt.recpt_qty < 0
        THEN
          l_ordr_ctgry       := 'RETURN';
          l_trnsfr_typ       := 'BN';
          l_trnsfr_mvmnt_typ := 'CD';
        ELSE
          l_ordr_ctgry       := 'ORDER';
          l_trnsfr_typ       := 'SS';
          l_trnsfr_mvmnt_typ := 'DC';
        END IF;

        IF c_receipt.curnc_flg = 'USD'
        THEN
          l_org := l_us_org;
        ELSE
          l_org := l_cn_org;
        END IF;

        /*IF the  customer has discounts associated with */ --Added by NS

        IF c_receipt.freight_discount IS NOT NULL OR
           c_receipt.pmt_discount IS NOT NULL
        THEN

          l_selling_price := c_receipt.item_cost_amt -
                             ((c_receipt.freight_discount +
                             c_receipt.pmt_discount) *
                             c_receipt.item_cost_amt) / 100;
        ELSE
          l_selling_price := c_receipt.item_cost_amt;
        END IF;

        /* Get the Product and Location Segments */

        get_gl_segments(c_receipt.bu_nm
                       ,c_receipt.shp_to_brnch_cd
                       ,l_prod_segment
                       ,l_loc_segment);

        IF c_receipt.cust_acct_id IS NULL OR
           (c_receipt.cust_acct_id IS NOT NULL AND
           nvl(c_receipt.category_id, l_category) <> l_category)
        THEN

          IF c_receipt.recpt_qty > 0 AND c_receipt.total_cost >= l_high OR
             c_receipt.recpt_qty < 0 AND c_receipt.total_cost <= l_low
          THEN
                    --
                    l_resale_line_int_id :=ozf_resale_lines_int_all_s.nextval; --Ver 3.1
                    --
            INSERT INTO ozf_resale_lines_int_all
              (
               resale_line_int_id
              ,object_version_number
              ,last_update_date
              ,last_updated_by
              ,creation_date
              ,created_by
              ,resale_batch_id
              ,status_code
              ,resale_transfer_type
              ,product_transfer_movement_type
              ,tracing_flag
              ,ship_from_cust_account_id
              ,ship_from_party_name
              ,ship_from_site_id
              ,sold_from_cust_account_id
              ,sold_from_party_name
              ,sold_from_site_id
              ,bill_to_cust_account_id
              ,bill_to_party_id
              ,bill_to_party_name
              ,
               --BILL_TO_PARTY_SITE_ID,
               end_cust_party_name
              ,end_cust_party_id
              ,direct_customer_flag
              ,order_category
              ,currency_code
              ,claimed_amount
              ,allowed_amount
              ,total_allowed_amount
              ,total_claimed_amount
              ,uom_code
              ,quantity
              ,calculated_price
              ,calculated_amount
              ,org_id
              ,data_source_code
              ,order_number
              ,date_ordered
              ,item_number
              ,inventory_item_id
              ,response_code
              ,acctd_calculated_price
              ,selling_price
              ,acctd_selling_price
              ,purchase_price
              ,purchase_uom_code
              ,total_accepted_amount
              ,accepted_amount
              ,po_number
              ,line_attribute5 --receipt number
              ,line_attribute1 --pay-to vendor code
              ,line_attribute2 --bu_nm
              ,line_attribute3 --src_sys_nm
              ,line_attribute6 --ship to branch code
              ,line_attribute11 --product segment
              ,line_attribute12 --location segment
              ,line_attribute13 --added by NS  freight_discount
              ,line_attribute14 --added by NS pmt_discount
              ,date_invoiced --01/30/13  gl date logic added
               )
            VALUES
              (
                l_resale_line_int_id --Ver 3.1
              ,1
              ,SYSDATE
              ,l_user_id
              ,SYSDATE
              ,l_user_id
              ,l_batch
              ,'OPEN'
              ,l_trnsfr_typ
              ,l_trnsfr_mvmnt_typ
              ,'T'
              ,c_receipt.cust_account_id
              ,c_receipt.party_name
              ,c_receipt.party_site_id
              ,c_receipt.cust_account_id
              ,c_receipt.party_name
              ,c_receipt.party_site_id
               --,nvl(c_receipt.bill_to_cust_acct_id,c_receipt.drpshp_cust_acct_id)  --Version 1.8
              ,c_receipt.bill_to_cust_acct_id --Version 1.8
              ,c_receipt.bill_to_party_id
              ,c_receipt.bill_to_party_name
              ,
               --c_receipt.party_site_id,
               c_receipt.bu_name
              ,c_receipt.bu_id
              ,'T'
              ,l_ordr_ctgry
              , --'ORDER',
               c_receipt.curnc_flg
              ,0
              ,0
              ,0
              ,0
              ,c_receipt.primary_uom_code
              ,c_receipt.recpt_qty
              ,0
              ,0
              ,l_org
              ,'ORCL_DW'
              ,c_receipt.receipt
              ,c_receipt.recpt_crt_dt
              ,c_receipt.segment1
              ,c_receipt.inventory_item_id
              ,'Y'
              ,0
              ,l_selling_price
              ,l_selling_price
              ,c_receipt.item_cost_amt
              ,nvl(c_receipt.oracle_uom_code, 'UNK')
              ,0
              ,0
              ,c_receipt.po_nbr
              ,c_receipt.vndr_recpt_unq_nbr
              ,c_receipt.pt_vndr_cd --pay-to vendor code
              ,c_receipt.bu_nm
              ,c_receipt.src_sys_nm
              ,c_receipt.shp_to_brnch_cd
              ,l_prod_segment
              ,l_loc_segment
              ,c_receipt.freight_discount
              ,c_receipt.pmt_discount
               --,l_end_date - 10            --Version 1.8 added the -10 days --Commented as per ESMS 267539
              ,l_fiscal_period_start + n_days --l_fiscal_period_start + 5 --Added as per ESMS 267539
               ); --Version 1.8 added the -10 days

            v_rec_cnt := v_rec_cnt + 1;

            UPDATE xxcus.xxcus_rebate_receipt_curr_tbl
               SET status_flag = 'P', process_date = SYSDATE
             WHERE ROWID = c_receipt.rowid;

          END IF;
        END IF;

      END LOOP;
      --
      fnd_file.put_line(fnd_file.log
                       ,'End time:  ' ||
                        to_char(SYSDATE, 'DD-MON-YYYY HH24:MI:SS') ||
                        ' for ' || l_sec);
      fnd_file.put_line(fnd_file.output
                       ,'End time:  ' ||
                        to_char(SYSDATE, 'DD-MON-YYYY HH24:MI:SS') ||
                        ' for ' || l_sec);
      --
      COMMIT;
      --
      -- Begin Ver 3.1
      --
      begin
            select
                     sum ( case when b.status_flag ='P' then 1 else 0 end )
                     -- Begin Ver 3.5
                    --,sum ( case  when b.status_flag ='P' then (nvl(b.item_cost_amt, 0) * nvl(b.recpt_qty, 0)) else 0 end )
                    ,sum ( case  when b.status_flag ='P'
                                  then (
                                              --nvl(b.item_cost_amt, 0) * nvl(b.recpt_qty, 0)
                                              (CASE
                                                   WHEN b.item_cost_amt < 0 THEN
                                                    b.item_cost_amt * -1
                                                   ELSE
                                                    b.item_cost_amt
                                                 END)*
                                                             (CASE
                                                               WHEN b.item_cost_amt < 0 THEN
                                                                b.recpt_qty * -1
                                                               ELSE
                                                                b.recpt_qty
                                                             END) / (CASE
                                                               WHEN buy_pkg_unt_cnt IS NULL THEN
                                                                1
                                                               WHEN buy_pkg_unt_cnt = 0 THEN
                                                                1
                                                               ELSE
                                                                buy_pkg_unt_cnt
                                                             END)
                                            )
                                  else 0 end
                              )
                     -- End Ver 3.5
                    ,sum ( case when nvl(b.status_flag, 'N') ='N' then 1 else 0 end )
                     -- Begin Ver 3.5
                   -- ,sum ( case  when nvl(b.status_flag, 'N') ='N' then (nvl(b.item_cost_amt, 0) * nvl(b.recpt_qty, 0)) else 0 end )
                    ,sum ( case  when b.status_flag ='N'
                                  then (
                                              --nvl(b.item_cost_amt, 0) * nvl(b.recpt_qty, 0)
                                              (CASE
                                                   WHEN b.item_cost_amt < 0 THEN
                                                    b.item_cost_amt * -1
                                                   ELSE
                                                    b.item_cost_amt
                                                 END)*
                                                             (CASE
                                                               WHEN b.item_cost_amt < 0 THEN
                                                                b.recpt_qty * -1
                                                               ELSE
                                                                b.recpt_qty
                                                             END) / (CASE
                                                               WHEN buy_pkg_unt_cnt IS NULL THEN
                                                                1
                                                               WHEN buy_pkg_unt_cnt = 0 THEN
                                                                1
                                                               ELSE
                                                                buy_pkg_unt_cnt
                                                             END)
                                            )
                                  else 0 end
                              )
                     -- End Ver 3.5
            into l_ct4_recon_rows_P
                   ,l_ct4_recon_amount_P
                   ,l_ct4_recon_rows_N
                   ,l_ct4_recon_amount_N
            from xxcus.xxcus_rebate_receipt_curr_tbl b
            where 1 =1
            and b.bu_nm =l_oracle_bu_nm
            and b.recpt_crt_dt between l_fiscal_week_start and l_fiscal_week_end
            ;
          --
      exception
       when others then
            l_ct4_recon_rows_P :=-96;
            l_ct4_recon_amount_P :=-96;
            l_ct4_recon_rows_N :=-96;
            l_ct4_recon_amount_N :=-96;
            print_log('Error in fetch of ct4 totals from xxcus.xxcus_rebate_receipt_sdw_tbl, message :'||sqlerrm);
      end;
      --
      begin
       savepoint ct4b;
       update xxcus.xxcus_rebates_recon_audit_b
               set  ct4_stg_processed_rows =l_ct4_recon_rows_P
                     ,ct4_stg_processed_amount =l_ct4_recon_amount_P
                     ,ct4_stg_unprocessed_rows =l_ct4_recon_rows_N
                     ,ct4_stg_unprocessed_amount =l_ct4_recon_amount_N
                     ,status_code ='IN PROCESS'
                     ,stage_code ='EBS-CT4'
                     ,last_update_date =sysdate
          where 1 =1
               and bu_name =p_bu_nm
               and dw_btch_id =l_dw_btch_id
               and status_code ='IN PROCESS'
               and stage_code ='EBS-CT3';
            print_log (' ');
            print_log (   'CT4 Complete, Total rows [P] :'||
                                   l_ct4_recon_rows_P||
                                   ', Total Amount [P] :'
                                   ||l_ct4_recon_amount_P||
                                    ', Total rows [N] :'||
                                   l_ct4_recon_rows_N||
                                   ', Total Amount [N] :'
                                   ||l_ct4_recon_amount_N
                              );
            print_log (' ');
       commit;
      exception
       when others then
        rollback to ct4b;
        print_log('Error in updating recon audit table status code to EBS-CT4, message :'||sqlerrm);
      end;
      --
      -- End Ver 3.1
      --
      --
      l_sec := 'Receipts Loaded...= ' || v_rec_cnt;
      fnd_file.put_line(fnd_file.log, l_sec);
      fnd_file.put_line(fnd_file.output, l_sec);
      --dbms_output.put_line('Receipts created...= ' || v_rec_cnt);

      UPDATE xxcus.xxcus_rebate_aud_tbl
         SET stage = l_sec, stg_date = SYSDATE, record_cnt = v_rec_cnt
       WHERE batch_created = l_batch_create
         AND procedure_name = l_procedure
         AND fiscal_period = p_fperiod
         AND post_run_id = l_runnum
         AND bu_nm = p_bu_nm;

    EXCEPTION
      WHEN OTHERS THEN
        l_err_msg := l_err_msg || 'Unexpected Error Occured in Section :' ||
                     '::ERROR :' || SQLERRM;
        dbms_output.put_line(l_err_msg);
        ROLLBACK;
    END;

    --insert batch

    BEGIN

      l_sec := 'Loading batches for receipts.';
      fnd_file.put_line(fnd_file.log
                       ,'Start time:  ' ||
                        to_char(SYSDATE, 'DD-MON-YYYY HH24:MI:SS') ||
                        ' for ' || l_sec);
      fnd_file.put_line(fnd_file.output
                       ,'Start time:  ' ||
                        to_char(SYSDATE, 'DD-MON-YYYY HH24:MI:SS') ||
                        ' for ' || l_sec);

      FOR c_batch IN (SELECT  /*+ PARALLEL (i 2) PARALLEL (p 2) */ -- Ver 3.6
                      DISTINCT i.sold_from_cust_account_id
                              ,i.object_version_number
                              ,i.org_id
                              ,i.data_source_code
                              ,p.party_id
                              ,i.currency_code
                              ,COUNT(*) batch_count
                              ,get_offer_type(i.sold_from_cust_account_id) offer_type --Version 1.9  10/28/13
                        FROM ozf_resale_lines_int_all i, hz_parties p
                       WHERE i.status_code = 'OPEN'
                         AND i.data_source_code = 'ORCL_DW'
                         AND i.line_attribute2 = l_oracle_bu_nm --Version 1.2   p_bu_nm
                         AND i.sold_from_party_name = p.party_name
                         AND p.attribute1 = 'HDS_MVID'
                         AND i.resale_batch_id IS NULL
                       GROUP BY sold_from_cust_account_id
                               ,i.object_version_number
                               ,i.org_id
                               ,data_source_code
                               ,p.party_id
                               ,i.currency_code)
      LOOP

        BEGIN

          l_batch     := NULL;
          l_batch_num := NULL;
          l_success   := TRUE;

          SELECT ozf_resale_batches_all_s.nextval INTO l_batch FROM dual;

          SELECT to_char(ozf_resale_batch_number_s.nextval)
            INTO l_batch_num
            FROM dual;

          l_sec := 'API to insert batch ID ' || l_batch;
          ozf_resale_batches_pkg.insert_row(px_resale_batch_id           => l_batch
                                           ,px_object_version_number     => c_batch.object_version_number
                                           ,p_last_update_date           => SYSDATE
                                           ,p_last_updated_by            => l_user_id
                                           ,p_creation_date              => SYSDATE
                                           ,p_request_id                 => NULL
                                           ,p_created_by                 => l_user_id
                                           ,p_created_from               => NULL
                                           ,p_last_update_login          => NULL
                                           ,p_program_application_id     => NULL
                                           ,p_program_update_date        => NULL
                                           ,p_program_id                 => NULL
                                           ,p_batch_number               => l_batch_num
                                           ,p_batch_type                 => 'TRACING'
                                           ,p_batch_count                => c_batch.batch_count
                                           ,p_year                       => NULL
                                           ,p_month                      => NULL
                                           ,p_report_date                => trunc(SYSDATE)
                                           ,p_report_start_date          => l_new_start_date -- l_start_date -- Ver 3.5
                                           ,p_report_end_date            => l_end_date
                                           ,p_status_code                => 'OPEN'
                                           ,p_data_source_code           => c_batch.data_source_code
                                           , -- BUG 5077213
                                            p_reference_type             => NULL
                                           ,p_reference_number           => NULL
                                           ,p_comments                   => NULL
                                           ,p_partner_claim_number       => NULL
                                           ,p_transaction_purpose_code   => '00'
                                           ,p_transaction_type_code      => '01'
                                           ,p_partner_type               => 'DS'
                                           ,p_partner_id                 => NULL
                                           ,p_partner_party_id           => c_batch.party_id
                                           ,p_partner_cust_account_id    => c_batch.sold_from_cust_account_id
                                           ,p_partner_site_id            => NULL
                                           ,p_partner_contact_party_id   => NULL
                                           ,p_partner_contact_name       => NULL
                                           ,p_partner_email              => NULL
                                           ,p_partner_phone              => NULL
                                           ,p_partner_fax                => NULL
                                           ,p_header_tolerance_operand   => NULL
                                           ,p_header_tolerance_calc_code => NULL
                                           ,p_line_tolerance_operand     => NULL
                                           ,p_line_tolerance_calc_code   => NULL
                                           ,p_currency_code              => c_batch.currency_code
                                           ,p_claimed_amount             => NULL
                                           ,p_allowed_amount             => NULL
                                           ,p_paid_amount                => NULL
                                           ,p_disputed_amount            => NULL
                                           ,p_accepted_amount            => NULL
                                           ,p_lines_invalid              => NULL
                                           ,p_lines_w_tolerance          => NULL
                                           ,p_lines_disputed             => NULL
                                           ,p_batch_set_id_code          => 'OTHER'
                                           ,p_credit_code                => NULL
                                           ,p_credit_advice_date         => NULL
                                           ,p_purge_flag                 => NULL
                                           ,p_attribute_category         => NULL
                                           ,p_attribute1                 => NULL
                                           ,p_attribute2                 => NULL
                                           ,p_attribute3                 => NULL
                                           ,p_attribute4                 => NULL
                                           ,p_attribute5                 => NULL
                                           ,p_attribute6                 => NULL
                                           ,p_attribute7                 => NULL
                                           ,p_attribute8                 => NULL
                                           ,p_attribute9                 => NULL
                                           ,p_attribute10                => NULL
                                           ,p_attribute11                => NULL
                                           ,p_attribute12                => NULL
                                           ,p_attribute13                => NULL
                                           ,p_attribute14                => NULL
                                           ,p_attribute15                => NULL
                                           ,px_org_id                    => c_batch.org_id
                                           ,p_direct_order_flag          => NULL); --API change with updgrade 12.1.3

          dbms_output.put_line('Successfully loaded batch id  ' || l_batch);
          l_sec := 'Successfully loaded batch id  ' || l_batch;
          fnd_file.put_line(fnd_file.log, l_sec);
          fnd_file.put_line(fnd_file.output, l_sec);

          UPDATE ozf_resale_lines_int_all
             SET resale_batch_id = l_batch
           WHERE resale_batch_id IS NULL
             AND status_code = 'OPEN'
             AND data_source_code = 'ORCL_DW'
             AND line_attribute2 = l_oracle_bu_nm --Version 1.2   p_bu_nm
             AND sold_from_cust_account_id =
                 c_batch.sold_from_cust_account_id
             AND org_id = c_batch.org_id
             AND object_version_number = c_batch.object_version_number
             AND currency_code = c_batch.currency_code;

          --table used for the HDS Funds Accrual to know what rebate customer the FAE needs to run for
          INSERT INTO xxcus.xxcus_rebt_cust_tbl
            (rebt_cust_acct_id
            ,resale_batch_id
            ,org_id
            ,fiscal_per_id
            ,bu_nm
            ,created_date
            ,offer_type) --Version 1.9  10/28/13
          VALUES
            (c_batch.sold_from_cust_account_id
            ,l_batch
            ,c_batch.org_id
            ,l_fiscal_per_id
            ,l_oracle_bu_nm --Version 1.2   p_bu_nm
            ,SYSDATE
            ,c_batch.offer_type);

          l_sec := 'Insert xxcus_rebt_cust_tbl with batch ' || l_batch;

        EXCEPTION
          WHEN OTHERS THEN
            l_err_msg := l_err_msg ||
                         'Unexpected Error Occured in Section :' ||
                         '::ERROR :' || SQLERRM;
            fnd_file.put_line(fnd_file.log, l_err_msg);
            l_success := FALSE;
            ROLLBACK;

        END;
        IF l_success
        THEN

          l_sec := 'Start process of batches; ';

          BEGIN

            ozf_resale_pub.start_process_iface(p_api_version      => 1.0
                                              ,p_init_msg_list    => fnd_api.g_false
                                              ,p_commit           => fnd_api.g_false
                                              ,p_validation_level => fnd_api.g_valid_level_full
                                              ,p_resale_batch_id  => l_batch
                                              ,x_return_status    => x_return_status
                                              ,x_msg_data         => x_msg_data
                                              ,x_msg_count        => x_msg_count);

            l_sec := 'Processed batch id  ' || l_batch || ' Status: ' ||
                     x_return_status;
            fnd_file.put_line(fnd_file.output, l_sec);

            IF x_return_status = fnd_api.g_ret_sts_success
            THEN
              COMMIT;

              ozf_resale_pub.start_payment(p_api_version      => 1.0
                                          ,p_init_msg_list    => fnd_api.g_false
                                          ,p_commit           => fnd_api.g_false
                                          ,p_validation_level => fnd_api.g_valid_level_full
                                          ,p_resale_batch_id  => l_batch
                                          ,x_return_status    => x_return_status
                                          ,x_msg_data         => x_msg_data
                                          ,x_msg_count        => x_msg_count);

              l_sec := 'Successfully closed payment batch id  ' || l_batch ||
                       ' Status: ' || x_return_status;

              IF x_return_status = fnd_api.g_ret_sts_success
              THEN
                fnd_file.put_line(fnd_file.output, l_sec);
                UPDATE xxcus.xxcus_rebt_cust_tbl c
                   SET status_code = 'CLOSED'
                 WHERE c.resale_batch_id = l_batch;

              ELSE
                l_sec := 'ERROR closed payment batch id  ' || l_batch ||
                         ' Status: ' || x_return_status || ' Reason = ' ||
                         x_msg_data;
                fnd_file.put_line(fnd_file.output, l_sec);
                fnd_file.put_line(fnd_file.log, l_sec);
                --10/23/13
                UPDATE xxcus.xxcus_rebt_cust_tbl c
                   SET status_code = x_return_status
                 WHERE c.resale_batch_id = l_batch;
                --
              END IF;
            ELSE
              l_sec := 'ERROR processing payment batch id  ' || l_batch ||
                       ' Status: ' || x_return_status || ' Reason = ' ||
                       x_msg_data;
              fnd_file.put_line(fnd_file.output, l_sec);
              fnd_file.put_line(fnd_file.log, l_sec);
              --10/23/13
              UPDATE xxcus.xxcus_rebt_cust_tbl c
                 SET status_code = x_return_status
               WHERE c.resale_batch_id = l_batch;
              --

            END IF;

          EXCEPTION
            WHEN OTHERS THEN
              l_err_msg := 'Error in proceccing batch ' || l_batch ||
                           ' at ' || l_sec;
              fnd_file.put_line(fnd_file.log, l_err_msg);
              fnd_file.put_line(fnd_file.output, l_err_msg);
              l_success := FALSE;
              ROLLBACK;
          END;

        END IF;

        COMMIT;

      END LOOP;

      fnd_file.put_line(fnd_file.log
                       ,'End time:  ' ||
                        to_char(SYSDATE, 'DD-MON-YYYY HH24:MI:SS') ||
                        ' for ' || l_sec);
      fnd_file.put_line(fnd_file.output
                       ,'End time:  ' ||
                        to_char(SYSDATE, 'DD-MON-YYYY HH24:MI:SS') ||
                        ' for ' || l_sec);

    END;
    --
    --call to put comments in receipts that are not loading, status_flag = 'N'
    l_sec := 'Call Receipt excepetion - comments.';
    fnd_file.put_line(fnd_file.log
                     ,'Start time:  ' ||
                      to_char(SYSDATE, 'DD-MON-YYYY HH24:MI:SS') ||
                      ' for ' || l_sec);
    fnd_file.put_line(fnd_file.output
                     ,'Start time:  ' ||
                      to_char(SYSDATE, 'DD-MON-YYYY HH24:MI:SS') ||
                      ' for ' || l_sec);
    --
    submit_receipt_exceptions(l_err_msg
                             ,l_err_code
                             ,l_start_date
                             ,l_end_date
                             ,l_low
                             ,l_high
                             ,l_oracle_bu_nm --Version 1.2  p_bu_nm
                             ,l_user_id
                             ,l_responsibility);
    --
    fnd_file.put_line(fnd_file.log
                     ,'End time:  ' ||
                      to_char(SYSDATE, 'DD-MON-YYYY HH24:MI:SS') ||
                      ' for ' || l_sec);
    fnd_file.put_line(fnd_file.output
                     ,'End time:  ' ||
                      to_char(SYSDATE, 'DD-MON-YYYY HH24:MI:SS') ||
                      ' for ' || l_sec);

    --updating the batch status to know which batches will have Third Party Accrual run
    --batches must have a "CLOSED" status
    --Wait for Third Party Accrual to run to call FAE
    l_sec := 'Waiting for Third Party Accruals to complete';
    fnd_file.put_line(fnd_file.log, l_sec);
    fnd_file.put_line(fnd_file.output, l_sec);

    fnd_file.put_line(fnd_file.log
                     ,'Start time:  ' ||
                      to_char(SYSDATE, 'DD-MON-YYYY HH24:MI:SS') ||
                      ' for ' || l_sec);
    fnd_file.put_line(fnd_file.output
                     ,'Start time:  ' ||
                      to_char(SYSDATE, 'DD-MON-YYYY HH24:MI:SS') ||
                      ' for ' || l_sec);

    BEGIN

      FOR i IN 1 .. 60
      LOOP

        SELECT COUNT(*)
          INTO l_status
          FROM xxcus.xxcus_rebt_cust_tbl x, ozf_resale_batches_all b --Version 1.4 8/27/13 ignoring rejected batches
         WHERE bu_nm = l_oracle_bu_nm --Version 1.2   p_bu_nm
           AND x.resale_batch_id = b.resale_batch_id
           AND x.status_code = 'CLOSED'
           AND b.status_code <> 'REJECTED'
           AND nvl(request_code, 'XX') <> 'C'
           AND nvl(status_flag, 'X') <> 'P';

        IF l_status <> 0
        THEN
          dbms_lock.sleep(l_sleep);

          FOR c_tpa IN (SELECT request_id
                              ,to_number(argument1) argument1
                              ,phase_code
                          FROM applsys.fnd_concurrent_requests r
                         WHERE r.concurrent_program_id = l_conc_program --Third Party Accrual From Resale Table
                           AND to_number(r.argument1) IN
                               (SELECT resale_batch_id
                                  FROM xxcus.xxcus_rebt_cust_tbl b
                                 WHERE bu_nm = l_oracle_bu_nm --Version 1.2   p_bu_nm
                                   AND status_code = 'CLOSED'
                                   AND nvl(request_code, 'XX') <> 'C'
                                   AND nvl(status_flag, 'X') <> 'P'
                                   AND resale_batch_id =
                                       to_number(r.argument1)))
          LOOP

            IF c_tpa.phase_code <> 'C'
            THEN
              IF (c_tpa.request_id != 0)
              THEN
                IF fnd_concurrent.wait_for_request(c_tpa.request_id
                                                  ,v_interval
                                                  ,v_max_time
                                                  ,v_phase
                                                  ,v_status
                                                  ,v_dev_phase
                                                  ,v_dev_status
                                                  ,v_message)
                THEN
                  l_sec := 'ReqID=' || l_req_id || ' DPhase ' ||
                           v_dev_phase || ' DStatus ' || v_dev_status ||
                           chr(10) || ' MSG - ' || v_message;
                  IF v_dev_phase = 'COMPLETE' AND v_dev_status = 'NORMAL'
                  THEN
                    UPDATE xxcus.xxcus_rebt_cust_tbl
                       SET request_code = c_tpa.phase_code
                          ,request_id   = c_tpa.request_id
                     WHERE resale_batch_id = c_tpa.argument1
                       AND bu_nm = l_oracle_bu_nm; --Version 1.2   p_bu_nm
                  ELSE

                    fnd_file.put_line(fnd_file.log, l_sec);
                    fnd_file.put_line(fnd_file.output, l_sec);

                    UPDATE xxcus.xxcus_rebt_cust_tbl
                       SET request_code = c_tpa.phase_code
                          ,request_id   = c_tpa.request_id
                     WHERE resale_batch_id = c_tpa.argument1
                       AND bu_nm = l_oracle_bu_nm; --Version 1.2   p_bu_nm

                  END IF;
                END IF;
              END IF;

            ELSE
              UPDATE xxcus.xxcus_rebt_cust_tbl
                 SET request_code = c_tpa.phase_code
                    ,request_id   = c_tpa.request_id
               WHERE resale_batch_id = c_tpa.argument1
                 AND bu_nm = l_oracle_bu_nm; --Version 1.2   p_bu_nm
            END IF;

          END LOOP;
        END IF;
      END LOOP;
      fnd_file.put_line(fnd_file.log
                       ,'End time:  ' ||
                        to_char(SYSDATE, 'DD-MON-YYYY HH24:MI:SS') ||
                        ' for ' || l_sec);
      fnd_file.put_line(fnd_file.output
                       ,'End time:  ' ||
                        to_char(SYSDATE, 'DD-MON-YYYY HH24:MI:SS') ||
                        ' for ' || l_sec);
    END;

    l_sec := 'Finished';
    fnd_file.put_line(fnd_file.log, l_sec);
    fnd_file.put_line(fnd_file.output, l_sec);

    -- update audit table
    UPDATE xxcus.xxcus_rebate_aud_tbl
       SET stage = l_sec, stg_date = SYSDATE
     WHERE (conc_request_id = nvl(fnd_global.conc_request_id, 0) OR
           trunc(processed_datetime) = trunc(SYSDATE))
       AND procedure_name = l_procedure
       AND fiscal_period = p_fperiod
       AND post_run_id = l_runnum
       AND bu_nm = p_bu_nm;
    COMMIT;
    --
    --new check if receipt load is just weekly or for month end closing
    --Version 1.9 FAE 10/10/2013
    BEGIN
      --Version 2.6 12/29/2014
      IF l_wkly_req_set = 'Y'
      THEN

        l_sec   := 'Check to see if FAE can run after loading ' || p_bu_nm;
        l_count := NULL;

        --Version 1.12 determin the run number for the period
        BEGIN
          SELECT MAX(post_run_id)
            INTO l_max_runnum
            FROM xxcus.xxcus_rebate_aud_tbl a, apps.xxcus_rebate_bu_nm_v b
           WHERE procedure_name = l_procedure
             AND fiscal_period = p_fperiod
             AND a.bu_nm = b.bu_nm
             AND b.file_delivery = 'WEEKLY'
             AND processed_datetime between l_monthend and sysdate; --Version 2.6

          l_sec := 'Max Run Number for checking if Request Set should run:  ' ||
                   l_max_runnum;
          fnd_file.put_line(fnd_file.log, l_sec);
          fnd_file.put_line(fnd_file.output, l_sec);
        EXCEPTION
          WHEN no_data_found THEN
            l_runnum := 0;
            l_sec    := 'Max Run Number "no data found in audit table:  ' ||
                        l_max_runnum;
            fnd_file.put_line(fnd_file.log, l_sec);
            fnd_file.put_line(fnd_file.output, l_sec);
        END;

        -- check to see if this is the month end week and will need all BU before running FAE
        IF l_end_date BETWEEN l_monthend AND SYSDATE
        THEN

          BEGIN
            SELECT COUNT(*) INTO l_count FROM apps.xxcus_rebate_bu_nm_v;
          EXCEPTION
            WHEN no_data_found THEN
              l_count := 0;
          END;

          -- Call Funds Accrual Engine - xxcus_tm_misc_pkg.submit_fae
          -- check to see if all BU have run so FAE will run with distinct cust acct
          --Version 1.11  changeing for month end process to run only when last BU has run
          --
          SELECT SUM(bu)
            INTO l_fae
            FROM (SELECT COUNT(*) bu
                    FROM apps.xxcus_rebate_bu_nm_v b
                   WHERE file_delivery = 'WEEKLY'
                     AND b.bu_nm IN
                         (SELECT bu_nm
                            FROM xxcus.xxcus_rebate_aud_tbl a
                           WHERE a.bu_nm = b.bu_nm
                             AND a.fiscal_period = p_fperiod
                             AND a.stage = 'Finished'
                             AND a.procedure_name = l_procedure
                             AND a.post_run_id = l_max_runnum) --Version 1.12
                  UNION
                  SELECT COUNT(*) bu
                    FROM apps.xxcus_rebate_bu_nm_v b
                   WHERE file_delivery = 'MONTHLY'
                     AND b.bu_nm IN
                         (SELECT bu_nm
                            FROM xxcus.xxcus_rebate_aud_tbl a
                           WHERE a.bu_nm = b.bu_nm
                             AND a.fiscal_period = p_fperiod
                             AND a.stage = 'Finished'
                             AND a.procedure_name = l_procedure))
           WHERE 1 = 1;

          l_sec := 'Month end week:  BU to process = ' || l_count ||
                   ' BU finished in Audit table = ' || l_fae;
          fnd_file.put_line(fnd_file.log, l_sec);

          IF l_fae = l_count
          THEN

            --Version 1.11 change to run request set
            l_sec := 'Weekly run at month end - weekly request set XXCUS_TM_REBATES_WEEKLY.';
            fnd_file.put_line(fnd_file.log, l_sec);
            fnd_file.put_line(fnd_file.output, l_sec);
            --
            submit_request_set(l_err_msg, l_err_code, p_fperiod, l_user_id);
            --
          END IF;
          --check to see what BU's send files weekly and will run FAE if this is the last one coming in
        ELSE
          BEGIN
            SELECT COUNT(*)
              INTO l_count
              FROM apps.xxcus_rebate_bu_nm_v
             WHERE file_delivery = 'WEEKLY';
          EXCEPTION
            WHEN no_data_found THEN
              l_count := 0;
          END;

          SELECT COUNT(*)
            INTO l_fae
            FROM apps.xxcus_rebate_bu_nm_v b
           WHERE file_delivery = 'WEEKLY'
             AND b.bu_nm IN
                 (SELECT bu_nm
                    FROM xxcus.xxcus_rebate_aud_tbl a
                   WHERE a.bu_nm = b.bu_nm
                     AND a.fiscal_period = p_fperiod
                     AND a.stage = 'Finished'
                     AND a.procedure_name = l_procedure
                     AND nvl(a.post_run_id, l_runnum) = l_runnum);

          l_sec := 'Weekly:  BU to process = ' || l_count ||
                   ' BU finished in Audit table = ' || l_fae;
          fnd_file.put_line(fnd_file.log, l_sec);

          IF l_fae = l_count
          THEN

            --Version 1.11  change to run request set
            -- l_sec := 'Weekly run to submit Funds Accrual request-HDS Funds Accrual Engine All Vendors.';
            l_sec := 'Weekly run weekly request set XXCUS_TM_REBATES_WEEKLY.';
            fnd_file.put_line(fnd_file.log, l_sec);
            fnd_file.put_line(fnd_file.output, l_sec);
            --
            submit_request_set(l_err_msg, l_err_code, p_fperiod, l_user_id);
            --
          END IF;
        END IF;
      END IF; --Version 2.6 12/29/2014
    END;
    --10/10/13  FAE
    -- Begin Ver 3.1
      --
      -- begin rebates recon post processor to update control totals CT5 and CT6.
      --
      print_log (' ');
      print_log ('Begin recon_audit_pp to update control totals ct5 and ct6');
      recon_audit_pp
                    (
                      p_bu_nm              =>p_bu_nm
                     ,p_dw_btch_id     =>l_dw_btch_id
                     ,p_oracle_bu_nm =>l_oracle_bu_nm
                     ,p_fiscal_wk_start =>l_fiscal_week_start
                     ,p_fiscal_wk_end =>l_fiscal_week_end
                    );
      print_log ('End recon_audit_pp to update control totals ct5 and ct6');
      print_log (' ');
      --
      -- end rebates recon post processor to update control totals CT5 and CT6.
      --
    -- End Ver 3.1
    --need to truncate the partition for the DW staging table for the next run for the BU
    BEGIN

      l_sec := 'Truncate partition in xxcus_rebate_receipt_sdw_tbl table by BU ' ||
               p_bu_nm;
      fnd_file.put_line(fnd_file.log
                       ,'Start time:  ' ||
                        to_char(SYSDATE, 'DD-MON-YYYY HH24:MI:SS') ||
                        ' for ' || l_sec);
      fnd_file.put_line(fnd_file.output
                       ,'Start time:  ' ||
                        to_char(SYSDATE, 'DD-MON-YYYY HH24:MI:SS') ||
                        ' for ' || l_sec);

      EXECUTE IMMEDIATE 'alter table xxcus.xxcus_rebate_receipt_sdw_tbl truncate partition ' ||
                        l_partition_sdw;

      fnd_file.put_line(fnd_file.log
                       ,'End time:  ' ||
                        to_char(SYSDATE, 'DD-MON-YYYY HH24:MI:SS') ||
                        ' for ' || l_sec);
      fnd_file.put_line(fnd_file.output
                       ,'End time:  ' ||
                        to_char(SYSDATE, 'DD-MON-YYYY HH24:MI:SS') ||
                        ' for ' || l_sec);

    EXCEPTION
      WHEN OTHERS THEN
        l_err_msg := 'Failed to truncate partition in xxcus_rebate_receipt_sdw_tbl for  ' ||
                     l_partition_sdw || ' - ' || SQLERRM;
        fnd_file.put_line(fnd_file.log, l_err_msg);
        RAISE program_error;
    END;

  EXCEPTION
    WHEN program_error THEN
      ROLLBACK;
      fnd_file.put_line(fnd_file.log, l_sec);
      fnd_file.put_line(fnd_file.output, l_sec);
      l_err_code := 2;
      l_err_msg  := l_err_msg || ' Error_Stack...' ||
                    dbms_utility.format_error_stack() ||
                    ' Error_Backtrace...' ||
                    dbms_utility.format_error_backtrace();
      fnd_file.put_line(fnd_file.log, l_err_msg);
      fnd_file.put_line(fnd_file.output, l_err_msg);
      retcode         := l_err_code;
      errbuf          := l_err_msg;
      l_err_callpoint := l_sec;

      xxcus_error_pkg.xxcus_error_main_api(p_called_from       => l_err_callfrom
                                          ,p_calling           => l_err_callpoint
                                          ,p_request_id        => l_req_id
                                          ,p_ora_error_msg     => substr(SQLERRM ||
                                                                         regexp_replace(l_err_msg
                                                                                       ,'[[:cntrl:]]'
                                                                                       ,NULL)
                                                                        ,1
                                                                        ,2000)
                                          ,p_error_desc        => substr(l_err_msg
                                                                        ,1
                                                                        ,240)
                                          ,p_distribution_list => l_distro_list
                                          ,p_module            => 'TM');

    WHEN OTHERS THEN
      fnd_file.put_line(fnd_file.log, l_sec);
      fnd_file.put_line(fnd_file.output, l_sec);
      l_err_code := 2;
      l_err_msg  := l_err_msg || ' Error_Stack...' ||
                    dbms_utility.format_error_stack() ||
                    ' Error_Backtrace...' ||
                    dbms_utility.format_error_backtrace();
      fnd_file.put_line(fnd_file.log, l_err_msg);
      fnd_file.put_line(fnd_file.output, l_err_msg);
      retcode         := l_err_code;
      errbuf          := l_err_msg;
      l_err_callpoint := l_sec;

      xxcus_error_pkg.xxcus_error_main_api(p_called_from       => l_err_callfrom
                                          ,p_calling           => l_err_callpoint
                                          ,p_request_id        => l_req_id
                                          ,p_ora_error_msg     => substr(SQLERRM ||
                                                                         regexp_replace(l_err_msg
                                                                                       ,'[[:cntrl:]]'
                                                                                       ,NULL)
                                                                        ,1
                                                                        ,2000)
                                          ,p_error_desc        => substr(l_err_msg
                                                                        ,1
                                                                        ,240)
                                          ,p_distribution_list => l_distro_list
                                          ,p_module            => 'TM');

  END load_rebate_receipts;

  /*******************************************************************************
  * Procedure:   LOAD_UOM_MAPPING
  * Description: This is called from LOADING PRODUCTS and RECEIPTS.  Need to check
  *              for new or changed uom xref in APEX to insert or update mapping
  *              table
  HISTORY
  ===============================================================================
  VERSION DATE          AUTHOR(S)       DESCRIPTION
  ------- -----------   --------------- -----------------------------------------
  1.0     01/11/2013    Kathy Poling    Initial creation of the procedure
                                        RFC 36582
  1.1     09/05/2013    Kathy Poling    Adding Parm for BU
                                        RFC 38031
  ********************************************************************************/
  PROCEDURE load_uom_mapping(p_bu_nm IN VARCHAR2) IS

    --
    -- Package Variables
    --
    l_sec    VARCHAR2(255);
    l_count  NUMBER;
    l_req_id NUMBER NULL;

    -- Error DEBUG
    l_err_callfrom  VARCHAR2(75) DEFAULT 'XXCUS_TM_INTERFACE_PKG.LOAD_UOM_MAPPING';
    l_err_callpoint VARCHAR2(75) DEFAULT 'Loading UOM from Apex';
    l_distro_list   VARCHAR2(75) DEFAULT 'HDSOracleDevelopers@hdsupply.com';
    l_err_msg       VARCHAR2(3000);
    l_err_code      NUMBER;

  BEGIN

    l_sec := 'UOM Mapping for p_bu_nm ' || p_bu_nm;
    fnd_file.put_line(fnd_file.log, l_sec);
    fnd_file.put_line(fnd_file.log
                     ,'Start time:  ' ||
                      to_char(SYSDATE, 'DD-MON-YYYY HH24:MI:SS') ||
                      ' for ' || l_sec);
    fnd_file.put_line(fnd_file.output, l_sec);
    fnd_file.put_line(fnd_file.output
                     ,'Start time:  ' ||
                      to_char(SYSDATE, 'DD-MON-YYYY HH24:MI:SS') ||
                      ' for ' || l_sec);

    l_count := 0;

    BEGIN
      SELECT COUNT(*)
        INTO l_count
        FROM ea_apps.hds_rebate_uom_xref_tbl@apxprd_lnk -- Ver 3.7 TMS 20171128-00198 hdsoracle.hds_rebate_uom_xref_tbl@apxprd_lnk
       WHERE bu_nm = nvl(p_bu_nm, bu_nm); --Version 1.1  9/5/13

    EXCEPTION
      WHEN no_data_found THEN
        l_count := 0;
    END;

    IF l_count > 1
    THEN
      BEGIN
        l_sec := 'Loop to insert new UOM';
        --insert new mapping from apex
        FOR c_new IN (SELECT id
                            ,purch_uom_cd
                            ,lob_nm
                            ,bu_nm
                            ,src_sys_nm
                            ,oracle_uom_code
                        FROM ea_apps.hds_rebate_uom_xref_tbl@apxprd_lnk -- TMS 20171128-00198 hdsoracle.hds_rebate_uom_xref_tbl@apxprd_lnk
                       WHERE nvl(oracle_uom_code, 'XXXX') <> 'XXXX'
                         AND bu_nm = nvl(p_bu_nm, bu_nm) --Version 1.1  9/5/13
                      MINUS
                      SELECT id
                            ,purch_uom_cd
                            ,lob_nm
                            ,bu_nm
                            ,src_sys_nm
                            ,oracle_uom_code
                        FROM xxcus.xxcus_rebate_uom_tbl
                       WHERE bu_nm = nvl(p_bu_nm, bu_nm)) --Version 1.1   9/5/13
        LOOP

          INSERT INTO xxcus.xxcus_rebate_uom_tbl
            (id
            ,purch_uom_cd
            ,lob_nm
            ,bu_nm
            ,src_sys_nm
            ,oracle_uom_code
            ,creation_date
            ,updated_date)
          VALUES
            (c_new.id
            ,c_new.purch_uom_cd
            ,c_new.lob_nm
            ,c_new.bu_nm
            ,c_new.src_sys_nm
            ,c_new.oracle_uom_code
            ,SYSDATE
            ,SYSDATE);

          l_sec := 'Inserting UOM from Apex for ID:  ' || c_new.id;

        END LOOP;
        COMMIT;
      EXCEPTION
        WHEN OTHERS THEN

          ROLLBACK;
      END;

      l_sec := 'Checking for updates on oracle uom code';

      l_count := 0;
      --checking for updates
      BEGIN
        SELECT COUNT(*)
          INTO l_count
          FROM xxcus.xxcus_rebate_uom_tbl                   o
              ,ea_apps.hds_rebate_uom_xref_tbl@apxprd_lnk a  -- Ver 3.7 TMS 20171128-00198 hdsoracle.hds_rebate_uom_xref_tbl@apxprd_lnk a
         WHERE o.id = a.id
           AND o.lob_nm = a.lob_nm
           AND o.bu_nm = a.bu_nm
           AND o.src_sys_nm = a.src_sys_nm
           AND nvl(o.oracle_uom_code, 'XXXX') <> a.oracle_uom_code
           AND a.bu_nm = nvl(p_bu_nm, a.bu_nm); --Version 1.1   9/5/13

      EXCEPTION
        WHEN no_data_found THEN
          l_count := 0;
      END;

      IF l_count > 1
      THEN

        BEGIN
          FOR c_update IN (SELECT o.id
                                 ,o.lob_nm
                                 ,o.bu_nm
                                 ,o.src_sys_nm
                                 ,a.purch_uom_cd
                                 ,a.oracle_uom_code
                             FROM xxcus.xxcus_rebate_uom_tbl                   o
                                 ,ea_apps.hds_rebate_uom_xref_tbl@apxprd_lnk a -- Ver 3.7 TMS 20171128-00198 hdsoracle.hds_rebate_uom_xref_tbl@apxprd_lnk a
                            WHERE o.id = a.id
                              AND o.lob_nm = a.lob_nm
                              AND o.bu_nm = a.bu_nm
                              AND o.src_sys_nm = a.src_sys_nm
                              AND nvl(o.oracle_uom_code, 'XXXX') <>
                                  a.oracle_uom_code
                              AND a.bu_nm = nvl(p_bu_nm, a.bu_nm) --Version 1.1   9/5/13
                           )
          LOOP

            UPDATE xxcus.xxcus_rebate_uom_tbl
               SET oracle_uom_code = c_update.oracle_uom_code
                  ,updated_date    = SYSDATE
             WHERE id = c_update.id
               AND lob_nm = c_update.lob_nm
               AND bu_nm = c_update.bu_nm
               AND src_sys_nm = c_update.src_sys_nm;

          END LOOP;
        EXCEPTION
          WHEN OTHERS THEN
            ROLLBACK;
        END;
      END IF;

    END IF;
  EXCEPTION
    WHEN program_error THEN
      ROLLBACK;
      fnd_file.put_line(fnd_file.log, l_sec);
      fnd_file.put_line(fnd_file.output, l_sec);
      l_err_code := 2;
      l_err_msg  := l_err_msg || ' Error_Stack...' ||
                    dbms_utility.format_error_stack() ||
                    ' Error_Backtrace...' ||
                    dbms_utility.format_error_backtrace();
      fnd_file.put_line(fnd_file.log, l_err_msg);
      fnd_file.put_line(fnd_file.output, l_err_msg);
      l_err_callpoint := l_sec;

      xxcus_error_pkg.xxcus_error_main_api(p_called_from       => l_err_callfrom
                                          ,p_calling           => l_err_callpoint
                                          ,p_request_id        => l_req_id
                                          ,p_ora_error_msg     => substr(regexp_replace(l_err_msg
                                                                                       ,'[[:cntrl:]]'
                                                                                       ,NULL)
                                                                        ,1
                                                                        ,2000)
                                          ,p_error_desc        => substr(l_err_msg
                                                                        ,1
                                                                        ,240)
                                          ,p_distribution_list => l_distro_list
                                          ,p_module            => 'TM');

    WHEN OTHERS THEN
      fnd_file.put_line(fnd_file.log, l_sec);
      fnd_file.put_line(fnd_file.output, l_sec);
      l_err_code := 2;
      l_err_msg  := l_err_msg || ' Error_Stack...' ||
                    dbms_utility.format_error_stack() ||
                    ' Error_Backtrace...' ||
                    dbms_utility.format_error_backtrace();
      fnd_file.put_line(fnd_file.log, l_err_msg);
      fnd_file.put_line(fnd_file.output, l_err_msg);
      l_err_callpoint := l_sec;

      xxcus_error_pkg.xxcus_error_main_api(p_called_from       => l_err_callfrom
                                          ,p_calling           => l_err_callpoint
                                          ,p_request_id        => l_req_id
                                          ,p_ora_error_msg     => substr(regexp_replace(l_err_msg
                                                                                       ,'[[:cntrl:]]'
                                                                                       ,NULL)
                                                                        ,1
                                                                        ,2000)
                                          ,p_error_desc        => substr(l_err_msg
                                                                        ,1
                                                                        ,240)
                                          ,p_distribution_list => l_distro_list
                                          ,p_module            => 'TM');

  END load_uom_mapping;

  /*******************************************************************************
  * Procedure:   ITEM_CATEGORY_INT_DELETE
  * Description: This will be used to clear records that errored in the item and
  *              category interface during the product interface.  We phase 2 the
  *              interface now executes by BU Name.  We need the errored records
  *              to remain in the interface for a few days to allow reporting and
  *              research.
  HISTORY
  ===============================================================================
  VERSION DATE          AUTHOR(S)       DESCRIPTION
  ------- -----------   --------------- -----------------------------------------
  1.0     04/26/2013    Kathy Poling    Initial creation of the procedure
                                        SR 202000
  ********************************************************************************/
  PROCEDURE item_cat_iface_delete(errbuf OUT VARCHAR2, retcode OUT NUMBER) IS
    --
    -- Package Variables
    --
    l_err_msg   VARCHAR2(3000);
    l_err_code  NUMBER;
    l_sec       VARCHAR2(255);
    l_count     NUMBER := 0;
    l_org_id    NUMBER := 84;
    l_procedure VARCHAR2(50) := 'LOAD_REBATE_PRODUCT';
    l_user_id   fnd_user.user_id%TYPE; --REBTINTERFACE user
    l_user CONSTANT VARCHAR2(100) := 'REBTINTERFACE';
    l_responsibility VARCHAR2(100) := 'XXCUS_CON'; --'HDS Trade Management Administrator - Rebates'
    l_category_set CONSTANT mtl_category_sets_tl.category_set_id%TYPE := 1100000041; --HDS Supplier Category
    l_calendar     CONSTANT gl_periods.period_set_name%TYPE := '4-4-QTR';

    -- Error DEBUG
    l_err_callfrom  VARCHAR2(75) DEFAULT 'XXCUS_TM_INTERFACE_PKG.LOAD_REBATE_PRODUCT';
    l_err_callpoint VARCHAR2(75) DEFAULT 'START';
    l_distro_list   VARCHAR2(75) DEFAULT 'HDSOracleDevelopers@hdsupply.com';

  BEGIN
    --Clear contents of interface tables
    --delete errors from items and category interface
    l_sec := 'Delete BUs last run from item/category interface.';
    fnd_file.put_line(fnd_file.log
                     ,'Start time:  ' ||
                      to_char(SYSDATE, 'DD-MON-YYYY HH24:MI:SS') ||
                      ' for ' || l_sec);
    fnd_file.put_line(fnd_file.output
                     ,'Start time:  ' ||
                      to_char(SYSDATE, 'DD-MON-YYYY HH24:MI:SS') ||
                      ' for ' || l_sec);

    DELETE FROM inv.mtl_interface_errors e
     WHERE transaction_id IN (SELECT transaction_id
                                FROM inv.mtl_system_items_interface i
                               WHERE i.transaction_id = e.transaction_id
                                 AND i.organization_id = l_org_id
                                 AND i.process_flag = 3)
        OR transaction_id IN (SELECT transaction_id
                                FROM inv.mtl_item_categories_interface c
                               WHERE c.transaction_id = e.transaction_id
                                 AND c.organization_id = l_org_id
                                 AND c.process_flag = 3);

    --Version 1.3
    DELETE FROM inv.mtl_system_items_interface
     WHERE organization_id = l_org_id
       AND process_flag = 3;
    --Version 1.3
    DELETE FROM inv.mtl_item_categories_interface
     WHERE organization_id = l_org_id
       AND process_flag = 3;

  EXCEPTION
    WHEN program_error THEN
      ROLLBACK;
      fnd_file.put_line(fnd_file.log, l_sec);
      fnd_file.put_line(fnd_file.output, l_sec);
      l_err_code := 2;
      l_err_msg  := l_err_msg || ' Error_Stack...' ||
                    dbms_utility.format_error_stack() ||
                    ' Error_Backtrace...' ||
                    dbms_utility.format_error_backtrace();
      fnd_file.put_line(fnd_file.log, l_err_msg);
      fnd_file.put_line(fnd_file.output, l_err_msg);
      l_err_callpoint := l_sec;

      xxcus_error_pkg.xxcus_error_main_api(p_called_from       => l_err_callfrom
                                          ,p_calling           => l_err_callpoint
                                          ,p_request_id        => fnd_global.conc_request_id()
                                          ,p_ora_error_msg     => substr(regexp_replace(l_err_msg
                                                                                       ,'[[:cntrl:]]'
                                                                                       ,NULL)
                                                                        ,1
                                                                        ,2000)
                                          ,p_error_desc        => substr(l_err_msg
                                                                        ,1
                                                                        ,240)
                                          ,p_distribution_list => l_distro_list
                                          ,p_module            => 'TM');

    WHEN OTHERS THEN
      fnd_file.put_line(fnd_file.log, l_sec);
      fnd_file.put_line(fnd_file.output, l_sec);
      l_err_code := 2;
      l_err_msg  := l_err_msg || ' Error_Stack...' ||
                    dbms_utility.format_error_stack() ||
                    ' Error_Backtrace...' ||
                    dbms_utility.format_error_backtrace();
      fnd_file.put_line(fnd_file.log, l_err_msg);
      fnd_file.put_line(fnd_file.output, l_err_msg);
      l_err_callpoint := l_sec;

      xxcus_error_pkg.xxcus_error_main_api(p_called_from       => l_err_callfrom
                                          ,p_calling           => l_err_callpoint
                                          ,p_request_id        => fnd_global.conc_request_id()
                                          ,p_ora_error_msg     => substr(regexp_replace(l_err_msg
                                                                                       ,'[[:cntrl:]]'
                                                                                       ,NULL)
                                                                        ,1
                                                                        ,2000)
                                          ,p_error_desc        => substr(l_err_msg
                                                                        ,1
                                                                        ,240)
                                          ,p_distribution_list => l_distro_list
                                          ,p_module            => 'TM');

  END item_cat_iface_delete;

  /*******************************************************************************
  * Procedure:
  * Description: This is called from LOAD_REBATE_VENDOR for update party site uses
  *              and cust site use
  *
  HISTORY
  ===============================================================================
  VERSION DATE          AUTHOR(S)       DESCRIPTION
  ------- -----------   --------------- -----------------------------------------
  1.0     07/22/2010    Kathy Poling    Initial creation of the procedure SR 207447
                                        correcting issues with Party Site Use and
                                        Cust Site Use
  ********************************************************************************/

  PROCEDURE update_site_use(p_party_site_use_id IN NUMBER
                           ,p_status            IN VARCHAR2
                           ,p_party_site_id     IN NUMBER
                           ,p_party_site_number IN VARCHAR2) IS

    --
    -- Package Variables
    --
    l_sec                 VARCHAR2(500);
    l_primary_flag        VARCHAR2(1);
    l_req_id              NUMBER NULL;
    l_responsibility_id   NUMBER := 50622; --HDS Receivables Manager - Rebates USA
    l_resp_application_id NUMBER := 222; --AR
    l_user_id             NUMBER := 1211; --REBTINTERFACE

    -- Error DEBUG
    l_err_callfrom  VARCHAR2(75) DEFAULT 'XXCUS_TM_INTERFACE_PKG.UPDATE_SITE_USE';
    l_err_callpoint VARCHAR2(75) DEFAULT 'Updating site use status';
    l_distro_list   VARCHAR2(75) DEFAULT 'HDSOracleDevelopers@hdsupply.com';
    l_err_msg       VARCHAR2(3000);
    l_err_code      NUMBER;

    --API
    x_return_status         VARCHAR2(2000);
    x_msg_count             NUMBER;
    x_msg_data              VARCHAR2(2000);
    l_success               BOOLEAN DEFAULT TRUE;
    p_object_version_number NUMBER;

    --Party Site Use API
    p_party_site_use_rec hz_party_site_v2pub.party_site_use_rec_type;
    x_party_site_use_id  NUMBER;

    --Cust Site Use API
    p_cust_site_use_rec hz_cust_account_site_v2pub.cust_site_use_rec_type;
    x_site_use_id       NUMBER;

  BEGIN

    IF p_party_site_number LIKE '%~MSTR'
    THEN
      l_primary_flag := 'Y';
    ELSE
      l_primary_flag := 'N';
    END IF;

    ----changing status to A for MVID party site use
    x_return_status      := NULL;
    x_msg_count          := NULL;
    x_msg_data           := NULL;
    p_party_site_use_rec := NULL;
    p_cust_site_use_rec  := NULL;

    FOR r IN (SELECT *
                FROM ar.hz_party_site_uses
               WHERE party_site_use_id = p_party_site_use_id)
    LOOP

      p_object_version_number := r.object_version_number;

      p_party_site_use_rec.party_site_use_id := r.party_site_use_id;
      p_party_site_use_rec.site_use_type     := r.site_use_type;
      p_party_site_use_rec.party_site_id     := r.party_site_id;
      p_party_site_use_rec.primary_per_type  := l_primary_flag;
      p_party_site_use_rec.status            := p_status;
      p_party_site_use_rec.created_by_module := r.created_by_module;
      p_party_site_use_rec.application_id    := r.application_id;

    END LOOP;

    hz_party_site_v2pub.update_party_site_use('T'
                                             ,p_party_site_use_rec
                                             ,p_object_version_number
                                             ,x_return_status
                                             ,x_msg_count
                                             ,x_msg_data);
    COMMIT;

    l_sec := 'Update status for party site use id:  ' ||
             p_party_site_use_id || ' Status = ' || x_return_status;

    IF x_return_status = 'S'
    THEN

      fnd_file.put_line(fnd_file.log, l_sec);
    ELSE
      l_sec := 'ERROR update status - Party Site Use id:  ' ||
               p_party_site_use_id || ' Status = ' || x_return_status ||
               ' Reason = ' || x_msg_data;
      fnd_file.put_line(fnd_file.log, l_sec);
      fnd_file.put_line(fnd_file.output, l_sec);
    END IF;

    ----changing status to for MVID cust site use
    FOR c_use IN (SELECT site_use_id
                        ,hcsu.object_version_number
                        ,hcsu.orig_system_reference
                        ,hcsu.org_id
                        ,location
                    FROM ar.hz_cust_acct_sites_all hcas
                        ,ar.hz_cust_site_uses_all  hcsu
                   WHERE party_site_id = p_party_site_id
                     AND hcas.cust_acct_site_id = hcsu.cust_acct_site_id
                     AND site_use_code = 'BILL_TO'
                     AND hcsu.status <> p_status)
    LOOP

      --Version 1.5  primary flag change to the PayTo needs to stay on the MVID
      IF c_use.location LIKE '%~MSTR'
      THEN
        l_primary_flag := 'Y';
      ELSE
        l_primary_flag := 'N';
      END IF;

      x_return_status := NULL;
      x_msg_count     := NULL;
      x_msg_data      := NULL;

      --------------------------------------------------------------------------
      -- Apps Initialize
      --------------------------------------------------------------------------
      fnd_global.apps_initialize(l_user_id
                                ,l_responsibility_id
                                ,l_resp_application_id);

      mo_global.init('AR');
      mo_global.set_policy_context('S', c_use.org_id); --07/11/13
      fnd_request.set_org_id(fnd_global.org_id); --07/11/13

      FOR r IN (SELECT *
                  FROM hz_cust_site_uses_all
                 WHERE site_use_id = c_use.site_use_id)
      LOOP

        p_cust_site_use_rec.site_use_id                    := c_use.site_use_id;
        p_cust_site_use_rec.cust_acct_site_id              := r.cust_acct_site_id;
        p_cust_site_use_rec.site_use_code                  := r.site_use_code;
        p_cust_site_use_rec.primary_flag                   := l_primary_flag;
        p_cust_site_use_rec.status                         := p_status;
        p_cust_site_use_rec.location                       := r.location;
        p_cust_site_use_rec.contact_id                     := r.contact_id;
        p_cust_site_use_rec.bill_to_site_use_id            := r.bill_to_site_use_id;
        p_cust_site_use_rec.orig_system_reference          := r.orig_system_reference;
        p_cust_site_use_rec.sic_code                       := r.sic_code;
        p_cust_site_use_rec.payment_term_id                := r.payment_term_id;
        p_cust_site_use_rec.gsa_indicator                  := r.gsa_indicator;
        p_cust_site_use_rec.ship_partial                   := r.ship_partial;
        p_cust_site_use_rec.ship_via                       := r.ship_via;
        p_cust_site_use_rec.fob_point                      := r.fob_point;
        p_cust_site_use_rec.order_type_id                  := r.order_type_id;
        p_cust_site_use_rec.price_list_id                  := r.price_list_id;
        p_cust_site_use_rec.freight_term                   := r.freight_term;
        p_cust_site_use_rec.warehouse_id                   := r.warehouse_id;
        p_cust_site_use_rec.territory_id                   := r.territory_id;
        p_cust_site_use_rec.attribute_category             := r.attribute_category;
        p_cust_site_use_rec.attribute1                     := r.attribute1;
        p_cust_site_use_rec.attribute2                     := r.attribute2;
        p_cust_site_use_rec.attribute3                     := r.attribute3;
        p_cust_site_use_rec.attribute4                     := r.attribute4;
        p_cust_site_use_rec.attribute5                     := r.attribute5;
        p_cust_site_use_rec.attribute6                     := r.attribute6;
        p_cust_site_use_rec.attribute7                     := r.attribute7;
        p_cust_site_use_rec.attribute8                     := r.attribute8;
        p_cust_site_use_rec.attribute9                     := r.attribute9;
        p_cust_site_use_rec.attribute10                    := r.attribute10;
        p_cust_site_use_rec.tax_reference                  := r.tax_reference;
        p_cust_site_use_rec.sort_priority                  := r.sort_priority;
        p_cust_site_use_rec.tax_code                       := r.tax_code;
        p_cust_site_use_rec.attribute11                    := r.attribute11;
        p_cust_site_use_rec.attribute12                    := r.attribute12;
        p_cust_site_use_rec.attribute13                    := r.attribute13;
        p_cust_site_use_rec.attribute14                    := r.attribute14;
        p_cust_site_use_rec.attribute15                    := r.attribute15;
        p_cust_site_use_rec.attribute16                    := r.attribute16;
        p_cust_site_use_rec.attribute17                    := r.attribute17;
        p_cust_site_use_rec.attribute18                    := r.attribute18;
        p_cust_site_use_rec.attribute19                    := r.attribute19;
        p_cust_site_use_rec.attribute20                    := r.attribute20;
        p_cust_site_use_rec.attribute21                    := r.attribute21;
        p_cust_site_use_rec.attribute22                    := r.attribute22;
        p_cust_site_use_rec.attribute23                    := r.attribute23;
        p_cust_site_use_rec.attribute24                    := r.attribute24;
        p_cust_site_use_rec.attribute25                    := r.attribute25;
        p_cust_site_use_rec.demand_class_code              := r.demand_class_code;
        p_cust_site_use_rec.tax_header_level_flag          := r.tax_header_level_flag;
        p_cust_site_use_rec.tax_rounding_rule              := r.tax_rounding_rule;
        p_cust_site_use_rec.global_attribute1              := r.global_attribute1;
        p_cust_site_use_rec.global_attribute2              := r.global_attribute2;
        p_cust_site_use_rec.global_attribute3              := r.global_attribute3;
        p_cust_site_use_rec.global_attribute4              := r.global_attribute4;
        p_cust_site_use_rec.global_attribute5              := r.global_attribute5;
        p_cust_site_use_rec.global_attribute6              := r.global_attribute6;
        p_cust_site_use_rec.global_attribute7              := r.global_attribute7;
        p_cust_site_use_rec.global_attribute8              := r.global_attribute8;
        p_cust_site_use_rec.global_attribute9              := r.global_attribute9;
        p_cust_site_use_rec.global_attribute10             := r.global_attribute10;
        p_cust_site_use_rec.global_attribute11             := r.global_attribute11;
        p_cust_site_use_rec.global_attribute12             := r.global_attribute12;
        p_cust_site_use_rec.global_attribute13             := r.global_attribute13;
        p_cust_site_use_rec.global_attribute14             := r.global_attribute14;
        p_cust_site_use_rec.global_attribute15             := r.global_attribute15;
        p_cust_site_use_rec.global_attribute16             := r.global_attribute16;
        p_cust_site_use_rec.global_attribute17             := r.global_attribute17;
        p_cust_site_use_rec.global_attribute18             := r.global_attribute18;
        p_cust_site_use_rec.global_attribute19             := r.global_attribute19;
        p_cust_site_use_rec.global_attribute20             := r.global_attribute20;
        p_cust_site_use_rec.global_attribute_category      := r.global_attribute_category;
        p_cust_site_use_rec.primary_salesrep_id            := r.primary_salesrep_id;
        p_cust_site_use_rec.finchrg_receivables_trx_id     := r.finchrg_receivables_trx_id;
        p_cust_site_use_rec.dates_negative_tolerance       := r.dates_negative_tolerance;
        p_cust_site_use_rec.dates_positive_tolerance       := r.dates_positive_tolerance;
        p_cust_site_use_rec.date_type_preference           := r.date_type_preference;
        p_cust_site_use_rec.over_shipment_tolerance        := r.over_shipment_tolerance;
        p_cust_site_use_rec.under_shipment_tolerance       := r.under_shipment_tolerance;
        p_cust_site_use_rec.item_cross_ref_pref            := r.item_cross_ref_pref;
        p_cust_site_use_rec.over_return_tolerance          := r.over_return_tolerance;
        p_cust_site_use_rec.under_return_tolerance         := r.under_return_tolerance;
        p_cust_site_use_rec.ship_sets_include_lines_flag   := r.ship_sets_include_lines_flag;
        p_cust_site_use_rec.arrivalsets_include_lines_flag := r.arrivalsets_include_lines_flag;
        p_cust_site_use_rec.sched_date_push_flag           := r.sched_date_push_flag;
        p_cust_site_use_rec.invoice_quantity_rule          := r.invoice_quantity_rule;
        p_cust_site_use_rec.pricing_event                  := r.pricing_event;
        p_cust_site_use_rec.gl_id_rec                      := r.gl_id_rec;
        p_cust_site_use_rec.gl_id_rev                      := r.gl_id_rev;
        p_cust_site_use_rec.gl_id_tax                      := r.gl_id_tax;
        p_cust_site_use_rec.gl_id_freight                  := r.gl_id_freight;
        p_cust_site_use_rec.gl_id_clearing                 := r.gl_id_clearing;
        p_cust_site_use_rec.gl_id_unbilled                 := r.gl_id_unbilled;
        p_cust_site_use_rec.gl_id_unearned                 := r.gl_id_unearned;
        p_cust_site_use_rec.gl_id_unpaid_rec               := r.gl_id_unpaid_rec;
        p_cust_site_use_rec.gl_id_remittance               := r.gl_id_remittance;
        p_cust_site_use_rec.gl_id_factor                   := r.gl_id_factor;
        p_cust_site_use_rec.tax_classification             := r.tax_classification;
        p_cust_site_use_rec.created_by_module              := r.created_by_module;
        p_cust_site_use_rec.application_id                 := r.application_id;
        p_cust_site_use_rec.org_id                         := r.org_id;
        p_object_version_number                            := r.object_version_number;
      END LOOP;

      hz_cust_account_site_v2pub.update_cust_site_use('T'
                                                     ,p_cust_site_use_rec
                                                     ,p_object_version_number
                                                     ,x_return_status
                                                     ,x_msg_count
                                                     ,x_msg_data);
      COMMIT;

      IF x_return_status = 'S'
      THEN

        l_sec := 'Update status for site use id:  ' || c_use.site_use_id ||
                 ' Status = ' || x_return_status;
        fnd_file.put_line(fnd_file.output, l_sec);
      ELSE
        l_sec := 'ERROR update status - Site Use ID:  ' ||
                 c_use.site_use_id || ' Version = ' ||
                 p_object_version_number || ' Status = ' || x_return_status ||
                 ' Reason = ' || x_msg_data;
        fnd_file.put_line(fnd_file.log, l_sec);
        fnd_file.put_line(fnd_file.output, l_sec);
      END IF;

    END LOOP;

  EXCEPTION
    WHEN OTHERS THEN
      fnd_file.put_line(fnd_file.log, l_sec);
      fnd_file.put_line(fnd_file.output, l_sec);
      l_err_code := 2;
      l_err_msg  := l_err_msg || ' Error_Stack...' ||
                    dbms_utility.format_error_stack() ||
                    ' Error_Backtrace...' ||
                    dbms_utility.format_error_backtrace();
      fnd_file.put_line(fnd_file.log, l_err_msg);
      fnd_file.put_line(fnd_file.output, l_err_msg);
      l_err_callpoint := l_sec;

      xxcus_error_pkg.xxcus_error_main_api(p_called_from       => l_err_callfrom
                                          ,p_calling           => l_err_callpoint
                                          ,p_request_id        => l_req_id
                                          ,p_ora_error_msg     => substr(regexp_replace(l_err_msg
                                                                                       ,'[[:cntrl:]]'
                                                                                       ,NULL)
                                                                        ,1
                                                                        ,2000)
                                          ,p_error_desc        => substr(l_err_msg
                                                                        ,1
                                                                        ,240)
                                          ,p_distribution_list => l_distro_list
                                          ,p_module            => 'TM');

  END update_site_use;

  /*******************************************************************************
  * Procedure:   GET_BRANCH
  * Description: This is called from receipt interface to get branch
  *
  HISTORY
  ===============================================================================
  VERSION DATE          AUTHOR(S)       DESCRIPTION
  ------- -----------   --------------- -----------------------------------------
  1.0     09/27/2013    Kathy Poling    Initial creation of the procedure
                                        SR
  ********************************************************************************/

  FUNCTION get_branch(p_bu_nm    IN VARCHAR2
                     ,p_brnch_cd IN VARCHAR2
                     ,p_lob_nm   IN VARCHAR2
                      --,x_cust_acct OUT NUMBER
                      ) RETURN NUMBER IS

    l_brnch_attr VARCHAR2(30) := 'HDS_LOB_BRANCH';
    l_len_brnch  VARCHAR2(20);
    l_brnch_cd   VARCHAR2(20);
    l_cust_id    NUMBER;

    CURSOR chk_len_brnch(v_brnch_cd IN VARCHAR2) IS
      SELECT length(v_brnch_cd) FROM dual;

    CURSOR get_branch(p_bu_nm IN VARCHAR, p_brnch_cd IN VARCHAR2) IS
      SELECT cust_account_id
        FROM apps.xxcus_rebate_branch_vw, ar.hz_cust_accounts hca
       WHERE dw_bu_desc = p_bu_nm
         AND oper_branch = p_brnch_cd
         AND hca.customer_type = 'I'
         AND hca.attribute1 = l_brnch_attr
         AND hca.status = 'A'
         AND hca.account_number =
             p_lob_nm || '.' || rebates_fru || '.' || lob_branch;

    CURSOR get_default(p_bu_nm IN VARCHAR) IS
      SELECT cust_account_id
        FROM ar.hz_cust_accounts hca
       WHERE hca.customer_type = 'I'
         AND hca.attribute1 = l_brnch_attr
         AND hca.status = 'A'
         AND hca.account_number = p_lob_nm || '.' || 'DRPSHP';

  BEGIN
    BEGIN
      OPEN chk_len_brnch(p_brnch_cd);
      FETCH chk_len_brnch
        INTO l_len_brnch;
      CLOSE chk_len_brnch;
    EXCEPTION
      WHEN OTHERS THEN
        fnd_file.put_line(fnd_file.log
                         ,'@get_branch.chk_len_brnch, p_brnch_cd =' ||
                          p_brnch_cd || ', MSG =' || SQLERRM);
    END;
    --
    BEGIN
      OPEN get_branch(p_bu_nm, p_brnch_cd);
      FETCH get_branch
        INTO l_cust_id;
      CLOSE get_branch;
    EXCEPTION
      WHEN OTHERS THEN
        fnd_file.put_line(fnd_file.log
                         ,'@get_branch.get_branch-1, p_bu_nm =' || p_bu_nm ||
                          ',p_brnch_cd =' || p_brnch_cd || ', MSG =' ||
                          SQLERRM);
    END;
    --
    IF p_bu_nm = 'WATERWORKS' OR p_bu_nm = 'WHITECAP CONSTRUCTION'
    THEN

      IF l_len_brnch < '3'
      THEN

        l_brnch_cd := lpad(p_brnch_cd, 3, '0');

      ELSE

        l_brnch_cd := p_brnch_cd;

      END IF;

    ELSE

      l_brnch_cd := p_brnch_cd;

    END IF;
    --
    BEGIN
      OPEN get_branch(p_bu_nm, l_brnch_cd);
      FETCH get_branch
        INTO l_cust_id;
      CLOSE get_branch;
    EXCEPTION
      WHEN OTHERS THEN
        fnd_file.put_line(fnd_file.log
                         ,'@get_branch.get_branch-2, p_bu_nm =' || p_bu_nm ||
                          ',p_brnch_cd =' || l_brnch_cd || ', MSG =' ||
                          SQLERRM);
    END;
    --
    IF l_cust_id IS NULL
    THEN
      --
      BEGIN
        OPEN get_default(p_bu_nm);
        FETCH get_default
          INTO l_cust_id;
        CLOSE get_default;
      EXCEPTION
        WHEN OTHERS THEN
          fnd_file.put_line(fnd_file.log
                           ,'@get_branch.get_default, p_bu_nm =' ||
                            p_bu_nm || ', MSG =' || SQLERRM);
      END;
    END IF;
    --
    RETURN l_cust_id;
    --
  EXCEPTION
    WHEN OTHERS THEN
      fnd_file.put_line(fnd_file.log
                       ,'@get_branch.outer block, p_bu_nm =' || p_bu_nm ||
                        ', p_brnch_cd =' || p_brnch_cd || ',p_lob_nm =' ||
                        p_lob_nm);
      fnd_file.put_line(fnd_file.log
                       ,'@get_branch.outer block, Msg =' || SQLERRM);
  END get_branch;

  /*******************************************************************************
  * Procedure:   GET_OFFER_TYPE
  * Description: This is called from receipt interface to get volume offers for
  *              running FAE
  HISTORY
  ===============================================================================
  VERSION DATE          AUTHOR(S)       DESCRIPTION
  ------- -----------   --------------- -----------------------------------------
  1.0     09/27/2013    Kathy Poling    Initial creation of the procedure
                                        SR
  ********************************************************************************/

  FUNCTION get_offer_type(p_cust_acct_id IN NUMBER) RETURN VARCHAR2 IS
    l_offer_type VARCHAR2(30);
  BEGIN
    SELECT DISTINCT offer_type
      INTO l_offer_type
      FROM ozf.ozf_offers oo, ozf.ozf_offr_market_options mo
     WHERE offer_type = 'VOLUME_OFFER'
       AND status_code = 'ACTIVE'
       AND oo.qp_list_header_id = mo.qp_list_header_id
       AND mo.beneficiary_party_id = p_cust_acct_id;

    RETURN l_offer_type;
  EXCEPTION
    WHEN OTHERS THEN
      RETURN NULL;
  END get_offer_type;

  /*******************************************************************************
  * Procedure:   SUBMIT_RECEIPT_EXCEPTIONS
  * Description: This is for the concurrent request HDS Rebate Receipt Exceptions
  *              Process will update the the comments for unprocessed receipts
  *
  HISTORY
  ===============================================================================
  VERSION DATE          AUTHOR(S)       DESCRIPTION
  ------- -----------   --------------- -----------------------------------------
  1.0     10/30/2013    Kathy Poling    Initial creation of the procedure
                                        RFC 36582
  ********************************************************************************/

  PROCEDURE submit_receipt_exceptions(errbuf           OUT VARCHAR2
                                     ,retcode          OUT NUMBER
                                     ,p_start_date     IN DATE
                                     ,p_end_date       IN DATE
                                     ,p_low            IN NUMBER
                                     ,p_high           IN NUMBER
                                     ,p_bu_nm          IN VARCHAR2
                                     ,p_user           IN NUMBER
                                     ,p_responsibility IN VARCHAR2) IS
    --
    -- Package Variables
    --
    l_req_id             NUMBER NULL;
    v_phase              VARCHAR2(50);
    v_status             VARCHAR2(50);
    v_dev_status         VARCHAR2(50);
    v_dev_phase          VARCHAR2(50);
    v_message            VARCHAR2(250);
    v_error_message      VARCHAR2(3000);
    v_interval           NUMBER := 30; -- In seconds
    v_max_time           NUMBER := 1500; -- In seconds
    l_err_msg            VARCHAR2(3000);
    l_err_code           NUMBER;
    l_sec                VARCHAR2(255);
    l_statement          VARCHAR2(9000);
    l_program            VARCHAR2(30) DEFAULT 'XXCUSOZF_REBATES_RECPT_EXCPT';
    l_app                VARCHAR2(30) DEFAULT 'XXCUS';
    l_responsibility     NUMBER;
    l_user               VARCHAR2(100);
    l_user_id            NUMBER;
    l_can_submit_request BOOLEAN := TRUE;

    -- Error DEBUG
    l_err_callfrom  VARCHAR2(75) DEFAULT 'XXCUS_TM_INTERFACE_PKG.SUBMIT_RECEIPT_EXCEPTIONS';
    l_err_callpoint VARCHAR2(75) DEFAULT 'START';
    l_distro_list   VARCHAR2(75) DEFAULT 'HDSOracleDevelopers@hdsupply.com';

  BEGIN

    l_sec := 'Call to run concurrent request HDS Rebate Receipt Exceptions.';
    fnd_file.put_line(fnd_file.log
                     ,'Start time:  ' ||
                      to_char(SYSDATE, 'DD-MON-YYYY HH24:MI:SS') ||
                      ' for ' || l_sec);
    fnd_file.put_line(fnd_file.output
                     ,'Start time:  ' ||
                      to_char(SYSDATE, 'DD-MON-YYYY HH24:MI:SS') ||
                      ' for ' || l_sec);

    fnd_file.put_line(fnd_file.output
                     ,'*---------------------------------------------------------------------------------------------*');

    fnd_file.put_line(fnd_file.output
                     ,rpad('Receipt start date', 40, ' ') || ': ' ||
                      p_start_date);
    fnd_file.put_line(fnd_file.output
                     ,rpad('Receipt end date', 40, ' ') || ': ' ||
                      p_end_date);
    fnd_file.put_line(fnd_file.output
                     ,rpad('Low amount', 40, ' ') || ': ' || p_low);
    fnd_file.put_line(fnd_file.output
                     ,rpad('High amount', 40, ' ') || ': ' || p_high);
    fnd_file.put_line(fnd_file.output
                     ,rpad('BU Name', 40, ' ') || ': ' || p_bu_nm);

    fnd_file.put_line(fnd_file.log
                     ,'*---------------------------------------------------------------------------------------------*');

    fnd_file.put_line(fnd_file.log
                     ,rpad('Receipt start date', 40, ' ') || ': ' ||
                      p_start_date);
    fnd_file.put_line(fnd_file.log
                     ,rpad('Receipt end date', 40, ' ') || ': ' ||
                      p_end_date);
    fnd_file.put_line(fnd_file.log
                     ,rpad('Low amount', 40, ' ') || ': ' || p_low);
    fnd_file.put_line(fnd_file.log
                     ,rpad('High amount', 40, ' ') || ': ' || p_high);
    fnd_file.put_line(fnd_file.log
                     ,rpad('BU Name', 40, ' ') || ': ' || p_bu_nm);

    --------------------------------------------------------------------------
    -- Apps Initialize
    --------------------------------------------------------------------------
    l_sec := 'Setting global variables';

    BEGIN
      SELECT user_name INTO l_user FROM fnd_user WHERE user_id = p_user;

      fnd_file.put_line(fnd_file.log, 'User Name:  ' || l_user);
    EXCEPTION
      WHEN no_data_found THEN
        l_err_msg := 'Failed to find user: ' || SQLERRM;
        fnd_file.put_line(fnd_file.log, l_err_msg);
        RAISE program_error;
    END;

    --  Setup parameters for running FND JOBS!
    l_can_submit_request := xxcus_misc_pkg.set_responsibility(l_user
                                                             ,p_responsibility);

    IF l_can_submit_request
    THEN
      l_sec := 'Global Variables are set User Name:  ' || l_user ||
               ' Resp Name:  ' || p_responsibility;
      fnd_file.put_line(fnd_file.log, l_sec);
      fnd_file.put_line(fnd_file.output, l_sec);

    ELSE

      l_sec := 'Global Variables are not set for User Name:  ' || l_user ||
               ' Resp Name:  ' || p_responsibility;
      fnd_file.put_line(fnd_file.log, l_sec);
      fnd_file.put_line(fnd_file.output, l_sec);
      RAISE program_error;
    END IF;

    --------------------------------------------------------------------------
    -- Submit "HDS Rebate Receipt Exceptions"
    --------------------------------------------------------------------------
    l_req_id := fnd_request.submit_request(l_app
                                          ,l_program
                                          ,NULL
                                          ,NULL
                                          ,FALSE
                                          ,p_start_date
                                          ,p_end_date
                                          ,p_low
                                          ,p_high
                                          ,p_bu_nm);

    COMMIT;

    fnd_file.put_line(fnd_file.output, 'Request ID:  ' || l_req_id);
    fnd_file.put_line(fnd_file.log, 'Request ID:  ' || l_req_id);

    fnd_file.put_line(fnd_file.log
                     ,'*---------------------------------------------------------------------------------------------*');
    fnd_file.put_line(fnd_file.log
                     ,'End time:  ' ||
                      to_char(SYSDATE, 'DD-MON-YYYY HH24:MI:SS'));

    fnd_file.put_line(fnd_file.output
                     ,'*---------------------------------------------------------------------------------------------*');
    fnd_file.put_line(fnd_file.output
                     ,'End time:  ' ||
                      to_char(SYSDATE, 'DD-MON-YYYY HH24:MI:SS'));

    dbms_output.put_line(l_sec);
    dbms_output.put_line('Request ID:  ' || l_req_id);

  EXCEPTION
    WHEN program_error THEN
      ROLLBACK;
      l_err_code := 2;
      l_err_msg  := l_err_msg || ' Error_Stack...' ||
                    dbms_utility.format_error_stack() ||
                    ' Error_Backtrace...' ||
                    dbms_utility.format_error_backtrace();
      dbms_output.put_line(l_err_msg);
      fnd_file.put_line(fnd_file.log, l_err_msg);
      fnd_file.put_line(fnd_file.output, l_err_msg);
      l_err_callpoint := l_sec;

      xxcus_error_pkg.xxcus_error_main_api(p_called_from       => l_err_callfrom
                                          ,p_calling           => l_err_callpoint
                                          ,p_request_id        => l_req_id
                                          ,p_ora_error_msg     => substr(regexp_replace(l_err_msg
                                                                                       ,'[[:cntrl:]]'
                                                                                       ,NULL)
                                                                        ,1
                                                                        ,2000)
                                          ,p_error_desc        => substr(l_err_msg
                                                                        ,1
                                                                        ,240)
                                          ,p_distribution_list => l_distro_list
                                          ,p_module            => 'TM');

    WHEN OTHERS THEN
      fnd_file.put_line(fnd_file.log, l_sec);
      fnd_file.put_line(fnd_file.output, l_sec);
      l_err_code := 2;
      l_err_msg  := l_err_msg || ' Error_Stack...' ||
                    dbms_utility.format_error_stack() ||
                    ' Error_Backtrace...' ||
                    dbms_utility.format_error_backtrace();
      dbms_output.put_line(l_err_msg);
      fnd_file.put_line(fnd_file.log, l_err_msg);
      fnd_file.put_line(fnd_file.output, l_err_msg);
      l_err_callpoint := l_sec;

      xxcus_error_pkg.xxcus_error_main_api(p_called_from       => l_err_callfrom
                                          ,p_calling           => l_err_callpoint
                                          ,p_request_id        => l_req_id
                                          ,p_ora_error_msg     => substr(regexp_replace(l_err_msg
                                                                                       ,'[[:cntrl:]]'
                                                                                       ,NULL)
                                                                        ,1
                                                                        ,2000)
                                          ,p_error_desc        => substr(l_err_msg
                                                                        ,1
                                                                        ,240)
                                          ,p_distribution_list => l_distro_list
                                          ,p_module            => 'TM');

  END submit_receipt_exceptions;

  /*******************************************************************************
  * Procedure:   PULL_SDW
  * Description: This process will pull Receipts/Products from SDW by BU and
  *              call the uc4 procedure to start loading into Oracle
  *
  HISTORY
  ===============================================================================
  VERSION DATE          AUTHOR(S)       DESCRIPTION
  ------- -----------   --------------- ----------------------------------------
  1.0     27/30/2013    Kathy Poling    Initial creation of the procedure

  ********************************************************************************/

  PROCEDURE pull_sdw(errbuf    OUT VARCHAR2
                    ,retcode   OUT NUMBER
                    ,p_load    IN VARCHAR2
                    ,p_fperiod IN VARCHAR2
                    ,p_bu_nm   IN VARCHAR2) IS
    --
    -- Package Variables
    --
    l_req_id             NUMBER NULL;
    v_phase              VARCHAR2(50);
    v_status             VARCHAR2(50);
    v_dev_status         VARCHAR2(50);
    v_dev_phase          VARCHAR2(50);
    v_message            VARCHAR2(250);
    v_error_message      VARCHAR2(3000);
    v_interval           NUMBER := 30; -- In seconds
    v_max_time           NUMBER := 1500; -- In seconds
    l_can_submit_request BOOLEAN := TRUE;
    l_err_msg            VARCHAR2(3000);
    l_err_code           NUMBER;
    l_sec                VARCHAR2(255);
    l_statement          VARCHAR2(9000);
    l_partition          VARCHAR2(30);
    l_program            VARCHAR2(30);
    l_oracle_bu          VARCHAR2(50);
    l_app_name           VARCHAR2(30) := 'XXCUS';
    l_count              NUMBER;
    l_status             VARCHAR2(1); --Version 1.2
    l_request_running    NUMBER; --Version 1.2
    l_sleep              NUMBER := 60; --Version 1.2
    l_req_id_wait        NUMBER NULL; --Version 1.2
    l_partition_sdw      VARCHAR2(30);
    l_user_id            fnd_user.user_id%TYPE; --REBTINTERFACE user
    l_user CONSTANT VARCHAR2(100) := 'REBTINTERFACE';
    l_responsibility VARCHAR2(100) := 'XXCUS_CON'; --'HDS Trade Management Administrator - Rebates'

    -- Error DEBUG
    l_err_callfrom  VARCHAR2(75) DEFAULT 'XXCUS_TM_INTERFACE_PKG.PULL_SDW';
    l_err_callpoint VARCHAR2(75) DEFAULT 'START';
    l_distro_list   VARCHAR2(75) DEFAULT 'HDSOracleDevelopers@hdsupply.com';

  BEGIN

    BEGIN

      --lookup for mapping of bu name to the partition that needs truncate prior to loading receipts
      --to the table for comparison
      SELECT meaning
        INTO l_partition_sdw
        FROM fnd_lookup_values_vl
       WHERE lookup_type = 'XXCUS_REBATES_TABLE_PARTITION'
         AND description = p_bu_nm
         AND lookup_code LIKE 'SDW.%'
         AND enabled_flag = 'Y'
         AND nvl(end_date_active, SYSDATE) >= SYSDATE;
      fnd_file.put_line(fnd_file.log
                       ,'lookup partition for receipt:  ' ||
                        l_partition_sdw);
    EXCEPTION
      WHEN no_data_found THEN
        l_partition := 'OTHER';
        fnd_file.put_line(fnd_file.log
                         ,'lookup partition for receipt:  ' ||
                          l_partition_sdw);
    END;

    IF p_load = 'PRODUCT'
    THEN

      --product
      l_sec := 'Truncate partition in xxcus_rebate_product_sdw_tbl table by BU ' ||
               p_bu_nm;
      fnd_file.put_line(fnd_file.log
                       ,'Start time:  ' ||
                        to_char(SYSDATE, 'DD-MON-YYYY HH24:MI:SS') ||
                        ' for ' || l_sec);
      fnd_file.put_line(fnd_file.output
                       ,'Start time:  ' ||
                        to_char(SYSDATE, 'DD-MON-YYYY HH24:MI:SS') ||
                        ' for ' || l_sec);

      EXECUTE IMMEDIATE 'alter table xxcus.xxcus_rebate_product_sdw_tbl truncate partition ' ||
                        l_partition_sdw;

      fnd_file.put_line(fnd_file.log
                       ,'End time:  ' ||
                        to_char(SYSDATE, 'DD-MON-YYYY HH24:MI:SS') ||
                        ' for ' || l_sec);
      fnd_file.put_line(fnd_file.output
                       ,'End time:  ' ||
                        to_char(SYSDATE, 'DD-MON-YYYY HH24:MI:SS') ||
                        ' for ' || l_sec);

      INSERT /*+ APPEND */
      INTO xxcus.xxcus_rebate_product_sdw_tbl
        (SELECT geo_loc_hier_lob_nm
               ,bu_nm
               ,src_sys_nm
               ,prod_id
               ,sku_cd
               ,sku_desc_lng
               ,vndr_part_nbr
               ,prod_purch_uom
               ,po_repl_cost
               ,prim_upc
               ,secondary_upc
               ,tertiary_upc
               ,unspsc_cd
               ,as_of_dt
           FROM edw_common.edw_rebt_prod_mv@dw_edw_lnk
          WHERE bu_nm = p_bu_nm);
      COMMIT;

      xxcus_tm_interface_pkg.uc4_products(l_err_msg
                                         ,l_err_code
                                         ,p_fperiod
                                         ,p_bu_nm);

    ELSE

      --receipt
      l_sec := 'Truncate partition in xxcus_rebate_receipt_sdw_tbl table by BU ' ||
               p_bu_nm;
      fnd_file.put_line(fnd_file.log
                       ,'Start time:  ' ||
                        to_char(SYSDATE, 'DD-MON-YYYY HH24:MI:SS') ||
                        ' for ' || l_sec);
      fnd_file.put_line(fnd_file.output
                       ,'Start time:  ' ||
                        to_char(SYSDATE, 'DD-MON-YYYY HH24:MI:SS') ||
                        ' for ' || l_sec);

      EXECUTE IMMEDIATE 'alter table xxcus.xxcus_rebate_receipt_sdw_tbl truncate partition ' ||
                        l_partition_sdw;

      fnd_file.put_line(fnd_file.log
                       ,'End time:  ' ||
                        to_char(SYSDATE, 'DD-MON-YYYY HH24:MI:SS') ||
                        ' for ' || l_sec);
      fnd_file.put_line(fnd_file.output
                       ,'End time:  ' ||
                        to_char(SYSDATE, 'DD-MON-YYYY HH24:MI:SS') ||
                        ' for ' || l_sec);

      INSERT /*+ APPEND */
      INTO xxcus.xxcus_rebate_receipt_sdw_tbl
        (SELECT geo_loc_hier_lob_nm
               ,src_sys_nm
               ,bu_nm
               ,bu_cd
               ,fiscal_per_id
               ,vndr_recpt_unq_nbr
               ,pt_vndr_id
               ,pt_vndr_cd
               ,sf_vndr_id
               ,sf_vndr_cd
               ,rebt_vndr_id
               ,rebt_vndr_cd
               ,rebt_vndr_nm
               ,prod_id
               ,sku_cd
               ,vndr_part_nbr
               ,part_desc
               ,shp_to_brnch_id
               ,shp_to_brnch_cd
               ,shp_to_brnch_nm
               ,po_nbr
               ,po_crt_dt
               ,recpt_crt_dt
               ,drop_shp_flg
               ,prod_uom_cd
               ,item_cost_amt
               ,curnc_flg
               ,recpt_qty
               ,vndr_shp_qty
               ,buy_pkg_unt_cnt
               ,as_of_dt
           FROM edw_common.edw_rebt_vndr_recpt_mv@dw_edw_lnk
          WHERE bu_nm = p_bu_nm);
      COMMIT;

      xxcus_tm_interface_pkg.uc4_receipts(l_err_msg
                                         ,l_err_code
                                         ,p_fperiod
                                         ,p_bu_nm);

    END IF;

  EXCEPTION
    WHEN program_error THEN
      ROLLBACK;
      l_err_code := 2;
      l_err_msg  := l_err_msg || ' Error_Stack...' ||
                    dbms_utility.format_error_stack() ||
                    ' Error_Backtrace...' ||
                    dbms_utility.format_error_backtrace();
      dbms_output.put_line(l_err_msg);
      fnd_file.put_line(fnd_file.log, l_err_msg);
      fnd_file.put_line(fnd_file.output, l_err_msg);
      l_err_callpoint := l_sec;

      xxcus_error_pkg.xxcus_error_main_api(p_called_from       => l_err_callfrom
                                          ,p_calling           => l_err_callpoint
                                          ,p_request_id        => l_req_id
                                          ,p_ora_error_msg     => substr(regexp_replace(l_err_msg
                                                                                       ,'[[:cntrl:]]'
                                                                                       ,NULL)
                                                                        ,1
                                                                        ,2000)
                                          ,p_error_desc        => substr(l_err_msg
                                                                        ,1
                                                                        ,240)
                                          ,p_distribution_list => l_distro_list
                                          ,p_module            => 'TM');

    WHEN OTHERS THEN
      fnd_file.put_line(fnd_file.log, l_sec);
      fnd_file.put_line(fnd_file.output, l_sec);
      l_err_code := 2;
      l_err_msg  := l_err_msg || ' Error_Stack...' ||
                    dbms_utility.format_error_stack() ||
                    ' Error_Backtrace...' ||
                    dbms_utility.format_error_backtrace();
      dbms_output.put_line(l_err_msg);
      fnd_file.put_line(fnd_file.log, l_err_msg);
      fnd_file.put_line(fnd_file.output, l_err_msg);
      l_err_callpoint := l_sec;

      xxcus_error_pkg.xxcus_error_main_api(p_called_from       => l_err_callfrom
                                          ,p_calling           => l_err_callpoint
                                          ,p_request_id        => l_req_id
                                          ,p_ora_error_msg     => substr(regexp_replace(l_err_msg
                                                                                       ,'[[:cntrl:]]'
                                                                                       ,NULL)
                                                                        ,1
                                                                        ,2000)
                                          ,p_error_desc        => substr(l_err_msg
                                                                        ,1
                                                                        ,240)
                                          ,p_distribution_list => l_distro_list
                                          ,p_module            => 'TM');
  END pull_sdw;
  --
  /*******************************************************************************
  * Procedure:   wait_for_create_acctng
  * Description: This process will check if any existing rebates create accounting is still running
  *             if found, we will introduce a delay, go back and check again.
  * This will make sure we are not kicking off TM SLA extract or TM GL extract as the data is not ready yet
  *
  HISTORY
  ===============================================================================
  VERSION DATE          AUTHOR(S)           DESCRIPTION
  ------- -----------   ---------------     ----------------------------------------
  1.0     10/15/2014    Balaguru Seshadri   Initial creation of the procedure

  ********************************************************************************/
  FUNCTION wait_for_create_acctng RETURN BOOLEAN IS
    --
    b_check           BOOLEAN := TRUE;
    n_total_requests  NUMBER := 0;
    n_seconds_to_wait NUMBER := 120; --delay by 2 mins before checking again
    --
  BEGIN
    --
    WHILE (b_check)
    LOOP
      --
      BEGIN
        --
        SELECT COUNT(fcr.request_id)
          INTO n_total_requests
          FROM fnd_concurrent_requests fcr
         WHERE 1 = 1
           AND EXISTS
         (SELECT 1
                  FROM fnd_concurrent_programs
                 WHERE 1 = 1
                   AND concurrent_program_id = fcr.concurrent_program_id
                   AND concurrent_program_name = 'XLAACCPB' --Create Accounting
                )
           AND fcr.phase_code = 'R' --Check only running requests
           AND fcr.argument1 = '682' --Trade Management application id
           AND fcr.argument2 = '682' --Trade Management application id
           AND fcr.argument4 IN ('2021', '2023'); --2021 for HDS Rebates USA and 2023 for HDS Rebates CAD ledgers
        --
        IF (n_total_requests > 0)
        THEN
          -- need to set the delay here
          dbms_lock.sleep(n_seconds_to_wait); --What this means is we are going to wait for n seconds and check again
        ELSE
          --
          b_check := FALSE;
          EXIT;
          --
        END IF;
        --
      EXCEPTION
        WHEN OTHERS THEN
          b_check := FALSE;
      END;
      --
    END LOOP;
    --
    RETURN TRUE;
  EXCEPTION
    WHEN OTHERS THEN
      fnd_file.put_line(fnd_file.log
                       ,'@wait_for_create_acctng, message =' || SQLERRM);
      RETURN FALSE;
  END;
  --
  /*******************************************************************************
  * Procedure:   rebates_check_sla_excep
  * Description: This process will check if any existing rebates create accounting is still running
  *             if found, we will introduce a delay, go back and check again.
  * This will make sure we are not kicking off TM SLA extract or TM GL extract as the data is not ready yet
  *
  HISTORY
  ===============================================================================
  VERSION DATE          AUTHOR(S)           DESCRIPTION
  ------- -----------   ---------------     ----------------------------------------
  1.0     10/15/2014    Balaguru Seshadri   Initial creation of the procedure

  ********************************************************************************/
  --
  PROCEDURE sla_exceptions(retcode         OUT VARCHAR2
                          ,errbuf          OUT VARCHAR2
                          ,p_fiscal_period IN VARCHAR2) IS
    --
    n_req_id NUMBER := nvl(fnd_global.conc_request_id, -1);
    --
    l_request_id           NUMBER := 0;
    n_total_us_exceptions  NUMBER := 0;
    n_total_cad_exceptions NUMBER := 0;
    p_fiscal_year          NUMBER := NULL;
    v_curent_db_name       VARCHAR2(20) := 'NA';
    --v_prod_db_name         VARCHAR2(20) := 'EBIZPRD'; --3.0
    v_prod_db_name         VARCHAR2(20) := 'EBSPRD'; --3.0
    v_prod_rbt_email_id    VARCHAR2(40) := 'hdsoraclerebates@hdsupply.com';
    --
    d_from_date DATE := NULL;
    d_to_date   DATE := NULL;
    --
    l_err_msg  VARCHAR2(3000);
    l_err_code NUMBER;
    l_sec      VARCHAR2(255);

    l_ok       BOOLEAN := TRUE;
    l_success  NUMBER := 0;
    l_sysdate  DATE;
    l_end_date VARCHAR2(30);
    --
    l_gl_post              VARCHAR2(3);
    l_gl_post_cd           VARCHAR2(1);
    l_gl_trnsfr            VARCHAR2(3);
    l_gl_trnsfr_cd         VARCHAR2(1);
    l_report_level_meaning VARCHAR2(15);
    l_report_level         VARCHAR2(1);
    l_mode_meaning         VARCHAR2(15);
    l_mode                 VARCHAR2(1);
    --
    n_idx         NUMBER := 0;
    v_email       VARCHAR2(60) := NULL;
    ln_request_id NUMBER := 0;
    n_us_count    NUMBER := 0;
    n_cad_count   NUMBER := 0;
    --
    srs_failed        EXCEPTION;
    submitprog_failed EXCEPTION;
    submitset_failed  EXCEPTION;
    --
    file_id utl_file.file_type;
    c_maxline CONSTANT PLS_INTEGER := 32767;
    v_crlf      VARCHAR2(2) := chr(13); --||chr(10);
    v_file_path VARCHAR2(60) := NULL;
    v_file_name VARCHAR2(60) := NULL;
    --
    l_err_callfrom  VARCHAR2(75) DEFAULT 'XXCUS_TM_INTERFACE_PKG.sla_exceptions';
    l_err_callpoint VARCHAR2(75) DEFAULT 'START';
    l_distro_list   VARCHAR2(75) DEFAULT 'HDSOracleDevelopers@hdsupply.com';
    --
    CURSOR get_util_info IS
      SELECT /*+ index(sla_excep xxcus_ozf_sla_exceptions_n1) */
       ofu.cust_account_id      cust_account_id
      ,ofu.plan_id              plan_id
      ,sla_excep.utilization_id utilization_id
      ,sla_excep.event_id       event_id
      ,hzca.account_number      mvid
      ,hzp.party_name           mvid_name
        FROM xxcus.xxcus_ozf_sla_exceptions sla_excep
            ,ozf_funds_utilized_all_b       ofu
            ,hz_cust_accounts               hzca
            ,hz_parties                     hzp
       WHERE 1 = 1
         AND sla_excep.utilization_id = ofu.utilization_id
         AND hzca.cust_account_id = ofu.cust_account_id
         AND hzp.party_id = hzca.party_id;
    --
    CURSOR sla_error_detail IS
      SELECT 'REQUEST_ID|LEDGER_NAME|LEDGER_CURRENCY|PERIOD_NAME|USER_JE_SOURCE|EVENT_CLASS_CODE|USER_JE_CATEGORY_NAME|EVENT_DATE|EVENT_ID|EVENT_NUMBER|ENCODED_MESSAGE|UTILIZATION_ID|TRANSACTION_DATE|EVENT_TYPE_CODE|STATUS|STATUS_CODE|MVID_NAME|OFFER_CODE|OFFER_NAME|PRODUCT|ACCRUAL_AMOUNT|MVID|CUST_ACCOUNT_ID|LIST_HEADER_ID|' hds_rbt_exceptions
        FROM dual
      UNION ALL
      SELECT request_id || '|' || ledger_name || '|' || ledger_currency || '|' ||
             period_name || '|' || user_je_source || '|' ||
             event_class_code || '|' || user_je_category_name || '|' ||
             to_char(event_date, 'MM/DD/YYYY') || '|' || event_id || '|' ||
             event_number || '|' || encoded_message || '|' ||
             utilization_id || '|' ||
             to_char(transaction_date, 'MM/DD/YYYY') || '|' ||
             event_type_code || '|' || status || '|' || status_code || '|' ||
             mvid_name || '|' || offer_code || '|' || offer_name || '|' ||
             product || '|' || accrual_amount || '|' || mvid || '|' ||
             cust_account_id || '|' || list_header_id || '|'
        FROM xxcus.xxcus_ozf_sla_exceptions
       WHERE 1 = 1
         AND request_id = n_req_id;
    --
    CURSOR sla_error_summary IS
      SELECT encoded_message msg
        FROM xxcus.xxcus_ozf_sla_exceptions
       WHERE request_id = n_req_id
       GROUP BY encoded_message;
    --
    --
  BEGIN
    --
    -- Get fiscal year from the parameter p_fiscal_period
    --
    BEGIN
      SELECT regexp_substr(p_fiscal_period, '([[:digit:]]*$)')
        INTO p_fiscal_year
        FROM dual;
      --
      print_log('Current Fiscal Year: ' || p_fiscal_year);
      --
    EXCEPTION
      WHEN OTHERS THEN
        p_fiscal_year := NULL;
        fnd_file.put_line(fnd_file.log
                         ,'When other errors in fetching p_fiscal_year, message =' ||
                          SQLERRM);
    END;
    --
    IF (p_fiscal_year IS NOT NULL)
    THEN
      --
      EXECUTE IMMEDIATE 'TRUNCATE TABLE XXCUS.XXCUS_OZF_SLA_EXCEPTIONS';
      --
      -- As per Rebates IT team, the subleger period close exception report could contain records backdated to the beginning
      -- of the fiscal year. So we are going to take the year portion of the parameter p_fiscal_period and run the below query
      -- to derive the start of the fiscal year.
      --
      BEGIN
        SELECT MIN(start_date), MAX(end_date)
          INTO d_from_date, d_to_date
          FROM gl_periods
         WHERE 1 = 1
           AND period_set_name = '4-4-QTR'
           AND period_year = p_fiscal_year
           AND adjustment_period_flag = 'N'
           AND start_date <= trunc(SYSDATE);
        --
        print_log('Current Fiscal year start date: ' ||
                  to_char(d_from_date, 'mm/dd/yyyy'));
        --
        print_log('Current Fiscal month end date: ' ||
                  to_char(d_to_date, 'mm/dd/yyyy'));
        --
      EXCEPTION
        WHEN no_data_found THEN
          d_from_date := NULL;
          d_to_date   := NULL;
        WHEN OTHERS THEN
          fnd_file.put_line(fnd_file.log
                           ,'When other errors in fetching gl from and to dates for fiscal period =' ||
                            p_fiscal_period);
          fnd_file.put_line(fnd_file.log
                           ,'When other errors in fetching gl from and to dates, message =' ||
                            SQLERRM);
      END;
      --
      -- Insert all Rebates US SLA period close exception records
      --
      BEGIN
        --
        INSERT /*+ APPEND */
        INTO xxcus.xxcus_ozf_sla_exceptions t
          (SELECT n_req_id request_id
                 ,ent.ledger_id ledger_id
                 ,gld.name ledger_name
                 ,gld.currency_code ledger_currency
                 ,gps.period_name period_name
                 ,gjt.user_je_source_name user_je_source
                 ,xcl.event_class_code event_class_code
                 ,gjct.user_je_category_name user_je_category_name
                 ,to_char(xle.event_date, 'MM-DD-YYYY') event_date
                 ,xle.event_id event_id
                 ,xle.event_number event_number
                 ,xlaerr.encoded_msg encoded_message
                 ,ent.source_id_int_1 utilization_id
                 ,to_char(xle.transaction_date, 'MM-DD-YYYY') transaction_date
                 ,xle.on_hold_flag on_hold
                 ,xtt.event_type_code event_type_code
                 ,xlo1.meaning status
                 ,xle.event_status_code status_code
                 ,NULL customer_name
                 ,NULL offer_code
                 ,NULL offer_name
                 ,NULL product
                 ,0 accrual_amount
                 ,NULL mvid
                 ,NULL cust_account_id
                 ,NULL list_header_id
             FROM xla.xla_events               xle
                 ,xla.xla_transaction_entities ent
                 ,xla.xla_subledgers           xls
                  --          ,fnd_user                                fnu
                 ,gl_period_statuses        gps
                 ,gl_ledgers                gld
                 ,gl_je_sources_tl          gjt
                 ,gl_je_categories_tl       gjct
                 ,xla.xla_event_classes_tl  xcl
                 ,xla.xla_event_types_b     xet
                 ,xla.xla_event_types_tl    xtt
                 ,xla.xla_event_class_attrs xeca
                 ,xla_lookups               xlo1
                 ,xla.xla_ledger_options    xlp
                 ,xla_accounting_errors     xlaerr
            WHERE xls.application_id = xle.application_id
              AND xle.event_status_code IN ('I', 'U')
              AND xle.process_status_code IN ('U', 'D', 'E', 'R', 'I')
              AND xle.entity_id = ent.entity_id
              AND ent.application_id = xle.application_id
              AND ent.ledger_id = gps.ledger_id
              AND gps.application_id = 101
              AND xle.event_date BETWEEN gps.start_date AND gps.end_date
              AND gps.adjustment_period_flag <> 'Y' -- Added by krsankar for bug 8212297
              AND gld.ledger_id = ent.ledger_id
              AND gjt.je_source_name = xls.je_source_name
              AND gjt.language = userenv('LANG')
              AND xet.application_id = xle.application_id
              AND xet.event_type_code = xle.event_type_code
              AND xtt.application_id = xet.application_id
              AND xtt.event_type_code = xet.event_type_code
              AND xtt.event_class_code = xet.event_class_code
              AND xtt.entity_code = xet.entity_code
              AND xtt.language = userenv('LANG')
              AND xcl.application_id = xet.application_id
              AND xcl.entity_code = xet.entity_code
              AND xcl.event_class_code = xet.event_class_code
              AND xcl.application_id = ent.application_id
              AND xcl.entity_code = ent.entity_code
              AND xcl.language = userenv('LANG')
              AND xeca.application_id = xcl.application_id
              AND xeca.entity_code = xcl.entity_code
              AND xeca.event_class_code = xcl.event_class_code
              AND xeca.je_category_name = gjct.je_category_name
              AND gjct.language = userenv('LANG')
              AND xlo1.lookup_type = 'XLA_EVENT_STATUS'
              AND xlo1.lookup_code = xle.event_status_code
              AND ent.ledger_id = xlp.ledger_id
              AND ent.application_id = xlp.application_id
              AND xlp.capture_event_flag = 'Y'
              AND NOT EXISTS
            (SELECT aeh.event_id
                     FROM xla_ae_headers aeh
                    WHERE aeh.application_id = xle.application_id
                      AND aeh.event_id = xle.event_id)
              AND ent.ledger_id = 2021 --Rebates US ledger
              AND xle.application_id = 682
              AND xle.event_date BETWEEN d_from_date AND d_to_date --'04-AUG-14' AND '31-AUG-14'
              AND xls.je_source_name = 'Marketing'
              AND xlaerr.event_id(+) = xle.event_id
           UNION
           SELECT /*+ leading(aeh) */
            n_req_id request_id
          ,aeh.ledger_id ledger_id
          ,gld.name ledger_name
          ,gld.currency_code ledger_currency
          ,gps.period_name period_name
          ,gjt.user_je_source_name user_je_source
          ,xcl.event_class_code event_class_code
          ,gjct.user_je_category_name user_je_category_name
          ,to_char(aeh.accounting_date, 'MM-DD-YYYY') event_date
          ,xle.event_id event_id
          ,xle.event_number event_number
          ,xlaerr.encoded_msg encoded_message
          ,ent.source_id_int_1 utilization_id
          ,to_char(xle.transaction_date, 'MM-DD-YYYY') transaction_date
          ,xle.on_hold_flag on_hold
          ,xet.event_type_code event_type_code
          ,xlo4.meaning status
          ,aeh.accounting_entry_status_code status_code
          ,NULL customer_name
          ,NULL offer_code
          ,NULL offer_name
          ,NULL product
          ,0 accrual_amount
          ,NULL mvid
          ,NULL cust_account_id
          ,NULL list_header_id
             FROM xla.xla_ae_headers           aeh
                 ,xla.xla_events               xle
                 ,xla.xla_transaction_entities ent
                 ,xla.xla_subledgers           xls
                  --         ,fnd_user                           fnu
                 ,gl_period_statuses       gps
                 ,gl_ledgers               gld
                 ,gl_je_sources_tl         gjt
                 ,gl_je_categories_tl      gjct
                 ,xla.xla_event_types_b    xet
                 ,xla.xla_event_types_tl   xtt
                 ,xla.xla_event_classes_tl xcl
                 ,xla_lookups              xlo4
                 ,xla_accounting_errors    xlaerr
            WHERE xls.application_id = aeh.application_id
              AND aeh.event_id = xle.event_id
              AND aeh.application_id = xle.application_id
              AND xle.entity_id = ent.entity_id
              AND xle.application_id = ent.application_id
              AND aeh.period_name = gps.period_name
              AND aeh.ledger_id = gps.ledger_id
              AND gps.application_id = 101
              AND gld.ledger_id = aeh.ledger_id
              AND gjt.je_source_name = xls.je_source_name
              AND gjt.language = userenv('LANG')
              AND xet.application_id = xle.application_id
              AND xet.event_type_code = aeh.event_type_code
              AND xtt.application_id = xet.application_id
              AND xtt.event_type_code = xet.event_type_code
              AND xtt.entity_code = xet.entity_code
              AND xtt.event_class_code = xet.event_class_code
              AND xtt.language = userenv('LANG')
              AND xcl.application_id = xtt.application_id
              AND xcl.entity_code = xtt.entity_code
              AND xcl.event_class_code = xtt.event_class_code
              AND xcl.application_id = ent.application_id
              AND xcl.entity_code = ent.entity_code
              AND xcl.language = userenv('LANG')
              AND gjct.je_category_name = aeh.je_category_name
              AND gjct.language = userenv('LANG')
              AND aeh.gl_transfer_status_code IN ('N', 'E')
              AND xlo4.lookup_type = 'XLA_ACCOUNTING_ENTRY_STATUS'
              AND xlo4.lookup_code = aeh.accounting_entry_status_code
              AND aeh.ledger_id = 2021 --Rebates US ledger
              AND aeh.application_id = 682
              AND aeh.accounting_date BETWEEN d_from_date AND d_to_date --'04-AUG-14' AND '31-AUG-14'
              AND xls.je_source_name = 'Marketing'
              AND xlaerr.event_id(+) = xle.event_id);
        --
        n_total_us_exceptions := SQL%ROWCOUNT;
        --
        fnd_file.put_line(fnd_file.log
                         ,'@Rebates SLA US Exceptions, Total records :' ||
                          n_total_us_exceptions);
        --
        COMMIT;
        --
      EXCEPTION
        WHEN OTHERS THEN
          fnd_file.put_line(fnd_file.log
                           ,'@SLA US Exceptions Bulk Insert: Error :' ||
                            SQLERRM);
      END;
      --
      -- Insert all Rebates CAD SLA period close exception records
      --
      BEGIN
        --
        INSERT /*+ APPEND */
        INTO xxcus.xxcus_ozf_sla_exceptions t
          (SELECT n_req_id request_id
                 ,ent.ledger_id ledger_id
                 ,gld.name ledger_name
                 ,gld.currency_code ledger_currency
                 ,gps.period_name period_name
                 ,gjt.user_je_source_name user_je_source
                 ,xcl.event_class_code event_class_code
                 ,gjct.user_je_category_name user_je_category_name
                 ,to_char(xle.event_date, 'MM-DD-YYYY') event_date
                 ,xle.event_id event_id
                 ,xle.event_number event_number
                 ,xlaerr.encoded_msg encoded_message
                 ,ent.source_id_int_1 utilization_id
                 ,to_char(xle.transaction_date, 'MM-DD-YYYY') transaction_date
                 ,xle.on_hold_flag on_hold
                 ,xtt.event_type_code event_type_code
                 ,xlo1.meaning status
                 ,xle.event_status_code status_code
                 ,NULL mvid_name
                 ,NULL offer_code
                 ,NULL offer_name
                 ,NULL product
                 ,0 accrual_amount
                 ,NULL mvid
                 ,NULL cust_account_id
                 ,NULL list_header_id
             FROM xla.xla_events               xle
                 ,xla.xla_transaction_entities ent
                 ,xla.xla_subledgers           xls
                  --          ,fnd_user                                fnu
                 ,gl_period_statuses        gps
                 ,gl_ledgers                gld
                 ,gl_je_sources_tl          gjt
                 ,gl_je_categories_tl       gjct
                 ,xla.xla_event_classes_tl  xcl
                 ,xla.xla_event_types_b     xet
                 ,xla.xla_event_types_tl    xtt
                 ,xla.xla_event_class_attrs xeca
                 ,xla_lookups               xlo1
                 ,xla.xla_ledger_options    xlp
                 ,xla_accounting_errors     xlaerr
            WHERE xls.application_id = xle.application_id
              AND xle.event_status_code IN ('I', 'U')
              AND xle.process_status_code IN ('U', 'D', 'E', 'R', 'I')
              AND xle.entity_id = ent.entity_id
              AND ent.application_id = xle.application_id
              AND ent.ledger_id = gps.ledger_id
              AND gps.application_id = 101
              AND xle.event_date BETWEEN gps.start_date AND gps.end_date
              AND gps.adjustment_period_flag <> 'Y' -- Added by krsankar for bug 8212297
              AND gld.ledger_id = ent.ledger_id
              AND gjt.je_source_name = xls.je_source_name
              AND gjt.language = userenv('LANG')
              AND xet.application_id = xle.application_id
              AND xet.event_type_code = xle.event_type_code
              AND xtt.application_id = xet.application_id
              AND xtt.event_type_code = xet.event_type_code
              AND xtt.event_class_code = xet.event_class_code
              AND xtt.entity_code = xet.entity_code
              AND xtt.language = userenv('LANG')
              AND xcl.application_id = xet.application_id
              AND xcl.entity_code = xet.entity_code
              AND xcl.event_class_code = xet.event_class_code
              AND xcl.application_id = ent.application_id
              AND xcl.entity_code = ent.entity_code
              AND xcl.language = userenv('LANG')
              AND xeca.application_id = xcl.application_id
              AND xeca.entity_code = xcl.entity_code
              AND xeca.event_class_code = xcl.event_class_code
              AND xeca.je_category_name = gjct.je_category_name
              AND gjct.language = userenv('LANG')
              AND xlo1.lookup_type = 'XLA_EVENT_STATUS'
              AND xlo1.lookup_code = xle.event_status_code
              AND ent.ledger_id = xlp.ledger_id
              AND ent.application_id = xlp.application_id
              AND xlp.capture_event_flag = 'Y'
              AND NOT EXISTS
            (SELECT aeh.event_id
                     FROM xla_ae_headers aeh
                    WHERE aeh.application_id = xle.application_id
                      AND aeh.event_id = xle.event_id)
              AND ent.ledger_id = 2023 --Rebates CAD ledger
              AND xle.application_id = 682
              AND xle.event_date BETWEEN d_from_date AND d_to_date --'04-AUG-14' AND '31-AUG-14'
              AND xls.je_source_name = 'Marketing'
              AND xlaerr.event_id(+) = xle.event_id
           UNION
           SELECT /*+ leading(aeh) */
            n_req_id request_id
          ,aeh.ledger_id ledger_id
          ,gld.name ledger_name
          ,gld.currency_code ledger_currency
          ,gps.period_name period_name
          ,gjt.user_je_source_name user_je_source
          ,xcl.event_class_code event_class_code
          ,gjct.user_je_category_name user_je_category_name
          ,to_char(aeh.accounting_date, 'MM-DD-YYYY') event_date
          ,xle.event_id event_id
          ,xle.event_number event_number
          ,xlaerr.encoded_msg encoded_message
          ,ent.source_id_int_1 utilization_id
          ,to_char(xle.transaction_date, 'MM-DD-YYYY') transaction_date
          ,xle.on_hold_flag on_hold
          ,xet.event_type_code event_type_code
          ,xlo4.meaning status
          ,aeh.accounting_entry_status_code status_code
          ,NULL mvid_name
          ,NULL offer_code
          ,NULL offer_name
          ,NULL product
          ,0 accrual_amount
          ,NULL mvid
          ,NULL cust_account_id
          ,NULL list_header_id
             FROM xla.xla_ae_headers           aeh
                 ,xla.xla_events               xle
                 ,xla.xla_transaction_entities ent
                 ,xla.xla_subledgers           xls
                  --         ,fnd_user                           fnu
                 ,gl_period_statuses       gps
                 ,gl_ledgers               gld
                 ,gl_je_sources_tl         gjt
                 ,gl_je_categories_tl      gjct
                 ,xla.xla_event_types_b    xet
                 ,xla.xla_event_types_tl   xtt
                 ,xla.xla_event_classes_tl xcl
                 ,xla_lookups              xlo4
                 ,xla_accounting_errors    xlaerr
            WHERE xls.application_id = aeh.application_id
              AND aeh.event_id = xle.event_id
              AND aeh.application_id = xle.application_id
              AND xle.entity_id = ent.entity_id
              AND xle.application_id = ent.application_id
              AND aeh.period_name = gps.period_name
              AND aeh.ledger_id = gps.ledger_id
              AND gps.application_id = 101
              AND gld.ledger_id = aeh.ledger_id
              AND gjt.je_source_name = xls.je_source_name
              AND gjt.language = userenv('LANG')
              AND xet.application_id = xle.application_id
              AND xet.event_type_code = aeh.event_type_code
              AND xtt.application_id = xet.application_id
              AND xtt.event_type_code = xet.event_type_code
              AND xtt.entity_code = xet.entity_code
              AND xtt.event_class_code = xet.event_class_code
              AND xtt.language = userenv('LANG')
              AND xcl.application_id = xtt.application_id
              AND xcl.entity_code = xtt.entity_code
              AND xcl.event_class_code = xtt.event_class_code
              AND xcl.application_id = ent.application_id
              AND xcl.entity_code = ent.entity_code
              AND xcl.language = userenv('LANG')
              AND gjct.je_category_name = aeh.je_category_name
              AND gjct.language = userenv('LANG')
              AND aeh.gl_transfer_status_code IN ('N', 'E')
              AND xlo4.lookup_type = 'XLA_ACCOUNTING_ENTRY_STATUS'
              AND xlo4.lookup_code = aeh.accounting_entry_status_code
              AND aeh.ledger_id = 2023 --Rebates CAD ledger
              AND aeh.application_id = 682
              AND aeh.accounting_date BETWEEN d_from_date AND d_to_date --'04-AUG-14' AND '31-AUG-14'
              AND xls.je_source_name = 'Marketing'
              AND xlaerr.event_id(+) = xle.event_id);
        --
        n_total_cad_exceptions := SQL%ROWCOUNT;
        --
        fnd_file.put_line(fnd_file.log
                         ,'@Rebates SLA CAD Exceptions, Total records :' ||
                          n_total_cad_exceptions);
        --
        COMMIT;
        --
      EXCEPTION
        WHEN OTHERS THEN
          fnd_file.put_line(fnd_file.log
                           ,'@SLA CAD Exceptions Bulk Insert: Error :' ||
                            SQLERRM);
      END;
      --
      EXECUTE IMMEDIATE 'ALTER INDEX XXCUS.XXCUS_OZF_SLA_EXCEPTIONS_N1 REBUILD';
      --
      EXECUTE IMMEDIATE 'ALTER INDEX XXCUS.XXCUS_OZF_SLA_EXCEPTIONS_N2 REBUILD';
      --
      EXECUTE IMMEDIATE 'ALTER INDEX XXCUS.XXCUS_OZF_SLA_EXCEPTIONS_N3 REBUILD';
      --
      BEGIN
        --
        FOR util_rec IN get_util_info
        --
        LOOP
          --
          BEGIN
            --
            SAVEPOINT init_here;
            --
            UPDATE xxcus.xxcus_ozf_sla_exceptions
               SET cust_account_id = util_rec.cust_account_id
                  ,list_header_id  = util_rec.plan_id
                  ,mvid            = util_rec.mvid
                  ,mvid_name       = util_rec.mvid_name
             WHERE 1 = 1
               AND utilization_id = util_rec.utilization_id;
            --
          EXCEPTION
            WHEN OTHERS THEN
              ROLLBACK TO init_here;
              fnd_file.put_line(fnd_file.log
                               ,'@SLA exceptions table, failed to update attributes for utilization_id =' ||
                                util_rec.utilization_id || ', event_id =' ||
                                util_rec.event_id);
          END;
          --
        END LOOP;
        --
        COMMIT;
        --
      EXCEPTION
        WHEN OTHERS THEN
          fnd_file.put_line(fnd_file.log
                           ,'@update of mvid /mvid name, error =' ||
                            SQLERRM);
          ROLLBACK;
      END;
      --
      --
      BEGIN
        SAVEPOINT start_here;
        MERGE INTO xxcus.xxcus_ozf_sla_exceptions t
        USING (SELECT NAME           offer_code
                     ,description    offer_name
                     ,list_header_id plan_id
                 FROM qp_list_headers_vl
                WHERE 1 = 1) s
        ON (t.list_header_id = s.plan_id)
        WHEN MATCHED THEN
          UPDATE
             SET t.offer_code = s.offer_code, t.offer_name = s.offer_name;
      EXCEPTION
        WHEN OTHERS THEN
          ROLLBACK TO start_here;
          fnd_file.put_line(fnd_file.log
                           ,'@update of mvid /mvid name, error =' ||
                            SQLERRM);
      END;
      --
      -- generate a flat file with all the records so GSC IT rebates team can troubleshoot.
      --
      BEGIN
        --
        SELECT '/xx_iface/' || lower(NAME) || '/outbound'
          INTO v_file_path
          FROM v$database;
        --
      EXCEPTION
        WHEN OTHERS THEN
          print_log('@get file path, message =' || SQLERRM);
      END;
      /*
        --
        begin
         --
         v_file_name :='HDS_RBT_SLA_ERROR_DETAILS_'||n_req_id||'.txt';
         --
         file_id :=utl_file.fopen(v_file_path, v_file_name, 'W', C_MAXLINE);
         --
         for rec in sla_error_detail
          loop
           --
           utl_file.put_line(file_id, rec.hds_rbt_exceptions);
           --
          end loop;
         --
         utl_file.fclose(file_id);
         --
        exception
         when others then
          print_log('Issue in creating Rebates SLA Exceptions detail file for US and CAD, Error :'||sqlerrm);
        end;
        --
      */
      /*
        begin
         --
         v_file_name :='HDS_RBT_SLA_ERRORS_SUMMARY_'||n_req_id||'.txt';
         --
         file_id :=utl_file.fopen(v_file_path, v_file_name, 'W', C_MAXLINE);
         --
         utl_file.put_line(file_id, ' ');
         utl_file.put_line(file_id, 'Request ID: '||n_req_id);
         utl_file.put_line(file_id, 'Report    : '||'HDS Rebates: SLA Period Close Exceptions -Error Summary');
         utl_file.put_line(file_id, 'Run Date  : '||to_char(sysdate, 'MM-DD-YYYY'));
         utl_file.put_line(file_id, 'Request ID: '||n_req_id);
         utl_file.put_line(file_id, ' ');
         utl_file.put_line(file_id, 'Errors: '||n_req_id);
         utl_file.put_line(file_id, '=======');
         --
         for rec in sla_error_summary
          loop
           --
           n_idx :=n_idx+1;
           --
           utl_file.put_line(file_id, ' '||n_idx||') '||rec.msg);
           --
          end loop;
         --
         utl_file.fclose(file_id);
         --
        exception
         when others then
          print_log('Issue in creating Rebates SLA Exceptions summary file for US and CAD, Error :'||sqlerrm);
        end;
        --
      */
      -- Need to code logic to email
      --
      --
      BEGIN
        --
        SELECT COUNT(1)
          INTO n_us_count
          FROM xxcus.xxcus_ozf_sla_exceptions
         WHERE request_id = n_req_id
           AND ledger_id = 2021;
        --
      EXCEPTION
        WHEN no_data_found THEN
          n_us_count := 0;
        WHEN OTHERS THEN
          print_log('@get n_US_count, msg =' || SQLERRM);
          n_us_count := 0;
      END;
      --
      BEGIN
        --
        SELECT COUNT(1)
          INTO n_cad_count
          FROM xxcus.xxcus_ozf_sla_exceptions
         WHERE request_id = n_req_id
           AND ledger_id = 2023;
        --
      EXCEPTION
        WHEN no_data_found THEN
          n_cad_count := 0;
        WHEN OTHERS THEN
          print_log('@get n_CAD_count, msg =' || SQLERRM);
          n_cad_count := 0;
      END;
      --
      print_output(' ');
      print_output('Request ID: ' || n_req_id);
      print_output('Report    : ' ||
                   'HDS Rebates: SLA Period Close Exceptions -Error Summary');
      print_output('Run Date and Time : ' ||
                   to_char(SYSDATE, 'MM-DD-YYYY HH24:MI:SS'));
      print_output(' ');
      --
      IF (n_us_count > 0 OR n_cad_count > 0)
      THEN
        --
        print_output(' ');
        --
        IF (n_us_count > 0 AND n_cad_count = 0)
        THEN
          print_output('Impacted Ledgers: HDS Rebates USA');
        ELSIF (n_us_count = 0 AND n_cad_count > 0)
        THEN
          print_output('Impacted Ledgers: HDS Rebates CAN');
        ELSIF (n_us_count > 0 AND n_cad_count > 0)
        THEN
          print_output('Impacted Ledgers: HDS Rebates USA and HDS Rebates CAN');
        ELSE
          NULL;
        END IF;
        --
        print_output(' ');
        --
        print_output('Notification: Trade Management sub-ledger accounting entries exceptions exist.  Please review exception details and work with IT team if needed to resolve.');
        --
      ELSE
        print_output(' ');
        print_output('Impacted Ledgers: None');
        print_output(' ');
        print_output('Notification: We have no errors from the HDS Rebates: SLA Exceptions Report.');
      END IF;
      --
      print_output('');
      print_output('Technical Note to IT: The detail list of exceptions is available from the custom table xxcus.xxcus_ozf_sla_exceptions');
      print_output('');
      --
      print_output('This is an automated email. Please DO NOT reply to this message.');
      print_output('');
      --
      print_output('Thanks');
      --
      --
      BEGIN
        --
        BEGIN
          --
          BEGIN
            SELECT upper(NAME) INTO v_curent_db_name FROM v$database;
          EXCEPTION
            WHEN OTHERS THEN
              v_curent_db_name := 'NA';
              print_log('Unable to get current DB Name from v$database, message =' ||
                        SQLERRM);
          END;
          --
          IF (v_curent_db_name = v_prod_db_name)
          THEN
            v_email := v_prod_rbt_email_id;
          ELSE
            BEGIN
              SELECT email_address
                INTO v_email
                FROM fnd_user
               WHERE 1 = 1
                 AND user_id = fnd_global.user_id;
            EXCEPTION
              WHEN OTHERS THEN
                v_email := 'NA';
                print_log('@get v_email, message =' || SQLERRM);
            END;
          END IF;
          --
          IF v_email <> 'NA'
          THEN
            ln_request_id := fnd_request.submit_request(application => 'XXCUS'
                                                       ,program     => 'XXCUS_OZF_SLAEXCEP_STATUS'
                                                       ,description => ''
                                                       ,start_time  => ''
                                                       ,sub_request => FALSE
                                                       ,argument1   => v_email
                                                       ,argument2   => p_fiscal_period
                                                       ,argument3   => n_req_id);
            --
            IF ln_request_id > 0
            THEN
              --
              print_log('Submitted Rebates SLA Exception status email, request_id =' ||
                        ln_request_id);
              COMMIT;
            ELSE
              print_log('Failed to submit Rebates SLA Exception status email');
            END IF;
            --
          END IF;
        EXCEPTION
          WHEN OTHERS THEN
            print_log('@block of variable v_email, message =' || SQLERRM);
        END;
        --
      EXCEPTION
        WHEN OTHERS THEN
          print_log('Issue in sending TMGL refresh status...' || SQLERRM);
      END;
      --
    ELSE
      --
      print_log('Failed to fetch fiscal year, please check the value of the parameter p_fiscal_period to make sure the last 4 characters look like year');
      --
    END IF;
    --
  EXCEPTION
    WHEN OTHERS THEN
      fnd_file.put_line(fnd_file.log, l_sec);
      fnd_file.put_line(fnd_file.output, l_sec);
      l_err_code := 2;
      l_err_msg  := '@sla_exceptions - unknown error: ' ||
                    ' Error_Stack...' || dbms_utility.format_error_stack() ||
                    ' Error_Backtrace...' ||
                    dbms_utility.format_error_backtrace();
      dbms_output.put_line(l_err_msg);
      fnd_file.put_line(fnd_file.log, l_err_msg);
      fnd_file.put_line(fnd_file.output, l_err_msg);
      l_err_callpoint := l_sec;

      xxcus_error_pkg.xxcus_error_main_api(p_called_from       => l_err_callfrom
                                          ,p_calling           => l_err_callpoint
                                          ,p_request_id        => l_request_id
                                          ,p_ora_error_msg     => substr(regexp_replace(l_err_msg
                                                                                       ,'[[:cntrl:]]'
                                                                                       ,NULL)
                                                                        ,1
                                                                        ,2000)
                                          ,p_error_desc        => substr(l_err_msg
                                                                        ,1
                                                                        ,240)
                                          ,p_distribution_list => l_distro_list
                                          ,p_module            => 'TM');

  END sla_exceptions;
  --
  /*******************************************************************************
  * Procedure:   SUBMIT_REQUEST_SET
  * Description: This is for the concurrent request set XXCUS_TM_REBATE_WEEKLY
  *              Process will run the concurrent requests needed when the receipts
  *              for the last BU has loaded and TPA is complete
  HISTORY
  ===============================================================================
  VERSION DATE          AUTHOR(S)       DESCRIPTION
  ------- -----------   --------------- -----------------------------------------
  1.0     01/21/2014    Kathy Poling    Initial creation of the procedure
                                        SR 236145
  1.1     02/04/2014    Kathy Poling    Change Process Category to null submitting
                                        Create Accounting based on Mike's comment
                                        RFC 39202
  1.2     02/12/2014    Kathy Poling    SR 238652 Added MV refresh bu_spend and
                                        moved stages around
                                        RFC 39350
                                        Fixed parm for TM GL Journals Extract job on the Canada
                                        request SR 241317
  1.3     03/30/2014    Kathy Poling    Change parameter Process Category for Create Accounting
                                        from null to Channel Revenue Management
                                        SR 244409 RFC 39901
  1.4     04/21/2014    Kathy Poling    SR 244910 Change to parms for Create Accounting
                                        default was set to Yes and changed to No
  2.1     08/12/2014    Kathy Poling    SR 258560 Change in procedure submit_request_set
                                        Rebate TM Request Sets - need to be updated to
                                        run Subledger Accounting job instead of create accounting
                                        RFC 41627
  2.2     08/12/2014   Balaguru Seshadri ESMS 265235
  2.7     03/02/2015   Balaguru Seshdri  RFC 43009
  2.8     03/27/2015
  2.9   02/10/2016   Bala Seshadri   TMS 20160208-00208 or  ESMS 316266. Comment logic for HDS Rebates: SLA Exceptions Report and HDS Rebates: Generate TM GL Extract for US and CANADA
  ********************************************************************************/
  PROCEDURE submit_request_set(errbuf    OUT VARCHAR2
                              ,retcode   OUT NUMBER
                              ,p_fperiod IN VARCHAR2
                              ,p_user_id IN NUMBER) IS
    --
    -- Package Variables
    --
    l_request_id NUMBER NULL;
    --
    --
    v_email             VARCHAR2(60) := NULL;
    v_current_db_name   VARCHAR2(20) := 'NA';
    --v_prod_db_name      VARCHAR2(20) := 'EBIZPRD'; --3.0
    v_prod_db_name      VARCHAR2(20) := 'EBSPRD'; --3.0
    v_prod_rbt_email_id VARCHAR2(40) := 'hdsoraclerebates@hdsupply.com';
    --
    l_bool         BOOLEAN;
    l_iso_language VARCHAR2(30);
    --
    l_iso_territory VARCHAR2(30);
    --
    l_err_msg  VARCHAR2(3000);
    l_err_code NUMBER;
    l_sec      VARCHAR2(255);

    l_ok                  BOOLEAN := TRUE;
    l_success             NUMBER := 0;
    l_responsibility_id   NUMBER := 51609; --'HDS Trade Management Admin Super User - USD/CAD RBT'
    l_resp_application_id NUMBER := 682; --OZF
    l_sysdate             DATE;
    l_end_date            VARCHAR2(30);
    --Version 1.4 adding parms
    l_gl_post              VARCHAR2(3);
    l_gl_post_cd           VARCHAR2(1);
    l_gl_trnsfr            VARCHAR2(3);
    l_gl_trnsfr_cd         VARCHAR2(1);
    l_report_level_meaning VARCHAR2(15);
    l_report_level         VARCHAR2(1);
    l_mode_meaning         VARCHAR2(15);
    l_mode                 VARCHAR2(1);
    -- end parms Version 1.4
    srs_failed        EXCEPTION;
    submitprog_failed EXCEPTION;
    submitset_failed  EXCEPTION;

    -- Error DEBUG
    l_err_callfrom  VARCHAR2(75) DEFAULT 'XXCUS_TM_INTERFACE_PKG.SUBMIT_RERUEST_SET';
    l_err_callpoint VARCHAR2(75) DEFAULT 'START';
    l_distro_list   VARCHAR2(75) DEFAULT 'HDSOracleDevelopers@hdsupply.com';
   -- Begin ver 3.2
   cursor c_xml_defn is
    select
      a.default_language dflt_lang,
      a.default_territory dflt_territory,
      a.default_output_type dflt_output_type
    from xdo_templates_vl a
    where 1 =1
        and a.template_code =l_rebates_audit_job_code
        and nvl(a.end_date,sysdate) >= sysdate
        and a.object_version_number =
            (
               select max(xdt1.object_version_number)
               from xdo_templates_vl xdt1
               where 1 =1
                     and xdt1.template_code =a.template_code
                     and nvl(xdt1.end_date,sysdate) >= sysdate
            )
        and rownum < 2;
   --
   c_xml_defn_rec  c_xml_defn%rowtype :=Null;
   -- End ver 3.2
  BEGIN

    SELECT SYSDATE, to_char(trunc(SYSDATE), 'YYYY/MM/DD') || ' 00:00:00'
      INTO l_sysdate, l_end_date
      FROM dual;

    --------------------------------------------------------------------------
    -- Apps Initialize
    --------------------------------------------------------------------------
    fnd_global.apps_initialize(p_user_id
                              ,l_responsibility_id
                              ,l_resp_application_id);

    -- Step 1 - call set_request_set
    --
    l_sec := 'Launching Request Set';
    --
    fnd_file.put_line(fnd_file.log, l_sec);
    --
    l_ok := fnd_submit.set_request_set(application => 'OZF'
                                      ,request_set => 'XXCUSTMREBATE_WEEKLY');
    --
    -- ESMS 265235: Bala Seshadri: Begin
    --
    IF (l_ok AND l_success = 0)
    THEN
      -- l_success is zero because that's what the declared variable is initialized with
      --
      l_sec := '1st job - STAGE 05 (1st request) HDS Rebates: Print Rebates Receipt Exception Report)';
      --
      fnd_file.put_line(fnd_file.log, l_sec);
      --
      BEGIN
        SELECT lower(iso_language), iso_territory
          INTO l_iso_language, l_iso_territory
          FROM fnd_languages
         WHERE language_code = userenv('LANG');
      EXCEPTION
        WHEN OTHERS THEN
          print_log('@ get language settings for XML layout API, error =' ||
                    SQLERRM);
      END;
      --
      l_bool := fnd_submit.add_layout(template_appl_name => 'XXCUS'
                                     ,template_code      => 'XXCUS_OZF_RBT_RCPTS_IMP_STATUS'
                                     ,template_language  => l_iso_language
                                     ,template_territory => l_iso_territory
                                     ,output_format      => 'PDF');
      --
      l_ok := fnd_submit.submit_program(application => 'XXCUS'
                                       ,program     => 'XXCUS_OZF_RBT_RCPTS_IMP_STATUS'
                                       ,stage       => 'STAGE_05'
                                       ,argument1   => p_fperiod --Fiscal Period
                                        );
    ELSE
      l_success := -99;
      fnd_file.put_line(fnd_file.log, l_sec || ':  Failed = ' || l_success);
      RAISE submitprog_failed;
    END IF;
    --
    -- ESMS 265235: Bala Seshadri: End
    --
    -- 1st job - STAGE_1 Subledger accounting for US
    --
    IF l_ok AND l_success = 0
    THEN
      -- ------------------------------------
      -- Step 2 - call submit program for each program in the set
      -- Stage 1 with 3 requests in the stage
      -- -----------------------------------
      l_sec := '1st job - STAGE_1 Subledger Accounting Program - US request';
      --dbms_output.put_line(l_sec);
      fnd_file.put_line(fnd_file.log, l_sec);

      --Version 1.4 start 4/15/14
      BEGIN
        SELECT meaning, submit_gl_post_flag
          INTO l_gl_post, l_gl_post_cd
          FROM apps.xla_lookups xlk, apps.xla_subledger_options_v xso
         WHERE xlk.lookup_type = 'XLA_YES_NO'
           AND xlk.lookup_code = xso.submit_gl_post_flag
           AND xso.application_id = l_resp_application_id
           AND xso.ledger_id = 2021
           AND rownum = 1;
      EXCEPTION
        WHEN no_data_found THEN
          l_err_msg := 'Failed to find submit GL Post flag US: ' || SQLERRM;
          fnd_file.put_line(fnd_file.log, l_err_msg);
          RAISE program_error;
      END;

      BEGIN
        SELECT meaning, xso.submit_transfer_to_gl_flag
          INTO l_gl_trnsfr, l_gl_trnsfr_cd
          FROM apps.xla_lookups xlk, apps.xla_subledger_options_v xso
         WHERE xlk.lookup_type = 'XLA_YES_NO'
           AND xlk.lookup_code = xso.submit_transfer_to_gl_flag
           AND xso.application_id = l_resp_application_id
           AND xso.ledger_id = 2021
           AND rownum = 1;
      EXCEPTION
        WHEN no_data_found THEN
          l_err_msg := 'Failed to find submit GL Transfer flag US: ' ||
                       SQLERRM;
          fnd_file.put_line(fnd_file.log, l_err_msg);
          RAISE program_error;
      END;

      BEGIN
        SELECT meaning, xso.accounting_mode_code
          INTO l_mode_meaning, l_mode
          FROM apps.xla_lookups xlk, apps.xla_subledger_options_v xso
         WHERE xlk.lookup_type = 'XLA_ACCOUNTING_ENTRY_STATUS'
           AND xlk.lookup_code = xso.accounting_mode_code
           AND xso.application_id = l_resp_application_id
           AND xso.ledger_id = 2021
           AND rownum = 1;
      EXCEPTION
        WHEN no_data_found THEN
          l_err_msg := 'Failed to find Mode US: ' || SQLERRM;
          fnd_file.put_line(fnd_file.log, l_err_msg);
          RAISE program_error;
      END;

      BEGIN
        SELECT meaning, xso.summary_report_flag
          INTO l_report_level_meaning, l_report_level
          FROM apps.xla_lookups xlk, apps.xla_subledger_options_v xso
         WHERE xlk.lookup_type = 'XLA_REPORT_LEVEL'
           AND xlk.lookup_code = xso.summary_report_flag
           AND xso.application_id = l_resp_application_id
           AND xso.ledger_id = 2021
           AND rownum = 1;
      EXCEPTION
        WHEN no_data_found THEN
          l_err_msg := 'Failed to find Report Level US: ' || SQLERRM;
          fnd_file.put_line(fnd_file.log, l_err_msg);
          RAISE program_error;
      END;
      --
      --
      l_ok := fnd_submit.submit_program(application => 'XLA'
                                       ,program     => 'OZFSLAAP'
                                       ,stage       => 'STAGE_1'
                                       ,argument1   => 101 --Operating Unit
                                       ,argument2   => l_resp_application_id --Source Application
                                       ,argument3   => l_resp_application_id --Application ID
                                       ,argument4   => 'Y' --Dummy Parameter 0
                                       ,argument5   => 2021 --Ledger
                                       ,argument6   => 'CHANNEL_REVENUE_MANAGEMENT' --Process Category
                                       ,argument7   => l_end_date --End Date
                                       ,argument8   => 'Y' --Create Accounting Flag
                                       ,argument9   => 'Y' --Dummy Parameter 1
                                       ,argument10  => 'F' --Accounting Mode
                                       ,argument11  => 'Y' --Dummy Parameter 2
                                       ,argument12  => 'N' --Errors Only
                                       ,argument13  => l_report_level --Report  'S'
                                       ,argument14  => l_gl_trnsfr_cd --Transfer to General Ledger
                                       ,argument15  => 'Y' --Dummy Parameter 3
                                       ,argument16  => l_gl_post_cd --Post in General Ledger
                                       ,argument17  => l_sysdate --General Ledger Batch Name
                                       ,argument18  => NULL --chr(0)              --Mixed Currency Precision
                                       ,argument19  => 'N' --Include Zero Amount Entries and Lines
                                       ,argument20  => NULL --Request ID
                                       ,argument21  => NULL --Entity ID
                                       ,argument22  => 'Trade Management' --Source Application Name
                                       ,argument23  => 'Trade Management' --Application Name
                                       ,argument24  => 'HDS Rebates USA' --Ledger Name
                                       ,argument25  => 'Channel Revenue Management' --Process Category Name
                                       ,argument26  => 'Yes' --Create Acct
                                       ,argument27  => l_mode_meaning --Accounting Mode Name
                                       ,argument28  => 'No' --Errors Only
                                       ,argument29  => l_report_level_meaning --Report Level
                                       ,argument30  => l_gl_trnsfr --Transfer To GL
                                       ,argument31  => l_gl_post --Post in GL
                                       ,argument32  => 'No' --Include Zero Amt Lines and Entries
                                       ,argument33  => NULL --Valuation Method
                                       ,argument34  => NULL --Security ID Integer 1
                                       ,argument35  => NULL --Security ID Integer 2
                                       ,argument36  => NULL --Security ID Integer 3
                                       ,argument37  => NULL --Security ID Char 1
                                       ,argument38  => NULL --Security ID Char 2
                                       ,argument39  => NULL --Security ID Char 3
                                       ,argument40  => NULL --Concurrent Request ID
                                       ,argument41  => 'N' --Include User Transaction Identifiers
                                       ,argument42  => 'No' --P_INCLUDE_USER_TRX_IDENTIFIERS
                                       ,argument43  => 'N' --Enable Debug Log
                                       ,argument44  => fnd_global.user_id());

    ELSE
      l_success := -100;
      dbms_output.put_line('Success = ' || l_success);
      fnd_file.put_line(fnd_file.log, l_sec || ':  Failed = ' || l_success);
      RAISE srs_failed;
    END IF;

    -- --------------------------------------
    -- 2 request HDS Rebates: Generate TM Inventory Items Extract
    -- --------------------------------------
    IF l_ok AND l_success = 0
    THEN

      l_sec := '1st job - STAGE_10 (2nd request) HDS Rebates: Generate TM Inventory Items Extract)';
      --dbms_output.put_line(l_sec);
      fnd_file.put_line(fnd_file.log, l_sec);

      l_ok := fnd_submit.submit_program(application => 'XXCUS'
                                       ,program     => 'XXCUS_OZF_REBATE_ITEMS_EXTRACT'
                                       ,stage       => 'STAGE_1');
    ELSE
      l_success := -101;
      --dbms_output.put_line('Success = ' || l_success);
      fnd_file.put_line(fnd_file.log, l_sec || ':  Failed = ' || l_success);
      RAISE submitprog_failed;
    END IF;

    -- --------------------------------------
    -- 3 request HDS Rebates: Generate TM Customers Extract
    -- --------------------------------------
    IF l_ok AND l_success = 0
    THEN

      l_sec := '1st job - STAGE_10 (3nd request) HDS Rebates: Generate TM Customers Extract)';
      --dbms_output.put_line(l_sec);
      fnd_file.put_line(fnd_file.log, l_sec);

      l_ok := fnd_submit.submit_program(application => 'XXCUS'
                                       ,program     => 'XXCUS_OZF_REFRESH_CUSTOMERS'
                                       ,stage       => 'STAGE_1');
    ELSE
      l_success := -102;
      --dbms_output.put_line('Success = ' || l_success);
      fnd_file.put_line(fnd_file.log, l_sec || ':  Failed = ' || l_success);
      RAISE submitprog_failed;
    END IF;

    -- --------------------------------------
    -- New stage STAGE_5 with 1 request
    --
    -- STAGE_5 Subledger accounting for CANADA. Stage_5 is different from Stage_05 which was the new one we created.
    --
    IF l_ok AND l_success = 0
    THEN

      l_sec := '2rd job - STAGE_5 Subledger Accounting Program - CAD request';
      --dbms_output.put_line(l_sec);
      fnd_file.put_line(fnd_file.log, l_sec);

      --Version 1.4 start  4/15/14
      BEGIN
        SELECT meaning, submit_gl_post_flag
          INTO l_gl_post, l_gl_post_cd
          FROM apps.xla_lookups xlk, apps.xla_subledger_options_v xso
         WHERE xlk.lookup_type = 'XLA_YES_NO'
           AND xlk.lookup_code = xso.submit_gl_post_flag
           AND xso.application_id = l_resp_application_id
           AND xso.ledger_id = 2023
           AND rownum = 1;
      EXCEPTION
        WHEN no_data_found THEN
          l_err_msg := 'Failed to find submit GL Post flag CAD: ' ||
                       SQLERRM;
          fnd_file.put_line(fnd_file.log, l_err_msg);
          RAISE program_error;
      END;

      BEGIN
        SELECT meaning, xso.submit_transfer_to_gl_flag
          INTO l_gl_trnsfr, l_gl_trnsfr_cd
          FROM apps.xla_lookups xlk, apps.xla_subledger_options_v xso
         WHERE xlk.lookup_type = 'XLA_YES_NO'
           AND xlk.lookup_code = xso.submit_transfer_to_gl_flag
           AND xso.application_id = l_resp_application_id
           AND xso.ledger_id = 2023
           AND rownum = 1;
      EXCEPTION
        WHEN no_data_found THEN
          l_err_msg := 'Failed to find submit GL Transfer flag CAD: ' ||
                       SQLERRM;
          fnd_file.put_line(fnd_file.log, l_err_msg);
          RAISE program_error;
      END;

      BEGIN
        SELECT meaning, xso.accounting_mode_code
          INTO l_mode_meaning, l_mode
          FROM apps.xla_lookups xlk, apps.xla_subledger_options_v xso
         WHERE xlk.lookup_type = 'XLA_ACCOUNTING_ENTRY_STATUS'
           AND xlk.lookup_code = xso.accounting_mode_code
           AND xso.application_id = l_resp_application_id
           AND xso.ledger_id = 2023
           AND rownum = 1;
      EXCEPTION
        WHEN no_data_found THEN
          l_err_msg := 'Failed to find Mode CAD: ' || SQLERRM;
          fnd_file.put_line(fnd_file.log, l_err_msg);
          RAISE program_error;
      END;

      BEGIN
        SELECT meaning, xso.summary_report_flag
          INTO l_report_level_meaning, l_report_level
          FROM apps.xla_lookups xlk, apps.xla_subledger_options_v xso
         WHERE xlk.lookup_type = 'XLA_REPORT_LEVEL'
           AND xlk.lookup_code = xso.summary_report_flag
           AND xso.application_id = l_resp_application_id
           AND xso.ledger_id = 2023
           AND rownum = 1;
      EXCEPTION
        WHEN no_data_found THEN
          l_err_msg := 'Failed to find Report Level CAD: ' || SQLERRM;
          fnd_file.put_line(fnd_file.log, l_err_msg);
          RAISE program_error;
      END;
      --
      --
      l_ok := fnd_submit.submit_program(application => 'XLA'
                                       ,program     => 'OZFSLAAP'
                                       ,stage       => 'STAGE_5'
                                       ,argument1   => 102 --Operating Unit
                                       ,argument2   => l_resp_application_id --Source Application
                                       ,argument3   => l_resp_application_id --Application ID
                                       ,argument4   => 'Y' --Dummy Parameter 0
                                       ,argument5   => 2023 --Ledger
                                       ,argument6   => 'CHANNEL_REVENUE_MANAGEMENT' --Process Category
                                       ,argument7   => l_end_date --End Date
                                       ,argument8   => 'Y' --Create Accounting Flag
                                       ,argument9   => 'Y' --Dummy Parameter 1
                                       ,argument10  => 'F' --Accounting Mode
                                       ,argument11  => 'Y' --Dummy Parameter 2
                                       ,argument12  => 'N' --Errors Only
                                       ,argument13  => l_report_level --Report  'S'
                                       ,argument14  => l_gl_trnsfr_cd --Transfer to General Ledger
                                       ,argument15  => 'Y' --Dummy Parameter 3
                                       ,argument16  => l_gl_post_cd --Post in General Ledger
                                       ,argument17  => l_sysdate --General Ledger Batch Name
                                       ,argument18  => NULL --chr(0)              --Mixed Currency Precision
                                       ,argument19  => 'N' --Include Zero Amount Entries and Lines
                                       ,argument20  => NULL --Request ID
                                       ,argument21  => NULL --Entity ID
                                       ,argument22  => 'Trade Management' --Source Application Name
                                       ,argument23  => 'Trade Management' --Application Name
                                       ,argument24  => 'HDS Rebates CAN' --Ledger Name
                                       ,argument25  => 'Channel Revenue Management' --Process Category Name
                                       ,argument26  => 'Yes' --Create Acct
                                       ,argument27  => l_mode_meaning --Accounting Mode Name
                                       ,argument28  => 'No' --Errors Only
                                       ,argument29  => l_report_level_meaning --Report Level
                                       ,argument30  => l_gl_trnsfr --Transfer To GL
                                       ,argument31  => l_gl_post --Post in GL
                                       ,argument32  => 'No' --Include Zero Amt Lines and Entries
                                       ,argument33  => NULL --Valuation Method
                                       ,argument34  => NULL --Security ID Integer 1
                                       ,argument35  => NULL --Security ID Integer 2
                                       ,argument36  => NULL --Security ID Integer 3
                                       ,argument37  => NULL --Security ID Char 1
                                       ,argument38  => NULL --Security ID Char 2
                                       ,argument39  => NULL --Security ID Char 3
                                       ,argument40  => NULL --Concurrent Request ID
                                       ,argument41  => 'N' --Include User Transaction Identifiers
                                       ,argument42  => 'No' --P_INCLUDE_USER_TRX_IDENTIFIERS
                                       ,argument43  => 'N' --Enable Debug Log
                                       ,argument44  => fnd_global.user_id());

    ELSE
      l_success := -110;
      --dbms_output.put_line('Success = ' || l_success);
      fnd_file.put_line(fnd_file.log, l_sec || ':  Failed = ' || l_success);
      RAISE submitprog_failed;
    END IF;
    --
    -- ESMS 265235: Bala Seshadri: Begin
    -- At this point we are going to do the below items mentioned.
    -- kickoff transfer to gl journal entries for US -step 1 [Stage: STAGE_25]
    -- kickoff transfer to gl journal entries for CAD -step 2 [Stage: STAGE_25]
    -- kickoff CUSTOM sla exceptions report
    -- An email is sent to the distributions list. if instance is production then email to hdsoraclerebates@hdsupply.com
    -- If non production instance then email to the user running the request set
    -- wait until step 1 is complete
    -- Logic Begin @@@@@@@@@@@@@@@@@@@@@@@@@@@@
    IF l_ok AND l_success = 0
    THEN

      l_sec := 'STAGE_25 Transfer to GL for US';
      --dbms_output.put_line(l_sec);
      fnd_file.put_line(fnd_file.log, l_sec);

      l_ok := fnd_submit.submit_program(application => 'XLA'
                                       ,program     => 'XLAGLTRN'
                                       ,stage       => 'STAGE_25'
                                       ,argument1   => 682
                                       ,argument2   => 682
                                       ,argument3   => 'Y'
                                       ,argument4   => 2021
                                       ,argument5   => 'CHANNEL_REVENUE_MANAGEMENT'
                                       ,argument6   => to_char(trunc(SYSDATE)
                                                              ,'YYYY/MM/DD HH24:MI:SS') --2014/10/16 00:00:00
                                       ,argument7   => 'N'
                                       ,argument8   => NULL
                                       ,argument9   => NULL
                                       ,argument10  => 'Y'
                                       ,argument11  => NULL
                                       ,argument12  => NULL
                                       ,argument13  => 'Y'
                                       ,argument14  => 'Y'
                                       ,argument15  => 'N'
                                       ,argument16  => NULL
                                       ,argument17  => NULL
                                       ,argument18  => NULL
                                       ,argument19  => NULL
                                       ,argument20  => 'Trade Management'
                                       ,argument21  => 'Trade Management'
                                       ,argument22  => 'HDS Rebates USA'
                                       ,argument23  => 'Channel Revenue Management'
                                       ,argument24  => 'No'
                                       ,argument25  => NULL
                                       ,argument26  => NULL
                                       ,argument27  => NULL
                                       ,argument28  => 'Yes'
                                       ,argument29  => 'No'
                                       ,argument30  => NULL
                                       ,argument31  => NULL
                                       ,argument32  => NULL
                                       ,argument33  => NULL
                                       ,argument34  => NULL
                                       ,argument35  => NULL
                                       ,argument36  => NULL
                                       ,argument37  => NULL
                                       ,argument38  => 'N');
    ELSE
      l_success := -104;
      print_log(l_sec || ':  Failed = ' || l_success);
      RAISE submitprog_failed;
    END IF;
    --
    -- Submit Transfer to GL for Canadian ledger
    IF l_ok AND l_success = 0
    THEN

      l_sec := 'STAGE_25 Transfer to GL for CAD';
      --dbms_output.put_line(l_sec);
      fnd_file.put_line(fnd_file.log, l_sec);

      l_ok := fnd_submit.submit_program(application => 'XLA'
                                       ,program     => 'XLAGLTRN'
                                       ,stage       => 'STAGE_25'
                                       ,argument1   => 682
                                       ,argument2   => 682
                                       ,argument3   => 'Y'
                                       ,argument4   => 2023
                                       ,argument5   => 'CHANNEL_REVENUE_MANAGEMENT'
                                       ,argument6   => to_char(trunc(SYSDATE)
                                                              ,'YYYY/MM/DD HH24:MI:SS') --2014/10/16 00:00:00
                                       ,argument7   => 'N'
                                       ,argument8   => NULL
                                       ,argument9   => NULL
                                       ,argument10  => 'Y'
                                       ,argument11  => NULL
                                       ,argument12  => NULL
                                       ,argument13  => 'Y'
                                       ,argument14  => 'Y'
                                       ,argument15  => 'N'
                                       ,argument16  => NULL
                                       ,argument17  => NULL
                                       ,argument18  => NULL
                                       ,argument19  => NULL
                                       ,argument20  => 'Trade Management'
                                       ,argument21  => 'Trade Management'
                                       ,argument22  => 'HDS Rebates CAN'
                                       ,argument23  => 'Channel Revenue Management'
                                       ,argument24  => 'No'
                                       ,argument25  => NULL
                                       ,argument26  => NULL
                                       ,argument27  => NULL
                                       ,argument28  => 'Yes'
                                       ,argument29  => 'No'
                                       ,argument30  => NULL
                                       ,argument31  => NULL
                                       ,argument32  => NULL
                                       ,argument33  => NULL
                                       ,argument34  => NULL
                                       ,argument35  => NULL
                                       ,argument36  => NULL
                                       ,argument37  => NULL
                                       ,argument38  => 'N');
    ELSE
      l_success := -105;
      print_log(l_sec || ':  Failed = ' || l_success);
      RAISE submitprog_failed;
    END IF;
    --
   -- Begin Ver 2.9
    /*
    -- Submit custom SLA Exceptions report
    --
    IF l_ok AND l_success = 0
    THEN

      l_sec := 'STAGE_27 Rebates SLA Exceptions';
      --dbms_output.put_line(l_sec);
      fnd_file.put_line(fnd_file.log, l_sec);

      l_ok := fnd_submit.submit_program(application => 'XXCUS'
                                       ,program     => 'XXCUS_OZF_SLA_PER_CL_EXCEP_RPT'
                                       ,stage       => 'STAGE_27'
                                       ,argument1   => p_fperiod --fiscal period
                                        );
    ELSE
      l_success := -106;
      print_log(l_sec || ':  Failed = ' || l_success);
      RAISE submitprog_failed;
    END IF;
    -- Logic End @@@@@@@@@@@@@@@@@@@@@@@@@@@@
    --  ESMS 265235: Bala Seshadri: End
    */
    -- End Ver 2.9
    -- --------------------------------------
    -- New stage STAGE_30 with 1 request
    -- --------------------------------------
    IF l_ok AND l_success = 0
    THEN

      l_sec := '3rd job - STAGE_30 HDS Rebates: Generate TM SLA Accruals Extract -By Period';
      --dbms_output.put_line(l_sec);
      fnd_file.put_line(fnd_file.log, l_sec);

      l_ok := fnd_submit.submit_program(application => 'XXCUS'
                                       ,program     => 'XXCUS_OZF_TM_SLA_PERIOD'
                                       ,stage       => 'STAGE_30'
                                       ,argument1   => p_fperiod --Period
                                       ,argument2   => '131100' --Rebates GL Account
                                        );
    ELSE
      l_success := -130;
      --dbms_output.put_line('Success = ' || l_success);
      fnd_file.put_line(fnd_file.log, l_sec || ':  Failed = ' || l_success);
      RAISE submitprog_failed;
    END IF;
    --
    -- --------------------------------------
    -- New stage STAGE_35 with 1 request
    -- --------------------------------------
    IF l_ok AND l_success = 0
    THEN

      l_sec := '4th job - STAGE_35 HDS Rebates: Generate TM GL Journals Extract';
      --dbms_output.put_line(l_sec);
      fnd_file.put_line(fnd_file.log, l_sec);

      l_ok := fnd_submit.submit_program(application => 'XXCUS'
                                       ,program     => 'XXCUS_OZF_TM_RECON_JOURNALS'
                                       ,stage       => 'STAGE_35'
                                       ,argument1   => 'US' --Territory
                                       ,argument2   => p_fperiod --Period
                                       ,argument3   => '131100' --Rebates GL Account
                                        );
    ELSE
      l_success := -135;
      --dbms_output.put_line('Success = ' || l_success);
      fnd_file.put_line(fnd_file.log, l_sec || ':  Failed = ' || l_success);
      RAISE submitprog_failed;
    END IF;

    -- --------------------------------------
    -- New stage STAGE_40 with 1 request
    -- --------------------------------------
    IF l_ok AND l_success = 0
    THEN

      l_sec := '5th job - STAGE_40 HDS Rebates: Generate TM GL Journals Extract';
      --dbms_output.put_line(l_sec);
      fnd_file.put_line(fnd_file.log, l_sec);

      l_ok := fnd_submit.submit_program(application => 'XXCUS'
                                       ,program     => 'XXCUS_OZF_TM_RECON_JOURNALS'
                                       ,stage       => 'STAGE_40'
                                       ,argument1   => 'CA' --Territory    --1.2 fixed parm
                                       ,argument2   => p_fperiod --Period
                                       ,argument3   => '131100' --Rebates GL Account
                                        );
    ELSE
      l_success := -140;
      --dbms_output.put_line('Success = ' || l_success);
      fnd_file.put_line(fnd_file.log, l_sec || ':  Failed = ' || l_success);
      RAISE submitprog_failed;
    END IF;

    -- --------------------------------------
    -- New stage STAGE_TMGL_STATUS with 1 request
    -- --------------------------------------
    IF l_ok AND l_success = 0
    THEN
      --
      l_sec := 'STAGE_TMGL_STATUS HDS Rebates: Email TMGL Refresh Status';
      --
      fnd_file.put_line(fnd_file.log, l_sec);
      --
      --
      BEGIN
        SELECT upper(NAME) INTO v_current_db_name FROM v$database;
      EXCEPTION
        WHEN OTHERS THEN
          v_current_db_name := 'NA';
          print_log('Unable to get current DB Name from v$database, message =' ||
                    SQLERRM);
      END;
      --
      IF (v_current_db_name = v_prod_db_name)
      THEN
        --
        v_email := v_prod_rbt_email_id;
        --
      ELSE
        --
        BEGIN
          SELECT email_address
            INTO v_email
            FROM fnd_user
           WHERE 1 = 1
             AND user_id = fnd_global.user_id;
        EXCEPTION
          WHEN no_data_found THEN
            v_email := 'NA';
          WHEN OTHERS THEN
            v_email := 'NA';
            print_log('@get v_email, when others message =' || SQLERRM);
        END;
        --
      END IF;
      --
      --
      l_ok := fnd_submit.submit_program(application => 'XXCUS'
                                       ,program     => 'XXCUS_OZF_EMAIL_TMGL_STATUS'
                                       ,stage       => 'STAGE_TMGL_STATUS'
                                       ,argument1   => v_email
                                       ,argument2   => p_fperiod --Period
                                        );
    ELSE
      l_success := -150;
      --dbms_output.put_line('Success = ' || l_success);
      fnd_file.put_line(fnd_file.log, l_sec || ':  Failed = ' || l_success);
      RAISE submitprog_failed;
    END IF;
    --
    -- --------------------------------------
    -- New stage MV REFRESH with 3 request
    -- --------------------------------------
    IF l_ok AND l_success = 0
    THEN

      l_sec := 'MV_REFRESH (1st request) refresh Materialized Views (Vendor Spend MV)';
      --dbms_output.put_line(l_sec);
      fnd_file.put_line(fnd_file.log, l_sec);

      l_ok := fnd_submit.submit_program(application => 'XXCUS'
                                       ,program     => 'XXCUSOZF_MV_REFRESH'
                                       ,stage       => 'MV_REFRESH'
                                       ,argument1   => 'XXCUSOZF_REBATE_VNDR_SPEND_MV' --Materialized View  Name
                                        );
    ELSE
      l_success := -141;
      --dbms_output.put_line('Success = ' || l_success);
      fnd_file.put_line(fnd_file.log, l_sec || ':  Failed = ' || l_success);
      RAISE submitprog_failed;
    END IF;
    -- --------------------------------------
    -- 2 request
    -- --------------------------------------
    IF l_ok AND l_success = 0
    THEN

      l_sec := 'MV_REFRESH (2nd request) refresh Materialized Views (Purchases MV)';
      --dbms_output.put_line(l_sec);
      fnd_file.put_line(fnd_file.log, l_sec);

      l_ok := fnd_submit.submit_program(application => 'XXCUS'
                                       ,program     => 'XXCUSOZF_MV_REFRESH'
                                       ,stage       => 'MV_REFRESH'
                                       ,argument1   => 'XXCUSOZF_RBTINCOME_MV' --Materialized View  Name
                                        );
    ELSE
      l_success := -142;
      --dbms_output.put_line('Success = ' || l_success);
      fnd_file.put_line(fnd_file.log, l_sec || ':  Failed = ' || l_success);
      RAISE submitprog_failed;
    END IF;

    -- --------------------------------------
    -- 3 request
    -- --------------------------------------
    IF l_ok AND l_success = 0
    THEN

      l_sec := 'MV_REFRESH  (3rd request) refresh Materialized Views (Purchases MV)';
      --dbms_output.put_line(l_sec);
      fnd_file.put_line(fnd_file.log, l_sec);

      l_ok := fnd_submit.submit_program(application => 'XXCUS'
                                       ,program     => 'XXCUSOZF_MV_REFRESH'
                                       ,stage       => 'MV_REFRESH'
                                       ,argument1   => 'XXCUSOZF_PURCHASES_MV' --Materialized View  Name
                                        );
    ELSE
      l_success := -143;
      --dbms_output.put_line('Success = ' || l_success);
      fnd_file.put_line(fnd_file.log, l_sec || ':  Failed = ' || l_success);
      RAISE submitprog_failed;
    END IF;
    -- --------------------------------------
    -- New stage EXTRACT_YTD_INCOME_B with 1 request
    -- --------------------------------------
    IF l_ok AND l_success = 0
    THEN

      l_sec := '@Seq: 65, HDS Rebates: Extract YTD Income -By Period';

      fnd_file.put_line(fnd_file.log, l_sec);

      l_ok := fnd_submit.submit_program(application => 'XXCUS'
                                       ,program     => 'XXCUS_OZF_YTD_INCOME_PERIOD'
                                       ,stage       => 'EXTRACT_YTD_INCOME_B'
                                       ,argument1   => p_fperiod --Fiscal Period
                                        );
    ELSE
      l_success := -145.1;
      fnd_file.put_line(fnd_file.log, l_sec || ':  Failed = ' || l_success);
      RAISE srs_failed;
    END IF;
    --
    -- --------------------------------------
    -- New stage EXTRACT_GL_SUMMARY with 1 request
    -- --------------------------------------
    IF l_ok AND l_success = 0
    THEN

      l_sec := '@Seq: 68, HDS Rebates: Extract GL Summary Data -By Period';

      fnd_file.put_line(fnd_file.log, l_sec);

      l_ok := fnd_submit.submit_program(application => 'XXCUS'
                                       ,program     => 'XXCUS_OZF_GL_SUMMARY'
                                       ,stage       => 'EXTRACT_GL_SUMMARY'
                                       ,argument1   => p_fperiod --Fiscal Period
                                        );
    ELSE
      l_success := -145.2;
      fnd_file.put_line(fnd_file.log, l_sec || ':  Failed = ' || l_success);
      RAISE srs_failed;
    END IF;
    --
    -- --------------------------------------
    -- New stage PAM_REFRESH with 1 request
    -- --------------------------------------
    IF l_ok AND l_success = 0
    THEN

      l_sec := 'PAM_REFRESH HDS TM PAM Refresh';
      --dbms_output.put_line(l_sec);
      fnd_file.put_line(fnd_file.log, l_sec);

      l_ok := fnd_submit.submit_program(application => 'XXCUS'
                                       ,program     => 'XXCUS_PAM_REFRESH'
                                       ,stage       => 'PAM_REFRESH');
    ELSE
      l_success := -144.1;
      --dbms_output.put_line('Success = ' || l_success);
      fnd_file.put_line(fnd_file.log, l_sec || ':  Failed = ' || l_success);
      RAISE submitprog_failed;
    END IF;
    -- Bala 03/02/2015
    -- --------------------------------------
    -- Second request under stage PAM_REFRESH
    -- --------------------------------------
    IF l_ok AND l_success = 0
    THEN

      l_sec := 'PAM_REFRESH HDS Rebates: Update Collector Program';
      fnd_file.put_line(fnd_file.log, l_sec);

      l_ok := fnd_submit.submit_program(application => 'XXCUS'
                                       ,program     => 'XXCUSOZF_COLLECT'
                                       ,stage       => 'PAM_REFRESH');
    ELSE
      l_success := -144.2;
      --dbms_output.put_line('Success = ' || l_success);
      fnd_file.put_line(fnd_file.log, l_sec || ':  Failed = ' || l_success);
      RAISE submitprog_failed;
    END IF;
    --
    -- --------------------------------------
    -- New stage STAGE_PAM_STATUS -Notify PAM Refresh Status
    -- --------------------------------------
    IF l_ok AND l_success = 0
    THEN
      --
      l_sec := 'STAGE_PAM_STATUS -Notify PAM Refresh Status';
      --
      --
      BEGIN
        SELECT upper(NAME) INTO v_current_db_name FROM v$database;
      EXCEPTION
        WHEN OTHERS THEN
          v_current_db_name := 'NA';
          print_log('Unable to get current DB Name from v$database, message =' ||
                    SQLERRM);
      END;
      --
      IF (v_current_db_name = v_prod_db_name)
      THEN
        v_email := v_prod_rbt_email_id;
      ELSE
        BEGIN
          SELECT email_address
            INTO v_email
            FROM fnd_user
           WHERE 1 = 1
             AND user_id = fnd_global.user_id;
        EXCEPTION
          WHEN OTHERS THEN
            v_email := 'NA';
            print_log('@get v_email , error message =' || SQLERRM);
        END;
      END IF;
      --
      --
      fnd_file.put_line(fnd_file.log, l_sec);

      l_ok := fnd_submit.submit_program(application => 'XXCUS'
                                       ,program     => 'XXCUS_OZF_EMAIL_PAM_STATUS'
                                       ,stage       => 'STAGE_PAM_STATUS'
                                       ,argument1   => v_email);
    ELSE
      l_success := -244;
      --dbms_output.put_line('Success = ' || l_success);
      fnd_file.put_line(fnd_file.log, l_sec || ':  Failed = ' || l_success);
      RAISE submitprog_failed;
    END IF;
  /* --Bala 03/02/2015 -Begin retrofit ESMS: S269624 and S275346 RFC: RFC42900 changes with SVN revision 3476.
    --
    -- --------------------------------------
    -- New stage STAGE_45 with 2 request
    -- --------------------------------------
    IF l_ok AND l_success = 0
    THEN

      l_sec := 'STAGE_45 HDS Refresh Rebates Materialized Views (Accruals MV)';
      --dbms_output.put_line(l_sec);
      fnd_file.put_line(fnd_file.log, l_sec);

      l_ok := fnd_submit.submit_program(application => 'XXCUS'
                                       ,program     => 'XXCUSOZF_MV_REFRESH'
                                       ,stage       => 'STAGE_45'
                                       ,argument1   => 'XXCUSOZF_ACCRUALS_MV' --Materialized View  Name
                                        );
    ELSE
      l_success := -145;
      --dbms_output.put_line('Success = ' || l_success);
      fnd_file.put_line(fnd_file.log, l_sec || ':  Failed = ' || l_success);
      RAISE submitprog_failed;
    END IF;
  */   --Begin retrofit ESMS: S269624 and S275346 RFC: RFC42900 changes with SVN revision 3476.
    -- --------------------------------------
    -- 2nd request
    -- --------------------------------------
    IF l_ok AND l_success = 0
    THEN

      l_sec := 'STAGE_45 (2nd request) refresh Materialized Views (BU Spend MV)';
      --dbms_output.put_line(l_sec);
      fnd_file.put_line(fnd_file.log, l_sec);

      l_ok := fnd_submit.submit_program(application => 'XXCUS'
                                       ,program     => 'XXCUSOZF_MV_REFRESH'
                                       ,stage       => 'STAGE_45'
                                       ,argument1   => 'XXCUSOZF_BU_SPEND_MV' --Materialized View  Name
                                        );
    ELSE
      l_success := -147;
      --dbms_output.put_line('Success = ' || l_success);
      fnd_file.put_line(fnd_file.log, l_sec || ':  Failed = ' || l_success);
      RAISE submitprog_failed;
    END IF;
    --
    -- Begin Ver 2.9
    -- Submit custom SLA Exceptions report
    --
    IF l_ok AND l_success = 0
    THEN
     --
      l_sec := 'STAGE_90 Rebates SLA Exceptions';
      --
      fnd_file.put_line(fnd_file.log, l_sec);
      --
      l_ok := fnd_submit.submit_program(application => 'XXCUS'
                                       ,program     => 'XXCUS_OZF_SLA_PER_CL_EXCEP_RPT'
                                       ,stage       => 'STAGE_90'
                                       ,argument1   => p_fperiod --fiscal period
                                        );
      --
    ELSE
      l_success := -947;
      print_log(l_sec || ':  Failed = ' || l_success);
      RAISE submitprog_failed;
    END IF;
    --
    -- End Ver 2.9
    --
    -- Begin Ver 3.1
    -- Submit custom rebates weekly load receipts audit report
    --
    IF l_ok AND l_success = 0
    THEN
         --
         -- Begin ver 3.2
         begin
          --
          open c_xml_defn;
          fetch c_xml_defn into c_xml_defn_rec.dflt_lang, c_xml_defn_rec.dflt_territory, c_xml_defn_rec.dflt_output_type ;
          close c_xml_defn;
          --
         exception
          when others then
           c_xml_defn_rec.dflt_lang :='en';
           c_xml_defn_rec.dflt_territory :='00';
           c_xml_defn_rec.dflt_output_type :='PDF';
         end;
         -- End ver 3.2
         --
         if get_instance() =l_prod_instance then
          v_email :=l_email_id_prd;
         else
          v_email :=l_email_id_non_prd;
         end if;
     --
      l_sec := 'STAGE_100 -Audit Report';
      --
      fnd_file.put_line(fnd_file.log, l_sec);
      --
      -- Begin ver 3.2
      l_ok :=fnd_submit.add_layout
                    (
                      template_appl_name =>'XXCUS',
                      template_code =>l_rebates_audit_job_code,
                      template_language =>c_xml_defn_rec.dflt_lang,
                      template_territory =>c_xml_defn_rec.dflt_territory,
                      output_format =>c_xml_defn_rec.dflt_output_type
                    );
      -- End ver 3.2
      l_ok := fnd_submit.submit_program(application => 'XXCUS'
                                       ,program     =>l_rebates_audit_job_code --ver 3.2
                                       ,stage       => 'STAGE_100'
                                       ,argument1   =>v_email
                                        );
      --
    ELSE
      l_success := -957;
      print_log(l_sec || ':  Failed = ' || l_success);
      RAISE submitprog_failed;
    END IF;
    --
    -- End Ver 3.1
    --
    -- -----------------------------------------------
    -- All requests in the set have been submitted now
    -- -----------------------------------------------
    --
    -- So far we have submitted the individual requests for each stage. now submit the request set.
    --
    IF l_ok AND l_success = 0
    THEN
      l_request_id := fnd_submit.submit_set(NULL, FALSE);
      --dbms_output.put_line('Request_id = ' || l_request_id);
      fnd_file.put_line(fnd_file.log, 'Request_id = ' || l_request_id);
      COMMIT;

    ELSE
      l_success := -1000;
      --dbms_output.put_line('Success = ' || l_success);
      fnd_file.put_line(fnd_file.log, 'Failed = ' || l_success);
      RAISE submitset_failed;
    END IF;

  EXCEPTION
    WHEN srs_failed THEN
      ROLLBACK;
      l_err_code := 2;
      l_err_msg  := 'Call to set_request_set failed: ' || fnd_message.get ||
                    ' Error_Stack...' || dbms_utility.format_error_stack() ||
                    ' Error_Backtrace...' ||
                    dbms_utility.format_error_backtrace();
      dbms_output.put_line(l_err_msg);
      fnd_file.put_line(fnd_file.log, l_err_msg);
      fnd_file.put_line(fnd_file.output, l_err_msg);
      l_err_callpoint := l_sec;

      xxcus_error_pkg.xxcus_error_main_api(p_called_from       => l_err_callfrom
                                          ,p_calling           => l_err_callpoint
                                          ,p_request_id        => l_request_id
                                          ,p_ora_error_msg     => substr(regexp_replace(l_err_msg
                                                                                       ,'[[:cntrl:]]'
                                                                                       ,NULL)
                                                                        ,1
                                                                        ,2000)
                                          ,p_error_desc        => substr(l_err_msg
                                                                        ,1
                                                                        ,240)
                                          ,p_distribution_list => l_distro_list
                                          ,p_module            => 'TM');

    WHEN submitprog_failed THEN
      fnd_file.put_line(fnd_file.log, l_sec);
      fnd_file.put_line(fnd_file.output, l_sec);
      l_err_code := 2;
      l_err_msg  := 'Call to submit_program failed: ' || fnd_message.get ||
                    ' Error_Stack...' || dbms_utility.format_error_stack() ||
                    ' Error_Backtrace...' ||
                    dbms_utility.format_error_backtrace();
      dbms_output.put_line(l_err_msg);
      fnd_file.put_line(fnd_file.log, l_err_msg);
      fnd_file.put_line(fnd_file.output, l_err_msg);
      l_err_callpoint := l_sec;

      xxcus_error_pkg.xxcus_error_main_api(p_called_from       => l_err_callfrom
                                          ,p_calling           => l_err_callpoint
                                          ,p_request_id        => l_request_id
                                          ,p_ora_error_msg     => substr(regexp_replace(l_err_msg
                                                                                       ,'[[:cntrl:]]'
                                                                                       ,NULL)
                                                                        ,1
                                                                        ,2000)
                                          ,p_error_desc        => substr(l_err_msg
                                                                        ,1
                                                                        ,240)
                                          ,p_distribution_list => l_distro_list
                                          ,p_module            => 'TM');

    WHEN submitset_failed THEN
      fnd_file.put_line(fnd_file.log, l_sec);
      fnd_file.put_line(fnd_file.output, l_sec);
      l_err_code := 2;
      l_err_msg  := 'Call to submit_set failed: ' || fnd_message.get ||
                    ' Error_Stack...' || dbms_utility.format_error_stack() ||
                    ' Error_Backtrace...' ||
                    dbms_utility.format_error_backtrace();
      dbms_output.put_line(l_err_msg);
      fnd_file.put_line(fnd_file.log, l_err_msg);
      fnd_file.put_line(fnd_file.output, l_err_msg);
      l_err_callpoint := l_sec;

      xxcus_error_pkg.xxcus_error_main_api(p_called_from       => l_err_callfrom
                                          ,p_calling           => l_err_callpoint
                                          ,p_request_id        => l_request_id
                                          ,p_ora_error_msg     => substr(regexp_replace(l_err_msg
                                                                                       ,'[[:cntrl:]]'
                                                                                       ,NULL)
                                                                        ,1
                                                                        ,2000)
                                          ,p_error_desc        => substr(l_err_msg
                                                                        ,1
                                                                        ,240)
                                          ,p_distribution_list => l_distro_list
                                          ,p_module            => 'TM');

    WHEN OTHERS THEN
      fnd_file.put_line(fnd_file.log, l_sec);
      fnd_file.put_line(fnd_file.output, l_sec);
      l_err_code := 2;
      l_err_msg  := 'Request set submission failed - unknown error: ' ||
                    ' Error_Stack...' || dbms_utility.format_error_stack() ||
                    ' Error_Backtrace...' ||
                    dbms_utility.format_error_backtrace();
      dbms_output.put_line(l_err_msg);
      fnd_file.put_line(fnd_file.log, l_err_msg);
      fnd_file.put_line(fnd_file.output, l_err_msg);
      l_err_callpoint := l_sec;

      xxcus_error_pkg.xxcus_error_main_api(p_called_from       => l_err_callfrom
                                          ,p_calling           => l_err_callpoint
                                          ,p_request_id        => l_request_id
                                          ,p_ora_error_msg     => substr(regexp_replace(l_err_msg
                                                                                       ,'[[:cntrl:]]'
                                                                                       ,NULL)
                                                                        ,1
                                                                        ,2000)
                                          ,p_error_desc        => substr(l_err_msg
                                                                        ,1
                                                                        ,240)
                                          ,p_distribution_list => l_distro_list
                                          ,p_module            => 'TM');

  END submit_request_set;

END xxcus_tm_interface_pkg;
/