CREATE OR REPLACE PACKAGE BODY APPS."XXCUSOZF_PAM_PKG" IS

  -- -----------------------------------------------------------------------------
  -- |----------------------------< XXCUSOZF_PAM_PKG >--------------------------------|
  -- -----------------------------------------------------------------------------
  --
  --   VERSION DATE          AUTHOR(S)       DESCRIPTION
  -- ------- -----------   --------------- ---------------------------------
  -- 1.0     17-APR-2013   Dragan           Created this package.  RFC 37489
  --                                       Package used by the PAM project
  -- 1.1     14-Oct-2013   Dragan          Added parms to allow concurrent request
  --                                       to run RFC 38620 SR 228763
  -- 1.2    10/14/2014    Bala Seshadri  ESMS 265235. Invoke email notifiction at the end of the repop_main routine
  --  RFC 42038 / ESMS 267539 10/25/2014   Bala Seshadri Remove logic to send email. This logic is moved out as a new stage
  --                                                     within the custom sunday request set
  -- 1.3    03/03/2015   Bala Seshadri    ESMS  261553. Add logic to point the eaapxprd db link to a non prod instance
  --                                     or PRD instance depending on the instance the script runs
  -- 1.4   06/02/2015    Bala Seshadri  ESMS 290358.  Add contract_nbr and plan_id to the repop_program routine.
  --1.5   10/07/2015    Bala Seshadri  TMS 20151001-00046 / ESMS 303620
  --1.6   04/08/2016    Bala Seshadri  TMS 20160407-00172 / ESMS 322629, EBSPRD migration related fixes FOR GSC objects
  --1.7   07/19/2016    Bala Seshadri  TMS 20160719-00053 / ESMS 398770 - Update XXCUS PAM Refresh log to include start/stop times per repop step
  --1.8  02/02/2017    Bala Seshadri  TMS  20170202-00054 / ESMS 538724 - PAM components redesign [EBS Supplier Rebate Dashboard]
  --error handling
  g_err_email  VARCHAR2(200) := 'HDSOracleDevelopers@hdsupply.com';
  g_err_email2 VARCHAR2(200) :='HDSRebateManagementSystemNotifications@hdsupply.com'; --1.8
  --
  procedure print_log(p_message in varchar2) is
  begin
   if fnd_global.conc_request_id >0 then
    fnd_file.put_line(fnd_file.log, p_message);
   else
    dbms_output.put_line(p_message);
   end if;
  exception
   when others then
    fnd_file.put_line(fnd_file.log, 'Issue in print_log routine ='||sqlerrm);
  end print_log;
  --
  procedure set_globals is
    --
    l_err_program VARCHAR2(75) := 'XXCUSOZF_PAM_PKG.set_globals';
    l_sec         VARCHAR2(4000) DEFAULT 'Starting';
    --
  begin
    --
    l_sec := '@ begin : '||l_err_program;
    print_log(l_sec);
    --
    select (period_year-5) from_year
                ,period_year to_year
    into    xxcusozf_pam_pkg.g_from_year
              ,xxcusozf_pam_pkg.g_to_year
    from apps.gl_periods
    where 1 =1
    and sysdate between start_date and end_date
    and adjustment_period_flag ='N';
    --
    print_log('Global variables : extract from year =>'||xxcusozf_pam_pkg.g_from_year||', extract to year =>'||xxcusozf_pam_pkg.g_to_year);
    --
    l_sec := '@ get fiscal start date for year : '||xxcusozf_pam_pkg.g_from_year||', get fiscal end date for year : '||xxcusozf_pam_pkg.g_to_year;
    print_log(l_sec);
    --
    select min(start_date)
               ,max(end_date)
    into     xxcusozf_pam_pkg.g_extract_fiscal_start
               ,xxcusozf_pam_pkg.g_extract_fiscal_end
    from  apps.ozf_time_ent_year
    where 1 =1
    and ent_year_id >=xxcusozf_pam_pkg.g_from_year
    and ent_year_id <=xxcusozf_pam_pkg.g_to_year
    ;
    --
    print_log('Global variables : g_extract_fiscal_start =>'||to_char(xxcusozf_pam_pkg.g_extract_fiscal_start, 'mm/dd/yyyy')||', g_extract_fiscal_end =>'||to_char(xxcusozf_pam_pkg.g_extract_fiscal_end, 'mm/dd/yyyy'));
    --
    l_sec := '@ end : '||l_err_program;
    print_log(l_sec);
    --
  exception
   when others then
    print_log('When Other Errors in get_year: Message =>'||sqlerrm);
      xxcus_error_pkg.xxcus_error_main_api(p_called_from       => l_err_program
                                          ,p_calling           => l_sec
                                          ,p_ora_error_msg     => SQLERRM
                                          ,p_error_desc        => 'Error running package with PROGRAM ERROR'
                                          ,p_distribution_list => g_err_email
                                          ,p_module            => 'HDS Rebates : PAM');
  end set_globals;
  --
  PROCEDURE repop_main (pam_err out varchar2,
                        pam_sec out number
                        )
                        IS
    -- -----------------------------------------------------------------------------
    -- Name           : repop_vend_lov
    -- Date Written   : 16-JAN-2013
    -- Author         : Manny Rodriguez
    --
    -- Description : delete and Repopulate tables
    --
    --
    -- Modification History:
    --
    -- When         Who             Comments
    -- -----------  --------        -----------------------------------------------------
    -- 15-JAN-2013  Manny           Created
    -- 20-MAR-2013  Dragan          Added repop_pam_lob_purchase.
    -- 03-MAR-2015  Bala Seshadri   Logic to verify db links and drop /recreate if necessary
    ---------------------------------------------------------------------------------
    --error handling
    l_err_program           VARCHAR2(75) := 'XXCUSOZF_PAM_PKG.repop_main';
    l_sec                   VARCHAR2(4000) DEFAULT 'Starting';
    --
    ln_request_id           NUMBER :=fnd_global.conc_request_id; --Ver 1.6
    l_user_id number :=fnd_global.user_id; --Ver 1.6
    v_email                 VARCHAR2(60) :=Null;
    --
    v_curent_db_name        VARCHAR2(20) :='NA';
    --v_prod_db_name          VARCHAR2(20) :='EBIZPRD'; --1.6
    v_prod_db_name          VARCHAR2(20) :='EBSPRD'; --1.6
    v_prod_rbt_email_id     VARCHAR2(40) :='hdsoraclerebates@hdsupply.com';
    --parms
    b_proceed               BOOLEAN;
    --v_db_prd                VARCHAR2(15) :='EBIZPRD';  --1.6
    v_db_prd                VARCHAR2(15) :='EBSPRD';  --1.6
    --
    v_current_db            VARCHAR2(15) :=Null;
    n_count                 NUMBER       :=0;
    --
    v_db_link_sql           VARCHAR2(2000) :=Null;
    --
    v_error_msg             VARCHAR2(240) :=Null;
    v_date_format           VARCHAR2(240) :=Null;
    --
  BEGIN
   --
   -- get current database
   --
   begin
    --
    select value
    into    v_date_format
    from   nls_database_parameters
    where  parameter = 'NLS_DATE_FORMAT';
    --
    --print_log('Set date format : Before change: v_date_format =>'||v_date_format);
    --
    dbms_session.set_nls ('nls_date_format','''DD-MON-YYYY''');
    --
    select value
    into    v_date_format
    from   nls_database_parameters
    where  parameter = 'NLS_DATE_FORMAT';
    --
    --print_log('Set date format : After change: v_date_format =>'||v_date_format);
    --
    select upper(name)
    into   v_current_db
    from   v$database;
    --
   exception
    when others then
     v_current_db :='NONE';
   end;
   --
   --If the current instance is other than EBIZPRD, we will check if the existing db link eaapxprd points to eaapxprd database.
   --When the db link eaapxprd points to eaapxprd database, we will drop the existing link and recreate it to point to eaapxqa
   --
   if (v_current_db =v_db_prd) then
    --
    -- when the instance is EBIZPRD, the existing db link eaapxprd will need to be pointing to eaapxprd database.
    -- We don't need to check further and lets move on.
    --
    b_proceed :=TRUE;
    --
   else
    --
    -- when the instance is non EBIZPRD, then check if the  existing db link eaapxprd
    -- host entries point to eaapxprd. If so drop and recreate it using eaapxqa. If it's already pointing to eaapxqa
    -- just move on.
    -- If we drop and recreate the db link make sure we can query it. If we can query then the db link is active and good to go
    -- else email to the developer group and also error the concurrent program
    --
    begin
     --
      select count(1)
      into   n_count
      from   sys.user_db_links
      where  1 =1
        and  db_link like 'EAAPXPRD%'
        and  (upper(host) like '%EAAPXQA%' or upper(host) like '%EAAPXDEV%'); -- Ver 1.8
     --
     if n_count >=1 then
      --
      -- The existing db link eaapxprd is already pointing to eaapxqa database. No need to check further.
      --
      b_proceed :=TRUE;
      --
     else
      --
      -- First: Check to see if the DB link exists and then drop the db link. If no link exists then move on
      -- Second: Create the db link
      -- Third: Query a sample SQL using the db link to make sure the link is active and works fine.
      -- If not something is wrong, send an email to the developer group and also error the concurrent program
      --
      begin
       --
       begin
        --
        select count(1)
        into   n_count
        from   sys.user_db_links
        where  1 =1
          and  db_link ='EAAPXPRD.HSI.HUGHESSUPPLY.COM';
        --
        if n_count >0 then
         --
         execute immediate 'DROP DATABASE LINK EAAPXPRD.HSI.HUGHESSUPPLY.COM';
         --
         print_log('After dropping db link EAAPXPRD.HSI.HUGHESSUPPLY.COM');
         --
        end if;
        --
        begin
        --
        v_db_link_sql :=Null;
        --
                    -- Begin Ver 1.8
                    if v_current_db ='EBSDEV' then
                      --
                        v_db_link_sql :='create database link EAAPXPRD
                                          connect to INTERFACE_ORACLER12 identified by pa$$w0rd
                                          using ''(DESCRIPTION =    (ADDRESS = (PROTOCOL = TCP)(HOST = eaapxdev.hdsupply.net)(PORT = 1521))
                                                  (CONNECT_DATA =      (SERVER = DEDICATED)      (SERVICE_NAME = eaapxdev)))''
                                                ';
                        --
                    elsif v_current_db ='EBSQA' then
                      --
                        v_db_link_sql :='create database link EAAPXPRD
                                          connect to INTERFACE_ORACLER12 identified by pa$$w0rd
                                          using ''(DESCRIPTION =    (ADDRESS = (PROTOCOL = TCP)(HOST = eaapxqa.hdsupply.net)(PORT = 1521))
                                                  (CONNECT_DATA =      (SERVER = DEDICATED)      (SERVICE_NAME = eaapxqa)))''
                                                ';
                        --
                    else
                     Null;
                    end if;
                    -- End Ver 1.8
                    --
                    print_log('v_db_link_sql ='||v_db_link_sql);
                    --
                    execute immediate v_db_link_sql;
                     --
                    print_log('After creating db link EAAPXPRD.HSI.HUGHESSUPPLY.COM');
                    --
            begin
             --
             select count(1)
             into   n_count
             from   ea_apps.pam_vend_lov_tbl@eaapxprd;
             --
             print_log(' ');
             print_log(' ****** RULE: In a NON EBIZPRD EBS instance, DB LINK EAAPXPRD must point to EAAPXQA apex database.');
             print_log(' ');
             print_log(' ****** BEGIN CHECK TO SEE IF THE DB LINK EAAPXPRD WORKS *******');
             print_log(' ');
             print_log('@ea_apps.pam_vend_lov_tbl@eaapxprd, n_count ='||n_count);
             print_log(' ');
             print_log('Note: If n_count >0 then the DB link eaapxprd works by pointing to eaapxqa apex database');
             print_log(' ');
             print_log(' ****** END CHECK TO SEE IF THE DB LINK EAAPXPRD WORKS *******');
             --
             b_proceed :=TRUE;
             --
            exception
             when others then
                 --
                 print_log(' ');
                 print_log(' ****** RULE: In a NON EBIZPRD EBS instance, DB LINK EAAPXPRD must point to EAAPXDEV or EAAPXQA apex database.'); -- Ver 1.8
                 print_log(' ');
                 print_log(' ****** BEGIN CHECK TO SEE IF THE DB LINK EAAPXPRD WORKS *******');
                 print_log(' ');
                 print_log('@ea_apps.pam_vend_lov_tbl@eaapxprd, n_count =0');
                 print_log(' ');
                 print_log('Note: If n_count =0 then the DB link eaapxprd failed in SQL. Please check the db link creation.');
                 print_log(' ');
                 print_log(' ****** END CHECK TO SEE IF THE DB LINK EAAPXPRD WORKS *******');
                 --
              print_log(' ');
              print_log('@103, msg ='||sqlerrm);
              print_log(' ');
              b_proceed :=FALSE;
            end;
        --
        exception
         when others then
           print_log('@102, msg ='||sqlerrm);
           b_proceed :=FALSE;
        end;
        --
       exception
        when others then
         b_proceed :=FALSE;
         print_log('@104, msg ='||sqlerrm);
       end;
       --
      exception
       when others then
        b_proceed :=FALSE;
        print_log('@105, msg ='||sqlerrm);
      end;
      --
     end if;
     --
    exception
     --
     when others then
       b_proceed :=FALSE;
       print_log('@106, msg ='||sqlerrm);
       --
    end;
    --
   end if;
   --
   -- Doesn't matter which instance, the variable b_proceed needs to be TRUE to start populating the APEX tables
   --
   if (b_proceed) then
    --
    Null;
    --
    --Begin Ver 1.8
     --
     --xxcusozf_pam_pkg.g_customer_id :=p_customer_id;
     --
     --xxcusozf_pam_pkg.g_year :=to_char(xxcusozf_pam_pkg.get_year());
     --
     set_globals;
     --
     if  (xxcusozf_pam_pkg.g_from_year is null) then
         print_log('Failed to derive variable xxcusozf_pam_pkg.g_from_year, msg =>'||sqlerrm);
         raise program_error;
     else
             --if  (xxcusozf_pam_pkg.g_to_year is null) then
                 --print_log('Failed to derive variable xxcusozf_pam_pkg.g_to_year, msg =>'||sqlerrm);
                 --raise program_error;
             --else
                 --Null;
                 --print_log('@Get extract from year value, xxcusozf_pam_pkg.g_from_year ='||xxcusozf_pam_pkg.g_year);
             --end if;
             --
                 print_log(' ');
                 print_log('Begin PAM EBS extracts...'||to_char(sysdate, 'DD-MON-YYYY HH24:MI:SS'));
                  --
                    print_log(' ');
                    print_log('Begin PAM extract_bu_info extract...'||to_char(sysdate, 'DD-MON-YYYY HH24:MI:SS'));
                    xxcusozf_pam_pkg.extract_bu_info (p_user_id =>l_user_id, p_request_id =>ln_request_id);
                    print_log('End PAM extract_bu_info extract...'||to_char(sysdate, 'DD-MON-YYYY HH24:MI:SS'));
                    print_log(' ');
                  --
                    print_log('Begin PAM extract_vend_lov extract...'||to_char(sysdate, 'DD-MON-YYYY HH24:MI:SS'));
                    xxcusozf_pam_pkg.extract_vend_lov (p_user_id =>l_user_id, p_request_id =>ln_request_id);
                    print_log('End PAM extract_vend_lov extract...'||to_char(sysdate, 'DD-MON-YYYY HH24:MI:SS'));
                    print_log(' ');
                  --
                    print_log('Begin PAM extract_program_lov extract...'||to_char(sysdate, 'DD-MON-YYYY HH24:MI:SS'));
                    xxcusozf_pam_pkg.extract_program_lov (p_user_id =>l_user_id, p_request_id =>ln_request_id);
                    print_log('End PAM extract_program_lov extract...'||to_char(sysdate, 'DD-MON-YYYY HH24:MI:SS'));
                    print_log(' ');
                  --
                    print_log('Begin PAM extract_pam_purch extract...'||to_char(sysdate, 'DD-MON-YYYY HH24:MI:SS'));
                    xxcusozf_pam_pkg.extract_pam_purch (p_user_id =>l_user_id, p_request_id =>ln_request_id);
                    print_log('End PAM extract_pam_purch extract...'||to_char(sysdate, 'DD-MON-YYYY HH24:MI:SS'));
                    print_log(' ');
                  --
                    print_log('Begin PAM extract_pam_income extract...'||to_char(sysdate, 'DD-MON-YYYY HH24:MI:SS'));
                    xxcusozf_pam_pkg.extract_pam_income (p_user_id =>l_user_id, p_request_id =>ln_request_id);
                    print_log('End PAM extract_pam_income extract...'||to_char(sysdate, 'DD-MON-YYYY HH24:MI:SS'));
                    print_log(' ');
                  --
                    print_log('Begin PAM extract_pam_payments extract...'||to_char(sysdate, 'DD-MON-YYYY HH24:MI:SS'));
                    xxcusozf_pam_pkg.extract_pam_payments (p_user_id =>l_user_id, p_request_id =>ln_request_id);
                    print_log('End PAM extract_pam_payments extract...'||to_char(sysdate, 'DD-MON-YYYY HH24:MI:SS'));
                    print_log(' ');
                  --
                    print_log('Begin PAM extract_cal_pam_incomelob extract...'||to_char(sysdate, 'DD-MON-YYYY HH24:MI:SS'));
                    xxcusozf_pam_pkg.extract_cal_pam_incomelob (p_user_id =>l_user_id, p_request_id =>ln_request_id);
                    xxcusozf_pam_pkg.pam_income_table_filler
                           (
                              p_from_year =>xxcusozf_pam_pkg.g_from_year
                             ,p_to_year  =>xxcusozf_pam_pkg.g_to_year
                           )
                           ;
                    print_log('End PAM extract_cal_pam_incomelob extract...'||to_char(sysdate, 'DD-MON-YYYY HH24:MI:SS'));
                    print_log(' ');
                  --
                    print_log('Begin PAM extract_cal_pam_paydet extract...'||to_char(sysdate, 'DD-MON-YYYY HH24:MI:SS'));
                    xxcusozf_pam_pkg.extract_cal_pam_paydet (p_user_id =>l_user_id, p_request_id =>ln_request_id);
                    print_log('End PAM extract_cal_pam_paydet extract...'||to_char(sysdate, 'DD-MON-YYYY HH24:MI:SS'));
                    print_log(' ');
                  --
                    print_log('Begin PAM extract_cal_pam_lob_purchase extract...'||to_char(sysdate, 'DD-MON-YYYY HH24:MI:SS'));
                    xxcusozf_pam_pkg.extract_cal_pam_lob_purchase (p_user_id =>l_user_id, p_request_id =>ln_request_id);
                    xxcusozf_pam_pkg.pam_purchase_table_filler
                           (
                              p_from_year =>xxcusozf_pam_pkg.g_from_year
                             ,p_to_year  =>xxcusozf_pam_pkg.g_to_year
                           )                    
                    ;
                    print_log('End PAM extract_cal_pam_lob_purchase extract...'||to_char(sysdate, 'DD-MON-YYYY HH24:MI:SS'));
                    print_log(' ');
                  --
                    print_log('Begin PAM backup_apex_pam_current_data...'||to_char(sysdate, 'DD-MON-YYYY HH24:MI:SS'));
                    xxcusozf_pam_pkg.backup_apex_pam_current_data;
                    print_log('End PAM backup_apex_pam_current_data...'||to_char(sysdate, 'DD-MON-YYYY HH24:MI:SS'));
                    print_log(' ');
                   --
                    print_log('Begin PAM transfer_data_to_pam_apex...'||to_char(sysdate, 'DD-MON-YYYY HH24:MI:SS'));
                    xxcusozf_pam_pkg.transfer_data_to_pam_apex;
                    print_log('End PAM transfer_data_to_pam_apex...'||to_char(sysdate, 'DD-MON-YYYY HH24:MI:SS'));
                    print_log(' ');
                   --
                 print_log('End PAM EBS extracts...'||to_char(sysdate, 'DD-MON-YYYY HH24:MI:SS'));
                 print_log(' ');
                 --
             --End Ver 1.8
             --
             print_log(' ');
             --
     end if;
     --
   else
    --
    print_log('Variable b_proceed was set to FALSE in a non EBIZPRD instance, exit now');
    --
    xxcus_error_pkg.xxcus_error_main_api
      (
       p_called_from       => l_err_program
      ,p_calling           => 'Check db link EAAPXPRD'
      ,p_ora_error_msg     => SQLERRM
      ,p_error_desc        => '@Check db link EAAPXPRD to verify if its pointing to EAAPXQA apex database.'
      ,p_distribution_list => g_err_email
      ,p_module            => 'Rebates PAM Refresh'
      );
    --
    v_error_msg :='Error found in using the db link eaapxprd in a non prod instance. Please check the concurrent log in full';
    --
    b_proceed :=FND_CONCURRENT.SET_COMPLETION_STATUS('ERROR', v_error_msg);
    --
   end if;
   --
  EXCEPTION
    WHEN OTHERS THEN
      xxcus_error_pkg.xxcus_error_main_api(p_called_from       => l_err_program
                                          ,p_calling           => l_sec
                                          ,p_ora_error_msg     => SQLERRM
                                          ,p_error_desc        => 'Error running package with PROGRAM ERROR'
                                          ,p_distribution_list => g_err_email
                                          ,p_module            => 'PAM');

  END repop_main;
  --
  -- Begin Ver 1.8
  --
  -- Get from year for use in extracts
  --
  function get_year return number is
    --
    l_err_program VARCHAR2(75) := 'XXCUSOZF_PAM_PKG.get_year';
    l_sec         VARCHAR2(4000) DEFAULT 'Starting';
    l_year       NUMBER :=Null;
    --
  begin
    --
   select  extract (year from add_months(trunc(sysdate), xxcusozf_pam_pkg.g_pull_PAM_extract_months))  --go back three years from current date to fetch the beginning year
   into      l_year
   from    dual;
    --
    l_sec := '@ '||l_err_program||', beginning year :'||l_year;
    print_log(l_sec);
    --
    return l_year;
    --
  exception
   when others then
    print_log('When Other Errors in get_year: Message =>'||sqlerrm);
      xxcus_error_pkg.xxcus_error_main_api(p_called_from       => l_err_program
                                          ,p_calling           => l_sec
                                          ,p_ora_error_msg     => SQLERRM
                                          ,p_error_desc        => 'Error running package with PROGRAM ERROR'
                                          ,p_distribution_list => g_err_email
                                          ,p_module            => 'HDS Rebates : PAM');
     return -1;
  end get_year;
  --
  -- Extract business unit and line of business information
  --
  PROCEDURE extract_bu_info (p_user_id in number, p_request_id in number) IS
    --
    l_request_id number :=p_request_id;
    l_err_program VARCHAR2(75) := 'XXCUSOZF_PAM_PKG.extract_bu_info';
    l_sec         VARCHAR2(4000) DEFAULT 'Starting';
    --
  BEGIN
    --
    l_sec := '@ '||l_err_program||', Begin flush current records from EBS table xxcus.xxcus_pam_bu_name_tbl, Time :'||to_char(sysdate,'MM/DD/YYYY HH24:MI:SS');
    --
    delete xxcus.xxcus_pam_bu_name_tbl;
     --
    l_sec := '@ '||l_err_program||', End flush current records from EBS table xxcus.xxcus_pam_bu_name_tbl, deleted records :'||sql%rowcount||', Time :'||to_char(sysdate,'MM/DD/YYYY HH24:MI:SS');
    --
    l_sec := '@ '||l_err_program||', Begin Insert of EBS table xxcus.xxcus_pam_bu_name_tbl, Time :'||to_char(sysdate,'MM/DD/YYYY HH24:MI:SS');
    --
            insert into xxcus.xxcus_pam_bu_name_tbl
            (
            select bu_info.*
            from
            (
            select rownum id,
                       (
                         select territory_short_name
                         from   fnd_territories_vl
                         where 1 =1
                              and territory_code =(select country from hz_parties where party_id =hzrel.object_id)
                       ) country,
                      (
                          select /*+ RESULT_CACHE */ a.party_name
                          from   hz_parties a
                          where 1 =1
                          and a.party_id =hzrel.object_id
                      ) lob_name,
                      hzrel.object_id lob_id,
                     (
                       select /*+ RESULT_CACHE */ a.party_name
                       from   hz_parties a
                       where 1 =1
                       and a.party_id =hzrel.subject_id
                     ) bu_name,
                     hzrel.subject_id bu_id,
                     sysdate ebs_creation_date,
                    p_user_id ebs_created_by,
                    p_request_id request_id
            from hz_relationships hzrel
                      ,hz_parties bu
            where 1 =1
            and bu.attribute1 ='HDS_BU'
            and bu.attribute_category IN ('101', '102')
            and bu.attribute2 ='Y' --only PAM Extract Eligible
            and bu.party_id !=538552 --Ignore WHITECAP BU
            and hzrel.subject_id =bu.party_id
            and hzrel.relationship_type ='HEADQUARTERS/DIVISION'
            order by hzrel.object_id asc, hzrel.subject_id asc
            ) bu_info
            );
    --
    l_sec := '@ '||l_err_program||', End Insert of EBS table xxcus.xxcus_pam_bu_name_tbl, total records :'||sql%rowcount||', Time: '||to_char(sysdate,'MM/DD/YYYY HH24:MI:SS');
    --
    commit;
    --
  EXCEPTION
    WHEN OTHERS THEN
      print_log('@ '||l_err_program||', error in extract, msg => '||sqlerrm);
      xxcus_error_pkg.xxcus_error_main_api(p_called_from       => l_err_program
                                          ,p_calling           => l_sec
                                          ,p_request_id =>l_request_id
                                          ,p_ora_error_msg     => SQLERRM
                                          ,p_error_desc        => 'Error running package with PROGRAM ERROR'
                                          ,p_distribution_list => g_err_email
                                          ,p_module            => 'HDS Rebates : PAM');
  END extract_bu_info;
  --
  -- Extract master vendor list
  --
  PROCEDURE extract_vend_lov (p_user_id in number, p_request_id in number) IS
    --
    l_request_id number :=p_request_id;
    l_err_program VARCHAR2(75) := 'XXCUSOZF_PAM_PKG.extract_vend_lov';
    l_sec         VARCHAR2(4000) DEFAULT 'Starting';
    --
  BEGIN
    --
    l_sec := '@ '||l_err_program||', Begin flush current records from EBS table xxcus.xxcus_pam_vend_lov_tbl, Time :'||to_char(sysdate,'MM/DD/YYYY HH24:MI:SS');
    --
    delete xxcus.xxcus_pam_vend_lov_tbl;
     --
    l_sec := '@ '||l_err_program||', End flush current records from EBS table xxcus.xxcus_pam_vend_lov_tbl, deleted records :'||sql%rowcount||', Time :'||to_char(sysdate,'MM/DD/YYYY HH24:MI:SS');
    --
    l_sec := '@ '||l_err_program||', Begin Insert of EBS table xxcus.xxcus_pam_vend_lov_tbl, Time :'||to_char(sysdate,'MM/DD/YYYY HH24:MI:SS');
    --
    insert into xxcus.xxcus_pam_vend_lov_tbl
       (
           SELECT
                rownum
               ,mvid
               ,mv_name
               ,cust_id
               ,'ORACLE' source
               ,p_user_id ebs_created_by
               ,sysdate ebs_creation_date
               ,p_request_id request_id
           FROM xxcus_vend_lov_v
       );
    --
    l_sec := '@ '||l_err_program||', End Insert of EBS table xxcus.xxcus_pam_vend_lov_tbl, total records :'||sql%rowcount||', Time: '||to_char(sysdate,'MM/DD/YYYY HH24:MI:SS');
    --
    commit;
    --
  EXCEPTION
    WHEN OTHERS THEN
      print_log('@ '||l_err_program||', error in extract, msg => '||sqlerrm);
      xxcus_error_pkg.xxcus_error_main_api(p_called_from       => l_err_program
                                          ,p_calling           => l_sec
                                          ,p_request_id =>l_request_id
                                          ,p_ora_error_msg     => SQLERRM
                                          ,p_error_desc        => 'Error running package with PROGRAM ERROR'
                                          ,p_distribution_list => g_err_email
                                          ,p_module            => 'HDS Rebates : PAM');

  END extract_vend_lov;
  --
  -- Extract program / agreement list
  --
  PROCEDURE extract_program_lov (p_user_id in number, p_request_id in number) IS
    --
    l_request_id number :=p_request_id;
    l_err_program VARCHAR2(75) := 'XXCUSOZF_PAM_PKG.extract_program_lov';
    l_sec         VARCHAR2(4000) DEFAULT 'Starting';
    --l_year          varchar2(4) :=null;
    --
  BEGIN
    --
    l_sec := '@ '||l_err_program||', Begin flush current records from EBS table  xxcus.xxcus_pam_program_list_tbl, Time :'||to_char(sysdate,'MM/DD/YYYY HH24:MI:SS');
   print_log(l_sec);
    --
    delete xxcus.xxcus_pam_program_list_tbl;
     --
    l_sec := '@ '||l_err_program||', End flush current records from EBS table  xxcus.xxcus_pam_program_list_tbl, deleted records :'||sql%rowcount||', Time :'||to_char(sysdate,'MM/DD/YYYY HH24:MI:SS');
   print_log(l_sec);
    --
    l_sec := '@ '||l_err_program||', Begin Insert of EBS table  xxcus.xxcus_pam_program_list_tbl, Time :'||to_char(sysdate,'MM/DD/YYYY HH24:MI:SS');
   print_log(l_sec);
    --
    insert into xxcus.xxcus_pam_program_list_tbl
      (
        select
            rownum
           ,mvid
           ,mv_name
           ,agreement_yr
           ,agreement_status
           ,program_name
           ,program_description
           ,rebate_type
           ,agreement_name
           ,agreement_description
           ,payment_method
           ,payment_freq
           ,auto_renewal
           ,until_year
           ,activity_type
           ,autopay_enabled_flag
           ,global_flag
           ,back_to_or_min_guarantee_amt
           ,cust_id
           ,'ORACLE' SOURCE
           ,contract_nbr
           ,plan_id
           ,p_user_id ebs_created_by
           ,sysdate ebs_creation_date
           ,p_request_id request_id
           from xxcus_pam_program_v
           where 1 =1
                and agreement_yr >=xxcusozf_pam_pkg.g_from_year
      );
    --
    l_sec := '@ '||l_err_program||', End Insert of EBS table  xxcus.xxcus_pam_program_list_tbl, total records :'||sql%rowcount||', Time: '||to_char(sysdate,'MM/DD/YYYY HH24:MI:SS');
   print_log(l_sec);
    --
    commit;
    --
  EXCEPTION
    WHEN OTHERS THEN
      print_log('@ '||l_err_program||', error in extract, msg => '||sqlerrm);
      xxcus_error_pkg.xxcus_error_main_api(p_called_from       => l_err_program
                                          ,p_calling           => l_sec
                                          ,p_request_id =>l_request_id
                                          ,p_ora_error_msg     => SQLERRM
                                          ,p_error_desc        => 'Error running package with PROGRAM ERROR'
                                          ,p_distribution_list => g_err_email
                                          ,p_module            => 'HDS Rebates : PAM');

  END extract_program_lov;
  --
  -- Extract PAM purchases by lob/bu/rebate_type
  --
  PROCEDURE extract_pam_purch (p_user_id in number, p_request_id in number) IS
    --
    l_request_id number :=p_request_id;
    l_err_program VARCHAR2(75) := 'XXCUSOZF_PAM_PKG.extract_pam_purch';
    l_sec         VARCHAR2(4000) DEFAULT 'Starting';
    l_seq number :=0;
    l_period_name varchar2(10) :=Null;
    --
    cursor c_ytd_purchases_cal is
            select  /*+ parallel (p 2) */
                        hca.attribute2 mvid,
                        p.lob_id lob_id,
                        p.bu_id bu_id,
                        'CALENDAR' period_type,
                        p.calendar_year pam_year,
                        p.calendar_year sys_year,
                        sum (p.total_purchases) total_purchases,
                        p.mvid cust_id,
                        'ORACLE' source
                   from apps.xxcusozf_purchases_mv p,
                        apps.hz_cust_accounts hca,
                        apps.hz_parties z
                        --xxcus.xxcus_pam_bu_name_tbl pamlob
                  where     1 = 1
                        and p.mvid = hca.cust_account_id
                        and p.lob_id = z.party_id
                        and grp_mvid = 0
                        and grp_cal_year = 0
                        and grp_lob = 0
                        and grp_bu_id = 0
                        and grp_period = 1
                        and grp_qtr = 1
                        and grp_branch = 1
                        and grp_year = 1
                        and grp_cal_period = 0
                        --and p.lob_id =pamlob.lob_id
                        --and p.bu_id =pamlob.bu_id
                        and p.calendar_year >=xxcusozf_pam_pkg.g_from_year --g_year
               group by p.mvid,
                        hca.attribute2,
                        p.lob_id,
                        p.bu_id,
                        p.calendar_year
                        ;
    --
    cursor c_ytd_purchases_fiscal is
    SELECT /*+ PARALLEL (orl 4) PARALLEL (period 4) PARALLEL (qtr 4) PARALLEL (year 4) */
           (SELECT a.attribute2
              FROM apps.hz_parties a, apps.hz_cust_accounts b
             WHERE     a.party_id = b.party_id
                   AND b.cust_account_id = orl.sold_from_cust_account_id
                   AND b.attribute1 = 'HDS_MVID')
              mvid,
              orl.bill_to_party_id lob_id,
              orl.end_cust_party_id bu_id,
              'FISCAL' period_type,
              extract (year from date_ordered) pam_year, --nothing but calendar year
              year.ent_year_id sys_year, --nothing but fiscal year
              sum (orl.selling_price * orl.quantity) total_purchases,
              orl.sold_from_cust_account_id cust_id,
              'ORACLE' source
        from apps.ozf_resale_lines_all orl,
             apps.ozf_time_ent_period period,
             apps.ozf_time_ent_qtr qtr,
             apps.ozf_time_ent_year year,
             (
                select /*+ PARALLEL (a 4) */ a.*
                from    apps.ozf_time_day a
                where 1 =1
             ) t
       where     1 = 1
             and t.report_date = orl.date_ordered
             and orl.date_ordered >=to_date('01-JAN-'||xxcusozf_pam_pkg.g_from_year)
             and orl.date_ordered <=to_date('31-DEC-'||xxcusozf_pam_pkg.g_to_year)
             and t.ent_period_id = period.ent_period_id
             and t.ent_qtr_id = qtr.ent_qtr_id
             and t.ent_year_id = year.ent_year_id
    group by cube
                 (
                   orl.sold_from_cust_account_id,
                   orl.bill_to_party_id,
                   year.ent_year_id,
                   orl.end_cust_party_id,
                   extract (year from date_ordered)
                  )
    HAVING
                  (
                                 grouping (orl.sold_from_cust_account_id) =0
                         and grouping (orl.bill_to_party_id) =0
                         and grouping (orl.end_cust_party_id) =0
                         and grouping (year.ent_year_id) =0
                         and grouping(extract (year from date_ordered)) =0
                  )
                  ;
    --
  BEGIN
    --
    l_sec := '@ '||l_err_program||', Begin flush current records from EBS table xxcus.xxcus_pam_ytd_purch_tbl, Time :'||to_char(sysdate,'MM/DD/YYYY HH24:MI:SS');
    --
    print_log(l_sec);
    --
    if xxcusozf_pam_pkg.g_customer_id is null then
         delete xxcus.xxcus_pam_ytd_purch_tbl;
    else
          delete xxcus.xxcus_pam_ytd_purch_tbl where cust_id =xxcusozf_pam_pkg.g_customer_id;
    end if;
     --
    l_sec := '@ '||l_err_program||', End flush current records from EBS table xxcus.xxcus_pam_ytd_purch_tbl, deleted records :'||sql%rowcount||', Time :'||to_char(sysdate,'MM/DD/YYYY HH24:MI:SS');
    --
    print_log(l_sec);
    --
    l_sec := '@ '||l_err_program||',Calendar extract -begin insert of EBS table xxcus.xxcus_pam_ytd_purch_tbl, Time :'||to_char(sysdate,'MM/DD/YYYY HH24:MI:SS');
    --
    print_log(l_sec);
    --
    for c_rec in c_ytd_purchases_cal loop
       --
       l_seq :=l_seq +1;
       --
       insert into xxcus.xxcus_pam_ytd_purch_tbl
       (
          id
        ,mvid
        ,lob_id
        ,bu_id
        ,period_type
        ,pam_year
        ,sys_year
        ,total_purchases
        ,cust_id
        ,source
        ,ebs_created_by
        ,ebs_creation_date
        ,request_id
       )
       values
       (
          l_seq
        ,c_rec.mvid
        ,c_rec.lob_id
        ,c_rec.bu_id
        ,c_rec.period_type
        ,c_rec.pam_year
        ,c_rec.sys_year
        ,c_rec.total_purchases
        ,c_rec.cust_id
        ,c_rec.source
        ,p_user_id
        ,sysdate
        ,p_request_id
       )
       ;
       --
    end loop;
    --
    l_sec := '@ '||l_err_program||',Calendar extract - end of insert into EBS table xxcus.xxcus_pam_ytd_purch_tbl, total records :'||l_seq||', Time: '||to_char(sysdate,'MM/DD/YYYY HH24:MI:SS');
    --
    print_log(l_sec);
    --
    commit;
    --
    l_sec := '@ '||l_err_program||', Fiscal extract - begin insert into EBS table xxcus.xxcus_pam_ytd_purch_tbl, Time :'||to_char(sysdate,'MM/DD/YYYY HH24:MI:SS');
    --
    print_log(l_sec);
    --
    /*
    for c_rec in c_ytd_purchases_fiscal loop
       --
       l_seq :=l_seq +1;
       --
       insert into xxcus.xxcus_pam_ytd_purch_tbl
       (
          id
        ,mvid
        ,lob_id
        ,bu_id
        ,period_type
        ,pam_year
        ,sys_year
        ,total_purchases
        ,cust_id
        ,source
        ,ebs_created_by
        ,ebs_creation_date
        ,request_id
       )
       values
       (
          l_seq
        ,c_rec.mvid
        ,c_rec.lob_id
        ,c_rec.bu_id
        ,c_rec.period_type
        ,c_rec.pam_year
        ,c_rec.sys_year
        ,c_rec.total_purchases
        ,c_rec.cust_id
        ,c_rec.source
        ,p_user_id
        ,sysdate
        ,p_request_id
       )
       ;
       --
    end loop;
    --
    l_sec := '@ '||l_err_program||',Fiscal extract - end of insert into EBS table xxcus.xxcus_pam_ytd_purch_tbl, total records :'||l_seq||', Time: '||to_char(sysdate,'MM/DD/YYYY HH24:MI:SS');
    --
    print_log(l_sec);
    --

    commit;
    */
    --
  EXCEPTION
    WHEN OTHERS THEN
      print_log('@ '||l_err_program||', error in extract, msg => '||sqlerrm);
      xxcus_error_pkg.xxcus_error_main_api(p_called_from       => l_err_program
                                          ,p_calling           => l_sec
                                          ,p_request_id =>l_request_id
                                          ,p_ora_error_msg     => SQLERRM
                                          ,p_error_desc        => 'Error running package with PROGRAM ERROR'
                                          ,p_distribution_list => g_err_email
                                          ,p_module            => 'HDS Rebates : PAM');

  END extract_pam_purch;
  --
  -- extract pam ytd by income by lob/bu/agreement/period_type/rebate_type
  --
  PROCEDURE extract_pam_income (p_user_id in number, p_request_id in number) IS
    --
    l_request_id number :=p_request_id;
    l_err_program VARCHAR2(75) := 'XXCUSOZF_PAM_PKG.extract_pam_income';
    l_sec         VARCHAR2(4000) DEFAULT 'Starting';
    l_seq number :=0;
    --
    cursor c_ytd_income_cal is
     select /*+ parallel (ytdinc 2) */
            ytdinc.mvid,
            ytdinc.lob_id,
            pamlob.bu_id,
            ytdinc.plan_id,
            'CALENDAR' period_type,
            ytdinc.rebate_type,
            to_char (ytdinc.agreement_year) pam_year,
            to_char (ytdinc.agreement_year) sys_year,
            sum (ytdinc.accrual_amount) accrued_amt,
            ytdinc.ofu_cust_account_id cust_id,
            'ORACLE' source
       from xxcus.xxcus_ytd_income_b ytdinc
                   ,xxcus.xxcus_rebate_customers c
                   ,xxcus.xxcus_pam_bu_name_tbl pamlob
      where     1 = 1
            and c.customer_id = ytdinc.ofu_cust_account_id
            and c.customer_attribute1 = 'HDS_MVID'
            and ytdinc.agreement_year >=xxcusozf_pam_pkg.g_from_year
            and ytdinc.lob_id =pamlob.lob_id
            and ytdinc.bu =pamlob.bu_name
   group by
            ytdinc.ofu_cust_account_id,
            ytdinc.mvid,
            ytdinc.lob_id,
             pamlob.bu_id,
             ytdinc.plan_id,
            ytdinc.rebate_type,
            to_char (ytdinc.agreement_year)
            ;
     --
    cursor c_ytd_income_fiscal is
     select /*+ parallel (ytdinc 2) */
            ytdinc.mvid,
            ytdinc.lob_id,
            pamlob.bu_id bu_id,
            ytdinc.plan_id,
            'FISCAL' period_type,
            ytdinc.rebate_type,
            ytdinc.agreement_year pam_year,
            ytdinc.fiscal_year sys_year,
            sum (ytdinc.accrual_amount) accrued_amt,
            ytdinc.ofu_cust_account_id cust_id,
            'ORACLE' source
       from xxcus.xxcus_ytd_income_b ytdinc
                   ,xxcus.xxcus_rebate_customers c
                   --,apps.ozf_offers d
                   --,apps.qp_list_headers_tl e
                   ,xxcus.xxcus_pam_bu_name_tbl pamlob
      where     1 = 1
            and c.customer_id = ytdinc.ofu_cust_account_id
            and c.customer_attribute1 = 'HDS_MVID'
            and ytdinc.agreement_year >=xxcusozf_pam_pkg.g_from_year
            --and d.qp_list_header_id =ytdinc.plan_id
            --and e.list_header_id =d.qp_list_header_id
            and ytdinc.lob_id =pamlob.lob_id
            and ytdinc.bu =pamlob.bu_name
   group by
            ytdinc.ofu_cust_account_id,
            ytdinc.mvid,
            ytdinc.lob_id,
            ytdinc.bu,
            pamlob.bu_id,
             ytdinc.plan_id,
            ytdinc.rebate_type,
            ytdinc.agreement_year
            ,ytdinc.fiscal_year
            ;
    --
  BEGIN
    --
    l_sec := '@ '||l_err_program||', Begin flush current records from EBS table xxcus.xxcus_pam_ytd_income_tbl, Time :'||to_char(sysdate,'MM/DD/YYYY HH24:MI:SS');
    --
    print_log(l_sec);
    --
    if xxcusozf_pam_pkg.g_customer_id is null then
         delete xxcus.xxcus_pam_ytd_income_tbl;
    else
         delete xxcus.xxcus_pam_ytd_income_tbl where cust_id =xxcusozf_pam_pkg.g_customer_id;
    end if;
    --
    delete xxcus.xxcus_pam_ytd_income_tbl;
     --
    l_sec := '@ '||l_err_program||', End flush current records from EBS table xxcus.xxcus_pam_ytd_income_tbl, deleted records :'||sql%rowcount||', Time :'||to_char(sysdate,'MM/DD/YYYY HH24:MI:SS');
    --
    print_log(l_sec);
    --
    l_sec := '@ '||l_err_program||', Begin Insert of calendar data into ebs table xxcus.xxcus_pam_ytd_income_tbl, Time :'||to_char(sysdate,'MM/DD/YYYY HH24:MI:SS');
    --
    print_log(l_sec);
    --
    for c_rec in c_ytd_income_cal loop
       --
       l_seq :=l_seq +1;
       --
       insert into xxcus.xxcus_pam_ytd_income_tbl
       (
          id
        ,mvid
        ,lob_id
        ,bu_id
        ,plan_id
        ,period_type
        ,rebate_type
        ,pam_year
        ,sys_year
        ,accrued_amt
        ,cust_id
        ,source
        ,ebs_created_by
        ,ebs_creation_date
        ,request_id
       )
       values
       (
          l_seq
        ,c_rec.mvid
        ,c_rec.lob_id
        ,c_rec.bu_id
        ,c_rec.plan_id
        ,c_rec.period_type
        ,c_rec.rebate_type
        ,c_rec.pam_year
        ,c_rec.sys_year
        ,c_rec.accrued_amt
        ,c_rec.cust_id
        ,c_rec.source
        ,p_user_id
        ,sysdate
        ,p_request_id
       );
       --
    end loop;
    --
    l_sec := '@ '||l_err_program||', End Insert of calendar data into ebs table xxcus.xxcus_pam_ytd_income_tbl, total records :'||l_seq||', Time: '||to_char(sysdate,'MM/DD/YYYY HH24:MI:SS');
    --
    print_log(l_sec);
    --
    commit;
    --
    l_sec := '@ '||l_err_program||', Begin Insert of fiscal data into ebs table xxcus.xxcus_pam_ytd_income_tbl, Time :'||to_char(sysdate,'MM/DD/YYYY HH24:MI:SS');
    --
    print_log(l_sec);
    --
    for c_rec in c_ytd_income_fiscal loop
       --
       l_seq :=l_seq +1;
       --
       insert into xxcus.xxcus_pam_ytd_income_tbl
       (
          id
        ,mvid
        ,lob_id
        ,bu_id
        ,plan_id
        ,period_type
        ,rebate_type
        ,pam_year
        ,sys_year
        ,accrued_amt
        ,cust_id
        ,source
        ,ebs_created_by
        ,ebs_creation_date
        ,request_id
       )
       values
       (
          l_seq
        ,c_rec.mvid
        ,c_rec.lob_id
        ,c_rec.bu_id
        ,c_rec.plan_id
        ,c_rec.period_type
        ,c_rec.rebate_type
        ,c_rec.pam_year
        ,c_rec.sys_year
        ,c_rec.accrued_amt
        ,c_rec.cust_id
        ,c_rec.source
        ,p_user_id
        ,sysdate
        ,p_request_id
       );
       --
    end loop;
    --
    l_sec := '@ '||l_err_program||', End Insert of fiscal data into ebs table xxcus.xxcus_pam_ytd_income_tbl, total records :'||l_seq||', Time: '||to_char(sysdate,'MM/DD/YYYY HH24:MI:SS');
    --
    print_log(l_sec);
    --
    commit;
    --
  EXCEPTION
    WHEN OTHERS THEN
      print_log('@ '||l_err_program||', error in extract, msg => '||sqlerrm);
      xxcus_error_pkg.xxcus_error_main_api(p_called_from       => l_err_program
                                          ,p_calling           => l_sec
                                          ,p_request_id =>l_request_id
                                          ,p_ora_error_msg     => SQLERRM
                                          ,p_error_desc        => 'Error running package with PROGRAM ERROR'
                                          ,p_distribution_list => g_err_email
                                          ,p_module            => 'HDS Rebates : PAM');
      --
  END extract_pam_income;
  --
  -- extract pam pam payments
  --
  PROCEDURE extract_pam_payments (p_user_id in number, p_request_id in number) IS
    --
    l_request_id number :=p_request_id;
    l_err_program VARCHAR2(75) := 'XXCUSOZF_PAM_PKG.extract_pam_payments';
    l_sec         VARCHAR2(4000) DEFAULT 'Starting';
    l_seq number :=0;
    --
    cursor c_ytd_income_cal is
    select
           m.mvid mvid,
           m.mv_name mv_name,
           hp.party_id lob_id,
           (
              select /*+ */ bu_id
              from   xxcus.xxcus_pam_bu_name_tbl
              where 1 =1
              and lob_id =hp.party_id
              and bu_name =m.bu_name
           ) bu_id,
           m.plan_id,
           'CALENDAR' period_type,
           m.activity_type rebate_type,
           m.cal_year pam_year,
           m.cal_year sys_year,
           m.invoice_amount invoice_amt,
           m.payment_amount payment_amt,
           'ORACLE' source,
           m.cust_id cust_id
      from apps.hz_parties hp,
           (  select c.customer_id cust_id,
                     c.customer_attribute2 mvid,
                     c.party_name mv_name,
                     qlhv.attribute7 cal_year,
                     qlhv.list_header_id plan_id,
                     decode (flv.tag,
                             'CAN', 'HD SUPPLY CANADA',
                             'FMA', 'FACILITIES MAINTENANCE',
                             flv.attribute4)
                        lob_name,
                        flv.meaning bu_name,
                     decode (med.description,
                             'COOP', 'COOP',
                             'COOP_MIN', 'COOP',
                             'REBATE')
                        activity_type,
                     sum (
                          nvl (aps.amount_due_original, 0)
                        + nvl (aps.amount_adjusted, 0))
                        invoice_amount,
                     sum (nvl (aps.amount_applied, 0)) payment_amount
                from apps.ar_payment_schedules_all aps,
                     apps.ra_customer_trx_all hdr,
                     apps.qp_list_headers_vl qlhv,
                     apps.ozf_claim_lines_all xrf,
                     apps.fnd_lookup_values flv,
                     ozf.ozf_offers oo,
                     apps.ams_media_vl med,
                     apps.ozf_claims_all oca,
                     xxcus.xxcus_rebate_customers c
               where     1 = 1
                     and qlhv.attribute7 >=xxcusozf_pam_pkg.g_from_year
                     and c.customer_attribute1 = 'HDS_MVID'
                     and hdr.org_id in ('101', '102')
                     and c.customer_id = hdr.bill_to_customer_id
                     and aps.customer_trx_id = hdr.customer_trx_id
                     and oca.claim_number = hdr.interface_header_attribute1
                     and oca.claim_id = xrf.claim_id
                     and xrf.activity_id = qlhv.list_header_id
                     and flv.lookup_type = 'XXCUS_REBATE_BU_XREF'
                     and flv.meaning = hdr.attribute2(+)
                     and oo.qp_list_header_id = qlhv.list_header_id
                     and oo.activity_media_id = med.media_id
            group by c.customer_id,
                     c.customer_attribute2,
                     c.party_name,
                     qlhv.attribute7,
                     qlhv.list_header_id, --plan_id
                     decode (flv.tag,
                             'CAN', 'HD SUPPLY CANADA',
                             'FMA', 'FACILITIES MAINTENANCE',
                             flv.attribute4),
                             flv.meaning, --bu_name
                     decode (med.description,
                             'COOP', 'COOP',
                             'COOP_MIN', 'COOP',
                             'REBATE')
           ) m
     where 1 = 1
            and hp.party_name = m.lob_name
            and hp.attribute1 = 'HDS_LOB'
        ;
    --
    cursor c_ytd_income_fiscal is
    select
           m.mvid mvid,
           m.mv_name mv_name,
           hp.party_id lob_id,
           (
              select /*+ RESULT_CACHE */ party_id
              from   apps.hz_parties hp2 --xxcus.xxcus_pam_bu_name_tbl
              where 1 =1
              and hp2.party_name =m.bu_name
              and hp2.attribute1 ='HDS_BU'
           ) bu_id,
           m.plan_id,
           'FISCAL' period_type,
           m.activity_type rebate_type,
           m.fiscal_yr pam_year,
           m.fiscal_yr sys_year,
           --m.cal_year sys_year,
           m.invoice_amount invoice_amt,
           m.payment_amount payment_amt,
           'ORACLE' source,
           m.cust_id cust_id
      from apps.hz_parties hp,
           (  select c.customer_id cust_id,
                     c.customer_attribute2 mvid,
                     c.party_name mv_name,
                     t.ent_year_id fiscal_yr,
                     --qlhv.attribute7 cal_year,
                     qlhv.list_header_id plan_id,
                     decode (flv.tag,
                             'CAN', 'HD SUPPLY CANADA',
                             'FMA', 'FACILITIES MAINTENANCE',
                             flv.attribute4)
                        lob_name,
                        flv.meaning bu_name,
                     decode (med.description,
                             'COOP', 'COOP',
                             'COOP_MIN', 'COOP',
                             'REBATE')
                        activity_type,
                     sum (
                          nvl (aps.amount_due_original, 0)
                        + nvl (aps.amount_adjusted, 0))
                        invoice_amount,
                     sum (nvl (aps.amount_applied, 0)) payment_amount
                from apps.ar_payment_schedules_all aps,
                     apps.ra_customer_trx_all hdr,
                     apps.qp_list_headers_vl qlhv,
                     apps.ozf_claim_lines_all xrf,
                     apps.fnd_lookup_values flv,
                     ozf.ozf_offers oo,
                     apps.ams_media_vl med,
                     apps.ozf_claims_all oca,
                     xxcus.xxcus_rebate_customers c,
                     apps.ozf_time_day t
               where     1 = 1
                     --and qlhv.attribute7 >=:g_from_year --xxcusozf_pam_pkg.g_from_year -- for calendar year extracts only
                     and oca.gl_date between xxcusozf_pam_pkg.g_extract_fiscal_start and xxcusozf_pam_pkg.g_extract_fiscal_end
                     and c.customer_attribute1 = 'HDS_MVID'
                     and hdr.org_id in ('101', '102')
                     and c.customer_id = hdr.bill_to_customer_id
                     and aps.customer_trx_id = hdr.customer_trx_id
                     and oca.claim_number = hdr.interface_header_attribute1
                     and oca.claim_id = xrf.claim_id
                     and xrf.activity_id = qlhv.list_header_id
                     and flv.lookup_type = 'XXCUS_REBATE_BU_XREF'
                     and flv.meaning = hdr.attribute2(+)
                     and oo.qp_list_header_id = qlhv.list_header_id
                     and oo.activity_media_id = med.media_id
                     and t.report_date = oca.gl_date
            group by c.customer_id,
                     c.customer_attribute2,
                     c.party_name,
                     t.ent_year_id, --qlhv.attribute7,
                     qlhv.list_header_id, --plan_id
                     decode (flv.tag,
                             'CAN', 'HD SUPPLY CANADA',
                             'FMA', 'FACILITIES MAINTENANCE',
                             flv.attribute4),
                             flv.meaning, --bu_name
                     decode (med.description,
                             'COOP', 'COOP',
                             'COOP_MIN', 'COOP',
                             'REBATE')
           ) m
     where 1 = 1
            and hp.party_name = m.lob_name
            and hp.attribute1 = 'HDS_LOB'
            ;
    --
  BEGIN
    --
    l_sec := '@ '||l_err_program||', Begin flush current records from EBS table xxcus.xxcus_pam_ytd_payments_tbl, Time :'||to_char(sysdate,'MM/DD/YYYY HH24:MI:SS');
    --
    print_log(l_sec);
    --
    if xxcusozf_pam_pkg.g_customer_id  is null then
        delete xxcus.xxcus_pam_ytd_payments_tbl;
   else
        delete xxcus.xxcus_pam_ytd_payments_tbl where cust_id =xxcusozf_pam_pkg.g_customer_id;
   end if;
     --
    l_sec := '@ '||l_err_program||', End flush current records from EBS table xxcus.xxcus_pam_ytd_payments_tbl, deleted records :'||sql%rowcount||', Time :'||to_char(sysdate,'MM/DD/YYYY HH24:MI:SS');
    --
    print_log(l_sec);
    --
    l_sec := '@ '||l_err_program||', Begin Insert of calendar year extract into xxcus.xxcus_pam_ytd_payments_tbl, Time :'||to_char(sysdate,'MM/DD/YYYY HH24:MI:SS');
    --
    print_log(l_sec);
    --
    for c_rec in c_ytd_income_cal loop
       --
       l_seq :=l_seq +1;
       --
       insert into xxcus.xxcus_pam_ytd_payments_tbl
       (
          id
         ,mvid
         ,mv_name
         ,lob_id
         ,bu_id
         ,plan_id
         ,period_type
         ,rebate_type
         ,pam_year
         ,sys_year
         ,invoice_amt
         ,payment_amt
         ,source
         ,cust_id
         ,ebs_created_by
         ,ebs_creation_date
         ,request_id
       )
       values
       (
           l_seq
         ,c_rec.mvid
         ,c_rec.mv_name
         ,c_rec.lob_id
         ,c_rec.bu_id
         ,c_rec.plan_id
         ,c_rec.period_type
         ,c_rec.rebate_type
         ,c_rec.pam_year
         ,c_rec.sys_year
         ,c_rec.invoice_amt
         ,c_rec.payment_amt
         ,c_rec.source
         ,c_rec.cust_id
         ,p_user_id
         ,sysdate
         ,p_request_id
       );
       --
    end loop;
    --
    l_sec := '@ '||l_err_program||', End Insert of EBS calendar year extract into xxcus.xxcus_pam_ytd_payments_tbl, total records :'||l_seq||', Time: '||to_char(sysdate,'MM/DD/YYYY HH24:MI:SS');
    --
    print_log(l_sec);
    --
    commit;
    --
    --
   /*
    l_sec := '@ '||l_err_program||', Begin Insert of fiscal year extract into xxcus.xxcus_pam_ytd_payments_tbl, Time :'||to_char(sysdate,'MM/DD/YYYY HH24:MI:SS');
    --
    print_log(l_sec);
    --
    for c_rec in c_ytd_income_fiscal loop
       --
       l_seq :=l_seq +1;
       --
       insert into xxcus.xxcus_pam_ytd_payments_tbl
       (
          id
         ,mvid
         ,mv_name
         ,lob_id
         ,bu_id
         ,plan_id
         ,period_type
         ,rebate_type
         ,pam_year
         ,sys_year
         ,invoice_amt
         ,payment_amt
         ,source
         ,cust_id
         ,ebs_created_by
         ,ebs_creation_date
         ,request_id
       )
       values
       (
           l_seq
         ,c_rec.mvid
         ,c_rec.mv_name
         ,c_rec.lob_id
         ,c_rec.bu_id
         ,c_rec.plan_id
         ,c_rec.period_type
         ,c_rec.rebate_type
         ,c_rec.pam_year
         ,c_rec.sys_year
         ,c_rec.invoice_amt
         ,c_rec.payment_amt
         ,c_rec.source
         ,c_rec.cust_id
         ,p_user_id
         ,sysdate
         ,p_request_id
       );
       --
    end loop;
    --
    l_sec := '@ '||l_err_program||', End Insert of EBS fiscal year extract into xxcus.xxcus_pam_ytd_payments_tbl, total records :'||l_seq||', Time: '||to_char(sysdate,'MM/DD/YYYY HH24:MI:SS');
    --
    print_log(l_sec);
    --
    commit;
    */
    --
  EXCEPTION
    WHEN OTHERS THEN
      print_log('@ '||l_err_program||', error in extract, msg => '||sqlerrm);
      xxcus_error_pkg.xxcus_error_main_api(p_called_from       => l_err_program
                                          ,p_calling           => l_sec
                                          ,p_request_id =>l_request_id
                                          ,p_ora_error_msg     => SQLERRM
                                          ,p_error_desc        => 'Error running package with PROGRAM ERROR'
                                          ,p_distribution_list => g_err_email
                                          ,p_module            => 'HDS Rebates : PAM');

  END extract_pam_payments;
  --
  PROCEDURE extract_cal_pam_incomelob (p_user_id in number, p_request_id in number) IS
    --
    l_request_id number :=p_request_id;
    l_err_program VARCHAR2(75) := 'XXCUSOZF_PAM_PKG.extract_cal_pam_incomelob';
    l_sec         VARCHAR2(4000) DEFAULT 'Starting';
    l_seq number :=0;
    --
    cursor c_ytd_income_cal is
     select
            ytdinc.mvid,
            c.party_name mv_name,
            ytdinc.lob_id lob_id,
           (
              select pamlob.bu_id
              from   xxcus.xxcus_pam_bu_name_tbl pamlob
              where 1 =1
              and pamlob.lob_id =ytdinc.lob_id
              and pamlob.bu_name =ytdinc.bu
           ) bu_id,
           ytdinc.plan_id,
--            to_char (ytdinc.calendar_year) pam_year,
--            to_char (ytdinc.agreement_year) sys_year,
            to_char (ytdinc.agreement_year) pam_year,
            to_char (ytdinc.calendar_year) sys_year,
            'CALENDAR' period_type,
            ytdinc.rebate_type,
            ytdinc.calendar_month_id month_id,
            decode (ytdinc.calendar_month_id,
                    1, 'Jan',
                    2, 'Feb',
                    3, 'Mar',
                    4, 'Apr',
                    5, 'May',
                    6, 'Jun',
                    7, 'Jul',
                    8, 'Aug',
                    9, 'Sep',
                    10, 'Oct',
                    11, 'Nov',
                    'Dec')
                    ||'-'||to_char (ytdinc.calendar_year) --ADDED ON 10/27/2017
               month_name,
            sum (ytdinc.accrual_amount) amount,
            to_char (ytdinc.agreement_year) agreement_year,
            'ORACLE' source,
             ytdinc.ofu_cust_account_id cust_id
       from xxcus.xxcus_ytd_income_b ytdinc, xxcus.xxcus_rebate_customers c
      where     1 = 1
            and c.customer_id = ytdinc.ofu_cust_account_id
            and c.customer_attribute1 = 'HDS_MVID'
            and ytdinc.agreement_year >=xxcusozf_pam_pkg.g_from_year -- Just for income purpose to fetch the overlapping accruals we need to use current year -4
   group by
            ytdinc.mvid,
            ytdinc.ofu_cust_account_id,
            ytdinc.lob_id,
            c.party_name,
            ytdinc.rebate_type,
            to_char (ytdinc.calendar_year),
            to_char (ytdinc.agreement_year),
            ytdinc.calendar_month_id,
            ytdinc.bu,
            ytdinc.plan_id
            ;
    --
    cursor c_ytd_income_fiscal is
     select
            ytdinc.mvid,
            c.party_name mv_name,
            ytdinc.lob_id lob_id,
           (
              select /*+ RESULT_CACHE */ bu_id
              from   xxcus.xxcus_pam_bu_name_tbl pamlob -- apps.hz_parties
              where 1 =1
              and pamlob.lob_id =ytdinc.lob_id
              and pamlob.bu_name =ytdinc.bu
              --and party_name =ytdinc.bu
              --and attribute1 ='HDS_BU'
           ) bu_id,
           ytdinc.plan_id,
           ytdinc.agreement_year pam_year,
           ytdinc.fiscal_year sys_year,
            'FISCAL' period_type,
            ytdinc.rebate_type,
            ytdinc.fiscal_month_id month_id,
            d.name month_name,
            sum (ytdinc.accrual_amount) amount,
            ytdinc.agreement_year agreement_year,
            'ORACLE' source,
             ytdinc.ofu_cust_account_id cust_id
       from xxcus.xxcus_ytd_income_b ytdinc
                ,xxcus.xxcus_rebate_customers c
                ,apps.ozf_time_ent_period d
      where     1 = 1
            and c.customer_id = ytdinc.ofu_cust_account_id
            and c.customer_attribute1 = 'HDS_MVID'
            and ytdinc.agreement_year >= xxcusozf_pam_pkg.g_from_year
            and ytdinc.period_id =d.ent_period_id
   group by
            ytdinc.mvid,
            ytdinc.ofu_cust_account_id,
            ytdinc.lob_id,
            c.party_name,
            ytdinc.rebate_type,
            ytdinc.fiscal_year,
            ytdinc.agreement_year,
            d.name, --fiscal month with year
            ytdinc.fiscal_month_id,
            ytdinc.bu,
            ytdinc.plan_id
--     select
--            ytdinc.mvid,
--            c.party_name mv_name,
--            ytdinc.lob_id lob_id,
--           (
--              select /*+ RESULT_CACHE */ bu_id
--              from   xxcus.xxcus_pam_bu_name_tbl pamlob -- apps.hz_parties
--              where 1 =1
--              and pamlob.lob_id =ytdinc.lob_id
--              and pamlob.bu_name =ytdinc.bu
--              --and party_name =ytdinc.bu
--              --and attribute1 ='HDS_BU'
--           ) bu_id,
--           ytdinc.plan_id,
--           ytdinc.agreement_year pam_year,
--           ytdinc.fiscal_year sys_year,
--            'FISCAL' period_type,
--            ytdinc.rebate_type,
--            --ytdinc.fiscal_year||lpad(ytdinc.fiscal_month_id,2,'0') month_id,
--            ytdinc.fiscal_month_id month_id,
--            --substr(d.name, 1, instr(d.name, '-')-1) month_name,
--            d.name month_name,
--            sum (ytdinc.accrual_amount) amount,
--            ytdinc.agreement_year agreement_year,
--            'ORACLE' source,
--             ytdinc.ofu_cust_account_id cust_id
--       from xxcus.xxcus_ytd_income_b ytdinc
--                ,xxcus.xxcus_rebate_customers c
--                ,apps.ozf_time_ent_period d
--      where     1 = 1
--            --and c.customer_id IN (2113,2066,2603,2101,2349,2839)
--            and c.customer_id = ytdinc.ofu_cust_account_id
--            and c.customer_attribute1 = 'HDS_MVID'
--            and ytdinc.agreement_year >=xxcusozf_pam_pkg.g_from_year
--            and ytdinc.period_id =d.ent_period_id
--   group by
--            ytdinc.mvid,
--            ytdinc.ofu_cust_account_id,
--            ytdinc.lob_id,
--            c.party_name,
--            ytdinc.rebate_type,
--            ytdinc.fiscal_year,
--            ytdinc.agreement_year,
--            --substr(d.name, 1, instr(d.name, '-')-1),
--            d.name, --fiscal month with year
--            --ytdinc.fiscal_year||lpad(ytdinc.fiscal_month_id,2,'0'),
--            ytdinc.fiscal_month_id,
--            ytdinc.bu,
--            ytdinc.plan_id
            ;
    --
  --
  BEGIN
    --
    l_sec := '@ '||l_err_program||', Begin flush current records from EBS table  xxcus.xxcus_pam_income_lob_tbl, Time :'||to_char(sysdate,'MM/DD/YYYY HH24:MI:SS');
    --
    print_log(l_sec);
    --
    if xxcusozf_pam_pkg.g_customer_id  is null then
        delete xxcus.xxcus_pam_income_lob_tbl;
   else
        delete xxcus.xxcus_pam_income_lob_tbl where cust_id =xxcusozf_pam_pkg.g_customer_id;
   end if;
     --
    l_sec := '@ '||l_err_program||', End flush current records from EBS table  xxcus.xxcus_pam_income_lob_tbl, deleted records :'||sql%rowcount||', Time :'||to_char(sysdate,'MM/DD/YYYY HH24:MI:SS');
    --
    print_log(l_sec);
    --
    l_sec := '@ '||l_err_program||', Begin insert of calendar extract into xxcus.xxcus_pam_income_lob_tbl, Time :'||to_char(sysdate,'MM/DD/YYYY HH24:MI:SS');
    --
    print_log(l_sec);
    --
    for c_rec in c_ytd_income_cal loop
       --
       l_seq :=l_seq +1;
       --
       insert into xxcus.xxcus_pam_income_lob_tbl
       (
          id,
          mvid,
          mv_name,
          lob_id,
          bu_id,
          plan_id,
          pam_year,
          sys_year,
          period_type,
          rebate_type,
          month_id,
          month_name,
          amount,
          agreement_year,
          source,
          cust_id,
          ebs_created_by,
          ebs_creation_date,
          request_id
       )
       values
       (
           l_seq,
          c_rec.mvid,
          c_rec.mv_name,
          c_rec.lob_id,
          c_rec.bu_id,
          c_rec.plan_id,
          c_rec.pam_year,
          c_rec.sys_year,
          c_rec.period_type,
          c_rec.rebate_type,
          c_rec.month_id,
          c_rec.month_name,
          c_rec.amount,
          c_rec.agreement_year,
          c_rec.source,
          c_rec.cust_id
         ,p_user_id
         ,sysdate
         ,p_request_id
       );
       --
    end loop;
    --
    l_sec := '@ '||l_err_program||', End insert of calendar extract into  xxcus.xxcus_pam_income_lob_tbl, total records :'||l_seq||', Time: '||to_char(sysdate,'MM/DD/YYYY HH24:MI:SS');
    --
    print_log(l_sec);
    --
    commit;
    --
    --
    l_sec := '@ '||l_err_program||', Begin insert of fiscal extract into xxcus.xxcus_pam_income_lob_tbl, Time :'||to_char(sysdate,'MM/DD/YYYY HH24:MI:SS');
    --
    print_log(l_sec);
    --
    for c_rec in c_ytd_income_fiscal loop
       --
       l_seq :=l_seq +1;
       --
       insert into xxcus.xxcus_pam_income_lob_tbl
       (
          id,
          mvid,
          mv_name,
          lob_id,
          bu_id,
          plan_id,
          pam_year,
          sys_year,
          period_type,
          rebate_type,
          month_id,
          month_name,
          amount,
          agreement_year,
          source,
          cust_id,
          ebs_created_by,
          ebs_creation_date,
          request_id
       )
       values
       (
           l_seq,
          c_rec.mvid,
          c_rec.mv_name,
          c_rec.lob_id,
          c_rec.bu_id,
          c_rec.plan_id,
          c_rec.pam_year,
          c_rec.sys_year,
          c_rec.period_type,
          c_rec.rebate_type,
          c_rec.month_id,
          c_rec.month_name,
          c_rec.amount,
          c_rec.agreement_year,
          c_rec.source,
          c_rec.cust_id
         ,p_user_id
         ,sysdate
         ,p_request_id
       );
       --
    end loop;
    --
    l_sec := '@ '||l_err_program||', End insert of fiscal extract into  xxcus.xxcus_pam_income_lob_tbl, total records :'||l_seq||', Time: '||to_char(sysdate,'MM/DD/YYYY HH24:MI:SS');
    --
    print_log(l_sec);
    --
    commit;
    --
  EXCEPTION
    WHEN OTHERS THEN
      print_log('@ '||l_err_program||', error in extract, msg => '||sqlerrm);
      xxcus_error_pkg.xxcus_error_main_api(p_called_from       => l_err_program
                                          ,p_calling           => l_sec
                                          ,p_request_id =>l_request_id
                                          ,p_ora_error_msg     => SQLERRM
                                          ,p_error_desc        => 'Error running package with PROGRAM ERROR'
                                          ,p_distribution_list => g_err_email
                                          ,p_module            => 'HDS Rebates : PAM');

  END extract_cal_pam_incomelob;
  --
  PROCEDURE extract_cal_pam_paydet (p_user_id in number, p_request_id in number) IS
    --
    l_request_id number :=p_request_id;
    l_err_program VARCHAR2(75) := 'XXCUSOZF_PAM_PKG.extract_cal_pam_paydet';
    l_sec         VARCHAR2(4000) DEFAULT 'Starting';
    l_seq number :=0;
    --
    cursor c_payment_det_cal is
   select
          cr.pay_from_customer cust_id,
          hca.customer_attribute2 mvid,                                  --new
          hca.party_name mv_name,
          (
              select distinct lob_id
              from   xxcus.xxcus_pam_bu_name_tbl
              where 1 =1
                    and bu_id =hp1.party_id
          ) lob_id,
          hp1.party_id bu_id,
          (
            select qp_list_header_id
            from   apps.ozf_offers
            where 1 =1
            and offer_code =cr.attribute12
          ) plan_id,
          'CALENDAR' period_type,
          cr.attribute10 rebate_type,
          cr.receipt_date payment_date,
          trunc(cr.creation_date) creation_date,
          cr.attribute2 receipt_timeframe,
          cr.amount payment_amount,
          cr.currency_code currency_code,
--          hp1.party_name business_unit,
          rm.printed_name payment_method,
          cr.customer_receipt_reference receipt_reference,
          cr.receipt_number payment_number,
          cr.cash_receipt_id,
          sch.payment_schedule_id,
          cr.comments image_url,
          sch.amount_due_remaining,
          'ORACLE' source,
          sch.gl_date gl_date,
          cr.attribute11 agreement_year,
          cr.attribute12 agreement_code,
          cr.attribute13 agreement_name,
          cr.attribute11 pam_year,
          extract(year from cr.receipt_date) sys_year
     from apps.ar_cash_receipts_all cr,
          apps.ar_receipt_methods rm,                                    --NEW
          apps.hz_parties hp1,
          apps.ar_payment_schedules_all sch,
          xxcus.xxcus_rebate_customers hca                               --new
    where     1 = 1
          and cr.receipt_date between add_months(trunc(sysdate), xxcusozf_pam_pkg.g_months_to_pull_payments) and trunc(sysdate)
          and cr.org_id in (101, 102)
          and cr.status !='REV'
          and cr.receipt_method_id = rm.receipt_method_id
          and to_number (cr.attribute1) = hp1.party_id
          and hp1.attribute1 = 'HDS_BU'                                  --New
          and hca.customer_id = cr.pay_from_customer                     --new
          and sch.cash_receipt_id = cr.cash_receipt_id
            ;
    --
    cursor c_payment_det_fiscal is
   select
          cr.pay_from_customer cust_id,
          hca.customer_attribute2 mvid,                                  --new
          hca.party_name mv_name,
          (
              select party_id --distinct lob_id
              from   apps.hz_parties --xxcus.xxcus_pam_bu_name_tbl
              where 1 =1
                    --and bu_id =hp1.party_id
                    and party_id =hp1.party_id
                    and attribute1 ='HDS_LOB'
          ) lob_id,
          hp1.party_id bu_id,
          (
            select qp_list_header_id
            from   apps.ozf_offers
            where 1 =1
            and offer_code =cr.attribute12
          ) plan_id,
          'FISCAL' period_type,
          cr.attribute10 rebate_type,
          cr.receipt_date payment_date,
          trunc(cr.creation_date) creation_date,
          cr.attribute2 receipt_timeframe,
          cr.amount payment_amount,
          cr.currency_code currency_code,
--          hp1.party_name business_unit,
          rm.printed_name payment_method,
          cr.customer_receipt_reference receipt_reference,
          cr.receipt_number payment_number,
          cr.cash_receipt_id,
          sch.payment_schedule_id,
          cr.comments image_url,
          sch.amount_due_remaining,
          'ORACLE' source,
          sch.gl_date gl_date,
          cr.attribute11 agreement_year,
          cr.attribute12 agreement_code,
          cr.attribute13 agreement_name,
          ozftp.ent_year_id sys_year
     from apps.ar_cash_receipts_all cr,
          apps.ar_receipt_methods rm,                                    --NEW
          apps.hz_parties hp1,
          apps.ar_payment_schedules_all sch,
          xxcus.xxcus_rebate_customers hca,
          apps.ozf_time_ent_period ozftp                               --new
    where     1 = 1
          and sch.gl_date between xxcusozf_pam_pkg.g_extract_fiscal_start and xxcusozf_pam_pkg.g_extract_fiscal_end
          and cr.org_id in (101, 102)
          and cr.status !='REV'
          and cr.receipt_method_id = rm.receipt_method_id
          and to_number (cr.attribute1) = hp1.party_id
          and hp1.attribute1 = 'HDS_BU'                                  --New
          and hca.customer_id = cr.pay_from_customer                     --new
          and sch.cash_receipt_id = cr.cash_receipt_id
          and sch.gl_date between ozftp.start_date and ozftp.end_date
            ;
    --
  BEGIN
    --
    l_sec := '@ '||l_err_program||', Begin flush current records from EBS table  xxcus.xxcus_pam_payment_det_tbl, Time :'||to_char(sysdate,'MM/DD/YYYY HH24:MI:SS');
    --
    print_log(l_sec);
    --
    if xxcusozf_pam_pkg.g_customer_id  is null then
        delete xxcus.xxcus_pam_payment_det_tbl;
   else
        delete xxcus.xxcus_pam_payment_det_tbl where cust_id =xxcusozf_pam_pkg.g_customer_id;
   end if;
     --
    l_sec := '@ '||l_err_program||', End flush current records from EBS table  xxcus.xxcus_pam_payment_det_tbl, deleted records :'||sql%rowcount||', Time :'||to_char(sysdate,'MM/DD/YYYY HH24:MI:SS');
    --
    print_log(l_sec);
    --
    l_sec := '@ '||l_err_program||', Begin insert of calendar extract into xxcus.xxcus_pam_payment_det_tbl, Time :'||to_char(sysdate,'MM/DD/YYYY HH24:MI:SS');
    --
    print_log(l_sec);
    --
    for c_rec in c_payment_det_cal loop
       --
       l_seq :=l_seq +1;
       --
       insert into xxcus.xxcus_pam_payment_det_tbl
       (
             id
            ,cust_id
            ,mvid
            ,mv_name
            ,lob_id
            ,bu_id
            ,plan_id
            ,period_type
            ,rebate_type
            ,payment_date
            ,creation_date
            ,receipt_timeframe
            ,payment_amount
            ,currency_code
            ,payment_method
            ,receipt_reference
            ,payment_number
            ,cash_receipt_id
            ,payment_schedule_id
            ,image_url
            ,amount_due_remaining
            ,source
            ,gl_date
            ,agreement_year
            ,agreement_code
            ,agreement_name
            ,ebs_created_by
            ,ebs_creation_date
            ,request_id
       )
       values
       (
              l_seq
            ,c_rec.cust_id
            ,c_rec.mvid
            ,c_rec.mv_name
            ,c_rec.lob_id
            ,c_rec.bu_id
            ,c_rec.plan_id
            ,c_rec.period_type
            ,c_rec.rebate_type
            ,c_rec.payment_date
            ,c_rec.creation_date
            ,c_rec.receipt_timeframe
            ,c_rec.payment_amount
            ,c_rec.currency_code
            ,c_rec.payment_method
            ,c_rec.receipt_reference
            ,c_rec.payment_number
            ,c_rec.cash_receipt_id
            ,c_rec.payment_schedule_id
            ,c_rec.image_url
            ,c_rec.amount_due_remaining
            ,c_rec.source
            ,c_rec.gl_date
            ,c_rec.agreement_year
            ,c_rec.agreement_code
            ,c_rec.agreement_name
            ,p_user_id
            ,sysdate
            ,p_request_id
       );
       --
    end loop;
    --
    l_sec := '@ '||l_err_program||', End insert of calendar extract into  xxcus.xxcus_pam_payment_det_tbl, total records :'||l_seq||', Time: '||to_char(sysdate,'MM/DD/YYYY HH24:MI:SS');
    --
    print_log(l_sec);
    --
    commit;
    --
    l_sec := '@ '||l_err_program||', Begin insert of fiscal extract into xxcus.xxcus_pam_payment_det_tbl, Time :'||to_char(sysdate,'MM/DD/YYYY HH24:MI:SS');
    --
    print_log(l_sec);
    --
    for c_rec in c_payment_det_fiscal loop
       --
       l_seq :=l_seq +1;
       --
       insert into xxcus.xxcus_pam_payment_det_tbl
       (
             id
            ,cust_id
            ,mvid
            ,mv_name
            ,lob_id
            ,bu_id
            ,plan_id
            ,period_type
            ,rebate_type
            ,payment_date
            ,creation_date
            ,receipt_timeframe
            ,payment_amount
            ,currency_code
            ,payment_method
            ,receipt_reference
            ,payment_number
            ,cash_receipt_id
            ,payment_schedule_id
            ,image_url
            ,amount_due_remaining
            ,source
            ,gl_date
            ,agreement_year
            ,agreement_code
            ,agreement_name
            ,ebs_created_by
            ,ebs_creation_date
            ,request_id
       )
       values
       (
              l_seq
            ,c_rec.cust_id
            ,c_rec.mvid
            ,c_rec.mv_name
            ,c_rec.lob_id
            ,c_rec.bu_id
            ,c_rec.plan_id
            ,c_rec.period_type
            ,c_rec.rebate_type
            ,c_rec.payment_date
            ,c_rec.creation_date
            ,c_rec.receipt_timeframe
            ,c_rec.payment_amount
            ,c_rec.currency_code
            ,c_rec.payment_method
            ,c_rec.receipt_reference
            ,c_rec.payment_number
            ,c_rec.cash_receipt_id
            ,c_rec.payment_schedule_id
            ,c_rec.image_url
            ,c_rec.amount_due_remaining
            ,c_rec.source
            ,c_rec.gl_date
            ,c_rec.agreement_year
            ,c_rec.agreement_code
            ,c_rec.agreement_name
            ,p_user_id
            ,sysdate
            ,p_request_id
       );
       --
    end loop;
    --
    l_sec := '@ '||l_err_program||', End insert of fiscal extract into  xxcus.xxcus_pam_payment_det_tbl, total records :'||l_seq||', Time: '||to_char(sysdate,'MM/DD/YYYY HH24:MI:SS');
    --
    print_log(l_sec);
    --
    commit;
    --
  EXCEPTION
    WHEN OTHERS THEN
      print_log('@ '||l_err_program||', error in extract, msg => '||sqlerrm);
      xxcus_error_pkg.xxcus_error_main_api(p_called_from       => l_err_program
                                          ,p_calling           => l_sec
                                          ,p_request_id =>l_request_id
                                          ,p_ora_error_msg     => SQLERRM
                                          ,p_error_desc        => 'Error running package with PROGRAM ERROR'
                                          ,p_distribution_list => g_err_email
                                          ,p_module            => 'HDS Rebates : PAM');

  END extract_cal_pam_paydet;
  --
  --
  PROCEDURE extract_cal_pam_lob_purchase (p_user_id in number, p_request_id in number) IS
    --
    -- Begin declaration
    --
    l_request_id number :=p_request_id;
    l_err_program VARCHAR2(75) := 'XXCUSOZF_PAM_PKG.extract_cal_pam_lob_purchase';
    l_sec         VARCHAR2(4000) DEFAULT 'Starting';
    l_seq number :=0;
    l_pam_year number :=0;
    l_month_id number :=0;
    l_month_name varchar2(10) :=Null;
    l_pur_amount number :=0;
    --
    cursor c_pam_pur_lob_det_cal is
     select hca.attribute2 mvid,
            hca.cust_account_id cust_id,
            hp.party_name mv_name,
            pur.lob_id lob_id,
            pur.bu_id bu_id,
            'CALENDAR' period_type,
            pur.calendar_year pam_year,
            to_number(to_char(substr (pur.calendar_period, 6, 2))) month_id,
            decode (to_number(to_char(substr (pur.calendar_period, 6, 2))),
                    '1', 'Jan',
                    '2', 'Feb',
                    '3', 'Mar',
                    '4', 'Apr',
                    '5', 'May',
                    '6', 'Jun',
                    '7', 'Jul',
                    '8', 'Aug',
                    '9', 'Sep',
                    '10', 'Oct',
                    '11', 'Nov',
                    'Dec')
                    ||'-'||pur.calendar_year
               month_name,
            sum (pur.total_purchases) pur_amount,
            'ORACLE' source
       from apps.xxcusozf_purchases_mv pur,
            apps.hz_cust_accounts hca,
            apps.hz_parties hp,
            apps.hz_parties hp1,
            xxcus.xxcus_pam_bu_name_tbl pamlob
      where     1 = 1
            and pur.mvid = hca.cust_account_id
            and hca.party_id = hp.party_id
            and pur.lob_id = hp1.party_id
            and grp_mvid = 0
            and grp_lob = 0
            and grp_bu_id = 0
            and grp_period = 1
            and grp_qtr = 1
            and grp_branch = 1
            and grp_year = 1
            and grp_cal_year = 0
            and grp_cal_period = 0
            and pur.calendar_year >=xxcusozf_pam_pkg.g_from_year
            and pamlob.lob_id =pur.lob_id
            and pamlob.bu_id =pur.bu_id
   group by hca.attribute2,
            hca.cust_account_id,
            hp.party_name,
            pur.lob_id,
            pur.bu_id,
            pur.calendar_year,
            to_number(to_char(substr (pur.calendar_period, 6, 2)))
            ;
    --
    cursor c_pam_pur_lob_det_fiscal is
     select hca.attribute2 mvid,
            hca.cust_account_id cust_id,
            hp.party_name mv_name,
            pur.lob_id lob_id,
            pur.bu_id bu_id,
            'FISCAL' period_type,
            pur.calendar_year pam_year,
            --t.ent_year_id||lpad(t.sequence,2,'0') month_id,
            t.sequence month_id,
            substr(t.name, 1, instr(t.name, '-')-1)||'-'||pur.calendar_year month_name, --Modified to print YYYY as well on 10/27/2017
            sum (pur.total_purchases) pur_amount,
            'ORACLE' source
       from apps.xxcusozf_purchases_mv pur,
            apps.hz_cust_accounts hca,
            apps.hz_parties hp,
            apps.hz_parties hp1,
            apps.ozf_time_ent_period t
      where     1 = 1
            and pur.mvid = hca.cust_account_id
            and hca.party_id = hp.party_id
            and pur.lob_id = hp1.party_id
            and grp_mvid = 0
            and grp_lob = 0
            and grp_bu_id = 0
            and grp_period = 0
            and grp_qtr = 1
            and grp_branch = 1
            and grp_year = 1
            and grp_cal_year = 0
            and grp_cal_period = 1
            and pur.calendar_year >=xxcusozf_pam_pkg.g_from_year
            and t.ent_period_id =pur.period_id
            and t.sequence !=12 
            -- For all fiscal months excluding the last one  no need for additional logic.            
            -- The below condition will ensure we are not adding the purchases from the last fiscal period of the previous fiscal year  .
            -- The previous year last fiscal month is taken care of using a separate logic by invoking cursor c_fiscal_last_mon_of_prev_yr     
   group by hca.attribute2,
            hca.cust_account_id,
            hp.party_name,
            pur.lob_id,
            pur.bu_id,
            pur.calendar_year,
            t.sequence,
            substr(t.name, 1, instr(t.name, '-')-1)||'-'||pur.calendar_year --Modified to print YYYY as well on 10/27/2017
            ;
    --
    cursor c_fiscal_last_mon_of_prev_yr (p_year in number) is
    select b.bu_name
               ,a.cust_id
               ,a.mvid
               ,a.mv_name
               ,a.lob_id
               ,a.bu_id
               ,a.period_type
               ,a.pam_year
               ,a.month_id
               ,a.month_name
    from xxcus.xxcus_pam_purchase_lob_tbl a
             ,xxcus.xxcus_pam_bu_name_tbl b
    where 1 =1
    and a.bu_id =b.bu_id
    and a.period_type ='FISCAL'
    and a.pam_year =p_year
    and month_id =
                                    (
                                        select min(month_id)
                                        from xxcus.xxcus_pam_purchase_lob_tbl x
                                        where 1 =1
                                        and x.cust_id =a.cust_id
                                        and x.bu_id =a.bu_id
                                        and x.period_type =a.period_type
                                        and x.pam_year =a.pam_year
                                    )
    order by a.cust_id asc
                     ,a.bu_id asc
                     ,pam_year asc
    ;
    --
    -- End of declaration
  BEGIN
    --
    l_sec := '@ '||l_err_program||', Begin flush current records from EBS table  xxcus.xxcus_pam_purchase_lob_tbl, Time :'||to_char(sysdate,'MM/DD/YYYY HH24:MI:SS');
    --
    print_log(l_sec);
    --
    if xxcusozf_pam_pkg.g_customer_id  is null then
        delete xxcus.xxcus_pam_purchase_lob_tbl;
   else
        delete xxcus.xxcus_pam_purchase_lob_tbl where cust_id =xxcusozf_pam_pkg.g_customer_id;
   end if;
     --
    l_sec := '@ '||l_err_program||', End flush current records from EBS table  xxcus.xxcus_pam_purchase_lob_tbl, deleted records :'||sql%rowcount||', Time :'||to_char(sysdate,'MM/DD/YYYY HH24:MI:SS');
    --
    print_log(l_sec);
    --
    l_sec := '@ '||l_err_program||', Begin of calendar extract into  xxcus.xxcus_pam_purchase_lob_tbl, Time :'||to_char(sysdate,'MM/DD/YYYY HH24:MI:SS');
    --
    print_log(l_sec);
    --
    for c_rec in c_pam_pur_lob_det_cal loop
       --
       l_seq :=l_seq +1;
       --
       insert into xxcus.xxcus_pam_purchase_lob_tbl
       (
                 id
                ,mvid
                ,cust_id
                ,mv_name
                ,lob_id
                ,bu_id
                ,period_type
                ,pam_year
                ,month_id
                ,month_name
                ,pur_amount
                ,source
                ,ebs_created_by
                ,ebs_creation_date
                ,request_id
       )
       values
       (
              l_seq
            ,c_rec.mvid
            ,c_rec.cust_id
            ,c_rec.mv_name
            ,c_rec.lob_id
            ,c_rec.bu_id
            ,c_rec.period_type
            ,c_rec.pam_year
            ,c_rec.month_id
            ,c_rec.month_name
            ,round(c_rec.pur_amount, 2)
            ,c_rec.source
            ,p_user_id
            ,sysdate
            ,p_request_id
       );
       --
    end loop;
    --
    l_sec := '@ '||l_err_program||', End insert of calendar extract into xxcus.xxcus_pam_purchase_lob_tbl, total records :'||l_seq||', Time: '||to_char(sysdate,'MM/DD/YYYY HH24:MI:SS');
    --
    print_log(l_sec);
    --
    commit;
    --
    l_sec := '@ '||l_err_program||', Begin insert of fiscal extract into  xxcus.xxcus_pam_purchase_lob_tbl, Time :'||to_char(sysdate,'MM/DD/YYYY HH24:MI:SS');
    --
    print_log(l_sec);
    --
    for c_rec in c_pam_pur_lob_det_fiscal loop
       --
       l_seq :=l_seq +1;
       --
       insert into xxcus.xxcus_pam_purchase_lob_tbl
       (
                 id
                ,mvid
                ,cust_id
                ,mv_name
                ,lob_id
                ,bu_id
                ,period_type
                ,pam_year
                ,month_id
                ,month_name
                ,pur_amount
                ,source
                ,ebs_created_by
                ,ebs_creation_date
                ,request_id
                ,display_seq
       )
       values
       (
              l_seq
            ,c_rec.mvid
            ,c_rec.cust_id
            ,c_rec.mv_name
            ,c_rec.lob_id
            ,c_rec.bu_id
            ,c_rec.period_type
            ,c_rec.pam_year
            ,c_rec.month_id
            ,c_rec.month_name
            ,round(c_rec.pur_amount, 2)
            ,c_rec.source
            ,p_user_id
            ,sysdate
            ,p_request_id
            ,2
       );
       --
    end loop;
    --
    -- Begin insert of last fiscal month of the year we are working with for period type FISCAL
    -- Example: If the year is 2015 then we are trying to find if any purchases exists for Jan-2014
    --
    begin
         --
         for idx1 in xxcusozf_pam_pkg.g_from_year .. xxcusozf_pam_pkg.g_to_year loop
             --
             for crec in c_fiscal_last_mon_of_prev_yr (p_year =>idx1) loop
                 --
                 l_month_id :=0;
                 l_month_name :=Null;
                 l_pur_amount :=0;
                 --
                  begin
                     --
                        select
                                    t.sequence month_id,
                                    t.name month_name,
                                    nvl(round(sum (pur.total_purchases), 2), 0)
                        into
                                     l_month_id,
                                     l_month_name,
                                     l_pur_amount
                       from apps.xxcusozf_purchases_mv pur,
                            apps.hz_cust_accounts hca,
                            apps.hz_parties hp,
                            apps.hz_parties hp1,
                            apps.ozf_time_ent_period t
                       where     1 = 1
                            and pur.mvid =crec.cust_id
                            and pur.bu_id =crec.bu_id
                            and pur.mvid = hca.cust_account_id
                            and hca.party_id = hp.party_id
                            and pur.lob_id = hp1.party_id
                            and grp_mvid = 0
                            and grp_lob = 0
                            and grp_bu_id = 0
                            and grp_period = 0
                            and grp_qtr = 1
                            and grp_branch = 1
                            and grp_year = 1
                            and grp_cal_year = 0
                            and grp_cal_period = 1
                            and pur.calendar_year =crec.pam_year
                            and t.ent_period_id =pur.period_id
                            and pur.period_id =(crec.pam_year-1)||412
                        group by
                                t.sequence,
                                t.name
                         ;
                           --
                           l_seq :=l_seq +1;
                           --
                           insert into xxcus.xxcus_pam_purchase_lob_tbl
                           (
                                     id
                                    ,mvid
                                    ,cust_id
                                    ,mv_name
                                    ,lob_id
                                    ,bu_id
                                    ,period_type
                                    ,pam_year
                                    ,month_id
                                    ,month_name
                                    ,pur_amount
                                    ,source
                                    ,ebs_created_by
                                    ,ebs_creation_date
                                    ,request_id
                                    ,display_seq
                           )
                           values
                           (
                                  l_seq
                                ,crec.mvid
                                ,crec.cust_id
                                ,crec.mv_name
                                ,crec.lob_id
                                ,crec.bu_id
                                ,crec.period_type
                                ,crec.pam_year
                                ,l_month_id
                                ,l_month_name
                                ,l_pur_amount
                                ,'ORACLE'
                                ,888888
                                ,sysdate
                                ,p_request_id
                                ,1
                           );
                           --
                     --
                  exception
                   when no_data_found then
                    l_pur_amount :=0;
                   when others then
                    l_pur_amount :=0;
                  end;
                  --
                  -- New 12/13/2017
                 --
                 l_month_id :=0;
                 l_month_name :=Null;
                 l_pur_amount :=0;
                 --
                  begin
                     --
                        select
                                    t.sequence month_id,
                                    t.name month_name,
                                    nvl(round(sum (pur.total_purchases), 2), 0)
                        into
                                     l_month_id,
                                     l_month_name,
                                     l_pur_amount
                       from apps.xxcusozf_purchases_mv pur,
                            apps.hz_cust_accounts hca,
                            apps.hz_parties hp,
                            apps.hz_parties hp1,
                            apps.ozf_time_ent_period t
                       where     1 = 1
                            and pur.mvid =crec.cust_id
                            and pur.bu_id =crec.bu_id
                            and pur.mvid = hca.cust_account_id
                            and hca.party_id = hp.party_id
                            and pur.lob_id = hp1.party_id
                            and grp_mvid = 0
                            and grp_lob = 0
                            and grp_bu_id = 0
                            and grp_period = 0
                            and grp_qtr = 1
                            and grp_branch = 1
                            and grp_year = 1
                            and grp_cal_year = 0
                            and grp_cal_period = 1
                            and pur.calendar_year =crec.pam_year
                            and t.ent_period_id =pur.period_id
                            and pur.period_id =(crec.pam_year)||412
                        group by
                                t.sequence,
                                t.name
                         ;
                           --
                           l_seq :=l_seq +1;
                           --
                           insert into xxcus.xxcus_pam_purchase_lob_tbl
                           (
                                     id
                                    ,mvid
                                    ,cust_id
                                    ,mv_name
                                    ,lob_id
                                    ,bu_id
                                    ,period_type
                                    ,pam_year
                                    ,month_id
                                    ,month_name
                                    ,pur_amount
                                    ,source
                                    ,ebs_created_by
                                    ,ebs_creation_date
                                    ,request_id
                                    ,display_seq
                           )
                           values
                           (
                                  l_seq
                                ,crec.mvid
                                ,crec.cust_id
                                ,crec.mv_name
                                ,crec.lob_id
                                ,crec.bu_id
                                ,crec.period_type
                                ,crec.pam_year
                                ,l_month_id
                                ,l_month_name
                                ,l_pur_amount
                                ,'ORACLE'
                                ,888888
                                ,sysdate
                                ,p_request_id
                                ,1
                           );
                           --
                     --
                  exception
                   when no_data_found then
                    l_pur_amount :=0;
                   when others then
                    l_pur_amount :=0;
                  end;
                  --                           
                  -- New 12/13/2017                  
             end loop;
             --
         end loop;
         --
    exception
     when no_data_found then
      print_log('No data found in fetching of the last fiscal month of the year for any purchasem, message =>'||sqlerrm);
     when others then
      print_log('Other errors in fetching of the last fiscal month of the year for any purchasem, message =>'||sqlerrm);
    end;
    -- End insert of last fiscal month of the year we are working with for period type FISCAL
    --
    l_sec := '@ '||l_err_program||', End insert of fiscal extract into xxcus.xxcus_pam_purchase_lob_tbl, total records :'||l_seq||', Time: '||to_char(sysdate,'MM/DD/YYYY HH24:MI:SS');
    --
    print_log(l_sec);
    --
    commit;
    --
  EXCEPTION
    WHEN OTHERS THEN
      print_log('@ '||l_err_program||', error in extract, msg => '||sqlerrm);
      xxcus_error_pkg.xxcus_error_main_api(p_called_from       => l_err_program
                                          ,p_calling           => l_sec
                                          ,p_request_id =>l_request_id
                                          ,p_ora_error_msg     => SQLERRM
                                          ,p_error_desc        => 'Error running package with PROGRAM ERROR'
                                          ,p_distribution_list => g_err_email
                                          ,p_module            => 'HDS Rebates : PAM');

  END extract_cal_pam_lob_purchase;
  --
  procedure pam_income_table_filler (p_from_year in number, p_to_year in number) is
    --
    l_err_program VARCHAR2(75) := 'XXCUSOZF_PAM_PKG.pam_income_table_filler';
    l_sec         VARCHAR2(4000) DEFAULT 'Starting';
    l_id number :=0;
    l_max_yr_mm number :=0;
    l_new_month_id number :=0;
    l_proceed boolean :=Null;
    l_max number :=12; --Last  month id in a calendar year
    l_new_month_name varchar2(10) :=Null;
    l_month varchar2(3) :=Null;
    l_current_yr_mm number :=0;
    l_next_yr_mm number :=0;
    l_pam_year number :=0;
    --
    cursor all_mv (p_pam_year in number) is
    select a.cust_id, a.mvid, a.mv_name, a.period_type, a.rebate_type,pam_year,count(a.cust_id) total_recs
    from xxcus.xxcus_pam_income_lob_tbl a
    where 1 =1
    and pam_year =p_pam_year
    and sys_year =pam_year
    group by cust_id, mvid,mv_name,period_type,rebate_type,pam_year
    order by cust_id, mvid,mv_name,period_type,rebate_type,pam_year
    ;
    --
    type all_mv_type is table of all_mv%rowtype index by binary_integer;
    all_mv_rec all_mv_type;
    --
    cursor mv_max_period_recs
    ( p_cust_id in number, p_period_type in varchar, p_rebate_type in varchar2, p_max_month_id in number
     ,p_pam_year in number, p_plan_id in number, p_bu_id in number
    ) is
    select *
    from xxcus.xxcus_pam_income_lob_tbl
    where 1 =1
    and cust_id =p_cust_id
    and period_type =p_period_type
    and rebate_type =p_rebate_type
    and pam_year =  p_pam_year
    and plan_id =p_plan_id
    and bu_id =p_bu_id
    and month_id  =p_max_month_id
    and sys_year =pam_year
    ;
    --
    type mv_max_period_recs_type is table of mv_max_period_recs%rowtype index by binary_integer;
    mv_max_period_rec mv_max_period_recs_type;
    --
    cursor all_plan_bu
    (p_cust_id in number, p_period_type in varchar, p_rebate_type in varchar2, p_pam_year in number) is
    select pam_tbl.plan_id  plan_id
               ,pam_tbl.bu_id     bu_id
               ,max(month_id)   max_month_id
    from xxcus.xxcus_pam_income_lob_tbl pam_tbl
    where 1 =1
    and pam_tbl.cust_id =p_cust_id
    and pam_tbl.period_type =p_period_type
    and pam_tbl.rebate_type =p_rebate_type
    and pam_tbl.pam_year =p_pam_year
    --and pam_tbl.plan_id =3732537    -- plan id
    --and pam_tbl.bu_id =376856 -- bu id
    and pam_tbl.sys_year =pam_tbl.pam_year
    group by pam_tbl.plan_id, pam_tbl.bu_id
    order by pam_tbl.plan_id, pam_tbl.bu_id
    ;
    --
    type all_plan_bu_type is table of all_plan_bu%rowtype index by binary_integer;
    all_plan_bu_rec all_plan_bu_type;
    --
  cursor plan_bu_missing_periods
  (  p_cust_id in number, p_period_type in varchar, p_rebate_type in varchar2
    ,p_pam_year in number, p_plan_id in number, p_bu_id in number
  ) is
    select month_id month_id, month_name month_name
    from   xxcus.xxcus_pam_month_tbl mon_tbl
    where 1 =1
    and period_type =p_period_type
    and not exists
     (
        select pam_tbl.month_id
        from xxcus.xxcus_pam_income_lob_tbl pam_tbl
        where 1 =1
        and pam_tbl.cust_id = p_cust_id
        and period_type= p_period_type
        and pam_tbl.rebate_type =  p_rebate_type
        and pam_tbl.pam_year =  p_pam_year
        and pam_tbl.plan_id = p_plan_id
        and pam_tbl.bu_id = p_bu_id
        and mon_tbl.period_type =pam_tbl.period_type
        and mon_tbl.month_id =pam_tbl.month_id
        and pam_tbl.sys_year =pam_tbl.pam_year
        group by pam_tbl.plan_id, pam_tbl.bu_id, pam_tbl.month_id
    )
     order by 1 asc
     ;
    --
    type missing_periods_type is table of plan_bu_missing_periods%rowtype index by binary_integer;
    missing_periods_rec missing_periods_type;
    --
    -- End of declaration and main routine begins
  begin
    --
    print_log('Begin: PAM Income table update with amount zero for the remaining  months so APEX can report it easily.');
    --
    begin
     select max(id)
     into     l_id
     from  xxcus.xxcus_pam_income_lob_tbl
     ;
    exception
     when others then
      l_id :=0;
    end;
    --
    for i in p_from_year ..  p_to_year loop
        -- @@@ ***
        --
        open all_mv (p_pam_year =>i);
        fetch all_mv bulk collect into all_mv_rec;
        close all_mv;
        --
        if all_mv_rec.count >0 then
         --
         for idx in 1 .. all_mv_rec.count loop
            --
            begin
             --
             open all_plan_bu
                                    (
                                      p_cust_id =>all_mv_rec(idx).cust_id
                                     ,p_period_type =>all_mv_rec(idx).period_type
                                     ,p_rebate_type =>all_mv_rec(idx).rebate_type
                                     ,p_pam_year =>all_mv_rec(idx).pam_year
                                    ) ;
             fetch all_plan_bu bulk collect into all_plan_bu_rec; -- this collection holds plan_id, bu_id, max_month_id
             --print_log('all_plan_bu_rec.count ='||all_plan_bu_rec.count);
             close all_plan_bu;
              --
              if all_plan_bu_rec.count=0 then
               l_proceed :=FALSE;
              else
               l_proceed :=TRUE;
              end if;
              --
            exception
             when no_data_found then
                print_log('cursor all_plan_bu returned no records, msg ='||sqlerrm);
                l_proceed :=FALSE;
             when others then
                print_log('cursor all_plan_bu returned error in when others, msg ='||sqlerrm);
                l_proceed :=FALSE;
            end;
            --
            if (l_proceed) then
              --
              for m2 in 1 .. all_plan_bu_rec.count loop
               --
                  begin
                       open plan_bu_missing_periods
                                    (
                                      p_cust_id =>all_mv_rec(idx).cust_id
                                     ,p_period_type =>all_mv_rec(idx).period_type
                                     ,p_rebate_type =>all_mv_rec(idx).rebate_type
                                     ,p_pam_year =>all_mv_rec(idx).pam_year
                                     ,p_plan_id =>all_plan_bu_rec(m2).plan_id
                                     ,p_bu_id =>all_plan_bu_rec(m2).bu_id
                                    ) ;
                       fetch plan_bu_missing_periods bulk collect into missing_periods_rec; -- this colelction holds the missing month id's
                       --print_log('missing_periods_rec.count ='||missing_periods_rec.count);
                       close plan_bu_missing_periods;
                      --
                  exception
                     when no_data_found then
                        print_log('cursor plan_bu_missing_periods returned no records, msg ='||sqlerrm);
                        l_proceed :=FALSE;
                     when others then
                        print_log('cursor plan_bu_missing_periods returned error in when others, msg ='||sqlerrm);
                        l_proceed :=FALSE;
                  end;
                  --
                  if  missing_periods_rec.count>0 then
                      --
                       for m3 in 1 .. missing_periods_rec.count loop
                          -- @@@
                          --print_log('missing_periods_rec(m3).month_id ='||missing_periods_rec(m3).month_id||', all_mv_rec(idx).period_type ='||all_mv_rec(idx).period_type);
                          --
                          if m3 =1 then -- query once to get the base record attributes using the max month
                            --@@@
                              begin
                                  open mv_max_period_recs
                                    (
                                      p_cust_id =>all_mv_rec(idx).cust_id
                                     ,p_period_type =>all_mv_rec(idx).period_type
                                     ,p_rebate_type =>all_mv_rec(idx).rebate_type
                                     ,p_max_month_id =>all_plan_bu_rec(m2).max_month_id
                                     ,p_pam_year =>all_mv_rec(idx).pam_year
                                     ,p_plan_id =>all_plan_bu_rec(m2).plan_id
                                     ,p_bu_id =>all_plan_bu_rec(m2).bu_id
                                    )
                                  ;
                                  fetch mv_max_period_recs bulk collect into mv_max_period_rec;
                                  --print_log('mv_max_period_rec.count ='||mv_max_period_rec.count);
                                  close mv_max_period_recs;
                             exception
                             when no_data_found then
                                print_log('cursor mv_max_period_recs returned no records, msg ='||sqlerrm);
                             when others then
                                print_log('cursor mv_max_period_recs returned error in when others, msg ='||sqlerrm);
                             end;
                            -- @@@
                          else
                            Null;
                          end if;
                          --
                                  --
                              if mv_max_period_rec.count >0 then
                                --
                                for idx1 in 1 .. 1 loop
                                    --
                                     l_id :=l_id + 1;
                                     l_new_month_name :=missing_periods_rec(m3).month_name||'-'||all_mv_rec(idx).pam_year;
                                     --print_log('l_new_month_name =>'||l_new_month_name);
                                    --
                                    /*
                                    print_log('Cust ID ='||all_mv_rec(idx).cust_id||
                                                       ', Period_type ='||all_mv_rec(idx).period_type||
                                                       ', Rebate_type ='||all_mv_rec(idx).rebate_type||
                                                       ', Pam_year ='||all_mv_rec(idx).pam_year||
                                                       ', P_plan_id ='||all_plan_bu_rec(m2).plan_id||
                                                       ', P_bu_id ='||all_plan_bu_rec(m2).plan_id||
                                                       ', Max month id =>'||all_plan_bu_rec(m2).max_month_id||
                                                       ', New month id ='||missing_periods_rec(m3).month_id||
                                                       ', New month name ='||missing_periods_rec(m3).month_name ||
                                                       ', New month yyyy =>'||l_new_month_name
                                                     );
                                    */
                                    --
                                  --
                                   insert into xxcus.xxcus_pam_income_lob_tbl
                                    (
                                         id
                                        ,mvid
                                        ,mv_name
                                        ,lob_id
                                        ,bu_id
                                        ,plan_id
                                        ,pam_year
                                        ,sys_year
                                        ,period_type
                                        ,rebate_type
                                        ,month_id
                                        ,month_name
                                        ,amount
                                        ,agreement_year
                                        ,source
                                        ,cust_id
                                        ,ebs_created_by
                                        ,ebs_creation_date
                                        ,request_id

                                    )
                                   values
                                    (
                                         l_id
                                        ,mv_max_period_rec(idx1).mvid
                                        ,mv_max_period_rec(idx1).mv_name
                                        ,mv_max_period_rec(idx1).lob_id
                                        ,mv_max_period_rec(idx1).bu_id
                                        ,mv_max_period_rec(idx1).plan_id
                                        ,mv_max_period_rec(idx1).pam_year
                                        ,mv_max_period_rec(idx1).sys_year
                                        ,mv_max_period_rec(idx1).period_type
                                        ,mv_max_period_rec(idx1).rebate_type
                                        ,missing_periods_rec(m3).month_id
                                        ,l_new_month_name
                                        ,0 -- Always zero because these are filler records
                                        ,mv_max_period_rec(idx1).agreement_year
                                        ,mv_max_period_rec(idx1).source
                                        ,mv_max_period_rec(idx1).cust_id
                                        ,999999 --mv_max_period_rec(idx1).ebs_created_by
                                        ,sysdate
                                        ,mv_max_period_rec(idx1).request_id
                                    );
                                  --
                                   --print_log('one record inserted using income lob table filler , l_id =>'||l_id);
                                  --
                                end loop;   -- idx1 in 1 .. mv_max_period_rec.count loop
                                --
                              end if;
                              --
                            -- @@@
                            --
                       end loop;
                  end if;
                  --
               --
              end loop;
              --
            end if;
            --
         end loop;
         --
        end if;
        --
     -- @@@ ***
    end loop;
    --
    commit;
    --
    print_log('End: PAM Income table update with amount zero for the remaining months so APEX can report it easily.');
    --
  exception
   when others then
    print_log('When Other Errors in backup_apex_pam_current_data : Message =>'||sqlerrm);
      xxcus_error_pkg.xxcus_error_main_api(p_called_from       => l_err_program
                                          ,p_calling           => l_sec
                                          ,p_ora_error_msg     => SQLERRM
                                          ,p_error_desc        => 'Error running package with PROGRAM ERROR'
                                          ,p_distribution_list => g_err_email
                                          ,p_module            => 'HDS Rebates : PAM');
  end pam_income_table_filler;
  --
  -- *****
 procedure pam_purchase_table_filler (p_from_year in number, p_to_year in number) is
    --
    l_err_program VARCHAR2(75) := 'XXCUSOZF_PAM_PKG.pam_purchase_table_filler';
    l_sec         VARCHAR2(4000) DEFAULT 'Starting';
    l_id number :=0;
    l_max_yr_mm number :=0;
    l_new_month_id number :=0;
    l_proceed boolean :=Null;
    l_max number :=12; --Last  month id in a calendar year
    l_new_month_name varchar2(10) :=Null;
    l_month varchar2(3) :=Null;
    l_current_yr_mm number :=0;
    l_next_yr_mm number :=0;
    l_pam_year number :=0;
    --
    cursor all_mv (l_pam_yr in number) is
    select a.cust_id, a.mvid, a.mv_name, a.period_type, a.pam_year,a.bu_id,count(a.cust_id) total_recs
    from xxcus.xxcus_pam_purchase_lob_tbl a
    where 1 =1
    and a.pam_year = l_pam_yr                               
    group by a.cust_id, a.mvid, a.mv_name, a.period_type,a.bu_id, a.pam_year
    order by a.cust_id, a.mvid, a.mv_name, a.period_type,a.bu_id, a.pam_year
    ;
    --
    type all_mv_type is table of all_mv%rowtype index by binary_integer;
    all_mv_rec all_mv_type;
    --
    cursor mv_max_period_recs
    ( p_cust_id in number, p_period_type in varchar, p_max_month_id in number
     ,p_pam_year in number, p_bu_id in number
    ) is
    select *
    from xxcus.xxcus_pam_purchase_lob_tbl
    where 1 =1
    and cust_id =p_cust_id
    and period_type =p_period_type
    and pam_year =  p_pam_year
    and bu_id =p_bu_id
    and month_id  =p_max_month_id
    ;
    --
    type mv_max_period_recs_type is table of mv_max_period_recs%rowtype index by binary_integer;
    mv_max_period_rec mv_max_period_recs_type;
    --
    cursor all_plan_bu
    (p_cust_id in number, p_period_type in varchar, p_pam_year in number) is
    select pam_tbl.bu_id     bu_id
               ,max(month_id)   max_month_id
    from xxcus.xxcus_pam_purchase_lob_tbl pam_tbl
    where 1 =1
    and pam_tbl.cust_id =p_cust_id
    and pam_tbl.period_type =p_period_type
    and pam_tbl.pam_year =p_pam_year
    group by pam_tbl.bu_id
    order by pam_tbl.bu_id
    ;
    --
    type all_plan_bu_type is table of all_plan_bu%rowtype index by binary_integer;
    all_plan_bu_rec all_plan_bu_type;
    --
  cursor plan_bu_missing_periods
  (  p_cust_id in number, p_period_type in varchar,
     p_pam_year in number, p_bu_id in number
  ) is
    select month_id month_id, month_name month_name
    from   xxcus.xxcus_pam_month_tbl mon_tbl
    where 1 =1
    and period_type =p_period_type
    and not exists
     (
        select pam_tbl.month_id
        from xxcus.xxcus_pam_purchase_lob_tbl pam_tbl
        where 1 =1
        and pam_tbl.cust_id = p_cust_id
        and period_type= p_period_type
        and pam_tbl.pam_year =  p_pam_year
        and pam_tbl.bu_id = p_bu_id
        and mon_tbl.period_type =pam_tbl.period_type
        and mon_tbl.month_id =pam_tbl.month_id
        group by pam_tbl.bu_id, pam_tbl.month_id
    )
     order by 1 asc
     ;
    --
    type missing_periods_type is table of plan_bu_missing_periods%rowtype index by binary_integer;
    missing_periods_rec missing_periods_type;
    --
    -- End of declaration and main routine begins
  begin
    --
    print_log('Begin: PAM purchase lob table update with amount zero for the remaining  months so APEX can report it easily.');
    --
    begin
     select max(id)
     into     l_id
     from  xxcus.xxcus_pam_purchase_lob_tbl
     ;
    exception
     when others then
      l_id :=0;
    end;
    --
    for i in p_from_year ..  p_to_year loop
        --
        -- @@@ ***
        --
            open all_mv (l_pam_yr =>i);
            fetch all_mv bulk collect into all_mv_rec;
            close all_mv;
            --
            if all_mv_rec.count >0 then
             --
             for idx in 1 .. all_mv_rec.count loop
                --
                begin
                 --
                 open all_plan_bu
                                        (
                                          p_cust_id =>all_mv_rec(idx).cust_id
                                         ,p_period_type =>all_mv_rec(idx).period_type
                                         ,p_pam_year =>all_mv_rec(idx).pam_year
                                        ) ;
                 fetch all_plan_bu bulk collect into all_plan_bu_rec; -- this collection holds bu_id, max_month_id
                 --print_log('all_plan_bu_rec.count ='||all_plan_bu_rec.count);
                 close all_plan_bu;
                  --
                  if all_plan_bu_rec.count=0 then
                   l_proceed :=FALSE;
                  else
                   l_proceed :=TRUE;
                  end if;
                  --
                exception
                 when no_data_found then
                    print_log('cursor all_plan_bu returned no records, msg ='||sqlerrm);
                    l_proceed :=FALSE;
                 when others then
                    print_log('cursor all_plan_bu returned error in when others, msg ='||sqlerrm);
                    l_proceed :=FALSE;
                end;
                --
                if (l_proceed) then
                  --
                  for m2 in 1 .. all_plan_bu_rec.count loop
                   --
                      begin
                           open plan_bu_missing_periods
                                        (
                                          p_cust_id =>all_mv_rec(idx).cust_id
                                         ,p_period_type =>all_mv_rec(idx).period_type
                                         ,p_pam_year =>all_mv_rec(idx).pam_year
                                         ,p_bu_id =>all_plan_bu_rec(m2).bu_id
                                        ) ;
                           fetch plan_bu_missing_periods bulk collect into missing_periods_rec; -- this collection holds the missing month id's
                           --print_log('missing_periods_rec.count ='||missing_periods_rec.count);
                           close plan_bu_missing_periods;
                          --
                      exception
                         when no_data_found then
                            print_log('cursor plan_bu_missing_periods returned no records, msg ='||sqlerrm);
                            l_proceed :=FALSE;
                         when others then
                            print_log('cursor plan_bu_missing_periods returned error in when others, msg ='||sqlerrm);
                            l_proceed :=FALSE;
                      end;
                      --
                      if  missing_periods_rec.count>0 then
                          --
                           for m3 in 1 .. missing_periods_rec.count loop
                              -- @@@
                              --print_log('missing_periods_rec(m3).month_id ='||missing_periods_rec(m3).month_id||', all_mv_rec(idx).period_type ='||all_mv_rec(idx).period_type);
                              --
                              if m3 =1 then -- query once to get the base record attributes using the max month
                                --@@@
                                  begin
                                      open mv_max_period_recs
                                        (
                                          p_cust_id =>all_mv_rec(idx).cust_id
                                         ,p_period_type =>all_mv_rec(idx).period_type
                                         ,p_max_month_id =>all_plan_bu_rec(m2).max_month_id
                                         ,p_pam_year =>all_mv_rec(idx).pam_year
                                         ,p_bu_id =>all_plan_bu_rec(m2).bu_id
                                        )
                                      ;
                                      fetch mv_max_period_recs bulk collect into mv_max_period_rec;
                                      print_log('mv_max_period_rec.count ='||mv_max_period_rec.count);
                                      close mv_max_period_recs;
                                 exception
                                 when no_data_found then
                                    print_log('cursor mv_max_period_recs returned no records, msg ='||sqlerrm);
                                 when others then
                                    print_log('cursor mv_max_period_recs returned error in when others, msg ='||sqlerrm);
                                 end;
                                -- @@@
                              else
                                Null;
                              end if;
                              --
                                      --
                                  if mv_max_period_rec.count >0 then
                                    --
                                    for idx1 in 1 .. 1 loop
                                        --
                                         l_id :=l_id + 1;
                                         l_new_month_name :=missing_periods_rec(m3).month_name||'-'||all_mv_rec(idx).pam_year;
                                         --print_log('l_new_month_name =>'||l_new_month_name);
                                        --
                                      --
                                       insert into xxcus.xxcus_pam_purchase_lob_tbl
                                        (
                                             id
                                            ,mvid
                                            ,mv_name
                                            ,lob_id
                                            ,bu_id
                                            ,pam_year
                                            ,period_type
                                            ,month_id
                                            ,month_name
                                            ,pur_amount
                                            ,source
                                            ,cust_id
                                            ,ebs_created_by
                                            ,ebs_creation_date
                                            ,request_id

                                        )
                                       values
                                        (
                                             l_id
                                            ,mv_max_period_rec(idx1).mvid
                                            ,mv_max_period_rec(idx1).mv_name
                                            ,mv_max_period_rec(idx1).lob_id
                                            ,mv_max_period_rec(idx1).bu_id
                                            ,mv_max_period_rec(idx1).pam_year
                                            ,mv_max_period_rec(idx1).period_type
                                            ,missing_periods_rec(m3).month_id
                                            ,l_new_month_name
                                            ,0 -- Always zero because these are filler records
                                            ,mv_max_period_rec(idx1).source
                                            ,mv_max_period_rec(idx1).cust_id
                                            ,999999 --mv_max_period_rec(idx1).ebs_created_by
                                            ,sysdate
                                            ,mv_max_period_rec(idx1).request_id
                                        );
                                      --
                                       --print_log('one record inserted using income lob table filler , l_id =>'||l_id);
                                      --
                                    end loop;   -- idx1 in 1 .. mv_max_period_rec.count loop
                                    --
                                  end if;
                                  --
                                -- @@@
                                --
                           end loop;
                      end if;
                      --
                   --
                  end loop;
                  --
                end if;
                --
             end loop;
             --
            end if;
            --
        --
        -- @@@ ***
        --        
    end loop;    
    --
    commit;
    --
    print_log('End: PAM purchase lob table update with amount zero for the remaining  months so APEX can report it easily.');
    --
  exception
   when others then
    print_log('When Other Errors in backup_apex_pam_current_data : Message =>'||sqlerrm);
      xxcus_error_pkg.xxcus_error_main_api(p_called_from       => l_err_program
                                          ,p_calling           => l_sec
                                          ,p_ora_error_msg     => SQLERRM
                                          ,p_error_desc        => 'Error running package with PROGRAM ERROR'
                                          ,p_distribution_list => g_err_email
                                          ,p_module            => 'HDS Rebates : PAM');
  end pam_purchase_table_filler;
  --
  -- *****
  procedure backup_apex_pam_current_data is
    --
    l_err_program VARCHAR2(75) := 'XXCUSOZF_PAM_PKG.transfer_data_to_pam_apex';
    l_sec         VARCHAR2(4000) DEFAULT 'Starting';
    --
  begin
    --
    print_log('Begin: Cleanup existing EBS pam tmp tables which are mirror images of pam apex tables so we can backup current pam apex data');
    --
        delete from xxcus.pam_income_lob_tbl_tmp;
        delete from xxcus.pam_payment_det_tbl_tmp;
        delete from xxcus.pam_program_list_tbl_tmp;
        delete from xxcus.pam_purchase_lob_tbl_tmp;
        delete from xxcus.pam_vend_lov_tbl_tmp;
        delete from xxcus.pam_ytd_income_tbl_tmp;
        delete from xxcus.pam_ytd_payments_tbl_tmp;
        delete from xxcus.pam_ytd_purch_tbl_tmp;
    --
    print_log('End: Cleanup existing EBS pam tmp tables which are mirror images of pam apex tables so we can backup current pam apex data');
    --
    print_log('Begin: Insert into existing EBS pam tmp tables which are mirror images of pam apex tables so we can backup current pam apex data');
    --
        insert into xxcus.pam_income_lob_tbl_tmp (select * from ea_apps.pam_income_lob_tbl@eaapxprd);
        insert into xxcus.pam_payment_det_tbl_tmp (select * from ea_apps.pam_payment_det_tbl@eaapxprd);
        insert into xxcus.pam_program_list_tbl_tmp (select * from ea_apps.pam_program_list_tbl@eaapxprd);
        insert into xxcus.pam_purchase_lob_tbl_tmp (select * from ea_apps.pam_purchase_lob_tbl@eaapxprd);
        insert into xxcus.pam_vend_lov_tbl_tmp (select * from ea_apps.pam_vend_lov_tbl@eaapxprd);
        insert into xxcus.pam_ytd_income_tbl_tmp (select * from ea_apps.pam_ytd_income_tbl@eaapxprd);
        insert into xxcus.pam_ytd_payments_tbl_tmp (select * from ea_apps.pam_ytd_payments_tbl@eaapxprd);
        insert into xxcus.pam_ytd_purch_tbl_tmp (select * from ea_apps.pam_ytd_purch_tbl@eaapxprd);
    --
    print_log(' All good with the backup of current PAM APEX data.');
    --
    print_log('End: Insert into existing EBS pam tmp tables which are mirror images of pam apex tables so we can backup current pam apex data');
    --
  exception
   when others then
    print_log('When Other Errors in backup_apex_pam_current_data : Message =>'||sqlerrm);
      xxcus_error_pkg.xxcus_error_main_api(p_called_from       => l_err_program
                                          ,p_calling           => l_sec
                                          ,p_ora_error_msg     => SQLERRM
                                          ,p_error_desc        => 'Error running package with PROGRAM ERROR'
                                          ,p_distribution_list => g_err_email
                                          ,p_module            => 'HDS Rebates : PAM');
  end backup_apex_pam_current_data;
  --
  procedure transfer_data_to_pam_apex is
    --
    l_err_program VARCHAR2(75) := 'XXCUSOZF_PAM_PKG.transfer_data_to_pam_apex';
    l_sec         VARCHAR2(4000) DEFAULT 'Starting';
    b_all_done BOOLEAN :=FALSE;
    --
  begin
    --
    --  Process pam_bu_name_tbl@eaapxprd;
    begin
     --
     delete ea_apps.pam_bu_name_tbl@eaapxprd;
     --
     print_log('Deleted existing PAM APEX table pam_bu_name_tbl records :'||sql%rowcount);
     --
     commit;
     --
     print_log('Begin: Insert new EBS records into PAM APEX table pam_bu_name_tbl, Start time: '||to_char(sysdate, 'mm/dd/yyyy hh24:mi:ss'));
     --
     insert into ea_apps.pam_bu_name_tbl@eaapxprd
     (
                 id
                ,country
                ,lob_name
                ,lob_id
                ,bu_name
                ,bu_id
                ,ebs_creation_date
                ,ebs_created_by
                ,request_id
     )
     (
       select
                 id
                ,country
                ,lob_name
                ,lob_id
                ,bu_name
                ,bu_id
                ,ebs_creation_date
                ,ebs_created_by
                ,request_id
       from xxcus.xxcus_pam_bu_name_tbl
     );
     --
     print_log('End: Insert latest records from EBS into PAM APEX table pam_bu_name_tbl, total: '||to_char(sysdate, 'mm/dd/yyyy hh24:mi:ss')||', total records :'||sql%rowcount);
     --
     commit;
     --
    exception
     when others then
      xxcus_error_pkg.xxcus_error_main_api(p_called_from       => l_err_program
                                          ,p_calling           => l_sec
                                          ,p_ora_error_msg     => SQLERRM
                                          ,p_error_desc        => 'Error running Rebates PAM : transfer_data_to_pam_apex @ pam_bu_name_tbl with PROGRAM ERROR'
                                          ,p_distribution_list => g_err_email
                                          ,p_module            => 'PAM');
    end;
    --
    --  Process pam_vend_lov_tbl@eaapxprd;
    begin
     --
     delete ea_apps.pam_vend_lov_tbl@eaapxprd;
     --
     print_log('Deleted existing PAM APEX table pam_vend_lov_tbl records :'||sql%rowcount);
     --
     commit;
     --
     print_log('Begin: Insert new EBS records into PAM APEX table pam_vend_lov_tbl, Start time: '||to_char(sysdate, 'mm/dd/yyyy hh24:mi:ss'));
     --
     insert into ea_apps.pam_vend_lov_tbl@eaapxprd
     (
              id,
              mvid,
              mv_name,
              cust_id,
              source,
              ebs_created_by,
              ebs_creation_date,
              request_id
     )
     (
       select
              id,
              mvid,
              mv_name,
              cust_id,
              source,
              ebs_created_by,
              ebs_creation_date,
              request_id
       from xxcus.xxcus_pam_vend_lov_tbl
     );
     --
     print_log('End: Insert latest records from EBS into PAM APEX table pam_vend_lov_tbl, total: '||to_char(sysdate, 'mm/dd/yyyy hh24:mi:ss')||', total records :'||sql%rowcount);
     --
     commit;
     --
    exception
     when others then
      xxcus_error_pkg.xxcus_error_main_api(p_called_from       => l_err_program
                                          ,p_calling           => l_sec
                                          ,p_ora_error_msg     => SQLERRM
                                          ,p_error_desc        => 'Error running Rebates PAM : transfer_data_to_pam_apex @ pam_vend_lov_tbl with PROGRAM ERROR'
                                          ,p_distribution_list => g_err_email
                                          ,p_module            => 'PAM');
    end;
    --
    -- Process pam_program_list_tbl@eaapxprd;
    begin
     --
     delete ea_apps.pam_program_list_tbl@eaapxprd;
     --
     print_log('Deleted existing PAM APEX table pam_program_list_tbl records :'||sql%rowcount);
     --
     commit;
     --
     print_log('Begin: Insert new EBS records into PAM APEX table pam_program_list_tbl records, Start time: '||to_char(sysdate, 'mm/dd/yyyy hh24:mi:ss'));
     --
     insert into ea_apps.pam_program_list_tbl@eaapxprd
     (
              id,
              mvid,
              mv_name,
              agreement_yr,
              agreement_status,
              program_name,
              program_description,
              rebate_type,
              agreement_name,
              agreement_description,
              payment_method,
              payment_freq,
              auto_renewal,
              until_year,
              activity_type,
              autopay_enabled_flag,
              global_flag,
              back_to_or_min_guarantee_amt,
              cust_id,
              source,
              contract_nbr,
              plan_id,
              ebs_created_by,
              ebs_creation_date,
              request_id
     )
     (
       select
              id,
              mvid,
              mv_name,
              agreement_yr,
              agreement_status,
              program_name,
              program_description,
              rebate_type,
              agreement_name,
              agreement_description,
              payment_method,
              payment_freq,
              auto_renewal,
              until_year,
              activity_type,
              autopay_enabled_flag,
              global_flag,
              back_to_or_min_guarantee_amt,
              cust_id,
              source,
              contract_nbr,
              plan_id,
              ebs_created_by,
              ebs_creation_date,
              request_id
       from xxcus.xxcus_pam_program_list_tbl
     );
     --
     print_log('End: Insert new EBS records into PAM APEX table pam_program_list_tbl records, Start time: '||to_char(sysdate, 'mm/dd/yyyy hh24:mi:ss')||', total records :'||sql%rowcount);
     --
     commit;
     --
    exception
     when others then
      xxcus_error_pkg.xxcus_error_main_api(p_called_from       => l_err_program
                                          ,p_calling           => l_sec
                                          ,p_ora_error_msg     => SQLERRM
                                          ,p_error_desc        => 'Error running Rebates PAM : transfer_data_to_pam_apex @ pam_program_list_tbl with PROGRAM ERROR'
                                          ,p_distribution_list => g_err_email
                                          ,p_module            => 'PAM');
    end;
    --
    -- Process pam_ytd_purch_tbl@eaapxprd;
    begin
     --
     delete ea_apps.pam_ytd_purch_tbl@eaapxprd;
     --
     print_log('Deleted existing PAM APEX table pam_ytd_purch_tbl records :'||sql%rowcount);
     --
     commit;
     --
     print_log('Begin: Insert new EBS records into PAM APEX table pam_ytd_purch_tbl records, Start time: '||to_char(sysdate, 'mm/dd/yyyy hh24:mi:ss'));
     --
     insert into ea_apps.pam_ytd_purch_tbl@eaapxprd
     (
              id,
              mvid,
              lob_id,
              bu_id,
              year,
              total_purchases,
              cust_id,
              source,
              ebs_created_by,
              ebs_creation_date,
              request_id
     )
     (
       select
              id,
              mvid,
              lob_id,
              bu_id,
              pam_year,
              total_purchases,
              cust_id,
              source,
              ebs_created_by,
              ebs_creation_date,
              request_id
       from xxcus.xxcus_pam_ytd_purch_tbl
     );
     --
     print_log('End: Insert new EBS records into PAM APEX table pam_ytd_purch_tbl records, Start time: '||to_char(sysdate, 'mm/dd/yyyy hh24:mi:ss')||', total records :'||sql%rowcount);
     --
     commit;
     --
    exception
     when others then
      xxcus_error_pkg.xxcus_error_main_api(p_called_from       => l_err_program
                                          ,p_calling           => l_sec
                                          ,p_ora_error_msg     => SQLERRM
                                          ,p_error_desc        => 'Error running Rebates PAM : transfer_data_to_pam_apex @ pam_ytd_purch_tbl with PROGRAM ERROR'
                                          ,p_distribution_list => g_err_email
                                          ,p_module            => 'PAM');
    end;
    --
    -- Process pam_ytd_income_tbl@eaapxprd;
    begin
     --
     delete ea_apps.pam_ytd_income_tbl@eaapxprd;
     --
     commit;
     --
     print_log('Deleted existing PAM APEX table pam_ytd_income_tbl records');
     --
     print_log('Begin: Insert new EBS records into PAM APEX table pam_ytd_income_tbl records, Start time: '||to_char(sysdate, 'mm/dd/yyyy hh24:mi:ss'));
     --
     insert into ea_apps.pam_ytd_income_tbl@eaapxprd
     (
              id,
              mvid,
              lob_id,
              bu_id,
              plan_id,
              period_type,
              rebate_type,
              year,
              accrued_amt,
              cust_id,
              source,
              ebs_created_by,
              ebs_creation_date,
              request_id
     )
     (
       select
              id,
              mvid,
              lob_id,
              bu_id,
              plan_id,
              period_type,
              rebate_type,
              pam_year,
              accrued_amt,
              cust_id,
              source,
              ebs_created_by,
              ebs_creation_date,
              request_id
       from  xxcus.xxcus_pam_ytd_income_tbl
     );
     --
     print_log('End: Insert new EBS records into PAM APEX table pam_ytd_income_tbl records, Start time: '||to_char(sysdate, 'mm/dd/yyyy hh24:mi:ss')||', total records :'||sql%rowcount);
     --
     commit;
     --
    exception
     when others then
      xxcus_error_pkg.xxcus_error_main_api(p_called_from       => l_err_program
                                          ,p_calling           => l_sec
                                          ,p_ora_error_msg     => SQLERRM
                                          ,p_error_desc        => 'Error running Rebates PAM : transfer_data_to_pam_apex @ pam_ytd_income_tbl with PROGRAM ERROR'
                                          ,p_distribution_list => g_err_email
                                          ,p_module            => 'PAM');
    end;
    --
    -- Process pam_ytd_payments_tbl@eaapxprd;
    begin
     --
     delete ea_apps.pam_ytd_payments_tbl@eaapxprd;
     --
     print_log('Deleted existing PAM APEX table pam_ytd_payments_tbl records :'||sql%rowcount);
     --
     commit;
     --
     print_log('Begin: Insert new EBS records into PAM APEX table pam_ytd_payments_tbl records, Start time: '||to_char(sysdate, 'mm/dd/yyyy hh24:mi:ss'));
     --
     insert into ea_apps.pam_ytd_payments_tbl@eaapxprd
     (
              id,
              mvid,
              mv_name,
              lob_id,
              bu_id,
              plan_id,
              period_type,
              rebate_type,
              year,
              invoice_amt,
              payment_amt,
              source,
              cust_id,
              ebs_created_by,
              ebs_creation_date,
              request_id
     )
     (
       select
              id,
              mvid,
              mv_name,
              lob_id,
              bu_id,
              plan_id,
              period_type,
              rebate_type,
              pam_year,
              invoice_amt,
              payment_amt,
              source,
              cust_id,
              ebs_created_by,
              ebs_creation_date,
              request_id
       from  xxcus.xxcus_pam_ytd_payments_tbl
     );
     --
     print_log('End: Insert new EBS records into PAM APEX table pam_ytd_payments_tbl records, Start time: '||to_char(sysdate, 'mm/dd/yyyy hh24:mi:ss')||', total records :'||sql%rowcount);
     --
     commit;
     --
    exception
     when others then
      xxcus_error_pkg.xxcus_error_main_api(p_called_from       => l_err_program
                                          ,p_calling           => l_sec
                                          ,p_ora_error_msg     => SQLERRM
                                          ,p_error_desc        => 'Error running Rebates PAM : transfer_data_to_pam_apex @ pam_ytd_payments_tbl with PROGRAM ERROR'
                                          ,p_distribution_list => g_err_email
                                          ,p_module            => 'PAM');
    end;
    --
    -- Process pam_income_lob_tbl@eaapxprd;
    begin
     --
     delete ea_apps.pam_income_lob_tbl@eaapxprd;
     --
     print_log('Deleted existing PAM APEX table pam_income_lob_tbl records :'||sql%rowcount);
     --
     commit;
     --
     print_log('Begin: Insert new EBS records into PAM APEX table pam_income_lob_tbl records, Start time: '||to_char(sysdate, 'mm/dd/yyyy hh24:mi:ss'));
     --
     insert into ea_apps.pam_income_lob_tbl@eaapxprd
     (
              id,
              mvid,
              mv_name,
              lob_id,
              bu_id,
              plan_id,
              year,
              period_type,
              rebate_type,
              month_id,
              month_name,
              amount,
              agreement_year,
              source,
              cust_id,
              ebs_created_by,
              ebs_creation_date,
              request_id,
              sys_year
     )
     (
       select
              id,
              mvid,
              mv_name,
              lob_id,
              bu_id,
              plan_id,
              pam_year,
              period_type,
              rebate_type,
              month_id,
              month_name,
              amount,
              agreement_year,
              source,
              cust_id,
              ebs_created_by,
              ebs_creation_date,
              request_id,
              sys_year
       from  xxcus.xxcus_pam_income_lob_tbl
     );
     --
     print_log('End: Insert new EBS records into PAM APEX table pam_income_lob_tbl records, Start time: '||to_char(sysdate, 'mm/dd/yyyy hh24:mi:ss')||', total records :'||sql%rowcount);
     --
     commit;
     --
    exception
     when others then
      xxcus_error_pkg.xxcus_error_main_api(p_called_from       => l_err_program
                                          ,p_calling           => l_sec
                                          ,p_ora_error_msg     => SQLERRM
                                          ,p_error_desc        => 'Error running Rebates PAM : transfer_data_to_pam_apex @ pam_income_lob_tbl with PROGRAM ERROR'
                                          ,p_distribution_list => g_err_email
                                          ,p_module            => 'PAM');
    end;
    --
    -- Process pam_payment_det_tbl@eaapxprd;
    begin
     --
     delete ea_apps.pam_payment_det_tbl@eaapxprd;
     --
     print_log('Deleted existing PAM APEX table pam_payment_det_tbl records :'||sql%rowcount);
     --
     commit;
     --
     print_log('Begin: Insert new EBS records into PAM APEX table pam_payment_det_tbl records, Start time: '||to_char(sysdate, 'mm/dd/yyyy hh24:mi:ss'));
     --
     insert into ea_apps.pam_payment_det_tbl@eaapxprd
     (
              id,
              cust_id,
              mvid,
              mv_name,
              lob_id,
              bu_id,
              plan_id,
              period_type,
              rebate_type,
              payment_date,
              creation_date,
              receipt_timeframe,
              payment_amount,
              currency_code,
              payment_method,
              receipt_reference,
              payment_number,
              cash_receipt_id,
              payment_schedule_id,
              image_url,
              amount_due_remaining,
              source,
              gl_date,
              agreement_year,
              agreement_code,
              agreement_name,
              ebs_created_by,
              ebs_creation_date,
              request_id
     )
     (
       select
              id,
              cust_id,
              mvid,
              mv_name,
              lob_id,
              bu_id,
              plan_id,
              period_type,
              rebate_type,
              payment_date,
              creation_date,
              receipt_timeframe,
              payment_amount,
              currency_code,
              payment_method,
              receipt_reference,
              payment_number,
              cash_receipt_id,
              payment_schedule_id,
              image_url,
              amount_due_remaining,
              source,
              gl_date,
              agreement_year,
              agreement_code,
              agreement_name,
              ebs_created_by,
              ebs_creation_date,
              request_id
       from  xxcus.xxcus_pam_payment_det_tbl
     );
     --
     print_log('End: Insert new EBS records into PAM APEX table pam_payment_det_tbl records, Start time: '||to_char(sysdate, 'mm/dd/yyyy hh24:mi:ss')||', total records :'||sql%rowcount);
     --
     commit;
     --
    exception
     when others then
      xxcus_error_pkg.xxcus_error_main_api(p_called_from       => l_err_program
                                          ,p_calling           => l_sec
                                          ,p_ora_error_msg     => SQLERRM
                                          ,p_error_desc        => 'Error running Rebates PAM : transfer_data_to_pam_apex @ pam_payment_det_tbl with PROGRAM ERROR'
                                          ,p_distribution_list => g_err_email
                                          ,p_module            => 'PAM');
    end;
    --
    -- Process pam_purchase_lob_tbl@eaapxprd;
    begin
     --
     delete ea_apps.pam_purchase_lob_tbl@eaapxprd;
     --
     print_log('Deleted existing PAM APEX table pam_purchase_lob_tbl records :'||sql%rowcount);
     --
     commit;
     --
     print_log('Begin: Insert new EBS records into PAM APEX table pam_purchase_lob_tbl records, Start time: '||to_char(sysdate, 'mm/dd/yyyy hh24:mi:ss'));
     --
     insert into ea_apps.pam_purchase_lob_tbl@eaapxprd
     (
              id,
              mvid,
              cust_id,
              mv_name,
              lob_id,
              bu_id,
              period_type,
              year,
              month_id,
              month_name,
              pur_amount,
              source,
              ebs_created_by,
              ebs_creation_date,
              request_id
     )
     (
       select
              id,
              mvid,
              cust_id,
              mv_name,
              lob_id,
              bu_id,
              period_type,
              pam_year,
              month_id,
              month_name,
              pur_amount,
              source,
              ebs_created_by,
              ebs_creation_date,
              request_id
       from   xxcus.xxcus_pam_purchase_lob_tbl
     );
     --
     print_log('End: Insert new EBS records into PAM APEX table pam_purchase_lob_tbl records, Start time: '||to_char(sysdate, 'mm/dd/yyyy hh24:mi:ss')||', total records :'||sql%rowcount);
     --
     commit;
     --
    exception
     when others then
      xxcus_error_pkg.xxcus_error_main_api(p_called_from       => l_err_program
                                          ,p_calling           => l_sec
                                          ,p_ora_error_msg     => SQLERRM
                                          ,p_error_desc        => 'Error running Rebates PAM : transfer_data_to_pam_apex @ pam_purchase_lob_tbl with PROGRAM ERROR'
                                          ,p_distribution_list => g_err_email
                                          ,p_module            => 'PAM');
    end;
    --
  exception
   when others then
    print_log('When Other Errors in transfer_data_to_apex : Message =>'||sqlerrm);
      xxcus_error_pkg.xxcus_error_main_api(p_called_from       => l_err_program
                                          ,p_calling           => l_sec
                                          ,p_ora_error_msg     => SQLERRM
                                          ,p_error_desc        => 'Error running package with PROGRAM ERROR'
                                          ,p_distribution_list => g_err_email
                                          ,p_module            => 'HDS Rebates : PAM');
  end transfer_data_to_pam_apex;
  --
  -- End Ver 1.8
  --
END xxcusozf_pam_pkg;
/