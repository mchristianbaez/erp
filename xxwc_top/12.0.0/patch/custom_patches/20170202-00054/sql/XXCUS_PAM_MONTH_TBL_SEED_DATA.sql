/*
   Ticket#                  Date         Author             Notes
   -------------------------------------------------------------------------------------------------------------------------------------------------------
   TMS: 20170202-00054      11/10/2017   Balaguru Seshadri  Seed data for PAM month table on the Oracle EBS side
*/
declare
begin
delete XXCUS.XXCUS_PAM_MONTH_TBL;
Insert into XXCUS.XXCUS_PAM_MONTH_TBL
   (ID, MONTH_ID, MONTH_NAME, PERIOD_TYPE)
 Values
   (21, 1, 'Jan', 'CALENDAR');
Insert into XXCUS.XXCUS_PAM_MONTH_TBL
   (ID, MONTH_ID, MONTH_NAME, PERIOD_TYPE)
 Values
   (22, 2, 'Feb', 'CALENDAR');
Insert into XXCUS.XXCUS_PAM_MONTH_TBL
   (ID, MONTH_ID, MONTH_NAME, PERIOD_TYPE)
 Values
   (23, 3, 'Mar', 'CALENDAR');
Insert into XXCUS.XXCUS_PAM_MONTH_TBL
   (ID, MONTH_ID, MONTH_NAME, PERIOD_TYPE)
 Values
   (24, 4, 'Apr', 'CALENDAR');
Insert into XXCUS.XXCUS_PAM_MONTH_TBL
   (ID, MONTH_ID, MONTH_NAME, PERIOD_TYPE)
 Values
   (25, 5, 'May', 'CALENDAR');
Insert into XXCUS.XXCUS_PAM_MONTH_TBL
   (ID, MONTH_ID, MONTH_NAME, PERIOD_TYPE)
 Values
   (26, 6, 'Jun', 'CALENDAR');
Insert into XXCUS.XXCUS_PAM_MONTH_TBL
   (ID, MONTH_ID, MONTH_NAME, PERIOD_TYPE)
 Values
   (27, 7, 'Jul', 'CALENDAR');
Insert into XXCUS.XXCUS_PAM_MONTH_TBL
   (ID, MONTH_ID, MONTH_NAME, PERIOD_TYPE)
 Values
   (28, 8, 'Aug', 'CALENDAR');
Insert into XXCUS.XXCUS_PAM_MONTH_TBL
   (ID, MONTH_ID, MONTH_NAME, PERIOD_TYPE)
 Values
   (29, 9, 'Sep', 'CALENDAR');
Insert into XXCUS.XXCUS_PAM_MONTH_TBL
   (ID, MONTH_ID, MONTH_NAME, PERIOD_TYPE)
 Values
   (30, 10, 'Oct', 'CALENDAR');
Insert into XXCUS.XXCUS_PAM_MONTH_TBL
   (ID, MONTH_ID, MONTH_NAME, PERIOD_TYPE)
 Values
   (31, 11, 'Nov', 'CALENDAR');
Insert into XXCUS.XXCUS_PAM_MONTH_TBL
   (ID, MONTH_ID, MONTH_NAME, PERIOD_TYPE)
 Values
   (32, 12, 'Dec', 'CALENDAR');
Insert into XXCUS.XXCUS_PAM_MONTH_TBL
   (ID, MONTH_ID, MONTH_NAME, PERIOD_TYPE)
 Values
   (33, 1, 'Feb', 'FISCAL');
Insert into XXCUS.XXCUS_PAM_MONTH_TBL
   (ID, MONTH_ID, MONTH_NAME, PERIOD_TYPE)
 Values
   (34, 2, 'Mar', 'FISCAL');
Insert into XXCUS.XXCUS_PAM_MONTH_TBL
   (ID, MONTH_ID, MONTH_NAME, PERIOD_TYPE)
 Values
   (35, 3, 'Apr', 'FISCAL');
Insert into XXCUS.XXCUS_PAM_MONTH_TBL
   (ID, MONTH_ID, MONTH_NAME, PERIOD_TYPE)
 Values
   (36, 4, 'May', 'FISCAL');
Insert into XXCUS.XXCUS_PAM_MONTH_TBL
   (ID, MONTH_ID, MONTH_NAME, PERIOD_TYPE)
 Values
   (37, 5, 'Jun', 'FISCAL');
Insert into XXCUS.XXCUS_PAM_MONTH_TBL
   (ID, MONTH_ID, MONTH_NAME, PERIOD_TYPE)
 Values
   (38, 6, 'Jul', 'FISCAL');
Insert into XXCUS.XXCUS_PAM_MONTH_TBL
   (ID, MONTH_ID, MONTH_NAME, PERIOD_TYPE)
 Values
   (39, 7, 'Aug', 'FISCAL');
Insert into XXCUS.XXCUS_PAM_MONTH_TBL
   (ID, MONTH_ID, MONTH_NAME, PERIOD_TYPE)
 Values
   (40, 8, 'Sep', 'FISCAL');
Insert into XXCUS.XXCUS_PAM_MONTH_TBL
   (ID, MONTH_ID, MONTH_NAME, PERIOD_TYPE)
 Values
   (41, 9, 'Oct', 'FISCAL');
Insert into XXCUS.XXCUS_PAM_MONTH_TBL
   (ID, MONTH_ID, MONTH_NAME, PERIOD_TYPE)
 Values
   (42, 10, 'Nov', 'FISCAL');
Insert into XXCUS.XXCUS_PAM_MONTH_TBL
   (ID, MONTH_ID, MONTH_NAME, PERIOD_TYPE)
 Values
   (43, 11, 'Dec', 'FISCAL');
Insert into XXCUS.XXCUS_PAM_MONTH_TBL
   (ID, MONTH_ID, MONTH_NAME, PERIOD_TYPE)
 Values
   (44, 12, 'Jan', 'FISCAL');
COMMIT;
end;
/