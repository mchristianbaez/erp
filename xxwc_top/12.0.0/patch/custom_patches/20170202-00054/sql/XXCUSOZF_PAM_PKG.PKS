CREATE OR REPLACE PACKAGE APPS."XXCUSOZF_PAM_PKG" IS

  -- -----------------------------------------------------------------------------
  -- |----------------------------< XXCUSOZF_PAM_PKG >----------------------------|
  -- -----------------------------------------------------------------------------
  --
  -- {Start Of Comments}
  --
  -- Description:
  --   This is the truncate package.
  --
  --   VERSION DATE          AUTHOR(S)       DESCRIPTION
  -- ------- -----------   --------------- ---------------------------------
  -- 1.0     17-APR-2013   Dragan           Created this package.
  -- 1.1     09-JUL-2013   Dragan           Added repop_pam_sv_report procedure
  --                                        RFC 37489
  -- 1.2    02-FEB-2017   Bala Seshadri  Add global variables and new extract routines
  --
  g_year varchar2(4) :=Null; -- Ver 1.2
  g_from_year number; -- Ver 1.2
  g_to_year number; -- Ver 1.2
  g_extract_fiscal_start date; -- Ver 1.2
  g_extract_fiscal_end date; -- Ver 1.2  
  g_customer_id number :=Null; --Ver 1.2
  g_pull_PAM_extract_months number :=-36; --Ver 1.2
  g_months_to_pull_payments number :=-36; -- three years or 36 months -- Ver 1.2
  --
  PROCEDURE repop_main (pam_err out varchar2,
                        pam_sec out number
                        );
  --PROCEDURE repop_vend_lov; -- Ver 1.2
  --PROCEDURE repop_program; -- Ver 1.2
  --PROCEDURE repop_pam_purch; -- Ver 1.2
  --PROCEDURE repop_pam_income; -- Ver 1.2
  --PROCEDURE repop_pam_payments; -- Ver 1.2
  --PROCEDURE repop_pam_incomelob; -- Ver 1.2
  --PROCEDURE repop_pam_paydet; -- Ver 1.2
  --PROCEDURE repop_pam_lob_purchase; -- Ver 1.2
  --PROCEDURE repop_pam_sv_report; -- Ver 1.2
  -- Begin Ver 1.2
  --
    FUNCTION get_year return number;
  --
    PROCEDURE extract_bu_info (p_user_id in number, p_request_id in number);
  --
    PROCEDURE extract_vend_lov (p_user_id in number, p_request_id in number);
  --
    PROCEDURE extract_program_lov (p_user_id in number, p_request_id in number);
  -- 
  PROCEDURE extract_pam_purch (p_user_id in number, p_request_id in number);
  --
    PROCEDURE extract_pam_income (p_user_id in number, p_request_id in number);
  --
    PROCEDURE extract_pam_payments (p_user_id in number, p_request_id in number);
  --
    PROCEDURE extract_cal_pam_incomelob (p_user_id in number, p_request_id in number);
    --
    PROCEDURE extract_cal_pam_paydet (p_user_id in number, p_request_id in number);
    --
    PROCEDURE extract_cal_pam_lob_purchase (p_user_id in number, p_request_id in number);
    --
    PROCEDURE pam_income_table_filler (p_from_year in number, p_to_year in number);
    --
    PROCEDURE pam_purchase_table_filler (p_from_year in number, p_to_year in number);
    --
    PROCEDURE backup_apex_pam_current_data;
    --    
    PROCEDURE transfer_data_to_pam_apex;    
    --
  -- End Ver 1.2
END xxcusozf_pam_pkg;
/