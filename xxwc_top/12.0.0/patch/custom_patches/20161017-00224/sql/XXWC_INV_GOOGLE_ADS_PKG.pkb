CREATE OR REPLACE PACKAGE BODY APPS.XXWC_INV_GOOGLE_ADS_PKG
AS
   /**************************************************************************************************
    Copyright (c) HD Supply Group
    All rights reserved.
   ***************************************************************************************************
     $Header xxwc_inv_google_ads_pkg $
     Module Name: xxwc_inv_google_ads_pkg.pks

     PURPOSE:

     REVISIONS:
     Ver        Date        Author               Description
     ---------  ----------  ---------------      ------------------------
     1.0        01/31/2017  P.Vamshidhar         Initial Version(TMS#20161104-00076)
	 1.1        05/26/2017  Pattabhi Avula       Initial Version(TMS#20161017-00224)

    *************************************************************************************************/

   g_err_callfrom         VARCHAR2 (100) := 'XXWC_INV_GOOGLE_ADS_PKG';
   g_distro_list          VARCHAR2 (75) DEFAULT 'HDSOracleDevelopers@hdsupply.com';
   g_outbound_directory   VARCHAR2 (80) := 'XXWC_GOOGLE_LOCAL_ADS';
   g_errbuf               VARCHAR2 (1000);
   g_retcode              VARCHAR2 (10) := 0;

   /*************************************************************************************************
     Procedure : main

     PURPOSE:   This procedure will pull the website items for all child branches

     REVISIONS:
     Ver        Date        Author                     Description
     ---------  ----------  ---------------         -------------------------
     1.0        01/31/2017  P.Vamshidhar             Initial Version(TMS#20161104-00076)

   *************************************************************************************************/

   PROCEDURE main (x_errbuf            OUT VARCHAR2,
                   x_retcode           OUT VARCHAR2,
                   p_extract_type   IN     VARCHAR2,
                   p_on_hand_qty    IN     VARCHAR2,
                   p_web_item       IN     VARCHAR2)
   IS
   l_onhnd_qty NUMBER(10); -- Ver#1.1
   BEGIN
      IF UPPER (p_extract_type) = 'ITEM'
      THEN
         FND_FILE.PUT_LINE (FND_FILE.LOG, 'Item Extract Procedure Begin');
         item_extract (p_on_hand_qty, p_web_item);
         FND_FILE.PUT_LINE (FND_FILE.LOG, 'Item Extract Procedure End');
      ELSIF UPPER (p_extract_type) = 'INVENTORY'
      THEN
         FND_FILE.PUT_LINE (FND_FILE.LOG,
                            'Inventory Extract Procedure Begin');
         inventory_extract (p_on_hand_qty, p_web_item);
         FND_FILE.PUT_LINE (FND_FILE.LOG, 'Inventory Extract Procedure End');
		--<START> Ver#1.1
	  ELSIF UPPER (p_extract_type) = 'PLA'
      THEN
         FND_FILE.PUT_LINE (FND_FILE.LOG,
                            'PLA Extract Procedure Begin');
		BEGIN
         IF p_on_hand_qty='Y' 
		  THEN
		  l_onhnd_qty:=FND_PROFILE.VALUE('XXWC_INV_PLA_ONHAND_QTY_VAL');
		 ELSE
          l_onhnd_qty:=0;
		 END IF; 
       	FND_FILE.PUT_LINE (FND_FILE.LOG, 'Profile value '||l_onhnd_qty);	  
         PLA_FEED (l_onhnd_qty, p_web_item);
		END; 
         FND_FILE.PUT_LINE (FND_FILE.LOG, 'PLA Extract Procedure End');
		 --<END> Ver#1.1
      ELSE
         NULL;
      END IF;

      x_retcode := NVL (g_retcode, 0);
   EXCEPTION
      WHEN OTHERS
      THEN
         x_retcode := 2;
         x_errbuf := SUBSTR (SQLERRM, 1, 250);
   END;


   /*********************************************************************************************************
     Procedure : google_prod_category

     PURPOSE:   Created this functionto derive google_product_category for required item and organization

     REVISIONS:
     Ver        Date        Author                     Description
     ---------  ----------  ---------------         -------------------------
     1.0        01/31/2017  P.Vamshidhar             Initial Version(TMS#20161104-00076)

   **********************************************************************************************************/

   FUNCTION google_prod_category (P_INVENTORY_ITEM_ID   IN NUMBER,
                                  P_ORGANIZATION_ID     IN NUMBER)
      RETURN VARCHAR2
   IS
      ln_category_id        MTL_CATEGORIES.CATEGORY_ID%TYPE;
      ln_parent_cat_id      MTL_CATEGORIES.CATEGORY_ID%TYPE;
      lvc_category_set_id   MTL_ITEM_CATEGORIES.category_set_id%TYPE
                               := 1100000085;
      lvc_google_cat        VARCHAR2 (4000) := NULL;
   BEGIN
      SELECT category_id
        INTO ln_category_id
        FROM apps.mtl_item_categories
       WHERE     inventory_item_id = P_INVENTORY_ITEM_ID
             AND organization_id = P_ORGANIZATION_ID
             AND category_set_id = lvc_category_set_id;


      BEGIN
         SELECT CATEGORY_ID
           INTO ln_parent_cat_id
           FROM (    SELECT IC.CATEGORY_ID, IC.PARENT_CATEGORY_ID
                       FROM MTL_CATEGORY_SET_VALID_CATS IC
                 START WITH     CATEGORY_ID = ln_category_id
                            AND CATEGORY_SET_ID = lvc_category_set_id
                 CONNECT BY     PRIOR PARENT_CATEGORY_ID = CATEGORY_ID
                            AND CATEGORY_SET_ID = lvc_category_set_id)
          WHERE PARENT_CATEGORY_ID IS NULL AND ROWNUM < 2;
      EXCEPTION
         WHEN OTHERS
         THEN
            ln_category_id := 0;
      END;

      SELECT GOOGLE_PROD_CATEGORY
        INTO lvc_google_cat
        FROM XXWC.XXWC_LOC_GOOGLE_PRODCATA_TBL XLGPT
       WHERE ORA_CATEGORY_ID = ln_parent_cat_id;

      RETURN lvc_google_cat;
   EXCEPTION
      WHEN OTHERS
      THEN
         lvc_google_cat := 'Hardware';
         RETURN lvc_google_cat;
   END;

   /*********************************************************************************************************
     Procedure : ITEM_EXTRACT

     PURPOSE:   Created this procedure to generate  item extract (master level data)

     REVISIONS:
     Ver        Date        Author                     Description
     ---------  ----------  ---------------         -------------------------
     1.0        01/31/2017  P.Vamshidhar             Initial Version(TMS#20161104-00076)

   **********************************************************************************************************/
   PROCEDURE ITEM_EXTRACT (p_on_hand_qty IN VARCHAR2, p_web_item IN VARCHAR2)
   IS
      CURSOR CUR_ITEM (
         p_item_url    VARCHAR2)
      IS
         SELECT msi.inventory_item_id,
                msi.segment1 item_id,
                msi.DESCRIPTION,
                (SELECT TL_EXT_ATTR2 TL_EXT_ATTR2
                   FROM APPS.EGO_MTL_SY_ITEMS_EXT_TL
                  WHERE     INVENTORY_ITEM_ID = MSI.INVENTORY_ITEM_ID
                        AND ATTR_GROUP_ID = 479
                        AND ORGANIZATION_ID = 222
                        AND ROWNUM < 2)
                   LONG_DESCRIPTION,
                p_item_url || 'f_' || msi.segment1 || '.jpg' image_link,
                (SELECT CROSS_REFERENCE
                   FROM apps.mtl_cross_references
                  WHERE     CROSS_REFERENCE_TYPE = 'UPC'
                        AND INVENTORY_ITEM_ID = msi.inventory_item_id
                        AND ROWNUM < 2)
                   GTIN,
                (SELECT ATTRIBUTE_CHAR_VALUE
                   FROM XXWC_EGO_ALL_ATTR_V
                  WHERE     INVENTORY_ITEM_ID = MSI.INVENTORY_ITEM_ID
                        AND ATTRIBUTE_NAME = 'XXWC_M_P_N_ATTR'
                        AND ROWNUM < 2)
                   MPN,
                (SELECT ATTRIBUTE_CHAR_VALUE
                   FROM XXWC_EGO_ALL_ATTR_BASE_V
                  WHERE     INVENTORY_ITEM_ID = MSI.INVENTORY_ITEM_ID
                        AND ATTRIBUTE_NAME = 'XXWC_BRAND_ATTR'
                        AND ROWNUM < 2)
                   BRAND,
                apps.xxwc_inv_google_ads_pkg.GOOGLE_PROD_CATEGORY (
                   msi.inventory_item_id,
                   msi.organization_id)
                   google_prod_cate,
                (SELECT ATTRIBUTE_CHAR_VALUE
                   FROM XXWC_EGO_ALL_ATTR_BASE_V
                  WHERE     INVENTORY_ITEM_ID = MSI.INVENTORY_ITEM_ID
                        AND ATTRIBUTE_NAME = 'XXWC_COLOR_ATTR'
                        AND ROWNUM < 2)
                   COLOR,
                (SELECT ATTRIBUTE_CHAR_VALUE
                   FROM XXWC_EGO_ALL_ATTR_BASE_V
                  WHERE     INVENTORY_ITEM_ID = MSI.INVENTORY_ITEM_ID
                        AND ATTRIBUTE_NAME = 'XXWC_SIZE_ATTR'
                        AND ROWNUM < 2)
                   item_SIZE,
                (SELECT ATTRIBUTE_CHAR_VALUE
                   FROM XXWC_EGO_ALL_ATTR_BASE_V
                  WHERE     INVENTORY_ITEM_ID = MSI.INVENTORY_ITEM_ID
                        AND ATTRIBUTE_NAME = 'XXWC_GENDER_ATTR'
                        AND ROWNUM < 2)
                   gender,
                (SELECT 'Adult'
                   FROM XXWC_EGO_ALL_ATTR_BASE_V
                  WHERE     attribute_group_name IN ('XXWC_FOOTWEAR_AG',
                                                     'XXWC_GENERAL_CLOTHING_AG',
                                                     'XXWC_HIGH_VIS_APPAREL_AG',
                                                     'XXWC_RAIN_WEAR_AG')
                        AND inventory_item_id = MSI.INVENTORY_ITEM_ID
                        AND ROWNUM < 2)
                   age_group
           FROM APPS.MTL_SYSTEM_ITEMS_B msi,
                (SELECT INVENTORY_ITEM_ID, CATEGORY_CONCAT_SEGS
                   FROM mtl_item_categories_V
                  WHERE     CATEGORY_SET_ID = 1100000063
                        AND ORGANIZATION_ID = 222) WEB
          WHERE     1 = 1
                AND msi.organization_id = 222
                AND msi.inventory_item_status_code <> 'Inactive'
                AND msi.inventory_item_id = web.inventory_item_id
                AND web.CATEGORY_CONCAT_SEGS =
                       DECODE (p_web_item,
                               'Y', 'Y',
                               'N', 'N',
                               web.CATEGORY_CONCAT_SEGS)
                AND DECODE (p_on_hand_qty,  'Y', 'Y',  'ALL', 'Y',  'N') =
                       'Y'
                AND EXISTS
                       (SELECT 1
                          FROM APPS.mtl_onhand_quantities MOHQ
                         WHERE     MOHQ.INVENTORY_ITEM_ID =
                                      msi.inventory_item_id
                               AND NVL (TRANSACTION_QUANTITY, 0) > 0)
         UNION
         SELECT msi.inventory_item_id,
                msi.segment1 item_id,
                msi.DESCRIPTION,
                (SELECT TL_EXT_ATTR2
                   FROM APPS.EGO_MTL_SY_ITEMS_EXT_TL
                  WHERE     INVENTORY_ITEM_ID = MSI.INVENTORY_ITEM_ID
                        AND ATTR_GROUP_ID = 479
                        AND ORGANIZATION_ID = 222
                        AND ROWNUM < 2)
                   LONG_DESCRIPTION,
                p_item_url || 'f_' || msi.segment1 || '.jpg' image_link,
                (SELECT CROSS_REFERENCE
                   FROM apps.mtl_cross_references
                  WHERE     CROSS_REFERENCE_TYPE = 'UPC'
                        AND INVENTORY_ITEM_ID = msi.inventory_item_id
                        AND ROWNUM < 2)
                   GTIN,
                (SELECT ATTRIBUTE_CHAR_VALUE
                   FROM XXWC_EGO_ALL_ATTR_V
                  WHERE     INVENTORY_ITEM_ID = MSI.INVENTORY_ITEM_ID
                        AND ATTRIBUTE_NAME = 'XXWC_M_P_N_ATTR'
                        AND ROWNUM < 2)
                   MPN,
                (SELECT ATTRIBUTE_CHAR_VALUE
                   FROM XXWC_EGO_ALL_ATTR_BASE_V
                  WHERE     INVENTORY_ITEM_ID = MSI.INVENTORY_ITEM_ID
                        AND ATTRIBUTE_NAME = 'XXWC_BRAND_ATTR'
                        AND ROWNUM < 2)
                   BRAND,
                apps.xxwc_inv_google_ads_pkg.GOOGLE_PROD_CATEGORY (
                   msi.inventory_item_id,
                   msi.organization_id)
                   google_prod_cate,
                (SELECT ATTRIBUTE_CHAR_VALUE
                   FROM XXWC_EGO_ALL_ATTR_BASE_V
                  WHERE     INVENTORY_ITEM_ID = MSI.INVENTORY_ITEM_ID
                        AND ATTRIBUTE_NAME = 'XXWC_COLOR_ATTR'
                        AND ROWNUM < 2)
                   COLOR,
                (SELECT ATTRIBUTE_CHAR_VALUE
                   FROM XXWC_EGO_ALL_ATTR_BASE_V
                  WHERE     INVENTORY_ITEM_ID = MSI.INVENTORY_ITEM_ID
                        AND ATTRIBUTE_NAME = 'XXWC_SIZE_ATTR'
                        AND ROWNUM < 2)
                   item_SIZE,
                (SELECT ATTRIBUTE_CHAR_VALUE
                   FROM XXWC_EGO_ALL_ATTR_BASE_V
                  WHERE     INVENTORY_ITEM_ID = MSI.INVENTORY_ITEM_ID
                        AND ATTRIBUTE_NAME = 'XXWC_GENDER_ATTR'
                        AND ROWNUM < 2)
                   gender,
                (SELECT 'Adult'
                   FROM XXWC_EGO_ALL_ATTR_BASE_V
                  WHERE     attribute_group_name IN ('XXWC_FOOTWEAR_AG',
                                                     'XXWC_GENERAL_CLOTHING_AG',
                                                     'XXWC_HIGH_VIS_APPAREL_AG',
                                                     'XXWC_RAIN_WEAR_AG')
                        AND inventory_item_id = MSI.INVENTORY_ITEM_ID
                        AND ROWNUM < 2)
                   age_group
           FROM APPS.MTL_SYSTEM_ITEMS_B msi,
                (SELECT INVENTORY_ITEM_ID, CATEGORY_CONCAT_SEGS
                   FROM mtl_item_categories_V
                  WHERE     CATEGORY_SET_ID = 1100000063
                        AND ORGANIZATION_ID = 222) WEB
          WHERE     1 = 1
                AND msi.organization_id = 222
                AND msi.inventory_item_status_code <> 'Inactive'
                AND msi.inventory_item_id = web.inventory_item_id
                AND web.CATEGORY_CONCAT_SEGS =
                       DECODE (p_web_item,
                               'Y', 'Y',
                               'N', 'N',
                               web.CATEGORY_CONCAT_SEGS)
                AND DECODE (p_on_hand_qty,  'N', 'N',  'ALL', 'N',  'Y') =
                       'N'
                AND NOT EXISTS
                           (SELECT 1
                              FROM APPS.mtl_onhand_quantities MOHQ
                             WHERE     MOHQ.INVENTORY_ITEM_ID =
                                          msi.inventory_item_id
                                   AND NVL (TRANSACTION_QUANTITY, 0) > 0);

      LVC_WCCOM_URL      VARCHAR2 (300)
         := FND_PROFILE.VALUE ('XXWC_INV_WC_WEBSITE_IMAGE_URL');
      lvc_procedure      VARCHAR2 (100) := 'ITEM_EXTRACT';
      l_err_callpoint    VARCHAR2 (150) := 'START';
      l_message          VARCHAR2 (2000);

      l_item_file_name   VARCHAR2 (80);
      l_item_outfile     UTL_FILE.file_type;
      lvc_description    MTL_SYSTEM_ITEMS_B.DESCRIPTION%TYPE;
      lvc_web_long       APPS.EGO_MTL_SY_ITEMS_EXT_TL.TL_EXT_ATTR2%TYPE;
   BEGIN
      l_err_callpoint := 'Procedure Start';



      EXECUTE IMMEDIATE 'TRUNCATE TABLE XXWC.XXWC_LOC_GOOGLE_PRODCATA_TBL';

      l_err_callpoint := 'Inserting data into XXWC_LOC_GOOGLE_PRODCATA_TBL';

      INSERT INTO XXWC.XXWC_LOC_GOOGLE_PRODCATA_TBL (ORA_CATEGORY_ID,
                                                     ORA_PROD_CATEGORY,
                                                     GOOGLE_PROD_CATEGORY,
                                                     CREATION_DATE,
                                                     CREATED_BY,
                                                     LAST_UPDATED_DATE,
                                                     LAST_UPDATED_BY)
         (SELECT ORA_CATEGORY_ID,
                 ORA_PROD_CATEGORY,
                 GOOGLE_PROD_CATEGORY,
                 CREATION_DATE,
                 CREATED_BY,
                 LAST_UPDATED_DATE,
                 LAST_UPDATED_BY
            FROM XXWC_LOC_GOOGLE_PRODCATA_TBL@WCAPXPRD.HSI.HUGHESSUPPLY.COM);

      FND_FILE.PUT_LINE (
         FND_FILE.LOG,
         'Data inserted into XXWC_LOC_GOOGLE_PRODCATA_TBL ' || SQL%ROWCOUNT);


      l_err_callpoint := 'File name creation';

      l_item_file_name :=
         'Loc_Prod_Item_Feed_' || TO_CHAR (SYSDATE, 'MMDDYYYY') || '.txt';

      FND_FILE.PUT_LINE (FND_FILE.LOG, ' Fie Name ' || l_item_file_name);

      l_err_callpoint := 'Opening Outfile';

      l_item_outfile :=
         UTL_FILE.fopen (g_outbound_directory,
                         l_item_file_name,
                         'W',
                         32766);

      UTL_FILE.put_line (
         l_item_outfile,
            'itemid'
         || CHR (9)
         || 'title'
         || CHR (9)
         || 'description'
         || CHR (9)
         || 'image_link'
         || CHR (9)
         || 'condition'
         || CHR (9)
         || 'gtin'
         || CHR (9)
         || 'mpn'
         || CHR (9)
         || 'brand'
         || CHR (9)
         || 'google_product_category'
         || CHR (9)
         || 'color'
         || CHR (9)
         || 'size'
         || CHR (9)
         || 'gender'
         || CHR (9)
         || 'age_group');

      l_err_callpoint := 'Before Loop Starting';


      FOR rec_item IN cur_item (LVC_WCCOM_URL)
      LOOP
         l_err_callpoint :=
               ' Inventory_item_id '
            || rec_item.item_id
            || ' REGEXP Description ';


         lvc_description :=
            REGEXP_REPLACE (rec_item.DESCRIPTION,
                            '[[:cntrl:]][[:space:]]',
                            '');

         l_err_callpoint :=
               ' Inventory_item_id '
            || rec_item.item_id
            || ' REGEXP Long Description ';

         lvc_web_long :=
            REGEXP_REPLACE (rec_item.LONG_DESCRIPTION,
                            '[[:cntrl:]][[:space:]]',
                            '');

         l_err_callpoint := ' Inventory_item_id ' || rec_item.item_id;

         BEGIN
            UTL_FILE.put_line (
               l_item_outfile,
                  rec_item.item_id
               || CHR (9)
               || rec_item.description
               || CHR (9)
               || lvc_web_long
               || CHR (9)
               || rec_item.image_link
               || CHR (9)
               || 'New'
               || CHR (9)
               || rec_item.GTIN
               || CHR (9)
               || rec_item.mpn
               || CHR (9)
               || rec_item.brand
               || CHR (9)
               || rec_item.google_prod_cate
               || CHR (9)
               || rec_item.color
               || CHR (9)
               || rec_item.item_size
               || CHR (9)
               || rec_item.gender
               || CHR (9)
               || rec_item.age_group);
         EXCEPTION
            WHEN UTL_FILE.write_error
            THEN
               fnd_file.put_line (fnd_file.LOG,
                                  'write_error ' || rec_item.item_id);
               g_retcode := 1;
            WHEN OTHERS
            THEN
               fnd_file.put_line (
                  fnd_file.LOG,
                     ' Item '
                  || rec_item.item_id
                  || ' Error mess '
                  || SUBSTR (SQLERRM, 1, 250));
               g_retcode := 1;
         END;
      END LOOP;

      l_err_callpoint := ' File Close';

      UTL_FILE.fclose (l_item_outfile);
   EXCEPTION
      WHEN OTHERS
      THEN
         g_retcode := 1;

         IF UTL_FILE.IS_OPEN (l_item_outfile)
         THEN
            UTL_FILE.FCLOSE (l_item_outfile);
         END IF;

         l_message := SUBSTR (SQLERRM, 1, 250);
         xxcus_error_pkg.xxcus_error_main_api (
            p_called_from         => g_err_callfrom || '.' || lvc_procedure,
            p_calling             => l_err_callpoint,
            p_ora_error_msg       => l_message,
            p_error_desc          => l_message,
            p_distribution_list   => g_distro_list,
            p_module              => 'XXWC');
   END;

   /*********************************************************************************************************
     Procedure : UNLOAD_DATA

     PURPOSE:   Creating output file

     REVISIONS:
     Ver        Date        Author                     Description
     ---------  ----------  ---------------         -------------------------
     1.0        01/31/2017  P.Vamshidhar             Initial Version(TMS#20161104-00076)

   **********************************************************************************************************/

   FUNCTION UNLOAD_DATA (p_my_cursor   IN SYS_REFCURSOR,
                         p_file_name   IN VARCHAR2,
                         p_file_path   IN VARCHAR2,
                         p_file_extn   IN VARCHAR2)
      RETURN XXWC_SPOOL_FILE_TBL_NTT
      PIPELINED
   AS
      TYPE rows_ntt IS TABLE OF VARCHAR2 (32767);

      my_rows              rows_ntt;
      file_id              UTL_FILE.file_type;
      v_buffer             VARCHAR2 (32767);
      n_sid                NUMBER;
      v_sid                VARCHAR2 (10);
      v_name               VARCHAR2 (150);
      n_lines              PLS_INTEGER := 0;
      C_EOL       CONSTANT VARCHAR2 (1) := CHR (10);
      C_EOL_LEN   CONSTANT PLS_INTEGER := LENGTH (C_EOL);
      C_MAXLINE   CONSTANT PLS_INTEGER := 32767;
      --
      d_start_date         DATE := SYSDATE;
      d_end_date           DATE := NULL;
      p_limit              NUMBER := 5000;
   BEGIN
      -- Get the SID padded to append the file names for ease of identification
      n_sid := SYS_CONTEXT ('USERENV', 'sid');
      v_sid := LPAD (SYS_CONTEXT ('USERENV', 'sid'), 10, 0);

      --Assign the file name
      v_name := p_file_name || p_file_extn;

      --Get the file pointer
      file_id :=
         UTL_FILE.fopen (p_file_path,
                         v_name,
                         'A',
                         C_MAXLINE);

      LOOP
         FETCH p_my_cursor BULK COLLECT INTO my_rows LIMIT p_limit;

         EXIT WHEN my_rows.COUNT = 0;

         IF my_rows.COUNT > 0
         THEN
            FOR idx IN 1 .. my_rows.COUNT
            LOOP
               UTL_FILE.put_line (file_id, my_rows (idx));
            END LOOP;

            n_lines := n_lines + my_rows.COUNT;
         END IF;
      END LOOP;

      CLOSE p_my_cursor;

      --      UTL_FILE.put_line (file_id, v_buffer);
      UTL_FILE.fclose (file_id);
      PIPE ROW (XXWC_SPOOL_FILE_TBL (v_name,
                                     n_lines,
                                     n_sid,
                                     d_start_date,
                                     SYSDATE));
   EXCEPTION
      WHEN OTHERS
      THEN
         FND_FILE.PUT_LINE (
            FND_FILE.LOG,
            '@unload_data, outer block, when others -message =' || SQLERRM);
   END unload_data;

   /*********************************************************************************************************
     Procedure : INVENTORY_EXTRACT

     PURPOSE:   Created this procedure to generate inventory extract (org level)

     REVISIONS:
     Ver        Date        Author                     Description
     ---------  ----------  ---------------         -------------------------
     1.0        01/31/2017  P.Vamshidhar             Initial Version(TMS#20161104-00076)

   **********************************************************************************************************/

   PROCEDURE INVENTORY_EXTRACT (p_on_hand_qty   IN VARCHAR2,
                                p_web_item      IN VARCHAR2)
   IS
      lvc_procedure           VARCHAR2 (100) := 'INVENTORY_EXTRACT';
      l_err_callpoint         VARCHAR2 (500) := 'START';
      l_message               VARCHAR2 (2000);
      l_inventory_file_name   VARCHAR2 (80);
      l_inventory_outfile     UTL_FILE.file_type;

      CURSOR cur_web_items
      IS
         SELECT DISTINCT organization_id
           FROM XXWC.XXWC_GOOGLE_LOC_INVENTORY_TBL;

      ln_count                NUMBER;
      ln_price                NUMBER;

      CURSOR cur_table_data
      IS
         SELECT DISTINCT
                'XXWC.XXWC_ORG_ITEM_PRICE_' || organization_id || '##'
                   table_name
           FROM XXWC.XXWC_GOOGLE_LOC_INVENTORY_TBL;


      CURSOR files_written (
         p_file   IN VARCHAR2,
         p_path   IN VARCHAR2,
         p_extn   IN VARCHAR2)
      IS
         SELECT my_tbl.file_name,
                my_tbl.total_lines,
                my_tbl.session_id,
                TO_CHAR (my_tbl.start_date, 'DD-MON-YYYY HH24:MI:SS')
                   start_date_time,
                TO_CHAR (my_tbl.end_date, 'DD-MON-YYYY HH24:MI:SS')
                   end_date_time
           FROM TABLE (XXWC_INV_GOOGLE_ADS_PKG.UNLOAD_DATA (
                          CURSOR (
                             SELECT /*+ PARALLEL(XGLI, 2, 1) */
                                   organization_code
                                    || CHR (9)
                                    || item_number
                                    || CHR (9)
                                    || on_hand_qty
                                    || CHR (9)
                                    || TO_CHAR (item_price, 'FM9999990.00')
                                    || CHR (9)
                                    || availability
                                    || CHR (9)
                                    || ' '
                                    || CHR (9)
                                    || ' '
                                       google_local_data
                               FROM XXWC.XXWC_GOOGLE_LOC_INVENTORY_TEMP XGLI),
                          p_file,
                          p_path,
                          p_extn)) my_tbl;

      v_cr                    VARCHAR2 (1) := '';
      lvc_file_extn           VARCHAR2 (10) := '.txt';
      l_child_request_id      NUMBER;
      lvc_Application         Fnd_Application.Application_Short_Name%TYPE
                                 := 'XXWC';
      lvc_Cp_Short_Code       Fnd_Concurrent_Programs.Concurrent_Program_Name%TYPE
         := 'XXWC_GOOGLE_LOC_ADS_CHILD';
      l_request_data          VARCHAR2 (20) := '';
      l_parent_request_id     NUMBER;
      lvc_temp_insert         VARCHAR2 (4000);
      lvc_drop_stat           VARCHAR2 (300);
   BEGIN
      FND_FILE.PUT_LINE (FND_FILE.LOG, 'Procedure Start');

      l_request_data := fnd_conc_global.request_data;

      l_parent_request_id := fnd_global.conc_request_id;

      IF l_request_data IS NULL
      THEN
         l_request_data := '1';

         l_err_callpoint := 'Inserting GTT';

         -- Inserting data into XXWC.XXWC_GOOGLE_LOC_INVENTORY_TBL
         EXECUTE IMMEDIATE
            'TRUNCATE TABLE XXWC.XXWC_GOOGLE_LOC_INVENTORY_TBL';


         INSERT INTO /*+ APPEND */
                    XXWC.XXWC_GOOGLE_LOC_INVENTORY_TBL (INVENTORY_ITEM_ID,
                                                        ORGANIZATION_ID,
                                                        PRIMARY_UOM_CODE,
                                                        ORGANIZATION_CODE,
                                                        ITEM_NUMBER,
                                                        ON_HAND_QTY,
                                                        AVAILABILITY)
            SELECT msi.inventory_item_id,
                   msi.organization_id,
                   msi.primary_uom_code,
                   TO_CHAR (orgs.organization_code),
                   msi.segment1,
                   0 on_hand_qty,
                   'Out of Stock' Availability
              FROM APPS.mtl_system_items_b msi,
                   (SELECT organization_id, organization_code
                      FROM APPS.mtl_parameters orgs
                     WHERE     master_organization_id = 222
                           AND organization_id <> 222
                           AND attribute7 = 'Store'
                           AND NOT EXISTS
                                      (SELECT ffv.flex_value
                                         FROM apps.fnd_flex_value_sets ffvs,
                                              apps.fnd_flex_values ffv
                                        WHERE     ffvs.flex_value_set_id =
                                                     ffv.flex_value_set_id
                                              AND ffvs.flex_value_set_name =
                                                     'XXWC_ORG_GOOGLE_LOC_EXCLUDE_VS'
                                              AND ffv.enabled_flag = 'Y'
                                              AND NVL (ffv.start_date_active,
                                                       SYSDATE - 1) <=
                                                     SYSDATE
                                              AND NVL (ffv.end_date_active,
                                                       SYSDATE + 1) >=
                                                     SYSDATE
                                              AND ffv.flex_value =
                                                     orgs.organization_code))
                   orgs,
                   (SELECT INVENTORY_ITEM_ID, category_concat_segs
                      FROM mtl_item_categories_V a
                     WHERE     CATEGORY_SET_ID = 1100000063
                           AND ORGANIZATION_ID = 222
                           AND NOT EXISTS
                                      (SELECT 1
                                         FROM apps.mtl_system_items_b msi
                                        WHERE     msi.organization_id = 222
                                              AND msi.inventory_item_status_code =
                                                     'Inactive'
                                              AND msi.inventory_item_id =
                                                     a.inventory_item_id))
                   WEB
             WHERE     msi.organization_id = orgs.organization_id
                   AND msi.inventory_item_status_code <> 'Inactive'
                   AND msi.inventory_item_id = web.inventory_item_id
                   AND web.category_concat_segs =
                          DECODE (p_web_item,
                                  'Y', 'Y',
                                  'N', 'N',
                                  web.category_concat_segs)
                   AND (p_on_hand_qty = 'N' OR p_on_hand_qty = 'ALL')
                   AND NOT EXISTS
                              (SELECT 1
                                 FROM APPS.mtl_onhand_quantities mohq
                                WHERE     mohq.inventory_item_id =
                                             msi.inventory_item_id
                                      AND mohq.organization_id =
                                             msi.organization_id
                                      AND NVL (mohq.transaction_quantity, 0) >
                                             0)
            UNION
              SELECT msi.inventory_item_id,
                     msi.organization_id,
                     msi.primary_uom_code,
                     orgs.organization_code,
                     msi.segment1,
                     SUM (NVL (mohq.transaction_quantity, 0)) on_hand_qty,
                     DECODE (SUM (NVL (mohq.transaction_quantity, 0)),
                             0, 'Out of Stock',
                             1, 'Limited Availability',
                             2, 'Limited Availability',
                             'Available')
                FROM APPS.mtl_system_items_b msi,
                     APPS.mtl_onhand_quantities mohq,
                     APPS.mtl_parameters orgs,
                     (SELECT INVENTORY_ITEM_ID, category_concat_segs
                        FROM mtl_item_categories_V a
                       WHERE     CATEGORY_SET_ID = 1100000063
                             AND ORGANIZATION_ID = 222
                             AND NOT EXISTS
                                        (SELECT 1
                                           FROM apps.mtl_system_items_b msi
                                          WHERE     msi.organization_id = 222
                                                AND msi.inventory_item_status_code =
                                                       'Inactive'
                                                AND msi.inventory_item_id =
                                                       a.inventory_item_id))
                     WEB
               WHERE     msi.inventory_item_id = mohq.inventory_item_id
                     AND msi.organization_id = mohq.organization_id
                     AND msi.organization_id = orgs.organization_id
                     AND orgs.master_organization_id = 222
                     AND orgs.organization_id <> 222
                     AND orgs.attribute7 = 'Store'
                     AND NOT EXISTS
                                (SELECT ffv.flex_value
                                   FROM apps.fnd_flex_value_sets ffvs,
                                        apps.fnd_flex_values ffv
                                  WHERE     ffvs.flex_value_set_id =
                                               ffv.flex_value_set_id
                                        AND ffvs.flex_value_set_name =
                                               'XXWC_ORG_GOOGLE_LOC_EXCLUDE_VS'
                                        AND ffv.enabled_flag = 'Y'
                                        AND NVL (ffv.start_date_active,
                                                 SYSDATE - 1) <= SYSDATE
                                        AND NVL (ffv.end_date_active,
                                                 SYSDATE + 1) >= SYSDATE
                                        AND ffv.flex_value =
                                               orgs.organization_code)
                     AND msi.inventory_item_status_code <> 'Inactive'
                     AND NVL (mohq.transaction_quantity, 0) > 0
                     AND mohq.organization_id != 222
                     AND msi.inventory_item_id = web.inventory_item_id
                     AND web.category_concat_segs =
                            DECODE (p_web_item,
                                    'Y', 'Y',
                                    'N', 'N',
                                    web.category_concat_segs)
                     AND (p_on_hand_qty = 'Y' OR p_on_hand_qty = 'ALL')
            GROUP BY msi.inventory_item_id,
                     msi.organization_id,
                     msi.primary_uom_code,
                     orgs.organization_code,
                     msi.segment1;


         fnd_File.put_line (
            FND_fILE.LOG,
            'Number of records insert into gtt table ' || SQL%ROWCOUNT);

         l_err_callpoint := 'Insert completed';

         l_err_callpoint := 'Submitting child requests';

         FOR rec_web_items IN cur_web_items
         LOOP
            --item_pricing_data (rec_web_items.organization_id, 88176, NULL);

            ----- Sub Request Submittion

            l_err_callpoint :=
               'child request ' || rec_web_items.organization_id;


            fnd_file.put_line (fnd_file.LOG, l_err_callpoint);


            l_child_request_id :=
               fnd_request.submit_request (
                  application   => LVC_APPLICATION,
                  program       => LVC_CP_SHORT_CODE,
                  description   => '',
                  start_time    => '',
                  sub_request   => TRUE,
                  argument1     => rec_web_items.organization_id,
                  argument2     => 88176,
                  argument3     => NULL);

            l_err_callpoint :=
                  rec_web_items.organization_id
               || ' Org child request completed - '
               || l_child_request_id;

            FND_FILE.PUT_LINE (FND_FILE.LOG, l_err_callpoint);

            IF l_child_request_id > 0
            THEN
               fnd_file.put_line (
                  fnd_file.LOG,
                     'Submitted XXWC INV Local Product Inventory Org Feed Child ='
                  || rec_web_items.organization_id
                  || ', Request Id ='
                  || l_child_request_id);
            ELSE
               fnd_file.put_line (
                  fnd_file.LOG,
                     'Failed to submit XXWC INV Local Product Inventory Org Feed Child = '
                  || rec_web_items.organization_id);
            END IF;

            l_err_callpoint := 'Concurrent request pause';

            fnd_conc_global.set_req_globals (conc_status    => 'PAUSED',
                                             request_data   => l_request_data);

            l_request_data := TO_CHAR (TO_NUMBER (l_request_data) + 1);
            FND_FILE.PUT_LINE (FND_FILE.LOG, 'Request ' || l_request_data);
         END LOOP;

         COMMIT;
      ELSE
         FND_FILE.PUT_LINE (FND_FILE.LOG, 'Child Requests Completed');


         fnd_file.put_line (
            FND_FILE.LOG,
               ' Price update end '
            || TO_CHAR (SYSDATE, 'DD-MON-YYYY HH24:MI:SS'));

         l_err_callpoint := 'File name creation';

         l_inventory_file_name :=
            'Loc_Prod_Inv_Feed_' || TO_CHAR (SYSDATE, 'MMDDYYYY');

         fnd_file.put_line (fnd_file.LOG,
                            'File Name:' || l_inventory_file_name);

         l_inventory_outfile :=
            UTL_FILE.fopen (g_outbound_directory,
                            l_inventory_file_name || lvc_file_extn,
                            'W',
                            32766);

         l_err_callpoint := 'File Titles writing';

         UTL_FILE.put_line (
            l_inventory_outfile,
               'store code'
            || CHR (9)
            || 'itemid'
            || CHR (9)
            || 'quantity*'
            || CHR (9)
            || 'price'
            || CHR (9)
            || 'availability'
            || CHR (9)
            || 'sale price'
            || CHR (9)
            || 'sales price effective date');

         UTL_FILE.FCLOSE (l_inventory_outfile);

         l_err_callpoint := 'writing data into output file - Start';

         FOR rec_table_data IN cur_table_data
         LOOP
            l_err_callpoint :=
                  'insert into data into temp table for '
               || rec_table_data.table_name;

            EXECUTE IMMEDIATE
               'TRUNCATE TABLE XXWC.XXWC_GOOGLE_LOC_INVENTORY_TEMP';

            lvc_temp_insert :=
                  ' INSERT INTO /*+ APPEND */ XXWC.XXWC_GOOGLE_LOC_INVENTORY_TEMP'
               || '(ORGANIZATION_CODE,ITEM_NUMBER,ON_HAND_QTY,AVAILABILITY,ITEM_PRICE)'
               || '(SELECT ORGANIZATION_CODE,ITEM_NUMBER,ON_HAND_QTY,AVAILABILITY,SELLING_PRICE FROM '
               || rec_table_data.table_name
               || ')';

            FND_FILE.PUT_LINE (FND_FILE.LOG,
                               ' lvc_temp_insert --> ' || lvc_temp_insert);

            EXECUTE IMMEDIATE lvc_temp_insert;

            l_err_callpoint :=
               'Calling unload for table ' || rec_table_data.table_name;

            FOR records
               IN files_written (l_inventory_file_name,
                                 g_outbound_directory,
                                 lvc_file_extn)
            LOOP
               FND_FILE.PUT_LINE (
                  FND_FILE.LOG,
                     'File name ='
                  || records.file_name
                  || v_cr
                  || ', Total lines ='
                  || records.total_lines
                  || v_cr
                  || ', Session id ='
                  || records.session_id
                  || v_cr
                  || ', Start date and time ='
                  || records.start_date_time
                  || v_cr
                  || ', End date and time ='
                  || records.end_date_time);
            END LOOP;

            lvc_drop_stat := 'DROP TABLE ' || rec_table_data.table_name;

            l_err_callpoint := lvc_drop_stat;

            EXECUTE IMMEDIATE lvc_drop_stat;

            FND_FILE.PUT_LINE (FND_FILE.LOG,
                               rec_table_data.table_name || ' Table Dropped');
         END LOOP;

         l_err_callpoint := 'Output file writing completed';
      END IF;
   EXCEPTION
      WHEN OTHERS
      THEN
         --         l_err_callpoint := 'Error occured';
         g_retcode := 1;

         IF UTL_FILE.IS_OPEN (l_inventory_outfile)
         THEN
            UTL_FILE.FCLOSE (l_inventory_outfile);
         END IF;

         l_message := SUBSTR (SQLERRM, 1, 250);
         xxcus_error_pkg.xxcus_error_main_api (
            p_called_from         => g_err_callfrom || '.' || lvc_procedure,
            p_calling             => l_err_callpoint,
            p_ora_error_msg       => l_message,
            p_error_desc          => l_message,
            p_distribution_list   => g_distro_list,
            p_module              => 'XXWC');
   END inventory_extract;


   PROCEDURE xxwc_item_pricing_data (x_errbuf                OUT VARCHAR2,
                                     x_retcode               OUT VARCHAR2,
                                     p_organization_id    IN     NUMBER,
                                     p_cust_account_id    IN     NUMBER,
                                     p_cust_acct_use_id   IN     NUMBER)
   IS
      p_line_tbl                 qp_preq_grp.line_tbl_type;
      p_qual_tbl                 qp_preq_grp.qual_tbl_type;
      p_line_attr_tbl            qp_preq_grp.line_attr_tbl_type;
      p_line_detail_tbl          qp_preq_grp.line_detail_tbl_type;
      p_line_detail_qual_tbl     qp_preq_grp.line_detail_qual_tbl_type;
      p_line_detail_attr_tbl     qp_preq_grp.line_detail_attr_tbl_type;
      p_related_lines_tbl        qp_preq_grp.related_lines_tbl_type;
      p_control_rec              qp_preq_grp.control_record_type;
      x_line_tbl                 qp_preq_grp.line_tbl_type;
      x_line_qual                qp_preq_grp.qual_tbl_type;
      x_line_attr_tbl            qp_preq_grp.line_attr_tbl_type;
      x_line_detail_tbl          qp_preq_grp.line_detail_tbl_type;
      x_line_detail_qual_tbl     qp_preq_grp.line_detail_qual_tbl_type;
      x_line_detail_attr_tbl     qp_preq_grp.line_detail_attr_tbl_type;
      x_related_lines_tbl        qp_preq_grp.related_lines_tbl_type;
      x_return_status            VARCHAR2 (240);
      x_return_status_text       VARCHAR2 (240);
      qual_rec                   qp_preq_grp.qual_rec_type;
      line_attr_rec              qp_preq_grp.line_attr_rec_type;
      line_rec                   qp_preq_grp.line_rec_type;
      detail_rec                 qp_preq_grp.line_detail_rec_type;
      ldet_rec                   qp_preq_grp.line_detail_rec_type;
      rltd_rec                   qp_preq_grp.related_lines_rec_type;
      l_pricing_contexts_tbl     qp_attr_mapping_pub.contexts_result_tbl_type;
      l_qualifier_contexts_tbl   qp_attr_mapping_pub.contexts_result_tbl_type;
      v_line_tbl_cnt             INTEGER;

      i                          BINARY_INTEGER;
      j                          BINARY_INTEGER;
      k                          BINARY_INTEGER;

      l_version                  VARCHAR2 (240);
      l_file_val                 VARCHAR2 (60);
      l_modifier_name            VARCHAR2 (240);
      l_list_header_id           NUMBER;
      l_list_line_id             NUMBER;
      l_incomp_code              VARCHAR2 (30);
      l_modifier_type            VARCHAR2 (30);
      l_line_modifier_type       VARCHAR2 (30);
      l_item_category            NUMBER;
      l_gm_selling_price         NUMBER;
      l_message_level            NUMBER;
      l_currency_code            VARCHAR2 (240);
      l_org_id                   NUMBER := fnd_profile.VALUE ('ORG_ID');
      l_list_line_type_code      VARCHAR2 (30);           -- Shankar 27-Mar-15

      CURSOR cur
      IS
           SELECT inventory_item_id,
                  NVL (primary_uom_code, 'EA') primary_uom_code
             FROM xxwc.xxwc_md_search_product_gtt_tbl
         ORDER BY inventory_item_id;

      l_rec_cntr                 NUMBER;
      l_line_attr_tbl_cntr       NUMBER;
      l_qual_tbl_cntr            NUMBER;
      l_inventory_item_id        NUMBER;
      lvc_table_create           VARCHAR2 (1000);
      ln_num_rows                NUMBER := 0;
   BEGIN
      DELETE FROM XXWC_PA_WAREHOUSE_GT;

      BEGIN
         INSERT INTO xxwc.xxwc_pa_warehouse_gt (organization_id)
              VALUES (p_organization_id);
      EXCEPTION
         WHEN OTHERS
         THEN
            NULL;
      END;

      -------------------------------------------------------------
      -- Delete non-InvOrg items
      -------------------------------------------------------------
      DELETE FROM xxwc.xxwc_md_search_product_gtt_tbl;

      INSERT INTO xxwc.xxwc_md_search_product_gtt_tbl (inventory_item_id,
                                                       primary_uom_code)
         SELECT inventory_item_id, primary_uom_code
           FROM XXWC.XXWC_GOOGLE_LOC_INVENTORY_TBL
          WHERE ORGANIZATION_ID = P_ORGANIZATION_ID;

      ln_num_rows := SQL%ROWCOUNT;

      UPDATE xxwc.xxwc_md_search_product_gtt_tbl stg
         SET stg.primary_uom_code =
                (SELECT msib.primary_uom_code
                   FROM INV.MTL_SYSTEM_ITEMS_B msib
                  WHERE     msib.inventory_item_id = stg.inventory_item_id
                        AND msib.organization_id = p_organization_id),
             QUANTITYONHAND =
                xxwc_ascp_scwb_pkg.get_on_hand (stg.inventory_item_id,
                                                p_organization_id,
                                                'G'),
             OPEN_SALES_ORDERS =
                NVL (
                   xxwc_ascp_scwb_pkg.get_open_orders_qty (
                      stg.inventory_item_id,
                      p_organization_id),
                   0),
             LIST_PRICE =
                xxwc_inv_ais_pkg.shipto_last_price_paid (
                   p_cust_acct_use_id,
                   stg.inventory_item_id),                --TMS 20161128-00028
             LIST_PRICE_2 =
                xxwc_qp_market_price_util_pkg.get_market_list_price (
                   stg.inventory_item_id,
                   p_organization_id);

      UPDATE xxwc.xxwc_md_search_product_gtt_tbl stg
         SET RESERVABLE = NVL (QUANTITYONHAND, 0) - NVL (OPEN_SALES_ORDERS, 0);

      -- TMS 20160525-00219 , 20160801-00182, 20160801-00323 and 20160908-00188 end
      SELECT gsob.currency_code
        INTO l_currency_code
        FROM hr_operating_units hou, gl_sets_of_books gsob
       WHERE     hou.organization_id = l_org_id
             AND gsob.set_of_books_id = hou.set_of_books_id;


      SELECT gsob.currency_code
        INTO l_currency_code
        FROM hr_operating_units hou, gl_sets_of_books gsob
       WHERE     hou.organization_id = l_org_id
             AND gsob.set_of_books_id = hou.set_of_books_id;

      l_rec_cntr := 0;
      l_line_attr_tbl_cntr := 0;
      l_qual_tbl_cntr := 0;

      FOR rec IN cur
      LOOP
         l_rec_cntr := l_rec_cntr + 1;
         l_line_attr_tbl_cntr := l_line_attr_tbl_cntr + 1;
         l_qual_tbl_cntr := l_qual_tbl_cntr + 1;

         l_gm_selling_price := NULL;
         l_item_category := NULL;

         BEGIN
            SELECT mic.category_id
              INTO l_item_category
              FROM mtl_item_categories_v mic
             WHERE     mic.category_set_name = 'Inventory Category'
                   AND mic.inventory_item_id = rec.inventory_item_id
                   AND mic.organization_id = p_organization_id
                   AND ROWNUM = 1;
         EXCEPTION
            WHEN OTHERS
            THEN
               l_item_category := NULL;
         END;

         qp_attr_mapping_pub.build_contexts (
            p_request_type_code           => 'ONT',
            p_pricing_type                => 'L',
            x_price_contexts_result_tbl   => l_pricing_contexts_tbl,
            x_qual_contexts_result_tbl    => l_qualifier_contexts_tbl);

         ---- Control Record
         p_control_rec.pricing_event := 'LINE';                    -- 'BATCH';
         p_control_rec.calculate_flag := 'Y'; --QP_PREQ_GRP.G_SEARCH_N_CALCULATE;
         p_control_rec.simulation_flag := 'Y';
         p_control_rec.rounding_flag := 'Q';
         p_control_rec.manual_discount_flag := 'Y';
         p_control_rec.request_type_code := 'ONT';
         p_control_rec.temp_table_insert_flag := 'Y';

         ---- Line Records ---------
         line_rec.request_type_code := 'ONT';
         line_rec.line_id := -1; -- Order Line Id. This can be any thing for this script
         line_rec.line_index := l_rec_cntr;              -- Request Line Index
         line_rec.line_type_code := 'LINE';     -- LINE or ORDER(Summary Line)
         line_rec.pricing_effective_date := SYSDATE; -- Pricing as of what date ?
         line_rec.active_date_first := SYSDATE; -- Can be Ordered Date or Ship Date
         line_rec.active_date_second := SYSDATE; -- Can be Ordered Date or Ship Date
         line_rec.active_date_first_type := 'NO TYPE';             -- ORD/SHIP
         line_rec.active_date_second_type := 'NO TYPE';            -- ORD/SHIP
         line_rec.line_quantity := 1;                      -- Ordered Quantity

         line_rec.line_uom_code := rec.primary_uom_code;

         line_rec.currency_code := l_currency_code;

         line_rec.price_flag := 'Y'; -- Price Flag can have 'Y' , 'N'(No pricing) , 'P'(Phase)
         p_line_tbl (l_rec_cntr) := line_rec;

         ---- Line Attribute Record
         line_attr_rec.line_index := l_rec_cntr;
         line_attr_rec.pricing_context := 'ITEM';
         line_attr_rec.pricing_attribute := 'PRICING_ATTRIBUTE1';
         line_attr_rec.pricing_attr_value_from :=
            TO_CHAR (rec.inventory_item_id);              -- INVENTORY ITEM ID
         line_attr_rec.validated_flag := 'N';
         p_line_attr_tbl (l_line_attr_tbl_cntr) := line_attr_rec;

         IF l_item_category IS NOT NULL
         THEN
            l_line_attr_tbl_cntr := l_line_attr_tbl_cntr + 1;
            line_attr_rec.line_index := l_rec_cntr;
            line_attr_rec.pricing_context := 'ITEM';                        --
            line_attr_rec.pricing_attribute := 'PRICING_ATTRIBUTE2';
            line_attr_rec.pricing_attr_value_from := l_item_category; -- Category ID
            line_attr_rec.validated_flag := 'N';
            p_line_attr_tbl (l_line_attr_tbl_cntr) := line_attr_rec;
         END IF;

         qual_rec.line_index := l_rec_cntr;
         qual_rec.qualifier_context := 'ORDER';
         qual_rec.qualifier_attribute := 'QUALIFIER_ATTRIBUTE18';
         qual_rec.qualifier_attr_value_from := p_organization_id; -- SHIP_FROM_ORG_ID;
         qual_rec.comparison_operator_code := '=';
         qual_rec.validated_flag := 'Y';
         p_qual_tbl (l_qual_tbl_cntr) := qual_rec;

         IF p_cust_account_id IS NOT NULL
         THEN
            l_qual_tbl_cntr := l_qual_tbl_cntr + 1;
            qual_rec.line_index := l_rec_cntr;
            qual_rec.qualifier_context := 'CUSTOMER';
            qual_rec.qualifier_attribute := 'QUALIFIER_ATTRIBUTE32';
            qual_rec.qualifier_attr_value_from := p_cust_account_id; -- CUSTOMER ID;
            qual_rec.comparison_operator_code := '=';
            qual_rec.validated_flag := 'Y';
            p_qual_tbl (l_qual_tbl_cntr) := qual_rec;
         END IF;

         IF p_cust_acct_use_id IS NOT NULL
         THEN
            l_qual_tbl_cntr := l_qual_tbl_cntr + 1;
            qual_rec.line_index := l_rec_cntr;
            qual_rec.qualifier_context := 'CUSTOMER';
            qual_rec.qualifier_attribute := 'QUALIFIER_ATTRIBUTE11';
            qual_rec.qualifier_attr_value_from := p_cust_acct_use_id; -- Ship To Site Use ID;
            qual_rec.comparison_operator_code := '=';
            qual_rec.validated_flag := 'Y';
            p_qual_tbl (l_qual_tbl_cntr) := qual_rec;
         END IF;

         l_qual_tbl_cntr := l_qual_tbl_cntr + 1;

         qual_rec.line_index := l_rec_cntr;
         qual_rec.qualifier_context := 'ITEM_NUMBER';
         qual_rec.qualifier_attribute := 'QUALIFIER_ATTRIBUTE31';
         qual_rec.qualifier_attr_value_from := TO_CHAR (rec.inventory_item_id); -- ItemId
         qual_rec.comparison_operator_code := 'NOT=';
         qual_rec.validated_flag := 'Y';
         p_qual_tbl (l_qual_tbl_cntr) := qual_rec;

         IF l_item_category IS NOT NULL
         THEN
            l_qual_tbl_cntr := l_qual_tbl_cntr + 1;
            qual_rec.line_index := l_rec_cntr;
            qual_rec.qualifier_context := 'ITEM_CAT';
            qual_rec.qualifier_attribute := 'QUALIFIER_ATTRIBUTE31';
            qual_rec.qualifier_attr_value_from := TO_CHAR (l_item_category); -- CatergoryId
            qual_rec.comparison_operator_code := 'NOT=';
            qual_rec.validated_flag := 'Y';
            p_qual_tbl (l_qual_tbl_cntr) := qual_rec;
         END IF;

         UPDATE xxwc.xxwc_md_search_product_gtt_tbl
            SET sequence = l_rec_cntr
          WHERE inventory_item_id = rec.inventory_item_id;
      END LOOP;

      qp_preq_pub.price_request (p_line_tbl,
                                 p_qual_tbl,
                                 p_line_attr_tbl,
                                 p_line_detail_tbl,
                                 p_line_detail_qual_tbl,
                                 p_line_detail_attr_tbl,
                                 p_related_lines_tbl,
                                 p_control_rec,
                                 x_line_tbl,
                                 x_line_qual,
                                 x_line_attr_tbl,
                                 x_line_detail_tbl,
                                 x_line_detail_qual_tbl,
                                 x_line_detail_attr_tbl,
                                 x_related_lines_tbl,
                                 x_return_status,
                                 x_return_status_text);

      i := x_line_detail_tbl.FIRST;

      IF i IS NOT NULL
      THEN
         LOOP
            IF x_line_detail_tbl (i).automatic_flag = 'Y'
            THEN
               l_modifier_name := NULL;
               l_list_header_id := NULL;
               l_list_line_id := NULL;
               l_incomp_code := NULL;
               l_modifier_type := NULL;
               l_list_line_type_code := NULL;
               l_line_modifier_type := NULL;

               BEGIN
                  SELECT name, attribute10
                    INTO l_modifier_name, l_modifier_type
                    FROM qp_list_headers_vl
                   WHERE list_header_id =
                            x_line_detail_tbl (i).list_header_id;

                  l_list_header_id := x_line_detail_tbl (i).list_header_id;
                  l_list_line_id := x_line_detail_tbl (i).list_line_id;

                  IF l_list_line_id IS NOT NULL
                  THEN
                     SELECT incompatibility_grp_code,
                            list_line_type_code,
                            attribute5
                       INTO l_incomp_code,
                            l_list_line_type_code,
                            l_line_modifier_type
                       FROM qp_list_lines
                      WHERE list_line_id = l_list_line_id;

                     IF l_modifier_type = 'Contract Pricing'
                     THEN
                        l_modifier_type :=
                           NVL (l_line_modifier_type, l_modifier_type);
                     END IF;

                     IF l_modifier_type IS NOT NULL
                     THEN
                        BEGIN
                           SELECT description
                             INTO l_modifier_Type
                             FROM fnd_flex_values_vl
                            WHERE     flex_value_set_id = 1015252
                                  AND flex_value = l_modifier_Type;
                        EXCEPTION
                           WHEN OTHERS
                           THEN
                              NULL;
                        END;
                     END IF;

                     IF l_list_line_type_code = 'PLL'
                     THEN
                        l_incomp_code := 'PLL';
                     END IF;
                  END IF;
               EXCEPTION
                  WHEN OTHERS
                  THEN
                     l_modifier_name := NULL;
                     l_list_header_id := NULL;
                     l_list_line_id := NULL;
                     l_incomp_code := NULL;
                     l_modifier_type := NULL;
                     l_list_line_type_code := NULL;
                     l_line_modifier_type := NULL;
               END;

               -- Getting new selling price

               l_gm_selling_price := NULL;
               j := x_line_tbl.FIRST;

               IF j IS NOT NULL
               THEN
                  LOOP
                     IF x_line_tbl (j).LINE_INDEX =
                           x_line_detail_tbl (i).LINE_INDEX
                     THEN
                        l_gm_selling_price :=
                           x_line_tbl (j).adjusted_unit_price;
                     END IF;

                     EXIT WHEN l_gm_selling_price IS NOT NULL;
                     j := x_line_tbl.NEXT (j);
                  END LOOP;
               END IF;

               IF l_gm_selling_price = 0
               THEN
                  l_gm_selling_price := NULL;
               END IF;

               UPDATE xxwc.xxwc_md_search_product_gtt_tbl
                  SET selling_price = l_gm_selling_price,
                      modifier = l_modifier_name,
                      modifier_type = l_modifier_type
                WHERE sequence = x_line_detail_tbl (i).line_index;
            END IF;           -- IF x_line_detail_tbl (i).automatic_flag = 'Y'

            EXIT WHEN i = x_line_detail_tbl.LAST;
            i := x_line_detail_tbl.NEXT (i);
         END LOOP;
      END IF;


      BEGIN
         EXECUTE IMMEDIATE
               'DROP TABLE XXWC.XXWC_ORG_ITEM_PRICE_'
            || P_ORGANIZATION_ID
            || '##';

         FND_FILE.PUT_LINE (
            FND_FILE.LOG,
               'XXWC.XXWC_ORG_ITEM_PRICE_'
            || P_ORGANIZATION_ID
            || '##'
            || ' Table Dropped successfully');
      EXCEPTION
         WHEN OTHERS
         THEN
            FND_FILE.PUT_LINE (
               FND_FILE.LOG,
                  'XXWC.XXWC_ORG_ITEM_PRICE_'
               || P_ORGANIZATION_ID
               || '##'
               || '  - Failed Table Drop command');
      END;

      BEGIN
         lvc_table_create :=
               'CREATE TABLE XXWC.XXWC_ORG_ITEM_PRICE_'
            || P_ORGANIZATION_ID
            || '## AS SELECT GTT.ORGANIZATION_CODE,GTT.ITEM_NUMBER,GTT.ON_HAND_QTY,XMPL.SELLING_PRICE,GTT.AVAILABILITY'
            || ' FROM XXWC.XXWC_MD_SEARCH_PRODUCT_GTT_TBL XMPL, XXWC.XXWC_GOOGLE_LOC_INVENTORY_TBL GTT WHERE  '
            || ' XMPL.INVENTORY_ITEM_ID = GTT.INVENTORY_ITEM_ID AND GTT.ORGANIZATION_ID= '
            || P_ORGANIZATION_ID;

         FND_FILE.PUT_LINE (FND_FILE.LOG,
                            ' Table Script ' || lvc_table_create);

         EXECUTE IMMEDIATE lvc_table_create;

         FND_FILE.PUT_LINE (
            FND_FILE.LOG,
               'XXWC.XXWC_ORG_ITEM_PRICE_'
            || P_ORGANIZATION_ID
            || '##'
            || ' Table successfully created');
      EXCEPTION
         WHEN OTHERS
         THEN
            FND_FILE.PUT_LINE (
               FND_FILE.LOG,
                  'XXWC.XXWC_ORG_ITEM_PRICE_'
               || P_ORGANIZATION_ID
               || '##'
               || '  - table creation failed');
      END;
   END;
   --<START> Ver#1.1
   /*****************************************************************************
  Procedure : PLA_FEED

  PURPOSE:   Created this procedure to generate  PLA File (master level data)

  REVISIONS:
  Ver        Date        Author            Description
  ---------  ----------  ---------------   -------------------------
  1.0        05/26/2017  Pattabhi Avula    Initial Version(TMS#20161017-00224)

******************************************************************************/
  PROCEDURE PLA_FEED (p_onhnd_qty IN NUMBER, p_web_item IN VARCHAR2)
   IS
  -- Local Variables declare
  
   lvc_procedure          VARCHAR2 (100) := 'PLA_FEED';
   l_err_callpoint        VARCHAR2 (150) := 'START';
   l_message              VARCHAR2 (2000);
   l_web_link             VARCHAR2 (500) := FND_PROFILE.VALUE('XXWC_PLA_WEB_LINK');
   l_mobile_link          VARCHAR2 (500) := FND_PROFILE.VALUE('XXWC_PLA_MOBILE_LINK');
   l_web_image_link       VARCHAR2 (500) := FND_PROFILE.VALUE('XXWC_INV_WC_WEBSITE_IMAGE_URL');
                          
   l_pla_file_name        VARCHAR2 (80);
   l_pla_outfile          UTL_FILE.file_type;
   l_outbound_directory   dba_directories.directory_name%TYPE
                                        := 'XXWC_PLA_FEED_DATA_DIR'; 
   l_pla_dir           VARCHAR2(100) := NULL;				
   l_db_name           VARCHAR2 (20) := NULL;
   l_path              VARCHAR2 (240) := NULL;
   l_sql               VARCHAR2 (240) := NULL;

    CURSOR cur_pla 
	 IS
	  SELECT msi.segment1 item_id
	        ,xwc.xxwc_w_s_d_attr Title
            ,(SELECT tl_ext_attr2 tl_ext_attr2
                FROM apps.ego_mtl_sy_items_ext_tl
               WHERE inventory_item_id = msi.inventory_item_id
                 AND attr_group_id = 479
                 AND organization_id = 222) description
            ,apps.xxwc_inv_google_ads_pkg.google_prod_category (
                        msi.inventory_item_id,
                        msi.organization_id) google_prod_category
            ,(SELECT INITCAP(LOWER(SUBSTR(EGO_ITEM_PUB.Get_Category_Hierarchy_Names(category_set_id, category_id),19)))
                FROM mtl_item_categories
               WHERE inventory_item_id=msi.inventory_item_id
                 AND organization_id=msi.organization_id
                 AND category_set_id=1100000085) product_type
	        ,l_web_link||REPLACE(LOWER(REPLACE(regexp_replace(REPLACE(REPLACE(REPLACE(REPLACE(xwc.xxwc_w_s_d_attr||'-'||egat.attribute_char_value
             ,'.'),'+',''),'-','='),' ','-'), '( *[[:punct:]])', '-'),'--','-')),'---','--') link
            ,l_mobile_link||REPLACE(LOWER(REPLACE(regexp_replace(REPLACE(REPLACE(REPLACE(REPLACE(xwc.xxwc_w_s_d_attr||'-'||egat.attribute_char_value
             ,'.'),'+',''),'-','='),' ','-'), '( *[[:punct:]])', '-'),'--','-')),'---','--') mobile_link
	        ,l_web_image_link|| 'f_' || msi.segment1 || '.jpg' image_link
            ,'New' condition
            ,'In Stock' availability 
            ,(SELECT ll.operand
                FROM qp_secu_list_headers_v lh ,
                     xxwc.xxwc_qp_list_lines_mv ll,
                     mtl_system_items ms
               WHERE lh.list_type_code = 'PRL'
                 AND lh.name =  ('MARKET NATIONAL')
                 AND lh.list_header_id = ll.list_header_id
                 AND SYSDATE BETWEEN NVL (ll.start_date_active , SYSDATE - 1) 
				 AND NVL (ll.end_date_active , SYSDATE + 1)
                 AND ll.product_id     = msi.inventory_item_id
                 AND ROWNUM            = 1) price
	        ,attb.attribute_char_value brand	
	        ,egat.attribute_char_value mpn
            ,(SELECT cross_reference
               FROM mtl_cross_references
              WHERE cross_reference_type='UPC'
                AND inventory_item_id=msi.inventory_item_id
                AND ROWNUM < 2) gtin
		    ,CASE WHEN (attb.attribute_char_value IS NULL 
			  OR egat.attribute_char_value IS NULL)
		     THEN 'FALSE'
             ELSE 'TRUE'
             END AS identifier_exists
            ,msi.unit_weight shipping_weight
            ,CASE WHEN msi.unit_weight <=5.00 
			      THEN 'UPS Ground Shipping:'||ROUND(5.00, 2)||' USD'
                  WHEN (msi.unit_weight >5.00 AND msi.unit_weight<=100) 
				  THEN 'UPS Ground Shipping:'||ROUND((5+(msi.unit_weight-5)*0.5), 2)||' USD'
                  WHEN msi.unit_weight>100 
				  THEN 'UPS Ground Shipping:'||ROUND((52.5 + (msi.unit_weight-100)*.15), 2)||' USD'
             END AS shipping_service_price
            , '1ct' unit_pricing_measure
            ,'No' adult       
       FROM apps.mtl_system_items_b msi,
           (SELECT inventory_item_id, category_concat_segs
                        FROM mtl_item_categories_V a
                       WHERE     category_set_id = 1100000063
                             AND organization_id = 222
                             AND NOT EXISTS
                                        (SELECT 1
                                           FROM apps.mtl_system_items_b msi
                                          WHERE     msi.organization_id = 222
                                                AND msi.inventory_item_status_code =
                                                       'Inactive'
                                                AND msi.inventory_item_id =
                                                       a.inventory_item_id)) web,
	       (SELECT SUM(transaction_quantity) qty,
	               inventory_item_id
              FROM APPS.mtl_onhand_quantities
              GROUP BY inventory_item_id)mohq,
            xxwc_white_c_m_agv	xwc,
            xxwc_ego_all_attr_v	egat,
            xxwc_ego_all_attr_base_v attb			
      WHERE 1 = 1
        AND msi.organization_id = 222
        AND msi.inventory_item_status_code <> 'Inactive'
        AND msi.inventory_item_id = web.inventory_item_id
	    AND web.category_concat_segs =
                           DECODE (p_web_item,
                                   'Y', 'Y',
                                   web.category_concat_segs) 
	    AND xwc.inventory_item_id=msi.inventory_item_id
		AND egat.inventory_item_id=msi.inventory_item_id
		AND egat.organization_id=msi.organization_id
		AND egat.attribute_name = 'XXWC_M_P_N_ATTR'
		AND attb.inventory_item_id = msi.inventory_item_id
		AND attb.organization_id=msi.organization_id
        AND attb.attribute_name = 'XXWC_BRAND_ATTR'
	    AND mohq.inventory_item_id = msi.inventory_item_id
        AND mohq.qty >=p_onhnd_qty;
	   	   
BEGIN
      l_err_callpoint := 'Procedure Start';
	BEGIN
   SELECT LOWER (name) INTO l_db_name FROM v$database;
   
   l_path    := '/xx_iface/'||l_db_name ||'/outbound/pdh/pla_feed';

   l_sql :=
         'CREATE OR REPLACE DIRECTORY XXWC_PLA_FEED_DATA_DIR as'
      || ' '
      || ''''
      || l_path
      || '''';  
   FND_FILE.PUT_LINE (FND_FILE.LOG,'Checking the pla directory exists or not');
   
	BEGIN
	SELECT directory_name
	  INTO l_pla_dir
	  FROM dba_directories 
	 WHERE directory_name=l_outbound_directory;
	EXCEPTION
      WHEN NO_DATA_FOUND THEN
	  l_pla_dir:=NULL;
	  WHEN others THEN
	  FND_FILE.PUT_LINE (FND_FILE.LOG,'Error while checking the pla directory');
	  END;
	  
	IF l_pla_dir IS NULL
	 THEN  
    	FND_FILE.PUT_LINE (FND_FILE.LOG,'Creating the pla directory');
	    EXECUTE IMMEDIATE l_sql;
	FND_FILE.PUT_LINE (FND_FILE.LOG,'Created the pla directory');
    END IF;
	 
  EXCEPTION
    WHEN OTHERS
        THEN
    FND_FILE.PUT_LINE (FND_FILE.LOG,'Error in Directory creation and error details are: '||SQLERRM);
  END;   

      l_pla_file_name :=
         'Prod_PLA_Feed_' || TO_CHAR (SYSDATE, 'MMDDYYYYHH24MISS') || '.txt';

      FND_FILE.PUT_LINE (FND_FILE.LOG, ' Fie Name ' || l_pla_file_name);

      l_err_callpoint := 'Opening Outfile';
	  
	  l_pla_outfile :=
         UTL_FILE.fopen (l_outbound_directory,
                         l_pla_file_name,
                         'W',
                         32766);
	        UTL_FILE.put_line (
         l_pla_outfile,
            'itemid'
         || CHR (9)
         || 'title'
         || CHR (9)
         || 'description'
         || CHR (9)
		 || 'google product category'
         || CHR (9)
		 || 'product type'
         || CHR (9)
		 || 'link'
         || CHR (9)
		 || 'mobile link'
         || CHR (9)
         || 'image link'
         || CHR (9)
         || 'condition'
         || CHR (9)
         || 'availability'
         || CHR (9)
         || 'price'
         || CHR (9)
         || 'brand'
         || CHR (9)
         || 'mpn'
         || CHR (9)
         || 'gtin'
         || CHR (9)
         || 'identifier exists'
         || CHR (9)
         || 'shipping weight'
         || CHR (9)
         || 'shipping service price'
		 || CHR (9)
         || 'unit pricing measure'
		 || CHR (9)
         || 'adult');

      l_err_callpoint := 'Before Loop Starting';


      FOR rec_item IN cur_pla
      LOOP
	  
        l_err_callpoint := ' Inventory_item_id ' || rec_item.item_id;
 -- pending from here
         BEGIN
            UTL_FILE.put_line (
               l_pla_outfile,
                  rec_item.item_id
               || CHR (9)
               || rec_item.title
			   || CHR (9)
               || rec_item.description
               || CHR (9)
               || rec_item.google_prod_category
			   || CHR (9)
               || rec_item.product_type
			   || CHR (9)
               || rec_item.link
			   || CHR (9)
               || rec_item.mobile_link
               || CHR (9)
               || rec_item.image_link
               || CHR (9)
               || rec_item.condition
               || CHR (9)
               || rec_item.availability
               || CHR (9)
               || rec_item.price
               || CHR (9)
               || rec_item.brand
               || CHR (9)
               || rec_item.mpn
               || CHR (9)
               || rec_item.gtin
               || CHR (9)
               || rec_item.identifier_exists
               || CHR (9)
               || rec_item.shipping_weight
			   || CHR (9)
               || rec_item.shipping_service_price
			   || CHR (9)
               || rec_item.unit_pricing_measure
               || CHR (9)
               || rec_item.adult);
         EXCEPTION
            WHEN UTL_FILE.write_error
            THEN
               fnd_file.put_line (fnd_file.LOG,
                                  'write_error ' || rec_item.item_id);
               g_retcode := 1;
            WHEN OTHERS
            THEN
               fnd_file.put_line (
                  fnd_file.LOG,
                     ' Item '
                  || rec_item.item_id
                  || ' Error mess '
                  || SUBSTR (SQLERRM, 1, 250));
               g_retcode := 1;
         END;
      END LOOP;

      l_err_callpoint := ' File Close';

      UTL_FILE.fclose (l_pla_outfile);
   EXCEPTION
      WHEN OTHERS
      THEN
         g_retcode := 1;

         IF UTL_FILE.IS_OPEN (l_pla_outfile)
         THEN
            UTL_FILE.FCLOSE (l_pla_outfile);
         END IF;

         l_message := SUBSTR (SQLERRM, 1, 250);
         xxcus_error_pkg.xxcus_error_main_api (
            p_called_from         => g_err_callfrom || '.' || lvc_procedure,
            p_calling             => l_err_callpoint,
            p_ora_error_msg       => l_message,
            p_error_desc          => l_message,
            p_distribution_list   => g_distro_list,
            p_module              => 'XXWC');
   END PLA_FEED;
   --<END> Ver#1.1
END XXWC_INV_GOOGLE_ADS_PKG;
/