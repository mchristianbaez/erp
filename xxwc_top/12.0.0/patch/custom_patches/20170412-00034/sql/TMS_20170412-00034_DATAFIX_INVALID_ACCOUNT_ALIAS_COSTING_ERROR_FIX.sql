/******************************************************************************
  $Header TMS_20170412-00034_DATAFIX_INVALID_ACCOUNT_ALIAS_COSTING_ERROR_FIX.sql $
  Module Name:Data Fix script for 20170412-00034

  PURPOSE: Data fix script for 20170412-00034 Invalid/No Account alias for 
           transaction causing costing error.

  REVISIONS:
  Ver        Date         Author                Description
  ---------  -----------  ------------------    -------------------------------
  1.0        12-APR-2017  Pattabhi Avula        TMS#20170412-00034

*******************************************************************************/
SET SERVEROUTPUT ON SIZE 1000000


BEGIN
   DBMS_OUTPUT.put_line ('Before Update');

   UPDATE MTL_MATERIAL_TRANSACTIONS
      SET TRANSACTION_SOURCE_ID = 7856,
          costed_flag = 'N',
          transaction_group_id = NULL,
          ERROR_CODE = NULL,
          error_explanation = NULL
    WHERE transaction_id = 555596956 AND organization_id = 291;


   DBMS_OUTPUT.put_line ('Records update-' || SQL%ROWCOUNT);
   COMMIT;
EXCEPTION
   WHEN OTHERS
   THEN
      DBMS_OUTPUT.put_line ('Unable to update record ' || SQLERRM);
END;
/