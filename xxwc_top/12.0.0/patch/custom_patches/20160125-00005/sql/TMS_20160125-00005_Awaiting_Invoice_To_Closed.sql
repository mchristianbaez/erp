/*
 TMS: 20160125-00005    
 Date: 01/14/2016
 Notes: @SF Data fix script for I313591
*/

SET SERVEROUTPUT ON SIZE 100000;
SET VERIFY OFF;


update apps.oe_order_lines_all 
set open_flag='N',
flow_status_code='CLOSED'
where header_id= 35678573
and line_id=58570762;
		  
--1 row expected to be updated

commit;

/