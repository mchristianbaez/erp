/**************************************************************************
File Name:XXWC_AR_CUSTOMERS_MV.sql
TYPE:     Materialized View
Description: Materialized View for Customer Information for EComm and EDW Extracts.

VERSION DATE          AUTHOR(S)       DESCRIPTION
------- -----------   --------------- ---------------------------------
1.0     11-FEB-2015   Gopi Damuluri   TMS# 20141212-00194
2.0     03/11/2015    Raghavendra S   TMS# 20150303-00075 - FIN - Addition of Code Snippet for Line Breaks in All EDW Extract Programs
                                      and Views for all free entry text fields.
**************************************************************************/
DROP MATERIALIZED VIEW APPS.XXWC_AR_CUSTOMERS_MV;

CREATE MATERIALIZED VIEW APPS.XXWC_AR_CUSTOMERS_MV 
(BUSINESS_UNIT,SOURCE_SYSTEM,CUST_UNIQUE_IDENTIFIER,CUST_ACCOUNT_CODE,CUST_ACCOUNT_NAME,CUST_ACOUNT_TYPE,CUST_ACCOUNT_CLASS,CUST_ROLLUP_LEVEL1_LABEL,CUST_ROLLUP_LEVEL1_CODE,CUST_ROLLUP_LEVEL1_DESC,CUST_ROLLUP_LEVEL2_LABEL,CUST_ROLLUP_LEVEL2_CODE,CUST_ROLLUP_LEVEL2_DESC,CUST_ROLLUP_LEVEL3_LABEL,CUST_ROLLUP_LEVEL3_CODE,CUST_ROLLUP_LEVEL3_VALUE,REPORTING_GROUP_IDENTIFIER,HOMELOCATION,IN_FREIGHT_EXEMPT_FLAG,OUT_FREIGHT_EXEMPT_FLAG,PASSTHROUGH_DISC_ELIGIBLE_FLAG,REBATEPERCENTAGE,PAYMENT_TERMS_DESC,PAYMENT_TERM_DISC_PERC,CREDIT_LIMIT_AMT,TAXID,CREATION_DATE,LAST_UPDATE_DATE,FIRSTSALEDATE,LASTSALEDATE,CUST_ACCOUNT_STATUS,PARTY_SITE_NUMBER,PARTY_SITE_ID, PARTY_ID, SITE_USE_ID,SITE_USE_CODE,SITE_NAME,ADDRESS1,ADDRESS2,ADDRESS3,ADDRESS4,CITY,STATE_PROVINCE,POSTAL_CODE,COUNTRY,MAIN_PHONE_NUMBER,MAIN_FAX_NUMBER,MAIN_EMAIL,INSIDE_SALESREP_ID,OUTSIDE_SALESREP_ID,DEFAULT_BILL_TO_CUSTOMER,DEFAULT_BILL_TO_PARTY_SITE_NUM,DEFAULT_BILL_TO_SITE_NAME,BILL_TO_ADDRESS1,BILL_TO_ADDRESS2,BILL_TO_ADDRESS3,BILL_TO_ADDRESS4,BILL_TO_CITY,BILL_TO_STATE_PROVINCE,BILL_TO_POSTAL_CODE,BILL_TO_COUNTRY,DEF_BILL_TO_TELEPHONE,DEF_BILL_TO_FAX,CUSTOM_CUST_ATTR1_LABEL,CUSTOM_CUST_ATTR1_VALUE,CUSTOM_CUST_ATTR2_LABEL,CUSTOM_CUST_ATTR2_VALUE,CUSTOM_CUST_ATTR3_LABEL,CUSTOM_CUST_ATTR3_VALUE,CUSTOM_CUST_ATTR4_LABEL,CUSTOM_CUST_ATTR4_VALUE,CUSTOM_CUST_ATTR5_LABEL,CUSTOM_CUST_ATTR5_VALUE,DATE_DELETED,OPERATING_UNIT_ID,OPERATING_UNIT,CUSTOMER_NAME,SITE_USE_LOCATION_NAME,PRISM_CUSTOMER_NUMBER,PRISM_SITE_NUMBER,HCSUA_PRIMARY_FLAG,HCASA_SITE_STATUS,HP_PARTY_NUMBER,HP_PARTY_STATUS,HP_PARTY_ADDRESS1,HP_PARTY_ADDRESS2,HP_PARTY_ADDRESS3,HP_PARTY_ADDRESS4,HP_PARTY_CITY,HP_PARTY_COUNTY_PROVINCE,HP_PARTY_STATE,HP_PARTY_COUNTRY,HP_PARTY_POSTAL_CODE,HCA_LAST_UPDATE_DATE,HCASA_LAST_UPDATE_DATE,HCSUA_LAST_UPDATE_DATE,HP_LAST_UPDATE_DATE,HPS_LAST_UPDATE_DATE,HL_LAST_UPDATE_DATE,HCASA_CUST_ACCT_SITE_ID,SITE_USE_TYPE,ACCOUNT_ESTABLISHED_DATE,TAX_EXEMPT_CERT_NUM,TAX_EXEMPT_CERT_EXP_DATE,TAX_LOCALITY,PRICING_PROFIILE,NAICS_CODE,DODGE_NUMBER
, CUST_ACCOUNT_ID)
TABLESPACE APPS_TS_TX_DATA
PCTUSED    0
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          128K
            NEXT             128K
            MAXSIZE          UNLIMITED
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
            FLASH_CACHE      DEFAULT
            CELL_FLASH_CACHE DEFAULT
           )
NOCACHE
LOGGING
NOCOMPRESS
NOPARALLEL
BUILD IMMEDIATE
REFRESH COMPLETE ON DEMAND
WITH PRIMARY KEY
AS 
SELECT 'WHITECAP' business_unit
      ,'Oracle EBS' source_system
      -- 05/29/2012 CG changed to be unique per site
      --, (hca.account_number||to_char(hca.creation_date,'YYYYMMDD')) Cust_Unique_Identifier
      -- , hca.account_number Cust_Account_Code
      -- , hp.party_name Cust_Account_name
      -- 06/06/2012 CG Changed to make unique based onthe site use
      -- , (hps.party_site_number||to_char(hps.creation_date,'YYYYMMDD')) Cust_Unique_Identifier
      , (hps.party_site_number || hcsua.site_use_id) cust_unique_identifier
      ,                                          -- 08/16/12 CG: Updated per email to customer number and name
       -- NVL (hcasa.attribute17, hps.party_site_number) cust_account_code,
       -- hps.party_site_name cust_account_name,
       hca.account_number cust_account_code
      --,NVL (hca.account_name, hp.party_name) cust_account_name -- Commented for V2.0
      --,cust_type.meaning cust_acount_type         -- Commented for V2.0                                          -- customer_type_tl
      --,cust_class.meaning cust_account_class       -- Commented for V2.0 
      ,regexp_replace(NVL (hca.account_name, hp.party_name),  '[[:cntrl:]]', ' ') cust_account_name -- Added for V2.0
      ,regexp_replace(cust_type.meaning,  '[[:cntrl:]]', ' ') cust_acount_type         -- Added for V2.0                                          -- customer_type_tl
      ,regexp_replace(cust_class.meaning,  '[[:cntrl:]]', ' ') cust_account_class       -- Added for V2.0                                        -- customer_class_tl
      ,'NATIONAL ACCOUNT' cust_rollup_level1_label
       /*(CASE
            WHEN hca.customer_class_code = 'NATIONAL' THEN NVL (hca.attribute6, hca.account_number)
            ELSE ' '
         END) cust_rollup_level1_code */ -- commented for V2.0 
      , regexp_replace((CASE
            WHEN hca.customer_class_code = 'NATIONAL' THEN NVL (hca.attribute6, hca.account_number)
            ELSE ' '
         END),  '[[:cntrl:]]', ' ')
          cust_rollup_level1_code -- Added for V2.0
      /*, (CASE
            WHEN hca.customer_class_code = 'NATIONAL' THEN NVL (hca.account_name, hp.party_name)
            ELSE ' '
         END)  cust_rollup_level1_desc*/ -- commented for V2.0 
      , regexp_replace((CASE
            WHEN hca.customer_class_code = 'NATIONAL' THEN NVL (hca.account_name, hp.party_name)
            ELSE ' '
         END),  '[[:cntrl:]]', ' ') cust_rollup_level1_desc -- Added for V2.0
      ,'MASTER CUSTOMER' cust_rollup_level2_label
      --NVL (hca.attribute6, hca.account_number  cust_rollup_level2_code -- commented for V2.0 
      --NVL (hca.account_name, hp.party_name)  cust_rollup_level2_desc -- commented for V2.0 
      ,regexp_replace(NVL (hca.attribute6, hca.account_number),  '[[:cntrl:]]', ' ') cust_rollup_level2_code -- Added for V 2.0
      ,regexp_replace(NVL (hca.account_name, hp.party_name),  '[[:cntrl:]]', ' ')  cust_rollup_level2_desc -- Added for V 2.0
      ,' ' cust_rollup_level3_label
      ,' ' cust_rollup_level3_code
      ,' ' cust_rollup_level3_value
      -- 05/29/2012 CG Changed
      --, hp.orig_system_reference Reporting_Group_Identifier
      ,hca.account_number reporting_group_identifier
      ,' ' homelocation
      ,'N' in_freight_exempt_flag
      , (DECODE (hca.freight_term, 'EXEMPT', 'Y', 'N')) out_freight_exempt_flag
      ,'N' passthrough_disc_eligible_flag
      ,' ' rebatepercentage
      --,rt.payment_term_description payment_terms_desc -- Commented for V2.0
      ,regexp_replace(rt.payment_term_description,  '[[:cntrl:]]', ' ') payment_terms_desc -- Added for V2.0
      ,rt.perc_discount payment_term_disc_perc
      ,hcpa.overall_credit_limit credit_limit_amt
      ,' ' taxid
      ,hca.creation_date
      ,hca.last_update_date
      ,xxwc_mv_routines_pkg.get_first_sale_date (hca.cust_account_id) firstsaledate
      ,xxwc_mv_routines_pkg.get_last_sale_date (hca.cust_account_id) lastsaledate
      , (DECODE (hca.status,  'A', 'Active',  'I', 'Inactive',  hca.status)) cust_account_status --CustAcctStatus_tl
      -- Sites
      ,hps.party_site_number
      ,hps.party_site_id                                                                -- TMS# 20140211-00049
      ,hps.party_id                                                                     -- TMS# 20140529-00334
      ,hcsua.site_use_id
      ,hcsua.site_use_code
      --hcsua.locatio site_name -- commented for V2.0
      ,regexp_replace(hcsua.location,  '[[:cntrl:]]', ' ') site_name -- Added for V2.0
      
--      ,hl.address1 -- TMS# 20140327-00144
--      ,hl.address2 -- TMS# 20140327-00144
--      ,hl.address3 -- TMS# 20140327-00144
--      ,hl.address4 -- TMS# 20140327-00144
, regexp_replace(hl.address1,  '[[:cntrl:]]', ' ')  --  TMS# 20140327-00144
, regexp_replace(hl.address2,  '[[:cntrl:]]', ' ')  --  TMS# 20140327-00144
, regexp_replace(hl.address3,  '[[:cntrl:]]', ' ')  --  TMS# 20140327-00144
, regexp_replace(hl.address4,  '[[:cntrl:]]', ' ')  --  TMS# 20140327-00144
      ,hl.city
      , (CASE WHEN LENGTH (NVL (hl.state, hl.province)) > 2 THEN NULL ELSE NVL (hl.state, hl.province) END)
          state_province
      ,hl.postal_code
      ,hl.country
      ,SUBSTR (xxwc_mv_routines_pkg.get_phone_fax_number ('PHONE'
                                                         ,hca.party_id
                                                         ,NULL                         --, hcasa.party_site_id
                                                         ,'GEN')
              ,1
              ,80)
          main_phone_number
      ,SUBSTR (xxwc_mv_routines_pkg.get_phone_fax_number ('PHONE'
                                                         ,hca.party_id
                                                         ,NULL                         --, hcasa.party_site_id
                                                         ,'FAX')
              ,1
              ,80)
          main_fax_number 
      --,' ' main_email                 -- Commented by Mahesh 08-May-2014, TMS#20140501-00259
      ,hp.email_address main_email      -- Added by Mahesh 08-May-2014, TMS#20140501-00259
      ,jrs.salesrep_number inside_salesrep_id
      -- 10/08/2012 CG: Changed outside salesrep to be mapped identical to inside sales rep
      -- change based on email from Rich on 10/04/2012
      -- , ' ' outside_salesrep_id
      ,jrs.salesrep_number outside_salesrep_id
      -- 10/08/2012 CG
      -- Default Bill To where Applicable
      --hp_def_bill.party_name  default_bill_to_customer -- Commented for V2.0
      ,regexp_replace(hp_def_bill.party_name,  '[[:cntrl:]]', ' ') default_bill_to_customer -- Added for V2.0
      ,hps_def_bill.party_site_number default_bill_to_party_site_num
      --,hcsua_def_bill.location default_bill_to_site_name  -- Commentd by Raghavendra for TMS # 20141212-00191 
      , regexp_replace(hcsua_def_bill.location,  '[[:cntrl:]]', ' ') default_bill_to_site_name -- added by Raghavendra for TMS # 20141212-00191 
--      ,hl_def_bill.address1 bill_to_address1 --  TMS# 20140327-00144
--      ,hl_def_bill.address2 bill_to_address2 --  TMS# 20140327-00144
--      ,hl_def_bill.address3 bill_to_address3 --  TMS# 20140327-00144
--      ,hl_def_bill.address4 bill_to_address4 --  TMS# 20140327-00144
      , regexp_replace(hl_def_bill.address1,  '[[:cntrl:]]', ' ') bill_to_address1  --  TMS# 20140327-00144
      , regexp_replace(hl_def_bill.address2,  '[[:cntrl:]]', ' ') bill_to_address2  --  TMS# 20140327-00144
      , regexp_replace(hl_def_bill.address3,  '[[:cntrl:]]', ' ') bill_to_address3  --  TMS# 20140327-00144
      , regexp_replace(hl_def_bill.address4,  '[[:cntrl:]]', ' ') bill_to_address4  --  TMS# 20140327-00144    
      ,hl_def_bill.city bill_to_city
      ,                  -- 08/30/2012 CG: Changed to match main address lines mod for international addresses
                                      -- NVL (hl_def_bill.state, hl_def_bill.province) bill_to_state_province,
       (CASE
           WHEN LENGTH (NVL (hl_def_bill.state, hl_def_bill.province)) > 2 THEN NULL
           ELSE NVL (hl_def_bill.state, hl_def_bill.province)
        END)
          bill_to_state_province
      ,hl_def_bill.postal_code bill_to_postal_code
      ,hl_def_bill.country bill_to_country
      ,SUBSTR (xxwc_mv_routines_pkg.get_phone_fax_number ('PHONE'
                                                         ,hp_def_bill.party_id
                                                         ,NULL                  --, hps_def_bill.party_site_id
                                                         ,'GEN')
              ,1
              ,80)
          def_bill_to_telephone
      ,SUBSTR (xxwc_mv_routines_pkg.get_phone_fax_number ('PHONE'
                                                         ,hp_def_bill.party_id
                                                         ,NULL                  --, hps_def_bill.party_site_id
                                                         ,'FAX')
              ,1
              ,80)
          def_bill_to_fax
      -- 05/29/2012 CG Commented per map
      -- , 'DORMANT' Custom_Cust_Attr1_Label
      ,                                               -- 08/16/12 CG: Updated per email with party site number
       'Party Site Id' custom_cust_attr1_label
      ,hps.party_site_number custom_cust_attr1_value
      -- 05/29/2012 CG Commented per map
      -- , 'TARGET' Custom_Cust_Attr2_Label
      -- 10/15/2013 CG: TMS 20131014-00469: Mapped new values
      ,'PREDOMINANT TRADE' custom_cust_attr2_label
      --hca.attribute9  custom_cust_attr2_value -- commented for V2.0
      ,regexp_replace(hca.attribute9,  '[[:cntrl:]]', ' ')  custom_cust_attr2_value -- added for V2.0
      ,' ' custom_cust_attr3_label
      ,' ' custom_cust_attr3_value
      ,' ' custom_cust_attr4_label
      ,' ' custom_cust_attr4_value
      ,' ' custom_cust_attr5_label
      ,' ' custom_cust_attr5_value
      ,' ' date_deleted
      ,hou.organization_id operating_unit_id
      ,hou.name operating_unit
      ,hp.party_name customer_name
      --hcsua.location site_use_location_name -- Commented for V2.0
      --hca.attribute6 prism_customer_number -- Commented for V2.0
      --hcasa.attribute17 prism_site_number -- Commented for V2.0
      ,regexp_replace(hcsua.location,  '[[:cntrl:]]', ' ') site_use_location_name -- added for V2.0
      ,regexp_replace(hca.attribute6,  '[[:cntrl:]]', ' ') prism_customer_number -- added for V2.0
      ,regexp_replace(hcasa.attribute17,  '[[:cntrl:]]', ' ') prism_site_number -- added for V2.0
      -- 06/29/2012 CG: Added per Email request from Andre
      ,hcsua.primary_flag hcsua_primary_flag
      ,hcasa.status hcasa_site_status
      ,hp.party_number hp_party_number
      ,hp.status hp_party_status
      --hp.address1 hp_party_address1 -- Commented for V 2.0
      --hp.address2 hp_party_address2 -- Commented for V 2.0
      --hp.address3 hp_party_address3 -- Commented for V 2.0
      --hp.address4 hp_party_address4 -- Commented for V 2.0
      ,regexp_replace(hp.address1,  '[[:cntrl:]]', ' ') hp_party_address1 -- Added for V 2.0
      ,regexp_replace(hp.address2,  '[[:cntrl:]]', ' ') hp_party_address2 -- Added for V 2.0
      ,regexp_replace(hp.address3,  '[[:cntrl:]]', ' ') hp_party_address3 -- Added for V 2.0
      ,regexp_replace(hp.address4,  '[[:cntrl:]]', ' ') hp_party_address4 -- Added for V 2.0
      ,hp.city hp_party_city
      ,NVL (hp.county, hp.province) hp_party_county_province
      ,hp.state hp_party_state
      ,hp.country hp_party_country
      ,hp.postal_code hp_party_postal_code
      --LAST UPDATE DATES
      ,hca.last_update_date hca_last_update_date
      ,hcasa.last_update_date hcasa_last_update_date
      ,hcsua.last_update_date hcsua_last_update_date
      ,hp.last_update_date hp_last_update_date
      ,hps.last_update_date hps_last_update_date
      ,hl.last_update_date hl_last_update_date
      -- 10/01/12 CG: Added
      ,hcasa.cust_acct_site_id hcasa_cust_acct_site_id
      --hcsua.attribute1  site_use_type -- commented for V 2.0
      ,regexp_replace(hcsua.attribute1,  '[[:cntrl:]]', ' ')  site_use_type -- Added for V 2.0
      -- 01/30/13 CG: TMS 20130122-01580 - Added the Customer Account Date Established
      ,hca.account_established_date
      -- 10/04/2013 CG: TMS 20130815-00678: Added new empty fields and dodge number
      ,' ' tax_exempt_cert_num
      ,' ' tax_exempt_cert_exp_date
      ,' ' tax_locality
      ,' ' pricing_profiile
      ,' ' naics_code
      --hcsua.attribute4  dodge_number -- commented for V 2.0
      ,regexp_replace(hcsua.attribute4,  '[[:cntrl:]]', ' ')  dodge_number -- Added for V 2.0
      , hca.cust_account_id -- TMS# 20141212-00194
  -- 10/04/2013 CG: TMS 20130815-00678
  FROM hz_cust_accounts hca
      ,hz_parties hp
      ,fnd_lookup_values cust_type
      ,fnd_lookup_values cust_class
      ,hz_customer_profiles hcp
      ,hz_cust_profile_amts hcpa
      ,xxwc_ar_payment_terms_vw rt
      -- Sites
      ,hz_cust_acct_sites_all hcasa
      ,hz_party_sites hps
      ,hz_locations hl
      ,hz_cust_site_uses_all hcsua
      -- Salesrep
      ,jtf_rs_salesreps jrs
      --, per_all_people_f papf
      -- Default Bill To (where applicable)
      ,hz_cust_site_uses_all hcsua_def_bill
      ,hz_cust_acct_sites_all hcasa_def_bill
      ,hz_party_sites hps_def_bill
      ,hz_locations hl_def_bill
      ,hz_cust_accounts hca_def_bill
      ,hz_parties hp_def_bill
      -- Operating Unit
      ,hr_operating_units hou
 WHERE     hca.party_id = hp.party_id
       -- Customer Type Lookup
       AND hca.customer_type = cust_type.lookup_code(+)
       AND cust_type.lookup_type(+) = 'CUSTOMER_TYPE'
       AND cust_type.language(+) = 'US'
       AND cust_type.view_application_id(+) = 222                                   -- Receivables Application
       -- Customer Class Lookup
       AND hca.customer_class_code = cust_class.lookup_code(+)
       AND cust_class.lookup_type(+) = 'CUSTOMER CLASS'
       AND cust_class.language(+) = 'US'
       AND cust_class.view_application_id(+) = 222                                  -- Receivables Application
       -- Customer profile amounts
       AND hca.cust_account_id = hcp.cust_account_id(+)
       AND hca.party_id = hcp.party_id(+)
       AND hcp.site_use_id(+) IS NULL
       AND hcp.cust_account_profile_id = hcpa.cust_account_profile_id(+)
       AND hcpa.site_use_id(+) IS NULL
       -- Payment_term
       AND hcp.standard_terms = rt.payterm_id(+)
       AND rt.operating_unit_id = hou.organization_id(+)
       -- Sites (ALL)
       AND hca.cust_account_id = hcasa.cust_account_id
       AND hcasa.party_site_id = hps.party_site_id
       AND hps.location_id = hl.location_id
       AND hcasa.cust_acct_site_id = hcsua.cust_acct_site_id
       -- Salesrep
       AND hcsua.primary_salesrep_id = jrs.salesrep_id(+)
       AND hcsua.org_id = jrs.org_id(+)
       --and        jrs.person_id = papf.person_id(+)
       --and        TRUNC (papf.effective_start_date(+)) <= TRUNC (SYSDATE)
       --and        NVL (TRUNC (papf.effective_end_date(+)), TRUNC (SYSDATE)) >= TRUNC (SYSDATE)
       -- Default Bill to (where applicable)
       AND hcsua.bill_to_site_use_id = hcsua_def_bill.site_use_id(+)
       AND hcsua_def_bill.cust_acct_site_id = hcasa_def_bill.cust_acct_site_id(+)
       AND hcasa_def_bill.party_site_id = hps_def_bill.party_site_id(+)
       AND hps_def_bill.location_id = hl_def_bill.location_id(+)
       AND hcasa_def_bill.cust_account_id = hca_def_bill.cust_account_id(+)
       AND hca_def_bill.party_id = hp_def_bill.party_id(+)
       -- Operating Unit
       AND hcsua.org_id = hou.organization_id;


