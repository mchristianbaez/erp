   /*************************************************************************
   *   $Header XXWC_INV_REPLENISHMENT_DB.sql $
   *   Module Name: XXWC_INV_REPLENISHMENT_DB.sql
   *
   *   PURPOSE:   This package is used for Invoice Pre Processing
   *
   *   REVISIONS:
   *   Ver        Date         Author                Description
   *   ---------  ----------   ---------------       -------------------------
   *   1.0        30-Oct-2015   Manjula Chellappan    Initial Version
   *                                                  TMS# 20150812-00140 Replenishment Metrics Dashboard
   * ***************************************************************************/
DECLARE
l_db_name VARCHAR2(20);

BEGIN

SELECT name 
  INTO l_db_name
  FROM v$database;

	IF l_db_name = 'EBSQA' THEN
	EXECUTE IMMEDIATE 
		'CREATE OR REPLACE DIRECTORY XXWC_INV_REPLENISHMENT_DB as ''/xx_iface/ebsqa/outbound/Supplychain/Inventory_Replenishment_Dashboard''';
		ELSE
	EXECUTE IMMEDIATE 
		'CREATE OR REPLACE DIRECTORY XXWC_INV_REPLENISHMENT_DB as ''/xx_iface/ebizprd/outbound/Supplychain/Inventory_Replenishment_Dashboard''';	

	END IF;	

END;
/
