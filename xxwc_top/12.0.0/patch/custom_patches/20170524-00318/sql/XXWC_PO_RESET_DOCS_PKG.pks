CREATE OR REPLACE PACKAGE APPS.xxwc_po_reset_docs_pkg
AS
   /*************************************************************************
    Copyright (c) 2012 HD Supply Group
    All rights reserved.
   **************************************************************************
     $Header xxwc_po_reset_docs_pkg $
     Module Name: xxwc_po_reset_docs_pkg.pks

     PURPOSE:   

     REVISIONS:
     Ver        Date        Author                     Description
     ---------  ----------  ---------------         -------------------------
     1.0        04/28/2017  Krishna Kumar           Initial Version(TMS#20160823-00016)
   **************************************************************************/

   /*************************************************************************
     Procedure : update_po_reqs_status

     PURPOSE:   This procedure will update the status for no open items for Requisitions
	            and Purchase orders in workflow
     Parameter:

   ************************************************************************/
   PROCEDURE update_po_reqs_status (
      errbuf             OUT      VARCHAR2,
      retcode            OUT      VARCHAR2,
      p_document_type    IN       VARCHAR2,
      p_doc_number       IN       VARCHAR2,
	  p_del_Actn_Hist    IN       VARCHAR2
   );
   
END xxwc_po_reset_docs_pkg;
/