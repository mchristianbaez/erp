/*
TMS: Task ID: 20180525-00043 EBSPRD : WC Period Close Pending Transactions - All Orgs with Process ID - 4274416 completed.
*/
SET SERVEROUTPUT ON SIZE 1000000

BEGIN
  dbms_output.put_line('Before update');

  UPDATE apps.wsh_delivery_details
     SET oe_interfaced_flag = 'Y'
   WHERE delivery_detail_id = 28910249
     AND source_header_id = 72792955
     AND source_line_id = 123788993;


  dbms_output.put_line('Records updated-' || SQL%ROWCOUNT);
  COMMIT;
EXCEPTION
  WHEN OTHERS THEN
    dbms_output.put_line('Unable to update ' || SQLERRM);
END;
/