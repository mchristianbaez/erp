SET SERVEROUTPUT ON SIZE 100000;
SET VERIFY OFF;

DECLARE

 l_shipped_quantity NUMBER := 0;
 l_shipping_quantity  NUMBER := 0;
 l_requested_quantity   NUMBER := 0;
 l_requested_quantity_uom VARCHAR2(50);
 l_src_requested_quantity_uom  VARCHAR2(50);
 l_conversion_rate NUMBER := 0;
 l_con_rate NUMBER := 0;
 l_records_affected NUMBER := 0;
 l_result		VARCHAR2(30);
 l_line_id             NUMBER;
 l_file_name           VARCHAR2(255);
 l_ato_line_id         NUMBER;
  l_INVENTORY_ITEM_ID   NUMBER;



CURSOR lines IS
SELECT oel.line_id, oel.item_type_code, oel.ato_line_id
FROM oe_order_lines_all oel
WHERE oel.booked_flag = 'Y'
AND oel.open_flag = 'Y'
AND oel.shipping_interfaced_flag = 'Y'
AND oel.shippable_flag = 'Y'
AND NVL(oel.shipped_quantity,0) = 0
AND NVL(oel.shipping_quantity,0) = 0
AND oel.flow_status_code = 'AWAITING_SHIPPING'
AND Nvl(oel.invoiced_quantity,0)  = 0
AND Nvl(oel.invoice_interface_status_code,'N') ='N'
AND oel.line_id IN (59972326)
AND EXISTS (SELECT 1 FROM wsh_delivery_details wdd
            WHERE oel.line_id = wdd.source_line_id
            AND wdd.released_status = 'C'
            AND wdd.source_code = 'OE'
            AND wdd.oe_interfaced_flag IN ('Y')
            AND NVL(wdd.shipped_quantity, 0) > 0)
AND NOT EXISTS(SELECT 1
                 FROM wsh_delivery_details wdd
                WHERE wdd.source_line_id = oel.line_id
                  AND wdd.source_code = 'OE'
                  AND wdd.released_status <> 'C'
                 )
AND    EXISTS ( SELECT 1 FROM wf_process_activities p, wf_item_activity_statuses s
                WHERE p.instance_id	= s.process_activity
                AND   item_type		= 'OEOL'
                AND   item_key		= to_char(oel.line_id)
                AND   p.activity_name	IN( 'SHIP_LINE' )
                AND   s.activity_status  in('NOTIFIED', 'ERROR')
                AND   s.end_date IS NULL);

  cursor c_model is
  select p.instance_label, s.activity_status, s.item_key
  from wf_item_activity_statuses s,
       wf_process_activities p
  where s.process_activity = p.instance_id
  and s.item_type='OEOL'
  and p.activity_name = 'WAIT_FOR_CTO'
  and s.activity_status = 'NOTIFIED'
  and s.item_key = to_char(l_ato_line_id);

BEGIN

  Oe_debug_pub.debug_ON;
  Oe_debug_pub.initialize;
  Oe_debug_pub.setdebuglevel(5);
  l_file_name := Oe_debug_pub.set_debug_mode('FILE');
  Dbms_Output.put_line('DEBUG FILE IS located AT :' ||  l_file_name);


    oe_debug_pub.add('Processing line info ....');

 FOR x IN lines LOOP

---------------------------
  Oe_debug_pub.ADD('Updating line ID : ' || x.line_id);


	SELECT Sum(shipped_quantity) ,Sum (requested_quantity)
	INTO   l_shipping_quantity  ,l_requested_quantity
	FROM   wsh_delivery_details
	WHERE  source_line_id = x.line_id
	AND    source_code = 'OE'
	AND    released_status = 'C';

  Oe_debug_pub.ADD('Select Requeted Qty UOM');


	SELECT DISTINCT requested_quantity_uom  , src_requested_quantity_uom ,INVENTORY_ITEM_ID
	INTO   l_requested_quantity_uom   ,l_src_requested_quantity_uom ,l_INVENTORY_ITEM_ID
	FROM   wsh_delivery_details
	WHERE  source_line_id = x.line_id
	AND    source_code = 'OE'
	AND    released_status = 'C';

  IF   l_requested_quantity_uom   <> l_src_requested_quantity_uom THEN

    Oe_debug_pub.ADD('Select  UOM conversion rate');

     l_shipped_quantity:=OE_Order_Misc_Util.Convert_Uom
                          (p_item_id           => l_INVENTORY_ITEM_ID,
                          p_from_uom_code     => l_requested_quantity_uom,
                          p_to_uom_code       => l_src_requested_quantity_uom,
                          p_from_qty          => l_shipping_quantity);

  ELSE
    l_shipped_quantity :=l_shipping_quantity;

  END IF;

  Oe_debug_pub.ADD('Updating shipping details in oe_order_lines_all...');

	UPDATE  oe_order_lines_all SET
                shipped_quantity     = l_shipped_quantity,
                shipping_quantity    = l_shipping_quantity,
                shipping_quantity_uom= l_requested_quantity_uom,
                actual_shipment_date = (SELECT MIN(wts.actual_departure_date)
				        FROM   wsh_delivery_details wdd, wsh_delivery_assignments wda, wsh_delivery_legs wdl, wsh_trip_stops wts
			                WHERE  wdd.delivery_detail_id = wda.delivery_detail_id
				        AND    wda.delivery_id=wdl.delivery_id
				        AND    wdl.pick_up_stop_id = wts.stop_id
				        AND    wdd.source_line_id = x.line_id
				        AND    wdd.source_code = 'OE'
				        AND    wdd.released_status = 'C'),
                last_updated_by      =  -1,
                last_update_date     =  SYSDATE
	WHERE line_id = x.line_id;
 DBMS_OUTPUT.put_line ('Updating oe_order_lines_all done...'||sql%rowcount);
 
---------------------------
	Oe_debug_pub.ADD('Complete.');

     oe_debug_pub.add('Seting Context for line ' || x.line_id);

      Oe_standard_wf.OEOL_Selector(p_itemtype => 'OEOL',
                                   p_itemkey => to_char(x.line_id),
                                   p_actid => 12345,
                                   p_funcmode => 'SET_CTX',
                                   p_result => l_result);

      oe_debug_pub.add('Result: '||l_result );
      oe_debug_pub.add('Calling handleerror for Line...');

      wf_engine.HandleError('OEOL', to_char(x.line_id), 'SHIP_LINE', 'SKIP', 'SHIP_CONFIRM');

      oe_debug_pub.add('Updating delivery detail interface status...');


	UPDATE wsh_delivery_details
	SET    oe_interfaced_flag = 'Y',
	       last_updated_by    =  -1,
               last_update_date   =  SYSDATE
	WHERE  source_line_id = x.line_id
	AND    source_code = 'OE'
	AND    released_status = 'C';

	 DBMS_OUTPUT.put_line ('Updating delivery detail done...'||sql%rowcount);
	 
     oe_debug_pub.add('Updating delivery detail done...');

    l_ato_line_id := x.ato_line_id;
    oe_debug_pub.add('l_ato_line_id '||l_ato_line_id);

 IF ( x.item_type_code = 'CONFIG' AND l_ato_line_id IS NOT NULL) THEN

        oe_debug_pub.add('Processing line l_ato_line_id ....' ||l_ato_line_id);


    FOR j IN c_model LOOP

          	UPDATE  oe_order_lines_all SET
                shipped_quantity     = ordered_quantity,
                last_updated_by      =  -1,
                last_update_date     =  SYSDATE
	        WHERE line_id = l_ato_line_id;

          oe_debug_pub.add('updating line l_ato_line_id ....' ||l_ato_line_id);

          OE_Standard_WF.OEOL_SELECTOR
	   (p_itemtype => 'OEOL'
	   ,p_itemkey => to_char(l_ato_line_id)
	   ,p_actid => 12345
	   ,p_funcmode => 'SET_CTX'
	   ,p_result => l_result
	   );

     oe_debug_pub.add('call WF ENGINE API to COMPLETE WAIT FOR CTO');
     wf_engine.completeactivity('OEOL',to_char(l_ato_line_id),j.instance_label,'COMPLETE');
     oe_debug_pub.add('After call WF ENGINE API to COMPLETE ');

    END loop;


  END IF ;
 END LOOP;

    Oe_debug_pub.debug_OFF;


Exception
  When Others Then
    dbms_output.put_line(' Error in the base script : '||sqlerrm);
    oe_debug_pub.add('### Error in the base script : '||sqlerrm);
    Oe_debug_pub.debug_OFF;
End;
/
COMMIT;