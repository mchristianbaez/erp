--//============================================================================
--//
--// Object Name         :: XXCUS_CONCUR_EMP_EXPORT_350_V
--//
--// Object Type         :: View
--//
--// Object Description  :: This view shows the header info for Supervisor Extract.
--//
--// Version Control
--//============================================================================
--// Vers    Author                    Date                    Description
--//----------------------------------------------------------------------------
--// 1.0     Ashwin Sridhar     08/06/2018    Initial Build - Task ID: 20180227-00179--Adding Contractors to Concur Employee Feed
--// 1.1     Bala Seshadri        06/25/2018    Fix Org Division based on the custom 1 field for FM (TMS 20180625-00305)
--// 1.2     Bala Seshadri        06/27/2018    Fix Travel Class logic  (TMS 20180627-00108)
--//============================================================================
  CREATE OR REPLACE FORCE VIEW "APPS"."XXCUS_CONCUR_EMP_EXPORT_350_V" ("TRX_TYPE", "EMPLOYEE_ID", "NAME_PREFIX", "NAME_SUFFIX", "NICK_NAME", "REDRESS_NUMBER", "GENDER", "DATE_OF_BIRTH", "MANAGER_COMPANY_EMPL_ID", "JOB_TITLE", "WORK_PHONE", "WORK_PHONE_EXTENSION", "WORK_FAX", "HOME_PHONE", "CELL_PHONE", "PAGER_PHONE", "TRAVEL_NAME_REMARK", "TRAVEL_CLASS_NAME", "GDS_PROFILE_NAME", "ORGUNIT_OR_DIVISION", "HOME_STREET_ADDRESS", "HOME_CITY", "HOME_STATE", "HOME_POSTAL_CODE", "HOME_COUNTRY", "WORK_STREET_ADDRESS", "WORK_CITY", "WORK_STATE", "WORK_POSTAL_CODE", "WORK_COUNTRY", "EMAIL_2", "EMAIL_3", "CUSTOM_1", "CUSTOM_2", "CUSTOM_3", "CUSTOM_4", "CUSTOM_5", "CUSTOM_6", "CUSTOM_7", "CUSTOM_8", "CUSTOM_9", "CUSTOM_10", "CUSTOM_11", "CUSTOM_12", "CUSTOM_13", "CUSTOM_14", "CUSTOM_15", "CUSTOM_16", "CUSTOM_17", "CUSTOM_18", "CUSTOM_19", "CUSTOM_20", "CUSTOM_21", "CUSTOM_22", "CUSTOM_23", "CUSTOM_24", "CUSTOM_25", "XML_PROFILE_SYNC_ID", "PROFILE_USER_PERMISSION", "AMADEUS_USER_PERMISSION", "OPEN_BOOKING_USER_PERMISSION", "FUTURE_USE_5", "FUTURE_USE_6", "FUTURE_USE_7", "FUTURE_USE_8", "FUTURE_USE_9", "FUTURE_USE_10") AS 
  select '350' as trx_type, 
               to_char(to_number(regexp_replace(contractors.ntid, '[^0-9]+' ))) employee_id,
               to_char(null) name_prefix,
               to_char(null) name_suffix,
               to_char(null) nick_name,
               to_char(null) redress_number,
               to_char(null) gender,
               to_char(null) date_of_birth,    
               case
                 when spvr_emplid <> '00000' then to_char(to_number(regexp_replace(contractors.spvr_emplid, '[^0-9]+' )))
                 else to_char(to_number(regexp_replace(contractors.spvr_ntid, '[^0-9]+' )))
               end manager_company_empl_id,                
               contractors.job_descr job_title,                                                                                                               
               contractors.work_phone work_phone,
               contractors.work_ext work_phone_extension,
               to_char(null) work_fax,
               to_char(null) home_phone,
               to_char(null) cell_phone,
               to_char(null) pager_phone,
               to_char(null) travel_name_remark,   
               'DEFAULT' travel_class_name,     
               to_char(null) gds_profile_name, 
               -- Begin Ver 1.1      
               case 
                 when control_table.travel_reporting_lob ='FM' then 'FMA'
                 else 'HDS'
               end orgunit_or_division ,                                                                                                       
               --'HDS' orgunit_or_division, 
               -- End Ver 1.1      
               to_char(null) home_street_address, --1
               to_char(null) home_city,
               to_char(null) home_state,
               to_char(null) home_postal_code,
               to_char(null) home_country,
               to_char(null) work_street_address,
               contractors.loc_city work_city,
               contractors.loc_state work_state,
               contractors.loc_postal work_postal_code,
               to_char(null)  work_country,
               to_char(null) email_2,
               to_char(null) email_3,                                      
               'HD SUPPLY UDID 19='||control_table.travel_reporting_lob   custom_1,
               'HD SUPPLY UDID 22='||emp.fru                                                      custom_2,
               'HD SUPPLY UDID 29='||nvl( empo.travel_class_name, case when control_table.cntry_code = 'US' then 'DEFAULT' else 'Canada Default' end ) custom_3,
                'HD SUPPLY UDID 52='||contractors.spvr_email_addr custom_4,
                to_char(null) custom_5,
                to_char(null) custom_6,
                to_char(null) custom_7,
                to_char(null) custom_8,
                to_char(null) custom_9,
                to_char(null) custom_10,
                to_char(null) custom_11,
                to_char(null) custom_12,
                to_char(null) custom_13,
                to_char(null) custom_14,
                to_char(null) custom_15,
                to_char(null) custom_16,
                to_char(null) custom_17,
                to_char(null) custom_18,                                          
                to_char(null) custom_19,
                to_char(null) custom_20,
                to_char(null) custom_21,
                to_char(null) custom_22,
                to_char(null) custom_23,
                to_char(null) custom_24,
                to_char(null) custom_25,
                to_char(to_number(regexp_replace(contractors.ntid, '[^0-9]+' ))) xml_profile_sync_id,
                to_char(null) profile_user_permission,                
                to_char(null) amadeus_user_permission,                
                to_char(null) open_booking_user_permission,
                to_char(null) future_use_5,     
                to_char(null) future_use_6,
                to_char(null) future_use_7,
                to_char(null) future_use_8,
                to_char(null) future_use_9,
                to_char(null) future_use_10 
           from hdscmmn.hr_ad_all_vw@eaapxprd.hsi.hughessupply.com contractors
                left outer join hdscmmn.hr_employee_all_vw@eaapxprd.hsi.hughessupply.com emp on to_number(regexp_replace(contractors.ntid, '[^0-9]+' ))= emp.emplid                
                left outer join hdscmmn.et_location_code@eaapxprd.hsi.hughessupply.com et on contractors.fru = et.fru
                left outer join hdscmmn.concur_bu_xref@eaapxprd.hsi.hughessupply.com control_table on control_table.business_unit = contractors.ps_business_unit
                left outer join hdscmmn.concur_fru_override@eaapxprd.hsi.hughessupply.com fruo on contractors.fru = fruo.fru
                left outer join hdscmmn.concur_emp_override@eaapxprd.hsi.hughessupply.com empo on emp.emplid = empo.emplid
          where 1 =1 
          and contractors.usertype ='Contractor'
          and contractors.email_addr is not null
          and to_number(regexp_replace(contractors.ntid, '[^0-9]+' )) is not null
          and to_number(regexp_replace(contractors.spvr_ntid, '[^0-9]+' )) is not null  
          and (
                                ( 
                                    ( trunc(contractors.termination_dt) + 90 ) >= trunc(sysdate) and control_table.business_unit <> 'WW1US')
                                   or 
                                   (contractors.user_acct_status_code = 'A'  and control_table.business_unit <> 'WW1US')                                
                  )
            and (control_table.business_unit is not null  or fruo.fru is not null)
            and et.entrp_entity <> 'PPH'                        
union all
/*  350 record for all employees */
select '350' as trx_type,
               emp.emplid employee_id,
               to_char(null) name_prefix,
               to_char(null) name_suffix,
               to_char(null) nick_name,
               to_char(null) redress_number,
               to_char(null) gender,
               to_char(null) date_of_birth,     
                case
                   when empo.supervisor_id = 'NONE' then null
                   when empo.expense_access_flg = 'Y' then coalesce(empo.supervisor_id, emp.supervisor_id) /* Individual user overrides for enabling expense */
                   when empo.expense_access_flg = 'N' then null /* Individual user overrides for disabling Expense */
                   when fruo.fru is not null then coalesce(empo.supervisor_id, emp.supervisor_id) /*Overrides for FRUs with expense access before the rest of GSC frus */ 
                   when control_table.exp_active = 'Y' then  coalesce(empo.supervisor_id, emp.supervisor_id) /*Any employees at BUs who have expense access enabled */
                   when control_table.exp_active = 'N' then  coalesce(empo.supervisor_id, emp.supervisor_id) /*Any employees at BUs who have expense access disabled */                   
                   when emp.emplid in ( select supervisor_id
                                          from hdscmmn.hr_employee_all_vw@eaapxprd.hsi.hughessupply.com
                                         where business_unit in ( select distinct business_unit
                                                                    from hdscmmn.concur_bu_xref@eaapxprd.hsi.hughessupply.com
                                                                   where exp_active = 'Y' )
                                           and ( hr_status = 'A'
                                             or termination_dt >= sysdate - 90 ) )
                   then
                    coalesce(empo.supervisor_id, emp.supervisor_id) /*Any supervisors of employees who have expense access (for supervisor in other BUs, like executives reporting to Corp). */
                   else
                      null
                end manager_company_empl_id,         
               emp.job_descr job_title,                                                                                                             
               emp.work_phone work_phone,
               to_char(null) work_phone_extension,
               to_char(null) work_fax,
               to_char(null) home_phone,
               to_char(null) cell_phone,
               to_char(null) pager_phone,
               to_char(null) travel_name_remark, 
               -- Begin Ver 1.2
               case 
                 when (empo.emplid = emp.emplid and empo.travel_class_name is not null) then empo.travel_class_name
                 when (empo.emplid = emp.emplid and empo.travel_class_name is null and control_table.cntry_code = 'US') then 'DEFAULT' -- else 'Canada Default'               
                 when (empo.emplid = emp.emplid and empo.travel_class_name is null and control_table.cntry_code != 'US') then 'Canada Default'                 
                 when control_table.cntry_code = 'US' then 'DEFAULT' 
                 when control_table.cntry_code != 'US' then 'Canada Default'                 
                 else 'Default' 
                end travel_class_name,                    
              -- End Ver 1.2     
               to_char(null) gds_profile_name,                                                                                                          
               -- Begin Ver 1.1      
               case 
                 when control_table.travel_reporting_lob ='FM' then 'FMA'
                 else 'HDS'
               end orgunit_or_division ,                                                                                                       
               --'HDS' orgunit_or_division, 
               -- End Ver 1.1      
               to_char(null) home_street_address, --1
               to_char(null) home_city,
               to_char(null) home_state,
               to_char(null) home_postal_code,
               to_char(null) home_country,
               to_char(null) work_street_address,
               emp.loc_city work_city,
               emp.loc_state work_state,
               emp.loc_postal work_postal_code,
               control_table.cntry_code work_country,               
               to_char(null) email_2,
               to_char(null) email_3,                                      
               'HD SUPPLY UDID 19='||control_table.travel_reporting_lob   custom_1,
               'HD SUPPLY UDID 22='||emp.fru                                                      custom_2,
               'HD SUPPLY UDID 29='||nvl( empo.travel_class_name, case when control_table.cntry_code = 'US' then 'DEFAULT' else 'Canada Default' end ) custom_3,
                'HD SUPPLY UDID 52='||spvr_email_addr custom_4,
                to_char(null) custom_5,
                to_char(null) custom_6,
                to_char(null) custom_7,
                to_char(null) custom_8,
                to_char(null) custom_9,
                to_char(null) custom_10,
                to_char(null) custom_11,
                to_char(null) custom_12,
                to_char(null) custom_13,
                to_char(null) custom_14,
                to_char(null) custom_15,
                to_char(null) custom_16,
                to_char(null) custom_17,
                to_char(null) custom_18,                                          
                to_char(null) custom_19,
                to_char(null) custom_20,
                to_char(null) custom_21,
                to_char(null) custom_22,
                to_char(null) custom_23,
                to_char(null) custom_24,
                to_char(null) custom_25,
                emp.emplid xml_profile_sync_id,
                to_char(null) profile_user_permission,                
                to_char(null) amadeus_user_permission,                
                to_char(null) open_booking_user_permission,
                to_char(null) future_use_5,     
                to_char(null) future_use_6,
                to_char(null) future_use_7,
                to_char(null) future_use_8,
                to_char(null) future_use_9,
                to_char(null) future_use_10 
           from hdscmmn.hr_employee_all_vw@eaapxprd.hsi.hughessupply.com emp
                left outer join hdscmmn.et_location_code@eaapxprd.hsi.hughessupply.com et on emp.fru = et.fru
                left outer join hdscmmn.concur_bu_xref@eaapxprd.hsi.hughessupply.com control_table on control_table.business_unit = emp.business_unit
                left outer join hdscmmn.concur_fru_override@eaapxprd.hsi.hughessupply.com fruo on emp.fru = fruo.fru
                left outer join hdscmmn.concur_emp_override@eaapxprd.hsi.hughessupply.com empo on emp.emplid = empo.emplid
          where 1 =1   
          and ( hr_status = 'A' or termination_dt >= trunc(sysdate) - 120 )
            and (control_table.business_unit is not null  or fruo.fru is not null or empo.emplid is not null)
            and et.entrp_entity <> 'PPH';

   COMMENT ON TABLE "APPS"."XXCUS_CONCUR_EMP_EXPORT_350_V"  IS 'TMS: 20180227-00179';
