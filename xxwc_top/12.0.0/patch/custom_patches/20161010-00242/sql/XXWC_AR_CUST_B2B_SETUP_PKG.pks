CREATE OR REPLACE PACKAGE APPS.XXWC_AR_CUST_B2B_SETUP_PKG
AS
   /********************************************************************************************************************************
      $Header XXWC_AR_CUST_B2B_SETUP_PKG.pks $
      Module Name: XXWC_AR_CUST_B2B_SETUP_PKG.pks

      PURPOSE:   This package is used for AR customer b2b setup and triggered by oracle.apps.ar.hz.CustAccount.create event

      REVISIONS:
      Ver        Date        Author                     Description
      ---------  ----------  ---------------         -------------------------------------------------------------------------------
      1.0        07-Nov-2016  Rakesh Patel           TMS#20161010-00242-Change SOA and ASN to reflect priced copy settings on B2B Setup page
   *********************************************************************************************************************************/

   FUNCTION xxwc_create_b2b_setup( p_subscription_guid  IN     RAW,
                                  p_event               IN OUT wf_event_t
       ) RETURN VARCHAR2;
            
 END XXWC_AR_CUST_B2B_SETUP_PKG;
/