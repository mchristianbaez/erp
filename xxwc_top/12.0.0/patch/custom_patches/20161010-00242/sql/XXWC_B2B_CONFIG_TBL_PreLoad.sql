/*************************************************************************
    $Header XXWC_B2B_CONFIG_TBL_PreLoad.sql $
     Module Name: XXWC_B2B_CONFIG_TBL_PreLoad
   
     PURPOSE:   This script is used to merge the data from XXWC.XXWC_B2B_CUST_INFO_TBL and XXWC.XXWC_B2B_POD_INFO_TBL tables into this table.

     REVISIONS:
     Ver        Date        Author                             Description
     ---------  ----------  -------------------------------    -------------------------
     1.0        11/04/2016  Rakesh Patel                       TMS#20161010-00231- Integrate B2B and POD setup pages to a single page.
**************************************************************************/
SET SERVEROUTPUT ON SIZE 1000000
DECLARE
  l_count NUMBER := 0;
BEGIN
    apps.mo_global.set_policy_context('S',162);

   	DBMS_OUTPUT.put_line ('TMS# 20161010-00231 , Script 1 -Before Insert');
	
	DELETE FROM XXWC.XXWC_B2B_CONFIG_TBL;
	
	DBMS_OUTPUT.put_line ('TMS# 20161010-00231, After Delete, records Deleted from XXWC_B2B_CONFIG_TBL : '||sql%rowcount);
   
    --Load all B2B parties and associated accounts in ERP
	INSERT
	INTO XXWC.XXWC_B2B_CONFIG_TBL
	  (
        PARTY_ID,
        PARTY_NAME,
        PARTY_NUMBER,
        CUSTOMER_ID,
        DELIVER_SOA,
        DELIVER_ASN,
        DELIVER_INVOICE,
        START_DATE_ACTIVE,
        END_DATE_ACTIVE,
        CREATION_DATE,
        CREATED_BY,
        LAST_UPDATE_DATE,
        LAST_UPDATED_BY,
        TP_NAME,
        DELIVER_POA,
        DEFAULT_EMAIL,
        STATUS,
        NOTIFICATION_EMAIL,
        NOTIFY_ACCOUNT_MGR,
        SOA_EMAIL,
        ASN_EMAIL,
        COMMENTS,
        APPROVED_DATE,
        ACCOUNT_NUMBER,                                    
        ACCOUNT_NAME,                                     
        CUST_ACCOUNT_ID                                  
	  )
	SELECT
        CUST.PARTY_ID,                                        --CUST 1
        CUST.PARTY_NAME,                                      --CUST 2    
        CUST.PARTY_NUMBER,                                    --CUST 3
        CUST.CUSTOMER_ID,                                     --CUST 4 -- NULL Value not used in prod
        CUST.DELIVER_SOA,                                     --CUST 5
        CUST.DELIVER_ASN,                                     --CUST 6   
        CUST.DELIVER_INVOICE,                                 --CUST 7
        CUST.START_DATE_ACTIVE,                               --CUST 8
        CUST.END_DATE_ACTIVE,                                 --CUST 9
        CUST.CREATION_DATE,                                   --CUST 10
        CUST.CREATED_BY,                                      --CUST 11
        CUST.LAST_UPDATE_DATE,                                --CUST 12
        CUST.LAST_UPDATED_BY,                                 --CUST 13
        CUST.TP_NAME,                                         --CUST 14
        CUST.DELIVER_POA,                                     --CUST 15 
        CUST.DEFAULT_EMAIL,                                   --CUST 16   
        CUST.STATUS,                                          --CUST 17   
        CUST.NOTIFICATION_EMAIL,                              --CUST 18 
        CUST.NOTIFY_ACCOUNT_MGR,                              --CUST 19 
        CUST.SOA_EMAIL,                                       --CUST 20 
        CUST.ASN_EMAIL,                                       --CUST 21   
        CUST.COMMENTS,                                        --CUST 22
        CUST.APPROVED_DATE,                                   --CUST 23
        HCA.ACCOUNT_NUMBER,                                   --HCA  
        HCA.ACCOUNT_NAME,                                     --HCA   
        HCA.CUST_ACCOUNT_ID                                   --HCA
       FROM XXWC.XXWC_B2B_CUST_INFO_TBL CUST
           ,APPS.HZ_CUST_ACCOUNTS HCA
      WHERE HCA.PARTY_ID = CUST.PARTY_ID
        AND HCA.STATUS = 'A';
        
   DBMS_OUTPUT.put_line ('After Insert - B2B parties and associated accounts in ERP, records Inserted into XXWC_B2B_CONFIG_TBL table: '||sql%rowcount);
   COMMIT;
   
   --Load all B2B parties and account sites from POD table
   INSERT
	INTO XXWC.XXWC_B2B_CONFIG_TBL
	  (
        PARTY_ID,
        PARTY_NAME,
        PARTY_NUMBER,
        CUSTOMER_ID,
        DELIVER_SOA,
        DELIVER_ASN,
        DELIVER_INVOICE,
        START_DATE_ACTIVE,
        END_DATE_ACTIVE,
        CREATION_DATE,
        CREATED_BY,
        LAST_UPDATE_DATE,
        LAST_UPDATED_BY,
        TP_NAME,
        DELIVER_POA,
        DEFAULT_EMAIL,
        STATUS,
        NOTIFICATION_EMAIL,
        NOTIFY_ACCOUNT_MGR,
        SOA_EMAIL,
        ASN_EMAIL,
        COMMENTS,
        APPROVED_DATE,
        DELIVER_POD,
        POD_EMAIL,
        POD_FREQUENCY,
        ACCOUNT_NUMBER,
        ACCOUNT_NAME,
        CUST_ACCOUNT_ID,
        POD_LAST_SENT_DATE,
        POD_NEXT_SEND_DATE,
        ID,
        LOCATION,
        SITE_USE_ID,
        SOA_PRINT_PRICE
	  )
	SELECT
        NVL(POD.PARTY_ID,           CUST.PARTY_ID),           --POD 1 /CUST 1
        CUST.PARTY_NAME,                                      --CUST 2    
        CUST.PARTY_NUMBER,                                    --CUST 3
        CUST.CUSTOMER_ID,                                     --CUST 4
        CUST.DELIVER_SOA,                                     --CUST 5
        CUST.DELIVER_ASN,                                     --CUST 6   
        CUST.DELIVER_INVOICE,                                 --CUST 7
        NVL(POD.START_DATE_ACTIVE,  CUST.START_DATE_ACTIVE),  --POD 18 /CUST 8
        NVL(POD.END_DATE_ACTIVE,    CUST.END_DATE_ACTIVE),    --POD 19 /CUST 9
        NVL(POD.CREATION_DATE,      CUST.CREATION_DATE),      --POD 10 /CUST 10
        NVL(POD.CREATED_BY,         CUST.CREATED_BY),         --POD 11 /CUST 11
        NVL(POD.LAST_UPDATE_DATE,   CUST.LAST_UPDATE_DATE),   --POD 12 /CUST 12
        NVL(POD.LAST_UPDATED_BY,    CUST.LAST_UPDATED_BY),    --POD 13 /CUST 13
        CUST.TP_NAME,                                         --CUST 14
        CUST.DELIVER_POA,                                     --CUST 15 
        CUST.DEFAULT_EMAIL,                                   --CUST 16   
        CUST.STATUS,                                          --CUST 17   
        CUST.NOTIFICATION_EMAIL,                              --CUST 18 
        NVL(POD.NOTIFY_ACCOUNT_MGR, CUST.NOTIFY_ACCOUNT_MGR), --POD 17/-- CUST 19 
        CUST.SOA_EMAIL,                                       --CUST 20 
        CUST.ASN_EMAIL,                                       --CUST 21   
        CUST.COMMENTS,                                        --CUST 22
        CUST.APPROVED_DATE,                                   --CUST 23
        POD.DELIVER_POD,                                      --POD 5 /-- CUST 24 --Not required from this table
        POD.POD_EMAIL,                                        --POD 6 /-- CUST 25 --Not required from this table
        POD.POD_FREQUENCY,                                    --POD 7 /-- CUST 26 --Not required from this table
        POD.ACCOUNT_NUMBER,                                   --POD 3
        POD.ACCOUNT_NAME,                                     --POD 4
        POD.CUST_ACCOUNT_ID,                                  --POD 2
        POD.POD_LAST_SENT_DATE,                               --POD 8
        POD.POD_NEXT_SEND_DATE,                               --POD 9  
        POD.ID,                                               --POD 14
        POD.LOCATION,                                         --POD 15
        POD.SITE_USE_ID,                                      --POD 16
        --DECODE(APPS.XXWC_ONT_ROUTINES_PKG.CHECK_PRINT_PRICE (p_ship_to_org_id => POD.SITE_USE_ID), 0, 'Y', 1, 'Y', NULL, 'Y', 'N')
		(SELECT DECODE(hcas.attribute1, 1, 'Y', NULL, 'Y', 'N')
           FROM apps.hz_cust_acct_sites hcas,
                apps.hz_cust_site_uses hcsu
          WHERE hcsu.cust_acct_site_id = hcas.cust_acct_site_id
		    AND hcsu.org_id            = hcas.org_id
			AND hcsu.site_use_id       = pod.site_use_id
			AND ROWNUM = 1
            )
   FROM XXWC.XXWC_B2B_CUST_INFO_TBL CUST
       ,XXWC.XXWC_B2B_POD_INFO_TBL  POD
  WHERE CUST.PARTY_ID = POD.PARTY_ID
    AND POD.SITE_USE_ID IS NOT NULL; 
    
    DBMS_OUTPUT.put_line ('After Insert - B2B parties and account sites from POD table, records Inserted into XXWC_B2B_CONFIG_TBL table: '||sql%rowcount);
    COMMIT;
    
    --Merge cust and pod table data
    FOR r_pod_account IN ( SELECT *
                             FROM XXWC.XXWC_B2B_POD_INFO_TBL  
                            WHERE SITE_USE_ID IS NULL
               )
     LOOP
       BEGIN
          UPDATE XXWC.XXWC_B2B_CONFIG_TBL B2B_CONFIG
             SET B2B_CONFIG.DELIVER_POD        = r_pod_account.DELIVER_POD,       --POD 5 /-- CUST 24 (Not required from this table)
                 B2B_CONFIG.POD_EMAIL          = r_pod_account.POD_EMAIL,         --POD 6 /-- CUST 25 (Not required from this table)
                 B2B_CONFIG.POD_FREQUENCY      = r_pod_account.POD_FREQUENCY,     --POD 7 /-- CUST 26 (Not required from this table)
                 B2B_CONFIG.POD_LAST_SENT_DATE = r_pod_account.POD_LAST_SENT_DATE,--POD 8
                 B2B_CONFIG.POD_NEXT_SEND_DATE = r_pod_account.POD_LAST_SENT_DATE,--POD 9  
                 B2B_CONFIG.ID                 = r_pod_account.ID,                --POD 14
                 B2B_CONFIG.LOCATION           = r_pod_account.LOCATION,          --POD 15
                 B2B_CONFIG.NOTIFY_ACCOUNT_MGR = r_pod_account.NOTIFY_ACCOUNT_MGR --CUST 19 /POD 17
           WHERE B2B_CONFIG.SITE_USE_ID IS NULL
             AND PARTY_ID = r_pod_account.party_id
             AND cust_account_id = r_pod_account.cust_account_id;  
           
           DBMS_OUTPUT.put_line ('Records updated into XXWC_B2B_CONFIG_TBL table: '||sql%rowcount);  
           l_count := l_count+1;  
         EXCEPTION
         WHEN OTHERS THEN
            DBMS_OUTPUT.put_line ('Records not exist in XXWC_B2B_CONFIG_TBL table for PARTY_ID: '||r_pod_account.party_id ||' cust_account_id '||r_pod_account.cust_account_id);  
         END;  
          
     END LOOP;
      
   DBMS_OUTPUT.put_line ('After Update total records updated in XXWC_B2B_CONFIG_TBL table: '||l_count);

   UPDATE XXWC.XXWC_B2B_CONFIG_TBL B2B_CONFIG
      SET SOA_PRINT_PRICE = (
                               SELECT DECODE(hcas.attribute1, 1, 'Y', NULL, 'Y','N')-- DECODE(hcas.attribute1, 0, 'Y', 1, 'Y', NULL, 'Y', 'N')
                                 FROM apps.hz_cust_acct_sites hcas,
                                      apps.hz_party_sites hps,
                                      apps.hz_cust_site_uses hcsu
                                WHERE cust_account_id        = B2B_CONFIG.cust_account_id
                                  AND hcas.status            = 'A'
                                  AND hps.STATUS             = 'A'
                                  AND hcas.PARTY_SITE_ID     = hps.PARTY_SITE_ID
                                  AND hcsu.CUST_ACCT_SITE_ID = hcas.CUST_ACCT_SITE_ID
                                  AND hcsu.status            = 'A'
                                  AND hcsu.SITE_USE_CODE     = 'SHIP_TO'
                                  AND hcsu.status            = 'A'
                                  AND PRIMARY_FLAG           = 'Y'
                            )
    WHERE SITE_USE_ID IS NULL; 
	DBMS_OUTPUT.put_line ('After Update SOA_PRINT_PRICE total records updated in XXWC_B2B_CONFIG_TBL table: '||sql%rowcount);
	
	UPDATE XXWC.XXWC_B2B_CONFIG_TBL B2B_CONFIG
       SET STATUS = 'APPROVED'
     WHERE DELIVER_POD = 'Y'
	   AND STATUS IS NULL;
	
	DBMS_OUTPUT.put_line ('After Update status to approved for DELIVER_POD=Y in XXWC_B2B_CONFIG_TBL table records: '||sql%rowcount);

   COMMIT;
EXCEPTION
   WHEN OTHERS
   THEN
      ROLLBACK;
      DBMS_OUTPUT.put_line ('TMS: 20161010-00231, Errors ='||SQLERRM);
END;
/