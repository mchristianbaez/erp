CREATE OR REPLACE PACKAGE BODY APPS.XXWC_AR_CUST_B2B_SETUP_PKG
AS
   /********************************************************************************************************************************
      $Header XXWC_AR_CUST_B2B_SETUP_PKG.pkb $
      Module Name: XXWC_AR_CUST_B2B_SETUP_PKG.pkb

      PURPOSE:   h This package is used for AR customer b2b setup and triggered by oracle.apps.ar.hz.CustAccount.create event

      REVISIONS:
      Ver        Date        Author                     Description
      ---------  ----------  ---------------         -------------------------------------------------------------------------------
      1.0        28-Oct-2016  Rakesh Patel           TMS#20161010-00242-Change SOA and ASN to reflect priced copy settings on B2B Setup page
   *********************************************************************************************************************************/

   --Email Defaults
   g_dflt_email   fnd_user.email_address%TYPE := 'WC-ITDEVALERTS-U1@HDSupply.com';
   g_pkg_name     VARCHAR2 (50) := 'XXWC_AR_CUST_B2B_SETUP_PKG';
   
   /*************************************************************************
      PROCEDURE Name: xxwc_create_b2b_setup

      PURPOSE:   This procedure will create B2B setup when a new account is created for Trading Partner ('KPT','NPL')

      REVISIONS:
      Ver        Date          Author             Description
      ---------  ------------  ------------       ------------------
      1.0        28-Oct-2016   Rakesh Patel       TMS#20161010-00242-Change SOA and ASN to reflect priced copy settings on B2B Setup page
   ****************************************************************************/
   FUNCTION xxwc_create_b2b_setup(   p_subscription_guid   IN     RAW,
                                     p_event               IN OUT wf_event_t
       ) RETURN VARCHAR2 IS

          l_proc_name               VARCHAR2(2000) := 'xxwc_create_b2b_setup';
          l_sec                     VARCHAR2(200);
          l_custaccount_id          NUMBER;
          l_party_id                hz_cust_accounts.party_id%TYPE := NULL;
          l_count                   NUMBER; 
     BEGIN 
        l_sec := 'Start xxwc_create_b2b_setup';
	    l_custaccount_id  := p_event.GetValueForParameter('CUST_ACCOUNT_ID');
        
        l_sec := 'After Derive l_custaccount_id'||l_custaccount_id;
	    --To check if the part is setup as b2b customer
        BEGIN
           SELECT party_id 
             INTO l_party_id
             FROM hz_cust_accounts
            WHERE cust_account_id = l_custaccount_id;
        EXCEPTION WHEN OTHERS THEN
           l_party_id := NULL;
        END;
        l_sec := 'After Derive l_party_id'||l_party_id;

        SELECT COUNT(1) 
          INTO l_count
          FROM XXWC.XXWC_B2B_CONFIG_TBL
         WHERE party_id = l_party_id
           AND party_number IN (113695,154223)
           AND ROWNUM=1;
        
        l_sec := 'After Derive l_count'||l_count;
		IF l_count >0 THEN
		   INSERT
           	  INTO XXWC.XXWC_B2B_CONFIG_TBL
	         (
              PARTY_ID,
              PARTY_NAME,
              PARTY_NUMBER,
              --CUSTOMER_ID,
              DELIVER_SOA,
              DELIVER_ASN,
              DELIVER_INVOICE,
              START_DATE_ACTIVE,
              END_DATE_ACTIVE,
              --CREATION_DATE,-- This will be drived by the trigger on this table
              CREATED_BY,-- This will be drived by the trigger on this table
              --LAST_UPDATE_DATE,-- This will be drived by the trigger on this table
              --LAST_UPDATED_BY,-- This will be drived by the trigger on this table
              TP_NAME,
              DELIVER_POA,
              DEFAULT_EMAIL,
              STATUS,
              NOTIFICATION_EMAIL,
              NOTIFY_ACCOUNT_MGR,
              SOA_EMAIL,
              ASN_EMAIL,
              COMMENTS,
              APPROVED_DATE,
              DELIVER_POD,
              POD_EMAIL,
              POD_FREQUENCY,
              --ACCOUNT_NUMBER,-- This will be drived by the trigger on this table
              --ACCOUNT_NAME,-- This will be drived by the trigger on this table
              CUST_ACCOUNT_ID,
              --POD_LAST_SENT_DATE,--This field value should default
              --POD_NEXT_SEND_DATE,--This field value should default
              --ID, -- This will be drived by the trigger on this table
              --LOCATION, -- This will be drived by the trigger on this table
              SITE_USE_ID,--Not required to be copied
              SOA_PRINT_PRICE,
              POD_PRINT_PRICE
	          )
	       SELECT
	          PARTY_ID,
              PARTY_NAME,
              PARTY_NUMBER,
              --CUSTOMER_ID, 
              DELIVER_SOA,
              DELIVER_ASN,
              DELIVER_INVOICE,
              SYSDATE,--START_DATE_ACTIVE,
              END_DATE_ACTIVE,
              --CREATION_DATE,-- This will be drived by the trigger on this table
              -1,--CREATED_BY,
              --LAST_UPDATE_DATE,-- This will be drived by the trigger on this table
              --LAST_UPDATED_BY,-- This will be drived by the trigger on this table
              TP_NAME,
              DELIVER_POA,
              DEFAULT_EMAIL,
              STATUS,
              NOTIFICATION_EMAIL,
              NOTIFY_ACCOUNT_MGR,
              SOA_EMAIL,
              ASN_EMAIL,
              COMMENTS,
              APPROVED_DATE,
              DELIVER_POD,
              POD_EMAIL,
              POD_FREQUENCY,
              --ACCOUNT_NUMBER,-- This will be drived by the trigger on this table
              --ACCOUNT_NAME,-- This will be drived by the trigger on this table
              l_custaccount_id,--CUST_ACCOUNT_ID,
              --POD_LAST_SENT_DATE,--This field value should default
              --POD_NEXT_SEND_DATE,--This field value should default
              --ID, -- This will be drived by the trigger on this table
              --LOCATION,-- This will be drived by the trigger on this table
              NULL,--SITE_USE_ID, Not required to be copied
              SOA_PRINT_PRICE,
              POD_PRINT_PRICE
           FROM XXWC.XXWC_B2B_CONFIG_TBL 
          WHERE 1=1
            AND party_id = l_party_id
            AND LAST_UPDATE_DATE IN (SELECT MAX(LAST_UPDATE_DATE)
                                     FROM XXWC.XXWC_B2B_CONFIG_TBL
                                    WHERE party_id = l_party_id  
                                    )
            AND ROWNUM=1;
		END IF;
		 l_sec := 'After insert';
        COMMIT;
        
        RETURN ('SUCCESS');

     EXCEPTION
          WHEN OTHERS THEN
             xxcus_error_pkg.xxcus_error_main_api (
                                                p_called_from         => UPPER(g_pkg_name)||'.'||UPPER(l_proc_name)
                                               ,p_calling             => l_sec
                                               ,p_request_id          => fnd_global.conc_request_id
                                               ,p_ora_error_msg       => SUBSTR(SQLERRM,1,2000)
                                               ,p_error_desc          => 'Error in procedure '||l_proc_name
                                               ,p_distribution_list   => g_dflt_email
                                               ,p_module              => 'XXWC'
                                              );
         RETURN 'SUCCESS';
     END xxwc_create_b2b_setup;
	
END xxwc_ar_cust_b2b_setup_pkg;
/