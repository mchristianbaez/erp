/*************************************************************************
  $Header TMS_20160119-00040_REQUISITIONUPDATE.sql $
  Module Name: TMS_20160119-00040 Incorrect on order quantities because of 
               Internal req finally closed before ISO created.

  PURPOSE: Data fix to cancel Requisition 

  REVISIONS:
  Ver        Date         Author                Description
  ---------  -----------  ------------------    --------------------------
  1.0        19-Jan-2016  P.Vamshidhar          TMS#20160119-00040

**************************************************************************/ 
SET SERVEROUTPUT ON SIZE 1000000
DECLARE
BEGIN
   DBMS_OUTPUT.put_line ('TMS: 20160119-00040   , Before Update');

   UPDATE PO_REQUISITION_HEADERS_ALL
      SET AUTHORIZATION_STATUS = 'CANCELLED',
          CANCEL_FLAG = 'Y',
          LAST_UPDATE_DATE = SYSDATE,
          LAST_UPDATED_BY = 33710
    WHERE REQUISITION_HEADER_ID = 9921601 AND SEGMENT1 = '7548773';

   DBMS_OUTPUT.put_line (
         'TMS: 20160119-00040 Requisition Headers Updated and number of records (Expected: 1) : '
      || SQL%ROWCOUNT);

   UPDATE PO_REQUISITION_LINES_ALL
      SET CANCEL_FLAG = 'Y',
          QUANTITY_CANCELLED = QUANTITY,
          CANCEL_DATE = SYSDATE,
          CANCEL_REASON = 'TMS#20160119-00040',
          LAST_UPDATE_DATE = SYSDATE,
          LAST_UPDATED_BY = 33710
    WHERE REQUISITION_HEADER_ID = 9921601;

   DBMS_OUTPUT.put_line (
         'TMS: 20160119-00040 Requisition Lines Updated and number of records (Expected:25): '
      || SQL%ROWCOUNT);

   COMMIT;

   DBMS_OUTPUT.put_line ('TMS: 20160119-00040   , End Update');
EXCEPTION
   WHEN OTHERS
   THEN
      ROLLBACK;
      DBMS_OUTPUT.put_line ('TMS: 20160119-00040, Errors : ' || SQLERRM);
END;
/