create or replace 
package       XXEIS.EIS_XXWC_OM_UTIL_TUN_PKG
IS
 --//============================================================================
--//  
--// Change Request 			:: Performance Issue  
--//
--// Object Usage 				:: This Object Referred by "OPEN SALES ORDERS REPORT"
--//
--// Object Name         		:: EIS_XXWC_OM_UTIL_TUN_PKG
--//
--// Object Type         		:: Package Specification
--//
--// Object Description  		:: This Package will trigger in before report and insert the values into Table
--//								and also provides some values in the view through functions.
--// Version Control
--//============================================================================
--// Version    Author           Date          Description
--//----------------------------------------------------------------------------
--// 1.0        PRAMOD  	    04/30/2016     Initial Build  --TMS#20160429-00032   --performance Tuning
--//============================================================================

  TYPE T_CASHTYPE_VLDN_TBL IS TABLE OF VARCHAR2(100) INDEX BY VARCHAR2(100);
  G_EMPOLYEE_NAME_VLDN_TBL T_CASHTYPE_VLDN_TBL;
  G_EMPOLYEE_NAME  varchar2(200);
  
   Type CURSOR_TYPE is ref cursor;
   Type View_Tab Is Table Of XXEIS.XXWC_GTT_OE_ORDER_HEADERS%Rowtype Index By Binary_Integer;
   G_VIEW_TAB      VIEW_TAB ;
   G_VIEW_RECORD   VIEW_TAB ;
   
   TYPE VIEW_TAB2 IS TABLE OF XXEIS.XXWC_GTT_OE_ORDER_LINES%ROWTYPE INDEX BY BINARY_INTEGER;
   G_VIEW_TAB2      VIEW_TAB2 ;
   G_VIEW_RECORD2   VIEW_TAB2 ;
   
   TYPE VIEW_TAB3 IS TABLE OF XXEIS.XXWC_GTT_WSH_SHIPPING_TEMP_STG%ROWTYPE INDEX BY BINARY_INTEGER;
   G_VIEW_TAB3      VIEW_TAB3 ;
   G_VIEW_RECORD3   VIEW_TAB3 ;

   TYPE VIEW_TAB4 IS TABLE OF XXEIS.XXWC_GTT_PRINT_LOG_TBL%ROWTYPE INDEX BY BINARY_INTEGER;
   G_VIEW_TAB4      VIEW_TAB4 ;
   G_VIEW_RECORD4   VIEW_TAB4 ;
   
   TYPE VIEW_TAB5 IS TABLE OF XXEIS.XXWC_GTT_OPEN_ORDERS_TMP%ROWTYPE INDEX BY BINARY_INTEGER;
   G_VIEW_TAB5      VIEW_TAB5 ;
   G_VIEW_RECORD5   VIEW_TAB5 ;
   
   function GET_CREATED_BY_NAME (P_EMPLOYEE_ID in number,P_DATE in date)  RETURN VARCHAR2;
   --//============================================================================
--//
--// Object Name         :: get_created_by_name  
--//
--// Object Usage 		 :: This Object Referred by "OPEN SALES ORDERS REPORT"
--//
--// Object Type         :: FUNCTION
--//
--// Object Description  :: This  function is created for EIS_XXWC_OM_OPEN_ORDERS_V View to get the Created by name value.
--//
--// Version Control
--//============================================================================
--// Version    Author           Date          Description
--//----------------------------------------------------------------------------
--// 1.0        PRAMOD  	  04/30/2016       Initial Build  --TMS#20160429-00032   --performance Tuning
--//============================================================================
   
   PROCEDURE open_sales_order_pre_trig(p_org_id              VARCHAR2,
                                       p_salesrep_name       varchar2,
                                       p_ordered_date_from   date,
                                       p_ordered_date_to     date,
                                       p_order_line_status   varchar2,
                                       p_order_header_status varchar2,
                                       p_order_type          varchar2,
                                       p_cust_num            varchar2,
                                       p_cust_name           varchar2,      
                                       p_cust_job_name       VARCHAR2,
                                       P_CREATED_BY          varchar2,
                                       p_shipping_method     varchar2,
                                       p_schedule_date_from  date,
                                       p_schedule_date_to    date,
                                       p_payment_terms       varchar2);
--//============================================================================
--//
--// Object Name         :: open_sales_order_pre_trig  
--//
--// Object Usage 		 :: This Object Referred by "OPEN SALES ORDERS REPORT"
--//
--// Object Type         :: Procedure
--//
--// Object Description  :: This  procedure is created based on the EIS_XXWC_OM_OPEN_ORDERS_V View.Due to the 
--// 						performance issue ,spilt the view definition based on the driving tables and driving data.
--//
--// Version Control
--//============================================================================
--// Version    Author           Date          Description
--//----------------------------------------------------------------------------
--// 1.0        PRAMOD  	  04/30/2016       Initial Build  --TMS#20160429-00032  --performance Tuning
--//============================================================================




END EIS_XXWC_OM_UTIL_TUN_PKG;
/
