--//============================================================================
--//
--// Object Name         :: XXEIS.EIS_XXWC_AR_ORDER_HOLD_DET_V
--//
--// Object Type         :: View
--//
--// Object Description  :: This view will give credit hold and case folder details
--//
--// Version Control
--//============================================================================
--// Vers    Author                  Date          Description
--//----------------------------------------------------------------------------
--// 1.0     Maharajan Shunmugam     09/23/2015    TMS#20140516-00041 Credit - EIS Create Credit Hold Release Report
--//============================================================================
CREATE OR REPLACE VIEW XXEIS.EIS_XXWC_AR_ORDER_HOLD_DET_V
 ( ORDER_NUMBER
 	,CREATED_BY
	,TYPE
	,HOLD_NAME
	,RELEASED_FLAG
        ,ORDER_DATE
	,HOLD_APPLIED_DATE
	,APPLIED_BY_ID
	,APPLIED_BY_NAME
	,HOLD_RELEASED_DATE     
	,RELEASED_BY_USER_ID
	,RELEASED_BY_USER_NAME
	,RELEASE_REASON
	,RELEASE_COMMENT
	,BRANCH_NUMBER
        ,ORDER_AMOUNT
	,ACCOUNT_NAME
	,ACCOUNT_NUMBER
	,SITE_NAME
	,SITE_NUMBER
	,CREDIT_CLASSIFICATION
	,COLLECTOR
	,CASE_FOLDER_RELEASED_DATE
	,CASE_FOLDER_NUMBER
	,BR_NAME
	,BR_NUMBER
	,INVOICE_DATE
	,INVOICE_NUMBER
	,INV_AMOUNT_DUE_ORIGINAL
	,INV_AMOUNT_DUE_REMAINING
	,INV_DUE_DATE)
AS    
SELECT ooha.order_number,   
       holds.created_by,
       flv.meaning "TYPE",
       ohd.name "HOLD NAME",
       holds.released_flag,
       to_char(ooha.ordered_date,'DD-MON-YYYY HH24:MI:SS') "Order Date",
       to_char(holds.creation_date,'DD-MON-YYYY HH24:MI:SS') "HOLD APPLIED DATE",
       fu1.user_name "APPLIED BY ID",
       fu1.description "APPLIED BY NAME",
--       to_char(ohr.creation_date,'DD-MON-YYYY HH24:MI:SS')  "HOLD RELEASED DATE",
       TRUNC(ohr.creation_date)  "HOLD RELEASED DATE",
       fu.user_name "RELEASED BY USER ID",
       fu.description "RELEASED BY USER NAME",
       flv1.meaning RELEASE_REASON,
       ohr.release_comment,
       od.organization_code "BRANCH NUMBER",
       (SELECT apps.oe_totals_grp.get_order_total(ooha.header_id,null,'ALL') FROM dual)"Order Amount",
       hca.account_name,
       hca.account_number,
       hcsu.location site_name,
       hps.party_site_number site_number,
       hcp.credit_classification "CREDIT_CLASSIFICATION",
       ac.name "Collector",
      (SELECT xchcf.case_folder_released_date  FROM xxwc.xxwc_cr_hold_case_folder_stg xchcf
                                  WHERE xchcf.source_column1 = ooha.header_id
                                   AND   xchcf.created_by = holds.created_by
                                   AND   xchcf.case_folder_number IS NOT NULL 
                                  AND   ohsa.hold_entity_code = xchcf.review_type
                                   AND xchcf.FOLDER_CREATION_DATE
                BETWEEN to_char(holds.creation_date,'DD-MON-YYYY HH24:MI:SS') AND to_char(holds.creation_date+ 15/1440,'DD-MON-YYYY HH24:MI:SS') 
                                  and rownum=1 )   "Case Folder Released Date",
        (SELECT xchcf.case_folder_number   FROM xxwc.xxwc_cr_hold_case_folder_stg xchcf
                                  WHERE xchcf.source_column1 = ooha.header_id
                                   AND   xchcf.created_by = holds.created_by
                                   AND   xchcf.case_folder_number IS NOT NULL 
                                  AND   ohsa.hold_entity_code = xchcf.review_type
                                   --AND  a.status= 'CLOSED'
                                   AND xchcf.FOLDER_CREATION_DATE 
                BETWEEN to_char(holds.creation_date,'DD-MON-YYYY HH24:MI:SS') AND to_char(holds.creation_date+ 15/1440,'DD-MON-YYYY HH24:MI:SS') 
                                  and rownum=1 ) "Case Folder Number",
        xlct.fru_descr "Br Name", 
        xlct.entrp_loc  "Br Number",
        to_char(rcta.trx_date,'DD-MON-YYYY HH24:MI:SS') "Invoice Date",
        rcta.trx_number "Invoice Number",
        apsa.amount_due_original inv_amount_due_original ,
        apsa.amount_due_remaining inv_amount_due_remaining,
        to_char(apsa.due_date ,'DD-MON-YYYY HH24:MI:SS') "inv_due_date"
  FROM apps.oe_order_headers_all ooha,
       --oe_order_lines_all ool,        
       apps.oe_order_holds_all holds,
       apps.oe_hold_sources_all ohsa,
       apps.oe_hold_releases ohr,
       apps.oe_hold_definitions ohd,
       apps.fnd_lookup_values flv,
       apps.fnd_user fu,
       apps.fnd_user fu1,
       apps.org_organization_definitions od,
       xxcus.xxcus_location_code_tbl xlct,
       apps.hz_cust_accounts hca,
       apps.hz_cust_site_uses_all hcsu,
       apps.hz_party_sites hps,
       apps.hz_cust_acct_sites_all hcas,
       apps.fnd_lookup_values flv1,
       apps.hz_customer_profiles hcp,
       apps.ar_collectors ac,
       apps.ra_customer_trx_all rcta,
       apps.ar_payment_schedules_all apsa
   WHERE 1=1
     AND holds.header_id = ooha.header_id
     AND holds.hold_source_id = ohsa.hold_source_id
     AND ohsa.hold_id = ohd.hold_id
     AND holds.hold_release_id = ohr.hold_release_id(+)
     AND ohd.hold_id = 1  --Credit Check Failure
    AND fu.user_id(+)=ohr.created_by
    AND fu1.user_id = holds.created_by
    AND ooha.ship_from_org_id=od.organization_id
    AND xlct.lob_branch  = od.organization_code
    AND xlct.entrp_loc LIKE 'BW%'
    AND flv.lookup_code = ohsa.hold_entity_code
    AND flv.lookup_type = 'HOLD_ENTITY_DESC'
    AND flv.view_application_id     = 660
    and flv1.lookup_code = ohr.release_reason_code          
    AND flv1.lookup_type = 'RELEASE_REASON'
    AND flv1.view_application_id = 660
    AND ooha.sold_to_org_id = hca.cust_account_id
    AND ooha.invoice_to_org_id = hcsu.site_use_id
    AND hcsu.cust_acct_site_id = hcas.cust_acct_site_id
    AND hcas.party_site_id = hps.party_site_id
    AND hca.cust_account_id = hcp.cust_account_id
    AND hcp.site_use_id IS NULL
    AND hcp.collector_id = ac.collector_id  
    AND to_char(ooha.order_number) = rcta.interface_header_attribute1(+)
    AND rcta.interface_header_context = 'ORDER ENTRY'
    AND rcta.customer_trx_id = apsa.customer_trx_id(+)
    AND ooha.order_type_id in (1001,1004)
    AND hcp.credit_classification NOT IN ('CSALE')
    AND fu.user_id NOT IN (1,0,15986) --'XXWC_INT_SALESFULFILLMENT','SYSADMIN','AUTOINSTALL'
/