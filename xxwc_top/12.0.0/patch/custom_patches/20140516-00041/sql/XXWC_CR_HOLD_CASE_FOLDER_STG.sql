--//============================================================================
--//
--// Object Name         :: XXWC_CR_HOLD_CASE_FOLDER_STG
--//
--// Object Type         :: TABLE
--//
--// Object Description  :: to get case folder details
--//
--// Version Control
--//============================================================================
--// Vers    Author                  Date          Description
--//----------------------------------------------------------------------------
--// 1.0     Maharajan Shunmugam     09/23/2015    TMS#20140516-00041 Credit - EIS Create Credit Hold Release Report
--//============================================================================
CREATE TABLE XXWC.XXWC_CR_HOLD_CASE_FOLDER_STG
(SOURCE_COLUMN1 VARCHAR2(150),
CREATED_BY NUMBER(15),
CASE_FOLDER_NUMBER VARCHAR2(30),
REVIEW_TYPE VARCHAR2(30),
FOLDER_CREATION_DATE VARCHAR2(30),
CASE_FOLDER_RELEASED_DATE VARCHAR2(30))
/