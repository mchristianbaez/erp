/*************************************************************************
  $Header datafix_TMS_20170125_00038.sql $
  Module Name: TMS_20170125_00038


  REVISIONS:
  Ver        Date         Author                Description
  ---------  -----------  ------------------    --------------------------
   1.0       05-APR-2017  Niraj K Ranjan        TMS#20170125_00038   Ticket #23257254 Stuck
**************************************************************************/ 
SET SERVEROUTPUT ON SIZE 1000000
BEGIN
   DBMS_OUTPUT.put_line ('TMS: 20170125_00038   , Before Update');

   update apps.oe_order_lines_all
   set INVOICE_INTERFACE_STATUS_CODE='YES'
      ,REVREC_SIGNATURE_DATE=sysdate
      ,open_flag='N'
      ,flow_status_code='CLOSED'
      ,INVOICED_QUANTITY=2
   where line_id in (87952676, 87952684)
   and headeR_id= 53962081;

   DBMS_OUTPUT.put_line (
         'TMS: 20170125_00038  Sales order lines updated (Expected:2): '
      || SQL%ROWCOUNT);

   COMMIT;

   DBMS_OUTPUT.put_line ('TMS: 20170125_00038    , End Update');
EXCEPTION
   WHEN OTHERS
   THEN
      ROLLBACK;
      DBMS_OUTPUT.put_line ('TMS: 20170125_00038 , Errors : ' || SQLERRM);
END;
/
