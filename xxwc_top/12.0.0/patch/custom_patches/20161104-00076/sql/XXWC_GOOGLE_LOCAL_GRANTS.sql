   /***************************************************************************************************************
    File Name:XXWC_GOOGLE_LOCAL_GRANTS.sql
    PROGRAM TYPE: Grants Script
    PURPOSE:  For Apex Purpose
    HISTORY:    
    ===============================================================================================================
    VERSION DATE          AUTHOR(S)       DESCRIPTION
    ------- -----------   --------------- -------------------------------------------------------------------------
    1.0     14-Feb-2017   P.Vamshidhar    TMS#20161104-00076  - SEO - Local Inventory Ads (LIA)
   *****************************************************************************************************************/
GRANT SELECT ON  APPS.MTL_CATEGORY_SET_VALID_CATS TO INTERFACE_APEXWC
/   
GRANT SELECT ON  APPS.MTL_CATEGORIES TO INTERFACE_APEXWC
/
   