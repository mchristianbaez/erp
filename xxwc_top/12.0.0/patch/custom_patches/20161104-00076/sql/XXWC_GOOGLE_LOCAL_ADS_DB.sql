   /***********************************************************************************************************************
   *   $Header XXWC_GOOGLE_LOCAL_ADS_DB.sql $
   *   Module Name: XXWC_GOOGLE_LOCAL_ADS_DB.sql
   *
   *   PURPOSE:   This package is used for create DB Directory
   *
   *   REVISIONS:
   *   Ver        Date         Author                Description
   *   ---------  ----------   ---------------       -----------------------------------------------------------------
   *   1.0        10-Feb-2017  P.Vamshidhar          Initial Version(TMS#20161104-00076)
   * *********************************************************************************************************************/

DECLARE
   l_db_name   VARCHAR2 (20);
BEGIN
   SELECT name INTO l_db_name FROM v$database;

   IF l_db_name = 'EBSQA'
   THEN
      EXECUTE IMMEDIATE
         'CREATE OR REPLACE DIRECTORY XXWC_GOOGLE_LOCAL_ADS as ''/xx_iface/ebsqa/outbound/pdh/google_local_ads''';
   ELSIF l_db_name = 'EBSPRD'
   THEN
      EXECUTE IMMEDIATE
         'CREATE OR REPLACE DIRECTORY XXWC_GOOGLE_LOCAL_ADS as ''/xx_iface/ebsprd/outbound/pdh/google_local_ads''';
   END IF;
END;
/
