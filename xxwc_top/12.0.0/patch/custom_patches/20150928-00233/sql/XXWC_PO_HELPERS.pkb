CREATE OR REPLACE PACKAGE BODY APPS.xxwc_po_helpers
IS
   /*************************************************************************
    $Header XXWC_PO_HELPERS.PKG $
     Module Name: XXWC_PO_HELPERS
   
     PURPOSE:   This package is used to hold routines used in Purchasing Module

     REVISIONS:
     Ver        Date        Author                             Description
     ---------  ----------  -------------------------------    -------------------------
     1.0        11/04/2014  Vijaysrinivasan                    TMS#20141002-00050 Multi org changes 
     2.0        04/04/2015  Gopi Damuluri                      TMS#20140715-00036 RF - PO Acceptances
     2.1        09/29/2015  Gopi Damuluri                      TMS#20150928-00233 Confine PO Acceptances logic to POs approved before golive date
   **************************************************************************/

    l_req_id   NUMBER := fnd_global.conc_request_id;
--************************************************************
--20130610-00879  6/10/2013 removed all logging for concurrent processing , added additional condition
--AND NVL (prl.cancel_flag, 'N') = 'N' to "PURCHASE" reqs
--************************************************************
    PROCEDURE delete_inv_requisitions (p_errbuf OUT VARCHAR2, p_retcode OUT VARCHAR2, p_org_id NUMBER DEFAULT NULL)
    IS
        v_user_id        NUMBER;
        v_resp_id        NUMBER;
        v_resp_appl_id   NUMBER;
        v_org_id         NUMBER := 162;
        v_errbuf         CLOB;
        --  v_log_string     CLOB;
        n                NUMBER := 0;
    BEGIN
    --tms ticket 20130610-00879 
        v_org_id := NVL (p_org_id, v_org_id);

        SELECT user_id
          INTO v_user_id
          FROM fnd_user
         WHERE user_name = 'XXWC_INT_SUPPLYCHAIN';

        SELECT responsibility_id, application_id
          INTO v_resp_id, v_resp_appl_id
          FROM fnd_responsibility_vl
         WHERE responsibility_name = 'HDS Purchasing Super User';

        fnd_global.apps_initialize (user_id => v_user_id, resp_id => v_resp_id,   --'HDS Credit Assoc Cash App Mgr - WC'
                                                                               resp_appl_id => v_resp_appl_id);

        mo_global.init ('PO');
        mo_global.set_policy_context ('S', v_org_id);
        fnd_request.set_org_id (fnd_global.org_id);

        --******************************************************************************************************
        --******************************************************************************************************
        --******************************************************************************************************
        FOR r IN (SELECT DISTINCT requisition_header_id
                                 ,segment1
                                 ,interface_source_code
                                 ,authorization_status
                                 ,type_lookup_code
                    FROM (SELECT prh.requisition_header_id
                                ,prl.requisition_line_id
                                ,prh.segment1
                                ,prh.interface_source_code
                                ,prh.authorization_status
                                ,prh.type_lookup_code
                            FROM 
        --Modified the below line for TMS ## 20141002-00050  by VijaySrinivasan  on 11/04/2014 
                            po_requisition_headers prh, 
        --Modified the below line for TMS ## 20141002-00050  by VijaySrinivasan  on 11/04/2014 
                            po_requisition_lines prl
                           WHERE     prh.interface_source_code = 'INV'
                                 AND prh.authorization_status = 'INCOMPLETE' --'UNAPPROVED' -- Changed condition from UNAPPROVED to INCOMPLETE by Dheeresh (08/16/2012)
                                 AND prh.type_lookup_code = 'INTERNAL'
                                 AND prh.requisition_header_id = prl.requisition_header_id
                                 AND prl.line_location_id IS NULL))
        -- and nvl(prl.cancel_flag,'N')='N' ))
        LOOP
            n := n + 1;

            BEGIN
                po_reqs_sv.delete_req (r.requisition_header_id);
            
            EXCEPTION
                WHEN OTHERS
                THEN
                    v_errbuf :=
                           'Error_Stack...'
                        || CHR (10)
                        || DBMS_UTILITY.format_error_stack ()
                        || CHR (10)
                        || ' Error_Backtrace...'
                        || CHR (10)
                        || DBMS_UTILITY.format_error_backtrace ();
                    v_errbuf := 'error when delete requisition=' || r.requisition_header_id || ' ' || v_errbuf;
                    DBMS_OUTPUT.put_line (v_errbuf);

                    IF l_req_id > 0
                    THEN
                        fnd_file.put_line (fnd_file.LOG, v_errbuf);
                    END IF;
            END;
        END LOOP;

        COMMIT;

        n := 0;

        --******************************************************************************************************
        --******************************************************************************************************
        --******************************************************************************************************
        FOR r
            IN (SELECT prh.requisition_header_id
                      ,prl.requisition_line_id
                      ,prh.segment1
                      ,prh.interface_source_code
                      ,prh.authorization_status
                      ,prh.type_lookup_code
                  FROM 
        --Modified the below line for TMS ## 20141002-00050  by VijaySrinivasan  on 11/04/2014 
                  po_requisition_headers prh, 
        --Modified the below line for TMS ## 20141002-00050  by VijaySrinivasan  on 11/04/2014 
                  po_requisition_lines prl
                 WHERE     prh.interface_source_code = 'INV'
                       AND prh.authorization_status = 'APPROVED'
                       AND prh.type_lookup_code = 'PURCHASE'
                       AND prh.requisition_header_id = prl.requisition_header_id
                       AND prl.line_location_id IS NULL
                       AND NVL (prl.cancel_flag, 'N') = 'N')
        LOOP
            DECLARE
                l_return_status   VARCHAR2 (1000);
                l_msg_count       NUMBER;
                l_msg_data        VARCHAR2 (1000);
                lv_header_id      po_tbl_number;
                lv_line_id        po_tbl_number;
                m                 NUMBER := NULL;
                l_msg_dummy       VARCHAR2 (2000);
                l_output          VARCHAR2 (2000);
            BEGIN
                m := 1;

                lv_header_id := po_tbl_number (r.requisition_header_id);
                lv_line_id := po_tbl_number (r.requisition_line_id);
                po_req_document_cancel_grp.cancel_requisition (p_api_version     => 1.0
                                                              ,p_req_header_id   => lv_header_id
                                                              ,p_req_line_id     => lv_line_id
                                                              ,p_cancel_date     => SYSDATE
                                                              ,p_cancel_reason   => 'Cancelled Requisition'
                                                              ,p_source          => 'REQUISITION'
                                                              ,x_return_status   => l_return_status
                                                              ,x_msg_count       => l_msg_count
                                                              ,x_msg_data        => l_msg_data);
                COMMIT;
 
            EXCEPTION
                WHEN OTHERS
                THEN
                    v_errbuf :=
                           'Error_Stack...'
                        || CHR (10)
                        || DBMS_UTILITY.format_error_stack ()
                        || CHR (10)
                        || ' Error_Backtrace...'
                        || CHR (10)
                        || DBMS_UTILITY.format_error_backtrace ();
                    v_errbuf := 'error when cancel requisition=' || r.requisition_header_id || ' ' || v_errbuf;

                    --DBMS_OUTPUT.put_line (v_errbuf);

                    IF l_req_id > 0
                    THEN
                        fnd_file.put_line (fnd_file.LOG, v_errbuf);
                    END IF;
            END;
        END LOOP;
    EXCEPTION
        WHEN OTHERS
        THEN
            v_errbuf :=
                   'Error_Stack...'
                || CHR (10)
                || DBMS_UTILITY.format_error_stack ()
                || CHR (10)
                || ' Error_Backtrace...'
                || CHR (10)
                || DBMS_UTILITY.format_error_backtrace ();
            DBMS_OUTPUT.put_line (v_errbuf);
            p_errbuf := v_errbuf;

            IF l_req_id > 0
            THEN
                fnd_file.put_line (fnd_file.LOG
                                  ,'*Message from apps.xxwc_po_helpers.delete_inv_requisitions ' || v_errbuf);
            END IF;
    END;

    ----******************************************************
    ----******************************************************

    ----******************************************************

    -- POPULATE attribute7 on OEXOETEL FORM with user name created order

    FUNCTION xxwc_find_user_name (p_user_id NUMBER)
        RETURN VARCHAR2
    IS
        v_user_name   VARCHAR2 (124);
    --  42076
    BEGIN
        FOR r IN (SELECT display_name
                    FROM wf_users
                   WHERE name IN (SELECT u.user_name
                                    FROM fnd_user u, apps.per_people_f per
                                   WHERE user_id = p_user_id AND u.employee_id = per.person_id))
        LOOP
            v_user_name := r.display_name;
        END LOOP;

        IF v_user_name IS NULL
        THEN
            FOR r IN (SELECT display_name
                        FROM wf_users
                       WHERE name IN (SELECT u.user_name
                                        FROM fnd_user u
                                       WHERE user_id = p_user_id))
            LOOP
                v_user_name := r.display_name;
            END LOOP;
            --commented by Shankar  20140108-00167 to return just the user name
            --v_user_name := 'database user: ' || v_user_name;
        END IF;

        RETURN v_user_name;
    END;

    ----******************************************************
    -- FUNCTION DELIVERY_RUN (P_HEADER_ID NUMBER)
    --used by XXWC_OEXOETEL_CUSTOM.pll to update OEXOETEL form to check item
    --line.user_item_description out of synch with WSH_DELIVERY_DETAILS
    --return "Y" if needs to be updated- will call later
    --PROCEDURE UPDATE_DELIVERY_RUN (P_HEADER_ID NUMBER)
    --************************************************
    ----**********************************************

    FUNCTION delivery_run (p_header_id NUMBER)
        RETURN VARCHAR2
    IS
        v_change_flag   VARCHAR2 (1) := 'N';
    BEGIN
        FOR r IN (SELECT NVL (user_item_description, 'A') us_i_desc, line_id
                    FROM 
        --Modified the below line for TMS ## 20141002-00050  by VijaySrinivasan  on 11/04/2014 
                    oe_order_lines
                   WHERE header_id = p_header_id AND NVL (invoice_interface_status_code, 'X') <> 'YES') ----will not verify invoiced lines
        LOOP
            IF r.us_i_desc <> NVL (get_delivery_status (r.line_id), 'A')
            THEN
                v_change_flag := 'Y';
            END IF;
        END LOOP;

        RETURN v_change_flag;
    EXCEPTION
        WHEN OTHERS
        THEN
            RETURN v_change_flag;
    END;

    ----******************************************************
    --PROCEDURE UPDATE_DELIVERY_RUN (P_HEADER_ID NUMBER)
    --used by XXWC_OEXOETEL_CUSTOM.pll to update OEXOETEL form to update
    --order lines
    --************************************************
    PROCEDURE update_delivery_run (p_header_id NUMBER)
    IS
        v_delivery_status   VARCHAR2 (128);
    BEGIN
        FOR r IN (SELECT user_item_description, line_id, ROWID vrowid
                    FROM 
        --Modified the below line for TMS ## 20141002-00050  by VijaySrinivasan  on 11/04/2014 
                    oe_order_lines
                   WHERE header_id = p_header_id AND NVL (invoice_interface_status_code, 'X') <> 'YES') --will not update invoiced lines
        LOOP
            v_delivery_status := get_delivery_status (r.line_id);

            IF     v_delivery_status IS NOT NULL
               AND r.user_item_description IS NOT NULL
               AND v_delivery_status <> r.user_item_description
            THEN
        --Modified the below line for TMS ## 20141002-00050  by VijaySrinivasan  on 11/04/2014 
                UPDATE oe_order_lines
                   SET user_item_description = v_delivery_status
                      -- 03/23/13 CG: Added for the new shipping extension
                      ,attribute16 =
                           DECODE (v_delivery_status
                                  ,'OUT_FOR_DELIVERY', 'Y'
                                  ,'OUT_FOR_DELIVERY/PARTIAL_BACKORDER', 'Y'
                                  ,'BACKORDERED', 'N'
                                  ,'DELIVERED', 'D'
                                  ,NULL)
                 WHERE ROWID = r.vrowid;
            ELSIF v_delivery_status IS NOT NULL AND r.user_item_description IS NULL
            THEN
        --Modified the below line for TMS ## 20141002-00050  by VijaySrinivasan  on 11/04/2014 
                UPDATE oe_order_lines
                   SET user_item_description = v_delivery_status
                      -- 03/23/13 CG: Added for the new shipping extension
                      ,attribute16 =
                           DECODE (v_delivery_status
                                  ,'OUT_FOR_DELIVERY', 'Y'
                                  ,'OUT_FOR_DELIVERY/PARTIAL_BACKORDER', 'Y'
                                  ,'BACKORDERED', 'N'
                                  ,'DELIVERED', 'D'
                                  ,NULL)
                 WHERE ROWID = r.vrowid;
            ELSIF v_delivery_status IS NULL AND r.user_item_description IS NOT NULL
            THEN
        --Modified the below line for TMS ## 20141002-00050  by VijaySrinivasan  on 11/04/2014 
                UPDATE oe_order_lines
                   SET user_item_description = NULL
                 WHERE ROWID = r.vrowid;
            END IF;
        END LOOP;

        COMMIT;
    EXCEPTION
        WHEN OTHERS
        THEN
            NULL;
    END;

    ----******************************************************
    --  FUNCTION GET_DELIVERY_STATUS (P_LINE_ID NUMBER)
    --used by XXWC_OEXOETEL_CUSTOM.pll to update OEXOETEL form item
    --line.user_item_description to show delivery status of the order line line
    ----******************************************************
    FUNCTION get_delivery_status (p_line_id NUMBER)
        RETURN VARCHAR2
    IS
        PRAGMA AUTONOMOUS_TRANSACTION;
        v_delivery_status   VARCHAR2 (124);
        v_order_type        VARCHAR2 (124);
        v_count             NUMBER;                                                      --Added by Lee Spitzer 3/7/2013

        -- 03/26/13 CG Added
        v_item_type         VARCHAR2 (240);
        
        l_co_qty              NUMBER;
        l_shipped_qty         NUMBER;
        l_ordered_qty         NUMBER;
        l_force_ship_qty      NUMBER;
        l_inventory_item_id   NUMBER;
        l_ship_from_org_id    NUMBER;
        l_cancelled_flag      VARCHAR2(1);
        l_user_item_desc      VARCHAR2(1000);
        -- 08/25/2013 CG: TMS 20130826-00696: Update to exclude Drop Ships
        l_source_type_code    VARCHAR2(30);
        
        -- 11/11/2013 CG: TMS 20131021-00598: Bug fix
        v_shipping_status VARCHAR2 (124);
    BEGIN
        v_delivery_status := NULL;
        v_shipping_status := NULL;

        --Added 3/7/2013 to look at new shipping table
        BEGIN
              SELECT COUNT (*), status
                INTO v_count, v_shipping_status
                FROM xxwc_wsh_shipping_stg
               WHERE line_id = p_line_id AND status IS NOT NULL AND ROWNUM = 1
            GROUP BY status;
        EXCEPTION
            WHEN OTHERS
            THEN
                v_shipping_status := NULL;
                v_count := 0;
        END;

        -- 03/26/13 CG Added
        v_item_type := NULL;
        l_co_qty := NULL;
        l_ordered_qty := NULL;
        l_force_ship_qty := NULL;
        l_inventory_item_id := NULL;
        l_ship_from_org_id := NULL;
        l_cancelled_flag := NULL;
        l_user_item_desc := NULL;
        l_source_type_code := NULL;
        BEGIN
            SELECT msib.item_type
                  ,ol.ordered_quantity
                  ,ol.attribute11
                  , (SELECT NVL (
                                SUM (
                                      l.ordered_quantity
                                    * po_uom_s.po_uom_convert (um.unit_of_measure
                                                              ,msib.primary_unit_of_measure
                                                              ,l.inventory_item_id))
                               ,0)
                       FROM 
        --Modified the below line for TMS ## 20141002-00050  by VijaySrinivasan  on 11/04/2014 
                       oe_order_lines l, 
        --Modified the below line for TMS ## 20141002-00050  by VijaySrinivasan  on 11/04/2014 
                       oe_order_headers h, 
                       mtl_units_of_measure_vl um
                      WHERE     l.header_id = h.header_id
                            AND h.flow_status_code NOT IN ('ENTERED', 'CLOSED', 'CANCELLED')
                            AND l.line_type_id = 1005
                            AND l.ship_from_org_id = msib.organization_id
                            AND l.inventory_item_id = msib.inventory_item_id
                            AND l.order_quantity_uom = um.uom_code)
                  ,ol.inventory_item_id
                  ,ol.ship_from_org_id
                  ,ol.cancelled_flag
                  ,ol.user_item_description
                  -- 08/25/2013 CG: TMS 20130715-00472: Update to exclude Drop Ships
                  ,ol.source_type_code
              INTO v_item_type
                  ,l_ordered_qty
                  ,l_force_ship_qty
                  ,l_co_qty
                  ,l_inventory_item_id
                  ,l_ship_from_org_id
                  ,l_cancelled_flag
                  ,l_user_item_desc
                  ,l_source_type_code
              FROM 
        --Modified the below line for TMS ## 20141002-00050  by VijaySrinivasan  on 11/04/2014 
              oe_order_lines ol, 
              mtl_system_items_b msib
             WHERE     ol.line_id = p_line_id
                   AND ol.inventory_item_id = msib.inventory_item_id
                   AND ol.ship_from_org_id = msib.organization_id;
        EXCEPTION
            WHEN OTHERS
            THEN
                v_item_type := NULL;
                l_co_qty := NULL;
                l_ordered_qty := NULL;
                l_force_ship_qty := NULL;
                l_inventory_item_id := NULL;
                l_ship_from_org_id := NULL;
                l_cancelled_flag := NULL;
                l_user_item_desc := NULL;
                l_source_type_code := NULL;
        END;

        --added if statement to grab the status from shipping table if record is not in custom table
        
        -- 10/23/2013 CG: TMS 20131021-00598: Removed check so it can follow allocation logic for when DFF is populated
        --IF v_count = 0
        --THEN
        -- 10/23/2013 CG: TMS 20131021-00598
        
            --Removed 3/7/2013 for new shipping table logic by Lee Spitzer

            --SELECT L.MEANING --Removed for new shipping changes - update Ready to Release to Awaiting Shipping and Backordered to BACKORDERED
            -- 08/07/2013
            /*SELECT DECODE (l.meaning
                          ,'Ready to Release', 'Awaiting Shipping'
                          ,'Backordered', 'BACKORDERED'
                          ,l.meaning)
              INTO v_delivery_status
              FROM (  SELECT released_status
                        FROM wsh_delivery_details
                       WHERE source_line_id = p_line_id AND released_status IS NOT NULL
                    ORDER BY delivery_detail_id DESC) d
                  ,fnd_lookup_values_vl l
             WHERE l.lookup_type = 'PICK_STATUS' AND l.lookup_code = d.released_status AND ROWNUM = 1;*/

            IF NVL (v_item_type, 'UNKNOWN') IN ('INTANGIBLE', 'FRT', 'PTO')  -- AND v_delivery_status = 'Not Applicable'
            THEN
                v_delivery_status := 'Awaiting Shipping';
            ELSE
                -- 10/23/2013 CG: TMS 20131021-00598: Updated check so it can follow allocation logic for when DFF is populated
                l_shipped_qty :=
                        xxwc_ont_routines_pkg.get_onhand (p_inventory_item_id   => l_inventory_item_id
                                                         ,p_organization_id     => l_ship_from_org_id
                                                         ,p_subinventory        => 'General'
                                                         ,p_return_type         => 'H');
                
                IF l_force_ship_qty IS NULL
                THEN
                    -- 10/23/2013 CG: TMS 20131021-00598: Moved to before if
                    /*l_shipped_qty :=
                        xxwc_ont_routines_pkg.get_onhand (p_inventory_item_id   => l_inventory_item_id
                                                         ,p_organization_id     => l_ship_from_org_id
                                                         ,p_subinventory        => 'General'
                                                         ,p_return_type         => 'H');*/

                    IF l_shipped_qty < 0
                    THEN
                        l_shipped_qty := 0;
                    END IF;

                    l_shipped_qty := l_shipped_qty - l_co_qty;

                    IF l_shipped_qty <= 0
                    THEN
                        l_shipped_qty := 0;
                        v_delivery_status := 'BACKORDERED';
                    ELSIF l_shipped_qty >= l_ordered_qty
                    THEN
                        v_delivery_status := 'Awaiting Shipping';
                    ELSIF l_shipped_qty < l_ordered_qty AND l_shipped_qty > 0
                    THEN
                        v_delivery_status := 'PARTIAL BACKORDERED';
                    ELSE
                        v_delivery_status := 'Unknown Stat';
                    END IF;
                ELSE
                    -- 10/23/2013 CG: TMS 20131021-00598: Updated check so it can follow allocation logic for when DFF is populated                
                    -- v_delivery_status := 'Awaiting Shipping';
                    if l_ordered_qty = l_force_ship_qty then
                        v_delivery_status := 'Awaiting Shipping';
                    elsif l_ordered_qty > l_force_ship_qty and l_force_ship_qty > 0 then
                        v_delivery_status := 'PARTIAL BACKORDERED';
                    elsif l_force_ship_qty = 0 or l_shipped_qty <= 0 then
                        v_delivery_status := 'BACKORDERED';
                    else
                        v_delivery_status := 'Unknown Stat';
                    end if;
                    -- 10/23/2013 CG: TMS 20131021-00598
                END IF;
            END IF;
        
        -- 10/23/2013 CG: TMS 20131021-00598: Removed check so it can follow allocation logic for when DFF is populated
        --END IF;
        -- 10/23/2013 CG: TMS 20131021-00598

        /*if l_user_item_desc is not null and l_user_item_desc != v_delivery_status then
            v_delivery_status := l_user_item_desc; 
        end if;*/

        IF nvl(l_cancelled_flag, 'N') = 'Y' THEN
            v_delivery_status := 'CANCELLED';
        -- 08/25/2013 CG: TMS 20130715-00472: Update to exclude Drop Ships
        ELSIF NVL(l_source_type_code, 'INT') = 'EXTERNAL' THEN
            v_delivery_status := NULL;
        -- 11/11/2013 CG: TMS 20131021-00598: Bug fix
        ELSIF NVL(v_shipping_status, 'UNK') IN ('OUT_FOR_DELIVERY', 'DELIVERED', 'OUT_FOR_DELIVERY/PARTIAL_BACKORDER') THEN
            v_delivery_status := v_shipping_status;        
        END IF;

        -- 03/26/13 CG Added
        /*IF NVL (v_item_type, 'UNKNOWN') IN ('INTANGIBLE', 'FRT', 'PTO') -- AND v_delivery_status = 'Not Applicable'
        THEN
            v_delivery_status := 'Awaiting Shipping';
        END IF;*/
        
        RETURN (v_delivery_status);
    EXCEPTION
        WHEN OTHERS
        THEN
            v_delivery_status := NULL;
            RETURN v_delivery_status;
    END;
----******************************************************
----******************************************************

    -- 11/22/2013 CG: TMS 20131120-00028: New function to determine Doc Creation Method since the from
    -- Field does not display it. Used in Personalization 71.1 on PO Form (POXPOEPO)
    FUNCTION get_po_creation_method (p_po_header_id NUMBER) return VARCHAR2
    IS
        l_doc_creation_method varchar2(30);
    BEGIN
        l_doc_creation_method := null;
        select  document_creation_method
        into    l_doc_creation_method
        from    
        --Modified the below line for TMS ## 20141002-00050  by VijaySrinivasan  on 11/04/2014 
        po_headers
        where   po_header_id = p_po_header_id;
        
        return l_doc_creation_method;
    EXCEPTION
    WHEN OTHERS THEN
        return 'UNKNOWN';
    END get_po_creation_method;
    
    -- 11/22/2013 CG: TMS 20131120-00028: New function to determine if PO has drop ship lines. 
    -- Used in Personalization 71.1 on PO Form (POXPOEPO)
    FUNCTION has_drop_ship_lines (p_po_header_id NUMBER) return VARCHAR2
    IS
        l_ds_line_count number;
        l_has_drop_ship varchar2(1);
    BEGIN
        l_ds_line_count := null;
        l_has_drop_ship := null;
        
        select  count(*)
        into    l_ds_line_count
        from    
        --Modified the below line for TMS ## 20141002-00050  by VijaySrinivasan  on 11/04/2014 
                po_line_locations
        where   po_header_id = p_po_header_id
        and     nvl(drop_ship_flag,'N') = 'Y';
        
        if l_ds_line_count > 0 then
            l_has_drop_ship := 'Y';
        else
            l_has_drop_ship := 'N';
        end if;
        
        return l_has_drop_ship;
    EXCEPTION
    WHEN OTHERS THEN
        return 'UNKNOWN';
    END has_drop_ship_lines;
    
    -- 11/22/2013 CG: TMS 20131120-00028: New function to determine PO Ship to Location. 
    -- Used in Personalization 71.1 on PO Form (POXPOEPO)
    FUNCTION get_po_ship_to_location (p_po_header_id NUMBER) return VARCHAR2
    IS
        l_ship_to_location  varchar2(60);
    BEGIN
        l_ship_to_location := null;
        
        select  distinct(location_code)
        into    l_ship_to_location
        from    
        --Modified the below line for TMS ## 20141002-00050  by VijaySrinivasan  on 11/04/2014 
                po_line_locations plla
        --Modified the below line for TMS ## 20141002-00050  by VijaySrinivasan  on 11/04/2014 
                , hr_locations loc
        where   plla.po_header_id = p_po_header_id
        and     nvl(plla.drop_ship_flag,'N') = 'N'
        and     plla.ship_to_location_id = loc.location_id;
        
        return l_ship_to_location;
    EXCEPTION
    WHEN OTHERS THEN
        return NULL;
    END get_po_ship_to_location;

    -- 12/05/2013 CG: TMS 20131120-00026: New function to determine if PO Line is Open for Receipt/Billing
    -- to be able to default Promised Date. Used in Personalization 61 on PO Form (POXPOEPO)
    FUNCTION is_po_line_open (p_po_line_id NUMBER) RETURN varchar2
    IS
        l_count         number;
        l_po_line_open  varchar2(1);
    BEGIN
        l_count         := null;
        l_po_line_open  := null;
        begin
        
            select  count(*)
            into    l_count
        --Modified the below line for TMS ## 20141002-00050  by VijaySrinivasan  on 11/04/2014 
            from    po_line_locations
            where   po_line_id = p_po_line_id
            and     nvl(closed_code, 'ZZZ') not in ('CLOSED','FINALLY CLOSED','CLOSED FOR INVOICE','CLOSED FOR RECEIVING');
        
        exception
        when others then
            l_count := 0;
        end;
    
        if l_count >= 1 then
            l_po_line_open := 'Y';
        else 
            l_po_line_open := 'N';
        end if;
    
        RETURN l_po_line_open;
    EXCEPTION
    WHEN OTHERS THEN
        RETURN NULL;
    END is_po_line_open;

    -- Version# 2.0 > Start
   /*************************************************************************
      FUNCTION Name: get_po_acceptance_notes_cnt
   
      PURPOSE:   To count the number of PO Acceptances

      REVISIONS:
      Ver        Date        Author                     Description
      ---------  ----------  ---------------   -------------------------
      2.0        04/04/2015  Gopi Damuluri     TMS#20140715-00036 RF - PO Acceptances
      2.1        09/29/2015  Gopi Damuluri     TMS#20150928-00233 Confine PO Acceptances logic to POs approved before golive date            
   ****************************************************************************/
    FUNCTION get_po_acceptance_notes_cnt(p_po_header_id NUMBER) RETURN NUMBER
    IS
    l_edi_processed_flag     VARCHAR2(1) := 'N';
    l_po_acc_cnt             NUMBER      := 0;
    l_revision_num           NUMBER;
    l_po_accpt_date          DATE        := TO_DATE(fnd_profile.VALUE('XXWC_PO_ACCEPTANCE_DATE'), 'DD-MON-YYYY');  -- Version# 2.1
    BEGIN
       BEGIN
       SELECT NVL(edi_processed_flag, 'N')
            , revision_num
         INTO l_edi_processed_flag
            , l_revision_num
         FROM po_headers poh
        WHERE 1 = 1
          AND po_header_id = p_po_header_id
          AND COALESCE(approved_date, revised_date,TRUNC(SYSDATE)) >= l_po_accpt_date -- Version# 2.1
          ;
       EXCEPTION
       WHEN NO_DATA_FOUND THEN
         -- Updating the flags to handle for non PO transactions such as Rentals and RMAs
         l_edi_processed_flag := 'Y';
         l_po_acc_cnt := 1;
       END;
       
       IF l_edi_processed_flag = 'N' THEN
          SELECT COUNT(1)
            INTO l_po_acc_cnt
            FROM po_acceptances poa
           WHERE 1 = 1
             AND po_header_id = p_po_header_id
             AND revision_num = l_revision_num             
             AND accepted_flag = 'Y';
       ELSE
           l_po_acc_cnt := 1;
       END IF;
       
       RETURN l_po_acc_cnt;
    EXCEPTION
    WHEN OTHERS THEN
      RETURN 1;
    END GET_PO_ACCEPTANCE_NOTES_CNT;

   /*************************************************************************
      FUNCTION Name: get_po_acceptance_due_date
   
      PURPOSE:   To calculate PO Acceptance Due Date

      REVISIONS:
      Ver        Date        Author                     Description
      ---------  ----------  ---------------   -------------------------
      2.0        04/04/2015  Gopi Damuluri     TMS#20140715-00036 RF - PO Acceptances
   ****************************************************************************/
    FUNCTION get_po_acceptance_due_date (p_in_date DATE) RETURN VARCHAR2
    IS
     l_return_date        DATE;
     l_return_date_chr  VARCHAR2(30);
     l_curr_day            VARCHAR2(30);
    BEGIN
      SELECT TRIM(to_char(p_in_date,'DAY'))
        INTO l_curr_day
        FROM dual;

      dbms_output.put_line('l_curr_day - ' ||l_curr_day);

      IF l_curr_day = 'MONDAY' THEN
         l_return_date := p_in_date + 2;
      ELSIF l_curr_day = 'TUESDAY' THEN
         l_return_date := p_in_date + 2;
      ELSIF l_curr_day = 'WEDNESDAY' THEN
         l_return_date := p_in_date + 2;
      ELSIF l_curr_day = 'THURSDAY' THEN
         l_return_date := p_in_date + 4;
      ELSIF l_curr_day = 'FRIDAY' THEN
         l_return_date := p_in_date + 4;
      ELSIF l_curr_day = 'SATURDAY' THEN
         l_return_date := p_in_date + 3;
      ELSIF l_curr_day = 'SUNDAY' THEN
         l_return_date := p_in_date + 3;
      ELSE
        dbms_output.put_line('Invalid Day ***'||l_curr_day||'***');
      END IF;

      dbms_output.put_line('l_return_date - ' ||l_return_date);

      SELECT TO_CHAR(l_return_date, 'DD-MON-YYYY')
        INTO l_return_date_chr
        FROM dual;

       dbms_output.put_line('l_return_date_chr - ' ||l_return_date_chr);
       
     RETURN l_return_date_chr;

    EXCEPTION
    WHEN OTHERS THEN
      dbms_output.put_line('Error - ' ||SQLERRM);
      RETURN TO_CHAR(SYSDATE+ 2, 'DD-MON-YYYY');
    END;
    -- Version# 2.0 < End

END;
/