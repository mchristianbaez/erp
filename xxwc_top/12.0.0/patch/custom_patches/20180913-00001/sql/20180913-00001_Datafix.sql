/***********************************************************************************************************************************************
   NAME:     TMS_20180913-00001_Datafix.sql
   PURPOSE:  Data fix

   REVISIONS:
   Ver        Date        Author           Description
   ---------  ----------  ---------------  ----------------------------------------------------------------------------------------------------
   1.0        09/13/2018  Rakesh Patel     TMS#20180913-00001
************************************************************************************************************************************************/
SET SERVEROUTPUT ON SIZE 1000000

BEGIN
   DBMS_OUTPUT.put_line ('Before Insert');

  INSERT INTO xxwc.XXWC_AP_BRANCH_LIST_STG_TBL (
                                      AHH_WAREHOUSE_NUMBER,
                                      ORACLE_BRANCH_NUMBER,
                                      ORACLE_BW_NUMBER
									  )
     VALUES ('R023',
             130,
			'BW530'
             );

   DBMS_OUTPUT.put_line ('Records Inserted -' || SQL%ROWCOUNT);

   INSERT INTO xxwc.XXWC_AP_BRANCH_LIST_STG_TBL (
                                      AHH_WAREHOUSE_NUMBER,
                                      ORACLE_BRANCH_NUMBER,
                                      ORACLE_BW_NUMBER
									  )
     VALUES ('R031',
             517,
			'BW517'
             );

   DBMS_OUTPUT.put_line ('Records Inserted -' || SQL%ROWCOUNT);

   COMMIT;

   EXCEPTION
   WHEN OTHERS
   THEN
      DBMS_OUTPUT.put_line ('Unable to insert record ' || SQLERRM);
	  ROLLBACK;
END;
/