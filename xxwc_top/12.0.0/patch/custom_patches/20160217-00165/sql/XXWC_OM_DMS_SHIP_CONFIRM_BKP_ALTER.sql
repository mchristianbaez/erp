/*************************************************************************
 Copyright (c) 2012 HD Supply
 All rights reserved.
**************************************************************************
   $Header XXWC_OM_DMS_SHIP_CONFIRM_BKP $
  Module Name: XXWC_OM_DMS_SHIP_CONFIRM_BKP

  PURPOSE:

  REVISIONS:
  -- VERSION DATE          AUTHOR(S)     DESCRIPTION
  -- ------- -----------   ------------  -------------------------
  -- 1.0     22-FEB-2016   Gopi Damuluri TMS# 20160217-00165 - Changes to B2B POD - Include DMS Delivery_Id
**************************************************************************/
ALTER TABLE xxwc.xxwc_om_dms_ship_confirm_bkp ADD FILE_NAME VARCHAR2(200);
/