/*************************************************************************
 Copyright (c) 2012 HD Supply
 All rights reserved.
**************************************************************************
   $Header XXWC_OM_DMS_SHIP_CONF_GTT_TBL $
  Module Name: XXWC_OM_DMS_SHIP_CONF_GTT_TBL

  PURPOSE:

  REVISIONS:
  -- VERSION DATE          AUTHOR(S)     DESCRIPTION
  -- ------- -----------   ------------  -------------------------
  -- 1.0     22-FEB-2016   Gopi Damuluri TMS# 20160217-00165 - Changes to B2B POD - Include DMS Delivery_Id
**************************************************************************/
ALTER TABLE xxwc.xxwc_om_dms_ship_conf_gtt_tbl ADD FILE_NAME VARCHAR2(200);
/