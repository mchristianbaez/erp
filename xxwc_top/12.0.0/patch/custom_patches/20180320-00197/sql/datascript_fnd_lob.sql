/*******************************************************************
script name: Task ID: 20180320-00197.sql
purpose: datascript for fnd_lob sequence
revision history:
Version Date Aurthor Description
----------------------------------------------------------------
1.0 17-APR-2017 Nancy Pahwa Task ID: 20180320-00197
*******************************************************************/
SET serveroutput on size 1000000;
declare 
l_last_number number;
l_fnd_lob_file_id number;
l_next_val number;
Begin
-- Alter the increment:
EXECUTE IMMEDIATE 'alter sequence APPLSYS.FND_LOBS_S increment by 5000';
-- Engage the increment:
--select FND_LOBS_S.NEXTVAL from DUAL;
-- Run the following 2 queries again to verify last_number column in dba_sequences is greater than max(file_id) in fnd_lobs table
begin 
select FND_LOBS_S.NEXTVAL into l_next_val from DUAL;
select last_number into l_last_number from dba_sequences
where sequence_name = 'FND_LOBS_S'; 
dbms_output.put_line('last_number' || l_last_number);
select max(file_id) into l_fnd_lob_file_id from fnd_lobs; 
dbms_output.put_line('file id' || l_fnd_lob_file_id);

end;
--If yes now reverse the increment of sequence 
if l_last_number > l_fnd_lob_file_id then 
--Alter the increment back to only go by 1
EXECUTE IMMEDIATE'alter sequence APPLSYS.FND_LOBS_S increment by 1';
end if;
end;
/