------------------------------------------------------------------------------------
/******************************************************************************
   NAME       :  XXEIS.EIS_INV_XXWC_MOVING_AVG_COST_V
   REVISIONS:
	Ver         Date            Author             Description
   ---------  ----------    ---------------  ------------------------------------
    1.0       27-Mar-2017       Siva	            Initial Version -- TMS#20170314-00254 
******************************************************************************/
DROP VIEW XXEIS.EIS_INV_XXWC_MOVING_AVG_COST_V;

CREATE OR REPLACE VIEW XXEIS.EIS_INV_XXWC_MOVING_AVG_COST_V (CREATION_DATE, SKU, LOCATION, ACTUAL_COST, PRIOR_COST, NEW_COST, USER_UPDATED_BY, MMT_TRANSACTION_ID, MSI_INVENTORY_ITEM_ID, MSI_ORGANIZATION_ID)
AS
  SELECT /*+ INDEX(msi MTL_SYSTEM_ITEMS_B_U1)*/
    TRUNC(mmt.creation_date) creation_date,
    msi.segment1 sku,
    mp.organization_code location,
    mmt.actual_cost,
    mmt.prior_cost,
    mmt.new_cost,
    (SELECT ppf.full_name
    FROM apps.fnd_user fu,
      apps.per_all_people_f ppf
    WHERE fu.user_id   = mmt.created_by
    AND fu.employee_id = ppf.person_id
    AND TRUNC(sysdate) BETWEEN ppf.effective_start_date AND ppf.effective_end_date
    )user_updated_by,
    --Primary Keys
    mmt.transaction_id mmt_transaction_id,
    msi.inventory_item_id msi_inventory_item_id,
    msi.organization_id msi_organization_id
    --descr#flexfield#start
    --descr#flexfield#end
  FROM inv.mtl_material_transactions mmt,
    inv.mtl_system_items_b msi,
    inv.mtl_parameters mp
  WHERE mmt.inventory_item_id = msi.inventory_item_id
  AND mmt.organization_id     = msi.organization_id
  AND mmt.transaction_type_id = 80
  AND MMT.ORGANIZATION_ID     = MP.ORGANIZATION_ID
/
