/*
 TMS: 20150812-00160  
 Date: 08/12/2015
 Notes: To change CSP branch for agreement 12700
*/

SET SERVEROUTPUT ON SIZE 100000;


update  xxwc.xxwc_om_contract_pricing_hdr
set organization_id=257
where agreement_id=12700
and organization_id=264;

commit;

/