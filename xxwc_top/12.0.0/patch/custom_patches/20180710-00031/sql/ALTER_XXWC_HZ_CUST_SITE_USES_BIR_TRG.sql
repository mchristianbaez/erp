/******************************************************************************************************************************************************
        $Header XXWC_HZ_CUST_SITE_USES_BIR_TRG $
        Module Name: XXWC_HZ_CUST_SITE_USES_BIR_TRG.sql

        PURPOSE:   AHH Customer Conversion

        REVISIONS:
        Ver        Date        Author           Description
        ---------  ----------  ---------------  ------------------------------------------------------------------------------------------------
        1.0        06/06/2018  P.Vamshidhar     TMS#20180710-00031 -AHH Customer Conversion  - Child TMS
******************************************************************************************************************************************************/
SET SERVEROUT ON

DECLARE
   lvc_trigger_stat   VARCHAR2 (100);
BEGIN
   SELECT status
     INTO lvc_trigger_stat
     FROM dba_triggers
    WHERE trigger_name = 'XXWC_HZ_CUST_SITE_USES_BIR_TRG';

   IF lvc_trigger_stat = 'DISABLED'
   THEN
      EXECUTE IMMEDIATE
         'ALTER TRIGGER APPS.XXWC_HZ_CUST_SITE_USES_BIR_TRG ENABLE';
   ELSE
      EXECUTE IMMEDIATE
         'ALTER TRIGGER APPS.XXWC_HZ_CUST_SITE_USES_BIR_TRG DISABLE';
   END IF;
EXCEPTION
   WHEN OTHERS
   THEN
      DBMS_OUTPUT.PUT_LINE ('Error Occured:' || SQLERRM);
END;
/