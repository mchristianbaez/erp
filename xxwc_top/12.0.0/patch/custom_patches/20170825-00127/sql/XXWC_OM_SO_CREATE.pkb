CREATE OR REPLACE PACKAGE BODY APPS.XXWC_OM_SO_CREATE
AS
/*************************************************************************
     $Header XXWC_OM_SO_CREATE $
     Module Name: XXWC_OM_SO_CREATE.pks

     PURPOSE:   Handwrite Project

    Input Parameters:  a. p_order_header_rec - p_order_header_rec is a record type
                                       be used for order header information.

                       b. p_order_lines_tbl - p_order_lines_tbl is a table type
                                         will be used to store line level information.

    Output Parameters: o_error_code - 'E' - Error or 'S'- Success

                       o_error_msg  - Error message, reason for the failure.

     REVISIONS:
     Ver        Date        Author                     Description
     ---------  ----------  ---------------         -------------------------
     1.0        11/09/2016  Rakesh Patel            Handwrite Project
     2.0        3/15/2017   Rakesh Patel            TMS#20170313-00308 - Validate the user for resp access in order create api, enable debug message
     2.1        3/29/2017   Neha Saini              TMS Task ID: 20170330-00170  change procedure to fetch item proice by org  XXWC_OM_SO_CREATE.get_item_avbl_price
     2.2        4/17/2017   Rakesh Patel            TMS#20170417-00208- Mobile Quote Form 0.2 Release - PLSQL api changes 
     2.3        4/20/2017   Christian Baez          TMS#20170417-00208- Mobile Quote Form 0.2 Release - added get district by ntid function
     2.4        05/01/2017  Rakesh Patel            TMS#20170501-00147-Mobile pricing app 0.2 UC4 job for XXWC_OM_ORD_LN_MV mv refresh   
     2.5        05/22/2017  Rakesh Patel            TMS#20170518-00094-Mobile pricing app version 3.0 plsql api changes
     2.6        06/05/2017  Rakesh Patel            TMS#20170605-00288-Price and availability app- Wrong price return by the API (XXWC_OM_SO_CREATE.line_pricing_data)
     2.7        06/07/2017  Rakesh P./Christian B.  TMS#20170607-00031-Add access to DMRSMRVPEtc
     2.8        06/19/2017  Rakesh P.               TMS# 20170616-00319-Customer search - Allow executive leadership team to see all customers data
     2.9        08/22/2017  Rakesh P./Christian B.  TMS#20170817-00115-CheckIT 1.1.0 - Customer Prospect
     3.0        08/25/2017  Rakesh P./Christian B.  TMS#20170825-00127-CheckIT 1.1.0 Customer Prospecting Fast Follow
**************************************************************************/

   --Email Defaults
   g_dflt_email   fnd_user.email_address%TYPE    := 'WC-ITDEVALERTS-U1@HDSupply.com';

  /*******************************************************************************
   Procedure Name  :   DEBUG_LOG
   Description     :   This function will be used for log messages 

   Change History  :
   DATE         NAME              Modification
   --------     -------           ------------------------------------------
   15-Mar-17    Rakesh Patel      TMS#20170313-00308 - Validate the user for resp access in order create api, enable debug message
   ******************************************************************************/
   PROCEDURE DEBUG_LOG (  p_order_header_rec     IN  xxwc.xxwc_order_header_rec
                         ,p_order_lines_tbl      IN  xxwc.xxwc_order_lines_tbl
                         ,p_status               IN  VARCHAR2
                         ,p_order_number         IN  NUMBER
                         ,p_order_total          IN  NUMBER
                         ,p_message              IN  VARCHAR2)
   IS
       PRAGMA AUTONOMOUS_TRANSACTION;
   BEGIN
      g_call_from := 'Start DEBUG_LOG';
      
      INSERT INTO XXWC.XXWC_HW_LOG_TBL
            (
             ORDER_HEADER_REC        ,
             ORDER_LINES_TBL         ,
             HEADER_ACTION           ,
             ORDER_SOURCE_TYPE       ,
             WAREHOUSE_ID            ,
             ORDER_TYPE_ID           ,  
             ORACLE_ORDER_NUMBER     ,  
             ORACLE_ORDER_TOTAL      ,
             ORIG_SYS_DOCUMENT_REF   ,
             OFFLINE_ORDER_TOTAL     ,  
             ORDER_CREATED_BY        ,
             API_RETURN_STATUS       ,
             API_RETURN_MSG          ,
             CREATION_DATE           ,
             CREATED_BY              ,
             LAST_UPDATE_DATE        ,
             LAST_UPDATED_BY         ,
             ATTRIBUTE1              ,
             ATTRIBUTE2              ,
             ATTRIBUTE3              ,
             ATTRIBUTE4              ,
             ATTRIBUTE5              
             )
         VALUES
             (
              p_order_header_rec                       ,
              p_order_lines_tbl                        ,
              p_order_header_rec.header_action         , 
              p_order_header_rec.order_source_type     ,
              p_order_header_rec.ship_from_org_id      ,
              p_order_header_rec.order_type_id         ,
              p_order_number                           ,
              p_order_total                            ,
              p_order_header_rec.orig_sys_document_ref ,
              p_order_header_rec.offline_order_total   ,
              p_order_header_rec.user_id               ,
              p_status                                 ,
              p_message                                ,
              SYSDATE                                  ,
              -1                                       ,
              SYSDATE                                  ,
              -1                                       ,
              NULL                                     ,   
              NULL                                     ,
              NULL                                     ,
              NULL                                     ,
              NULL                                   
             ) ;  
      --FND_LOG.STRING (G_LOG_LEVEL, upper(G_PACKAGE||'.'||G_CALL_FROM) ||' '|| G_CALL_POINT, P_MESSAGE);
      COMMIT;
      g_call_from := 'End DEBUG_LOG';
  EXCEPTION
    WHEN OTHERS THEN
        g_sqlcode := SQLCODE;
        g_sqlerrm := SQLERRM;
        g_message := 'Error in generating debug log ';
    
        xxcus_error_pkg.xxcus_error_main_api (  p_called_from         => g_package||'.'||g_call_from
                                              , p_calling             => g_call_from
                                              , p_ora_error_msg       => SQLERRM
                                              , p_error_desc          => g_message || g_sqlcode || g_sqlerrm
                                              , p_distribution_list   => g_distro_list
                                              , p_module              => g_module);

   
   END DEBUG_LOG;
  
   PROCEDURE write_output (p_message IN VARCHAR2)
   IS
   BEGIN
      --fnd_file.put_line (fnd_file.output, p_message);
      DBMS_OUTPUT.PUT_LINE(p_message);
   END;

 -- To display the ON ACCOUNT OR CASH C.O.D based on Customer Payment Terms
   FUNCTION payment_term (p_term_id IN NUMBER)
      RETURN VARCHAR2
   AS
      /***************************************************************************************************************************************
      Purpose: This function is used in following reports
      1.Pickticket,
      2.Rental return worksheet,
      3.Quote Detail Report
      4. Sales Receipt Report
      5. Repair quote
      6. Rental receipt

      *************************************************************************************************************************************** */
      CURSOR c_acctterm
      IS
         SELECT NAME, term_id
           FROM ra_terms_tl
          WHERE term_id = p_term_id;
   BEGIN
      RETURN 'ON ACCOUNT';
      FOR r_acctterm IN c_acctterm
      LOOP
         IF r_acctterm.NAME IN ('COD', 'PRFRDCASH')
         THEN
            RETURN 'CASH C.O.D';
         ELSE
            RETURN 'ON ACCOUNT';
         END IF;
      END LOOP;
   EXCEPTION
      WHEN NO_DATA_FOUND
      THEN
         RETURN NULL;
      WHEN OTHERS
      THEN
         RETURN (SQLERRM || '-' || SQLCODE);
   END payment_term;

   /*******************************************************************************
   Function Name   :   IS_VALID_OM_USER
   Description     :   This function will be used to valid if user has access to valid om resp.

   Change History  :
   DATE         NAME              Modification
   --------     -------           ------------------------------------------
   15-Mar-17    Rakesh Patel      TMS#20170313-00308 - Validate the user for resp access in order create api, enable debug message
   ******************************************************************************/
   FUNCTION IS_VALID_OM_USER (p_user_id IN NUMBER)
      RETURN VARCHAR2
   AS 
      l_count NUMBER :=0;
   BEGIN
      SELECT count(1) 
        INTO l_count 
        FROM FND_USER fuser,
             PER_PEOPLE_F per,
             FND_USER_RESP_GROUPS furg,
             FND_RESPONSIBILITY_TL frt
       WHERE fuser.EMPLOYEE_ID = per.PERSON_ID
         AND fuser.USER_ID = furg.USER_ID
         AND fuser.USER_ID = p_user_id
         AND (TO_CHAR (fuser.END_DATE) IS NULL OR fuser.END_DATE > SYSDATE)
         AND frt.RESPONSIBILITY_ID = furg.RESPONSIBILITY_ID
         AND (TO_CHAR (furg.END_DATE) IS NULL OR furg.END_DATE > SYSDATE)
         AND frt.LANGUAGE = 'US'
         AND frt.RESPONSIBILITY_NAME IN ('HDS Order Mgmt Pricing Full - WC'
                                        ,'HDS Order Mgmt Pricing Limited - WC'
                                        ,'HDS Order Mgmt Pricing Standard - WC'
                                        ,'HDS Order Mgmt Pricing Super - WC'
                                        ,'HDS Rental Order Mgmt Pricing Super - WC'
                                        );
        IF l_count > 0 THEN
           RETURN 'Y';
        ELSE
           RETURN 'N';
        END IF;
   EXCEPTION
      WHEN OTHERS
      THEN
         RETURN 'N';
   END IS_VALID_OM_USER;

--Rev# 2.5 < Start 
 /*******************************************************************************
   Function Name   :   IS_COMMODITY_ITEMS
   Description     :   This function will check if the item is a commodity item

   Change History  :
   DATE         NAME              Modification
   --------     -------           ------------------------------------------
   25-May-17    Rakesh Patel      TMS#20170518-00094-Mobile pricing app version 3.0 plsql api changes
   ******************************************************************************/
   FUNCTION IS_COMMODITY_ITEMS (p_inventory_item_id IN NUMBER)
      RETURN VARCHAR2
   AS 
      l_count NUMBER :=0;
   BEGIN
      SELECT count(1) 
        INTO l_count 
        FROM mtl_category_sets_v B
           , mtl_item_categories_v a
       WHERE B.category_set_name = 'Inventory Category'
         AND a.category_set_id   = b.category_set_id
         AND a.organization_id   = 222
         AND a.inventory_item_id = p_inventory_item_id
         AND EXISTS (SELECT 1
                       FROM fnd_lookup_values  flv
                      WHERE flv.lookup_type  = 'XXWC_LAST_PRICE_PAID_EXCLUSION'
                        AND flv.meaning      = a.category_id
                        AND flv.enabled_flag = 'Y'
                        AND TRUNC(SYSDATE) BETWEEN TRUNC(flv.start_date_active) AND TRUNC(NVL(flv.end_date_active,SYSDATE))
                      );
        IF l_count > 0 THEN
           RETURN 'Y';
        ELSE
           RETURN 'N';
        END IF;
   EXCEPTION
      WHEN OTHERS
      THEN
         RETURN 'N';
   END IS_COMMODITY_ITEMS ;
   --Rev# 2.5 < End
   
  /*******************************************************************************
   Procedure Name   :   get_onhand
   Description     :   This function will be called by DF to create sales order in ebs.

   Change History  :
   DATE         NAME              Modification
   --------     -------           ------------------------------------------
   09-Nov-16    Rakesh Patel      Initial  Draft.
   ******************************************************************************/

FUNCTION get_onhand (
       p_inventory_item_id   IN NUMBER
      ,p_organization_id     IN NUMBER
      ,p_base_uom            IN VARCHAR2
      ,p_ord_uom             IN VARCHAR2
      ,p_return_type         IN VARCHAR2
    ) RETURN NUMBER IS
     l_onhand_qty NUMBER;
       BEGIN
       l_onhand_qty :=  xxwc_ont_routines_pkg.get_onhand
                               (p_inventory_item_id =>  p_inventory_item_id
                                ,p_organization_id  =>  p_organization_id -- TMS# 20130618-01287
                                ,p_subinventory      => 'General'
                                ,p_return_type      =>  p_return_type); --H = Onhand, T - Available, R - Reservable

        if p_ord_uom is not null and p_base_uom is not null
             and p_ord_uom != p_base_uom
        then
            select    (l_onhand_qty
                                * po_uom_s.po_uom_convert (p_base_uom
                                             , p_ord_uom
                                             , p_inventory_item_id
                                             )
                            )
            into        l_onhand_qty
            from        dual;
         end if;

         RETURN l_onhand_qty;
    END;

/****************************************************************************************************************
      FUNCTION Name: get_sales_rep_id

      PURPOSE:   To derive Salesrep id associated to Customer

   ****************************************************************************************************************/
   FUNCTION get_sales_rep_id (p_cust_account_id   IN NUMBER) RETURN NUMBER
     AS
     l_salesrep_id   NUMBER;

     CURSOR cur_salesrep (p_customer_id IN NUMBER)
         IS
     SELECT jrs.salesrep_id salesrep_id
       FROM hz_cust_acct_sites_all     hcas
          , hz_cust_site_uses_all      hcsu
          , jtf_rs_salesreps           jrs
          , jtf_rs_resource_extns      jrre
      WHERE 1 = 1
        AND hcas.cust_account_id     = p_customer_id
        AND hcas.cust_acct_site_id   = hcsu.cust_acct_site_id
        AND hcsu.primary_salesrep_id = jrs.salesrep_id
        AND hcas.status              = 'A'
        AND hcsu.status              = 'A'
        AND jrs.resource_id          = jrre.resource_id
        AND jrs.org_id               = hcas.org_id
        AND jrs.salesrep_id          != -3 -- No Sales Credit
        AND SYSDATE BETWEEN jrre.start_Date_active and NVL(jrre.end_date_active, SYSDATE+1)
        AND rownum = 1;
       l_sec                     VARCHAR2 (200);
   BEGIN

     l_sec := 'Derive Salesrep id';

     FOR rec IN cur_salesrep(p_cust_account_id) LOOP
        l_salesrep_id := rec.salesrep_id;
     END LOOP;

     RETURN l_salesrep_id;

   EXCEPTION
   WHEN OTHERS THEN
     RETURN NULL;
   END get_sales_rep_id;

   /****************************************************************************************************************
      FUNCTION Name: get_paymemt_term_id

      PURPOSE:   To derive payment term id associated to Customer

   ****************************************************************************************************************/
   FUNCTION get_payment_term_id (p_cust_account_id   IN NUMBER) RETURN NUMBER
     AS
     l_PAYMENT_TERM_ID   NUMBER;

     CURSOR cur_payment_term (p_cust_account_id IN NUMBER)
         IS
       SELECT standard_terms PAYMENT_TERM_ID
       FROM apps.hz_customer_profiles
       WHERE cust_account_id = p_cust_account_id
       AND  SITE_USE_ID IS NULL;
       l_sec                     VARCHAR2 (200);
   BEGIN

     l_sec := 'Derive payment_term id';

     FOR rec IN cur_payment_term(p_cust_account_id) LOOP
        l_PAYMENT_TERM_ID := rec.PAYMENT_TERM_ID;
     END LOOP;

     RETURN l_PAYMENT_TERM_ID;

   EXCEPTION
   WHEN OTHERS THEN
     RETURN NULL;
   END get_payment_term_id;

    /****************************************************************************************************************
      FUNCTION Name: get_payment_term_nm

      PURPOSE:   To derive payment term name associated to Customer

   ****************************************************************************************************************/
   FUNCTION get_payment_term_nm (p_cust_account_id   IN NUMBER) RETURN VARCHAR2
     AS
     l_PAYMENT_TERM_NAME  VARCHAR2 (200);

     CURSOR cur_payment_term_name (p_cust_account_id IN NUMBER)
         IS
       SELECT term.name payment_term_name
       FROM apps.hz_customer_profiles hcp
       ,ra_terms term
       WHERE hcp.cust_account_id = p_cust_account_id
       AND  hcp.standard_terms   = term.term_id(+)
       AND  hcp.SITE_USE_ID IS NULL;
       l_sec                     VARCHAR2 (200);
   BEGIN

     l_sec := 'Derive payment_term name';

     FOR rec IN cur_payment_term_name(p_cust_account_id) LOOP
        l_PAYMENT_TERM_NAME := rec.payment_term_name;
     END LOOP;

     RETURN l_PAYMENT_TERM_NAME;

   EXCEPTION
   WHEN OTHERS THEN
     RETURN NULL;
   END get_payment_term_nm;

PROCEDURE CREATE_SALES_ORDER
(
     p_order_header_rec     IN   xxwc.xxwc_order_header_rec
    ,p_order_lines_tbl      IN   xxwc.xxwc_order_lines_tbl
    ,o_status               OUT VARCHAR2
    ,o_order_number         OUT NUMBER
    ,o_message              OUT VARCHAR2
    ,p_debug_mode           IN  VARCHAR2 DEFAULT NULL    --E/T/NULL --TMS#20170313-00308 
)
IS
   l_sec     VARCHAR2(3000);
   l_err_msg VARCHAR2(3000);

   l_return_status                VARCHAR2 (2000);

   l_msg_count                    NUMBER;
   l_msg_data                     VARCHAR2 (2000);
   -- PARAMETERS
   l_org                          VARCHAR2 (20)                          := '162'; -- OPERATING UNIT
   -- INPUT VARIABLES FOR PROCESS_ORDER API
   l_header_rec                   oe_order_pub.header_rec_type;
   l_line_tbl                     oe_order_pub.line_tbl_type;
   l_action_request_tbl           oe_order_pub.request_tbl_type;
   -- OUT VARIABLES FOR PROCESS_ORDER API
   l_header_rec_out               oe_order_pub.header_rec_type;
   l_header_val_rec_out           oe_order_pub.header_val_rec_type;
   l_header_adj_tbl_out           oe_order_pub.header_adj_tbl_type;
   l_header_adj_val_tbl_out       oe_order_pub.header_adj_val_tbl_type;
   l_header_price_att_tbl_out     oe_order_pub.header_price_att_tbl_type;
   l_header_adj_att_tbl_out       oe_order_pub.header_adj_att_tbl_type;
   l_header_adj_assoc_tbl_out     oe_order_pub.header_adj_assoc_tbl_type;
   l_header_scredit_tbl_out       oe_order_pub.header_scredit_tbl_type;
   l_header_scredit_val_tbl_out   oe_order_pub.header_scredit_val_tbl_type;
   l_line_tbl_out                 oe_order_pub.line_tbl_type;
   l_line_val_tbl_out             oe_order_pub.line_val_tbl_type;
   l_line_adj_tbl_out             oe_order_pub.line_adj_tbl_type;
   l_line_adj_val_tbl_out         oe_order_pub.line_adj_val_tbl_type;
   l_line_price_att_tbl_out       oe_order_pub.line_price_att_tbl_type;
   l_line_adj_att_tbl_out         oe_order_pub.line_adj_att_tbl_type;
   l_line_adj_assoc_tbl_out       oe_order_pub.line_adj_assoc_tbl_type;
   l_line_scredit_tbl_out         oe_order_pub.line_scredit_tbl_type;
   l_line_scredit_val_tbl_out     oe_order_pub.line_scredit_val_tbl_type;
   l_lot_serial_tbl_out           oe_order_pub.lot_serial_tbl_type;
   l_lot_serial_val_tbl_out       oe_order_pub.lot_serial_val_tbl_type;
   l_action_request_tbl_out       oe_order_pub.request_tbl_type;
   l_msg_index                    NUMBER;
   l_data                         VARCHAR2 (2000);
   l_loop_count                   NUMBER;
   l_debug_file                   VARCHAR2 (200);
   l_user_id                      NUMBER;
   l_resp_id                      NUMBER;
   l_resp_appl_id                 NUMBER;
   l_tbl_idx                      NUMBER :=1;
   l_cust_contact_id              NUMBER;
   l_hdr_create_or_update         VARCHAR2(2000);
   l_header_id                    NUMBER := NULL;
   l_line_id                      NUMBER := NULL;
   l_return_msg                   VARCHAR2 (2000);
   lv_msg_data                    VARCHAR2 (4000);
   l_msg_index_out                NUMBER;
   l_order_source_id              NUMBER;
   l_oracle_order_total           NUMBER;
   l_line_type_id                 NUMBER; 
   l_invoice_to_org_id            NUMBER;
   l_user_name                    apps.fnd_user.user_name%TYPE;--TMS#20170313-00308 
   l_xxwc_oso_debug               VARCHAR2(30) := fnd_profile.value('XXWC_OSO_DEBUG');--TMS#20170313-00308 
BEGIN
   l_sec := 'Start CREATE_SALES_ORDER';
   l_header_id := NULL;
   l_order_source_id := NULL;
   l_hdr_create_or_update := oe_globals.g_opr_create;

   --TMS#20170313-00308 Start
   IF IS_VALID_OM_USER ( p_order_header_rec.user_id ) = 'Y' THEN
      l_user_id := p_order_header_rec.user_id;
   ELSE
      BEGIN 
       SELECT USER_NAME    
         INTO l_user_name
         FROM apps.fnd_user
        WHERE user_id = p_order_header_rec.user_id;
      EXCEPTION
      WHEN OTHERS THEN
         l_user_name := NULL;
      END; 
      
      o_message := 'Responsibility not assigned to NTID : ' ||l_user_name||' to create/ update sales order ';
      RETURN;
   END IF;
   --TMS#20170313-00308 End
   
   BEGIN
     SELECT order_source_id
       INTO l_order_source_id
       FROM apps.oe_order_sources oos
      WHERE oos.enabled_flag = 'Y'
        AND UPPER(name) = UPPER(TRIM(p_order_header_rec.order_source_type));
   EXCEPTION
   WHEN NO_DATA_FOUND THEN
      o_message      := 'Sales Order Source '||p_order_header_rec.order_source_type||' not exist in ERP';
      o_status       :='E';
      RETURN;
   WHEN OTHERS THEN
      o_message      := 'Sales Order Source '||p_order_header_rec.order_source_type||' not exist in ERP';
      o_status       :='E';
      RETURN;
   END;

   DBMS_OUTPUT.put_line ('Sales Order order_source_id : '||l_order_source_id);
   
   --TMS#20170313-00308 Start
   IF p_order_header_rec.invoice_to_org_id IS NULL THEN
     o_message      := 'Bill to site is not provided, please validate customer bill to site setup - invoice_to_org_id: '||p_order_header_rec.invoice_to_org_id;
     o_status       :='E';
     RETURN;
   END IF;
   
   IF p_order_header_rec.ship_from_org_id IS NULL THEN
     o_message      := 'Ship to site is not provided, please validate customer ship to site site setup - ship_from_org_id: '||p_order_header_rec.ship_from_org_id;
     o_status       :='E';
     RETURN;
   END IF;
   --TMS#20170313-00308 End
   
   IF p_order_header_rec.order_number IS NOT NULL OR p_order_header_rec.header_action = oe_globals.g_book_order THEN
      BEGIN
        SELECT header_id
          INTO l_header_id
          FROM apps.oe_order_headers_all ooh
         WHERE ooh.order_number = TRIM(p_order_header_rec.order_number);
      EXCEPTION
      WHEN NO_DATA_FOUND THEN
         o_message      := 'Sales Order '||p_order_header_rec.order_number||' not exist in ERP';
         o_status       :='E';
         RETURN;
      WHEN OTHERS THEN
         o_message      := 'Sales Order '||p_order_header_rec.order_number||' not exist in ERP';
         o_status       :='E';
          RETURN;
     END;
     DBMS_OUTPUT.put_line ('Sales Order header_id : '||l_header_id);
   END IF;

    -- INITIALIZATION REQUIRED FOR R12
   mo_global.set_policy_context ('S', l_org);
   mo_global.init ('ONT');

   l_resp_id      := 50857;
   l_resp_appl_id := 660;

   --INITIALIZE ENVIRONMENT
   fnd_global.apps_initialize (l_user_id, l_resp_id, l_resp_appl_id);

   -- INITIALIZE ACTION REQUEST RECORD
   IF p_order_header_rec.header_action = oe_globals.g_book_order and l_header_id IS NOT NULL THEN
      l_action_request_tbl (1)                  := oe_order_pub.g_miss_request_rec;
      l_action_request_tbl (1).request_type     := oe_globals.g_book_order;
      l_action_request_tbl (1).entity_code      := oe_globals.G_ENTITY_HEADER;
      l_action_request_tbl(1).entity_id         := l_header_id;
      l_hdr_create_or_update                    := oe_globals.g_opr_update;

      DBMS_OUTPUT.put_line ('Book Sales Order : '||p_order_header_rec.order_number);
   ELSE
      IF p_order_header_rec.header_action = oe_globals.g_opr_create THEN
         l_hdr_create_or_update := oe_globals.g_opr_create;
      ELSIF p_order_header_rec.header_action = oe_globals.g_opr_update THEN
        l_hdr_create_or_update := oe_globals.g_opr_update;
      ELSIF p_order_header_rec.header_action = oe_globals.g_opr_delete THEN
        l_hdr_create_or_update := oe_globals.g_opr_delete;
      END IF;
      -- INITIALIZE HEADER RECORD
      l_header_rec := oe_order_pub.g_miss_header_rec;

      IF l_header_id IS NOT NULL THEN
         l_header_rec.header_id         := l_header_id;
      END IF;
      
      IF p_order_header_rec.order_type_id = 1004 THEN
        l_line_type_id := 1005;
      ELSIF p_order_header_rec.order_type_id = 1011 THEN
        l_line_type_id := 1012;
      ELSIF p_order_header_rec.order_type_id = 1006 THEN
        l_line_type_id := 1007;    
      ELSIF p_order_header_rec.order_type_id = 1001 THEN
        l_line_type_id := 1002;              
      END IF;
      
      l_header_rec.order_type_id         := p_order_header_rec.order_type_id;
      l_header_rec.order_source_id       := l_order_source_id;
      l_header_rec.sold_to_org_id        := p_order_header_rec.sold_to_org_id;
      l_header_rec.ship_to_org_id        := p_order_header_rec.ship_to_org_id;
      l_header_rec.orig_sys_document_ref := p_order_header_rec.orig_sys_document_ref;
      l_header_rec.invoice_to_org_id     := p_order_header_rec.invoice_to_org_id;
      
      IF p_order_header_rec.invoice_to_org_id IS NULL THEN
         BEGIN
            SELECT hcsu_b.site_use_id
              INTO l_invoice_to_org_id
              FROM hz_Cust_site_uses_all hcsu_b
                 , hz_Cust_site_uses_all hcsu_s
             WHERE hcsu_s.site_use_id       = p_order_header_rec.ship_to_org_id
               AND hcsu_s.site_use_code     = 'SHIP_TO'
               AND hcsu_b.cust_acct_site_id = hcsu_s.cust_acct_site_id
               AND hcsu_b.site_use_code     = 'BILL_TO'
               AND hcsu_s.status            = 'A'
               AND hcsu_b.status            = 'A'
               AND ROWNUM                   = 1;
         EXCEPTION
         WHEN OTHERS THEN
             SELECT hcsu.site_use_id
              INTO l_invoice_to_org_id
              FROM hz_cust_acct_sites_all   hcas
                 , hz_cust_site_uses_all    hcsu
             WHERE 1                           = 1
               AND hcas.cust_account_id        = p_order_header_rec.sold_to_org_id
               AND hcas.cust_acct_site_id      = hcsu.cust_acct_site_id
               AND hcas.status                 = 'A'
               AND hcsu.status                 = 'A'
               AND hcsu.site_use_code          = 'BILL_TO'
               AND hcas.org_id                 = 162
               AND ROWNUM = 1;  
         END;
         l_header_rec.invoice_to_org_id  := l_invoice_to_org_id;
      END IF;
      
      l_header_rec.ship_from_org_id      := p_order_header_rec.ship_from_org_id;
      l_header_rec.cust_po_number        := p_order_header_rec.cust_po_number; -- Customer PO number
      l_header_rec.operation             := l_hdr_create_or_update;--oe_globals.g_opr_create;
      l_header_rec.ordered_date          := SYSDATE;
      -- Added for rev#2.2 > Start     
      IF p_order_header_rec.salesrep_id IS NOT NULL THEN
         l_header_rec.salesrep_id           := p_order_header_rec.salesrep_id;
      END IF;
      -- Added for rev#2.2 > End
      IF p_order_header_rec.payment_term_id IS NOT NULL THEN
         l_header_rec.payment_term_id       := p_order_header_rec.payment_term_id;
      END IF;
      l_header_rec.attribute11           := p_order_header_rec.auth_buyer_id;
      l_header_rec.org_id                := p_order_header_rec.org_id;
      l_header_rec.shipping_method_code  := p_order_header_rec.shipping_method_code;
      l_header_rec.freight_carrier_code  := p_order_header_rec.freight_carrier_code;


      l_header_rec.invoice_to_contact_id := p_order_header_rec.invoice_to_contact_id;
      l_header_rec.ship_to_contact_id    := p_order_header_rec.ship_to_contact_id;

   FOR l_tbl_idx IN p_order_lines_tbl.first..p_order_lines_tbl.last LOOP
      l_line_id := NULL;
      IF p_order_lines_tbl(l_tbl_idx).line_action = oe_globals.g_opr_create THEN
         -- INITIALIZE LINE RECORD
         l_line_tbl (l_tbl_idx)                   := oe_order_pub.g_miss_line_rec;
         l_line_tbl (l_tbl_idx).operation         := oe_globals.g_opr_create; -- Mandatory Operation to Pass
         l_line_tbl (l_tbl_idx).inventory_item_id := p_order_lines_tbl(l_tbl_idx).inventory_item_id;
         l_line_tbl (l_tbl_idx).ordered_quantity  := p_order_lines_tbl(l_tbl_idx).ordered_quantity;
         l_line_tbl (l_tbl_idx).ship_from_org_id  := p_order_lines_tbl(l_tbl_idx).ship_from_org_id;
         l_line_tbl (l_tbl_idx).subinventory      := p_order_lines_tbl(l_tbl_idx).subinventory;
         l_line_tbl (l_tbl_idx).line_type_id      := l_line_type_id;--p_order_lines_tbl(l_tbl_idx).line_type_id;
      ELSIF p_order_lines_tbl(l_tbl_idx).line_action = oe_globals.g_opr_update THEN
         -- INITIALIZE LINE RECORD
         BEGIN
            SELECT line_id
              INTO l_line_id
              FROM apps.oe_order_lines_all ool
             WHERE ool.line_id = TRIM(p_order_lines_tbl(l_tbl_idx).line_id);
         EXCEPTION
         WHEN OTHERS THEN
           l_line_id := NULL;
         END;
         IF l_line_id IS NOT NULL THEN
            l_line_tbl (l_tbl_idx)                   := oe_order_pub.g_miss_line_rec;
            l_line_tbl (l_tbl_idx).line_id           := p_order_lines_tbl(l_tbl_idx).line_id;
            l_line_tbl (l_tbl_idx).operation         := oe_globals.g_opr_update; -- Mandatory Operation to Pass
            l_line_tbl (l_tbl_idx).inventory_item_id := p_order_lines_tbl(l_tbl_idx).inventory_item_id;
            l_line_tbl (l_tbl_idx).ordered_quantity  := p_order_lines_tbl(l_tbl_idx).ordered_quantity;
            l_line_tbl (l_tbl_idx).ship_from_org_id  := p_order_lines_tbl(l_tbl_idx).ship_from_org_id;
            l_line_tbl (l_tbl_idx).subinventory      := p_order_lines_tbl(l_tbl_idx).subinventory;
            l_line_tbl (l_tbl_idx).line_type_id      := l_line_type_id;--p_order_lines_tbl(l_tbl_idx).line_type_id;
         END IF;
      ELSIF p_order_lines_tbl(l_tbl_idx).line_action = oe_globals.g_opr_delete THEN
         BEGIN
            SELECT line_id
              INTO l_line_id
              FROM apps.oe_order_lines_all ool
             WHERE ool.line_id = TRIM(p_order_lines_tbl(l_tbl_idx).line_id);
         EXCEPTION
         WHEN OTHERS THEN
           l_line_id := NULL;
         END;

         IF l_line_id IS NOT NULL THEN
            -- INITIALIZE LINE RECORD
            l_line_tbl (l_tbl_idx)              := oe_order_pub.g_miss_line_rec;
            l_line_tbl (l_tbl_idx).line_id      := p_order_lines_tbl(l_tbl_idx).line_id;
            l_line_tbl (l_tbl_idx).operation    := oe_globals.g_opr_delete; -- Mandatory Operation to Pass
         END IF;
      ELSE
        -- INITIALIZE LINE RECORD
        l_line_tbl (l_tbl_idx)                   := oe_order_pub.g_miss_line_rec;
        l_line_tbl (l_tbl_idx).operation         := oe_globals.g_opr_create; -- Mandatory Operation to Pass
        l_line_tbl (l_tbl_idx).inventory_item_id := p_order_lines_tbl(l_tbl_idx).inventory_item_id;
        l_line_tbl (l_tbl_idx).ordered_quantity  := p_order_lines_tbl(l_tbl_idx).ordered_quantity;
        l_line_tbl (l_tbl_idx).ship_from_org_id  := p_order_lines_tbl(l_tbl_idx).ship_from_org_id;
        l_line_tbl (l_tbl_idx).subinventory      := p_order_lines_tbl(l_tbl_idx).subinventory;
        l_line_tbl (l_tbl_idx).line_type_id      := l_line_type_id;-- p_order_lines_tbl(l_tbl_idx).line_type_id;
      END IF;
     END LOOP;
   END IF;
   
   -- CALLTO PROCESS ORDER API
   oe_order_pub.process_order (
         p_org_id=> l_org,
         --     p_operating_unit           => NULL,
         p_api_version_number=> 1.0,
         p_header_rec=> l_header_rec,
         p_line_tbl=> l_line_tbl,
         p_action_request_tbl=> l_action_request_tbl,
         -- OUT variables
         x_header_rec=> l_header_rec_out,
         x_header_val_rec=> l_header_val_rec_out,
         x_header_adj_tbl=> l_header_adj_tbl_out,
         x_header_adj_val_tbl=> l_header_adj_val_tbl_out,
         x_header_price_att_tbl=> l_header_price_att_tbl_out,
         x_header_adj_att_tbl=> l_header_adj_att_tbl_out,
         x_header_adj_assoc_tbl=> l_header_adj_assoc_tbl_out,
         x_header_scredit_tbl=> l_header_scredit_tbl_out,
         x_header_scredit_val_tbl=> l_header_scredit_val_tbl_out,
         x_line_tbl=> l_line_tbl_out,
         x_line_val_tbl=> l_line_val_tbl_out,
         x_line_adj_tbl=> l_line_adj_tbl_out,
         x_line_adj_val_tbl=> l_line_adj_val_tbl_out,
         x_line_price_att_tbl=> l_line_price_att_tbl_out,
         x_line_adj_att_tbl=> l_line_adj_att_tbl_out,
         x_line_adj_assoc_tbl=> l_line_adj_assoc_tbl_out,
         x_line_scredit_tbl=> l_line_scredit_tbl_out,
         x_line_scredit_val_tbl=> l_line_scredit_val_tbl_out,
         x_lot_serial_tbl=> l_lot_serial_tbl_out,
         x_lot_serial_val_tbl=> l_lot_serial_val_tbl_out,
         x_action_request_tbl=> l_action_request_tbl_out,
         x_return_status=> l_return_status,
         x_msg_count=> l_msg_count,
         x_msg_data=> l_msg_data
      );

      -- CHECK RETURN STATUS
      IF l_return_status = fnd_api.g_ret_sts_success
      THEN
         DBMS_OUTPUT.put_line ('Sales Order Successfully Created');
        
         o_order_number :=l_header_rec_out.order_number;
         o_status       :=l_return_status;

         apps.OM_TAX_UTIL.CALCULATE_TAX( p_header_id     => l_header_rec_out.header_id --IN NUMBER
                                        ,x_return_status => l_return_status --OUT NOCOPY
         ) ;
        
         DBMS_OUTPUT.put_line ('Calcuate Tax');
         COMMIT;
      
         -- Retrieve messages
         lv_msg_data := NULL;

         IF l_order_source_id = 1041 THEN
            BEGIN
               SELECT SUM (( NVL (ool.ordered_quantity, 0) * NVL (ool.unit_selling_price, 0) ))-- + NVL (ool.tax_value, 0) )
                 INTO l_oracle_order_total
                 FROM oe_order_lines_all ool
                WHERE ool.header_id = l_header_rec_out.header_id;
            EXCEPTION
            WHEN OTHERS THEN
               l_oracle_order_total := 0;
            END;
         END IF;

         IF l_oracle_order_total <> p_order_header_rec.offline_order_total THEN
             lv_msg_data := 'Offline order total: '||p_order_header_rec.offline_order_total||' is not matching with order total: '||l_oracle_order_total;
         END IF;

         FOR i IN 1 .. l_msg_count
         LOOP
               Oe_Msg_Pub.get (
                              p_msg_index       => i,
                              p_encoded         => Fnd_Api.G_FALSE,
                              p_data            => l_msg_data,
                              p_msg_index_out   => l_msg_index_out);

               lv_msg_data := lv_msg_data || ':' || SUBSTR (l_msg_data, 1, 100);
         END LOOP;

         IF lv_msg_data IS NOT NULL THEN
            o_message := lv_msg_data;
         END IF;
         
         --TMS#20170313-00308 Start
         IF NVL(p_debug_mode, 'N') = 'T' OR NVL(l_xxwc_oso_debug, 'X') = 'Y' THEN
            DEBUG_LOG( p_order_header_rec     => p_order_header_rec
                      ,p_order_lines_tbl      => p_order_lines_tbl
                      ,p_status               => o_status
                      ,p_order_number         => o_order_number
                      ,p_order_total          => l_oracle_order_total
                      ,p_message              => o_message
                     );
         END IF;
         --TMS#20170313-00308 End

         COMMIT;
      ELSE
         DBMS_OUTPUT.put_line ('Failed to Create Sales Order');
         o_message      := 'Failed to Create Sales Order';
         o_status       :='E';

         -- Retrieve messages
         lv_msg_data := NULL;

         FOR i IN 1 .. l_msg_count
         LOOP
               Oe_Msg_Pub.get (
                              p_msg_index       => i,
                              p_encoded         => Fnd_Api.G_FALSE,
                              p_data            => l_msg_data,
                              p_msg_index_out   => l_msg_index_out);

               lv_msg_data := lv_msg_data || ':' || SUBSTR (l_msg_data, 1, 100);
         END LOOP;

         IF lv_msg_data IS NOT NULL THEN
            o_message := lv_msg_data;
         END IF;
         
         --TMS#20170313-00308 Start
         IF NVL(p_debug_mode, 'N') = 'T' OR NVL(l_xxwc_oso_debug, 'X') = 'Y' THEN
            DEBUG_LOG( p_order_header_rec     => p_order_header_rec
                      ,p_order_lines_tbl      => p_order_lines_tbl
                      ,p_status               => o_status
                      ,p_order_number         => o_order_number
                      ,p_order_total          => l_oracle_order_total
                      ,p_message              => o_message
                     );
         END IF;
         --TMS#20170313-00308 End
         
         ROLLBACK;
      END IF;

      RETURN;
    EXCEPTION
    WHEN OTHERS THEN

      write_output('Error ...'||SQLERRM);

      xxcus_error_pkg.xxcus_error_main_api (
         p_called_from         => 'XXWC_OM_SO_CREATE.XXWC_OM_SO_CREATE'
        ,p_calling             => l_sec
        ,p_request_id          => fnd_global.conc_request_id
        ,p_ora_error_msg       => SQLERRM
        ,p_error_desc          => 'Error creating text file with File Names'
        ,p_distribution_list   => g_dflt_email
        ,p_module              => 'XXWC');
       RETURN;
    END CREATE_SALES_ORDER; --end of procedure

--Ver 2.5 >Start
 /*******************************************************************************
   FUNCTION Name  :   GET_CUSTOMER_INFO_ACTIVE
   Description     :   This FUNCTION wull return customers that have made a purchase in the last 90 days

   Change History  :
   DATE         NAME               ver          Modification
   --------     -------            ----        ------------------------------------------
    2.5        05/22/2017  Rakesh Patel        TMS#20170518-00094-Mobile pricing app version 3.0 plsql api changes
   ******************************************************************************/
FUNCTION GET_CUSTOMER_INFO_ACTIVE ( p_cust_account_code IN VARCHAR2
                                   ,p_customer_name     IN VARCHAR2
                                   ,p_user_nt_id        IN VARCHAR2
                                   ) return sys_refcursor
IS
   l_sec       VARCHAR2 (3000);
   l_condition VARCHAR2 (4000);
   v_rc sys_refcursor;
BEGIN
   l_sec := 'Start GET_CUSTOMER_INFO_ACTIVE';
   
   IF p_user_nt_id IS NULL THEN
       IF p_cust_account_code IS NOT NULL AND p_customer_name IS NOT NULL THEN
           OPEN v_rc FOR 'SELECT  CUST_ACCT.cust_account_id,
                               CUST_ACCT.account_number CUST_ACCOUNT_CODE,
                               PARTY.PARTY_NAME customer_name,
                               LOC.CITY,
                               (CASE
                                   WHEN LENGTH (NVL (LOC.state, LOC.province)) > 2 THEN NULL
                                        ELSE NVL (LOC.state, LOC.province)
                                END)  state_province,
                               LOC.ADDRESS1,
                               LOC.Postal_Code,
                               LOC.ADDRESS2,
                               (DECODE (CUST_ACCT.status,  ''A'', ''Active'',  ''I'', ''Inactive'',  CUST_ACCT.status))
                               cust_account_status ,
                               SUBSTR (xxwc_mv_routines_pkg.get_phone_fax_number (''PHONE'',
                                                          PARTY.party_id,
                                                          NULL --, hcasa.party_site_id
                                                              ,
                                                          ''GEN''),
                                                          1,
                                                            80) main_phone_number,
                               PARTY.email_address main_email,
                               acct_site.cust_acct_site_id hcasa_cust_acct_site_id,
                               SITE.SITE_USE_ID SHIP_TO_SITE_USE_ID,
                               NVL((select site_use_id
                                  from hz_Cust_site_uses_all
                                   where site_use_code = ''BILL_TO''
                                    and cust_acct_site_id = acct_site.cust_acct_site_id
                                    AND status=''A'' ),
                                    (select hcsu.site_use_id
                                  from hz_Cust_site_uses_all hcsu
                                     , hz_Cust_acct_sites_all hcas
                                   where hcsu.site_use_code = ''BILL_TO''
                                    and hcsu.cust_acct_site_id = hcas.cust_acct_site_id
                                    AND hcas.cust_account_id  = acct_site.cust_account_id
                                    AND hcsu.status=''A''
                                    AND hcas.status=''A''
                                    AND ROWNUM = 1 )) BILL_TO_SITE_USE_ID,
                                (select PRIMARY_SALESREP_ID
                                   from hz_Cust_site_uses_all
                                 where site_use_code = ''BILL_TO''
                                  and cust_acct_site_id = acct_site.cust_acct_site_id
                                  AND status=''A'')  PRIMARY_SALESREP_ID
                              ,XXWC_B2B_SO_IB_PKG.get_sales_rep (CUST_ACCT.cust_account_id ) sales_rep_name
                              ,XXWC_OM_SO_CREATE.get_sales_rep_id (CUST_ACCT.cust_account_id ) sales_rep_id
                              ,SITE.location SITE_NAME
                              ,CUST_ACCT.attribute7 AUTH_BUYER
                              ,XXWC_OM_SO_CREATE.get_payment_term_id (CUST_ACCT.cust_account_id) payment_term_id  --payment_term_id
                              ,XXWC_OM_SO_CREATE.get_payment_term_nm (CUST_ACCT.cust_account_id) payment_term_name  --payment_term_name
                              ,DECODE(SUBSTR(cust_acct.attribute4,1,3), ''NAT'', ''Y'', ''N'') national --Rev 2.8
                         FROM
                              HZ_CUST_ACCT_SITES     ACCT_SITE,
                              HZ_PARTY_SITES             PARTY_SITE,
                              HZ_LOCATIONS               LOC,
                              HZ_CUST_SITE_USES_ALL       SITE,
                              HZ_PARTIES           PARTY,
                              HZ_CUST_ACCOUNTS      CUST_ACCT
                        WHERE
                               SITE.SITE_USE_CODE         = ''SHIP_TO''
                          AND   SITE.CUST_ACCT_SITE_ID     = ACCT_SITE.CUST_ACCT_SITE_ID
                          AND   ACCT_SITE.PARTY_SITE_ID    = PARTY_SITE.PARTY_SITE_ID
                          AND   PARTY_SITE.LOCATION_ID     = LOC.LOCATION_ID
                          and    acct_site.status=''A''
                          AND ACCT_SITE.CUST_ACCOUNT_ID=CUST_ACCT.CUST_ACCOUNT_ID
                          AND CUST_ACCT.PARTY_ID=PARTY.PARTY_ID
                          AND CUST_ACCT.status=''A''
                          AND CUST_ACCT.account_number = :p_cust_account_code
                          AND UPPER(PARTY.PARTY_NAME)     LIKE UPPER(:p_customer_name)||''%''
                          AND EXISTS ( SELECT 1
                                         FROM apps.XXWC_OM_ORD_LN_MV xolm
                                        WHERE 1 = 1
                                          AND xolm.SOLD_TO_ORG_ID       = CUST_ACCT.CUST_ACCOUNT_ID
                                          AND xolm.SHIP_TO_ORG_ID       = SITE.SITE_USE_ID
                                          AND xolm.line_creation_date  >= TRUNC(SYSDATE) - 90
                                        )'
                          USING p_cust_account_code, p_customer_name;
                          RETURN v_rc;
       ELSIF p_cust_account_code IS NOT NULL THEN
         open v_rc for 'SELECT  PARTY.PARTY_NAME customer_name,
                               CUST_ACCT.cust_account_id,
                               CUST_ACCT.account_number CUST_ACCOUNT_CODE,
                               LOC.CITY,
                               (CASE
                                   WHEN LENGTH (NVL (LOC.state, LOC.province)) > 2 THEN NULL
                                        ELSE NVL (LOC.state, LOC.province)
                                END)  state_province,
                               LOC.ADDRESS1,
                               LOC.Postal_Code,
                               LOC.ADDRESS2,
                               (DECODE (CUST_ACCT.status,  ''A'', ''Active'',  ''I'', ''Inactive'',  CUST_ACCT.status))
                               cust_account_status ,
                               SUBSTR (xxwc_mv_routines_pkg.get_phone_fax_number (''PHONE'',
                                                          PARTY.party_id,
                                                          NULL --, hcasa.party_site_id
                                                              ,
                                                          ''GEN''),
                                                          1,
                                                            80) main_phone_number,
                               PARTY.email_address main_email,
                               acct_site.cust_acct_site_id hcasa_cust_acct_site_id,
                               SITE.SITE_USE_ID SHIP_TO_SITE_USE_ID,
                               NVL((select site_use_id
                                  from hz_Cust_site_uses_all
                                   where site_use_code = ''BILL_TO''
                                    and cust_acct_site_id = acct_site.cust_acct_site_id
                                    AND status=''A'' ),
                                    (select hcsu.site_use_id
                                  from hz_Cust_site_uses_all hcsu
                                     , hz_Cust_acct_sites_all hcas
                                   where hcsu.site_use_code = ''BILL_TO''
                                    and hcsu.cust_acct_site_id = hcas.cust_acct_site_id
                                    AND hcas.cust_account_id  = acct_site.cust_account_id
                                    AND hcsu.status=''A''
                                    AND hcas.status=''A''
                                    AND ROWNUM = 1 )) BILL_TO_SITE_USE_ID,
                                (select PRIMARY_SALESREP_ID
                                   from hz_Cust_site_uses_all
                                 where site_use_code = ''BILL_TO''
                                  and cust_acct_site_id = acct_site.cust_acct_site_id
                                  AND status=''A''
                                  )  PRIMARY_SALESREP_ID
                              ,XXWC_B2B_SO_IB_PKG.get_sales_rep (CUST_ACCT.cust_account_id ) sales_rep_name
                              ,XXWC_OM_SO_CREATE.get_sales_rep_id (CUST_ACCT.cust_account_id ) sales_rep_id
                              ,SITE.location SITE_NAME
                              ,CUST_ACCT.attribute7 AUTH_BUYER
                              ,XXWC_OM_SO_CREATE.get_payment_term_id (CUST_ACCT.cust_account_id) payment_term_id  --payment_term_id
                              ,XXWC_OM_SO_CREATE.get_payment_term_nm (CUST_ACCT.cust_account_id) payment_term_name  --payment_term_name
                              ,DECODE(SUBSTR(cust_acct.attribute4,1,3), ''NAT'', ''Y'', ''N'') national --Rev 2.8
                         FROM
                              HZ_CUST_ACCT_SITES     ACCT_SITE,
                              HZ_PARTY_SITES             PARTY_SITE,
                              HZ_LOCATIONS               LOC,
                              HZ_CUST_SITE_USES_ALL       SITE,
                              HZ_PARTIES           PARTY,
                              HZ_CUST_ACCOUNTS      CUST_ACCT
                        WHERE
                               SITE.SITE_USE_CODE         = ''SHIP_TO''
                          AND   SITE.CUST_ACCT_SITE_ID     = ACCT_SITE.CUST_ACCT_SITE_ID
                          AND   ACCT_SITE.PARTY_SITE_ID    = PARTY_SITE.PARTY_SITE_ID
                          AND   PARTY_SITE.LOCATION_ID     = LOC.LOCATION_ID
                          and    acct_site.status=''A''
                          AND ACCT_SITE.CUST_ACCOUNT_ID=CUST_ACCT.CUST_ACCOUNT_ID
                          AND CUST_ACCT.PARTY_ID=PARTY.PARTY_ID
                          AND CUST_ACCT.status=''A''
                          AND site.status=''A''
                          AND CUST_ACCT.account_number = :p_cust_account_code
                          AND EXISTS ( SELECT 1
                                         FROM apps.XXWC_OM_ORD_LN_MV xolm
                                        WHERE 1 = 1
                                          AND xolm.SOLD_TO_ORG_ID       = CUST_ACCT.CUST_ACCOUNT_ID
                                          AND xolm.SHIP_TO_ORG_ID       = SITE.SITE_USE_ID
                                          AND xolm.line_creation_date  >= TRUNC(SYSDATE) - 90
                                        )'
                          using p_cust_account_code;
                RETURN v_rc;
       ELSIF p_customer_name IS NOT NULL THEN
       open v_rc for 'SELECT  PARTY.PARTY_NAME customer_name,
                               CUST_ACCT.cust_account_id,
                               CUST_ACCT.account_number CUST_ACCOUNT_CODE,
                               LOC.CITY,
                               (CASE
                                   WHEN LENGTH (NVL (LOC.state, LOC.province)) > 2 THEN NULL
                                        ELSE NVL (LOC.state, LOC.province)
                                END)  state_province,
                               LOC.ADDRESS1,
                               LOC.Postal_Code,
                               LOC.ADDRESS2,
                               (DECODE (CUST_ACCT.status,  ''A'', ''Active'',  ''I'', ''Inactive'',  CUST_ACCT.status))
                               cust_account_status ,
                               SUBSTR (xxwc_mv_routines_pkg.get_phone_fax_number (''PHONE'',
                                                          PARTY.party_id,
                                                          NULL --, hcasa.party_site_id
                                                              ,
                                                          ''GEN''),
                                                          1,
                                                            80) main_phone_number,
                               PARTY.email_address main_email,
                               acct_site.cust_acct_site_id hcasa_cust_acct_site_id,
                               SITE.SITE_USE_ID SHIP_TO_SITE_USE_ID,
                               NVL((select site_use_id
                                  from hz_Cust_site_uses_all
                                   where site_use_code = ''BILL_TO''
                                    and cust_acct_site_id = acct_site.cust_acct_site_id
                                    AND status=''A'' ),
                                    (select hcsu.site_use_id
                                  from hz_Cust_site_uses_all hcsu
                                     , hz_Cust_acct_sites_all hcas
                                   where hcsu.site_use_code = ''BILL_TO''
                                    and hcsu.cust_acct_site_id = hcas.cust_acct_site_id
                                    AND hcas.cust_account_id  = acct_site.cust_account_id
                                    AND hcsu.status=''A''
                                    AND hcas.status=''A''
                                    AND ROWNUM = 1 )) BILL_TO_SITE_USE_ID,
                                (select PRIMARY_SALESREP_ID
                                   from hz_Cust_site_uses_all
                                 where site_use_code = ''BILL_TO''
                                  and cust_acct_site_id = acct_site.cust_acct_site_id
                                  AND status=''A'' )  PRIMARY_SALESREP_ID
                              ,XXWC_B2B_SO_IB_PKG.get_sales_rep (CUST_ACCT.cust_account_id ) sales_rep_name
                              ,XXWC_OM_SO_CREATE.get_sales_rep_id (CUST_ACCT.cust_account_id ) sales_rep_id
                              ,SITE.location SITE_NAME
                              ,CUST_ACCT.attribute7 AUTH_BUYER
                              ,XXWC_OM_SO_CREATE.get_payment_term_id (CUST_ACCT.cust_account_id) payment_term_id  --payment_term_id
                              ,XXWC_OM_SO_CREATE.get_payment_term_nm (CUST_ACCT.cust_account_id) payment_term_name  --payment_term_name
                              ,DECODE(SUBSTR(cust_acct.attribute4,1,3), ''NAT'', ''Y'', ''N'') national --Rev 2.8
                         FROM
                              HZ_CUST_ACCT_SITES     ACCT_SITE,
                              HZ_PARTY_SITES             PARTY_SITE,
                              HZ_LOCATIONS               LOC,
                              HZ_CUST_SITE_USES_ALL       SITE,
                              HZ_PARTIES           PARTY,
                              HZ_CUST_ACCOUNTS      CUST_ACCT
                        WHERE
                               SITE.SITE_USE_CODE         = ''SHIP_TO''
                          AND   SITE.CUST_ACCT_SITE_ID     = ACCT_SITE.CUST_ACCT_SITE_ID
                          AND   ACCT_SITE.PARTY_SITE_ID    = PARTY_SITE.PARTY_SITE_ID
                          AND   PARTY_SITE.LOCATION_ID     = LOC.LOCATION_ID
                          and    acct_site.status=''A''
                          AND ACCT_SITE.CUST_ACCOUNT_ID=CUST_ACCT.CUST_ACCOUNT_ID
                          AND CUST_ACCT.PARTY_ID=PARTY.PARTY_ID
                          AND CUST_ACCT.status=''A''
                          AND site.status=''A''
                          AND UPPER(PARTY.PARTY_NAME)     LIKE UPPER(:p_customer_name)||''%''
                          AND EXISTS ( SELECT 1
                                         FROM apps.XXWC_OM_ORD_LN_MV xolm
                                        WHERE 1 = 1
                                          AND xolm.SOLD_TO_ORG_ID       = CUST_ACCT.CUST_ACCOUNT_ID
                                          AND xolm.SHIP_TO_ORG_ID       = SITE.SITE_USE_ID
                                          AND xolm.line_creation_date  >= TRUNC(SYSDATE) - 90
                                        )'
                          using p_customer_name;
                          RETURN v_rc;
         END IF;                 
     ELSE -- with NT id
        IF p_cust_account_code IS NOT NULL AND p_customer_name IS NOT NULL THEN
            open v_rc for 'SELECT  CUST_ACCT.cust_account_id,
                               CUST_ACCT.account_number CUST_ACCOUNT_CODE,
                               PARTY.PARTY_NAME customer_name,
                               LOC.CITY,
                               (CASE
                                   WHEN LENGTH (NVL (LOC.state, LOC.province)) > 2 THEN NULL
                                        ELSE NVL (LOC.state, LOC.province)
                                END)  state_province,
                               LOC.ADDRESS1,
                               LOC.Postal_Code,
                               LOC.ADDRESS2,
                               (DECODE (CUST_ACCT.status,  ''A'', ''Active'',  ''I'', ''Inactive'',  CUST_ACCT.status))
                               cust_account_status ,
                               SUBSTR (xxwc_mv_routines_pkg.get_phone_fax_number (''PHONE'',
                                                          PARTY.party_id,
                                                          NULL --, hcasa.party_site_id
                                                              ,
                                                          ''GEN''),
                                                          1,
                                                            80) main_phone_number,
                               PARTY.email_address main_email,
                               acct_site.cust_acct_site_id hcasa_cust_acct_site_id,
                               SITE.SITE_USE_ID SHIP_TO_SITE_USE_ID,
                               NVL((select site_use_id
                                  from hz_Cust_site_uses_all
                                   where site_use_code = ''BILL_TO''
                                    and cust_acct_site_id = acct_site.cust_acct_site_id
                                    AND status=''A'' ),
                                    (select hcsu.site_use_id
                                  from hz_Cust_site_uses_all hcsu
                                     , hz_Cust_acct_sites_all hcas
                                   where hcsu.site_use_code = ''BILL_TO''
                                    and hcsu.cust_acct_site_id = hcas.cust_acct_site_id
                                    AND hcas.cust_account_id  = acct_site.cust_account_id
                                    AND hcsu.status=''A''
                                    AND hcas.status=''A''
                                    AND ROWNUM = 1 )) BILL_TO_SITE_USE_ID,
                                (select PRIMARY_SALESREP_ID
                                   from hz_Cust_site_uses_all
                                 where site_use_code = ''BILL_TO''
                                  and cust_acct_site_id = acct_site.cust_acct_site_id
                                  AND status=''A'')  PRIMARY_SALESREP_ID
                              ,XXWC_B2B_SO_IB_PKG.get_sales_rep (CUST_ACCT.cust_account_id ) sales_rep_name
                              ,XXWC_OM_SO_CREATE.get_sales_rep_id (CUST_ACCT.cust_account_id ) sales_rep_id
                              ,SITE.location SITE_NAME
                              ,CUST_ACCT.attribute7 AUTH_BUYER
                              ,XXWC_OM_SO_CREATE.get_payment_term_id (CUST_ACCT.cust_account_id) payment_term_id  --payment_term_id
                              ,XXWC_OM_SO_CREATE.get_payment_term_nm (CUST_ACCT.cust_account_id) payment_term_name  --payment_term_name
                              ,DECODE(SUBSTR(cust_acct.attribute4,1,3), ''NAT'', ''Y'', ''N'') national --Rev 2.8
                         FROM
                              HZ_CUST_ACCT_SITES     ACCT_SITE,
                              HZ_PARTY_SITES             PARTY_SITE,
                              HZ_LOCATIONS               LOC,
                              HZ_CUST_SITE_USES_ALL       SITE,
                              HZ_PARTIES           PARTY,
                              HZ_CUST_ACCOUNTS      CUST_ACCT
                        WHERE
                               SITE.SITE_USE_CODE         = ''SHIP_TO''
                          AND   SITE.CUST_ACCT_SITE_ID     = ACCT_SITE.CUST_ACCT_SITE_ID
                          AND   ACCT_SITE.PARTY_SITE_ID    = PARTY_SITE.PARTY_SITE_ID
                          AND   PARTY_SITE.LOCATION_ID     = LOC.LOCATION_ID
                          and    acct_site.status=''A''
                          AND ACCT_SITE.CUST_ACCOUNT_ID=CUST_ACCT.CUST_ACCOUNT_ID
                          AND CUST_ACCT.PARTY_ID=PARTY.PARTY_ID
                          AND CUST_ACCT.status=''A''
                          AND site.status=''A''
                          AND CUST_ACCT.account_number = :p_cust_account_code
                          AND UPPER(PARTY.PARTY_NAME)     LIKE UPPER(:p_customer_name)||''%''
                          AND EXISTS ( SELECT 1
                                         FROM apps.jtf_rs_salesreps           jrs
                                            , apps.jtf_rs_resource_extns      jrre
                                        WHERE 1 = 1
                                          AND jrs.salesrep_id          = SITE.primary_salesrep_id
                                          AND jrs.org_id               = ACCT_SITE.org_id
                                          AND jrs.resource_id          = jrre.resource_id
                                          AND jrre.user_name           = :p_user_nt_id
                                          AND sysdate BETWEEN jrre.start_Date_active and NVL(jrre.end_date_active, sysdate+1)
                                          AND rownum=1  
                                          --Rev 2.7 < Start
                                        UNION  
                                         SELECT 1                                            
                                           FROM xxwc.xxwc_sr_salesreps sr,
                                                xxwc.xxwc_sr_salesrep_hierarchy srh,
                                                xxwc.xxwc_sr_salesrep_hierarchy srmh
                                          WHERE sr.salesrep_id = SITE.primary_salesrep_id
                                            AND sr.salesrep_id = srh.sr_id
                                            AND sr.eeid = srmh.mgr_eeid(+)
                                            AND srh.mgr_eeid = XXWC_OM_SO_CREATE.get_eeid_number(upper(:p_user_nt_id))
                                            AND sr.eeid(+) != XXWC_OM_SO_CREATE.get_eeid_number(upper(:p_user_nt_id))
                                            AND sr.ntid IS NOT NULL
                                         --Rev 2.7 < End
                                         --Rev 2.8 < Start
                                        UNION
                                         SELECT 1
                                           FROM fnd_lookup_values  flv
                                          WHERE flv.lookup_type  = ''XXWC_CHECKIT_SUPER_USERS''
                                            AND flv.lookup_code  = UPPER(:p_user_nt_id)
                                            AND flv.enabled_flag = ''Y''
                                            AND TRUNC(SYSDATE) BETWEEN TRUNC(flv.start_date_active) AND TRUNC(NVL(flv.end_date_active,SYSDATE))
                                        --Rev 2.8 < End     
                                        )
                          AND EXISTS ( SELECT 1
                                         FROM apps.XXWC_OM_ORD_LN_MV xolm
                                        WHERE 1 = 1
                                          AND xolm.SOLD_TO_ORG_ID       = CUST_ACCT.CUST_ACCOUNT_ID
                                          AND xolm.SHIP_TO_ORG_ID       = SITE.SITE_USE_ID
                                          AND xolm.line_creation_date  >= TRUNC(SYSDATE) - 90
                                        )'
                          using p_cust_account_code, p_customer_name, p_user_nt_id, p_user_nt_id, p_user_nt_id, p_user_nt_id; --Rev 2.8
             RETURN v_rc;
     
       ELSIF p_cust_account_code IS NOT NULL THEN
          open v_rc for 'SELECT  PARTY.PARTY_NAME customer_name,
                               CUST_ACCT.cust_account_id,
                               CUST_ACCT.account_number CUST_ACCOUNT_CODE,
                               LOC.CITY,
                               (CASE
                                   WHEN LENGTH (NVL (LOC.state, LOC.province)) > 2 THEN NULL
                                        ELSE NVL (LOC.state, LOC.province)
                                END)  state_province,
                               LOC.ADDRESS1,
                               LOC.Postal_Code,
                               LOC.ADDRESS2,
                               (DECODE (CUST_ACCT.status,  ''A'', ''Active'',  ''I'', ''Inactive'',  CUST_ACCT.status))
                               cust_account_status ,
                               SUBSTR (xxwc_mv_routines_pkg.get_phone_fax_number (''PHONE'',
                                                          PARTY.party_id,
                                                          NULL --, hcasa.party_site_id
                                                              ,
                                                          ''GEN''),
                                                          1,
                                                            80) main_phone_number,
                               PARTY.email_address main_email,
                               acct_site.cust_acct_site_id hcasa_cust_acct_site_id,
                               SITE.SITE_USE_ID SHIP_TO_SITE_USE_ID,
                               NVL((select site_use_id
                                  from hz_Cust_site_uses_all
                                   where site_use_code = ''BILL_TO''
                                    and cust_acct_site_id = acct_site.cust_acct_site_id
                                    AND status=''A'' ),
                                    (select hcsu.site_use_id
                                  from hz_Cust_site_uses_all hcsu
                                     , hz_Cust_acct_sites_all hcas
                                   where hcsu.site_use_code = ''BILL_TO''
                                    and hcsu.cust_acct_site_id = hcas.cust_acct_site_id
                                    AND hcas.cust_account_id  = acct_site.cust_account_id
                                    AND hcsu.status=''A''
                                    AND hcas.status=''A''
                                    AND ROWNUM = 1 )) BILL_TO_SITE_USE_ID,
                                (select PRIMARY_SALESREP_ID
                                   from hz_Cust_site_uses_all
                                 where site_use_code = ''BILL_TO''
                                  and cust_acct_site_id = acct_site.cust_acct_site_id
                                  AND status=''A''
                                  )  PRIMARY_SALESREP_ID
                              ,XXWC_B2B_SO_IB_PKG.get_sales_rep (CUST_ACCT.cust_account_id ) sales_rep_name
                              ,XXWC_OM_SO_CREATE.get_sales_rep_id (CUST_ACCT.cust_account_id ) sales_rep_id
                              ,SITE.location SITE_NAME
                              ,CUST_ACCT.attribute7 AUTH_BUYER
                              ,XXWC_OM_SO_CREATE.get_payment_term_id (CUST_ACCT.cust_account_id) payment_term_id  --payment_term_id
                              ,XXWC_OM_SO_CREATE.get_payment_term_nm (CUST_ACCT.cust_account_id) payment_term_name  --payment_term_name
                              ,DECODE(SUBSTR(cust_acct.attribute4,1,3), ''NAT'', ''Y'', ''N'') national --Rev 2.8
                         FROM
                              HZ_CUST_ACCT_SITES     ACCT_SITE,
                              HZ_PARTY_SITES             PARTY_SITE,
                              HZ_LOCATIONS               LOC,
                              HZ_CUST_SITE_USES_ALL       SITE,
                              HZ_PARTIES           PARTY,
                              HZ_CUST_ACCOUNTS      CUST_ACCT
                        WHERE
                               SITE.SITE_USE_CODE         = ''SHIP_TO''
                          AND   SITE.CUST_ACCT_SITE_ID     = ACCT_SITE.CUST_ACCT_SITE_ID
                          AND   ACCT_SITE.PARTY_SITE_ID    = PARTY_SITE.PARTY_SITE_ID
                          AND   PARTY_SITE.LOCATION_ID     = LOC.LOCATION_ID
                          and    acct_site.status=''A''
                          AND ACCT_SITE.CUST_ACCOUNT_ID=CUST_ACCT.CUST_ACCOUNT_ID
                          AND CUST_ACCT.PARTY_ID=PARTY.PARTY_ID
                          AND CUST_ACCT.status=''A''
                          AND site.status=''A''
                          AND CUST_ACCT.account_number = :p_cust_account_code
                          AND EXISTS ( SELECT 1
                                         FROM apps.jtf_rs_salesreps           jrs
                                            , apps.jtf_rs_resource_extns      jrre
                                        WHERE 1 = 1
                                          AND jrs.salesrep_id          = SITE.primary_salesrep_id
                                          AND jrs.org_id               = ACCT_SITE.org_id
                                          AND jrs.resource_id          = jrre.resource_id
                                          AND jrre.user_name           = :p_user_nt_id
                                          AND sysdate BETWEEN jrre.start_Date_active and NVL(jrre.end_date_active, sysdate+1)
                                          AND rownum=1
                                          --Rev 2.7 < Start
                                      UNION  
                                         SELECT 1                                            
                                           FROM xxwc.xxwc_sr_salesreps sr,
                                                xxwc.xxwc_sr_salesrep_hierarchy srh,
                                                xxwc.xxwc_sr_salesrep_hierarchy srmh
                                          WHERE sr.salesrep_id = SITE.primary_salesrep_id
                                            AND sr.salesrep_id = srh.sr_id
                                            AND sr.eeid = srmh.mgr_eeid(+)
                                            AND srh.mgr_eeid = XXWC_OM_SO_CREATE.get_eeid_number(upper(:p_user_nt_id))
                                            AND sr.eeid(+) != XXWC_OM_SO_CREATE.get_eeid_number(upper(:p_user_nt_id))
                                            AND sr.ntid IS NOT NULL
                                          --Rev 2.7 > End 
                                          --Rev 2.8 < Start
                                        UNION
                                         SELECT 1
                                           FROM fnd_lookup_values  flv
                                          WHERE flv.lookup_type  = ''XXWC_CHECKIT_SUPER_USERS''
                                            AND flv.lookup_code  = UPPER(:p_user_nt_id)
                                            AND flv.enabled_flag = ''Y''
                                            AND TRUNC(SYSDATE) BETWEEN TRUNC(flv.start_date_active) AND TRUNC(NVL(flv.end_date_active,SYSDATE))
                                        --Rev 2.8 < End     

                                        )
                          AND EXISTS ( SELECT 1
                                         FROM apps.XXWC_OM_ORD_LN_MV xolm
                                        WHERE 1 = 1
                                          AND xolm.SOLD_TO_ORG_ID       = CUST_ACCT.CUST_ACCOUNT_ID
                                          AND xolm.SHIP_TO_ORG_ID       = SITE.SITE_USE_ID
                                          AND xolm.line_creation_date  >= TRUNC(SYSDATE) - 90
                                        )'
                          using p_cust_account_code, p_user_nt_id, p_user_nt_id, p_user_nt_id, p_user_nt_id; --Rev 2.8 
                RETURN v_rc;
       ELSIF p_customer_name IS NOT NULL THEN
       open v_rc for 'SELECT  PARTY.PARTY_NAME customer_name,
                               CUST_ACCT.cust_account_id,
                               CUST_ACCT.account_number CUST_ACCOUNT_CODE,
                               LOC.CITY,
                               (CASE
                                   WHEN LENGTH (NVL (LOC.state, LOC.province)) > 2 THEN NULL
                                        ELSE NVL (LOC.state, LOC.province)
                                END)  state_province,
                               LOC.ADDRESS1,
                               LOC.Postal_Code,
                               LOC.ADDRESS2,
                               (DECODE (CUST_ACCT.status,  ''A'', ''Active'',  ''I'', ''Inactive'',  CUST_ACCT.status))
                               cust_account_status ,
                               SUBSTR (xxwc_mv_routines_pkg.get_phone_fax_number (''PHONE'',
                                                          PARTY.party_id,
                                                          NULL --, hcasa.party_site_id
                                                              ,
                                                          ''GEN''),
                                                          1,
                                                            80) main_phone_number,
                               PARTY.email_address main_email,
                               acct_site.cust_acct_site_id hcasa_cust_acct_site_id,
                               SITE.SITE_USE_ID SHIP_TO_SITE_USE_ID,
                               NVL((select site_use_id
                                  from hz_Cust_site_uses_all
                                   where site_use_code = ''BILL_TO''
                                    and cust_acct_site_id = acct_site.cust_acct_site_id
                                    AND status=''A'' ),
                                    (select hcsu.site_use_id
                                  from hz_Cust_site_uses_all hcsu
                                     , hz_Cust_acct_sites_all hcas
                                   where hcsu.site_use_code = ''BILL_TO''
                                    and hcsu.cust_acct_site_id = hcas.cust_acct_site_id
                                    AND hcas.cust_account_id  = acct_site.cust_account_id
                                    AND hcsu.status=''A''
                                    AND hcas.status=''A''
                                    AND ROWNUM = 1 )) BILL_TO_SITE_USE_ID,
                                (select PRIMARY_SALESREP_ID
                                   from hz_Cust_site_uses_all
                                 where site_use_code = ''BILL_TO''
                                  and cust_acct_site_id = acct_site.cust_acct_site_id
                                  AND status=''A'' )  PRIMARY_SALESREP_ID
                              ,XXWC_B2B_SO_IB_PKG.get_sales_rep (CUST_ACCT.cust_account_id ) sales_rep_name
                              ,XXWC_OM_SO_CREATE.get_sales_rep_id (CUST_ACCT.cust_account_id ) sales_rep_id
                              ,SITE.location SITE_NAME
                              ,CUST_ACCT.attribute7 AUTH_BUYER
                              ,XXWC_OM_SO_CREATE.get_payment_term_id (CUST_ACCT.cust_account_id) payment_term_id  --payment_term_id
                              ,XXWC_OM_SO_CREATE.get_payment_term_nm (CUST_ACCT.cust_account_id) payment_term_name  --payment_term_name
                              ,DECODE(SUBSTR(cust_acct.attribute4,1,3), ''NAT'', ''Y'', ''N'') national --Rev 2.8
                         FROM
                              HZ_CUST_ACCT_SITES     ACCT_SITE,
                              HZ_PARTY_SITES             PARTY_SITE,
                              HZ_LOCATIONS               LOC,
                              HZ_CUST_SITE_USES_ALL       SITE,
                              HZ_PARTIES           PARTY,
                              HZ_CUST_ACCOUNTS      CUST_ACCT
                        WHERE
                               SITE.SITE_USE_CODE         = ''SHIP_TO''
                          AND   SITE.CUST_ACCT_SITE_ID     = ACCT_SITE.CUST_ACCT_SITE_ID
                          AND   ACCT_SITE.PARTY_SITE_ID    = PARTY_SITE.PARTY_SITE_ID
                          AND   PARTY_SITE.LOCATION_ID     = LOC.LOCATION_ID
                          and    acct_site.status=''A''
                          AND ACCT_SITE.CUST_ACCOUNT_ID=CUST_ACCT.CUST_ACCOUNT_ID
                          AND CUST_ACCT.PARTY_ID=PARTY.PARTY_ID
                          AND CUST_ACCT.status=''A''
                          AND site.status=''A''
                          AND UPPER(party.party_name)     LIKE UPPER(:p_customer_name)||''%''
                          AND EXISTS ( SELECT 1
                                         FROM apps.jtf_rs_salesreps           jrs
                                            , apps.jtf_rs_resource_extns      jrre
                                        WHERE 1 = 1
                                          AND jrs.salesrep_id          = SITE.primary_salesrep_id
                                          AND jrs.org_id               = ACCT_SITE.org_id
                                          AND jrs.resource_id          = jrre.resource_id
                                          AND jrre.user_name           = :p_user_nt_id
                                          AND sysdate BETWEEN jrre.start_Date_active and NVL(jrre.end_date_active, sysdate+1)
                                          AND rownum=1
                                          --Rev 2.7 < Start
                                        UNION  
                                         SELECT 1                                            
                                           FROM xxwc.xxwc_sr_salesreps sr,
                                                xxwc.xxwc_sr_salesrep_hierarchy srh,
                                                xxwc.xxwc_sr_salesrep_hierarchy srmh
                                          WHERE sr.salesrep_id = SITE.primary_salesrep_id
                                            AND sr.salesrep_id = srh.sr_id
                                            AND sr.eeid = srmh.mgr_eeid(+)
                                            AND srh.mgr_eeid = XXWC_OM_SO_CREATE.get_eeid_number(upper(:p_user_nt_id))
                                            AND sr.eeid(+) != XXWC_OM_SO_CREATE.get_eeid_number(upper(:p_user_nt_id))
                                            AND sr.ntid IS NOT NULL  
                                         --Rev 2.7 > End   
                                         --Rev 2.8 < Start
                                        UNION
                                         SELECT 1
                                           FROM fnd_lookup_values  flv
                                          WHERE flv.lookup_type  = ''XXWC_CHECKIT_SUPER_USERS''
                                            AND flv.lookup_code  = UPPER(:p_user_nt_id)
                                            AND flv.enabled_flag = ''Y''
                                            AND TRUNC(SYSDATE) BETWEEN TRUNC(flv.start_date_active) AND TRUNC(NVL(flv.end_date_active,SYSDATE))
                                        --Rev 2.8 < End     
                                        )
                           AND EXISTS ( SELECT 1
                                         FROM apps.XXWC_OM_ORD_LN_MV xolm
                                        WHERE 1 = 1
                                          AND xolm.SOLD_TO_ORG_ID       = CUST_ACCT.CUST_ACCOUNT_ID
                                          AND xolm.SHIP_TO_ORG_ID       = SITE.SITE_USE_ID
                                          AND xolm.line_creation_date  >= TRUNC(SYSDATE) - 90
                                        )'
                          using p_customer_name, p_user_nt_id, p_user_nt_id, p_user_nt_id, p_user_nt_id; --Rev 2.8
                          RETURN v_rc;
       END IF;  
     END IF; --p_user_nt_id                   
  

   l_sec := 'End GET_CUSTOMER_INFO_ACTIVE';
EXCEPTION
   WHEN OTHERS
   THEN
      RETURN NULL;
   END GET_CUSTOMER_INFO_ACTIVE; --GET_CUSTOMER_INFO_ACTIVE
  --Ver 2.5  < End

   FUNCTION GET_CUSTOMER_INFO_RS( p_cust_account_code IN VARCHAR2
                                 ,p_customer_name     IN VARCHAR2
                                 ,p_user_nt_id        IN VARCHAR2  DEFAULT NULL  --Added for Rev 2.2
                                 ,p_filter            IN VARCHAR2  DEFAULT NULL  --Added for Rev 2.5
                                 ) return sys_refcursor IS
    v_rc sys_refcursor;
   BEGIN
      mo_global.set_policy_context('S',162);

      IF p_cust_account_code IS NOT NULL AND p_customer_name IS NOT NULL THEN
         IF p_user_nt_id IS NULL THEN-- Added for rev#2.2
            --Added for Rev 2.5 > Start
            IF UPPER(NVL(p_filter, 'XXX')) = 'PURCHASES' THEN  --Added for Rev 2.5
               v_rc := GET_CUSTOMER_INFO_ACTIVE(  p_cust_account_code => p_cust_account_code
                                                 ,p_customer_name     => p_customer_name
                                                 ,p_user_nt_id        => NULL
                                               ); 
               RETURN v_rc;                                 
            ELSE  --Added for Rev 2.5 > End 
               open v_rc for 'SELECT  CUST_ACCT.cust_account_id,
                               CUST_ACCT.account_number CUST_ACCOUNT_CODE,
                               PARTY.PARTY_NAME customer_name,
                               LOC.CITY,
                               (CASE
                                   WHEN LENGTH (NVL (LOC.state, LOC.province)) > 2 THEN NULL
                                        ELSE NVL (LOC.state, LOC.province)
                                END)  state_province,
                               LOC.ADDRESS1,
                               LOC.Postal_Code,
                               LOC.ADDRESS2,
                               (DECODE (CUST_ACCT.status,  ''A'', ''Active'',  ''I'', ''Inactive'',  CUST_ACCT.status))
                               cust_account_status ,
                               SUBSTR (xxwc_mv_routines_pkg.get_phone_fax_number (''PHONE'',
                                                          PARTY.party_id,
                                                          NULL --, hcasa.party_site_id
                                                              ,
                                                          ''GEN''),
                                                          1,
                                                            80) main_phone_number,
                               PARTY.email_address main_email,
                               acct_site.cust_acct_site_id hcasa_cust_acct_site_id,
                               SITE.SITE_USE_ID SHIP_TO_SITE_USE_ID,
                               NVL((select site_use_id
                                  from hz_Cust_site_uses_all
                                   where site_use_code = ''BILL_TO''
                                    and cust_acct_site_id = acct_site.cust_acct_site_id
                                    AND status=''A'' ),
                                    (select hcsu.site_use_id
                                  from hz_Cust_site_uses_all hcsu
                                     , hz_Cust_acct_sites_all hcas
                                   where hcsu.site_use_code = ''BILL_TO''
                                    and hcsu.cust_acct_site_id = hcas.cust_acct_site_id
                                    AND hcas.cust_account_id  = acct_site.cust_account_id
                                    AND hcsu.status=''A''
                                    AND hcas.status=''A''
                                    AND ROWNUM = 1 )) BILL_TO_SITE_USE_ID,
                                (select PRIMARY_SALESREP_ID
                                   from hz_Cust_site_uses_all
                                 where site_use_code = ''BILL_TO''
                                  and cust_acct_site_id = acct_site.cust_acct_site_id
                                  AND status=''A'')  PRIMARY_SALESREP_ID
                              ,XXWC_B2B_SO_IB_PKG.get_sales_rep (CUST_ACCT.cust_account_id ) sales_rep_name
                              ,XXWC_OM_SO_CREATE.get_sales_rep_id (CUST_ACCT.cust_account_id ) sales_rep_id
                              ,SITE.location SITE_NAME
                              ,CUST_ACCT.attribute7 AUTH_BUYER
                              ,XXWC_OM_SO_CREATE.get_payment_term_id (CUST_ACCT.cust_account_id) payment_term_id  --payment_term_id
                              ,XXWC_OM_SO_CREATE.get_payment_term_nm (CUST_ACCT.cust_account_id) payment_term_name  --payment_term_name
                              ,DECODE(SUBSTR(cust_acct.attribute4,1,3), ''NAT'', ''Y'', ''N'') national --Rev 2.8
                         FROM
                              HZ_CUST_ACCT_SITES     ACCT_SITE,
                              HZ_PARTY_SITES             PARTY_SITE,
                              HZ_LOCATIONS               LOC,
                              HZ_CUST_SITE_USES_ALL       SITE,
                              HZ_PARTIES           PARTY,
                              HZ_CUST_ACCOUNTS      CUST_ACCT
                        WHERE
                               SITE.SITE_USE_CODE         = ''SHIP_TO''
                          AND   SITE.CUST_ACCT_SITE_ID     = ACCT_SITE.CUST_ACCT_SITE_ID
                          AND   ACCT_SITE.PARTY_SITE_ID    = PARTY_SITE.PARTY_SITE_ID
                          AND   PARTY_SITE.LOCATION_ID     = LOC.LOCATION_ID
                          and    acct_site.status=''A''
                          AND ACCT_SITE.CUST_ACCOUNT_ID=CUST_ACCT.CUST_ACCOUNT_ID
                          AND CUST_ACCT.PARTY_ID=PARTY.PARTY_ID
                          AND CUST_ACCT.status=''A''
                          AND site.status=''A''
                          AND CUST_ACCT.account_number = :p_cust_account_code
                          AND UPPER(PARTY.PARTY_NAME)     LIKE UPPER(:p_customer_name)||''%'''
                     USING p_cust_account_code, p_customer_name;
                     RETURN v_rc;     
           END IF; --Added for Rev 2.5 
         -- Added for rev#2.2 > Start                 
         ELSE
            --Added for Rev 2.5 > Start
            IF UPPER(NVL(p_filter, 'XXX')) = 'PURCHASES' THEN  --Added for Rev 2.5
               v_rc := GET_CUSTOMER_INFO_ACTIVE(  p_cust_account_code => p_cust_account_code
                                                 ,p_customer_name     => p_customer_name
                                                 ,p_user_nt_id        => p_user_nt_id
                                               ); 
               RETURN v_rc;                                
            ELSE  --Added for Rev 2.5 > End   
               open v_rc for 'SELECT  CUST_ACCT.cust_account_id,
                               CUST_ACCT.account_number CUST_ACCOUNT_CODE,
                               PARTY.PARTY_NAME customer_name,
                               LOC.CITY,
                               (CASE
                                   WHEN LENGTH (NVL (LOC.state, LOC.province)) > 2 THEN NULL
                                        ELSE NVL (LOC.state, LOC.province)
                                END)  state_province,
                               LOC.ADDRESS1,
                               LOC.Postal_Code,
                               LOC.ADDRESS2,
                               (DECODE (CUST_ACCT.status,  ''A'', ''Active'',  ''I'', ''Inactive'',  CUST_ACCT.status))
                               cust_account_status ,
                               SUBSTR (xxwc_mv_routines_pkg.get_phone_fax_number (''PHONE'',
                                                          PARTY.party_id,
                                                          NULL --, hcasa.party_site_id
                                                              ,
                                                          ''GEN''),
                                                          1,
                                                            80) main_phone_number,
                               PARTY.email_address main_email,
                               acct_site.cust_acct_site_id hcasa_cust_acct_site_id,
                               SITE.SITE_USE_ID SHIP_TO_SITE_USE_ID,
                               NVL((select site_use_id
                                  from hz_Cust_site_uses_all
                                   where site_use_code = ''BILL_TO''
                                    and cust_acct_site_id = acct_site.cust_acct_site_id
                                    AND status=''A'' ),
                                    (select hcsu.site_use_id
                                  from hz_Cust_site_uses_all hcsu
                                     , hz_Cust_acct_sites_all hcas
                                   where hcsu.site_use_code = ''BILL_TO''
                                    and hcsu.cust_acct_site_id = hcas.cust_acct_site_id
                                    AND hcas.cust_account_id  = acct_site.cust_account_id
                                    AND hcsu.status=''A''
                                    AND hcas.status=''A''
                                    AND ROWNUM = 1 )) BILL_TO_SITE_USE_ID,
                                (select PRIMARY_SALESREP_ID
                                   from hz_Cust_site_uses_all
                                 where site_use_code = ''BILL_TO''
                                  and cust_acct_site_id = acct_site.cust_acct_site_id
                                  AND status=''A'')  PRIMARY_SALESREP_ID
                              ,XXWC_B2B_SO_IB_PKG.get_sales_rep (CUST_ACCT.cust_account_id ) sales_rep_name
                              ,XXWC_OM_SO_CREATE.get_sales_rep_id (CUST_ACCT.cust_account_id ) sales_rep_id
                              ,SITE.location SITE_NAME
                              ,CUST_ACCT.attribute7 AUTH_BUYER
                              ,XXWC_OM_SO_CREATE.get_payment_term_id (CUST_ACCT.cust_account_id) payment_term_id  --payment_term_id
                              ,XXWC_OM_SO_CREATE.get_payment_term_nm (CUST_ACCT.cust_account_id) payment_term_name  --payment_term_name
                              ,DECODE(SUBSTR(cust_acct.attribute4,1,3), ''NAT'', ''Y'', ''N'') national --Rev 2.8
                         FROM
                              HZ_CUST_ACCT_SITES     ACCT_SITE,
                              HZ_PARTY_SITES             PARTY_SITE,
                              HZ_LOCATIONS               LOC,
                              HZ_CUST_SITE_USES_ALL       SITE,
                              HZ_PARTIES           PARTY,
                              HZ_CUST_ACCOUNTS      CUST_ACCT
                        WHERE
                               SITE.SITE_USE_CODE         = ''SHIP_TO''
                          AND   SITE.CUST_ACCT_SITE_ID     = ACCT_SITE.CUST_ACCT_SITE_ID
                          AND   ACCT_SITE.PARTY_SITE_ID    = PARTY_SITE.PARTY_SITE_ID
                          AND   PARTY_SITE.LOCATION_ID     = LOC.LOCATION_ID
                          and    acct_site.status=''A''
                          AND ACCT_SITE.CUST_ACCOUNT_ID=CUST_ACCT.CUST_ACCOUNT_ID
                          AND CUST_ACCT.PARTY_ID=PARTY.PARTY_ID
                          AND CUST_ACCT.status=''A''
                          AND site.status=''A''
                          AND CUST_ACCT.account_number = :p_cust_account_code
                          AND UPPER(PARTY.PARTY_NAME)     LIKE UPPER(:p_customer_name)||''%''
                          AND EXISTS ( SELECT 1
                                         FROM apps.jtf_rs_salesreps           jrs
                                            , apps.jtf_rs_resource_extns      jrre
                                        WHERE 1 = 1
                                          AND jrs.salesrep_id          = SITE.primary_salesrep_id
                                          AND jrs.org_id               = ACCT_SITE.org_id
                                          AND jrs.resource_id          = jrre.resource_id
                                          AND jrre.user_name           = :p_user_nt_id
                                          AND sysdate BETWEEN jrre.start_Date_active and NVL(jrre.end_date_active, sysdate+1)
                                          AND rownum=1
                                          --Rev 2.7 < Start
                                        UNION  
                                         SELECT 1                                            
                                           FROM xxwc.xxwc_sr_salesreps sr,
                                                xxwc.xxwc_sr_salesrep_hierarchy srh,
                                                xxwc.xxwc_sr_salesrep_hierarchy srmh
                                          WHERE sr.salesrep_id = SITE.primary_salesrep_id
                                            AND sr.salesrep_id = srh.sr_id
                                            AND sr.eeid = srmh.mgr_eeid(+)
                                            AND srh.mgr_eeid = XXWC_OM_SO_CREATE.get_eeid_number(upper(:p_user_nt_id))
                                            AND sr.eeid(+) != XXWC_OM_SO_CREATE.get_eeid_number(upper(:p_user_nt_id))
                                            AND sr.ntid IS NOT NULL    
                                          --Rev 2.7 > End   
                                          --Rev 2.8 < Start
                                        UNION
                                         SELECT 1
                                           FROM fnd_lookup_values  flv
                                          WHERE flv.lookup_type  = ''XXWC_CHECKIT_SUPER_USERS''
                                            AND flv.lookup_code  = UPPER(:p_user_nt_id)
                                            AND flv.enabled_flag = ''Y''
                                            AND TRUNC(SYSDATE) BETWEEN TRUNC(flv.start_date_active) AND TRUNC(NVL(flv.end_date_active,SYSDATE))
                                        --Rev 2.8 < End     
                                        )'
                          using p_cust_account_code, p_customer_name, p_user_nt_id, p_user_nt_id, p_user_nt_id, p_user_nt_id; --Rev 2.8
             RETURN v_rc;
            END IF; --Added for Rev 2.5   
         END IF; 
         -- Added for rev#2.2 < End  
      ELSIF p_cust_account_code IS NOT NULL THEN
         IF p_user_nt_id IS NULL THEN-- Added for rev#2.2
            --Added for Rev 2.5 > Start
            IF UPPER(NVL(p_filter, 'XXX')) = 'PURCHASES' THEN  --Added for Rev 2.5
               v_rc := GET_CUSTOMER_INFO_ACTIVE(  p_cust_account_code => p_cust_account_code
                                                 ,p_customer_name     => NULL
                                                 ,p_user_nt_id        => NULL
                                               ); 
               RETURN v_rc;                                   
            ELSE  --Added for Rev 2.5 > End 
               open v_rc for 'SELECT  PARTY.PARTY_NAME customer_name,
                               CUST_ACCT.cust_account_id,
                               CUST_ACCT.account_number CUST_ACCOUNT_CODE,
                               LOC.CITY,
                               (CASE
                                   WHEN LENGTH (NVL (LOC.state, LOC.province)) > 2 THEN NULL
                                        ELSE NVL (LOC.state, LOC.province)
                                END)  state_province,
                               LOC.ADDRESS1,
                               LOC.Postal_Code,
                               LOC.ADDRESS2,
                               (DECODE (CUST_ACCT.status,  ''A'', ''Active'',  ''I'', ''Inactive'',  CUST_ACCT.status))
                               cust_account_status ,
                               SUBSTR (xxwc_mv_routines_pkg.get_phone_fax_number (''PHONE'',
                                                          PARTY.party_id,
                                                          NULL --, hcasa.party_site_id
                                                              ,
                                                          ''GEN''),
                                                          1,
                                                            80) main_phone_number,
                               PARTY.email_address main_email,
                               acct_site.cust_acct_site_id hcasa_cust_acct_site_id,
                               SITE.SITE_USE_ID SHIP_TO_SITE_USE_ID,
                               NVL((select site_use_id
                                  from hz_Cust_site_uses_all
                                   where site_use_code = ''BILL_TO''
                                    and cust_acct_site_id = acct_site.cust_acct_site_id
                                    AND status=''A'' ),
                                    (select hcsu.site_use_id
                                  from hz_Cust_site_uses_all hcsu
                                     , hz_Cust_acct_sites_all hcas
                                   where hcsu.site_use_code = ''BILL_TO''
                                    and hcsu.cust_acct_site_id = hcas.cust_acct_site_id
                                    AND hcas.cust_account_id  = acct_site.cust_account_id
                                    AND hcsu.status=''A''
                                    AND hcas.status=''A''
                                    AND ROWNUM = 1 )) BILL_TO_SITE_USE_ID,
                                (select PRIMARY_SALESREP_ID
                                   from hz_Cust_site_uses_all
                                 where site_use_code = ''BILL_TO''
                                  and cust_acct_site_id = acct_site.cust_acct_site_id
                                  AND status=''A''
                                  )  PRIMARY_SALESREP_ID
                              ,XXWC_B2B_SO_IB_PKG.get_sales_rep (CUST_ACCT.cust_account_id ) sales_rep_name
                              ,XXWC_OM_SO_CREATE.get_sales_rep_id (CUST_ACCT.cust_account_id ) sales_rep_id
                              ,SITE.location SITE_NAME
                              ,CUST_ACCT.attribute7 AUTH_BUYER
                              ,XXWC_OM_SO_CREATE.get_payment_term_id (CUST_ACCT.cust_account_id) payment_term_id  --payment_term_id
                              ,XXWC_OM_SO_CREATE.get_payment_term_nm (CUST_ACCT.cust_account_id) payment_term_name  --payment_term_name
                              ,DECODE(SUBSTR(cust_acct.attribute4,1,3), ''NAT'', ''Y'', ''N'') national --Rev 2.8
                         FROM
                              HZ_CUST_ACCT_SITES     ACCT_SITE,
                              HZ_PARTY_SITES             PARTY_SITE,
                              HZ_LOCATIONS               LOC,
                              HZ_CUST_SITE_USES_ALL       SITE,
                              HZ_PARTIES           PARTY,
                              HZ_CUST_ACCOUNTS      CUST_ACCT
                        WHERE
                               SITE.SITE_USE_CODE         = ''SHIP_TO''
                          AND   SITE.CUST_ACCT_SITE_ID     = ACCT_SITE.CUST_ACCT_SITE_ID
                          AND   ACCT_SITE.PARTY_SITE_ID    = PARTY_SITE.PARTY_SITE_ID
                          AND   PARTY_SITE.LOCATION_ID     = LOC.LOCATION_ID
                          and    acct_site.status=''A''
                          AND ACCT_SITE.CUST_ACCOUNT_ID=CUST_ACCT.CUST_ACCOUNT_ID
                          AND CUST_ACCT.PARTY_ID=PARTY.PARTY_ID
                          AND CUST_ACCT.status=''A''
                          AND site.status=''A''
                          AND CUST_ACCT.account_number = :p_cust_account_code'
                          using p_cust_account_code;
                RETURN v_rc;
            END IF; --Added for Rev 2.5   
         -- Added for rev#2.2 > Start                 
         ELSE
            --Added for Rev 2.5 > Start
            IF UPPER(NVL(p_filter, 'XXX')) = 'PURCHASES' THEN  --Added for Rev 2.5
               v_rc := GET_CUSTOMER_INFO_ACTIVE(  p_cust_account_code => p_cust_account_code
                                                 ,p_customer_name     => NULL
                                                 ,p_user_nt_id        => p_user_nt_id
                                               ); 
               RETURN v_rc;                                
            ELSE  --Added for Rev 2.5 > End   
               open v_rc for 'SELECT  PARTY.PARTY_NAME customer_name,
                               CUST_ACCT.cust_account_id,
                               CUST_ACCT.account_number CUST_ACCOUNT_CODE,
                               LOC.CITY,
                               (CASE
                                   WHEN LENGTH (NVL (LOC.state, LOC.province)) > 2 THEN NULL
                                        ELSE NVL (LOC.state, LOC.province)
                                END)  state_province,
                               LOC.ADDRESS1,
                               LOC.Postal_Code,
                               LOC.ADDRESS2,
                               (DECODE (CUST_ACCT.status,  ''A'', ''Active'',  ''I'', ''Inactive'',  CUST_ACCT.status))
                               cust_account_status ,
                               SUBSTR (xxwc_mv_routines_pkg.get_phone_fax_number (''PHONE'',
                                                          PARTY.party_id,
                                                          NULL --, hcasa.party_site_id
                                                              ,
                                                          ''GEN''),
                                                          1,
                                                            80) main_phone_number,
                               PARTY.email_address main_email,
                               acct_site.cust_acct_site_id hcasa_cust_acct_site_id,
                               SITE.SITE_USE_ID SHIP_TO_SITE_USE_ID,
                               NVL((select site_use_id
                                  from hz_Cust_site_uses_all
                                   where site_use_code = ''BILL_TO''
                                    and cust_acct_site_id = acct_site.cust_acct_site_id
                                    AND status=''A'' ),
                                    (select hcsu.site_use_id
                                  from hz_Cust_site_uses_all hcsu
                                     , hz_Cust_acct_sites_all hcas
                                   where hcsu.site_use_code = ''BILL_TO''
                                    and hcsu.cust_acct_site_id = hcas.cust_acct_site_id
                                    AND hcas.cust_account_id  = acct_site.cust_account_id
                                    AND hcsu.status=''A''
                                    AND hcas.status=''A''
                                    AND ROWNUM = 1 )) BILL_TO_SITE_USE_ID,
                                (select PRIMARY_SALESREP_ID
                                   from hz_Cust_site_uses_all
                                 where site_use_code = ''BILL_TO''
                                  and cust_acct_site_id = acct_site.cust_acct_site_id
                                  AND status=''A''
                                  )  PRIMARY_SALESREP_ID
                              ,XXWC_B2B_SO_IB_PKG.get_sales_rep (CUST_ACCT.cust_account_id ) sales_rep_name
                              ,XXWC_OM_SO_CREATE.get_sales_rep_id (CUST_ACCT.cust_account_id ) sales_rep_id
                              ,SITE.location SITE_NAME
                              ,CUST_ACCT.attribute7 AUTH_BUYER
                              ,XXWC_OM_SO_CREATE.get_payment_term_id (CUST_ACCT.cust_account_id) payment_term_id  --payment_term_id
                              ,XXWC_OM_SO_CREATE.get_payment_term_nm (CUST_ACCT.cust_account_id) payment_term_name  --payment_term_name
                              ,DECODE(SUBSTR(cust_acct.attribute4,1,3), ''NAT'', ''Y'', ''N'') national --Rev 2.8
                         FROM
                              HZ_CUST_ACCT_SITES     ACCT_SITE,
                              HZ_PARTY_SITES             PARTY_SITE,
                              HZ_LOCATIONS               LOC,
                              HZ_CUST_SITE_USES_ALL       SITE,
                              HZ_PARTIES           PARTY,
                              HZ_CUST_ACCOUNTS      CUST_ACCT
                        WHERE
                               SITE.SITE_USE_CODE         = ''SHIP_TO''
                          AND   SITE.CUST_ACCT_SITE_ID     = ACCT_SITE.CUST_ACCT_SITE_ID
                          AND   ACCT_SITE.PARTY_SITE_ID    = PARTY_SITE.PARTY_SITE_ID
                          AND   PARTY_SITE.LOCATION_ID     = LOC.LOCATION_ID
                          and    acct_site.status=''A''
                          AND ACCT_SITE.CUST_ACCOUNT_ID=CUST_ACCT.CUST_ACCOUNT_ID
                          AND CUST_ACCT.PARTY_ID=PARTY.PARTY_ID
                          AND CUST_ACCT.status=''A''
                          AND site.status=''A''
                          AND CUST_ACCT.account_number = :p_cust_account_code
                          AND EXISTS ( SELECT 1
                                         FROM apps.jtf_rs_salesreps           jrs
                                            , apps.jtf_rs_resource_extns      jrre
                                        WHERE 1 = 1
                                          AND jrs.salesrep_id          = SITE.primary_salesrep_id
                                          AND jrs.org_id               = ACCT_SITE.org_id
                                          AND jrs.resource_id          = jrre.resource_id
                                          AND jrre.user_name           = :p_user_nt_id
                                          AND sysdate BETWEEN jrre.start_Date_active and NVL(jrre.end_date_active, sysdate+1)
                                          AND rownum=1
                                          --Rev 2.7 < Start
                                         UNION  
                                         SELECT 1                                            
                                           FROM xxwc.xxwc_sr_salesreps sr,
                                                xxwc.xxwc_sr_salesrep_hierarchy srh,
                                                xxwc.xxwc_sr_salesrep_hierarchy srmh
                                          WHERE sr.salesrep_id = SITE.primary_salesrep_id
                                            AND sr.salesrep_id = srh.sr_id
                                            AND sr.eeid = srmh.mgr_eeid(+)
                                            AND srh.mgr_eeid = XXWC_OM_SO_CREATE.get_eeid_number(upper(:p_user_nt_id))
                                            AND sr.eeid(+) != XXWC_OM_SO_CREATE.get_eeid_number(upper(:p_user_nt_id))
                                            AND sr.ntid IS NOT NULL    
                                          --Rev 2.7 > End  
                                          --Rev 2.8 < Start
                                        UNION
                                         SELECT 1
                                           FROM fnd_lookup_values  flv
                                          WHERE flv.lookup_type  = ''XXWC_CHECKIT_SUPER_USERS''
                                            AND flv.lookup_code  = UPPER(:p_user_nt_id)
                                            AND flv.enabled_flag = ''Y''
                                            AND TRUNC(SYSDATE) BETWEEN TRUNC(flv.start_date_active) AND TRUNC(NVL(flv.end_date_active,SYSDATE))
                                        --Rev 2.8 < End     
                                        )'
                          using p_cust_account_code, p_user_nt_id, p_user_nt_id, p_user_nt_id, p_user_nt_id; --Rev 2.8
                RETURN v_rc;
             END IF; --Added for Rev 2.5                   
         END IF; 
         -- Added for rev#2.2 < End  
      ELSIF p_customer_name IS NOT NULL THEN
         IF p_user_nt_id IS NULL THEN-- Added for rev#2.2
            --Added for Rev 2.5 > Start
            IF UPPER(NVL(p_filter, 'XXX')) = 'PURCHASES' THEN  --Added for Rev 2.5
               v_rc := GET_CUSTOMER_INFO_ACTIVE(  p_cust_account_code => NULL
                                                 ,p_customer_name     => p_customer_name
                                                 ,p_user_nt_id        => NULL
                                               );
               RETURN v_rc;                                 
            ELSE  --Added for Rev 2.5 > End 
              open v_rc for 'SELECT  PARTY.PARTY_NAME customer_name,
                               CUST_ACCT.cust_account_id,
                               CUST_ACCT.account_number CUST_ACCOUNT_CODE,
                               LOC.CITY,
                               (CASE
                                   WHEN LENGTH (NVL (LOC.state, LOC.province)) > 2 THEN NULL
                                        ELSE NVL (LOC.state, LOC.province)
                                END)  state_province,
                               LOC.ADDRESS1,
                               LOC.Postal_Code,
                               LOC.ADDRESS2,
                               (DECODE (CUST_ACCT.status,  ''A'', ''Active'',  ''I'', ''Inactive'',  CUST_ACCT.status))
                               cust_account_status ,
                               SUBSTR (xxwc_mv_routines_pkg.get_phone_fax_number (''PHONE'',
                                                          PARTY.party_id,
                                                          NULL --, hcasa.party_site_id
                                                              ,
                                                          ''GEN''),
                                                          1,
                                                            80) main_phone_number,
                               PARTY.email_address main_email,
                               acct_site.cust_acct_site_id hcasa_cust_acct_site_id,
                               SITE.SITE_USE_ID SHIP_TO_SITE_USE_ID,
                               NVL((select site_use_id
                                  from hz_Cust_site_uses_all
                                   where site_use_code = ''BILL_TO''
                                    and cust_acct_site_id = acct_site.cust_acct_site_id
                                    AND status=''A'' ),
                                    (select hcsu.site_use_id
                                  from hz_Cust_site_uses_all hcsu
                                     , hz_Cust_acct_sites_all hcas
                                   where hcsu.site_use_code = ''BILL_TO''
                                    and hcsu.cust_acct_site_id = hcas.cust_acct_site_id
                                    AND hcas.cust_account_id  = acct_site.cust_account_id
                                    AND hcsu.status=''A''
                                    AND hcas.status=''A''
                                    AND ROWNUM = 1 )) BILL_TO_SITE_USE_ID,
                                (select PRIMARY_SALESREP_ID
                                   from hz_Cust_site_uses_all
                                 where site_use_code = ''BILL_TO''
                                  and cust_acct_site_id = acct_site.cust_acct_site_id
                                  AND status=''A'' )  PRIMARY_SALESREP_ID
                              ,XXWC_B2B_SO_IB_PKG.get_sales_rep (CUST_ACCT.cust_account_id ) sales_rep_name
                              ,XXWC_OM_SO_CREATE.get_sales_rep_id (CUST_ACCT.cust_account_id ) sales_rep_id
                              ,SITE.location SITE_NAME
                              ,CUST_ACCT.attribute7 AUTH_BUYER
                              ,XXWC_OM_SO_CREATE.get_payment_term_id (CUST_ACCT.cust_account_id) payment_term_id  --payment_term_id
                              ,XXWC_OM_SO_CREATE.get_payment_term_nm (CUST_ACCT.cust_account_id) payment_term_name  --payment_term_name
                              ,DECODE(SUBSTR(cust_acct.attribute4,1,3), ''NAT'', ''Y'', ''N'') national --Rev 2.8
                         FROM
                              HZ_CUST_ACCT_SITES     ACCT_SITE,
                              HZ_PARTY_SITES             PARTY_SITE,
                              HZ_LOCATIONS               LOC,
                              HZ_CUST_SITE_USES_ALL       SITE,
                              HZ_PARTIES           PARTY,
                              HZ_CUST_ACCOUNTS      CUST_ACCT
                        WHERE
                               SITE.SITE_USE_CODE         = ''SHIP_TO''
                          AND   SITE.CUST_ACCT_SITE_ID     = ACCT_SITE.CUST_ACCT_SITE_ID
                          AND   ACCT_SITE.PARTY_SITE_ID    = PARTY_SITE.PARTY_SITE_ID
                          AND   PARTY_SITE.LOCATION_ID     = LOC.LOCATION_ID
                          and    acct_site.status=''A''
                          AND ACCT_SITE.CUST_ACCOUNT_ID=CUST_ACCT.CUST_ACCOUNT_ID
                          AND CUST_ACCT.PARTY_ID=PARTY.PARTY_ID
                          AND CUST_ACCT.status=''A''
                          AND site.status=''A''
                          AND UPPER(PARTY.PARTY_NAME)     LIKE UPPER(:p_customer_name)||''%'''
                          using p_customer_name;
                          RETURN v_rc;
            END IF; --Added for Rev 2.5              
         -- Added for rev#2.2 > Start                 
         ELSE
            --Added for Rev 2.5 > Start
            IF UPPER(NVL(p_filter, 'XXX')) = 'PURCHASES' THEN  --Added for Rev 2.5
               v_rc := GET_CUSTOMER_INFO_ACTIVE(  p_cust_account_code => NULL
                                                 ,p_customer_name     => p_customer_name
                                                 ,p_user_nt_id        => p_user_nt_id
                                               ); 
               RETURN v_rc;                                  
            ELSE  --Added for Rev 2.5 > End 
               open v_rc for 'SELECT  PARTY.PARTY_NAME customer_name,
                               CUST_ACCT.cust_account_id,
                               CUST_ACCT.account_number CUST_ACCOUNT_CODE,
                               LOC.CITY,
                               (CASE
                                   WHEN LENGTH (NVL (LOC.state, LOC.province)) > 2 THEN NULL
                                        ELSE NVL (LOC.state, LOC.province)
                                END)  state_province,
                               LOC.ADDRESS1,
                               LOC.Postal_Code,
                               LOC.ADDRESS2,
                               (DECODE (CUST_ACCT.status,  ''A'', ''Active'',  ''I'', ''Inactive'',  CUST_ACCT.status))
                               cust_account_status ,
                               SUBSTR (xxwc_mv_routines_pkg.get_phone_fax_number (''PHONE'',
                                                          PARTY.party_id,
                                                          NULL --, hcasa.party_site_id
                                                              ,
                                                          ''GEN''),
                                                          1,
                                                            80) main_phone_number,
                               PARTY.email_address main_email,
                               acct_site.cust_acct_site_id hcasa_cust_acct_site_id,
                               SITE.SITE_USE_ID SHIP_TO_SITE_USE_ID,
                               NVL((select site_use_id
                                  from hz_Cust_site_uses_all
                                   where site_use_code = ''BILL_TO''
                                    and cust_acct_site_id = acct_site.cust_acct_site_id
                                    AND status=''A'' ),
                                    (select hcsu.site_use_id
                                  from hz_Cust_site_uses_all hcsu
                                     , hz_Cust_acct_sites_all hcas
                                   where hcsu.site_use_code = ''BILL_TO''
                                    and hcsu.cust_acct_site_id = hcas.cust_acct_site_id
                                    AND hcas.cust_account_id  = acct_site.cust_account_id
                                    AND hcsu.status=''A''
                                    AND hcas.status=''A''
                                    AND ROWNUM = 1 )) BILL_TO_SITE_USE_ID,
                                (select PRIMARY_SALESREP_ID
                                   from hz_Cust_site_uses_all
                                 where site_use_code = ''BILL_TO''
                                  and cust_acct_site_id = acct_site.cust_acct_site_id
                                  AND status=''A'' )  PRIMARY_SALESREP_ID
                              ,XXWC_B2B_SO_IB_PKG.get_sales_rep (CUST_ACCT.cust_account_id ) sales_rep_name
                              ,XXWC_OM_SO_CREATE.get_sales_rep_id (CUST_ACCT.cust_account_id ) sales_rep_id
                              ,SITE.location SITE_NAME
                              ,CUST_ACCT.attribute7 AUTH_BUYER
                              ,XXWC_OM_SO_CREATE.get_payment_term_id (CUST_ACCT.cust_account_id) payment_term_id  --payment_term_id
                              ,XXWC_OM_SO_CREATE.get_payment_term_nm (CUST_ACCT.cust_account_id) payment_term_name  --payment_term_name
                              ,DECODE(SUBSTR(cust_acct.attribute4,1,3), ''NAT'', ''Y'', ''N'') national --Rev 2.8
                         FROM
                              HZ_CUST_ACCT_SITES     ACCT_SITE,
                              HZ_PARTY_SITES             PARTY_SITE,
                              HZ_LOCATIONS               LOC,
                              HZ_CUST_SITE_USES_ALL       SITE,
                              HZ_PARTIES           PARTY,
                              HZ_CUST_ACCOUNTS      CUST_ACCT
                        WHERE
                               SITE.SITE_USE_CODE         = ''SHIP_TO''
                          AND   SITE.CUST_ACCT_SITE_ID     = ACCT_SITE.CUST_ACCT_SITE_ID
                          AND   ACCT_SITE.PARTY_SITE_ID    = PARTY_SITE.PARTY_SITE_ID
                          AND   PARTY_SITE.LOCATION_ID     = LOC.LOCATION_ID
                          and    acct_site.status=''A''
                          AND ACCT_SITE.CUST_ACCOUNT_ID=CUST_ACCT.CUST_ACCOUNT_ID
                          AND CUST_ACCT.PARTY_ID=PARTY.PARTY_ID
                          AND CUST_ACCT.status=''A''
                          AND site.status=''A''
                          AND UPPER(party.party_name)     LIKE UPPER(:p_customer_name)||''%''
                          AND EXISTS ( SELECT 1
                                         FROM apps.jtf_rs_salesreps           jrs
                                            , apps.jtf_rs_resource_extns      jrre
                                        WHERE 1 = 1
                                          AND jrs.salesrep_id          = SITE.primary_salesrep_id
                                          AND jrs.org_id               = ACCT_SITE.org_id
                                          AND jrs.resource_id          = jrre.resource_id
                                          AND jrre.user_name           = :p_user_nt_id
                                          AND sysdate BETWEEN jrre.start_Date_active and NVL(jrre.end_date_active, sysdate+1)
                                          AND rownum=1
                                         --Rev 2.7 < Start
                                         UNION  
                                         SELECT 1                                            
                                           FROM xxwc.xxwc_sr_salesreps sr,
                                                xxwc.xxwc_sr_salesrep_hierarchy srh,
                                                xxwc.xxwc_sr_salesrep_hierarchy srmh
                                          WHERE sr.salesrep_id = SITE.primary_salesrep_id
                                            AND sr.salesrep_id = srh.sr_id
                                            AND sr.eeid = srmh.mgr_eeid(+)
                                            AND srh.mgr_eeid = XXWC_OM_SO_CREATE.get_eeid_number(upper(:p_user_nt_id))
                                            AND sr.eeid(+) != XXWC_OM_SO_CREATE.get_eeid_number(upper(:p_user_nt_id))
                                            AND sr.ntid IS NOT NULL    
                                         --Rev 2.7 > End    
                                         --Rev 2.8 < Start
                                        UNION
                                         SELECT 1
                                           FROM fnd_lookup_values  flv
                                          WHERE flv.lookup_type  = ''XXWC_CHECKIT_SUPER_USERS''
                                            AND flv.lookup_code  = UPPER(:p_user_nt_id)
                                            AND flv.enabled_flag = ''Y''
                                            AND TRUNC(SYSDATE) BETWEEN TRUNC(flv.start_date_active) AND TRUNC(NVL(flv.end_date_active,SYSDATE))
                                        --Rev 2.8 < End     
                                        )'
                          using p_customer_name, p_user_nt_id, p_user_nt_id, p_user_nt_id, p_user_nt_id; --Rev 2.8 
                          RETURN v_rc;
             END IF; --Added for Rev 2.5             
         END IF; 
         -- Added for rev#2.2 < End  
      END IF;
   END; --GET_CUSTOMER_INFO_RS

   FUNCTION GET_CUSTOMER_CONTACTS(p_cust_account_id IN NUMBER) return sys_refcursor IS
   v_rec sys_refcursor;
   BEGIN

  open v_rec for 'SELECT party.PERSON_FIRST_NAME,
       party.PERSON_LAST_NAME,
       PARTY.EMAIL_ADDRESS,
       party.PRIMARY_PHONE_NUMBER,
       acct_role.cust_account_role_id
  FROM hz_cust_account_roles acct_role,
       --    hz_contact_points      hcp,
       hz_parties party,
       hz_parties rel_party,
       hz_relationships rel,
       hz_org_contacts org_cont,
       hz_cust_accounts role_acct
 WHERE     acct_role.party_id = rel.party_id
       AND NOT EXISTS
              (SELECT ''1''
                 FROM hz_role_responsibility rr
                WHERE rr.cust_account_role_id =
                         acct_role.cust_account_role_id)
       AND acct_role.role_type = ''CONTACT''
       AND org_cont.party_relationship_id = rel.relationship_id
       AND rel.subject_id = party.party_id
       AND rel_party.party_id = rel.party_id
       AND acct_role.cust_account_id = role_acct.cust_account_id
       AND role_acct.party_id = rel.object_id
       AND rel.subject_table_name = ''HZ_PARTIES''
       AND rel.object_table_name = ''HZ_PARTIES''
       AND (org_cont.job_title IS NULL AND org_cont.job_title_code IS NULL)
       AND party.status = ''A''
       AND rel_party.status = ''A''
       AND rel.status = ''A''
       AND acct_role.status = ''A''
       AND role_acct.cust_account_id =:p_cust_account_id'
       using p_cust_account_id;
       RETURN v_rec;
   END;

   FUNCTION GET_AUTH_BUYER(p_cust_account_id IN NUMBER) return sys_refcursor IS
   v_rec sys_refcursor;
   BEGIN

     open v_rec for 'SELECT hp.party_name auth_buyer, cont.contact_id auth_buyer_id, DECODE(cont.status, ''I'',''Inactive'',''A'',''Active'', cont.status) Status
                       FROM ar_contacts_v cont, hz_parties hp, hz_role_responsibility cont_role
                      WHERE cont.contact_party_id = hp.party_id
                        AND cont.contact_id = cont_role.cust_account_role_id
                        AND cont_role.responsibility_type = ''AUTH_BUYER'' AND NVL(cont.status,''A'') = ''A''
                        AND cont.customer_id = :p_cust_account_id'
       using p_cust_account_id;
       RETURN v_rec;
   END;

   FUNCTION GET_BRANCH_INFO(p_user_nt_id IN VARCHAR2) return sys_refcursor IS
   v_rec sys_refcursor;
   BEGIN


  open v_rec for 'SELECT PROFILE_OPTION_VALUE Warehouse_id, mp.ORGANIZATION_CODE Warehouse_num, org.NAME Warehouse_Name,user_id, description user_name, email_address
                    FROM apps.fnd_profile_option_values fpov,
                        apps.fnd_user fu,
                        apps.fnd_profile_options fpo,
                       mtl_parameters mp,
                      HR_ALL_ORGANIZATION_UNITS org
     WHERE PROFILE_OPTION_NAME = ''XXWC_OM_DEFAULT_SHIPPING_ORG''
       AND (fu.user_id = fpov.level_value OR level_id =  ''10001'')
       AND fpo.profile_option_id = fpov.profile_option_id
       AND mp.organization_id = fpov.PROFILE_OPTION_VALUE
       AND org.organization_id = mp.organization_id
       AND fu.user_name = :p_user_nt_id
       AND rownum =1
       ORDER BY level_id desc'
       using p_user_nt_id;
       RETURN v_rec;

   END;

   FUNCTION GET_BRANCH_INFO(  p_user_nt_id IN VARCHAR2
                             ,p_email_id IN VARCHAR2 ) return sys_refcursor IS
   v_rec sys_refcursor;
   BEGIN
      IF p_email_id IS NOT NULL THEN
         open v_rec for 'SELECT PROFILE_OPTION_VALUE Warehouse_id, mp.ORGANIZATION_CODE Warehouse_num, org.NAME Warehouse_Name,user_id, description user_name, email_address
                    FROM apps.fnd_profile_option_values fpov,
                        apps.fnd_user fu,
                        apps.fnd_profile_options fpo,
                       mtl_parameters mp,
                      HR_ALL_ORGANIZATION_UNITS org
         WHERE PROFILE_OPTION_NAME = ''XXWC_OM_DEFAULT_SHIPPING_ORG''
         AND (fu.user_id = fpov.level_value OR level_id =  ''10001'')
         AND fpo.profile_option_id = fpov.profile_option_id
         AND mp.organization_id = fpov.PROFILE_OPTION_VALUE
         AND org.organization_id = mp.organization_id
         AND UPPER(fu.EMAIL_ADDRESS) = UPPER(:p_email_id)
         AND rownum =1
         ORDER BY level_id desc'
         using p_email_id;
         RETURN v_rec;
      ELSE
        open v_rec for 'SELECT PROFILE_OPTION_VALUE Warehouse_id, mp.ORGANIZATION_CODE Warehouse_num, org.NAME Warehouse_Name,user_id, description user_name, email_address
                    FROM apps.fnd_profile_option_values fpov,
                        apps.fnd_user fu,
                        apps.fnd_profile_options fpo,
                       mtl_parameters mp,
                      HR_ALL_ORGANIZATION_UNITS org
         WHERE PROFILE_OPTION_NAME = ''XXWC_OM_DEFAULT_SHIPPING_ORG''
         AND (fu.user_id = fpov.level_value OR level_id =  ''10001'')
         AND fpo.profile_option_id = fpov.profile_option_id
         AND mp.organization_id = fpov.PROFILE_OPTION_VALUE
         AND org.organization_id = mp.organization_id
         AND fu.user_name = :p_user_nt_id
         AND rownum =1
         ORDER BY level_id desc'
         using p_user_nt_id;
         RETURN v_rec;
      END IF;

   END;

  FUNCTION GET_BRANCH_DETAILS(  p_organization_id IN VARCHAR2 DEFAULT NULL
                               ) return sys_refcursor IS
   v_rec sys_refcursor;
   BEGIN
       OPEN v_rec for 'SELECT  mp.organization_id
       ,mp.organization_code
       ,hl.LOCATION_CODE
       ,hl.address_line_1 branch_street
       ,DECODE (hl.town_or_city, NULL, NULL, hl.town_or_city || '',''||'')
      || DECODE (hl.region_2, NULL, hl.region_2 || '',''||'' , hl.region_2 || '',''||'''' )
      || DECODE (hl.postal_code, NULL, NULL, hl.postal_code) branch_city
      ,hl.telephone_number_1 branch_tel1
      ,hl.telephone_number_2 branch_tel2
       ,mp.attribute6
       ,mp.attribute7
       ,mp.attribute8
       ,mp.attribute9
       ,mp.attribute10
       ,mp.attribute11
       ,mp.attribute12
       ,mp.attribute13
       ,mp.attribute14
       ,mp.attribute15
FROM apps.mtl_parameters mp
   , apps.hr_all_organization_units haou
   , apps.hr_locations_all hl
WHERE mp.organization_id = haou.organization_id(+)
  AND haou.location_id   = hl.location_id
  AND mp.MASTER_ORGANIZATION_ID    = 222
  AND mp.organization_id = NVL(:p_organization_id, mp.organization_id)'
       using p_organization_id;
       RETURN v_rec;
   END;

PROCEDURE line_pricing_data ( p_organization_id NUMBER
                              , p_cust_account_id NUMBER
                              , p_site_use_id NUMBER
                             )
IS
   p_line_tbl                 qp_preq_grp.line_tbl_type;
   p_qual_tbl                 qp_preq_grp.qual_tbl_type;
   p_line_attr_tbl            qp_preq_grp.line_attr_tbl_type;
   p_line_detail_tbl          qp_preq_grp.line_detail_tbl_type;
   p_line_detail_qual_tbl     qp_preq_grp.line_detail_qual_tbl_type;
   p_line_detail_attr_tbl     qp_preq_grp.line_detail_attr_tbl_type;
   p_related_lines_tbl        qp_preq_grp.related_lines_tbl_type;
   p_control_rec              qp_preq_grp.control_record_type;
   x_line_tbl                 qp_preq_grp.line_tbl_type;
   x_line_qual                qp_preq_grp.qual_tbl_type;
   x_line_attr_tbl            qp_preq_grp.line_attr_tbl_type;
   x_line_detail_tbl          qp_preq_grp.line_detail_tbl_type;
   x_line_detail_qual_tbl     qp_preq_grp.line_detail_qual_tbl_type;
   x_line_detail_attr_tbl     qp_preq_grp.line_detail_attr_tbl_type;
   x_related_lines_tbl        qp_preq_grp.related_lines_tbl_type;
   x_return_status            VARCHAR2 (240);
   x_return_status_text       VARCHAR2 (240);
   qual_rec                   qp_preq_grp.qual_rec_type;
   line_attr_rec              qp_preq_grp.line_attr_rec_type;
   line_rec                   qp_preq_grp.line_rec_type;
   detail_rec                 qp_preq_grp.line_detail_rec_type;
   ldet_rec                   qp_preq_grp.line_detail_rec_type;
   rltd_rec                   qp_preq_grp.related_lines_rec_type;
   l_pricing_contexts_tbl     qp_attr_mapping_pub.contexts_result_tbl_type;
   l_qualifier_contexts_tbl   qp_attr_mapping_pub.contexts_result_tbl_type;
   v_line_tbl_cnt             INTEGER;

   i                          BINARY_INTEGER;
   j                          BINARY_INTEGER;
   k                          BINARY_INTEGER;

   l_version                  VARCHAR2 (240);
   l_file_val                 VARCHAR2 (60);
   l_modifier_name            VARCHAR2 (240);
   l_list_header_id           NUMBER;
   l_list_line_id             NUMBER;
   l_incomp_code              VARCHAR2 (30);
   l_modifier_type            VARCHAR2 (30);
   l_line_modifier_type       VARCHAR2 (30);
   l_item_category            NUMBER;
   l_gm_selling_price         NUMBER;
   l_message_level            NUMBER;
   l_currency_code            VARCHAR2 (240);  --TMS#20141002-0038 Added Veera
   l_org_id                   NUMBER := fnd_profile.value('ORG_ID');
   l_list_line_type_code      VARCHAR2 (30);              -- Shankar 27-Mar-15


   CURSOR cur IS
   SELECT inventory_item_id
        , NVL(primary_uom_code,'EA') primary_uom_code
     FROM xxwc.xxwc_md_search_product_gtt_tbl
    ORDER BY inventory_item_id
    ;

   l_rec_cntr              NUMBER;
   l_line_attr_tbl_cntr    NUMBER;
   l_qual_tbl_cntr         NUMBER;
   l_inventory_item_id     NUMBER;
BEGIN

   DELETE FROM XXWC_PA_WAREHOUSE_GT;

   BEGIN
      INSERT INTO xxwc.xxwc_pa_warehouse_gt (organization_id)
           VALUES ( p_organization_id);
   EXCEPTION
      WHEN OTHERS
      THEN
         NULL;
   END;

   SELECT gsob.currency_code
     INTO l_currency_code
     FROM hr_operating_units hou, gl_sets_of_books gsob
    WHERE     hou.organization_id = l_org_id
          AND gsob.set_of_books_id = hou.set_of_books_id;

   l_rec_cntr           := 0;
   l_line_attr_tbl_cntr := 0;
   l_qual_tbl_cntr      := 0;

   FOR rec IN cur LOOP

   l_rec_cntr           := l_rec_cntr + 1;
   l_line_attr_tbl_cntr := l_line_attr_tbl_cntr + 1;
   l_qual_tbl_cntr      := l_qual_tbl_cntr + 1;

   l_gm_selling_price := NULL;
   l_item_category := NULL;

      BEGIN
         SELECT mic.category_id
           INTO l_item_category
           FROM mtl_item_categories_v mic
          WHERE     mic.category_set_name = 'Inventory Category'
                AND mic.inventory_item_id = rec.inventory_item_id
                AND mic.organization_id = p_organization_id
                AND ROWNUM = 1;
      EXCEPTION
         WHEN OTHERS
         THEN
            l_item_category := NULL;
      END;

      --fnd_message.debug('2');
      qp_attr_mapping_pub.build_contexts (
         p_request_type_code           => 'ONT',
         p_pricing_type                => 'L',
         x_price_contexts_result_tbl   => l_pricing_contexts_tbl,
         x_qual_contexts_result_tbl    => l_qualifier_contexts_tbl);

      ---- Control Record
      p_control_rec.pricing_event := 'LINE';                       -- 'BATCH';
      p_control_rec.calculate_flag := 'Y'; --QP_PREQ_GRP.G_SEARCH_N_CALCULATE;
      p_control_rec.simulation_flag := 'Y';
      p_control_rec.rounding_flag := 'Q';
      p_control_rec.manual_discount_flag := 'Y';
      p_control_rec.request_type_code := 'ONT';
      p_control_rec.temp_table_insert_flag := 'Y';

      ---- Line Records ---------
      line_rec.request_type_code := 'ONT';
      line_rec.line_id := -1; -- Order Line Id. This can be any thing for this script
      line_rec.line_index := l_rec_cntr;                        -- Request Line Index
      line_rec.line_type_code := 'LINE';        -- LINE or ORDER(Summary Line)
      line_rec.pricing_effective_date := SYSDATE; -- Pricing as of what date ?
      line_rec.active_date_first := SYSDATE; -- Can be Ordered Date or Ship Date
      line_rec.active_date_second := SYSDATE; -- Can be Ordered Date or Ship Date
      line_rec.active_date_first_type := 'NO TYPE';                -- ORD/SHIP
      line_rec.active_date_second_type := 'NO TYPE';               -- ORD/SHIP
      line_rec.line_quantity := 1; -- Ordered Quantity

      line_rec.line_uom_code := rec.primary_uom_code;
      -- 20160307-00117 < End

      line_rec.currency_code := l_currency_code; --TMS#20141002-00038 Added by Veera

      line_rec.price_flag := 'Y'; -- Price Flag can have 'Y' , 'N'(No pricing) , 'P'(Phase)
      p_line_tbl (l_rec_cntr) := line_rec;

      ---- Line Attribute Record
      line_attr_rec.line_index := l_rec_cntr;
      line_attr_rec.pricing_context := 'ITEM';
      line_attr_rec.pricing_attribute := 'PRICING_ATTRIBUTE1';
      line_attr_rec.pricing_attr_value_from := TO_CHAR (rec.inventory_item_id); -- INVENTORY ITEM ID
      line_attr_rec.validated_flag := 'N';
      p_line_attr_tbl (l_line_attr_tbl_cntr) := line_attr_rec;

      IF l_item_category IS NOT NULL
      THEN
         l_line_attr_tbl_cntr     := l_line_attr_tbl_cntr + 1;
         line_attr_rec.line_index := l_rec_cntr;
         line_attr_rec.pricing_context := 'ITEM';                           --
         line_attr_rec.pricing_attribute := 'PRICING_ATTRIBUTE2';
         line_attr_rec.pricing_attr_value_from := l_item_category; -- Category ID
         line_attr_rec.validated_flag := 'N';
         p_line_attr_tbl (l_line_attr_tbl_cntr) := line_attr_rec;
      END IF;

      qual_rec.line_index := l_rec_cntr;
      qual_rec.qualifier_context := 'ORDER';
      qual_rec.qualifier_attribute := 'QUALIFIER_ATTRIBUTE18';
      qual_rec.qualifier_attr_value_from := p_organization_id;         -- SHIP_FROM_ORG_ID;
      qual_rec.comparison_operator_code := '=';
      qual_rec.validated_flag := 'Y';
      p_qual_tbl (l_qual_tbl_cntr) := qual_rec;

      IF p_cust_account_id is not null then
      l_qual_tbl_cntr := l_qual_tbl_cntr + 1;
      qual_rec.line_index := l_rec_cntr;
      qual_rec.qualifier_context := 'CUSTOMER';
      qual_rec.qualifier_attribute := 'QUALIFIER_ATTRIBUTE32';
      qual_rec.qualifier_attr_value_from := p_cust_account_id;                         -- CUSTOMER ID;
      qual_rec.comparison_operator_code := '=';
      qual_rec.validated_flag := 'Y';
      p_qual_tbl (l_qual_tbl_cntr) := qual_rec;
      END IF;

      IF p_site_use_id is not null then
      l_qual_tbl_cntr := l_qual_tbl_cntr + 1;
      qual_rec.line_index := l_rec_cntr;
      qual_rec.qualifier_context := 'CUSTOMER';
      qual_rec.qualifier_attribute := 'QUALIFIER_ATTRIBUTE11';
      qual_rec.qualifier_attr_value_from := p_site_use_id;               -- Ship To Site Use ID;
      qual_rec.comparison_operator_code := '=';
      qual_rec.validated_flag := 'Y';
      p_qual_tbl (l_qual_tbl_cntr) := qual_rec;
      END IF;

      -- TMS# 20151111-00161 > Start
      l_qual_tbl_cntr := l_qual_tbl_cntr + 1;

      qual_rec.line_index := l_rec_cntr;
      qual_rec.qualifier_context := 'ITEM_NUMBER';
      qual_rec.qualifier_attribute := 'QUALIFIER_ATTRIBUTE31';
      qual_rec.qualifier_attr_value_from := TO_CHAR ( rec.inventory_item_id);       -- ItemId
      qual_rec.comparison_operator_code := 'NOT=';
      qual_rec.validated_flag := 'Y';
      p_qual_tbl (l_qual_tbl_cntr) := qual_rec;

      IF l_item_category IS NOT NULL
      THEN
         l_qual_tbl_cntr := l_qual_tbl_cntr + 1;
         qual_rec.line_index := l_rec_cntr;
         qual_rec.qualifier_context := 'ITEM_CAT';
         qual_rec.qualifier_attribute := 'QUALIFIER_ATTRIBUTE31';
         qual_rec.qualifier_attr_value_from := TO_CHAR (l_item_category); -- CatergoryId
         qual_rec.comparison_operator_code := 'NOT=';
         qual_rec.validated_flag := 'Y';
         p_qual_tbl (l_qual_tbl_cntr) := qual_rec;
      END IF;

            UPDATE xxwc.xxwc_md_search_product_gtt_tbl
               SET sequence      = l_rec_cntr
             WHERE inventory_item_id = rec.inventory_item_id;

   END LOOP;

      qp_preq_pub.price_request (p_line_tbl,
                                 p_qual_tbl,
                                 p_line_attr_tbl,
                                 p_line_detail_tbl,
                                 p_line_detail_qual_tbl,
                                 p_line_detail_attr_tbl,
                                 p_related_lines_tbl,
                                 p_control_rec,
                                 x_line_tbl,
                                 x_line_qual,
                                 x_line_attr_tbl,
                                 x_line_detail_tbl,
                                 x_line_detail_qual_tbl,
                                 x_line_detail_attr_tbl,
                                 x_related_lines_tbl,
                                 x_return_status,
                                 x_return_status_text);

      i := x_line_detail_tbl.FIRST;
      IF i IS NOT NULL THEN

       LOOP

            --fnd_message.debug('7');
            IF x_line_detail_tbl (i).automatic_flag = 'Y'
            THEN
               l_modifier_name := NULL;
               l_list_header_id := NULL;
               l_list_line_id := NULL;
               l_incomp_code := NULL;
               l_modifier_type := NULL;
               l_list_line_type_code := NULL;
               l_line_modifier_type := NULL;

               BEGIN
                  SELECT name, attribute10
                    INTO l_modifier_name, l_modifier_type
                    FROM qp_list_headers_vl
                   WHERE list_header_id = x_line_detail_tbl (i).list_header_id;

                  l_list_header_id := x_line_detail_tbl (i).list_header_id;
                  l_list_line_id := x_line_detail_tbl (i).list_line_id;

                  IF l_list_line_id IS NOT NULL THEN
                     SELECT incompatibility_grp_code,
                            list_line_type_code,
                            attribute5
                       INTO l_incomp_code,
                            l_list_line_type_code,
                            l_line_modifier_type
                       FROM qp_list_lines
                      WHERE list_line_id = l_list_line_id;

                     IF l_modifier_type = 'Contract Pricing' THEN
                        l_modifier_type := NVL (l_line_modifier_type, l_modifier_type);
                     END IF;

                     IF l_modifier_type IS NOT NULL THEN
                        BEGIN
                           SELECT description
                             INTO l_modifier_Type
                             FROM fnd_flex_values_vl
                            WHERE flex_value_set_id = 1015252
                              AND flex_value = l_modifier_Type;
                        EXCEPTION
                           WHEN OTHERS THEN
                              NULL;
                        END;
                     END IF;

                     IF l_list_line_type_code = 'PLL' THEN
                        l_incomp_code := 'PLL';
                     END IF;

                  END IF;
               EXCEPTION
                  WHEN OTHERS THEN
                     l_modifier_name := NULL;
                     l_list_header_id := NULL;
                     l_list_line_id := NULL;
                     l_incomp_code := NULL;
                     l_modifier_type := NULL;
                     l_list_line_type_code := NULL;
                     l_line_modifier_type := NULL;
               END;
                -- Getting new selling price

                  l_gm_selling_price := NULL;
                  j := x_line_tbl.FIRST;
                  IF j IS NOT NULL THEN
                    LOOP
                      IF x_line_tbl (j).LINE_INDEX = x_line_detail_tbl (i).LINE_INDEX THEN
                          l_gm_selling_price := x_line_tbl (j).adjusted_unit_price;
                      END IF;
                      EXIT WHEN l_gm_selling_price IS NOT NULL;
                      j          := x_line_tbl.NEXT (j);
                    END LOOP;
                  END IF;

            IF l_gm_selling_price = 0 THEN
              l_gm_selling_price := NULL;
            END IF;

            UPDATE xxwc.xxwc_md_search_product_gtt_tbl
               SET selling_price = l_gm_selling_price
                 , modifier      = l_modifier_name --Rev 2.2
                 , modifier_type = l_modifier_type --Rev 2.2
             WHERE sequence  = x_line_detail_tbl(i).line_index;

            END IF;

            EXIT WHEN i = x_line_detail_tbl.LAST;
            i := x_line_detail_tbl.NEXT (i);

         END LOOP;
      END IF;
END;

--Shankar 20150504-00288  04-May-2015 changes
FUNCTION get_list_price(i_organization_id in number, i_inventory_item_id in NUMBER)
RETURN NUMBER IS
G_MARKET_NTL_PRL_ID     NUMBER := fnd_profile.value ('XXWC_QP_MARKET_NATIONAL_PRL');
G_MARKET_NTL_STP_PRL_ID NUMBER := fnd_profile.value ('XXWC_QP_MARKET_NTL_SETUP_PRL');
G_MARKET_CAT_PRL_ID     NUMBER := fnd_profile.value ('XXWC_QP_MARKET_CATEGORY_PRL');
 o_list_price apps.qp_list_lines.operand%TYPE;
 o_list_header_id APPS.qp_list_headers.list_header_id%TYPE;
 o_pl_name APPS.qp_list_headers.name%TYPE;
BEGIN

     BEGIN

      SELECT  ll.operand, lh.list_header_id, lh.name
        INTO o_list_price, o_list_header_id, o_pl_name
        FROM    apps.qp_list_headers lh,
                apps.qp_qualifiers qq,
                apps.qp_list_lines ll,
                apps.qp_pricing_attributes qpa
        WHERE   lh.list_type_code = 'PRL'
        AND     lh.list_header_id not in (G_MARKET_NTL_PRL_ID, G_MARKET_CAT_PRL_ID)
        AND     lh.attribute10 = 'Market Price List'
        AND     lh.list_header_id = qq.list_header_id
        AND     qq.list_line_id = -1
        AND     qq.qualifier_context = 'ORDER'
        AND     qq.qualifier_attribute = 'QUALIFIER_ATTRIBUTE18'
        AND     qq.qualifier_attr_value = to_char(i_organization_id)
        AND     SYSDATE BETWEEN NVL (qq.start_date_active, SYSDATE - 1) AND NVL (qq.end_date_active, SYSDATE + 1)
        AND     lh.list_header_id = ll.list_header_id
        AND     SYSDATE BETWEEN NVL (ll.start_date_active, SYSDATE - 1) AND NVL (ll.end_date_active, SYSDATE + 1)
        AND     ll.list_header_id  = qpa.list_header_id
        AND     ll.qualification_ind  = qpa.qualification_ind
        AND     ll.pricing_phase_id = qpa.pricing_phase_id
        AND     ll.list_line_id = qpa.list_line_id
        AND     qpa.product_attribute_context = 'ITEM'
        AND     qpa.product_attribute = 'PRICING_ATTRIBUTE1'
        AND     qpa.product_attribute_datatype = 'C'
        AND     qpa.product_attr_value = to_char(i_inventory_item_id)
        AND     ROWNUM = 1;


          EXCEPTION
            WHEN OTHERS THEN o_list_price := NULL ;

          END ;

          IF o_list_price IS NULL THEN
          BEGIN

          SELECT  ll.operand, lh.list_header_id, lh.name
            INTO    o_list_price, o_list_header_id, o_pl_name
            FROM    apps.qp_list_headers lh,
                    apps.qp_list_lines ll,
                    apps.qp_pricing_attributes qpa
            WHERE   lh.list_header_id = G_MARKET_NTL_PRL_ID
            AND     lh.list_header_id = ll.list_header_id
            AND     SYSDATE BETWEEN NVL (ll.start_date_active, SYSDATE - 1) AND NVL (ll.end_date_active, SYSDATE + 1)
            AND     ll.list_header_id  = qpa.list_header_id
            AND     ll.qualification_ind  = qpa.qualification_ind
            AND     ll.pricing_phase_id = qpa.pricing_phase_id
            AND     ll.list_line_id = qpa.list_line_id
            AND     qpa.product_attribute_context = 'ITEM'
            AND     qpa.product_attribute = 'PRICING_ATTRIBUTE1'
            AND     qpa.product_attribute_datatype = 'C'
            AND     qpa.product_attr_value = to_char(i_inventory_item_id)
            AND     ROWNUM = 1;

          EXCEPTION
            WHEN OTHERS THEN o_list_price := NULL ;
          END ;
          END IF ;

         IF o_list_price IS NULL THEN
         BEGIN
                 SELECT  ll.operand, lh.list_header_id, lh.name
                INTO    o_list_price, o_list_header_id, o_pl_name
                FROM    apps.qp_list_headers lh,
                        apps.qp_list_lines ll,
                        apps.qp_pricing_attributes qpa
                WHERE   lh.list_header_id = G_MARKET_CAT_PRL_ID
                AND     lh.list_header_id = ll.list_header_id
                AND     SYSDATE BETWEEN NVL (ll.start_date_active, SYSDATE - 1) AND NVL (ll.end_date_active, SYSDATE + 1)
                AND     ll.list_header_id  = qpa.list_header_id
                AND     ll.qualification_ind  = qpa.qualification_ind
                AND     ll.pricing_phase_id = qpa.pricing_phase_id
                AND     ll.list_line_id = qpa.list_line_id
                AND     qpa.product_attribute_context = 'ITEM'
                AND     qpa.product_attribute = 'PRICING_ATTRIBUTE1'
                AND     qpa.product_attribute_datatype = 'C'
                AND     qpa.product_attr_value = to_char(i_inventory_item_id)
                AND     ROWNUM = 1;

          EXCEPTION
            WHEN OTHERS THEN o_list_price := 0 ;
          END ;
          END IF ;

         RETURN o_list_price;
END;

  PROCEDURE GET_CUSTOMER_PURCH_INFO( p_customer_id   IN NUMBER
                                   , p_jobsiteid     IN NUMBER
                                   , p_item_num_tbl IN  xxwc.xxwc_item_num_tbl --Rev 2.4 
                                   , o_cur_output    OUT SYS_REFCURSOR -- return sys_refcursor
                             ) 
                             IS
  BEGIN
     DELETE FROM xxwc.xxwc_md_search_product_gtt_tbl;
   
     --Rev 2.4 < Start
     FOR i IN p_item_num_tbl.FIRST .. p_item_num_tbl.LAST
     LOOP
        BEGIN
           INSERT INTO xxwc.xxwc_md_search_product_gtt_tbl (
                                 partnumber,
                                 shortdescription,  
                                 inventory_item_id, 
                                 primary_uom_code)
                     SELECT msib.segment1
                           ,msib.description  
                           ,msib.inventory_item_id
                           ,msib.primary_uom_code
                       FROM mtl_system_items_b msib
                      WHERE msib.segment1   = p_item_num_tbl (i)
                        AND organization_id = 222;
        EXCEPTION
           WHEN OTHERS
           THEN
              NULL;
        END;
     END LOOP;
                              
     IF p_jobsiteid IS NOT NULL THEN
     --Ver 2.5 <Start  
      OPEN o_cur_output FOR
          'SELECT  xolm.order_number
                 ,xolm.cust_po_number
                 ,xmsp.partnumber
                 ,xmsp.shortdescription part_description 
                 ,xmsp.inventory_item_id
                 ,xolm.ordered_quantity
                 ,xmsp.primary_uom_code
                 ,xolm.unit_list_price
                 ,DECODE(XXWC_OM_SO_CREATE.IS_COMMODITY_ITEMS(xmsp.inventory_item_id), ''Y'', xolm.unit_list_price, xolm.unit_selling_price) unit_selling_price
                 ,xolm.line_gm_percent
                 ,TO_CHAR(xolm.line_creation_date,''YYYY-MM-DD'')||''T''||to_char(sysdate,''HH24:MI:SS'')||''Z'' order_date 
                 ,ood.organization_name warehouse_name 
                 ,ood.organization_code warehouse_code 
                 ,(SELECT LISTAGG(NVL(
                                   (SELECT ffvt.description
                                        FROM fnd_flex_value_sets ffvs ,
                                             fnd_flex_values ffv ,
                                             fnd_flex_values_tl ffvt
                                        WHERE ffvs.flex_value_set_id     = ffv.flex_value_set_id
                                          AND ffv.flex_value_id      = ffvt.flex_value_id
                                          AND ffvt.language          = USERENV(''LANG'')
                                          AND FLEX_VALUE_SET_NAME    = ''WC_MODIFIER_TYPE''
                                          AND ffv.FLEX_VALUE         = qll.attribute1
                                    ),
                                    (SELECT ffvt.description
                                        FROM fnd_flex_value_sets ffvs ,
                                             fnd_flex_values ffv ,
                                             fnd_flex_values_tl ffvt
                                        WHERE ffvs.flex_value_set_id     = ffv.flex_value_set_id
                                          AND ffv.flex_value_id      = ffvt.flex_value_id
                                          AND ffvt.language          = USERENV(''LANG'')
                                          AND FLEX_VALUE_SET_NAME    = ''WC_MODIFIER_TYPE''
                                          AND ffv.FLEX_VALUE         = qlh.attribute10
                                      )
                                    ), '', '') WITHIN GROUP (ORDER BY qlh.name DESC) 
                      FROM ont.oe_price_adjustments opd, qp_list_headers qlh , qp_list_lines qll
                     WHERE header_id          = xolm.header_id
                       AND line_id            = xolm.line_id
                       AND qlh.list_header_id = opd.list_header_id
                       AND qlh.list_header_id = qll.list_header_id
                       AND qll.list_line_id   = opd.list_line_id
                       AND NOT EXISTS
                                     (SELECT 1
                                        FROM qp_list_headers a, qp_list_lines b
                                       WHERE b.list_header_id = a.list_header_id
                                         AND a.active_flag = ''Y''
                                         AND a.list_type_code <> ''PRL''
                                         AND b.qualification_ind = 0
                                         AND b.pricing_phase_id > 1
                                         AND a.list_header_id = opd.list_header_id
                                         AND b.list_line_id = opd.list_line_id)) modifier_type 
            FROM  xxwc.xxwc_md_search_product_gtt_tbl xmsp
                  ,apps.xxwc_om_ord_ln_mv xolm
                  ,apps.org_organization_definitions ood 
            WHERE 1=1
              AND xolm.inventory_item_id   = xmsp.inventory_item_id
              AND xolm.flow_status_code    = ''CLOSED''
              AND xolm.line_category_code  = ''ORDER''
              AND xolm.line_creation_date >= TRUNC(SYSDATE) - 90
              AND xolm.ship_from_org_id    = ood.organization_id 
              AND xolm.ship_to_org_id      = :p_jobsiteid
            ORDER BY xmsp.partnumber'
         using p_jobsiteid;
     ELSE 
        OPEN o_cur_output FOR
          'SELECT  xolm.order_number
                 ,xolm.cust_po_number
                 ,xmsp.partnumber
                 ,xmsp.shortdescription part_description
                 ,xmsp.inventory_item_id
                 ,xolm.ordered_quantity
                 ,xmsp.primary_uom_code
                 ,xolm.unit_list_price
                 ,DECODE(XXWC_OM_SO_CREATE.IS_COMMODITY_ITEMS(xmsp.inventory_item_id), ''Y'', xolm.unit_list_price, xolm.unit_selling_price) unit_selling_price
                 ,xolm.line_gm_percent
                 ,TO_CHAR(xolm.line_creation_date,''YYYY-MM-DD'')||''T''||to_char(sysdate,''HH24:MI:SS'')||''Z'' order_date 
                 ,ood.organization_name warehouse_name  
                 ,ood.organization_code warehouse_code 
                 ,(SELECT LISTAGG(NVL(
                                       (SELECT ffvt.description
                                        FROM fnd_flex_value_sets ffvs ,
                                             fnd_flex_values ffv ,
                                             fnd_flex_values_tl ffvt
                                        WHERE ffvs.flex_value_set_id     = ffv.flex_value_set_id
                                          AND ffv.flex_value_id      = ffvt.flex_value_id
                                          AND ffvt.language          = USERENV(''LANG'')
                                          AND FLEX_VALUE_SET_NAME    = ''WC_MODIFIER_TYPE''
                                          AND ffv.FLEX_VALUE         = qll.attribute1),
                                         (SELECT ffvt.description
                                        FROM fnd_flex_value_sets ffvs ,
                                             fnd_flex_values ffv ,
                                             fnd_flex_values_tl ffvt
                                        WHERE ffvs.flex_value_set_id     = ffv.flex_value_set_id
                                          AND ffv.flex_value_id      = ffvt.flex_value_id
                                          AND ffvt.language          = USERENV(''LANG'')
                                          AND FLEX_VALUE_SET_NAME    = ''WC_MODIFIER_TYPE''
                                          AND ffv.FLEX_VALUE         = qlh.attribute10)
                                          ) , '', '') WITHIN GROUP (ORDER BY qlh.name DESC) 
                      FROM ont.oe_price_adjustments opd, qp_list_headers qlh, qp_list_lines qll
                     WHERE header_id          = xolm.header_id
                       AND line_id            = xolm.line_id
                       AND qlh.list_header_id = qll.list_header_id
                       AND qll.list_line_id   = opd.list_line_id
                       AND qlh.list_header_id = opd.list_header_id
                       AND NOT EXISTS
                                     (SELECT 1
                                        FROM qp_list_headers a, qp_list_lines b
                                       WHERE b.list_header_id = a.list_header_id
                                         AND a.active_flag = ''Y''
                                         AND a.list_type_code <> ''PRL''
                                         AND b.qualification_ind = 0
                                         AND b.pricing_phase_id > 1
                                         AND a.list_header_id = opd.list_header_id
                                         AND b.list_line_id = opd.list_line_id)) modifier_type 
            FROM  xxwc.xxwc_md_search_product_gtt_tbl xmsp
                  ,apps.xxwc_om_ord_ln_mv xolm
                  ,apps.org_organization_definitions ood --Ver 2.5
            WHERE 1=1
              AND xolm.inventory_item_id   = xmsp.inventory_item_id
              AND xolm.flow_status_code    = ''CLOSED''
              AND xolm.line_category_code  = ''ORDER''
              AND xolm.line_creation_date >= TRUNC(SYSDATE) - 90
              AND xolm.ship_from_org_id    = ood.organization_id --Ver 2.5
              AND xolm.sold_to_org_id      = :p_customer_id
            ORDER BY xmsp.partnumber'
         using p_customer_id;
       --Ver 2.5 > End  
     END IF;    
     --Rev 2.4 < End
   END;
  
   --Rev 2.2 > Start
   PROCEDURE GET_CUSTOMER_PURCH_PRODUCTS ( p_customer_id IN NUMBER
                                         , p_jobsiteid   IN NUMBER
                                         , p_item_search IN VARCHAR2
                                         , o_cur_output  OUT SYS_REFCURSOR -- return sys_refcursor
                                         ) 
                             IS
  BEGIN
     DELETE FROM xxwc.xxwc_md_search_product_gtt_tbl;
        
     GET_PRODUCT_INFO_AIS( p_branch_id   =>  222 
                          ,p_customer_id =>  p_customer_id
                          ,p_jobsiteid   =>  p_jobsiteid
                          ,p_item_search =>  p_item_search
                          ,p_filter      =>  'STANDARD'--p_filter
                          ,p_Sort        =>  'PARTNUMBER' --p_Sort
                          ,p_call_pr_api =>  'N'
                          );
                          
     IF p_jobsiteid IS NOT NULL THEN  
      OPEN o_cur_output FOR
          'SELECT DISTINCT xmsp.partnumber
                 ,xmsp.shortdescription part_description
            FROM  xxwc.xxwc_md_search_product_gtt_tbl xmsp
                  ,apps.xxwc_om_ord_ln_mv xolm
            WHERE 1=1
              AND xolm.inventory_item_id   = xmsp.inventory_item_id
              AND xolm.flow_status_code    = ''CLOSED''
              AND xolm.line_category_code  = ''ORDER''
              AND xolm.line_creation_date >= TRUNC(SYSDATE) - 90
              AND xolm.ship_to_org_id      = :p_jobsiteid
            ORDER BY xmsp.partnumber'
         using p_jobsiteid;
     ELSE 
        OPEN o_cur_output FOR
          'SELECT DISTINCT xmsp.partnumber
                 ,xmsp.shortdescription part_description
            FROM  xxwc.xxwc_md_search_product_gtt_tbl xmsp
                  ,apps.xxwc_om_ord_ln_mv xolm
            WHERE 1=1
              AND xolm.inventory_item_id   = xmsp.inventory_item_id
              AND xolm.flow_status_code    = ''CLOSED''
              AND xolm.line_category_code  = ''ORDER''
              AND xolm.line_creation_date >= TRUNC(SYSDATE) - 90
              AND xolm.sold_to_org_id      = :p_customer_id
            ORDER BY xmsp.partnumber'
         using p_customer_id;
     END IF;    
   END;
   --Rev 2.2 > End
        
PROCEDURE GET_PRODUCT_INFO(   p_branch_id   IN NUMBER
                             ,p_customer_id IN NUMBER
                             ,p_jobsiteid   IN NUMBER
                             ,p_item_search IN VARCHAR2
                             ,p_filter      IN VARCHAR2
                             ,p_Sort        IN VARCHAR2
                             ,p_warhouse_num_tbl IN  xxwc.xxwc_warhouse_num_tbl DEFAULT NULL --Rev 2.2
                             ,p_filter_stock IN VARCHAR2 DEFAULT NULL --Rev 2.5
                             ,o_cur_output  OUT SYS_REFCURSOR -- return sys_refcursor
                             )
                             IS
   v_rec sys_refcursor;
   l_count NUMBER;
   
  BEGIN
     --Rev 2.2 > Start
     IF p_warhouse_num_tbl.count >0 THEN
        DELETE FROM xxwc.xxwc_item_avbl_price_tbl;
        DELETE FROM xxwc.xxwc_md_search_product_gtt_tbl;
        
        GET_PRODUCT_INFO_AIS( p_branch_id   =>  p_branch_id 
                             ,p_customer_id =>  p_customer_id
                             ,p_jobsiteid   =>  p_jobsiteid
                             ,p_item_search =>  p_item_search
                             ,p_filter      =>  p_filter
                             ,p_Sort        =>  p_Sort
                             ,p_call_pr_api =>  'N'
                          );
       --Rev 2.5 < Start
       IF p_filter_stock IS NOT NULL THEN
          IF p_filter_stock = 'STOCKABLE' THEN
             DELETE FROM xxwc.xxwc_md_search_product_gtt_tbl xmsp
              WHERE EXISTS (SELECT 1
                              FROM apps.mtl_system_items_b msib
                             WHERE msib.organization_id    = 222
                               AND msib.inventory_item_id  = xmsp.inventory_item_id
                               AND MSIB.stock_enabled_flag = 'N'
                            );
          ELSIF p_filter_stock = 'NONSTOCKABLE' THEN
             DELETE FROM xxwc.xxwc_md_search_product_gtt_tbl xmsp
              WHERE EXISTS (SELECT 1
                              FROM apps.mtl_system_items_b msib
                             WHERE msib.organization_id    = 222
                               AND msib.inventory_item_id  = xmsp.inventory_item_id
                               AND MSIB.stock_enabled_flag = 'Y'
                            );  
          END IF;
       END IF;
       --Rev 2.5 > End                          
                          
        -- insert the records from AIS search into the xxwc_item_avbl_price_tbl
           BEGIN
               FOR j IN p_warhouse_num_tbl.FIRST .. p_warhouse_num_tbl.LAST
               LOOP
                  BEGIN
                     INSERT INTO xxwc.xxwc_item_avbl_price_tbl (
                                 organization_code,
                                 organization_id,
                                 segment1,
                                 part_description,
                                 inventory_item_id, 
                                 primary_uom_code)
                          SELECT mp.organization_code
                                ,mp.organization_id
                                ,xmsp.partnumber
                                ,xmsp.shortdescription 
                                ,xmsp.inventory_item_id
                                ,xmsp.primary_uom_code
                            FROM  xxwc.xxwc_md_search_product_gtt_tbl xmsp
                                , mtl_parameters mp
                                , apps.mtl_system_items_b msib
                           WHERE 1=1
                             AND msib.organization_id   = mp.organization_id
                             AND msib.inventory_item_id = xmsp.inventory_item_id
                             AND mp.organization_code   = p_warhouse_num_tbl (j);
                  EXCEPTION
                  WHEN OTHERS
                  THEN
                     NULL;
                  END;
               END LOOP; --p_warhouse_num_tbl
           EXCEPTION
           WHEN OTHERS  THEN
               NULL;
           END;
        
        line_pricing_data (p_cust_account_id   => p_customer_id,
                           p_site_use_id       => p_jobsiteid);
        
        --Rev 2.5 < Start
        IF p_filter_stock IS NOT NULL AND p_filter_stock = 'AVAILABLE' THEN
           OPEN o_cur_output FOR
                'SELECT aiap.segment1
                      , aiap.part_description
                      , aiap.inventory_item_id
                      , aiap.organization_id
                      , xxwc_inv_ais_pkg.shipto_last_price_paid (:p_jobsiteid, aiap.inventory_item_id) last_price_paid
                      , aiap.modifier  
                      , aiap.SELLING_PRICE list_price
                      , aiap.primary_uom_code primary_uom_code
                      , (NVL(xxwc_ascp_scwb_pkg.get_on_hand(aiap.inventory_item_id, aiap.organization_id, ''G''),0) -
                        NVL(xxwc_ascp_scwb_pkg.get_open_orders_qty(aiap.inventory_item_id, aiap.organization_id),0) ) available_qty
                          FROM xxwc.xxwc_item_avbl_price_tbl aiap
                   WHERE (NVL(xxwc_ascp_scwb_pkg.get_on_hand(aiap.inventory_item_id, aiap.organization_id, ''G''),0) -
                        NVL(xxwc_ascp_scwb_pkg.get_open_orders_qty(aiap.inventory_item_id, aiap.organization_id),0) ) >0      
                  ORDER BY aiap.segment1, aiap.organization_id'
            using p_jobsiteid;
        ELSE --Rev 2.5 > End
           OPEN o_cur_output FOR
                'SELECT aiap.segment1
                      , aiap.part_description
                      , aiap.inventory_item_id
                      , aiap.organization_id
                      , xxwc_inv_ais_pkg.shipto_last_price_paid (:p_jobsiteid, aiap.inventory_item_id) last_price_paid
                      , aiap.modifier  
                      , aiap.SELLING_PRICE list_price
                      , aiap.primary_uom_code primary_uom_code
                      , (NVL(xxwc_ascp_scwb_pkg.get_on_hand(aiap.inventory_item_id, aiap.organization_id, ''G''),0) -
                        NVL(xxwc_ascp_scwb_pkg.get_open_orders_qty(aiap.inventory_item_id, aiap.organization_id),0) ) available_qty
                          FROM xxwc.xxwc_item_avbl_price_tbl aiap
                  ORDER BY aiap.segment1, aiap.organization_id'
            using p_jobsiteid;
        END IF;   --Rev 2.5
     ELSE 
     --Rev 2.2 < End
        --Rev 2.5 < Start
        IF p_filter_stock IS NOT NULL THEN
           IF p_filter_stock = 'STOCKABLE' THEN
               SELECT count(1)
                 INTO l_count
                 FROM MTL_SYSTEM_ITEMS_B msi
                WHERE segment1 = UPPER(p_item_search)
                  AND organization_id = p_branch_id
                  AND msi.stock_enabled_flag = 'Y';
           ELSIF p_filter_stock = 'NONSTOCKABLE' THEN
              SELECT count(1)
              INTO l_count
              FROM MTL_SYSTEM_ITEMS_B msi
             WHERE segment1 = UPPER(p_item_search)
               AND organization_id = p_branch_id
               AND msi.stock_enabled_flag = 'N';
           ELSIF p_filter_stock = 'AVAILABLE' THEN
              SELECT count(1)
              INTO l_count
              FROM MTL_SYSTEM_ITEMS_B msi
             WHERE segment1 = UPPER(p_item_search)
               AND organization_id = p_branch_id
               AND (NVL(xxwc_ascp_scwb_pkg.get_on_hand(msi.inventory_item_id, msi.organization_id, 'G'),0) -
                   NVL(xxwc_ascp_scwb_pkg.get_open_orders_qty(msi.inventory_item_id, msi.organization_id),0) ) >0;
           
           END IF;                  
        ELSE --Rev 2.5 > End    
           SELECT count(1)
             INTO l_count
             FROM MTL_SYSTEM_ITEMS_B msi
            WHERE segment1 = UPPER(p_item_search)
              AND organization_id = p_branch_id;
        END IF;--Rev 2.5
             
        IF l_count = 1 THEN
           OPEN o_cur_output FOR 'SELECT msi.segment1,
             msi.description part_description,
             msi.inventory_item_id,
             msi.organization_id,
             xxwc_inv_ais_pkg.shipto_last_price_paid (:p_jobsiteid, msi.inventory_item_id) last_price_paid,
             NULL,
             XXWC_OM_SO_CREATE.get_list_price( msi.organization_id, msi.inventory_item_id  ) list_price,
             msi.primary_uom_code primary_uom_code,
             (NVL(xxwc_ascp_scwb_pkg.get_on_hand(msi.inventory_item_id, msi.organization_id, ''G''),0) -
             NVL(xxwc_ascp_scwb_pkg.get_open_orders_qty(msi.inventory_item_id, msi.organization_id),0) ) available_qty
              FROM MTL_SYSTEM_ITEMS_B msi
             WHERE segment1 = UPPER(:p_item_search)
               AND msi.organization_id = :p_branch_id
               AND EXISTS ( SELECT 1
                     FROM APPS.XXWC_MD_SEARCH_PRODUCTS_VW xmsp
                    WHERE xmsp.PARTNUMBER = msi.segment1
                      AND xmsp.organization_id = 222)'
          using p_jobsiteid, p_item_search, p_branch_id;
        ELSE
          GET_PRODUCT_INFO_AIS( p_branch_id   =>  p_branch_id --Rev 2.2
                               ,p_customer_id =>  p_customer_id
                               ,p_jobsiteid   =>  p_jobsiteid
                               ,p_item_search =>  p_item_search
                               ,p_filter      =>  p_filter
                               ,p_Sort        =>  p_Sort
                               ,p_call_pr_api =>  'Y' --Rev 2.2
                               );

          --Rev 2.5 < Start
          IF p_filter_stock IS NOT NULL THEN
             IF p_filter_stock = 'STOCKABLE' THEN
                DELETE FROM xxwc.xxwc_md_search_product_gtt_tbl xmsp
                WHERE EXISTS (SELECT 1
                              FROM apps.mtl_system_items_b msib
                             WHERE msib.organization_id    = 222
                               AND msib.inventory_item_id  = xmsp.inventory_item_id
                               AND MSIB.stock_enabled_flag = 'N'
                            );
             ELSIF p_filter_stock = 'NONSTOCKABLE' THEN
                DELETE FROM xxwc.xxwc_md_search_product_gtt_tbl xmsp
                 WHERE EXISTS (SELECT 1
                              FROM apps.mtl_system_items_b msib
                             WHERE msib.organization_id    = 222
                               AND msib.inventory_item_id  = xmsp.inventory_item_id
                               AND MSIB.stock_enabled_flag = 'Y'
                            );  
             END IF;
          END IF;
          --Rev 2.5 > End                          
          
          --Rev 2.5 < Start
          IF p_filter_stock IS NOT NULL AND p_filter_stock = 'AVAILABLE' THEN 
             OPEN o_cur_output FOR 'SELECT msi.segment1,
                msi.description part_description,
                msi.inventory_item_id,
                msi.organization_id,
                xxwc_inv_ais_pkg.shipto_last_price_paid (:p_jobsiteid, msi.inventory_item_id) last_price_paid,
                xmsp.modifier,
                xmsp.SELLING_PRICE list_price,
                --XXWC_OM_SO_CREATE.get_list_price( msi.organization_id, msi.inventory_item_id  ) list_price,
                xmsp.primary_uom_code primary_uom_code,
                (NVL(xxwc_ascp_scwb_pkg.get_on_hand(msi.inventory_item_id, msi.organization_id, ''G''),0) -
                NVL(xxwc_ascp_scwb_pkg.get_open_orders_qty(msi.inventory_item_id, msi.organization_id),0) ) available_qty
                FROM MTL_SYSTEM_ITEMS_B msi, xxwc.xxwc_md_search_product_gtt_tbl xmsp
               WHERE xmsp.inventory_item_id = msi.inventory_item_id
                 AND (NVL(xxwc_ascp_scwb_pkg.get_on_hand(msi.inventory_item_id, msi.organization_id, ''G''),0) -
                      NVL(xxwc_ascp_scwb_pkg.get_open_orders_qty(msi.inventory_item_id, msi.organization_id),0) ) >0
                 AND msi.organization_id = :p_branch_id'
             using p_jobsiteid, p_branch_id;
           ELSE
             OPEN o_cur_output FOR 'SELECT msi.segment1,
                msi.description part_description,
                msi.inventory_item_id,
                msi.organization_id,
                xxwc_inv_ais_pkg.shipto_last_price_paid (:p_jobsiteid, msi.inventory_item_id) last_price_paid,
                xmsp.modifier,
                xmsp.SELLING_PRICE list_price,
                --XXWC_OM_SO_CREATE.get_list_price( msi.organization_id, msi.inventory_item_id  ) list_price,
                xmsp.primary_uom_code primary_uom_code,
                (NVL(xxwc_ascp_scwb_pkg.get_on_hand(msi.inventory_item_id, msi.organization_id, ''G''),0) -
                NVL(xxwc_ascp_scwb_pkg.get_open_orders_qty(msi.inventory_item_id, msi.organization_id),0) ) available_qty
                FROM MTL_SYSTEM_ITEMS_B msi, xxwc.xxwc_md_search_product_gtt_tbl xmsp
               WHERE xmsp.inventory_item_id = msi.inventory_item_id
                 AND msi.organization_id = :p_branch_id'
             using p_jobsiteid, p_branch_id;
           END IF;  
        END IF;
     END IF; --Rev 2.2   
   END;
--Rev 2.2 > Start
PROCEDURE GET_PRODUCT_INFO(   p_branch_id   IN NUMBER
                             ,p_customer_id IN NUMBER
                             ,p_jobsiteid   IN NUMBER
                             ,p_item_search IN VARCHAR2
                             ,p_filter      IN VARCHAR2
                             ,p_Sort        IN VARCHAR2
                             )
                             IS
   v_rec sys_refcursor;
   l_count NUMBER;
  BEGIN
     GET_PRODUCT_INFO_AIS( p_branch_id   =>  p_branch_id 
                          ,p_customer_id =>  p_customer_id
                          ,p_jobsiteid   =>  p_jobsiteid
                          ,p_item_search =>  p_item_search
                          ,p_filter      =>  p_filter
                          ,p_Sort        =>  p_Sort
                          ,p_call_pr_api =>  'Y'
                          );

   END;
   
   /*************************************************************************
     Procedure : refresh_xxwc_om_ord_ln_mv

     PURPOSE:   This procedure is to refresh AIS refresh_xxwc_om_ord_ln_mv

     REVISIONS:
     Ver        Date        Author                  Description
     ---------  ----------  ---------------         -------------------------
     1.0        04/25/2017  Rakesh Patel           Initial Version TMS# TMS#20170417-00208-Mobile Quote Form 0.2 Release - PLSQL api changes  
   ************************************************************************/
    PROCEDURE refresh_xxwc_om_ord_ln_mv (errbuf  OUT VARCHAR2,
                                         retcode OUT VARCHAR2) IS   
       l_error_message CLOB;
       l_conc_req_id   NUMBER := fnd_global.conc_request_id;
       l_sec           VARCHAR2(2000);
    BEGIN
       fnd_file.put_line(fnd_file.log,'Starting XXWC_OM_ORD_LN_MV Refresh');
       fnd_file.put_line(fnd_file.log,' ');
   
       fnd_file.put_line(fnd_file.log,'Refresh Start Time '||to_char(sysdate, 'MM/DD/YYYY HH24:MI:SS'));
       
       l_sec := 'Before XXWC_OM_ORD_LN_MV refresh';
   
       DBMS_SNAPSHOT.REFRESH(LIST =>'APPS.XXWC_OM_ORD_LN_MV'
                            , PUSH_DEFERRED_RPC => FALSE
                            , REFRESH_AFTER_ERRORS =>FALSE
                            , PURGE_OPTION=>0
                            , PARALLELISM =>0
                            , ATOMIC_REFRESH=>FALSE
                            , NESTED=> TRUE);
    
       fnd_file.put_line(fnd_file.log,'Refresh End Time '||to_char(sysdate, 'MM/DD/YYYY HH24:MI:SS'));
       l_sec := 'After XXWC_OM_ORD_LN_MV refresh';
       COMMIT;    
    EXCEPTION
    WHEN OTHERS THEN
       l_error_message := 'refresh_xxwc_om_ord_ln_mv '
                || 'Error_Stack...'
                || DBMS_UTILITY.format_error_stack ()
                || ' Error_Backtrace...'
                || DBMS_UTILITY.format_error_backtrace ();
       xxcus_error_pkg.xxcus_error_main_api (
           p_called_from         => 'XXWC_OM_SO_CREATE.refresh_xxwc_om_ord_ln_mv'
          ,p_calling             => 'refresh_xxwc_om_ord_ln_mv'
          ,p_request_id          => l_conc_req_id
          ,p_ora_error_msg       => l_error_message
          ,p_error_desc          => 'Error refresh_xxwc_om_ord_ln_mv Refresh'
          ,p_distribution_list   => g_dflt_email
          ,p_module              => 'XXWC');

        dbms_output.put_line ('Error in main refresh_xxwc_om_ord_ln_mv. Error: '||l_error_message);
        fnd_file.put_line(fnd_file.log, 'Error in main refresh_xxwc_om_ord_ln_mv. Error: '||l_error_message);

        errbuf := l_error_message;
        retcode := '2';
    END;
   --Rev 2.2 > End

   FUNCTION GET_ORDER_INFO(  p_order_number     IN NUMBER
                            ,p_order_type_id    IN NUMBER
                            ,p_created_by       IN NUMBER
                            ,p_customer_number  IN VARCHAR2
                            ,p_order_from_date  IN VARCHAR2
                            ,p_order_to_date    IN VARCHAR2
                            ,p_Warehouse_id     IN NUMBER
                            ,p_status           IN VARCHAR2
                           ) return sys_refcursor IS
   v_rec sys_refcursor;
   l_org    VARCHAR2 (20)  := '162'; -- OPERATING UNIT
   l_cust_account_id  NUMBER;
   BEGIN

     mo_global.set_policy_context ('S', l_org);

     IF p_customer_number IS NOT NULL THEN
       BEGIN
          SELECT cust_account_id
            INTO l_cust_account_id
            FROM apps.hz_Cust_accounts
           WHERE account_number = p_customer_number;
        EXCEPTION
        WHEN OTHERS THEN
         l_cust_account_id := NULL;
        END;
     END IF;

     IF p_order_number IS NULL AND  p_order_type_id IS NULL AND p_created_by IS NULL AND p_customer_number IS NULL
     AND p_order_from_date IS NULL AND p_Warehouse_id IS NULL THEN
       RETURN NULL;
     ELSIF p_order_number IS NOT NULL AND  p_order_type_id IS NULL AND p_created_by IS NULL AND p_customer_number IS NULL
     AND p_order_from_date IS NULL AND p_Warehouse_id IS NULL THEN
     OPEN
       v_rec for
       'SELECT ooh.order_number
      ,cust_acct.account_number cust_account_code
      ,ooh.header_id
      ,ooh.creation_date order_date
      ,ot.transaction_type_id order_type_id
      ,ot.name order_type
      ,NULL order_source
      ,ooh.order_source_id
      ,oos.name order_source_type
      ,ooh.sold_to_org_id
      ,ooh.ship_to_org_id
      ,ooh.invoice_to_org_id
      ,ooh.ship_from_org_id
      ,ooh.booked_flag
      ,ooh.salesrep_id
      ,XXWC_B2B_SO_IB_PKG.get_sales_rep (ooh.sold_to_org_id ) sales_rep_name
      ,ooh.shipping_method_code
      ,wcs.ship_method_meaning
      ,ooh.freight_carrier_code
      ,ooh.cust_po_number
      ,ooh.flow_status_code
      ,party.party_name customer_name
      ,NULL email_address
      ,ship_loc.address1 ship_address1
      ,ship_loc.address2 ship_address2
      ,ship_loc.address3 ship_address3
      ,ship_loc.address4 ship_address4
      ,ship_loc.city     ship_city
      ,ship_loc.state    ship_state
      ,ship_loc.postal_code ship_to_postal_code
      ,bill_loc.address1 billto_address1
      ,bill_loc.address2 billto_address2
      ,bill_loc.address3 billto_address3
      ,bill_loc.address4 billto_address4
      ,bill_loc.city     billto_city
      ,bill_loc.state    billto_state
      ,bill_loc.postal_code billto_postal_code
      ,ooh.transactional_curr_code currency_code
      ,ooh.fob_point_code
      ,ooh.freight_terms_code
      ,ooh.cust_po_number
      ,ooh.ship_to_contact_id
      ,ship_party.PERSON_LAST_NAME    || DECODE (ship_party.PERSON_FIRST_NAME, NULL, NULL, '', '' || ship_party.PERSON_FIRST_NAME) SHIP_TO_CONTACT
      ,ooh.invoice_to_contact_id
      ,invoice_party.PERSON_LAST_NAME || DECODE (invoice_party.PERSON_FIRST_NAME, NULL, NULL, '', '' || invoice_party.PERSON_FIRST_NAME) INVOICE_TO_CONTACT
      ,hl.LOCATION_CODE
      ,hl.address_line_1 branch_street
      ,DECODE (hl.town_or_city, NULL, NULL, hl.town_or_city || '',''||'')
      || DECODE (hl.region_2, NULL, hl.region_2 || '',''||'' , hl.region_2 || '',''||'''' )
      || DECODE (hl.postal_code, NULL, NULL, hl.postal_code) branch_city
      ,hl.telephone_number_1 branch_tel1
      ,hl.telephone_number_2 branch_tel2
      ,fu.description created_by
      ,ship_su.attribute3 map
      ,XXWC_OM_SO_CREATE.Payment_term(ooh.payment_term_id) CCTDHR
      ,ooh.attribute11 auth_buyer_id
      ,CUST_ACCT.attribute7 AUTH_BUYER
      ,ooh.ORIG_SYS_DOCUMENT_REF ORDER_SOURCE_REFERENCE
      FROM oe_order_headers_all ooh
       , apps.oe_order_sources oos
       , oe_transaction_types_tl ot
       , hz_cust_accounts cust_acct
       , hz_parties party
       , wsh_carrier_services_v wcs
       , hz_cust_site_uses_all ship_su
       , hz_cust_acct_sites_all ship_cas
       , hz_party_sites ship_ps
       , hz_locations ship_loc
       , hz_cust_site_uses_all bill_su
       , hz_cust_acct_sites_all bill_cas
       , hz_party_sites bill_ps
       , hz_locations bill_loc
       , hz_cust_account_roles ship_roles
       , hz_relationships ship_rel
       , hz_parties ship_party
       , hz_cust_accounts ship_acct
       , hz_cust_account_roles invoice_roles
       , hz_parties invoice_party
       , hz_relationships invoice_rel
       , hz_cust_accounts invoice_acct
       , hr_all_organization_units ship_from_org
       , hr_locations_all hl
       , fnd_user fu
      WHERE ooh.order_source_id             = oos.order_source_id
        AND ooh.order_type_id               = ot.transaction_type_id
        AND ooh.sold_to_org_id              = cust_acct.cust_account_id
        AND cust_acct.party_id              = party.party_id(+)
        AND wcs.ship_method_code            = ooh.shipping_method_code
        AND ooh.ship_to_org_id              = ship_su.site_use_id(+)
        AND ship_su.cust_acct_site_id       = ship_cas.cust_acct_site_id(+)
        AND ship_cas.party_site_id          = ship_ps.party_site_id(+)
        AND ship_loc.location_id(+)         = ship_ps.location_id
        AND ooh.invoice_to_org_id           = bill_su.site_use_id(+)
        AND bill_su.cust_acct_site_id       = bill_cas.cust_acct_site_id(+)
        AND bill_cas.party_site_id          = bill_ps.party_site_id(+)
        AND bill_loc.location_id(+)         = bill_ps.location_id
        AND ooh.ship_to_contact_id          = ship_roles.cust_account_role_id(+)
        AND ship_roles.party_id             = ship_rel.party_id(+)
        AND ship_roles.role_type(+)         = ''CONTACT''
        AND ship_roles.cust_account_id      = ship_acct.cust_account_id(+)
        AND NVL (ship_rel.object_id, -1)    = NVL (ship_acct.party_id, -1)
        AND ship_rel.subject_id             = ship_party.party_id(+)
        AND ooh.invoice_to_contact_id       = invoice_roles.cust_account_role_id(+)
        AND invoice_roles.party_id          = invoice_rel.party_id(+)
        AND invoice_roles.role_type(+)      = ''CONTACT''
        AND invoice_roles.cust_account_id   = invoice_acct.cust_account_id(+)
        AND NVL (invoice_rel.object_id, -1) = NVL (invoice_acct.party_id, -1)
        AND invoice_rel.subject_id          = invoice_party.party_id(+)
        AND ooh.ship_from_org_id            = ship_from_org.organization_id(+)
        AND ship_from_org.location_id       = hl.location_id
        AND ooh.created_by                  = fu.user_id
        AND ooh.order_number                = :p_order_number'
       using p_order_number;
     ELSIF p_order_from_date IS NOT NULL THEN
       OPEN
       v_rec for
       'SELECT ooh.order_number
      ,cust_acct.account_number cust_account_code
      ,ooh.header_id
      ,ooh.creation_date order_date
      ,ot.transaction_type_id order_type_id
      ,ot.name order_type
      ,NULL order_source
      ,ooh.order_source_id
      ,oos.name order_source_type
      ,ooh.sold_to_org_id
      ,ooh.ship_to_org_id
      ,ooh.invoice_to_org_id
      ,ooh.ship_from_org_id
      ,ooh.booked_flag
      ,ooh.salesrep_id
      ,XXWC_B2B_SO_IB_PKG.get_sales_rep (ooh.sold_to_org_id ) sales_rep_name
      ,ooh.shipping_method_code
      ,wcs.ship_method_meaning
      ,ooh.freight_carrier_code
      ,ooh.cust_po_number
      ,ooh.flow_status_code
      ,party.party_name customer_name
      ,NULL email_address
      ,ship_loc.address1 ship_address1
      ,ship_loc.address2 ship_address2
      ,ship_loc.address3 ship_address3
      ,ship_loc.address4 ship_address4
      ,ship_loc.city     ship_city
      ,ship_loc.state    ship_state
      ,ship_loc.postal_code ship_to_postal_code
      ,bill_loc.address1 billto_address1
      ,bill_loc.address2 billto_address2
      ,bill_loc.address3 billto_address3
      ,bill_loc.address4 billto_address4
      ,bill_loc.city     billto_city
      ,bill_loc.state    billto_state
      ,bill_loc.postal_code billto_postal_code
      ,ooh.transactional_curr_code currency_code
      ,ooh.fob_point_code
      ,ooh.freight_terms_code
      ,ooh.cust_po_number
      ,ooh.ship_to_contact_id
      ,ship_party.PERSON_LAST_NAME    || DECODE (ship_party.PERSON_FIRST_NAME, NULL, NULL, '', '' || ship_party.PERSON_FIRST_NAME) SHIP_TO_CONTACT
      ,ooh.invoice_to_contact_id
      ,invoice_party.PERSON_LAST_NAME || DECODE (invoice_party.PERSON_FIRST_NAME, NULL, NULL, '', '' || invoice_party.PERSON_FIRST_NAME) INVOICE_TO_CONTACT
      ,hl.LOCATION_CODE
      ,hl.address_line_1 branch_street
      ,DECODE (hl.town_or_city, NULL, NULL, hl.town_or_city || '',''||'')
      || DECODE (hl.region_2, NULL, hl.region_2 || '',''||'' , hl.region_2 || '',''||'''' )
      || DECODE (hl.postal_code, NULL, NULL, hl.postal_code) branch_city
      ,hl.telephone_number_1 branch_tel1
      ,hl.telephone_number_2 branch_tel2
      ,fu.description created_by
      ,ship_su.attribute3 map
      ,XXWC_OM_SO_CREATE.Payment_term(ooh.payment_term_id) CCTDHR
      ,ooh.attribute11 auth_buyer_id
      ,CUST_ACCT.attribute7 AUTH_BUYER
      ,ooh.ORIG_SYS_DOCUMENT_REF ORDER_SOURCE_REFERENCE
      FROM oe_order_headers_all ooh
       , apps.oe_order_sources oos
       , oe_transaction_types_tl ot
       , hz_cust_accounts cust_acct
       , hz_parties party
       , wsh_carrier_services_v wcs
       , hz_cust_site_uses_all ship_su
       , hz_cust_acct_sites_all ship_cas
       , hz_party_sites ship_ps
       , hz_locations ship_loc
       , hz_cust_site_uses_all bill_su
       , hz_cust_acct_sites_all bill_cas
       , hz_party_sites bill_ps
       , hz_locations bill_loc
       , hz_cust_account_roles ship_roles
       , hz_relationships ship_rel
       , hz_parties ship_party
       , hz_cust_accounts ship_acct
       , hz_cust_account_roles invoice_roles
       , hz_parties invoice_party
       , hz_relationships invoice_rel
       , hz_cust_accounts invoice_acct
       , hr_all_organization_units ship_from_org
       , hr_locations_all hl
       , fnd_user fu
      WHERE ooh.order_source_id             = oos.order_source_id
        AND ooh.order_type_id               = ot.transaction_type_id
        AND ooh.sold_to_org_id              = cust_acct.cust_account_id
        AND cust_acct.party_id              = party.party_id(+)
        AND wcs.ship_method_code            = ooh.shipping_method_code
        AND ooh.ship_to_org_id              = ship_su.site_use_id(+)
        AND ship_su.cust_acct_site_id       = ship_cas.cust_acct_site_id(+)
        AND ship_cas.party_site_id          = ship_ps.party_site_id(+)
        AND ship_loc.location_id(+)         = ship_ps.location_id
        AND ooh.invoice_to_org_id           = bill_su.site_use_id(+)
        AND bill_su.cust_acct_site_id       = bill_cas.cust_acct_site_id(+)
        AND bill_cas.party_site_id          = bill_ps.party_site_id(+)
        AND bill_loc.location_id(+)         = bill_ps.location_id
        AND ooh.ship_to_contact_id          = ship_roles.cust_account_role_id(+)
        AND ship_roles.party_id             = ship_rel.party_id(+)
        AND ship_roles.role_type(+)         = ''CONTACT''
        AND ship_roles.cust_account_id      = ship_acct.cust_account_id(+)
        AND NVL (ship_rel.object_id, -1)    = NVL (ship_acct.party_id, -1)
        AND ship_rel.subject_id             = ship_party.party_id(+)
        AND ooh.invoice_to_contact_id       = invoice_roles.cust_account_role_id(+)
        AND invoice_roles.party_id          = invoice_rel.party_id(+)
        AND invoice_roles.role_type(+)      = ''CONTACT''
        AND invoice_roles.cust_account_id   = invoice_acct.cust_account_id(+)
        AND NVL (invoice_rel.object_id, -1) = NVL (invoice_acct.party_id, -1)
        AND invoice_rel.subject_id          = invoice_party.party_id(+)
        AND ooh.ship_from_org_id            = ship_from_org.organization_id(+)
        AND ship_from_org.location_id       = hl.location_id
        AND ooh.created_by                  = fu.user_id
        AND ooh.order_number   = NVL(:p_order_number  , ooh.order_number)
          AND ooh.order_type_id  = NVL(:p_order_type_id ,ooh.order_type_id)
          AND ooh.created_by     = NVL(:p_created_by    ,ooh.created_by)
          AND ooh.sold_to_org_id = NVL(:p_cust_account_id ,ooh.sold_to_org_id )
          AND TRUNC(ooh.ordered_Date) BETWEEN TO_DATE(:p_order_from_date,''YYYY-MM-DD'')  AND TO_DATE(:p_order_to_date,''YYYY-MM-DD'')
          AND ooh.ship_from_org_id = NVL(:p_Warehouse_id, ooh.ship_from_org_id )
          ORDER BY ooh.order_number desc'
       using p_order_number, p_order_type_id, p_created_by, l_cust_account_id, p_order_from_date,p_order_to_date, p_Warehouse_id;
     ELSE
     OPEN
       v_rec for
       'SELECT ooh.order_number
      ,cust_acct.account_number cust_account_code
      ,ooh.header_id
      ,ooh.creation_date order_date
      ,ot.transaction_type_id order_type_id
      ,ot.name order_type
      ,NULL order_source
      ,ooh.order_source_id
      ,oos.name order_source_type
      ,ooh.sold_to_org_id
      ,ooh.ship_to_org_id
      ,ooh.invoice_to_org_id
      ,ooh.ship_from_org_id
      ,ooh.booked_flag
      ,ooh.salesrep_id
      ,XXWC_B2B_SO_IB_PKG.get_sales_rep (ooh.sold_to_org_id ) sales_rep_name
      ,ooh.shipping_method_code
      ,wcs.ship_method_meaning
      ,ooh.freight_carrier_code
      ,ooh.cust_po_number
      ,ooh.flow_status_code
      ,party.party_name customer_name
      ,NULL email_address
      ,ship_loc.address1 ship_address1
      ,ship_loc.address2 ship_address2
      ,ship_loc.address3 ship_address3
      ,ship_loc.address4 ship_address4
      ,ship_loc.city     ship_city
      ,ship_loc.state    ship_state
      ,ship_loc.postal_code ship_to_postal_code
      ,bill_loc.address1 billto_address1
      ,bill_loc.address2 billto_address2
      ,bill_loc.address3 billto_address3
      ,bill_loc.address4 billto_address4
      ,bill_loc.city     billto_city
      ,bill_loc.state    billto_state
      ,bill_loc.postal_code billto_postal_code
      ,ooh.transactional_curr_code currency_code
      ,ooh.fob_point_code
      ,ooh.freight_terms_code
      ,ooh.cust_po_number
      ,ooh.ship_to_contact_id
      ,ship_party.PERSON_LAST_NAME    || DECODE (ship_party.PERSON_FIRST_NAME, NULL, NULL, '', '' || ship_party.PERSON_FIRST_NAME) SHIP_TO_CONTACT
      ,ooh.invoice_to_contact_id
      ,invoice_party.PERSON_LAST_NAME || DECODE (invoice_party.PERSON_FIRST_NAME, NULL, NULL, '', '' || invoice_party.PERSON_FIRST_NAME) INVOICE_TO_CONTACT
      ,hl.LOCATION_CODE
      ,hl.address_line_1 branch_street
      ,DECODE (hl.town_or_city, NULL, NULL, hl.town_or_city || '',''||'')
      || DECODE (hl.region_2, NULL, hl.region_2 || '',''||'' , hl.region_2 || '',''||'''' )
      || DECODE (hl.postal_code, NULL, NULL, hl.postal_code) branch_city
      ,hl.telephone_number_1 branch_tel1
      ,hl.telephone_number_2 branch_tel2
      ,fu.description created_by
      ,ship_su.attribute3 map
      ,XXWC_OM_SO_CREATE.Payment_term(ooh.payment_term_id) CCTDHR
      ,ooh.attribute11 auth_buyer_id
      ,CUST_ACCT.attribute7 AUTH_BUYER
      ,ooh.ORIG_SYS_DOCUMENT_REF ORDER_SOURCE_REFERENCE
      FROM oe_order_headers_all ooh
       , apps.oe_order_sources oos
       , oe_transaction_types_tl ot
       , hz_cust_accounts cust_acct
       , hz_parties party
       , wsh_carrier_services_v wcs
       , hz_cust_site_uses_all ship_su
       , hz_cust_acct_sites_all ship_cas
       , hz_party_sites ship_ps
       , hz_locations ship_loc
       , hz_cust_site_uses_all bill_su
       , hz_cust_acct_sites_all bill_cas
       , hz_party_sites bill_ps
       , hz_locations bill_loc
       , hz_cust_account_roles ship_roles
       , hz_relationships ship_rel
       , hz_parties ship_party
       , hz_cust_accounts ship_acct
       , hz_cust_account_roles invoice_roles
       , hz_parties invoice_party
       , hz_relationships invoice_rel
       , hz_cust_accounts invoice_acct
       , hr_all_organization_units ship_from_org
       , hr_locations_all hl
       , fnd_user fu
      WHERE ooh.order_source_id             = oos.order_source_id
        AND ooh.order_type_id               = ot.transaction_type_id
        AND ooh.sold_to_org_id              = cust_acct.cust_account_id
        AND cust_acct.party_id              = party.party_id(+)
        AND wcs.ship_method_code            = ooh.shipping_method_code
        AND ooh.ship_to_org_id              = ship_su.site_use_id(+)
        AND ship_su.cust_acct_site_id       = ship_cas.cust_acct_site_id(+)
        AND ship_cas.party_site_id          = ship_ps.party_site_id(+)
        AND ship_loc.location_id(+)         = ship_ps.location_id
        AND ooh.invoice_to_org_id           = bill_su.site_use_id(+)
        AND bill_su.cust_acct_site_id       = bill_cas.cust_acct_site_id(+)
        AND bill_cas.party_site_id          = bill_ps.party_site_id(+)
        AND bill_loc.location_id(+)         = bill_ps.location_id
        AND ooh.ship_to_contact_id          = ship_roles.cust_account_role_id(+)
        AND ship_roles.party_id             = ship_rel.party_id(+)
        AND ship_roles.role_type(+)         = ''CONTACT''
        AND ship_roles.cust_account_id      = ship_acct.cust_account_id(+)
        AND NVL (ship_rel.object_id, -1)    = NVL (ship_acct.party_id, -1)
        AND ship_rel.subject_id             = ship_party.party_id(+)
        AND ooh.invoice_to_contact_id       = invoice_roles.cust_account_role_id(+)
        AND invoice_roles.party_id          = invoice_rel.party_id(+)
        AND invoice_roles.role_type(+)      = ''CONTACT''
        AND invoice_roles.cust_account_id   = invoice_acct.cust_account_id(+)
        AND NVL (invoice_rel.object_id, -1) = NVL (invoice_acct.party_id, -1)
        AND invoice_rel.subject_id          = invoice_party.party_id(+)
        AND ooh.ship_from_org_id            = ship_from_org.organization_id(+)
        AND ship_from_org.location_id       = hl.location_id
        AND ooh.created_by                  = fu.user_id
        AND ooh.order_number   = NVL(:p_order_number  , ooh.order_number)
        AND ooh.order_type_id  = NVL(:p_order_type_id ,ooh.order_type_id)
        AND ooh.created_by     = NVL(:p_created_by    ,ooh.created_by)
        AND ooh.sold_to_org_id = NVL(:p_cust_account_id ,ooh.sold_to_org_id )
        AND ooh.ship_from_org_id = NVL(:p_Warehouse_id, ooh.ship_from_org_id )
        ORDER BY ooh.order_number desc'
       using p_order_number, p_order_type_id, p_created_by, l_cust_account_id, p_Warehouse_id;
     END IF;

       RETURN v_rec;
   END;

   FUNCTION GET_LINE_INFO(  p_header_id     IN NUMBER
                           ) return sys_refcursor IS
   v_rec sys_refcursor;
   l_org    VARCHAR2 (20)  := '162'; -- OPERATING UNIT
   l_cust_account_id  NUMBER;
   BEGIN

      mo_global.set_policy_context ('S', l_org);
      mo_global.init ('ONT');

     OPEN
       v_rec for
       'SELECT ool.line_id
              ,ool.line_number
              ,ool.ordered_item SEGMENT1
              ,msib.description part_description
              ,ool.ship_from_org_id    line_ship_from_org_id
              ,ool.inventory_item_id
              ,ool.ordered_quantity QTY_ORDERED
              ,XXWC_OM_SO_CREATE.get_onhand (      p_inventory_item_id => ool.inventory_item_id
                                ,p_organization_id  =>  ool.ship_from_org_id
                                ,p_base_uom         => msib.primary_unit_of_measure
                                ,p_ord_uom          => um.unit_of_measure
                                ,p_return_type      =>  ''H''
                                ) onhand
              ,XXWC_OM_SO_CREATE.get_onhand (      p_inventory_item_id => ool.inventory_item_id
                                ,p_organization_id  =>  ool.ship_from_org_id
                                ,p_base_uom         => msib.primary_unit_of_measure
                                ,p_ord_uom          => um.unit_of_measure
                                ,p_return_type      =>  ''T''
                                ) available
              ,ool.attribute11 Forceship
              ,ool.shipped_quantity QTY_SHIPPED
              ,ool.cancelled_quantity
              ,ool.cancelled_quantity
              ,ool.fulfilled_quantity
              ,ool.unit_list_price
              ,ool.unit_selling_price
              ,NVL (ool.ordered_quantity, 0) * NVL (ool.unit_selling_price, 0) extended_price
              ,ool.tax_value
              ,ool.ORDER_QUANTITY_UOM UOM
              ,ool.shipping_quantity_uom
              ,ool.line_type_id
              ,ool.ship_to    line_ship_to
              ,ool.ship_from_location line_ship_from_location
              ,ool.ship_to_address1    line_ship_to_address1
              ,ool.ship_to_address2 line_ship_to_address2
              ,ool.deliver_to line_deliver_to
              ,ool.deliver_to_location    line_deliver_to_location
              ,ool.deliver_to_address1    line_deliver_to_address1
              ,ool.invoice_to line_invoice_to
              ,ool.invoice_to_location    line_invoice_to_location
              ,ool.invoice_to_address1    line_invoice_to_address1
              ,ool.invoice_to_address2 line_invoice_to_address2
              ,ool.flow_status_code line_status
              ,ool.user_item_description
              ,msib.unit_weight unit_weight
              ,( SELECT  LISTAGG(l.segment1, ''|''||'' '') WITHIN GROUP (ORDER BY l.segment1)
                   FROM mtl_item_locations l
                  , mtl_secondary_locators_all_v v
                WHERE l.inventory_location_id = v.secondary_locator
                  AND v.inventory_item_id     = msib.inventory_item_id
                  AND v.organization_id       = ool.ship_from_org_id) LOCATORS
              ,xxwc_om_cfd_pkg.vendor_part_num(ool.inventory_item_id) vendor_part_num
              ,um.unit_of_measure ord_uom
               ,msib.primary_unit_of_measure base_uom
         FROM oe_order_lines_v ool
            , mtl_system_items_b msib
            , mtl_units_of_measure_vl um
        WHERE 1=1
          AND msib.inventory_item_id = ool.inventory_item_id
          AND msib.organization_id   = ool.ship_from_org_id
          AND ool.order_quantity_uom = um.uom_code
          AND ool.header_id          = :p_header_id
          ORDER BY ool.line_number'
       using p_header_id;
       RETURN v_rec;
   END;

  /*******************************************************************************
   Procedure Name   :   CALCULATE_TAX
   Description     :   This function will be called by DF to calculate tax

   Change History  :
   DATE         NAME              Modification
   --------     -------           ------------------------------------------
   22-Nov-16    Rakesh Patel      Initial  Draft.
   ******************************************************************************/

PROCEDURE CALCUALTE_TAX
(
     p_order_header_rec     IN  OE_Order_PUB.Header_Rec_Type
    ,p_order_line_rec       IN  OE_Order_PUB.Line_Rec_Type
    --,o_order_out_rec        OUT  xxwc.xxwc_order_out_rec
    ,o_x_tax_value          OUT NUMBER
)
IS
   l_sec     VARCHAR2(3000);
   l_err_msg VARCHAR2(3000);
   l_return_status                VARCHAR2 (2000);
   -- PARAMETERS
   l_org                          VARCHAR2 (20)                          := '162'; -- OPERATING UNIT
   -- INPUT VARIABLES FOR PROCESS_ORDER API
   o_tax_value                    NUMBER;
   l_header_rec                   oe_order_pub.header_rec_type;
   l_line_rec                     oe_order_pub.Line_Rec_Type;
   -- OUT VARIABLES FOR PROCESS_ORDER API
   l_tax_out_tbl                 OM_TAX_UTIL.om_tax_out_tab_type;

   l_user_id                      NUMBER;
   l_resp_id                      NUMBER;
   l_resp_appl_id                 NUMBER;
BEGIN
   l_sec := 'Start CALCUALTE_TAX';

   -- INITIALIZATION REQUIRED FOR R12
   mo_global.set_policy_context ('S', l_org);
   mo_global.init ('ONT');

   l_user_id      := 40236;
   l_resp_id      := 50857;
   l_resp_appl_id := 660;

   --INITIALIZE ENVIRONMENT
   fnd_global.apps_initialize (l_user_id, l_resp_id, l_resp_appl_id);

   -- INITIALIZE HEADER RECORD
   l_header_rec := oe_order_pub.g_miss_header_rec;

   l_header_rec.header_id               := p_order_header_rec.header_id;
   l_header_rec.transactional_curr_code := p_order_header_rec.transactional_curr_code;
   l_header_rec.conversion_rate         := p_order_header_rec.conversion_rate;
   l_header_rec.order_type_id           := p_order_header_rec.order_type_id;
   l_header_rec.order_source_id         := p_order_header_rec.order_source_id;
   l_header_rec.sold_to_org_id          := p_order_header_rec.sold_to_org_id;
   l_header_rec.ship_to_org_id          := p_order_header_rec.ship_to_org_id;
   l_header_rec.orig_sys_document_ref   := p_order_header_rec.orig_sys_document_ref;
   l_header_rec.invoice_to_org_id       := p_order_header_rec.invoice_to_org_id;
   l_header_rec.ship_from_org_id        := p_order_header_rec.ship_from_org_id;
   l_header_rec.cust_po_number          := p_order_header_rec.cust_po_number; -- Customer PO number
  -- l_header_rec.operation             := oe_globals.g_opr_create;
   l_header_rec.booked_flag             := p_order_header_rec.booked_flag;
   l_header_rec.ordered_date            := SYSDATE;
   l_header_rec.salesrep_id             := p_order_header_rec.salesrep_id;
   l_header_rec.org_id                  := p_order_header_rec.org_id;
   l_header_rec.shipping_method_code    := p_order_header_rec.shipping_method_code;
   l_header_rec.freight_carrier_code    := p_order_header_rec.freight_carrier_code;

   -- INITIALIZE LINE RECORD
   l_line_rec := oe_order_pub.g_miss_line_rec;
   l_line_rec.inventory_item_id      := p_order_line_rec.inventory_item_id;
   l_line_rec.ordered_quantity       := p_order_line_rec.ordered_quantity;
   l_line_rec.unit_selling_price     := p_order_line_rec.unit_selling_price;
   l_line_rec.tax_exempt_number      := p_order_line_rec.tax_exempt_number;
   l_line_rec.tax_exempt_reason_code := p_order_line_rec.tax_exempt_reason_code;
   l_line_rec.ship_from_org_id       := p_order_line_rec.ship_from_org_id;
   l_line_rec.subinventory           := p_order_line_rec.subinventory;
   l_line_rec.line_type_id           := p_order_line_rec.line_type_id;
   l_line_rec.line_id                := p_order_line_rec.line_id;
   l_line_rec.header_id              := p_order_line_rec.header_id;
   l_line_rec.tax_date               := p_order_line_rec.tax_date;
   l_line_rec.fob_point_code         := p_order_line_rec.fob_point_code;
   l_line_rec.tax_code               := p_order_line_rec.tax_code;
   l_line_rec.schedule_ship_date     := p_order_line_rec.schedule_ship_date;
   l_line_rec.order_quantity_uom     := p_order_line_rec.order_quantity_uom;
   l_line_rec.line_number            := p_order_line_rec.line_number;
   l_line_rec.user_item_description  := p_order_line_rec.user_item_description;
   l_line_rec.invoice_to_org_id      := p_order_line_rec.invoice_to_org_id;
   l_line_rec.Line_Type_id           := p_order_line_rec.Line_Type_id;

   OM_TAX_UTIL.TAX_LINE( p_line_rec       => l_line_rec,
                         p_header_rec     => l_header_rec,
                         x_tax_value      => o_tax_value,
                         x_tax_out_tbl    => l_tax_out_tbl,
                         x_return_status  => l_return_status
                         );

      IF l_return_status = fnd_api.g_ret_sts_success
      THEN
        DBMS_OUTPUT.put_line ('Tax sucessfully Calculated');
       o_x_tax_value := o_tax_value;
      END IF;

      RETURN;
    EXCEPTION
    WHEN OTHERS THEN
      xxcus_error_pkg.xxcus_error_main_api (
         p_called_from         => 'XXWC_OM_SO_CREATE.CALCUALTE_TAX'
        ,p_calling             => l_sec
        ,p_request_id          => fnd_global.conc_request_id
        ,p_ora_error_msg       => SQLERRM
        ,p_error_desc          => 'Error when calulation tax'
        ,p_distribution_list   => g_dflt_email
        ,p_module              => 'XXWC');
       RETURN;
    END CALCUALTE_TAX; --end of procedure

 /*************************************************************************
      PROCEDURE Name: create_contact

      PURPOSE:   To create contact when contact does not exist in Oracle

      REVISIONS:
      Ver        Date        Author                     Description
      ---------  ----------  ---------------         -------------------------
      1.0        04/09/2014  Gopi Damuluri             Initial Version
   ****************************************************************************/
   PROCEDURE create_contact (p_customer_id       IN     NUMBER,
                             p_first_name        IN     VARCHAR2,
                             p_last_name         IN     VARCHAR2,
                             p_contact_num       IN     VARCHAR2,
                             p_email_id          IN     VARCHAR2,
                             p_fax_num           IN     VARCHAR2,
                             p_cust_contact_id      OUT NUMBER,
                             p_error_message        OUT VARCHAR2)
   IS
      l_party_id               NUMBER;
      p_create_person_rec      HZ_PARTY_V2PUB.person_rec_type;
      x_party_id               NUMBER;
      x_party_number           VARCHAR2 (2000);
      x_profile_id             NUMBER;
      x_return_status          VARCHAR2 (2000);
      x_msg_count              NUMBER;
      x_msg_data               VARCHAR2 (2000);

      p_org_contact_rec        HZ_PARTY_CONTACT_V2PUB.ORG_CONTACT_REC_TYPE;
      x_org_contact_id         NUMBER;
      x_party_rel_id           NUMBER;
      x_party_id2              NUMBER;
      x_party_number2          VARCHAR2 (2000);

      p_cr_cust_acc_role_rec   HZ_CUST_ACCOUNT_ROLE_V2PUB.cust_account_role_rec_type;
      x_cust_account_role_id   NUMBER;
      l_exception              EXCEPTION;

      p_contact_point_rec      HZ_CONTACT_POINT_V2PUB.CONTACT_POINT_REC_TYPE;

      p_edi_rec                HZ_CONTACT_POINT_V2PUB.EDI_REC_TYPE;
      p_email_rec              HZ_CONTACT_POINT_V2PUB.EMAIL_REC_TYPE;
      p_phone_rec              HZ_CONTACT_POINT_V2PUB.PHONE_REC_TYPE;
      p_telex_rec              HZ_CONTACT_POINT_V2PUB.TELEX_REC_TYPE;
      p_web_rec                HZ_CONTACT_POINT_V2PUB.WEB_REC_TYPE;

      x_contact_point_id       NUMBER;
      l_sec                    VARCHAR2 (100);
   BEGIN
      l_sec := 'Derive Customer Party Id';

      ------------------------------------
      -- Derive Customer Party Id
      ------------------------------------
      SELECT party_id
        INTO l_party_id
        FROM hz_cust_accounts_all hca
       WHERE cust_account_id = p_customer_id;

      l_sec := 'Create a definition contact';

      ------------------------------------
      -- 1. Create a definition contact
      ------------------------------------
      IF p_first_name IS NULL OR p_last_name IS NULL
      THEN
         p_error_message := 'First Name or Last Name is empty';
         RAISE l_exception;
      ELSE
         p_create_person_rec.person_first_name := p_first_name;
         p_create_person_rec.person_last_name := p_last_name;
      END IF;

      p_create_person_rec.created_by_module := 'TCA_V1_API';

      HZ_PARTY_V2PUB.create_person ('T',
                                    p_create_person_rec,
                                    x_party_id,
                                    x_party_number,
                                    x_profile_id,
                                    x_return_status,
                                    x_msg_count,
                                    x_msg_data);

      IF x_return_status != 'S'
      THEN
         p_error_message := x_msg_data;
         RAISE l_exception;
      END IF;

      l_sec := 'Create a relation cont-org using party_id from step 1';
      ------------------------------------
      -- 2. Create a relation cont-org using party_id from step 1
      ------------------------------------
      p_org_contact_rec.created_by_module := 'TCA_V1_API';
      p_org_contact_rec.party_rel_rec.subject_id := x_party_id; --<<value for party_id from step 1>
      p_org_contact_rec.party_rel_rec.subject_type := 'PERSON';
      p_org_contact_rec.party_rel_rec.subject_table_name := 'HZ_PARTIES';
      p_org_contact_rec.party_rel_rec.object_id := l_party_id; --<<value for party_id from step 0>
      p_org_contact_rec.party_rel_rec.object_type := 'ORGANIZATION';
      p_org_contact_rec.party_rel_rec.object_table_name := 'HZ_PARTIES';
      p_org_contact_rec.party_rel_rec.relationship_code := 'CONTACT_OF';
      p_org_contact_rec.party_rel_rec.relationship_type := 'CONTACT';
      p_org_contact_rec.party_rel_rec.start_date := SYSDATE;
      --         p_org_contact_rec.contact_number                   := p_contact_num;

      hz_party_contact_v2pub.create_org_contact ('T',
                                                 p_org_contact_rec,
                                                 x_org_contact_id,
                                                 x_party_rel_id,
                                                 x_party_id2,
                                                 x_party_number,
                                                 x_return_status,
                                                 x_msg_count,
                                                 x_msg_data);

      IF x_return_status != 'S'
      THEN
         p_error_message := x_msg_data;
         RAISE l_exception;
      END IF;

      IF p_contact_num IS NOT NULL
      THEN
         l_sec := 'Create a Phone Contact Point using party_id';
         ------------------------------------------------------------------------------------------------------------
         -- 3.1 Create a Phone Contact Point using party_id
         ------------------------------------------------------------------------------------------------------------
         -- Initializing the Mandatory API parameters
         p_contact_point_rec.contact_point_type := 'PHONE';
         p_contact_point_rec.owner_table_name := 'HZ_PARTIES';
         p_contact_point_rec.owner_table_id := x_party_id2;      --l_party_id;
         p_contact_point_rec.primary_flag := 'Y';
         p_contact_point_rec.contact_point_purpose := 'BUSINESS';
         p_contact_point_rec.created_by_module := 'BO_API';

         p_phone_rec.phone_area_code := NULL;
         p_phone_rec.phone_country_code := '1';
         p_phone_rec.phone_number := p_contact_num;
         p_phone_rec.phone_line_type := 'MOBILE';

         fnd_file.put_line (
            fnd_file.LOG,
            'Calling the API hz_contact_point_v2pub.create_contact_point');

         HZ_CONTACT_POINT_V2PUB.CREATE_CONTACT_POINT (
            p_init_msg_list       => FND_API.G_TRUE,
            p_contact_point_rec   => p_contact_point_rec,
            p_edi_rec             => p_edi_rec,
            p_email_rec           => p_email_rec,
            p_phone_rec           => p_phone_rec,
            p_telex_rec           => p_telex_rec,
            p_web_rec             => p_web_rec,
            x_contact_point_id    => x_contact_point_id,
            x_return_status       => x_return_status,
            x_msg_count           => x_msg_count,
            x_msg_data            => x_msg_data);

         IF x_return_status != 'S'
         THEN
            p_error_message := x_msg_data;
            RAISE l_exception;
         END IF;
      END IF;                             -- IF p_contact_num IS NOT NULL THEN

      l_sec := 'Create a Email Contact Point using party_id';

      ------------------------------------------------------------------------------------------------------------
      -- 3.2 Create a Email Contact Point using party_id
      ------------------------------------------------------------------------------------------------------------
      IF p_email_id IS NOT NULL
      THEN
         -- Initializing the Mandatory API parameters
         p_contact_point_rec.contact_point_type := 'EMAIL';
         p_contact_point_rec.owner_table_name := 'HZ_PARTIES';
         p_contact_point_rec.owner_table_id := x_party_id2;      --l_party_id;
         p_contact_point_rec.primary_flag := 'Y';
         p_contact_point_rec.contact_point_purpose := 'BUSINESS';
         p_contact_point_rec.created_by_module := 'BO_API';

         p_email_rec.email_format := 'MAILHTML';
         p_email_rec.email_address := p_email_id;

         fnd_file.put_line (
            fnd_file.LOG,
            'Calling the API hz_contact_point_v2pub.create_contact_point');

         HZ_CONTACT_POINT_V2PUB.CREATE_CONTACT_POINT (
            p_init_msg_list       => FND_API.G_TRUE,
            p_contact_point_rec   => p_contact_point_rec,
            p_edi_rec             => p_edi_rec,
            p_email_rec           => p_email_rec,
            p_phone_rec           => p_phone_rec,
            p_telex_rec           => p_telex_rec,
            p_web_rec             => p_web_rec,
            x_contact_point_id    => x_contact_point_id,
            x_return_status       => x_return_status,
            x_msg_count           => x_msg_count,
            x_msg_data            => x_msg_data);

         IF x_return_status != 'S'
         THEN
            p_error_message := x_msg_data;
            RAISE l_exception;
         END IF;
      END IF;                                -- IF p_email_id IS NOT NULL THEN

      l_sec := 'Create a contact using party_id you get in step 3';
      ------------------------------------------------------------------------------------------------------------
      -- 4. Create a contact using party_id you get in step 3
      ------------------------------------------------------------------------------------------------------------
      p_cr_cust_acc_role_rec.party_id := x_party_id2; --<<value for party_id from step 2>
      p_cr_cust_acc_role_rec.cust_account_id := p_customer_id; --<<value for cust_account_id from step 0>
      p_cr_cust_acc_role_rec.role_type := 'CONTACT';
      p_cr_cust_acc_role_rec.created_by_module := 'TCA_V1_API';

      HZ_CUST_ACCOUNT_ROLE_V2PUB.CREATE_CUST_ACCOUNT_ROLE (
         'T',
         p_cr_cust_acc_role_rec,
         x_cust_account_role_id,
         x_return_status,
         x_msg_count,
         x_msg_data);

      IF x_return_status != 'S'
      THEN
         p_error_message := x_msg_data;
         RAISE l_exception;
      ELSE
         p_cust_contact_id := x_cust_account_role_id;
         COMMIT;
      END IF;
   EXCEPTION
      WHEN l_exception
      THEN
         DBMS_OUTPUT.put_line (
               'p_customer_id - '
            || p_customer_id
            || ' in L_EXCEPTION, p_error_message - '
            || p_error_message);
      WHEN OTHERS
      THEN
         ROLLBACK;
         p_cust_contact_id := NULL;
         fnd_file.put_line (
            fnd_file.LOG,
               'p_customer_id - '
            || p_customer_id
            || ' error in  CREATE_CONTACT procedure - '
            || SQLERRM);
   END create_contact;

   /*************************************************************************
    Input Parameters:  a. p_xxwc_address_rec - address info.

    Output Parameters: o_order_out_rec is a record type and will have following column
                       o_error_code - 'E' - Error or 'S'- Success
                       o_error_msg  - Error message, reason for the failure.
**************************************************************************/
   PROCEDURE CREATE_JOB_SITE
   (
     p_xxwc_address_rec     IN  xxwc.xxwc_address_rec
    ,o_status               OUT VARCHAR2
    ,o_ship_to_site_use_id  OUT NUMBER
    ,o_bill_to_site_use_id  OUT NUMBER
    ,o_message              OUT VARCHAR2
   ) IS
   l_exception              EXCEPTION;
   l_party_id               NUMBER;
   l_user_id                NUMBER;
   l_resp_id                NUMBER;
   l_resp_appl_id           NUMBER;
   l_sec                    VARCHAR2(4000);
   l_error_message          VARCHAR2(4000);
   x_return_status          VARCHAR2 (2000);
   x_msg_count              NUMBER;
   x_msg_data               VARCHAR2 (2000);

   --location api variable
   P_location_rec           HZ_LOCATION_V2PUB.LOCATION_REC_TYPE;
   x_location_id            NUMBER;

   --party site api variable
   p_party_site_rec         hz_party_site_v2pub.party_site_rec_type;
   x_party_site_id          NUMBER;
   x_party_site_number      VARCHAR2(2000);

   --cust account site api variable
   p_cust_acct_site_rec     hz_cust_account_site_v2pub.cust_acct_site_rec_type;
   x_cust_acct_site_id      NUMBER;

   --cust account site use api variable
   p_cust_site_use_rec      HZ_CUST_ACCOUNT_SITE_V2PUB.CUST_SITE_USE_REC_TYPE;
   p_customer_profile_rec   HZ_CUSTOMER_PROFILE_V2PUB.CUSTOMER_PROFILE_REC_TYPE;
   x_party_site_use_id      NUMBER;
   x_site_use_id            NUMBER;
   l_user_name              apps.fnd_user.user_name%TYPE;--TMS#20170313-00308 
BEGIN
      l_sec := 'Derive Customer Party Id';

      -- Setting the Context --
      mo_global.init('AR');

     IF IS_VALID_OM_USER ( p_xxwc_address_rec.user_id ) = 'Y' THEN --TMS#20170313-00308 
       l_user_id := p_xxwc_address_rec.user_id;
     ELSE
        --TMS#20170313-00308 Start
        BEGIN 
           SELECT USER_NAME    
             INTO l_user_name
            FROM apps.fnd_user
           WHERE user_id = p_xxwc_address_rec.user_id;
        EXCEPTION
        WHEN OTHERS THEN
           l_user_name := NULL;
        END; 
        o_status  := 'E';
        o_message := 'Responsibility not assigned to NTID : ' ||l_user_name||' to create/ update sales order ';
        RETURN;
        --TMS#20170313-00308 End
     END IF;
   
      l_resp_id      := 51208;
      l_resp_appl_id := 222;

      mo_global.set_org_context ( 162, NULL, 'AR' ) ;
  
      --INITIALIZE ENVIRONMENT
      fnd_global.apps_initialize (l_user_id, l_resp_id, l_resp_appl_id);

      mo_global.set_policy_context('S',162);
      fnd_global.set_nls_context('AMERICAN');

      ------------------------------------
      -- Derive Customer Party Id
      ------------------------------------
      BEGIN
        SELECT party_id
          INTO l_party_id
          FROM hz_cust_accounts_all hca
         WHERE cust_account_id = p_xxwc_address_rec.customer_id;
      EXCEPTION
      WHEN OTHERS THEN
         o_message := 'INVALID CUSTOMER '||' p_xxwc_address_rec.customer_id '||x_msg_data;
         RAISE l_exception;
      END;

      l_sec := 'Create a location';

      ------------------------------------
      -- 1. Create a location
      ------------------------------------
       p_location_rec.country           := p_xxwc_address_rec.country;--'US';
       p_location_rec.address1          := p_xxwc_address_rec.address1;--'Shareoracleapps';
       p_location_rec.address2          := p_xxwc_address_rec.address2;
       p_location_rec.address3          := p_xxwc_address_rec.address3;
       p_location_rec.address4          := p_xxwc_address_rec.address4;
       p_location_rec.city              := p_xxwc_address_rec.city;--san Mateo';
       p_location_rec.county            := p_xxwc_address_rec.county;--san Mateo';
       p_location_rec.postal_code       := p_xxwc_address_rec.postal_code;--94401';
       p_location_rec.state             := p_xxwc_address_rec.state;--'CA';
       p_location_rec.created_by_module := 'TCA_V1_API';

       HZ_LOCATION_V2PUB.CREATE_LOCATION
           (
             p_init_msg_list => FND_API.G_TRUE,
             p_location_rec  => p_location_rec,
             x_location_id   => x_location_id,
             x_return_status => x_return_status,
             x_msg_count     => x_msg_count,
             x_msg_data      => x_msg_data);


      IF x_return_status != 'S'
      THEN
         o_message := 'CREATE_LOCATION'||x_msg_data;
         RAISE l_exception;
      END IF;

      l_sec := 'Create a party site';
      ------------------------------------
      -- 2. Create party site
      ------------------------------------
       -- Initializing the Mandatory API parameters
       p_party_site_rec.party_id                 := l_party_id;
       p_party_site_rec.location_id              := x_location_id;
       p_party_site_rec.identifying_address_flag := 'Y';
       p_party_site_rec.party_site_name          := p_xxwc_address_rec.location;
       p_party_site_rec.created_by_module        := 'TCA_V1_API';

       l_sec:= 'Calling the API hz_party_site_v2pub.create_party_site';

       HZ_PARTY_SITE_V2PUB.CREATE_PARTY_SITE
                   (
                    p_init_msg_list     => FND_API.G_TRUE,
                    p_party_site_rec    => p_party_site_rec,
                    x_party_site_id     => x_party_site_id,
                    x_party_site_number => x_party_site_number,
                    x_return_status     => x_return_status,
                    x_msg_count         => x_msg_count,
                    x_msg_data          => x_msg_data
                           );
      IF x_return_status != 'S'
      THEN
         o_message := 'CREATE_PARTY_SITE'||x_msg_data;
         RAISE l_exception;
      END IF;

      l_sec := 'Create Cust acct site';
      ------------------------------------
      -- 4. Cust acct site use
      ------------------------------------
      p_cust_acct_site_rec.cust_account_id   := p_xxwc_address_rec.customer_id;
      p_cust_acct_site_rec.party_site_id     := x_party_site_id;
      p_cust_acct_site_rec.created_by_module := 'TCA_V1_API';

      l_sec:= 'Calling the API hz_cust_account_site_v2pub.create_cust_acct_site';

      hz_cust_account_site_v2pub.create_cust_acct_site (    'T'                   ,
                                                                p_cust_acct_site_rec  ,
                                                                x_cust_acct_site_id   ,
                                                                x_return_status       ,
                                                                x_msg_count           ,
                                                                x_msg_data
                                                            ) ;
     IF x_return_status != 'S'
     THEN
       o_message := 'CREATE_CUST_ACCT_SITE'||x_msg_data;
       RAISE l_exception;
     END IF;

     IF p_xxwc_address_rec.ship_to_flag = 'Y' THEN
        p_cust_site_use_rec.cust_acct_site_id := x_cust_acct_site_id;
        p_cust_site_use_rec.site_use_code     := 'SHIP_TO';
       -- p_cust_site_use_rec.primary_flag      := 'Y';
        p_cust_site_use_rec.location          :=  p_xxwc_address_rec.location;
        p_cust_site_use_rec.created_by_module := 'TCA_V1_API';

        l_sec:= 'Calling the API hz_party_site_v2pub.Create_party_site_use';

         hz_cust_account_site_v2pub.create_cust_site_use (  p_init_msg_list          =>   'T'                     ,
                                                            p_cust_site_use_rec      =>   p_cust_site_use_rec     ,
                                                            p_customer_profile_rec   =>   p_customer_profile_rec  ,
                                                            p_create_profile         =>   NULL                    ,
                                                            p_create_profile_amt     =>   NULL                    ,
                                                            x_site_use_id            =>   x_site_use_id           ,
                                                            x_return_status          =>   x_return_status         ,
                                                            x_msg_count              =>   x_msg_count             ,
                                                            x_msg_data               =>   x_msg_data
                                                         );
        IF x_return_status != 'S'
        THEN
          o_message := 'CREATE_CUST_SITE_USE'||x_msg_data;
          RAISE l_exception;
        END IF;

        IF x_return_status = 'S' THEN
           COMMIT;
           o_status          := x_return_status;
           o_ship_to_site_use_id := x_site_use_id;
        END IF;

     END IF; --SHIP_TO

     IF p_xxwc_address_rec.bill_to_flag = 'Y' THEN
        p_cust_site_use_rec.cust_acct_site_id := x_cust_acct_site_id;
        p_cust_site_use_rec.site_use_code     := 'BILL_TO';
        p_cust_site_use_rec.location          :=  p_xxwc_address_rec.location;
        p_cust_site_use_rec.created_by_module := 'TCA_V1_API';

        l_sec:= 'Calling the API hz_party_site_v2pub.Create_party_site_use';

         hz_cust_account_site_v2pub.create_cust_site_use (  p_init_msg_list          =>   'T'                     ,
                                                            p_cust_site_use_rec      =>   p_cust_site_use_rec     ,
                                                            p_customer_profile_rec   =>   p_customer_profile_rec  ,
                                                            p_create_profile         =>   NULL                    ,
                                                            p_create_profile_amt     =>   NULL                    ,
                                                            x_site_use_id            =>   x_site_use_id           ,
                                                            x_return_status          =>   x_return_status         ,
                                                            x_msg_count              =>   x_msg_count             ,
                                                            x_msg_data               =>   x_msg_data
                                                         );
        IF x_return_status != 'S'
        THEN
          o_message := 'CREATE_CUST_SITE_USE'||x_msg_data;
          RAISE l_exception;
        END IF;

        IF x_return_status = 'S' THEN
           COMMIT;
           o_status          := x_return_status;
           o_bill_to_site_use_id := x_site_use_id;
        END IF;

     END IF; --SHIP_TO
   EXCEPTION
      WHEN l_exception
      THEN
         o_status          := 'E';
         fnd_file.put_line (
            fnd_file.LOG,
               'customer_id - '
            || p_xxwc_address_rec.customer_id
            || ' in L_EXCEPTION, p_error_message - '
            || l_error_message);
      WHEN OTHERS
      THEN
         ROLLBACK;
         o_ship_to_site_use_id := NULL;
         o_bill_to_site_use_id := NULL;
         o_status          := 'E';
         o_message         :=  'customer_id - '
            || p_xxwc_address_rec.customer_id
            || ' error in CREATE_JOB_SITE procedure - '
            || SUBSTR(SQLERRM,1,1000);
         DBMS_OUTPUT.put_line (
               'customer_id - '
            || p_xxwc_address_rec.customer_id
            || ' error in CREATE_CONTACT procedure - '
            || SUBSTR(SQLERRM,1,1000));
   END;

   PROCEDURE CREATE_ITEM (
      p_Parts_On_Fly_Rec   IN OUT NOCOPY xxwc_Parts_On_Fly_Rec,
      x_Return_Status         OUT NOCOPY VARCHAR2,
      x_Msg_Data              OUT NOCOPY VARCHAR2,
      x_Msg_Count             OUT NOCOPY NUMBER,
      x_part_number           OUT NOCOPY VARCHAR2,
      x_inventory_item_id     OUT NOCOPY NUMBER,
      x_list_price            OUT NOCOPY NUMBER,
      x_primary_uom_code      OUT NOCOPY VARCHAR2
    ) IS
      l_PARTS_ON_FLY_REC  XXWC_INV_PARTS_ON_FLY_PKG.PARTS_ON_FLY_REC;
   BEGIN
   l_PARTS_ON_FLY_REC.item_number                  := p_Parts_On_Fly_Rec.item_number;
   l_PARTS_ON_FLY_REC.item_description             := p_Parts_On_Fly_Rec.item_description;
   l_PARTS_ON_FLY_REC.mst_organization_id          := p_Parts_On_Fly_Rec.mst_organization_id;--Master Organization id
   l_PARTS_ON_FLY_REC.assgn_organization_id        := p_Parts_On_Fly_Rec.assgn_organization_id;--assign Organization id
   l_parts_on_fly_rec.price_list_header_id         := p_parts_on_fly_rec.price_list_header_id;
   l_parts_on_fly_rec.item_type_code               := p_parts_on_fly_rec.item_type_code;
   l_parts_on_fly_rec.item_template_id             := p_parts_on_fly_rec.item_template_id;
   l_parts_on_fly_rec.primary_uom_code             := p_parts_on_fly_rec.primary_uom_code;
   l_parts_on_fly_rec.weight_uom_code              := p_parts_on_fly_rec.weight_uom_code;
   l_parts_on_fly_rec.unit_weight                  := p_parts_on_fly_rec.unit_weight;
   l_parts_on_fly_rec.buyer_id                     := p_parts_on_fly_rec.buyer_id;
   l_parts_on_fly_rec.vendor_id                    := p_parts_on_fly_rec.vendor_id;
   l_parts_on_fly_rec.vendor_site_id               := p_parts_on_fly_rec.vendor_site_id;
   l_parts_on_fly_rec.manufacturer_part_number     := p_parts_on_fly_rec.manufacturer_part_number;
   l_parts_on_fly_rec.org_id                       := p_parts_on_fly_rec.org_id;
   l_parts_on_fly_rec.category_id                  := p_parts_on_fly_rec.category_id;
   l_parts_on_fly_rec.hazardous_material_flag      := p_parts_on_fly_rec.hazardous_material_flag;
   l_parts_on_fly_rec.hazard_class_id              := p_parts_on_fly_rec.hazard_class_id;
   l_parts_on_fly_rec.list_price                   := p_parts_on_fly_rec.list_price;
   l_parts_on_fly_rec.market_price                 := p_parts_on_fly_rec.market_price;
   l_parts_on_fly_rec.item_cost                    := p_parts_on_fly_rec.item_cost;
   l_parts_on_fly_rec.radio_group_value            := p_parts_on_fly_rec.radio_group_value; --part or fly
   l_parts_on_fly_rec.transaction_id               := p_parts_on_fly_rec.transaction_id;
   l_parts_on_fly_rec.organization_code            := p_parts_on_fly_rec.organization_code;

    APPS.XXWC_INV_PARTS_ON_FLY_PKG.CREATE_ITEM (
      p_Parts_On_Fly_Rec   => l_PARTS_ON_FLY_REC,
      x_Return_Status      => x_return_status,
      x_Msg_Data           => x_Msg_Data,
      x_Msg_Count          => x_msg_count);

    IF (x_return_status = 'S')
    THEN
       BEGIN
          SELECT msi.segment1
                ,msi.inventory_item_id
                ,primary_uom_code
                ,p_Parts_On_Fly_Rec.list_price
            INTO x_part_number
                ,x_inventory_item_id
                ,x_primary_uom_code
                ,x_list_price
            FROM apps.mtl_system_items_b msi
           WHERE segment1        = p_Parts_On_Fly_Rec.item_number
             AND organization_id = p_Parts_On_Fly_Rec.assgn_organization_id;
       EXCEPTION
       WHEN OTHERS THEN
          x_part_number       := NULL;
          x_inventory_item_id := NULL;
          x_primary_uom_code  := NULL;
          x_list_price        := NULL;
       END;


    END If;

   END;

   PROCEDURE GET_PRODUCT_INFO_AIS( p_branch_id   IN NUMBER   --Rev 2.2
                                  ,p_customer_id IN NUMBER
                                  ,p_jobsiteid   IN NUMBER
                                  ,p_item_search IN VARCHAR2
                                  ,p_filter      IN VARCHAR2
                                  ,p_Sort        IN VARCHAR2
                                  ,p_call_pr_api IN VARCHAR2 DEFAULT NULL --Rev 2.2
                                 )-- return sys_refcursor
                             IS
   v_rec sys_refcursor;
   l_inventory_item_id NUMBER;
   o_list_price NUMBER;
     l_record_count number := 0; --to count the rows returned by the search
  l_count        number := 0; --to count the trimmed string with special characters
  l_count2       number := 0; --to count the words in a string
  l_count3       number := 0; --to check if first three letters are numbers and special sp/ or sp- or R
  l_count4       number := 0; --to check if first ten letters are numbers for UPC
  l_count5       number := 0; --to check if word has all characters
  -- TMS 20160525-00219 , 20160801-00182, 20160801-00323 and 20160908-00188 start
  l_count6       number := 0; --to check that remaining string is number
  l_count7       number := 0; --to check that remaining string has special character and number
  l_count8       number := 0; --to check if first three letters are numbers only
  l_count9       number := 0; -- Frequent words
  l_sorting_type varchar2(50) := p_sort; -- filter for sort
   -- TMS 20160525-00219 , 20160801-00182, 20160801-00323 and 20160908-00188 end
  /*l_max_rows         integer := 10000;
  l_counter        integer := 0;
  number_of_steps  integer := 5;
  score_range_size integer; -- 33 for 3 steps, 25 for 4, 20 for 5 etc
  this_score_group integer; -- final step is 1, penultimate step is 2 ...
  last_score_group integer := 0; -- to compare change*/
  /* This is string token which is being taken in from the AIS LIte form. All the special characters are replaced with
  / and special character to handle the search. Also the trailing 's' from every word in the string is being replaced by null
  to handle gloves or glove type searches. This way they return same number of records*/
   -- TMS 20160525-00219 , 20160801-00182, 20160801-00323 and 20160908-00188 start
  p_tokens1         VARCHAR2(240) := replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(p_item_search,
                                                                                                                     '-',
                                                                                                                     '\-'),
                                                                                                             '$',
                                                                                                             '\$'),
                                                                                                     '*',
                                                                                                     '\*'),
                                                                                             '&',
                                                                                             '\&'),
                                                                                     '@',
                                                                                     '\@'),
                                                                             ',',
                                                                             '\,'),
                                                                     ';',
                                                                     '\;'),
                                                             '+',
                                                             '\+'),
                                                     '%',
                                                     ' '),
                                             '#',
                                             '\#');
 -- TMS 20160525-00219 , 20160801-00182, 20160801-00323 and 20160908-00188 end
  l_organization_id number := p_branch_id;--:search_criteria.organization_id;
  -- Trim the token to get partial partnumber. nvl is used incase there is only one string
  l_token_trim       varchar2(4000) := nvl(lower(replace(substr(p_tokens1,
                                                                1,
                                                                instr(p_tokens1,
                                                                      ' ') - 1),
                                                         '%',
                                                         ' ')),
                                           '99999999');
  l_token_trim_count number := length(l_token_trim);
  --TMS 20160324-00153 Start
  -- Template for firt part of the token as part number
  l_trim_template varchar2(32000) := '<query>' || chr(10) ||
                                     '   <textquery lang="ENGLISH" grammar="CONTEXT">' ||
                                     l_token_trim || chr(10) ||
                                     '     <progression>' || chr(10) ||
                                     '       <seq><rewrite>transform((TOKENS, "{", "}", ""))</rewrite></seq>' ||
                                     chr(10) ||
                                     '       <seq><rewrite>transform((TOKENS, "", "%", " "))</rewrite></seq>' ||
                                     chr(10) ||
                                     '       <seq><rewrite>transform((TOKENS, "%", "", ""))</rewrite></seq>' ||
                                     chr(10) ||
                                     '       <seq><rewrite>transform((TOKENS, "%", "%", ""))</rewrite></seq>' ||
                                     chr(10) || ' </progression>' ||
                                     chr(10) || '   </textquery>' ||
                                     chr(10) || '</query>';
  --TMS 20160324-00153 End

  l_trim_template1 varchar2(32000) := '<query>' || chr(10) ||
                                      '   <textquery lang="ENGLISH" grammar="CONTEXT">' ||
                                      l_token_trim || chr(10) ||
                                      '     <progression>' ||
                                      chr(10) ||
                                      '       <seq><rewrite>transform((TOKENS, "{", "}", ""))</rewrite></seq>' ||
                                      chr(10) || ' </progression>' ||
                                      chr(10) || '   </textquery>' ||
                                      chr(10) || '</query>';
  -- Read remaining token as description.nvl is used incase there is only one string
  l_token_rem_trim varchar2(4000) := nvl(lower(replace(substr(p_tokens1,
                                                              instr(p_tokens1,
                                                                    ' ') + 1),
                                                       '%',
                                                       ' ')),
                                         '99999999');
  -- TMS 20160525-00219 , 20160801-00182, 20160801-00323 and 20160908-00188 start
  --TMS 20160324-00153 Start
  -- Template for remaining part of token as description
  l_rem_template varchar2(32000) := '<query>' || chr(10) ||
                                    '   <textquery lang="ENGLISH" grammar="CONTEXT">' ||
                                    l_token_rem_trim || chr(10) ||
                                    '     <progression>' || chr(10) ||
                                    '       <seq><rewrite>transform((TOKENS, "{", "}", " "))</rewrite></seq>' ||
                                    chr(10) || '
                                            <seq><rewrite>transform((TOKENS, "{", "}", "AND"))</rewrite></seq>' ||
                                    chr(10) || '
                                       <seq><rewrite>transform((TOKENS, "%", "", " AND "))</rewrite></seq>' ||
                                    chr(10) || '
                                       <seq><rewrite>transform((TOKENS, "", "%", " AND "))</rewrite></seq>' ||
                                    chr(10) || '
                                       <seq><rewrite>transform((TOKENS, "%", "%", ""))</rewrite></seq>' ||
                                    chr(10) || '
                                          <seq><rewrite>transform((TOKENS, "?{", "}", "AND"))</rewrite></seq>' ||
                                   /*  chr(10) || ' <seq><rewrite>transform((TOKENS, "${", "}", "AND"))</rewrite></seq>' ||*/
                                    chr(10) || ' </progression>' ||
                                    chr(10) || '   </textquery>' ||
                                    chr(10) || '</query>';
   --TMS 20160324-00153 End
  -- special character l_count7 description
  l_rem_template1 varchar2(32000) := '<query>' || chr(10) ||
                                     '   <textquery lang="ENGLISH" grammar="CONTEXT">' ||
                                     l_token_rem_trim || chr(10) ||
                                     '     <progression>' || chr(10) ||
                                     '       <seq><rewrite>transform((TOKENS, "{", "}", ""))</rewrite></seq>' ||
                                     chr(10) || ' </progression>' ||
                                     chr(10) || '   </textquery>' ||
                                     chr(10) || '</query>';


   -- TMS 20160525-00219 , 20160801-00182, 20160801-00323 and 20160908-00188 end
  -- Token intake as description
  l_tokens1 varchar2(4000) := lower(replace(p_tokens1, '%', ' '));
  -- Token intake as partnumber
  l_tokens_p varchar2(4000) := lower(replace(p_tokens1, '%', ' '));
     -- TMS 20160525-00219 , 20160801-00182, 20160801-00323 and 20160908-00188 start
   -- Template for frequent words like water, nails etc
  l_frequent_template varchar2(32000) := '<query>' || chr(10) ||
                                         '   <textquery lang="ENGLISH" grammar="CONTEXT">' ||
                                         l_token_rem_trim || chr(10) ||
                                         '     <progression>' ||
                                         chr(10) ||
                                         '       <seq><rewrite>transform((TOKENS, "{", "}", " "))</rewrite></seq>' ||
                                         chr(10) || '
                                                                                <seq><rewrite>transform((TOKENS, "${", "}", ""))</rewrite></seq>' ||
                                         chr(10) ||
                                         ' </progression>' ||
                                         chr(10) ||
                                         '   </textquery>' ||
                                         chr(10) || '</query>';
    -- TMS 20160525-00219 , 20160801-00182, 20160801-00323 and 20160908-00188 end
  --TMS 20160324-00153 Start
  -- Template for partnumber
  l_query_template varchar2(32000) := '<query>' || chr(10) ||
                                      '   <textquery lang="ENGLISH" grammar="CONTEXT">' ||
                                      l_tokens_p || chr(10) ||
                                      '     <progression>' ||
                                      chr(10) ||
                                      '       <seq><rewrite>transform((TOKENS, "{", "}", ""))</rewrite></seq>' ||
                                      chr(10) ||
                                      '       <seq><rewrite>transform((TOKENS, "", "%", " "))</rewrite></seq>' ||
                                      chr(10) ||
                                      '       <seq><rewrite>transform((TOKENS, "%", "", ""))</rewrite></seq>' ||
                                      chr(10) ||
                                      '       <seq><rewrite>transform((TOKENS, "%", "%", ""))</rewrite></seq>' ||
                                      chr(10) || ' </progression>' ||
                                      chr(10) || '   </textquery>' ||
                                      chr(10) || '</query>';
  ----TMS 20160324-00153 End
  ----TMS 20160324-00153 Start
  -- Template for short description
  l_query_template2 varchar2(32000) := '<query>' || chr(10) ||
                                       '   <textquery lang="ENGLISH" grammar="CONTEXT">' ||
                                       l_tokens1 || chr(10) ||
                                       '     <progression>' ||
                                       chr(10) ||
                                       '       <seq><rewrite>transform((TOKENS, "{", "}", " "))</rewrite></seq>' ||
                                       chr(10) || '
                                            <seq><rewrite>transform((TOKENS, "{", "}", "AND"))</rewrite></seq>' ||
                                       chr(10) || '
                                       <seq><rewrite>transform((TOKENS, "", "%", " AND "))</rewrite></seq>' ||
                                       chr(10) || '
                                       <seq><rewrite>transform((TOKENS, "%", "", " AND "))</rewrite></seq>' ||
                                       chr(10) || '
                                       <seq><rewrite>transform((TOKENS, "%", "%", " AND "))</rewrite></seq>' ||
                                       chr(10) || '
                                          <seq><rewrite>transform((TOKENS, "?{", "}", "AND"))</rewrite></seq>' ||
                                       chr(10) || '
                                          <seq><rewrite>transform((TOKENS, "${", "}", "AND"))</rewrite></seq>' ||
                                       chr(10) || ' </progression>' ||
                                       chr(10) || '   </textquery>' ||
                                       chr(10) || '</query>';
   -- TMS 20160525-00219 , 20160801-00182, 20160801-00323 and 20160908-00188 start
  -- Template for short description for single word search
  --TMS 20161128-00028 start
  l_query_template7 varchar2(32000) := '<query>' || chr(10) ||
                                       '   <textquery lang="ENGLISH" grammar="CONTEXT">' ||
                                       l_tokens1 || chr(10) ||
                                       '     <progression>' ||
                                       chr(10) ||
                                       '       <seq><rewrite>transform((TOKENS, "{", "}", " "))</rewrite></seq>' ||
                                       chr(10) || '
                                       <seq><rewrite>transform((TOKENS, "%", "", ""))</rewrite></seq>' ||
                                       chr(10) || '
                                       <seq><rewrite>transform((TOKENS, "", "%", ""))</rewrite></seq>' ||
                                       chr(10) || '
                                          <seq><rewrite>transform((TOKENS, "?{", "}", ""))</rewrite></seq>' ||
                                       chr(10) || '
                                          <seq><rewrite>transform((TOKENS, "${", "}", ""))</rewrite></seq>' ||
                                       chr(10) || ' </progression>' ||
                                       chr(10) || '   </textquery>' ||
                                       chr(10) || '</query>';
  --TMS 20161128-00028 end
  --TMS 20160324-00153 End
  -- TMS 20160525-00219 , 20160801-00182, 20160801-00323 and 20160908-00188 end
  -- Template for short description for trimmed portion
  l_query_template5 varchar2(32000) := '<query>' || chr(10) ||
                                       '   <textquery lang="ENGLISH" grammar="CONTEXT">' ||
                                       l_token_trim || chr(10) ||
                                       '     <progression>' ||
                                       chr(10) ||
                                       '       <seq><rewrite>transform((TOKENS, "{", "}", " "))</rewrite></seq>' ||
                                       chr(10) || '
                                       <seq><rewrite>transform((TOKENS, "", "%", ""))</rewrite></seq>' ||
                                       chr(10) || ' </progression>' ||
                                       chr(10) || '   </textquery>' ||
                                       chr(10) || '</query>';
    -- TMS 20160525-00219 , 20160801-00182, 20160801-00323 and 20160908-00188 start
  --TMS 20160324-00153 Start
  -- Template for short description for remaining trimmed token
  l_query_template6 varchar2(32000) := '<query>' || chr(10) ||
                                       '   <textquery lang="ENGLISH" grammar="CONTEXT">' ||
                                       l_token_rem_trim || chr(10) ||
                                       '     <progression>' ||
                                       chr(10) ||
                                       '       <seq><rewrite>transform((TOKENS, "{", "}", " "))</rewrite></seq>' ||
                                       chr(10) || '
                                            <seq><rewrite>transform((TOKENS, "{", "}", "AND"))</rewrite></seq>' ||
                                       chr(10) || '
                                       <seq><rewrite>transform((TOKENS, "%", "", " AND "))</rewrite></seq>' ||
                                       chr(10) || '
                                       <seq><rewrite>transform((TOKENS, "", "%", " AND "))</rewrite></seq>' ||
                                       chr(10) || '
                                          <seq><rewrite>transform((TOKENS, "?{", "}", "AND"))</rewrite></seq>' ||
                                      /*chr(10) || ' <seq><rewrite>transform((TOKENS, "${", "}", "AND"))</rewrite></seq>' ||*/
                                       chr(10) || ' </progression>' ||
                                       chr(10) || '   </textquery>' ||
                                       chr(10) || '</query>';
  --TMS 20160324-00153 End
  -- TMS 20160525-00219 , 20160801-00182, 20160801-00323 and 20160908-00188 end
  -- Token intake for cross reference
  l_tokens_c varchar2(4000) := lower(replace(p_tokens1, '%', ' '));
  --TMS 20160324-00153 Start
  -- Template for cross reference
  l_query_template3 varchar2(32000) := '<query>' || chr(10) ||
                                       '   <textquery lang="ENGLISH" grammar="CONTEXT">' ||
                                       l_tokens_c || chr(10) ||
                                       '     <progression>' ||
                                       chr(10) ||
                                       '       <seq><rewrite>transform((TOKENS, "{", "}", ""))</rewrite></seq>' ||
                                       chr(10) || '
                                            <seq><rewrite>transform((TOKENS, "{", "}", "AND"))</rewrite></seq>' ||
                                       chr(10) ||
                                       '       <seq><rewrite>transform((TOKENS, "%", "", ""))</rewrite></seq>' ||
                                       chr(10) ||
                                       '       <seq><rewrite>transform((TOKENS, "", "%", ""))</rewrite></seq>' ||
                                       chr(10) || ' </progression>' ||
                                       chr(10) || '   </textquery>' ||
                                       chr(10) || '</query>';

  -- Template for cross reference part2 for single word
  l_query_template4 varchar2(32000) := '<query>' || chr(10) ||
                                       '   <textquery lang="ENGLISH" grammar="CONTEXT">' ||
                                       l_tokens_c || chr(10) ||
                                       '     <progression>' ||
                                       chr(10) ||
                                       '       <seq><rewrite>transform((TOKENS, "{", "}", ""))</rewrite></seq>' ||
                                       chr(10) || '
                                            <seq><rewrite>transform((TOKENS, "{", "}", "AND"))</rewrite></seq>' ||
                                       chr(10) ||
                                       '       <seq><rewrite>transform((TOKENS, "%", "", ""))</rewrite></seq>' ||
                                       chr(10) ||
                                       '       <seq><rewrite>transform((TOKENS, "", "%", ""))</rewrite></seq>' ||
                                       chr(10) ||
                                       '       <seq><rewrite>transform((TOKENS, "%", "%", ""))</rewrite></seq>' ||
                                       chr(10) || ' </progression>' ||
                                       chr(10) || '   </textquery>' ||
                                       chr(10) || '</query>';
  --TMS 20160324-00153 End
  --changes for TMS 20160801-00182. starts
   l_item_searched       VARCHAR2 (500);
  --changes for TMS 20160801-00182. ends

  BEGIN
     l_item_searched := p_item_search;--:search_criteria.item_desc;

  --changes for TMS 20160801-00182. ends

   --:CONTROL.QUERY_COUNT := NULL;
  -- 20160307-00117 > Start
  DELETE FROM XXWC_PA_WAREHOUSE_GT;
  Delete from xxwc.XXWC_MD_SEARCH_PRODUCT_GTT_TBL;

  BEGIN
    INSERT INTO xxwc.xxwc_pa_warehouse_gt
      (organization_id)
    values
      (p_branch_id);--:search_criteria.organization_id);
  EXCEPTION
    WHEN OTHERS THEN
      NULL;
  END;
  -- 20160307-00117 < End

  -- form error if nothing is added in the search field
  IF p_item_search is null then
    write_output('Please enter atleast one search criteria');
  else
    null;
  END IF;
  -- form error if search string added is less then 4
  IF length(p_item_search) < 4 then
    write_output('Please enter more words into search criteria');
  else
    null;
  END IF;

  --go_block('XXWC_INV_ITEM_SEARCH_V');

  --CLEAR_BLOCK(NO_VALIDATE);

  begin

    --This count is to find out number of words in a string
    select length(p_tokens1) - length(replace(p_tokens1, ' ', '')) + 1
      into l_count2
      FROM dual;

    begin
      if l_count2 > 1 then
        -- This count is to look if there is any special character in the trimmed part of the token
        SELECT count(*)
          into l_count
          FROM dual
         WHERE REGEXP_LIKE(l_token_trim, '[$*&@,;+-/"]');
     -- TMS 20160525-00219 , 20160801-00182, 20160801-00323 and 20160908-00188 start
        /* --first three digit as number minus the sp/ and r
        select count(*)
          into l_count3
          from dual
         where 1 = 1
           and REGEXP_LIKE(substr(l_token_trim, 1, 3),
                           '^R{1}|SP|SP/|[0-9]{3}');*/
     -- TMS 20160525-00219 , 20160801-00182, 20160801-00323 and 20160908-00188 end
        --first three digit as number minus the sp/ and r
        select count(*)
          into l_count3
          from dual
         where 1 = 1
           and REGEXP_LIKE(substr(l_token_trim, 1, 3), '^R{1}|SP|SP/');
        --remaining string first three as number
        select count(*)
          into l_count6
          from dual
         where 1 = 1
           and REGEXP_LIKE(substr(l_token_rem_trim, 1), '^[[:digit:]]+$');
       -- TMS 20160525-00219 , 20160801-00182, 20160801-00323 and 20160908-00188 start
      --remaining string contain special character
        select count(*)
          into l_count7
          from dual
         where 1 = 1
           and REGEXP_LIKE(substr(l_token_rem_trim, 1), '[$*&@,;+-/"]');
        -- TMS 20160525-00219 , 20160801-00182, 20160801-00323 and 20160908-00188 end
       --TMS 20160324-00153 Start
        -- All characters
        select count(*)
          into l_count5
          from dual
         where 1 = 1
           and regexp_like(p_tokens1, '^[[:alpha:]]+$');
        --TMS 20160324-00153 End
        -- TMS 20160525-00219 , 20160801-00182, 20160801-00323 and 20160908-00188 start
        --first three digit as number only
        select count(*)
          into l_count8
          from dual
         where 1 = 1
           and REGEXP_LIKE(substr(l_token_trim, 1, 3), '[0-9]{3}');
        -- TMS 20160525-00219 , 20160801-00182, 20160801-00323 and 20160908-00188 end
      end if;
    end;
    begin
      if l_count2 = 1 then
        -- This count is to look if there is any special character in the trimmed part of the token
        SELECT count(*)
          into l_count
          FROM dual
         WHERE REGEXP_LIKE(p_tokens1, '[$*&@,;+-/"]');
        --First 10 digit as numbers
        select count(*)
          into l_count4
          FROM dual
         WHERE regexp_like(p_tokens1, '^[0-9]{10}');
        --first three digit as number minus the sp/ and r
        select count(*)
          into l_count3
          from dual
         where 1 = 1
           and REGEXP_LIKE(substr(p_tokens1, 1, 3), '^R{1}|SP|SP/|[0-9]{3}');

        --first string as all character
        select count(*)
          into l_count5
          from dual
         where 1 = 1
           and regexp_like(p_tokens1, '^[[:alpha:]]+$');
      -- TMS 20160525-00219 , 20160801-00182, 20160801-00323 and 20160908-00188 start
        -- frequent words
        select count(*)
          into l_count9
          from dual
         where 1 = 1
           and regexp_like(p_tokens1, 'NAIL')
            or regexp_like(p_tokens1, 'NAILS')
            or regexp_like(p_tokens1, 'WATER')
            or regexp_like(p_tokens1, 'REBAR');
        -- TMS 20160525-00219 , 20160801-00182, 20160801-00323 and 20160908-00188 end
      end if;
    end;
    --Multi word search
    if l_count2 > 1 then
        -- TMS 20160525-00219 , 20160801-00182, 20160801-00323 and 20160908-00188 start
      --search like SP/ water
      if p_tokens1 is not null and l_count = 1 and l_count3 = 1 then
        --TMS 20160324-00153 End
        if l_organization_id != 222 then
          if p_filter = 'ONHAND' then
            insert into XXWC.XXWC_MD_SEARCH_PRODUCT_GTT_TBL
              (score,
               shortdescription,
               partnumber,
               quantityonhand,
               open_sales_orders, --Added by Rakesh Patel on 03/25/2016 for TMS#20151023-00037/20160328-00211
               list_price,
               inventory_item_id,
               -- organization_id,
               primary_uom_code)
              (select /*+ result_cache */
                score(3) + score(2) scr,
                -- type,
                --  name,
                shortdescription,
                partnumber,
                null,
                /*xxwc_ascp_scwb_pkg.get_on_hand(inventory_item_id,
                l_organization_id,
                'G') quantity_onhand,*/
                null,
                /*xxwc_ascp_scwb_pkg.get_open_orders_qty(inventory_item_id,
                l_organization_id) open_orders_qty, --Added by Rakesh Patel on 03/25/2016 for TMS#20151023-00037/20160328-00211*/
                /*xxwc_inv_ais_pkg.shipto_last_price_paid(:search_criteria.site_use_id,
                inventory_item_id) list_price,*/
                null,

                -- cross_reference,
                inventory_item_id,
                -- organization_id,
                -- l_organization_id,
                -- item_type,
                primary_uom_code
                 from XXWC_MD_SEARCH_PRODUCTS_MV_S
                where /*organization_id + 0 = l_organization_id and*/

                0 < (select nvl(xxwc_ascp_scwb_pkg.get_on_hand(inventory_item_id,
                                                               l_organization_id,
                                                               'G'),
                                0)
                       from dual)

            and ((contains(partnumber, l_trim_template, 2) > 0 and
                contains(dummy, l_rem_template, 3) > 0)));

          elsif p_filter = 'STANDARD' then

            insert into XXWC.XXWC_MD_SEARCH_PRODUCT_GTT_TBL
              (score,
               shortdescription,
               partnumber,
               quantityonhand,
               open_sales_orders, --Added by Rakesh Patel on 03/25/2016 for TMS#20151023-00037/20160328-00211
               list_price,
               inventory_item_id,
               -- organization_id,
               primary_uom_code)
              (select /*+ result_cache */
                score(3) + score(2) scr,
                -- type,
                --  name,
                shortdescription,
                partnumber,
                null,
                /*xxwc_ascp_scwb_pkg.get_on_hand(inventory_item_id,
                l_organization_id,
                'G') quantity_onhand,*/
                null,
                /*xxwc_ascp_scwb_pkg.get_open_orders_qty(inventory_item_id,
                l_organization_id) open_orders_qty, --Added by Rakesh Patel on 03/25/2016 for TMS#20151023-00037/20160328-00211*/
                /*xxwc_inv_ais_pkg.shipto_last_price_paid(:search_criteria.site_use_id,
                inventory_item_id) list_price,*/
                null,

                -- cross_reference,
                inventory_item_id,
                -- organization_id,
                -- l_organization_id,
                -- item_type,
                primary_uom_code
                 from XXWC_MD_SEARCH_PRODUCTS_MV_S
                where ((0 < (select nvl(xxwc_ascp_scwb_pkg.get_on_hand(inventory_item_id,
                                                                       l_organization_id,
                                                                       'G'),
                                        0)
                               from dual) and xxwc_mv_routines_pkg.get_item_category(inventory_item_id,
                                                                                       l_organization_id,
                                                                                       'Sales Velocity',
                                                                                       1) in
                      ('N', 'Z')) or xxwc_mv_routines_pkg.get_item_category(inventory_item_id,
                                                                             l_organization_id,
                                                                             'Sales Velocity',
                                                                             1) not in
                      ('N', 'Z') or (0 < (select nvl(xxwc_ascp_scwb_pkg.get_on_hand(inventory_item_id,
                                                                                     l_organization_id,
                                                                                     'G'),
                                                      0)
                                             from dual) and
                      xxwc_mv_routines_pkg.get_item_category(inventory_item_id,
                                                                             l_organization_id,
                                                                             'Sales Velocity',
                                                                             1) is null and
                      item_type = 'SPECIAL'))
                  and ((contains(partnumber, l_trim_template, 2) > 0 and
                      contains(dummy, l_rem_template, 3) > 0)));

          else
            insert into XXWC.XXWC_MD_SEARCH_PRODUCT_GTT_TBL
              (score,
               shortdescription,
               partnumber,
               quantityonhand,
               open_sales_orders, --Added by Rakesh Patel on 03/25/2016 for TMS#20151023-00037/20160328-00211/20160328-00211
               list_price,
               inventory_item_id,
               -- organization_id,
               primary_uom_code)
              (select /*+ result_cache */
                score(3) + score(2) scr,
                -- type,
                --  name,
                shortdescription,
                partnumber,
                null,
                /*xxwc_ascp_scwb_pkg.get_on_hand(inventory_item_id,
                l_organization_id,
                'G') quantity_onhand,*/
                null,
                /*xxwc_ascp_scwb_pkg.get_open_orders_qty(inventory_item_id,
                l_organization_id) open_orders_qty, --Added by Rakesh Patel on 03/25/2016 for TMS#20151023-00037/20160328-00211*/
                /*xxwc_inv_ais_pkg.shipto_last_price_paid(:search_criteria.site_use_id,
                inventory_item_id) list_price,*/
                null,
                -- cross_reference,
                inventory_item_id,
                -- organization_id,
                -- l_organization_id,
                -- item_type,
                primary_uom_code
                 from XXWC_MD_SEARCH_PRODUCTS_MV_S
                where /*organization_id + 0 = l_organization_id
                                                                                                                                                                                                                                                                                                                                                                                                                                                           and*/
                ((contains(partnumber, l_trim_template, 2) > 0 and
                contains(dummy, l_rem_template, 3) > 0)));

          end if;
        else
          insert into XXWC.XXWC_MD_SEARCH_PRODUCT_GTT_TBL
            (score,
             shortdescription,
             partnumber,
             quantityonhand,
             open_sales_orders, --Added by Rakesh Patel on 03/25/2016 for TMS#20151023-00037/20160328-00211/20160328-00211
             list_price,
             inventory_item_id,
             -- organization_id,
             primary_uom_code)
            (select /*+ result_cache */
              score(3) + score(2) scr,
              -- type,
              --  name,
              shortdescription,
              partnumber,
              null,
              /*xxwc_ascp_scwb_pkg.get_on_hand(inventory_item_id,
              l_organization_id,
              'G') quantity_onhand,*/
              null,
              /*xxwc_ascp_scwb_pkg.get_open_orders_qty(inventory_item_id,
              l_organization_id) open_orders_qty, --Added by Rakesh Patel on 03/25/2016 for TMS#20151023-00037/20160328-00211*/
              /*xxwc_inv_ais_pkg.shipto_last_price_paid(:search_criteria.site_use_id,
              inventory_item_id) list_price,*/
              null,

              -- cross_reference,
              inventory_item_id,
              -- organization_id,
              -- l_organization_id,
              -- item_type,
              primary_uom_code
               from XXWC_MD_SEARCH_PRODUCTS_MV_S
              where /*organization_id + 0 = l_organization_id
                                                                                                                                                                                                                                                                                                                                                                                                                                       and*/
              ((contains(partnumber, l_trim_template, 2) > 0 and
              contains(dummy, l_rem_template, 3) > 0)));

        end if;
      end if;
      --sorting order partnumber
      -- Ist word no special except sp/ sp- R  then search in partnumber description combination
      -- to eliminate the duplicates for words starting with R added l_count5= 0 condition to this if
      --TMS 20160324-00153 Start
      if p_tokens1 is not null and l_count = 0 and l_count3 = 1 and
         l_count8 = 0 and l_count6 = 0 and l_count5 = 0 and l_count7 = 0 then
        --TMS 20160324-00153 End
        if l_organization_id != 222 then
          if p_filter = 'ONHAND' then
            insert into XXWC.XXWC_MD_SEARCH_PRODUCT_GTT_TBL
              (score,
               shortdescription,
               partnumber,
               quantityonhand,
               open_sales_orders, --Added by Rakesh Patel on 03/25/2016 for TMS#20151023-00037/20160328-00211
               list_price,
               inventory_item_id,
               -- organization_id,
               primary_uom_code)
              (select /*+ result_cache */
                score(3) + score(2) scr,
                -- type,
                --  name,
                shortdescription,
                partnumber,
                null,
                /*xxwc_ascp_scwb_pkg.get_on_hand(inventory_item_id,
                l_organization_id,
                'G') quantity_onhand,*/
                null,
                /*xxwc_ascp_scwb_pkg.get_open_orders_qty(inventory_item_id,
                l_organization_id) open_orders_qty, --Added by Rakesh Patel on 03/25/2016 for TMS#20151023-00037/20160328-00211*/
                /*xxwc_inv_ais_pkg.shipto_last_price_paid(:search_criteria.site_use_id,
                inventory_item_id) list_price,*/
                null,

                -- cross_reference,
                inventory_item_id,
                -- organization_id,
                -- l_organization_id,
                -- item_type,
                primary_uom_code
                 from XXWC_MD_SEARCH_PRODUCTS_MV_S
                where /*organization_id + 0 = l_organization_id and*/

                0 < (select nvl(xxwc_ascp_scwb_pkg.get_on_hand(inventory_item_id,
                                                               l_organization_id,
                                                               'G'),
                                0)
                       from dual)

            and ((contains(partnumber, l_trim_template, 2) > 0 and
                contains(dummy, l_rem_template, 3) > 0)));

          elsif p_filter = 'STANDARD' then

            insert into XXWC.XXWC_MD_SEARCH_PRODUCT_GTT_TBL
              (score,
               shortdescription,
               partnumber,
               quantityonhand,
               open_sales_orders, --Added by Rakesh Patel on 03/25/2016 for TMS#20151023-00037/20160328-00211
               list_price,
               inventory_item_id,
               -- organization_id,
               primary_uom_code)
              (select /*+ result_cache */
                score(3) + score(2) scr,
                -- type,
                --  name,
                shortdescription,
                partnumber,
                null,
                /*xxwc_ascp_scwb_pkg.get_on_hand(inventory_item_id,
                l_organization_id,
                'G') quantity_onhand,*/
                null,
                /*xxwc_ascp_scwb_pkg.get_open_orders_qty(inventory_item_id,
                l_organization_id) open_orders_qty, --Added by Rakesh Patel on 03/25/2016 for TMS#20151023-00037/20160328-00211*/
                /*xxwc_inv_ais_pkg.shipto_last_price_paid(:search_criteria.site_use_id,
                inventory_item_id) list_price,*/
                null,

                -- cross_reference,
                inventory_item_id,
                -- organization_id,
                -- l_organization_id,
                -- item_type,
                primary_uom_code
                 from XXWC_MD_SEARCH_PRODUCTS_MV_S
                where ((0 < (select nvl(xxwc_ascp_scwb_pkg.get_on_hand(inventory_item_id,
                                                                       l_organization_id,
                                                                       'G'),
                                        0)
                               from dual) and xxwc_mv_routines_pkg.get_item_category(inventory_item_id,
                                                                                       l_organization_id,
                                                                                       'Sales Velocity',
                                                                                       1) in
                      ('N', 'Z')) or xxwc_mv_routines_pkg.get_item_category(inventory_item_id,
                                                                             l_organization_id,
                                                                             'Sales Velocity',
                                                                             1) not in
                      ('N', 'Z') or (0 < (select nvl(xxwc_ascp_scwb_pkg.get_on_hand(inventory_item_id,
                                                                                     l_organization_id,
                                                                                     'G'),
                                                      0)
                                             from dual) and
                      xxwc_mv_routines_pkg.get_item_category(inventory_item_id,
                                                                             l_organization_id,
                                                                             'Sales Velocity',
                                                                             1) is null and
                      item_type = 'SPECIAL'))
                  and ((contains(partnumber, l_trim_template, 2) > 0 and
                      contains(dummy, l_rem_template, 3) > 0)));

          else
            insert into XXWC.XXWC_MD_SEARCH_PRODUCT_GTT_TBL
              (score,
               shortdescription,
               partnumber,
               quantityonhand,
               open_sales_orders, --Added by Rakesh Patel on 03/25/2016 for TMS#20151023-00037/20160328-00211/20160328-00211
               list_price,
               inventory_item_id,
               -- organization_id,
               primary_uom_code)
              (select /*+ result_cache */
                score(3) + score(2) scr,
                -- type,
                --  name,
                shortdescription,
                partnumber,
                null,
                /*xxwc_ascp_scwb_pkg.get_on_hand(inventory_item_id,
                l_organization_id,
                'G') quantity_onhand,*/
                null,
                /*xxwc_ascp_scwb_pkg.get_open_orders_qty(inventory_item_id,
                l_organization_id) open_orders_qty, --Added by Rakesh Patel on 03/25/2016 for TMS#20151023-00037/20160328-00211*/
                /*xxwc_inv_ais_pkg.shipto_last_price_paid(:search_criteria.site_use_id,
                inventory_item_id) list_price,*/
                null,
                -- cross_reference,
                inventory_item_id,
                -- organization_id,
                -- l_organization_id,
                -- item_type,
                primary_uom_code
                 from XXWC_MD_SEARCH_PRODUCTS_MV_S
                where /*organization_id + 0 = l_organization_id
                                                                                                                                                                                                                                                                                                                                                                                                                                                           and*/
                ((contains(partnumber, l_trim_template, 2) > 0 and
                contains(dummy, l_rem_template, 3) > 0)));

          end if;
        else
          insert into XXWC.XXWC_MD_SEARCH_PRODUCT_GTT_TBL
            (score,
             shortdescription,
             partnumber,
             quantityonhand,
             open_sales_orders, --Added by Rakesh Patel on 03/25/2016 for TMS#20151023-00037/20160328-00211/20160328-00211
             list_price,
             inventory_item_id,
             -- organization_id,
             primary_uom_code)
            (select /*+ result_cache */
              score(3) + score(2) scr,
              -- type,
              --  name,
              shortdescription,
              partnumber,
              null,
              /*xxwc_ascp_scwb_pkg.get_on_hand(inventory_item_id,
              l_organization_id,
              'G') quantity_onhand,*/
              null,
              /*xxwc_ascp_scwb_pkg.get_open_orders_qty(inventory_item_id,
              l_organization_id) open_orders_qty, --Added by Rakesh Patel on 03/25/2016 for TMS#20151023-00037/20160328-00211*/
              /*xxwc_inv_ais_pkg.shipto_last_price_paid(:search_criteria.site_use_id,
              inventory_item_id) list_price,*/
              null,

              -- cross_reference,
              inventory_item_id,
              -- organization_id,
              -- l_organization_id,
              -- item_type,
              primary_uom_code
               from XXWC_MD_SEARCH_PRODUCTS_MV_S
              where /*organization_id + 0 = l_organization_id
                                                                                                                                                                                                                                                                                                                                                                                                                                       and*/
              ((contains(partnumber, l_trim_template, 2) > 0 and
              contains(dummy, l_rem_template, 3) > 0)));

        end if;
      end if;

      -- if remaining string contain special character then go through dummy2 field which has just partnumber and description combination don't go through cross reference combinations
      if p_tokens1 is not null and l_count = 0 and l_count3 = 0 and
         l_count8 = 1 and l_count6 = 0 and l_count5 = 0 and l_count7 = 1 then
        --TMS 20160324-00153 End
        if l_organization_id != 222 then
          if p_filter = 'ONHAND' then
            insert into XXWC.XXWC_MD_SEARCH_PRODUCT_GTT_TBL
              (score,
               shortdescription,
               partnumber,
               quantityonhand,
               open_sales_orders, --Added by Rakesh Patel on 03/25/2016 for TMS#20151023-00037/20160328-00211
               list_price,
               inventory_item_id,
               -- organization_id,
               primary_uom_code)
              (select /*+ result_cache */
                score(3) + score(2) scr,
                -- type,
                --  name,
                shortdescription,
                partnumber,
                null,
                /*xxwc_ascp_scwb_pkg.get_on_hand(inventory_item_id,
                l_organization_id,
                'G') quantity_onhand,*/
                null,
                /*xxwc_ascp_scwb_pkg.get_open_orders_qty(inventory_item_id,
                l_organization_id) open_orders_qty, --Added by Rakesh Patel on 03/25/2016 for TMS#20151023-00037/20160328-00211*/
                /*xxwc_inv_ais_pkg.shipto_last_price_paid(:search_criteria.site_use_id,
                inventory_item_id) list_price,*/
                null,

                -- cross_reference,
                inventory_item_id,
                -- organization_id,
                -- l_organization_id,
                -- item_type,
                primary_uom_code
                 from XXWC_MD_SEARCH_PRODUCTS_MV_S
                where /*organization_id + 0 = l_organization_id and*/

                0 < (select nvl(xxwc_ascp_scwb_pkg.get_on_hand(inventory_item_id,
                                                               l_organization_id,
                                                               'G'),
                                0)
                       from dual)

            and ((contains(partnumber2, l_trim_template1, 2) > 0 and
                contains(dummy2, l_rem_template1, 3) > 0)));

          elsif p_filter = 'STANDARD' then

            insert into XXWC.XXWC_MD_SEARCH_PRODUCT_GTT_TBL
              (score,
               shortdescription,
               partnumber,
               quantityonhand,
               open_sales_orders, --Added by Rakesh Patel on 03/25/2016 for TMS#20151023-00037/20160328-00211
               list_price,
               inventory_item_id,
               -- organization_id,
               primary_uom_code)
              (select /*+ result_cache */
                score(3) + score(2) scr,
                -- type,
                --  name,
                shortdescription,
                partnumber,
                null,
                /*xxwc_ascp_scwb_pkg.get_on_hand(inventory_item_id,
                l_organization_id,
                'G') quantity_onhand,*/
                null,
                /*xxwc_ascp_scwb_pkg.get_open_orders_qty(inventory_item_id,
                l_organization_id) open_orders_qty, --Added by Rakesh Patel on 03/25/2016 for TMS#20151023-00037/20160328-00211*/
                /*xxwc_inv_ais_pkg.shipto_last_price_paid(:search_criteria.site_use_id,
                inventory_item_id) list_price,*/
                null,

                -- cross_reference,
                inventory_item_id,
                -- organization_id,
                -- l_organization_id,
                -- item_type,
                primary_uom_code
                 from XXWC_MD_SEARCH_PRODUCTS_MV_S
                where ((0 < (select nvl(xxwc_ascp_scwb_pkg.get_on_hand(inventory_item_id,
                                                                       l_organization_id,
                                                                       'G'),
                                        0)
                               from dual) and xxwc_mv_routines_pkg.get_item_category(inventory_item_id,
                                                                                       l_organization_id,
                                                                                       'Sales Velocity',
                                                                                       1) in
                      ('N', 'Z')) or xxwc_mv_routines_pkg.get_item_category(inventory_item_id,
                                                                             l_organization_id,
                                                                             'Sales Velocity',
                                                                             1) not in
                      ('N', 'Z') or (0 < (select nvl(xxwc_ascp_scwb_pkg.get_on_hand(inventory_item_id,
                                                                                     l_organization_id,
                                                                                     'G'),
                                                      0)
                                             from dual) and
                      xxwc_mv_routines_pkg.get_item_category(inventory_item_id,
                                                                             l_organization_id,
                                                                             'Sales Velocity',
                                                                             1) is null and
                      item_type = 'SPECIAL'))
                  and ((contains(partnumber2, l_trim_template1, 2) > 0 and
                      contains(dummy2, l_rem_template1, 3) > 0)));

          else
            insert into XXWC.XXWC_MD_SEARCH_PRODUCT_GTT_TBL
              (score,
               shortdescription,
               partnumber,
               quantityonhand,
               open_sales_orders, --Added by Rakesh Patel on 03/25/2016 for TMS#20151023-00037/20160328-00211/20160328-00211
               list_price,
               inventory_item_id,
               -- organization_id,
               primary_uom_code)
              (select /*+ result_cache */
                score(3) + score(2) scr,
                -- type,
                --  name,
                shortdescription,
                partnumber,
                null,
                /*xxwc_ascp_scwb_pkg.get_on_hand(inventory_item_id,
                l_organization_id,
                'G') quantity_onhand,*/
                null,
                /*xxwc_ascp_scwb_pkg.get_open_orders_qty(inventory_item_id,
                l_organization_id) open_orders_qty, --Added by Rakesh Patel on 03/25/2016 for TMS#20151023-00037/20160328-00211*/
                /*xxwc_inv_ais_pkg.shipto_last_price_paid(:search_criteria.site_use_id,
                inventory_item_id) list_price,*/
                null,
                -- cross_reference,
                inventory_item_id,
                -- organization_id,
                -- l_organization_id,
                -- item_type,
                primary_uom_code
                 from XXWC_MD_SEARCH_PRODUCTS_MV_S
                where /*organization_id + 0 = l_organization_id
                                                                                                                                                                                                                                                                                                                                                                                                                                                           and*/
                ((contains(partnumber2, l_trim_template1, 2) > 0 and
                contains(dummy2, l_rem_template1, 3) > 0)));

          end if;
        else
          insert into XXWC.XXWC_MD_SEARCH_PRODUCT_GTT_TBL
            (score,
             shortdescription,
             partnumber,
             quantityonhand,
             open_sales_orders, --Added by Rakesh Patel on 03/25/2016 for TMS#20151023-00037/20160328-00211/20160328-00211
             list_price,
             inventory_item_id,
             -- organization_id,
             primary_uom_code)
            (select /*+ result_cache */
              score(3) + score(2) scr,
              -- type,
              --  name,
              shortdescription,
              partnumber,
              null,
              /*xxwc_ascp_scwb_pkg.get_on_hand(inventory_item_id,
              l_organization_id,
              'G') quantity_onhand,*/
              null,
              /*xxwc_ascp_scwb_pkg.get_open_orders_qty(inventory_item_id,
              l_organization_id) open_orders_qty, --Added by Rakesh Patel on 03/25/2016 for TMS#20151023-00037/20160328-00211*/
              /*xxwc_inv_ais_pkg.shipto_last_price_paid(:search_criteria.site_use_id,
              inventory_item_id) list_price,*/
              null,

              -- cross_reference,
              inventory_item_id,
              -- organization_id,
              -- l_organization_id,
              -- item_type,
              primary_uom_code
               from XXWC_MD_SEARCH_PRODUCTS_MV_S
              where /*organization_id + 0 = l_organization_id
                                                                                                                                                                                                                                                                                                                                                                                                                                       and*/
              ((contains(partnumber2, l_trim_template1, 2) > 0 and
              contains(dummy2, l_rem_template1, 3) > 0)));

        end if;
      end if;
      ---Ist word String with no special look in description only
      -- to avoid duplicate added l_count5 = 0 to this condition as l_count5 looks for complete string
      --TMS 20160324-00153 Start
      if p_tokens1 is not null and l_count = 0 and l_count3 = 0 and
         l_count5 = 0 and l_count8 = 0 and l_count7 = 0 then
        --TMS 20160324-00153 End
        if l_organization_id != 222 then
          if p_filter = 'ONHAND' then
            insert into XXWC.XXWC_MD_SEARCH_PRODUCT_GTT_TBL
              (score,
               shortdescription,
               partnumber,
               quantityonhand,
               open_sales_orders, --Added by Rakesh Patel on 03/25/2016 for TMS#20151023-00037/20160328-00211/20160328-00211
               list_price,
               inventory_item_id,
               -- organization_id,
               primary_uom_code)
              (select /*+ result_cache */
                score(1) scr,
                -- type,
                --  name,
                shortdescription,
                partnumber,
                null,
                /*xxwc_ascp_scwb_pkg.get_on_hand(inventory_item_id,
                l_organization_id,
                'G') quantity_onhand,*/
                null,
                /*xxwc_ascp_scwb_pkg.get_open_orders_qty(inventory_item_id,
                l_organization_id) open_orders_qty, --Added by Rakesh Patel on 03/25/2016 for TMS#20151023-00037/20160328-00211*/
                /*xxwc_inv_ais_pkg.shipto_last_price_paid(:search_criteria.site_use_id,
                inventory_item_id) list_price,*/
                null,

                -- cross_reference,
                inventory_item_id,
                -- organization_id,
                -- l_organization_id,
                -- item_type,
                primary_uom_code
                 from XXWC_MD_SEARCH_PRODUCTS_MV_S
                where /*organization_id + 0 = l_organization_id
                                                                                                                                                                                                                                                                                                                                                                                                                                                           and*/

                0 < (select nvl(xxwc_ascp_scwb_pkg.get_on_hand(inventory_item_id,
                                                               l_organization_id,
                                                               'G'),
                                0)
                       from dual)
            and (contains(shortdescription, l_query_template2, 1) > 0)
               /*order by partnumber desc, quantity_onhand desc, scr desc*/
               );

          elsif p_filter = 'STANDARD' then

            insert into XXWC.XXWC_MD_SEARCH_PRODUCT_GTT_TBL
              (score,
               shortdescription,
               partnumber,
               quantityonhand,
               open_sales_orders, --Added by Rakesh Patel on 03/25/2016 for TMS#20151023-00037/20160328-00211/20160328-00211
               list_price,
               inventory_item_id,
               -- organization_id,
               primary_uom_code)
              (select /*+ result_cache */
                score(1) scr,
                -- type,
                --  name,
                shortdescription,
                partnumber,
                null,
                /*xxwc_ascp_scwb_pkg.get_on_hand(inventory_item_id,
                l_organization_id,
                'G') quantity_onhand,*/
                null,
                /*xxwc_ascp_scwb_pkg.get_open_orders_qty(inventory_item_id,
                l_organization_id) open_orders_qty, --Added by Rakesh Patel on 03/25/2016 for TMS#20151023-00037/20160328-00211*/
                /*xxwc_inv_ais_pkg.shipto_last_price_paid(:search_criteria.site_use_id,
                inventory_item_id) list_price,*/
                null,

                -- cross_reference,
                inventory_item_id,
                -- organization_id,
                -- l_organization_id,
                -- item_type,
                primary_uom_code
                 from XXWC_MD_SEARCH_PRODUCTS_MV_S
                where ((0 < (select nvl(xxwc_ascp_scwb_pkg.get_on_hand(inventory_item_id,
                                                                       l_organization_id,
                                                                       'G'),
                                        0)
                               from dual) and xxwc_mv_routines_pkg.get_item_category(inventory_item_id,
                                                                                       l_organization_id,
                                                                                       'Sales Velocity',
                                                                                       1) in
                      ('N', 'Z')) or xxwc_mv_routines_pkg.get_item_category(inventory_item_id,
                                                                             l_organization_id,
                                                                             'Sales Velocity',
                                                                             1) not in
                      ('N', 'Z') or (0 < (select nvl(xxwc_ascp_scwb_pkg.get_on_hand(inventory_item_id,
                                                                                     l_organization_id,
                                                                                     'G'),
                                                      0)
                                             from dual) and
                      xxwc_mv_routines_pkg.get_item_category(inventory_item_id,
                                                                             l_organization_id,
                                                                             'Sales Velocity',
                                                                             1) is null and
                      item_type = 'SPECIAL'))
                  and (contains(shortdescription, l_query_template2, 1) > 0)
               /* order by partnumber desc, quantity_onhand desc, scr desc*/
               );
          else
            insert into XXWC.XXWC_MD_SEARCH_PRODUCT_GTT_TBL
              (score,
               shortdescription,
               partnumber,
               quantityonhand,
               open_sales_orders, --Added by Rakesh Patel on 03/25/2016 for TMS#20151023-00037/20160328-00211/20160328-00211
               list_price,
               inventory_item_id,
               -- organization_id,
               primary_uom_code)
              (select /*+ result_cache */
                score(1) scr,
                -- type,
                --  name,
                shortdescription,
                partnumber,
                null,
                /*xxwc_ascp_scwb_pkg.get_on_hand(inventory_item_id,
                l_organization_id,
                'G') quantity_onhand,*/
                null,
                /*xxwc_ascp_scwb_pkg.get_open_orders_qty(inventory_item_id,
                l_organization_id) open_orders_qty, --Added by Rakesh Patel on 03/25/2016 for TMS#20151023-00037/20160328-00211*/
                /*xxwc_inv_ais_pkg.shipto_last_price_paid(:search_criteria.site_use_id,
                inventory_item_id) list_price,*/
                null,

                -- cross_reference,
                inventory_item_id,
                -- organization_id,
                -- l_organization_id,
                -- item_type,
                primary_uom_code
                 from XXWC_MD_SEARCH_PRODUCTS_MV_S
                where (contains(shortdescription, l_query_template2, 1) > 0));

          end if;
        else
          insert into XXWC.XXWC_MD_SEARCH_PRODUCT_GTT_TBL
            (score,
             shortdescription,
             partnumber,
             quantityonhand,
             open_sales_orders, --Added by Rakesh Patel on 03/25/2016 for TMS#20151023-00037/20160328-00211/20160328-00211
             list_price,
             inventory_item_id,
             -- organization_id,
             primary_uom_code)
            (select /*+ result_cache */
              score(1) scr,
              -- type,
              --  name,
              shortdescription,
              partnumber,
              null,
              /*xxwc_ascp_scwb_pkg.get_on_hand(inventory_item_id,
              l_organization_id,
              'G') quantity_onhand,*/
              null,
              /*xxwc_ascp_scwb_pkg.get_open_orders_qty(inventory_item_id,
              l_organization_id) open_orders_qty, --Added by Rakesh Patel on 03/25/2016 for TMS#20151023-00037/20160328-00211*/
              /*xxwc_inv_ais_pkg.shipto_last_price_paid(:search_criteria.site_use_id,
              inventory_item_id) list_price,*/
              null,
              -- cross_reference,
              inventory_item_id,
              -- organization_id,
              -- l_organization_id,
              -- item_type,
              primary_uom_code
               from XXWC_MD_SEARCH_PRODUCTS_MV_S
              where (contains(shortdescription, l_query_template2, 1) > 0));
        end if;
      end if;
      ---if first word in the string is number and second is all number too then only in part number
      if p_tokens1 is not null and l_count = 0 and l_count3 = 1 and
         l_count6 = 1 /*and l_token_trim_count >= 3*/
       then
        if l_organization_id != 222 then
          if p_filter = 'ONHAND' then

            insert into XXWC.XXWC_MD_SEARCH_PRODUCT_GTT_TBL
              (score,
               shortdescription,
               partnumber,
               quantityonhand,
               open_sales_orders, --Added by Rakesh Patel on 03/25/2016 for TMS#20151023-00037/20160328-00211/20160328-00211
               list_price,
               inventory_item_id,
               -- organization_id,
               primary_uom_code)
              (select /*+ result_cache */
                score(2) scr,
                -- type,
                --  name,
                shortdescription,
                partnumber,
                null,
                /*xxwc_ascp_scwb_pkg.get_on_hand(inventory_item_id,
                l_organization_id,
                'G') quantity_onhand,*/
                null,
                /*xxwc_ascp_scwb_pkg.get_open_orders_qty(inventory_item_id,
                l_organization_id) open_orders_qty, --Added by Rakesh Patel on 03/25/2016 for TMS#20151023-00037/20160328-00211*/
                /*xxwc_inv_ais_pkg.shipto_last_price_paid(:search_criteria.site_use_id,
                inventory_item_id) list_price,*/
                null,
                -- cross_reference,
                inventory_item_id,
                -- organization_id,
                -- l_organization_id,
                -- item_type,
                primary_uom_code
                 from XXWC_MD_SEARCH_PRODUCTS_MV_S
                where /*organization_id + 0 = l_organization_id
                                                                                                                                                                                                                                                                                                                                                                                                                                                           and*/
                0 < (select nvl(xxwc_ascp_scwb_pkg.get_on_hand(inventory_item_id,
                                                               l_organization_id,
                                                               'G'),
                                0)
                       from dual)
            and contains(partnumber, l_query_template, 2) > 0);

          elsif p_filter = 'STANDARD' then

            insert into XXWC.XXWC_MD_SEARCH_PRODUCT_GTT_TBL
              (score,
               shortdescription,
               partnumber,
               quantityonhand,
               open_sales_orders, --Added by Rakesh Patel on 03/25/2016 for TMS#20151023-00037/20160328-00211/20160328-00211
               list_price,
               inventory_item_id,
               -- organization_id,
               primary_uom_code)
              (select /*+ result_cache */
                score(2) scr,
                -- type,
                --  name,
                shortdescription,
                partnumber,
                null,
                /*xxwc_ascp_scwb_pkg.get_on_hand(inventory_item_id,
                l_organization_id,
                'G') quantity_onhand,*/
                null,
                /*xxwc_ascp_scwb_pkg.get_open_orders_qty(inventory_item_id,
                l_organization_id) open_orders_qty, --Added by Rakesh Patel on 03/25/2016 for TMS#20151023-00037/20160328-00211*/
                /*xxwc_inv_ais_pkg.shipto_last_price_paid(:search_criteria.site_use_id,
                inventory_item_id) list_price,*/
                null,
                -- cross_reference,
                inventory_item_id,
                -- organization_id,
                -- l_organization_id,
                -- item_type,
                primary_uom_code
                 from XXWC_MD_SEARCH_PRODUCTS_MV_S
                where ((0 < (select nvl(xxwc_ascp_scwb_pkg.get_on_hand(inventory_item_id,
                                                                       l_organization_id,
                                                                       'G'),
                                        0)
                               from dual) and xxwc_mv_routines_pkg.get_item_category(inventory_item_id,
                                                                                       l_organization_id,
                                                                                       'Sales Velocity',
                                                                                       1) in
                      ('N', 'Z')) or xxwc_mv_routines_pkg.get_item_category(inventory_item_id,
                                                                             l_organization_id,
                                                                             'Sales Velocity',
                                                                             1) not in
                      ('N', 'Z') or (0 < (select nvl(xxwc_ascp_scwb_pkg.get_on_hand(inventory_item_id,
                                                                                     l_organization_id,
                                                                                     'G'),
                                                      0)
                                             from dual) and
                      xxwc_mv_routines_pkg.get_item_category(inventory_item_id,
                                                                             l_organization_id,
                                                                             'Sales Velocity',
                                                                             1) is null and
                      item_type = 'SPECIAL'))
                  and contains(partnumber, l_query_template, 2) > 0);
          else

            insert into XXWC.XXWC_MD_SEARCH_PRODUCT_GTT_TBL
              (score,
               shortdescription,
               partnumber,
               quantityonhand,
               open_sales_orders, --Added by Rakesh Patel on 03/25/2016 for TMS#20151023-00037/20160328-00211/20160328-00211
               list_price,
               inventory_item_id,
               -- organization_id,
               primary_uom_code)
              (select /*+ result_cache */
                score(2) scr,
                -- type,
                --  name,
                shortdescription,
                partnumber,
                null,
                /*xxwc_ascp_scwb_pkg.get_on_hand(inventory_item_id,
                l_organization_id,
                'G') quantity_onhand,*/
                null,
                /*xxwc_ascp_scwb_pkg.get_open_orders_qty(inventory_item_id,
                l_organization_id) open_orders_qty, --Added by Rakesh Patel on 03/25/2016 for TMS#20151023-00037/20160328-00211*/
                /*xxwc_inv_ais_pkg.shipto_last_price_paid(:search_criteria.site_use_id,
                inventory_item_id) list_price,*/
                null,
                -- cross_reference,
                inventory_item_id,
                -- organization_id,
                -- l_organization_id,
                -- item_type,
                primary_uom_code
                 from XXWC_MD_SEARCH_PRODUCTS_MV_S
                where contains(partnumber, l_query_template, 2) > 0);
          end if;
        else
          insert into XXWC.XXWC_MD_SEARCH_PRODUCT_GTT_TBL
            (score,
             shortdescription,
             partnumber,
             quantityonhand,
             open_sales_orders, --Added by Rakesh Patel on 03/25/2016 for TMS#20151023-00037/20160328-00211/20160328-00211
             list_price,
             inventory_item_id,
             -- organization_id,
             primary_uom_code)
            (select /*+ result_cache */
              score(2) scr,
              -- type,
              --  name,
              shortdescription,
              partnumber,
              null,
              /*xxwc_ascp_scwb_pkg.get_on_hand(inventory_item_id,
              l_organization_id,
              'G') quantity_onhand,*/
              null,
              /*xxwc_ascp_scwb_pkg.get_open_orders_qty(inventory_item_id,
              l_organization_id) open_orders_qty, --Added by Rakesh Patel on 03/25/2016 for TMS#20151023-00037/20160328-00211*/
              /*xxwc_inv_ais_pkg.shipto_last_price_paid(:search_criteria.site_use_id,
              inventory_item_id) list_price,*/
              null,
              -- cross_reference,
              inventory_item_id,
              -- organization_id,
              -- l_organization_id,
              -- item_type,
              primary_uom_code
               from XXWC_MD_SEARCH_PRODUCTS_MV_S
              where contains(partnumber, l_query_template, 2) > 0);
        end if;
      end if;

      -- if there is special character as handled above query search takes the below path by searching only through description
      if p_tokens1 is not null and l_count = 1 and l_count3 = 0 then
        if l_organization_id != 222 then
          if p_filter = 'ONHAND' then

            insert into XXWC.XXWC_MD_SEARCH_PRODUCT_GTT_TBL
              (score,
               shortdescription,
               partnumber,
               quantityonhand,
               open_sales_orders, --Added by Rakesh Patel on 03/25/2016 for TMS#20151023-00037/20160328-00211/20160328-00211
               list_price,
               inventory_item_id,
               -- organization_id,
               primary_uom_code)
              (select /*+ result_cache */
                score(1) + score(2) scr,
                -- type,
                --  name,
                shortdescription,
                partnumber,
                null,
                /*xxwc_ascp_scwb_pkg.get_on_hand(inventory_item_id,
                l_organization_id,
                'G') quantity_onhand,*/
                null,
                /*xxwc_ascp_scwb_pkg.get_open_orders_qty(inventory_item_id,
                l_organization_id) open_orders_qty, --Added by Rakesh Patel on 03/25/2016 for TMS#20151023-00037/20160328-00211*/
                /*xxwc_inv_ais_pkg.shipto_last_price_paid(:search_criteria.site_use_id,
                inventory_item_id) list_price,*/
                null,
                -- cross_reference,
                inventory_item_id,
                -- organization_id,
                -- l_organization_id,
                -- item_type,
                primary_uom_code
                 from XXWC_MD_SEARCH_PRODUCTS_MV_S
                where 0 < (select nvl(xxwc_ascp_scwb_pkg.get_on_hand(inventory_item_id,
                                                                     l_organization_id,
                                                                     'G'),
                                      0)
                             from dual)
                  and (contains(shortdescription, l_query_template5, 1) > 0 and
                      contains(shortdescription, l_query_template6, 2) > 0));
          elsif p_filter = 'STANDARD' then

            insert into XXWC.XXWC_MD_SEARCH_PRODUCT_GTT_TBL
              (score,
               shortdescription,
               partnumber,
               quantityonhand,
               open_sales_orders, --Added by Rakesh Patel on 03/25/2016 for TMS#20151023-00037/20160328-00211/20160328-00211
               list_price,
               inventory_item_id,
               -- organization_id,
               primary_uom_code)
              (select /*+ result_cache */
                score(1) + score(2) scr,
                -- type,
                --  name,
                shortdescription,
                partnumber,
                null,
                /*xxwc_ascp_scwb_pkg.get_on_hand(inventory_item_id,
                l_organization_id,
                'G') quantity_onhand,*/
                null,
                /*xxwc_ascp_scwb_pkg.get_open_orders_qty(inventory_item_id,
                l_organization_id) open_orders_qty, --Added by Rakesh Patel on 03/25/2016 for TMS#20151023-00037/20160328-00211*/
                /*xxwc_inv_ais_pkg.shipto_last_price_paid(:search_criteria.site_use_id,
                inventory_item_id) list_price,*/
                null,
                -- cross_reference,
                inventory_item_id,
                -- organization_id,
                -- l_organization_id,
                -- item_type,
                primary_uom_code
                 from XXWC_MD_SEARCH_PRODUCTS_MV_S
                where ((0 < (select nvl(xxwc_ascp_scwb_pkg.get_on_hand(inventory_item_id,
                                                                       l_organization_id,
                                                                       'G'),
                                        0)
                               from dual) and xxwc_mv_routines_pkg.get_item_category(inventory_item_id,
                                                                                       l_organization_id,
                                                                                       'Sales Velocity',
                                                                                       1) in
                      ('N', 'Z')) or xxwc_mv_routines_pkg.get_item_category(inventory_item_id,
                                                                             l_organization_id,
                                                                             'Sales Velocity',
                                                                             1) not in
                      ('N', 'Z') or (0 < (select nvl(xxwc_ascp_scwb_pkg.get_on_hand(inventory_item_id,
                                                                                     l_organization_id,
                                                                                     'G'),
                                                      0)
                                             from dual) and
                      xxwc_mv_routines_pkg.get_item_category(inventory_item_id,
                                                                             l_organization_id,
                                                                             'Sales Velocity',
                                                                             1) is null and
                      item_type = 'SPECIAL'))
                  and (contains(shortdescription, l_query_template5, 1) > 0 and
                      contains(shortdescription, l_query_template6, 2) > 0));

          else

            insert into XXWC.XXWC_MD_SEARCH_PRODUCT_GTT_TBL
              (score,
               shortdescription,
               partnumber,
               quantityonhand,
               open_sales_orders, --Added by Rakesh Patel on 03/25/2016 for TMS#20151023-00037/20160328-00211/20160328-00211
               list_price,
               inventory_item_id,
               -- organization_id,
               primary_uom_code)
              (select /*+ result_cache */
                score(1) + score(2) scr,
                -- type,
                --  name,
                shortdescription,
                partnumber,
                null,
                /*xxwc_ascp_scwb_pkg.get_on_hand(inventory_item_id,
                l_organization_id,
                'G') quantity_onhand,*/
                null,
                /*xxwc_ascp_scwb_pkg.get_open_orders_qty(inventory_item_id,
                l_organization_id) open_orders_qty, --Added by Rakesh Patel on 03/25/2016 for TMS#20151023-00037/20160328-00211*/
                /*xxwc_inv_ais_pkg.shipto_last_price_paid(:search_criteria.site_use_id,
                inventory_item_id) list_price,*/
                null,
                -- cross_reference,
                inventory_item_id,
                -- organization_id,
                -- l_organization_id,
                -- item_type,
                primary_uom_code
                 from XXWC_MD_SEARCH_PRODUCTS_MV_S
                where (contains(shortdescription, l_query_template5, 1) > 0 and
                      contains(shortdescription, l_query_template6, 2) > 0));
          end if;
        else
          insert into XXWC.XXWC_MD_SEARCH_PRODUCT_GTT_TBL
            (score,
             shortdescription,
             partnumber,
             quantityonhand,
             open_sales_orders, --Added by Rakesh Patel on 03/25/2016 for TMS#20151023-00037/20160328-00211/20160328-00211
             list_price,
             inventory_item_id,
             -- organization_id,
             primary_uom_code)
            (select /*+ result_cache */
              score(1) + score(2) scr,
              -- type,
              --  name,
              shortdescription,
              partnumber,
              null,
              /*xxwc_ascp_scwb_pkg.get_on_hand(inventory_item_id,
              l_organization_id,
              'G') quantity_onhand,*/
              null,
              /*xxwc_ascp_scwb_pkg.get_open_orders_qty(inventory_item_id,
              l_organization_id) open_orders_qty, --Added by Rakesh Patel on 03/25/2016 for TMS#20151023-00037/20160328-00211*/
              /*xxwc_inv_ais_pkg.shipto_last_price_paid(:search_criteria.site_use_id,
              inventory_item_id) list_price,*/
              null,
              -- cross_reference,
              inventory_item_id,
              -- organization_id,
              -- l_organization_id,
              -- item_type,
              primary_uom_code
               from XXWC_MD_SEARCH_PRODUCTS_MV_S
              where (contains(shortdescription, l_query_template5, 1) > 0 and
                    contains(shortdescription, l_query_template6, 2) > 0));
        end if;
      end if;
      -- if there is all characters as handled above query search takes the below path by searching only through description
      if p_tokens1 is not null and l_count5 = 1 then
        if l_organization_id != 222 then
          if p_filter = 'ONHAND' then
            insert into XXWC.XXWC_MD_SEARCH_PRODUCT_GTT_TBL
              (score,
               shortdescription,
               partnumber,
               quantityonhand,
               open_sales_orders, --Added by Rakesh Patel on 03/25/2016 for TMS#20151023-00037/20160328-00211/20160328-00211
               list_price,
               inventory_item_id,
               -- organization_id,
               primary_uom_code)
              (select /*+ result_cache */
                score(1) scr,
                -- type,
                --  name,
                shortdescription,
                partnumber,
                null,
                /*xxwc_ascp_scwb_pkg.get_on_hand(inventory_item_id,
                l_organization_id,
                'G') quantity_onhand,*/
                null,
                /*xxwc_ascp_scwb_pkg.get_open_orders_qty(inventory_item_id,
                l_organization_id) open_orders_qty, --Added by Rakesh Patel on 03/25/2016 for TMS#20151023-00037/20160328-00211*/
                /*xxwc_inv_ais_pkg.shipto_last_price_paid(:search_criteria.site_use_id,
                inventory_item_id) list_price,*/
                null,
                -- cross_reference,
                inventory_item_id,
                -- organization_id,
                -- l_organization_id,
                -- item_type,
                primary_uom_code
                 from XXWC_MD_SEARCH_PRODUCTS_MV_S
                where 0 < (select nvl(xxwc_ascp_scwb_pkg.get_on_hand(inventory_item_id,
                                                                     l_organization_id,
                                                                     'G'),
                                      0)
                             from dual)
                     /*nvl(xxwc_ascp_scwb_pkg.get_on_hand(inventory_item_id,
                                                    l_organization_id,
                                                    'G'),
                     0) > 0*/
                  and (contains(shortdescription, l_query_template2, 1) > 0));
          elsif p_filter = 'STANDARD' then

            insert into XXWC.XXWC_MD_SEARCH_PRODUCT_GTT_TBL
              (score,
               shortdescription,
               partnumber,
               quantityonhand,
               open_sales_orders, --Added by Rakesh Patel on 03/25/2016 for TMS#20151023-00037/20160328-00211/20160328-00211 --Added by Rakesh Patel on 03/25/2016 for TMS#20151023-00037/20160328-00211/20160328-00211
               list_price,
               inventory_item_id,
               -- organization_id,
               primary_uom_code)
              (select /*+ result_cache */
                score(1) scr,
                -- type,
                --  name,
                shortdescription,
                partnumber,
                null,
                /*xxwc_ascp_scwb_pkg.get_on_hand(inventory_item_id,
                l_organization_id,
                'G') quantity_onhand,*/
                null,
                /*xxwc_ascp_scwb_pkg.get_open_orders_qty(inventory_item_id,
                l_organization_id) open_orders_qty, --Added by Rakesh Patel on 03/25/2016 for TMS#20151023-00037/20160328-00211*/
                /*xxwc_inv_ais_pkg.shipto_last_price_paid(:search_criteria.site_use_id,
                inventory_item_id) list_price,*/
                null,
                -- cross_reference,
                inventory_item_id,
                -- organization_id,
                -- l_organization_id,
                -- item_type,
                primary_uom_code
                 from XXWC_MD_SEARCH_PRODUCTS_MV_S
                where ((0 < (select nvl(xxwc_ascp_scwb_pkg.get_on_hand(inventory_item_id,
                                                                       l_organization_id,
                                                                       'G'),
                                        0)
                               from dual) and xxwc_mv_routines_pkg.get_item_category(inventory_item_id,
                                                                                       l_organization_id,
                                                                                       'Sales Velocity',
                                                                                       1) in
                      ('N', 'Z')) or xxwc_mv_routines_pkg.get_item_category(inventory_item_id,
                                                                             l_organization_id,
                                                                             'Sales Velocity',
                                                                             1) not in
                      ('N', 'Z') or (0 < (select nvl(xxwc_ascp_scwb_pkg.get_on_hand(inventory_item_id,
                                                                                     l_organization_id,
                                                                                     'G'),
                                                      0)
                                             from dual) and
                      xxwc_mv_routines_pkg.get_item_category(inventory_item_id,
                                                                             l_organization_id,
                                                                             'Sales Velocity',
                                                                             1) is null and
                      item_type = 'SPECIAL'))
                  and (contains(shortdescription, l_query_template2, 1) > 0));

          else

            insert into XXWC.XXWC_MD_SEARCH_PRODUCT_GTT_TBL
              (score,
               shortdescription,
               partnumber,
               quantityonhand,
               open_sales_orders, --Added by Rakesh Patel on 03/25/2016 for TMS#20151023-00037/20160328-00211/20160328-00211
               list_price,
               inventory_item_id,
               -- organization_id,
               primary_uom_code)
              (select /*+ result_cache */
                score(1) scr,
                -- type,
                --  name,
                shortdescription,
                partnumber,
                null,
                /*xxwc_ascp_scwb_pkg.get_on_hand(inventory_item_id,
                l_organization_id,
                'G') quantity_onhand,*/
                null,
                /*xxwc_ascp_scwb_pkg.get_open_orders_qty(inventory_item_id,
                l_organization_id) open_orders_qty, --Added by Rakesh Patel on 03/25/2016 for TMS#20151023-00037/20160328-00211*/
                /*xxwc_inv_ais_pkg.shipto_last_price_paid(:search_criteria.site_use_id,
                inventory_item_id) list_price,*/
                null,
                -- cross_reference,
                inventory_item_id,
                -- organization_id,
                -- l_organization_id,
                -- item_type,
                primary_uom_code
                 from XXWC_MD_SEARCH_PRODUCTS_MV_S
                where (contains(shortdescription, l_query_template2, 1) > 0));

          end if;
        else
          insert into XXWC.XXWC_MD_SEARCH_PRODUCT_GTT_TBL
            (score,
             shortdescription,
             partnumber,
             quantityonhand,
             open_sales_orders, --Added by Rakesh Patel on 03/25/2016 for TMS#20151023-00037/20160328-00211/20160328-00211
             list_price,
             inventory_item_id,
             -- organization_id,
             primary_uom_code)
            (select /*+ result_cache */
              score(1) scr,
              -- type,
              --  name,
              shortdescription,
              partnumber,
              null,
              /*xxwc_ascp_scwb_pkg.get_on_hand(inventory_item_id,
              l_organization_id,
              'G') quantity_onhand,*/
              null,
              /*xxwc_ascp_scwb_pkg.get_open_orders_qty(inventory_item_id,
              l_organization_id) open_orders_qty, --Added by Rakesh Patel on 03/25/2016 for TMS#20151023-00037/20160328-00211*/
              /*xxwc_inv_ais_pkg.shipto_last_price_paid(:search_criteria.site_use_id,
              inventory_item_id) list_price,*/
              null,
              -- cross_reference,
              inventory_item_id,
              -- organization_id,
              -- l_organization_id,
              -- item_type,
              primary_uom_code
               from XXWC_MD_SEARCH_PRODUCTS_MV_S
              where (contains(shortdescription, l_query_template2, 1) > 0));
        end if;
      end if;
      --line_pricing_data;
      IF p_jobsiteid IS NOT NULL THEN
         IF NVL(p_call_pr_api, 'Y') = 'Y' THEN --Rev 2.2
            line_pricing_data (  p_organization_id => p_branch_id
                               , p_cust_account_id => p_customer_id
                               , p_site_use_id     => p_jobsiteid
                              );
         END IF; --Rev 2.2                     
      END IF;
      for rec in (select /*+ result_cache */
                  /*+ INDEX(MT MTL_SYSTEM_ITEMS_B_U1)*/
                   GT.score,
                   GT.shortdescription,
                   GT.partnumber,
                   GT.quantityonhand                quantity_onhand,
                   GT.open_sales_orders, --Added by Rakesh Patel on 03/25/2016 for TMS#20151023-00037/20160328-00211/20160328-00211
                   GT.list_price,
                   GT.inventory_item_id,
                   GT.selling_price,
                   GT.modifier,
                   GT.modifier_type,
                   p_branch_id organization_id,
                   GT.primary_uom_code,
                   GT.RESERVABLE,
                   GT.LIST_PRICE_2
                    from XXWC.XXWC_MD_SEARCH_PRODUCT_GTT_TBL GT
                   order by case
                              when l_sorting_type = 'PARTNUMBER' then
                               partnumber
                            end asc,
                            case
                              when l_sorting_type = 'ONHAND' then
                               quantityonhand
                            end desc, --TMS 20161128-00028
                            case
                              when l_sorting_type = 'SCR' then
                               score
                            end desc) loop  --TMS 20161128-00028
        --:XXWC_INV_ITEM_SEARCH_V.reservable := rec.RESERVABLE; --Added by Rakesh Patel on 03/25/2016 for TMS#20151023-00037/20160328-00211

       -- :XXWC_INV_ITEM_SEARCH_V.open_sales_orders := rec.open_sales_orders; --Added by Rakesh Patel on 03/25/2016 for TMS#20151023-00037/20160328-00211

      --  :XXWC_INV_ITEM_SEARCH_V.INVENTORY_ITEM_ID := rec.inventory_item_id;

       -- :XXWC_INV_ITEM_SEARCH_V.ORGANIZATION_ID := :search_criteria.organization_id;

       -- :XXWC_INV_ITEM_SEARCH_V.SEGMENT1 := rec.partnumber;

        --:XXWC_INV_ITEM_SEARCH_V.DESCRIPTION := rec.shortdescription;

       -- :XXWC_INV_ITEM_SEARCH_V.ON_HAND_QTY     := rec.quantity_onhand;
       -- :XXWC_INV_ITEM_SEARCH_V.LAST_PRICE_PAID := rec.list_price;
        --:XXWC_INV_ITEM_SEARCH_V.SELLING_PRICE := rec.list_price; -- 20160307-00117
       -- :XXWC_INV_ITEM_SEARCH_V.LIST_PRICE       := rec.list_price_2; -- 20160307-00117
       -- :XXWC_INV_ITEM_SEARCH_V.SELLING_PRICE    := NVL(rec.SELLING_PRICE,
                                                 --         :XXWC_INV_ITEM_SEARCH_V.LIST_PRICE); -- 20160307-00117
       -- :XXWC_INV_ITEM_SEARCH_V.MOD_TYPE         := rec.modifier_type; -- 20160307-00117
      --  :XXWC_INV_ITEM_SEARCH_V.MODIFIER_APPLIED := rec.modifier; -- 20160307-00117
       -- :XXWC_INV_ITEM_SEARCH_V.PRIMARY_UOM      := rec.primary_uom_code;
        l_record_count                           := l_record_count + 1;
     --   next_record;

      --  :CONTROL.QUERY_COUNT := l_record_count;

      /*score_range_size := 100 / number_of_steps;
                          this_score_group := rec.score / score_range_size;
                          l_counter        := l_counter + 1;
                          exit when this_score_group < last_score_group;
                          last_score_group := this_score_group;
                          exit when l_counter >= l_max_rows;
                        */
                        NULL;
      END LOOP;

      if l_record_count = 0 then
        if l_organization_id != 222 then
          if p_filter = 'ONHAND' then
            insert into XXWC.XXWC_MD_SEARCH_PRODUCT_GTT_TBL
              (score,
               shortdescription,
               partnumber,
               quantityonhand,
               open_sales_orders, --Added by Rakesh Patel on 03/25/2016 for TMS#20151023-00037/20160328-00211
               list_price,
               inventory_item_id,
               -- organization_id,
               primary_uom_code)
              (select /*+ result_cache */
                score(1) + score(2) + score(3) + score(4) + score(5) scr,
                -- type,
                --  name,
                shortdescription,
                partnumber,
                null,
                /*xxwc_ascp_scwb_pkg.get_on_hand(inventory_item_id,
                l_organization_id,
                'G') quantity_onhand,*/
                null,
                /*xxwc_ascp_scwb_pkg.get_open_orders_qty(inventory_item_id,
                l_organization_id) open_orders_qty, --Added by Rakesh Patel on 03/25/2016 for TMS#20151023-00037/20160328-00211*/
                /*xxwc_inv_ais_pkg.shipto_last_price_paid(:search_criteria.site_use_id,
                inventory_item_id) list_price,*/
                null,
                -- cross_reference,
                inventory_item_id,
                -- organization_id,
                -- l_organization_id,
                -- item_type,
                primary_uom_code
                 from XXWC_MD_SEARCH_PRODUCTS_MV_S
                where 0 < (select nvl(xxwc_ascp_scwb_pkg.get_on_hand(inventory_item_id,
                                                                     l_organization_id,
                                                                     'G'),
                                      0)
                             from dual)

                  and (contains(partnumber, l_query_template, 1) > 0 or
                      contains(shortdescription, l_query_template2, 4) > 0 or
                      contains(cross_reference, l_query_template3, 5) > 0 or
                      (contains(partnumber, l_trim_template, 2) > 0 and
                      contains(dummy, l_rem_template, 3) > 0)

                      ));
          elsif p_filter = 'STANDARD' then

            insert into XXWC.XXWC_MD_SEARCH_PRODUCT_GTT_TBL
              (score,
               shortdescription,
               partnumber,
               quantityonhand,
               open_sales_orders, --Added by Rakesh Patel on 03/25/2016 for TMS#20151023-00037/20160328-00211
               list_price,
               inventory_item_id,
               -- organization_id,
               primary_uom_code)
              (select /*+ result_cache */
                score(1) + score(2) + score(3) + score(4) + score(5) scr,
                -- type,
                --  name,
                shortdescription,
                partnumber,
                null,
                /*xxwc_ascp_scwb_pkg.get_on_hand(inventory_item_id,
                l_organization_id,
                'G') quantity_onhand,*/
                null,
                /*xxwc_ascp_scwb_pkg.get_open_orders_qty(inventory_item_id,
                l_organization_id) open_orders_qty, --Added by Rakesh Patel on 03/25/2016 for TMS#20151023-00037/20160328-00211*/
                /*xxwc_inv_ais_pkg.shipto_last_price_paid(:search_criteria.site_use_id,
                inventory_item_id) list_price,*/
                null,
                -- cross_reference,
                inventory_item_id,
                -- organization_id,
                -- l_organization_id,
                -- item_type,
                primary_uom_code
                 from XXWC_MD_SEARCH_PRODUCTS_MV_S
                where ((0 < (select nvl(xxwc_ascp_scwb_pkg.get_on_hand(inventory_item_id,
                                                                       l_organization_id,
                                                                       'G'),
                                        0)
                               from dual) and xxwc_mv_routines_pkg.get_item_category(inventory_item_id,
                                                                                       l_organization_id,
                                                                                       'Sales Velocity',
                                                                                       1) in
                      ('N', 'Z')) or xxwc_mv_routines_pkg.get_item_category(inventory_item_id,
                                                                             l_organization_id,
                                                                             'Sales Velocity',
                                                                             1) not in
                      ('N', 'Z') or (0 < (select nvl(xxwc_ascp_scwb_pkg.get_on_hand(inventory_item_id,
                                                                                     l_organization_id,
                                                                                     'G'),
                                                      0)
                                             from dual) and
                      xxwc_mv_routines_pkg.get_item_category(inventory_item_id,
                                                                             l_organization_id,
                                                                             'Sales Velocity',
                                                                             1) is null and
                      item_type = 'SPECIAL'))
                  and (contains(partnumber, l_query_template, 1) > 0 or
                      contains(shortdescription, l_query_template2, 4) > 0 or
                      contains(cross_reference, l_query_template3, 5) > 0 or
                      (contains(partnumber, l_trim_template, 2) > 0 and
                      contains(dummy, l_rem_template, 3) > 0)));
          else

            insert into XXWC.XXWC_MD_SEARCH_PRODUCT_GTT_TBL
              (score,
               shortdescription,
               partnumber,
               quantityonhand,
               open_sales_orders, --Added by Rakesh Patel on 03/25/2016 for TMS#20151023-00037/20160328-00211
               list_price,
               inventory_item_id,
               -- organization_id,
               primary_uom_code)
              (select /*+ result_cache */
                score(1) + score(2) + score(3) + score(4) + score(5) scr,
                -- type,
                --  name,
                shortdescription,
                partnumber,
                null,
                /*xxwc_ascp_scwb_pkg.get_on_hand(inventory_item_id,
                l_organization_id,
                'G') quantity_onhand,*/
                null,
                /*xxwc_ascp_scwb_pkg.get_open_orders_qty(inventory_item_id,
                l_organization_id) open_orders_qty, --Added by Rakesh Patel on 03/25/2016 for TMS#20151023-00037/20160328-00211*/
                /*xxwc_inv_ais_pkg.shipto_last_price_paid(:search_criteria.site_use_id,
                inventory_item_id) list_price,*/
                null,
                -- cross_reference,
                inventory_item_id,
                -- organization_id,
                -- l_organization_id,
                -- item_type,
                primary_uom_code
                 from XXWC_MD_SEARCH_PRODUCTS_MV_S
                where (contains(partnumber, l_query_template, 1) > 0 or
                      contains(shortdescription, l_query_template2, 4) > 0 or
                      contains(cross_reference, l_query_template3, 5) > 0 or
                      (contains(partnumber, l_trim_template, 2) > 0 and
                      contains(dummy, l_rem_template, 3) > 0)));

          end if;
        else
          insert into XXWC.XXWC_MD_SEARCH_PRODUCT_GTT_TBL
            (score,
             shortdescription,
             partnumber,
             quantityonhand,
             open_sales_orders, --Added by Rakesh Patel on 03/25/2016 for TMS#20151023-00037/20160328-00211
             list_price,
             inventory_item_id,
             -- organization_id,
             primary_uom_code)
            (select /*+ result_cache */
              score(1) + score(2) + score(3) + score(4) + score(5) scr,
              -- type,
              --  name,
              shortdescription,
              partnumber,
              null,
              /*xxwc_ascp_scwb_pkg.get_on_hand(inventory_item_id,
              l_organization_id,
              'G') quantity_onhand,*/
              null,
              /*xxwc_ascp_scwb_pkg.get_open_orders_qty(inventory_item_id,
              l_organization_id) open_orders_qty, --Added by Rakesh Patel on 03/25/2016 for TMS#20151023-00037/20160328-00211*/
              /*xxwc_inv_ais_pkg.shipto_last_price_paid(:search_criteria.site_use_id,
              inventory_item_id) list_price,*/
              null,
              -- cross_reference,
              inventory_item_id,
              -- organization_id,
              -- l_organization_id,
              -- item_type,
              primary_uom_code
               from XXWC_MD_SEARCH_PRODUCTS_MV_S
              where /*organization_id + 0 = l_organization_id
                                                                                                                                                                                                                                                                                                                                                                                                                        and*/
              (contains(partnumber, l_query_template, 1) > 0 or
              contains(shortdescription, l_query_template2, 4) > 0 or
              contains(cross_reference, l_query_template3, 5) > 0 or
              (contains(partnumber, l_trim_template, 2) > 0 and
              contains(dummy, l_rem_template, 3) > 0)));
        end if;

        --line_pricing_data;
        IF p_jobsiteid IS NOT NULL THEN
           IF NVL(p_call_pr_api, 'Y') = 'Y' THEN --Rev 2.2
              line_pricing_data (  p_organization_id => p_branch_id
                                 , p_cust_account_id => p_customer_id
                                 , p_site_use_id     => p_jobsiteid
                                );
           END IF; --Rev 2.2                          
        END IF;

        for rec in (select /*+ result_cache */
                     /*+ INDEX(MT MTL_SYSTEM_ITEMS_B_U1)*/
                     GT.score,
                     GT.shortdescription,
                     GT.partnumber,
                     GT.quantityonhand                quantity_onhand,
                     GT.open_sales_orders, --Added by Rakesh Patel on 03/25/2016 for TMS#20151023-00037/20160328-00211
                     GT.list_price,
                     GT.inventory_item_id,
                     GT.selling_price,
                     GT.modifier,
                     GT.modifier_type,
                     p_branch_id,--:search_criteria.organization_id organization_id,
                     GT.primary_uom_code,
                     GT.RESERVABLE,
                     GT.LIST_PRICE_2
                      from XXWC.XXWC_MD_SEARCH_PRODUCT_GTT_TBL GT
                     order by case
                                when l_sorting_type = 'PARTNUMBER' then
                                 partnumber
                              end asc,
                              case
                                when l_sorting_type = 'ONHAND' then
                                 quantityonhand
                              end desc, --TMS 20161128-0002
                              case
                                when l_sorting_type = 'SCR' then
                                 score
                              end desc) loop --TMS 20161128-00028
          --:XXWC_INV_ITEM_SEARCH_V.reservable := rec.RESERVABLE; --Added by Rakesh Patel on 03/25/2016 for TMS#20151023-00037/20160328-00211

         -- :XXWC_INV_ITEM_SEARCH_V.open_sales_orders := rec.open_sales_orders; --Added by Rakesh Patel on 03/25/2016 for TMS#20151023-00037/20160328-00211

         -- :XXWC_INV_ITEM_SEARCH_V.INVENTORY_ITEM_ID := rec.inventory_item_id;

        --  :XXWC_INV_ITEM_SEARCH_V.ORGANIZATION_ID := :search_criteria.organization_id;

       --   :XXWC_INV_ITEM_SEARCH_V.SEGMENT1 := rec.partnumber;

       --  :XXWC_INV_ITEM_SEARCH_V.DESCRIPTION := rec.shortdescription;

      --   :XXWC_INV_ITEM_SEARCH_V.ON_HAND_QTY     := rec.quantity_onhand;
       --   :XXWC_INV_ITEM_SEARCH_V.LAST_PRICE_PAID := rec.list_price;
          --:XXWC_INV_ITEM_SEARCH_V.SELLING_PRICE := rec.list_price; -- 20160307-00117
       --   :XXWC_INV_ITEM_SEARCH_V.LIST_PRICE       := rec.list_price_2; -- 20160307-00117
       --   :XXWC_INV_ITEM_SEARCH_V.SELLING_PRICE    := NVL(rec.SELLING_PRICE,
          --                                                :XXWC_INV_ITEM_SEARCH_V.LIST_PRICE); -- 20160307-00117
       --   :XXWC_INV_ITEM_SEARCH_V.MOD_TYPE         := rec.modifier_type; -- 20160307-00117
        --  :XXWC_INV_ITEM_SEARCH_V.MODIFIER_APPLIED := rec.modifier; -- 20160307-00117

        --  :XXWC_INV_ITEM_SEARCH_V.PRIMARY_UOM := rec.primary_uom_code;
          l_record_count                      := l_record_count + 1;

        --  next_record;

       --   :CONTROL.QUERY_COUNT := l_record_count;

        /* score_range_size := 100 / number_of_steps;
                                  this_score_group := rec.score / score_range_size;
                                  l_counter        := l_counter + 1;
                                  exit when this_score_group < last_score_group;
                                  last_score_group := this_score_group;
                                  exit when l_counter >= l_max_rows;
                                  */
                                 NULL;
        END LOOP;

        --if l_record_count = 0 then
        --  go_block('SEARCH_CRITERIA');
       --   raise form_trigger_failure;
       -- end if;
       -- first_record;
      end if;
     -- first_record;
      --  end if;
    end if;
    /* This is code is for One word that came in the token */

    if l_count2 = 1 then
      -- frequent words
      if p_tokens1 is not null and l_count9 = 1 then
        --TMS 20160324-00153 End
        if l_organization_id != 222 then
          if p_filter = 'ONHAND' then

            insert into XXWC.XXWC_MD_SEARCH_PRODUCT_GTT_TBL
              (score,
               shortdescription,
               partnumber,
               quantityonhand,
               open_sales_orders, --Added by Rakesh Patel on 03/25/2016 for TMS#20151023-00037/20160328-00211
               list_price,
               inventory_item_id,
               -- organization_id,
               primary_uom_code)
              (select /*+ result_cache */
                score(1) scr,
                -- type,
                --  name,
                shortdescription,
                partnumber,
                null,
                /*xxwc_ascp_scwb_pkg.get_on_hand(inventory_item_id,
                l_organization_id,
                'G') quantity_onhand,*/
                null,
                /*xxwc_ascp_scwb_pkg.get_open_orders_qty(inventory_item_id,
                l_organization_id) open_orders_qty, --Added by Rakesh Patel on 03/25/2016 for TMS#20151023-00037/20160328-00211*/
                /*xxwc_inv_ais_pkg.shipto_last_price_paid(:search_criteria.site_use_id,
                inventory_item_id) list_price,*/
                null,
                -- cross_reference,
                inventory_item_id,
                -- organization_id,
                -- l_organization_id,
                -- item_type,
                primary_uom_code
                 from XXWC_MD_SEARCH_PRODUCTS_MV_S
                where 0 < (select nvl(xxwc_ascp_scwb_pkg.get_on_hand(inventory_item_id,
                                                                     l_organization_id,
                                                                     'G'),
                                      0)
                             from dual)
                     /*nvl(xxwc_ascp_scwb_pkg.get_on_hand(inventory_item_id,
                                                    l_organization_id,
                                                    'G'),
                     0) > 0*/
                  and (contains(shortdescription, l_frequent_template, 1) > 0));

          elsif p_filter = 'STANDARD' then

            insert into XXWC.XXWC_MD_SEARCH_PRODUCT_GTT_TBL
              (score,
               shortdescription,
               partnumber,
               quantityonhand,
               open_sales_orders, --Added by Rakesh Patel on 03/25/2016 for TMS#20151023-00037/20160328-00211
               list_price,
               inventory_item_id,
               -- organization_id,
               primary_uom_code)
              (select /*+ result_cache */
                score(1) scr,
                -- type,
                --  name,
                shortdescription,
                partnumber,
                null,
                /*xxwc_ascp_scwb_pkg.get_on_hand(inventory_item_id,
                l_organization_id,
                'G') quantity_onhand,*/
                null,
                /*xxwc_ascp_scwb_pkg.get_open_orders_qty(inventory_item_id,
                l_organization_id) open_orders_qty, --Added by Rakesh Patel on 03/25/2016 for TMS#20151023-00037/20160328-00211*/
                /*xxwc_inv_ais_pkg.shipto_last_price_paid(:search_criteria.site_use_id,
                inventory_item_id) list_price,*/
                null,
                -- cross_reference,
                inventory_item_id,
                -- organization_id,
                -- l_organization_id,
                -- item_type,
                primary_uom_code
                 from XXWC_MD_SEARCH_PRODUCTS_MV_S
                where ((0 < (select nvl(xxwc_ascp_scwb_pkg.get_on_hand(inventory_item_id,
                                                                       l_organization_id,
                                                                       'G'),
                                        0)
                               from dual) and xxwc_mv_routines_pkg.get_item_category(inventory_item_id,
                                                                                       l_organization_id,
                                                                                       'Sales Velocity',
                                                                                       1) in
                      ('N', 'Z')) or xxwc_mv_routines_pkg.get_item_category(inventory_item_id,
                                                                             l_organization_id,
                                                                             'Sales Velocity',
                                                                             1) not in
                      ('N', 'Z') or (0 < (select nvl(xxwc_ascp_scwb_pkg.get_on_hand(inventory_item_id,
                                                                                     l_organization_id,
                                                                                     'G'),
                                                      0)
                                             from dual) and
                      xxwc_mv_routines_pkg.get_item_category(inventory_item_id,
                                                                             l_organization_id,
                                                                             'Sales Velocity',
                                                                             1) is null and
                      item_type = 'SPECIAL'))
                  and (contains(shortdescription, l_frequent_template, 1) > 0));
          else

            insert into XXWC.XXWC_MD_SEARCH_PRODUCT_GTT_TBL
              (score,
               shortdescription,
               partnumber,
               quantityonhand,
               open_sales_orders, --Added by Rakesh Patel on 03/25/2016 for TMS#20151023-00037/20160328-00211
               list_price,
               inventory_item_id,
               -- organization_id,
               primary_uom_code)
              (select /*+ result_cache */
                score(1) scr,
                -- type,
                --  name,
                shortdescription,
                partnumber,
                null,
                /*xxwc_ascp_scwb_pkg.get_on_hand(inventory_item_id,
                l_organization_id,
                'G') quantity_onhand,*/
                null,
                /*xxwc_ascp_scwb_pkg.get_open_orders_qty(inventory_item_id,
                l_organization_id) open_orders_qty, --Added by Rakesh Patel on 03/25/2016 for TMS#20151023-00037/20160328-00211*/
                /*xxwc_inv_ais_pkg.shipto_last_price_paid(:search_criteria.site_use_id,
                inventory_item_id) list_price,*/
                null,

                -- cross_reference,
                inventory_item_id,
                -- organization_id,
                -- l_organization_id,
                -- item_type,
                primary_uom_code
                 from XXWC_MD_SEARCH_PRODUCTS_MV_S
                where (contains(shortdescription, l_frequent_template, 1) > 0));
          end if;
        else
          insert into XXWC.XXWC_MD_SEARCH_PRODUCT_GTT_TBL
            (score,
             shortdescription,
             partnumber,
             quantityonhand,
             open_sales_orders, --Added by Rakesh Patel on 03/25/2016 for TMS#20151023-00037/20160328-00211
             list_price,
             inventory_item_id,
             -- organization_id,
             primary_uom_code)
            (select /*+ result_cache */
              score(1) scr,
              -- type,
              --  name,
              shortdescription,
              partnumber,
              null,
              /*xxwc_ascp_scwb_pkg.get_on_hand(inventory_item_id,
              l_organization_id,
              'G') quantity_onhand,*/
              null,
              /*xxwc_ascp_scwb_pkg.get_open_orders_qty(inventory_item_id,
              l_organization_id) open_orders_qty, --Added by Rakesh Patel on 03/25/2016 for TMS#20151023-00037/20160328-00211*/
              /*xxwc_inv_ais_pkg.shipto_last_price_paid(:search_criteria.site_use_id,
              inventory_item_id) list_price,*/
              null,

              -- cross_reference,
              inventory_item_id,
              -- organization_id,
              -- l_organization_id,
              -- item_type,
              primary_uom_code
               from XXWC_MD_SEARCH_PRODUCTS_MV_S
              where (contains(shortdescription, l_frequent_template, 1) > 0));
        end if;
      end if;
      -- if there is first three letter number or sp/ or sp- or R
      --To remove duplicates from the form for single word search starting with R added condition of l_count5 = 0 to this if clause
      --TMS 20160324-00153 Start
      if p_tokens1 is not null and l_count3 = 1 and l_count4 = 0 and
         l_count5 = 0 and l_count9 = 0 then
        --TMS 20160324-00153 End
        if l_organization_id != 222 then
          if p_filter = 'ONHAND' then

            insert into XXWC.XXWC_MD_SEARCH_PRODUCT_GTT_TBL
              (score,
               shortdescription,
               partnumber,
               quantityonhand,
               open_sales_orders, --Added by Rakesh Patel on 03/25/2016 for TMS#20151023-00037/20160328-00211
               list_price,
               inventory_item_id,
               -- organization_id,
               primary_uom_code)
              (select /*+ result_cache */
                score(1) + score(5) scr,
                -- type,
                --  name,
                shortdescription,
                partnumber,
                null,
                /*xxwc_ascp_scwb_pkg.get_on_hand(inventory_item_id,
                l_organization_id,
                'G') quantity_onhand,*/
                null,
                /*xxwc_ascp_scwb_pkg.get_open_orders_qty(inventory_item_id,
                l_organization_id) open_orders_qty, --Added by Rakesh Patel on 03/25/2016 for TMS#20151023-00037/20160328-00211*/
                /*xxwc_inv_ais_pkg.shipto_last_price_paid(:search_criteria.site_use_id,
                inventory_item_id) list_price,*/
                null,
                -- cross_reference,
                inventory_item_id,
                -- organization_id,
                -- l_organization_id,
                -- item_type,
                primary_uom_code
                 from XXWC_MD_SEARCH_PRODUCTS_MV_S
                where 0 < (select nvl(xxwc_ascp_scwb_pkg.get_on_hand(inventory_item_id,
                                                                     l_organization_id,
                                                                     'G'),
                                      0)
                             from dual)
                     /*nvl(xxwc_ascp_scwb_pkg.get_on_hand(inventory_item_id,
                                                    l_organization_id,
                                                    'G'),
                     0) > 0*/
                  and (contains(partnumber, l_query_template, 1) > 0 or
                      contains(cross_reference, l_query_template4, 5) > 0));

          elsif p_filter = 'STANDARD' then

            insert into XXWC.XXWC_MD_SEARCH_PRODUCT_GTT_TBL
              (score,
               shortdescription,
               partnumber,
               quantityonhand,
               open_sales_orders, --Added by Rakesh Patel on 03/25/2016 for TMS#20151023-00037/20160328-00211
               list_price,
               inventory_item_id,
               -- organization_id,
               primary_uom_code)
              (select /*+ result_cache */
                score(1) + score(5) scr,
                -- type,
                --  name,
                shortdescription,
                partnumber,
                null,
                /*xxwc_ascp_scwb_pkg.get_on_hand(inventory_item_id,
                l_organization_id,
                'G') quantity_onhand,*/
                null,
                /*xxwc_ascp_scwb_pkg.get_open_orders_qty(inventory_item_id,
                l_organization_id) open_orders_qty, --Added by Rakesh Patel on 03/25/2016 for TMS#20151023-00037/20160328-00211*/
                /*xxwc_inv_ais_pkg.shipto_last_price_paid(:search_criteria.site_use_id,
                inventory_item_id) list_price,*/
                null,
                -- cross_reference,
                inventory_item_id,
                -- organization_id,
                -- l_organization_id,
                -- item_type,
                primary_uom_code
                 from XXWC_MD_SEARCH_PRODUCTS_MV_S
                where ((0 < (select nvl(xxwc_ascp_scwb_pkg.get_on_hand(inventory_item_id,
                                                                       l_organization_id,
                                                                       'G'),
                                        0)
                               from dual) and xxwc_mv_routines_pkg.get_item_category(inventory_item_id,
                                                                                       l_organization_id,
                                                                                       'Sales Velocity',
                                                                                       1) in
                      ('N', 'Z')) or xxwc_mv_routines_pkg.get_item_category(inventory_item_id,
                                                                             l_organization_id,
                                                                             'Sales Velocity',
                                                                             1) not in
                      ('N', 'Z') or (0 < (select nvl(xxwc_ascp_scwb_pkg.get_on_hand(inventory_item_id,
                                                                                     l_organization_id,
                                                                                     'G'),
                                                      0)
                                             from dual) and
                      xxwc_mv_routines_pkg.get_item_category(inventory_item_id,
                                                                             l_organization_id,
                                                                             'Sales Velocity',
                                                                             1) is null and
                      item_type = 'SPECIAL'))
                  and (contains(partnumber, l_query_template, 1) > 0 or
                      contains(cross_reference, l_query_template4, 5) > 0));
          else

            insert into XXWC.XXWC_MD_SEARCH_PRODUCT_GTT_TBL
              (score,
               shortdescription,
               partnumber,
               quantityonhand,
               open_sales_orders, --Added by Rakesh Patel on 03/25/2016 for TMS#20151023-00037/20160328-00211
               list_price,
               inventory_item_id,
               -- organization_id,
               primary_uom_code)
              (select /*+ result_cache */
                score(1) + score(5) scr,
                -- type,
                --  name,
                shortdescription,
                partnumber,
                null,
                /*xxwc_ascp_scwb_pkg.get_on_hand(inventory_item_id,
                l_organization_id,
                'G') quantity_onhand,*/
                null,
                /*xxwc_ascp_scwb_pkg.get_open_orders_qty(inventory_item_id,
                l_organization_id) open_orders_qty, --Added by Rakesh Patel on 03/25/2016 for TMS#20151023-00037/20160328-00211*/
                /*xxwc_inv_ais_pkg.shipto_last_price_paid(:search_criteria.site_use_id,
                inventory_item_id) list_price,*/
                null,
                -- cross_reference,
                inventory_item_id,
                -- organization_id,
                -- l_organization_id,
                -- item_type,
                primary_uom_code
                 from XXWC_MD_SEARCH_PRODUCTS_MV_S
                where (contains(partnumber, l_query_template, 1) > 0 or
                      contains(cross_reference, l_query_template4, 5) > 0));
          end if;
        else
          insert into XXWC.XXWC_MD_SEARCH_PRODUCT_GTT_TBL
            (score,
             shortdescription,
             partnumber,
             quantityonhand,
             open_sales_orders, --Added by Rakesh Patel on 03/25/2016 for TMS#20151023-00037/20160328-00211
             list_price,
             inventory_item_id,
             -- organization_id,
             primary_uom_code)
            (select /*+ result_cache */
              score(1) + score(5) scr,
              -- type,
              --  name,
              shortdescription,
              partnumber,
              null,
              /*xxwc_ascp_scwb_pkg.get_on_hand(inventory_item_id,
              l_organization_id,
              'G') quantity_onhand,*/
              null,
              /*xxwc_ascp_scwb_pkg.get_open_orders_qty(inventory_item_id,
              l_organization_id) open_orders_qty, --Added by Rakesh Patel on 03/25/2016 for TMS#20151023-00037/20160328-00211*/
              /*xxwc_inv_ais_pkg.shipto_last_price_paid(:search_criteria.site_use_id,
              inventory_item_id) list_price,*/
              null,
              -- cross_reference,
              inventory_item_id,
              -- organization_id,
              -- l_organization_id,
              -- item_type,
              primary_uom_code
               from XXWC_MD_SEARCH_PRODUCTS_MV_S
              where (contains(partnumber, l_query_template, 1) > 0 or
                    contains(cross_reference, l_query_template4, 5) > 0));
        end if;
      end if;
      -----This is if the string is mixture of alphabet and digits then look in part number and cross ref
      if p_tokens1 is not null and l_count3 = 0 and l_count = 0 and
         l_count4 = 0 and l_count5 = 0 and l_count9 = 0 then
        if l_organization_id != 222 then
          if p_filter = 'ONHAND' then

            insert into XXWC.XXWC_MD_SEARCH_PRODUCT_GTT_TBL
              (score,
               shortdescription,
               partnumber,
               quantityonhand,
               open_sales_orders, --Added by Rakesh Patel on 03/25/2016 for TMS#20151023-00037/20160328-00211
               list_price,
               inventory_item_id,
               -- organization_id,
               primary_uom_code)
              (select /*+ result_cache */
                score(1) + score(5) scr,
                -- type,
                --  name,
                shortdescription,
                partnumber,
                null,
                /*xxwc_ascp_scwb_pkg.get_on_hand(inventory_item_id,
                l_organization_id,
                'G') quantity_onhand,*/
                null,
                /*xxwc_ascp_scwb_pkg.get_open_orders_qty(inventory_item_id,
                l_organization_id) open_orders_qty, --Added by Rakesh Patel on 03/25/2016 for TMS#20151023-00037/20160328-00211*/
                /*xxwc_inv_ais_pkg.shipto_last_price_paid(:search_criteria.site_use_id,
                inventory_item_id) list_price,*/
                null,
                -- cross_reference,
                inventory_item_id,
                -- organization_id,
                -- l_organization_id,
                -- item_type,
                primary_uom_code
                 from XXWC_MD_SEARCH_PRODUCTS_MV_S
                where 0 < (select nvl(xxwc_ascp_scwb_pkg.get_on_hand(inventory_item_id,
                                                                     l_organization_id,
                                                                     'G'),
                                      0)
                             from dual)

                  and (contains(partnumber, l_query_template, 1) > 0 or
                      contains(cross_reference, l_query_template4, 5) > 0));

          elsif p_filter = 'STANDARD' then

            insert into XXWC.XXWC_MD_SEARCH_PRODUCT_GTT_TBL
              (score,
               shortdescription,
               partnumber,
               quantityonhand,
               open_sales_orders, --Added by Rakesh Patel on 03/25/2016 for TMS#20151023-00037/20160328-00211
               list_price,
               inventory_item_id,
               -- organization_id,
               primary_uom_code)
              (select /*+ result_cache */
                score(1) + score(5) scr,
                -- type,
                --  name,
                shortdescription,
                partnumber,
                null,
                /*xxwc_ascp_scwb_pkg.get_on_hand(inventory_item_id,
                l_organization_id,
                'G') quantity_onhand,*/
                null,
                /*xxwc_ascp_scwb_pkg.get_open_orders_qty(inventory_item_id,
                l_organization_id) open_orders_qty, --Added by Rakesh Patel on 03/25/2016 for TMS#20151023-00037/20160328-00211*/
                /*xxwc_inv_ais_pkg.shipto_last_price_paid(:search_criteria.site_use_id,
                inventory_item_id) list_price,*/
                null,
                -- cross_reference,
                inventory_item_id,
                -- organization_id,
                -- l_organization_id,
                -- item_type,
                primary_uom_code
                 from XXWC_MD_SEARCH_PRODUCTS_MV_S
                where ((0 < (select nvl(xxwc_ascp_scwb_pkg.get_on_hand(inventory_item_id,
                                                                       l_organization_id,
                                                                       'G'),
                                        0)
                               from dual) and xxwc_mv_routines_pkg.get_item_category(inventory_item_id,
                                                                                       l_organization_id,
                                                                                       'Sales Velocity',
                                                                                       1) in
                      ('N', 'Z')) or xxwc_mv_routines_pkg.get_item_category(inventory_item_id,
                                                                             l_organization_id,
                                                                             'Sales Velocity',
                                                                             1) not in
                      ('N', 'Z') or (0 < (select nvl(xxwc_ascp_scwb_pkg.get_on_hand(inventory_item_id,
                                                                                     l_organization_id,
                                                                                     'G'),
                                                      0)
                                             from dual) and
                      xxwc_mv_routines_pkg.get_item_category(inventory_item_id,
                                                                             l_organization_id,
                                                                             'Sales Velocity',
                                                                             1) is null and
                      item_type = 'SPECIAL'))
                  and (contains(partnumber, l_query_template, 1) > 0 or
                      contains(cross_reference, l_query_template4, 5) > 0));
          else

            insert into XXWC.XXWC_MD_SEARCH_PRODUCT_GTT_TBL
              (score,
               shortdescription,
               partnumber,
               quantityonhand,
               open_sales_orders, --Added by Rakesh Patel on 03/25/2016 for TMS#20151023-00037/20160328-00211
               list_price,
               inventory_item_id,
               -- organization_id,
               primary_uom_code)
              (select /*+ result_cache */
                score(1) + score(5) scr,
                -- type,
                --  name,
                shortdescription,
                partnumber,
                null,
                /*xxwc_ascp_scwb_pkg.get_on_hand(inventory_item_id,
                l_organization_id,
                'G') quantity_onhand,*/
                null,
                /*xxwc_ascp_scwb_pkg.get_open_orders_qty(inventory_item_id,
                l_organization_id) open_orders_qty, --Added by Rakesh Patel on 03/25/2016 for TMS#20151023-00037/20160328-00211*/
                /*xxwc_inv_ais_pkg.shipto_last_price_paid(:search_criteria.site_use_id,
                inventory_item_id) list_price,*/
                null,
                -- cross_reference,
                inventory_item_id,
                -- organization_id,
                -- l_organization_id,
                -- item_type,
                primary_uom_code
                 from XXWC_MD_SEARCH_PRODUCTS_MV_S
                where /*organization_id + 0 = l_organization_id
                                                                                                                                                                                                                                                                                                                                                                                                                                                           and*/
                (contains(partnumber, l_query_template, 1) > 0 or
                contains(cross_reference, l_query_template4, 5) > 0));

          end if;
        else
          insert into XXWC.XXWC_MD_SEARCH_PRODUCT_GTT_TBL
            (score,
             shortdescription,
             partnumber,
             quantityonhand,
             open_sales_orders, --Added by Rakesh Patel on 03/25/2016 for TMS#20151023-00037/20160328-00211
             list_price,
             inventory_item_id,
             -- organization_id,
             primary_uom_code)
            (select /*+ result_cache */
              score(1) + score(5) scr,
              -- type,
              --  name,
              shortdescription,
              partnumber,
              null,
              /*xxwc_ascp_scwb_pkg.get_on_hand(inventory_item_id,
              l_organization_id,
              'G') quantity_onhand,*/
              null,
              /*xxwc_ascp_scwb_pkg.get_open_orders_qty(inventory_item_id,
              l_organization_id) open_orders_qty, --Added by Rakesh Patel on 03/25/2016 for TMS#20151023-00037/20160328-00211*/
              /*xxwc_inv_ais_pkg.shipto_last_price_paid(:search_criteria.site_use_id,
              inventory_item_id) list_price,*/
              null,
              -- cross_reference,
              inventory_item_id,
              -- organization_id,
              -- l_organization_id,
              -- item_type,
              primary_uom_code
               from XXWC_MD_SEARCH_PRODUCTS_MV_S
              where /*organization_id + 0 = l_organization_id
                                                                                                                                                                                                                                                                                                                                                                                                                                       and*/
              (contains(partnumber, l_query_template, 1) > 0 or
              contains(cross_reference, l_query_template4, 5) > 0));
        end if;
      end if;
      -- if there are 10 characters and that too numbers UPC
      if p_tokens1 is not null and l_count4 = 1 and l_count9 = 0 then
        if l_organization_id != 222 then
          if p_filter = 'ONHAND' then

            insert into XXWC.XXWC_MD_SEARCH_PRODUCT_GTT_TBL
              (score,
               shortdescription,
               partnumber,
               quantityonhand,
               open_sales_orders, --Added by Rakesh Patel on 03/25/2016 for TMS#20151023-00037/20160328-00211
               list_price,
               inventory_item_id,
               -- organization_id,
               primary_uom_code)
              (select /*+ result_cache */
                score(1) + score(5) scr,
                -- type,
                --  name,
                shortdescription,
                partnumber,
                null,
                /*xxwc_ascp_scwb_pkg.get_on_hand(inventory_item_id,
                l_organization_id,
                'G') quantity_onhand,*/
                null,
                /*xxwc_ascp_scwb_pkg.get_open_orders_qty(inventory_item_id,
                l_organization_id) open_orders_qty, --Added by Rakesh Patel on 03/25/2016 for TMS#20151023-00037/20160328-00211*/
                /*xxwc_inv_ais_pkg.shipto_last_price_paid(:search_criteria.site_use_id,
                inventory_item_id) list_price,*/
                null,
                -- cross_reference,
                inventory_item_id,
                -- organization_id,
                -- l_organization_id,
                -- item_type,
                primary_uom_code
                 from XXWC_MD_SEARCH_PRODUCTS_MV_S
                where 0 < (select nvl(xxwc_ascp_scwb_pkg.get_on_hand(inventory_item_id,
                                                                     l_organization_id,
                                                                     'G'),
                                      0)
                             from dual)

                  and (contains(cross_reference, l_query_template4, 5) > 0)
                   or (contains(partnumber, l_query_template, 1) > 0)
               /*order by partnumber desc, quantity_onhand desc, scr desc*/
               );
          elsif p_filter = 'STANDARD' then

            insert into XXWC.XXWC_MD_SEARCH_PRODUCT_GTT_TBL
              (score,
               shortdescription,
               partnumber,
               quantityonhand,
               open_sales_orders, --Added by Rakesh Patel on 03/25/2016 for TMS#20151023-00037/20160328-00211
               list_price,
               inventory_item_id,
               -- organization_id,
               primary_uom_code)
              (select /*+ result_cache */
                score(1) + score(5) scr,
                -- type,
                --  name,
                shortdescription,
                partnumber,
                null,
                /*xxwc_ascp_scwb_pkg.get_on_hand(inventory_item_id,
                l_organization_id,
                'G') quantity_onhand,*/
                null,
                /*xxwc_ascp_scwb_pkg.get_open_orders_qty(inventory_item_id,
                l_organization_id) open_orders_qty, --Added by Rakesh Patel on 03/25/2016 for TMS#20151023-00037/20160328-00211*/
                /*xxwc_inv_ais_pkg.shipto_last_price_paid(:search_criteria.site_use_id,
                inventory_item_id) list_price,*/
                null,
                -- cross_reference,
                inventory_item_id,
                -- organization_id,
                -- l_organization_id,
                -- item_type,
                primary_uom_code
                 from XXWC_MD_SEARCH_PRODUCTS_MV_S
                where ((0 < (select nvl(xxwc_ascp_scwb_pkg.get_on_hand(inventory_item_id,
                                                                       l_organization_id,
                                                                       'G'),
                                        0)
                               from dual) and xxwc_mv_routines_pkg.get_item_category(inventory_item_id,
                                                                                       l_organization_id,
                                                                                       'Sales Velocity',
                                                                                       1) in
                      ('N', 'Z')) or xxwc_mv_routines_pkg.get_item_category(inventory_item_id,
                                                                             l_organization_id,
                                                                             'Sales Velocity',
                                                                             1) not in
                      ('N', 'Z') or (0 < (select nvl(xxwc_ascp_scwb_pkg.get_on_hand(inventory_item_id,
                                                                                     l_organization_id,
                                                                                     'G'),
                                                      0)
                                             from dual) and
                      xxwc_mv_routines_pkg.get_item_category(inventory_item_id,
                                                                             l_organization_id,
                                                                             'Sales Velocity',
                                                                             1) is null and
                      item_type = 'SPECIAL'))
                  and (contains(cross_reference, l_query_template4, 5) > 0)
                   or (contains(partnumber, l_query_template, 1) > 0)

               );

          else

            insert into XXWC.XXWC_MD_SEARCH_PRODUCT_GTT_TBL
              (score,
               shortdescription,
               partnumber,
               inventory_item_id,
               primary_uom_code)
              (select /*+ result_cache */
                score(1) + score(5) scr,
                shortdescription,
                partnumber,
                inventory_item_id,
                primary_uom_code
                 from XXWC_MD_SEARCH_PRODUCTS_MV_S
                where (contains(cross_reference, l_query_template4, 5) > 0)
                   or (contains(partnumber, l_query_template, 1) > 0)

               );
          end if;
        else
          insert into XXWC.XXWC_MD_SEARCH_PRODUCT_GTT_TBL
            (score,
             shortdescription,
             partnumber,
             inventory_item_id,
             primary_uom_code)
            (select /*+ result_cache */
              score(1) + score(5) scr,
              shortdescription,
              partnumber,
              inventory_item_id,
              primary_uom_code
               from XXWC_MD_SEARCH_PRODUCTS_MV_S
              where (contains(cross_reference, l_query_template4, 5) > 0)
                 or (contains(partnumber, l_query_template, 1) > 0)

             );
        end if;
      end if;
      ----Specials or characters just look in description
      if p_tokens1 is not null and l_count = 1 and l_count3 = 0 and
         l_count9 = 0 then
        if l_organization_id != 222 then
          if p_filter = 'ONHAND' then

            insert into XXWC.XXWC_MD_SEARCH_PRODUCT_GTT_TBL
              (score,
               shortdescription,
               partnumber,
               inventory_item_id,
               primary_uom_code)
              (select /*+ result_cache */
                score(4) scr,
                shortdescription,
                partnumber,
                inventory_item_id,
                primary_uom_code
                 from XXWC_MD_SEARCH_PRODUCTS_MV_S
                where 0 < (select nvl(xxwc_ascp_scwb_pkg.get_on_hand(inventory_item_id,
                                                                     l_organization_id,
                                                                     'G'),
                                      0)
                             from dual)

                  and (contains(shortdescription, l_query_template7, 4) > 0)
               /*order by partnumber desc, quantity_onhand desc, scr desc*/
               );
          elsif p_filter = 'STANDARD' then

            insert into XXWC.XXWC_MD_SEARCH_PRODUCT_GTT_TBL
              (score,
               shortdescription,
               partnumber,
               inventory_item_id,
               primary_uom_code)
              (select /*+ result_cache */
                score(4) scr,
                shortdescription,
                partnumber,
                inventory_item_id,
                primary_uom_code
                 from XXWC_MD_SEARCH_PRODUCTS_MV_S
                where ((0 < (select nvl(xxwc_ascp_scwb_pkg.get_on_hand(inventory_item_id,
                                                                       l_organization_id,
                                                                       'G'),
                                        0)
                               from dual) and xxwc_mv_routines_pkg.get_item_category(inventory_item_id,
                                                                                       l_organization_id,
                                                                                       'Sales Velocity',
                                                                                       1) in
                      ('N', 'Z')) or xxwc_mv_routines_pkg.get_item_category(inventory_item_id,
                                                                             l_organization_id,
                                                                             'Sales Velocity',
                                                                             1) not in
                      ('N', 'Z') or (0 < (select nvl(xxwc_ascp_scwb_pkg.get_on_hand(inventory_item_id,
                                                                                     l_organization_id,
                                                                                     'G'),
                                                      0)
                                             from dual) and
                      xxwc_mv_routines_pkg.get_item_category(inventory_item_id,
                                                                             l_organization_id,
                                                                             'Sales Velocity',
                                                                             1) is null and
                      item_type = 'SPECIAL'))
                  and (contains(shortdescription, l_query_template7, 4) > 0)

               );
          else

            insert into XXWC.XXWC_MD_SEARCH_PRODUCT_GTT_TBL
              (score,
               shortdescription,
               partnumber,
               inventory_item_id,
               primary_uom_code)
              (select /*+ result_cache */
                score(4) scr,
                shortdescription,
                partnumber,
                inventory_item_id,
                primary_uom_code
                 from XXWC_MD_SEARCH_PRODUCTS_MV_S
                where (contains(shortdescription, l_query_template7, 4) > 0)

               );

          end if;
        else
          insert into XXWC.XXWC_MD_SEARCH_PRODUCT_GTT_TBL
            (score,
             shortdescription,
             partnumber,
             inventory_item_id,
             primary_uom_code)
            (select /*+ result_cache */
              score(4) scr,
              shortdescription,
              partnumber,
              inventory_item_id,
              primary_uom_code
               from XXWC_MD_SEARCH_PRODUCTS_MV_S
              where (contains(shortdescription, l_query_template7, 4) > 0));

        end if;
      end if;
      --- all characters then look in description and xref
      if p_tokens1 is not null and l_count5 = 1 and l_count3 = 0 and
         l_count9 = 0 then
        if l_organization_id != 222 then
          if p_filter = 'ONHAND' then

            insert into XXWC.XXWC_MD_SEARCH_PRODUCT_GTT_TBL
              (score,
               shortdescription,
               partnumber,
               inventory_item_id,
               primary_uom_code)
              (select /*+ result_cache */
                score(4) + score(5) scr,
                shortdescription,
                partnumber,
                inventory_item_id,
                primary_uom_code
                 from XXWC_MD_SEARCH_PRODUCTS_MV_S
                where 0 < (select nvl(xxwc_ascp_scwb_pkg.get_on_hand(inventory_item_id,
                                                                     l_organization_id,
                                                                     'G'),
                                      0)
                             from dual)

                  and (contains(shortdescription, l_query_template7, 4) > 0 or
                      contains(cross_reference, l_query_template3, 5) > 0)
               /*order by partnumber desc, quantity_onhand desc, scr desc*/
               );
          elsif p_filter = 'STANDARD' then

            insert into XXWC.XXWC_MD_SEARCH_PRODUCT_GTT_TBL
              (score,
               shortdescription,
               partnumber,
               inventory_item_id,
               primary_uom_code)
              (select /*+ result_cache */
                score(4) + score(5) scr,
                shortdescription,
                partnumber,
                inventory_item_id,
                primary_uom_code
                 from XXWC_MD_SEARCH_PRODUCTS_MV_S
                where ((0 < (select nvl(xxwc_ascp_scwb_pkg.get_on_hand(inventory_item_id,
                                                                       l_organization_id,
                                                                       'G'),
                                        0)
                               from dual) and xxwc_mv_routines_pkg.get_item_category(inventory_item_id,
                                                                                       l_organization_id,
                                                                                       'Sales Velocity',
                                                                                       1) in
                      ('N', 'Z')) or xxwc_mv_routines_pkg.get_item_category(inventory_item_id,
                                                                             l_organization_id,
                                                                             'Sales Velocity',
                                                                             1) not in
                      ('N', 'Z') or (0 < (select nvl(xxwc_ascp_scwb_pkg.get_on_hand(inventory_item_id,
                                                                                     l_organization_id,
                                                                                     'G'),
                                                      0)
                                             from dual) and
                      xxwc_mv_routines_pkg.get_item_category(inventory_item_id,
                                                                             l_organization_id,
                                                                             'Sales Velocity',
                                                                             1) is null and
                      item_type = 'SPECIAL'))
                  and (contains(shortdescription, l_query_template7, 4) > 0 or
                      contains(cross_reference, l_query_template3, 5) > 0)

               );
          else

            insert into XXWC.XXWC_MD_SEARCH_PRODUCT_GTT_TBL
              (score,
               shortdescription,
               partnumber,
               inventory_item_id,
               primary_uom_code)
              (select /*+ result_cache */
                score(4) + score(5) scr,
                shortdescription,
                partnumber,
                inventory_item_id,
                primary_uom_code
                 from XXWC_MD_SEARCH_PRODUCTS_MV_S
                where (contains(shortdescription, l_query_template7, 4) > 0 or
                      contains(cross_reference, l_query_template3, 5) > 0)

               );

          end if;
        else
          insert into XXWC.XXWC_MD_SEARCH_PRODUCT_GTT_TBL
            (score,
             shortdescription,
             partnumber,
             inventory_item_id,
             primary_uom_code)
            (select /*+ result_cache */
              score(4) + score(5) scr,
              shortdescription,
              partnumber,
              inventory_item_id,
              primary_uom_code
               from XXWC_MD_SEARCH_PRODUCTS_MV_S
              where (contains(shortdescription, l_query_template7, 4) > 0 or
                    contains(cross_reference, l_query_template3, 5) > 0)

             );

        end if;
      end if;
      --- all characters then look in description with l_count3 count
      if p_tokens1 is not null and l_count5 = 1 and l_count3 = 1 and
         l_count9 = 0 then
        if l_organization_id != 222 then
          if p_filter = 'ONHAND' then

            insert into XXWC.XXWC_MD_SEARCH_PRODUCT_GTT_TBL
              (score,
               shortdescription,
               partnumber,
               inventory_item_id,
               primary_uom_code)
              (select /*+ result_cache */
                score(4) scr,
                shortdescription,
                partnumber,
                inventory_item_id,
                primary_uom_code
                 from XXWC_MD_SEARCH_PRODUCTS_MV_S
                where 0 < (select nvl(xxwc_ascp_scwb_pkg.get_on_hand(inventory_item_id,
                                                                     l_organization_id,
                                                                     'G'),
                                      0)
                             from dual)

                  and (contains(shortdescription, l_query_template7, 4) > 0)
               );
          elsif p_filter = 'STANDARD' then

            insert into XXWC.XXWC_MD_SEARCH_PRODUCT_GTT_TBL
              (score,
               shortdescription,
               partnumber,
               inventory_item_id,
               primary_uom_code)
              (select /*+ result_cache */
                score(4) scr,
                shortdescription,
                partnumber,
                inventory_item_id,
                primary_uom_code
                 from XXWC_MD_SEARCH_PRODUCTS_MV_S
                where ((0 < (select nvl(xxwc_ascp_scwb_pkg.get_on_hand(inventory_item_id,
                                                                       l_organization_id,
                                                                       'G'),
                                        0)
                               from dual) and xxwc_mv_routines_pkg.get_item_category(inventory_item_id,
                                                                                       l_organization_id,
                                                                                       'Sales Velocity',
                                                                                       1) in
                      ('N', 'Z')) or xxwc_mv_routines_pkg.get_item_category(inventory_item_id,
                                                                             l_organization_id,
                                                                             'Sales Velocity',
                                                                             1) not in
                      ('N', 'Z') or (0 < (select nvl(xxwc_ascp_scwb_pkg.get_on_hand(inventory_item_id,
                                                                                     l_organization_id,
                                                                                     'G'),
                                                      0)
                                             from dual) and
                      xxwc_mv_routines_pkg.get_item_category(inventory_item_id,
                                                                             l_organization_id,
                                                                             'Sales Velocity',
                                                                             1) is null and
                      item_type = 'SPECIAL'))
                  and (contains(shortdescription, l_query_template7, 4) > 0)

               );
          else

            insert into XXWC.XXWC_MD_SEARCH_PRODUCT_GTT_TBL
              (score,
               shortdescription,
               list_price,
               inventory_item_id,
               primary_uom_code)
              (select /*+ result_cache */
                score(4) scr,
                shortdescription,
                partnumber,
                inventory_item_id,
                primary_uom_code
                 from XXWC_MD_SEARCH_PRODUCTS_MV_S
                where (contains(shortdescription, l_query_template7, 4) > 0)

               );

          end if;
        else
          insert into XXWC.XXWC_MD_SEARCH_PRODUCT_GTT_TBL
            (score,
             shortdescription,
             partnumber,
             inventory_item_id,
             primary_uom_code)
            (select /*+ result_cache */
              score(4) scr,
              shortdescription,
              partnumber,
              inventory_item_id,
              primary_uom_code
               from XXWC_MD_SEARCH_PRODUCTS_MV_S
              where (contains(shortdescription, l_query_template7, 4) > 0));

        end if;
      end if;
      IF p_jobsiteid IS NOT NULL THEN
         IF NVL(p_call_pr_api, 'Y') = 'Y' THEN --Rev 2.2  
            line_pricing_data (  p_organization_id => p_branch_id
                               , p_cust_account_id => p_customer_id
                               , p_site_use_id     => p_jobsiteid
                              );
         END IF; --Rev 2.2                 
      END IF;

      for rec in (select /*+ result_cache */
                   /*+ INDEX(MT MTL_SYSTEM_ITEMS_B_U1)*/
                   GT.score                         scr,
                   GT.shortdescription,
                   GT.partnumber,
                   GT.quantityonhand                quantity_onhand,
                   GT.open_sales_orders, --Added by Rakesh Patel on 03/25/2016 for TMS#20151023-00037/20160328-00211
                   GT.list_price,
                   GT.inventory_item_id,
                   GT.selling_price,
                   GT.modifier,
                   GT.modifier_type,
                   p_branch_id,--:search_criteria.organization_id organization_id,
                   GT.primary_uom_code,
                   GT.RESERVABLE,
                   GT.LIST_PRICE_2
                    from XXWC.XXWC_MD_SEARCH_PRODUCT_GTT_TBL GT
                   order by case
                              when l_sorting_type = 'PARTNUMBER' then
                               partnumber
                            end asc,
                            case
                              when l_sorting_type = 'ONHAND' then
                               quantityonhand
                            end desc, --TMS 20161128-00028
                            case
                              when l_sorting_type = 'SCR' then
                               score
                            end desc) loop --TMS 20161128-00028
        l_record_count                      := l_record_count + 1;
      END LOOP;

      if l_record_count = 0 then
        if l_organization_id != 222 then
          if p_filter = 'ONHAND' then
            insert into XXWC.XXWC_MD_SEARCH_PRODUCT_GTT_TBL
              (score,
               shortdescription,
               partnumber,
               inventory_item_id,
               primary_uom_code)
              (select /*+ result_cache */
                score(1) + score(2) + score(3) + score(4) + score(5) scr,
                shortdescription,
                partnumber,
                inventory_item_id,
                primary_uom_code
                 from XXWC_MD_SEARCH_PRODUCTS_MV_S
                where 0 < (select nvl(xxwc_ascp_scwb_pkg.get_on_hand(inventory_item_id,
                                                                     l_organization_id,
                                                                     'G'),
                                      0)
                             from dual)

                  and (contains(partnumber, l_query_template, 1) > 0 or
                      contains(shortdescription, l_query_template7, 4) > 0 or
                      contains(cross_reference, l_query_template3, 5) > 0 or
                      (contains(partnumber, l_trim_template, 2) > 0 and
                      contains(dummy, l_rem_template, 3) > 0)

                      ));
          elsif p_filter = 'STANDARD' then

            insert into XXWC.XXWC_MD_SEARCH_PRODUCT_GTT_TBL
              (score,
               shortdescription,
               partnumber,
               inventory_item_id,
               primary_uom_code)
              (select /*+ result_cache */
                score(1) + score(2) + score(3) + score(4) + score(5) scr,
                shortdescription,
                partnumber,
                inventory_item_id,
                primary_uom_code
                 from XXWC_MD_SEARCH_PRODUCTS_MV_S
                where ((0 < (select nvl(xxwc_ascp_scwb_pkg.get_on_hand(inventory_item_id,
                                                                       l_organization_id,
                                                                       'G'),
                                        0)
                               from dual) and xxwc_mv_routines_pkg.get_item_category(inventory_item_id,
                                                                                       l_organization_id,
                                                                                       'Sales Velocity',
                                                                                       1) in
                      ('N', 'Z')) or xxwc_mv_routines_pkg.get_item_category(inventory_item_id,
                                                                             l_organization_id,
                                                                             'Sales Velocity',
                                                                             1) not in
                      ('N', 'Z') or (0 < (select nvl(xxwc_ascp_scwb_pkg.get_on_hand(inventory_item_id,
                                                                                     l_organization_id,
                                                                                     'G'),
                                                      0)
                                             from dual) and
                      xxwc_mv_routines_pkg.get_item_category(inventory_item_id,
                                                                             l_organization_id,
                                                                             'Sales Velocity',
                                                                             1) is null and
                      item_type = 'SPECIAL'))
                  and (contains(partnumber, l_query_template, 1) > 0 or
                      contains(shortdescription, l_query_template7, 4) > 0 or
                      contains(cross_reference, l_query_template3, 5) > 0 or
                      (contains(partnumber, l_trim_template, 2) > 0 and
                      contains(dummy, l_rem_template, 3) > 0)));
          else

            insert into XXWC.XXWC_MD_SEARCH_PRODUCT_GTT_TBL
              (score,
               shortdescription,
               partnumber,
               inventory_item_id,
               primary_uom_code)
              (select /*+ result_cache */
                score(1) + score(2) + score(3) + score(4) + score(5) scr,
                shortdescription,
                partnumber,
                inventory_item_id,
                primary_uom_code
                 from XXWC_MD_SEARCH_PRODUCTS_MV_S
                where (contains(partnumber, l_query_template, 1) > 0 or
                      contains(shortdescription, l_query_template7, 4) > 0 or
                      contains(cross_reference, l_query_template3, 5) > 0 or
                      (contains(partnumber, l_trim_template, 2) > 0 and
                      contains(dummy, l_rem_template, 3) > 0)));

          end if;
        else
          insert into XXWC.XXWC_MD_SEARCH_PRODUCT_GTT_TBL
            (score,
             shortdescription,
             partnumber,
             inventory_item_id,
             primary_uom_code)
            (select /*+ result_cache */
              score(1) + score(2) + score(3) + score(4) + score(5) scr,
              shortdescription,
              partnumber,
              inventory_item_id,
              primary_uom_code
               from XXWC_MD_SEARCH_PRODUCTS_MV_S
              where (contains(partnumber, l_query_template, 1) > 0 or
                    contains(shortdescription, l_query_template7, 4) > 0 or
                    contains(cross_reference, l_query_template3, 5) > 0 or
                    (contains(partnumber, l_trim_template, 2) > 0 and
                    contains(dummy, l_rem_template, 3) > 0)));
        end if;
        IF p_jobsiteid IS NOT NULL THEN
           IF NVL(p_call_pr_api, 'Y') = 'Y' THEN --Rev 2.2  
              line_pricing_data (  p_organization_id => p_branch_id
                       , p_cust_account_id => p_customer_id
                       , p_site_use_id     => p_jobsiteid
                      );
           END IF; --Rev 2.2          
        END IF;
      end if;
      --first_record;
      --   end if;
    end if;
  end;
END;

--TMS#20170313-00308 Start
PROCEDURE line_pricing_data (   p_cust_account_id NUMBER
                              , p_site_use_id NUMBER
                             )
IS
   p_line_tbl                 qp_preq_grp.line_tbl_type;
   p_qual_tbl                 qp_preq_grp.qual_tbl_type;
   p_line_attr_tbl            qp_preq_grp.line_attr_tbl_type;
   p_line_detail_tbl          qp_preq_grp.line_detail_tbl_type;
   p_line_detail_qual_tbl     qp_preq_grp.line_detail_qual_tbl_type;
   p_line_detail_attr_tbl     qp_preq_grp.line_detail_attr_tbl_type;
   p_related_lines_tbl        qp_preq_grp.related_lines_tbl_type;
   p_control_rec              qp_preq_grp.control_record_type;
   x_line_tbl                 qp_preq_grp.line_tbl_type;
   x_line_qual                qp_preq_grp.qual_tbl_type;
   x_line_attr_tbl            qp_preq_grp.line_attr_tbl_type;
   x_line_detail_tbl          qp_preq_grp.line_detail_tbl_type;
   x_line_detail_qual_tbl     qp_preq_grp.line_detail_qual_tbl_type;
   x_line_detail_attr_tbl     qp_preq_grp.line_detail_attr_tbl_type;
   x_related_lines_tbl        qp_preq_grp.related_lines_tbl_type;
   x_return_status            VARCHAR2 (240);
   x_return_status_text       VARCHAR2 (240);
   qual_rec                   qp_preq_grp.qual_rec_type;
   line_attr_rec              qp_preq_grp.line_attr_rec_type;
   line_rec                   qp_preq_grp.line_rec_type;
   detail_rec                 qp_preq_grp.line_detail_rec_type;
   ldet_rec                   qp_preq_grp.line_detail_rec_type;
   rltd_rec                   qp_preq_grp.related_lines_rec_type;
   l_pricing_contexts_tbl     qp_attr_mapping_pub.contexts_result_tbl_type;
   l_qualifier_contexts_tbl   qp_attr_mapping_pub.contexts_result_tbl_type;
   v_line_tbl_cnt             INTEGER;

   i                          BINARY_INTEGER;
   j                          BINARY_INTEGER;
   k                          BINARY_INTEGER;

   l_version                  VARCHAR2 (240);
   l_file_val                 VARCHAR2 (60);
   l_modifier_name            VARCHAR2 (240);
   l_list_header_id           NUMBER;
   l_list_line_id             NUMBER;
   l_incomp_code              VARCHAR2 (30);
   l_modifier_type            VARCHAR2 (30);
   l_line_modifier_type       VARCHAR2 (30);
   l_item_category            NUMBER;
   l_gm_selling_price         NUMBER;
   l_message_level            NUMBER;
   l_currency_code            VARCHAR2 (240);  
   l_org_id                   NUMBER := fnd_profile.value('ORG_ID');
   l_list_line_type_code      VARCHAR2 (30);   


   CURSOR cur IS
   SELECT inventory_item_id
        , NVL(primary_uom_code,'EA') primary_uom_code
        , organization_id
     FROM xxwc.xxwc_item_avbl_price_tbl
    ORDER BY inventory_item_id
    ;

   l_rec_cntr              NUMBER;
   l_line_attr_tbl_cntr    NUMBER;
   l_qual_tbl_cntr         NUMBER;
   l_inventory_item_id     NUMBER;
BEGIN
   DELETE FROM XXWC_PA_WAREHOUSE_GT; --rev# 2.6  

   SELECT gsob.currency_code
     INTO l_currency_code
     FROM hr_operating_units hou, gl_sets_of_books gsob
    WHERE     hou.organization_id = l_org_id
          AND gsob.set_of_books_id = hou.set_of_books_id;

   l_rec_cntr           := 0;
   l_line_attr_tbl_cntr := 0;
   l_qual_tbl_cntr      := 0;

   FOR rec IN cur LOOP
    
      --rev# 2.6  < Start
      BEGIN
       INSERT INTO xxwc.xxwc_pa_warehouse_gt
         (organization_id)
       values
         (rec.organization_id);
      EXCEPTION
      WHEN OTHERS THEN
         NULL;
      END;
      --rev# 2.6  > End

      l_rec_cntr           := l_rec_cntr + 1;
      l_line_attr_tbl_cntr := l_line_attr_tbl_cntr + 1;
      l_qual_tbl_cntr      := l_qual_tbl_cntr + 1;

      l_gm_selling_price := NULL;
      l_item_category := NULL;
 
      BEGIN
         SELECT mic.category_id
           INTO l_item_category
           FROM mtl_item_categories_v mic
          WHERE     mic.category_set_name = 'Inventory Category'
                AND mic.inventory_item_id = rec.inventory_item_id
                AND mic.organization_id = rec.organization_id
                AND ROWNUM = 1;
      EXCEPTION
         WHEN OTHERS
         THEN
            l_item_category := NULL;
      END;

      --fnd_message.debug('2');
      qp_attr_mapping_pub.build_contexts (
         p_request_type_code           => 'ONT',
         p_pricing_type                => 'L',
         x_price_contexts_result_tbl   => l_pricing_contexts_tbl,
         x_qual_contexts_result_tbl    => l_qualifier_contexts_tbl);

      ---- Control Record
      p_control_rec.pricing_event := 'LINE';                       -- 'BATCH';
      p_control_rec.calculate_flag := 'Y'; --QP_PREQ_GRP.G_SEARCH_N_CALCULATE;
      p_control_rec.simulation_flag := 'Y';
      p_control_rec.rounding_flag := 'Q';
      p_control_rec.manual_discount_flag := 'Y';
      p_control_rec.request_type_code := 'ONT';
      p_control_rec.temp_table_insert_flag := 'Y';

      ---- Line Records ---------
      line_rec.request_type_code := 'ONT';
      line_rec.line_id := -1; -- Order Line Id. This can be any thing for this script
      line_rec.line_index := l_rec_cntr;                        -- Request Line Index
      line_rec.line_type_code := 'LINE';        -- LINE or ORDER(Summary Line)
      line_rec.pricing_effective_date := SYSDATE; -- Pricing as of what date ?
      line_rec.active_date_first := SYSDATE; -- Can be Ordered Date or Ship Date
      line_rec.active_date_second := SYSDATE; -- Can be Ordered Date or Ship Date
      line_rec.active_date_first_type := 'NO TYPE';                -- ORD/SHIP
      line_rec.active_date_second_type := 'NO TYPE';               -- ORD/SHIP
      line_rec.line_quantity := 1; -- Ordered Quantity

      line_rec.line_uom_code := rec.primary_uom_code;
      -- 20160307-00117 < End

      line_rec.currency_code := l_currency_code; 

      line_rec.price_flag := 'Y'; -- Price Flag can have 'Y' , 'N'(No pricing) , 'P'(Phase)
      p_line_tbl (l_rec_cntr) := line_rec;

      ---- Line Attribute Record
      line_attr_rec.line_index := l_rec_cntr;
      line_attr_rec.pricing_context := 'ITEM';
      line_attr_rec.pricing_attribute := 'PRICING_ATTRIBUTE1';
      line_attr_rec.pricing_attr_value_from := TO_CHAR (rec.inventory_item_id); -- INVENTORY ITEM ID
      line_attr_rec.validated_flag := 'N';
      p_line_attr_tbl (l_line_attr_tbl_cntr) := line_attr_rec;

      IF l_item_category IS NOT NULL
      THEN
         l_line_attr_tbl_cntr     := l_line_attr_tbl_cntr + 1;
         line_attr_rec.line_index := l_rec_cntr;
         line_attr_rec.pricing_context := 'ITEM';                           --
         line_attr_rec.pricing_attribute := 'PRICING_ATTRIBUTE2';
         line_attr_rec.pricing_attr_value_from := l_item_category; -- Category ID
         line_attr_rec.validated_flag := 'N';
         p_line_attr_tbl (l_line_attr_tbl_cntr) := line_attr_rec;
      END IF;

      qual_rec.line_index := l_rec_cntr;
      qual_rec.qualifier_context := 'ORDER';
      qual_rec.qualifier_attribute := 'QUALIFIER_ATTRIBUTE18';
      qual_rec.qualifier_attr_value_from := rec.organization_id;--p_organization_id;         -- SHIP_FROM_ORG_ID;
      qual_rec.comparison_operator_code := '=';
      qual_rec.validated_flag := 'Y';
      p_qual_tbl (l_qual_tbl_cntr) := qual_rec;

      IF p_cust_account_id is not null then
         l_qual_tbl_cntr := l_qual_tbl_cntr + 1;
         qual_rec.line_index := l_rec_cntr;
         qual_rec.qualifier_context := 'CUSTOMER';
         qual_rec.qualifier_attribute := 'QUALIFIER_ATTRIBUTE32';
         qual_rec.qualifier_attr_value_from := p_cust_account_id;                         -- CUSTOMER ID;
         qual_rec.comparison_operator_code := '=';
         qual_rec.validated_flag := 'Y';
         p_qual_tbl (l_qual_tbl_cntr) := qual_rec;
      END IF;

      IF p_site_use_id is not null then
         l_qual_tbl_cntr := l_qual_tbl_cntr + 1;
         qual_rec.line_index := l_rec_cntr;
         qual_rec.qualifier_context := 'CUSTOMER';
         qual_rec.qualifier_attribute := 'QUALIFIER_ATTRIBUTE11';
         qual_rec.qualifier_attr_value_from := p_site_use_id;               -- Ship To Site Use ID;
         qual_rec.comparison_operator_code := '=';
         qual_rec.validated_flag := 'Y';
         p_qual_tbl (l_qual_tbl_cntr) := qual_rec;
      END IF;

      l_qual_tbl_cntr := l_qual_tbl_cntr + 1;

      qual_rec.line_index := l_rec_cntr;
      qual_rec.qualifier_context := 'ITEM_NUMBER';
      qual_rec.qualifier_attribute := 'QUALIFIER_ATTRIBUTE31';
      qual_rec.qualifier_attr_value_from := TO_CHAR ( rec.inventory_item_id);       -- ItemId
      qual_rec.comparison_operator_code := 'NOT=';
      qual_rec.validated_flag := 'Y';
      p_qual_tbl (l_qual_tbl_cntr) := qual_rec;

      IF l_item_category IS NOT NULL
      THEN
         l_qual_tbl_cntr := l_qual_tbl_cntr + 1;
         qual_rec.line_index := l_rec_cntr;
         qual_rec.qualifier_context := 'ITEM_CAT';
         qual_rec.qualifier_attribute := 'QUALIFIER_ATTRIBUTE31';
         qual_rec.qualifier_attr_value_from := TO_CHAR (l_item_category); -- CatergoryId
         qual_rec.comparison_operator_code := 'NOT=';
         qual_rec.validated_flag := 'Y';
         p_qual_tbl (l_qual_tbl_cntr) := qual_rec;
      END IF;

            UPDATE xxwc.xxwc_item_avbl_price_tbl
               SET sequence          = l_rec_cntr
             WHERE inventory_item_id = rec.inventory_item_id
               AND organization_id   = rec.organization_id;

   END LOOP;

      qp_preq_pub.price_request (p_line_tbl,
                                 p_qual_tbl,
                                 p_line_attr_tbl,
                                 p_line_detail_tbl,
                                 p_line_detail_qual_tbl,
                                 p_line_detail_attr_tbl,
                                 p_related_lines_tbl,
                                 p_control_rec,
                                 x_line_tbl,
                                 x_line_qual,
                                 x_line_attr_tbl,
                                 x_line_detail_tbl,
                                 x_line_detail_qual_tbl,
                                 x_line_detail_attr_tbl,
                                 x_related_lines_tbl,
                                 x_return_status,
                                 x_return_status_text);

      i := x_line_detail_tbl.FIRST;
      IF i IS NOT NULL THEN

       LOOP

            --fnd_message.debug('7');
            IF x_line_detail_tbl (i).automatic_flag = 'Y'
            THEN
               l_modifier_name := NULL;
               l_list_header_id := NULL;
               l_list_line_id := NULL;
               l_incomp_code := NULL;
               l_modifier_type := NULL;
               l_list_line_type_code := NULL;
               l_line_modifier_type := NULL;

               BEGIN
                  SELECT name, attribute10
                    INTO l_modifier_name, l_modifier_type
                    FROM qp_list_headers_vl
                   WHERE list_header_id = x_line_detail_tbl (i).list_header_id;

                  l_list_header_id := x_line_detail_tbl (i).list_header_id;
                  l_list_line_id := x_line_detail_tbl (i).list_line_id;

                  IF l_list_line_id IS NOT NULL THEN
                     SELECT incompatibility_grp_code,
                            list_line_type_code,
                            attribute5
                       INTO l_incomp_code,
                            l_list_line_type_code,
                            l_line_modifier_type
                       FROM qp_list_lines
                      WHERE list_line_id = l_list_line_id;

                     IF l_modifier_type = 'Contract Pricing' THEN
                        l_modifier_type := NVL (l_line_modifier_type, l_modifier_type);
                     END IF;

                     IF l_modifier_type IS NOT NULL THEN
                        BEGIN
                           SELECT description
                             INTO l_modifier_Type
                             FROM fnd_flex_values_vl
                            WHERE flex_value_set_id = 1015252
                              AND flex_value = l_modifier_Type;
                        EXCEPTION
                           WHEN OTHERS THEN
                              NULL;
                        END;
                     END IF;

                     IF l_list_line_type_code = 'PLL' THEN
                        l_incomp_code := 'PLL';
                     END IF;

                  END IF;
               EXCEPTION
                  WHEN OTHERS THEN
                     l_modifier_name := NULL;
                     l_list_header_id := NULL;
                     l_list_line_id := NULL;
                     l_incomp_code := NULL;
                     l_modifier_type := NULL;
                     l_list_line_type_code := NULL;
                     l_line_modifier_type := NULL;
               END;
                -- Getting new selling price

                  l_gm_selling_price := NULL;
                  j := x_line_tbl.FIRST;
                  IF j IS NOT NULL THEN
                    LOOP
                      IF x_line_tbl (j).LINE_INDEX = x_line_detail_tbl (i).LINE_INDEX THEN
                          l_gm_selling_price := x_line_tbl (j).adjusted_unit_price;
                      END IF;
                      EXIT WHEN l_gm_selling_price IS NOT NULL;
                      j          := x_line_tbl.NEXT (j);
                    END LOOP;
                  END IF;

            IF l_gm_selling_price = 0 THEN
              l_gm_selling_price := NULL;
            END IF;

            UPDATE xxwc.xxwc_item_avbl_price_tbl
               SET selling_price = l_gm_selling_price
                 , modifier      = l_modifier_name --Rev 2.2
                 , modifier_type = l_modifier_type --Rev 2.2
             WHERE sequence  = x_line_detail_tbl(i).line_index;

            END IF;

            EXIT WHEN i = x_line_detail_tbl.LAST;
            i := x_line_detail_tbl.NEXT (i);

         END LOOP;
      END IF;
END;
  /*******************************************************************************
   Procedure Name  :   get_item_avbl_price
   Description     :   This function will be used for getting item price for orgs

   Change History  :
   DATE         NAME               ver          Modification
   --------     -------            ----        ------------------------------------------
   29-Mar-17   neha Saini         2.1           TMS Task ID: 20170330-00170  change procedure to fetch item proice by org  XXWC_OM_SO_CREATE.get_item_avbl_price

   ******************************************************************************/
PROCEDURE get_item_avbl_price (
   p_item_num_tbl       IN     xxwc.xxwc_item_num_tbl,
   p_warhouse_num_tbl   IN     xxwc.xxwc_warhouse_num_tbl,
   p_cust_account_id    IN     NUMBER,
   p_site_use_id        IN     NUMBER,
   o_cur_output            OUT SYS_REFCURSOR)
IS
   l_sec   VARCHAR2 (3000);
BEGIN
   l_sec := 'Start get_item_avbl_price';

   DELETE FROM XXWC.XXWC_ITEM_AVBL_PRICE_TBL;

   FOR i IN p_item_num_tbl.FIRST .. p_item_num_tbl.LAST
   LOOP
      IF p_warhouse_num_tbl.COUNT > 0 --ver2.1 changes starts
      THEN                                                            
         BEGIN
            FOR j IN p_warhouse_num_tbl.FIRST .. p_warhouse_num_tbl.LAST
            LOOP
               BEGIN
                  INSERT INTO xxwc.xxwc_item_avbl_price_tbl (
                                 organization_code,
                                 segment1,
                                 part_description,  --Rev 2.2
                                 inventory_item_id, 
                                 primary_uom_code)
                     SELECT mp.organization_code
                           ,msib.segment1
                           ,msib.description  --Rev 2.2
                           ,msib.inventory_item_id
                           ,msib.primary_uom_code
                       FROM mtl_system_items_b msib, mtl_parameters mp
                      WHERE mp.organization_id = msib.organization_id
                        AND msib.segment1 = p_item_num_tbl (i)
                        AND mp.organization_code = p_warhouse_num_tbl (j);
               EXCEPTION
                  WHEN OTHERS
                  THEN
                     NULL;
               END;
            END LOOP;
         EXCEPTION
            WHEN OTHERS
            THEN
               NULL;
         END;

      ELSE --IF p_warhouse_num_tbl.COUNT > 0 THEN 

         BEGIN
            INSERT INTO xxwc.xxwc_item_avbl_price_tbl (organization_id,
                                                       segment1, 
                                                       inventory_item_id, 
                                                       primary_uom_code)
               SELECT organization_id, segment1, inventory_item_id, primary_uom_code
                 FROM mtl_system_items_b
                WHERE segment1 = p_item_num_tbl (i);
         EXCEPTION
            WHEN OTHERS
            THEN
               NULL; 
         END;
      END IF; --IF p_warhouse_num_tbl.COUNT > 0 THEN
   END LOOP;
   
   IF p_warhouse_num_tbl.COUNT > 0 THEN
       UPDATE xxwc.xxwc_item_avbl_price_tbl aiap
          SET (organization_id, organization_name) =
                 (SELECT organization_id, organization_name
                    FROM apps.org_organization_definitions ood
                   WHERE ood.organization_code = aiap.organization_code);
   ELSE
       DELETE FROM xxwc.xxwc_item_avbl_price_tbl
       WHERE organization_id = 222;
       
       UPDATE xxwc.xxwc_item_avbl_price_tbl aiap
          SET (organization_code, organization_name) =
                 (SELECT organization_code, organization_name
                    FROM apps.org_organization_definitions ood
                   WHERE ood.organization_id = aiap.organization_id);
   END IF;

   /**UPDATE xxwc.xxwc_item_avbl_price_tbl aiap
      SET aiap.long_description = (SELECT msit.long_description
                                     FROM apps.MTL_SYSTEM_ITEMS_TL msit
                                    WHERE 1=1
                                      AND msit.inventory_item_id = aiap.inventory_item_id
                                      AND msit.organization_id = aiap.organization_id
                                      AND rownum = 1
                                   );**/
   
--   UPDATE xxwc.xxwc_item_avbl_price_tbl aiap
--      SET (inventory_item_id, primary_uom_code) =
--             (SELECT inventory_item_id, primary_uom_code
--                FROM apps.xxwc_md_search_products_mv_s xmsp
--               WHERE xmsp.partnumber = aiap.segment1);
--ver2.1 changes ends
   line_pricing_data (p_cust_account_id   => p_cust_account_id,
                      p_site_use_id       => p_site_use_id);

   --Rev 2.2 < Start
   UPDATE XXWC.XXWC_ITEM_AVBL_PRICE_TBL aiap
      SET aiap.gm_percent = ROUND((selling_price - 
                                                 cst_cost_api.get_item_cost(1, 
                                                                            inventory_item_id, 
                                                                            aiap.organization_id, 
                                                                            NULL, 
                                                                            NULL)
                                   )
                                   / selling_price*100
                                 ,2)
    WHERE selling_price <> 0;
   --Rev 2.2 > End
 
   OPEN o_cur_output FOR
     --Rev 2.2 -- Added part_description, modifier, modifier_type, gm_percent to below sql
      'SELECT segment1
             , ( NVL(xxwc_ascp_scwb_pkg.get_on_hand(inventory_item_id, organization_id, ''G''),0) -
                 NVL(xxwc_ascp_scwb_pkg.get_open_orders_qty(inventory_item_id, organization_id),0) ) available_qty
             , selling_price list_price
             , organization_code    warehouse_num
             , organization_name warehouse_name
             , part_description  
             , modifier 
             , modifier_type 
             , gm_percent 
         FROM xxwc.xxwc_item_avbl_price_tbl';

   l_sec := 'End get_item_avbl_price';
EXCEPTION
   WHEN OTHERS
   THEN
      g_sqlcode := SQLCODE;
      g_sqlerrm := SQLERRM;
      g_message := 'Error in generating debug log ';

      xxcus_error_pkg.xxcus_error_main_api (
         p_called_from         => 'XXWC_OM_SO_CREATE.get_item_avbl_price',
         p_calling             => l_sec,
         p_ora_error_msg       => SQLERRM,
         p_error_desc          => g_message || g_sqlcode || g_sqlerrm,
         p_distribution_list   => g_distro_list,
         p_module              => 'XXWC');
      RETURN;
END get_item_avbl_price;
--TMS#20170313-00308 End

--Begin revision 2.3 - C.Baez
FUNCTION GET_DISTRICT_INFO_BY_NTID(p_sr_ntid IN VARCHAR2)return sys_refcursor--return sys_refcursor
    IS v_rec sys_refcursor;
    BEGIN
        IF p_sr_ntid IS NULL THEN
          RETURN NULL;
        ELSE
          OPEN v_rec FOR
            'SELECT
            DISTINCT a.organization_code AS branch_code
            ,home_district AS district
            ,a.organization_name AS warehouse_name
            ,c.address_line_1||'' ''||c.town_or_city||'' ''||nvl(region_2,region_1)||'' ''||c.postal_code as address
            FROM org_organization_definitions a
            ,hr_all_organization_units b
            ,hr_locations c
            ,xxwc_sr_salesreps s
            WHERE 1 = 1
            AND a.operating_unit = 162
            AND a.organization_code NOT IN (''WCC'', ''MST'')
            AND b.organization_id(+) =a.organization_id
            AND c.location_id(+) =b.location_id
            AND s.home_br(+) = a.organization_code
            AND home_district_code = (SELECT HOME_DISTRICT_CODE FROM xxwc_sr_salesreps WHERE ntid = :p_sr_ntid)
            ORDER BY a.organization_code' 
            using p_sr_ntid;
        END IF;
    
    RETURN v_rec;--return cursor

END;--end function GET_DISTRICT_INFO_BY_NTID
--End revision 2.3 - C.Baez

--Begin revision 2.7 - C.Baez
FUNCTION GET_BRANCH_INFO_BY_BR_CODE(p_br_code IN VARCHAR2)return sys_refcursor--return sys_refcursor
    IS v_rec sys_refcursor;
    BEGIN
       IF p_br_code IS NULL THEN
          RETURN NULL;
        ELSE
          OPEN v_rec FOR
            'SELECT
            DISTINCT a.organization_code AS branch_code
            ,home_district AS district
            ,a.organization_name AS warehouse_name
            ,c.address_line_1||'' ''||c.town_or_city||'' ''||nvl(region_2,region_1)||'' ''||c.postal_code as address
            FROM org_organization_definitions a
            ,hr_all_organization_units b
            ,hr_locations c
            ,xxwc_sr_salesreps s
            WHERE 1 = 1
            AND a.operating_unit = 162
            AND a.organization_code NOT IN (''WCC'', ''MST'')
            AND b.organization_id(+) =a.organization_id
            AND c.location_id(+) =b.location_id
            AND s.home_br(+) = a.organization_code
            AND a.organization_code = :p_br_code
            ORDER BY a.organization_code' 
            using p_br_code;
        END IF;
    
    RETURN v_rec;--return cursor

END;--end function GET_BRANCH_INFO_BY_BR_CODE

FUNCTION GET_EEID_NUMBER( NTID_IN VARCHAR2)
  RETURN NUMBER
IS
  L_EEID NUMBER(30);
  
BEGIN

  SELECT EEID 
  INTO L_EEID 
  FROM XXWC.XXWC_SR_SALESREPS
  WHERE NTID = NTID_IN
  AND ROWNUM < 2;

    RETURN L_EEID;
  
  EXCEPTION
    WHEN NO_DATA_FOUND THEN L_EEID := 0;

    RETURN L_EEID;
END;
--End revision 2.7 - C.Baez

--Begin revision 2.9 - C.Baez
/*
Name: GET_CUSTOMER_PROSPECT_INFO
Date: 08/21/2017
Author: Christian Baez, HD Supply
Description: Oracle Function to return sales rep name and phone number for specific customer account name
Parameters: 
    Input: Customer Name
    Output: Customer Name, Primary Address (BILL_TO), Sales Rep Name, Sales Rep Phone Number
*/
FUNCTION GET_CUSTOMER_PROSPECT_INFO ( p_customer_name     IN VARCHAR2
                                   ) return sys_refcursor
IS
   l_sec       VARCHAR2 (3000);
   v_rc sys_refcursor;
BEGIN
   l_sec := 'Start GET_CUSTOMER_PROSPECT_INFO';

   mo_global.set_policy_context('S',162);

   IF p_customer_name IS NOT NULL THEN

        OPEN v_rc for

        'SELECT                         
            PARTY.PARTY_NAME customer_name
            ,LOC.ADDRESS1||'' ''||
                LOC.ADDRESS2||'' ''||
                LOC.CITY||'' ''||
                (CASE
                    WHEN LENGTH (NVL (LOC.state, LOC.province)) > 2 THEN NULL
                        ELSE NVL (LOC.state, LOC.province)
                END)||'' ''||
                LOC.Postal_Code address    
                ,site.ATTRIBUTE1 --revision 3.0
            ,XXWC_OM_SO_CREATE.get_master_acct_sales_rep (CUST_ACCT.cust_account_id ) sales_rep_name --revision 3.0
            ,XXWC_OM_SO_CREATE.get_salesrep_phone (CUST_ACCT.cust_account_id ) sales_rep_phone                       
            ,XXWC_OM_SO_CREATE.get_sales_rep_states_active (:p_customer_name ) sales_rep_states      --revision 3.0
        FROM
            HZ_CUST_ACCT_SITES     ACCT_SITE,
            HZ_PARTY_SITES             PARTY_SITE,
            HZ_LOCATIONS               LOC,
            HZ_CUST_SITE_USES_ALL       SITE,
            HZ_PARTIES           PARTY,
            HZ_CUST_ACCOUNTS      CUST_ACCT
        WHERE 1 = 1 
        AND   SITE.SITE_USE_CODE         = ''BILL_TO''
        AND   SITE.CUST_ACCT_SITE_ID     = ACCT_SITE.CUST_ACCT_SITE_ID
        AND   ACCT_SITE.PARTY_SITE_ID    = PARTY_SITE.PARTY_SITE_ID
        AND   PARTY_SITE.LOCATION_ID     = LOC.LOCATION_ID
        and   acct_site.status=''A''
        AND   ACCT_SITE.CUST_ACCOUNT_ID=CUST_ACCT.CUST_ACCOUNT_ID
        AND   CUST_ACCT.PARTY_ID=PARTY.PARTY_ID
        AND   CUST_ACCT.status=''A''
        AND   UPPER(PARTY.PARTY_NAME)     LIKE UPPER(:p_customer_name)||''%'''
        USING p_customer_name,p_customer_name; --revision 3.0
        RETURN v_rc;
    END IF;

EXCEPTION
   WHEN OTHERS
   THEN
      RETURN NULL;
END GET_CUSTOMER_PROSPECT_INFO;

/*
Name: GET_SALES_REP_PHONE
Date: 08/21/2017
Author: Christian Baez, HD Supply
Description: Oracle Function to return sales rep phone number
Parameters: 
    Input: Customer Account ID
    Output: Sales Rep Phone Number
*/
FUNCTION GET_SALESREP_PHONE (p_cust_account_id   IN NUMBER) RETURN VARCHAR2
     AS
     l_source_phone  VARCHAR2(2000);
     CURSOR cur_salesrep_phone (p_cust_account_id IN NUMBER)
         IS
     SELECT DISTINCT jrre.source_phone
       FROM hz_cust_acct_sites_all     hcas
          , hz_cust_site_uses_all      hcsu
          , jtf_rs_salesreps           jrs
          , jtf_rs_resource_extns      jrre
      WHERE 1 = 1
	    AND hcsu.ATTRIBUTE1          = 'MSTR' --revision 3.0
        AND hcas.cust_account_id     = p_cust_account_id
        AND hcas.cust_acct_site_id   = hcsu.cust_acct_site_id
        AND hcsu.primary_salesrep_id = jrs.salesrep_id
        AND jrs.resource_id          = jrre.resource_id
        AND jrs.org_id               = hcas.org_id
        AND jrs.salesrep_id          != -3 -- No Sales Credit
        AND hcas.status              = 'A'
        AND hcsu.status              = 'A'
        AND jrre.source_phone       IS NOT NULL
        AND SYSDATE BETWEEN jrre.start_Date_active and NVL(jrre.end_date_active, SYSDATE+1)
        ;

       l_sec                     VARCHAR2 (200);
   BEGIN

     l_sec := 'Derive Salesrep Phones';
     FOR rec IN cur_salesrep_phone(p_cust_account_id) LOOP
       IF l_source_phone IS NULL THEN
          l_source_phone := rec.source_phone;
       ELSE
          l_source_phone := rec.source_phone||','||l_source_phone;
       END IF;
     END LOOP;

     RETURN l_source_phone;

   EXCEPTION
   WHEN OTHERS THEN
         xxcus_error_pkg.xxcus_error_main_api (
            p_called_from         => 'XXWC_OM_SO_CREATE.GET_SALES_REP_PHONE',
            p_calling             => l_sec,
            p_request_id          => fnd_global.conc_request_id,
            p_ora_error_msg       => SUBSTR (SQLERRM, 2000),
            p_error_desc          => 'Error in deriving Salesrep phones ',
            p_distribution_list   => 'WC-ITDEVALERTS-U1@HDSupply.com',
            p_module              => 'XXWC');

     RETURN NULL;
   END get_salesrep_phone;
   
--End revision 2.9 - C.Baez
--Start revision 3.0 - C.Baez
   /*
  Name: GET_SALES_REP_STATES_ACTIVE
  Date: 08/21/2017
  Author: Christian Baez, HD Supply
  Description: Oracle Function to return sales rep states that active
  Parameters: 
      Input: Customer Account ID
      Output: Sales Rep State
  */
  
  FUNCTION GET_SALES_REP_STATES_ACTIVE (p_customer_name IN VARCHAR2) RETURN VARCHAR2
     AS
     l_state  VARCHAR2(2000);
     CURSOR cur_salesrep_state (p_customer_name IN VARCHAR2)
         IS
     SELECT DISTINCT (CASE
                    WHEN LENGTH (NVL (LOC.state, LOC.province)) > 2 THEN NULL
                        ELSE NVL (LOC.state, LOC.province)
                    END) Site_State
       FROM apps.HZ_CUST_ACCT_SITES_ALL     ACCT_SITE,
            apps.HZ_PARTY_SITES             PARTY_SITE,
            apps.HZ_LOCATIONS               LOC,
            apps.HZ_CUST_SITE_USES_ALL       SITE,
            apps.HZ_PARTIES           PARTY,
            apps.HZ_CUST_ACCOUNTS      CUST_ACCT
      WHERE 1 = 1
        AND   SITE.SITE_USE_CODE         = 'BILL_TO'
        AND   SITE.CUST_ACCT_SITE_ID     = ACCT_SITE.CUST_ACCT_SITE_ID
        AND   ACCT_SITE.PARTY_SITE_ID    = PARTY_SITE.PARTY_SITE_ID
        AND   PARTY_SITE.LOCATION_ID     = LOC.LOCATION_ID
        AND   acct_site.status='A'
        AND   ACCT_SITE.CUST_ACCOUNT_ID=CUST_ACCT.CUST_ACCOUNT_ID
        AND   CUST_ACCT.PARTY_ID=PARTY.PARTY_ID
        AND   CUST_ACCT.status='A'
        AND   UPPER(PARTY.PARTY_NAME)     LIKE UPPER(p_customer_name)||'%'
        ;

       l_sec                     VARCHAR2 (200);

   BEGIN

     l_sec := 'Derive Salesrep States';
     FOR rec IN cur_salesrep_state(p_customer_name) LOOP
       IF l_state IS NULL THEN
          l_state := rec.site_state;
       ELSE
          l_state := rec.site_state||';'||l_state;
       END IF;
     END LOOP;

     RETURN l_state;

   EXCEPTION
   WHEN OTHERS THEN
         xxcus_error_pkg.xxcus_error_main_api (
            p_called_from         => 'XXWC_OM_SO_CREATE.GET_SALES_REP_STATES_ACTIVE',
            p_calling             => l_sec,
            p_request_id          => fnd_global.conc_request_id,
            p_ora_error_msg       => SUBSTR (SQLERRM, 2000),
            p_error_desc          => 'Error in deriving Salesrep states ',
            p_distribution_list   => 'WC-ITDEVALERTS-U1@HDSupply.com',
            p_module              => 'XXWC');

     RETURN NULL;
   END GET_SALES_REP_STATES_ACTIVE;
   

    /****************************************************************************************************************
    FUNCTION Name: GET_MASTER_ACCT_SALES_REP

      PURPOSE:   To derive Master Acount Level Salesrep Names associated to P_PARTY_ID (Customer)

      REVISIONS:
      Ver        Date        Author                     Description
      ---------  ----------  ---------------         -------------------------
      1.0        08/24/2017  Christian Baez           Created function to retrieve master acct salesrep
   ****************************************************************************************************************/
   FUNCTION get_master_acct_sales_rep (p_cust_account_id   IN NUMBER) RETURN VARCHAR2
     AS
     l_sales_rep_name  VARCHAR2(32767);

     CURSOR cur_salesrep (p_customer_id IN NUMBER)
         IS
     SELECT DISTINCT REPLACE(jrre.source_name, ',', '')  sales_rep_name
       FROM apps.hz_cust_acct_sites_all     hcas
          , apps.hz_cust_site_uses_all      hcsu
          , apps.jtf_rs_salesreps           jrs
          , apps.jtf_rs_resource_extns      jrre
      WHERE 1 = 1
        AND hcsu.ATTRIBUTE1          = 'MSTR'
        AND hcas.cust_account_id     = p_customer_id
        AND hcas.cust_acct_site_id   = hcsu.cust_acct_site_id
        AND hcsu.primary_salesrep_id = jrs.salesrep_id
        AND hcas.status              = 'A'
        AND hcsu.status              = 'A'
        AND jrs.resource_id          = jrre.resource_id
        AND jrs.org_id               = hcas.org_id
        AND jrs.salesrep_id          != -3 -- No Sales Credit
        AND SYSDATE BETWEEN jrre.start_Date_active and NVL(jrre.end_date_active, SYSDATE+1)
        ;
       l_sec                     VARCHAR2 (200);
   BEGIN

     l_sec := 'Derive Master Acct Salesrep Names';

     FOR rec IN cur_salesrep(p_cust_account_id) LOOP
       IF l_sales_rep_name IS NULL THEN
          l_sales_rep_name := rec.sales_rep_name;
       ELSE
           IF rec.sales_rep_name IS NOT NULL THEN
              l_sales_rep_name := rec.sales_rep_name||' ; '||l_sales_rep_name;
           END IF;
       END IF;
     END LOOP;
     RETURN l_sales_rep_name;

   EXCEPTION
   WHEN OTHERS THEN
         xxcus_error_pkg.xxcus_error_main_api (
            p_called_from         => 'XXWC_OM_SO_CREATE.GET_MASTER_ACCT_SALES_REP',
            p_calling             => l_sec,
            p_request_id          => fnd_global.conc_request_id,
            p_ora_error_msg       => SUBSTR (SQLERRM, 2000),
            p_error_desc          => 'Error in deriving Master Account Salesrep Names ',
            p_distribution_list   => g_dflt_email,
            p_module              => 'XXWC');

     RETURN NULL;
   END get_master_acct_sales_rep;
--End revision 3.0 - C.Baez

END XXWC_OM_SO_CREATE;
/
