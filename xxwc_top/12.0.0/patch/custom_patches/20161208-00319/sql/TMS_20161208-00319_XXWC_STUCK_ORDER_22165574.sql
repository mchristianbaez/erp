/*************************************************************************
  $Header 20161208-00319_XXWC_STUCK_ORDER_22165574.sql $
  Module Name: 20161208-00319  Data Fix script for 20161208-00319

  PURPOSE: Data fix script for 20161208-00319--No permanent fix in process.

  REVISIONS:
  Ver        Date         Author                Description
  ---------  -----------  ------------------    --------------------------
  1.0        17-JAN-2017  Ashwin Sridhar        TMS#20161208-00319 

**************************************************************************/ 
SET SERVEROUTPUT ON SIZE 1000000;
DECLARE
BEGIN
   
  DBMS_OUTPUT.PUT_LINE('TMS: 20161208-00319 , Before Update');

  UPDATE apps.oe_order_lines_all
  SET    flow_status_code ='CANCELLED'
  ,      cancelled_flag   ='Y'
  WHERE  line_id          =83826265 
  AND    header_id        =51349965;

  DBMS_OUTPUT.PUT_LINE(
         'TMS: 20161208-00319  Sales order line updated (Expected:1): '
      || SQL%ROWCOUNT);

  COMMIT;

  DBMS_OUTPUT.PUT_LINE('TMS: 20161208-00319 , End Update');
   
EXCEPTION
WHEN others THEN
     
  ROLLBACK;
     
  DBMS_OUTPUT.put_line ('TMS: 20161208-00319 , Errors : ' || SQLERRM);

END;
/