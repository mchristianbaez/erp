/*******************************************************************************************************
  -- Table Name XXWC_AHH_LPP_LOG_GTTs
  -- ***************************************************************************************************
  --
  -- PURPOSE: GTT to store errorneous record
  -- HISTORY
  -- ===================================================================================================
  -- ===================================================================================================
  -- VERSION DATE          AUTHOR(S)       DESCRIPTION
  -- ------- -----------   --------------- -------------------------------------------------------------
  -- 1.0     21-Sep-2018   Niraj K Ranjan   TMS#20180926-00003   AHH LLP price upload
********************************************************************************************************/
CREATE GLOBAL TEMPORARY TABLE XXWC.XXWC_AHH_LPP_LOG_GTT (
  SXE_CUSTOMER_NUMBER                  VARCHAR2(240 BYTE),
  SXE_SITE_NUMBER                      VARCHAR2(240 BYTE), 
  SXE_PART_NUMBER                      VARCHAR2(240 BYTE), 
  SXE_LPP                              VARCHAR2(240 BYTE),
  ERROR_CODE                           NUMBER,
  ERROR_MESSAGE                        VARCHAR2(2000 BYTE)
)
ON COMMIT PRESERVE ROWS;
