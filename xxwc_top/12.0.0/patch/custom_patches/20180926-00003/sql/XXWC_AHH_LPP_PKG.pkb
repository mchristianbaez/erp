CREATE OR REPLACE PACKAGE BODY APPS.XXWC_AHH_LPP_PKG
IS
   /***********************************************************************************************************        $Header XXWC_AHH_LPP_PKG $
        Module Name: XXWC_AHH_LPP_PKG.pkb

        PURPOSE:   AHH LPP Conversion

        REVISIONS:
        Ver        Date        Author           Description
        ---------  ----------  ---------------  ------------------------------------------------------------------------------------------------
        1.0        09/21/2018  Niraj K Ranjan    TMS#20180926-00003   AHH LLP price upload
   ****************************************************************************************************************/


   g_err_callfrom      VARCHAR2 (1000) := 'XXWC_AHH_LPP_PKG';
   g_module            VARCHAR2 (100) := 'XXWC';
   g_distro_list       VARCHAR2 (80) := 'wc-itdevalerts-u1@hdsupply.com';
   
   /***********************************************************************************************************        $Header XXWC_AHH_LPP_PKG $
        Procedure Name: LOG_RECORD_STATS

        PURPOSE:   To generate output file with record statistics

        REVISIONS:
        Ver        Date        Author           Description
        ---------  ----------  ---------------  ------------------------------------------------------------------------------------------------
        1.0        09/26/2018  Niraj K Ranjan    TMS#20180926-00003   AHH LLP price upload
   ****************************************************************************************************************/
   PROCEDURE log_record_stats(p_request_id NUMBER)
   IS
      l_total_records NUMBER;
      l_success_records NUMBER;
      l_failure_records NUMBER;
      l_serial_num      NUMBER;
      l_sxe_lpp         VARCHAR2(240);
      
      CURSOR cr_error
      IS
         SELECT SXE_CUSTOMER_NUMBER,
                SXE_SITE_NUMBER    , 
                SXE_PART_NUMBER    , 
                SXE_LPP            ,
                ERROR_MESSAGE
         FROM XXWC.XXWC_AHH_LPP_LOG_GTT
         WHERE 1=1
         ORDER BY ERROR_CODE ASC;
   BEGIN
      SELECT COUNT(1) INTO l_total_records
      FROM XXWC.XXWC_AHH_LPP_STG_EXT_TBL;
      
      SELECT COUNT(1) INTO l_success_records
      FROM XXWC_AHH_OE_AIS_LPP_TBL
      WHERE request_id = p_request_id;
      
      SELECT COUNT(1) INTO l_failure_records
      FROM XXWC.XXWC_AHH_LPP_LOG_GTT;
      
      fnd_file.put_line(fnd_file.OUTPUT, 'Total File Records   :'||l_total_records);
      fnd_file.put_line(fnd_file.OUTPUT, 'Total Success Records:'||l_success_records);
      fnd_file.put_line(fnd_file.OUTPUT, 'Total Failure Records:'||l_failure_records);
      fnd_file.put_line(fnd_file.OUTPUT, '------------------------------------------------------------------------');
      fnd_file.put_line(fnd_file.OUTPUT, 'Following are the list of Failure Records');
      fnd_file.put_line(fnd_file.OUTPUT, '------------------------------------------------------------------------');
      fnd_file.put_line(fnd_file.OUTPUT, 'SL_NO|SXE_CUSTOMER_NUMBER|SXE_SITE_NUMBER|SXE_PART_NUMBER|SXE_LPP|ERROR_REASON|');
      l_serial_num := 1;
      FOR rec_error IN cr_error
      LOOP
         l_sxe_lpp := trim(rtrim(REGEXP_REPLACE(rec_error.SXE_LPP, '[$,`^-]', ''),chr(13)));
         fnd_file.put_line(fnd_file.OUTPUT, l_serial_num||'|'||
                                            rec_error.SXE_CUSTOMER_NUMBER||'|'||
                                            rec_error.SXE_SITE_NUMBER||'|'||
                                            rec_error.SXE_PART_NUMBER||'|'||
                                            l_sxe_lpp||'|'||
                                            rec_error.ERROR_MESSAGE||'|');
         l_serial_num := l_serial_num + 1;
      END LOOP;
      
   EXCEPTION
      WHEN OTHERS THEN
         fnd_file.put_line (fnd_file.LOG, 'Error inside log_record_stats=>'||sqlerrm);
   END;
   /***********************************************************************************************************        $Header XXWC_AHH_LPP_PKG $
        Procedure Name: LOG_ERROR_MESSAGE

        PURPOSE:   To insert error message into GTT table to display all at a time on output file

        REVISIONS:
        Ver        Date        Author           Description
        ---------  ----------  ---------------  ------------------------------------------------------------------------------------------------
        1.0        09/26/2018  Niraj K Ranjan    TMS#20180926-00003   AHH LLP price upload
   ****************************************************************************************************************/
   PROCEDURE log_error_Message (p_ahh_cust_num      IN VARCHAR2, 
                                p_ahh_site_num      IN VARCHAR2,
                                p_ahh_part_number   IN VARCHAR2,
                                p_ahh_lpp           IN VARCHAR2,
                                p_error_code        IN NUMBER,
                                p_error_message     IN VARCHAR2)
   IS
      PRAGMA AUTONOMOUS_TRANSACTION; 
   BEGIN
      INSERT INTO XXWC.XXWC_AHH_LPP_LOG_GTT (
                                              SXE_CUSTOMER_NUMBER,
                                              SXE_SITE_NUMBER    , 
                                              SXE_PART_NUMBER    , 
                                              SXE_LPP            ,
                                              ERROR_CODE         ,
                                              ERROR_MESSAGE                                              
                                            )
                                     VALUES(
                                            p_ahh_cust_num,
                                            p_ahh_site_num,
                                            p_ahh_part_number,
                                            p_ahh_lpp,
                                            p_error_code,
                                            p_error_message
                                           );
      COMMIT;
   EXCEPTION
      WHEN OTHERS THEN
         rollback;
         fnd_file.put_line (fnd_file.LOG, 'Error inside log_error_Message=>'||sqlerrm);
   END;
   /***********************************************************************************************************        $Header XXWC_AHH_LPP_PKG $
        Procedure Name: GET_ORCL_CUSTOMER_SITE_INFO

        PURPOSE:   To get customer and its site info

        REVISIONS:
        Ver        Date        Author           Description
        ---------  ----------  ---------------  ------------------------------------------------------------------------------------------------
        1.0        09/26/2018  Niraj K Ranjan    TMS#20180926-00003   AHH LLP price upload
   ****************************************************************************************************************/
   PROCEDURE get_orcl_customer_site_info (p_ahh_cust_num      IN VARCHAR2, 
                                          p_ahh_site_num      IN VARCHAR2,
                                          p_ora_cust_acct_id  OUT NUMBER,
                                          p_ora_cust_acct_num OUT VARCHAR2,
                                          p_ora_cust_site_id  OUT NUMBER,
                                          p_ora_cust_site_num OUT VARCHAR2,
                                          p_err_msg           OUT VARCHAR2)
   IS
       l_cust_acct_id number;
       l_cust_acct_num varchar2(50);
       l_cust_site_use_id number;
       l_cust_site_number VARCHAR2(30);
   BEGIN
       BEGIN
          SELECT hca.cust_account_id,hca.account_number
            INTO l_cust_acct_id,l_cust_acct_num
            FROM XXWC.XXWC_AHH_AR_CUST_CROSS_OVER_T xaac,
                 hz_cust_accounts hca
           WHERE     xaac.CUST_NUM =
                        TRIM (TO_CHAR (p_ahh_cust_num))
             AND TRIM (
                        REPLACE (
                           REPLACE (xaac.oracle_cust_num,
                                    CHR (13),
                                    ''),
                           CHR (10),
                           '')) = hca.account_number
                 -- AND hca.status = 'A'
                 AND ROWNUM = 1;
       EXCEPTION
          WHEN OTHERS
          THEN
             BEGIN
                SELECT hca.cust_account_id,hca.account_number
                  INTO l_cust_acct_id,l_cust_acct_num
                  FROM hz_cust_accounts hca,
                       HZ_CUST_ACCT_SITES_ALL hcas
                 WHERE     hca.cust_account_id = hcas.cust_account_id
                       AND NVL (hcas.attribute17, '123') =
                              TRIM (
                                 TO_CHAR (p_ahh_cust_num))
                       -- AND hca.status = 'A'
                       AND hcas.org_id = 162
                       -- AND hca.attribute4 = 'AHH' -- ?????
                       AND hcas.bill_to_flag = 'P'
                       AND ROWNUM = 1;
             EXCEPTION
                 WHEN NO_DATA_FOUND
                 THEN
                    BEGIN
                        SELECT hca.CUST_ACCOUNT_ID,hca.account_number
                          INTO l_cust_acct_id,l_cust_acct_num
                          FROM hz_cust_accounts hca,APPS.HZ_CUST_ACCT_SITES_ALL hcas
                         WHERE NVL (hcas.ATTRIBUTE17, '123') =
                                  TRIM (
                                        TO_CHAR (
                                           upper(p_ahh_cust_num))
                                     || '-'
                                     || TO_CHAR (
                                           upper(p_ahh_site_num)))
                           AND hca.cust_account_id = hcas.cust_account_id
                           -- AND hca.status = 'A'
                           AND ROWNUM = 1;
                    EXCEPTION
                       WHEN NO_DATA_FOUND
                       THEN
                          BEGIN
                             SELECT hca.cust_account_id,hca.account_number
                               INTO l_cust_acct_id,l_cust_acct_num
                               from APPS.HZ_CUST_ACCOUNTS HCA,
                                    APPS.HZ_CUST_ACCT_SITES_ALL hcas
                               where     HCA.CUST_ACCOUNT_ID = HCAS.CUST_ACCOUNT_ID
                               AND NVL (hcas.attribute17, '123') LIKE p_ahh_cust_num||'-%'                             
                               -- AND hca.status = 'A'
                               AND hcas.org_id = 162
                               -- and HCA.ATTRIBUTE4 = 'AHH' -- ?????
                                  and rownum = 1;
                           EXCEPTION
                              WHEN OTHERS
                              THEN
                                 p_err_msg := p_err_msg|| 'Customer not available in Oracle1';
                           END;
                       WHEN OTHERS
                       THEN
                           p_err_msg :=
                                 p_err_msg
                              || 'Customer not available in Oracle2';
                    END;
                 WHEN OTHERS
                 THEN
                     p_err_msg :=
                           p_err_msg
                        || 'Customer not available in Oracle3';
             END;
       END;

       IF l_cust_acct_id IS NOT NULL
       THEN
          p_ora_cust_acct_id  := l_cust_acct_id;
          p_ora_cust_acct_num := l_cust_acct_num;
          ----------------------------------------------------------------------------------
          -- Derive Ship-To Address using Prism PartySiteNumber - Validate Customer Site
          ----------------------------------------------------------------------------------
          BEGIN
             SELECT /*hcas.cust_acct_site_id,
                    hcas.cust_account_id,
                    hcsu.bill_to_site_use_id,
                    hcas.party_site_id*/
                    hcsu.site_use_id,
                    hps.party_site_number
               INTO /*l_ship_to_address_id,
                    l_ship_to_cust_id,
                    l_bill_to_site_use_id,
                    l_party_site_id*/
                    l_cust_site_use_id,
                    l_cust_site_number
               FROM hz_cust_acct_sites_all hcas,
                    hz_cust_site_uses_all hcsu,
                    hz_party_sites hps
              WHERE     1 = 1
                    -- AND hcas.status = 'A'
                    -- AND hcsu.status = 'A'
                    -- AND hps.status = 'A'
                    AND hcas.attribute17 =
                           TRIM (
                                 TO_CHAR (upper(p_ahh_cust_num))
                              || '-'
                              || TO_CHAR (upper(p_ahh_site_num)))
                    AND hps.party_site_id = hcas.party_site_id
                    AND hcas.cust_acct_site_id =
                           hcsu.cust_acct_site_id
                    AND hcsu.site_use_code = 'SHIP_TO'
                    AND hcas.org_id = 162
                    AND ROWNUM = 1;
          EXCEPTION
             WHEN OTHERS THEN
               p_err_msg :=
                           p_err_msg
                        || 'Customer site not available in Oracle';
          END;
          p_ora_cust_site_id  := l_cust_site_use_id;
          p_ora_cust_site_num := l_cust_site_number;
       END IF;
   EXCEPTION
      WHEN OTHERS THEN
         p_err_msg := SUBSTR(SQLERRM,1,2000);
   END;
   /***********************************************************************************************************        $Header XXWC_AHH_LPP_PKG $
        Procedure Name: MAIN

        PURPOSE:   Main program being called from concurrent program

        REVISIONS:
        Ver        Date        Author           Description
        ---------  ----------  ---------------  ------------------------------------------------------------------------------------------------
        1.0        09/26/2018  Niraj K Ranjan    TMS#20180926-00003   AHH LLP price upload
   ****************************************************************************************************************/
   PROCEDURE main (x_errbuf OUT VARCHAR2, x_retcode OUT VARCHAR2 )
   IS
      l_inventory_item_id NUMBER;
      l_user_id           NUMBER := APPS.FND_PROFILE.VALUE('USER_ID');
      l_orcl_part_nbr     VARCHAR2(40) ;
      l_orcl_cust_acct_id      NUMBER;
      l_orcl_customer_nbr VARCHAR2(50) ;
      l_orcl_cust_acct_site_id NUMBER;
      l_orcl_customer_site_nbr   VARCHAR2(50) ;
      l_ship_to_org_id    number;    
      l_cust_account_id   number;
      l_last_price_paid   NUMBER;
      l_rec_count         NUMBER;
      l_existing_pp       NUMBER;
      l_ignore_record     VARCHAR2(1);
      l_sec               VARCHAR2 (32767); 
      l_error_code        NUMBER;
      l_error_msg         VARCHAR2 (4000);
      l_val_exception     EXCEPTION;
      l_request_id        NUMBER := fnd_global.conc_request_id;
	  l_prev_request_id   NUMBER;
      
      CURSOR CUR_MAIN
      IS
        SELECT  SXE_CUSTOMER_NUMBER   
               ,ORACLE_CUSTOMER_NUMBER
               ,SXE_SITE_NUMBER       
               ,ORACLE_SITE_NUMBER    
               ,SXE_PART_NUMBER       
               ,ORACLE_PART_NUMBER    
               ,SXE_LPP
          FROM xxwc.XXWC_AHH_LPP_STG_EXT_TBL XALS; 
    
   BEGIN
      l_sec := 'Start Main';
      fnd_file.put_line (fnd_file.LOG, l_sec);

      l_sec := 'Start Loop';
      fnd_file.put_line (fnd_file.LOG, l_sec);
      FOR rec_main IN CUR_MAIN
      LOOP
         BEGIN
		    l_prev_request_id := NULL;
            l_inventory_item_id := null;
            l_orcl_part_nbr := null;
            l_rec_count := NULL;
            l_existing_pp := NULL;
            l_error_code := NULL;
            l_ignore_record := 'N';
            l_sec := 'manupulate l_last_price_paid FOR SXE_PART_NUMBER:'||rec_main.SXE_PART_NUMBER||
			' SXE_LPP:'||rec_main.SXE_LPP||' SXE_CUSTOMER_NUMBER:'||rec_main.SXE_CUSTOMER_NUMBER||' SXE_SITE_NUMBER:'||rec_main.SXE_SITE_NUMBER;    
            l_last_price_paid :=  to_number(trim(rtrim(REGEXP_REPLACE(rec_main.SXE_LPP, '[$,`^-]', ''),chr(13)))) ;
            --fetch oracle inventory item id   
            BEGIN
			   l_sec := 'Fetch inventory_item_id'; 
               select msi.inventory_item_id,msi.segment1
               into l_inventory_item_id,l_orcl_part_nbr
               from xxwc.XXWC_ITEM_MASTER_REF xim,
                    mtl_system_items_b msi
               where icsp_prod = upper(rec_main.sxe_part_number)
               and  xim.item = msi.segment1
               and msi.organization_id = 222
			   AND ROWNUM = 1;
            EXCEPTION
			   WHEN NO_DATA_FOUND THEN
			      l_inventory_item_id := null;
                  l_orcl_part_nbr := null;
                  l_error_code := '1';
                  l_error_msg := 'Item is not found';
                  raise l_val_exception;
               when others then
                  l_inventory_item_id := null;
                  l_orcl_part_nbr := null;
                  l_error_code := '2';
                  l_error_msg := SUBSTR(l_sec||'=>'||SQLERRM,1,2000);
                  raise l_val_exception;
            END;
            
            --fetch oracle customer and site info
            l_sec := 'Call get_orcl_customer_site_info';    
            get_orcl_customer_site_info (p_ahh_cust_num      =>  rec_main.SXE_CUSTOMER_NUMBER , 
                                   p_ahh_site_num     => rec_main.SXE_SITE_NUMBER ,
                                   p_ora_cust_acct_id => l_orcl_cust_acct_id ,
                                   p_ora_cust_acct_num => l_orcl_customer_nbr,
                                   p_ora_cust_site_id => l_orcl_cust_acct_site_id ,
                                   p_ora_cust_site_num => l_orcl_customer_site_nbr,
                                   p_err_msg          => l_error_msg
                                 );
            IF rec_main.SXE_CUSTOMER_NUMBER IS NOT NULL AND l_orcl_cust_acct_id IS NULL THEN
                l_error_code := '3';
                l_error_msg := l_error_msg||' - Customer not found';
                raise l_val_exception;
            END IF;
            IF rec_main.SXE_SITE_NUMBER IS NOT NULL AND l_orcl_cust_acct_site_id IS NULL THEN
                l_error_code := '4';
                l_error_msg := l_error_msg||' - Customer site not found';
                raise l_val_exception;
            END IF;
            
            BEGIN    
                IF  rec_main.SXE_SITE_NUMBER IS NOT NULL THEN    
                     l_sec := 'Fetch1 LAST_PRICE_PAID';
                     SELECT LAST_PRICE_PAID,REQUEST_ID 
					 INTO l_existing_pp,l_prev_request_id
                     FROM XXWC_AHH_OE_AIS_LPP_TBL 
                     WHERE AHH_PART_NBR = rec_main.SXE_PART_NUMBER
                     AND   AHH_CUSTOMER_NBR = rec_main.SXE_CUSTOMER_NUMBER
                     AND   AHH_SITE_NBR  = rec_main.SXE_SITE_NUMBER
                     AND  END_DATE_ACTIVE IS NULL
                     AND ROWNUM = 1;
					 
					 IF l_existing_pp IS NOT NULL AND l_existing_pp <> l_last_price_paid 
                     THEN
					    l_sec := 'update1 XXWC_AHH_OE_AIS_LPP_TBL';
                        UPDATE XXWC_AHH_OE_AIS_LPP_TBL
                        SET END_DATE_ACTIVE = SYSDATE,
                            LAST_UPDATE_DATE = SYSDATE,
                            LAST_UPDATED_BY = l_user_id
                        WHERE AHH_PART_NBR = rec_main.SXE_PART_NUMBER
                        AND   AHH_CUSTOMER_NBR = rec_main.SXE_CUSTOMER_NUMBER
                        AND   AHH_SITE_NBR  = rec_main.SXE_SITE_NUMBER
                        AND  END_DATE_ACTIVE IS NULL;
					 END IF;
                ELSE
				   l_sec := 'Fetch2 LAST_PRICE_PAID';
                   SELECT LAST_PRICE_PAID,REQUEST_ID 
				    INTO l_existing_pp,l_prev_request_id
                    FROM XXWC_AHH_OE_AIS_LPP_TBL 
                    WHERE AHH_PART_NBR = rec_main.SXE_PART_NUMBER
                    AND   AHH_CUSTOMER_NBR = rec_main.SXE_CUSTOMER_NUMBER
                    AND   AHH_SITE_NBR IS NULL
                    AND   END_DATE_ACTIVE IS NULL
                    AND ROWNUM = 1;
					
					 IF l_existing_pp IS NOT NULL AND l_existing_pp <> l_last_price_paid 
                     THEN
					    l_sec := 'update2 XXWC_AHH_OE_AIS_LPP_TBL';
                        UPDATE XXWC_AHH_OE_AIS_LPP_TBL
                        SET END_DATE_ACTIVE = SYSDATE,
                            LAST_UPDATE_DATE = SYSDATE,
                            LAST_UPDATED_BY = l_user_id
                        WHERE AHH_PART_NBR = rec_main.SXE_PART_NUMBER
                        AND   AHH_CUSTOMER_NBR = rec_main.SXE_CUSTOMER_NUMBER
                        AND   AHH_SITE_NBR IS NULL
                        AND   END_DATE_ACTIVE IS NULL;
					 END IF;
                END IF;
            EXCEPTION
               WHEN OTHERS THEN
                  l_existing_pp := NULL;
            END;
            
            IF l_existing_pp IS NOT NULL AND l_existing_pp = l_last_price_paid 
            THEN
                l_ignore_record := 'Y';
			    IF l_prev_request_id = l_request_id THEN
				   l_error_code := '5';
                   l_error_msg := 'Duplicate Record';
                   RAISE l_val_exception; 
				END IF;
            END IF;
            
            IF l_ignore_record = 'N' THEN
               l_sec := 'INSERT INTO XXWC_AHH_OE_AIS_LPP_TBL';
               INSERT INTO XXWC_AHH_OE_AIS_LPP_TBL(INVENTORY_ITEM_ID        
                                                ,ORCL_PART_NBR            
                                                ,ORCL_CUSTOMER_NBR        
                                                ,ORCL_CUSTOMER_SITE_NBR   
                                                ,SHIP_TO_ORG_ID           
                                                ,CUST_ACCOUNT_ID          
                                                ,AHH_CUSTOMER_NBR         
                                                ,AHH_SITE_NBR             
                                                ,AHH_PART_NBR             
                                                ,LAST_PRICE_PAID          
                                                ,LINE_CREATION_DATE       
                                                ,LAST_PRICE_PAID_EXCL_FLAG
                                                ,START_DATE_ACTIVE        
                                                ,END_DATE_ACTIVE
                                                ,REQUEST_ID                                                
                                                ,CREATED_BY               
                                                ,LAST_UPDATED_BY          
                                                ,CREATION_DATE            
                                                ,LAST_UPDATE_DATE         
                                                ,LAST_UPDATE_LOGIN )
                                         VALUES( l_inventory_item_id                --INVENTORY_ITEM_ID
                                               ,l_orcl_part_nbr                    --ORCL_PART_NBR
                                               ,l_ORCL_CUSTOMER_NBR                --ORCL_CUSTOMER_NBR
                                               ,l_ORCL_CUSTOMER_SITE_NBR           --ORCL_CUSTOMER_SITE_NBR
                                               ,l_orcl_cust_acct_site_id                 --SHIP_TO_ORG_ID
                                               ,l_orcl_cust_acct_id                --CUST_ACCOUNT_ID
                                               ,rec_main.SXE_CUSTOMER_NUMBER       --AHH_CUSTOMER_NBR
                                               ,upper(rec_main.SXE_SITE_NUMBER)           --AHH_SITE_NBR    
                                               ,upper(rec_main.SXE_PART_NUMBER)           --AHH_PART_NBR    
                                               ,l_last_price_paid                  --LAST_PRICE_PAID
                                               ,null                               --LINE_CREATION_DATE
                                               ,'Y'                               --LAST_PRICE_PAID_EXCL_FLAG
                                               ,(sysdate-1)                       --START_DATE_ACTIVE   ??
                                               ,null                               --END_DATE_ACTIVE
                                               ,l_request_id                       --REQUEST_ID
                                               ,l_user_id                          --CREATED_BY
                                               ,l_user_id                          --LAST_UPDATED_BY
                                               ,sysdate                            --CREATION_DATE
                                               ,sysdate                            --LAST_UPDATE_DATE
                                               ,l_user_id                          --LAST_UPDATE_LOGIN
                                              );
         ELSE
            l_error_code := '6';
            l_error_msg := ' Record is ignored because LPP is similar with existing price';
            raise l_val_exception;
         END IF;
         commit;
         EXCEPTION
            WHEN l_val_exception THEN
               ROLLBACK;
               fnd_file.put_line (fnd_file.LOG, l_error_msg||' for SXE_PART_NUMBER:'||rec_main.SXE_PART_NUMBER||' SXE_CUSTOMER_NUMBER:'||rec_main.SXE_CUSTOMER_NUMBER||' SXE_SITE_NUMBER:'||rec_main.SXE_SITE_NUMBER);
               l_sec := 'Call log_error_Message from l_val_exception';
               log_error_Message(p_ahh_cust_num     => rec_main.SXE_CUSTOMER_NUMBER
                                 ,p_ahh_site_num     => rec_main.SXE_SITE_NUMBER
                                 ,p_ahh_part_number  => rec_main.SXE_PART_NUMBER
                                 ,p_ahh_lpp          => rec_main.SXE_LPP
                                 ,p_error_code       => l_error_code
                                 ,p_error_message    => SUBSTR(l_error_msg,1,2000)
                                );
            WHEN OTHERS
            THEN
               ROLLBACK;
               l_error_msg := SQLERRM;
               fnd_file.put_line (fnd_file.LOG, l_sec||' =>'||l_error_msg||' for SXE_PART_NUMBER:'||rec_main.SXE_PART_NUMBER||' SXE_CUSTOMER_NUMBER:'||rec_main.SXE_CUSTOMER_NUMBER||' SXE_SITE_NUMBER:'||rec_main.SXE_SITE_NUMBER);
               l_sec := 'Call log_error_Message from when others';
               log_error_Message(p_ahh_cust_num     => rec_main.SXE_CUSTOMER_NUMBER
                                 ,p_ahh_site_num     => rec_main.SXE_SITE_NUMBER
                                 ,p_ahh_part_number  => rec_main.SXE_PART_NUMBER
                                 ,p_ahh_lpp          => rec_main.SXE_LPP
                                 ,p_error_code       => l_error_code
                                 ,p_error_message    => SUBSTR(l_error_msg,1,2000)
                                );
         END;
      END LOOP;     
      l_sec := 'End Loop';
      fnd_file.put_line (fnd_file.LOG, l_sec);
      l_sec := 'Call log_record_stats';
      log_record_stats(p_request_id => l_request_id);
      fnd_file.put_line (fnd_file.LOG, 'End Main');
   EXCEPTION
      WHEN OTHERS
      THEN
         fnd_file.put_line (fnd_file.LOG, 'error occured (Main):' || l_sec);
         xxcus_error_pkg.xxcus_error_main_api (
            p_called_from         => g_err_callfrom || '.MAIN',
            p_calling             => l_sec,
            p_ora_error_msg       => SQLERRM,
            p_error_desc          => NULL,
            p_distribution_list   => g_distro_list,
            p_module              => g_module);
   END;

END XXWC_AHH_LPP_PKG;
/