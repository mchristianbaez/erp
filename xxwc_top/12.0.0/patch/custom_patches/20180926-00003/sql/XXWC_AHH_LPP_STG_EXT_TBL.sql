/*******************************************************************************************************
  -- Table Name XXWC_AHH_LPP_STG_EXT_TBL
  -- ***************************************************************************************************
  --
  -- PURPOSE: External table used to store Last Price Paid details
  -- HISTORY
  -- ===================================================================================================
  -- ===================================================================================================
  -- VERSION DATE          AUTHOR(S)       DESCRIPTION
  -- ------- -----------   --------------- -------------------------------------------------------------
  -- 1.0     21-Sep-2018   Niraj K Ranjan   TMS#20180926-00003   AHH LLP price upload
********************************************************************************************************/
CREATE TABLE XXWC.XXWC_AHH_LPP_STG_EXT_TBL
(
  SXE_CUSTOMER_NUMBER                  VARCHAR2(240 BYTE),
  ORACLE_CUSTOMER_NUMBER               VARCHAR2(240 BYTE),
  SXE_SITE_NUMBER                      VARCHAR2(240 BYTE),
  ORACLE_SITE_NUMBER                   VARCHAR2(240 BYTE),  
  SXE_PART_NUMBER                      VARCHAR2(240 BYTE),
  ORACLE_PART_NUMBER                   VARCHAR2(240 BYTE),  
  SXE_LPP                              VARCHAR2(240 BYTE)
)
ORGANIZATION EXTERNAL
  (  TYPE ORACLE_LOADER
     DEFAULT DIRECTORY XXWC_AHH_USER_CONV_DIR
     ACCESS PARAMETERS 
       ( RECORDS DELIMITED BY NEWLINE CHARACTERSET WE8MSWIN1252
      SKIP 1
    BADFILE 'XXWC_AHH_LPP_STG_EXT_TBL.bad'
    DISCARDFILE 'XXWC_AHH_LPP_STG_EXT_TBL.dsc'
    FIELDS TERMINATED BY '|'
    OPTIONALLY ENCLOSED BY '"' AND '"'
    REJECT ROWS WITH ALL NULL FIELDS
                     )
     LOCATION (XXWC_AHH_USER_CONV_DIR:'XXWC_AHH_LPP.csv')
  )
REJECT LIMIT UNLIMITED;