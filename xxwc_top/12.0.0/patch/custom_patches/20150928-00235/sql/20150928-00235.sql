/* Data fix to correct the Item description in the PO
TMS# 20150928-00235 - By Manjula on 29-Sep-15
*/

SET SERVEROUTPUT ON SIZE 1000000

DECLARE

l_sec VARCHAR2(50);

BEGIN

l_sec := 'Script 1';

update po_lines_archive_all 
set Item_description = '3M E-A-R EXPRESS POD PLUGS CORDED IN VENDING PACK VP311-1115, 4 PAIRS/PACK, 125 PACKS/CASE'
where po_line_id = 5115016;

   DBMS_OUTPUT.put_line (
      'Script 1 -After update , Lines updated : ' || SQL%ROWCOUNT);

l_sec := 'Script 2';

--Reset EDI Flag
update po_headers_all
set print_count = 0, 
printed_date = NULL, 
edi_processed_flag = NULL 
where segment1 = ('1209691');

   DBMS_OUTPUT.put_line (
      'Script 2 -After update , Lines updated : ' || SQL%ROWCOUNT);

l_sec := 'Script 3';

--Reset EDI flag
update po_headers_archive_all
Set print_count = 0, 
printed_date = NULL, 
edi_processed_flag = NULL 
where segment1 = ('1209691');

   DBMS_OUTPUT.put_line (
      'Script 3 -After update , Lines updated : ' || SQL%ROWCOUNT);

COMMIT;

EXCEPTION
   WHEN OTHERS
   THEN
      ROLLBACK;
      DBMS_OUTPUT.put_line ('Error at ' ||l_sec||' : ' || SQLERRM);

END;
/
