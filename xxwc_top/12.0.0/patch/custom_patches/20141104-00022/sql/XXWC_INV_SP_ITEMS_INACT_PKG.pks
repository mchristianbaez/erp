CREATE OR REPLACE PACKAGE apps.xxwc_inv_sp_items_inact_pkg AS
  /**************************************************************************
  *
  * FUNCTION | PROCEDURE | CURSOR
  *  XXWC_INV_SP_ITEMS_INACT_PKG
  *
  * DESCRIPTION
  *  Report Only shows Special Items Inactivate 
  *
  * PARAMETERS
  * ==========
  * NAME              TYPE     DESCRIPTION
  * ----------------- -------- ---------------------------------------------
  * P_DAYS            <IN>       Number of Days
  * P_TYPE            <IN>       Process Type
  *
  *
  *
  *
  * CALLED BY
  *   Which program, if any, calls this one
   * HISTORY
  * =======
  *
  * VERSION DATE        AUTHOR(S)       DESCRIPTION
  * ------- ----------- --------------- ------------------------------------
  * 1.00    07/11/2014   Gajendra M      Creation(TMS#20141104-00022)
  *
  *************************************************************************/
  /*************************************************************************
  
     1. Procedure to Write Log Messages
  
  **************************************************************************/
  PROCEDURE write_log(p_log_msg VARCHAR2);
  /*************************************************************************
  
     2. Procedure to Load items
  
  **************************************************************************/
  PROCEDURE load_items(p_retcode OUT VARCHAR2);
  /*************************************************************************
  
      3. Procedure to Load On order Quantity for Invetory Special Items
  
  **************************************************************************/
  PROCEDURE load_on_ord_qty(p_retcode OUT VARCHAR2);

  /*************************************************************************
  
     4. Procedure to extract and Special Items Items
  
  **************************************************************************/
  PROCEDURE extract_special_items(p_retcode OUT NUMBER
                                 ,p_days    IN NUMBER);
  /*************************************************************************
  
      6. Procedure to update end dated buyers
  
  **************************************************************************/
  PROCEDURE update_buyers(p_retcode OUT VARCHAR2);

  /*************************************************************************
  
     5. Procedure to update CSP and Special Inactive Items in Interface Table
  
  **************************************************************************/
  PROCEDURE update_special_items_status(p_retcode OUT NUMBER
                                       ,p_type    IN VARCHAR2);
  /*************************************************************************
  
       6. Main Procedure to run the sub loads sequentially
  
  **************************************************************************/
  PROCEDURE load_inv_sp_main(errbuf         OUT VARCHAR2
                            ,retcode        OUT NUMBER
                            ,p_no_of_days   IN NUMBER
                            ,p_process_type IN VARCHAR2);

END;
/