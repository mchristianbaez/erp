CREATE TABLE XXWC.XXWC_INV_SPECIAL_ITEMS_TBL
/*************************************************************************
  $Header XXWC_INV_SPECIAL_ITEMS_TBL.sql $
  Module Name: XXWC_INV_SPECIAL_ITEMS_TBL
  PURPOSE: Load the final CSP and Special Items for Inactivation through Item Import
  TMS Task Id :  20141104-00022
  REVISIONS:
  Ver        Date         Author                Description
  ---------  -----------  ------------------    ----------------
  1.0        07-Jan-2015  Gajendra Mavilla      Initial Version
**************************************************************************/
(
  INVENTORY_ITEM_ID          NUMBER            NOT NULL,
  ORGANIZATION_ID             NUMBER            NOT NULL,
  ORGANIZATION_CODE           VARCHAR2(3 BYTE),
  PART_NUMBER                 VARCHAR2(40 BYTE),
  CREATION_DATE               DATE              NOT NULL,
  LAST_UPDATE_DATE            DATE              NOT NULL,
  LAST_ACTIVITY_DATE          DATE,
  OH_EXISTS                   VARCHAR2(1 BYTE),
  SO_EXISTS                   VARCHAR2(1 BYTE),
  QUOTE_EXISTS                VARCHAR2(1 BYTE),
  PO_EXISTS                   VARCHAR2(1 BYTE),
  ACTIVE_CSP_EXISTS           VARCHAR2(1 BYTE),
  INACTIVE_CSP_EXISTS         VARCHAR2(1 BYTE),
  INVENTORY_ITEM_STATUS_CODE  VARCHAR2(10 BYTE) NOT NULL,
  ITEM_TYPE                   VARCHAR2(30 BYTE),
  PRIMARY_UOM_CODE            VARCHAR2(3 BYTE),
  BUYER_ID                    NUMBER(9)
)
/