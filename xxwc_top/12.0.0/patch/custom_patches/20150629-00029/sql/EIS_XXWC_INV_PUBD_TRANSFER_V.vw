CREATE OR REPLACE VIEW XXEIS.EIS_XXWC_INV_PUBD_TRANSFER_V (ORG, ITEM_NUMBER, DESCRIPTION, QUANTITY, UOM, SHELF_LIFE_DAYS, DAYS_IN_PUBD, ITEM_COST, ITEM_EXTENDED_COST, VENDOR_NAME, MMT_TRANSACTION_ID, MSIB_INVENTORY_ITEM_ID, MSIB_ORGANIZATION_ID)
AS
  SELECT /*+ INDEX(mmt MTL_MATERIAL_TRANSACTIONS_N3)*/
	mp.organization_code org,
    msib.concatenated_segments item_number,
    msib.description,
    xxeis.eis_xxwc_inv_util_pkg.get_onhand_qty(mmt.inventory_item_id,mmt.organization_id,mmt.subinventory_code) quantity,
    muom.unit_of_measure uom,
    msib.shelf_life_days,
    ROUND((sysdate-mmt.transaction_date),2) days_in_pubd ,
    NVL(apps.cst_cost_api.get_item_cost (1, msib.inventory_item_id, msib.organization_id), 0) item_cost ,
    (xxeis.eis_xxwc_inv_util_pkg.get_onhand_qty(mmt.inventory_item_id,mmt.organization_id,mmt.subinventory_code))*(ROUND(NVL(apps.cst_cost_api.get_item_cost (1, msib.inventory_item_id, msib.organization_id), 0),2)) item_extended_cost,
    (SELECT ap.vendor_name
    FROM apps.mtl_cross_references mic,
      apps.ap_suppliers ap
    WHERE mic.inventory_item_id  = msib.inventory_item_id
    AND mic.cross_reference_type = 'VENDOR'
    AND mic.attribute1           = ap.segment1
    AND rownum                   =1
    )vendor_name,
    mmt.transaction_id mmt_transaction_id,
    msib.inventory_item_id msib_inventory_item_id,
    msib.organization_id msib_organization_id
  FROM apps.mtl_material_transactions mmt,
    apps.mtl_system_items_kfv msib,
    apps.mtl_parameters mp ,
    apps.mtl_units_of_measure muom
  WHERE mmt.transaction_type_id IN (2,41)
  AND mmt.organization_id        = msib.organization_id
  AND mmt.inventory_item_id      = msib.inventory_item_id
  AND mmt.organization_id        = mp.organization_id
  AND mmt.transaction_uom        = muom.uom_code
  AND mmt.subinventory_code      ='PUBD'
/
