--Report Name            : INV PUBD TRANSFER-WC
--Import Instructions    : Run this file in XXEIS user from SQL*Plus or any tool like Toad, SQL Navigator. 
prompt Creating View Data for INV PUBD TRANSFER-WC
set scan off define off
DECLARE
BEGIN 
--Inserting View EIS_XXWC_INV_PUBD_TRANSFER_V
xxeis.eis_rs_ins.v( 'EIS_XXWC_INV_PUBD_TRANSFER_V',401,'','','','','SA059956','XXEIS','Eis Xxwc Inv Pubd Transfer V','EXIPTV','','');
--Delete View Columns for EIS_XXWC_INV_PUBD_TRANSFER_V
xxeis.eis_rs_utility.delete_view_rows('EIS_XXWC_INV_PUBD_TRANSFER_V',401,FALSE);
--Inserting View Columns for EIS_XXWC_INV_PUBD_TRANSFER_V
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_PUBD_TRANSFER_V','ITEM_EXTENDED_COST',401,'Item Extended Cost','ITEM_EXTENDED_COST','','','','SA059956','NUMBER','','','Item Extended Cost','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_PUBD_TRANSFER_V','DAYS_IN_PUBD',401,'Days In Pubd','DAYS_IN_PUBD','','','','SA059956','NUMBER','','','Days In Pubd','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_PUBD_TRANSFER_V','SHELF_LIFE_DAYS',401,'Shelf Life Days','SHELF_LIFE_DAYS','','','','SA059956','NUMBER','','','Shelf Life Days','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_PUBD_TRANSFER_V','UOM',401,'Uom','UOM','','','','SA059956','VARCHAR2','','','Uom','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_PUBD_TRANSFER_V','QUANTITY',401,'Quantity','QUANTITY','','','','SA059956','NUMBER','','','Quantity','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_PUBD_TRANSFER_V','DESCRIPTION',401,'Description','DESCRIPTION','','','','SA059956','VARCHAR2','','','Description','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_PUBD_TRANSFER_V','ITEM_NUMBER',401,'Item Number','ITEM_NUMBER','','','','SA059956','VARCHAR2','','','Item Number','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_PUBD_TRANSFER_V','ORG',401,'Org','ORG','','','','SA059956','VARCHAR2','','','Org','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_PUBD_TRANSFER_V','ITEM_COST',401,'Item Cost','ITEM_COST','','','','SA059956','NUMBER','','','Item Cost','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_PUBD_TRANSFER_V','VENDOR_NAME',401,'Vendor Name','VENDOR_NAME','','','','SA059956','VARCHAR2','','','Vendor Name','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_PUBD_TRANSFER_V','MMT_TRANSACTION_ID',401,'Mmt Transaction Id','MMT_TRANSACTION_ID','','','','SA059956','NUMBER','','','Mmt Transaction Id','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_PUBD_TRANSFER_V','MSIB_INVENTORY_ITEM_ID',401,'Msib Inventory Item Id','MSIB_INVENTORY_ITEM_ID','','','','SA059956','NUMBER','','','Msib Inventory Item Id','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_PUBD_TRANSFER_V','MSIB_ORGANIZATION_ID',401,'Msib Organization Id','MSIB_ORGANIZATION_ID','','','','SA059956','NUMBER','','','Msib Organization Id','','','');
--Inserting View Components for EIS_XXWC_INV_PUBD_TRANSFER_V
--Inserting View Component Joins for EIS_XXWC_INV_PUBD_TRANSFER_V
END;
/
set scan on define on
prompt Creating Report LOV Data for INV PUBD TRANSFER-WC
set scan off define off
DECLARE
BEGIN 
--Inserting Report LOVs - INV PUBD TRANSFER-WC
xxeis.eis_rs_ins.lov( 401,'','''Show All'' ,''Show Only > 90 Days'',''Show Only > 180 Days''','EIS INV PUBD Days LOV','','SA059956',NULL,'N','','');
END;
/
set scan on define on
prompt Creating Report Data for INV PUBD TRANSFER-WC
set scan off define off
DECLARE
BEGIN 
--Deleting Report data - INV PUBD TRANSFER-WC
xxeis.eis_rs_utility.delete_report_rows( 'INV PUBD TRANSFER-WC' );
--Inserting Report - INV PUBD TRANSFER-WC
xxeis.eis_rs_ins.r( 401,'INV PUBD TRANSFER-WC','','Create new EIS Report that displays the date of manual sub-inventory transfer to the PUBD subinventory','','','','SA059956','EIS_XXWC_INV_PUBD_TRANSFER_V','Y','','','SA059956','','N','Transaction Reports','','CSV,EXCEL,','N');
--Inserting Report Columns - INV PUBD TRANSFER-WC
xxeis.eis_rs_ins.rc( 'INV PUBD TRANSFER-WC',401,'DAYS_IN_PUBD','Days In PUBD','Days In Pubd','','~~~','default','','7','N','','','','','','','','SA059956','N','N','','EIS_XXWC_INV_PUBD_TRANSFER_V','','');
xxeis.eis_rs_ins.rc( 'INV PUBD TRANSFER-WC',401,'DESCRIPTION','Description','Description','','','default','','3','N','','','','','','','','SA059956','N','N','','EIS_XXWC_INV_PUBD_TRANSFER_V','','');
xxeis.eis_rs_ins.rc( 'INV PUBD TRANSFER-WC',401,'ITEM_EXTENDED_COST','Item Extended Cost','Item Extended Cost','','~,~.~2','default','','9','N','','','','','','','','SA059956','N','N','','EIS_XXWC_INV_PUBD_TRANSFER_V','','');
xxeis.eis_rs_ins.rc( 'INV PUBD TRANSFER-WC',401,'ITEM_NUMBER','Item Number','Item Number','','','default','','2','N','','','','','','','','SA059956','N','N','','EIS_XXWC_INV_PUBD_TRANSFER_V','','');
xxeis.eis_rs_ins.rc( 'INV PUBD TRANSFER-WC',401,'ORG','Org','Org','','','default','','1','N','','','','','','','','SA059956','N','N','','EIS_XXWC_INV_PUBD_TRANSFER_V','','');
xxeis.eis_rs_ins.rc( 'INV PUBD TRANSFER-WC',401,'QUANTITY','Quantity(in PUBD)','Quantity','','~~~','default','','4','N','','','','','','','','SA059956','N','N','','EIS_XXWC_INV_PUBD_TRANSFER_V','','');
xxeis.eis_rs_ins.rc( 'INV PUBD TRANSFER-WC',401,'SHELF_LIFE_DAYS','Shelf Life Days','Shelf Life Days','','~~~','default','','6','N','','','','','','','','SA059956','N','N','','EIS_XXWC_INV_PUBD_TRANSFER_V','','');
xxeis.eis_rs_ins.rc( 'INV PUBD TRANSFER-WC',401,'UOM','UOM','Uom','','','default','','5','N','','','','','','','','SA059956','N','N','','EIS_XXWC_INV_PUBD_TRANSFER_V','','');
xxeis.eis_rs_ins.rc( 'INV PUBD TRANSFER-WC',401,'ITEM_COST','Item Cost','Item Cost','','~,~.~2','default','','8','N','','','','','','','','SA059956','N','N','','EIS_XXWC_INV_PUBD_TRANSFER_V','','');
xxeis.eis_rs_ins.rc( 'INV PUBD TRANSFER-WC',401,'VENDOR_NAME','Vendor Name','Vendor Name','','','default','','10','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_INV_PUBD_TRANSFER_V','','');
--Inserting Report Parameters - INV PUBD TRANSFER-WC
xxeis.eis_rs_ins.rp( 'INV PUBD TRANSFER-WC',401,'Date','Date','','IN','EIS INV PUBD Days LOV','''Show All''','VARCHAR2','Y','Y','1','','N','CONSTANT','SA059956','Y','N','','','');
--Inserting Report Conditions - INV PUBD TRANSFER-WC
xxeis.eis_rs_ins.rcn( 'INV PUBD TRANSFER-WC',401,'','','','','and ((:Date= ''Show Only > 180 Days''
          AND  EXIPTV.DAYS_IN_PUBD > 180)
     or (:Date=''Show Only > 90 Days''
          AND  EXIPTV.DAYS_IN_PUBD > 90)
     or (:Date=''Show All'')
     )','Y','1','','SA059956');
--Inserting Report Sorts - INV PUBD TRANSFER-WC
xxeis.eis_rs_ins.rs( 'INV PUBD TRANSFER-WC',401,'ITEM_NUMBER','ASC','SA059956','1','');
--Inserting Report Triggers - INV PUBD TRANSFER-WC
--Inserting Report Templates - INV PUBD TRANSFER-WC
--Inserting Report Portals - INV PUBD TRANSFER-WC
--Inserting Report Dashboards - INV PUBD TRANSFER-WC
--Inserting Report Security - INV PUBD TRANSFER-WC
xxeis.eis_rs_ins.rsec( 'INV PUBD TRANSFER-WC','660','','21623',401,'SA059956','','');
xxeis.eis_rs_ins.rsec( 'INV PUBD TRANSFER-WC','660','','22480',401,'SA059956','','');
xxeis.eis_rs_ins.rsec( 'INV PUBD TRANSFER-WC','660','','51044',401,'SA059956','','');
xxeis.eis_rs_ins.rsec( 'INV PUBD TRANSFER-WC','660','','51045',401,'SA059956','','');
xxeis.eis_rs_ins.rsec( 'INV PUBD TRANSFER-WC','660','','50901',401,'SA059956','','');
xxeis.eis_rs_ins.rsec( 'INV PUBD TRANSFER-WC','660','','51025',401,'SA059956','','');
xxeis.eis_rs_ins.rsec( 'INV PUBD TRANSFER-WC','660','','50860',401,'SA059956','','');
xxeis.eis_rs_ins.rsec( 'INV PUBD TRANSFER-WC','660','','50886',401,'SA059956','','');
xxeis.eis_rs_ins.rsec( 'INV PUBD TRANSFER-WC','660','','50859',401,'SA059956','','');
xxeis.eis_rs_ins.rsec( 'INV PUBD TRANSFER-WC','660','','50858',401,'SA059956','','');
xxeis.eis_rs_ins.rsec( 'INV PUBD TRANSFER-WC','660','','50857',401,'SA059956','','');
--Inserting Report Pivots - INV PUBD TRANSFER-WC
END;
/
set scan on define on
