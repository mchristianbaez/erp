/*************************************************************************
  $Header TMS_20170830-00171_LINES_CANCEL.sql $
  Module Name: TMS_20170830-00171 Data Fix for SO lines cancel 

  PURPOSE: Data Fix for item SO#25606989 lines cancel 

  REVISIONS:
  Ver        Date         Author                Description
  ---------  -----------  ------------------    --------------------------
  1.0        22-SEP-2017  Pattabhi Avula         TMS#20170830-00171

**************************************************************************/ 
SET SERVEROUTPUT ON SIZE 1000000;
DECLARE
BEGIN
   DBMS_OUTPUT.put_line ('TMS: 20170830-00171   , Before Update');

UPDATE OE_ORDER_LINES_ALL
SET FLOW_STATUS_CODE='CANCELLED',
OPEN_FlAG='N'
Where HEADER_ID=62839169;

-- Expected to update on row

   DBMS_OUTPUT.put_line (
         'TMS: 20170830-00171 Sales order lines updated (Expected:5): '
      || SQL%ROWCOUNT);

   COMMIT;

   DBMS_OUTPUT.put_line ('TMS: 20170830-00171   , End Update');
EXCEPTION
   WHEN OTHERS
   THEN
      ROLLBACK;
      DBMS_OUTPUT.put_line ('TMS: 20170830-00171, Errors : ' || SQLERRM);
END;
/