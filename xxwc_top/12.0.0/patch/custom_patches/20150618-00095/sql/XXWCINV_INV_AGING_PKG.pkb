CREATE OR REPLACE PACKAGE BODY APPS.XXWCINV_INV_AGING_PKG
AS
/***************************************************************************
    $Header XXWCINV_INV_AGING_PKG.pkb $
    Module Name: XXWCINV_INV_AGING_PKG
    PURPOSE: Package used for Aging Report

    REVISIONS:
    Ver    Date           Author                Description
    ------ ---------      ------------------    ----------------
    1.0    Unknown        Unknown               Initial Version
 -- There are multiple versions in SVN. To Follow the standards adding the history from Version 2.0
    2.0    19-Jun-2015    Manjula Chellappan    TMS# 20150618-00095 Inventory Aging Report - Redesign concurrent program

****************************************************************************/
    -- define global variables
    xxwc_error        EXCEPTION;

    -- Error DEBUG
    l_err_msg         VARCHAR2 (2000);
    l_err_callfrom    VARCHAR2 (175) := 'XXWCINV_INV_AGING_PKG';
    l_err_callpoint   VARCHAR2 (175) := 'START';
    l_distro_list     VARCHAR2 (80) := 'OracleDevelopmentGroup@hdsupply.com';
    l_module          VARCHAR2 (80);

    /***************************************************************************************/
    /* CP_Org_Name_P : function to translate organization_id into organization name        */
    /***************************************************************************************/
    FUNCTION cp_org_name_p
        RETURN VARCHAR2
    IS
        l_org_name   VARCHAR2 (80) := NULL;
    BEGIN
        l_module := 'CP_Org_Name_P';
        l_err_callpoint := 'Start';

        IF p_branch IS NOT NULL
        THEN
            SELECT SUBSTR (organization_name, 1, 80)
              INTO l_org_name
              FROM org_organization_definitions
             WHERE organization_id = p_branch;
        END IF;

        RETURN l_org_name;
    EXCEPTION
        WHEN OTHERS
        THEN
            RETURN NULL;
    END cp_org_name_p;

    /***************************************************************************************/
    /* CP_vendor_Name_P : function to get vendor source        				   */
    /***************************************************************************************/
    
    FUNCTION cp_vendor_name_p(p_inventory_item_id NUMBER, p_organization_id NUMBER)
        RETURN VARCHAR2
    IS
        l_vendor_name   VARCHAR2 (80) := NULL;
    BEGIN
   
   SELECT DECODE (SSO.SOURCE_TYPE, 3, PV.VENDOR_NAME, 1, msp.ORGANIZATION_CODE) 
          INTO l_vendor_name  
   FROM apps.MRP_SR_ASSIGNMENTS SRA,
       apps.MRP_ASSIGNMENT_SETS MAS,
       apps.MRP_SOURCING_RULES SR,       
       apps.MRP_SR_SOURCE_ORG SSO,
       apps.MRP_SR_RECEIPT_ORG SRO,
       po_vendors PV,
       mtl_parameters msp
 WHERE SR.sourcing_rule_id = SRA.sourcing_rule_id
   AND SRA.assignment_set_id = mas.assignment_set_id
   AND   SRA.INVENTORY_ITEM_ID   =  p_inventory_item_id
   AND mas.assignment_set_id = 1
   AND PV.vendor_id(+) = SSO.vendor_id
   AND SR.sourcing_rule_id = SRO.sourcing_rule_id
   AND SRO.sr_receipt_id = SSO.sr_receipt_id
   AND msp.ORGANIZATION_ID(+) = SSO.SOURCE_ORGANIZATION_ID 
   AND TRUNC (SYSDATE) BETWEEN SRO.EFFECTIVE_DATE AND NVL (DISABLE_DATE, SYSDATE + 1)
   AND SRA.ORGANIZATION_ID     = p_organization_id 
   AND SRA.ASSIGNMENT_TYPE = 6;
        RETURN l_vendor_name;
    EXCEPTION
        WHEN OTHERS
        THEN
            RETURN NULL;
    END cp_vendor_name_p;

    /***************************************************************************************/
    /* CP_Supplier_Name_P : function translate vendor_id into vendor name                  */
    /* Created this function by Maharajan S on 04/23/2014 for ESMS#247548
    /***************************************************************************************/
    FUNCTION cp_supplier_name_p
        RETURN VARCHAR2
    IS
        l_supplier_name   VARCHAR2 (80) := NULL;
    BEGIN
        l_module := 'CP_Supplier_Name_P';
        l_err_callpoint := 'Start';

        IF p_supplier_id IS NOT NULL
        THEN
            SELECT SUBSTR (vendor_name, 1, 80)
              INTO l_supplier_name
              FROM ap_suppliers
             WHERE vendor_id = p_supplier_id;
        END IF;

        RETURN l_supplier_name;
    EXCEPTION
        WHEN OTHERS
        THEN
            RETURN NULL;
    END cp_supplier_name_p;

    /***************************************************************************************/
    /* Get_Reserve_Amt : private function to determine which reserve aging group is being  */
    /*                   used and calculate the reserve amount                             */
    /***************************************************************************************/
    FUNCTION get_reserve_amt (p_reserve_name IN VARCHAR2, p_aging_days IN NUMBER, p_amount IN NUMBER)
        RETURN NUMBER
    IS
        CURSOR c_reserv
        IS
            SELECT NVL (bucket1_days, 999999) bucket1_days
                  ,NVL (bucket1_rate, 0) / 100 bucket1_rate
                  ,NVL (bucket2_days, 999999) bucket2_days
                  ,NVL (bucket2_rate, 0) / 100 bucket2_rate
                  ,NVL (bucket3_days, 999999) bucket3_days
                  ,NVL (bucket3_rate, 0) / 100 bucket3_rate
                  ,NVL (bucket4_days, 999999) bucket4_days
                  ,NVL (bucket4_rate, 0) / 100 bucket4_rate
                  ,NVL (bucket5_days, 999999) bucket5_days
                  ,NVL (bucket5_rate, 0) / 100 bucket5_rate
                  ,NVL (bucket6_days, 999999) bucket6_days
                  ,NVL (bucket6_rate, 0) / 100 bucket6_rate
                  ,NVL (bucket7_days, 999999) bucket7_days
                  ,NVL (bucket7_rate, 0) / 100 bucket7_rate
                  ,NVL (undetermined_rate, 0) / 100 undetermined_rate
              FROM xxwc.xxwcinv_inv_aging_groups
             WHERE UPPER (group_name) = UPPER (p_reserve_name);

        r_reserv        c_reserv%ROWTYPE;

        l_reserve_amt   NUMBER := 0;
    BEGIN
        l_module := 'Get_Reserve_Amt';
        l_err_callpoint := 'Start';

        OPEN c_reserv;

        FETCH c_reserv INTO r_reserv;

        IF c_reserv%NOTFOUND
        THEN
            CLOSE c_reserv;

            RETURN (0);
        END IF;

        l_err_callpoint := 'After Cursor';

        IF p_aging_days < 0
        THEN
            l_reserve_amt := p_amount * r_reserv.undetermined_rate;
        ELSIF p_aging_days < r_reserv.bucket1_days
        THEN
            l_reserve_amt := p_amount * r_reserv.bucket1_rate;
        ELSIF p_aging_days < r_reserv.bucket2_days
        THEN
            l_reserve_amt := p_amount * r_reserv.bucket2_rate;
        ELSIF p_aging_days < r_reserv.bucket3_days
        THEN
            l_reserve_amt := p_amount * r_reserv.bucket3_rate;
        ELSIF p_aging_days < r_reserv.bucket4_days
        THEN
            l_reserve_amt := p_amount * r_reserv.bucket4_rate;
        ELSIF p_aging_days < r_reserv.bucket5_days
        THEN
            l_reserve_amt := p_amount * r_reserv.bucket5_rate;
        ELSIF p_aging_days < r_reserv.bucket6_days
        THEN
            l_reserve_amt := p_amount * r_reserv.bucket6_rate;
        ELSIF p_aging_days < r_reserv.bucket7_days
        THEN
            l_reserve_amt := p_amount * r_reserv.bucket7_rate;
        ELSE
            l_reserve_amt := p_amount * r_reserv.undetermined_rate;
        END IF;

        CLOSE c_reserv;

        l_err_callpoint := 'End';

        RETURN (ROUND (l_reserve_amt, 2));
    EXCEPTION
        WHEN OTHERS
        THEN
            ROLLBACK;
            l_err_msg := 'Error in ' || l_err_callpoint || ' WITH  ' || SUBSTR (SQLERRM, 1, 1000);
            --dbms_output.put_line(l_fulltext);
            fnd_file.put_line (fnd_file.LOG, l_err_msg);
            fnd_file.put_line (fnd_file.output, l_err_msg);

            xxcus_error_pkg.xxcus_error_main_api (
                p_called_from         => l_err_callfrom
               ,p_calling             => l_err_callpoint
               ,p_ora_error_msg       => SQLERRM
               ,p_error_desc          => 'Error running XXWCINV_INV_AGING_PKG package with PROGRAM ERROR'
               ,p_distribution_list   => l_distro_list
               ,p_module              => l_module);
    END get_reserve_amt;

    ------------------------------------------------------------------
    -- wrapper function to call ad-hoc version of report
    -- Satish U: 06-MAR-2012 : Commented Market Parameter as it is not used any more
    ------------------------------------------------------------------
    FUNCTION beforereport
        RETURN BOOLEAN
    IS
        --Added Select Distinct on Cursor for Receiving SubQuery
        --Removed 12/12/12
        /*
          Cursor C1 Is
            Select MSI.inventory_item_id,
                   MSI.organization_id,
                   MSI.segment1 item_number,
                   MSI.description item_description,
                   Decode(MSI.shelf_life_code, '1', MSI.shelf_life_days, 0) shelf_life_days,
                   MSI.lot_control_code,
                   MP.organization_code location,
                   MP.attribute8 district,
                   MP.attribute9 region,
                   MP.attribute6 market,
                   DECODE (mc.segment2,
                        '6000', 'Y',
                        '6007', 'Y',
                        '6016', 'Y',
                        '6019', 'Y',
                        '6021', 'Y',
                        '6022', 'Y',
                        '6023', 'Y',
                        'N') rebar, --Added 12/12/12 to include additional rebar category classes
                   --Decode(MC.segment2, '6000', 'Y', 'N') rebar, --Removed 12/12/12
                   MC.concatenated_segments item_category,
                   AP.segment1 supplier_number,
                   AP.vendor_name supplier_name,
                   MOQD.orig_date_received,
                   MOQD.primary_transaction_quantity,
                   (MOQD.primary_transaction_quantity * CIC.item_cost) item_cost
              From MTL_SYSTEM_ITEMS_B MSI,
                   MTL_PARAMETERS MP,
                   MTL_ITEM_CATEGORIES MIC,
                   MTL_CATEGORIES_KFV MC,
                   MTL_CATEGORY_SETS_TL MCS,
                   MTL_ONHAND_QUANTITIES_DETAIL MOQD,
                   CST_ITEM_COSTS CIC,
                   CST_COST_TYPES CCT,
                   AP_SUPPLIERS AP,
                   (Select DISTINCT RSH.vendor_id, RSL.item_id, RSL.to_organization_id --Added Select Distinct LHS 06/21/2012
                                From RCV_SHIPMENT_HEADERS RSH, RCV_SHIPMENT_LINES RSL, RCV_TRANSACTIONS RT
                               Where RSH.shipment_header_id = RSL.shipment_header_id
                                 And RSH.receipt_source_code = 'VENDOR'
                                 And RT.shipment_header_id = RSH.shipment_header_id
                                 And RT.shipment_line_id = RSL.shipment_line_id
                                 And RT.transaction_type = 'RECEIVE'
                                 And RT.transaction_date = (Select Max(RT2.transaction_date) From RCV_SHIPMENT_LINES RSL2, RCV_TRANSACTIONS RT2
                                                             Where RSL2.item_id = RSL.item_id
                                                               And RSL2.to_organization_id = RSL.to_organization_id
                                                               And RT2.shipment_line_id = RSL2.shipment_line_id
                                                               And RT2.transaction_type = 'RECEIVE')
                  ) RCV
             Where MSI.organization_id = MP.organization_id
               And MSI.inventory_item_id = MIC.inventory_Item_id
               And MSI.organization_id = MIC.organization_id
               And MIC.category_id = MC.category_id
        --       And MC.structure_id = MCS.structure_id
               And MIC.category_set_id = MCS.category_set_id
               And MCS.category_set_name = 'Inventory Category'
               And MSI.inventory_item_id = MOQD.inventory_item_id
               And MSI.organization_id = MOQD.organization_id
               And MSI.inventory_item_id = CIC.inventory_Item_Id
               And MSI.organization_id = CIC.organization_id
               And CIC.cost_type_id = CCT.cost_type_Id
               And CCT.cost_type = 'Average'
               And MSI.inventory_item_id = RCV.item_id(+)
               And MSI.organization_id = RCV.to_organization_id(+)
               And RCV.vendor_id = AP.vendor_id(+)
               And MP.attribute9 = Nvl(p_region, MP.attribute9)   -- region
               And MC.concatenated_segments = Nvl(p_item_category, MC.concatenated_segments)  -- category
               And Nvl(AP.vendor_id,-99) = Nvl(p_supplier_id, Nvl(AP.vendor_id,-99))   -- supplier
               --And Nvl(MP.attribute12,'~') = Nvl(P_MARKET, Nvl(MP.attribute12,'~'))  -- market
               And MP.organization_id = Nvl(P_BRANCH, MP.organization_id)  -- branch
               And Trunc(Trunc(SYSDATE) - Nvl(MOQD.orig_date_received,(SYSDATE-999))) Between Nvl(P_DAYS_LOW, 0) And Nvl(P_DAYS_HIGH, 999999)
             Order By MP.organization_code, MSI.segment1;
        */

        --Added 12/12/12 to include Subinventory, Receiving, and In-Transit
        CURSOR c1
        IS
            --Subinventory
            SELECT msi.inventory_item_id
                  ,msi.organization_id
--Added source_vendor_name by Maha on 04/23/14 for ESMS#247548
		 , (SELECT cp_vendor_name_p(msi.inventory_item_id,msi.organization_id) FROM dual) source_vendor_name 
                  ,msi.segment1 item_number
                  ,msi.description item_description
                  ,DECODE (msi.shelf_life_code, '1', msi.shelf_life_days, 0) shelf_life_days
                  ,msi.lot_control_code
                  ,mp.organization_code location
                  ,mp.attribute8 district
                  ,mp.attribute9 region
                  ,mp.attribute6 market
                  ,DECODE (mc.segment2
                          ,'6000', 'Y'
                          ,'6007', 'Y'
                          ,'6016', 'Y'
                          ,'6019', 'Y'
                          ,'6021', 'Y'
                          ,'6022', 'Y'
                          ,'6023', 'Y'
                          ,'N')
                       rebar
                  ,                                        --added 12/12/12 to include additional rebar category classes
                   --Decode(MC.segment2, '6000', 'Y', 'N') rebar, removed 12/12/12
                   mc.concatenated_segments item_category
                  -- 08/19/2013 CG: TMS 20130110-01638: Updated for Point (2) to pull true vendor
                  -- ,ap.segment1 supplier_number
                  -- ,ap.vendor_name supplier_name
                  ,x1.segment1 supplier_number
                  ,x1.vendor_name supplier_name
                  -- 08/19/2013 CG: TMS 20130110-01638: Updated for Point (2) to pull true vendor
                  ,moqd.orig_date_received
                  ,moqd.primary_transaction_quantity
                  , (moqd.primary_transaction_quantity * cic.item_cost) item_cost
                  ,'Subinventory' material_location
                  ,DECODE (moqd.is_consigned,  1, 'Y',  2, 'N') consigned_flag                         --added 3/15/2013
              FROM mtl_system_items_b msi
                  ,mtl_parameters mp
                  ,mtl_item_categories mic
                  ,mtl_categories_kfv mc
                  ,mtl_category_sets_tl mcs
                  ,mtl_onhand_quantities_detail moqd
                  ,cst_item_costs cic
                  ,cst_cost_types cct
                  -- 08/19/2013 CG: TMS 20130110-01638: Updated for Point (2) to pull true vendor
                  -- ,ap_suppliers ap
                  ,(SELECT DISTINCT rsh.vendor_id, rsl.item_id, rsl.to_organization_id --Added Select Distinct LHS 06/21/2012
                      FROM rcv_shipment_headers rsh, rcv_shipment_lines rsl, rcv_transactions rt
                     WHERE     rsh.shipment_header_id = rsl.shipment_header_id
                           AND rsh.receipt_source_code = 'VENDOR'
                           AND rt.shipment_header_id = rsh.shipment_header_id
                           AND rt.shipment_line_id = rsl.shipment_line_id
                           AND rt.transaction_type = 'RECEIVE'
                           AND rt.transaction_date =
                                   (SELECT MAX (rt2.transaction_date)
                                      FROM rcv_shipment_lines rsl2, rcv_transactions rt2
                                     WHERE     rsl2.item_id = rsl.item_id
                                           AND rsl2.to_organization_id = rsl.to_organization_id
                                           AND rt2.shipment_line_id = rsl2.shipment_line_id
                                           AND rt2.transaction_type = 'RECEIVE')) rcv
                   -- 08/19/2013 CG: TMS 20130110-01638: Updated for Point (2) to pull true vendor
                   ,APPS.XXWC_INV_ITEM_SEARCH_VENDOR_V x1
             WHERE     msi.organization_id = mp.organization_id
                   AND msi.inventory_item_id = mic.inventory_item_id
                   AND msi.organization_id = mic.organization_id
                   AND mic.category_id = mc.category_id
                   --       And MC.structure_id = MCS.structure_id
                   AND mic.category_set_id = mcs.category_set_id
                   AND mcs.category_set_name = 'Inventory Category'
                   AND msi.inventory_item_id = moqd.inventory_item_id
                   AND msi.organization_id = moqd.organization_id
                   AND msi.inventory_item_id = cic.inventory_item_id
                   AND msi.organization_id = cic.organization_id
                   AND cic.cost_type_id = cct.cost_type_id
                   AND cct.cost_type = 'Average'
                   AND msi.inventory_item_id = rcv.item_id(+)
                   AND msi.organization_id = rcv.to_organization_id(+)
                   -- 08/19/2013 CG: TMS 20130110-01638: Updated for Point (2) to pull true vendor
                   -- AND rcv.vendor_id = ap.vendor_id(+)
                   -- AND NVL (ap.vendor_id, -99) = NVL (p_supplier_id, NVL (ap.vendor_id, -99))                -- supplier
                   AND msi.inventory_item_id = x1.item_id(+)
                   AND msi.organization_id = x1.organization_id(+)
                   AND x1.vendor_site_id(+) is not null
                   -- AND mp.organization_code = x1.dest_organization_code(+)
                   AND NVL(x1.vendor_id, -99) = NVL (p_supplier_id, NVL (x1.vendor_id, -99)) 
                   -- 08/19/2013 CG: TMS 20130110-01638: Updated for Point (2) to pull true vendor
                   AND mp.attribute9 = NVL (p_region, mp.attribute9)                                           -- region
                   AND mc.concatenated_segments = NVL (p_item_category, mc.concatenated_segments)            -- category
                   --And Nvl(MP.attribute12,'~') = Nvl(P_MARKET, Nvl(MP.attribute12,'~'))  -- market
                   AND mp.organization_id = NVL (p_branch, mp.organization_id)                                 -- branch
                   AND TRUNC (TRUNC (SYSDATE) - NVL (moqd.orig_date_received, (SYSDATE - 999))) BETWEEN NVL (p_days_low
                                                                                                            ,0)
                                                                                                    AND NVL (
                                                                                                            p_days_high
                                                                                                           ,999999)
            --       Order By MP.organization_code, MSI.segment1
            UNION ALL
            ----Intransit
            SELECT msib.inventory_item_id
                  ,msib.organization_id
		--Added source_vendor_name by Maha on 04/23/14 for ESMS#247548
		 , (SELECT cp_vendor_name_p(msib.inventory_item_id,msib.organization_id) FROM dual) source_vendor_name 
                  ,msib.segment1
                  ,msib.description
                  ,msib.shelf_life_days
                  ,msib.lot_control_code
                  ,mp_from.organization_code location
                  ,mp_from.attribute8 district
                  ,mp_from.attribute9 region
                  ,mp_from.attribute6 market
                  ,                                    --Decode(MC.segment2, '6000', 'Y', 'N') rebar, --removed 12/12/12
                   DECODE (mc.segment2
                          ,'6000', 'Y'
                          ,'6007', 'Y'
                          ,'6016', 'Y'
                          ,'6019', 'Y'
                          ,'6021', 'Y'
                          ,'6022', 'Y'
                          ,'6023', 'Y'
                          ,'N')
                       rebar
                  ,                                        --added 12/12/12 to include additional rebar category classes
                   mc.concatenated_segments item_category
                  -- 08/19/2013 CG: TMS 20130110-01638: Updated for Point (2) to pull true vendor
                  -- ,ap.segment1 supplier_number
                  -- ,ap.vendor_name supplier_name
                  ,x1.segment1 supplier_number
                  ,x1.vendor_name supplier_name
                  -- 08/19/2013 CG: TMS 20130110-01638: Updated for Point (2) to pull true vendor
                  ,xxwcinv_inv_aging_pkg.get_intransit_bod (
                       msib.inventory_item_id
                      ,rsl.from_organization_id
                      ,rsl.to_organization_id
                      ,rsh.receipt_source_code
                      ,NVL (rsl.quantity_shipped, 0) - NVL (rsl.quantity_received, 0))
                       orig_date_received
                  , (NVL (rsl.quantity_shipped, 0) - NVL (rsl.quantity_received, 0)) quantity
                  , (NVL (rsl.quantity_shipped, 0) - NVL (rsl.quantity_received, 0)) * cic.item_cost transaction_cost
                  ,'Intransit' material_location
                  ,'N' consigned_flag                                                                  --added 3/15/2013
              FROM rcv_shipment_headers rsh
                  ,rcv_shipment_lines rsl
                  ,mtl_system_items_b msib
                  ,mtl_parameters mp_from
                  ,mtl_item_categories mic
                  ,mtl_categories_kfv mc
                  ,mtl_category_sets_tl mcs
                  -- 08/19/2013 CG: TMS 20130110-01638: Updated for Point (2) to pull true vendor
                  -- ,ap_suppliers ap
                  ,cst_item_costs cic
                  ,cst_cost_types cct
                  ,(SELECT DISTINCT rsh.vendor_id, rsl.item_id, rsl.to_organization_id --Added Select Distinct LHS 06/21/2012
                      FROM rcv_shipment_headers rsh, rcv_shipment_lines rsl, rcv_transactions rt
                     WHERE     rsh.shipment_header_id = rsl.shipment_header_id
                           AND rsh.receipt_source_code = 'VENDOR'
                           AND rt.shipment_header_id = rsh.shipment_header_id
                           AND rt.shipment_line_id = rsl.shipment_line_id
                           AND rt.transaction_type = 'RECEIVE'
                           AND rt.transaction_date =
                                   (SELECT MAX (rt2.transaction_date)
                                      FROM rcv_shipment_lines rsl2, rcv_transactions rt2
                                     WHERE     rsl2.item_id = rsl.item_id
                                           AND rsl2.to_organization_id = rsl.to_organization_id
                                           AND rt2.shipment_line_id = rsl2.shipment_line_id
                                           AND rt2.transaction_type = 'RECEIVE')) rcv
                   -- 08/19/2013 CG: TMS 20130110-01638: Updated for Point (2) to pull true vendor
                   ,APPS.XXWC_INV_ITEM_SEARCH_VENDOR_V x1                   
             WHERE     rsh.receipt_source_code IN ('INVENTORY', 'INTERNAL ORDER')
                   AND rsh.shipment_header_id = rsl.shipment_header_id
                   AND rsl.item_id = msib.inventory_item_id
                   AND rsl.from_organization_id = msib.organization_id
                   AND rsl.from_organization_id = mp_from.organization_id
                   AND NVL (rsl.quantity_shipped, 0) - NVL (rsl.quantity_received, 0) > 0
                   AND msib.inventory_item_id = mic.inventory_item_id
                   AND msib.organization_id = mic.organization_id
                   AND mic.category_id = mc.category_id
                   AND mic.category_set_id = mcs.category_set_id
                   AND mcs.category_set_name = 'Inventory Category'
                   AND msib.inventory_item_id = rcv.item_id(+)
                   -- 08/19/2013 CG: TMS 20130110-01638: Updated for Point (2) to pull true vendor
                   -- AND rcv.vendor_id = ap.vendor_id(+)
                   -- AND NVL (ap.vendor_id, -99) = NVL (p_supplier_id, NVL (ap.vendor_id, -99))                -- supplier
                   AND msib.inventory_item_id = x1.item_id(+)
                   AND msib.organization_id = x1.organization_id(+)
                   AND x1.vendor_site_id(+) is not null
                   -- AND mp_from.organization_code = x1.dest_organization_code(+)
                   AND NVL(x1.vendor_id, -99) = NVL (p_supplier_id, NVL (x1.vendor_id, -99)) 
                   -- 08/19/2013 CG: TMS 20130110-01638: Updated for Point (2) to pull true vendor
                   AND msib.organization_id = rcv.to_organization_id(+)
                   AND cic.cost_type_id = cct.cost_type_id
                   AND cct.cost_type = 'Average'
                   AND cic.inventory_item_id = msib.inventory_item_id
                   AND cic.organization_id = msib.organization_id
                   -----Parameters
                   AND mp_from.attribute9 = NVL (p_region, mp_from.attribute9)                                 -- region
                   AND mc.concatenated_segments = NVL (p_item_category, mc.concatenated_segments)            -- category
                   --And Nvl(MP.attribute12,'~') = Nvl(P_MARKET, Nvl(MP.attribute12,'~'))  -- market
                   AND mp_from.organization_id = NVL (p_branch, mp_from.organization_id)                       -- branch
            UNION ALL
            ---Receiving
            SELECT msib.inventory_item_id
                  ,msib.organization_id
		 --Added source_vendor_name by Maha on 04/23/14 for ESMS#247548
		 , (SELECT cp_vendor_name_p(msib.inventory_item_id,msib.organization_id) FROM dual) source_vendor_name 
                  ,msib.segment1
                  ,msib.description
                  ,msib.shelf_life_days
                  ,msib.lot_control_code
                  ,mp.organization_code ship_from_branch_num
                  ,mp.attribute8 district
                  ,mp.attribute9 region
                  ,mp.attribute6 market
                  ,                                    --Decode(MC.segment2, '6000', 'Y', 'N') rebar, --removed 12/12/12
                   DECODE (mc.segment2
                          ,'6000', 'Y'
                          ,'6007', 'Y'
                          ,'6016', 'Y'
                          ,'6019', 'Y'
                          ,'6021', 'Y'
                          ,'6022', 'Y'
                          ,'6023', 'Y'
                          ,'N')
                       rebar
                  ,                                        --added 12/12/12 to include additional rebar category classes
                   mc.concatenated_segments item_category
                  -- 08/19/2013 CG: TMS 20130110-01638: Updated for Point (2) to pull true vendor
                  -- ,ap.segment1 supplier_number
                  -- ,ap.vendor_name supplier_name
                  ,x1.segment1 supplier_number
                  ,x1.vendor_name supplier_name
                  -- 08/19/2013 CG: TMS 20130110-01638: Updated for Point (2) to pull true vendor
                  ,SYSDATE
                  ,  (NVL (receive.quantity, 0) + NVL (correct.quantity, 0) - NVL (rtv.quantity, 0))
                   - (NVL (deliver.quantity, 0) + NVL (correct1.quantity, 0) - NVL (rtr.quantity, 0))
                       balance
                  ,  (  (NVL (receive.quantity, 0) + NVL (correct.quantity, 0) - NVL (rtv.quantity, 0))
                      - (NVL (deliver.quantity, 0) + NVL (correct1.quantity, 0) - NVL (rtr.quantity, 0)))
                   * rt.po_unit_price
                       cost
                  ,'Receiving' material_location
                  ,'N' consigned_flag                                                                  --added 3/15/2013
              FROM mtl_system_items_b msib
                  ,mtl_parameters mp
                  ,mtl_item_categories mic
                  ,mtl_categories_kfv mc
                  ,mtl_category_sets_tl mcs
                  ,cst_item_costs cic
                  ,cst_cost_types cct
                  -- 08/19/2013 CG: TMS 20130110-01638: Updated for Point (2) to pull true vendor
                  -- ,ap_suppliers ap
                  ,rcv_shipment_headers rsh
                  ,rcv_shipment_lines rsl
                  ,rcv_transactions rt
                  ,(  SELECT shipment_header_id, shipment_line_id, NVL (SUM (quantity), 0) quantity
                        FROM rcv_transactions
                       WHERE transaction_type = 'RECEIVE'
                    GROUP BY shipment_header_id, shipment_line_id) receive
                  ,(  SELECT shipment_header_id, shipment_line_id, NVL (SUM (quantity), 0) quantity
                        FROM rcv_transactions
                       WHERE transaction_type = 'DELIVER'
                    GROUP BY shipment_header_id, shipment_line_id) deliver
                  ,(  SELECT shipment_header_id, shipment_line_id, NVL (SUM (quantity), 0) quantity
                        FROM rcv_transactions
                       WHERE transaction_type = 'CORRECT' AND destination_type_code = 'RECEIVING'
                    GROUP BY shipment_header_id, shipment_line_id) correct
                  ,(  SELECT shipment_header_id, shipment_line_id, NVL (SUM (quantity), 0) quantity
                        FROM rcv_transactions
                       WHERE transaction_type = 'CORRECT' AND destination_type_code = 'INVENTORY'
                    GROUP BY shipment_header_id, shipment_line_id) correct1
                  ,(  SELECT shipment_header_id, shipment_line_id, NVL (SUM (quantity), 0) quantity
                        FROM rcv_transactions
                       WHERE transaction_type = 'RETURN TO VENDOR'
                    GROUP BY shipment_header_id, shipment_line_id) rtv
                  ,(  SELECT shipment_header_id, shipment_line_id, NVL (SUM (quantity), 0) quantity
                        FROM rcv_transactions
                       WHERE transaction_type = 'RETURN TO RECEIVING'
                    GROUP BY shipment_header_id, shipment_line_id) rtr
                  -- 08/19/2013 CG: TMS 20130110-01638: Updated for Point (2) to pull true vendor
                  ,APPS.XXWC_INV_ITEM_SEARCH_VENDOR_V x1
             WHERE     rsh.receipt_source_code = 'VENDOR'
                   AND rt.transaction_type = 'RECEIVE'
                   AND rsh.shipment_header_id = rsl.shipment_header_id
                   AND rsl.shipment_header_id = rt.shipment_header_id
                   AND rsl.shipment_line_id = rt.shipment_line_id
                   AND rt.shipment_header_id = receive.shipment_header_id(+)
                   AND rt.shipment_line_id = receive.shipment_line_id(+)
                   AND rt.shipment_header_id = deliver.shipment_header_id(+)
                   AND rt.shipment_line_id = deliver.shipment_line_id(+)
                   AND rt.shipment_header_id = correct.shipment_header_id(+)
                   AND rt.shipment_line_id = correct.shipment_line_id(+)
                   AND rt.shipment_header_id = correct1.shipment_header_id(+)
                   AND rt.shipment_line_id = correct1.shipment_line_id(+)
                   AND rt.shipment_header_id = rtv.shipment_header_id(+)
                   AND rt.shipment_line_id = rtv.shipment_line_id(+)
                   AND rt.shipment_header_id = rtr.shipment_header_id(+)
                   AND rt.shipment_line_id = rtr.shipment_line_id(+)
                   AND rsl.item_id = msib.inventory_item_id
                   AND rsl.to_organization_id = msib.organization_id
                   AND rsl.to_organization_id = mp.organization_id
                   -- 08/19/2013 CG: TMS 20130110-01638: Updated for Point (2) to pull true vendor
                   -- AND rsh.vendor_id = ap.vendor_id(+)
                   -- AND NVL (ap.vendor_id, -99) = NVL (p_supplier_id, NVL (ap.vendor_id, -99))                -- supplier
                   AND msib.inventory_item_id = x1.item_id(+)
                   AND msib.organization_id = x1.organization_id(+)
                   AND x1.vendor_site_id(+) is not null
                   -- AND mp.organization_code = x1.dest_organization_code(+)
                   AND NVL(x1.vendor_id, -99) = NVL (p_supplier_id, NVL (x1.vendor_id, -99)) 
                   -- 08/19/2013 CG: TMS 20130110-01638: Updated for Point (2) to pull true vendor
                   --and    nvl(rsl.quantity_shipped,0) - nvl(rsl.quantity_received,0) > 0
                   AND msib.inventory_item_id = mic.inventory_item_id
                   AND msib.organization_id = mic.organization_id
                   AND mic.category_id = mc.category_id
                   AND mic.category_set_id = mcs.category_set_id
                   AND mcs.category_set_name = 'Inventory Category'
                   AND cic.cost_type_id = cct.cost_type_id
                   AND cct.cost_type = 'Average'
                   AND cic.inventory_item_id = msib.inventory_item_id
                   AND cic.organization_id = msib.organization_id
                   AND   (NVL (receive.quantity, 0) + NVL (correct.quantity, 0) - NVL (rtv.quantity, 0))
                       - (NVL (deliver.quantity, 0) + NVL (correct1.quantity, 0) - NVL (rtr.quantity, 0)) > 0 -----Parameters
                   AND mp.attribute9 = NVL (p_region, mp.attribute9)                                           -- region
                   AND mc.concatenated_segments = NVL (p_item_category, mc.concatenated_segments)            -- category
                   --And Nvl(MP.attribute12,'~') = Nvl(P_MARKET, Nvl(MP.attribute12,'~'))  -- market
                   AND mp.organization_id = NVL (p_branch, mp.organization_id);                                 -- branc
 --Commented below cursor by Maha on 04/23/14 for ESMS#247548
     /*   CURSOR c_sourcing (
            p_item_id    NUMBER
           ,p_org_id     NUMBER)
        IS
              SELECT DECODE (sso.source_type,  3, sso.vendor_name,  1, sso.source_organization_code) vendor_name -- 1=transfer-from  3=buy-from
                FROM mrp_sourcing_rules sr
                    ,mrp_sr_receipt_org_v sro
                    ,mrp_sr_source_org_v sso
                    ,mrp_sr_assignments_v sra
                    ,mrp_assignment_sets mas
               WHERE     sr.sourcing_rule_id = sro.sourcing_rule_id
                     AND TRUNC (SYSDATE) BETWEEN sro.effective_date AND NVL (disable_date, SYSDATE + 1)
                     AND sro.sr_receipt_id = sso.sr_receipt_id
                     AND sra.sourcing_rule_id = sr.sourcing_rule_id
                     AND sra.assignment_set_id = mas.assignment_set_id
                     AND mas.assignment_set_name = 'WC Default'
                     AND sra.inventory_item_id = p_item_id
                     AND (   (sra.organization_id = p_org_id AND sra.assignment_type = 6)
                          OR                                                                       -- 3=item  6=item/org
                             (sra.organization_id IS NULL AND sra.assignment_type = 3))
            ORDER BY sso.allocation_percent DESC, sso.RANK;*/

        l_source_vendor          VARCHAR2 (80);
        l_reserve_group          VARCHAR2 (40);

        l_aging_days             NUMBER := 0;

        l_90_qty                 NUMBER := 0;
        l_180_qty                NUMBER := 0;
        l_270_qty                NUMBER := 0;
        l_365_qty                NUMBER := 0;
        l_630_qty                NUMBER := 0;
        l_730_qty                NUMBER := 0;
        l_over_730_qty           NUMBER := 0;
        l_undetermined_qty       NUMBER := 0;

        l_90_amt                 NUMBER := 0;
        l_180_amt                NUMBER := 0;
        l_270_amt                NUMBER := 0;
        l_365_amt                NUMBER := 0;
        l_630_amt                NUMBER := 0;
        l_730_amt                NUMBER := 0;
        l_over_730_amt           NUMBER := 0;
        l_undetermined_amt       NUMBER := 0;

        l_90_rsv                 NUMBER := 0;
        l_180_rsv                NUMBER := 0;
        l_270_rsv                NUMBER := 0;
        l_365_rsv                NUMBER := 0;
        l_630_rsv                NUMBER := 0;
        l_730_rsv                NUMBER := 0;
        l_over_730_rsv           NUMBER := 0;
        l_undetermined_rsv       NUMBER := 0;

        l_old_item_id            NUMBER;
        l_old_organization_id    NUMBER;
        l_old_item_number        VARCHAR2 (80);
        l_old_item_description   VARCHAR2 (2000);
        l_old_location           VARCHAR2 (3);
        l_old_district           VARCHAR2 (80);
        l_old_item_category      VARCHAR2 (80);
        l_old_reserve_type       VARCHAR2 (80);
        l_old_supplier_number    VARCHAR2 (20);
        l_old_supplier_name      VARCHAR2 (2000);
        l_old_source_vendor      VARCHAR2 (2000);
        l_old_rebar              VARCHAR2 (1);
        l_old_market             VARCHAR2 (80);

        l_material_location      VARCHAR2 (15);

        first_time               BOOLEAN := TRUE;
        l_cnt                    NUMBER := 0;

        l_consigned_flag         VARCHAR2 (1);                                                         --added 3/15/2013
        
        l_time_sensitive         VARCHAR2(3); --Added 8/26/2013
        l_special                VARCHAR2(3); --Added 8/26/2013
        
    BEGIN
        l_module := 'BeforeReport';
        l_err_callpoint := 'Start';

        fnd_file.put_line (fnd_file.LOG, l_cnt || ' records processed');
        fnd_file.put_line (fnd_file.LOG, 'P_REGION = ' || p_region);
        fnd_file.put_line (fnd_file.LOG, 'P_LOCATION = ' || p_branch);
        --FND_FILE.Put_LIne(FND_FILE.Log,'P_MARKET = '||P_MARKET);
        fnd_file.put_line (fnd_file.LOG, 'P_ITEM_CATEGORY = ' || p_item_category);
        fnd_file.put_line (fnd_file.LOG, 'P_RESERVE_GROUP = ' || p_reserve_group);
        fnd_file.put_line (fnd_file.LOG, 'P_SUPPLIER_ID = ' || p_supplier_id);
        fnd_file.put_line (fnd_file.LOG, 'P_DAYS_LOW = ' || p_days_low);
        fnd_file.put_line (fnd_file.LOG, 'P_DAYS_HIGH = ' || p_days_high);

        FOR r1 IN c1
        LOOP
            l_err_callpoint := 'Top of Loop';
            l_cnt := l_cnt + 1;

            l_err_callpoint := 'Before Sourcing Rule cursor';

 --Commented below block by Maha on 04/23/14 for ESMS#247548

           /* OPEN c_sourcing (r1.inventory_item_id, r1.organization_id);

            FETCH c_sourcing INTO l_source_vendor;

            IF c_sourcing%NOTFOUND
            THEN
                l_source_vendor := NULL;
            END IF;

            CLOSE c_sourcing;*/

            l_err_callpoint := 'Before reserve group formula';

            --TMS 20130827-00676 Removed 8/26/2013 for new aging logic to get Time Sensitive and Specials
          /*  -- determine reserve group based upo following rules
            IF r1.shelf_life_days BETWEEN 1 AND 365 OR r1.lot_control_code != 1
            THEN
                l_reserve_group := 'CHEMICALS/TIME SENSITIVE';
            ELSIF SUBSTR (r1.item_number, 1, 2) = 'SP'
            THEN
                l_reserve_group := 'SPECIALS';
            ELSE
                l_reserve_group := 'ALL OTHER INVENTORY';
            END IF;
          */
          
            --TMS 20130827-00676 Added 8/26/2013 for new aging logic to get Time Sensitive and Specials
            
            l_time_sensitive := XXWCINV_INV_AGING_PKG.CHECK_TIME_SENSITIVE
                             (p_inventory_item_id => r1.inventory_item_id
                             ,p_organization_id => r1.organization_id);
          
          
            l_special := XXWCINV_INV_AGING_PKG.CHECK_SPECIAL
                             (p_inventory_item_id => r1.inventory_item_id
                             ,p_organization_id => r1.organization_id);
          
            
            IF l_time_sensitive = 'Yes'
            THEN
                l_reserve_group := 'CHEMICALS/TIME SENSITIVE';
            ELSIF l_special = 'Yes' 
            THEN
                l_reserve_group := 'SPECIALS';
            ELSE
                l_reserve_group := 'ALL OTHER INVENTORY';
            END IF;
                       
            --TMS 20130827-00676 end Added 8/26/2013 for update Time Sensitve and Special Logic
            
            l_err_callpoint := 'Before first-pass check';

            -- on first pass set all old variables
            IF first_time
            THEN
                fnd_file.put_line (fnd_file.LOG, 'Processing FIRST_TIME logic');
                first_time := FALSE;
                l_old_item_id := r1.inventory_item_id;
                l_old_organization_id := r1.organization_id;
                l_old_item_number := r1.item_number;
                l_old_item_description := r1.item_description;
                l_old_location := r1.location;
                l_old_district := r1.district;
                l_old_item_category := r1.item_category;
                l_old_reserve_type := l_reserve_group;
                l_old_supplier_number := r1.supplier_number;
                l_old_supplier_name := r1.supplier_name;
  --            l_old_source_vendor := l_source_vendor;        --Commented and added below by Maha on 04/23/14 for ESMS#247548
		l_old_source_vendor :=  r1.source_vendor_name;
                l_old_rebar := r1.rebar;
                l_old_market := r1.market;
                l_material_location := r1.material_location;
                l_consigned_flag := r1.consigned_flag;                                                 --added 3/15/2013
            END IF;

            l_err_callpoint := 'Before change in control fields';

            -- check to see if any control fields changed
            IF    l_old_item_number != r1.item_number
               OR l_old_location != r1.location
               OR l_old_supplier_number != r1.supplier_number
            THEN
                l_err_callpoint := 'Before reserve limit check';
                -- Satish U: 06-MAR-2012  Commenting out follwing lgogic as  P_REV_AMT_LIMIT is not more a parameter
                -- If (l_90_rsv + l_180_rsv + l_270_rsv + l_365_rsv + l_630_rsv + l_730_rsv + l_over_730_rsv + l_undetermined_rsv) >= P_RSV_AMT_LIMIT Then
                --      FND_FILE.Put_Line(FND_FILE.Log,'Writing temp record for '||l_old_item_number||' '||l_old_location||' '||l_old_supplier_number);
                l_err_callpoint := 'Before insert into XXWCINV_INV_AGING_TEMP';

                INSERT INTO xxwc.xxwcinv_inv_aging_temp (location
                                                        ,district
                                                        ,market
                                                        ,part_number
                                                        ,reserve_group
                                                        ,rebar
                                                        ,supplier_number
                                                        ,supplier_name
                                                        ,source_location
                                                        ,part_description
                                                        ,category_class
                                                        ,quantity_90_days
                                                        ,quantity_180_days
                                                        ,quantity_270_days
                                                        ,quantity_365_days
                                                        ,quantity_630_days
                                                        ,quantity_730_days
                                                        ,quantity_over_730_days
                                                        ,quantity_undet_days
                                                        ,amount_90_days
                                                        ,amount_180_days
                                                        ,amount_270_days
                                                        ,amount_365_days
                                                        ,amount_630_days
                                                        ,amount_730_days
                                                        ,amount_over_730_days
                                                        ,amount_undet_days
                                                        ,reserve_90_days
                                                        ,reserve_180_days
                                                        ,reserve_270_days
                                                        ,reserve_365_days
                                                        ,reserve_630_days
                                                        ,reserve_730_days
                                                        ,reserve_over_730_days
                                                        ,reserve_undet_days
                                                        ,material_location                              --added 12/12/12
                                                        ,consigned_flag                                --added 3/15/2013
                                                                       )
                     VALUES (l_old_location                                                                  -- location
                            ,l_old_district                                                                  -- district
                            ,l_old_market                                                                      -- market
                            ,l_old_item_number                                                            -- part_number
                            ,l_old_reserve_type                                                         -- reserve_group
                            ,l_old_rebar                                                                        -- rebar
                            ,l_old_supplier_number                                                    -- supplier_number
                            ,l_old_supplier_name                                                        -- supplier_name
                            ,l_old_source_vendor                                                      -- source_location
                            ,l_old_item_description                                                  -- part_description
                            ,l_old_item_category                                                       -- category_class
                            ,l_90_qty                                                                  -- 90day_quantity
                            ,l_180_qty                                                                -- 180day_quantity
                            ,l_270_qty                                                                -- 270day_quantity
                            ,l_365_qty                                                                -- 365day_quantity
                            ,l_630_qty                                                                -- 630day_quantity
                            ,l_730_qty                                                                -- 730day_quantity
                            ,l_over_730_qty                                                      -- over_730day_quantity
                            ,l_undetermined_qty                                                        -- undet_quantity
                            ,l_90_amt                                                                    -- 90day_amount
                            ,l_180_amt                                                                  -- 180day_amount
                            ,l_270_amt                                                                  -- 270day_amount
                            ,l_365_amt                                                                  -- 365day_amount
                            ,l_630_amt                                                                  -- 630day_amount
                            ,l_730_amt                                                                  -- 730day_amount
                            ,l_over_730_amt                                                        -- over_730day_amount
                            ,l_undetermined_amt                                                          -- undet_amount
                            ,l_90_rsv                                                                   -- 90day_reserve
                            ,l_180_rsv                                                                 -- 180day_reserve
                            ,l_270_rsv                                                                 -- 270day_reserve
                            ,l_365_rsv                                                                 -- 365day_reserve
                            ,l_630_rsv                                                                 -- 630day_reserve
                            ,l_730_rsv                                                                 -- 730day_reserve
                            ,l_over_730_rsv                                                       -- over_730day_reserve
                            ,l_undetermined_rsv                                                         -- undet_reserve
                            ,l_material_location                                     -- added 12/12/12 material_location
                            ,l_consigned_flag                                          -- added 3/15/2013 consigned_flag
                                             );

                --End If;
                COMMIT;

                l_err_callpoint := 'Before setting control variables';
                -- set old variables
                l_old_item_id := r1.inventory_item_id;
                l_old_organization_id := r1.organization_id;
                l_old_item_number := r1.item_number;
                l_old_item_description := r1.item_description;
                l_old_location := r1.location;
                l_old_district := r1.district;
                l_old_item_category := r1.item_category;
                l_old_reserve_type := l_reserve_group;
                l_old_supplier_number := r1.supplier_number;
                l_old_supplier_name := r1.supplier_name;
--              l_old_source_vendor := l_source_vendor;		--Commented and added below by Maha on 04/23/14 for ESMS#247548
		l_old_source_vendor := r1.source_vendor_name;
                l_old_rebar := r1.rebar;
                l_old_market := r1.market;
                l_material_location := r1.material_location;
                l_consigned_flag := r1.consigned_flag;                                                 --added 3/15/2013

                l_err_callpoint := 'Before resetting computation variables';
                -- reset computation variables
                l_90_qty := 0;
                l_180_qty := 0;
                l_270_qty := 0;
                l_365_qty := 0;
                l_630_qty := 0;
                l_730_qty := 0;
                l_over_730_qty := 0;
                l_undetermined_qty := 0;

                l_90_amt := 0;
                l_180_amt := 0;
                l_270_amt := 0;
                l_365_amt := 0;
                l_630_amt := 0;
                l_730_amt := 0;
                l_over_730_amt := 0;
                l_undetermined_amt := 0;

                l_90_rsv := 0;
                l_180_rsv := 0;
                l_270_rsv := 0;
                l_365_rsv := 0;
                l_630_rsv := 0;
                l_730_rsv := 0;
                l_over_730_rsv := 0;
                l_undetermined_rsv := 0;
            END IF;

            l_err_callpoint := 'Before checking aging days';

            IF r1.orig_date_received IS NULL
            THEN
                l_aging_days := -1;
            ELSE
                l_aging_days := TRUNC (SYSDATE - r1.orig_date_received);
            END IF;

            l_err_callpoint := 'Before computing bucket qty/values/reserve amt';

            -- populate buckets based upon days since receipt
            -- 08/19/2013 CG: TMS 20130110-01638: Updated for point (1), moving intransits from undetermined to current bucket 
            -- IF l_aging_days < 0
            IF l_aging_days < 0 and l_material_location != 'Intransit'
            THEN
                l_undetermined_qty := l_undetermined_qty + r1.primary_transaction_quantity;
                l_undetermined_amt := l_undetermined_amt + r1.item_cost;
                l_undetermined_rsv := l_undetermined_rsv + get_reserve_amt (l_reserve_group, NULL, r1.item_cost);
            ELSIF (l_aging_days < 90
            -- 08/19/2013 CG: TMS 20130110-01638: Updated for point (1), moving intransits from undetermined to current bucket
                   OR 
                   (l_aging_days < 0 and l_material_location = 'Intransit')
                  ) 
            THEN
                l_90_qty := l_90_qty + r1.primary_transaction_quantity;
                l_90_amt := l_90_amt + r1.item_cost;
                -- 08/19/2013 CG: TMS 20130110-01638: Updated for point (1), moving intransits from undetermined to current bucket
                IF l_aging_days < 0 and l_material_location = 'Intransit' THEN
                    l_90_rsv := l_90_rsv + get_reserve_amt (l_reserve_group, NULL, r1.item_cost);
                ELSE
                    l_90_rsv := l_90_rsv + get_reserve_amt (l_reserve_group, l_aging_days, r1.item_cost);
                END IF;
            ELSIF l_aging_days < 180
            THEN
                l_180_qty := l_180_qty + r1.primary_transaction_quantity;
                l_180_amt := l_180_amt + r1.item_cost;
                l_180_rsv := l_180_rsv + get_reserve_amt (l_reserve_group, l_aging_days, r1.item_cost);
            ELSIF l_aging_days < 270
            THEN
                l_270_qty := l_270_qty + r1.primary_transaction_quantity;
                l_270_amt := l_270_amt + r1.item_cost;
                l_270_rsv := l_270_rsv + get_reserve_amt (l_reserve_group, l_aging_days, r1.item_cost);
            ELSIF l_aging_days < 365
            THEN
                l_365_qty := l_365_qty + r1.primary_transaction_quantity;
                l_365_amt := l_365_amt + r1.item_cost;
                l_365_rsv := l_365_rsv + get_reserve_amt (l_reserve_group, l_aging_days, r1.item_cost);
            ELSIF l_aging_days < 630
            THEN
                l_630_qty := l_630_qty + r1.primary_transaction_quantity;
                l_630_amt := l_630_amt + r1.item_cost;
                l_630_rsv := l_630_rsv + get_reserve_amt (l_reserve_group, l_aging_days, r1.item_cost);
            ELSIF l_aging_days < 730
            THEN
                l_730_qty := l_730_qty + r1.primary_transaction_quantity;
                l_730_amt := l_730_amt + r1.item_cost;
                l_730_rsv := l_730_rsv + get_reserve_amt (l_reserve_group, l_aging_days, r1.item_cost);
            ELSE
                l_over_730_qty := l_over_730_qty + r1.primary_transaction_quantity;
                l_over_730_amt := l_over_730_amt + r1.item_cost;
                l_over_730_rsv := l_over_730_rsv + get_reserve_amt (l_reserve_group, l_aging_days, r1.item_cost);
            END IF;

            l_err_callpoint := 'Before end of loop';
        END LOOP;

        -- after last record, so generate final insert
        l_err_callpoint := 'After loop';

        IF l_cnt > 0
        THEN
            BEGIN
                l_err_callpoint := 'Before final reserve amount limit check';
                -- Satish U: 06-MAR-2012  Commenting out follwing lgogic as  P_REV_AMT_LIMIT is not more a parameter
                --If (l_90_rsv + l_180_rsv + l_270_rsv + l_365_rsv + l_630_rsv + l_730_rsv + l_over_730_rsv + l_undetermined_rsv) >= P_RSV_AMT_LIMIT Then
                l_err_callpoint := 'Before final insert into XXWCINV_INV_AGING_TEMP';

                INSERT INTO xxwc.xxwcinv_inv_aging_temp (location
                                                        ,district
                                                        ,market
                                                        ,part_number
                                                        ,reserve_group
                                                        ,rebar
                                                        ,supplier_number
                                                        ,supplier_name
                                                        ,source_location
                                                        ,part_description
                                                        ,category_class
                                                        ,quantity_90_days
                                                        ,quantity_180_days
                                                        ,quantity_270_days
                                                        ,quantity_365_days
                                                        ,quantity_630_days
                                                        ,quantity_730_days
                                                        ,quantity_over_730_days
                                                        ,quantity_undet_days
                                                        ,amount_90_days
                                                        ,amount_180_days
                                                        ,amount_270_days
                                                        ,amount_365_days
                                                        ,amount_630_days
                                                        ,amount_730_days
                                                        ,amount_over_730_days
                                                        ,amount_undet_days
                                                        ,reserve_90_days
                                                        ,reserve_180_days
                                                        ,reserve_270_days
                                                        ,reserve_365_days
                                                        ,reserve_630_days
                                                        ,reserve_730_days
                                                        ,reserve_over_730_days
                                                        ,reserve_undet_days
                                                        ,material_location                              --Added 12/12/12
                                                        ,consigned_flag                 --Added 3/15/2013 20130312-01176
                                                                       )
                     VALUES (l_old_location                                                                  -- location
                            ,l_old_district                                                                  -- district
                            ,l_old_market                                                                      -- market
                            ,l_old_item_number                                                            -- part_number
                            ,l_old_reserve_type                                                         -- reserve_group
                            ,l_old_rebar                                                                        -- rebar
                            ,l_old_supplier_number                                                    -- supplier_number
                            ,l_old_supplier_name                                                        -- supplier_name
                            ,l_old_source_vendor                                                      -- source_location
                            ,l_old_item_description                                                  -- part_description
                            ,l_old_item_category                                                       -- category_class
                            ,l_90_qty                                                                  -- 90day_quantity
                            ,l_180_qty                                                                -- 180day_quantity
                            ,l_270_qty                                                                -- 270day_quantity
                            ,l_365_qty                                                                -- 365day_quantity
                            ,l_630_qty                                                                -- 630day_quantity
                            ,l_730_qty                                                                -- 730day_quantity
                            ,l_over_730_qty                                                      -- over_730day_quantity
                            ,l_undetermined_qty                                                        -- undet_quantity
                            ,l_90_amt                                                                    -- 90day_amount
                            ,l_180_amt                                                                  -- 180day_amount
                            ,l_270_amt                                                                  -- 270day_amount
                            ,l_365_amt                                                                  -- 365day_amount
                            ,l_630_amt                                                                  -- 630day_amount
                            ,l_730_amt                                                                  -- 730day_amount
                            ,l_over_730_amt                                                        -- over_730day_amount
                            ,l_undetermined_amt                                                          -- undet_amount
                            ,l_90_rsv                                                                   -- 90day_reserve
                            ,l_180_rsv                                                                 -- 180day_reserve
                            ,l_270_rsv                                                                 -- 270day_reserve
                            ,l_365_rsv                                                                 -- 365day_reserve
                            ,l_630_rsv                                                                 -- 630day_reserve
                            ,l_730_rsv                                                                 -- 730day_reserve
                            ,l_over_730_rsv                                                       -- over_730day_reserve
                            ,l_undetermined_rsv                                                         -- undet_reserve
                            ,l_material_location                                     -- added 12/12/12 material_location
                            ,l_consigned_flag                           -- added 3/15/2013 consigned_flag 20130312-01176
                                             );

                --End If;
                COMMIT;
            END;
        END IF;

        l_err_callpoint := 'At End of BeforeReport';
        RETURN TRUE;
    EXCEPTION
        WHEN OTHERS
        THEN
            ROLLBACK;
            l_err_msg := 'Error in ' || l_err_callpoint || ' WITH  ' || SUBSTR (SQLERRM, 1, 1000);
            --dbms_output.put_line(l_fulltext);
            fnd_file.put_line (fnd_file.LOG, l_err_msg);
            fnd_file.put_line (fnd_file.output, l_err_msg);

            xxcus_error_pkg.xxcus_error_main_api (
                p_called_from         => l_err_callfrom
               ,p_calling             => l_err_callpoint
               ,p_ora_error_msg       => SQLERRM
               ,p_error_desc          => 'Error running XXWCINV_INV_AGING_PKG package with PROGRAM ERROR'
               ,p_distribution_list   => l_distro_list
               ,p_module              => l_module);
            RETURN FALSE;
    END beforereport;

    ------------------------------------------------------------------
    -- performs clean-up on temp table after report completes
    ------------------------------------------------------------------
    FUNCTION afterreport
        RETURN BOOLEAN
    IS
    BEGIN
        l_module := 'AfterReport';
        l_err_callpoint := 'Start';

        DELETE FROM xxwc.xxwcinv_inv_aging_temp;

        COMMIT;
        RETURN TRUE;
    EXCEPTION
        WHEN OTHERS
        THEN
            RAISE PROGRAM_ERROR;
    END afterreport;

    /***************************************************************************************/
    /* Update_Orig_Date_Rcvd : procedure to update the orig_date_received column on new    */
    /*                  onhand detail records created by intercompany receipt transactions */
    /***************************************************************************************/
    PROCEDURE update_orig_date_rcvd (errbuf             OUT VARCHAR2
                                    ,retcode            OUT VARCHAR2
                                    ,p_trx_id        IN     NUMBER
                                    ,p_xfer_trx_id   IN     NUMBER)
    IS
    
       
    BEGIN
        
        
            update_orig_date_rcvd_api (p_trx_id, p_xfer_trx_id);
        
       
        errbuf := 'Success';
        retcode := 0;

        COMMIT;
        
    EXCEPTION
        WHEN xxwc_error
        THEN
            errbuf := 'Processing Error';
            retcode := 2;
            ROLLBACK;
        WHEN OTHERS
        THEN
            errbuf := SQLERRM;
            retcode := 2;
            ROLLBACK;
    END update_orig_date_rcvd;

    /***************************************************************************************/
    /* Purge_Interco : procedure to clean-out the XXWC.XXWCINV_INTERCO_TRANSFERS custom table */
    /*                  for records that are not associated with intercompany sales orders */
    /***************************************************************************************/
    PROCEDURE purge_interco (errbuf OUT VARCHAR2, retcode OUT VARCHAR2, p_days IN NUMBER)
    AS
        l_cnt   NUMBER;
    BEGIN
        l_module := 'Purge_Interco';
        l_err_callpoint := 'Start';

        fnd_file.put_line (fnd_file.LOG
                          ,'Purging stagnent records from XXWC.XXWCINV_INTERCO_TRANSFERS older than ' || p_days);

        DELETE FROM xxwc.xxwcinv_interco_transfers
              WHERE transaction_id IS NULL AND TRUNC (creation_date) < TRUNC (SYSDATE) - p_days;

        l_err_callpoint := 'After Delete';
        l_cnt := SQL%ROWCOUNT;

        fnd_file.put_line (fnd_file.LOG, l_cnt || ' records deleted...');
        l_err_callpoint := 'At End';

        errbuf := 'Success';
        retcode := 0;
    EXCEPTION
        WHEN OTHERS
        THEN
            errbuf := SQLERRM;
            retcode := 2;
            ROLLBACK;
            l_err_msg := 'Error in ' || l_err_callpoint || ' WITH  ' || SUBSTR (SQLERRM, 1, 1000);
            --dbms_output.put_line(l_fulltext);
            fnd_file.put_line (fnd_file.LOG, l_err_msg);
            fnd_file.put_line (fnd_file.output, l_err_msg);

            xxcus_error_pkg.xxcus_error_main_api (
                p_called_from         => l_err_callfrom
               ,p_calling             => l_err_callpoint
               ,p_ora_error_msg       => SQLERRM
               ,p_error_desc          => 'Error running XXWCINV_INV_AGING_PKG package with PROGRAM ERROR'
               ,p_distribution_list   => l_distro_list
               ,p_module              => l_module);
    END purge_interco;


/***************************************************************************
    $Header XXWCINV_INV_AGING_PKG.pkb $
    Module Name: XXWCINV_INV_AGING_PKG.generate_csv
    PURPOSE: Procedure used for Aging Report

    REVISIONS:
    Ver    Date           Author                Description
    ------ ---------      ------------------    ----------------
    1.0    Unknown        Unknown               Initial Version
 -- There are multiple versions in SVN. To Follow the standards adding the history from Version 2.0
    2.0    19-Jun-2015    Manjula Chellappan    TMS# 20150618-00095 Inventory Aging Report - Redesign concurrent program
****************************************************************************/
   PROCEDURE generate_csv (errbuf               OUT VARCHAR2,
                           retcode              OUT VARCHAR2,
                           p_region          IN     VARCHAR2,
                           p_branch          IN     NUMBER, --p_market           IN   VARCHAR2, -- Satish U: 06-MAR-2012
                           p_item_category   IN     VARCHAR2,
                           p_reserve_group   IN     VARCHAR2,
                           p_supplier_id     IN     NUMBER,
                           p_days_low        IN     NUMBER,
                           p_days_high       IN     NUMBER,
                           p_ftp_flag        IN     VARCHAR2 -- Added for Revision 2.0
                                                            )
   IS
      --p_rsv_amt_limit    IN   NUMBER) Is  -- Satish U: 06-MAR-2012

      CURSOR c1
      IS
           SELECT location,
                  district,
                  market,
                  part_number,
                  reserve_group,
                  rebar,
                  consigned_flag              --added 3/15/2013 20130312-01176
                                ,
                  supplier_number,
                  supplier_name,
                  source_location,
                  part_description,
                  category_class,
                  material_location                           --added 12/12/12
                                   ,
                  SUM (quantity_90_days) quantity_90_days,
                  SUM (quantity_180_days) quantity_180_days,
                  SUM (quantity_270_days) quantity_270_days,
                  SUM (quantity_365_days) quantity_365_days,
                  SUM (quantity_630_days) quantity_630_days,
                  SUM (quantity_730_days) quantity_730_days,
                  SUM (quantity_over_730_days) quantity_over_730_days,
                  SUM (quantity_undet_days) quantity_undet_days,
                  SUM (
                       quantity_90_days
                     + quantity_180_days
                     + quantity_270_days
                     + quantity_365_days
                     + quantity_630_days
                     + quantity_730_days
                     + quantity_over_730_days
                     + quantity_undet_days)
                     total_oh,
                  SUM (amount_90_days) amount_90_days,
                  SUM (amount_180_days) amount_180_days,
                  SUM (amount_270_days) amount_270_days,
                  SUM (amount_365_days) amount_365_days,
                  SUM (amount_630_days) amount_630_days,
                  SUM (amount_730_days) amount_730_days,
                  SUM (amount_over_730_days) amount_over_730_days,
                  SUM (amount_undet_days) amount_undet_days,
                  SUM (
                       amount_90_days
                     + amount_180_days
                     + amount_270_days
                     + amount_365_days
                     + amount_630_days
                     + amount_730_days
                     + amount_over_730_days
                     + amount_undet_days)
                     total_amount
             -- ,Sum(reserve_90_days) reserve_90_days
             -- ,Sum(reserve_180_days) reserve_180_days
             -- ,Sum(reserve_270_days) reserve_270_days
             -- ,Sum(reserve_365_days) reserve_365_days
             -- ,Sum(reserve_630_days) reserve_630_days
             -- ,Sum(reserve_730_days) reserve_730_days
             -- ,Sum(reserve_over_730_days) reserve_over_730_days
             -- ,Sum(reserve_undet_days) reserve_undet_days
             -- ,Sum(reserve_90_days + reserve_180_days + reserve_270_days + reserve_365_days + reserve_630_days +
             --      reserve_730_days + reserve_over_730_days + reserve_undet_days) total_reserve
             -- ,Sum(reserve_365_days + reserve_630_days + reserve_730_days + reserve_over_730_days + reserve_undet_days) over365_reserve
             -- ,Decode(Sum(reserve_90_days + reserve_180_days + reserve_270_days + reserve_365_days + reserve_630_days + reserve_730_days + reserve_over_730_days + reserve_undet_days), 0, 0,
             --      Round(Sum(amount_90_days + amount_180_days + amount_270_days + amount_365_days + amount_630_days + amount_730_days + amount_over_730_days + amount_undet_days) /
             --        Sum(reserve_90_days + reserve_180_days + reserve_270_days + reserve_365_days + reserve_630_days + reserve_730_days + reserve_over_730_days + reserve_undet_days), 2)) combined_total_reserve
             FROM xxwc.xxwcinv_inv_aging_temp
            WHERE reserve_group = NVL (p_reserve_group, reserve_group) ---Added to Filter for P_RESERVE_GROUP Parameter LHS 5/25/2012
         GROUP BY location,
                  district,
                  market,
                  part_number,
                  reserve_group,
                  rebar,
                  supplier_number,
                  supplier_name,
                  source_location,
                  part_description,
                  category_class,
                  material_location                           --added 12/12/12
                                   ,
                  consigned_flag              --added 3/15/2013 20130312-01176
         ORDER BY 1,
                  2,
                  3,
                  4,
                  5,
                  6;

      l_cnt             NUMBER := 0;

      -- Added for Revision 2.0
      l_instance_name   VARCHAR2 (20);
      l_file_name       VARCHAR2 (100);
      l_file_dir        VARCHAR2 (100);
      l_file_handle     UTL_FILE.file_type;
   BEGIN
      l_module := 'Generate_CSV';
      l_err_callpoint := 'Start';

      xxwcinv_inv_aging_pkg.p_region := p_region;
      xxwcinv_inv_aging_pkg.p_branch := p_branch;
      --XXWCINV_INV_AGING_PKG.P_MARKET := p_market;
      xxwcinv_inv_aging_pkg.p_item_category := p_item_category;
      xxwcinv_inv_aging_pkg.p_supplier_id := p_supplier_id;
      xxwcinv_inv_aging_pkg.p_reserve_group := p_reserve_group;
      xxwcinv_inv_aging_pkg.p_days_low := p_days_low;
      xxwcinv_inv_aging_pkg.p_days_high := p_days_high;
      --XXWCINV_INV_AGING_PKG.P_RSV_AMT_LIMIT := p_rsv_amt_limit;

      l_err_callpoint := 'Before calling BeforeReport';

      -- execute routine to populate temporary table
      IF NOT beforereport
      THEN
         fnd_file.put_line (
            fnd_file.LOG,
            'Error generating report data in Before Report Trigger');
         RAISE xxwc_error;
      END IF;

      -- populate fields for output headings
      cp_supplier_name := cp_supplier_name_p;
      cp_org_name := cp_org_name_p;



      IF NVL (p_ftp_flag, 'N') <> 'Y'
      THEN                                 -- Added condition for Revision 2.0
         l_err_callpoint := 'Before generating report headings';

         -- generate report output
         fnd_file.put_line (
            fnd_file.output,
               TO_CHAR (SYSDATE, 'DD-MON-YYYY HH24:MI')
            || '||||||HD Supply Inventory Aging Report');
         fnd_file.put_line (fnd_file.output, 'Parameters');
         fnd_file.put_line (fnd_file.output, 'Region: ' || p_region);
         fnd_file.put_line (fnd_file.output, 'Location: ' || cp_org_name);
         --FND_FILE.Put_Line(FND_FILE.Output, 'Market: '||p_market);
         fnd_file.put_line (fnd_file.output,
                            'Item Category : ' || p_item_category);
         fnd_file.put_line (fnd_file.output,
                            'Supplier : ' || cp_supplier_name);
         fnd_file.put_line (fnd_file.output,
                            'Reserve Group: ' || p_reserve_group);
         fnd_file.put_line (
            fnd_file.output,
            'Days Range: ' || p_days_low || ' - ' || p_days_high);
         --FND_FILE.Put_Line(FND_FILE.Output, 'Reserve Amount Limit: '||p_rsv_amt_limit);

         fnd_file.put_line (fnd_file.output, ' ');
         /*****************
         FND_FILE.Put_Line(FND_FILE.Output, 'location'||'|'||
                                            'district'||'|'||
                                            'part_number'||'|'||
                                            'reserve_group'||'|'||
                                            'rebar'||'|'||
                                            'supplier_number'||'|'||
                                            'supplier_name'||'|'||
                                            'source_location'||'|'||
                                            'part_description'||'|'||
                                            'category_class'||'|'||
                                            'total_oh'||'|'||
                                            'quantity_90_days'||'|'||
                                            'quantity_180_days'||'|'||
                                            'quantity_270_days'||'|'||
                                            'quantity_365_days'||'|'||
                                            'quantity_630_days'||'|'||
                                            'quantity_730_days'||'|'||
                                            'quantity_over_730_days'||'|'||
                                            'quantity_undet_days'||'|'||
                                            'total_amount'||'|'||
                                            'amount_90_days'||'|'||
                                            'amount_180_days'||'|'||
                                            'amount_270_days'||'|'||
                                            'amount_365_days'||'|'||
                                            'amount_630_days'||'|'||
                                            'amount_730_days'||'|'||
                                            'amount_over_730_days'||'|'||
                                            'amount_undet_days'||'|'||
                                            'total_reserve'||'|'||
                                            'reserve_90_days'||'|'||
                                            'reserve_180_days'||'|'||
                                            'reserve_270_days'||'|'||
                                            'reserve_365_days'||'|'||
                                            'reserve_630_days'||'|'||
                                            'reserve_730_days'||'|'||
                                            'reserve_over_730_days'||'|'||
                                            'reserve_undet_days'||'|'||
                                            'over365_reserve'||'|'||
                                            'combined_total_reserve'
                                           );
             ***************/

         fnd_file.put_line (
            fnd_file.output,
               'location'
            || '|'
            || 'district'
            || '|'
            || 'part_number'
            || '|'
            || 'reserve_group'
            || '|'
            || 'rebar'
            || '|'
            || 'consigned_flag'
            || '|'
            || 'supplier_number'
            || '|'
            || 'supplier_name'
            || '|'
            || 'source_location'
            || '|'
            || 'part_description'
            || '|'
            || 'category_class'
            || '|'
            || 'material_location'
            || '|'
            || 'total_oh'
            || '|'
            || 'quantity_90_days'
            || '|'
            || 'quantity_180_days'
            || '|'
            || 'quantity_270_days'
            || '|'
            || 'quantity_365_days'
            || '|'
            || 'quantity_630_days'
            || '|'
            || 'quantity_730_days'
            || '|'
            || 'quantity_over_730_days'
            || '|'
            || 'quantity_undet_days'
            || '|'
            || 'total_amount'
            || '|'
            || 'amount_90_days'
            || '|'
            || 'amount_180_days'
            || '|'
            || 'amount_270_days'
            || '|'
            || 'amount_365_days'
            || '|'
            || 'amount_630_days'
            || '|'
            || 'amount_730_days'
            || '|'
            || 'amount_over_730_days'
            || '|'
            || 'amount_undet_days');
         l_err_callpoint := 'Before Loop';

         FOR r1 IN c1
         LOOP
            l_cnt := l_cnt + 1;

            l_err_callpoint := 'Before generating detail output';
            /*********************
            FND_FILE.Put_Line(FND_FILE.Output, R1.location||'|'||
                                               R1.district||'|'||
                                               R1.part_number||'|'||
                                               R1.reserve_group||'|'||
                                               R1.rebar||'|'||
                                               R1.supplier_number||'|'||
                                               R1.supplier_name||'|'||
                                               R1.source_location||'|'||
                                               R1.part_description||'|'||
                                               R1.category_class||'|'||
                                               R1.total_oh||'|'||
                                               R1.quantity_90_days||'|'||
                                               R1.quantity_180_days||'|'||
                                               R1.quantity_270_days||'|'||
                                               R1.quantity_365_days||'|'||
                                               R1.quantity_630_days||'|'||
                                               R1.quantity_730_days||'|'||
                                               R1.quantity_over_730_days||'|'||
                                               R1.quantity_undet_days||'|'||
                                               R1.total_amount||'|'||
                                               R1.amount_90_days||'|'||
                                               R1.amount_180_days||'|'||
                                               R1.amount_270_days||'|'||
                                               R1.amount_365_days||'|'||
                                               R1.amount_630_days||'|'||
                                               R1.amount_730_days||'|'||
                                               R1.amount_over_730_days||'|'||
                                               R1.amount_undet_days||'|'||
                                               R1.total_reserve||'|'||
                                               R1.reserve_90_days||'|'||
                                               R1.reserve_180_days||'|'||
                                               R1.reserve_270_days||'|'||
                                               R1.reserve_365_days||'|'||
                                               R1.reserve_630_days||'|'||
                                               R1.reserve_730_days||'|'||
                                               R1.reserve_over_730_days||'|'||
                                               R1.reserve_undet_days||'|'||
                                               R1.over365_reserve||'|'||
                                               R1.combined_total_reserve
                                              );
          ***********************/
            fnd_file.put_line (
               fnd_file.output,
                  r1.location
               || '|'
               || r1.district
               || '|'
               || r1.part_number
               || '|'
               || r1.reserve_group
               || '|'
               || r1.rebar
               || '|'
               || r1.consigned_flag
               || '|'
               || r1.supplier_number
               || '|'
               || r1.supplier_name
               || '|'
               || r1.source_location
               || '|'
               || r1.part_description
               || '|'
               || r1.category_class
               || '|'
               || r1.material_location
               || '|'
               || r1.total_oh
               || '|'
               || r1.quantity_90_days
               || '|'
               || r1.quantity_180_days
               || '|'
               || r1.quantity_270_days
               || '|'
               || r1.quantity_365_days
               || '|'
               || r1.quantity_630_days
               || '|'
               || r1.quantity_730_days
               || '|'
               || r1.quantity_over_730_days
               || '|'
               || r1.quantity_undet_days
               || '|'
               || r1.total_amount
               || '|'
               || r1.amount_90_days
               || '|'
               || r1.amount_180_days
               || '|'
               || r1.amount_270_days
               || '|'
               || r1.amount_365_days
               || '|'
               || r1.amount_630_days
               || '|'
               || r1.amount_730_days
               || '|'
               || r1.amount_over_730_days
               || '|'
               || r1.amount_undet_days);
         END LOOP;

         l_err_callpoint := 'After Loop';

         IF l_cnt = 0
         THEN
            fnd_file.put_line (fnd_file.output,
                               l_cnt || ' records generated');
         END IF;

         errbuf := 'Success';
         retcode := 0;
         l_err_callpoint := 'At End';

         fnd_file.put_line (fnd_file.LOG, l_cnt || ' records generated');
      -- Added the below else for Revision 2.0 to generate flat file
      ELSE
         FOR rec_instance IN (SELECT instance_name FROM v$instance)
         LOOP
            l_instance_name := rec_instance.instance_name;
         END LOOP;

         l_file_name :=
               TO_CHAR (SYSDATE, 'YYYY_MM_DD')
            || '_INVENTORY_AGING_'
            || l_instance_name
            || '.csv';

         l_file_dir := 'XXWC_ACC_REPORTS';
         l_file_handle := UTL_FILE.fopen (l_file_dir, l_file_name, 'w');

         l_module := 'Generate_CSV';
         l_err_callpoint := 'Start';


         l_err_callpoint := 'Before generating report headings for flat file';

         -- Generate report output
         UTL_FILE.put_line (l_file_handle, 'sep=|');
         UTL_FILE.put_line (
            l_file_handle,
               TO_CHAR (SYSDATE, 'DD-MON-YYYY HH24:MI')
            || '||||||HD Supply Inventory Aging Report');
         UTL_FILE.put_line (l_file_handle, 'Parameters');
         UTL_FILE.put_line (l_file_handle, 'Region: ' || p_region);
         UTL_FILE.put_line (l_file_handle, 'Location: ' || cp_org_name);

         UTL_FILE.put_line (l_file_handle,
                            'Item Category : ' || p_item_category);
         UTL_FILE.put_line (l_file_handle, 'Supplier : ' || cp_supplier_name);
         UTL_FILE.put_line (l_file_handle,
                            'Reserve Group: ' || p_reserve_group);
         UTL_FILE.put_line (
            l_file_handle,
            'Days Range: ' || p_days_low || ' - ' || p_days_high);

         UTL_FILE.put_line (l_file_handle, ' ');

         UTL_FILE.put_line (
            l_file_handle,
               'location'
            || '|'
            || 'district'
            || '|'
            || 'part_number'
            || '|'
            || 'reserve_group'
            || '|'
            || 'rebar'
            || '|'
            || 'consigned_flag'
            || '|'
            || 'supplier_number'
            || '|'
            || 'supplier_name'
            || '|'
            || 'source_location'
            || '|'
            || 'part_description'
            || '|'
            || 'category_class'
            || '|'
            || 'material_location'
            || '|'
            || 'total_oh'
            || '|'
            || 'quantity_90_days'
            || '|'
            || 'quantity_180_days'
            || '|'
            || 'quantity_270_days'
            || '|'
            || 'quantity_365_days'
            || '|'
            || 'quantity_630_days'
            || '|'
            || 'quantity_730_days'
            || '|'
            || 'quantity_over_730_days'
            || '|'
            || 'quantity_undet_days'
            || '|'
            || 'total_amount'
            || '|'
            || 'amount_90_days'
            || '|'
            || 'amount_180_days'
            || '|'
            || 'amount_270_days'
            || '|'
            || 'amount_365_days'
            || '|'
            || 'amount_630_days'
            || '|'
            || 'amount_730_days'
            || '|'
            || 'amount_over_730_days'
            || '|'
            || 'amount_undet_days');
         l_err_callpoint := 'Before Loop';

         FOR r1 IN c1
         LOOP
            l_cnt := l_cnt + 1;

            l_err_callpoint := 'Before generating detail output';

            UTL_FILE.put_line (
               l_file_handle,
                  r1.location
               || '|'
               || r1.district
               || '|'
               || r1.part_number
               || '|'
               || r1.reserve_group
               || '|'
               || r1.rebar
               || '|'
               || r1.consigned_flag
               || '|'
               || r1.supplier_number
               || '|'
               || r1.supplier_name
               || '|'
               || r1.source_location
               || '|'
               || r1.part_description
               || '|'
               || r1.category_class
               || '|'
               || r1.material_location
               || '|'
               || r1.total_oh
               || '|'
               || r1.quantity_90_days
               || '|'
               || r1.quantity_180_days
               || '|'
               || r1.quantity_270_days
               || '|'
               || r1.quantity_365_days
               || '|'
               || r1.quantity_630_days
               || '|'
               || r1.quantity_730_days
               || '|'
               || r1.quantity_over_730_days
               || '|'
               || r1.quantity_undet_days
               || '|'
               || r1.total_amount
               || '|'
               || r1.amount_90_days
               || '|'
               || r1.amount_180_days
               || '|'
               || r1.amount_270_days
               || '|'
               || r1.amount_365_days
               || '|'
               || r1.amount_630_days
               || '|'
               || r1.amount_730_days
               || '|'
               || r1.amount_over_730_days
               || '|'
               || r1.amount_undet_days);
         END LOOP;

         l_err_callpoint := 'After Loop';

         IF l_cnt = 0
         THEN
            UTL_FILE.put_line (l_file_handle, l_cnt || ' records generated');
         END IF;

         errbuf := 'Success';
         retcode := 0;
         l_err_callpoint := 'At End';

         UTL_FILE.fclose (l_file_handle);
      END IF;
   EXCEPTION
      WHEN xxwc_error
      THEN
         errbuf := 'Warning';
         retcode := 1;
      WHEN OTHERS
      THEN
         errbuf := SQLERRM;
         retcode := 2;
         ROLLBACK;
         l_err_msg :=
               'Error in '
            || l_err_callpoint
            || ' WITH  '
            || SUBSTR (SQLERRM, 1, 1000);
         --dbms_output.put_line(l_fulltext);
         fnd_file.put_line (fnd_file.LOG, l_err_msg);
         fnd_file.put_line (fnd_file.output, l_err_msg);

         xxcus_error_pkg.xxcus_error_main_api (
            p_called_from         => l_err_callfrom,
            p_calling             => l_err_callpoint,
            p_ora_error_msg       => SQLERRM,
            p_error_desc          => 'Error running XXWCINV_INV_AGING_PKG package with PROGRAM ERROR',
            p_distribution_list   => l_distro_list,
            p_module              => l_module);
   END generate_csv;

    --*****************************************************************************************************
    --*****************************************************************************************************
    --*****************************************************************************************************
    /* by Rasikha 15-JUN-2012
       The White Cap Inventory Aging Report needs to be run on a weekly basis and then stored to a network
      location.  The White Cap Inventory Aging Report needs to be saved to a central location for viewing
      by the Finance Team.  The report should be saved to the following location: P:\ACCOUNTING\INVENTORY AGING.
      To allow for the storage of multiple versions of the file, they should be named as follows:
      YEAR_MONTH_DAY_INVENTORY_AGING (2012_06_04_INVENTORY_AGING).*/
    --*****************************************************************************************************
    --*****************************************************************************************************
    PROCEDURE run_report_weekly
    IS
        v_errbuf           VARCHAR2 (2000);
        v_retcode          VARCHAR2 (2000);
        v_file_name        VARCHAR2 (256);
        v_directory_path   VARCHAR2 (256);
        v_os_error         CLOB;
    BEGIN
        SELECT directory_path
          INTO v_directory_path
          FROM all_directories
         WHERE directory_name = 'XXWC_ACC_REPORTS';

        generate_csv_utl_file (errbuf            => v_errbuf
                              ,retcode           => v_retcode
                              ,p_region          => NULL
                              ,p_branch          => NULL
                              ,p_item_category   => NULL
                              ,p_reserve_group   => NULL
                              ,p_supplier_id     => NULL
                              ,p_days_low        => 0
                              ,p_days_high       => 999999
                              ,p_file_name       => v_file_name);

        BEGIN
            xxwc_ftp.ftpconnect ('intra2a.corporate.whitecap.net', 'corporate\wcftp', '@49a843');
            xxwc_ftp.setdirectory ('/ACCOUNTING/INVENTORY AGING');
            xxwc_ftp.putbinaryfile (v_directory_path || '/' || v_file_name, v_file_name);
            xxwc_ftp.close_ftp;
            DBMS_OUTPUT.put_line ('file name=' || v_file_name);

            --will delete after files were moved to another server
            SELECT xxwc_edi_iface_pkg.xxwc_oscommand_run ('rm ' || v_directory_path || '/' || v_file_name)
              INTO v_os_error
              FROM DUAL;

            COMMIT;

            IF v_os_error IS NOT NULL
            THEN
                DBMS_OUTPUT.put_line (
                    'v_Os_error when removing report file =' || v_os_error || 'for filr :' || v_file_name);
            END IF;
        END;
    END;

    ---****************************************************************
    ---****************************************************************
    -- by Rasikha 15-JUN-2012
    -- to start the job for xxwcinv_inv_aging_pkg.run_report_weekly
    ---****************************************************************
    ---****************************************************************
    FUNCTION xxwc_next_month_end_aging
        RETURN VARCHAR2
    IS
        v_string   CLOB;
    BEGIN
        FOR r IN (  SELECT end_date
                      FROM gl_periods
                     WHERE period_set_name = '4-4-QTR' AND adjustment_period_flag = 'N' AND end_date > SYSDATE
                  ORDER BY end_date)
        LOOP
            v_string := v_string || ',' || TO_CHAR (r.end_date - 1, 'YYYYMMDD');
        END LOOP;

        -- DBMS_OUTPUT.PUT_LINE( V_STRING);
        v_string := SUBSTR (v_string, 2);
        --  DBMS_OUTPUT.PUT_LINE( V_STRING);
        RETURN v_string;
    END;

    --RUN THIS TO RESTART MONTH END REPORT FROM TOAD OR SQL PROMP - BECAUSE APPS HAS insufficient privileges EXECUTE IN FROM PACKAGE
    /*  DECLARE
           v_repeat_string   VARCHAR2 (1024);
           v_end_date        TIMESTAMP WITH TIME ZONE;
       BEGIN
           SELECT MAX (end_date) + 3
             INTO v_end_date
             FROM gl_periods
            WHERE period_set_name = '4-4-QTR' AND adjustment_period_flag = 'N' AND end_date > SYSDATE;

           v_repeat_string := 'FREQ=DAILY;BYDATE=' || xxwcinv_inv_aging_pkg.xxwc_next_month_end_aging || ';BYHOUR=23;BYMINUTE=55;BYSECOND=0;';
          -- DBMS_OUTPUT.put_line (v_repeat_string);

           BEGIN
               SYS.DBMS_SCHEDULER.drop_job (job_name => 'XXWCINV_INV_AGING_REPORT_ME');
           EXCEPTION
               WHEN OTHERS
               THEN
                   NULL;
           END;

           EXECUTE IMMEDIATE 'ALTER SESSION SET TIME_ZONE = ''-4:00''';

           DBMS_SCHEDULER.create_job (
               job_name          => 'xxwcinv_inv_aging_report_me',
               job_type          => 'PLSQL_BLOCK',
               job_action        => 'BEGIN xxwcinv_inv_aging_pkg.run_report_weekly; END;',
               start_date        => CURRENT_TIMESTAMP,
               repeat_interval   => v_repeat_string,
               end_date          => v_end_date,
               enabled           => TRUE,
               comments          => 'schedule the inventory aging report export to the P:schedule it for every month end Saturday/Sunday night at 11:55 PM EST ');
       END;*/
    --RUN THIS TO RESTART WEEKLY REPORT END REPORT FROM TOAD OR SQL PROMP - BECAUSE APPS HAS insufficient privileges EXECUTE IN FROM PACKAGE
    /*
    BEGIN
  EXECUTE IMMEDIATE 'ALTER SESSION SET TIME_ZONE = ''-4:00''';
     DBMS_SCHEDULER.create_job (
         job_name          => 'xxwcinv_inv_aging_report',
         job_type          => 'PLSQL_BLOCK',
         job_action        => 'BEGIN xxwcinv_inv_aging_pkg.run_report_weekly; END;',
         start_date        => CURRENT_TIMESTAMP,
         repeat_interval   => 'FREQ=WEEKLY;BYDAY=THU;BYHOUR=23;BYMINUTE=55;BYSECOND=0;',
         end_date          => NULL,
         enabled           => TRUE,
         comments          => 'schedule the inventory aging report export to the P:every Thursday night at 11:55 PM EST ');
 END;


 BEGIN
     DBMS_SCHEDULER.drop_job (job_name => 'xxwcinv_inv_aging_report');
 END;


 SELECT owner, job_name, enabled FROM dba_scheduler_jobs;
 select table_name from dict where table_name like '%SCHEDULER%';
 SELECT * FROM DBA_SCHEDULER_JOBS*/

    --************************************************************************************
    --  generate_csv_utl_file
    --overloaded by Rasikha 15-JUN-2012 to create report file, wich will be used to ftp to another server once a week, file will deleted after ftp
    --************************************************************************************
    --************************************************************************************
    PROCEDURE generate_csv_utl_file (errbuf               OUT VARCHAR2
                                    ,retcode              OUT VARCHAR2
                                    ,p_region          IN     VARCHAR2
                                    ,p_branch          IN     NUMBER
                                    ,p_item_category   IN     VARCHAR2
                                    ,p_reserve_group   IN     VARCHAR2
                                    ,p_supplier_id     IN     NUMBER
                                    ,p_days_low        IN     NUMBER
                                    ,p_days_high       IN     NUMBER
                                    ,p_file_name          OUT VARCHAR2) IS
   
        --p_rsv_amt_limit    IN   NUMBER) Is  -- Satish U: 06-MAR-2012
        l_file_name       VARCHAR2 (100);
        l_file_dir        VARCHAR2 (100);
        l_file_handle     UTL_FILE.file_type;
        v_instance_name   VARCHAR2 (164);
        v_error_message   CLOB;

        CURSOR c1
        IS
              SELECT location
                    ,district
                    ,market
                    ,part_number
                    ,reserve_group
                    ,rebar
                    ,consigned_flag
                    ,                                                                   --added 3/15/2013 20130312-01176
                     supplier_number
                    ,supplier_name
                    ,source_location
                    ,part_description
                    ,category_class
                    ,material_location
                    ,                                                                                   --added 12/12/12
                     SUM (quantity_90_days) quantity_90_days
                    ,SUM (quantity_180_days) quantity_180_days
                    ,SUM (quantity_270_days) quantity_270_days
                    ,SUM (quantity_365_days) quantity_365_days
                    ,SUM (quantity_630_days) quantity_630_days
                    ,SUM (quantity_730_days) quantity_730_days
                    ,SUM (quantity_over_730_days) quantity_over_730_days
                    ,SUM (quantity_undet_days) quantity_undet_days
                    ,SUM (
                           quantity_90_days
                         + quantity_180_days
                         + quantity_270_days
                         + quantity_365_days
                         + quantity_630_days
                         + quantity_730_days
                         + quantity_over_730_days
                         + quantity_undet_days)
                         total_oh
                    ,SUM (amount_90_days) amount_90_days
                    ,SUM (amount_180_days) amount_180_days
                    ,SUM (amount_270_days) amount_270_days
                    ,SUM (amount_365_days) amount_365_days
                    ,SUM (amount_630_days) amount_630_days
                    ,SUM (amount_730_days) amount_730_days
                    ,SUM (amount_over_730_days) amount_over_730_days
                    ,SUM (amount_undet_days) amount_undet_days
                    ,SUM (
                           amount_90_days
                         + amount_180_days
                         + amount_270_days
                         + amount_365_days
                         + amount_630_days
                         + amount_730_days
                         + amount_over_730_days
                         + amount_undet_days)
                         total_amount
                FROM xxwc.xxwcinv_inv_aging_temp
               WHERE reserve_group = NVL (p_reserve_group, reserve_group) ---Added to Filter for P_RESERVE_GROUP Parameter LHS 5/25/2012
            GROUP BY location
                    ,district
                    ,market
                    ,part_number
                    ,reserve_group
                    ,rebar
                    ,supplier_number
                    ,supplier_name
                    ,source_location
                    ,part_description
                    ,category_class
                    ,material_location
                    ,                                                                                   --added 12/12/12
                     consigned_flag                                                     --added 3/15/2013 20130312-01176
            ORDER BY 1
                    ,2
                    ,3
                    ,4
                    ,5
                    ,6;

        l_cnt             NUMBER := 0;
    BEGIN
        FOR ir IN (SELECT instance_name FROM v$instance)
        LOOP
            v_instance_name := ir.instance_name;
        END LOOP;

        l_file_name := TO_CHAR (SYSDATE, 'YYYY_MM_DD') || '_INVENTORY_AGING_' || v_instance_name || '.csv';
        --DBMS_OUTPUT.put_line ('l_file_name=' || l_file_name);

        p_file_name := l_file_name;
        l_file_dir := 'XXWC_ACC_REPORTS';
        --ODPDIR
        l_file_handle := UTL_FILE.fopen (l_file_dir, l_file_name, 'w');
        l_module := 'Generate_CSV';
        l_err_callpoint := 'Start';

        xxwcinv_inv_aging_pkg.p_region := p_region;
        xxwcinv_inv_aging_pkg.p_branch := p_branch;

        xxwcinv_inv_aging_pkg.p_item_category := p_item_category;
        xxwcinv_inv_aging_pkg.p_supplier_id := p_supplier_id;
        xxwcinv_inv_aging_pkg.p_reserve_group := p_reserve_group;
        xxwcinv_inv_aging_pkg.p_days_low := p_days_low;
        xxwcinv_inv_aging_pkg.p_days_high := p_days_high;

        l_err_callpoint := 'Before calling BeforeReport';

        -- execute routine to populate temporary table
        IF NOT beforereport
        THEN
            DBMS_OUTPUT.put_line ('Error generating report data in Before Report Trigger');
            RAISE xxwc_error;
        END IF;

        -- populate fields for output headings
        cp_supplier_name := cp_supplier_name_p;
        cp_org_name := cp_org_name_p;

        l_err_callpoint := 'Before generating report headings';

        -- generate report output
        UTL_FILE.put_line (l_file_handle, 'sep=|');
        UTL_FILE.put_line (l_file_handle
                          ,TO_CHAR (SYSDATE, 'DD-MON-YYYY HH24:MI') || '||||||HD Supply Inventory Aging Report');
        UTL_FILE.put_line (l_file_handle, 'Parameters');
        UTL_FILE.put_line (l_file_handle, 'Region: ' || p_region);
        UTL_FILE.put_line (l_file_handle, 'Location: ' || cp_org_name);
        --UTL_FILE.put_line (l_file_handle, 'Market: '||p_market);
        UTL_FILE.put_line (l_file_handle, 'Item Category : ' || p_item_category);
        UTL_FILE.put_line (l_file_handle, 'Supplier : ' || cp_supplier_name);
        UTL_FILE.put_line (l_file_handle, 'Reserve Group: ' || p_reserve_group);
        UTL_FILE.put_line (l_file_handle, 'Days Range: ' || p_days_low || ' - ' || p_days_high);
        --UTL_FILE.put_line (l_file_handle, 'Reserve Amount Limit: '||p_rsv_amt_limit);

        UTL_FILE.put_line (l_file_handle, ' ');

        UTL_FILE.put_line (
            l_file_handle
           ,   'location'
            || '|'
            || 'district'
            || '|'
            || 'part_number'
            || '|'
            || 'reserve_group'
            || '|'
            || 'rebar'
            || '|'
            || 'consigned_flag'
            || '|'
            || 'supplier_number'
            || '|'
            || 'supplier_name'
            || '|'
            || 'source_location'
            || '|'
            || 'part_description'
            || '|'
            || 'category_class'
            || '|'
            || 'material_location'
            || '|'
            || 'total_oh'
            || '|'
            || 'quantity_90_days'
            || '|'
            || 'quantity_180_days'
            || '|'
            || 'quantity_270_days'
            || '|'
            || 'quantity_365_days'
            || '|'
            || 'quantity_630_days'
            || '|'
            || 'quantity_730_days'
            || '|'
            || 'quantity_over_730_days'
            || '|'
            || 'quantity_undet_days'
            || '|'
            || 'total_amount'
            || '|'
            || 'amount_90_days'
            || '|'
            || 'amount_180_days'
            || '|'
            || 'amount_270_days'
            || '|'
            || 'amount_365_days'
            || '|'
            || 'amount_630_days'
            || '|'
            || 'amount_730_days'
            || '|'
            || 'amount_over_730_days'
            || '|'
            || 'amount_undet_days');
        l_err_callpoint := 'Before Loop';

        FOR r1 IN c1
        LOOP
            l_cnt := l_cnt + 1;

            l_err_callpoint := 'Before generating detail output';

            UTL_FILE.put_line (
                l_file_handle
               ,   r1.location
                || '|'
                || r1.district
                || '|'
                || r1.part_number
                || '|'
                || r1.reserve_group
                || '|'
                || r1.rebar
                || '|'
                || r1.consigned_flag
                || '|'
                || r1.supplier_number
                || '|'
                || r1.supplier_name
                || '|'
                || r1.source_location
                || '|'
                || r1.part_description
                || '|'
                || r1.category_class
                || '|'
                || r1.material_location
                || '|'
                || r1.total_oh
                || '|'
                || r1.quantity_90_days
                || '|'
                || r1.quantity_180_days
                || '|'
                || r1.quantity_270_days
                || '|'
                || r1.quantity_365_days
                || '|'
                || r1.quantity_630_days
                || '|'
                || r1.quantity_730_days
                || '|'
                || r1.quantity_over_730_days
                || '|'
                || r1.quantity_undet_days
                || '|'
                || r1.total_amount
                || '|'
                || r1.amount_90_days
                || '|'
                || r1.amount_180_days
                || '|'
                || r1.amount_270_days
                || '|'
                || r1.amount_365_days
                || '|'
                || r1.amount_630_days
                || '|'
                || r1.amount_730_days
                || '|'
                || r1.amount_over_730_days
                || '|'
                || r1.amount_undet_days);
        END LOOP;

        l_err_callpoint := 'After Loop';

        IF l_cnt = 0
        THEN
            UTL_FILE.put_line (l_file_handle, l_cnt || ' records generated');
        END IF;

        errbuf := 'Success';
        retcode := 0;
        l_err_callpoint := 'At End';

        DBMS_OUTPUT.put_line (l_cnt || ' records generated');
        UTL_FILE.fclose (l_file_handle);
    EXCEPTION
        WHEN xxwc_error
        THEN
            errbuf := 'Warning';
            retcode := 1;
        WHEN OTHERS
        THEN
            UTL_FILE.fclose (l_file_handle);
            errbuf := SQLERRM;
            retcode := 2;
            ROLLBACK;
            v_error_message := 'Error in ' || l_err_callpoint || ' WITH  ' || SUBSTR (SQLERRM, 1, 1000);
            --dbms_output.put_line(l_fulltext);

            --  UTL_FILE.put_line (l_file_handle, l_err_msg);
            v_error_message :=
                   v_error_message
                || 'Error_Stack...'
                || DBMS_UTILITY.format_error_stack ()
                || ' Error_Backtrace...'
                || DBMS_UTILITY.format_error_backtrace ();
            -- DBMS_OUTPUT.put_line (v_error_message);
            xxcus_error_pkg.xxcus_error_main_api (
                p_called_from         => l_err_callfrom
               ,p_calling             => l_err_callpoint
               ,p_ora_error_msg       => v_error_message
               ,p_error_desc          => 'Error running XXWCINV_INV_AGING_PKG package with PROGRAM ERROR'
               ,p_distribution_list   => l_distro_list
               ,p_module              => l_module);
    END;

    /***************************************************************************************/
    /* Update_Orig_Date_Rcvd : procedure to update the orig_date_received column on new    */
    /*                  onhand detail records created by intercompany receipt transactions */
    /***************************************************************************************/
    PROCEDURE update_orig_date_rcvd_api (p_trx_id IN NUMBER, p_xfer_trx_id IN NUMBER)
    IS
        -- cursor to retrieve intercompany transfer records
        CURSOR c1
        IS
              --SELECT xfer.orig_date_received, xfer.quantity, xfer.ROWID row_id --removed 10/2/2013 TMS Ticket 20131011-00343  
              SELECT xfer.orig_date_received, abs(xfer.quantity) quantity, xfer.ROWID row_id --added 10/2/2013 TMS Ticket 20131011-00343  
                FROM xxwc.xxwcinv_interco_transfers xfer
               WHERE xfer.transaction_id = p_xfer_trx_id
            ORDER BY xfer.orig_date_received DESC;

        r1          c1%ROWTYPE;

        l_qty       NUMBER := 0;
        l_trx_qty   NUMBER;
        l_rowid     ROWID;
        l_mod_name VARCHAR2 (100) := 'xxwcinv_inv_aging_pkg.update_orig_date_rcvd_api' ;
        
        -- Added to remove use of global variables 20131023-00301  
        xxwc_error2        EXCEPTION;
        
        l_err_msg2         VARCHAR2 (2000);
        l_err_callfrom2   VARCHAR2 (175) := 'XXWCINV_INV_AGING_PKG';
        l_err_callpoint2   VARCHAR2 (175) := 'START';
        l_distro_list2     VARCHAR2 (80) := 'OracleDevelopmentGroup@hdsupply.com';
        l_module2          VARCHAR2 (80);
    BEGIN
        --COMMIT;

        l_module2 := 'Update_Orig_Date_Rcvd';
        l_err_callpoint2 := 'Start';

        fnd_file.put_line (fnd_file.LOG, 'Processing for trx_id ' || p_trx_id || ' and src trx id ' || p_xfer_trx_id);
        
        l_err_callpoint2 := 'Before retrieve detail information for processing';

        -- retrieve detail information for processing
        BEGIN
            SELECT transaction_quantity, ROWID
              INTO l_trx_qty, l_rowid
              FROM mtl_onhand_quantities_detail
             WHERE create_transaction_id = p_trx_id;
        EXCEPTION
            WHEN NO_DATA_FOUND
            THEN
                fnd_file.put_line (fnd_file.LOG, 'Unable to find onhand quantity detail for trx id :' || p_trx_id);
                RAISE xxwc_error2;
        END;

        l_err_callpoint2 := 'Before retrieve records from intercompany transfer table';

        -- retrieve records from intercompany transfer table
        OPEN c1;

        FETCH c1 INTO r1;

        IF c1%NOTFOUND
        THEN
            fnd_file.put_line (fnd_file.LOG, 'No intercompany records for src trx id :' || p_xfer_trx_id);
            
       
            CLOSE c1;

            RAISE xxwc_error2;
        END IF;

        l_err_callpoint2 := 'Before Qty comparisons';

        -- if existing record matches qty,then update only rcvd date
        IF r1.quantity = l_trx_qty
        THEN
            l_err_callpoint2 := 'Found inter-company record for same quantity';
            fnd_file.put_line (fnd_file.LOG
                              ,'Found inter-company record for same quantity : ' || r1.orig_date_received);
                              
 
             UPDATE mtl_onhand_quantities_detail
               SET orig_date_received = r1.orig_date_received
             WHERE ROWID = l_rowid;

            -- remove processed record from custom table
            DELETE FROM xxwc.xxwcinv_interco_transfers
                  WHERE ROWID = r1.row_id;

            l_qty := 0;
        ELSIF r1.quantity > l_trx_qty
        THEN
            l_err_callpoint2 := 'Found inter-company record for larger quantity';
            fnd_file.put_line (fnd_file.LOG
                              ,'Found inter-company record for larger quantity : ' || r1.orig_date_received);
            
            UPDATE mtl_onhand_quantities_detail
               SET orig_date_received = r1.orig_date_received
             WHERE ROWID = l_rowid;

            -- remove processed record from custom table
            UPDATE xxwc.xxwcinv_interco_transfers
               SET quantity = quantity - l_trx_qty
             WHERE ROWID = r1.row_id;

            l_qty := r1.quantity - l_trx_qty;
        ELSE
            l_err_callpoint2 := 'Found inter-company record for smaller quantity';
            fnd_file.put_line (fnd_file.LOG
                              ,'Found inter-company record for smaller quantity : ' || r1.orig_date_received);
            
            -- else set rcvd date and reduce qty, and create new MOQD records
            l_qty := l_trx_qty - r1.quantity;

            UPDATE mtl_onhand_quantities_detail
               SET orig_date_received = r1.orig_date_received
                  ,transaction_quantity = r1.quantity
                  ,primary_transaction_quantity = r1.quantity
             WHERE ROWID = l_rowid;

            -- remove processed record from custom table
            DELETE FROM xxwc.xxwcinv_interco_transfers
                  WHERE ROWID = r1.row_id;

            l_err_callpoint2 := 'Before Loop';

            LOOP
                FETCH c1 INTO r1;

                EXIT WHEN c1%NOTFOUND;

                -- create new records for remaining quantity and orig_date_received
                INSERT INTO mtl_onhand_quantities_detail (inventory_item_id
                                                         ,organization_id
                                                         ,date_received
                                                         ,last_update_date
                                                         ,last_updated_by
                                                         ,creation_date
                                                         ,created_by
                                                         ,last_update_login
                                                         ,primary_transaction_quantity
                                                         ,subinventory_code
                                                         ,revision
                                                         ,locator_id
                                                         ,create_transaction_id
                                                         ,update_transaction_id
                                                         ,lot_number
                                                         ,orig_date_received
                                                         ,cost_group_id
                                                         ,containerized_flag
                                                         ,project_id
                                                         ,task_id
                                                         ,onhand_quantities_id
                                                         ,organization_type
                                                         ,owning_organization_id
                                                         ,owning_tp_type
                                                         ,planning_organization_id
                                                         ,planning_tp_type
                                                         ,transaction_uom_code
                                                         ,transaction_quantity
                                                         ,secondary_uom_code
                                                         ,secondary_transaction_quantity
                                                         ,is_consigned
                                                         ,lpn_id
                                                         ,status_id)
                    SELECT inventory_item_id
                          ,organization_id
                          ,date_received
                          ,last_update_date
                          ,last_updated_by
                          ,creation_date
                          ,4                                                                               -- created_by
                          ,last_update_login
                          ,LEAST (l_qty, r1.quantity)                                    -- primary_transaction_quantity
                          ,subinventory_code
                          ,revision
                          ,locator_id
                          ,create_transaction_id
                          ,update_transaction_id
                          ,lot_number
                          ,r1.orig_date_received                                                   -- orig_date_received
                          ,cost_group_id
                          ,containerized_flag
                          ,project_id
                          ,task_id
                          ,onhand_quantities_id
                          ,organization_type
                          ,owning_organization_id
                          ,owning_tp_type
                          ,planning_organization_id
                          ,planning_tp_type
                          ,transaction_uom_code
                          ,LEAST (l_qty, r1.quantity)                                            -- transaction_quantity
                          ,secondary_uom_code
                          ,secondary_transaction_quantity
                          ,is_consigned
                          ,lpn_id
                          ,status_id
                      FROM mtl_onhand_quantities_detail
                     WHERE ROWID = l_rowid;

                -- update/delete temp record accordingly
                IF r1.quantity > l_qty
                THEN
                    UPDATE xxwc.xxwcinv_interco_transfers
                       SET quantity = quantity - l_qty
                           WHERE ROWID = r1.row_id;

                    l_qty := 0;
                ELSE
                    -- remove processed record from custom table
                    DELETE FROM xxwc.xxwcinv_interco_transfers
                          WHERE ROWID = r1.row_id;

                    l_qty := l_qty - r1.quantity;
                END IF;

                -- if all quantity accounted for, the exit
                IF l_qty <= 0
                THEN
                    EXIT;
                END IF;
            END LOOP;

            l_err_callpoint2 := 'After Loop';

            -- handle situation where not enough intercompany shipments to handle intercompany receipts
            IF l_qty > 0
            THEN
                l_err_callpoint2 := 'Not enough intercompany shipments';

                INSERT INTO mtl_onhand_quantities_detail (inventory_item_id
                                                         ,organization_id
                                                         ,date_received
                                                         ,last_update_date
                                                         ,last_updated_by
                                                         ,creation_date
                                                         ,created_by
                                                         ,last_update_login
                                                         ,primary_transaction_quantity
                                                         ,subinventory_code
                                                         ,revision
                                                         ,locator_id
                                                         ,create_transaction_id
                                                         ,update_transaction_id
                                                         ,lot_number
                                                         ,orig_date_received
                                                         ,cost_group_id
                                                         ,containerized_flag
                                                         ,project_id
                                                         ,task_id
                                                         ,onhand_quantities_id
                                                         ,organization_type
                                                         ,owning_organization_id
                                                         ,owning_tp_type
                                                         ,planning_organization_id
                                                         ,planning_tp_type
                                                         ,transaction_uom_code
                                                         ,transaction_quantity
                                                         ,secondary_uom_code
                                                         ,secondary_transaction_quantity
                                                         ,is_consigned
                                                         ,lpn_id
                                                         ,status_id)
                    SELECT inventory_item_id
                          ,organization_id
                          ,date_received
                          ,last_update_date
                          ,last_updated_by
                          ,creation_date
                          ,4                                                                               -- created_by
                          ,last_update_login
                          ,LEAST (l_qty, r1.quantity)                                    -- primary_transaction_quantity
                          ,subinventory_code
                          ,revision
                          ,locator_id
                          ,create_transaction_id
                          ,update_transaction_id
                          ,lot_number
                          ,r1.orig_date_received                                                   -- orig_date_received
                          ,cost_group_id
                          ,containerized_flag
                          ,project_id
                          ,task_id
                          ,onhand_quantities_id
                          ,organization_type
                          ,owning_organization_id
                          ,owning_tp_type
                          ,planning_organization_id
                          ,planning_tp_type
                          ,transaction_uom_code
                          ,LEAST (l_qty, r1.quantity)                                            -- transaction_quantity
                          ,secondary_uom_code
                          ,secondary_transaction_quantity
                          ,is_consigned
                          ,lpn_id
                          ,status_id
                      FROM mtl_onhand_quantities_detail
                     WHERE ROWID = l_rowid;
            END IF;
        END IF;

        CLOSE c1;

--        COMMIT; --Removed commit on 8/26/2013 TMS 20130826-00733 to prevent issues with executing code from a database trigger

        l_err_callpoint2 := 'At End';
        fnd_file.put_line (fnd_file.LOG, 'Processing complete....');
    --COMMIT; Removed commit on 8/26/2013 TMS 20130826-00733 to prevent issues with executing code from a database trigger

    EXCEPTION
        WHEN xxwc_error2
        THEN
            --ROLLBACK  Removed rollback on 8/26/2013 TMS 20130826-00733 to prevent issues with executing code from a database trigger
               UPDATE xxwc.xxwcinv_interco_transfers
                       SET quantity = quantity - l_qty
                     WHERE ROWID = r1.row_id;

        WHEN OTHERS
        THEN
            IF c1%ISOPEN
            THEN
                CLOSE c1;
            END IF;

             UPDATE xxwc.xxwcinv_interco_transfers
                       SET quantity = quantity - l_qty
                     WHERE ROWID = r1.row_id;


            --ROLLBACK; --Removed rollback on 8/26/2013 TMS 20130826-00733 to prevent issues with executing code from a database trigger
            l_err_msg2 := 'Error in ' || l_err_callpoint2 || ' WITH  ' || SUBSTR (SQLERRM, 1, 1000);
            --dbms_output.put_line(l_fulltext);
            fnd_file.put_line (fnd_file.LOG, l_err_msg2);
            fnd_file.put_line (fnd_file.output, l_err_msg2);

            xxcus_error_pkg.xxcus_error_main_api (
                p_called_from         => l_err_callfrom2
               ,p_calling             => l_err_callpoint2
               ,p_ora_error_msg       => SQLERRM
               ,p_error_desc          => 'Error running XXWCINV_INV_AGING_PKG package with PROGRAM ERROR'
               ,p_distribution_list   => l_distro_list2
               ,p_module              => l_module2);
    /*BEGIN

    insert into XXWC.XXWCINV_INTERCO_TRANSFERS_BK (CREATION_DATE, TRANSACTION_TYPE) values (sysdate, 'WORKED');
    */

    END update_orig_date_rcvd_api;

    FUNCTION get_intransit_bod (p_inventory_item_id      IN NUMBER
                               ,p_from_organization_id   IN NUMBER
                               ,p_to_organization_id     IN NUMBER
                               ,p_receipt_source_code    IN VARCHAR2
                               ,p_quantity               IN NUMBER)
        RETURN DATE
    IS
        l_date   DATE;
    BEGIN
        BEGIN
            SELECT MIN (xit.orig_date_received)
              INTO l_date
              FROM rcv_shipment_headers rsh
                  ,rcv_shipment_lines rsl
                  ,mtl_material_transactions mmt
                  ,xxwc.xxwcinv_interco_transfers xit
             WHERE     rsh.shipment_header_id = rsl.shipment_header_id
                   AND NVL (rsl.quantity_shipped, 0) - NVL (rsl.quantity_received, 0) > 0
                   AND rsl.item_id = mmt.inventory_item_id
                   AND rsl.from_organization_id = mmt.organization_id
                   AND rsh.shipment_num = mmt.shipment_number
                   AND mmt.transaction_id = xit.transaction_id
                   AND mmt.inventory_item_id = xit.inventory_item_id
                   AND mmt.organization_id = xit.organization_id
                   AND xit.quantity >= p_quantity
                   AND rsl.item_id = p_inventory_item_id
                   AND rsl.from_organization_id = p_from_organization_id
                   AND rsl.to_organization_id = p_to_organization_id
                   AND rsh.receipt_source_code = p_receipt_source_code;
        EXCEPTION
            WHEN OTHERS
            THEN
                l_date := SYSDATE;
        END;

        RETURN l_date;
    END get_intransit_bod;
/*
   Function CHECK_TIME_SENSITIVE Added TMS 20130827-00676 on 8/26/2013 by Lee Spitzer to validate if item is considered Time Sensitive, returns  Yes or No
     (p_inventory_item_id IN NUMBER  -- Inventory Item Id
     ,p_organization_id IN NUMBER)   -- Organization Id
        RETURN VARCHAR2;
   */ 
       
     FUNCTION CHECK_TIME_SENSITIVE
                             (p_inventory_item_id IN NUMBER
                             ,p_organization_id IN NUMBER)
        RETURN VARCHAR2
        IS
        
        l_time_sensitive varchar2(3);
        
        BEGIN
        
          BEGIN
            SELECT mcb.segment1
            INTO   l_time_sensitive
            FROM   mtl_system_items_b msib,
                   mtl_item_categories mic,
                   mtl_categories_b mcb,
                   mtl_category_sets mcs
            WHERE  msib.organization_id = p_organization_id
            AND    msib.inventory_item_id = p_inventory_item_id
            AND    msib.inventory_item_id = mic.inventory_item_id
            AND    msib.organization_id = mic.organization_id
            AND    mcs.category_set_id = mic.category_set_id
            AND    mcb.category_id  = mic.category_id 
            AND    mcs.category_set_name = 'Time Sensitive';
          EXCEPTION
              WHEN NO_DATA_FOUND THEN
                    l_time_sensitive := 'No';
          END;
          
            RETURN l_time_sensitive;
          
        END;
        
   /*Function CHECK_SPECIAL Added TMS 20130827-00676 on 8/26/2013 by Lee Spitzer if item is considered to be a special, return Yes or No
     (p_inventory_item_id IN NUMBER  -- Inventory Item Id
     ,p_organization_id IN NUMBER)   -- Organization Id
        RETURN VARCHAR2;
   */
   
   FUNCTION CHECK_SPECIAL     (p_inventory_item_id IN NUMBER
                             ,p_organization_id IN NUMBER)
        RETURN VARCHAR2
        IS
         l_special VARCHAR2(80);
         l_item    VARCHAR2(40);
         l_item_type VARCHAR2(30);
         l_check   VARCHAR2(3);
        
        BEGIN
        
          BEGIN
            SELECT mcb.segment1, msib.segment1, msib.item_type
            INTO   l_special, l_item, l_item_type
            FROM   mtl_system_items_b msib,
                   mtl_item_categories mic,
                   mtl_categories_b mcb,
                   mtl_category_sets mcs
            WHERE  msib.organization_id = p_organization_id
            AND    msib.inventory_item_id = p_inventory_item_id
            AND    msib.inventory_item_id = mic.inventory_item_id
            AND    msib.organization_id = mic.organization_id
            AND    mcs.category_set_id = mic.category_set_id
            AND    mcb.category_id  = mic.category_id 
            AND    mcs.category_set_name = 'Inventory Category';
          EXCEPTION
              WHEN NO_DATA_FOUND THEN
                    l_special := NULL;
                    l_item := NULL;
                    l_item_type := NULL;
          END;
          
          l_check := 'No';
          
          IF l_special = 'SP' THEN
              
              l_check := 'Yes';
          
          ELSIF l_item_type = 'SPECIAL' THEN
                
                  l_check := 'Yes';
                
          ELSIF substr(l_item,1,2) = 'SP' THEN
          
                  l_check := 'Yes';
          ELSE
          
                l_check := 'No';
                
          END IF;
                
                
          RETURN l_check;
            
          
          
        END;
        
END xxwcinv_inv_aging_pkg;
/


