/*************************************************************************
      Script Name: XXWC_CHECKITAPP_PKG_GRANT.sql

      PURPOSE:   Grant execute on XXWC_CHECKITAPP_PKG to INTERFACE_OSO package

      REVISIONS:
      Ver        Date          Author             Description
      ---------  ------------  ------------       ------------------
      1.0        10-Nov-2017   Niraj K Ranjan       TMS#20171102-00074 - Mobile apps new PL/SQL procedures for WOC
****************************************************************************/
GRANT EXECUTE ON apps.XXWC_CHECKITAPP_PKG TO INTERFACE_OSO
/