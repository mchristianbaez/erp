  /********************************************************************************
  FILE NAME: XXWC_ORDER_PAYMENT_API_GRANTS.sql
  
  PROGRAM TYPE: Order Payment API grants for online sales order form
  
  PURPOSE: Order Payment API grants for online sales order form
  
  HISTORY
  ==========================================================================================
  VERSION DATE          AUTHOR(S)       DESCRIPTION
  ------- -----------   --------------- ----------------------------------------------------
  1.0     11/03/2017    Rakesh P.       TMS#20171102-00074-Mobile apps new plsql procedures for WOC
  *******************************************************************************************/
GRANT ALL ON xxwc.XXWC_ORDER_PAYMENT_REC TO INTERFACE_OSO;

GRANT ALL ON xxwc.XXWC_ORDER_PAYMENTS_TBL TO INTERFACE_OSO;





