  /********************************************************************************
  FILE NAME: XXWC_ORDER_PAYMENT_REC.sql
  
  PROGRAM TYPE: Order payment record type online sales order form
  
  PURPOSE: Order payment record type online sales order form
  
  HISTORY
  ==========================================================================================
  VERSION DATE          AUTHOR(S)       DESCRIPTION
  ------- -----------   --------------- ----------------------------------------------------
  1.0     11/03/2017    Rakesh P.       TMS#20171102-00074-Mobile apps new plsql procedures for WOC
  *******************************************************************************************/
CREATE OR REPLACE TYPE xxwc.xxwc_order_payment_rec
AS
  OBJECT (   PAYMENT_TYPE       VARCHAR2(50)
            ,CREDIT_CARD_TYPE   VARCHAR2(50) 
            ,IDENTIFYING_NUMBER VARCHAR2(3000)
            ,PERCENT            NUMBER
            ,PAYMENT_AMOUNT     NUMBER  
            ,ATTRIBUTE1         VARCHAR2(50)        
            ,ATTRIBUTE2         VARCHAR2(50)
            ,ATTRIBUTE3         VARCHAR2(50)
            ,ATTRIBUTE4         VARCHAR2(50)
            ,ATTRIBUTE5         VARCHAR2(50)
			,STATIC FUNCTION g_miss_null RETURN xxwc_order_payment_rec
            );
/

CREATE OR REPLACE TYPE BODY xxwc.xxwc_order_payment_rec
AS
   STATIC FUNCTION g_miss_null
       RETURN xxwc_order_payment_rec
   AS
   BEGIN
      RETURN xxwc_order_payment_rec ( PAYMENT_TYPE            => NULL  
                                     ,CREDIT_CARD_TYPE        => NULL
                                     ,IDENTIFYING_NUMBER      => NULL
                                     ,PERCENT                 => NULL
                                     ,PAYMENT_AMOUNT          => NULL
                                     ,ATTRIBUTE1              => NULL
                                     ,ATTRIBUTE2              => NULL
                                     ,ATTRIBUTE3              => NULL
                                     ,ATTRIBUTE4              => NULL
                                     ,ATTRIBUTE5              => NULL 
                                   );
   END g_miss_null;
END;
/

CREATE PUBLIC SYNONYM xxwc_order_payment_rec FOR xxwc.xxwc_order_payment_rec;

show err;
EXIT;

