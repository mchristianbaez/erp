 /********************************************************************************
  FILE NAME: XXWC_ADDRESS_REC.sql
  
  PROGRAM TYPE: Get Address  info
  
  PURPOSE: Get Address info
  
  HISTORY
  ==========================================================================================
  VERSION DATE          AUTHOR(S)       DESCRIPTION
  ------- -----------   --------------- ----------------------------------------------------
  1.0     11/03/2017    Rakesh P.       TMS#20171102-00074-Mobile apps new plsql procedures for WOC
  1.1	  12/19/2017	Niraj K			TMS#20171102-00074-Mobile apps new plsql procedures for WOC
  *******************************************************************************************/
  
create or replace 
TYPE               XXWC.XXWC_ADDRESS_REC
AS
  OBJECT (   customer_id                  NUMBER,
             country                      VARCHAR2(60),
             address1                     VARCHAR2(240),
             address2                     VARCHAR2(240),
             address3                     VARCHAR2(240),
             address4                     VARCHAR2(240),
             city                         VARCHAR2(60),
             county                       VARCHAR2(60),
             state                        VARCHAR2(60),
             postal_code                  VARCHAR2(60),
             sold_to_flag                 VARCHAR2(1),
             ship_to_flag                 VARCHAR2(1),
             bill_to_flag                 VARCHAR2(1),
             deliver_to_flag              VARCHAR2(1),
             location                     VARCHAR2(240),
             last_name                    VARCHAR2(3000),
             first_name                   VARCHAR2(3000),
             title                        VARCHAR2(240),
             email                        VARCHAR2(240),
             contact_num                  VARCHAR2(240),
             FAX_NUM                      VARCHAR2(240),
             user_ntid                    VARCHAR2(100)  --Modified Raja on 14/12/2017 to change Data type from Number to Character
            ,STATIC FUNCTION g_miss_null RETURN xxwc_address_rec
            );
			/
create or replace 
TYPE BODY      XXWC.XXWC_ADDRESS_REC
AS
   STATIC FUNCTION g_miss_null
       RETURN xxwc_address_rec
   AS
   BEGIN
      RETURN xxwc_address_rec ( customer_id            => NULL,
                                country                => NULL,
                                address1               => NULL,
                                address2               => NULL,
                                address3               => NULL,
                                address4               => NULL,
                                city                   => NULL,
                                county                 => NULL,
                                state                  => NULL,
                                postal_code            => NULL,
                                sold_to_flag           => NULL,
                                ship_to_flag           => NULL,
                                bill_to_flag           => NULL,
                                deliver_to_flag        => NULL,
                                location               => NULL,
                                last_name              => NULL,
                                first_name             => NULL,
                                title                  => NULL,
                                email                  => NULL,
                                contact_num            => NULL,
                                FAX_NUM                => null,
                                user_ntid                => NULL --Modified Raja on 14/12/2017 to change Data type from Number to Character
                                   );
   END g_miss_null;
END;
/
DROP PUBLIC SYNONYM XXWC_ADDRESS_REC;
/
CREATE PUBLIC SYNONYM XXWC_ADDRESS_REC FOR xxwc.XXWC_ADDRESS_REC;

show err;
EXIT;