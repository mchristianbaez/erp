/*************************************************************************
      Script Name: XXWC_WOC_APP_LOG_TBL_SYN.sql

      PURPOSE:   This is to create Synonym

      REVISIONS:
      Ver        Date          Author             Description
      ---------  ------------  ------------       ------------------
      1.0        01/18/2018    Niraj K Ranjan     TMS#20171102-00074-Mobile apps new plsql procedures for WOC    
 ****************************************************************************/
DROP SYNONYM XXWC_WOC_APP_LOG_TBL; 
CREATE SYNONYM XXWC_WOC_APP_LOG_TBL FOR XXWC.XXWC_WOC_APP_LOG_TBL;