/*
 TMS: 20151211-00051 [Parent]
 TMS: 20151211-00056 [Child]
 Date: 12/13/2015
 Notes: Script to  
*/
set serveroutput on size 1000000
declare
 --
 n_id number;
 n_loc number :=0;
 --
begin --Main Processing...
   --
   n_loc :=102;
   --
   begin 
    --
     select id
     into    n_id
     from  xxcus.xxcusap_auditfrt_pull_seq_tmp
     where 1 =1;
    --
    n_loc :=104;
    --
   exception
    when no_data_found then
     --
     n_id :=null;
     --
     n_loc :=105;
     --
     dbms_output.put_line('@ Failed to pull the inital id from the temp control table, variable n_id ='||n_id||', message ='||sqlerrm);
     --   
    when others then
     --
     n_id :=null;
     --
     n_loc :=106;
     --
     dbms_output.put_line('@ Failed to pull the inital id from the temp control table, variable n_id ='||n_id||', other error message ='||sqlerrm);
     --
   end;
   --
   --
   begin 
       --
       savepoint sqr1;
       --
        delete xxcus.xxcusap_audit_frt_pay_ext_tbl
        where 1 =1
              and id >=n_id;
        --
             if (sql%rowcount >0) then
                dbms_output.put_line('Deleted '||sql%rowcount||' rows from xxcus.xxcusap_audit_frt_pay_ext_tbl where id >='||n_id);
                --  
             else
                dbms_output.put_line('Failed to delete rows from xxcus.xxcusap_audit_frt_pay_ext_tbl where id >='||n_id);        
             end if;
        --
   exception  
            when others then
                    --
                    rollback to sqr1;
                    --      
                    dbms_output.put_line(('Error in delete of the control table XXCUS.XXCUSAP_AUDIT_FRT_PAY_EXT_TBL where id >= , '||n_id||', error message ='||sqlerrm));
                    --                    
   end;
   --
   commit;
   --   
exception
   when others then
      dbms_output.put_line ('TMS: 20151211-00051, Errors =' || sqlerrm);
end;
/