create or replace
PACKAGE BODY            APPS.XXWC_PO_PURGE_REQ_PKG
AS 
/*************************************************************************
   *   $Header PO_PURGE_REQ $
   *   Module Name: XXWC_PO_PURGE_REQ_PKG
   *
   *   PURPOSE:   delete records from PO_PURGE_REQ_LIST tables 
   *
   *   REVISIONS:
   *   Ver        Date        Author                     Description
   *   ---------  ----------  ---------------         -------------------------
   *   1.0        10/05/2015  Hari Prasad M           Initial Version
   *   
   * ***************************************************************************/

 PROCEDURE XXWC_PO_PURGE_REQ             (errbuf    OUT VARCHAR2,
                                     retcode   OUT VARCHAR2)
									
IS 
l_count_appr number;
g_distro_list               VARCHAR2 (75)
                                     DEFAULT 'HDSOracleDevelopers@hdsupply.com';
l_sec                       VARCHAR2 (2000):=null;
l_req_id                    NUMBER (10);									 
Begin
l_req_id := fnd_global.conc_request_id;
 select count(1)
 into l_count_appr
 FROM PO_PURGE_REQ_LIST a
 WHERE 1 = 1
 AND NOT EXISTS
 (SELECT 1
 FROM po_requisition_headers_all
 WHERE TYPE_LOOKUP_CODE = 'PURCHASE'
 AND requisition_header_id = a.requisition_header_id
 AND authorization_status = 'CANCELLED');
 
l_sec:='Error while deleting the records if not exist';
 
FND_FILE.PUT_LINE (FND_FILE.LOG,'Approved requisition records has been deleted if not exist:-'||l_count_appr);
 
 IF l_count_appr >0
 then 
 

DELETE
 FROM PO_PURGE_REQ_LIST a
 WHERE 1 = 1
 AND NOT EXISTS
 (SELECT 1
 FROM po_requisition_headers_all
 WHERE TYPE_LOOKUP_CODE = 'PURCHASE'
 AND requisition_header_id = a.requisition_header_id
 AND authorization_status = 'CANCELLED');
 
 End if;
 
 select count(1)
 into l_count_appr
 FROM PO.PO_PURGE_REQ_LIST a
 WHERE EXISTS
 (SELECT 1
 FROM po_requisition_headers_all
 WHERE requisition_header_id = a.requisition_header_id
 AND authorization_status = 'APPROVED');
 
 l_sec:='Error while deleting the records if exist';
 
FND_FILE.PUT_LINE (FND_FILE.LOG,'Approved requisition records has been deleted if exist:-'||l_count_appr);
 
  IF l_count_appr >0
 then 
 
DELETE
 FROM PO.PO_PURGE_REQ_LIST a
 WHERE EXISTS
 (SELECT 1
 FROM po_requisition_headers_all
 WHERE requisition_header_id = a.requisition_header_id
 AND authorization_status = 'APPROVED');
End if;
COMMIT;

Exception 
when others then 
FND_FILE.PUT_LINE (FND_FILE.LOG, 'Error while deleting the records ');
l_sec:='Error while deleting the records';
xxcus_error_pkg.xxcus_error_main_api (
            p_called_from         => 'XXWC_PO_PURGE_REQ_PKG.XXWC_PO_PURGE_REQ',
            p_calling             => l_sec,
            p_request_id          => l_req_id,
            p_ora_error_msg       => SUBSTR (l_sec, 1, 2000),
            p_error_desc          => 'OTHERS Exception',
            p_distribution_list   => g_distro_list,
            p_module              => 'XXWC');
End XXWC_PO_PURGE_REQ;

End XXWC_PO_PURGE_REQ_PKG;
/