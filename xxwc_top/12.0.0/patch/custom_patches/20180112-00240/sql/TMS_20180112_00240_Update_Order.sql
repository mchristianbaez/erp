/******************************************************************************
  $Header TMS_0180112_00240_Data_Fix.sql $
  Module Name:Data Fix script for 20180112-00240

  PURPOSE: Data fix script for 20180112-00240 Update Order

  REVISIONS:
  Ver        Date         Author                Description
  ---------  -----------  ------------------    -------------------------------
  1.0        22-JAN-2018  Krishna Kumar         20180112-00240

*******************************************************************************/

ALTER SESSION SET CURRENT_SCHEMA=apps;
/
BEGIN
   mo_global.set_policy_context ('S', '162');
END;
/
SET serveroutput ON SIZE 500000;
DECLARE

BEGIN

UPDATE 	apps.oe_order_headers_all 
SET 	flow_status_code='CANCELLED',
		open_flag='N'
WHERE 	header_id=67701599;

dbms_output.put_line('Number of Rows Updated '||SQL%ROWCOUNT);

COMMIT;

EXCEPTION
    WHEN OTHERS THEN
      ROLLBACK;
      dbms_output.put_line(substr(sqlerrm, 1, 240));
END;
/