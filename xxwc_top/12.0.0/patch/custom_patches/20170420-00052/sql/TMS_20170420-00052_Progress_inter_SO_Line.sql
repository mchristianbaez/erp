/*************************************************************************
$Header TMS_20170420-00052_Progress_inter_SO_Line.sql $
Module Name: TMS_20170420-00052  Data Script Fix to Progress
Internal Sales Order Line for Line id 93928651
PURPOSE: Data Script Fix to Progress Internal Sales Order Line
REVISIONS:
Ver        Date         Author                Description
---------  -----------  ------------------    --------------------------
1.0        31-MAY-2017  Sundaramoorthy        TMS#20170420-00052
**************************************************************************/

clear buffer; 
SET serveroutput ON size 500000

DECLARE
  l_line_id  NUMBER:=93928651;
  l_result   VARCHAR2(30);
  l_file_val VARCHAR2(500);

  BEGIN
     DBMS_OUTPUT.put_line ('TMS: 20170420-00052 , Before Block');
     
	 oe_debug_pub.debug_on;
     oe_debug_pub.initialize;
     l_file_val:=OE_DEBUG_PUB.Set_Debug_Mode('FILE');
     oe_Debug_pub.setdebuglevel(5);
     DBMS_OUTPUT.put_line(' Inside the script');
     DBMS_OUTPUT.put_line('file path is:'||l_file_val);
         
    OE_Standard_WF.OEOL_SELECTOR
                             (p_itemtype => 'OEOL'
                             ,p_itemkey => to_char(l_line_id)
                             ,p_actid => 12345
                             ,p_funcmode => 'SET_CTX'
                             ,p_result => l_result
                             );

	oe_debug_pub.add('Retrun value of OEOL_selector function is'||l_result);
     wf_engine.handleError('OEOL', TO_CHAR(l_line_id),'INVOICE_INTERFACE','SKIP','COMPLETE');
     oe_debug_pub.debug_off;
     
	 DBMS_OUTPUT.put_line ('TMS: 20170420-00052 , Script Completed');

	 EXCEPTION
WHEN OTHERS THEN
  DBMS_OUTPUT.put_line('Error occurred: ' || SQLERRM);
  Oe_debug_pub.ADD('Error occurred: ' || SQLERRM);
  ROLLBACK;
END;
/