CREATE OR REPLACE PACKAGE BODY APPS.XXWC_AR_BILL_TRUST_INTF_PKG
AS        
   /*************************************************************************
    Copyright (c) 2012 Lucidity Consulting Group
    All rights reserved.
   **************************************************************************
     $Header xxwc_ar_bill_trust_intf_pkg $
     Module Name: xxwc_ar_bill_trust_intf_pkg.pks

     PURPOSE:   This package is called by the concurrent programs
                XXWC Bill Trust Open Invoice Outbound Interface
                XXWC Bill Trust Monthly Statements Outbound Interface

     REVISIONS:
     Ver        Date        Author                     Description
     ---------  ----------  ---------------         -------------------------
     1.0        01/11/2012  Consuelo Gonzalez      Initial Version
     2.0        05/15/2012  Consuelo Gonzalez      Modified to integrate
                                                   viveks initial code
     3.0        06/06/2012  Consuelo Gonzalez      Altered to loop prism notes
                                                   with prism original line number
                                                   Changes to pull the PRISM
                                                   Rental dates from new stg tbls
     4.0        06/08/2012  Consuelo Gonzalez      Altered to pull individual
                                                   PRISM notes per line instead
                                                   of grouping and single record
                                                   Modify number format for dollar
                                                   amounts to be 999,999,999.00
     5.0        06/18/2012  Consuelo Gonzalez      Modified gen_open_invoice_file to:
                                                   * Pull notes from OM directly from
                                                     order lines
                                                   * Altered header level where clause
                                                     to exclude source for late charges
     6.0        07/03/2012  Consuelo Gonzalez      Modified gen_open_balance to alter
                                                   invoice numbers created prior to 06/14
                                                   to BT, they were sent by Prism and number
                                                   is different from the one in EBS.
     7.0        07/11/2012  Consuelo Gonzalez      Modified gen_open_balance to alter
                                                   invoice number created prior to 06/14
                                                   only removing -00
     8.0        08/08/2012  Consuelo Gonzalez      Changed currency format to 999,999,999,000
                                                   on the invoice extract
     9.0        08/15/2012  Consuelo Gonzalez      Changed unit price rounding to be dependent
                                                   on decimal places with a minimum of 2 and
                                                   a max of 5. Trimming trailing 0s.
    10.0        09/06/2012  Consuelo Gonzalez      gen_open_invoice_file: Added section to OM 
                                                   Line notes to pull in serial numbers in the 
                                                   WDD Attribute1 field   
    11.0        09/28/2012  Consuelo Gonzalez      gen_open_invoice_file: changed inclusion
                                                   exclusion of CM transactions
    12.0        10/11/2012  Consuelo Gonzalez      Changes for CP# 872/ TMS 20121217-00785: to show CM Application
                                                   Changes for CP# 923/ TMS 20121217-00710: for repair order tool detail
    13.0        12/20/2012  Consuelo Gonzalez      Changes for CP# 2001/ TMS 20121217-00744: Added GSA Notes from the order line
                                                   Changes for CP# 2030/ TMS 20121217-00640: Changes for C! 1% Surcharge in Comm  
    14.0        02/18/2013  Consuelo Gonzalez      Changes for CP# 872/ TMS 20121217-00785: to dispaly CM Remaining balance instead
                                                            of original amounts as with regular invoices.
                                                   Changes for TMS 20130214-01693: To remove line feed from item description                     
    15.0        06/21/2013  Gopi Damuluri          TMS: 20130621-01119 Header level notes (Shipping instructions) for the rental orders should be reflected 

on bill trust invoices
                                                   Zero dollar credit memos for rental orders should be displayed as REM type
                                                   Resolve issues with Control Characters
    16.0        09/03/2013  Gopi Damuluri          TMS# 20130312-01672 Added Logic for Reference Invoices.
    17.0        09/24/2013  Gopi Damuluri          TMS# 20130823-00477
                                                   -- Remove Packing Instructions
                                                   -- Include header Shipping Instructions for all Order Types
    18.0        12/20/2013  Gopi Damuluri          TMS# 20131016-00409
                                                   -- Add CountryOfOrigin
                                                   -- Add DeliveryId
                                                   -- Add GSA Compliance and TAA Compliance

    19.0    02/25/2014  Harsha Yedla       TMS#20140203-00308--Bill Trust Invoice Feed - needs special character processing on Received By Field
    19.1        05/21/2014  Gopi Damuluri          TMS# 20140528-00047
                                                   Logic to derive Send_To_Customer Flag implemented for B2B Integration process.
                                                   This flag will help BillTrust decide whether to send the Invoice to Customer or hold it.
   20.0       07/23/2014  Maharajan Shunmugam     TMS# 20140723-00171 New parameter'Apply unearn discounts' handling in process lockbox
   21.0       07/30/2014  Maharajan Shunmugam     TMS# 20140731-00049 New parameter'No of instances' handling in process lockbox        
   22.0       09/04/2014  Gopi Damuluri           TMS# 20140903-00159 Change the Concurrent Program Wait Time to 21600 seconds (6hrs)
   23.0       09/21/2014  Gopi Damuluri           TMS# 20140903-00159 Add Trace to BillTrust Invoice Extract Process
   24.0       02/25/2015  RAGHAVENDRA S           TMS# 20150217-00170 Add Function to remove the line breaks for order by column 
   25.0       04/21/2015  Maharajan Shunmugam     TMS# 20150420-00037 Billtrust invoice extract not pulling 1% lumber tax assessment correctly 
   26.0       05/28/2015  Pattabhi Avula          TMS# 20150521-00030 Commented the Trace scripts to BillTrust Invoice Extract Process			
   27.0       05/02/2015  Raghavendra S           TMS# 20150303-00075 - FIN - Addition of Code Snippet for Line Breaks in All EDW Extract Programs
                                                  and Views for all free entry text fields.   
   28.0       10/02/2015  Maharajan Shunmugam    TMS#20150805-00039 Billtrust - Improvements to BT extract logic
   29.0       01/08/2016  Kishorebabu V          TMS#20150804-00061 AR - Billtrust lockbox program sending developer alerts for data related issues
   **************************************************************************/

   /*************************************************************************
     Procedure : Write_Log

     PURPOSE:   This procedure logs debug message in Concurrent Log file
     Parameter:
            IN
                p_debug_msg      -- Debug Message
   ************************************************************************/
   PROCEDURE write_log (p_debug_msg IN VARCHAR2)
   IS
   BEGIN
      fnd_file.put_line (fnd_file.LOG, p_debug_msg);
   END write_log;

   /*************************************************************************
      Procedure : Write_Error

     PURPOSE:   This procedure logs error message
     Parameter:
            IN
                p_debug_msg      -- Debug Message
   ************************************************************************/

   --add message to concurrent output file
   PROCEDURE write_error (p_debug_msg IN VARCHAR2)
   IS
      l_req_id          NUMBER        := fnd_global.conc_request_id;
      l_err_callfrom    VARCHAR2 (75) DEFAULT 'XXWC_AR_BILL_TRUST_INT_PKG';
      l_err_callpoint   VARCHAR2 (75) DEFAULT 'START';
      l_distro_list     VARCHAR2 (75)
                                   DEFAULT 'HDSOracleDevelopers@hdsupply.com';
   BEGIN
      xxcus_error_pkg.xxcus_error_main_api
         (p_called_from            => l_err_callfrom,
          p_calling                => l_err_callpoint,
          p_request_id             => l_req_id,
          p_ora_error_msg          => SUBSTR (p_debug_msg, 1, 2000),
          p_error_desc             => 'Error running xxwc_billtrust_intf_pkg with PROGRAM ERROR',
          p_distribution_list      => l_distro_list,
          p_module                 => 'AR'
         );
   END write_error;

   /*************************************************************************
     Procedure : gen_open_invoice_file

     PURPOSE:   This procedure creates file for the open invoices to
                Bill Trust

     REVISIONS:
     Ver        Date        Author                     Description
     ---------  ----------  ---------------         -------------------------
    19.1        05/21/2014  Gopi Damuluri          TMS# 20140528-00047
                                                   Logic to derive Send_To_Customer Flag implemented for B2B Integration process.
                                                   This flag will help BillTrust decide whether to send the Invoice to Customer or hold it.
    23.0        09/21/2014  Gopi Damuluri          TMS# 20140903-00159 Add Trace to BillTrust Invoice Extract Process
    25.0        04/22/2014  Maharajan Shunmugam    TMS# 20150420-00037 Billtrust invoice extract not pulling 1% lumber tax assessment correctly
    26.0       05/28/2015  Pattabhi Avula          TMS# 20150521-00030 Commented the Trace scripts to BillTrust Invoice Extract Process			
    27.0        05/02/2015  Raghavendra S          TMS# 20150303-00075 - FIN - Addition of Code Snippet for Line Breaks in All EDW Extract Programs
                                                   and Views for all free entry text fields.
   ************************************************************************/
   PROCEDURE gen_open_invoice_file (
      errbuf             OUT      VARCHAR2,
      retcode            OUT      VARCHAR2,
      p_directory_name   IN OUT   VARCHAR2,
      p_file_name        IN OUT   VARCHAR2,
      p_from_date        IN       VARCHAR2,
      p_to_date          IN       VARCHAR2
   )
   IS
      CURSOR cur_operating_units
      IS
         /*SELECT DISTINCT (x1.org_id) org_id, ou.organization_id,
                         ou.NAME org_name, ou.location_id, hl.address_line_1,
                         hl.address_line_2, hl.town_or_city, hl.region_2,
                         hl.postal_code, hl.region_1, hl.country
                    FROM xxwc_bill_trust_inv_headers_vw x1,
                         hr_all_organization_units ou,
                         hr_locations hl
                   WHERE x1.org_id = ou.organization_id
                     AND ou.location_id = hl.location_id
                ORDER BY ou.organization_id;*/
          SELECT   DISTINCT (ou.organization_id) org_id, ou.organization_id,
                     ou.NAME org_name, ou.location_id, hl.address_line_1,
                     hl.address_line_2, hl.town_or_city, hl.region_2,
                     hl.postal_code, hl.region_1, hl.country
                FROM hr_all_organization_units ou,
                     hr_locations hl
               WHERE ou.organization_id = 162 -- Need to set to profile
                 AND ou.location_id = hl.location_id
            ORDER BY ou.organization_id;

      CURSOR cur_inv_headers (c_from_date DATE, c_to_date DATE, c_org_id NUMBER)
      IS
         SELECT   x1.*
             FROM xxwc_bill_trust_inv_headers_vw x1
            WHERE x1.org_id = c_org_id  
              AND (   (    c_from_date IS NOT NULL
                       AND c_to_date IS NOT NULL
                       AND TRUNC (x1.invoice_date) BETWEEN TRUNC (c_from_date)
                                                       AND TRUNC (c_to_date)
                      )
                   OR (    c_from_date IS NULL
                       AND c_to_date IS NULL
                       AND x1.bt_send_date IS NULL
                      )
                  )
              -- 03/08/2012 CGonzalez: Exclude all Rebill transactions
              -- 06/05/2012 CGonzalez: NVLd in case manuals didnt have a batch
              -- 06/18/2012 CGonzalez: Modified to also exclude late charges source
              -- 09/28/2012 CGonzalez: Change to include Closed OM CMs
              AND ( 
                    ( UPPER (NVL (x1.batch_source, 'ZZZ')) NOT IN ('REBILL', 'REBILL-CM', 'LATE CHARGES SOURCE') )
                    OR
                    (
                      UPPER (NVL (x1.batch_source, 'ZZZ')) IN ('ORDER MANAGEMENT', 'REPAIR OM SOURCE', 'STANDARD OM SOURCE')
                      AND x1.transaction_type = 'CRM'
                    )
                  )
              -- 03/23/2012 CGonzalez: Excluded COD and Preffered Cash
              -- 06/05/2012 CGonzalez: NVL Payment term to pick DM/CM that naturally dont have a term
              AND (NVL (x1.terms, 'ZZZ') NOT IN ('COD', 'PRFRDCASH')
                    OR
                   NVL (x1.acct_payment_term, 'ZZZ') NOT IN ('COD', 'PRFRDCASH')
                  )
         ORDER BY x1.invoice_number;

      CURSOR cur_inv_lines (c_invoice_id NUMBER)
      IS
         SELECT   x1.*
             FROM xxwc_bill_trust_inv_lines_vw x1
            WHERE x1.invoice_id = c_invoice_id
         -- 02/18/2013 CGonzalez: Changed to sort with the new line number (OM LIne number and Prism Line Number)
         ORDER BY x1.new_line_number;

      -- 04/01/2012 CG To Pull Prism Line Notes
      -- 06/08/2012 CG Modified the source of the data for the line notes
      -- from table xxwc.xxwc_ar_prism_inv_notes_tbl to
      -- xxwc.XXWC_AR_PRISM_INV_LN_NOTES_STG
      CURSOR cur_prism_ln_note (c_trx_number VARCHAR2, c_line_number VARCHAR2)
      IS
         SELECT DISTINCT (trx_number), line_no, line_order, sequencenumber,
                         description
                    FROM xxwc.xxwc_ar_prism_inv_ln_notes_stg
                   WHERE trx_number = c_trx_number
                     AND line_no = c_line_number
                     AND line_order != 1
                     AND PRINT = 'Y'
                ORDER BY line_order, sequencenumber;

      
      -- 06/11/2012 CG: added to breakout shipping/packing instructions
      -- from OM that have CRs
      -- 06/18/2012 CG: Modified to pull directly from OM Lines
      -- 09/06/2012 CG: Added block to pull serial numbers from
      -- WDD Attribute1 as notes for BT. Set in LVL1, moved Ship
      -- notes and packing notes to LVL2 and LVL3 
      -- 01/09/13 CG: Changing code for shipping and packing instruction
      -- pull to improve performance     
      CURSOR cur_oracle_ln_note (c_oe_line_id NUMBER)
      IS
         -- 01/09/13 CG: Added for performance
         WITH    cntr1     AS
                (
                    SELECT  LEVEL AS n
                    FROM    dual
                    CONNECT BY  LEVEL <= ( SELECT   MAX ( LENGTH (oel1.shipping_instructions) - LENGTH (REPLACE (oel1.shipping_instructions, CHR (10))) ) + 1
                                           FROM     oe_order_lines_all oel1
                                           WHERE    oel1.line_id = c_oe_line_id AND oel1.shipping_instructions IS NOT NULL
                                         )
                )
/*  -- Version# 17.0 Commented Start >    , cntr2     AS
                (
                    SELECT  LEVEL AS n
                    FROM    dual
                    CONNECT BY  LEVEL <= ( SELECT   MAX ( LENGTH (oel1.packing_instructions) - LENGTH (REPLACE (oel1.packing_instructions, CHR (10))) ) + 1
                                           FROM     oe_order_lines_all oel1
                                           WHERE    oel1.line_id = c_oe_line_id AND oel1.packing_instructions IS NOT NULL
                                         )
                )    */ -- Version# 17.0 Commented End <
         -- 01/09/13 CG: Added for performance
-- Version# 18.0 > Start
         SELECT line_id, 
                1 gsa_lvl, 
                'COUNTRY_CODE'                                                                                                 country_code,
                XXWC_OM_CFD_PKG.GET_COUNTRY_OF_ORIGIN(ool.invoice_to_org_id, ool.inventory_item_id)     om_note,
                rownum                                                                                                         rid
           FROM oe_order_lines_all                                                                                             ool
          WHERE 1 = 1
            AND ool.line_id                  = c_oe_line_id
         UNION
-- Version# 18.0 < End
         select oola.line_id
              , 2 SN_LVL
--              , wdd.attribute1 serial_number_list
--              , ('SN: '||wdd.attribute1) om_note
              , oola.attribute7 serial_number_list
              , ('Serial Number: '||oola.attribute7) om_note
              , 1 rid                                         
         from   oe_order_lines_all oola
         where  oola.line_id = c_oe_line_id
         and    oola.attribute7 is not null
         UNION
         -- 01/09/13 CG: Changed for performance
         select oel2.line_id
              , 3 ship_lvl
              , oel2.shipping_instructions
              , REGEXP_SUBSTR (shipping_instructions
                                , '[^' || CHR (10) || ']+'
                                , 1
                                , n) om_note
              , n rid
           FROM oe_order_lines_all oel2
           JOIN cntr1 ON cntr1.n <= LENGTH (oel2.shipping_instructions) - LENGTH( REPLACE(oel2.shipping_instructions, CHR(10) ) ) + 1
         AND   oel2.line_id = c_oe_line_id AND oel2.shipping_instructions IS NOT NULL  
/*       UNION  -- Version# 17.0 -- Commented > Start
         -- 01/09/13 CG: Changed for performance
         select oel2.line_id
                , 4 pck_lvl
                , oel2.packing_instructions
                , REGEXP_SUBSTR (packing_instructions
                                  , '[^' || CHR (10) || ']+'
                                  , 1
                                  , n) om_note
                , n rid
         FROM   oe_order_lines_all oel2
         JOIN   cntr2 ON cntr2.n <= LENGTH (oel2.packing_instructions) - LENGTH( REPLACE(oel2.packing_instructions, CHR(10) ) ) + 1
         AND    oel2.line_id = c_oe_line_id AND oel2.packing_instructions IS NOT NULL 
         -- Version# 17.0 -- Commented < End
*/         UNION
         -- 12/21/12 CG: Added for CP# 2001/ TMS 20121217-00744
             SELECT line_id, 
                    5 gsa_lvl, 
                    attribute14 gsa_note,
                    attribute14 om_note,
                    /*REGEXP_SUBSTR (attribute14,
                                   '[^' || CHR (10) || ']+',
                                   1,
                                   LEVEL
                                  ) om_note,*/
                    -- LEVEL rid
                    rownum rid
               FROM oe_order_lines_all
              WHERE attribute14 IS NOT NULL
                AND line_id = c_oe_line_id
         /*CONNECT BY LEVEL <=
                         LENGTH (attribute14)
                       - LENGTH (REPLACE (attribute14, CHR (10)))
                       + 1
                AND line_id = PRIOR line_id
                AND PRIOR DBMS_RANDOM.VALUE IS NOT NULL*/
           ORDER BY 2, 5;

      -- 10/11/2012 CG: Added for CP #923 to add repair tool information
      -- 10/11/2012 CG: Added another parameter for the invoice_id 
      -- to pull the CM applications
      CURSOR cur_repair_info (c_oe_line_id NUMBER, c_invoice_id NUMBER, c_invoice_line_id NUMBER) is -- Version# 16.0
      -- 10/11/2012 CG: Added
         select app.customer_trx_id
                , 1 CM_APP_LVL
                , ('Inv '||rcta.trx_number||' amt '||TO_CHAR(app.amount_applied,'FM9999999.90')) App_Data
                , ('Applied to '||rcta.trx_number||' $'||TO_CHAR(app.amount_applied,'FM9999999.90')) om_note
                , rownum rid
             from ar_receivable_applications_all app
                , ra_customer_trx_all            rcta
         where  app.application_type = 'CM'
         and    app.display = 'Y'
         and    app.customer_trx_id = c_invoice_id
         and    app.applied_customer_trx_id = rcta.customer_trx_id
         UNION -- Version# 16.0 > Start
         select rctl_r.customer_trx_id
              , 1 CM_APP_LVL
              ,  ('Inv '||rctl_r.customer_trx_id) App_Data
              , ('Reference Invoice# '||rcta.trx_number) om_note
              , rownum rid
         from oe_order_lines_all        ool
            , ra_customer_trx_lines_all rctl
            , ra_customer_trx_lines_all rctl_r
            , ra_customer_trx_all       rcta
        where 1 = 1
          and ool.line_type_id               = 1008                                 -- CREDIT ONLY
          and rctl_r.customer_trx_id         = c_invoice_id
          and rctl_r.customer_trx_line_id    = c_invoice_line_id
          and to_char(ool.LINE_ID)           = rctl_r.interface_line_attribute6
          and to_char(ool.REFERENCE_LINE_ID) = rctl.interface_line_attribute6
          and rcta.customer_trx_id           = rctl.customer_trx_id
          and NOT EXISTS (SELECT '1' FROM ar_receivable_applications_all app
                 WHERE 1 = 1
                   AND app.application_type = 'CM'
                   AND app.display          = 'Y'
                   AND app.customer_trx_id  = rctl_r.customer_trx_id
                          )
          -- Version# 16.0 < End
         UNION
         SELECT oel.line_id
               , 2
               , msi.concatenated_segments repair_item
               , (csl1.meaning||' billing for part '||msi.concatenated_segments||' on SR '||cia.incident_number||' and Repair '||cr.repair_number) om_note    

   
               , cral.repair_actual_line_id rid       
          FROM   csd_repairs cr
               , cs_incidents_all_b cia
               , cs_estimate_details ced
               , csd_repair_actuals cra
               , csd_repair_actual_lines cral
               , oe_order_headers_all oeh
               , oe_order_lines_all oel
               , mtl_system_items_vl msi
               , cs_transaction_types_vl stt
               , cs_txn_billing_types sbt
               , cs_lookups csl
               , cs_billing_type_categories cbtc
               , cs_lookups csl1
               , mtl_system_items_vl msi1
         WHERE         cr.incident_id = cia.incident_id
                 AND cr.repair_line_id = cra.repair_line_id
                 AND cra.repair_actual_id = cral.repair_actual_id
                 AND cral.estimate_detail_id = ced.estimate_detail_id
                 AND ced.original_source_code = 'DR'
                 AND cr.inventory_item_id = msi.inventory_item_id
                 AND msi.organization_id = cs_std.get_item_valdn_orgzn_id
                 AND sbt.txn_billing_type_id = ced.txn_billing_type_id
                 AND sbt.transaction_type_id = stt.transaction_type_id
                 AND ced.order_header_id = oeh.header_id(+)
                 AND ced.order_line_id = oel.line_id(+)
                 AND csl.lookup_type = 'CS_BILLING_CATEGORY'
                 AND csl.lookup_code = cbtc.billing_category
                 AND csl1.lookup_type = 'MTL_SERVICE_BILLABLE_FLAG'
                 AND csl1.lookup_code = cbtc.billing_type
                 AND sbt.billing_type = cbtc.billing_type
                 AND csl.enabled_flag = 'Y'
                 AND TRUNC (SYSDATE) BETWEEN TRUNC ( NVL (csl.start_date_active, SYSDATE)  )
                                         AND  TRUNC ( NVL (csl.end_date_active, SYSDATE) )
                 AND cral.replaced_item_id = msi1.inventory_item_id(+)
                 AND msi1.organization_id(+) = cs_std.get_item_valdn_orgzn_id
                 AND oel.line_id = c_oe_line_id  
        Order by 2,5;

-- Version# 15 > Start
      CURSOR cur_oracle_rent_note (c_oe_num NUMBER, c_trx_id NUMBER) -- Version# 18.0
      IS
         WITH    cntr1     AS
                (
                    SELECT  LEVEL AS n
                    FROM    dual
                    CONNECT BY  LEVEL <= ( SELECT   MAX ( LENGTH (oeh1.shipping_instructions) - LENGTH (REPLACE (oeh1.shipping_instructions, CHR (10))) ) + 1
                                           FROM     oe_order_headers_all oeh1
                                           WHERE    oeh1.order_number = c_oe_num AND oeh1.shipping_instructions IS NOT NULL
                                         )
                )
               , cntr2     AS
                (
                    SELECT  LEVEL AS n
                    FROM    dual
                    CONNECT BY  LEVEL <= ( SELECT   MAX ( LENGTH (cr.problem_description) - LENGTH (REPLACE (cr.problem_description, CHR (10))) ) + 1
                                             FROM cs_estimate_details  ced
                                                , csd_repairs          cr
                                                , oe_order_headers_all ooh
                                            WHERE   1 = 1
                                              AND ooh.order_number         = c_oe_num
                                              AND ced.order_header_id      = ooh.header_id 
                                              AND ced.original_source_id   = cr.repair_line_id
                                              AND ced.original_source_code = 'DR'
                                              AND cr.problem_description IS NOT NULL
                                              AND rownum = 1
                                         )
                ) 
         select oeh2.header_id
                , oeh2.shipping_instructions
                , DECODE(rownum,1,'Shipping Notes: ','')||REGEXP_SUBSTR (shipping_instructions
                                  , '[^' || CHR (10) || ']+'
                                  , 1
                                  , n) om_note
                , rownum rid
         FROM   oe_order_headers_all oeh2
         JOIN   cntr1 ON cntr1.n <= LENGTH (oeh2.shipping_instructions) - LENGTH( REPLACE(oeh2.shipping_instructions, CHR(10) ) ) + 1
         AND    oeh2.order_number = c_oe_num AND oeh2.shipping_instructions IS NOT NULL  
         UNION
--         (SELECT 1 header_id
--              , cr.problem_description
--              , REGEXP_SUBSTR (problem_description
--                                , '[^' || CHR (10) || ']+'
--                                , 1
--                                , n) om_note
--              , n rid
--           FROM csd_repairs cr
--           JOIN cntr2 ON cntr2.n <= LENGTH (cr.problem_description) - LENGTH( REPLACE(cr.problem_description, CHR(10) ) ) + 1
--            AND cr.problem_description IS NOT NULL
--            AND rownum = 1)
         (SELECT cr2.header_id
              , cr2.problem_description
              , DECODE(rownum,1,'Repair Notes: ','')||REGEXP_SUBSTR (cr2.problem_description
                                , '[^' || CHR (10) || ']+'
                                , 1
                                , n) om_note
              , rownum rid
           FROM (SELECT ooh.header_id
                      , cr.problem_description 
                   FROM cs_estimate_details ced
                      , csd_repairs cr
                      , oe_order_headers_all ooh
                  WHERE 1 = 1
                    AND ooh.order_number         = c_oe_num
                    AND ced.order_header_id      = ooh.header_id 
                    AND ced.original_source_id   = cr.repair_line_id
                    AND ced.original_source_code = 'DR'
                    AND cr.problem_description IS NOT NULL
                    AND rownum = 1) cr2
           JOIN cntr2 ON cntr2.n <= LENGTH (cr2.problem_description) - LENGTH( REPLACE(cr2.problem_description, CHR(10) ) ) + 1)
-- Version# 18.0 > Start
           UNION
               ( SELECT c_trx_id
                      , 'Delivery_Id' Delivery_Id
                      , get_delivery_id(c_trx_id) om_note
                      , 1 rid
                   FROM dual)
-- Version# 18.0 < End
      ORDER BY 1, 4;
      
-- Version# 15 < End

      /*CURSOR cur_oracle_ln_note (
         c_invoice_line_id NUMBER)
      IS
             SELECT invoice_line_id
                   ,1 ship_lvl
                   ,shipping_instructions
                   ,REGEXP_SUBSTR (shipping_instructions
                                  ,'[^' || CHR (10) || ']+'
                                  ,1
                                  ,LEVEL)
                       om_note
                   ,LEVEL rid
               FROM xxwc_bill_trust_inv_lines_vw
              WHERE     shipping_instructions IS NOT NULL
                    AND invoice_line_id = c_invoice_line_id
         CONNECT BY     LEVEL <=
                             LENGTH (shipping_instructions)
                           - LENGTH (
                                REPLACE (shipping_instructions, CHR (10)))
                           + 1
                    AND invoice_line_id = PRIOR invoice_line_id
                    AND PRIOR DBMS_RANDOM.VALUE IS NOT NULL
         UNION
             SELECT invoice_line_id
                   ,2 pck_lvl
                   ,packing_instructions
                   ,REGEXP_SUBSTR (packing_instructions
                                  ,'[^' || CHR (10) || ']+'
                                  ,1
                                  ,LEVEL)
                       om_note
                   ,LEVEL rid
               FROM xxwc_bill_trust_inv_lines_vw
              WHERE     packing_instructions IS NOT NULL
                    AND invoice_line_id = c_invoice_line_id
         CONNECT BY     LEVEL <=
                             LENGTH (packing_instructions)
                           - LENGTH (
                                REPLACE (packing_instructions, CHR (10)))
                           + 1
                    AND invoice_line_id = PRIOR invoice_line_id
                    AND PRIOR DBMS_RANDOM.VALUE IS NOT NULL
         ORDER BY 1, 2, 5;*/

      /*  -- 06/08/2012: Changed to pull from a different notes table  (line Level)
        select  distinct(trx_number)
                , line_number
                , sequence_number
                , description prism_line_note
        from    xxwc.xxwc_ar_prism_inv_notes_tbl x1
        where   x1.trx_number = c_trx_number
        and     to_char(x1.line_number) = c_line_number
        and     x1.description is not null
        and     nvl(x1.print_flag, 'N') = 'Y'
        order by x1.sequence_number;*/

      -- Cursor to pull messages? notes located elsewheree
      v_file                UTL_FILE.file_type;
      l_filename            VARCHAR2 (240);
      l_from_date           DATE;
      l_to_date             DATE;
      l_cur_date            VARCHAR2 (240);
      l_count_hdrs          NUMBER;
      l_count_lns           NUMBER;
      l_prism_notes         VARCHAR2 (2000);
      l_prism_note_ln       NUMBER;
      -- 04/12/2012 Added Rental Data
      l_rental_start_date   DATE;
      l_rental_end_date     DATE;
      l_rental_type         VARCHAR2 (240);
      l_line_number         VARCHAR2 (15);
      l_order_qty           NUMBER;
      l_shipped_qty         NUMBER;
      l_unit_price          VARCHAR2 (240);
-- 12/03/2012 CG: Added for CP #872 Modify credit memo document to show invoices applied through the auto-application process, with a remaining balance of $0
      l_total_amt_applied   NUMBER;
      l_amount_remaining    NUMBER;
      
      -- 12/21/2012 CG: Added for CP#2030/TMS 20121217-00640 Modify tax process to charge a 1% surcharge on commodity items for CA orders
      l_line_tax_amt        NUMBER;
      l_line_tax_rate       NUMBER;
      l_total_tax_amt       NUMBER;
      l_max_line_number     NUMBER;
      l_hdr_tax_amt         NUMBER;
      l_hdr_tax_rate        NUMBER;
      l_item_tax_code       VARCHAR2(240);
      l_rent_note_exists    NUMBER; -- Version# 15
      
      l_send_to_cust_flag   VARCHAR2(1);      -- Version# 19.1
      l_deliver_invoice     VARCHAR2(1);      -- Version# 19.1
      l_order_source        VARCHAR2(240);    -- Version# 19.1
   BEGIN
   
/* Version# 27 Starts -- Commented trace scripts by pattabhi for TMS#20150521-00030
EXECUTE IMMEDIATE 'alter session set sql_trace=true'; -- Version# 23
EXECUTE IMMEDIATE 'alter session set events ''10046 trace name context forever, level 12'''; -- Version# 23
EXECUTE IMMEDIATE 'ALTER SESSION SET tracefile_identifier=BillTrust'; -- Version# 23
Version# 27 Ends -- Commented trace scripts by pattabhi for TMS#20150521-00030 */   
      write_log ('Begining EBS To Bill Trust Open Invoice File Generation ');
      write_log ('========================================================');
      write_log ('  ');
      --Initialize the Out Parameter
      errbuf := '';
      retcode := '0';
      l_from_date := TO_DATE (p_from_date, 'YYYY/MM/DD HH24:MI:SS');
      l_to_date := TO_DATE (p_to_date, 'YYYY/MM/DD HH24:MI:SS');
      -- Setting current date for file header
      l_cur_date := NULL;

      SELECT TO_CHAR (SYSDATE, 'MM/DD/YYYY')
        INTO l_cur_date
        FROM DUAL;

      -- Displaying parameters passed in the concurrent program log file
      write_log ('Parameters');
      write_log (RPAD ('File Drop Directory: ', 25, ' ') || p_directory_name);
      write_log (RPAD ('File Name: ', 25, ' ') || p_file_name);
      write_log (   RPAD ('From Date: ', 25, ' ')
                 || TO_CHAR (l_from_date, 'MM/DD/YYYY')
                );
      write_log (   RPAD ('To Date: ', 25, ' ')
                 || TO_CHAR (l_to_date, 'MM/DD/YYYY')
                );

      IF p_file_name IS NULL
      THEN
         --Derive the file name in case the file name is not passed
         SELECT    'WC_AR_BILLTRUST_INVOICE_'
                || TO_CHAR (SYSDATE, 'MMDDYYYY_HH_MI_SS')
                || '.txt'
           INTO l_filename
           FROM DUAL;
      ELSE
         l_filename := p_file_name;
      END IF;

      write_log (RPAD ('File Name for Export: ', 25, ' ') || l_filename);
      write_log ('  ');
      write_log (' Opening the File handler');

      IF p_directory_name IS NULL
      THEN
         write_log (' Setting the directory');
         p_directory_name := 'ORACLE_INT_UC4';
      END IF;

      --Open the file handler
      v_file :=
         UTL_FILE.fopen (LOCATION       => p_directory_name,
                         filename       => l_filename,
                         open_mode      => 'w'
                        );
      write_log (' Writing to the file ... Opening Header Loop');
      l_count_hdrs := 0;
      l_count_lns := 0;

      FOR c0 IN cur_operating_units
      LOOP
         EXIT WHEN cur_operating_units%NOTFOUND;
         -- Writing File Header
         UTL_FILE.put_line (v_file,
                            UPPER (   'FH'
                                   || '|'
                                   || l_cur_date
                                   || '|'
                                   || 'WCI'
                                   || '|'
                                   || regexp_replace(c0.org_name,  '[[:cntrl:]]', ' ') -- Added for V 27.0
                                   || '|'
                                   || regexp_replace(c0.address_line_1,  '[[:cntrl:]]', ' ') -- Added for V 27.0
                                   || '|'
                                   || c0.town_or_city
                                   || '|'
                                   || c0.region_2
                                   || '|'
                                   || c0.postal_code
                                   || '|'
                                   || c0.region_1
                                   || '|'
                                   || '|'
                                  --02/20/12 CG Commented per UAT1 Issue #50
                                  --c0.country||'|'
                                  )
                           );

         FOR c1 IN cur_inv_headers (l_from_date, l_to_date, c0.org_id)
         LOOP
            EXIT WHEN cur_inv_headers%NOTFOUND;
            l_count_hdrs := l_count_hdrs + 1;
            -- 04/12/2012 CGonzalez: Added rental data
            l_rental_start_date := NULL;
            l_rental_end_date := NULL;
            l_rental_type := NULL;

            IF    UPPER (c1.order_type) LIKE '%RENTAL%'
               OR UPPER (c1.rental_type) LIKE '%RENTAL%'
            THEN
               BEGIN
                  SELECT x1.rental_ship_date, x1.rental_return_date
                    INTO l_rental_start_date, l_rental_end_date
                    FROM xxwc_bill_trust_inv_lines_vw x1
                   WHERE x1.invoice_id = c1.invoice_id
                     AND x1.rental_ship_date IS NOT NULL
                     AND ROWNUM = 1;
               EXCEPTION
                  WHEN OTHERS
                  THEN
                     l_rental_start_date := NULL;
                     l_rental_end_date := NULL;
               END;

               IF c1.batch_source NOT LIKE 'PRISM%'
               THEN
                  l_rental_type := c1.order_type;
               ELSE
                  l_rental_type := c1.rental_type;
               END IF;
            END IF;

            -- 04/12/2012 CGonzalez: Added rental data
            
            -- Version# 19.1 > Start
            l_send_to_cust_flag := 'Y';
            l_deliver_invoice   := 'N';
            l_order_source      := NULL;
            -------------------------------------------------------------------------
            -- Check "Deliver_Invoice" Flag from B2B Customer Info Table
            -------------------------------------------------------------------------
            BEGIN
              SELECT deliver_invoice
                INTO l_deliver_invoice
                FROM xxwc.xxwc_b2b_cust_info_tbl     stg
               WHERE stg.party_id                  = c1.party_id;
            EXCEPTION
            WHEN OTHERS THEN
              l_deliver_invoice := 'N';
            END;
            
            IF l_deliver_invoice = 'Y' THEN
               -------------------------------------------------------------------------
               -- Derive Order Source
               -------------------------------------------------------------------------
               BEGIN
                 SELECT oos.name
                   INTO l_order_source
                   FROM oe_order_headers_all  ooh
                      , oe_order_sources      oos
                  WHERE 1 = 1
                    AND ooh.order_number    = c1.order_number
                    AND ooh.order_source_id = oos.order_source_id
                    AND rownum = 1;
               EXCEPTION
               WHEN OTHERS THEN
                 l_order_source      := NULL;
               END;
               
               -------------------------------------------------------------------------
               -- Derive Send_To_Customer Flag
               -------------------------------------------------------------------------
               IF l_order_source = 'B2B LIASON' THEN
                  l_send_to_cust_flag := 'N';
               ELSE
                  l_send_to_cust_flag := 'Y';
               END IF;
            END IF;
            -- Version# 19.1 < End

            -- Writting Header Record
            UTL_FILE.put_line
               (v_file,
                UPPER
                   (   c1.header_record_type
                    || '|'
                    || c1.invoice_id
                    || '|'
                    || regexp_replace(c1.customer_account_number,  '[[:cntrl:]]', ' ') -- Added for V 27.0
                    || '|'
                    || regexp_replace(c1.customer_name, '[[:cntrl:]]', ' ') -- Added for V 27.0
                    || '|'
                    || regexp_replace(c1.remit_to_address_code,  '[[:cntrl:]]', ' ') -- Added for V 27.0
                    || '|'
                    || regexp_replace(c1.cust_ship_to_number,  '[[:cntrl:]]', ' ') -- Added for V 27.0
                    || '|'
                    || NULL
                    || '|'
                    ||  -- c1.cust_ship_to_name||'|'|| -- 03/12/2012 Blank out
                       regexp_replace(c1.cust_ship_to_location,  '[[:cntrl:]]', ' ') -- Added for V 27.0
                    || '|'
                    || regexp_replace(c1.cust_ship_to_address1,  '[[:cntrl:]]', ' ') -- Added for V 27.0
                    || '|'
                    || (   regexp_replace(c1.cust_ship_to_address2,  '[[:cntrl:]]', ' ')
                        || ' '
                        || regexp_replace(c1.cust_ship_to_address3,  '[[:cntrl:]]', ' ')
                       ) 
                    || '|'
                    || c1.cust_ship_to_city
                    || '|'
                    || c1.cust_ship_to_state_prov
                    || '|'
                    || c1.cust_ship_to_zip_code
                    || '|'
                    ||              --02/20/12 CG Commented per UAT1 Issue #50
                       NULL
                    || '|'
                    ||                        --c1.cust_ship_to_country||'|'||
                       regexp_replace(c1.cust_bill_to_number,  '[[:cntrl:]]', ' ') -- Added for V 27.0
                    || '|'
                    || NULL
                    || '|'
                    || -- 03/12/2012 Blank out and move to bill to location c1.cust_bill_to_name||'|'||
                       regexp_replace(c1.cust_bill_to_name,  '[[:cntrl:]]', ' ') -- Added for V 27.0
                    || '|'
                    ||           -- 03/12/2012 c1.cust_bill_to_location||'|'||
                       regexp_replace(c1.cust_bill_to_address1,  '[[:cntrl:]]', ' ') -- Added for V 27.0
                    || '|'
                    || regexp_replace(c1.cust_bill_to_address2,  '[[:cntrl:]]', ' ') -- Added for V 27.0
                    || '|'
                    || c1.cust_bill_to_city
                    || '|'
                    || c1.cust_bill_to_state_prov
                    || '|'
                    || c1.cust_bill_to_zip_code
                    || '|'
                    ||              --02/20/12 CG Commented per UAT1 Issue #50
                       NULL
                    || '|'
                    ||                        --c1.cust_bill_to_country||'|'||
                       c1.invoice_number
                    || '|'
                    || TO_CHAR (c1.invoice_date, 'MM/DD/YYYY')
                    || '|'
                    || regexp_replace(c1.po_number,  '[[:cntrl:]]', ' ')  --  Version# 15
                    || '|'
                    || regexp_replace(c1.order_number,  '[[:cntrl:]]', ' ')  -- Added for V 27.0  
                    || '|'
                    || c1.order_date
                    || '|'
                    || regexp_replace(c1.terms,  '[[:cntrl:]]', ' ')  --  Version# 15
                    || '|'
                    || regexp_replace(c1.ship_via,  '[[:cntrl:]]', ' ')  --  -- Added for V 27.0
                    || '|'
                    || regexp_replace(c1.ordered_by, '[[:cntrl:]]',' ')  --- added regexp_replace funtion for V 24.0
                    || '|'
                    || regexp_replace(c1.account_manager,  '[[:cntrl:]]', ' ')  -- Added for V 27.0
                    || '|'
                    || c1.taken_by
                    || '|'
                    || c1.acct_job_no
                    || '|'
                    || c1.customer_job_no
                    || '|'
                    || regexp_replace(c1.received_by,  '[[:cntrl:]]', ' ')     ----added regexp replace by harsha for removing CHRF TMS#20140203-00308 
                    || '|'
                    || c1.transaction_type
                    || '|'
                    || c1.branch_code
                    || '|'
                    || regexp_replace(c1.branch_address1,  '[[:cntrl:]]', ' ')  --  Added for V 27.0
                    || '|'
                    || regexp_replace(c1.branch_address2,  '[[:cntrl:]]', ' ')  --  Added for V 27.0
                    || '|'
                    || c1.branch_city
                    || '|'
                    || c1.branch_state
                    || '|'
                    || c1.branch_zip_code
                    || '|'
                    || regexp_replace(c1.branch_region_1,  '[[:cntrl:]]', ' ')  --  Added for V 27.0
                    || '|'
                    ||              --02/20/12 CG Commented per UAT1 Issue #50
                       NULL
                    || '|'
                    ||                              --c1.branch_country||'|'||
                       c1.branch_phone_number
                    || '|'
                    || '|'                                           /*||'|'||
                                     c1.territory_code*/
                    || regexp_replace(c1.whs_branch_name,  '[[:cntrl:]]', ' ')  --  Added for V 27.0
                    || '|'
                    || l_rental_type
                    || '|'
                    || TO_CHAR (l_rental_start_date, 'MM/DD/YYYY')
                    || '|'
                    || TO_CHAR (l_rental_end_date, 'MM/DD/YYYY')
                    || '|'                  -- 19.1
                    || l_send_to_cust_flag  -- 19.1
                   )
               );


/* Version# 17.0 -- Add Header Shipping Notes for all Order Types
            -- Rental Order Header Shipping Notes >> Version# 15 Start
            IF    UPPER (c1.order_type) LIKE '%RENTAL%'
               OR UPPER (c1.rental_type) LIKE '%RENTAL%'
            THEN
*/

            l_rent_note_exists := NULL;
            FOR c1A IN cur_oracle_rent_note (c1.order_number, c1.invoice_id)  -- Version# 18.0
            LOOP

             -- IF c1A.rid = 1 THEN
             
             IF l_rent_note_exists IS NULL AND TRIM(c1A.om_note) IS NOT NULL THEN -- Version# 18.0
               l_rent_note_exists := 1;
             
               UTL_FILE.put_line
                   (v_file,
                    UPPER (   'D'
                           || '|'
                           || c1.invoice_id
                           || '|'
                           || '0'
                           || '|'
                           || '0'
                           || '|'
                           || 'HDRDESC'
                           || '|'
                           || NULL
                           || '|'
                           || NULL
                           || '|'
                           || NULL -- uom_code
                           || '|'
                           || 1 --l_order_qty
                           || '|'
                           || 1 -- l_shipped_qty
                           || '|'
                           || 0 -- c2.qty_backordered
                           || '|'
                           || 0 -- l_unit_price
                           || '|'
                           || 0 -- extended_price
                           || '|'
                           || NULL
                           || '|'
                           || NULL
                           || '|'
                           || NULL
                           || '|'
                           || NULL
                           || '|'
                           || NULL
                           || '|'
                           || NULL
                           || '|'
                           || NULL
                           || '|'
                           || NULL
                           || '|'
                           || 0
                           || '|'
                           || NULL
                           || '|'
                           || NULL
                           || '|'
                           || NULL
                          )
                   );

              UTL_FILE.put_line (v_file,
                              UPPER (   'N'
                                     || '|'
                                     || c1.invoice_id
                                     || '|'
                                     || '0'
                                     || '|'
                                     || '********************************************'
                                     || '|'
                                     || NULL
                                     || '|'
                                    )
                             );
               END IF;

              UTL_FILE.put_line (v_file,
                              UPPER (   'N'
                                     || '|'
                                     || c1.invoice_id
                                     || '|'
                                     || '0'
                                     || '|'
                                     || TRIM (regexp_replace(c1A.om_note,  '[[:cntrl:]]', ' ') ) -- Version# 15
                                     || '|'
                                     || NULL
                                     || '|'
                                    )
                             );

            END LOOP;

             IF l_rent_note_exists = 1 THEN
              l_rent_note_exists := NULL;
              UTL_FILE.put_line (v_file,
                              UPPER (   'N'
                                     || '|'
                                     || c1.invoice_id
                                     || '|'
                                     || '0'
                                     || '|'
                                     || '********************************************'
                                     || '|'
                                     || NULL
                                     || '|'
                                    )
                             );
            END IF;  -- IF c1A.n = 1 THEN

--            END IF; -- IF    UPPER (c1.order_type) LIKE '%RENTAL%'  -- Version# 17.0 -- Add Header Shipping Notes for all Order Types

            -- Rental Order Header Shipping Notes << Version# 15 End

            -- Opening Loop to Pull Invoice Lines
            -- 12/21/2012 CG: Added for CP#2030/TMS 20121217-00640 Modify tax process to charge a 1% surcharge on commodity items for CA orders
            write_log ('Invoice Number: '||c1.invoice_number);
            l_total_tax_amt       := 0;
            FOR c2 IN cur_inv_lines (c1.invoice_id)
            LOOP
               EXIT WHEN cur_inv_lines%NOTFOUND;
               
               write_log ('c2.line_number ' || c2.line_number);
               write_log ('c2.new_line_number ' || c2.new_line_number);
               
               l_count_lns := l_count_lns + 1;
               l_order_qty := NULL;
               l_shipped_qty := NULL;
               l_line_number := NULL;

               IF UPPER (c2.part_number) = 'HDRSHIP'
               THEN
                  l_order_qty := 1;
                  l_shipped_qty := 1;
               ELSE
                  l_order_qty := c2.qty_ord;
                  l_shipped_qty := c2.qty_shipped;
               END IF;

               -- 08/15/2012 CG: Changed to accomodate request for dynamic precision
               -- of unit price based on data, with a minimum of 2 dec places and
               -- a max of 5, without trailling 0s.
               l_unit_price := NULL;

               BEGIN
                  SELECT
                 --TRIM (TO_CHAR (price, '999,999,990.0000')) unit_price_now,
                         (CASE
                             WHEN (LENGTH (SUBSTR (TO_CHAR (c2.price),
                                                     INSTR (TO_CHAR (c2.price),
                                                            '.'
                                                           )
                                                   + 1
                                                  )
                                          )
                                  ) <= 2
                                THEN TRIM (TO_CHAR (c2.price,
                                                    '999,999,990.00')
                                          )
                             WHEN (LENGTH (SUBSTR (TO_CHAR (c2.price),
                                                     INSTR (TO_CHAR (c2.price),
                                                            '.'
                                                           )
                                                   + 1
                                                  )
                                          )
                                  ) = 3
                                THEN TRIM (TO_CHAR (c2.price,
                                                    '999,999,990.000'
                                                   )
                                          )
                             WHEN (LENGTH (SUBSTR (TO_CHAR (c2.price),
                                                     INSTR (TO_CHAR (c2.price),
                                                            '.'
                                                           )
                                                   + 1
                                                  )
                                          )
                                  ) = 4
                                THEN TRIM (TO_CHAR (c2.price,
                                                    '999,999,990.0000'
                                                   )
                                          )
                             ELSE TRIM (TO_CHAR (c2.price,
                                                 '999,999,990.00000')
                                       )
                          END
                         ) unit_price_going_fwd
                    INTO l_unit_price
                    FROM DUAL;
               EXCEPTION
                  WHEN OTHERS
                  THEN
                     l_unit_price :=
                               TRIM (TO_CHAR (c2.price, '999,999,990.00000'));
               END;

               -- 12/21/2012 CG: Added for CP#2030/TMS 20121217-00640 Modify tax process to charge a 1% surcharge on commodity items for CA orders
               l_line_tax_amt        := 0;
               l_line_tax_rate       := 0;
               IF c1.trx_ship_state = 'CA' or c1.pr_ship_state = 'CA' 
               THEN
                   write_log ('Tax Test - State: '||c1.trx_ship_state ||' - '||c1.pr_ship_state);
                   
                   l_item_tax_code := null;
                   begin
                    select  msib.attribute22
                    into    l_item_tax_code
                    from    mtl_system_items_b msib
                            , mtl_parameters mp
                    where   msib.segment1 = c2.part_number
                    and     msib.organization_id = mp.master_organization_id -- mp.organization_id 
                    and     mp.organization_code = c2.branch_code
                    and     rownum = 1;
                   exception
                   when others then
                    l_item_tax_code := null;
                   end;
                   
                   write_log ('Item - tax code: '||c2.part_number||' - '||l_item_tax_code);
                   
                   IF nvl(l_item_tax_code,'UNKNOWN') = '76431' 
                   THEN
                       l_line_tax_amt        := 0;
                       l_line_tax_rate       := 0;
                    /*  begin               
                        select  tax.taxamt
                                , tax.taxrate*100
                        into    l_line_tax_amt
                                , l_line_tax_rate                    
                        from    ra_customer_trx_lines_all trx_ln
                                , ra_customer_trx_lines_all tx_ln
                                , taxware.taxaudit_header hdr
                                , taxware.taxaudit_tax tax
                        where   trx_ln.customer_trx_line_id = c2.invoice_line_id
                        and     trx_ln.customer_trx_line_id = tx_ln.link_to_cust_trx_line_id
                        and     tx_ln.line_type = 'TAX'
                        and     tx_ln.customer_trx_id = hdr.oracleid
                        and     hdr.headerno = tax.headerno
                        and     tx_ln.line_number = tax.detailno
                        and     trx_ln.extended_amount = tax.baseamt
                        and     tax.taxlevel = 'SS';
                       exception
                       when others then
                        l_line_tax_amt        := 0;
                        l_line_tax_rate       := 0;
                       end;*/ 

--//commented above block and added below block for ver 25.0 

		    BEGIN
                       SELECT   tax.taxamt
                               ,tax.taxrate*100
                        INTO    l_line_tax_amt
                               ,l_line_tax_rate    
                        FROM   ra_customer_trx_lines_all trx_ln
                              ,zx.zx_lines zl
                              ,taxware.taxaudit_header hdr
                              ,taxware.taxaudit_tax tax
                              ,taxware.taxaudit_detail twdtl
                        WHERE   trx_ln.customer_trx_line_id     = c2.invoice_line_id
                        AND     hdr.headerno                    = tax.headerno
                        AND     hdr.headerno                    = twdtl.headerno            
                        AND     trx_ln.line_number              = twdtl.invlinenum   
                        AND     hdr.oracleid                    = trx_ln.customer_trx_id
                        AND     tax.detailno                    = twdtl.detailno             
                        AND     trx_ln.extended_amount          = CASE twdtl.credind WHEN 'C' THEN (-1)* (tax.baseamt) ELSE tax.baseamt END
                        AND     tax.taxlevel                    = 'SS'
                        AND     zl.application_id               = 222
                        AND     zl.trx_id                       =  trx_ln.customer_trx_id
                        AND     zl.trx_line_id                  = trx_ln.customer_trx_line_id
                        AND     zl.tax                          = 'STATE';
                        EXCEPTION
                        WHEN OTHERS THEN
                        l_line_tax_amt        := 0;
                        l_line_tax_rate       := 0;
                        END;
                       
                       IF NVL(l_line_tax_amt,0) > 0 then
                        l_total_tax_amt := l_total_tax_amt + l_line_tax_amt;               
                       END IF;
                   END IF;   
               END IF;
               l_max_line_number := c2.line_number;
               
               write_log ('Line Tax - Total Tax (CA 1): '||l_line_tax_amt||' - '||l_total_tax_amt);
               -- 12/21/2012 CG: Added for CP#2030/TMS 20121217-00640
               
               -- 06/08/2012 CG Modified number format to contain commas
               
               UTL_FILE.put_line
                   (v_file,
                    UPPER (  regexp_replace(c2.line_record_type,  '[[:cntrl:]]', ' ')  -- Version# 2560
                           || '|'
                           || c2.invoice_id
                           || '|'
                           || c2.invoice_line_id
                           || '|'
                           -- 02/18/2013 CGonzalez: Changed to sort with the new line number (OM LIne number and Prism Line Number)
                           -- || c2.line_number
                           || c2.new_line_number
                           || '|'
                           || regexp_replace(c2.part_number,  '[[:cntrl:]]', ' ')  -- Version# 27.0
                           || '|'
                           || regexp_replace(c2.part_description,  '[[:cntrl:]]', ' ')  -- Version# 27.0
                           || '|'
                           || regexp_replace(c2.branch,  '[[:cntrl:]]', ' ')  -- Version# 27.0
                           || '|'
                           || c2.uom_code
                           || '|'
                           ||       -- 06/08/2012 CG Modified to accom freight
                                    -- c2.qty_ord||'|'||
                                    -- c2.qty_shipped||'|'||
                              l_order_qty
                           || '|'
                           || l_shipped_qty
                           || '|'
                           || c2.qty_backordered
                           || '|'
                           -- 08/15/2012 CG: Changed to accomodate request for dynamic precision
                           -- of unit price based on data, with a minimum of 2 dec places and
                           -- a max of 5, without trailling 0s.
                           -- || TRIM (TO_CHAR (c2.price, '999,999,990.0000'))
                           || l_unit_price
                           || '|'
                           || TRIM (TO_CHAR (c2.extended_price,
                                             '999,999,990.00'
                                            )
                                   )
                           || '|'
                           || regexp_replace(c2.branch_address1,  '[[:cntrl:]]', ' ')  -- Version# 27.0
                           || '|'
                           || regexp_replace(c2.branch_address2,  '[[:cntrl:]]', ' ') -- Version# 27.0
                           || '|'
                           || c2.branch_city
                           || '|'
                           || c2.branch_state
                           || '|'
                           || c2.branch_zip_code
                           || '|'
                           || regexp_replace(c2.branch_region_1,  '[[:cntrl:]]', ' ')  -- Version# 27.0
                           || '|'
                           ||       --02/20/12 CG Commented per UAT1 Issue #50
                              NULL
                           || '|'
                           ||                       --c2.branch_country||'|'||
                              regexp_replace(c2.branch_phone_number,  '[[:cntrl:]]', ' ')  -- Version# 27.0
                           || '|'
                           || TRIM (TO_CHAR (c2.discount_amount,
                                             '999,999,990.00'
                                            )
                                   )
                           || '|'
                           || TO_CHAR (c2.discount_date, 'MM/DD/YYYY')
                           || '|'
                           -- 12/21/2012 CG: Added for CP#2030/TMS 20121217-00640: Modified to remove the 1%
                           || trim (to_char( (c2.tax_rate - nvl(l_line_tax_rate,0)) , '990.00') )
                           || '|'
                           ||    -- 02/20/12 CG Added per UAT Issues #
                                 -- 02/23/12 CG changed taxable for tax amount
                                 -- c2.taxable
                              -- 12/21/2012 CG: Added for CP#2030/TMS 20121217-00640: Modified to remove the 1%
                              TRIM (TO_CHAR ( (c2.tax_amount - nvl(l_line_tax_amt,0)), '999,999,990.00'))
                          )
                   );

                -- 10/11/2012 CG: Added for CP #923 to add repair tool information   
                -- and CP #872 Modify credit memo document to show invoices applied through the auto-application process, with a remaining balance of $0   
                -- CP# 872/ TMS 20121217-00785: to show CM Application
                -- CP# 923/ TMS 20121217-00710: for repair order tool detail  
                IF c1.batch_source NOT LIKE 'PRISM%' then
                    write_log (' Repair Line Info');
                    
                    for c5 in cur_repair_info (c2.oe_order_line_id, c2.invoice_id, c2.invoice_line_id)  -- Version# 16.0
                    loop                    
                        exit when cur_repair_info%notfound;
                        
                        UTL_FILE.put_line (v_file,
                                        UPPER (   c2.note_record_type
                                               || '|'
                                               || c2.invoice_id
                                               || '|'
                                               -- 02/18/2013 CGonzalez: Changed to sort with the new line number (OM LIne number and Prism Line Number)
                                               -- || c2.line_number
                                               || c2.new_line_number
                                               || '|'
                                               || TRIM (regexp_replace(c5.om_note,  '[[:cntrl:]]', ' ') ) -- Version# 15
                                               || '|'
                                               || NULL
                                               || '|'
                                              )
                                       );
                        
                    end loop;
                
                END IF;
                -- 10/11/2012 CG: Added for CP #923

               -- 01/13/2012 Adding line level comments from Order Line
               -- Looking at the known seeded comments type fields
               -- Adding Line Level
               -- 12/21/12 CG: Added for CP# 2001/ TMS 20121217-00744. Removed 
               -- condition to check only for Shipping and Packing Instructions
               /*IF     (   c2.shipping_instructions IS NOT NULL
                       OR c2.packing_instructions IS NOT NULL
                      )
                  -- 04/01/2012 CG: Excluded for PRISM Source
                  AND c1.batch_source NOT LIKE 'PRISM%'
               --or c2.service_txn_comments is not null
               --or c2.revrec_comments is not null)*/
               IF c1.batch_source NOT LIKE 'PRISM%'
               THEN
                  write_log (' Non Prism Note');
                  -- 06/11/2012 CG: Changed to use cursor and pull data
                  -- parsed from the view if they have CRs
                  /*UTL_FILE.PUT_LINE (v_file,UPPER(
                                      c2.note_record_type||'|'||
                                      c2.invoice_id||'|'||
                                      c2.line_number||'|'||
                                      c2.shipping_instructions||'|'||
                                      c2.packing_instructions||'|'
                                      --c2.service_txn_comments||'|'||
                                      --c2.revrec_comments||'|'
                                      )
                                    );*/
                  l_prism_note_ln := 0;

                  -- 08/17/2012 CG: Corrected to pass the order line ID to the expected cursor
                  -- FOR c4 IN cur_oracle_ln_note (c2.invoice_line_id)                  
                  FOR c4 IN cur_oracle_ln_note (c2.oe_order_line_id)
                  LOOP
                     EXIT WHEN cur_oracle_ln_note%NOTFOUND;
                     
                     IF c4.gsa_lvl = 1 THEN
                        IF c4.om_note IS NOT NULL THEN
                           UTL_FILE.put_line (v_file,
                                              UPPER (   c2.note_record_type
                                                     || '|'
                                                     || c2.invoice_id
                                                     || '|'
                                                     -- 02/18/2013 CGonzalez: Changed to sort with the new line number (OM LIne number and Prism Line Number)
                                                     -- || c2.line_number
                                                     || c2.new_line_number
                                                     || '|'
                                                     || 'Country of Origin: '||SUBSTR(TRIM (regexp_replace(c4.om_note,  '[[:cntrl:]]', ' ') ),1,1000) --Version# 15
                                                     || '|'
                                                     || NULL
                                                     || '|'
                                                    --c2.service_txn_comments||'|'||
                                                    --c2.revrec_comments||'|'
                                                    )
                                             );
                        END IF;
                     ELSE
                        UTL_FILE.put_line (v_file,
                                           UPPER (   c2.note_record_type
                                                  || '|'
                                                  || c2.invoice_id
                                                  || '|'
                                                  -- 02/18/2013 CGonzalez: Changed to sort with the new line number (OM LIne number and Prism Line Number)
                                                  -- || c2.line_number
                                                  || c2.new_line_number
                                                  || '|'
                                                  || SUBSTR(TRIM (regexp_replace(c4.om_note,  '[[:cntrl:]]', ' ') ),1,1000) -- Version# 15
                                                  || '|'
                                                  || NULL
                                                  || '|'
                                                 --c2.service_txn_comments||'|'||
                                                 --c2.revrec_comments||'|'
                                                 )
                                          );
                     END IF;
                     l_prism_note_ln := l_prism_note_ln + 1;
                  END LOOP;

                  write_log (   ' Non Prism Note Exit. Printed '
                             || l_prism_note_ln
                             || ' notes'
                            );
               -- 06/11/2012 CG
               ELSIF c1.batch_source LIKE 'PRISM%'
               THEN
                  -- 04/01/2012 pull prism invoice notes from new staging table
                  l_prism_notes := NULL;
                  l_prism_note_ln := 0;
                  write_log (' Prism Note Entry');

                  FOR c3 IN cur_prism_ln_note (c1.invoice_number,
                                               c2.prism_line_number
                                              )
                  LOOP
                     EXIT WHEN cur_prism_ln_note%NOTFOUND;
                     -- 06/08/2012 CG: Commenting and moving into the loop
                     --                want to display individual prism notes
                     /*l_prism_notes := l_prism_notes||c3.prism_line_note||' ';
                     l_prism_note_ln := length (l_prism_notes);

                     if l_prism_note_ln >= 2000
                     then
                         l_prism_notes := substr(l_prism_notes, 1, 2000);
                         exit;
                     end if;*/
                     UTL_FILE.put_line (v_file,
                                        UPPER (   c2.note_record_type
                                               || '|'
                                               || c2.invoice_id
                                               || '|'
                                               -- 02/18/2013 CGonzalez: Changed to sort with the new line number (OM LIne number and Prism Line Number)
                                               -- || c2.line_number
                                               || c2.new_line_number
                                               || '|'
                                               ||
                                                  -- 06/08/2012 CG Modified to change data source
                                                  --c3.prism_line_note||'|'||
                                                  regexp_replace(c3.description,  '[[:cntrl:]]', ' ')  -- Version# 15
                                               || '|'
                                               || NULL
                                               || '|'
                                              )
                                       );
                     l_prism_note_ln := l_prism_note_ln + 1;
                  END LOOP;

                  write_log (   ' Prism Note Exit. Printed '
                             || l_prism_note_ln
                             || ' notes'
                            );
                   -- 06/08/2012 CG: Commenting and moving into the loop
                   --                want to display individual prism notes
                   /*if l_prism_notes is not null then

                       Write_Log (' Prism Note Print' );
                       UTL_FILE.PUT_LINE (v_file,UPPER(
                                       c2.note_record_type||'|'||
                                       c2.invoice_id||'|'||
                                       c2.line_number||'|'||
                                       l_prism_notes||'|'||
                                       NULL||'|'
                                       )
                                     );

                   end if;*/
               ELSE
                  NULL;                                               -- noop
               END IF;
            
               write_log ('END c2.new_line_number ' || c2.new_line_number);
               
            END LOOP;


            -- 12/03/2012 CG: added for CP #872 Modify credit memo document to show invoices applied through the auto-application process, with a remaining balance of $0
            -- To determine the total amounts applied
            IF c1.batch_source in ('REPAIR OM SOURCE','STANDARD OM SOURCE','ORDER MANAGEMENT')
                AND c1.transaction_type = 'CRM'
            THEN
                l_total_amt_applied := 0;
                l_amount_remaining  := 0;
                 
                begin
                    select sum(app.amount_applied)
                    into   l_total_amt_applied
                    from   ar_receivable_applications_all app
                           , ra_customer_trx_all rcta
                    where  app.application_type = 'CM'
                    and    app.display = 'Y'
                    and    app.customer_trx_id = c1.invoice_id
                    and    app.applied_customer_trx_id = rcta.customer_trx_id;
                exception
                when others then
                    l_total_amt_applied := c1.total_shipping_and_handling;
                end;
                
                l_amount_remaining := c1.amount_due_remaining;
                
            ELSE
                l_total_amt_applied := 0;
                l_amount_remaining  := 0;
                l_total_amt_applied := c1.total_shipping_and_handling;
                l_amount_remaining  := c1.total_invoice;
            END IF;
            -- 12/03/2012 CG

            -- 12/21/2012 CG: Added for CP#2030/TMS 20121217-00640: Modified to remove the 1%
            IF (c1.trx_ship_state = 'CA' or c1.pr_ship_state = 'CA') AND NVL(l_total_tax_amt,0) > 0 THEN
            
                write_log ('New tax line being added');
            
                UTL_FILE.put_line
                   (v_file,
                    UPPER (   'D'
                           || '|'
                           || c1.invoice_id
                           || '|'
                           || 99999
                           || '|'
                           || to_char(nvl(l_max_line_number,0)+1)
                           || '|'
                           || '1% ASSESSMENT'
                           || '|'
                           || 'CA STATE 1% LUMBER ASSESSMENT'
                           || '|'
                           || c1.branch_code
                           || '|'
                           || 'EA'
                           || '|'
                           || 1
                           || '|'
                           || 1
                           || '|'
                           || 0
                           || '|'
                           || TRIM (TO_CHAR (NVL(l_total_tax_amt,0), '999,999,990.00' )) -- NVL(l_total_tax_amt,0)
                           || '|'
                           || TRIM (TO_CHAR (NVL(l_total_tax_amt,0), '999,999,990.00' ))
                           || '|'
                           || regexp_replace(c1.branch_address1,  '[[:cntrl:]]', ' ') -- Added for Version 27.0
                           || '|'
                           || regexp_replace(c1.branch_address2,  '[[:cntrl:]]', ' ') -- Added for Version 27.0
                           || '|'
                           || c1.branch_city
                           || '|'
                           || c1.branch_state
                           || '|'
                           || c1.branch_zip_code
                           || '|'
                           || regexp_replace(c1.branch_region_1,  '[[:cntrl:]]', ' ') -- Added for Version 27.0
                           || '|'
                           || NULL
                           || '|'
                           || regexp_replace(c1.branch_phone_number,  '[[:cntrl:]]', ' ') -- Added for Version 27.0
                           || '|'
                           || '0.00' --TRIM (TO_CHAR (0, '999,999,990.00' ) )
                           || '|'
                           || TO_DATE ('01/01/1952', 'MM/DD/YYYY')
                           || '|'
                           -- 12/21/2012 CG: Added for CP#2030/TMS 20121217-00640: Modified to remove the 1%
                           || 1
                           || '|'
                           || '0.00' --TRIM (TO_CHAR ( 0, '999,999,990.00'))
                          ));
                  
                l_hdr_tax_amt   := c1.total_tax - l_total_tax_amt;
                -- 03/01/2013 CG: TMS 20130201-01380: Changed to recalc tax rate based on subtotal and tax amount
                -- l_hdr_tax_rate  := c1.tax_rate - 1;
                if nvl(l_hdr_tax_amt, 0) > 0 then
                    begin                    
                        l_hdr_tax_rate := round((l_hdr_tax_amt/c1.total_gross)*100, 2);  
                    exception
                    when others then
                        l_hdr_tax_rate := 0;
                    end;
                else
                    l_hdr_tax_rate := 0;
                end if;
            ELSE
                l_hdr_tax_amt   := c1.total_tax;
                -- 03/01/2013 CG: TMS 20130201-01380: Changed to recalc tax rate based on subtotal and tax amount
                -- l_hdr_tax_rate  := c1.tax_rate;
                if nvl(l_hdr_tax_amt, 0) > 0 then
                    begin                    
                        l_hdr_tax_rate := round((l_hdr_tax_amt/c1.total_gross)*100, 2);  
                    exception
                    when others then
                        l_hdr_tax_rate := 0;
                    end;
                else
                    l_hdr_tax_rate := 0;
                end if;  
            END IF;
            
            -- 12/21/2012 CG: Added for CP#2030/TMS 20121217-00640: Modified to remove the 1%

            write_log (' Total Print');
            -- Writting Total Records
            UTL_FILE.put_line
                      (v_file,
                       UPPER (   c1.total_record_type
                              || '|'
                              || c1.invoice_id
                              || '|'
                              || TRIM (TO_CHAR ( (nvl(l_total_tax_amt,0)+c1.total_gross), -- Total Gross
                                                '999,999,990.00'
                                               )
                                      )
                              || '|'
                              -- -- 12/21/2012 CG: Added for CP#2030/TMS 20121217-00640: Modified to remove the 1%
                              || TRIM (TO_CHAR ( nvl(l_hdr_tax_amt,0), -- c1.total_tax, -- Total Tax
                                                '999,999,990.00')
                                      )
                              || '|'
                              -- 12/03/2012 CG: added for CP #872 (TMS 20121217-00785) Changed to l_total_amt_applied from c1.total_shipping_and_handling
                              /*|| TRIM
                                     (TO_CHAR (c1.total_shipping_and_handling,
                                               '999,999,990.00'
                                              )
                                     )*/
                              || TRIM
                                     (TO_CHAR (l_total_amt_applied,         -- Total Shipping -n- Handling Invoice - Amount Applied for CM
                                               '999,999,990.00'
                                              )
                                     )
                              || '|'
                              -- 02/15/2013 CG: Changed to pull the amount due remaining for CMs and Invoice total for all others
                              /*|| TRIM (TO_CHAR (c1.total_invoice,
                                                '999,999,990.00'
                                               )
                                      )*/
                              || TRIM (TO_CHAR (l_amount_remaining,
                                                '999,999,990.00'
                                               )
                                      )
                              || '|'
                              -- 12/03/2012 CG: added for CP #872 Changed to l_total_amt_applied from c1.total_shipping_and_handling
                              || l_hdr_tax_rate -- c1.tax_rate
                             )
                      );
            -- Opening Loop to Pull Invoice Messages/Comments at Invoice Header Level
            -- Per Email 01/12/2012 Notes will be at the Order Line Level in a seeded field
            /*UTL_FILE.PUT_LINE (v_file,
                               'N'||'|'||
                               c1.invoice_id||'|'||
                               'NO MESSAGE CURSOR FOR INVOICE HEADER!!!'
                              );*/

            -- 03/23/2012 CGonzalez: Added portion to update send date for BT Transactions
            write_log (' Trx Update');

            BEGIN
               UPDATE ra_customer_trx_all
                  SET attribute15 = TO_CHAR (SYSDATE, 'MM/DD/YYYY')
                      , attribute_category = c1.org_id
                WHERE customer_trx_id = c1.invoice_id;
            EXCEPTION
               WHEN OTHERS
               THEN
                  write_log
                     (   '  Could not update Bill Trust Invoice Print Date for Invoice '
                      || c1.invoice_number
                     );
            END;
         END LOOP;

         -- Writing End of File per Operating Unit
         UTL_FILE.put_line (v_file,
                               'FE'
                            || '|'
                            || 'END OF COMPANY DATA FILE'
                            || '|'
                            || 'WCI'
                            || '|'
                           );
      END LOOP;

      -- Control numbers in log file for concurrent program
      write_log ('  ');
      write_log (   ' Exported '
                 || l_count_hdrs
                 || ' invoices and '
                 || l_count_lns
                 || ' invoice lines.'
                );
      write_log (' Closing the the file ...');
      --Closing the file handler
      UTL_FILE.fclose (v_file);
      COMMIT;

/* Version# 27 Starts here -- Commented trace scripts by pattabhi for TMS#20150521-00030
EXECUTE IMMEDIATE 'alter session set events ''10046 trace name context off'''; -- Version# 23
EXECUTE IMMEDIATE 'alter session set sql_trace=false'; -- Version# 23 
Version# 27 Ends here -- Commented trace scripts by pattabhi for TMS#20150521-00030  */

   EXCEPTION
      WHEN UTL_FILE.invalid_path
      THEN
         UTL_FILE.fclose (v_file);
         errbuf := 'File Path is invalid.';
         retcode := '2';
         write_error (errbuf);
         write_log (errbuf);
      WHEN UTL_FILE.invalid_mode
      THEN
         UTL_FILE.fclose (v_file);
         errbuf := 'The open_mode parameter in FOPEN is invalid.';
         retcode := '2';
         write_error (errbuf);
         write_log (errbuf);
      WHEN UTL_FILE.invalid_filehandle
      THEN
         UTL_FILE.fclose (v_file);
         errbuf := 'File handle is invalid..';
         retcode := '2';
         write_error (errbuf);
         write_log (errbuf);
      WHEN UTL_FILE.invalid_operation
      THEN
         UTL_FILE.fclose (v_file);
         errbuf := 'File could not be opened or operated on as requested';
         retcode := '2';
         write_error (errbuf);
         write_log (errbuf);
      WHEN UTL_FILE.write_error
      THEN
         UTL_FILE.fclose (v_file);
         errbuf :=
                 'Operating system error occurred during the write operation';
         retcode := '2';
         write_error (errbuf);
         write_log (errbuf);
      WHEN UTL_FILE.internal_error
      THEN
         UTL_FILE.fclose (v_file);
         write_log ('Unspecified PL/SQL error.');
      WHEN UTL_FILE.file_open
      THEN
         UTL_FILE.fclose (v_file);
         errbuf := 'The requested operation failed because the file is open.';
         retcode := '2';
         write_error (errbuf);
         write_log (errbuf);
      WHEN UTL_FILE.invalid_filename
      THEN
         UTL_FILE.fclose (v_file);
         errbuf := 'The filename parameter is invalid.';
         retcode := '2';
         write_error (errbuf);
         write_log (errbuf);
      WHEN UTL_FILE.access_denied
      THEN
         UTL_FILE.fclose (v_file);
         errbuf := 'Permission to access to the file location is denied.';
         retcode := '2';
         write_error (errbuf);
         write_log (errbuf);
      WHEN OTHERS
      THEN
         UTL_FILE.fclose (v_file);
         errbuf := 'Error Msg :' || SQLERRM;
         retcode := '2';
         write_error (errbuf);
         write_log (errbuf);
   END gen_open_invoice_file;

   /*************************************************************************
     Procedure : gen_statements_file

     PURPOSE:   This procedure creates file for the Monthly Statements to
                Bill Trust
     Parameter:
	 REVISIONS:
     Ver          Date        Author                     Description
     ---------  ----------  ---------------         -------------------------
     25.0        05/02/2015   Raghavendra S         TMS# 20150303-00075 - FIN - Addition of Code Snippet for Line Breaks in All EDW Extract Programs
                                                    and Views for all free entry text fields.
     26.0        10/02/2015  Maharajan Shunmugam    TMS#20150805-00039 Billtrust - Improvements to BT extract logic
   ************************************************************************/
   PROCEDURE gen_statements_file (
      errbuf             OUT      VARCHAR2,
      retcode            OUT      VARCHAR2,
      p_directory_name   IN OUT   VARCHAR2,
      p_file_name        IN OUT   VARCHAR2
   )
   IS
      CURSOR cur_statement_header
      IS
         SELECT   'H' record_level, 
                  x1.cust_account_id,
                  x1.customer_account_number, x1.customer_name,
                  x1.prim_bill_to_site, x1.prim_bill_to_address1,
                  x1.prim_bill_to_address2, x1.prim_bill_to_city,
                  x1.prim_bill_to_city_province, x1.prim_bill_to_zip_code,
                  x1.prim_bill_to_country, x1.account_manager,
                  x1.acct_mgr_office_number, x1.remit_to_address_code,
                  x1.stmt_by_job                  -- Added 01/17/12 CGonzalez
                                ,
                  x1.send_statement_flag               -- Added 03/08/2012 CG
                                        ,
                  x1.send_credit_bal_flag              -- added 03/08/2012 CG
                                         ,
                  SUM (x1.amount_due_remaining) total_due_bal,
                  SUM (x1.current_balance) current_bal,
                  SUM (x1.thirty_days_bal) thirty_days_bal,
                  SUM (x1.sixty_days_bal) sixty_days_bal,
                  SUM (x1.ninety_days_bal) ninety_days_bal,
                  SUM (x1.over_ninety_days_bal) over_ninety_days_bal
        --     FROM xxwc_bill_trust_stmts_vw x1                                   --commented and added below for ver#26.0
               FROM XXWC.XXWC_AR_BT_HEADER_TBL x1
            WHERE x1.send_statement_flag = 'Y'            -- Added 03/08/2012
              -- 03/23/2012 CGonzalez : Added to exclude customer with payment terms COD and Preferred Cash
              -- 06/05/2012 CGonzalez : Added NVL for Trx that naturally dont have a term (DM/CM)
              AND NVL (x1.cust_payment_term, 'ZZZ') NOT IN
                                                         ('COD', 'PRFRDCASH')
         GROUP BY 'H',
                  x1.cust_account_id,
                  x1.customer_account_number,
                  x1.customer_name,
                  x1.prim_bill_to_site,
                  x1.prim_bill_to_address1,
                  x1.prim_bill_to_address2,
                  x1.prim_bill_to_city,
                  x1.prim_bill_to_city_province,
                  x1.prim_bill_to_zip_code,
                  x1.prim_bill_to_country,
                  x1.account_manager,
                  x1.acct_mgr_office_number,
                  x1.remit_to_address_code,
                  x1.stmt_by_job,
                  x1.send_statement_flag                -- Added 03/08/2012 CG
                                        ,
                  x1.send_credit_bal_flag               -- added 03/08/2012 CG
         ORDER BY x1.customer_account_number;

      CURSOR cur_job_level (p_cust_account_id NUMBER)
      IS
         SELECT   'J' job_record_level, 
                  x1.cust_account_id,
                  x1.customer_account_number, x1.customer_name,
                  x1.prim_bill_to_site, x1.prim_bill_to_address1,
                  x1.prim_bill_to_address2, x1.prim_bill_to_city,
                  x1.prim_bill_to_city_province, x1.prim_bill_to_zip_code,
                  x1.prim_bill_to_country, x1.account_manager,
                  x1.remit_to_address_code
                                          -- 02/23/2012 CG Commented the Address Type restriction
                  ,
                  (CASE
                      WHEN                    /*x1.trx_addr_type = 'JOB' and*/
                          x1.stmt_by_job = 'Y'
                         THEN x1.trx_addr_type
                      ELSE NULL
                   END
                  ) site_type
                             -- Commented 01/17/2012 CGonzalez, x1.customer_job_number
                  ,
                  (CASE
                      WHEN x1.stmt_by_job = 'Y'
                         THEN x1.customer_job_number
                      ELSE NULL
                   END
                  ) customer_job_number,
                  (CASE
                      WHEN                    /*x1.trx_addr_type = 'JOB' and*/
                          x1.stmt_by_job = 'Y'
                         THEN x1.trx_bill_to_site
                      ELSE NULL
                   END
                  ) trx_bill_site_name,
                  'T' total_record_level,
                  SUM (x1.amount_due_remaining) total_due_bal,
                  SUM (x1.current_balance) current_bal,
                  SUM (x1.thirty_days_bal) thirty_days_bal,
                  SUM (x1.sixty_days_bal) sixty_days_bal,
                  SUM (x1.ninety_days_bal) ninety_days_bal,
                  SUM (x1.over_ninety_days_bal) over_ninety_days_bal
            -- FROM xxwc_bill_trust_stmts_vw x1                            --commented and added below for ver#26.0
             FROM XXWC.XXWC_AR_BT_HEADER_TBL  x1
            WHERE x1.cust_account_id = p_cust_account_id
         GROUP BY 'J', 	
                  x1.cust_account_id,
                  x1.customer_account_number,
                  x1.customer_name,
                  x1.prim_bill_to_site,
                  x1.prim_bill_to_address1,
                  x1.prim_bill_to_address2,
                  x1.prim_bill_to_city,
                  x1.prim_bill_to_city_province,
                  x1.prim_bill_to_zip_code,
                  x1.prim_bill_to_country,
                  x1.account_manager,
                  x1.remit_to_address_code,
                  (CASE
                      WHEN                    /*x1.trx_addr_type = 'JOB' and*/
                          x1.stmt_by_job = 'Y'
                         THEN x1.trx_addr_type
                      ELSE NULL
                   END
                  )
                   -- 01/17/2012 CGonzalez , x1.customer_job_number
         ,
                  (CASE
                      WHEN x1.stmt_by_job = 'Y'
                         THEN x1.customer_job_number
                      ELSE NULL
                   END),
                  (CASE
                      WHEN                    /*x1.trx_addr_type = 'JOB' and*/
                          x1.stmt_by_job = 'Y'
                         THEN x1.trx_bill_to_site
                      ELSE NULL
                   END
                  ),
                  'T' 
         ORDER BY x1.customer_account_number
                                            -- 01/17/2012 CGonzalez , x1.customer_job_number;
                                            -- 02/24/2012 CGonzalez changed sorting from UAT1
                                            -- changed sorting to be by transaction billing address
                                            /*, (case when x1.stmt_by_job = 'Y'
                                                    then x1.customer_job_number
                                                    else null
                                               end);*/
         ,
                  (CASE
                      WHEN                    /*x1.trx_addr_type = 'JOB' and*/
                          x1.stmt_by_job = 'Y'
                         THEN x1.trx_bill_to_site
                      ELSE NULL
                   END
                  );

      CURSOR cur_statement_detail (
         p_cust_account_id   NUMBER,
         p_site_type         VARCHAR2,
         p_job_number        VARCHAR2
      )
      IS
         SELECT   'D' detail_record_type,
         	x1.*
           --  FROM xxwc_bill_trust_stmts_vw x1            --commented and added below for ver#26.0
             FROM XXWC.XXWC_AR_BT_HEADER_TBL x1
            WHERE x1.cust_account_id = p_cust_account_id
              AND NVL
                     ((CASE
                          WHEN                /*x1.trx_addr_type = 'JOB' and*/
                              x1.stmt_by_job = 'Y'
                             THEN x1.trx_addr_type
                          ELSE NULL
                       END
                      ),
                      'UNKNOWN'
                     ) = NVL (p_site_type, 'UNKNOWN')
              -- 01/17/12 CGonzalez and       nvl(x1.customer_job_number, 'ZZZUNKNOWN') = nvl(p_job_number,'ZZZUNKNOWN')
              AND NVL ((CASE
                           WHEN x1.stmt_by_job = 'Y'
                              THEN x1.customer_job_number
                           ELSE NULL
                        END
                       ),
                       'ZZZUNKNOWN'
                      ) = NVL (p_job_number, 'ZZZUNKNOWN')
         -- 02/23/2012 CG Changed sorting
         --order by x1.trx_number;
         ORDER BY x1.trx_date;

      v_file                  UTL_FILE.file_type;
      l_filename              VARCHAR2 (100);
      l_close_date            DATE;
      l_count_hdrs            NUMBER;
      l_count_jobs            NUMBER;
      l_count_dtls            NUMBER;
      l_customer_job_number   VARCHAR2 (240);
      l_manager_phone         VARCHAR2 (30);

      TYPE invoice_stat IS TABLE OF apps.xxwc_bill_trust_stmts_vw%ROWTYPE;                  -- Added for version 26.0
      l_invoice_stat        invoice_stat;                                                   -- Added for version 26.0

   BEGIN
      write_log ('Begining EBS To Bill Trust Open Invoice File Generation ');
      write_log ('========================================================');
      write_log ('  ');
      --Initialize the Out Parameter
      errbuf := '';
      retcode := '0';
      write_log ('Parameters');
      write_log (RPAD ('File Drop Directory: ', 25, ' ') || p_directory_name);
      write_log (RPAD ('File Name: ', 25, ' ') || p_file_name);

      IF p_file_name IS NULL
      THEN
         --Derive the file name in case the file name is not passed
         SELECT    'WC_AR_BILLTRUST_STATEMENT_'
                || TO_CHAR (SYSDATE, 'MMDDYYYY_HH_MI_SS')
                || '.txt'
           INTO l_filename
           FROM DUAL;
      ELSE
         l_filename := p_file_name;
      END IF;

      write_log (RPAD ('File Name for Export ', 25, ' ') || l_filename);
      l_close_date := NULL;

      BEGIN
         SELECT TRUNC (SYSDATE, 'MM')
           INTO l_close_date
           FROM DUAL;
      EXCEPTION
         WHEN OTHERS
         THEN
            l_close_date := NULL;
      END;

      write_log (   RPAD ('Close Date ', 25, ' ')
                 || TO_CHAR (l_close_date, 'MM/DD/YY')
                );
      write_log ('  ');
      write_log (' Opening the File handler');

      IF p_directory_name IS NULL
      THEN
         write_log (' Setting the directory');
         p_directory_name := 'ORACLE_INT_UC4';
      END IF;

      --Open the file handler
      v_file :=
         UTL_FILE.fopen (LOCATION       => p_directory_name,
                         filename       => l_filename,
                         open_mode      => 'w'
                        );
      write_log (' Writing to the file ...');
      l_count_hdrs := 0;
      l_count_jobs := 0;
      l_count_dtls := 0;

--<<Added for ver#26.0 Start >>
      SELECT *
        BULK COLLECT INTO l_invoice_stat
        FROM xxwc_bill_trust_stmts_vw;

      FOR indx IN 1 .. l_invoice_stat.COUNT                                                
      LOOP
      INSERT INTO XXWC.XXWC_AR_BT_HEADER_TBL 
					    (	CUST_ACCOUNT_ID,    
					     	CUSTOMER_ACCOUNT_NUMBER,  
					     	CUSTOMER_NAME,
				             	CUST_PAYMENT_TERM , 
						PRIM_BILL_TO_ID,   
						PRIM_BILL_TO_SITE ,  
						PRIM_BILL_TO_ADDRESS1,  
						PRIM_BILL_TO_ADDRESS2, 
						PRIM_BILL_TO_CITY ,  
						PRIM_BILL_TO_CITY_PROVINCE,   
						PRIM_BILL_TO_ZIP_CODE ,   
						PRIM_BILL_TO_COUNTRY,   
						ACCOUNT_MANAGER  , 
						ACCT_MGR_OFFICE_NUMBER , 
						REMIT_TO_ADDRESS_CODE ,
						REMIT_TO_LN1,  
						REMIT_TO_LN2, 
						REMIT_TO_LN3 , 
						REMIT_TO_LN4 ,
						REMIT_TO_LN5 , 
						STMT_BY_JOB ,  
						SEND_STATEMENT_FLAG,   
						SEND_CREDIT_BAL_FLAG ,    
						PAYMENT_SCHEDULE_ID ,     
						TRX_BILL_TO_SITE ,   
						TRX_CUSTOMER_ID ,   
						TRX_BILL_SITE_USE_ID ,    
						TRX_SITE_USE_CODE  ,  
						TRX_ADDR_TYPE ,  
						CUSTOMER_JOB_NUMBER  ,  
						TRX_NUMBER , 
						TRX_DATE  ,         
						CUSTOMER_PO_NUMBER , 
						PMT_STATUS ,   
						TRX_TYPE   ,  
						AMOUNT_DUE_ORIGINAL ,     
						AMOUNT_DUE_REMAINING  ,       
						TRX_AGE ,        
						CURRENT_BALANCE ,        
						THIRTY_DAYS_BAL,        
						SIXTY_DAYS_BAL ,         
						NINETY_DAYS_BAL ,        
						OVER_NINETY_DAYS_BAL )
     					 VALUES	(l_invoice_stat(indx).CUST_ACCOUNT_ID,    
					     	 l_invoice_stat(indx).CUSTOMER_ACCOUNT_NUMBER,  
					     	 l_invoice_stat(indx).CUSTOMER_NAME,
				             	 l_invoice_stat(indx).CUST_PAYMENT_TERM , 
						 l_invoice_stat(indx).PRIM_BILL_TO_ID,   
						 l_invoice_stat(indx).PRIM_BILL_TO_SITE ,  
						 l_invoice_stat(indx).PRIM_BILL_TO_ADDRESS1,  
						 l_invoice_stat(indx).PRIM_BILL_TO_ADDRESS2, 
						 l_invoice_stat(indx).PRIM_BILL_TO_CITY ,  
						 l_invoice_stat(indx).PRIM_BILL_TO_CITY_PROVINCE,   
						 l_invoice_stat(indx).PRIM_BILL_TO_ZIP_CODE ,   
						 l_invoice_stat(indx).PRIM_BILL_TO_COUNTRY,   
						 l_invoice_stat(indx).ACCOUNT_MANAGER  , 
						 l_invoice_stat(indx).ACCT_MGR_OFFICE_NUMBER , 
						 l_invoice_stat(indx).REMIT_TO_ADDRESS_CODE ,
						 l_invoice_stat(indx).REMIT_TO_LN1,  
						 l_invoice_stat(indx).REMIT_TO_LN2, 
						 l_invoice_stat(indx).REMIT_TO_LN3 , 
						 l_invoice_stat(indx).REMIT_TO_LN4 ,
						 l_invoice_stat(indx).REMIT_TO_LN5 , 
						 l_invoice_stat(indx).STMT_BY_JOB ,  
						 l_invoice_stat(indx).SEND_STATEMENT_FLAG,   
						 l_invoice_stat(indx).SEND_CREDIT_BAL_FLAG ,    
						 l_invoice_stat(indx).PAYMENT_SCHEDULE_ID ,     
						 l_invoice_stat(indx).TRX_BILL_TO_SITE ,   
						 l_invoice_stat(indx).TRX_CUSTOMER_ID ,   
						 l_invoice_stat(indx).TRX_BILL_SITE_USE_ID ,    
						 l_invoice_stat(indx).TRX_SITE_USE_CODE  ,  
						 l_invoice_stat(indx).TRX_ADDR_TYPE ,  
						 l_invoice_stat(indx).CUSTOMER_JOB_NUMBER  ,  
						 l_invoice_stat(indx).TRX_NUMBER , 
						 l_invoice_stat(indx).TRX_DATE  ,         
						 l_invoice_stat(indx).CUSTOMER_PO_NUMBER , 
						 l_invoice_stat(indx).PMT_STATUS ,   
						 l_invoice_stat(indx).TRX_TYPE   ,  
						 l_invoice_stat(indx).AMOUNT_DUE_ORIGINAL ,     
						 l_invoice_stat(indx).AMOUNT_DUE_REMAINING  ,       
						 l_invoice_stat(indx).TRX_AGE ,        
						 l_invoice_stat(indx).CURRENT_BALANCE ,        
						 l_invoice_stat(indx).THIRTY_DAYS_BAL,        
						 l_invoice_stat(indx).SIXTY_DAYS_BAL ,         
						 l_invoice_stat(indx).NINETY_DAYS_BAL ,        
						 l_invoice_stat(indx).OVER_NINETY_DAYS_BAL );			
      
      END LOOP;
                      
COMMIT;
--<<Added for ver#26.0 End >>

      FOR c1 IN cur_statement_header
      LOOP
         EXIT WHEN cur_statement_header%NOTFOUND;

         IF    (NVL (c1.total_due_bal, 0) > 0)
            OR (NVL (c1.total_due_bal, 0) <= 0
                AND c1.send_credit_bal_flag = 'Y'
               )
         THEN
            l_count_hdrs := l_count_hdrs + 1;
            l_manager_phone := NULL;

            IF c1.acct_mgr_office_number IS NOT NULL
            THEN
               BEGIN
                  SELECT DECODE (LENGTH (c1.acct_mgr_office_number),
                                 10, SUBSTR (c1.acct_mgr_office_number, 1, 3)
                                  || '-'
                                  || SUBSTR (c1.acct_mgr_office_number, 4, 3)
                                  || '-'
                                  || SUBSTR (c1.acct_mgr_office_number, 7, 4),
                                 7,  SUBSTR (c1.acct_mgr_office_number, 1, 3)
                                  || '-'
                                  || SUBSTR (c1.acct_mgr_office_number, 4, 4),
                                 c1.acct_mgr_office_number
                                )
                    INTO l_manager_phone
                    FROM DUAL;
               EXCEPTION
                  WHEN OTHERS
                  THEN
                     l_manager_phone := c1.acct_mgr_office_number;
               END;
            END IF;

            -- Customer Statement Header Record
            UTL_FILE.put_line
                    (v_file,
                     UPPER (   c1.record_level
                            || '|'
                            || c1.customer_account_number
                            || '|'
                            || NULL
                            || '|'
                            ||        -- 03/12/2012 CG c1.customer_name||'|'||
                               regexp_replace(c1.customer_name,  '[[:cntrl:]]', ' ') -- Added for Version 27.0
                            || '|'
                            ||    -- 03/12/2012 CG c1.prim_bill_to_site||'|'||
                               regexp_replace(c1.prim_bill_to_address1,  '[[:cntrl:]]', ' ') -- Added for Version 27.0
                            || '|'
                            || regexp_replace(c1.prim_bill_to_address2,  '[[:cntrl:]]', ' ') -- Added for Version 27.0
                            || '|'
                            || c1.prim_bill_to_city
                            || '|'
                            || c1.prim_bill_to_city_province
                            || '|'
                            || c1.prim_bill_to_zip_code
                            || '|'
                            || NULL
                            || '|'
                            ||               -- c1.prim_bill_to_country||'|'||
                               c1.account_manager
                            || '|'
                            || regexp_replace(c1.remit_to_address_code,  '[[:cntrl:]]', ' ') -- Added for Version 25.06
                            || '|'
                            || TRIM (TO_CHAR (c1.total_due_bal,
                                              '999,999,990.00'
                                             )
                                    )
                            || '|'
                            || TRIM (TO_CHAR (c1.current_bal,
                                              '999,999,990.00')
                                    )
                            || '|'
                            || TRIM (TO_CHAR (c1.thirty_days_bal,
                                              '999,999,990.00'
                                             )
                                    )
                            || '|'
                            || TRIM (TO_CHAR (c1.sixty_days_bal,
                                              '999,999,990.00'
                                             )
                                    )
                            || '|'
                            || TRIM (TO_CHAR (c1.ninety_days_bal,
                                              '999,999,990.00'
                                             )
                                    )
                            || '|'
                            || TRIM (TO_CHAR (c1.over_ninety_days_bal,
                                              '999,999,990.00'
                                             )
                                    )
                            || '|'
                            || TO_CHAR (l_close_date, 'MM/DD/YY')
                            || '|'
                            || l_manager_phone
                            || '|'
                           )
                    );

            -- Opening Job Level Records
            FOR c2 IN cur_job_level (c1.cust_account_id)
            LOOP
               EXIT WHEN cur_job_level%NOTFOUND;

               -- Printing Second level if they are Jobs
               -- 02/23/2012 CGonzalez: changed condition to use this grouping statement
               -- for all sites
               IF                 /*nvl(c2.site_type,'UNKNOWN') = 'JOB' and*/
                  -- 01/17/12 CGonzalez
                  c1.stmt_by_job = 'Y'
               THEN
                  l_count_jobs := l_count_jobs + 1;
                  -- Printing Job Level First Level of Data
                  UTL_FILE.put_line (v_file,
                                     UPPER (   c2.job_record_level
                                            || '|'
                                            || regexp_replace(c2.customer_account_number,  '[[:cntrl:]]', ' ') -- Added for Version 27.0
                                            || '|'
                                            || 'Customer Job Number'
                                            || '|'
                                            || regexp_replace(c2.customer_job_number,  '[[:cntrl:]]', ' ') -- Added for Version 27.0
                                            || '|'
                                            || regexp_replace(c2.trx_bill_site_name,  '[[:cntrl:]]', ' ') -- Added for Version 27.0
                                            || '|'
                                           )
                                    );
               END IF;

               -- Opening Cursor For Transaction Detail
               FOR c3 IN cur_statement_detail (c2.cust_account_id,
                                               c2.site_type,
                                               c2.customer_job_number
                                              )
               LOOP
                  EXIT WHEN cur_statement_detail%NOTFOUND;
                  l_count_dtls := l_count_dtls + 1;
                  -- 02/23/2012 CG From UAT1 display Customer Job Number only when Stmt by Job = Y
                  l_customer_job_number := NULL;

                  IF c1.stmt_by_job = 'Y'
                  THEN
                     l_customer_job_number := c3.customer_job_number;
                  END IF;

                  -- Printing Statement Detail Level
                  UTL_FILE.put_line
                             (v_file,
                              UPPER (   c3.detail_record_type
                                     || '|'
                                     || c3.customer_account_number
                                     || '|'
                                     ||
-- 02/23/2012 CG From UAT1 display Customer Job Number only when Stmt by Job = Y
                                        l_customer_job_number
                                     || '|'
                                     ||        --c3.customer_job_number||'|'||
                                        regexp_replace(c3.trx_number,  '[[:cntrl:]]', ' ') -- Added for Version 27.0
                                     || '|'
                                     || TO_CHAR (c3.trx_date, 'MM/DD/YY')
                                     || '|'
                                     || regexp_replace(c3.customer_po_number,  '[[:cntrl:]]', ' ') -- Version# 15
                                     || '|'
                                     || regexp_replace(c3.pmt_status,  '[[:cntrl:]]', ' ') -- Added for Version 27.0
                                     || c3.trx_type
                                     || '|'
                                     || TRIM (TO_CHAR (c3.amount_due_original,
                                                       '999,999,990.00'
                                                      )
                                             )
                                     || '|'
                                     || TRIM
                                            (TO_CHAR (c3.amount_due_remaining,
                                                      '999,999,990.00'
                                                     )
                                            )
                                     || '|'
                                    )
                             );
               END LOOP;

               -- 02/23/2012 CGonzalez: changed condition to use this grouping statement
               -- for all sites
               IF                  /*nvl(c2.site_type,'UNKNOWN') = 'JOB' and*/
                  -- 01/17/12 CGonzalez
                  c1.stmt_by_job = 'Y'
               THEN
                  -- Printing Job Level Subtotals
                  -- 02/23/2012 CGonzalez Added from UAT1 to allow BT to display correctly
                  UTL_FILE.put_line (v_file,
                                        UPPER (   c2.total_record_level
                                               || '|'
                                               || c2.customer_account_number
                                               || '|'
                                               || c2.customer_job_number
                                               || '|'
                                              )
                                     || 'Total'
                                     || '|'
                                     || 'Current'
                                     || '|'
                                     || '1-30 Days'
                                     || '|'
                                     || '31-60 Days'
                                     || '|'
                                     || '61-90 Days'
                                     || '|'
                                     || '90+ Days'
                                     || '|'
                                    );
                  UTL_FILE.put_line
                             (v_file,
                              UPPER (   c2.total_record_level
                                     || '|'
                                     || c2.customer_account_number
                                     || '|'
                                     || c2.customer_job_number
                                     || '|'
                                     || TRIM (TO_CHAR (c2.total_due_bal,
                                                       '999,999,990.00'
                                                      )
                                             )
                                     || '|'
                                     || TRIM (TO_CHAR (c2.current_bal,
                                                       '999,999,990.00'
                                                      )
                                             )
                                     || '|'
                                     || TRIM (TO_CHAR (c2.thirty_days_bal,
                                                       '999,999,990.00'
                                                      )
                                             )
                                     || '|'
                                     || TRIM (TO_CHAR (c2.sixty_days_bal,
                                                       '999,999,990.00'
                                                      )
                                             )
                                     || '|'
                                     || TRIM (TO_CHAR (c2.ninety_days_bal,
                                                       '999,999,990.00'
                                                      )
                                             )
                                     || '|'
                                     || TRIM
                                            (TO_CHAR (c2.over_ninety_days_bal,
                                                      '999,999,990.00'
                                                     )
                                            )
                                     || '|'
                                    )
                             );
               END IF;
            END LOOP;
         END IF;
      END LOOP;

      write_log ('  ');
      write_log (' Printed ' || l_count_hdrs || ' statement header records');
      write_log (   ' Printed '
                 || l_count_jobs
                 || ' statement customer job records'
                );
      write_log (' Printed ' || l_count_dtls || ' statement detail records');
      write_log (' Closing the the file ...');
      --Closing the file handler
      UTL_FILE.fclose (v_file);

      EXECUTE IMMEDIATE 'TRUNCATE TABLE XXWC.XXWC_AR_BT_HEADER_TBL';             --Added for ver 26.0
 							  
   EXCEPTION
      WHEN UTL_FILE.invalid_path
      THEN
         UTL_FILE.fclose (v_file);
         errbuf := 'File Path is invalid.';
         retcode := '2';
         write_error (errbuf);
      WHEN UTL_FILE.invalid_mode
      THEN
         UTL_FILE.fclose (v_file);
         errbuf := 'The open_mode parameter in FOPEN is invalid.';
         retcode := '2';
         write_error (errbuf);
      WHEN UTL_FILE.invalid_filehandle
      THEN
         UTL_FILE.fclose (v_file);
         errbuf := 'File handle is invalid..';
         retcode := '2';
         write_error (errbuf);
      WHEN UTL_FILE.invalid_operation
      THEN
         UTL_FILE.fclose (v_file);
         errbuf := 'File could not be opened or operated on as requested';
         retcode := '2';
         write_error (errbuf);
      WHEN UTL_FILE.write_error
      THEN
         UTL_FILE.fclose (v_file);
         errbuf :=
                 'Operating system error occurred during the write operation';
         retcode := '2';
         write_error (errbuf);
      WHEN UTL_FILE.internal_error
      THEN
         UTL_FILE.fclose (v_file);
         write_log ('Unspecified PL/SQL error.');
      WHEN UTL_FILE.file_open
      THEN
         UTL_FILE.fclose (v_file);
         errbuf := 'The requested operation failed because the file is open.';
         retcode := '2';
         write_error (errbuf);
      WHEN UTL_FILE.invalid_filename
      THEN
         UTL_FILE.fclose (v_file);
         errbuf := 'The filename parameter is invalid.';
         retcode := '2';
         write_error (errbuf);
      WHEN UTL_FILE.access_denied
      THEN
         UTL_FILE.fclose (v_file);
         errbuf := 'Permission to access to the file location is denied.';
         retcode := '2';
         write_error (errbuf);
      WHEN OTHERS
      THEN
         UTL_FILE.fclose (v_file);
         errbuf := 'Error Msg :' || SQLERRM;
         retcode := '2';
         write_error (errbuf);
   END gen_statements_file;

   /*************************************************************************
   *  Copyright (c) 2011 Lucidity Consulting Group
   *  All rights reserved.
   **************************************************************************
   *   $Header xxwc_billtrust_interface_pkg.pks $
   *   Module Name: xxwc_billtrust_interface_pkg.pks
   *
   *   PURPOSE:   This package is called by the concurrent program XXWC Billtrust Open Balance Outbound Interface
   *
   *   REVISIONS:
   *   Ver        Date        Author                     Description
   *   ---------  ----------  ---------------         -------------------------
   *   1.0        11/30/2011  Vivek Lakaman             Initial Version
   * ************************************************************************/

   /*************************************************************************
   *   Procedure : get_open_balance
   *
   *   PURPOSE:   This procedure get the amount remaining for the invoice/receipts
   *   Parameter:
   *          IN
   *              p_debug_msg      -- Debug Message
   * ************************************************************************/
   FUNCTION get_open_balance (p_trx_type IN VARCHAR2, p_trx_id IN NUMBER)
      RETURN NUMBER
   IS
      l_amount   NUMBER;
   BEGIN
      IF p_trx_type = 'PMT'
      THEN
         SELECT SUM (amount_due_remaining)
           INTO l_amount
           FROM ar_payment_schedules_all
          WHERE cash_receipt_id = p_trx_id;
      ELSE
         SELECT SUM (amount_due_remaining)
           INTO l_amount
           FROM ar_payment_schedules_all
          WHERE customer_trx_id = p_trx_id;
      END IF;

      RETURN l_amount;
   EXCEPTION
      WHEN OTHERS
      THEN
         RETURN 0;
   END get_open_balance;

     /*************************************************************************
   *   Procedure : open_balance
   *
   *   PURPOSE:   This procedure creates file for the open balance
   *   Parameter:
   *          IN
   *              p_debug_msg      -- Debug Message
   * ************************************************************************/
   PROCEDURE open_balance (
      errbuf              OUT      VARCHAR2,
      retcode             OUT      VARCHAR2,
      px_directory_name   IN OUT   VARCHAR2,
      px_file_name        IN OUT   VARCHAR2
   )
   IS
      l_filename   VARCHAR2 (240);

      -- 02/24/2012 CGonzalez
      -- Corrected from UAT1, changed the customer number from the original reference
      -- to the real account number and the invoice portion to pull the CM as well
      -- 07/03/2012 CGonzalez
      -- Modified and substringed transactions created prior to 06/14 to remove dash
      -- and anything after that since they were sent from PRISM and not oracle
      CURSOR c_data
      IS
         SELECT                  -- 07/03/12 CG: trx_number,
                                 -- 07/11/12 CG: adjusted to only exclude -00
                (CASE
                    WHEN TRUNC (trx.creation_date) < '14-jun-2012'
                    AND SUBSTR (trx.trx_number, -3) = '-00'
                       THEN SUBSTR (trx.trx_number,
                                    1,
                                    DECODE (INSTR (trx.trx_number, '-', 1, 1),
                                            0, LENGTH (trx.trx_number),
                                              INSTR (trx.trx_number, '-', 1,
                                                     1)
                                            - 1
                                           )
                                   )
                    ELSE trx.trx_number
                 END
                ) trx_number,
                '' invoice_suffix,
                get_open_balance ('INV', customer_trx_id) open_balance,
                
                -- 02/24/2012 CGonzalez
                --orig_system_reference account_number
                hcs.account_number account_number
           FROM ra_customer_trx_all trx, hz_cust_accounts hcs
          WHERE trx.bill_to_customer_id = hcs.cust_account_id
            AND trx.status_trx = 'OP'
            AND trx.complete_flag = 'Y'
            -- 02/24/2012 CGonzalez
            --AND get_open_balance ('INV', customer_trx_id) > 0
            AND ABS (get_open_balance ('INV', customer_trx_id)) > 0
         UNION
         SELECT                   -- 07/03/12 CG: receipt_number trx_number,
                                  -- 07/11/12 CG: adjusted to only exclude -00
                (CASE
                    WHEN TRUNC (trx.creation_date) < '14-jun-2012'
                    AND SUBSTR (trx.receipt_number, -3) = '-00'
                       THEN SUBSTR (trx.receipt_number,
                                    1,
                                    DECODE (INSTR (trx.receipt_number,
                                                   '-',
                                                   1,
                                                   1
                                                  ),
                                            0, LENGTH (trx.receipt_number),
                                              INSTR (trx.receipt_number,
                                                     '-',
                                                     1,
                                                     1
                                                    )
                                            - 1
                                           )
                                   )
                    ELSE trx.receipt_number
                 END
                ) trx_number,
                '' invoice_suffix,
                get_open_balance ('PMT', cash_receipt_id) open_balance,
                
                -- 02/24/2012 CGonzalez
                --hcs.orig_system_reference account_number
                hcs.account_number account_number
           FROM ar_cash_receipts_all trx, hz_cust_accounts hcs
          WHERE trx.pay_from_customer = hcs.cust_account_id
            AND ABS (get_open_balance ('PMT', cash_receipt_id)) > 0;

      v_file       UTL_FILE.file_type;
   BEGIN
      write_log (' At Begin ');
      --Initialize the Out Parameter
      errbuf := '';
      retcode := '0';
      write_log (' Out Parameter');
      write_log (' px_directory_name =>' || px_directory_name);
      write_log (' px_file_name      =>' || px_file_name);

      IF px_file_name IS NULL
      THEN
         write_log (' File Name is not passed so derive the file name');

         --Derive the file name in case the file name is not passed
         SELECT 'IGOPENBAL_' || TO_CHAR (SYSDATE, 'MMDDYYYY_HHMISS') || '.csv'
           INTO l_filename
           FROM DUAL;

         write_log (' l_filename =>' || l_filename);
         write_log (' Setting the File Name');
      --px_file_name := l_filename;
      END IF;

      write_log (' Opening the File handler');

      IF px_directory_name IS NULL
      THEN
         write_log (' Setting the directory');
         px_directory_name := 'ORACLE_INT_UC4';
      END IF;

      --Open the file handler
      v_file :=
         UTL_FILE.fopen (LOCATION       => NVL (px_directory_name,
                                                'ORACLE_INT_UC4'
                                               ),
                         filename       => NVL (px_file_name, l_filename),
                         open_mode      => 'w'
                        );
      --Write the file haeder
      write_log (' Writing to the file header...');
      UTL_FILE.put_line (v_file, 'VER' || ',' || 'IGOPENBAL' || ',' || '1.0');

      FOR cur_rec IN c_data
      LOOP
         write_log (' Writing to the file ...');
         UTL_FILE.put_line (v_file,
                               cur_rec.trx_number
                            || ','
                            || cur_rec.invoice_suffix
                            || ','
                            || cur_rec.open_balance
                            || ','
                            || cur_rec.account_number
                           );
      END LOOP;

      write_log (' Closing the the file ...');
      --Closing the file handler
      UTL_FILE.fclose (v_file);
   EXCEPTION
      WHEN UTL_FILE.invalid_path
      THEN
         UTL_FILE.fclose (v_file);
         errbuf := 'File Path is invalid.';
         retcode := '2';
         write_error (errbuf);
      WHEN UTL_FILE.invalid_mode
      THEN
         UTL_FILE.fclose (v_file);
         errbuf := 'The open_mode parameter in FOPEN is invalid.';
         retcode := '2';
         write_error (errbuf);
      WHEN UTL_FILE.invalid_filehandle
      THEN
         UTL_FILE.fclose (v_file);
         errbuf := 'File handle is invalid..';
         retcode := '2';
         write_error (errbuf);
      WHEN UTL_FILE.invalid_operation
      THEN
         UTL_FILE.fclose (v_file);
         errbuf := 'File could not be opened or operated on as requested';
         retcode := '2';
         write_error (errbuf);
      WHEN UTL_FILE.write_error
      THEN
         UTL_FILE.fclose (v_file);
         errbuf :=
                 'Operating system error occurred during the write operation';
         retcode := '2';
         write_error (errbuf);
      WHEN UTL_FILE.internal_error
      THEN
         UTL_FILE.fclose (v_file);
         write_log ('Unspecified PL/SQL error.');
      WHEN UTL_FILE.file_open
      THEN
         UTL_FILE.fclose (v_file);
         errbuf := 'The requested operation failed because the file is open.';
         retcode := '2';
         write_error (errbuf);
      WHEN UTL_FILE.invalid_filename
      THEN
         UTL_FILE.fclose (v_file);
         errbuf := 'The filename parameter is invalid.';
         retcode := '2';
         write_error (errbuf);
      WHEN UTL_FILE.access_denied
      THEN
         UTL_FILE.fclose (v_file);
         errbuf := 'Permission to access to the file location is denied.';
         retcode := '2';
         write_error (errbuf);
      WHEN OTHERS
      THEN
         UTL_FILE.fclose (v_file);
         errbuf := 'Error Msg :' || SQLERRM;
         retcode := '2';
         write_error (errbuf);
   END open_balance;

   /*************************************************************************
   *   Procedure : submit_job
   *
   *   PURPOSE:   This procedure is called by UC4 to initiate the BT Interfaces
   *   Parameter:
   *          IN
   *              p_debug_msg      -- Debug Message
   *
   * 1.0       07/23/2014  Maharajan Shunmugam     TMS# 20140723-00171 New parameter'Apply unearn discounts' handling in process lockbox
   * 2.0       07/30/2014  Maharajan Shunmugam     TMS# 20140731-00049 New parameter'No of instances' handling in process lockbox 
   * 22.0      09/04/2014  Gopi Damuluri           TMS# 20140903-00159 Change the Concurrent Program Wait Time to 21600 seconds (6hrs)
   * 29.0      01/08/2016  Kishorebabu V           TMS# 20150804-00061 AR - Billtrust lockbox program sending developer alerts for data related issues
   * ************************************************************************/
   PROCEDURE submit_job (
      errbuf                  OUT      VARCHAR2,
      retcode                 OUT      VARCHAR2,
      p_user_name             IN       VARCHAR2,
      p_responsibility_name   IN       VARCHAR2,
      p_program_name          IN       VARCHAR2,
      p_ob_directory_name     IN       VARCHAR2,
      p_operating_unit_name   IN       VARCHAR2,
      p_rcp_file_name         IN       VARCHAR2
   )
   IS
      -- Variable definitions
      l_package               VARCHAR2 (50)  := 'XXWC_AR_BILL_TRUST_INTF_PKG';
      l_req_id                NUMBER          NULL;
      v_phase                 VARCHAR2 (50);
      v_status                VARCHAR2 (50);
      v_dev_status            VARCHAR2 (50);
      v_dev_phase             VARCHAR2 (50);
      v_message               VARCHAR2 (250);
      v_error_message         VARCHAR2 (3000);
      l_err_msg               VARCHAR2 (3000);
      l_err_code              NUMBER;
      l_statement             VARCHAR2 (9000);
      l_user_id               NUMBER;
      l_responsibility_id     NUMBER;
      l_resp_application_id   NUMBER;
      l_org_id                NUMBER;
      l_valid_directory       VARCHAR2 (240);
      l_transmission_name     VARCHAR2 (240);
      l_transmission_seq      NUMBER;
      l_trans_exists          VARCHAR2 (1);
      l_receipt_ib_path       VARCHAR2 (240);
      l_count_lockbox_recs    NUMBER;
      l_err_callfrom          VARCHAR2 (75)
                                        DEFAULT 'XXWC_AR_BILL_TRUST_INTF_PKG';
      l_err_callpoint         VARCHAR2 (75)        DEFAULT 'SUBMIT_JOB';
      l_distro_list           VARCHAR2 (75)
                                   DEFAULT 'HDSOracleDevelopers@hdsupply.com';
      l_application_name      VARCHAR2 (30);
      l_program_short_name    VARCHAR2 (240);
      l_argument1             VARCHAR2 (240);
      l_argument2             VARCHAR2 (240);
      l_argument3             VARCHAR2 (240);
      l_argument4             VARCHAR2 (240);
      l_argument5             VARCHAR2 (240);
      l_argument6             VARCHAR2 (240);
      l_argument7             VARCHAR2 (240);
      l_argument8             VARCHAR2 (240);
      l_argument9             VARCHAR2 (240);
      l_argument10            VARCHAR2 (240);
      l_argument11            VARCHAR2 (240);
      l_argument12            VARCHAR2 (240);
      l_argument13            VARCHAR2 (240);
      l_argument14            VARCHAR2 (240);
      l_argument15            VARCHAR2 (240);
      l_argument16            VARCHAR2 (240);
      l_argument17            VARCHAR2 (240);
      l_argument18            VARCHAR2 (240);
      l_argument19            VARCHAR2 (240);
      l_argument20            VARCHAR2 (240);
      l_argument21            VARCHAR2 (240);
      l_argument22            VARCHAR2 (240);        --Added for ver 1.0 by Maha on 7/23/14 
      l_argument23            VARCHAR2 (240);        --Added for ver 2.0 by Maha on 7/30/14
      l_valid                 VARCHAR2 (1);
   BEGIN
      DBMS_OUTPUT.put_line ('Entering submit_job...');
      DBMS_OUTPUT.put_line ('p_user_name ' || p_user_name);
      DBMS_OUTPUT.put_line ('p_responsibility_name ' || p_responsibility_name);
      DBMS_OUTPUT.put_line ('p_program_name ' || p_program_name);
      DBMS_OUTPUT.put_line ('p_ob_directory_name ' || p_ob_directory_name);
      DBMS_OUTPUT.put_line ('p_operating_unit_name ' || p_operating_unit_name);
      DBMS_OUTPUT.put_line ('p_rcp_file_name ' || p_rcp_file_name);
      -- Resetting verification variable
      l_user_id := NULL;
      l_responsibility_id := NULL;
      l_resp_application_id := NULL;
      l_application_name := NULL;
      l_program_short_name := NULL;
      l_valid_directory := NULL;
      l_org_id := NULL;
      /*l_argument1               := null;
      l_argument2             := null;
      l_argument3             := null;
      l_argument4             := null;
      l_argument5             := null;
      l_argument6             := null;
      l_argument7             := null;
      l_argument8             := null;
      l_argument9             := null;
      l_argument10            := null;
      l_argument11            := null;
      l_argument12            := null;
      l_argument13            := null;
      l_argument14            := null;
      l_argument15            := null;
      l_argument16            := null;
      l_argument17            := null;
      l_argument18            := null;
      l_argument19            := null;
      l_argument20            := null;
      l_argument21            := null;*/
      l_transmission_name := NULL;
      l_transmission_seq := 0;
      l_trans_exists := NULL;
      l_receipt_ib_path := NULL;

      -- Deriving Ids from initalization variables
      BEGIN
         SELECT user_id
           INTO l_user_id
           FROM fnd_user
          WHERE user_name = UPPER (p_user_name)
            AND SYSDATE BETWEEN start_date AND NVL (end_date,
                                                    TRUNC (SYSDATE) + 1
                                                   );
      EXCEPTION
         WHEN NO_DATA_FOUND
         THEN
            l_err_msg :=
                       'UserName ' || p_user_name || ' not defined in Oracle';
            RAISE PROGRAM_ERROR;
         WHEN OTHERS
         THEN
            l_err_msg :=
                      'Error deriving user_id for UserName - ' || p_user_name;
            RAISE PROGRAM_ERROR;
      END;

      BEGIN
         SELECT responsibility_id, application_id
           INTO l_responsibility_id, l_resp_application_id
           FROM fnd_responsibility_vl
          WHERE responsibility_name = p_responsibility_name
            AND SYSDATE BETWEEN start_date AND NVL (end_date,
                                                    TRUNC (SYSDATE) + 1
                                                   );
      EXCEPTION
         WHEN NO_DATA_FOUND
         THEN
            l_err_msg :=
                  'Responsibility '
               || p_responsibility_name
               || ' not defined in Oracle';
            RAISE PROGRAM_ERROR;
         WHEN OTHERS
         THEN
            l_err_msg :=
                  'Error deriving Responsibility_id for '
               || p_responsibility_name;
            RAISE PROGRAM_ERROR;
      END;

      -- Verifying Operating unit
      BEGIN
         SELECT organization_id
           INTO l_org_id
           FROM hr_operating_units
          WHERE NAME = p_operating_unit_name;
      EXCEPTION
         WHEN NO_DATA_FOUND
         THEN
            l_err_msg :=
                  'Operating unit '
               || p_operating_unit_name
               || ' is invalid in Oracle';
            RAISE PROGRAM_ERROR;
         WHEN OTHERS
         THEN
            l_err_msg :=
                  'Other Operating unit validation error for '
               || p_operating_unit_name
               || '. Error: '
               || SUBSTR (SQLERRM, 1, 250);
            RAISE PROGRAM_ERROR;
      END;

      -- Verifying program name
      BEGIN
         SELECT fcp.concurrent_program_name, fa.application_short_name
           INTO l_program_short_name, l_application_name
           FROM fnd_concurrent_programs fcp, fnd_application fa
          WHERE UPPER (fcp.concurrent_program_name) = UPPER (p_program_name)
            AND fcp.application_id = fa.application_id;
      EXCEPTION
         WHEN NO_DATA_FOUND
         THEN
            l_err_msg :=
                     'Program ' || p_program_name || ' not defined in Oracle';
            RAISE PROGRAM_ERROR;
         WHEN OTHERS
         THEN
            l_err_msg :=
                  'Error finding program/application for '
               || p_program_name
               || ' Error: '
               || SUBSTR (SQLERRM, 1, 250);
            RAISE PROGRAM_ERROR;
      END;

      l_valid := NULL;

      IF p_program_name IN
            ('XXWC_AR_BILL_TRUST_STATEMENTS', 'XXWC_AR_BILL_TRUST_INVOICES',
             'XXWC_AR_BILL_TRUST_OPENBAL')
      THEN
         DBMS_OUTPUT.put_line ('Verifying outbound parameters...');

         IF p_ob_directory_name IS NULL
         THEN
            DBMS_OUTPUT.put_line
               ('Outbound Directory Name cant be NULL for this Bill Trust Interface'
               );
            l_err_msg :=
               'Outbound Directory Name cant be NULL for this Bill Trust Interface';
            RAISE PROGRAM_ERROR;
         ELSE
            l_argument1 := p_ob_directory_name;
            l_argument2 := NULL;
            l_valid := 'Y';
         END IF;
      /*
      -- Cant use the view to validate the directory
      begin
          select  directory_name
          into    l_valid_directory
          from    dba_directories
          where   directory_name = p_ob_directory_name;
      exception
      when no_data_found then
          dbms_output.put_line('Outbound directory '||p_ob_directory_name||' not defined in Oracle');
          l_err_msg := 'Outbound directory '||p_ob_directory_name||' not defined in Oracle';
          RAISE program_error;
      when others then
          dbms_output.put_line('Other Outbound directory validation error for '||p_ob_directory_name||'. Error: '||substr(SQLERRM,1,250));
          l_err_msg := 'Other Outbound directory validation error for '||p_ob_directory_name||'. Error: '||substr(SQLERRM,1,250);
          RAISE program_error;
      end;*/
      ELSIF p_program_name = 'ARLPLB'
      THEN
         DBMS_OUTPUT.put_line ('Verifying inbound parameters...');
         -- Lockbox Launch for Receipts
         l_argument1 := 'Y';                              -- New Transmission
         l_argument2 := NULL;                              -- Transmission Id
         l_argument3 := NULL;                          -- Original Request Id
         l_argument4 := NULL;                            -- Transmission Name
         l_transmission_name := NULL;
         l_trans_exists := NULL;

         <<retry_name>>
         SELECT    'XXWC_BT_'
                || TO_CHAR (SYSDATE, 'MMDDYYYY')
                || DECODE (l_transmission_seq,
                           0, NULL,
                           '_' || l_transmission_seq
                          )
           INTO l_transmission_name
           FROM DUAL;

         BEGIN
            SELECT 'Y'
              INTO l_trans_exists
              FROM ar_transmissions_all
             WHERE transmission_name = l_transmission_name
               AND org_id = l_org_id;

            l_transmission_seq := l_transmission_seq + 1;
            GOTO retry_name;
         EXCEPTION
            WHEN NO_DATA_FOUND
            THEN
               l_argument4 := l_transmission_name;
            WHEN OTHERS
            THEN
               l_transmission_seq := l_transmission_seq + 1;
               GOTO retry_name;
         END;

         -- Import Submission Block
         l_argument5 := 'Y';                                  -- Submit Import

         IF p_rcp_file_name IS NULL
         THEN
            DBMS_OUTPUT.put_line ('Inbound Receipt file name cannot be null');
            l_err_msg := 'Inbound Receipt file name cannot be null';
            RAISE PROGRAM_ERROR;
         ELSE
            l_argument6 :=
                  fnd_profile.VALUE ('XXWC_BT_RECEIPT_IB_PATH')
               || p_rcp_file_name;                                -- Data File
         END IF;

         l_argument7 := 'XXWC_BT_LOCKBOX_LOAD';                -- Control File

         BEGIN
            SELECT TO_CHAR (transmission_format_id)
              INTO l_argument8                       -- Transmission Format Id
              FROM ar_transmission_formats
             WHERE format_name = 'XXWC_LOCKBOX_BT'
                   AND status_lookup_code = 'A';
         EXCEPTION
            WHEN OTHERS
            THEN
               DBMS_OUTPUT.put_line
                      ('Bill Trust transmission format not defined in Oracle');
               l_err_msg :=
                       'Bill Trust transmission format not defined in Oracle';
               RAISE PROGRAM_ERROR;
         END;

         -- Submit Validation Portion
         l_argument9 := 'Y';                              -- Submit Validation
         l_argument10 := 'N';                        -- Pay Unrelated Invoices

         BEGIN
            SELECT TO_CHAR (lockbox_id)
              INTO l_argument11                                  -- Lockbox Id
              FROM ar_lockboxes_all
             WHERE lockbox_number = 'BT9999' AND status = 'A';
         EXCEPTION
            WHEN OTHERS
            THEN
               DBMS_OUTPUT.put_line
                                  ('Bill Trust Lockbox not defined in Oracle');
               l_err_msg := 'Bill Trust Lockbox not defined in Oracle';
               RAISE PROGRAM_ERROR;
         END;

         l_argument12 := NULL;                                      -- GL Date
         l_argument13 := 'A';                                 -- Report Format
         l_argument14 := 'N';                         -- Complete Batches Only
         l_argument15 := 'Y';                              -- Submit Postbatch
         l_argument16 := 'N';                  -- Alternate name search option
         l_argument17 := 'Y';  -- Post Partial Amount or Reject Entire Receipt
         l_argument18 := NULL;                       -- USSGL Transaction Code
         l_argument19 := l_org_id;                          -- Organization Id
--//Added Apply earn discount for ver 20.0 by Maha on 7/23/2014
         l_argument20 := 'Y';                       --Apply unearn discount   
         l_argument21 :=  1 ;                        --No of instances
         l_argument22 := 'L';                               -- Submission Type
         l_argument23 := NULL ;                                -- Scoring model

        -- l_argument20 := 'L';                               -- Submission Type
        -- l_argument21 := NULL;                                -- Scoring model
         l_valid := 'Y';
      ELSE
         DBMS_OUTPUT.put_line
                            ('Invalid program name for Bill Trust Processing');
         l_valid := 'N';
         l_err_msg := 'Invalid program name for Bill Trust Processing';
         RAISE PROGRAM_ERROR;
      END IF;

      -- Environment Initialization
      fnd_global.apps_initialize (l_user_id,
                                  l_responsibility_id,
                                  l_resp_application_id
                                 );
      mo_global.set_policy_context ('S', l_org_id);
      fnd_request.set_org_id (l_org_id);

      IF NVL (l_valid, 'N') = 'Y'
      THEN
         IF p_program_name IN
               ('XXWC_AR_BILL_TRUST_STATEMENTS',
                'XXWC_AR_BILL_TRUST_OPENBAL')
         THEN
            l_req_id :=
               fnd_request.submit_request (application      => l_application_name,
                                           program          => l_program_short_name,
                                           description      => NULL,
                                           start_time       => SYSDATE,
                                           sub_request      => FALSE,
                                           argument1        => l_argument1,
                                           argument2        => l_argument2
                                          );
         ELSIF p_program_name = 'XXWC_AR_BILL_TRUST_INVOICES'
         THEN
            l_req_id :=
               fnd_request.submit_request (application      => l_application_name,
                                           program          => l_program_short_name,
                                           description      => NULL,
                                           start_time       => SYSDATE,
                                           sub_request      => FALSE,
                                           argument1        => l_argument1,
                                           argument2        => l_argument2,
                                           argument3        => l_argument3,
                                           argument4        => l_argument4
                                          );
         ELSIF p_program_name = 'ARLPLB'
         THEN
            DBMS_OUTPUT.put_line ('Submitting lockbox...');
            l_req_id :=
               fnd_request.submit_request (application      => l_application_name,
                                           program          => l_program_short_name,
                                           description      => NULL,
                                           start_time       => SYSDATE,
                                           sub_request      => FALSE,
                                           argument1        => l_argument1,
                                           argument2        => l_argument2,
                                           argument3        => l_argument3,
                                           argument4        => l_argument4,
                                           argument5        => l_argument5,
                                           argument6        => l_argument6,
                                           argument7        => l_argument7,
                                           argument8        => l_argument8,
                                           argument9        => l_argument9,
                                           argument10       => l_argument10,
                                           argument11       => l_argument11,
                                           argument12       => l_argument12,
                                           argument13       => l_argument13,
                                           argument14       => l_argument14,
                                           argument15       => l_argument15,
                                           argument16       => l_argument16,
                                           argument17       => l_argument17,
                                           argument18       => l_argument18,
                                           argument19       => l_argument19,
                                           argument20       => l_argument20,
                                           argument21       => l_argument21,
                                     argument22       => l_argument22,        --Added by Maha for ver 20.0 on 7/23/2014
                                           argument23       => l_argument23         --Added by Maha for ver 20.0 on 7/23/2014
                                          );
         ELSE
            l_statement := 'Unknown program';
            l_err_msg := l_statement;
            DBMS_OUTPUT.put_line (l_statement);
            RAISE PROGRAM_ERROR;
         END IF;

         COMMIT;
         DBMS_OUTPUT.put_line
                      (   'Concurrent Program Request Submitted. Request ID: '
                       || l_req_id
                      );

         IF (l_req_id != 0)
         THEN
            IF fnd_concurrent.wait_for_request
                                       (l_req_id,
                                        6,
                                        21600 --3600  -- 04/17/2012 Changed from 15000 -- Version# 22.0
                                            ,
                                        v_phase,
                                        v_status,
                                        v_dev_phase,
                                        v_dev_status,
                                        v_message
                                       )
            THEN
               v_error_message :=
                     'ReqID:'
                  || l_req_id
                  || '-DPhase:'
                  || v_dev_phase
                  || '-DStatus:'
                  || v_dev_status
                  || CHR (10)
                  || 'MSG:'
                  || v_message;

               -- Error Returned
               IF v_dev_phase != 'COMPLETE' OR v_dev_status != 'NORMAL'
               THEN
                  l_statement :=
                        'EBS Conc Prog Completed with problems '
                     || v_error_message
                     || '.';
                  l_err_msg := l_statement;
                  DBMS_OUTPUT.put_line (l_statement);
                  RAISE PROGRAM_ERROR;
               ELSE
                  -- For Lockbox Determine if there are failed records and error process
                  IF p_program_name = 'ARLPLB'
                  THEN
                     l_count_lockbox_recs := 0;

                     BEGIN
                        SELECT COUNT (*)
                          INTO l_count_lockbox_recs
                          FROM ar_payments_interface_all
                         WHERE transmission_request_id = l_req_id;
                     EXCEPTION
                        WHEN OTHERS
                        THEN
                           l_count_lockbox_recs := 0;
                     END;

                     IF l_count_lockbox_recs > 0
                     THEN
                        l_statement :=
                              'Errored receipts in lockbox interface from request id '
                           || l_req_id
                           || '. Please review log file and table';
                        l_err_msg := l_statement;
                        DBMS_OUTPUT.put_line (l_statement);
                        -- RAISE PROGRAM_ERROR;  --Commented as per ver 29.0
                     ELSE
                        retcode := 1;
                     END IF;
                  ELSE
                     retcode := 1;
                  END IF;
               END IF;
            ELSE
               l_statement := 'EBS Conc Program Wait timed out';
               l_err_msg := l_statement;
               DBMS_OUTPUT.put_line (l_statement);
               RAISE PROGRAM_ERROR;
            END IF;
         ELSE
            l_statement := 'EBS Conc Program not initated';
            l_err_msg := l_statement;
            DBMS_OUTPUT.put_line (l_statement);
            RAISE PROGRAM_ERROR;
         END IF;
      END IF;

      DBMS_OUTPUT.put_line ('Exiting submit_job...');
   EXCEPTION
      WHEN PROGRAM_ERROR
      THEN
         ROLLBACK;
         l_err_code := 2;
         l_err_msg :=
            SUBSTR ((l_err_msg || ' ERROR ' || SUBSTR (SQLERRM, 1, 2000)),
                    1,
                    2000
                   );
         DBMS_OUTPUT.put_line (l_err_msg);
         xxcus_error_pkg.xxcus_error_main_api
                                        (p_called_from            => l_err_callfrom,
                                         p_calling                => l_err_callpoint,
                                         p_request_id             => l_req_id,
                                         p_ora_error_msg          => SUBSTR
                                                                        (SQLERRM,
                                                                         1,
                                                                         2000
                                                                        ),
                                         p_error_desc             => SUBSTR
                                                                        (l_err_msg,
                                                                         1,
                                                                         2000
                                                                        ),
                                         p_distribution_list      => l_distro_list,
                                         p_module                 => 'AR'
                                        );
         retcode := l_err_code;
         errbuf := l_err_msg;
      WHEN OTHERS
      THEN
         l_err_code := 2;
         l_err_msg :=
            SUBSTR ((l_err_msg || ' ERROR ' || SUBSTR (SQLERRM, 1, 2000)),
                    1,
                    2000
                   );
         DBMS_OUTPUT.put_line (l_err_msg);
         xxcus_error_pkg.xxcus_error_main_api
                                        (p_called_from            => l_err_callfrom,
                                         p_calling                => l_err_callpoint,
                                         p_request_id             => l_req_id,
                                         p_ora_error_msg          => SUBSTR
                                                                        (SQLERRM,
                                                                         1,
                                                                         2000
                                                                        ),
                                         p_error_desc             => SUBSTR
                                                                        (l_err_msg,
                                                                         1,
                                                                         2000
                                                                        ),
                                         p_distribution_list      => l_distro_list,
                                         p_module                 => 'AR'
                                        );
         retcode := l_err_code;
         errbuf := l_err_msg;
   END submit_job;

-- Version# 18.0 > Start
   -- Function to derive DeliveryId detals
   FUNCTION get_delivery_id (p_cust_trx_id IN NUMBER) return VARCHAR2
   IS
      l_del_id    VARCHAR2 (200);
      l_row_cnt   NUMBER := 1;
   BEGIN
     FOR rec_del IN (SELECT distinct stg.delivery_id  
                       FROM ra_Customer_Trx_lines_all rcta
                          , xxwc_wsh_shipping_stg     stg
                      WHERE interface_line_attribute2 = 'STANDARD ORDER'
                        AND INTERFACE_LINE_ATTRIBUTE6 = stg.line_id
                        AND rcta.customer_trx_id      = p_cust_trx_id) 
     LOOP
       
       IF l_row_cnt = 1 THEN
         l_del_id  := 'Delivery Tag#: '||rec_del.delivery_id;
         l_row_cnt := l_row_cnt + 1;
       ELSE
         l_del_id  := l_del_id||' , '||rec_del.delivery_id;
       END IF;
     END LOOP;   
  
     RETURN l_del_id;
   END get_delivery_id;
-- Version# 18.0 < End

END XXWC_AR_BILL_TRUST_INTF_PKG;
/