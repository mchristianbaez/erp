/*************************************************************************
  $Header TMS_20160823-00130 _SHIPPED_TO_CLOSE.sql $
  Module Name: TMS_20160922-00038  


  REVISIONS:
  Ver        Date         Author                Description
  ---------  -----------  ------------------    --------------------------
  1.0        03-OCT-2016  Niraj K Ranjan        TMS#20160922-00038 

**************************************************************************/ 
SET SERVEROUTPUT ON SIZE 1000000
DECLARE
BEGIN
   DBMS_OUTPUT.put_line ('TMS: 20160922-00038    , Before Update');

   update apps.oe_order_lines_all
   set 
     INVOICE_INTERFACE_STATUS_CODE='NOT_ELIGIBLE'
    ,open_flag='N'
    ,flow_status_code='CLOSED'
    --,INVOICED_QUANTITY=1
   where line_id =79489524
   and headeR_id= 48632314;

   DBMS_OUTPUT.put_line (
         'TMS: 20160922-00038  Sales order lines updated (Expected:1): '
      || SQL%ROWCOUNT);

   COMMIT;

   DBMS_OUTPUT.put_line ('TMS: 20160922-00038    , End Update');
EXCEPTION
   WHEN OTHERS
   THEN
      ROLLBACK;
      DBMS_OUTPUT.put_line ('TMS: 20160922-00038 , Errors : ' || SQLERRM);
END;
/
