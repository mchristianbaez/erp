/***********************************************************************************************************************************************
   NAME:     TMS#20180830-00007_Create_DB_Link.sql
   PURPOSE:  Create DB link from EBS TO APEX

   REVISIONS:
   Ver        Date        Author           Description
   ---------  ----------  ---------------  ----------------------------------------------------------------------------------------------------
   1.0        08/30/2018  Rakesh Patel    TMS#20180830-00007-Create DB link from EBS TO APEX
************************************************************************************************************************************************/
SET SERVEROUTPUT ON SIZE 1000000

--for ebsqa with INTERFACE_APEXWC
/*DROP database link apexwc.HSI.HUGHESSUPPLY.COM;

CREATE DATABASE LINK apexwc.hsi.hughessupply.com
CONNECT TO INTERFACE_APEXWC
IDENTIFIED BY HDSadmin 
USING '(DESCRIPTION =(ADDRESS = (PROTOCOL = TCP)(HOST = wcapxqa.hdsupply.net)(PORT = 1521)) (CONNECT_DATA = (SERVER = DEDICATED) (SERVICE_NAME = wcapxqa)))'; */

--for apexwc with INTERFACE_APEXWC

/*CREATE DATABASE LINK apexwc.hsi.hughessupply.com
CONNECT TO INTERFACE_APEXWC
IDENTIFIED BY HDSadmin 
USING '(DESCRIPTION =(ADDRESS = (PROTOCOL = TCP)(HOST = wcapxprd.hdsupply.net)(PORT = 1521)) (CONNECT_DATA = (SERVER = DEDICATED) (SERVICE_NAME = wcapxprd)))'; */

DROP database link wcapxprd.HSI.HUGHESSUPPLY.COM;

CREATE DATABASE LINK wcapxprd.HSI.HUGHESSUPPLY.COM
CONNECT TO WC_APPS
IDENTIFIED BY apps4wcprd 
USING '(DESCRIPTION =(ADDRESS = (PROTOCOL = TCP)(HOST = wcapxprd.hdsupply.net)(PORT = 1521)) (CONNECT_DATA = (SERVER = DEDICATED) (SERVICE_NAME = wcapxprd)))';