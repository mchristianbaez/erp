/* Formatted on 12/16/2016 9:06:30 AM (QP5 v5.265.14096.38000) */
--
-- XXWCAR_DCTM_CUSTOMER_SITE_VW  (View)
-- reverting the multi org changes

  /**************************************************************************
     $Header XXWCAR_DCTM_CUSTOMER_SITE_VW$
     Module Name: XXWCAR_DCTM_CUSTOMER_SITE_VW.vw

     PURPOSE:   This is a view for Documentum credit Applciation .

     REVISIONS:
     Ver        Date        Author             Description
     ---------  ----------  ---------------   -------------------------
      1.0       unknowm     unknown            Initial creation
      1.1       11/12/2016  Neha Saini         changes for - Task ID: 20161215-00232
      1.2       18/01/2017  Niraj K Ranjan     TMS#20161215-00232 Tax - update xxwcar_dctm_customer_site_vw
   /*************************************************************************/

CREATE OR REPLACE FORCE VIEW apps.xxwcar_dctm_customer_site_vw
(
   custno,
   contactid,
   location,
   site_use_code,                                                     --ver1.1
   site_use_id,                                                       --ver1.2
   prism_customer_number,                                             --ver1.2
   status                                                             --ver1.1
)
AS
   SELECT DISTINCT
          hca.account_number,
          hps.party_site_number,
          hcsu.location,
          hcsu.site_use_code,                             --changes for ver1.1
		  hcsu.site_use_id,                                --changes for ver1.2
		  hca.attribute6 prism_customer_number,           --changes for ver1.2
          DECODE (hcas.status,
                  'A', 'Active',
                  'I', 'Inactive',
                  hcas.status)                            --changes for ver1.1
     FROM ar.hz_cust_accounts hca,
          ar.hz_cust_site_uses_all hcsu,
          ar.hz_cust_acct_sites_all hcas,
          ar.hz_party_sites hps
    WHERE     hca.cust_account_id = hcas.cust_account_id
          AND hcas.party_site_id = hps.party_site_id(+)
          AND hcas.cust_acct_site_id = hcsu.cust_acct_site_id(+)
		  AND hcsu.site_use_code = 'BILL_TO'              --changes for ver1.2
          --AND hcsu.status = 'A'                           --changes for ver1.2
          AND hcas.org_id = 162
   /*Below all changes for ver1.2*/
   UNION
   SELECT DISTINCT
          hca.account_number,
          hps.party_site_number,
          hcsu.location,
          hcsu.site_use_code,  
          hcsu.site_use_id,		  
          hca.attribute6 prism_customer_number,
          DECODE (hcas.status,         
                  'A', 'Active',
                  'I', 'Inactive',
                  hcas.status)                            
     FROM ar.hz_cust_accounts hca,
          ar.hz_cust_site_uses_all hcsu,
          ar.hz_cust_acct_sites_all hcas,
          ar.hz_party_sites hps
    WHERE     hca.cust_account_id = hcas.cust_account_id
          AND hcas.party_site_id = hps.party_site_id
          AND hcas.cust_acct_site_id = hcsu.cust_acct_site_id
          AND hcsu.site_use_code = 'SHIP_TO'
          --AND hcsu.status = 'A'
          AND NOT EXISTS
                 (SELECT '1'
                    FROM ar.hz_cust_site_uses_all hcsu_b
                   WHERE     hcsu_b.cust_acct_site_id =
                                hcsu.cust_acct_site_id
                         AND hcsu_b.site_use_code = 'BILL_TO')
          AND hcas.org_id = 162;