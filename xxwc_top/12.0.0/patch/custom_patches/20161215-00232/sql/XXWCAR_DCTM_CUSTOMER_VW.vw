/**************************************************************************
   $Header XXWCAR_DCTM_CUSTOMER_VW$
   Module Name: XXWCAR_DCTM_CUSTOMER_VW.vw

   PURPOSE:   This is a view for Documentum credit Applciation .

   REVISIONS:
   Ver        Date        Author             Description
   ---------  ----------  ---------------   -------------------------
    1.0       unknowm     unknown            Initial creation
    1.1       19/01/2017  Niraj K Ranjan     TMS#20161215-00232   Tax - update xxwcar_dctm_customer_site_vw
/*************************************************************************/
CREATE OR REPLACE FORCE VIEW "APPS"."XXWCAR_DCTM_CUSTOMER_VW" 
("CUSTNO", "COMPANY", "COLLECTOR", "STATUS", "ADDRESS_LINE1", "CITY", "STATE_PROVICE", "ZIP_CODE") AS 
  SELECT hca.account_number
      ,hca.account_name
      ,ac.description
	  ,hca.status                             --Ver 1.1
      ,hl.address1
      ,hl.city
      ,nvl(hl.state, hl.province) state_provice
      ,hl.postal_code      
  FROM ar.hz_cust_accounts          hca
      ,ar.hz_cust_acct_sites_all    hcas
      ,ar.hz_customer_profiles      hcp
      ,ar.ar_collectors             ac
      ,ar.hz_party_sites            hps
      ,ar.hz_locations              hl
      ,ar.hz_cust_site_uses_all     hcsu
 WHERE hca.cust_account_id = hcas.cust_account_id
   AND hcas.org_id = 162
   --AND hca.status = 'A'                     --Ver 1.1
   AND hca.cust_account_id = hcp.cust_account_id
   AND hcp.collector_id = ac.collector_id
   AND hcp.site_use_id IS NULL
   AND hcas.party_site_id = hps.party_site_id
   AND hps.location_id = hl.location_id
   AND hcas.cust_acct_site_id = hcsu.cust_acct_site_id
   AND hcas.org_id = hcsu.org_id
   AND hcsu.site_use_code = 'BILL_TO'
   AND hcsu.primary_flag = 'Y'
   --SR 190094 adding address to cust account
;
