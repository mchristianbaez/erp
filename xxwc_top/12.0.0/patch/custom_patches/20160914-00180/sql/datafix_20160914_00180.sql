/************************************************************************************************************************************
  $Header datafix_20160914_00180.sql $
  Module Name: TMS#20160914-00180 Data Fix script.

  PURPOSE: To purge data from XXWC_INV_MIN_MAX_TEMP table

  REVISIONS:
  Ver        Date         Author                Description
  ---------  -----------  ------------------    ---------------------------------------------------------------------------------
  1.0        12-sep-2016  P.Vamshidhar          TMS#20160914-00180 - XXWC.XXWC_ISR_DETAILS_082316

***********************************************************************************************************************************/ 

SET SERVEROUTPUT ON SIZE 100000;

SET VERIFY OFF;

DROP TABLE XXWC.XXWC_ISR_DETAILS_082316;
/











