--//============================================================================
--//
--// Object Name         :: xxwc_ar_cust_attribute_v
--//
--// Object Type         :: View
--//
--// Object Description  :: This is customer attribute extract to agilone
--//
--// Version Control
--//============================================================================
--// Vers    Author                  Date          Description
--//----------------------------------------------------------------------------
--// 1.0     Maharajan Shunmugam     09/23/2015    TMS#20150824-00143 Agile One Supplemental Customer Attribute File
--//============================================================================
CREATE OR REPLACE VIEW apps.xxxwc_ar_cust_attribute_v
(cust_attr)
AS
   SELECT ( hps.party_site_number||hcsua.site_use_id
            || '|'
            || hp.party_number
            || '|'
            || hp.party_name
            || '|'
            || DECODE(hcsua.site_use_code||hcsua.primary_flag, 'BILL_TOY',1,0))
             cust_attr
     FROM hz_cust_accounts hca
      ,hz_parties hp
      ,fnd_lookup_values cust_type
      ,fnd_lookup_values cust_class
      ,hz_customer_profiles hcp
      ,hz_cust_profile_amts hcpa
      ,xxwc_ar_payment_terms_vw rt
      -- Sites
      ,hz_cust_acct_sites_all hcasa
      ,hz_party_sites hps
      ,hz_locations hl
      ,hz_cust_site_uses_all hcsua
      -- Salesrep
      ,jtf_rs_salesreps jrs
      --, per_all_people_f papf
      -- Default Bill To (where applicable)
      ,hz_cust_site_uses_all hcsua_def_bill
      ,hz_cust_acct_sites_all hcasa_def_bill
      ,hz_party_sites hps_def_bill
      ,hz_locations hl_def_bill
      ,hz_cust_accounts hca_def_bill
      ,hz_parties hp_def_bill
      -- Operating Unit
      ,hr_operating_units hou
 WHERE     hca.party_id = hp.party_id
       -- Customer Type Lookup
       AND hca.customer_type = cust_type.lookup_code(+)
       AND cust_type.lookup_type(+) = 'CUSTOMER_TYPE'
       AND cust_type.language(+) = 'US'
       AND cust_type.view_application_id(+) = 222                                   -- Receivables Application
       -- Customer Class Lookup
       AND hca.customer_class_code = cust_class.lookup_code(+)
       AND cust_class.lookup_type(+) = 'CUSTOMER CLASS'
       AND cust_class.language(+) = 'US'
       AND cust_class.view_application_id(+) = 222                                  -- Receivables Application
       -- Customer profile amounts
       AND hca.cust_account_id = hcp.cust_account_id(+)
       AND hca.party_id = hcp.party_id(+)
       AND hcp.site_use_id(+) IS NULL
       AND hcp.cust_account_profile_id = hcpa.cust_account_profile_id(+)
       AND hcpa.site_use_id(+) IS NULL
       -- Payment_term
       AND hcp.standard_terms = rt.payterm_id(+)
       AND rt.operating_unit_id = hou.organization_id(+)
       -- Sites (ALL)
       AND hca.cust_account_id = hcasa.cust_account_id
       AND hcasa.party_site_id = hps.party_site_id
       AND hps.location_id = hl.location_id
       AND hcasa.cust_acct_site_id = hcsua.cust_acct_site_id
       -- Salesrep
       AND hcsua.primary_salesrep_id = jrs.salesrep_id(+)
       AND hcsua.org_id = jrs.org_id(+)
       -- Default Bill to (where applicable)
       AND hcsua.bill_to_site_use_id = hcsua_def_bill.site_use_id(+)
       AND hcsua_def_bill.cust_acct_site_id = hcasa_def_bill.cust_acct_site_id(+)
       AND hcasa_def_bill.party_site_id = hps_def_bill.party_site_id(+)
       AND hps_def_bill.location_id = hl_def_bill.location_id(+)
       AND hcasa_def_bill.cust_account_id = hca_def_bill.cust_account_id(+)
       AND hca_def_bill.party_id = hp_def_bill.party_id(+)
       -- Operating Unit
       AND hcsua.org_id = hou.organization_id
      AND hou.organization_id = 162
      ORDER BY hp.party_id;