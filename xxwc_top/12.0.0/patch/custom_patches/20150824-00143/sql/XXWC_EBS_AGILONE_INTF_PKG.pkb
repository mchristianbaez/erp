CREATE OR REPLACE PACKAGE BODY APPS.xxwc_ebs_agilone_intf_pkg
--//============================================================================
--//
--// Object Name         :: xxwc_ebs_agilone_intf_pkg
--//
--// Object Type         :: Package Specification
--//
--// Object Description  :: This is an outbound interface from oracle ebs to agilone.
--//
--// Version Control
--//============================================================================
--// Vers    Author           Date          Description
--//----------------------------------------------------------------------------
--// 1.0     Mahesh Kudle     26/05/2014    Initial Build - TMS#20140602-00295
--// 2.0     Maharajan S      09/23/2015    TMS#20150824-00143 Agile One Supplemental Customer Attribute File
--//============================================================================
AS

g_dflt_email            VARCHAR2(50) := 'HDSOracleDevelopers@hdsupply.com';
--//============================================================================
--//
--// Object Name         :: create_file
--//
--// Object Type         :: Procedure
--//
--// Object Description  :: This procedure creates file for the outbound extract
--//
--// Version Control
--//============================================================================
--// Vers    Author           Date          Description
--//----------------------------------------------------------------------------
--// 1.0     Mahesh Kudle     26/05/2014    Initial Build - TMS#20140602-00295
--//============================================================================
PROCEDURE create_file (p_errbuf           OUT  VARCHAR2
                      ,p_retcode          OUT  NUMBER
                      ,p_operating_unit   IN   NUMBER
                      ,p_directory_name   IN   VARCHAR2
                      ,p_source_name      IN   VARCHAR2
                      ,p_view_name        IN   VARCHAR2)
IS

      -- Intialize Variables
      l_err_msg               VARCHAR2 (2000);
      l_err_code              NUMBER;
	  l_err_callfrom          VARCHAR2(100) ;
      l_err_callpoint         VARCHAR2(100) ;
      
      -- Reference Cursor Variables
      TYPE ref_cur IS REF CURSOR;
      view_cur                 ref_cur;
      view_rec                 xxwc_ebs_agilone_intf_pkg.xxwc_ob_file_rec;

      --File Variables
      l_filehandle             UTL_FILE.file_type;
      l_filename               VARCHAR2 (240);
      l_base_filename          VARCHAR2 (240);
      l_view_name              VARCHAR2 (100);
      l_org_id                 NUMBER;
      l_org_name               VARCHAR2 (240);
      l_count                  NUMBER;
      l_query                  VARCHAR2 (2000);
      
      l_program_error          EXCEPTION;

   BEGIN

    l_err_callfrom := 'XXWC_EBS_AGILONE_INTF_PKG.CREATE_FILE';
    l_err_callpoint:= 'Create File';
	
      -- Building file name
      l_filename :=
            'WHITECAP_'
         || p_source_name
         || '_'
         || TO_CHAR (SYSDATE, 'YYYYMMDD')
         || '.csv';
      l_base_filename := l_filename;
      l_filename := 'TMP_' || l_filename;

       -- Delete if file already exists
        BEGIN
          UTL_FILE.fremove (p_directory_name,l_base_filename);
          UTL_FILE.fremove (p_directory_name,l_filename);
        EXCEPTION
        WHEN OTHERS THEN
          NULL;
        END;

      --Open the file handler
      l_filehandle := UTL_FILE.fopen (LOCATION     => p_directory_name,
                                      filename     => l_filename,
                                      open_mode    => 'w',
                                      max_linesize => 32767);

      -- Building out query
      DBMS_OUTPUT.put_line  ('Building Query');
      l_query := NULL;

      IF p_operating_unit IS NOT NULL
      THEN
         l_query :=
               'SELECT * FROM '
            || p_view_name
            || ' WHERE operating_unit_id = '
            || p_operating_unit;
      ELSE
         l_query := 'SELECT * FROM ' || p_view_name;
      END IF;

     -- write_log ('Section 1 built...');
     DBMS_OUTPUT.put_line  ('l_query ' || l_query);

      OPEN view_cur FOR l_query;

      DBMS_OUTPUT.put_line  ('Writing to the file ... Opening Cursor');
      l_count := 0;

      LOOP
         FETCH view_cur
          INTO view_rec;

         EXIT WHEN view_cur%NOTFOUND;
         UTL_FILE.put_line (l_filehandle, UPPER (view_rec.rec_line));
         l_count := l_count + 1;
      END LOOP;

      DBMS_OUTPUT.put_line  ('Wrote ' || l_count || ' records to file');
      DBMS_OUTPUT.put_line  ('Closing File Handler');

      --Closing the file handler
      UTL_FILE.fclose (l_filehandle);

      -- 'Rename file for pickup';
      UTL_FILE.frename(p_directory_name, l_filename, p_directory_name, l_base_filename);
	  
	  p_retcode := 0;

   EXCEPTION 
    WHEN l_program_error THEN
      l_err_code := 2;
	  p_retcode  := 2;

      utl_file.fclose(l_filehandle);
      utl_file.fremove(p_directory_name, l_base_filename);
	  
    WHEN OTHERS THEN
      l_err_code := 2;
	  p_retcode  := 2;
      l_err_msg  := ' ERROR ' || SQLCODE ||  substr(SQLERRM, 1, 2000);

      utl_file.fclose(l_filehandle);
      utl_file.fremove(p_directory_name, l_base_filename);

      XXCUS_error_pkg.XXCUS_error_main_api (
         p_called_from         => l_err_callfrom
        ,p_calling             => l_err_callpoint
        ,p_request_id          => -1
        ,p_ora_error_msg       => SQLERRM
        ,p_error_desc          => l_err_msg
        ,p_distribution_list   => g_dflt_email
        ,p_module              => 'APPS'); 

END CREATE_FILE;

--//============================================================================
--//
--// Object Name         :: main
--//
--// Object Type         :: Procedure
--//
--// Object Description  :: This procedure is called by UC4 to initiate Outbound Interfaces
--//
--// Version Control
--//============================================================================
--// Vers    Author           Date          Description
--//----------------------------------------------------------------------------
--// 1.0     Mahesh Kudle     26/05/2014    Initial Build - TMS#20140602-00295
--//============================================================================
PROCEDURE main (p_errbuf              OUT VARCHAR2
               ,p_retcode             OUT NUMBER
               ,p_operating_unit_name IN  VARCHAR2
               ,p_ob_directory_name   IN  VARCHAR2
               ,p_objects_source      IN  VARCHAR2
               ,p_view_name           IN  VARCHAR2)
IS

      -- Variable definitions
      l_err_msg             VARCHAR2 (3000);
      l_err_code            NUMBER;      
      l_org_id              NUMBER;
      l_dir_exists          NUMBER;
      l_view_exists         NUMBER;
	  l_err_callfrom        VARCHAR2(100) ;
      l_err_callpoint       VARCHAR2(100) ;

   BEGIN
   
    l_err_callfrom := 'XXWC_EBS_AGILONE_INTF_PKG.MAIN';
    l_err_callpoint:= 'Main';
	
      DBMS_OUTPUT.put_line ('Entering main...');
      DBMS_OUTPUT.put_line ('p_operating_unit_name: ' || p_operating_unit_name);
      DBMS_OUTPUT.put_line ('p_ob_directory_name: ' || p_ob_directory_name);
      DBMS_OUTPUT.put_line ('p_objects_source: ' || p_objects_source);
      DBMS_OUTPUT.put_line ('p_view_name: ' || p_view_name);

      -- Resetting verification variable
      l_org_id := NULL;

      -- Verifying Operating unit
      BEGIN
	  l_err_callpoint:= 'Validating Operating Unit';
         SELECT organization_id
           INTO l_org_id
           FROM hr_operating_units
          WHERE NAME = p_operating_unit_name;
      EXCEPTION
         WHEN NO_DATA_FOUND
         THEN
            l_err_msg := 'Operating unit '|| p_operating_unit_name|| ' is invalid in Oracle';
            RAISE program_error;
         WHEN OTHERS
         THEN
            l_err_msg :=
                  'Other Operating unit validation error for '|| p_operating_unit_name|| '. Error: '|| SUBSTR (SQLERRM, 1, 250);
            RAISE program_error;
      END;

      l_dir_exists := 0;
        BEGIN
		l_err_callpoint:= 'Validating Directory name';
          SELECT COUNT(1)
            INTO l_dir_exists
            FROM all_directories
           WHERE directory_name = p_ob_directory_name;
        EXCEPTION
        WHEN OTHERS THEN
          l_err_msg := 'Error validating directory   :'||SQLERRM;
          RAISE program_error;
        END;

        -- Raise ProgramError if directory path does not exist
        IF l_dir_exists = 0 THEN
          l_err_msg := 'Directory does not exist in Oracle :'||p_ob_directory_name;
          RAISE program_error;
        END IF;

      l_view_exists := 0;
        BEGIN
		l_err_callpoint:= 'Validating View name';
          SELECT COUNT(1)
            INTO l_view_exists
            FROM all_objects
           WHERE object_name = p_view_name
           and object_type = 'VIEW';
        EXCEPTION
        WHEN OTHERS THEN
          l_err_msg := 'Error validating view   :'||SQLERRM;
          RAISE program_error;
        END;

        -- Raise ProgramError if view does not exist
        IF l_view_exists = 0 THEN
          l_err_msg := 'View does not exist in Oracle :'||p_view_name;
          RAISE program_error;
        END IF;

      mo_global.set_policy_context ('S', l_org_id);

      DBMS_OUTPUT.put_line ( 'Entering CREATE_FILE...'  );

      CREATE_FILE (p_errbuf => p_errbuf,
                  p_retcode => p_retcode,
                  p_operating_unit  =>l_org_id,
                  p_directory_name  =>p_ob_directory_name,
                  p_source_name     =>p_objects_source,
                  p_view_name       =>p_view_name);

      DBMS_OUTPUT.put_line ( 'Exiting CREATE_FILE...'  );
      DBMS_OUTPUT.put_line ('Exiting main...');

      p_retcode := 0;

   EXCEPTION
      WHEN program_error
      THEN
         ROLLBACK;
         l_err_code := 2;
         l_err_msg :=
            SUBSTR ((l_err_msg || ' ERROR ' || SUBSTR (SQLERRM, 1, 2000)),
                    1,
                    2000
                   );
         DBMS_OUTPUT.put_line (l_err_msg);
        p_retcode := 2;
      WHEN OTHERS
      THEN
         l_err_code := 2;
         l_err_msg :=
            SUBSTR ((l_err_msg || ' ERROR ' || SUBSTR (SQLERRM, 1, 2000)),
                    1,
                    2000
                   );
         DBMS_OUTPUT.put_line (l_err_msg);
         p_retcode := 2;
		 
      XXCUS_error_pkg.XXCUS_error_main_api (
         p_called_from         => l_err_callfrom
        ,p_calling             => l_err_callpoint
        ,p_request_id          => -1
        ,p_ora_error_msg       => SQLERRM
        ,p_error_desc          => l_err_msg
        ,p_distribution_list   => g_dflt_email
        ,p_module              => 'APPS'); 
		
   END main;
   
--//============================================================================
--//
--// Object Name         :: xxwc_market_price_extract
--//
--// Object Type         :: Procedure
--//
--// Object Description  :: This procedure is used to generate national market 
--//                        extract to pricing team
--//
--// Version Control
--//============================================================================
--// Vers    Author           Date          Description
--//----------------------------------------------------------------------------
--// 1.0     Mahesh Kudle     26/05/2014    Initial Build - TMS#20140602-00295
--//============================================================================
PROCEDURE  xxwc_market_price_extract (p_errbuf             OUT VARCHAR2
                                     ,p_retcode            OUT NUMBER
                                     ,p_inventory_category IN VARCHAR2
                                     ,p_include_specials   IN VARCHAR2
									 ,p_directory_name     IN VARCHAR2
									 ,p_source_name        IN VARCHAR2)
IS
    l_filehandle     UTL_FILE.file_type;
	l_filename       VARCHAR2 (240);
	l_base_filename  VARCHAR2 (240);	
	l_err_msg        VARCHAR2 (3000);
    l_err_code       NUMBER;
	l_err_callfrom   VARCHAR2(100) ;
    l_err_callpoint  VARCHAR2(100) ;	
    l_error_message2 CLOB;   
   
    TYPE MarketPrlTyp  IS REF CURSOR;
    v_market_prl    MarketPrlTyp;
    prl_headers     varchar2(10000);
    prl_lines       varchar2(10000);
    v_stmt_str      VARCHAR2(30000);
    v_category_stmt VARCHAR2(1000);
    v_include_spec  VARCHAR2(1000);
    l_count         NUMBER;
      
    cursor cur_prl_sequence is
        select  lookup_code, meaning
        from    fnd_lookup_values
        where   lookup_type = 'XXWC_MARKET_EXTRACT_ORDER'
        and     enabled_flag = 'Y'
        order by to_number (lookup_code);

BEGIN

    l_err_callfrom := 'XXWC_EBS_AGILONE_INTF_PKG.XXWC_MARKET_PRICE_EXTRACT';
    l_err_callpoint:= 'XXWC Market Price Extract';
	
    DBMS_OUTPUT.put_line('Starting XXWC QP Generate Market Price Lists Extract');
    DBMS_OUTPUT.put_line('=====================================================');
    DBMS_OUTPUT.put_line(' ');

	l_filename :=
            'WHITECAP_'
         || p_source_name
         || '_'
         || TO_CHAR (SYSDATE, 'YYYYMMDD')
         || '.csv';
      l_base_filename := l_filename;
	  l_filename := 'TMP_' || l_filename;
	  
	  -- Delete if file already exists
        BEGIN
          UTL_FILE.FREMOVE (p_directory_name,l_base_filename);
          UTL_FILE.FREMOVE (p_directory_name,l_filename);
        EXCEPTION
        WHEN OTHERS THEN
          NULL;
        END;
	  
	  l_filehandle := UTL_FILE.fopen (LOCATION     => p_directory_name,
                                      filename     => l_filename,
                                      open_mode    => 'w',
                                      max_linesize => 32767);

    v_stmt_str := 'select chr(39)||x1.item_number||'||''''||'|'||''''||'||x1.item_description||'||''''||'|'||''''||'||x1.item_category||'||''''||'|'||'''';
    v_stmt_str := v_stmt_str||'||MAX (DECODE (name, '||''''||'MARKET NATIONAL'||''''||', x1.cfp_flag, NULL))';
    
    for c1 in cur_prl_sequence
    loop
        exit when cur_prl_sequence%notfound;
        v_stmt_str := v_stmt_str||'||'||''''||'|'||''''||'||MAX (DECODE (name, '||''''||c1.meaning||''''||', operand, NULL))';
    end loop;
    
    v_category_stmt := null;
    IF P_INVENTORY_CATEGORY IS NOT NULL THEN                
        v_category_stmt := ' and nvl(apps.xxwc_mv_routines_pkg.get_item_category (msib.inventory_item_id, msib.organization_id, '||''''||'Inventory Category'||''''||', 0), '||''''||'X'||''''||') = 
                                nvl(nvl(:p_inventory_category, apps.xxwc_mv_routines_pkg.get_item_category (msib.inventory_item_id, msib.organization_id, '||''''||'Inventory Category'||''''||', 0), '||''''||'X'||''''||')';
    END IF;    
    
    v_include_spec := null;
    IF P_INCLUDE_SPECIALS IS NOT NULL THEN
        IF P_INCLUDE_SPECIALS = 'N' THEN
            v_include_spec := ' and msib.item_type != '||''''||'SPECIAL'||''''; 
        END IF;
    END IF;
    
    v_stmt_str := v_stmt_str||chr(10)||'from (  
	              select  qlh.name
                             , qlh.description
                             , msib.segment1 item_number
                             , qll.operand
                             , REGEXP_REPLACE (msib.description, '||''''||'[^[:alnum:]|[:blank:]|[:punct:]]+'||''''||', null) item_description
                             , xxwc_mv_routines_pkg.get_item_category (msib.inventory_item_id,msib.organization_id,'||''''||'Inventory Category'||''''||',null)  item_category
                             , (case when qlh.list_header_id = apps.fnd_profile.value ('||''''||'XXWC_QP_MARKET_NATIONAL_PRL'||''''||') then qll.attribute1 else null end) cfp_flag
                     from    qp_list_headers qlh
                         , qp_list_lines qll
                         , qp_pricing_attributes qpa
                         , mtl_system_items_b msib
                     where   qlh.list_header_id != apps.fnd_profile.value ('||''''||'XXWC_QP_MARKET_NTL_SETUP_PRL'||''''||')
                     and     qlh.list_type_code = '||''''||'PRL'||''''||'
                     --and     qlh.start_date_active <= trunc(sysdate)--commented on 12/2/2013 Ram Talluri TMS #20131202-00166
                     and     NVL(qlh.start_date_active,TRUNC(SYSDATE-1)) <= trunc(sysdate)--ADDED on 12/2/2013 Ram Talluri TMS #20131202-00166
                     and     nvl(qlh.end_date_active, trunc(sysdate+1)) > trunc(sysdate)
                     and     (qlh.attribute10 = '||''''||'Market Price List'||''''||' or qlh.list_header_id = apps.fnd_profile.value ('||''''||'XXWC_QP_MARKET_NATIONAL_PRL'||''''||'))
                     and     qlh.list_header_id = qll.list_header_id
                     and     qll.list_line_type_code = '||''''||'PLL'||''''||'
                     --and     qll.start_date_active <= trunc(sysdate)--commented on 12/2/2013 Ram Talluri TMS #20131202-00166
                     AND     nvl(qll.start_date_active,TRUNC(SYSDATE-1)) <= trunc(sysdate)--ADDED on 12/2/2013 Ram Talluri TMS #20131202-00166
                     and     nvl(qll.end_date_active, trunc(sysdate+1)) > trunc(sysdate)
                     and     qll.list_header_id = qpa.list_header_id
                     and     qll.qualification_ind = qpa.qualification_ind
                     and     qll.pricing_phase_id = qpa.pricing_phase_id
                     and     qll.list_line_id = qpa.list_line_id
                     and     qpa.product_attribute_context = '||''''||'ITEM'||''''||'
                     and     qpa.product_attribute = '||''''||'PRICING_ATTRIBUTE1'||''''||'
                     and     to_number(qpa.product_attr_value) = msib.inventory_item_id
                     and     msib.organization_id = 222'||v_category_stmt||v_include_spec||'
                  )x1
                 group by x1.item_number, x1.item_description, x1.item_category
                 order by x1.item_number';
                                        
    DBMS_OUTPUT.put_line('Select statement: ');
    DBMS_OUTPUT.put_line(substr(v_stmt_str,1,1000)); 
    
    prl_headers:= null;
    prl_headers := 'Item Number| Item Description| Item Category| CFP Flag';
    for c1 in cur_prl_sequence
    loop
        exit when cur_prl_sequence%notfound;
        prl_headers := prl_headers||'|'||c1.meaning;
    end loop;
    
    prl_headers := prl_headers||'|X';
    
    --write_output(prl_headers);
	UTL_FILE.put_line (l_filehandle, prl_headers);
    
    OPEN v_market_prl FOR v_stmt_str;

    -- Fetch rows from result set one at a time:
    l_count := 0;
    LOOP
      FETCH v_market_prl INTO prl_lines;
      EXIT WHEN v_market_prl%NOTFOUND;
      --write_output(prl_lines||'|X');
	  UTL_FILE.put_line (l_filehandle, prl_lines||'|X');
      l_count := l_count + 1;
    END LOOP;

    -- Close cursor:
    CLOSE v_market_prl;
    
    DBMS_OUTPUT.put_line(' ');
    DBMS_OUTPUT.put_line('=====================================================');
    DBMS_OUTPUT.put_line(' ');
    DBMS_OUTPUT.put_line('Extracted '||l_count||' lines');
    
    commit;
	
	--Closing the file handler
      UTL_FILE.fclose (l_filehandle);
	  
	  utl_file.frename(p_directory_name, l_filename, p_directory_name, l_base_filename);

	  p_retcode := 0;
	  
EXCEPTION
WHEN OTHERS THEN
    l_error_message2 := 'xxwc_market_price_extract'
                || 'Error_Stack...'
                || DBMS_UTILITY.format_error_stack ()
                || ' Error_Backtrace...'
                || DBMS_UTILITY.format_error_backtrace ();

    
    DBMS_OUTPUT.put_line('Error in main xxwc_market_price_extract. Error: '||l_error_message2);
    
    l_err_msg := l_error_message2;	
    l_err_code:= 2;
    p_retcode := 2;
	
      XXCUS_error_pkg.XXCUS_error_main_api (
         p_called_from         => l_err_callfrom
        ,p_calling             => l_err_callpoint
        ,p_request_id          => -1
        ,p_ora_error_msg       => SQLERRM
        ,p_error_desc          => l_err_msg
        ,p_distribution_list   => g_dflt_email
        ,p_module              => 'APPS'); 
		
END xxwc_market_price_extract;

--//============================================================================
--//
--// Object Name         :: xxwc_national_market_price
--//
--// Object Type         :: Procedure
--//
--// Object Description  :: This procedure is used to generate national market 
--//                        extract to agilone
--//
--// Version Control
--//============================================================================
--// Vers    Author           Date          Description
--//----------------------------------------------------------------------------
--// 1.0     Mahesh Kudle     26/05/2014    Initial Build - TMS#20140602-00295
--//============================================================================
PROCEDURE  xxwc_national_market_price(p_errbuf             OUT VARCHAR2
                                     ,p_retcode            OUT NUMBER
                                     ,p_inventory_category IN VARCHAR2
                                     ,p_include_specials   IN VARCHAR2
									 ,p_directory_name     IN VARCHAR2
									 ,p_source_name        IN VARCHAR2)
IS
    l_filehandle     UTL_FILE.file_type;
	l_filename       VARCHAR2 (240);
	l_base_filename  VARCHAR2 (240);	
	l_err_msg        VARCHAR2 (3000);
    l_err_code       NUMBER;
    l_err_callfrom   VARCHAR2(100) ;
    l_err_callpoint  VARCHAR2(100) ;	
    l_error_message2 CLOB;   
   
    TYPE MarketPrlTyp  IS REF CURSOR;
    v_market_prl    MarketPrlTyp;
    prl_headers     varchar2(10000);
    prl_lines       varchar2(10000);
    v_stmt_str      VARCHAR2(30000);
    v_category_stmt VARCHAR2(1000);
    v_include_spec  VARCHAR2(1000);
    l_count         NUMBER;
      
    cursor cur_prl_sequence is
        select  lookup_code, meaning
        from    fnd_lookup_values
        where   lookup_type = 'XXWC_MARKET_EXTRACT_ORDER'
        and     enabled_flag = 'Y'
        order by to_number (lookup_code);

BEGIN
    
	l_err_callfrom := 'XXWC_EBS_AGILONE_INTF_PKG.XXWC_NATIONAL_MARKET_PRICE';
    l_err_callpoint:= 'XXWC National Market Price';
	
    DBMS_OUTPUT.put_line('Starting XXWC QP Generate Market Price Lists Extract');
    DBMS_OUTPUT.put_line('=====================================================');
    DBMS_OUTPUT.put_line(' ');

	l_filename :=
            'WHITECAP_'
         || p_source_name
         || '_'
         || TO_CHAR (SYSDATE, 'YYYYMMDD')
         || '.csv';
      l_base_filename := l_filename;
	  l_filename := 'TMP_' || l_filename;
	  
	  -- Delete if file already exists
        BEGIN
          UTL_FILE.FREMOVE (p_directory_name,l_base_filename);
          UTL_FILE.FREMOVE (p_directory_name,l_filename);
        EXCEPTION
        WHEN OTHERS THEN
          NULL;
        END;
	  
	  l_filehandle := UTL_FILE.fopen (LOCATION     => p_directory_name,
                                      filename     => l_filename,
                                      open_mode    => 'w',
                                      max_linesize => 32767);

    v_stmt_str := 'select x1.item_number||'||''''||'|'||''''||'||x1.item_description||'||''''||'|'||''''||'||x1.item_category||'||''''||'|'||'''';
    v_stmt_str := v_stmt_str||'||MAX (DECODE (name, '||''''||'MARKET NATIONAL'||''''||', x1.cfp_flag, NULL))';
    
    for c1 in cur_prl_sequence
    loop
        exit when cur_prl_sequence%notfound;
        v_stmt_str := v_stmt_str||'||'||''''||'|'||''''||'||MAX (DECODE (name, '||''''||c1.meaning||''''||', operand, NULL))';
    end loop;
    
    v_category_stmt := null;
    IF P_INVENTORY_CATEGORY IS NOT NULL THEN                
        v_category_stmt := ' and nvl(apps.xxwc_mv_routines_pkg.get_item_category (msib.inventory_item_id, msib.organization_id, '||''''||'Inventory Category'||''''||', 0), '||''''||'X'||''''||') = 
                                nvl(nvl(:p_inventory_category, apps.xxwc_mv_routines_pkg.get_item_category (msib.inventory_item_id, msib.organization_id, '||''''||'Inventory Category'||''''||', 0), '||''''||'X'||''''||')';
    END IF;    
    
    v_include_spec := null;
    IF P_INCLUDE_SPECIALS IS NOT NULL THEN
        IF P_INCLUDE_SPECIALS = 'N' THEN
            v_include_spec := ' and msib.item_type != '||''''||'SPECIAL'||''''; 
        END IF;
    END IF;
    
    v_stmt_str := v_stmt_str||chr(10)||'from (  
	              select  qlh.name
                             , qlh.description
                             , msib.segment1 item_number
                             , qll.operand
                             , REGEXP_REPLACE (msib.description, '||''''||'[^[:alnum:]|[:blank:]|[:punct:]]+'||''''||', null) item_description
                             , xxwc_mv_routines_pkg.get_item_category (msib.inventory_item_id,msib.organization_id,'||''''||'Inventory Category'||''''||',null)  item_category
                             , (case when qlh.list_header_id = apps.fnd_profile.value ('||''''||'XXWC_QP_MARKET_NATIONAL_PRL'||''''||') then qll.attribute1 else null end) cfp_flag
                     from    qp_list_headers qlh
                         , qp_list_lines qll
                         , qp_pricing_attributes qpa
                         , mtl_system_items_b msib
                     where   qlh.list_header_id != apps.fnd_profile.value ('||''''||'XXWC_QP_MARKET_NTL_SETUP_PRL'||''''||')
                     and     qlh.list_type_code = '||''''||'PRL'||''''||'
                     --and     qlh.start_date_active <= trunc(sysdate)--commented on 12/2/2013 Ram Talluri TMS #20131202-00166
                     and     NVL(qlh.start_date_active,TRUNC(SYSDATE-1)) <= trunc(sysdate)--ADDED on 12/2/2013 Ram Talluri TMS #20131202-00166
                     and     nvl(qlh.end_date_active, trunc(sysdate+1)) > trunc(sysdate)
                     and     (qlh.attribute10 = '||''''||'Market Price List'||''''||' or qlh.list_header_id = apps.fnd_profile.value ('||''''||'XXWC_QP_MARKET_NATIONAL_PRL'||''''||'))
                     and     qlh.list_header_id = qll.list_header_id
                     and     qll.list_line_type_code = '||''''||'PLL'||''''||'
                     --and     qll.start_date_active <= trunc(sysdate)--commented on 12/2/2013 Ram Talluri TMS #20131202-00166
                     AND     nvl(qll.start_date_active,TRUNC(SYSDATE-1)) <= trunc(sysdate)--ADDED on 12/2/2013 Ram Talluri TMS #20131202-00166
                     and     nvl(qll.end_date_active, trunc(sysdate+1)) > trunc(sysdate)
                     and     qll.list_header_id = qpa.list_header_id
                     and     qll.qualification_ind = qpa.qualification_ind
                     and     qll.pricing_phase_id = qpa.pricing_phase_id
                     and     qll.list_line_id = qpa.list_line_id
                     and     qpa.product_attribute_context = '||''''||'ITEM'||''''||'
                     and     qpa.product_attribute = '||''''||'PRICING_ATTRIBUTE1'||''''||'
                     and     to_number(qpa.product_attr_value) = msib.inventory_item_id
                     and     msib.organization_id = 222'||v_category_stmt||v_include_spec||'
                  )x1
                 group by x1.item_number, x1.item_description, x1.item_category
                 order by x1.item_number';
                                        
    DBMS_OUTPUT.put_line('Select statement: ');
     DBMS_OUTPUT.put_line(substr(v_stmt_str,1,1000)); 
    
    prl_headers:= null;
    prl_headers := 'Item Number| Item Description| Item Category| CFP Flag';
    for c1 in cur_prl_sequence
    loop
        exit when cur_prl_sequence%notfound;
        prl_headers := prl_headers||'|'||c1.meaning;
    end loop;
    
    prl_headers := prl_headers||'|X';
    
    --write_output(prl_headers);
	UTL_FILE.put_line (l_filehandle, prl_headers);
    
    OPEN v_market_prl FOR v_stmt_str;

    -- Fetch rows from result set one at a time:
    l_count := 0;
    LOOP
      FETCH v_market_prl INTO prl_lines;
      EXIT WHEN v_market_prl%NOTFOUND;
      --write_output(prl_lines||'|X');
	  UTL_FILE.put_line (l_filehandle, prl_lines||'|X');
      l_count := l_count + 1;
    END LOOP;

    -- Close cursor:
    CLOSE v_market_prl;
    
    DBMS_OUTPUT.put_line(' ');
    DBMS_OUTPUT.put_line('=====================================================');
    DBMS_OUTPUT.put_line(' ');
    DBMS_OUTPUT.put_line('Extracted '||l_count||' lines');
    
    commit;
	
	--Closing the file handler
      UTL_FILE.fclose (l_filehandle);
	  
	  utl_file.frename(p_directory_name, l_filename, p_directory_name, l_base_filename);

	  p_retcode := 0;
	  
EXCEPTION
WHEN OTHERS THEN
    l_error_message2 := 'xxwc_national_market_price'
                || 'Error_Stack...'
                || DBMS_UTILITY.format_error_stack ()
                || ' Error_Backtrace...'
                || DBMS_UTILITY.format_error_backtrace ();

    
    DBMS_OUTPUT.put_line('Error in main xxwc_national_market_price. Error: '||l_error_message2);
    
    l_err_msg := l_error_message2;	
    l_err_code:= 2;
    p_retcode := 2;
	
      XXCUS_error_pkg.XXCUS_error_main_api (
         p_called_from         => l_err_callfrom
        ,p_calling             => l_err_callpoint
        ,p_request_id          => -1
        ,p_ora_error_msg       => SQLERRM
        ,p_error_desc          => l_err_msg
        ,p_distribution_list   => g_dflt_email
        ,p_module              => 'APPS'); 
		
END xxwc_national_market_price;


--//============================================================================
--//
--// Object Name         :: xxwc_customer_attribute
--//
--// Object Type         :: Procedure
--//
--// Object Description  :: This procedure is used to generate customer attribute 
--//                        extract to agilone
--//
--// Version Control
--//============================================================================
--// Vers    Author                  Date          Description
--//----------------------------------------------------------------------------
--// 1.0     Maharajan Shunmugam     09/23/2015    TMS#20150824-00143 Agile One Supplemental Customer Attribute File
--//============================================================================
PROCEDURE  xxwc_customer_attribute   (p_errbuf             OUT VARCHAR2
                                     ,p_retcode            OUT NUMBER)
IS
   -- Intialize Variables
      l_err_msg               VARCHAR2 (2000);
      l_err_code              NUMBER;
      l_err_callfrom          VARCHAR2(100) ;
      l_err_callpoint         VARCHAR2(100) ;
      
      -- Reference Cursor Variables
      TYPE ref_cur IS REF CURSOR;
      view_cur                 ref_cur;
      view_rec                 xxwc_ebs_agilone_intf_pkg.xxwc_ob_cust_file_rec;

      --File Variables
      l_filehandle             UTL_FILE.file_type;
      l_filename               VARCHAR2 (240);
      l_base_filename          VARCHAR2 (240);
      l_view_name              VARCHAR2 (100) := 'XXXWC_AR_CUST_ATTRIBUTE_V';
      l_org_id                 NUMBER;
      l_org_name               VARCHAR2 (240);
      l_count                  NUMBER;
      l_query                  VARCHAR2 (2000);
      l_directory_name         VARCHAR2(100) := 'XXWC_AGILONE_OB_DIR';      
      l_program_error          EXCEPTION;

   BEGIN

    l_err_callfrom := 'XXWC_EBS_AGILONE_INTF_PKG.XXWC_CUSTOMER_ATTRIBUTE';
    l_err_callpoint:= 'xxwc_customer_attribtue';
	
      -- Building file name
      l_filename :=
            'WHITECAP_CUSTOMER_ATTRB'
         || '_'
         || TO_CHAR (SYSDATE, 'YYYYMMDD')
         || '.csv';
      l_base_filename := l_filename;
      l_filename := 'TMP_' || l_filename;

       -- Delete if file already exists
        BEGIN
          UTL_FILE.fremove (L_directory_name,l_base_filename);
          UTL_FILE.fremove (L_directory_name,l_filename);
        EXCEPTION
        WHEN OTHERS THEN
          NULL;
        END;

      --Open the file handler
      l_filehandle := UTL_FILE.fopen (LOCATION     => l_directory_name,
                                      filename     => l_filename,
                                      open_mode    => 'w',
                                      max_linesize => 32767);

      -- Building out query
      DBMS_OUTPUT.put_line  ('Building Query');

      l_query := NULL;

        l_query := 'SELECT * FROM ' || l_view_name;

     -- write_log ('Section 1 built...');
     DBMS_OUTPUT.put_line  ('l_query ' || l_query);

      OPEN view_cur FOR l_query;

      DBMS_OUTPUT.put_line  ('Writing to the file ... Opening Cursor');
      l_count := 0;

      LOOP
         FETCH view_cur
          INTO view_rec;

         EXIT WHEN view_cur%NOTFOUND;
         UTL_FILE.put_line (l_filehandle, UPPER (view_rec.cust_attr));
         l_count := l_count + 1;
      END LOOP;

      DBMS_OUTPUT.put_line  ('Wrote ' || l_count || ' records to file');
      DBMS_OUTPUT.put_line  ('Closing File Handler');

      --Closing the file handler
      UTL_FILE.fclose (l_filehandle);

      -- 'Rename file for pickup';
      UTL_FILE.frename(l_directory_name, l_filename, l_directory_name, l_base_filename);
	  
	  p_retcode := 0;

   EXCEPTION 
    WHEN l_program_error THEN
          l_err_code := 2;
	  p_retcode  := 2;

      utl_file.fclose(l_filehandle);
      utl_file.fremove(l_directory_name, l_base_filename);
	  
    WHEN OTHERS THEN
          l_err_code := 2;
	  p_retcode  := 2;
      l_err_msg  := ' ERROR ' || SQLCODE ||  substr(SQLERRM, 1, 2000);

      utl_file.fclose(l_filehandle);
      utl_file.fremove(l_directory_name, l_base_filename);

      XXCUS_error_pkg.XXCUS_error_main_api (
         p_called_from         => l_err_callfrom
        ,p_calling             => l_err_callpoint
        ,p_request_id          => -1
        ,p_ora_error_msg       =>  SUBSTR(DBMS_UTILITY.format_error_stack ()|| DBMS_UTILITY.format_error_backtrace (), 1,2000)
        ,p_error_desc          => l_err_msg
        ,p_distribution_list   => g_dflt_email
        ,p_module              => 'XXWC'); 
END xxwc_customer_attribute;
END xxwc_ebs_agilone_intf_pkg;
/