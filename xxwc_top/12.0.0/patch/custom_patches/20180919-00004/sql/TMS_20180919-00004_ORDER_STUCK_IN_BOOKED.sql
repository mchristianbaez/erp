/******************************************************************************
  $Header TMS_20180919-00004_ORDER_STUCK_IN_BOOKED.sql $
  Module Name:Data Fix script for 20180919-00004

  PURPOSE: Data fix script for 20180919-00004 Order stuck in Booked

  REVISIONS:
  Ver        Date         Author                Description
  ---------  -----------  ------------------    -------------------------------
  1.0        19-SEP-2018  Pattabhi Avula        TMS#20180919-00004 - Order not closed SO#26904695

*******************************************************************************/
SET SERVEROUTPUT ON SIZE 1000000

BEGIN
   DBMS_OUTPUT.put_line ('Before Update');

   UPDATE apps.oe_order_headers_all
      SET open_flag = 'N',
          flow_status_Code = 'CLOSED'
    WHERE header_id=67302991;


   DBMS_OUTPUT.put_line ('Records update-' || SQL%ROWCOUNT);
   COMMIT;
EXCEPTION
   WHEN OTHERS
   THEN
      DBMS_OUTPUT.put_line ('Unable to update record ' || SQLERRM);
END;
/