CREATE OR REPLACE PACKAGE BODY APPS."XXCUS_ERROR_PKG" AS
  /********************************************************************************
  FILE NAME: APPS.XXCUS_ERROR_PKG.pkb

  PROGRAM TYPE: PL/SQL Package Body

  PURPOSE: To send the error mail to concern distribution list

  HISTORY
  -- ===============================================================================
  -- VERSION DATE          AUTHOR(S)       DESCRIPTION
   ------- -----------   --------------- -----------------------------------------
   1.0     12/03/2014    Poling Kathy    ESMS257200 Initial version.
   1.1     03/21/2012    Kathy Poling    Change to make the environment a variable
                                         in the body of the email SR 134102, RFC 33208
   1.2     23-JUL-2013   Manny           Text alert
   1.3     14-May-2014   Kathy Poling    SR 250125 fix to remove format from dbms_utility
                                         that is being used to error information in the
                                         packages using regexp to remove the formatting
   1.4     29-May-2014   Kathy Poling    SR 250676 change to loop for sending multiple
                                         email notification when too many records
                                         to be sent.  Also removed the check for the last
                                         email when it loops could stop the records from
                                         being sent.
   1.5     26-AUG-2014   Manny           Code to only send alerts on prd
   1.6     13-May-2015   Pattabhi Avula  TMS20150512-00227 -- Disabled the text alert
                                         email triggering part
   1.7     08/28/2015    Pattabhi Avula  TMS20150728-00070 Split Error Handling API
                                         by Schema based lookup value
   1.8     12/14/2015    Pattabhi Avula  TMS20151214-00040 - Resolve WC error notifications
                                         that are being sent to GSC Distribution list
   1.9     04/08/2016   Balaguru Seshadri TMS 20160407-00172 / ESMS 322629, EBSPRD migration related fixes FOR GSC objects
   1.9     04/08/2016   Balaguru Seshadri TMS 20160407-00172 / ESMS 322629, EBSPRD migration related fixes FOR GSC objects
   2.0     02/07/2017   Niraj K Ranjan    TMS 20170116-00272 / Modify Oracle Applications custom error alert email to avoid use of HTML table
  ********************************************************************************/
  --
  -- Package Variables
  --
  g_package      VARCHAR2(33) := 'XXCUS_ERROR_PKG.';
  g_exceptemail  VARCHAR2(100) := 'HDSOracleDevelopers@hdsupply.com'; -- email used for errors within
  g_excepterror  VARCHAR2(400);
  g_ErrorStatus  NUMBER;
  g_host         VARCHAR2(256) := 'mailoutrelay.hdsupply.net';
  g_hostport     VARCHAR2(20) := '25';
  g_ErrorMessage VARCHAR2(9000);
  FATAL_ERROR EXCEPTION;

  /*Function parse_gtt (v_data1 varchar2, v_mod varchar2)
   return varchar2 IS

     v_test     VARCHAR2(240);
     v_insert1  VARCHAR2(240) DEFAULT NULL;
     v_insert2  VARCHAR2(240) DEFAULT NULL;
     v_module   VARCHAR2(40);
     pad        NUMBER DEFAULT 1;
     v_pos1     NUMBER DEFAULT 2;
     v_pos2     NUMBER DEFAULT 0;
     strlength  NUMBER DEFAULT 0;

   BEGIN
     v_test     := v_data1;
     v_module   := v_mod;

       strlength := instr(v_test,'''' ,1 ,2); -- Length of string
       LOOP
         v_pos2 := instr(v_test,',',1 ,pad) - 1; -- Ending Position
         IF v_pos2 < v_pos1 -- Ending position will = -1
          THEN
           -- when it wraps around.
           v_insert1 := substr(v_test ,v_pos1 ,(strlength - v_pos1));

  --ready to insert into global temp.  need to get values from 1st cursor and parse.  then find the associated records.
   insert into  xxhsi_sorted_errors_gtt
                      (gt_error_id,gt_module,gt_distro_list) Values (pad,v_module,v_insert1);
        ELSE
           v_insert2 := substr(v_test
                              ,v_pos1
                              ,(v_pos2 - v_pos1) + 1);
   insert into  xxhsi_sorted_errors_gtt
                      (gt_error_id,gt_module,gt_distro_list) Values (pad,v_module,v_insert2);
         END IF;
         pad    := pad + 1;
         v_pos1 := v_pos2 + 2;
         EXIT WHEN v_pos2 = -1;
       END LOOP;
       COMMIT;
     COMMIT;
     RETURN v_data1;
   END parse_gtt;

   */

  -- ------------------------ man_arg_error ------------------------------
  --
  -- Description: This procedure is called by business processes which have
  --              identified a mandatory argument which needs to be NOT null.
  --              If the argument is null then need to error.
  --              Varchar2 format.
  --
  PROCEDURE man_arg_error(p_api_name       IN VARCHAR2,
                          p_argument       IN VARCHAR2,
                          p_argument_value IN VARCHAR2) IS

    --
  BEGIN
    --
    IF (p_argument_value IS NULL) THEN
      raise_application_error(-20001,
                              'Mandatory item not received for column : **' ||
                              p_argument || '** for API : ' || p_api_name);
    END IF;
    --
  END man_arg_error;

  -- ------------------------ xxcus_error_main_api ------------------------------
  --
  -- Description: This procedure is called by business processes when errors
  --              are found.  It will write to the table : xxcus_error_tbl
  --              when can then be reported on.
  --
  -- ===============================================================================
  -- VERSION     DATE          AUTHOR(S)       DESCRIPTION
   -------     -----------   --------------- -----------------------------------------
  --   1.8     12/14/2015    Pattabhi Avula  TMS20151214-00040 - Resolve WC error notifications
  --                                         that are being sent to GSC Distribution list
-- ===============================================================================

  PROCEDURE xxcus_error_main_api(p_called_from        IN VARCHAR2,
                                 p_calling            IN VARCHAR2 DEFAULT NULL,
                                 p_request_id         IN VARCHAR2 DEFAULT NULL,
                                 p_ora_error_msg      IN VARCHAR2 DEFAULT NULL,
                                 p_error_desc         IN VARCHAR2 DEFAULT NULL,
                                 p_distribution_list  IN VARCHAR2,
                                 p_distribution_list2 IN VARCHAR2 DEFAULT NULL,
                                 p_distribution_list3 IN VARCHAR2 DEFAULT NULL,
                                 p_distribution_list4 IN VARCHAR2 DEFAULT NULL,
                                 p_distribution_list5 IN VARCHAR2 DEFAULT NULL,
                                 p_created_by         IN VARCHAR2 DEFAULT NULL,
                                 p_last_login         IN VARCHAR2 DEFAULT NULL,
                                 p_module             IN VARCHAR2 DEFAULT NULL,
                                 p_module2            IN VARCHAR2 DEFAULT NULL,
                                 p_module3            IN VARCHAR2 DEFAULT NULL,
                                 p_module4            IN VARCHAR2 DEFAULT NULL,
                                 p_module5            IN VARCHAR2 DEFAULT NULL,
                                 p_argument1          IN VARCHAR2 DEFAULT NULL,
                                 p_argument2          IN VARCHAR2 DEFAULT NULL,
                                 p_argument3          IN VARCHAR2 DEFAULT NULL,
                                 p_argument4          IN VARCHAR2 DEFAULT NULL,
                                 p_argument5          IN VARCHAR2 DEFAULT NULL,
                                 p_argument6          IN VARCHAR2 DEFAULT NULL,
                                 p_argument7          IN VARCHAR2 DEFAULT NULL,
                                 p_argument8          IN VARCHAR2 DEFAULT NULL,
                                 p_argument9          IN VARCHAR2 DEFAULT NULL,
                                 p_argument10         IN VARCHAR2 DEFAULT NULL,
                                 p_argument11         IN VARCHAR2 DEFAULT NULL,
                                 p_argument12         IN VARCHAR2 DEFAULT NULL,
                                 p_argument13         IN VARCHAR2 DEFAULT NULL,
                                 p_argument14         IN VARCHAR2 DEFAULT NULL,
                                 p_argument15         IN VARCHAR2 DEFAULT NULL,
                                 p_argument16         IN VARCHAR2 DEFAULT NULL,
                                 p_argument17         IN VARCHAR2 DEFAULT NULL,
                                 p_argument18         IN VARCHAR2 DEFAULT NULL,
                                 p_argument19         IN VARCHAR2 DEFAULT NULL,
                                 p_argument20         IN VARCHAR2 DEFAULT NULL,
                                 p_argument21         IN VARCHAR2 DEFAULT NULL,
                                 p_argument22         IN VARCHAR2 DEFAULT NULL,
                                 p_argument23         IN VARCHAR2 DEFAULT NULL,
                                 p_argument24         IN VARCHAR2 DEFAULT NULL,
                                 p_argument25         IN VARCHAR2 DEFAULT NULL,
                                 p_argument26         IN VARCHAR2 DEFAULT NULL,
                                 p_argument27         IN VARCHAR2 DEFAULT NULL,
                                 p_argument28         IN VARCHAR2 DEFAULT NULL,
                                 p_argument29         IN VARCHAR2 DEFAULT NULL,
                                 p_argument30         IN VARCHAR2 DEFAULT NULL) IS

    --
    -- Local variables
    --
    l_proc          VARCHAR2(72) := g_package || 'xxhsi_error_main';
    l_sender        VARCHAR2(100);
    l_sid           VARCHAR2(8);
    l_pkgproc       VARCHAR2(100) := 'XXCUS_ERROR_PKG.xxcus_error_main_api';

  -- Var 1.7 starts here --Added by pattabhi on 28-Aug-2015 for TMS20150728-00070

	l_distribution  VARCHAR2(100);

	CURSOR cur_error
    IS
      SELECT lookup_code,
	         description
      FROM   fnd_lookup_values
      WHERE  lookup_type='XXWC_ERROR_EMAIL'
      AND    language     ='US'
      AND    enabled_flag ='Y'
      AND    TRUNC(SYSDATE) BETWEEN start_date_active AND NVL(end_date_active, TRUNC(SYSDATE) + 1);

  -- Var 1.7 Ends here --Added by pattabhi on 28-Aug-2015 for TMS20150728-00070

  BEGIN
    --
    -- Local variables
    --
    SELECT lower(NAME) INTO l_sid FROM v$database;
    l_sender := 'Oracle.Applications_' || l_sid || '@hdsupply.com';
    -- Ensure required fields are passed.

    --
    XXCUS_ERROR_PKG.man_arg_error(p_api_name       => l_proc,
                                  p_argument       => 'Called_From',
                                  p_argument_value => p_called_from);


  -- Var 1.7 starts here --Added by pattabhi on 28-Aug-2015 for TMS20150728-00070
  -- --------------------------------------------------------------------------------
  -- Comparing p_called_from value from parameter and fetching the distribution
  -- Maild from the lookup
  -- --------------------------------------------------------------------------------
   FOR e_rec IN cur_error LOOP

   -- IF p_called_from LIKE e_rec.lookup_code||'%' THEN  -- Commented for Ver 1.8
    IF UPPER(p_called_from) LIKE e_rec.lookup_code||'%' THEN  -- Added for Ver 1.8
       l_distribution := e_rec.description;
    END IF;

   END LOOP;

     IF l_distribution IS NULL THEN
        SELECT description
        INTO   l_distribution
        FROM   fnd_lookup_values
        WHERE  lookup_type='XXWC_ERROR_EMAIL'
        AND    language     ='US'
        AND    enabled_flag ='Y'
        AND    lookup_code='DEFAULT'
     	AND    TRUNC(SYSDATE) BETWEEN start_date_active AND NVL(end_date_active, TRUNC(SYSDATE) + 1);
     END IF;

   --  p_distribution_list:=l_distribution;


-- Var 1.7 Ends here --Added by pattabhi on 28-Aug-2015 for TMS20150728-00070

    XXCUS_ERROR_PKG.man_arg_error(p_api_name       => l_proc,
                                  p_argument       => 'Distribution_list',
                                  p_argument_value => l_distribution);  -- Ver 1.7

    BEGIN
      -- Insert into table xxcus_errors_tbl
      INSERT INTO xxcus.xxcus_errors_tbl
        (called_from,
         CALLING,
         request_id,
         ora_error_msg,
         error_desc,
         distribution_list1,
         distribution_list2,
         distribution_list3,
         distribution_list4,
         distribution_list5,
         created_by,
         last_login,
         module1,
         module2,
         module3,
         module4,
         module5,
         ARGUMENT1,
         ARGUMENT2,
         ARGUMENT3,
         ARGUMENT4,
         ARGUMENT5,
         ARGUMENT6,
         ARGUMENT7,
         ARGUMENT8,
         ARGUMENT9,
         ARGUMENT10,
         ARGUMENT11,
         ARGUMENT12,
         ARGUMENT13,
         ARGUMENT14,
         ARGUMENT15,
         ARGUMENT16,
         ARGUMENT17,
         ARGUMENT18,
         ARGUMENT19,
         ARGUMENT20,
         ARGUMENT21,
         ARGUMENT22,
         ARGUMENT23,
         ARGUMENT24,
         ARGUMENT25,
         ARGUMENT26,
         ARGUMENT27,
         ARGUMENT28,
         ARGUMENT29,
         ARGUMENT30)
      VALUES
        (substr(p_called_from, 1, 75),
         substr(p_calling, 1, 75),
         p_request_id,
         p_ora_error_msg,
         p_error_desc,
         upper(l_distribution),  -- Ver1.7
         upper(p_distribution_list2),
         upper(p_distribution_list3),
         upper(p_distribution_list4),
         upper(p_distribution_list5),
         p_created_by,
         p_last_login,
         substr(p_module, 1, 24),
         substr(p_module2, 1, 24),
         substr(p_module3, 1, 24),
         substr(p_module4, 1, 24),
         substr(p_module5, 1, 24),
         P_ARGUMENT1,
         P_ARGUMENT2,
         P_ARGUMENT3,
         P_ARGUMENT4,
         P_ARGUMENT5,
         P_ARGUMENT6,
         P_ARGUMENT7,
         P_ARGUMENT8,
         P_ARGUMENT9,
         P_ARGUMENT10,
         P_ARGUMENT11,
         P_ARGUMENT12,
         P_ARGUMENT13,
         P_ARGUMENT14,
         P_ARGUMENT15,
         P_ARGUMENT16,
         P_ARGUMENT17,
         P_ARGUMENT18,
         P_ARGUMENT19,
         P_ARGUMENT20,
         P_ARGUMENT21,
         P_ARGUMENT22,
         P_ARGUMENT23,
         P_ARGUMENT24,
         P_ARGUMENT25,
         P_ARGUMENT26,
         P_ARGUMENT27,
         P_ARGUMENT28,
         P_ARGUMENT29,
         P_ARGUMENT30);

      g_excepterror := 'Error inserting into xxhsi_error_tbl' || chr(10) ||
                       SQLERRM;

    EXCEPTION
      WHEN OTHERS THEN
        raise_application_error(-20001,
                                'Error inserting into error table.');


        xxcus_misc_pkg.html_email(p_to            => g_exceptemail,
                                  p_from          => l_sender,
                                  p_text          => 'test',
                                  p_subject       => 'Fatal Error in error Table package :' ||
                                                     l_pkgproc,
                                  p_html          => g_excepterror,
                                  p_smtp_hostname => g_host,
                                  p_smtp_portnum  => g_hostport);
        raise_application_error(-20001, 'Error reporting the error table.');
    END xxhsi_error_main;

  END;

  /* ------------------------ email_errors ------------------------------
  --
  -- Description: This procedure is called by the application to process
  --              the errors in the table xxcus_errors_tbl.
  --              It should only process errors that are awaiting pickup
  --              by the email flag ='N'.
  --              It should 1st get a list of values of Distribution emails
  --              to be emailed.  Then it should get a list of Called from
  --              processes for these individual distribution groups.
  --
  --
  --
  --HISTORY
  -- ===============================================================================
  -- VERSION DATE          AUTHOR(S)       DESCRIPTION
   ------- -----------   --------------- -----------------------------------------
   1.1     03/21/2012    Kathy Poling    Change to make the environment a variable
                                         in the body of the email SR 134102, RFC 33208
   1.2     23-JUL-2013   Manny           Text alert
   1.3     14-May-2014   Kathy Poling    SR 250125 fix to remove format from dbms_utility
                                         that is being used to error information in the
                                         packages using regexp to remove the formatting
   1.4     29-May-2014   Kathy Poling    SR 250676 change to loop for sending multiple
                                         email notification when too many records
                                         to be sent.  Also removed the check for the last
                                         email when it loops could stop the records from
                                         being sent.
   1.5     26-AUG-2014   Manny           Code to only send alerts on prd
   1.6     13-May-2015   Pattabhi Avula  TMS20150512-00227 -- Disabled the text alert
                                         email triggering part
   2.0     02/07/2017   Niraj K Ranjan    TMS 20170116-00272 / Modify Oracle Applications custom error alert email to avoid use of HTML table
  ********************************************************************************/

  PROCEDURE email_errors(errbuf OUT VARCHAR2, retcode OUT NUMBER) IS

    --Intialize Variables

    l_dflt_email   fnd_user.email_address%TYPE := 'HDSOracleDevelopers@hdsupply.com';
    l_pkgproc      VARCHAR2(100) := 'XXCUS_ERROR_PKG.email_errors';
    l_sender       VARCHAR2(100);
    l_sid          VARCHAR2(8);
    l_body         VARCHAR2(32767);
    l_body_header  VARCHAR2(32767);
    l_body_detail  VARCHAR2(32767);
    l_body_footer  VARCHAR2(32767);
    l_sql_hint     VARCHAR2(32767);
    l_error_hint   VARCHAR2(1000);
    l_newline      NUMBER(20) := 1;
    l_mins_elapsed number(20);
    l_mins_allowed number(20) := 0;
    l_max_date     DATE;
    l_count        NUMBER; --Version 1.4
    l_record_cnt   NUMBER := 0; --Version 1.4
    l_err_msg      CLOB;   --Version 1.4
    
  BEGIN

    --
    -- Local variables
    --
    SELECT lower(NAME) INTO l_sid FROM v$database;
    l_sender := 'Oracle.Applications_' || l_sid || '@hdsupply.com';

 --IF l_sid='ebizprd' THEN  --1.5 --1.9
IF l_sid = 'ebsprd' THEN  --1.9

     --Version 1.4
    --Get lookups for number of records
    l_err_msg := 'Get lookup for record count for looping';
    BEGIN
      SELECT to_number(description)
        INTO l_record_cnt
        FROM fnd_lookup_values_vl
       WHERE lookup_type = 'XXCUS_MISC_LOOKUPS'
         AND lookup_code = 'ERROR_API_1'
         AND enabled_flag = 'Y'
         AND nvl(end_date_active, SYSDATE) >= SYSDATE;
    EXCEPTION
      WHEN no_data_found THEN
        l_err_msg := l_err_msg || ' : ' || SQLERRM;
        fnd_file.put_line(fnd_file.log, l_err_msg);
        RAISE fatal_error;
    END;

       fnd_file.put_line(fnd_file.log
                         ,'Record count = '|| l_record_cnt );

  --Version 1.4 added loop to get all reords e
   For c_distro_list in (SELECT ert1.distribution_list1, ert1.module1
                            FROM xxcus.xxcus_errors_tbl ert1
                           WHERE ert1.email_flag1 = 'N'
                           GROUP BY ert1.distribution_list1, ert1.module1)

     LOOP

    --Get list of Distribution group to email

   l_count := 0;

    For c_distro in (SELECT ert1.distribution_list1, ert1.module1
                            FROM xxcus.xxcus_errors_tbl ert1
                           WHERE ert1.email_flag1 = 'N'
                           and ert1.distribution_list1 = c_distro_list.distribution_list1
                           and ert1.module1 = c_distro_list.module1
                           GROUP BY ert1.distribution_list1, ert1.module1)

     LOOP
      l_dflt_email := c_distro_list.distribution_list1;
     --Change start for Ver 2.0 TMS#20170116-00272
      l_body_header := 'Hd Supply'||'<BR>'||
	                   'Error Report For '||upper(l_sid)||'<BR>'||'<BR>';

/*l_body_header := '<style type="text/css">
.style1 {
	border-style: solid;
	border-width: 1px;
}
.style2 {
	border: 1px solid #000000;
}
.style3 {
	border: 1px solid #000000;
	background-color: #FFFF00;
}
.style4 {
	color: #FFFF00;
	border: 1px solid #000000;
	background-color: #000000;
}
.style5 {
	font-size: large;
}
.style6 {
	font-size: xx-large;
}
.style7 {
	color: #FFCC00;
}
</style>
<p><span class="style6"><span class="style7"><strong>HD SUPPLY</strong></span><br />
<span class="style5">0' ||upper(l_sid) ||' </span></p>
<BR><table border="2" cellpadding="2" cellspacing="2" width="100%">' ||
                       '<td class="style2"><B>CALLED FROM</B></td>
                        <td class="style2"><B>CALLING</B></td>'||'
                        <td class="style2"><B>ORA ERROR MSG</B></td>
                        <td class="style2"><B>REQUEST ID</B></td><tr>'; */
--Change End for Ver 2.0 TMS#20170116-00272
      --Calculate if the Distribution group time has elapsed from xxhsi_errors_email table
      --SEC 2.8
      --Version 1.4
/*     BEGIN
        SELECT nvl(MAX(tbl1.EMAIL_DIST_DATE1), SYSDATE - 1)
          INTO l_max_date
          FROM xxcus.xxcus_errors_tbl tbl1
         WHERE tbl1.distribution_list1 = c_distro_list.distribution_list1
           and tbl1.email_flag1 = 'Y';
       -- DBMS_OUTPUT.put_line('l_max_date : ' || l_max_date || chr(10) || 'distrolist ' || c_distro_list.distribution_list1); --debug.

      EXCEPTION
        when NO_DATA_FOUND THEN
          l_max_date := sysdate - 1;
        when OTHERS then
          g_excepterror := 'Sec 2.8 : Error selecting the max date for the calculator' ||
                           chr(10) || SQLERRM;
          raise FATAL_ERROR;
      END;
*/

      --  List of processes by distro group.
      FOR c_called_from in (SELECT DISTINCT (ert.called_from)
                              FROM xxcus.xxcus_errors_tbl ert
                             WHERE ert.email_flag1 = 'N'
                               AND ert.distribution_list1 =
                                   c_distro_list.distribution_list1) LOOP


--V1.2   TEXT alert.  Adding here to get called from , but will only submit the first one found.
--       Will utilize the newline counter to ensure that.
--V1.6 -- Code changes starts here : TMS20150512-00227 -- Disabled the text alert email triggering part
/*IF hdsoracle.HDS_HDSORACLE_MISC_PKG.alert_oncall@APXPRD_LNK.HSI.HUGHESSUPPLY.COM  ='T' AND l_newline=1  then

        xxcus_misc_pkg.html_email(p_to      =>  hdsoracle.HDS_HDSORACLE_MISC_PKG.get_oncallname@APXPRD_LNK.HSI.HUGHESSUPPLY.COM ,
                                  p_from    => l_sender,
                                  p_text    => 'test',
                                  p_subject => '***ERRORS ALERT***',
                                  p_html          => c_called_from.called_from,
                                  p_smtp_hostname => g_host,
                                  p_smtp_portnum  => g_hostport);

END IF; */

--V1.6 -- Code changes Ends here : TMS20150512-00227 -- Disabled the text alert email triggering part
        -- Cursor to gather details for errors.
        For c_errors_dist in (SELECT rownum, ert.*
                                FROM xxcus.xxcus_errors_tbl ert
                               WHERE ert.email_flag1 = 'N'
                                 AND ert.distribution_list1 =
                                     c_distro_list.distribution_list1
                                 AND ert.called_from =
                                     c_called_from.called_from
                               order by error_id) LOOP
          --Get list of Error table info for the distribution group.

          l_count := l_count + 1;


          --Change start for Ver 2.0 TMS#20170116-00272
          /*l_body_detail := l_body_detail || '<td class="style3">' ||
                           c_errors_dist.called_from ||  '</td><td class="style3">' ||
                           c_errors_dist.calling ||      '</td><td class="style3">' ||
                           regexp_replace(c_errors_dist.ora_error_msg, '[[:cntrl:]]', NULL) ||'</td><td class="style3">' ||    --Version 1.3
                           c_errors_dist.request_id || '</td><TR>';*/

          l_body_detail := l_body_detail||
		                   'Called From: '||c_errors_dist.called_from||'<BR>'||
						   'Calling: '||c_errors_dist.calling||'<BR>'||
						   'ORA Error Msg: '||regexp_replace(c_errors_dist.ora_error_msg, '[[:cntrl:]]', NULL)||'<BR>'||
						   'Request ID: '||c_errors_dist.request_id||'<BR>'||'<BR>';
		  --regexp_replace(c_errors_dist.ora_error_msg, '[[:cntrl:]]', NULL)
		  --Change End for Ver 2.0 TMS#20170116-00272
          l_error_hint  := l_error_hint || ',' || c_errors_dist.error_id;
          l_newline     := l_newline + 1;

           fnd_file.put_line (fnd_file.LOG ,l_body_detail);

          --Flip the email flag if allowed by the IF statement  (need to incorporate into error_id)
          --SEC 3.0
          BEGIN
            update xxcus.xxcus_errors_tbl ert3
               set ert3.email_flag1 = 'Y', ert3.EMAIL_DIST_DATE1 = sysdate
             where ert3.error_id = c_errors_dist.error_id;
          EXCEPTION
            when others then
              g_excepterror := 'Sec 3.0 : Error updating the email flags' ||
                               chr(10) || SQLERRM;
              raise FATAL_ERROR;
          END;

          IF l_newline > 10 THEN
            l_error_hint := l_error_hint || chr(10);
            l_newline    := 0;
          END IF;
          --Version 1.4
          EXIT WHEN l_count > l_record_cnt;

        END LOOP;
        --DBMS_OUTPUT.put_line('l_count: ' || l_count );
        l_body_detail := l_body_detail || chr(10);
      END LOOP;

      --SEC 3.3
      --Version 1.4 removing the time checking
/*      BEGIN
        SELECT trunc((SYSDATE - l_max_date) * 1440, 0)
          into l_mins_elapsed
          FROM dual;
        --DBMS_OUTPUT.put_line('l_mins_elapsed : ' || l_mins_elapsed); --debug (remove upon code review).

      EXCEPTION
        when others then
          g_excepterror := 'Sec 3.3 : Error getting sysdate from dual' ||
                           chr(10) || SQLERRM;
          raise FATAL_ERROR;
      END;

      --SEC3.4
      -- v1.1
     BEGIN
        SELECT erm.email_timer
          into l_mins_allowed
          FROM xxcus.xxcus_errors_email erm
         where upper(erm.email_group) = c_distro_list.distribution_list1
           and nvl(erm.email_module, 'DFLT') =
               nvl(c_distro_list.module1, 'DFLT');
        --DBMS_OUTPUT.put_line('l_mins_allowed : ' || l_mins_allowed); --debug.

      EXCEPTION
        -- Distribution account not setup yet, so set it up.  Except of course when no distro group is used.
        when NO_DATA_FOUND THEN

          BEGIN
            insert into xxcus.xxcus_errors_email erm1
            values
              (UPPER(c_distro_list.distribution_list1),  --v1.1
               nvl(c_distro_list.module1, 'DFLT'),
               30);
          EXCEPTION
            WHEN OTHERS THEN
              g_excepterror := 'Sec 3.4 : Error inserting default time into email table' ||
                               chr(10) || SQLERRM;
              raise FATAL_ERROR;
          END;

          l_mins_allowed := 30;

        when others then
          g_excepterror := 'Sec 3.4 : Error getting time from xxcus.xxcus_errors_email table' ||
                           chr(10) || SQLERRM;
          raise FATAL_ERROR;
      END;
*/
      l_error_hint := rtrim(l_error_hint, ',');
      l_error_hint := '(' || ltrim(l_error_hint, ',') || ')';
	  
	  --Change start for Ver 2.0 TMS#20170116-00272
      l_sql_hint   :=  '<BR>SELECT  ert.*<BR>' ||
                      'FROM    xxcus.xxcus_errors_tbl ert<BR>' ||
                      'WHERE   ert.error_id in <BR>' || l_error_hint ||
                      '<BR>ORDER BY ERT.ERROR_ID DESC' || '';
      /*l_sql_hint := 'SELECT  ert.*'||chr(10)||
	                'FROM    xxcus.xxcus_errors_tbl ert'||chr(10)||
					'WHERE   ert.error_id in '||chr(10)||
					l_error_hint||chr(10)||
					'ORDER BY ERT.ERROR_ID DESC';*/
      l_body_footer :=  'Use the following SQL statment to help debug further.<BR>' ||
                       l_sql_hint;
     -- l_body_footer := 'Use the following SQL statment to help debug further.'||chr(10)||chr(10)||l_sql_hint;
      l_body        := l_body_header||
	                   l_body_detail||
					   l_body_footer;
      --Change End for Ver 2.0 TMS#20170116-00272
      -- Send email if time elapsed is allowed.
      --IF l_mins_allowed < l_mins_elapsed then

        xxcus_misc_pkg.html_email(p_to      => l_dflt_email,
                                  p_from    => l_sender,
                                  p_text    => 'test',
                                  p_subject => '***ERRORS ALERT****ERRORS ALERT***',
                                  --p_subject       => l_subject,
                                  p_html          => l_body,
                                  p_smtp_hostname => g_host,
                                  p_smtp_portnum  => g_hostport);

        COMMIT;
      --ELSE
       -- ROLLBACK;
      --END IF;

      --reset for other distribution group
      l_body        := NULL;
      l_body_header := NULL;
      l_body_detail := NULL;
      l_body_footer := NULL;
      l_sql_hint    := NULL;
      l_error_hint  := NULL;
      l_newline     := NULL;

      --EXIT WHEN l_count > l_record_cnt;

    END LOOP;
   -- DBMS_OUTPUT.put_line('l_count: ' || l_count );
    END LOOP;
    COMMIT;


END IF;

  EXCEPTION
    WHEN fatal_error THEN
      ROLLBACK;

      xxcus_misc_pkg.html_email(p_to      => l_dflt_email,
                                p_from    => l_sender,
                                p_text    => 'test',
                                p_subject => '***ERRORS ALERT****ERRORS ALERT***',
                                --p_subject       => l_subject,
                                p_html          => l_body,
                                p_smtp_hostname => g_host,
                                p_smtp_portnum  => g_hostport);
      raise_application_error(-20001, 'Error reporting the error table.');

  END email_errors;
END XXCUS_ERROR_PKG;
/
