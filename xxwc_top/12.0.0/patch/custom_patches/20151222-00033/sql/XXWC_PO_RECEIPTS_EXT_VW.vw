/* 
-- ************************************************************************
-- $Header XXWC_PO_RECEIPTS_EXT_VW $
-- Module Name: XXWC 

-- REVISIONS:
-- Ver        Date        Author             Description
-- ---------  ----------  ----------         ----------------   
-- 1.0       8/31/2012    Consuela Gonzalez     Initial Version. 
-- 1.1       1/6/2015     Neha Saini             TMS#20151222-00033 added check for special characters                                   
-- **************************************************************************/
CREATE OR REPLACE VIEW APPS.XXWC_PO_RECEIPTS_EXT_VW AS
    SELECT operating_unit_id
          ,receipt_date interface_date
          , (   TRIM (business_unit)
             || '|'
             || TRIM (source_system)
             || '|'
             || TRIM (ship_from_vendor_num)
             || '|'
             || TRIM (vendor_address_name)
             || '|'
             || TRIM (vendor_address_ln1)
             || '|'
             || TRIM (vendor_address_ln2)
             || '|'
             || TRIM (vendor_city)
             || '|'
             || TRIM (vendor_state_province)
             || '|'
             || TRIM (vendor_postal_code)
             || '|'
             || TRIM (vendor_fax)
             || '|'
             || TRIM (vendor_country)
             || '|'
             || TRIM (vendor_telephone)
             || '|'
             || TRIM (drop_ship_flag)
             || '|'
             || TRIM (po_number)
             || '|'
             || TO_CHAR (po_create_date, 'MM/DD/YYYY')
             || '|'
             || TO_CHAR (receipt_date, 'MM/DD/YYYY')
             || '|'
             || TRIM (bill_to_branch_number)
             || '|'
             || TRIM (bill_to_branch_name)
             || '|'
             || TRIM (bill_to_address1)
             || '|'
             || TRIM ( (bill_to_address2 || DECODE (bill_to_address3, NULL, NULL, ' ') || bill_to_address3))
             || '|'
             || TRIM (bill_to_city)
             || '|'
             || TRIM (bill_to_state_province)
             || '|'
             || TRIM (bill_to_postal_code)
             || '|'
             || TRIM (bill_to_country)
             || '|'
             || TRIM (ship_to_branch_number)
             || '|'
             || TRIM (ship_to_branch_name)
             || '|'
             || TRIM (ship_to_address1)
             || '|'
             || TRIM ( SUBSTR((ship_to_address2 || DECODE (ship_to_address3, NULL, NULL, ' ') || ship_to_address3),1,50)) --Added 09/30/13 : SUBSTR :TMS# 20130930-00279
             || '|'
             || TRIM (ship_to_city)
             || '|'
             || TRIM (ship_to_state_province)
             || '|'
             || TRIM (ship_to_postal_code)
             || '|'
             || TRIM (ship_to_country)
             || '|'
             || TRIM (sku_number)
             || '|'
           || TRIM(REGEXP_REPLACE (vendor_part_number
             ,'[^' || CHR(32) || '-' || CHR(127) || ']',--Addded by Neha for TMS#20151222-00033
             ' '))
             || '|'
             || TRIM (REGEXP_REPLACE (xxwc_helpers.remove_special_chars (item_description),
                                 '[' ||  CHR (124) || ']', 
                                 '')) --Addded by Neha for TMS#20151222-00033
             || '|'
             || ordered_qty
             || '|'
             || received_qty
             || '|'
             || invoice_cost
             || '|'
             || TRIM (purchase_uom)
             || '|'
             || conv_fact_to_each
             || '|'
             || TRIM (invoice_num)
             || '|'
             || unique_po_identifier
             || '|'
             || TRIM (last_update_date))
               po_receipt_data
      FROM xxwc_po_receipts_vw;
