CREATE OR REPLACE PACKAGE BODY APPS.XXWC_CUSTOMER_EXTRACT_PKG
AS
/**************************************************************************
 *
 * HEADER  
 *   Organization / Role / Member Group Interface to Commerce
 *
 * PROGRAM NAME
 *  XXWC_CUSTOMER_EXTRACT_PKG.pkb
 * 
 * DESCRIPTION
 *  this package contains all of the procedures/functions used to generate organization, organization role 
 *    and organization member group extract files for the Commerce interface
 *
 * HISTORY
 * =======
 *
 * VERSION DATE        AUTHOR(S)       DESCRIPTION
 * ------- ----------- --------------- ------------------------------------
 * 1.00    10-OCT-2012 Scott Spivey    Lucidity Consulting Group - Creation
 * 1.10    22-OCT-2012 Scott Spivey    removed address book data file
 *                                     added organization role data file
 * 1.20    29-OCT-2012 Scott Spivey    added organization member group data file
 * 1.30    14-DEC-2012 Scott Spivey    split full extract logic and delta extract logic to simplfy queries
 *                                     handle scenario when inactivating primary bill-to from site level
 * 1.40    16-JAN-2013 Scott Spivey    correct logic for start/end active date range validation on price lists
 *                                     correct source column of price list name
 * 1.50     6-FEB-2013 Scott Spivey    limit size of output fields based upon WCS field sizes
 *                                     utilize credit profile information at both customer level and site level
 *                                     change logic for cash_only to allow_on_account, previously logic was backwards for WCS
 * 1.60    13-FEB-2013 Scott Spivey    eliminate the records for Buyer Organization and Whitecap Buyer Organization in full extract per WCS
 *                                     set nickname to be orgentityname for organization units
 * 1.70    01-DEC-2013 Gopi Damuluri   TMS# 20131121-00230
 *                                     SpeedBuild Changes: CSP Information is now sent for all the Customers who have CSP setup.
 *                                     orgEntityField3 - is added to the file. It carries CatalogFilter Name
 * 1.71    06-FEB-2014 Gopi Damuluri   TMS# 20140206-00067
 *                                     SpeedBuild Changes: To differentiate the Delta extract between SpeedBuild and Non-SpeedBuild customers.
 * 1.72    26-FEB-2014 Gopi Damuluri   TMS# 20140211-00049 Changes to include Catalog Filter changes
 * 1.73    30-APR-2014 Gopi Damuluri   TMS# 20140502-00193 Changes with Customer Contacts
 * 1.74    27-MAY-2014 Gopi Damuluri   TMS# 20140529-00334 Catalog Filter at Master Party Level
 * 1.75    14-JAN-2015 Gopi Damuluri   TMS# 20150114-00119 Add Trace to EComm Customer Extract Process
 * 1.76    01-FEB-2015 Gopi Damuluri   TMS# 20141212-00194 Tuning of EComm Organization/Customer Extract
 . 1.77    28-May-2015 Pattabhi Avula  TMS# 20150521-00030 Commented trace scripts to EComm Customer Extract Process
 * 1.77    18-JAN-2017 Pattabhi Avula  TMS#20160915-00144 - Implement Automated Account Linking through File 
 *                                     Upload Process
 *************************************************************************/
   xxwc_error                  EXCEPTION;

   l_directory                 VARCHAR2 (80) := 'XXWC_WCS_CUSTOMER_EXTRACTS_DIR';

   l_org_outfile               UTL_FILE.file_type;
   l_org_tmp_file_name         VARCHAR2 (80) := 'tmp_wc_organization.csv';
   l_org_file_name             VARCHAR2 (80) := 'wc_organization.csv';

   l_org_role_outfile          UTL_FILE.file_type;
   l_org_role_tmp_file_name    VARCHAR2 (80) := 'tmp_wc_organizationrole.csv';
   l_org_role_file_name        VARCHAR2 (80) := 'wc_organizationrole.csv';

   l_org_group_outfile         UTL_FILE.file_type;
   l_org_group_tmp_file_name   VARCHAR2 (80)
                                  := 'tmp_wc_organizationmembergroup.csv';
   l_org_group_file_name       VARCHAR2 (80)
                                  := 'wc_organizationmembergroup.csv';

/********************************************************************************
ProcedureName : extract_wrapper
Purpose       : Wrapper API to generate Delta or Full Organization Extract
HISTORY
===============================================================================
VERSION DATE          AUTHOR(S)       DESCRIPTION
------- -----------   --------------- -----------------------------------------

********************************************************************************/
   PROCEDURE extract_wrapper (
      errbuf                   OUT VARCHAR2
     ,retcode                  OUT VARCHAR2
     ,p_extract_method      IN     VARCHAR2 DEFAULT 'DELTA'
     ,p_last_extract_date   IN     VARCHAR2)
   AS
      l_request_id   NUMBER;
      l_phase        VARCHAR2 (80);
      l_status       VARCHAR2 (80);
      l_dev_phase    VARCHAR2 (80);
      l_dev_status   VARCHAR2 (80);
      l_message      VARCHAR2 (4000);
      l_success      BOOLEAN;

      l_user_id      NUMBER;
      l_resp_id      NUMBER;
      l_appl_id      NUMBER := 222;                     -- Receivables
      l_ou_id        NUMBER := 162;                     -- HDS White Cap - Org
   BEGIN
      errbuf := 'Success';
      retcode := '0';

      IF NVL (p_extract_method, 'DELTA') NOT IN ('FULL', 'DELTA')
      THEN
         DBMS_OUTPUT.put_line (
            'P_EXTRACT_METHOD has an invalid value : ' || p_extract_method);
         RAISE xxwc_error;
      ELSE
         DBMS_OUTPUT.put_line ('P_EXTRACT_METHOD = ' || p_extract_method);
         DBMS_OUTPUT.put_line (
            'P_LAST_EXTRACT_DATE = ' || p_last_extract_date);
      END IF;

      BEGIN
         SELECT user_id
           INTO l_user_id
           FROM fnd_user
          WHERE user_name = 'XXWC_INT_SALESFULFILLMENT';
      EXCEPTION
         WHEN OTHERS
         THEN
            l_user_id := 3;
      END;

      BEGIN
         SELECT responsibility_id
           INTO l_resp_id
           FROM fnd_responsibility_vl
          WHERE     responsibility_key = 'XXWC_CUSTOMER_MAINTENANCE'
                AND application_id = l_appl_id;
      EXCEPTION
         WHEN OTHERS
         THEN
            l_resp_id := 50992;    -- XXWC_CUSTOMER_MAINTENANCE responsibility
      END;

      BEGIN
         SELECT organization_id
           INTO l_ou_id
           FROM hr_operating_units
          WHERE short_code = 'HDSWCORG';
      EXCEPTION
         WHEN OTHERS
         THEN
            l_ou_id := 162;                             -- HDS White Cap - Org
      END;

      -- set Oracle Applications environment
      apps.fnd_global.apps_initialize (user_id        => l_user_id
                                      ,resp_id        => l_resp_id
                                      ,resp_appl_id   => l_appl_id);

      -- 161 = HDS White Cap operating unit
      apps.mo_global.set_policy_context ('S', l_ou_id);
      apps.mo_global.init ('AR');

      -- don't show concurrent request in user's queue unless status=WARNING or ERROR
      --  l_success := FND_REQUEST.Set_Options(implicit  => 'WARNING');
      --  FND_REQUEST.Set_Org_Id(org_id IN number default NULL);

      l_request_id :=
         fnd_request.submit_request (
            application   => 'XXWC'
           ,program       => 'XXWC_CUSTOMER_EXTRACT'
           ,description   => NULL
           ,start_time    => NULL
           ,sub_request   => FALSE
           ,argument1     => NVL (p_extract_method, 'DELTA')
           ,argument2     => p_last_extract_date);

      COMMIT;

      IF NVL (l_request_id, 0) = 0
      THEN
         -- submission failed
         DBMS_OUTPUT.put_line ('Unable to submit concurrent program');
         RAISE xxwc_error;
      ELSE
         DBMS_OUTPUT.put_line ('Concurrent Request = ' || l_request_id);
      END IF;


      IF fnd_concurrent.wait_for_request (request_id   => l_request_id
                                         ,interval     => 60
                                         ,max_wait     => 0
                                         ,phase        => l_phase
                                         ,status       => l_status
                                         ,dev_phase    => l_dev_phase
                                         ,dev_status   => l_dev_status
                                         ,MESSAGE      => l_message)
      THEN
         DBMS_OUTPUT.put_line (
            'Phase = ' || l_phase || ' and status = ' || l_status);
         DBMS_OUTPUT.put_line ('Request Completion Message = ' || l_message);

         -- return completion information
         IF l_dev_phase = 'COMPLETE'
         THEN
            CASE l_dev_status
               WHEN 'NORMAL'
               THEN
                  errbuf := l_status;
                  retcode := '0';
               WHEN 'ERROR'
               THEN
                  errbuf := l_status;
                  retcode := '2';
               WHEN 'WARNING'
               THEN
                  errbuf := l_status;
                  retcode := '1';
               ELSE
                  errbuf := l_status;
                  retcode := '2';
            END CASE;
         ELSE
            DBMS_OUTPUT.put_line (
               'Request Has Not Completed prior to wait process max_wait');
            DBMS_OUTPUT.put_line (
                  'DEV Phase = '
               || l_dev_phase
               || ' and DEV status = '
               || l_dev_status);
            errbuf := l_status;
            retcode := '1';
         END IF;
      ELSE
         DBMS_OUTPUT.put_line (
            'Unable to determine completion status of concurrent program');
         errbuf := 'Unable to determine completion status';
         retcode := '2';
      END IF;
   EXCEPTION
      WHEN xxwc_error
      THEN
         errbuf := 'Error';
         retcode := '2';
      WHEN OTHERS
      THEN
         errbuf := 'Error';
         retcode := '2';
         DBMS_OUTPUT.put_line (SQLERRM);
   END extract_wrapper;


/********************************************************************************
ProcedureName : ECOMM_EXTRACT_FULL
Purpose       : API to generate Full Organization/Customer Extract
HISTORY
===============================================================================
VERSION DATE          AUTHOR(S)       DESCRIPTION
------- -----------   --------------- -----------------------------------------
1.74    27-MAY-2014   Gopi Damuluri   TMS# 20140529-00334 Catalog Filter at Master Party Level
********************************************************************************/
  -- customer extract logic for full extracts
  -- split the FULL and DELTA extracts into two separate procedures
  -- to improve performance and to simplify queries
  -- any changes made to this procedure may have to be made in the delta procedure
   PROCEDURE ecomm_extract_full (errbuf OUT VARCHAR2, retcode OUT VARCHAR2)
   IS
      CURSOR c_organization
      IS
         -- retrieve customer level information
         SELECT mv.cust_account_code
               ,mv.cust_account_code orgentityname
               ,'o=whitecap buyer organization,o=buyer organization,o=root organization'
                   parentdn
               -- begin  ver 1.50     6-FEB-2013
               ,DECODE (
                   INSTR (SUBSTR( mv.cust_rollup_level2_desc, 1, 512), '"')
                  ,0, SUBSTR( mv.cust_rollup_level2_desc, 1, 512)
                  ,   '"'
                   || REPLACE (SUBSTR( mv.cust_rollup_level2_desc, 1, 512), '"', '""')
                   || '"')
                   description
               ,'Organization' orgentitytype
               ,DECODE (mv.cust_account_status, 'Active', '0', '-1') status
               ,NULL administratorlastname
               ,NULL administratorfirstname
               ,NULL administratormiddlename
               ,DECODE (INSTR (SUBSTR( mv.address1, 1, 256), '"')
                       ,0, SUBSTR(mv.address1, 1, 256)
                       ,'"' || REPLACE (SUBSTR(mv.address1,1,256), '"', '""') || '"')
                   address1
               ,DECODE (
                   NVL (mv.address2, mv.address3)
                  ,NULL, NULL
                  ,DECODE (
                      INSTR (SUBSTR( NVL (mv.address2, mv.address3),1, 128), '"')
                     ,0, SUBSTR( NVL (mv.address2, mv.address3), 1, 128)
                     ,   '"'
                      || REPLACE (SUBSTR( NVL (mv.address2, mv.address3), 1, 128), '"', '""')
                      || '"'))
                   address2
               ,DECODE (
                   DECODE (mv.address2, NULL, NULL, mv.address3)
                  ,NULL, NULL
                  ,DECODE (
                      INSTR (SUBSTR(mv.address3, 1, 128)
                            ,'"')
                     ,0, SUBSTR(mv.address3, 1, 128)
                     ,   '"'
                      || REPLACE (
                            SUBSTR (mv.address3, 1, 128)
                           ,'"'
                           ,'""')
                      || '"'))
                   address3
               ,SUBSTR( mv.city, 1, 128) city
               ,SUBSTR( mv.state_province, 1, 128) state
               ,SUBSTR( mv.country, 1, 128) country
               ,SUBSTR( mv.postal_code, 1, 40) zipcode
               ,DECODE(hcp_site.name, 'COD Customers', 'false', 
                       DECODE(hcp_acct.name, 'COD Customers', 'false', 
                              DECODE(hcp_site.credit_hold, 'Y', 'false', 
                                     DECODE(hcp_acct.credit_hold,'Y', 'false','true'))))  allow_on_account
               ,NULL price_list_name
               ,SUBSTR( mv.cust_unique_identifier, 1, 254) nickname
               ,SUBSTR( mv.main_phone_number, 1, 32) phone1
               ,SUBSTR( mv.main_fax_number, 1, 32) fax1
               -- end  ver 1.50     6-FEB-2013
               ,mv.hcasa_cust_acct_site_id
               , mv.party_id -- Version# 1.74
               , xxwc_speedbuild_ob_pkg.get_parent_party_num(mv.party_id) parent_party_num -- Version# 1.74
           FROM XXWC.XXWC_AR_CUSTOMERS_MV_TBL mv -- Version# 1.76
               ,hz_cust_site_uses_all hcsu
               -- begin   ver 1.50  6-Feb-2013
               ,(SELECT hca.account_number
                       ,hcpc.name
                       ,hcp.credit_hold
                       ,hcp.last_update_date
                   FROM hz_cust_accounts hca
                       ,hz_customer_profiles hcp
                       ,hz_cust_profile_classes hcpc
                  WHERE     hcp.profile_class_id = hcpc.profile_class_id
                        AND hcp.cust_account_id = hca.cust_account_id
                        AND hcp.site_use_id IS NULL
                        AND hcp.status = 'A') hcp_acct
               ,(SELECT hca.account_number
                       ,hcp.site_use_id
                       ,hcpc.name
                       ,hcp.credit_hold
                       ,hcp.last_update_date
                   FROM hz_cust_accounts hca
                       ,hz_customer_profiles hcp
                       ,hz_cust_profile_classes hcpc
                  WHERE     hcp.profile_class_id = hcpc.profile_class_id
                        AND hcp.cust_account_id = hca.cust_account_id
                        AND hcp.site_use_id IS NOT NULL
                        AND hcp.status = 'A') hcp_site
                -- end ver 1.50  6-Feb-2013
          WHERE     mv.site_use_code = 'BILL_TO'
                AND mv.hcsua_primary_flag = 'Y'
                AND mv.cust_account_code = hcp_acct.account_number(+)       -- ver 1.50  6-Feb-2013
                AND mv.cust_account_code = hcp_site.account_number(+)       -- ver 1.50  6-Feb-2013
                AND mv.site_use_id = hcp_site.site_use_id(+)                -- ver 1.50  6-Feb-2013
                AND mv.site_use_id = hcsu.site_use_id
                AND mv.cust_account_status = 'Active'
                AND mv.hcasa_site_status = 'A'
                AND hcsu.status = 'A' -- only send active customers/sites for full extract
         UNION
         -- retrieve customer site level information
         SELECT mv.cust_account_code
               ,mv.party_site_number orgentityname
               ,   'o='
                || mv.cust_account_code
                || ',o=whitecap buyer organization,o=buyer organization,o=root organization'
                   parentdn
               -- begin  ver 1.50     6-FEB-2013
               ,NVL (
                   DECODE (
                      INSTR (SUBSTR( mv.site_use_location_name, 1, 512), '"')
                     ,0, SUBSTR( mv.site_use_location_name, 1, 512)
                     ,   '"'
                      || REPLACE (SUBSTR( mv.site_use_location_name, 1, 512), '"', '""')
                      || '"')
                  ,DECODE (
                      INSTR (SUBSTR( mv.cust_rollup_level2_desc, 1, 512), '"')
                     ,0, SUBSTR( mv.cust_rollup_level2_desc, 1, 512)
                     ,   '"'
                      || REPLACE (SUBSTR( mv.cust_rollup_level2_desc, 1, 512), '"', '""')
                      || '"'))
                   description
               ,--           NULL description,
                'OrganizationUnit' orgentitytype
               ,DECODE (mv.hcasa_site_status
                       ,'I', '-1'
                       ,DECODE (hcsu.status, 'A', '0', '-1'))
                   status
               ,NULL administratorlastname
               ,NULL administratorfirstname
               ,NULL administratormiddlename
               ,DECODE (INSTR (SUBSTR( mv.address1, 1, 256), '"')
                       ,0, SUBSTR(mv.address1, 1, 256)
                       ,'"' || REPLACE (SUBSTR(mv.address1, 1, 256), '"', '""') || '"')
                   address1
               ,DECODE (
                   NVL (mv.address2, mv.address3)
                  ,NULL, NULL
                  ,DECODE (
                      INSTR (SUBSTR( NVL (mv.address2, mv.address3),1, 128), '"')
                     ,0, SUBSTR( NVL (mv.address2, mv.address3), 1, 128)
                     ,   '"'
                      || REPLACE (SUBSTR( NVL (mv.address2, mv.address3), 1, 128), '"', '""')
                      || '"'))
                   address2
               ,DECODE (
                   DECODE (mv.address2, NULL, NULL, mv.address3)
                  ,NULL, NULL
                  ,DECODE (
                      INSTR (SUBSTR(mv.address3, 1, 128)
                            ,'"')
                     ,0, SUBSTR(mv.address3, 1, 128)
                     ,   '"'
                      || REPLACE (
                            SUBSTR (mv.address3, 1, 128)
                           ,'"'
                           ,'""')
                      || '"'))
                   address3
               ,SUBSTR( mv.city, 1, 128) city
               ,SUBSTR( mv.state_province, 1, 128) state
               ,SUBSTR( mv.country, 1, 128) country
               ,SUBSTR( mv.postal_code, 1, 40) zipcode
               ,DECODE(hcp_site.name, 'COD Customers', 'false', 
                       DECODE(hcp_acct.name, 'COD Customers', 'false', 
                              DECODE(hcp_site.credit_hold, 'Y', 'false', 
                                     DECODE(hcp_acct.credit_hold,'Y', 'false','true'))))  allow_on_account
               ,SUBSTR( NVL (site_contracts.name, customer_contracts.name), 1, 64)
                   price_list_name
--               ,SUBSTR( mv.cust_unique_identifier, 1, 254) nickname             -- ver 1.60  13-FEB-2013
               ,SUBSTR( mv.party_site_number, 1, 254) nickname                    -- ver 1.60  13-FEB-2013
               ,SUBSTR( mv.main_phone_number, 1, 32) phone1
               ,SUBSTR( mv.main_fax_number, 1, 32) fax1
               -- end  ver 1.50     6-FEB-2013
               ,mv.hcasa_cust_acct_site_id
               , mv.party_id -- Version# 1.74
               , xxwc_speedbuild_ob_pkg.get_parent_party_num(mv.party_id) parent_party_num -- Version# 1.74
           FROM XXWC.XXWC_AR_CUSTOMERS_MV_TBL mv -- Version# 1.76
               ,hz_cust_accounts hca
               ,hz_cust_site_uses_all hcsu
               ,(SELECT qq.qualifier_attr_value site_use_id
                       ,NVL (flv.description, flv.meaning) name                -- 16-JAN-2013  ver 1.4 enhancement
                       , GREATEST (qq.last_update_date, flv.last_update_date) last_update_date -- handle scenario when contract added to lookup type / contract created
                   FROM qp_secu_list_headers_vl qlh
                       ,qp_qualifiers_v qq
                       ,fnd_lookup_values flv
                  WHERE 1 = 1
                         AND    flv.lookup_type = 'XXWC_CONTRACT_PRICING' 
                         AND flv.enabled_flag = 'Y'
                         AND TRUNC (SYSDATE) BETWEEN NVL(flv.start_date_active
                                                        ,SYSDATE - 1)
                                                AND NVL (flv.end_date_active
                                                        ,SYSDATE + 1)
                        AND flv.meaning = qlh.name 
                        AND EXISTS (SELECT /*+ nl_sj  */ '1' -- Version# 1.76
                             FROM xxwc_om_contract_pricing_hdr xx_csp
                            WHERE 1 = 1
                              -- AND to_char(xx_csp.agreement_id) = qlh.attribute14 -- Version# 1.76
                              AND xx_csp.agreement_id = TO_NUMBER(qlh.attribute14) -- Version# 1.76
                              AND AGREEMENT_STATUS = 'APPROVED'
                           )                                              
                        AND qlh.list_type_code = 'DLT'
                        AND qlh.active_flag = 'Y'
                        AND TRUNC (SYSDATE) BETWEEN NVL(qlh.start_date_active
                                                        ,SYSDATE - 1)
                                                AND NVL (qlh.end_date_active
                                                        ,SYSDATE + 1)
                        AND qlh.list_header_id = qq.list_header_id
                        AND qq.list_line_id = -1     -- header level qualifier
                        AND qq.qualifier_context = 'CUSTOMER'
                        AND qq.qualifier_attribute = 'QUALIFIER_ATTRIBUTE11' --  ship-to   site_use_id
                        -- begin 16-JAN-2013  ver 1.4 enhancement
                        AND EXISTS (SELECT 'x' -- has web-enabled items available
                                      FROM qp_modifier_summary_v qms
                                          ,XXWC.XXWC_QP_ECOMMERCE_ITEMS_MV_TBL msi
                                     WHERE     qms.list_header_id = qlh.list_header_id
                                           AND qms.product_attr = 'PRICING_ATTRIBUTE1' -- 1=item  2=category 3=all
                                           AND TO_NUMBER(qms.product_attr_val) = msi.inventory_item_id
                                           AND qms.list_line_type_code = 'DIS'
                                           AND qms.modifier_level_code = 'LINE'
                                           AND TRUNC (SYSDATE) BETWEEN NVL (
                                                        qms.start_date_active
                                                       ,SYSDATE - 1)
                                                 AND NVL (qms.end_date_active
                                                         ,SYSDATE + 1)
                                   )
                        -- end  16-JAN-2013  ver 1.4 enhancement
                        AND qlh.list_header_id =
                               (SELECT MAX (qlh.list_header_id) -- handle potential of multiple active contract price lists
                                  FROM qp_secu_list_headers_vl qlh2
                                      ,qp_qualifiers_v qq2
                                      ,fnd_lookup_values flv2 
                                 WHERE     qq.qualifier_attr_value =
                                              qq2.qualifier_attr_value
                                       AND qq2.qualifier_context = 'CUSTOMER'
                                       AND qq2.list_header_id =
                                              qlh2.list_header_id
                                       AND UPPER (qlh2.name) NOT LIKE 'VQN%'
                                       AND qq2.list_line_id = -1 -- header level qualifier
                                       AND qlh2.list_type_code = 'DLT'
                                       AND qlh2.active_flag = 'Y'
                                       AND TRUNC (SYSDATE) BETWEEN NVL(qlh2.start_date_active
                                                                     ,  SYSDATE
                                                                      - 1)
                                                               AND NVL (
                                                                      qlh2.end_date_active
                                                                     ,  SYSDATE
                                                                      + 1)
                                       AND qq2.qualifier_context = 'CUSTOMER'
                                       AND qq2.qualifier_attribute =
                                              'QUALIFIER_ATTRIBUTE11' --  ship-to   site_use_id
                                       AND TRUNC (SYSDATE) BETWEEN NVL(flv2.start_date_active  
                                                                     ,  SYSDATE
                                                                      - 1)
                                                               AND NVL (
                                                                      flv2.end_date_active
                                                                     ,  SYSDATE
                                                                      + 1)
                                       AND flv2.enabled_flag = 'Y' 
                                       AND qlh2.name = flv.meaning
                                       AND flv2.lookup_type = 'XXWC_CONTRACT_PRICING'                                      
                                       )) site_contracts
               ,(SELECT qq.qualifier_attr_value cust_account_id
                        , NVL (flv.description, flv.meaning) name               -- 16-JAN-2013  ver 1.4 enhancement
                        , GREATEST (qq.last_update_date, flv.last_update_date) last_update_date  
                   FROM qp_secu_list_headers_vl qlh
                       ,qp_qualifiers_v qq
                       ,fnd_lookup_values flv
                  WHERE 1 = 1
                        AND flv.lookup_type = 'XXWC_CONTRACT_PRICING' 
                        AND flv.enabled_flag = 'Y'
                        AND TRUNC (SYSDATE) BETWEEN NVL(flv.start_date_active
                                                        ,SYSDATE - 1)
                                                AND NVL (flv.end_date_active
                                                        ,SYSDATE + 1)
                        AND flv.meaning = qlh.name 
                        AND UPPER (qlh.name) NOT LIKE 'VQN%'
                        AND TRUNC (SYSDATE) BETWEEN NVL(qlh.start_date_active
                                                        ,SYSDATE - 1)
                                                AND NVL (qlh.end_date_active
                                                        ,SYSDATE + 1)
                        AND qlh.list_header_id = qq.list_header_id
                        AND qq.list_line_id = -1     -- header level qualifier
                        AND qlh.list_type_code = 'DLT'
                        AND qlh.active_flag = 'Y'
                        AND qq.qualifier_context = 'CUSTOMER'
                        AND qq.qualifier_attribute = 'QUALIFIER_ATTRIBUTE32' -- bill-to customer
                        -- begin 16-JAN-2013  ver 1.4 enhancement
                        AND EXISTS (SELECT 'x' -- has web-enabled items available
                                      FROM qp_modifier_summary_v qms
                                          ,XXWC.XXWC_QP_ECOMMERCE_ITEMS_MV_TBL msi
                                     WHERE     qms.list_header_id = qlh.list_header_id
                                           AND qms.product_attr = 'PRICING_ATTRIBUTE1' -- 1=item  2=category 3=all
                                           AND TO_NUMBER(qms.product_attr_val) = msi.inventory_item_id
                                           AND qms.list_line_type_code = 'DIS'
                                           AND qms.modifier_level_code = 'LINE'
                                           AND TRUNC (SYSDATE) BETWEEN NVL (
                                                        qms.start_date_active
                                                       ,SYSDATE - 1)
                                                 AND NVL (qms.end_date_active
                                                         ,SYSDATE + 1)
                                   )
                        -- end 16-JAN-2013  ver 1.4 enhancement
                        AND qlh.list_header_id =
                               (SELECT MAX (qlh2.list_header_id)
                                  FROM qp_secu_list_headers_vl qlh2
                                      ,qp_qualifiers_v qq2
                                      ,fnd_lookup_values flv2 
                                 WHERE qq.qualifier_attr_value = qq2.qualifier_attr_value
                                       AND qq2.qualifier_context = 'CUSTOMER'
                                       AND qq2.list_header_id = qlh2.list_header_id
                                       AND UPPER (qlh2.name) NOT LIKE 'VQN%'
                                       AND qq2.list_line_id = -1 -- header level qualifier
                                       AND qlh2.list_type_code = 'DLT'
                                       AND qlh2.active_flag = 'Y'
                                       AND TRUNC (SYSDATE) BETWEEN NVL(qlh2.start_date_active
                                                                     ,  SYSDATE
                                                                      - 1)
                                                               AND NVL (
                                                                      qlh2.end_date_active
                                                                     ,  SYSDATE
                                                                      + 1)
                                       AND qq2.qualifier_context = 'CUSTOMER'
                                       AND qq2.qualifier_attribute = 'QUALIFIER_ATTRIBUTE32' -- bill-to customer  cust_account_id
                                       AND TRUNC (SYSDATE) BETWEEN NVL(flv2.start_date_active
                                                                     ,  SYSDATE
                                                                      - 1)
                                                               AND NVL (
                                                                      flv2.end_date_active
                                                                     ,  SYSDATE
                                                                      + 1)
                                       AND flv2.enabled_flag = 'Y'
                                       AND qlh2.name = flv.meaning
                                       AND flv2.lookup_type = 'XXWC_CONTRACT_PRICING'
                                       )) customer_contracts
               -- begin    ver 1.50  6-Feb-2013
               ,(SELECT hca.account_number
                       ,hcpc.name
                       ,hcp.credit_hold
                       ,hcp.last_update_date
                   FROM hz_cust_accounts hca
                       ,hz_customer_profiles hcp
                       ,hz_cust_profile_classes hcpc
                  WHERE     hcp.profile_class_id = hcpc.profile_class_id
                        AND hcp.cust_account_id = hca.cust_account_id
                        AND hcp.site_use_id IS NULL
                        AND hcp.status = 'A') hcp_acct
               ,(SELECT hca.account_number
                       ,hcsu.cust_acct_site_id
                       ,hcpc.name
                       ,hcp.credit_hold
                       ,hcp.last_update_date
                   FROM hz_cust_accounts hca
                       ,hz_cust_site_uses_all hcsu
                       ,hz_customer_profiles hcp
                       ,hz_cust_profile_classes hcpc
                  WHERE     hcp.profile_class_id = hcpc.profile_class_id
                        AND hcp.cust_account_id = hca.cust_account_id
                        AND hcp.site_use_id = hcsu.site_use_id
                        AND hcp.status = 'A') hcp_site
               -- end    ver 1.50  6-Feb-2013
          WHERE     mv.cust_account_code = hca.account_number  
                AND hca.status = 'A'
                AND hca.cust_account_id = customer_contracts.cust_account_id(+)
                AND mv.site_use_id = site_contracts.site_use_id(+)
                AND mv.site_use_code = 'SHIP_TO'
                AND mv.site_use_id = hcsu.site_use_id
                -- verify primary bill-to exists
                AND EXISTS (SELECT 'x' FROM XXWC.XXWC_AR_CUSTOMERS_MV_TBL mv2 -- Version# 1.76
                             WHERE     mv.cust_account_code = mv2.cust_account_code
                                   AND mv2.site_use_code = 'BILL_TO'
                                   AND mv2.hcsua_primary_flag = 'Y')
                -- retrieve credit profile  /credit hold information
                AND mv.cust_account_code = hcp_acct.account_number(+)              -- rev 1.50   6-Feb-2013
                AND mv.cust_account_code = hcp_site.account_number(+)              -- rev 1.50   6-Feb-2013
                AND mv.hcasa_cust_acct_site_id = hcp_site.cust_acct_site_id(+)     -- rev 1.50   6-Feb-2013
                AND mv.hcasa_site_status = 'A'
                AND hcsu.status = 'A' -- only send active sites/site-uses for full extract
         ORDER BY 1
                 ,5
                 ,2
                 ,4;

      l_cnt             NUMBER := 0;
      l_role_cnt        NUMBER := 0;
      l_group_cnt       NUMBER := 0;

      l_ownerdn         VARCHAR2 (240);

      l_err_callfrom    VARCHAR2 (75)
                           := 'XXWC_CUSTOMER_EXTRACT_PKG.Ecomm_Extract_Full';
      l_err_callpoint   VARCHAR2 (75) := 'START';
      l_distro_list     VARCHAR2 (75)
                           DEFAULT 'HDSOracleDevelopers@hdsupply.com';
      l_message         VARCHAR2 (2000);
   BEGIN
      errbuf := 'Success';
      retcode := 0;

      fnd_file.put_line (fnd_file.LOG
                        ,'Creating Organization File : ' || l_org_file_name);

      l_err_callpoint := 'Before FOPEN of Organization File';
      l_org_outfile := UTL_FILE.fopen (l_directory, l_org_tmp_file_name, 'w');
      l_org_role_outfile :=
         UTL_FILE.fopen (l_directory, l_org_role_tmp_file_name, 'w');
      l_org_role_outfile :=
         UTL_FILE.fopen (l_directory, l_org_role_tmp_file_name, 'w');
      l_org_group_outfile :=
         UTL_FILE.fopen (l_directory, l_org_group_tmp_file_name, 'w');

      -- generate column headings
      UTL_FILE.put_line (l_org_outfile, 'Organization');
      UTL_FILE.put_line (
         l_org_outfile
        ,   'orgEntityName|parentDN|description|orgEntityType|status|administratorLastName|administratorFirstName|'
         || 'administratorMiddleName|address1|address2|address3|city|state|country|zipCode|orgEntityField1|'
         || 'orgEntityField2|orgEntityField3|nickName|phone1|email2|fax1'); -- Version# 1.72
--         || 'orgEntityField2|nickName|phone1|fax1');

      -- generate column headings
      UTL_FILE.put_line (l_org_role_outfile, 'OrganizationRole');
      UTL_FILE.put_line (l_org_role_outfile, 'orgentityDN|roleName|delete');

      -- generate column headings
      UTL_FILE.put_line (l_org_group_outfile, 'MemberGroupMember');
      UTL_FILE.put_line (
         l_org_group_outfile
        ,'memberGroupName|distinguishedName|excluded|memberType|delete');

/* rev 1.60  13-FEB-2013 eliminate the records for Buyer Organization and Whitecap Buyer Organization
      -- generate top parent organization records for full extract only
      -- create organization/role for buyer organization
      UTL_FILE.put_line (
         l_org_outfile
        ,'Buyer Organization|o=root organization|Buyer Organization|Organization|0||||501 W Church St|||Orlando|FL|US|32805||||');
      UTL_FILE.put_line (
         l_org_role_outfile
        ,'o=buyer organization,o=root organization|Buyer Administrator|0');

      -- create organization roles for whitecap buyer organization
      UTL_FILE.put_line (
         l_org_outfile
        ,   'Whitecap Buyer Organization|o=buyer organization,o=root organization|Whitecap Buyer Organization|Organization|0|'
         || '|||501 W Church St|||Orlando|FL|US|32805||||');
      UTL_FILE.put_line (
         l_org_role_outfile
        ,'o=whitecap buyer organization,o=buyer organization,o=root organization|Buyer Administrator|0');

      l_cnt := l_cnt + 2;
      l_role_cnt := l_role_cnt + 2;
*/

      FOR r_cust IN c_organization
      LOOP
         l_cnt := l_cnt + 1;

         -- generate organization data
         UTL_FILE.put_line (
            l_org_outfile
           ,   r_cust.orgentityname
            || '|'
            || r_cust.parentdn
            || '|'
            || r_cust.description
            || '|'
            || r_cust.orgentitytype
            || '|'
            || r_cust.status
            || '|'
            || r_cust.administratorlastname
            || '|'
            || r_cust.administratorfirstname
            || '|'
            || r_cust.administratormiddlename
            || '|'
            || r_cust.address1
            || '|'
            || r_cust.address2
            || '|'
            || r_cust.address3
            || '|'
            || r_cust.city
            || '|'
            || r_cust.state
            || '|'
            || r_cust.country
            || '|'
            || r_cust.zipcode
            || '|'
            || r_cust.price_list_name                     -- orgEntityField1
            || '|'
            || r_cust.allow_on_account                    -- orgEntityField2
            || '|'       
--            || 'WC_'||r_cust.orgEntityName                -- orgEntityField3 -- Version# 1.70
            || 'WC_'||r_cust.parent_party_num               -- orgEntityField3 -- Version# Version# 1.74
            || '|'       
            || r_cust.nickname
            || '|'
            || r_cust.phone1
            || '|'
--            || r_cust.email2 -- Version# 1.72
  --          || '|'
            || r_cust.fax1);

         -- create role record for each organization / organization unit
         IF r_cust.orgentitytype = 'Organization'
         THEN
            l_ownerdn :=
               LOWER ('o=' || r_cust.orgentityname || ',' || r_cust.parentdn);
            UTL_FILE.put_line (l_org_role_outfile
                              ,l_ownerdn || '|' || 'Buyer Administrator|0');
         ELSE
            l_ownerdn :=
               LOWER (
                  'ou=' || r_cust.orgentityname || ',' || r_cust.parentdn);
            UTL_FILE.put_line (
               l_org_role_outfile
              ,l_ownerdn || '|' || 'Organization Participant|0');
         END IF;

         l_role_cnt := l_role_cnt + 1;


         -- create group member records
         UTL_FILE.put_line (
            l_org_group_outfile
           ,   'RegisteredCustomers'
            || '|'
            || l_ownerdn
            || '|'
            || 'false|organization|0');
         l_group_cnt := l_group_cnt + 1;
      END LOOP;

      -- close data files
      UTL_FILE.fclose (l_org_outfile);
      UTL_FILE.fclose (l_org_role_outfile);
      UTL_FILE.fclose (l_org_group_outfile);

      fnd_file.put_line (fnd_file.LOG
                        ,l_cnt || ' organization records generated');
      fnd_file.put_line (
         fnd_file.LOG
        ,l_role_cnt || ' organization role records generated');
      fnd_file.put_line (
         fnd_file.LOG
        ,l_group_cnt || ' organization member-group-member records generated');

      fnd_file.put_line (
         fnd_file.LOG
        ,   'Renaming organization file '
         || l_org_tmp_file_name
         || ' => '
         || l_org_file_name);
      UTL_FILE.frename (src_location    => l_directory
                       ,src_filename    => l_org_tmp_file_name
                       ,dest_location   => l_directory
                       ,dest_filename   => l_org_file_name
                       ,overwrite       => TRUE);

      fnd_file.put_line (
         fnd_file.LOG
        ,   'Renaming organization role file '
         || l_org_role_tmp_file_name
         || ' => '
         || l_org_role_file_name);
      UTL_FILE.frename (src_location    => l_directory
                       ,src_filename    => l_org_role_tmp_file_name
                       ,dest_location   => l_directory
                       ,dest_filename   => l_org_role_file_name
                       ,overwrite       => TRUE);

      fnd_file.put_line (
         fnd_file.LOG
        ,   'Renaming organization group file '
         || l_org_group_tmp_file_name
         || ' => '
         || l_org_group_file_name);
      UTL_FILE.frename (src_location    => l_directory
                       ,src_filename    => l_org_group_tmp_file_name
                       ,dest_location   => l_directory
                       ,dest_filename   => l_org_group_file_name
                       ,overwrite       => TRUE);

      fnd_file.put_line (fnd_file.LOG, 'Process Complete');
   EXCEPTION
      WHEN OTHERS
      THEN
         errbuf := 'Error';
         retcode := 2;
         fnd_file.put_line (fnd_file.LOG, SQLERRM);

         IF UTL_FILE.is_open (l_org_outfile)
         THEN
            UTL_FILE.fclose (l_org_outfile);
         END IF;

         IF UTL_FILE.is_open (l_org_role_outfile)
         THEN
            UTL_FILE.fclose (l_org_role_outfile);
         END IF;

         IF UTL_FILE.is_open (l_org_group_outfile)
         THEN
            UTL_FILE.fclose (l_org_group_outfile);
         END IF;

         xxcus_error_pkg.xxcus_error_main_api (
            p_called_from         => l_err_callfrom
           ,p_calling             => l_err_callpoint
           ,p_ora_error_msg       => SQLERRM
           ,p_error_desc          => l_message
           ,p_distribution_list   => l_distro_list
           ,p_module              => 'XXWC');
   END ecomm_extract_full;

/********************************************************************************
ProcedureName : ECOMM_EXTRACT_DELTA
Purpose       : API to generate Organization/Customer Extract for Delta Date
HISTORY
===============================================================================
VERSION DATE          AUTHOR(S)       DESCRIPTION
------- -----------   --------------- -----------------------------------------
1.74    27-MAY-2014   Gopi Damuluri   TMS# 20140529-00334 Catalog Filter at Master Party Level
1.76    01-FEB-2015   Gopi Damuluri   TMS# 20141212-00194 Tuning of EComm Org Extract
********************************************************************************/
   -- procedure to generate delta extract
   -- split the FULL and DELTA extracts into two separate procedures
   -- to improve performance and to simplify queries
   -- any changes made to this procedure may have to be made in the delta procedure
   PROCEDURE ecomm_extract_delta (errbuf                   OUT VARCHAR2
                                 ,retcode                  OUT VARCHAR2
                                 ,p_last_extract_date   IN     DATE)
   IS
      CURSOR c_organization
      IS
         -- retrieve customer level information
         SELECT /*+ use_index(mv XXWC.XXWC_QP_ECOMMERCE_ITEMS_MV_N1) */ mv.cust_account_code
               ,mv.cust_account_code orgentityname
               ,'o=whitecap buyer organization,o=buyer organization,o=root organization'
                   parentdn
               -- begin  ver 1.50     6-FEB-2013
               ,DECODE (
                   INSTR (SUBSTR(mv.cust_rollup_level2_desc, 1, 512), '"')
                  ,0, SUBSTR( mv.cust_rollup_level2_desc, 1, 512)
                  ,   '"'
                   || REPLACE (SUBSTR( mv.cust_rollup_level2_desc, 1, 512), '"', '""')
                   || '"')
                   description
               ,'Organization' orgentitytype
               ,DECODE (mv.cust_account_status, 'Active', '0', '-1') status
               ,NULL administratorlastname
               ,NULL administratorfirstname
               ,NULL administratormiddlename
               ,DECODE (INSTR (SUBSTR( mv.address1, 1, 256), '"')
                       ,0, SUBSTR(mv.address1, 1, 256)
                       ,'"' || REPLACE (SUBSTR(mv.address1, 1, 256), '"', '""') || '"')
                   address1
               ,DECODE (
                   NVL (mv.address2, mv.address3)
                  ,NULL, NULL
                  ,DECODE (
                      INSTR (SUBSTR( NVL (mv.address2, mv.address3),1, 128), '"')
                     ,0, SUBSTR( NVL (mv.address2, mv.address3), 1, 128)
                     ,   '"'
                      || REPLACE (SUBSTR( NVL (mv.address2, mv.address3), 1, 128), '"', '""')
                      || '"'))
                   address2
               ,DECODE (
                   DECODE (mv.address2, NULL, NULL, mv.address3)
                  ,NULL, NULL
                  ,DECODE (
                      INSTR (SUBSTR(mv.address3, 1, 128)
                            ,'"')
                     ,0, SUBSTR(mv.address3, 1, 128)
                     ,   '"'
                      || REPLACE (
                            SUBSTR (mv.address3, 1, 128)
                           ,'"'
                           ,'""')
                      || '"'))
                   address3
               ,SUBSTR( mv.city, 1, 128) city
               ,SUBSTR( mv.state_province, 1, 128) state
               ,SUBSTR( mv.country, 1, 128) country
               ,SUBSTR( mv.postal_code, 1, 40) zipcode
               ,DECODE (NVL(hcp_site.name, hcp_acct.name)
                       ,'COD Customers', 'false'
                       ,DECODE (Nvl(hcp_site.credit_hold, hcp_acct.credit_hold), 'Y', 'false', 'true'))
                   allow_on_account
               ,NULL price_list_name
               ,SUBSTR( mv.cust_unique_identifier, 1, 254) nickname
               ,SUBSTR( mv.main_phone_number, 1, 32) phone1
               ,NULL                               email2 -- Version# 1.72
               ,SUBSTR( mv.main_fax_number, 1, 32) fax1
               -- end  ver 1.50     6-FEB-2013
               ,mv.hcasa_cust_acct_site_id
               , mv.party_id -- Version# 1.74
               , xxwc_speedbuild_ob_pkg.get_parent_party_num(mv.party_id) parent_party_num -- Version# 1.74
           FROM XXWC.XXWC_AR_CUSTOMERS_MV_TBL mv -- Version# 1.76
               ,hz_cust_site_uses_all hcsu
               -- begin   ver 1.50  6-Feb-2013
               ,(SELECT hca.account_number
                       ,hcpc.name
                       ,hcp.credit_hold
                       ,hcp.last_update_date
                       , hca.cust_account_id
                   FROM hz_cust_accounts hca
                       ,hz_customer_profiles hcp
                       ,hz_cust_profile_classes hcpc
                  WHERE     hcp.profile_class_id = hcpc.profile_class_id
                        AND hcp.cust_account_id = hca.cust_account_id
                        AND hcp.site_use_id IS NULL
                        AND hcp.status = 'A') hcp_acct
               ,(SELECT hca.account_number
                       ,hcp.site_use_id
                       ,hcpc.name
                       ,hcp.credit_hold
                       ,hcp.last_update_date
                   FROM hz_cust_accounts hca
                       ,hz_customer_profiles hcp
                       ,hz_cust_profile_classes hcpc
                  WHERE     hcp.profile_class_id = hcpc.profile_class_id
                        AND hcp.cust_account_id = hca.cust_account_id
                        AND hcp.site_use_id IS NOT NULL
                        AND hcp.status = 'A') hcp_site
                -- end ver 1.50  6-Feb-2013
          WHERE     mv.site_use_code = 'BILL_TO'
                AND NOT EXISTS (SELECT '1' FROM xxwc.xxwc_sb_qp_temp_tbl sb_stg WHERE mv.cust_account_code = sb_stg.customer_number) -- Version# 1.72
                AND NOT EXISTS (SELECT /*+ hash_aj index_ffs(OOH XXWC_OE_ORDER_HDR_AL_N2)  */ '1' FROM oe_order_headers_all ooh WHERE ooh.sold_to_org_id = hcp_acct.cust_account_id AND ooh.order_type_id IN (1162, 1163)) -- Revision# 1.72 -- Version# 1.76
--                AND NOT EXISTS (SELECT '1' FROM xxwc.xxwc_sb_qp_temp_tbl stg WHERE 1 = 1 AND mv.cust_account_code = stg.customer_number) -- Revision# 1.72 -- Version# 1.76
                AND (   mv.hcsua_primary_flag = 'Y'
                     OR NOT EXISTS
                               (SELECT 'x'
                                  FROM hz_cust_acct_sites_all hcas -- handle situation when inactivating primary bill-to
                                      ,hz_cust_accounts hca                  -- ver 1.50  6-Feb-2013
                                 WHERE     mv.cust_account_code = hca.account_number           -- ver 1.50  6-Feb-2013
                                       AND hca.cust_account_id =
                                              hcas.cust_account_id
                                       AND hcas.bill_to_flag = 'P'))
                AND mv.cust_account_code = hcp_acct.account_number(+)       -- ver 1.50  6-Feb-2013
                AND mv.cust_account_code = hcp_site.account_number(+)       -- ver 1.50  6-Feb-2013
                AND mv.site_use_id = hcp_site.site_use_id(+)                -- ver 1.50  6-Feb-2013
--                AND NOT EXISTS (SELECT '1' FROM xxwc.xxwc_sb_qp_temp_tbl sb_stg WHERE mv.cust_account_code = sb_stg.customer_number) -- Version# 1.72
                AND mv.site_use_id = hcsu.site_use_id
                AND (    (   hca_last_update_date >= p_last_extract_date
                          OR            -- handles changes to customer account
                            hcasa_last_update_date >= p_last_extract_date
                          OR      -- handles changes to customer account sites
                            hcsua_last_update_date >= p_last_extract_date
                          OR  -- handles changes to customer account site uses
                            hp_last_update_date >= p_last_extract_date
                          OR                       -- handles changes to party
                            hps_last_update_date >= p_last_extract_date
                          OR                 -- handles changes to party sites
                            hl_last_update_date >= p_last_extract_date -- handles changes to party site locations
                          -- begin ver 1.50  6-Feb-2013
                          OR
                            hcp_acct.last_update_date >= p_last_extract_date    -- handles changes in customer level profile
                          OR
                            hcp_site.last_update_date >= p_last_extract_date    -- handles changes in site level profile
                          -- end ver 1.50  6-Feb-2013
                                                                      )
                     AND -- eliminate any inactive sites/uses that didnt change at that level
                         NOT (   (    mv.hcsua_last_update_date <
                                         p_last_extract_date
                                  AND hcsu.status = 'I')
                              OR (    mv.hcasa_last_update_date <
                                         p_last_extract_date
                                  AND mv.hcasa_site_status = 'I')))
         UNION
         -- retrieve customer site level information
         SELECT mv.cust_account_code
               ,mv.party_site_number orgentityname
               ,   'o='
                || mv.cust_account_code
                || ',o=whitecap buyer organization,o=buyer organization,o=root organization'
                   parentdn
               -- begin  ver 1.50     6-FEB-2013
               ,NVL (
                   DECODE (
                      INSTR (SUBSTR( mv.site_use_location_name, 1, 512), '"')
                     ,0, SUBSTR( mv.site_use_location_name, 1, 512)
                     ,   '"'
                      || REPLACE (SUBSTR( mv.site_use_location_name, 1, 512), '"', '""')
                      || '"')
                  ,DECODE (
                      INSTR (SUBSTR( mv.cust_rollup_level2_desc, 1, 512), '"')
                     ,0, SUBSTR( mv.cust_rollup_level2_desc, 1, 512)
                     ,   '"'
                      || REPLACE (SUBSTR( mv.cust_rollup_level2_desc, 1, 512), '"', '""')
                      || '"'))
                   description
               ,--           NULL description,
                'OrganizationUnit' orgentitytype
               ,DECODE (mv.hcasa_site_status
                       ,'I', '-1'
                       ,DECODE (hcsu.status, 'A', '0', '-1'))
                   status
               ,NULL administratorlastname
               ,NULL administratorfirstname
               ,NULL administratormiddlename
               ,DECODE (INSTR (SUBSTR( mv.address1, 1, 256), '"')
                       ,0, SUBSTR(mv.address1, 1, 256)
                       ,'"' || REPLACE (SUBSTR(mv.address1, 1, 256), '"', '""') || '"')
                   address1
               ,DECODE (
                   NVL (mv.address2, mv.address3)
                  ,NULL, NULL
                  ,DECODE (
                      INSTR (SUBSTR( NVL (mv.address2, mv.address3),1, 128), '"')
                     ,0, SUBSTR( NVL (mv.address2, mv.address3), 1, 128)
                     ,   '"'
                      || REPLACE (SUBSTR( NVL (mv.address2, mv.address3), 1, 128), '"', '""')
                      || '"'))
                   address2
               ,DECODE (
                   DECODE (mv.address2, NULL, NULL, mv.address3)
                  ,NULL, NULL
                  ,DECODE (
                      INSTR (SUBSTR(mv.address3, 1, 128)
                            ,'"')
                     ,0, SUBSTR(mv.address3, 1, 128)
                     ,   '"'
                      || REPLACE (
                            SUBSTR (mv.address3, 1, 128)
                           ,'"'
                           ,'""')
                      || '"'))
                   address3
               ,SUBSTR( mv.city, 1, 128) city
               ,SUBSTR( mv.state_province, 1, 128) state
               ,SUBSTR( mv.country, 1, 128) country
               ,SUBSTR( mv.postal_code, 1, 40) zipcode
               ,DECODE(hcp_site.name, 'COD Customers', 'false', 
                       DECODE(hcp_acct.name, 'COD Customers', 'false', 
                              DECODE(hcp_site.credit_hold, 'Y', 'false', 
                                     DECODE(hcp_acct.credit_hold,'Y', 'false','true'))))  allow_on_account
               ,SUBSTR( NVL (site_contracts.name, customer_contracts.name), 1, 64)
                   price_list_name
--               ,SUBSTR( mv.cust_unique_identifier, 1, 254) nickname             -- ver 1.60  13-FEB-2013
               ,SUBSTR( mv.party_site_number, 1, 254) nickname                    -- ver 1.60  13-FEB-2013
               ,SUBSTR( mv.main_phone_number, 1, 32) phone1
               ,NULL                               email2 -- Version# 1.72               
               ,SUBSTR( mv.main_fax_number, 1, 32) fax1
               -- end  ver 1.50     6-FEB-2013
               ,mv.hcasa_cust_acct_site_id
               , mv.party_id -- Version# 1.74
               , xxwc_speedbuild_ob_pkg.get_parent_party_num(mv.party_id) parent_party_num -- Version# 1.74               
           FROM XXWC.XXWC_AR_CUSTOMERS_MV_TBL mv -- Version# 1.76
               ,hz_cust_accounts hca
               ,hz_cust_site_uses_all hcsu
               ,(SELECT qq.qualifier_attr_value site_use_id
                       , NVL (flv.description, flv.meaning) name 
                       ,GREATEST (qq.last_update_date
                                , flv.last_update_date 
                                , qlh.last_update_date) last_update_date
                                , MAX (QLH.LIST_HEADER_ID) over () hid -- Version# 1.76
                            -- handle scenario when contract added to lookup type / contract created
                   FROM qp_secu_list_headers_vl qlh
                       ,qp_qualifiers_v qq
                       ,fnd_lookup_values flv 
                  WHERE     qq.qualifier_context = 'CUSTOMER'
                        AND qq.list_header_id = qlh.list_header_id
                        AND qq.list_line_id = -1     -- header level qualifier
                        AND qlh.list_type_code = 'DLT'
                        AND qlh.active_flag = 'Y'
                        AND TRUNC (SYSDATE) BETWEEN NVL(qlh.start_date_active
                                                        ,SYSDATE - 1)
                                                AND NVL (qlh.end_date_active
                                                        ,SYSDATE + 1)
                        AND qq.qualifier_context = 'CUSTOMER'
                        AND qq.qualifier_attribute = 'QUALIFIER_ATTRIBUTE11' --  ship-to   site_use_id
                        AND TRUNC (SYSDATE) BETWEEN NVL(flv.start_date_active  
                                                        ,SYSDATE - 1)
                                                AND NVL (flv.end_date_active
                                                        ,SYSDATE + 1)
                        AND flv.enabled_flag = 'Y'
                        AND qlh.name = flv.meaning
                        AND UPPER (qlh.name) NOT LIKE 'VQN%'
                        AND flv.lookup_type = 'XXWC_CONTRACT_PRICING'
                        AND EXISTS (SELECT /*+ nl_sj push_pred use_nl(QMS.ql QMS.qpa msi) */ 'x' -- has web-enabled items available -- Version# 1.76
                                      FROM qp_modifier_summary_v qms
                                          ,XXWC.XXWC_QP_ECOMMERCE_ITEMS_MV_TBL msi
                                     WHERE     qms.list_header_id = qlh.list_header_id
                                           AND qms.product_attr = 'PRICING_ATTRIBUTE1' -- 1=item  2=category 3=all
                                           AND TO_NUMBER(qms.product_attr_val) = msi.inventory_item_id
                                           AND qms.list_line_type_code = 'DIS'
                                           AND qms.modifier_level_code = 'LINE'
                                           AND TRUNC (SYSDATE) BETWEEN NVL (
                                                        qms.start_date_active
                                                       ,SYSDATE - 1)
                                                 AND NVL (qms.end_date_active
                                                         ,SYSDATE + 1)
                                   )
--                      AND qlh.list_header_id =	   -- Version# 1.76 > Start
--                             (SELECT MAX (qlh.list_header_id) -- handle potential of multiple active contract price lists
--                                FROM qp_secu_list_headers_vl qlh2
--                                    ,qp_qualifiers_v qq2
--                                    ,fnd_lookup_values flv2
--                               WHERE 1 = 1
--                                     AND qq.qualifier_attr_value = qq2.qualifier_attr_value
--                                     AND qq2.qualifier_context = 'CUSTOMER'
--                                     AND qq2.list_header_id = qlh2.list_header_id
--                                     AND UPPER (qlh2.name) NOT LIKE 'VQN%'
--                                     AND qq2.list_line_id = -1 -- header level qualifier
--                                     AND qlh2.list_type_code = 'DLT'
--                                     AND qlh2.active_flag = 'Y'
--                                     AND TRUNC (SYSDATE) BETWEEN NVL(qlh2.start_date_active
--                                                                   ,  SYSDATE
--                                                                    - 1)
--                                                             AND NVL (
--                                                                    qlh2.end_date_active
--                                                                   ,  SYSDATE
--                                                                    + 1)
--                                     AND qq2.qualifier_context = 'CUSTOMER'
--                                     AND qq2.qualifier_attribute = 'QUALIFIER_ATTRIBUTE11' --  ship-to
--                                     AND TRUNC (SYSDATE) BETWEEN NVL(flv2.start_date_active 
--                                                                   ,  SYSDATE
--                                                                    - 1)
--                                                             AND NVL (
--                                                                    flv2.end_date_active
--                                                                   ,  SYSDATE
--                                                                    + 1)
--                                     AND flv2.enabled_flag = 'Y'
--                                     AND qlh2.name = flv.meaning
--                                     AND flv2.lookup_type = 'XXWC_CONTRACT_PRICING'
--                                       )  -- Version# 1.76 < End
                                       ) site_contracts
               ,(SELECT qq.qualifier_attr_value cust_account_id
                       ,NVL (flv.description, flv.meaning) name
                       , GREATEST (qq.last_update_date
                                 , flv.last_update_date 
                                 , qlh.last_update_date)
                           last_update_date
                         , MAX (QLH.LIST_HEADER_ID) over () hid -- Version# 1.76
                   FROM qp_secu_list_headers_vl qlh
                       ,qp_qualifiers_v qq
                       ,fnd_lookup_values flv
                  WHERE     qq.qualifier_context = 'CUSTOMER'
                        AND qq.list_header_id = qlh.list_header_id
                        AND qq.list_line_id = -1     -- header level qualifier
                        AND qlh.list_type_code = 'DLT'
                        AND qlh.active_flag = 'Y'
                        AND TRUNC (SYSDATE) BETWEEN NVL(qlh.start_date_active
                                                        ,SYSDATE - 1)
                                                AND NVL (qlh.end_date_active
                                                        ,SYSDATE + 1)
                        AND qq.qualifier_context = 'CUSTOMER'
                        AND qq.qualifier_attribute = 'QUALIFIER_ATTRIBUTE32' -- bill-to customer  cust_account_id
                        AND TRUNC (SYSDATE) BETWEEN NVL(flv.start_date_active 
                                                        ,SYSDATE - 1)
                                                AND NVL (flv.end_date_active
                                                        ,SYSDATE + 1)
                        AND flv.enabled_flag = 'Y'
                        AND UPPER (qlh.name) NOT LIKE 'VQN%'
                        AND qlh.name = flv.meaning
                        AND flv.lookup_type = 'XXWC_CONTRACT_PRICING' 
                        AND EXISTS (SELECT /*+ nl_sj push_pred use_nl(QMS.ql QMS.qpa msi) */ 'x' -- has web-enabled items available -- Version# 1.76
                                      FROM qp_modifier_summary_v qms
                                          ,XXWC.XXWC_QP_ECOMMERCE_ITEMS_MV_TBL msi
                                     WHERE     qms.list_header_id = qlh.list_header_id
                                           AND qms.product_attr = 'PRICING_ATTRIBUTE1' -- 1=item  2=category 3=all
                                           AND TO_NUMBER(qms.product_attr_val) = msi.inventory_item_id
                                           AND qms.list_line_type_code = 'DIS'
                                           AND qms.modifier_level_code = 'LINE'
                                           AND TRUNC (SYSDATE) BETWEEN NVL (
                                                        qms.start_date_active
                                                       ,SYSDATE - 1)
                                                 AND NVL (qms.end_date_active
                                                         ,SYSDATE + 1)
                                   )
-- Version# 1.76 > start
--                        AND qlh.list_header_id = (SELECT MAX (qlh2.list_header_id)
--                                  FROM qp_secu_list_headers_vl qlh2
--                                      ,qp_qualifiers_v qq2
--                                      ,fnd_lookup_values flv2
--                                 WHERE     qq.qualifier_attr_value =
--                                              qq2.qualifier_attr_value
--                                       AND qq2.qualifier_context = 'CUSTOMER'
--                                       AND qq2.list_header_id =
--                                              qlh2.list_header_id
--                                       AND UPPER (qlh2.name) NOT LIKE 'VQN%'
--                                       AND qq2.list_line_id = -1 -- header level qualifier
--                                       AND qlh2.list_type_code = 'DLT'
--                                       AND qlh2.active_flag = 'Y'
--                                       AND TRUNC (SYSDATE) BETWEEN NVL(qlh2.start_date_active
--                                                                     , SYSDATE
--                                                                      - 1)
--                                                               AND NVL (
--                                                                      qlh2.end_date_active
--                                                                     ,  SYSDATE
--                                                                      + 1)
--                                       AND qq2.qualifier_context = 'CUSTOMER'
--                                       AND qq2.qualifier_attribute =
--                                              'QUALIFIER_ATTRIBUTE32' -- bill-to customer  cust_account_id
--                                       AND TRUNC (SYSDATE) BETWEEN NVL(flv2.start_date_active  
--                                                                     , SYSDATE
--                                                                      - 1)
--                                                               AND NVL (
--                                                                      flv2.end_date_active
--                                                                     ,  SYSDATE
--                                                                      + 1)
--                                       AND flv2.enabled_flag = 'Y'
--                                       AND qlh2.name = flv.meaning
--                                       AND flv2.lookup_type = 'XXWC_CONTRACT_PRICING' 
--                                       )
-- Version# 1.76 < End
                                        ) customer_contracts
               -- begin    ver 1.50  6-Feb-2013
               ,(SELECT hca.account_number
                       ,hcpc.name
                       ,hcp.credit_hold
                       ,hcp.last_update_date
                   FROM hz_cust_accounts hca
                       ,hz_customer_profiles hcp
                       ,hz_cust_profile_classes hcpc
                  WHERE     hcp.profile_class_id = hcpc.profile_class_id
                        AND hcp.cust_account_id = hca.cust_account_id
                        AND hcp.site_use_id IS NULL
                        AND hcp.status = 'A') hcp_acct
               ,(SELECT hca.account_number
                       ,hcsu.cust_acct_site_id
                       ,hcpc.name
                       ,hcp.credit_hold
                       ,hcp.last_update_date
                   FROM hz_cust_accounts hca
                       ,hz_cust_site_uses_all hcsu
                       ,hz_customer_profiles hcp
                       ,hz_cust_profile_classes hcpc
                  WHERE     hcp.profile_class_id = hcpc.profile_class_id
                        AND hcp.cust_account_id = hca.cust_account_id
                        AND hcp.site_use_id = hcsu.site_use_id
                        AND hcp.status = 'A') hcp_site
               -- end    ver 1.50  6-Feb-2013
          WHERE     mv.cust_account_code = hca.account_number
                AND hca.status = 'A'
                AND NOT EXISTS (SELECT '1' FROM xxwc.xxwc_sb_qp_temp_tbl sb_stg WHERE mv.cust_account_code = sb_stg.customer_number) -- Version# 1.72
                AND hca.cust_account_id = customer_contracts.cust_account_id(+)
                AND NOT EXISTS (SELECT '1' FROM oe_order_headers_all ooh WHERE ooh.sold_to_org_id = hca.cust_account_id AND ooh.order_type_id IN (1162, 1163)) -- Revision# 1.72
--                AND NOT EXISTS (SELECT '1' FROM xxwc.xxwc_sb_qp_temp_tbl stg WHERE 1 = 1 AND hca.cust_account_id = stg.customer_id) -- Revision# 1.72
                AND mv.site_use_id = site_contracts.site_use_id(+)
                AND mv.site_use_code = 'SHIP_TO'
                AND mv.site_use_id = hcsu.site_use_id
                -- verify primary bill-to exists
                AND EXISTS (SELECT 'x' FROM XXWC.XXWC_AR_CUSTOMERS_MV_TBL mv2   -- Version# 1.76
                             WHERE     mv.cust_account_code = mv2.cust_account_code
                                   AND mv2.site_use_code = 'BILL_TO'
                                   AND mv2.hcsua_primary_flag = 'Y')
                -- retrieve credit profile / credit hold information
                AND mv.cust_account_code = hcp_acct.account_number(+)              -- rev 1.50   6-Feb-2013
                AND mv.cust_account_code = hcp_site.account_number(+)              -- rev 1.50   6-Feb-2013
                AND mv.hcasa_cust_acct_site_id = hcp_site.cust_acct_site_id(+)     -- rev 1.50   6-Feb-2013
                AND (    (   mv.hcasa_last_update_date >= p_last_extract_date
                          OR      -- handles changes to customer account sites
                            mv.hcsua_last_update_date >= p_last_extract_date
                          OR  -- handles changes to customer account site uses
                            mv.hps_last_update_date >= p_last_extract_date
                          OR                 -- handles changes to party sites
                            mv.hl_last_update_date >= p_last_extract_date
                          OR        -- handles changes to party site locations
                            NVL (
                                site_contracts.last_update_date
                               ,NVL (customer_contracts.last_update_date
                                    ,SYSDATE - 30)) >= p_last_extract_date
                          OR         -- handles changes to contract price list
                            hcp_site.last_update_date >= p_last_extract_date     -- rev 1.50   6-Feb-2013
                          OR
                            hcp_acct.last_update_date >= p_last_extract_date     -- rev 1.50   6-Feb-2013
                          OR
                            EXISTS
                                (SELECT 'x'
                                   FROM hz_cust_acct_sites_all hcas
                                  WHERE     hca.cust_account_id = hcas.cust_account_id
                                        AND hcas.status = 'A'
                                        AND hcas.bill_to_flag = 'P'
                                        AND hcas.creation_date >= p_last_extract_date -- handles creation of bill-to that allows org/org-unit extract
                                                                  ))
                     AND -- eliminate any inactive sites/uses that didnt change at that level
                         NOT (   (    mv.hcsua_last_update_date <
                                         p_last_extract_date
                                  AND hcsu.status = 'I')
                              OR (    mv.hcasa_last_update_date <
                                         p_last_extract_date
                                  AND mv.hcasa_site_status = 'I')))
         -- retrieve customer level information --Version# 1.50 > Start
         UNION
         SELECT mv.cust_account_code
               ,mv.cust_account_code orgentityname
               ,'o=whitecap buyer organization,o=buyer organization,o=root organization'
                   parentdn
               -- begin  ver 1.50     6-FEB-2013
               ,DECODE (
                   INSTR (SUBSTR(mv.cust_rollup_level2_desc, 1, 512), '"')
                  ,0, SUBSTR( mv.cust_rollup_level2_desc, 1, 512)
                  ,   '"'
                   || REPLACE (SUBSTR( mv.cust_rollup_level2_desc, 1, 512), '"', '""')
                   || '"')
                   description
               ,'Organization' orgentitytype
               ,DECODE (mv.cust_account_status, 'Active', '0', '-1') status
               ,NULL administratorlastname
               ,NULL administratorfirstname
               ,NULL administratormiddlename
               ,DECODE (INSTR (SUBSTR( mv.address1, 1, 256), '"')
                       ,0, SUBSTR(mv.address1, 1, 256)
                       ,'"' || REPLACE (SUBSTR(mv.address1, 1, 256), '"', '""') || '"')
                   address1
               ,DECODE (
                   NVL (mv.address2, mv.address3)
                  ,NULL, NULL
                  ,DECODE (
                      INSTR (SUBSTR( NVL (mv.address2, mv.address3),1, 128), '"')
                     ,0, SUBSTR( NVL (mv.address2, mv.address3), 1, 128)
                     ,   '"'
                      || REPLACE (SUBSTR( NVL (mv.address2, mv.address3), 1, 128), '"', '""')
                      || '"'))
                   address2
               ,DECODE (
                   DECODE (mv.address2, NULL, NULL, mv.address3)
                  ,NULL, NULL
                  ,DECODE (
                      INSTR (SUBSTR(mv.address3, 1, 128)
                            ,'"')
                     ,0, SUBSTR(mv.address3, 1, 128)
                     ,   '"'
                      || REPLACE (
                            SUBSTR (mv.address3, 1, 128)
                           ,'"'
                           ,'""')
                      || '"'))
                   address3
               ,SUBSTR( mv.city, 1, 128) city
               ,SUBSTR( mv.state_province, 1, 128) state
               ,SUBSTR( mv.country, 1, 128) country
               ,SUBSTR( mv.postal_code, 1, 40) zipcode
               ,DECODE (NVL(hcp_site.name, hcp_acct.name)
                       ,'COD Customers', 'false'
                       ,DECODE (Nvl(hcp_site.credit_hold, hcp_acct.credit_hold), 'Y', 'false', 'true'))
                   allow_on_account
               ,NULL price_list_name
               ,SUBSTR( mv.cust_unique_identifier, 1, 254) nickname
               ,SUBSTR( mv.main_phone_number, 1, 32) phone1
               ,NULL                               email2 -- Version# 1.72
               ,SUBSTR( mv.main_fax_number, 1, 32) fax1
               -- end  ver 1.50     6-FEB-2013
               ,mv.hcasa_cust_acct_site_id
               , mv.party_id -- Version# 1.74
               , xxwc_speedbuild_ob_pkg.get_parent_party_num(mv.party_id) parent_party_num -- Version# 1.74               
           FROM XXWC.XXWC_AR_CUSTOMERS_MV_TBL mv -- Version# 1.76
               , xxwc.xxwc_sb_qp_temp_tbl sb_tmp
               ,hz_cust_site_uses_all hcsu
               -- begin   ver 1.50  6-Feb-2013
               ,(SELECT hca.account_number
                       ,hcpc.name
                       ,hcp.credit_hold
                       ,hcp.last_update_date
                   FROM hz_cust_accounts hca
                       ,hz_customer_profiles hcp
                       ,hz_cust_profile_classes hcpc
                  WHERE     hcp.profile_class_id = hcpc.profile_class_id
                        AND hcp.cust_account_id = hca.cust_account_id
                        AND hcp.site_use_id IS NULL
                        AND hcp.status = 'A') hcp_acct
               ,(SELECT hca.account_number
                       ,hcp.site_use_id
                       ,hcpc.name
                       ,hcp.credit_hold
                       ,hcp.last_update_date
                   FROM hz_cust_accounts hca
                       ,hz_customer_profiles hcp
                       ,hz_cust_profile_classes hcpc
                  WHERE     hcp.profile_class_id = hcpc.profile_class_id
                        AND hcp.cust_account_id = hca.cust_account_id
                        AND hcp.site_use_id IS NOT NULL
                        AND hcp.status = 'A') hcp_site
                -- end ver 1.50  6-Feb-2013
          WHERE     mv.site_use_code = 'BILL_TO'
                AND mv.cust_account_code = sb_tmp.customer_number
                AND (   mv.hcsua_primary_flag = 'Y'
                     OR NOT EXISTS
                               (SELECT 'x'
                                  FROM hz_cust_acct_sites_all hcas -- handle situation when inactivating primary bill-to
                                      ,hz_cust_accounts hca                  -- ver 1.50  6-Feb-2013
                                 WHERE     mv.cust_account_code = hca.account_number           -- ver 1.50  6-Feb-2013
                                       AND hca.cust_account_id =
                                              hcas.cust_account_id
                                       AND hcas.bill_to_flag = 'P'))
                AND mv.cust_account_code = hcp_acct.account_number(+)       -- ver 1.50  6-Feb-2013
                AND mv.cust_account_code = hcp_site.account_number(+)       -- ver 1.50  6-Feb-2013
                AND mv.site_use_id = hcp_site.site_use_id(+)                -- ver 1.50  6-Feb-2013
                AND mv.site_use_id = hcsu.site_use_id
         UNION
         -- retrieve customer site level information
         SELECT mv.cust_account_code
               ,mv.party_site_number orgentityname
               ,   'o='
                || mv.cust_account_code
                || ',o=whitecap buyer organization,o=buyer organization,o=root organization'
                   parentdn
               -- begin  ver 1.50     6-FEB-2013
               ,NVL (
                   DECODE (
                      INSTR (SUBSTR( mv.site_use_location_name, 1, 512), '"')
                     ,0, SUBSTR( mv.site_use_location_name, 1, 512)
                     ,   '"'
                      || REPLACE (SUBSTR( mv.site_use_location_name, 1, 512), '"', '""')
                      || '"')
                  ,DECODE (
                      INSTR (SUBSTR( mv.cust_rollup_level2_desc, 1, 512), '"')
                     ,0, SUBSTR( mv.cust_rollup_level2_desc, 1, 512)
                     ,   '"'
                      || REPLACE (SUBSTR( mv.cust_rollup_level2_desc, 1, 512), '"', '""')
                      || '"'))
                   description
               ,--           NULL description,
                'OrganizationUnit' orgentitytype
               ,DECODE (mv.hcasa_site_status
                       ,'I', '-1'
                       ,DECODE (hcsu.status, 'A', '0', '-1'))
                   status
               ,NULL administratorlastname
               ,NULL administratorfirstname
               ,NULL administratormiddlename
               ,DECODE (INSTR (SUBSTR( mv.address1, 1, 256), '"')
                       ,0, SUBSTR(mv.address1, 1, 256)
                       ,'"' || REPLACE (SUBSTR(mv.address1, 1, 256), '"', '""') || '"')
                   address1
               ,DECODE (
                   NVL (mv.address2, mv.address3)
                  ,NULL, NULL
                  ,DECODE (
                      INSTR (SUBSTR( NVL (mv.address2, mv.address3),1, 128), '"')
                     ,0, SUBSTR( NVL (mv.address2, mv.address3), 1, 128)
                     ,   '"'
                      || REPLACE (SUBSTR( NVL (mv.address2, mv.address3), 1, 128), '"', '""')
                      || '"'))
                   address2
               ,DECODE (
                   DECODE (mv.address2, NULL, NULL, mv.address3)
                  ,NULL, NULL
                  ,DECODE (
                      INSTR (SUBSTR(mv.address3, 1, 128)
                            ,'"')
                     ,0, SUBSTR(mv.address3, 1, 128)
                     ,   '"'
                      || REPLACE (
                            SUBSTR (mv.address3, 1, 128)
                           ,'"'
                           ,'""')
                      || '"'))
                   address3
               ,SUBSTR( mv.city, 1, 128) city
               ,SUBSTR( mv.state_province, 1, 128) state
               ,SUBSTR( mv.country, 1, 128) country
               ,SUBSTR( mv.postal_code, 1, 40) zipcode
               ,DECODE(hcp_site.name, 'COD Customers', 'false', 
                       DECODE(hcp_acct.name, 'COD Customers', 'false', 
                              DECODE(hcp_site.credit_hold, 'Y', 'false', 
                                     DECODE(hcp_acct.credit_hold,'Y', 'false','true'))))  allow_on_account
--               ,SUBSTR( NVL (site_contracts.name, customer_contracts.name), 1, 64)  -- Version# 1.72
                 ,SUBSTR( NVL (site_contracts.name, NVL(customer_contracts.name, mv.customer_name||'-'||mv.cust_account_code)), 1, 64) -- Version# 1.72
                   price_list_name
--               ,SUBSTR( mv.cust_unique_identifier, 1, 254) nickname             -- ver 1.60  13-FEB-2013
               ,SUBSTR( mv.party_site_number, 1, 254) nickname                    -- ver 1.60  13-FEB-2013
               ,SUBSTR( mv.main_phone_number, 1, 32) phone1
               ,NVL ((SELECT email_address
                        FROM hz_contact_points 
                       WHERE owner_table_name = 'HZ_PARTY_SITES'
                         AND owner_table_id   = mv.party_site_id
                         AND email_format     = 'MAILHTML'
                         AND contact_point_purpose = 'SPEEDBUILD'
                         AND status           = 'A')
                    , (SELECT email_address
                        FROM hz_contact_points 
                       WHERE owner_table_name = 'HZ_PARTY_SITES'
                         AND owner_table_id   = mv_pbt.party_site_id
                         AND email_format     = 'MAILHTML'
                         AND contact_point_purpose = 'SPEEDBUILD'
                         AND status           = 'A')) email2   -- Version# 1.72
               ,SUBSTR( mv.main_fax_number, 1, 32) fax1
               -- end  ver 1.50     6-FEB-2013
               ,mv.hcasa_cust_acct_site_id
               , mv.party_id -- Version# 1.74
               , xxwc_speedbuild_ob_pkg.get_parent_party_num(mv.party_id) parent_party_num -- Version# 1.74               
           FROM XXWC.XXWC_AR_CUSTOMERS_MV_TBL mv   -- Version# 1.76
               , XXWC.XXWC_AR_CUSTOMERS_MV_TBL mv_pbt                  -- Version# 1.72 -- Version# 1.76
               , xxwc.xxwc_sb_qp_temp_tbl sb_tmp
               ,hz_cust_accounts hca
               ,hz_cust_site_uses_all hcsu
               ,(SELECT qq.qualifier_attr_value site_use_id
                      --  , NVL (flv.description, flv.meaning) name -- Version# 1.70
                       , qlh.name name -- Version# 1.70
                       ,GREATEST (qq.last_update_date
                             --   , flv.last_update_date -- Version# 1.70
                                , qlh.last_update_date)
                           last_update_date -- handle scenario when contract added to lookup type / contract created
                         , MAX (QLH.LIST_HEADER_ID) over () hid -- Version# 1.76
                   FROM qp_secu_list_headers_vl qlh
                       ,qp_qualifiers_v qq
                 --      ,fnd_lookup_values flv -- Version# 1.70
                  WHERE     qq.qualifier_context = 'CUSTOMER'
                        AND qq.list_header_id = qlh.list_header_id
                        AND qq.list_line_id = -1     -- header level qualifier
                        AND qlh.list_type_code = 'DLT'
                        AND qlh.active_flag = 'Y'
                        AND TRUNC (SYSDATE) BETWEEN NVL(qlh.start_date_active
                                                        ,SYSDATE - 1)
                                                AND NVL (qlh.end_date_active
                                                        ,SYSDATE + 1)
                        AND qq.qualifier_context = 'CUSTOMER'
                        AND qq.qualifier_attribute = 'QUALIFIER_ATTRIBUTE11' --  ship-to   site_use_id
               AND EXISTS (SELECT  /*+ nl_sj  */  '1' -- Version# 1.76
                     FROM xxwc_om_contract_pricing_hdr xx_csp
                    WHERE 1 = 1
                      -- AND to_char(xx_csp.agreement_id) = qlh.attribute14 -- Version# 1.76
                      AND xx_csp.agreement_id = TO_NUMBER(qlh.attribute14) -- Version# 1.76
                      AND agreement_status = 'APPROVED'
                   )                                       -- Version# 1.70 < End
                        AND EXISTS (SELECT /*+ nl_sj push_pred use_nl(QMS.ql QMS.qpa msi) */ 'x' -- has web-enabled items available
                                      FROM qp_modifier_summary_v qms
                                          ,XXWC.XXWC_QP_ECOMMERCE_ITEMS_MV_TBL msi
                                     WHERE     qms.list_header_id = qlh.list_header_id
                                           AND qms.product_attr = 'PRICING_ATTRIBUTE1' -- 1=item  2=category 3=all
                                           AND TO_NUMBER(qms.product_attr_val) = msi.inventory_item_id
                                           AND qms.list_line_type_code = 'DIS'
                                           AND qms.modifier_level_code = 'LINE'
                                           AND TRUNC (SYSDATE) BETWEEN NVL (
                                                        qms.start_date_active
                                                       ,SYSDATE - 1)
                                                 AND NVL (qms.end_date_active
                                                         ,SYSDATE + 1)
                                   )
-- Version# 1.76 > Start
--                        AND qlh.list_header_id =
--                               (SELECT MAX (qlh.list_header_id) -- handle potential of multiple active contract price lists
--                                  FROM qp_secu_list_headers_vl qlh2
--                                      ,qp_qualifiers_v qq2
--                                      -- ,fnd_lookup_values flv2
--                                 WHERE 1 = 1
--                                       AND qq.qualifier_attr_value = qq2.qualifier_attr_value
--                                       AND qq2.qualifier_context = 'CUSTOMER'
--                                       AND qq2.list_header_id = qlh2.list_header_id
--                                       AND UPPER (qlh2.name) NOT LIKE 'VQN%'
--                                       AND qq2.list_line_id = -1 -- header level qualifier
--                                       AND qlh2.list_type_code = 'DLT'
--                                       AND qlh2.active_flag = 'Y'
--                                       AND TRUNC (SYSDATE) BETWEEN NVL(qlh2.start_date_active
--                                                                     ,  SYSDATE
--                                                                      - 1)
--                                                               AND NVL (
--                                                                      qlh2.end_date_active
--                                                                     ,  SYSDATE
--                                                                      + 1)
--                                       AND qq2.qualifier_context = 'CUSTOMER'
--                                       AND qq2.qualifier_attribute = 'QUALIFIER_ATTRIBUTE11' --  ship-to
--                       AND EXISTS (SELECT /*+ nl_sj  */ '1'  -- Version# 1.76
--                             FROM xxwc_om_contract_pricing_hdr xx_csp
--                            WHERE 1 = 1
--                              -- AND to_char(xx_csp.agreement_id) = qlh2.attribute14 -- Version# 1.76
--                              AND xx_csp.agreement_id = TO_NUMBER(qlh2.attribute14) -- Version# 1.76
--                              AND agreement_status = 'APPROVED'
--                           )                                       -- Version# 1.70 < End
--                                       )
-- Version# 1.76 < End
                                       ) site_contracts
               ,(SELECT qq.qualifier_attr_value cust_account_id
                      -- ,NVL (flv.description, flv.meaning) name
                       , qlh.name -- Version# 1.70
                       , GREATEST (qq.last_update_date
                                 -- , flv.last_update_date -- Version# 1.70
                                 , qlh.last_update_date)
                           last_update_date
                         , MAX (QLH.LIST_HEADER_ID) over () hid -- Version# 1.76
                   FROM qp_secu_list_headers_vl qlh
                       ,qp_qualifiers_v qq
                   --    ,fnd_lookup_values flv
                  WHERE     qq.qualifier_context = 'CUSTOMER'
                        AND qq.list_header_id = qlh.list_header_id
                        AND qq.list_line_id = -1     -- header level qualifier
                        AND qlh.list_type_code = 'DLT'
                        AND qlh.active_flag = 'Y'
                        AND TRUNC (SYSDATE) BETWEEN NVL(qlh.start_date_active
                                                        ,SYSDATE - 1)
                                                AND NVL (qlh.end_date_active
                                                        ,SYSDATE + 1)
                        AND qq.qualifier_context = 'CUSTOMER'
                        AND qq.qualifier_attribute = 'QUALIFIER_ATTRIBUTE32' -- bill-to customer  cust_account_id
               AND EXISTS (SELECT /*+ nl_sj  */ '1' -- Version# 1.76
                     FROM xxwc_om_contract_pricing_hdr xx_csp
                    WHERE 1 = 1
                      -- AND to_char(xx_csp.agreement_id) = qlh.attribute14 -- Version# 1.76
                      AND xx_csp.agreement_id = TO_NUMBER(qlh.attribute14) -- Version# 1.76
                      AND agreement_status = 'APPROVED'
                   )                                       -- Version# 1.70 < End
                        AND UPPER (qlh.name) NOT LIKE 'VQN%'
                        AND EXISTS (SELECT /*+ nl_sj push_pred use_nl(QMS.ql QMS.qpa msi) */ 'x' -- has web-enabled items available -- Version# 1.76
                                      FROM qp_modifier_summary_v qms
                                          ,XXWC.XXWC_QP_ECOMMERCE_ITEMS_MV_TBL msi
                                     WHERE     qms.list_header_id = qlh.list_header_id
                                           AND qms.product_attr = 'PRICING_ATTRIBUTE1' -- 1=item  2=category 3=all
                                           AND TO_NUMBER(qms.product_attr_val) = msi.inventory_item_id
                                           AND qms.list_line_type_code = 'DIS'
                                           AND qms.modifier_level_code = 'LINE'
                                           AND TRUNC (SYSDATE) BETWEEN NVL (
                                                        qms.start_date_active
                                                       ,SYSDATE - 1)
                                                 AND NVL (qms.end_date_active
                                                         ,SYSDATE + 1)
                                   )
-- Version# 1.76 > Start
--                        AND qlh.list_header_id =
--                               (SELECT MAX (qlh2.list_header_id)
--                                  FROM qp_secu_list_headers_vl qlh2
--                                      ,qp_qualifiers_v qq2
--                                --      ,fnd_lookup_values flv2
--                                 WHERE     qq.qualifier_attr_value =
--                                              qq2.qualifier_attr_value
--                                       AND qq2.qualifier_context = 'CUSTOMER'
--                                       AND qq2.list_header_id =
--                                              qlh2.list_header_id
--                                       AND UPPER (qlh2.name) NOT LIKE 'VQN%'
--                                       AND qq2.list_line_id = -1 -- header level qualifier
--                                       AND qlh2.list_type_code = 'DLT'
--                                       AND qlh2.active_flag = 'Y'
--                                       AND TRUNC (SYSDATE) BETWEEN NVL(qlh2.start_date_active
--                                                                     , SYSDATE
--                                                                      - 1)
--                                                               AND NVL (
--                                                                      qlh2.end_date_active
--                                                                     ,  SYSDATE
--                                                                      + 1)
--                                       AND qq2.qualifier_context = 'CUSTOMER'
--                                       AND qq2.qualifier_attribute =
--                                              'QUALIFIER_ATTRIBUTE32' -- bill-to customer  cust_account_id
--                       AND EXISTS (SELECT /*+ nl_sj  */  '1'  -- Version# 1.76
--                             FROM xxwc_om_contract_pricing_hdr xx_csp
--                            WHERE 1 = 1
--                              -- AND to_char(xx_csp.agreement_id) = qlh2.attribute14 -- Version# 1.76
--                              AND xx_csp.agreement_id = to_number(qlh2.attribute14) -- Version# 1.76
--                              AND agreement_status = 'APPROVED'
--                           )                                       -- Version# 1.70 < End
--                                       )
-- Version# 1.76 < End
                                       ) customer_contracts
               -- begin    ver 1.50  6-Feb-2013
               ,(SELECT hca.account_number
                       ,hcpc.name
                       ,hcp.credit_hold
                       ,hcp.last_update_date
                   FROM hz_cust_accounts hca
                       ,hz_customer_profiles hcp
                       ,hz_cust_profile_classes hcpc
                  WHERE     hcp.profile_class_id = hcpc.profile_class_id
                        AND hcp.cust_account_id = hca.cust_account_id
                        AND hcp.site_use_id IS NULL
                        AND hcp.status = 'A') hcp_acct
               ,(SELECT hca.account_number
                       ,hcsu.cust_acct_site_id
                       ,hcpc.name
                       ,hcp.credit_hold
                       ,hcp.last_update_date
                   FROM hz_cust_accounts hca
                       ,hz_cust_site_uses_all hcsu
                       ,hz_customer_profiles hcp
                       ,hz_cust_profile_classes hcpc
                  WHERE     hcp.profile_class_id = hcpc.profile_class_id
                        AND hcp.cust_account_id = hca.cust_account_id
                        AND hcp.site_use_id = hcsu.site_use_id
                        AND hcp.status = 'A') hcp_site
               -- end    ver 1.50  6-Feb-2013
          WHERE     mv.cust_account_code = hca.account_number
                AND mv.cust_account_code = mv_pbt.cust_account_code   -- Version# 1.72
                AND mv_pbt.site_use_code = 'BILL_TO'                  -- Version# 1.72
                and mv_pbt.hcsua_primary_flag = 'Y'                   -- Version# 1.72
                and mv_pbt.hcasa_site_status = 'A'                    -- Version# 1.72
                AND mv.cust_account_code = sb_tmp.customer_number
                AND hca.status = 'A'
                AND hca.cust_account_id = customer_contracts.cust_account_id(+)
                AND mv.site_use_id = site_contracts.site_use_id(+)
                AND mv.site_use_code = 'SHIP_TO'
                AND mv.site_use_id = hcsu.site_use_id
                -- verify primary bill-to exists
                AND EXISTS (SELECT 'x' FROM XXWC.XXWC_AR_CUSTOMERS_MV_TBL mv2 -- Version# 1.76
                             WHERE     mv.cust_account_code = mv2.cust_account_code
                                   AND mv2.site_use_code = 'BILL_TO'
                                   AND mv2.hcsua_primary_flag = 'Y')
                -- retrieve credit profile / credit hold information
                AND mv.cust_account_code = hcp_acct.account_number(+)              -- rev 1.50   6-Feb-2013
                AND mv.cust_account_code = hcp_site.account_number(+)              -- rev 1.50   6-Feb-2013
                AND mv.hcasa_cust_acct_site_id = hcp_site.cust_acct_site_id(+)     -- rev 1.50   6-Feb-2013-- 20131121-00230 < End
         ORDER BY 1
                 ,5
                 ,2
                 ,4;

      l_cnt             NUMBER := 0;
      l_role_cnt        NUMBER := 0;
      l_group_cnt       NUMBER := 0;

      l_ownerdn         VARCHAR2 (240);

      l_err_callfrom    VARCHAR2 (75)
                           := 'XXWC_CUSTOMER_EXTRACT_PKG.Ecomm_Extract_Delta';
      l_err_callpoint   VARCHAR2 (75) := 'START';
      l_distro_list     VARCHAR2 (75)
                           DEFAULT 'HDSOracleDevelopers@hdsupply.com';
      l_message         VARCHAR2 (2000);
   BEGIN
      errbuf := 'Success';
      retcode := 0;

      fnd_file.put_line (fnd_file.LOG
                        ,'Creating Organization File : ' || l_org_file_name);

      l_err_callpoint := 'Before FOPEN of Organization File';
      l_org_outfile := UTL_FILE.fopen (l_directory, l_org_tmp_file_name, 'w');
      l_org_role_outfile :=
         UTL_FILE.fopen (l_directory, l_org_role_tmp_file_name, 'w');
      l_org_role_outfile :=
         UTL_FILE.fopen (l_directory, l_org_role_tmp_file_name, 'w');
      l_org_group_outfile :=
         UTL_FILE.fopen (l_directory, l_org_group_tmp_file_name, 'w');

      -- generate column headings
      UTL_FILE.put_line (l_org_outfile, 'Organization');
      UTL_FILE.put_line (
         l_org_outfile
        ,   'orgEntityName|parentDN|description|orgEntityType|status|administratorLastName|administratorFirstName|'
         || 'administratorMiddleName|address1|address2|address3|city|state|country|zipCode|orgEntityField1|'
--         || 'orgEntityField2|nickName|phone1|fax1');
         || 'orgEntityField2|orgEntityField3|nickName|phone1|email2|fax1');  -- Version# 1.70 -- Version# 1.72

      -- generate column headings
      UTL_FILE.put_line (l_org_role_outfile, 'OrganizationRole');
      UTL_FILE.put_line (l_org_role_outfile, 'orgentityDN|roleName|delete');

      -- generate column headings
      UTL_FILE.put_line (l_org_group_outfile, 'MemberGroupMember');
      UTL_FILE.put_line (
         l_org_group_outfile
        ,'memberGroupName|distinguishedName|excluded|memberType|delete');

      -- Version# 1.70 > Start
      --------------------------------------------------------------------
      -- Truncate Temporary Tables
      --------------------------------------------------------------------
      BEGIN
        EXECUTE IMMEDIATE 'TRUNCATE TABLE xxwc.xxwc_sb_qp_temp_tbl';       
        
        INSERT INTO xxwc.xxwc_sb_qp_temp_tbl (CUSTOMER_ID
                                            , CUSTOMER_NUMBER)
        SELECT distinct hca.cust_account_id customer_id
             , hca.account_number
          FROM oe_order_headers_all  ooha
             , hz_cust_accounts_all  hca
         WHERE 1 =1 
           AND ooha.order_Type_id IN (1162, 1163)
           AND ooha.flow_status_code IN ('PENDING_CUSTOMER_ACCEPTANCE', 'CUSTOMER_ACCEPTED')
           AND hca.cust_account_id = ooha.sold_to_org_id
           AND EXISTS (SELECT '1' 
                         FROM oe_order_lines_all  oola
                        WHERE 1 = 1
                          AND ooha.header_id = oola.header_id
                          AND oola.line_type_id = 1161
                          AND oola.flow_status_code IN ('PENDING_CUSTOMER_ACCEPTANCE', 'CUSTOMER_ACCEPTED')
                          AND trunc(oola.last_update_date) BETWEEN p_last_extract_date AND TRUNC(SYSDATE) + 1)
        UNION
        SELECT distinct hca.cust_account_id
             , hca.account_number
          FROM xxwc_om_contract_pricing_hdr xx_csp
             , hz_cust_accounts_all         hca
         WHERE xx_csp.last_update_date BETWEEN p_last_extract_date AND TRUNC(SYSDATE) + 1
           AND xx_csp.agreement_status = 'APPROVED'
           AND hca.cust_account_id     = xx_csp.customer_id 
           AND EXISTS (SELECT '1' 
                         FROM oe_order_headers_all  ooha 
                        WHERE ooha.order_Type_id IN (1162, 1163)
                          AND ooha.flow_status_code IN ('PENDING_CUSTOMER_ACCEPTANCE', 'CUSTOMER_ACCEPTED')
                          AND hca.cust_account_id = ooha.sold_to_org_id
                           );

        -- Revision# 1.72 > Start
        INSERT INTO xxwc.xxwc_sb_qp_temp_tbl (CUSTOMER_ID
                                            , CUSTOMER_NUMBER)
              SELECT hca.cust_account_id
                   , mv.cust_account_code
                FROM XXWC.XXWC_AR_CUSTOMERS_MV_TBL mv -- Version# 1.76
               , hz_cust_accounts_all hca
               , hz_cust_site_uses_all hcsu
               ,(SELECT hca.account_number
                       ,hcpc.name
                       ,hcp.credit_hold
                       ,hcp.last_update_date
                   FROM hz_cust_accounts hca
                       ,hz_customer_profiles hcp
                       ,hz_cust_profile_classes hcpc
                  WHERE     hcp.profile_class_id = hcpc.profile_class_id
                        AND hcp.cust_account_id = hca.cust_account_id
                        AND hcp.site_use_id IS NULL
                        AND hcp.status = 'A') hcp_acct
               ,(SELECT hca.account_number
                       ,hcp.site_use_id
                       ,hcpc.name
                       ,hcp.credit_hold
                       ,hcp.last_update_date
                   FROM hz_cust_accounts hca
                       ,hz_customer_profiles hcp
                       ,hz_cust_profile_classes hcpc
                  WHERE     hcp.profile_class_id = hcpc.profile_class_id
                        AND hcp.cust_account_id = hca.cust_account_id
                        AND hcp.site_use_id IS NOT NULL
                        AND hcp.status = 'A') hcp_site
               WHERE 1 = 1
--                AND mv.cust_account_code = '66324000'
                AND EXISTS (SELECT '1' FROM oe_order_headers_all ooh WHERE ooh.sold_to_org_id = hca.cust_account_id AND ooh.order_type_id IN (1162, 1163))
                AND hca.account_number   = mv.cust_account_code
                AND mv.cust_account_code = hcp_acct.account_number(+)       
                AND mv.cust_account_code = hcp_site.account_number(+)       
                AND mv.site_use_id       = hcp_site.site_use_id(+)                
                AND mv.site_use_id       = hcsu.site_use_id
                AND (    (   mv.hca_last_update_date   >= p_last_extract_date 
                          OR mv.hcasa_last_update_date >= p_last_extract_date
                          OR mv.hcsua_last_update_date >= p_last_extract_date
                          OR mv.hps_last_update_date   >= p_last_extract_date
                          OR mv.hl_last_update_date    >= p_last_extract_date
--                          OR NVL (site_contracts.last_update_date ,NVL (customer_contracts.last_update_date ,SYSDATE - 30)) >= p_last_extract_date
                          OR hcp_site.last_update_date >= p_last_extract_date
                          OR hcp_acct.last_update_date >= p_last_extract_date
                          OR EXISTS (SELECT 'x'
                                       FROM hz_cust_acct_sites_all hcas
                                      WHERE hca.cust_account_id = hcas.cust_account_id
                                        AND hcas.status = 'A'
                                        AND hcas.bill_to_flag = 'P'
                                        AND hcas.creation_date >= p_last_extract_date -- handles creation of bill-to that allows org/org-unit extract
                                                                  ))
                     AND -- eliminate any inactive sites/uses that didnt change at that level
                         NOT (   (mv.hcsua_last_update_date < p_last_extract_date AND hcsu.status = 'I')
                              OR (mv.hcasa_last_update_date < p_last_extract_date AND mv.hcasa_site_status = 'I')));
        -- Revision# 1.72 < End

        l_err_callpoint := 'Insert into XXWC.XXWC_SB_QP_TEMP_TBL 1.3 - ';
        -- Version# 1.73 > Start
        INSERT INTO xxwc.xxwc_sb_qp_temp_tbl (CUSTOMER_ID
                                            , CUSTOMER_NUMBER)
        SELECT distinct hca.cust_account_id
                      , hca.account_number
          FROM hz_contact_points    hcp
             , hz_party_sites       hps
             , hz_cust_accounts_all hca
         WHERE hcp.owner_table_name       = 'HZ_PARTY_SITES'
           AND hcp.owner_table_id         = hps.party_site_id
           AND hps.party_id               = hca.party_id
           AND hcp.email_format           = 'MAILHTML'
           AND hcp.contact_point_purpose  = 'SPEEDBUILD'
           AND hcp.status                 = 'A'           
           AND hcp.last_update_date      >= p_last_extract_date
           AND EXISTS (SELECT '1' FROM oe_order_headers_all ooh WHERE ooh.sold_to_org_id = hca.cust_account_id AND ooh.order_type_id IN (1162, 1163))
           AND NOT EXISTS (SELECT '1' 
                             FROM xxwc.xxwc_sb_qp_temp_tbl stg 
                            WHERE stg.customer_id        = hca.cust_account_id 
                              AND stg.customer_number    = hca.account_number);
         -- Version# 1.73 < End

      EXCEPTION 
      WHEN OTHERS THEN
         NULL;
      END;
      -- Version# 1.70 < End

      FOR r_cust IN c_organization
      LOOP
         l_cnt := l_cnt + 1;

         -- generate organization data
         UTL_FILE.put_line (
            l_org_outfile
           ,   r_cust.orgentityname
            || '|'
            || r_cust.parentdn
            || '|'
            || r_cust.description
            || '|'
            || r_cust.orgentitytype
            || '|'
            || r_cust.status
            || '|'
            || r_cust.administratorlastname
            || '|'
            || r_cust.administratorfirstname
            || '|'
            || r_cust.administratormiddlename
            || '|'
            || r_cust.address1
            || '|'
            || r_cust.address2
            || '|'
            || r_cust.address3
            || '|'
            || r_cust.city
            || '|'
            || r_cust.state
            || '|'
            || r_cust.country
            || '|'
            || r_cust.zipcode
            || '|'
            || r_cust.price_list_name                       -- orgEntityField1
            || '|'
            || r_cust.allow_on_account                      -- orgEntityField2
            || '|'
--            || 'WC_'||r_cust.orgEntityName                  -- orgEntityField3  -- Version# 1.70
            || 'WC_'||r_cust.parent_party_num               -- orgEntityField3 -- Version# Version# 1.74
            || '|'
            || r_cust.nickname
            || '|'
            || r_cust.phone1
            || '|'
            || r_cust.email2 -- Version# 1.72
            || '|'
            || r_cust.fax1);

         -- create role record for each organization / organization unit
         IF r_cust.orgentitytype = 'Organization'
         THEN
            l_ownerdn :=
               LOWER ('o=' || r_cust.orgentityname || ',' || r_cust.parentdn);
            UTL_FILE.put_line (
               l_org_role_outfile
              ,   l_ownerdn
               || '|'
               || 'Buyer Administrator|'
               || ABS (TO_NUMBER (r_cust.status)));
         ELSE
            l_ownerdn :=
               LOWER (
                  'ou=' || r_cust.orgentityname || ',' || r_cust.parentdn);
            UTL_FILE.put_line (
               l_org_role_outfile
              ,   l_ownerdn
               || '|'
               || 'Organization Participant|'
               || ABS (TO_NUMBER (r_cust.status)));
         END IF;

         l_role_cnt := l_role_cnt + 1;


         -- create group member records
         UTL_FILE.put_line (
            l_org_group_outfile
           ,   'RegisteredCustomers'
            || '|'
            || l_ownerdn
            || '|'
            || 'false|organization|'
            || ABS (TO_NUMBER (r_cust.status)));
         l_group_cnt := l_group_cnt + 1;
      END LOOP;

      -- close data files
      UTL_FILE.fclose (l_org_outfile);
      UTL_FILE.fclose (l_org_role_outfile);
      UTL_FILE.fclose (l_org_group_outfile);

      fnd_file.put_line (fnd_file.LOG
                        ,l_cnt || ' organization records generated');
      fnd_file.put_line (
         fnd_file.LOG
        ,l_role_cnt || ' organization role records generated');
      fnd_file.put_line (
         fnd_file.LOG
        ,l_group_cnt || ' organization member-group-member records generated');

      fnd_file.put_line (
         fnd_file.LOG
        ,   'Renaming organization file '
         || l_org_tmp_file_name
         || ' => '
         || l_org_file_name);
      UTL_FILE.frename (src_location    => l_directory
                       ,src_filename    => l_org_tmp_file_name
                       ,dest_location   => l_directory
                       ,dest_filename   => l_org_file_name
                       ,overwrite       => TRUE);

      fnd_file.put_line (
         fnd_file.LOG
        ,   'Renaming organization role file '
         || l_org_role_tmp_file_name
         || ' => '
         || l_org_role_file_name);
      UTL_FILE.frename (src_location    => l_directory
                       ,src_filename    => l_org_role_tmp_file_name
                       ,dest_location   => l_directory
                       ,dest_filename   => l_org_role_file_name
                       ,overwrite       => TRUE);

      fnd_file.put_line (
         fnd_file.LOG
        ,   'Renaming organization group file '
         || l_org_group_tmp_file_name
         || ' => '
         || l_org_group_file_name);
      UTL_FILE.frename (src_location    => l_directory
                       ,src_filename    => l_org_group_tmp_file_name
                       ,dest_location   => l_directory
                       ,dest_filename   => l_org_group_file_name
                       ,overwrite       => TRUE);

      fnd_file.put_line (fnd_file.LOG, 'Process Complete');
   EXCEPTION
      WHEN OTHERS
      THEN
         errbuf := 'Error';
         retcode := 2;
         fnd_file.put_line (fnd_file.LOG, SQLERRM);

         IF UTL_FILE.is_open (l_org_outfile)
         THEN
            UTL_FILE.fclose (l_org_outfile);
         END IF;

         IF UTL_FILE.is_open (l_org_role_outfile)
         THEN
            UTL_FILE.fclose (l_org_role_outfile);
         END IF;

         IF UTL_FILE.is_open (l_org_group_outfile)
         THEN
            UTL_FILE.fclose (l_org_group_outfile);
         END IF;

         xxcus_error_pkg.xxcus_error_main_api (
            p_called_from         => l_err_callfrom
           ,p_calling             => l_err_callpoint
           ,p_ora_error_msg       => SQLERRM
           ,p_error_desc          => l_message
           ,p_distribution_list   => l_distro_list
           ,p_module              => 'XXWC');
   END ecomm_extract_delta;

/********************************************************************************
ProcedureName : ECOMM_EXTRACT
Purpose       : Wrapper API to generate Organization/Customer Extract
HISTORY
===============================================================================
VERSION DATE          AUTHOR(S)       DESCRIPTION
------- -----------   --------------- -----------------------------------------
1.74    27-MAY-2014   Gopi Damuluri   TMS# 20140529-00334 Catalog Filter at Master Party Level
1.75    14-JAN-2015   Gopi Damuluri   TMS# 20150114-00119 Add Trace to EComm Customer Extract Process
1.77    28-May-2015   Pattabhi Avula  TMS# 20150521-00030 Commented trace scripts to EComm Customer Extract Process
********************************************************************************/
   PROCEDURE ecomm_extract (
      errbuf                   OUT VARCHAR2
     ,retcode                  OUT VARCHAR2
     ,p_extract_method      IN     VARCHAR2 DEFAULT 'DELTA'
     ,p_last_extract_date   IN     VARCHAR2)
   IS
      l_last_extract_date   DATE
         := TRUNC (
               NVL (fnd_date.canonical_to_date (p_last_extract_date)
                   ,SYSDATE - 3));

      l_errbuf              VARCHAR2 (80);
      l_retcode             VARCHAR2 (80);

      l_err_callfrom        VARCHAR2 (75)
                               := 'XXWC_CUSTOMER_EXTRACT_PKG.Ecomm_Extract';
      l_err_callpoint       VARCHAR2 (75) := 'START';
      l_distro_list         VARCHAR2 (75)
                               DEFAULT 'HDSOracleDevelopers@hdsupply.com';
      l_message             VARCHAR2 (2000);
   BEGIN
      errbuf := 'Success';
      retcode := 0;
/* Version# 1.77 Starts here -- Commented trace scripts by pattabhi for TMS#20150521-00030
EXECUTE IMMEDIATE 'alter session set sql_trace=true'; -- Version# 1.75
EXECUTE IMMEDIATE 'alter session set events ''10046 trace name context forever, level 12'''; -- Version# 1.75
EXECUTE IMMEDIATE 'ALTER SESSION SET tracefile_identifier=ECommCust'; -- Version# 1.75
Version# 1.77 Ends here -- Commented trace scripts by pattabhi for TMS#20150521-00030  */

      fnd_file.put_line (fnd_file.LOG
                        ,'P_EXTRACT_METHOD = ' || p_extract_method);
      fnd_file.put_line (fnd_file.LOG
                        ,'P_LAST_EXTRACT_DATE = ' || p_last_extract_date);

      -- Version# 1.76 > Start
      BEGIN
        EXECUTE IMMEDIATE 'TRUNCATE TABLE XXWC.XXWC_AR_CUSTOMERS_MV_TBL';
        INSERT INTO XXWC.XXWC_AR_CUSTOMERS_MV_TBL SELECT * FROM APPS.XXWC_AR_CUSTOMERS_MV;
        COMMIT;
        DBMS_STATS.gather_index_stats (ownname => 'XXWC', indname => 'XXWC_AR_CUSTOMERS_MV_N1');
      EXCEPTION
      WHEN OTHERS THEN
         fnd_file.put_line (fnd_file.LOG,'Error creating AR Cust Temporary Table ' || SQLERRM);
      END;

      BEGIN
        EXECUTE IMMEDIATE 'TRUNCATE TABLE XXWC.XXWC_QP_ECOMMERCE_ITEMS_MV_TBL';
        INSERT INTO XXWC.XXWC_QP_ECOMMERCE_ITEMS_MV_TBL SELECT * FROM APPS.XXWC_QP_ECOMMERCE_ITEMS_MV;
        COMMIT;
        DBMS_STATS.gather_index_stats (ownname => 'XXWC', indname => 'XXWC_QP_ECOMMERCE_ITEMS_MV_N1');
      EXCEPTION
      WHEN OTHERS THEN
         fnd_file.put_line (fnd_file.LOG,'Error creating QP EComm Temporary Table ' || SQLERRM);
      END;
      -- Version# 1.76 < End

      -- split logic into seperate procedures for performance and to simplify the queries  
      IF p_extract_method = 'FULL'
      THEN
         ecomm_extract_full (l_errbuf, l_retcode);
      ELSE
         ecomm_extract_delta (l_errbuf, l_retcode, l_last_extract_date);
      END IF;


      errbuf := l_errbuf;
      retcode := l_retcode;
   EXCEPTION
      WHEN OTHERS
      THEN
         errbuf := 'Error';
         retcode := 2;
         fnd_file.put_line (fnd_file.LOG, SQLERRM);

         IF UTL_FILE.is_open (l_org_outfile)
         THEN
            UTL_FILE.fclose (l_org_outfile);
         END IF;

         IF UTL_FILE.is_open (l_org_role_outfile)
         THEN
            UTL_FILE.fclose (l_org_role_outfile);
         END IF;

         IF UTL_FILE.is_open (l_org_group_outfile)
         THEN
            UTL_FILE.fclose (l_org_group_outfile);
         END IF;

         xxcus_error_pkg.xxcus_error_main_api (
            p_called_from         => l_err_callfrom
           ,p_calling             => l_err_callpoint
           ,p_ora_error_msg       => SQLERRM
           ,p_error_desc          => l_message
           ,p_distribution_list   => l_distro_list
           ,p_module              => 'XXWC');
   END ecomm_extract;
   
procedure ecomm_acct_request(X_ERRBUF      OUT VARCHAR2,
                             X_RETCODE     OUT VARCHAR2,
							 p_session_id  IN VARCHAR2)
IS
/******************************************************************************************************
-- Object Name: ecomm_acct_request
--
-- Object Type: Procedure
--
-- PURPOSE: Procedure will use to Implement Automated Account Linking through File Upload Process
--          for APEX
-- HISTORY
-- ==========================================================================================================
-- ==========================================================================================================
-- VERSION DATE          AUTHOR(S)       DESCRIPTION
-- ------- -----------   --------------- --------------------------------------------------------------------
-- 1.0     18-JAN-2017   Pattabhi Avula  TMS#20160915-00144 - Implement Automated Account Linking through File 
--                                       Upload Process
--                                       Initial version
   ************************************************************************************************************/
      l_err_callfrom    VARCHAR2 (75)
                           := 'XXWC_CUSTOMER_EXTRACT_PKG.ecomm_acct_request';
      l_err_callpoint   VARCHAR2 (150) := 'START';
      l_distro_list     VARCHAR2 (75)
                           DEFAULT 'HDSOracleDevelopers@hdsupply.com';
      l_message         VARCHAR2 (2000);
	  
	  l_ecomm_admin_file_name             VARCHAR2 (80);
	  l_ecomm_admin_outfile               UTL_FILE.file_type;
	  l_admin_directory                 VARCHAR2 (80) := 'XXWC_WCS_LINK_ACCT_DIR';
	  
	  -- Program local variables
	  l_cust_num        VARCHAR2(100);
	  l_cust_mail       VARCHAR2(100);
	  
	  -- DB name extract local variables
	  l_db_name       VARCHAR2 (20) := NULL;
      l_path          VARCHAR2 (240) := NULL;
      l_sql           VARCHAR2 (240) := NULL;
	  
	  CURSOR c_ecomm_cust_dtls(p_cust_num VARCHAR2)
	  IS
	   SELECT  hps.party_site_number
       FROM    hz_cust_accounts hca,
               hz_cust_acct_sites_all hcas,
               hz_cust_site_uses_all hcsu,
               hz_party_sites hps     
       WHERE   hca.cust_account_id= hcas.cust_account_id
       AND     hcas.party_site_id=hps.party_site_id
       AND     hcas.cust_acct_site_id=hcsu.cust_acct_site_id
       AND     hcsu.site_use_code='SHIP_TO'
       AND     hcsu.status='A'
	   AND     hca.account_number=p_cust_num;
	  -- For avoiding the duplicate records
      CURSOR cur_session
      IS
       SELECT DISTINCT session_id,
	                   customer_number,
	                   customer_email	 
     FROM   xxwc.xxwc_ecomm_email_stg_tbl 
	 WHERE  SESSION_ID=p_session_id;	  

BEGIN
  BEGIN
    SELECT LOWER (name) INTO l_db_name FROM v$database;

   l_path:= '/xx_iface/'||l_db_name ||'/outbound/wcs/person';
   

   l_sql :=
         'CREATE OR REPLACE DIRECTORY XXWC_WCS_LINK_ACCT_DIR as'
      || ' '
      || ''''
      || l_path
      || '''';

   fnd_file.put_line (fnd_file.LOG,'DBA Directory Path: ' || l_path);
   fnd_file.put_line (fnd_file.LOG,' ');
   fnd_file.put_line (fnd_file.LOG,'SQL: ' || l_sql);
   fnd_file.put_line (fnd_file.LOG,' ');
   fnd_file.put_line (fnd_file.LOG,
      'Begin setup of directory XXWC_CASS_INBOUND_WKLY_IFACE');

   EXECUTE IMMEDIATE l_sql;

   fnd_file.put_line (fnd_file.LOG,
      'End setup of directory XXWC_CASS_INBOUND_WKLY_IFACE');
  l_err_callpoint:='Before fetching the data from Apex database';
  -- Fetching the data from wcapex prod database
	INSERT INTO  xxwc.xxwc_ecomm_email_stg_tbl(ID             
                                           ,SESSION_ID     
                                           ,CUSTOMER_NUMBER
                                           ,CUSTOMER_EMAIL 
                                           ,STATUS         
                                           ,CREATED_BY     
                                           ,CREATED_ON
					                      )
										  SELECT ID             
                                                 ,SESSION_ID     
                                                 ,CUSTOMER_NUMBER
                                                 ,CUSTOMER_EMAIL 
                                                 ,STATUS         
                                                 ,CREATED_BY     
                                                 ,CREATED_ON
										  FROM WC_APPS.XXWC_ECOMM_EMAIL_STG_TBL@WCAPXPRD.HSI.HUGHESSUPPLY.COM
	                                      WHERE session_id=p_session_id;
	COMMIT;									  
  EXCEPTION
    WHEN OTHERS THEN
	  UPDATE xxwc.xxwc_ecomm_email_stg_tbl
     SET status     = 'Error - Invalid Record'
 WHERE session_id = p_session_id;

  UPDATE wc_apps.xxwc_ecomm_email_stg_tbl@wcapxprd.hsi.hughessupply.com
   SET status     = 'Error - Invalid Record'
 WHERE session_id = p_session_id;

COMMIT;
   
	l_err_callpoint:='Exception occurred while fetching the data from Apex database';
    NULL;
  END;
  
    -- File name
	l_ecomm_admin_file_name :='AdminRole_'||TO_CHAR(SYSDATE,'YYYYMMDD_HH24MISS')||'.csv'; ----changed by Rakesh Patel on 2/1/2017
	-- Loop starting to fetch the site numbers
	l_ecomm_admin_outfile := UTL_FILE.fopen (l_admin_directory, l_ecomm_admin_file_name, 'w');
	
	UTL_FILE.put_line (l_ecomm_admin_outfile, 'AdminRole|||');
	UTL_FILE.put_line (l_ecomm_admin_outfile, 'logonId|roleName|orgentityDN|registrationType');

   FOR H IN cur_session LOOP	
   FOR I IN c_ecomm_cust_dtls(H.customer_number) LOOP
     
   UTL_FILE.put_line (l_ecomm_admin_outfile,
                      H.customer_email
                      || '|Organization Participant|ou='
					  || LOWER(I.party_site_number) --changed by Rakesh Patel on 2/1/2017
					  ||','
					  ||'o='
					  ||H.customer_number
					  ||','
					  ||'o=whitecap buyer organization,o=buyer organization,o=root organization|R'
					  ); 
  END LOOP;
  END LOOP; -- CLOSING THE LOOP FOR H CURSOR
    UTL_FILE.fclose (l_ecomm_admin_outfile);

UPDATE xxwc.xxwc_ecomm_email_stg_tbl
   SET status     = 'Pending Upload'
 WHERE session_id = p_session_id;

UPDATE wc_apps.xxwc_ecomm_email_stg_tbl@wcapxprd.hsi.hughessupply.com
   SET status     = 'Pending Upload'
 WHERE session_id = p_session_id;

COMMIT;

EXCEPTION
  WHEN OTHERS THEN  
  
  UPDATE xxwc.xxwc_ecomm_email_stg_tbl
   SET status     = 'Error - Creating file'
 WHERE session_id = p_session_id;

  UPDATE wc_apps.xxwc_ecomm_email_stg_tbl@wcapxprd.hsi.hughessupply.com
   SET status     = 'Error - Creating file'
 WHERE session_id = p_session_id;

COMMIT;
   
       xxcus_error_pkg.xxcus_error_main_api (
          p_called_from         => l_err_callfrom
         ,p_calling             => l_err_callpoint
         ,p_ora_error_msg       => SQLERRM
         ,p_error_desc          => l_message
         ,p_distribution_list   => l_distro_list
         ,p_module              => 'XXWC');
END ecomm_acct_request;  

 /*************************************************************************
      PROCEDURE Name: submit_job

      PURPOSE:   To submit Ecomm Account link from APEX

      REVISIONS:
      Ver        Date        Author                     Description
      ---------  ----------  ---------------         -------------------------
      1.0        23/01/2017  Pattabhi Avula            Initial Version
                                                       TMS#20160915-00144
  ****************************************************************************/
   PROCEDURE submit_job (p_session_id          IN     VARCHAR2)
   IS
      ----------------------------------------------------------------------------------
      -- Variable definitions
      ----------------------------------------------------------------------------------
      l_user_name             VARCHAR2(150) := 'XXWC_INT_SALESFULFILLMENT';
      l_responsibility_name   VARCHAR2(150) := 'HDS Order Mgmt Super User - WC';


      l_req_id                NUMBER NULL;
      l_phase                 VARCHAR2 (50);
      l_status                VARCHAR2 (50);
      l_dev_status            VARCHAR2 (50);
      l_dev_phase             VARCHAR2 (50);
      l_message               VARCHAR2 (250);
      l_error_message         VARCHAR2 (3000);
      l_err_msg               VARCHAR2 (3000);
      l_err_code              NUMBER;
      l_statement             VARCHAR2 (9000);
      l_user_id               NUMBER;
      l_responsibility_id     NUMBER;
      l_resp_application_id   NUMBER;
      l_instance              VARCHAR2 (50);
      l_apex_url              VARCHAR2 (500);

      l_err_callfrom          VARCHAR2 (75) DEFAULT 'XXCUS_ERROR_PKG';
      l_err_callpoint         VARCHAR2 (75) DEFAULT 'START';
      l_distro_list           VARCHAR2 (75)
                                 DEFAULT 'HDSOracleDevelopers@hdsupply.com';
      l_sec                   VARCHAR2 (100);
	  
	   PRAGMA AUTONOMOUS_TRANSACTION;
   BEGIN
      l_sec := 'Derive UserId';

      ----------------------------------------------------------------------------------
      -- Derive UserId
      ----------------------------------------------------------------------------------
      BEGIN
         SELECT user_id
           INTO l_user_id
           FROM fnd_user
          WHERE     user_name = l_user_name
                AND SYSDATE BETWEEN start_date
                                AND NVL (end_date, TRUNC (SYSDATE) + 1);
      EXCEPTION
         WHEN NO_DATA_FOUND
         THEN
            l_err_msg :=
               'UserName ' || l_user_name || ' not defined in Oracle';
            RAISE PROGRAM_ERROR;
         WHEN OTHERS
         THEN
            l_err_msg :=
               'Error deriving user_id for UserName - ' || l_user_name;
            RAISE PROGRAM_ERROR;
      END;
    
      
      l_sec := 'Derive ResponsibilityId and ApplicationId';

      ----------------------------------------------------------------------------------
      -- Derive ResponsibilityId and ApplicationId
      ----------------------------------------------------------------------------------
      BEGIN
         SELECT responsibility_id, application_id
           INTO l_responsibility_id, l_resp_application_id
           FROM fnd_responsibility_vl
          WHERE     responsibility_name = l_responsibility_name
                AND SYSDATE BETWEEN start_date
                                AND NVL (end_date, TRUNC (SYSDATE) + 1);
      EXCEPTION
         WHEN NO_DATA_FOUND
         THEN
            l_err_msg :=
                  'Responsibility '||
              l_responsibility_name
               || ' not defined in Oracle';
            RAISE PROGRAM_ERROR;
         WHEN OTHERS
         THEN
            l_err_msg :=
                  'Error deriving Responsibility_id for '
               || l_responsibility_name;
            RAISE PROGRAM_ERROR;
      END;

      l_sec := 'Apps Initialize';
      ----------------------------------------------------------------------------------
      -- Apps Initialize
      ----------------------------------------------------------------------------------
      FND_GLOBAL.APPS_INITIALIZE (l_user_id,
                                  l_responsibility_id,
                                  l_resp_application_id);

      l_sec :=
         'Submitting program XXWC B2B Inbound SO Interface';
      ----------------------------------------------------------------------------------
      -- Submitting program XXWC EComm Account Linking Program
      ----------------------------------------------------------------------------------
      l_req_id :=
         fnd_request.submit_request (
            application   => 'XXWC',
            program       => 'XXWC_ECOMM_APEX_ACCT_LINK',
            description   => NULL,
            start_time    => SYSDATE,
            sub_request   => FALSE,
            argument1     => p_session_id);

      COMMIT;

    --  p_errbuf := l_req_id;

      fnd_file.put_line (
         fnd_file.LOG,
         'Concurrent Program Request Submitted. Request ID: ' || l_req_id);

      IF (l_req_id != 0)
      THEN
         IF fnd_concurrent.wait_for_request (l_req_id,
                                             6,
                                             3600 -- 04/17/2012 Changed from 15000
                                                 ,
                                             l_phase,
                                             l_status,
                                             l_dev_phase,
                                             l_dev_status,
                                             l_message)
         THEN
            l_error_message :=
                  'ReqID:'
               || l_req_id
               || '-DPhase:'
               || l_dev_phase
               || '-DStatus:'
               || l_dev_status
               || CHR (10)
               || 'MSG:'
               || l_message;

            -- Error Returned
            IF l_dev_phase != 'COMPLETE' OR l_dev_status != 'NORMAL'
            THEN
               l_statement :=
                     'EBS Conc Prog Completed with problems '
                  || l_error_message
                  || '.';
               l_err_msg := l_statement;
               fnd_file.put_line (fnd_file.LOG, l_statement);
               RAISE PROGRAM_ERROR;
            --            ELSE
            --                 p_retcode := 1;
            END IF;
         ELSE
            l_statement := 'EBS Conc Program Wait timed out';
            l_err_msg := l_statement;
            fnd_file.put_line (fnd_file.LOG, l_statement);
            RAISE PROGRAM_ERROR;
         END IF;
      ELSE
         l_statement := 'EBS Conc Program not initated';
         l_err_msg := l_statement;
         fnd_file.put_line (fnd_file.LOG, l_statement);
         RAISE PROGRAM_ERROR;
      END IF;
   EXCEPTION
      WHEN PROGRAM_ERROR
      THEN
         ROLLBACK;
         l_err_code := 2;
         l_err_msg :=
            SUBSTR ( (l_err_msg || ' ERROR ' || SUBSTR (SQLERRM, 1, 2000)),
                    1,
                    3000);
         fnd_file.put_line (fnd_file.LOG, l_err_msg);

         xxcus_error_pkg.xxcus_error_main_api (
            p_called_from         => 'XXWC_CUSTOMER_EXTRACT_PKG.SUBMIT_JOB',
            p_calling             => l_sec,
            p_request_id          => fnd_global.conc_request_id,
            p_ora_error_msg       => SUBSTR (SQLERRM, 1, 2000),
            p_error_desc          => l_err_msg,
            p_distribution_list   => l_distro_list,
            p_module              => 'XXWC');
      --   p_retcode := l_err_code;
       --  p_errbuf := l_err_msg;
      WHEN OTHERS
      THEN
         l_err_code := 2;
         l_err_msg :=
            SUBSTR ( (l_err_msg || ' ERROR ' || SUBSTR (SQLERRM, 1, 2000)),
                    1,
                    3000);
         fnd_file.put_line (fnd_file.LOG, l_err_msg);

         xxcus_error_pkg.xxcus_error_main_api (
            p_called_from         => 'XXWC_CUSTOMER_EXTRACT_PKG.SUBMIT_JOB',
            p_calling             => l_sec,
            p_request_id          => fnd_global.conc_request_id,
            p_ora_error_msg       => SUBSTR (SQLERRM, 1, 2000),
            p_error_desc          => 'Error in Ecom Account link SUBMIT_JOB procedure, When Others Exception',
            p_distribution_list   => l_distro_list,
            p_module              => 'XXWC');

       --  p_retcode := l_err_code;
        -- p_errbuf := l_err_msg;
       ROLLBACK;
         fnd_file.put_line (
            fnd_file.LOG,
            'Error in  submit job for Ecomm Account link program' || SQLERRM);
   END submit_job;     
END XXWC_CUSTOMER_EXTRACT_PKG;
/