CREATE TABLE  XXWC.XXWC_ECOMM_EMAIL_STG_TBL
/* Ver        Date         Author           Description
   ---------  -----------  ---------------  ------------------------------------
   1.0        02-FEB-2017  Pattabhi Avula   TMS#20160915-00144 - Implement Automated Account Linking through File 
--                                          Upload Process
                                            Initial Version   */
(ID               NUMBER(20)  NOT NULL, 
 SESSION_ID               VARCHAR2(40), 
 CUSTOMER_NUMBER          NUMBER(20),   
 CUSTOMER_EMAIL           VARCHAR2(40), 
 STATUS                   VARCHAR2(40), 
 CREATED_BY               VARCHAR2(20), 
 CREATED_ON               DATE)
/