SET SERVEROUTPUT ON SIZE 1000000

/*************************************************************************
      PURPOSE:  Stuck Average cost update records in interface table. 

      REVISIONS:
      Ver        Date          Author             Description
      ---------  ------------  ------------       ------------------
      1.0        17-APR-2018  Ram Talluri     Initial Version - TMS #20180416-00456  
	************************************************************************/ 

DECLARE
   v_count   NUMBER := 0;

   CURSOR mti_delete_acu_recs
   IS
      SELECT mt.transaction_interface_id,
             mt.organization_id,
             mt.inventory_item_id,
             msi.segment1,
             ood.organization_code
        FROM apps.mtl_transactions_interface mt,
             apps.org_organization_definitions ood,
             apps.mtl_system_items_b msi
       WHERE     source_code = 'ACU'
             AND ERROR_CODE IS NOT NULL
             AND mt.organization_id = ood.organization_id
             AND msi.inventory_item_id = mt.inventory_item_id
             AND msi.organization_id = mt.organization_id
             --and ood.organization_code IN ('381','106')
             AND process_flag = '3'
             AND (   EXISTS
                        (SELECT 1
                           FROM cst_item_costs c
                          WHERE     inventory_item_id = mt.inventory_item_id
                                AND c.organization_id = mt.organization_id
                                AND item_cost > 0
                                AND cost_type_id = 2)
                  OR EXISTS
                        (SELECT 1
                           FROM mtl_onhand_quantities_detail d
                          WHERE     inventory_item_id = mt.inventory_item_id
                                AND d.organization_id = mt.organization_id));

   CURSOR mti_recs
   IS
      SELECT mt.transaction_interface_id,
             mt.organization_id,
             mt.inventory_item_id,
             msi.segment1,
             ood.organization_code
        FROM mtl_transactions_interface mt,
             org_organization_definitions ood,
             mtl_system_items_b msi
       WHERE     source_code = 'ACU'
             AND ERROR_CODE ='Account period'
             AND mt.organization_id = ood.organization_id
             AND msi.inventory_item_id = mt.inventory_item_id
             AND msi.organization_id = mt.organization_id
             --and ood.organization_code IN ('381','106')
             AND EXISTS
                    (SELECT 1
                       FROM cst_item_costs c
                      WHERE     inventory_item_id = mt.inventory_item_id
                            AND c.organization_id = mt.organization_id
                            AND item_cost = 0
                            AND cost_type_id = 2)
             AND NOT EXISTS
                    (SELECT 1
                       FROM mtl_onhand_quantities_detail d
                      WHERE     inventory_item_id = mt.inventory_item_id
                            AND d.organization_id = mt.organization_id)
             AND process_flag = '3';
BEGIN
   DBMS_OUTPUT.put_line ('TMS: 20180416-00456  , Script 1 -Before Delete');

   FOR i IN mti_delete_acu_recs
   LOOP
      BEGIN
         DELETE FROM mtl_transactions_interface
               WHERE     1 = 1
                     AND process_flag = 3
                     AND transaction_interface_id =
                            i.transaction_interface_id
                     AND source_code = 'ACU'
                     AND inventory_item_id = i.inventory_item_id;

         v_count := v_count + 1;
      EXCEPTION
         WHEN OTHERS
         THEN
            DBMS_OUTPUT.PUT_LINE (
                  'Error in deleting average cost MTI for transaction_interface_id-'
               || i.transaction_interface_id
               || SQLERRM);
      END;
   END LOOP;

   COMMIT;

   DBMS_OUTPUT.put_line (
         'TMS: 20180416-00456, Script 1 -After delete, rows deleted: '
      || v_count);


   v_count := 0;

   FOR j IN mti_recs
   LOOP
      BEGIN
         UPDATE mtl_transactions_interface
            SET process_flag = 1,
                lock_flag = 2,
                transaction_mode = 3,
                validation_required = 1,
                ERROR_CODE = NULL,
                error_explanation = NULL,
                request_id = NULL,
                transaction_date = SYSDATE
          WHERE     1 = 1
                AND process_flag = 3
                AND transaction_interface_id = j.transaction_interface_id
                AND source_code = 'ACU'
                AND inventory_item_id = j.inventory_item_id;

         v_count := v_count + 1;
      EXCEPTION
         WHEN OTHERS
         THEN
            DBMS_OUTPUT.PUT_LINE (
                  'Error in updating average cost_to MTI for transaction_interface_id-'
               || j.transaction_interface_id
               || SQLERRM);
      END;
   END LOOP;

   COMMIT;

   DBMS_OUTPUT.put_line (
         'TMS: 20180416-00456, Script 1 -After update, rows updated: '
      || v_count);
EXCEPTION
   WHEN OTHERS
   THEN
      DBMS_OUTPUT.PUT_LINE ('Error is' || SQLERRM);
END;
/