CREATE OR REPLACE PACKAGE APPS.XXWC_AHH_INTF_STATS_PKG AS
/************************************************************************************************************************************
   $Header XXWC_AHH_INTF_STATS_PKG $
   Module Name: XXWC_AHH_INTF_STATS_PKG.pks

   PURPOSE:   This package is called by the concurrent programs
              To sed email 
   REVISIONS:
   Ver        Date        Author             Description
   ---------  ----------  ---------------   -------------------------------------------------------------------------------------
    1.0       08/07/2018  P.Vamshidhar       Initial Build - Task ID:TMS#20180803-00026 - AH Harries Interface Stats Automation
/********************************************************************************************************************************/
p_start_date DATE;

FUNCTION afterReport RETURN BOOLEAN;

FUNCTION beforereport RETURN BOOLEAN;

END XXWC_AHH_INTF_STATS_PKG;
/