--Report Name            : Cash Drawer Exception Report
--Import Instructions : 1. If RTF template needs to be uploaded to report automatically then move the RTF template to physical path of APPS_DATA_FILE_DIR oracle directory. Check dba_directories view to get the physical path of oracle directory. 
--                         (If you want to use any other oracle directory other than APPS_DATA_FILE_DIR then replace APPS_DATA_FILE_DIR with your oracle directory name below.)
--                      2. Make sure XXEIS schema has read/write permissions to above used oracle directory and give full permissions to the directory at OS level.
--                      3. Run this file in XXEIS user from SQL*Plus or any tool like Toad, SQL Navigator. 
exec xxeis.eis_rs_ins.g_rtf_dir := 'APPS_DATA_FILE_DIR' ;
prompt Creating View Data for Cash Drawer Exception Report
set scan off define off
DECLARE
BEGIN 
--Inserting View EIS_XXWC_CASH_DRAWN_EXCE_V
xxeis.eis_rs_ins.v( 'EIS_XXWC_CASH_DRAWN_EXCE_V',660,'','','','','XXEIS_RS_ADMIN','XXEIS','Eis Xxwc Ar Cash Drwr Recon V','EXACDRV','','');
--Delete View Columns for EIS_XXWC_CASH_DRAWN_EXCE_V
xxeis.eis_rs_utility.delete_view_rows('EIS_XXWC_CASH_DRAWN_EXCE_V',660,FALSE);
--Inserting View Columns for EIS_XXWC_CASH_DRAWN_EXCE_V
xxeis.eis_rs_ins.vc( 'EIS_XXWC_CASH_DRAWN_EXCE_V','CASH_DATE',660,'Cash Date','CASH_DATE','','','','XXEIS_RS_ADMIN','DATE','','','Cash Date','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_CASH_DRAWN_EXCE_V','BRANCH_NUMBER',660,'Branch Number','BRANCH_NUMBER','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Branch Number','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_CASH_DRAWN_EXCE_V','INVOICE_DATE',660,'Invoice Date','INVOICE_DATE','','','','XXEIS_RS_ADMIN','DATE','','','Invoice Date','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_CASH_DRAWN_EXCE_V','INVOICE_NUMBER',660,'Invoice Number','INVOICE_NUMBER','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Invoice Number','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_CASH_DRAWN_EXCE_V','CUSTOMER_NUMBER',660,'Customer Number','CUSTOMER_NUMBER','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Customer Number','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_CASH_DRAWN_EXCE_V','CASH_AMOUNT',660,'Cash Amount','CASH_AMOUNT','','','','XXEIS_RS_ADMIN','NUMBER','','','Cash Amount','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_CASH_DRAWN_EXCE_V','CARD_ISSUER_NAME',660,'Card Issuer Name','CARD_ISSUER_NAME','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Card Issuer Name','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_CASH_DRAWN_EXCE_V','CASH_TYPE',660,'Cash Type','CASH_TYPE','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Cash Type','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_CASH_DRAWN_EXCE_V','PAYMENT_TYPE_CODE',660,'Payment Type Code','PAYMENT_TYPE_CODE','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Payment Type Code','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_CASH_DRAWN_EXCE_V','ORDER_NUMBER',660,'Order Number','ORDER_NUMBER','','','','XXEIS_RS_ADMIN','NUMBER','','','Order Number','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_CASH_DRAWN_EXCE_V','TAKEN_BY',660,'Taken By','TAKEN_BY','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Taken By','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_CASH_DRAWN_EXCE_V','SEGMENT_NUMBER',660,'Segment Number','SEGMENT_NUMBER','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Segment Number','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_CASH_DRAWN_EXCE_V','CHK_CARDNO',660,'Chk Cardno','CHK_CARDNO','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Chk Cardno','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_CASH_DRAWN_EXCE_V','ORDER_AMOUNT',660,'Order Amount','ORDER_AMOUNT','','~,~2','','XXEIS_RS_ADMIN','NUMBER','','','Order Amount','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_CASH_DRAWN_EXCE_V','NAME',660,'Name','NAME','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Name','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_CASH_DRAWN_EXCE_V','CUSTOMER_NAME',660,'Customer Name','CUSTOMER_NAME','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Customer Name','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_CASH_DRAWN_EXCE_V','ORDER_DATE',660,'Order Date','ORDER_DATE','','','','XXEIS_RS_ADMIN','DATE','','','Order Date','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_CASH_DRAWN_EXCE_V','DIFF',660,'Diff','DIFF','','~,~2','','XXEIS_RS_ADMIN','NUMBER','','','Diff','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_CASH_DRAWN_EXCE_V','ON_ACCOUNT',660,'On Account','ON_ACCOUNT','','','','XXEIS_RS_ADMIN','NUMBER','','','On Account','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_CASH_DRAWN_EXCE_V','DIST_DATE',660,'Dist Date','DIST_DATE','','','','XXEIS_RS_ADMIN','DATE','','','Dist Date','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_CASH_DRAWN_EXCE_V','CUST_ACCOUNT_ID',660,'Cust Account Id','CUST_ACCOUNT_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Cust Account Id','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_CASH_DRAWN_EXCE_V','PARTY_ID',660,'Party Id','PARTY_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Party Id','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_CASH_DRAWN_EXCE_V','PAYMENT_CHANNEL_NAME',660,'Payment Channel Name','PAYMENT_CHANNEL_NAME','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Payment Channel Name','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_CASH_DRAWN_EXCE_V','CHECK_NUMBER',660,'Check Number','CHECK_NUMBER','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Check Number','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_CASH_DRAWN_EXCE_V','CARD_ISSUER_CODE',660,'Card Issuer Code','CARD_ISSUER_CODE','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Card Issuer Code','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_CASH_DRAWN_EXCE_V','CARD_NUMBER',660,'Card Number','CARD_NUMBER','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Card Number','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_CASH_DRAWN_EXCE_V','ORG_ID',660,'Org Id','ORG_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Org Id','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_CASH_DRAWN_EXCE_V','HEADER_ID',660,'Header Id','HEADER_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Header Id','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_CASH_DRAWN_EXCE_V','PAYMENT_TYPE_CODE_NEW',660,'Payment Type Code New','PAYMENT_TYPE_CODE_NEW','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Payment Type Code New','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_CASH_DRAWN_EXCE_V','PROCESS_ID',660,'Process Id','PROCESS_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Process Id','','','');
--Inserting View Components for EIS_XXWC_CASH_DRAWN_EXCE_V
--Inserting View Component Joins for EIS_XXWC_CASH_DRAWN_EXCE_V
END;
/
set scan on define on
prompt Creating Report LOV Data for Cash Drawer Exception Report
set scan off define off
DECLARE
BEGIN 
--Inserting Report LOVs - Cash Drawer Exception Report
xxeis.eis_rs_ins.lov( 660,'SELECT organization_code,organization_id,organization_name FROM org_organization_definitions','','AR Organizaion Code LOV','','XXEIS_RS_ADMIN',NULL,'N','','');
END;
/
set scan on define on
prompt Creating Report Data for Cash Drawer Exception Report
set scan off define off
DECLARE
BEGIN 
--Deleting Report data - Cash Drawer Exception Report
xxeis.eis_rs_utility.delete_report_rows( 'Cash Drawer Exception Report' );
--Inserting Report - Cash Drawer Exception Report
xxeis.eis_rs_ins.r( 660,'Cash Drawer Exception Report','','This report provides a total listing of all Cash, Check and Credit Card Sales by Branch (Customer, Taken By, Order Number, Cash Date, Cash Amount, Cash Type, Invoice Number, Invoice Date, Payment Type, Check/Credit Card No.)','','','','XXEIS_RS_ADMIN','EIS_XXWC_CASH_DRAWN_EXCE_V','Y','','','XXEIS_RS_ADMIN','','N','White Cap Reports','RTF,PDF,','CSV,HTML,Html Summary,XML,Pivot Excel,EXCEL,','N');
--Inserting Report Columns - Cash Drawer Exception Report
xxeis.eis_rs_ins.rc( 'Cash Drawer Exception Report',660,'BRANCH_NUMBER','Branch Number','Branch Number','','','default','','1','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_CASH_DRAWN_EXCE_V','','');
xxeis.eis_rs_ins.rc( 'Cash Drawer Exception Report',660,'CASH_AMOUNT','Cash Amount','Cash Amount','','~T~D~2','default','','9','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_CASH_DRAWN_EXCE_V','','');
xxeis.eis_rs_ins.rc( 'Cash Drawer Exception Report',660,'CASH_DATE','Cash Date','Cash Date','','','default','','7','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_CASH_DRAWN_EXCE_V','','');
xxeis.eis_rs_ins.rc( 'Cash Drawer Exception Report',660,'CASH_TYPE','Cash Type','Cash Type','','','default','','17','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_CASH_DRAWN_EXCE_V','','');
xxeis.eis_rs_ins.rc( 'Cash Drawer Exception Report',660,'CHK_CARDNO','Check Card No','Chk Cardno','','','default','','14','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_CASH_DRAWN_EXCE_V','','');
xxeis.eis_rs_ins.rc( 'Cash Drawer Exception Report',660,'CUSTOMER_NUMBER','Customer Number','Customer Number','','','default','','3','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_CASH_DRAWN_EXCE_V','','');
xxeis.eis_rs_ins.rc( 'Cash Drawer Exception Report',660,'INVOICE_NUMBER','Invoice Number','Invoice Number','','','default','','18','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_CASH_DRAWN_EXCE_V','','');
xxeis.eis_rs_ins.rc( 'Cash Drawer Exception Report',660,'NAME','Receipt Method','Name','','','default','','16','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_CASH_DRAWN_EXCE_V','','');
xxeis.eis_rs_ins.rc( 'Cash Drawer Exception Report',660,'ORDER_AMOUNT','Order Amount','Order Amount','','~T~D~2','default','','8','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_CASH_DRAWN_EXCE_V','','');
xxeis.eis_rs_ins.rc( 'Cash Drawer Exception Report',660,'ORDER_NUMBER','Order Number','Order Number','','~~~','default','','2','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_CASH_DRAWN_EXCE_V','','');
xxeis.eis_rs_ins.rc( 'Cash Drawer Exception Report',660,'PAYMENT_TYPE_CODE','Payment Type','Payment Type Code','','','default','','13','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_CASH_DRAWN_EXCE_V','','');
xxeis.eis_rs_ins.rc( 'Cash Drawer Exception Report',660,'SEGMENT_NUMBER','Segment Number','Segment Number','','','default','','20','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_CASH_DRAWN_EXCE_V','','');
xxeis.eis_rs_ins.rc( 'Cash Drawer Exception Report',660,'TAKEN_BY','Created By','Taken By','','','default','','5','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_CASH_DRAWN_EXCE_V','','');
xxeis.eis_rs_ins.rc( 'Cash Drawer Exception Report',660,'CUSTOMER_NAME','Customer Name','Customer Name','','','default','','4','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_CASH_DRAWN_EXCE_V','','');
xxeis.eis_rs_ins.rc( 'Cash Drawer Exception Report',660,'CARD_ISSUER_NAME','Card Type','Card Issuer Name','','','default','','15','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_CASH_DRAWN_EXCE_V','','');
xxeis.eis_rs_ins.rc( 'Cash Drawer Exception Report',660,'DIFF','Diff','Diff','','~T~D~2','default','','10','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_CASH_DRAWN_EXCE_V','','');
xxeis.eis_rs_ins.rc( 'Cash Drawer Exception Report',660,'ORDER_DATE','Order Date','Order Date','','','default','','6','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_CASH_DRAWN_EXCE_V','','');
xxeis.eis_rs_ins.rc( 'Cash Drawer Exception Report',660,'INVOICE_DATE','Invoice Date','Invoice Date','','','default','','19','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_CASH_DRAWN_EXCE_V','','');
xxeis.eis_rs_ins.rc( 'Cash Drawer Exception Report',660,'ON_ACCOUNT','On Account','On Account','','~T~D~2','default','','11','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_CASH_DRAWN_EXCE_V','','');
xxeis.eis_rs_ins.rc( 'Cash Drawer Exception Report',660,'DIST_DATE','Discrepancy Date','Dist Date','','','default','','12','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_CASH_DRAWN_EXCE_V','','');
--Inserting Report Parameters - Cash Drawer Exception Report
xxeis.eis_rs_ins.rp( 'Cash Drawer Exception Report',660,'Location','Branch Number','BRANCH_NUMBER','IN','AR Organizaion Code LOV','','VARCHAR2','Y','Y','1','','N','CONSTANT','XXEIS_RS_ADMIN','Y','N','','','');
xxeis.eis_rs_ins.rp( 'Cash Drawer Exception Report',660,'As of Date','As of Date','CASH_DATE','>=','','','DATE','Y','Y','2','','N','CONSTANT','XXEIS_RS_ADMIN','Y','N','','','');
--Inserting Report Conditions - Cash Drawer Exception Report
xxeis.eis_rs_ins.rcn( 'Cash Drawer Exception Report',660,'','','','','and PROCESS_ID= :SYSTEM.PROCESS_ID','Y','1','','XXEIS_RS_ADMIN');
--Inserting Report Sorts - Cash Drawer Exception Report
--Inserting Report Triggers - Cash Drawer Exception Report
xxeis.eis_rs_ins.rt( 'Cash Drawer Exception Report',660,'begin
XXEIS.EIS_RS_XXWC_COM_UTIL_PKG.G_CASH_TO_DATE:=:As of Date;
XXEIS.eis_xxwc_cash_exception_pkg.process_cash_exception_orders(P_PROCESS_ID=> :SYSTEM.PROCESS_ID,
p_location => :Location,
p_as_of_date => :As of Date );
End;','B','Y','XXEIS_RS_ADMIN');
xxeis.eis_rs_ins.rt( 'Cash Drawer Exception Report',660,'begin
XXEIS.eis_xxwc_cash_exception_pkg.clear_temp_tables(P_PROCESS_ID=> :SYSTEM.PROCESS_ID);
end;','A','Y','XXEIS_RS_ADMIN');
--Inserting Report Templates - Cash Drawer Exception Report
xxeis.eis_rs_ins.R_Tem( 'Cash Drawer Exception Report','Cash Drawer Reconciliation','Cash Drawer Reconciliation','','','','','','','','','','','','XXEIS_RS_ADMIN');
--Inserting Report Portals - Cash Drawer Exception Report
--Inserting Report Dashboards - Cash Drawer Exception Report
xxeis.eis_rs_ins.r_dash( 'Cash Drawer Exception Report','Dynamic 757','Dynamic 757','scatter','large','Customer Name','Customer Name','Cash Amount','Cash Amount','Count','XXEIS_RS_ADMIN');
xxeis.eis_rs_ins.r_dash( 'Cash Drawer Exception Report','Dynamic 751','Dynamic 751','pie','large','Cash Amount','Cash Amount','Customer Name','Customer Name','Count','XXEIS_RS_ADMIN');
xxeis.eis_rs_ins.r_dash( 'Cash Drawer Exception Report','Dynamic 752','Dynamic 752','pie','large','Customer Name','Customer Name','Cash Amount','Cash Amount','Count','XXEIS_RS_ADMIN');
xxeis.eis_rs_ins.r_dash( 'Cash Drawer Exception Report','Dynamic 753','Dynamic 753','vertical stacked bar','large','Customer Name','Customer Name','Cash Amount','Cash Amount','Count','XXEIS_RS_ADMIN');
xxeis.eis_rs_ins.r_dash( 'Cash Drawer Exception Report','Dynamic 755','Dynamic 755','point','large','Customer Name','Customer Name','Cash Amount','Cash Amount','Count','XXEIS_RS_ADMIN');
xxeis.eis_rs_ins.r_dash( 'Cash Drawer Exception Report','Dynamic 759','Dynamic 759','horizontal stacked bar','large','Customer Name','Customer Name','Cash Amount','Cash Amount','Count','XXEIS_RS_ADMIN');
xxeis.eis_rs_ins.r_dash( 'Cash Drawer Exception Report','Dynamic 756','Dynamic 756','stacked line','large','Customer Name','Customer Name','Cash Amount','Cash Amount','Count','XXEIS_RS_ADMIN');
xxeis.eis_rs_ins.r_dash( 'Cash Drawer Exception Report','Dynamic 758','Dynamic 758','vertical stacked bar','large','Customer Name','Customer Name','Cash Amount','Cash Amount','Count','XXEIS_RS_ADMIN');
xxeis.eis_rs_ins.r_dash( 'Cash Drawer Exception Report','Dynamic 754','Dynamic 754','absolute line','large','Customer Name','Customer Name','Cash Amount','Cash Amount','Count','XXEIS_RS_ADMIN');
--Inserting Report Security - Cash Drawer Exception Report
xxeis.eis_rs_ins.rsec( 'Cash Drawer Exception Report','','LB048272','',660,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Cash Drawer Exception Report','101','','20434',660,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Cash Drawer Exception Report','101','','20475',660,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Cash Drawer Exception Report','660','','50878',660,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Cash Drawer Exception Report','660','','50877',660,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Cash Drawer Exception Report','660','','50879',660,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Cash Drawer Exception Report','660','','50853',660,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Cash Drawer Exception Report','660','','50880',660,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Cash Drawer Exception Report','660','','50854',660,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Cash Drawer Exception Report','101','','50721',660,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Cash Drawer Exception Report','101','','50662',660,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Cash Drawer Exception Report','101','','50720',660,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Cash Drawer Exception Report','101','','50723',660,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Cash Drawer Exception Report','101','','50801',660,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Cash Drawer Exception Report','660','','50847',660,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Cash Drawer Exception Report','260','','50758',660,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Cash Drawer Exception Report','260','','50770',660,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Cash Drawer Exception Report','660','','50886',660,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Cash Drawer Exception Report','401','','50835',660,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Cash Drawer Exception Report','660','','50838',660,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Cash Drawer Exception Report','401','','50840',660,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Cash Drawer Exception Report','101','','50665',660,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Cash Drawer Exception Report','101','','50722',660,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Cash Drawer Exception Report','101','','50780',660,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Cash Drawer Exception Report','101','','50667',660,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Cash Drawer Exception Report','101','','50668',660,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Cash Drawer Exception Report','701','','50546',660,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Cash Drawer Exception Report','660','','21623',660,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Cash Drawer Exception Report','20005','','50880',660,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Cash Drawer Exception Report','660','','50858',660,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Cash Drawer Exception Report','660','','51044',660,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Cash Drawer Exception Report','','TW008334','',660,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Cash Drawer Exception Report','660','','50857',660,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Cash Drawer Exception Report','660','','51045',660,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Cash Drawer Exception Report','660','','50901',660,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Cash Drawer Exception Report','660','','51025',660,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Cash Drawer Exception Report','660','','50860',660,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Cash Drawer Exception Report','660','','50859',660,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Cash Drawer Exception Report','20005','','50900',660,'XXEIS_RS_ADMIN','','');
--Inserting Report Pivots - Cash Drawer Exception Report
xxeis.eis_rs_ins.rpivot( 'Cash Drawer Exception Report',660,'By Payment Type','1','1,0|1,2,1','1,1,0,0|None|2');
--Inserting Report Pivot Details For Pivot - By Payment Type
xxeis.eis_rs_ins.rpivot_dtls( 'Cash Drawer Exception Report',660,'By Payment Type','CASH_TYPE','PAGE_FIELD','','','1','','');
xxeis.eis_rs_ins.rpivot_dtls( 'Cash Drawer Exception Report',660,'By Payment Type','CUSTOMER_NUMBER','PAGE_FIELD','','','2','','');
xxeis.eis_rs_ins.rpivot_dtls( 'Cash Drawer Exception Report',660,'By Payment Type','NAME','PAGE_FIELD','','','3','','');
xxeis.eis_rs_ins.rpivot_dtls( 'Cash Drawer Exception Report',660,'By Payment Type','CUSTOMER_NAME','PAGE_FIELD','','','4','','');
xxeis.eis_rs_ins.rpivot_dtls( 'Cash Drawer Exception Report',660,'By Payment Type','CASH_AMOUNT','DATA_FIELD','SUM','','1','','');
xxeis.eis_rs_ins.rpivot_dtls( 'Cash Drawer Exception Report',660,'By Payment Type','BRANCH_NUMBER','ROW_FIELD','','','1','','');
xxeis.eis_rs_ins.rpivot_dtls( 'Cash Drawer Exception Report',660,'By Payment Type','PAYMENT_TYPE_CODE','ROW_FIELD','','','2','','');
xxeis.eis_rs_ins.rpivot_dtls( 'Cash Drawer Exception Report',660,'By Payment Type','CARD_ISSUER_NAME','ROW_FIELD','','','3','','');
--Inserting Report Summary Calculation Columns For Pivot- By Payment Type
END;
/
set scan on define on
