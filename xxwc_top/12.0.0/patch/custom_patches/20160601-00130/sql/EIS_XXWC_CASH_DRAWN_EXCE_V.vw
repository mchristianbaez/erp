-----------------------------------------------------------------------------------------------
/********************************************************************************************************
  $Header XXEIS.EIS_XXWC_CASH_DRAWN_EXCE_V $
  Module Name : Order Management
  PURPOSE	  :  Replaced the old view definition with new view.This view is created based on 
				 custom table XXEIS.EIS_XXWC_CASH_DRAWN_EXCE_TBL and this custom table data
				 gets populated by XXEIS.EIS_XXWC_CASH_EXCEPTION_PKG package.
  TMS Task Id : 20160411-00103
  REVISIONS   :
   Ver        Date        Author           Description
   ---------  ----------  ---------------  ------------------------------------
   1.0		 04/12/2016   Siva  			TMS#20160411-00103 -- Performance Tuning
*********************************************************************************************************/
DROP VIEW XXEIS.EIS_XXWC_CASH_DRAWN_EXCE_V;

CREATE OR REPLACE VIEW XXEIS.EIS_XXWC_CASH_DRAWN_EXCE_V (ORDER_NUMBER, ORG_ID,
  PAYMENT_TYPE_CODE, PAYMENT_TYPE_CODE_NEW, TAKEN_BY, CASH_TYPE, CHK_CARDNO,
  CARD_NUMBER, CARD_ISSUER_CODE, CARD_ISSUER_NAME, CASH_AMOUNT, CUSTOMER_NUMBER
  , CUSTOMER_NAME, BRANCH_NUMBER, CHECK_NUMBER, ORDER_DATE, CASH_DATE,
  PAYMENT_CHANNEL_NAME, NAME, ORDER_AMOUNT, DIFF, PARTY_ID, CUST_ACCOUNT_ID,
  INVOICE_NUMBER, INVOICE_DATE, SEGMENT_NUMBER, HEADER_ID, ON_ACCOUNT,
  DIST_DATE, PROCESS_ID)
AS
  SELECT		
    ORDER_NUMBER,
    ORG_ID,
    PAYMENT_TYPE_CODE,
    PAYMENT_TYPE_CODE_NEW,
    TAKEN_BY,
    CASH_TYPE,
    CHK_CARDNO,
    CARD_NUMBER,
    CARD_ISSUER_CODE,
    CARD_ISSUER_NAME,
    CASH_AMOUNT,
    CUSTOMER_NUMBER,
    CUSTOMER_NAME,
    BRANCH_NUMBER,
    CHECK_NUMBER,
    ORDER_DATE,
    CASH_DATE,
    PAYMENT_CHANNEL_NAME,
    name,
    ORDER_AMOUNT,
    DIFF,
    PARTY_ID,
    CUST_ACCOUNT_ID,
    INVOICE_NUMBER,
    INVOICE_DATE,
    SEGMENT_NUMBER,
    HEADER_ID,
    ON_ACCOUNT,
    DIST_DATE,
    PROCESS_ID 
  FROM
    XXEIS.EIS_XXWC_CASH_DRAWN_EXCE_TBL
/
