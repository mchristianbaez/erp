/* Formatted on 4/22/2014 4:39:22 PM (QP5 v5.206) */
-- Start of DDL Script for Package Body APPS.XXWC_EDI_IFACE_PKG
-- Generated 4/22/2014 4:39:20 PM from APPS@EBIZPRD

CREATE OR REPLACE PACKAGE BODY apps.xxwc_edi_iface_pkg
IS
    ---****************************************************************
    ---****************************************************************
    ---********run_edi_driver_dbms_job to call from dbms_job***********
    ---****************************************************************
    --2/21/2013 TMS 20130110-01202: changed  PROCEDURE populate_apex_fields  (p_file_name IN VARCHAR2)- Rasikha Galimova

 /*  **************************************************************************
      $Header xxwc_edi_iface_pkg $
      Module Name: xxwc_edi_iface_pkg.pkb

      PURPOSE:   This package is used by the Vendor Minimum Form

      REVISIONS:
      Ver        Date        Author               Description
      ---------  ----------  ---------------      -------------------------
      1.0        2/21/2013  Rasikha Galimova      TMS 20130110-01202 changed  PROCEDURE populate_apex_fields  (p_file_name IN VARCHAR2)
      1.1        11/07/2014 Maharajan Shunmugam   TMS#20141001-00259   Canada Multi org changes
	  1.2        07/07/2017 Pattabhi Avula        TMS#20170606-00071 External table
                                             	  issue,putting log messages
    **************************************************************************/



    PROCEDURE run_edi_driver_dbms_job
    IS
        v_errbuf    VARCHAR2 (2000);
        v_retcode   VARCHAR2 (2000);
    BEGIN
        xxwc_edi_iface_pkg.xxwc_edi_process_driver (v_errbuf
                                                   ,v_retcode
                                                   ,'N'
                                                   ,'N');
    --DBMS_OUTPUT.put_line (v_retcode || ',' || v_errbuf);
    END;

    PROCEDURE delete_log_files
    IS
        v_os_error   VARCHAR2 (4000);
    BEGIN
        FOR r IN (SELECT 'rm ' || directory_path || '/' || e.file_name command_do
                    FROM xxwc.xxwc_edi_inbound_files e, all_directories d
                   WHERE e.file_name LIKE '%.log' AND d.directory_name = 'EDI_INBOUND')
        LOOP
            SELECT xxwc_edi_iface_pkg.xxwc_oscommand_run (r.command_do) INTO v_os_error FROM DUAL;

            IF v_os_error IS NOT NULL
            THEN
                xxwc_log (v_os_error);
            END IF;
        END LOOP;
    END;

    ---****************************************************************
    ---****************************************************************
    -- to start the job for edi, it it broken, can be run anytime, will not resubmit
    ---****************************************************************
    ---****************************************************************
    PROCEDURE start_edi_dbms_job
    IS
        v_job    NUMBER;
        v_main   NUMBER;
        v_fail   NUMBER := 0;
    BEGIN
        --check that we don't have the same job broken
        SELECT COUNT (*)
          INTO v_fail
          FROM user_jobs
         WHERE what = 'xxwc_edi_iface_pkg.run_edi_driver_dbms_job;' AND broken = 'Y';

        IF v_fail > 0
        THEN
            FOR r IN (SELECT job
                        FROM user_jobs
                       WHERE what = 'xxwc_edi_iface_pkg.run_edi_driver_dbms_job;' AND broken = 'Y')
            LOOP
                DBMS_JOB.remove (r.job);
                COMMIT;
            END LOOP;
        END IF;

        -- check we don't have the same job runnig job
        SELECT COUNT (*)
          INTO v_main
          FROM user_jobs
         WHERE what = 'xxwc_edi_iface_pkg.run_edi_driver_dbms_job;';

        IF v_main = 0
        THEN
            ---submit job which runs weekdays every 8 min
            DBMS_JOB.submit (v_job
                            ,'xxwc_edi_iface_pkg.run_edi_driver_dbms_job;'
                            ,SYSDATE
                            ,' least(
   next_day(SYSDATE - 1,''MONDAY''),
   next_day(SYSDATE - 1,''TUESDAY''),
   next_day(SYSDATE - 1,''WEDNESDAY''),
   next_day(SYSDATE - 1,''THURSDAY''),
   next_day(SYSDATE - 1,''FRIDAY'')
)+1/180');
            COMMIT;
        END IF;
    END;

    ---****************************************************************
    ---****************************************************************
    -- xxwc_edi_process_driver  is main procedure in this package
    ---****************************************************************
    ---****************************************************************
    PROCEDURE xxwc_edi_process_driver (p_errbuf                         OUT NOCOPY VARCHAR2
                                      ,p_retcode                        OUT NOCOPY VARCHAR2
                                      ,p_process_files_with_errors                 VARCHAR2 DEFAULT NULL
                                      ,p_delete_files_with_errors                  VARCHAR2 DEFAULT NULL)
    IS
        vl_req_id                     NUMBER;
        v_child_req_id                NUMBER;
        vr_result                     BOOLEAN;

        vr_phase                      VARCHAR2 (64);
        vr_status                     VARCHAR2 (64);
        vr_dev_phase                  VARCHAR2 (64);
        vr_dev_status                 VARCHAR2 (64);
        vr_debugging_message1         VARCHAR2 (264);
        v_exit_exception              EXCEPTION;
        v_exit_weekend                EXCEPTION;

        v_os_error                    CLOB;
        v_os_command                  CLOB;
        v_send_email                  NUMBER;
        v_process_files_with_errors   VARCHAR2 (16);
        v_delete_files_with_errors    VARCHAR2 (16);
        v_day_of_week                 VARCHAR2 (16);
        v_inbound_path                VARCHAR2 (256) := fnd_profile.VALUE ('ECE_IN_FILE_PATH');
        v_procedure_name              VARCHAR2 (256) := 'xxwc_edi_process_driver';

        v_errbuf                      CLOB;
        v_err_code                    VARCHAR2 (16) := '0';
        v_user_id                     NUMBER;
        v_user_name                   VARCHAR2 (164) := UPPER ('xxwc_int_supplychain');
        v_responsibility_name         VARCHAR2 (164) := 'e-Commerce Gateway';
        v_resp_id                     NUMBER;
        v_resp_appl_id                NUMBER;
        v_package_name                VARCHAR2 (128) := 'xxwc_edi_iface_pkg';
        v_org_id                      NUMBER;
        v_dist_edi_email_group        VARCHAR2 (256);
        v_distro_list                 VARCHAR2 (256) := 'HDSOracleDevelopers@hdsupply.com';
        v_invoice_date                DATE;
        v_age_invoice                 VARCHAR2 (1);
    BEGIN
        --process will run only weekdays
        BEGIN
            SELECT TRIM (TO_CHAR (SYSDATE, 'DAY')) INTO v_day_of_week FROM DUAL;

            IF v_day_of_week IN ('SUNDAY', 'SATURDAY')
            THEN
                RAISE v_exit_weekend;
            END IF;
        END;

        delete_log_files;
        v_process_files_with_errors := NVL (p_process_files_with_errors, 'N');
        v_delete_files_with_errors := NVL (p_delete_files_with_errors, 'N');

        v_errbuf := NULL;
        v_err_code := '0';
        p_retcode := '0';
        p_errbuf := NULL;
        --check for new edi files in xx_iface directoru=y
        xxwc_check_for_new_edi_files (v_err_code);

        IF v_err_code <> '0'
        THEN
            v_errbuf := 'error after call to xxwc_check_for_new_edi_files in xxwc_edi_process_driver';

            RAISE v_exit_exception;
        END IF;

        BEGIN
            SELECT user_id
              INTO v_user_id
              FROM fnd_user
             WHERE user_name = UPPER (v_user_name);
        EXCEPTION
            WHEN OTHERS
            THEN
                v_errbuf := 'User - ' || v_user_name || ' not defined in Oracle';

                RAISE v_exit_exception;
        END;

        BEGIN
            SELECT responsibility_id, application_id
              INTO v_resp_id, v_resp_appl_id
              FROM fnd_responsibility_vl
             WHERE     responsibility_name = v_responsibility_name
                   AND SYSDATE BETWEEN start_date AND NVL (end_date, TRUNC (SYSDATE) + 1);
        EXCEPTION
            WHEN NO_DATA_FOUND
            THEN
                v_errbuf := 'Responsibility - ' || v_responsibility_name || ' not defined in Oracle';

                RAISE v_exit_exception;
            WHEN OTHERS
            THEN
                v_errbuf := 'Error deriving Responsibility_id for ResponsibilityName - ' || v_responsibility_name;

                RAISE v_exit_exception;
        END;

        fnd_global.apps_initialize (user_id => v_user_id, resp_id => v_resp_id, resp_appl_id => v_resp_appl_id);
        mo_global.init ('EC');

        --here loop in xxwc.xxwc_edi_inbound_files_history and call xxwc_edi_810_request
        --for each line, update table with process flag
        FOR r
            IN (SELECT DISTINCT file_name, insert_date, invoice_number
                  FROM xxwc.xxwc_edi_inbound_files_history
                 WHERE process_flag = 'S' AND file_name LIKE 'INI%.dat'
                UNION
                --reprocess files with status='E' older than 24 hours
                SELECT DISTINCT file_name, insert_date, invoice_number
                  FROM xxwc.xxwc_edi_inbound_files_history
                 WHERE     process_flag = 'E'
                       AND (v_process_files_with_errors = 'Y' OR SYSDATE > process_date + 1)
                       AND file_name LIKE 'INI%.dat')
        LOOP
            vl_req_id := 0;
            v_invoice_date := NULL;
            v_age_invoice := NULL;

            IF r.file_name LIKE 'INI%.dat'
            THEN
                IF r.invoice_number IS NULL
                THEN
                    populate_apex_fields (r.file_name);
                END IF;

                COMMIT;
                --read the file and find invoice date, if not found use file insert date
                v_invoice_date := NVL (xxwc_edi_iface_pkg.get_invoice_date (r.file_name), r.insert_date);

                IF v_invoice_date IS NULL
                THEN
                    v_errbuf := 'EDI file - ' || r.file_name || ' missing invoice date';

                    RAISE v_exit_exception;
                --program will process only files with invoice(insert) date 5 days old
                ELSIF v_invoice_date IS NOT NULL AND SYSDATE < v_invoice_date + 5
                THEN
                    v_age_invoice := 'Y';
                ELSIF v_invoice_date IS NOT NULL AND SYSDATE >= v_invoice_date + 5
                THEN
                    v_age_invoice := 'N';
                    vl_req_id := xxwc_edi_810_request (r.file_name);
                END IF;
            END IF;

            IF vl_req_id = 0 AND v_age_invoice = 'N'
            THEN
                v_errbuf :=
                    ' An error when submitting the xxwc_edi_request for file:' || r.file_name || ' request_id =0 ';

                UPDATE xxwc.xxwc_edi_inbound_files_history
                   SET process_date = SYSDATE, process_flag = 'E', error_message = SUBSTR (v_errbuf, 1, 2000)
                 WHERE file_name = r.file_name;
            END IF;
        END LOOP;

        COMMIT;
        v_errbuf := NULL;

        --wait for requests
        FOR r IN (SELECT a.*
                    FROM xxwc.xxwc_edi_inbound_files_history a
                   WHERE a.process_flag = 'W' AND a.file_name LIKE 'INI%.dat')
        LOOP
            vr_result :=
                fnd_concurrent.wait_for_request (r.request_id
                                                ,10
                                                ,0
                                                ,vr_phase
                                                ,vr_status
                                                ,vr_dev_phase
                                                ,vr_dev_status
                                                ,vr_debugging_message1);

            v_child_req_id := NULL;

            IF NVL (r.request_id, 0) > 0
            THEN
                FOR rl IN (SELECT request_id
                             FROM fnd_concurrent_requests
                            WHERE parent_request_id = r.request_id)
                LOOP
                    v_child_req_id := rl.request_id;

                    UPDATE xxwc.xxwc_edi_inbound_files_history
                       SET child_request_id = v_child_req_id
                     WHERE request_id = r.request_id;
                END LOOP;
            END IF;

            IF vr_result AND vr_dev_phase = 'COMPLETE' AND vr_dev_status <> 'NORMAL'
            THEN
                IF v_child_req_id IS NOT NULL
                THEN
                    v_errbuf := '; child_request_id =' || v_child_req_id;
                END IF;

                v_errbuf :=
                       ' An error when submit the xxwc_edi_request for file:'
                    || r.file_name
                    || '; Phase: '
                    || vr_dev_phase
                    || '; Status: '
                    || vr_dev_status
                    || '; message='
                    || vr_debugging_message1
                    || '; request_id ='
                    || r.request_id
                    || '; child_request_id ='
                    || v_child_req_id;

                UPDATE xxwc.xxwc_edi_inbound_files_history
                   SET return_status = vr_status
                      ,process_date = SYSDATE
                      ,process_flag = 'E'
                      ,error_message = SUBSTR (v_errbuf, 1, 2000)
                 WHERE file_name = r.file_name;
            ELSIF vr_result AND vr_dev_phase = 'COMPLETE' AND vr_dev_status = 'NORMAL'
            THEN
                v_errbuf :=
                       ' Request xxwc_edi_request for file:'
                    || r.file_name
                    || ' was successfully processed:  Phase 2: '
                    || vr_phase
                    || ' Status: '
                    || vr_status
                    || ' message='
                    || vr_debugging_message1
                    || ' request_id ='
                    || r.request_id;

                UPDATE xxwc.xxwc_edi_inbound_files_history
                   SET return_status = vr_status
                      ,process_date = SYSDATE
                      ,process_flag = 'P'
                      ,error_message = SUBSTR (v_errbuf, 1, 2000)
                 WHERE file_name = r.file_name;
            ELSIF NOT vr_result
            THEN
                v_errbuf :=
                       ' An error  WAIT FOR REQUEST FAILED - STATUS UNKNOWN:'
                    || r.file_name
                    || '; Phase: '
                    || vr_dev_phase
                    || '; Status: '
                    || vr_dev_status
                    || '; message='
                    || vr_debugging_message1
                    || '; request_id ='
                    || r.request_id;

                UPDATE xxwc.xxwc_edi_inbound_files_history
                   SET return_status = vr_status
                      ,process_date = SYSDATE
                      ,process_flag = 'E'
                      ,error_message = SUBSTR (v_errbuf, 1, 2000)
                 WHERE file_name = r.file_name;
            END IF;
        END LOOP;

        COMMIT;

        --move processed files to archive directory
        BEGIN
            FOR r IN (SELECT file_name
                        FROM xxwc.xxwc_edi_inbound_files_history
                       WHERE file_name LIKE 'INI%.dat' AND return_status = 'Normal' AND process_flag = 'P')
            LOOP
                SELECT xxwc_edi_iface_pkg.xxwc_oscommand_run (
                              'mv '
                           || v_inbound_path
                           || '/'
                           || r.file_name
                           || ' '
                           || v_inbound_path
                           || '/archive/suc'
                           || r.file_name)
                  INTO v_os_error
                  FROM DUAL;

                UPDATE xxwc.xxwc_edi_inbound_files_history
                   SET process_flag = 'ArchivedSuccess'
                 WHERE file_name = r.file_name;

                COMMIT;

                IF v_os_error IS NOT NULL
                THEN
                    xxwc_log ('v_Os_error when archiving =' || v_os_error || 'for filr :' || r.file_name);
                END IF;
            END LOOP;
        END;

        COMMIT;

        --send error email to BA one a day or when directly processing errors
        SELECT COUNT ('a')
          INTO v_send_email
          FROM xxwc.xxwc_edi_inbound_files_history
         WHERE     process_flag = 'E'
               AND (SYSDATE > (NVL (email_sent_date, SYSDATE - 1.1) + 1) OR v_process_files_with_errors = 'Y');

        IF v_send_email > 0
        THEN
            get_concurrent_req_log_files;

            BEGIN
                SELECT meaning
                  INTO v_dist_edi_email_group
                  FROM fnd_lookup_values
                 WHERE lookup_type = 'XXWC_COMMON_LOOKUP' AND lookup_code = 'EDI_EMAIL' AND enabled_flag = 'Y';
            EXCEPTION
                WHEN OTHERS
                THEN
                    xxwc_log (
                        'lookup_type =  XXWC_COMMON_LOOKUP  AND lookup_code =  EDI_EMAIL not setup in Applications;');
            END;

            --send errors by email
            IF v_dist_edi_email_group IS NOT NULL
            THEN
                FOR r
                    IN (SELECT file_name, request_id
                          FROM xxwc.xxwc_edi_inbound_files_history
                         WHERE     process_flag = 'E'
                               AND request_id IS NOT NULL
                               AND SYSDATE > (NVL (email_sent_date, SYSDATE - 1.1) + 1))
                LOOP
                    send_error_email (r.request_id, r.file_name, v_dist_edi_email_group);

                    -- send_error_email (r.request_id, r.file_name, 'rasikha.galimova@hdsupply.com');

                    UPDATE xxwc.xxwc_edi_inbound_files_history
                       SET email_sent_date = SYSDATE
                     WHERE process_flag = 'E' AND request_id = r.request_id;
                END LOOP;

                COMMIT;
            END IF;
        END IF;

        -- remove all files with errors 30 days old or when parameter delete_files_with_errors ='Y'
        BEGIN
            FOR r
                IN (SELECT file_name
                      FROM xxwc.xxwc_edi_inbound_files_history
                     WHERE     file_name LIKE 'INI%.dat'
                           AND process_flag = 'E'
                           AND (insert_date <= SYSDATE - 30 OR v_delete_files_with_errors = 'Y'))
            LOOP
                v_os_command :=
                       'mv '
                    || v_inbound_path
                    || '/'
                    || r.file_name
                    || ' '
                    || v_inbound_path
                    || '/archive/err'
                    || r.file_name;
                xxwc_log (v_os_command);

                SELECT xxwc_edi_iface_pkg.xxwc_oscommand_run (v_os_command) INTO v_os_error FROM DUAL;

                UPDATE xxwc.xxwc_edi_inbound_files_history
                   SET process_flag = 'ArchivedError'
                 WHERE file_name = r.file_name;

                COMMIT;

                IF v_os_error IS NOT NULL
                THEN
                    xxwc_log (
                        'v_Os_error moving files with errors to archive=' || v_os_error || 'for file :' || r.file_name);
                END IF;
            END LOOP;

            INSERT INTO xxwc.xxwc_edi_inbound_files_archive
                SELECT *
                  FROM xxwc.xxwc_edi_inbound_files_history
                 WHERE process_flag LIKE 'Archived%';

            DELETE FROM xxwc.xxwc_edi_inbound_files_history
                  WHERE process_flag LIKE 'Archived%';

            COMMIT;
        END;

        p_retcode := '0';
        p_errbuf := NULL;
    EXCEPTION
        WHEN v_exit_weekend
        THEN
            p_retcode := '0';
            p_errbuf := 'EDI program is not running on SUNDAY or SATURDAY; ';
            v_err_code := '0';
            xxwc_log ('EDI program is not running on SUNDAY or SATURDAY;');
        WHEN v_exit_exception
        THEN
            p_retcode := '2';
            v_err_code := '2';
            p_errbuf := v_errbuf;
            xxwc_log (v_errbuf);
        WHEN OTHERS
        THEN
            v_errbuf :=
                   'Error_Stack...'
                || CHR (10)
                || DBMS_UTILITY.format_error_stack ()
                || CHR (10)
                || ' Error_Backtrace...'
                || CHR (10)
                || DBMS_UTILITY.format_error_backtrace ();
            xxwc_log ('Error running ' || v_package_name || '.' || v_procedure_name || ' ' || v_errbuf);
            v_err_code := '2';
            xxcus_error_pkg.xxcus_error_main_api (
                p_called_from         => v_package_name
               ,p_calling             => v_procedure_name
               ,p_request_id          => NULL
               ,p_ora_error_msg       => SUBSTR (v_errbuf, 1, 2000)
               ,p_error_desc          => 'Error running ' || v_package_name || '.' || v_procedure_name
               ,p_distribution_list   => v_distro_list
               ,p_module              => 'XXWC');
            p_retcode := v_err_code;
            p_errbuf := SUBSTR (v_errbuf, 1, 2000);
    END xxwc_edi_process_driver;

    --******************************************************************************
    --******************************************************************************
    --***********PROCEDURE xxwc_check_for_new_edi_files*******************************************
    --******************************************************************************
    /* ******************************************************************************
	  $Header xxwc_check_for_new_edi_files $
      Module Name: xxwc_check_for_new_edi_files.proc

      PURPOSE:   This procedure will check the EDI files validation

      REVISIONS:
      Ver        Date        Author                  Description
      ---------  ----------  ---------------         -------------------------
	  1.2        07/07/2017  Pattabhi Avula          TMS#20170606-00071   External table 
	                                                 issue, putting log messages
	****************************************************************************** */
    PROCEDURE xxwc_check_for_new_edi_files (p_err_code OUT VARCHAR2)
    IS
        v_package_name               VARCHAR2 (256) := 'xxwc_edi_iface_pkg';
        v_procedure_name             VARCHAR2 (256) := 'xxwc_check_for_new_edi_files';
        v_inbound_path               VARCHAR2 (256) := fnd_profile.VALUE ('ECE_IN_FILE_PATH');
        v_inbound_directory          VARCHAR2 (256);
        v_edi_inbound_path_is_null   EXCEPTION;

        v_distro_list                VARCHAR2 (256) := 'HDSOracleDevelopers@hdsupply.com';
        v_errbuf                     CLOB := NULL;
        v_err_code                   VARCHAR2 (16) := '0';
    BEGIN
        p_err_code := v_err_code;

        IF v_inbound_path IS NULL
        THEN
            RAISE v_edi_inbound_path_is_null;
        END IF;

        FOR r IN (SELECT directory_name
                    FROM all_directories
                   WHERE directory_path = v_inbound_path)
        LOOP
            v_inbound_directory := r.directory_name;
        END LOOP;

        IF NVL (v_inbound_directory, 'null') <> 'EDI_INBOUND'
        THEN
            RAISE v_edi_inbound_path_is_null;
        END IF;
        BEGIN  -- Ver# 1.2
        INSERT INTO xxwc.xxwc_edi_inbound_files_history (file_name
                                                        ,insert_date
                                                        ,process_date
                                                        ,process_flag)
            SELECT e.file_name
                  ,SYSDATE
                  ,NULL
                  ,'S'
              FROM xxwc.xxwc_edi_inbound_files e
             WHERE     e.file_name NOT IN (SELECT file_name FROM xxwc.xxwc_edi_inbound_files_history)
                   AND e.file_name LIKE 'INI%.dat';

        COMMIT;
		-- <START> Ver# 1.2
        EXCEPTION
		WHEN OTHERS THEN
		v_errbuf:='Exception Error occured in insert statement '||SQLERRM;
		xxwc_log(v_errbuf);
		END;
		-- <END> Ver# 1.2
        -- delete  file_names from table which are not present in the directory - may be deleted manually
        DELETE FROM xxwc.xxwc_edi_inbound_files_history
              WHERE file_name NOT IN (SELECT NVL (e.file_name, 'null')
                                        FROM xxwc.xxwc_edi_inbound_files e);

        COMMIT;
        p_err_code := v_err_code;
    EXCEPTION
        WHEN v_edi_inbound_path_is_null
        THEN
            v_errbuf :=
                ' Profile option ECE_IN_FILE_PATH not setup in APPS or directory EDI_INBOUND not set up in database; ';
            v_err_code := '2';
            p_err_code := v_err_code;
            xxcus_error_pkg.xxcus_error_main_api (
                p_called_from         => v_package_name
               ,p_calling             => v_procedure_name
               ,p_request_id          => NULL
               ,p_ora_error_msg       => SUBSTR (v_errbuf, 1, 2000)
               ,p_error_desc          => 'Error running ' || v_package_name || '.' || v_procedure_name
               ,p_distribution_list   => v_distro_list
               ,p_module              => 'XXWC');
        WHEN OTHERS
        THEN
            v_errbuf :=
                   'Error_Stack...'
                || CHR (10)
                || DBMS_UTILITY.format_error_stack ()
                || CHR (10)
                || ' Error_Backtrace...'
                || CHR (10)
                || DBMS_UTILITY.format_error_backtrace ()
				|| SQLERRM; -- Ver# 1.2
            xxwc_log ('Error running ' || v_package_name || '.' || v_procedure_name || ' ' || v_errbuf);
            v_err_code := '2';
            xxcus_error_pkg.xxcus_error_main_api (
                p_called_from         => v_package_name
               ,p_calling             => v_procedure_name
               ,p_request_id          => NULL
               ,p_ora_error_msg       => SUBSTR (v_errbuf||SQLERRM, 1, 2000)  --SUBSTR (v_errbuf, 1, 2000)  -- Ver# 1.2
               ,p_error_desc          => 'Error running ' || v_package_name || '.' || v_procedure_name
               ,p_distribution_list   => v_distro_list
               ,p_module              => 'XXWC');
            p_err_code := v_err_code;
    END xxwc_check_for_new_edi_files;

    --******************************************************************************
    --******************************************************************************
    --******    FUNCTION xxwc_edi_810_request (p_file_name IN VARCHAR2)
    --    RETURN NUMBER**************************************************
    --******************************************************************************
    --******************************************************************************
    --******************************************************************************
    FUNCTION xxwc_edi_810_request (p_file_name IN VARCHAR2)
        RETURN NUMBER
    IS
        v_package_name     VARCHAR2 (256) := 'xxwc_edi_iface_pkg';
        v_procedure_name   VARCHAR2 (256) := 'xxwc_edi_810_request';
        v_request_id       NUMBER := 0;
        v_distro_list      VARCHAR2 (256) := 'HDSOracleDevelopers@hdsupply.com';
        v_errbuf           CLOB;
        v_err_code         VARCHAR2 (16) := '0';
        v_inbound_path     VARCHAR2 (256) := fnd_profile.VALUE ('ECE_IN_FILE_PATH');
    BEGIN
        v_request_id :=
            fnd_request.submit_request ('EC'
                                       ,'ECEINI'
                                       ,''
                                       ,''
                                       ,FALSE
                                       ,v_inbound_path                                               --Inbound File Path
                                       ,p_file_name                                                   --Inbound Datafile
                                       ,'Y'                                               -- Execute Open Interface Flag
                                       ,NULL                                                       --Payables Batch Name
                                       ,NULL                                                       --Payables: Hold Name
                                       ,NULL                                                     --Payables: Hold Reason
                                       ,NULL                                                         --Payables: GL Date
                                       ,'N'                                                            --Payables: Purge
                                       ,'N'                                                           --Summarize Report
                                       ,'INI'                                                         --Transaction Type
                                       ,238                                                                   --Map Code
                                       ,0                                                           --Debug Mode For INI
                                       ,'UTF8');                                                --Data File Characterset

        COMMIT;

        UPDATE xxwc.xxwc_edi_inbound_files_history
           SET request_id = v_request_id, process_flag = 'W'
         WHERE file_name = p_file_name;

        COMMIT;
        RETURN v_request_id;
    EXCEPTION
        WHEN OTHERS
        THEN
            v_errbuf :=
                   'Error_Stack...'
                || CHR (10)
                || DBMS_UTILITY.format_error_stack ()
                || CHR (10)
                || ' Error_Backtrace...'
                || CHR (10)
                || DBMS_UTILITY.format_error_backtrace ();
            xxwc_log ('Error running ' || v_package_name || '.' || v_procedure_name || ' ' || v_errbuf);
            v_err_code := '2';
            xxcus_error_pkg.xxcus_error_main_api (
                p_called_from         => v_package_name
               ,p_calling             => v_procedure_name
               ,p_request_id          => NULL
               ,p_ora_error_msg       => SUBSTR (v_errbuf, 1, 2000)
               ,p_error_desc          => 'Error running ' || v_package_name || '.' || v_procedure_name
               ,p_distribution_list   => v_distro_list
               ,p_module              => 'XXWC');
            RETURN v_request_id;
    END xxwc_edi_810_request;

    --******************************************************************************
    --******************************************************************************
    --********* FUNCTION xxwc_edi_856_request (p_file_name IN VARCHAR2)**********************
    --******************************************************************************
    --******************************************************************************
    --******************************************************************************
    FUNCTION xxwc_edi_856_request (p_file_name IN VARCHAR2)
        RETURN NUMBER
    IS
        v_package_name     VARCHAR2 (256) := 'xxwc_edi_iface_pkg';
        v_procedure_name   VARCHAR2 (256) := 'xxwc_edi_856_request';
        v_request_id       NUMBER := 0;
        v_distro_list      VARCHAR2 (256) := 'HDSOracleDevelopers@hdsupply.com';
        v_errbuf           CLOB;
        v_err_code         VARCHAR2 (16) := '0';
        v_inbound_path     VARCHAR2 (256) := fnd_profile.VALUE ('ECE_IN_FILE_PATH');
    BEGIN
        v_request_id :=
            fnd_request.submit_request ('EC'
                                       ,'ECASNI'
                                       ,''
                                       ,''
                                       ,FALSE
                                       ,v_inbound_path                                               --Inbound File Path
                                       ,p_file_name                                                   --Inbound Datafile
                                       ,'Y'                                          -- Execute Open Interface Flag Flag
                                       ,'ASNI'                                                        --Transaction Type
                                       ,333                                                                   --Map Code
                                       ,0                                                          --Debug Mode for ASNI
                                       ,'UTF8');                                                --Data File Characterset
        COMMIT;

        UPDATE xxwc.xxwc_edi_inbound_files_history
           SET request_id = v_request_id, process_flag = 'W'
         WHERE file_name = p_file_name;

        COMMIT;

        RETURN v_request_id;
    EXCEPTION
        WHEN OTHERS
        THEN
            v_errbuf :=
                   'Error_Stack...'
                || CHR (10)
                || DBMS_UTILITY.format_error_stack ()
                || CHR (10)
                || ' Error_Backtrace...'
                || CHR (10)
                || DBMS_UTILITY.format_error_backtrace ();
            xxwc_log ('Error running ' || v_package_name || '.' || v_procedure_name || ' ' || v_errbuf);
            v_err_code := '2';
            xxcus_error_pkg.xxcus_error_main_api (
                p_called_from         => v_package_name
               ,p_calling             => v_procedure_name
               ,p_request_id          => v_request_id
               ,p_ora_error_msg       => SUBSTR (v_errbuf, 1, 2000)
               ,p_error_desc          => 'Error running ' || v_package_name || '.' || v_procedure_name
               ,p_distribution_list   => v_distro_list
               ,p_module              => 'XXWC');
            RETURN v_request_id;
    END xxwc_edi_856_request;

    --******************************************************************************
    --******************************************************************************
    --****** PROCEDURE get_concurrent_req_log_files******************
    --will move req.log files to iface directory
    --******************************************************************************
    --******************************************************************************
    --****************************************************************************
    PROCEDURE get_concurrent_req_log_files
    IS
        v_package_name          VARCHAR2 (256) := 'xxwc_edi_iface_pkg';
        v_procedure_name        VARCHAR2 (256) := 'get_concurrent_req_log_files';
        v_request_id            NUMBER := 0;
        v_distro_list           VARCHAR2 (256) := 'HDSOracleDevelopers@hdsupply.com';
        v_errbuf                CLOB;
        v_err_code              VARCHAR2 (16) := '0';
        v_inbound_path          VARCHAR2 (256) := fnd_profile.VALUE ('ECE_IN_FILE_PATH');
        vr_result               BOOLEAN;
        vr_phase                VARCHAR2 (64);
        vr_status               VARCHAR2 (64);
        vr_dev_phase            VARCHAR2 (64);
        vr_dev_status           VARCHAR2 (64);
        vr_debugging_message1   VARCHAR2 (264);
    BEGIN
        FOR r IN (SELECT file_name, request_id
                    FROM xxwc.xxwc_edi_inbound_files_history
                   WHERE process_flag = 'E' AND request_id IS NOT NULL)
        LOOP
            v_request_id :=
                fnd_request.submit_request (application   => 'XXWC'
                                           ,program       => 'XXWC_GET_CP_LOG_FILE'
                                           ,description   => NULL
                                           ,start_time    => SYSDATE
                                           ,sub_request   => FALSE
                                           ,argument1     => r.request_id
                                           ,argument2     => v_inbound_path);

            COMMIT;
            vr_result :=
                fnd_concurrent.wait_for_request (v_request_id
                                                ,2
                                                ,0
                                                ,vr_phase
                                                ,vr_status
                                                ,vr_dev_phase
                                                ,vr_dev_status
                                                ,vr_debugging_message1);
        END LOOP;
    EXCEPTION
        WHEN OTHERS
        THEN
            v_errbuf :=
                   'Error_Stack...'
                || CHR (10)
                || DBMS_UTILITY.format_error_stack ()
                || CHR (10)
                || ' Error_Backtrace...'
                || CHR (10)
                || DBMS_UTILITY.format_error_backtrace ();
            xxwc_log ('Error running ' || v_package_name || '.' || v_procedure_name || ' ' || v_errbuf);
            v_err_code := '2';
            xxcus_error_pkg.xxcus_error_main_api (
                p_called_from         => v_package_name
               ,p_calling             => v_procedure_name
               ,p_request_id          => v_request_id
               ,p_ora_error_msg       => SUBSTR (v_errbuf, 1, 2000)
               ,p_error_desc          => 'Error running ' || v_package_name || '.' || v_procedure_name
               ,p_distribution_list   => v_distro_list
               ,p_module              => 'XXWC');
    END get_concurrent_req_log_files;

    --******************************************************************************
    --******************************************************************************
    --*********PROCEDURE send_error_email ****************************************
    --******************************************************************************
    --******************************************************************************
    --******************************************************************************
    PROCEDURE send_error_email (p_request_id IN VARCHAR2, p_file_name IN VARCHAR2, p_email IN VARCHAR2)
    IS
        n                  NUMBER;
        v_distro_list      VARCHAR2 (256) := 'HDSOracleDevelopers@hdsupply.com';
        -- v_distro_list      VARCHAR2 (256) := 'rasikha.galimova@hdsupply.com';
        v_file_text        CLOB;
        v_line             CLOB;
        v_line_text        CLOB;
        v_file             VARCHAR2 (234);
        v_filehandle       UTL_FILE.file_type;
        v_filedir          VARCHAR2 (256) := fnd_profile.VALUE ('ECE_IN_FILE_PATH');
        v_body_header      CLOB;

        v_os_error         CLOB;
        v_sid              VARCHAR2 (164);
        v_request_id       VARCHAR2 (123);
        v_email            VARCHAR2 (164);
        v_package_name     VARCHAR2 (256) := 'xxwc_edi_iface_pkg';
        v_procedure_name   VARCHAR2 (256) := 'send_error_email';
        v_errbuf           CLOB;
        v_err_code         VARCHAR2 (16) := '0';
    BEGIN
        v_email := p_email;

        v_file := 'l' || p_request_id || '.log';
        UTL_FILE.fclose (v_filehandle);
        v_filehandle := UTL_FILE.fopen ('EDI_INBOUND', v_file, 'r');

        v_request_id := p_request_id;

        LOOP
            n := n + 1;

            BEGIN
                UTL_FILE.get_line (v_filehandle, v_line);

                IF v_line IS NULL
                THEN
                    NULL;
                ELSE
                    v_line_text := v_line_text || v_line || '<br>';
                END IF;
            EXCEPTION
                WHEN NO_DATA_FOUND
                THEN
                    EXIT;
            END;
        END LOOP;

        SELECT LOWER (name) INTO v_sid FROM v$database;

        v_body_header :=
               'ERROR REPORT FOR file '
            || p_file_name
            || ' EDI request_id  :'
            || v_request_id
            || ' from   '
            || UPPER (v_sid);
        v_file_text :=
               '<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html>
<head>
  <meta content="text/html; charset=ISO-8859-1"
 http-equiv="content-type">
  <title></title>
</head>
<body>'
            || '<h1 style="text-decoration: underline; font-family: monospace;"><small>'
            || v_body_header
            || '</small></h1>'
            || v_line_text
            || ' </body></html>';

        UTL_FILE.fclose (v_filehandle);
        xxcus_misc_pkg.html_email (p_to              => v_email
                                  ,p_from            => 'edi.errors@hdsupply.com'
                                  ,p_text            => 'test'
                                  ,p_subject         => v_body_header
                                  ,                                                     -- p_subject       => l_subject,
                                   p_html            => v_file_text
                                  ,p_smtp_hostname   => 'mailoutrelay.hdsupply.net'
                                  ,p_smtp_portnum    => '25');

        BEGIN
            UPDATE xxwc.xxwc_edi_inbound_files_history
               SET cp_log_file = v_file_text
             WHERE file_name = p_file_name;
        END;

        COMMIT;
    EXCEPTION
        WHEN OTHERS
        THEN
            BEGIN                                                                  --try to close the file if it is open
                UTL_FILE.fclose (v_filehandle);
            EXCEPTION
                WHEN OTHERS
                THEN
                    NULL;
            END;

            v_errbuf :=
                   'Error_Stack...'
                || CHR (10)
                || DBMS_UTILITY.format_error_stack ()
                || CHR (10)
                || ' Error_Backtrace...'
                || CHR (10)
                || DBMS_UTILITY.format_error_backtrace ();
            xxwc_log ('Error running ' || v_package_name || '.' || v_procedure_name || ' ' || v_errbuf);
            v_err_code := '2';
            xxcus_error_pkg.xxcus_error_main_api (
                p_called_from         => v_package_name
               ,p_calling             => v_procedure_name
               ,p_request_id          => v_request_id
               ,p_ora_error_msg       => SUBSTR (v_errbuf, 1, 2000)
               ,p_error_desc          => 'Error running ' || v_package_name || '.' || v_procedure_name
               ,p_distribution_list   => v_distro_list
               ,p_module              => 'XXWC');
    END send_error_email;

    --******************************************************************************
    --******************************************************************************/
    /*
1. Line_indicator -in the file line 6, start position 92, width 4;
2.  Vendor Name - not in the file, can be found  from invoice or PO(in the file)
3.  Invoice Number - in the file line 6, start position 101, width 50; -Line_indicator= 1000
4.  Invoice Date- in the file : line 6, start position 151, width 15; -Line_indicator= 1000
5.  P.O. Number (if applicable)- in the file line 6, start position 166, width 20;  -Line_indicator= 1000
6.  Invoice Amount - in the file line 6, start position 186, width 22;  -Line_indicator= 1000
7.  vendor_number := TRIM (SUBSTR (vnewline, 182, 35)); -Line_indicator= 0010

    */
    --******************************************************************************
    PROCEDURE populate_apex_fields (p_file_name IN VARCHAR2)
    IS
        vsfile             UTL_FILE.file_type;
        vnewline           VARCHAR2 (2000);
        n                  NUMBER := 0;
        v_file_name        VARCHAR2 (248) := p_file_name;
        v_directory_name   VARCHAR2 (248) := 'EDI_INBOUND';
        v_package_name     VARCHAR2 (256) := 'xxwc_edi_iface_pkg';
        v_procedure_name   VARCHAR2 (256) := 'populate_apex_fields';
        v_errbuf           CLOB;

        v_vendor_number    VARCHAR2 (64);
        v_vendor_name      VARCHAR2 (128);
        v_invoice_number   VARCHAR2 (64);
        v_invoice_date     VARCHAR2 (64);
        v_po_number        VARCHAR2 (64);
        v_invoice_amount   VARCHAR2 (64);
        v_line_indicator   VARCHAR2 (64);
    BEGIN
        vsfile := UTL_FILE.fopen (v_directory_name, v_file_name, 'r');

        v_vendor_number := NULL;

        IF UTL_FILE.is_open (vsfile)
        THEN
            LOOP
                v_line_indicator := NULL;

                BEGIN
                    BEGIN
                        UTL_FILE.get_line (vsfile, vnewline);
                        n := n + 1;
                        v_line_indicator := SUBSTR (vnewline, 92, 4);
                    EXCEPTION
                        WHEN NO_DATA_FOUND
                        THEN
                            EXIT;
                    END;

                    IF v_line_indicator = '1000'                                                                -- n = 6
                    THEN
                        v_invoice_number := SUBSTR (vnewline, 101, 50);
                        v_invoice_date := SUBSTR (vnewline, 151, 15);
                        v_po_number := SUBSTR (vnewline, 166, 20);
                        v_invoice_amount := SUBSTR (vnewline, 186, 22);

                        EXIT;
                    END IF;

                    IF v_line_indicator = '0010'                                                                -- n = 6
                    THEN
                        v_vendor_number := TRIM (SUBSTR (vnewline, 182, 35));

                        FOR t IN (SELECT vendor_name, segment1 vendor_number
                                    FROM po_vendors
                                   WHERE segment1 = v_vendor_number)
                        LOOP
                            v_vendor_name := t.vendor_name;
                        END LOOP;

                        IF v_vendor_number IS NULL
                        THEN
                            FOR t IN (SELECT vendor_name, segment1 vendor_number
                                        FROM po_vendors
                                       WHERE     vendor_id IN (SELECT vendor_id
                                                                 FROM po_headers
                                                                WHERE segment1 = TRIM (v_po_number))
                                             AND enabled_flag = 'Y')
                            LOOP
                                v_vendor_number := t.vendor_number;
                                v_vendor_name := t.vendor_name;
                            END LOOP;
                        END IF;
                    END IF;
                END;
            END LOOP;

            UTL_FILE.fclose (vsfile);
        --  COMMIT;
        END IF;

        FOR r IN (SELECT l.ROWID v_rowid, l.*
                    FROM xxwc.xxwc_edi_inbound_files_history l
                   WHERE file_name = p_file_name)
        LOOP
            IF v_vendor_number IS NULL AND v_po_number IS NOT NULL
            THEN
                FOR t IN (SELECT vendor_name, segment1 vendor_number
                            FROM po_vendors
                           WHERE     vendor_id IN (SELECT vendor_id
                                                     FROM po_headers
                                                    WHERE segment1 = TRIM (v_po_number))
                                 AND enabled_flag = 'Y')
                LOOP
                    v_vendor_number := t.vendor_number;
                    v_vendor_name := t.vendor_name;
                END LOOP;
            END IF;

            UPDATE xxwc.xxwc_edi_inbound_files_history
               SET invoice_number = v_invoice_number
                  ,invoice_date = v_invoice_date
                  ,po_number = v_po_number
                  ,invoice_amount = v_invoice_amount
                  ,vendor_number = v_vendor_number
                  ,vendor_name = v_vendor_name
             WHERE ROWID = r.v_rowid;
        END LOOP;

        COMMIT;
    EXCEPTION
        WHEN OTHERS
        THEN
            BEGIN                                                                  --try to close the file if it is open
                UTL_FILE.fclose (vsfile);
            EXCEPTION
                WHEN OTHERS
                THEN
                    NULL;
            END;

            v_errbuf :=
                   'Error_Stack...'
                || CHR (10)
                || DBMS_UTILITY.format_error_stack ()
                || CHR (10)
                || ' Error_Backtrace...'
                || CHR (10)
                || DBMS_UTILITY.format_error_backtrace ();
            xxwc_log ('Error running ' || v_package_name || '.' || v_procedure_name || ' ' || v_errbuf);
    END populate_apex_fields;

    --******************************************************************************
    --******************************************************************************
    --******************************************************************************

    FUNCTION get_invoice_date (p_file_name IN VARCHAR2)
        RETURN DATE
    IS
        vsfile             UTL_FILE.file_type;
        vnewline           VARCHAR2 (2000);
        n                  NUMBER := 0;
        v_file_name        VARCHAR2 (248) := p_file_name;
        v_directory_name   VARCHAR2 (248) := 'EDI_INBOUND';
        v_package_name     VARCHAR2 (256) := 'xxwc_edi_iface_pkg';
        v_procedure_name   VARCHAR2 (256) := 'READ_810_FILE';
        v_errbuf           CLOB;
        v_invoice_date     DATE;
    BEGIN
        vsfile := UTL_FILE.fopen (v_directory_name, v_file_name, 'r');

        --     VSFILE := UTL_FILE.FOPEN ('/xx_iface/ebizfqa/inbound/EDI', V_FILE_NAME, 'r');

        IF UTL_FILE.is_open (vsfile)
        THEN
            LOOP
                BEGIN
                    BEGIN
                        UTL_FILE.get_line (vsfile, vnewline);
                        n := n + 1;
                    EXCEPTION
                        WHEN NO_DATA_FOUND
                        THEN
                            EXIT;
                    END;

                    IF n = 6
                    THEN
                        v_invoice_date := TO_DATE (SUBSTR (vnewline, 151, 8), 'YYYYMMDD');

                        EXIT;
                    END IF;
                END;
            END LOOP;

            UTL_FILE.fclose (vsfile);
        --  COMMIT;
        END IF;

        RETURN v_invoice_date;
    EXCEPTION
        WHEN OTHERS
        THEN
            BEGIN                                                                  --try to close the file if it is open
                UTL_FILE.fclose (vsfile);
                RETURN NULL;
            EXCEPTION
                WHEN OTHERS
                THEN
                    NULL;
            END;

            v_errbuf :=
                   'Error_Stack...'
                || CHR (10)
                || DBMS_UTILITY.format_error_stack ()
                || CHR (10)
                || ' Error_Backtrace...'
                || CHR (10)
                || DBMS_UTILITY.format_error_backtrace ();
            xxwc_log ('Error running ' || v_package_name || '.' || v_procedure_name || ' ' || v_errbuf);
    END get_invoice_date;

    --******************************************************************************
    --******************************************************************************
    --******************************************************************************

    PROCEDURE xxwc_log (p_message VARCHAR2)
    IS
    BEGIN
        INSERT INTO xxwc.xxwc_edi_log (insert_date, log_message)
             VALUES (SYSDATE, p_message);

        DELETE from xxwc.xxwc_edi_log
         WHERE insert_date < SYSDATE - 14;
         
        COMMIT;
        fnd_file.put_line (fnd_file.LOG, p_message);
    END xxwc_log;

    --******************************************************************************
    --******************************************************************************
    --******************************************************************************
    PROCEDURE rename_edi_working_out_files (p_errbuf OUT NOCOPY VARCHAR2, p_retcode OUT NOCOPY VARCHAR2)
    -- this program will take files from running  'OUT: Purchase Order (850/ORDERS)'
    --,'OUT: Purchase Order Change (860/ORDCHG)' concurrent programs and RENAME file extention from  .tmp to .dat
    --tms ticket 20130415-00522
    --concurrent program:
    --scheduled every 5 min and incompa
    IS
        v_database_name                VARCHAR2 (124);
        v_edi_out_directory_name       VARCHAR2 (124) := NULL;
        v_edi_out_directory_path       VARCHAR2 (124) := NULL;
        v_external_tmp_table           VARCHAR2 (124) := NULL;
        v_new_file_name                VARCHAR2 (124) := NULL;
        v_exit_exception               EXCEPTION;

        v_cp_running                   NUMBER := 0;
        v_errbuf                       CLOB;
        v_poco_concurrent_program_id   NUMBER;
        v_poo_concurrent_program_id    NUMBER;
    BEGIN
        --run this program only when parent program finished processing
        --find concurrent program id
        BEGIN
            SELECT concurrent_program_id
              INTO v_poo_concurrent_program_id
              FROM fnd_concurrent_programs_tl
             WHERE user_concurrent_program_name = 'OUT: Purchase Order (850/ORDERS)';
        EXCEPTION
            WHEN OTHERS
            THEN
                NULL;
        END;

        BEGIN
            SELECT concurrent_program_id
              INTO v_poco_concurrent_program_id
              FROM fnd_concurrent_programs_tl
             WHERE user_concurrent_program_name = 'OUT: Purchase Order Change (860/ORDCHG)';
        EXCEPTION
            WHEN OTHERS
            THEN
                NULL;
        END;

        IF v_cp_running = 0
        THEN
            SELECT SUBSTR (LOWER (name), 1, 7)
              INTO v_database_name
              FROM v$database
             WHERE ROWNUM = 1;

            v_edi_out_directory_path := '/xx_iface/' || v_database_name || '/outbound/EDI';

            -- check that edi_outbound oracle directory exists:
            FOR r IN (SELECT directory_name
                        FROM all_directories
                       WHERE directory_path = v_edi_out_directory_path)
            LOOP
                v_edi_out_directory_name := r.directory_name;
            END LOOP;

            IF v_edi_out_directory_name IS NULL
            THEN
                EXECUTE IMMEDIATE
                    ' CREATE OR REPLACE DIRECTORY  XXWC_EDI_OUTBOUND_DIR  as ''' || v_edi_out_directory_path || '''';

                EXECUTE IMMEDIATE 'GRANT ALL ON DIRECTORY XXWC_EDI_OUTBOUND_DIR TO PUBLIC';
            END IF;

            --check again that directory exists
            FOR r IN (SELECT directory_name
                        FROM all_directories
                       WHERE directory_path = v_edi_out_directory_path)
            LOOP
                v_edi_out_directory_name := r.directory_name;
            END LOOP;

            -- check that external custom table exist for the directory
            FOR r IN (SELECT table_name
                        FROM all_tables
                       WHERE table_name = 'DIRECTORY_LISTING' AND owner = 'XXWC')
            LOOP
                v_external_tmp_table := r.table_name;
            END LOOP;

            --BELOW LINE WILL POPULATE TEMP TABLE   WITH directory content
            --poo files
            apps.xxwc_list_directory (v_edi_out_directory_path);

            FOR r IN (SELECT name
                        FROM xxwc.directory_listing
                       WHERE name LIKE '%POO%tmp')
            LOOP
                v_new_file_name := REPLACE (r.name, '.tmp', '.dat');                                --change to real one
                v_cp_running := check_cp_is_running_now (v_poo_concurrent_program_id);

                IF v_cp_running = 0
                THEN
                    UTL_FILE.frename ('XXWC_EDI_OUTBOUND_DIR'
                                     ,r.name
                                     ,'XXWC_EDI_OUTBOUND_DIR'
                                     ,v_new_file_name
                                     ,FALSE);
                END IF;
            END LOOP;

            COMMIT;
            --poco files
            apps.xxwc_list_directory (v_edi_out_directory_path);

            FOR r IN (SELECT name
                        FROM xxwc.directory_listing
                       WHERE name LIKE '%POCO%tmp')
            LOOP
                v_new_file_name := REPLACE (r.name, '.tmp', '.dat');                                --change to real one
                v_cp_running := check_cp_is_running_now (v_poco_concurrent_program_id);

                IF v_cp_running = 0
                THEN
                    UTL_FILE.frename ('XXWC_EDI_OUTBOUND_DIR'
                                     ,r.name
                                     ,'XXWC_EDI_OUTBOUND_DIR'
                                     ,v_new_file_name
                                     ,FALSE);
                END IF;
            END LOOP;
        END IF;

        p_retcode := 0;
        COMMIT;
    EXCEPTION
        WHEN OTHERS
        THEN
            p_retcode := 2;
            v_errbuf :=
                   ' Error_Stack...'
                || CHR (10)
                || DBMS_UTILITY.format_error_stack ()
                || CHR (10)
                || ' Error_Backtrace...'
                || CHR (10)
                || DBMS_UTILITY.format_error_backtrace ();
            xxwc_log ('Error running ' || 'xxwc_edi_iface_pkg' || '.' || 'rename_WORKING_file' || ' ' || v_errbuf);
    END;

    -----***********************************************************
    -----***********************************************************
    FUNCTION check_cp_is_running_now (p_concurrent_program_id NUMBER)
        RETURN NUMBER
    IS
        v_return   NUMBER := 0;
    BEGIN
        FOR r
            IN (SELECT 1
                  FROM fnd_concurrent_requests
                 WHERE     concurrent_program_id = p_concurrent_program_id
                       AND actual_start_date > SYSDATE - 1
                       AND status_code = 'R')
        LOOP
            v_return := 1;
        END LOOP;

        RETURN v_return;
    END;
END;
/

-- Grants for Package Body
GRANT EXECUTE ON apps.xxwc_edi_iface_pkg TO xxwc_prism_execute_role
/
GRANT EXECUTE ON apps.xxwc_edi_iface_pkg TO bs006141
/
GRANT EXECUTE ON apps.xxwc_edi_iface_pkg TO xxwc_dev_admin WITH GRANT OPTION
/
GRANT EXECUTE ON apps.xxwc_edi_iface_pkg TO interface_prism
/
GRANT EXECUTE ON apps.xxwc_edi_iface_pkg TO interface_xxcus
/

-- End of DDL Script for Package Body APPS.XXWC_EDI_IFACE_PKG
