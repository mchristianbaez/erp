 /****************************************************************************************************
  Script: TMS_20170624-00005_Datafix.sql
  Description: Costing error -CST_MATCH_TXFR_CG_TXFR_ORG
  HISTORY
  =====================================================================================================
  VERSION DATE               AUTHOR(S)       DESCRIPTION
  ------- -----------------  --------------- ----------------------------------------------------------
  -- 1.0     24-JUN-2017   P.Vamshidhar       TMS#20170624-00005 - Stuck transaction
  *****************************************************************************************************/
SET SERVEROUT ON
  
BEGIN

DBMS_OUTPUT.PUT_LINE('Before Update');

delete from mtl_material_transactions_temp where transaction_header_id=590037730;
  
DBMS_OUTPUT.PUT_LINE('After Update - Records: '||SQL%ROWCOUNT);
COMMIT;
DBMS_OUTPUT.PUT_LINE('Commit completed');
EXCEPTION
WHEN OTHERS THEN
DBMS_OUTPUT.PUT_LINE('Error Occred '||SUBSTR(SQLERRM,1,250));
END;
/
