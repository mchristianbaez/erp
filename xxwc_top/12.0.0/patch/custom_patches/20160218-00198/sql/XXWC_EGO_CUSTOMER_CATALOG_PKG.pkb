CREATE OR REPLACE PACKAGE BODY APPS.XXWC_EGO_CUSTOMER_CATALOG_PKG
/******************************************************************************************************
-- File Name: XXWC_EGO_CUSTOMER_CATALOG_PKG.pkb
--
-- PROGRAM TYPE: PL/SQL Script   <API>
--
-- PURPOSE: Developed to Extract PDH Catalog data for required customer
-- HISTORY
-- ==========================================================================================================
-- ==========================================================================================================
-- VERSION DATE          AUTHOR(S)       DESCRIPTION
-- ------- -----------   --------------- --------------------------------------------------------------------
-- 1.0     17-FEB-2016   P.Vamshidhar   TMS#20160219-00063 - External Customer Catalog Extract
-- 1.1     06-JUN-2016   P.Vamshidhar   TMS#20160219-00063 - External Customer Catalog Extract
--                                      Post Production issues fixed.
--1.2      19-Aug-2016   P.Vamshidhar   TMS#20160719-00044 - Kiewit Phase 2  - Add additional attribute in Data Extract
--                                      TMS#20160817-00060 - Kiewit- Customer Catalog Extract Staging prg fix
--                                      TMS#20160603-00067 - Kiewit Phase 2 - Upload Data to PDH
--                                      TMS#20160822-00181 - Kiewit Phase 2  - Additional Requirement for Multi-sourced products.
--1.3      19-Sep-2016   P.Vamshidhar   TMS#20160602-00013 - Kiewit Phase 2 -FTP Extract files to Kiewit
--1.4      30-Sep-2016   P.Vamshidhar   TMS#20160930-00032 - Kiewit Phase 2 - Features and Benefits issue fix.
--1.5      01-Jun-2017   P.Vamshidhar   TMS#20170421-00199 - UPC Validation XXWC B2B Customer Catalog
           26-Jun-2017   P.Vamshidhar   TMS#20170412-00044 - Extracting EGO pages data.
--1.6      26-APR-2018   Nancy Pahwa    TMS#20160218-00198   AR Writeoff and AR unapply reapply

************************************************************************************************************/
IS
   g_sec            VARCHAR2 (1000);
   g_err_callfrom   VARCHAR2 (100) := 'XXWC_EGO_CUSTOMER_CATALOG_PKG';
   g_distro_list    VARCHAR2 (100) := 'HDSOracleDevelopers@hdsupply.com';
   g_procedure      VARCHAR2 (100);
   -- Added below global variables in Rev 1.2 - begin
   g_line_open      VARCHAR2 (100)
      := '/*************************************************************************';
   g_line_close     VARCHAR2 (100)
      := '**************************************************************************/';
   g_user_id        FND_USER.USER_ID%TYPE;
   g_request_id     NUMBER := FND_GLOBAL.CONC_REQUEST_ID;

   -- Added Above global variables in Rev 1.2 - End

   PROCEDURE Generate_txt (x_err_buf               OUT VARCHAR2,
                           x_ret_code              OUT VARCHAR2,
                           p_export_type        IN     VARCHAR2,
                           p_customer_id        IN     NUMBER,
                           p_customer_catalog   IN     VARCHAR2,
                           p_extract_type       IN     VARCHAR2,
                           p_from_date          IN     VARCHAR2)
   /******************************************************************************************************
   -- PROGRAM TYPE: Procedure
   -- Name: Generate_txt
   -- PURPOSE: Developed to Extract PDH Catalog data for required customer
   -- HISTORY
   -- ==========================================================================================================
   -- ==========================================================================================================
   -- VERSION DATE          AUTHOR(S)       DESCRIPTION
   -- ------- -----------   --------------- --------------------------------------------------------------------
   -- 1.0     17-FEB-2016   P.Vamshidhar   TMS#20160219-00063 - External Customer Catalog Extract
   -- 1.1     06-JUN-2016   P.Vamshidhar   TMS#20160219-00063 - External Customer Catalog Extract
                                           Post Production issues fixed.
   --1.2      19-Aug-2016   P.Vamshidhar   TMS#20160719-00044 - Kiewit Phase 2  - Add additional attribute in Data Extract
                                           TMS#20160822-00181 - Kiewit Phase 2  - Additional Requirement for
                                           Multi-sourced products.
   --1.3      19-Sep-2016   P.Vamshidhar   TMS#20160602-00013 - Kiewit Phase 2 -FTP Extract files to Kiewit
   --1.5      26-Jun-2017   P.Vamshidhar   TMS#20170412-00044 - Extracting EGO pages data.
   ************************************************************************************************************/

   IS
      -- re-wrote below query in 1.1
      CURSOR CUR_ITEM_CATLOG (
         cp_customer_id        VARCHAR2,
         p_customer_catalog    VARCHAR2,
         ld_from_date          DATE)
      IS
         SELECT DISTINCT ITEM_NUMBER, CUST_CATALOG_NAME -- Added cust_catalog_name in Rev 1.2
           FROM XXWC.XXWC_B2B_CAT_CUSTOMER_TBL xct
          WHERE     NVL (cp_customer_id, xct.CUSTOMER_ID) = xct.CUSTOMER_ID
                AND NVL (UPPER (P_CUSTOMER_CATALOG),
                         UPPER (xct.CUST_CATALOG_NAME)) =
                       UPPER (xct.CUST_CATALOG_NAME)
                AND EXISTS
                       (SELECT 1
                          FROM APPS.MTL_SYSTEM_ITEMS_B msib
                         WHERE     msib.LAST_UPDATE_DATE >=
                                      NVL (ld_from_date,
                                           msib.LAST_UPDATE_DATE)
                               AND msib.organization_id = 222
                               AND msib.inventory_item_id =
                                      xct.inventory_item_id);


      CURSOR CUR_EGO_DATA (
         P_ITEM_NUMBER    VARCHAR2,
         P_IMAGE_URL      VARCHAR2,
         p_extrat_type    VARCHAR2,
         p_customer_id    NUMBER)
      IS
           SELECT B.INVENTORY_ITEM_ID,
                  2 SORTORDER,
                  B.SEGMENT1 item_number,
                  C.SEGMENT1 Catalog_name,
                  A.ATTR_GROUP_DISP_NAME,
                  a.ATTRIBUTE_GROUP_NAME,
                  a.ATTR_DISPLAY_NAME,
                  a.ATTRIBUTE_NAME,
                  COALESCE (
                     DECODE (
                        a.ATTRIBUTE_NAME,
                        'XXWC_A_F_1_ATTR',    P_IMAGE_URL
                                           || '/'
                                           || ATTRIBUTE_CHAR_VALUE, -- Changed in Rev 1.2
                        ATTRIBUTE_CHAR_VALUE),
                     TO_CHAR (ATTRIBUTE_NUMBER_VALUE),
                     TO_CHAR (TO_DATE (ATTRIBUTE_DATE_VALUE, 'mm/dd/yyyy'),
                              'DD-MON-YYYY'))
                     ATTRIBUTE_VALUE
             FROM APPS.XXWC_EGO_EXTN_CUST_ATTR_V A,
                  APPS.MTL_SYSTEM_ITEMS_B B,
                  MTL_ITEM_CATALOG_GROUPS_B_KFV C
            WHERE     A.INVENTORY_ITEM_ID = B.INVENTORY_ITEM_ID
                  AND A.ORGANIZATION_ID = B.ORGANIZATION_ID
                  AND b.ITEM_CATALOG_GROUP_ID = C.ITEM_CATALOG_GROUP_ID
                  AND B.SEGMENT1 = P_ITEM_NUMBER
                  AND (   (p_extrat_type = 'Full Extract')
                       OR (    p_extrat_type = 'Data Extract'
                           AND (   A.ATTRIBUTE_CHAR_VALUE IS NOT NULL
                                OR A.ATTRIBUTE_NUMBER_VALUE IS NOT NULL
                                OR A.ATTRIBUTE_DATE_VALUE IS NOT NULL))
                       OR (    p_extrat_type = 'Blank Extract'
                           AND A.ATTRIBUTE_CHAR_VALUE IS NULL
                           AND A.ATTRIBUTE_NUMBER_VALUE IS NULL
                           AND A.ATTRIBUTE_DATE_VALUE IS NULL))
                  AND (   p_customer_id IS NULL
                       OR (    p_customer_id IS NOT NULL
                           AND NOT EXISTS
                                  (SELECT 1
                                     FROM XXWC.XXWC_B2B_CAT_ATTR_TBL XCAT
                                    WHERE     p_customer_id = XCAT.CUSTOMER_ID
                                          AND A.ATTRIBUTE_ID = XCAT.ATTR_ID)))
         ORDER BY B.INVENTORY_ITEM_ID,
                  a.ATTRIBUTE_GROUP_NAME,
                  a.ATTRIBUTE_NAME;

      CURSOR CUR_ITEM_MAST (
         P_ITEM_NUMBER    VARCHAR2)
      IS
         SELECT MSIB.INVENTORY_ITEM_ID,
                MSIB.DESCRIPTION,
                MSIB.SEGMENT1 ITEM_NUMBER,
                MSIB.PRIMARY_UOM_CODE UOM,
                REPLACE (REPLACE (MSIB.LONG_DESCRIPTION, CHR (10), NULL),
                         CHR (13),
                         NULL)
                   LONG_DESCRIPTION,
                MSIB.SHELF_LIFE_DAYS,
                MSIB.ATTRIBUTE22 TAXWARE_CODE,
                MSIB.SEGMENT1 MSDS,
                MSIB.UNIT_WEIGHT SHIPPING_WEIGHT,
                MSIB.HAZARDOUS_MATERIAL_FLAG HAZMAT_FLAG,
                DECODE (MSIB.HAZARDOUS_MATERIAL_FLAG, 'Y', 'Yes', 'No')
                   Hazmat,
                MSIB.FIXED_LOT_MULTIPLIER,
                (SELECT CATEGORY_CONCAT_SEGS
                   FROM apps.MTL_ITEM_CATEGORIES_V
                  WHERE     inventory_item_id = msib.inventory_item_id
                        AND organization_id = 222
                        AND CATEGORY_SET_NAME = 'Inventory Category')
                   Cat_Class,
                (SELECT CATEGORY_CONCAT_SEGS
                   FROM apps.MTL_ITEM_CATEGORIES_V
                  WHERE     inventory_item_id = msib.inventory_item_id
                        AND organization_id = 222
                        AND CATEGORY_SET_NAME = 'WC Web Hierarchy')
                   WC_Web_Hierarchy,
                (SELECT CATEGORY_CONCAT_SEGS
                   FROM apps.MTL_ITEM_CATEGORIES_V
                  WHERE     inventory_item_id = msib.inventory_item_id
                        AND organization_id = 222
                        AND CATEGORY_SET_NAME = 'Time Sensitive')
                   Time_Sensitive,
                item_catalog_group_id                      -- Added in Rev 1.5
           FROM APPS.MTL_SYSTEM_ITEMS_VL MSIB
          WHERE     MSIB.ORGANIZATION_ID = 222
                AND MSIB.INVENTORY_ITEM_STATUS_CODE NOT IN ('Inactive',
                                                            'Discontinu',
                                                            'New Item')
                AND MSIB.SEGMENT1 = P_ITEM_NUMBER;

      -- Re-wrote cursor query in Rev 1.2
      CURSOR CUR_CREFF_MFG (
         p_inventory_item_id    NUMBER)
      IS
           SELECT MCR.CROSS_REFERENCE, MCR.ATTRIBUTE1, aps.vendor_name
             FROM APPS.MTL_CROSS_REFERENCES mcr, apps.ap_suppliers aps
            WHERE     CROSS_REFERENCE_TYPE = 'VENDOR'
                  AND mcr.INVENTORY_ITEM_ID = p_inventory_item_id
                  AND MCR.ATTRIBUTE1 = aps.segment1(+)
                  AND ROWNUM < 6
         ORDER BY mcr.creation_date;

      -- Re-wrote cursor query in Rev 1.2
      CURSOR CUR_CREF_UPC (
         p_inventory_item_id    NUMBER)
      IS
           SELECT MCR.CROSS_REFERENCE, aps.vendor_name attribute1
             FROM APPS.MTL_CROSS_REFERENCES mcr, apps.ap_suppliers aps
            WHERE     mcr.CROSS_REFERENCE_TYPE = 'UPC'
                  AND mcr.INVENTORY_ITEM_ID = p_inventory_item_id
                  AND MCR.ATTRIBUTE1 = aps.segment1(+)
                  AND ROWNUM < 6
         ORDER BY mcr.creation_date;

      -- Added cursor in Rev 1.5 by Vamshi
      CURSOR CUR_EGO_PAGE_DATA (
         p_item_catalog_group_id    NUMBER,
         p_inventory_item_id        NUMBER,
         p_item_number              VARCHAR2,
         p_extrat_type              VARCHAR2)
      IS
         SELECT p_inventory_item_id,
                2 SORTORDER,
                p_item_number item_number,
                micg.SEGMENT1 Catalog_name,
                ag.ATTR_GROUP_DISP_NAME,
                ag.ATTR_GROUP_NAME ATTRIBUTE_GROUP_NAME,
                agc.ATTR_DISPLAY_NAME,
                agc.ATTR_NAME ATTRIBUTE_NAME,
                NULL ATTRIBUTE_VALUE
           FROM EGO_ATTRS_V AGC,
                EGO_ATTR_GROUPS_V AG,
                EGO_PAGE_ENTRIES_V EPEV,
                MTL_ITEM_CATALOG_GROUPS micg
          WHERE     1 = 1
                AND AGC.APPLICATION_ID = AG.APPLICATION_ID
                AND AGC.ATTR_GROUP_TYPE = AG.ATTR_GROUP_TYPE
                AND AGC.ATTR_GROUP_NAME = AG.ATTR_GROUP_NAME
                AND AGC.DISPLAY_CODE != 'H'
                AND AGC.ENABLED_FLAG = 'Y'
                AND micg.item_catalog_group_id = p_item_catalog_group_id
                AND ag.ATTR_GROUP_NAME = EPEV.ATTR_GROUP_NAME
                AND epev.PAGE_ENTRY_CLASS_CODE = micg.item_catalog_group_id
                AND EXISTS
                       (SELECT 1
                          FROM EGO_MTL_SY_ITEMS_EXT_B EMSI
                         WHERE EMSI.ITEM_CATALOG_GROUP_ID =
                                  micg.ITEM_CATALOG_GROUP_ID)
                AND NOT EXISTS
                           (SELECT 1
                              FROM EGO_MTL_SY_ITEMS_EXT_B EMSI
                             WHERE     1 = 1
                                   AND ORGANIZATION_ID = 222
                                   AND EMSI.ITEM_CATALOG_GROUP_ID =
                                          micg.ITEM_CATALOG_GROUP_ID
                                   AND emsi.inventory_item_id =
                                          p_inventory_item_id
                                   AND AG.ATTR_GROUP_ID = EMSI.ATTR_GROUP_ID)
                AND p_extrat_type IN ('Full Extract', 'Blank Extract');

      x_file_path        VARCHAR2 (100);
      lvc_file_name      VARCHAR2 (100);
      lvc_file           UTL_FILE.FILE_TYPE;
      lvc_outbound_dir   VARCHAR2 (100);
      MAX_LINE_LENGTH    BINARY_INTEGER := 32767;
      lvc_string         VARCHAR2 (3000) := NULL;
      ln_count           NUMBER := 1;
      ln_mast_org        NUMBER;
      EXECUTION_ERROR    EXCEPTION;
      ln_upc_count       NUMBER;
      ln_vendor_count    NUMBER;
      l_err_msg          VARCHAR2 (2000);
      l_customer_name    VARCHAR2 (2000);
      l_image_flag       VARCHAR2 (1);                     -- Added in Rev 1.2
      lvc_msds_url       VARCHAR2 (1000)
         := FND_PROFILE.VALUE ('XXWC_CUST_CATALOG_SITEHAWK_URL');
      lvc_images_url     VARCHAR2 (1000)
         := FND_PROFILE.VALUE ('XXWC_INV_WC_WEBSITE_IMAGE_URL');
      lp_from_date       DATE
                            := TO_DATE (P_FROM_DATE, 'YYYY/MM/DD HH24:MI:SS');
      lvc_conc_program   VARCHAR2 (100)
                            := 'XXWC Customer Catalog Extract Program';
      ln_request_id      NUMBER := FND_GLOBAL.CONC_REQUEST_ID;
   BEGIN
      -- Initializing variables
      g_sec := 'Generate_txt - Procedure  - Start';
      g_procedure := 'Generate_txt';

      Fnd_file.put_line (fnd_file.LOG, g_sec);
      fnd_file.put_line (fnd_file.LOG, 'Parameters ');
      fnd_file.put_line (fnd_file.LOG, 'Export Type  : ' || p_export_type);
      fnd_file.put_line (fnd_file.LOG, 'Customer Id  : ' || p_customer_id);
      fnd_file.put_line (fnd_file.LOG,
                         'Cust Catalog : ' || p_customer_catalog);
      fnd_file.put_line (fnd_file.LOG, 'Extract Type : ' || p_extract_type);
      fnd_file.put_line (fnd_file.LOG, 'From Date    : ' || p_from_date);
      fnd_file.put_line (fnd_file.LOG, 'lp_from_date : ' || lp_from_date);

      -- Date validation

      IF P_FROM_DATE IS NULL
      THEN
         BEGIN
            SELECT MAX (ACTUAL_COMPLETION_DATE)
              INTO lp_from_date
              FROM fnd_conc_req_summary_v
             WHERE     USER_CONCURRENT_PROGRAM_NAME = lvc_conc_program
                   AND STATUS_CODE = 'C'
                   AND request_id < ln_request_id;
         EXCEPTION
            WHEN OTHERS
            THEN
               lp_from_date := NULL;
         END;
      END IF;

      fnd_file.put_line (fnd_file.LOG,
                         'Final lp_from_date : ' || lp_from_date);

      -- Below code added in Rev 1.3 by Vamshi -  Begin

      IF p_export_type = 'Customer Extract'
      THEN
         lvc_outbound_dir := 'XXWC_INV_CUSTOMER_CATALOG';
      ELSE
         lvc_outbound_dir := 'XXWC_INV_DATACOLLECT_CATALOG';
      END IF;

      fnd_file.put_line (
         fnd_file.LOG,
         'Outbound Destination Directory : ' || lvc_outbound_dir);

      -- Above code added in Rev 1.3 by Vamshi -  End

      IF p_customer_id IS NOT NULL
      THEN
         BEGIN
            SELECT SUBSTR (party_name, 1, 50)
              INTO l_customer_name
              FROM apps.hz_parties
             WHERE party_id = p_customer_id;

            fnd_file.put_line (fnd_file.LOG,
                               'Customer Name ' || l_customer_name);
         EXCEPTION
            WHEN OTHERS
            THEN
               l_customer_name := 'Customer_Catalog_Extract';
         END;
      ELSE
         l_customer_name := 'Customer_Catalog_Extract';
      END IF;

      lvc_file_name :=
            REPLACE (TRANSLATE (l_customer_name, '()&/*-+', '_'), ' ', '_')
         || TO_CHAR (SYSDATE, 'MMDDYYYYHH24MISS')
         || '.dat';

      fnd_file.put_line (fnd_file.LOG, 'lvc_file_name ' || lvc_file_name);

      g_sec := 'Opening file at OS level';
      -- Opening File
      lvc_file :=
         UTL_FILE.FOPEN (lvc_outbound_dir,
                         lvc_file_name,
                         'W',
                         MAX_LINE_LENGTH);

      -- Writing Header
      lvc_string :=
         '|ITEM_NUMBER|ICC|ATTR_GROUP_DISP_NAME|ATTR_DISPLAY_NAME|ATTRIBUTE_VALUE|';
      UTL_FILE.PUT_LINE (lvc_file, lvc_string);

      g_sec := 'Data Extraction start';


      FOR REC_ITEM_CATLOG
         IN CUR_ITEM_CATLOG (p_customer_id, p_customer_catalog, lp_from_date)
      LOOP
         g_sec := 'Catalog level';

         FOR REC_ITEM_MAST IN CUR_ITEM_MAST (REC_ITEM_CATLOG.ITEM_NUMBER)
         LOOP
            g_sec := REC_ITEM_MAST.ITEM_NUMBER || ' Item Stated';
            fnd_file.put_line (fnd_file.LOG, g_sec);


            IF p_extract_type IN ('Full Extract', 'Data Extract')
            THEN
               -- Description attribute
               lvc_string := NULL;
               lvc_string :=
                     '|'
                  || REC_ITEM_MAST.ITEM_NUMBER
                  || '|MASTER|ATTRIBUTE|Description|'
                  || REC_ITEM_MAST.DESCRIPTION
                  || '|';
               UTL_FILE.PUT_LINE (lvc_file, lvc_string);
            END IF;

            --UOM Attribute
            IF p_extract_type = 'Full Extract'
            THEN
               lvc_string := NULL;
               lvc_string :=
                     '|'
                  || REC_ITEM_MAST.ITEM_NUMBER
                  || '|MASTER|ATTRIBUTE|UOM|'
                  || REC_ITEM_MAST.UOM
                  || '|';
               UTL_FILE.PUT_LINE (lvc_file, lvc_string);

               --LONG_DESCRIPTION Attribute
               lvc_string := NULL;
               lvc_string :=
                     '|'
                  || REC_ITEM_MAST.ITEM_NUMBER
                  || '|MASTER|ATTRIBUTE|Long Description|'
                  || REC_ITEM_MAST.LONG_DESCRIPTION
                  || '|';
               UTL_FILE.PUT_LINE (lvc_file, lvc_string);

               -- Shelf life days Attribute
               lvc_string := NULL;
               lvc_string :=
                     '|'
                  || REC_ITEM_MAST.ITEM_NUMBER
                  || '|MASTER|ATTRIBUTE|Shelf Life Days|'
                  || REC_ITEM_MAST.SHELF_LIFE_DAYS
                  || '|';
               UTL_FILE.PUT_LINE (lvc_file, lvc_string);

               --TAXWARE_CODE Attribute
               lvc_string := NULL;
               lvc_string :=
                     '|'
                  || REC_ITEM_MAST.ITEM_NUMBER
                  || '|MASTER|ATTRIBUTE|Taxware Code|'
                  || REC_ITEM_MAST.TAXWARE_CODE
                  || '|';
               UTL_FILE.PUT_LINE (lvc_file, lvc_string);

               -- SHIPPING_WEIGHT Attribute
               lvc_string := NULL;
               lvc_string :=
                     '|'
                  || REC_ITEM_MAST.ITEM_NUMBER
                  || '|MASTER|ATTRIBUTE|Shipping Weight|'
                  || REC_ITEM_MAST.SHIPPING_WEIGHT
                  || '|';
               UTL_FILE.PUT_LINE (lvc_file, lvc_string);

               -- HAZMAT Attribute
               lvc_string := NULL;
               lvc_string :=
                     '|'
                  || REC_ITEM_MAST.ITEM_NUMBER
                  || '|MASTER|ATTRIBUTE|Hazardous Material|'
                  || REC_ITEM_MAST.Hazmat
                  || '|';
               UTL_FILE.PUT_LINE (lvc_file, lvc_string);

               -- Fixed Lot Multiplier Attribute
               lvc_string := NULL;
               lvc_string :=
                     '|'
                  || REC_ITEM_MAST.ITEM_NUMBER
                  || '|MASTER|ATTRIBUTE|Fixed Lot Multiplier|'
                  || REC_ITEM_MAST.FIXED_LOT_MULTIPLIER
                  || '|';
               UTL_FILE.PUT_LINE (lvc_file, lvc_string);

               -- Category Class
               lvc_string := NULL;
               lvc_string :=
                     '|'
                  || REC_ITEM_MAST.ITEM_NUMBER
                  || '|MASTER|ATTRIBUTE|Category Class|'
                  || REC_ITEM_MAST.Cat_Class
                  || '|';
               UTL_FILE.PUT_LINE (lvc_file, lvc_string);

               -- WC Web Hierarchy Attribute
               lvc_string := NULL;
               lvc_string :=
                     '|'
                  || REC_ITEM_MAST.ITEM_NUMBER
                  || '|MASTER|ATTRIBUTE|WC Web Hierarchy|'
                  || REC_ITEM_MAST.WC_Web_Hierarchy
                  || '|';
               UTL_FILE.PUT_LINE (lvc_file, lvc_string);

               -- TIME SENSITIVE Attribute
               lvc_string := NULL;
               lvc_string :=
                     '|'
                  || REC_ITEM_MAST.ITEM_NUMBER
                  || '|MASTER|ATTRIBUTE|Time Sensitive|'
                  || REC_ITEM_MAST.Time_Sensitive
                  || '|';
               UTL_FILE.PUT_LINE (lvc_file, lvc_string);
            ELSIF p_extract_type = 'Data Extract'
            THEN
               -- UOM Attribute
               IF REC_ITEM_MAST.UOM IS NOT NULL
               THEN
                  lvc_string := NULL;
                  lvc_string :=
                        '|'
                     || REC_ITEM_MAST.ITEM_NUMBER
                     || '|MASTER|ATTRIBUTE|UOM|'
                     || REC_ITEM_MAST.UOM
                     || '|';
                  UTL_FILE.PUT_LINE (lvc_file, lvc_string);
               END IF;

               -- Long Description Attribute
               IF REC_ITEM_MAST.LONG_DESCRIPTION IS NOT NULL
               THEN
                  lvc_string := NULL;
                  lvc_string :=
                        '|'
                     || REC_ITEM_MAST.ITEM_NUMBER
                     || '|MASTER|ATTRIBUTE|Long Description|'
                     || REC_ITEM_MAST.LONG_DESCRIPTION
                     || '|';
                  UTL_FILE.PUT_LINE (lvc_file, lvc_string);
               END IF;

               --SHELF_LIFE_DAYS Attribute
               IF REC_ITEM_MAST.SHELF_LIFE_DAYS IS NOT NULL
               THEN
                  lvc_string := NULL;
                  lvc_string :=
                        '|'
                     || REC_ITEM_MAST.ITEM_NUMBER
                     || '|MASTER|ATTRIBUTE|Shelf Life Days|'
                     || REC_ITEM_MAST.SHELF_LIFE_DAYS
                     || '|';
                  UTL_FILE.PUT_LINE (lvc_file, lvc_string);
               END IF;

               -- Taxware code Attribute

               IF REC_ITEM_MAST.TAXWARE_CODE IS NOT NULL
               THEN
                  lvc_string := NULL;
                  lvc_string :=
                        '|'
                     || REC_ITEM_MAST.ITEM_NUMBER
                     || '|MASTER|ATTRIBUTE|Taxware Code|'
                     || REC_ITEM_MAST.TAXWARE_CODE
                     || '|';
                  UTL_FILE.PUT_LINE (lvc_file, lvc_string);
               END IF;

               -- SHIPPING_WEIGHT Attribute
               IF REC_ITEM_MAST.SHIPPING_WEIGHT IS NOT NULL
               THEN
                  lvc_string := NULL;
                  lvc_string :=
                        '|'
                     || REC_ITEM_MAST.ITEM_NUMBER
                     || '|MASTER|ATTRIBUTE|Shipping Weight|'
                     || REC_ITEM_MAST.SHIPPING_WEIGHT
                     || '|';
                  UTL_FILE.PUT_LINE (lvc_file, lvc_string);
               END IF;


               -- HAZMAT Attribute
               IF REC_ITEM_MAST.Hazmat IS NOT NULL
               THEN
                  lvc_string := NULL;
                  lvc_string :=
                        '|'
                     || REC_ITEM_MAST.ITEM_NUMBER
                     || '|MASTER|ATTRIBUTE|Hazardous Material|'
                     || REC_ITEM_MAST.Hazmat
                     || '|';
                  UTL_FILE.PUT_LINE (lvc_file, lvc_string);
               END IF;

               -- Fixed Lot Multiplier Attribute
               IF REC_ITEM_MAST.FIXED_LOT_MULTIPLIER IS NOT NULL
               THEN
                  lvc_string := NULL;
                  lvc_string :=
                        '|'
                     || REC_ITEM_MAST.ITEM_NUMBER
                     || '|MASTER|ATTRIBUTE|Fixed Lot Multiplier|'
                     || REC_ITEM_MAST.FIXED_LOT_MULTIPLIER
                     || '|';
                  UTL_FILE.PUT_LINE (lvc_file, lvc_string);
               END IF;

               -- Category Class
               IF REC_ITEM_MAST.Cat_Class IS NOT NULL
               THEN
                  lvc_string := NULL;
                  lvc_string :=
                        '|'
                     || REC_ITEM_MAST.ITEM_NUMBER
                     || '|MASTER|ATTRIBUTE|Category Class|'
                     || REC_ITEM_MAST.Cat_Class
                     || '|';
                  UTL_FILE.PUT_LINE (lvc_file, lvc_string);
               END IF;

               -- WC Web Hierarchy Attribute
               IF REC_ITEM_MAST.WC_Web_Hierarchy IS NOT NULL
               THEN
                  lvc_string := NULL;
                  lvc_string :=
                        '|'
                     || REC_ITEM_MAST.ITEM_NUMBER
                     || '|MASTER|ATTRIBUTE|WC Web Hierarchy|'
                     || REC_ITEM_MAST.WC_Web_Hierarchy
                     || '|';
                  UTL_FILE.PUT_LINE (lvc_file, lvc_string);
               END IF;


               -- TIME SENSITIVE Attribute
               IF REC_ITEM_MAST.Time_Sensitive IS NOT NULL
               THEN
                  lvc_string := NULL;
                  lvc_string :=
                        '|'
                     || REC_ITEM_MAST.ITEM_NUMBER
                     || '|MASTER|ATTRIBUTE|Time Sensitive|'
                     || REC_ITEM_MAST.Time_Sensitive
                     || '|';
                  UTL_FILE.PUT_LINE (lvc_file, lvc_string);
               END IF;
            ELSIF p_extract_type = 'Blank Extract'
            THEN
               -- UOM Attribute
               IF REC_ITEM_MAST.UOM IS NULL
               THEN
                  lvc_string := NULL;
                  lvc_string :=
                        '|'
                     || REC_ITEM_MAST.ITEM_NUMBER
                     || '|MASTER|ATTRIBUTE|UOM|'
                     || REC_ITEM_MAST.UOM
                     || '|';
                  UTL_FILE.PUT_LINE (lvc_file, lvc_string);
               END IF;

               -- Long Description Attribute
               IF REC_ITEM_MAST.LONG_DESCRIPTION IS NULL
               THEN
                  lvc_string := NULL;
                  lvc_string :=
                        '|'
                     || REC_ITEM_MAST.ITEM_NUMBER
                     || '|MASTER|ATTRIBUTE|Long Description|'
                     || REC_ITEM_MAST.LONG_DESCRIPTION
                     || '|';
                  UTL_FILE.PUT_LINE (lvc_file, lvc_string);
               END IF;

               --SHELF_LIFE_DAYS Attribute
               IF REC_ITEM_MAST.SHELF_LIFE_DAYS IS NULL
               THEN
                  lvc_string := NULL;
                  lvc_string :=
                        '|'
                     || REC_ITEM_MAST.ITEM_NUMBER
                     || '|MASTER|ATTRIBUTE|Shelf Life Days|'
                     || REC_ITEM_MAST.SHELF_LIFE_DAYS
                     || '|';
                  UTL_FILE.PUT_LINE (lvc_file, lvc_string);
               END IF;

               -- Taxware code Attribute

               IF REC_ITEM_MAST.TAXWARE_CODE IS NULL
               THEN
                  lvc_string := NULL;
                  lvc_string :=
                        '|'
                     || REC_ITEM_MAST.ITEM_NUMBER
                     || '|MASTER|ATTRIBUTE|Taxware Code|'
                     || REC_ITEM_MAST.TAXWARE_CODE
                     || '|';
                  UTL_FILE.PUT_LINE (lvc_file, lvc_string);
               END IF;

               --Shipping Weight Attribute.
               IF REC_ITEM_MAST.SHIPPING_WEIGHT IS NULL
               THEN
                  lvc_string := NULL;
                  lvc_string :=
                        '|'
                     || REC_ITEM_MAST.ITEM_NUMBER
                     || '|MASTER|ATTRIBUTE|Shipping Weight|'
                     || REC_ITEM_MAST.SHIPPING_WEIGHT
                     || '|';
                  UTL_FILE.PUT_LINE (lvc_file, lvc_string);
               END IF;


               -- HAZMAT Attribute
               IF REC_ITEM_MAST.Hazmat IS NULL
               THEN
                  lvc_string := NULL;
                  lvc_string :=
                        '|'
                     || REC_ITEM_MAST.ITEM_NUMBER
                     || '|MASTER|ATTRIBUTE|Hazardous Material|'
                     || REC_ITEM_MAST.Hazmat
                     || '|';
                  UTL_FILE.PUT_LINE (lvc_file, lvc_string);
               END IF;

               -- Fixed Lot Multiplier Attribute
               IF REC_ITEM_MAST.FIXED_LOT_MULTIPLIER IS NULL
               THEN
                  lvc_string := NULL;
                  lvc_string :=
                        '|'
                     || REC_ITEM_MAST.ITEM_NUMBER
                     || '|MASTER|ATTRIBUTE|Fixed Lot Multiplier|'
                     || REC_ITEM_MAST.FIXED_LOT_MULTIPLIER
                     || '|';
                  UTL_FILE.PUT_LINE (lvc_file, lvc_string);
               END IF;


               -- Category Class
               IF REC_ITEM_MAST.Cat_Class IS NULL
               THEN
                  lvc_string := NULL;
                  lvc_string :=
                        '|'
                     || REC_ITEM_MAST.ITEM_NUMBER
                     || '|MASTER|ATTRIBUTE|Category Class|'
                     || REC_ITEM_MAST.Cat_Class
                     || '|';
                  UTL_FILE.PUT_LINE (lvc_file, lvc_string);
               END IF;


               -- WC Web Hierarchy Attribute
               IF REC_ITEM_MAST.WC_Web_Hierarchy IS NULL
               THEN
                  lvc_string := NULL;
                  lvc_string :=
                        '|'
                     || REC_ITEM_MAST.ITEM_NUMBER
                     || '|MASTER|ATTRIBUTE|WC Web Hierarchy|'
                     || REC_ITEM_MAST.WC_Web_Hierarchy
                     || '|';
                  UTL_FILE.PUT_LINE (lvc_file, lvc_string);
               END IF;

               -- TIME SENSITIVE Attribute
               IF REC_ITEM_MAST.Time_Sensitive IS NULL
               THEN
                  lvc_string := NULL;
                  lvc_string :=
                        '|'
                     || REC_ITEM_MAST.ITEM_NUMBER
                     || '|MASTER|ATTRIBUTE|Time Sensitive|'
                     || REC_ITEM_MAST.Time_Sensitive
                     || '|';
                  UTL_FILE.PUT_LINE (lvc_file, lvc_string);
               END IF;
            END IF;

            g_sec := 'UPC Extract';

            -- UPC Attribute
            ln_upc_count := 0;

            SELECT COUNT (1)
              INTO ln_upc_count
              FROM APPS.MTL_CROSS_REFERENCES mcr
             WHERE     mcr.CROSS_REFERENCE_TYPE = 'UPC'
                   AND mcr.INVENTORY_ITEM_ID =
                          REC_ITEM_MAST.inventory_item_id;

            IF ln_upc_count > 0
            THEN
               IF p_extract_type IN ('Full Extract', 'Data Extract')
               THEN
                  ln_count := 1;

                  FOR REC_CREF_UPC
                     IN CUR_CREF_UPC (REC_ITEM_MAST.INVENTORY_ITEM_ID)
                  LOOP
                     -- Added below code in Rev 1.2 - Begin
                     IF     l_customer_name LIKE '%KIEWIT%'
                        AND p_export_type = 'Customer Extract'
                     THEN
                        ln_count := NULL;

                        IF ln_upc_count > 1
                        THEN
                           REC_CREF_UPC.ATTRIBUTE1 := 'WhiteCap Approved MFG';
                        END IF;
                     END IF;

                     -- Added Above code in Rev 1.2 - End

                     lvc_string := NULL;
                     lvc_string :=
                           '|'
                        || REC_ITEM_MAST.ITEM_NUMBER
                        || '|MASTER|ATTRIBUTE|UPC'
                        || ln_count
                        || '|'
                        || REC_CREF_UPC.CROSS_REFERENCE
                        || '|';
                     UTL_FILE.PUT_LINE (lvc_file, lvc_string);

                     lvc_string := NULL;
                     lvc_string :=
                           '|'
                        || REC_ITEM_MAST.ITEM_NUMBER
                        || '|MASTER|ATTRIBUTE|UPC'
                        || ln_count
                        || ' Owner|'
                        || REC_CREF_UPC.ATTRIBUTE1
                        || '|';
                     UTL_FILE.PUT_LINE (lvc_file, lvc_string);

                     -- Added below code in Rev 1.2 - Begin
                     IF     l_customer_name LIKE '%KIEWIT%'
                        AND p_export_type = 'Customer Extract'
                     THEN
                        EXIT;
                     END IF;

                     -- Added above code in Rev 1.2 - End

                     ln_count := ln_count + 1;
                  END LOOP;
               END IF;
            ELSE
               IF p_extract_type IN ('Full Extract', 'Blank Extract')
               THEN
                  lvc_string := NULL;
                  lvc_string :=
                        '|'
                     || REC_ITEM_MAST.ITEM_NUMBER
                     || '|MASTER|ATTRIBUTE|UPC||';
                  UTL_FILE.PUT_LINE (lvc_file, lvc_string);
                  lvc_string := NULL;
                  lvc_string :=
                        '|'
                     || REC_ITEM_MAST.ITEM_NUMBER
                     || '|MASTER|ATTRIBUTE|UPC Owner||';
                  UTL_FILE.PUT_LINE (lvc_file, lvc_string);
               END IF;
            END IF;

            g_sec := 'MFG Item Number';
            -- MFG Item Number Attribute
            ln_vendor_count := 0;

            SELECT COUNT (1)
              INTO ln_vendor_count
              FROM APPS.MTL_CROSS_REFERENCES mcr
             WHERE     mcr.CROSS_REFERENCE_TYPE = 'VENDOR'
                   AND mcr.INVENTORY_ITEM_ID =
                          REC_ITEM_MAST.inventory_item_id;

            IF ln_vendor_count > 0
            THEN
               IF p_extract_type IN ('Full Extract', 'Data Extract')
               THEN
                  ln_count := 1;

                  FOR REC_CREFF_MFG
                     IN CUR_CREFF_MFG (REC_ITEM_MAST.INVENTORY_ITEM_ID)
                  LOOP
                     -- Added below code in Rev 1.2 - Begin
                     IF     l_customer_name LIKE '%KIEWIT%'
                        AND p_export_type = 'Customer Extract'
                     THEN
                        ln_count := NULL;

                        IF ln_vendor_count > 1
                        THEN
                           REC_CREFF_MFG.vendor_name :=
                              'WhiteCap Approved MFG';
                        END IF;
                     END IF;

                     -- Added above code in Rev 1.2 - End

                     lvc_string := NULL;
                     lvc_string :=
                           '|'
                        || REC_ITEM_MAST.ITEM_NUMBER
                        || '|MASTER|ATTRIBUTE|Vendor Part Number'
                        || ln_count
                        || '|'
                        || REC_CREFF_MFG.CROSS_REFERENCE
                        || '|';
                     UTL_FILE.PUT_LINE (lvc_file, lvc_string);

                     lvc_string := NULL;
                     lvc_string :=
                           '|'
                        || REC_ITEM_MAST.ITEM_NUMBER
                        || '|MASTER|ATTRIBUTE|Vendor'
                        || ln_count
                        || ' Owner|'
                        || REC_CREFF_MFG.vendor_name
                        || '|';
                     UTL_FILE.PUT_LINE (lvc_file, lvc_string);

                     -- Added below code in Rev 1.2 - Begin

                     IF     l_customer_name LIKE '%KIEWIT%'
                        AND p_export_type = 'Customer Extract'
                     THEN
                        EXIT;
                     END IF;

                     -- Added above code in Rev 1.2 - End

                     ln_count := ln_count + 1;
                  END LOOP;
               END IF;
            ELSE
               IF p_extract_type IN ('Full Extract', 'Blank Extract')
               THEN
                  lvc_string := NULL;
                  lvc_string :=
                        '|'
                     || REC_ITEM_MAST.ITEM_NUMBER
                     || '|MASTER|ATTRIBUTE|Vendor Part Number||';
                  UTL_FILE.PUT_LINE (lvc_file, lvc_string);

                  lvc_string := NULL;
                  lvc_string :=
                        '|'
                     || REC_ITEM_MAST.ITEM_NUMBER
                     || '|MASTER|ATTRIBUTE|Vendor Owner||';
                  UTL_FILE.PUT_LINE (lvc_file, lvc_string);
               END IF;
            END IF;

            -- MSDS sheets priting.
            IF REC_ITEM_MAST.HAZMAT_FLAG = 'Y'
            THEN
               -- MSDS details
               IF p_extract_type IN ('Full Extract', 'Data Extract')
               THEN
                  lvc_string := NULL;
                  lvc_string :=
                        '|'
                     || REC_ITEM_MAST.ITEM_NUMBER
                     || '|MASTER|ATTACHMENT|MSDS|'
                     || lvc_msds_url
                     || REC_ITEM_MAST.MSDS
                     || '|';
                  UTL_FILE.PUT_LINE (lvc_file, lvc_string);
               ELSE
                  IF REC_ITEM_MAST.MSDS IS NULL
                  THEN
                     lvc_string := NULL;
                     lvc_string :=
                           '|'
                        || REC_ITEM_MAST.ITEM_NUMBER
                        || '|MASTER|ATTACHMENT|MSDS|'
                        || lvc_msds_url
                        || REC_ITEM_MAST.MSDS
                        || '|';
                     UTL_FILE.PUT_LINE (lvc_file, lvc_string);
                  END IF;
               END IF;
            END IF;

            -- Added below code in Rev 1.2 - Begin
            IF p_extract_type <> 'Blank Extract'
            THEN
               lvc_string := NULL;
               lvc_string :=
                     '|'
                  || REC_ITEM_MAST.ITEM_NUMBER
                  || '|MASTER|ATTRIBUTE|Availability|'
                  || REC_ITEM_CATLOG.CUST_CATALOG_NAME
                  || '|';
               UTL_FILE.PUT_LINE (lvc_file, lvc_string);
            END IF;

            -- Added agove code in Rev 1.2 - End

            FOR REC_EGO_DATA IN CUR_EGO_DATA (REC_ITEM_CATLOG.ITEM_NUMBER,
                                              lvc_images_url,
                                              p_extract_type,
                                              p_customer_id)
            LOOP
               lvc_string := NULL;

               -- Added below code in Rev 1.2 - Begin
               IF REC_EGO_DATA.ATTRIBUTE_NAME = 'XXWC_A_F_1_ATTR'
               THEN
                  l_image_flag := NULL;

                  BEGIN
                     SELECT ATTRIBUTE_CHAR_VALUE
                       INTO l_image_flag
                       FROM APPS.XXWC_EGO_EXTN_CUST_ATTR_V
                      WHERE     INVENTORY_ITEM_ID =
                                   REC_EGO_DATA.INVENTORY_ITEM_ID
                            AND ATTRIBUTE_NAME = 'XXWC_A_F_1_IMAGE_CHECK';

                     IF l_image_flag = 'N'
                     THEN
                        CONTINUE;
                     END IF;
                  EXCEPTION
                     WHEN OTHERS
                     THEN
                        CONTINUE;
                  END;
               END IF;

               -- Added above code in Rev 1.2 - End

               lvc_string :=
                     '|'
                  || REC_ITEM_MAST.ITEM_NUMBER
                  || '|'
                  || REC_EGO_DATA.Catalog_name
                  || '|'
                  || REC_EGO_DATA.ATTR_GROUP_DISP_NAME
                  || '|'
                  || REC_EGO_DATA.ATTR_DISPLAY_NAME
                  || '|'
                  || REC_EGO_DATA.ATTRIBUTE_VALUE
                  || '|';

               UTL_FILE.PUT_LINE (lvc_file, lvc_string);
            END LOOP;

            --Added below loop in rev 1.5
            FOR REC_EGO_PAGE_DATA
               IN CUR_EGO_PAGE_DATA (REC_ITEM_MAST.item_catalog_group_id,
                                     REC_ITEM_MAST.inventory_item_id,
                                     REC_ITEM_MAST.ITEM_NUMBER,
                                     p_extract_type)
            LOOP
               lvc_string := NULL;

               lvc_string :=
                     '|'
                  || REC_ITEM_MAST.ITEM_NUMBER
                  || '|'
                  || REC_EGO_PAGE_DATA.Catalog_name
                  || '|'
                  || REC_EGO_PAGE_DATA.ATTR_GROUP_DISP_NAME
                  || '|'
                  || REC_EGO_PAGE_DATA.ATTR_DISPLAY_NAME
                  || '|'
                  || REC_EGO_PAGE_DATA.ATTRIBUTE_VALUE
                  || '|';

               UTL_FILE.PUT_LINE (lvc_file, lvc_string);
            END LOOP;
            --Added above loop in rev 1.5
         END LOOP;
      END LOOP;

      COMMIT;

      UTL_FILE.FCLOSE (lvc_file);
      FND_FILE.PUT_LINE (FND_FILE.LOG, 'Extract Completed');
   EXCEPTION
      WHEN EXECUTION_ERROR
      THEN
         x_ret_code := 2;
         fnd_file.put_line (fnd_file.LOG, 'Error Occured ' || g_sec);

         IF UTL_FILE.is_open (LVC_FILE)
         THEN
            UTL_FILE.FCLOSE (LVC_FILE);
         END IF;
      WHEN OTHERS
      THEN
         IF UTL_FILE.is_open (LVC_FILE)
         THEN
            UTL_FILE.FCLOSE (LVC_FILE);
         END IF;

         l_err_msg :=
               l_err_msg
            || ' ...Error_Stack...'
            || DBMS_UTILITY.format_error_stack ()
            || ' Error_Backtrace...'
            || DBMS_UTILITY.format_error_backtrace ();

         -- Calling ERROR API
         xxcus_error_pkg.xxcus_error_main_api (
            p_called_from         => g_err_callfrom || '.' || g_procedure,
            p_ora_error_msg       => SUBSTR (l_err_msg, 1, 2000),
            p_error_desc          => g_sec,
            p_distribution_list   => g_distro_list,
            p_module              => 'INV');
         x_ret_code := 2;
   END;


   PROCEDURE CUST_CATALOG_STG_EXTRACT (x_err_buf               OUT VARCHAR2,
                                       x_ret_code              OUT VARCHAR2,
                                       p_export_type        IN     VARCHAR2,
                                       p_customer_id        IN     NUMBER,
                                       p_customer_catalog   IN     VARCHAR2,
                                       p_extract_type       IN     VARCHAR2)
   /******************************************************************************************************
   -- PROGRAM TYPE: Procedure
   -- Name: CUST_CATALOG_STG_EXTRACT
   -- PURPOSE: Developed to extract data from stanging table.
   -- HISTORY
   -- ==========================================================================================================
   -- ==========================================================================================================
   -- VERSION DATE          AUTHOR(S)       DESCRIPTION
   -- ------- -----------   --------------- --------------------------------------------------------------------
   -- 1.0     17-FEB-2016   P.Vamshidhar   TMS#20160219-00063 - External Customer Catalog Extract
   -- 1.1     06-JUN-2016   P.Vamshidhar   TMS#20160219-00063 - External Customer Catalog Extract
                                           Post Production issues fixed.
   --1.2      19-Aug-2016   P.Vamshidhar   TMS#20160817-00060 - Kiewit- Customer Catalog Extract Staging prg fix
   --1.4      30-Sep-2016   P.Vamshidhar   TMS#20160930-00032 - Kiewit Phase 2 - Features and Benefits issue fix.
   ************************************************************************************************************/
   IS
      CURSOR CUR_STG_DATA (
         p_extrat_type         VARCHAR2,
         p_customer_id         NUMBER,
         p_customer_catalog    VARCHAR2,
         p_image_url           VARCHAR2)
      IS
         SELECT    '|'
                || ITEM_NUMBER
                || '|'
                || ICC
                || '|'
                || ATTR_GROUP_DISP_NAME
                || '|'
                || ATTR_DISPLAY_NAME
                || '|'
                || DECODE (
                      SUBSTR (ATTR_DISPLAY_NAME, 1, 14),
                      'Fullsize Image', DECODE (
                                           INSTR (
                                              ATTRIBUTE_VALUE,
                                              'www.whitecap.com/wcsstore/WhiteCap/Images/Catalog/WhiteCap/FullImage',
                                              1),
                                           1, ATTRIBUTE_VALUE,
                                              p_image_url
                                           || '/'
                                           || ATTRIBUTE_VALUE),
                      ATTRIBUTE_VALUE)                       -- Changed in 1.2
                || '|'
                   STG_DATA
           FROM XXWC.XXWC_B2B_CAT_STG_TBL xbct
          WHERE     (   (p_extrat_type = 'Full Extract')
                     OR (    p_extrat_type = 'Data Extract'
                         AND xbct.ATTRIBUTE_VALUE IS NOT NULL)
                     OR (    p_extrat_type = 'Blank Extract'
                         AND xbct.ATTRIBUTE_VALUE IS NULL))
                AND (   p_customer_id IS NULL
                     OR (    p_customer_id IS NOT NULL
                         AND EXISTS
                                (SELECT 1
                                   FROM XXWC.XXWC_B2B_CAT_CUSTOMER_TBL XBCCT
                                  WHERE     p_customer_id = XBCCT.CUSTOMER_ID
                                        AND NVL (
                                               UPPER (p_customer_catalog),
                                               UPPER (
                                                  XBCCT.CUST_CATALOG_NAME)) =
                                               UPPER (
                                                  XBCCT.CUST_CATALOG_NAME))));


      -- Added below code in Rev 1.4 - Begin
      CURSOR CUR_CUST_DATA (
         p_extrat_type         VARCHAR2,
         p_customer_id         NUMBER,
         p_customer_catalog    VARCHAR2)
      IS
         SELECT DISTINCT ITEM_NUMBER
           FROM XXWC.XXWC_B2B_CAT_STG_TBL xbct
          WHERE     (   (p_extrat_type = 'Full Extract')
                     OR (    p_extrat_type = 'Data Extract'
                         AND xbct.ATTRIBUTE_VALUE IS NOT NULL)
                     OR (    p_extrat_type = 'Blank Extract'
                         AND xbct.ATTRIBUTE_VALUE IS NULL))
                AND (   p_customer_id IS NULL
                     OR (    p_customer_id IS NOT NULL
                         AND EXISTS
                                (SELECT 1
                                   FROM XXWC.XXWC_B2B_CAT_CUSTOMER_TBL XBCCT
                                  WHERE     p_customer_id = XBCCT.CUSTOMER_ID
                                        AND NVL (
                                               UPPER (p_customer_catalog),
                                               UPPER (
                                                  XBCCT.CUST_CATALOG_NAME)) =
                                               UPPER (
                                                  XBCCT.CUST_CATALOG_NAME))));

      CURSOR CUR_ITEM_DATA (
         p_extrat_type         VARCHAR2,
         p_customer_id         NUMBER,
         p_customer_catalog    VARCHAR2,
         p_item_number         VARCHAR2,
         p_image_url           VARCHAR2)
      IS
         SELECT ITEM_NUMBER,
                ICC,
                ATTR_GROUP_DISP_NAME,
                ATTR_DISPLAY_NAME,
                ATTRIBUTE_VALUE,
                   ITEM_NUMBER
                || '|'
                || ICC
                || '|'
                || ATTR_GROUP_DISP_NAME
                || '|'
                || ATTR_DISPLAY_NAME
                || '|'
                || DECODE (
                      SUBSTR (ATTR_DISPLAY_NAME, 1, 14),
                      'Fullsize Image', DECODE (
                                           INSTR (
                                              ATTRIBUTE_VALUE,
                                              'www.whitecap.com/wcsstore/WhiteCap/Images/Catalog/WhiteCap/FullImage',
                                              1),
                                           1, ATTRIBUTE_VALUE,
                                              p_image_url
                                           || '/'
                                           || ATTRIBUTE_VALUE),
                      ATTRIBUTE_VALUE)
                || '|'
                   STG_DATA
           FROM XXWC.XXWC_B2B_CAT_STG_TBL xbct
          WHERE     (   (p_extrat_type = 'Full Extract')
                     OR (    p_extrat_type = 'Data Extract'
                         AND xbct.ATTRIBUTE_VALUE IS NOT NULL)
                     OR (    p_extrat_type = 'Blank Extract'
                         AND xbct.ATTRIBUTE_VALUE IS NULL))
                AND xbct.item_number = p_item_number
                AND (   p_customer_id IS NULL
                     OR (    p_customer_id IS NOT NULL
                         AND EXISTS
                                (SELECT 1
                                   FROM XXWC.XXWC_B2B_CAT_CUSTOMER_TBL XBCCT
                                  WHERE     p_customer_id = XBCCT.CUSTOMER_ID
                                        AND NVL (
                                               UPPER (p_customer_catalog),
                                               UPPER (
                                                  XBCCT.CUST_CATALOG_NAME)) =
                                               UPPER (
                                                  XBCCT.CUST_CATALOG_NAME))));

      -- Added above code in Rev 1.4 - Begin



      lvc_file_name      VARCHAR2 (1000);
      l_customer_name    VARCHAR2 (1000);
      lvc_file           UTL_FILE.FILE_TYPE;
      lvc_outbound_dir   VARCHAR2 (100) := 'XXWC_INV_CUSTOMER_CATALOG';
      g_sec              VARCHAR2 (1000);
      max_line_length    BINARY_INTEGER := 32767;
      lvc_string         VARCHAR2 (3000) := NULL;
      ln_upc_count       NUMBER := 0;                      -- Added in Rev 1.4
      execution_error    EXCEPTION;
      g_procedure        VARCHAR2 (1000) := 'cust_catalog_stg_extract';
      l_err_msg          VARCHAR2 (1000);
      lvc_images_url     VARCHAR2 (1000)
         := FND_PROFILE.VALUE ('XXWC_INV_WC_WEBSITE_IMAGE_URL'); -- Added in 1.2
   BEGIN
      g_sec := 'Opening file at OS level';
      Fnd_file.put_line (fnd_file.LOG, g_sec);
      fnd_file.put_line (fnd_file.LOG, 'Parameters ');
      fnd_file.put_line (fnd_file.LOG, 'Export Type  : ' || p_export_type);
      fnd_file.put_line (fnd_file.LOG, 'Customer Id  : ' || p_customer_id);
      fnd_file.put_line (fnd_file.LOG,
                         'Cust Catalog : ' || p_customer_catalog);
      fnd_file.put_line (fnd_file.LOG, 'Extract Type : ' || p_extract_type);

      BEGIN
         SELECT REPLACE (party_name, ' ', '_')
           INTO l_customer_name
           FROM apps.hz_parties
          WHERE party_id = p_customer_id;
      EXCEPTION
         WHEN OTHERS
         THEN
            l_customer_name := NULL;
      END;

      lvc_file_name :=
            REPLACE (TRANSLATE (l_customer_name, '()&/*-+', '_'), ' ', '_')
         || TO_CHAR (SYSDATE, 'MMDDYYYYHH24MISS')
         || '.dat';

      fnd_file.put_line (fnd_file.LOG, 'File Name: ' || lvc_file_name);
      -- Opening File
      lvc_file :=
         UTL_FILE.FOPEN (lvc_outbound_dir,
                         lvc_file_name,
                         'W',
                         max_line_length);

      fnd_file.put_line (fnd_file.LOG, 'File Extract Started ');

      -- Writing Header
      lvc_string :=
         '|ITEM_NUMBER|ICC|ATTR_GROUP_DISP_NAME|ATTR_DISPLAY_NAME|ATTRIBUTE_VALUE|';
      UTL_FILE.PUT_LINE (lvc_file, lvc_string);

      -- Added below code in Rev 1.4
      IF p_export_type = 'Customer Extract'
      THEN
         FOR rec_cust_data
            IN cur_cust_data (p_extract_type,
                              p_customer_id,
                              p_customer_catalog)
         LOOP
            BEGIN
               ln_upc_count := 0;

               SELECT COUNT (1)
                 INTO ln_upc_count
                 FROM XXWC.XXWC_B2B_CAT_STG_TBL xbct
                WHERE     xbct.item_number = rec_cust_data.item_number
                      AND xbct.attr_display_name LIKE 'UPC%'
                      AND xbct.attr_display_name NOT LIKE 'UPC%Owner%';
            EXCEPTION
               WHEN NO_DATA_FOUND
               THEN
                  ln_upc_count := 0;
            END;

            FOR rec_item_data IN cur_item_data (p_extract_type,
                                                p_customer_id,
                                                p_customer_catalog,
                                                rec_cust_data.item_number,
                                                lvc_images_url)
            LOOP
               IF     rec_item_data.attr_display_name LIKE 'UPC%'
                  AND ln_upc_count > 1
               THEN
                  IF rec_item_data.attr_display_name LIKE 'UPC%Owner'
                  THEN
                     CONTINUE;
                  END IF;

                  lvc_string :=
                        rec_item_data.ITEM_NUMBER
                     || '|'
                     || rec_item_data.ICC
                     || '|'
                     || rec_item_data.ATTR_GROUP_DISP_NAME
                     || '|UPC|'
                     || rec_item_data.ATTRIBUTE_VALUE
                     || '|';

                  UTL_FILE.PUT_LINE (lvc_file, lvc_string);


                  lvc_string :=
                        rec_item_data.ITEM_NUMBER
                     || '|'
                     || rec_item_data.ICC
                     || '|'
                     || rec_item_data.ATTR_GROUP_DISP_NAME
                     || '|UPC Owner|WhiteCap Approved MFG|';

                  UTL_FILE.PUT_LINE (lvc_file, lvc_string);

                  CONTINUE;
               END IF;

               lvc_string := rec_item_data.stg_data;
               UTL_FILE.PUT_LINE (lvc_file, lvc_string);
            END LOOP;
         END LOOP;
      ELSE
         FOR rec_stg_data IN cur_stg_data (p_extract_type,
                                           p_customer_id,
                                           p_customer_catalog,
                                           lvc_images_url)
         LOOP
            lvc_string := rec_stg_data.stg_data;
            UTL_FILE.PUT_LINE (lvc_file, lvc_string);
         END LOOP;
      END IF;

      -- Added above code in Rev 1.4

      UTL_FILE.FCLOSE (lvc_file);

      FND_FILE.PUT_LINE (FND_FILE.LOG, 'Extract Completed');
   EXCEPTION
      WHEN EXECUTION_ERROR
      THEN
         x_ret_code := 2;
         x_err_buf := 'Execution Error Occured';
         fnd_file.put_line (fnd_file.LOG, 'Error Occured ' || g_sec);

         IF UTL_FILE.is_open (LVC_FILE)
         THEN
            UTL_FILE.FCLOSE (LVC_FILE);
         END IF;
      WHEN OTHERS
      THEN
         IF UTL_FILE.is_open (LVC_FILE)
         THEN
            UTL_FILE.FCLOSE (LVC_FILE);
         END IF;

         l_err_msg :=
               l_err_msg
            || ' ...Error_Stack...'
            || DBMS_UTILITY.format_error_stack ()
            || ' Error_Backtrace...'
            || DBMS_UTILITY.format_error_backtrace ();

         fnd_file.put_line (fnd_file.LOG, l_err_msg);

         -- Calling ERROR API
         xxcus_error_pkg.xxcus_error_main_api (
            p_called_from         => g_err_callfrom || '.' || g_procedure,
            p_ora_error_msg       => SUBSTR (l_err_msg, 1, 2000),
            p_error_desc          => g_sec,
            p_distribution_list   => g_distro_list,
            p_module              => 'INV');
         x_ret_code := 2;
         x_err_buf := l_err_msg;
   END;


   PROCEDURE PDH_ATTRIBUTES_UPDATE (xerrbuf             OUT VARCHAR2,
                                    xretcode            OUT VARCHAR2,
                                    P_BATCH_NUMBER   IN     VARCHAR2)
   IS
      /******************************************************************************************************
      -- PROGRAM TYPE: Procedure
      -- Name: PDH_ATTRIBUTES_UPDATE
      -- PURPOSE: Developed to update PDH attribues
      -- HISTORY
      -- ==========================================================================================================
      -- ==========================================================================================================
      -- VERSION DATE          AUTHOR(S)       DESCRIPTION
      -- ------- -----------   --------------- --------------------------------------------------------------------
      -- 1.0     17-FEB-2016   P.Vamshidhar   TMS#20160219-00063 - External Customer Catalog Extract
      --1.2      19-Aug-2016   P.Vamshidhar   TMS#20160603-00067 - Kiewit Phase 2 - Upload Data to PDH
      --                                      (Procedure re-wrote completely and consider as new version).
      ************************************************************************************************************/

      -- Variable Declaration
      lvc_data_update      VARCHAR2 (1) := NULL;
      execution_error      EXCEPTION;
      l_err_mess           VARCHAR2 (4000);
      l_xref_err_count     NUMBER;
      ln_count             NUMBER := 0;

      -- Cursors Declare
      CURSOR CUR_BATCHES (
         CP_BATCH_NUMBER    NUMBER)
      IS
         SELECT DISTINCT BATCH_NUMBER
           FROM XXWC.XXWC_B2B_CAT_STG_TBL XCST
          WHERE     NVL (CP_BATCH_NUMBER, XCST.BATCH_NUMBER) =
                       XCST.BATCH_NUMBER
                AND REQUEST_ID = g_request_id;

      CURSOR CUR_STG_ITEM (
         CP_BATCH_NUMBER    NUMBER)
      IS
           SELECT XCST.ITEM_NUMBER, MSIB.INVENTORY_ITEM_ID
             FROM XXWC.XXWC_B2B_CAT_STG_TBL XCST, APPS.MTL_SYSTEM_ITEMS_B MSIB
            WHERE     XCST.ITEM_NUMBER = MSIB.SEGMENT1
                  AND XCST.BATCH_NUMBER = CP_BATCH_NUMBER
                  AND MSIB.ORGANIZATION_ID = 222
                  AND XCST.REQUEST_ID = G_REQUEST_ID
                  AND XCST.ITEM_NUMBER NOT IN (SELECT ITEM_NUMBER
                                                 FROM XXWC.XXWC_B2B_CAT_STG_TBL
                                                WHERE PROCESS_STATUS IN ('Success',
                                                                         'Error'))
         GROUP BY XCST.ITEM_NUMBER, MSIB.INVENTORY_ITEM_ID;

      lvc_mst_data_valid   VARCHAR2 (1);
      lvc_pdh_data_valid   VARCHAR2 (1);
      lvc_mst_data_upd     VARCHAR2 (1);
      lvc_pdh_data_upd     VARCHAR2 (1);
      lvc_xref_data_upd    VARCHAR2 (1);
      ln_success_items     NUMBER := 0;
      ln_error_items       NUMBER := 0;
      ln_total_items       NUMBER := 0;
      x_mas_upd_mes        VARCHAR2 (32000);
      x_pdh_upd_mes        VARCHAR2 (32000);
      x_fut_upd_mes        VARCHAR2 (32000);
      x_upc_upd_mes        VARCHAR2 (32000);
      x_ven_upd_mes        VARCHAR2 (32000);
   BEGIN
      g_procedure := 'PDH_ATTRIBUTES_UPDATE';
      -- Cleaning Log table
      write_log ('Cleaning Log Table before Process  - Start');

      cleanup_log;

      write_log ('Cleaning Log Table before Process  - Done');

      -- Output Titles Printing--
      FND_FILE.PUT_LINE (
         FND_FILE.OUTPUT,
         '---------------------------------------------------------------------------------------------------- ');
      FND_FILE.PUT_LINE (
         FND_FILE.OUTPUT,
         '      BATCH NUMBER                      Total Items        Success Items        Error Items ');
      FND_FILE.PUT_LINE (
         FND_FILE.OUTPUT,
         '---------------------------------------------------------------------------------------------------- ');

      -- Updating Data Before Process
      write_log ('Updating Data before Process  - Start');

      lvc_data_update := UPD_BEFORE_PROCESS;

      write_log ('Updating Data before Process  - Done ' || lvc_data_update);

      IF lvc_data_update = 'Y'
      THEN
         FOR REC_BATCHES IN CUR_BATCHES (P_BATCH_NUMBER)
         LOOP
            FND_FILE.PUT_LINE (FND_FILE.LOG,
                               'Batch Number :' || REC_BATCHES.BATCH_NUMBER);

            BEGIN
               ln_total_items := 0;

               SELECT COUNT (DISTINCT ITEM_NUMBER)
                 INTO ln_total_items
                 FROM XXWC.XXWC_B2B_CAT_STG_TBL XCST
                WHERE     XCST.BATCH_NUMBER = REC_BATCHES.BATCH_NUMBER
                      AND request_id = g_request_id;
            EXCEPTION
               WHEN NO_DATA_FOUND
               THEN
                  ln_total_items := 0;
            END;

               -- Added below code in Rev 1.5
               write_log ('Validating XREF UPC Data ');

               MTL_UPC_VALIDATION;

               write_log ('Validating XREF Data  - End');

            BEGIN
               SELECT COUNT (DISTINCT item_number)
                 INTO ln_count
                 FROM XXWC.XXWC_B2B_CAT_STG_TBL XCST,
                      APPS.MTL_SYSTEM_ITEMS_B MSIB
                WHERE     XCST.ITEM_NUMBER = MSIB.SEGMENT1
                      AND XCST.BATCH_NUMBER = REC_BATCHES.BATCH_NUMBER
                      AND XCST.REQUEST_ID = G_REQUEST_ID
                      AND MSIB.ORGANIZATION_ID = 222
                      AND XCST.ITEM_NUMBER NOT IN (SELECT ITEM_NUMBER
                                                     FROM XXWC.XXWC_B2B_CAT_STG_TBL
                                                    WHERE PROCESS_STATUS IN ('Success'));

               FND_FILE.PUT_LINE (FND_FILE.LOG,
                                  ' Number of Items' || ln_count);
            EXCEPTION
               WHEN OTHERS
               THEN
                  FND_FILE.PUT_LINE (FND_FILE.LOG,
                                     'Number of Items(Exp) ' || ln_count);
            END;


            FOR REC_STG_ITEM IN CUR_STG_ITEM (REC_BATCHES.BATCH_NUMBER)
            LOOP
               -- Variable Initialization

               lvc_mst_data_valid := NULL;
               lvc_pdh_data_valid := NULL;
               l_xref_err_count := 0;
               lvc_mst_data_upd := NULL;
               lvc_xref_data_upd := NULL;
               lvc_pdh_data_upd := NULL;
               x_mas_upd_mes := NULL;
               x_pdh_upd_mes := NULL;
               x_upc_upd_mes := NULL;
               x_ven_upd_mes := NULL;
               x_fut_upd_mes := NULL;


               FND_FILE.PUT_LINE (
                  FND_FILE.LOG,
                     '/******************************'
                  || REC_STG_ITEM.ITEM_NUMBER
                  || '  Process Begin ***********************/ ');
               FND_FILE.PUT_LINE (
                  FND_FILE.LOG,
                  '/*** Validation Begin ******************************************************************************/ ');

               FND_FILE.PUT_LINE (
                  FND_FILE.LOG,
                  'Item Number :' || REC_STG_ITEM.ITEM_NUMBER);

               -- Master Data Validation
               write_log ('Validating Master Data  - Start');

               lvc_mst_data_valid := NULL;

               lvc_mst_data_valid :=
                  MASTER_DATA_VALIDATION (REC_STG_ITEM.ITEM_NUMBER);

               write_log ('Validating Master Data  - End');

               -- PDH Data Validation
               write_log ('Validating PDH Data  - Start');

               lvc_pdh_data_valid := NULL;

               lvc_pdh_data_valid :=
                  PDH_DATA_VALIDATION (REC_STG_ITEM.ITEM_NUMBER);

               write_log ('Validating PDH Data   - End');

               -- XREF data validation
               write_log ('Validating XREF Data  - Start');

               BEGIN
                  UPDATE XXWC.XXWC_B2B_CAT_STG_TBL XCST
                     SET Process_status = 'Error',
                         Error_message = 'Invalid Vendor'
                   WHERE     ATTR_display_Name LIKE '%Owner%'
                         AND ICC = 'MASTER'
                         AND ITEM_NUMBER = REC_STG_ITEM.ITEM_NUMBER
                         AND ATTR_GROUP_DISP_NAME = 'ATTRIBUTE'
                         AND (   ATTR_DISPLAY_NAME LIKE 'UPC%'
                              OR UPPER (ATTR_DISPLAY_NAME) LIKE 'VENDOR%')
                         AND REGEXP_LIKE (ATTRIBUTE_VALUE, '^[0-9]+$');

                  SELECT COUNT (1)
                    INTO l_xref_err_count
                    FROM XXWC.XXWC_B2B_CAT_STG_TBL
                   WHERE     item_number = REC_STG_ITEM.ITEM_NUMBER
                         AND (   UPPER (ATTR_DISPLAY_NAME) LIKE 'VENDOR%'
                              OR UPPER (ATTR_DISPLAY_NAME) LIKE 'UPC%')
                         AND PROCESS_STATUS = 'Error';
               EXCEPTION
                  WHEN OTHERS
                  THEN
                     l_xref_err_count := 0;
               END;

               write_log (
                     'Master Validation:  '
                  || lvc_mst_data_valid
                  || ' XRef Error count:  '
                  || NVL (l_xref_err_count, 0)
                  || 'PDH Data Validation:  '
                  || lvc_pdh_data_valid);
               FND_FILE.PUT_LINE (
                  FND_FILE.LOG,
                  '/*** Validation End ***********************************************************************************/ ');

               IF     NVL (lvc_mst_data_valid, 'N') = 'Y'
                  AND NVL (lvc_pdh_data_valid, 'N') = 'Y'
                  AND NVL (l_xref_err_count, 0) = 0
               THEN
                  FND_FILE.PUT_LINE (
                     FND_FILE.LOG,
                        '/******************************'
                     || REC_STG_ITEM.ITEM_NUMBER
                     || ' - Item Update Begin********************************/');
                  FND_FILE.PUT_LINE (
                     FND_FILE.LOG,
                     '/*** Update Process Begin ***************************************************************************************/');

                  lvc_mst_data_upd :=
                     MASTER_DATA_UPDATE (REC_STG_ITEM.ITEM_NUMBER,
                                         X_MAS_UPD_MES);

                  write_log (
                     'Updating Master Data - Done ' || lvc_mst_data_upd);

                  write_log ('Updating Cross Reference Data  - Start');

                  lvc_xref_data_upd :=
                     XREF_DATA_UPDATE (REC_STG_ITEM.ITEM_NUMBER,
                                       X_UPC_UPD_MES,
                                       X_VEN_UPD_MES);

                  write_log (
                        'Updating Cross Reference Data  - Done '
                     || lvc_xref_data_upd);

                  -- PDH Data Update Process
                  write_log ('Updating PDH Data  - Start');

                  lvc_pdh_data_upd :=
                     PDH_DATA_UPDATE (REC_STG_ITEM.ITEM_NUMBER,
                                      X_PDH_UPD_MES,
                                      X_FUT_UPD_MES);

                  write_log (
                     'Updating PDH Data  - Done ' || lvc_pdh_data_upd);

                  FND_fILE.PUT_LINE (
                     FND_fILE.LOG,
                        'lvc_mst_data_upd: '
                     || lvc_mst_data_upd
                     || ' lvc_xref_data_upd:'
                     || lvc_xref_data_upd
                     || ' lvc_pdh_data_upd:'
                     || lvc_pdh_data_upd);

                  IF     lvc_mst_data_upd = 'Y'
                     AND lvc_xref_data_upd = 'Y'
                     AND lvc_pdh_data_upd = 'Y'
                  THEN
                     --UPC Update
                     UPDATE XXWC.XXWC_B2B_CAT_STG_TBL
                        SET PROCESS_STATUS = 'Success', Error_message = NULL
                      WHERE     ITEM_NUMBER = REC_STG_ITEM.ITEM_NUMBER
                            AND request_id = g_request_id;

                     write_log (
                        'No Records Updated (Success) :' || SQL%ROWCOUNT);
                  ELSE
                     ROLLBACK;

                     IF lvc_mst_data_upd = 'N'
                     THEN
                        UPDATE XXWC.XXWC_B2B_CAT_STG_TBL
                           SET ERROR_MESSAGE = X_MAS_UPD_MES,
                               PROCESS_STATUS = 'Error'
                         WHERE     item_number = REC_STG_ITEM.ITEM_NUMBER
                               AND request_id = g_request_id
                               AND ICC = 'MASTER'
                               AND ATTR_GROUP_DISP_NAME IN ('ATTRIBUTE',
                                                            'ATTACHMENT')
                               AND (    ATTR_DISPLAY_NAME NOT LIKE 'UPC%'
                                    AND UPPER (ATTR_DISPLAY_NAME) NOT LIKE
                                           'VENDOR%');

                        write_log (
                           'No Records Updated (Master) <>S' || SQL%ROWCOUNT);
                     END IF;

                     IF lvc_xref_data_upd = 'N'
                     THEN
                        IF X_UPC_UPD_MES IS NOT NULL
                        THEN
                           UPDATE XXWC.XXWC_B2B_CAT_STG_TBL
                              SET PROCESS_STATUS = 'Error',
                                  Error_message = X_UPC_UPD_MES
                            WHERE     ITEM_NUMBER = REC_STG_ITEM.ITEM_NUMBER
                                  AND ICC = 'MASTER'
                                  AND PROCESS_STATUS <> 'Error'
                                  AND ATTR_GROUP_DISP_NAME = 'ATTRIBUTE'
                                  AND ATTR_DISPLAY_NAME LIKE 'UPC%';

                           write_log (
                              'No Records Updated (UPC) <>S' || SQL%ROWCOUNT);
                        END IF;

                        IF X_VEN_UPD_MES IS NOT NULL
                        THEN
                           UPDATE XXWC.XXWC_B2B_CAT_STG_TBL
                              SET PROCESS_STATUS = 'Error',
                                  Error_message = X_VEN_UPD_MES,
                                  PROCESSED_DATE = SYSDATE,
                                  LAST_UPDATED_BY = g_user_id,
                                  LAST_UPDATED_DATE = SYSDATE
                            WHERE     ITEM_NUMBER = REC_STG_ITEM.ITEM_NUMBER
                                  AND ICC = 'MASTER'
                                  AND PROCESS_STATUS <> 'Error'
                                  AND ATTR_GROUP_DISP_NAME = 'ATTRIBUTE'
                                  AND ATTR_DISPLAY_NAME LIKE 'VENDOR%';

                           write_log (
                                 'No Records Updated (VENDOR) <>S'
                              || SQL%ROWCOUNT);
                        END IF;
                     END IF;

                     IF lvc_pdh_data_upd = 'N'
                     THEN
                        IF X_PDH_UPD_MES IS NOT NULL
                        THEN
                           UPDATE XXWC.XXWC_B2B_CAT_STG_TBL
                              SET PROCESS_STATUS = 'Error',
                                  PROCESSED_DATE = SYSDATE,
                                  ERROR_MESSAGE = X_PDH_UPD_MES,
                                  LAST_UPDATED_BY = g_user_id,
                                  LAST_UPDATED_DATE = SYSDATE
                            WHERE     ITEM_NUMBER = REC_STG_ITEM.ITEM_NUMBER
                                  AND ICC <> 'MASTER'
                                  AND ATTR_GROUP_DISP_NAME NOT LIKE
                                         'Features%Benefits%';

                           write_log (
                                 'No Records Updated (REGULAR) <>S'
                              || SQL%ROWCOUNT);
                        END IF;

                        IF X_FUT_UPD_MES IS NOT NULL
                        THEN
                           UPDATE XXWC.XXWC_B2B_CAT_STG_TBL
                              SET PROCESS_STATUS = 'Error',
                                  PROCESSED_DATE = SYSDATE,
                                  ERROR_MESSAGE = X_FUT_UPD_MES,
                                  LAST_UPDATED_BY = g_user_id,
                                  LAST_UPDATED_DATE = SYSDATE
                            WHERE     ITEM_NUMBER = REC_STG_ITEM.ITEM_NUMBER
                                  AND ICC <> 'MASTER'
                                  AND ATTR_GROUP_DISP_NAME LIKE
                                         'Features%Benefits%';

                           write_log (
                                 'No Records Updated (FUTURE) <>S'
                              || SQL%ROWCOUNT);
                        END IF;
                     END IF;
                  END IF;

                  COMMIT;

                  FND_FILE.PUT_LINE (
                     FND_FILE.LOG,
                     '/*** Update Process End **********************************************************************************/');
               END IF;

               FND_FILE.PUT_LINE (
                  FND_FILE.LOG,
                     '/******************************'
                  || REC_STG_ITEM.ITEM_NUMBER
                  || ' - Item Process End *************************/');
            END LOOP;

            BEGIN
               ln_error_items := 0;

               SELECT COUNT (DISTINCT ITEM_NUMBER)
                 INTO ln_error_items
                 FROM XXWC.XXWC_B2B_CAT_STG_TBL XCST
                WHERE     BATCH_NUMBER = REC_BATCHES.BATCH_NUMBER
                      AND PROCESS_STATUS = 'Error'
                      AND request_id = g_request_id;
            EXCEPTION
               WHEN OTHERS
               THEN
                  ln_error_items := 0;
            END;

            BEGIN
               ln_success_items := 0;

               SELECT COUNT (DISTINCT ITEM_NUMBER)
                 INTO ln_success_items
                 FROM XXWC.XXWC_B2B_CAT_STG_TBL XCST
                WHERE     BATCH_NUMBER = REC_BATCHES.BATCH_NUMBER
                      AND PROCESS_STATUS = 'Success'
                      AND request_id = g_request_id;
            EXCEPTION
               WHEN OTHERS
               THEN
                  ln_success_items := 0;
            END;

            FND_FILE.PUT_LINE (
               FND_FILE.OUTPUT,
                  '     '
               || RPAD (REC_BATCHES.BATCH_NUMBER, 22, ' ')
               || '    '
               || LPAD (ln_total_items, 15, ' ')
               || '   '
               || LPAD (ln_success_items, 15, ' ')
               || '      '
               || LPAD (ln_error_items, 15, ' '));


            DELETE FROM XXWC.XXWC_B2B_CAT_STG_TBL XCST
                  WHERE     XCST.PROCESS_STATUS = 'Success'
                        AND XCST.BATCH_NUMBER = REC_BATCHES.BATCH_NUMBER
                        AND request_id = g_request_id
                        AND NOT EXISTS
                               (SELECT 1
                                  FROM XXWC.XXWC_B2B_CAT_STG_TBL XCST1
                                 WHERE     XCST.ITEM_NUMBER =
                                              XCST1.ITEM_NUMBER
                                       AND XCST.BATCH_NUMBER =
                                              XCST1.BATCH_NUMBER
                                       AND XCST1.PROCESS_STATUS = 'Error'
                                       AND request_id = g_request_id);

            -- Added below code in Rev 1.4 - Begin
            UPDATE XXWC.XXWC_B2B_CAT_STG_TBL
               SET ERROR_MESSAGE = NULL
             WHERE ERROR_MESSAGE IN ('UPC Picked', 'VENDOR Picked');

            -- Added above code in Rev 1.4 - End.
            COMMIT;
         END LOOP;
      END IF;

      BEGIN
         SELECT COUNT (1)
           INTO ln_error_items
           FROM XXWC.XXWC_B2B_CAT_STG_TBL
          WHERE request_id = g_request_id AND PROCESS_STATUS = 'Error';

         IF NVL (ln_error_items, 0) > 0
         THEN
            xretcode := 1;
         ELSE
            xretcode := 0;
         END IF;
      EXCEPTION
         WHEN OTHERS
         THEN
            xretcode := 1;
      END;
   EXCEPTION
      WHEN EXECUTION_ERROR
      THEN
         l_err_mess :=
               l_err_mess
            || ' ...Error_Stack...'
            || DBMS_UTILITY.format_error_stack ()
            || ' Error_Backtrace...'
            || DBMS_UTILITY.format_error_backtrace ();

         -- Calling ERROR API
         xxcus_error_pkg.xxcus_error_main_api (
            p_called_from         => g_err_callfrom || '.' || g_procedure,
            p_ora_error_msg       => SUBSTR (l_err_mess, 1, 2000),
            p_error_desc          => SUBSTR (l_err_mess, 1, 240),
            p_distribution_list   => g_distro_list,
            p_module              => 'INV');
      WHEN OTHERS
      THEN
         l_err_mess :=
               l_err_mess
            || ' ...Error_Stack...'
            || DBMS_UTILITY.format_error_stack ()
            || ' Error_Backtrace...'
            || DBMS_UTILITY.format_error_backtrace ();

         -- Calling ERROR API
         xxcus_error_pkg.xxcus_error_main_api (
            p_called_from         => g_err_callfrom || '.' || g_procedure,
            p_ora_error_msg       => SUBSTR (l_err_mess, 1, 2000),
            p_error_desc          => SUBSTR (l_err_mess, 1, 240),
            p_distribution_list   => g_distro_list,
            p_module              => 'INV');
         xretcode := 2;
   END;


   PROCEDURE write_log (p_log_msg VARCHAR2)
   /**********************************************************************************************************
     $Header write_log $
     PURPOSE:     Procedure to write Log messages

     REVISIONS:
     Ver    Date         Author                Description
     ------ -----------  ------------------    -------------------------------------------------------------
      1.2      19-Aug-2016   P.Vamshidhar         TMS#20160603-00067 - Kiewit Phase 2 - Upload Data to PDH
     **********************************************************************************************************/
   IS
      l_start     NUMBER;
      l_log_msg   VARCHAR2 (4000);
   BEGIN
      IF p_log_msg = 'g_line_open' OR p_log_msg = 'g_line_close'
      THEN
         l_log_msg :=
            '/**************************************************************************/';
      ELSE
         l_log_msg :=
            p_log_msg || ' : ' || TO_CHAR (SYSDATE, 'DD-MON-YYYY HH:MI:SS');
         xxwc_common_tunning_helpers.elapsed_time (
            'CUSTOMER_CATALOG_PKG',
            SUBSTR (p_log_msg, 1, 500),
            l_start);
      END IF;

      fnd_file.put_line (fnd_file.LOG, l_log_msg);
   END write_log;


   PROCEDURE cleanup_log
   /*********************************************************************************************************
     $Header cleanup_log $
     PURPOSE:     Procedure to cleanup the Log table

     REVISIONS:
     Ver    Date         Author                Description
     ------ -----------  ------------------    -------------------------------------------------------------
      1.2      19-Aug-2016   P.Vamshidhar         TMS#20160603-00067 - Kiewit Phase 2 - Upload Data to PDH

     ********************************************************************************************************/
   IS
      l_cleanup_before   NUMBER := 10;
   BEGIN
      DELETE FROM xxwc.xxwc_interfaces##log
            WHERE     interface_name = 'XXWC_CUSTOMER_CATALOG_PKG'
                  AND TRUNC (insert_date) <
                         TRUNC (SYSDATE) - l_cleanup_before;

      write_log ('xxwc.xxwc_interfaces##log cleanup completed');
   EXCEPTION
      WHEN OTHERS
      THEN
         write_log (
               'xxwc.xxwc_interfaces##log cleanup failed '
            || SUBSTR (SQLERRM, 1, 1900));
   END cleanup_log;

   FUNCTION Validate_value_set (p_value_set_id   IN NUMBER,
                                p_attr_value     IN VARCHAR)
      RETURN VARCHAR2
   /*********************************************************************************************************
     $Header Validate_value_set $
     PURPOSE:     Procedure to validate value with Value set.

     REVISIONS:
     Ver    Date         Author                Description
     ------ -----------  ------------------    -------------------------------------------------------------
      1.2      19-Aug-2016   P.Vamshidhar         TMS#20160603-00067 - Kiewit Phase 2 - Upload Data to PDH

     ********************************************************************************************************/
   IS
      lvc_value_set_type      VARCHAR2 (10);
      lvc_return_value        VARCHAR2 (10);
      lvc_query               VARCHAR2 (32000);
      lvc_where_clause        VARCHAR2 (32000);
      lx_success              VARCHAR2 (100);
      lx_x_mapping_code       VARCHAR2 (2000);
      ln_where_clause         NUMBER;
      lvc_id_column_name      VARCHAR2 (1000);
      lvc_value_column_name   VARCHAR2 (1000);
      l_success_mess          VARCHAR2 (1000);
      l_error_mess            VARCHAR2 (1000);
   BEGIN
      write_log (
         'Value Set ID ' || p_value_set_id || ' Value ' || p_attr_value);


      SELECT VALIDATION_TYPE
        INTO lvc_value_set_type
        FROM APPS.FND_FLEX_VALUE_SETS
       WHERE FLEX_VALUE_SET_ID = p_value_set_id;

      IF lvc_value_set_type = 'N'
      THEN
         lvc_return_value := 'Y';
         RETURN lvc_return_value;
      ELSIF lvc_value_set_type = 'I'
      THEN
         BEGIN
            SELECT 'Y'
              INTO lvc_return_value
              FROM APPS.FND_FLEX_VALUE_SETS FFVS, APPS.FND_FLEX_VALUES FFV
             WHERE     FFVS.FLEX_VALUE_SET_ID = FFV.FLEX_VALUE_SET_ID
                   AND FFVS.FLEX_VALUE_SET_ID = p_value_set_id
                   AND FFV.FLEX_VALUE = p_attr_value
                   AND FFV.ENABLED_FLAG = 'Y';

            RETURN lvc_return_value;
         EXCEPTION
            WHEN NO_DATA_FOUND
            THEN
               lvc_return_value := 'N';
               RETURN lvc_return_value;
         END;
      ELSIF lvc_value_set_type = 'F'
      THEN
         IF p_attr_value IS NULL
         THEN
            RETURN NULL;
         END IF;

         lvc_query := NULL;
         FND_FLEX_VAL_API.get_table_vset_select (
            p_value_set_id   => p_value_set_id,
            x_select         => lvc_query,
            x_mapping_code   => lx_x_mapping_code,
            x_success        => lx_success);

         ln_where_clause := INSTR (lvc_query, 'WHERE', 1);

         BEGIN
            SELECT ID_COLUMN_NAME, VALUE_COLUMN_NAME
              INTO lvc_id_column_name, lvc_value_column_name
              FROM FND_FLEX_VALIDATION_TABLES
             WHERE FLEX_VALUE_SET_ID = p_value_set_id;

            IF lvc_id_column_name IS NOT NULL
            THEN
               lvc_where_clause :=
                  lvc_id_column_name || ' = ' || '''' || p_attr_value || '''';
            ELSE
               lvc_where_clause :=
                     lvc_value_column_name
                  || ' = '
                  || ''''
                  || p_attr_value
                  || '''';
            END IF;
         EXCEPTION
            WHEN NO_DATA_FOUND
            THEN
               lvc_id_column_name := NULL;
         END;

         IF ln_where_cLause > 0
         THEN
            lvc_query := lvc_query || ' AND ' || lvc_where_clause;
         ELSE
            lvc_query := lvc_query || ' WHERE ' || lvc_where_clause;
         END IF;

         lvc_query :=
               'SELECT '
            || '''Y'''
            || SUBSTR (lvc_query, INSTR (lvc_query, 'FROM', 1));


         EXECUTE IMMEDIATE lvc_query INTO lvc_return_value;

         RETURN lvc_return_value;
      ELSE
         RETURN 'Y';
      END IF;

      RETURN lvc_return_value;
   EXCEPTION
      WHEN OTHERS
      THEN
         lvc_return_value := 'N';
         RETURN lvc_return_value;
   END;


   FUNCTION UPD_BEFORE_PROCESS
      RETURN VARCHAR2
   /*********************************************************************************************************
     $Header UPD_BEFORE_PROCESS $
     PURPOSE:     Procedure to update data in staging table before process start

     REVISIONS:
     Ver    Date         Author                Description
     ------ -----------  ------------------    -------------------------------------------------------------
      1.2      19-Aug-2016   P.Vamshidhar      TMS#20160603-00067 - Kiewit Phase 2 - Upload Data to PDH
      1.4      30-Sep-2016   P.Vamshidhar      TMS#20160930-00032 - Kiewit Phase 2 - Features and Benefits issue fix.
     ********************************************************************************************************/
   IS
      l_success_mess    VARCHAR2 (1000);
      l_error_mess      VARCHAR2 (1000);
      l_err_msg         VARCHAR2 (4000);
      EXECUTION_ERROR   EXCEPTION;

      CURSOR C1
      IS
         SELECT DISTINCT ITEM_NUMBER
           FROM XXWC.XXWC_B2B_CAT_STG_TBL XCST;
   BEGIN
      g_sec := 'Re-Setting Process Status Flag';

      write_log (g_sec);

      -- Resetting Process status flag for new and error records.
      BEGIN
         l_success_mess := 'Resetting Process flag Completed Successfully';
         l_error_mess := 'Resetting Process flag Completed with Error';

         write_log ('Resetting Process Flag');

         UPDATE XXWC.XXWC_B2B_CAT_STG_TBL XCST
            SET XCST.PROCESS_STATUS = 'New', XCST.Error_message = NULL
          WHERE NVL (XCST.PROCESS_STATUS, 'Error') = 'Error';

         COMMIT;

         write_log (l_success_mess);
      EXCEPTION
         WHEN NO_DATA_FOUND
         THEN
            write_log (l_success_mess);
         WHEN OTHERS
         THEN
            l_err_msg :=
                  ' Error occured, While Resetting Process Status '
               || SUBSTR (SQLERRM, 1, 200);
            write_log (l_err_msg);
            write_log (l_error_mess);
            RAISE EXECUTION_ERROR;
      END;

      -- Updating Request id for all tobe processed records

      BEGIN
         l_success_mess := 'Resetting Request_id Completed Successfully';
         l_error_mess := 'Resetting Request_id Completed with Error';

         write_log ('Resetting Request_id');

         UPDATE XXWC.XXWC_B2B_CAT_STG_TBL XCST
            SET REQUEST_ID = g_request_id
          WHERE XCST.PROCESS_STATUS = 'New';

         FND_FILE.PUT_LINE (FND_FILE.LOG,
                            ' No Rows Updated (Request Id) ' || SQL%ROWCOUNT);
         COMMIT;

         write_log (l_success_mess);
      EXCEPTION
         WHEN NO_DATA_FOUND
         THEN
            write_log (l_success_mess);
         WHEN OTHERS
         THEN
            l_err_msg :=
                  ' Error occured, While Resetting Request_id '
               || SUBSTR (SQLERRM, 1, 200);
            write_log (l_err_msg);
            write_log (l_error_mess);
            RAISE EXECUTION_ERROR;
      END;



      -- Validating Items from Stating table to base tables.
      g_sec := 'Checking for Inactive Items';

      BEGIN
         l_success_mess := 'Valdating Items Completed Successfully';
         l_error_mess := 'Valdating Items Completed with Error';

         write_log ('Valdating Items');

         UPDATE XXWC.XXWC_B2B_CAT_STG_TBL XCST
            SET XCST.PROCESS_STATUS = 'Error',
                XCST.Error_message =
                   'The items status is Inactive. No updates allowed'
          WHERE     EXISTS
                       (SELECT 1
                          FROM APPS.MTL_SYSTEM_ITEMS_B MSIB
                         WHERE     MSIB.ORGANIZATION_ID = 222
                               AND MSIB.SEGMENT1 = XCST.ITEM_NUMBER
                               AND MSIB.INVENTORY_ITEM_STATUS_CODE IN ('Inactive',
                                                                       'Discontinu',
                                                                       'New Item'))
                AND request_id = g_request_id;

         COMMIT;
         write_log (l_success_mess);
      EXCEPTION
         WHEN NO_DATA_FOUND
         THEN
            write_log (l_success_mess);
         WHEN OTHERS
         THEN
            l_err_msg :=
                  ' Error occured, While updating items '
               || SUBSTR (SQLERRM, 1, 200);
            write_log (l_err_msg);
            write_log (l_error_mess);
            RAISE EXECUTION_ERROR;
      END;


      -- Validating Items .
      g_sec := 'Checking Item Exist in MTL_SYSTEM_ITEMS_B';

      BEGIN
         l_success_mess := 'Checking Items Exist Completed';
         l_error_mess := 'Checking Items Exist with Error';

         write_log ('Valdating Items Exist');

         UPDATE XXWC.XXWC_B2B_CAT_STG_TBL XCST
            SET XCST.PROCESS_STATUS = 'Error',
                XCST.Error_message = 'Item does not exist in Oracle.'
          WHERE     NOT EXISTS
                       (SELECT 1
                          FROM APPS.MTL_SYSTEM_ITEMS_B MSIB
                         WHERE     MSIB.ORGANIZATION_ID = 222
                               AND MSIB.SEGMENT1 = XCST.ITEM_NUMBER)
                AND request_id = g_request_id;

         COMMIT;
         write_log (l_success_mess);
      EXCEPTION
         WHEN NO_DATA_FOUND
         THEN
            write_log (l_success_mess);
         WHEN OTHERS
         THEN
            l_err_msg :=
                  ' Error occured, While updating items '
               || SUBSTR (SQLERRM, 1, 200);
            write_log (l_err_msg);
            write_log (l_error_mess);
            RAISE EXECUTION_ERROR;
      END;


      -- Validating ICC
      g_sec := 'Validating ICC';

      BEGIN
         l_success_mess := 'Valdating ICC Completed Successfully';
         l_error_mess := 'Valdating ICC Completed with Error';


         UPDATE XXWC.XXWC_B2B_CAT_STG_TBL XCST
            SET XCST.PROCESS_STATUS = 'Error',
                XCST.Error_message =
                   XCST.Error_message || ' | ' || 'Invalid ICC'
          WHERE     NOT EXISTS
                       (SELECT 1
                          FROM APPS.MTL_ITEM_CATALOG_GROUPS_KFV KFV
                         WHERE     KFV.ENABLED_FLAG = 'Y'
                               AND UPPER (KFV.CONCATENATED_SEGMENTS) =
                                      UPPER (XCST.ICC))
                AND XCST.REQUEST_ID = G_REQUEST_ID
                AND XCST.ICC <> 'MASTER';

         FND_FILE.PUT_LINE (FND_FILE.LOG,
                            ' No Rows Updated (ICC) ' || SQL%ROWCOUNT);

         write_log (l_success_mess);
      EXCEPTION
         WHEN NO_DATA_FOUND
         THEN
            write_log (l_success_mess);
         WHEN OTHERS
         THEN
            l_err_msg :=
                  ' Error occured, While validating ICC'
               || SUBSTR (SQLERRM, 1, 200);
            write_log (l_err_msg);
            write_log (l_error_mess);
            RAISE EXECUTION_ERROR;
      END;

      -- Validating ATTRIBUTE Groups
      g_sec := 'Validating Attribute Groups';

      BEGIN
         l_success_mess := 'Valdating Attribute Groups Completed Successfully';
         l_error_mess := 'Valdating Attribute Groups Completed with Error';

         UPDATE XXWC.XXWC_B2B_CAT_STG_TBL XCST
            SET XCST.PROCESS_STATUS = 'Error',
                XCST.Error_message =
                   XCST.Error_message || ' | ' || 'Invalid Attribute Group'
          WHERE     NOT EXISTS
                       (SELECT 1
                          FROM APPS.EGO_ATTR_GROUPS_V EAGV
                         WHERE UPPER (EAGV.ATTR_GROUP_DISP_NAME) =
                                  UPPER (XCST.ATTR_GROUP_DISP_NAME))
                AND XCST.REQUEST_ID = G_REQUEST_ID
                AND XCST.ICC <> 'MASTER';

         FND_FILE.PUT_LINE (FND_FILE.LOG,
                            ' No Rows Updated (Groups) ' || SQL%ROWCOUNT);

         write_log (l_success_mess);
      EXCEPTION
         WHEN NO_DATA_FOUND
         THEN
            write_log (l_success_mess);
         WHEN OTHERS
         THEN
            l_err_msg :=
                  'Error occured, While validating Attribute Groups'
               || SUBSTR (SQLERRM, 1, 200);
            write_log (l_err_msg);
            write_log (l_error_mess);

            RAISE EXECUTION_ERROR;
      END;

      -- Validating ATTRIBUTEs
      g_sec := 'Validating Attributes';

      BEGIN
         l_success_mess := 'Valdating Attributes Completed Successfully';
         l_error_mess := 'Valdating Attributes Completed with Error';

         UPDATE XXWC.XXWC_B2B_CAT_STG_TBL XCST
            SET XCST.PROCESS_STATUS = 'Error',
                XCST.Error_message =
                   XCST.Error_message || ' | ' || 'Invalid Attribute'
          WHERE     NOT EXISTS
                       (SELECT 1
                          FROM APPS.EGO_ATTRS_V EAV
                         WHERE     EAV.ENABLED_FLAG = 'Y'
                               AND UPPER (EAV.ATTR_DISPLAY_NAME) =
                                      UPPER (XCST.ATTR_DISPLAY_NAME))
                AND XCST.REQUEST_ID = G_REQUEST_ID
                AND XCST.ICC <> 'MASTER';

         FND_FILE.PUT_LINE (FND_FILE.LOG,
                            ' No Rows Updated (Attributes) ' || SQL%ROWCOUNT);

         write_log (l_success_mess);
      EXCEPTION
         WHEN NO_DATA_FOUND
         THEN
            write_log (l_success_mess);
         WHEN OTHERS
         THEN
            l_err_msg :=
                  'Error occured, While Validating Attributes'
               || SUBSTR (SQLERRM, 1, 200);
            write_log (l_err_msg);
            write_log (l_error_mess);
            RAISE EXECUTION_ERROR;
      END;


      -- Added below code in Rev 1.4  - Begin
      g_sec := 'Validating Attributes Under Attribute Group';

      BEGIN
         l_success_mess :=
            'Valdating Attribute Names/Groups Completed Successfully';
         l_error_mess :=
            'Valdating Attribute Names/Groups Completed with Error';

         FOR C2 IN C1
         LOOP
            BEGIN
               UPDATE XXWC.XXWC_B2B_CAT_STG_TBL XCST
                  SET XCST.PROCESS_STATUS = 'Error',
                      Error_message =
                            Error_message
                         || ' | '
                         || XCST.ATTR_DISPLAY_NAME
                         || ' Attribute is not defined under Attribute group '
                         || XCST.ATTR_GROUP_DISP_NAME
                WHERE     XCST.ICC <> 'MASTER'
                      AND XCST.ITEM_NUMBER = C2.ITEM_NUMBER
                      AND NOT EXISTS
                             (SELECT 1
                                FROM APPS.EGO_ATTR_GROUPS_V EAGV,
                                     APPS.EGO_ATTRS_V EAV
                               WHERE     EAGV.ATTR_GROUP_NAME =
                                            EAV.ATTR_GROUP_NAME
                                     AND XCST.ATTR_GROUP_DISP_NAME =
                                            EAGV.ATTR_GROUP_DISP_NAME
                                     AND XCST.ATTR_DISPLAY_NAME =
                                            EAV.ATTR_DISPLAY_NAME);
            EXCEPTION
               WHEN NO_DATA_FOUND
               THEN
                  NULL;
            END;
         END LOOP;

         write_log (l_success_mess);
      EXCEPTION
         WHEN NO_DATA_FOUND
         THEN
            write_log (l_success_mess);
         WHEN OTHERS
         THEN
            l_err_msg :=
                  'Error occured, While Validating Attributes'
               || SUBSTR (SQLERRM, 1, 200);
            write_log (l_err_msg);
            write_log (l_error_mess);
            RAISE EXECUTION_ERROR;
      END;

      -- Added above code in Rev 1.4  - End

      -- Removing non-printing charecters.

      g_sec := 'Updating Non-Print Charecters';

      BEGIN
         l_success_mess := 'Updating Non-Print char Completed Successfully';
         l_error_mess := 'Updating Non-Print char Completed with Error';

         UPDATE XXWC.XXWC_B2B_CAT_STG_TBL XCST
            SET ATTRIBUTE_VALUE =
                   REGEXP_REPLACE (ATTRIBUTE_VALUE, '([^[:print:]])', ''),
                ATTR_DISPLAY_NAME =
                   REGEXP_REPLACE (ATTR_DISPLAY_NAME, '([^[:print:]])', '')
          WHERE REQUEST_ID = G_REQUEST_ID;

         write_log ('Rows Updated ' || SQL%ROWCOUNT);
         write_log (l_success_mess);
      EXCEPTION
         WHEN NO_DATA_FOUND
         THEN
            write_log (l_success_mess);
         WHEN OTHERS
         THEN
            l_err_msg :=
                  'Error occured, While removing non-printable charectors.'
               || SUBSTR (SQLERRM, 1, 200);
            write_log (l_err_msg);
            write_log (l_error_mess);
            RAISE EXECUTION_ERROR;
      END;

      g_sec := 'Updating Control Charecters';

      BEGIN
         l_success_mess := 'Updating Control char Completed Successfully';
         l_error_mess := 'Updating Control char Completed with Error';

         UPDATE XXWC.XXWC_B2B_CAT_STG_TBL
            SET ATTRIBUTE_VALUE = REPLACE (ATTRIBUTE_VALUE, CHR (10), ''),
                ATTR_DISPLAY_NAME = REPLACE (ATTR_DISPLAY_NAME, CHR (10), '')
          WHERE REQUEST_ID = G_REQUEST_ID;


         write_log ('Rows Updated ' || SQL%ROWCOUNT);

         write_log (l_success_mess);
      EXCEPTION
         WHEN NO_DATA_FOUND
         THEN
            write_log (l_success_mess);
         WHEN OTHERS
         THEN
            l_err_msg :=
                  'Error occured, While removing control charectors.'
               || SUBSTR (SQLERRM, 1, 200);
            write_log (l_err_msg);
            write_log (l_error_mess);

            RAISE EXECUTION_ERROR;
      END;

      -- Updating Invalid Cross Reference values

      g_sec := 'Updating Invalid Cross Reference values';


      COMMIT;
      RETURN 'Y';
   EXCEPTION
      WHEN EXECUTION_ERROR
      THEN
         l_err_msg :=
               l_err_msg
            || ' ...Error_Stack...'
            || DBMS_UTILITY.format_error_stack ()
            || ' Error_Backtrace...'
            || DBMS_UTILITY.format_error_backtrace ();

         -- Calling ERROR API
         xxcus_error_pkg.xxcus_error_main_api (
            p_called_from         => g_err_callfrom || '.' || g_procedure,
            p_ora_error_msg       => SUBSTR (l_err_msg, 1, 2000),
            p_error_desc          => SUBSTR (l_err_msg, 1, 240),
            p_distribution_list   => g_distro_list,
            p_module              => 'INV');

         RETURN 'N';
      WHEN OTHERS
      THEN
         l_err_msg :=
               l_err_msg
            || ' ...Error_Stack...'
            || DBMS_UTILITY.format_error_stack ()
            || ' Error_Backtrace...'
            || DBMS_UTILITY.format_error_backtrace ();

         -- Calling ERROR API
         xxcus_error_pkg.xxcus_error_main_api (
            p_called_from         => g_err_callfrom || '.' || g_procedure,
            p_ora_error_msg       => SUBSTR (l_err_msg, 1, 2000),
            p_error_desc          => SUBSTR (l_err_msg, 1, 240),
            p_distribution_list   => g_distro_list,
            p_module              => 'INV');

         RETURN 'N';
   END;


   FUNCTION MASTER_DATA_UPDATE (P_ITEM_NUMBER   IN     VARCHAR2,
                                X_ERR_MESS         OUT VARCHAR2)
      RETURN VARCHAR2
   /*********************************************************************************************************
     $Header MASTER_DATA_UPDATE $
     PURPOSE:     Procedure to update master data

     REVISIONS:
     Ver    Date         Author                Description
     ------ -----------  ------------------    -------------------------------------------------------------
      1.2      19-Aug-2016   P.Vamshidhar         TMS#20160603-00067 - Kiewit Phase 2 - Upload Data to PDH

     ********************************************************************************************************/
   IS
      -- Cursors Declaration
      CURSOR CUR_ITEM (
         P_ITEM_NUMBER1    VARCHAR2)
      IS
         SELECT XBST.ROWID ROW_ID,
                TRIM (XBST.ATTR_DISPLAY_NAME) ATTR_DISPLAY_NAME,
                XBST.ATTRIBUTE_VALUE ATTRIBUTE_VALUE,
                FFV.FLEX_VALUE
           FROM XXWC.XXWC_B2B_CAT_STG_TBL XBST,
                APPS.FND_FLEX_VALUE_SETS FFVS,
                APPS.FND_FLEX_VALUES_VL FFV
          WHERE     XBST.ITEM_NUMBER = P_ITEM_NUMBER1
                AND XBST.ICC = 'MASTER'
                AND XBST.ATTR_GROUP_DISP_NAME = 'ATTRIBUTE'
                AND (   XBST.ATTR_DISPLAY_NAME NOT LIKE 'UPC%'
                     OR UPPER (XBST.ATTR_DISPLAY_NAME) NOT LIKE 'VENDOR%')
                AND FFVS.FLEX_VALUE_SET_NAME = 'XXWC_MASTER_LEVEL_ATTR_VS'
                AND FFVS.FLEX_VALUE_SET_ID = FFV.FLEX_VALUE_SET_ID
                AND FFV.ENABLED_FLAG = 'Y'
                AND UPPER (TRIM (FFV.description)) =
                       UPPER (TRIM (XBST.ATTR_DISPLAY_NAME));

      -- Variables Declaration
      l_success_mess                VARCHAR2 (2000);
      l_error_mess                  VARCHAR2 (2000);
      l_err_msg                     VARCHAR2 (4000);
      ln_error_count                NUMBER := 0;
      EXECUTION_ERROR               EXCEPTION;


      -- API Related
      l_item_table                  EGO_Item_PUB.Item_Tbl_Type;
      x_item_table                  EGO_Item_PUB.Item_Tbl_Type;
      x_return_status               VARCHAR2 (1);
      x_msg_count                   NUMBER (10);
      x_msg_data                    VARCHAR2 (1000);
      x_message_list                Error_Handler.Error_Tbl_Type;
      l_error_message_list          Error_handler.error_tbl_type;

      -- General
      ln_inventory_item_id          MTL_SYSTEM_ITEMS_B.INVENTORY_ITEM_ID%TYPE;
      lvc_dimension_uom_code        MTL_SYSTEM_ITEMS_B.DIMENSION_UOM_CODE%TYPE;
      ln_unit_length                MTL_SYSTEM_ITEMS_B.UNIT_LENGTH%TYPE;
      ln_unit_width                 MTL_SYSTEM_ITEMS_B.UNIT_WIDTH%TYPE;
      ln_unit_height                MTL_SYSTEM_ITEMS_B.UNIT_HEIGHT%TYPE;
      lvc_description               MTL_SYSTEM_ITEMS_B.DESCRIPTION%TYPE;
      ln_shelf_life_days            MTL_SYSTEM_ITEMS_B.SHELF_LIFE_DAYS%TYPE;
      ln_fixed_lot_multiplier       MTL_SYSTEM_ITEMS_B.FIXED_LOT_MULTIPLIER%TYPE;
      lvc_attribute22               MTL_SYSTEM_ITEMS_B.ATTRIBUTE22%TYPE;
      lvc_hazardous_material_flag   MTL_SYSTEM_ITEMS_B.HAZARDOUS_MATERIAL_FLAG%TYPE;
      ln_unit_weight                MTL_SYSTEM_ITEMS_B.UNIT_WEIGHT%TYPE;
      lvc_uomcode                   VARCHAR2 (40);
      ln_valid_number               NUMBER;
      ln_vs_count                   NUMBER;
      lvc_return_flag               VARCHAR2 (1);
   BEGIN
      g_procedure := 'MASTER_DATA_UPDATE';
      g_sec := 'Retriving Item Data from Master Tables';

      BEGIN
         l_success_mess := 'Retribing Item Data Completed Successfully';
         l_error_mess := ' Retribing Item Data Completed with Error';

         SELECT INVENTORY_ITEM_ID,
                DIMENSION_UOM_CODE,
                UNIT_LENGTH,
                UNIT_WIDTH,
                UNIT_HEIGHT,
                DESCRIPTION,
                SHELF_LIFE_DAYS,
                FIXED_LOT_MULTIPLIER,
                ATTRIBUTE22,
                HAZARDOUS_MATERIAL_FLAG,
                UNIT_WEIGHT
           INTO ln_inventory_item_id,
                lvc_dimension_uom_code,
                ln_unit_length,
                ln_unit_width,
                ln_unit_height,
                lvc_description,
                ln_shelf_life_days,
                ln_fixed_lot_multiplier,
                lvc_attribute22,
                lvc_hazardous_material_flag,
                ln_unit_weight
           FROM APPS.MTL_SYSTEM_ITEMS_B
          WHERE SEGMENT1 = P_ITEM_NUMBER AND ORGANIZATION_ID = 222;

         write_log (l_success_mess);
      EXCEPTION
         WHEN NO_DATA_FOUND
         THEN
            l_err_msg :=
                  ' ...Error_Stack...'
               || DBMS_UTILITY.format_error_stack ()
               || ' Error_Backtrace...'
               || DBMS_UTILITY.format_error_backtrace ();
            write_log (l_err_msg);
            RAISE EXECUTION_ERROR;
         WHEN OTHERS
         THEN
            l_err_msg :=
                  ' ...Error_Stack...'
               || DBMS_UTILITY.format_error_stack ()
               || ' Error_Backtrace...'
               || DBMS_UTILITY.format_error_backtrace ();
            write_log (l_err_msg);
            RAISE EXECUTION_ERROR;
      END;

      g_sec := 'Initializing API PLSQL table';

      l_success_mess := 'Initializing PLSQL variables Completed Successfully';
      l_error_mess := ' Initializing PLSQL variables Completed with Error';

      FOR REC_ITEM IN CUR_ITEM (P_ITEM_NUMBER)
      LOOP
         FND_FILE.PUT_LINE (
            FND_FILE.LOG,
               ' '
            || REC_ITEM.ATTR_DISPLAY_NAME
            || ' '
            || REC_ITEM.ATTRIBUTE_VALUE);

         -- Hazardous Material flag validation
         IF REC_ITEM.ATTR_DISPLAY_NAME = 'Hazardous Material'
         THEN
            IF NVL (UPPER (SUBSTR (REC_ITEM.ATTRIBUTE_VALUE, 1, 1)), 'A') NOT IN ('Y',
                                                                                  'N')
            THEN
               UPDATE XXWC.XXWC_B2B_CAT_STG_TBL XBST
                  SET error_message = 'Value should be Y or N',
                      process_status = 'Error'
                WHERE ROWID = REC_ITEM.row_id;

               ln_error_count := ln_error_count + 1;

               CONTINUE;
            END IF;

            IF SUBSTR (REC_ITEM.ATTRIBUTE_VALUE, 1, 1) <>
                  lvc_hazardous_material_flag
            THEN
               l_item_table (1).HAZARDOUS_MATERIAL_FLAG :=
                  SUBSTR (REC_ITEM.ATTRIBUTE_VALUE, 1, 1);
            ELSE
               CONTINUE;
            END IF;
         END IF;

         IF REC_ITEM.ATTR_DISPLAY_NAME IN ('Unit Length',
                                           'Unit Width',
                                           'Unit Height',
                                           'Shipping Weight',
                                           'Fixed Lot Multiplier',
                                           'Shelf Life Days')
         THEN
            IF REC_ITEM.ATTRIBUTE_VALUE IS NOT NULL
            THEN
               BEGIN
                  SELECT 1
                    INTO ln_valid_number
                    FROM DUAL
                   WHERE (   REGEXP_LIKE (
                                NVL (REC_ITEM.ATTRIBUTE_VALUE, 0.00),
                                '^[0-9]+(\.([0-9]{1,20})?)?$')
                          OR REGEXP_LIKE (
                                NVL (REC_ITEM.ATTRIBUTE_VALUE, 0.00),
                                '^+(.([0-9]{1,20})?)?$'));
               EXCEPTION
                  WHEN NO_DATA_FOUND
                  THEN
                     ln_valid_number := 0;
                  WHEN OTHERS
                  THEN
                     ln_valid_number := 0;
               END;

               IF ln_valid_number = 0
               THEN
                  UPDATE XXWC.XXWC_B2B_CAT_STG_TBL
                     SET ERROR_MESSAGE =
                               '"'
                            || REC_ITEM.ATTRIBUTE_VALUE
                            || '"'
                            || ' is an Invalid Value. Please enter numeric values only.',
                         PROCESS_STATUS = 'Error'
                   WHERE ROWID = REC_ITEM.row_id;

                  ln_error_count := ln_error_count + 1;
                  CONTINUE;
               END IF;
            END IF;

            IF     REC_ITEM.ATTR_DISPLAY_NAME = 'Unit Length'
               AND NVL (REC_ITEM.ATTRIBUTE_VALUE, 0) <>
                      NVL (ln_unit_length, 0)
            THEN
               l_item_table (1).UNIT_LENGTH := REC_ITEM.ATTRIBUTE_VALUE;
            ELSIF     REC_ITEM.ATTR_DISPLAY_NAME = 'Unit Width'
                  AND NVL (REC_ITEM.ATTRIBUTE_VALUE, 0) <>
                         NVL (ln_unit_width, 0)
            THEN
               l_item_table (1).UNIT_WIDTH := REC_ITEM.ATTRIBUTE_VALUE;
            ELSIF     REC_ITEM.ATTR_DISPLAY_NAME = 'Unit Height'
                  AND NVL (REC_ITEM.ATTRIBUTE_VALUE, 0) <>
                         NVL (ln_unit_height, 0)
            THEN
               l_item_table (1).UNIT_HEIGHT := REC_ITEM.ATTRIBUTE_VALUE;
            ELSIF     REC_ITEM.ATTR_DISPLAY_NAME = 'Shipping Weight'
                  AND NVL (REC_ITEM.ATTRIBUTE_VALUE, 0) <>
                         NVL (ln_unit_weight, 0)
            THEN
               l_item_table (1).UNIT_WEIGHT := REC_ITEM.ATTRIBUTE_VALUE;
            ELSIF     REC_ITEM.ATTR_DISPLAY_NAME = 'Fixed Lot Multiplier'
                  AND NVL (REC_ITEM.ATTRIBUTE_VALUE, 0) <>
                         NVL (ln_fixed_lot_multiplier, 0)
            THEN
               l_item_table (1).FIXED_LOT_MULTIPLIER :=
                  REC_ITEM.ATTRIBUTE_VALUE;
            ELSIF     REC_ITEM.ATTR_DISPLAY_NAME = 'Shelf Life Days'
                  AND NVL (REC_ITEM.ATTRIBUTE_VALUE, 0) <>
                         NVL (ln_shelf_life_days, 0)
            THEN
               l_item_table (1).SHELF_LIFE_DAYS := REC_ITEM.ATTRIBUTE_VALUE;
            END IF;
         END IF;

         IF     REC_ITEM.ATTR_DISPLAY_NAME = 'Taxware Code'
            AND REC_ITEM.ATTRIBUTE_VALUE <> lvc_attribute22
         THEN
            BEGIN
               ln_vs_count := 0;

               SELECT COUNT (1)
                 INTO ln_vs_count
                 FROM apps.fnd_flex_value_sets ffvs,
                      apps.fnd_flex_values_vl ffv
                WHERE     ffvs.flex_value_set_id = ffv.flex_value_set_id
                      AND ffvs.flex_value_set_name = 'XXWC TAXWARE CODE'
                      AND FFV.ENABLED_FLAG = 'Y'
                      AND FFV.FLEX_VALUE = REC_ITEM.ATTRIBUTE_VALUE;
            EXCEPTION
               WHEN OTHERS
               THEN
                  ln_vs_count := 0;
            END;


            IF ln_vs_count = 1
            THEN
               l_item_table (1).ATTRIBUTE22 := REC_ITEM.ATTRIBUTE_VALUE;
            ELSE
               UPDATE XXWC.XXWC_B2B_CAT_STG_TBL XBST
                  SET error_message =
                            REC_ITEM.ATTRIBUTE_VALUE
                         || ' Value not listed Taxware codes list',
                      process_status = 'Error'
                WHERE ROWID = REC_ITEM.row_id;

               ln_error_count := ln_error_count + 1;
            END IF;
         ELSIF REC_ITEM.ATTR_DISPLAY_NAME = 'Dimension UOM'
         THEN
            BEGIN
               lvc_uomcode := NULL;

               SELECT UOM_CODE
                 INTO lvc_uomcode
                 FROM MTL_UNITS_OF_MEASURE
                WHERE UNIT_OF_MEASURE = REC_ITEM.ATTRIBUTE_VALUE;
            EXCEPTION
               WHEN OTHERS
               THEN
                  lvc_uomcode := NULL;
            END;

            IF lvc_uomcode IS NOT NULL
            THEN
               l_item_table (1).DIMENSION_UOM_CODE := lvc_uomcode;
            ELSE
               UPDATE XXWC.XXWC_B2B_CAT_STG_TBL XBST
                  SET error_message =
                            REC_ITEM.ATTRIBUTE_VALUE
                         || ' Value not listed Unit of Measurements list',
                      process_status = 'Error'
                WHERE ROWID = REC_ITEM.row_id;

               ln_error_count := ln_error_count + 1;
            END IF;
         ELSIF     REC_ITEM.ATTR_DISPLAY_NAME = 'Description'
               AND lvc_description <> REC_ITEM.ATTRIBUTE_VALUE
         THEN
            REC_ITEM.ATTRIBUTE_VALUE :=
               REPLACE (
                  REPLACE (
                     REPLACE (
                        REPLACE (
                           REPLACE (
                              REPLACE (
                                 REPLACE (
                                    REPLACE (
                                       REPLACE (
                                          REPLACE (
                                             REPLACE (
                                                REPLACE (
                                                   REPLACE (
                                                      REPLACE (
                                                         REPLACE (
                                                            REPLACE (
                                                               REPLACE (
                                                                  REPLACE (
                                                                     REPLACE (
                                                                        REPLACE (
                                                                           REPLACE (
                                                                              REPLACE (
                                                                                 REPLACE (
                                                                                    REPLACE (
                                                                                       REPLACE (
                                                                                          REPLACE (
                                                                                             REPLACE (
                                                                                                REPLACE (
                                                                                                   REPLACE (
                                                                                                      REPLACE (
                                                                                                         REPLACE (
                                                                                                            REGEXP_REPLACE (
                                                                                                               REC_ITEM.ATTRIBUTE_VALUE,
                                                                                                               '[[:cntrl:]]'),
                                                                                                            '\\|',
                                                                                                            ''),
                                                                                                         '\\n',
                                                                                                         ''),
                                                                                                      '\\r',
                                                                                                      ''),
                                                                                                   CHR (
                                                                                                      14845090),
                                                                                                   ''),
                                                                                                CHR (
                                                                                                   49838),
                                                                                                ''),
                                                                                             CHR (
                                                                                                14844051),
                                                                                             ''),
                                                                                          '\\',
                                                                                          '\'),
                                                                                       CHR (
                                                                                          14844057),
                                                                                       ''),
                                                                                    CHR (
                                                                                       49853),
                                                                                    '1/2'),
                                                                                 CHR (
                                                                                    49854),
                                                                                 '3/4'),
                                                                              CHR (
                                                                                 49852),
                                                                              '1/4'),
                                                                           CHR (
                                                                              14844066),
                                                                           ''),
                                                                        '\\*',
                                                                        ''),
                                                                     '\\^',
                                                                     ''''),
                                                                  '~',
                                                                  ''),
                                                               '%',
                                                               ''),
                                                            '`',
                                                            ''),
                                                         '!',
                                                         ''),
                                                      '@',
                                                      ''),
                                                   '#',
                                                   ''),
                                                '&',
                                                ''),
                                             '=',
                                             ''),
                                          '>',
                                          ''),
                                       '<',
                                       ''),
                                    ';',
                                    ''),
                                 ':',
                                 ''),
                              '*',
                              ''),
                           '^',
                           ''),
                        CHR (14844061),
                        ''),
                     CHR (14844057),
                     ''),
                  CHR (14844051),
                  '');
            l_item_table (1).DESCRIPTION := REC_ITEM.ATTRIBUTE_VALUE;
         END IF;
      END LOOP;

      IF NVL (ln_error_count, 0) = 0
      THEN
         write_log (l_success_mess);
      ELSE
         write_log (l_error_mess);

         RAISE EXECUTION_ERROR;
      END IF;

      g_sec := 'Item Standard API calling ';

      l_success_mess := 'Item Standard API Completed Successfully';
      l_error_mess := 'Item Standard API variables Completed with Error';

      IF NVL (l_item_table.COUNT, 0) > 0 AND ln_error_count = 0
      THEN
         l_item_table (1).attribute_category := 'WC';
         l_item_table (1).Transaction_Type := 'UPDATE';
         l_item_table (1).inventory_item_id := ln_inventory_item_id;
         l_item_table (1).organization_id := 222;

         EGO_ITEM_PUB.Process_Items (p_api_version     => 1.0,
                                     p_init_msg_list   => FND_API.g_TRUE,
                                     p_commit          => FND_API.g_TRUE,
                                     p_Item_Tbl        => l_item_table,
                                     x_Item_Tbl        => x_item_table,
                                     x_return_status   => x_return_status,
                                     x_msg_count       => x_msg_count);

         g_sec := 'Item Standard API calling Completed ';

         IF x_return_status <> 'S'
         THEN
            lvc_return_flag := 'N';
            write_log (l_error_mess);
            Error_Handler.Get_message_list (l_error_message_list);

            FOR i IN 1 .. l_error_message_list.COUNT
            LOOP
               l_error_mess := l_error_message_list (i).MESSAGE_TEXT;
            END LOOP;

            X_ERR_MESS := l_error_mess;
            write_log (l_error_mess);
         ELSE
            write_log (l_success_mess);
            X_ERR_MESS := NULL;
            lvc_return_flag := 'Y';
         END IF;
      ELSIF NVL (l_item_table.COUNT, 0) = 0 AND ln_error_count = 0
      THEN
         write_log ('No Records Processed/No Errors');
         X_ERR_MESS := NULL;
         lvc_return_flag := 'Y';
      ELSIF NVL (ln_error_count, 0) > 0
      THEN
         write_log ('Master Data have Errors, hence not processed');
         lvc_return_flag := 'N';
         X_ERR_MESS := NULL;
      END IF;

      l_item_table.delete;
      write_log ('Item Master Data Completed');
      RETURN lvc_return_flag;
   EXCEPTION
      WHEN EXECUTION_ERROR
      THEN
         l_err_msg :=
               l_err_msg
            || ' ...Error_Stack...'
            || DBMS_UTILITY.format_error_stack ()
            || ' Error_Backtrace...'
            || DBMS_UTILITY.format_error_backtrace ();

         -- Calling ERROR API
         xxcus_error_pkg.xxcus_error_main_api (
            p_called_from         => g_err_callfrom || '.' || g_procedure,
            p_ora_error_msg       => SUBSTR (l_err_msg, 1, 2000),
            p_error_desc          => SUBSTR (l_err_msg, 1, 240),
            p_distribution_list   => g_distro_list,
            p_module              => 'INV');

         write_log (l_err_msg);
         X_ERR_MESS := l_err_msg;

         RETURN 'N';
      WHEN OTHERS
      THEN
         l_err_msg :=
               l_err_msg
            || ' ...Error_Stack...'
            || DBMS_UTILITY.format_error_stack ()
            || ' Error_Backtrace...'
            || DBMS_UTILITY.format_error_backtrace ();

         -- Calling ERROR API
         xxcus_error_pkg.xxcus_error_main_api (
            p_called_from         => g_err_callfrom || '.' || g_procedure,
            p_ora_error_msg       => SUBSTR (l_err_msg, 1, 2000),
            p_error_desc          => SUBSTR (l_err_msg, 1, 240),
            p_distribution_list   => g_distro_list,
            p_module              => 'INV');
         write_log (l_err_msg);
         X_ERR_MESS := l_err_msg;
         RETURN 'N';
   END;


   FUNCTION validate_date (p_string IN VARCHAR2)
      RETURN DATE
   /*********************************************************************************************************
     $Header validate_date $
     PURPOSE:     Procedure to validate date
     REVISIONS:
     Ver    Date         Author                Description
     ------ -----------  ------------------    -------------------------------------------------------------
      1.2      19-Aug-2016   P.Vamshidhar         TMS#20160603-00067 - Kiewit Phase 2 - Upload Data to PDH
     ********************************************************************************************************/
   IS
   BEGIN
      RETURN TO_DATE (p_string, 'MONTH DD, YYYY');
   EXCEPTION
      WHEN OTHERS
      THEN
         BEGIN
            RETURN TO_DATE (p_string, 'MM/DD/YYYY');
         EXCEPTION
            WHEN OTHERS
            THEN
               BEGIN
                  RETURN TO_DATE (p_string, 'DD-MON-RR');
               EXCEPTION
                  WHEN OTHERS
                  THEN
                     RETURN NULL;
               END;
         END;
   END;

   FUNCTION PDH_DATA_UPDATE (P_ITEM_NUMBER    IN     VARCHAR2,
                             X_PDH_ERR_MESS      OUT VARCHAR2,
                             X_FUT_ERR_MESS      OUT VARCHAR2)
      RETURN VARCHAR2
   IS
      g_sec                  VARCHAR2 (1000);
      g_procedure            VARCHAR2 (100) := 'PDH_DATA_UPDATE';

      CURSOR CUR_STG_ITEM_GROUP (
         CP_ITEM_NUMBER    VARCHAR2)
      IS
           SELECT XCST.ITEM_NUMBER,
                  EAV.APPLICATION_ID,
                  EAGV.ATTR_GROUP_ID,
                  EAGV.ATTR_GROUP_NAME,
                  EAGV.ATTR_GROUP_TYPE,
                  XCST.ATTR_GROUP_DISP_NAME                -- Added in Rev 1.5
             FROM APPS.EGO_ATTR_GROUPS_V EAGV,
                  APPS.EGO_ATTRS_V EAV,
                  XXWC.XXWC_B2B_CAT_STG_TBL XCST
            WHERE     EAV.ATTR_GROUP_NAME = EAGV.ATTR_GROUP_NAME
                  AND XCST.ATTR_GROUP_DISP_NAME = EAGV.ATTR_GROUP_DISP_NAME
                  AND EAV.ATTR_DISPLAY_NAME = XCST.ATTR_DISPLAY_NAME
                  AND XCST.ITEM_NUMBER = CP_ITEM_NUMBER
                  AND XCST.PROCESS_STATUS IN ('New')
         GROUP BY XCST.ITEM_NUMBER,
                  EAV.APPLICATION_ID,
                  EAGV.ATTR_GROUP_ID,
                  EAGV.ATTR_GROUP_NAME,
                  EAGV.ATTR_GROUP_TYPE,
                  XCST.ATTR_GROUP_DISP_NAME;               -- Added in Rev 1.5

      CURSOR CUR_STG_ITEM_GROUP_ATTR (
         CP_ITEM_NUMBER    VARCHAR2,
         CP_GROUP_NAME     VARCHAR2)
      IS
           SELECT XCST.ITEM_NUMBER,
                  EAV.APPLICATION_ID,
                  EAGV.ATTR_GROUP_ID,
                  EAGV.ATTR_GROUP_NAME,
                  EAGV.ATTR_GROUP_TYPE,
                  EAV.DATABASE_COLUMN,
                  EAV.DATA_TYPE_CODE,
                  EAV.ATTR_NAME,
                  XCST.ATTRIBUTE_VALUE ATTRIBUTE_VALUE,
                  XCST.ROWID ROW_ID,
                  EAV.VALUE_SET_ID,
                  EAV.VALUE_SET_NAME,
                  XCST.ATTR_DISPLAY_NAME
             FROM APPS.EGO_ATTR_GROUPS_V EAGV,
                  APPS.EGO_ATTRS_V EAV,
                  XXWC.XXWC_B2B_CAT_STG_TBL XCST
            WHERE     EAV.ATTR_GROUP_NAME = EAGV.ATTR_GROUP_NAME
                  AND XCST.ATTR_GROUP_DISP_NAME = EAGV.ATTR_GROUP_DISP_NAME
                  AND EAV.ATTR_DISPLAY_NAME = XCST.ATTR_DISPLAY_NAME
                  AND XCST.ITEM_NUMBER = CP_ITEM_NUMBER
                  AND EAV.ENABLED_FLAG = 'Y'
                  AND EAGV.ATTR_GROUP_NAME = CP_GROUP_NAME
                  AND EAV.ATTR_NAME <> 'XXWC_FEATURES_BENEFITS_ATTR'
                  AND XCST.PROCESS_STATUS IN ('New')
                  AND (    XCST.ATTR_DISPLAY_NAME NOT LIKE 'Thumbnail Image%'
                       AND XCST.ATTR_DISPLAY_NAME NOT LIKE 'Fullsize Image%')
         ORDER BY EAV.ATTR_GROUP_NAME;


      CURSOR CUR_STG_ITEM_FUTU_ATTR (
         CP_ITEM_NUMBER    VARCHAR2)
      IS
           SELECT XCST.ITEM_NUMBER,
                  EAV.APPLICATION_ID,
                  EAGV.ATTR_GROUP_ID,
                  EAGV.ATTR_GROUP_NAME,
                  EAGV.ATTR_GROUP_TYPE,
                  EAV.DATABASE_COLUMN,
                  EAV.DATA_TYPE_CODE,
                  EAV.ATTR_NAME,
                  XCST.ATTRIBUTE_VALUE ATTRIBUTE_VALUE,
                  XCST.ROWID ROW_ID,
                  EAV.VALUE_SET_ID,
                  EAV.VALUE_SET_NAME,
                  XCST.ATTR_DISPLAY_NAME
             FROM APPS.EGO_ATTR_GROUPS_V EAGV,
                  APPS.EGO_ATTRS_V EAV,
                  XXWC.XXWC_B2B_CAT_STG_TBL XCST
            WHERE     EAV.ATTR_GROUP_NAME = EAGV.ATTR_GROUP_NAME
                  AND XCST.ATTR_GROUP_DISP_NAME = EAGV.ATTR_GROUP_DISP_NAME
                  AND EAV.ATTR_DISPLAY_NAME = XCST.ATTR_DISPLAY_NAME
                  AND XCST.ITEM_NUMBER = CP_ITEM_NUMBER
                  AND EAV.ENABLED_FLAG = 'Y'
                  AND EAV.ATTR_NAME = 'XXWC_FEATURES_BENEFITS_ATTR'
                  AND XCST.PROCESS_STATUS IN ('New')
         ORDER BY EAV.ATTR_GROUP_NAME;

      -- API variables
      l_attr_row_table       EGO_USER_ATTR_ROW_TABLE;
      l_attr_data_table      EGO_USER_ATTR_DATA_TABLE;
      l_error_message_list   Error_handler.error_tbl_type;
      x_failed_row_id_list   VARCHAR2 (1000);
      x_return_status        VARCHAR2 (100);
      x_msg_count            NUMBER;
      x_msg_data             VARCHAR2 (10000);
      x_error_code           NUMBER;
      l_row_identifier       NUMBER;
      l_attr_row_index       NUMBER := 1;
      l_attr_data_index      NUMBER := 1;
      l_org_id               NUMBER := 222;
      ln_fut_count           NUMBER := NULL;


      -- Variables Declaration
      l_success_mess         VARCHAR2 (4000);
      l_error_mess           VARCHAR2 (4000);
      lvc_attr_sql           VARCHAR2 (32000);
      ln_inventory_item_id   MTL_SYSTEM_ITEMS_B.INVENTORY_ITEM_ID%TYPE;
      l_attribute_value      VARCHAR2 (4000);
      lvc_validate_vs        VARCHAR2 (10);
      l_err_msg              VARCHAR2 (4000);
      ln_error_count         NUMBER := 0;
      ln_datatype_flag       NUMBER;
      ld_attr_date           DATE;
      lvc_return_flag        VARCHAR2 (1);
      lvc_return_flag1       VARCHAR2 (1);
      l_application_id       NUMBER;
      l_attr_group_id        NUMBER;
      l_attr_group_name      VARCHAR2 (100);
      l_attr_group_type      VARCHAR2 (100);
      l_count                NUMBER;                       -- Added in Rev 1.5
   BEGIN
      g_procedure := 'PDH_DATA_UPDATE';
      l_attr_row_index := 0;
      l_attr_data_index := 0;
      l_attr_data_table := EGO_USER_ATTR_DATA_TABLE ();
      l_attr_row_table := EGO_USER_ATTR_ROW_TABLE ();

      g_sec := 'Initializing API PLSQL table';
      l_success_mess := 'Initializing PLSQL variables Completed Successfully';
      l_error_mess := ' Initializing PLSQL variables Completed with Error';

      SELECT inventory_item_id
        INTO ln_inventory_item_id
        FROM MTL_SYSTEM_ITEMS_B
       WHERE SEGMENT1 = P_ITEM_NUMBER AND ORGANIZATION_ID = l_org_id;

      g_sec := 'PDH Attribute Groups Process start';

      write_log (g_sec);

      FOR REC_STG_ITEM_GROUP IN CUR_STG_ITEM_GROUP (P_ITEM_NUMBER)
      LOOP
         -- Added below code in Rev 1.4 -  Begin
         IF REC_STG_ITEM_GROUP.ATTR_GROUP_NAME = 'XXWC_FEATURES_BENEFITS_AG'
         THEN
            CONTINUE;
         END IF;

         -- Added below code in Rev 1.5 -  Begin
         l_count := 0;
         write_log (g_sec);

         BEGIN
            SELECT COUNT (1)
              INTO l_count
              FROM APPS.EGO_MTL_SY_ITEMS_EXT_B A, APPS.MTL_SYSTEM_ITEMS_B B
             WHERE     A.INVENTORY_ITEM_ID = B.INVENTORY_ITEM_ID
                   AND B.ORGANIZATION_ID = 222
                   AND B.SEGMENT1 = REC_STG_ITEM_GROUP.ITEM_NUMBER
                   AND ATTR_GROUP_ID = REC_STG_ITEM_GROUP.ATTR_GROUP_ID;
         EXCEPTION
            WHEN OTHERS
            THEN
               l_count := 0;
         END;

         g_sec :=
               'Item '
            || REC_STG_ITEM_GROUP.ITEM_NUMBER
            || ' Attr Group Id '
            || REC_STG_ITEM_GROUP.ATTR_GROUP_ID;

         IF l_count = 0
         THEN
            l_count := 0;

            BEGIN
               SELECT COUNT (1)
                 INTO l_count
                 FROM XXWC.XXWC_B2B_CAT_STG_TBL XCST
                WHERE     XCST.ITEM_NUMBER = REC_STG_ITEM_GROUP.ITEM_NUMBER
                      AND XCST.ATTR_GROUP_DISP_NAME =
                             REC_STG_ITEM_GROUP.ATTR_GROUP_DISP_NAME
                      AND XCST.ATTRIBUTE_VALUE IS NOT NULL;
            EXCEPTION
               WHEN OTHERS
               THEN
                  l_count := 0;
            END;

            IF l_count = 0
            THEN
               CONTINUE;
            END IF;

            g_sec :=
                  'Item '
               || REC_STG_ITEM_GROUP.ITEM_NUMBER
               || ' Attr Group Name '
               || REC_STG_ITEM_GROUP.ATTR_GROUP_DISP_NAME;
            write_log (g_sec);
         END IF;

         -- Added below code in Rev 1.5 - End

         -- Added above code in Rev 1.4 -  End

         l_attr_row_table.EXTEND;
         l_attr_row_index := l_attr_row_table.COUNT;
         l_row_identifier := l_attr_row_index;
         l_attr_row_table (l_attr_row_index) :=
            ego_user_attr_row_obj (l_row_identifier,
                                   rec_stg_item_group.attr_group_id,
                                   rec_stg_item_group.application_id,
                                   rec_stg_item_group.attr_group_type,
                                   rec_stg_item_group.attr_group_name,
                                   'ITEM_LEVEL',
                                   NULL,
                                   NULL,
                                   NULL,
                                   NULL,
                                   NULL,
                                   'SYNC');

         g_sec := 'PDH Group Attribute Values Process';
         write_log ('Group Name ' || rec_stg_item_group.attr_group_name);

         FOR REC_STG_ITEM_GROUP_ATTR
            IN CUR_STG_ITEM_GROUP_ATTR (REC_STG_ITEM_GROUP.ITEM_NUMBER,
                                        REC_STG_ITEM_GROUP.ATTR_GROUP_NAME)
         LOOP
            write_log (
                  'Attribute Name '
               || REC_STG_ITEM_GROUP_ATTR.ATTR_DISPLAY_NAME
               || ' Attribute Value '
               || REC_STG_ITEM_GROUP_ATTR.ATTRIBUTE_VALUE);

            -- Variables declaration
            lvc_attr_sql := NULL;
            l_attribute_value := NULL;
            l_err_msg := NULL;
            lvc_validate_vs := NULL;
            ln_datatype_flag := NULL;
            ld_attr_date := NULL;

            lvc_attr_sql :=
                  'SELECT '
               || REC_STG_ITEM_GROUP_ATTR.DATABASE_COLUMN
               || ' FROM APPS.EGO_MTL_SY_ITEMS_EXT_B WHERE ORGANIZATION_ID=222 AND INVENTORY_ITEM_ID= :1 AND ATTR_GROUP_ID = :2';

            g_sec := 'PDH Group Attribute Values Query';

            BEGIN
               EXECUTE IMMEDIATE lvc_attr_sql
                  INTO l_attribute_value
                  USING ln_inventory_item_id,
                        REC_STG_ITEM_GROUP_ATTR.attr_group_id;
            EXCEPTION
               WHEN OTHERS
               THEN
                  l_attribute_value := NULL;
            END;

            IF NVL (TO_CHAR (l_attribute_value), 'ABCXYZ') =
                  NVL (TO_CHAR (REC_STG_ITEM_GROUP_ATTR.ATTRIBUTE_VALUE),
                       'ABCXYZ')
            THEN
               CONTINUE;
            END IF;


            IF REC_STG_ITEM_GROUP_ATTR.DATA_TYPE_CODE IN ('C', 'A')
            THEN
               l_attr_data_table.EXTEND;
               l_attr_data_index := l_attr_data_table.COUNT;
               l_attr_data_table (l_attr_data_index) :=
                  ego_user_attr_data_obj (
                     l_row_identifier,
                     REC_STG_ITEM_GROUP_ATTR.ATTR_NAME,
                     REC_STG_ITEM_GROUP_ATTR.ATTRIBUTE_VALUE,
                     NULL,
                     NULL,
                     NULL,
                     NULL,
                     l_row_identifier);
            ELSIF REC_STG_ITEM_GROUP_ATTR.DATA_TYPE_CODE = 'N'
            THEN
               g_sec :=
                  'Validating Attribute Value Attribute Data Type Is Number';

               BEGIN
                  SELECT 1
                    INTO ln_datatype_flag
                    FROM DUAL
                   WHERE (   REGEXP_LIKE (
                                NVL (REC_STG_ITEM_GROUP_ATTR.ATTRIBUTE_VALUE,
                                     0),
                                '^[0-9]+(\.([0-9]{1,20})?)?$')
                          OR REGEXP_LIKE (
                                NVL (REC_STG_ITEM_GROUP_ATTR.ATTRIBUTE_VALUE,
                                     0),
                                '^+(.([0-9]{1,20})?)?$'));
               EXCEPTION
                  WHEN OTHERS
                  THEN
                     ln_datatype_flag := 0;
               END;

               IF NVL (ln_datatype_flag, 0) = 0
               THEN
                  l_err_msg :=
                        '"'
                     || REC_STG_ITEM_GROUP_ATTR.ATTRIBUTE_VALUE -- Changed in Rev 1.4
                     || '"'
                     || ' is an Invalid Value. Please enter numeric values only.';


                  UPDATE XXWC.XXWC_B2B_CAT_STG_TBL
                     SET ERROR_MESSAGE = l_err_msg, PROCESS_STATUS = 'Error'
                   WHERE ROWID = REC_STG_ITEM_GROUP_ATTR.row_id;

                  ln_error_count := ln_error_count + 1;
                  CONTINUE;
               END IF;


               l_attr_data_table.EXTEND;
               l_attr_data_index := l_attr_data_table.COUNT;
               l_attr_data_table (l_attr_data_index) :=
                  ego_user_attr_data_obj (
                     l_row_identifier,
                     REC_STG_ITEM_GROUP_ATTR.ATTR_NAME,
                     NULL,
                     REC_STG_ITEM_GROUP_ATTR.ATTRIBUTE_VALUE,
                     NULL,
                     NULL,
                     NULL,
                     l_row_identifier);
            ELSIF REC_STG_ITEM_GROUP_ATTR.DATA_TYPE_CODE = 'X'
            THEN
               IF REC_STG_ITEM_GROUP_ATTR.ATTRIBUTE_VALUE IS NOT NULL
               THEN
                  ld_attr_date :=
                     validate_date (REC_STG_ITEM_GROUP_ATTR.ATTRIBUTE_VALUE);

                  IF ld_attr_date IS NULL
                  THEN
                     l_err_msg :=
                           REC_STG_ITEM_GROUP_ATTR.ATTRIBUTE_VALUE
                        || ' is an invalid value. Please use date format DD-MON-YYYY';

                     ln_error_count := ln_error_count + 1;

                     UPDATE XXWC.XXWC_B2B_CAT_STG_TBL
                        SET ERROR_MESSAGE = l_err_msg,
                            PROCESS_STATUS = 'Error'
                      WHERE ROWID = REC_STG_ITEM_GROUP_ATTR.row_id;

                     CONTINUE;
                  END IF;



                  l_attr_data_table.EXTEND;
                  l_attr_data_index := l_attr_data_table.COUNT;
                  l_attr_data_table (l_attr_data_index) :=
                     ego_user_attr_data_obj (
                        l_row_identifier,
                        REC_STG_ITEM_GROUP_ATTR.ATTR_NAME,
                        NULL,
                        NULL,
                        ld_attr_date,
                        NULL,
                        NULL,
                        l_row_identifier);
               END IF;
            END IF;
         END LOOP;
      END LOOP;

      write_log (l_success_mess);

      l_success_mess := 'Calling Oracle API Completed Successfully';
      l_error_mess := 'Calling Oracle API Completed with Error';


      g_sec := 'Calling Oracle API';
      write_log (g_sec);
      write_log (
            'table'
         || NVL (l_attr_data_table.COUNT, 0)
         || ' Errors '
         || NVL (ln_error_count, 0));

      IF NVL (l_attr_data_table.COUNT, 0) > 0 AND NVL (ln_error_count, 0) = 0
      THEN
         write_log ('Entered into PDH Update API' || l_attr_data_table.COUNT);

         EGO_ITEM_PUB.PROCESS_USER_ATTRS_FOR_ITEM (
            P_API_VERSION             => '1.0',
            P_INVENTORY_ITEM_ID       => ln_inventory_item_id,
            P_ORGANIZATION_ID         => l_org_id,
            P_ATTRIBUTES_ROW_TABLE    => l_attr_row_table,
            P_ATTRIBUTES_DATA_TABLE   => l_attr_data_table,
            X_FAILED_ROW_ID_LIST      => x_failed_row_id_list,
            X_RETURN_STATUS           => x_return_status,
            X_ERRORCODE               => x_error_code,
            X_MSG_COUNT               => x_msg_count,
            X_MSG_DATA                => x_msg_data);

         g_sec := 'Validting Oracle API Results';

         IF x_return_status <> 'S'
         THEN
            write_log (l_error_mess);
            Error_Handler.Get_message_list (l_error_message_list);

            FOR i IN 1 .. l_error_message_list.COUNT
            LOOP
               l_err_msg := l_error_message_list (i).MESSAGE_TEXT;
            END LOOP;

            X_PDH_ERR_MESS := l_err_msg;

            g_sec := 'Updating API Results to custom table';
            write_log (l_err_msg);
            lvc_return_flag := 'N';
         ELSE
            write_log (l_success_mess);
            X_PDH_ERR_MESS := NULL;
            lvc_return_flag := 'Y';
         END IF;
      ELSIF NVL (ln_error_count, 0) > 0
      THEN
         lvc_return_flag := 'N';
         X_PDH_ERR_MESS := 'Data Error';
      ELSIF     NVL (l_attr_data_table.COUNT, 0) = 0
            AND NVL (ln_error_count, 0) = 0
      THEN
         X_PDH_ERR_MESS := NULL;
         lvc_return_flag := 'Y';
      END IF;

      l_attr_data_table.delete;
      l_attr_row_table.delete;

      --- Future and Benefits Process.
      BEGIN
         FND_FILE.PUT_LINE (
            FND_fILE.LOG,
               'Checking any Future And Benefits Attributes for respective Item '
            || p_item_number);

         SELECT COUNT (1)
           INTO ln_fut_count
           FROM APPS.EGO_ATTR_GROUPS_V EAGV,
                APPS.EGO_ATTRS_V EAV,
                XXWC.XXWC_B2B_CAT_STG_TBL XCST
          WHERE     EAV.ATTR_GROUP_NAME = EAGV.ATTR_GROUP_NAME
                AND XCST.ATTR_GROUP_DISP_NAME = EAGV.ATTR_GROUP_DISP_NAME
                AND EAV.ATTR_DISPLAY_NAME = XCST.ATTR_DISPLAY_NAME
                AND XCST.ITEM_NUMBER = p_item_number
                AND XCST.ATTR_GROUP_DISP_NAME LIKE 'Features%Benefits%'
                AND XCST.PROCESS_STATUS IN ('New');

         lvc_return_flag1 := 'Y';

         IF NVL (ln_fut_count, 0) > 0
         THEN
            BEGIN
                 SELECT EAV.APPLICATION_ID,
                        EAGV.ATTR_GROUP_ID,
                        EAGV.ATTR_GROUP_NAME,
                        EAGV.ATTR_GROUP_TYPE
                   INTO l_application_id,
                        l_attr_group_id,
                        l_attr_group_name,
                        l_attr_group_type
                   FROM APPS.EGO_ATTR_GROUPS_V EAGV,
                        APPS.EGO_ATTRS_V EAV,
                        XXWC.XXWC_B2B_CAT_STG_TBL XCST
                  WHERE     EAV.ATTR_GROUP_NAME = EAGV.ATTR_GROUP_NAME
                        AND XCST.ATTR_GROUP_DISP_NAME =
                               EAGV.ATTR_GROUP_DISP_NAME
                        AND EAV.ATTR_DISPLAY_NAME = XCST.ATTR_DISPLAY_NAME
                        AND XCST.ITEM_NUMBER = P_ITEM_NUMBER
                        AND EAGV.ATTR_GROUP_NAME = 'XXWC_FEATURES_BENEFITS_AG'
               GROUP BY XCST.ITEM_NUMBER,
                        EAV.APPLICATION_ID,
                        EAGV.ATTR_GROUP_ID,
                        EAGV.ATTR_GROUP_NAME,
                        EAGV.ATTR_GROUP_TYPE;

               FND_FILE.PUT_LINE (FND_fILE.LOG,
                                  'Application Id : ' || l_application_id);
               FND_FILE.PUT_LINE (FND_fILE.LOG,
                                  'Attr_group_id  : ' || l_attr_group_id);
               FND_FILE.PUT_LINE (FND_fILE.LOG,
                                  'Attr Group Name: ' || l_attr_group_name);
               FND_FILE.PUT_LINE (FND_fILE.LOG,
                                  'Attr Group Type: ' || l_attr_group_type);

               x_fut_err_mess := NULL;
               l_attr_row_index := 0;
               l_attr_row_table.EXTEND;
               l_attr_row_index := l_attr_row_table.COUNT;
               l_row_identifier := l_attr_row_index;
               l_attr_row_table (l_attr_row_index) :=
                  ego_user_attr_row_obj (l_row_identifier,
                                         l_attr_group_id,
                                         l_application_id,
                                         l_attr_group_type,
                                         l_attr_group_name,
                                         'ITEM_LEVEL',
                                         NULL,
                                         NULL,
                                         NULL,
                                         NULL,
                                         NULL,
                                         'SYNC');

               FOR REC_STG_ITEM_FUTU_ATTR
                  IN CUR_STG_ITEM_FUTU_ATTR (P_ITEM_NUMBER)
               LOOP
                  l_attr_data_index := 0;
                  l_attr_data_table.EXTEND;
                  l_attr_data_index := l_attr_data_table.COUNT;
                  l_attr_data_table (l_attr_data_index) :=
                     ego_user_attr_data_obj (
                        l_row_identifier,
                        REC_STG_ITEM_FUTU_ATTR.ATTR_NAME,
                        REC_STG_ITEM_FUTU_ATTR.ATTRIBUTE_VALUE,
                        NULL,
                        NULL,
                        NULL,
                        NULL,
                        l_row_identifier);


                  EGO_ITEM_PUB.PROCESS_USER_ATTRS_FOR_ITEM (
                     P_API_VERSION             => '1.0',
                     P_INVENTORY_ITEM_ID       => ln_inventory_item_id,
                     P_ORGANIZATION_ID         => l_org_id,
                     P_ATTRIBUTES_ROW_TABLE    => l_attr_row_table,
                     P_ATTRIBUTES_DATA_TABLE   => l_attr_data_table,
                     X_FAILED_ROW_ID_LIST      => x_failed_row_id_list,
                     X_RETURN_STATUS           => x_return_status,
                     X_ERRORCODE               => x_error_code,
                     X_MSG_COUNT               => x_msg_count,
                     X_MSG_DATA                => x_msg_data);

                  FND_FILE.PUT_LINE (
                     FND_FILE.LOG,
                        ' FUTURE Attribute Value '
                     || REC_STG_ITEM_FUTU_ATTR.ATTRIBUTE_VALUE
                     || ' Status '
                     || x_return_status);

                  IF x_return_status <> 'S'
                  THEN
                     Error_Handler.Get_message_list (l_error_message_list);

                     FOR i IN 1 .. l_error_message_list.COUNT
                     LOOP
                        l_err_msg := l_error_message_list (i).MESSAGE_TEXT;
                     END LOOP;

                     x_fut_err_mess := x_fut_err_mess || ' ' || l_err_msg;
                     write_log (l_err_msg);
                  END IF;

                  FND_FILE.PUT_LINE (
                     FND_fILE.LOG,
                        'Attribute Name: '
                     || REC_STG_ITEM_FUTU_ATTR.ATTR_NAME
                     || ' Attribute Value: '
                     || REC_STG_ITEM_FUTU_ATTR.ATTRIBUTE_VALUE
                     || 'Return Status : '
                     || x_return_status);

                  l_attr_data_table.delete;
               END LOOP;
            EXCEPTION
               WHEN OTHERS
               THEN
                  FND_FILE.PUT_LINE (
                     FND_FILE.LOG,
                     'Error Occured(Group) ' || SUBSTR (SQLERRM, 1, 250));
            END;
         END IF;

         IF REPLACE (x_fut_err_mess, ' ', '') IS NOT NULL
         THEN
            lvc_return_flag1 := 'N';
         END IF;

         FND_FILE.PUT_LINE (FND_fILE.LOG,
                            'Features and Benefits Processe Completed.');
      EXCEPTION
         WHEN NO_DATA_FOUND
         THEN
            FND_FILE.PUT_LINE (
               FND_fILE.LOG,
               'No Rows found for Features and Benefits ' || p_item_number);
         WHEN OTHERS
         THEN
            FND_FILE.PUT_LINE (
               FND_fILE.LOG,
               'Error Occured (Main) ' || SUBSTR (SQLERRM, 1, 250));
      END;

      FND_FILE.PUT_LINE (
         FND_fILE.LOG,
            ' X_PDH_ERR_MESS: '
         || X_PDH_ERR_MESS
         || ' X_FUT_ERR_MESS:'
         || X_FUT_ERR_MESS);
      FND_FILE.PUT_LINE (
         FND_fILE.LOG,
            ' lvc_return_flag: '
         || lvc_return_flag
         || ' lvc_return_flag1:'
         || lvc_return_flag1);

      IF lvc_return_flag = 'Y' AND lvc_return_flag1 = 'Y'
      THEN
         lvc_return_flag := 'Y';
      ELSE
         lvc_return_flag := 'N';
      END IF;

      -- Deleting data from PL/SQL tables.
      l_attr_data_table.delete;
      l_attr_row_table.delete;
      RETURN lvc_return_flag;
   EXCEPTION
      WHEN OTHERS
      THEN
         lvc_return_flag := 'N';
         RETURN lvc_return_flag;
         write_log ('Error Occured (Future)');
   END;
PROCEDURE AR_WRITEOFF_UPDATE(P_BATCH_NUMBER IN NUMBER,
                             P_STATUS IN VARCHAR2,
                               p_user_NAME    IN VARCHAR2,
                               P_RESP_ID      IN NUMBER,
                               P_RESP_APPL_ID IN NUMBER)
  /*********************************************************************************************************
       $Header AR_WRITEOFF_UPDATE $
       PURPOSE:     Procedure to trigger writeoff update process from Apex.
       REVISIONS:
    -- Version When         Who        Did what
    -- ------- -----------  --------   -----------------------------------------------------
     --1.6      26-APR-2018   Nancy Pahwa    TMS#20160218-00198   AR Writeoff and AR unapply reapply
    ---------------------------------------------------------------------------------*/
   IS
    l_request_id NUMBER;
    l_err_mess   VARCHAR2(4000);
  BEGIN
    BEGIN
      SELECT user_id
        INTO g_user_id
        FROM APPS.FND_USER
       WHERE USER_NAME = P_USER_NAME;
    EXCEPTION
      WHEN NO_DATA_FOUND THEN
        l_err_mess := P_USER_NAME || ' Not Exist';
       write_log(l_err_mess);
    END;
  
    write_log('Initializing Variables ');
  
    APPS.MO_GLOBAL.SET_POLICY_CONTEXT('S', 162);
    APPS.fnd_global.apps_initialize(g_user_id, p_resp_id, p_resp_appl_id);
  
    l_request_id := fnd_request.submit_request(application => 'XXWC',
                                               program     => 'XXWC_AR_RECEIPT_MASS_WRITEOFF',
                                               description => NULL,
                                               start_time  => SYSDATE,
                                               sub_request => FALSE,
                                               argument1   => P_BATCH_NUMBER,
                                               argument2   => P_STATUS);
  
    IF l_request_id > 0 THEN
      COMMIT;
    END IF;
  END;
  PROCEDURE AR_UNAPPLY_REAPPLY_UPDATE(P_BATCH_NUMBER IN NUMBER,
                             P_STATUS IN VARCHAR2,
                               p_user_NAME    IN VARCHAR2,
                               P_RESP_ID      IN NUMBER,
                               P_RESP_APPL_ID IN NUMBER)
  /*********************************************************************************************************
       $Header AR_WRITEOFF_UPDATE $
       PURPOSE:     Procedure to trigger unapply reapply update process from Apex.
       REVISIONS:
    -- Version When         Who        Did what
    -- ------- -----------  --------   -----------------------------------------------------
       --1.6      26-APR-2018   Nancy Pahwa    TMS#20160218-00198   AR Writeoff and AR unapply reapply
    ---------------------------------------------------------------------------------*/
   IS
    l_request_id NUMBER;
    l_err_mess   VARCHAR2(4000);
  BEGIN
    BEGIN
      SELECT user_id
        INTO g_user_id
        FROM APPS.FND_USER
       WHERE USER_NAME = P_USER_NAME;
    EXCEPTION
      WHEN NO_DATA_FOUND THEN
        l_err_mess := P_USER_NAME || ' Not Exist';
       write_log(l_err_mess);
    END;
  
    write_log('Initializing Variables ');
  
    APPS.MO_GLOBAL.SET_POLICY_CONTEXT('S', 162);
    APPS.fnd_global.apps_initialize(g_user_id, p_resp_id, p_resp_appl_id);
  
    l_request_id := fnd_request.submit_request(application => 'XXWC',
                                               program     => 'XXWC_AR_RECEIPT_MASS_REAPPLY',
                                               description => NULL,
                                               start_time  => SYSDATE,
                                               sub_request => FALSE,
                                               argument1   => P_BATCH_NUMBER,
                                               argument2   => P_STATUS);
  
    IF l_request_id > 0 THEN
      COMMIT;
    END IF;
  END;
   PROCEDURE PDH_UPDATE (P_BATCH_NUMBER   IN NUMBER,
                         p_user_NAME      IN VARCHAR2,
                         P_RESP_ID        IN NUMBER,
                         P_RESP_APPL_ID   IN NUMBER)
   /*********************************************************************************************************
     $Header pdh_update $
     PURPOSE:     Procedure to trigger pdh update process from Apex.
     REVISIONS:
     Ver    Date         Author                Description
     ------ -----------  ------------------    -------------------------------------------------------------
      1.2      19-Aug-2016   P.Vamshidhar         TMS#20160603-00067 - Kiewit Phase 2 - Upload Data to PDH
     ********************************************************************************************************/
   IS
      l_request_id   NUMBER;
      l_err_mess     VARCHAR2 (4000);
   BEGIN
      BEGIN
         SELECT user_id
           INTO g_user_id
           FROM APPS.FND_USER
          WHERE USER_NAME = P_USER_NAME;
      EXCEPTION
         WHEN NO_DATA_FOUND
         THEN
            l_err_mess := P_USER_NAME || ' Not Exist';
            write_log (l_err_mess);
      END;

      write_log ('Initializing Variables ');

      APPS.MO_GLOBAL.SET_POLICY_CONTEXT ('S', 162);
      APPS.fnd_global.apps_initialize (g_user_id, p_resp_id, p_resp_appl_id);

      l_request_id :=
         fnd_request.submit_request (
            application   => 'XXWC',
            program       => 'XXWC_EGO_PDH_DATA_UPDATE',
            description   => NULL,
            start_time    => SYSDATE,
            sub_request   => FALSE,
            argument1     => P_BATCH_NUMBER);

      IF l_request_id > 0
      THEN
         COMMIT;
      END IF;
   END;

   FUNCTION XREF_DATA_UPDATE (P_ITEM_NUMBER    IN     VARCHAR2,
                              X_UPC_ERR_MESS      OUT VARCHAR2,
                              X_VEN_ERR_MESS      OUT VARCHAR2)
      RETURN VARCHAR2
   /*********************************************************************************************************
     $Header XREF_DATA_UPDATE $
     PURPOSE:     Procedure to trigger pdh update process from Apex.
     REVISIONS:
     Ver    Date         Author                Description
     ------ -----------  ------------------    -------------------------------------------------------------
      1.2      19-Aug-2016   P.Vamshidhar         TMS#20160603-00067 - Kiewit Phase 2 - Upload Data to PDH
     ********************************************************************************************************/
   IS
      -- Cursors declaration
      CURSOR CUR_XREF_NEW (
         P_ITEM_NUMBER1    VARCHAR2)
      IS
         SELECT XCST.ROWID ROW_ID,
                'UPC' Cross_Reference_Type,
                XCST.ATTRIBUTE_VALUE cross_Reference,
                '162' ATTRIBUTE_CATEGORY,
                APS.SEGMENT1 ATTRIBUTE1,
                MSIB.INVENTORY_ITEM_ID
           FROM XXWC.XXWC_B2B_CAT_STG_TBL XCST,
                APPS.MTL_SYSTEM_ITEMS_B MSIB,
                XXWC.XXWC_B2B_CAT_STG_TBL XCST1,
                APPS.AP_SUPPLIERS APS
          WHERE     XCST.ATTR_DISPLAY_NAME LIKE 'UPC%'
                AND XCST.ATTR_DISPLAY_NAME NOT LIKE '%Owner'
                AND msib.segment1 = xcst.item_number
                AND msib.organization_id = 222
                AND XCST.ITEM_NUMBER = XCST1.ITEM_NUMBER
                AND XCST1.ATTR_DISPLAY_NAME LIKE 'UPC%'
                AND XCST.ATTR_DISPLAY_NAME =
                       SUBSTR (XCST1.ATTR_DISPLAY_NAME, 1, 4)
                AND XCST1.ATTR_DISPLAY_NAME LIKE 'UPC%Owner'
                AND APS.VENDOR_NAME(+) = XCST1.ATTRIBUTE_VALUE
                AND XCST.ITEM_NUMBER = P_ITEM_NUMBER1
                AND NVL (XCST.PROCESS_STATUS, 'Success') <> 'Error'
                AND NOT EXISTS
                       (SELECT 1
                          FROM APPS.MTL_CROSS_REFERENCES MCR
                         WHERE     MCR.INVENTORY_ITEM_ID =
                                      MSIB.INVENTORY_ITEM_ID
                               AND MCR.CROSS_REFERENCE = XCST.ATTRIBUTE_VALUE
                               AND MCR.CROSS_REFERENCE_TYPE = 'UPC')
         UNION ALL
         SELECT XCST.ROWID ROW_ID,
                'VENDOR' Cross_Reference_Type,
                XCST.ATTRIBUTE_VALUE cross_Reference,
                '162' ATTRIBUTE_CATEGORY,
                APS.SEGMENT1 ATTRIBUTE1,
                MSIB.INVENTORY_ITEM_ID
           FROM XXWC.XXWC_B2B_CAT_STG_TBL XCST,
                APPS.MTL_SYSTEM_ITEMS_B MSIB,
                XXWC.XXWC_B2B_CAT_STG_TBL XCST1,
                APPS.AP_SUPPLIERS APS
          WHERE     XCST.ATTR_DISPLAY_NAME LIKE 'Vendor Part Number%'
                AND XCST.ATTR_DISPLAY_NAME NOT LIKE 'Vendor%Owner'
                AND msib.segment1 = xcst.item_number
                AND msib.organization_id = 222
                AND XCST.ITEM_NUMBER = XCST1.ITEM_NUMBER
                AND XCST1.ATTR_DISPLAY_NAME LIKE 'Vendor%Owner'
                AND SUBSTR (XCST.ATTR_DISPLAY_NAME, 1, 6) =
                       SUBSTR (XCST1.ATTR_DISPLAY_NAME, 1, 6)
                AND SUBSTR (TRIM (XCST.ATTR_DISPLAY_NAME), -1) =
                       SUBSTR (XCST1.ATTR_DISPLAY_NAME, 7, 1)
                AND APS.VENDOR_NAME(+) = XCST1.ATTRIBUTE_VALUE
                AND XCST.ITEM_NUMBER = P_ITEM_NUMBER1
                AND NVL (XCST.PROCESS_STATUS, 'Success') <> 'Error'
                AND NOT EXISTS
                       (SELECT 1
                          FROM APPS.MTL_CROSS_REFERENCES MCR
                         WHERE     MCR.INVENTORY_ITEM_ID =
                                      MSIB.INVENTORY_ITEM_ID
                               AND MCR.CROSS_REFERENCE = XCST.ATTRIBUTE_VALUE
                               AND MCR.CROSS_REFERENCE_TYPE = 'VENDOR');



      CURSOR CUR_XREF_UPDATE (
         P_ITEM_NUMBER1   IN VARCHAR2)
      IS
         SELECT XCST.ROWID ROW_ID,
                'UPC' Cross_Reference_Type,
                XCST.ATTRIBUTE_VALUE cross_Reference,
                '162' ATTRIBUTE_CATEGORY,
                APS1.SEGMENT1 MCR_VENDOR_NUM,
                APS2.SEGMENT1 XCST_VENDOR_NUM,
                MSIB.INVENTORY_ITEM_ID,
                MCR.CROSS_REFERENCE_ID
           FROM XXWC.XXWC_B2B_CAT_STG_TBL XCST,
                APPS.MTL_SYSTEM_ITEMS_B MSIB,
                XXWC.XXWC_B2B_CAT_STG_TBL XCST1,
                APPS.MTL_CROSS_REFERENCES MCR,
                APPS.AP_SUPPLIERS APS1,
                APPS.AP_SUPPLIERS APS2
          WHERE     XCST.ATTR_DISPLAY_NAME LIKE 'UPC%'
                AND XCST.ATTR_DISPLAY_NAME NOT LIKE '%Owner'
                AND msib.segment1 = xcst.item_number
                AND msib.organization_id = 222
                AND XCST.ITEM_NUMBER = XCST1.ITEM_NUMBER
                AND XCST1.ATTR_DISPLAY_NAME LIKE 'UPC%'
                AND XCST.ITEM_NUMBER = P_ITEM_NUMBER
                AND XCST.ATTR_DISPLAY_NAME =
                       SUBSTR (XCST1.ATTR_DISPLAY_NAME, 1, 4)
                AND XCST1.ATTR_DISPLAY_NAME LIKE 'UPC%Owner'
                AND MCR.CROSS_REFERENCE_TYPE = 'UPC'
                AND MSIB.INVENTORY_ITEM_ID = MCR.INVENTORY_ITEM_ID
                AND MCR.CROSS_REFERENCE = XCST.ATTRIBUTE_VALUE
                AND MCR.ATTRIBUTE1 = APS1.SEGMENT1(+)
                AND XCST1.ATTRIBUTE_VALUE = APS2.VENDOR_NAME(+)
                AND NVL (XCST.PROCESS_STATUS, 'Success') <> 'Error'
         UNION ALL
         SELECT XCST.ROWID ROW_ID,
                'VENDOR' Cross_Reference_Type,
                XCST.ATTRIBUTE_VALUE cross_Reference,
                '162' ATTRIBUTE_CATEGORY,
                APS1.SEGMENT1 MCR_VENDOR_NUM,
                APS2.SEGMENT1 XCST_VENDOR_NUM,
                MSIB.INVENTORY_ITEM_ID,
                MCR.CROSS_REFERENCE_ID
           FROM XXWC.XXWC_B2B_CAT_STG_TBL XCST,
                XXWC.XXWC_B2B_CAT_STG_TBL XCST1,
                MTL_SYSTEM_ITEMS_B MSIB,
                MTL_CROSS_REFERENCES MCR,
                APPS.AP_SUPPLIERS APS1,
                APPS.AP_SUPPLIERS APS2
          WHERE     XCST.ATTR_DISPLAY_NAME LIKE 'Vendor Part Number%'
                AND XCST.ATTR_DISPLAY_NAME NOT LIKE 'Vendor%Owner'
                AND XCST.ITEM_NUMBER = XCST1.ITEM_NUMBER
                AND XCST1.ATTR_DISPLAY_NAME LIKE 'Vendor%Owner'
                AND XCST1.ATTR_DISPLAY_NAME NOT LIKE 'Vendor Part Number%'
                AND SUBSTR (XCST.ATTR_DISPLAY_NAME, 1, 6) =
                       SUBSTR (XCST1.ATTR_DISPLAY_NAME, 1, 6)
                AND NVL (XCST.PROCESS_STATUS, 'Success') <> 'Error'
                AND XCST.ITEM_NUMBER = MSIB.SEGMENT1
                AND XCST.ITEM_NUMBER = P_ITEM_NUMBER
                AND MSIB.ORGANIZATION_ID = 222
                AND MSIB.INVENTORY_ITEM_ID = MCR.INVENTORY_ITEM_ID
                AND XCST.ATTRIBUTE_VALUE = MCR.CROSS_REFERENCE
                AND MCR.CROSS_REFERENCE_TYPE = 'VENDOR'
                AND MCR.ATTRIBUTE1 = APS1.SEGMENT1(+)
                AND XCST1.ATTRIBUTE_VALUE = APS2.VENDOR_NAME(+);

      -- Variables Declaration
      l_success_mess    VARCHAR2 (4000);
      l_error_mess      VARCHAR2 (4000);
      l_err_msg         VARCHAR2 (4000);
      execution_error   EXCEPTION;
      ln_rec_count      NUMBER := 0;
      lvc_create        VARCHAR2 (1) := 'X';
      lvc_update        VARCHAR2 (1) := 'X';
      lvc_return_flag   VARCHAR (1);

      l_init_msg_list   VARCHAR2 (2) := FND_API.G_TRUE;
      l_commit          VARCHAR2 (2) := FND_API.G_TRUE;
      l_XRef_tbl        MTL_CROSS_REFERENCES_PUB.XRef_Tbl_Type;
      x_message_list    Error_Handler.Error_Tbl_Type;
      x_return_status   VARCHAR2 (2);
      x_msg_count       NUMBER := 0;
      L_ERROR_MESSAGE   VARCHAR2 (1000);
   BEGIN
      g_procedure := 'XREF_DATA_UPDATE';
      g_sec := 'Initializing API PLSQL table -  CREATE';
      l_success_mess :=
         'Initializing PLSQL variables to CREATE Completed Successfully';
      l_error_mess :=
         ' Initializing PLSQL variables to CREATE Completed with Error';


      --Cross References Creation
      FOR REF_XREF_NEW IN CUR_XREF_NEW (P_ITEM_NUMBER)
      LOOP
         ln_rec_count := ln_rec_count + 1;
         l_XRef_tbl (ln_rec_count).Cross_Reference :=
            REF_XREF_NEW.cross_Reference;
         l_XRef_tbl (ln_rec_count).Cross_Reference_Type :=
            REF_XREF_NEW.Cross_Reference_Type;
         l_XRef_tbl (ln_rec_count).Inventory_Item_Id :=
            REF_XREF_NEW.inventory_item_id;
         l_XRef_tbl (ln_rec_count).Org_Independent_Flag := 'Y';
         l_XRef_tbl (ln_rec_count).ATTRIBUTE_CATEGORY :=
            REF_XREF_NEW.ATTRIBUTE_CATEGORY;
         l_XRef_tbl (ln_rec_count).attribute1 := REF_XREF_NEW.ATTRIBUTE1;
         l_XRef_tbl (ln_rec_count).Transaction_Type := 'CREATE';

         UPDATE XXWC.XXWC_B2B_CAT_STG_TBL
            SET Error_message = 'UPC Picked'
          WHERE ROWID = REF_XREF_NEW.row_id;
      END LOOP;

      write_log (l_success_mess);

      g_sec := 'Calling Standard API - CREATE';
      write_log ('Checking Records count to Call Standard API');

      IF NVL (l_xref_tbl.COUNT, 0) > 0
      THEN
         l_success_mess := 'Standard API to CREATE Completed Successfully';
         l_error_mess := 'Standard API to CREATE Completed with Error';


         MTL_CROSS_REFERENCES_PUB.Process_XRef (
            p_api_version     => 1.0,
            p_init_msg_list   => l_init_msg_list,
            p_commit          => l_commit,
            p_XRef_Tbl        => l_XRef_tbl,
            x_return_status   => x_return_status,
            x_msg_count       => x_msg_count,
            x_message_list    => x_message_list);

         IF x_return_status != 'S'
         THEN
            write_log (l_error_mess);
            lvc_create := 'N';
            Error_Handler.GET_MESSAGE_LIST (x_message_list => x_message_list);

            FOR i IN 1 .. x_message_list.COUNT
            LOOP
               L_ERROR_MESSAGE :=
                  L_ERROR_MESSAGE || '|' || X_MESSAGE_LIST (I).MESSAGE_TEXT;
            END LOOP;

            X_UPC_ERR_MESS := L_ERROR_MESSAGE;
         ELSE
            write_log (l_success_mess);
            lvc_create := 'Y';
            X_UPC_ERR_MESS := NULL;
         END IF;
      END IF;

      write_log ('New Cross Rerences Process Completed');

      g_sec := 'Initializing API PLSQL table -  UPDATE';
      l_success_mess :=
         'Initializing PLSQL variables to UPDATE Completed Successfully';
      l_error_mess :=
         ' Initializing PLSQL variables to UPDATE Completed with Error';

      l_xref_tbl.DELETE;
      fnd_msg_pub.delete_msg;
      l_init_msg_list := FND_API.G_TRUE;
      l_commit := FND_API.G_TRUE;
      x_return_status := NULL;
      x_msg_count := 0;
      ln_rec_count := 0;

      -- Cross References Updation
      FOR REC_XREF_UPDATE IN CUR_XREF_UPDATE (P_ITEM_NUMBER)
      LOOP
         IF REC_XREF_UPDATE.MCR_VENDOR_NUM = REC_XREF_UPDATE.XCST_VENDOR_NUM
         THEN
            CONTINUE;
         END IF;

         ln_rec_count := ln_rec_count + 1;
         l_XRef_tbl (ln_rec_count).attribute1 :=
            REC_XREF_UPDATE.XCST_VENDOR_NUM;
         l_XRef_tbl (ln_rec_count).Transaction_Type := 'UPDATE';
         l_XRef_tbl (ln_rec_count).cross_reference_id :=
            REC_XREF_UPDATE.CROSS_REFERENCE_ID;

         UPDATE XXWC.XXWC_B2B_CAT_STG_TBL
            SET Error_message = 'VENDOR Picked'
          WHERE ROWID = REC_XREF_UPDATE.row_id;
      END LOOP;

      IF NVL (l_xref_tbl.COUNT, 0) > 0
      THEN
         MTL_CROSS_REFERENCES_PUB.Process_XRef (
            p_api_version     => 1.0,
            p_init_msg_list   => l_init_msg_list,
            p_commit          => l_commit,
            p_XRef_Tbl        => l_XRef_tbl,
            x_return_status   => x_return_status,
            x_msg_count       => x_msg_count,
            x_message_list    => x_message_list);

         IF X_RETURN_STATUS != 'S'
         THEN
            lvc_update := 'N';
            write_log (l_error_mess);
            Error_Handler.GET_MESSAGE_LIST (x_message_list => x_message_list);

            FOR i IN 1 .. x_message_list.COUNT
            LOOP
               L_ERROR_MESSAGE :=
                  L_ERROR_MESSAGE || '|' || X_MESSAGE_LIST (I).MESSAGE_TEXT;
            END LOOP;

            X_VEN_ERR_MESS := L_ERROR_MESSAGE;
         ELSE
            lvc_update := 'Y';
            write_log (l_success_mess);
         END IF;
      END IF;

      fnd_file.put_line (
         fnd_File.LOG,
         'lvc_create ' || lvc_create || ' lvc_update ' || lvc_update);

      write_log ('Update Cross Rerences Process Completed');

      IF    (lvc_create = 'Y' AND lvc_update = 'Y')
         OR (lvc_create = 'X' AND lvc_update = 'X')
         OR (lvc_create = 'Y' AND lvc_update = 'X')
         OR (lvc_create = 'X' AND lvc_update = 'Y')
      THEN
         lvc_return_flag := 'Y';
         X_VEN_ERR_MESS := NULL;
         X_UPC_ERR_MESS := NULL;
      ELSIF lvc_create = 'N' OR lvc_update = 'N'
      THEN
         lvc_return_flag := 'N';
      END IF;

      fnd_file.put_line (fnd_File.LOG, 'lvc_return_flag ' || lvc_return_flag);
      RETURN lvc_return_flag;
   EXCEPTION
      WHEN OTHERS
      THEN
         write_log ('Error Occured ' || SUBSTR (SQLERRM, 1, 200));
         write_log (
            'Error Flag Create: ' || lvc_create || ' Update ' || lvc_update);
         lvc_return_flag := 'N';
         X_VEN_ERR_MESS := 'Error Occured ' || SUBSTR (SQLERRM, 1, 200);
         X_UPC_ERR_MESS := 'Error Occured ' || SUBSTR (SQLERRM, 1, 200);
         RETURN lvc_return_flag;
   END;


   FUNCTION MASTER_DATA_VALIDATION (P_ITEM_NUMBER IN VARCHAR2)
      RETURN VARCHAR2
   /*********************************************************************************************************
     $Header MASTER_DATA_VALIDATION $
     PURPOSE:     Procedure to validate master data
     REVISIONS:
     Ver    Date         Author                Description
     ------ -----------  ------------------    -------------------------------------------------------------
      1.2      19-Aug-2016   P.Vamshidhar         TMS#20160603-00067 - Kiewit Phase 2 - Upload Data to PDH
     ********************************************************************************************************/
   IS
      -- Cursors Declaration
      CURSOR CUR_ITEM (
         P_ITEM_NUMBER1    VARCHAR2)
      IS
         SELECT XBST.ROWID ROW_ID,
                TRIM (XBST.ATTR_DISPLAY_NAME) ATTR_DISPLAY_NAME,
                XBST.ATTRIBUTE_VALUE ATTRIBUTE_VALUE,
                FFV.FLEX_VALUE
           FROM XXWC.XXWC_B2B_CAT_STG_TBL XBST,
                APPS.FND_FLEX_VALUE_SETS FFVS,
                APPS.FND_FLEX_VALUES_VL FFV
          WHERE     XBST.ITEM_NUMBER = P_ITEM_NUMBER1
                AND XBST.ICC = 'MASTER'
                AND XBST.ATTR_GROUP_DISP_NAME = 'ATTRIBUTE'
                AND (   XBST.ATTR_DISPLAY_NAME NOT LIKE 'UPC%'
                     OR UPPER (XBST.ATTR_DISPLAY_NAME) NOT LIKE 'VENDOR%')
                AND FFVS.FLEX_VALUE_SET_NAME = 'XXWC_MASTER_LEVEL_ATTR_VS'
                AND FFVS.FLEX_VALUE_SET_ID = FFV.FLEX_VALUE_SET_ID
                AND FFV.ENABLED_FLAG = 'Y'
                AND UPPER (TRIM (FFV.description)) =
                       UPPER (TRIM (XBST.ATTR_DISPLAY_NAME));

      -- Variables Declaration
      l_err_msg                     VARCHAR2 (4000);
      ln_error_count                NUMBER := 0;
      EXECUTION_ERROR               EXCEPTION;

      -- General
      ln_inventory_item_id          MTL_SYSTEM_ITEMS_B.INVENTORY_ITEM_ID%TYPE;
      lvc_dimension_uom_code        MTL_SYSTEM_ITEMS_B.DIMENSION_UOM_CODE%TYPE;
      ln_unit_length                MTL_SYSTEM_ITEMS_B.UNIT_LENGTH%TYPE;
      ln_unit_width                 MTL_SYSTEM_ITEMS_B.UNIT_WIDTH%TYPE;
      ln_unit_height                MTL_SYSTEM_ITEMS_B.UNIT_HEIGHT%TYPE;
      lvc_description               MTL_SYSTEM_ITEMS_B.DESCRIPTION%TYPE;
      ln_shelf_life_days            MTL_SYSTEM_ITEMS_B.SHELF_LIFE_DAYS%TYPE;
      ln_fixed_lot_multiplier       MTL_SYSTEM_ITEMS_B.FIXED_LOT_MULTIPLIER%TYPE;
      lvc_attribute22               MTL_SYSTEM_ITEMS_B.ATTRIBUTE22%TYPE;
      lvc_hazardous_material_flag   MTL_SYSTEM_ITEMS_B.HAZARDOUS_MATERIAL_FLAG%TYPE;
      ln_unit_weight                MTL_SYSTEM_ITEMS_B.UNIT_WEIGHT%TYPE;
      lvc_uomcode                   VARCHAR2 (40);
      ln_valid_number               NUMBER;
      ln_vs_count                   NUMBER;
      lvc_return_flag               VARCHAR2 (1);
   BEGIN
      g_procedure := 'MASTER_DATA_UPDATE';
      g_sec := 'Retriving Item Data from Master Tables';

      BEGIN
         SELECT INVENTORY_ITEM_ID,
                DIMENSION_UOM_CODE,
                UNIT_LENGTH,
                UNIT_WIDTH,
                UNIT_HEIGHT,
                DESCRIPTION,
                SHELF_LIFE_DAYS,
                FIXED_LOT_MULTIPLIER,
                ATTRIBUTE22,
                HAZARDOUS_MATERIAL_FLAG,
                UNIT_WEIGHT
           INTO ln_inventory_item_id,
                lvc_dimension_uom_code,
                ln_unit_length,
                ln_unit_width,
                ln_unit_height,
                lvc_description,
                ln_shelf_life_days,
                ln_fixed_lot_multiplier,
                lvc_attribute22,
                lvc_hazardous_material_flag,
                ln_unit_weight
           FROM APPS.MTL_SYSTEM_ITEMS_B
          WHERE SEGMENT1 = P_ITEM_NUMBER AND ORGANIZATION_ID = 222;
      EXCEPTION
         WHEN NO_DATA_FOUND
         THEN
            l_err_msg :=
                  ' ...Error_Stack...'
               || DBMS_UTILITY.format_error_stack ()
               || ' Error_Backtrace...'
               || DBMS_UTILITY.format_error_backtrace ();
            write_log (l_err_msg);
            RAISE EXECUTION_ERROR;
         WHEN OTHERS
         THEN
            l_err_msg :=
                  ' ...Error_Stack...'
               || DBMS_UTILITY.format_error_stack ()
               || ' Error_Backtrace...'
               || DBMS_UTILITY.format_error_backtrace ();
            write_log (l_err_msg);
            RAISE EXECUTION_ERROR;
      END;

      g_sec := 'Validating Item Master Data  - Start';

      FOR REC_ITEM IN CUR_ITEM (P_ITEM_NUMBER)
      LOOP
         write_log (
               REC_ITEM.ATTR_DISPLAY_NAME
            || ' '
            || REC_ITEM.ATTRIBUTE_VALUE
            || ' Validation Start');

         -- Hazardous Material flag validation
         IF REC_ITEM.ATTR_DISPLAY_NAME = 'Hazardous Material'
         THEN
            IF NVL (UPPER (SUBSTR (REC_ITEM.ATTRIBUTE_VALUE, 1, 1)), 'A') NOT IN ('Y',
                                                                                  'N')
            THEN
               UPDATE XXWC.XXWC_B2B_CAT_STG_TBL XBST
                  SET error_message = 'Value should be Y or N',
                      process_status = 'Error'
                WHERE ROWID = REC_ITEM.row_id;

               ln_error_count := ln_error_count + 1;

               CONTINUE;
            END IF;
         END IF;


         IF REC_ITEM.ATTR_DISPLAY_NAME IN ('Unit Length',
                                           'Unit Width',
                                           'Unit Height',
                                           'Shipping Weight',
                                           'Fixed Lot Multiplier',
                                           'Shelf Life Days')
         THEN
            IF REC_ITEM.ATTRIBUTE_VALUE IS NOT NULL
            THEN
               BEGIN
                  SELECT 1
                    INTO ln_valid_number
                    FROM DUAL
                   WHERE (   REGEXP_LIKE (
                                NVL (REC_ITEM.ATTRIBUTE_VALUE, 0.00),
                                '^[0-9]+(\.([0-9]{1,20})?)?$')
                          OR REGEXP_LIKE (
                                NVL (REC_ITEM.ATTRIBUTE_VALUE, 0.00),
                                '^+(.([0-9]{1,20})?)?$'));
               EXCEPTION
                  WHEN NO_DATA_FOUND
                  THEN
                     ln_valid_number := 0;
                  WHEN OTHERS
                  THEN
                     ln_valid_number := 0;
               END;

               IF ln_valid_number = 0
               THEN
                  UPDATE XXWC.XXWC_B2B_CAT_STG_TBL
                     SET ERROR_MESSAGE =
                               '"'
                            || REC_ITEM.ATTR_DISPLAY_NAME
                            || '"'
                            || ' Needs to have number values only ',
                         PROCESS_STATUS = 'Error'
                   WHERE ROWID = REC_ITEM.row_id;

                  ln_error_count := ln_error_count + 1;
                  CONTINUE;
               END IF;
            END IF;
         END IF;

         IF     REC_ITEM.ATTR_DISPLAY_NAME = 'Taxware Code'
            AND REC_ITEM.ATTRIBUTE_VALUE <> lvc_attribute22
         THEN
            BEGIN
               ln_vs_count := 0;

               SELECT COUNT (1)
                 INTO ln_vs_count
                 FROM apps.fnd_flex_value_sets ffvs,
                      apps.fnd_flex_values_vl ffv
                WHERE     ffvs.flex_value_set_id = ffv.flex_value_set_id
                      AND ffvs.flex_value_set_name = 'XXWC TAXWARE CODE'
                      AND FFV.ENABLED_FLAG = 'Y'
                      AND FFV.FLEX_VALUE = REC_ITEM.ATTRIBUTE_VALUE;
            EXCEPTION
               WHEN OTHERS
               THEN
                  ln_vs_count := 0;
            END;


            IF ln_vs_count = 0
            THEN
               UPDATE XXWC.XXWC_B2B_CAT_STG_TBL XBST
                  SET error_message =
                            REC_ITEM.ATTRIBUTE_VALUE
                         || ' Value not listed Taxware codes list',
                      process_status = 'Error'
                WHERE ROWID = REC_ITEM.row_id;

               ln_error_count := ln_error_count + 1;
            END IF;
         ELSIF REC_ITEM.ATTR_DISPLAY_NAME = 'Dimension UOM'
         THEN
            BEGIN
               lvc_uomcode := NULL;

               SELECT UOM_CODE
                 INTO lvc_uomcode
                 FROM MTL_UNITS_OF_MEASURE
                WHERE UNIT_OF_MEASURE = REC_ITEM.ATTRIBUTE_VALUE;
            EXCEPTION
               WHEN OTHERS
               THEN
                  lvc_uomcode := NULL;
            END;

            IF lvc_uomcode IS NULL
            THEN
               UPDATE XXWC.XXWC_B2B_CAT_STG_TBL XBST
                  SET error_message =
                            REC_ITEM.ATTRIBUTE_VALUE
                         || ' Value not listed Unit of Measurements list',
                      process_status = 'Error'
                WHERE ROWID = REC_ITEM.row_id;

               ln_error_count := ln_error_count + 1;
            END IF;
         END IF;

         write_log (REC_ITEM.ATTR_DISPLAY_NAME || ' Validation Done');
      END LOOP;

      write_log ('Item Master Validation Completed');

      IF NVL (ln_error_count, 0) > 0
      THEN
         lvc_return_flag := 'N';
      ELSE
         lvc_return_flag := 'Y';
      END IF;

      RETURN lvc_return_flag;
   EXCEPTION
      WHEN EXECUTION_ERROR
      THEN
         l_err_msg :=
               l_err_msg
            || ' ...Error_Stack...'
            || DBMS_UTILITY.format_error_stack ()
            || ' Error_Backtrace...'
            || DBMS_UTILITY.format_error_backtrace ();

         -- Calling ERROR API
         xxcus_error_pkg.xxcus_error_main_api (
            p_called_from         => g_err_callfrom || '.' || g_procedure,
            p_ora_error_msg       => SUBSTR (l_err_msg, 1, 2000),
            p_error_desc          => SUBSTR (l_err_msg, 1, 240),
            p_distribution_list   => g_distro_list,
            p_module              => 'INV');

         write_log (l_err_msg);

         RETURN 'N';
      WHEN OTHERS
      THEN
         l_err_msg :=
               l_err_msg
            || ' ...Error_Stack...'
            || DBMS_UTILITY.format_error_stack ()
            || ' Error_Backtrace...'
            || DBMS_UTILITY.format_error_backtrace ();

         -- Calling ERROR API
         xxcus_error_pkg.xxcus_error_main_api (
            p_called_from         => g_err_callfrom || '.' || g_procedure,
            p_ora_error_msg       => SUBSTR (l_err_msg, 1, 2000),
            p_error_desc          => SUBSTR (l_err_msg, 1, 240),
            p_distribution_list   => g_distro_list,
            p_module              => 'INV');
         write_log (l_err_msg);
         RETURN 'N';
   END;


   FUNCTION PDH_DATA_VALIDATION (P_ITEM_NUMBER IN VARCHAR2)
      RETURN VARCHAR2
   /*********************************************************************************************************
     $Header PDH_DATA_VALIDATION $
     PURPOSE:     Procedure to validate PDH Data
     REVISIONS:
     Ver    Date         Author                Description
     ------ -----------  ------------------    -------------------------------------------------------------
      1.2      19-Aug-2016   P.Vamshidhar      TMS#20160603-00067 - Kiewit Phase 2 - Upload Data to PDH
      1.4      30-Sep-2016   P.Vamshidhar      TMS#20160930-00032 - Kiewit Phase 2 - Features and Benefits issue fix.
     ********************************************************************************************************/
   IS
      CURSOR CUR_STG_ITEM_GROUP (
         CP_ITEM_NUMBER    VARCHAR2)
      IS
           SELECT XCST.ITEM_NUMBER,
                  EAV.APPLICATION_ID,
                  EAGV.ATTR_GROUP_ID,
                  EAGV.ATTR_GROUP_NAME,
                  EAGV.ATTR_GROUP_TYPE
             FROM APPS.EGO_ATTR_GROUPS_V EAGV,
                  APPS.EGO_ATTRS_V EAV,
                  XXWC.XXWC_B2B_CAT_STG_TBL XCST
            WHERE     EAV.ATTR_GROUP_NAME = EAGV.ATTR_GROUP_NAME
                  AND XCST.ATTR_GROUP_DISP_NAME = EAGV.ATTR_GROUP_DISP_NAME
                  AND EAV.ATTR_DISPLAY_NAME = XCST.ATTR_DISPLAY_NAME
                  AND XCST.ITEM_NUMBER = CP_ITEM_NUMBER
                  AND XCST.PROCESS_STATUS IN ('New')
         GROUP BY XCST.ITEM_NUMBER,
                  EAV.APPLICATION_ID,
                  EAGV.ATTR_GROUP_ID,
                  EAGV.ATTR_GROUP_NAME,
                  EAGV.ATTR_GROUP_TYPE;

      CURSOR CUR_STG_ITEM_GROUP_ATTR (
         CP_ITEM_NUMBER    VARCHAR2,
         CP_GROUP_NAME     VARCHAR2)
      IS
           SELECT XCST.ITEM_NUMBER,
                  EAV.APPLICATION_ID,
                  EAGV.ATTR_GROUP_ID,
                  EAGV.ATTR_GROUP_NAME,
                  EAGV.ATTR_GROUP_TYPE,
                  EAV.DATABASE_COLUMN,
                  EAV.DATA_TYPE_CODE,
                  EAV.ATTR_NAME,
                  XCST.ATTRIBUTE_VALUE ATTRIBUTE_VALUE,
                  XCST.ROWID ROW_ID,
                  EAV.VALUE_SET_ID,
                  EAV.VALUE_SET_NAME,
                  XCST.ATTR_DISPLAY_NAME,
                  EAV.MAXIMUM_SIZE                --Added by Vamshi in Rev 1.4
             FROM APPS.EGO_ATTR_GROUPS_V EAGV,
                  APPS.EGO_ATTRS_V EAV,
                  XXWC.XXWC_B2B_CAT_STG_TBL XCST
            WHERE     EAV.ATTR_GROUP_NAME = EAGV.ATTR_GROUP_NAME
                  AND XCST.ATTR_GROUP_DISP_NAME = EAGV.ATTR_GROUP_DISP_NAME
                  AND EAV.ATTR_DISPLAY_NAME = XCST.ATTR_DISPLAY_NAME
                  AND XCST.ITEM_NUMBER = CP_ITEM_NUMBER
                  AND EAGV.ATTR_GROUP_NAME = CP_GROUP_NAME
                  AND EAV.ENABLED_FLAG = 'Y'
                  AND XCST.PROCESS_STATUS IN ('New')
                  AND (    XCST.ATTR_DISPLAY_NAME NOT LIKE 'Thumbnail Image%'
                       AND XCST.ATTR_DISPLAY_NAME NOT LIKE 'Fullsize Image%')
         ORDER BY EAV.ATTR_GROUP_NAME;

      -- Variables Declaration
      lvc_attr_sql           VARCHAR2 (32000);
      ln_inventory_item_id   MTL_SYSTEM_ITEMS_B.INVENTORY_ITEM_ID%TYPE;
      l_attribute_value      VARCHAR2 (4000);
      lvc_validate_vs        VARCHAR2 (10);
      l_err_msg              VARCHAR2 (4000);
      ln_error_count         NUMBER := 0;
      ln_datatype_flag       NUMBER;
      ld_attr_date           DATE;
      lvc_return_flag        VARCHAR2 (1);
      l_org_id               NUMBER := 222;
   BEGIN
      g_procedure := 'PDH_DATA_VALIDATION';

      SELECT inventory_item_id
        INTO ln_inventory_item_id
        FROM MTL_SYSTEM_ITEMS_B
       WHERE SEGMENT1 = P_ITEM_NUMBER AND ORGANIZATION_ID = l_org_id;

      g_sec := 'PDH Attribute Groups Process start';

      write_log (g_sec);

      FOR REC_STG_ITEM_GROUP IN CUR_STG_ITEM_GROUP (P_ITEM_NUMBER)
      LOOP
         g_sec := 'PDH Group Attribute Values Process';

         write_log ('Group Name ' || rec_stg_item_group.attr_group_name);

         FOR REC_STG_ITEM_GROUP_ATTR
            IN CUR_STG_ITEM_GROUP_ATTR (REC_STG_ITEM_GROUP.ITEM_NUMBER,
                                        REC_STG_ITEM_GROUP.ATTR_GROUP_NAME)
         LOOP
            write_log (
                  'Attribute Name '
               || REC_STG_ITEM_GROUP_ATTR.ATTR_DISPLAY_NAME
               || ' Attribute Value '
               || REC_STG_ITEM_GROUP_ATTR.ATTRIBUTE_VALUE);

            -- Variables declaration
            lvc_attr_sql := NULL;
            l_attribute_value := NULL;
            l_err_msg := NULL;
            lvc_validate_vs := NULL;
            ln_datatype_flag := NULL;
            ld_attr_date := NULL;
            lvc_attr_sql :=
                  'SELECT '
               || REC_STG_ITEM_GROUP_ATTR.DATABASE_COLUMN
               || ' FROM APPS.EGO_MTL_SY_ITEMS_EXT_B WHERE ORGANIZATION_ID=222 AND INVENTORY_ITEM_ID= :1 AND ATTR_GROUP_ID = :2';

            g_sec := 'PDH Group Attribute Values Query';

            BEGIN
               EXECUTE IMMEDIATE lvc_attr_sql
                  INTO l_attribute_value
                  USING ln_inventory_item_id,
                        REC_STG_ITEM_GROUP_ATTR.attr_group_id;
            EXCEPTION
               WHEN OTHERS
               THEN
                  l_attribute_value := NULL;
            END;

            IF NVL (TO_CHAR (l_attribute_value), 'ABCX') =
                  NVL (TO_CHAR (REC_STG_ITEM_GROUP_ATTR.ATTRIBUTE_VALUE),
                       'ABCX')
            THEN
               CONTINUE;
            END IF;

            g_sec := 'PDH Group Attribute Values Value set Validation';

            IF     REC_STG_ITEM_GROUP_ATTR.VALUE_SET_ID IS NOT NULL
               AND REC_STG_ITEM_GROUP_ATTR.ATTRIBUTE_VALUE IS NOT NULL
            THEN
               lvc_validate_vs := NULL;

               lvc_validate_vs :=
                  Validate_value_set (
                     REC_STG_ITEM_GROUP_ATTR.value_set_id,
                     REC_STG_ITEM_GROUP_ATTR.ATTRIBUTE_VALUE);

               IF NVL (lvc_validate_vs, 'N') = 'N'
               THEN
                  l_err_msg :=
                        REC_STG_ITEM_GROUP_ATTR.ATTRIBUTE_VALUE
                     || ' Value not listed in Valueset in '
                     || REC_STG_ITEM_GROUP_ATTR.VALUE_SET_NAME;

                  UPDATE XXWC.XXWC_B2B_CAT_STG_TBL
                     SET ERROR_MESSAGE = l_err_msg, PROCESS_STATUS = 'Error'
                   WHERE ROWID = REC_STG_ITEM_GROUP_ATTR.row_id;

                  ln_error_count := ln_error_count + 1;
                  CONTINUE;
               END IF;
            END IF;

            g_sec := 'PDH Group Attribute Values datatype validation';

            IF REC_STG_ITEM_GROUP_ATTR.DATA_TYPE_CODE = 'N'
            THEN
               g_sec :=
                  'Validating Attribute Value Attribute Data Type Is Number';

               BEGIN
                  ln_datatype_flag := 0;

                  SELECT 1
                    INTO ln_datatype_flag
                    FROM DUAL
                   WHERE (   REGEXP_LIKE (
                                NVL (REC_STG_ITEM_GROUP_ATTR.ATTRIBUTE_VALUE,
                                     0),
                                '^[0-9]+(\.([0-9]{1,20})?)?$')
                          OR REGEXP_LIKE (
                                NVL (REC_STG_ITEM_GROUP_ATTR.ATTRIBUTE_VALUE,
                                     0),
                                '^+(.([0-9]{1,20})?)?$'));
               EXCEPTION
                  WHEN OTHERS
                  THEN
                     ln_datatype_flag := 0;
               END;

               IF NVL (ln_datatype_flag, 0) = 0
               THEN
                  l_err_msg :=
                        '"'
                     || REC_STG_ITEM_GROUP_ATTR.ATTRIBUTE_VALUE -- Changed in Rev 1.4
                     || '"'
                     || ' is an Invalid Value. Please enter numeric values only.';


                  UPDATE XXWC.XXWC_B2B_CAT_STG_TBL
                     SET ERROR_MESSAGE = l_err_msg, PROCESS_STATUS = 'Error'
                   WHERE ROWID = REC_STG_ITEM_GROUP_ATTR.row_id;

                  ln_error_count := ln_error_count + 1;
                  CONTINUE;
               END IF;
            ELSIF REC_STG_ITEM_GROUP_ATTR.DATA_TYPE_CODE = 'X'
            THEN
               g_sec :=
                  'Validating Attribute Value Attribute Data Type Is Date';

               IF REC_STG_ITEM_GROUP_ATTR.ATTRIBUTE_VALUE IS NOT NULL
               THEN
                  ld_attr_date :=
                     validate_date (REC_STG_ITEM_GROUP_ATTR.ATTRIBUTE_VALUE);

                  fnd_file.put_line (fnd_file.LOG,
                                     'ld_attr_date --> ' || ld_attr_date);

                  IF ld_attr_date IS NULL
                  THEN
                     l_err_msg :=
                           REC_STG_ITEM_GROUP_ATTR.ATTR_DISPLAY_NAME
                        || ' is an invalid value. Please use date format DD-MON-YYYY';

                     ln_error_count := ln_error_count + 1;

                     UPDATE XXWC.XXWC_B2B_CAT_STG_TBL
                        SET ERROR_MESSAGE = l_err_msg,
                            PROCESS_STATUS = 'Error'
                      WHERE ROWID = REC_STG_ITEM_GROUP_ATTR.row_id;

                     CONTINUE;
                  END IF;
               END IF;
            -- Added below code in Rev 1.4 - Begin
            ELSIF REC_STG_ITEM_GROUP_ATTR.DATA_TYPE_CODE IN ('C', 'A')
            THEN
               IF    (    LENGTH (REC_STG_ITEM_GROUP_ATTR.ATTRIBUTE_VALUE) >
                             NVL (REC_STG_ITEM_GROUP_ATTR.MAXIMUM_SIZE, 150)
                      AND REC_STG_ITEM_GROUP_ATTR.DATABASE_COLUMN LIKE
                             'C_EXT_A%')
                  OR (    LENGTH (REC_STG_ITEM_GROUP_ATTR.ATTRIBUTE_VALUE) >
                             NVL (REC_STG_ITEM_GROUP_ATTR.MAXIMUM_SIZE, 1000)
                      AND REC_STG_ITEM_GROUP_ATTR.DATABASE_COLUMN LIKE
                             'TL_EXT_A%')
               THEN
                  SELECT DECODE (
                            SUBSTR (REC_STG_ITEM_GROUP_ATTR.DATABASE_COLUMN,
                                    1,
                                    5),
                            'C_EXT', 'Attribute value length is greater that maximum length 150',
                            'TL_EX', 'Attribute value length is greater that maximum length 1000',
                            NULL)
                    INTO l_err_msg
                    FROM DUAL;

                  UPDATE XXWC.XXWC_B2B_CAT_STG_TBL
                     SET ERROR_MESSAGE = l_err_msg, PROCESS_STATUS = 'Error'
                   WHERE ROWID = REC_STG_ITEM_GROUP_ATTR.row_id;

                  ln_error_count := ln_error_count + 1;
                  CONTINUE;
               END IF;
            -- Added above code in Rev 1.4 - End
            END IF;
         END LOOP;
      END LOOP;

      g_sec := ' PDH Validation Completed';

      write_log (g_sec);

      IF NVL (ln_error_count, 0) > 0
      THEN
         lvc_return_flag := 'N';
      ELSE
         lvc_return_flag := 'Y';
      END IF;

      COMMIT;

      RETURN lvc_return_flag;
   EXCEPTION
      WHEN OTHERS
      THEN
         lvc_return_flag := 'N';
         write_log (SUBSTR (SQLERRM, 1, 200));
         RETURN lvc_return_flag;
   END;

   PROCEDURE MTL_UPC_VALIDATION
   /*******************************************************************************************************************************
       PROCEDURE : MTL_UPC_VALIDATION

       REVISIONS:
      Ver        Date        Author                     Description
     ---------  ----------  ---------------    ----------------------------------------------------------------------------------
    -- 1.5      01-Jun-2017 P.Vamshidhar       TMS#20170421-00199 - UPC Validation XXWC B2B Customer Catalog
    *********************************************************************************************************************************/
   IS
      lvc_return_status   BOOLEAN;
      l_procedure         VARCHAR2 (100) := 'MTL_UPC_VALIDATION';
      l_sec               VARCHAR2 (300);
      lvc_item_number     VARCHAR2 (30);
      lvc_upc_exist       VARCHAR2 (1);
      x_ret_mess          VARCHAR2 (1000);
      l_err_msg           VARCHAR2 (1000);

      CURSOR CUR_UPC
      IS
         SELECT ROWID row_id, attribute_value, item_number
           FROM XXWC.XXWC_B2B_CAT_STG_TBL XCST
          WHERE     1 = 1
                AND ATTR_DISPLAY_NAME LIKE 'UPC%'
                AND ATTR_DISPLAY_NAME NOT LIKE 'UPC%Owner%'
                AND PROCESS_STATUS <> 'Error';
   BEGIN
      l_sec := ' Procedure Start - UPC Cross Reference ';

      FOR REC_UPC IN CUR_UPC
      LOOP
         X_RET_MESS := NULL;
         lvc_return_status := NULL;

         IF    REC_UPC.attribute_value IS NULL
            OR REC_UPC.attribute_value = 'UPCEXEMPT'
         THEN
            CONTINUE;
         ELSE
            lvc_return_status :=
               APPS.xxwc_pim_chg_item_mgmt.upc_validation (
                  REC_UPC.attribute_value);

            IF (lvc_return_status)
            THEN
               l_sec :=
                     ' Deriving Item number for Cross Reference '
                  || REC_UPC.attribute_value;

               BEGIN
                  SELECT B.SEGMENT1
                    INTO lvc_item_number
                    FROM APPS.MTL_CROSS_REFERENCES A,
                         APPS.MTL_SYSTEM_ITEMS_B B
                   WHERE     A.INVENTORY_ITEM_ID = B.INVENTORY_ITEM_ID
                         AND B.ORGANIZATION_ID = 222
                         AND B.SEGMENT1 = REC_UPC.ITEM_NUMBER
                         AND A.CROSS_REFERENCE_TYPE IN ('UPC',
                                                        'EAN',
                                                        'PENDING',
                                                        'GTIN')
                         AND A.CROSS_REFERENCE = REC_UPC.attribute_value
                         AND ROWNUM = 1;
               EXCEPTION
                  WHEN OTHERS
                  THEN
                     lvc_item_number := NULL;
               END;

               IF REC_UPC.ITEM_NUMBER = NVL (lvc_item_number, 'ABCXYZ')
               THEN
                  CONTINUE;
               ELSE
                  BEGIN
                     SELECT B.SEGMENT1
                       INTO lvc_item_number
                       FROM APPS.MTL_CROSS_REFERENCES A,
                            APPS.MTL_SYSTEM_ITEMS_B B
                      WHERE     A.INVENTORY_ITEM_ID = B.INVENTORY_ITEM_ID
                            AND B.ORGANIZATION_ID = 222
                            AND A.CROSS_REFERENCE_TYPE IN ('UPC',
                                                           'EAN',
                                                           'PENDING',
                                                           'GTIN')
                            AND A.CROSS_REFERENCE = REC_UPC.attribute_value
                            AND ROWNUM = 1;
                  EXCEPTION
                     WHEN OTHERS
                     THEN
                        lvc_item_number := NULL;
                  END;
               END IF;

               IF lvc_item_number IS NOT NULL
               THEN
                  X_RET_MESS :=
                        'UPC'
                     || REC_UPC.attribute_value
                     || ' is associated to item '
                     || lvc_item_number;
               ELSE
                  CONTINUE;
               END IF;
            ELSE
               X_RET_MESS :=
                     'The UPC value '
                  || REC_UPC.attribute_value
                  || ' is invalid.';
            END IF;
         END IF;

         IF x_ret_mess IS NOT NULL
         THEN
            UPDATE XXWC.XXWC_B2B_CAT_STG_TBL XCST
               SET ERROR_MESSAGE = x_ret_mess, PROCESS_STATUS = 'Error'
             WHERE ROWID = REC_UPC.ROW_ID;
         END IF;
      END LOOP;
   EXCEPTION
      WHEN OTHERS
      THEN
         -- Calling ERROR API
         l_err_msg := SUBSTR (SQLERRM, 1, 250);
         xxcus_error_pkg.xxcus_error_main_api (
            p_called_from         => g_err_callfrom || '.' || l_procedure,
            p_ora_error_msg       => SUBSTR (l_err_msg, 1, 2000),
            p_error_desc          => SUBSTR (l_err_msg, 1, 240),
            p_distribution_list   => g_distro_list,
            p_module              => 'INV');
         write_log (l_err_msg);
   END;
END XXWC_EGO_CUSTOMER_CATALOG_PKG;
/