/*************************************************************************
Copyright (c) 2012 HD Supply
All rights reserved.
**************************************************************************
   $Header "XXWC"."XXWC_APPLY_EXT_TBL" $
  Module Name: grants on "XXWC"."XXWC_APPLY_EXT_TBL" 
  PURPOSE:

  REVISIONS:
  -- VERSION DATE          AUTHOR(S)   TMS TASK        DESCRIPTION
  -- ------- -----------   ------------ ---------      -------------------------
  -- 1.0     26-APR-2018  Nancy Pahwa  20160218-00198  Initially Created
**************************************************************************/
GRANT SELECT ON "XXWC"."XXWC_APPLY_EXT_TBL" TO "EA_APEX";
GRANT ALTER ON "XXWC"."XXWC_APPLY_EXT_TBL" TO "EA_APEX";