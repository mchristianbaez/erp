CREATE TABLE "XXWC"."XXWC_WRITEOFF_RCPT_EXT_TBL" 
/*************************************************************************
Copyright (c) 2012 HD Supply
All rights reserved.
**************************************************************************
   $Header XXWC_WRITEOFF_RCPT_EXT_TBL$
  Module Name: XXWC_WRITEOFF_RCPT_EXT_TBL
  PURPOSE:

  REVISIONS:
  -- VERSION DATE          AUTHOR(S)   TMS TASK        DESCRIPTION
  -- ------- -----------   ------------ ---------      -------------------------
  -- 1.0     26-APR-2018  Nancy Pahwa  20160218-00198  Initially Created
**************************************************************************/
   (	"CUSTOMER_NUMBER" VARCHAR2(30), 
	"RECEIPT_NUMBER" VARCHAR2(30), 
	"RECEIPT_DATE" DATE, 
	"RECEIPT_AMOUNT" NUMBER, 
	"ACTIVITY_NAME" VARCHAR2(50), 
	"WRITEOFF_AMOUNT" NUMBER, 
	"COMMENTS" VARCHAR2(2000), 
	"STATUS" VARCHAR2(100)
   ) 
   ORGANIZATION EXTERNAL 
    ( TYPE ORACLE_LOADER
      DEFAULT DIRECTORY "XXWC_PDH_ITEM_LOAD_DIR"
      ACCESS PARAMETERS
      ( RECORDS DELIMITED BY NEWLINE CHARACTERSET WE8MSWIN1252
    SKIP 1
    BADFILE 'writeoffLoadBad.bad'
    DISCARDFILE 'writeoffLoadDiscard.dsc'
    FIELDS TERMINATED BY ','
    OPTIONALLY ENCLOSED BY '"' AND '"'
    REJECT ROWS WITH ALL NULL FIELDS
      )
      LOCATION
       ( 'XXWC_Writeoff_Data_Load.csv'
       )
    )
   REJECT LIMIT UNLIMITED ;
