CREATE OR REPLACE PACKAGE XXWC_AR_RCPT_TRANSACTION_PKG AUTHID CURRENT_USER AS
/*************************************************************************
   *   $Header xxwc_ar_rcpt_transaction_pkg.pks $
   *   Module Name: xxwc_ar_rcpt_transaction_pkg
   *
   *   PURPOSE:   Used in receipt mass write off
   *
   *   REVISIONS:
   *   Ver        Date        Author                     Description
   *   ---------  ----------  ---------------         -------------------------
   *  1.0        04/24/2018   Niraj K Ranjan          Initial Version
   * ***************************************************************************/
   
 /*************************************************************************
    *   PROCEDURE Name: receipt_writeoff
    *
    *   PURPOSE:   AR Receipt write off in mass
	
	HISTORY
    ===============================================================================
    VERSION DATE          AUTHOR(S)              DESCRIPTION
    ------- -----------   ---------------  -----------------------------------------
    1.0     04/24/2018    Niraj K Ranjan   TMS#20160218-00198   AR APEX project - Ability to apply payments in mass
   *****************************************************************************/
   PROCEDURE receipt_writeoff(errbuf              OUT VARCHAR2,
                              retcode             OUT NUMBER,
							  p_batch_id          IN  NUMBER,
							  p_rec_status        IN  VARCHAR2);
							  
   /*************************************************************************
    *   PROCEDURE Name: receipt_reapply
    *
    *   PURPOSE:   AR Receipt apply/unapply/both in mass
	
	HISTORY
    ===============================================================================
    VERSION DATE          AUTHOR(S)              DESCRIPTION
    ------- -----------   ---------------  -----------------------------------------
    1.0     04/24/2018    Niraj K Ranjan   TMS#20160218-00198   AR APEX project - Ability to apply payments in mass
   *****************************************************************************/
   PROCEDURE receipt_reapply(errbuf              OUT VARCHAR2,
                             retcode             OUT NUMBER,
							 p_batch_id          IN  NUMBER,
							 p_rec_status        IN  VARCHAR2);	
							 
   /*************************************************************************
    *   PROCEDURE Name: xxwc_cash_file_upload_prc
    *
    *   PURPOSE:   AR Receipt file upload from apex
	
	HISTORY
    ===============================================================================
    VERSION DATE          AUTHOR(S)              DESCRIPTION
    ------- -----------   ---------------  -----------------------------------------
    1.0     05/07/2018    Nancy Pahwa      TMS#20160218-00198   AR APEX project - Ability to apply payments in mass
   *****************************************************************************/
  PROCEDURE xxwc_cash_file_upload_prc(p_filename     IN VARCHAR2,
                                      p_directory    IN VARCHAR2,
                                      p_location     IN VARCHAR2 DEFAULT NULL,
                                      p_new_filename OUT VARCHAR2);  
							 

END xxwc_ar_rcpt_transaction_pkg;
/

