/*************************************************************************
    *   Index Name: XXWC_AR_RCPT_MASS_WRITEOFF_TAB_GRANT
    *
    *   PURPOSE:   provide Grant
	
	HISTORY
    ===============================================================================
    VERSION DATE          AUTHOR(S)              DESCRIPTION
    ------- -----------   --------------- -----------------------------------------
    1.0     5/07/2018     Niraj K ranjan         TMS#20160218-00198   AR APEX project - Ability to apply payments in mass
*****************************************************************************/
GRANT ALL ON XXWC_AR_RCPT_MASS_WRITEOFF_TBL TO EA_APEX;
GRANT ALL ON XXWC_AR_RCPT_MASS_REAPPLY_TBL TO EA_APEX;