/*************************************************************************
    *   Index Name: XXWC_AR_RCPT_MASS_WRITEOFF_TAB_IDX
    *
    *   PURPOSE:   Create Index
	
	HISTORY
    ===============================================================================
    VERSION DATE          AUTHOR(S)              DESCRIPTION
    ------- -----------   --------------- -----------------------------------------
    1.0     5/07/2018     Niraj K ranjan         TMS#20160218-00198   AR APEX project - Ability to apply payments in mass
*****************************************************************************/
CREATE INDEX XXWC.XXWC_AR_RCPT_MASS_WRITEOFF_N1 ON XXWC.XXWC_AR_RCPT_MASS_WRITEOFF_TBL(BATCH_ID);
CREATE INDEX XXWC.XXWC_AR_RCPT_MASS_WRITEOFF_N2 ON XXWC.XXWC_AR_RCPT_MASS_WRITEOFF_TBL(CASH_RECEIPT_ID);
CREATE INDEX XXWC.XXWC_AR_RCPT_MASS_WRITEOFF_N3 ON XXWC.XXWC_AR_RCPT_MASS_WRITEOFF_TBL(STATUS);
CREATE INDEX XXWC.XXWC_AR_RCPT_MASS_REAPPLY_N1 ON XXWC.XXWC_AR_RCPT_MASS_REAPPLY_TBL(BATCH_ID);
CREATE INDEX XXWC.XXWC_AR_RCPT_MASS_REAPPLY_N2 ON XXWC.XXWC_AR_RCPT_MASS_REAPPLY_TBL(CASH_RECEIPT_ID);
CREATE INDEX XXWC.XXWC_AR_RCPT_MASS_REAPPLY_N3 ON XXWC.XXWC_AR_RCPT_MASS_REAPPLY_TBL(STATUS);