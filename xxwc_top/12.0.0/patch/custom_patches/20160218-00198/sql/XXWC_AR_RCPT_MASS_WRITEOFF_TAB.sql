/*************************************************************************
    *   Table Name: XXWC_AR_RCPT_MASS_WRITEOFF_TAB
    *
    *   PURPOSE:   Store data for AR Receipt to be write off
	
	HISTORY
    ===============================================================================
    VERSION DATE          AUTHOR(S)              DESCRIPTION
    ------- -----------   --------------- -----------------------------------------
    1.0     4/23/2018     Niraj K ranjan         TMS#20160218-00198   AR APEX project - Ability to apply payments in mass
*****************************************************************************/
CREATE TABLE XXWC.XXWC_AR_RCPT_MASS_WRITEOFF_TBL
(
 BATCH_ID           NUMBER
,REQUEST_ID         NUMBER
,CASH_RECEIPT_ID    NUMBER
,CUSTOMER_NUMBER 	VARCHAR2(30)
,RECEIPT_NUMBER	    VARCHAR2(30)
,RECEIPT_DATE	    DATE
,RECEIPT_AMOUNT 	NUMBER
,ACTIVITY_NAME	    VARCHAR2(50)
,WRITEOFF_AMOUNT    NUMBER
,COMMENTS           VARCHAR2(2000)
,STATUS	            VARCHAR2(100) 
,ERROR_MESSAGE      VARCHAR2(2000)
,CREATED_BY         NUMBER
,CREATION_DATE      DATE
,LAST_UPDATED_BY    NUMBER
,LAST_UPDATE_DATE   DATE
,LAST_UPDATE_LOGIN  NUMBER
);