/*************************************************************************
  $Header TMS_20160915-00302_Order_still_open.sql $
  Module Name: TMS#20160915-00302 - 21409241 Still Open -- Datafix script

  PURPOSE: Data fix to update status code

  REVISIONS:
  Ver        Date         Author                Description
  ---------  -----------  ------------------    --------------------------
  1.0        10-OCT-2016  Pattabhi Avula         TMS#20160915-00302 

**************************************************************************/ 
SET SERVEROUTPUT ON SIZE 100000;
SET VERIFY OFF;
DECLARE
BEGIN
   DBMS_OUTPUT.put_line ('TMS: 20160915-00302    , Before Update');

update apps.oe_order_lines_all
set FLOW_STATUS_CODE='CANCELLED',
CANCELLED_FLAG='Y'
where line_id = 75884441
and header_id= 46381258;
   DBMS_OUTPUT.put_line (
         'TMS: 20160915-00302  Sales order lines updated (Expected:1): '
      || SQL%ROWCOUNT);

   COMMIT;

   DBMS_OUTPUT.put_line ('TMS: 20160915-00302    , End Update');
EXCEPTION
   WHEN OTHERS
   THEN
      ROLLBACK;
      DBMS_OUTPUT.put_line ('TMS: 20160915-00302 , Errors : ' || SQLERRM);
END;
/