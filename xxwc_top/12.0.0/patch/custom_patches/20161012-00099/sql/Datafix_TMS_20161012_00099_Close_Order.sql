/*************************************************************************
  $Header Datafix_TMS_20161012_00099_Close_Order.sql $
  Module Name: TMS_20161012_00099


  REVISIONS:
  Ver        Date         Author                Description
  ---------  -----------  ------------------    --------------------------
   1.0       17-JAN-2016  Niraj K Ranjan        TMS#20161012_00099   ORDER 15389168 and 15595835
**************************************************************************/ 
SET SERVEROUTPUT ON SIZE 1000000
BEGIN
   DBMS_OUTPUT.put_line ('TMS: 20161012_00099    , Before Update');

   UPDATE apps.oe_order_headers_all
    SET flow_status_code='CLOSED',
        open_flag='N',
		last_updated_by = -1,
		last_update_date = sysdate
    WHERE header_id in (23082019,22216711);



   DBMS_OUTPUT.put_line (
         'TMS: 20161012_00099  Sales order lines updated (Expected:2): '
      || SQL%ROWCOUNT);

   COMMIT;

   DBMS_OUTPUT.put_line ('TMS: 20161012_00099    , End Update');
EXCEPTION
   WHEN OTHERS
   THEN
      ROLLBACK;
      DBMS_OUTPUT.put_line ('TMS: 20161012_00099 , Errors : ' || SQLERRM);
END;
/
