CREATE OR REPLACE PACKAGE XXWC_PO_SUPPLIER_CONFN_PKG AS
 /*************************************************************************************
   $Header XXWC_PO_SUPPLIER_CONFN_PKG $
   Module Name: XXWC_PO_SUPPLIER_CONFN_PKG.pks

   PURPOSE:   This package attaches the URL Information to PO mails the confirmation on the same to Buyers.

   REVISIONS:
   Ver        Date        Author                             Description
   ---------  ----------  -------------------------------    -------------------------
   1.0        18/04/2018  Ashwin Sridhar                     Initial Version-TMS#20180305-00300--Automate PO Confirmations - Oracle EBS
  *************************************************************************************/

FUNCTION XXWC_SUPP_CONFIRM_BE_PROC(p_subscription_guid   IN RAW
                                  ,p_event               IN OUT wf_event_t)
  RETURN VARCHAR2;

PROCEDURE XXWC_SUPP_CONFIRM_PROC(p_errbuf    OUT VARCHAR2
                                ,p_retcode   OUT VARCHAR2
                                ,p_po_number IN NUMBER);
  
PROCEDURE XXWC_SEND_BUYER_EMAIL_PROC(p_po_number IN NUMBER
                                    ,p_email     IN VARCHAR2
                                    ,p_url       IN VARCHAR2
                                    );

PROCEDURE XXWC_RAISE_SUPP_CONFIRMATION(p_po_number IN NUMBER
                                      ,p_error_msg OUT VARCHAR2);

END XXWC_PO_SUPPLIER_CONFN_PKG;
/