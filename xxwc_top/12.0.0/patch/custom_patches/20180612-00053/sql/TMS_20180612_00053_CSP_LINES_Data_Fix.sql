/*************************************************************************
      PROCEDURE Name: TMS_20180612_00053_CSP_LINES_Data_Fix.sql

      PURPOSE:   This procedure will be called from CSP form to display cat class items detail in pop up screen.

      REVISIONS:
      Ver        Date          Author             Description
      ---------  ------------  ------------       ------------------
      1.0        07/30/2018  Niraj K Ranjan        TMS#20180612-00053 CSPs Loaded but No Modifier
   ****************************************************************************/
SET SERVEROUTPUT ON SIZE 1000000

--Modifier header exists but line does not exists
DECLARE
   l_responsibility_key     fnd_responsibility_vl.responsibility_key%TYPE :=  'XXWC_ORDER_MGMT_PRICING_FULL';
   l_user_name              fnd_user.user_name%TYPE := 'XXWC_INT_SALESFULFILLMENT';
   l_user_id                NUMBER;
   l_responsibility_id      NUMBER;
   l_resp_application_id    NUMBER;
   e_prc_error              EXCEPTION;
   l_retmsg                 VARCHAR2(2000);
   l_req_id                 NUMBER;
   l_agreement_id           NUMBER;
   
   CURSOR cr_hrd
   IS
      select distinct xocph2.AGREEMENT_ID
      from apps.xxwc_om_contract_pricing_lines xocp2,
           apps.xxwc_om_contract_pricing_hdr xocph2
     where xocp2.agreement_id = xocph2.agreement_id
	   --and xocp2.agreement_id IN (9503 --7576)
       and agreement_line_id in
                             (select distinct  xocpl.agreement_line_id 
                                from apps.xxwc_om_contract_pricing_hdr xocph,
                                     apps.xxwc_om_contract_pricing_lines xocpl
                               where 1=1
                                 and xocph.agreement_id = xocpl.agreement_id
                                 and xocph.agreement_status = 'APPROVED'
                                 and xocpl.line_status = 'APPROVED'
                                 and nvl(trunc(xocpl.end_date),trunc(sysdate+1)) >  trunc(sysdate)
                                 and  exists (
								               select 1 
											   from apps.qp_list_headers qlh 
											   where substr(version_no,1,instr(version_no,'.',1)-1) = to_char(xocph.agreement_id)
											     and qlh.active_flag = 'Y'
											  )
                                 and not exists (select 1 
								                   from apps.qp_list_lines qll 
												  where qll.attribute2 = xocpl.agreement_line_id)
							  )
     order by AGREEMENT_ID;
	  
   CURSOR cr_line(p_agreement_id number)
   IS
    select distinct xocph2.AGREEMENT_ID, xocp2.agreement_line_id
      from apps.xxwc_om_contract_pricing_lines xocp2,
           apps.xxwc_om_contract_pricing_hdr xocph2
     where xocp2.agreement_id = xocph2.agreement_id
       and agreement_line_id in
                             (select distinct  xocpl.agreement_line_id 
                                from apps.xxwc_om_contract_pricing_hdr xocph,
                                     apps.xxwc_om_contract_pricing_lines xocpl
                               where 1=1
                                 and xocph.agreement_id = xocpl.agreement_id
                                 and xocph.agreement_status = 'APPROVED'
                                 and xocpl.line_status = 'APPROVED'
                                 and nvl(trunc(xocpl.end_date),trunc(sysdate+1)) >  trunc(sysdate)
                                 and  exists (
								               select 1 
											   from apps.qp_list_headers qlh 
											   where substr(version_no,1,instr(version_no,'.',1)-1) = to_char(xocph.agreement_id)
											   and qlh.active_flag = 'Y'
											  )
                                 and not exists (select 1 
								                   from apps.qp_list_lines qll 
												  where qll.attribute2 = xocpl.agreement_line_id)
							  )
     order by AGREEMENT_ID;
BEGIN
   DBMS_OUTPUT.PUT_LINE('Start Data fix for case2');
   BEGIN
      SELECT usr.user_id,resp.responsibility_id,resp.application_id
      INTO l_user_id,l_responsibility_id,l_resp_application_id
      FROM APPS.fnd_user_resp_groups_all usr_resp,
           APPS.fnd_responsibility_vl resp,
           APPS.fnd_user  usr
      WHERE resp.responsibility_key = l_responsibility_key
      AND usr.user_name = l_user_name
      AND usr_resp.user_id = usr.user_id
      AND usr_resp.responsibility_id = resp.responsibility_id;
	  
	  FND_GLOBAL.APPS_INITIALIZE (l_user_id,
                                  l_responsibility_id,
                                  l_resp_application_id
								 );
   EXCEPTION
	  WHEN OTHERS THEN
	      l_retmsg := 'User, Responsibility and applications are not identified';
	 	   RAISE e_prc_error;
   END;
   
   FOR rec_hdr IN cr_hrd
   LOOP
      l_agreement_id := rec_hdr.agreement_id;
	  
	  FOR rec_line IN cr_line(l_agreement_id)
	  LOOP
	  
	    UPDATE apps.xxwc_om_contract_pricing_lines
	       SET line_status = 'AWAITING_APPROVAL',
		       revision_number = 0,
			   interfaced_flag = 'N'
	     WHERE agreement_id = l_agreement_id
		   AND agreement_line_id = rec_line.agreement_line_id;
		  
		  DBMS_OUTPUT.put_line ('Records Updated xxwc_om_contract_pricing_lines-' || SQL%ROWCOUNT);

	  END LOOP;
	  
      l_req_id := fnd_request.submit_request (  'XXWC',-- Application short name
                                                'XXWC_OM_CSP_IMP_MODIFIER',--Short name of concurrent program
                                                NULL,--Description 
                                                SYSDATE,--start time
                                                FALSE,--subsequent report name
                                                l_agreement_id);
      commit;
	  DBMS_OUTPUT.PUT_LINE('Import Modifier Program submitted for agreement_id:'||l_agreement_id||
	                        ' with request_id:'||l_req_id);
   END LOOP;
   DBMS_OUTPUT.PUT_LINE('End Data fix for case2');
EXCEPTION
   WHEN e_prc_error THEN
      DBMS_OUTPUT.PUT_LINE('agreement_id:'||l_agreement_id||'=>'||l_retmsg);
   WHEN OTHERS THEN
      l_retmsg := sUBSTR(SQLERRM,1,2000);
	  DBMS_OUTPUT.PUT_LINE('agreement_id:'||l_agreement_id||'=>'||l_retmsg);
END;
/