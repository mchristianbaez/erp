/*************************************************************************
      PROCEDURE Name: TMS_20180612_00053_CSP_HDR_Data_Fix.sql

      PURPOSE:   This procedure will be called from CSP form to display cat class items detail in pop up screen.

      REVISIONS:
      Ver        Date          Author             Description
      ---------  ------------  ------------       ------------------
      1.0        07/30/2018  Niraj K Ranjan        TMS#20180612-00053 CSPs Loaded but No Modifier
   ****************************************************************************/
SET SERVEROUTPUT ON SIZE 1000000

--Modifier header and line does not exists
DECLARE
   l_responsibility_key     fnd_responsibility_vl.responsibility_key%TYPE :=  'XXWC_ORDER_MGMT_PRICING_FULL';
   l_user_name              fnd_user.user_name%TYPE := 'XXWC_INT_SALESFULFILLMENT';
   l_user_id                NUMBER;
   l_responsibility_id      NUMBER;
   l_resp_application_id    NUMBER;
   e_prc_error              EXCEPTION;
   l_retmsg                 VARCHAR2(2000);
   l_req_id                 NUMBER;
   l_agreement_id           NUMBER;
   
   CURSOR cr_hrd
   IS
      select distinct xocph.AGREEMENT_ID --, xocpl.agreement_line_id
      from apps.xxwc_om_contract_pricing_hdr xocph,
           apps.xxwc_om_contract_pricing_lines xocpl
      where 1=1
	  and xocph.agreement_id IN (
	                           519554,
	                           519554,
							   666546,
							   192481,
							   449652,
							   470567,
							   537719,
							   393559,
							   449680,
                               95336
                               )
	  and xocph.agreement_id = xocpl.agreement_id
      and xocph.agreement_status = 'APPROVED'
      and xocpl.line_status = 'APPROVED'
      and nvl(trunc(xocpl.end_date),trunc(sysdate+1)) >  trunc(sysdate)
      and not exists (select 1 from apps.qp_list_headers qlh where substr(version_no,1,instr(version_no,'.',1)-1) = to_char(xocph.agreement_id))
      and not exists (select 1 from apps.qp_list_lines qll where qll.attribute2 = xocpl.agreement_line_id)
      order by xocph.AGREEMENT_ID;
	  
   CURSOR cr_line(p_agreement_id number)
   IS
      select distinct xocph.agreement_id, xocpl.agreement_line_id
      from apps.xxwc_om_contract_pricing_hdr xocph,
           apps.xxwc_om_contract_pricing_lines xocpl
      where xocph.agreement_id = p_agreement_id
	  and xocph.agreement_id = xocpl.agreement_id
      and xocph.agreement_status IN ('APPROVED','AWAITING_APPROVAL')
      and xocpl.line_status = 'APPROVED'
      and nvl(trunc(xocpl.end_date),trunc(sysdate+1)) >  trunc(sysdate)
      and not exists (select 1 from apps.qp_list_headers qlh where substr(version_no,1,instr(version_no,'.',1)-1) = to_char(xocph.agreement_id))
      and not exists (select 1 from apps.qp_list_lines qll where qll.attribute2 = xocpl.agreement_line_id)
      order by xocph.AGREEMENT_ID,xocpl.agreement_line_id;
BEGIN
   DBMS_OUTPUT.PUT_LINE('Start Data fix for case1');
   BEGIN
      SELECT usr.user_id,resp.responsibility_id,resp.application_id
      INTO l_user_id,l_responsibility_id,l_resp_application_id
      FROM APPS.fnd_user_resp_groups_all usr_resp,
           APPS.fnd_responsibility_vl resp,
           APPS.fnd_user  usr
      WHERE resp.responsibility_key = l_responsibility_key
      AND usr.user_name = l_user_name
      AND usr_resp.user_id = usr.user_id
      AND usr_resp.responsibility_id = resp.responsibility_id;
	  
	  FND_GLOBAL.APPS_INITIALIZE (l_user_id,
                                  l_responsibility_id,
                                  l_resp_application_id
								 );
   EXCEPTION
	  WHEN OTHERS THEN
	      l_retmsg := 'User, Responsibility and applications are not identified';
	 	   RAISE e_prc_error;
   END;
   
   UPDATE xxwc.xxwc_om_contract_pricing_hdr
      SET agreement_type='CSP'
    WHERE agreement_id=666546;
  
   DBMS_OUTPUT.put_line ('Records Updated in xxwc_om_contract_pricing_hdr -' || SQL%ROWCOUNT);
   COMMIT;
	  
   FOR rec_hdr IN cr_hrd
   LOOP
      l_agreement_id := rec_hdr.agreement_id;
      
      UPDATE apps.xxwc_om_contract_pricing_hdr
	  SET agreement_status = 'AWAITING_APPROVAL',
	  revision_number = 0
	  WHERE agreement_id = l_agreement_id;
	  
	  DBMS_OUTPUT.put_line ('Records Updated in xxwc_om_contract_pricing_hdr -' || SQL%ROWCOUNT);
	  COMMIT;
	  
	  FOR rec_line IN cr_line(l_agreement_id)
	  LOOP
	      UPDATE apps.xxwc_om_contract_pricing_lines
	      SET line_status = 'AWAITING_APPROVAL',
		      revision_number = 0,
			  interfaced_flag = 'N'
	      WHERE agreement_id = l_agreement_id
		  AND   agreement_line_id = rec_line.agreement_line_id;
		  
		  DBMS_OUTPUT.put_line ('Records Updated xxwc_om_contract_pricing_lines-' || SQL%ROWCOUNT);
	  END LOOP;
	 
      COMMIT;
	  
      l_req_id := fnd_request.submit_request (  'XXWC',-- Application short name
                                                'XXWC_OM_CSP_IMP_MODIFIER',--Short name of concurrent program
                                                NULL,--Description 
                                                SYSDATE,--start time
                                                FALSE,--subsequent report name
                                                l_agreement_id);
      commit;
	  DBMS_OUTPUT.PUT_LINE('Import Modifier Program submitted for agreement_id:'||l_agreement_id||
	                        ' with request_id:'||l_req_id);
   END LOOP;
   
   
   DBMS_OUTPUT.PUT_LINE('End Data fix for case1');
EXCEPTION
   WHEN e_prc_error THEN
      DBMS_OUTPUT.PUT_LINE('agreement_id:'||l_agreement_id||'=>'||l_retmsg);
   WHEN OTHERS THEN
      l_retmsg := SUBSTR(SQLERRM,1,2000);
	  DBMS_OUTPUT.PUT_LINE('agreement_id:'||l_agreement_id||'=>'||l_retmsg);
END;
/
