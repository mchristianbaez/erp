--Report Name            : Freight Adder - WC
--Import Instructions    : Run this file in XXEIS user from SQL*Plus or any tool like Toad, SQL Navigator. 
prompt Creating Object Data EIS_XXWC_GL_FREIGHT_ADDER_V
set scan off define off
DECLARE
mod_exist VARCHAR2(1);
BEGIN 
mod_exist := XXEIS.EIS_RSC_INSERT.check_if_module_exist(101);
IF mod_exist = 'Y' THEN 
--Inserting Object EIS_XXWC_GL_FREIGHT_ADDER_V
xxeis.eis_rsc_ins.v( 'EIS_XXWC_GL_FREIGHT_ADDER_V',101,'','','','','SA059956','XXEIS','Eis Xxwc Gl Freight Adder V','EXGFAV','','','VIEW','US','','');
--Delete Object Columns for EIS_XXWC_GL_FREIGHT_ADDER_V
xxeis.eis_rsc_utility.delete_view_rows('EIS_XXWC_GL_FREIGHT_ADDER_V',101,FALSE);
--Inserting Object Columns for EIS_XXWC_GL_FREIGHT_ADDER_V
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_GL_FREIGHT_ADDER_V','MP_ORGANIZATION_ID',101,'Mp Organization Id','MP_ORGANIZATION_ID','','','','SA059956','NUMBER','','','Mp Organization Id','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_GL_FREIGHT_ADDER_V','POV_VENDOR_ID',101,'Pov Vendor Id','POV_VENDOR_ID','','','','SA059956','NUMBER','','','Pov Vendor Id','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_GL_FREIGHT_ADDER_V','DISTRICT',101,'District','DISTRICT','','','','SA059956','VARCHAR2','','','District','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_GL_FREIGHT_ADDER_V','REGION',101,'Region','REGION','','','','SA059956','VARCHAR2','','','Region','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_GL_FREIGHT_ADDER_V','VENDOR_TYPE',101,'Vendor Type','VENDOR_TYPE','','','','SA059956','VARCHAR2','','','Vendor Type','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_GL_FREIGHT_ADDER_V','FREIGHT_BASIS',101,'Freight Basis','FREIGHT_BASIS','','','','SA059956','VARCHAR2','','','Freight Basis','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_GL_FREIGHT_ADDER_V','FREIGHT_PER_LB',101,'Freight Per Lb','FREIGHT_PER_LB','','','','SA059956','NUMBER','','','Freight Per Lb','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_GL_FREIGHT_ADDER_V','IMPORT_FLAG',101,'Import Flag','IMPORT_FLAG','','','','SA059956','VARCHAR2','','','Import Flag','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_GL_FREIGHT_ADDER_V','FREIGHT_DUTY',101,'Freight Duty','FREIGHT_DUTY','','','','SA059956','NUMBER','','','Freight Duty','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_GL_FREIGHT_ADDER_V','VENDOR_NAME',101,'Vendor Name','VENDOR_NAME','','','','SA059956','VARCHAR2','','','Vendor Name','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_GL_FREIGHT_ADDER_V','VENDOR_NUMBER',101,'Vendor Number','VENDOR_NUMBER','','','','SA059956','VARCHAR2','','','Vendor Number','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_GL_FREIGHT_ADDER_V','ORGANIZATION_CODE',101,'Organization Code','ORGANIZATION_CODE','','','','SA059956','VARCHAR2','','','Organization Code','','','','US');
--Inserting Object Components for EIS_XXWC_GL_FREIGHT_ADDER_V
xxeis.eis_rsc_ins.vcomp( 'EIS_XXWC_GL_FREIGHT_ADDER_V','AP_SUPPLIERS',101,'AP_SUPPLIERS','POV','POV','SA059956','SA059956','140913638','Ap Suppliers Stores Information About Your Supplie','','','','','','',' ',' ',' ',' ',' ');
xxeis.eis_rsc_ins.vcomp( 'EIS_XXWC_GL_FREIGHT_ADDER_V','MTL_PARAMETERS',101,'MTL_PARAMETERS','MP','MP','SA059956','SA059956','140913638','Inventory Control Options And Defaults','','','','','','',' ',' ',' ',' ',' ');
--Inserting Object Component Joins for EIS_XXWC_GL_FREIGHT_ADDER_V
xxeis.eis_rsc_ins.vcj( 'EIS_XXWC_GL_FREIGHT_ADDER_V','AP_SUPPLIERS','POV',101,'EXGFAV.POV_VENDOR_ID','=','POV.VENDOR_ID(+)','','','','Y','SA059956');
xxeis.eis_rsc_ins.vcj( 'EIS_XXWC_GL_FREIGHT_ADDER_V','MTL_PARAMETERS','MP',101,'EXGFAV.MP_ORGANIZATION_ID','=','MP.ORGANIZATION_ID(+)','','','','Y','SA059956');
ELSE
RAISE_APPLICATION_ERROR (-20001,'Module does not exist. Import/Create the module with id 101');
END IF;
END;
/
prompt Creating Report LOV Data for Freight Adder - WC
set scan off define off
DECLARE
mod_exist VARCHAR2(1);
BEGIN 
mod_exist := XXEIS.EIS_RSC_INSERT.check_if_module_exist(101);
IF mod_exist = 'Y' THEN 
--Inserting Report LOVs - Freight Adder - WC
xxeis.eis_rsc_ins.lov( '','select distinct segment1 vendor_number,vendor_name from po_vendors order by lpad(segment1,10)','','EIS XXWC Vendor Num LOV','','SA059956',NULL,'N','','','N','N','','','US');
xxeis.eis_rsc_ins.lov( 101,'SELECT ood.organization_code warehouse, ood.organization_name
  FROM org_organization_definitions ood
 WHERE    1=1
 AND SYSDATE < NVL (ood.disable_date, SYSDATE + 1)
       AND EXISTS
              (SELECT 1
                 FROM XXEIS.EIS_ORG_ACCESS_V
                WHERE ORGANIZATION_ID = OOD.ORGANIZATION_ID)
order by 1','','EIS XXWC GL Org Code LOV','This LOV Lists all the organization code details.','SA059956',NULL,'N','','','N','N','','','US');
xxeis.eis_rsc_ins.lov( '','SELECT DISTINCT mp.attribute9 region
FROM mtl_parameters mp
WHERE mp.attribute9 IS NOT NULL
ORDER BY 1','','EIS XXWC Region LOV','This LOV Lists all region details.','SA059956',NULL,'N','','','N','N','','','US');
xxeis.eis_rsc_ins.lov( '','SELECT DISTINCT mp.attribute8 district
FROM mtl_parameters mp
WHERE mp.attribute8 IS NOT NULL
ORDER BY 1','','EIS XXWC District LOV','This LOV lists all District details','SA059956',NULL,'N','','','N','N','','','US');
null;
ELSE
RAISE_APPLICATION_ERROR (-20001,'Module does not exist. Import/Create the module with id 101');
END IF;
END;
/
prompt Creating Report Data for Freight Adder - WC
set scan off define off
DECLARE
mod_exist varchar2(1);
BEGIN 
mod_exist := XXEIS.EIS_RSC_INSERT.check_if_module_exist(101);
IF mod_exist = 'Y' THEN 
--Deleting Report data - Freight Adder - WC
xxeis.eis_rsc_utility.delete_report_rows( 'Freight Adder - WC' );
--Inserting Report - Freight Adder - WC
xxeis.eis_rsc_ins.r( 101,'Freight Adder - WC','','Pull all freight adders for all branches and include region/district','','','','SA059956','EIS_XXWC_GL_FREIGHT_ADDER_V','Y','','','SA059956','','N','White Cap Reports','','CSV,EXCEL,','N','','','','','','N','','US','','','','');
--Inserting Report Columns - Freight Adder - WC
xxeis.eis_rsc_ins.rc( 'Freight Adder - WC',101,'DISTRICT','District','District','','','default','','10','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_GL_FREIGHT_ADDER_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Freight Adder - WC',101,'FREIGHT_BASIS','Freight Basis','Freight Basis','','','default','','7','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_GL_FREIGHT_ADDER_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Freight Adder - WC',101,'FREIGHT_DUTY','Freight Duty','Freight Duty','','~T~D~','default','','4','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_GL_FREIGHT_ADDER_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Freight Adder - WC',101,'FREIGHT_PER_LB','Freight Per Lb','Freight Per Lb','','~T~D~','default','','6','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_GL_FREIGHT_ADDER_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Freight Adder - WC',101,'IMPORT_FLAG','Import Flag','Import Flag','','','default','','5','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_GL_FREIGHT_ADDER_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Freight Adder - WC',101,'ORGANIZATION_CODE','Org Code','Organization Code','','','default','','1','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_GL_FREIGHT_ADDER_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Freight Adder - WC',101,'REGION','Region','Region','','','default','','9','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_GL_FREIGHT_ADDER_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Freight Adder - WC',101,'VENDOR_NAME','Vendor Name','Vendor Name','','','default','','3','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_GL_FREIGHT_ADDER_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Freight Adder - WC',101,'VENDOR_NUMBER','Vendor Number','Vendor Number','','','default','','2','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_GL_FREIGHT_ADDER_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Freight Adder - WC',101,'VENDOR_TYPE','Vendor Type','Vendor Type','','','default','','8','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_GL_FREIGHT_ADDER_V','','','','US','');
--Inserting Report Parameters - Freight Adder - WC
xxeis.eis_rsc_ins.rp( 'Freight Adder - WC',101,'District','District','DISTRICT','IN','EIS XXWC District LOV','','VARCHAR2','N','Y','2','Y','Y','CONSTANT','SA059956','Y','N','','','','EIS_XXWC_GL_FREIGHT_ADDER_V','','','US','');
xxeis.eis_rsc_ins.rp( 'Freight Adder - WC',101,'Org','Organization Code','ORGANIZATION_CODE','IN','EIS XXWC GL Org Code LOV','','VARCHAR2','N','Y','1','Y','Y','CONSTANT','SA059956','Y','N','','','','EIS_XXWC_GL_FREIGHT_ADDER_V','','','US','');
xxeis.eis_rsc_ins.rp( 'Freight Adder - WC',101,'Region','Region','REGION','IN','EIS XXWC Region LOV','','VARCHAR2','N','Y','3','Y','Y','CONSTANT','SA059956','Y','N','','','','EIS_XXWC_GL_FREIGHT_ADDER_V','','','US','');
xxeis.eis_rsc_ins.rp( 'Freight Adder - WC',101,'Vendor Num','Vendor Number','VENDOR_NUMBER','IN','EIS XXWC Vendor Num LOV','','VARCHAR2','N','Y','4','Y','Y','CONSTANT','SA059956','Y','N','','','','EIS_XXWC_GL_FREIGHT_ADDER_V','','','US','');
--Inserting Dependent Parameters - Freight Adder - WC
--Inserting Report Conditions - Freight Adder - WC
xxeis.eis_rsc_ins.rcnh( 'Freight Adder - WC',101,'EXGFAV.ORGANIZATION_CODE IN Org','SIMPLE','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','ORGANIZATION_CODE','','Org','','','','','EIS_XXWC_GL_FREIGHT_ADDER_V','','','','','','IN','Y','Y','','','','','1',101,'Freight Adder - WC','EXGFAV.ORGANIZATION_CODE IN Org');
xxeis.eis_rsc_ins.rcnh( 'Freight Adder - WC',101,'EXGFAV.DISTRICT IN District','SIMPLE','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','DISTRICT','','District','','','','','EIS_XXWC_GL_FREIGHT_ADDER_V','','','','','','IN','Y','Y','','','','','1',101,'Freight Adder - WC','EXGFAV.DISTRICT IN District');
xxeis.eis_rsc_ins.rcnh( 'Freight Adder - WC',101,'EXGFAV.REGION IN Region','SIMPLE','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','REGION','','Region','','','','','EIS_XXWC_GL_FREIGHT_ADDER_V','','','','','','IN','Y','Y','','','','','1',101,'Freight Adder - WC','EXGFAV.REGION IN Region');
xxeis.eis_rsc_ins.rcnh( 'Freight Adder - WC',101,'EXGFAV.VENDOR_NUMBER IN Vendor Num','SIMPLE','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','VENDOR_NUMBER','','Vendor Num','','','','','EIS_XXWC_GL_FREIGHT_ADDER_V','','','','','','IN','Y','Y','','','','','1',101,'Freight Adder - WC','EXGFAV.VENDOR_NUMBER IN Vendor Num');
--Inserting Report Sorts - Freight Adder - WC
xxeis.eis_rsc_ins.rs( 'Freight Adder - WC',101,'ORGANIZATION_CODE','ASC','SA059956','1','');
xxeis.eis_rsc_ins.rs( 'Freight Adder - WC',101,'VENDOR_TYPE','ASC','SA059956','2','');
xxeis.eis_rsc_ins.rs( 'Freight Adder - WC',101,'VENDOR_NUMBER','ASC','SA059956','3','');
--Inserting Report Triggers - Freight Adder - WC
--inserting report templates - Freight Adder - WC
--Inserting Report Portals - Freight Adder - WC
--inserting report dashboards - Freight Adder - WC
--Exporting report views table information -  
xxeis.eis_rsc_ins.rviews( 'Freight Adder - WC','101','EIS_XXWC_GL_FREIGHT_ADDER_V','EIS_XXWC_GL_FREIGHT_ADDER_V','N','');
--inserting report security - Freight Adder - WC
xxeis.eis_rsc_ins.rsec( 'Freight Adder - WC','101','','XXCUS_GL_ACCOUNTANT_USD_PVF',101,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'Freight Adder - WC','101','','XXCUS_GL_ACCOUNTANT_USD_PS',101,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'Freight Adder - WC','101','','XXCUS_GL_ACCOUNTANT_USD',101,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'Freight Adder - WC','101','','XXCUS_GL_ACCOUNTANT_GLOBAL',101,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'Freight Adder - WC','101','','XXCUS_GL_ACCOUNTANT_CAD_PVF',101,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'Freight Adder - WC','101','','XXCUS_GL_ACCOUNTANT_CAD_PS',101,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'Freight Adder - WC','101','','XXCUS_GL_ACCOUNTANT_CAD',101,'SA059956','','','');
--Inserting Report Pivots - Freight Adder - WC
--Inserting Report   Version details- Freight Adder - WC
xxeis.eis_rsc_ins.rv( 'Freight Adder - WC','','Freight Adder - WC','SA059956');
ELSE
RAISE_APPLICATION_ERROR (-20001,'Module does not exist. Import/Create the module with id 101');
END IF;
END;
/
