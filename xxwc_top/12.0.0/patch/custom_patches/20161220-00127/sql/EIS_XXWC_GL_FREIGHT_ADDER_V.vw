---------------------------------------------------------------------------------------------------------------
/**************************************************************************************************************
  $Header XXEIS.EIS_XXWC_GL_FREIGHT_ADDER_V $
  REVISIONS   :
  VERSION 		DATE               AUTHOR(S)       DESCRIPTION
  ------- -----------------  --------------- -------------------------------------
  1.0    	   01-Jan-2017        	Siva   		 TMS#20161220-00127 
**************************************************************************************************************/
DROP VIEW XXEIS.EIS_XXWC_GL_FREIGHT_ADDER_V;
CREATE OR REPLACE VIEW XXEIS.EIS_XXWC_GL_FREIGHT_ADDER_V (ORGANIZATION_CODE, VENDOR_NUMBER, VENDOR_NAME, FREIGHT_DUTY,
 IMPORT_FLAG, FREIGHT_PER_LB, FREIGHT_BASIS, VENDOR_TYPE, REGION, DISTRICT, POV_VENDOR_ID, MP_ORGANIZATION_ID)
AS
  SELECT /*+ INDEX(pov AP_SUPPLIERS_U1) INDEX(mp XXWC_OBIEE_MTL_PARAMETERS1)*/
    mp.organization_code,
    pov.segment1 vendor_number,
    pov.vendor_name,
    xvm.freight_duty,
    xvm.import_flag,
    xvm.freight_per_lb,
    xvm.freight_basis,
    xvm.vendor_type,
    mp.attribute9 region,
    mp.attribute8 district,
    --Primary Keys
    pov.vendor_id pov_vendor_id,
    mp.organization_id mp_organization_id
    --descr#flexfield#start
    --descr#flexfield#end
  FROM xxwc.xxwc_po_freight_burden_tbl xvm,
    apps.ap_suppliers pov,
    apps.mtl_parameters mp
  WHERE xvm.vendor_id     = pov.vendor_id(+)
  AND xvm.organization_id = mp.organization_id
/
