--Report Name            : EIS Force Ship Report - WC
--Import Instructions    : Run this file in XXEIS user from SQL*Plus or any tool like Toad, SQL Navigator. 
prompt Creating Object Data XXEIS_WC_OM_FORCESHIP_V
set scan off define off
DECLARE
mod_exist VARCHAR2(1);
BEGIN 
mod_exist := XXEIS.EIS_RSC_INSERT.check_if_module_exist(660);
IF mod_exist = 'Y' THEN 
--Inserting Object XXEIS_WC_OM_FORCESHIP_V
xxeis.eis_rsc_ins.v( 'XXEIS_WC_OM_FORCESHIP_V',660,'','','','','ANONYMOUS','XXEIS','Xxeis Wc Om Forceship V','XWOFV','','','VIEW','US','','','');
--Delete Object Columns for XXEIS_WC_OM_FORCESHIP_V
xxeis.eis_rsc_utility.delete_view_rows('XXEIS_WC_OM_FORCESHIP_V',660,FALSE);
--Inserting Object Columns for XXEIS_WC_OM_FORCESHIP_V
xxeis.eis_rsc_ins.vc( 'XXEIS_WC_OM_FORCESHIP_V','FORCE_SHIP_USER',660,'Force Ship User','FORCE_SHIP_USER','','','','ANONYMOUS','VARCHAR2','','','Force Ship User','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_WC_OM_FORCESHIP_V','DATE_OF_FORCE_SHIP',660,'Date Of Force Ship','DATE_OF_FORCE_SHIP','','','','ANONYMOUS','DATE','','','Date Of Force Ship','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_WC_OM_FORCESHIP_V','FORCE_SHIP_QUANTITY',660,'Force Ship Quantity','FORCE_SHIP_QUANTITY','','~T~D~2','','ANONYMOUS','NUMBER','','','Force Ship Quantity','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_WC_OM_FORCESHIP_V','ORDER_QUANTITY',660,'Order Quantity','ORDER_QUANTITY','','~T~D~2','','ANONYMOUS','NUMBER','','','Order Quantity','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_WC_OM_FORCESHIP_V','PART_NUMBER',660,'Part Number','PART_NUMBER','','','','ANONYMOUS','VARCHAR2','','','Part Number','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_WC_OM_FORCESHIP_V','LINE_NUMBER',660,'Line Number','LINE_NUMBER','','','','ANONYMOUS','NUMBER','','','Line Number','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_WC_OM_FORCESHIP_V','ORDER_NUMBER',660,'Order Number','ORDER_NUMBER','','','','ANONYMOUS','NUMBER','','','Order Number','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_WC_OM_FORCESHIP_V','BRANCH',660,'Branch','BRANCH','','','','ANONYMOUS','VARCHAR2','','','Branch','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_WC_OM_FORCESHIP_V','BRANCH_NUMBER',660,'Branch Number','BRANCH_NUMBER','','','','ANONYMOUS','VARCHAR2','','','Branch Number','','','','US','');
--Inserting Object Components for XXEIS_WC_OM_FORCESHIP_V
--Inserting Object Component Joins for XXEIS_WC_OM_FORCESHIP_V
ELSE
RAISE_APPLICATION_ERROR (-20001,'Module does not exist. Import/Create the module with id 660');
END IF;
END;
/
prompt Creating Report LOV Data for EIS Force Ship Report - WC
set scan off define off
DECLARE
mod_exist VARCHAR2(1);
BEGIN 
mod_exist := XXEIS.EIS_RSC_INSERT.check_if_module_exist(660);
IF mod_exist = 'Y' THEN 
--Inserting Report LOVs - EIS Force Ship Report - WC
xxeis.eis_rsc_ins.lov( 660,'select distinct organization_code org_code,organization_name org_name from org_organization_definitions','','Branch Lov','','XXEIS_RS_ADMIN',NULL,'N','','','','','','','US');
null;
ELSE
RAISE_APPLICATION_ERROR (-20001,'Module does not exist. Import/Create the module with id 660');
END IF;
END;
/
prompt Creating Report Data for EIS Force Ship Report - WC
set scan off define off
DECLARE
mod_exist varchar2(1);
BEGIN 
mod_exist := XXEIS.EIS_RSC_INSERT.check_if_module_exist(660);
IF mod_exist = 'Y' THEN 
--Deleting Report data - EIS Force Ship Report - WC
xxeis.eis_rsc_utility.delete_report_rows( 'EIS Force Ship Report - WC',660 );
--Inserting Report - EIS Force Ship Report - WC
xxeis.eis_rsc_ins.r( 660,'EIS Force Ship Report - WC','','EIS Force Ship Report - WC is within the Sales and Fulfillment workspace','','','','MR020532','XXEIS_WC_OM_FORCESHIP_V','Y','','','MR020532','','N','White Cap Reports','PDF,','CSV,Pivot Excel,EXCEL,','N','','','','','','','','US','','','','','','','','','','','','','','','','');
--Inserting Report Columns - EIS Force Ship Report - WC
xxeis.eis_rsc_ins.rc( 'EIS Force Ship Report - WC',660,'BRANCH','Branch','Branch','','','default','','2','N','Y','','','','','','','MR020532','N','N','','XXEIS_WC_OM_FORCESHIP_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'EIS Force Ship Report - WC',660,'BRANCH_NUMBER','Branch Number','Branch Number','','','default','','1','N','Y','','','','','','','MR020532','N','N','','XXEIS_WC_OM_FORCESHIP_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'EIS Force Ship Report - WC',660,'DATE_OF_FORCE_SHIP','Date Of Force Ship','Date Of Force Ship','','','default','','8','N','Y','','','','','','','MR020532','N','N','','XXEIS_WC_OM_FORCESHIP_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'EIS Force Ship Report - WC',660,'FORCE_SHIP_QUANTITY','Force Ship Quantity','Force Ship Quantity','','~T~D~0','default','','7','N','Y','','','','','','','MR020532','N','N','','XXEIS_WC_OM_FORCESHIP_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'EIS Force Ship Report - WC',660,'FORCE_SHIP_USER','Force Ship User','Force Ship User','','','default','','9','N','Y','','','','','','','MR020532','N','N','','XXEIS_WC_OM_FORCESHIP_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'EIS Force Ship Report - WC',660,'LINE_NUMBER','Line Number','Line Number','','~~~','default','','4','N','Y','','','','','','','MR020532','N','N','','XXEIS_WC_OM_FORCESHIP_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'EIS Force Ship Report - WC',660,'ORDER_NUMBER','Order Number','Order Number','','~~~','default','','3','N','Y','','','','','','','MR020532','N','N','','XXEIS_WC_OM_FORCESHIP_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'EIS Force Ship Report - WC',660,'ORDER_QUANTITY','Order Quantity','Order Quantity','','~T~D~0','default','','6','N','Y','','','','','','','MR020532','N','N','','XXEIS_WC_OM_FORCESHIP_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'EIS Force Ship Report - WC',660,'PART_NUMBER','Part Number','Part Number','','','default','','5','N','Y','','','','','','','MR020532','N','N','','XXEIS_WC_OM_FORCESHIP_V','','','','US','','');
--Inserting Report Parameters - EIS Force Ship Report - WC
xxeis.eis_rsc_ins.rp( 'EIS Force Ship Report - WC',660,'Branch','Branch','BRANCH_NUMBER','IN','Branch Lov','','VARCHAR2','Y','Y','1','Y','Y','CONSTANT','MR020532','Y','N','','','','XXEIS_WC_OM_FORCESHIP_V','','','US','');
xxeis.eis_rsc_ins.rp( 'EIS Force Ship Report - WC',660,'Date','Date','DATE_OF_FORCE_SHIP','IN','','','DATE','N','Y','2','Y','Y','CURRENT_DATE','MR020532','Y','N','','Start Date','','XXEIS_WC_OM_FORCESHIP_V','','','US','');
--Inserting Dependent Parameters - EIS Force Ship Report - WC
--Inserting Report Conditions - EIS Force Ship Report - WC
xxeis.eis_rsc_ins.rcnh( 'EIS Force Ship Report - WC',660,'XWOFV.BRANCH_NUMBER IN Branch','SIMPLE','','','Y','','1');
xxeis.eis_rsc_ins.rcnd( '','','BRANCH_NUMBER','','Branch','','','','','XXEIS_WC_OM_FORCESHIP_V','','','','','','IN','Y','Y','','','','','1',660,'EIS Force Ship Report - WC','XWOFV.BRANCH_NUMBER IN Branch');
xxeis.eis_rsc_ins.rcnh( 'EIS Force Ship Report - WC',660,'XWOFV.DATE_OF_FORCE_SHIP IN Date','SIMPLE','','','Y','','2');
xxeis.eis_rsc_ins.rcnd( '','','DATE_OF_FORCE_SHIP','','Date','','','','','XXEIS_WC_OM_FORCESHIP_V','','','','','','IN','Y','Y','','','','','1',660,'EIS Force Ship Report - WC','XWOFV.DATE_OF_FORCE_SHIP IN Date');
xxeis.eis_rsc_ins.rcnh( 'EIS Force Ship Report - WC',660,'FreeText','FREE_TEXT','','','Y','','3');
xxeis.eis_rsc_ins.rcnd( '','','','','','','','','','','','','','','','','Y','Y','','','','and XWOFV.FORCE_SHIP_QUANTITY is not null','1',660,'EIS Force Ship Report - WC','FreeText');
--Inserting Report Sorts - EIS Force Ship Report - WC
xxeis.eis_rsc_ins.rs( 'EIS Force Ship Report - WC',660,'BRANCH_NUMBER','ASC','MR020532','1','');
xxeis.eis_rsc_ins.rs( 'EIS Force Ship Report - WC',660,'ORDER_NUMBER','ASC','MR020532','2','');
xxeis.eis_rsc_ins.rs( 'EIS Force Ship Report - WC',660,'LINE_NUMBER','ASC','MR020532','3','');
--Inserting Report Triggers - EIS Force Ship Report - WC
--inserting report templates - EIS Force Ship Report - WC
--Inserting Report Portals - EIS Force Ship Report - WC
--inserting report dashboards - EIS Force Ship Report - WC
--Exporting report views table information -  
xxeis.eis_rsc_ins.rviews( 'EIS Force Ship Report - WC','660','XXEIS_WC_OM_FORCESHIP_V','XXEIS_WC_OM_FORCESHIP_V','N','');
--inserting report security - EIS Force Ship Report - WC
xxeis.eis_rsc_ins.rsec( 'EIS Force Ship Report - WC','660','','XXWC_RENTAL_OM_PRICING_SUPER',660,'MR020532','','','');
xxeis.eis_rsc_ins.rsec( 'EIS Force Ship Report - WC','660','','XXWC_ORDER_MGMT_SUPER_USER',660,'MR020532','','','');
xxeis.eis_rsc_ins.rsec( 'EIS Force Ship Report - WC','660','','XXWC_ORDER_MGMT_READ_SHIPPING',660,'MR020532','','','');
xxeis.eis_rsc_ins.rsec( 'EIS Force Ship Report - WC','660','','XXWC_ORDER_MGMT_READ_ONLY_COST',660,'MR020532','','','');
xxeis.eis_rsc_ins.rsec( 'EIS Force Ship Report - WC','660','','XXWC_ORDER_MGMT_READ_ONLY',660,'MR020532','','','');
xxeis.eis_rsc_ins.rsec( 'EIS Force Ship Report - WC','660','','XXWC_ORDER_MGMT_PRICING_SUPER',660,'MR020532','','','');
xxeis.eis_rsc_ins.rsec( 'EIS Force Ship Report - WC','660','','XXWC_ORDER_MGMT_PRICING_STD',660,'MR020532','','','');
xxeis.eis_rsc_ins.rsec( 'EIS Force Ship Report - WC','660','','XXWC_ORDER_MGMT_PRICING_LTD',660,'MR020532','','','');
xxeis.eis_rsc_ins.rsec( 'EIS Force Ship Report - WC','660','','XXWC_ORDER_MGMT_PRICING_FULL',660,'MR020532','','','');
xxeis.eis_rsc_ins.rsec( 'EIS Force Ship Report - WC','20005','','XXWC_VIEW_ALL_EIS_REPORTS',660,'MR020532','','','');
--Inserting Report Pivots - EIS Force Ship Report - WC
--Inserting Report Distribution Details 
--Inserting Report Distribution Details 
--Inserting Report SubTotals Details 
--Inserting Report aggregations 
--Inserting Report   Version details- EIS Force Ship Report - WC
xxeis.eis_rsc_ins.rv( 'EIS Force Ship Report - WC','','EIS Force Ship Report - WC','SA059956','05-SEP-2017');
ELSE
RAISE_APPLICATION_ERROR (-20001,'Module does not exist. Import/Create the module with id 660');
END IF;
END;
/
