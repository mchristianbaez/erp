--Report Name            : CCG EBS Audit Report - WC
--Import Instructions    : Run this file in XXEIS user from SQL*Plus or any tool like Toad, SQL Navigator. 
prompt Creating Object Data EIS_XXWC_CCG_AUDIT_V
set scan off define off
DECLARE
mod_exist VARCHAR2(1);
BEGIN 
mod_exist := XXEIS.EIS_RSC_INSERT.check_if_module_exist(85000);
IF mod_exist = 'Y' THEN 
--Inserting Object EIS_XXWC_CCG_AUDIT_V
xxeis.eis_rsc_ins.v( 'EIS_XXWC_CCG_AUDIT_V',85000,'','','','','SR056655','XXEIS','Eis Xxwc Ccg Audit V','EXCAV','','','VIEW','US','','','');
--Delete Object Columns for EIS_XXWC_CCG_AUDIT_V
xxeis.eis_rsc_utility.delete_view_rows('EIS_XXWC_CCG_AUDIT_V',85000,FALSE);
--Inserting Object Columns for EIS_XXWC_CCG_AUDIT_V
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_CCG_AUDIT_V','CHANGE_OBJECT_ID',85000,'Change Object Id','CHANGE_OBJECT_ID','','','','SR056655','NUMBER','','','Change Object Id','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_CCG_AUDIT_V','TARGET_NAME',85000,'Target Name','TARGET_NAME','','','','SR056655','VARCHAR2','','','Target Name','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_CCG_AUDIT_V','OBJECT_NAME',85000,'Object Name','OBJECT_NAME','','','','SR056655','VARCHAR2','','','Object Name','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_CCG_AUDIT_V','OWNING_APPLICATION_NAME',85000,'Owning Application Name','OWNING_APPLICATION_NAME','','','','SR056655','VARCHAR2','','','Owning Application Name','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_CCG_AUDIT_V','TABLE_NAME',85000,'Table Name','TABLE_NAME','','','','SR056655','VARCHAR2','','','Table Name','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_CCG_AUDIT_V','TABLE_DESC',85000,'Table Desc','TABLE_DESC','','','','SR056655','VARCHAR2','','','Table Desc','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_CCG_AUDIT_V','AUDIT_DATE',85000,'Audit Date','AUDIT_DATE','','','','SR056655','DATE','','','Audit Date','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_CCG_AUDIT_V','AUDIT_TYPE',85000,'Audit Type','AUDIT_TYPE','','','','SR056655','VARCHAR2','','','Audit Type','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_CCG_AUDIT_V','APP_USER_NAME',85000,'App User Name','APP_USER_NAME','','','','SR056655','VARCHAR2','','','App User Name','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_CCG_AUDIT_V','RESPONSIBILITY_NAME',85000,'Responsibility Name','RESPONSIBILITY_NAME','','','','SR056655','VARCHAR2','','','Responsibility Name','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_CCG_AUDIT_V','DB_USER',85000,'Db User','DB_USER','','','','SR056655','VARCHAR2','','','Db User','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_CCG_AUDIT_V','OSUSER',85000,'Osuser','OSUSER','','','','SR056655','VARCHAR2','','','Osuser','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_CCG_AUDIT_V','OBJECT_PRIMARY_KEY',85000,'Object Primary Key','OBJECT_PRIMARY_KEY','','','','SR056655','VARCHAR2','','','Object Primary Key','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_CCG_AUDIT_V','OBJECT_PRIMARY_DISPLAY',85000,'Object Primary Display','OBJECT_PRIMARY_DISPLAY','','','','SR056655','VARCHAR2','','','Object Primary Display','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_CCG_AUDIT_V','PRIMARY_KEY_VALUE',85000,'Primary Key Value','PRIMARY_KEY_VALUE','','','','SR056655','VARCHAR2','','','Primary Key Value','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_CCG_AUDIT_V','PRIMARY_KEY_DISPLAY',85000,'Primary Key Display','PRIMARY_KEY_DISPLAY','','','','SR056655','VARCHAR2','','','Primary Key Display','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_CCG_AUDIT_V','COLUMN_DESC',85000,'Column Desc','COLUMN_DESC','','','','SR056655','VARCHAR2','','','Column Desc','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_CCG_AUDIT_V','OLD_DISPLAY',85000,'Old Display','OLD_DISPLAY','','','','SR056655','VARCHAR2','','','Old Display','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_CCG_AUDIT_V','NEW_DISPLAY',85000,'New Display','NEW_DISPLAY','','','','SR056655','VARCHAR2','','','New Display','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_CCG_AUDIT_V','CHANGED_FLAG',85000,'Changed Flag','CHANGED_FLAG','','','','SR056655','VARCHAR2','','','Changed Flag','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_CCG_AUDIT_V','PRIMARY_KEY_DESCRIPTION',85000,'Primary Key Description','PRIMARY_KEY_DESCRIPTION','','','','SR056655','VARCHAR2','','','Primary Key Description','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_CCG_AUDIT_V','OBJ_PK_DESCRIPTION',85000,'Obj Pk Description','OBJ_PK_DESCRIPTION','','','','SR056655','VARCHAR2','','','Obj Pk Description','','','','US','');
--Inserting Object Components for EIS_XXWC_CCG_AUDIT_V
--Inserting Object Component Joins for EIS_XXWC_CCG_AUDIT_V
ELSE
RAISE_APPLICATION_ERROR (-20001,'Module does not exist. Import/Create the module with id 85000');
END IF;
END;
/
prompt Creating Report LOV Data for CCG EBS Audit Report - WC
set scan off define off
DECLARE
mod_exist VARCHAR2(1);
BEGIN 
mod_exist := XXEIS.EIS_RSC_INSERT.check_if_module_exist(85000);
IF mod_exist = 'Y' THEN 
--Inserting Report LOVs - CCG EBS Audit Report - WC
xxeis.eis_rsc_ins.lov( 85000,'SELECT  AO.CHANGE_OBJECT_ID
FROM TICK_CHG_TRACK_OBJECTS@EBS2CCG AO','','CCG Change Object ID LOV','CCG Change Object ID LOV','SA059956',NULL,'N','','','N','N','','','US');
null;
ELSE
RAISE_APPLICATION_ERROR (-20001,'Module does not exist. Import/Create the module with id 85000');
END IF;
END;
/
prompt Creating Report Data for CCG EBS Audit Report - WC
set scan off define off
DECLARE
mod_exist varchar2(1);
BEGIN 
mod_exist := XXEIS.EIS_RSC_INSERT.check_if_module_exist(85000);
IF mod_exist = 'Y' THEN 
--Deleting Report data - CCG EBS Audit Report - WC
xxeis.eis_rsc_utility.delete_report_rows( 'CCG EBS Audit Report - WC',85000 );
--Inserting Report - CCG EBS Audit Report - WC
xxeis.eis_rsc_ins.r( 85000,'CCG EBS Audit Report - WC','','CCG EBS Audit Report - WC','','','','SR056655','EIS_XXWC_CCG_AUDIT_V','Y','','','SR056655','','N','WC Audit Reports','','CSV,EXCEL,','N','','','','','','N','','US','','','','','','','','','','','','','','','','');
--Inserting Report Columns - CCG EBS Audit Report - WC
xxeis.eis_rsc_ins.rc( 'CCG EBS Audit Report - WC',85000,'APP_USER_NAME','App User Name','App User Name','','','default','','1','','Y','','','','','','','SR056655','N','N','','EIS_XXWC_CCG_AUDIT_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'CCG EBS Audit Report - WC',85000,'AUDIT_DATE','Audit Date','Audit Date','','','default','','2','','Y','','','','','','','SR056655','N','N','','EIS_XXWC_CCG_AUDIT_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'CCG EBS Audit Report - WC',85000,'AUDIT_TYPE','Audit Type','Audit Type','','','default','','3','','Y','','','','','','','SR056655','N','N','','EIS_XXWC_CCG_AUDIT_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'CCG EBS Audit Report - WC',85000,'CHANGE_OBJECT_ID','Change Object ID','Change Object Id','','~~~','default','','4','','Y','','','','','','','SR056655','N','N','','EIS_XXWC_CCG_AUDIT_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'CCG EBS Audit Report - WC',85000,'CHANGED_FLAG','Changed Flag','Changed Flag','','','default','','5','','Y','','','','','','','SR056655','N','N','','EIS_XXWC_CCG_AUDIT_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'CCG EBS Audit Report - WC',85000,'COLUMN_DESC','Column Desc','Column Desc','','','default','','6','','Y','','','','','','','SR056655','N','N','','EIS_XXWC_CCG_AUDIT_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'CCG EBS Audit Report - WC',85000,'DB_USER','DB User','Db User','','','default','','7','','Y','','','','','','','SR056655','N','N','','EIS_XXWC_CCG_AUDIT_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'CCG EBS Audit Report - WC',85000,'NEW_DISPLAY','New Display','New Display','','','default','','8','','Y','','','','','','','SR056655','N','N','','EIS_XXWC_CCG_AUDIT_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'CCG EBS Audit Report - WC',85000,'OBJ_PK_DESCRIPTION','Obj Pk Description','Obj Pk Description','','','default','','9','','Y','','','','','','','SR056655','N','N','','EIS_XXWC_CCG_AUDIT_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'CCG EBS Audit Report - WC',85000,'OBJECT_NAME','Object Name','Object Name','','','default','','10','','Y','','','','','','','SR056655','N','N','','EIS_XXWC_CCG_AUDIT_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'CCG EBS Audit Report - WC',85000,'OBJECT_PRIMARY_DISPLAY','Object Primary Display','Object Primary Display','','','default','','11','','Y','','','','','','','SR056655','N','N','','EIS_XXWC_CCG_AUDIT_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'CCG EBS Audit Report - WC',85000,'OBJECT_PRIMARY_KEY','Object Primary Key','Object Primary Key','','','default','','12','','Y','','','','','','','SR056655','N','N','','EIS_XXWC_CCG_AUDIT_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'CCG EBS Audit Report - WC',85000,'OLD_DISPLAY','Old Display','Old Display','','','default','','13','','Y','','','','','','','SR056655','N','N','','EIS_XXWC_CCG_AUDIT_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'CCG EBS Audit Report - WC',85000,'OSUSER','OSuser','Osuser','','','default','','14','','Y','','','','','','','SR056655','N','N','','EIS_XXWC_CCG_AUDIT_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'CCG EBS Audit Report - WC',85000,'OWNING_APPLICATION_NAME','Owning Application Name','Owning Application Name','','','default','','15','','Y','','','','','','','SR056655','N','N','','EIS_XXWC_CCG_AUDIT_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'CCG EBS Audit Report - WC',85000,'PRIMARY_KEY_DESCRIPTION','Primary Key Description','Primary Key Description','','','default','','16','','Y','','','','','','','SR056655','N','N','','EIS_XXWC_CCG_AUDIT_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'CCG EBS Audit Report - WC',85000,'PRIMARY_KEY_DISPLAY','Primary Key Display','Primary Key Display','','','default','','17','','Y','','','','','','','SR056655','N','N','','EIS_XXWC_CCG_AUDIT_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'CCG EBS Audit Report - WC',85000,'PRIMARY_KEY_VALUE','Primary Key Value','Primary Key Value','','','default','','18','','Y','','','','','','','SR056655','N','N','','EIS_XXWC_CCG_AUDIT_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'CCG EBS Audit Report - WC',85000,'RESPONSIBILITY_NAME','Responsibility Name','Responsibility Name','','','default','','19','','Y','','','','','','','SR056655','N','N','','EIS_XXWC_CCG_AUDIT_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'CCG EBS Audit Report - WC',85000,'TABLE_DESC','Table Desc','Table Desc','','','default','','20','','Y','','','','','','','SR056655','N','N','','EIS_XXWC_CCG_AUDIT_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'CCG EBS Audit Report - WC',85000,'TABLE_NAME','Table Name','Table Name','','','','','21','','Y','','','','','','','SR056655','N','N','','EIS_XXWC_CCG_AUDIT_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'CCG EBS Audit Report - WC',85000,'TARGET_NAME','Target Name','Target Name','','','','','22','','Y','','','','','','','SR056655','N','N','','EIS_XXWC_CCG_AUDIT_V','','','','US','','');
--Inserting Report Parameters - CCG EBS Audit Report - WC
xxeis.eis_rsc_ins.rp( 'CCG EBS Audit Report - WC',85000,'Audit Date From','Audit Date From','AUDIT_DATE','>=','','','DATE','Y','Y','1','N','Y','CONSTANT','SR056655','Y','N','','Start Date','','EIS_XXWC_CCG_AUDIT_V','','','US','');
xxeis.eis_rsc_ins.rp( 'CCG EBS Audit Report - WC',85000,'Audit Date To','Audit Date To','AUDIT_DATE','<=','','','DATE','Y','Y','2','N','Y','CONSTANT','SR056655','Y','','','End Date','','EIS_XXWC_CCG_AUDIT_V','','','US','');
xxeis.eis_rsc_ins.rp( 'CCG EBS Audit Report - WC',85000,'Change Object ID','Change Object Id','CHANGE_OBJECT_ID','IN','CCG Change Object ID LOV','','NUMBER','N','Y','3','Y','Y','CONSTANT','SR056655','Y','N','','','','EIS_XXWC_CCG_AUDIT_V','','','US','');
--Inserting Dependent Parameters - CCG EBS Audit Report - WC
--Inserting Report Conditions - CCG EBS Audit Report - WC
xxeis.eis_rsc_ins.rcnh( 'CCG EBS Audit Report - WC',85000,'EXCAV.AUDIT_DATE >= Audit Date From','SIMPLE','','','Y','','1');
xxeis.eis_rsc_ins.rcnd( '','','AUDIT_DATE','','Audit Date From','','','','','EIS_XXWC_CCG_AUDIT_V','','','','','','GREATER_THAN_EQUALS','Y','Y','','','','','1',85000,'CCG EBS Audit Report - WC','EXCAV.AUDIT_DATE >= Audit Date From');
xxeis.eis_rsc_ins.rcnh( 'CCG EBS Audit Report - WC',85000,'EXCAV.AUDIT_DATE <= Audit Date To','SIMPLE','','','Y','','2');
xxeis.eis_rsc_ins.rcnd( '','','AUDIT_DATE','','Audit Date To','','','','','EIS_XXWC_CCG_AUDIT_V','','','','','','LESS_THAN_EQUALS','Y','Y','','','','','1',85000,'CCG EBS Audit Report - WC','EXCAV.AUDIT_DATE <= Audit Date To');
xxeis.eis_rsc_ins.rcnh( 'CCG EBS Audit Report - WC',85000,'EXCAV.CHANGE_OBJECT_ID IN Change Object Id','SIMPLE','','','Y','','3');
xxeis.eis_rsc_ins.rcnd( '','','CHANGE_OBJECT_ID','','Change Object ID','','','','','EIS_XXWC_CCG_AUDIT_V','','','','','','IN','Y','Y','','','','','1',85000,'CCG EBS Audit Report - WC','EXCAV.CHANGE_OBJECT_ID IN Change Object Id');
--Inserting Report Sorts - CCG EBS Audit Report - WC
xxeis.eis_rsc_ins.rs( 'CCG EBS Audit Report - WC',85000,'CHANGE_OBJECT_ID','ASC','SR056655','1','');
xxeis.eis_rsc_ins.rs( 'CCG EBS Audit Report - WC',85000,'AUDIT_DATE','ASC','SR056655','2','');
--Inserting Report Triggers - CCG EBS Audit Report - WC
--inserting report templates - CCG EBS Audit Report - WC
--Inserting Report Portals - CCG EBS Audit Report - WC
--inserting report dashboards - CCG EBS Audit Report - WC
--Exporting report views table information -  
xxeis.eis_rsc_ins.rviews( 'CCG EBS Audit Report - WC','85000','EIS_XXWC_CCG_AUDIT_V','EIS_XXWC_CCG_AUDIT_V','N','');
--inserting report security - CCG EBS Audit Report - WC
xxeis.eis_rsc_ins.rsec( 'CCG EBS Audit Report - WC','20004','','XXEIS_RSC_EXPRESS_ADMIN',85000,'SR056655','','','');
xxeis.eis_rsc_ins.rsec( 'CCG EBS Audit Report - WC','','10012196','',85000,'SR056655','','N','');
--Inserting Report Pivots - CCG EBS Audit Report - WC
--Inserting Report Distribution Details 
--Inserting Report Distribution Details 
--Inserting Report SubTotals Details 
--Inserting Report aggregations 
--Inserting Report   Version details- CCG EBS Audit Report - WC
xxeis.eis_rsc_ins.rv( 'CCG EBS Audit Report - WC','','CCG EBS Audit Report - WC','SA059956','19-JUN-2018');
ELSE
RAISE_APPLICATION_ERROR (-20001,'Module does not exist. Import/Create the module with id 85000');
END IF;
END;
/
