/******************************************************************************
   NAME:       XXWC_OM_SO_LINE_PICK_AUD_TBL.sql
   PURPOSE:    Table to store line level info for pick slip print log
   
   REVISIONS:
   Ver        Date        Author               Description
   ---------  ----------  ---------------  ------------------------------------
   1.0        02/05/2017  Niraj K Ranjan   Initial Version TMS#20160815-00078   
                                           Branch Visit - Pick Ticket Reprint Improvements
******************************************************************************/
CREATE TABLE XXWC.XXWC_OM_SO_LINE_PICK_AUD_TBL
(
  HDR_SEQUENCE_ID        NUMBER,
  HEADER_ID              NUMBER,
  LINE_ID                NUMBER,
  SHIPPING_INSTRUCTIONS  VARCHAR2(2000 BYTE),
  SHIP_FROM_ORG_ID       NUMBER,
  REQUEST_DATE           DATE,
  SHIPPING_METHOD_CODE   VARCHAR2(30 BYTE),
  FLOW_STATUS_CODE       VARCHAR2(30 BYTE),
  ORDERED_QUANTITY       NUMBER,
  CANCELLED_QUANTITY     NUMBER,
  CREATION_DATE          DATE,
  CREATED_BY             NUMBER,
  LAST_UPDATE_DATE       DATE,
  LAST_UPDATED_BY        NUMBER,
  LINE_NUMBER            NUMBER,
  SHIPMENT_NUMBER        NUMBER
);