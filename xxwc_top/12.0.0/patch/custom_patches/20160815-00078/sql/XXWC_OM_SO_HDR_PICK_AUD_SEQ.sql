/******************************************************************************
   NAME:       XXWC_OM_SO_HDR_PICK_AUD_SEQ.sql
   PURPOSE:    sequence

   REVISIONS:
   Ver        Date        Author               Description
   ---------  ----------  ---------------  ------------------------------------
   1.0        02/05/2017  Niraj K Ranjan   Initial Version TMS#20160815-00078   
                                           Branch Visit - Pick Ticket Reprint Improvements
******************************************************************************/
CREATE SEQUENCE  "XXWC"."XXWC_OM_SO_HDR_PICK_AUD_SEQ"  
MINVALUE 1 
MAXVALUE 9999999999999999999999999999
INCREMENT BY 1 
START WITH 61 
CACHE 20 
NOORDER  NOCYCLE ;
