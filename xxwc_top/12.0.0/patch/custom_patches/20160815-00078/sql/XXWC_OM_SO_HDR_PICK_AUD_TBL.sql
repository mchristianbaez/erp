/******************************************************************************
   NAME:       XXWC_OM_SO_HDR_PICK_AUD_TBL.sql
   PURPOSE:    Table to store header level info for pick slip print log
   
   REVISIONS:
   Ver        Date        Author               Description
   ---------  ----------  ---------------  ------------------------------------
   1.0        02/05/2017  Niraj K Ranjan   Initial Version TMS#20160815-00078   
                                           Branch Visit - Pick Ticket Reprint Improvements
******************************************************************************/
CREATE TABLE XXWC.XXWC_OM_SO_HDR_PICK_AUD_TBL
(
  SEQUENCE_ID            NUMBER,
  HEADER_ID              NUMBER,
  SHIPPING_INSTRUCTIONS  VARCHAR2(2000 BYTE),
  SHIP_FROM_ORG_ID       NUMBER,
  REQUEST_DATE           DATE,
  FRIEIGHT_CARRIER_CODE  VARCHAR2(30 BYTE),
  SOLD_TO_CONTACT_ID     NUMBER,
  CREATION_DATE          DATE,
  CREATED_BY             NUMBER,
  LAST_UPDATE_DATE       DATE,
  LAST_UPDATED_BY        NUMBER,
  PRINTER                VARCHAR2(100 BYTE),
  COPIES                 NUMBER,
  PRINT_ORG_ID           NUMBER  
);