CREATE OR REPLACE PACKAGE BODY APPS.xxwc_ont_routines_pkg
AS
/*************************************************************************
  $Header xxwc_ont_routines_pkg$
  Module Name: xxwc_ont_routines_pkg.pkb

  PURPOSE:   This package will contain all Order Management specific routines

  REVISIONS:
  Ver        Date        Author                     Description
  ---------  ----------  ---------------         -------------------------
  1.0        5/9/2012     LCG Consulting           Initial Version
  2.0        8/30/2013    Ram Talluri              Task ID: 20130830-00571 added Parameter P_INCL_HAWAII_BRANCHES
  3.0        9/5/2013     Ram Talluri              Task ID: 20130830-00571 added Parameter P_INCL_HAWAII_BRANCHES - Removed LCG copy right comment
  4.0        9/11/2013    Gopi Damuluri            TMS# 20130904-00928 "Price Change Hold" is included for BILL ONLY and CREDIT ONLY line types of 
                                                   COUNTER and RETURN ORDER Types.
  5.0        2/2/2014     Gopi Damuluri            TMS# 20130912-00853
                                                   Performance improvement for "XXWC: OM Procedure to Release Pricing Change Hold" process.
  5.1        13/2/2014    Gopi Damuluri            TMS# 20140213-00147
                                                   Confine APPLY_PRICE_CHANGE_HOLD proc to only Standard Orders and Counter Orders
  5.2        10/27/2014   Pattabhi Avula           TMS# 20141002-00060  -- Canada OU Changes done 
  5.3        12/29/2014   Gopi Damuluri            TMS# 20141229-00096 - Resolve Incorrect Profile Values for Multi-Org changes.
  5.4        01/27/2015   Pattabhi Avula           TMS# 20150202-00030 - @PERF: wf:OEOH   -- Performance issue code changes
  5.5        02/09/2015   Gopi Damuluri            TMS# 20150210-00007 Performance Tuning - XXWC_ONT_ROUTINES_PKG.CHECK_PRINT_PRICE
                                                   TMS# 20141119-00027 Performance Tuning - XXWC_ONT_ROUTINES_PKG.COUNT_ORDER_HOLDS
  7.0        02/19/2016   Rakesh Patel             TMS# 20151117-00031 Pick ticket printing for Zero quantities 
  8.0        03/09/2016   Manjula Chellappan       TMS# 20150622-00156 Shipping Extension modification for PUBD
  9.0        06/01/2016   Gopi Damuluri            TMS# 20150820-00132 DMS - Impact on Packing Slip (Trigger only one copy)
  10.0       01/23/2017   Rakesh Patel             TMS# 20161116-00261 User item description not updated based on the line status   
  11.0       03/30/2017   P.Vamshidhar             TMS#20170330-00413 - Sales Receipt Modification to add group id 
  12.0       04/28/2017   Niraj K Ranjan           TMS#20160815-00078   Branch Visit - Pick Ticket Reprint Improvements
**************************************************************************/

g_pkg_name        VARCHAR2(100):='XXWC_ONT_ROUTINES_PKG';  -- Version# 5.4, TMS# 20150202-00030

   -- add debug message to log table and concurrent log file
   -- This package will contain all Order Management specific routines

   /*************************************************************************
     *   Procedure : GET_ORDER_TOTAL
     *
     *   PURPOSE:   This procedure return order total excluding Freight and Tzxes
     *   Parameter:
     *          IN
     *              p_Header_id       --  SO Header ID
     * ************************************************************************/
   FUNCTION get_order_total (p_header_id IN NUMBER)
      RETURN NUMBER
   IS
      l_order_sum   NUMBER;

      -- Satish U: 28-NOV-2011 : Modified Query
      CURSOR get_sum_order_lines_cur (l_header_id NUMBER)
      IS
         SELECT SUM (  unit_selling_price
                     * (ordered_quantity - cancelled_quantity)
                     * DECODE (line_category_code, 'ORDER', 1, -1)
                    )
           FROM apps.oe_order_lines
          WHERE header_id = l_header_id;
   BEGIN 
    --fnd_file.put_line(fnd_file.log, 'Before open '||p_header_id);
      OPEN get_sum_order_lines_cur (p_header_id);

      FETCH get_sum_order_lines_cur
       INTO l_order_sum;

      CLOSE get_sum_order_lines_cur;
     --INSERT INTO XX_TEST VALUES (P_HEADER_ID, l_order_sum);
      RETURN l_order_sum;
   EXCEPTION
      WHEN OTHERS
      THEN
         RETURN NULL;
   END get_order_total;

   /*************************************************************************
    *   Procedure : GET_PO_REQD_FLAG
    *
    *   PURPOSE:   This procedure return DFF Attribute3 from Customer Account Sites,
    *              If PO is required the attribute will have Y value
    *   Parameter:
    *          IN
    *              p_bill_to_site_use_id       -- Bill to Site Use Id
    *              p_ship_to_site_use_id       -- Ship TO Site Use Id
    * ************************************************************************/
   FUNCTION get_po_reqd_flag (
      p_bill_to_site_use_id   IN   NUMBER,
      p_ship_to_site_use_id   IN   NUMBER
   )
      RETURN VARCHAR2
   IS
      l_po_reqd_flag   VARCHAR2 (1);
   BEGIN
      fnd_log.STRING (c_level_statement,
                      'GET_PO_REQD_FLAG',
                      'p_ship_to_site_use_id=>' || p_ship_to_site_use_id
                     );
      fnd_log.STRING (c_level_statement,
                      'GET_PO_REQD_FLAG',
                      'p_bill_to_site_use_id=>' || p_bill_to_site_use_id
                     );

      IF p_ship_to_site_use_id IS NOT NULL
      THEN
         fnd_log.STRING (c_level_statement,
                         'GET_PO_REQD_FLAG',
                         'ship to loop=>'
                        );

         BEGIN
            SELECT NVL (hcas.attribute3, 'N')
              INTO l_po_reqd_flag
              FROM apps.hz_cust_acct_sites hcas, apps.hz_cust_site_uses hcsu
             WHERE hcsu.site_use_id = p_ship_to_site_use_id
               AND hcsu.site_use_code = 'SHIP_TO'
               AND hcsu.cust_acct_site_id = hcas.cust_acct_site_id;

            fnd_log.STRING (c_level_statement,
                            'GET_PO_REQD_FLAG',
                            'l_po_reqd_flag ship to =>' || l_po_reqd_flag
                           );

            IF l_po_reqd_flag = 'Y'
            THEN
               RETURN l_po_reqd_flag;
            END IF;
         EXCEPTION
            WHEN OTHERS
            THEN
               l_po_reqd_flag := 'N';
         END;
      END IF;

      IF p_bill_to_site_use_id IS NOT NULL
      THEN
         fnd_log.STRING (c_level_statement,
                         'GET_PO_REQD_FLAG',
                         'bill to loop=>'
                        );

         BEGIN
            SELECT NVL (hcas.attribute3, 'N')
              INTO l_po_reqd_flag
              FROM apps.hz_cust_acct_sites hcas, apps.hz_cust_site_uses hcsu
             WHERE hcsu.site_use_id = p_bill_to_site_use_id
               AND hcsu.site_use_code = 'BILL_TO'
               AND hcsu.cust_acct_site_id = hcas.cust_acct_site_id;

            fnd_log.STRING (c_level_statement,
                            'GET_PO_REQD_FLAG',
                            'l_po_reqd_flag bill to=>' || l_po_reqd_flag
                           );

            IF l_po_reqd_flag = 'Y'
            THEN
               RETURN l_po_reqd_flag;
            END IF;
         EXCEPTION
            WHEN OTHERS
            THEN
               l_po_reqd_flag := 'N';
         END;
      END IF;

      RETURN l_po_reqd_flag;
   EXCEPTION
      WHEN OTHERS
      THEN
         fnd_log.STRING (c_level_statement,
                         'GET_PO_REQD_FLAG',
                         'error=>' || SQLERRM
                        );
         RETURN 'N';
   END get_po_reqd_flag;

   /*************************************************************************
    *   Procedure : GET_SERIAL_CONTROL_CODE
    *
    *   PURPOSE:   This procedure return Serial Control Code value for an Item and organization
    *   Parameter:
    *          IN
    *              p_inventory_item_id       -- Inventory Item Id
    *              p_ship_to_site_use_id       -- Ship TO Site Use Id
    * ************************************************************************/
   FUNCTION get_serial_control_code (
      p_inventory_item_id   IN   NUMBER,
      p_ship_from_org_id    IN   NUMBER
   )
      RETURN NUMBER
   IS
      l_serial_number_control_code   NUMBER;
   BEGIN
      SELECT serial_number_control_code
        INTO l_serial_number_control_code
        FROM mtl_system_items
       WHERE inventory_item_id = p_inventory_item_id
         AND organization_id = p_ship_from_org_id;

      RETURN l_serial_number_control_code;
   EXCEPTION
      WHEN OTHERS
      THEN
         fnd_log.STRING (c_level_statement,
                         'GET_SERIAL_CONTROL_CODE',
                         'error=>' || SQLERRM
                        );
         RETURN NULL;
   END get_serial_control_code;

   /*************************************************************************
    *   Function : GET_LOT_CONTROL_CODE
    *
    *   PURPOSE:   This procedure return Lot Control Code value for an Item and Organization
    *          IN
    *              p_inventory_item_id      -- Inventory Item ID
    *              p_ship_from_org_id       -- Ship From Org ID
    * ************************************************************************/
   FUNCTION get_lot_control_code (
      p_inventory_item_id   IN   NUMBER,
      p_ship_from_org_id    IN   NUMBER
   )
      RETURN NUMBER
   IS
      l_lot_control_code   NUMBER;
   BEGIN
      SELECT lot_control_code
        INTO l_lot_control_code
        FROM mtl_system_items
       WHERE inventory_item_id = p_inventory_item_id
         AND organization_id = p_ship_from_org_id;

      RETURN l_lot_control_code;
   EXCEPTION
      WHEN OTHERS
      THEN
         fnd_log.STRING (c_level_statement,
                         'GET_LOT_CONTROL_CODE',
                         'error=>' || SQLERRM
                        );
         RETURN NULL;
   END get_lot_control_code;

   /*************************************************************************
    *   Function : GET_RESERVATION_QTY
    *
    *   PURPOSE:   This procedure return reserved qty for a sales order line
    *          IN
    *              p_header_id      -- Sales Order Header ID
    *              p_line_id       -- Sales Order Line Id
    * ************************************************************************/
   FUNCTION get_reservation_qty (p_header_id IN NUMBER, p_line_id IN NUMBER)
      RETURN NUMBER
   IS
      l_open_quantity        NUMBER                                      := 0;
      l_reserved_quantity    NUMBER                                      := 0;
      l_mtl_sales_order_id   NUMBER;
      l_return_status        VARCHAR2 (1);
      l_msg_count            NUMBER;
      l_msg_data             VARCHAR2 (240);
      l_rsv_rec              inv_reservation_global.mtl_reservation_rec_type;
      l_rsv_tbl              inv_reservation_global.mtl_reservation_tbl_type;
      l_count                NUMBER;
      l_x_error_code         NUMBER;
      l_lock_records         VARCHAR2 (1);
      l_sort_by_req_date     NUMBER;
      l_converted_qty        NUMBER;
      l_inventory_item_id    NUMBER;
      l_order_quantity_uom   VARCHAR2 (30);
   BEGIN
      l_mtl_sales_order_id :=
           oe_header_util.get_mtl_sales_order_id (p_header_id      => p_header_id);
      l_rsv_rec.demand_source_header_id := l_mtl_sales_order_id;
      l_rsv_rec.demand_source_line_id := p_line_id;
      l_rsv_rec.organization_id := NULL;
      inv_reservation_pub.query_reservation_om_hdr_line
                                    (p_api_version_number             => 1.0,
                                     p_init_msg_lst                   => fnd_api.g_true,
                                     x_return_status                  => l_return_status,
                                     x_msg_count                      => l_msg_count,
                                     x_msg_data                       => l_msg_data,
                                     p_query_input                    => l_rsv_rec,
                                     x_mtl_reservation_tbl            => l_rsv_tbl,
                                     x_mtl_reservation_tbl_count      => l_count,
                                     x_error_code                     => l_x_error_code,
                                     p_lock_records                   => l_lock_records,
                                     p_sort_by_req_date               => l_sort_by_req_date
                                    );

      BEGIN
         SELECT order_quantity_uom, inventory_item_id
           INTO l_order_quantity_uom, l_inventory_item_id
           FROM apps.oe_order_lines
          WHERE line_id = p_line_id;
      EXCEPTION
         WHEN OTHERS
         THEN
            l_order_quantity_uom := NULL;
      END;

      FOR i IN 1 .. l_rsv_tbl.COUNT
      LOOP
         l_rsv_rec := l_rsv_tbl (i);

         IF NVL (l_order_quantity_uom, l_rsv_rec.reservation_uom_code) <>
                                               l_rsv_rec.reservation_uom_code
         THEN
            l_converted_qty :=
               oe_order_misc_util.convert_uom
                                             (l_inventory_item_id,
                                              l_rsv_rec.reservation_uom_code,
                                              l_order_quantity_uom,
                                              l_rsv_rec.reservation_quantity
                                             );
            l_reserved_quantity := l_reserved_quantity + l_converted_qty;
         ELSE
            l_reserved_quantity :=
                         l_reserved_quantity + l_rsv_rec.reservation_quantity;
         END IF;
      END LOOP;

      RETURN l_reserved_quantity;
   EXCEPTION
      WHEN NO_DATA_FOUND
      THEN
         RETURN NULL;
   END;

   /*************************************************************************
     *   Function : GGET_DELVRY_CHARGE_EXEMPT_INFO
     *
     *   PURPOSE:   This procedure returns Delivery Charge  Excempt info : DFF Attribute14
     *   Parameter:
     *          IN
     *              p_Organization_ID       --  Warehouse
     * ************************************************************************/
   FUNCTION get_delvry_charge_exempt_info (p_organization_id IN NUMBER)
      RETURN VARCHAR2
   IS
      l_delivery_charge_exempt_flag   VARCHAR2 (3);
   BEGIN
      SELECT SUBSTR(mp.attribute14,1,1)
        INTO l_delivery_charge_exempt_flag
        FROM mtl_parameters mp
       WHERE mp.organization_id = p_organization_id;

      RETURN NVL (l_delivery_charge_exempt_flag, 'N');
   EXCEPTION
      WHEN OTHERS
      THEN
         RETURN 'N';
   END get_delvry_charge_exempt_info;

   /*************************************************************************
   *   Function : GET_CC_NUMBER
   *
   *   PURPOSE:   This procedure parses the string and return CC number
   *   Parameter:
   *          IN
   *              P_String        --  Credit Card Swiper String
   * ************************************************************************/
   FUNCTION get_cc_number (p_cc_string IN VARCHAR2)
      RETURN VARCHAR2
   IS
      l_cc_number   VARCHAR2 (30);
      l_cc_string   VARCHAR2 (240);
   BEGIN
      SELECT SUBSTR (p_cc_string, 3, INSTR (p_cc_string, CHR (94)) - 3)
        INTO l_cc_number
        FROM DUAL;

      /*Select SubStr(l_CC_String,3,instr(l_CC_String,chr(94)) - 3 )
       Into l_CC_Number
       From Dual;
       */
      RETURN l_cc_number;
   END get_cc_number;

   /*************************************************************************
   *   Function : GET_CC_EXP_DATE
   *
   *   PURPOSE:   This procedure parses the string and return CC Expiration Date value
   *   Parameter:
   *          IN
   *              P_String        --  Credit Card Swiper String
   * ************************************************************************/
   FUNCTION get_cc_exp_date (p_cc_string IN VARCHAR2)
      RETURN VARCHAR2
   IS
      l_cc_string            VARCHAR2 (240);
      --l_CC_Expiration_Date  Varchar2(30) ;
      l_cc_expiration_date   DATE;                                          --
   BEGIN
      --Select SubStr(P_CC_String,instr(P_CC_String,chr(94),1,2) + 1,4 )
      SELECT TO_DATE (TO_CHAR (TO_DATE (SUBSTR (p_cc_string,
                                                  INSTR (p_cc_string,
                                                         CHR (94),
                                                         1,
                                                         2
                                                        )
                                                + 1,
                                                4
                                               ),
                                        'RRMM'
                                       ),
                               'DD-MON-RRRR'
                              )
                     )
        INTO l_cc_expiration_date
        FROM DUAL;

      /**************
      Select TO_CHAR(To_Date(SubStr(l_CC_String,instr(l_CC_String,chr(94),1,2) + 1,4 ),'MMRR'),'MM-RRRR')
      Into l_CC_Expiration_Date
      From Dual;
      **********************/
      RETURN l_cc_expiration_date;
   END get_cc_exp_date;

    /*************************************************************************
   *   Function : GET_CC_HOLDER_NAME
   *
   *   PURPOSE:   This procedure parses the string and return CC Holder Name
   *   Parameter:
   *          IN
   *              P_String        --  Credit Card Swiper String
   * ************************************************************************/
   FUNCTION get_cc_holder_name (p_cc_string IN VARCHAR2)
      RETURN VARCHAR2
   IS
      l_cc_string              VARCHAR2 (240);
      l_cc_holder_name         VARCHAR2 (60);
      l_cc_holder_name_begin   NUMBER;
      l_cc_holder_name_end     NUMBER;
   BEGIN
      l_cc_holder_name_begin := INSTR (p_cc_string, CHR (94), 1, 1) + 1;
      l_cc_holder_name_end := INSTR (p_cc_string, CHR (94), 1, 2) - 1;

      SELECT UPPER (TRIM (SUBSTR (p_cc_string,
                                  l_cc_holder_name_begin,
                                  l_cc_holder_name_end
                                  - l_cc_holder_name_begin
                                 )
                         )
                   )
        INTO l_cc_holder_name
        FROM DUAL;

      RETURN l_cc_holder_name;
   END get_cc_holder_name;

   /*************************************************************************
   *   Function : get_credit_card_type
   *
   *   PURPOSE:   This procedure parses the string and return CC TYpe
   *   Parameter:
   *          IN
   *              P_String        --  Credit Card Swiper String
   * ************************************************************************/
   FUNCTION get_credit_card_type (p_cc_string IN VARCHAR2)
      RETURN VARCHAR2
   IS
      l_cc_type              VARCHAR2 (30);
      c_cc_type_mastercard   VARCHAR2 (30)  := 'MasterCard';
      c_cc_type_visa         VARCHAR2 (30)  := 'Visa';
      c_cc_type_amex         VARCHAR2 (30)  := 'American Express';
      c_cc_type_discover     VARCHAR2 (30)  := 'Discover';
      p_credit_card_number   VARCHAR2 (200);
   BEGIN
      SELECT xxwc_ont_routines_pkg.get_cc_number (p_cc_string)
        INTO p_credit_card_number
        FROM DUAL;

      -- IF SUBSTR (p_credit_card_number, 1, 2) IN
      --                                         ('51', '52', '53', '54', '55')
      IF p_credit_card_number IS NOT NULL
      THEN
         IF     LENGTH (p_credit_card_number) = 16
            AND SUBSTR (p_credit_card_number, 1, 1) = '5'
         THEN
            l_cc_type := c_cc_type_mastercard;
         END IF;

         IF     LENGTH (p_credit_card_number) IN (13, 16)
            AND SUBSTR (p_credit_card_number, 1, 1) IN ('4')
         THEN
            l_cc_type := c_cc_type_visa;
         END IF;

         IF     LENGTH (p_credit_card_number) IN (15)
            AND SUBSTR (p_credit_card_number, 1, 2) IN ('34', '37')
         THEN
            l_cc_type := c_cc_type_amex;
         END IF;

         IF     SUBSTR (p_credit_card_number, 1, 4) IN ('6011')
            AND LENGTH (p_credit_card_number) IN (16)
         THEN
            l_cc_type := c_cc_type_discover;
         END IF;
      END IF;

      RETURN l_cc_type;
   END get_credit_card_type;

     /*************************************************************************
   *   Function : get_credit_card_code
   *
   *   PURPOSE:   This procedure parses the string and return CC Code
   *   Parameter:
   *          IN
   *              P_String        --  Credit Card Swiper String
   * ************************************************************************/
   FUNCTION get_credit_card_code (p_credit_card_number IN VARCHAR2)
      RETURN VARCHAR2
   IS
      c_cc_type_mastercard   VARCHAR2 (30) := 'MASTERCARD';
      c_cc_type_visa         VARCHAR2 (30) := 'VISA';
      c_cc_type_amex         VARCHAR2 (30) := 'AMEX';
      c_cc_type_discover     VARCHAR2 (30) := 'DISCOVER';
   BEGIN
      IF SUBSTR (p_credit_card_number, 1, 2) IN
                                              ('51', '52', '53', '54', '55')
      THEN
         IF LENGTH (p_credit_card_number) = 16
         THEN
            RETURN c_cc_type_mastercard;
         END IF;
      ELSIF SUBSTR (p_credit_card_number, 1, 1) IN ('4')
      THEN
         IF LENGTH (p_credit_card_number) IN (13, 16)
         THEN
            RETURN c_cc_type_visa;
         END IF;
      ELSIF SUBSTR (p_credit_card_number, 1, 2) IN ('34', '37')
      THEN
         IF LENGTH (p_credit_card_number) IN (15)
         THEN
            RETURN c_cc_type_amex;
         END IF;
      ELSIF SUBSTR (p_credit_card_number, 1, 4) IN ('6011')
      THEN
         IF LENGTH (p_credit_card_number) IN (16)
         THEN
            RETURN c_cc_type_discover;
         END IF;
      END IF;

      RETURN NULL;
   END get_credit_card_code;

   /*************************************************************************
   *   Function : get_credit_card_exp_date
   *
   *   PURPOSE:   This procedure parses the string and return CC Expiration Date
   *   Parameter:
   *          IN
   *              P_String        --  Credit Card Swiper String
   * ************************************************************************/
   FUNCTION get_credit_card_exp_date (p_cc_exp_date IN VARCHAR2)
      RETURN VARCHAR2
   IS
      l_expiration_date   VARCHAR2 (30);
      l_exp_date          DATE;
   BEGIN
      IF p_cc_exp_date IS NOT NULL
      THEN
         /***
         Select  Substr(p_CC_Exp_Date,3,2) || '-' ||  TO_Char(To_Date( Substr(P_CC_Exp_Date,1,2), 'RRRR'),'RRRR')
         Into l_Expiration_Date
         From Dual;
         ***/
         SELECT TO_DATE (p_cc_exp_date || '01', 'YYMMDD')
           INTO l_exp_date
           FROM DUAL;

         SELECT TO_CHAR (LAST_DAY (l_exp_date), 'DD-MON-RRRR')
           INTO l_expiration_date
           FROM DUAL;

         RETURN l_expiration_date;
      ELSE
         RETURN NULL;
      END IF;
   END get_credit_card_exp_date;

   /*************************************************************************
   *   Function : Check_Pending_Counter_Sales
   *
   *   PURPOSE:   This procedure is called from WF Node, takes Line_Is as input parameter
   *              and checks if this any pending Material transaction in interface and also checks if
   *              there is any couter sales line for same item and organization that is not fulfilled.
   *
   * ************************************************************************/
   PROCEDURE check_pending_counter_sales (
      itemtype    IN              VARCHAR2,
      itemkey     IN              VARCHAR2,
      actid       IN              NUMBER,
      funcmode    IN              VARCHAR2,
      resultout   IN OUT NOCOPY /* file.sql.39 change */ VARCHAR2
   )
   IS
      l_debug_level                  CONSTANT NUMBER
                                                := oe_debug_pub.g_debug_level;
      l_inventory_item_id                     NUMBER;
      l_organization_id                       NUMBER;
      l_line_id                               NUMBER;
      l_header_id                             NUMBER;
      l_row_count                             NUMBER;
      l_pending_trx_exists                    VARCHAR2 (1);
      l_so_lines_count                        NUMBER;
      l_sol_activity_count                    NUMBER;
      c_transaction_source_type_id   CONSTANT NUMBER       := 2;
      --Sales order
      c_transaction_action_id        CONSTANT NUMBER       := 1;
      --  Issue From Stores
      c_transaction_type_id          CONSTANT NUMBER       := 33;
      l_proc_name                             VARCHAR2(100):='Check_Pending_Counter_Sales';
      l_arg3                                  VARCHAR2(50);
   -- Sales order issue
   BEGIN
      --
      -- RUN mode - normal process execution
      --
      IF l_debug_level > 0
      THEN
         oe_debug_pub.ADD ('Entering Check Wait to Fulfill Line' || itemkey,
                           1
                          );
      END IF;

      IF (funcmode = 'RUN')
      THEN
         oe_standard_wf.set_msg_context (actid);
         
         l_arg3:=TO_CHAR(actid);

         IF itemtype = oe_globals.g_wfi_lin
         THEN
            l_line_id := TO_NUMBER (itemkey);

            -- Get Inventory Item ID and Warehouse Details for a give Sales Order Line
            SELECT inventory_item_id, ship_from_org_id, header_id
              INTO l_inventory_item_id, l_organization_id, l_header_id
              FROM apps.oe_order_lines
             WHERE line_id = l_line_id;

            -- Check if  There are any Pending Transactions in MTL_Transactions Table for a given Item and Warehouse
            SELECT COUNT (*)
              INTO l_row_count
              FROM mtl_transactions_interface
             WHERE inventory_item_id = l_inventory_item_id
               AND organization_id = l_organization_id
               AND transaction_source_type_id = c_transaction_source_type_id
               AND transaction_action_id = c_transaction_action_id
               AND transaction_type_id = c_transaction_type_id
               AND ERROR_CODE IS NULL;

            IF l_row_count > 0
            THEN
               -- Counter Sales Pending Transactions Exist : Go Back To Wait Node
               resultout := 'COMPLETE:N';
               RETURN;
            ELSE
               -- THere are no pending Transactions : Progress Line
               --resultout := 'COMPLETE:Y';
               l_pending_trx_exists := 'N';
            END IF;

            -- Satish U: 08-MAR-2012 :
            IF l_pending_trx_exists = 'N'
            THEN
               -- Get Number of Counter Sales in a GIven Sales ORder
               SELECT COUNT (*)
                 INTO l_so_lines_count
                 FROM wf_item_activity_statuses ias,
                      wf_process_activities pa,
                      apps.oe_order_lines ool
                WHERE ias.item_key = TO_CHAR (ool.line_id)
                  AND ias.item_type = 'OEOL'
                  AND inventory_item_id = l_inventory_item_id
                  AND ias.process_activity = pa.instance_id
                  AND pa.activity_name = 'XXWC_R_BILL_ONLY_INV_INTERFACE'
                  -- And pa.Activity_name = 'XXWC_R_BILL_ONLY_INV_WT_INTERF'
                  AND ool.header_id = l_header_id;

               -- Get Number of Counter Sales  that completed  Inventory Interface Node
               SELECT COUNT (*)
                 INTO l_sol_activity_count
                 FROM wf_item_activity_statuses ias,
                      wf_process_activities pa,
                      apps.oe_order_lines ool
                WHERE ias.item_key = TO_CHAR (ool.line_id)
                  AND ias.item_type = 'OEOL'
                  AND ias.process_activity = pa.instance_id
                  AND inventory_item_id = l_inventory_item_id
                  AND pa.activity_name = 'FULFILL_LINE'
                  --And pa.Activity_name     = 'WC_INVENTORY_INTERFACE'
                  AND ias.activity_status = 'COMPLETE'
                  AND ool.header_id = l_header_id
                  AND EXISTS (
                         SELECT 1
                           FROM wf_process_activities pa2,
                                wf_item_activity_statuses ias2
                          WHERE pa2.activity_name =
                                              'XXWC_R_BILL_ONLY_INV_INTERFACE'
                            AND ias2.process_activity = pa2.instance_id
                            AND pa2.instance_id = pa.instance_id);

               IF l_so_lines_count = l_sol_activity_count
               THEN
                  resultout := 'COMPLETE:Y';
                  RETURN;
               ELSE
                  resultout := 'COMPLETE:N';
                  RETURN;
               END IF;
            END IF;
         ELSE
            RAISE fnd_api.g_exc_unexpected_error;
         END IF;
      ELSE
         -- Order should go thru normal process flow.
         resultout := 'COMPLETE:N';
         RETURN;
      END IF;
   EXCEPTION
      WHEN OTHERS
      THEN
         -- The line below records this function call in the error system
         -- in the case of an exception.
         wf_core.CONTEXT (g_pkg_name, -- 'XXWC_ONT_ROUTINES_PKG', -- Version# 5.4, TMS# 20150202-00030
                          l_proc_name, -- 'Check_Pending_Counter_Sales', -- Version# 5.4, TMS# 20150202-00030
                          itemtype,
                          itemkey,
                          l_arg3,  -- TO_CHAR (actid),-- Version# 5.4, TMS# 20150202-00030
                          funcmode
                         );
         -- start data fix project
         oe_standard_wf.add_error_activity_msg (p_actid         => actid,
                                                p_itemtype      => itemtype,
                                                p_itemkey       => itemkey
                                               );
         -- end data fix project
         oe_standard_wf.save_messages;
         oe_standard_wf.clear_msg_context;

         IF l_debug_level > 0
         THEN
            oe_debug_pub.ADD
                          (   'When Other in Check_Pending_Counter_Sales_Trx'
                           || SQLERRM
                          );
         END IF;

         RAISE;
   END check_pending_counter_sales;

   --- PRocedure Get_Salesrep_Name
   -- Gets Salesrep Person name for a given Site_USE_ID
   --  P_Return_TYpe Can have values 'NAMEReturn Salesrep_Rec.Salesrep_Name ; ' or 'ID'
   -- Function return Salesrep Name if p_RETURN_TYPE is NAME
   -- Function return Salesrep ID if p_RETURN_TYPE is ID
   FUNCTION get_salesrep_info (p_site_use_id NUMBER, p_return_type VARCHAR2)
      RETURN VARCHAR2
   IS
      -- Satish U: 27-May-2012 : Get Salesrep From Primary Bill To Site.
      CURSOR salesrep_cur
      IS
         SELECT jrs.salesrep_id, jrs.resource_id, jrs.person_id, jrs.org_id,
                jrs.salesrep_number,
                NVL (jrs.NAME, papf.full_name) salesrep_name,
                jrs.start_date_active, jrs.end_date_active,
                papf.employee_number
           FROM apps.jtf_rs_salesreps_mo_v jrs,  -- Replaced the "JTF_RS_SALESREPS" table by pattabhi on 10/27/2014 for TMS# 20141002-00060 
                jtf_rs_resource_extns jrre,
                hr.per_all_people_f papf,
                apps.hz_cust_site_uses hcsu,
                apps.hz_cust_acct_sites hcas
          WHERE jrs.resource_id = jrre.resource_id(+)
            AND jrs.person_id = papf.person_id(+)
            AND TRUNC (papf.effective_start_date(+)) <= TRUNC (SYSDATE)
            AND NVL (TRUNC (papf.effective_end_date(+)), TRUNC (SYSDATE)) >=
                                                               TRUNC (SYSDATE)
            AND hcsu.site_use_id = p_site_use_id
            AND hcsu.cust_acct_site_id = hcas.cust_acct_site_id
            AND jrs.salesrep_id =
                   xxwc_cust_credit_limit_pkg.get_salesrep_id
                                                         (hcas.cust_account_id);

      --And  hcsu.PRIMARY_SALESREP_ID = jrs.Salesrep_ID ;
      l_salesrep_name   VARCHAR2 (80);
   BEGIN
      FOR salsrep_rec IN salesrep_cur
      LOOP
         IF p_return_type = 'NAME'
         THEN
            RETURN salsrep_rec.salesrep_name;
         ELSE
            RETURN salsrep_rec.salesrep_id;
         END IF;
      END LOOP;

      RETURN NULL;
   EXCEPTION
      WHEN OTHERS
      THEN
         RETURN NULL;
   END get_salesrep_info;

    -- 05/03/2012 CGonzalez
   -- Get Credit Card Brand for Order Payment Form
   FUNCTION get_credit_card_brand (p_cc_string IN VARCHAR2)
      RETURN VARCHAR2
   IS
      l_cc_number       VARCHAR2 (240);
      l_card_type       VARCHAR2 (240);
      l_string_length   NUMBER;
      l_found_type      VARCHAR2 (1);

      CURSOR cur_credit_card_types (p_length NUMBER)
      IS
         SELECT DISTINCT (icir.card_issuer_code) card_issuer_code,
                         icit.card_issuer_name, icir.card_number_length,
                         LENGTH (icir.card_number_prefix) pref_length
                    FROM iby_creditcard_issuers_b icib,
                         iby_creditcard_issuers_tl icit,
                         iby_cc_issuer_ranges icir
                   WHERE icib.card_issuer_accepted_flag = 'Y'
                     AND icib.card_issuer_code = icit.card_issuer_code
                     AND icib.card_issuer_code = icir.card_issuer_code
                     AND icir.card_number_length = p_length
                ORDER BY 2, 3;

      CURSOR cur_credit_card_ranges (
         p_card_code         VARCHAR2,
         p_card_num_length   NUMBER,
         p_pref_length       NUMBER
      )
      IS
         SELECT *
           FROM iby_cc_issuer_ranges
          WHERE card_issuer_code = p_card_code
            AND card_number_length = p_card_num_length
            AND LENGTH (card_number_prefix) = p_pref_length;
   BEGIN
      SELECT REPLACE(xxwc_ont_routines_pkg.get_cc_number (p_cc_string), CHR(32),NULL)
        INTO l_cc_number
        FROM DUAL;

      IF l_cc_number IS NOT NULL
      THEN
         l_string_length := NULL;

         BEGIN
            SELECT LENGTH (l_cc_number)
              INTO l_string_length
              FROM DUAL;
         EXCEPTION
            WHEN OTHERS
            THEN
               l_string_length := 0;
         END;

         --dbms_output.put_line ('l_string_length '|| l_string_length);
         IF l_string_length > 0
         THEN
            l_found_type := NULL;
            l_card_type := NULL;

            FOR c1 IN cur_credit_card_types (l_string_length)
            LOOP
               EXIT WHEN cur_credit_card_types%NOTFOUND;

               --dbms_output.put_line ('card_issuer_code '||c1.card_issuer_code);
               --dbms_output.put_line ('card_number_length '||c1.card_number_length);
               --dbms_output.put_line ('card_number_length '||c1.pref_length);
               FOR c2 IN cur_credit_card_ranges (c1.card_issuer_code,
                                                 c1.card_number_length,
                                                 c1.pref_length
                                                )
               LOOP
                  EXIT WHEN cur_credit_card_ranges%NOTFOUND;

                  --dbms_output.put_line ('Card Number prefix '||c2.card_number_prefix);
                  IF c2.card_number_prefix =
                                      SUBSTR (l_cc_number, 1, c1.pref_length)
                  THEN
                     l_card_type := c1.card_issuer_name;
                     l_found_type := 'Y';
                     EXIT;
                  ELSE
                     l_found_type := 'N';
                  END IF;
               END LOOP;

               IF l_found_type = 'Y'
               THEN
                  EXIT;
               END IF;
            END LOOP;
         ELSE
            l_card_type := NULL;
         END IF;
      END IF;

      RETURN l_card_type;
   EXCEPTION
      WHEN OTHERS
      THEN
         RETURN NULL;
   END get_credit_card_brand;

   /*****************************************************************************************************************************
     Procedure progress_so_lines
     Purpose   :  Procedure is called from Concurrent Program to progress Sales Order lines that are waiting at
                  'WC Wait Until Progressed' sub process.
     Parameters  :
     P_HEADER_ID    : If User passes parameter  then program will look for all Sales Order lines with in given Sales Order Header,
                      else it will look for all Sales Order lines that are waiting at sub process 'WC Wait Until Progressed'
     Change History :
     Resource Name    : Change Date          Change Purpose and change description
     -------------      ------------         --------------------------------------
     Satish U           21-DEC-2012          TMS # 20121214-00966   : Defined Constant for Process name, as we have two sub process with same name.
     Satish U           31-JAN-2013          MS# 20130201-01025     : Code was modified to consider SO lines in Cancelled status too.
   **********************************************************************************************************************************
   -- Following Procredure is called to Progress Sales Order Lines that are waiting for external pgograms to be called to pgoress them
   -- If User Passes value for P_Header_ID then its SO LInes that are waiting to progressed will be progressed Else  SO Lines of All the Orders will be progressed
   **********************************************************************************************************************************/
   PROCEDURE progress_so_lines (
      errbuf        OUT      VARCHAR2,
      retcode       OUT      VARCHAR2,
      p_header_id   IN       NUMBER DEFAULT NULL
   )
   IS
   
                     
     ----  Satish U: 12/21/2012 : TMS # 20121214-00966   Defined Constant for Process Name
      -- Satish U : 20130201-01025  : JAN-31-2013 : Process Cancelled Lines too.
     C_PROCESS_NAME  Constant   Varchar2(30) := 'WC_WAIT_UNTIL_PROGRESSED';
     l_activity                 VARCHAR2(80):='XXWC_WAIT_UNTIL_PROGRESSED'; -- Version# 5.4, TMS# 20150202-00030
     l_line_id                  VARCHAR2(50); -- Version# 5.4, TMS# 20150202-00030
      CURSOR order_cur
      IS
         SELECT   ool.line_id, ool.header_id
             FROM apps.oe_order_lines ool
            WHERE ool.line_category_code = 'ORDER'
            --AND ool.cancelled_flag = 'N' SatishU: 31-JAN-2013  TMS# 20130201-01025
            --AND ool.ordered_quantity > 0 SatishU: 31-JAN-2013  TMS# 20130201-01025
            AND (p_header_id IS NULL OR ool.header_id = p_header_id)
            AND EXISTS (
                     SELECT 'exists'
                       FROM wf_item_activity_statuses a,
                            wf_process_activities b
                      WHERE a.item_type = 'OEOL'
                        AND a.process_activity = b.instance_id
                        AND b.activity_name = 'XXWC_WAIT_UNTIL_PROGRESSED'
                        AND b.Process_Name  = C_PROCESS_NAME
                        AND activity_status <> 'COMPLETE'
                        AND TO_NUMBER (a.item_key) = ool.line_id)
         ORDER BY ool.line_number;

      l_api_name       VARCHAR2 (30) := 'Progress_SO_Lines';
      l_distro_list    VARCHAR2 (75)
                                    DEFAULT 'HDSOracleDevelopers@hdsupply.com';
      l_package_name   VARCHAR2 (75) DEFAULT 'XXWC_ONT_ROUTINES_PKG';  
   BEGIN
      fnd_file.put_line (fnd_file.LOG, '100 : Begin of API : ' || l_api_name);
      fnd_global.apps_initialize (fnd_global.user_id,
                                  fnd_global.resp_id,
                                  fnd_global.resp_appl_id
                                 );
      fnd_file.put_line (fnd_file.LOG, '110 : Loop Through the Cursor : ');

      FOR i IN order_cur
      LOOP
         BEGIN
         
           l_line_id := TO_CHAR (i.line_id); -- Version# 5.4, TMS# 20150202-00030
            fnd_file.put_line (fnd_file.LOG,
                                  '120 : Complete Activity for SO Line ID : '
                               || i.line_id
                              );
            wf_engine.completeactivity ('OEOL',
                                        l_line_id,  --  TO_CHAR (i.line_id), -- Version# 5.4, TMS# 20150202-00030
                                        l_activity,  -- 'XXWC_WAIT_UNTIL_PROGRESSED', -- Version# 5.4, TMS# 20150202-00030
                                        NULL
                                       );
         EXCEPTION
            WHEN OTHERS
            THEN
               retcode := 1;
               fnd_file.put_line
                  (fnd_file.LOG,
                      '150 : When others Exception while completing acivity for SO Line ID  : '
                   || i.line_id
                  );
               fnd_file.put_line (fnd_file.LOG,
                                  '151 : Error Message : ' || SQLERRM
                                 );
         END;
      END LOOP;

      wf_engine.background (itemtype              => 'OEOL',
                            minthreshold          => NULL,
                            maxthreshold          => NULL,
                            process_deferred      => TRUE,
                            process_timeout       => TRUE,
                            process_stuck         => FALSE
                           );
   EXCEPTION
      WHEN OTHERS
      THEN
         retcode := 2;
         errbuf := SQLERRM;
         fnd_file.put_line (fnd_file.LOG, '160 : When others Exception ');
         fnd_file.put_line (fnd_file.LOG,
                            '161 : Error Message : ' || SQLERRM);
         fnd_file.put_line (fnd_file.output, SQLERRM);
         -- Calling ERROR API
         xxcus_error_pkg.xxcus_error_main_api
            (p_called_from            => l_package_name,
             p_calling                => 'Concurrent Program exception for XXWC_ONT_ROUTINES_PKG.Progress_SO_Lines',
             p_request_id             => fnd_global.conc_request_id,
             p_ora_error_msg          => SQLERRM,
             p_error_desc             => 'When others Exception ',
             p_distribution_list      => l_distro_list,
             p_module                 => 'OM'
            );
   END progress_so_lines;


   /*****************************************************************************************************************************
     Procedure progress_Counter_so_lines
     Purpose   :  Procedure is called from Concurrent Program to progress Sales Order lines that are waiting at
                  'WC Wait Until Progressed' sub process.
     Parameters  :
     P_HEADER_ID    : If User passes parameter  then program will look for all Sales Order lines with in given Sales Order Header,
                      else it will look for all Sales Order lines that are waiting at sub process 'WC Wait Until Progressed'
     Change History :
     Resource Name    : Change Date          Change Purpose and change description
     -------------      ------------         --------------------------------------
     Satish U           21-DEC-2012          TMS # 20121214-00966   : Initial extension was written
     Satish U           31-JAN-2013          MS# 20130201-01025     : Code was modified to consider SO lines in Cancelled status too.
   **********************************************************************************************************************************/
   -- Following Procredure is called to Progress Sales Order Lines that are waiting for external pgograms to be called to pgoress them
   -- If User Passes value for P_Header_ID then its SO LInes that are waiting to progressed will be progressed Else  SO Lines of All the Orders will be progressed
   ----  Satish U: 12/21/2012 : TMS # 20121214-00966
   ----Ram Talluri8/30/2013 : Task ID: 20130830-00571 added Parameter P_INCL_HAWAII_BRANCHES and modified cursor query logic to include/exclude Hawaii branches
   PROCEDURE progress_Counter_so_lines (
      errbuf        OUT      VARCHAR2,
      retcode       OUT      VARCHAR2,
      p_header_id   IN       NUMBER DEFAULT NULL,
      P_INCL_HAWAII_BRANCHES IN VARCHAR2    --Ram Talluri8/30/2013 : Task ID: 20130830-00571 added Parameter P_INCL_HAWAII_BRANCHES
   )
   IS

     C_PROCESS_NAME  Constant   Varchar2(30) := 'WC_CSOL_WAIT_UNTIL_PROGRESSED';
     -- Satish U : 20130201-01025  : JAN-31-2013 : Process Cancelled Lines too.
     
     CURSOR order_cur
     IS
     SELECT   ool.line_id, ool.header_id
         FROM apps.oe_order_lines ool
         WHERE ool.line_category_code = 'ORDER'
         --AND ool.cancelled_flag = 'N'  SatishU: 31-JAN-2013  TMS# 20130201-01025
         --AND ool.ordered_quantity > 0  SatishU: 31-JAN-2013  TMS# 20130201-01025
         AND (p_header_id IS NULL OR ool.header_id = p_header_id)
         AND EXISTS (
             SELECT 'exists'
             FROM wf_item_activity_statuses a,
                  wf_process_activities b
             WHERE a.item_type = 'OEOL'
             AND a.process_activity = b.instance_id
             AND b.activity_name = 'XXWC_WAIT_UNTIL_PROGRESSED'
             AND b.Process_Name  = C_PROCESS_NAME
             --AND activity_status <> 'COMPLETE'--Commented by Ram Talluri8/30/2013 : Task ID: 20130830-00571
             AND activity_status NOT IN ('COMPLETE')--Added by Ram Talluri8/30/2013 : Task ID: 20130830-00571
             -- AND TO_NUMBER (a.item_key) = ool.line_id--Commented by --Ram Talluri8/30/2013 : Task ID: 20130830-00571
             AND a.item_key = TO_CHAR(ool.line_id));--Added by Ram Talluri8/30/2013 : Task ID: 20130830-00571
         
      
      CURSOR non_hawaii_order_cur  --Cursor added by Ram Talluri8/30/2013 : Task ID: 20130830-00571 
      IS
         SELECT   ool.line_id, ool.header_id
         FROM apps.oe_order_lines ool
         WHERE ool.line_category_code = 'ORDER'
         AND (p_header_id IS NULL OR ool.header_id = p_header_id)
         AND EXISTS (
             SELECT 'exists'
             FROM wf_item_activity_statuses a,
                  wf_process_activities b
             WHERE a.item_type = 'OEOL'
             AND a.process_activity = b.instance_id
             AND b.activity_name = 'XXWC_WAIT_UNTIL_PROGRESSED'
             AND b.Process_Name  = C_PROCESS_NAME
             AND activity_status NOT IN('COMPLETE')
             AND a.item_key = TO_CHAR(ool.line_id))
         AND NOT EXISTS
                     (SELECT '1'
                        FROM FND_LOOKUP_VALUES FLV
                       WHERE     1 = 1
                         AND FLV.LOOKUP_TYPE = 'XXWC_HAWAII_BRANCHES'
                         AND OOL.SHIP_FROM_ORG_ID = TO_NUMBER(FLV.LOOKUP_CODE));
                             
    
   
      l_api_name       VARCHAR2 (30) := 'Progress_Counter_SO_Lines';
      l_distro_list    VARCHAR2 (75)
                                    DEFAULT 'HDSOracleDevelopers@hdsupply.com';
      l_package_name   VARCHAR2 (75) DEFAULT 'XXWC_ONT_ROUTINES_PKG';
      l_line_id        VARCHAR2(50); -- Version# 5.4, TMS# 20150202-00030
      l_arg_line_id    VARCHAR2(50); -- Version# 5.4, TMS# 20150202-00030
      l_arg_activity   VARCHAR2(80):='XXWC_WAIT_UNTIL_PROGRESSED';  -- Version# 5.4, TMS# 20150202-00030
      
   BEGIN
      fnd_file.put_line (fnd_file.LOG, '100 : Begin of API : ' || l_api_name);
      fnd_global.apps_initialize (fnd_global.user_id,
                                  fnd_global.resp_id,
                                  fnd_global.resp_appl_id
                                 );
      
      
    IF NVL(P_INCL_HAWAII_BRANCHES,'N')='N' THEN
    
    fnd_file.put_line (fnd_file.LOG, '110A : Loop Through the Cursor : ');

        FOR i IN non_hawaii_order_cur
         LOOP
          BEGIN
            l_line_id:= TO_CHAR (i.line_id);  -- Version# 5.4, TMS# 20150202-00030
            --fnd_file.put_line (fnd_file.LOG,'120A : Complete Activity for SO Line ID : '|| i.line_id);--Commented by Ram Talluri8/30/2013 : Task ID: 20130830-00571
            wf_engine.completeactivity ('OEOL',
                                        l_line_id, -- TO_CHAR (i.line_id), -- Version# 5.4, TMS# 20150202-00030
                                        l_arg_activity,  -- 'XXWC_WAIT_UNTIL_PROGRESSED', -- Version# 5.4, TMS# 20150202-00030
                                        NULL
                                       );
           EXCEPTION
            WHEN OTHERS
            THEN
               retcode := 1;
               fnd_file.put_line
                  (fnd_file.LOG,
                      '150A : When others Exception while completing acivity for Counter SO Line ID  : '
                   || i.line_id
                  );
               fnd_file.put_line (fnd_file.LOG,
                                  '151A : Error Message : ' || SQLERRM
                                 );
           END;
         END LOOP;
         
         wf_engine.background (itemtype              => 'OEOL',
                            minthreshold          => NULL,
                            maxthreshold          => NULL,
                            process_deferred      => TRUE,
                            process_timeout       => TRUE,
                            process_stuck         => FALSE
                           );
         
         
    ELSIF NVL(P_INCL_HAWAII_BRANCHES,'N')='Y' THEN
    
    fnd_file.put_line (fnd_file.LOG, '110B : Loop Through the Cursor : ');     
    
         FOR i IN order_cur
         LOOP
          BEGIN
            l_arg_line_id:= TO_CHAR (i.line_id); -- Version# 5.4, TMS# 20150202-00030
            --fnd_file.put_line (fnd_file.LOG,'120B : Complete Activity for SO Line ID : '|| i.line_id);--Commented by Ram Talluri8/30/2013 : Task ID: 20130830-00571
            wf_engine.completeactivity ('OEOL',
                                        l_arg_line_id,  -- TO_CHAR (i.line_id), -- Version# 5.4, TMS# 20150202-00030
                                        l_arg_activity,  -- 'XXWC_WAIT_UNTIL_PROGRESSED', -- Version# 5.4, TMS# 20150202-00030
                                        NULL
                                       );
           EXCEPTION
            WHEN OTHERS
            THEN
               retcode := 1;
               fnd_file.put_line
                  (fnd_file.LOG,
                      '150B : When others Exception while completing acivity for Counter SO Line ID  : '
                   || i.line_id
                  );
               fnd_file.put_line (fnd_file.LOG,
                                  '151B : Error Message : ' || SQLERRM
                                 );
           END;
         END LOOP;
         
         wf_engine.background (itemtype              => 'OEOL',
                            minthreshold          => NULL,
                            maxthreshold          => NULL,
                            process_deferred      => TRUE,
                            process_timeout       => TRUE,
                            process_stuck         => FALSE
                           );
    END IF;      
      
   EXCEPTION
      WHEN OTHERS
      THEN
         retcode := 2;
         errbuf := SQLERRM;
         fnd_file.put_line (fnd_file.LOG, '160 : When others Exception ');
         fnd_file.put_line (fnd_file.LOG,
                            '161 : Error Message : ' || SQLERRM);
         fnd_file.put_line (fnd_file.output, SQLERRM);
         -- Calling ERROR API
         xxcus_error_pkg.xxcus_error_main_api
            (p_called_from            => l_package_name,
             p_calling                => 'Concurrent Program exception for XXWC_ONT_ROUTINES_PKG.Progress_Counter_SO_Lines',
             p_request_id             => fnd_global.conc_request_id,
             p_ora_error_msg          => SQLERRM,
             p_error_desc             => 'When others Exception ',
             p_distribution_list      => l_distro_list,
             p_module                 => 'OM'
            );
   END progress_Counter_so_lines;

     /*************************************************************************
   *   Function : Check_SO_LINES_CLOSED
   *
   *   PURPOSE:   This procedure is called from WF Node at Header Level.
   *   Checks if all the lines are created for rental Orders and also checks if all the SO lines are closed or not
   *   If not then moves Header to wait mode until next day.
   * ************************************************************************/
   PROCEDURE check_so_lines_closed (
      itemtype    IN              VARCHAR2,
      itemkey     IN              VARCHAR2,
      actid       IN              NUMBER,
      funcmode    IN              VARCHAR2,
      resultout   IN OUT NOCOPY   VARCHAR2
   )
   IS
      l_header_id          NUMBER;
      l_open_lines_count   NUMBER;
      l_rma_line_count     NUMBER;
      l_row_count          NUMBER;

      /* Cursor to Get only the Shipped Lines */
      CURSOR cur_rental_line (p_order_header_id NUMBER)
      IS
         SELECT   *
             FROM apps.oe_order_lines oel
            WHERE oel.header_id = p_order_header_id
              AND oel.flow_status_code NOT IN ('CANCELLED')
              AND link_to_line_id IS NULL
              AND actual_shipment_date IS NOT NULL
         ORDER BY line_id;

      -- Cursor to check if RMA Lines are created or not
      CURSOR cur_rma_details (p_order_header_id NUMBER, p_line_id NUMBER)
      IS
         SELECT   COUNT (*) line_count
             FROM apps.oe_order_lines
            WHERE header_id = p_order_header_id
              AND return_attribute1 = p_order_header_id
              AND return_attribute2 = p_line_id
              AND line_category_code = 'RETURN'
              AND flow_status_code <> 'CANCELLED'
         ORDER BY shipment_number;

      -- Cursor to check if Order Type is WC RENTAL TERM  and ATTRIBUTE1 Term
      CURSOR cur_hdr_cur (p_header_id NUMBER)
      IS
         SELECT ott.NAME order_type, ooh.attribute1
           FROM apps.oe_order_headers ooh, oe_transaction_types_v ott
          WHERE ott.transaction_type_id = ooh.order_type_id
            AND ooh.header_id = p_header_id;

      l_return_flag        BOOLEAN;
      l_proc_name           VARCHAR2(80):='Check_SO_Lines_Closed'; -- Version# 5.4, TMS# 20150202-00030
      l_arg                 VARCHAR2(50);  -- Version# 5.4, TMS# 20150202-00030
   BEGIN
      -- RUN mode - normal process execution
      --
      fnd_log.STRING (c_level_statement,
                      'XXWC_ONT_ROUTINES_PKG.CHECK_SO_LINES_CLOSED',
                      ' Func Mode :' || funcmode
                     );

      IF (funcmode = 'RUN')
      THEN
         fnd_log.STRING (c_level_statement,
                         'XXWC_ONT_ROUTINES_PKG.CHECK_SO_LINES_CLOSED',
                         'Item Type :' || itemtype
                        );
         fnd_log.STRING (c_level_statement,
                         'XXWC_ONT_ROUTINES_PKG.CHECK_SO_LINES_CLOSED',
                         'G_WFI_HDR :' || oe_globals.g_wfi_hdr
                        );
         oe_standard_wf.set_msg_context (actid);
         l_arg :=TO_CHAR (actid);   -- -- Version# 5.4, TMS# 20150202-00030

         IF itemtype = oe_globals.g_wfi_hdr
         THEN
            l_header_id := TO_NUMBER (itemkey);

            -- Get Total number of lines in Closed Status
            SELECT COUNT (*)
              INTO l_open_lines_count
              FROM apps.oe_order_lines
             WHERE header_id = l_header_id
               AND flow_status_code NOT IN ('CLOSED', 'CANCELLED');

            fnd_log.STRING (c_level_statement,
                            'XXWC_ONT_ROUTINES_PKG.CHECK_SO_LINES_CLOSED',
                            'Open Lines Count :' || l_open_lines_count
                           );

            -- If Line Count is > 0 Then Return Flase
            IF l_open_lines_count > 0
            THEN
               resultout := 'COMPLETE:N';
               RETURN;
            END IF;

            FOR hdr_rec IN cur_hdr_cur (l_header_id)
            LOOP
               IF    (INSTR (UPPER (hdr_rec.order_type), 'TERM', 1) > 0)
                  OR (INSTR (UPPER (hdr_rec.attribute1), 'TERM', 1) > 0)
               THEN
                  FOR rental_line_rec IN cur_rental_line (l_header_id)
                  LOOP
                     fnd_log.STRING
                              (c_level_statement,
                               'XXWC_ONT_ROUTINES_PKG.CHECK_SO_LINES_CLOSED',
                               'For Loop of Rental Line REC :'
                              );

                     FOR rma_rec_count IN
                        cur_rma_details (l_header_id, rental_line_rec.line_id)
                     LOOP
                        l_rma_line_count := rma_rec_count.line_count;

                        IF l_rma_line_count = 0
                        THEN
                           -- If Line Count is > 0 Then Return Flase
                           fnd_log.STRING
                              (c_level_statement,
                               'XXWC_ONT_ROUTINES_PKG.CHECK_SO_LINES_CLOSED',
                               'RMA Lines Count :' || l_rma_line_count
                              );
                           resultout := 'COMPLETE:N';
                           RETURN;
                        END IF;
                     END LOOP;
                  END LOOP;
               END IF;
            END LOOP;

            fnd_log.STRING (c_level_statement,
                            'XXWC_ONT_ROUTINES_PKG.CHECK_SO_LINES_CLOSED',
                            'Returning Y :'
                           );
            resultout := 'COMPLETE:Y';
            RETURN;
         END IF;
      ELSE
         --- It should always be OEOH
         resultout := 'COMPLETE:N';
         RETURN;
      END IF;
   EXCEPTION
      WHEN OTHERS
      THEN
         -- The line below records this function call in the error system
         -- in the case of an exception.
         wf_core.CONTEXT (g_pkg_name, -- 'XXWC_ONT_ROUTINES_PKG', -- Version# 5.4, TMS# 20150202-00030
                          l_proc_name,  --  'Check_SO_Lines_Closed', -- Version# 5.4, TMS# 20150202-00030
                          itemtype,
                          itemkey,
                          l_arg,  -- TO_CHAR (actid), -- Version# 5.4, TMS# 20150202-00030
                          funcmode
                         );
         -- start data fix project
         oe_standard_wf.add_error_activity_msg (p_actid         => actid,
                                                p_itemtype      => itemtype,
                                                p_itemkey       => itemkey
                                               );
         -- end data fix project
         oe_standard_wf.save_messages;
         oe_standard_wf.clear_msg_context;
         fnd_log.STRING (c_level_statement,
                         'XXWC_ONT_ROUTINES_PKG.CHECK_SO_LINES_CLOSED',
                         ' When Others Error  :' || SQLERRM
                        );
         RAISE;
   END check_so_lines_closed;

   -- 06/28/2012 CG: Added for sales and fulfillment process change
   PROCEDURE write_log (p_debug_msg IN VARCHAR2)
   IS
   BEGIN
      fnd_file.put_line (fnd_file.LOG, p_debug_msg);
      DBMS_OUTPUT.put_line (p_debug_msg);
   END write_log;

   FUNCTION xxwc_pick_release_call (
      p_order_number   IN   NUMBER,
      p_user_id        IN   NUMBER,
      p_resp_id        IN   NUMBER,
      p_resp_appl_id   IN   NUMBER,
      p_session_id     IN   NUMBER
   )
      RETURN VARCHAR2
   IS
      l_return_status   VARCHAR2 (400);
      l_req_id          NUMBER;
   BEGIN
      --fnd_global.apps_initialize(p_user_id, p_resp_id, p_resp_appl_id);
      --mo_global.init('ONT');
      l_return_status := NULL;
      l_req_id := NULL;
      l_req_id :=
         fnd_request.submit_request (application      => 'XXWC',
                                     program          => 'XXWC_ORD_PICK_RELEASE_PROC',
                                     description      => NULL,
                                     start_time       => SYSDATE,
                                     sub_request      => FALSE,
                                     argument1        => p_order_number,
                                     argument2        => p_user_id,
                                     argument3        => p_resp_id,
                                     argument4        => p_resp_appl_id,
                                     argument5        => p_session_id
                                    );

      -- commit;
      IF NVL (l_req_id, 0) > 0
      THEN
         l_return_status :=
               'Concurrent request '
            || l_req_id
            || ' has been submitted to pick order '
            || p_order_number;
      ELSE
         l_return_status :=
                  'Could not submit request to pick order ' || p_order_number;
      END IF;

      RETURN l_return_status;
   EXCEPTION
      WHEN OTHERS
      THEN
         RETURN ('Could not submit pick job for sales order '
                 || p_order_number
                );
   END xxwc_pick_release_call;

   PROCEDURE xxwc_pick_release_order (
      /*errbuf                     OUT VARCHAR2
       , retcode               OUT VARCHAR2
       ,*/
      p_order_number   IN   NUMBER,
      p_organization_id IN  NUMBER, --Added 10/26/2012
      p_user_id        IN   NUMBER,
      p_resp_id        IN   NUMBER,
      p_resp_appl_id   IN   NUMBER,
      p_session_id     IN   NUMBER
   )
   IS
      CURSOR cur_pick_orgs
      IS
         SELECT DISTINCT (ooha.header_id), oola.ship_from_org_id,
                         ooha.source_document_type_id, order_type_id
                    FROM apps.oe_order_headers ooha, apps.oe_order_lines oola
                   WHERE ooha.order_number = p_order_number
                     AND NVL (ooha.booked_flag, 'N') = 'Y'
                     AND NVL (ooha.cancelled_flag, 'N') = 'N'
                     AND ooha.header_id = oola.header_id
                     AND NVL (oola.cancelled_flag, 'N') = 'N'
                     AND oola.line_category_code = 'ORDER'
                     AND oola.ship_from_org_id = nvl(p_organization_id,oola.ship_from_org_id); --added 10/26/2012


      l_return_status      VARCHAR2 (1);
      l_msg_count          NUMBER (15);
      l_msg_data           VARCHAR2 (2000);
      l_count              NUMBER (15);
      l_msg_data_out       VARCHAR2 (2000);
      l_mesg               VARCHAR2 (2000);
      p_count              NUMBER (15);
      p_new_batch_id       NUMBER;
      l_rule_id            NUMBER;
      l_rule_name          VARCHAR2 (2000);
      l_batch_prefix       VARCHAR2 (2000);
      l_batch_info_rec     wsh_picking_batches_pub.batch_info_rec;
      l_request_id         NUMBER;
      v_phase              VARCHAR2 (50);
      v_status             VARCHAR2 (50);
      v_dev_status         VARCHAR2 (50);
      v_dev_phase          VARCHAR2 (50);
      v_message            VARCHAR2 (250);
      v_error_message      VARCHAR2 (3000);
      v_wait               BOOLEAN;
      l_open_deliveries    NUMBER;
      l_pick_rule_string   VARCHAR2 (50);
   BEGIN
      write_log (   '  Entering pick_release_order for p_order_number '
                 || p_order_number
                );
      fnd_global.apps_initialize (p_user_id, p_resp_id, p_resp_appl_id);
      mo_global.init ('ONT');

      FOR c1 IN cur_pick_orgs
      LOOP
         EXIT WHEN cur_pick_orgs%NOTFOUND;

         --if it's internal then pick the
         IF c1.source_document_type_id = 10
         THEN
            l_pick_rule_string := 'All Internal';
         ELSE
            l_pick_rule_string := 'Unreleased Std';
         END IF;

         BEGIN
            SELECT wpr.picking_rule_id, wpr.NAME
              INTO l_rule_id, l_rule_name
              FROM wsh_picking_rules wpr
             WHERE wpr.organization_id = c1.ship_from_org_id
               --AND wpr.NAME LIKE '%Unreleased Std%'          --'%Backordered%'
               AND wpr.NAME LIKE '%' || l_pick_rule_string || '%'
               AND TRUNC (wpr.start_date_active) <= TRUNC (SYSDATE)
               AND NVL (TRUNC (wpr.end_date_active), TRUNC (SYSDATE + 1)) >
                                                               TRUNC (SYSDATE)
               AND ROWNUM = 1;
         EXCEPTION
            WHEN OTHERS
            THEN
               l_rule_id := NULL;
               l_rule_name := NULL;
         END;

         IF l_rule_id IS NOT NULL
         THEN
            --l_batch_info_rec.backorders_only_flag := 'I';
            --l_batch_info_rec.existing_rsvs_only_flag := 'N';
            l_batch_info_rec.order_header_id := c1.header_id;
            --l_batch_info_rec.from_scheduled_ship_date := NULL;
            l_batch_info_rec.organization_id := c1.ship_from_org_id;
            l_batch_info_rec.document_set_id := NULL;
            l_batch_info_rec.document_set_name := NULL;
            --l_batch_info_rec.include_planned_lines := 'N';
            --l_batch_info_rec.autocreate_delivery_flag := 'Y';
            --l_batch_info_rec.autodetail_pr_flag := 'Y';
            --l_batch_info_rec.allocation_method := 'I';
            --l_batch_info_rec.pick_from_locator_id := NULL;
            --l_batch_info_rec.auto_pick_confirm_flag := 'Y';
            --l_batch_info_rec.autopack_flag := 'Y';
            --l_batch_info_rec.append_flag := 'Y';
            l_batch_prefix := NULL;
            --added LHS
            l_batch_info_rec.order_type_id := c1.order_type_id;
            --end added
            wsh_picking_batches_pub.create_batch
                                         (p_api_version        => 1.0,
                                          p_init_msg_list      => fnd_api.g_true,
                                          p_commit             => fnd_api.g_true,
                                          x_return_status      => l_return_status,
                                          x_msg_count          => l_msg_count,
                                          x_msg_data           => l_msg_data,
                                          p_rule_id            => l_rule_id,
                                          p_rule_name          => l_rule_name,
                                          p_batch_rec          => l_batch_info_rec,
                                          p_batch_prefix       => l_batch_prefix,
                                          x_batch_id           => p_new_batch_id
                                         );

            IF l_return_status = 'S'
            THEN
               write_log (   '  Pick Release Batch Got Created Sucessfully '
                          || p_new_batch_id
                         );
            ELSE
               write_log
                       (   '   Could not generate pick batch. Message count '
                        || l_msg_count
                       );

               IF l_msg_count = 1
               THEN
                  write_log ('  Error Message ' || l_msg_data);
               ELSIF l_msg_count > 1
               THEN
                  LOOP
                     p_count := p_count + 1;
                     l_msg_data :=
                        fnd_msg_pub.get (fnd_msg_pub.g_next, fnd_api.g_false);
                     write_log (   ' Error Message ('
                                || p_count
                                || '): '
                                || l_msg_data
                               );
                  END LOOP;
               END IF;
            END IF;

            write_log ('  Submitting Pick Release for Batch '
                       || p_new_batch_id
                      );
            -- Release the batch Created Above
            wsh_picking_batches_pub.release_batch
                                          (p_api_version        => 1.0,
                                           p_init_msg_list      => fnd_api.g_true,
                                           p_commit             => fnd_api.g_true,
                                           x_return_status      => l_return_status,
                                           x_msg_count          => l_msg_count,
                                           x_msg_data           => l_msg_data,
                                           p_batch_id           => p_new_batch_id,
                                           p_batch_name         => NULL,
                                           p_log_level          => 1,
                                           p_release_mode       => 'CONCURRENT',
                                           x_request_id         => l_request_id
                                          );

            IF l_return_status = 'S'
            THEN
               write_log ('  Pick Selection List Generation ' || l_request_id);
            /*v_wait := fnd_concurrent.wait_for_request (l_request_id,
                                                6,
                                                1800,
                                                v_phase,
                                                v_status,
                                                v_dev_phase,
                                                v_dev_status,
                                                v_message
                                               );
            write_log ('  Pick Program Completion Status: ' || v_status);*/
            ELSE
               IF l_msg_count = 1
               THEN
                  write_log ('  Error Message: ' || l_msg_data);
               ELSIF l_msg_count > 1
               THEN
                  LOOP
                     p_count := p_count + 1;
                     l_msg_data :=
                        fnd_msg_pub.get (fnd_msg_pub.g_next, fnd_api.g_false);
                     write_log (   '  Error Message ('
                                || p_count
                                || '): '
                                || l_msg_data
                               );
                  END LOOP;
               END IF;
            END IF;
         ELSE
            write_log (   '  Could not find pick rule for header_id '
                       || c1.header_id
                      );
         END IF;

         write_log (   '  Exiting pick_release_order for header_id '
                    || c1.header_id
                   );
      END LOOP;
   EXCEPTION
      WHEN OTHERS
      THEN
         write_log ('Could not pick/ship order ' || p_order_number);
         write_log ('Error: ' || SQLERRM);
         RAISE;
   END xxwc_pick_release_order;

   PROCEDURE xxwc_pack_slip_report (
      p_order_number   IN   NUMBER,
      p_print_price    IN   NUMBER,
      p_user_id        IN   NUMBER,
      p_resp_id        IN   NUMBER,
      p_resp_appl_id   IN   NUMBER,
      p_session_id     IN   NUMBER
   )
   IS
      CURSOR cur_valid_orgs
      IS
         SELECT DISTINCT (ooha.header_id) order_header_id,
                         oola.ship_from_org_id
                    FROM apps.oe_order_headers ooha, apps.oe_order_lines oola
                   WHERE ooha.order_number = p_order_number
                     AND NVL (ooha.booked_flag, 'N') = 'Y'
                     AND NVL (ooha.cancelled_flag, 'N') = 'N'
                     AND ooha.header_id = oola.header_id
                     AND NVL (oola.cancelled_flag, 'N') = 'N'
                     AND NVL (oola.open_flag, 'Y') = 'Y'
                     AND oola.line_category_code = 'ORDER'
                     AND oola.flow_status_code NOT IN
                            ('PRE-BILLING_ACCEPTANCE', 'CLOSED', 'CANCELLED',
                             'SHIPPED');

      x_request_id          NUMBER;
      l_printer             VARCHAR2 (30);
      l_copies              NUMBER;
      l_set_print_options   BOOLEAN;
      l_set_mode            BOOLEAN;
      xml_layout            BOOLEAN;
      l_print_hazmat        NUMBER;
      l_user_def_org_id     NUMBER;
   BEGIN
      write_log (   '  Entering xxwc_pack_slip_report for p_order_number '
                 || p_order_number
                );
      fnd_global.apps_initialize (p_user_id, p_resp_id, p_resp_appl_id);
      mo_global.init ('ONT');

      FOR c1 IN cur_valid_orgs
      LOOP
         EXIT WHEN cur_valid_orgs%NOTFOUND;
         -- Getting printer information
         l_printer := NULL;
         l_user_def_org_id :=
                           fnd_profile.VALUE ('XXWC_OM_DEFAULT_SHIPPING_ORG');

         IF NVL (l_user_def_org_id, -1) = c1.ship_from_org_id
         THEN
            l_printer := fnd_profile.VALUE ('PRINTER');
         ELSE
            BEGIN
               SELECT wrp.printer_name
                 INTO l_printer
                 FROM wsh_report_printers wrp, fnd_concurrent_programs fcp
                WHERE level_type_id = 10008
                  AND NVL (wrp.enabled_flag, 'N') = 'Y'
                  AND NVL (wrp.default_printer_flag, 'N') = 'Y'
                  AND wrp.concurrent_program_id = fcp.concurrent_program_id
                  AND fcp.concurrent_program_name = 'XXWC_OM_PACK_SLIP'
                  AND wrp.level_value_id = c1.ship_from_org_id;
            EXCEPTION
               WHEN OTHERS
               THEN
                  l_printer := fnd_profile.VALUE ('PRINTER');
            END;
         END IF;

         l_copies := '1';
         l_set_print_options :=
            fnd_request.set_print_options (printer               => l_printer,
                                           style                 => NULL,
                                           copies                => l_copies,
                                           save_output           => TRUE,
                                           print_together        => 'N',
                                           validate_printer      => 'RESOLVE'
                                          );
         xml_layout :=
            fnd_request.add_layout ('XXWC',
                                    'XXWC_OM_PACK_SLIP',
                                    'en',
                                    'US',
                                    'PDF'
                                   );
         x_request_id :=
            fnd_request.submit_request
                                   (application      => 'XXWC',
                                    program          => 'XXWC_OM_PACK_SLIP',
                                    description      => 'XXWC OM Pack Slip Report',
                                    sub_request      => FALSE,
                                    argument1        => c1.ship_from_org_id,
                                    argument2        => c1.order_header_id,
                                    argument3        => p_print_price,
                                    argument4        => NULL,
                                    argument5        => NULL,
                                    argument6        => '2',
                                    argument7        => NULL,
                                    argument8        => p_print_price
                                   );
      END LOOP;

      COMMIT;
   EXCEPTION
      WHEN OTHERS
      THEN
         write_log ('Could not print pack slip for ' || p_order_number);
         write_log ('Error: ' || SQLERRM);
         RAISE;
   END xxwc_pack_slip_report;

   FUNCTION get_onhand (
      p_inventory_item_id   IN   NUMBER,
      p_organization_id     IN   NUMBER,
      p_subinventory        IN   VARCHAR2,
      p_return_type         IN   VARCHAR2,
      p_lot_number          IN   VARCHAR2 DEFAULT NULL
   )
      RETURN NUMBER
   IS
      --Used in XXWC_PACKING_SLIP Report to retrieve on hand information
      qoh                NUMBER;
      rqoh               NUMBER;
      qr                 NUMBER;
      qs                 NUMBER;
      att                NUMBER;
      atr                NUMBER;
      ls                 VARCHAR2 (1);
      mc                 NUMBER;
      md                 VARCHAR2 (1000);
      l_lot_control      BOOLEAN;
      l_serial_control   BOOLEAN;
      l_lc_code          NUMBER;
      l_sn_code          NUMBER;
      l_lot_number       VARCHAR2 (30);
   BEGIN
      IF p_lot_number IS NOT NULL
      THEN
         l_lot_control := TRUE;
         l_lot_number := p_lot_number;
      ELSE
         l_lot_control := FALSE;
         l_lot_number := NULL;
      END IF;

      inv_quantity_tree_pub.clear_quantity_cache;
      inv_quantity_tree_pub.query_quantities
                                  (p_api_version_number       => 1.0,
                                   p_init_msg_lst             => 'T',
                                   x_return_status            => ls,
                                   x_msg_count                => mc,
                                   x_msg_data                 => md,
                                   p_organization_id          => p_organization_id,
                                   p_inventory_item_id        => p_inventory_item_id,
                                   p_tree_mode                => 1
                                                                  --'Reservation Mode'
      ,
                                   p_is_revision_control      => FALSE,
                                   p_is_lot_control           => l_lot_control
                                                                       --FALSE
                                                                              --l_lot_control
      ,
                                   p_is_serial_control        => FALSE
                                                                      --l_serial_control
      ,
                                   p_lot_expiration_date      => SYSDATE,
                                   p_revision                 => NULL,
                                   p_lot_number               => l_lot_number,
                                   p_subinventory_code        => p_subinventory,
                                   p_locator_id               => NULL,
                                   x_qoh                      => qoh,
                                   x_rqoh                     => rqoh,
                                   x_qr                       => qr,
                                   x_qs                       => qs,
                                   x_att                      => att,
                                   x_atr                      => atr
                                  );

      IF p_return_type = 'H'
      THEN
         RETURN (qoh);
      ELSIF p_return_type = 'R'
      THEN
         RETURN (atr);
      ELSIF p_return_type = 'T'
      THEN
         RETURN (att);
      ELSE
         RETURN 0;
      END IF;
   EXCEPTION
      WHEN OTHERS
      THEN
         RETURN (0);
   END get_onhand;


   /*************************************************************************
    *  Function : COUNT_ORDER_HOLDS
    *
    *  PURPOSE:   Function returns 0 when there are no Credit Holds and returns 1 when there are 1 or more holds
    *
    *  REVISIONS:
    *  Ver        Date         Author            Description
    *  ---------  ----------   ---------------   -------------------------
    *  5.5        02/09/2015   Gopi Damuluri     TMS# 20141119-00027 Performance Tuning - XXWC_ONT_ROUTINES_PKG.COUNT_ORDER_HOLDS
    * ************************************************************************/

   FUNCTION count_order_holds (p_header_id IN NUMBER)
      RETURN NUMBER
   IS
      l_count_holds   NUMBER;
   BEGIN
      SELECT COUNT (ooha.order_hold_id)
        INTO l_count_holds
        FROM apps.oe_order_holds ooha,
             apps.oe_hold_sources ohsa,
             oe_hold_definitions ohd
       WHERE ooha.header_id = p_header_id
         AND NVL (ooha.released_flag, 'N') = 'N'
         AND ooha.hold_source_id = ohsa.hold_source_id
         AND ohsa.hold_id = ohd.hold_id
         AND ohd.type_code IN ('CREDIT')
         AND ROWNUM = 1; -- Version# 5.5

      RETURN (NVL (l_count_holds, 0));
   EXCEPTION
      WHEN OTHERS
      THEN
         RETURN (0);
   END count_order_holds;


   -- Version# 7.0 > Start
   /*************************************************************************
   *   PROCEDURE : wait_for_running_request
   *
   *   PURPOSE:   This procedure is called from submit_print_batch to wait for XXWC_OM_PICK_SLIP to complete.
   
   *  REVISIONS:
   *  Ver        Date         Author            Description
   *  ---------  ----------   ---------------   -------------------------
   *  1.0        02/19/2016   Rakesh Patel      TMS# 20151117-00031 Pick ticket printing for Zero quantities                                 
   * ************************************************************************/
   PROCEDURE wait_for_running_request ( p_order_hdr_id IN VARCHAR2 )
   IS
      CURSOR cu_running_requests ( p_order_hdr_id IN VARCHAR2)
      IS
         SELECT /*+ Index(fnd_concurrent_requests_N7) */ request_id
           FROM apps.fnd_concurrent_requests fcr
          WHERE fcr.concurrent_program_id = 75400
            AND (status_code = 'R' OR status_code = 'I')
            AND (phase_code = 'R' OR phase_code ='P')
            AND argument2 = p_order_hdr_id;

      l_request_id    NUMBER;
      lc_phase        VARCHAR2 (100);
      lc_status       VARCHAR2 (100);
      lc_dev_phase    VARCHAR2 (100);
      lc_dev_status   VARCHAR2 (100);
      lc_message      VARCHAR2 (100);
      lc_waited BOOLEAN;
   BEGIN
      LOOP
         OPEN cu_running_requests( p_order_hdr_id );

         FETCH cu_running_requests
          INTO l_request_id;
         
         IF cu_running_requests%NOTFOUND
         THEN
            CLOSE cu_running_requests;

            EXIT;
         END IF;

         CLOSE cu_running_requests;

         IF l_request_id > 0
         THEN
            lc_waited:=fnd_concurrent.wait_for_request (
                                             request_id      => l_request_id,
                                             INTERVAL        => 2,
                                             max_wait        => 180,
                                             phase           => lc_phase,
                                             status          => lc_status,
                                             dev_phase       => lc_dev_phase,
                                             dev_status      => lc_dev_status,
                                             MESSAGE         => lc_message
                                            );
         END IF;
      END LOOP;
   EXCEPTION
      WHEN OTHERS
      THEN
         write_log ('Error in wait_for_running_request order header id ' || p_order_hdr_id);
         write_log ('Error: ' || SQLERRM);
         RAISE;
   END wait_for_running_request;
   -- Version# 7.0 > End
   
   /*************************************************************************
   *   PROCEDURE : submit_print_batch
   *
   * Purpose: submit_print_batch  
   *  REVISIONS:
   *  Ver        Date         Author            Description
   *  ---------  ----------   ---------------   -------------------------
   *  11.0       03/30/2017   P.Vamshidhar             TMS#20170330-00413 - Sales Receipt Modification to add group id 
   *12.0       04/28/2017   Niraj K Ranjan           TMS#20160815-00078   Branch Visit - Pick Ticket Reprint Improvements
   * ************************************************************************/
       
   
   FUNCTION submit_print_batch (
      p_batch_id       IN   NUMBER,
      p_group_id       IN   NUMBER,
      p_process_flag   IN   NUMBER,
      p_user_id        IN   NUMBER,
      p_resp_id        IN   NUMBER,
      p_resp_appl_id   IN   NUMBER
   )
      RETURN NUMBER
   IS
      PRAGMA AUTONOMOUS_TRANSACTION;             --commented out LHS 08142012
      l_return              NUMBER;                            --varchar2(1);
      l_exception           EXCEPTION;
      x_request_id          NUMBER;
      l_set_print_options   BOOLEAN;
      l_set_mode            BOOLEAN;
      xml_layout            BOOLEAN;
      l_save_output         BOOLEAN;
      l_sub_request         BOOLEAN;

      CURSOR c_print
      IS
         SELECT ROWID, batch_id, process_flag, GROUP_ID, application,
                program, description, start_time, sub_request, printer,
                style, copies, save_output, print_together, validate_printer,
                template_appl_name, template_code, template_language,
                template_territory, output_format, nls_language
           FROM xxwc_print_requests_temp xprt
          WHERE process_flag = p_process_flag
            AND batch_id = p_batch_id
            AND GROUP_ID = p_group_id;

      CURSOR c_arg
      IS
         SELECT   ROWID, NVL (VALUE, NULL) VALUE, argument
             FROM xxwc_print_requests_arg_temp
            WHERE batch_id = p_batch_id
              AND process_flag = p_process_flag
              AND GROUP_ID = p_group_id
         ORDER BY p_group_id, p_batch_id, argument;

      l_count               NUMBER;
      l_argument            VARCHAR2 (32767);
      l_value               VARCHAR2 (250);
      l_sql                 VARCHAR2 (32767);
      
      l_program             VARCHAR2 (30); -- Version# 9.0
      ln_header_id          number; --Rev 12.0
      l_printer_loc         NUMBER; --Rev 12.0
      l_reprint_ind         NUMBER; --Rev 12.0
      l_print_org_id        NUMBER; --Rev 12.0
   BEGIN
      fnd_global.apps_initialize (p_user_id, p_resp_id, p_resp_appl_id);
      mo_global.init ('ONT');
      -- mo_global.set_policy_context ('S', 162);
    
      --DBMS_OUTPUT.put_line ('Starting');
      --dbms_output.put_line(p_user_id);
      FOR r_print IN c_print
      LOOP
         l_set_mode := fnd_request.set_mode (TRUE);

         IF r_print.save_output = 'TRUE'
         THEN
            l_save_output := TRUE;
         ELSE
            l_save_output := FALSE;
         END IF;

         IF r_print.sub_request = 'TRUE'
         THEN
            l_sub_request := TRUE;
         ELSE
            l_sub_request := FALSE;
         END IF;

         l_set_print_options :=
            fnd_request.set_print_options
                                    (printer               => r_print.printer,
                                     style                 => r_print.style,
                                     copies                => r_print.copies,
                                     save_output           => l_save_output,
                                     print_together        => r_print.print_together,
                                     validate_printer      => 'RESOLVE'
                                    );            --r_print.validate_printer);
         xml_layout :=
            fnd_request.add_layout
                            (template_appl_name      => r_print.template_appl_name,
                             template_code           => r_print.template_code,
                             template_language       => r_print.template_language,
                             template_territory      => r_print.template_territory,
                             output_format           => r_print.output_format,
                             nls_language            => r_print.nls_language
                            );
         l_count := 0;
         --DBMS_OUTPUT.put_line ('Count is 0');

         FOR r_arg IN c_arg
         LOOP
            -- Version# 7.0 > Start
                    IF r_print.program = 'XXWC_OM_PACK_SLIP' AND r_arg.argument = '2' THEN
                        wait_for_running_request ( p_order_hdr_id => r_arg.VALUE );
                    END IF;
            -- Version# 7.0 < End
            
            IF r_arg.VALUE IS NULL
            THEN
               l_value := 'NULL';
            ELSE
               l_value := '' || r_arg.VALUE || '';
            END IF;

            IF l_count = 0
            THEN
               l_argument := l_value;
            ELSE
               l_argument := l_argument || ',' || l_value;
            END IF;

            l_count := l_count + 1;
            --DBMS_OUTPUT.put_line (l_argument);
            --DBMS_OUTPUT.put_line (l_count);

            BEGIN
               UPDATE xxwc_print_requests_arg_temp
                  SET process_flag = 2
                WHERE ROWID = r_arg.ROWID
                  AND process_flag = p_process_flag
                  AND batch_id = p_batch_id
                  AND argument = r_arg.argument;
            EXCEPTION
               WHEN OTHERS
               THEN
                  DBMS_OUTPUT.put_line ('can not update ' || SQLCODE
                                        || SQLERRM
                                       );
                  RAISE l_exception;
            END;
         END LOOP;

        -- Rev 11.0 Changes begin
        if r_print.program IN ('XXWC_OM_SRECEIPT_CUSTOMER','XXWC_OM_SRECEIPT','XXWC_OM_PACK_SLIP')  THEN
        l_argument := l_argument || ',' || p_group_id;        
        END IF;
        -- Rev 11.0 Changes End.
         
         
         -- Version# 9.0 > Start
         IF r_print.program = 'XXWC_OM_PACK_SLIP' AND r_print.copies = 0 THEN
           l_program := 'XXWC_OM_PACK_SLIP_0_COPIES';
         ELSE
           l_program := r_print.program;
         END IF;
         -- Version# 9.0 < End
         
         l_sql :=
               'declare
                        x_request_id number;

                      begin
                                x_request_id := fnd_request.submit_request
                                                ('''
            || r_print.application
            || '''
                                                ,'''
            || l_program -- Version# 9.0
            -- || r_print.program -- Version# 9.0
            || '''
                                                ,'''
            || r_print.description
            || '''
                                                ,SYSDATE
                                                ,FALSE
                                                ,'
            || l_argument
            || ');

                      end;';
         --DBMS_OUTPUT.put_line (l_sql);

         EXECUTE IMMEDIATE l_sql;

         COMMIT;

         BEGIN
            UPDATE xxwc_print_requests_temp
               SET process_flag = 2,
                   last_update_date = SYSDATE,
                   last_updated_by = p_user_id
             WHERE ROWID = r_print.ROWID
               AND process_flag = p_process_flag
               AND batch_id = p_batch_id;
         EXCEPTION
            WHEN OTHERS
            THEN
               RAISE l_exception;
         END;
         --Start Ver 12.0
         IF r_print.program IN ('XXWC_OM_PICK_SLIP','XXWC_INT_ORD_PICK') THEN
            BEGIN
			   BEGIN
                 SELECT value 
				   INTO ln_header_id 
				   FROM xxwc_print_requests_arg_temp 
                  WHERE GROUP_ID=p_group_id 
				    AND BATCH_ID=p_batch_id 
					AND argument=2;

                  SELECT value 
				    INTO l_print_org_id 
					FROM xxwc_print_requests_arg_temp 
                   WHERE GROUP_ID=p_group_id 
				     AND BATCH_ID=p_batch_id 
					 AND argument=1;
			   EXCEPTION
               WHEN OTHERS
               THEN
                  RAISE l_exception;
               END;
			   
                SELECT COUNT(1) INTO l_reprint_ind
                 FROM xxwc.xxwc_om_so_hdr_pick_aud_tbl xosh
                 WHERE xosh.header_id = ln_header_id;
                 
                  SELECT count(1) INTO l_printer_loc
                    FROM wsh_report_printers wrp,
                         fnd_concurrent_programs_vl fcp,
                         org_organization_definitions od
                   WHERE     od.organization_id = wrp.organization_id(+)
                         AND wrp.concurrent_program_id = fcp.concurrent_program_id
                         AND wrp.level_type_id = 10008
                         AND fcp.user_concurrent_program_name='XXWC Internal Order Pick Slip'         
                         and wrp.ENABLED_FLAG='Y'
                         and wrp.DEFAULT_PRINTER_FLAG='Y'
                         and wrp.PRINTER_NAME= r_print.printer
                         and od.organization_id=(SELECT ship_from_org_id 
                                                   FROM oe_order_headers_all 
                                                  WHERE header_id = ln_header_id);  

                IF (r_print.program = 'XXWC_OM_PICK_SLIP' )
                    --if internal order and printer belog to shipping location rather than receiving location                
                    OR (r_print.program = 'XXWC_INT_ORD_PICK' AND l_printer_loc > 0)
                    --if internal order and not automatic printing
                    OR (r_print.program = 'XXWC_INT_ORD_PICK' AND l_reprint_ind >= 1 
                        AND FND_GLOBAL.USER_NAME <> 'XXWC_INT_SALESFULFILLMENT')
                THEN
                   APPS.XXWC_ONT_ROUTINES_PKG.XXWC_LOG_PICKSLIP_PRINT_HIST
                                            (p_header_id => ln_header_id,
                                             p_printer   => r_print.printer, 
                                p_copies    => r_print.copies,
                                p_prnt_org_id => l_print_org_id
                                ); 
                END IF;                                          
            END;     
         END IF;
         --End Ver 12.0
      END LOOP;

      COMMIT;
      l_return := x_request_id;
      RETURN l_return;
   EXCEPTION
      WHEN OTHERS
      THEN
         ROLLBACK;
         l_return := '0';
         RETURN l_return;
   END submit_print_batch;

   FUNCTION get_printer (
      p_ccp_application           IN   VARCHAR2,
      p_concurrent_program_name   IN   VARCHAR2,
      p_organization_id           IN   NUMBER,
      p_user_id                   IN   NUMBER,
      p_resp_id                   IN   NUMBER,
      p_resp_appl_id              IN   NUMBER
   )
      RETURN VARCHAR2
   IS
      l_exception         EXCEPTION;
      l_printer_profile   VARCHAR2 (30) := fnd_profile.VALUE ('PRINTER');
      l_printer_org       VARCHAR2 (30) DEFAULT NULL;
      l_printer_user      VARCHAR2 (30) DEFAULT NULL;
      l_printer           VARCHAR2 (30);
   BEGIN
      --fnd_global.apps_initialize (p_user_id, p_resp_id, p_resp_appl_id);
      --mo_global.init ('ONT');

      --Retrieve Default Printer Profile
      IF     p_ccp_application IS NOT NULL
         AND p_concurrent_program_name IS NOT NULL
      THEN
         IF p_organization_id IS NOT NULL
         THEN
            l_printer_org :=
               xxwc_ont_routines_pkg.default_org_printer
                                                  (p_ccp_application,
                                                   p_concurrent_program_name,
                                                   p_organization_id,
                                                   p_user_id,
                                                   p_resp_id,
                                                   p_resp_appl_id
                                                  );
         END IF;

         IF p_user_id IS NOT NULL
         THEN
            l_printer_user :=
               xxwc_ont_routines_pkg.default_user_printer
                                                  (p_ccp_application,
                                                   p_concurrent_program_name,
                                                   p_organization_id,
                                                   p_user_id,
                                                   p_resp_id,
                                                   p_resp_appl_id
                                                  );
         END IF;
      END IF;

      --Go to the user level first
      IF l_printer_user IS NOT NULL
      THEN
         l_printer := l_printer_user;
      ELSE
         l_printer := l_printer_org;
      END IF;

      l_printer := NVL (l_printer, l_printer_profile);
      RETURN l_printer;
   EXCEPTION
      WHEN OTHERS
      THEN
         RETURN l_printer_profile;
   END get_printer;

   FUNCTION default_org_printer (
      p_ccp_application           IN   VARCHAR2,
      p_concurrent_program_name   IN   VARCHAR2,
      p_organization_id           IN   NUMBER,
      p_user_id                   IN   NUMBER,
      p_resp_id                   IN   NUMBER,
      p_resp_appl_id              IN   NUMBER
   )
      RETURN VARCHAR2
   IS
      l_printer_org   VARCHAR2 (30) DEFAULT NULL;
   BEGIN
      -- fnd_global.apps_initialize (p_user_id, p_resp_id, p_resp_appl_id);
      -- mo_global.init ('ONT');

      --Retrieve Default Printer Profile
      BEGIN
         SELECT wrp.printer_name
           INTO l_printer_org
           FROM wsh_report_printers wrp,
                fnd_concurrent_programs fcp,
                fnd_application fa
          WHERE level_type_id = 10008                           --Organization
            AND NVL (wrp.enabled_flag, 'N') = 'Y'
            AND NVL (wrp.default_printer_flag, 'N') = 'Y'
            AND wrp.concurrent_program_id = fcp.concurrent_program_id
            AND fcp.concurrent_program_name = p_concurrent_program_name
            AND fcp.application_id = fa.application_id
            AND fa.application_short_name = p_ccp_application
            AND wrp.level_value_id = p_organization_id;
      EXCEPTION
         WHEN OTHERS
         THEN
            l_printer_org := NULL;
      END;

      RETURN l_printer_org;
   END default_org_printer;

   FUNCTION default_user_printer (
      p_ccp_application           IN   VARCHAR2,
      p_concurrent_program_name   IN   VARCHAR2,
      p_organization_id           IN   NUMBER,
      p_user_id                   IN   NUMBER,
      p_resp_id                   IN   NUMBER,
      p_resp_appl_id              IN   NUMBER
   )
      RETURN VARCHAR2
   IS
      l_printer_user   VARCHAR2 (30) DEFAULT NULL;
   BEGIN
      --fnd_global.apps_initialize (p_user_id, p_resp_id, p_resp_appl_id);
      --mo_global.init ('ONT');

      --Retrieve Default Printer Profile
      l_printer_user := NULL;

      BEGIN
         SELECT wrp.printer_name
           INTO l_printer_user
           FROM wsh_report_printers wrp,
                fnd_concurrent_programs fcp,
                fnd_application fa
          WHERE level_type_id = 10004                                   --User
            AND NVL (wrp.enabled_flag, 'N') = 'Y'
            AND NVL (wrp.default_printer_flag, 'N') = 'Y'
            AND wrp.concurrent_program_id = fcp.concurrent_program_id
            AND fcp.concurrent_program_name = p_concurrent_program_name
            AND fcp.application_id = fa.application_id
            AND fa.application_short_name = p_ccp_application
            AND wrp.level_value_id = p_user_id;
      EXCEPTION
         WHEN OTHERS
         THEN
            l_printer_user := NULL;
      END;

      RETURN l_printer_user;
   END default_user_printer;

  PROCEDURE check_available (
      p_header_id   IN       NUMBER,
      p_organization_id IN   NUMBER, --added 10/26/2012
      x_count       OUT      NUMBER,
      x_message     OUT      VARCHAR2
   )
/*************************************************************************
  $Header xxwc_ont_routines_pkg$
  Module Name: xxwc_ont_routines_pkg.pkb

  PURPOSE:   This package will contain all Order Management specific routines

  REVISIONS:
  Ver        Date        Author                     Description
  ---------  ----------  ---------------         -------------------------
  1.0        5/9/2012     LCG Consulting           Initial Version  
  8.0        03/09/2016   Manjula Chellappan       TMS# 20150622-00156 Shipping Extension modification for PUBD
**************************************************************************/   
   IS
      CURSOR c_lines
      IS
         SELECT oola.ROWID, oola.inventory_item_id, oola.ordered_quantity,
                oola.subinventory, oola.ship_from_org_id, oola.line_number,
                oola.shipment_number, msik.concatenated_segments,
                oola.order_quantity_uom, --added 10242012
                msik.primary_uom_code, --added 10242012
                oola.line_id, --added 10242012                
--              oola.attribute11 --Added TMS Ticket 20130501-03903 -- Commented for Ver 8.0
                DECODE(oola.subinventory,'PUBD', oola.ordered_quantity, oola.attribute11) attribute11  --Added for Ver 8.0                
           FROM apps.oe_order_lines oola, mtl_system_items_kfv msik
          WHERE oola.header_id = p_header_id
            AND oola.flow_status_code = 'AWAITING_SHIPPING'
            AND oola.inventory_item_id = msik.inventory_item_id
            AND oola.ship_from_org_id = msik.organization_id
            AND NVL(msik.mtl_transactions_enabled_flag,'N') = 'Y'  --Exclude Non-Transactable Items added 10242012
            AND oola.ship_from_org_id = nvl(p_organization_id,oola.ship_from_org_id); --added 10/26/2012


      l_count            NUMBER           DEFAULT 0;
      l_message          VARCHAR2 (10000) DEFAULT NULL;
      l_available_qty    NUMBER;
      l_back_order_qty   NUMBER;
      l_ordered_qty      NUMBER;
      l_reserved_qty     NUMBER;
      l_force_ship_qty   NUMBER;
   BEGIN
      FOR r_lines IN c_lines
      LOOP
         l_available_qty :=
            xxwc_ont_routines_pkg.get_onhand
                           (p_inventory_item_id      => r_lines.inventory_item_id,
                            p_organization_id        => r_lines.ship_from_org_id,
                            p_subinventory           => r_lines.subinventory,
                            p_return_type            => 'H' --Changed from T to R --10242012 --Changed from R to H TMS Ticket 06/06/2013 20130501-03903
                           );
         --Added 10242012
         l_ordered_qty :=
            inv_convert.inv_um_convert
                                      (item_id            => r_lines.inventory_item_id,
                                       PRECISION          => 5,
                                       from_quantity      => r_lines.ordered_quantity,
                                       from_unit          => r_lines.order_quantity_uom,
                                       to_unit            => r_lines.primary_uom_code,
                                       from_name          => NULL,
                                       to_name            => NULL
                                      );

         -- 08/21/2013 CG: TMS 20130715-00472: Added to account for Force Ship Qty being taken to base UOM
         l_force_ship_qty :=
            inv_convert.inv_um_convert
                                      (item_id            => r_lines.inventory_item_id,
                                       PRECISION          => 5,
                                       from_quantity      => to_number(r_lines.attribute11),
                                       from_unit          => r_lines.order_quantity_uom,
                                       to_unit            => r_lines.primary_uom_code,
                                       from_name          => NULL,
                                       to_name            => NULL
                                      );
         
         BEGIN
          SELECT NVL (SUM (mr.primary_reservation_quantity), 0)
          INTO       l_reserved_qty
          FROM mtl_reservations mr
          WHERE mr.demand_source_line_id = r_lines.line_id
          AND mr.inventory_item_id = r_lines.inventory_item_id
          AND mr.organization_id = r_lines.ship_from_org_id;
         EXCEPTION
            WHEN others then
                l_reserved_qty := 0;
         END;

         if l_reserved_qty > 0 then

            l_back_order_qty := nvl(l_reserved_qty,0) - nvl(l_ordered_qty,0);

         --added TMS Ticket 20130501-03903
         
         elsif r_lines.attribute11 IS NOT NULL and r_lines.attribute11 > 0 THEN
         
            -- 08/21/2013 CG: TMS 20130715-00472: Added to account for Force Ship Qty being taken to base UOM
            -- l_back_order_qty := nvl(r_lines.attribute11,0) - nvl(l_ordered_qty,0);
            l_back_order_qty := nvl(l_force_ship_qty,0) - nvl(l_ordered_qty,0);
         
         --end added TMS Ticket 20130501-03903
         
         else

            l_back_order_qty := nvl(l_available_qty,0) - nvl(l_ordered_qty,0); --Added consider UOM Conversion 10242012

         end if;

        --End Added 102412

            --l_back_order_qty := l_available_qty - r_lines.ordered_quantity; removed 10242012


         IF l_back_order_qty < 0
         THEN
            l_count := 0 + 1;

            IF l_message IS NULL
            THEN
               l_message :=
                        r_lines.line_number || '.' || r_lines.shipment_number;
            ELSE
               l_message :=
                     l_message
                  || ', '
                  || r_lines.line_number
                  || '.'
                  || r_lines.shipment_number;
            END IF;
         END IF;
      END LOOP;

      x_count := l_count;
      x_message := l_message;
   END check_available;

   PROCEDURE check_lot_reservation (
      p_header_id   IN       NUMBER,
      p_organization_id IN   NUMBER, --added 10/26/2012
      x_lot_count   OUT      NUMBER,
      x_check       OUT      VARCHAR2,
      x_message     OUT      VARCHAR2
   )
   IS
      CURSOR c_lots
      IS
         SELECT   oola.ROWID, oola.line_id, oola.line_number,
                  oola.shipment_number, oola.ship_from_org_id,
                  oola.inventory_item_id
             FROM apps.oe_order_lines oola
            WHERE oola.header_id = p_header_id
              AND oola.ship_from_org_id = nvl(p_organization_id,oola.ship_from_org_id) --added 10/26/2012
              AND oola.flow_status_code = 'AWAITING_SHIPPING'
              AND oola.attribute11 IS NULL
              AND EXISTS (
                     SELECT *
                       FROM mtl_system_items_b msib
                      WHERE oola.inventory_item_id = msib.inventory_item_id
                        AND oola.ship_from_org_id = msib.organization_id
                        AND msib.lot_control_code = 2)
         ORDER BY oola.line_number, oola.shipment_number;

      l_count          NUMBER           DEFAULT 0;
      l_message        VARCHAR2 (10000) DEFAULT '';
      l_cursor_count   NUMBER;
      l_check          VARCHAR2 (1)     DEFAULT 'N';
      r_lots           c_lots%ROWTYPE;
   BEGIN
      IF c_lots%ISOPEN
      THEN
         CLOSE c_lots;
      END IF;

      OPEN c_lots;

      FETCH c_lots
       INTO r_lots;

      l_cursor_count := c_lots%ROWCOUNT;

      WHILE c_lots%FOUND
      LOOP
         SELECT COUNT (*)
           INTO l_count
           FROM mtl_reservations mr
          WHERE EXISTS (
                   SELECT *
                     FROM apps.oe_order_lines oola
                    WHERE mr.demand_source_line_id = r_lots.line_id
                      AND mr.inventory_item_id = r_lots.inventory_item_id
                      AND mr.organization_id = r_lots.ship_from_org_id
                      AND oola.line_id = r_lots.line_id);

         IF l_count = 0
         THEN
            --set check flag to Y
            l_check := 'Y';

            IF l_message IS NULL
            THEN
               l_message :=
                          r_lots.line_number || '.' || r_lots.shipment_number;
            ELSE
               l_message :=
                     l_message
                  || ', '
                  || r_lots.line_number
                  || '.'
                  || r_lots.shipment_number;
            END IF;
         END IF;

         FETCH c_lots
          INTO r_lots;
      END LOOP;

      x_lot_count := l_cursor_count;
      x_check := l_check;
      x_message := l_message;

      CLOSE c_lots;
   END check_lot_reservation;

   PROCEDURE check_lot_fs_reservation (
      p_header_id   IN       NUMBER,
      p_organization_id IN   NUMBER, --added 10/26/2012
      x_lot_count   OUT      NUMBER,
      x_check       OUT      VARCHAR2,
      x_message     OUT      VARCHAR2
   )
   IS
      CURSOR c_lots
      IS
         SELECT   oola.ROWID, oola.line_id, oola.line_number,
                  oola.shipment_number, oola.ordered_quantity,
                  oola.inventory_item_id, oola.attribute11,
                  oola.order_quantity_uom, oola.ship_from_org_id
             FROM apps.oe_order_lines oola
            WHERE oola.header_id = p_header_id
              AND oola.ship_from_org_id = nvl(p_organization_id,oola.ship_from_org_id) --added 10/26/2012
              AND oola.flow_status_code = 'AWAITING_SHIPPING'
              AND oola.attribute11 IS NOT NULL
              AND EXISTS (
                     SELECT *
                       FROM mtl_system_items_b msib
                      WHERE oola.inventory_item_id = msib.inventory_item_id
                        AND oola.ship_from_org_id = msib.organization_id
                        AND msib.lot_control_code = 2)
         ORDER BY oola.line_number, oola.shipment_number;

      l_count              NUMBER           DEFAULT 0;
      l_message            VARCHAR2 (10000) DEFAULT '';
      l_cursor_count       NUMBER;
      l_check              VARCHAR2 (1)     DEFAULT 'N';
      r_lots               c_lots%ROWTYPE;
      l_ordered_qty        NUMBER;
      l_force_ship_qty     NUMBER;
      l_reserved_qty       NUMBER;
      l_back_ordered_qty   NUMBER;
      l_primary_uom_code   VARCHAR2 (3);
   BEGIN
      IF c_lots%ISOPEN
      THEN
         CLOSE c_lots;
      END IF;

      OPEN c_lots;

      FETCH c_lots
       INTO r_lots;

      l_cursor_count := c_lots%ROWCOUNT;

      WHILE c_lots%FOUND
      LOOP
         BEGIN
            SELECT primary_uom_code
              INTO l_primary_uom_code
              FROM mtl_system_items_b
             WHERE inventory_item_id = r_lots.inventory_item_id
               AND organization_id = r_lots.ship_from_org_id;
         EXCEPTION
            WHEN OTHERS
            THEN
               l_primary_uom_code := r_lots.order_quantity_uom;
         END;

         BEGIN
            l_force_ship_qty :=
               NVL
                  (inv_convert.inv_um_convert
                                      (item_id            => r_lots.inventory_item_id,
                                       PRECISION          => 5,
                                       from_quantity      => r_lots.attribute11,
                                       from_unit          => r_lots.order_quantity_uom,
                                       to_unit            => l_primary_uom_code,
                                       from_name          => NULL,
                                       to_name            => NULL
                                      ),
                   0
                  );
         EXCEPTION
            WHEN OTHERS
            THEN
               l_force_ship_qty := 0;
         END;

         SELECT NVL (COUNT (mr.reservation_id), 0),
                NVL (SUM (mr.primary_reservation_quantity), 0)
           INTO l_count,
                l_reserved_qty
           FROM mtl_reservations mr
          WHERE EXISTS (
                   SELECT *
                     FROM apps.oe_order_lines oola
                    WHERE mr.demand_source_line_id = r_lots.line_id
                      AND mr.inventory_item_id = r_lots.inventory_item_id
                      AND mr.organization_id = r_lots.ship_from_org_id
                      AND oola.line_id = r_lots.line_id);

         l_back_ordered_qty := l_force_ship_qty - l_reserved_qty;

         IF l_back_ordered_qty > 0
         THEN
            l_check := 'Y';

            IF l_message IS NULL
            THEN
               l_message :=
                     r_lots.line_number
                  || '.'
                  || r_lots.shipment_number
                  || ' BO Qty : '
                  || l_back_ordered_qty
                  || ' ';
            ELSE
               l_message :=
                     l_message
                  || ', '
                  || r_lots.line_number
                  || '.'
                  || r_lots.shipment_number
                  || ' BO Qty : '
                  || l_back_ordered_qty
                  || ' ';
            END IF;
         END IF;

         FETCH c_lots
          INTO r_lots;
      END LOOP;

      x_lot_count := l_cursor_count;
      x_check := l_check;
      x_message := l_message;

      CLOSE c_lots;
   END check_lot_fs_reservation;

   FUNCTION org_printer (
      p_ccp_application           IN   VARCHAR2,
      p_concurrent_program_name   IN   VARCHAR2,
      p_printer_name              IN   VARCHAR2
   )
      RETURN NUMBER
   IS
      l_organization_id   NUMBER;
   BEGIN
      BEGIN
         SELECT TO_NUMBER (wrp.level_value_id)
           INTO l_organization_id
           FROM wsh_report_printers wrp,
                fnd_concurrent_programs fcp,
                fnd_application fa
          WHERE level_type_id = 10008                           --Organization
            AND NVL (wrp.enabled_flag, 'N') = 'Y'
            --AND NVL (wrp.default_printer_flag, 'N') = 'Y'
            AND wrp.concurrent_program_id = fcp.concurrent_program_id
            AND fcp.concurrent_program_name = p_concurrent_program_name
            AND fcp.application_id = fa.application_id
            AND fa.application_short_name = p_ccp_application
            AND wrp.printer_name = p_printer_name;
      EXCEPTION
         WHEN NO_DATA_FOUND
         THEN
            l_organization_id := NULL;
         WHEN OTHERS
         THEN
            l_organization_id := NULL;
      END;

      RETURN l_organization_id;
   END org_printer;

   FUNCTION internal_order_check (p_header_id IN NUMBER)
      RETURN VARCHAR2
   IS
      l_return                   VARCHAR2 (1);
      l_deliver_to_location_id   NUMBER;
      l_stock_location_id        NUMBER;
      l_requisition_header_id    NUMBER;
   BEGIN
      BEGIN
         SELECT ooha.source_document_id
           INTO l_requisition_header_id
           FROM apps.oe_order_headers ooha
          WHERE ooha.header_id = p_header_id
            AND ooha.source_document_type_id = 10;               --Requisition
      EXCEPTION
         WHEN OTHERS
         THEN
            l_requisition_header_id := NULL;
      END;

      IF l_requisition_header_id IS NOT NULL
      THEN
         BEGIN
            SELECT DISTINCT prla.deliver_to_location_id, mp.attribute15
                       INTO l_deliver_to_location_id, l_stock_location_id
                       FROM apps.po_requisition_headers prha,
                            apps.po_requisition_lines prla,
                            hr_locations hl,
                            mtl_parameters mp
                      WHERE prha.requisition_header_id =
                                                    prla.requisition_header_id
                        AND prla.deliver_to_location_id = hl.location_id
                        AND prla.destination_organization_id =
                                                            mp.organization_id
                        AND prha.requisition_header_id =
                                                       l_requisition_header_id;
         EXCEPTION
            WHEN OTHERS
            THEN
               l_return := 'X';
         END;

         IF l_deliver_to_location_id = l_stock_location_id
         THEN
            -- 08/09/2012 Flipped to be P, instead of S
            l_return := 'P';
         ELSE
            l_return := 'S';
         END IF;
      ELSE
         l_return := 'N';
      END IF;

      RETURN l_return;
   EXCEPTION
      WHEN OTHERS
      THEN
         RETURN 'E';                  --default N if we run into an exception
   END;

   PROCEDURE submit_internal_order_pick_rpt (
      errbuf            OUT      VARCHAR2,
      retcode           OUT      VARCHAR2,
      p_order_number    IN       NUMBER,
      p_creation_date   IN       VARCHAR2,
      p_copies          IN       NUMBER                       --added 08142012
   )
   IS
      -- 08/16/2012 CG: Edited to remove duplicate lines caused by multiple line reqs
      CURSOR cur_get_orgs
      IS
         SELECT DISTINCT (req_hdr.segment1) req_number,
                                                       --req_ln.line_num req_line_number,
                                                       ooha.order_number,

                         --oola.line_number,
                         req_hdr.requisition_header_id,
                                                       --req_ln.requisition_line_id,
                                                       ooha.header_id,

                         --oola.line_id,
                         req_ln.destination_organization_id,
                         mp_to.organization_code ship_to_org,
                         req_ln.source_organization_id,
                         mp_from.organization_code ship_from_org,
                         oola.ship_from_org_id,
                         xxwc_ont_routines_pkg.internal_order_check
                                                   (ooha.header_id)
                                                                  order_type
                    FROM apps.po_requisition_headers req_hdr,
                         apps.po_requisition_lines req_ln,
                         apps.oe_order_headers ooha,
                         oe_order_sources oos,
                         apps.oe_order_lines oola,
                         mtl_parameters mp_from,
                         org_organization_definitions ood_from,
                         mtl_parameters mp_to,
                         org_organization_definitions ood_to,
                         mtl_system_items_b msib,
                         hr_operating_units hou
                   WHERE req_hdr.type_lookup_code = 'INTERNAL'
                     AND req_hdr.requisition_header_id =
                                                  req_ln.requisition_header_id
                     AND req_ln.requisition_header_id =
                                                       oola.source_document_id
                     AND req_ln.requisition_line_id =
                                                  oola.source_document_line_id
                     AND oola.header_id = ooha.header_id
                     AND ooha.order_source_id = oos.order_source_id
                     AND oos.NAME = 'Internal'
                     AND req_ln.source_organization_id =
                                                       mp_from.organization_id
                     AND mp_from.organization_id = ood_from.organization_id
                     AND req_ln.destination_organization_id =
                                                         mp_to.organization_id
                     AND mp_to.organization_id = ood_to.organization_id
                     AND req_ln.item_id = msib.inventory_item_id
                     AND req_ln.destination_organization_id =
                                                          msib.organization_id
                     AND ood_from.operating_unit = hou.organization_id
                     AND TRUNC (ooha.creation_date) =
                            NVL (TRUNC (TO_DATE (p_creation_date,
                                                 'YYYY/MM/DD HH24:MI:SS'
                                                )
                                       ),
                                 TRUNC (SYSDATE)
                                )
                     AND ooha.order_number =
                                       NVL (p_order_number, ooha.order_number)
                     AND xxwc_ont_routines_pkg.internal_order_check
                                                               (ooha.header_id) IN
                                                                   ('S', 'P')
                     AND NOT EXISTS (
                            SELECT 'Not Printed'
                              FROM xxwc.xxwc_internal_so_pick_rpt_tbl x1
                             WHERE x1.requisition_header_id =
                                                 req_hdr.requisition_header_id
                               --AND x1.requisition_line_id = req_ln.requisition_line_id
                               AND x1.header_id = ooha.header_id
                                                                --AND x1.line_id = oola.line_id
                         )
                ORDER BY 3, 4;

      CURSOR cur_process
      IS
         SELECT x1.ROWID row_id, x1.*
           FROM xxwc.xxwc_internal_so_pick_rpt_tbl x1
          WHERE x1.status = 'NEW';

      l_group_id           NUMBER;
      l_from_batch_id      NUMBER;
      l_to_batch_id        NUMBER;
      l_from_org_printer   VARCHAR2 (30);
      l_to_org_printer     VARCHAR2 (30);
      l_request_id         NUMBER;
   BEGIN
      xxwc_ont_routines_pkg.write_log
               ('Starting XXWC Print Internal Sales Orders Pick Slip Process');
      xxwc_ont_routines_pkg.write_log
               ('===========================================================');
      xxwc_ont_routines_pkg.write_log ('Initializing variables');
      fnd_global.apps_initialize (fnd_global.user_id,
                                  fnd_global.resp_id,
                                  fnd_global.resp_appl_id
                                 );
      mo_global.init ('ONT');
      xxwc_ont_routines_pkg.write_log ('Variables initialized...');
      xxwc_ont_routines_pkg.write_log
                ('===========================================================');
      xxwc_ont_routines_pkg.write_log (' ');
      -- Initialize Group ID
      l_group_id := NULL;

      SELECT xxwc_print_request_groups_s.NEXTVAL
        INTO l_group_id
        FROM DUAL;

      FOR c1 IN cur_get_orgs
      LOOP
         EXIT WHEN cur_get_orgs%NOTFOUND;

         -- Initialize batch ID
         SELECT xxwc_print_requests_s.NEXTVAL
           INTO l_from_batch_id
           FROM DUAL;

         SELECT xxwc_print_requests_s.NEXTVAL
           INTO l_to_batch_id
           FROM DUAL;

         -- Pull Org Printer for From Org
         l_from_org_printer := NULL;

         BEGIN
            SELECT xxwc_ont_routines_pkg.default_org_printer
                                                      ('XXWC',
                                                       'XXWC_INT_ORD_PICK',
                                                       c1.ship_from_org_id,
                                                       fnd_global.user_id,
                                                       fnd_global.resp_id,
                                                       fnd_global.resp_appl_id
                                                      )
              INTO l_from_org_printer
              FROM DUAL;
         EXCEPTION
            WHEN OTHERS
            THEN
               l_from_org_printer := NULL;
         END;

         -- Pull Org Printer for To Org
         l_to_org_printer := NULL;

         BEGIN
            SELECT xxwc_ont_routines_pkg.default_org_printer
                                              ('XXWC',
                                               'XXWC_INT_ORD_PICK',
                                               c1.destination_organization_id,
                                               fnd_global.user_id,
                                               fnd_global.resp_id,
                                               fnd_global.resp_appl_id
                                              )
              INTO l_to_org_printer
              FROM DUAL;
         EXCEPTION
            WHEN OTHERS
            THEN
               l_to_org_printer := NULL;
         END;

         xxwc_ont_routines_pkg.write_log
            ('*********************************************************************************'
            );
         xxwc_ont_routines_pkg.write_log (   'Printing selections for Req '
                                          || c1.req_number
                                         /*|| ' line number '
                                         || c1.req_line_number*/
                                         );
         xxwc_ont_routines_pkg.write_log ('On Sales Order ' || c1.order_number
                                                                              /*|| ' line number '
                                                                              || c1.line_number*/
         );
         xxwc_ont_routines_pkg.write_log (   'From Org/Printer/Batch Id: '
                                          || c1.ship_from_org
                                          || '/'
                                          || l_from_org_printer
                                          || '/'
                                          || l_from_batch_id
                                         );
         xxwc_ont_routines_pkg.write_log (   'To Org/Printer/Batch Id: '
                                          || c1.ship_to_org
                                          || '/'
                                          || l_to_org_printer
                                          || '/'
                                          || l_to_batch_id
                                         );

         -- Insert Once into XXWC.XXWC_PRINT_REQUESTS_TEMP and XXWC_PRINT_REQUESTS_ARG_TEMP and XXWC.XXWC_INTERNAL_SO_PICK_RPT_TBL for From Org
         IF l_from_org_printer IS NOT NULL AND c1.order_type IN ('P', 'S')
         THEN
            INSERT INTO xxwc.xxwc_print_requests_temp
                        (created_by, creation_date,
                         last_updated_by, last_update_date,
                         batch_id, process_flag,
                         GROUP_ID, application,
                         program,
                         description,
                         start_time, sub_request,
                         printer, style,
                         copies,
                         save_output, print_together,
                         validate_printer,
                         template_appl_name,
                         template_code,
                         template_language,
                         template_territory, output_format,
                         nls_language
                        )
                 VALUES (fnd_global.user_id                      -- created_by
                                           , SYSDATE,
                         fnd_global.user_id                  -- last_udated_by
                                           , SYSDATE,
                         l_from_batch_id                           -- BATCH_ID
                                        , '1'                  -- PROCESS_FLAG
                                             ,
                         l_group_id                                -- GROUP_ID
                                   , 'XXWC'                     -- APPLICATION
                                           ,
                         'XXWC_INT_ORD_PICK'                        -- PROGRAM
                                            ,
                         'XXWC Internal Order Pick Slip'        -- DESCRIPTION
                                                        ,
                         NULL                                    -- START_TIME
                             , 'FALSE'                          -- SUB_REQUEST
                                      ,
                         l_from_org_printer                         -- PRINTER
                                           , NULL                     -- STYLE
                                                 ,
                         --1                                           -- COPIES --removed 08142012
                         p_copies                  --COPIES   --added 08142012
                                 ,
                         'TRUE'                                 -- SAVE_OUTPUT
                               , 'N'                         -- PRINT_TOGETHER
                                    ,
                         'RESOLVE'                         -- VALIDATE_PRINTER
                                  ,
                         'XXWC'                          -- TEMPLATE_APPL_NAME
                               ,
                         'XXWC_INT_ORD_PICK'                  -- TEMPLATE_CODE
                                            ,
                         'en'                             -- TEMPLATE_LANGUAGE
                             ,
                         'US'                            -- TEMPLATE_TERRITORY
                             , 'PDF'                          -- OUTPUT_FORMAT
                                    ,
                         'en'                                  -- NLS_LANGUAGE
                        );

            INSERT INTO xxwc.xxwc_print_requests_arg_temp
                 VALUES (l_from_batch_id, 1, l_group_id, 1,
                         c1.ship_from_org_id);                 -- Organization

            INSERT INTO xxwc.xxwc_print_requests_arg_temp
                 VALUES (l_from_batch_id, 1, l_group_id, 2, c1.header_id);

            -- Order Number
            INSERT INTO xxwc.xxwc_print_requests_arg_temp
                 VALUES (l_from_batch_id, 1, l_group_id, 3, 2);

            -- Print Pricing
            INSERT INTO xxwc.xxwc_print_requests_arg_temp
                 VALUES (l_from_batch_id, 1, l_group_id, 4, NULL);  -- Reprint

            INSERT INTO xxwc.xxwc_print_requests_arg_temp
                 VALUES (l_from_batch_id, 1, l_group_id, 5, NULL);

            -- Print Kit Details
            INSERT INTO xxwc.xxwc_print_requests_arg_temp
                 VALUES (l_from_batch_id, 1, l_group_id, 6, 2);

            -- Send to Rightfax Yes or No?
            INSERT INTO xxwc.xxwc_print_requests_arg_temp
                 VALUES (l_from_batch_id, 1, l_group_id, 7, NULL);

           -- Fax Comment --Updated 2/13/2013
           INSERT INTO xxwc.xxwc_print_requests_arg_temp
                 VALUES (l_from_batch_id, 1, l_group_id, 8, NULL);

            --Hazmat --Updated 2/13/2013 changed argument from 8 to 9
            INSERT INTO xxwc.xxwc_print_requests_arg_temp
                 VALUES (l_from_batch_id, 1, l_group_id, 9, 2);

            -- Print Hazmat
            INSERT INTO xxwc.xxwc_internal_so_pick_rpt_tbl
                        (requisition_header_id,
                                               --requisition_line_id,
                                               header_id,
                         --line_id,
                         req_number,
                                    --req_line_number,
                                    order_number,
                                                 --line_number,
                                                 source_org_id,
                         destination_org_id, print_date,
                         print_batch_id, print_group_id, printer,
                         status, created_by, last_updated_by,
                         creation_date, last_updated_date
                        )
                 VALUES (c1.requisition_header_id,
                                                  -- c1.requisition_line_id,
                                                  c1.header_id,
                         -- c1.line_id,
                         c1.req_number,
                                       -- c1.req_line_number,
                                       c1.order_number,
                                                       -- c1.line_number,
                                                       c1.ship_from_org_id,
                         c1.destination_organization_id, SYSDATE,
                         l_from_batch_id, l_group_id, l_from_org_printer,
                         'NEW', fnd_global.user_id, fnd_global.user_id,
                         SYSDATE, SYSDATE
                        );
         ELSE
            xxwc_ont_routines_pkg.write_log
                                      ('Ship from org printer not defined...');
         END IF;

         -- Insert Once into XXWC.XXWC_PRINT_REQUESTS_TEMP and XXWC_PRINT_REQUESTS_ARG_TEMP and XXWC.XXWC_INTERNAL_SO_PICK_RPT_TBL for To Org
         IF l_to_org_printer IS NOT NULL AND c1.order_type IN ('S')
         THEN
            INSERT INTO xxwc.xxwc_print_requests_temp
                        (created_by, creation_date,
                         last_updated_by, last_update_date,
                         batch_id, process_flag,
                         GROUP_ID, application,
                         program,
                         description,
                         start_time, sub_request,
                         printer, style,
                         copies,
                         save_output, print_together,
                         validate_printer,
                         template_appl_name,
                         template_code,
                         template_language,
                         template_territory, output_format,
                         nls_language
                        )
                 VALUES (fnd_global.user_id                      -- created_by
                                           , SYSDATE,
                         fnd_global.user_id                  -- last_udated_by
                                           , SYSDATE,
                         l_to_batch_id                             -- BATCH_ID
                                      , '1'                    -- PROCESS_FLAG
                                           ,
                         l_group_id                                -- GROUP_ID
                                   , 'XXWC'                     -- APPLICATION
                                           ,
                         'XXWC_INT_ORD_PICK'                        -- PROGRAM
                                            ,
                         'XXWC Internal Order Pick Slip'        -- DESCRIPTION
                                                        ,
                         NULL                                    -- START_TIME
                             , 'FALSE'                          -- SUB_REQUEST
                                      ,
                         l_to_org_printer                           -- PRINTER
                                         , NULL                       -- STYLE
                                               ,
                         --1                                           -- COPIES --removed 08142012
                         p_copies                  --COPIES   --added 08142012
                                 ,
                         'TRUE'                                 -- SAVE_OUTPUT
                               , 'N'                         -- PRINT_TOGETHER
                                    ,
                         'RESOLVE'                         -- VALIDATE_PRINTER
                                  ,
                         'XXWC'                          -- TEMPLATE_APPL_NAME
                               ,
                         'XXWC_INT_ORD_PICK'                  -- TEMPLATE_CODE
                                            ,
                         'en'                             -- TEMPLATE_LANGUAGE
                             ,
                         'US'                            -- TEMPLATE_TERRITORY
                             , 'PDF'                          -- OUTPUT_FORMAT
                                    ,
                         'en'                                  -- NLS_LANGUAGE
                        );

            INSERT INTO xxwc.xxwc_print_requests_arg_temp
                 VALUES (l_to_batch_id, 1, l_group_id, 1, c1.ship_from_org_id);

            -- Organization
            INSERT INTO xxwc.xxwc_print_requests_arg_temp
                 VALUES (l_to_batch_id, 1, l_group_id, 2, c1.header_id);

            -- Order Number
            INSERT INTO xxwc.xxwc_print_requests_arg_temp
                 VALUES (l_to_batch_id, 1, l_group_id, 3, 2); -- Print Pricing

            INSERT INTO xxwc.xxwc_print_requests_arg_temp
                 VALUES (l_to_batch_id, 1, l_group_id, 4, NULL);    -- Reprint

            INSERT INTO xxwc.xxwc_print_requests_arg_temp
                 VALUES (l_to_batch_id, 1, l_group_id, 5, NULL);

            -- Print Kit Details
            INSERT INTO xxwc.xxwc_print_requests_arg_temp
                 VALUES (l_to_batch_id, 1, l_group_id, 6, 2);

            -- Send to Rightfax Yes or No?
            INSERT INTO xxwc.xxwc_print_requests_arg_temp
                 VALUES (l_to_batch_id, 1, l_group_id, 7, NULL); -- Fax Number

            INSERT INTO xxwc.xxwc_print_requests_arg_temp
                 VALUES (l_to_batch_id, 1, l_group_id, 8, 2);  -- Print Hazmat

            INSERT INTO xxwc.xxwc_internal_so_pick_rpt_tbl
                        (requisition_header_id,
                                               -- requisition_line_id,
                                               header_id,
                         -- line_id,
                         req_number,
                                    -- req_line_number,
                                    order_number,
                                                 -- line_number,
                                                 source_org_id,
                         destination_org_id, print_date,
                         print_batch_id, print_group_id, printer, status,
                         created_by, last_updated_by, creation_date,
                         last_updated_date
                        )
                 VALUES (c1.requisition_header_id,
                                                  -- c1.requisition_line_id,
                                                  c1.header_id,
                         -- c1.line_id,
                         c1.req_number,
                                       -- c1.req_line_number,
                                       c1.order_number,
                                                       -- c1.line_number,
                                                       c1.ship_from_org_id,
                         c1.destination_organization_id, SYSDATE,
                         l_to_batch_id, l_group_id, l_to_org_printer, 'NEW',
                         fnd_global.user_id, fnd_global.user_id, SYSDATE,
                         SYSDATE
                        );
         ELSE
            xxwc_ont_routines_pkg.write_log
                                        ('Ship to org printer not defined...');
         END IF;
      END LOOP;

      COMMIT;
      xxwc_ont_routines_pkg.write_log
                ('===========================================================');
      xxwc_ont_routines_pkg.write_log
                  ('Loaded tables for printing...starting call to submit jobs');
      xxwc_ont_routines_pkg.write_log
                ('===========================================================');

      -- New Loop to process the loaded data into XXWC.XXWC_INTERNAL_SO_PICK_RPT_TBL and XXWC.XXWC_PRINT_REQUESTS_TEMP
      FOR c2 IN cur_process
      LOOP
         EXIT WHEN cur_process%NOTFOUND;
         xxwc_ont_routines_pkg.write_log
            ('*********************************************************************************'
            );
         xxwc_ont_routines_pkg.write_log (   'Processing Req/Order/Printer: '
                                          || c2.req_number
                                          || '/'
                                          --|| c2.req_line_number
                                          --|| '/'
                                          || c2.order_number
                                          || '/'
                                          --|| c2.line_number
                                          --|| '/'
                                          || c2.printer
                                         );
         l_request_id := NULL;

         SELECT xxwc_ont_routines_pkg.submit_print_batch
                             (c2.print_batch_id                  -- p_batch_id
                                               ,
                              c2.print_group_id                  -- p_group_id
                                               ,
                              1                              -- p_process_flag
                               ,
                              fnd_global.user_id                  -- p_user_id
                                                ,
                              fnd_global.resp_id                  -- p_resp_id
                                                ,
                              fnd_global.resp_appl_id
                             )                               -- p_resp_appl_id
           INTO l_request_id
           FROM DUAL;

         --if l_request_id > 0 then
         xxwc_ont_routines_pkg.write_log ('Request ID: ' || l_request_id);

         UPDATE xxwc.xxwc_internal_so_pick_rpt_tbl x1
            SET x1.status = 'SUBMITTED',
                x1.request_id = l_request_id,
                x1.last_updated_date = SYSDATE,
                x1.last_updated_by = fnd_global.user_id
          WHERE ROWID = c2.row_id
            AND EXISTS (
                   SELECT 'Submitted'
                     FROM xxwc_print_requests_temp x2
                    WHERE x2.process_flag = 2
                      AND x2.GROUP_ID = x1.print_group_id
                      AND x2.batch_id = x1.print_batch_id);
      --else
      --    xxwc_ont_routines_pkg.write_log('Could not submit concurrent job. l_request_id '||l_request_id);
      --end if;
      END LOOP;

      COMMIT;
      xxwc_ont_routines_pkg.write_log
                ('===========================================================');
      xxwc_ont_routines_pkg.write_log ('Submission tasks completed');
   EXCEPTION
      WHEN OTHERS
      THEN
         xxwc_ont_routines_pkg.write_log
                         ('Error submitting requests for Internal Pick Slips');
         xxwc_ont_routines_pkg.write_log ('ERROR: ' || SQLERRM);
         RAISE;
   END submit_internal_order_pick_rpt;

   PROCEDURE insert_char_arg (
      batch_id   IN   NUMBER,
      GROUP_ID   IN   NUMBER,
      argument   IN   NUMBER,
      VALUE      IN   VARCHAR2
   )
   IS
   BEGIN
      INSERT INTO xxwc_print_requests_arg_temp
           VALUES (batch_id, 1, GROUP_ID, argument,
                   CHR (39) || VALUE || CHR (39));
   EXCEPTION
      WHEN OTHERS
      THEN
         RAISE;
   END insert_char_arg;

   FUNCTION get_user_profile_printer (p_user_id IN NUMBER)
      RETURN VARCHAR2
   IS
      l_printer   VARCHAR2 (240);
   BEGIN
      l_printer := NULL;

      SELECT profile_option_value
        INTO l_printer
        FROM fnd_profile_option_values
       WHERE profile_option_id = 109                                -- printer
         AND level_id = 10004                                          -- user
         AND level_value = p_user_id;

      RETURN l_printer;
   EXCEPTION
      WHEN OTHERS
      THEN
         RETURN NULL;
   END get_user_profile_printer;


   PROCEDURE fulfillment_acceptance(
      errbuf            OUT      VARCHAR2,
      retcode           OUT      VARCHAR2,
      p_header_id       IN       NUMBER)
   IS
   l_header_rec              Oe_Order_Pub.Header_Rec_Type;
   o_header_rec              Oe_order_pub.Header_Rec_Type ;
   o_header_val_rec          Oe_Order_Pub.Header_Val_Rec_Type;
   o_header_adj_tbl          Oe_Order_Pub.Header_Adj_Tbl_Type;
   o_header_adj_val_tbl      Oe_Order_Pub.Header_Adj_Val_Tbl_Type;
   o_header_price_att_tbl    Oe_Order_Pub.Header_Price_Att_Tbl_Type;
   o_header_adj_att_tbl      Oe_Order_Pub.Header_Adj_Att_Tbl_Type;
   o_header_adj_assoc_tbl    Oe_Order_Pub.Header_Adj_Assoc_Tbl_Type;
   o_header_scredit_tbl      Oe_Order_Pub.Header_Scredit_Tbl_Type;
   o_header_scredit_val_tbl  Oe_Order_Pub.Header_Scredit_Val_Tbl_Type;
   l_line_tbl                Oe_Order_Pub.Line_Tbl_Type;
   o_line_tbl                Oe_Order_Pub.Line_Tbl_Type;
   b_line_tbl                Oe_Order_Pub.Line_Tbl_Type; -- empty table
   o_line_val_tbl            Oe_Order_Pub.Line_Val_Tbl_Type;
   o_line_adj_tbl            Oe_Order_Pub.Line_Adj_Tbl_Type;
   o_line_adj_val_tbl        Oe_Order_Pub.Line_Adj_Val_Tbl_Type;
   o_line_price_att_tbl      Oe_Order_Pub.Line_Price_Att_Tbl_Type;
   o_line_adj_att_tbl        Oe_Order_Pub.Line_Adj_Att_Tbl_Type;
   o_line_adj_assoc_tbl      Oe_Order_Pub.Line_Adj_Assoc_Tbl_Type;
   o_line_scredit_tbl        Oe_Order_Pub.Line_Scredit_Tbl_Type;
   o_line_scredit_val_tbl    Oe_Order_Pub.Line_Scredit_Val_Tbl_Type;
   o_lot_serial_tbl          Oe_Order_Pub.Lot_Serial_Tbl_Type;
   o_lot_serial_val_tbl      Oe_Order_Pub.Lot_Serial_Val_Tbl_Type;
   l_action_request_tbl      Oe_Order_Pub.Request_Tbl_Type;
   o_action_request_tbl      oe_order_pub.Request_Tbl_Type ;
   l_return_status           VARCHAR2 (240);
   l_msg_count               NUMBER;
   l_msg_data                VARCHAR2 (4000);
   --i                         NUMBER;
   l_request_rec             OE_ORDER_PUB.request_rec_type;
   
   l_line_tbl_index          NUMBER;
   
   l_hold_activity    VARCHAR2(250);
    l_item_type        VARCHAR2(10);
    l_activity         VARCHAR2(250);
    l_result           VARCHAR2(10);

   CURSOR c_hdr
       IS
   SELECT a.*
     FROM xxwc_om_fulfill_acceptance a, apps.oe_order_headers b
    WHERE a.header_id=b.header_id
      AND nvl(a.process_flag,'N')='N'
      AND a.header_id=nvl(p_header_id, a.header_id)
      AND b.flow_status_code='BOOKED'
      AND exists (select 'x' from apps.oe_order_lines l
                   where l.header_id=b.header_id
                     and l.flow_status_code='PRE-BILLING_ACCEPTANCE');

   CURSOR c_lines IS
   SELECT a.rowid rid
          , b.shipped_quantity
          , a.*
     FROM   xxwc_om_fulfill_acceptance a, 
            apps.oe_order_lines b
    WHERE a.header_id = nvl(p_header_id, a.header_id)
      AND nvl(a.process_flag,'N')='N'
      AND a.header_id=b.header_id
      AND a.line_id = b.line_id
      AND nvl(b.booked_flag, 'N') = 'Y'
      AND b.accepted_by is null
      AND b.flow_status_code = 'PRE-BILLING_ACCEPTANCE';

   /*
   CURSOR c_line(i_header_id number)
       IS
   SELECT a.line_id, a.fulfilled_quantity
     FROM oe_order_lines_all a
    WHERE a.header_id=i_header_id
      AND a.flow_status_code='PRE-BILLING_ACCEPTANCE';
*/

   BEGIN
      Oe_Msg_Pub.INITIALIZE;
      
      -- for c_hdr_rec in c_hdr
      for c1 in c_lines
      loop
        exit when c_lines%notfound;
        
            -- 04/29/2013 CG Commented
            /*
            -- ACTION REQUEST RECORD for acceptance
            -- line level action
            l_request_rec.entity_code := OE_GLOBALS.G_ENTITY_HEADER;
            -- line id
            l_request_rec.entity_id := c_hdr_rec.header_id;
            -- action requested
            l_request_rec.request_type := OE_GLOBALS.G_ACCEPT_FULFILLMENT;
            -- parameters
            l_request_rec.param1 := c_hdr_rec.acceptance_comments;
            l_request_rec.param2 := c_hdr_rec.accepted_signature;
            l_request_rec.param4 := 'N';
            -- customer signature date
            l_request_rec.date_param1 := c_hdr_rec.accepted_date;
            -- inserting request record into action request table
            l_action_request_tbl(1) := l_request_rec;*/
            -- 04/29/2013 CG Commented
            
/*
       for c_line_rec in c_line (c_hdr_rec.header_id)
           loop
            l_line_tbl(i).line_id := c_line_rec.line_id;
            l_line_tbl(i).header_id :=  c_hdr_rec.header_id;
            l_line_tbl(i).accepted_quantity :=  c_line_rec.fulfilled_quantity ;
            l_line_tbl(i).accepted_by  :=  c_hdr_rec.created_by;
            l_line_tbl(i).revrec_comments :=  c_hdr_rec.acceptance_comments ;
            l_line_tbl(i).revrec_signature :=  c_hdr_rec.accepted_signature ;
            l_line_tbl(i).revrec_signature_date :=  sysdate;
            l_line_tbl(i).revrec_implicit_flag :=  'N';
            l_line_tbl(i).operation := OE_GLOBALS.G_OPR_UPDATE;
             i := i + 1;
           end loop;
  */
            
            l_line_tbl_index := 1;

            l_line_tbl (l_line_tbl_index) := oe_order_pub.g_miss_line_rec;
            l_line_tbl (l_line_tbl_index).header_id := c1.header_id;
            l_line_tbl (l_line_tbl_index).line_id := c1.line_id;
            
            l_line_tbl(l_line_tbl_index).accepted_quantity :=  c1.shipped_quantity ;
            l_line_tbl(l_line_tbl_index).accepted_by  :=  c1.created_by;
            l_line_tbl(l_line_tbl_index).revrec_comments :=  c1.acceptance_comments ;
            l_line_tbl(l_line_tbl_index).revrec_signature :=  c1.accepted_signature ;
            l_line_tbl(l_line_tbl_index).revrec_signature_date :=  sysdate;
            l_line_tbl(l_line_tbl_index).revrec_implicit_flag :=  'N';
            
            begin
                SELECT papf.last_name
                        || (case when papf.last_name is not null then ', ' else null end)
                        || papf.first_name
                   INTO l_line_tbl(l_line_tbl_index).revrec_reference_document
                   FROM per_all_people_f papf, fnd_user fu
                  WHERE papf.person_id = fu.employee_id 
                  AND fu.user_id = c1.created_by
                  AND trunc(effective_start_date) <= trunc(sysdate)
                  AND nvl(trunc(effective_end_date), trunc(sysdate)) >= trunc(sysdate)
                  AND rownum = 1;
            exception
            when others then
                l_line_tbl(l_line_tbl_index).revrec_reference_document := null;
            end;
            l_line_tbl(l_line_tbl_index).operation := OE_GLOBALS.G_OPR_UPDATE;
            
            l_line_tbl (l_line_tbl_index).operation := oe_globals.g_opr_update;

            Oe_Order_Pub.process_order(
            p_api_version_number      => 1.0
          , p_header_rec              => l_header_rec
          , p_line_tbl                => l_line_tbl
          --, p_action_request_tbl      => l_action_request_tbl
          , x_return_status           => l_return_status
          , x_msg_count               => l_msg_count
          , x_msg_data                => l_msg_data
          , x_header_rec              => o_header_rec
          , x_header_val_rec          => o_header_val_rec
          , x_header_adj_tbl          => o_header_adj_tbl
          , x_header_adj_val_tbl      => o_header_adj_val_tbl
          , x_header_price_att_tbl    => o_header_price_att_tbl
          , x_header_adj_att_tbl      => o_header_adj_att_tbl
          , x_header_adj_assoc_tbl    => o_header_adj_assoc_tbl
          , x_header_scredit_tbl      => o_header_scredit_tbl
          , x_header_scredit_val_tbl  => o_header_scredit_val_tbl
          , x_line_tbl                => o_line_tbl
          , x_line_val_tbl            => o_line_val_tbl
          , x_line_adj_tbl            => o_line_adj_tbl
          , x_line_adj_val_tbl        => o_line_adj_val_tbl
          , x_line_price_att_tbl      => o_line_price_att_tbl
          , x_line_adj_att_tbl        => o_line_adj_att_tbl
          , x_line_adj_assoc_tbl      => o_line_adj_assoc_tbl
          , x_line_scredit_tbl        => o_line_scredit_tbl
          , x_line_scredit_val_tbl    => o_line_scredit_val_tbl
          , x_lot_serial_tbl          => o_lot_serial_tbl
          , x_lot_serial_val_tbl      => o_lot_serial_val_tbl
          , x_action_request_tbl      => o_action_request_tbl
          );

          IF (l_return_status != Fnd_Api.g_ret_sts_success) THEN
            IF (l_msg_count > 0) THEN
                FOR i IN 1..l_msg_count LOOP
                    l_msg_data := SUBSTR(l_msg_data || Oe_Msg_Pub.get(
                                      p_msg_index => i,
                                      p_encoded   => 'F'),1,2000);
               write_log ('  1.2.Order Update.'||l_return_status||'='||l_msg_data||'='||l_msg_count);
               END LOOP;
            ELSE
             write_log ('  3.1.Unable to Update the Order.'||l_msg_data);
            END IF;

              update xxwc_om_fulfill_acceptance
                 set error_msg = substr(l_msg_data,1,2000)
               where rowid = c1.rid;
               /*where header_id=c_hdr_rec.header_id
                 and nvl(process_flag,'N')='N';*/

          ELSIF (l_return_status = Fnd_Api.g_ret_sts_success) THEN
              
             -- Progressing Line
                fnd_file.put_line (fnd_file.output,'Attempting to progress line...');
                l_item_type        := null;
                l_activity         := null;
                l_result           := null;
                
                begin
                
                    SELECT wpa_to.process_name || ':' || wpa_to.activity_name,
                           wias_to.item_type
                    INTO   l_activity, l_item_type
                    FROM   wf_item_activity_statuses wias_to,
                           wf_process_activities wpa_to,
                           wf_activities wa,
                           wf_item_activity_statuses wias_from,
                           wf_process_activities wpa_from,
                           wf_activity_transitions wat,
                           apps.oe_order_lines ol
                    WHERE  wpa_to.instance_id= wias_to.process_activity
                    AND    wat.to_process_activity = wpa_to.instance_id
                    AND    wias_from.process_activity = wat.from_process_activity
                    AND    wpa_from.instance_id = wias_from.process_activity
                    AND    wias_from.end_date IS NOT NULL
                    AND    wias_from.item_type = 'OEOL'
                    AND    wias_from.item_key = to_char(ol.line_id)
                    AND    ol.flow_status_code = 'PRE-BILLING_ACCEPTANCE'
                    AND    ol.line_id = c1.line_id
                    AND    wa.item_type = wias_to.item_type
                    AND    wa.NAME = wpa_to.activity_name
                    AND    wa.FUNCTION = 'OE_STANDARD_WF.STANDARD_BLOCK'
                    AND    wa.end_date IS NULL
                    AND    wias_to.end_date IS NULL
                    AND    wias_to.activity_status = 'NOTIFIED'
                    AND    wias_to.item_type = wias_from.item_type
                    AND    wias_to.item_key = wias_from.item_key;

                    fnd_file.put_line (fnd_file.output,'Processing item type and activity: '||l_item_type||'/'||l_activity||' for header/line '||c1.header_id||'/'||c1.line_id);

                    wf_engine.CompleteActivity(l_item_type, To_Char(c1.line_id),l_activity, l_result);

                    fnd_file.put_line (fnd_file.output,'Line progression result '||l_result);

                exception
                when others then
                    l_item_type        := null;
                    l_activity         := null;
                    fnd_file.put_line (fnd_file.output,'Could not get activity ID for header/line: '||c1.header_id||'/'||c1.line_id);
                end;

                
              
              
              update xxwc_om_fulfill_acceptance
                 set process_flag='Y',
                     last_update_date = sysdate,
                     last_updated_by  = fnd_global.user_id
               where rowid = c1.rid;
               /*where header_id=c_hdr_rec.header_id
                 and nvl(process_flag,'N')='N';*/
          END IF;
         commit;
       end loop;
   END fulfillment_acceptance;

   PROCEDURE GET_RVP_INFO
      (p_organization_id      IN NUMBER
      ,x_name           OUT VARCHAR2
      ,x_phone          OUT VARCHAR2)
    IS

   l_name varchar2(150) DEFAULT NULL;
   l_phone varchar2(150) DEFAULT NULL;
   l_region varchar2(150) DEFAULT NULL;

   BEGIN

    BEGIN
           select attribute9
           into   l_region
           from   mtl_parameters
           where  organization_id = p_organization_id;
    EXCEPTION
           when others then
                l_region := NULL;
    END;


   IF l_region is not null then

        BEGIN
               select ffv.attribute1, ffv.attribute2
                into  l_name, l_phone
            from   fnd_flex_value_sets ffvs,
                   fnd_flex_values ffv
            where  ffvs.flex_value_set_name like 'XXWC_REGION'
            and    ffv.value_category = 'XXWC_RVP'
            and    ffvs.flex_value_set_id = ffv.flex_value_set_id
            and    ffv.flex_value = l_region;
       EXCEPTION
            when others then
                    l_name := NULL;
                    l_phone := NULL;
       END;

   END IF;


    x_phone := l_phone;
    x_name := l_name;

   END GET_RVP_INFO;

   PROCEDURE GET_CC_INFO
      (P_HEADER_ID      IN NUMBER
      ,P_TRXN_EXTENSION_ID IN NUMBER
      ,X_MASKED_CC_NUMBER      OUT VARCHAR2
      ,X_CHNAME            OUT VARCHAR2
      ,X_CARD_ISSUER_CODE   OUT VARCHAR2
      ,X_AUTH_CODE      OUT VARCHAR2
      ,X_AMOUNT         OUT NUMBER)
     IS



    l_masked_cc_number varchar2(150) DEFAULT NULL;
    l_chname           varchar2(150) DEFAULT NULL;
    l_card_issuer_code varchar2(150) DEFAULT NULL;
    l_auth_code        varchar2(150) DEFAULT NULL;
    l_amount           number   DEFAULT NULL;

   BEGIN

       BEGIN
        select ic.MASKED_CC_NUMBER,
               ic.chname,
               ic.card_issuer_code,
               itc.authcode,
               op.prepaid_amount
        into   l_masked_cc_number,
               l_chname,
               l_card_issuer_code,
               l_auth_code,
               l_amount
        from   iby_fndcpt_tx_extensions x,
               oe_payments op,
               apps.oe_order_headers ooha,
               iby_fndcpt_tx_operations o,
               iby_trxn_summaries_all itsa,
               iby_creditcard ic,
               iby_trxn_core itc
        where  ooha.header_id = p_header_id
        and    op.trxn_extension_id = p_trxn_extension_id
        and    x.origin_application_id = 660
        and    itsa.PAYERINSTRID = ic.instrid
        and    op.payment_type_code = 'CREDIT_CARD'
        and    itsa.TRXNTYPEID = 2
        and    op.trxn_extension_id = o.trxn_extension_id
        and    op.header_id = ooha.header_id
        and    op.header_id = ooha.header_id
        and    x.trxn_extension_id = o.trxn_extension_id
        and    o.transactionid = itsa.transactionid
        and    itsa.TRXNMID = itc.TRXNMID;
       EXCEPTION
          when others then
               l_masked_cc_number := NULL;
               l_chname := NULL;
               l_card_issuer_code := NULL;
               l_auth_code := NULL;
               l_amount := NULL;
       END;

    x_masked_cc_number := l_masked_cc_number;
    x_chname := l_chname;
    x_card_issuer_code := l_card_issuer_code;
    x_auth_code := l_auth_code;
    x_amount := l_amount;

   END GET_CC_INFO;

   FUNCTION CHECK_DELIVERY_DOCS_SUBMIT
          (p_header_id in number)
    return varchar2 is

    l_count number;
    l_group_id number;
    l_rownum number;
    l_return varchar2(1);
    l_order_type varchar2(50);


   BEGIN


       BEGIN
            select ottt.name
            into   l_order_type
            from   oe_transaction_types_tl ottt
            where  exists (select *
                           from   apps.oe_order_headers ooha
                           where  ooha.order_type_id = ottt.transaction_type_id
                           and    ooha.header_id = p_header_id);
       EXCEPTION

             when others then
                    l_order_type := '';
        END;



        if l_order_type like '%STANDARD%' THEN


               BEGIN
                  select rownum,
                         x.group_id,
                         x.cnt_group_id
                  into   l_rownum,
                         l_group_id,
                         l_count
                  from (select xprt.group_id,
                               count(xprt.group_id) cnt_group_id
                        from   xxwc_print_requests_arg_temp xprat,
                               apps.oe_order_headers ooha,
                               xxwc_print_requests_temp xprt
                        where  ooha.header_id = xprat.value
                        and    xprat.argument = 2
                        and    xprat.group_id = xprt.group_id
                        and    ooha.header_id = p_header_id
                        and    xprat.batch_id = xprt.batch_id
                        and    xprt.PROGRAM = 'XXWC_OM_PACK_SLIP'
                        and    xprt.APPLICATION = 'XXWC'
                        and    xprt.process_flag = 2
                        group by xprt.group_id
                        having count(xprt.group_id) = 2) x
               where rownum = 1;

               exception
                when others then
                       l_group_id := 0;
                       l_count := 0;
               end;


                    if l_count = 2 then

                        l_return := 'Y';

                    else

                        l_return := 'N';

                    end if;

       ELSIF l_order_type like '%INTERNAL%' THEN


        BEGIN
                  select rownum,
                         x.group_id,
                         x.cnt_group_id
                  into   l_rownum,
                         l_group_id,
                         l_count
                  from (select xprt.group_id,
                               count(xprt.group_id) cnt_group_id
                        from   xxwc_print_requests_arg_temp xprat,
                               apps.oe_order_headers ooha,
                               xxwc_print_requests_temp xprt
                        where  ooha.header_id = xprat.value
                        and    xprat.argument = 2
                        and    xprat.group_id = xprt.group_id
                        and    ooha.header_id = p_header_id
                        and    xprat.batch_id = xprt.batch_id
                        and    xprt.PROGRAM = 'XXWC_INT_ORD_PACK'
                        and    xprt.APPLICATION = 'XXWC'
                        and    xprt.process_flag = 2
                        group by xprt.group_id
                        having count(xprt.group_id) = 2) x
               where rownum = 1;

               exception
                when others then
                       l_group_id := 0;
                       l_count := 0;
               end;


                    if l_count = 1 then

                        l_return := 'Y';

                    else

                        l_return := 'N';

                    end if;

       ELSE

            l_return := 'N';


       END IF;


   return l_return;

   end CHECK_DELIVERY_DOCS_SUBMIT;


-- Added by Shankar Hariharan 29-Sep-2012 for Adding OM lines from AIS
   PROCEDURE ADD_LINES_TO_ORDER (i_line_tbl IN OE_ORDER_PUB.Line_Tbl_Type,
                                 o_msg_data OUT varchar2,
                                 o_return_status OUT varchar2)
   IS

     /* Initialize the proper Context */
     -- l_org_id   NUMBER := fnd_profile.value('ORG_ID');
     l_org_id   NUMBER := mo_global.get_current_org_id;
    -- l_application_id  NUMBER := fnd_profile.value('RESP_APPL_ID');

     /* MZ4Md211 */
    -- l_responsibility_id  NUMBER := fnd_profile.value('RESP_ID');
    -- l_user_id   NUMBER := fnd_profile.value('USER_ID'); 
     
          /* Initialize the record to G_MISS to enable defaulting */
     l_header_rec   OE_ORDER_PUB.Header_Rec_Type := OE_ORDER_PUB.G_MISS_HEADER_REC;
     l_old_header_rec  OE_ORDER_PUB.Header_Rec_Type;

     l_line_tbl   OE_ORDER_PUB.Line_Tbl_Type;
     l_old_line_tbl   OE_ORDER_PUB.Line_Tbl_Type;

     l_action_request_tbl       OE_ORDER_PUB.Request_Tbl_Type;

     x_header_rec   OE_ORDER_PUB.Header_Rec_Type;
     x_header_val_rec           OE_ORDER_PUB.Header_Val_Rec_Type;
     x_Header_Adj_tbl           OE_ORDER_PUB.Header_Adj_Tbl_Type;
     x_Header_Adj_val_tbl       OE_ORDER_PUB.Header_Adj_Val_Tbl_Type;
     x_Header_price_Att_tbl     OE_ORDER_PUB.Header_Price_Att_Tbl_Type;
     x_Header_Adj_Att_tbl       OE_ORDER_PUB.Header_Adj_Att_Tbl_Type;
     x_Header_Adj_Assoc_tbl     OE_ORDER_PUB.Header_Adj_Assoc_Tbl_Type;
     x_Header_Scredit_tbl       OE_ORDER_PUB.Header_Scredit_Tbl_Type;
     x_Header_Scredit_val_tbl   OE_ORDER_PUB.Header_Scredit_Val_Tbl_Type;
     x_line_tbl                 OE_ORDER_PUB.Line_Tbl_Type;
     x_line_val_tbl             OE_ORDER_PUB.Line_Val_Tbl_Type;
     x_Line_Adj_tbl             OE_ORDER_PUB.Line_Adj_Tbl_Type;
     x_Line_Adj_val_tbl         OE_ORDER_PUB.Line_Adj_Val_Tbl_Type;
     x_Line_price_Att_tbl       OE_ORDER_PUB.Line_Price_Att_Tbl_Type;
     x_Line_Adj_Att_tbl         OE_ORDER_PUB.Line_Adj_Att_Tbl_Type;
     x_Line_Adj_Assoc_tbl       OE_ORDER_PUB.Line_Adj_Assoc_Tbl_Type;
     x_Line_Scredit_tbl         OE_ORDER_PUB.Line_Scredit_Tbl_Type;
     x_Line_Scredit_val_tbl     OE_ORDER_PUB.Line_Scredit_Val_Tbl_Type;
     x_Lot_Serial_tbl           OE_ORDER_PUB.Lot_Serial_Tbl_Type;
     x_Lot_Serial_val_tbl       OE_ORDER_PUB.Lot_Serial_Val_Tbl_Type;
     x_action_request_tbl       OE_ORDER_PUB.Request_Tbl_Type;

     l_return_status   VARCHAR2(2000);
     l_msg_count   NUMBER;
     l_msg_data   VARCHAR2(2000);
     lv_msg_data  VARCHAR2(2000);

     l_line_cnt   number := 0;
     l_top_model_line_index  number;
     l_link_to_line_index  number;

     l_msg_index_out number(10);
     --PRAGMA AUTONOMOUS_TRANSACTION;

   BEGIN

     fnd_global.apps_initialize (fnd_global.user_id,
                                  fnd_global.resp_id,
                                  fnd_global.resp_appl_id
                                 );
     mo_global.init('ONT');
     mo_global.set_policy_context('S',l_org_id);
/*
 fnd_global.apps_initialize(16010, 50886, 660, NULL);
 mo_global.init('ONT');
 mo_global.set_policy_context('S',162);
*/
    for i in 1..i_line_tbl.count
     loop
          l_line_cnt := l_line_cnt + 1;
          l_line_tbl(l_line_cnt)     := OE_ORDER_PUB.G_MISS_LINE_REC;
          l_line_tbl(l_line_cnt).operation  := OE_GLOBALS.G_OPR_CREATE;
          l_line_tbl(l_line_cnt).header_id := i_line_tbl(i).header_id;
          l_line_tbl(l_line_cnt).inventory_item_id := i_line_tbl(i).inventory_item_id;
          l_line_tbl(l_line_cnt).ship_from_org_id := i_line_tbl(i).ship_from_org_id;
          l_line_tbl(l_line_cnt).ordered_quantity  := nvl(i_line_tbl(i).ordered_quantity,1);
     end loop;

         OE_ORDER_PUB.Process_Order
         (
          p_api_version_number => 1,
          p_org_id  => l_org_id,
          p_init_msg_list         => FND_API.G_TRUE,
          p_return_values         => FND_API.G_TRUE,
          p_action_commit         => FND_API.G_TRUE,
          x_return_status         => l_return_status,
          x_msg_count             => l_msg_count,
          x_msg_data              => l_msg_data,
          p_action_request_tbl => l_action_request_tbl,
          p_header_rec            => l_header_rec,
          p_old_header_rec        => l_old_header_rec,
          p_line_tbl  => l_line_tbl,
          p_old_line_tbl  => l_old_line_tbl,
          x_header_rec            => x_header_rec,
          x_header_val_rec        => x_header_val_rec,
          x_Header_Adj_tbl        => x_Header_Adj_tbl,
          x_Header_Adj_val_tbl    => x_Header_Adj_val_tbl,
          x_Header_price_Att_tbl  => x_Header_Price_Att_Tbl,
          x_Header_Adj_Att_tbl    => x_Header_Adj_Att_Tbl,
          x_Header_Adj_Assoc_tbl  => x_Header_Adj_Assoc_Tbl,
          x_Header_Scredit_tbl    => x_Header_Scredit_Tbl,
          x_Header_Scredit_val_tbl=> x_Header_Scredit_Val_Tbl,
          x_line_tbl              => x_Line_Tbl,
          x_line_val_tbl          => x_Line_Val_Tbl,
          x_Line_Adj_tbl          => x_Line_Adj_Tbl,
          x_Line_Adj_val_tbl      => x_Line_Adj_Val_Tbl,
          x_Line_price_Att_tbl    => x_Line_Price_Att_Tbl,
          x_Line_Adj_Att_tbl      => x_Line_Adj_Att_Tbl,
          x_Line_Adj_Assoc_tbl    => x_Line_Adj_Assoc_Tbl,
          x_Line_Scredit_tbl      => x_Line_Scredit_Tbl,
          x_Line_Scredit_val_tbl  => x_Line_Scredit_Val_Tbl,
          x_Lot_Serial_tbl        => x_Lot_Serial_Tbl,
          x_Lot_Serial_val_tbl    => x_Lot_Serial_Val_Tbl,
          x_action_request_tbl    => x_action_request_tbl
         );
     if l_return_status = FND_API.G_RET_STS_SUCCESS  then
       FOR i IN 1 .. l_msg_count
         LOOP
          Oe_Msg_Pub.get( p_msg_index => i
          ,p_encoded => Fnd_Api.G_FALSE
          ,p_data => l_msg_data
          ,p_msg_index_out => l_msg_index_out);
          lv_msg_data := lv_msg_data||':'||substr(l_msg_data,1,100);
         END LOOP;
       o_msg_data := nvl(lv_msg_data,'Lines Added Successfully');
       o_return_status := l_return_status;
--       o_msg_data:= 'Lines Added Successfully';
--       o_return_status := 'S';
       commit;
     else
    -- Retrieve messages
         FOR i IN 1 .. l_msg_count
         LOOP
          Oe_Msg_Pub.get( p_msg_index => i
          ,p_encoded => Fnd_Api.G_FALSE
          ,p_data => l_msg_data
          ,p_msg_index_out => l_msg_index_out);
          lv_msg_data := lv_msg_data||':'||substr(l_msg_data,1,100);
         END LOOP;
       o_msg_data := lv_msg_data;
       o_return_status := l_return_status;
     end if;
   END ADD_LINES_TO_ORDER;

   FUNCTION CHECK_LOT_RESERVATION_FNC
   (   p_header_id   IN       NUMBER,
      p_organization_id IN   NUMBER,
      p_return               VARCHAR2 --Added 10/26/2012
   ) RETURN VARCHAR2 is


      l_count          NUMBER           DEFAULT 0;
      l_message        VARCHAR2 (10000) DEFAULT '';
      l_check          VARCHAR2 (1)     DEFAULT  'N';

      l_return         VARCHAR2(10000);

   BEGIN


        xxwc_ont_routines_pkg.check_lot_reservation (p_header_id
                                                    ,p_organization_id
                                                    ,l_count
                                                    ,l_check
                                                    ,l_message);




      if P_RETURN = 'COUNT' THEN

            l_return := l_count;


      elsif P_RETURN = 'CHECK' THEN

        if l_count > 0 and l_check = 'Y' then


                l_return := 'Y';

        else

                l_return := 'N';

        end if;

      elsif P_RETURN = 'MESSAGE' THEN


        l_return := l_message;



      else

        l_return := NULL;


      end if;




      return l_return;


   END CHECK_LOT_RESERVATION_FNC;

     --Added by Lee Spitzer 01-15-2013 to check print price on customer ship to DFF
   --Pass the order header id and return 1 or 2 to restrict pricing
        --1 = Print Price
        --2 = Do Not Print Price
  -- 5.5        02/09/2015   Gopi Damuluri            TMS# 

   /*************************************************************************
    *  Function : CHECK_PRINT_PRICE
    *
    *  PURPOSE:   Function checks Print Price Flag on the Customer Site DFF
    *
    *  REVISIONS:
    *  Ver        Date        Author             Description
    *  ---------  ----------  ---------------    -------------------------
    *  5.5        02/09/2015  Gopi Damuluri      TMS# 20150210-00007 Performance Tuning - XXWC_ONT_ROUTINES_PKG.CHECK_PRINT_PRICE
    * ************************************************************************/
    FUNCTION CHECK_PRINT_PRICE (p_ship_to_org_id IN NUMBER) -- Version# 5.5
--        (p_header_id IN NUMBER)
        --Removed 06192013 per TMS Ticket 20130619-01155   ,p_name      IN VARCHAR2) --Added TMS Ticket 20130501-04183
      RETURN NUMBER IS

       l_check_print NUMBER;

    BEGIN

    /*Removed 06192013 per TMS Ticket 20130619-01155
    --If this is a RECEIPT then
        --Attribute1 in 0 or 2 return 2 else return 1
     IF p_name = 'RECEIPT' THEN
    
      BEGIN
        SELECT decode(hcasa2.attribute1,
                  0,2,
                  1,1,
                  2,2,
                  1)
        INTO   l_check_print
        FROM   oe_order_headers_all ooha,
               ---Sold_to Org Id Customer
               hz_cust_site_uses_all  hcsua2,
               hz_cust_acct_sites_all    hcasa2,
               hz_cust_accounts      hca2,
               hz_parties          hp2
        WHERE  hcsua2.SITE_USE_id = ooha.ship_to_org_id
        AND    hcsua2.CUST_ACCT_SITE_ID = hcasa2.cust_acct_site_id
        AND    hcasa2.cust_account_id = hca2.cust_account_id
        AND    hca2.party_id = hp2.party_id
        AND   ooha.header_id = p_header_id;
      EXCEPTION
        WHEN OTHERS THEN
            --Removed per TMS 20130501-04183 so we return the actual value and a NVL = 1
            --l_check_print := 2;
            --Updated per TMS 20130501-04183 so we return the actual value and a NVL = 1
            l_check_print := 1;
      END;
  
      --If this is a PICK then
        --Attribute1 in 2 return 2 else return 1
    
      ELSIF p_name = 'PICK' THEN
        
          BEGIN
            SELECT decode(hcasa2.attribute1,
                      0,1,
                      1,1,
                      2,2,
                      1)
            INTO   l_check_print
            FROM   oe_order_headers_all ooha,
                   ---Sold_to Org Id Customer
                   hz_cust_site_uses_all  hcsua2,
                   hz_cust_acct_sites_all    hcasa2,
                   hz_cust_accounts      hca2,
                   hz_parties          hp2
            WHERE  hcsua2.SITE_USE_id = ooha.ship_to_org_id
            AND    hcsua2.CUST_ACCT_SITE_ID = hcasa2.cust_acct_site_id
            AND    hcasa2.cust_account_id = hca2.cust_account_id
            AND    hca2.party_id = hp2.party_id
            AND   ooha.header_id = p_header_id;
          EXCEPTION
            WHEN OTHERS THEN
                --Removed per TMS 20130501-04183 so we return the actual value and a NVL = 1
                --l_check_print := 2;
                --Updated per TMS 20130501-04183 so we return the actual value and a NVL = 1
                l_check_print := 1;
          END;
    
      --IF this IS A PACK THEN
        --Attribute1 in 0 or 2 return 2 else return 1
      ELSIF p_name = 'PACK' THEN
        
          BEGIN
            SELECT decode(hcasa2.attribute1,
                      0,2,
                      1,1,
                      2,2,
                      1)
            INTO   l_check_print
            FROM   oe_order_headers_all ooha,
                   ---Sold_to Org Id Customer
                   hz_cust_site_uses_all  hcsua2,
                   hz_cust_acct_sites_all    hcasa2,
                   hz_cust_accounts      hca2,
                   hz_parties          hp2
            WHERE  hcsua2.SITE_USE_id = ooha.ship_to_org_id
            AND    hcsua2.CUST_ACCT_SITE_ID = hcasa2.cust_acct_site_id
            AND    hcasa2.cust_account_id = hca2.cust_account_id
            AND    hca2.party_id = hp2.party_id
            AND   ooha.header_id = p_header_id;
          EXCEPTION
            WHEN OTHERS THEN
                --Removed per TMS 20130501-04183 so we return the actual value and a NVL = 1
                --l_check_print := 2;
                --Updated per TMS 20130501-04183 so we return the actual value and a NVL = 1
                l_check_print := 1;
          END;
    
        ELSE
        
            l_check_print := 1;
            
        END IF;
        
      RETURN l_check_print;
    */
    
    --Added back Per TMS Ticket 20130619-01155
    
      BEGIN
-- Version# 5.5 > Start
        SELECT decode(hcasa2.attribute1,2,2,1)
        INTO   l_check_print
        FROM   apps.hz_cust_site_uses     hcsua2,
               apps.hz_cust_acct_sites    hcasa2
        WHERE  hcsua2.SITE_USE_id       = p_ship_to_org_id
        AND    hcsua2.CUST_ACCT_SITE_ID = hcasa2.cust_acct_site_id
        AND    hcsua2.org_id              = hcasa2.org_id;

/*
        SELECT decode(hcasa2.attribute1,2,2,1)
        INTO   l_check_print
        FROM   apps.oe_order_headers ooha,
               ---Sold_to Org Id Customer
               apps.hz_cust_site_uses  hcsua2,
               apps.hz_cust_acct_sites    hcasa2,
               hz_cust_accounts      hca2,
               hz_parties          hp2
        WHERE  hcsua2.SITE_USE_id = ooha.ship_to_org_id
        AND    hcsua2.CUST_ACCT_SITE_ID = hcasa2.cust_acct_site_id
        AND    hcasa2.cust_account_id = hca2.cust_account_id
        AND    hca2.party_id = hp2.party_id
        AND   ooha.header_id = p_header_id;
*/ 
-- Version# 5.5 < End
      EXCEPTION
        WHEN OTHERS THEN
            l_check_print := 2;
      END;

      RETURN l_check_print;
      
    END CHECK_PRINT_PRICE;


 PROCEDURE progress_inv_hold_lines (
      errbuf        OUT      VARCHAR2,
      retcode       OUT      VARCHAR2,
      p_header_id   IN       NUMBER DEFAULT NULL
   )
   IS
     --  Shankar TMS  [20130115-01230]: 01/16/2013
     -- Progress lines on invoice hold if the associated holds are released. Without this
     -- process the lines will be on timeout for 12 hours resulting in the credit and new line
     -- getting invoiced on different dates
     C_PROCESS_NAME  Constant   Varchar2(30) := 'LINE_DEF_INVOICE_INTERFACE_SUB';
      CURSOR order_cur
      IS
         SELECT   ool.line_id, ool.header_id
             FROM apps.oe_order_lines ool
            WHERE ool.line_category_code in ('ORDER','RETURN')
              AND ool.cancelled_flag = 'N'
              AND ool.flow_status_code='INVOICE_HOLD'
              AND (p_header_id IS NULL OR ool.header_id = p_header_id)
              AND ool.ordered_quantity > 0
          AND EXISTS (
                     SELECT 'exists'
                       FROM wf_item_activity_statuses a,
                            wf_process_activities b
                      WHERE a.item_type = 'OEOL'
                        AND a.process_activity = b.instance_id
                        AND b.activity_name = 'INVOICE_INTERFACE_ELIGIBLE'
                        AND b.Process_Name  = 'LINE_DEF_INVOICE_INTERFACE_SUB'
                        AND activity_status <> 'COMPLETE'
                        AND TO_NUMBER (a.item_key) = ool.line_id)
         AND not exists
                    (select 1
                       from apps.oe_order_holds x
                      where x.header_id=ool.header_id
                        and ool.line_id= nvl(x.line_id,ool.line_id)
                        and released_flag='N')
         ORDER BY ool.header_id, ool.line_number;

      l_api_name       VARCHAR2 (30) := 'Progress_Inv_Hold_Lines';
      l_distro_list    VARCHAR2 (75)
                                    DEFAULT 'HDSOracleDevelopers@hdsupply.com';
      l_package_name   VARCHAR2 (75) DEFAULT 'XXWC_ONT_ROUTINES_PKG';
      l_line_id        VARCHAR2(50);  -- Version# 5.4, TMS# 20150202-00030
      l_activity       VARCHAR2(80):='INVOICE_INTERFACE_ELIGIBLE';  -- Version# 5.4, TMS# 20150202-00030
   BEGIN
      fnd_file.put_line (fnd_file.LOG, '100 : Begin of API : ' || l_api_name);
      fnd_global.apps_initialize (fnd_global.user_id,
                                  fnd_global.resp_id,
                                  fnd_global.resp_appl_id
                                 );
      fnd_file.put_line (fnd_file.LOG, '110 : Loop Through the Cursor : ');

      FOR i IN order_cur
      LOOP
         BEGIN
           l_line_id:= TO_CHAR (i.line_id);  -- Version# 5.4, TMS# 20150202-00030
            fnd_file.put_line (fnd_file.LOG,
                                  '120 : Complete Activity for SO Line ID : '
                               || i.line_id
                              );
            wf_engine.completeactivity ('OEOL',
                                        l_line_id, -- TO_CHAR (i.line_id), -- Version# 5.4, TMS# 20150202-00030
                                        l_activity,  -- 'INVOICE_INTERFACE_ELIGIBLE' -- Version# 5.4, TMS# 20150202-00030
                                        NULL
                                       );
         EXCEPTION
            WHEN OTHERS
            THEN
               retcode := 1;
               fnd_file.put_line
                  (fnd_file.LOG,
                      '150 : When others Exception while completing acivity for SO Line ID  : '
                   || i.line_id
                  );
               fnd_file.put_line (fnd_file.LOG,
                                  '151 : Error Message : ' || SQLERRM
                                 );
         END;
      END LOOP;

      wf_engine.background (itemtype              => 'OEOL',
                            minthreshold          => NULL,
                            maxthreshold          => NULL,
                            process_deferred      => TRUE,
                            process_timeout       => TRUE,
                            process_stuck         => FALSE
                           );
   EXCEPTION
      WHEN OTHERS
      THEN
         retcode := 2;
         errbuf := SQLERRM;
         fnd_file.put_line (fnd_file.LOG, '160 : When others Exception ');
         fnd_file.put_line (fnd_file.LOG,
                            '161 : Error Message : ' || SQLERRM);
         fnd_file.put_line (fnd_file.output, SQLERRM);
         -- Calling ERROR API
         xxcus_error_pkg.xxcus_error_main_api
            (p_called_from            => l_package_name,
             p_calling                => 'Concurrent Program exception for XXWC_ONT_ROUTINES_PKG.Progress_Inv_Hold_Lines',
             p_request_id             => fnd_global.conc_request_id,
             p_ora_error_msg          => SQLERRM,
             p_error_desc             => 'When others Exception ',
             p_distribution_list      => l_distro_list,
             p_module                 => 'OM'
            );
   END progress_inv_hold_lines;

   PROCEDURE om_assign_order_line_wf (
      errbuf        OUT      VARCHAR2,
      retcode       OUT      VARCHAR2,
      p_header_id   IN       NUMBER DEFAULT NULL
   )
   IS
   --Created by Ram Talluri for assinging missing order line_workflows
   --24-Jan-2013--creation_date
   --TMS 20130201-01304

      CURSOR order_line_cur
      IS
         SELECT
            oh.header_id,
            oh.order_type_id,
            ool.line_id
    FROM apps.oe_order_lines ool, wf_items wi, apps.oe_order_headers oh
   WHERE     1 = 1
         AND (p_header_id IS NULL OR ool.header_id = p_header_id)
         AND oh.header_id = ool.header_id
         AND ool.line_category_code = 'ORDER'
         AND ool.cancelled_flag = 'N'
         AND ool.ordered_quantity > 0
         AND wi.item_type='OEOL'
         AND wi.item_key = TO_CHAR(ool.line_id)
         AND wi.PARENT_ITEM_KEY = TO_CHAR(oh.header_id)
         AND ool.flow_status_code not in ('CLOSED','CANCELLED','DRAFT','DRAFT_CUSTOMER_REJECTED','OFFER_EXPIRED','PENDING_CUSTOMER_ACCEPTANCE' ) --'BOOKED'
         AND NOT EXISTS
                    (SELECT 'not exists'
                       FROM wf_item_activity_statuses a
                      WHERE a.item_type = 'OEOL'
                        AND a.item_key = TO_CHAR(ool.line_id))
         ORDER BY ool.line_id;

      l_item_key varchar2(256);
      l_result varchar2(30);

      l_api_name       VARCHAR2 (30) := 'Assign_SO_line_workflow';
      l_distro_list    VARCHAR2 (75)
                                    DEFAULT 'HDSOracleDevelopers@hdsupply.com';
      l_package_name   VARCHAR2 (75) DEFAULT 'XXWC_ONT_ROUTINES_PKG';
   BEGIN
      fnd_file.put_line (fnd_file.LOG, '100 : Begin of API : ' || l_api_name);
      fnd_global.apps_initialize (fnd_global.user_id,
                                  fnd_global.resp_id,
                                  fnd_global.resp_appl_id
                                 );
      fnd_file.put_line (fnd_file.LOG, '110 : Loop Through the Cursor : ');

      FOR i IN order_line_cur
      LOOP

      l_item_key:=NULL;

      l_item_key:=TO_CHAR(i.line_id);

         BEGIN
            fnd_file.put_line (fnd_file.LOG,
                                  '120 : Assinging order workflow for line_id: '
                               || i.line_id
                              );
            OE_Standard_WF.OEOL_SELECTOR
                (p_itemtype => 'OEOL'
                ,p_itemkey => l_item_key
                ,p_actid => 12345
                ,p_funcmode => 'SET_CTX'
                ,p_result => l_result
                );
            wf_engine.startprocess('OEOL', l_item_key);

         COMMIT;

         EXCEPTION
            WHEN OTHERS
            THEN
               retcode := 1;
               fnd_file.put_line
                  (fnd_file.LOG,
                      '150 : When others Exception while assigning workflow for SO Line ID  : '
                   || i.line_id
                  );
               fnd_file.put_line (fnd_file.LOG,
                                  '151 : Error Message : ' || SQLERRM
                                 );
         END;
      END LOOP;

   EXCEPTION
      WHEN OTHERS
      THEN
         retcode := 2;
         errbuf := SQLERRM;
         fnd_file.put_line (fnd_file.LOG, '160 : When others Exception ');
         fnd_file.put_line (fnd_file.LOG,
                            '161 : Error Message : ' || SQLERRM);
         fnd_file.put_line (fnd_file.output, SQLERRM);
         -- Calling ERROR API
         xxcus_error_pkg.xxcus_error_main_api
            (p_called_from            => l_package_name,
             p_calling                => 'Concurrent Program exception for om_assign_order_line_wf',
             p_request_id             => fnd_global.conc_request_id,
             p_ora_error_msg          => SQLERRM,
             p_error_desc             => 'When others Exception ',
             p_distribution_list      => l_distro_list,
             p_module                 => 'OM'
            );
   END om_assign_order_line_wf;

/*************************************************************************
  Function : xxwc_rel_price_chg_hold_prc

  PURPOSE:   Procedure to release XXWC_PRICE_CHANGE_HOLD hold on the order lines
             TMS Ticket 20130121-00447

  REVISIONS:
  Ver        Date        Author                     Description
  ---------  ----------  ---------------         -------------------------
  1.0        02/05/2013  Consuelo Gonzalez      Initial Version
  2.0       07/10/2014  Ram Talluri           Fulfillment acceptance date logic added TMS #20140710-00222
*************************************************************************/
PROCEDURE xxwc_rel_price_chg_hold_prc ( errbuf                OUT VARCHAR2,
                                        retcode               OUT VARCHAR2,
                                        p_header_id            IN NUMBER DEFAULT NULL,
                                        p_order_type           IN NUMBER,      -- Added. Version# 5.0
                                        p_incl_hawaii_branches IN VARCHAR2,    -- Added. Version# 5.0
                                        p_last_update_date     IN VARCHAR2     -- Added. Version# 5.0
                                        )
IS

    l_return_status     varchar2(10) := null;
    l_msg_count         number := null;
    l_msg_data          varchar2(300) := null;
    l_hold_source_rec  OE_HOLDS_PVT.hold_source_rec_type := OE_HOLDS_PVT.G_MISS_HOLD_SOURCE_REC;
    l_hold_release_rec  OE_HOLDS_PVT.Hold_Release_Rec_Type := OE_HOLDS_PVT.G_MISS_HOLD_RELEASE_REC;

    l_hold_source_id NUMBER  ; --:= 97598 ;
    l_hold_id NUMBER := 1007 ;  --oe_hold_sources_all.
    l_hold_entity_code NUMBER ; --:= 'O' ;
    l_hold_entity_id NUMBER ; --:= 781146 ;
    l_header_id NUMBER ;
    l_line_id NUMBER ;
    l_release_reason_code VARCHAR2(240) := 'MGR_APPROVAL_HLD';
    l_release_comment VARCHAR2(240) := 'Hold release for Pricing Lockdown';
    l_order_hold_id NUMBER ;

    l_hold_activity    VARCHAR2(250);
    l_item_type        VARCHAR2(10);
    l_activity         VARCHAR2(250);
    l_result           VARCHAR2(10);
    l_last_update_date    DATE;   -- Version# 5.0

/*
    cursor cur_unrel_orders is
            SELECT a.order_hold_id
                   , a.hold_source_id
                   , a.header_id
                   , a.line_id
                   , b.hold_entity_code
                   , b.hold_entity_id
                   , oh.order_number
                   , (ol.line_number||'.'||ol.shipment_number) Line_Number
              FROM     oe_order_holds_all a,
                       oe_hold_sources_all b,
                       oe_order_headers_all oh,
                       oe_order_lines_all ol
             WHERE       a.released_flag = 'N'
                     AND a.hold_source_id = b.hold_source_id
                     AND b.hold_id = l_hold_id
                     AND a.header_id = nvl(p_header_id, a.header_id)
                     AND a.line_id is not null
                     AND a.header_id = oh.header_id
                     AND a.line_id = ol.line_id
                     AND ol.flow_status_code = 'INVOICE_HOLD' ;
*/

    CURSOR cur_unrel_orders (p_date IN DATE) IS
    ----------------------------------------------------------
    -- Derives orders of both Hawaiin and Non-Hawaiin branches
    ----------------------------------------------------------
    SELECT a.order_hold_id
           , a.hold_source_id
           , a.header_id
           , a.line_id
           , b.hold_entity_code
           , b.hold_entity_id
           , oh.order_number
           , (ol.line_number||'.'||ol.shipment_number) Line_Number
      FROM apps.oe_order_holds                        a,
           apps.oe_hold_sources                       b,
           apps.oe_order_headers                     oh,
           apps.oe_order_lines                       ol
     WHERE a.released_flag                        = 'N'
       AND a.hold_source_id                       = b.hold_source_id
       AND b.hold_id                              = l_hold_id
       AND a.header_id                            = NVL(p_header_id, a.header_id)
       AND a.line_id                         IS NOT NULL
       AND a.header_id                            = oh.header_id
       AND a.line_id                              = ol.line_id
       AND oh.order_type_id                       = NVL(p_order_type, oh.order_type_id)
       AND TRUNC(ol.last_update_date)            >= p_date                                     -- Version# 5.0 
       AND apps.xxwc_om_force_ship_pkg.is_row_locked (ol.ROWID, 'OE_ORDER_LINES_ALL')  = 'N'   -- Version# 5.0
       AND ol.flow_status_code = 'INVOICE_HOLD'
       AND NVL(p_incl_hawaii_branches,'Y') = 'Y'
    ---------------------------------------------------------------------------------------------------------
    -- Derives orders lines for which fulfillment acceptance was applied manually.
    --Added by Ram Talluri for TMS #20140710-00222 7/21/2014
    ---------------------------------------------------------------------------------------------------------
    UNION
    SELECT a.order_hold_id
           , a.hold_source_id
           , a.header_id
           , a.line_id
           , b.hold_entity_code
           , b.hold_entity_id
           , oh.order_number
           , (ol.line_number||'.'||ol.shipment_number) Line_Number
      FROM apps.oe_order_holds                        a,
           apps.oe_hold_sources                       b,
           apps.oe_order_headers                     oh,
           apps.oe_order_lines                       ol
     WHERE a.released_flag                        = 'N'
       AND a.hold_source_id                       = b.hold_source_id
       AND b.hold_id                              = l_hold_id
       AND a.header_id                            = NVL(p_header_id, a.header_id)
       AND a.line_id                         IS NOT NULL
       AND a.header_id                            = oh.header_id
       AND a.line_id                              = ol.line_id
       AND oh.order_type_id                       = NVL(p_order_type, oh.order_type_id)
       AND ol.revrec_signature_date>= p_date
       AND apps.xxwc_om_force_ship_pkg.is_row_locked (ol.ROWID, 'OE_ORDER_LINES_ALL')  = 'N'   
       AND ol.flow_status_code = 'INVOICE_HOLD'
       AND NVL(p_incl_hawaii_branches,'Y') = 'Y'
    UNION
    ----------------------------------------------------------
    -- Derives orders of Non-Hawaiin branches only
    ----------------------------------------------------------
    SELECT a.order_hold_id
         , a.hold_source_id
         , a.header_id
         , a.line_id
         , b.hold_entity_code
         , b.hold_entity_id
         , oh.order_number
         , (ol.line_number||'.'||ol.shipment_number) Line_Number
      FROM apps.oe_order_holds a,
           apps.oe_hold_sources b,
           apps.oe_order_headers oh,
           apps.oe_order_lines ol
     WHERE a.released_flag = 'N'
       AND a.hold_source_id = b.hold_source_id
       AND b.hold_id = l_hold_id
       AND a.header_id = nvl(p_header_id, a.header_id)
       AND a.line_id is not null
       AND a.header_id = oh.header_id
       AND a.line_id = ol.line_id
       AND oh.order_type_id  = NVL(p_order_type, oh.order_type_id)                             -- Version# 5.0
       AND TRUNC(ol.last_update_date) >=  p_date                                               -- Version# 5.0
       AND apps.xxwc_om_force_ship_pkg.is_row_locked (ol.ROWID, 'OE_ORDER_LINES_ALL')  = 'N'   -- Version# 5.0
       AND ol.flow_status_code = 'INVOICE_HOLD'
       AND NVL(p_incl_hawaii_branches,'Y') = 'N'
       AND NOT EXISTS(SELECT '1'
                        FROM fnd_lookup_values flv
                       WHERE 1 = 1
                         AND flv.lookup_type     = 'XXWC_HAWAII_BRANCHES'
                         AND ol.ship_from_org_id = TO_NUMBER(flv.lookup_code)
                     )
    ---------------------------------------------------------------------------------------------------------
    -- Derives orders lines for which fulfillment acceptance was applied manually excludes Hawaii orders.
    --Added by Ram Talluri for TMS #20140710-00222 7/21/2014
    ---------------------------------------------------------------------------------------------------------
    UNION
    SELECT a.order_hold_id
         , a.hold_source_id
         , a.header_id
         , a.line_id
         , b.hold_entity_code
         , b.hold_entity_id
         , oh.order_number
         , (ol.line_number||'.'||ol.shipment_number) Line_Number
      FROM apps.oe_order_holds a,
           apps.oe_hold_sources b,
           apps.oe_order_headers oh,
           apps.oe_order_lines ol
     WHERE a.released_flag = 'N'
       AND a.hold_source_id = b.hold_source_id
       AND b.hold_id = l_hold_id
       AND a.header_id = nvl(p_header_id, a.header_id)
       AND a.line_id is not null
       AND a.header_id = oh.header_id
       AND a.line_id = ol.line_id
       AND oh.order_type_id  = NVL(p_order_type, oh.order_type_id)                            
       AND ol.revrec_signature_date>= p_date
       AND apps.xxwc_om_force_ship_pkg.is_row_locked (ol.ROWID, 'OE_ORDER_LINES_ALL')  = 'N'   
       AND ol.flow_status_code = 'INVOICE_HOLD'
       AND NVL(p_incl_hawaii_branches,'Y') = 'N'
       AND NOT EXISTS(SELECT '1'
                        FROM fnd_lookup_values flv
                       WHERE 1 = 1
                         AND flv.lookup_type     = 'XXWC_HAWAII_BRANCHES'
                         AND ol.ship_from_org_id = TO_NUMBER(flv.lookup_code)
                     )
       ;

begin

    fnd_file.put_line (fnd_file.output, 'Starting procedure to release picking holds...');
    fnd_file.put_line (fnd_file.output, ' ');

    fnd_file.put_line (fnd_file.output, 'p_header_id...'||p_header_id);
    fnd_file.put_line (fnd_file.output, 'p_order_type...'||p_order_type);
    fnd_file.put_line (fnd_file.output, 'p_incl_hawaii_branches...'||p_incl_hawaii_branches);
    


    fnd_global.apps_initialize (fnd_global.user_id,
                                fnd_global.resp_id,
                                fnd_global.resp_appl_id
                               );

    mo_global.set_policy_context ('S', fnd_global.org_id);

    mo_global.init('ONT');

    commit;

-- Version# 5.0 > Start
    ----------------------------------------------------------
    -- If LastUpdateDate input parameter is not provided,
    -- Process will run for last 5 days
    ----------------------------------------------------------
    IF p_last_update_date IS NULL THEN
       l_last_update_date := TRUNC(SYSDATE) - 5;
    ELSE
       l_last_update_date := TO_DATE(p_last_update_date,'DD-MON-YYYY');
    END IF;
-- Version# 5.0 < End

    for c1 in cur_unrel_orders(l_last_update_date)
    Loop
        exit when cur_unrel_orders%notfound;

        fnd_file.put_line (fnd_file.output, 'Order Number/Line Number: '||c1.order_number||'/'||c1.line_number);

        l_hold_source_rec.hold_source_id := c1.hold_source_id ;
        l_hold_source_rec.hold_id := l_hold_id ;  --oe_hold_sources_all.
        l_hold_source_rec.hold_entity_code := c1.hold_entity_code ;
        l_hold_source_rec.hold_entity_id := c1.hold_entity_id ;
        l_hold_source_rec.header_id :=  c1.header_id ;
        l_hold_source_rec.line_id := c1.line_id ;

        l_hold_release_rec.hold_source_id := c1.hold_source_id ;
        l_hold_release_rec.release_reason_code := l_release_reason_code ;
        --l_hold_release_rec.release_comment := l_release_comment ;
        l_hold_release_rec.order_hold_id := c1.order_hold_id;

        oe_holds_pub.Release_Holds  ( p_api_version             => 1.0
                                      , p_init_msg_list         => FND_API.G_TRUE
                                      , p_commit                => FND_API.G_TRUE
                                      , p_validation_level      => FND_API.G_VALID_LEVEL_FULL
                                      , p_hold_source_rec       => l_hold_source_rec
                                      , p_hold_release_rec      => l_hold_release_rec
                                      , x_return_status         => l_return_status
                                      , x_msg_count             => l_msg_count
                                      , x_msg_data              => l_msg_data);


        fnd_file.put_line (fnd_file.output,'Status: '||l_return_status);

        if l_return_status <> 'S' then
            fnd_file.put_line (fnd_file.output,'Msg Cnt: '||l_msg_count);

            IF l_msg_count > 1 THEN

                    FOR i IN 1 .. l_msg_count
                    LOOP
                        fnd_file.put_line (fnd_file.output, 'Msg (' || i ||'): '|| SUBSTR (fnd_msg_pub.get(p_encoded => fnd_api.g_false), 1, 255) );
                    END LOOP;
            ELSE
                fnd_file.put_line (fnd_file.output, 'Msg: ' || l_msg_data);
            END IF;

        else
            -- Progressing Line
            fnd_file.put_line (fnd_file.output,'Attempting to progress line...');
            l_hold_activity    := null;
            l_item_type        := null;
            l_activity         := null;
            l_result           := null;

            BEGIN
                SELECT activity_name
                INTO   l_hold_activity
                FROM   oe_hold_definitions
                WHERE  hold_id = l_hold_id;
            EXCEPTION
                WHEN NO_DATA_FOUND THEN
                    l_hold_activity := null;
                    fnd_file.put_line (fnd_file.output,'Hold Activity name not found!!');
                WHEN OTHERS THEN
                    l_hold_activity := null;
                    fnd_file.put_line (fnd_file.output,'Error looking for hold activity name. Error: '||SQLERRM);
            END;

            if l_hold_activity is not null then
                begin
                    SELECT wpa_to.process_name || ':' || wpa_to.activity_name,
                           wias_to.item_type
                    INTO   l_activity, l_item_type
                    FROM   wf_item_activity_statuses wias_to,
                           wf_process_activities wpa_to,
                           wf_activities wa,
                           wf_item_activity_statuses wias_from,
                           wf_process_activities wpa_from,
                           wf_activity_transitions wat
                    WHERE  wpa_to.instance_id= wias_to.process_activity
                    AND    wat.to_process_activity = wpa_to.instance_id
                    AND    wat.result_code = 'ON_HOLD'
                    AND    wias_from.process_activity = wat.from_process_activity
                    AND    wpa_from.instance_id = wias_from.process_activity
                            AND    wpa_from.activity_name = l_hold_activity  -- 8284926
                    AND    wias_from.activity_result_code = 'ON_HOLD'
                    AND    wias_from.end_date IS NOT NULL
                    AND    wias_from.item_type = 'OEOL'
                    AND    wias_from.item_key =  To_Char(c1.line_id)
                    AND    wa.item_type = wias_to.item_type
                    AND    wa.NAME = wpa_to.activity_name
                    AND    wa.FUNCTION = 'OE_STANDARD_WF.STANDARD_BLOCK'
                    AND    wa.end_date IS NULL
                    AND    wias_to.end_date IS NULL
                    AND    wias_to.activity_status = 'NOTIFIED'
                    AND    wias_to.item_type = wias_from.item_type
                    AND    wias_to.item_key = wias_from.item_key;

                    fnd_file.put_line (fnd_file.output,'Processing item type and activity: '||l_item_type||'/'||l_activity||' for header/line '||c1.header_id||'/'||c1.line_id);

                    wf_engine.CompleteActivity(l_item_type, To_Char(c1.line_id),l_activity, l_result);

                    fnd_file.put_line (fnd_file.output,'Line progression result '||l_result);

                exception
                when others then
                    l_item_type        := null;
                    l_activity         := null;
                    fnd_file.put_line (fnd_file.output,'Could not get activity ID for header/line: '||c1.header_id||'/'||c1.line_id);
                end;

            end if;

        end if;

    END LOOP ;

    COMMIT;

exception
when others then
    fnd_file.put_line (fnd_file.output,'Stat:'||l_return_status);
    fnd_file.put_line (fnd_file.output,'Msg:'||l_msg_data);
    fnd_file.put_line (fnd_file.output,'Err:'||SQLERRM);
    --retcode := 2;
    errbuf := SQLERRM;

    xxcus_error_pkg.xxcus_error_main_api
                                        (p_called_from            => 'XXWC_ONT_ROUTINES_PKG.XXWC_REL_PRICE_CHG_HOLD_PRC',
                                         p_calling                => 'Exception in concurrent program to release pricing chg hold',
                                         p_request_id             => fnd_global.conc_request_id,
                                         p_ora_error_msg          => SQLERRM,
                                         p_error_desc             => 'When others Exception ',
                                         p_distribution_list      => 'HDSOracleDevelopers@hdsupply.com',
                                         p_module                 => 'ONT'
                                        );

    rollback;
    raise;
end xxwc_rel_price_chg_hold_prc;

   /*************************************************************************
*   Function : Apply_Price_Change_Hold
*
*   PURPOSE:   This procedure is called from Standard Line WF Node at line Level.
*   Checks if XXWC_PRICE_CHANGE_HOLD exists at line level, if not then creates a hold at the line level.
*  Ver        Date        Author                     Description
*  ---------  ----------  ---------------         -------------------------
* 1.0        02/05/2013    Satish U      Initial Version MS Ticket 20130121-00447
* ************************************************************************/
   PROCEDURE Apply_Price_Change_Hold (
      itemtype    IN              VARCHAR2,
      itemkey     IN              VARCHAR2,
      actid       IN              NUMBER,
      funcmode    IN              VARCHAR2,
      resultout   IN OUT NOCOPY   VARCHAR2
   ) Is
      l_line_id          NUMBER;
      l_return_status    VARCHAR2(30);
      l_result_out       VARCHAR2(240);
      l_msg_count        NUMBER;
      l_msg_data         VARCHAR2(2000);
      K                  Number;
      --
      l_debug_level CONSTANT NUMBER := oe_debug_pub.g_debug_level;
      --

      l_header_id               NUMBER;
      l_Line_Type_id            NUMBER;
      l_Hold_ID                 Number;
      l_hold_source_rec   OE_HOLDS_PVT.HOLD_SOURCE_REC_TYPE;
      l_hold_exists       Varchar2(1) := 'N';
      l_Line_TYpe_Name    Varchar2(80);
      C_HOLD_NAME         Varchar2(80) := 'XXWC_PRICE_CHANGE_HOLD' ;
      C_LINE_TYPE_STD     Varchar2(80) := 'STANDARD LINE' ;
      l_proc_name           VARCHAR2(80):='Apply_Price_Change_Hold';  -- Version# 5.4, TMS# 20150202-00030
      l_arg3                VARCHAR2(50);  -- Version# 5.4, TMS# 20150202-00030

      APPLY_HOLD_EXP      Exception ;
   Begin
      --
      IF l_debug_level  > 0 THEN
         oe_debug_pub.add('*** Beging of API  XXWC_ONT_ROUTINES_PKG.Apply_Price_Change_Hold for item type/item key'||ITEMTYPE||'/'||ITEMKEY , 1 ) ;
      END IF;

      if (funcmode = 'RUN') then
         l_line_id      := to_number(itemkey);

         Begin

-- Version# 5.1  > Start
             SELECT header_id
               INTO l_header_id
               FROM (SELECT ool.header_id
                       FROM apps.oe_order_lines        ool
                          , apps.oe_order_headers ooh
                      WHERE ool.line_id             = l_line_id
                        AND ool.line_type_id        = FND_PROFILE.VALUE('XXWC_STANDARD_LINE_TYPE') --  1002 -- Version# 5.3 Replaced the hard coded value by pattabhi on 10/27/2014 TMS# 20141002-00060                  -- Standard Order               -- Standard Line
                        AND ooh.order_type_id       = FND_PROFILE.VALUE('XXWC_STANDARD_ORDER_TYPE')  -- 1001 -- Version# 5.3 Replaced the hard coded value by pattabhi on 10/27/2014 TMS# 20141002-00060                  -- Standard Order
                        AND ooh.header_id           = ool.header_id
                     UNION
                     SELECT ool.header_id
                       FROM apps.oe_order_lines        ool
                          , apps.oe_order_headers ooh
                      WHERE ool.line_id             = l_line_id
                        AND ool.line_type_id        = FND_PROFILE.VALUE('XXWC_BILL_ONLY_LINE_TYPE') --  1003  Replaced the hard coded value by pattabhi on 10/27/2014 TMS# 20141002-000601003                 -- Bill Only Line
                        AND ooh.order_type_id       = FND_PROFILE.VALUE('XXWC_COUNTER_ORDER_TYPE')  -- 1004 Replaced the hard coded value by pattabhi on 10/28/2014 TMS# 20141002-00060                -- Counter Order
                        AND ooh.header_id           = ool.header_id);

/*
             Select ool.Header_ID , ott.Name
             Into L_Header_ID , l_line_Type_Name
             From OE_ORDER_LINES_ALL ool,
                  oe_transaction_types ott
             Where ool.Line_ID = l_Line_ID
             And ott.Transaction_Type_Code = 'LINE'
--             and ott.Order_Category_Code = 'ORDER' -- Version# 4.0
             and ott.Transaction_TYpe_ID = ool.Line_TYpe_ID
--             And ott.Name = C_LINE_TYPE_STD        -- Version# 4.0
             ;
*/

-- Version# 5.1  < End
         Exception
            When Others Then
--                oe_debug_pub.add('*** Line Type is Not Standard Line' ) ;    -- Version# 4.0
                oe_debug_pub.add('*** Error while deriving Order HeaderId' ) ; -- Version# 4.0
                resultout := 'COMPLETE';
                OE_STANDARD_WF.Clear_Msg_Context;
                return;
         End ;
         OE_STANDARD_WF.Set_Msg_Context(actid);
         l_arg3:=TO_CHAR (actid);   -- Version# 5.4, TMS# 20150202-00030 

         -- Check if Hold Exists on Sales Order Line
         --Satish U : 02/05/2013 :
         Begin
             Select Hold_ID
             Into  l_Hold_ID
             From OE_HOLD_DEFINITIONS
             Where Name = C_HOLD_NAME ;
         Exception
             When Others Then
                  l_Hold_ID := NULL;
         End ;

         If l_Hold_ID is NULL Then
            -- no result needed
            resultout := 'COMPLETE';
            oe_debug_pub.add('*** XXWC PRICE CHANGE HOLD Is not Defined' ) ;
            return;
         End If;

         Begin
             -- Check if Hold exists at Order Line Level
             Select 'Y'
             Into l_Hold_Exists
             From   oe_order_holds ooh,
                  oe_hold_sources ohs
             Where ooh.Header_Id = l_header_id
             AND   ooh.Line_ID   = l_Line_ID
             and   ohs.Hold_Id   = l_Hold_ID
             And   ooh.Hold_Release_ID is Null
             And   ooh.hold_source_ID = ohs.Hold_Source_ID
             AND   rownum = 1;

             -- no result needed
             resultout := 'COMPLETE';
             oe_debug_pub.add('*** XXWC PRICE CHANGE HOLD exists, so no need to create another hold' ) ;
             return;
         Exception
            When Others Then
               l_Hold_Exists := 'N';
         End ;


         If l_Hold_Exists = 'N' Then
            l_hold_source_rec := OE_HOLDS_PVT.G_MISS_HOLD_SOURCE_REC;
            l_hold_source_rec.hold_entity_code := 'O';          -- order level hold
            l_hold_source_rec.hold_entity_id   := l_header_id; -- header_id of the order
            l_hold_source_rec.header_id        := l_header_id;
            l_hold_source_rec.line_id          := l_Line_id;
            l_hold_source_rec.Hold_id          := l_Hold_id;
            l_hold_source_rec.hold_until_date  := NULL; -- SYSDATE + 1;

            OE_Holds_PUB.Apply_Holds(p_api_version       => 1.0
                              , p_init_msg_list     => FND_API.G_TRUE
                              , p_commit            => FND_API.G_TRUE
                              , p_hold_source_rec   => l_hold_source_rec
                              , x_return_status     => l_return_status
                              , x_msg_count         => l_msg_count
                              , x_msg_data          => l_msg_data);

            If l_Return_Status  = FND_API.G_RET_STS_SUCCESS Then
               oe_debug_pub.add('*** Hold Created Successfully' ) ;
               resultout := 'COMPLETE';
               OE_STANDARD_WF.Clear_Msg_Context;
               return;
            Else
               oe_debug_pub.add('*** Could not create Hold' ) ;
            End IF;
         End If;
      Elsif (funcmode = 'CANCEL') then
         -- no result needed
         resultout := 'COMPLETE';
         OE_STANDARD_WF.Clear_Msg_Context;
         return;
      End If;
   EXCEPTION
      WHEN APPLY_HOLD_EXP   Then

         -- Satish U: 08-DEC-2011 Added following logic to retrieve error message
         FOR K IN 1 .. l_msg_count loop
            l_msg_data := oe_msg_pub.get( p_msg_index => k, p_encoded => 'F'  );
         End Loop ;

         IF l_debug_level  > 0 THEN
            oe_debug_pub.add(  'Apply_Price_Change_Hold exception in XXWC_ONT_ROUTINES_PKG.Apply_Price_Change_Hold' ) ;
            oe_debug_pub.add('Exception error message is  '||l_msg_data,0.5); -- bug 4189737
         END IF;


         -- The line below records this function call in the error system
         -- in the case of an exception.
         wf_core.CONTEXT (g_pkg_name,  -- 'XXWC_ONT_ROUTINES_PKG', -- Version# 5.4, TMS# 20150202-00030
                          l_proc_name, -- 'Apply_Price_Change_Hold', -- Version# 5.4, TMS# 20150202-00030
                          itemtype,
                          itemkey,
                          l_arg3,  -- TO_CHAR (actid), -- Version# 5.4, TMS# 20150202-00030
                          funcmode
                         );
         -- start data fix project
         oe_standard_wf.add_error_activity_msg (p_actid         => actid,
                                                p_itemtype      => itemtype,
                                                p_itemkey       => itemkey
                                               );
         -- end data fix project
         oe_standard_wf.save_messages;
         oe_standard_wf.clear_msg_context;
         RAISE;
      WHEN OTHERS  THEN
         IF l_debug_level  > 0 THEN
            oe_debug_pub.add(  'Others exception in XXWC_ONT_ROUTINES_PKG.Apply_Price_Change_Hold' ) ;
            oe_debug_pub.add('Exception is '||sqlerrm,0.5); -- bug 4189737
         END IF;
         -- The line below records this function call in the error system
         -- in the case of an exception.
         wf_core.CONTEXT (g_pkg_name,  --'XXWC_ONT_ROUTINES_PKG', -- Version# 5.4, TMS# 20150202-00030
                          l_proc_name, --'Apply_Price_Change_Hold', -- Version# 5.4, TMS# 20150202-00030
                          itemtype,
                          itemkey,
                          l_arg3, --TO_CHAR (actid), -- Version# 5.4, TMS# 20150202-00030
                          funcmode
                         );
         -- start data fix project
         oe_standard_wf.add_error_activity_msg (p_actid         => actid,
                                                p_itemtype      => itemtype,
                                                p_itemkey       => itemkey
                                               );
         -- end data fix project
         oe_standard_wf.save_messages;
         oe_standard_wf.clear_msg_context;
         RAISE;
   End Apply_Price_Change_Hold;

-- 06/19/2013 CG: TMS 20130617-01047: Added to be used by the Backorder Report to determine if a lot line has lot reservation or not
function is_lot_line_reserved (p_line_id in NUMBER) return varchar2 is
    l_ordered_qty number;
    l_reserved_qty number;
begin
    l_ordered_qty := 0;
    l_reserved_qty := 0;

    begin
        select  nvl(ordered_quantity, 0)
        into    l_ordered_qty
        from    apps.oe_order_lines
        where   line_id = p_line_id;
    exception
    when others then
        l_ordered_qty := 0; 
    end;
    
    begin
        SELECT NVL (SUM (mr.primary_reservation_quantity), 0)
           INTO l_reserved_qty
           FROM mtl_reservations mr
          WHERE EXISTS (
                   SELECT *
                     FROM apps.oe_order_lines oola
                    WHERE oola.line_id = p_line_id
                      AND mr.demand_source_line_id = oola.line_id
                      AND mr.inventory_item_id = oola.inventory_item_id
                      AND mr.organization_id = oola.ship_from_org_id);
    exception
    when others then
        l_reserved_qty := 0;
    end;
    
    if l_reserved_qty < l_ordered_qty then
        return ('N');
    else
        return ('Y');
    end if;
exception
when others then
    return ('N');
end is_lot_line_reserved;

/*************************************************************************
  Function : xxwc_bko_visibility_prc

  PURPOSE:   Procedure to check backorder status and update user item
             description for lines that have not been processed through
             the shipping tables
             TMS 20130715-00472

  REVISIONS:
  Ver        Date        Author                     Description
  ---------  ----------  ---------------         -------------------------
  1.0        08/20/2013  Consuelo Gonzalez      Initial Version
  8.0        03/09/2016   Manjula Chellappan       TMS# 20150622-00156 Shipping Extension modification for PUBD  
*************************************************************************/
PROCEDURE xxwc_bko_visibility_prc ( RETCODE     OUT VARCHAR2
                                   , ERRMSG     OUT VARCHAR2)
IS
    l_error_message2 clob;
    l_conc_req_id   number := fnd_global.conc_request_id;
    l_start_time    number;  
    l_end_time      number;
    l_login_id      number := fnd_global.login_id;    
    
    cursor cur_order_lines is
        select  oh.order_number
                , (ol.line_number||'.'||ol.shipment_number) line_number
                , ol.ordered_item
                , ol.ordered_quantity
--              , ol.attribute11 force_ship_qty -- Commented for Ver 8.0
                , DECODE(ol.subinventory, 'PUBD', ol.ordered_quantity, ol.attribute11) force_ship_qty -- Added for Ver 8.0
                , ol.user_item_description
                , oh.header_id
                , ol.line_id
                , ol.ship_from_org_id
                , ol.inventory_item_id
                , ol.cancelled_flag
                , ol.attribute17 bko_dff
                , ol.rowid ol_rid
                , ol.subinventory -- Added for Ver 8.0
        from    apps.oe_order_headers oh
                ,apps.oe_order_lines ol
        where   oh.order_type_id in (FND_PROFILE.VALUE('XXWC_STANDARD_ORDER_TYPE'),FND_PROFILE.VALUE('XXWC_INTERNAL_ORDER_TYPE')) -- (1001,1011)  Replaced the hard coded value by pattabhi on 10/27/2014 TMS# 20141002-000601003 
        and     oh.header_id  = ol.header_id
        and     ol.creation_date >= (sysdate - 10)
        and     ol.flow_status_code != 'CLOSED'
        -- 08/25/2013 CG: TMS 20130826-00696: Update to exclude Drop Ships
        and     ol.source_type_code != 'EXTERNAL'
        and     ( 
                    (nvl(ol.booked_flag, 'N') = 'Y'
                     and ol.attribute17 is null
                     and not exists (select  'Delivery Docs Gen'
                                    from    xxwc.xxwc_wsh_shipping_stg x1
                                    where   x1.header_id = ol.header_id
                                    and     x1.line_id = ol.line_id
                                    and     x1.ship_from_org_id = ol.ship_from_org_id)
                    )
                    OR
                    ( nvl(ol.booked_flag, 'N') = 'Y'
                      and ol.last_update_date >= (sysdate - interval '15' minute)
                      and not exists (select  'Delivery Docs Gen'
                                        from    xxwc.xxwc_wsh_shipping_stg x1
                                        where   x1.header_id = ol.header_id
                                        and     x1.line_id = ol.line_id
                                        and     x1.ship_from_org_id = ol.ship_from_org_id)
                    )
                 );
    
    v_delivery_status     VARCHAR2 (124);
    l_shipped_qty         NUMBER;
    l_count               NUMBER;
    l_commit_limit        NUMBER;
    l_item_type           VARCHAR2(240);
    l_co_qty              NUMBER;
    
BEGIN
    write_log('Starting BKO Status Updates...');
    write_log('==============================');
    write_log(' ');
    
    l_count := 0;
    l_commit_limit := 0;
    for c1 in cur_order_lines
    loop
        exit when cur_order_lines%notfound;
        
        write_log('Order '||c1.order_number||' - Line Number '||c1.line_number||' - BKO DFF: '||c1.bko_dff);
        write_log('Ordered Item '||c1.ordered_item||' - Ordered Qty: '||c1.ordered_quantity||' - Force Ship Qty: '||c1.force_ship_qty);
        write_log('Current Status '||c1.user_item_description);
        
        v_delivery_status := NULL;
        l_item_type       := NULL;
        l_co_qty          := NULL;
        
        BEGIN
            SELECT msib.item_type                  
                  , (SELECT NVL (
                                SUM (
                                      l.ordered_quantity
                                    * po_uom_s.po_uom_convert (um.unit_of_measure
                                                              ,msib.primary_unit_of_measure
                                                              ,l.inventory_item_id))
                               ,0)
                       FROM apps.oe_order_lines l, apps.oe_order_headers h, mtl_units_of_measure_vl um
                      WHERE     l.header_id = h.header_id
                            AND h.flow_status_code NOT IN ('ENTERED', 'CLOSED', 'CANCELLED')
                            AND l.line_type_id = FND_PROFILE.VALUE('XXWC_COUNTER_LINE_TYPE')  -- 1005 Replaced the hard coded value by pattabhi on 10/28/2014 TMS# 20141002-00060 
                            AND l.ship_from_org_id = msib.organization_id
                            AND l.inventory_item_id = msib.inventory_item_id
                            AND l.order_quantity_uom = um.uom_code)
              INTO l_item_type
                   , l_co_qty
              FROM apps.oe_order_lines ol, mtl_system_items_b msib
             WHERE     ol.line_id = c1.line_id
                   AND ol.inventory_item_id = msib.inventory_item_id
                   AND ol.ship_from_org_id = msib.organization_id;
        EXCEPTION
            WHEN OTHERS
            THEN
                l_item_type       := NULL;
                l_co_qty          := NULL;
        END;
        

        IF NVL (l_item_type, 'UNKNOWN') IN ('INTANGIBLE', 'FRT', 'PTO')
        THEN
            v_delivery_status := 'Awaiting Shipping';
        ELSE
            IF c1.force_ship_qty IS NULL
            THEN
                l_shipped_qty :=
                    xxwc_ont_routines_pkg.get_onhand (p_inventory_item_id   => c1.inventory_item_id
                                                     ,p_organization_id     => c1.ship_from_org_id
--                                                   ,p_subinventory        => 'General' -- Commented for Ver 8.0
                                                     ,p_subinventory        => c1.subinventory -- Added for Ver 8.0
                                                     ,p_return_type         => 'H');

                IF l_shipped_qty < 0
                THEN
                    l_shipped_qty := 0;
                END IF;

                l_shipped_qty := l_shipped_qty - l_co_qty;

                write_log('l_shipped_qty: '||l_shipped_qty);

                IF l_shipped_qty <= 0
                THEN
                    l_shipped_qty := 0;
                    v_delivery_status := 'BACKORDERED';
                ELSIF l_shipped_qty >= c1.ordered_quantity
                THEN
                    v_delivery_status := 'Awaiting Shipping';
                ELSIF l_shipped_qty < c1.ordered_quantity AND l_shipped_qty > 0
                THEN
                    v_delivery_status := 'PARTIAL BACKORDERED';
                ELSE
                    v_delivery_status := 'Unknown Stat';
                END IF;
            ELSE
                if c1.force_ship_qty = 0 then
                    v_delivery_status := 'BACKORDERED';
                elsif c1.force_ship_qty = c1.ordered_quantity then
                    v_delivery_status := 'Awaiting Shipping';
                elsif c1.force_ship_qty > 1 and c1.force_ship_qty < c1.ordered_quantity then
                    v_delivery_status := 'PARTIAL BACKORDERED';
                else
                    v_delivery_status := 'Awaiting Shipping';
                end if;
            END IF;
        END IF;

        -- if c1.user_item_description is not null and c1.user_item_description != v_delivery_status then
        --     v_delivery_status := c1.user_item_description; 
        -- end if;

        IF nvl(c1.cancelled_flag, 'N') = 'Y' THEN
            v_delivery_status := 'CANCELLED';
        END IF;
        
        write_log('New Status '||v_delivery_status);
        
        if c1.bko_dff is null or c1.user_item_description != v_delivery_status then
        
            begin
                UPDATE oe_order_lines
                       SET user_item_description = v_delivery_status
                           ,attribute16 = DECODE (v_delivery_status
                                                  ,'OUT_FOR_DELIVERY', 'Y'
                                                  ,'OUT_FOR_DELIVERY/PARTIAL_BACKORDER', 'Y'
                                                  ,'DELIVERED', 'D'
                                                  ,NULL)
                           ,attribute17 = 'Y'
                     WHERE ROWID = c1.ol_rid;
                
                l_count := l_count + 1;
            exception
            when others then
                write_log('Could not update line. Error: '||SQLERRM);
            end;
        end if;
    
        l_commit_limit := l_commit_limit + 1;
        
        if l_commit_limit >= 100 then
            commit;
        end if;        
        
        write_log(' ');
    end loop;
    
    write_log('Updated '||l_count||'  order lines');
    
    commit;
EXCEPTION
WHEN OTHERS THEN
    l_error_message2 := 'xxwc_ont_routines_pkg.xxwc_bko_visibility_prc '
                || 'Error_Stack...'
                || DBMS_UTILITY.format_error_stack ()
                || ' Error_Backtrace...'
                || DBMS_UTILITY.format_error_backtrace ();

    xxcus_error_pkg.xxcus_error_main_api (
        p_called_from         => 'xxwc_ont_routines_pkg.xxwc_bko_visibility_prc'
       ,p_calling             => 'xxwc_ont_routines_pkg.xxwc_bko_visibility_prc'
       ,p_request_id          => l_conc_req_id
       ,p_ora_error_msg       => l_error_message2
       ,p_error_desc          => 'Error running XXWC OM Backorder Visibility Processing'
       ,p_distribution_list   => 'cgonzalez@luciditycg.com'
       ,p_module              => 'ONT');

    write_log('Error in main xxwc_ont_routines_pkg.xxwc_bko_visibility_prc. Error: '||l_error_message2);
    
    errmsg := l_error_message2;
    retcode := '2';
END xxwc_bko_visibility_prc;

-- Version# 10.0 > Start
/*************************************************************************
   *   Function : xxwc_update_user_item_desc
   *
   *   PURPOSE:   This procedure is called from WF Node, takes Line_Is as input parameter
   *              and update user_item_description at line level
   *
   * REVISIONS:
   *  Ver        Date         Author            Description
   *  ---------  ----------   ---------------   -------------------------
   *  10.0        01/23/2017   Rakesh Patel      TMS# 20161116-00261 User item description not updated based on the line status 
   * ************************************************************************/
   PROCEDURE xxwc_update_user_item_desc (
      itemtype    IN              VARCHAR2,
      itemkey     IN              VARCHAR2,
      actid       IN              NUMBER,
      funcmode    IN              VARCHAR2,
      resultout   IN OUT NOCOPY VARCHAR2
   )
   IS
      l_debug_level    CONSTANT NUMBER := oe_debug_pub.g_debug_level;
      l_line_id        NUMBER;
      l_wait_sec       NUMBER;
      l_proc_name      VARCHAR2(100) := 'update_user_item_description';
      l_arg3           VARCHAR2(50);
      x_line_tbl       OE_Order_PUB.Line_Tbl_Type;
      l_return_status  VARCHAR2(30);
   BEGIN
      IF l_debug_level > 0
      THEN
         oe_debug_pub.ADD ('Entering Check Wait to Fulfill Line' || itemkey,
                           1
                          );
      END IF;

      IF (funcmode = 'RUN')
      THEN
         oe_standard_wf.set_msg_context (actid);

         l_arg3:=TO_CHAR(actid);

         IF itemtype = oe_globals.G_WFI_LIN 
         THEN
            l_line_id := TO_NUMBER (itemkey);

            OE_Line_Util.Lock_Rows(  p_line_id       => l_line_id
                                   , p_header_id     => NULL
                                   , x_line_tbl      => x_line_tbl
                                   , x_return_status => l_return_status
                                  );
         
            UPDATE oe_order_lines ool
               SET user_item_description = 'DELIVERED'
                   , attribute16         = 'D'
                   , last_update_date    = SYSDATE
                   , last_updated_by     = FND_GLOBAL.USER_ID
                   , last_update_login   = FND_GLOBAL.LOGIN_ID
             WHERE 1 = 1
               AND line_id = l_line_id
               AND user_item_description  = 'OUT_FOR_DELIVERY';
            
            COMMIT; -- to release lock
                          
            resultout := 'COMPLETE';
            RETURN;

         END IF;
      ELSE
         -- Order line should go thru normal process flow.
         resultout := 'COMPLETE';
         RETURN;
      END IF;
   EXCEPTION
      WHEN OTHERS
      THEN
        resultout := 'COMPLETE';
        RETURN;
   END xxwc_update_user_item_desc;
   -- Version# 10.0 < End
   
   /*************************************************************************
   *   Procedure : XXWC_LOG_PICKSLIP_PRINT_HIST
   *
   *   PURPOSE:   This procedure insert print log of pick slip report into custom tables.
   *
   * REVISIONS:
   *  Ver        Date         Author            Description
   *  ---------  ----------   ---------------   -------------------------
   *  12.0        04/27/2017   Niraj K Ranjan   TMS#20160815-00078   Branch Visit - Pick Ticket Reprint Improvements
   * ************************************************************************/
   PROCEDURE xxwc_log_pickslip_print_hist (p_header_id    IN NUMBER,
                                           p_printer      IN VARCHAR2,
                                           p_copies       IN NUMBER,
                                           p_prnt_org_id  IN NUMBER   
                                          )
   IS
      ln_user_id                  FND_USER.USER_ID%TYPE := FND_GLOBAL.USER_ID;
      lvc_shipping_instructions   XXWC.XXWC_OM_SO_HDR_PICK_AUD_TBL.SHIPPING_INSTRUCTIONS%TYPE;
      ln_ship_from_org_id         NUMBER;
      ld_request_date             DATE;
      lvc_freight_carrier_code    XXWC.XXWC_OM_SO_HDR_PICK_AUD_TBL.FRIEIGHT_CARRIER_CODE%TYPE;
      lvc_sold_to_contact_id      NUMBER;
      ln_sequence_id              NUMBER := XXWC.XXWC_OM_SO_HDR_PICK_AUD_SEQ.NEXTVAL;
      v_errbuf                    VARCHAR2 (4000);
   BEGIN
      BEGIN
         SELECT shipping_instructions,
                ship_from_org_id,
                request_date,
                FREIGHT_CARRIER_CODE,
                sold_to_contact_id
           INTO lvc_shipping_instructions,
                ln_ship_from_org_id,
                ld_request_date,
                lvc_freight_carrier_code,
                lvc_sold_to_contact_id
           FROM OE_ORDER_HEADERS_ALL
          WHERE HEADER_ID = p_header_id;
      EXCEPTION
         WHEN OTHERS
         THEN
            lvc_shipping_instructions := NULL;
            ln_ship_from_org_id := NULL;
            ld_request_date := NULL;
            lvc_freight_carrier_code := NULL;
            lvc_sold_to_contact_id := NULL;
      END;

      IF    lvc_shipping_instructions IS NOT NULL
         OR ln_ship_from_org_id IS NOT NULL
         OR ld_request_date IS NOT NULL
         OR lvc_freight_carrier_code IS NOT NULL
         OR lvc_sold_to_contact_id IS NOT NULL
      THEN
         INSERT INTO XXWC.XXWC_OM_SO_HDR_PICK_AUD_TBL (SEQUENCE_ID,
                                                       HEADER_ID,
                                                       SHIPPING_INSTRUCTIONS,
                                                       SHIP_FROM_ORG_ID,
                                                       PRINT_ORG_ID, 
                                                       REQUEST_DATE,
                                                       FRIEIGHT_CARRIER_CODE,
                                                       SOLD_TO_CONTACT_ID,
                                                       PRINTER,
                                                       COPIES,
                                                       CREATION_DATE,
                                                       CREATED_BY,
                                                       LAST_UPDATE_DATE,
                                                       LAST_UPDATED_BY)
              VALUES (ln_sequence_id,
                      p_header_id,
                      lvc_shipping_instructions,
                      ln_ship_from_org_id,
                      p_prnt_org_id, 
                      ld_request_date,
                      lvc_freight_carrier_code,
                      lvc_sold_to_contact_id,
                      P_PRINTER,
                      P_COPIES,
                      SYSDATE,
                      ln_user_id,
                      SYSDATE,
                      ln_user_id);
      END IF;

      INSERT INTO XXWC.XXWC_OM_SO_LINE_PICK_AUD_TBL (HDR_SEQUENCE_ID,
                                                     HEADER_ID,
                                                     LINE_ID,
													 LINE_NUMBER,
													 SHIPMENT_NUMBER,
                                                     SHIPPING_INSTRUCTIONS,
                                                     SHIP_FROM_ORG_ID,
                                                     REQUEST_DATE,
                                                     SHIPPING_METHOD_CODE,
                                                     FLOW_STATUS_CODE,
                                                     ORDERED_QUANTITY,
                                                     CANCELLED_QUANTITY,
                                                     CREATION_DATE,
                                                     CREATED_BY,
                                                     LAST_UPDATE_DATE,
                                                     LAST_UPDATED_BY)
         SELECT ln_sequence_id,
                HEADER_ID,
                LINE_ID,
				LINE_NUMBER,
				SHIPMENT_NUMBER,
                SHIPPING_INSTRUCTIONS,
                SHIP_FROM_ORG_ID,
                REQUEST_DATE,
                SHIPPING_METHOD_CODE,
                FLOW_STATUS_CODE,
                ORDERED_QUANTITY,
                CANCELLED_QUANTITY,
                SYSDATE,
                LN_USER_ID,
                SYSDATE,
                LN_USER_ID
           FROM APPS.OE_ORDER_LINES_ALL
          WHERE HEADER_ID = P_HEADER_ID;
      COMMIT;
   EXCEPTION
      WHEN OTHERS
      THEN
         v_errbuf :=
               'Error_Stack...'
            || CHR (10)
            || DBMS_UTILITY.format_error_stack ()
            || CHR (10)
            || ' Error_Backtrace...'
            || CHR (10)
            || DBMS_UTILITY.format_error_backtrace ();



         xxcus_error_pkg.xxcus_error_main_api (
            p_called_from         => 'XXWC_ONT_ROUTINES_PKG',
            p_calling             => 'XXWC_LOG_PICKSLIP_PRINT_HIST',
            p_request_id          => fnd_global.conc_request_id,
            p_ora_error_msg       => SUBSTR (
                                       REGEXP_REPLACE (v_errbuf,
                                                       '[[:cntrl:]]',
                                                       NULL),
                                       1,
                                       2000),
            p_error_desc          =>    'Error running '
                                     || 'XXWC_ONT_ROUTINES_PKG'
                                     || '.'
                                     || 'XXWC_LOG_PICKSLIP_PRINT_HIST',
            p_distribution_list   => 'HDSOracleDevelopers@hdsupply.com',
            p_module              => 'ONT');
   END xxwc_log_pickslip_print_hist;
   
   /*************************************************************************
   *   Function : xxwc_get_line_priority
   *
   *   PURPOSE:   This procedure is called from XXWC_OM_PICK_SLIP report to display lines
   *              changes according to priority.
   *
   * REVISIONS:
   *  Ver        Date         Author            Description
   *  ---------  ----------   ---------------   -------------------------
   *  12.0        04/27/2017   Niraj K Ranjan   TMS#20160815-00078   Branch Visit - Pick Ticket Reprint Improvements
   * ************************************************************************/
   FUNCTION xxwc_get_line_priority(p_header_id        IN NUMBER
                                  ,p_line_id          IN NUMBER
								  ,p_line_number      IN NUMBER
								  ,p_max_seq          IN NUMBER
                                  ,p_pre_max_seq      IN NUMBER
                                  ,p_report_prog_name IN VARCHAR2
                                  ,p_ship_from_org_id IN NUMBER
                                  ,p_scenario         IN VARCHAR2) -- S = SELECT, W = WHERE, Q = QTY
   RETURN NUMBER
   IS
      l_line_count NUMBER;
      l_whse_chng_line_count NUMBER;
      l_line_priority NUMBER := 0;
      l_reprint_ind NUMBER;
   BEGIN
      IF p_report_prog_name IN ('XXWC_OM_PICK_SLIP','XXWC_INT_ORD_PICK') THEN
          SELECT COUNT(1) INTO l_reprint_ind
          FROM xxwc.xxwc_om_so_hdr_pick_aud_tbl xosh
          WHERE xosh.header_id = p_header_id
		  AND   xosh.print_org_id = p_ship_from_org_id;
          
          --CANCELLED
          IF p_scenario IN ('S','W') THEN
              --Query to check if this line is CANCELLED as compared to previous version
              SELECT COUNT(1) INTO l_line_count
               FROM xxwc.xxwc_om_so_line_pick_aud_tbl XOSL
               WHERE header_id = p_header_id
               AND   line_id = p_line_id
               AND   flow_status_code = 'CANCELLED'
               AND   hdr_sequence_id = p_max_seq
               AND   ship_from_org_id= p_ship_from_org_id --Added on 5/19 by Rakesh Patel
               AND  NOT EXISTS(SELECT 1 
                                FROM xxwc.xxwc_om_so_line_pick_aud_tbl xosl2
                                 WHERE xosl2.header_id = p_header_id
                                 AND   xosl2.line_id =p_line_id
                                 AND   xosl2.flow_status_code = 'CANCELLED'
                                 AND   xosl2.hdr_sequence_id = p_pre_max_seq
                              ) ;
              IF l_line_count > 0 AND l_reprint_ind > 1 THEN
                 l_line_priority := 1;
                 RETURN l_line_priority;
              END IF;
          END IF; -- IF p_scenario IN ('S','W') THEN
          
          --NEW LINE
          IF p_scenario = 'S' THEN
              --Query to check if this line is NEW LINE as compared to previous version
              SELECT COUNT(1) INTO l_line_count
              FROM   xxwc.xxwc_om_so_line_pick_aud_tbl xosl
              WHERE xosl.line_id = p_line_id
              AND   xosl.header_id = p_header_id
              AND   hdr_sequence_id = p_pre_max_seq;
              --If a line's whse is changed then that line will be considered as new line for destination whse
              SELECT COUNT(1) INTO l_whse_chng_line_count FROM dual WHERE
              (SELECT ship_from_org_id 
               FROM xxwc.xxwc_om_so_line_pick_aud_tbl
               WHERE header_id = p_header_id
               AND   line_id =p_line_id
               AND   ship_from_org_id = p_ship_from_org_id
               AND   hdr_sequence_id = p_max_seq) <>
                                              (SELECT ship_from_org_id 
                                               FROM xxwc.xxwc_om_so_line_pick_aud_tbl
                                               WHERE header_id = p_header_id
                                               AND   line_id =p_line_id
                                               AND   hdr_sequence_id = p_pre_max_seq);
                                               
              IF (l_line_count = 0 OR l_whse_chng_line_count > 0) AND l_reprint_ind > 1 THEN
                 l_line_priority := 2;
                 RETURN l_line_priority;
              END IF;
          END IF; -- IF p_scenario = 'S' THEN
          
          --QUANTITY CHANGE
          IF p_scenario IN ('Q', 'S') THEN
              --Query to check if ordered QUANTITY has changed in previous version and current version
              SELECT COUNT(1) INTO l_line_count FROM dual WHERE
              (SELECT SUM(ordered_quantity) 
               FROM xxwc.xxwc_om_so_line_pick_aud_tbl
               WHERE header_id = p_header_id
               --AND   line_id =p_line_id
			   AND   line_number = p_line_number
			   AND   hdr_sequence_id = p_max_seq) <>
                                          (SELECT SUM(ordered_quantity) 
                                           FROM xxwc.xxwc_om_so_line_pick_aud_tbl
                                           WHERE header_id = p_header_id
                                           --AND   line_id =p_line_id
										   AND   line_number = p_line_number
				                            AND   hdr_sequence_id = p_pre_max_seq); 
               
              IF l_line_count > 0 AND l_reprint_ind > 1 THEN
                 l_line_priority := 3;
                 RETURN l_line_priority;
              END IF;
          END IF; -- IF p_scenario IN ('Q', 'S') THEN
          
          --WAREHOUSE MODIFIED
          IF p_scenario IN ('W','S') THEN
              -- Query to check if the warehouse is modified (parameter value = source Warehouse)
              --If a line's whse is changed then that line will be considered as modified line for source whse
              SELECT COUNT(1) INTO l_line_count FROM dual WHERE
              (SELECT ship_from_org_id 
               FROM xxwc.xxwc_om_so_line_pick_aud_tbl
               WHERE header_id = p_header_id
               AND   line_id =p_line_id
               AND   hdr_sequence_id = p_max_seq) <>
                                              (SELECT ship_from_org_id 
                                               FROM xxwc.xxwc_om_so_line_pick_aud_tbl
                                               WHERE header_id = p_header_id
                                               AND   line_id =p_line_id
                                               AND   ship_from_org_id = p_ship_from_org_id
                                               AND   hdr_sequence_id = p_pre_max_seq); 
              IF l_line_count > 0 AND l_reprint_ind > 1 THEN
                 l_line_priority := 4;
                 RETURN l_line_priority;
              END IF;
          END IF; -- IF p_scenario IN ('W','S') THEN 
                                           
      END IF;          
      l_line_priority := 98;      
      RETURN l_line_priority;
   EXCEPTION
      WHEN OTHERS THEN
         l_line_priority := 99;
         RETURN l_line_priority;
   END xxwc_get_line_priority;
   
END xxwc_ont_routines_pkg;
/