/******************************************************************************
   NAME:       XXWC_OM_SO_HDR_PICK_AUD_TBL_SYN.sql
   PURPOSE:    Synonym for table XXWC_OM_SO_HDR_PICK_AUD_TBL

   REVISIONS:
   Ver        Date        Author               Description
   ---------  ----------  ---------------  ------------------------------------
   1.0        02/05/2017  Niraj K Ranjan   Initial Version TMS#20160815-00078   
                                           Branch Visit - Pick Ticket Reprint Improvements
******************************************************************************/
CREATE OR REPLACE SYNONYM APPS.XXWC_OM_SO_HDR_PICK_AUD_TBL FOR XXWC.XXWC_OM_SO_HDR_PICK_AUD_TBL;