/*
 TMS: 20160922-00243 
 Date: 09/22/2016
 Notes: Script to update interfaced_flag  records for DCTM_OBJECT_ID ='092116-063734-226328' to "X" -- Duplicate so that they do not get pulled for the subsequent runs
*/
set serveroutput on size 1000000
declare
 --
 n_loc number;
 --
begin --Main Processing...
   --
   n_loc :=101;
   --
   begin 
    --
    update xxcus.xxcusap_dctm_inv_header_tbl set interfaced_flag ='D' --DUPLICATE
    where 1 =1
	and DCTM_OBJECT_ID ='092116-063734-226328';
    --
    dbms_output.put_line('Total rows updated in table xxcus.xxcusap_dctm_inv_header_tbl for DCTM_OBJECT_ID 092116-063734-226328 =>'||sql%rowcount);
    --
    n_loc :=102;
    --
    commit;
    --
   exception
    when others then
     --
     n_loc :=103;
     --
     dbms_output.put_line('@ Failed to update table xxcus.xxcusap_dctm_inv_header_tbl, @'||n_loc||', message ='||sqlerrm);
     --   
     rollback;
     --
   end;
   --  
    begin 
    --
    update xxcus.xxcusap_dctm_inv_line_tbl set interfaced_flag ='D' --DUPLICATE
    where 1 =1
	and DCTM_OBJECT_ID ='092116-063734-226328'
	and INVOICE_LINE_ID =13116945;
    --
    dbms_output.put_line('Total rows updated in table xxcus.xxcusap_dctm_inv_line_tbl for DCTM_OBJECT_ID 092116-063734-226328 =>'||sql%rowcount);
    --
    n_loc :=104;
    --
    commit;
    --
   exception
    when others then
     --
     n_loc :=105;
     --
     dbms_output.put_line('@ Failed to update table xxcus.xxcusap_dctm_inv_line_tbl, @'||n_loc||', message ='||sqlerrm);
     --   
     rollback;
     --
   end;
   -- 
exception
   when others then
      dbms_output.put_line ('TMS: 20160512-00141 , Outer block errors =' || sqlerrm);
end;
/