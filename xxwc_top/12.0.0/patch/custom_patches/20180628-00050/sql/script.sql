--Report Name            : AHH Price Variance Report - WC
--Import Instructions    : Run this file in XXEIS user from SQL*Plus or any tool like Toad, SQL Navigator. 
--Creating View EIS_XXWC_OM_AHH_PRICE_VAR_V
prompt Table type Objects cannot be imported
set scan off define off
prompt Creating Object XXEIS.EIS_XXWC_OM_AHH_PRICE_VAR_V
set scan off define off
DECLARE
mod_exist varchar2(1);
stmt CLOB;
v_objlength INTEGER := 0;
v_offset INTEGER := 1;
v_sql_long DBMS_SQL.varchar2s;
v_sql_long_count INTEGER := 1;
c NUMBER;
r NUMBER;
l_stmt varchar2(32000);
l_view_cur pls_integer;
l_view_rows NUMBER;
BEGIN 
mod_exist := XXEIS.EIS_RSC_INSERT.check_if_module_exist(660);
IF mod_exist = 'Y' THEN 
dbms_lob.createtemporary(stmt,TRUE);
dbms_lob.write( lob_loc   => stmt,amount    => length('create or replace view XXEIS.EIS_XXWC_OM_AHH_PRICE_VAR_V
 ("WAREHOUSE","ORDER_TYPE","ORDERED_DATE","LINE_FLOW_STATUS_CODE","ORDER_NUMBER","CREATED_BY","CUSTOMER_NUMBER","CUSTOMER_NAME","AVERAGE_COST","SALESREP_NAME","SALES","SALE_COST","ITEM","ITEM_DESC","QTY","UNIT_SELLING_PRICE","LINE_TYPE","DISTRICT","BRANCH_NAME","PRICE_SOURCE_TYPE","PRICE_TYPE","SYSTEM_PRICE","SXE_LAST_PRICE_PAID","VARIANCE_DELIVERED_PRICE","VARIANCE_FINAL_SELL_PRICE","SALES_IMPACT_DELIVERED_PRICE"
) as '),offset    => 1, BUFFER    => 'create or replace view XXEIS.EIS_XXWC_OM_AHH_PRICE_VAR_V
 ("WAREHOUSE","ORDER_TYPE","ORDERED_DATE","LINE_FLOW_STATUS_CODE","ORDER_NUMBER","CREATED_BY","CUSTOMER_NUMBER","CUSTOMER_NAME","AVERAGE_COST","SALESREP_NAME","SALES","SALE_COST","ITEM","ITEM_DESC","QTY","UNIT_SELLING_PRICE","LINE_TYPE","DISTRICT","BRANCH_NAME","PRICE_SOURCE_TYPE","PRICE_TYPE","SYSTEM_PRICE","SXE_LAST_PRICE_PAID","VARIANCE_DELIVERED_PRICE","VARIANCE_FINAL_SELL_PRICE","SALES_IMPACT_DELIVERED_PRICE"
) as ');
l_stmt :=  'SELECT
  ship_from_org.organization_code warehouse,
  oth.name order_type,
  trunc (oh.ordered_date) ordered_date,
  ol.flow_status_code line_flow_status_code,
  oh.order_number,
  oh.attribute7 created_by,
  XXEIS.EIS_XXWC_INV_PRE_REG_RPT_PKG.GET_CUSTOMER_NUMBER ( OL.SOLD_TO_ORG_ID, ''ACCOUNTNUM'') CUSTOMER_NUMBER ,
  XXEIS.EIS_XXWC_INV_PRE_REG_RPT_PKG.GET_CUSTOMER_NUMBER ( OL.SOLD_TO_ORG_ID, NULL) CUSTOMER_NAME,
  CASE
    WHEN mcvc.segment2 = ''PRMO''
    THEN (-1 * ol.unit_selling_price)
    ELSE xxeis.eis_xxwc_inv_pre_Reg_rpt_pkg.get_order_line_cost ( OL.LINE_ID)
  END average_cost,
  xxeis.EIS_XXWC_INV_PRE_REG_RPT_PKG.GET_SALESREP_DETAILS(oh.salesrep_id,oh.org_id,''SALESREPNAME'') salesrep_name,
  (NVL ( DECODE ( mcvc.segment2, ''PRMO'', 0, NVL (ol.unit_selling_price, 0) * DECODE (ol.line_category_code, ''RETURN'', (ol.ordered_quantity * -1), ol.ordered_quantity)), 0)) sales,
  (NVL ( DECODE ( mcvc.segment2, ''PRMO'',                                   -1 * ( NVL (ol.unit_selling_price, 0) * DECODE (ol.line_category_code, ''RETURN'', (ol.ordered_quantity * -1), ol.ordered_quantity)), (DECODE (ol.line_category_code, ''RETURN'', (ol.ordered_quantity * -1), ol.ordered_quantity)) * NVL ( xxeis.eis_xxwc_inv_pre_Reg_rpt_pkg.get_order_line_cost ( OL.LINE_ID), 0)), 0)) sale_cost,
  msi.segment1 item,
  CASE
    WHEN msi.segment1 = ''Rental Charge''
    THEN ol.user_item_description
    ELSE msi.description
  END item_desc,
  DECODE (ol.line_category_code, ''RETURN'', (ol.ordered_quantity * -1), ol.ordered_quantity) qty,
  DECODE (mcvc.segment2, ''PRMO'', 0, ol.unit_selling_price) unit_selling_price,
  otl.name line_type,
  ship_from_org.attribute8 district,
  hou.name branch_name,
  (SELECT MAX(vs.description) description
  FROM apps.oe_price_adjustments_v adj,
    (SELECT attribute10,
      name,
      list_header_id
    FROM apps.qp_secu_list_headers_vl lh
    WHERE context     =162
    AND automatic_flag=''Y''
    )lh ,
    (SELECT NVL(ffv.description,''MKT'') description,
      flex_value
    FROM apps.fnd_flex_values_vl ffv
    WHERE flex_value_set_id=1015252
    AND enabled_flag       =''Y''
    ) vs
  WHERE adj.header_id         = ol.header_id
  AND adj.line_id             = ol.line_id
  AND adj.list_header_id      = lh.list_header_id
  AND lh.attribute10          = vs.flex_value
  AND adj.applied_flag        =''Y''
  AND adj.list_line_type_code<>''FREIGHT_CHARGE''
  ) price_source_type,
  (SELECT
    CASE
      WHEN last_value(adj.automatic_flag) over (partition BY adj.header_id, adj.line_id order by adj.pricing_group_sequence ,adj.price_adjustment_id ,adj.creation_date ,adj.created_by ,adj.last_update_date ,adj.last_updated_by ,adj.last_update_login ,adj.program_application_id ,adj.program_id ,adj.program_update_date rows BETWEEN unbounded preceding AND unbounded following) = ''N''
      THEN ''MANUAL''
      WHEN lh.attribute10 = ''Contract Pricing''
      OR lh.name LIKE ''CSP%''
      THEN ''CONTRACT''
      WHEN lh.attribute10 = ''Vendor Quote''
      THEN ''VQN''
      ELSE ''SYSTEM''
    END price_type
  FROM apps.oe_price_adjustments_v adj,
    (SELECT attribute10,
      name,
      list_header_id
    FROM apps.qp_secu_list_headers_vl lh
    WHERE context     =162
    AND automatic_flag=''Y''
    )lh ,
    (SELECT NVL(ffv.description,''MKT'') description,
      flex_value
    FROM apps.fnd_flex_values_vl ffv
    WHERE flex_value_set_id=1015252
    AND enabled_flag       =''Y''
    ) vs
  WHERE adj.header_id         = ol.header_id
  AND adj.line_id             = ol.line_id
  AND adj.list_header_id      = lh.list_header_id
  AND lh.attribute10          = vs.flex_value
  AND adj.applied_flag        =''Y''
  AND adj.list_line_type_code<>''FREIGHT_CHARGE''
  AND rownum                  =1
  ) price_type,
  ol.unit_list_price system_price,
  xoal.last_price_paid sxe_last_price_paid,
  ROUND(DECODE(NVL(ol.unit_list_price,0),0,0,((NVL(ol.unit_list_price,0)      - NVL(xoal.last_price_paid,0))/NVL(ol.unit_list_price,0))),4)variance_delivered_price,
  ROUND(DECODE(NVL(ol.unit_selling_price,0),0,0,((NVL(ol.unit_selling_price,0)-NVL(xoal.last_price_paid,0))/NVL(ol.unit_selling_price,0))),4)variance_final_sell_price,
  ((NVL(ol.ordered_quantity,0)                                                * NVL(ol.unit_selling_price,0))-(NVL(ol.ordered_quantity,0)*NVL(xoal.last_price_paid,0))) sales_impact_delivered_price
  --descr#flexfield#start
  --descr#flexfield#end
FROM ra_customer_trx rct,
  ra_customer_trx_lines rctl,
  oe_order_headers oh,
  oe_order_lines ol,
  mtl_parameters ship_from_org ,
  hr_all_organization_units hou,
  oe_transaction_types_vl oth,
  oe_transaction_types_vl otl,
  mtl_system_items_kfv msi,
  mtl_item_categories micc,
  mtl_categories_kfv mcvc,
  xxwc.xxwc_oe_ais_lpp_tbl xoal
WHERE rct.customer_trx_id            = rctl.customer_trx_id
AND rctl.interface_line_attribute6   = TO_CHAR (ol.line_id)
AND rctl.interface_line_attribute1   = TO_CHAR (oh.order_number)
AND rctl.interface_line_context      = ''ORDER ENTRY''
AND oh.header_id                     = ol.header_id
AND ol.ship_from_org_id              = ship_from_org.organization_id(+)
AND ship_from_org.organization_id    = hou.organization_id(+)
AND oh.order_type_id                 = oth.transaction_type_id
AND oh.org_id                        = oth.org_id
AND ol.line_type_id                  = otl.transaction_type_id
AND ol.org_id                        = otl.org_id
AND ol.inventory_item_id             = msi.inventory_item_id(+)
AND ol.ship_from_org_id              = msi.organization_id(+)
AND msi.inventory_item_id            = micc.inventory_item_id(+)
AND msi.organization_id              = micc.organization_id(+)
AND micc.category_id                 = mcvc.category_id(+)
AND mcvc.structure_id(+)             = 101
AND micc.category_set_id(+)          = 1100000062
AND ol.inventory_item_id             = xoal.inventory_item_id(+)
AND ol.ship_to_org_id                = xoal.ship_to_org_id(+)
AND ol.ordered_item NOT             IN (''CONTOFFSET'', ''CONTBILL'')
AND TRUNC (oh.ordered_date)          < TRUNC (SYSDATE)
AND ol.flow_status_code             IN (''CLOSED'')
AND ol.invoice_interface_status_code = ''YES''
AND oth.name                        != ''INTERNAL ORDER''
AND ol.source_type_code             IN (''INTERNAL'', ''EXTERNAL'')
AND rctl.creation_date              >= TO_DATE ( TO_CHAR (xxeis.EIS_XXWC_INV_PRE_REG_RPT_PKG.GET_DATE_FROM ,xxeis.eis_rs_utility.get_date_format
  || '' HH24:MI:SS'') ,xxeis.eis_rs_utility.get_date_format
  || '' HH24:MI:SS'') + 0.25
AND rctl.creation_date <= TO_DATE ( TO_CHAR (xxeis.EIS_XXWC_INV_PRE_REG_RPT_PKG.GET_DATE_FROM ,xxeis.eis_rs_utility.get_date_format
  || '' HH24:MI:SS'') ,xxeis.eis_rs_utility.get_date_format
  || '' HH24:MI:SS'') + 1.25
AND NOT EXISTS
  (SELECT /*+INDEX(gcc GL_CODE_COMBINATIONS_U1)*/
    ''Y''
  FROM gl_code_combinations_kfv gcc
  WHERE GCC.CODE_COMBINATION_ID = MSI.COST_OF_SALES_ACCOUNT
  AND GCC.SEGMENT4              = ''646080''
  )
UNION /*-- Same Days Counter Orders*/
SELECT  /*+ INDEX(oh XXWC_OE_ORDER_HEADERS_ALL_N8)*/
ship_from_org.organization_code warehouse';
dbms_lob.APPEND( dest_lob  => stmt,      src_lob   => l_stmt );
l_stmt :=  ',
oth.name order_type,
trunc (oh.ordered_date) ordered_date,
ol.flow_status_code line_flow_status_code,
  oh.order_number,
  oh.attribute7 created_by,
  XXEIS.EIS_XXWC_INV_PRE_REG_RPT_PKG.GET_CUSTOMER_NUMBER ( OL.SOLD_TO_ORG_ID, ''ACCOUNTNUM'') CUSTOMER_NUMBER,
  XXEIS.EIS_XXWC_INV_PRE_REG_RPT_PKG.GET_CUSTOMER_NUMBER ( OL.SOLD_TO_ORG_ID, NULL) CUSTOMER_NAME,
  CASE
    WHEN mcvc.segment2 = ''PRMO''
    THEN (                    -1 * ol.unit_selling_price)
    WHEN ol.flow_status_code IN (''CLOSED'')
    THEN xxeis.eis_xxwc_inv_pre_Reg_rpt_pkg.get_order_line_cost ( OL.LINE_ID)
    WHEN otl.name = ''BILL ONLY''
    THEN 0
    ELSE DECODE ( NVL ( apps.cst_cost_api.get_item_cost (1, msi.inventory_item_id, msi.organization_id), 0) , 0, ol.unit_cost, apps.cst_cost_api.get_item_cost (1, msi.inventory_item_id, msi.organization_id))
  END average_cost,
  xxeis.EIS_XXWC_INV_PRE_REG_RPT_PKG.GET_SALESREP_DETAILS(oh.salesrep_id,oh.org_id,''SALESREPNAME'') salesrep_name,
  (NVL ( DECODE ( mcvc.segment2, ''PRMO'', 0, NVL (ol.unit_selling_price, 0) * DECODE (ol.line_category_code, ''RETURN'', (ol.ordered_quantity * -1), ol.ordered_quantity)), 0)) sales,
  (NVL ( DECODE ( mcvc.segment2, ''PRMO'',                                   -1 * ( NVL (ol.unit_selling_price, 0) * DECODE (ol.line_category_code, ''RETURN'', (ol.ordered_quantity * -1), ol.ordered_quantity)), (
  CASE
    WHEN ol.flow_status_code IN (''CLOSED'')
    THEN xxeis.eis_xxwc_inv_pre_Reg_rpt_pkg.get_order_line_cost ( OL.LINE_ID)
    WHEN otl.name = ''BILL ONLY''
    THEN 0
    ELSE DECODE ( NVL ( apps.cst_cost_api.get_item_cost ( 1, msi.inventory_item_id, msi.organization_id), 0), 0, ol.unit_cost, apps.cst_cost_api.get_item_cost ( 1, msi.inventory_item_id, msi.organization_id))
  END) * (DECODE (ol.line_category_code, ''RETURN'', (ol.ordered_quantity * -1), ol.ordered_quantity))), 0)) sale_cost,
  msi.segment1 item,
  CASE
    WHEN msi.segment1 = ''Rental Charge''
    THEN ol.user_item_description
    ELSE msi.description
  END item_desc,
  DECODE (ol.line_category_code, ''RETURN'', (ol.ordered_quantity * -1), ol.ordered_quantity) qty,
  DECODE (mcvc.segment2, ''PRMO'', 0, ol.unit_selling_price) unit_selling_price,
  otl.name line_type,
  ship_from_org.attribute8 district,
  hou.name branch_name,
  (SELECT MAX(vs.description) description
  FROM apps.oe_price_adjustments_v adj,
    (SELECT attribute10,
      name,
      list_header_id
    FROM apps.qp_secu_list_headers_vl lh
    WHERE context     =162
    AND automatic_flag=''Y''
    )lh ,
    (SELECT NVL(ffv.description,''MKT'') description,
      flex_value
    FROM apps.fnd_flex_values_vl ffv
    WHERE flex_value_set_id=1015252
    AND enabled_flag       =''Y''
    ) vs
  WHERE adj.header_id         = ol.header_id
  AND adj.line_id             = ol.line_id
  AND adj.list_header_id      = lh.list_header_id
  AND lh.attribute10          = vs.flex_value
  AND adj.applied_flag        =''Y''
  AND adj.list_line_type_code<>''FREIGHT_CHARGE''
  ) price_source_type,
  (SELECT
    CASE
      WHEN last_value(adj.automatic_flag) over (partition BY adj.header_id, adj.line_id order by adj.pricing_group_sequence ,adj.price_adjustment_id ,adj.creation_date ,adj.created_by ,adj.last_update_date ,adj.last_updated_by ,adj.last_update_login ,adj.program_application_id ,adj.program_id ,adj.program_update_date rows BETWEEN unbounded preceding AND unbounded following) = ''N''
      THEN ''MANUAL''
      WHEN lh.attribute10 = ''Contract Pricing''
      OR lh.name LIKE ''CSP%''
      THEN ''CONTRACT''
      WHEN lh.attribute10 = ''Vendor Quote''
      THEN ''VQN''
      ELSE ''SYSTEM''
    END price_type
  FROM apps.oe_price_adjustments_v adj,
    (SELECT attribute10,
      name,
      list_header_id
    FROM apps.qp_secu_list_headers_vl lh
    WHERE context     =162
    AND automatic_flag=''Y''
    )lh ,
    (SELECT NVL(ffv.description,''MKT'') description,
      flex_value
    FROM apps.fnd_flex_values_vl ffv
    WHERE flex_value_set_id=1015252
    AND enabled_flag       =''Y''
    ) vs
  WHERE adj.header_id         = ol.header_id
  AND adj.line_id             = ol.line_id
  AND adj.list_header_id      = lh.list_header_id
  AND lh.attribute10          = vs.flex_value
  AND adj.applied_flag        =''Y''
  AND adj.list_line_type_code<>''FREIGHT_CHARGE''
  AND rownum                  =1
  ) price_type,
  ol.unit_list_price system_price,
  xoal.last_price_paid sxe_last_price_paid,
  ROUND(DECODE(NVL(ol.unit_list_price,0),0,0,((NVL(ol.unit_list_price,0)      - NVL(xoal.last_price_paid,0))/NVL(ol.unit_list_price,0))),4)variance_delivered_price,
  ROUND(DECODE(NVL(ol.unit_selling_price,0),0,0,((NVL(ol.unit_selling_price,0)-NVL(xoal.last_price_paid,0))/NVL(ol.unit_selling_price,0))),4)variance_final_sell_price,
  ((NVL(ol.ordered_quantity,0)                                                * NVL(ol.unit_selling_price,0))-(NVL(ol.ordered_quantity,0)*NVL(xoal.last_price_paid,0))) sales_impact_delivered_price
  --descr#flexfield#start
  --descr#flexfield#end
FROM 
  oe_order_headers oh,
  oe_order_lines ol,
  mtl_parameters ship_from_org ,
  hr_all_organization_units hou,
  oe_transaction_types_vl oth,
  oe_transaction_types_vl otl,
  xxwc.xxwc_oe_ais_lpp_tbl xoal,
  mtl_system_items_kfv msi,
  mtl_item_categories micc,
  mtl_categories_kfv mcvc  
WHERE  oh.header_id          = ol.header_id
AND ol.ship_from_org_id      = ship_from_org.organization_id(+)
AND ship_from_org.organization_id = hou.organization_id(+)
AND oh.order_type_id         = oth.transaction_type_id
AND oh.org_id                = oth.org_id
AND ol.line_type_id          = otl.transaction_type_id
AND ol.org_id                = otl.org_id
AND ol.inventory_item_id     = xoal.inventory_item_id(+)
AND ol.ship_to_org_id        = xoal.ship_to_org_id(+)
AND ol.inventory_item_id     = msi.inventory_item_id(+) 
AND ol.ship_from_org_id      = msi.organization_id(+)   
AND msi.inventory_item_id    = micc.inventory_item_id(+)
AND msi.organization_id      = micc.organization_id(+)
AND micc.category_set_id(+)  = 1100000062
AND micc.category_id         = mcvc.category_id(+)
AND mcvc.structure_id(+)     = 101
AND ol.flow_status_code     <> ''CANCELLED''
AND ol.ordered_item NOT     IN (''CONTOFFSET'', ''CONTBILL'')
AND oth.name                != ''INTERNAL ORDER''
AND TRUNC (SYSDATE)          = TRUNC (XXEIS.EIS_XXWC_INV_PRE_REG_RPT_PKG.get_date_from)
AND oth.name                 = ''COUNTER ORDER''
AND OH.FLOW_STATUS_CODE      = ''BOOKED''
AND TRUNC (oh.ordered_date)  = TRUNC (SYSDATE)
AND ol.source_type_code     IN (''INTERNAL'', ''EXTERNAL'')
AND NOT EXISTS
  (SELECT  /*+INDEX(gcc GL_CODE_COMBINATIONS_U1)*/
    ''Y''
  FROM gl_code_combinations_kfv gcc
  WHERE gcc.code_combination_id = msi.cost_of_sales_account
  AND GCC.SEGMENT4              = ''646080''
  )
--UNION 3
UNION /* Invoiced Lines after 6 AM on the same day */
SELECT /*+ INDEX(oh OE_ORDER_HEADERS_U1)*/
ship_from_org.organization_code warehouse,
oth.name order_type,
trunc (oh.ordered_date) ordered_date,
ol.flow_status_code line_flow_status_code,
  oh.order_number,
  oh.attribute7 created_by';
dbms_lob.APPEND( dest_lob  => stmt,      src_lob   => l_stmt );
l_stmt :=  ',
  XXEIS.EIS_XXWC_INV_PRE_REG_RPT_PKG.GET_CUSTOMER_NUMBER ( OL.SOLD_TO_ORG_ID, ''ACCOUNTNUM'') CUSTOMER_NUMBER,
  XXEIS.EIS_XXWC_INV_PRE_REG_RPT_PKG.GET_CUSTOMER_NUMBER ( OL.SOLD_TO_ORG_ID, NULL) CUSTOMER_NAME,
  CASE
    WHEN mcvc.segment2 = ''PRMO''
    THEN (-1 * ol.unit_selling_price)
    ELSE xxeis.eis_xxwc_inv_pre_Reg_rpt_pkg.get_order_line_cost ( OL.LINE_ID)
  END average_cost,
  xxeis.EIS_XXWC_INV_PRE_REG_RPT_PKG.GET_SALESREP_DETAILS(oh.salesrep_id,oh.org_id,''SALESREPNAME'') salesrep_name,
  (NVL ( DECODE ( mcvc.segment2, ''PRMO'', 0, NVL (ol.unit_selling_price, 0) * DECODE (ol.line_category_code, ''RETURN'', (ol.ordered_quantity * -1), ol.ordered_quantity)), 0)) sales,
  (NVL ( DECODE ( mcvc.segment2, ''PRMO'',                                   -1 * ( NVL (ol.unit_selling_price, 0) * DECODE (ol.line_category_code, ''RETURN'', (ol.ordered_quantity * -1), ol.ordered_quantity)), (DECODE (ol.line_category_code, ''RETURN'', (ol.ordered_quantity * -1), ol.ordered_quantity)) * NVL ( xxeis.eis_xxwc_inv_pre_Reg_rpt_pkg.get_order_line_cost ( OL.LINE_ID), 0)), 0)) sale_cost,
  msi.segment1 item,
  CASE
    WHEN msi.segment1 = ''Rental Charge''
    THEN ol.user_item_description
    ELSE msi.description
  END item_desc,
  DECODE (ol.line_category_code, ''RETURN'', (ol.ordered_quantity * -1), ol.ordered_quantity) qty,
  DECODE (mcvc.segment2, ''PRMO'', 0, ol.unit_selling_price) unit_selling_price,
  otl.name line_type,
  ship_from_org.attribute8 district,
  hou.name branch_name,
  (SELECT MAX(vs.description) description
  FROM apps.oe_price_adjustments_v adj,
    (SELECT attribute10,
      name,
      list_header_id
    FROM apps.qp_secu_list_headers_vl lh
    WHERE context     =162
    AND automatic_flag=''Y''
    )lh ,
    (SELECT NVL(ffv.description,''MKT'') description,
      flex_value
    FROM apps.fnd_flex_values_vl ffv
    WHERE flex_value_set_id=1015252
    AND enabled_flag       =''Y''
    ) vs
  WHERE adj.header_id         = ol.header_id
  AND adj.line_id             = ol.line_id
  AND adj.list_header_id      = lh.list_header_id
  AND lh.attribute10          = vs.flex_value
  AND adj.applied_flag        =''Y''
  AND adj.list_line_type_code<>''FREIGHT_CHARGE''
  ) price_source_type,
  (SELECT
    CASE
      WHEN last_value(adj.automatic_flag) over (partition BY adj.header_id, adj.line_id order by adj.pricing_group_sequence ,adj.price_adjustment_id ,adj.creation_date ,adj.created_by ,adj.last_update_date ,adj.last_updated_by ,adj.last_update_login ,adj.program_application_id ,adj.program_id ,adj.program_update_date rows BETWEEN unbounded preceding AND unbounded following) = ''N''
      THEN ''MANUAL''
      WHEN lh.attribute10 = ''Contract Pricing''
      OR lh.name LIKE ''CSP%''
      THEN ''CONTRACT''
      WHEN lh.attribute10 = ''Vendor Quote''
      THEN ''VQN''
      ELSE ''SYSTEM''
    END price_type
  FROM apps.oe_price_adjustments_v adj,
    (SELECT attribute10,
      name,
      list_header_id
    FROM apps.qp_secu_list_headers_vl lh
    WHERE context     =162
    AND automatic_flag=''Y''
    )lh ,
    (SELECT NVL(ffv.description,''MKT'') description,
      flex_value
    FROM apps.fnd_flex_values_vl ffv
    WHERE flex_value_set_id=1015252
    AND enabled_flag       =''Y''
    ) vs
  WHERE adj.header_id         = ol.header_id
  AND adj.line_id             = ol.line_id
  AND adj.list_header_id      = lh.list_header_id
  AND lh.attribute10          = vs.flex_value
  AND adj.applied_flag        =''Y''
  AND adj.list_line_type_code<>''FREIGHT_CHARGE''
  AND rownum                  =1
  ) price_type,
  ol.unit_list_price system_price,
  xoal.last_price_paid sxe_last_price_paid,
  ROUND(DECODE(NVL(ol.unit_list_price,0),0,0,((NVL(ol.unit_list_price,0)      - NVL(xoal.last_price_paid,0))/NVL(ol.unit_list_price,0))),4)variance_delivered_price,
  ROUND(DECODE(NVL(ol.unit_selling_price,0),0,0,((NVL(ol.unit_selling_price,0)-NVL(xoal.last_price_paid,0))/NVL(ol.unit_selling_price,0))),4)variance_final_sell_price,
  ((NVL(ol.ordered_quantity,0) * NVL(ol.unit_selling_price,0))-(NVL(ol.ordered_quantity,0)*NVL(xoal.last_price_paid,0))) sales_impact_delivered_price
  --descr#flexfield#start
  --descr#flexfield#end
FROM
  ra_customer_trx rct,
  ra_customer_trx_lines rctl,
  oe_order_headers oh,
  oe_order_lines ol,
  mtl_parameters ship_from_org,
  hr_all_organization_units hou,
  oe_transaction_types_vl oth,
  oe_transaction_types_vl otl,
  xxwc.xxwc_oe_ais_lpp_tbl xoal,
  mtl_system_items_kfv msi,
  mtl_categories_kfv mcvc,
  mtl_item_categories micc
WHERE rct.customer_trx_id         = rctl.customer_trx_id
AND rctl.interface_line_attribute6 = TO_CHAR (ol.line_id)    
AND RCTL.INTERFACE_LINE_ATTRIBUTE1 = TO_CHAR (OH.ORDER_NUMBER)
AND RCTL.INTERFACE_LINE_CONTEXT = ''ORDER ENTRY''
and oh.header_id          = ol.header_id
AND ol.ship_from_org_id      = ship_from_org.organization_id(+)
AND ship_from_org.organization_id      = hou.organization_id(+)
AND oh.order_type_id         = oth.transaction_type_id
AND oh.org_id                = oth.org_id
AND ol.line_type_id          = otl.transaction_type_id
AND ol.org_id                = otl.org_id
AND ol.inventory_item_id     = xoal.inventory_item_id(+)
AND ol.ship_to_org_id        = xoal.ship_to_org_id(+)
AND ol.inventory_item_id     = msi.inventory_item_id(+) 
AND ol.ship_from_org_id      = msi.organization_id(+)   
AND msi.inventory_item_id    = micc.inventory_item_id(+)
AND msi.organization_id      = micc.organization_id(+)
AND micc.category_id         = mcvc.category_id(+)
AND mcvc.structure_id(+)     = 101
AND micc.category_set_id(+)  = 1100000062
AND ol.flow_status_code     <> ''CANCELLED''
AND ol.ordered_item NOT     IN (''CONTOFFSET'', ''CONTBILL'')
AND oth.name                != ''INTERNAL ORDER''
AND TRUNC (SYSDATE)          = TRUNC (xxeis.EIS_XXWC_INV_PRE_REG_RPT_PKG.get_date_from)
AND rctl.creation_date      >= TO_DATE ( TO_CHAR (xxeis.EIS_XXWC_INV_PRE_REG_RPT_PKG.get_date_from ,xxeis.eis_rs_utility.get_date_format
  || '' HH24:MI:SS'') ,xxeis.eis_rs_utility.get_date_format
  || '' HH24:MI:SS'') + 0.25
AND ol.source_type_code        IN (''INTERNAL'', ''EXTERNAL'')
AND NOT EXISTS
  (SELECT    /*+INDEX(gcc GL_CODE_COMBINATIONS_U1)*/
    ''Y''
  FROM gl_code_combinations_kfv gcc
  WHERE GCC.CODE_COMBINATION_ID = MSI.COST_OF_SALES_ACCOUNT
  AND GCC.SEGMENT4              = ''646080''
  )
--- UNION 4-1
UNION
SELECT 
ship_from_org.organization_code warehouse,
oth.name order_type,
trunc (oh.ordered_date) ordered_date,
ol.flow_status_code line_flow_status_code,
oh.order_number,
  oh.attribute7 created_by,
  XXEIS.EIS_XXWC_INV_PRE_REG_RPT_PKG.GET_CUSTOMER_NUMBER ( OL.SOLD_TO_ORG_ID, ''ACCOUNTNUM'') CUSTOMER_NUMBER,
  XXEIS.EIS_XXWC_INV_PRE_REG_RPT_PKG.GET_CUSTOMER_NUMBER ( OL.SOLD_TO_ORG_ID, NULL) CUSTOMER_NAME,
  CASE
    WHEN mcvc.segment2 = ''PRMO''
    THEN (                    -1 * ol.unit_selling_price)
    WHEN ol.flow_status_code IN (''CLOSED'', ''INVOICED'', ''INVOICED_PARTIAL'', ''INVOICE_DELIVERY'', ''INVOICE_HOLD'', ''INVOICE_INCOMPLETE'')
    THEN xxeis.eis_xxwc_inv_pre_Reg_rpt_pkg.get_order_line_cost (OL.LINE_ID)
    WHEN otl.name = ''BILL ONLY''
    THEN 0
    ELSE NVL ( apps.cst_cost_api.get_item_cost (1';
dbms_lob.APPEND( dest_lob  => stmt,      src_lob   => l_stmt );
l_stmt :=  ', msi.inventory_item_id, msi.organization_id), 0)
  END average_cost,
  xxeis.EIS_XXWC_INV_PRE_REG_RPT_PKG.GET_SALESREP_DETAILS(oh.salesrep_id,oh.org_id,''SALESREPNAME'') salesrep_name,
  (NVL ( DECODE ( mcvc.segment2, ''PRMO'', 0, NVL (ol.unit_selling_price, 0) * DECODE (ol.line_category_code, ''RETURN'', (ol.ordered_quantity * -1), ol.ordered_quantity)), 0)) sales,
  (NVL ( DECODE ( mcvc.segment2, ''PRMO'',                                   -1 * ( NVL (ol.unit_selling_price, 0) * DECODE (ol.line_category_code, ''RETURN'', (ol.ordered_quantity * -1), ol.ordered_quantity)), (
  CASE
    WHEN ol.flow_status_code IN (''CLOSED'', ''INVOICED'', ''INVOICED_PARTIAL'', ''INVOICE_DELIVERY'', ''INVOICE_HOLD'', ''INVOICE_INCOMPLETE'')
    THEN xxeis.eis_xxwc_inv_pre_Reg_rpt_pkg.get_order_line_cost ( OL.LINE_ID) --TMS#20150921-00332  by Siva    on 04-12-2016
    WHEN otl.name = ''BILL ONLY''
    THEN 0
    ELSE NVL ( apps.cst_cost_api.get_item_cost ( 1, msi.inventory_item_id, msi.organization_id), 0)
  END) * (DECODE (ol.line_category_code, ''RETURN'', (ol.ordered_quantity * -1), ol.ordered_quantity))), 0)) sale_cost,
  msi.segment1 item,
  CASE
    WHEN msi.segment1 = ''Rental Charge''
    THEN ol.user_item_description
    ELSE msi.description
  END item_desc,
  DECODE (ol.line_category_code, ''RETURN'', (ol.ordered_quantity * -1), ol.ordered_quantity) qty,
  DECODE (mcvc.segment2, ''PRMO'', 0, ol.unit_selling_price) unit_selling_price,
  otl.name line_type,
  ship_from_org.attribute8 district,
  hou.name branch_name,
  (SELECT MAX(vs.description) description
  FROM apps.oe_price_adjustments_v adj,
    (SELECT attribute10,
      name,
      list_header_id
    FROM apps.qp_secu_list_headers_vl lh
    WHERE context     =162
    AND automatic_flag=''Y''
    )lh ,
    (SELECT NVL(ffv.description,''MKT'') description,
      flex_value
    FROM apps.fnd_flex_values_vl ffv
    WHERE flex_value_set_id=1015252
    AND enabled_flag       =''Y''
    ) vs
  WHERE adj.header_id         = ol.header_id
  AND adj.line_id             = ol.line_id
  AND adj.list_header_id      = lh.list_header_id
  AND lh.attribute10          = vs.flex_value
  AND adj.applied_flag        =''Y''
  AND adj.list_line_type_code<>''FREIGHT_CHARGE''
  ) price_source_type,
  (SELECT
    CASE
      WHEN last_value(adj.automatic_flag) over (partition BY adj.header_id, adj.line_id order by adj.pricing_group_sequence ,adj.price_adjustment_id ,adj.creation_date ,adj.created_by ,adj.last_update_date ,adj.last_updated_by ,adj.last_update_login ,adj.program_application_id ,adj.program_id ,adj.program_update_date rows BETWEEN unbounded preceding AND unbounded following) = ''N''
      THEN ''MANUAL''
      WHEN lh.attribute10 = ''Contract Pricing''
      OR lh.name LIKE ''CSP%''
      THEN ''CONTRACT''
      WHEN lh.attribute10 = ''Vendor Quote''
      THEN ''VQN''
      ELSE ''SYSTEM''
    END price_type
  FROM apps.oe_price_adjustments_v adj,
    (SELECT attribute10,
      name,
      list_header_id
    FROM apps.qp_secu_list_headers_vl lh
    WHERE context     =162
    AND automatic_flag=''Y''
    )lh ,
    (SELECT NVL(ffv.description,''MKT'') description,
      flex_value
    FROM apps.fnd_flex_values_vl ffv
    WHERE flex_value_set_id=1015252
    AND enabled_flag       =''Y''
    ) vs
  WHERE adj.header_id         = ol.header_id
  AND adj.line_id             = ol.line_id
  AND adj.list_header_id      = lh.list_header_id
  AND lh.attribute10          = vs.flex_value
  AND adj.applied_flag        =''Y''
  AND adj.list_line_type_code<>''FREIGHT_CHARGE''
  AND rownum                  =1
  ) price_type,
  ol.unit_list_price system_price,
  xoal.last_price_paid sxe_last_price_paid,
  ROUND(DECODE(NVL(ol.unit_list_price,0),0,0,((NVL(ol.unit_list_price,0)      - NVL(xoal.last_price_paid,0))/NVL(ol.unit_list_price,0))),4)variance_delivered_price,
  ROUND(DECODE(NVL(ol.unit_selling_price,0),0,0,((NVL(ol.unit_selling_price,0)-NVL(xoal.last_price_paid,0))/NVL(ol.unit_selling_price,0))),4)variance_final_sell_price,
  ((NVL(ol.ordered_quantity,0)                                                * NVL(ol.unit_selling_price,0))-(NVL(ol.ordered_quantity,0)*NVL(xoal.last_price_paid,0))) sales_impact_delivered_price
  --descr#flexfield#start
  --descr#flexfield#end
FROM 
  oe_order_headers oh,
  oe_order_lines ol,
  mtl_parameters ship_from_org ,
  hr_all_organization_units hou,
  oe_transaction_types_vl oth,
  oe_transaction_types_vl otl,
  xxwc.xxwc_oe_ais_lpp_tbl xoal,
  mtl_system_items_kfv msi,
    mtl_item_categories micc,
  mtl_categories_kfv mcvc
WHERE  oh.header_id           = ol.header_id
AND ol.ship_from_org_id       = ship_from_org.organization_id(+)
AND ship_from_org.organization_id       = hou.organization_id(+)
AND oh.order_type_id          = oth.transaction_type_id
AND oh.org_id                 = oth.org_id
AND ol.line_type_id           = otl.transaction_type_id
AND ol.org_id                 = otl.org_id
AND ol.inventory_item_id      = xoal.inventory_item_id(+)
AND ol.ship_to_org_id         = xoal.ship_to_org_id(+)
AND ol.inventory_item_id = msi.inventory_item_id(+)  
AND ol.ship_from_org_id = msi.organization_id(+)    
AND msi.inventory_item_id     = micc.inventory_item_id(+)
AND msi.organization_id       = micc.organization_id(+)
AND micc.category_id          = mcvc.category_id(+)
AND mcvc.structure_id(+)      = 101
AND micc.category_set_id(+)   = 1100000062
AND ol.flow_status_code NOT  IN (''CLOSED'', ''CANCELLED'')
AND ol.ordered_item NOT      IN (''CONTOFFSET'', ''CONTBILL'')
AND oth.name                 != ''INTERNAL ORDER''
AND OTH.name                  = ''STANDARD ORDER''
AND (OL.USER_ITEM_DESCRIPTION = ''DELIVERED''
OR ( OL.SOURCE_TYPE_CODE      = ''EXTERNAL''
AND OL.FLOW_STATUS_CODE       = ''INVOICE_HOLD''))
AND TRUNC (SYSDATE)           = TRUNC (xxeis.EIS_XXWC_INV_PRE_REG_RPT_PKG.get_date_from)
AND OL.SOURCE_TYPE_CODE      IN (''INTERNAL'', ''EXTERNAL'')
AND NOT EXISTS
  (SELECT /*+INDEX(gcc GL_CODE_COMBINATIONS_U1)*/
    ''Y''
  FROM gl_code_combinations_kfv gcc
  WHERE gcc.code_combination_id = msi.cost_of_sales_account
  AND GCC.SEGMENT4              = ''646080''
  )
--- UNION 4-2
UNION
SELECT  /*+ INDEX(ol XXWC_OE_ORDER_LN_AL_N13) */
ship_from_org.organization_code warehouse,
oth.name order_type,
trunc (oh.ordered_date) ordered_date,
ol.flow_status_code line_flow_status_code,
  oh.order_number,
  oh.attribute7 created_by,
  XXEIS.EIS_XXWC_INV_PRE_REG_RPT_PKG.GET_CUSTOMER_NUMBER ( OL.SOLD_TO_ORG_ID, ''ACCOUNTNUM'') CUSTOMER_NUMBER,
  XXEIS.EIS_XXWC_INV_PRE_REG_RPT_PKG.GET_CUSTOMER_NUMBER ( OL.SOLD_TO_ORG_ID, NULL) CUSTOMER_NAME,
  CASE
    WHEN mcvc.segment2 = ''PRMO''
    THEN (                    -1 * ol.unit_selling_price)
    WHEN ol.flow_status_code IN (''CLOSED'', ''INVOICED'', ''INVOICED_PARTIAL'', ''INVOICE_DELIVERY'', ''INVOICE_HOLD'', ''INVOICE_INCOMPLETE'')
    THEN xxeis.eis_xxwc_inv_pre_Reg_rpt_pkg.get_order_line_cost ( OL.LINE_ID)
    WHEN otl.name = ''BILL ONLY''
    THEN 0
    ELSE NVL ( apps.cst_cost_api.get_item_cost (1, msi.inventory_item_id, msi.organization_id), 0)
  END average_cost,
  xxeis.EIS_XXWC_INV_PRE_REG_RPT_PKG.GET_SALESREP_DETAILS(oh.salesrep_id';
dbms_lob.APPEND( dest_lob  => stmt,      src_lob   => l_stmt );
l_stmt :=  ',oh.org_id,''SALESREPNAME'') salesrep_name,
  (NVL ( DECODE ( mcvc.segment2, ''PRMO'', 0, NVL (ol.unit_selling_price, 0) * DECODE (ol.line_category_code, ''RETURN'', (ol.ordered_quantity * -1), ol.ordered_quantity)), 0)) sales,
  (NVL ( DECODE ( mcvc.segment2, ''PRMO'',                                   -1 * ( NVL (ol.unit_selling_price, 0) * DECODE (ol.line_category_code, ''RETURN'', (ol.ordered_quantity * -1), ol.ordered_quantity)), (
  CASE
    WHEN ol.flow_status_code IN (''CLOSED'', ''INVOICED'', ''INVOICED_PARTIAL'', ''INVOICE_DELIVERY'', ''INVOICE_HOLD'', ''INVOICE_INCOMPLETE'')
    THEN xxeis.eis_xxwc_inv_pre_Reg_rpt_pkg.get_order_line_cost ( OL.LINE_ID)
    WHEN otl.name = ''BILL ONLY''
    THEN 0
    ELSE NVL ( apps.cst_cost_api.get_item_cost ( 1, msi.inventory_item_id, msi.organization_id), 0)
  END) * (DECODE (ol.line_category_code, ''RETURN'', (ol.ordered_quantity * -1), ol.ordered_quantity))), 0)) sale_cost,
  msi.segment1 item,
  CASE
    WHEN msi.segment1 = ''Rental Charge''
    THEN ol.user_item_description
    ELSE msi.description
  END item_desc,
  DECODE (ol.line_category_code, ''RETURN'', (ol.ordered_quantity * -1), ol.ordered_quantity) qty,
  DECODE (mcvc.segment2, ''PRMO'', 0, ol.unit_selling_price) unit_selling_price,
  otl.name line_type,
  ship_from_org.attribute8 district,
  hou.name branch_name,
  (SELECT MAX(vs.description) description
  FROM apps.oe_price_adjustments_v adj,
    (SELECT attribute10,
      name,
      list_header_id
    FROM apps.qp_secu_list_headers_vl lh
    WHERE context     =162
    AND automatic_flag=''Y''
    )lh ,
    (SELECT NVL(ffv.description,''MKT'') description,
      flex_value
    FROM apps.fnd_flex_values_vl ffv
    WHERE flex_value_set_id=1015252
    AND enabled_flag       =''Y''
    ) vs
  WHERE adj.header_id         = ol.header_id
  AND adj.line_id             = ol.line_id
  AND adj.list_header_id      = lh.list_header_id
  AND lh.attribute10          = vs.flex_value
  AND adj.applied_flag        =''Y''
  AND adj.list_line_type_code<>''FREIGHT_CHARGE''
  ) price_source_type,
  (SELECT
    CASE
      WHEN last_value(adj.automatic_flag) over (partition BY adj.header_id, adj.line_id order by adj.pricing_group_sequence ,adj.price_adjustment_id ,adj.creation_date ,adj.created_by ,adj.last_update_date ,adj.last_updated_by ,adj.last_update_login ,adj.program_application_id ,adj.program_id ,adj.program_update_date rows BETWEEN unbounded preceding AND unbounded following) = ''N''
      THEN ''MANUAL''
      WHEN lh.attribute10 = ''Contract Pricing''
      OR lh.name LIKE ''CSP%''
      THEN ''CONTRACT''
      WHEN lh.attribute10 = ''Vendor Quote''
      THEN ''VQN''
      ELSE ''SYSTEM''
    END price_type
  FROM apps.oe_price_adjustments_v adj,
    (SELECT attribute10,
      name,
      list_header_id
    FROM apps.qp_secu_list_headers_vl lh
    WHERE context     =162
    AND automatic_flag=''Y''
    )lh ,
    (SELECT NVL(ffv.description,''MKT'') description,
      flex_value
    FROM apps.fnd_flex_values_vl ffv
    WHERE flex_value_set_id=1015252
    AND enabled_flag       =''Y''
    ) vs
  WHERE adj.header_id         = ol.header_id
  AND adj.line_id             = ol.line_id
  AND adj.list_header_id      = lh.list_header_id
  AND lh.attribute10          = vs.flex_value
  AND adj.applied_flag        =''Y''
  AND adj.list_line_type_code<>''FREIGHT_CHARGE''
  AND rownum                  =1
  ) price_type,
  ol.unit_list_price system_price,
  xoal.last_price_paid sxe_last_price_paid,
  ROUND(DECODE(NVL(ol.unit_list_price,0),0,0,((NVL(ol.unit_list_price,0)      - NVL(xoal.last_price_paid,0))/NVL(ol.unit_list_price,0))),4)variance_delivered_price,
  ROUND(DECODE(NVL(ol.unit_selling_price,0),0,0,((NVL(ol.unit_selling_price,0)-NVL(xoal.last_price_paid,0))/NVL(ol.unit_selling_price,0))),4)variance_final_sell_price,
  ((NVL(ol.ordered_quantity,0)                                                * NVL(ol.unit_selling_price,0))-(NVL(ol.ordered_quantity,0)*NVL(xoal.last_price_paid,0))) sales_impact_delivered_price
  --descr#flexfield#start
  --descr#flexfield#end
FROM 
  oe_order_headers oh,
  oe_order_lines ol,
  mtl_parameters ship_from_org ,
  hr_all_organization_units hou,
  oe_transaction_types_vl oth,
  oe_transaction_types_vl otl,
  xxwc.xxwc_oe_ais_lpp_tbl xoal,
  mtl_system_items_kfv msi,
  mtl_item_categories micc,
  mtl_categories_kfv mcvc
WHERE oh.header_id           = ol.header_id
AND ol.ship_from_org_id      = ship_from_org.organization_id(+)
AND ship_from_org.organization_id      = hou.organization_id(+)
AND oh.order_type_id         = oth.transaction_type_id
AND oh.org_id                = oth.org_id
AND ol.line_type_id          = otl.transaction_type_id
AND ol.org_id                = otl.org_id
AND ol.inventory_item_id     = xoal.inventory_item_id(+)
AND ol.ship_to_org_id        = xoal.ship_to_org_id(+)
AND ol.inventory_item_id     = msi.inventory_item_id(+)  
AND ol.ship_from_org_id      = msi.organization_id(+)  
AND msi.inventory_item_id    = micc.inventory_item_id(+)
AND msi.organization_id      = micc.organization_id(+)
AND micc.category_id         = mcvc.category_id(+)
AND mcvc.structure_id(+)     = 101
AND micc.category_set_id(+)  = 1100000062
AND ol.flow_status_code NOT IN (''CLOSED'', ''CANCELLED'')
AND ol.ordered_item NOT     IN (''CONTOFFSET'', ''CONTBILL'')
AND oth.name                != ''INTERNAL ORDER''
AND OTH.name                IN (''REPAIR ORDER'', ''RETURN ORDER'')
AND ol.flow_status_code      = ''INVOICE_HOLD''
AND TRUNC (SYSDATE)          = TRUNC (XXEIS.EIS_XXWC_INV_PRE_REG_RPT_PKG.GET_DATE_FROM)
AND OL.SOURCE_TYPE_CODE     IN (''INTERNAL'', ''EXTERNAL'')
AND NOT EXISTS
  (SELECT  /*+INDEX(gcc GL_CODE_COMBINATIONS_U1)*/
    ''Y''
  FROM gl_code_combinations_kfv gcc
  WHERE gcc.code_combination_id = msi.cost_of_sales_account
  AND GCC.SEGMENT4              = ''646080''
  )
--- UNION 4-3
UNION
SELECT 
ship_from_org.organization_code warehouse,
oth.name order_type,
trunc (oh.ordered_date) ordered_date,
ol.flow_status_code line_flow_status_code,
oh.order_number,
  oh.attribute7 created_by,
  XXEIS.EIS_XXWC_INV_PRE_REG_RPT_PKG.GET_CUSTOMER_NUMBER ( OL.SOLD_TO_ORG_ID, ''ACCOUNTNUM'') CUSTOMER_NUMBER,
  XXEIS.EIS_XXWC_INV_PRE_REG_RPT_PKG.GET_CUSTOMER_NUMBER ( OL.SOLD_TO_ORG_ID, NULL) CUSTOMER_NAME,
  CASE
    WHEN mcvc.segment2 = ''PRMO''
    THEN (                    -1 * ol.unit_selling_price)
    WHEN ol.flow_status_code IN (''CLOSED'', ''INVOICED'', ''INVOICED_PARTIAL'', ''INVOICE_DELIVERY'', ''INVOICE_HOLD'', ''INVOICE_INCOMPLETE'')
    THEN xxeis.eis_xxwc_inv_pre_Reg_rpt_pkg.get_order_line_cost ( OL.LINE_ID)
    WHEN otl.name = ''BILL ONLY''
    THEN 0
    ELSE NVL ( apps.cst_cost_api.get_item_cost (1, msi.inventory_item_id, msi.organization_id), 0)
  END average_cost,
  xxeis.EIS_XXWC_INV_PRE_REG_RPT_PKG.GET_SALESREP_DETAILS(oh.salesrep_id,oh.org_id,''SALESREPNAME'') salesrep_name,
  (NVL ( DECODE ( mcvc.segment2, ''PRMO'', 0, NVL (ol.unit_selling_price, 0) * DECODE (ol.line_category_code, ''RETURN'', (ol.ordered_quantity * -1), ol.ordered_quantity)), 0)) sales,
  (NVL ( DECODE ( mcvc.segment2, ''PRMO'',                                   -1 * ( NVL (ol.unit_selling_price';
dbms_lob.APPEND( dest_lob  => stmt,      src_lob   => l_stmt );
l_stmt :=  ', 0) * DECODE (ol.line_category_code, ''RETURN'', (ol.ordered_quantity * -1), ol.ordered_quantity)), (
  CASE
    WHEN ol.flow_status_code IN (''CLOSED'', ''INVOICED'', ''INVOICED_PARTIAL'', ''INVOICE_DELIVERY'', ''INVOICE_HOLD'', ''INVOICE_INCOMPLETE'')
    THEN xxeis.eis_xxwc_inv_pre_Reg_rpt_pkg.get_order_line_cost ( OL.LINE_ID)
    WHEN otl.name = ''BILL ONLY''
    THEN 0
    ELSE NVL ( apps.cst_cost_api.get_item_cost ( 1, msi.inventory_item_id, msi.organization_id), 0)
  END) * (DECODE (ol.line_category_code, ''RETURN'', (ol.ordered_quantity * -1), ol.ordered_quantity))), 0)) sale_cost,
  msi.segment1 item,
  CASE
    WHEN msi.segment1 = ''Rental Charge''
    THEN ol.user_item_description
    ELSE msi.description
  END item_desc,
  DECODE (ol.line_category_code, ''RETURN'', (ol.ordered_quantity * -1), ol.ordered_quantity) qty,
  DECODE (mcvc.segment2, ''PRMO'', 0, ol.unit_selling_price) unit_selling_price,
  otl.name line_type,
  ship_from_org.attribute8 district,
  hou.name branch_name,
  (SELECT MAX(vs.description) description
  FROM apps.oe_price_adjustments_v adj,
    (SELECT attribute10,
      name,
      list_header_id
    FROM apps.qp_secu_list_headers_vl lh
    WHERE context     =162
    AND automatic_flag=''Y''
    )lh ,
    (SELECT NVL(ffv.description,''MKT'') description,
      flex_value
    FROM apps.fnd_flex_values_vl ffv
    WHERE flex_value_set_id=1015252
    AND enabled_flag       =''Y''
    ) vs
  WHERE adj.header_id         = ol.header_id
  AND adj.line_id             = ol.line_id
  AND adj.list_header_id      = lh.list_header_id
  AND lh.attribute10          = vs.flex_value
  AND adj.applied_flag        =''Y''
  AND adj.list_line_type_code<>''FREIGHT_CHARGE''
  ) price_source_type,
  (SELECT
    CASE
      WHEN last_value(adj.automatic_flag) over (partition BY adj.header_id, adj.line_id order by adj.pricing_group_sequence ,adj.price_adjustment_id ,adj.creation_date ,adj.created_by ,adj.last_update_date ,adj.last_updated_by ,adj.last_update_login ,adj.program_application_id ,adj.program_id ,adj.program_update_date rows BETWEEN unbounded preceding AND unbounded following) = ''N''
      THEN ''MANUAL''
      WHEN lh.attribute10 = ''Contract Pricing''
      OR lh.name LIKE ''CSP%''
      THEN ''CONTRACT''
      WHEN lh.attribute10 = ''Vendor Quote''
      THEN ''VQN''
      ELSE ''SYSTEM''
    END price_type
  FROM apps.oe_price_adjustments_v adj,
    (SELECT attribute10,
      name,
      list_header_id
    FROM apps.qp_secu_list_headers_vl lh
    WHERE context     =162
    AND automatic_flag=''Y''
    )lh ,
    (SELECT NVL(ffv.description,''MKT'') description,
      flex_value
    FROM apps.fnd_flex_values_vl ffv
    WHERE flex_value_set_id=1015252
    AND enabled_flag       =''Y''
    ) vs
  WHERE adj.header_id         = ol.header_id
  AND adj.line_id             = ol.line_id
  AND adj.list_header_id      = lh.list_header_id
  AND lh.attribute10          = vs.flex_value
  AND adj.applied_flag        =''Y''
  AND adj.list_line_type_code<>''FREIGHT_CHARGE''
  AND rownum                  =1
  ) price_type,
  ol.unit_list_price system_price,
  xoal.last_price_paid sxe_last_price_paid,
  ROUND(DECODE(NVL(ol.unit_list_price,0),0,0,((NVL(ol.unit_list_price,0)      - NVL(xoal.last_price_paid,0))/NVL(ol.unit_list_price,0))),4)variance_delivered_price,
  ROUND(DECODE(NVL(ol.unit_selling_price,0),0,0,((NVL(ol.unit_selling_price,0)-NVL(xoal.last_price_paid,0))/NVL(ol.unit_selling_price,0))),4)variance_final_sell_price,
  ((NVL(ol.ordered_quantity,0) * NVL(ol.unit_selling_price,0))-(NVL(ol.ordered_quantity,0)*NVL(xoal.last_price_paid,0))) sales_impact_delivered_price
  --descr#flexfield#start
  --descr#flexfield#end
FROM
  oe_order_headers oh,
  oe_order_lines ol,
  mtl_parameters ship_from_org ,
  hr_all_organization_units hou,
  oe_transaction_types_vl oth,
  oe_transaction_types_vl otl,
  xxwc.xxwc_oe_ais_lpp_tbl xoal,
  mtl_system_items_kfv msi,
  mtl_item_categories micc,
  mtl_categories_kfv mcvc
WHERE  oh.header_id          = ol.header_id
AND ol.ship_from_org_id      = ship_from_org.organization_id(+)
AND ship_from_org.organization_id      = hou.organization_id(+)
AND oh.order_type_id         = oth.transaction_type_id
AND oh.org_id                = oth.org_id
AND ol.line_type_id          = otl.transaction_type_id
AND ol.org_id                = otl.org_id
AND ol.inventory_item_id     = xoal.inventory_item_id(+)
AND ol.ship_to_org_id        = xoal.ship_to_org_id(+)
AND ol.inventory_item_id = msi.inventory_item_id(+) 
AND  ol.ship_from_org_id = msi.organization_id(+)  
AND msi.inventory_item_id    = micc.inventory_item_id(+)
AND msi.organization_id      = micc.organization_id(+)
AND micc.category_id         = mcvc.category_id(+)
AND mcvc.structure_id(+)     = 101
AND micc.category_set_id(+)  = 1100000062
AND ol.flow_status_code NOT IN (''CLOSED'', ''CANCELLED'')
AND ol.ordered_item NOT     IN (''CONTOFFSET'', ''CONTBILL'')
AND oth.name                != ''INTERNAL ORDER''
AND EXISTS
  (SELECT 1
  FROM ra_interface_lines_all ril
  WHERE ril.interface_line_context = ''ORDER ENTRY''
  AND TO_CHAR (ol.line_id)         = ril.interface_line_attribute6
  AND TO_CHAR (OH.ORDER_NUMBER)    = RIL.INTERFACE_LINE_ATTRIBUTE1
  )
AND TRUNC (SYSDATE)      = TRUNC (XXEIS.EIS_XXWC_INV_PRE_REG_RPT_PKG.GET_DATE_FROM)
AND OL.SOURCE_TYPE_CODE IN (''INTERNAL'', ''EXTERNAL'')
AND NOT EXISTS
  (SELECT /*+INDEX(gcc GL_CODE_COMBINATIONS_U1)*/
    ''Y''
  FROM gl_code_combinations_kfv gcc
  WHERE gcc.code_combination_id = msi.cost_of_sales_account
  AND GCC.SEGMENT4              = ''646080''
  )
UNION
-- for rental  orders before invoice
SELECT  /*+ INDEX(ol XXWC_OE_ORDER_LN_AL_N13))*/
ship_from_org.organization_code warehouse,
oth.name order_type,
trunc (oh.ordered_date) ordered_date,
ol.flow_status_code line_flow_status_code,
  oh.order_number,
  oh.attribute7 created_by,
  XXEIS.EIS_XXWC_INV_PRE_REG_RPT_PKG.GET_CUSTOMER_NUMBER ( OL.SOLD_TO_ORG_ID, ''ACCOUNTNUM'') CUSTOMER_NUMBER,
  XXEIS.EIS_XXWC_INV_PRE_REG_RPT_PKG.GET_CUSTOMER_NUMBER ( OL.SOLD_TO_ORG_ID, NULL) CUSTOMER_NAME,
  CASE
    WHEN mcvc.segment2 = ''PRMO''
    THEN (                    -1 * ol.unit_selling_price)
    WHEN ol.flow_status_code IN (''CLOSED'', ''INVOICED'', ''INVOICED_PARTIAL'', ''INVOICE_DELIVERY'', ''INVOICE_HOLD'', ''INVOICE_INCOMPLETE'')
    THEN xxeis.eis_xxwc_inv_pre_Reg_rpt_pkg.get_order_line_cost (OL.LINE_ID)
    WHEN otl.name = ''BILL ONLY''
    THEN 0
    ELSE NVL ( apps.cst_cost_api.get_item_cost (1, msi.inventory_item_id, msi.organization_id), 0)
  END average_cost,
  xxeis.EIS_XXWC_INV_PRE_REG_RPT_PKG.GET_SALESREP_DETAILS(oh.salesrep_id,oh.org_id,''SALESREPNAME'') salesrep_name,
  (NVL ( DECODE ( mcvc.segment2, ''PRMO'', 0, NVL (ol.unit_selling_price, 0) * DECODE (ol.line_category_code, ''RETURN'', (ol.ordered_quantity * -1), ol.ordered_quantity)), 0)) sales,
  (NVL ( DECODE ( mcvc.segment2, ''PRMO'',                                   -1 * ( NVL (ol.unit_selling_price, 0) * DECODE (ol.line_category_code, ''RETURN'', (ol.ordered_quantity * -1), ol.ordered_quantity)), (
  CASE
    WHEN ol.flow_status_code IN (''CLOSED''';
dbms_lob.APPEND( dest_lob  => stmt,      src_lob   => l_stmt );
l_stmt :=  ', ''INVOICED'', ''INVOICED_PARTIAL'', ''INVOICE_DELIVERY'', ''INVOICE_HOLD'', ''INVOICE_INCOMPLETE'')
    THEN xxeis.eis_xxwc_inv_pre_Reg_rpt_pkg.get_order_line_cost ( OL.LINE_ID)
    WHEN otl.name = ''BILL ONLY''
    THEN 0
    ELSE NVL ( apps.cst_cost_api.get_item_cost ( 1, msi.inventory_item_id, msi.organization_id), 0)
  END) * (DECODE (ol.line_category_code, ''RETURN'', (ol.ordered_quantity * -1), ol.ordered_quantity))), 0)) sale_cost,
  msi.segment1 item,
  CASE
    WHEN msi.segment1 = ''Rental Charge''
    THEN ol.user_item_description
    ELSE msi.description
  END item_desc,
  DECODE (ol.line_category_code, ''RETURN'', (ol.ordered_quantity * -1), ol.ordered_quantity) qty,
  DECODE (mcvc.segment2, ''PRMO'', 0, ol.unit_selling_price) unit_selling_price,
  otl.name line_type,
  ship_from_org.attribute8 district,
  hou.name branch_name,
  (SELECT MAX(vs.description) description
  FROM apps.oe_price_adjustments_v adj,
    (SELECT attribute10,
      name,
      list_header_id
    FROM apps.qp_secu_list_headers_vl lh
    WHERE context     =162
    AND automatic_flag=''Y''
    )lh ,
    (SELECT NVL(ffv.description,''MKT'') description,
      flex_value
    FROM apps.fnd_flex_values_vl ffv
    WHERE flex_value_set_id=1015252
    AND enabled_flag       =''Y''
    ) vs
  WHERE adj.header_id         = ol.header_id
  AND adj.line_id             = ol.line_id
  AND adj.list_header_id      = lh.list_header_id
  AND lh.attribute10          = vs.flex_value
  AND adj.applied_flag        =''Y''
  AND adj.list_line_type_code<>''FREIGHT_CHARGE''
  ) price_source_type,
  (SELECT
    CASE
      WHEN last_value(adj.automatic_flag) over (partition BY adj.header_id, adj.line_id order by adj.pricing_group_sequence ,adj.price_adjustment_id ,adj.creation_date ,adj.created_by ,adj.last_update_date ,adj.last_updated_by ,adj.last_update_login ,adj.program_application_id ,adj.program_id ,adj.program_update_date rows BETWEEN unbounded preceding AND unbounded following) = ''N''
      THEN ''MANUAL''
      WHEN lh.attribute10 = ''Contract Pricing''
      OR lh.name LIKE ''CSP%''
      THEN ''CONTRACT''
      WHEN lh.attribute10 = ''Vendor Quote''
      THEN ''VQN''
      ELSE ''SYSTEM''
    END price_type
  FROM apps.oe_price_adjustments_v adj,
    (SELECT attribute10,
      name,
      list_header_id
    FROM apps.qp_secu_list_headers_vl lh
    WHERE context     =162
    AND automatic_flag=''Y''
    )lh ,
    (SELECT NVL(ffv.description,''MKT'') description,
      flex_value
    FROM apps.fnd_flex_values_vl ffv
    WHERE flex_value_set_id=1015252
    AND enabled_flag       =''Y''
    ) vs
  WHERE adj.header_id         = ol.header_id
  AND adj.line_id             = ol.line_id
  AND adj.list_header_id      = lh.list_header_id
  AND lh.attribute10          = vs.flex_value
  AND adj.applied_flag        =''Y''
  AND adj.list_line_type_code<>''FREIGHT_CHARGE''
  AND rownum                  =1
  ) price_type,
  ol.unit_list_price system_price,
  xoal.last_price_paid sxe_last_price_paid,
  ROUND(DECODE(NVL(ol.unit_list_price,0),0,0,((NVL(ol.unit_list_price,0)      - NVL(xoal.last_price_paid,0))/NVL(ol.unit_list_price,0))),4)variance_delivered_price,
  ROUND(DECODE(NVL(ol.unit_selling_price,0),0,0,((NVL(ol.unit_selling_price,0)-NVL(xoal.last_price_paid,0))/NVL(ol.unit_selling_price,0))),4)variance_final_sell_price,
  ((NVL(ol.ordered_quantity,0)                                                * NVL(ol.unit_selling_price,0))-(NVL(ol.ordered_quantity,0)*NVL(xoal.last_price_paid,0))) sales_impact_delivered_price
  --descr#flexfield#start
  --descr#flexfield#end
FROM 
  oe_order_headers oh,
  oe_order_lines ol,
  mtl_parameters ship_from_org ,
  hr_all_organization_units hou,
  oe_transaction_types_vl oth,
  oe_transaction_types_vl otl,
  xxwc.xxwc_oe_ais_lpp_tbl xoal,
  mtl_system_items_kfv msi,
  mtl_item_categories micc,
  mtl_categories_kfv mcvc
WHERE oh.header_id           = ol.header_id
AND ol.ship_from_org_id      = ship_from_org.organization_id(+)
AND ship_from_org.organization_id      = hou.organization_id(+)
AND oh.order_type_id         = oth.transaction_type_id
AND oh.org_id                = oth.org_id
AND ol.line_type_id          = otl.transaction_type_id
AND ol.org_id                = otl.org_id
AND ol.inventory_item_id     = xoal.inventory_item_id(+)
AND ol.ship_to_org_id        = xoal.ship_to_org_id(+)
AND ol.inventory_item_id = msi.inventory_item_id(+) 
AND ol.ship_from_org_id = msi.organization_id(+)   
AND msi.inventory_item_id    = micc.inventory_item_id(+)
AND msi.organization_id      = micc.organization_id(+)
AND micc.category_id         = mcvc.category_id(+)
AND mcvc.structure_id(+)     = 101
AND micc.category_set_id(+)  = 1100000062
AND ol.flow_status_code     <> ''CANCELLED''
AND ol.ordered_item NOT     IN (''CONTOFFSET'', ''CONTBILL'')
AND oth.name                != ''INTERNAL ORDER''
AND ol.line_type_id NOT     IN (1015, 1007)
AND ( ( oth.name            IN (''WC SHORT TERM RENTAL'', ''WC LONG TERM RENTAL'')
AND ol.flow_status_code     IN (''INVOICE_HOLD'', ''CLOSED'')
AND TRUNC (oh.creation_date) = TRUNC (SYSDATE) )
OR ( oth.name               IN (''WC SHORT TERM RENTAL'', ''WC LONG TERM RENTAL'')
AND ol.flow_status_code NOT IN (''CLOSED'', ''CANCELLED'')
AND ol.flow_status_code     IN (''INVOICE_HOLD'')
AND TRUNC (SYSDATE)          = TRUNC (xxeis.EIS_XXWC_INV_PRE_REG_RPT_PKG.get_date_from)))
AND ol.source_type_code     IN (''INTERNAL'', ''EXTERNAL'')
AND NOT EXISTS
  (SELECT /*+INDEX(gcc GL_CODE_COMBINATIONS_U1)*/
    ''Y''
  FROM gl_code_combinations_kfv gcc
  WHERE 1                     = 1
  AND GCC.CODE_COMBINATION_ID = MSI.COST_OF_SALES_ACCOUNT
  AND gcc.segment4            = ''646080''
  )
UNION
SELECT
mp.organization_code warehouse,
oth.name order_type,
trunc (oh.ordered_date) ordered_date,
null line_flow_status_code,
oh.order_number,
  oh.attribute7 created_by,
  XXEIS.EIS_XXWC_INV_PRE_REG_RPT_PKG.GET_CUSTOMER_NUMBER ( rct.bill_to_customer_id, ''ACCOUNTNUM'') CUSTOMER_NUMBER,
  XXEIS.EIS_XXWC_INV_PRE_REG_RPT_PKG.GET_CUSTOMER_NUMBER ( rct.bill_to_customer_id, NULL) CUSTOMER_NAME,
  NVL ( xxeis.eis_rs_xxwc_com_util_pkg.get_item_cost ( msi.inventory_item_id, msi.organization_id), 0) average_cost,
  xxeis.EIS_XXWC_INV_PRE_REG_RPT_PKG.GET_SALESREP_DETAILS(oh.salesrep_id,oh.org_id,''SALESREPNAME'') salesrep_name,
  NVL ( NVL (rctl.quantity_invoiced, rctl.quantity_credited) * NVL (rctl.unit_selling_price, 0), 0) sales,
  0 sale_cost,
  msi.segment1 item,
  msi.description item_desc,
  NVL (rctl.quantity_invoiced, rctl.quantity_credited) qty,
  rctl.unit_selling_price unit_selling_price,
  NULL line_type,
  mp.attribute8 district,
  hou.name branch_name,
  NULL price_source_type,
  NULL price_type,
  to_number(NULL) system_price,
  to_number(NULL) sxe_last_price_paid,
  to_number(NULL) variance_delivered_price,
  to_number(NULL) variance_final_sell_price,
  to_number(NULL) sales_impact_delivered_price
  --descr#flexfield#start
  --descr#flexfield#end
FROM ra_customer_trx rct,
  ra_customer_trx_lines rctl,
  oe_order_headers oh,
  oe_transaction_types_vl oth,
  mtl_parameters mp';
dbms_lob.APPEND( dest_lob  => stmt,      src_lob   => l_stmt );
l_stmt :=  ',
  hr_all_organization_units hou,
  mtl_system_items_kfv msi   
WHERE  rct.customer_trx_id =  rctl.customer_trx_id         
and rctl.interface_line_attribute1 =  TO_CHAR (oh.order_number)  
AND rctl.interface_line_context  = ''ORDER ENTRY''
AND RCTL.CREATION_DATE          >= XXEIS.EIS_XXWC_INV_PRE_REG_RPT_PKG.GET_DATE_FROM + 0.25
AND RCTL.CREATION_DATE          <= XXEIS.EIS_XXWC_INV_PRE_REG_RPT_PKG.GET_DATE_FROM + 1.25
AND oh.order_type_id = oth.transaction_type_id 
AND oh.org_id          = oth.org_id
AND oh.ship_from_org_id = mp.organization_id       
AND mp.organization_id           = hou.organization_id
AND rct.interface_header_context = ''ORDER ENTRY''
AND rctl.interface_line_attribute2= oth.name       
AND rctl.description             = ''Delivery Charge''
AND rctl.line_type               = ''LINE''
AND mp.organization_id           = msi.organization_id
AND RCTL.INVENTORY_ITEM_ID       = MSI.INVENTORY_ITEM_ID
AND NOT EXISTS
  (SELECT 1
  FROM OE_ORDER_LINES_ALL
  WHERE LINE_ID =RCTL.INTERFACE_LINE_ATTRIBUTE6
  )
UNION
SELECT
ship_from_org.organization_code warehouse,
oth.name order_type,
trunc (oh.ordered_date) ordered_date,
ol.flow_status_code line_flow_status_code,
OH.ORDER_NUMBER,
  oh.attribute7 created_by,
  XXEIS.EIS_XXWC_INV_PRE_REG_RPT_PKG.GET_CUSTOMER_NUMBER ( OL.SOLD_TO_ORG_ID, ''ACCOUNTNUM'') CUSTOMER_NUMBER,
  XXEIS.EIS_XXWC_INV_PRE_REG_RPT_PKG.GET_CUSTOMER_NUMBER ( OL.SOLD_TO_ORG_ID, NULL) CUSTOMER_NAME,
  CASE
    WHEN mcvc.segment2 = ''PRMO''
    THEN (                    -1 * ol.unit_selling_price)
    WHEN ol.flow_status_code IN (''CLOSED'', ''INVOICED'', ''INVOICED_PARTIAL'', ''INVOICE_DELIVERY'', ''INVOICE_HOLD'', ''INVOICE_INCOMPLETE'')
    THEN xxeis.eis_xxwc_inv_pre_Reg_rpt_pkg.get_order_line_cost ( OL.LINE_ID)
    WHEN otl.name = ''BILL ONLY''
    THEN 0
    ELSE NVL ( apps.cst_cost_api.get_item_cost (1, msi.inventory_item_id, msi.organization_id), 0)
  END average_cost,
  xxeis.EIS_XXWC_INV_PRE_REG_RPT_PKG.GET_SALESREP_DETAILS(oh.salesrep_id,oh.org_id,''SALESREPNAME'') salesrep_name,
  (NVL ( DECODE ( mcvc.segment2, ''PRMO'', 0, NVL (ol.unit_selling_price, 0) * DECODE (ol.line_category_code, ''RETURN'', (ol.ordered_quantity * -1), ol.ordered_quantity)), 0)) sales,
  (NVL ( DECODE ( mcvc.segment2, ''PRMO'',                                   -1 * ( NVL (ol.unit_selling_price, 0) * DECODE (ol.line_category_code, ''RETURN'', (ol.ordered_quantity * -1), ol.ordered_quantity)), (
  CASE
    WHEN ol.flow_status_code IN (''CLOSED'', ''INVOICED'', ''INVOICED_PARTIAL'', ''INVOICE_DELIVERY'', ''INVOICE_HOLD'', ''INVOICE_INCOMPLETE'')
    THEN xxeis.eis_xxwc_inv_pre_Reg_rpt_pkg.get_order_line_cost ( OL.LINE_ID)
    WHEN otl.name = ''BILL ONLY''
    THEN 0
    ELSE NVL ( apps.cst_cost_api.get_item_cost ( 1, msi.inventory_item_id, msi.organization_id), 0)
  END) * (DECODE (ol.line_category_code, ''RETURN'', (ol.ordered_quantity * -1), ol.ordered_quantity))), 0)) sale_cost,
  msi.segment1 item,
  CASE
    WHEN msi.segment1 = ''Rental Charge''
    THEN ol.user_item_description
    ELSE msi.description
  END item_desc,
  DECODE (ol.line_category_code, ''RETURN'', (ol.ordered_quantity * -1), ol.ordered_quantity) qty,
  DECODE (mcvc.segment2, ''PRMO'', 0, ol.unit_selling_price) unit_selling_price,
  otl.name line_type,
  ship_from_org.attribute8 district,
  hou.name branch_name,
  (SELECT MAX(vs.description) description
  FROM apps.oe_price_adjustments_v adj,
    (SELECT attribute10,
      name,
      list_header_id
    FROM apps.qp_secu_list_headers_vl lh
    WHERE context     =162
    AND automatic_flag=''Y''
    )lh ,
    (SELECT NVL(ffv.description,''MKT'') description,
      flex_value
    FROM apps.fnd_flex_values_vl ffv
    WHERE flex_value_set_id=1015252
    AND enabled_flag       =''Y''
    ) vs
  WHERE adj.header_id         = ol.header_id
  AND adj.line_id             = ol.line_id
  AND adj.list_header_id      = lh.list_header_id
  AND lh.attribute10          = vs.flex_value
  AND adj.applied_flag        =''Y''
  AND adj.list_line_type_code<>''FREIGHT_CHARGE''
  ) price_source_type,
  (SELECT
    CASE
      WHEN last_value(adj.automatic_flag) over (partition BY adj.header_id, adj.line_id order by adj.pricing_group_sequence ,adj.price_adjustment_id ,adj.creation_date ,adj.created_by ,adj.last_update_date ,adj.last_updated_by ,adj.last_update_login ,adj.program_application_id ,adj.program_id ,adj.program_update_date rows BETWEEN unbounded preceding AND unbounded following) = ''N''
      THEN ''MANUAL''
      WHEN lh.attribute10 = ''Contract Pricing''
      OR lh.name LIKE ''CSP%''
      THEN ''CONTRACT''
      WHEN lh.attribute10 = ''Vendor Quote''
      THEN ''VQN''
      ELSE ''SYSTEM''
    END price_type
  FROM apps.oe_price_adjustments_v adj,
    (SELECT attribute10,
      name,
      list_header_id
    FROM apps.qp_secu_list_headers_vl lh
    WHERE context     =162
    AND automatic_flag=''Y''
    )lh ,
    (SELECT NVL(ffv.description,''MKT'') description,
      flex_value
    FROM apps.fnd_flex_values_vl ffv
    WHERE flex_value_set_id=1015252
    AND enabled_flag       =''Y''
    ) vs
  WHERE adj.header_id         = ol.header_id
  AND adj.line_id             = ol.line_id
  AND adj.list_header_id      = lh.list_header_id
  AND lh.attribute10          = vs.flex_value
  AND adj.applied_flag        =''Y''
  AND adj.list_line_type_code<>''FREIGHT_CHARGE''
  AND rownum                  =1
  ) price_type,
  ol.unit_list_price system_price,
  xoal.last_price_paid sxe_last_price_paid,
  ROUND(DECODE(NVL(ol.unit_list_price,0),0,0,((NVL(ol.unit_list_price,0)      - NVL(xoal.last_price_paid,0))/NVL(ol.unit_list_price,0))),4)variance_delivered_price,
  ROUND(DECODE(NVL(ol.unit_selling_price,0),0,0,((NVL(ol.unit_selling_price,0)-NVL(xoal.last_price_paid,0))/NVL(ol.unit_selling_price,0))),4)variance_final_sell_price,
  ((NVL(ol.ordered_quantity,0)                                                * NVL(ol.unit_selling_price,0))-(NVL(ol.ordered_quantity,0)*NVL(xoal.last_price_paid,0))) sales_impact_delivered_price
  --descr#flexfield#start
  --descr#flexfield#end
FROM 
  oe_order_headers oh,
  oe_order_lines ol,
  mtl_parameters ship_from_org,
  hr_all_organization_units hou,
  oe_transaction_types_vl oth,
  oe_transaction_types_vl otl,
  xxwc.xxwc_oe_ais_lpp_tbl xoal,
  mtl_system_items_kfv msi,
  mtl_categories_kfv mcvc,
  mtl_item_categories micc
WHERE oh.header_id           = ol.header_id
AND ol.ship_from_org_id      = ship_from_org.organization_id(+)      
AND ship_from_org.organization_id      = hou.organization_id(+)
AND oh.order_type_id         = oth.transaction_type_id
AND oh.org_id                = oth.org_id
AND ol.line_type_id          = otl.transaction_type_id
AND ol.org_id                = otl.org_id
AND ol.inventory_item_id     = xoal.inventory_item_id(+)
AND ol.ship_to_org_id        = xoal.ship_to_org_id(+)
AND ol.inventory_item_id     = msi.inventory_item_id(+) 
AND msi.organization_id(+)   = ol.ship_from_org_id
AND msi.inventory_item_id    = micc.inventory_item_id(+)
AND msi.organization_id      = micc.organization_id(+)
AND micc.category_id         = mcvc.category_id(+)
AND mcvc.structure_id(+)     = 101
AND micc.category_set_id(+)  = 1100000062
AND ol.ordered_item NOT     IN (''CONTOFFSET''';
dbms_lob.APPEND( dest_lob  => stmt,      src_lob   => l_stmt );
l_stmt :=  ', ''CONTBILL'')
AND ol.flow_status_code NOT IN (''CLOSED'', ''CANCELLED'')
AND oth.name                != ''INTERNAL ORDER''
AND OTH.name                 = ''STANDARD ORDER''
AND OL.USER_ITEM_DESCRIPTION = ''OUT_FOR_DELIVERY''
AND OL.FLOW_STATUS_CODE      = ''AWAITING_SHIPPING''
AND TRUNC(SYSDATE)           = TRUNC(xxeis.EIS_XXWC_INV_PRE_REG_RPT_PKG.get_date_from)
AND OL.SOURCE_TYPE_CODE     IN (''INTERNAL'', ''EXTERNAL'')
AND NOT EXISTS
  (SELECT /*+INDEX(gcc GL_CODE_COMBINATIONS_U1)*/
    ''Y''
  FROM gl_code_combinations_kfv gcc
  WHERE gcc.code_combination_id = msi.cost_of_sales_account
  AND GCC.SEGMENT4              = ''646080''
  )';
dbms_lob.APPEND( dest_lob  => stmt,      src_lob   => l_stmt );
v_objlength := DBMS_LOB.getlength (stmt);
IF v_objlength <= 32000 THEN 
EXECUTE IMMEDIATE to_char(stmt);
ELSE
v_offset := 1; 
WHILE v_offset <= v_objlength
LOOP
-- each record is 256 bytes from the LOB
v_sql_long (v_sql_long_count) := DBMS_LOB.SUBSTR (stmt, 256, v_offset);
-- increment the record count
v_sql_long_count := v_sql_long_count + 1;
-- figure out the new offset
v_offset := v_offset + 256;
END LOOP;
BEGIN
  -- open cursor
 c := DBMS_SQL.open_cursor;
 -- parse VARCHAR2S table
 DBMS_SQL.parse (c, v_sql_long, 1, v_sql_long_count - 1, FALSE, 1);
 -- Execute the cursor
r := DBMS_SQL.EXECUTE (c);
-- Close the cursor
 DBMS_SQL.close_cursor (c);
 END;
END IF;
ELSE
RAISE_APPLICATION_ERROR (-20001,'Module does not exist. Import/Create the module with id 660');
END IF;
END;
/
prompt Creating Object Data EIS_XXWC_OM_AHH_PRICE_VAR_V
set scan off define off
DECLARE
mod_exist VARCHAR2(1);
BEGIN 
mod_exist := XXEIS.EIS_RSC_INSERT.check_if_module_exist(660);
IF mod_exist = 'Y' THEN 
--Inserting Object EIS_XXWC_OM_AHH_PRICE_VAR_V
xxeis.eis_rsc_ins.v( 'EIS_XXWC_OM_AHH_PRICE_VAR_V',660,'','','','','SA059956','XXEIS','Eis Xxwc Om Ahh Price Var V','EXOAPVV','','','VIEW','US','','','');
--Delete Object Columns for EIS_XXWC_OM_AHH_PRICE_VAR_V
xxeis.eis_rsc_utility.delete_view_rows('EIS_XXWC_OM_AHH_PRICE_VAR_V',660,FALSE);
--Inserting Object Columns for EIS_XXWC_OM_AHH_PRICE_VAR_V
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_AHH_PRICE_VAR_V','SALES_IMPACT_DELIVERED_PRICE',660,'Sales Impact Delivered Price','SALES_IMPACT_DELIVERED_PRICE','','','','SA059956','NUMBER','','','Sales Impact Delivered Price','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_AHH_PRICE_VAR_V','UNIT_SELLING_PRICE',660,'Unit Selling Price','UNIT_SELLING_PRICE','','','','SA059956','NUMBER','','','Unit Selling Price','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_AHH_PRICE_VAR_V','LINE_TYPE',660,'Line Type','LINE_TYPE','','','','SA059956','VARCHAR2','','','Line Type','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_AHH_PRICE_VAR_V','DISTRICT',660,'District','DISTRICT','','','','SA059956','VARCHAR2','','','District','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_AHH_PRICE_VAR_V','BRANCH_NAME',660,'Branch Name','BRANCH_NAME','','','','SA059956','VARCHAR2','','','Branch Name','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_AHH_PRICE_VAR_V','PRICE_SOURCE_TYPE',660,'Price Source Type','PRICE_SOURCE_TYPE','','','','SA059956','VARCHAR2','','','Price Source Type','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_AHH_PRICE_VAR_V','PRICE_TYPE',660,'Price Type','PRICE_TYPE','','','','SA059956','VARCHAR2','','','Price Type','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_AHH_PRICE_VAR_V','SYSTEM_PRICE',660,'System Price','SYSTEM_PRICE','','','','SA059956','NUMBER','','','System Price','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_AHH_PRICE_VAR_V','SXE_LAST_PRICE_PAID',660,'Sxe Last Price Paid','SXE_LAST_PRICE_PAID','','','','SA059956','NUMBER','','','Sxe Last Price Paid','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_AHH_PRICE_VAR_V','VARIANCE_DELIVERED_PRICE',660,'Variance Delivered Price','VARIANCE_DELIVERED_PRICE','','','','SA059956','NUMBER','','','Variance Delivered Price','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_AHH_PRICE_VAR_V','VARIANCE_FINAL_SELL_PRICE',660,'Variance Final Sell Price','VARIANCE_FINAL_SELL_PRICE','','','','SA059956','NUMBER','','','Variance Final Sell Price','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_AHH_PRICE_VAR_V','ORDER_NUMBER',660,'Order Number','ORDER_NUMBER','','','','SA059956','NUMBER','','','Order Number','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_AHH_PRICE_VAR_V','CREATED_BY',660,'Created By','CREATED_BY','','','','SA059956','VARCHAR2','','','Created By','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_AHH_PRICE_VAR_V','CUSTOMER_NUMBER',660,'Customer Number','CUSTOMER_NUMBER','','','','SA059956','VARCHAR2','','','Customer Number','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_AHH_PRICE_VAR_V','CUSTOMER_NAME',660,'Customer Name','CUSTOMER_NAME','','','','SA059956','VARCHAR2','','','Customer Name','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_AHH_PRICE_VAR_V','AVERAGE_COST',660,'Average Cost','AVERAGE_COST','','','','SA059956','NUMBER','','','Average Cost','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_AHH_PRICE_VAR_V','SALESREP_NAME',660,'Salesrep Name','SALESREP_NAME','','','','SA059956','VARCHAR2','','','Salesrep Name','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_AHH_PRICE_VAR_V','SALES',660,'Sales','SALES','','','','SA059956','NUMBER','','','Sales','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_AHH_PRICE_VAR_V','SALE_COST',660,'Sale Cost','SALE_COST','','','','SA059956','NUMBER','','','Sale Cost','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_AHH_PRICE_VAR_V','ITEM',660,'Item','ITEM','','','','SA059956','VARCHAR2','','','Item','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_AHH_PRICE_VAR_V','ITEM_DESC',660,'Item Desc','ITEM_DESC','','','','SA059956','VARCHAR2','','','Item Desc','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_AHH_PRICE_VAR_V','QTY',660,'Qty','QTY','','','','SA059956','NUMBER','','','Qty','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_AHH_PRICE_VAR_V','LINE_FLOW_STATUS_CODE',660,'Line Flow Status Code','LINE_FLOW_STATUS_CODE','','','','SA059956','VARCHAR2','','','Line Flow Status Code','','','','','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_AHH_PRICE_VAR_V','ORDERED_DATE',660,'Ordered Date','ORDERED_DATE','','','','SA059956','DATE','','','Ordered Date','','','','','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_AHH_PRICE_VAR_V','ORDER_TYPE',660,'Order Type','ORDER_TYPE','','','','SA059956','VARCHAR2','','','Order Type','','','','','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_AHH_PRICE_VAR_V','WAREHOUSE',660,'Warehouse','WAREHOUSE','','','','SA059956','VARCHAR2','','','Warehouse','','','','','');
--Inserting Object Components for EIS_XXWC_OM_AHH_PRICE_VAR_V
--Inserting Object Component Joins for EIS_XXWC_OM_AHH_PRICE_VAR_V
ELSE
RAISE_APPLICATION_ERROR (-20001,'Module does not exist. Import/Create the module with id 660');
END IF;
END;
/
prompt Creating Report LOV Data for AHH Price Variance Report - WC
set scan off define off
DECLARE
mod_exist VARCHAR2(1);
BEGIN 
mod_exist := XXEIS.EIS_RSC_INSERT.check_if_module_exist(660);
IF mod_exist = 'Y' THEN 
--Inserting Report LOVs - AHH Price Variance Report - WC
xxeis.eis_rsc_ins.lov( 660,'select  RS.Name,SALESREP_ID  from  RA_SALESREPS RS
WHERE  RS.NAME is not null','','OM SALES REP','This gives the sales representative name','XXEIS_RS_ADMIN',NULL,'N','','','','','','','US');
xxeis.eis_rsc_ins.lov( 660,'SELECT ORGANIZATION_CODE WAREHOUSE,ORGANIZATION_NAME FROM ORG_ORGANIZATION_DEFINITIONS OOD WHERE SYSDATE < NVL(OOD.DISABLE_DATE,SYSDATE+1) AND EXISTS (SELECT 1 FROM XXEIS.EIS_ORG_ACCESS_V  WHERE organization_id = ood.organization_id )
UNION
SELECT ''All'', ''All Organizations'' from Dual','','OM Warehouse All','','ANONYMOUS',NULL,'N','','','','','','','US');
xxeis.eis_rsc_ins.lov( 660,'SELECT DISTINCT attribute7 created_by
FROM apps.oe_order_headers_all','','Order Created By Lov','','ANONYMOUS',NULL,'N','','','','','','','US');
null;
ELSE
RAISE_APPLICATION_ERROR (-20001,'Module does not exist. Import/Create the module with id 660');
END IF;
END;
/
prompt Creating Report Data for AHH Price Variance Report - WC
set scan off define off
DECLARE
mod_exist varchar2(1);
BEGIN 
mod_exist := XXEIS.EIS_RSC_INSERT.check_if_module_exist(660);
IF mod_exist = 'Y' THEN 
--Deleting Report data - AHH Price Variance Report - WC
xxeis.eis_rsc_utility.delete_report_rows( 'AHH Price Variance Report - WC',660 );
--Inserting Report - AHH Price Variance Report - WC
xxeis.eis_rsc_ins.r( 660,'AHH Price Variance Report - WC','','AHH Price Variance Report - WC','','','','SA059956','EIS_XXWC_OM_AHH_PRICE_VAR_V','Y','','','SA059956','','N','White Cap Reports','','CSV,EXCEL,','N','','','','','','N','','US','','','','','','','','','','','','','','','','');
--Inserting Report Columns - AHH Price Variance Report - WC
xxeis.eis_rsc_ins.rc( 'AHH Price Variance Report - WC',660,'AVERAGE_COST','Product Unit Cost','Average Cost','','$~T~D~2','default','','20','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_OM_AHH_PRICE_VAR_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'AHH Price Variance Report - WC',660,'BRANCH_NAME','Branch Name','Branch Name','','','default','','2','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_OM_AHH_PRICE_VAR_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'AHH Price Variance Report - WC',660,'CREATED_BY','Created By','Created By','','','default','','9','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_OM_AHH_PRICE_VAR_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'AHH Price Variance Report - WC',660,'CUSTOMER_NAME','Customer Name','Customer Name','','','default','','4','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_OM_AHH_PRICE_VAR_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'AHH Price Variance Report - WC',660,'CUSTOMER_NUMBER','Customer Number','Customer Number','','','default','','3','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_OM_AHH_PRICE_VAR_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'AHH Price Variance Report - WC',660,'DISTRICT','District','District','','','default','','1','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_OM_AHH_PRICE_VAR_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'AHH Price Variance Report - WC',660,'ITEM','SKU Number','Item','','','default','','6','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_OM_AHH_PRICE_VAR_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'AHH Price Variance Report - WC',660,'ITEM_DESC','SKU Description','Item Desc','','','default','','7','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_OM_AHH_PRICE_VAR_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'AHH Price Variance Report - WC',660,'ORDER_NUMBER','Order Number','Order Number','','~~~','default','','5','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_OM_AHH_PRICE_VAR_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'AHH Price Variance Report - WC',660,'PRICE_SOURCE_TYPE','Original Price Source','Price Source Type','','','default','','10','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_OM_AHH_PRICE_VAR_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'AHH Price Variance Report - WC',660,'PRICE_TYPE','Final Price Type','Price Type','','','default','','11','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_OM_AHH_PRICE_VAR_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'AHH Price Variance Report - WC',660,'QTY','Order Qty','Qty','','~~~','default','','13','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_OM_AHH_PRICE_VAR_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'AHH Price Variance Report - WC',660,'SALES','Sales$','Sales','','$~T~D~2','default','','12','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_OM_AHH_PRICE_VAR_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'AHH Price Variance Report - WC',660,'SALES_IMPACT_DELIVERED_PRICE','Sales Impact Final Price','Sales Impact Delivered Price','','$~T~D~2','default','','19','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_OM_AHH_PRICE_VAR_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'AHH Price Variance Report - WC',660,'SALESREP_NAME','Salesrep Name','Salesrep Name','','','default','','8','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_OM_AHH_PRICE_VAR_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'AHH Price Variance Report - WC',660,'SXE_LAST_PRICE_PAID','SXE Last Price Paid','Sxe Last Price Paid','','$~T~D~2','default','','16','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_OM_AHH_PRICE_VAR_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'AHH Price Variance Report - WC',660,'SYSTEM_PRICE','Original Selling Price','System Price','','$~T~D~2','default','','14','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_OM_AHH_PRICE_VAR_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'AHH Price Variance Report - WC',660,'UNIT_SELLING_PRICE','Final Selling price','Unit Selling Price','','$~T~D~2','default','','15','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_OM_AHH_PRICE_VAR_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'AHH Price Variance Report - WC',660,'VARIANCE_DELIVERED_PRICE','Variance Delivered Price','Variance Delivered Price','','~T~D~1','default','','17','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_OM_AHH_PRICE_VAR_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'AHH Price Variance Report - WC',660,'VARIANCE_FINAL_SELL_PRICE','Variance Final Sell Price','Variance Final Sell Price','','~T~D~1','default','','18','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_OM_AHH_PRICE_VAR_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'AHH Price Variance Report - WC',660,'Final_GM%','Final GM%','GM%','NUMBER','~T~D~1','default','','21','Y','Y','','','','','','(case when (NVL(EXOAPVV.sales,0) !=0  AND NVL(EXOAPVV.sale_cost,0) !=0) then  decode(EXOAPVV.line_type,''CREDIT ONLY'',(-1*(((EXOAPVV.sales-EXOAPVV.sale_cost)/(EXOAPVV.sales))*100)),(((EXOAPVV.sales-EXOAPVV.sale_cost)/(EXOAPVV.sales))*100)) WHEN NVL(EXOAPVV.sale_cost,0) =0  THEN decode(EXOAPVV.line_type,''CREDIT ONLY'',(-1*100),100)  WHEN nvl(EXOAPVV.sales,0) != 0  THEN decode(EXOAPVV.line_type,''CREDIT ONLY'',(-1*(((EXOAPVV.sales-EXOAPVV.sale_cost)/(EXOAPVV.sales))*100)),(((EXOAPVV.sales-EXOAPVV.sale_cost)/(EXOAPVV.sales))*100))  else 0 end)','SA059956','N','N','','','','','','US','','');
--Inserting Report Parameters - AHH Price Variance Report - WC
xxeis.eis_rsc_ins.rp( 'AHH Price Variance Report - WC',660,'Created By','Created By','CREATED_BY','IN','Order Created By Lov','','VARCHAR2','N','Y','3','Y','Y','CONSTANT','SA059956','Y','N','','','','EIS_XXWC_OM_AHH_PRICE_VAR_V','','','US','');
xxeis.eis_rsc_ins.rp( 'AHH Price Variance Report - WC',660,'Salesrep Name','Salesrep Name','SALESREP_NAME','IN','OM SALES REP','','VARCHAR2','N','Y','4','Y','Y','CONSTANT','SA059956','Y','N','','','','EIS_XXWC_OM_AHH_PRICE_VAR_V','','','US','');
xxeis.eis_rsc_ins.rp( 'AHH Price Variance Report - WC',660,'As Of Date','As Of Date','','','','','DATE','Y','Y','2','Y','N','CONSTANT','SA059956','Y','N','','Start Date','','EIS_XXWC_OM_AHH_PRICE_VAR_V','','','US','');
xxeis.eis_rsc_ins.rp( 'AHH Price Variance Report - WC',660,'Warehouse','Warehouse','','','OM Warehouse All','''All''','VARCHAR2','Y','Y','1','Y','N','CONSTANT','SA059956','Y','N','','','','EIS_XXWC_OM_AHH_PRICE_VAR_V','','','US','');
--Inserting Dependent Parameters - AHH Price Variance Report - WC
--Inserting Report Conditions - AHH Price Variance Report - WC
xxeis.eis_rsc_ins.rcnh( 'AHH Price Variance Report - WC',660,'FreeText','FREE_TEXT','','','Y','','1');
xxeis.eis_rsc_ins.rcnd( '','','','','','','','','','','','','','','','','Y','Y','','','','AND ( ''All'' IN (:Warehouse) OR (WAREHOUSE IN (:Warehouse)))
AND (EXOAPVV.ORDER_TYPE != ''STANDARD ORDER'' OR  (EXOAPVV.ORDER_TYPE = ''STANDARD ORDER''  
AND EXOAPVV.ORDERED_DATE < =:As Of Date)
 OR (EXOAPVV.ORDER_TYPE = ''STANDARD ORDER''  
 AND EXOAPVV.LINE_FLOW_STATUS_CODE IN (''CLOSED'',''INVOICE_DELIVERY'',''INVOICE_HOLD'',''INVOICE_INCOMPLETE'',''INVOICE_NOT_APPLICABLE'',''INVOICE_RFR'',''INVOICE_UNEXPECTED_ERROR'',''PARTIAL_INVOICE_RFR'')))
and NVL(EXOAPVV.QTY,0)>0
and  NVL(EXOAPVV.SALES,0)<>0','1',660,'AHH Price Variance Report - WC','FreeText');
xxeis.eis_rsc_ins.rcnh( 'AHH Price Variance Report - WC',660,'EXOAPVV.CREATED_BY IN Created By','SIMPLE','','','Y','','2');
xxeis.eis_rsc_ins.rcnd( '','','CREATED_BY','','Created By','','','','','EIS_XXWC_OM_AHH_PRICE_VAR_V','','','','','','IN','Y','Y','','','','','1',660,'AHH Price Variance Report - WC','EXOAPVV.CREATED_BY IN Created By');
xxeis.eis_rsc_ins.rcnh( 'AHH Price Variance Report - WC',660,'EXOAPVV.SALESREP_NAME IN Salesrep Name','SIMPLE','','','Y','','3');
xxeis.eis_rsc_ins.rcnd( '','','SALESREP_NAME','','Salesrep Name','','','','','EIS_XXWC_OM_AHH_PRICE_VAR_V','','','','','','IN','Y','Y','','','','','1',660,'AHH Price Variance Report - WC','EXOAPVV.SALESREP_NAME IN Salesrep Name');
--Inserting Report Sorts - AHH Price Variance Report - WC
xxeis.eis_rsc_ins.rs( 'AHH Price Variance Report - WC',660,'CREATED_BY','ASC','SA059956','1','');
xxeis.eis_rsc_ins.rs( 'AHH Price Variance Report - WC',660,'ORDER_NUMBER','ASC','SA059956','2','');
--Inserting Report Triggers - AHH Price Variance Report - WC
xxeis.eis_rsc_ins.rt( 'AHH Price Variance Report - WC',660,'begin
XXEIS.EIS_XXWC_INV_PRE_REG_RPT_PKG.set_date_from(:As Of Date);
end;','B','Y','SA059956','BQ');
--inserting report templates - AHH Price Variance Report - WC
--Inserting Report Portals - AHH Price Variance Report - WC
--inserting report dashboards - AHH Price Variance Report - WC
--Exporting report views table information -  
xxeis.eis_rsc_ins.rviews( 'AHH Price Variance Report - WC','660','EIS_XXWC_OM_AHH_PRICE_VAR_V','EIS_XXWC_OM_AHH_PRICE_VAR_V','N','');
--inserting report security - AHH Price Variance Report - WC
xxeis.eis_rsc_ins.rsec( 'AHH Price Variance Report - WC','660','','XXWC_AO_OEENTRY_REC',660,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'AHH Price Variance Report - WC','660','','XXWC_AO_OEENTRY_PO_RPT',660,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'AHH Price Variance Report - WC','660','','XXWC_AO_OEENTRY',660,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'AHH Price Variance Report - WC','660','','XXWC_ORDER_MGMT_READ_SHIPPING',660,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'AHH Price Variance Report - WC','660','','XXWC_ORDER_MGMT_READ_ONLY',660,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'AHH Price Variance Report - WC','660','','XXWC_ORDER_MGMT_PRICING_SUPER',660,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'AHH Price Variance Report - WC','660','','XXWC_ORDER_MGMT_PRICING_STD',660,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'AHH Price Variance Report - WC','660','','XXWC_ORDER_MGMT_PRICING_LTD',660,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'AHH Price Variance Report - WC','660','','XXWC_ORDER_MGMT_PRICING_FULL',660,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'AHH Price Variance Report - WC','660','','ORDER_MGMT_SUPER_USER',660,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'AHH Price Variance Report - WC','20005','','XXWC_VIEW_ALL_EIS_REPORTS',660,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'AHH Price Variance Report - WC','20005','','XXWC_SALES_SUPPORT_ADMIN',660,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'AHH Price Variance Report - WC','660','','XXWC_RENTAL_OM_PRICING_SUPER',660,'SA059956','','','');
--Inserting Report Pivots - AHH Price Variance Report - WC
--Inserting Report Distribution Details 
--Inserting Report Distribution Details 
--Inserting Report SubTotals Details 
--Inserting Report aggregations 
--Inserting Report   Version details- AHH Price Variance Report - WC
xxeis.eis_rsc_ins.rv( 'AHH Price Variance Report - WC','','AHH Price Variance Report - WC','SA059956','03-OCT-2018');
ELSE
RAISE_APPLICATION_ERROR (-20001,'Module does not exist. Import/Create the module with id 660');
END IF;
END;
/
