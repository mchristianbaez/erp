CREATE OR REPLACE PACKAGE BODY APPS.xxwc_gen_routines_pkg
AS
   -- This package will contain all generic routines which are used by all Workstreams
   /*
   **  Writes the message to the log table for the spec'd level and module
   **  if logging is enabled for this level and module
   ** Examples for Module Name If following procedure is called from OM PLSQL Package
   **  'ONT.PLSQL.PACKAGE_NAME.PROCEDURE'
   **  Example for module name where procedure is getting called from Forms
   **   'ONT.FORMS.FORNNAME.PACKAGENAME.PROCEDURENAME' "
   **  Example for mdoule name where procedure is getting called from library
   **   'INV.PLL,CUSTOMPLLNAME.PACKAGE.PROCEDURE'
   **   REVISIONS:
   **   Ver        Date            Author            Description
   **   ---------  ----------      ---------------   ------------------------------------
   **   1.0        31-10-2014      Pattabhi Avula     TMS 20141002-00064 20141002-00064  Canada changes
   **   1.1        26-11-2015      Pattabhi Avula     TMS 20150729-00197 Ship to, bill to contact number changes
   **   1.2        16-12-2015      Pattabhi Avula     TMS 20151215-00225 - XXWC_Packing slip report - printing out without Job Site Contact name
   **   1.3        22-02-2016      Kishore            TMS 20160219-00134 - Ship to contact on the Pick and Pack slip are not displayed
   **   1.4        20-06-2016      Pattabhi Avula     TMS 20160620-00131 - Printed By name change in CFD Reports
   **   1.5        08-07-2016      P.Vamshidhar       TMS 20160708-00119 - Needs to Correct or Sync Printed by and Created by Fields in Sales Receipt.
   */

   l_package_name   VARCHAR2 (100) := 'XXWC_GEN_ROUTINES_PKG';
   l_distro_list    VARCHAR2 (100) DEFAULT 'HDSOracleDevelopers@hdsupply.com';

   PROCEDURE log_msg (p_debug_level   IN NUMBER,
                      p_mod_name      IN VARCHAR2,
                      p_debug_msg     IN VARCHAR2)
   IS
      PRAGMA AUTONOMOUS_TRANSACTION;
   BEGIN
      IF (p_debug_level >= c_current_runtime_level)
      THEN
         fnd_log.STRING (p_debug_level, p_mod_name, p_debug_msg);
      END IF;

      COMMIT;
   END log_msg;

   FUNCTION get_profile_value (p_profile_option_name IN VARCHAR2)
      RETURN VARCHAR2
   IS
      l_profile_option_value   VARCHAR2 (50);
   BEGIN
      SELECT fpov.profile_option_value
        INTO l_profile_option_value
        FROM fnd_user fu,
             fnd_profile_options fpo,
             fnd_profile_option_values fpov,
             fnd_profile_options_tl fpot
       WHERE     fu.user_id = fpov.level_value
             AND fpo.profile_option_id = fpov.profile_option_id
             AND fpo.profile_option_name = fpot.profile_option_name
             AND fpot.LANGUAGE = 'US'
             AND fpov.level_id = c_site_level
             AND fu.user_name = fnd_profile.VALUE ('USERNAME')
             AND UPPER (fpo.profile_option_name) =
                    UPPER (p_profile_option_name);

      RETURN l_profile_option_value;
   END get_profile_value;

   -- Satish U: Nov 29 2011
   /*
   **   Ver        Date            Author            Description
   **   ---------  ----------      ---------------   ------------------------------------
   **   1.4        20-06-2016      Pattabhi Avula     TMS 20160620-00131 - Printed By name change in CFD Reports
   **   1.5        08-07-2016      P.Vamshidhar       TMS 20160708-00119 - Needs to Correct or Sync Printed by and Created by Fields in Sales Receipt.
   */
   FUNCTION takenby (p_user_id NUMBER, p_name_type VARCHAR2 DEFAULT 'LONG')
      RETURN VARCHAR2
   AS
      CURSOR c_takenby
      IS
         SELECT    papf.first_name
                || (CASE
                       WHEN SUBSTR (papf.last_name, 1, 1) IS NOT NULL
                       THEN
                          ' '
                       ELSE
                          NULL
                    END)
                || papf.last_name
                   full_name,
                   papf.last_name
                || (CASE
                       WHEN papf.last_name IS NOT NULL THEN ', '
                       ELSE NULL
                    END)
                || SUBSTR (papf.first_name, 1, 1)
                   short_name,
                fu.user_name user_name,
                --papf.first_name || (case when papf.last_name is not null then '' else null end) ||' '|| SUBSTR (papf.last_name, 1, 1) with_intial  -- Ver# 1.4 commented in 1.5
                papf.first_name || ' ' || SUBSTR (papf.last_name, 1, 1)
                   with_intial                                     -- Ver# 1.5
           FROM per_all_people_f papf, fnd_user fu
          WHERE papf.person_id(+) = fu.employee_id AND fu.user_id = p_user_id;
   BEGIN
      FOR r_takenby IN c_takenby
      LOOP
         IF UPPER (p_name_type) = 'SHORT'
         THEN
            IF r_takenby.short_name IS NULL
            THEN
               RETURN r_takenby.user_name;
            ELSE
               RETURN r_takenby.short_name;
            END IF;
         ELSIF UPPER (p_name_type) = 'LONG'
         THEN
            IF r_takenby.full_name IS NULL
            THEN
               RETURN r_takenby.user_name;
            ELSE
               RETURN r_takenby.full_name;
            END IF;
         -- <START>  Ver#1.4
         ELSIF UPPER (p_name_type) = 'LAST_INITIAL'
         THEN
            IF r_takenby.with_intial IS NULL
            THEN
               RETURN r_takenby.user_name;
            ELSE
               RETURN r_takenby.with_intial; 
            END IF;
         -- <END>  Ver#1.4
         ELSE
            RETURN r_takenby.user_name;
         END IF;
      END LOOP;
   EXCEPTION
      WHEN NO_DATA_FOUND
      THEN
         RETURN NULL;
      WHEN OTHERS
      THEN
         RETURN (SQLERRM || '-' || SQLCODE);
   END takenby;

   -- This function is used to display the Salesrep Name based on Salesrep ID and Name Type
   /*
        P_NAME_TYPE    := SHORT   ==> It return Last Name with Initial
        P_NAME_TYPE    := LONG    ==> It return First Name and Last Name
   */
   FUNCTION salesman (p_salesrep_id    NUMBER,
                      p_name_type      VARCHAR2 DEFAULT 'LONG')
      RETURN VARCHAR2
   AS
      /***************************************************************************************************************************************
      Purpose: This function is used in following reports
      1.Pickticket,
      2.Rental return worksheet,
      3.Quote Detail Report
      4. Sales Receipt Report
      5. Repair quote
      6. Rental receipt


      *************************************************************************************************************************************** */
      CURSOR c_salesman
      IS
         SELECT papf.first_name || ' ' || papf.last_name full_name,
                papf.last_name || ', ' || SUBSTR (papf.first_name, 1, 1)
                   short_name,
                jrs.NAME salesman,
                jrs.person_id
           FROM per_all_people_f papf, jtf_rs_salesreps_mo_v jrs --jtf_rs_salesreps jrs
          WHERE     papf.person_id(+) = jrs.person_id
                AND jrs.salesrep_id = p_salesrep_id;               --100000040
   BEGIN
      FOR r_salesman IN c_salesman
      LOOP
         IF r_salesman.person_id IS NOT NULL
         THEN
            IF UPPER (p_name_type) = 'SHORT'
            THEN
               RETURN r_salesman.short_name;
            ELSIF UPPER (p_name_type) = 'LONG'
            THEN
               RETURN r_salesman.full_name;
            END IF;
         ELSIF r_salesman.person_id IS NULL
         THEN
            IF UPPER (p_name_type) = 'SHORT'
            THEN
               RETURN r_salesman.salesman;
            ELSIF UPPER (p_name_type) = 'LONG'
            THEN
               RETURN r_salesman.salesman;
            END IF;
         END IF;
      END LOOP;
   EXCEPTION
      WHEN NO_DATA_FOUND
      THEN
         RETURN NULL;
      WHEN OTHERS
      THEN
         RETURN (SQLERRM || '-' || SQLCODE);
   END salesman;

   -- To display the ON ACCOUNT OR CASH C.O.D based on Customer Payment Terms
   FUNCTION payment_term (p_term_id IN NUMBER)
      RETURN VARCHAR2
   AS
      /***************************************************************************************************************************************
      Purpose: This function is used in following reports
      1.Pickticket,
      2.Rental return worksheet,
      3.Quote Detail Report
      4. Sales Receipt Report
      5. Repair quote
      6. Rental receipt

      *************************************************************************************************************************************** */
      CURSOR c_acctterm
      IS
         SELECT NAME, term_id
           FROM ra_terms_tl
          WHERE term_id = p_term_id;
   BEGIN
      FOR r_acctterm IN c_acctterm
      LOOP
         IF r_acctterm.NAME IN ('COD', 'PRFRDCASH')
         THEN
            RETURN 'CASH C.O.D';
         ELSE
            RETURN 'ON ACCOUNT';
         END IF;
      END LOOP;
   EXCEPTION
      WHEN NO_DATA_FOUND
      THEN
         RETURN NULL;
      WHEN OTHERS
      THEN
         RETURN (SQLERRM || '-' || SQLCODE);
   END payment_term;

   -- To display the White CAP Company Address and Phone numbers
   PROCEDURE branch_address (p_ship_from_org_id   IN     NUMBER,
                             p_branch                OUT VARCHAR2,
                             p_street                OUT VARCHAR2,
                             p_city                  OUT VARCHAR2,
                             p_phone                 OUT VARCHAR2,
                             p_fax                   OUT VARCHAR2,
                             p_ret_code              OUT NUMBER,
                             p_ret_msg               OUT VARCHAR2)
   AS
      /***************************************************************************************************************************************
      Purpose: This function is used in following reports
      1. Pickticket,
      2. Rental return worksheet,
      3. Quote Detail Report
      4. Sales Receipt Report
      5. Repair quote
      6. Rental receipt

      07/26/2012 CGonzalez: Modified per issue #759 to accomodate for new inventory org location.
                            we only want to pull the organization primary location
      *************************************************************************************************************************************** */
      CURSOR c_branch_info (
         p_ship_org    NUMBER)
      IS
         SELECT hl.location_code branch,
                hl.address_line_1 branch_street,
                   DECODE (hl.town_or_city,
                           NULL, NULL,
                           hl.town_or_city || ', ')
                || DECODE (hl.region_2,
                           NULL, hl.region_2 || ', ',
                           hl.region_2 || ', ')
                || DECODE (hl.postal_code, NULL, NULL, hl.postal_code)
                   branch_city,
                hl.telephone_number_1 branch_tel1,
                hl.telephone_number_2 branch_tel2
           FROM hr_all_organization_units hou, hr_locations_all hl
          --WHERE hl.inventory_organization_id = p_ship_org;
          WHERE     hou.organization_id = p_ship_org
                AND hou.location_id = hl.location_id;

      l_branch   VARCHAR2 (5000);
      l_street   VARCHAR2 (5000);
      l_city     VARCHAR2 (2000);
      l_phone    VARCHAR2 (500);
      l_fax      VARCHAR2 (500);
   BEGIN
      OPEN c_branch_info (p_ship_from_org_id);

      FETCH c_branch_info
         INTO l_branch,
              l_street,
              l_city,
              l_phone,
              l_fax;

      IF c_branch_info%NOTFOUND
      THEN
         l_branch := '';
         l_street := '';
         l_city := '';
         l_phone := '';
         l_fax := '';
      END IF;

      CLOSE c_branch_info;

      p_branch := l_branch;
      p_street := l_street;
      p_city := l_city;
      p_phone := l_phone;
      p_fax := l_fax;
      p_ret_code := 0;
      p_ret_msg := 'Success';
   EXCEPTION
      WHEN OTHERS
      THEN
         p_ret_code := SQLCODE;
         p_ret_msg := SUBSTR (SQLERRM, 0, 2000);
   --return '';
   --         p_branch := '';
   --         p_street := '';
   --         p_city := '';
   --         p_phone := '';
   --         p_fax := '';
   END branch_address;

   -- To display the White CAP Site Address and Phone numbers
   PROCEDURE site_address (p_site_use_code   IN     VARCHAR2,
                           p_site_use_id     IN     NUMBER,
                           p_addr1              OUT VARCHAR2,
                           p_loc                OUT VARCHAR2,
                           p_addr2              OUT VARCHAR2,
                           p_addr3              OUT VARCHAR2,
                           p_addr5              OUT VARCHAR2,
                           p_code               OUT VARCHAR2,
                           p_phone              OUT VARCHAR2,
                           p_ret_code           OUT NUMBER,
                           p_ret_msg            OUT VARCHAR2,
                           p_site_number        OUT VARCHAR2)
   AS
      /***************************************************************************************************************************************
      Purpose: This function is used in following reports
      1.Pickticket,
      2.Rental return worksheet,
      3.Quote Detail Report
      4. Sales Receipt Report
      5. Repair quote
      6. Rental receipt

      *************************************************************************************************************************************** */
      CURSOR c_site_info (
         p_site_code    VARCHAR2,
         p_site_id      NUMBER)
      IS
         SELECT hl.address1,
                hcsu.LOCATION,
                hl.address2,
                hl.address3,
                   DECODE (hl.city, NULL, NULL, hl.city || ', ')
                || DECODE (hl.state,
                           NULL, hl.province || ', ',
                           hl.state || ', ')
                || DECODE (hl.postal_code, NULL, NULL, hl.postal_code)
                   add5,
                hcp.phone_area_code,
                hcp.phone_number,
                hps.party_site_number
           FROM apps.hz_cust_site_uses hcsu,
                apps.hz_cust_acct_sites hcas,
                hz_party_sites hps,
                hz_locations hl,
                hz_contact_points hcp
          WHERE     1 = 1
                AND hcsu.cust_acct_site_id = hcas.cust_acct_site_id
                AND hcas.party_site_id = hps.party_site_id
                AND hcp.owner_table_id(+) = hps.party_site_id
                AND hps.location_id = hl.location_id(+)
                AND hcp.owner_table_name(+) = 'HZ_PARTY_SITES'
                AND hcp.primary_flag(+) = 'Y'
                AND site_use_code = p_site_code
                AND site_use_code = 'SHIP_TO'
                AND hcsu.site_use_id = p_site_id
         -- Version# 1.1 > Start
         UNION
         SELECT hl.address1,
                hcsu.LOCATION,
                hl.address2,
                hl.address3,
                   DECODE (hl.city, NULL, NULL, hl.city || ', ')
                || DECODE (hl.state,
                           NULL, hl.province || ', ',
                           hl.state || ', ')
                || DECODE (hl.postal_code, NULL, NULL, hl.postal_code)
                   add5,
                hcp.phone_area_code,
                hcp.phone_number,
                hps.party_site_number
           FROM apps.hz_cust_site_uses hcsu,
                apps.hz_cust_acct_sites hcas,
                hz_party_sites hps,
                hz_locations hl,
                hz_contact_points hcp
          WHERE     1 = 1
                AND hcsu.cust_acct_site_id = hcas.cust_acct_site_id
                AND hcas.party_site_id = hps.party_site_id
                AND hcp.owner_table_id(+) = hps.party_site_id
                AND hps.location_id = hl.location_id(+)
                AND hcp.owner_table_name(+) = 'HZ_PARTY_SITES'
                AND hcp.primary_flag(+) = 'Y'
                AND hcsu.site_use_code = 'BILL_TO'
                AND hcsu.primary_flag = 'Y'
                AND hcas.cust_account_id IN (SELECT hcas2.cust_account_id
                                               FROM apps.hz_cust_acct_sites hcas2,
                                                    apps.hz_cust_site_uses hcsu2
                                              WHERE     1 = 1
                                                    AND hcsu2.cust_acct_site_id =
                                                           hcas2.cust_acct_site_id
                                                    AND hcsu2.site_use_id =
                                                           p_site_id
                                                    AND hcsu2.site_use_code =
                                                           p_site_code
                                                    AND hcsu2.site_use_code =
                                                           'BILL_TO');

      -- Version# 1.1 < End

      l_addr1         VARCHAR2 (5000);
      l_loc           VARCHAR2 (2000);
      l_addr2         VARCHAR2 (5000);
      l_addr3         VARCHAR2 (5000);
      l_addr5         VARCHAR2 (5000);
      l_code          VARCHAR2 (5000);
      l_phone         VARCHAR2 (5000);
      l_site_number   VARCHAR2 (500);
      l_location      VARCHAR2 (5000);
   BEGIN
      OPEN c_site_info (p_site_use_code, p_site_use_id);

      FETCH c_site_info
         INTO l_addr1,
              l_loc,
              l_addr2,
              l_addr3,
              l_addr5,
              l_code,
              l_phone,
              l_site_number;

      IF l_phone IS NOT NULL
      THEN
         IF INSTR (l_phone, '-', 4) = 0
         THEN
            l_phone :=
               (   l_code
                || '-'
                || SUBSTR (l_phone, 1, 3)
                || '-'
                || SUBSTR (l_phone, 4, 7));
         ELSE
            l_phone := (l_code || '-' || l_phone);
         END IF;
      ELSE
         l_phone := '';
      END IF;

      IF c_site_info%NOTFOUND
      THEN
         l_addr1 := '';
         l_loc := '';
         l_addr2 := '';
         l_addr3 := '';
         l_addr5 := '';
         l_code := '';
         l_phone := '';
         l_site_number := '';
      END IF;

      CLOSE c_site_info;

      p_addr1 := l_addr1;
      p_loc := l_loc;
      p_addr2 := l_addr2;
      p_addr3 := l_addr3;
      p_addr5 := l_addr5;
      p_code := l_code;
      p_phone := l_phone;
      p_site_number := l_site_number;
      p_ret_code := 0;
      p_ret_msg := 'Success';
   EXCEPTION
      WHEN OTHERS
      THEN
         p_ret_code := SQLCODE;
         p_ret_msg := SUBSTR (SQLERRM, 0, 2000);
         --return '';
         --         p_branch := '';
         --         p_street := '';
         --         p_city := '';
         --         p_phone := '';
         --         p_fax := '';

         -- Calling ERROR API
         xxcus_error_pkg.xxcus_error_main_api (
            p_called_from         => l_package_name,
            p_calling             => 'Derive Site Address - WHEN OTHERS Exception',
            p_request_id          => fnd_global.conc_request_id,
            p_ora_error_msg       => SQLERRM,
            p_error_desc          => 'Derive Site Address - WHEN OTHERS Exception',
            p_distribution_list   => l_distro_list,
            p_module              => 'OM');
   END site_address;

   --Kiran T Dec 7 2011
   /***************************************************************************************************************************************
  Purpose: This function is used in following reports
  1.Rental Pickticket
  *************************************************************************************************************************************** */
   FUNCTION unit_list_price_calc (p_line_id              NUMBER,
                                  p_inventory_item_id    NUMBER)
      RETURN NUMBER
   IS
      CURSOR c_unit_list_price (
         cp_line_id              NUMBER,
         cp_inventory_item_id    NUMBER)
      IS
         SELECT SUM (c.unit_list_price)
           FROM apps.oe_order_lines c
          WHERE     c.link_to_line_id IN (SELECT b.line_id
                                            FROM apps.oe_order_lines b
                                           WHERE b.link_to_line_id IN (SELECT a.line_id
                                                                         FROM apps.oe_order_lines a
                                                                        WHERE a.line_id =
                                                                                 cp_line_id))
                AND inventory_item_id =
                       (SELECT DISTINCT related_item_id
                          FROM mtl_related_items
                         WHERE     inventory_item_id = cp_inventory_item_id
                               AND attr_char1 = 'Rental');

      l_line_number       NUMBER := 0;
      l_line_id           NUMBER := 0;
      l_unit_list_price   NUMBER := 0;
   BEGIN
      OPEN c_unit_list_price (p_line_id, p_inventory_item_id);

      FETCH c_unit_list_price INTO l_unit_list_price;

      CLOSE c_unit_list_price;

      RETURN l_unit_list_price;
   EXCEPTION
      WHEN OTHERS
      THEN
         RETURN 0;
   END unit_list_price_calc;

   --Kiran T Dec 7 2011
   /***************************************************************************************************************************************
  Purpose: This function is used in following reports
  1.Rental Pickticket
  *************************************************************************************************************************************** */
   FUNCTION tax_calc (p_header_id NUMBER)
      RETURN NUMBER
   IS
      CURSOR c_tax (cp_header_id NUMBER)
      IS
         SELECT SUM (NVL (adjusted_amount, 0))
           FROM oe_price_adjustments
          WHERE header_id = cp_header_id                                    --
                                        AND list_line_type_code = 'TAX';

      l_tax_cost   NUMBER := 0;
   BEGIN
      OPEN c_tax (p_header_id);

      FETCH c_tax INTO l_tax_cost;

      CLOSE c_tax;

      RETURN l_tax_cost;
   EXCEPTION
      WHEN OTHERS
      THEN
         RETURN 0;
   END tax_calc;

   -- This function is used to display the Resource Name based on Resource ID and Name Type
   /*
        P_NAME_TYPE    := SHORT   ==> It return Last Name with Initial
        P_NAME_TYPE    := LONG    ==> It return First Name and Last Name
   */
   FUNCTION technician (p_resource_id    NUMBER,
                        p_name_type      VARCHAR2 DEFAULT 'LONG')
      RETURN VARCHAR2
   AS
      /***************************************************************************************************************************************
      Purpose: This function is used in following reports
      1. Repair quote Report
      *************************************************************************************************************************************** */
      CURSOR c_resource
      IS
         SELECT    source_first_name
                || ''
                || source_middle_name
                || ''
                || source_last_name
                   full_name,
                source_last_name || ',' || SUBSTR (source_first_name, 1, 1)
                   short_name
           FROM jtf_rs_resource_extns
          WHERE resource_id(+) = p_resource_id;
   BEGIN
      FOR r_resource IN c_resource
      LOOP
         IF UPPER (p_name_type) = 'SHORT'
         THEN
            IF r_resource.short_name IS NULL
            THEN
               RETURN r_resource.full_name;
            ELSE
               RETURN r_resource.short_name;
            END IF;
         ELSIF UPPER (p_name_type) = 'LONG'
         THEN
            IF r_resource.full_name IS NULL
            THEN
               RETURN r_resource.short_name;
            ELSE
               RETURN r_resource.full_name;
            END IF;
         ELSE
            RETURN r_resource.short_name;
         END IF;
      END LOOP;
   EXCEPTION
      WHEN NO_DATA_FOUND
      THEN
         RETURN NULL;
      WHEN OTHERS
      THEN
         RETURN (SQLERRM || '-' || SQLCODE);
   END technician;

   /****************************************************************
   --Function Get_TOtal_Days_ONRent
   --Returns number of days on Rent
   -- Parameters Line_ID  ( Sales Order Line ID
   -- Description : This Function is used in Re-Rent Invoice Report
   -- History :  12-13-2011 Satish Upadhyayula
   *****************************************************************/
   FUNCTION get_total_days_onrent (p_so_line_id NUMBER)
      RETURN NUMBER
   IS
      l_total_days_onrent      NUMBER;
      l_actual_shipment_date   DATE;
      l_rental_return_date     DATE;
   BEGIN
      SELECT actual_shipment_date
        INTO l_actual_shipment_date
        FROM apps.oe_order_lines
       WHERE line_id = p_so_line_id;

      l_rental_return_date := l_actual_shipment_date;

      WHILE l_rental_return_date < SYSDATE
      LOOP
         l_rental_return_date := l_rental_return_date + 28;
      END LOOP;

      l_total_days_onrent := l_rental_return_date - l_actual_shipment_date;
      RETURN l_total_days_onrent;
   EXCEPTION
      WHEN OTHERS
      THEN
         RETURN NULL;
   END get_total_days_onrent;

   /****************************************************************
   --- Function Get_Qty_OnRent
   -- Returns Total Qty on Rent for a given Sales Order Line ID
   -- Description : THis function is used in Re-Rent Invoice Report
   -- History : 12-13-2011 : Satish Upadhyayula
   *****************************************************************/
   FUNCTION get_qty_returned (p_so_line_id NUMBER)
      RETURN NUMBER
   IS
      l_qty_returned   NUMBER;
   BEGIN
      SELECT SUM (ordered_quantity - cancelled_quantity)
        INTO l_qty_returned
        FROM apps.oe_order_lines
       WHERE     line_category_code = 'RETURN'
             AND flow_status_code = 'CLOSED'
             AND link_to_line_id = p_so_line_id;

      RETURN l_qty_returned;
   EXCEPTION
      WHEN OTHERS
      THEN
         RETURN 0;
   END get_qty_returned;

   /****************************************************************
   --- Function Get_Item_Cost
   -- Returns Item Cost from a PO Line  for a given Sales Order Line ID
   -- Description : THis function is used in Re-Rent Invoice Report
   -- History : 12-13-2011 : Satish Upadhyayula
   *****************************************************************/
   FUNCTION get_item_cost (p_so_line_id NUMBER)
      RETURN NUMBER
   IS
      l_item_cost   NUMBER;
   BEGIN
      SELECT unit_price
        INTO l_item_cost
        FROM apps.po_lines pol, apps.oe_order_lines ool, apps.po_headers poh
       WHERE     ool.line_id = p_so_line_id
             AND ool.attribute10 = poh.segment1
             AND pol.po_header_id = poh.po_header_id
             AND ool.inventory_item_id = pol.item_id
             AND ROWNUM = 1;

      RETURN l_item_cost;
   EXCEPTION
      WHEN OTHERS
      THEN
         RETURN 0;
   END get_item_cost;

   /****************************************************************
   --- Function Get_Item_UOM
   -- Returns Item UOM from a PO Line  for a given Sales Order Line ID
   -- Description : THis function is used in Re-Rent Invoice Report
   -- History : 12-13-2011 : Satish Upadhyayula
   *****************************************************************/
   FUNCTION get_item_uom (p_so_line_id NUMBER)
      RETURN VARCHAR2
   IS
   BEGIN
      RETURN ('28 Days');
   END get_item_uom;

   /****************************************************************
   --- Function Curr_Period_End_Date
   -- Returns Current Period End Date for a given Sales Order Line ID
   -- Description : THis function is used in Re-Rent Invoice Report
   -- History : 12-13-2011 : Satish Upadhyayula
   *****************************************************************/
   FUNCTION curr_period_end_date (p_so_line_id NUMBER)
      RETURN DATE
   IS
      l_period_end_date        DATE;
      l_actual_shipment_date   DATE;
   BEGIN
      SELECT actual_shipment_date
        INTO l_actual_shipment_date
        FROM apps.oe_order_lines
       WHERE line_id = p_so_line_id;

      l_period_end_date := l_actual_shipment_date;

      WHILE l_period_end_date < SYSDATE
      LOOP
         l_period_end_date := l_period_end_date + 28;
      END LOOP;

      RETURN l_period_end_date;
   EXCEPTION
      WHEN OTHERS
      THEN
         RETURN NULL;
   END curr_period_end_date;

   /****************************************************************
     --- Function Get_Trx_Type_ID
     -- Returns Transaction Type ID for given Transaction Name
     -- Description : THis function is used in Rental Mass Additions (1503)
     -- History : 01-19-2012 : Srinivas Malkapuram
     *****************************************************************/
   FUNCTION get_trx_type_id (p_trx_type_name VARCHAR2)
      RETURN NUMBER
   IS
      l_trx_type_id   NUMBER;
   BEGIN
      SELECT transaction_type_id
        INTO l_trx_type_id
        FROM mtl_transaction_types
       WHERE     UPPER (transaction_type_name) = UPPER (p_trx_type_name)
             AND ROWNUM = 1;

      RETURN l_trx_type_id;
   EXCEPTION
      WHEN OTHERS
      THEN
         RETURN 0;
   END get_trx_type_id;

   /****************************************************************
       --- Function Get_Item_Serial_Code
       -- Checks whether inventory item has Serial Control Code
       -- Description : THis function is used in Personalizations for Rental Mass Additions (1503)
       -- History : 01-19-2012 : Srinivas Malkapuram
       *****************************************************************/
   FUNCTION get_item_serial_code (p_inventory_item_id    NUMBER,
                                  p_organization_id      NUMBER)
      RETURN NUMBER
   IS
      i_serial_code   NUMBER;
   BEGIN
      SELECT serial_number_control_code
        INTO i_serial_code
        FROM mtl_system_items_b
       WHERE     inventory_item_id = p_inventory_item_id
             AND organization_id = p_organization_id;

      RETURN NVL (i_serial_code, 1);
   EXCEPTION
      WHEN OTHERS
      THEN
         RETURN 1;
   END;

   /****************************************************************
    --- Function Is_Rental_Item_Exists
    -- Checks if Equivalent Rental Item Exists in the Same Org
    -- Description : THis function is used in Personalizations for Rental Mass Additions (1503)
    -- History : 01-19-2012 : Srinivas Malkapuram
    *****************************************************************/
   FUNCTION is_rental_item_exists (p_item_number        VARCHAR2,
                                   p_organization_id    NUMBER)
      RETURN NUMBER
   IS
      i_count   NUMBER;
   BEGIN
      SELECT COUNT (*)
        INTO i_count
        FROM mtl_system_items_b
       WHERE     segment1 LIKE 'R' || p_item_number
             AND organization_id = p_organization_id;

      RETURN i_count;
   EXCEPTION
      WHEN OTHERS
      THEN
         RETURN (0);
   END;

   /****************************************************************
    --- Function Get_NonSerial_Asset_Number
    -- Gets Asset Number for NonSerialized Items within inventory Org
    -- Description : THis function is used in Rental Asset Transfer Report (1504)
    -- History : 01-20-2012 : Srinivas Malkapuram
    *****************************************************************/
   FUNCTION get_nonserial_asset_number (p_segment1           VARCHAR2,
                                        p_organization_id    NUMBER)
      RETURN VARCHAR2
   IS
      i_nonserial_asset_number   VARCHAR2 (20);
   BEGIN
      SELECT fab.asset_number
        INTO i_nonserial_asset_number
        FROM mtl_parameters mp,
             fa_locations fal,
             fa_distribution_history fadh,
             fa_additions_b fab
       WHERE     1 = 1
             AND mp.attribute10 = fal.segment1
             AND fal.location_id = fadh.location_id
             AND fadh.asset_id = fab.asset_id
             AND fab.attribute4 = p_segment1
             AND mp.organization_id = p_organization_id
             AND ROWNUM = 1;

      --It is assumed that there will be only 1 Nonserialized item per Inventory Org
      RETURN i_nonserial_asset_number;
   EXCEPTION
      WHEN OTHERS
      THEN
         RETURN NULL;
   END;

   /****************************************************************
       --- Function Get_Serialized_Asset_Number
       -- Gets Asset Number for Serialized Items within inventory Org
       -- Description : THis function is used in Rental Asset Transfer Report (1504)
       -- History : 01-20-2012 : Srinivas Malkapuram
       *****************************************************************/
   FUNCTION get_serial_asset_number (p_transaction_id       NUMBER,
                                     p_inventory_item_id    NUMBER,
                                     p_organization_id      NUMBER)
      RETURN VARCHAR2
   IS
      i_serial_asset_number   VARCHAR2 (20);
   BEGIN
      SELECT mut.serial_number
        INTO i_serial_asset_number
        FROM mtl_unit_transactions mut
       WHERE     1 = 1
             AND mut.inventory_item_id = p_inventory_item_id
             AND mut.organization_id = p_organization_id
             AND mut.transaction_id = p_transaction_id
             AND ROWNUM = 1;

      RETURN i_serial_asset_number;
   EXCEPTION
      WHEN OTHERS
      THEN
         RETURN NULL;
   END;

   /****************************************************************
    --- Function Get_Asset_Type
    -- Gets Asset Number for NonSerialized Items within inventory Org
    -- Description : THis function is used in Rental Asset Transfer Report (1504)
    -- History : 01-20-2012 : Srinivas Malkapuram
    *****************************************************************/
   FUNCTION get_asset_type (p_serial_control_code    NUMBER,
                            p_asset_number           VARCHAR2)
      RETURN VARCHAR2
   IS
      i_asset_type   VARCHAR2 (50);
   BEGIN
      IF p_serial_control_code = 5
      THEN
         SELECT fab.asset_type
           INTO i_asset_type
           FROM fa_additions_b fab
          WHERE 1 = 1 AND fab.serial_number = p_asset_number AND ROWNUM = 1;
      ELSE
         SELECT fab.asset_type
           INTO i_asset_type
           FROM fa_additions_b fab
          WHERE 1 = 1 AND fab.asset_number = p_asset_number AND ROWNUM = 1;
      END IF;

      RETURN i_asset_type;
   EXCEPTION
      WHEN OTHERS
      THEN
         RETURN NULL;
   END;

   /****************************************************************
    --- Function Get_Asset_Number
    -- Gets Asset Number for NonSerialized Items within inventory Org
    -- Description : THis function is used in Rental Asset Transfer Report (1504)
    -- History : 01-20-2012 : Srinivas Malkapuram
    *****************************************************************/
   FUNCTION get_asset_number (p_serial_control_code    NUMBER,
                              p_asset_number           VARCHAR2)
      RETURN VARCHAR2
   IS
      i_asset_number   VARCHAR2 (50);
   BEGIN
      IF p_serial_control_code = 5
      THEN
         SELECT fab.asset_number
           INTO i_asset_number
           FROM fa_additions_b fab
          WHERE 1 = 1 AND fab.serial_number = p_asset_number AND ROWNUM = 1;
      ELSE
         i_asset_number := p_asset_number;
      END IF;

      RETURN i_asset_number;
   EXCEPTION
      WHEN OTHERS
      THEN
         RETURN NULL;
   END;

   /****************************************************************
  --- Function Get_Asset_Category
  -- Gets Asset Number for NonSerialized Items within inventory Org
  -- Description : THis function is used in Rental Asset Transfer Report (1504)
  -- History : 01-20-2012 : Srinivas Malkapuram
  *****************************************************************/
   FUNCTION get_asset_segments (p_asset_number VARCHAR2)
      RETURN VARCHAR2
   IS
      i_asset_segments   VARCHAR2 (100);
   BEGIN
      SELECT segment1 || ',' || segment2
        INTO i_asset_segments
        FROM fa_additions_b fab, fa_categories fac
       WHERE     1 = 1
             AND fac.category_id = fab.asset_category_id
             AND fab.asset_number = p_asset_number
             AND ROWNUM = 1;

      RETURN i_asset_segments;
   EXCEPTION
      WHEN OTHERS
      THEN
         RETURN NULL;
   END;

   -- To display the White CAP Job site contact and Phone numbers   ---  Developed By Thiru
   -- Ver 1.1 Added new condition on 26-Nov-2015 for TMS 20150729-00197
   -- Ver 1.2 Added outer join condition for primary flag on 16-12-2015 for TMS 20151215-00225
   /*************************************************************************
      PROCEDURE Name: job_site_contact

      PURPOSE:   To create contact when contact does not exist in Oracle

      REVISIONS:
      Ver        Date        Author                     Description
      ---------  ----------  ---------------         -------------------------
      1.3        02-22-2016  Kishore                 TMS 20160219-00134 - Ship to contact on the Pick and Pack slip are not displayed
   ****************************************************************************/
   PROCEDURE job_site_contact (p_ship_to_contact_id       NUMBER,
                               p_contact_name         OUT VARCHAR2,
                               p_contact_number       OUT VARCHAR2)
   IS
      l_cont_name      VARCHAR2 (200) := NULL;
      v_name           VARCHAR2 (500);
      v_number         VARCHAR2 (100);
      l_rel_party_id   NUMBER;                                 -- Version# 1.3
      l_phone_number   VARCHAR2 (20);                          -- Version# 1.3
      l_area_code      VARCHAR2 (20);                          -- Version# 1.3
   -- Version# 1.3 > Start
   /*
         CURSOR curr_contact (p_ship_to_contact_id NUMBER)
         IS
            SELECT ph.area_code, ph.phone_number, co.title, co.first_name,
                   co.last_name
               FROM ar_contacts_v co, ar_phones_v ph
             WHERE 1 = 1
               AND co.rel_party_id = ph.owner_table_id(+)
               AND ph.owner_table_name(+) = 'HZ_PARTIES'
               AND ph.status(+) = 'A'
           --    and ph.primary_flag='Y'  -- Ver 1.1  Ver 1.2
               and ph.primary_flag(+)='Y'  -- Ver 1.2
               AND (ph.phone_type='GEN' OR ph.phone_type='MOBILE' OR ph.phone_type IS NULL)  -- Ver 1.1
               AND co.contact_id = p_ship_to_contact_id
               AND ROWNUM = 1;
   */
   -- Version# 1.3 < End

   BEGIN
      -- Version# 1.3 > Start
      IF p_ship_to_contact_id IS NOT NULL
      THEN
         -------------------------------------------------------------------
         -- Derive Contact First and Last Names
         -------------------------------------------------------------------
         SELECT title || '  ' || first_name || '  ' || last_name,
                rel_party_id
           INTO l_cont_name, l_rel_party_id
           FROM ar_contacts_v co
          WHERE 1 = 1 AND co.contact_id = p_ship_to_contact_id AND ROWNUM = 1;

         -------------------------------------------------------------------
         -- Derive Phone Number and Area Code
         -------------------------------------------------------------------
         BEGIN
            SELECT ph.area_code, ph.phone_number
              INTO l_area_code, l_phone_number
              FROM ar_phones_v ph
             WHERE     1 = 1
                   AND l_rel_party_id = ph.owner_table_id(+)
                   AND ph.owner_table_name(+) = 'HZ_PARTIES'
                   AND ph.status(+) = 'A'
                   AND ph.primary_flag(+) = 'Y'
                   AND (   ph.phone_type = 'GEN'
                        OR ph.phone_type = 'MOBILE'
                        OR ph.phone_type IS NULL)
                   AND ROWNUM = 1;
         EXCEPTION
            WHEN OTHERS
            THEN
               NULL;
         END;

         -------------------------------------------------------------------
         -- Format Phone Number
         -------------------------------------------------------------------
         IF l_phone_number IS NOT NULL
         THEN
            IF INSTR (l_phone_number, '-', 4) = 0
            THEN
               v_number :=
                  (   l_area_code
                   || '-'
                   || SUBSTR (l_phone_number, 1, 3)
                   || '-'
                   || SUBSTR (l_phone_number, 4, 7));
            ELSE
               v_number := (l_area_code || '-' || l_phone_number);
            END IF;
         ELSE
            v_number := NULL;
         END IF;                         -- IF l_phone_number IS NOT NULL THEN
      END IF;                       --IF p_ship_to_contact_id IS NOT NULL THEN


      /*
            FOR v_curr_contact IN curr_contact (p_ship_to_contact_id)
            LOOP
               l_cont_name :=
                     v_curr_contact.title
                  || '  '
                  || v_curr_contact.first_name
                  || '  '
                  || v_curr_contact.last_name;

      --------------
               IF v_curr_contact.phone_number IS NOT NULL
               THEN
                  IF INSTR (v_curr_contact.phone_number, '-', 4) = 0
                  THEN
                     v_number :=
                        (   v_curr_contact.area_code
                         || '-'
                         || SUBSTR (v_curr_contact.phone_number, 1, 3)
                         || '-'
                         || SUBSTR (v_curr_contact.phone_number, 4, 7)
                        );
                  ELSE
                     v_number :=
                        (   v_curr_contact.area_code
                         || '-'
                         || v_curr_contact.phone_number
                        );
                  END IF;
               ELSE
                  v_number := NULL;
               END IF;
      -----------------
            END LOOP;
      */
      -- Version# 1.3 < End

      p_contact_name := TRIM (l_cont_name);
      p_contact_number := TRIM (v_number);
   EXCEPTION
      WHEN OTHERS
      THEN
         p_contact_name := '';
         p_contact_number := '';
   /*********************************************************************************************************************
   The purpose of this procedure is to find site contact and phone number.this is used for following reports.
   1.Repair Quote,
   2.Pick Ticket,
   3.Rental Pick Ticket,
   4.Sales Receipt,
   5.Rental Sales Receipt,
   6.Quote,
   7.Rental Quote,
   8.Repair Tool Transafer,
   9.Repair Acknowledgement.
   **********************************************************************************************************************/
   END;

   -- To display the White CAP Job site contact and Phone numbers   ---  Developed By Thiru
   FUNCTION MAP (p_ship_to_org_id NUMBER)
      RETURN VARCHAR2
   IS
      l_map   VARCHAR2 (150) := NULL;
   BEGIN
      SELECT ship_su.attribute3
        INTO l_map
        FROM apps.hz_cust_site_uses ship_su,
             apps.hz_cust_acct_sites ship_cas,
             hz_party_sites bill_ps
       WHERE     1 = 1
             AND ship_su.cust_acct_site_id = ship_cas.cust_acct_site_id
             AND ship_cas.party_site_id = bill_ps.party_site_id
             AND site_use_code = 'SHIP_TO'
             AND ship_su.site_use_id = p_ship_to_org_id;

      RETURN (l_map);
   EXCEPTION
      WHEN NO_DATA_FOUND
      THEN
         RETURN (' ');
      WHEN OTHERS
      THEN
         RETURN (' ');
   /*********************************************************************************************************************
   The purpose of this procedure is to find map number.this is used for following reports.
   1.Repair Quote,
   2.Pick Ticket,
   3.Rental Pick Ticket,
   4.Sales Receipt,
   5.Rental Sales Receipt,
   6.Quote,
   7.Rental Quote,
   8.Repair Tool Transafer,
   9.Repair Acknowledgement.
   **********************************************************************************************************************/
   END;

   /*********************************************************************************************************************
   Returns the Vendor Locations
   1.PO REPORT
   **********************************************************************************************************************/
   PROCEDURE vendor_locations (p_location_id              NUMBER,
                               x_location_code        OUT VARCHAR2,
                               x_address_line_1       OUT VARCHAR2,
                               x_address_line_2       OUT VARCHAR2,
                               x_town_or_city         OUT VARCHAR2,
                               x_country              OUT VARCHAR2,
                               x_postax_code          OUT VARCHAR2,
                               x_telephone_number_1   OUT VARCHAR2)
   IS
      CURSOR c_locations (cp_location_id NUMBER)
      IS
         SELECT location_code,
                address_line_1,
                address_line_2,
                town_or_city,
                country,
                postal_code,
                telephone_number_1
           FROM hr_locations_all
          WHERE location_id = cp_location_id;

      l_location_code        VARCHAR2 (100);
      l_address_line_1       VARCHAR2 (100);
      l_address_line_2       VARCHAR2 (100);
      l_town_or_city         VARCHAR2 (100);
      l_country              VARCHAR2 (100);
      l_postal_code          VARCHAR2 (100);
      l_telephone_number_1   VARCHAR2 (100);
   BEGIN
      OPEN c_locations (p_location_id);

      FETCH c_locations
         INTO x_location_code,
              x_address_line_1,
              x_address_line_2,
              x_town_or_city,
              x_country,
              x_postax_code,
              x_telephone_number_1;

      CLOSE c_locations;
   EXCEPTION
      WHEN OTHERS
      THEN
         BEGIN
            x_location_code := '';
            x_address_line_1 := '';
            x_address_line_2 := '';
            x_town_or_city := '';
            x_country := '';
            x_postax_code := '';
            x_telephone_number_1 := '';
         END;
   END;

   /*********************************************************************************************************************
   Returns the Vendor Details
   1.PO REPORT
   **********************************************************************************************************************/
   PROCEDURE vendordetails_po (p_vendor_id              NUMBER,
                               p_vendor_site_id         NUMBER,
                               x_country            OUT VARCHAR2,
                               x_area_code          OUT VARCHAR2,
                               x_phone              OUT VARCHAR2,
                               x_fax                OUT VARCHAR2,
                               x_vendor_site_code   OUT VARCHAR2,
                               x_address_line1      OUT VARCHAR2,
                               x_address_line2      OUT VARCHAR2,
                               x_city               OUT VARCHAR2,
                               x_state              OUT VARCHAR2,
                               x_zip                OUT VARCHAR2)
   IS
      CURSOR c_vendordetails (
         cp_vendor_id         NUMBER,
         cp_vendor_site_id    NUMBER)
      IS
         SELECT country,
                area_code,
                phone,
                fax,
                vendor_site_code,
                address_line1,
                address_line2,
                city,
                state,
                zip
           FROM ap_supplier_sites
          WHERE     vendor_id = cp_vendor_id
                AND vendor_site_id = cp_vendor_site_id;

      --14529
      l_country            VARCHAR2 (100);
      l_area_code          VARCHAR2 (100);
      l_phone              VARCHAR2 (100);
      l_fax                VARCHAR2 (100);
      l_vendor_site_code   VARCHAR2 (100);
      l_address_line1      VARCHAR2 (100);
      l_address_line2      VARCHAR2 (100);
      l_city               VARCHAR2 (100);
      l_state              VARCHAR2 (100);
      l_zip                VARCHAR2 (100);
   BEGIN
      OPEN c_vendordetails (p_vendor_id, p_vendor_site_id);

      FETCH c_vendordetails
         INTO x_country,
              x_area_code,
              x_phone,
              x_fax,
              x_vendor_site_code,
              x_address_line1,
              x_address_line2,
              x_city,
              x_state,
              x_zip;

      CLOSE c_vendordetails;
   EXCEPTION
      WHEN OTHERS
      THEN
         BEGIN
            x_country := '';
            x_area_code := '';
            x_phone := '';
            x_fax := '';
            x_vendor_site_code := '';
            x_address_line1 := '';
            x_address_line2 := '';
            x_city := '';
            x_state := '';
            x_zip := '';
         END;
   END;

   ---Required for Receipt Traveller and Back Order Report   ---  Developed By Thiru
   --Used in BackOrder Report
   FUNCTION on_hand_qty (p_item_id NUMBER, p_org_id NUMBER)
      RETURN NUMBER
   IS
      x_return_status   VARCHAR2 (1);
      x_msg_data        VARCHAR2 (4000);
      x_msg_count       NUMBER;
      x_qoh             NUMBER;
      x_rqoh            NUMBER;
      x_qr              NUMBER;
      x_qs              NUMBER;
      x_att             NUMBER;
      x_atr             NUMBER;
      x_sqoh            NUMBER;
      x_srqoh           NUMBER;
      x_sqr             NUMBER;
      x_sqs             NUMBER;
      x_satt            NUMBER;
      x_sqtr            NUMBER;
   BEGIN
      --inv_globals.set_org_id (161);
      inv_quantity_tree_pub.clear_quantity_cache;
      inv_quantity_tree_pub.query_quantities (
         p_api_version_number    => 1.0,
         x_return_status         => x_return_status,
         x_msg_count             => x_msg_count,
         x_msg_data              => x_msg_data,
         p_organization_id       => p_org_id,
         p_inventory_item_id     => p_item_id,
         p_tree_mode             => 1,
         p_is_revision_control   => FALSE,
         p_is_lot_control        => FALSE,
         p_is_serial_control     => FALSE,
         p_grade_code            => NULL,
         p_revision              => NULL,
         p_lot_number            => NULL,
         p_subinventory_code     => NULL,
         p_locator_id            => NULL,
         x_qoh                   => x_qoh,
         x_rqoh                  => x_rqoh,
         x_qr                    => x_qr,
         x_qs                    => x_qs,
         x_att                   => x_att,
         x_atr                   => x_atr,
         x_sqoh                  => x_sqoh,
         x_srqoh                 => x_srqoh,
         x_sqr                   => x_sqr,
         x_sqs                   => x_sqs,
         x_satt                  => x_satt,
         x_satr                  => x_sqtr);
      RETURN (x_atr);
   EXCEPTION
      WHEN NO_DATA_FOUND
      THEN
         RETURN NULL;
      WHEN OTHERS
      THEN
         RETURN NULL;
   END on_hand_qty;

   ---Required for white cap CFD reports   ---  Developed By Tavva
   FUNCTION verify_state_as_ca (p_site_use_id IN NUMBER)
      RETURN NUMBER
   IS
      CURSOR c_site_info (
         p_site_id    NUMBER)
      IS
         SELECT hl.address1,
                hcsu.LOCATION,
                hl.address2,
                hl.address3,
                hl.state,
                   DECODE (hl.city, NULL, NULL, hl.city || ', ')
                || DECODE (hl.state,
                           NULL, hl.province || ', ',
                           hl.state || ', ')
                || DECODE (hl.postal_code, NULL, NULL, hl.postal_code)
                   add5,
                hcp.phone_area_code,
                hcp.phone_number,
                hps.party_site_number
           FROM apps.hz_cust_site_uses hcsu,
                apps.hz_cust_acct_sites hcas,
                hz_party_sites hps,
                hz_locations hl,
                hz_contact_points hcp
          WHERE     1 = 1
                AND hcsu.cust_acct_site_id = hcas.cust_acct_site_id
                AND hcas.party_site_id = hps.party_site_id
                AND hcp.owner_table_id(+) = hps.party_site_id
                AND hps.location_id = hl.location_id(+)
                AND hcp.owner_table_name(+) = 'HZ_PARTY_SITES'
                AND hcp.primary_flag(+) = 'Y'
                AND site_use_code = 'SHIP_TO'
                AND hcsu.site_use_id = p_site_id;
   BEGIN
      FOR i IN c_site_info (p_site_use_id)
      LOOP
         IF i.state = 'CA'
         THEN
            RETURN 1;
         ELSE
            RETURN 0;
         END IF;
      END LOOP;
   EXCEPTION
      WHEN OTHERS
      THEN
         RETURN 0;
   END verify_state_as_ca;

   --Required for white cap cancel order lines personalization and returns organization id --Develope by harsha for tms#20130806-00698

   FUNCTION XXWC_GET_USER_PROF_VAL (V_PROFILE_USER VARCHAR2)
      RETURN NUMBER
   AS
      v_errbuf   CLOB;
      V_VAL      NUMBER;
   BEGIN
      BEGIN
         SELECT VALUE
           INTO V_VAL
           FROM (SELECT PO.PROFILE_OPTION_NAME "NAME",
                        PO.USER_PROFILE_OPTION_NAME,
                        DECODE (TO_CHAR (POV.LEVEL_ID),
                                '10001', 'SITE',
                                '10002', 'APP',
                                '10003', 'RESP',
                                '10005', 'SERVER',
                                '10006', 'ORG',
                                '10004', 'USER',
                                '***')
                           "LEVEL",
                        DECODE (TO_CHAR (POV.LEVEL_ID),
                                '10001', '',
                                '10002', APP.APPLICATION_SHORT_NAME,
                                '10003', RSP.RESPONSIBILITY_KEY,
                                '10005', SVR.NODE_NAME,
                                '10006', ORG.NAME,
                                '10004', USR.USER_NAME,
                                '***')
                           "CONTEXT",
                        POV.PROFILE_OPTION_VALUE "VALUE"
                   FROM APPS.FND_PROFILE_OPTIONS_VL PO,
                        APPS.FND_PROFILE_OPTION_VALUES POV,
                        APPS.FND_USER USR,
                        APPS.FND_APPLICATION APP,
                        APPS.FND_RESPONSIBILITY RSP,
                        APPS.FND_NODES SVR,
                        APPS.HR_OPERATING_UNITS ORG
                  WHERE     1 = 1
                        AND POV.APPLICATION_ID = PO.APPLICATION_ID
                        AND POV.PROFILE_OPTION_ID = PO.PROFILE_OPTION_ID
                        AND USR.USER_ID(+) = POV.LEVEL_VALUE
                        AND RSP.APPLICATION_ID(+) =
                               POV.LEVEL_VALUE_APPLICATION_ID
                        AND RSP.RESPONSIBILITY_ID(+) = POV.LEVEL_VALUE
                        AND APP.APPLICATION_ID(+) = POV.LEVEL_VALUE
                        AND SVR.NODE_ID(+) = POV.LEVEL_VALUE
                        AND ORG.ORGANIZATION_ID(+) = POV.LEVEL_VALUE
                        AND PO.PROFILE_OPTION_NAME =
                               'XXWC_OM_DEFAULT_SHIPPING_ORG') xxwc
          WHERE xxwc.context = V_PROFILE_USER;
      EXCEPTION
         WHEN NO_DATA_FOUND
         THEN
            v_val := 0;
         WHEN OTHERS
         THEN
            v_errbuf :=
                  'Error_Stack...'
               || CHR (10)
               || DBMS_UTILITY.format_error_stack ()
               || CHR (10)
               || ' Error_Backtrace...'
               || CHR (10)
               || DBMS_UTILITY.format_error_backtrace ();

            xxwc_helpers.xxwc_log_error (
               p_message          => v_errbuf,
               p_package_name     => 'XXWC_GEN_ROUTINES_PKG',
               p_procedure_name   => 'XXWC_GET_USER_PROF_VAL',
               p_module_name      => 'ONT');


            xxcus_error_pkg.xxcus_error_main_api (
               p_called_from         => 'XXWC_GEN_ROUTINES_PKG',
               p_calling             => 'XXWC_GET_USER_PROF_VAL',
               p_request_id          => fnd_global.conc_request_id,
               p_ora_error_msg       => SUBSTR (
                                          REGEXP_REPLACE (v_errbuf,
                                                          '[[:cntrl:]]',
                                                          NULL),
                                          1,
                                          2000),
               p_error_desc          => SUBSTR (v_errbuf, 1, 240),
               p_distribution_list   => 'HDSOracleDevelopers@hdsupply.com',
               p_module              => 'ONT');



            v_val := 0;
      END;

      DBMS_OUTPUT.put_line ('v_val..' || v_val);



      RETURN V_VAL;
   END XXWC_GET_USER_PROF_VAL;
END xxwc_gen_routines_pkg;
/
