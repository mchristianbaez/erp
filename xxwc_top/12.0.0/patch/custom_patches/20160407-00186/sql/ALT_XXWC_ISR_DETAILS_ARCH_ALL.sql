/*************************************************************************************************************************************************************
   -- File Name:  ALT_XXWC_ISR_DETAILS_ARCH_ALL.sql
   --
   -- PROGRAM TYPE: Table
   --
   -- PURPOSE:  Altered table to disable Parallelism
   -- HISTORY
   -- ===========================================================================================================================================================
   -- ===========================================================================================================================================================
   -- VERSION DATE          AUTHOR(S)       DESCRIPTION
   -- ------- -----------   --------------- ---------------------------------------------------------------------------------------------------------------------
   -- 1.0     18-APR-2016   P.Vamshidhar    TMS#20160407-00186#Remove Parallelism for ISR

  ***************************************************************************************************************************************************************/

ALTER TABLE XXWC.XXWC_ISR_DETAILS_ARCH_ALL NOPARALLEL;