/*************************************************************************
  $Header TMS_20160928-00029_CLOSE_ORDER_LINE.sql $
  Module Name: TMS_20160928-00029  Data Fix script for 17517650

  PURPOSE: Data fix script for 17517650--No permanent fix in process (But patch available per oracle)

  REVISIONS:
  Ver        Date         Author                Description
  ---------  -----------  ------------------    --------------------------
  1.0        10-OCT-2016  Niraj K Ranjan        TMS#20160928-00029 

**************************************************************************/ 
SET SERVEROUTPUT ON SIZE 1000000
DECLARE
BEGIN
   DBMS_OUTPUT.put_line ('TMS: 20160928-00029    , Before Update');

update apps.oe_order_lines_all
set 
  INVOICE_INTERFACE_STATUS_CODE='NOT_ELIGIBLE'
 ,open_flag='N'
 ,flow_status_code='CLOSED'
--,INVOICED_QUANTITY=1
where line_id in (79544365, 79489524);

   DBMS_OUTPUT.put_line (
         'TMS: 20160928-00029  Sales order lines updated (Expected:1): '
      || SQL%ROWCOUNT);

   COMMIT;

   DBMS_OUTPUT.put_line ('TMS: 20160928-00029    , End Update');
EXCEPTION
   WHEN OTHERS
   THEN
      ROLLBACK;
      DBMS_OUTPUT.put_line ('TMS: 20160928-00029 , Errors : ' || SQLERRM);
END;
/


