/*
 TMS: 20160114-00026    
 Date: 01/14/2016
 Notes: @SF Data fix script for I643240
*/

SET SERVEROUTPUT ON SIZE 100000;
SET VERIFY OFF;


update apps.oe_order_lines_all
set INVOICE_INTERFACE_STATUS_CODE='NOT_ELIGIBLE'
,open_flag='N'
,flow_status_code='CLOSED'
--,INVOICED_QUANTITY=1
where line_id =61967542
and headeR_id=37795084;
		  
--1 row expected to be updated

commit;

/