CREATE OR REPLACE PACKAGE BODY APPS."XXCUSCE_BAI_INTERFACE_PKG" AS
/*
-- Copyright 2012 HD Supply  Inc (Orlando, FL) - All rights reserved
--
--    HDS GSC Finance -Oracle Applications Release 12.1.3
--    Module: Cash Management
--    Package Name:      XXCUSCE_BAI_INTERFACE_PKG
--
--    Revision History:
--    Ver        Date        Author                                  Description
--    ---------  ----------  ---------------                         -------------------------------
--    1.0                                                                         Package header template missing
--    1.1        11/20/2015  Balaguru Seshadri    Comment logic to backup ce_statement_headers and lines into custom table xxcusce_statement_headers and lines
--    1.2        03/03/2016  Balaguru Seshadri    Add procedure swap_bai_value_date [ ESMS: 262456   TMS: 20151130-00206]
*/
 --
    -- Begin Ver 1.2
    procedure swap_bai_value_date
     (
       p_file_path varchar2
      ,p_file_name varchar2
     )  is
    /*
     -- Author: Balaguru Seshadri
     -- Parameters: 
     -- Scope: Used by Bank of America Canada treasury statement file import program
     -- Modification History
     -- ESMS               TMS                           Date                     Version    Comments
     -- =========== ===============  ==========        =======  ========================
     -- 262456          20151130-00206   01-MAR-2016   1.2           Created.
     */ 
    --
        file_id         utl_file.file_type;
        l_tmp_file_id utl_file.file_type;
        b_proceed boolean :=null;
        l_req_id number;
        l_path VARCHAR2(240) :=Null;
        --
        l_tmp_path varchar2(20) :='XXWC_PNC_RECON';
        l_line_rec_num varchar2(20) :=Null;
        l_line_num number :=0;
        --
        l_statement_date varchar2(20) :=Null;
        l_funds_type varchar2(20) :=Null;
        l_V_date varchar2(20) :=Null;
        --
        l_command VARCHAR2(4000) :=NULL;
        l_result VARCHAR2(4000) :=NULL;
        l_distro_list   VARCHAR2(75) DEFAULT 'HDSOracleDevelopers@hdsupply.com';
       --
       l_header varchar2(240) :='TRANSIT,ACCOUNT,PAYCHECK_NUM,NET_PAY';
       --
        l_buffer                     varchar2(32767);
        l_16_V_new_line   varchar2(32767);
        l_bkup_file_name  varchar2(150);
        l_tmp_file_name    varchar2(150);
      --    
        l_lines         pls_integer :=0;
        c_eol           constant varchar2(1) :=chr(10);
        c_eol_len   constant pls_integer :=length(c_eol);
        c_maxline  constant pls_integer :=32767;   
       --    
     --
         procedure print_log(p_message in varchar2) is
         begin
           fnd_file.put_line(fnd_file.log, p_message);
           fnd_file.put_line(fnd_file.output, p_message);
         end print_log;
      --    
      function get_field (v_delimiter in varchar2, n_field_no in number ,v_line_read in varchar2, p_which_line in number ) return varchar2 is
           n_start_field_pos number;
           n_end_field_pos   number;
           v_get_field       varchar2(2000);
      begin
           if n_field_no = 1 then
              n_start_field_pos := 1;
           else
              n_start_field_pos := instr(v_line_read,v_delimiter,1,n_field_no-1)+1;
           end if;
          
           n_end_field_pos   := instr(v_line_read,v_delimiter,1,n_field_no) -1;
           if n_end_field_pos > 0 then
              v_get_field := substr(v_line_read,n_start_field_pos,(n_end_field_pos - n_start_field_pos)+1);
           else
              v_get_field := substr(v_line_read,n_start_field_pos); 
           end if;

           return v_get_field;
      exception
        when others then
         print_log ('Line# ='||p_which_line);
         print_log ('Line ='||v_line_read);
         print_log ('get field: '||sqlerrm);
      end get_field;
      --
    begin  --Main processing
         --
         b_proceed :=true;
         --
         if fnd_global.conc_request_id >0 then
          l_req_id :=fnd_global.conc_request_id;
         else
          l_req_id :=0;     
         end if;
         --
         print_log(' ');     
         print_log('Parameters:');
         print_log(' ');
         print_log(' p_file_path :'||p_file_path); 
         print_log(' p_file_name :'||p_file_name);     
         print_log(''); 
         --
         print_log('Begin process.');
         print_log('');  
          --
          --Assign the file name 
           l_bkup_file_name :=p_file_name||'.original'; --backup original file
           --
           --We are creating tmp file so as to swap the value date to the statement date if we've a record_type of 16 and fund type of V
           --       
           l_tmp_file_name :=p_file_name||'.tmp'; 
           --
             print_log('Working variables :');
             print_log(' ');         
             print_log(' l_bkup_file_name :'||l_bkup_file_name);
             print_log(' l_tmp_file_name :'||l_tmp_file_name);
             print_log(''); 
          --       
             begin
                    l_command :='cp'
                             ||' '
                             ||p_file_path
                             ||'/'
                             ||p_file_name
                             ||' '
                             ||p_file_path
                             ||'/'
                             ||l_bkup_file_name;
                     --
                     print_log(' ');                                      
                     print_log('Before backup of original file: '||p_file_name);
                     print_log(' ');
                     print_log(l_command);
                     --
                     select xxwc_edi_iface_pkg.xxwc_oscommand_run (l_command)
                     into   l_result 
                     from   dual;
                    --
                    l_command :=null;
                     --                
                    b_proceed :=TRUE;
                    --
                     print_log(' ');                                      
                     print_log('After backup of original file: '||p_file_name);
                     print_log(' ');                
             exception 
              when others then
                print_log('Failed to backup file '||p_file_name);
                xxcus_error_pkg.xxcus_error_main_api(p_called_from       => 'apps.xxcusce_bai_interface_pkg.swap_bai_value_date'
                                                ,p_calling           => 'Failed to backup file '||p_file_name
                                                ,p_request_id        => fnd_global.conc_request_id
                                                ,p_ora_error_msg     => substr(' Error_Stack...' ||
                                                                               dbms_utility.format_error_stack() ||
                                                                               ' Error_Backtrace...' ||
                                                                               dbms_utility.format_error_backtrace()
                                                                              ,1
                                                                              ,2000)
                                                ,p_error_desc        => substr(sqlerrm
                                                                              ,1
                                                                              ,240)
                                                ,p_distribution_list => l_distro_list
                                                ,p_module            => 'GSC CASH MGMT: PRE PROCESS BAI CANADA FILE');            
                b_proceed :=FALSE;       
             end; 
           --
           --Get the file pointer
           file_id :=utl_file.fopen(l_tmp_path, p_file_name, 'r'); --incoming file is read
           l_tmp_file_id :=utl_file.fopen(l_tmp_path, l_tmp_file_name, 'w'); --tmp file is written
           --
           begin 
               --
                loop
                  --               
                   begin
                       --
                       utl_file.get_line(file_id, l_buffer);
                       l_line_num :=l_line_num +1;
                       --
                      l_line_rec_num :=get_field (v_delimiter =>',', n_field_no =>1 ,v_line_read =>l_buffer, p_which_line =>l_line_num);
                      --
                         if l_line_rec_num ='02' then
                           --
                                      l_statement_date :=get_field (v_delimiter =>',', n_field_no =>5 ,v_line_read =>l_buffer, p_which_line =>l_line_num);
                                      print_log('l_line_num ='||l_line_num||', l_line_rec_num ='||l_line_rec_num||', l_statement_date ='||l_statement_date);
                           --
                         elsif l_line_rec_num ='16' then
                           --
                                  l_funds_type :=get_field (v_delimiter =>',', n_field_no =>4 ,v_line_read =>l_buffer, p_which_line =>l_line_num);
                                  l_V_date :=get_field (v_delimiter =>',', n_field_no =>5 ,v_line_read =>l_buffer, p_which_line =>l_line_num); 
                                  print_log('l_line_num ='||l_line_num||', l_funds_type ='||l_funds_type||', l_V_date ='||l_V_date);
                                   l_16_V_new_line :=SUBSTR(l_buffer, 1, instr(l_buffer, 'V')-1)||'V'||regexp_replace(SUBSTR(l_buffer, instr(l_buffer, 'V')+1), '[0-9]{6}',l_statement_date);
                          --                                           
                         else
                          null;
                         end if; 
                      -- 
                          if l_line_rec_num !='16' then
                               --
                               -- we are in a line that doesn't require any modification to date value, just write the line to the new temp file
                               --
                               utl_file.put_line(l_tmp_file_id, l_buffer);
                               --
                          elsif (l_line_rec_num ='16' and l_funds_type ='V') then
                               --
                               -- we are in a line that does require modification to field value_date, edit the line before copying to the new temp file
                               --
                               utl_file.put_line(l_tmp_file_id, l_16_V_new_line);
                               print_log('Modifying line# '||l_line_num||' as shown below.');
                               print_log('Old line : '||l_buffer);
                               print_log('New line : '||l_16_V_new_line);
                               --                  
                          elsif (l_line_rec_num ='16' and l_funds_type !='V') then
                               --
                               -- we are in a line that is of record type 16 but of fund type other than "V", just write the original line to the new temp file
                               --
                               utl_file.put_line(l_tmp_file_id, l_buffer); 
                               --
                          else 
                              --Everything else just copy the original line with no edits
                               utl_file.put_line(l_tmp_file_id, l_buffer);
                               --                                                  
                          end if;
                      --              
                   exception
                    when no_data_found then
                     exit;
                   end;
                end loop;
           exception
             when others then
              print_log('Error in reading file , msg ='||sqlerrm);
           end;
           --
           utl_file.fclose(file_id);
           utl_file.fclose(l_tmp_file_id);       
           --
             begin
                     l_command :='mv'
                               ||' '
                               ||p_file_path
                               ||'/' 
                               ||l_tmp_file_name --temp csv file
                               ||' '
                               ||p_file_path
                               ||'/' 
                               ||p_file_name; --final csv file
                     --                     
                     print_log('MV command:');
                     print_log('============');
                     print_log(l_command);       
                     --             
                     select xxwc_edi_iface_pkg.xxwc_oscommand_run (l_command)
                     into   l_result 
                     from   dual;             
                    --
             exception 
              when others then
                print_log('Failed to rename the temp file '||l_tmp_file_name||' to final file '||p_file_name);
                xxcus_error_pkg.xxcus_error_main_api(p_called_from       =>  'apps.xxcusce_bai_interface_pkg.swap_bai_value_date'
                                                ,p_calling           => 'Rename temp file to final'
                                                ,p_request_id        => fnd_global.conc_request_id
                                                ,p_ora_error_msg     => substr(' Error_Stack...' ||
                                                                               dbms_utility.format_error_stack() ||
                                                                               ' Error_Backtrace...' ||
                                                                               dbms_utility.format_error_backtrace()
                                                                              ,1
                                                                              ,2000)
                                                ,p_error_desc        => substr(sqlerrm
                                                                              ,1
                                                                              ,240)
                                                ,p_distribution_list => l_distro_list
                                                ,p_module            => 'GSC CASH MGMT: PRE PROCESS BAI CANADA FILE');                         
             end;
         --
         print_log('End process.');
         --
    exception
     when others then
      print_log ('Main Block, Error =>'||sqlerrm);
            xxcus_error_pkg.xxcus_error_main_api(p_called_from       => 'apps.xxcusce_bai_interface_pkg.swap_bai_value_date'
                                                ,p_calling           => 'Outer block'
                                                ,p_request_id        => fnd_global.conc_request_id
                                                ,p_ora_error_msg     => substr(' Error_Stack...' ||
                                                                               dbms_utility.format_error_stack() ||
                                                                               ' Error_Backtrace...' ||
                                                                               dbms_utility.format_error_backtrace()
                                                                              ,1
                                                                              ,2000)
                                                ,p_error_desc        => substr(sqlerrm
                                                                              ,1
                                                                              ,240)
                                                ,p_distribution_list => l_distro_list
                                                ,p_module            => 'GSC CASH MGMT: PRE PROCESS BAI CANADA FILE');  
    end swap_bai_value_date; 
    -- End Ver 1.2
    --
  PROCEDURE load_pd_bai_files(errbuf           OUT VARCHAR2
                             ,retcode          OUT NUMBER
                             ,p_bank_file_name IN VARCHAR2
                             ,p_bank_name      IN VARCHAR2
                             ,p_directory_name IN VARCHAR2
                             ,p_bank_stmt_map  IN VARCHAR2
                             ) IS
    --
    -- Name:
    -- LOAD_PD_BAI_FILES
    --
    -- Description:
    -- Load the prior day (PD) BAI files in to Oracle Treasury standard BAI statement tables
    -- after verifying for any missing PD BAI bank accounts in the BAI files.
    --
    -- Arguments:
    -- p_bank_file_name  - Bank BAI file name
    -- p_bank_name        - Bank Name
    -- p_directory_name  - Unix Directory where BAI files are picked up from
    --
    -- History:
    -- Syam Palli           15-FEB-2006       Initial Creation
    --
    -- Manny Rodriguez     08-MAR-2006       Updated weekend and holiday logic for missing PD bank accts
    --                     08-MAR-2006       Changed GL_DATE to Varchar2 for the CESQLLDR parameter list
    --  Manny Rodriguez    06-DEC-2006       Changed Package to look for rowcount of the Suntrust file.
    --  Line 1276
    --  Manny Rodriguez    07-MAR-2007       Changed line 1131 to look for greater than OR equal to.
    --  Manny Rodriguez    09-JUL-2007       Changed Checksum to remove trailing spaces.
    --  Manny Rodriguez    04-AUG-2007       Changed hard-coded banks to look at lookup_type XXHSI_BANKS off
    --                                       the lookup table fnd_lookup_values
    --  Manny Rodriguez    05-AUG-2007       Moved the Cash position program to the PD section (line 849)
    --  Manny Rodriguez    12-SEP-2007       Fixed Lockbox code to use nvl(replace()) instead of rpad
    --  Balaguru Seshadri   20-NOV-2015      TMS 20151118-00139, ESMS 308524  -Ver 1.1 -Comment logic to backup ce_statement_headers and lines into custom table xxcusce_statement_headers and lines
    --  Balaguru Seshadri  03/02/2016          Call procedure xxcus_gsc_bai_datefix -- Ver 1.2 ESMS: 262456   TMS: 20151130-00206

    v_req_id                    NUMBER         := fnd_global.conc_request_id;
    v_user_id                   NUMBER         := fnd_global.user_id;
    v_error_message             VARCHAR2(3000) := NULL;
    v_error_code                VARCHAR2(240)  := NULL;
    v_phase                     VARCHAR2(200);
    v_status                    VARCHAR2(200);
    v_dev_phase                 VARCHAR2(200)  := 'NOT COMPLETE';
    v_dev_status                VARCHAR2(200);
    v_message                   VARCHAR2(3000);
    v_request_status            BOOLEAN;
    v_error_status              NUMBER;
    v_start_date                DATE;
    v_end_date                  DATE;
    vl_start_date               VARCHAR2(100);
    vl_end_date                 VARCHAR2(100);
    vl_classification_rollup    VARCHAR2(1);
    v_open_coll_process         NUMBER;
    v_close_coll_process        NUMBER;
    v_batch_name                VARCHAR2(200) := to_char(SYSDATE, 'DD-MON-YYYY HH:MM:SS');
    v_rec_count                 NUMBER := NULL;
    pl_errorstatus              NUMBER;
    v_bank_party_id             NUMBER;
    v_bank_branch_name          VARCHAR2(2000);
    v_bank_name                 VARCHAR2(2000);
    v_map_id                    NUMBER;
    v_03_rec_id                 NUMBER;
    v_req_id1                   NUMBER := NULL;
    v_file_status               VARCHAR2(10)  := 'S';
    v_bank_branch_count         NUMBER := 0;
    v_archive_directory         VARCHAR2(240);
    v_upd_xxcusce_bank_file_trg VARCHAR2(500);
    v_bai_count                 NUMBER;
    v_holiday                   VARCHAR2(1);
    v_not_corp_flag             VARCHAR2(1)   := 'N';  -- WC

    l_sec                       VARCHAR2(250);
    l_user_id                   NUMBER;
    l_resp_key                  VARCHAR2(30);
    l_resp_id                   NUMBER;
    l_resp_appl_id              NUMBER;
    l_can_submit_request        BOOLEAN := TRUE;
    l_globalset                 VARCHAR2(100);
    b_compl boolean; -- Ver 1.2
    process_err EXCEPTION;

  BEGIN
    --  Setup parameters for running FND JOBS!
    l_can_submit_request := xxcus_misc_pkg.set_responsibility('GLINTERFACE', 'Cash Management Superuser');

    IF l_can_submit_request THEN
      l_globalset := 'Global Variables are set.';
    ELSE
      l_globalset := 'Global Variables are not set.';
      l_sec       := 'Global Variables are not set for the Responsibility of XXHSI_CON and the User of Conversion.';
      fnd_file.put_line(fnd_file.log, l_sec);
      fnd_file.put_line(fnd_file.output, l_sec);
      RAISE program_error;
    END IF;
    -- Begin Ver 1.2
    begin  
         --
         if p_bank_name ='BANK OF AMERICA CANADA' then
            swap_bai_value_date
             (
               p_file_path =>p_directory_name
              ,p_file_name =>p_bank_file_name
             );         
         end if;
         --
    exception
     when others then
      b_compl := Fnd_Concurrent.Set_Completion_Status('ERROR', 'Failed to pre process the original file for value date functinality. please check the log file.');
    end; 
    -- End Ver 1.2
    fnd_file.put_line(fnd_file.log, l_globalset);
    fnd_file.put_line(fnd_file.output, l_globalset);

    fnd_file.put_line(fnd_file.log, 'Input parameters are....:');
    fnd_file.put_line(fnd_file.log, 'p_bank_file_name...... = ' || p_bank_file_name);
    fnd_file.put_line(fnd_file.log, 'p_bank_name...... = ' || p_bank_name);
    fnd_file.put_line(fnd_file.log, 'p_directory_name...... = ' || p_directory_name);

    --
    -- Derive the Bank statement map id for BAI format BAI2
    --
    BEGIN
      SELECT map_id
        INTO v_map_id
        FROM ce_bank_stmt_int_map
       WHERE enabled = 'Y'
         AND format_name = p_bank_stmt_map --'BAI2_PRD'
         ;
    EXCEPTION
      WHEN OTHERS THEN
        v_error_message := 'Failed to derive the Mapping Id for BAI2_PRD .....with error: ' ||SQLERRM;
        v_error_code    := substr(SQLCODE, 1, 30);
        RAISE process_err;
    END;

    --
    -- Derive the concrentared bank name
    --
    BEGIN
      SELECT DISTINCT bank_name
           , branch_party_id
        INTO v_bank_name
            ,v_bank_party_id
        FROM apps.ce_bank_branches_v
       WHERE pk_id IN
             (SELECT bank_branch_id
                FROM ce.ce_bank_accounts ba
               WHERE account_classification =
                     decode(fnd_profile.value_wnps('CE_BANK_ACCOUNT_SECURITY_ACCESS'),
                            'ALL',
                            decode(account_classification, 'EXTERNAL', 'X',
                                    account_classification),
                            fnd_profile.value_wnps('CE_BANK_ACCOUNT_SECURITY_ACCESS')))
         AND upper(bank_name) = upper(p_bank_name)
         AND upper(bank_branch_name) = 'CORPORATE';

    EXCEPTION
      WHEN OTHERS THEN
        v_not_corp_flag := 'Y'; -- WC
    END;

    BEGIN
      SELECT lower(NAME)
        INTO pl_instance
        FROM v$database;
    EXCEPTION
      WHEN OTHERS THEN
        NULL;
    END;
    pl_sender := 'Oracle.Applications_' || pl_instance ||'@hughessupply.com';

    fnd_file.put_line(fnd_file.log,'Before Submitting the CESQLLDR......!:');
    --
    -- Submit the SQL loader program to load the BAI file in to BAI staging table
    --
    v_req_id := apps.fnd_request.submit_request(application => 'CE',
                                                program => 'CESQLLDR',
                                                description => NULL,
                                                start_time => SYSDATE,
                                                sub_request => FALSE,
                                                argument1 => 'IMPORT', --'Load and Import'
                                                ---Process Option
                                                argument2 => v_map_id,
                                                --- Mapping Name
                                                argument3 => p_bank_file_name,
                                                ------Data File Name
                                                argument4 => p_directory_name,
                                                ----Directory Path
                                                argument5 => NULL,
                                                ---- Bank Branch Name
                                                argument6 => NULL,
                                                --- Bank Account Number
                                                argument7 => to_char(TRUNC(sysdate), 'YYYY/MM/DD HH24:MI:SS'),
                                                ---- GL Date
                                                argument8 => NULL,
                                                ---- Organization
                                                argument9 => NULL,
                                                ---- Receivables Activity
                                                argument10 => NULL,
                                                --- Payment Method
                                                argument11 => NULL,
                                                --- NSF Handling
                                                argument12 => 'N',
                                                ---- Display Debug
                                                argument13 => NULL,
                                                ---- Debug Path
                                                argument14 => NULL
                                                ---- Debug File
                                                );
    COMMIT;

    IF nvl(v_req_id, 0) = 0 THEN
      --- CESQLLDR
      v_error_message := 'Failed to submit the Bank Statement SQL Loader process for: ' ||
                         p_bank_file_name || ' with error: ' || SQLERRM;
      v_error_code    := substr(SQLCODE, 1, 30);
      RAISE process_err;
    ELSE

      ----CESQLLDR
      v_dev_phase := 'NOT COMPLETE';
      --
      -- Wait for the previous concurrent request (CESQLLDR) to complete
      --
      v_request_status := fnd_concurrent.wait_for_request(request_id => v_req_id,
                                                          INTERVAL => 1,
                                                          max_wait => 15000,
                                                          phase => v_phase,
                                                          status => v_status,
                                                          dev_phase => v_dev_phase,
                                                          dev_status => v_dev_status,
                                                          message => v_message);
      COMMIT;

      IF (v_dev_phase = 'COMPLETE' AND
         (v_dev_status = 'NORMAL' OR v_dev_status = 'WARNING')) THEN
        ---CESQLLDR completed?
        BEGIN
          --
          -- Get the Request Id and Status of sub concurrent request (CESLRPROBAI2) spawned by CESQLLDR
          --
          v_request_status := fnd_concurrent.get_request_status(request_id => v_req_id1,
                                                                appl_shortname => 'CE',
                                                                program => 'CESLRPROBAI2',
                                                                phase => v_phase,
                                                                status => v_status,
                                                                dev_phase => v_dev_phase,
                                                                dev_status => v_dev_status,
                                                                message => v_message);
          fnd_file.put_line(fnd_file.log,
                            'The request Id for CESLRPROBAI2 is ..' ||
                             v_req_id1);

          v_dev_phase := 'NOT COMPLETE';
          --
          -- Wait for the concurrent request CESLRPROBAI2 to complete
          --
          v_request_status := fnd_concurrent.wait_for_request(request_id => (v_req_id1),
                                                              INTERVAL => 1,
                                                              max_wait => 15000,
                                                              phase => v_phase,
                                                              status => v_status,
                                                              dev_phase => v_dev_phase,
                                                              dev_status => v_dev_status,
                                                              message => v_message);
          COMMIT;
          IF (v_dev_phase = 'COMPLETE' AND
             (v_dev_status = 'NORMAL' OR v_dev_status = 'WARNING')) THEN
            ---CESLRPROBAI2 completed?
            NULL;
          ELSE
            v_error_message := 'Failed to submit the Run SQL*Loader- BAI2 for: ' ||
                               p_bank_file_name || ' with error: ' ||
                               SQLERRM;
            v_error_code    := substr(SQLCODE, 1, 30);
            RAISE process_err;
          END IF;
        END;

        fnd_file.put_line(fnd_file.log, 'Before back-up the BAI files....');

        --
        --   Filtering the bank accounts from BAI interface lines table for which the BAI trasaction code set to 'Y'
        --
        v_req_id := NULL;
        dbms_lock.sleep(15); -- Wait 5 seconds for CESLRPROBAI2 before proceeding to deletion.

        insert_bank_recon(v_error_code, v_error_message);

        --
        -- Load data
        --

        fnd_file.put_line(fnd_file.log,
                          'Before Submitting the ARPLABIM......!:');
        --
        -- Update the Status field in triggering table xxcus.XXCUSCE_BANK_FILE_TRG to indicate the completion of processing the BAI file
        --
        BEGIN
          v_upd_xxcusce_bank_file_trg := 'Update xxcus.XXCUSCE_BANK_FILE_TRG set status =  ' || '''' ||
                                         v_file_status || '''' ||
                                         '  where FILE_NAME = ' || '''' ||
                                         p_bank_file_name || '''';
          EXECUTE IMMEDIATE v_upd_xxcusce_bank_file_trg;
          COMMIT;
        EXCEPTION
          WHEN OTHERS THEN
            v_error_message := 'Error while updating the xxcus.XXCUSCE_BANK_FILE_TRG table for...: ' ||
                               p_bank_file_name;
            RAISE process_err;
        END;
        v_error_message := 'Successfully Completed the PD BAI Import Process for...: ' ||
                           v_bank_name;
        fnd_file.put_line(fnd_file.log, v_error_message);

        xxcus_misc_pkg.html_email(p_to => g_default_email,
                                  p_from => 'Oracle.Applications@hughessupply.com',
                                  p_subject => 'Prior Day Bank Statement for ' ||
                                                v_bank_name ||
                                                ' has been processed successfully.',
                                  p_text => v_error_message,
                                  p_html => 'Prior Day Bank Statement for ' ||
                                             v_bank_name ||
                                             ' has been processed successfully.' ||
                                             'THIS IS A SEND ONLY EMAIL - DO NOT REPLY ',
                                  p_smtp_hostname => p_host,
                                  p_smtp_portnum => 25);

        ----   END IF;   --- Missing Files?
      ELSIF (v_dev_phase = 'COMPLETE' AND v_dev_status = 'WARNING') THEN
        ----CESQLLDR completed?
        v_error_message := 'Previous-Day Bank Statement Loader Process completed with ' ||
                           v_dev_status || ' status for the BAI file..:' ||
                           p_bank_file_name || ' and error message is ..: ' ||
                           v_message || ',';
        v_error_code    := substr(SQLCODE, 1, 30);
        fnd_file.put_line(fnd_file.log, v_error_message);

        xxcus_misc_pkg.html_email(p_to => g_default_email,
                                  p_from => 'Oracle.Applications@hughessupply.com',
                                  p_subject => 'Previuos Day Bank Statement Load program Warning for ' ||
                                                v_bank_name,
                                  p_text => v_error_message,
                                  p_html => v_error_message ||
                                             ' As the data has been loaded for this bank statement with WARNING!. ' ||
                                             'THIS IS A SEND ONLY EMAIL - DO NOT REPLY ',
                                  p_smtp_hostname => p_host,
                                  p_smtp_portnum => 25);

      ELSE
        ----CESQLLDR completed?
        v_error_message := 'PD Bank Statement Loader Process failed for the BAI file...:' ||
                           p_bank_file_name || ' with error: ' || v_message; ----||' SQLERRM: '||SQLERRM;
        v_error_code    := substr(SQLCODE, 1, 30);
        RAISE process_err;

      END IF; ----CESQLLDR completed?
    END IF; ----CESQLLDR
    --
    -- Submit the Missing Bank Accounts Report for Prior Day BAI files
    -- Logic flow : If yesterday is Monday and a Holiday, then search last 4 days of bank statements
    --                       If today is Monday, then search last 3 days   of bank statments
    --                       Otherwise, use only today's data for search.
    BEGIN

      BEGIN
        -- If yesterday is Monday and a holiday then process statement 1, else process statment 2
        SELECT 'T'
          INTO v_holiday -- IF 'T', then statement 1 will process
          FROM xtr.xtr_holidays
         WHERE holiday_date = trunc(SYSDATE - 1)
           AND to_char(SYSDATE - 1, 'dy') = 'mon';
      EXCEPTION
        WHEN no_data_found THEN
          v_holiday := 'F'; -- IF 'F', then statement 2 will process
      END;

      IF v_holiday = 'F' THEN
        -- Not a holiday, normal check.
        BEGIN

          SELECT COUNT(*)
            INTO v_bai_count
            FROM xxcus.xxcusce_bank_file_trg
           WHERE upper(bank_name) IN
                 (SELECT meaning
                    FROM fnd_lookup_values
                   WHERE lookup_type = 'XXHSI_BANKS')
             AND status = 'S'
             AND pd_or_cd_flag = 'PD'
             AND creation_date IN
                 (SELECT to_char(dt, 'YYYY-MM-DD')
                    FROM (SELECT to_date(SYSDATE + 1) - rownum dt
                            FROM all_objects)
                   WHERE rownum <=
                         decode(to_char(SYSDATE, 'dy'), 'mon', 3, 1));
        EXCEPTION
          WHEN no_data_found THEN
            v_bai_count := 0;
          WHEN OTHERS THEN
            v_error_message := 'Failed to get the count of processed BAI files from xxcus.XXCUSCE_BANK_FILE_TRG -- with error: ' ||
                               SQLERRM;
            fnd_file.put_line(fnd_file.log, v_error_message);
        END;
      ELSE
        BEGIN
          --  Monday Holiday yesterday!
          SELECT COUNT(*)
            INTO v_bai_count
            FROM xxcus.xxcusce_bank_file_trg
           WHERE upper(bank_name) IN
                 (SELECT meaning
                    FROM fnd_lookup_values
                   WHERE lookup_type = 'XXHSI_BANKS')
             AND status = 'S'
             AND pd_or_cd_flag = 'PD'
             AND creation_date IN
                 (SELECT to_char(dt, 'YYYY-MM-DD')
                    FROM (SELECT to_date(SYSDATE + 1) - rownum dt
                            FROM all_objects)
                   WHERE rownum <= 4);
        EXCEPTION
          WHEN no_data_found THEN
            v_bai_count := 0;
          WHEN OTHERS THEN
            v_error_message := 'Failed to get the count of processed BAI files from xxcus.XXCUSCE_BANK_FILE_TRG -- with error: ' ||
                               SQLERRM;
            fnd_file.put_line(fnd_file.log, v_error_message);
        END;
      END IF;

      IF v_bai_count >= g_total_banks THEN
        ---- Change the 1 to 5 when all the banks are setup correctly
        --
        -- Finding the missing bank accounts in the BAI file
        --  why do we do this ?MR2
        missing_pd_bank_accts(v_error_code, v_error_message, v_rec_count,
                              v_bank_name, v_archive_directory, 'PD');
        COMMIT;
      END IF;
    EXCEPTION
      WHEN no_data_found THEN
        v_bai_count := 0;
      WHEN OTHERS THEN
        v_error_message := 'Failed to submit the Submit the Missing Bank Accounts Report for Prior Day BAI files -- with error: ' ||
                           SQLERRM;
        fnd_file.put_line(fnd_file.log, v_error_message);
    END;
    fnd_file.put_line(fnd_file.log,
                      'Before taking the backup of ce.ce_statement_headers for ' ||
                       v_bank_name || ' .....');
    -- Begin Ver 1.1
    --
    -- Per HDS GSC Finance team the backup of the ce_statement_headers and lines into the custom tables are not required. ESMS# 308524
    -- ESMS: http://intra.corporate.whitecap.net/operations/app_taskmanager/ticketlinker.aspx?ticket=S308524
    --
    /*
    --
    -- Taking back up of BAI Transactions tables
    --
    BEGIN
      INSERT INTO xxcus.xxcusce_statement_headers
        SELECT csh.*
          FROM ce.ce_statement_headers csh
              ,(SELECT bank_account_id
                      ,statement_number
                      ,org_id
                  FROM ce.ce_statement_headers csh
                MINUS
                SELECT bank_account_id
                      ,statement_number
                      ,org_id
                  FROM xxcus.xxcusce_statement_headers) hdm
         WHERE 1 = 1
           AND csh.bank_account_id = hdm.bank_account_id
           AND csh.statement_number = hdm.statement_number
           AND csh.org_id = hdm.org_id;
      COMMIT;
    EXCEPTION
      WHEN OTHERS THEN
        v_error_message := 'Failed to Backup the table ce.ce_statement_headers for ' ||
                           v_bank_name || ' .....with error: ' || SQLERRM;
        v_error_code    := substr(SQLCODE, 1, 30);
        fnd_file.put_line(fnd_file.log, v_error_message);

        xxcus_misc_pkg.html_email(p_to => g_default_email,
                                  p_from => 'Oracle.Applications@hughessupply.com',
                                  p_subject => 'PD BAI file is missing Metavante bank accounts for ...:' ||
                                                v_bank_name,
                                  p_text => v_error_message,
                                  p_html => v_error_message ||
                                             'THIS IS A SEND ONLY EMAIL - DO NOT REPLY ',
                                  p_smtp_hostname => p_host,
                                  p_smtp_portnum => 25);
    END;

    fnd_file.put_line(fnd_file.log,
                      'Before taking the backup of CE_STATEMENT_LINES for ' ||
                       v_bank_name || ' .....');
    BEGIN
      INSERT INTO xxcus.xxcusce_statement_lines
        SELECT csl.*
          FROM ce_statement_lines csl
              ,(SELECT statement_header_id
                      ,line_number
                  FROM ce_statement_lines
                MINUS
                SELECT statement_header_id
                      ,line_number
                  FROM xxcus.xxcusce_statement_lines) hdm
         WHERE 1 = 1
           AND csl.statement_header_id = hdm.statement_header_id
           AND csl.line_number = hdm.line_number;
      COMMIT;
    EXCEPTION
      WHEN OTHERS THEN
        v_error_message := 'Failed to Back up the table CE_STATEMENT_LINES for ' ||
                           v_bank_name || ' ....with error: ' || SQLERRM;
        v_error_code    := substr(SQLCODE, 1, 30);
        fnd_file.put_line(fnd_file.log, v_error_message);

        xxcus_misc_pkg.html_email(p_to => g_default_email,
                                  p_from => 'Oracle.Applications@hughessupply.com',
                                  p_subject => 'PD BAI file is missing Metavante bank accounts for ...:' ||
                                                v_bank_name,
                                  p_text => v_error_message,
                                  p_html => v_error_message ||
                                             'THIS IS A SEND ONLY EMAIL - DO NOT REPLY ',
                                  p_smtp_hostname => p_host,
                                  p_smtp_portnum => 25);
    END;
   */
   -- End Ver 1.1
  EXCEPTION
    WHEN process_err THEN
      fnd_file.put_line(fnd_file.log, v_error_message);

      xxcus_misc_pkg.html_email(p_to => g_default_email,
                                p_from => 'Oracle.Applications@hughessupply.com',
                                p_subject => 'Prior Day Bank Statement processing error for ' ||
                                              v_bank_name, p_text => 'LUONG',
                                --v_error_message,
                                p_html => v_error_message ||
                                           ' No Data will be processes for this bank statement. Please reconcile the error and re-submit the bank statement import process. ' ||
                                           'THIS IS A SEND ONLY EMAIL - DO NOT REPLY ',
                                p_smtp_hostname => p_host,
                                p_smtp_portnum => 25);

      RAISE program_error;

    WHEN OTHERS THEN
      v_error_message := 'Load BAI Program failed due to ...' || SQLERRM;
      fnd_file.put_line(fnd_file.log,
                        'Load BAI Program failed due to ...' || SQLERRM);

      xxcus_misc_pkg.html_email(p_to => g_default_email,
                                p_from => 'Oracle.Applications@hughessupply.com',
                                p_subject => 'PD BAI file is missing Metavante bank accounts for the Bank...:' ||
                                              p_bank_file_name,
                                p_text => v_error_message,
                                p_html => v_error_message ||
                                           'THIS IS A SEND ONLY EMAIL - DO NOT REPLY ',
                                p_smtp_hostname => p_host,
                                p_smtp_portnum => 25);

      RAISE program_error;
  END load_pd_bai_files;





  PROCEDURE cash_calc(errbuf      OUT VARCHAR2
                     ,retcode     OUT NUMBER
                     ,p_bank_name IN VARCHAR2) IS
    --
    -- Name:
    -- CASH_CALC
    --
    -- Description:
    -- Backup Intra Day Statement Headers and Lines.
    -- and delete CAH CALC related records.
    --
    -- Arguments:
    -- p_bank_name      - Bank Name
    --
    -- History:
    -- Syam Palli           15-FEB-2006       Initial Creation
    --

    v_error_message VARCHAR2(3000) := NULL;
    v_error_code    VARCHAR2(240) := NULL;
    pl_errorstatus  NUMBER;
    process_err EXCEPTION;
  BEGIN
    BEGIN
      SELECT lower(NAME)
        INTO pl_instance
        FROM v$database;
    EXCEPTION
      WHEN OTHERS THEN
        NULL;
    END;
    pl_sender := 'Oracle.Applications_' || pl_instance ||
                 '@hughessupply.com';
    --
    --- Take back up of BAI Transaction tables
    --
    BEGIN
      INSERT INTO xxcus.xxcusce_intra_stmt_headers
        SELECT csh.*
          FROM ce.ce_intra_stmt_headers csh
              ,(SELECT bank_account_id
                      ,statement_number
                      ,org_id
                  FROM ce.ce_intra_stmt_headers csh
                MINUS
                SELECT bank_account_id
                      ,statement_number
                      ,org_id
                  FROM xxcus.xxcusce_intra_stmt_headers) hdm
         WHERE 1 = 1
           AND csh.bank_account_id = hdm.bank_account_id
           AND csh.statement_number = hdm.statement_number
           AND csh.org_id = hdm.org_id;
      COMMIT;
    EXCEPTION
      WHEN OTHERS THEN
        v_error_message := 'Failed to backup the table ce.ce_intra_stmt_headers for ' ||
                           p_bank_name || ' .....with error: ' || SQLERRM;
        v_error_code    := substr(SQLCODE, 1, 30);
        COMMIT;
        fnd_file.put_line(fnd_file.log, v_error_message);
        pl_errorstatus := sendmailjpkg.sendmail(smtpservername => p_host --'hsim1.hsi.hughessupply.com'
                                               , sender => pl_sender
                                                ----   'Oracle.Applications@hughessupply.com'
                                               ,
                                                recipient => g_default_email
                                                --pl_dflt_email
                                               , ccrecipient => '',
                                                bccrecipient => '',
                                                subject => 'Failed to Back up or Delete from the BAI Transaction Tables ...:',
                                                BODY => v_error_message ||
                                                         sendmailjpkg.eol ||
                                                         'THIS IS A SEND ONLY EMAIL - DO NOT REPLY ',
                                                errormessage => v_error_message,
                                                attachments => NULL);
    END;
    BEGIN
      INSERT INTO xxcus.xxcusce_intra_stmt_lines
        SELECT csl.*
          FROM ce_intra_stmt_lines csl
              ,(SELECT statement_header_id
                      ,line_number
                  FROM ce_intra_stmt_lines
                MINUS
                SELECT statement_header_id
                      ,line_number
                  FROM xxcus.xxcusce_intra_stmt_lines) hdm
         WHERE 1 = 1
           AND csl.statement_header_id = hdm.statement_header_id
           AND csl.line_number = hdm.line_number;
      COMMIT;
    EXCEPTION
      WHEN OTHERS THEN
        v_error_message := 'Failed to back up the table CE_INTRA_STMT_LINES for ' ||
                           p_bank_name || '....with error: ' || SQLERRM;
        v_error_code    := substr(SQLCODE, 1, 30);
        COMMIT;

        fnd_file.put_line(fnd_file.log, v_error_message);
        pl_errorstatus := sendmailjpkg.sendmail(smtpservername => p_host --'hsim1.hsi.hughessupply.com'
                                               , sender => pl_sender
                                                ----   'Oracle.Applications@hughessupply.com'
                                               ,
                                                recipient => g_default_email
                                                --pl_dflt_email
                                               , ccrecipient => '',
                                                bccrecipient => '',
                                                subject => 'Failed to Back up or Delete from the BAI Transaction Tables ...:',
                                                BODY => v_error_message ||
                                                         sendmailjpkg.eol ||
                                                         'THIS IS A SEND ONLY EMAIL - DO NOT REPLY ',
                                                errormessage => v_error_message,
                                                attachments => NULL);

    END;

    --
    --- Delete the CASH CALC related records from BAI Transaction tables
    --

    BEGIN
      DELETE FROM ce_intra_stmt_lines csh
       WHERE csh.statement_header_id IN
             (SELECT csl.statement_header_id
                FROM ce_intra_stmt_lines  csl
                    ,ce_transaction_codes ctc
               WHERE csl.trx_code_id = ctc.transaction_code_id
                 AND nvl(ctc.attribute1, 'N') <> 'Y')
         AND csh.statement_line_id IN
             (SELECT csl.statement_line_id
                FROM ce_intra_stmt_lines  csl
                    ,ce_transaction_codes ctc
               WHERE csl.trx_code_id = ctc.transaction_code_id
                 AND nvl(ctc.attribute1, 'N') <> 'Y')
         AND csh.line_number IN
             (SELECT csl.line_number
                FROM ce_intra_stmt_lines  csl
                    ,ce_transaction_codes ctc
               WHERE csl.trx_code_id = ctc.transaction_code_id
                 AND nvl(ctc.attribute1, 'N') <> 'Y');
      fnd_file.put_line(fnd_file.log,
                        'Total no.of records deleted from the table CE_INTRA_STMT_LINES ...: ' ||
                         SQL%ROWCOUNT);

      COMMIT;

    EXCEPTION
      WHEN OTHERS THEN
        v_error_message := 'Failed to delete from the table CE_INTRA_STMT_LINES for  ' ||
                           p_bank_name || '...with error: ' || SQLERRM;
        v_error_code    := substr(SQLCODE, 1, 30);
        COMMIT;

        fnd_file.put_line(fnd_file.log, v_error_message);
        pl_errorstatus := sendmailjpkg.sendmail(smtpservername => p_host --'hsim1.hsi.hughessupply.com'
                                               , sender => pl_sender
                                                ----   'Oracle.Applications@hughessupply.com'
                                               ,
                                                recipient => g_default_email
                                                --pl_dflt_email
                                               , ccrecipient => '',
                                                bccrecipient => '',
                                                subject => 'Failed to Back up or Delete from the BAI Transaction Tables ...:',
                                                BODY => v_error_message ||
                                                         sendmailjpkg.eol ||
                                                         'THIS IS A SEND ONLY EMAIL - DO NOT REPLY ',
                                                errormessage => v_error_message,
                                                attachments => NULL);
    END;

    BEGIN
      DELETE FROM ce.ce_intra_stmt_headers csh
       WHERE csh.statement_header_id NOT IN
             (SELECT DISTINCT csl.statement_header_id
                FROM ce_intra_stmt_lines csl);
      fnd_file.put_line(fnd_file.log,
                        'Total no.of records deleted from the table ce.ce_intra_stmt_headers ...: ' ||
                         SQL%ROWCOUNT);
      COMMIT;
    EXCEPTION
      WHEN OTHERS THEN
        v_error_message := 'Failed to delete from the table ce.ce_intra_stmt_headers for ' ||
                           p_bank_name || ' .....with error: ' || SQLERRM;
        v_error_code    := substr(SQLCODE, 1, 30);
        COMMIT;

        fnd_file.put_line(fnd_file.log, v_error_message);
        pl_errorstatus := sendmailjpkg.sendmail(smtpservername => p_host --'hsim1.hsi.hughessupply.com'
                                               , sender => pl_sender
                                                ----  'Oracle.Applications@hughessupply.com'
                                               ,
                                                recipient => g_default_email
                                                --pl_dflt_email
                                               , ccrecipient => '',
                                                bccrecipient => '',
                                                subject => 'Failed to Back up or Delete from the BAI Transaction Tables ...:',
                                                BODY => v_error_message ||
                                                         sendmailjpkg.eol ||
                                                         'THIS IS A SEND ONLY EMAIL - DO NOT REPLY ',
                                                errormessage => v_error_message,
                                                attachments => NULL);
    END;

  EXCEPTION
    WHEN OTHERS THEN
      v_error_message := 'Cash Calc Program failed due to ...' || SQLERRM;
      fnd_file.put_line(fnd_file.log,
                        'Cash Calc Program failed due to ...' || SQLERRM);
      pl_errorstatus := sendmailjpkg.sendmail(smtpservername => p_host --'hsim1.hsi.hughessupply.com'
                                             , sender => pl_sender
                                              ----  'Oracle.Applications@hughessupply.com'
                                             , recipient => g_default_email
                                              --pl_dflt_email
                                             , ccrecipient => '',
                                              bccrecipient => '',
                                              subject => 'Failed to Back up or Delete from the BAI Transaction Tables ...:',
                                              BODY => v_error_message ||
                                                       sendmailjpkg.eol ||
                                                       'THIS IS A SEND ONLY EMAIL - DO NOT REPLY ',
                                              errormessage => v_error_message,
                                              attachments => NULL);
  END cash_calc;

  PROCEDURE missing_pd_bank_accts(p_errbuf         OUT VARCHAR2
                                 ,p_retcode        OUT VARCHAR2
                                 ,p_rec_count      OUT NUMBER
                                 ,p_bank_name      IN VARCHAR2
                                 ,p_directory_name IN VARCHAR2
                                 ,p_pd_or_cd_flag  IN VARCHAR2) IS
    --
    -- Name:
    -- MISSING_PD_BANK_ACCTS
    --
    -- Description:
    -- Report the missing BAI bank accounts in the PD BAI file.
    --
    -- Arguments:
    -- p_bank_name        - Bank Name
    -- p_directory_name  - Unix Directory where BAI files are picked up from
    -- p_pd_or_cd_flag    - Flag to indicate whether the BAI file is for Current Day or Prior Day
    --
    -- History:
    -- Syam Palli           15-FEB-2006       Initial Creation
    --
    g_request_id        NUMBER := fnd_global.conc_request_id;
    g_user_id           NUMBER := fnd_global.user_id;
    g_login_id          NUMBER := fnd_profile.value('LOGIN_ID');
    l_file_id           NUMBER(15);
    l_msg               VARCHAR2(500);
    l_header_string     VARCHAR2(1000);
    l_bank_string       VARCHAR2(1000);
    l_trailer_string    VARCHAR2(1000);
    l_trans_file_name   VARCHAR2(100);
    l_hsi_interface_out VARCHAR2(100);
    x_return_status     VARCHAR2(1);
    x_msg_data          VARCHAR2(2000);
    pl_user_name        fnd_user.user_name%TYPE;
    pl_user_id          fnd_user.user_id%TYPE;
    pl_dflt_email       fnd_user.email_address%TYPE;
    pl_email            fnd_user.email_address%TYPE;
    pl_errormessage     VARCHAR2(4000);
    pl_errorstatus      NUMBER;
    l_value_error       VARCHAR2(200);


    /* UTL File Variable(s) */
    -- this file handle is for regular output file
    l_file_handle utl_file.file_type;
    pl_sender     VARCHAR2(100);
    pl_instance   VARCHAR2(20);
    --
    -- Cursor to select missing bank account in the Prior Day bank statements.
    --
    CURSOR bank_accts IS
      SELECT cbb.bank_name
            ,cba.bank_account_num
            ,cbb.bank_branch_name
        FROM ce.ce_bank_accounts     cba
            ,apps.ce_bank_branches_v cbb
       WHERE cba.bank_id = cbb.bank_party_id
         AND cba.bank_account_id IN
             (SELECT ba.bank_account_id
                FROM ce.ce_bank_accounts ba
              MINUS
              SELECT csh.bank_account_id
                FROM ce.ce_statement_headers csh
               WHERE trunc(csh.statement_date) =
                     xxcusce_custom_pkg.get_prev_business_day(to_char(SYSDATE,
                                                                        'DD-MON-YYYY')))
       ORDER BY cbb.bank_name;
    bank_accts_rec bank_accts%ROWTYPE;
    v_rec_count    INTEGER := 0;
  BEGIN
    BEGIN
      SELECT lower(NAME)
        INTO pl_instance
        FROM v$database;
    EXCEPTION
      WHEN OTHERS THEN
        NULL;
    END;
    pl_sender := 'Oracle.Applications_' || pl_instance ||
                 '@hughessupply.com';

    BEGIN
      OPEN bank_accts;
      FETCH bank_accts
        INTO bank_accts_rec;
      v_rec_count := bank_accts%ROWCOUNT;
      CLOSE bank_accts;
      p_rec_count := nvl(v_rec_count, 0);
      IF p_rec_count = 0 THEN
        fnd_file.put_line(fnd_file.log,
                          'All required prior day bank accounts are present in PD BAI file(s).');
        pl_errorstatus := sendmailjpkg.sendmail(smtpservername => p_host --'hsim1.hsi.hughessupply.com'
                                               , sender => pl_sender
                                                --'Oracle.Applications@hughessupply.com'
                                               ,
                                                recipient => g_default_bai_email
                                                --pl_dflt_email
                                               , ccrecipient => '',
                                                bccrecipient => '',
                                                subject => 'All Prior Day multi-bank BAI account information is present. ',
                                                BODY => 'All Prior Day multi-bank BAI account information is present. ' ||
                                                         sendmailjpkg.eol ||
                                                         'THIS IS A SEND ONLY EMAIL - DO NOT REPLY ',
                                                errormessage => pl_errormessage,
                                                attachments => NULL);
      END IF;
    END;
    IF p_rec_count > 0 THEN
      BEGIN
        fnd_client_info.set_org_context(81);
      END;
      p_file_dir := '/xx_iface/' || pl_instance || '/xtr/outbound';
      fnd_file.put_line(fnd_file.log,
                        ' The P_file_dir is ...:' || p_file_dir);
      /* Check whether file is already open, IF open close it */
      IF utl_file.is_open(l_file_handle) THEN
        /* Close file */
        utl_file.fclose(l_file_handle);
      END IF;

      BEGIN

        p_file_name := 'HSI_TR_' || 'Missing_bank_accts' || '_' ||
                       to_char(SYSDATE, 'mmddyy_hh24_mi_ss') || '.txt';

        fnd_file.put_line(fnd_file.log,
                          ' The p_file_name is ...:' || p_file_name);

        l_file_handle := utl_file.fopen(p_file_dir, p_file_name, 'w');
      EXCEPTION
        WHEN utl_file.invalid_path THEN
          raise_application_error(-20111,
                                  'INVALID PATH -- File location or filename was invalid');
      END;

      IF utl_file.is_open(l_file_handle) THEN
        l_header_string := 'The following required bank account(s) are missing from the Previous Day bank statement import file(s): ';
        utl_file.put_line(l_file_handle, l_header_string);

        l_header_string := 'Date of Report: ' ||
                           to_char(SYSDATE, 'MM/DD/YYYY HH:MI AM');
        utl_file.put_line(l_file_handle, l_header_string);

        l_header_string := rpad('Bank Name', 40, ' ') || '    ' ||
                           rpad('Account Number', 20, ' ') || '    ' ||
                           rpad('Bank Account Desc', 50, ' ') || '    ' ||
                           'CONDITION: ';
        utl_file.put_line(l_file_handle, l_header_string);

        FOR c_bank IN bank_accts
        LOOP
          x_return_status := NULL;
          l_bank_string   := rpad(c_bank.bank_name, 40, ' ') || '    ' ||
                             rpad(c_bank.bank_account_num, 20, ' ') ||
                             '    ' ||
                             rpad(c_bank.bank_branch_name, 50, ' ') ||
                             '    ' ||
                             'No summary is reported for bank account in PD BAI file.';
          utl_file.put_line(l_file_handle, l_bank_string);
        END LOOP;
        /* Close file */
        utl_file.fclose(l_file_handle);

        pl_errorstatus := sendmailjpkg.sendmail(smtpservername => p_host --'hsim1.hsi.hughessupply.com'
                                               , sender => pl_sender
                                                --'Oracle.Applications@hughessupply.com'
                                               ,
                                                recipient => g_default_bai_email
                                                --pl_dflt_email
                                               , ccrecipient => '',
                                                bccrecipient => '',
                                                subject => 'Prior Day multi-bank BAI account information is missing. ',
                                                BODY => 'Prior Day multi-bank BAI account information is missing. Please check bank statements for the accounts attached in this email. ' ||
                                                         sendmailjpkg.eol ||
                                                         'THIS IS A SEND ONLY EMAIL - DO NOT REPLY ',
                                                errormessage => pl_errormessage,
                                                attachments => sendmailjpkg.attachments_list(p_file_dir || '/' ||
                                                                                              p_file_name));

      ELSE
        l_value_error := 'Unable to Open UTL File';
      END IF;
    END IF; ------p_rec_count >0
  EXCEPTION
    WHEN utl_file.invalid_path THEN
      raise_application_error(-20100,
                              'INVALID_PATH -- File location or filename was invalid');
    WHEN utl_file.invalid_mode THEN
      raise_application_error(-20101,
                              'INVALID_MODE -- The open_mode parameter in FOPEN was invalid');
    WHEN utl_file.invalid_filehandle THEN
      raise_application_error(-20102,
                              'INVALID_FILEHANDLE -- File handle was invalid');
    WHEN utl_file.invalid_operation THEN
      raise_application_error(-20103,
                              'INVALID_OPERATION -- File could not be opened or operated on as requested');
    WHEN utl_file.read_error THEN
      raise_application_error(-20104,
                              'READ_ERROR -- Operating system error occurred during the read operation');
    WHEN utl_file.write_error THEN
      raise_application_error(-20105,
                              'WRITE_ERROR -- Operating system error occurred during the write operation');
    WHEN utl_file.internal_error THEN
      raise_application_error(-20106,
                              'INTERNAL_ERROR -- UnspecIFied PL/SQL error');
    WHEN value_error THEN
      fnd_file.put_line(fnd_file.log,
                        'IN VALUE ERROR string is : ' ||
                         substr(l_bank_string, 1, 200) || ' ' ||
                         length(l_bank_string));
      utl_file.fclose(l_file_handle);
    WHEN OTHERS THEN
      ROLLBACK;
      fnd_file.put_line(fnd_file.log,
                        ' ERROR' || SQLCODE || substr(SQLERRM, 1, 200));
      RAISE;
  END missing_pd_bank_accts;




  PROCEDURE cash_position_pd_cashflow(errbuf           OUT VARCHAR2
                                     ,retcode          OUT NUMBER
                                     ,p_worksheet_name IN VARCHAR2
                                     ,p_as_of_date     IN VARCHAR2
                                     ,p_display_debug  IN VARCHAR2
                                     ,p_debug_path     IN VARCHAR2
                                     ,p_debug_file     IN VARCHAR2) IS
    --
    -- Name:
    -- Cash_position_PD_Cashflow
    -- Purpose:
    -- To submit the standard concurrent program Cash Position Prior Day Cash Flow and Overdue Transactions Program
    -- after all the BAI loads are completed.
    -- Arguments:
    -- p_as_of_date  - Current date
    -- p_worksheet_name - Cash Worksheet Name
    --
    -- History:
    -- Syam Palli           15-FEB-2006       Initial Creation
    --
    v_req_id         NUMBER := fnd_global.conc_request_id;
    v_user_id        NUMBER := fnd_global.user_id;
    v_error_message  VARCHAR2(3000) := NULL;
    v_error_code     VARCHAR2(240) := NULL;
    v_phase          VARCHAR2(200);
    v_status         VARCHAR2(200);
    v_dev_phase      VARCHAR2(200);
    v_dev_status     VARCHAR2(200);
    v_message        VARCHAR2(3000);
    v_request_status BOOLEAN;
    pl_errorstatus   NUMBER;
    process_err EXCEPTION;
  BEGIN

    fnd_file.put_line(fnd_file.log, 'Paramaeters Values:');
    fnd_file.put_line(fnd_file.log,
                      'p_worksheet_name...... = ' || p_worksheet_name);
    fnd_file.put_line(fnd_file.log, 'p_as_of_date...... = ' || p_as_of_date);
    BEGIN
      SELECT lower(NAME)
        INTO pl_instance
        FROM v$database;
    EXCEPTION
      WHEN OTHERS THEN
        NULL;
    END;
    pl_sender := 'Oracle.Applications_' || pl_instance ||
                 '@hughessupply.com';
    --
    -- Submit the Cash Position Prior Day Cash Flow and Overdue Transactions Program
    --
    v_req_id := apps.fnd_request.submit_request(application => 'CE',
                                                program => 'CECPPRIB',
                                                description => NULL,
                                                start_time => SYSDATE,
                                                sub_request => FALSE,
                                                argument1 => p_worksheet_name,
                                                argument2 => p_as_of_date,
                                                argument3 => p_display_debug,
                                                argument4 => p_debug_path,
                                                argument5 => p_debug_file);
    COMMIT;
    fnd_file.put_line(fnd_file.log, 'After Submitting the CECPPRIB......!:');
    IF nvl(v_req_id, 0) = 0 THEN
      --- CECPPRIB
      v_error_message := 'Failed to submit Cash Position Prior Day Cash Flow and Overdue Transactions Program: ' ||
                         SQLERRM;
      v_error_code    := substr(SQLCODE, 1, 30);
      RAISE process_err;
    ELSE
      ----CECPPRIB
      v_request_status := fnd_concurrent.wait_for_request(request_id => v_req_id,
                                                          INTERVAL => 5,
                                                          max_wait => 15000,
                                                          phase => v_phase,
                                                          status => v_status,
                                                          dev_phase => v_dev_phase,
                                                          dev_status => v_dev_status,
                                                          message => v_message);
      COMMIT;
      IF (v_dev_phase = 'COMPLETE' AND v_dev_status = 'NORMAL') THEN
        ---CECPPRIB completed?
        v_error_message := 'Successfully completed the Cash Position Prior Day Cash Flow and Overdue Transactions Program. ';
        v_error_code    := substr(SQLCODE, 1, 30);
        fnd_file.put_line(fnd_file.log, v_error_message);
        pl_errorstatus := sendmailjpkg.sendmail(smtpservername => p_host --'hsim1.hsi.hughessupply.com'
                                               , sender => pl_sender
                                                ----   'Oracle.Applications@hughessupply.com'
                                               ,
                                                recipient => g_default_bai_email
                                                --pl_dflt_email
                                               , ccrecipient => '',
                                                bccrecipient => '',
                                                subject => v_error_message,
                                                BODY => v_error_message ||
                                                         sendmailjpkg.eol ||
                                                         'THIS IS A SEND ONLY EMAIL - DO NOT REPLY ',
                                                errormessage => v_error_message,
                                                attachments => NULL);
      ELSE
        v_error_message := 'Cash Position Prior Day Cash Flow and Overdue Transactions Program completed with ' ||
                           v_dev_status || ' status..:' ||
                           ' and the error message is ..: ' || SQLERRM;
        v_error_code    := substr(SQLCODE, 1, 30);
        fnd_file.put_line(fnd_file.log, v_error_message);
        pl_errorstatus := sendmailjpkg.sendmail(smtpservername => p_host --'hsim1.hsi.hughessupply.com'
                                               , sender => pl_sender
                                                ----   'Oracle.Applications@hughessupply.com'
                                               ,
                                                recipient => g_default_bai_email
                                                --pl_dflt_email
                                               , ccrecipient => '',
                                                bccrecipient => '',
                                                subject => 'Cash Position Prior Day Cash Flow and Overdue Transactions Program completed with ' ||
                                                            v_dev_status ||
                                                            ' status..:',
                                                BODY => v_error_message ||
                                                         sendmailjpkg.eol ||
                                                         'THIS IS A SEND ONLY EMAIL - DO NOT REPLY ',
                                                errormessage => v_error_message,
                                                attachments => NULL);
      END IF;
    END IF;
  EXCEPTION
    WHEN process_err THEN
      fnd_file.put_line(fnd_file.log, v_error_message);
      pl_errorstatus := sendmailjpkg.sendmail(smtpservername => p_host --'hsim1.hsi.hughessupply.com'
                                             , sender => pl_sender
                                              ----   'Oracle.Applications@hughessupply.com'
                                             , recipient => g_default_email
                                              --pl_dflt_email
                                             , ccrecipient => '',
                                              bccrecipient => '',
                                              subject => 'Failed to complete Cash Position Prior Day Cash Flow and Overdue Transactions Program. ',
                                              BODY => v_error_message ||
                                                       ' No Data will be processes for this bank statement. Please reconcile the error and re-submit the bank statement import process. ' ||
                                                       sendmailjpkg.eol ||
                                                       'THIS IS A SEND ONLY EMAIL - DO NOT REPLY ',
                                              errormessage => v_error_message,
                                              attachments => NULL);
      RAISE program_error;
    WHEN OTHERS THEN
      v_error_message := 'Cash Position Prior Day Cash Flow and Overdue Transactions Program failed due to...' ||
                         SQLERRM;
      fnd_file.put_line(fnd_file.log,
                        'Cash Position PD Cashflow Program failed due to ...' ||
                         SQLERRM);
      pl_errorstatus := sendmailjpkg.sendmail(smtpservername => p_host --'hsim1.hsi.hughessupply.com'
                                             , sender => pl_sender
                                              ----  'Oracle.Applications@hughessupply.com'
                                             , recipient => g_default_email
                                              --pl_dflt_email
                                             , ccrecipient => '',
                                              bccrecipient => '',
                                              subject => 'Cash Position Prior Day Cash Flow and Overdue Transactions Program failed...:',
                                              BODY => v_error_message ||
                                                       sendmailjpkg.eol ||
                                                       'THIS IS A SEND ONLY EMAIL - DO NOT REPLY ',
                                              errormessage => v_error_message,
                                              attachments => NULL);
      RAISE program_error;
  END cash_position_pd_cashflow;


  -- ------------------------ timed_alert ------------------------------
  --
  -- Description: This procedure is called by the application to alert
  --              associated groups with an email when a bank has not
  --              processed as of a specified time.

  PROCEDURE timed_alert(errbuf  OUT VARCHAR2
                       ,retcode OUT NUMBER) IS
    v_bai_count     NUMBER := 0;
    v_holiday       VARCHAR2(1);
    l_failure       VARCHAR2(250);
    l_expected      NUMBER := 0;
    l_success       VARCHAR2(250);
    l_body          VARCHAR2(32767);
    l_body_header   VARCHAR2(32767);
    l_body_detail   VARCHAR2(32767);
    l_body_footer   VARCHAR2(32767);
    v_error_message VARCHAR2(3000) := NULL;
    v_host          VARCHAR2(256) := 'hsim1.hsi.hughessupply.com';
    v_errorstatus   NUMBER;

    -- EMAIL VARIABLES
    l_dflt_email fnd_user.email_address%TYPE := 'Treasury_Oracle_Tech@hughessupply.com';
    l_sender     VARCHAR2(100);
    l_sid        VARCHAR2(3);


  BEGIN
    --
    -- Local variables
    --
    SELECT lower(NAME)
      INTO l_sid
      FROM v$database;
    l_sender := 'Oracle.Applications_' || l_sid || '@hdsupply.com';
    --
    -- Logic flow :copied from PD load files.



    BEGIN
      -- If yesterday is a holiday then process statement 1, else process statment 2
      SELECT 'T'
        INTO v_holiday -- IF 'T', then statement 1 will process
        FROM xtr.xtr_holidays
       WHERE (holiday_date = trunc(SYSDATE - 1));
    EXCEPTION
      WHEN no_data_found THEN
        IF (to_char(SYSDATE, 'dy') = 'mon') THEN
          --If today is a Monday, process like a holiday
          v_holiday := 'T';
        ELSE
          v_holiday := 'F'; -- IF 'F', then statement 2 will process
        END IF;
    END;



    IF v_holiday = 'F' THEN
      -- Not a holiday, normal check.
      BEGIN

        -- DEBUG NAME
        g_err_callpoint := 'Non-holiday processing';
        -- DEBUG NAME

        l_expected := g_total_banks * 2;

        SELECT COUNT(*)
          INTO v_bai_count
          FROM xxcus.xxcusce_bank_file_trg
         WHERE upper(bank_name) IN
               (SELECT meaning
                  FROM fnd_lookup_values
                 WHERE lookup_type = 'XXHSI_BANKS')
           AND status = 'S'
           AND creation_date = to_char(SYSDATE, 'YYYY-MM-DD');

      EXCEPTION
        WHEN no_data_found THEN
          v_bai_count := 0;
        WHEN OTHERS THEN
          v_error_message := 'Error in ' || g_err_callpoint ||
                             '.  Please check error table for more info.';
          fnd_file.put_line(fnd_file.log, v_error_message);

          xxcus_error_pkg.xxcus_error_main_api(p_called_from => g_err_callfrom,
                                               p_calling => g_err_callpoint,
                                               p_ora_error_msg => SQLERRM,
                                               p_error_desc => 'Error getting counts for' ||
                                                                g_err_callpoint,
                                               p_distribution_list => g_distro_list);
          RAISE;


      END;

    ELSE

      BEGIN
        --   Holiday yesterday!
        -- DEBUG NAME
        g_err_callpoint := 'Holiday processing';
        -- DEBUG NAME

        l_expected := g_total_banks;

        SELECT COUNT(*) -- no prior days needed
          INTO v_bai_count
          FROM xxcus.xxcusce_bank_file_trg
         WHERE upper(bank_name) IN
               (SELECT meaning
                  FROM fnd_lookup_values
                 WHERE lookup_type = 'XXHSI_BANKS')
           AND status = 'S'
           AND creation_date = to_char(SYSDATE, 'YYYY-MM-DD');

      EXCEPTION
        WHEN no_data_found THEN
          v_bai_count := 0;
        WHEN OTHERS THEN
          v_error_message := 'Error in ' || g_err_callpoint ||
                             '.  Please check error table for more info.';
          fnd_file.put_line(fnd_file.log, v_error_message);

          xxcus_error_pkg.xxcus_error_main_api(p_called_from => g_err_callfrom,
                                               p_calling => g_err_callpoint,
                                               p_ora_error_msg => SQLERRM,
                                               p_error_desc => 'Error getting counts for' ||
                                                                g_err_callpoint,
                                               p_distribution_list => g_distro_list);
          RAISE;

      END;


    END IF;


    --  INSERT CODE For TIMED CHECKER!!!!!!!!!

    --  If v_bai_count <= g_total_banks and it's 12:30pm then run the alert email.

    IF v_bai_count < l_expected THEN
      ---- Change the 1 to 5 when all the banks are setup correctly
      --

      -- Build email if necessary

      l_body_header := chr(10) ||
                       'TREASURY PROCESSING REPORT  - Potential Problems' ||
                       chr(10) || chr(10) ||
                       'BANK NAME                  STATUS       FILE TYPE               ' ||
                       chr(10) ||
                       '==================================================' ||
                       chr(10);

      l_body_detail := l_body_detail || chr(10) || '<FAILURES>' || chr(10);
      -- Code for building body of email.

      -- Failure CD part of the body
      FOR c_pd_failures IN (SELECT DISTINCT (bank_name)
                                           ,status
                                           ,pd_or_cd_flag
                              FROM xxcus.xxcusce_bank_file_trg
                             WHERE status = 'N'
                               AND creation_date =
                                   to_char(SYSDATE, 'YYYY-MM-DD')
                               AND pd_or_cd_flag = 'PD'
                               AND bank_name NOT IN
                                   (SELECT DISTINCT (a.bank_name)
                                      FROM xxcus.xxcusce_bank_file_trg a
                                     WHERE a.status = 'S'
                                       AND a.pd_or_cd_flag = 'PD'
                                       AND a.creation_date =
                                           to_char(SYSDATE, 'YYYY-MM-DD')))
      LOOP

        l_body_detail := l_body_detail ||
                         rpad(c_pd_failures.bank_name, 29, ' ') ||
                         rpad(c_pd_failures.status, 15, ' ') ||
                         rpad(c_pd_failures.pd_or_cd_flag, 15, ' ') ||
                         chr(10);

      END LOOP;

      -- Failure CD part of the body
      FOR c_cd_failures IN (SELECT DISTINCT (bank_name)
                                           ,status
                                           ,pd_or_cd_flag
                              FROM xxcus.xxcusce_bank_file_trg
                             WHERE status = 'N'
                               AND creation_date =
                                   to_char(SYSDATE, 'YYYY-MM-DD')
                               AND pd_or_cd_flag = 'CD'
                               AND bank_name NOT IN
                                   (SELECT DISTINCT (a.bank_name)
                                      FROM xxcus.xxcusce_bank_file_trg a
                                     WHERE a.status = 'S'
                                       AND a.pd_or_cd_flag = 'CD'
                                       AND a.creation_date =
                                           to_char(SYSDATE, 'YYYY-MM-DD')))
      LOOP

        l_body_detail := l_body_detail ||
                         rpad(c_cd_failures.bank_name, 29, ' ') ||
                         rpad(c_cd_failures.status, 15, ' ') ||
                         rpad(c_cd_failures.pd_or_cd_flag, 15, ' ') ||
                         chr(10);

      END LOOP;
      l_body_detail := l_body_detail || chr(10) || '<SUCCESSES>' || chr(10);

      -- Success part of the body
      FOR c_sucesses IN (SELECT DISTINCT (bank_name)
                                        ,status
                                        ,pd_or_cd_flag
                           FROM xxcus.xxcusce_bank_file_trg
                          WHERE status = 'S'
                            AND creation_date =
                                to_char(SYSDATE, 'YYYY-MM-DD')
                          ORDER BY pd_or_cd_flag)

      LOOP
        l_body_detail := l_body_detail ||
                         rpad(c_sucesses.bank_name, 29, ' ') ||
                         rpad(c_sucesses.status, 15, ' ') ||
                         rpad(c_sucesses.pd_or_cd_flag, 15, ' ') || chr(10);

      END LOOP;

      l_body_footer := chr(10) ||
                       'Possible failure for Bank File Feeds.  Expected count at this time not received.' ||
                       chr(10) || 'Only ' || to_char(v_bai_count) ||
                       ' of the ' || to_char(l_expected) ||
                       ' expected successful feeds were received.';

      --   Put it all together and shake it all about, that's what it's all about!
      l_body := l_body_header || l_body_detail || l_body_footer;

      -- Send email if time elapsed is allowed.

      v_errorstatus := sendmailjpkg.sendmail(smtpservername => v_host --'hsim1.hsi.hughessupply.com'
                                            , sender => l_sender
                                             --'Oracle.Applications@hughessupply.com'
                                            , recipient => l_dflt_email,
                                             ccrecipient => '',
                                             bccrecipient => '',
                                             subject => '********TREASURY ALERT****TREASURY ALERT*********',
                                             BODY => l_body,
                                             errormessage => v_error_message);
    END IF;


  END timed_alert;

  --  ================================================================================
  --  SR #2833   Check for bank transaction detail 16 and transaction type code 115 to
  --             merge am and pm deposits and correct the lockbox number removing the
  --             last two digits that represent the am or pm deposit.
  --  ================================================================================
  --  VERSION    DATE            AUTHOR(S)         DESCRIPTION
  --  -------    -------------   ---------------   ----------------------------------
  --  1.0        16-APR-2009     Kathy Poling       Created
  --  ********************************************************************************
  PROCEDURE update_bai_file_115(errbuf  OUT VARCHAR2
                               ,retcode OUT NUMBER) IS
  BEGIN

    FOR getamt IN (SELECT bank_account_num
                         ,statement_number
                         ,trx_date
                         ,substr(customer_text, 1, 6) customer_text
                         ,SUM(amount) amount
                     FROM ce_statement_lines_interface
                    WHERE trunc(creation_date) = trunc(SYSDATE)
                      AND trx_code = '115'
                      AND (bank_account_num, statement_number,
                           substr(customer_text, 1, 6)) IN
                          (SELECT bank_account_num
                                 ,statement_number
                                 ,substr(customer_text, 1, 6)
                             FROM ce_statement_lines_interface
                           HAVING COUNT(*) > 1
                            GROUP BY bank_account_num
                                    ,statement_number
                                    ,substr(customer_text, 1, 6))
                    GROUP BY bank_account_num
                            ,statement_number
                            ,trx_date
                            ,substr(customer_text, 1, 6))
    LOOP

      UPDATE ce_statement_lines_interface
         SET amount = getamt.amount
       WHERE statement_number = getamt.statement_number
         AND bank_account_num = getamt.bank_account_num
         AND trx_date = getamt.trx_date
         AND substr(customer_text, 1, 6) = getamt.customer_text
         AND trx_code = '115';

    END LOOP;

    COMMIT;

    FOR gettrx IN (SELECT bank_account_num
                         ,statement_number
                         ,trx_date
                         ,customer_text
                     FROM ce_statement_lines_interface
                    WHERE trunc(creation_date) = trunc(SYSDATE)
                      AND trx_code = '115'
                      AND substr(customer_text, -2) <> '01'
                      AND (bank_account_num, statement_number,
                           substr(customer_text, 1, 6)) IN
                          (SELECT bank_account_num
                                 ,statement_number
                                 ,substr(customer_text, 1, 6)
                             FROM ce_statement_lines_interface
                           HAVING COUNT(*) > 1
                            GROUP BY bank_account_num
                                    ,statement_number
                                    ,substr(customer_text, 1, 6)))

    LOOP

      DELETE FROM ce_statement_lines_interface
       WHERE trx_code = '115'
         AND statement_number = gettrx.statement_number
         AND bank_account_num = gettrx.bank_account_num
         AND trx_date = gettrx.trx_date
         AND customer_text = gettrx.customer_text;

    END LOOP;

    COMMIT;

    UPDATE ce_statement_lines_interface
       SET customer_text   = substr(customer_text, 1, 6)
          ,bank_trx_number = substr(customer_text, 1, 6)
     WHERE trunc(creation_date) = trunc(SYSDATE)
       AND trx_code = '115';

    COMMIT;

  EXCEPTION

    WHEN program_error THEN
      ROLLBACK;
      fnd_file.put_line(fnd_file.output, SQLERRM);

      xxcus_error_pkg.xxcus_error_main_api(p_called_from => g_err_callfrom,
                                           p_calling => g_err_callpoint,
                                           p_ora_error_msg => SQLERRM,
                                           p_error_desc => 'Error updating trns code 115' ||
                                                            g_err_callpoint,
                                           p_distribution_list => g_distro_list);

    WHEN OTHERS THEN
      ROLLBACK;
      fnd_file.put_line(fnd_file.output, SQLERRM);

      xxcus_error_pkg.xxcus_error_main_api(p_called_from => g_err_callfrom,
                                           p_calling => g_err_callpoint,
                                           p_ora_error_msg => SQLERRM,
                                           p_error_desc => 'Error Error updating trns code 115' ||
                                                            g_err_callpoint,
                                           p_distribution_list => g_distro_list);
      COMMIT;
  END update_bai_file_115;

  --  ================================================================================
  --  SR # 21140 Global Bank Account Reconciliation - need to load bank files into
  --             another org to allow users to reconcile the bank files but continue
  --             to allow them to create JE's when the bank files are first loaded into
  --             org 81.  Users don't want to wait two days for files being received
  --             from Eclipse to reconcile and then run the JE (Once a JE has been run
  --             Oracle will reconcile the account with that JE - standard Oracle)
  --  ================================================================================
  --  VERSION    DATE            AUTHOR(S)         DESCRIPTION
  --  -------    -------------   ---------------   ----------------------------------
  --  1.0        21-Oct-2009     Kathy Poling       Created
  --  ********************************************************************************
  PROCEDURE insert_bank_recon(errbuf  OUT VARCHAR2
                             ,retcode OUT NUMBER) IS

    v_error_code    VARCHAR2(240) := NULL;
    v_error_message VARCHAR2(3000) := NULL;
    l_bank_org CONSTANT ap.ap_bank_accounts_all.org_id%TYPE := 322; --bank org for global recon

  BEGIN

    -- DEBUG NAME
    g_err_callpoint := 'Inserting bank file for global reconciliation';
    -- DEBUG NAME

    FOR get_bank IN (SELECT statement_number
                           ,bank_account_num
                           ,statement_date
                           ,bank_name
                           ,bank_branch_name
                           ,control_begin_balance
                           ,control_total_dr
                           ,control_total_cr
                           ,control_end_balance
                           ,control_dr_line_count
                           ,control_cr_line_count
                           ,control_line_count
                           ,currency_code
                           ,attribute_category
                           ,attribute1
                           ,attribute2
                           ,attribute3
                           ,attribute4
                           ,attribute5
                           ,attribute6
                           ,attribute7
                           ,attribute8
                           ,attribute9
                           ,attribute10
                           ,attribute11
                           ,attribute12
                           ,attribute13
                           ,attribute14
                           ,attribute15
                           ,check_digits
                           ,cashflow_balance
                           ,int_calc_balance
                           ,intra_day_flag
                           ,one_day_float
                           ,two_day_float
                       FROM ce.ce_statement_headers_int
                      WHERE trunc(creation_date) = trunc(SYSDATE)
                        AND bank_account_num IN
                            (SELECT rtrim(bank_account_num, 'RECON')
                               FROM ap.ap_bank_accounts_all
                              WHERE org_id IN
                                    (SELECT description
                                       FROM fnd_lookup_values
                                      WHERE lookup_type =
                                            'XXCUS_OCM_INTERFACE_PKG'
                                        AND meaning LIKE 'BANK_ORG_%')))
    LOOP

      INSERT INTO ce.ce_statement_headers_int
        (statement_number
        ,bank_account_num
        ,statement_date
        ,bank_name
        ,bank_branch_name
        ,control_begin_balance
        ,control_total_dr
        ,control_total_cr
        ,control_end_balance
        ,control_dr_line_count
        ,control_cr_line_count
        ,control_line_count
        ,record_status_flag
        ,currency_code
        ,created_by
        ,creation_date
        ,attribute_category
        ,attribute1
        ,attribute2
        ,attribute3
        ,attribute4
        ,attribute5
        ,attribute6
        ,attribute7
        ,attribute8
        ,attribute9
        ,attribute10
        ,attribute11
        ,attribute12
        ,attribute13
        ,attribute14
        ,attribute15
        ,last_updated_by
        ,last_update_date
        ,org_id
        ,check_digits
        ,cashflow_balance
        ,int_calc_balance
        ,intra_day_flag
        ,one_day_float
        ,two_day_float)
      VALUES
        (get_bank.statement_number
        ,get_bank.bank_account_num || 'RECON'
        ,get_bank.statement_date
        ,get_bank.bank_name
        ,get_bank.bank_branch_name
        ,get_bank.control_begin_balance
        ,get_bank.control_total_dr
        ,get_bank.control_total_cr
        ,get_bank.control_end_balance
        ,get_bank.control_dr_line_count
        ,get_bank.control_cr_line_count
        ,get_bank.control_line_count
        ,NULL
        ,get_bank.currency_code
        ,-1
        ,SYSDATE
        ,get_bank.attribute_category
        ,get_bank.attribute1
        ,get_bank.attribute2
        ,get_bank.attribute3
        ,get_bank.attribute4
        ,get_bank.attribute5
        ,get_bank.attribute6
        ,get_bank.attribute7
        ,get_bank.attribute8
        ,get_bank.attribute9
        ,get_bank.attribute10
        ,get_bank.attribute11
        ,get_bank.attribute12
        ,get_bank.attribute13
        ,get_bank.attribute14
        ,get_bank.attribute15
        ,-1
        ,SYSDATE
        ,l_bank_org
        ,get_bank.check_digits
        ,get_bank.cashflow_balance
        ,get_bank.int_calc_balance
        ,get_bank.intra_day_flag
        ,get_bank.one_day_float
        ,get_bank.two_day_float);

      FOR get_bank_lines IN (SELECT bank_account_num
                                   ,statement_number
                                   ,line_number
                                   ,trx_date
                                   ,trx_code
                                   ,effective_date
                                   ,trx_text
                                   ,invoice_text
                                   ,bank_account_text
                                   ,amount
                                   ,currency_code
                                   ,user_exchange_rate_type
                                   ,exchange_rate_date
                                   ,exchange_rate
                                   ,original_amount
                                   ,charges_amount
                                   ,bank_trx_number
                                   ,customer_text
                                   ,attribute_category
                                   ,attribute1
                                   ,attribute2
                                   ,attribute3
                                   ,attribute4
                                   ,attribute5
                                   ,attribute6
                                   ,attribute7
                                   ,attribute8
                                   ,attribute9
                                   ,attribute10
                                   ,attribute11
                                   ,attribute12
                                   ,attribute13
                                   ,attribute14
                                   ,attribute15
                               FROM ce.ce_statement_lines_interface
                              WHERE bank_account_num =
                                    get_bank.bank_account_num
                                AND statement_number =
                                    get_bank.statement_number)
      LOOP

        INSERT INTO ce.ce_statement_lines_interface
          (bank_account_num
          ,statement_number
          ,line_number
          ,trx_date
          ,trx_code
          ,effective_date
          ,trx_text
          ,invoice_text
          ,bank_account_text
          ,amount
          ,currency_code
          ,user_exchange_rate_type
          ,exchange_rate_date
          ,exchange_rate
          ,original_amount
          ,charges_amount
          ,bank_trx_number
          ,customer_text
          ,created_by
          ,creation_date
          ,last_updated_by
          ,last_update_date
          ,attribute_category
          ,attribute1
          ,attribute2
          ,attribute3
          ,attribute4
          ,attribute5
          ,attribute6
          ,attribute7
          ,attribute8
          ,attribute9
          ,attribute10
          ,attribute11
          ,attribute12
          ,attribute13
          ,attribute14
          ,attribute15)
        VALUES
          (get_bank_lines.bank_account_num || 'RECON'
          ,get_bank_lines.statement_number
          ,get_bank_lines.line_number
          ,get_bank_lines.trx_date
          ,get_bank_lines.trx_code
          ,get_bank_lines.effective_date
          ,get_bank_lines.trx_text
          ,get_bank_lines.invoice_text
          ,get_bank_lines.bank_account_text
          ,get_bank_lines.amount
          ,get_bank_lines.currency_code
          ,get_bank_lines.user_exchange_rate_type
          ,get_bank_lines.exchange_rate_date
          ,get_bank_lines.exchange_rate
          ,get_bank_lines.original_amount
          ,get_bank_lines.charges_amount
          ,get_bank_lines.bank_trx_number
          ,get_bank_lines.customer_text
          ,-1
          ,SYSDATE
          ,-1
          ,SYSDATE
          ,get_bank_lines.attribute_category
          ,get_bank_lines.attribute1
          ,get_bank_lines.attribute2
          ,get_bank_lines.attribute3
          ,get_bank_lines.attribute4
          ,get_bank_lines.attribute5
          ,get_bank_lines.attribute6
          ,get_bank_lines.attribute7
          ,get_bank_lines.attribute8
          ,get_bank_lines.attribute9
          ,get_bank_lines.attribute10
          ,get_bank_lines.attribute11
          ,get_bank_lines.attribute12
          ,get_bank_lines.attribute13
          ,get_bank_lines.attribute14
          ,get_bank_lines.attribute15);

      END LOOP;
    END LOOP;

    -- Check for bank transaction detail 16 and transaction type code 115 to merge am and pm deposits
    update_bai_file_115(v_error_code, v_error_message);

  EXCEPTION

    WHEN program_error THEN
      ROLLBACK;
      fnd_file.put_line(fnd_file.output, SQLERRM);

      xxcus_error_pkg.xxcus_error_main_api(p_called_from => g_err_callfrom,
                                           p_calling => g_err_callpoint,
                                           p_ora_error_msg => SQLERRM,
                                           p_error_desc => 'Error insert bank recon' ||
                                                            g_err_callpoint,
                                           p_distribution_list => g_distro_list);

    WHEN OTHERS THEN
      ROLLBACK;
      fnd_file.put_line(fnd_file.output, SQLERRM);

      xxcus_error_pkg.xxcus_error_main_api(p_called_from => g_err_callfrom,
                                           p_calling => g_err_callpoint,
                                           p_ora_error_msg => SQLERRM,
                                           p_error_desc => 'Error insert bank recon' ||
                                                            g_err_callpoint,
                                           p_distribution_list => g_distro_list);
      COMMIT;
  END insert_bank_recon;

  PROCEDURE uc4_wrapper(errbuf                OUT VARCHAR2
                       ,retcode               OUT NUMBER
                       ,p_bank_file_name      IN VARCHAR2
                       ,p_bank_name           IN VARCHAR2
                       ,p_directory_name      IN VARCHAR2
                       ,p_user_name           IN VARCHAR2
                       ,p_responsibility_name IN VARCHAR2
                       ,p_bank_stmt_map       IN VARCHAR2) IS
    --
    -- Package Variables
    --

    l_package    VARCHAR2(50) := 'xxcusce_bai_interface_pkg';
    l_dflt_email VARCHAR2(200) := 'HDSOracleDevelopers@hdsupply.com';
    l_email      fnd_user.email_address%TYPE;

    l_req_id              NUMBER NULL;
    v_phase               VARCHAR2(50);
    v_status              VARCHAR2(50);
    v_dev_status          VARCHAR2(50);
    v_dev_phase           VARCHAR2(50);
    v_message             VARCHAR2(250);
    v_error_message       VARCHAR2(3000);
    v_supplier_id         NUMBER;
    v_rec_cnt             NUMBER := 0;
    l_message             VARCHAR2(150);
    l_errormessage        VARCHAR2(3000);
    pl_errorstatus        NUMBER;
    l_can_submit_request  BOOLEAN := TRUE;
    l_globalset           VARCHAR2(100);
    l_err_msg             VARCHAR2(3000);
    l_err_code            NUMBER;
    l_sec                 VARCHAR2(255);
    l_statement           VARCHAR2(9000);
    l_user                fnd_user.user_id%TYPE; --REBTINTERFACE user
    l_user_id             NUMBER;
    l_responsibility_id   NUMBER;
    l_resp_application_id NUMBER;

    -- Error DEBUG
    l_err_callfrom  VARCHAR2(75) DEFAULT 'xxcus_error_pkg';
    l_err_callpoint VARCHAR2(75) DEFAULT 'START';
    l_distro_list   VARCHAR2(75) DEFAULT 'HDSOracleDevelopers@hdsupply.com';
    l_file_exists_cnt    NUMBER := 0;
    file_processed       EXCEPTION;
  BEGIN

    --------------------------------------------------------------------------
    -- Check if the file is already processed in Oracle
    --------------------------------------------------------------------------
     BEGIN
       SELECT count(1)
         INTO l_file_exists_cnt
         FROM fnd_concurrent_requests             fcr
            , fnd_concurrent_programs_vl          fcpv
        WHERE fcr.concurrent_program_id         = fcpv.concurrent_program_id
          AND fcpv.user_concurrent_program_name = 'HDS Cash Management Previous Day BAI Load and Import Process'
          AND fcr.argument2                     = p_bank_name
          AND fcr.argument1                     = p_bank_file_name
          AND fcr.status_code                   = 'C';
     EXCEPTION
     WHEN OTHERS THEN
     fnd_file.put_line(fnd_file.log, 'Error - '||SQLERRM);
     l_file_exists_cnt := 0;
     END;

     IF l_file_exists_cnt > 0 THEN
       RAISE file_processed;
     END IF;

    --------------------------------------------------------------------------
    -- Deriving UserId
    --------------------------------------------------------------------------
    BEGIN
      SELECT user_id
        INTO l_user_id
        FROM fnd_user
       WHERE 1 = 1
         AND user_name = upper(p_user_name)
         AND SYSDATE BETWEEN start_date AND
             nvl(end_date, trunc(SYSDATE) + 1);
    EXCEPTION
      WHEN no_data_found THEN
        l_err_msg := 'UserName - ' || p_user_name ||
                     ' not defined in Oracle';
        RAISE program_error;
      WHEN OTHERS THEN
        l_err_msg := 'Error deriving user_id for UserName - ' ||
                     p_user_name;
        RAISE program_error;
    END;

    --------------------------------------------------------------------------
    -- Deriving ResponsibilityId and ResponsibilityApplicationId
    --------------------------------------------------------------------------
    BEGIN
      SELECT responsibility_id
            ,application_id
        INTO l_responsibility_id
            ,l_resp_application_id
        FROM fnd_responsibility_vl
       WHERE responsibility_name = p_responsibility_name
         AND SYSDATE BETWEEN start_date AND
             nvl(end_date, trunc(SYSDATE) + 1);
    EXCEPTION
      WHEN no_data_found THEN
        l_err_msg := 'Responsibility - ' || p_responsibility_name ||
                     ' not defined in Oracle';
        RAISE program_error;
      WHEN OTHERS THEN
        l_err_msg := 'Error deriving Responsibility_id for ResponsibilityName - ' ||
                     p_responsibility_name;
        RAISE program_error;
    END;

    l_sec := 'UC4 call to run concurrent request ' || p_bank_name ||
             ' BAI Load File Process.';

    --------------------------------------------------------------------------
    -- Apps Initialize
    --------------------------------------------------------------------------
    --  FND_GLOBAL.APPS_INITIALIZE (57170, 50232, 20003);
    fnd_global.apps_initialize(l_user_id, l_responsibility_id,
                               l_resp_application_id);

    --------------------------------------------------------------------------
    -- Submit "HDS Cash Management Previous Day BAI Load and Import Process"
    --------------------------------------------------------------------------
    l_req_id := fnd_request.submit_request(application => 'XXCUS',
                                           program => 'XXCUSBANKSTMTIMPORT',
                                           description => NULL,
                                           start_time => SYSDATE,
                                           sub_request => FALSE,
                                           argument1 => p_bank_file_name,
                                           argument2 => p_bank_name,
                                           argument3 => p_directory_name,
                                           argument4 => p_bank_stmt_map);

    COMMIT;
    dbms_output.put_line('After fnd_request');
    IF (l_req_id != 0) THEN
      IF fnd_concurrent.wait_for_request(l_req_id, 6, 15000, v_phase,
                                         v_status, v_dev_phase, v_dev_status,
                                         v_message) THEN
        v_error_message := chr(10) || 'ReqID=' || l_req_id || ' DPhase ' ||
                           v_dev_phase || ' DStatus ' || v_dev_status ||
                           chr(10) || ' MSG - ' || v_message;
        -- Error Returned
        IF v_dev_phase != 'COMPLETE'
           OR v_dev_status != 'NORMAL' THEN
          l_statement := 'An error occured running the xxcusce_bai_interface_pkg Bank BAI Load Process' ||
                         v_error_message || '.';
          fnd_file.put_line(fnd_file.log, l_statement);
          fnd_file.put_line(fnd_file.output, l_statement);
          RAISE program_error;
        END IF;
        -- Then Success!
      ELSE
        l_statement := 'An error occured running the xxcusce_bai_interface_pkg Bank BAI Load Process' ||
                       v_error_message || '.';
        fnd_file.put_line(fnd_file.log, l_statement);
        fnd_file.put_line(fnd_file.output, l_statement);
        RAISE program_error;
      END IF;

    ELSE
      l_statement := 'An error occured running the xxcusce_bai_interface_pkg Bank BAI Load Process';
      fnd_file.put_line(fnd_file.log, l_statement);
      fnd_file.put_line(fnd_file.output, l_statement);
      RAISE program_error;
    END IF;

    dbms_output.put_line(l_sec);
    dbms_output.put_line('Request ID:  ' || l_req_id);
  EXCEPTION
    WHEN file_processed THEN
      fnd_file.put_line(fnd_file.output, 'File - '||p_bank_file_name||' is already processed');
      fnd_file.put_line(fnd_file.log, 'File - '||p_bank_file_name||' is already processed');
      dbms_output.put_line('File - '||p_bank_file_name||' is already processed');
    WHEN program_error THEN
      ROLLBACK;
      l_err_code := 2;
      l_err_msg  := l_sec;
      l_err_msg  := l_err_msg || ' ERROR ' || substr(SQLERRM, 1, 2000);
      dbms_output.put_line(l_err_msg);
      fnd_file.put_line(fnd_file.log, l_err_msg);
      fnd_file.put_line(fnd_file.output, l_err_msg);
      l_err_callpoint := l_message;

      xxcus_error_pkg.xxcus_error_main_api(p_called_from => l_err_callfrom,
                                           p_calling => l_err_callpoint,
                                           p_request_id => l_req_id,
                                           p_ora_error_msg => SQLERRM,
                                           p_error_desc => 'Error running xxcusce_bai_interface_pkg package with PROGRAM ERROR',
                                           p_distribution_list => l_distro_list,
                                           p_module => 'AP');

      fnd_file.put_line(fnd_file.output, 'Fix the error!');
    WHEN OTHERS THEN
      fnd_file.put_line(fnd_file.log, l_sec);
      fnd_file.put_line(fnd_file.output, l_sec);
      l_err_code := 2;
      l_err_msg  := l_sec;
      l_err_msg  := l_sec || l_err_msg || ' ERROR ' || SQLCODE ||
                    substr(SQLERRM, 1, 2000);
      dbms_output.put_line(l_err_msg);
      fnd_file.put_line(fnd_file.log, l_err_msg);
      fnd_file.put_line(fnd_file.output, l_err_msg);
      l_err_callpoint := l_message;

      xxcus_error_pkg.xxcus_error_main_api(p_called_from => l_err_callfrom,
                                           p_calling => l_err_callpoint,
                                           p_request_id => l_req_id,
                                           p_ora_error_msg => substr(l_err_msg,
                                                                      1, 2000),
                                           p_error_desc => 'Error running xxcusce_bai_interface_pkg package with OTHERS Exception',
                                           p_distribution_list => l_distro_list,
                                           p_module => 'AP');

  END uc4_wrapper;

END xxcusce_bai_interface_pkg;
/