------------------------------------------------------------------------------------
/******************************************************************************
   NAME       :  APPS.EIS_XXWC_PO_AUTO_REAPPRVD_PO_V
   REVISIONS:
	Ver         Date            Author             Description
   ---------  ----------    ---------------  ------------------------------------
    1.0       	26-May-2017     Siva 	            Initial Version --TMS#20170523-00014   		  		 
******************************************************************************/
CREATE OR REPLACE VIEW APPS.EIS_XXWC_PO_AUTO_REAPPRVD_PO_V (PO_NUMBER, STATUS, REVISION_NUM, CREATION_DATE, APPROVED_DATE, BUYER, VENDOR_NAME, VENDOR_NUMBER, VENDOR_SITE_CODE, VENDOR_CONTACT, BUYER_APPROVAL_LIMIT, PO_AMOUNT, PO_PREVIOUS_AMOUNT, ORIGINAL_PO_AMOUNT, SHIP_TO_LOCATION, BRANCH, DISTRICT, REGION)
AS
  SELECT poh.segment1 po_number,
    xxeis.eis_po_xxwc_pur_det_rec_po_pkg.get_authorization_status(NVL(poh.authorization_status,'INCOMPLETE'))
    || DECODE (poh.cancel_flag, 'Y', ', '
    || xxeis.eis_po_xxwc_pur_det_rec_po_pkg.get_lookup_meaning('CANCELLED','DOCUMENT STATE'), NULL)
    || DECODE (NVL (poh.closed_code, 'OPEN'), 'OPEN', NULL, ', '
    || xxeis.eis_po_xxwc_pur_det_rec_po_pkg.get_lookup_meaning(NVL(poh.closed_code,'OPEN'),'DOCUMENT STATE'))
    || DECODE (poh.frozen_flag, 'Y', ', '
    || xxeis.eis_po_xxwc_pur_det_rec_po_pkg.get_lookup_meaning('FROZEN','DOCUMENT STATE'), NULL)
    || DECODE (poh.user_hold_flag, 'Y', ', '
    || xxeis.eis_po_xxwc_pur_det_rec_po_pkg.get_lookup_meaning('ON HOLD','DOCUMENT STATE'), NULL) status ,
    poh.revision_num,
    TRUNC(poh.creation_date) creation_date,
    TRUNC(poh.approved_date) approved_date,
    papf.full_name buyer,
    pov.vendor_name,
    pov.segment1 vendor_number,
    pvs.vendor_site_code,
    DECODE(poh.vendor_contact_id, NULL, NULL,DECODE(hp.person_first_name,NULL,hp.person_last_name, hp.person_last_name
    ||', '
    || hp.person_first_name)) vendor_contact,
    (SELECT cr.amount_limit
    FROM apps.po_position_controls a,
      apps.po_control_groups ag,
      apps.po_control_functions cf,
      apps.po_control_rules cr
    WHERE a.control_group_id     = ag.control_group_id
    AND a.control_function_id    = cf.control_function_id
    AND cf.control_function_name = 'Approve Standard Purchase Orders'
    AND cr.object_code           = 'DOCUMENT_TOTAL'
    AND (sysdate BETWEEN a.start_date AND NVL (a.end_date, sysdate + 1))
    AND cr.control_group_id = a.control_group_id
    AND ( ''               IS NULL
    OR cr.object_code      IN
      (SELECT lookup_code
      FROM po_lookup_codes
      WHERE lookup_type = 'CONTROLLED_OBJECT'
      AND displayed_field LIKE ''
      ))
    AND cf.enabled_flag = 'Y'
    AND AG.ENABLED_FLAG = 'Y'
    AND A.POSITION_ID   = PAAF.POSITION_ID
    AND rownum          =1
    ) buyer_approval_limit,
    apps.po_core_s.get_total ('H', poh.po_header_id) po_amount,
    apps.po_core_s.get_archive_total_for_any_rev (poh.po_header_id, 'H', 'PO', 'STANDARD', poh.revision_num - 1, 'Y') po_previous_amount,
	apps.po_core_s.get_archive_total_for_any_rev (poh.po_header_id,'H','PO','STANDARD',0,'Y') original_po_amount,
    hrl1.location_code ship_to_location,
    mp.organization_code branch,
    mp.attribute8 district,
    mp.attribute9 region
  FROM po_headers poh,
    hr_locations_all_tl hrl1,
    per_all_people_f papf,
    po_vendors pov,
    po_vendor_sites_all pvs,
    po_vendor_contacts pvc,
    hz_parties hp,
    fnd_user fu,
    mtl_parameters mp,
    per_all_assignments_f paaf
  WHERE poh.ship_to_location_id = hrl1.location_id (+)
  AND hrl1.language(+)          = userenv('LANG')
  AND poh.agent_id              = papf.person_id(+)
  AND TRUNC(sysdate) BETWEEN papf.effective_start_date(+) AND papf.effective_end_date(+)
  AND poh.vendor_id                     = pov.vendor_id (+)
  AND poh.vendor_site_id                = pvs.vendor_site_id (+)
  AND poh.vendor_contact_id             = pvc.vendor_contact_id(+)
  AND poh.vendor_site_id                = pvc.vendor_site_id (+)
  AND pvc.per_party_id                  = hp.party_id(+)
  AND poh.revision_num                  > 0
  AND NVL (poh.cancel_flag, 'N')        = 'N'
  AND poh.authorization_status          = 'APPROVED'
  AND poh.type_lookup_code              = 'STANDARD'
  AND poh.org_id                        = 162
  AND poh.agent_id                      = fu.employee_id
  AND poh.agent_id                      = paaf.person_id
  AND SUBSTR (hrl1.location_code, 1, 3) = mp.organization_code
  AND fu.user_id                        =
    (SELECT last_updated_by
    FROM apps.po_headers_archive_all
    WHERE po_header_id       = poh.po_header_id
    AND revision_num         = poh.revision_num
    AND latest_external_flag = 'Y'
    )
/
