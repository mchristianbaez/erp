--Report Name            : HDS - FM Oracle Consolidated Billing Details
--Import Instructions    : Run this file in XXEIS user from SQL*Plus or any tool like Toad, SQL Navigator. 
prompt Creating Object Data EIS_XXWC_GL_CONSOLIDATE_BILL_V
set scan off define off
DECLARE
mod_exist VARCHAR2(1);
BEGIN 
mod_exist := XXEIS.EIS_RSC_INSERT.check_if_module_exist(101);
IF mod_exist = 'Y' THEN 
--Inserting Object EIS_XXWC_GL_CONSOLIDATE_BILL_V
xxeis.eis_rsc_ins.v( 'EIS_XXWC_GL_CONSOLIDATE_BILL_V',101,'','','','','ANONYMOUS','XXEIS','Xxcus Eis Iproc Gl Sl 180 V','EXGCB2V','','','VIEW','US','','','');
--Delete Object Columns for EIS_XXWC_GL_CONSOLIDATE_BILL_V
xxeis.eis_rsc_utility.delete_view_rows('EIS_XXWC_GL_CONSOLIDATE_BILL_V',101,FALSE);
--Inserting Object Columns for EIS_XXWC_GL_CONSOLIDATE_BILL_V
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_GL_CONSOLIDATE_BILL_V','PO_SHIP_TO_LOC',101,'Po Ship To Loc','PO_SHIP_TO_LOC','','','','ANONYMOUS','VARCHAR2','','','Po Ship To Loc','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_GL_CONSOLIDATE_BILL_V','GL_DATE',101,'Gl Date','GL_DATE','','','','ANONYMOUS','DATE','','','Gl Date','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_GL_CONSOLIDATE_BILL_V','POL_CLOSED_CODE',101,'Pol Closed Code','POL_CLOSED_CODE','','','','ANONYMOUS','VARCHAR2','','','Pol Closed Code','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_GL_CONSOLIDATE_BILL_V','PO_CLOSED_CODE',101,'Po Closed Code','PO_CLOSED_CODE','','','','ANONYMOUS','VARCHAR2','','','Po Closed Code','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_GL_CONSOLIDATE_BILL_V','AP_INV_DATE',101,'Ap Inv Date','AP_INV_DATE','','','','ANONYMOUS','DATE','','','Ap Inv Date','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_GL_CONSOLIDATE_BILL_V','GCC50328PROJECT_CODE',101,'Gcc50328project Code','GCC50328PROJECT_CODE','','','','ANONYMOUS','VARCHAR2','','','Gcc50328project Code','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_GL_CONSOLIDATE_BILL_V','GCC50328ACCOUNTDESCR',101,'Gcc50328accountdescr','GCC50328ACCOUNTDESCR','','','','ANONYMOUS','VARCHAR2','','','Gcc50328accountdescr','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_GL_CONSOLIDATE_BILL_V','GCC50328ACCOUNT',101,'Gcc50328account','GCC50328ACCOUNT','','','','ANONYMOUS','VARCHAR2','','','Gcc50328account','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_GL_CONSOLIDATE_BILL_V','GCC50328COST_CENTER',101,'Gcc50328cost Center','GCC50328COST_CENTER','','','','ANONYMOUS','VARCHAR2','','','Gcc50328cost Center','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_GL_CONSOLIDATE_BILL_V','GCC50328LOCATIONDESCR',101,'Gcc50328locationdescr','GCC50328LOCATIONDESCR','','','','ANONYMOUS','VARCHAR2','','','Gcc50328locationdescr','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_GL_CONSOLIDATE_BILL_V','GCC50328LOCATION',101,'Gcc50328location','GCC50328LOCATION','','','','ANONYMOUS','VARCHAR2','','','Gcc50328location','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_GL_CONSOLIDATE_BILL_V','GCC50328PRODUCT',101,'Gcc50328product','GCC50328PRODUCT','','','','ANONYMOUS','VARCHAR2','','','Gcc50328product','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_GL_CONSOLIDATE_BILL_V','XXCUS_IMAGE_LINK',101,'Xxcus Image Link','XXCUS_IMAGE_LINK','','','','ANONYMOUS','VARCHAR2','','','Xxcus Image Link','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_GL_CONSOLIDATE_BILL_V','XXCUS_PO_CREATED_BY',101,'Xxcus Po Created By','XXCUS_PO_CREATED_BY','','','','ANONYMOUS','VARCHAR2','','','Xxcus Po Created By','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_GL_CONSOLIDATE_BILL_V','XXCUS_LINE_DESC',101,'Xxcus Line Desc','XXCUS_LINE_DESC','','','','ANONYMOUS','VARCHAR2','','','Xxcus Line Desc','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_GL_CONSOLIDATE_BILL_V','SLA_EVENT_TYPE',101,'Sla Event Type','SLA_EVENT_TYPE','','','','ANONYMOUS','VARCHAR2','','','Sla Event Type','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_GL_CONSOLIDATE_BILL_V','BOL_NUMBER',101,'Bol Number','BOL_NUMBER','','','','ANONYMOUS','VARCHAR2','','','Bol Number','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_GL_CONSOLIDATE_BILL_V','PO_NUMBER',101,'Po Number','PO_NUMBER','','','','ANONYMOUS','VARCHAR2','','','Po Number','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_GL_CONSOLIDATE_BILL_V','BATCH_NAME',101,'Batch Name','BATCH_NAME','','','','ANONYMOUS','VARCHAR2','','','Batch Name','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_GL_CONSOLIDATE_BILL_V','NAME',101,'Name','NAME','','','','ANONYMOUS','VARCHAR2','','','Name','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_GL_CONSOLIDATE_BILL_V','EFFECTIVE_PERIOD_NUM',101,'Effective Period Num','EFFECTIVE_PERIOD_NUM','','','','ANONYMOUS','NUMBER','','','Effective Period Num','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_GL_CONSOLIDATE_BILL_V','TYPE',101,'Type','TYPE','','','','ANONYMOUS','VARCHAR2','','','Type','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_GL_CONSOLIDATE_BILL_V','PERIOD_NAME',101,'Period Name','PERIOD_NAME','','','','ANONYMOUS','VARCHAR2','','','Period Name','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_GL_CONSOLIDATE_BILL_V','CURRENCY_CODE',101,'Currency Code','CURRENCY_CODE','','','','ANONYMOUS','VARCHAR2','','','Currency Code','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_GL_CONSOLIDATE_BILL_V','GL_ACCOUNT_STRING',101,'Gl Account String','GL_ACCOUNT_STRING','','','','ANONYMOUS','VARCHAR2','','','Gl Account String','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_GL_CONSOLIDATE_BILL_V','TRANSACTION_NUM',101,'Transaction Num','TRANSACTION_NUM','','','','ANONYMOUS','VARCHAR2','','','Transaction Num','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_GL_CONSOLIDATE_BILL_V','SLA_DIST_ACCOUNTED_NET',101,'Sla Dist Accounted Net','SLA_DIST_ACCOUNTED_NET','','~T~D~2','','ANONYMOUS','NUMBER','','','Sla Dist Accounted Net','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_GL_CONSOLIDATE_BILL_V','SLA_DIST_ACCOUNTED_DR',101,'Sla Dist Accounted Dr','SLA_DIST_ACCOUNTED_DR','','~T~D~2','','ANONYMOUS','NUMBER','','','Sla Dist Accounted Dr','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_GL_CONSOLIDATE_BILL_V','SLA_DIST_ACCOUNTED_CR',101,'Sla Dist Accounted Cr','SLA_DIST_ACCOUNTED_CR','','~T~D~2','','ANONYMOUS','NUMBER','','','Sla Dist Accounted Cr','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_GL_CONSOLIDATE_BILL_V','ACCOUNTED_CR',101,'Accounted Cr','ACCOUNTED_CR','','~T~D~2','','ANONYMOUS','NUMBER','','','Accounted Cr','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_GL_CONSOLIDATE_BILL_V','ACCOUNTED_DR',101,'Accounted Dr','ACCOUNTED_DR','','~T~D~2','','ANONYMOUS','NUMBER','','','Accounted Dr','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_GL_CONSOLIDATE_BILL_V','ENTERED_CR',101,'Entered Cr','ENTERED_CR','','~T~D~2','','ANONYMOUS','NUMBER','','','Entered Cr','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_GL_CONSOLIDATE_BILL_V','ENTERED_DR',101,'Entered Dr','ENTERED_DR','','~T~D~2','','ANONYMOUS','NUMBER','','','Entered Dr','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_GL_CONSOLIDATE_BILL_V','CUSTOMER_OR_VENDOR_NUMBER',101,'Customer Or Vendor Number','CUSTOMER_OR_VENDOR_NUMBER','','','','ANONYMOUS','VARCHAR2','','','Customer Or Vendor Number','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_GL_CONSOLIDATE_BILL_V','CUSTOMER_OR_VENDOR',101,'Customer Or Vendor','CUSTOMER_OR_VENDOR','','','','ANONYMOUS','VARCHAR2','','','Customer Or Vendor','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_GL_CONSOLIDATE_BILL_V','GL_SL_LINK_ID',101,'Gl Sl Link Id','GL_SL_LINK_ID','','','','ANONYMOUS','NUMBER','','','Gl Sl Link Id','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_GL_CONSOLIDATE_BILL_V','LINE_ENT_CR',101,'Line Ent Cr','LINE_ENT_CR','','~T~D~2','','ANONYMOUS','NUMBER','','','Line Ent Cr','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_GL_CONSOLIDATE_BILL_V','LINE_ENT_DR',101,'Line Ent Dr','LINE_ENT_DR','','~T~D~2','','ANONYMOUS','NUMBER','','','Line Ent Dr','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_GL_CONSOLIDATE_BILL_V','LINE_ACCTD_CR',101,'Line Acctd Cr','LINE_ACCTD_CR','','~T~D~2','','ANONYMOUS','NUMBER','','','Line Acctd Cr','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_GL_CONSOLIDATE_BILL_V','LINE_ACCTD_DR',101,'Line Acctd Dr','LINE_ACCTD_DR','','~T~D~2','','ANONYMOUS','NUMBER','','','Line Acctd Dr','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_GL_CONSOLIDATE_BILL_V','HNUMBER',101,'Hnumber','HNUMBER','','','','ANONYMOUS','NUMBER','','','Hnumber','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_GL_CONSOLIDATE_BILL_V','LINE_DESCR',101,'Line Descr','LINE_DESCR','','','','ANONYMOUS','VARCHAR2','','','Line Descr','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_GL_CONSOLIDATE_BILL_V','ENTRY',101,'Entry','ENTRY','','','','ANONYMOUS','VARCHAR2','','','Entry','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_GL_CONSOLIDATE_BILL_V','ACC_DATE',101,'Acc Date','ACC_DATE','','','','ANONYMOUS','DATE','','','Acc Date','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_GL_CONSOLIDATE_BILL_V','JE_LINE_NUM',101,'Je Line Num','JE_LINE_NUM','','','','ANONYMOUS','NUMBER','','','Je Line Num','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_GL_CONSOLIDATE_BILL_V','JE_CATEGORY',101,'Je Category','JE_CATEGORY','','','','ANONYMOUS','VARCHAR2','','','Je Category','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_GL_CONSOLIDATE_BILL_V','SOURCE',101,'Source','SOURCE','','','','ANONYMOUS','VARCHAR2','','','Source','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_GL_CONSOLIDATE_BILL_V','PO_REQ_LINE_ITEM_DESC',101,'Po Req Line Item Desc','PO_REQ_LINE_ITEM_DESC','','','','ANONYMOUS','VARCHAR2','','','Po Req Line Item Desc','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_GL_CONSOLIDATE_BILL_V','PO_REQ_DESC',101,'Po Req Desc','PO_REQ_DESC','','','','ANONYMOUS','VARCHAR2','','','Po Req Desc','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_GL_CONSOLIDATE_BILL_V','FETCH_SEQ',101,'Fetch Seq','FETCH_SEQ','','','','ANONYMOUS','NUMBER','','','Fetch Seq','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_GL_CONSOLIDATE_BILL_V','APPLIED_TO_SOURCE_ID_NUM_1',101,'Applied To Source Id Num 1','APPLIED_TO_SOURCE_ID_NUM_1','','','','ANONYMOUS','NUMBER','','','Applied To Source Id Num 1','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_GL_CONSOLIDATE_BILL_V','SOURCE_DIST_ID_NUM_1',101,'Source Dist Id Num 1','SOURCE_DIST_ID_NUM_1','','','','ANONYMOUS','NUMBER','','','Source Dist Id Num 1','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_GL_CONSOLIDATE_BILL_V','SOURCE_DIST_TYPE',101,'Source Dist Type','SOURCE_DIST_TYPE','','','','ANONYMOUS','VARCHAR2','','','Source Dist Type','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_GL_CONSOLIDATE_BILL_V','GCC50368FUTURE_USEDESCR',101,'Gcc50368future Usedescr','GCC50368FUTURE_USEDESCR','','','','ANONYMOUS','VARCHAR2','','','Gcc50368future Usedescr','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_GL_CONSOLIDATE_BILL_V','GCC50368FUTURE_USE',101,'Gcc50368future Use','GCC50368FUTURE_USE','','','','ANONYMOUS','VARCHAR2','','','Gcc50368future Use','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_GL_CONSOLIDATE_BILL_V','GCC50368SUBACCOUNTDESCR',101,'Gcc50368subaccountdescr','GCC50368SUBACCOUNTDESCR','','','','ANONYMOUS','VARCHAR2','','','Gcc50368subaccountdescr','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_GL_CONSOLIDATE_BILL_V','GCC50368SUBACCOUNT',101,'Gcc50368subaccount','GCC50368SUBACCOUNT','','','','ANONYMOUS','VARCHAR2','','','Gcc50368subaccount','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_GL_CONSOLIDATE_BILL_V','GCC50368ACCOUNTDESCR',101,'Gcc50368accountdescr','GCC50368ACCOUNTDESCR','','','','ANONYMOUS','VARCHAR2','','','Gcc50368accountdescr','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_GL_CONSOLIDATE_BILL_V','GCC50368ACCOUNT',101,'Gcc50368account','GCC50368ACCOUNT','','','','ANONYMOUS','VARCHAR2','','','Gcc50368account','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_GL_CONSOLIDATE_BILL_V','GCC50368DEPARTMENTDESCR',101,'Gcc50368departmentdescr','GCC50368DEPARTMENTDESCR','','','','ANONYMOUS','VARCHAR2','','','Gcc50368departmentdescr','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_GL_CONSOLIDATE_BILL_V','GCC50368DEPARTMENT',101,'Gcc50368department','GCC50368DEPARTMENT','','','','ANONYMOUS','VARCHAR2','','','Gcc50368department','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_GL_CONSOLIDATE_BILL_V','GCC50368DIVISIONDESCR',101,'Gcc50368divisiondescr','GCC50368DIVISIONDESCR','','','','ANONYMOUS','VARCHAR2','','','Gcc50368divisiondescr','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_GL_CONSOLIDATE_BILL_V','GCC50368DIVISION',101,'Gcc50368division','GCC50368DIVISION','','','','ANONYMOUS','VARCHAR2','','','Gcc50368division','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_GL_CONSOLIDATE_BILL_V','GCC50368PRODUCTDESCR',101,'Gcc50368productdescr','GCC50368PRODUCTDESCR','','','','ANONYMOUS','VARCHAR2','','','Gcc50368productdescr','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_GL_CONSOLIDATE_BILL_V','GCC50368PRODUCT',101,'Gcc50368product','GCC50368PRODUCT','','','','ANONYMOUS','VARCHAR2','','','Gcc50368product','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_GL_CONSOLIDATE_BILL_V','GCC50328FUTURE_USE_2DESCR',101,'Gcc50328future Use 2descr','GCC50328FUTURE_USE_2DESCR','','','','ANONYMOUS','VARCHAR2','','','Gcc50328future Use 2descr','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_GL_CONSOLIDATE_BILL_V','GCC50328FUTURE_USE_2',101,'Gcc50328future Use 2','GCC50328FUTURE_USE_2','','','','ANONYMOUS','VARCHAR2','','','Gcc50328future Use 2','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_GL_CONSOLIDATE_BILL_V','GCC50328FURTURE_USEDESCR',101,'Gcc50328furture Usedescr','GCC50328FURTURE_USEDESCR','','','','ANONYMOUS','VARCHAR2','','','Gcc50328furture Usedescr','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_GL_CONSOLIDATE_BILL_V','GCC50328FURTURE_USE',101,'Gcc50328furture Use','GCC50328FURTURE_USE','','','','ANONYMOUS','VARCHAR2','','','Gcc50328furture Use','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_GL_CONSOLIDATE_BILL_V','GCC50328PROJECT_CODEDESCR',101,'Gcc50328project Codedescr','GCC50328PROJECT_CODEDESCR','','','','ANONYMOUS','VARCHAR2','','','Gcc50328project Codedescr','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_GL_CONSOLIDATE_BILL_V','GCC50328COST_CENTERDESCR',101,'Gcc50328cost Centerdescr','GCC50328COST_CENTERDESCR','','','','ANONYMOUS','VARCHAR2','','','Gcc50328cost Centerdescr','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_GL_CONSOLIDATE_BILL_V','GCC50328PRODUCTDESCR',101,'Gcc50328productdescr','GCC50328PRODUCTDESCR','','','','ANONYMOUS','VARCHAR2','','','Gcc50328productdescr','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_GL_CONSOLIDATE_BILL_V','CODE_COMBINATION_ID',101,'Code Combination Id','CODE_COMBINATION_ID','','','','ANONYMOUS','NUMBER','','','Code Combination Id','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_GL_CONSOLIDATE_BILL_V','ITEM_OVRHD_COST',101,'Item Ovrhd Cost','ITEM_OVRHD_COST','','~T~D~2','','ANONYMOUS','NUMBER','','','Item Ovrhd Cost','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_GL_CONSOLIDATE_BILL_V','ITEM_MATERIAL_COST',101,'Item Material Cost','ITEM_MATERIAL_COST','','~T~D~2','','ANONYMOUS','NUMBER','','','Item Material Cost','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_GL_CONSOLIDATE_BILL_V','ITEM_TXN_QTY',101,'Item Txn Qty','ITEM_TXN_QTY','','~T~D~2','','ANONYMOUS','NUMBER','','','Item Txn Qty','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_GL_CONSOLIDATE_BILL_V','ITEM_UNIT_WT',101,'Item Unit Wt','ITEM_UNIT_WT','','~T~D~2','','ANONYMOUS','NUMBER','','','Item Unit Wt','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_GL_CONSOLIDATE_BILL_V','PART_DESCRIPTION',101,'Part Description','PART_DESCRIPTION','','','','ANONYMOUS','VARCHAR2','','','Part Description','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_GL_CONSOLIDATE_BILL_V','PART_NUMBER',101,'Part Number','PART_NUMBER','','','','ANONYMOUS','VARCHAR2','','','Part Number','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_GL_CONSOLIDATE_BILL_V','AP_INV_SOURCE',101,'Ap Inv Source','AP_INV_SOURCE','','','','ANONYMOUS','VARCHAR2','','','Ap Inv Source','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_GL_CONSOLIDATE_BILL_V','SALES_ORDER',101,'Sales Order','SALES_ORDER','','','','ANONYMOUS','VARCHAR2','','','Sales Order','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_GL_CONSOLIDATE_BILL_V','ASSOCIATE_NUM',101,'Associate Num','ASSOCIATE_NUM','','','','ANONYMOUS','VARCHAR2','','','Associate Num','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_GL_CONSOLIDATE_BILL_V','SLA_DIST_ENTERED_NET',101,'Sla Dist Entered Net','SLA_DIST_ENTERED_NET','','~T~D~2','','ANONYMOUS','NUMBER','','','Sla Dist Entered Net','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_GL_CONSOLIDATE_BILL_V','SLA_DIST_ENTERED_DR',101,'Sla Dist Entered Dr','SLA_DIST_ENTERED_DR','','~T~D~2','','ANONYMOUS','NUMBER','','','Sla Dist Entered Dr','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_GL_CONSOLIDATE_BILL_V','SLA_DIST_ENTERED_CR',101,'Sla Dist Entered Cr','SLA_DIST_ENTERED_CR','','~T~D~2','','ANONYMOUS','NUMBER','','','Sla Dist Entered Cr','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_GL_CONSOLIDATE_BILL_V','SLA_LINE_ENTERED_NET',101,'Sla Line Entered Net','SLA_LINE_ENTERED_NET','','~T~D~2','','ANONYMOUS','NUMBER','','','Sla Line Entered Net','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_GL_CONSOLIDATE_BILL_V','SLA_LINE_ENTERED_CR',101,'Sla Line Entered Cr','SLA_LINE_ENTERED_CR','','~T~D~2','','ANONYMOUS','NUMBER','','','Sla Line Entered Cr','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_GL_CONSOLIDATE_BILL_V','SLA_LINE_ENTERED_DR',101,'Sla Line Entered Dr','SLA_LINE_ENTERED_DR','','~T~D~2','','ANONYMOUS','NUMBER','','','Sla Line Entered Dr','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_GL_CONSOLIDATE_BILL_V','SLA_LINE_ACCOUNTED_NET',101,'Sla Line Accounted Net','SLA_LINE_ACCOUNTED_NET','','~T~D~2','','ANONYMOUS','NUMBER','','','Sla Line Accounted Net','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_GL_CONSOLIDATE_BILL_V','SLA_LINE_ACCOUNTED_CR',101,'Sla Line Accounted Cr','SLA_LINE_ACCOUNTED_CR','','~T~D~2','','ANONYMOUS','NUMBER','','','Sla Line Accounted Cr','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_GL_CONSOLIDATE_BILL_V','SLA_LINE_ACCOUNTED_DR',101,'Sla Line Accounted Dr','SLA_LINE_ACCOUNTED_DR','','~T~D~2','','ANONYMOUS','NUMBER','','','Sla Line Accounted Dr','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_GL_CONSOLIDATE_BILL_V','H_SEQ_ID',101,'H Seq Id','H_SEQ_ID','','','','ANONYMOUS','NUMBER','','','H Seq Id','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_GL_CONSOLIDATE_BILL_V','SEQ_NUM',101,'Seq Num','SEQ_NUM','','','','ANONYMOUS','NUMBER','','','Seq Num','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_GL_CONSOLIDATE_BILL_V','SEQ_ID',101,'Seq Id','SEQ_ID','','','','ANONYMOUS','NUMBER','','','Seq Id','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_GL_CONSOLIDATE_BILL_V','LLINE',101,'Lline','LLINE','','','','ANONYMOUS','NUMBER','','','Lline','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_GL_CONSOLIDATE_BILL_V','LSEQUENCE',101,'Lsequence','LSEQUENCE','','','','ANONYMOUS','VARCHAR2','','','Lsequence','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_GL_CONSOLIDATE_BILL_V','BATCH',101,'Batch','BATCH','','','','ANONYMOUS','NUMBER','','','Batch','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_GL_CONSOLIDATE_BILL_V','JE_HEADER_ID',101,'Je Header Id','JE_HEADER_ID','','','','ANONYMOUS','NUMBER','','','Je Header Id','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_GL_CONSOLIDATE_BILL_V','ORACLE_ACCOUNT',101,'Oracle Account','ORACLE_ACCOUNT','','','','ANONYMOUS','VARCHAR2','','','Oracle Account','','','','','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_GL_CONSOLIDATE_BILL_V','ORACLE_COST_CENTER',101,'Oracle Cost Center','ORACLE_COST_CENTER','','','','ANONYMOUS','VARCHAR2','','','Oracle Cost Center','','','','','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_GL_CONSOLIDATE_BILL_V','ORACLE_LOCATION',101,'Oracle Location','ORACLE_LOCATION','','','','ANONYMOUS','VARCHAR2','','','Oracle Location','','','','','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_GL_CONSOLIDATE_BILL_V','ORACLE_PRODUCT',101,'Oracle Product','ORACLE_PRODUCT','','','','ANONYMOUS','VARCHAR2','','','Oracle Product','','','','','');
--Inserting Object Components for EIS_XXWC_GL_CONSOLIDATE_BILL_V
xxeis.eis_rsc_ins.vcomp( 'EIS_XXWC_GL_CONSOLIDATE_BILL_V','XXCUS_NATURAL_ACCT_TBL',101,'XXCUS_NATURAL_ACCT_TBL','XNAT','XNAT','ANONYMOUS','ANONYMOUS','-1','Xxcus Natural Acct Tbl','N','','','','','',' ',' ',' ',' ',' ');
--Inserting Object Component Joins for EIS_XXWC_GL_CONSOLIDATE_BILL_V
xxeis.eis_rsc_ins.vcj( 'EIS_XXWC_GL_CONSOLIDATE_BILL_V','XXCUS_NATURAL_ACCT_TBL','XNAT',101,'EXGCB2V.ORACLE_ACCOUNT','=','XNAT.Z_OLD_VALUE(+)','','','1','Y','ANONYMOUS');
ELSE
RAISE_APPLICATION_ERROR (-20001,'Module does not exist. Import/Create the module with id 101');
END IF;
END;
/
--Exporting View Source View Component -  EIS_XXWC_GL_CONSOLIDATE_BILL_V
--Exporting View Component Data of the View -  EIS_XXWC_GL_CONSOLIDATE_BILL_V
prompt Creating Object Data XXCUS_NATURAL_ACCT_TBL
set scan off define off
DECLARE
mod_exist VARCHAR2(1);
BEGIN 
mod_exist := XXEIS.EIS_RSC_INSERT.check_if_module_exist(101);
IF mod_exist = 'Y' THEN 
--Inserting Object XXCUS_NATURAL_ACCT_TBL
xxeis.eis_rsc_ins.v( 'XXCUS_NATURAL_ACCT_TBL',101,'','','','','ANONYMOUS','XXCUS','Xxcus Natural Acct Tbl','XNAT','','','TABLE','US','','','');
--Delete Object Columns for XXCUS_NATURAL_ACCT_TBL
xxeis.eis_rsc_utility.delete_view_rows('XXCUS_NATURAL_ACCT_TBL',101,FALSE);
--Inserting Object Columns for XXCUS_NATURAL_ACCT_TBL
xxeis.eis_rsc_ins.vc( 'XXCUS_NATURAL_ACCT_TBL','PROJECTDESCR',101,'Projectdescr','PROJECTDESCR','','','','ANONYMOUS','VARCHAR2','','','Projectdescr','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXCUS_NATURAL_ACCT_TBL','LOB',101,'Lob','LOB','','','','ANONYMOUS','VARCHAR2','','','Lob','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXCUS_NATURAL_ACCT_TBL','LOB_SUBCLASS',101,'Lob Subclass','LOB_SUBCLASS','','','','ANONYMOUS','VARCHAR2','','','Lob Subclass','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXCUS_NATURAL_ACCT_TBL','SYSTEM_CD',101,'System Cd','SYSTEM_CD','','','','ANONYMOUS','VARCHAR2','','','System Cd','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXCUS_NATURAL_ACCT_TBL','TARGET_SYS',101,'Target Sys','TARGET_SYS','','','','ANONYMOUS','VARCHAR2','','','Target Sys','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXCUS_NATURAL_ACCT_TBL','MAP_LEVEL',101,'Map Level','MAP_LEVEL','','','','ANONYMOUS','VARCHAR2','','','Map Level','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXCUS_NATURAL_ACCT_TBL','MAP_SUBLEVEL',101,'Map Sublevel','MAP_SUBLEVEL','','','','ANONYMOUS','VARCHAR2','','','Map Sublevel','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXCUS_NATURAL_ACCT_TBL','Z_OLD_VALUE',101,'Z Old Value','Z_OLD_VALUE','','','','ANONYMOUS','VARCHAR2','','','Z Old Value','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXCUS_NATURAL_ACCT_TBL','Z_OLD_VALUE2',101,'Z Old Value2','Z_OLD_VALUE2','','','','ANONYMOUS','VARCHAR2','','','Z Old Value2','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXCUS_NATURAL_ACCT_TBL','Z_OLD_VALUE3',101,'Z Old Value3','Z_OLD_VALUE3','','','','ANONYMOUS','VARCHAR2','','','Z Old Value3','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXCUS_NATURAL_ACCT_TBL','Z_OLD_VALUE4',101,'Z Old Value4','Z_OLD_VALUE4','','','','ANONYMOUS','VARCHAR2','','','Z Old Value4','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXCUS_NATURAL_ACCT_TBL','Z_NEW_VALUE',101,'Z New Value','Z_NEW_VALUE','','','','ANONYMOUS','VARCHAR2','','','Z New Value','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXCUS_NATURAL_ACCT_TBL','Z_NEW_VALUE2',101,'Z New Value2','Z_NEW_VALUE2','','','','ANONYMOUS','VARCHAR2','','','Z New Value2','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXCUS_NATURAL_ACCT_TBL','Z_NEW_VALUE3',101,'Z New Value3','Z_NEW_VALUE3','','','','ANONYMOUS','VARCHAR2','','','Z New Value3','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXCUS_NATURAL_ACCT_TBL','Z_NEW_VALUE4',101,'Z New Value4','Z_NEW_VALUE4','','','','ANONYMOUS','VARCHAR2','','','Z New Value4','','','','US','');
--Inserting Object Components for XXCUS_NATURAL_ACCT_TBL
--Inserting Object Component Joins for XXCUS_NATURAL_ACCT_TBL
ELSE
RAISE_APPLICATION_ERROR (-20001,'Module does not exist. Import/Create the module with id 101');
END IF;
END;
/
prompt Creating Report LOV Data for HDS - FM Oracle Consolidated Billing Details
set scan off define off
DECLARE
mod_exist VARCHAR2(1);
BEGIN 
mod_exist := XXEIS.EIS_RSC_INSERT.check_if_module_exist(101);
IF mod_exist = 'Y' THEN 
--Inserting Report LOVs - HDS - FM Oracle Consolidated Billing Details
xxeis.eis_rsc_ins.lov( 101,'SELECT distinct user_je_source_name FROM gl_je_sources','','EIS_GL_JE_SOURCES_LOV','LOV of all Available Journal Sources','XXEIS_RS_ADMIN',NULL,'','','','','','','','US');
xxeis.eis_rsc_ins.lov( '','SELECT ffv.flex_value, ffvtl.description, decode(ffv.summary_flag,''Y'', ''Parent'',''N'', ''Child'') Type
				FROM
					fnd_flex_value_sets ffvs ,
					fnd_flex_values ffv,
					fnd_flex_values_tl ffvtl
				WHERE   upper(ffvs.flex_value_set_name) = upper(''XXCUS_GL_PRODUCT'')
				 and ffv.flex_value_set_id = ffvs.flex_value_set_id
				 and ffv.FLEX_VALUE_ID = ffvtl.FLEX_VALUE_ID
				AND ffv.enabled_flag = upper(''Y'')
				AND ffv.summary_flag in (''Y'',''N'')
				/* AND ffv.summary_flag <> upper(''Y'') */
				AND ffvtl.LANGUAGE = USERENV(''LANG'')
				 AND xxeis.eis_gl_security_pkg.validate_segment_value( ''SEGMENT1'' , ffv.flex_value)=''TRUE''
				order by ffv.flex_value	','','XXCUS_GL_PRODUCT','XXCUS_GL_PRODUCT','MM050208',NULL,'','','','','','','','US');
xxeis.eis_rsc_ins.lov( '','SELECT ffv.flex_value, ffvtl.description, decode(ffv.summary_flag,''Y'', ''Parent'',''N'', ''Child'') Type
				FROM
					fnd_flex_value_sets ffvs ,
					fnd_flex_values ffv,
					fnd_flex_values_tl ffvtl
				WHERE   upper(ffvs.flex_value_set_name) = upper(''XXCUS_GL_LOCATION'')
				 and ffv.flex_value_set_id = ffvs.flex_value_set_id
				 and ffv.FLEX_VALUE_ID = ffvtl.FLEX_VALUE_ID
				AND ffv.enabled_flag = upper(''Y'')
				AND ffv.summary_flag in (''Y'',''N'')
				/* AND ffv.summary_flag <> upper(''Y'') */
				AND ffvtl.LANGUAGE = USERENV(''LANG'')
				 AND xxeis.eis_gl_security_pkg.validate_segment_value( ''SEGMENT2'' , ffv.flex_value)=''TRUE''
				order by ffv.flex_value	','','XXCUS_GL_LOCATION','XXCUS_GL_LOCATION','MM050208',NULL,'','','','','','','','US');
xxeis.eis_rsc_ins.lov( 101,'select distinct period_name
from gl_je_headers','','HDS_GL_PERIOD_NAME','GL Period Name','XXEIS_RS_ADMIN',NULL,'N','','','','','','','US');
null;
ELSE
RAISE_APPLICATION_ERROR (-20001,'Module does not exist. Import/Create the module with id 101');
END IF;
END;
/
prompt Creating Report Data for HDS - FM Oracle Consolidated Billing Details
set scan off define off
DECLARE
mod_exist varchar2(1);
BEGIN 
mod_exist := XXEIS.EIS_RSC_INSERT.check_if_module_exist(101);
IF mod_exist = 'Y' THEN 
--Deleting Report data - HDS - FM Oracle Consolidated Billing Details
xxeis.eis_rsc_utility.delete_report_rows( 'HDS - FM Oracle Consolidated Billing Details',101 );
--Inserting Report - HDS - FM Oracle Consolidated Billing Details
xxeis.eis_rsc_ins.r( 101,'HDS - FM Oracle Consolidated Billing Details','','This report will provide detail from the subledger for all entries posted for the account in general ledger. This was created for the accounting team to research the iProcurement PO invoices. Copied from the original report to include some additional columns.
ESMS# 306022
','','','','SA059956','EIS_XXWC_GL_CONSOLIDATE_BILL_V','Y','','','SA059956','','N','HDS Standard Reports','','CSV,EXCEL,','N','','','','','','N','','US','','HDS-WW Oracle AP Invoice Details ','','','','','','','','','','','','','','');
--Inserting Report Columns - HDS - FM Oracle Consolidated Billing Details
xxeis.eis_rsc_ins.rc( 'HDS - FM Oracle Consolidated Billing Details',101,'NAME','Ledger Name','Name','','','default','','16','N','Y','','','','','','','SA059956','N','N','','EIS_XXWC_GL_CONSOLIDATE_BILL_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'HDS - FM Oracle Consolidated Billing Details',101,'PERIOD_NAME','Period Name','Period Name','','','default','','4','N','Y','','','','','','','SA059956','N','N','','EIS_XXWC_GL_CONSOLIDATE_BILL_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'HDS - FM Oracle Consolidated Billing Details',101,'SOURCE','Source','Source','','','default','','1','N','Y','','','','','','','SA059956','N','N','','EIS_XXWC_GL_CONSOLIDATE_BILL_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'HDS - FM Oracle Consolidated Billing Details',101,'ACC_DATE','Date','Acc Date','','','default','','2','N','Y','','','','','','','SA059956','N','N','','EIS_XXWC_GL_CONSOLIDATE_BILL_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'HDS - FM Oracle Consolidated Billing Details',101,'BATCH_NAME','Batch Name','Batch Name','','','default','','5','N','Y','','','','','','','SA059956','N','N','','EIS_XXWC_GL_CONSOLIDATE_BILL_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'HDS - FM Oracle Consolidated Billing Details',101,'ENTRY','Journal Entry','Entry','','','default','','6','N','Y','','','','','','','SA059956','N','N','','EIS_XXWC_GL_CONSOLIDATE_BILL_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'HDS - FM Oracle Consolidated Billing Details',101,'GCC50328ACCOUNT','Oracle Account','Gcc50328account','','','default','','11','N','Y','','','','','','','SA059956','N','N','','EIS_XXWC_GL_CONSOLIDATE_BILL_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'HDS - FM Oracle Consolidated Billing Details',101,'GCC50328ACCOUNTDESCR','Account Description','Gcc50328accountdescr','','','default','','14','N','Y','','','','','','','SA059956','N','N','','EIS_XXWC_GL_CONSOLIDATE_BILL_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'HDS - FM Oracle Consolidated Billing Details',101,'GCC50328LOCATION','Location','Gcc50328location','','','default','','10','N','Y','','','','','','','SA059956','N','N','','EIS_XXWC_GL_CONSOLIDATE_BILL_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'HDS - FM Oracle Consolidated Billing Details',101,'GCC50328PRODUCT','Product','Gcc50328product','','','default','','9','N','Y','','','','','','','SA059956','N','N','','EIS_XXWC_GL_CONSOLIDATE_BILL_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'HDS - FM Oracle Consolidated Billing Details',101,'GCC50328PROJECT_CODE','Project Code','Gcc50328project Code','','','default','','13','N','Y','','','','','','','SA059956','N','N','','EIS_XXWC_GL_CONSOLIDATE_BILL_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'HDS - FM Oracle Consolidated Billing Details',101,'GL_ACCOUNT_STRING','GL Account String','Gl Account String','','','default','','8','N','Y','','','','','','','SA059956','N','N','','EIS_XXWC_GL_CONSOLIDATE_BILL_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'HDS - FM Oracle Consolidated Billing Details',101,'JE_CATEGORY','Journal Category','Je Category','','','default','','3','N','Y','','','','','','','SA059956','N','N','','EIS_XXWC_GL_CONSOLIDATE_BILL_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'HDS - FM Oracle Consolidated Billing Details',101,'XNAT.Z_NEW_VALUE','SAP Account','Z New Value','','','default','','12','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_GL_CONSOLIDATE_BILL_V','XXCUS_NATURAL_ACCT_TBL','XNAT','','US','','');
xxeis.eis_rsc_ins.rc( 'HDS - FM Oracle Consolidated Billing Details',101,'SLA_DIST_ACCOUNTED_NET','Accounted Amount','Sla Dist Accounted Net','','~T~D~2','default','','15','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_GL_CONSOLIDATE_BILL_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'HDS - FM Oracle Consolidated Billing Details',101,'LINE_DESCR','Line Description','Line Descr','VARCHAR2','','default','','7','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_GL_CONSOLIDATE_BILL_V','','','','US','','');
--Inserting Report Parameters - HDS - FM Oracle Consolidated Billing Details
xxeis.eis_rsc_ins.rp( 'HDS - FM Oracle Consolidated Billing Details',101,'Location','Gcc50328location','GCC50328LOCATION','IN','XXCUS_GL_LOCATION','','VARCHAR2','N','Y','3','Y','Y','CONSTANT','SA059956','Y','N','','','','EIS_XXWC_GL_CONSOLIDATE_BILL_V','','','US','');
xxeis.eis_rsc_ins.rp( 'HDS - FM Oracle Consolidated Billing Details',101,'Product','Gcc50328product','GCC50328PRODUCT','IN','XXCUS_GL_PRODUCT','','VARCHAR2','Y','Y','2','Y','Y','CONSTANT','SA059956','Y','N','','','','EIS_XXWC_GL_CONSOLIDATE_BILL_V','','','US','');
xxeis.eis_rsc_ins.rp( 'HDS - FM Oracle Consolidated Billing Details',101,'Period Name','Period Name','PERIOD_NAME','IN','HDS_GL_PERIOD_NAME','','VARCHAR2','Y','Y','1','Y','Y','CONSTANT','SA059956','Y','N','','','','EIS_XXWC_GL_CONSOLIDATE_BILL_V','','','US','');
xxeis.eis_rsc_ins.rp( 'HDS - FM Oracle Consolidated Billing Details',101,'Source','Source','SOURCE','IN','EIS_GL_JE_SOURCES_LOV','','VARCHAR2','N','Y','4','Y','Y','CONSTANT','SA059956','Y','N','','','','EIS_XXWC_GL_CONSOLIDATE_BILL_V','','','US','');
--Inserting Dependent Parameters - HDS - FM Oracle Consolidated Billing Details
--Inserting Report Conditions - HDS - FM Oracle Consolidated Billing Details
xxeis.eis_rsc_ins.rcnh( 'HDS - FM Oracle Consolidated Billing Details',101,'FreeText','FREE_TEXT','','','Y','','1');
xxeis.eis_rsc_ins.rcnd( '','','','','','','','','','','','','','','','','Y','Y','','','','and XNAT.SYSTEM_CD(+)= ''ORCL-FM''','1',101,'HDS - FM Oracle Consolidated Billing Details','FreeText');
xxeis.eis_rsc_ins.rcnh( 'HDS - FM Oracle Consolidated Billing Details',101,'EXGCB2V.PERIOD_NAME IN Period Name','SIMPLE','','','Y','','2');
xxeis.eis_rsc_ins.rcnd( '','','PERIOD_NAME','','Period Name','','','','','EIS_XXWC_GL_CONSOLIDATE_BILL_V','','','','','','IN','Y','Y','','','','','1',101,'HDS - FM Oracle Consolidated Billing Details','EXGCB2V.PERIOD_NAME IN Period Name');
xxeis.eis_rsc_ins.rcnh( 'HDS - FM Oracle Consolidated Billing Details',101,'EXGCB2V.GCC50328PRODUCT IN Product','SIMPLE','','','Y','','3');
xxeis.eis_rsc_ins.rcnd( '','','GCC50328PRODUCT','','Product','','','','','EIS_XXWC_GL_CONSOLIDATE_BILL_V','','','','','','IN','Y','Y','','','','','1',101,'HDS - FM Oracle Consolidated Billing Details','EXGCB2V.GCC50328PRODUCT IN Product');
xxeis.eis_rsc_ins.rcnh( 'HDS - FM Oracle Consolidated Billing Details',101,'EXGCB2V.GCC50328LOCATION IN Location','SIMPLE','','','Y','','4');
xxeis.eis_rsc_ins.rcnd( '','','GCC50328LOCATION','','Location','','','','','EIS_XXWC_GL_CONSOLIDATE_BILL_V','','','','','','IN','Y','Y','','','','','1',101,'HDS - FM Oracle Consolidated Billing Details','EXGCB2V.GCC50328LOCATION IN Location');
xxeis.eis_rsc_ins.rcnh( 'HDS - FM Oracle Consolidated Billing Details',101,'EXGCB2V.SOURCE IN Source','SIMPLE','','','Y','','5');
xxeis.eis_rsc_ins.rcnd( '','','SOURCE','','Source','','','','','EIS_XXWC_GL_CONSOLIDATE_BILL_V','','','','','','IN','Y','Y','','','','','1',101,'HDS - FM Oracle Consolidated Billing Details','EXGCB2V.SOURCE IN Source');
--Inserting Report Sorts - HDS - FM Oracle Consolidated Billing Details
xxeis.eis_rsc_ins.rs( 'HDS - FM Oracle Consolidated Billing Details',101,'BATCH_NAME','ASC','SA059956','1','');
--Inserting Report Triggers - HDS - FM Oracle Consolidated Billing Details
--inserting report templates - HDS - FM Oracle Consolidated Billing Details
--Inserting Report Portals - HDS - FM Oracle Consolidated Billing Details
--inserting report dashboards - HDS - FM Oracle Consolidated Billing Details
--Exporting report views table information -  
xxeis.eis_rsc_ins.rviews( 'HDS - FM Oracle Consolidated Billing Details','101','EIS_XXWC_GL_CONSOLIDATE_BILL_V','XXCUS_NATURAL_ACCT_TBL','N','XNAT');
xxeis.eis_rsc_ins.rviews( 'HDS - FM Oracle Consolidated Billing Details','101','EIS_XXWC_GL_CONSOLIDATE_BILL_V','EIS_XXWC_GL_CONSOLIDATE_BILL_V','N','');
--inserting report security - HDS - FM Oracle Consolidated Billing Details
xxeis.eis_rsc_ins.rsec( 'HDS - FM Oracle Consolidated Billing Details','','MM025705','',101,'SA059956','','N','');
xxeis.eis_rsc_ins.rsec( 'HDS - FM Oracle Consolidated Billing Details','','KM026385','',101,'SA059956','','N','');
xxeis.eis_rsc_ins.rsec( 'HDS - FM Oracle Consolidated Billing Details','101','','XXCUS_GL_INQUIRY',101,'SA059956','','','');
--Inserting Report Pivots - HDS - FM Oracle Consolidated Billing Details
--Inserting Report Distribution Details 
--Inserting Report Distribution Details 
--Inserting Report SubTotals Details 
--Inserting Report aggregations 
--Inserting Report   Version details- HDS - FM Oracle Consolidated Billing Details
xxeis.eis_rsc_ins.rv( 'HDS - FM Oracle Consolidated Billing Details','','HDS - FM Oracle Consolidated Billing Details','SA059956','23-FEB-2018');
ELSE
RAISE_APPLICATION_ERROR (-20001,'Module does not exist. Import/Create the module with id 101');
END IF;
END;
/
