create or replace
PACKAGE XXEIS.EIS_RS_REPORTS_CHECK_LIST_PKG AUTHID CURRENT_USER
IS
  --
  -- To modify this template, edit file PKGSPEC.TXT in TEMPLATE
  -- directory of SQL Navigator
  --
  -- Purpose: Briefly explain the functionality of the package
  --
  --
  -- MODIFICATION HISTORY
  -- Person     Version   Header-No   Bug-No  Issue-No  Date         Comments
  -- ---------  ------    ----------  ------  --------  -----------  -------------------
  -- Sunil      7.0.0                                   10-Dec-2009  Created the Package
  -- Ramana     7.0.0                                   20-Aug-2010  Added new procedure Add_Component_Columns for bug 4451
  -- Madhavi      8.0.0                                 31-Aug-2012  Removed grant script to apps at the end of the file for case #8177
  -- Ravindra   802                                     06-Oct-2014  Created report_check_multiple_param,custom_report_check,seeded_report_sql_check,LOV_CHECK and SEEDED_FLAG_CHECK Procedures
  g_log   VARCHAR2 (4000);
  G_ERROR VARCHAR2 (1000);

  TYPE PARAM_LST IS TABLE OF VARCHAR2 (2000)
      INDEX BY BINARY_INTEGER;

  G_REQUEST_IDS            PARAM_LST;
  G_LOG_REQUEST_IDS        PARAM_LST;

PROCEDURE report_checks(
    p_process_id       IN NUMBER,
    p_application_name IN VARCHAR2 DEFAULT NULL,
    p_report_name      IN VARCHAR2 DEFAULT NULL );
PROCEDURE Add_Component_Columns(
    p_process_Id  IN NUMBER,
    p_application IN VARCHAR2 ,
    p_report      IN VARCHAR2 ,
    p_operation   IN VARCHAR2 ,
    p_component   IN VARCHAR2 DEFAULT NULL );
  /*
  FUNCTION get_flags (p_application_id      IN NUMBER
  ,p_view_name           IN VARCHAR2
  )
  RETURN VARCHAR2;
  */
PROCEDURE report_check_multiple_param(
    p_process_id       IN NUMBER,
    p_application_name IN VARCHAR2 DEFAULT NULL,
    p_cust_seed_flag     IN VARCHAR2 DEFAULT NULL);
PROCEDURE CUSTOM_REPORT_CHECK(
    p_process_id       IN NUMBER,
    P_APPLICATION_NAME IN VARCHAR2 DEFAULT NULL);
PROCEDURE SEEDED_FLAG_CHECK(
    P_PROCESS_ID       IN NUMBER);
PROCEDURE LOV_CHECK(
    p_process_id       in number);
PROCEDURE seeded_report_sql_check(
    p_process_id       in number,
    P_APPLICATION_NAME IN VARCHAR2 DEFAULT NULL);
  PROCEDURE multi_reports_sub(
    p_process_id       IN NUMBER,
    p_application_name IN VARCHAR2 DEFAULT NULL);
END eis_rs_reports_check_list_pkg;
/

