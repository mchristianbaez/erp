create or replace
PACKAGE BODY      xxwc_inv_item_cross_ref_pkg
/************************************************************************************************************************
 *
 * HEADER
 *   XXWC_INV_ITEM_CROSS_REF_PKG
 *
 * PROGRAM NAME
 *  XXWC_INV_ITEM_CROSS_REF_PKG.pkb
 *
 * DESCRIPTION
 *  This package body will upload the item cross reference in to base table using the API
 *
 *
 * PARAMETERS
 * ==========
 * NAME              DESCRIPTION
.* ----------------- ------------------------------------------------------
    None
 *
 * DEPENDENCIES
 *   None
 *
 * CALLED BY
 *   Item Cross reference Upload WebADI
 *
 * LAST UPDATE DATE   12-FEB-2015
 *
 * HISTORY
 * =======
 *
 * VERSION DATE        AUTHOR(S)       		DESCRIPTION
 * ------- ----------- --------------- -----------------------------------------------------------------------------
 * 1.0    12-FEB-2015 Gajendra M      		Created for Item Cross reference Upload WebADI for TMS # 20141212-00129
 * 2.0    08-JUN-2015 P.Vamshi        		TMS#20150511-00067-SCM: Issue in package XXWC_INV_ITEM_CROSS_REF_PKG
 * 3.0    20-JUL-2015 Maharajan Shunmugam 	TMS# 20150708-00250 Modify the XXWC_INV_ITEM_CROSS_REF_PKG to remove error emailing from the data validation
 * 4.0    21-AUG-2015 Hari Prasad M         TMS# 20150806-00126 Improve Exception handling in Package XXWC_INV_ITEM_CROSS_REF_PKG    
 section.
 ***********************************************************************************************************************/
AS
   /**************************************************************************
    *
    * PROCEDURE
    * upload_xref
    *
    * DESCRIPTION
    *  This procedure will upload the cross reference in to base table thorough Web ADI
    *
    * PARAMETERS
    * ==========
    * NAME                   TYPE                  DESCRIPTION
   .* ----------------- -------- ---------------------------------------------
    * P_TRANSACTION_TYPE     VARCHAR2 IN      Transaction Type
    * P_ITEM_NUMBER          VARCHAR2 IN      inventory item number
    * P_TRANSACTION_TYPE     VARCHAR2 IN      transaction type for cross-reference
    * P_CROSS_TYPE           VARCAHR2 IN      cross-reference type
    * XREF_CODE              VARCHAR2 IN      ross-reference
    * P_NEW_XREF_CODE        VARCHAR2 IN      new cross-reference
    * P_MST_VEN_NO           VARCHAR2 IN      Master Vendor Number
    * P_MST_VEN_NAME         VARCHAR2 IN      Master Vendor Name
    * P_DESCRIPTION          VARCHAR2 IN      ITem Description
    * P_MST_VEN_NO_NAME      VARCHAR2 IN      Master Vendor Name Read Only
    *
    * RETURN VALUE
    *  None
    *************************************************************************/
   PROCEDURE upload_xref (p_item_number        IN VARCHAR2,
                          p_transaction_type   IN VARCHAR2,
                          p_cross_type         IN VARCHAR2,
                          p_xref_code          IN VARCHAR2 DEFAULT NULL,
                          p_new_xref_code      IN VARCHAR2,
                          p_mst_ven_no         IN VARCHAR2 DEFAULT NULL,
                          p_mst_ven_name       IN VARCHAR2 DEFAULT NULL,
                          p_description        IN VARCHAR2 DEFAULT NULL,
                          p_mst_ven_no_name    IN VARCHAR2 DEFAULT NULL)
   AS
      -- --------------------------------------------------------------
      -- Declaring Global Exception
      -- --------------------------------------------------------------
      xxwc_error         EXCEPTION;
      -- --------------------------------------------------------------
      -- Declaring local variables
      -- --------------------------------------------------------------
      l_org_id           NUMBER := FND_PROFILE.VALUE ('XXWC_ITEM_MASTER_ORG'); -- Value defaulted by Vamshi in TMS#20150511-00067 V2.0
      l_item_id          NUMBER;
      l_upc_code         VARCHAR2 (255);
      l_xref_type        VARCHAR2 (25);
      l_xref_id          NUMBER;
      l_ven_num          VARCHAR2 (150);
      l_vendor_no        VARCHAR2 (150);
      l_xref_tbl         mtl_cross_references_pub.xref_tbl_type;
      l_cnt              NUMBER := 0;
      l_return_status    VARCHAR2 (1);
      l_msg_count        NUMBER;
      l_message_list     error_handler.error_tbl_type;
      l_module           VARCHAR2 (100) := 'INV';
      l_error_message    VARCHAR2 (2000);
      l_api_error_msg    VARCHAR2 (4000);
      l_err_callfrom     VARCHAR2 (175) := 'XXWC_INV_ITEM_CROSS_REF_PKG';
      l_err_callpoint    VARCHAR2 (175) := 'START';
      l_distro_list      VARCHAR2 (80) := 'HDSOracleDevelopers@hdsupply.com';
      l_invoke_api       NUMBER := 0;
      l_display_msg      VARCHAR2 (4000) := NULL;
      l_operating_unit   VARCHAR2 (10) := fnd_profile.VALUE ('ORG_ID'); --Added by Vamshi in TMS#20150511-00067 V2.0
   BEGIN
      fnd_message.clear;
      /*       -- commented below code in TMS#20150511-00067 by Vamshi (V2.0)
      l_err_callpoint := 'Before Organization Code validatiion logic';
          BEGIN
            SELECT organization_id
              INTO l_org_id
              FROM mtl_parameters
             WHERE organization_code = 'MST';
          EXCEPTION
            WHEN no_data_found THEN
              l_invoke_api  := 1;
              l_display_msg := 'Organization MST is not valid.';
          END;
      */
      l_err_callpoint := 'Before Item Number validatiion logic';

      BEGIN
         SELECT inventory_item_id
           INTO l_item_id
           FROM mtl_system_items_b
          WHERE organization_id = l_org_id AND segment1 = p_item_number;
      EXCEPTION
         WHEN NO_DATA_FOUND
         THEN
            l_invoke_api := 1;
            l_display_msg :=
               l_display_msg || ' Item ' || p_item_number || ' is not valid.';
			   ------Added below begin for v 4.0
  WHEN OTHERS 
  THEN
  RAISE xxwc_error;
			---- Added below end for v 4.0
  
      END;

      l_err_callpoint := 'Before Cross Reference Type Validatiion logic';

      BEGIN
         SELECT cross_reference_type
           INTO l_xref_type
           FROM mtl_cross_reference_types
          WHERE     cross_reference_type = p_cross_type
                AND TRUNC (SYSDATE) <= NVL (disable_date, SYSDATE + 1);
      EXCEPTION
         WHEN NO_DATA_FOUND
         THEN
            l_invoke_api := 1;
            l_display_msg :=
                  l_display_msg
               || ' Cross Refrence Type '
               || p_cross_type
               || ' is not valid.';
     		   ---Added below begin for v 4.0
     WHEN OTHERS 
     THEN
     RAISE xxwc_error;
			---- Added below end for v 4.0
  			   
      END;

      IF p_transaction_type IS NULL
      THEN
         l_invoke_api := 1;
         l_display_msg :=
               l_display_msg
            || ' CREATE or UPDATE or DELETE TRANSACTION_TYPE cannot be blank.';
      END IF;

      IF ( (p_transaction_type = 'DELETE') AND (p_xref_code IS NULL))
      THEN
         l_invoke_api := 1;
         l_display_msg :=
            l_display_msg || ' DELETE XREF_CODE cannot be blank.';
      END IF;

      IF ( (p_transaction_type = 'DELETE') AND (p_new_xref_code IS NOT NULL))
      THEN
         l_invoke_api := 1;
         l_display_msg :=
            l_display_msg || ' DELETE NEW_XREF_CODE should be blank.';
      END IF;

      IF (    (p_transaction_type = 'UPDATE')
          AND (p_new_xref_code IS NULL)
          AND (p_mst_ven_no IS NULL))
      THEN
         l_invoke_api := 1;
         l_display_msg :=
               l_display_msg
            || ' UPDATE NEW_XREF_CODE or VENDOR/UPC_OWNER_NUM cannot be blank';
      END IF;

      IF ( (p_transaction_type = 'CREATE') AND (p_new_xref_code IS NULL))
      THEN
         l_invoke_api := 1;
         l_display_msg :=
            l_display_msg || ' CREATE NEW_XREF_CODE cannot be blank.';
      END IF;

      IF ( (p_transaction_type = 'CREATE') AND (p_xref_code IS NOT NULL))
      THEN
         l_invoke_api := 1;
         l_display_msg :=
            l_display_msg || ' CREATE XREF_CODE should be blank.';
      END IF;

      IF ( (p_new_xref_code IS NOT NULL) AND (p_transaction_type = 'CREATE'))
      THEN
         l_err_callpoint := 'Before Cross Reference value validatiion logic';

         BEGIN
            SELECT cross_reference
              INTO l_upc_code
              FROM mtl_cross_references_b
             WHERE     cross_reference_type = p_cross_type
                   AND inventory_item_id = l_item_id
                   AND cross_reference = p_new_xref_code;

            l_invoke_api := 1;
            l_display_msg :=
                  l_display_msg
               || ' Cross Refrence '
               || p_new_xref_code
               || ' already exists.';
         EXCEPTION
            WHEN NO_DATA_FOUND
            THEN
               NULL;
		 		   ------Added below begin for v 4.0
         WHEN OTHERS 
         THEN
         RAISE xxwc_error;
			---- Added below end for v 4.0
  	   
         END;
      END IF;

      IF p_transaction_type = 'UPDATE'
      THEN
         l_err_callpoint :=
            'Before Cross Reference Id and Cross Referecne Validatiion logic';

         BEGIN
            SELECT cross_reference_id, cross_reference
              INTO l_xref_id, l_upc_code
              FROM mtl_cross_references_b
             WHERE     cross_reference_type = p_cross_type
                   AND inventory_item_id = l_item_id
                   AND cross_reference = NVL (p_new_xref_code, p_xref_code);
         EXCEPTION
            WHEN OTHERS
            THEN
               l_invoke_api := 1;
               l_display_msg :=
                     l_display_msg
                  || ' Cross Refrence '
                  || p_xref_code
                  || ' does not exists.';
         END;
      END IF;

      IF ( (p_xref_code IS NOT NULL) AND (p_transaction_type = 'DELETE'))
      THEN
         l_err_callpoint := 'Before Cross Reference Validatiion logic';

         BEGIN
            SELECT cross_reference_id
              INTO l_xref_id
              FROM mtl_cross_references_b
             WHERE     cross_reference_type = p_cross_type
                   AND inventory_item_id = l_item_id
                   AND cross_reference = p_xref_code;
         EXCEPTION
            WHEN NO_DATA_FOUND
            THEN
               l_invoke_api := 1;
               l_display_msg :=
                     l_display_msg
                  || ' Cross Refrence '
                  || p_xref_code
                  || ' does not exists.';
	     		   ------Added below begin for v 4.0
        WHEN OTHERS 
        THEN
        RAISE xxwc_error;
			---- Added below end for v 4.0
  			  
         END;
      END IF;

      IF ( (p_transaction_type = 'CREATE') AND (p_mst_ven_no IS NOT NULL))
      THEN
         l_err_callpoint := 'Before Vendor Number Validatiion logic';

         BEGIN
            -- added regexp function in below query by Vamshi in TMS#20150511-00067
            SELECT REGEXP_REPLACE (attribute1, '[[:cntrl:]]')
              INTO l_vendor_no
              FROM mtl_cross_references_b a
             WHERE     1 = 1
                   AND organization_id = l_org_id
                   AND inventory_item_id = l_item_id
                   AND cross_reference_type = p_cross_type
                   AND attribute1 = p_mst_ven_no
                   AND EXISTS
                          (SELECT 'X'
                             FROM mtl_cross_references_b b
                            WHERE     b.inventory_item_id =
                                         a.inventory_item_id
                                  AND b.organization_id = a.organization_id
                                  AND b.cross_reference_type =
                                         a.cross_reference_type
                                  AND b.attribute1 = a.attribute1);

            l_invoke_api := 1;
            l_display_msg :=
                  l_display_msg
               || ' Vendor Number '
               || p_mst_ven_no
               || ' is already exists.';
         EXCEPTION
            WHEN NO_DATA_FOUND
            THEN
               l_invoke_api := 0;
			   		   ------Added below begin for v 4.0
          WHEN OTHERS 
          THEN
          RAISE xxwc_error;
			---- Added below end for v 4.0
  
         END;
      END IF;

      IF (l_invoke_api = 1)
      THEN
         --fnd_message.set_name ('XXWC', 'XXWC_INV_ITEM_CROSS_REF_PKG');   --commented for Ver# 3.0
         --fnd_message.set_token ('ERROR_MESSAGE', l_display_msg);	   --commented for Ver# 3.0
         RAISE xxwc_error;
      END IF;

      l_err_callpoint := 'Before Org independent flag logic';

      IF (l_invoke_api = 0)
      THEN
         l_xref_tbl (1).organization_id := NULL;
         l_xref_tbl (1).org_independent_flag := 'Y';
         l_err_callpoint := 'Before populating API variables';
         l_xref_tbl (1).inventory_item_id := l_item_id;
         l_xref_tbl (1).cross_reference_type := p_cross_type;
         l_xref_tbl (1).cross_reference := NVL (p_new_xref_code, l_upc_code);
         l_xref_tbl (1).attribute1 := p_mst_ven_no;
         l_xref_tbl (1).attribute_category := l_operating_unit; --'162';  -- Changed by Vamshi in TMS#TMS#20150511-00067 (V2.0)
         l_xref_tbl (1).transaction_type := p_transaction_type;
         l_xref_tbl (1).cross_reference_id := l_xref_id;
         l_xref_tbl (1).last_update_date := SYSDATE;
         l_xref_tbl (1).last_updated_by := fnd_global.user_id;
         l_xref_tbl (1).creation_date := SYSDATE;
         l_xref_tbl (1).created_by := fnd_global.user_id;
         l_xref_tbl (1).last_update_login := fnd_global.login_id;
         l_xref_tbl (1).request_id := fnd_global.conc_request_id;
         l_xref_tbl (1).program_application_id := fnd_global.prog_appl_id;
         l_xref_tbl (1).program_id := fnd_global.conc_program_id;
         l_xref_tbl (1).program_update_date := SYSDATE;
         l_err_callpoint :=
            'Before call to MTL_CROSS_REFERENCES_PUB.Process_XRef';
         mtl_cross_references_pub.process_xref (
            p_api_version     => 1.0,
            p_init_msg_list   => fnd_api.g_true,
            p_commit          => fnd_api.g_false,
            p_xref_tbl        => l_xref_tbl,
            x_return_status   => l_return_status,
            x_msg_count       => l_msg_count,
            x_message_list    => l_message_list);

         IF l_return_status <> 'S'
         THEN
            l_display_msg := l_display_msg || ' API Error. ';
          --  fnd_message.set_name ('XXWC', 'XXWC_ADI_ERROR_MSG');    --commented for Ver# 3.0
           -- fnd_message.set_token ('ERROR_MESSAGE', l_display_msg); --commented for Ver# 3.0
            RAISE xxwc_error;
         END IF;

         COMMIT;
      END IF;

      l_err_callpoint := 'Before End';
   EXCEPTION
      WHEN xxwc_error
      THEN
         ROLLBACK;
         --l_error_message := l_display_msg; --- Commented for v 4.0
         l_error_message :=  substr(l_display_msg||'-'||sqlerrm,1,4000);
         fnd_message.set_name ('XXWC', 'XXWC_ADI_ERROR_MSG');
        -- fnd_message.set_token ('ERROR_MESSAGE', l_display_msg); ---Commented for v 4.0
		fnd_message.set_token ('ERROR_MESSAGE', substr(l_display_msg||'-'||sqlerrm,1,4000)); ---Added for v 4.0
         -- Added below exception code by Vamshi  in TMS#TMS#20150511-00067 (V2.0).
--commented for Ver# 3.0
        /* xxcus_error_pkg.xxcus_error_main_api (
            p_called_from         => l_err_callfrom,
            p_calling             => l_err_callpoint,
            p_ora_error_msg       => SQLERRM,
            p_error_desc          => SUBSTR (l_error_message, 1, 240),
            p_distribution_list   => l_distro_list,
            p_module              => l_module);*/       
      WHEN OTHERS
      THEN
         ROLLBACK;
		    ---Commented below for v 4.0
         /*l_error_message :=
               'Un-Identified error occurred and Development Team is notified - '
            || l_display_msg; --Changed by Vamshi  in TMS#TMS#20150511-00067 (V2.0).*/
			---Commented End for v 4.0
			---Added  below for v 4.0
			 l_error_message :=
               'Un-Identified error occurred and Development Team is notified - '
            || substr(l_display_msg||'-'||sqlerrm,1,4000); 
			---Added  end for v 4.0
         fnd_message.set_name ('XXWC', 'XXWC_ADI_ERROR_MSG');
         fnd_message.set_token ('ERROR_MESSAGE', l_error_message);
         xxcus_error_pkg.xxcus_error_main_api (
            p_called_from         => l_err_callfrom,
            p_calling             => l_err_callpoint,
            p_ora_error_msg       => SQLERRM,
            p_error_desc          => SUBSTR (l_error_message, 1, 240),
            p_distribution_list   => l_distro_list,
            p_module              => l_module);
   END upload_xref;
END XXWC_INV_ITEM_CROSS_REF_PKG;
/