/*************************************************************************
  $Header ALT_XXWC_EXPECT_RECTS_REP_GTT_TBL.sql $
  Module Name: ALT_XXWC_EXPECT_RECTS_REP_GTT_TBL.sql

  PURPOSE:   Modifying columns data type.

  REVISIONS:
  Ver        Date        Author                     Description
  ---------  ----------  ---------------         -------------------------
  1.0        12/12/2017  P.Vamshidhar            TMS#20171211-00325 - Order on Excepted Receipts Report
**************************************************************************/
ALTER TABLE XXWC.XXWC_EXPECT_RECTS_REP_GTT_TBL
   MODIFY (SORT1 DATE,
           SORT2 DATE,
           SORT3 NUMBER,
           SORT4 NUMBER,
           SORT5 NUMBER,
           SORT6 NUMBER);
/		   