SET SERVEROUTPUT ON
SET VERIFY OFF;

SET SERVEROUTPUT ON SIZE 100000;

DECLARE
   /**************************************************************************
   *   Datafix for Disabling unused Concurrent Programs
   *
   *
   *   PURPOSE:  Datafix for Disabling unused Concurrent Programs.
   *   REVISIONS:
   *   Ver        Date        Author                     Description
   *   ---------  ----------  ---------------         -------------------------
   *   1.0        11/29/2017  Krishna                 Initial Version TMS# 20171113-00008
   *                                                  Disable not used concurrent programs.
   * ***************************************************************************/
   CURSOR c_conc_prg_det
   IS
      SELECT FCP.concurrent_program_name short_name,
             FCP.enabled_flag,
             FA.application_name
        FROM apps.FND_CONCURRENT_PROGRAMS_VL FCP, apps.FND_APPLICATION_TL FA
       WHERE     1 = 1
             AND FCP.application_id = FA.application_id
             AND FCP.user_concurrent_program_name IN ('XXWC INV Item X - Reference Conversion Program',
                                                      'XXWC ASL Conversion',
                                                      'XXWC DMS Single Order File Creation',
                                                      'XXWC Item Cross-Reference Load',
                                                      'XXWC Create Segment Modifiers',
                                                      'XXWC OM Print Bulk SO by Salesrep/Date',
                                                      'XXWC OM Prism RMA Report',
                                                      'XXWC Cycle Count Scheduler for All Orgs',
                                                      'XXWC Hazmat Bill Of Lading Report',
                                                      'XXWC Quote Export Report',
                                                      'XXWC Pricing Segmentation - Decommission Process',
                                                      'XXWC Best Price Wins - Decommission Process',
                                                      'XXWC INV PUBD Onhand Conversion SQL Loader Program',
                                                      'XXWC INV PUBD Update BOD after on hand conversion',
                                                      'XXWC Quote Export WebADI Template',
                                                      'XXWC INV PUBD Onhand Conversion Program',
                                                      'XXWC OM Customer Sales Receipt - NEW',
                                                      'XXWC OM Sales Receipt - NEW',
                                                      'XXWC OM Pricing Guard Rail Error Report',
                                                      'XXWC AR Receipts Conversion SQL Loader Program',
                                                      'XXWC AR Load Customer Contacts',
                                                      'XXWC AP Open Invoice Conversion Import Program',
                                                      'XXWC AR Invoice Conversion Import Program',
                                                      'XXWC AP Supplier Contacts SQL Loader Program',
                                                      'XXWC AR Invoice Conversion SQL Loader Program',
                                                      'XXWC AR Customer Conversion',
                                                      'XXWC Pricing Segmentation - Item Inactivated',
                                                      'XXWC EGO Load Top level Attrs',
                                                      'XXWC PRISM Sales History Upload',
                                                      'XXWC AP Closed Invoice Conversion SQL Loader Program',
                                                      'XXWC AP Open Invoice Conversion SQL Loader Program',
                                                      'XXWC Rental Sales Order Conversion',
                                                      'XXWC AP Supplier Conversion Import Program',
                                                      'XXWC Prism To EBS AR Invoice Interface',
                                                      'XXWC Backordered Transactions Processing - Batch',
                                                      'XXWC AR Customer Contact Points SQL Loader Program',
                                                      'XXWC AR Prism POA Lockbox File Creation',
                                                      'XXWC QP Price List Conversion Progam',
                                                      'XXWC Prism-EBS Customer Interface Processing',
                                                      'XXWC AP Supplier Banks SQL Loader Program',
                                                      'XXWC EGO Load Features and Benefits',
                                                      'XXWC GetPaid ARTRAN - Load Dump Table',
                                                      'XXWC Pick Selection List Generation - SRS',
                                                      'XXWC AR Sales Invoice Interface - Load Staging',
                                                      'XXWC AR Prism Refund Process',
                                                      'XXWC AP Supplier Sites SQL Loader Program',
                                                      'XXWC AR Process Prism Lockbox',
                                                      'XXWC AR Site Use Location Name DataFix',
                                                      'XXWC AR Customer Sites SQL Loader Program',
                                                      'XXWC AP Invoice Interface - Load Staging',
                                                      'XXWC AR Receipts Conversion Import Program',
                                                      'XXWC AP Closed Invoice Conversion Import Program',
                                                      'XXWC QP Price List Header Conversion Staging Load',
                                                      'XXWC QP Bulk Modifer List HDR SQL Loader Program',
                                                      'XXWC CSP Header Loader',
                                                      'XXWC QP Price List Line Conversion Staging Load',
                                                      'XXWC GetPaid AREXTI - Load Dump Table',
                                                      'XXWC AR Prism NON POA Lockbox File Creation',
                                                      'XXWC Prism To EBS AP Invoice Interface',
                                                      'XXWC OE Open Sales Order Lines Load Program',
                                                      'XXWC Cycle Count Update Excluded Item',
                                                      'XXWC Prism To EBS AR Invoice Interface - Update Revenue Amount',
                                                      'XXWC AR Inactivate Customers - (Converted)',
                                                      'XXWC AR Customers SQL Loader Program',
                                                      'XXWC AP Supplier Banks Conversion Import Program',
                                                      'XXWC AR Customer OE Ship To Conversion',
                                                      'HDS AP GSC: Update Supplier Site Flags [Purchasing/Debit Memo]',
                                                      'XXWC GetPaid ARCUST - Load Dump Table',
                                                      'XXWC QP Generate Market Category Price List',
                                                      'XXWC AR Customer Balances Outbound Interface',
                                                      'XXWC AP Suppliers SQL Loader Program',
                                                      'XXWC GetPaid ARMAST - Load Dump Table',
                                                      'HDS AP SQL*Loader-Bank Map for Conv ',
                                                      'XXWC Quote Analysis Report',
                                                      'XXWC Prism To EBS AP Invoice Interface - Attach Documents',
                                                      'XXWC Prism To EBS AR DebitMemo Interface',
                                                      'XXWC INV Item Xref Conversion SQL Loader Program',
                                                      'XXWC ASL PO Conversion SQL Loader')
             AND FCP.enabled_flag = 'Y';

   lp_user_id        NUMBER;                                       --Vamshi Id
   lp_resp_id        NUMBER;
   lp_resp_appl_id   NUMBER;
BEGIN
   BEGIN
      SELECT user_id
        INTO lp_user_id
        FROM apps.fnd_user
       WHERE user_name = 'VP038429';
   EXCEPTION
      WHEN OTHERS
      THEN
         lp_user_id := 33710;                                      --Vamshi Id
   END;

   BEGIN
      SELECT responsibility_id, application_id
        INTO lp_resp_id, lp_resp_appl_id
        FROM apps.fnd_responsibility a
       WHERE a.responsibility_key = 'XXWC_IT_FUNC_CONFIGURATOR';
   EXCEPTION
      WHEN OTHERS
      THEN
         lp_resp_id := 50861;              --'XXWC_IT_FUNCTIONAL CONFIGURATOR'
         lp_resp_appl_id := 20005;             --Responsibility Application Id
   END;

   fnd_global.apps_initialize (lp_user_id, lp_resp_id, lp_resp_appl_id);

   mo_global.set_policy_context ('S', 162);

   FOR J IN c_conc_prg_det
   LOOP
      BEGIN
         fnd_program.enable_program (j.short_name,             --CP Short Name
                                                  j.application_name, -- Application Name
                                                                     'N'); --Enable Flag 'Y' or 'N'

         COMMIT;
      EXCEPTION
         WHEN OTHERS
         THEN
            DBMS_OUTPUT.PUT_LINE ('Main Exception : ' || SQLERRM);
      END;

      DBMS_OUTPUT.PUT_LINE (j.short_name || ' is disabled ');
   END LOOP;
END;
/