SET SERVEROUTPUT ON
SET VERIFY OFF;

SET SERVEROUTPUT ON SIZE 100000;

DECLARE
   /**************************************************************************
      *
      *   PURPOSE:  To disable not used Froms.
      *   REVISIONS:
      *   Ver        Date        Author                     Description
      *   ---------  ----------  ---------------         -------------------------
      *   1.0        11/29/2017  Krishna                 Initial Version TMS# 20171113-00016
      *                                                  Disable not used Forms.
      * ***************************************************************************/
   CURSOR cur_form_id
   IS
        SELECT ffv.form_id,
               FFV.USER_FORM_NAME,
               COUNT (FLRF.LOGIN_ID),
               MIN (TRUNC (FLRF.START_TIME))
          FROM APPS.FND_FORM_VL FFV, apps.FND_LOGIN_RESP_FORMS FLRF
         WHERE     1 = 1
               AND USER_FORM_NAME IN ('XXWC Vendor Minimum Pilot',
                                      'XXWC Pricing GuardRail Setup',
                                      'XXWC Pricing GuardRail Info',
                                      'XXWC OM Price Check Form')
               AND FFV.FORM_ID = FLRF.FORM_ID(+)
      GROUP BY ffv.form_id, FFV.FORM_NAME, FFV.USER_FORM_NAME
        --HAVING COUNT (FLRF.LOGIN_ID) < 1
      ORDER BY COUNT (FLRF.LOGIN_ID);

   CURSOR cur_fuc_id (p_form_id NUMBER)
   IS
      SELECT FUNCTION_ID
        FROM apps.fnd_form_functions
       WHERE form_id = p_form_id;

   CURSOR cur_menu_id (p_func_id NUMBER)
   IS
      SELECT MENU_ID,
             ENTRY_SEQUENCE,
             function_id,
             PROMPT,
             DESCRIPTION
        FROM APPS.FND_MENU_ENTRIES_VL
       WHERE function_id = p_func_id;

   l_user_id           NUMBER;                                     --Vamshi Id
   l_resp_id           NUMBER;
   l_resp_appl_id      NUMBER;

   l_menu_enttry_seq   NUMBER;
   l_row_id            VARCHAR2 (20);
   l_function_id       NUMBER;
   l_seq               NUMBER;
BEGIN
   BEGIN
      SELECT user_id
        INTO l_user_id
        FROM apps.fnd_user
       WHERE user_name = 'VP038429';
   EXCEPTION
      WHEN OTHERS
      THEN
         l_user_id := 33710;                                       --Vamshi Id
   END;

   BEGIN
      SELECT responsibility_id, application_id
        INTO l_resp_id, l_resp_appl_id
        FROM apps.fnd_responsibility a
       WHERE a.responsibility_key = 'XXWC_IT_FUNC_CONFIGURATOR';
   EXCEPTION
      WHEN OTHERS
      THEN
         l_resp_id := 50861;               --'XXWC_IT_FUNCTIONAL CONFIGURATOR'
         l_resp_appl_id := 20005;              --Responsibility Application Id
   END;

   fnd_global.apps_initialize (l_user_id, l_resp_id, l_resp_appl_id);

   mo_global.set_policy_context ('S', 162);

   FOR i IN cur_form_id
   LOOP
      FOR j IN cur_fuc_id (i.form_id)
      LOOP
         FOR k IN cur_menu_id (j.FUNCTION_ID)
         LOOP
            DBMS_OUTPUT.PUT_LINE ('Passing the cursor values to API');

            BEGIN
               FND_MENU_ENTRIES_PKG.UPDATE_ROW (
                  x_menu_id             => k.MENU_ID,               -- Menu ID
                  x_entry_sequence      => k.ENTRY_SEQUENCE, -- Sequence Number
                  x_sub_menu_id         => NULL,                -- Sub menu ID
                  x_function_id         => k.function_id,       -- Function ID
                  x_grant_flag          => 'N',                  -- Grant Flag
                  x_prompt              => k.PROMPT,                 -- Prompt
                  x_description         => k.DESCRIPTION, --'Function for Testing',
                  x_last_update_date    => SYSDATE,
                  x_last_updated_by     => fnd_global.user_id, -- Last Updated by
                  x_last_update_login   => fnd_global.login_id -- Last update login
                                                              );
            EXCEPTION
               WHEN OTHERS
               THEN
                  DBMS_OUTPUT.PUT_LINE (SQLERRM || ' Is the error');
            END;
         END LOOP;                                                   -- k loop
      END LOOP;                                                      -- j loop

      DBMS_OUTPUT.PUT_LINE (i.user_form_name || ' is disabled ');
   END LOOP;                                                         -- i loop

   COMMIT;
EXCEPTION
   WHEN OTHERS
   THEN
      DBMS_OUTPUT.PUT_LINE (SQLERRM || ' Is the error');
END;
/
   