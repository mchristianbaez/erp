--//============================================================================
--//
--// Object Name         :: XXCUS_CONCUR_EMP_EXPORT_310_V
--//
--// Object Type         :: View
--//
--// Object Description  :: This view shows all the Contractors in HRMS application.
--//
--// Version Control
--//============================================================================
--// Vers    Author             Date          Description
--//----------------------------------------------------------------------------
--// 1.0     Ashwin Sridhar     08/06/2018    Initial Build - Task ID: 20180227-00179-Adding Contractors to Concur Employee Feed
--// 1.1     Ashwin Sridhar     17/08/2018    TMS#20180815-00009-AHH Associate turn Expense On in Concur
--// 1.2     Ashwin Sridhar     05/09/2018    TMS#20180905-00001-AHH Associate turn Expense On in Concur
--//============================================================================
  CREATE OR REPLACE FORCE EDITIONABLE VIEW "APPS"."XXCUS_CONCUR_EMP_EXPORT_310_V" ("TRX_TYPE", "EMPLOYEE_ID", "LOGIN_ID", "FIRST_NAME", "MIDDLE_NAME", "LAST_NAME", "EMAIL_ADDRESS", "PASSWD", "LOCALE_CODE", "EXPENSE_USER", "EXPENSE_APPROVER", "INVOICE_USER", "INVOICE_APPROVER", "TRAVEL_USER", "ACTIVE", "NO_MIDDLE_NAME", "LOCATE_AND_ALERT", "EXPENSEIT_USER_ROLE", "FUTURE_USE_5", "FUTURE_USE_6", "FUTURE_USE_7", "FUTURE_USE_8", "FUTURE_USE_9", "FUTURE_USE_10") AS 
  select '310' trx_type
           ,to_char(to_number(regexp_replace(concur_temp.ntid, '[^0-9]+' ))) employee_id
           ,concur_temp.email_addr    login_id
           ,concur_temp.first_name  first_name 
           ,trim(concur_temp.middle_name) middle_name
           ,concur_temp.last_name  last_name
           ,concur_temp.email_addr email_address
           ,to_char(null) passwd
           ,control_table.locle_code locale_code
           ,'N' expense_user
           ,'N' expense_approver
           ,'N'  invoice_user
           ,'N'  invoice_approver
           ,'Y' Travel_user -- Always Y for contractors 
           ,case
               -- The number of days used here is based on the number of days contractors are retained in the view hdscmmn.hr_ad_all_vw after a termination date is set. 
               when (
                                ( 
                                    ( trunc(termination_dt) + 14 ) >= trunc(sysdate) and control_table.business_unit <> 'WW1US'
                                ) 
                                   or user_acct_status_code = 'A'
                          ) then 'Y'
               else 'N'
            end Active  
        ,case
           when trim(concur_temp.middle_name) is null then 'Y'
           else 'N'
         end no_middle_name
        ,'Not enrolled'   locate_and_alert
        ,'N'  expenseIT_user_role
        ,to_char(null) future_use_5
        ,to_char(null) future_use_6
        ,to_char(null) future_use_7
        ,to_char(null) future_use_8
        ,to_char(null) future_use_9
        ,to_char(null) future_use_10    
from   hdscmmn.hr_ad_all_vw@eaapxprd.hsi.hughessupply.com           concur_temp 
           ,hdscmmn.concur_bu_xref@eaapxprd.hsi.hughessupply.com      control_table
where 1 =1
and usertype ='Contractor'
and email_addr is not null
and to_number(regexp_replace(concur_temp.ntid, '[^0-9]+' )) is not null
and to_number(regexp_replace(concur_temp.spvr_ntid, '[^0-9]+' )) is not null
and ( to_number(regexp_replace(concur_temp.spvr_ntid, '[^0-9]+' )) is not null AND to_number(regexp_replace(concur_temp.spvr_emplid, '[^0-9]+' )) is not null )               
          and (
                                ( 
                                    ( trunc(concur_temp.termination_dt) + 14 ) >= trunc(sysdate) and control_table.business_unit <> 'WW1US')
                                   or 
                                   (concur_temp.user_acct_status_code = 'A'  and control_table.business_unit <> 'WW1US')                                
                  )
and control_table.business_unit(+) =concur_temp.ps_business_unit
AND to_number(regexp_replace(concur_temp.NTID, '[^0-9]+' )) NOT IN (SELECT EMPLOYEE_NUMBER FROM XXCUS.XXCUS_AHH_CONTRACTOR_TO_CONCUR) --Added by Ashwin Sridhar on 05-Sep-2018 for TMS#20180905-00001 for v1.2
/*Change Started for v1.1 by Ashwin.S on 17-Aug-2018 for TMS#20180815-00009*/
/*AND to_number(regexp_replace(concur_temp.NTID, '[^0-9]+' )) NOT IN ('69273','69191','69181','69577','69232','69423','69281','69334','69489','69280'
,'69759','69197','69243','69341','69296','69455','69620','69347','69300','69274','69522','69743','69250','69357'
,'69702','69782','69665','69414','69787','69555','69237','69547','69386','69611','69424','69478','69323','69477'
,'69311','69310','69342','69270','69687','69470','69691','69189','69229','69302','69319','69351','69435','69480'
,'69585','69592','70283','70360','70703')*/ --Commented by Ashwin Sridhar on 05-Sep-2018 for TMS#20180905-00001 for v1.2
/*Change Ended for v1.1*/
order by ntid desc;

COMMENT ON TABLE "APPS"."XXCUS_CONCUR_EMP_EXPORT_310_V"  IS 'TMS: 20180227-00179';