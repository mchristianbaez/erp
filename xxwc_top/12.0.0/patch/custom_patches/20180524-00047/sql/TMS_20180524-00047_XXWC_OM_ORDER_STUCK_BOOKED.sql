/*************************************************************************
  $Header TMS_20180524-00047_XXWC_OM_ORDER_STUCK_BOOKED.sql $
  Module Name: TMS_XXWC_OM_ORDER_STUCK_BOOKED.sql

  PURPOSE:   Created to process the order headers stucked in booked status
             and lines are in CLOSED or CANCELLED status.

  REVISIONS:
  Ver        Date        Author               Description
  ---------  ----------  ---------------      -------------------------
  1.0        05/30/2018  Pattabhi Avula       TMS#20180524-00047
  **************************************************************************/
SET SERVEROUTPUT ON SIZE 1000000;
DECLARE
BEGIN
 DBMS_OUTPUT.put_line ('TMS: Datafix script    , Before Update');
 -- Updating the headers table
		  UPDATE oe_order_headers_all
		     SET flow_status_code = 'CLOSED'
                 ,open_flag = 'N'
		   WHERE order_number = 28405361;
		   	 
	 COMMIT;
		   
DBMS_OUTPUT.put_line ('Records update-' || SQL%ROWCOUNT);

	  DBMS_OUTPUT.put_line ('TMS: 20180524-00047  , End Update');
EXCEPTION
 WHEN OTHERS THEN
 DBMS_OUTPUT.put_line ('TMS: 20180524-00047 , Errors : ' || SQLERRM);
END;
/