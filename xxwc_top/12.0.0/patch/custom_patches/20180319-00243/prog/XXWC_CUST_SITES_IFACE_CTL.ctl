-- ******************************************************************************************************
-- *  Copyright (c) 2011 Lucidity Consulting Group
-- *  All rights reserved.
-- ******************************************************************************************************
-- *   $Header XXWC_CUST_IFACE_CTL.ctl $
-- *   Module Name: XXWC_CUST_IFACE_CTL.ctl
-- *
-- *   PURPOSE:   This is the control file for loading the staging table XXWC.XXWC_AR_CUST_SITE_IFACE_STG
-- *
-- *   REVISIONS:
-- *   Ver        Date        Author                     Description
-- *   ---------  ----------  ---------------         -------------------------------------------------
-- *   1.0        30/04/2018  Ashwin Sridhar          TMS#20180319-00243 - AH HARRIS Customer Interface
-- * ***************************************************************************************************
OPTIONS (SKIP=0)
LOAD DATA 
INFILE 'XXWC_AR_CUST_SITE_IFACE_STG.csv' 
BADFILE 'XXWC_AR_CUST_SITE_IFACE_STG.bad'
DISCARDFILE 'XXWC_AR_CUST_SITE_IFACE_STG.dsc'
APPEND
INTO TABLE XXWC_AR_CUST_SITE_IFACE_STG
FIELDS TERMINATED BY "|"  OPTIONALLY ENCLOSED BY '"'
TRAILING NULLCOLS
(         
ORIG_SYSTEM_REFERENCE       "TRIM(:ORIG_SYSTEM_REFERENCE)"       
,ORIG_SYSTEM                 "TRIM(:ORIG_SYSTEM)"   
,ADDRESS1                           "TRIM(:ADDRESS1)"
,ADDRESS2                           "TRIM(:ADDRESS2)"
,ADDRESS3                           "TRIM(:ADDRESS3)"
,ADDRESS4                           "TRIM(:ADDRESS4)"
,COUNTRY                            "TRIM(:COUNTRY)"
,CITY                               "TRIM(:CITY)"
,POSTAL_CODE                        "TRIM(:POSTAL_CODE)"
,STATE                              "TRIM(:STATE)"
,TAXING_STATE                       "TRIM(:TAXING_STATE)"
,COUNTY                             "TRIM(:COUNTY)"
,LEGACY_CUSTOMER_NUMBER             "TRIM(:LEGACY_CUSTOMER_NUMBER)"
,LEGACY_PARTY_SITE_NUMBER           "TRIM(:LEGACY_PARTY_SITE_NUMBER)"
,LEGACY_PARTY_SITE_NAME             "TRIM(:LEGACY_PARTY_SITE_NAME)"
,PROVINCE                           "TRIM(:PROVINCE)"
,LOCATION_ID                        "TRIM(:LOCATION_ID)"
,TIMEZONE_ID                        "TRIM(:TIMEZONE_ID)"
,IDENTIFYING_ADDRESS_FLAG           "TRIM(:IDENTIFYING_ADDRESS_FLAG)"
,PARTY_SITE_NAME                    "TRIM(:PARTY_SITE_NAME)"
,SITE_USE_TYPE                      "TRIM(:SITE_USE_TYPE)"
,PRIMARY_PER_TYPE                   "TRIM(:PRIMARY_PER_TYPE)"
,TERRITORY                          "TRIM(:TERRITORY)"
,BILL_TO_LOCATION                   "TRIM(:BILL_TO_LOCATION)"
,PRIMARY_BILL_TO_FLAG               "TRIM(:PRIMARY_BILL_TO_FLAG)"
,GSA_INDICATOR                      "TRIM(:GSA_INDICATOR)"
,PRIMARY_SALESREP_NUMBER            "TRIM(:PRIMARY_SALESREP_NUMBER)"
,PRIMARY_SHIP_TO_FLAG               "TRIM(:PRIMARY_SHIP_TO_FLAG)"
,FOB_POINT                          "TRIM(:FOB_POINT)"
,FREIGHT_TERM                       "TRIM(:FREIGHT_TERM)"
,ORDER_TYPE                         "TRIM(:ORDER_TYPE)"
,PRICE_LIST_NAME                    "TRIM(:PRICE_LIST_NAME)"
,SHIP_VIA                           "TRIM(:SHIP_VIA)"
,SHIP_SETS_INCLUDE_LINES_FLAG       "TRIM(:SHIP_SETS_INCLUDE_LINES_FLAG)"
,ARRIVALSETS_INCLUDE_LINES_FLAG     "TRIM(:ARRIVALSETS_INCLUDE_LINES_FLAG)"
,INVOICE_QUANTITY_RULE              "TRIM(:INVOICE_QUANTITY_RULE)"
,OVER_SHIPMENT_TOLERANCE            "TRIM(:OVER_SHIPMENT_TOLERANCE)"
,UNDER_SHIPMENT_TOLERANCE           "TRIM(:UNDER_SHIPMENT_TOLERANCE)"
,ITEM_CROSS_REF_PREF                "TRIM(:ITEM_CROSS_REF_PREF)"
,DATE_TYPE_PREFERENCE               "TRIM(:DATE_TYPE_PREFERENCE)"
,OVER_RETURN_TOLERANCE              "TRIM(:OVER_RETURN_TOLERANCE)"
,UNDER_RETURN_TOLERANCE             "TRIM(:UNDER_RETURN_TOLERANCE)"
,SCHED_DATE_PUSH_FLAG               "TRIM(:SCHED_DATE_PUSH_FLAG)"
,WAREHOUSE_CODE                     "TRIM(:WAREHOUSE_CODE)"
,DATES_NEGATIVE_TOLERANCE           "TRIM(:DATES_NEGATIVE_TOLERANCE)"
,DATES_POSITIVE_TOLERANCE           "TRIM(:DATES_POSITIVE_TOLERANCE)"
,FINCHRG_RECEIVABLES_TRX_NAME       "TRIM(:FINCHRG_RECEIVABLES_TRX_NAME)"
,LEGACY_PARTY_NUMBER                "TRIM(:LEGACY_PARTY_NUMBER)"
,PARTY_SITE_ID                      "TRIM(:PARTY_SITE_ID)"
,PARTY_SITE_NUMBER                  "TRIM(:PARTY_SITE_NUMBER)"
,PARTY_SITE_USE_ID                  "TRIM(:PARTY_SITE_USE_ID)"
,CUST_ACCT_SITE_ID                  "TRIM(:CUST_ACCT_SITE_ID)"
,CUST_SITE_USE_ID                   "TRIM(:CUST_SITE_USE_ID)"
,LOCATION                           "TRIM(:LOCATION)"
,OVERALL_CREDIT_LIMIT               "TRIM(:OVERALL_CREDIT_LIMIT)"
,TRX_CREDIT_LIMIT                   "TRIM(:TRX_CREDIT_LIMIT)"
,MIN_DUNNING_AMOUNT                 "TRIM(:MIN_DUNNING_AMOUNT)"
,MIN_DUNNING_INVOICE_AMPOUNT        "TRIM(:MIN_DUNNING_INVOICE_AMPOUNT)"
,MAX_INTEREST_CHARGE                "TRIM(:MAX_INTEREST_CHARGE)"
,MIN_STATEMENT_AMOUNT               "TRIM(:MIN_STATEMENT_AMOUNT)"
,CLEARING_DAYS                      "TRIM(:CLEARING_DAYS)"
,AUTO_REC_INCL_DISPUTED_FLAG        "TRIM(:AUTO_REC_INCL_DISPUTED_FLAG)"
,AUTO_REC_MIN_RECEIPT_AMOUNT        "TRIM(:AUTO_REC_MIN_RECEIPT_AMOUNT)"
,CHARGE_ON_FINANCE_CHARGE_FLAG      "TRIM(:CHARGE_ON_FINANCE_CHARGE_FLAG)"
,LOCK_BOX_MATCHING_OPTION           "TRIM(:LOCK_BOX_MATCHING_OPTION)"
,AUTOCASH_HIERARCHY_NAME            "TRIM(:AUTOCASH_HIERARCHY_NAME)"
,CREDIT_CLASSIFICATION              "TRIM(:CREDIT_CLASSIFICATION)"
,AUTOPAY_FLAG                       "TRIM(:AUTOPAY_FLAG)"
,HDS_SITE_FLAG                      "TRIM(:HDS_SITE_FLAG)"
,JOB_NAME                           "TRIM(:JOB_NAME)"
,JOB_NUMBER                         "TRIM(:JOB_NUMBER)"
,CREDIT_HOLD                        "TRIM(:CREDIT_HOLD)"
,CREDIT_ANALYST_NAME                "TRIM(:CREDIT_ANALYST_NAME)"
,LEGACY_SITE_TYPE                   "TRIM(:LEGACY_SITE_TYPE)"
,PROCESS_STATUS                     "TRIM(:PROCESS_STATUS)"
,SALESREP_ID                        "TRIM(:SALESREP_ID)"
,ATTRIBUTE1                         "TRIM(:ATTRIBUTE1)"       
,ATTRIBUTE2                         "TRIM(:ATTRIBUTE2)"       
,ATTRIBUTE3                         "TRIM(:ATTRIBUTE3)"
,ATTRIBUTE4                         "TRIM(:ATTRIBUTE4)"
,ATTRIBUTE5                         "TRIM(:ATTRIBUTE5)"
,ATTRIBUTE6                         "TRIM(:ATTRIBUTE6)"
,ATTRIBUTE7                         "TRIM(:ATTRIBUTE7)"
,ATTRIBUTE8                         "TRIM(:ATTRIBUTE8)"
,ATTRIBUTE9                         "TRIM(:ATTRIBUTE9)"
,ATTRIBUTE10                        "TRIM(:ATTRIBUTE10)"
,ATTRIBUTE11                        "TRIM(:ATTRIBUTE11)"
,ATTRIBUTE12                        "TRIM(:ATTRIBUTE12)"
,ATTRIBUTE13                        "TRIM(:ATTRIBUTE13)"
,ATTRIBUTE14                        "TRIM(:ATTRIBUTE14)"
,ATTRIBUTE15                        "TRIM(:ATTRIBUTE15)"
,ATTRIBUTE16                        "TRIM(:ATTRIBUTE16)"
,ATTRIBUTE17                        "TRIM(:ATTRIBUTE17)"
,ATTRIBUTE18                        "TRIM(:ATTRIBUTE18)"
,ATTRIBUTE19                        "TRIM(:ATTRIBUTE19)"
,ATTRIBUTE20                        "TRIM(:ATTRIBUTE20)"
,FSD                                "REPLACE(REPLACE(TRIM(:FSD),CHR(13),''),CHR(10),'')"  
)