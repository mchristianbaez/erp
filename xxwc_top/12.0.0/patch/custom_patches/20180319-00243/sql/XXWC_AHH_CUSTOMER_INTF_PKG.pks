CREATE OR REPLACE PACKAGE APPS.XXWC_AHH_CUSTOMER_INTF_PKG
IS
   /******************************************************************************************************************************************************
        $Header XXWC_AHH_CUSTOMER_INTF_PKG $
        Module Name: XXWC_AHH_CUSTOMER_INTF_PKG.pks

        PURPOSE:   AHH Customer Interface

        REVISIONS:
        Ver        Date        Author           Description
        ---------  ----------  ---------------  ------------------------------------------------------------------------------------------------
          1.0     06/20/2018    P.Vamshidhar    TMS#20180319-00243 - AH HARRIS Customer Interface
   ******************************************************************************************************************************************************/

   PROCEDURE main (x_errbuf          OUT VARCHAR2,
                   x_retcode         OUT VARCHAR2,
                   p_validation   IN     VARCHAR2);

   PROCEDURE validate_inbound_data;

   PROCEDURE preprocess;

   PROCEDURE debug (P_RECORD_ID IN NUMBER, p_msg IN VARCHAR2);

   PROCEDURE CREATE_CUSTOMER (P_CUSTOMER_NUMBER   IN     VARCHAR2,
                              p_cust_account_id      OUT NUMBER,
                              p_party_id             OUT NUMBER);

   PROCEDURE return_msg (in_msg_count     IN     NUMBER,
                         in_msg_data      IN     VARCHAR2,
                         out_return_msg      OUT VARCHAR2);

   PROCEDURE primary_bill_to (p_customer_number   IN VARCHAR2,
                              p_cust_account_id   IN NUMBER,
                              p_party_id          IN NUMBER);

   PROCEDURE primary_ship_to (p_customer_number   IN VARCHAR2,
                              p_cust_account_id   IN NUMBER,
                              p_party_id          IN NUMBER);

   PROCEDURE multiple_shipto_sites (p_customer_number   IN VARCHAR2,
                                    p_cust_account_id   IN NUMBER,
                                    p_party_id          IN NUMBER);

   PROCEDURE add_sites;

   PROCEDURE add_shipto_sites (p_customer_number   IN VARCHAR2,
                               p_cust_account_id   IN NUMBER,
                               p_party_id          IN NUMBER);
END;
/
