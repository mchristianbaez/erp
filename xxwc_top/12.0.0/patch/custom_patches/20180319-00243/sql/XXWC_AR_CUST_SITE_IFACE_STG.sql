 /*******************************************************************************************
  FILE NAME: XXWC.XXWC_AR_CUST_SITE_IFACE_STG
  PROGRAM TYPE: Table script

  PURPOSE: Conversion purpose for AH Harries

  HISTORY
  ============================================================================================
  VERSION DATE          AUTHOR(S)       DESCRIPTION
  ------- -----------   --------------- ------------------------------------------------------
   1.0     06/20/2018    P.Vamshidhar    TMS#20180319-00243 - AH HARRIS Customer Interface
  *********************************************************************************************/
ALTER TABLE XXWC.XXWC_AR_CUST_SITE_IFACE_STG
 DROP PRIMARY KEY CASCADE;

DROP TABLE XXWC.XXWC_AR_CUST_SITE_IFACE_STG CASCADE CONSTRAINTS;

CREATE TABLE XXWC.XXWC_AR_CUST_SITE_IFACE_STG
(
  CUST_SITE_IFACE_STG_ID          NUMBER        NOT NULL,
  ORIG_SYSTEM_REFERENCE           VARCHAR2(240 BYTE),
  ORIG_SYSTEM                     VARCHAR2(30 BYTE),
  ATTRIBUTE1                      VARCHAR2(150 BYTE),
  ATTRIBUTE2                      VARCHAR2(150 BYTE),
  ATTRIBUTE3                      VARCHAR2(150 BYTE),
  ATTRIBUTE4                      VARCHAR2(150 BYTE),
  ATTRIBUTE5                      VARCHAR2(150 BYTE),
  ATTRIBUTE6                      VARCHAR2(150 BYTE),
  ATTRIBUTE7                      VARCHAR2(150 BYTE),
  ATTRIBUTE8                      VARCHAR2(150 BYTE),
  ATTRIBUTE9                      VARCHAR2(150 BYTE),
  ATTRIBUTE10                     VARCHAR2(150 BYTE),
  ATTRIBUTE11                     VARCHAR2(150 BYTE),
  ATTRIBUTE12                     VARCHAR2(150 BYTE),
  ATTRIBUTE13                     VARCHAR2(150 BYTE),
  ATTRIBUTE14                     VARCHAR2(150 BYTE),
  ATTRIBUTE15                     VARCHAR2(150 BYTE),
  ATTRIBUTE16                     VARCHAR2(150 BYTE),
  ATTRIBUTE17                     VARCHAR2(150 BYTE),
  ATTRIBUTE18                     VARCHAR2(150 BYTE),
  ATTRIBUTE19                     VARCHAR2(150 BYTE),
  ATTRIBUTE20                     VARCHAR2(150 BYTE),
  COUNTRY                         VARCHAR2(60 BYTE),
  ADDRESS1                        VARCHAR2(240 BYTE),
  ADDRESS2                        VARCHAR2(240 BYTE),
  ADDRESS3                        VARCHAR2(240 BYTE),
  ADDRESS4                        VARCHAR2(240 BYTE),
  CITY                            VARCHAR2(60 BYTE),
  POSTAL_CODE                     VARCHAR2(60 BYTE),
  STATE                           VARCHAR2(60 BYTE),
  COUNTY                          VARCHAR2(60 BYTE),
  LEGACY_CUSTOMER_NUMBER          VARCHAR2(30 BYTE),
  LEGACY_PARTY_SITE_NUMBER        VARCHAR2(30 BYTE),
  LEGACY_PARTY_SITE_NAME          VARCHAR2(240 BYTE),
  PROVINCE                        VARCHAR2(60 BYTE),
  LOCATION_ID                     NUMBER,
  TIMEZONE_ID                     NUMBER,
  IDENTIFYING_ADDRESS_FLAG        VARCHAR2(1 BYTE),
  PARTY_SITE_NAME                 VARCHAR2(240 BYTE),
  SITE_USE_TYPE                   VARCHAR2(30 BYTE),
  PRIMARY_PER_TYPE                VARCHAR2(1 BYTE),
  TERRITORY                       VARCHAR2(30 BYTE),
  BILL_TO_LOCATION                VARCHAR2(30 BYTE),
  PRIMARY_BILL_TO_FLAG            VARCHAR2(30 BYTE),
  GSA_INDICATOR                   VARCHAR2(10 BYTE),
  PRIMARY_SALESREP_NUMBER         VARCHAR2(30 BYTE),
  PRIMARY_SHIP_TO_FLAG            VARCHAR2(1 BYTE),
  FOB_POINT                       VARCHAR2(30 BYTE),
  FREIGHT_TERM                    VARCHAR2(30 BYTE),
  ORDER_TYPE                      VARCHAR2(30 BYTE),
  PRICE_LIST_NAME                 VARCHAR2(60 BYTE),
  SHIP_VIA                        VARCHAR2(30 BYTE),
  SHIP_SETS_INCLUDE_LINES_FLAG    VARCHAR2(1 BYTE),
  ARRIVALSETS_INCLUDE_LINES_FLAG  VARCHAR2(1 BYTE),
  INVOICE_QUANTITY_RULE           VARCHAR2(30 BYTE),
  OVER_SHIPMENT_TOLERANCE         NUMBER,
  UNDER_SHIPMENT_TOLERANCE        NUMBER,
  ITEM_CROSS_REF_PREF             VARCHAR2(30 BYTE),
  DATE_TYPE_PREFERENCE            VARCHAR2(30 BYTE),
  OVER_RETURN_TOLERANCE           NUMBER,
  UNDER_RETURN_TOLERANCE          NUMBER,
  SCHED_DATE_PUSH_FLAG            VARCHAR2(1 BYTE),
  WAREHOUSE_CODE                  VARCHAR2(30 BYTE),
  DATES_NEGATIVE_TOLERANCE        NUMBER,
  DATES_POSITIVE_TOLERANCE        NUMBER,
  FINCHRG_RECEIVABLES_TRX_NAME    VARCHAR2(50 BYTE),
  LEGACY_PARTY_NUMBER             VARCHAR2(30 BYTE),
  PARTY_SITE_ID                   NUMBER,
  PARTY_SITE_NUMBER               VARCHAR2(30 BYTE),
  PARTY_SITE_USE_ID               NUMBER,
  CUST_ACCT_SITE_ID               NUMBER,
  CUST_SITE_USE_ID                NUMBER,
  LOCATION                        VARCHAR2(30 BYTE),
  OVERALL_CREDIT_LIMIT            NUMBER,
  TRX_CREDIT_LIMIT                NUMBER,
  MIN_DUNNING_AMOUNT              NUMBER,
  MIN_DUNNING_INVOICE_AMPOUNT     NUMBER,
  MAX_INTEREST_CHARGE             NUMBER,
  MIN_STATEMENT_AMOUNT            NUMBER,
  CLEARING_DAYS                   NUMBER,
  AUTO_REC_INCL_DISPUTED_FLAG     VARCHAR2(1 BYTE),
  AUTO_REC_MIN_RECEIPT_AMOUNT     NUMBER,
  CHARGE_ON_FINANCE_CHARGE_FLAG   VARCHAR2(1 BYTE),
  LOCK_BOX_MATCHING_OPTION        VARCHAR2(30 BYTE),
  AUTOCASH_HIERARCHY_NAME         VARCHAR2(30 BYTE),
  REVIEW_CYCLE                    VARCHAR2(30 BYTE),
  CREDIT_CLASSIFICATION           VARCHAR2(30 BYTE),
  AUTOPAY_FLAG                    VARCHAR2(1 BYTE),
  HDS_SITE_FLAG                   VARCHAR2(20 BYTE),
  JOB_NAME                        VARCHAR2(64 BYTE),
  JOB_NUMBER                      VARCHAR2(30 BYTE),
  CREDIT_HOLD                     VARCHAR2(1 BYTE),
  CREDIT_ANALYST_NAME             VARCHAR2(50 BYTE),
  LEGACY_SITE_TYPE                NUMBER,
  PROCESS_STATUS                  VARCHAR2(15 BYTE),
  VALIDATED_FLAG                  VARCHAR2(30 BYTE),
  PROCESS_ERROR                   VARCHAR2(4000 BYTE),
  CREATION_DATE                   DATE,
  CREATED_BY                      NUMBER,
  LAST_UPDATE_DATE                DATE,
  LAST_UPDATED_BY                 NUMBER,
  PROCESSING_DATE                 DATE,
  ARCHIVE_DATE                    DATE,
  REQUEST_ID                      NUMBER,
  ORG_ID                          NUMBER        DEFAULT NVL(TO_NUMBER(DECODE(SUBSTRB(USERENV('CLIENT_INFO'),1,1), ' ', NULL,SUBSTRB(USERENV('CLIENT_INFO'),1,10))),162),
  TAXING_STATE                    VARCHAR2(10 BYTE),
  SALESREP_ID                     VARCHAR2(10 BYTE),
  INSIDE_SALESREP_ID              VARCHAR2(30 BYTE),
  FSD                             VARCHAR2(30 BYTE)
)
TABLESPACE XXWC_DATA
RESULT_CACHE (MODE DEFAULT)
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MAXSIZE          UNLIMITED
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
            FLASH_CACHE      DEFAULT
            CELL_FLASH_CACHE DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX XXWC.XXWC_AR_CUST_SITE_IFACE_STG_PK ON XXWC.XXWC_AR_CUST_SITE_IFACE_STG
(CUST_SITE_IFACE_STG_ID)
LOGGING
TABLESPACE XXWC_DATA
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MAXSIZE          UNLIMITED
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
            FLASH_CACHE      DEFAULT
            CELL_FLASH_CACHE DEFAULT
           )
NOPARALLEL;

ALTER INDEX XXWC.XXWC_AR_CUST_SITE_IFACE_STG_PK
  MONITORING USAGE;


CREATE OR REPLACE TRIGGER XXWC.XXWC_AR_CUST_SITE_IFACE_STG_T1
   BEFORE INSERT
   ON XXWC.XXWC_AR_CUST_SITE_IFACE_STG
   REFERENCING NEW AS New OLD AS Old
   FOR EACH ROW
DECLARE
   L_CUST_SITE_IFACE_STG_ID   NUMBER;
/******************************************************************************
   NAME:       XXWC_AR_CUST_SITE_IFACE_STG_T1
   PURPOSE:

   REVISIONS:
   Ver        Date        Author           Description
   ---------  ----------  ---------------  ------------------------------------
   1.0        3/29/2012             1. Created this trigger.

   NOTES:

   Automatically available Auto Replace Keywords:
      Object Name:     XXWC_AR_CUST_SITE_IFACE_STG_T1
      Sysdate:         3/29/2012
      Date and Time:   3/29/2012, 3:57:46 PM, and 3/29/2012 3:57:46 PM
      Username:         (set in TOAD Options, Proc Templates)
      Table Name:      XXWC_AR_CUST_SITE_IFACE_STG (set in the "New PL/SQL Object" dialog)
      Trigger Options:  (set in the "New PL/SQL Object" dialog)
******************************************************************************/
BEGIN
   L_CUST_SITE_IFACE_STG_ID := 0;

   SELECT XXWC_AR_CUST_SITE_IFACE_S.NEXTVAL
     INTO L_CUST_SITE_IFACE_STG_ID
     FROM DUAL;

   :NEW.CUST_SITE_IFACE_STG_ID := L_CUST_SITE_IFACE_STG_ID;

   :NEW.creation_date := SYSDATE;
   :NEW.created_by := 1;
EXCEPTION
   WHEN OTHERS
   THEN
      -- Consider logging the error and then re-raise
      RAISE;
END XXWC_AR_CUST_SITE_IFACE_STG_T1;
/


CREATE OR REPLACE SYNONYM APPS.XXWC_AR_CUST_SITE_IFACE_STG FOR XXWC.XXWC_AR_CUST_SITE_IFACE_STG;


CREATE OR REPLACE PUBLIC SYNONYM XXWC_AR_CUST_SITE_IFACE_STG FOR XXWC.XXWC_AR_CUST_SITE_IFACE_STG;


ALTER TABLE XXWC.XXWC_AR_CUST_SITE_IFACE_STG ADD (
  CONSTRAINT XXWC_AR_CUST_SITE_IFACE_STG_PK
  PRIMARY KEY
  (CUST_SITE_IFACE_STG_ID)
  USING INDEX XXWC.XXWC_AR_CUST_SITE_IFACE_STG_PK
  ENABLE VALIDATE);

GRANT ALTER, DELETE, INDEX, INSERT, REFERENCES, SELECT, UPDATE, ON COMMIT REFRESH, QUERY REWRITE, DEBUG, FLASHBACK ON XXWC.XXWC_AR_CUST_SITE_IFACE_STG TO APPS;

GRANT DELETE, INSERT, SELECT, UPDATE ON XXWC.XXWC_AR_CUST_SITE_IFACE_STG TO EA_APEX;

GRANT DELETE, INSERT, SELECT, UPDATE ON XXWC.XXWC_AR_CUST_SITE_IFACE_STG TO INTERFACE_APEXWC;

GRANT SELECT ON XXWC.XXWC_AR_CUST_SITE_IFACE_STG TO INTERFACE_PRISM;

GRANT DELETE, INSERT, SELECT, UPDATE ON XXWC.XXWC_AR_CUST_SITE_IFACE_STG TO XXWC_DEV_ADMIN_ROLE;

GRANT DELETE, INSERT, SELECT, UPDATE ON XXWC.XXWC_AR_CUST_SITE_IFACE_STG TO XXWC_EDIT_IFACE_ROLE;

GRANT INSERT ON XXWC.XXWC_AR_CUST_SITE_IFACE_STG TO XXWC_PRISM_INSERT_ROLE;

GRANT SELECT ON XXWC.XXWC_AR_CUST_SITE_IFACE_STG TO XXWC_PRISM_SELECT_ROLE;
/