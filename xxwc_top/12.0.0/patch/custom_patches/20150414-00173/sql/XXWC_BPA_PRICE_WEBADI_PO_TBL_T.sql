   /***************************************************************************************************************************
   -- File Name: XXWC_BPA_PRICE_WEBADI_PO_TBL_T.sql
   --
   -- PURPOSE: Table to store BPA price zone data for BPA price zone webadi.
   -- HISTORY
   -- =========================================================================================================================
   -- =========================================================================================================================
   -- VERSION DATE          AUTHOR(S)       DESCRIPTION
   -- ------- -----------   --------------- -----------------------------------------------------------------------------------
   -- 1.0     01-Apr-2015   P.Vamshidhar    Initial Version.
   --                                       TMS#20150414-00173-2015 PLM - Implement Cost Management tool enhancements
   --                                       Table will be used in BPA Price zone webadi.
   ***************************************************************************************************************************/

CREATE TABLE XXWC.XXWC_BPA_PRICE_WEBADI_PO_TBL
(
  PO_HEADER_ID  NUMBER,
  USERNAME      VARCHAR2(30 BYTE),
  CREATIONDATE  DATE
);


GRANT ALL ON XXWC.XXWC_BPA_PRICE_WEBADI_PO_TBL TO APPS;

CREATE SYNONYM APPS.XXWC_BPA_PRICE_WEBADI_PO_TBL FOR XXWC.XXWC_BPA_PRICE_WEBADI_PO_TBL;
/
