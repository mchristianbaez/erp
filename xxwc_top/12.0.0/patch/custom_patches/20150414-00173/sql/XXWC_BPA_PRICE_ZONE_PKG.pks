CREATE OR REPLACE package APPS.xxwc_bpa_price_zone_pkg
AS
   /*************************************************************************************************
   *   $Header xxwc_bpa_price_zone_pkg $                                                            *
   *   Module Name: xxwc_bpa_price_zone_pkg                                                         *
   *                                                                                                *
   *   PURPOSE:   This package is used by the XXWC BPA Price Zone Extension                         *
   *                                                                                                *
   *   REVISIONS:                                                                                   *
   *   Ver        Date        Author                     Description                                *
   *   ---------  ----------  ---------------         -------------------------                     *
   *   1.0        02/08/2014  Lee Spitzer                Initial Version 0130917-00676              *
   *                                                         Vendor Cost Management Improvements    *
   * ************************************************************************************************/

   /*************************************************************************************************   
   *   Procedure load_bpa_price_zone_stg                                                            *
   *   Purpose : Procedure loads XXWC BPA Price Zone form data into XXWC BPA PZ staging table       *
   *                                                                                                *
   *  PROCEDURE load_bpa_price_zone_stg    (i_po_number   IN VARCHAR2,                              *
   *                               i_vendor      IN VARCHAR2,                                       *
   *                               i_vendor_num  IN VARCHAR2,                                       *
   *                               i_vendor_site_code IN VARCHAR2,                                  * 
   *                               i_item_number IN VARCHAR2,                                       *
   *                               -- i_item_description IN VARCHAR2,                               *
   *                               --i_uom_code    IN VARCHAR2,                                     *
   *                               --i_upc_code    IN VARCHAR2,                                     *
   *                               --i_mfg_part_number IN VARCHAR2,                                 *
   *                               i_expiration_date IN DATE,                                       *
   *                               i_implement_date  IN DATE,                                       *
   *                               l_national_price  IN NUMBER,                                     *
   *                               l_national_quantity IN NUMBER,                                   *
   *                               l_national_break  IN NUMBER,                                     *
   *                               i_promo_price IN NUMBER,                                         *
   *                               i_promo_start_date IN DATE,                                      *
   *                               i_promo_end_date IN DATE,                                        *
   *                               i_price_zone1 IN NUMBER,                                         *
   *                               i_quantity_zone1 IN NUMBER,                                      *
   *                               i_quantity_price1 IN NUMBER,                                     *
   *                               i_price_zone2 IN NUMBER,                                         *
   *                               i_quantity_zone2 IN NUMBER,                                      *
   *                               i_quantity_price2 IN NUMBER,                                     *
   *                               i_price_zone3 IN NUMBER,                                         *
   *                               i_quantity_zone3 IN NUMBER,                                      *
   *                               i_quantity_price3 IN NUMBER,                                     *
   *                               i_price_zone4 IN NUMBER,                                         *
   *                               i_quantity_zone4 IN NUMBER,                                      *
   *                               i_quantity_price4 IN NUMBER,                                     *
   *                               i_price_zone5 IN NUMBER,                                         *
   *                               i_quantity_zone5 IN NUMBER,                                      *
   *                               i_quantity_price5 IN NUMBER,                                     *
   *                               i_price_zone6 IN NUMBER,                                         *
   *                               i_quantity_zone6 IN NUMBER,                                      *
   *                               i_quantity_price6 IN NUMBER,                                     *
   *                               i_price_zone7 IN NUMBER,                                         *
   *                               i_quantity_zone7 IN NUMBER,                                      *
   *                               i_quantity_price7 IN NUMBER,                                     *
   *                               i_price_zone8 IN NUMBER,                                         *
   *                               i_quantity_zone8 IN NUMBER,                                      *
   *                               i_quantity_price8 IN NUMBER,                                     *
   *                               i_price_zone9 IN NUMBER,                                         *
   *                               i_quantity_zone9 IN NUMBER,                                      *
   *                               i_quantity_price9 IN NUMBER,                                     *
   *                               i_price_zone10 IN NUMBER,                                        *
   *                               i_quantity_zone10 IN NUMBER,                                     *
   *                               i_quantity_price10 IN NUMBER,                                    *
   *                               i_price_zone11 IN NUMBER,                                        *
   *                               i_quantity_zone11 IN NUMBER,                                     *
   *                               i_quantity_price11 IN NUMBER,                                    *
   *                               i_price_zone12 IN NUMBER,                                        *
   *                               i_quantity_zone12 IN NUMBER,                                     *
   *                               i_quantity_price12 IN NUMBER,                                    *
   *                               i_price_zone13 IN NUMBER,                                        *
   *                               i_quantity_zone13 IN NUMBER,                                     *
   *                               i_quantity_price13 IN NUMBER,                                    *
   *                               i_price_zone14 IN NUMBER,                                        *
   *                               i_quantity_zone14 IN NUMBER,                                     *
   *                               i_quantity_price14 IN NUMBER,                                    *
   *                               i_price_zone15 IN NUMBER,                                        *
   *                               i_quantity_zone15 IN NUMBER,                                     *
   *                               i_quantity_price15 IN NUMBER,                                    *
   *                               i_price_zone16 IN NUMBER,                                        *
   *                               i_quantity_zone16 IN NUMBER,                                     *
   *                               i_quantity_price16 IN NUMBER,                                    *
   *                               i_price_zone17 IN NUMBER,                                        *
   *                               i_quantity_zone17 IN NUMBER,                                     *
   *                               i_quantity_price17 IN NUMBER,                                    *
   *                               i_price_zone18 IN NUMBER,                                        *
   *                               i_quantity_zone18 IN NUMBER,                                     *
   *                               i_quantity_price18 IN NUMBER,                                    *
   *                               i_price_zone19 IN NUMBER,                                        *
   *                               i_quantity_zone19 IN NUMBER,                                     *
   *                               i_quantity_price19 IN NUMBER,                                    *
   *                               i_price_zone20 IN NUMBER,                                        *
   *                               i_quantity_zone20 IN NUMBER,                                     *
   *                               i_quantity_price20 IN NUMBER,                                    *
   *                               i_price_zone21 IN NUMBER,                                        *
   *                               i_quantity_zone21 IN NUMBER,                                     *
   *                               i_quantity_price21 IN NUMBER,                                    *
   *                               i_price_zone22 IN NUMBER,                                        *
   *                               i_quantity_zone22 IN NUMBER,                                     *
   *                               i_quantity_price22 IN NUMBER,                                    *
   *                               i_price_zone23 IN NUMBER,                                        *
   *                               i_quantity_zone23 IN NUMBER,                                     *
   *                               i_quantity_price23 IN NUMBER,                                    *
   *                               i_price_zone24 IN NUMBER,                                        *
   *                               i_quantity_zone24 IN NUMBER,                                     *
   *                               i_quantity_price24 IN NUMBER,                                    *
   *                               i_price_zone25 IN NUMBER,                                        *
   *                               i_quantity_zone25 IN NUMBER,                                     *
   *                               i_quantity_price25 IN NUMBER,                                    *
   *                               i_price_zone26 IN NUMBER,                                        *
   *                               i_quantity_zone26 IN NUMBER,                                     *
   *                               i_quantity_price26 IN NUMBER,                                    *
   *                               i_price_zone27 IN NUMBER,                                        *
   *                               i_quantity_zone27 IN NUMBER,                                     *
   *                               i_quantity_price27 IN NUMBER,                                    *
   *                               i_price_zone28 IN NUMBER,                                        *
   *                               i_quantity_zone28 IN NUMBER,                                     *
   *                               i_quantity_price28 IN NUMBER,                                    *
   *                               i_price_zone29 IN NUMBER,                                        *
   *                               i_quantity_zone29 IN NUMBER,                                     *
   *                               i_quantity_price29 IN NUMBER,                                    *
   *                               i_price_zone30 IN NUMBER,                                        *
   *                               i_quantity_zone30 IN NUMBER,                                     *
   *                               i_quantity_price30 IN NUMBER,                                    *
   *                               i_org_zone_1 IN VARCHAR2,                                        *
   *                               i_org_zone_2 IN VARCHAR2,                                        *
   *                               i_org_zone_3 IN VARCHAR2,                                        *
   *                               i_org_zone_4 IN VARCHAR2,                                        *
   *                               i_org_zone_5 IN VARCHAR2,                                        *
   *                               i_org_zone_6 IN VARCHAR2,                                        *
   *                               i_org_zone_7 IN VARCHAR2,                                        *
   *                               i_org_zone_8 IN VARCHAR2,                                        *
   *                               i_org_zone_9 IN VARCHAR2,                                        *
   *                               i_org_zone_10 IN VARCHAR2,                                       *
   *                               i_org_zone_11 IN VARCHAR2,                                       *
   *                               i_org_zone_12 IN VARCHAR2,                                       *
   *                               i_org_zone_13 IN VARCHAR2,                                       *
   *                               i_org_zone_14 IN VARCHAR2,                                       *
   *                               i_org_zone_15 IN VARCHAR2,                                       *
   *                               i_org_zone_16 IN VARCHAR2,                                       *
   *                               i_org_zone_17 IN VARCHAR2,                                       *
   *                               i_org_zone_18 IN VARCHAR2,                                       *
   *                               i_org_zone_19 IN VARCHAR2,                                       *
   *                               i_org_zone_20 IN VARCHAR2,                                       *
   *                               i_org_zone_21 IN VARCHAR2,                                       *
   *                               i_org_zone_22 IN VARCHAR2,                                       *
   *                               i_org_zone_23 IN VARCHAR2,                                       *
   *                               i_org_zone_24 IN VARCHAR2,                                       *
   *                               i_org_zone_25 IN VARCHAR2,                                       *
   *                               i_org_zone_26 IN VARCHAR2,                                       *
   *                               i_org_zone_27 IN VARCHAR2,                                       *
   *                               i_org_zone_28 IN VARCHAR2,                                       *
   *                               i_org_zone_29 IN VARCHAR2,                                       *
   *                               i_org_zone_30 IN VARCHAR2                                        *
   *                               );                                                               *
   *                                                                                                *
   *                                                                                                *
   *                                                                                                *
   *                                                                                                *
   *   REVISIONS:                                                                                   *
   *   Ver        Date        Author                     Description                                *
   *   ---------  ----------  ---------------         -------------------------                     *
   *   1.0        02/08/2014   Lee Spitzer              Initial Versions TMS 20130917-00676         *
   *                                                         Vendor Cost Management Improvements    *
   *                                                                                                *
   /************************************************************************************************/
   
   
  
   PROCEDURE load_bpa_price_zone_stg    (i_po_number   IN VARCHAR2,
                                  i_vendor      IN VARCHAR2,
                                  i_vendor_num  IN VARCHAR2,
                                  i_vendor_site_code IN VARCHAR2,
                                  i_item_number IN VARCHAR2,
                                  -- i_item_description IN VARCHAR2,
                                  --i_uom_code    IN VARCHAR2,
                                  --i_upc_code    IN VARCHAR2,
                                  --i_mfg_part_number IN VARCHAR2,
                                  i_expiration_date IN DATE,
                                  i_implement_date  IN DATE,
                                  l_national_price  IN NUMBER,
                                  l_national_quantity IN NUMBER,
                                  l_national_break  IN NUMBER,
                                  i_promo_price IN NUMBER,
                                  i_promo_start_date IN DATE,
                                  i_promo_end_date IN DATE,
                                  i_price_zone1 IN NUMBER,
                                  i_quantity_zone1 IN NUMBER,
                                  i_quantity_price1 IN NUMBER,
                                  i_price_zone2 IN NUMBER,
                                  i_quantity_zone2 IN NUMBER,
                                  i_quantity_price2 IN NUMBER,
                                  i_price_zone3 IN NUMBER,
                                  i_quantity_zone3 IN NUMBER,
                                  i_quantity_price3 IN NUMBER,
                                  i_price_zone4 IN NUMBER,
                                  i_quantity_zone4 IN NUMBER,
                                  i_quantity_price4 IN NUMBER,
                                  i_price_zone5 IN NUMBER,
                                  i_quantity_zone5 IN NUMBER,
                                  i_quantity_price5 IN NUMBER,
                                  i_price_zone6 IN NUMBER,
                                  i_quantity_zone6 IN NUMBER,
                                  i_quantity_price6 IN NUMBER,
                                  i_price_zone7 IN NUMBER,
                                  i_quantity_zone7 IN NUMBER,
                                  i_quantity_price7 IN NUMBER,
                                  i_price_zone8 IN NUMBER,
                                  i_quantity_zone8 IN NUMBER,
                                  i_quantity_price8 IN NUMBER,
                                  i_price_zone9 IN NUMBER,
                                  i_quantity_zone9 IN NUMBER,
                                  i_quantity_price9 IN NUMBER,
                                  i_price_zone10 IN NUMBER,
                                  i_quantity_zone10 IN NUMBER,
                                  i_quantity_price10 IN NUMBER,
                                  i_price_zone11 IN NUMBER,
                                  i_quantity_zone11 IN NUMBER,
                                  i_quantity_price11 IN NUMBER,
                                  i_price_zone12 IN NUMBER,
                                  i_quantity_zone12 IN NUMBER,
                                  i_quantity_price12 IN NUMBER,
                                  i_price_zone13 IN NUMBER,
                                  i_quantity_zone13 IN NUMBER,
                                  i_quantity_price13 IN NUMBER,
                                  i_price_zone14 IN NUMBER,
                                  i_quantity_zone14 IN NUMBER,
                                  i_quantity_price14 IN NUMBER,
                                  i_price_zone15 IN NUMBER,
                                  i_quantity_zone15 IN NUMBER,
                                  i_quantity_price15 IN NUMBER,
                                  i_price_zone16 IN NUMBER,
                                  i_quantity_zone16 IN NUMBER,
                                  i_quantity_price16 IN NUMBER,
                                  i_price_zone17 IN NUMBER,
                                  i_quantity_zone17 IN NUMBER,
                                  i_quantity_price17 IN NUMBER,
                                  i_price_zone18 IN NUMBER,
                                  i_quantity_zone18 IN NUMBER,
                                  i_quantity_price18 IN NUMBER,
                                  i_price_zone19 IN NUMBER,
                                  i_quantity_zone19 IN NUMBER,
                                  i_quantity_price19 IN NUMBER,
                                  i_price_zone20 IN NUMBER,
                                  i_quantity_zone20 IN NUMBER,
                                  i_quantity_price20 IN NUMBER,
                                  i_price_zone21 IN NUMBER,
                                  i_quantity_zone21 IN NUMBER,
                                  i_quantity_price21 IN NUMBER,
                                  i_price_zone22 IN NUMBER,
                                  i_quantity_zone22 IN NUMBER,
                                  i_quantity_price22 IN NUMBER,
                                  i_price_zone23 IN NUMBER,
                                  i_quantity_zone23 IN NUMBER,
                                  i_quantity_price23 IN NUMBER,
                                  i_price_zone24 IN NUMBER,
                                  i_quantity_zone24 IN NUMBER,
                                  i_quantity_price24 IN NUMBER,
                                  i_price_zone25 IN NUMBER,
                                  i_quantity_zone25 IN NUMBER,
                                  i_quantity_price25 IN NUMBER,
                                  i_price_zone26 IN NUMBER,
                                  i_quantity_zone26 IN NUMBER,
                                  i_quantity_price26 IN NUMBER,
                                  i_price_zone27 IN NUMBER,
                                  i_quantity_zone27 IN NUMBER,
                                  i_quantity_price27 IN NUMBER,
                                  i_price_zone28 IN NUMBER,
                                  i_quantity_zone28 IN NUMBER,
                                  i_quantity_price28 IN NUMBER,
                                  i_price_zone29 IN NUMBER,
                                  i_quantity_zone29 IN NUMBER,
                                  i_quantity_price29 IN NUMBER,
                                  i_price_zone30 IN NUMBER,
                                  i_quantity_zone30 IN NUMBER,
                                  i_quantity_price30 IN NUMBER,
                                  i_org_zone_1 IN VARCHAR2,
                                  i_org_zone_2 IN VARCHAR2,
                                  i_org_zone_3 IN VARCHAR2,
                                  i_org_zone_4 IN VARCHAR2,
                                  i_org_zone_5 IN VARCHAR2,
                                  i_org_zone_6 IN VARCHAR2,
                                  i_org_zone_7 IN VARCHAR2,
                                  i_org_zone_8 IN VARCHAR2,
                                  i_org_zone_9 IN VARCHAR2,
                                  i_org_zone_10 IN VARCHAR2,
                                  i_org_zone_11 IN VARCHAR2,
                                  i_org_zone_12 IN VARCHAR2,
                                  i_org_zone_13 IN VARCHAR2,
                                  i_org_zone_14 IN VARCHAR2,
                                  i_org_zone_15 IN VARCHAR2,
                                  i_org_zone_16 IN VARCHAR2,
                                  i_org_zone_17 IN VARCHAR2,
                                  i_org_zone_18 IN VARCHAR2,
                                  i_org_zone_19 IN VARCHAR2,
                                  i_org_zone_20 IN VARCHAR2,
                                  i_org_zone_21 IN VARCHAR2,
                                  i_org_zone_22 IN VARCHAR2,
                                  i_org_zone_23 IN VARCHAR2,
                                  i_org_zone_24 IN VARCHAR2,
                                  i_org_zone_25 IN VARCHAR2,
                                  i_org_zone_26 IN VARCHAR2,
                                  i_org_zone_27 IN VARCHAR2,
                                  i_org_zone_28 IN VARCHAR2,
                                  i_org_zone_29 IN VARCHAR2,
                                  i_org_zone_30 IN VARCHAR2
                                  );   

   
   /*************************************************************************************************   
   *   Function get_price_zone_info                                                                 *
   *   Purpose : returns the price value from the price zone                                        *
   *                                                                                                *
   *                                                                                                *
   *   FUNCTION get_price_zone_info  (p_po_header_id IN NUMBER,                                     *
   *                                  p_inventory_item_id IN NUMBER,                                *
   *                                  p_price_zone  IN NUMBER,                                      *
   *                                  p_attribute   IN VARCHAR2)                                    *
   *                           RETURN NUMBER;                                                       *
   *                                                                                                *
   *   REVISIONS:                                                                                   *
   *   Ver        Date        Author                     Description                                *
   *   ---------  ----------  ---------------         -------------------------                     *
   *   1.0        02/08/2014   Lee Spitzer              Initial Versions TMS 20130917-00676         *
   *                                                         Vendor Cost Management Improvements    *
   *                                                                                                *
   /************************************************************************************************/
                                
   
  FUNCTION get_price_zone_info  (p_po_header_id IN NUMBER,
                                 p_inventory_item_id IN NUMBER,
                                 p_price_zone  IN NUMBER,
                                 p_attribute   IN VARCHAR2)
                      RETURN NUMBER;
                      
  
   /*************************************************************************************************   
   *   Function get_price_zone_break_info                                                           *
   *   Purpose : returns the price zone break value from the price zone                             *
   *                                                                                                *
   *                                                                                                *
   *   FUNCTION get_price_zone_info  (p_po_header_id IN NUMBER,                                     *
   *                                  p_inventory_item_id IN NUMBER,                                *
   *                                  p_price_zone  IN NUMBER,                                      *
   *                                  p_quantity    IN NUMBER,                                      *
   *                                  p_attribute   IN VARCHAR2)                                    *
   *                           RETURN NUMBER;                                                       *
   *                                                                                                *
   *   REVISIONS:                                                                                   *
   *   Ver        Date        Author                     Description                                *
   *   ---------  ----------  ---------------         -------------------------                     *
   *   1.0        02/08/2014   Lee Spitzer              Initial Versions TMS 20130917-00676         *
   *                                                         Vendor Cost Management Improvements    *
   *                                                                                                *
   /************************************************************************************************/
   
  FUNCTION get_price_zone_break_info  
                                (p_po_header_id IN NUMBER,
                                 p_inventory_item_id IN NUMBER,
                                 p_price_zone  IN NUMBER,
                                 p_quantity    IN NUMBER,
                                 p_attribute   IN VARCHAR2)
                     RETURN NUMBER;                       
  

   /*************************************************************************************************   
   *   Procedure load_vendor_price_zone                                                             *
   *   Purpose : Used to load vendor price zones from the Vendor Price Zone Web                     *
   *                                                                                                *
   *                                                                                                *
   *   PROCEDURE load_vendor_price_zone (p_vendor IN VARCHAR2,                                      *
   *                                 p_vendor_site IN VARCHAR2,                                     *
   *                                 p_price_zone IN NUMBER,                                        *
   *                                 p_organization_code IN VARCHAR2,                               *
   *                                 p_org_pricing_zone IN VARCHAR2,                                *
   *                                 p_org_type IN VARCHAR2,                                        *
   *                                 p_district IN VARCHAR2,                                        *
   *                                 p_region IN VARCHAR2,                                          *
   *                                 p_organization_name IN VARCHAR2);                              *
   *                                                                                                *
   *   REVISIONS:                                                                                   *
   *   Ver        Date        Author                     Description                                *
   *   ---------  ----------  ---------------         -------------------------                     *
   *   1.0        02/08/2014   Lee Spitzer              Initial Versions TMS 20130917-00676         *
   *                                                         Vendor Cost Management Improvements    *
   *                                                                                                *
   /************************************************************************************************/
   

                      
  PROCEDURE load_vendor_price_zone (p_vendor IN VARCHAR2,
                                    p_vendor_site IN VARCHAR2,
                                    p_price_zone IN NUMBER,
                                    p_organization_code IN VARCHAR2,
                                    p_org_pricing_zone IN VARCHAR2,
                                    p_org_type IN VARCHAR2,
                                    p_district IN VARCHAR2,
                                    p_region IN VARCHAR2,
                                    p_organization_name IN VARCHAR2);
  
  

    
   /*************************************************************************************************   
   *   Function get_price_zone_orgs                                                                 *
   *   Purpose : Used in the BPA Price Zone Web ADI to display the orgs in a price zone             *
   *                                                                                                *
   *                                                                                                *
   *    FUNCTION  get_price_zone_orgs    (p_vendor_id IN NUMBER,                                    *
   *                                 p_vendor_site_id IN NUMBER,                                    *
   *                                 p_price_zone IN NUMBER)                                        *
   *                     RETURN VARCHAR2;                                                           *
   *                                                                                                *
   *   REVISIONS:                                                                                   *
   *   Ver        Date        Author                     Description                                *
   *   ---------  ----------  ---------------         -------------------------                     *
   *   1.0        02/08/2014   Lee Spitzer              Initial Versions TMS 20130917-00676         *
   *                                                         Vendor Cost Management Improvements    *
   *                                                                                                *
   /************************************************************************************************/
   
  
  FUNCTION  get_price_zone_orgs    (p_vendor_id IN NUMBER,
                                    p_vendor_site_id IN NUMBER,
                                    p_price_zone IN NUMBER)
                        RETURN VARCHAR2;
                        
                        
   /*************************************************************************************************   
   *   Procedure process_bpa_lines                                                                  *
   *   Purpose : Used in the process_staging to process staging records on xxwc_bpa_pz_staging_tbl  *
   *         and load into the PO Interface Tables and process the interface records                *
   *                                                                                                *
   *                                                                                                *
   *              PROCEDURE process_bpa_lines                                                       *
   *                               (p_po_number IN VARCHAR2                                         *
   *                               ,p_process_flag in NUMBER);                                      *
   *                                                                                                *
   *   REVISIONS:                                                                                   *
   *   Ver        Date        Author                     Description                                *
   *   ---------  ----------  ---------------         -------------------------                     *
   *   1.0        02/08/2014   Lee Spitzer              Initial Versions TMS 20130917-00676         *
   *                                                         Vendor Cost Management Improvements    *
   *                                                                                                *
   /************************************************************************************************/
  
  
  PROCEDURE process_bpa_lines
                                  (p_po_number IN VARCHAR2
                                  ,p_process_flag in NUMBER);
                                  
 
   /*************************************************************************************************   
   *   Procedure process_price_zone_lines                                                           *
   *   Purpose : Used in the process_staging to process staging records on xxwc_bpa_pz_staging_tbl  *
   *         and load into the XXWC Price Zone Tables and XXWC Price Quantity Breaks Tables         *
   *                                                                                                *
   *                                                                                                *
   *              PROCEDURE process_price_zone_lines                                                *
   *                               (p_po_number IN VARCHAR2                                         *
   *                               ,p_process_flag in NUMBER);                                      *
   *                                                                                                *
   *   REVISIONS:                                                                                   *
   *   Ver        Date        Author                     Description                                *
   *   ---------  ----------  ---------------         -------------------------                     *
   *   1.0        02/08/2014   Lee Spitzer              Initial Versions TMS 20130917-00676         *
   *                                                         Vendor Cost Management Improvements    *
   *                                                                                                *
   *   1.1        02/20/2015   Lee Spitzer              TMS Ticket 20150225-00021 - Fix issue with  *
   *                                                         incorrect price zone reference for zone*
   *                                                         21                                     *
   *                                                                                                *
   /************************************************************************************************/
  
  
   PROCEDURE process_price_zone_lines
                                  (p_po_number IN VARCHAR2
                                  ,p_process_flag IN NUMBER);
  
 
 
   /*************************************************************************************************   
   *   Procedure process_promo_lines                                                                *
   *   Purpose : Used in the process_staging to process staging records on xxwc_bpa_pz_staging_tbl  *
   *         and load into the XXWC Promo Tables                                                    *
   *                                                                                                *
   *                                                                                                *
   *              PROCEDURE process_promo_lines                                                     *
   *                               (p_po_number IN VARCHAR2                                         *
   *                               ,p_process_flag in NUMBER);                                      *
   *                                                                                                *
   *   REVISIONS:                                                                                   *
   *   Ver        Date        Author                     Description                                *
   *   ---------  ----------  ---------------         -------------------------                     *
   *   1.0        02/08/2014   Lee Spitzer              Initial Versions TMS 20130917-00676         *
   *                                                         Vendor Cost Management Improvements    *
   *                                                                                                *
   /************************************************************************************************/

  
  PROCEDURE process_promo_lines
                                  (p_po_number IN VARCHAR2
                                  ,p_process_flag IN NUMBER);
                                  

   /*************************************************************************************************   
   *   Procedure process_staging                                                                    *
   *   Purpose : Main procudure to used in the concurrent program XXWC BPA Price Zone Import        *
   *                                                                                                *
   *                                                                                                *
   *      PROCEDURE process_staging (Errbuf     OUT NOCOPY VARCHAR2                                 *
   *                         ,Retcode    OUT NOCOPY NUMBER                                          *
   *                         ,p_po_number IN VARCHAR2                                               *
   *                         ,p_process_flag IN NUMBER);                                            *
   *                                                                                                *
   *   REVISIONS:                                                                                   *
   *   Ver        Date        Author                     Description                                *
   *   ---------  ----------  ---------------         -------------------------                     *
   *   1.0        02/08/2014   Lee Spitzer              Initial Versions TMS 20130917-00676         *
   *                                                         Vendor Cost Management Improvements    *
   *                                                                                                *
   /************************************************************************************************/
                                  
  
  PROCEDURE process_staging (Errbuf     OUT NOCOPY VARCHAR2
                            ,Retcode    OUT NOCOPY NUMBER
                            ,p_po_number IN VARCHAR2
                            ,p_process_flag IN NUMBER);
                            
  
   /*************************************************************************************************   
   *   Procedure update_expiration_date                                                             *
   *   Purpose : Mass Update Expiration Date on the XXWC Price Zone Staging Table                   *    
   *           used in the XXWC BPA Price Zone Form                                                 *
   *                                                                                                *
   *                                                                                                *
   *  PROCEDURE update_expiration_date (p_po_number in VARCHAR2                                     *
   *                                   ,p_expiration_date in DATE);                                 *
   *                                                                                                *
   *   REVISIONS:                                                                                   *
   *   Ver        Date        Author                     Description                                *
   *   ---------  ----------  ---------------         -------------------------                     *
   *   1.0        02/08/2014   Lee Spitzer              Initial Versions TMS 20130917-00676         *
   *                                                         Vendor Cost Management Improvements    *
   *                                                                                                *
   /************************************************************************************************/

  
  PROCEDURE update_expiration_date (p_po_number in VARCHAR2
                                   ,p_expiration_date in DATE);
  
  
   /*************************************************************************************************   
   *   Procedure update_implement_date                                                              *
   *   Purpose : Mass Update Implement Date on the XXWC Price Zone Staging Table                    *    
   *           used in the XXWC BPA Price Zone Form                                                 *
   *                                                                                                *
   *                                                                                                *
   *  PROCEDURE update_implement_date (p_po_number in VARCHAR2                                      *
   *                                   ,p_implement_date in DATE);                                  *
   *                                                                                                *
   *   REVISIONS:                                                                                   *
   *   Ver        Date        Author                     Description                                *
   *   ---------  ----------  ---------------         -------------------------                     *
   *   1.0        02/08/2014   Lee Spitzer              Initial Versions TMS 20130917-00676         *
   *                                                         Vendor Cost Management Improvements    *
   *                                                                                                *
   /************************************************************************************************/

  PROCEDURE update_implement_date  (p_po_number in VARCHAR2
                                   ,p_implement_date IN DATE);


                                     
   /*************************************************************************************************   
   *   Procedure get_pricing                                                                        *
   *   Purpose : Used in the po_custom_price_pub to return the best price in PO and Req Forms       *
   *           Also used in XXWC Items To Meet Vendor Minimums Form                                 *
   *                                                                                                *
   *                                                                                                *
   *   PROCEDURE get_pricing (p_po_header_id IN NUMBER                                              *
   *                         ,p_organization_id IN NUMBER                                           *
   *                         ,p_inventory_item_id IN NUMBER                                         *
   *                         ,p_po_line_id IN NUMBER                                                *
   *                         ,p_quantity IN NUMBER                                                  *
   *                         ,p_date IN DATE                                                        *
   *                         ,x_vendor_price_zone OUT NUMBER                                        *
   *                         ,x_national_price OUT NUMBER                                           *
   *                         ,x_national_quantity_price OUT NUMBER                                  *
   *                         ,x_promo_price  OUT NUMBER                                             *
   *                         ,x_price_zone_price OUT NUMBER                                         *
   *                         ,x_price_zone_quantity_price OUT NUMBER                                *
   *                         ,x_best_price OUT NUMBER);                                             *
   *                                                                                                *
   *   REVISIONS:                                                                                   *
   *   Ver        Date        Author                     Description                                *
   *   ---------  ----------  ---------------         -------------------------                     *
   *   1.0        02/08/2014   Lee Spitzer              Initial Versions TMS 20130917-00676         *
   *                                                         Vendor Cost Management Improvements    *
   *                                                                                                *
   /************************************************************************************************/

  PROCEDURE get_pricing (p_po_header_id IN NUMBER
                        ,p_organization_id IN NUMBER
                        ,p_inventory_item_id IN NUMBER
                        ,p_po_line_id IN NUMBER
                        ,p_quantity IN NUMBER
                        ,p_date IN DATE
                        ,x_vendor_price_zone OUT NUMBER
                        ,x_national_price OUT NUMBER
                        ,x_national_quantity_price OUT NUMBER
                        ,x_promo_price  OUT NUMBER
                        ,x_price_zone_price OUT NUMBER
                        ,x_price_zone_quantity_price OUT NUMBER
                        ,x_best_price OUT NUMBER);
                        
  
     
   /*************************************************************************************************   
   *   Function get_default_vendor_contact_id                                                       *
   *   Purpose : used to get the default contact id in the PO Form #16 Personalization              *            *
   *                                                                                                *
   *                                                                                                *
   *   FUNCTION get_default_vendor_contact_id (p_vendor_id IN NUMBER                                *
   *                                      ,p_vendor_site_id IN NUMBER                               *
   *                                      ,p_organization_id IN NUMBER)                             *
   *                                      RETURN NUMBER;                                            *
   *                                                                                                *
   *   REVISIONS:                                                                                   *
   *   Ver        Date        Author                     Description                                *
   *   ---------  ----------  ---------------         -------------------------                     *
   *   1.0        02/08/2014   Lee Spitzer              Initial Versions TMS 20130917-00676         *
   *                                                         Vendor Cost Management Improvements    *
   *                                                                                                *
   /************************************************************************************************/
    
  FUNCTION get_default_vendor_contact_id (p_vendor_id IN NUMBER
                                         ,p_vendor_site_id IN NUMBER
                                         ,p_organization_id IN NUMBER)
                                         RETURN NUMBER;
      
                        
 END xxwc_bpa_price_zone_pkg;
/
Show Errors;