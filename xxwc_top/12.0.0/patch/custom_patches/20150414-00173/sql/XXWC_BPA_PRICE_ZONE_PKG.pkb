CREATE OR REPLACE PACKAGE BODY APPS.xxwc_bpa_price_zone_pkg
AS
   /****************************************************************************************************************************************
   *   $Header xxwc_bpa_price_zone_pkg $                                                                                                   *
   *   Module Name: xxwc_bpa_price_zone_pkg                                                                                                *
   *                                                                                                                                       *
   *   PURPOSE:   This package is used by the XXWC BPA Price Zone Extension                                                                *
   *                                                                                                                                       *
   *   REVISIONS:                                                                                                                          *
   *   Ver        Date        Author                     Description                                                                       *
   *   ---------  ----------  ---------------         -------------------------                                                            *
   *   1.0        02/08/2014  Lee Spitzer                Initial Version 0130917-00676                                                     *
   *                                                         Vendor Cost Management Improvements                                           *
   *   1.1        11/21/2014  Manjula Chellappan         TMS #  20141002-00041  Multi-Org                                                  *
   *   1.2        01/12/2015  Lee Spitzer                TMS Ticket 20140909-00032                                                         *
   *   1.3        04/16/2015  P.vamshidhar               TMS #  20150414-00173  2015 PLM - Implement Cost Management tool enhancements     *
   *                                                     process_price_zone_lines procedure                                                *
   * ***************************************************************************************************************************************/

   G_PKG_NAME   CONSTANT VARCHAR2 (30) := 'xxwc_bpa_price_zone_pkg';

   g_log_head   CONSTANT VARCHAR2 (50) := 'po.plsql.' || G_PKG_NAME || '.';

   -- Debugging
   g_debug_stmt          BOOLEAN := PO_DEBUG.is_debug_stmt_on;
   g_debug_unexp         BOOLEAN := PO_DEBUG.is_debug_unexp_on;


   -- Error DEBUG
   g_exception           EXCEPTION;
   g_err_msg             VARCHAR2 (2000);
   g_err_callfrom        VARCHAR2 (175) := 'XXWC_BPA_PRICE_ZONE_PKG';
   g_err_callpoint       VARCHAR2 (175) := 'START';
   g_distro_list         VARCHAR2 (80)
                            := 'OracleDevelopmentGroup@hdsupply.com';
   g_module              VARCHAR2 (80);

   /*************************************************************************************************
   *   Procedure load_bpa_price_zone_stg                                                            *
   *   Purpose : Procedure loads XXWC BPA Price Zone form data into XXWC BPA PZ staging table       *
   *                                                                                                *
   *  PROCEDURE load_bpa_price_zone_stg    (i_po_number   IN VARCHAR2,                              *
   *                               i_vendor      IN VARCHAR2,                                       *
   *                               i_vendor_num  IN VARCHAR2,                                       *
   *                               i_vendor_site_code IN VARCHAR2,                                  *
   *                               i_item_number IN VARCHAR2,                                       *
   *                               -- i_item_description IN VARCHAR2,                               *
   *                               --i_uom_code    IN VARCHAR2,                                     *
   *                               --i_upc_code    IN VARCHAR2,                                     *
   *                               --i_mfg_part_number IN VARCHAR2,                                 *
   *                               i_expiration_date IN DATE,                                       *
   *                               i_implement_date  IN DATE,                                       *
   *                               l_national_price  IN NUMBER,                                     *
   *                               l_national_quantity IN NUMBER,                                   *
   *                               l_national_break  IN NUMBER,                                     *
   *                               i_promo_price IN NUMBER,                                         *
   *                               i_promo_start_date IN DATE,                                      *
   *                               i_promo_end_date IN DATE,                                        *
   *                               i_price_zone1 IN NUMBER,                                         *
   *                               i_quantity_zone1 IN NUMBER,                                      *
   *                               i_quantity_price1 IN NUMBER,                                     *
   *                               i_price_zone2 IN NUMBER,                                         *
   *                               i_quantity_zone2 IN NUMBER,                                      *
   *                               i_quantity_price2 IN NUMBER,                                     *
   *                               i_price_zone3 IN NUMBER,                                         *
   *                               i_quantity_zone3 IN NUMBER,                                      *
   *                               i_quantity_price3 IN NUMBER,                                     *
   *                               i_price_zone4 IN NUMBER,                                         *
   *                               i_quantity_zone4 IN NUMBER,                                      *
   *                               i_quantity_price4 IN NUMBER,                                     *
   *                               i_price_zone5 IN NUMBER,                                         *
   *                               i_quantity_zone5 IN NUMBER,                                      *
   *                               i_quantity_price5 IN NUMBER,                                     *
   *                               i_price_zone6 IN NUMBER,                                         *
   *                               i_quantity_zone6 IN NUMBER,                                      *
   *                               i_quantity_price6 IN NUMBER,                                     *
   *                               i_price_zone7 IN NUMBER,                                         *
   *                               i_quantity_zone7 IN NUMBER,                                      *
   *                               i_quantity_price7 IN NUMBER,                                     *
   *                               i_price_zone8 IN NUMBER,                                         *
   *                               i_quantity_zone8 IN NUMBER,                                      *
   *                               i_quantity_price8 IN NUMBER,                                     *
   *                               i_price_zone9 IN NUMBER,                                         *
   *                               i_quantity_zone9 IN NUMBER,                                      *
   *                               i_quantity_price9 IN NUMBER,                                     *
   *                               i_price_zone10 IN NUMBER,                                        *
   *                               i_quantity_zone10 IN NUMBER,                                     *
   *                               i_quantity_price10 IN NUMBER,                                    *
   *                               i_price_zone11 IN NUMBER,                                        *
   *                               i_quantity_zone11 IN NUMBER,                                     *
   *                               i_quantity_price11 IN NUMBER,                                    *
   *                               i_price_zone12 IN NUMBER,                                        *
   *                               i_quantity_zone12 IN NUMBER,                                     *
   *                               i_quantity_price12 IN NUMBER,                                    *
   *                               i_price_zone13 IN NUMBER,                                        *
   *                               i_quantity_zone13 IN NUMBER,                                     *
   *                               i_quantity_price13 IN NUMBER,                                    *
   *                               i_price_zone14 IN NUMBER,                                        *
   *                               i_quantity_zone14 IN NUMBER,                                     *
   *                               i_quantity_price14 IN NUMBER,                                    *
   *                               i_price_zone15 IN NUMBER,                                        *
   *                               i_quantity_zone15 IN NUMBER,                                     *
   *                               i_quantity_price15 IN NUMBER,                                    *
   *                               i_price_zone16 IN NUMBER,                                        *
   *                               i_quantity_zone16 IN NUMBER,                                     *
   *                               i_quantity_price16 IN NUMBER,                                    *
   *                               i_price_zone17 IN NUMBER,                                        *
   *                               i_quantity_zone17 IN NUMBER,                                     *
   *                               i_quantity_price17 IN NUMBER,                                    *
   *                               i_price_zone18 IN NUMBER,                                        *
   *                               i_quantity_zone18 IN NUMBER,                                     *
   *                               i_quantity_price18 IN NUMBER,                                    *
   *                               i_price_zone19 IN NUMBER,                                        *
   *                               i_quantity_zone19 IN NUMBER,                                     *
   *                               i_quantity_price19 IN NUMBER,                                    *
   *                               i_price_zone20 IN NUMBER,                                        *
   *                               i_quantity_zone20 IN NUMBER,                                     *
   *                               i_quantity_price20 IN NUMBER,                                    *
   *                               i_price_zone21 IN NUMBER,                                        *
   *                               i_quantity_zone21 IN NUMBER,                                     *
   *                               i_quantity_price21 IN NUMBER,                                    *
   *                               i_price_zone22 IN NUMBER,                                        *
   *                               i_quantity_zone22 IN NUMBER,                                     *
   *                               i_quantity_price22 IN NUMBER,                                    *
   *                               i_price_zone23 IN NUMBER,                                        *
   *                               i_quantity_zone23 IN NUMBER,                                     *
   *                               i_quantity_price23 IN NUMBER,                                    *
   *                               i_price_zone24 IN NUMBER,                                        *
   *                               i_quantity_zone24 IN NUMBER,                                     *
   *                               i_quantity_price24 IN NUMBER,                                    *
   *                               i_price_zone25 IN NUMBER,                                        *
   *                               i_quantity_zone25 IN NUMBER,                                     *
   *                               i_quantity_price25 IN NUMBER,                                    *
   *                               i_price_zone26 IN NUMBER,                                        *
   *                               i_quantity_zone26 IN NUMBER,                                     *
   *                               i_quantity_price26 IN NUMBER,                                    *
   *                               i_price_zone27 IN NUMBER,                                        *
   *                               i_quantity_zone27 IN NUMBER,                                     *
   *                               i_quantity_price27 IN NUMBER,                                    *
   *                               i_price_zone28 IN NUMBER,                                        *
   *                               i_quantity_zone28 IN NUMBER,                                     *
   *                               i_quantity_price28 IN NUMBER,                                    *
   *                               i_price_zone29 IN NUMBER,                                        *
   *                               i_quantity_zone29 IN NUMBER,                                     *
   *                               i_quantity_price29 IN NUMBER,                                    *
   *                               i_price_zone30 IN NUMBER,                                        *
   *                               i_quantity_zone30 IN NUMBER,                                     *
   *                               i_quantity_price30 IN NUMBER,                                    *
   *                               i_org_zone_1 IN VARCHAR2,                                        *
   *                               i_org_zone_2 IN VARCHAR2,                                        *
   *                               i_org_zone_3 IN VARCHAR2,                                        *
   *                               i_org_zone_4 IN VARCHAR2,                                        *
   *                               i_org_zone_5 IN VARCHAR2,                                        *
   *                               i_org_zone_6 IN VARCHAR2,                                        *
   *                               i_org_zone_7 IN VARCHAR2,                                        *
   *                               i_org_zone_8 IN VARCHAR2,                                        *
   *                               i_org_zone_9 IN VARCHAR2,                                        *
   *                               i_org_zone_10 IN VARCHAR2,                                       *
   *                               i_org_zone_11 IN VARCHAR2,                                       *
   *                               i_org_zone_12 IN VARCHAR2,                                       *
   *                               i_org_zone_13 IN VARCHAR2,                                       *
   *                               i_org_zone_14 IN VARCHAR2,                                       *
   *                               i_org_zone_15 IN VARCHAR2,                                       *
   *                               i_org_zone_16 IN VARCHAR2,                                       *
   *                               i_org_zone_17 IN VARCHAR2,                                       *
   *                               i_org_zone_18 IN VARCHAR2,                                       *
   *                               i_org_zone_19 IN VARCHAR2,                                       *
   *                               i_org_zone_20 IN VARCHAR2,                                       *
   *                               i_org_zone_21 IN VARCHAR2,                                       *
   *                               i_org_zone_22 IN VARCHAR2,                                       *
   *                               i_org_zone_23 IN VARCHAR2,                                       *
   *                               i_org_zone_24 IN VARCHAR2,                                       *
   *                               i_org_zone_25 IN VARCHAR2,                                       *
   *                               i_org_zone_26 IN VARCHAR2,                                       *
   *                               i_org_zone_27 IN VARCHAR2,                                       *
   *                               i_org_zone_28 IN VARCHAR2,                                       *
   *                               i_org_zone_29 IN VARCHAR2,                                       *
   *                               i_org_zone_30 IN VARCHAR2                                        *
   *                               );                                                               *
   *                                                                                                *
   *                                                                                                *
   *                                                                                                *
   *                                                                                                *
   *   REVISIONS:                                                                                   *
   *   Ver        Date        Author                     Description                                *
   *   ---------  ----------  ---------------         -------------------------                     *
   *   1.0        02/08/2014   Lee Spitzer              Initial Versions TMS 20130917-00676         *
   *                                                         Vendor Cost Management Improvements    *
   *                                                                                                *
   /************************************************************************************************/


   PROCEDURE load_bpa_price_zone_stg (i_po_number           IN VARCHAR2,
                                      i_vendor              IN VARCHAR2,
                                      i_vendor_num          IN VARCHAR2,
                                      i_vendor_site_code    IN VARCHAR2,
                                      i_item_number         IN VARCHAR2,
                                      -- i_item_description IN VARCHAR2,
                                      --i_uom_code    IN VARCHAR2,
                                      --i_upc_code    IN VARCHAR2,
                                      --i_mfg_part_number IN VARCHAR2,
                                      i_expiration_date     IN DATE,
                                      i_implement_date      IN DATE,
                                      l_national_price      IN NUMBER,
                                      l_national_quantity   IN NUMBER,
                                      l_national_break      IN NUMBER,
                                      i_promo_price         IN NUMBER,
                                      i_promo_start_date    IN DATE,
                                      i_promo_end_date      IN DATE,
                                      i_price_zone1         IN NUMBER,
                                      i_quantity_zone1      IN NUMBER,
                                      i_quantity_price1     IN NUMBER,
                                      i_price_zone2         IN NUMBER,
                                      i_quantity_zone2      IN NUMBER,
                                      i_quantity_price2     IN NUMBER,
                                      i_price_zone3         IN NUMBER,
                                      i_quantity_zone3      IN NUMBER,
                                      i_quantity_price3     IN NUMBER,
                                      i_price_zone4         IN NUMBER,
                                      i_quantity_zone4      IN NUMBER,
                                      i_quantity_price4     IN NUMBER,
                                      i_price_zone5         IN NUMBER,
                                      i_quantity_zone5      IN NUMBER,
                                      i_quantity_price5     IN NUMBER,
                                      i_price_zone6         IN NUMBER,
                                      i_quantity_zone6      IN NUMBER,
                                      i_quantity_price6     IN NUMBER,
                                      i_price_zone7         IN NUMBER,
                                      i_quantity_zone7      IN NUMBER,
                                      i_quantity_price7     IN NUMBER,
                                      i_price_zone8         IN NUMBER,
                                      i_quantity_zone8      IN NUMBER,
                                      i_quantity_price8     IN NUMBER,
                                      i_price_zone9         IN NUMBER,
                                      i_quantity_zone9      IN NUMBER,
                                      i_quantity_price9     IN NUMBER,
                                      i_price_zone10        IN NUMBER,
                                      i_quantity_zone10     IN NUMBER,
                                      i_quantity_price10    IN NUMBER,
                                      i_price_zone11        IN NUMBER,
                                      i_quantity_zone11     IN NUMBER,
                                      i_quantity_price11    IN NUMBER,
                                      i_price_zone12        IN NUMBER,
                                      i_quantity_zone12     IN NUMBER,
                                      i_quantity_price12    IN NUMBER,
                                      i_price_zone13        IN NUMBER,
                                      i_quantity_zone13     IN NUMBER,
                                      i_quantity_price13    IN NUMBER,
                                      i_price_zone14        IN NUMBER,
                                      i_quantity_zone14     IN NUMBER,
                                      i_quantity_price14    IN NUMBER,
                                      i_price_zone15        IN NUMBER,
                                      i_quantity_zone15     IN NUMBER,
                                      i_quantity_price15    IN NUMBER,
                                      i_price_zone16        IN NUMBER,
                                      i_quantity_zone16     IN NUMBER,
                                      i_quantity_price16    IN NUMBER,
                                      i_price_zone17        IN NUMBER,
                                      i_quantity_zone17     IN NUMBER,
                                      i_quantity_price17    IN NUMBER,
                                      i_price_zone18        IN NUMBER,
                                      i_quantity_zone18     IN NUMBER,
                                      i_quantity_price18    IN NUMBER,
                                      i_price_zone19        IN NUMBER,
                                      i_quantity_zone19     IN NUMBER,
                                      i_quantity_price19    IN NUMBER,
                                      i_price_zone20        IN NUMBER,
                                      i_quantity_zone20     IN NUMBER,
                                      i_quantity_price20    IN NUMBER,
                                      i_price_zone21        IN NUMBER,
                                      i_quantity_zone21     IN NUMBER,
                                      i_quantity_price21    IN NUMBER,
                                      i_price_zone22        IN NUMBER,
                                      i_quantity_zone22     IN NUMBER,
                                      i_quantity_price22    IN NUMBER,
                                      i_price_zone23        IN NUMBER,
                                      i_quantity_zone23     IN NUMBER,
                                      i_quantity_price23    IN NUMBER,
                                      i_price_zone24        IN NUMBER,
                                      i_quantity_zone24     IN NUMBER,
                                      i_quantity_price24    IN NUMBER,
                                      i_price_zone25        IN NUMBER,
                                      i_quantity_zone25     IN NUMBER,
                                      i_quantity_price25    IN NUMBER,
                                      i_price_zone26        IN NUMBER,
                                      i_quantity_zone26     IN NUMBER,
                                      i_quantity_price26    IN NUMBER,
                                      i_price_zone27        IN NUMBER,
                                      i_quantity_zone27     IN NUMBER,
                                      i_quantity_price27    IN NUMBER,
                                      i_price_zone28        IN NUMBER,
                                      i_quantity_zone28     IN NUMBER,
                                      i_quantity_price28    IN NUMBER,
                                      i_price_zone29        IN NUMBER,
                                      i_quantity_zone29     IN NUMBER,
                                      i_quantity_price29    IN NUMBER,
                                      i_price_zone30        IN NUMBER,
                                      i_quantity_zone30     IN NUMBER,
                                      i_quantity_price30    IN NUMBER,
                                      i_org_zone_1          IN VARCHAR2,
                                      i_org_zone_2          IN VARCHAR2,
                                      i_org_zone_3          IN VARCHAR2,
                                      i_org_zone_4          IN VARCHAR2,
                                      i_org_zone_5          IN VARCHAR2,
                                      i_org_zone_6          IN VARCHAR2,
                                      i_org_zone_7          IN VARCHAR2,
                                      i_org_zone_8          IN VARCHAR2,
                                      i_org_zone_9          IN VARCHAR2,
                                      i_org_zone_10         IN VARCHAR2,
                                      i_org_zone_11         IN VARCHAR2,
                                      i_org_zone_12         IN VARCHAR2,
                                      i_org_zone_13         IN VARCHAR2,
                                      i_org_zone_14         IN VARCHAR2,
                                      i_org_zone_15         IN VARCHAR2,
                                      i_org_zone_16         IN VARCHAR2,
                                      i_org_zone_17         IN VARCHAR2,
                                      i_org_zone_18         IN VARCHAR2,
                                      i_org_zone_19         IN VARCHAR2,
                                      i_org_zone_20         IN VARCHAR2,
                                      i_org_zone_21         IN VARCHAR2,
                                      i_org_zone_22         IN VARCHAR2,
                                      i_org_zone_23         IN VARCHAR2,
                                      i_org_zone_24         IN VARCHAR2,
                                      i_org_zone_25         IN VARCHAR2,
                                      i_org_zone_26         IN VARCHAR2,
                                      i_org_zone_27         IN VARCHAR2,
                                      i_org_zone_28         IN VARCHAR2,
                                      i_org_zone_29         IN VARCHAR2,
                                      i_org_zone_30         IN VARCHAR2)
   IS
      l_inventory_item_id   NUMBER;
      l_po_header_id        NUMBER;
      l_message             VARCHAR2 (2000);
      l_exists              NUMBER;
      l_promo_exists        NUMBER;
      l_action              VARCHAR2 (10);

      CURSOR c_zone
      IS
         SELECT 1 price_zone,
                i_price_zone1 price,
                i_quantity_zone1 quantity,
                i_quantity_price1 quantity_price
           FROM DUAL
          WHERE i_price_zone1 IS NOT NULL
         UNION
         SELECT 2 price_zone,
                i_price_zone2 price,
                i_quantity_zone2 quantity,
                i_quantity_price2 quantity_price
           FROM DUAL
          WHERE i_price_zone2 IS NOT NULL
         UNION
         SELECT 3 price_zone,
                i_price_zone3 price,
                i_quantity_zone3 quantity,
                i_quantity_price3 quantity_price
           FROM DUAL
          WHERE i_price_zone3 IS NOT NULL
         UNION
         SELECT 4 price_zone,
                i_price_zone4 price,
                i_quantity_zone4 quantity,
                i_quantity_price4 quantity_price
           FROM DUAL
          WHERE i_price_zone4 IS NOT NULL
         UNION
         SELECT 5 price_zone,
                i_price_zone5 price,
                i_quantity_zone5 quantity,
                i_quantity_price5 quantity_price
           FROM DUAL
          WHERE i_price_zone5 IS NOT NULL
         UNION
         SELECT 6 price_zone,
                i_price_zone6 price,
                i_quantity_zone6 quantity,
                i_quantity_price6 quantity_price
           FROM DUAL
          WHERE i_price_zone6 IS NOT NULL
         UNION
         SELECT 7 price_zone,
                i_price_zone7 price,
                i_quantity_zone7 quantity,
                i_quantity_price7 quantity_price
           FROM DUAL
          WHERE i_price_zone7 IS NOT NULL
         UNION
         SELECT 8 price_zone,
                i_price_zone8 price,
                i_quantity_zone8 quantity,
                i_quantity_price8 quantity_price
           FROM DUAL
          WHERE i_price_zone8 IS NOT NULL
         UNION
         SELECT 9 price_zone,
                i_price_zone9 price,
                i_quantity_zone9 quantity,
                i_quantity_price9 quantity_price
           FROM DUAL
          WHERE i_price_zone9 IS NOT NULL
         UNION
         SELECT 10 price_zone,
                i_price_zone10 price,
                i_quantity_zone10 quantity,
                i_quantity_price10 quantity_price
           FROM DUAL
          WHERE i_price_zone10 IS NOT NULL
         UNION
         SELECT 11 price_zone,
                i_price_zone11 price,
                i_quantity_zone11 quantity,
                i_quantity_price11 quantity_price
           FROM DUAL
          WHERE i_price_zone11 IS NOT NULL
         UNION
         SELECT 12 price_zone,
                i_price_zone12 price,
                i_quantity_zone12 quantity,
                i_quantity_price12 quantity_price
           FROM DUAL
          WHERE i_price_zone12 IS NOT NULL
         UNION
         SELECT 13 price_zone,
                i_price_zone13 price,
                i_quantity_zone13 quantity,
                i_quantity_price13 quantity_price
           FROM DUAL
          WHERE i_price_zone13 IS NOT NULL
         UNION
         SELECT 14 price_zone,
                i_price_zone14 price,
                i_quantity_zone14 quantity,
                i_quantity_price14 quantity_price
           FROM DUAL
          WHERE i_price_zone14 IS NOT NULL
         UNION
         SELECT 15 price_zone,
                i_price_zone15 price,
                i_quantity_zone15 quantity,
                i_quantity_price15 quantity_price
           FROM DUAL
          WHERE i_price_zone15 IS NOT NULL
         UNION
         SELECT 16 price_zone,
                i_price_zone16 price,
                i_quantity_zone16 quantity,
                i_quantity_price16 quantity_price
           FROM DUAL
          WHERE i_price_zone16 IS NOT NULL
         UNION
         SELECT 17 price_zone,
                i_price_zone17 price,
                i_quantity_zone17 quantity,
                i_quantity_price17 quantity_price
           FROM DUAL
          WHERE i_price_zone17 IS NOT NULL
         UNION
         SELECT 18 price_zone,
                i_price_zone18 price,
                i_quantity_zone18 quantity,
                i_quantity_price18 quantity_price
           FROM DUAL
          WHERE i_price_zone18 IS NOT NULL
         UNION
         SELECT 19 price_zone,
                i_price_zone19 price,
                i_quantity_zone19 quantity,
                i_quantity_price19 quantity_price
           FROM DUAL
          WHERE i_price_zone19 IS NOT NULL
         UNION
         SELECT 20 price_zone,
                i_price_zone20 price,
                i_quantity_zone20 quantity,
                i_quantity_price20 quantity_price
           FROM DUAL
          WHERE i_price_zone20 IS NOT NULL
         UNION
         SELECT 21 price_zone,
                i_price_zone21 price,
                i_quantity_zone21 quantity,
                i_quantity_price21 quantity_price
           FROM DUAL
          WHERE i_price_zone21 IS NOT NULL
         UNION
         SELECT 22 price_zone,
                i_price_zone22 price,
                i_quantity_zone22 quantity,
                i_quantity_price22 quantity_price
           FROM DUAL
          WHERE i_price_zone22 IS NOT NULL
         UNION
         SELECT 23 price_zone,
                i_price_zone23 price,
                i_quantity_zone23 quantity,
                i_quantity_price23 quantity_price
           FROM DUAL
          WHERE i_price_zone23 IS NOT NULL
         UNION
         SELECT 24 price_zone,
                i_price_zone24 price,
                i_quantity_zone24 quantity,
                i_quantity_price24 quantity_price
           FROM DUAL
          WHERE i_price_zone24 IS NOT NULL
         UNION
         SELECT 25 price_zone,
                i_price_zone25 price,
                i_quantity_zone25 quantity,
                i_quantity_price25 quantity_price
           FROM DUAL
          WHERE i_price_zone25 IS NOT NULL
         UNION
         SELECT 26 price_zone,
                i_price_zone26 price,
                i_quantity_zone26 quantity,
                i_quantity_price26 quantity_price
           FROM DUAL
          WHERE i_price_zone26 IS NOT NULL
         UNION
         SELECT 27 price_zone,
                i_price_zone27 price,
                i_quantity_zone27 quantity,
                i_quantity_price27 quantity_price
           FROM DUAL
          WHERE i_price_zone27 IS NOT NULL
         UNION
         SELECT 28 price_zone,
                i_price_zone28 price,
                i_quantity_zone28 quantity,
                i_quantity_price28 quantity_price
           FROM DUAL
          WHERE i_price_zone28 IS NOT NULL
         UNION
         SELECT 29 price_zone,
                i_price_zone29 price,
                i_quantity_zone29 quantity,
                i_quantity_price29 quantity_price
           FROM DUAL
          WHERE i_price_zone29 IS NOT NULL
         UNION
         SELECT 30 price_zone,
                i_price_zone30 price,
                i_quantity_zone30 quantity,
                i_quantity_price30 quantity_price
           FROM DUAL
          WHERE i_price_zone30 IS NOT NULL;
   BEGIN
      g_err_callpoint := 'load_bpa_price_zone_stg';

      BEGIN
         SELECT inventory_item_id
           INTO l_inventory_item_id
           FROM mtl_system_items_b
          WHERE     segment1 = i_item_number
                AND organization_id =
                       FND_PROFILE.VALUE ('XXWC_ITEM_MASTER_ORG');
      EXCEPTION
         WHEN OTHERS
         THEN
            l_inventory_item_id := NULL;
      END;

      IF l_inventory_item_id IS NULL
      THEN
         l_message := 'Item is invalid';
         RAISE_APPLICATION_ERROR (-20001, l_message);
      END IF;

      /*
       IF i_price_zone1 IS NULL THEN
         l_message := 'price_zone can not be null';
         RAISE_APPLICATION_ERROR(-20001,l_message);
       END IF;

       IF i_quanity_zone1 IS NULL THEN
         l_message := 'price_zone_price can not be null';
         RAISE_APPLICATION_ERROR(-20001,l_message);
       END IF;

      IF i_quanity_price_zone1 < 0 THEN
         l_message := 'price_zone_price can not less than 0';
         RAISE_APPLICATION_ERROR(-20001,l_message);
      END IF;

      IF i_quantity_zone1 IS NULL AND i_quantity_price IS NOT NULL THEN
         l_message := 'quantity_zone1 can not null if quantity_zone1 is populated';
         RAISE_APPLICATION_ERROR(-20001,l_message);
       END IF;

      IF i_price_zone_quantity IS NOT NULL AND i_price_zone_quantity_price IS NULL THEN
         l_message := 'price_zone_quatity_price can not null if price_zone_quantity is populated';
         RAISE_APPLICATION_ERROR(-20001,l_message);
       END IF;

      IF nvl(i_price_zone_quantity,0) < 0 THEN
         l_message := 'price_zone_quatity can not be less than 0';
         RAISE_APPLICATION_ERROR(-20001,l_message);
       END IF;

      IF nvl(i_price_zone_quantity_price,0) < 0 THEN
         l_message := 'price_zone_quatity_price can not be less than 0';
         RAISE_APPLICATION_ERROR(-20001,l_message);
       END IF;

      */


      BEGIN
         SELECT pha.po_header_id                            --, pla.po_line_id
           INTO l_po_header_id                                --, l_po_line_id
           FROM po_headers pha --Modified by Manjula on 21-Nov-14 for TMS # 20141002-00041 Multi-Org
          --po_lines pla
          WHERE pha.segment1 = i_po_number AND ROWNUM = 1;
      EXCEPTION
         WHEN OTHERS
         THEN
            l_po_header_id := NULL;
      --l_po_line_id := NULL;
      END;

      IF l_po_header_id IS NULL
      THEN
         l_message := 'PO number is invalid. PO ' || i_po_number;
         RAISE_APPLICATION_ERROR (-20001, l_message);
      END IF;

      IF l_inventory_item_id IS NOT NULL AND l_po_header_id IS NOT NULL
      THEN
         BEGIN
            INSERT
              INTO apps.xxwc_BPA_PZ_STAGING_TBL (creation_date,
                                                 created_by,
                                                 last_update_date,
                                                 last_updated_by,
                                                 po_number     --,po_header_id
                                                          --,inventory_item_id
                                                 ,
                                                 item_number,
                                                 expiration_date,
                                                 implement_date,
                                                 bpa_table_flag,
                                                 national_price,
                                                 national_quantity,
                                                 national_break,
                                                 promo_price,
                                                 promo_start_date,
                                                 promo_end_date,
                                                 promo_table_flag,
                                                 price_zone_price1,
                                                 price_zone_quantity1,
                                                 price_zone_quantity_price1,
                                                 price_zone_price2,
                                                 price_zone_quantity2,
                                                 price_zone_quantity_price2,
                                                 price_zone_price3,
                                                 price_zone_quantity3,
                                                 price_zone_quantity_price3,
                                                 price_zone_price4,
                                                 price_zone_quantity4,
                                                 price_zone_quantity_price4,
                                                 price_zone_price5,
                                                 price_zone_quantity5,
                                                 price_zone_quantity_price5,
                                                 price_zone_price6,
                                                 price_zone_quantity6,
                                                 price_zone_quantity_price6,
                                                 price_zone_price7,
                                                 price_zone_quantity7,
                                                 price_zone_quantity_price7,
                                                 price_zone_price8,
                                                 price_zone_quantity8,
                                                 price_zone_quantity_price8,
                                                 price_zone_price9,
                                                 price_zone_quantity9,
                                                 price_zone_quantity_price9,
                                                 price_zone_price10,
                                                 price_zone_quantity10,
                                                 price_zone_quantity_price10,
                                                 price_zone_price11,
                                                 price_zone_quantity11,
                                                 price_zone_quantity_price11,
                                                 price_zone_price12,
                                                 price_zone_quantity12,
                                                 price_zone_quantity_price12,
                                                 price_zone_price13,
                                                 price_zone_quantity13,
                                                 price_zone_quantity_price13,
                                                 price_zone_price14,
                                                 price_zone_quantity14,
                                                 price_zone_quantity_price14,
                                                 price_zone_price15,
                                                 price_zone_quantity15,
                                                 price_zone_quantity_price15,
                                                 price_zone_price16,
                                                 price_zone_quantity16,
                                                 price_zone_quantity_price16,
                                                 price_zone_price17,
                                                 price_zone_quantity17,
                                                 price_zone_quantity_price17,
                                                 price_zone_price18,
                                                 price_zone_quantity18,
                                                 price_zone_quantity_price18,
                                                 price_zone_price19,
                                                 price_zone_quantity19,
                                                 price_zone_quantity_price19,
                                                 price_zone_price20,
                                                 price_zone_quantity20,
                                                 price_zone_quantity_price20,
                                                 price_zone_price21,
                                                 price_zone_quantity21,
                                                 price_zone_quantity_price21,
                                                 price_zone_price22,
                                                 price_zone_quantity22,
                                                 price_zone_quantity_price22,
                                                 price_zone_price23,
                                                 price_zone_quantity23,
                                                 price_zone_quantity_price23,
                                                 price_zone_price24,
                                                 price_zone_quantity24,
                                                 price_zone_quantity_price24,
                                                 price_zone_price25,
                                                 price_zone_quantity25,
                                                 price_zone_quantity_price25,
                                                 price_zone_price26,
                                                 price_zone_quantity26,
                                                 price_zone_quantity_price26,
                                                 price_zone_price27,
                                                 price_zone_quantity27,
                                                 price_zone_quantity_price27,
                                                 price_zone_price28,
                                                 price_zone_quantity28,
                                                 price_zone_quantity_price28,
                                                 price_zone_price29,
                                                 price_zone_quantity29,
                                                 price_zone_quantity_price29,
                                                 price_zone_price30,
                                                 price_zone_quantity30,
                                                 price_zone_quantity_price30,
                                                 price_zone_table_flag,
                                                 approve_flag)
            VALUES (SYSDATE                                    --creation_date
                           ,
                    fnd_global.user_id                            --created_by
                                      ,
                    SYSDATE                                 --last_update_date
                           ,
                    fnd_global.user_id                       --last_updated_by
                                      ,
                    i_po_number                                    --po_number
                               --,NULL --po_header_id
                               --,NULL --inventory_item_id
                    ,
                    i_item_number                               -- item_number
                                 ,
                    i_expiration_date                       -- expiration_date
                                     ,
                    i_implement_date                         -- implement_date
                                    ,
                    1                                         --bpa_table_flag
                     ,
                    l_national_price                          --national_price
                                    ,
                    l_national_quantity                    --national_quantity
                                       ,
                    l_national_break                          --national_break
                                    ,
                    i_promo_price                                --promo_price
                                 ,
                    i_promo_start_date                      --promo_start_date
                                      ,
                    i_promo_end_date                          --promo_end_date
                                    ,
                    1                                       --promo_table_flag
                     ,
                    i_price_zone1                                --price_zone1
                                 ,
                    i_quantity_zone1                       --price_zone_price1
                                    ,
                    i_quantity_price1                  -- price_zone_quantity1
                                     ,
                    i_price_zone2                          --price_zone_price2
                                 ,
                    i_quantity_zone2                    --price_zone_quantity2
                                    ,
                    i_quantity_price2             --price_zone_quantity_price2
                                     ,
                    i_price_zone3                          --price_zone_price3
                                 ,
                    i_quantity_zone3                    --price_zone_quantity3
                                    ,
                    i_quantity_price3             --price_zone_quantity_price3
                                     ,
                    i_price_zone4                          --price_zone_price4
                                 ,
                    i_quantity_zone4                    --price_zone_quantity4
                                    ,
                    i_quantity_price4             --price_zone_quantity_price4
                                     ,
                    i_price_zone5                          --price_zone_price5
                                 ,
                    i_quantity_zone5 --i_quantity_zone5 --price_zone_quantity5
                                    ,
                    i_quantity_price5             --price_zone_quantity_price5
                                     ,
                    i_price_zone6                          --price_zone_price6
                                 ,
                    i_quantity_zone6                    --price_zone_quantity6
                                    ,
                    i_quantity_price6             --price_zone_quantity_price6
                                     ,
                    i_price_zone7                          --price_zone_price7
                                 ,
                    i_quantity_zone7                    --price_zone_quantity7
                                    ,
                    i_quantity_price7            -- price_zone_quantity_price7
                                     ,
                    i_price_zone8                          --price_zone_price8
                                 ,
                    i_quantity_zone8                    --price_zone_quantity8
                                    ,
                    i_quantity_price8             --price_zone_quantity_price8
                                     ,
                    i_price_zone9                          --price_zone_price9
                                 ,
                    i_quantity_zone9                    --price_zone_quantity9
                                    ,
                    i_quantity_price9             --price_zone_quantity_price9
                                     ,
                    i_price_zone10                        --price_zone_price10
                                  ,
                    i_quantity_zone10                  --price_zone_quantity10
                                     ,
                    i_quantity_price10           --price_zone_quantity_price10
                                      ,
                    i_price_zone11                        --price_zone_price11
                                  ,
                    i_quantity_zone11                  --price_zone_quantity11
                                     ,
                    i_quantity_price11           --price_zone_quantity_price11
                                      ,
                    i_price_zone12                        --price_zone_price12
                                  ,
                    i_quantity_zone12                  --price_zone_quantity12
                                     ,
                    i_quantity_price12           --price_zone_quantity_price12
                                      ,
                    i_price_zone13                        --price_zone_price13
                                  ,
                    i_quantity_zone13                  --price_zone_quantity13
                                     ,
                    i_quantity_price13           --price_zone_quantity_price13
                                      ,
                    i_price_zone14                        --price_zone_price14
                                  ,
                    i_quantity_zone14                  --price_zone_quantity14
                                     ,
                    i_quantity_price14           --price_zone_quantity_price14
                                      ,
                    i_price_zone15                        --price_zone_price15
                                  ,
                    i_quantity_zone15                  --price_zone_quantity15
                                     ,
                    i_quantity_price15           --price_zone_quantity_price15
                                      ,
                    i_price_zone16                        --price_zone_price16
                                  ,
                    i_quantity_zone16                  --price_zone_quantity16
                                     ,
                    i_quantity_price16           --price_zone_quantity_price16
                                      ,
                    i_price_zone17                        --price_zone_price17
                                  ,
                    i_quantity_zone17                  --price_zone_quantity17
                                     ,
                    i_quantity_price17           --price_zone_quantity_price17
                                      ,
                    i_price_zone18                        --price_zone_price18
                                  ,
                    i_quantity_zone18                  --price_zone_quantity18
                                     ,
                    i_quantity_price18           --price_zone_quantity_price18
                                      ,
                    i_price_zone19                        --price_zone_price19
                                  ,
                    i_quantity_zone19                  --price_zone_quantity19
                                     ,
                    i_quantity_price19           --price_zone_quantity_price19
                                      ,
                    i_price_zone20                        --price_zone_price20
                                  ,
                    i_quantity_zone20                  --price_zone_quantity20
                                     ,
                    i_quantity_price20           --price_zone_quantity_price20
                                      ,
                    i_price_zone21                        --price_zone_price21
                                  ,
                    i_quantity_zone21                  --price_zone_quantity21
                                     ,
                    i_quantity_price21           --price_zone_quantity_price21
                                      ,
                    i_price_zone22                        --price_zone_price22
                                  ,
                    i_quantity_zone22                  --price_zone_quantity22
                                     ,
                    i_quantity_price22           --price_zone_quantity_price22
                                      ,
                    i_price_zone23                        --price_zone_price23
                                  ,
                    i_quantity_zone23                  --price_zone_quantity23
                                     ,
                    i_quantity_price23           --price_zone_quantity_price23
                                      ,
                    i_price_zone24                        --price_zone_price24
                                  ,
                    i_quantity_zone24                  --price_zone_quantity24
                                     ,
                    i_quantity_price24           --price_zone_quantity_price24
                                      ,
                    i_price_zone25                        --price_zone_price25
                                  ,
                    i_quantity_zone25                  --price_zone_quantity25
                                     ,
                    i_quantity_price25           --price_zone_quantity_price25
                                      ,
                    i_price_zone26                        --price_zone_price26
                                  ,
                    i_quantity_zone26                  --price_zone_quantity26
                                     ,
                    i_quantity_price26           --price_zone_quantity_price26
                                      ,
                    i_price_zone27                        --price_zone_price27
                                  ,
                    i_quantity_zone27                  --price_zone_quantity27
                                     ,
                    i_quantity_price27           --price_zone_quantity_price27
                                      ,
                    i_price_zone28                        --price_zone_price28
                                  ,
                    i_quantity_zone28                  --price_zone_quantity28
                                     ,
                    i_quantity_price28           --price_zone_quantity_price28
                                      ,
                    i_price_zone29                        --price_zone_price29
                                  ,
                    i_quantity_zone29                  --price_zone_quantity29
                                     ,
                    i_quantity_price29           --price_zone_quantity_price29
                                      ,
                    i_price_zone30                        --price_zone_price30
                                  ,
                    i_quantity_zone30                  --price_zone_quantity30
                                     ,
                    i_quantity_price30           --price_zone_quantity_price30
                                      ,
                    1                                  --price_zone_table_flag
                     ,
                    1                                           --approve_flag
                     );
         EXCEPTION
            WHEN OTHERS
            THEN
               g_err_callpoint := g_err_callpoint || ' 10';

               RAISE g_exception;
         END;
      END IF;
   /*
   IF l_inventory_item_id IS NOT NULL AND l_po_header_id IS NOT NULL THEN


     FOR R_ZONE IN C_ZONE
       LOOP

     BEGIN

       SELECT count(*)
       into   l_exists
       from   APPS.XXWC_BPA_PRICE_ZONE_TBL --Modified by Manjula on 21-Nov-14 for TMS # 20141002-00041 Multi-Org
       WHERE  po_header_id = l_po_header_id
       AND    inventory_item_id = l_inventory_item_id
       AND    price_zone = r_zone.price_zone
       ;
     exception
         WHEN others THEN
             l_exists := 0;
     END;

     if l_exists = 0 then

     BEGIN
       INSERT INTO APPS.XXWC_BPA_PRICE_ZONE_TBL--Modified by Manjula on 21-Nov-14 for TMS # 20141002-00041 Multi-Org
         (creation_date
         ,created_by
         ,last_update_date
         ,last_updated_by
         ,po_header_id
         ,inventory_item_id
         ,price_zone
         ,price_zone_price
         ,price_zone_quantity
         ,price_zone_quantity_price)
         VALUES
         (SYSDATE
         ,fnd_global.user_id
         ,SYSDATE
         ,fnd_global.user_id
         ,l_po_header_id
         ,l_inventory_item_id
         ,r_zone.price_zone
         ,r_zone.price
         ,r_zone.quantity
         ,r_zone.quantity_price);
     EXCEPTION
       WHEN others THEN
           l_message := 'Could not insert into XXWC_BPA_PRICE_ZONE_TBL '|| SQLCODE || SQLERRM;
           RAISE_APPLICATION_ERROR(-20001,l_message);
     END;

     ELSE

       BEGIN
         UPDATE APPS.XXWC_BPA_PRICE_ZONE_TBL--Modified by Manjula on 21-Nov-14 for TMS # 20141002-00041 Multi-Org
         SET    price_zone_price = r_zone.price
               ,price_zone_quantity = r_zone.quantity
               ,price_zone_quantity_price = r_zone.quantity_price
               ,last_updated_by = fnd_global.user_id
               ,last_update_date = sysdate
         WHERE  po_header_id = l_po_header_id
         AND    inventory_item_id = l_inventory_item_id
         AND    price_zone = r_zone.price_zone;
     EXCEPTION
       WHEN others THEN
           l_message := 'Could not update XXWC_BPA_PRICE_ZONE_TBL '|| SQLCODE || SQLERRM;
           RAISE_APPLICATION_ERROR(-20001,l_message);
     END;

     END IF;

     END LOOP;


     BEGIN

       SELECT count(*)
       INTO   l_promo_exists
       from   APPS.XXWC_BPA_PROMO_TBL--Modified by Manjula on 21-Nov-14 for TMS # 20141002-00041 Multi-Org
       WHERE  po_header_id = l_po_header_id
       AND    inventory_item_id = l_inventory_item_id;
       --AND    nvl(price_zone = i_price_zone
       exception
         WHEN others THEN
             l_promo_exists := 0;
     END;

     IF l_promo_exists = 0 THEN

     if i_promo_price is not null then

       IF i_promo_start_date IS NULL THEN
          l_message := 'Must have promo start date';
          RAISE_APPLICATION_ERROR(-20001,l_message);
        END IF;

       IF i_promo_end_date IS NULL THEN
          l_message := 'Must have promo end date';
          RAISE_APPLICATION_ERROR(-20001,l_message);
        END IF;



     BEGIN
       INSERT INTO APPS.XXWC_BPA_PROMO_TBL
         (creation_date
         ,created_by
         ,last_update_date
         ,last_updated_by
         ,po_header_id
         ,inventory_item_id
         ,promo_price
         ,promo_start_date
         ,promo_end_date)
         VALUES
         (SYSDATE
         ,fnd_global.user_id
         ,SYSDATE
         ,fnd_global.user_id
         ,l_po_header_id
         ,l_inventory_item_id
         ,i_promo_price
         ,i_promo_start_date
         ,i_promo_end_date);
     EXCEPTION
       WHEN others THEN
           l_message := 'Could not insert into XXWC_BPA_PROMO_TBL '|| SQLCODE || SQLERRM;
           RAISE_APPLICATION_ERROR(-20001,l_message);
     END;

     ELSE

     BEGIN
         UPDATE APPS.XXWC_BPA_PROMO_TBL--Modified by Manjula on 21-Nov-14 for TMS # 20141002-00041 Multi-Org
         SET    promo_price = i_promo_price
               ,promo_start_date = i_promo_start_date
               ,promo_end_date = i_promo_end_date
               ,last_updated_by = fnd_global.user_id
               ,last_update_date = sysdate
         WHERE  po_header_id = l_po_header_id
         AND    inventory_item_id = l_inventory_item_id
         ;
     EXCEPTION
       WHEN others THEN
           l_message := 'Could not update XXWC_BPA_PROMO_TBL '|| SQLCODE || SQLERRM;
           RAISE_APPLICATION_ERROR(-20001,l_message);
     END;


      END IF;


   ELSE

       DELETE APPS.XXWC_BPA_PROMO_TBL--Modified by Manjula on 21-Nov-14 for TMS # 20141002-00041 Multi-Org
         WHERE  po_header_id = l_po_header_id
         AND    inventory_item_id = l_inventory_item_id;

    END IF;

   END IF;
   */

   EXCEPTION
      WHEN g_exception
      THEN
         g_err_msg :=
               'Error in '
            || g_err_callpoint
            || ' WITH  '
            || SUBSTR (SQLERRM, 1, 1000);
         --dbms_output.put_line(l_fulltext);

         xxcus_error_pkg.xxcus_error_main_api (
            p_called_from         => g_err_callfrom,
            p_calling             => g_err_callpoint,
            p_ora_error_msg       => SQLERRM,
            p_error_desc          =>    'Error in call '
                                     || g_err_callfrom
                                     || g_err_callpoint,
            p_distribution_list   => g_distro_list,
            p_module              => g_module);
      WHEN OTHERS
      THEN
         g_err_msg :=
               'Error in '
            || g_err_callpoint
            || ' WITH  '
            || SUBSTR (SQLERRM, 1, 1000);
         --dbms_output.put_line(l_fulltext);

         xxcus_error_pkg.xxcus_error_main_api (
            p_called_from         => g_err_callfrom,
            p_calling             => g_err_callpoint,
            p_ora_error_msg       => SQLERRM,
            p_error_desc          =>    'WHEN OTHERS in call '
                                     || g_err_callfrom
                                     || g_err_callpoint,
            p_distribution_list   => g_distro_list,
            p_module              => g_module);
   END;

   /*************************************************************************************************
   *   Function get_price_zone_info                                                                 *
   *   Purpose : returns the price value from the price zone                                        *
   *                                                                                                *
   *                                                                                                *
   *   FUNCTION get_price_zone_info  (p_po_header_id IN NUMBER,                                     *
   *                                  p_inventory_item_id IN NUMBER,                                *
   *                                  p_price_zone  IN NUMBER,                                      *
   *                                  p_attribute   IN VARCHAR2)                                    *
   *                           RETURN NUMBER;                                                       *
   *                                                                                                *
   *   REVISIONS:                                                                                   *
   *   Ver        Date        Author                     Description                                *
   *   ---------  ----------  ---------------         -------------------------                     *
   *   1.0        02/08/2014   Lee Spitzer              Initial Versions TMS 20130917-00676         *
   *                                                         Vendor Cost Management Improvements    *
   *                                                                                                *
   /************************************************************************************************/


   FUNCTION get_price_zone_info (p_po_header_id        IN NUMBER,
                                 p_inventory_item_id   IN NUMBER,
                                 p_price_zone          IN NUMBER,
                                 p_attribute           IN VARCHAR2)
      RETURN NUMBER
   IS
      l_price                  NUMBER;
      --l_quantity  NUMBER;
      --l_quantity_price   NUMBER;
      l_return                 NUMBER DEFAULT NULL;
      l_effective_start_date   DATE;
      l_effective_end_date     DATE;
   BEGIN
      g_err_callpoint := 'get_price_zone_info';

      BEGIN
         SELECT price_zone_price --, price_zone_quantity, price_zone_quantity_price --, effective_start_date, effective_end_date
           INTO l_price --, l_quantity, l_quantity_price--, l_effective_start_date, l_effective_end_date
           FROM XXWC.xxwc_bpa_price_zone_tbl --TMS # 20141002-00041 Multi-Org - Lee Spitzer 12/1/2014 - used to work with Web ADI and Form for Multi-Org Changes;
          WHERE     po_header_id = p_po_header_id
                AND inventory_item_id = p_inventory_item_id
                AND price_zone = p_price_zone
                AND fnd_profile.VALUE ('ORG_ID') = org_id; --TMS # 20141002-00041 Multi-Org - Lee Spitzer 12/1/2014 - used to work with Web ADI and Form for Multi-Org Changes;
      EXCEPTION
         WHEN OTHERS
         THEN
            l_price := NULL;
      --l_quantity := NULL;
      --l_quantity_price := NULL;
      END;

      IF p_attribute = 'PRICE'
      THEN
         l_return := l_price;
      /*
      ELSIF p_attribute = 'QUANTITY' THEN

            l_return := l_quantity;

      ELSIF p_attribute = 'QUANTITY_PRICE' THEN

            l_return := l_quantity_price;

     /*
      ELSIF p_attribute = 'EFFECTIVE_START_DATE' THEN

            l_return := to_char(l_effective_start_date);

      ELSIF p_attribute = 'EFFECTIVE_END_DATE' THEN

            l_return := to_char(l_effective_end_date);
      */
      ELSE
         l_return := NULL;
      END IF;

      RETURN l_return;
   EXCEPTION
      WHEN OTHERS
      THEN
         RETURN l_return;

         g_err_msg :=
               'Error in '
            || g_err_callpoint
            || ' WITH  '
            || SUBSTR (SQLERRM, 1, 1000);
         --dbms_output.put_line(l_fulltext);

         xxcus_error_pkg.xxcus_error_main_api (
            p_called_from         => g_err_callfrom,
            p_calling             => g_err_callpoint,
            p_ora_error_msg       => SQLERRM,
            p_error_desc          =>    'Error in call '
                                     || g_err_callfrom
                                     || g_err_callpoint,
            p_distribution_list   => g_distro_list,
            p_module              => g_module);
   END;


   /*************************************************************************************************
   *   Function get_price_zone_break_info                                                           *
   *   Purpose : returns the price zone break value from the price zone                             *
   *                                                                                                *
   *                                                                                                *
   *   FUNCTION get_price_zone_break_info  (p_po_header_id IN NUMBER,                               *
   *                                  p_inventory_item_id IN NUMBER,                                *
   *                                  p_price_zone  IN NUMBER,                                      *
   *                                  p_quantity    IN NUMBER,                                      *
   *                                  p_attribute   IN VARCHAR2)                                    *
   *                           RETURN NUMBER;                                                       *
   *                                                                                                *
   *   REVISIONS:                                                                                   *
   *   Ver        Date        Author                     Description                                *
   *   ---------  ----------  ---------------         -------------------------                     *
   *   1.0        02/08/2014   Lee Spitzer              Initial Versions TMS 20130917-00676         *
   *                                                         Vendor Cost Management Improvements    *
   *   1.1        12/29/2014   Lee Spitzer              TMS Ticket #20141024-00033 Unable to expire *
   *                                                         BPA lines with updated item description*
   *                                                          and update Price Zone with null zone  *
   *                                                                                                *
   /************************************************************************************************/

   FUNCTION get_price_zone_break_info (p_po_header_id        IN NUMBER,
                                       p_inventory_item_id   IN NUMBER,
                                       p_price_zone          IN NUMBER,
                                       p_quantity            IN NUMBER,
                                       p_attribute           IN VARCHAR2)
      RETURN NUMBER
   IS
      --l_price   NUMBER;
      l_quantity               NUMBER;
      l_quantity_price         NUMBER;
      l_return                 NUMBER DEFAULT NULL;
      l_effective_start_date   DATE;
      l_effective_end_date     DATE;
   BEGIN
      g_err_callpoint := 'get_price_zone_break_info';

      IF P_QUANTITY = 0
      THEN
         --Added 12/29/2014 TMS Ticket #20141024-00033
         --Check to see if there is 0 quantity on price break table to retrieve break price when 0 quantity
         BEGIN
            --Added --Check if p_quantity of 0 exists in price breaks table
            SELECT NVL (price_zone_quantity, 0),
                   NVL (price_zone_quantity_price, 0) --, effective_start_date, effective_end_date
              INTO l_quantity, l_quantity_price --, l_effective_start_date, l_effective_end_date
              FROM XXWC.XXWC_BPA_PRICE_ZONE_BREAKS_TBL --TMS # 20141002-00041 Multi-Org - Lee Spitzer 12/1/2014 - used to work with Web ADI and Form for Multi-Org Changes
             WHERE     po_header_id = p_po_header_id
                   AND inventory_item_id = p_inventory_item_id
                   AND price_zone = p_price_zone
                   AND price_zone_quantity = p_quantity
                   AND fnd_profile.VALUE ('ORG_ID') = org_id;      --TMS # 201
         EXCEPTION
            WHEN NO_DATA_FOUND
            THEN
               l_quantity := NULL;
               l_quantity_price := NULL;
            WHEN TOO_MANY_ROWS
            THEN
               l_quantity := NULL;
               l_quantity_price := NULL;
            WHEN OTHERS
            THEN
               l_quantity := NULL;
               l_quantity_price := NULL;
         END;

         --If records was not found, then get price from lines table
         IF l_quantity IS NULL
         THEN --Added 12/29/14 TMS Ticket #20141024-00033 - If there isn't a break price at 0 quantity, get the line price
            BEGIN
               SELECT 0, price_zone_price --, effective_start_date, effective_end_date
                 INTO l_quantity, l_quantity_price --, l_effective_start_date, l_effective_end_date
                 FROM XXWC.xxwc_bpa_price_zone_tbl --TMS # 20141002-00041 Multi-Org - Lee Spitzer 12/1/2014 - used to work with Web ADI and Form for Multi-Org Changes
                WHERE     po_header_id = p_po_header_id
                      AND inventory_item_id = p_inventory_item_id
                      AND price_zone = p_price_zone
                      AND fnd_profile.VALUE ('ORG_ID') = org_id; --TMS # 20141002-00041 Multi-Org - Lee Spitzer 12/1/2014 - used to work with Web ADI and Form for Multi-Org Changes
            EXCEPTION
               WHEN OTHERS
               THEN
                  l_quantity := NULL;
                  l_quantity_price := NULL;
            END;
         END IF;             --End 12/29/2014 Added TMS Ticket #20141024-00033
      ELSE
         BEGIN
            SELECT price_zone_quantity, price_zone_quantity_price --, effective_start_date, effective_end_date
              INTO l_quantity, l_quantity_price --, l_effective_start_date, l_effective_end_date
              FROM XXWC.XXWC_BPA_PRICE_ZONE_BREAKS_TBL --TMS # 20141002-00041 Multi-Org - Lee Spitzer 12/1/2014 - used to work with Web ADI and Form for Multi-Org Changes
             WHERE     po_header_id = p_po_header_id
                   AND inventory_item_id = p_inventory_item_id
                   AND price_zone = p_price_zone
                   AND price_zone_quantity = p_quantity
                   AND fnd_profile.VALUE ('ORG_ID') = org_id; --TMS # 20141002-00041 Multi-Org Lee Spitzer 12/1/2014 - used to work with Web ADI and Form for Multi-Org Changes
         EXCEPTION
            WHEN OTHERS
            THEN
               --l_price := NULL;
               l_quantity := NULL;
               l_quantity_price := NULL;
         END;
      END IF;

      --IF p_attribute = 'PRICE' THEN

      --      l_return := l_price;

      --ELSIF p_attribute = 'QUANTITY' THEN

      IF p_attribute = 'QUANTITY'
      THEN
         l_return := l_quantity;
      ELSIF p_attribute = 'QUANTITY_PRICE'
      THEN
         l_return := l_quantity_price;
      /*
       ELSIF p_attribute = 'EFFECTIVE_START_DATE' THEN

             l_return := to_char(l_effective_start_date);

       ELSIF p_attribute = 'EFFECTIVE_END_DATE' THEN

             l_return := to_char(l_effective_end_date);
       */
      ELSE
         l_return := NULL;
      END IF;

      RETURN l_return;
   EXCEPTION
      WHEN OTHERS
      THEN
         l_return := NULL;

         g_err_msg :=
               'Error in '
            || g_err_callpoint
            || ' WITH  '
            || SUBSTR (SQLERRM, 1, 1000);
         --dbms_output.put_line(l_fulltext);

         xxcus_error_pkg.xxcus_error_main_api (
            p_called_from         => g_err_callfrom,
            p_calling             => g_err_callpoint,
            p_ora_error_msg       => SQLERRM,
            p_error_desc          =>    'Error in call '
                                     || g_err_callfrom
                                     || g_err_callpoint,
            p_distribution_list   => g_distro_list,
            p_module              => g_module);
   END;

   /*************************************************************************************************
   *   Procedure load_vendor_price_zone                                                             *
   *   Purpose : Used to load vendor price zones from the Vendor Price Zone Web                     *
   *                                                                                                *
   *                                                                                                *
   *   PROCEDURE load_vendor_price_zone (p_vendor IN VARCHAR2,                                      *
   *                                 p_vendor_site IN VARCHAR2,                                     *
   *                                 p_price_zone IN NUMBER,                                        *
   *                                 p_organization_code IN VARCHAR2,                               *
   *                                 p_org_pricing_zone IN VARCHAR2,                                *
   *                                 p_org_type IN VARCHAR2,                                        *
   *                                 p_district IN VARCHAR2,                                        *
   *                                 p_region IN VARCHAR2,                                          *
   *                                 p_organization_name IN VARCHAR2);                              *
   *                                                                                                *
   *   REVISIONS:                                                                                   *
   *   Ver        Date        Author                     Description                                *
   *   ---------  ----------  ---------------         -------------------------                     *
   *   1.0        02/08/2014   Lee Spitzer              Initial Versions TMS 20130917-00676         *
   *                                                         Vendor Cost Management Improvements    *
   *   1.1        12/29/2014   Lee Spitzer              TMS Ticket #20141024-00033 Unable to expire *
   *                                                         BPA lines with updated item description*
   *                                                          and update Price Zone with null zone  *
   *                                                                                                *
   *                                                                                                *
   /************************************************************************************************/



   PROCEDURE load_vendor_price_zone (p_vendor              IN VARCHAR2,
                                     p_vendor_site         IN VARCHAR2,
                                     p_price_zone          IN NUMBER,
                                     p_organization_code   IN VARCHAR2,
                                     p_org_pricing_zone    IN VARCHAR2,
                                     p_org_type            IN VARCHAR2,
                                     p_district            IN VARCHAR2,
                                     p_region              IN VARCHAR2,
                                     p_organization_name   IN VARCHAR2)
   IS
      l_vendor_id         NUMBER;
      l_vendor_site_id    NUMBER;
      l_organization_id   NUMBER;
      l_exists            NUMBER;
      l_message           VARCHAR2 (2000);
   BEGIN
      g_err_callfrom := 'load_vendor_price_zone';

      BEGIN
         SELECT vendor_id
           INTO l_vendor_id
           FROM ap_suppliers
          WHERE vendor_name = p_vendor;
      EXCEPTION
         WHEN OTHERS
         THEN
            l_vendor_id := NULL;
      END;

      IF l_vendor_id IS NULL
      THEN
         l_message := 'Could not find Vendor';
         RAISE_APPLICATION_ERROR (-20001, l_message);
      END IF;


      BEGIN
         SELECT vendor_site_id
           INTO l_vendor_site_id
           FROM ap_supplier_sites_all
          WHERE vendor_id = l_vendor_id AND vendor_site_code = p_vendor_site;
      EXCEPTION
         WHEN OTHERS
         THEN
            l_vendor_site_id := NULL;
      END;

      IF l_vendor_site_id IS NULL
      THEN
         l_message := 'Could not find Vendor Site';
         RAISE_APPLICATION_ERROR (-20001, l_message);
      END IF;


      BEGIN
         SELECT organization_id
           INTO l_organization_id
           FROM mtl_parameters
          WHERE organization_code = p_organization_code;
      EXCEPTION
         WHEN OTHERS
         THEN
            l_organization_id := NULL;
      END;

      IF l_organization_id IS NULL
      THEN
         l_message := 'Could not find Org';
         RAISE_APPLICATION_ERROR (-20001, l_message);
      END IF;


      IF p_price_zone IS NOT NULL
      THEN
         IF p_price_zone < 1 OR p_price_zone > 30
         THEN
            l_message := 'Price zone value must be between 1 and 30';
            RAISE_APPLICATION_ERROR (-20001, l_message);
         END IF;
      END IF;


      BEGIN
         SELECT COUNT (*)
           INTO l_exists
           FROM XXWC.XXWC_VENDOR_PRICE_ZONE_TBL xvpzt
          WHERE     organization_id = l_organization_id
                AND vendor_id = l_vendor_id
                AND vendor_site_id = l_vendor_site_id;
      EXCEPTION
         WHEN OTHERS
         THEN
            l_exists := 0;
      END;

      IF l_exists > 0
      THEN
         --if p_price_zone is not null then --removed 12/29/14 #20141024-00033
         IF NVL (p_price_zone, 0) IS NOT NULL
         THEN                                 --Added 12/29/14 #20141024-00033
            BEGIN
               UPDATE XXWC.XXWC_VENDOR_PRICE_ZONE_TBL xvpzt
                  SET price_zone = p_price_zone,
                      last_updated_by = fnd_global.user_id,
                      last_update_date = SYSDATE
                WHERE     organization_id = l_organization_id
                      AND vendor_id = l_vendor_id
                      AND vendor_site_id = l_vendor_site_id
                      --AND    price_zone != p_price_zone --Removed 12/29/14 #20141024-00033
                      AND NVL (price_zone, 0) != NVL (p_price_zone, 0) --Added 12/29/14 #20141024-00033
                                                                      ;
            EXCEPTION
               WHEN OTHERS
               THEN
                  l_message :=
                        'Could not update XXWC.XXWC_VENDOR_PRICE_ZONE_TBL '
                     || SQLCODE
                     || SQLERRM;
                  RAISE_APPLICATION_ERROR (-20001, l_message);
            END;
         ELSE
            DELETE XXWC.XXWC_VENDOR_PRICE_ZONE_TBL xvpzt --Modified by Manjula on 21-Nov-14 for TMS # 20141002-00041 Multi-Org
             WHERE     organization_id = l_organization_id
                   AND vendor_id = l_vendor_id
                   AND vendor_site_id = l_vendor_site_id;
         END IF;
      ELSE
         BEGIN
            INSERT INTO XXWC.XXWC_VENDOR_PRICE_ZONE_TBL (creation_date,
                                                         created_by,
                                                         last_update_date,
                                                         last_updated_by,
                                                         vendor_id,
                                                         vendor_site_id,
                                                         organization_id,
                                                         price_zone)
                 VALUES (SYSDATE,
                         fnd_global.user_id,
                         SYSDATE,
                         fnd_global.user_id,
                         l_vendor_id,
                         l_vendor_site_id,
                         l_organization_id,
                         p_price_zone);
         EXCEPTION
            WHEN OTHERS
            THEN
               l_message :=
                     'Could not update XXWC.XXWC_VENDOR_PRICE_ZONE_TBL '
                  || SQLCODE
                  || SQLERRM;
               RAISE_APPLICATION_ERROR (-20001, l_message);
         END;
      END IF;
   EXCEPTION
      WHEN OTHERS
      THEN
         g_err_msg :=
               'Error in '
            || g_err_callpoint
            || ' WITH  '
            || SUBSTR (SQLERRM, 1, 1000);
         --dbms_output.put_line(l_fulltext);

         xxcus_error_pkg.xxcus_error_main_api (
            p_called_from         => g_err_callfrom,
            p_calling             => g_err_callpoint,
            p_ora_error_msg       => SQLERRM,
            p_error_desc          =>    'Error in call '
                                     || g_err_callfrom
                                     || g_err_callpoint,
            p_distribution_list   => g_distro_list,
            p_module              => g_module);
   END;


   /*************************************************************************************************
   *   Function get_price_zone_orgs                                                                 *
   *   Purpose : Used in the BPA Price Zone Web ADI to display the orgs in a price zone             *
   *                                                                                                *
   *                                                                                                *
   *    FUNCTION  get_price_zone_orgs    (p_vendor_id IN NUMBER,                                    *
   *                                 p_vendor_site_id IN NUMBER,                                    *
   *                                 p_price_zone IN NUMBER)                                        *
   *                     RETURN VARCHAR2;                                                           *
   *                                                                                                *
   *   REVISIONS:                                                                                   *
   *   Ver        Date        Author                     Description                                *
   *   ---------  ----------  ---------------         -------------------------                     *
   *   1.0        02/08/2014   Lee Spitzer              Initial Versions TMS 20130917-00676         *
   *                                                         Vendor Cost Management Improvements    *
   *                                                                                                *
   /************************************************************************************************/



   FUNCTION get_price_zone_orgs (p_vendor_id        IN NUMBER,
                                 p_vendor_site_id   IN NUMBER,
                                 p_price_zone       IN NUMBER)
      RETURN VARCHAR2
   IS
      l_organization_code   VARCHAR2 (3);
      l_string              VARCHAR2 (2000);
      l_loop_count          NUMBER;

      CURSOR c_org
      IS
           SELECT mp.organization_code
             FROM mtl_parameters mp, XXWC.XXWC_VENDOR_PRICE_ZONE_TBL xvpz
            WHERE     xvpz.vendor_id = p_vendor_id
                  AND xvpz.vendor_site_id = p_vendor_site_id
                  AND xvpz.price_zone = p_price_zone
                  AND xvpz.organization_id = mp.organization_id
         ORDER BY mp.organization_code;
   BEGIN
      g_err_callfrom := 'get_price_zone_orgs';

      l_loop_count := 0;

      FOR r_org IN c_org
      LOOP
         IF l_loop_count = 0
         THEN
            l_string := r_org.organization_code;
         ELSE
            l_string := l_string || ', ' || r_org.organization_code;
         END IF;

         l_loop_count := l_loop_count + 1;
      END LOOP;

      RETURN l_string;
   EXCEPTION
      WHEN OTHERS
      THEN
         l_string := NULL;

         g_err_msg :=
               'Error in '
            || g_err_callpoint
            || ' WITH  '
            || SUBSTR (SQLERRM, 1, 1000);
         --dbms_output.put_line(l_fulltext);

         xxcus_error_pkg.xxcus_error_main_api (
            p_called_from         => g_err_callfrom,
            p_calling             => g_err_callpoint,
            p_ora_error_msg       => SQLERRM,
            p_error_desc          =>    'Error in call '
                                     || g_err_callfrom
                                     || g_err_callpoint,
            p_distribution_list   => g_distro_list,
            p_module              => g_module);
   END;


   /***************************************************************************************************
   *   Procedure process_bpa_lines                                                                    *
   *   Purpose : Used in the process_staging to process staging records on xxwc_bpa_pz_staging_tbl    *
   *         and load into the PO Interface Tables and process the interface records                  *
   *                                                                                                  *
   *                                                                                                  *
   *              PROCEDURE process_bpa_lines                                                         *
   *                               (p_po_number IN VARCHAR2                                           *
   *                               ,p_process_flag in NUMBER);                                        *
   *                                                                                                  *
   *   REVISIONS:                                                                                     *
   *   Ver        Date        Author                     Description                                  *
   *   ---------  ----------  ---------------         -------------------------                       *
   *   1.0        02/08/2014   Lee Spitzer              Initial Versions TMS 20130917-00676           *
   *                                                         Vendor Cost Management Improvements      *
   *   1.1        12/29/2014   Lee Spitzer              TMS Ticket 20141024-00033 Unable to expire    *
   *                                                         BPA lines with updated item description  *
   *                                                         and update Price Zone with null zone     *
   *   1.2        01/12/2015   Lee Spitzer              TMS Ticket 20140909-00032    Changed needed on *
   *                                                         XXWC BPA Price Zone form                 *
   /***************************************************************************************************/

   PROCEDURE process_bpa_lines (p_po_number      IN VARCHAR2,
                                p_process_flag   IN NUMBER)
   IS
      CURSOR c_bpa_lines
      IS
         SELECT xbpst.ROWID,
                xbpst.po_number,
                xbpst.item_number,
                xbpst.expiration_date
           FROM APPS.xxwc_bpa_pz_staging_tbl xbpst --Modified by Manjula on 21-Nov-14 for TMS # 20141002-00041 Multi-Org
          WHERE     xbpst.bpa_table_flag = p_process_flag
                AND xbpst.po_number = p_po_number
                AND NVL (xbpst.implement_date, SYSDATE - 1) <= SYSDATE
                AND xbpst.approve_flag = 2                          --Approved
                AND NOT EXISTS
                           (SELECT *
                              FROM po_headers pha, --Modified by Manjula on 21-Nov-14 for TMS # 20141002-00041 Multi-Org
                                   po_lines pla, --Modified by Manjula on 21-Nov-14 for TMS # 20141002-00041 Multi-Org
                                   mtl_system_items_b msib
                             WHERE     pha.segment1 = xbpst.po_number
                                   AND pha.po_header_id = pla.po_header_id
                                   AND pla.item_id = msib.inventory_item_id
                                   AND xbpst.item_number = msib.segment1
                                   AND msib.organization_id =
                                          FND_PROFILE.VALUE (
                                             'XXWC_ITEM_MASTER_ORG'))
         UNION
         SELECT xbpst.ROWID,
                xbpst.po_number,
                xbpst.item_number,
                xbpst.expiration_date
           FROM APPS.xxwc_bpa_pz_staging_tbl xbpst --Modified by Manjula on 21-Nov-14 for TMS # 20141002-00041 Multi-Org
          WHERE     xbpst.bpa_table_flag = p_process_flag
                AND xbpst.po_number = p_po_number
                AND NVL (xbpst.implement_date, SYSDATE - 1) <= SYSDATE
                AND NVL (approve_flag, 1) = 2
                AND xbpst.expiration_date IS NOT NULL
                AND EXISTS
                       (SELECT *
                          FROM po_headers pha, --Modified by Manjula on 21-Nov-14 for TMS # 20141002-00041 Multi-Org
                               po_lines pla, --Modified by Manjula on 21-Nov-14 for TMS # 20141002-00041 Multi-Org
                               mtl_system_items_b msib
                         WHERE     pha.segment1 = xbpst.po_number
                               AND pha.po_header_id = pla.po_header_id
                               AND pla.item_id = msib.inventory_item_id
                               AND xbpst.item_number = msib.segment1
                               AND msib.organization_id =
                                      FND_PROFILE.VALUE (
                                         'XXWC_ITEM_MASTER_ORG'))
         --Added TMS Ticket 20140909-00032 1/12/2015 to process unexpired lines
         UNION
         SELECT xbpst.ROWID,
                xbpst.po_number,
                xbpst.item_number,
                xbpst.expiration_date
           FROM APPS.xxwc_bpa_pz_staging_tbl xbpst --Modified by Manjula on 21-Nov-14 for TMS # 20141002-00041 Multi-Org
          WHERE     xbpst.bpa_table_flag = p_process_flag
                AND xbpst.po_number = p_po_number
                AND NVL (xbpst.implement_date, SYSDATE - 1) <= SYSDATE
                AND NVL (approve_flag, 1) = 2
                AND xbpst.expiration_date IS NULL
                AND EXISTS
                       (SELECT *
                          FROM po_headers pha, --Modified by Manjula on 21-Nov-14 for TMS # 20141002-00041 Multi-Org
                               po_lines pla, --Modified by Manjula on 21-Nov-14 for TMS # 20141002-00041 Multi-Org
                               mtl_system_items_b msib
                         WHERE     pha.segment1 = xbpst.po_number
                               AND pha.po_header_id = pla.po_header_id
                               AND pla.item_id = msib.inventory_item_id
                               AND xbpst.item_number = msib.segment1
                               AND pla.expiration_date IS NOT NULL
                               AND msib.organization_id =
                                      FND_PROFILE.VALUE (
                                         'XXWC_ITEM_MASTER_ORG'));


      l_exists                 NUMBER;
      l_inventory_item_id      NUMBER;
      l_po_header_id           NUMBER;
      v_process_code           VARCHAR2 (25);
      v_header_id              NUMBER;
      v_po_status              VARCHAR2 (240);
      v_po_line_num            NUMBER := NULL;
      l_po_line_num            NUMBER := NULL;
      v_need_by_date           DATE;
      v_uom                    VARCHAR2 (25);
      v_vendor_prod_num        VARCHAR2 (25);
      l_description            VARCHAR2 (240);
      l_unit_price             NUMBER := 0;
      l_uom_code               VARCHAR2 (25);
      l_return_status          VARCHAR2 (1);
      l_count                  NUMBER;
      l_expiration_count       NUMBER;
      l_bpa_item_description   VARCHAR2 (240); --Added 12/29/2014 TMS Ticket 20141024-00033
      l_unexpiration_count     NUMBER; --Added 01/12/2015 TMS Ticket 20140909-00032
      l_po_line_id             NUMBER; --Added 01/12/2015 TMS Ticket 20140909-00032
   BEGIN
      g_err_callfrom := 'process_bpa_lines';

      --Find if there are any lines to add to the BPA if items don't exist
      BEGIN
         SELECT COUNT (*)
           INTO l_count
           FROM APPS.xxwc_bpa_pz_staging_tbl xbpst --Modified by Manjula on 21-Nov-14 for TMS # 20141002-00041 Multi-Org
          WHERE     xbpst.bpa_table_flag = p_process_flag
                AND xbpst.po_number = p_po_number
                AND NVL (xbpst.implement_date, SYSDATE - 1) <= SYSDATE
                AND NVL (approve_flag, 1) = 2
                AND NOT EXISTS
                           (SELECT *
                              FROM po_headers pha, --Modified by Manjula on 21-Nov-14 for TMS # 20141002-00041 Multi-Org
                                   po_lines pla, --Modified by Manjula on 21-Nov-14 for TMS # 20141002-00041 Multi-Org
                                   mtl_system_items_b msib
                             WHERE     pha.segment1 = xbpst.po_number
                                   AND pha.po_header_id = pla.po_header_id
                                   AND pla.item_id = msib.inventory_item_id
                                   AND xbpst.item_number = msib.segment1
                                   AND msib.organization_id =
                                          FND_PROFILE.VALUE (
                                             'XXWC_ITEM_MASTER_ORG'));
      EXCEPTION
         WHEN OTHERS
         THEN
            l_count := 0;
      END;


      --Find if there are any lines to update on the BPA if item has an expiration date
      BEGIN
         SELECT COUNT (*)
           INTO l_expiration_count
           FROM APPS.xxwc_bpa_pz_staging_tbl xbpst --Modified by Manjula on 21-Nov-14 for TMS # 20141002-00041 Multi-Org
          WHERE     xbpst.bpa_table_flag = p_process_flag
                AND xbpst.po_number = p_po_number
                AND NVL (xbpst.implement_date, SYSDATE - 1) <= SYSDATE
                AND NVL (approve_flag, 1) = 2
                AND xbpst.expiration_date IS NOT NULL
                AND EXISTS
                       (SELECT *
                          FROM po_headers pha, --Modified by Manjula on 21-Nov-14 for TMS # 20141002-00041 Multi-Org
                               po_lines pla, --Modified by Manjula on 21-Nov-14 for TMS # 20141002-00041 Multi-Org
                               mtl_system_items_b msib
                         WHERE     pha.segment1 = xbpst.po_number
                               AND pha.po_header_id = pla.po_header_id
                               AND pla.item_id = msib.inventory_item_id
                               AND xbpst.item_number = msib.segment1
                               AND msib.organization_id =
                                      FND_PROFILE.VALUE (
                                         'XXWC_ITEM_MASTER_ORG'));
      EXCEPTION
         WHEN OTHERS
         THEN
            l_count := 0;
      END;

      --Added 01/12/2015 TMS Ticket 20140909-00032
      --Find if there are any lines to update on the BPA if item has an expiration date
      BEGIN
         SELECT COUNT (*)
           INTO l_unexpiration_count
           FROM APPS.xxwc_bpa_pz_staging_tbl xbpst --Modified by Manjula on 21-Nov-14 for TMS # 20141002-00041 Multi-Org
          WHERE     xbpst.bpa_table_flag = p_process_flag
                AND xbpst.po_number = p_po_number
                AND NVL (xbpst.implement_date, SYSDATE - 1) <= SYSDATE
                AND NVL (approve_flag, 1) = 2
                AND xbpst.expiration_date IS NULL
                AND EXISTS
                       (SELECT *
                          FROM po_headers pha, --Modified by Manjula on 21-Nov-14 for TMS # 20141002-00041 Multi-Org
                               po_lines pla, --Modified by Manjula on 21-Nov-14 for TMS # 20141002-00041 Multi-Org
                               mtl_system_items_b msib
                         WHERE     pha.segment1 = xbpst.po_number
                               AND pha.po_header_id = pla.po_header_id
                               AND pla.item_id = msib.inventory_item_id
                               AND xbpst.item_number = msib.segment1
                               AND pla.expiration_date IS NOT NULL
                               AND msib.organization_id =
                                      FND_PROFILE.VALUE (
                                         'XXWC_ITEM_MASTER_ORG'));
      EXCEPTION
         WHEN OTHERS
         THEN
            l_count := 0;
      END;


      --if l_count > 0 or l_expiration_count > 0 then --Removed 01/12/2015 TMS Ticket 20140909-00032
      IF l_count > 0 OR l_expiration_count > 0 OR l_unexpiration_count > 0
      THEN                        --Added 01/12/2015 TMS Ticket 20140909-00032
         BEGIN
            SELECT po_header_id
              INTO l_po_header_id
              FROM po_headers_all
             WHERE segment1 = p_po_number;
         EXCEPTION
            WHEN OTHERS
            THEN
               l_po_header_id := NULL;
         END;


         IF l_po_header_id IS NOT NULL
         THEN
            SELECT po.po_headers_interface_s.NEXTVAL   -- get unique header_id
              INTO v_header_id
              FROM DUAL;


            -- Insert Header Record
            INSERT INTO po.po_headers_interface (interface_header_id,
                                                 batch_id,
                                                 process_code,
                                                 action,
                                                 org_id,
                                                 document_type_code,
                                                 document_num,
                                                 vendor_id,
                                                 vendor_site_id,
                                                 vendor_doc_num,
                                                 agent_id,
                                                 ship_to_location_id,
                                                 effective_date,
                                                 global_agreement_flag)
               SELECT v_header_id,
                      v_header_id,
                      'PENDING',
                      'UPDATE',
                      org_id,
                      type_lookup_code,
                      segment1,
                      vendor_id,
                      vendor_site_id,
                      NULL,
                      agent_id,
                      ship_to_location_id,
                      SYSDATE,
                      'Y'
                 FROM po_headers_all
                WHERE po_header_id = l_po_header_id;


            FOR r_bpa_lines IN c_bpa_lines
            LOOP
               BEGIN
                  SELECT inventory_item_id,
                         description,
                         primary_unit_of_measure
                    INTO l_inventory_item_id, l_description, l_uom_code
                    FROM mtl_system_items_b
                   WHERE     segment1 = r_bpa_lines.item_number
                         AND organization_id =
                                FND_PROFILE.VALUE ('XXWC_ITEM_MASTER_ORG');
               EXCEPTION
                  WHEN OTHERS
                  THEN
                     l_inventory_item_id := NULL;
                     l_description := NULL;
                     l_uom_code := NULL;
               END;



               --if l_expiration_count > 0 THEN --Removed 01/12/2015 TMS Ticket 20140909-00032
               IF l_expiration_count > 0 OR l_unexpiration_count > 0
               THEN               --Added 01/12/2015 TMS Ticket 20140909-00032
                  BEGIN
                     SELECT line_num -- removed 12/29/2014 TMS Ticket #20141024-00033
                                    , po_line_id --added 01/12/2015 TMS Ticket 20140909-00032
                       INTO v_po_line_num -- removed 12/29/2014 TMS Ticket #20141024-00033
                                         , l_po_line_id --Added 01/12/2015 TMS Ticket 20140909-00032
                       --SELECT line_num, item_description --added 12/29/2014 TMS Ticket #20141024-00033
                       --INTO   v_po_line_num, l_bpa_item_description -- added 12/29/2014 TMS Ticket #20141024-00033
                       FROM po_lines_all
                      WHERE     po_header_id = l_po_header_id
                            AND item_id = l_inventory_item_id
                            AND ROWNUM = 1;    --get the first record returned
                  EXCEPTION
                     WHEN OTHERS
                     THEN
                        v_po_line_num := NULL;
                  --l_bpa_item_description := NULL; -- added 12/29/2014 TMS Ticket #20141024-00033
                  END;

                  l_description := NULL; --added 12/29/2014 TMS Ticket #20141024-00033 reset description to null
               ELSE
                  v_po_line_num := NULL;
               --l_bpa_item_description := NULL;

               END IF;

               --Added 01/12/2015 TMS Ticket 20140909-00032
               --If we don't update the PO Line directly, when we unexpire a line, the interface will add a new line to the existing BPA which we don't want
               IF l_unexpiration_count > 0
               THEN
                  --Need to update the original line for the expiration line
                  BEGIN
                     UPDATE po_lines
                        SET expiration_date = NULL
                      WHERE po_line_id = l_po_line_id;
                  EXCEPTION
                     WHEN OTHERS
                     THEN
                        RAISE;
                  END;
               END IF;

               -- Adding Lines
               INSERT
                 INTO po.po_lines_interface (interface_line_id,
                                             interface_header_id,
                                             process_code,
                                             action,
                                             line_num,
                                             shipment_num,
                                             line_type,
                                             item            --added 7/22/2014
                                                 ,
                                             item_id,
                                             item_description,
                                             unit_of_measure,
                                             quantity,
                                             unit_price,
                                             promised_date,
                                             need_by_date,
                                             vendor_product_num -- 07/16/2013 CG: TMS 20130711-00842: Added to correct issue with Consigned Items not being added correctly to PO
                                                               ,
                                             consigned_flag,
                                             NEGOTIATED_BY_PREPARER_FLAG,
                                             ALLOW_PRICE_OVERRIDE_FLAG,
                                             NOT_TO_EXCEED_PRICE,
                                             effective_date,
                                             expiration_date)
               VALUES (po.po_lines_interface_s.NEXTVAL,
                       v_header_id,
                       'PENDING',
                       'UPDATE',
                       v_po_line_num,
                       NULL,
                       'Goods',
                       r_bpa_lines.item_number          --Item added 7/22/2014
                                              ,
                       L_INVENTORY_ITEM_ID,
                       L_DESCRIPTION,
                       l_uom_code,
                       NULL -- 04/25/2013 TMS 20130424-01825; CG Changed to calculate with BPA Cost first the org list price (SCWB contains that logic)
                                             -- , :VENDOR_MIN_ITEMS.LIST_PRICE
                       ,
                       l_unit_price,
                       NULL,
                       NULL,
                       NULL -- 07/16/2013 CG: TMS 20130711-00842: Added to correct issue with Consigned Items not being added correctly to PO
                           ,
                       NULL,
                       'N',
                       'Y',
                       NULL,
                       SYSDATE,
                       r_bpa_lines.expiration_date);



               UPDATE APPS.xxwc_bpa_pz_staging_tbl --Modified by Manjula on 21-Nov-14 for TMS # 20141002-00041 Multi-Org
                  SET bpa_table_flag = 2
                WHERE     ROWID = r_bpa_lines.ROWID
                      AND NVL (implement_date, SYSDATE - 1) <= SYSDATE
                      AND NVL (approve_flag, 1) = 2;

               COMMIT;
            END LOOP;

            PO_PDOI_GRP.start_process (
               p_api_version                  => 1.0,
               p_init_msg_list                => 'T',
               p_validation_level             => 100,
               p_commit                       => 'T',
               x_return_status                => l_return_status,
               p_gather_intf_tbl_stat         => 'F',
               p_calling_module               => 'UNKNOWN', --PO_PDOI_CONSTANTS.g_CALL_MOD_CONCURRENT_PRGM,
               p_selected_batch_id            => v_header_id,
               p_batch_size                   => 5000, --PO_PDOI_CONSTANTS.g_DEF_BATCH_SIZE,
               p_buyer_id                     => NULL,
               p_document_type                => 'BLANKET',
               p_document_subtype             => NULL,
               p_create_items                 => 'N',
               p_create_sourcing_rules_flag   => 'N',
               p_rel_gen_method               => NULL,
               p_sourcing_level               => NULL,
               p_sourcing_inv_org_id          => NULL,
               p_approved_status              => 'APPROVED',
               p_process_code                 => 'PENDING', --PO_PDOI_CONSTANTS.g_process_code_PENDING,
               p_interface_header_id          => NULL,
               p_org_id                       => 162,
               p_ga_flag                      => NULL);

            COMMIT;
         END IF;
      ELSE
         UPDATE APPS.xxwc_bpa_pz_staging_tbl --Modified by Manjula on 21-Nov-14 for TMS # 20141002-00041 Multi-Org
            SET bpa_table_flag = 2
          WHERE     po_number = p_po_number
                AND NVL (implement_date, SYSDATE - 1) <= SYSDATE
                AND NVL (approve_flag, 1) = 2;

         COMMIT;
      END IF;
   EXCEPTION
      WHEN OTHERS
      THEN
         g_err_msg :=
               'Error in '
            || g_err_callpoint
            || ' WITH  '
            || SUBSTR (SQLERRM, 1, 1000);
         --dbms_output.put_line(l_fulltext);

         xxcus_error_pkg.xxcus_error_main_api (
            p_called_from         => g_err_callfrom,
            p_calling             => g_err_callpoint,
            p_ora_error_msg       => SQLERRM,
            p_error_desc          =>    'Error in call '
                                     || g_err_callfrom
                                     || g_err_callpoint,
            p_distribution_list   => g_distro_list,
            p_module              => g_module);
   END process_bpa_lines;


   /*************************************************************************************************
   *   Procedure process_price_zone_lines                                                           *
   *   Purpose : Used in the process_staging to process staging records on xxwc_bpa_pz_staging_tbl  *
   *         and load into the XXWC Price Zone Tables and XXWC Price Quantity Breaks Tables         *
   *                                                                                                *
   *                                                                                                *
   *              PROCEDURE process_price_zone_lines                                                *
   *                               (p_po_number IN VARCHAR2                                         *
   *                               ,p_process_flag in NUMBER);                                      *
   *                                                                                                *
   *   REVISIONS:                                                                                   *
   *   Ver        Date        Author                     Description                                *
   *   ---------  ----------  ---------------         -------------------------                     *
   *   1.0        02/08/2014   Lee Spitzer              Initial Versions TMS 20130917-00676         *
   *                                                         Vendor Cost Management Improvements    *
   *   1.1        02/20/2015   Lee Spitzer              TMS Ticket 20150225-00021 - Fix issue with  *
   *                                                         incorrect price zone reference for zone*
   *                                                         21                                     *
   *   1.3        04/15/2015   P.vamshidhar             TMS #  20150414-00173  2015 PLM - Implement *
   *                                                    Cost Management tool enhancements           *                                                                                               *
   *                                                                                                *
   *                                                                                                *
   /************************************************************************************************/


   PROCEDURE process_price_zone_lines (p_po_number      IN VARCHAR2,
                                       p_process_flag   IN NUMBER)
   IS
      CURSOR c_zone (p_row_id IN VARCHAR2)
      IS
         SELECT 0 price_zone,
                national_price price_zone_price,
                national_quantity price_zone_quantity,
                national_break price_zone_quantity_price
           FROM APPS.XXWC_BPA_PZ_STAGING_TBL --Modified by Manjula on 21-Nov-14 for TMS # 20141002-00041 Multi-Org
          WHERE ROWID = p_row_id
         UNION
         SELECT 1 price_zone,
                PRICE_ZONE_PRICE1 price_zone_price,
                price_zone_quantity1 price_zone_quantity,
                price_zone_quantity_price1 price_zone_quantity_price
           FROM APPS.XXWC_BPA_PZ_STAGING_TBL --Modified by Manjula on 21-Nov-14 for TMS # 20141002-00041 Multi-Org
          WHERE ROWID = p_row_id
         UNION
         SELECT 2 price_zone,
                PRICE_ZONE_PRICE2 price_zone_price,
                price_zone_quantity2 price_zone_quantity,
                price_zone_quantity_price2 price_zone_quantity_price
           FROM APPS.XXWC_BPA_PZ_STAGING_TBL --Modified by Manjula on 21-Nov-14 for TMS # 20141002-00041 Multi-Org
          WHERE ROWID = p_row_id
         UNION
         SELECT 3 price_zone,
                PRICE_ZONE_PRICE3 price_zone_price,
                price_zone_quantity3 price_zone_quantity,
                price_zone_quantity_price3 price_zone_quantity_price
           FROM APPS.XXWC_BPA_PZ_STAGING_TBL --Modified by Manjula on 21-Nov-14 for TMS # 20141002-00041 Multi-Org
          WHERE ROWID = p_row_id
         UNION
         SELECT 4 price_zone,
                PRICE_ZONE_PRICE4 price_zone_price,
                price_zone_quantity4 price_zone_quantity,
                price_zone_quantity_price4 price_zone_quantity_price
           FROM APPS.XXWC_BPA_PZ_STAGING_TBL --Modified by Manjula on 21-Nov-14 for TMS # 20141002-00041 Multi-Org
          WHERE ROWID = p_row_id
         UNION
         SELECT 5 price_zone,
                PRICE_ZONE_PRICE5 price_zone_price,
                price_zone_quantity5 price_zone_quantity,
                price_zone_quantity_price5 price_zone_quantity_price
           FROM APPS.XXWC_BPA_PZ_STAGING_TBL --Modified by Manjula on 21-Nov-14 for TMS # 20141002-00041 Multi-Org
          WHERE ROWID = p_row_id
         UNION
         SELECT 6 price_zone,
                PRICE_ZONE_PRICE6 price_zone_price,
                price_zone_quantity6 price_zone_quantity,
                price_zone_quantity_price6 price_zone_quantity_price
           FROM APPS.XXWC_BPA_PZ_STAGING_TBL --Modified by Manjula on 21-Nov-14 for TMS # 20141002-00041 Multi-Org
          WHERE ROWID = p_row_id
         UNION
         SELECT 7 price_zone,
                PRICE_ZONE_PRICE7 price_zone_price,
                price_zone_quantity7 price_zone_quantity,
                price_zone_quantity_price7 price_zone_quantity_price
           FROM APPS.XXWC_BPA_PZ_STAGING_TBL --Modified by Manjula on 21-Nov-14 for TMS # 20141002-00041 Multi-Org
          WHERE ROWID = p_row_id
         UNION
         SELECT 8 price_zone,
                PRICE_ZONE_PRICE8 price_zone_price,
                price_zone_quantity8 price_zone_quantity,
                price_zone_quantity_price8 price_zone_quantity_price
           FROM APPS.XXWC_BPA_PZ_STAGING_TBL --Modified by Manjula on 21-Nov-14 for TMS # 20141002-00041 Multi-Org
          WHERE ROWID = p_row_id
         UNION
         SELECT 9 price_zone,
                PRICE_ZONE_PRICE9 price_zone_price,
                price_zone_quantity9 price_zone_quantity,
                price_zone_quantity_price9 price_zone_quantity_price
           FROM APPS.XXWC_BPA_PZ_STAGING_TBL --Modified by Manjula on 21-Nov-14 for TMS # 20141002-00041 Multi-Org
          WHERE ROWID = p_row_id
         UNION
         SELECT 10 price_zone,
                PRICE_ZONE_PRICE10 price_zone_price,
                price_zone_quantity10 price_zone_quantity,
                price_zone_quantity_price10 price_zone_quantity_price
           FROM APPS.XXWC_BPA_PZ_STAGING_TBL --Modified by Manjula on 21-Nov-14 for TMS # 20141002-00041 Multi-Org
          WHERE ROWID = p_row_id
         UNION
         SELECT 11 price_zone,
                PRICE_ZONE_PRICE11 price_zone_price,
                price_zone_quantity11 price_zone_quantity,
                price_zone_quantity_price11 price_zone_quantity_price
           FROM APPS.XXWC_BPA_PZ_STAGING_TBL --Modified by Manjula on 21-Nov-14 for TMS # 20141002-00041 Multi-Org
          WHERE ROWID = p_row_id
         UNION
         SELECT 12 price_zone,
                PRICE_ZONE_PRICE12 price_zone_price,
                price_zone_quantity12 price_zone_quantity,
                price_zone_quantity_price12 price_zone_quantity_price
           FROM APPS.XXWC_BPA_PZ_STAGING_TBL --Modified by Manjula on 21-Nov-14 for TMS # 20141002-00041 Multi-Org
          WHERE ROWID = p_row_id
         UNION
         SELECT 13 price_zone,
                PRICE_ZONE_PRICE13 price_zone_price,
                price_zone_quantity13 price_zone_quantity,
                price_zone_quantity_price13 price_zone_quantity_price
           FROM APPS.XXWC_BPA_PZ_STAGING_TBL --Modified by Manjula on 21-Nov-14 for TMS # 20141002-00041 Multi-Org
          WHERE ROWID = p_row_id
         UNION
         SELECT 14 price_zone,
                PRICE_ZONE_PRICE14 price_zone_price,
                price_zone_quantity14 price_zone_quantity,
                price_zone_quantity_price14 price_zone_quantity_price
           FROM APPS.XXWC_BPA_PZ_STAGING_TBL --Modified by Manjula on 21-Nov-14 for TMS # 20141002-00041 Multi-Org
          WHERE ROWID = p_row_id
         UNION
         SELECT 15 price_zone,
                PRICE_ZONE_PRICE15 price_zone_price,
                price_zone_quantity15 price_zone_quantity,
                price_zone_quantity_price15 price_zone_quantity_price
           FROM APPS.XXWC_BPA_PZ_STAGING_TBL --Modified by Manjula on 21-Nov-14 for TMS # 20141002-00041 Multi-Org
          WHERE ROWID = p_row_id
         UNION
         SELECT 16 price_zone,
                PRICE_ZONE_PRICE16 price_zone_price,
                price_zone_quantity16 price_zone_quantity,
                price_zone_quantity_price16 price_zone_quantity_price
           FROM APPS.XXWC_BPA_PZ_STAGING_TBL --Modified by Manjula on 21-Nov-14 for TMS # 20141002-00041 Multi-Org
          WHERE ROWID = p_row_id
         UNION
         SELECT 17 price_zone,
                PRICE_ZONE_PRICE17 price_zone_price,
                price_zone_quantity17 price_zone_quantity,
                price_zone_quantity_price17 price_zone_quantity_price
           FROM APPS.XXWC_BPA_PZ_STAGING_TBL --Modified by Manjula on 21-Nov-14 for TMS # 20141002-00041 Multi-Org
          WHERE ROWID = p_row_id
         UNION
         SELECT 18 price_zone,
                PRICE_ZONE_PRICE18 price_zone_price,
                price_zone_quantity18 price_zone_quantity,
                price_zone_quantity_price18 price_zone_quantity_price
           FROM APPS.XXWC_BPA_PZ_STAGING_TBL --Modified by Manjula on 21-Nov-14 for TMS # 20141002-00041 Multi-Org
          WHERE ROWID = p_row_id
         UNION
         SELECT 19 price_zone,
                PRICE_ZONE_PRICE19 price_zone_price,
                price_zone_quantity19 price_zone_quantity,
                price_zone_quantity_price19 price_zone_quantity_price
           FROM APPS.XXWC_BPA_PZ_STAGING_TBL --Modified by Manjula on 21-Nov-14 for TMS # 20141002-00041 Multi-Org
          WHERE ROWID = p_row_id
         UNION
         SELECT 20 price_zone,
                PRICE_ZONE_PRICE20 price_zone_price,
                price_zone_quantity20 price_zone_quantity,
                price_zone_quantity_price20 price_zone_quantity_price
           FROM APPS.XXWC_BPA_PZ_STAGING_TBL --Modified by Manjula on 21-Nov-14 for TMS # 20141002-00041 Multi-Org
          WHERE ROWID = p_row_id
         UNION
         SELECT 21 price_zone,
                PRICE_ZONE_PRICE21 price_zone_price,
                price_zone_quantity21 price_zone_quantity,
                --price_zone_quantity_price11 price_zone_quantity_price - Removed 2/20/2015 TMS Ticket 20150225-00021 due to incorrect reference to price zone 11
                price_zone_quantity_price21 price_zone_quantity_price --added 2/20/2015 TMS Ticket 20150225-00021 to correct reference of price zone 21
           FROM APPS.XXWC_BPA_PZ_STAGING_TBL --Modified by Manjula on 21-Nov-14 for TMS # 20141002-00041 Multi-Org
          WHERE ROWID = p_row_id
         UNION
         SELECT 22 price_zone,
                PRICE_ZONE_PRICE22 price_zone_price,
                price_zone_quantity22 price_zone_quantity,
                price_zone_quantity_price22 price_zone_quantity_price
           FROM APPS.XXWC_BPA_PZ_STAGING_TBL --Modified by Manjula on 21-Nov-14 for TMS # 20141002-00041 Multi-Org
          WHERE ROWID = p_row_id
         UNION
         SELECT 23 price_zone,
                PRICE_ZONE_PRICE23 price_zone_price,
                price_zone_quantity23 price_zone_quantity,
                price_zone_quantity_price23 price_zone_quantity_price
           FROM APPS.XXWC_BPA_PZ_STAGING_TBL --Modified by Manjula on 21-Nov-14 for TMS # 20141002-00041 Multi-Org
          WHERE ROWID = p_row_id
         UNION
         SELECT 24 price_zone,
                PRICE_ZONE_PRICE24 price_zone_price,
                price_zone_quantity24 price_zone_quantity,
                price_zone_quantity_price24 price_zone_quantity_price
           FROM APPS.XXWC_BPA_PZ_STAGING_TBL --Modified by Manjula on 21-Nov-14 for TMS # 20141002-00041 Multi-Org
          WHERE ROWID = p_row_id
         UNION
         SELECT 25 price_zone,
                PRICE_ZONE_PRICE25 price_zone_price,
                price_zone_quantity25 price_zone_quantity,
                price_zone_quantity_price25 price_zone_quantity_price
           FROM APPS.XXWC_BPA_PZ_STAGING_TBL --Modified by Manjula on 21-Nov-14 for TMS # 20141002-00041 Multi-Org
          WHERE ROWID = p_row_id
         UNION
         SELECT 26 price_zone,
                PRICE_ZONE_PRICE26 price_zone_price,
                price_zone_quantity26 price_zone_quantity,
                price_zone_quantity_price26 price_zone_quantity_price
           FROM APPS.XXWC_BPA_PZ_STAGING_TBL --Modified by Manjula on 21-Nov-14 for TMS # 20141002-00041 Multi-Org
          WHERE ROWID = p_row_id
         UNION
         SELECT 27 price_zone,
                PRICE_ZONE_PRICE27 price_zone_price,
                price_zone_quantity27 price_zone_quantity,
                price_zone_quantity_price27 price_zone_quantity_price
           FROM APPS.XXWC_BPA_PZ_STAGING_TBL --Modified by Manjula on 21-Nov-14 for TMS # 20141002-00041 Multi-Org
          WHERE ROWID = p_row_id
         UNION
         SELECT 28 price_zone,
                PRICE_ZONE_PRICE28 price_zone_price,
                price_zone_quantity28 price_zone_quantity,
                price_zone_quantity_price28 price_zone_quantity_price
           FROM APPS.XXWC_BPA_PZ_STAGING_TBL --Modified by Manjula on 21-Nov-14 for TMS # 20141002-00041 Multi-Org
          WHERE ROWID = p_row_id
         UNION
         SELECT 29 price_zone,
                PRICE_ZONE_PRICE29 price_zone_price,
                price_zone_quantity29 price_zone_quantity,
                price_zone_quantity_price29 price_zone_quantity_price
           FROM APPS.XXWC_BPA_PZ_STAGING_TBL --Modified by Manjula on 21-Nov-14 for TMS # 20141002-00041 Multi-Org
          WHERE ROWID = p_row_id
         UNION
         SELECT 30 price_zone,
                PRICE_ZONE_PRICE30 price_zone_price,
                price_zone_quantity30 price_zone_quantity,
                price_zone_quantity_price30 price_zone_quantity_price
           FROM APPS.XXWC_BPA_PZ_STAGING_TBL --Modified by Manjula on 21-Nov-14 for TMS # 20141002-00041 Multi-Org
          WHERE ROWID = p_row_id;


      CURSOR c_bpa_lines
      IS
         SELECT xbpst.ROWID, xbpst.po_number, xbpst.item_number
           FROM apps.xxwc_bpa_pz_staging_tbl xbpst --Modified by Manjula on 21-Nov-14 for TMS # 20141002-00041 Multi-Org
          WHERE     xbpst.price_zone_table_flag = p_process_flag
                AND xbpst.po_number = p_po_number
                AND NVL (xbpst.implement_date, SYSDATE - 1) <= SYSDATE
                AND xbpst.approve_flag = 2;                        --Approved;


      l_po_header_id        NUMBER;
      l_inventory_item_id   NUMBER;
      l_exists              NUMBER;
   BEGIN
      g_err_callfrom := 'process_price_zone_lines';

      BEGIN
         SELECT po_header_id
           INTO l_po_header_id
           FROM po_headers_all
          WHERE segment1 = p_po_number;
      EXCEPTION
         WHEN OTHERS
         THEN
            l_po_header_id := NULL;
      END;


      FOR r_bpa_lines IN c_bpa_lines
      LOOP
         BEGIN
            SELECT inventory_item_id
              INTO l_inventory_item_id
              FROM mtl_system_items_b
             WHERE     segment1 = r_bpa_lines.item_number
                   AND organization_id =
                          FND_PROFILE.VALUE ('XXWC_ITEM_MASTER_ORG');
         EXCEPTION
            WHEN OTHERS
            THEN
               l_inventory_item_id := NULL;
         END;


         FOR r_zone IN c_zone (r_bpa_lines.ROWID)
         LOOP
            BEGIN
               SELECT COUNT (*)
                 INTO l_exists
                 FROM APPS.XXWC_BPA_PRICE_ZONE_TBL --Modified by Manjula on 21-Nov-14 for TMS # 20141002-00041 Multi-Org
                WHERE     po_header_id = l_po_header_id
                      AND inventory_item_id = l_inventory_item_id
                      AND price_zone = r_zone.price_zone;
            EXCEPTION
               WHEN OTHERS
               THEN
                  l_exists := 0;
            END;

            IF l_exists = 0
            THEN
               BEGIN
                  INSERT
                    INTO APPS.XXWC_BPA_PRICE_ZONE_TBL --Modified by Manjula on 21-Nov-14 for TMS # 20141002-00041 Multi-Org
                                                      (creation_date,
                                                       created_by,
                                                       last_update_date,
                                                       last_updated_by,
                                                       po_header_id,
                                                       inventory_item_id,
                                                       price_zone,
                                                       price_zone_price)
                  --,price_zone_quantity
                  --,price_zone_quantity_price)
                  VALUES (SYSDATE,
                          fnd_global.user_id,
                          SYSDATE,
                          fnd_global.user_id,
                          l_po_header_id,
                          l_inventory_item_id,
                          r_zone.price_zone,
                          r_zone.price_zone_price);
               --,r_zone.price_zone_quantity
               --,r_zone.price_zone_quantity_price);
               EXCEPTION
                  WHEN OTHERS
                  THEN
                     NULL;
               /*l_message := 'Could not insert into XXWC_BPA_PRICE_ZONE_TBL '|| SQLCODE || SQLERRM;
               RAISE_APPLICATION_ERROR(-20001,l_message);
               */
               END;
            ELSE
               BEGIN
                  IF r_zone.price_zone_price IS NOT NULL
                  THEN
                     UPDATE APPS.XXWC_BPA_PRICE_ZONE_TBL --Modified by Manjula on 21-Nov-14 for TMS # 20141002-00041 Multi-Org
                        SET price_zone_price = r_zone.price_zone_price -- Commented below code by Vamshi in 1.3V TMS #  20150414-00173 and added above code price_zone_price = r_zone.price_zone_price
 /*   price_zone_price =   case when
                           r_zone.price_zone_price is null then
                           price_zone_price else
                              r_zone.price_zone_price end*/
                           --,price_zone_quantity = r_zone.price_zone_quantity
               --,price_zone_quantity_price = r_zone.price_zone_quantity_price
                            ,
                            last_updated_by = fnd_global.user_id,
                            last_update_date = SYSDATE
                      WHERE     po_header_id = l_po_header_id
                            AND inventory_item_id = l_inventory_item_id
                            AND price_zone = r_zone.price_zone;
                  -- added else clasuse by Vamshi in 1.3V TMS #  20150414-00173
                  ELSE
                     DELETE FROM APPS.XXWC_BPA_PRICE_ZONE_TBL
                           WHERE     po_header_id = l_po_header_id
                                 AND inventory_item_id = l_inventory_item_id
                                 AND price_zone = r_zone.price_zone;
                  END IF;
               EXCEPTION
                  WHEN OTHERS
                  THEN
                     NULL;
               /*l_message := 'Could not update XXWC_BPA_PRICE_ZONE_TBL '|| SQLCODE || SQLERRM;
               RAISE_APPLICATION_ERROR(-20001,l_message);
               */
               END;
            END IF;


            --Checking for Price Breaks

            BEGIN
               SELECT COUNT (*)
                 INTO l_exists
                 FROM APPS.XXWC_BPA_PRICE_ZONE_BREAKS_TBL --Modified by Manjula on 21-Nov-14 for TMS # 20141002-00041 Multi-Org
                WHERE     po_header_id = l_po_header_id
                      AND inventory_item_id = l_inventory_item_id
                      AND price_zone = r_zone.price_zone
                      AND price_zone_quantity = r_zone.price_zone_quantity;
            EXCEPTION
               WHEN OTHERS
               THEN
                  l_exists := 0;
            END;

            IF l_exists = 0 AND r_zone.price_zone_quantity_price IS NOT NULL
            THEN
               BEGIN
                  INSERT
                    INTO APPS.XXWC_BPA_PRICE_ZONE_BREAKS_TBL --Modified by Manjula on 21-Nov-14 for TMS # 20141002-00041 Multi-Org
                                                             (
                            creation_date,
                            created_by,
                            last_update_date,
                            last_updated_by,
                            po_header_id,
                            inventory_item_id,
                            price_zone,
                            price_zone_quantity,
                            price_zone_quantity_price)
                  VALUES (SYSDATE,
                          fnd_global.user_id,
                          SYSDATE,
                          fnd_global.user_id,
                          l_po_header_id,
                          l_inventory_item_id,
                          r_zone.price_zone,
                          r_zone.price_zone_quantity,
                          r_zone.price_zone_quantity_price);
               EXCEPTION
                  WHEN OTHERS
                  THEN
                     NULL;
               /*l_message := 'Could not insert into XXWC_BPA_PRICE_ZONE_TBL '|| SQLCODE || SQLERRM;
               RAISE_APPLICATION_ERROR(-20001,l_message);
               */
               END;
            ELSIF l_exists > 0 AND r_zone.price_zone_quantity_price IS NULL
            THEN
               DELETE APPS.XXWC_BPA_PRICE_ZONE_BREAKS_TBL --Modified by Manjula on 21-Nov-14 for TMS # 20141002-00041 Multi-Org
                WHERE     po_header_id = l_po_header_id
                      AND inventory_item_id = l_inventory_item_id
                      AND price_zone = r_zone.price_zone
                      AND price_zone_quantity = r_zone.price_zone_quantity
                      AND r_zone.price_zone_quantity_price IS NULL;
            ELSIF     l_exists > 0
                  AND r_zone.price_zone_quantity_price IS NOT NULL
            THEN
               BEGIN
                  UPDATE APPS.XXWC_BPA_PRICE_ZONE_BREAKS_TBL --Modified by Manjula on 21-Nov-14 for TMS # 20141002-00041 Multi-Org
                     SET price_zone_quantity = r_zone.price_zone_quantity,
                         price_zone_quantity_price =
                            r_zone.price_zone_quantity_price,
                         last_updated_by = fnd_global.user_id,
                         last_update_date = SYSDATE
                   WHERE     po_header_id = l_po_header_id
                         AND inventory_item_id = l_inventory_item_id
                         AND price_zone = r_zone.price_zone
                         AND price_zone_quantity = r_zone.price_zone_quantity
                         AND r_zone.price_zone_quantity_price IS NOT NULL;
               EXCEPTION
                  WHEN OTHERS
                  THEN
                     NULL;
               /*l_message := 'Could not update XXWC_BPA_PRICE_ZONE_TBL '|| SQLCODE || SQLERRM;
               RAISE_APPLICATION_ERROR(-20001,l_message);
               */
               END;
            END IF;
         END LOOP;


         UPDATE apps.xxwc_bpa_pz_staging_tbl --Modified by Manjula on 21-Nov-14 for TMS # 20141002-00041 Multi-Org
            SET price_zone_table_flag = 2,
                last_update_date = SYSDATE,
                last_updated_by = fnd_global.user_id
          WHERE     ROWID = r_bpa_lines.ROWID
                AND NVL (implement_date, SYSDATE - 1) <= SYSDATE
                AND NVL (approve_flag, 1) = 2;

         COMMIT;
      END LOOP;
   EXCEPTION
      WHEN OTHERS
      THEN
         g_err_msg :=
               'Error in '
            || g_err_callpoint
            || ' WITH  '
            || SUBSTR (SQLERRM, 1, 1000);
         --dbms_output.put_line(l_fulltext);

         xxcus_error_pkg.xxcus_error_main_api (
            p_called_from         => g_err_callfrom,
            p_calling             => g_err_callpoint,
            p_ora_error_msg       => SQLERRM,
            p_error_desc          =>    'Error in call '
                                     || g_err_callfrom
                                     || g_err_callpoint,
            p_distribution_list   => g_distro_list,
            p_module              => g_module);
   END process_price_zone_lines;


   /*************************************************************************************************
   *   Procedure process_promo_lines                                                                *
   *   Purpose : Used in the process_staging to process staging records on xxwc_bpa_pz_staging_tbl  *
   *         and load into the XXWC Promo Tables                                                    *
   *                                                                                                *
   *                                                                                                *
   *              PROCEDURE process_promo_lines                                                     *
   *                               (p_po_number IN VARCHAR2                                         *
   *                               ,p_process_flag in NUMBER);                                      *
   *                                                                                                *
   *   REVISIONS:                                                                                   *
   *   Ver        Date        Author                     Description                                *
   *   ---------  ----------  ---------------         -------------------------                     *
   *   1.0        02/08/2014   Lee Spitzer              Initial Versions TMS 20130917-00676         *
   *                                                         Vendor Cost Management Improvements    *
   *                                                                                                *
   /************************************************************************************************/


   PROCEDURE process_promo_lines (p_po_number      IN VARCHAR2,
                                  p_process_flag   IN NUMBER)
   IS
      CURSOR c_bpa_lines
      IS
         SELECT xbpst.ROWID,
                xbpst.po_number,
                xbpst.item_number,
                xbpst.promo_price,
                xbpst.promo_start_date,
                xbpst.promo_end_date
           FROM apps.xxwc_bpa_pz_staging_tbl xbpst --Modified by Manjula on 21-Nov-14 for TMS # 20141002-00041 Multi-Org
          WHERE     xbpst.promo_table_flag = p_process_flag
                AND xbpst.po_number = p_po_number
                AND xbpst.promo_price IS NOT NULL
                AND xbpst.promo_start_date IS NOT NULL
                AND xbpst.promo_end_date IS NOT NULL
                AND NVL (xbpst.implement_date, SYSDATE - 1) <= SYSDATE
                AND xbpst.approve_flag = 2;                        --Approved;


      l_promo_exists        NUMBER;
      l_inventory_item_id   NUMBER;
      l_po_header_id        NUMBER;
      l_message             VARCHAR2 (2000);
   BEGIN
      g_err_callfrom := 'process_promo_lines';

      BEGIN
         SELECT po_header_id
           INTO l_po_header_id
           FROM po_headers_all
          WHERE segment1 = p_po_number;
      EXCEPTION
         WHEN OTHERS
         THEN
            l_po_header_id := NULL;
      END;

      FOR r_bpa_lines IN c_bpa_lines
      LOOP
         BEGIN
            SELECT inventory_item_id
              INTO l_inventory_item_id
              FROM mtl_system_items_b
             WHERE     segment1 = r_bpa_lines.item_number
                   AND organization_id =
                          FND_PROFILE.VALUE ('XXWC_ITEM_MASTER_ORG');
         EXCEPTION
            WHEN OTHERS
            THEN
               l_inventory_item_id := NULL;
         END;


         BEGIN
            SELECT COUNT (*)
              INTO l_promo_exists
              FROM APPS.XXWC_BPA_PROMO_TBL
             WHERE     po_header_id = l_po_header_id
                   AND inventory_item_id = l_inventory_item_id;
         --AND    nvl(price_zone = i_price_zone
         EXCEPTION
            WHEN OTHERS
            THEN
               l_promo_exists := 0;
         END;

         IF l_promo_exists = 0
         THEN
            IF r_bpa_lines.promo_price IS NOT NULL
            THEN
               IF r_bpa_lines.promo_start_date IS NULL
               THEN
                  /*l_message := 'Must have promo start date';
                  RAISE_APPLICATION_ERROR(-20001,l_message);
                 */
                  NULL;
               END IF;

               IF r_bpa_lines.promo_end_date IS NULL
               THEN
                  /*l_message := 'Must have promo end date';
                  RAISE_APPLICATION_ERROR(-20001,l_message);
                 */
                  NULL;
               END IF;

               BEGIN
                  INSERT INTO APPS.XXWC_BPA_PROMO_TBL --Modified by Manjula on 21-Nov-14 for TMS # 20141002-00041 Multi-Org
                                                      (creation_date,
                                                       created_by,
                                                       last_update_date,
                                                       last_updated_by,
                                                       po_header_id,
                                                       inventory_item_id,
                                                       promo_price,
                                                       promo_start_date,
                                                       promo_end_date)
                       VALUES (SYSDATE,
                               fnd_global.user_id,
                               SYSDATE,
                               fnd_global.user_id,
                               l_po_header_id,
                               l_inventory_item_id,
                               r_bpa_lines.promo_price,
                               r_bpa_lines.promo_start_date,
                               r_bpa_lines.promo_end_date);
               EXCEPTION
                  WHEN OTHERS
                  THEN
                     l_message :=
                           'Could not insert into XXWC_BPA_PROMO_TBL '
                        || SQLCODE
                        || SQLERRM;
                     RAISE_APPLICATION_ERROR (-20001, l_message);
               END;
            ELSE
               BEGIN
                  UPDATE APPS.XXWC_BPA_PROMO_TBL --Modified by Manjula on 21-Nov-14 for TMS # 20141002-00041 Multi-Org
                     SET promo_price = r_bpa_lines.promo_price,
                         promo_start_date = r_bpa_lines.promo_start_date,
                         promo_end_date = r_bpa_lines.promo_end_date,
                         last_updated_by = fnd_global.user_id,
                         last_update_date = SYSDATE
                   WHERE     po_header_id = l_po_header_id
                         AND inventory_item_id = l_inventory_item_id;
               EXCEPTION
                  WHEN OTHERS
                  THEN
                     l_message :=
                           'Could not update XXWC_BPA_PROMO_TBL '
                        || SQLCODE
                        || SQLERRM;
                     RAISE_APPLICATION_ERROR (-20001, l_message);
               END;
            END IF;
         ELSE
            DELETE APPS.XXWC_BPA_PROMO_TBL --Modified by Manjula on 21-Nov-14 for TMS # 20141002-00041 Multi-Org
             WHERE     po_header_id = l_po_header_id
                   AND inventory_item_id = l_inventory_item_id;
         END IF;

         UPDATE APPS.XXWC_bpa_pz_staging_tbl --Modified by Manjula on 21-Nov-14 for TMS # 20141002-00041 Multi-Org
            SET promo_table_flag = 2,
                last_update_date = SYSDATE,
                last_updated_by = fnd_global.user_id
          WHERE     ROWID = r_bpa_lines.ROWID
                AND NVL (implement_date, SYSDATE - 1) <= SYSDATE
                AND NVL (approve_flag, 1) = 2;

         COMMIT;
      END LOOP;
   EXCEPTION
      WHEN OTHERS
      THEN
         g_err_msg :=
               'Error in '
            || g_err_callpoint
            || ' WITH  '
            || SUBSTR (SQLERRM, 1, 1000);
         --dbms_output.put_line(l_fulltext);

         xxcus_error_pkg.xxcus_error_main_api (
            p_called_from         => g_err_callfrom,
            p_calling             => g_err_callpoint,
            p_ora_error_msg       => SQLERRM,
            p_error_desc          =>    'Error in call '
                                     || g_err_callfrom
                                     || g_err_callpoint,
            p_distribution_list   => g_distro_list,
            p_module              => g_module);
   END process_promo_lines;


   /*************************************************************************************************
   *   Procedure process_staging                                                                    *
   *   Purpose : Main procudure to used in the concurrent program XXWC BPA Price Zone Import        *
   *                                                                                                *
   *                                                                                                *
   *      PROCEDURE process_staging (Errbuf     OUT NOCOPY VARCHAR2                                 *
   *                         ,Retcode    OUT NOCOPY NUMBER                                          *
   *                         ,p_po_number IN VARCHAR2                                               *
   *                         ,p_process_flag IN NUMBER);                                            *
   *                                                                                                *
   *   REVISIONS:                                                                                   *
   *   Ver        Date        Author                     Description                                *
   *   ---------  ----------  ---------------         -------------------------                     *
   *   1.0        02/08/2014   Lee Spitzer              Initial Versions TMS 20130917-00676         *
   *                                                         Vendor Cost Management Improvements    *
   *                                                                                                *
   /************************************************************************************************/



   PROCEDURE process_staging (Errbuf              OUT NOCOPY VARCHAR2,
                              Retcode             OUT NOCOPY NUMBER,
                              p_po_number      IN            VARCHAR2,
                              p_process_flag   IN            NUMBER)
   IS
      CURSOR c_bpa
      IS
         SELECT DISTINCT po_number
           FROM apps.xxwc_bpa_pz_staging_tbl xbpst --Modified by Manjula on 21-Nov-14 for TMS # 20141002-00041 Multi-Org
          WHERE     xbpst.po_number = NVL (p_po_number, xbpst.po_number)
                AND NVL (xbpst.implement_date, SYSDATE - 1) <= SYSDATE
                AND xbpst.approve_flag = 2;                        --Approved;
   BEGIN
      g_err_callfrom := 'process_staging';

      FOR r_bpa IN c_bpa
      LOOP
         process_bpa_lines (r_bpa.po_number, p_process_flag);


         process_price_zone_lines (r_bpa.po_number, p_process_flag);

         process_promo_lines (r_bpa.po_number, p_process_flag);



         DELETE APPS.XXWC_BPA_PZ_STAGING_TBL --Modified by Manjula on 21-Nov-14 for TMS # 20141002-00041 Multi-Org
          WHERE     PO_NUMBER = r_bpa.po_number
                AND NVL (implement_date, SYSDATE - 1) <= SYSDATE
                AND NVL (approve_flag, 1) = 2;

         COMMIT;
      END LOOP;


      retcode := 0;
   EXCEPTION
      WHEN OTHERS
      THEN
         g_err_msg :=
               'Error in '
            || g_err_callpoint
            || ' WITH  '
            || SUBSTR (SQLERRM, 1, 1000);
         --dbms_output.put_line(l_fulltext);

         xxcus_error_pkg.xxcus_error_main_api (
            p_called_from         => g_err_callfrom,
            p_calling             => g_err_callpoint,
            p_ora_error_msg       => SQLERRM,
            p_error_desc          => 'Error in call XXWC_BPA_PRICE_ZONE_PKG.PROCESS_STAGING',
            p_distribution_list   => g_distro_list,
            p_module              => g_module);
   END process_staging;

   /*************************************************************************************************
     *   Procedure update_expiration_date                                                             *
     *   Purpose : Mass Update Expiration Date on the XXWC Price Zone Staging Table                   *
     *           used in the XXWC BPA Price Zone Form                                                 *
     *                                                                                                *
     *                                                                                                *
     *  PROCEDURE update_expiration_date (p_po_number in VARCHAR2                                     *
     *                                   ,p_expiration_date in DATE);                                 *
     *                                                                                                *
     *   REVISIONS:                                                                                   *
     *   Ver        Date        Author                     Description                                *
     *   ---------  ----------  ---------------         -------------------------                     *
     *   1.0        02/08/2014   Lee Spitzer              Initial Versions TMS 20130917-00676         *
     *                                                         Vendor Cost Management Improvements    *
     *                                                                                                *
     /************************************************************************************************/



   PROCEDURE update_expiration_date (p_po_number         IN VARCHAR2,
                                     p_expiration_date   IN DATE)
   IS
   BEGIN
      g_err_callfrom := 'update_expiration_date';


      UPDATE APPS.XXWC_BPA_PZ_STAGING_TBL --Modified by Manjula on 21-Nov-14 for TMS # 20141002-00041 Multi-Org
         SET expiration_date = p_expiration_date
       WHERE PO_NUMBER = P_PO_NUMBER;

      COMMIT;
   EXCEPTION
      WHEN OTHERS
      THEN
         g_err_msg :=
               'Error in '
            || g_err_callpoint
            || ' WITH  '
            || SUBSTR (SQLERRM, 1, 1000);
         --dbms_output.put_line(l_fulltext);

         xxcus_error_pkg.xxcus_error_main_api (
            p_called_from         => g_err_callfrom,
            p_calling             => g_err_callpoint,
            p_ora_error_msg       => SQLERRM,
            p_error_desc          =>    'Error in call '
                                     || g_err_callfrom
                                     || g_err_callpoint,
            p_distribution_list   => g_distro_list,
            p_module              => g_module);
   END update_expiration_date;

   /*************************************************************************************************
   *   Procedure update_implement_date                                                              *
   *   Purpose : Mass Update Implement Date on the XXWC Price Zone Staging Table                    *
   *           used in the XXWC BPA Price Zone Form                                                 *
   *                                                                                                *
   *                                                                                                *
   *  PROCEDURE update_implement_date (p_po_number in VARCHAR2                                      *
   *                                   ,p_implement_date in DATE);                                  *
   *                                                                                                *
   *   REVISIONS:                                                                                   *
   *   Ver        Date        Author                     Description                                *
   *   ---------  ----------  ---------------         -------------------------                     *
   *   1.0        02/08/2014   Lee Spitzer              Initial Versions TMS 20130917-00676         *
   *                                                         Vendor Cost Management Improvements    *
   *                                                                                                *
   /************************************************************************************************/

   PROCEDURE update_implement_date (p_po_number        IN VARCHAR2,
                                    p_implement_date   IN DATE)
   IS
   BEGIN
      g_err_callfrom := 'update_implement_date';

      UPDATE APPS.XXWC_BPA_PZ_STAGING_TBL --Modified by Manjula on 21-Nov-14 for TMS # 20141002-00041 Multi-Org
         SET implement_date = p_implement_date
       WHERE PO_NUMBER = P_PO_NUMBER;

      COMMIT;
   EXCEPTION
      WHEN OTHERS
      THEN
         g_err_msg :=
               'Error in '
            || g_err_callpoint
            || ' WITH  '
            || SUBSTR (SQLERRM, 1, 1000);
         --dbms_output.put_line(l_fulltext);

         xxcus_error_pkg.xxcus_error_main_api (
            p_called_from         => g_err_callfrom,
            p_calling             => g_err_callpoint,
            p_ora_error_msg       => SQLERRM,
            p_error_desc          =>    'Error in call '
                                     || g_err_callfrom
                                     || g_err_callpoint,
            p_distribution_list   => g_distro_list,
            p_module              => g_module);
   END update_implement_date;

   /*************************************************************************************************
  *   Procedure get_pricing                                                                        *
  *   Purpose : Used in the po_custom_price_pub to return the best price in PO and Req Forms       *
  *           Also used in XXWC Items To Meet Vendor Minimums Form                                 *
  *                                                                                                *
  *                                                                                                *
  *   PROCEDURE get_pricing (p_po_header_id IN NUMBER                                              *
  *                         ,p_organization_id IN NUMBER                                           *
  *                         ,p_inventory_item_id IN NUMBER                                         *
  *                         ,p_po_line_id IN NUMBER                                                *
  *                         ,p_quantity IN NUMBER                                                  *
  *                         ,p_date IN DATE                                                        *
  *                         ,x_vendor_price_zone OUT NUMBER                                        *
  *                         ,x_national_price OUT NUMBER                                           *
  *                         ,x_national_quantity_price OUT NUMBER                                  *
  *                         ,x_promo_price  OUT NUMBER                                             *
  *                         ,x_price_zone_price OUT NUMBER                                         *
  *                         ,x_price_zone_quantity_price OUT NUMBER                                *
  *                         ,x_best_price OUT NUMBER);                                             *
  *                                                                                                *
  *   REVISIONS:                                                                                   *
  *   Ver        Date        Author                     Description                                *
  *   ---------  ----------  ---------------         -------------------------                     *
  *   1.0        02/08/2014   Lee Spitzer              Initial Versions TMS 20130917-00676         *
  *                                                         Vendor Cost Management Improvements    *
  *                                                                                                *
  /************************************************************************************************/


   PROCEDURE get_pricing (p_po_header_id                IN     NUMBER,
                          p_organization_id             IN     NUMBER,
                          p_inventory_item_id           IN     NUMBER,
                          p_po_line_id                  IN     NUMBER,
                          p_quantity                    IN     NUMBER,
                          p_date                        IN     DATE,
                          x_vendor_price_zone              OUT NUMBER,
                          x_national_price                 OUT NUMBER,
                          x_national_quantity_price        OUT NUMBER,
                          x_promo_price                    OUT NUMBER,
                          x_price_zone_price               OUT NUMBER,
                          x_price_zone_quantity_price      OUT NUMBER,
                          x_best_price                     OUT NUMBER)
   IS
      l_vendor_id                 NUMBER;
      l_vendor_site_id            NUMBER;
      l_price                     NUMBER DEFAULT NULL;
      l_price_zone                NUMBER;
      l_national_price            NUMBER;
      l_national_quantity         NUMBER;
      l_national_quantity_price   NUMBER;
      l_promo_price               NUMBER;
      l_zone_price                NUMBER;
      l_zone_quantity             NUMBER;
      l_zone_quantity_price       NUMBER;
      l_api_version               NUMBER := 1.0;
      l_api_name                  VARCHAR2 (60) := 'GET_PRICING';
      l_log_head         CONSTANT VARCHAR2 (100) := g_log_head || l_api_name;
      l_progress                  VARCHAR2 (3) := '000';
      l_national_break_exists     NUMBER;
      l_zone_break_exists         NUMBER;
   BEGIN
      g_err_callfrom := 'get_pricing';

      IF g_debug_stmt
      THEN
         PO_DEBUG.debug_begin (l_log_head);
         PO_DEBUG.debug_var (l_log_head,
                             l_progress,
                             'p_po_header_id ',
                             p_po_header_id);
         PO_DEBUG.debug_var (l_log_head,
                             l_progress,
                             'p_organization_id ',
                             p_organization_id);
         PO_DEBUG.debug_var (l_log_head,
                             l_progress,
                             'p_inventory_item_id ',
                             p_inventory_item_id);
         PO_DEBUG.debug_var (l_log_head,
                             l_progress,
                             'p_po_line_id ',
                             p_po_line_id);
         PO_DEBUG.debug_var (l_log_head,
                             l_progress,
                             'p_quantity ',
                             p_quantity);
         PO_DEBUG.debug_var (l_log_head,
                             l_progress,
                             'p_date ',
                             p_date);
      END IF;



      BEGIN
         SELECT pha.vendor_id, pha.vendor_site_id
           INTO l_vendor_id, l_vendor_site_id
           FROM po_headers pha, --Modified by Manjula on 21-Nov-14 for TMS # 20141002-00041 Multi-Org
                               po_lines pla --Modified by Manjula on 21-Nov-14 for TMS # 20141002-00041 Multi-Org
          WHERE     pha.po_header_id = pla.po_header_id
                AND pla.po_line_id = p_po_line_id
                AND pla.po_header_Id = p_po_header_id
                AND pla.item_id = p_inventory_item_id;
      EXCEPTION
         WHEN OTHERS
         THEN
            l_vendor_id := NULL;
            l_vendor_site_id := NULL;
      END;


      IF g_debug_stmt
      THEN
         l_progress := '010';
         PO_DEBUG.debug_var (l_log_head,
                             l_progress,
                             'l_vendor_id ',
                             l_vendor_id);
         PO_DEBUG.debug_var (l_log_head,
                             l_progress,
                             'l_vendor_site_id ',
                             l_vendor_site_id);
      END IF;

      --Get the price zone from the vendor, item, and organization
      BEGIN
         SELECT NVL (price_zone, 0)
           INTO l_price_zone
           FROM XXWC.XXWC_VENDOR_PRICE_ZONE_TBL
          WHERE     vendor_id = l_vendor_id
                AND vendor_site_id = l_vendor_site_id
                AND organization_id = p_organization_id;
      EXCEPTION
         WHEN OTHERS
         THEN
            l_price_zone := 0;
      END;


      IF g_debug_stmt
      THEN
         l_progress := '020';
         PO_DEBUG.debug_var (l_log_head,
                             l_progress,
                             'l_price_zone ',
                             l_price_zone);
      END IF;

      --If the vendor and vendor site is not null get the promo, nation,and price zone price
      IF l_vendor_id IS NOT NULL AND l_vendor_site_id IS NOT NULL
      THEN
         --Get the promo price
         BEGIN
            SELECT promo_price
              INTO l_promo_price
              FROM APPS.XXWC_BPA_PROMO_TBL --Modified by Manjula on 21-Nov-14 for TMS # 20141002-00041 Multi-Org
             WHERE     inventory_item_id = p_inventory_item_id
                   AND po_header_id = p_po_header_Id
                   AND p_date BETWEEN promo_start_date AND promo_end_date;
         EXCEPTION
            WHEN OTHERS
            THEN
               l_promo_price := NULL;
         END;

         IF g_debug_stmt
         THEN
            l_progress := '030';
            PO_DEBUG.debug_var (l_log_head,
                                l_progress,
                                'l_promo_price ',
                                l_promo_price);
         END IF;


         --Get National Price
         BEGIN
            SELECT price_zone_price
              INTO l_national_price
              FROM APPS.XXWC_BPA_PRICE_ZONE_TBL --Modified by Manjula on 21-Nov-14 for TMS # 20141002-00041 Multi-Org
             WHERE     inventory_item_id = p_inventory_item_id
                   AND po_header_id = p_po_header_Id
                   AND price_zone = 0;
         EXCEPTION
            WHEN OTHERS
            THEN
               l_national_price := NULL;
         END;


         IF g_debug_stmt
         THEN
            l_progress := '040';
            PO_DEBUG.debug_var (l_log_head,
                                l_progress,
                                'l_national_price ',
                                l_national_price);
         END IF;



         BEGIN
            SELECT COUNT (*)
              INTO l_national_break_exists
              FROM apps.xxwc_bpa_price_zone_breaks_tbl --Modified by Manjula on 21-Nov-14 for TMS # 20141002-00041 Multi-Org
             WHERE     inventory_item_id = p_inventory_item_id
                   AND po_header_id = p_po_header_Id
                   AND price_zone = 0;
         EXCEPTION
            WHEN OTHERS
            THEN
               l_national_break_exists := 0;
         END;


         IF g_debug_stmt
         THEN
            l_progress := '050';
            PO_DEBUG.debug_var (l_log_head,
                                l_progress,
                                'l_national_break_exists ',
                                l_national_break_exists);
         END IF;



         IF l_national_break_exists > 0
         THEN
            BEGIN
               SELECT MAX (price_zone_quantity)
                 INTO l_national_quantity
                 FROM APPS.XXWC_BPA_PRICE_ZONE_BREAKS_TBL xppzt --Modified by Manjula on 21-Nov-14 for TMS # 20141002-00041 Multi-Org
                WHERE     po_header_id = p_po_header_id
                      AND inventory_item_id = p_inventory_item_id
                      AND price_zone = 0
                      AND price_zone_quantity <= NVL (p_quantity, 0);
            EXCEPTION
               WHEN OTHERS
               THEN
                  l_national_quantity := NULL;
            END;


            IF g_debug_stmt
            THEN
               l_progress := '060';
               PO_DEBUG.debug_var (l_log_head,
                                   l_progress,
                                   'l_national_quantity ',
                                   l_national_quantity);
            END IF;


            IF l_national_quantity IS NOT NULL
            THEN
               BEGIN
                  SELECT price_zone_quantity_price
                    INTO l_national_quantity_price
                    FROM APPS.XXWC_BPA_PRICE_ZONE_BREAKS_TBL xppzt --Modified by Manjula on 21-Nov-14 for TMS # 20141002-00041 Multi-Org
                   WHERE     po_header_id = p_po_header_id
                         AND inventory_item_id = p_inventory_item_id
                         AND price_zone = 0
                         AND price_zone_quantity = l_national_quantity;
               EXCEPTION
                  WHEN OTHERS
                  THEN
                     l_national_quantity_price := NULL;
               END;
            ELSE
               l_national_quantity_price := NULL;
            END IF;


            IF g_debug_stmt
            THEN
               l_progress := '070';
               PO_DEBUG.debug_var (l_log_head,
                                   l_progress,
                                   'l_national_quantity_price ',
                                   l_national_quantity_price);
            END IF;
         ELSE
            l_national_quantity_price := NULL;
         END IF;

         --Get Price Zone Price
         BEGIN
            SELECT price_zone_price
              INTO l_zone_price
              FROM APPS.XXWC_BPA_PRICE_ZONE_TBL --Modified by Manjula on 21-Nov-14 for TMS # 20141002-00041 Multi-Org
             WHERE     inventory_item_id = p_inventory_item_id
                   AND po_header_id = p_po_header_Id
                   AND price_zone = l_price_zone;
         EXCEPTION
            WHEN OTHERS
            THEN
               l_zone_price := NULL;
         END;


         IF g_debug_stmt
         THEN
            l_progress := '080';
            PO_DEBUG.debug_var (l_log_head,
                                l_progress,
                                'l_zone_price ',
                                l_zone_price);
         END IF;


         BEGIN
            SELECT COUNT (*)
              INTO l_zone_break_exists
              FROM apps.xxwc_bpa_price_zone_breaks_tbl --Modified by Manjula on 21-Nov-14 for TMS # 20141002-00041 Multi-Org
             WHERE     inventory_item_id = p_inventory_item_id
                   AND po_header_id = p_po_header_Id
                   AND price_zone = l_price_zone;
         EXCEPTION
            WHEN OTHERS
            THEN
               l_zone_break_exists := 0;
         END;


         IF g_debug_stmt
         THEN
            l_progress := '090';
            PO_DEBUG.debug_var (l_log_head,
                                l_progress,
                                'l_zone_break_exists ',
                                l_zone_break_exists);
         END IF;


         IF l_zone_break_exists > 0
         THEN
            BEGIN
               SELECT MAX (price_zone_quantity)
                 INTO l_zone_quantity
                 FROM APPS.XXWC_BPA_PRICE_ZONE_BREAKS_TBL xppzt --Modified by Manjula on 21-Nov-14 for TMS # 20141002-00041 Multi-Org
                WHERE     po_header_id = p_po_header_id
                      AND inventory_item_id = p_inventory_item_id
                      AND price_zone = l_price_zone
                      AND price_zone_quantity <= NVL (p_quantity, 0);
            EXCEPTION
               WHEN OTHERS
               THEN
                  l_zone_quantity := NULL;
            END;


            IF g_debug_stmt
            THEN
               l_progress := '100';
               PO_DEBUG.debug_var (l_log_head,
                                   l_progress,
                                   'l_zone_quantity ',
                                   l_zone_quantity);
            END IF;


            IF l_zone_quantity IS NOT NULL
            THEN
               BEGIN
                  SELECT price_zone_quantity_price
                    INTO l_zone_quantity_price
                    FROM APPS.XXWC_BPA_PRICE_ZONE_BREAKS_TBL xppzt --Modified by Manjula on 21-Nov-14 for TMS # 20141002-00041 Multi-Org
                   WHERE     po_header_id = p_po_header_id
                         AND inventory_item_id = p_inventory_item_id
                         AND price_zone = l_price_zone
                         AND price_zone_quantity = l_zone_quantity;
               EXCEPTION
                  WHEN OTHERS
                  THEN
                     l_zone_quantity_price := NULL;
               END;
            ELSE
               l_zone_quantity_price := NULL;
            END IF;
         ELSE
            l_zone_quantity_price := NULL;
         END IF;



         IF g_debug_stmt
         THEN
            l_progress := '110';
            PO_DEBUG.debug_var (l_log_head,
                                l_progress,
                                'l_zone_quantity_price ',
                                l_zone_quantity_price);
         END IF;
      END IF;



      IF l_vendor_id IS NOT NULL AND l_vendor_site_id IS NOT NULL
      THEN
         --Setting Price Preferences

         --start with NULL

         l_price := NULL;


         --Setting price to national price

         IF l_national_price IS NOT NULL
         THEN
            --if there is a national quantity and quantity met it, set the price to national price quantity

            IF l_national_quantity_price IS NOT NULL
            THEN
               l_price := l_national_quantity_price;
            ELSE
               l_price := l_national_price;
            END IF;
         ELSE
            l_price := l_price;
         END IF;



         -- Resetting price to zone price if one is there

         IF l_zone_price IS NOT NULL
         THEN
            IF l_zone_quantity_price IS NOT NULL
            THEN
               l_price := l_zone_quantity_price;
            ELSE
               l_price := l_zone_price;
            END IF;
         ELSE
            l_price := l_price;
         END IF;



         IF l_promo_price IS NOT NULL
         THEN
            l_price := l_promo_price;
         ELSE
            l_price := l_price;
         END IF;
      ELSE
         l_price := l_price;
      END IF;


      IF g_debug_stmt
      THEN
         l_progress := '120';
         PO_DEBUG.debug_var (l_log_head,
                             l_progress,
                             'l_price ',
                             l_price);
      END IF;


      x_vendor_price_zone := l_price_zone;
      x_national_price := l_national_price;
      x_national_quantity_price := l_national_quantity_price;
      x_promo_price := l_promo_price;
      x_price_zone_price := l_zone_price;
      x_price_zone_quantity_price := l_zone_quantity_price;
      x_best_price := l_price;
   EXCEPTION
      WHEN OTHERS
      THEN
         g_err_msg :=
               'Error in '
            || g_err_callpoint
            || ' WITH  '
            || SUBSTR (SQLERRM, 1, 1000);
         --dbms_output.put_line(l_fulltext);

         xxcus_error_pkg.xxcus_error_main_api (
            p_called_from         => g_err_callfrom,
            p_calling             => g_err_callpoint,
            p_ora_error_msg       => SQLERRM,
            p_error_desc          =>    'Error in call '
                                     || g_err_callfrom
                                     || g_err_callpoint,
            p_distribution_list   => g_distro_list,
            p_module              => g_module);
   END get_pricing;



   /*************************************************************************************************
   *   Function get_default_vendor_contact_id                                                       *
   *   Purpose : used to get the default contact id in the PO Form #16 Personalization              *            *
   *                                                                                                *
   *                                                                                                *
   *   FUNCTION get_default_vendor_contact_id (p_vendor_id IN NUMBER                                *
   *                                      ,p_vendor_site_id IN NUMBER                               *
   *                                      ,p_organization_id IN NUMBER)                             *
   *                                      RETURN NUMBER;                                            *
   *                                                                                                *
   *   REVISIONS:                                                                                   *
   *   Ver        Date        Author                     Description                                *
   *   ---------  ----------  ---------------         -------------------------                     *
   *   1.0        02/08/2014   Lee Spitzer              Initial Versions TMS 20130917-00676         *
   *                                                         Vendor Cost Management Improvements    *
   *                                                                                                *
   /************************************************************************************************/

   FUNCTION get_default_vendor_contact_id (p_vendor_id         IN NUMBER,
                                           p_vendor_site_id    IN NUMBER,
                                           p_organization_id   IN NUMBER)
      RETURN NUMBER
   IS
      l_vendor_contact_id   NUMBER;
   BEGIN
      g_err_callfrom := 'get_default_vendor_contact_id';

      BEGIN
         SELECT vendor_contact_id
           INTO l_vendor_contact_id
           FROM XXWC.XXWC_PO_VENDOR_MINIMUM
          WHERE     NVL (default_flag, 'N') = 'Y'
                AND vendor_id = p_vendor_id
                AND vendor_site_id = p_vendor_site_id
                AND organization_id = p_organization_id;
      EXCEPTION
         WHEN NO_DATA_FOUND
         THEN
            l_vendor_contact_id := NULL;
         WHEN TOO_MANY_ROWS
         THEN
            BEGIN
               SELECT vendor_contact_id
                 INTO l_vendor_contact_id
                 FROM (SELECT vendor_contact_id
                         FROM XXWC.XXWC_PO_VENDOR_MINIMUM
                        WHERE     NVL (default_flag, 'N') = 'Y'
                              AND vendor_id = p_vendor_id
                              AND vendor_site_id = p_vendor_site_id
                              AND organization_id = p_organization_id)
                WHERE ROWNUM = 1;
            EXCEPTION
               WHEN OTHERS
               THEN
                  l_vendor_contact_id := NULL;
            END;
         WHEN OTHERS
         THEN
            l_vendor_contact_id := NULL;
      END;

      RETURN l_vendor_contact_id;
   EXCEPTION
      WHEN OTHERS
      THEN
         l_vendor_contact_id := NULL;

         g_err_msg :=
               'Error in '
            || g_err_callpoint
            || ' WITH  '
            || SUBSTR (SQLERRM, 1, 1000);
         --dbms_output.put_line(l_fulltext);

         xxcus_error_pkg.xxcus_error_main_api (
            p_called_from         => g_err_callfrom,
            p_calling             => g_err_callpoint,
            p_ora_error_msg       => SQLERRM,
            p_error_desc          =>    'Error in call '
                                     || g_err_callfrom
                                     || g_err_callpoint,
            p_distribution_list   => g_distro_list,
            p_module              => g_module);
   END get_default_vendor_contact_id;
END xxwc_bpa_price_zone_pkg;
/
Show Errors;
/