CREATE OR REPLACE PACKAGE APPS.XXWC_BPA_PRICE_ZONE_FORM_PKG
IS
   /***************************************************************************************************************************
   -- File Name: APPS.XXWC_BPA_PRICE_ZONE_FORM_PKG.pkb
   --
   -- PROGRAM TYPE: PL/SQL Script   <API>
   --
   -- PURPOSE: Package is to populate data into Global temp table XXWC_BPA_PRICE_ZONE_V_TEMP_TBL
   --          and XXWC.XXWC_BPA_PRC_ZONE_WEBADI_TMP_T
   -- HISTORY
   -- =========================================================================================================================
   -- =========================================================================================================================
   -- VERSION DATE          AUTHOR(S)       DESCRIPTION
   -- ------- -----------   --------------- -----------------------------------------------------------------------------------
   -- 1.0     01-Apr-2015   P.Vamshidhar    Created this package.
   --                                       TMS#20150414-00173-2015 PLM - Implement Cost Management tool enhancements
   --                                       package created to populate data into global temp XXWC_BPA_PRICE_ZONE_V_TEMP_TBL
   --                                       and XXWC.XXWC_BPA_PRC_ZONE_WEBADI_TMP_T tables for supplier and supplier list.
   --                                       Tables will be used in BPA_PRICE_ZONE form and PRICE ZONE webadi.
   ***************************************************************************************************************************/

   PROCEDURE XXWC_BPA_ZONE_TEMP_TBL_PRC (p_vendor_id        IN     NUMBER,
                                         p_vendor_site_id   IN     NUMBER,
                                         p_po_number        IN     VARCHAR2,
                                         x_err_message         OUT VARCHAR2);


   PROCEDURE XXWC_BPA_SUPPLIST_TEMP_TBL_PRC (
      p_supp_list_id   IN     NUMBER,
      x_err_mess          OUT VARCHAR2);


   PROCEDURE XXWC_BPA_PRC_ZONE_WEBADI_PRC (
      p_vendor_id        IN     NUMBER,
      p_vendor_site_id   IN     NUMBER,
      p_po_header_id     IN     NUMBER,
      x_err_message         OUT VARCHAR2);


   PROCEDURE XXWC_BPA_PZ_STAGING_TBL_LOAD (
      P_CREATION_DATE                 IN DATE,
      P_CREATED_BY                    IN NUMBER,
      P_LAST_UPDATE_DATE              IN DATE,
      P_LAST_UPDATED_BY               IN NUMBER,
      P_VENDOR_NAME                   IN VARCHAR2,
      P_VENDOR_NUM                    IN VARCHAR2,
      P_VENDOR_SITE_CODE              IN VARCHAR2,
      P_PO_NUMBER                     IN VARCHAR2,
      P_ITEM_NUMBER                   IN VARCHAR2,
      P_ITEM_DESCRIPTION              IN VARCHAR2,
      P_VENDOR_ITEM_NUMBER            IN VARCHAR2,
      P_EXPIRATION_DATE               IN DATE,
      P_IMPLEMENT_DATE                IN DATE,
      P_BPA_TABLE_FLAG                IN NUMBER,
      P_NATIONAL_PRICE                IN NUMBER,
      P_NATIONAL_QUANTITY             IN NUMBER,
      P_NATIONAL_BREAK                IN NUMBER,
      P_PROMO_PRICE                   IN NUMBER,
      P_PROMO_START_DATE              IN DATE,
      P_PROMO_END_DATE                IN DATE,
      P_PROMO_TABLE_FLAG              IN NUMBER,
      P_PRICE_ZONE_PRICE1             IN NUMBER,
      P_PRICE_ZONE_QUANTITY1          IN NUMBER,
      P_PRICE_ZONE_QUANTITY_PRICE1    IN NUMBER,
      P_PRICE_ZONE_PRICE2             IN NUMBER,
      P_PRICE_ZONE_QUANTITY2          IN NUMBER,
      P_PRICE_ZONE_QUANTITY_PRICE2    IN NUMBER,
      P_PRICE_ZONE_PRICE3             IN NUMBER,
      P_PRICE_ZONE_QUANTITY3          IN NUMBER,
      P_PRICE_ZONE_QUANTITY_PRICE3    IN NUMBER,
      P_PRICE_ZONE_PRICE4             IN NUMBER,
      P_PRICE_ZONE_QUANTITY4          IN NUMBER,
      P_PRICE_ZONE_QUANTITY_PRICE4    IN NUMBER,
      P_PRICE_ZONE_PRICE5             IN NUMBER,
      P_PRICE_ZONE_QUANTITY5          IN NUMBER,
      P_PRICE_ZONE_QUANTITY_PRICE5    IN NUMBER,
      P_PRICE_ZONE_PRICE6             IN NUMBER,
      P_PRICE_ZONE_QUANTITY6          IN NUMBER,
      P_PRICE_ZONE_QUANTITY_PRICE6    IN NUMBER,
      P_PRICE_ZONE_PRICE7             IN NUMBER,
      P_PRICE_ZONE_QUANTITY7          IN NUMBER,
      P_PRICE_ZONE_QUANTITY_PRICE7    IN NUMBER,
      P_PRICE_ZONE_PRICE8             IN NUMBER,
      P_PRICE_ZONE_QUANTITY8          IN NUMBER,
      P_PRICE_ZONE_QUANTITY_PRICE8    IN NUMBER,
      P_PRICE_ZONE_PRICE9             IN NUMBER,
      P_PRICE_ZONE_QUANTITY9          IN NUMBER,
      P_PRICE_ZONE_QUANTITY_PRICE9    IN NUMBER,
      P_PRICE_ZONE_PRICE10            IN NUMBER,
      P_PRICE_ZONE_QUANTITY10         IN NUMBER,
      P_PRICE_ZONE_QUANTITY_PRICE10   IN NUMBER,
      P_PRICE_ZONE_PRICE11            IN NUMBER,
      P_PRICE_ZONE_QUANTITY11         IN NUMBER,
      P_PRICE_ZONE_QUANTITY_PRICE11   IN NUMBER,
      P_PRICE_ZONE_PRICE12            IN NUMBER,
      P_PRICE_ZONE_QUANTITY12         IN NUMBER,
      P_PRICE_ZONE_QUANTITY_PRICE12   IN NUMBER,
      P_PRICE_ZONE_PRICE13            IN NUMBER,
      P_PRICE_ZONE_QUANTITY13         IN NUMBER,
      P_PRICE_ZONE_QUANTITY_PRICE13   IN NUMBER,
      P_PRICE_ZONE_PRICE14            IN NUMBER,
      P_PRICE_ZONE_QUANTITY14         IN NUMBER,
      P_PRICE_ZONE_QUANTITY_PRICE14   IN NUMBER,
      P_PRICE_ZONE_PRICE15            IN NUMBER,
      P_PRICE_ZONE_QUANTITY15         IN NUMBER,
      P_PRICE_ZONE_QUANTITY_PRICE15   IN NUMBER,
      P_PRICE_ZONE_PRICE16            IN NUMBER,
      P_PRICE_ZONE_QUANTITY16         IN NUMBER,
      P_PRICE_ZONE_QUANTITY_PRICE16   IN NUMBER,
      P_PRICE_ZONE_PRICE17            IN NUMBER,
      P_PRICE_ZONE_QUANTITY17         IN NUMBER,
      P_PRICE_ZONE_QUANTITY_PRICE17   IN NUMBER,
      P_PRICE_ZONE_PRICE18            IN NUMBER,
      P_PRICE_ZONE_QUANTITY18         IN NUMBER,
      P_PRICE_ZONE_QUANTITY_PRICE18   IN NUMBER,
      P_PRICE_ZONE_PRICE19            IN NUMBER,
      P_PRICE_ZONE_QUANTITY19         IN NUMBER,
      P_PRICE_ZONE_QUANTITY_PRICE19   IN NUMBER,
      P_PRICE_ZONE_PRICE20            IN NUMBER,
      P_PRICE_ZONE_QUANTITY20         IN NUMBER,
      P_PRICE_ZONE_QUANTITY_PRICE20   IN NUMBER,
      P_PRICE_ZONE_PRICE21            IN NUMBER,
      P_PRICE_ZONE_QUANTITY21         IN NUMBER,
      P_PRICE_ZONE_QUANTITY_PRICE21   IN NUMBER,
      P_PRICE_ZONE_PRICE22            IN NUMBER,
      P_PRICE_ZONE_QUANTITY22         IN NUMBER,
      P_PRICE_ZONE_QUANTITY_PRICE22   IN NUMBER,
      P_PRICE_ZONE_PRICE23            IN NUMBER,
      P_PRICE_ZONE_QUANTITY23         IN NUMBER,
      P_PRICE_ZONE_QUANTITY_PRICE23   IN NUMBER,
      P_PRICE_ZONE_PRICE24            IN NUMBER,
      P_PRICE_ZONE_QUANTITY24         IN NUMBER,
      P_PRICE_ZONE_QUANTITY_PRICE24   IN NUMBER,
      P_PRICE_ZONE_PRICE25            IN NUMBER,
      P_PRICE_ZONE_QUANTITY25         IN NUMBER,
      P_PRICE_ZONE_QUANTITY_PRICE25   IN NUMBER,
      P_PRICE_ZONE_PRICE26            IN NUMBER,
      P_PRICE_ZONE_QUANTITY26         IN NUMBER,
      P_PRICE_ZONE_QUANTITY_PRICE26   IN NUMBER,
      P_PRICE_ZONE_PRICE27            IN NUMBER,
      P_PRICE_ZONE_QUANTITY27         IN NUMBER,
      P_PRICE_ZONE_QUANTITY_PRICE27   IN NUMBER,
      P_PRICE_ZONE_PRICE28            IN NUMBER,
      P_PRICE_ZONE_QUANTITY28         IN NUMBER,
      P_PRICE_ZONE_QUANTITY_PRICE28   IN NUMBER,
      P_PRICE_ZONE_PRICE29            IN NUMBER,
      P_PRICE_ZONE_QUANTITY29         IN NUMBER,
      P_PRICE_ZONE_QUANTITY_PRICE29   IN NUMBER,
      P_PRICE_ZONE_PRICE30            IN NUMBER,
      P_PRICE_ZONE_QUANTITY30         IN NUMBER,
      P_PRICE_ZONE_QUANTITY_PRICE30   IN NUMBER,
      P_PRICE_ZONE_TABLE_FLAG         IN NUMBER,
      P_ORG_PRICE_ZONE_1              IN VARCHAR2,
      P_ORG_PRICE_ZONE_2              IN VARCHAR2,
      P_ORG_PRICE_ZONE_3              IN VARCHAR2,
      P_ORG_PRICE_ZONE_4              IN VARCHAR2,
      P_ORG_PRICE_ZONE_5              IN VARCHAR2,
      P_ORG_PRICE_ZONE_6              IN VARCHAR2,
      P_ORG_PRICE_ZONE_7              IN VARCHAR2,
      P_ORG_PRICE_ZONE_8              IN VARCHAR2,
      P_ORG_PRICE_ZONE_9              IN VARCHAR2,
      P_ORG_PRICE_ZONE_10             IN VARCHAR2,
      P_ORG_PRICE_ZONE_11             IN VARCHAR2,
      P_ORG_PRICE_ZONE_12             IN VARCHAR2,
      P_ORG_PRICE_ZONE_13             IN VARCHAR2,
      P_ORG_PRICE_ZONE_14             IN VARCHAR2,
      P_ORG_PRICE_ZONE_15             IN VARCHAR2,
      P_ORG_PRICE_ZONE_16             IN VARCHAR2,
      P_ORG_PRICE_ZONE_17             IN VARCHAR2,
      P_ORG_PRICE_ZONE_18             IN VARCHAR2,
      P_ORG_PRICE_ZONE_19             IN VARCHAR2,
      P_ORG_PRICE_ZONE_20             IN VARCHAR2,
      P_ORG_PRICE_ZONE_21             IN VARCHAR2,
      P_ORG_PRICE_ZONE_22             IN VARCHAR2,
      P_ORG_PRICE_ZONE_23             IN VARCHAR2,
      P_ORG_PRICE_ZONE_24             IN VARCHAR2,
      P_ORG_PRICE_ZONE_25             IN VARCHAR2,
      P_ORG_PRICE_ZONE_26             IN VARCHAR2,
      P_ORG_PRICE_ZONE_27             IN VARCHAR2,
      P_ORG_PRICE_ZONE_28             IN VARCHAR2,
      P_ORG_PRICE_ZONE_29             IN VARCHAR2,
      P_ORG_PRICE_ZONE_30             IN VARCHAR2,
      P_APPROVE_FLAG                  IN NUMBER,
      P_ORG_ID                        IN NUMBER);

   FUNCTION BPA_PRICE_VALIDATE (P_ZONE             IN NUMBER,
                                P_ZONE_PRICE       IN NUMBER,
                                P_ZONE_PRICE_QTY   IN NUMBER,
                                P_ZONE_QTY_PRICE   IN NUMBER)
      RETURN VARCHAR2;
END XXWC_BPA_PRICE_ZONE_FORM_PKG;
/
