create or replace package APPS.XXWC_BPA_IMPACT_REPORT_PKG as
   /*************************************************************************************************
   *    $Header XXWC_BPA_IMPACT_REPORT_PKG $                                                        *
   *   Module Name: XXWC_BPA_IMPACT_REPORT_PKG                                                      *
   *                                                                                                *
   *   PURPOSE:   This package is used to create the CSV version of the impact report               *
   *                                                                                                * 
   *   REVISIONS:                                                                                   *
   *   Ver        Date        Author                     Description                                *
   *   ---------  ----------  ---------------         -------------------------                     *
   *   1.0        08/25/2014  Lee Spitzer              Initial Version 20140726-00006               *
   *                                                     New BPA Impact Report - deploy CSV version *
   *                                                                                                *      
   *   2.0        05/25/2015 P.Vamshidhar              TMS#20150414-00173                           *
   *                                                   New BPA Impact Report - deploy CSV version   *
   /************************************************************************************************/

  g_return NUMBER;
  g_message VARCHAR2(2000);

   /*************************************************************************************************   
   *   Procedure generate_csv                                                                       *
   *   Purpose : Procudure execute the CSV version of the impact report                             *
   *                                                                                                *
   *                                                                                                *
   *     PROCEDURE generate_csv (errbuf          OUT VARCHAR2                                       *
   *                         ,retcode              OUT VARCHAR2                                     *
   *                         ,P_PO_NUMBER          IN  VARCHAR2   --PO Number                                  *
   *                         ,P_DC_MODE            IN  VARCHAR2   --DC or Branch                                  *
   *                         ,P_NOTES              IN  VARCHAR2                                     *
   *                         ,P_MST_ORG_ID         IN  NUMNBER                                      *
   *                         ,P_ORG_ID             IN  NUMBER                                       *
   *                                                                                                *
   *   REVISIONS:                                                                                   *
   *   Ver        Date        Author                     Description                                *
   *   ---------  ----------  ---------------         -------------------------                     *
   *   1.0        08/25/2014  Lee Spitzer              Initial Version 20140726-00006               *
   *                                                     New BPA Impact Report - deploy CSV version *
   *                                                                                                *
   /************************************************************************************************/
   

    PROCEDURE generate_csv (errbuf               OUT VARCHAR2
                           ,retcode              OUT VARCHAR2
                           ,P_PO_NUMBER          IN  VARCHAR2
                           ,P_DC_MODE            IN  VARCHAR2
                           ,P_NOTES              IN  VARCHAR2
                           ,P_MST_ORG_ID         IN  NUMBER
                           ,P_ORG_ID             IN  NUMBER);

   /*************************************************************************************************   
   *   Procedure insert_into_temp                                                                   *
   *   Purpose : Procudure inserts records into temp table                                          *
   *                                                                                                *
   *                                                                                                *
   *              PROCEDURE insert_into_temp                                                        *
   *                        (p_po_number         IN VARCHAR2                                        *
   *                        ,p_mst_org_id        IN NUMBER                                          *
   *                        ,p_org_id            IN NUMBER                                        *
   *                        ,x_return            OUT NUMBER
   *                        ,x_message           OUT VARCHAR2                                                                        *
   *   REVISIONS:                                                                                   *
   *   Ver        Date        Author                     Description                                *
   *   ---------  ----------  ---------------         -------------------------                     *
   *   1.0        08/25/2014  Lee Spitzer              Initial Version 20140726-00006               *
   *                                                     New BPA Impact Report - deploy CSV version *
   *                                                                                                *
   /************************************************************************************************/
   
    PROCEDURE insert_into_temp
                            (p_po_number         IN VARCHAR2
                            ,p_mst_org_id        IN NUMBER
                            ,p_org_id            IN NUMBER
                            ,p_dc_mode           IN VARCHAR2
                            ,x_return            OUT NUMBER
                            ,x_message           OUT VARCHAR2);


  
  /*************************************************************************************************   
   *   Function  get_rpm_dpm                                                                        *
   *   Purpose : Returns the RPM or DPM based on the region                                         *
   *                                                                                                *
   *                                                                                                *
   *              FUNCTION get_rpm_dpm                                                              *
   *                        (p_region   IN VARCHAR2)                                                *
   *                     VARCHAR2                                                                   *
   *   REVISIONS:                                                                                   *
   *   Ver        Date        Author                     Description                                *
   *   ---------  ----------  ---------------         -------------------------                     *
   *   1.0        08/25/2014  Lee Spitzer              Initial Version 20140726-00006               *
   *                                                     New BPA Impact Report - deploy CSV version *
   *                                                                                                *
   /************************************************************************************************/
   
    FUNCTION  get_rpm_dpm
                           (p_region            IN VARCHAR2)
        RETURN VARCHAR2;
        
   /*************************************************************************************************   
   *   Function  get_current_cost                                                                   *
   *   Purpose : Returns the current cost                                                           *
   *                                                                                                *
   *                                                                                                *
   *              FUNCTION get_current_cost                                                         *
   *                       (p_po_header_id       IN NUMBER                                          *
   *                       ,p_inventory_item_id  IN NUMBER                                          *
   *                       ,p_price_zone         IN NUMBER)                                         *
   *                     NUMBER                                                                     *
   *   REVISIONS:                                                                                   *
   *   Ver        Date        Author                     Description                                *
   *   ---------  ----------  ---------------         -------------------------                     *
   *   1.0        08/25/2014  Lee Spitzer              Initial Version 20140726-00006               *
   *                                                     New BPA Impact Report - deploy CSV version *
   *                                                                                                *
   /************************************************************************************************/

    FUNCTION get_current_cost
                           (p_po_header_id       IN NUMBER
                           ,p_inventory_item_id  IN NUMBER
                           ,p_price_zone         IN NUMBER)
        RETURN NUMBER;
        
   /*************************************************************************************************   
   *   Function  get_updated_cost                                                                   *
   *   Purpose : Returns the new cost                                                               *
   *                                                                                                *
   *                                                                                                *
   *              FUNCTION get_updated_cost                                                         *
   *                        (p_po_number        IN VARCHAR2                                         *
   *                        ,p_item_number      IN VARCHAR2                                         *
   *                        ,p_price_zone       IN NUMBER)                                          *
   *                RETURN NUMBER;                                                                  *
   *                                                                                                *
   *   REVISIONS:                                                                                   *
   *   Ver        Date        Author                     Description                                *
   *   ---------  ----------  ---------------         -------------------------                     *
   *   1.0        08/25/2014  Lee Spitzer              Initial Version 20140726-00006               *
   *                                                     New BPA Impact Report - deploy CSV version *
   *                                                                                                *
   /************************************************************************************************/

    FUNCTION get_updated_cost
                            (p_po_number        IN VARCHAR2
                            ,p_item_number      IN VARCHAR2
                            ,p_price_zone       IN NUMBER)
        RETURN NUMBER;
    
   /*************************************************************************************************   
   *   Function: get_history_start_date                                                             *
   *   Purpose : Get history start date                                                             *
   *                                                                                                *
   *                                                                                                *
   *              FUNCTION get_history_start_date                                                   *
   *                        (p_mst_org_id      IN NUMBER)                                           *
   *               RETURN DATE;                                                                     *
   *                                                                                                *
   *   REVISIONS:                                                                                   *
   *   Ver        Date        Author                     Description                                *
   *   ---------  ----------  ---------------         -------------------------                     *
   *   1.0        08/25/2014  Lee Spitzer              Initial Version 20140726-00006               *
   *                                                     New BPA Impact Report - deploy CSV version *
   *                                                                                                *
   /************************************************************************************************/
    
    FUNCTION get_history_start_date
                             (p_mst_org_id      IN NUMBER)
        RETURN DATE;
   
   /*************************************************************************************************   
   *   Procedure get_history                                                                        *
   *   Purpose : Get history data                                                                   *
   *                                                                                                *
   *                                                                                                *
   *              PROCEDURE get_history                                                             *
   *                        (p_inventory_item_id IN NUMBER                                          *
   *                        ,p_organization_id IN NUMBER                                            *
   *                        ,p_dc_mode IN VARCHAR2                                                  *
   *                        ,p_start_date IN DATE                                                   *
   *                        ,X_HISTORY_UNITS OUT NUMBER                                             *
   *                        ,X_HISTORY_COGS  OUT NUMBER);                                           *
   *                                                                                                *
   *   REVISIONS:                                                                                   *
   *   Ver        Date        Author                     Description                                *
   *   ---------  ----------  ---------------         -------------------------                     *
   *   1.0        08/25/2014  Lee Spitzer              Initial Version 20140726-00006               *
   *                                                     New BPA Impact Report - deploy CSV version *
   *                                                                                                *
   /************************************************************************************************/
    
    PROCEDURE get_history
                            (p_inventory_item_id IN NUMBER
                            ,p_organization_id IN NUMBER
                            ,p_dc_mode IN VARCHAR2
                            ,p_start_date IN DATE
                            ,X_HISTORY_UNITS OUT NUMBER
                            ,X_HISTORY_COGS  OUT NUMBER
                            ,X_PURCHASE_HISTORY OUT NUMBER
                            ,X_PURCHASE_COST OUT NUMBER);

/*************************************************************************************************   
   *   Procedure generate_csv_new                                                                   *
   *   Purpose : Procudure execute the New CSV version of the impact report                         *
   *                                                                                                *
   *                                                                                                *
   *     PROCEDURE generate_csv (P_PO_NUMBER          IN  VARCHAR2   --PO Number                       *
   *                            ,P_DC_MODE            IN  VARCHAR2   --DC or Branch                    *
   *                         ,P_NOTES              IN  VARCHAR2                                     *
   *                         ,P_MST_ORG_ID         IN  NUMNBER                                      *
   *                         ,P_ORG_ID             IN  NUMBER                                       *
   *                                                                                                *
   *   REVISIONS:                                                                                   *
   *   Ver        Date        Author                     Description                                *
   *   ---------  ----------  ---------------         -------------------------                     *
   *   2.0        05/25/2015 P.Vamshidhar             TMS#20150414-00173                            *
   *                                                  New BPA Impact Report - deploy CSV version    *
   *                                                                                                *
   /************************************************************************************************/


    PROCEDURE generate_csv_new (P_PO_NUMBER          IN  VARCHAR2
                           ,P_DC_MODE            IN  VARCHAR2
                           ,P_NOTES              IN  VARCHAR2
                           ,P_MST_ORG_ID         IN  NUMBER
                           ,P_ORG_ID             IN  NUMBER);
                            
  END XXWC_BPA_IMPACT_REPORT_PKG;