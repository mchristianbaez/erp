   /***************************************************************************************************************************
   -- File Name: XXWC_BPA_PRICE_ZONE_V_TEMP_TBL_T.sql
   --
   -- PURPOSE: Global temporary table to store BPA price zone data for BPA price zone form.
   -- HISTORY
   -- =========================================================================================================================
   -- =========================================================================================================================
   -- VERSION DATE          AUTHOR(S)       DESCRIPTION
   -- ------- -----------   --------------- -----------------------------------------------------------------------------------
   -- 1.0     01-Apr-2015   P.Vamshidhar    Initial version.
   --                                       TMS#20150414-00173-2015 PLM - Implement Cost Management tool enhancements
   --                                       Table will be used in BPA Price zone form.           
   ***************************************************************************************************************************/
CREATE TABLE XXWC.XXWC_BPA_PRICE_ZONE_V_TEMP_TBL
(
  PO_NUMBER                    VARCHAR2(20 BYTE),
  VENDOR_NAME                  VARCHAR2(240 BYTE),
  VENDOR_NUM                   VARCHAR2(30 BYTE),
  VENDOR_SITE_CODE             VARCHAR2(15 BYTE),
  ITEM_NUMBER                  VARCHAR2(40 BYTE),
  ITEM_DESCRIPTION             VARCHAR2(240 BYTE),
  VENDOR_ITEM_NUMBER           VARCHAR2(255 BYTE),
  EXPIRATION_DATE              DATE,
  NATIONAL_PRICE               NUMBER,
  NATIONAL_QUANTITY            NUMBER,
  NATIONAL_BREAK               NUMBER,
  PROMO_PRICE                  NUMBER,
  PROMO_START_DATE             DATE,
  PROMO_END_DATE               DATE,
  PRICE_ZONE_PRICE1            NUMBER,
  PRICE_ZONE_QUANTITY1         NUMBER,
  PRICE_ZONE_QUANTITY_PRICE1   NUMBER,
  PRICE_ZONE_PRICE2            NUMBER,
  PRICE_ZONE_QUANTITY2         NUMBER,
  PRICE_ZONE_QUANTITY_PRICE2   NUMBER,
  PRICE_ZONE_PRICE3            NUMBER,
  PRICE_ZONE_QUANTITY3         NUMBER,
  PRICE_ZONE_QUANTITY_PRICE3   NUMBER,
  PRICE_ZONE_PRICE4            NUMBER,
  PRICE_ZONE_QUANTITY4         NUMBER,
  PRICE_ZONE_QUANTITY_PRICE4   NUMBER,
  PRICE_ZONE_PRICE5            NUMBER,
  PRICE_ZONE_QUANTITY5         NUMBER,
  PRICE_ZONE_QUANTITY_PRICE5   NUMBER,
  PRICE_ZONE_PRICE6            NUMBER,
  PRICE_ZONE_QUANTITY6         NUMBER,
  PRICE_ZONE_QUANTITY_PRICE6   NUMBER,
  PRICE_ZONE_PRICE7            NUMBER,
  PRICE_ZONE_QUANTITY7         NUMBER,
  PRICE_ZONE_QUANTITY_PRICE7   NUMBER,
  PRICE_ZONE_PRICE8            NUMBER,
  PRICE_ZONE_QUANTITY8         NUMBER,
  PRICE_ZONE_QUANTITY_PRICE8   NUMBER,
  PRICE_ZONE_PRICE9            NUMBER,
  PRICE_ZONE_QUANTITY9         NUMBER,
  PRICE_ZONE_QUANTITY_PRICE9   NUMBER,
  PRICE_ZONE_PRICE10           NUMBER,
  PRICE_ZONE_QUANTITY10        NUMBER,
  PRICE_ZONE_QUANTITY_PRICE10  NUMBER,
  PRICE_ZONE_PRICE11           NUMBER,
  PRICE_ZONE_QUANTITY11        NUMBER,
  PRICE_ZONE_QUANTITY_PRICE11  NUMBER,
  PRICE_ZONE_PRICE12           NUMBER,
  PRICE_ZONE_QUANTITY12        NUMBER,
  PRICE_ZONE_QUANTITY_PRICE12  NUMBER,
  PRICE_ZONE_PRICE13           NUMBER,
  PRICE_ZONE_QUANTITY13        NUMBER,
  PRICE_ZONE_QUANTITY_PRICE13  NUMBER,
  PRICE_ZONE_PRICE14           NUMBER,
  PRICE_ZONE_QUANTITY14        NUMBER,
  PRICE_ZONE_QUANTITY_PRICE14  NUMBER,
  PRICE_ZONE_PRICE15           NUMBER,
  PRICE_ZONE_QUANTITY15        NUMBER,
  PRICE_ZONE_QUANTITY_PRICE15  NUMBER,
  PRICE_ZONE_PRICE16           NUMBER,
  PRICE_ZONE_QUANTITY16        NUMBER,
  PRICE_ZONE_QUANTITY_PRICE16  NUMBER,
  PRICE_ZONE_PRICE17           NUMBER,
  PRICE_ZONE_QUANTITY17        NUMBER,
  PRICE_ZONE_QUANTITY_PRICE17  NUMBER,
  PRICE_ZONE_PRICE18           NUMBER,
  PRICE_ZONE_QUANTITY18        NUMBER,
  PRICE_ZONE_QUANTITY_PRICE18  NUMBER,
  PRICE_ZONE_PRICE19           NUMBER,
  PRICE_ZONE_QUANTITY19        NUMBER,
  PRICE_ZONE_QUANTITY_PRICE19  NUMBER,
  PRICE_ZONE_PRICE20           NUMBER,
  PRICE_ZONE_QUANTITY20        NUMBER,
  PRICE_ZONE_QUANTITY_PRICE20  NUMBER,
  PRICE_ZONE_PRICE21           NUMBER,
  PRICE_ZONE_QUANTITY21        NUMBER,
  PRICE_ZONE_QUANTITY_PRICE21  NUMBER,
  PRICE_ZONE_PRICE22           NUMBER,
  PRICE_ZONE_QUANTITY22        NUMBER,
  PRICE_ZONE_QUANTITY_PRICE22  NUMBER,
  PRICE_ZONE_PRICE23           NUMBER,
  PRICE_ZONE_QUANTITY23        NUMBER,
  PRICE_ZONE_QUANTITY_PRICE23  NUMBER,
  PRICE_ZONE_PRICE24           NUMBER,
  PRICE_ZONE_QUANTITY24        NUMBER,
  PRICE_ZONE_QUANTITY_PRICE24  NUMBER,
  PRICE_ZONE_PRICE25           NUMBER,
  PRICE_ZONE_QUANTITY25        NUMBER,
  PRICE_ZONE_QUANTITY_PRICE25  NUMBER,
  PRICE_ZONE_PRICE26           NUMBER,
  PRICE_ZONE_QUANTITY26        NUMBER,
  PRICE_ZONE_QUANTITY_PRICE26  NUMBER,
  PRICE_ZONE_PRICE27           NUMBER,
  PRICE_ZONE_QUANTITY27        NUMBER,
  PRICE_ZONE_QUANTITY_PRICE27  NUMBER,
  PRICE_ZONE_PRICE28           NUMBER,
  PRICE_ZONE_QUANTITY28        NUMBER,
  PRICE_ZONE_QUANTITY_PRICE28  NUMBER,
  PRICE_ZONE_PRICE29           NUMBER,
  PRICE_ZONE_QUANTITY29        NUMBER,
  PRICE_ZONE_QUANTITY_PRICE29  NUMBER,
  PRICE_ZONE_PRICE30           NUMBER,
  PRICE_ZONE_QUANTITY30        NUMBER,
  PRICE_ZONE_QUANTITY_PRICE30  NUMBER,
  PO_HEADER_ID                 NUMBER,
  INVENTORY_ITEM_ID            NUMBER,
  VENDOR_ID                    NUMBER,
  VENDOR_SITE_ID               NUMBER,
  VENDOR_CONTACT_ID            NUMBER,
  IMPLEMENT_DATE               DATE,
  PRICE_ZONE_QUANTITY          NUMBER,
  ORG_ID                       NUMBER,
  CREATED_BY                   NUMBER,
  INSERT_DATE                  DATE
);

GRANT ALL ON XXWC.XXWC_BPA_PRICE_ZONE_V_TEMP_TBL TO APPS;

CREATE SYNONYM APPS.XXWC_BPA_PRICE_ZONE_V_TEMP_TBL FOR XXWC.XXWC_BPA_PRICE_ZONE_V_TEMP_TBL;
