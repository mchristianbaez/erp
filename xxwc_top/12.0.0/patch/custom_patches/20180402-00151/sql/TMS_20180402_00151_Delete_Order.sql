/******************************************************************************
  $Header TMS_ 20180402_00151_Data_Fix.sql $
  Module Name:Data Fix script for  20180402-00151

  PURPOSE: Data fix script for  20180402_00151 Delete Counter Order

  REVISIONS:
  Ver        Date         Author                Description
  ---------  -----------  ------------------    -------------------------------
  1.0        10-Apr-2018  Krishna Kumar         20180402-00151

*******************************************************************************/

ALTER SESSION SET CURRENT_SCHEMA=apps;
/
BEGIN
   mo_global.set_policy_context ('S', '162');
END;
/
SET serveroutput ON SIZE 500000;
DECLARE

BEGIN

DELETE FROM apps.oe_order_headers_all WHERE order_number = 27291577;

dbms_output.put_line('Number of Rows Deleted '||SQL%ROWCOUNT);

COMMIT;

EXCEPTION
    WHEN OTHERS THEN
      ROLLBACK;
      dbms_output.put_line(substr(sqlerrm, 1, 240));
END;
/