/*
Ticket#                            Date         Author            Notes
-------------------------------------------------------------------------------------------------------------------------------------------------------------------
TMS  20170531-00077 / ESMS 563286  05/28/2017   Balaguru Seshadri Add new field hds_non_history
*/

DECLARE
   l_db_name       VARCHAR2 (20) := NULL;
   l_path_inb     VARCHAR2 (240) := NULL;
   l_path_oub   VARCHAR2 (240) := NULL;
   l_sql           VARCHAR2 (240) := NULL;
--
BEGIN
   --
   DBMS_OUTPUT.put_line (' ');
   DBMS_OUTPUT.put_line ('Begin : alter table - add new field HDS_NON_HISTORY');
   DBMS_OUTPUT.put_line (' ');     
   l_sql :='ALTER TABLE XXCUS.XXCUS_OPN_FRU_LOC_ALL ADD (HDS_NON_HISTORY VARCHAR2(1 BYTE))';
   -- 
   EXECUTE IMMEDIATE l_sql;
   --
   DBMS_OUTPUT.put_line (' ');
   DBMS_OUTPUT.put_line ('End : alter table - add new field HDS_NON_HISTORY');
   DBMS_OUTPUT.put_line (' ');  
EXCEPTION
 WHEN OTHERS THEN
  DBMS_OUTPUT.PUT_LINE('OUTER BLOCK : '||SQLERRM);
END;
/