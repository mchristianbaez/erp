/*****************************************************************************************************
Copyright (c) 2017 HD Supply Group
All rights reserved.
******************************************************************************************************
$Header TMS_20170113-00103_req_line_qty_upd.sql $
Module Name: TMS_20170113-00103_req_line_qty_upd.sql
PURPOSE:
REVISIONS:
Ver        Date        Author               Description
---------  ----------  ---------------      -------------------------
1.0        05/31/2017  Pattabhi Avula       Initial Version(TMS#20170113-00103)
*****************************************************************************************************/
SET SERVEROUTPUT ON SIZE 100000;
BEGIN

DBMS_OUTPUT.put_line ('TMS: 20170113-00103    , Before Update');

UPDATE  apps.po_requisition_lines_all
   SET  quantity              = 0
 WHERE  requisition_line_id   = 10112289 
   AND  requisition_header_id = 3381905;
   
      DBMS_OUTPUT.put_line (
         'TMS: 20170113-00103  po_requisition_lines_all table Updated record count: '
      || SQL%ROWCOUNT);

           
UPDATE apps.po_req_distributions_all
   SET req_line_quantity   = 0
 WHERE requisition_line_id = 10112289
   AND distribution_id     = 10161677;
   
        DBMS_OUTPUT.put_line (
         'TMS: 20170113-00103  po_req_distributions_all Table Updated record count: '
      || SQL%ROWCOUNT);

   COMMIT;
   
   DBMS_OUTPUT.put_line ('TMS: 20170113-00103     , End Update');
EXCEPTION
   WHEN OTHERS
   THEN
      ROLLBACK;
      DBMS_OUTPUT.put_line ('TMS: 20170113-00103 , Errors : ' || SQLERRM);
END;
/
