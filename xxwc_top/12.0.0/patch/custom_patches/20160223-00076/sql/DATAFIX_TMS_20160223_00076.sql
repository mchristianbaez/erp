/*
 TMS: 20160223-00076  Unable to receive Return order #19724853
 created: 2/23/2016
 Created by: Ram Talluri
 Last Updated: 2/23/2016
  */
SET SERVEROUTPUT ON SIZE 1000000


BEGIN
   DBMS_OUTPUT.put_line ('Before update');

   UPDATE apps.oe_order_lines_all
      SET project_id = 1                                 --Drop Project Number
    WHERE line_id IN (65068706, 65068642, 65068695) AND header_id = 39732641;

   DBMS_OUTPUT.put_line ('After update');

   DBMS_OUTPUT.put_line ('Records update-' || SQL%ROWCOUNT);

   COMMIT;

EXCEPTION
   WHEN OTHERS
   THEN
      DBMS_OUTPUT.put_line ('Unable to update ' || SQLERRM);
      ROLLBACK; 
END;
/