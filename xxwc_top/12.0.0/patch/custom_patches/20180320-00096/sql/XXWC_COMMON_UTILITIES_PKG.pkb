CREATE OR REPLACE PACKAGE BODY apps.XXWC_COMMON_UTILITIES_PKG
/***********************************************************************************************************
FILE NAME: APPS.XXWC_COMMON_UTILITIES_PKG.pkb

PROGRAM TYPE: PL/SQL Package body

PURPOSE: To Changing the file permission to particular directory file

HISTORY
=============================================================================================================
VERSION DATE          AUTHOR(S)       DESCRIPTION
------- -----------   --------------- -----------------------------------------------------------------------
1.0     03/27/2018    Pattabhi Avula  TMS#20171101-00037 Initial version.
1.1     03/29/2018    P.Vamshidhar    TMS#Add logic to remove duplicate emails being sent for SOAs and ASNs
**************************************************************************************************************/
AS
   l_package_name   VARCHAR2 (100)
                       := 'XXWC_COMMON_UTILITIES_PKG.FILE_PERMISSIONS_CHNG';
   l_distro_list    VARCHAR2 (100) DEFAULT 'WC-ITDevelopment-U1@HDSupply.com';

   PROCEDURE FILE_PERMISSIONS_CHNG (p_dir_path    IN VARCHAR2,
                                    p_file_name   IN VARCHAR2)
   IS
      lvc_command   VARCHAR2 (500);
      l_result      VARCHAR2 (500);
   BEGIN
      lvc_command := 'chmod 777' || ' ' || p_dir_path || '/' || p_file_name;

      BEGIN
         SELECT xxwc_edi_iface_pkg.xxwc_oscommand_run (lvc_command)
           INTO l_result
           FROM DUAL;
      EXCEPTION
         WHEN OTHERS
         THEN
            fnd_file.put_line (fnd_file.LOG,
                               'File permission Result is ' || l_result);
      END;
   EXCEPTION
      WHEN OTHERS
      THEN
         fnd_file.put_line (fnd_file.LOG, 'Error is ' || SQLERRM);
   END;

   /*****************************************************************************************************************
   Procedure Name: XXWC_OM_SOA_EMAIL_PRC

   PROGRAM TYPE: Procedure name

   PURPOSE: To remove duplicate email addresses.  This procedure being used in sales order receipt and pack slip rdfs

   HISTORY
   ==================================================================================================================
   VERSION DATE          AUTHOR(S)       DESCRIPTION
   ------- -----------   --------------- ----------------------------------------------------------------------------
   1.1     03/29/2018    P.Vamshidhar    Initial Version
                                         TMS#Add logic to remove duplicate emails being sent for SOAs and ASNs
   ******************************************************************************************************************/
   PROCEDURE XXWC_OM_SOA_EMAIL_PRC (p_to_email_addr   IN     VARCHAR2,
                                    p_cc_email_addr   IN     VARCHAR2,
                                    p_request_id      IN     NUMBER,
                                    x_to_email_addr      OUT VARCHAR2,
                                    x_cc_email_addr      OUT VARCHAR2)
   IS
      PROCEDURE email (p_email_add   IN     VARCHAR2,
                       x_email_add      OUT VARCHAR2,
                       p_flag               VARCHAR2)
      IS
         CURSOR CUR_EMAIL
         IS
            SELECT DISTINCT a.EMAIL_ADDRESS email_address
              FROM XXWC.XXWC_OM_SOA_EMAIL_GTT a
             WHERE a.FLAG = p_flag AND p_flag = 'TO'
            UNION ALL
            SELECT DISTINCT a.EMAIL_ADDRESS email_address
              FROM XXWC.XXWC_OM_SOA_EMAIL_GTT a
             WHERE     FLAG = p_flag
                   AND p_flag = 'CC'
                   AND NOT EXISTS
                          (SELECT 1
                             FROM XXWC.XXWC_OM_SOA_EMAIL_GTT b
                            WHERE     b.EMAIL_ADDRESS = a.email_address
                                  AND b.flag = 'TO');


         from_email   VARCHAR2 (1000);
         new_email    VARCHAR2 (1000);
         l_email      VARCHAR2 (1000);
         ln_cnt       NUMBER := 1;
      BEGIN
         from_email := p_email_add;
         from_email := REPLACE (from_email, ';', ',');

         LOOP
            EXIT WHEN ln_cnt = 0;
            l_email := NULL;
            l_email := SUBSTR (from_email, 1, INSTR (from_email, ',', 1) - 1);

            INSERT INTO XXWC.XXWC_OM_SOA_EMAIL_GTT (EMAIL_ADDRESS, FLAG)
                 VALUES (l_email, P_FLAG);

            ln_cnt := ln_cnt + 1;
            from_email := SUBSTR (from_email, INSTR (from_email, ',', 1) + 1);

            IF INSTR (from_email, ',', 1) = 0
            THEN
               INSERT INTO XXWC.XXWC_OM_SOA_EMAIL_GTT (EMAIL_ADDRESS, flag)
                    VALUES (from_email, p_flag);

               ln_cnt := 0;
            END IF;
         END LOOP;

         new_email := NULL;

         FOR REC_EMAIL IN CUR_EMAIL
         LOOP
            IF new_email IS NOT NULL
            THEN
               new_email := new_email || ',' || REC_EMAIL.EMAIL_ADDRESS;
            ELSE
               new_email := REC_EMAIL.EMAIL_ADDRESS;
            END IF;
         END LOOP;

         x_email_add := new_email;
      EXCEPTION
         WHEN OTHERS
         THEN
            x_email_add := p_email_add;
            xxcus_error_pkg.xxcus_error_main_api (
               p_called_from         => 'xxwc_om_soa_email_prc',
               p_calling             => 'xxwc_om_soa_email_prc child procedure',
               p_request_id          => p_request_id,
               p_ora_error_msg       => SQLERRM,
               p_error_desc          => 'p_email_add:' || p_email_add,
               p_distribution_list   => 'WC-ITDEVALERTS-U1@HDSupply.com',
               p_module              => 'XXWC');
      END;
   BEGIN
      IF p_to_email_addr IS NOT NULL
      THEN
         email (LOWER (p_to_email_addr), x_to_email_addr, 'TO');
      END IF;

      IF p_cc_email_addr IS NOT NULL
      THEN
         email (LOWER (p_cc_email_addr), x_cc_email_addr, 'CC');
      END IF;
   EXCEPTION
      WHEN OTHERS
      THEN
         xxcus_error_pkg.xxcus_error_main_api (
            p_called_from         => 'XXWC_COMMON_UTILITIES_PKG.XXWC_OM_SOA_EMAIL_PRC',
            p_calling             => 'xxwc_om_soa_email_prc',
            p_request_id          => p_request_id,
            p_ora_error_msg       => SQLERRM,
            p_error_desc          =>    'To email:'
                                     || p_to_email_addr
                                     || ' cc Email:'
                                     || p_cc_email_addr,
            p_distribution_list   => 'WC-ITDEVALERTS-U1@HDSupply.com',
            p_module              => 'XXWC');
         x_to_email_addr := p_to_email_addr;
         x_cc_email_addr := p_cc_email_addr;
   END;
END XXWC_COMMON_UTILITIES_PKG;
/