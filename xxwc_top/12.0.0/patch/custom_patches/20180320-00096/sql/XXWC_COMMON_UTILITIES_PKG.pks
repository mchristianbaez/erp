CREATE OR REPLACE PACKAGE apps.XXWC_COMMON_UTILITIES_PKG
AS
   /********************************************************************************
   FILE NAME: APPS.XXWC_COMMON_UTILITIES_PKG.pks

   PROGRAM TYPE: PL/SQL Package spec

   PURPOSE: To Changing the file permission to particular directory file

   HISTORY
   ===============================================================================
   VERSION DATE          AUTHOR(S)       DESCRIPTION
   ------- -----------   --------------- -----------------------------------------
   1.0     03/27/2018    Pattabhi Avula  TMS#20171101-00037 Initial version.
   1.1     03/29/2018    P.Vamshidhar    TMS#Add logic to remove duplicate emails being sent for SOAs and ASNs
   *******************************************************************************/

   PROCEDURE FILE_PERMISSIONS_CHNG (p_dir_path    IN VARCHAR2,
                                    p_file_name   IN VARCHAR2);
                                    
   -- Added below procedure in Rev 1.1                                   

   PROCEDURE XXWC_OM_SOA_EMAIL_PRC (p_to_email_addr   IN     VARCHAR2,
                                    p_cc_email_addr   IN     VARCHAR2,
                                    p_request_id      IN     NUMBER,
                                    x_to_email_addr      OUT VARCHAR2,
                                    x_cc_email_addr      OUT VARCHAR2);
END XXWC_COMMON_UTILITIES_PKG;
/