--Report Name            : HDS GL Journals to AP Payments
--Import Instructions : 1. If RTF template needs to be uploaded to report automatically then move the RTF template to physical path of APPS_DATA_FILE_DIR oracle directory. Check dba_directories view to get the physical path of oracle directory. 
--                         (If you want to use any other oracle directory other than APPS_DATA_FILE_DIR then replace APPS_DATA_FILE_DIR with your oracle directory name below.)
--                      2. Make sure XXEIS schema has read/write permissions to above used oracle directory and give full permissions to the directory at OS level.
--                      3. Run this file in XXEIS user from SQL*Plus or any tool like Toad, SQL Navigator. 
exec xxeis.eis_rsc_ins.g_rtf_dir := 'APPS_DATA_FILE_DIR' ;
prompt Creating Object Data EIS_GL_JRNL_TO_AP_PMTS_V
set scan off define off
DECLARE
mod_exist VARCHAR2(1);
BEGIN 
mod_exist := XXEIS.EIS_RSC_INSERT.check_if_module_exist(101);
IF mod_exist = 'Y' THEN 
--Inserting Object EIS_GL_JRNL_TO_AP_PMTS_V
xxeis.eis_rsc_ins.v( 'EIS_GL_JRNL_TO_AP_PMTS_V',101,'This view shows details on the general ledger journal and the corresponding payment transactions from AP.','','','','XXEIS_RS_ADMIN','XXEIS','EIS GL Journals To AP Payments','EGLAP','','','VIEW','US','Y','','');
--Delete Object Columns for EIS_GL_JRNL_TO_AP_PMTS_V
xxeis.eis_rsc_utility.delete_view_rows('EIS_GL_JRNL_TO_AP_PMTS_V',101,FALSE);
--Inserting Object Columns for EIS_GL_JRNL_TO_AP_PMTS_V
xxeis.eis_rsc_ins.vc( 'EIS_GL_JRNL_TO_AP_PMTS_V','JE_CURRENCY',101,'Currency','JE_CURRENCY','','','','XXEIS_RS_ADMIN','VARCHAR2','GL_JE_HEADERS','CURRENCY_CODE','Je Currency','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_GL_JRNL_TO_AP_PMTS_V','ACCRUAL_REV_CHANGE_SIGN',101,'Type of reversal (Change Sign or Switch Dr/Cr)','ACCRUAL_REV_CHANGE_SIGN','','','','XXEIS_RS_ADMIN','VARCHAR2','GL_JE_HEADERS','ACCRUAL_REV_CHANGE_SIGN_FLAG','Accrual Rev Change Sign','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_GL_JRNL_TO_AP_PMTS_V','JE_CURR_CONV_RATE',101,'Currency exchange rate','JE_CURR_CONV_RATE','','~T~D~2','','XXEIS_RS_ADMIN','NUMBER','GL_JE_HEADERS','CURRENCY_CONVERSION_RATE','Je Curr Conv Rate','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_GL_JRNL_TO_AP_PMTS_V','JE_CURR_CONV_TYPE',101,'Type of currency exchange rate','JE_CURR_CONV_TYPE','','','','XXEIS_RS_ADMIN','VARCHAR2','GL_JE_HEADERS','CURRENCY_CONVERSION_TYPE','Je Curr Conv Type','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_GL_JRNL_TO_AP_PMTS_V','JE_CURR_CONV_DATE',101,'Currency conversion date','JE_CURR_CONV_DATE','','','','XXEIS_RS_ADMIN','DATE','GL_JE_HEADERS','CURRENCY_CONVERSION_DATE','Je Curr Conv Date','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_GL_JRNL_TO_AP_PMTS_V','JE_EXTERNAL_REF',101,'Extra reference column','JE_EXTERNAL_REF','','','','XXEIS_RS_ADMIN','VARCHAR2','GL_JE_HEADERS','EXTERNAL_REFERENCE','Je External Ref','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_GL_JRNL_TO_AP_PMTS_V','JE_ACCOUNTED_DR',101,'Journal entry line debit amount in base currency','JE_ACCOUNTED_DR','','~T~D~2','','XXEIS_RS_ADMIN','NUMBER','GL_JE_LINES','ACCOUNTED_DR','Je Accounted Dr','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_GL_JRNL_TO_AP_PMTS_V','JE_ACCOUNTED_CR',101,'Journal entry line credit amount in base currency','JE_ACCOUNTED_CR','','~T~D~2','','XXEIS_RS_ADMIN','NUMBER','GL_JE_LINES','ACCOUNTED_CR','Je Accounted Cr','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_GL_JRNL_TO_AP_PMTS_V','JE_LINE_DESCRIPTION',101,'Journal entry line description','JE_LINE_DESCRIPTION','','','','XXEIS_RS_ADMIN','VARCHAR2','GL_JE_LINES','DESCRIPTION','Je Line Description','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_GL_JRNL_TO_AP_PMTS_V','CHECK_DATE',101,'Payment date','CHECK_DATE~CHECK_DATE','','','','XXEIS_RS_ADMIN','DATE','AP_CHECKS_ALL','CHECK_DATE','Check Date','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_GL_JRNL_TO_AP_PMTS_V','GL_SL_LINK_TABLE',101,'Table containing associated subledger data','GL_SL_LINK_TABLE','','','','XXEIS_RS_ADMIN','VARCHAR2','GL_JE_LINES','GL_SL_LINK_TABLE','Gl Sl Link Table','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_GL_JRNL_TO_AP_PMTS_V','JE_CREATED_BY',101,'Standard Who column','JE_CREATED_BY','','','','XXEIS_RS_ADMIN','VARCHAR2','GL_JE_HEADERS','CREATED_BY','Je Created By','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_GL_JRNL_TO_AP_PMTS_V','JE_BALANCED',101,'Balanced journal entry flag','JE_BALANCED','','','','XXEIS_RS_ADMIN','VARCHAR2','GL_JE_HEADERS','BALANCED_JE_FLAG','Je Balanced','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_GL_JRNL_TO_AP_PMTS_V','BUDGET_DESCRIPTION',101,'Budget version description','BUDGET_DESCRIPTION','','','','XXEIS_RS_ADMIN','VARCHAR2','GL_BUDGET_VERSIONS','DESCRIPTION','Budget Description','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_GL_JRNL_TO_AP_PMTS_V','INVOICE_NUM',101,'Journal import reference column','INVOICE_NUM','','','','XXEIS_RS_ADMIN','VARCHAR2','AP_AE_LINES_ALL','REFERENCE5','Invoice Num','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_GL_JRNL_TO_AP_PMTS_V','JE_STATUS',101,'Journal entry header status lookup code','JE_STATUS','','','','XXEIS_RS_ADMIN','VARCHAR2','GL_JE_HEADERS','STATUS','Je Status','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_GL_JRNL_TO_AP_PMTS_V','JE_TYPE',101,'Balance type (Actual, Budget, or Encumbrance)','JE_TYPE','','','','XXEIS_RS_ADMIN','VARCHAR2','GL_JE_HEADERS','ACTUAL_FLAG','Je Type','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_GL_JRNL_TO_AP_PMTS_V','JE_POSTED_DATE',101,'Date journal entry header was posted','JE_POSTED_DATE','','','','XXEIS_RS_ADMIN','DATE','GL_JE_HEADERS','POSTED_DATE','Je Posted Date','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_GL_JRNL_TO_AP_PMTS_V','JE_CREATION_DATE',101,'Standard Who column','JE_CREATION_DATE','','','','XXEIS_RS_ADMIN','DATE','GL_JE_HEADERS','CREATION_DATE','Je Creation Date','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_GL_JRNL_TO_AP_PMTS_V','JE_SOURCE',101,'Journal entry source user defined name','JE_SOURCE','','','','XXEIS_RS_ADMIN','VARCHAR2','GL_JE_SOURCES_TL','USER_JE_SOURCE_NAME','Je Source','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_GL_JRNL_TO_AP_PMTS_V','JE_CATEGORY',101,'JE Category','JE_CATEGORY','','','','XXEIS_RS_ADMIN','VARCHAR2','GL_JE_CATEGORIES','USER_JE_CATEGORY_NAME','Je Category','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_GL_JRNL_TO_AP_PMTS_V','JE_PERIOD',101,'Accounting period','JE_PERIOD','','','GL Periods LOV','XXEIS_RS_ADMIN','VARCHAR2','GL_JE_HEADERS','PERIOD_NAME','Je Period','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_GL_JRNL_TO_AP_PMTS_V','JE_NAME',101,'Journal entry header name','JE_NAME','','','GL AP Payment Journals LOV','XXEIS_RS_ADMIN','VARCHAR2','GL_JE_HEADERS','NAME','Je Name','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_GL_JRNL_TO_AP_PMTS_V','JE_DESCRIPTION',101,'Journal entry description','JE_DESCRIPTION','','','','XXEIS_RS_ADMIN','VARCHAR2','GL_JE_HEADERS','DESCRIPTION','Je Description','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_GL_JRNL_TO_AP_PMTS_V','BUDGET_NAME',101,'Budget Name','BUDGET_NAME','','','','XXEIS_RS_ADMIN','VARCHAR2','GL_BUDGET_VERSIONS','BUDGET_NAME','Budget Name','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_GL_JRNL_TO_AP_PMTS_V','BUDGET_TYPE',101,'Budget type(only STANDARD is used)','BUDGET_TYPE','','','','XXEIS_RS_ADMIN','VARCHAR2','GL_BUDGET_VERSIONS','BUDGET_TYPE','Budget Type','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_GL_JRNL_TO_AP_PMTS_V','GL_ACCOUNT_STRING',101,'GL Account String','GL_ACCOUNT_STRING','','','GL Accounts LOV','XXEIS_RS_ADMIN','VARCHAR2','CALCULATION COLUMN','CALCULATION COLUMN','Gl Account String','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_GL_JRNL_TO_AP_PMTS_V','BATCH_NAME',101,'Name of journal entry batch','BATCH_NAME','','','GL AP Payment Batch Names LOV','XXEIS_RS_ADMIN','VARCHAR2','GL_JE_BATCHES','NAME','Batch Name','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_GL_JRNL_TO_AP_PMTS_V','BATCH_DESCRIPTION',101,'Journal entry batch description','BATCH_DESCRIPTION','','','','XXEIS_RS_ADMIN','VARCHAR2','GL_JE_BATCHES','DESCRIPTION','Batch Description','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_GL_JRNL_TO_AP_PMTS_V','BUDGET_STATUS',101,'Version status lookup code','BUDGET_STATUS','','','','XXEIS_RS_ADMIN','VARCHAR2','GL_BUDGET_VERSIONS','STATUS','Budget Status','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_GL_JRNL_TO_AP_PMTS_V','ACCRUAL_REVERSAL',101,'Reversed journal entry flag','ACCRUAL_REVERSAL','','','','XXEIS_RS_ADMIN','VARCHAR2','GL_JE_HEADERS','ACCRUAL_REV_FLAG','Accrual Reversal','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_GL_JRNL_TO_AP_PMTS_V','JE_MULTI_BAL_SEG',101,'Multiple balancing segment flag','JE_MULTI_BAL_SEG','','','','XXEIS_RS_ADMIN','VARCHAR2','GL_JE_HEADERS','MULTI_BAL_SEG_FLAG','Je Multi Bal Seg','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_GL_JRNL_TO_AP_PMTS_V','ACCRUAL_REV_EFFECTIVE_DATE',101,'Reversed journal entry effective date','ACCRUAL_REV_EFFECTIVE_DATE','','','','XXEIS_RS_ADMIN','DATE','GL_JE_HEADERS','ACCRUAL_REV_EFFECTIVE_DATE','Accrual Rev Effective Date','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_GL_JRNL_TO_AP_PMTS_V','TRANSFER_DATE_FROM_SLA_TO_GL',101,'General Ledger transfer date','TRANSFER_DATE_FROM_SLA_TO_GL~TRANSFER_DATE_FROM_SLA_TO_GL','','','','XXEIS_RS_ADMIN','DATE','XLA_AE_HEADERS','GL_TRANSFER_DATE','Transfer Date From Sla To Gl','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_GL_JRNL_TO_AP_PMTS_V','SLA_ACCOUNT',101,'Sla Account','SLA_ACCOUNT~SLA_ACCOUNT','','','','XXEIS_RS_ADMIN','VARCHAR2','CALCULATION COLUMN','CALCULATION COLUMN','Sla Account','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_GL_JRNL_TO_AP_PMTS_V','SLA_ACCOUNTED_CR',101,'Sla Accounted Cr','SLA_ACCOUNTED_CR~SLA_ACCOUNTED_CR','','~T~D~2','','XXEIS_RS_ADMIN','NUMBER','XLA_DISTRIBUTION_LINKS','UNROUNDED_ACCOUNTED_CR','Sla Accounted Cr','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_GL_JRNL_TO_AP_PMTS_V','SLA_ACCOUNTED_DR',101,'Sla Accounted Dr','SLA_ACCOUNTED_DR~SLA_ACCOUNTED_DR','','~T~D~2','','XXEIS_RS_ADMIN','NUMBER','XLA_DISTRIBUTION_LINKS','UNROUNDED_ACCOUNTED_DR','Sla Accounted Dr','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_GL_JRNL_TO_AP_PMTS_V','SLA_ACCOUNTING_DATE',101,'Accounting date','SLA_ACCOUNTING_DATE~SLA_ACCOUNTING_DATE','','','','XXEIS_RS_ADMIN','DATE','XLA_AE_HEADERS','ACCOUNTING_DATE','Sla Accounting Date','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_GL_JRNL_TO_AP_PMTS_V','SLA_ACCOUNT_CLASS',101,'Sla Account Class','SLA_ACCOUNT_CLASS~SLA_ACCOUNT_CLASS','','','','XXEIS_RS_ADMIN','VARCHAR2','XLA_AE_LINES','ACCOUNTING_CLASS_CODE','Sla Account Class','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_GL_JRNL_TO_AP_PMTS_V','SLA_CODE_COMBINATION_ID',101,'Sla Code Combination Id','SLA_CODE_COMBINATION_ID~SLA_CODE_COMBINATION_ID','','','','XXEIS_RS_ADMIN','NUMBER','XLA_AE_LINES','CODE_COMBINATION_ID','Sla Code Combination Id','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_GL_JRNL_TO_AP_PMTS_V','SLA_ENTERED_CR',101,'Unrounded Entered Credit Amount for the journal line','SLA_ENTERED_CR~SLA_ENTERED_CR','','~T~D~2','','XXEIS_RS_ADMIN','NUMBER','XLA_DISTRIBUTION_LINKS','UNROUNDED_ENTERED_CR','Sla Entered Cr','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_GL_JRNL_TO_AP_PMTS_V','SLA_ENTERED_DR',101,'Unrounded Entered Debit Amount for the journal line','SLA_ENTERED_DR~SLA_ENTERED_DR','','~T~D~2','','XXEIS_RS_ADMIN','NUMBER','XLA_DISTRIBUTION_LINKS','UNROUNDED_ENTERED_DR','Sla Entered Dr','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_GL_JRNL_TO_AP_PMTS_V','SLA_EVENT_TYPE',101,'Event type code','SLA_EVENT_TYPE~SLA_EVENT_TYPE','','','','XXEIS_RS_ADMIN','VARCHAR2','XLA_AE_HEADERS','EVENT_TYPE_CODE','Sla Event Type','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_GL_JRNL_TO_AP_PMTS_V','ACCRUAL_REV_PERIOD_NAME',101,'Reversed journal entry reversal period','ACCRUAL_REV_PERIOD_NAME','','','','XXEIS_RS_ADMIN','VARCHAR2','GL_JE_HEADERS','ACCRUAL_REV_PERIOD_NAME','Accrual Rev Period Name','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_GL_JRNL_TO_AP_PMTS_V','ACCRUAL_REV_STATUS',101,'Reversed journal entry status','ACCRUAL_REV_STATUS','','','','XXEIS_RS_ADMIN','VARCHAR2','GL_JE_HEADERS','ACCRUAL_REV_STATUS','Accrual Rev Status','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_GL_JRNL_TO_AP_PMTS_V','JE_CATEGORY_NAME',101,'Je Category Name','JE_CATEGORY_NAME~JE_CATEGORY_NAME','','','','XXEIS_RS_ADMIN','VARCHAR2','GL_JE_CATEGORIES','JE_CATEGORY_NAME','Je Category Name','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_GL_JRNL_TO_AP_PMTS_V','JE_SOURCE_NAME',101,'Journal entry source name','JE_SOURCE_NAME~JE_SOURCE_NAME','','','','XXEIS_RS_ADMIN','VARCHAR2','GL_JE_SOURCES_TL','JE_SOURCE_NAME','Je Source Name','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_GL_JRNL_TO_AP_PMTS_V','BANK_ACCOUNT_NUM',101,'Suppliers bank account number for electronic payment purposes','BANK_ACCOUNT_NUM~BANK_ACCOUNT_NUM','','','','XXEIS_RS_ADMIN','VARCHAR2','AP_CHECKS_ALL','BANK_ACCOUNT_NUM','Bank Account Num','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_GL_JRNL_TO_AP_PMTS_V','CHECK_CLEARED_AMOUNT',101,'Payment cleared amount','CHECK_CLEARED_AMOUNT~CHECK_CLEARED_AMOUNT','','~T~D~2','','XXEIS_RS_ADMIN','NUMBER','AP_CHECKS_ALL','CLEARED_AMOUNT','Check Cleared Amount','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_GL_JRNL_TO_AP_PMTS_V','CHECK_CLEARED_DATE',101,'Payment cleared date','CHECK_CLEARED_DATE~CHECK_CLEARED_DATE','','','','XXEIS_RS_ADMIN','DATE','AP_CHECKS_ALL','CLEARED_DATE','Check Cleared Date','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_GL_JRNL_TO_AP_PMTS_V','CHECK_CURRENCY_CODE',101,'Currency code','CHECK_CURRENCY_CODE~CHECK_CURRENCY_CODE','','','','XXEIS_RS_ADMIN','VARCHAR2','AP_CHECKS_ALL','CURRENCY_CODE','Check Currency Code','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_GL_JRNL_TO_AP_PMTS_V','CHECK_DOC_SEQ_VALUE',101,'Voucher number (sequential numbering) for payment','CHECK_DOC_SEQ_VALUE~CHECK_DOC_SEQ_VALUE','','~T~D~2','','XXEIS_RS_ADMIN','NUMBER','AP_CHECKS_ALL','DOC_SEQUENCE_VALUE','Check Doc Seq Value','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_GL_JRNL_TO_AP_PMTS_V','CHECK_NUMBER',101,'Payment number','CHECK_NUMBER~CHECK_NUMBER','','','','XXEIS_RS_ADMIN','NUMBER','AP_CHECKS_ALL','CHECK_NUMBER','Check Number','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_GL_JRNL_TO_AP_PMTS_V','INOICE_DATE',101,'Invoice date','INOICE_DATE~INOICE_DATE','','','','XXEIS_RS_ADMIN','DATE','AP_INVOICES_ALL','INVOICE_DATE','Inoice Date','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_GL_JRNL_TO_AP_PMTS_V','INVOICE_AMOUNT',101,'Invoice amount','INVOICE_AMOUNT~INVOICE_AMOUNT','','~T~D~2','','XXEIS_RS_ADMIN','NUMBER','AP_INVOICES_ALL','INVOICE_AMOUNT','Invoice Amount','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_GL_JRNL_TO_AP_PMTS_V','INVOICE_CURRENCY_CODE',101,'Currency code of invoice','INVOICE_CURRENCY_CODE~INVOICE_CURRENCY_CODE','','','','XXEIS_RS_ADMIN','VARCHAR2','AP_INVOICES_ALL','INVOICE_CURRENCY_CODE','Invoice Currency Code','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_GL_JRNL_TO_AP_PMTS_V','INVOICE_DOC_SEQ_VALUE',101,'Voucher number (Sequential Numbering) for invoice','INVOICE_DOC_SEQ_VALUE~INVOICE_DOC_SEQ_VALUE','','~T~D~2','','XXEIS_RS_ADMIN','NUMBER','AP_INVOICES_ALL','DOC_SEQUENCE_VALUE','Invoice Doc Seq Value','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_GL_JRNL_TO_AP_PMTS_V','LEDGER_NAME',101,'Ledger name','LEDGER_NAME~LEDGER_NAME','','','','XXEIS_RS_ADMIN','VARCHAR2','GL_LEDGERS','NAME','Ledger Name','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_GL_JRNL_TO_AP_PMTS_V','VENDOR_NAME',101,'Supplier name','VENDOR_NAME~VENDOR_NAME','','','','XXEIS_RS_ADMIN','VARCHAR2','PO_VENDORS','VENDOR_NAME','Vendor Name','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_GL_JRNL_TO_AP_PMTS_V','VENDOR_SITE',101,'Vendor Site','VENDOR_SITE~VENDOR_SITE','','','','XXEIS_RS_ADMIN','VARCHAR2','XLA_AE_LINES','PARTY_SITE_ID','Vendor Site','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_GL_JRNL_TO_AP_PMTS_V','JE_LINE_NUM',101,'Journal entry line number','JE_LINE_NUM','','','','XXEIS_RS_ADMIN','NUMBER','GL_JE_LINES','JE_LINE_NUM','Je Line Num','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_GL_JRNL_TO_AP_PMTS_V','JE_EFFECTIVE_DATE',101,'Journal entry line effective date','JE_EFFECTIVE_DATE','','','','XXEIS_RS_ADMIN','DATE','GL_JE_LINES','EFFECTIVE_DATE','Je Effective Date','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_GL_JRNL_TO_AP_PMTS_V','JE_ENTERED_DR',101,'Journal entry line debit amount in entered currency','JE_ENTERED_DR','','~T~D~2','','XXEIS_RS_ADMIN','NUMBER','GL_JE_LINES','ENTERED_DR','Je Entered Dr','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_GL_JRNL_TO_AP_PMTS_V','JE_ENTERED_CR',101,'Journal entry line credit amount in entered currency','JE_ENTERED_CR','','~T~D~2','','XXEIS_RS_ADMIN','NUMBER','GL_JE_LINES','ENTERED_CR','Je Entered Cr','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_GL_JRNL_TO_AP_PMTS_V','SLA_ACCOUNTED_NET',101,'Sla Accounted Net','SLA_ACCOUNTED_NET','','~T~D~2','','XXEIS_RS_ADMIN','NUMBER','CALCULATION COLUMN','CALCULATION COLUMN','Sla Accounted Net','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_GL_JRNL_TO_AP_PMTS_V','SLA_ENTERED_NET',101,'Sla Entered Net','SLA_ENTERED_NET','','~T~D~2','','XXEIS_RS_ADMIN','NUMBER','CALCULATION COLUMN','CALCULATION COLUMN','Sla Entered Net','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_GL_JRNL_TO_AP_PMTS_V','AE_LINE_NUM',101,'Ae Line Num','AE_LINE_NUM~AE_LINE_NUM','','','','XXEIS_RS_ADMIN','NUMBER','XLA_AE_LINES','AE_LINE_NUM','Ae Line Num','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_GL_JRNL_TO_AP_PMTS_V','APPLICATION_ID',101,'Application Id','APPLICATION_ID~APPLICATION_ID','','','','XXEIS_RS_ADMIN','NUMBER','XLA_AE_LINES','APPLICATION_ID','Application Id','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_GL_JRNL_TO_AP_PMTS_V','INVOICE_PAYMENT_ID',101,'Invoice payment identifier','INVOICE_PAYMENT_ID~INVOICE_PAYMENT_ID','','','','XXEIS_RS_ADMIN','NUMBER','AP_INVOICE_PAYMENTS_ALL','INVOICE_PAYMENT_ID','Invoice Payment Id','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_GL_JRNL_TO_AP_PMTS_V','PAYMENT_HIST_DIST_ID',101,'Payment History Distribution Identifier','PAYMENT_HIST_DIST_ID~PAYMENT_HIST_DIST_ID','','','','XXEIS_RS_ADMIN','NUMBER','AP_PAYMENT_HIST_DISTS','PAYMENT_HIST_DIST_ID','Payment Hist Dist Id','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_GL_JRNL_TO_AP_PMTS_V','REVERSED_JE_HEADER_ID',101,'Defining column of the journal entry that is reversed by this journal entry','REVERSED_JE_HEADER_ID','','','','XXEIS_RS_ADMIN','NUMBER','GL_JE_HEADERS','REVERSED_JE_HEADER_ID','Reversed Je Header Id','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_GL_JRNL_TO_AP_PMTS_V','GL_SL_LINK_ID',101,'Link to associated subledger data','GL_SL_LINK_ID','','','','XXEIS_RS_ADMIN','NUMBER','GL_JE_LINES','GL_SL_LINK_ID','Gl Sl Link Id','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_GL_JRNL_TO_AP_PMTS_V','JE_BATCH_ID',101,'Journal entry batch defining column','JE_BATCH_ID','','','','XXEIS_RS_ADMIN','NUMBER','GL_JE_BATCHES','JE_BATCH_ID','Je Batch Id','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_GL_JRNL_TO_AP_PMTS_V','JE_HEADER_ID',101,'Journal entry header defining column','JE_HEADER_ID','','','','XXEIS_RS_ADMIN','NUMBER','GL_JE_HEADERS','JE_HEADER_ID','Je Header Id','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_GL_JRNL_TO_AP_PMTS_V','BUDGET_VERSION_ID',101,'Budget version defining column','BUDGET_VERSION_ID','','','','XXEIS_RS_ADMIN','NUMBER','GL_BUDGET_VERSIONS','BUDGET_VERSION_ID','Budget Version Id','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_GL_JRNL_TO_AP_PMTS_V','VENDOR_SITE_ID',101,'Vendor Site Id','VENDOR_SITE_ID~VENDOR_SITE_ID','','','','XXEIS_RS_ADMIN','NUMBER','XLA_AE_LINES','PARTY_SITE_ID','Vendor Site Id','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_GL_JRNL_TO_AP_PMTS_V','CODE_COMBINATION_ID',101,'Key flexfield combination defining column','CODE_COMBINATION_ID','','','','XXEIS_RS_ADMIN','NUMBER','GL_CODE_COMBINATIONS','CODE_COMBINATION_ID','Code Combination Id','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_GL_JRNL_TO_AP_PMTS_V','ACCRUAL_REV_JE_HEADER_ID',101,'Reversed journal entry defining column','ACCRUAL_REV_JE_HEADER_ID','','','','XXEIS_RS_ADMIN','NUMBER','GL_JE_HEADERS','ACCRUAL_REV_JE_HEADER_ID','Accrual Rev Je Header Id','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_GL_JRNL_TO_AP_PMTS_V','CHECK_ID',101,'Payment identifier','CHECK_ID~CHECK_ID','','','','XXEIS_RS_ADMIN','NUMBER','AP_CHECKS_ALL','CHECK_ID','Check Id','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_GL_JRNL_TO_AP_PMTS_V','INVOICE_ID',101,'Invoice identifier','INVOICE_ID~INVOICE_ID','','','','XXEIS_RS_ADMIN','NUMBER','AP_INVOICES_ALL','INVOICE_ID','Invoice Id','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_GL_JRNL_TO_AP_PMTS_V','LEDGER_ID',101,'Ledger defining column','LEDGER_ID~LEDGER_ID','','','','XXEIS_RS_ADMIN','NUMBER','GL_LEDGERS','LEDGER_ID','Ledger Id','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_GL_JRNL_TO_AP_PMTS_V','VENDOR_ID',101,'Vendor Id','VENDOR_ID~VENDOR_ID','','','','XXEIS_RS_ADMIN','NUMBER','XLA_AE_LINES','PARTY_ID','Vendor Id','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_GL_JRNL_TO_AP_PMTS_V','AE_HEADER_ID',101,'Ae Header Id','AE_HEADER_ID~AE_HEADER_ID','','','','XXEIS_RS_ADMIN','NUMBER','XLA_AE_LINES','AE_HEADER_ID','Ae Header Id','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_GL_JRNL_TO_AP_PMTS_V','REF_AE_HEADER_ID',101,'Reversed subledger journal entry  header identifier.','REF_AE_HEADER_ID~REF_AE_HEADER_ID','','','','XXEIS_RS_ADMIN','NUMBER','XLA_DISTRIBUTION_LINKS','REF_AE_HEADER_ID','Ref Ae Header Id','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_GL_JRNL_TO_AP_PMTS_V','TEMP_LINE_NUM',101,'It is the intermediary line number generated by the accounting engine when the entries are in most detail form','TEMP_LINE_NUM~TEMP_LINE_NUM','','','','XXEIS_RS_ADMIN','NUMBER','XLA_DISTRIBUTION_LINKS','TEMP_LINE_NUM','Temp Line Num','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_GL_JRNL_TO_AP_PMTS_V','ACCTG_VENDOR_ID',101,'Acctg Vendor Id','ACCTG_VENDOR_ID','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Acctg Vendor Id','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_GL_JRNL_TO_AP_PMTS_V','ACCTG_VENDOR_SITE_ID',101,'Acctg Vendor Site Id','ACCTG_VENDOR_SITE_ID','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Acctg Vendor Site Id','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_GL_JRNL_TO_AP_PMTS_V','AE_LINE_ID',101,'Ae Line Id','AE_LINE_ID','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Ae Line Id','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_GL_JRNL_TO_AP_PMTS_V','ENCUMBRANCE_TYPE',101,'Encumbrance Type','ENCUMBRANCE_TYPE','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Encumbrance Type','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_GL_JRNL_TO_AP_PMTS_V','ENCUMBRANCE_TYPE_ID',101,'Encumbrance Type Id','ENCUMBRANCE_TYPE_ID','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Encumbrance Type Id','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_GL_JRNL_TO_AP_PMTS_V','GIR_JE_HEADER_ID',101,'Gir Je Header Id','GIR_JE_HEADER_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Gir Je Header Id','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_GL_JRNL_TO_AP_PMTS_V','GIR_JE_LINE_NUM',101,'Gir Je Line Num','GIR_JE_LINE_NUM','','','','XXEIS_RS_ADMIN','NUMBER','','','Gir Je Line Num','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_GL_JRNL_TO_AP_PMTS_V','GL#SEGMENT1',101,'Gl#Segment1','GL#SEGMENT1','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Gl#Segment1','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_GL_JRNL_TO_AP_PMTS_V','GL#SEGMENT2',101,'Gl#Segment2','GL#SEGMENT2','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Gl#Segment2','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_GL_JRNL_TO_AP_PMTS_V','GL#SEGMENT3',101,'Gl#Segment3','GL#SEGMENT3','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Gl#Segment3','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_GL_JRNL_TO_AP_PMTS_V','GL#SEGMENT4',101,'Gl#Segment4','GL#SEGMENT4','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Gl#Segment4','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_GL_JRNL_TO_AP_PMTS_V','GL#SEGMENT5',101,'Gl#Segment5','GL#SEGMENT5','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Gl#Segment5','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_GL_JRNL_TO_AP_PMTS_V','GL#SEGMENT6',101,'Gl#Segment6','GL#SEGMENT6','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Gl#Segment6','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_GL_JRNL_TO_AP_PMTS_V','GL#SEGMENT7',101,'Gl#Segment7','GL#SEGMENT7','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Gl#Segment7','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_GL_JRNL_TO_AP_PMTS_V','GL#SEGMENT8',101,'Gl#Segment8','GL#SEGMENT8','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Gl#Segment8','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_GL_JRNL_TO_AP_PMTS_V','JL_JE_HEADER_ID',101,'Jl Je Header Id','JL_JE_HEADER_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Jl Je Header Id','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_GL_JRNL_TO_AP_PMTS_V','REFERENCE_10',101,'Reference 10','REFERENCE_10','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Reference 10','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_GL_JRNL_TO_AP_PMTS_V','REFERENCE_6',101,'Reference 6','REFERENCE_6','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Reference 6','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_GL_JRNL_TO_AP_PMTS_V','REFERENCE_7',101,'Reference 7','REFERENCE_7','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Reference 7','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_GL_JRNL_TO_AP_PMTS_V','SET_OF_BOOKS',101,'Set Of Books','SET_OF_BOOKS','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Set Of Books','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_GL_JRNL_TO_AP_PMTS_V','SUPPLIER',101,'Supplier','SUPPLIER','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Supplier','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_GL_JRNL_TO_AP_PMTS_V','COPYRIGHT',101,'Copyright','COPYRIGHT','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Copyright','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_GL_JRNL_TO_AP_PMTS_V','GCC#50328#ACCOUNT',101,'Accounting Flexfield (KFF): Segment ''Account'' under chart of accounts ''XXCUS Accounting Flexfield''','GCC#50328#ACCOUNT','','','','XXEIS_RS_ADMIN','VARCHAR2','GL_CODE_COMBINATIONS','SEGMENT4','GCC#Account','50328','1014550','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_GL_JRNL_TO_AP_PMTS_V','GCC#50328#ACCOUNT#DESCR',101,'Accounting Flexfield (KFF) : Segment ''Account'' under chart of accounts ''XXCUS Accounting Flexfield''','GCC#50328#ACCOUNT#DESCR','','','','XXEIS_RS_ADMIN','VARCHAR2','GL_CODE_COMBINATIONS','SEGMENT4','GCC#Account Descr','50328','1014550','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_GL_JRNL_TO_AP_PMTS_V','GCC#50328#COST_CENTER',101,'Accounting Flexfield (KFF): Segment ''Cost Center'' under chart of accounts ''XXCUS Accounting Flexfield''','GCC#50328#COST_CENTER','','','','XXEIS_RS_ADMIN','VARCHAR2','GL_CODE_COMBINATIONS','SEGMENT3','GCC#Cost Center','50328','1014549','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_GL_JRNL_TO_AP_PMTS_V','GCC#50328#COST_CENTER#DESCR',101,'Accounting Flexfield (KFF) : Segment ''Cost Center'' under chart of accounts ''XXCUS Accounting Flexfield''','GCC#50328#COST_CENTER#DESCR','','','','XXEIS_RS_ADMIN','VARCHAR2','GL_CODE_COMBINATIONS','SEGMENT3','GCC#Cost Center Descr','50328','1014549','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_GL_JRNL_TO_AP_PMTS_V','GCC#50328#FUTURE_USE',101,'Accounting Flexfield (KFF): Segment ''Future Use'' under chart of accounts ''XXCUS Accounting Flexfield''','GCC#50328#FUTURE_USE','','','','XXEIS_RS_ADMIN','VARCHAR2','GL_CODE_COMBINATIONS','SEGMENT6','GCC#Future Use','50328','1014552','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_GL_JRNL_TO_AP_PMTS_V','GCC#50328#FUTURE_USE#DESCR',101,'Accounting Flexfield (KFF) : Segment ''Future Use'' under chart of accounts ''XXCUS Accounting Flexfield''','GCC#50328#FUTURE_USE#DESCR','','','','XXEIS_RS_ADMIN','VARCHAR2','GL_CODE_COMBINATIONS','SEGMENT6','GCC#Future Use Descr','50328','1014552','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_GL_JRNL_TO_AP_PMTS_V','GCC#50328#FUTURE_USE_2',101,'Accounting Flexfield (KFF): Segment ''Future Use 2'' under chart of accounts ''XXCUS Accounting Flexfield''','GCC#50328#FUTURE_USE_2','','','','XXEIS_RS_ADMIN','VARCHAR2','GL_CODE_COMBINATIONS','SEGMENT7','GCC#Future Use 2','50328','1014948','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_GL_JRNL_TO_AP_PMTS_V','GCC#50328#FUTURE_USE_2#DESCR',101,'Accounting Flexfield (KFF) : Segment ''Future Use 2'' under chart of accounts ''XXCUS Accounting Flexfield''','GCC#50328#FUTURE_USE_2#DESCR','','','','XXEIS_RS_ADMIN','VARCHAR2','GL_CODE_COMBINATIONS','SEGMENT7','GCC#Future Use 2 Descr','50328','1014948','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_GL_JRNL_TO_AP_PMTS_V','GCC#50328#LOCATION',101,'Accounting Flexfield (KFF): Segment ''Location'' under chart of accounts ''XXCUS Accounting Flexfield''','GCC#50328#LOCATION','','','','XXEIS_RS_ADMIN','VARCHAR2','GL_CODE_COMBINATIONS','SEGMENT2','GCC#Location','50328','1014548','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_GL_JRNL_TO_AP_PMTS_V','GCC#50328#LOCATION#DESCR',101,'Accounting Flexfield (KFF) : Segment ''Location'' under chart of accounts ''XXCUS Accounting Flexfield''','GCC#50328#LOCATION#DESCR','','','','XXEIS_RS_ADMIN','VARCHAR2','GL_CODE_COMBINATIONS','SEGMENT2','GCC#Location Descr','50328','1014548','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_GL_JRNL_TO_AP_PMTS_V','GCC#50328#PRODUCT',101,'Accounting Flexfield (KFF): Segment ''Product'' under chart of accounts ''XXCUS Accounting Flexfield''','GCC#50328#PRODUCT','','','','XXEIS_RS_ADMIN','VARCHAR2','GL_CODE_COMBINATIONS','SEGMENT1','GCC#Product','50328','1014547','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_GL_JRNL_TO_AP_PMTS_V','GCC#50328#PRODUCT#DESCR',101,'Accounting Flexfield (KFF) : Segment ''Product'' under chart of accounts ''XXCUS Accounting Flexfield''','GCC#50328#PRODUCT#DESCR','','','','XXEIS_RS_ADMIN','VARCHAR2','GL_CODE_COMBINATIONS','SEGMENT1','GCC#Product Descr','50328','1014547','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_GL_JRNL_TO_AP_PMTS_V','GCC#50328#PROJECT_CODE',101,'Accounting Flexfield (KFF): Segment ''Project Code'' under chart of accounts ''XXCUS Accounting Flexfield''','GCC#50328#PROJECT_CODE','','','','XXEIS_RS_ADMIN','VARCHAR2','GL_CODE_COMBINATIONS','SEGMENT5','GCC#Project Code','50328','1014551','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_GL_JRNL_TO_AP_PMTS_V','GCC#50328#PROJECT_CODE#DESCR',101,'Accounting Flexfield (KFF) : Segment ''Project Code'' under chart of accounts ''XXCUS Accounting Flexfield''','GCC#50328#PROJECT_CODE#DESCR','','','','XXEIS_RS_ADMIN','VARCHAR2','GL_CODE_COMBINATIONS','SEGMENT5','GCC#Project Code Descr','50328','1014551','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_GL_JRNL_TO_AP_PMTS_V','GCC1#50328#ACCOUNT',101,'Accounting Flexfield (KFF): Segment ''Account'' under chart of accounts ''XXCUS Accounting Flexfield''','GCC1#50328#ACCOUNT','','','','XXEIS_RS_ADMIN','VARCHAR2','GL_CODE_COMBINATIONS','SEGMENT4','GCC1#Account','50328','1014550','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_GL_JRNL_TO_AP_PMTS_V','GCC1#50328#ACCOUNT#DESCR',101,'Accounting Flexfield (KFF) : Segment ''Account'' under chart of accounts ''XXCUS Accounting Flexfield''','GCC1#50328#ACCOUNT#DESCR','','','','XXEIS_RS_ADMIN','VARCHAR2','GL_CODE_COMBINATIONS','SEGMENT4','GCC1#Account Descr','50328','1014550','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_GL_JRNL_TO_AP_PMTS_V','GCC1#50328#COST_CENTER',101,'Accounting Flexfield (KFF): Segment ''Cost Center'' under chart of accounts ''XXCUS Accounting Flexfield''','GCC1#50328#COST_CENTER','','','','XXEIS_RS_ADMIN','VARCHAR2','GL_CODE_COMBINATIONS','SEGMENT3','GCC1#Cost Center','50328','1014549','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_GL_JRNL_TO_AP_PMTS_V','GCC1#50328#COST_CENTER#DESCR',101,'Accounting Flexfield (KFF) : Segment ''Cost Center'' under chart of accounts ''XXCUS Accounting Flexfield''','GCC1#50328#COST_CENTER#DESCR','','','','XXEIS_RS_ADMIN','VARCHAR2','GL_CODE_COMBINATIONS','SEGMENT3','GCC1#Cost Center Descr','50328','1014549','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_GL_JRNL_TO_AP_PMTS_V','GCC1#50328#FUTURE_USE',101,'Accounting Flexfield (KFF): Segment ''Future Use'' under chart of accounts ''XXCUS Accounting Flexfield''','GCC1#50328#FUTURE_USE','','','','XXEIS_RS_ADMIN','VARCHAR2','GL_CODE_COMBINATIONS','SEGMENT6','GCC1#Future Use','50328','1014552','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_GL_JRNL_TO_AP_PMTS_V','GCC1#50328#FUTURE_USE#DESCR',101,'Accounting Flexfield (KFF) : Segment ''Future Use'' under chart of accounts ''XXCUS Accounting Flexfield''','GCC1#50328#FUTURE_USE#DESCR','','','','XXEIS_RS_ADMIN','VARCHAR2','GL_CODE_COMBINATIONS','SEGMENT6','GCC1#Future Use Descr','50328','1014552','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_GL_JRNL_TO_AP_PMTS_V','GCC1#50328#FUTURE_USE_2',101,'Accounting Flexfield (KFF): Segment ''Future Use 2'' under chart of accounts ''XXCUS Accounting Flexfield''','GCC1#50328#FUTURE_USE_2','','','','XXEIS_RS_ADMIN','VARCHAR2','GL_CODE_COMBINATIONS','SEGMENT7','GCC1#Future Use 2','50328','1014948','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_GL_JRNL_TO_AP_PMTS_V','GCC1#50328#FUTURE_USE_2#DESCR',101,'Accounting Flexfield (KFF) : Segment ''Future Use 2'' under chart of accounts ''XXCUS Accounting Flexfield''','GCC1#50328#FUTURE_USE_2#DESCR','','','','XXEIS_RS_ADMIN','VARCHAR2','GL_CODE_COMBINATIONS','SEGMENT7','GCC1#Future Use 2 Descr','50328','1014948','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_GL_JRNL_TO_AP_PMTS_V','GCC1#50328#LOCATION',101,'Accounting Flexfield (KFF): Segment ''Location'' under chart of accounts ''XXCUS Accounting Flexfield''','GCC1#50328#LOCATION','','','','XXEIS_RS_ADMIN','VARCHAR2','GL_CODE_COMBINATIONS','SEGMENT2','GCC1#Location','50328','1014548','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_GL_JRNL_TO_AP_PMTS_V','GCC1#50328#LOCATION#DESCR',101,'Accounting Flexfield (KFF) : Segment ''Location'' under chart of accounts ''XXCUS Accounting Flexfield''','GCC1#50328#LOCATION#DESCR','','','','XXEIS_RS_ADMIN','VARCHAR2','GL_CODE_COMBINATIONS','SEGMENT2','GCC1#Location Descr','50328','1014548','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_GL_JRNL_TO_AP_PMTS_V','GCC1#50328#PRODUCT',101,'Accounting Flexfield (KFF): Segment ''Product'' under chart of accounts ''XXCUS Accounting Flexfield''','GCC1#50328#PRODUCT','','','','XXEIS_RS_ADMIN','VARCHAR2','GL_CODE_COMBINATIONS','SEGMENT1','GCC1#Product','50328','1014547','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_GL_JRNL_TO_AP_PMTS_V','GCC1#50328#PRODUCT#DESCR',101,'Accounting Flexfield (KFF) : Segment ''Product'' under chart of accounts ''XXCUS Accounting Flexfield''','GCC1#50328#PRODUCT#DESCR','','','','XXEIS_RS_ADMIN','VARCHAR2','GL_CODE_COMBINATIONS','SEGMENT1','GCC1#Product Descr','50328','1014547','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_GL_JRNL_TO_AP_PMTS_V','GCC1#50328#PROJECT_CODE',101,'Accounting Flexfield (KFF): Segment ''Project Code'' under chart of accounts ''XXCUS Accounting Flexfield''','GCC1#50328#PROJECT_CODE','','','','XXEIS_RS_ADMIN','VARCHAR2','GL_CODE_COMBINATIONS','SEGMENT5','GCC1#Project Code','50328','1014551','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_GL_JRNL_TO_AP_PMTS_V','GCC1#50328#PROJECT_CODE#DESCR',101,'Accounting Flexfield (KFF) : Segment ''Project Code'' under chart of accounts ''XXCUS Accounting Flexfield''','GCC1#50328#PROJECT_CODE#DESCR','','','','XXEIS_RS_ADMIN','VARCHAR2','GL_CODE_COMBINATIONS','SEGMENT5','GCC1#Project Code Descr','50328','1014551','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_GL_JRNL_TO_AP_PMTS_V','AI#167#BOL',101,'Descriptive flexfield (DFF): Invoice Column Name: BOL Context: 167','AI#167#BOL','','','','XXEIS_RS_ADMIN','VARCHAR2','AP_INVOICES_ALL','ATTRIBUTE6','Ai#167#Bol','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_GL_JRNL_TO_AP_PMTS_V','AI#163#BOL',101,'Descriptive flexfield (DFF): Invoice Column Name: BOL Context: 163','AI#163#BOL','','','','XXEIS_RS_ADMIN','VARCHAR2','AP_INVOICES_ALL','ATTRIBUTE6','Ai#163#Bol','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_GL_JRNL_TO_AP_PMTS_V','AI#163#BizTalk_ID_Frt_Biztal',101,'Descriptive flexfield (DFF): Invoice Column Name: BizTalk ID-Frt Biztalk Context: 163','AI#163#BizTalk_ID_Frt_Biztal','','','','XXEIS_RS_ADMIN','VARCHAR2','AP_INVOICES_ALL','ATTRIBUTE1','Ai#163#Biztalk Id-Frt Biztalk','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_GL_JRNL_TO_AP_PMTS_V','AI#163#Branch_Frt_Biztalk',101,'Descriptive flexfield (DFF): Invoice Column Name: Branch-Frt Biztalk Context: 163','AI#163#Branch_Frt_Biztalk','','','','XXEIS_RS_ADMIN','VARCHAR2','AP_INVOICES_ALL','ATTRIBUTE2','Ai#163#Branch-Frt Biztalk','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_GL_JRNL_TO_AP_PMTS_V','AI#167#Canada_Tax_Type',101,'Descriptive flexfield (DFF): Invoice Column Name: Canada Tax Type Context: 167','AI#167#Canada_Tax_Type','','','','XXEIS_RS_ADMIN','VARCHAR2','AP_INVOICES_ALL','ATTRIBUTE5','Ai#167#Canada Tax Type','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_GL_JRNL_TO_AP_PMTS_V','AI#166#Case_Identifier',101,'Descriptive flexfield (DFF): Invoice Column Name: Case Identifier Context: 166','AI#166#Case_Identifier','','','','XXEIS_RS_ADMIN','VARCHAR2','AP_INVOICES_ALL','ATTRIBUTE1','Ai#166#Case Identifier','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_GL_JRNL_TO_AP_PMTS_V','AI#163#DCTM_Image_Link',101,'Descriptive flexfield (DFF): Invoice Column Name: DCTM Image Link Context: 163','AI#163#DCTM_Image_Link','','','','XXEIS_RS_ADMIN','VARCHAR2','AP_INVOICES_ALL','ATTRIBUTE14','Ai#163#Dctm Image Link','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_GL_JRNL_TO_AP_PMTS_V','AI#166#Deduction_Code',101,'Descriptive flexfield (DFF): Invoice Column Name: Deduction Code Context: 166','AI#166#Deduction_Code','','','','XXEIS_RS_ADMIN','VARCHAR2','AP_INVOICES_ALL','ATTRIBUTE2','Ai#166#Deduction Code','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_GL_JRNL_TO_AP_PMTS_V','AI#166#Description',101,'Descriptive flexfield (DFF): Invoice Column Name: Description Context: 166','AI#166#Description','','','','XXEIS_RS_ADMIN','VARCHAR2','AP_INVOICES_ALL','ATTRIBUTE4','Ai#166#Description','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_GL_JRNL_TO_AP_PMTS_V','AI#162#Documentum_PO_Number',101,'Descriptive flexfield (DFF): Invoice Column Name: Documentum PO Number Context: 162','AI#162#Documentum_PO_Number','','','','XXEIS_RS_ADMIN','VARCHAR2','AP_INVOICES_ALL','ATTRIBUTE2','Ai#162#Documentum Po Number','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_GL_JRNL_TO_AP_PMTS_V','AI#166#Employee',101,'Descriptive flexfield (DFF): Invoice Column Name: Employee Context: 166','AI#166#Employee','','','','XXEIS_RS_ADMIN','VARCHAR2','AP_INVOICES_ALL','ATTRIBUTE3','Ai#166#Employee','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_GL_JRNL_TO_AP_PMTS_V','AI#163#Invoice_Type',101,'Descriptive flexfield (DFF): Invoice Column Name: Invoice Type Context: 163','AI#163#Invoice_Type','','','','XXEIS_RS_ADMIN','VARCHAR2','AP_INVOICES_ALL','ATTRIBUTE11','Ai#163#Invoice Type','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_GL_JRNL_TO_AP_PMTS_V','AI#163#PO_Number',101,'Descriptive flexfield (DFF): Invoice Column Name: PO Number Context: 163','AI#163#PO_Number','','','','XXEIS_RS_ADMIN','VARCHAR2','AP_INVOICES_ALL','ATTRIBUTE4','Ai#163#Po Number','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_GL_JRNL_TO_AP_PMTS_V','AI#167#PO_Number',101,'Descriptive flexfield (DFF): Invoice Column Name: PO Number Context: 167','AI#167#PO_Number','','','','XXEIS_RS_ADMIN','VARCHAR2','AP_INVOICES_ALL','ATTRIBUTE4','Ai#167#Po Number','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_GL_JRNL_TO_AP_PMTS_V','AI#163#POS_System_Code_Frt_B',101,'Descriptive flexfield (DFF): Invoice Column Name: POS System Code-Frt Biztalk Context: 163','AI#163#POS_System_Code_Frt_B','','','','XXEIS_RS_ADMIN','VARCHAR2','AP_INVOICES_ALL','ATTRIBUTE8','Ai#163#Pos System Code-Frt Biztalk','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_GL_JRNL_TO_AP_PMTS_V','AI#163#R11_Invoice_ID',101,'Descriptive flexfield (DFF): Invoice Column Name: R11 Invoice ID Context: 163','AI#163#R11_Invoice_ID','','','','XXEIS_RS_ADMIN','VARCHAR2','AP_INVOICES_ALL','ATTRIBUTE15','Ai#163#R11 Invoice Id','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_GL_JRNL_TO_AP_PMTS_V','AI#166#R11_Invoice_ID',101,'Descriptive flexfield (DFF): Invoice Column Name: R11 Invoice ID Context: 166','AI#166#R11_Invoice_ID','','','','XXEIS_RS_ADMIN','VARCHAR2','AP_INVOICES_ALL','ATTRIBUTE15','Ai#166#R11 Invoice Id','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_GL_JRNL_TO_AP_PMTS_V','AI#163#SO_Number',101,'Descriptive flexfield (DFF): Invoice Column Name: SO Number Context: 163','AI#163#SO_Number','','','','XXEIS_RS_ADMIN','VARCHAR2','AP_INVOICES_ALL','ATTRIBUTE3','Ai#163#So Number','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_GL_JRNL_TO_AP_PMTS_V','AI#167#SO_Number',101,'Descriptive flexfield (DFF): Invoice Column Name: SO Number Context: 167','AI#167#SO_Number','','','','XXEIS_RS_ADMIN','VARCHAR2','AP_INVOICES_ALL','ATTRIBUTE3','Ai#167#So Number','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_GL_JRNL_TO_AP_PMTS_V','GCC#Branch',101,'Descriptive flexfield (DFF): GL Accounts Column Name: Branch','GCC#Branch','','','','XXEIS_RS_ADMIN','VARCHAR2','GL_CODE_COMBINATIONS','ATTRIBUTE1','Gcc#Branch','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_GL_JRNL_TO_AP_PMTS_V','GCC1#Branch',101,'Descriptive flexfield (DFF): GL Accounts Column Name: Branch','GCC1#Branch','','','','XXEIS_RS_ADMIN','VARCHAR2','GL_CODE_COMBINATIONS','ATTRIBUTE1','Gcc1#Branch','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_GL_JRNL_TO_AP_PMTS_V','JL#Account',101,'Descriptive flexfield (DFF): Enter Journals: Lines Column Name: Account','JL#Account','','','','XXEIS_RS_ADMIN','VARCHAR2','GL_JE_LINES','ATTRIBUTE3','Jl#Account','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_GL_JRNL_TO_AP_PMTS_V','JL#Branch',101,'Descriptive flexfield (DFF): Enter Journals: Lines Column Name: Branch','JL#Branch','','','','XXEIS_RS_ADMIN','VARCHAR2','GL_JE_LINES','ATTRIBUTE1','Jl#Branch','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_GL_JRNL_TO_AP_PMTS_V','JL#Dept',101,'Descriptive flexfield (DFF): Enter Journals: Lines Column Name: Dept','JL#Dept','','','','XXEIS_RS_ADMIN','VARCHAR2','GL_JE_LINES','ATTRIBUTE2','Jl#Dept','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_GL_JRNL_TO_AP_PMTS_V','JL#POS_Branch',101,'Descriptive flexfield (DFF): Enter Journals: Lines Column Name: POS Branch','JL#POS_Branch','','','','XXEIS_RS_ADMIN','VARCHAR2','GL_JE_LINES','ATTRIBUTE5','Jl#Pos Branch','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_GL_JRNL_TO_AP_PMTS_V','JL#Sub_Acct',101,'Descriptive flexfield (DFF): Enter Journals: Lines Column Name: Sub Acct','JL#Sub_Acct','','','','XXEIS_RS_ADMIN','VARCHAR2','GL_JE_LINES','ATTRIBUTE4','Jl#Sub Acct','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_GL_JRNL_TO_AP_PMTS_V','JL#WC_Formula',101,'Descriptive flexfield (DFF): Enter Journals: Lines Column Name: WC Formula','JL#WC_Formula','','','','XXEIS_RS_ADMIN','VARCHAR2','GL_JE_LINES','ATTRIBUTE6','Jl#Wc Formula','','','','US','');
--Inserting Object Components for EIS_GL_JRNL_TO_AP_PMTS_V
xxeis.eis_rsc_ins.vcomp( 'EIS_GL_JRNL_TO_AP_PMTS_V','GL_JE_HEADERS',101,'GL_JE_HEADERS','JH','JH','XXEIS_RS_ADMIN','XXEIS_RS_ADMIN','-1','Journal Entry Headers','','','','','','Y',' ',' ',' ',' ',' ');
xxeis.eis_rsc_ins.vcomp( 'EIS_GL_JRNL_TO_AP_PMTS_V','GL_JE_LINES',101,'GL_JE_LINES','JL','JL','XXEIS_RS_ADMIN','XXEIS_RS_ADMIN','-1','Journal Entry Lines','','','','','','Y',' ',' ',' ',' ',' ');
xxeis.eis_rsc_ins.vcomp( 'EIS_GL_JRNL_TO_AP_PMTS_V','GL_BUDGET_VERSIONS',101,'GL_BUDGET_VERSIONS','GBV','GBV','XXEIS_RS_ADMIN','XXEIS_RS_ADMIN','-1','GL Budget Versions','','','','','','Y',' ',' ',' ',' ',' ');
xxeis.eis_rsc_ins.vcomp( 'EIS_GL_JRNL_TO_AP_PMTS_V','XLA_AE_LINES',101,'XLA_AE_LINES','AEL','AEL','XXEIS_RS_ADMIN','XXEIS_RS_ADMIN','-1','SLA Journal Lines','','','','','','Y',' ',' ',' ',' ',' ');
xxeis.eis_rsc_ins.vcomp( 'EIS_GL_JRNL_TO_AP_PMTS_V','XLA_AE_HEADERS',101,'XLA_AE_HEADERS','AEH','AEH','XXEIS_RS_ADMIN','XXEIS_RS_ADMIN','-1','SLA Journal Headers','','','','','','Y',' ',' ',' ',' ',' ');
xxeis.eis_rsc_ins.vcomp( 'EIS_GL_JRNL_TO_AP_PMTS_V','XLA_DISTRIBUTION_LINKS',101,'XLA_DISTRIBUTION_LINKS','ADL','ADL','XXEIS_RS_ADMIN','XXEIS_RS_ADMIN','-1','SLA Distributions Links','','','','','','Y',' ',' ',' ',' ',' ');
xxeis.eis_rsc_ins.vcomp( 'EIS_GL_JRNL_TO_AP_PMTS_V','AP_PAYMENT_HIST_DISTS',101,'AP_PAYMENT_HIST_DISTS','APHD','APHD','XXEIS_RS_ADMIN','XXEIS_RS_ADMIN','-1','Payable Status History.','','','','','APHD','Y',' ',' ',' ',' ',' ');
xxeis.eis_rsc_ins.vcomp( 'EIS_GL_JRNL_TO_AP_PMTS_V','AP_INVOICE_PAYMENTS_ALL',101,'AP_INVOICE_PAYMENTS_ALL','AIP','AIP','XXEIS_RS_ADMIN','XXEIS_RS_ADMIN','-1','Invoice Payment Records','','','','','','Y',' ',' ',' ',' ',' ');
xxeis.eis_rsc_ins.vcomp( 'EIS_GL_JRNL_TO_AP_PMTS_V','AP_INVOICES_ALL',101,'AP_INVOICES_ALL','AI','AI','XXEIS_RS_ADMIN','XXEIS_RS_ADMIN','-1','Detailed Invoice Records','','','','','','Y',' ',' ',' ',' ',' ');
xxeis.eis_rsc_ins.vcomp( 'EIS_GL_JRNL_TO_AP_PMTS_V','AP_CHECKS_ALL',101,'AP_CHECKS_ALL','AC','AC','XXEIS_RS_ADMIN','XXEIS_RS_ADMIN','-1','Supplier Payment Data','','','','','','Y',' ',' ',' ',' ',' ');
xxeis.eis_rsc_ins.vcomp( 'EIS_GL_JRNL_TO_AP_PMTS_V','GL_LEDGERS',101,'GL_LEDGERS','LE','LE','XXEIS_RS_ADMIN','XXEIS_RS_ADMIN','-1','Ledgers','','','','','','Y',' ',' ',' ',' ',' ');
xxeis.eis_rsc_ins.vcomp( 'EIS_GL_JRNL_TO_AP_PMTS_V','GL_JE_BATCHES',101,'GL_JE_BATCHES','JB','JB','XXEIS_RS_ADMIN','XXEIS_RS_ADMIN','-1','Journal Entry Batches','','','','','','Y',' ',' ',' ',' ',' ');
xxeis.eis_rsc_ins.vcomp( 'EIS_GL_JRNL_TO_AP_PMTS_V','GL_CODE_COMBINATIONS_KFV',101,'GL_CODE_COMBINATIONS','GCC1','GCC1','XXEIS_RS_ADMIN','XXEIS_RS_ADMIN','-1','SLA Account','','','','','','Y',' ',' ',' ',' ',' ');
xxeis.eis_rsc_ins.vcomp( 'EIS_GL_JRNL_TO_AP_PMTS_V','GL_CODE_COMBINATIONS_KFV',101,'GL_CODE_COMBINATIONS','GCC','GCC','XXEIS_RS_ADMIN','XXEIS_RS_ADMIN','-1','GL Journal Line Account','','','','','','Y',' ',' ',' ',' ',' ');
--Inserting Object Component Joins for EIS_GL_JRNL_TO_AP_PMTS_V
xxeis.eis_rsc_ins.vcj( 'EIS_GL_JRNL_TO_AP_PMTS_V','GL_JE_HEADERS','JH',101,'EGLAP.JE_HEADER_ID','=','JH.JE_HEADER_ID(+)','','','','Y','XXEIS_RS_ADMIN');
xxeis.eis_rsc_ins.vcj( 'EIS_GL_JRNL_TO_AP_PMTS_V','GL_JE_LINES','JL',101,'EGLAP.JE_HEADER_ID','=','JL.JE_HEADER_ID(+)','','','','Y','XXEIS_RS_ADMIN');
xxeis.eis_rsc_ins.vcj( 'EIS_GL_JRNL_TO_AP_PMTS_V','GL_JE_LINES','JL',101,'EGLAP.JE_LINE_NUM','=','JL.JE_LINE_NUM(+)','','','','Y','XXEIS_RS_ADMIN');
xxeis.eis_rsc_ins.vcj( 'EIS_GL_JRNL_TO_AP_PMTS_V','GL_BUDGET_VERSIONS','GBV',101,'EGLAP.BUDGET_VERSION_ID','=','GBV.BUDGET_VERSION_ID(+)','','','','Y','XXEIS_RS_ADMIN');
xxeis.eis_rsc_ins.vcj( 'EIS_GL_JRNL_TO_AP_PMTS_V','XLA_AE_LINES','AEL',101,'EGLAP.AE_HEADER_ID','=','AEL.AE_HEADER_ID(+)','','','','Y','XXEIS_RS_ADMIN');
xxeis.eis_rsc_ins.vcj( 'EIS_GL_JRNL_TO_AP_PMTS_V','XLA_AE_LINES','AEL',101,'EGLAP.AE_LINE_NUM','=','AEL.AE_LINE_NUM(+)','','','','Y','XXEIS_RS_ADMIN');
xxeis.eis_rsc_ins.vcj( 'EIS_GL_JRNL_TO_AP_PMTS_V','XLA_AE_LINES','AEL',101,'EGLAP.APPLICATION_ID','=','AEL.APPLICATION_ID(+)','','','','Y','XXEIS_RS_ADMIN');
xxeis.eis_rsc_ins.vcj( 'EIS_GL_JRNL_TO_AP_PMTS_V','XLA_AE_HEADERS','AEH',101,'EGLAP.AE_HEADER_ID','=','AEH.AE_HEADER_ID(+)','','','','Y','XXEIS_RS_ADMIN');
xxeis.eis_rsc_ins.vcj( 'EIS_GL_JRNL_TO_AP_PMTS_V','XLA_AE_HEADERS','AEH',101,'EGLAP.APPLICATION_ID','=','AEH.APPLICATION_ID(+)','','','','Y','XXEIS_RS_ADMIN');
xxeis.eis_rsc_ins.vcj( 'EIS_GL_JRNL_TO_AP_PMTS_V','XLA_DISTRIBUTION_LINKS','ADL',101,'EGLAP.APPLICATION_ID','=','ADL.APPLICATION_ID(+)','','','','Y','XXEIS_RS_ADMIN');
xxeis.eis_rsc_ins.vcj( 'EIS_GL_JRNL_TO_AP_PMTS_V','XLA_DISTRIBUTION_LINKS','ADL',101,'EGLAP.REF_AE_HEADER_ID','=','ADL.REF_AE_HEADER_ID(+)','','','','Y','XXEIS_RS_ADMIN');
xxeis.eis_rsc_ins.vcj( 'EIS_GL_JRNL_TO_AP_PMTS_V','XLA_DISTRIBUTION_LINKS','ADL',101,'EGLAP.TEMP_LINE_NUM','=','ADL.TEMP_LINE_NUM(+)','','','','Y','XXEIS_RS_ADMIN');
xxeis.eis_rsc_ins.vcj( 'EIS_GL_JRNL_TO_AP_PMTS_V','XLA_DISTRIBUTION_LINKS','ADL',101,'EGLAP.AE_HEADER_ID','=','ADL.AE_HEADER_ID(+)','','','','Y','XXEIS_RS_ADMIN');
xxeis.eis_rsc_ins.vcj( 'EIS_GL_JRNL_TO_AP_PMTS_V','AP_PAYMENT_HIST_DISTS','APHD',101,'EGLAP.PAYMENT_HIST_DIST_ID','=','APHD.PAYMENT_HIST_DIST_ID(+)','','','','Y','XXEIS_RS_ADMIN');
xxeis.eis_rsc_ins.vcj( 'EIS_GL_JRNL_TO_AP_PMTS_V','AP_INVOICE_PAYMENTS_ALL','AIP',101,'EGLAP.INVOICE_PAYMENT_ID','=','AIP.INVOICE_PAYMENT_ID(+)','','','','Y','XXEIS_RS_ADMIN');
xxeis.eis_rsc_ins.vcj( 'EIS_GL_JRNL_TO_AP_PMTS_V','AP_INVOICES_ALL','AI',101,'EGLAP.INVOICE_ID','=','AI.INVOICE_ID(+)','','','','Y','XXEIS_RS_ADMIN');
xxeis.eis_rsc_ins.vcj( 'EIS_GL_JRNL_TO_AP_PMTS_V','AP_CHECKS_ALL','AC',101,'EGLAP.CHECK_ID','=','AC.CHECK_ID(+)','','','','Y','XXEIS_RS_ADMIN');
xxeis.eis_rsc_ins.vcj( 'EIS_GL_JRNL_TO_AP_PMTS_V','GL_LEDGERS','LE',101,'EGLAP.LEDGER_NAME','=','LE.NAME(+)','','','','Y','XXEIS_RS_ADMIN');
xxeis.eis_rsc_ins.vcj( 'EIS_GL_JRNL_TO_AP_PMTS_V','GL_JE_BATCHES','JB',101,'EGLAP.JE_BATCH_ID','=','JB.JE_BATCH_ID(+)','','','','Y','XXEIS_RS_ADMIN');
xxeis.eis_rsc_ins.vcj( 'EIS_GL_JRNL_TO_AP_PMTS_V','GL_CODE_COMBINATIONS_KFV','GCC1',101,'EGLAP.SLA_CODE_COMBINATION_ID','=','GCC1.CODE_COMBINATION_ID(+)','','','','','XXEIS_RS_ADMIN');
xxeis.eis_rsc_ins.vcj( 'EIS_GL_JRNL_TO_AP_PMTS_V','GL_CODE_COMBINATIONS_KFV','GCC',101,'EGLAP.CODE_COMBINATION_ID','=','GCC.CODE_COMBINATION_ID(+)','','','','','XXEIS_RS_ADMIN');
ELSE
RAISE_APPLICATION_ERROR (-20001,'Module does not exist. Import/Create the module with id 101');
END IF;
END;
/
prompt Creating Report LOV Data for HDS GL Journals to AP Payments
set scan off define off
DECLARE
mod_exist VARCHAR2(1);
BEGIN 
mod_exist := XXEIS.EIS_RSC_INSERT.check_if_module_exist(101);
IF mod_exist = 'Y' THEN 
--Inserting Report LOVs - HDS GL Journals to AP Payments
xxeis.eis_rsc_ins.lov( 101,'select  per.period_name , led.name ledger_name, per.period_year, per.period_num, per.start_date, per.end_date
from    gl_periods per,gl_ledgers led
where  per.period_set_name = led.period_set_name
and     GL_SECURITY_PKG.VALIDATE_ACCESS(led.ledger_ID) = ''TRUE''
and     led.accounted_period_type = per.period_type','null','EIS_GL_PERIOD_LOV','Derives GL Periods based on the corresponding setups in the sets of books','XXEIS_RS_ADMIN',NULL,'','','','','','','','US');
xxeis.eis_rsc_ins.lov( 101,'SELECT  CONCATENATED_SEGMENTS
FROM   gl_code_combinations_kfv glcc
WHERE  chart_of_accounts_id IN (
SELECT led.chart_of_accounts_id
FROM gl_ledgers led
WHERE GL_SECURITY_PKG.VALIDATE_ACCESS(LED.LEDGER_ID) = ''TRUE'')
and nvl(summary_flag,''N'') = upper(''N'')
order by CONCATENATED_SEGMENTS','','EIS_GLACCOUNT_CONCAT_LOV','EIS_GLACCOUNT_CONCAT_LOV','XXEIS_RS_ADMIN',NULL,'','','','','','','','US');
xxeis.eis_rsc_ins.lov( 101,'SELECT LED.NAME ledger_name
from
GL_ACCESS_SET_LEDGERS AC,
GL_LEDGERS                      LED
WHERE AC.ACCESS_SET_ID          = FND_PROFILE.VALUE(''GL_ACCESS_SET_ID'')
AND LED.LEDGER_ID                     = AC.LEDGER_ID
AND LED.OBJECT_TYPE_CODE       = ''L''
UNION
SELECT LED.NAME
FROM   GL_LEDGERS LED
WHERE  gl_security_pkg.validate_access(LED.ledger_id) = ''TRUE'' ','','EIS_GL_LEDGERS','Ledger and ledger sets of an access set.','XXEIS_RS_ADMIN',NULL,'','','','','','','','US');
xxeis.eis_rsc_ins.lov( 101,'Select
      distinct name Journal_Name, Currency_code Currency_code
From  gl_je_headers
Where gl_security_pkg.validate_access(ledger_id) = ''TRUE''
And   je_source = ''Payables''
And   je_category = ''Payments''','','EIS_GL_AP_PAYMENTS_JOURNAL_LOV','This Lov displays the distinct Journal Name for AP Payments.','XXEIS_RS_ADMIN',NULL,'','','','','','','','US');
xxeis.eis_rsc_ins.lov( 101,'select distinct jeb.name Batch_Name,
                jeb.Default_period_name Period,
                Decode(jeb.status,''P'',''Posted'',''U'',''Unposted'') Posting_status
from   gl_je_batches jeb,
       gl_je_headers jeh
where  gl_security_pkg.validate_access(jeh.ledger_id) = ''TRUE''
And    jeh.je_batch_id     = jeb.je_batch_id
And    jeh.je_source = ''Payables''
And    jeh.je_category = ''Payments''','','EIS_GL_AP_PAYMENTS_BATCH_LOV','Displays the batch names of AP Payment Journals.','XXEIS_RS_ADMIN',NULL,'','','','','','','','US');
null;
ELSE
RAISE_APPLICATION_ERROR (-20001,'Module does not exist. Import/Create the module with id 101');
END IF;
END;
/
prompt Creating Report Data for HDS GL Journals to AP Payments
set scan off define off
DECLARE
mod_exist varchar2(1);
BEGIN 
mod_exist := XXEIS.EIS_RSC_INSERT.check_if_module_exist(101);
IF mod_exist = 'Y' THEN 
--Deleting Report data - HDS GL Journals to AP Payments
xxeis.eis_rsc_utility.delete_report_rows( 'HDS GL Journals to AP Payments',101 );
--Inserting Report - HDS GL Journals to AP Payments
xxeis.eis_rsc_ins.r( 101,'HDS GL Journals to AP Payments','','This report provides general ledger journal information and the corresponding tieback details to AP Payments. This report can be used to generate journal information and its corresponding sub-ledger detail for AP Payments within a given GL Period, GL Batch Name or GL Journal Name','','','','XXEIS_RS_ADMIN','EIS_GL_JRNL_TO_AP_PMTS_V','Y','','','XXEIS_RS_ADMIN','','N','HDS Standard Reports','','CSV,Pivot Excel,EXCEL,','','','','','','','','','US','','','','','','','','','','','','','','','','');
--Inserting Report Columns - HDS GL Journals to AP Payments
xxeis.eis_rsc_ins.rc( 'HDS GL Journals to AP Payments',101,'GL_ACCOUNT_STRING','GL Account String','GL Account String','','','','','12','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_GL_JRNL_TO_AP_PMTS_V','','','GROUP_BY','US','','');
xxeis.eis_rsc_ins.rc( 'HDS GL Journals to AP Payments',101,'GL_SL_LINK_TABLE','Gl Sl Link Table','Table containing associated subledger data','','','','','52','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_GL_JRNL_TO_AP_PMTS_V','','','GROUP_BY','US','','');
xxeis.eis_rsc_ins.rc( 'HDS GL Journals to AP Payments',101,'INVOICE_NUM','Invoice Num','Journal import reference column','','','','','32','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_GL_JRNL_TO_AP_PMTS_V','','','GROUP_BY','US','','');
xxeis.eis_rsc_ins.rc( 'HDS GL Journals to AP Payments',101,'JE_ACCOUNTED_CR','Je Accounted Cr','Journal entry line credit amount in base currency','','','','','18','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_GL_JRNL_TO_AP_PMTS_V','','','SUM','US','','');
xxeis.eis_rsc_ins.rc( 'HDS GL Journals to AP Payments',101,'JE_ACCOUNTED_DR','Je Accounted Dr','Journal entry line debit amount in base currency','','','','','16','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_GL_JRNL_TO_AP_PMTS_V','','','SUM','US','','');
xxeis.eis_rsc_ins.rc( 'HDS GL Journals to AP Payments',101,'JE_BALANCED','Je Balanced','Balanced journal entry flag','','','','','36','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_GL_JRNL_TO_AP_PMTS_V','','','GROUP_BY','US','','');
xxeis.eis_rsc_ins.rc( 'HDS GL Journals to AP Payments',101,'BANK_ACCOUNT_NUM','Bank Account Num','Suppliers bank account number for electronic payment purposes','','','','','19','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_GL_JRNL_TO_AP_PMTS_V','','','GROUP_BY','US','','');
xxeis.eis_rsc_ins.rc( 'HDS GL Journals to AP Payments',101,'CHECK_CLEARED_AMOUNT','Check Cleared Amount','Payment cleared amount','','','','','20','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_GL_JRNL_TO_AP_PMTS_V','','','SUM','US','','');
xxeis.eis_rsc_ins.rc( 'HDS GL Journals to AP Payments',101,'CHECK_CLEARED_DATE','Check Cleared Date','Payment cleared date','','','','','21','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_GL_JRNL_TO_AP_PMTS_V','','','GROUP_BY','US','','');
xxeis.eis_rsc_ins.rc( 'HDS GL Journals to AP Payments',101,'CHECK_CURRENCY_CODE','Check Currency Code','Currency code','','','','','22','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_GL_JRNL_TO_AP_PMTS_V','','','GROUP_BY','US','','');
xxeis.eis_rsc_ins.rc( 'HDS GL Journals to AP Payments',101,'CHECK_DOC_SEQ_VALUE','Check Doc Seq Value','Voucher number (sequential numbering) for payment','','','','','23','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_GL_JRNL_TO_AP_PMTS_V','','','SUM','US','','');
xxeis.eis_rsc_ins.rc( 'HDS GL Journals to AP Payments',101,'CHECK_NUMBER','Check Number','Payment number','','','','','24','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_GL_JRNL_TO_AP_PMTS_V','','','SUM','US','','');
xxeis.eis_rsc_ins.rc( 'HDS GL Journals to AP Payments',101,'INOICE_DATE','Inoice Date','Invoice date','','','','','25','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_GL_JRNL_TO_AP_PMTS_V','','','GROUP_BY','US','','');
xxeis.eis_rsc_ins.rc( 'HDS GL Journals to AP Payments',101,'INVOICE_AMOUNT','Invoice Amount','Invoice amount','','','','','26','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_GL_JRNL_TO_AP_PMTS_V','','','SUM','US','','');
xxeis.eis_rsc_ins.rc( 'HDS GL Journals to AP Payments',101,'INVOICE_CURRENCY_CODE','Invoice Currency Code','Currency code of invoice','','','','','27','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_GL_JRNL_TO_AP_PMTS_V','','','GROUP_BY','US','','');
xxeis.eis_rsc_ins.rc( 'HDS GL Journals to AP Payments',101,'INVOICE_DOC_SEQ_VALUE','Invoice Doc Seq Value','Voucher number (Sequential Numbering) for invoice','','','','','28','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_GL_JRNL_TO_AP_PMTS_V','','','SUM','US','','');
xxeis.eis_rsc_ins.rc( 'HDS GL Journals to AP Payments',101,'JE_CATEGORY_NAME','Je Category Name','Je Category Name','','','','','43','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_GL_JRNL_TO_AP_PMTS_V','','','GROUP_BY','US','','');
xxeis.eis_rsc_ins.rc( 'HDS GL Journals to AP Payments',101,'JE_SOURCE_NAME','Je Source Name','Journal entry source name','','','','','44','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_GL_JRNL_TO_AP_PMTS_V','','','GROUP_BY','US','','');
xxeis.eis_rsc_ins.rc( 'HDS GL Journals to AP Payments',101,'LEDGER_NAME','Ledger Name','Ledger name','','','','','29','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_GL_JRNL_TO_AP_PMTS_V','','','GROUP_BY','US','','');
xxeis.eis_rsc_ins.rc( 'HDS GL Journals to AP Payments',101,'VENDOR_NAME','Vendor Name','Supplier name','','','','','30','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_GL_JRNL_TO_AP_PMTS_V','','','GROUP_BY','US','','');
xxeis.eis_rsc_ins.rc( 'HDS GL Journals to AP Payments',101,'VENDOR_SITE','Vendor Site','Vendor Site','','','','','31','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_GL_JRNL_TO_AP_PMTS_V','','','GROUP_BY','US','','');
xxeis.eis_rsc_ins.rc( 'HDS GL Journals to AP Payments',101,'CHECK_DATE','Check Date','Payment date','','','','','54','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_GL_JRNL_TO_AP_PMTS_V','','','GROUP_BY','US','','');
xxeis.eis_rsc_ins.rc( 'HDS GL Journals to AP Payments',101,'SLA_ACCOUNT','Sla Account','Sla Account','','','','','55','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_GL_JRNL_TO_AP_PMTS_V','','','GROUP_BY','US','','');
xxeis.eis_rsc_ins.rc( 'HDS GL Journals to AP Payments',101,'SLA_ACCOUNTED_CR','Sla Accounted Cr','Sla Accounted Cr','','','','','56','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_GL_JRNL_TO_AP_PMTS_V','','','SUM','US','','');
xxeis.eis_rsc_ins.rc( 'HDS GL Journals to AP Payments',101,'SLA_ACCOUNTED_DR','Sla Accounted Dr','Sla Accounted Dr','','','','','57','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_GL_JRNL_TO_AP_PMTS_V','','','SUM','US','','');
xxeis.eis_rsc_ins.rc( 'HDS GL Journals to AP Payments',101,'SLA_ACCOUNTING_DATE','Sla Accounting Date','Accounting date','','','','','59','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_GL_JRNL_TO_AP_PMTS_V','','','GROUP_BY','US','','');
xxeis.eis_rsc_ins.rc( 'HDS GL Journals to AP Payments',101,'SLA_ACCOUNTED_NET','Sla Accounted Net','Sla Accounted Net','','','','','58','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_GL_JRNL_TO_AP_PMTS_V','','','SUM','US','','');
xxeis.eis_rsc_ins.rc( 'HDS GL Journals to AP Payments',101,'SLA_ENTERED_NET','Sla Entered Net','Sla Entered Net','','','','','64','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_GL_JRNL_TO_AP_PMTS_V','','','SUM','US','','');
xxeis.eis_rsc_ins.rc( 'HDS GL Journals to AP Payments',101,'SLA_CODE_COMBINATION_ID','Sla Code Combination Id','Sla Code Combination Id','','','','','61','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_GL_JRNL_TO_AP_PMTS_V','','','SUM','US','','');
xxeis.eis_rsc_ins.rc( 'HDS GL Journals to AP Payments',101,'SLA_ENTERED_CR','Sla Entered Cr','Unrounded Entered Credit Amount for the journal line','','','','','62','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_GL_JRNL_TO_AP_PMTS_V','','','SUM','US','','');
xxeis.eis_rsc_ins.rc( 'HDS GL Journals to AP Payments',101,'SLA_ENTERED_DR','Sla Entered Dr','Unrounded Entered Debit Amount for the journal line','','','','','63','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_GL_JRNL_TO_AP_PMTS_V','','','SUM','US','','');
xxeis.eis_rsc_ins.rc( 'HDS GL Journals to AP Payments',101,'JE_CURR_CONV_RATE','Je Curr Conv Rate','Currency exchange rate','','','','','34','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_GL_JRNL_TO_AP_PMTS_V','','','SUM','US','','');
xxeis.eis_rsc_ins.rc( 'HDS GL Journals to AP Payments',101,'JE_CURR_CONV_TYPE','Je Curr Conv Type','Type of currency exchange rate','','','','','33','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_GL_JRNL_TO_AP_PMTS_V','','','GROUP_BY','US','','');
xxeis.eis_rsc_ins.rc( 'HDS GL Journals to AP Payments',101,'SLA_EVENT_TYPE','Sla Event Type','Event type code','','','','','65','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_GL_JRNL_TO_AP_PMTS_V','','','GROUP_BY','US','','');
xxeis.eis_rsc_ins.rc( 'HDS GL Journals to AP Payments',101,'TRANSFER_DATE_FROM_SLA_TO_GL','Transfer Date From Sla To Gl','General Ledger transfer date','','','','','66','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_GL_JRNL_TO_AP_PMTS_V','','','GROUP_BY','US','','');
xxeis.eis_rsc_ins.rc( 'HDS GL Journals to AP Payments',101,'JE_CATEGORY','Je Category','JE Category','','','','','6','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_GL_JRNL_TO_AP_PMTS_V','','','GROUP_BY','US','','');
xxeis.eis_rsc_ins.rc( 'HDS GL Journals to AP Payments',101,'JE_CREATED_BY','Je Created By','Standard Who column','','','','','50','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_GL_JRNL_TO_AP_PMTS_V','','','GROUP_BY','US','','');
xxeis.eis_rsc_ins.rc( 'HDS GL Journals to AP Payments',101,'JE_CREATION_DATE','Je Creation Date','Standard Who column','','','','','37','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_GL_JRNL_TO_AP_PMTS_V','','','GROUP_BY','US','','');
xxeis.eis_rsc_ins.rc( 'HDS GL Journals to AP Payments',101,'JE_CURRENCY','Je Currency','Currency','','','','','10','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_GL_JRNL_TO_AP_PMTS_V','','','GROUP_BY','US','','');
xxeis.eis_rsc_ins.rc( 'HDS GL Journals to AP Payments',101,'JE_CURR_CONV_DATE','Je Curr Conv Date','Currency conversion date','','','','','35','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_GL_JRNL_TO_AP_PMTS_V','','','GROUP_BY','US','','');
xxeis.eis_rsc_ins.rc( 'HDS GL Journals to AP Payments',101,'JE_DESCRIPTION','Je Description','Journal entry description','','','','','4','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_GL_JRNL_TO_AP_PMTS_V','','','GROUP_BY','US','','');
xxeis.eis_rsc_ins.rc( 'HDS GL Journals to AP Payments',101,'JE_EFFECTIVE_DATE','Je Effective Date','Journal entry line effective date','','','','','11','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_GL_JRNL_TO_AP_PMTS_V','','','GROUP_BY','US','','');
xxeis.eis_rsc_ins.rc( 'HDS GL Journals to AP Payments',101,'JE_ENTERED_CR','Je Entered Cr','Journal entry line credit amount in entered currency','','','','','17','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_GL_JRNL_TO_AP_PMTS_V','','','SUM','US','','');
xxeis.eis_rsc_ins.rc( 'HDS GL Journals to AP Payments',101,'JE_ENTERED_DR','Je Entered Dr','Journal entry line debit amount in entered currency','','','','','15','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_GL_JRNL_TO_AP_PMTS_V','','','SUM','US','','');
xxeis.eis_rsc_ins.rc( 'HDS GL Journals to AP Payments',101,'JE_EXTERNAL_REF','Je External Ref','Extra reference column','','','','','53','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_GL_JRNL_TO_AP_PMTS_V','','','GROUP_BY','US','','');
xxeis.eis_rsc_ins.rc( 'HDS GL Journals to AP Payments',101,'JE_LINE_DESCRIPTION','Je Line Description','Journal entry line description','','','','','14','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_GL_JRNL_TO_AP_PMTS_V','','','GROUP_BY','US','','');
xxeis.eis_rsc_ins.rc( 'HDS GL Journals to AP Payments',101,'JE_LINE_NUM','Je Line Num','Journal entry line number','','','','','13','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_GL_JRNL_TO_AP_PMTS_V','','','SUM','US','','');
xxeis.eis_rsc_ins.rc( 'HDS GL Journals to AP Payments',101,'JE_MULTI_BAL_SEG','Je Multi Bal Seg','Multiple balancing segment flag','','','','','51','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_GL_JRNL_TO_AP_PMTS_V','','','GROUP_BY','US','','');
xxeis.eis_rsc_ins.rc( 'HDS GL Journals to AP Payments',101,'JE_NAME','Je Name','Journal entry header name','','','','','3','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_GL_JRNL_TO_AP_PMTS_V','','','GROUP_BY','US','','');
xxeis.eis_rsc_ins.rc( 'HDS GL Journals to AP Payments',101,'JE_PERIOD','Je Period','Accounting period','','','','','5','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_GL_JRNL_TO_AP_PMTS_V','','','GROUP_BY','US','','');
xxeis.eis_rsc_ins.rc( 'HDS GL Journals to AP Payments',101,'JE_POSTED_DATE','Je Posted Date','Date journal entry header was posted','','','','','38','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_GL_JRNL_TO_AP_PMTS_V','','','GROUP_BY','US','','');
xxeis.eis_rsc_ins.rc( 'HDS GL Journals to AP Payments',101,'JE_SOURCE','Je Source','Journal entry source user defined name','','','','','7','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_GL_JRNL_TO_AP_PMTS_V','','','GROUP_BY','US','','');
xxeis.eis_rsc_ins.rc( 'HDS GL Journals to AP Payments',101,'JE_STATUS','Je Status','Journal entry header status lookup code','','','','','9','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_GL_JRNL_TO_AP_PMTS_V','','','GROUP_BY','US','','');
xxeis.eis_rsc_ins.rc( 'HDS GL Journals to AP Payments',101,'JE_TYPE','Je Type','Balance type (Actual, Budget, or Encumbrance)','','','','','8','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_GL_JRNL_TO_AP_PMTS_V','','','GROUP_BY','US','','');
xxeis.eis_rsc_ins.rc( 'HDS GL Journals to AP Payments',101,'ACCRUAL_REVERSAL','Accrual Reversal','Reversed journal entry flag','','','','','45','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_GL_JRNL_TO_AP_PMTS_V','','','GROUP_BY','US','','');
xxeis.eis_rsc_ins.rc( 'HDS GL Journals to AP Payments',101,'ACCRUAL_REV_CHANGE_SIGN','Accrual Rev Change Sign','Type of reversal (Change Sign or Switch Dr/Cr)','','','','','47','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_GL_JRNL_TO_AP_PMTS_V','','','GROUP_BY','US','','');
xxeis.eis_rsc_ins.rc( 'HDS GL Journals to AP Payments',101,'ACCRUAL_REV_EFFECTIVE_DATE','Accrual Rev Effective Date','Reversed journal entry effective date','','','','','48','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_GL_JRNL_TO_AP_PMTS_V','','','GROUP_BY','US','','');
xxeis.eis_rsc_ins.rc( 'HDS GL Journals to AP Payments',101,'ACCRUAL_REV_PERIOD_NAME','Accrual Rev Period Name','Reversed journal entry reversal period','','','','','49','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_GL_JRNL_TO_AP_PMTS_V','','','GROUP_BY','US','','');
xxeis.eis_rsc_ins.rc( 'HDS GL Journals to AP Payments',101,'ACCRUAL_REV_STATUS','Accrual Rev Status','Reversed journal entry status','','','','','46','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_GL_JRNL_TO_AP_PMTS_V','','','GROUP_BY','US','','');
xxeis.eis_rsc_ins.rc( 'HDS GL Journals to AP Payments',101,'BATCH_DESCRIPTION','Batch Description','Journal entry batch description','','','','','2','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_GL_JRNL_TO_AP_PMTS_V','','','GROUP_BY','US','','');
xxeis.eis_rsc_ins.rc( 'HDS GL Journals to AP Payments',101,'BATCH_NAME','Batch Name','Name of journal entry batch','','','','','1','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_GL_JRNL_TO_AP_PMTS_V','','','GROUP_BY','US','','');
xxeis.eis_rsc_ins.rc( 'HDS GL Journals to AP Payments',101,'BUDGET_DESCRIPTION','Budget Description','Budget version description','','','','','40','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_GL_JRNL_TO_AP_PMTS_V','','','GROUP_BY','US','','');
xxeis.eis_rsc_ins.rc( 'HDS GL Journals to AP Payments',101,'BUDGET_NAME','Budget Name','Budget Name','','','','','39','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_GL_JRNL_TO_AP_PMTS_V','','','GROUP_BY','US','','');
xxeis.eis_rsc_ins.rc( 'HDS GL Journals to AP Payments',101,'BUDGET_STATUS','Budget Status','Version status lookup code','','','','','41','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_GL_JRNL_TO_AP_PMTS_V','','','GROUP_BY','US','','');
xxeis.eis_rsc_ins.rc( 'HDS GL Journals to AP Payments',101,'BUDGET_TYPE','Budget Type','Budget type(only STANDARD is used)','','','','','42','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_GL_JRNL_TO_AP_PMTS_V','','','GROUP_BY','US','','');
xxeis.eis_rsc_ins.rc( 'HDS GL Journals to AP Payments',101,'SLA_ACCOUNT_CLASS','Sla Account Class','Sla Account Class','','','','','60','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_GL_JRNL_TO_AP_PMTS_V','','','GROUP_BY','US','','');
--Inserting Report Parameters - HDS GL Journals to AP Payments
xxeis.eis_rsc_ins.rp( 'HDS GL Journals to AP Payments',101,'Period','GL Period','JE_PERIOD','IN','EIS_GL_PERIOD_LOV','','VARCHAR2','Y','Y','1','Y','Y','CONSTANT','XXEIS_RS_ADMIN','Y','N','','','','EIS_GL_JRNL_TO_AP_PMTS_V','','','US','');
xxeis.eis_rsc_ins.rp( 'HDS GL Journals to AP Payments',101,'Batch Name','GL Batch Name','BATCH_NAME','IN','EIS_GL_AP_PAYMENTS_BATCH_LOV','','VARCHAR2','N','Y','3','Y','Y','CONSTANT','XXEIS_RS_ADMIN','Y','N','','','','EIS_GL_JRNL_TO_AP_PMTS_V','','','US','');
xxeis.eis_rsc_ins.rp( 'HDS GL Journals to AP Payments',101,'Journal Name','GL Journal Name','JE_NAME','IN','EIS_GL_AP_PAYMENTS_JOURNAL_LOV','','VARCHAR2','N','Y','4','Y','Y','CONSTANT','XXEIS_RS_ADMIN','Y','N','','','','EIS_GL_JRNL_TO_AP_PMTS_V','','','US','');
xxeis.eis_rsc_ins.rp( 'HDS GL Journals to AP Payments',101,'Ledger Name','Ledger Name','LEDGER_NAME','IN','EIS_GL_LEDGERS','','VARCHAR2','N','Y','2','Y','Y','CONSTANT','XXEIS_RS_ADMIN','Y','N','','','','EIS_GL_JRNL_TO_AP_PMTS_V','','','US','');
xxeis.eis_rsc_ins.rp( 'HDS GL Journals to AP Payments',101,'GL Account String','GL Account String','GL_ACCOUNT_STRING','IN','EIS_GLACCOUNT_CONCAT_LOV','','VARCHAR2','N','Y','5','Y','Y','CONSTANT','XXEIS_RS_ADMIN','Y','Y','GL#SEGMENT','','','EIS_GL_JRNL_TO_AP_PMTS_V','','','US','');
--Inserting Dependent Parameters - HDS GL Journals to AP Payments
--Inserting Report Conditions - HDS GL Journals to AP Payments
xxeis.eis_rsc_ins.rcnh( 'HDS GL Journals to AP Payments',101,'BATCH_NAME IN :Batch Name ','SIMPLE','','','Y','','1');
xxeis.eis_rsc_ins.rcnd( '','','BATCH_NAME','','Batch Name','','','','','EIS_GL_JRNL_TO_AP_PMTS_V','','','','','','IN','Y','Y','','','','','1',101,'HDS GL Journals to AP Payments','BATCH_NAME IN :Batch Name ');
xxeis.eis_rsc_ins.rcnh( 'HDS GL Journals to AP Payments',101,'GL_ACCOUNT_STRING IN :GL Account String ','SIMPLE','','','Y','','2');
xxeis.eis_rsc_ins.rcnd( '','','GL_ACCOUNT_STRING','','GL Account String','','','','','EIS_GL_JRNL_TO_AP_PMTS_V','','','','','','IN','Y','Y','','','','','1',101,'HDS GL Journals to AP Payments','GL_ACCOUNT_STRING IN :GL Account String ');
xxeis.eis_rsc_ins.rcnh( 'HDS GL Journals to AP Payments',101,'JE_NAME IN :Journal Name ','SIMPLE','','','Y','','3');
xxeis.eis_rsc_ins.rcnd( '','','JE_NAME','','Journal Name','','','','','EIS_GL_JRNL_TO_AP_PMTS_V','','','','','','IN','Y','Y','','','','','1',101,'HDS GL Journals to AP Payments','JE_NAME IN :Journal Name ');
xxeis.eis_rsc_ins.rcnh( 'HDS GL Journals to AP Payments',101,'JE_PERIOD IN :Period ','SIMPLE','','','Y','','4');
xxeis.eis_rsc_ins.rcnd( '','','JE_PERIOD','','Period','','','','','EIS_GL_JRNL_TO_AP_PMTS_V','','','','','','IN','Y','Y','','','','','1',101,'HDS GL Journals to AP Payments','JE_PERIOD IN :Period ');
xxeis.eis_rsc_ins.rcnh( 'HDS GL Journals to AP Payments',101,'LEDGER_NAME IN :Ledger Name ','SIMPLE','','','Y','','5');
xxeis.eis_rsc_ins.rcnd( '','','LEDGER_NAME','','Ledger Name','','','','','EIS_GL_JRNL_TO_AP_PMTS_V','','','','','','IN','Y','Y','','','','','1',101,'HDS GL Journals to AP Payments','LEDGER_NAME IN :Ledger Name ');
--Inserting Report Sorts - HDS GL Journals to AP Payments
--Inserting Report Triggers - HDS GL Journals to AP Payments
--inserting report templates - HDS GL Journals to AP Payments
xxeis.eis_rsc_ins.r_tem( 'HDS GL Journals to AP Payments','HDS GL Journals to AP Payments','Seeded template for HDS GL Journals to AP Payments','','','','','','','','','','','HDS GL Journals to AP Payments.rtf','XXEIS_RS_ADMIN','X','','','Y','Y','','');
--Inserting Report Portals - HDS GL Journals to AP Payments
--inserting report dashboards - HDS GL Journals to AP Payments
xxeis.eis_rsc_ins.R_dash( 'HDS GL Journals to AP Payments','GL Journals to AP Payments','GL Journals to AP Payments','pie','large','Supplier','Supplier','Entered Dr','Entered Dr','Sum','XXEIS_RS_ADMIN');
--Exporting report views table information -  
xxeis.eis_rsc_ins.rviews( 'HDS GL Journals to AP Payments','101','EIS_GL_JRNL_TO_AP_PMTS_V','EIS_GL_JRNL_TO_AP_PMTS_V','N','');
--inserting report security - HDS GL Journals to AP Payments
xxeis.eis_rsc_ins.rsec( 'HDS GL Journals to AP Payments','101','','XXCUS_GL_ACCOUNTANT_CAD',101,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS GL Journals to AP Payments','101','','XXCUS_GL_ACCOUNTANT_CAD_PVF',101,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS GL Journals to AP Payments','101','','XXCUS_GL_ACCOUNTANT_GLOBAL',101,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS GL Journals to AP Payments','101','','GNRL_LDGR_LTMR_ACCNTNT',101,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS GL Journals to AP Payments','101','','XXCUS_GL_ACCOUNTANT_USD',101,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS GL Journals to AP Payments','101','','XXCUS_GL_ACCOUNTANT_USD_PVF',101,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS GL Journals to AP Payments','101','','XXCUS_GL_INQUIRY',101,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS GL Journals to AP Payments','101','','HDS GL INQUIRY',101,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS GL Journals to AP Payments','101','','GNRL_LDGR_LTMR_NQR',101,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS GL Journals to AP Payments','101','','XXCUS_GL_INQUIRY_PVF',101,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS GL Journals to AP Payments','101','','XXCUS_GL_MANAGER',101,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS GL Journals to AP Payments','101','','XXCUS_GL_MANAGER_GLOBAL',101,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS GL Journals to AP Payments','101','','XXCUS_GL_MANAGER_PVF',101,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS GL Journals to AP Payments','101','','HDS_CAD_MNTH_END_PROCS',101,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS GL Journals to AP Payments','101','','GNRL_LDGR_FSS',101,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS GL Journals to AP Payments','101','','GNRL_LDGR_LTMR_FSS',101,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS GL Journals to AP Payments','101','','HDS_GNRL_LDGR_CAD',101,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS GL Journals to AP Payments','101','','HDS_GNRL_LDGR_SPR_USR',101,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS GL Journals to AP Payments','20005','','XXWC_VIEW_ALL_EIS_REPORTS',101,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS GL Journals to AP Payments','101','','XXCUS_GL_MANAGER_PS',101,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS GL Journals to AP Payments','101','','XXCUS_GL_INQUIRY_PS',101,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS GL Journals to AP Payments','101','','XXCUS_GL_ACCOUNTANT_USD_PS',101,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS GL Journals to AP Payments','101','','XXCUS_GL_ACCOUNTANT_CAD_PS',101,'XXEIS_RS_ADMIN','','','');
--Inserting Report Pivots - HDS GL Journals to AP Payments
xxeis.eis_rsc_ins.rpivot( 'HDS GL Journals to AP Payments',101,'Pivot','1','1,0|1,2,1','1,1,0,0|None|2');
--Inserting Report Pivot Details For Pivot - Pivot
xxeis.eis_rsc_ins.rpivot_dtls( 'HDS GL Journals to AP Payments',101,'Pivot','GL_ACCOUNT_STRING','ROW_FIELD','','GL Account String','5','1','xlNormal','');
xxeis.eis_rsc_ins.rpivot_dtls( 'HDS GL Journals to AP Payments',101,'Pivot','INVOICE_NUM','PAGE_FIELD','','','','1','xlNormal','');
xxeis.eis_rsc_ins.rpivot_dtls( 'HDS GL Journals to AP Payments',101,'Pivot','CHECK_NUMBER','PAGE_FIELD','','','','1','xlNormal','');
xxeis.eis_rsc_ins.rpivot_dtls( 'HDS GL Journals to AP Payments',101,'Pivot','LEDGER_NAME','ROW_FIELD','','','1','1','xlNormal','');
xxeis.eis_rsc_ins.rpivot_dtls( 'HDS GL Journals to AP Payments',101,'Pivot','SLA_ACCOUNTED_CR','DATA_FIELD','SUM','','2','1','xlNormal','');
xxeis.eis_rsc_ins.rpivot_dtls( 'HDS GL Journals to AP Payments',101,'Pivot','SLA_ACCOUNTED_DR','DATA_FIELD','SUM','','1','1','xlNormal','');
xxeis.eis_rsc_ins.rpivot_dtls( 'HDS GL Journals to AP Payments',101,'Pivot','SLA_ACCOUNTED_NET','DATA_FIELD','SUM','','3','1','xlNormal','');
xxeis.eis_rsc_ins.rpivot_dtls( 'HDS GL Journals to AP Payments',101,'Pivot','JE_CURRENCY','ROW_FIELD','','','2','1','xlNormal','');
xxeis.eis_rsc_ins.rpivot_dtls( 'HDS GL Journals to AP Payments',101,'Pivot','JE_LINE_NUM','ROW_FIELD','','','4','1','xlNormal','');
xxeis.eis_rsc_ins.rpivot_dtls( 'HDS GL Journals to AP Payments',101,'Pivot','JE_NAME','PAGE_FIELD','','','','1','xlNormal','');
xxeis.eis_rsc_ins.rpivot_dtls( 'HDS GL Journals to AP Payments',101,'Pivot','JE_PERIOD','PAGE_FIELD','','','','1','xlNormal','');
xxeis.eis_rsc_ins.rpivot_dtls( 'HDS GL Journals to AP Payments',101,'Pivot','JE_STATUS','PAGE_FIELD','','','','1','xlNormal','');
xxeis.eis_rsc_ins.rpivot_dtls( 'HDS GL Journals to AP Payments',101,'Pivot','JE_TYPE','PAGE_FIELD','','','','1','xlNormal','');
xxeis.eis_rsc_ins.rpivot_dtls( 'HDS GL Journals to AP Payments',101,'Pivot','BATCH_NAME','ROW_FIELD','','','3','1','xlNormal','');
--Inserting Report Summary Calculation Columns For Pivot- Pivot
--Inserting Report Distribution Details 
--Inserting Report Distribution Details 
--Inserting Report SubTotals Details 
--Inserting Report aggregations 
--Inserting Report   Version details- HDS GL Journals to AP Payments
xxeis.eis_rsc_ins.rv( 'HDS GL Journals to AP Payments','','HDS GL Journals to AP Payments','AB065961','27-JUN-2017');
ELSE
RAISE_APPLICATION_ERROR (-20001,'Module does not exist. Import/Create the module with id 101');
END IF;
END;
/
