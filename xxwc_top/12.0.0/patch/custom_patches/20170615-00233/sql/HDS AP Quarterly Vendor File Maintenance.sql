--Report Name            : HDS AP Quarterly Vendor File Maintenance
--Import Instructions    : Run this file in XXEIS user from SQL*Plus or any tool like Toad, SQL Navigator. 
prompt Creating Object Data XXEIS_VENDOR_FILE_M
set scan off define off
DECLARE
mod_exist VARCHAR2(1);
BEGIN 
mod_exist := XXEIS.EIS_RSC_INSERT.check_if_module_exist(200);
IF mod_exist = 'Y' THEN 
--Inserting Object XXEIS_VENDOR_FILE_M
xxeis.eis_rsc_ins.v( 'XXEIS_VENDOR_FILE_M',200,'Paste SQL View for HDS AP Vendor File Maintenance','1.0','','','XXEIS_RS_ADMIN','APPS','XXEIS_VENDOR_FILE_M','X4BV1','','','VIEW','US','','','');
--Delete Object Columns for XXEIS_VENDOR_FILE_M
xxeis.eis_rsc_utility.delete_view_rows('XXEIS_VENDOR_FILE_M',200,FALSE);
--Inserting Object Columns for XXEIS_VENDOR_FILE_M
xxeis.eis_rsc_ins.vc( 'XXEIS_VENDOR_FILE_M','VENDOR_NAME',200,'','','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Vendor Name','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_VENDOR_FILE_M','VENDOR_NUMBER',200,'','','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Vendor Number','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_VENDOR_FILE_M','VENDOR_SITE_CODE',200,'','','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Vendor Site Code','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_VENDOR_FILE_M','TAX_ID',200,'','','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Tax Id','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_VENDOR_FILE_M','ADDRESS_LINE1',200,'','','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Address Line1','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_VENDOR_FILE_M','ADDRESS_LINE2',200,'','','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Address Line2','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_VENDOR_FILE_M','CITY',200,'','','','','','XXEIS_RS_ADMIN','VARCHAR2','','','City','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_VENDOR_FILE_M','STATE',200,'','','','','','XXEIS_RS_ADMIN','VARCHAR2','','','State','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_VENDOR_FILE_M','ZIP',200,'','','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Zip','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_VENDOR_FILE_M','PAY_GROUP_LOOKUP_CODE',200,'','','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Pay Group Lookup Code','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_VENDOR_FILE_M','ORG_ID',200,'','','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Org Id','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_VENDOR_FILE_M','PRIMARY_LOB',200,'','','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Primary Lob','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_VENDOR_FILE_M','REMITTANCE_EMAIL',200,'','','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Remittance Email','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_VENDOR_FILE_M','TYPE_1099',200,'','','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Type 1099','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_VENDOR_FILE_M','VENDOR_TYPE_LOOKUP_CODE',200,'','','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Vendor Type Lookup Code','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_VENDOR_FILE_M','INACTIVE_DATE',200,'','','','','','XXEIS_RS_ADMIN','DATE','','','Inactive Date','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_VENDOR_FILE_M','INVOICE_DATE',200,'','','','','','XXEIS_RS_ADMIN','DATE','','','Invoice Date','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_VENDOR_FILE_M','FIRST_NAME',200,'','','','','','XXEIS_RS_ADMIN','VARCHAR2','','','First Name','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_VENDOR_FILE_M','LAST_NAME',200,'','','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Last Name','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_VENDOR_FILE_M','PHONE',200,'','','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Phone','','','','US','');
--Inserting Object Components for XXEIS_VENDOR_FILE_M
--Inserting Object Component Joins for XXEIS_VENDOR_FILE_M
ELSE
RAISE_APPLICATION_ERROR (-20001,'Module does not exist. Import/Create the module with id 200');
END IF;
END;
/
--There are no LOV/LOV TL entries for the report HDS AP Quarterly Vendor File Maintenance
prompt Creating Report Data for HDS AP Quarterly Vendor File Maintenance
set scan off define off
DECLARE
mod_exist varchar2(1);
BEGIN 
mod_exist := XXEIS.EIS_RSC_INSERT.check_if_module_exist(200);
IF mod_exist = 'Y' THEN 
--Deleting Report data - HDS AP Quarterly Vendor File Maintenance
xxeis.eis_rsc_utility.delete_report_rows( 'HDS AP Quarterly Vendor File Maintenance',200 );
--Inserting Report - HDS AP Quarterly Vendor File Maintenance
xxeis.eis_rsc_ins.r( 200,'HDS AP Quarterly Vendor File Maintenance','','','','','','XXEIS_RS_ADMIN','XXEIS_VENDOR_FILE_M','Y','','','XXEIS_RS_ADMIN','','N','HDS Standard Reports','','CSV,Pivot Excel,EXCEL,','Y','','','','','','','','US','','','','','','','','','','','','','','','','');
--Inserting Report Columns - HDS AP Quarterly Vendor File Maintenance
xxeis.eis_rsc_ins.rc( 'HDS AP Quarterly Vendor File Maintenance',200,'ADDRESS_LINE1','Address Line1','','','','default','','5','N','Y','','','','','','','XXEIS_RS_ADMIN','N','N','','XXEIS_VENDOR_FILE_M','','','GROUP_BY','US','','');
xxeis.eis_rsc_ins.rc( 'HDS AP Quarterly Vendor File Maintenance',200,'ADDRESS_LINE2','Address Line2','','','','default','','6','N','Y','','','','','','','XXEIS_RS_ADMIN','N','N','','XXEIS_VENDOR_FILE_M','','','GROUP_BY','US','','');
xxeis.eis_rsc_ins.rc( 'HDS AP Quarterly Vendor File Maintenance',200,'CITY','City','','','','default','','7','N','Y','','','','','','','XXEIS_RS_ADMIN','N','N','','XXEIS_VENDOR_FILE_M','','','GROUP_BY','US','','');
xxeis.eis_rsc_ins.rc( 'HDS AP Quarterly Vendor File Maintenance',200,'PAY_GROUP_LOOKUP_CODE','Pay Group Lookup Code','','','','default','','10','N','Y','','','','','','','XXEIS_RS_ADMIN','N','N','','XXEIS_VENDOR_FILE_M','','','GROUP_BY','US','','');
xxeis.eis_rsc_ins.rc( 'HDS AP Quarterly Vendor File Maintenance',200,'STATE','State','','','','default','','8','N','Y','','','','','','','XXEIS_RS_ADMIN','N','N','','XXEIS_VENDOR_FILE_M','','','GROUP_BY','US','','');
xxeis.eis_rsc_ins.rc( 'HDS AP Quarterly Vendor File Maintenance',200,'TAX_ID','Tax Id','','','','default','','4','N','Y','','','','','','','XXEIS_RS_ADMIN','N','N','','XXEIS_VENDOR_FILE_M','','','GROUP_BY','US','','');
xxeis.eis_rsc_ins.rc( 'HDS AP Quarterly Vendor File Maintenance',200,'VENDOR_NAME','Vendor Name','','','','default','','1','N','Y','','','','','','','XXEIS_RS_ADMIN','N','N','','XXEIS_VENDOR_FILE_M','','','GROUP_BY','US','','');
xxeis.eis_rsc_ins.rc( 'HDS AP Quarterly Vendor File Maintenance',200,'VENDOR_NUMBER','Vendor Number','','','','default','','2','N','Y','','','','','','','XXEIS_RS_ADMIN','N','N','','XXEIS_VENDOR_FILE_M','','','GROUP_BY','US','','');
xxeis.eis_rsc_ins.rc( 'HDS AP Quarterly Vendor File Maintenance',200,'VENDOR_SITE_CODE','Vendor Site Code','','','','default','','3','N','Y','','','','','','','XXEIS_RS_ADMIN','N','N','','XXEIS_VENDOR_FILE_M','','','GROUP_BY','US','','');
xxeis.eis_rsc_ins.rc( 'HDS AP Quarterly Vendor File Maintenance',200,'ZIP','Zip','','','','default','','9','N','Y','','','','','','','XXEIS_RS_ADMIN','N','N','','XXEIS_VENDOR_FILE_M','','','GROUP_BY','US','','');
xxeis.eis_rsc_ins.rc( 'HDS AP Quarterly Vendor File Maintenance',200,'INVOICE_DATE','Last_Invoice_Date','','','','default','','12','','Y','','','','','','','XXEIS_RS_ADMIN','N','N','','XXEIS_VENDOR_FILE_M','','','MAX','US','','');
--Inserting Report Parameters - HDS AP Quarterly Vendor File Maintenance
--Inserting Dependent Parameters - HDS AP Quarterly Vendor File Maintenance
--Inserting Report Conditions - HDS AP Quarterly Vendor File Maintenance
xxeis.eis_rsc_ins.rcnh( 'HDS AP Quarterly Vendor File Maintenance',200,'ORG_ID = ''163'' ','ADVANCED','','1#$#','Y','','1');
xxeis.eis_rsc_ins.rcnd( '','','','','','','','','','','','','','','','EQUALS','Y','N','ORG_ID','''163''','','','1',200,'HDS AP Quarterly Vendor File Maintenance','ORG_ID = ''163'' ');
xxeis.eis_rsc_ins.rcnh( 'HDS AP Quarterly Vendor File Maintenance',200,'VENDOR_SITE_CODE NOT IN ''OFFICE'' ','ADVANCED','','1#$#','Y','','2');
xxeis.eis_rsc_ins.rcnd( '','','VENDOR_SITE_CODE','','','','','','','XXEIS_VENDOR_FILE_M','','','','','','NOTIN','Y','N','','''OFFICE''','','','1',200,'HDS AP Quarterly Vendor File Maintenance','VENDOR_SITE_CODE NOT IN ''OFFICE'' ');
xxeis.eis_rsc_ins.rcnh( 'HDS AP Quarterly Vendor File Maintenance',200,'Free Text ','FREE_TEXT','','','Y','','3');
xxeis.eis_rsc_ins.rcnd( '','','','','','','','','','','','','','','','','Y','','','','','AND (X4BV1.INVOICE_DATE >= (SYSDATE - 546))
AND X4BV1.INACTIVE_DATE IS NULL','1',200,'HDS AP Quarterly Vendor File Maintenance','Free Text ');
--Inserting Report Sorts - HDS AP Quarterly Vendor File Maintenance
--Inserting Report Triggers - HDS AP Quarterly Vendor File Maintenance
--inserting report templates - HDS AP Quarterly Vendor File Maintenance
--Inserting Report Portals - HDS AP Quarterly Vendor File Maintenance
--inserting report dashboards - HDS AP Quarterly Vendor File Maintenance
--Exporting report views table information -  
xxeis.eis_rsc_ins.rviews( 'HDS AP Quarterly Vendor File Maintenance','200','XXEIS_VENDOR_FILE_M','XXEIS_VENDOR_FILE_M','N','');
--inserting report security - HDS AP Quarterly Vendor File Maintenance
xxeis.eis_rsc_ins.rsec( 'HDS AP Quarterly Vendor File Maintenance','200','','',200,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS AP Quarterly Vendor File Maintenance','200','','',200,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS AP Quarterly Vendor File Maintenance','200','','PAYABLES_MANAGER',200,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS AP Quarterly Vendor File Maintenance','200','','',200,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS AP Quarterly Vendor File Maintenance','200','','PAYABLES_INQUIRY',200,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS AP Quarterly Vendor File Maintenance','200','','',200,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS AP Quarterly Vendor File Maintenance','200','','',200,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS AP Quarterly Vendor File Maintenance','200','','HDS_AP_INQUIRY_US_GSC',200,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS AP Quarterly Vendor File Maintenance','200','','HDS_AP_DISBURSEMTS_US_GSC',200,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS AP Quarterly Vendor File Maintenance','200','','',200,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS AP Quarterly Vendor File Maintenance','200','','',200,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS AP Quarterly Vendor File Maintenance','200','','',200,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS AP Quarterly Vendor File Maintenance','200','','HDS_PYABLS_MNGR_CAN',200,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS AP Quarterly Vendor File Maintenance','200','','',200,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS AP Quarterly Vendor File Maintenance','200','','HDS_AP_SUPPLIER_MAINT_US_GSC',200,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS AP Quarterly Vendor File Maintenance','200','','',200,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS AP Quarterly Vendor File Maintenance','200','','HDS_OIE_USER',200,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS AP Quarterly Vendor File Maintenance','200','','HDS_AP_ADMIN_US_GSC',200,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS AP Quarterly Vendor File Maintenance','200','','HDS_AP_MGR_NOSUP_US_GSC',200,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS AP Quarterly Vendor File Maintenance','200','','HDS_AP_TRNS_ENTRY_US_GSC',200,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS AP Quarterly Vendor File Maintenance','200','','HDS_AP_ADMIN_US_GSCIWO',200,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS AP Quarterly Vendor File Maintenance','200','','HDS_AP_ADMIN_US_IWO',200,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS AP Quarterly Vendor File Maintenance','200','','HDS_AP_DISBUREMTS_US_IWO',200,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS AP Quarterly Vendor File Maintenance','200','','HDS_AP_INQUIRY_US_IWO',200,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS AP Quarterly Vendor File Maintenance','200','','HDS_AP_TRNS_ENTRY_US_IWO',200,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS AP Quarterly Vendor File Maintenance','200','','XXWC_PAY_NO_CALENDAR',200,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS AP Quarterly Vendor File Maintenance','200','','XXWC_PAY_W_CALENDAR',200,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS AP Quarterly Vendor File Maintenance','200','','XXWC_PAY_DISBURSE',200,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS AP Quarterly Vendor File Maintenance','200','','XXWC_PAYABLES_INQUIRY',200,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS AP Quarterly Vendor File Maintenance','200','','XXWC_PAY_MANAGER',200,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS AP Quarterly Vendor File Maintenance','200','','XXWC_PAY_VENDOR_BANK_DETAILS',200,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS AP Quarterly Vendor File Maintenance','200','','XXWC_PAY_VENDOR_MSTR',200,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS AP Quarterly Vendor File Maintenance','200','','XXWC_PAY_VENDOR_MSTR_INQUIRY',200,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS AP Quarterly Vendor File Maintenance','200','','HDS_AP_MGR_NOSUP_US_IWO',200,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS AP Quarterly Vendor File Maintenance','200','','HDS_AP_MANAGER',200,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS AP Quarterly Vendor File Maintenance','200','','HDS_PYBLS_MNGR',200,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS AP Quarterly Vendor File Maintenance','20005','','XXWC_VIEW_ALL_EIS_REPORTS',200,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS AP Quarterly Vendor File Maintenance','200','','HDS_AP_TRNS_ENTRY_CAD_GSC',200,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS AP Quarterly Vendor File Maintenance','200','','HDS_AP_SUPPLIER_MAINT_CAD_GSC',200,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS AP Quarterly Vendor File Maintenance','200','','HDS_AP_MGR_NOSUP_CAD_GSC',200,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS AP Quarterly Vendor File Maintenance','200','','HDS_AP_DISBURSEMTS_CAD_GSC',200,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS AP Quarterly Vendor File Maintenance','200','','HDS_AP_ADMIN_CAD_GSC',200,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS AP Quarterly Vendor File Maintenance','200','','HDS_PAYABLES_CLOSE_GSC_GLBL',200,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS AP Quarterly Vendor File Maintenance','200','','HDS_PAYABLES_CLOSE_GLBL',200,'XXEIS_RS_ADMIN','','','');
--Inserting Report Pivots - HDS AP Quarterly Vendor File Maintenance
--Inserting Report Distribution Details 
--Inserting Report Distribution Details 
--Inserting Report SubTotals Details 
--Inserting Report aggregations 
--Inserting Report   Version details- HDS AP Quarterly Vendor File Maintenance
xxeis.eis_rsc_ins.rv( 'HDS AP Quarterly Vendor File Maintenance','','HDS AP Quarterly Vendor File Maintenance','AB065961','27-JUN-2017');
ELSE
RAISE_APPLICATION_ERROR (-20001,'Module does not exist. Import/Create the module with id 200');
END IF;
END;
/
