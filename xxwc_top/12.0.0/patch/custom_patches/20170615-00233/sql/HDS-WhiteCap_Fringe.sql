--Report Name            : HDS-WhiteCap_Fringe
--Import Instructions    : Run this file in XXEIS user from SQL*Plus or any tool like Toad, SQL Navigator. 
prompt Creating Object Data EIS_GL_JOURNAL_DETAILS_V
set scan off define off
DECLARE
mod_exist VARCHAR2(1);
BEGIN 
mod_exist := XXEIS.EIS_RSC_INSERT.check_if_module_exist(101);
IF mod_exist = 'Y' THEN 
--Inserting Object EIS_GL_JOURNAL_DETAILS_V
xxeis.eis_rsc_ins.v( 'EIS_GL_JOURNAL_DETAILS_V',101,'This view shows details of the general ledger journal detail information.','','','','XXEIS_RS_ADMIN','XXEIS','EIS GL Journal Details','EGJDV','','','VIEW','US','Y','','');
--Delete Object Columns for EIS_GL_JOURNAL_DETAILS_V
xxeis.eis_rsc_utility.delete_view_rows('EIS_GL_JOURNAL_DETAILS_V',101,FALSE);
--Inserting Object Columns for EIS_GL_JOURNAL_DETAILS_V
xxeis.eis_rsc_ins.vc( 'EIS_GL_JOURNAL_DETAILS_V','JE_DESCRIPTION',101,'Journal entry description','JE_DESCRIPTION','','','','XXEIS_RS_ADMIN','VARCHAR2','GL_JE_HEADERS','DESCRIPTION','Je Description','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_GL_JOURNAL_DETAILS_V','JE_CURRENCY',101,'Currency','JE_CURRENCY','','','','XXEIS_RS_ADMIN','VARCHAR2','GL_JE_HEADERS','CURRENCY_CODE','Je Currency','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_GL_JOURNAL_DETAILS_V','JE_STATUS',101,'Journal entry header status lookup code','JE_STATUS','','','','XXEIS_RS_ADMIN','VARCHAR2','GL_JE_HEADERS','STATUS','Je Status','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_GL_JOURNAL_DETAILS_V','JE_TYPE',101,'Balance type (Actual, Budget, or Encumbrance)','JE_TYPE','','','GL Journal Entry Types LOV','XXEIS_RS_ADMIN','VARCHAR2','GL_JE_HEADERS','ACTUAL_FLAG','Je Type','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_GL_JOURNAL_DETAILS_V','BATCH_NAME',101,'Name of journal entry batch','BATCH_NAME','','','GL Batches LOV','XXEIS_RS_ADMIN','VARCHAR2','GL_JE_BATCHES','NAME','Batch Name','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_GL_JOURNAL_DETAILS_V','JE_POSTED_DATE',101,'Date journal entry header was posted','JE_POSTED_DATE','','','','XXEIS_RS_ADMIN','DATE','GL_JE_HEADERS','POSTED_DATE','Je Posted Date','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_GL_JOURNAL_DETAILS_V','JE_CREATION_DATE',101,'Standard Who column','JE_CREATION_DATE','','','','XXEIS_RS_ADMIN','DATE','GL_JE_HEADERS','CREATION_DATE','Je Creation Date','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_GL_JOURNAL_DETAILS_V','JE_CREATED_BY',101,'Standard Who column','JE_CREATED_BY','','','','XXEIS_RS_ADMIN','VARCHAR2','GL_JE_HEADERS','CREATED_BY','Je Created By','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_GL_JOURNAL_DETAILS_V','JE_BALANCED',101,'Balanced journal entry flag','JE_BALANCED','','','','XXEIS_RS_ADMIN','VARCHAR2','GL_JE_HEADERS','BALANCED_JE_FLAG','Je Balanced','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_GL_JOURNAL_DETAILS_V','ACCRUAL_REVERSAL',101,'Reversed journal entry flag','ACCRUAL_REVERSAL','','','','XXEIS_RS_ADMIN','VARCHAR2','GL_JE_HEADERS','ACCRUAL_REV_FLAG','Accrual Reversal','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_GL_JOURNAL_DETAILS_V','JE_MULTI_BAL_SEG',101,'Multiple balancing segment flag','JE_MULTI_BAL_SEG','','','','XXEIS_RS_ADMIN','VARCHAR2','GL_JE_HEADERS','MULTI_BAL_SEG_FLAG','Je Multi Bal Seg','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_GL_JOURNAL_DETAILS_V','ACCRUAL_REV_EFFECTIVE_DATE',101,'Reversed journal entry effective date','ACCRUAL_REV_EFFECTIVE_DATE','','','','XXEIS_RS_ADMIN','DATE','GL_JE_HEADERS','ACCRUAL_REV_EFFECTIVE_DATE','Accrual Rev Effective Date','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_GL_JOURNAL_DETAILS_V','ACCRUAL_REV_PERIOD_NAME',101,'Reversed journal entry reversal period','ACCRUAL_REV_PERIOD_NAME','','','','XXEIS_RS_ADMIN','VARCHAR2','GL_JE_HEADERS','ACCRUAL_REV_PERIOD_NAME','Accrual Rev Period Name','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_GL_JOURNAL_DETAILS_V','ACCRUAL_REV_STATUS',101,'Reversed journal entry status','ACCRUAL_REV_STATUS','','','','XXEIS_RS_ADMIN','VARCHAR2','GL_JE_HEADERS','ACCRUAL_REV_STATUS','Accrual Rev Status','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_GL_JOURNAL_DETAILS_V','ACCRUAL_REV_CHANGE_SIGN',101,'Type of reversal (Change Sign or Switch Dr/Cr)','ACCRUAL_REV_CHANGE_SIGN','','','','XXEIS_RS_ADMIN','VARCHAR2','GL_JE_HEADERS','ACCRUAL_REV_CHANGE_SIGN_FLAG','Accrual Rev Change Sign','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_GL_JOURNAL_DETAILS_V','JE_CURR_CONV_RATE',101,'Currency exchange rate','JE_CURR_CONV_RATE','','~T~D~2','','XXEIS_RS_ADMIN','NUMBER','GL_JE_HEADERS','CURRENCY_CONVERSION_RATE','Je Curr Conv Rate','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_GL_JOURNAL_DETAILS_V','JE_CURR_CONV_TYPE',101,'Type of currency exchange rate','JE_CURR_CONV_TYPE','','','','XXEIS_RS_ADMIN','VARCHAR2','GL_JE_HEADERS','CURRENCY_CONVERSION_TYPE','Je Curr Conv Type','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_GL_JOURNAL_DETAILS_V','JE_CURR_CONV_DATE',101,'Currency conversion date','JE_CURR_CONV_DATE','','','','XXEIS_RS_ADMIN','DATE','GL_JE_HEADERS','CURRENCY_CONVERSION_DATE','Je Curr Conv Date','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_GL_JOURNAL_DETAILS_V','JE_ACCOUNTED_DR',101,'Journal entry line debit amount in base currency','JE_ACCOUNTED_DR','','~T~D~2','','XXEIS_RS_ADMIN','NUMBER','GL_JE_LINES','ACCOUNTED_DR','Je Accounted Dr','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_GL_JOURNAL_DETAILS_V','JE_ACCOUNTED_CR',101,'Journal entry line credit amount in base currency','JE_ACCOUNTED_CR','','~T~D~2','','XXEIS_RS_ADMIN','NUMBER','GL_JE_LINES','ACCOUNTED_CR','Je Accounted Cr','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_GL_JOURNAL_DETAILS_V','JE_LINE_DESCRIPTION',101,'Journal entry line description','JE_LINE_DESCRIPTION','','','','XXEIS_RS_ADMIN','VARCHAR2','GL_JE_LINES','DESCRIPTION','Je Line Description','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_GL_JOURNAL_DETAILS_V','GL_ACCOUNT_STRING',101,'GL Account String','GL_ACCOUNT_STRING','','','GL Accounts LOV','XXEIS_RS_ADMIN','VARCHAR2','CALCULATION COLUMN','CALCULATION COLUMN','Gl Account String','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_GL_JOURNAL_DETAILS_V','BUDGET_NAME',101,'Budget Name','BUDGET_NAME','','','','XXEIS_RS_ADMIN','VARCHAR2','GL_BUDGET_VERSIONS','BUDGET_NAME','Budget Name','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_GL_JOURNAL_DETAILS_V','BUDGET_TYPE',101,'Budget type(only STANDARD is used)','BUDGET_TYPE','','','','XXEIS_RS_ADMIN','VARCHAR2','GL_BUDGET_VERSIONS','BUDGET_TYPE','Budget Type','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_GL_JOURNAL_DETAILS_V','BUDGET_STATUS',101,'Version status lookup code','BUDGET_STATUS','','','','XXEIS_RS_ADMIN','VARCHAR2','GL_BUDGET_VERSIONS','STATUS','Budget Status','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_GL_JOURNAL_DETAILS_V','LEDGER_NAME',101,'Ledger name','LEDGER_NAME~LEDGER_NAME','','','','XXEIS_RS_ADMIN','VARCHAR2','GL_LEDGERS','NAME','Ledger Name','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_GL_JOURNAL_DETAILS_V','JE_NAME',101,'Journal entry header name','JE_NAME','','','GL Journal Names LOV','XXEIS_RS_ADMIN','VARCHAR2','GL_JE_HEADERS','NAME','Je Name','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_GL_JOURNAL_DETAILS_V','BUDGET_DESCRIPTION',101,'Budget version description','BUDGET_DESCRIPTION','','','','XXEIS_RS_ADMIN','VARCHAR2','GL_BUDGET_VERSIONS','DESCRIPTION','Budget Description','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_GL_JOURNAL_DETAILS_V','ENCUMBRANCE_TYPE',101,'Encumbrance type name','ENCUMBRANCE_TYPE','','','','XXEIS_RS_ADMIN','VARCHAR2','GL_ENCUMBRANCE_TYPES','ENCUMBRANCE_TYPE','Encumbrance Type','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_GL_JOURNAL_DETAILS_V','JE_EXTERNAL_REF',101,'Extra reference column','JE_EXTERNAL_REF','','','','XXEIS_RS_ADMIN','VARCHAR2','GL_JE_HEADERS','EXTERNAL_REFERENCE','Je External Ref','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_GL_JOURNAL_DETAILS_V','JE_LINE_NUM',101,'Journal entry line number','JE_LINE_NUM','','','','XXEIS_RS_ADMIN','NUMBER','GL_JE_LINES','JE_LINE_NUM','Je Line Num','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_GL_JOURNAL_DETAILS_V','JE_EFFECTIVE_DATE',101,'Journal entry line effective date','JE_EFFECTIVE_DATE','','','','XXEIS_RS_ADMIN','DATE','GL_JE_LINES','EFFECTIVE_DATE','Je Effective Date','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_GL_JOURNAL_DETAILS_V','JE_ENTERED_DR',101,'Journal entry line debit amount in entered currency','JE_ENTERED_DR','','~T~D~2','','XXEIS_RS_ADMIN','NUMBER','GL_JE_LINES','ENTERED_DR','Je Entered Dr','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_GL_JOURNAL_DETAILS_V','JE_ENTERED_CR',101,'Journal entry line credit amount in entered currency','JE_ENTERED_CR','','~T~D~2','','XXEIS_RS_ADMIN','NUMBER','GL_JE_LINES','ENTERED_CR','Je Entered Cr','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_GL_JOURNAL_DETAILS_V','JE_BATCH_ID',101,'Journal entry batch defining column','JE_BATCH_ID','','','','XXEIS_RS_ADMIN','NUMBER','GL_JE_BATCHES','JE_BATCH_ID','Je Batch Id','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_GL_JOURNAL_DETAILS_V','BATCH_DESCRIPTION',101,'Journal entry batch description','BATCH_DESCRIPTION','','','','XXEIS_RS_ADMIN','VARCHAR2','GL_JE_BATCHES','DESCRIPTION','Batch Description','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_GL_JOURNAL_DETAILS_V','JE_SOURCE',101,'Journal entry source user defined name','JE_SOURCE','','','GL Journal Sources LOV','XXEIS_RS_ADMIN','VARCHAR2','GL_JE_SOURCES_TL','USER_JE_SOURCE_NAME','Je Source','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_GL_JOURNAL_DETAILS_V','JE_CATEGORY',101,'JE Category','JE_CATEGORY','','','','XXEIS_RS_ADMIN','VARCHAR2','GL_JE_CATEGORIES','USER_JE_CATEGORY_NAME','Je Category','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_GL_JOURNAL_DETAILS_V','JE_PERIOD',101,'Accounting period','JE_PERIOD','','','GL Periods LOV','XXEIS_RS_ADMIN','VARCHAR2','GL_JE_HEADERS','PERIOD_NAME','Je Period','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_GL_JOURNAL_DETAILS_V','ACCRUAL_REV_JE_HEADER_ID',101,'Reversed journal entry defining column','ACCRUAL_REV_JE_HEADER_ID','','','','XXEIS_RS_ADMIN','NUMBER','GL_JE_HEADERS','ACCRUAL_REV_JE_HEADER_ID','Accrual Rev Je Header Id','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_GL_JOURNAL_DETAILS_V','REVERSED_JE_HEADER_ID',101,'Defining column of the journal entry that is reversed by this journal entry','REVERSED_JE_HEADER_ID','','','','XXEIS_RS_ADMIN','NUMBER','GL_JE_HEADERS','REVERSED_JE_HEADER_ID','Reversed Je Header Id','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_GL_JOURNAL_DETAILS_V','CODE_COMBINATION_ID',101,'Key flexfield combination defining column','CODE_COMBINATION_ID','','','','XXEIS_RS_ADMIN','NUMBER','GL_CODE_COMBINATIONS','CODE_COMBINATION_ID','Code Combination Id','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_GL_JOURNAL_DETAILS_V','ENCUMBRANCE_TYPE_ID',101,'Encumbrance type defining column','ENCUMBRANCE_TYPE_ID~ENCUMBRANCE_TYPE_ID','','','','XXEIS_RS_ADMIN','NUMBER','GL_ENCUMBRANCE_TYPES','ENCUMBRANCE_TYPE_ID','Encumbrance Type Id','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_GL_JOURNAL_DETAILS_V','JE_CATEGORY_NAME',101,'Journal entry category user defined name','JE_CATEGORY_NAME~JE_CATEGORY_NAME','','','','XXEIS_RS_ADMIN','VARCHAR2','GL_JE_CATEGORIES_TL','USER_JE_CATEGORY_NAME','Je Category Name','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_GL_JOURNAL_DETAILS_V','JE_SOURCE_NAME',101,'Journal entry source user defined name','JE_SOURCE_NAME~JE_SOURCE_NAME','','','','XXEIS_RS_ADMIN','VARCHAR2','GL_JE_SOURCES_TL','USER_JE_SOURCE_NAME','Je Source Name','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_GL_JOURNAL_DETAILS_V','LEDGER_ID',101,'Ledger defining column','LEDGER_ID~LEDGER_ID','','','','XXEIS_RS_ADMIN','NUMBER','GL_LEDGERS','LEDGER_ID','Ledger Id','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_GL_JOURNAL_DETAILS_V','BUDGET_VERSION_ID',101,'Budget version defining column','BUDGET_VERSION_ID','','','','XXEIS_RS_ADMIN','NUMBER','GL_BUDGET_VERSIONS','BUDGET_VERSION_ID','Budget Version Id','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_GL_JOURNAL_DETAILS_V','JE_HEADER_ID',101,'Journal entry header defining column','JE_HEADER_ID','','','','XXEIS_RS_ADMIN','NUMBER','GL_JE_HEADERS','JE_HEADER_ID','Je Header Id','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_GL_JOURNAL_DETAILS_V','ACCOUNTED_NET',101,'Accounted Net','ACCOUNTED_NET','','~T~D~2','','XXEIS_RS_ADMIN','NUMBER','CALCULATION COLUMN','CALCULATION COLUMN','Accounted Net','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_GL_JOURNAL_DETAILS_V','ENTERED_NET',101,'Entered Net','ENTERED_NET','','~T~D~2','','XXEIS_RS_ADMIN','NUMBER','CALCULATION COLUMN','CALCULATION COLUMN','Entered Net','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_GL_JOURNAL_DETAILS_V','GCC#50328#PRODUCT',101,'Accounting Flexfield (KFF): Segment ''Product'' under chart of accounts ''XXCUS Accounting Flexfield''','GCC#50328#PRODUCT','','','','XXEIS_RS_ADMIN','VARCHAR2','GL_CODE_COMBINATIONS','SEGMENT1','GCC#Product','50328','1014547','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_GL_JOURNAL_DETAILS_V','GCC#50328#PRODUCT#DESCR',101,'Accounting Flexfield (KFF) : Segment ''Product'' under chart of accounts ''XXCUS Accounting Flexfield''','GCC#50328#PRODUCT#DESCR','','','','XXEIS_RS_ADMIN','VARCHAR2','GL_CODE_COMBINATIONS','SEGMENT1','GCC#Product Descr','50328','1014547','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_GL_JOURNAL_DETAILS_V','GCC#50328#LOCATION',101,'Accounting Flexfield (KFF): Segment ''Location'' under chart of accounts ''XXCUS Accounting Flexfield''','GCC#50328#LOCATION','','','','XXEIS_RS_ADMIN','VARCHAR2','GL_CODE_COMBINATIONS','SEGMENT2','GCC#Location','50328','1014548','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_GL_JOURNAL_DETAILS_V','GCC#50328#LOCATION#DESCR',101,'Accounting Flexfield (KFF) : Segment ''Location'' under chart of accounts ''XXCUS Accounting Flexfield''','GCC#50328#LOCATION#DESCR','','','','XXEIS_RS_ADMIN','VARCHAR2','GL_CODE_COMBINATIONS','SEGMENT2','GCC#Location Descr','50328','1014548','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_GL_JOURNAL_DETAILS_V','GCC#50328#COST_CENTER',101,'Accounting Flexfield (KFF): Segment ''Cost Center'' under chart of accounts ''XXCUS Accounting Flexfield''','GCC#50328#COST_CENTER','','','','XXEIS_RS_ADMIN','VARCHAR2','GL_CODE_COMBINATIONS','SEGMENT3','GCC#Cost Center','50328','1014549','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_GL_JOURNAL_DETAILS_V','GCC#50328#COST_CENTER#DESCR',101,'Accounting Flexfield (KFF) : Segment ''Cost Center'' under chart of accounts ''XXCUS Accounting Flexfield''','GCC#50328#COST_CENTER#DESCR','','','','XXEIS_RS_ADMIN','VARCHAR2','GL_CODE_COMBINATIONS','SEGMENT3','GCC#Cost Center Descr','50328','1014549','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_GL_JOURNAL_DETAILS_V','GCC#50328#ACCOUNT',101,'Accounting Flexfield (KFF): Segment ''Account'' under chart of accounts ''XXCUS Accounting Flexfield''','GCC#50328#ACCOUNT','','','','XXEIS_RS_ADMIN','VARCHAR2','GL_CODE_COMBINATIONS','SEGMENT4','GCC#Account','50328','1014550','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_GL_JOURNAL_DETAILS_V','GCC#50328#ACCOUNT#DESCR',101,'Accounting Flexfield (KFF) : Segment ''Account'' under chart of accounts ''XXCUS Accounting Flexfield''','GCC#50328#ACCOUNT#DESCR','','','','XXEIS_RS_ADMIN','VARCHAR2','GL_CODE_COMBINATIONS','SEGMENT4','GCC#Account Descr','50328','1014550','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_GL_JOURNAL_DETAILS_V','GCC#50328#PROJECT_CODE',101,'Accounting Flexfield (KFF): Segment ''Project Code'' under chart of accounts ''XXCUS Accounting Flexfield''','GCC#50328#PROJECT_CODE','','','','XXEIS_RS_ADMIN','VARCHAR2','GL_CODE_COMBINATIONS','SEGMENT5','GCC#Project Code','50328','1014551','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_GL_JOURNAL_DETAILS_V','GCC#50328#PROJECT_CODE#DESCR',101,'Accounting Flexfield (KFF) : Segment ''Project Code'' under chart of accounts ''XXCUS Accounting Flexfield''','GCC#50328#PROJECT_CODE#DESCR','','','','XXEIS_RS_ADMIN','VARCHAR2','GL_CODE_COMBINATIONS','SEGMENT5','GCC#Project Code Descr','50328','1014551','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_GL_JOURNAL_DETAILS_V','GCC#50328#FURTURE_USE',101,'Accounting Flexfield : Segment ''Furture Use'' under chart of accounts ''XXCUS Accounting Flexfield''','GCC#50328#FURTURE_USE','','','','XXEIS_RS_ADMIN','VARCHAR2','GL_CODE_COMBINATIONS','SEGMENT6','GCC#Furture Use','50328','1014552','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_GL_JOURNAL_DETAILS_V','GCC#50328#FURTURE_USE#DESCR',101,'Accounting Flexfield : Segment ''Furture Use'' under chart of accounts ''XXCUS Accounting Flexfield''','GCC#50328#FURTURE_USE#DESCR','','','','XXEIS_RS_ADMIN','VARCHAR2','GL_CODE_COMBINATIONS','SEGMENT6','GCC#Furture Use Descr','50328','1014552','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_GL_JOURNAL_DETAILS_V','GCC#50328#FUTURE_USE_2',101,'Accounting Flexfield (KFF): Segment ''Future Use 2'' under chart of accounts ''XXCUS Accounting Flexfield''','GCC#50328#FUTURE_USE_2','','','','XXEIS_RS_ADMIN','VARCHAR2','GL_CODE_COMBINATIONS','SEGMENT7','GCC#Future Use 2','50328','1014948','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_GL_JOURNAL_DETAILS_V','GCC#50328#FUTURE_USE_2#DESCR',101,'Accounting Flexfield (KFF) : Segment ''Future Use 2'' under chart of accounts ''XXCUS Accounting Flexfield''','GCC#50328#FUTURE_USE_2#DESCR','','','','XXEIS_RS_ADMIN','VARCHAR2','GL_CODE_COMBINATIONS','SEGMENT7','GCC#Future Use 2 Descr','50328','1014948','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_GL_JOURNAL_DETAILS_V','GCC#50368#PRODUCT',101,'Accounting Flexfield (KFF): Segment ''Product'' under chart of accounts ''XXCUS HDS Canada Litemor - COA''','GCC#50368#PRODUCT','','','','XXEIS_RS_ADMIN','VARCHAR2','GL_CODE_COMBINATIONS','SEGMENT1','GCC#Product','50368','1014596','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_GL_JOURNAL_DETAILS_V','GCC#50368#PRODUCT#DESCR',101,'Accounting Flexfield (KFF) : Segment ''Product'' under chart of accounts ''XXCUS HDS Canada Litemor - COA''','GCC#50368#PRODUCT#DESCR','','','','XXEIS_RS_ADMIN','VARCHAR2','GL_CODE_COMBINATIONS','SEGMENT1','GCC#Product Descr','50368','1014596','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_GL_JOURNAL_DETAILS_V','GCC#50368#DIVISION',101,'Accounting Flexfield (KFF): Segment ''Division'' under chart of accounts ''XXCUS HDS Canada Litemor - COA''','GCC#50368#DIVISION','','','','XXEIS_RS_ADMIN','VARCHAR2','GL_CODE_COMBINATIONS','SEGMENT2','GCC#Division','50368','1014597','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_GL_JOURNAL_DETAILS_V','GCC#50368#DIVISION#DESCR',101,'Accounting Flexfield (KFF) : Segment ''Division'' under chart of accounts ''XXCUS HDS Canada Litemor - COA''','GCC#50368#DIVISION#DESCR','','','','XXEIS_RS_ADMIN','VARCHAR2','GL_CODE_COMBINATIONS','SEGMENT2','GCC#Division Descr','50368','1014597','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_GL_JOURNAL_DETAILS_V','GCC#50368#DEPARTMENT',101,'Accounting Flexfield (KFF): Segment ''Department'' under chart of accounts ''XXCUS HDS Canada Litemor - COA''','GCC#50368#DEPARTMENT','','','','XXEIS_RS_ADMIN','VARCHAR2','GL_CODE_COMBINATIONS','SEGMENT3','GCC#Department','50368','1014598','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_GL_JOURNAL_DETAILS_V','GCC#50368#DEPARTMENT#DESCR',101,'Accounting Flexfield (KFF) : Segment ''Department'' under chart of accounts ''XXCUS HDS Canada Litemor - COA''','GCC#50368#DEPARTMENT#DESCR','','','','XXEIS_RS_ADMIN','VARCHAR2','GL_CODE_COMBINATIONS','SEGMENT3','GCC#Department Descr','50368','1014598','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_GL_JOURNAL_DETAILS_V','GCC#50368#ACCOUNT',101,'Accounting Flexfield (KFF): Segment ''Account'' under chart of accounts ''XXCUS HDS Canada Litemor - COA''','GCC#50368#ACCOUNT','','','','XXEIS_RS_ADMIN','VARCHAR2','GL_CODE_COMBINATIONS','SEGMENT4','GCC#Account','50368','1014599','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_GL_JOURNAL_DETAILS_V','GCC#50368#ACCOUNT#DESCR',101,'Accounting Flexfield (KFF) : Segment ''Account'' under chart of accounts ''XXCUS HDS Canada Litemor - COA''','GCC#50368#ACCOUNT#DESCR','','','','XXEIS_RS_ADMIN','VARCHAR2','GL_CODE_COMBINATIONS','SEGMENT4','GCC#Account Descr','50368','1014599','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_GL_JOURNAL_DETAILS_V','GCC#50368#SUBACCOUNT',101,'Accounting Flexfield (KFF): Segment ''SubAccount'' under chart of accounts ''XXCUS HDS Canada Litemor - COA''','GCC#50368#SUBACCOUNT','','','','XXEIS_RS_ADMIN','VARCHAR2','GL_CODE_COMBINATIONS','SEGMENT5','GCC#SubAccount','50368','1014600','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_GL_JOURNAL_DETAILS_V','GCC#50368#SUBACCOUNT#DESCR',101,'Accounting Flexfield (KFF) : Segment ''SubAccount'' under chart of accounts ''XXCUS HDS Canada Litemor - COA''','GCC#50368#SUBACCOUNT#DESCR','','','','XXEIS_RS_ADMIN','VARCHAR2','GL_CODE_COMBINATIONS','SEGMENT5','GCC#SubAccount Descr','50368','1014600','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_GL_JOURNAL_DETAILS_V','GCC#50368#FUTURE_USE',101,'Accounting Flexfield (KFF): Segment ''Future Use'' under chart of accounts ''XXCUS HDS Canada Litemor - COA''','GCC#50368#FUTURE_USE','','','','XXEIS_RS_ADMIN','VARCHAR2','GL_CODE_COMBINATIONS','SEGMENT6','GCC#Future Use','50368','1014601','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_GL_JOURNAL_DETAILS_V','GCC#50368#FUTURE_USE#DESCR',101,'Accounting Flexfield (KFF) : Segment ''Future Use'' under chart of accounts ''XXCUS HDS Canada Litemor - COA''','GCC#50368#FUTURE_USE#DESCR','','','','XXEIS_RS_ADMIN','VARCHAR2','GL_CODE_COMBINATIONS','SEGMENT6','GCC#Future Use Descr','50368','1014601','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_GL_JOURNAL_DETAILS_V','POSTED_BY',101,'Posted By','POSTED_BY','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Posted By','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_GL_JOURNAL_DETAILS_V','ACCOUNTED_SUM',101,'Accounted Sum','ACCOUNTED_SUM','','~T~D~2','','XXEIS_RS_ADMIN','NUMBER','','','Accounted Sum','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_GL_JOURNAL_DETAILS_V','ENTERED_SUM',101,'Entered Sum','ENTERED_SUM','','','','XXEIS_RS_ADMIN','NUMBER','','','Entered Sum','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_GL_JOURNAL_DETAILS_V','GL#SEGMENT1',101,'Gl#Segment1','GL#SEGMENT1','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Gl#Segment1','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_GL_JOURNAL_DETAILS_V','GL#SEGMENT2',101,'Gl#Segment2','GL#SEGMENT2','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Gl#Segment2','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_GL_JOURNAL_DETAILS_V','GL#SEGMENT3',101,'Gl#Segment3','GL#SEGMENT3','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Gl#Segment3','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_GL_JOURNAL_DETAILS_V','GL#SEGMENT4',101,'Gl#Segment4','GL#SEGMENT4','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Gl#Segment4','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_GL_JOURNAL_DETAILS_V','GL#SEGMENT5',101,'Gl#Segment5','GL#SEGMENT5','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Gl#Segment5','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_GL_JOURNAL_DETAILS_V','GL#SEGMENT6',101,'Gl#Segment6','GL#SEGMENT6','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Gl#Segment6','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_GL_JOURNAL_DETAILS_V','GL#SEGMENT7',101,'Gl#Segment7','GL#SEGMENT7','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Gl#Segment7','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_GL_JOURNAL_DETAILS_V','GL#SEGMENT8',101,'Gl#Segment8','GL#SEGMENT8','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Gl#Segment8','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_GL_JOURNAL_DETAILS_V','GLE_ENCUMBRANCE_TYPE_ID',101,'Gle Encumbrance Type Id','GLE_ENCUMBRANCE_TYPE_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Gle Encumbrance Type Id','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_GL_JOURNAL_DETAILS_V','GL_ACCOUNT_TYPE',101,'Gl Account Type','GL_ACCOUNT_TYPE','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Gl Account Type','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_GL_JOURNAL_DETAILS_V','GL_SL_LINK_ID',101,'Gl Sl Link Id','GL_SL_LINK_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Gl Sl Link Id','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_GL_JOURNAL_DETAILS_V','GL_SL_LINK_TABLE',101,'Gl Sl Link Table','GL_SL_LINK_TABLE','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Gl Sl Link Table','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_GL_JOURNAL_DETAILS_V','JEC_JE_CATEGORY_NAME',101,'Jec Je Category Name','JEC_JE_CATEGORY_NAME','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Jec Je Category Name','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_GL_JOURNAL_DETAILS_V','JES_JE_SOURCE_NAME',101,'Jes Je Source Name','JES_JE_SOURCE_NAME','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Jes Je Source Name','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_GL_JOURNAL_DETAILS_V','JL_JE_HEADER_ID',101,'Jl Je Header Id','JL_JE_HEADER_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Jl Je Header Id','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_GL_JOURNAL_DETAILS_V','PERIOD_END_DATE',101,'Period End Date','PERIOD_END_DATE','','','','XXEIS_RS_ADMIN','DATE','','','Period End Date','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_GL_JOURNAL_DETAILS_V','PERIOD_SET_NAME',101,'Period Set Name','PERIOD_SET_NAME','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Period Set Name','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_GL_JOURNAL_DETAILS_V','PERIOD_START_DATE',101,'Period Start Date','PERIOD_START_DATE','','','','XXEIS_RS_ADMIN','DATE','','','Period Start Date','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_GL_JOURNAL_DETAILS_V','PERIOD_TYPE',101,'Period Type','PERIOD_TYPE','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Period Type','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_GL_JOURNAL_DETAILS_V','REFERENCE_1',101,'Reference 1','REFERENCE_1','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Reference 1','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_GL_JOURNAL_DETAILS_V','REFERENCE_10',101,'Reference 10','REFERENCE_10','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Reference 10','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_GL_JOURNAL_DETAILS_V','REFERENCE_2',101,'Reference 2','REFERENCE_2','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Reference 2','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_GL_JOURNAL_DETAILS_V','REFERENCE_3',101,'Reference 3','REFERENCE_3','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Reference 3','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_GL_JOURNAL_DETAILS_V','REFERENCE_4',101,'Reference 4','REFERENCE_4','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Reference 4','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_GL_JOURNAL_DETAILS_V','REFERENCE_5',101,'Reference 5','REFERENCE_5','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Reference 5','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_GL_JOURNAL_DETAILS_V','REFERENCE_6',101,'Reference 6','REFERENCE_6','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Reference 6','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_GL_JOURNAL_DETAILS_V','REFERENCE_7',101,'Reference 7','REFERENCE_7','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Reference 7','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_GL_JOURNAL_DETAILS_V','REFERENCE_8',101,'Reference 8','REFERENCE_8','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Reference 8','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_GL_JOURNAL_DETAILS_V','REFERENCE_9',101,'Reference 9','REFERENCE_9','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Reference 9','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_GL_JOURNAL_DETAILS_V','SET_OF_BOOKS',101,'Set Of Books','SET_OF_BOOKS','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Set Of Books','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_GL_JOURNAL_DETAILS_V','SET_OF_BOOKS_ID',101,'Set Of Books Id','SET_OF_BOOKS_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Set Of Books Id','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_GL_JOURNAL_DETAILS_V','COPYRIGHT',101,'Copyright','COPYRIGHT','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Copyright','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_GL_JOURNAL_DETAILS_V','GCC#50328#FUTURE_USE',101,'Accounting Flexfield (KFF): Segment ''Future Use'' under chart of accounts ''XXCUS Accounting Flexfield''','GCC#50328#FUTURE_USE','','','','XXEIS_RS_ADMIN','VARCHAR2','GL_CODE_COMBINATIONS','SEGMENT6','GCC#Future Use','50328','1014552','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_GL_JOURNAL_DETAILS_V','GCC#50328#FUTURE_USE#DESCR',101,'Accounting Flexfield (KFF) : Segment ''Future Use'' under chart of accounts ''XXCUS Accounting Flexfield''','GCC#50328#FUTURE_USE#DESCR','','','','XXEIS_RS_ADMIN','VARCHAR2','GL_CODE_COMBINATIONS','SEGMENT6','GCC#Future Use Descr','50328','1014552','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_GL_JOURNAL_DETAILS_V','GCC#Branch',101,'Descriptive flexfield (DFF): GL Accounts Column Name: Branch','GCC#Branch','','','','XXEIS_RS_ADMIN','VARCHAR2','GL_CODE_COMBINATIONS','ATTRIBUTE1','Gcc#Branch','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_GL_JOURNAL_DETAILS_V','JL#Account',101,'Descriptive flexfield (DFF): Enter Journals: Lines Column Name: Account','JL#Account','','','','XXEIS_RS_ADMIN','VARCHAR2','GL_JE_LINES','ATTRIBUTE3','Jl#Account','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_GL_JOURNAL_DETAILS_V','JL#Branch',101,'Descriptive flexfield (DFF): Enter Journals: Lines Column Name: Branch','JL#Branch','','','','XXEIS_RS_ADMIN','VARCHAR2','GL_JE_LINES','ATTRIBUTE1','Jl#Branch','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_GL_JOURNAL_DETAILS_V','JL#Dept',101,'Descriptive flexfield (DFF): Enter Journals: Lines Column Name: Dept','JL#Dept','','','','XXEIS_RS_ADMIN','VARCHAR2','GL_JE_LINES','ATTRIBUTE2','Jl#Dept','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_GL_JOURNAL_DETAILS_V','JL#POS_Branch',101,'Descriptive flexfield (DFF): Enter Journals: Lines Column Name: POS Branch','JL#POS_Branch','','','','XXEIS_RS_ADMIN','VARCHAR2','GL_JE_LINES','ATTRIBUTE5','Jl#Pos Branch','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_GL_JOURNAL_DETAILS_V','JL#Sub_Acct',101,'Descriptive flexfield (DFF): Enter Journals: Lines Column Name: Sub Acct','JL#Sub_Acct','','','','XXEIS_RS_ADMIN','VARCHAR2','GL_JE_LINES','ATTRIBUTE4','Jl#Sub Acct','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_GL_JOURNAL_DETAILS_V','JL#WC_Formula',101,'Descriptive flexfield (DFF): Enter Journals: Lines Column Name: WC Formula','JL#WC_Formula','','','','XXEIS_RS_ADMIN','VARCHAR2','GL_JE_LINES','ATTRIBUTE6','Jl#Wc Formula','','','','US','');
--Inserting Object Components for EIS_GL_JOURNAL_DETAILS_V
xxeis.eis_rsc_ins.vcomp( 'EIS_GL_JOURNAL_DETAILS_V','GL_JE_BATCHES',101,'GL_JE_BATCHES','JB','JB','XXEIS_RS_ADMIN','XXEIS_RS_ADMIN','-1','Journal Entry Batches','','','','','','Y',' ',' ',' ',' ',' ');
xxeis.eis_rsc_ins.vcomp( 'EIS_GL_JOURNAL_DETAILS_V','GL_CODE_COMBINATIONS_KFV',101,'GL_CODE_COMBINATIONS','GCC','GCC','XXEIS_RS_ADMIN','XXEIS_RS_ADMIN','-1','GL Code Combinations','','','','','','Y',' ',' ',' ',' ',' ');
xxeis.eis_rsc_ins.vcomp( 'EIS_GL_JOURNAL_DETAILS_V','GL_LEDGERS',101,'GL_LEDGERS','LE','LE','XXEIS_RS_ADMIN','XXEIS_RS_ADMIN','-1','Ledgers','','','','','','Y',' ',' ',' ',' ',' ');
xxeis.eis_rsc_ins.vcomp( 'EIS_GL_JOURNAL_DETAILS_V','GL_JE_HEADERS',101,'GL_JE_HEADERS','JH','JH','XXEIS_RS_ADMIN','XXEIS_RS_ADMIN','-1','Journal Entry Headers','','','','','','Y',' ',' ',' ',' ',' ');
xxeis.eis_rsc_ins.vcomp( 'EIS_GL_JOURNAL_DETAILS_V','GL_JE_LINES',101,'GL_JE_LINES','JL','JL','XXEIS_RS_ADMIN','XXEIS_RS_ADMIN','-1','Journal Entry Lines','','','','','','Y',' ',' ',' ',' ',' ');
xxeis.eis_rsc_ins.vcomp( 'EIS_GL_JOURNAL_DETAILS_V','GL_BUDGET_VERSIONS',101,'GL_BUDGET_VERSIONS','GBV','GBV','XXEIS_RS_ADMIN','XXEIS_RS_ADMIN','-1','GL Budget Versions','','','','','','Y',' ',' ',' ',' ',' ');
xxeis.eis_rsc_ins.vcomp( 'EIS_GL_JOURNAL_DETAILS_V','GL_ENCUMBRANCE_TYPES',101,'GL_ENCUMBRANCE_TYPES','GLE','GLE','XXEIS_RS_ADMIN','XXEIS_RS_ADMIN','-1','Encumbrance Type Definitions','','','','','','Y',' ',' ',' ',' ',' ');
xxeis.eis_rsc_ins.vcomp( 'EIS_GL_JOURNAL_DETAILS_V','GL_JE_SOURCES',101,'GL_JE_SOURCES','JES','JES','XXEIS_RS_ADMIN','XXEIS_RS_ADMIN','-1','Journal Entry Sources','','','','','','Y',' ',' ',' ',' ',' ');
xxeis.eis_rsc_ins.vcomp( 'EIS_GL_JOURNAL_DETAILS_V','GL_JE_CATEGORIES',101,'GL_JE_CATEGORIES','JEC','JEC','XXEIS_RS_ADMIN','XXEIS_RS_ADMIN','-1','Journal Entry Categories','','','','','','Y',' ',' ',' ',' ',' ');
--Inserting Object Component Joins for EIS_GL_JOURNAL_DETAILS_V
xxeis.eis_rsc_ins.vcj( 'EIS_GL_JOURNAL_DETAILS_V','GL_JE_SOURCES','JES',101,'EGJDV.JE_SOURCE_NAME','=','JES.JE_SOURCE_NAME(+)','','','','','XXEIS_RS_ADMIN');
xxeis.eis_rsc_ins.vcj( 'EIS_GL_JOURNAL_DETAILS_V','GL_JE_CATEGORIES','JEC',101,'EGJDV.JE_CATEGORY_NAME','=','JEC.JE_CATEGORY_NAME(+)','','','','','XXEIS_RS_ADMIN');
xxeis.eis_rsc_ins.vcj( 'EIS_GL_JOURNAL_DETAILS_V','GL_LEDGERS','LE',101,'EGJDV.LEDGER_NAME','=','LE.NAME(+)','','','','Y','XXEIS_RS_ADMIN');
xxeis.eis_rsc_ins.vcj( 'EIS_GL_JOURNAL_DETAILS_V','GL_JE_HEADERS','JH',101,'EGJDV.JE_HEADER_ID','=','JH.JE_HEADER_ID(+)','','','','Y','XXEIS_RS_ADMIN');
xxeis.eis_rsc_ins.vcj( 'EIS_GL_JOURNAL_DETAILS_V','GL_JE_BATCHES','JB',101,'EGJDV.JE_BATCH_ID','=','JB.JE_BATCH_ID(+)','','','','Y','XXEIS_RS_ADMIN');
xxeis.eis_rsc_ins.vcj( 'EIS_GL_JOURNAL_DETAILS_V','GL_JE_LINES','JL',101,'EGJDV.JE_LINE_NUM','=','JL.JE_LINE_NUM(+)','','','1','Y','XXEIS_RS_ADMIN');
xxeis.eis_rsc_ins.vcj( 'EIS_GL_JOURNAL_DETAILS_V','GL_JE_LINES','JL',101,'EGJDV.JL_JE_HEADER_ID','=','JL.JE_HEADER_ID(+)','','','2','Y','XXEIS_RS_ADMIN');
xxeis.eis_rsc_ins.vcj( 'EIS_GL_JOURNAL_DETAILS_V','GL_BUDGET_VERSIONS','GBV',101,'EGJDV.BUDGET_VERSION_ID','=','GBV.BUDGET_VERSION_ID(+)','','','','Y','XXEIS_RS_ADMIN');
xxeis.eis_rsc_ins.vcj( 'EIS_GL_JOURNAL_DETAILS_V','GL_ENCUMBRANCE_TYPES','GLE',101,'EGJDV.ENCUMBRANCE_TYPE_ID','=','GLE.ENCUMBRANCE_TYPE_ID(+)','','','','Y','XXEIS_RS_ADMIN');
xxeis.eis_rsc_ins.vcj( 'EIS_GL_JOURNAL_DETAILS_V','GL_CODE_COMBINATIONS_KFV','GCC',101,'EGJDV.CODE_COMBINATION_ID','=','GCC.CODE_COMBINATION_ID(+)','','','','','XXEIS_RS_ADMIN');
ELSE
RAISE_APPLICATION_ERROR (-20001,'Module does not exist. Import/Create the module with id 101');
END IF;
END;
/
--Exporting View Source View Component -  EIS_GL_JOURNAL_DETAILS_V
--Exporting View Component Data of the View -  EIS_GL_JOURNAL_DETAILS_V
prompt Creating Object Data GL_CODE_COMBINATIONS_KFV
set scan off define off
DECLARE
mod_exist VARCHAR2(1);
BEGIN 
mod_exist := XXEIS.EIS_RSC_INSERT.check_if_module_exist(101);
IF mod_exist = 'Y' THEN 
--Inserting Object GL_CODE_COMBINATIONS_KFV
xxeis.eis_rsc_ins.v( 'GL_CODE_COMBINATIONS_KFV',101,'Account combinations','1.0','','','ANONYMOUS','APPS','Gl Code Combinations Kfv','GCCK','','','VIEW','US','','','');
--Delete Object Columns for GL_CODE_COMBINATIONS_KFV
xxeis.eis_rsc_utility.delete_view_rows('GL_CODE_COMBINATIONS_KFV',101,FALSE);
--Inserting Object Columns for GL_CODE_COMBINATIONS_KFV
xxeis.eis_rsc_ins.vc( 'GL_CODE_COMBINATIONS_KFV','ATTRIBUTE3',101,'Descriptive flexfield segment','ATTRIBUTE3','','','','ANONYMOUS','VARCHAR2','GL_CODE_COMBINATIONS_KFV','ATTRIBUTE3','Attribute3','','','','US','');
xxeis.eis_rsc_ins.vc( 'GL_CODE_COMBINATIONS_KFV','ATTRIBUTE2',101,'Descriptive flexfield segment','ATTRIBUTE2','','','','ANONYMOUS','VARCHAR2','GL_CODE_COMBINATIONS_KFV','ATTRIBUTE2','Attribute2','','','','US','');
xxeis.eis_rsc_ins.vc( 'GL_CODE_COMBINATIONS_KFV','ATTRIBUTE1',101,'Descriptive flexfield segment','ATTRIBUTE1','','','','ANONYMOUS','VARCHAR2','GL_CODE_COMBINATIONS_KFV','ATTRIBUTE1','Attribute1','','','','US','');
xxeis.eis_rsc_ins.vc( 'GL_CODE_COMBINATIONS_KFV','END_DATE_ACTIVE',101,'Date after which key flexfield combination is invalid','END_DATE_ACTIVE','','','','ANONYMOUS','DATE','GL_CODE_COMBINATIONS_KFV','END_DATE_ACTIVE','End Date Active','','','','US','');
xxeis.eis_rsc_ins.vc( 'GL_CODE_COMBINATIONS_KFV','START_DATE_ACTIVE',101,'Date before which key flexfield combination is invalid','START_DATE_ACTIVE','','','','ANONYMOUS','DATE','GL_CODE_COMBINATIONS_KFV','START_DATE_ACTIVE','Start Date Active','','','','US','');
xxeis.eis_rsc_ins.vc( 'GL_CODE_COMBINATIONS_KFV','ALLOCATION_CREATE_FLAG',101,'Not currently used','ALLOCATION_CREATE_FLAG','','','','ANONYMOUS','VARCHAR2','GL_CODE_COMBINATIONS_KFV','ALLOCATION_CREATE_FLAG','Allocation Create Flag','','','','US','');
xxeis.eis_rsc_ins.vc( 'GL_CODE_COMBINATIONS_KFV','TEMPLATE_ID',101,'Summary template defining column','TEMPLATE_ID','','','','ANONYMOUS','NUMBER','GL_CODE_COMBINATIONS_KFV','TEMPLATE_ID','Template Id','','','','US','');
xxeis.eis_rsc_ins.vc( 'GL_CODE_COMBINATIONS_KFV','DESCRIPTION',101,'Key flexfield description','DESCRIPTION','','','','ANONYMOUS','VARCHAR2','GL_CODE_COMBINATIONS_KFV','DESCRIPTION','Description','','','','US','');
xxeis.eis_rsc_ins.vc( 'GL_CODE_COMBINATIONS_KFV','SEGMENT30',101,'Key flexfield segment','SEGMENT30','','','','ANONYMOUS','VARCHAR2','GL_CODE_COMBINATIONS_KFV','SEGMENT30','Segment30','','','','US','');
xxeis.eis_rsc_ins.vc( 'GL_CODE_COMBINATIONS_KFV','SEGMENT29',101,'Key flexfield segment','SEGMENT29','','','','ANONYMOUS','VARCHAR2','GL_CODE_COMBINATIONS_KFV','SEGMENT29','Segment29','','','','US','');
xxeis.eis_rsc_ins.vc( 'GL_CODE_COMBINATIONS_KFV','SEGMENT28',101,'Key flexfield segment','SEGMENT28','','','','ANONYMOUS','VARCHAR2','GL_CODE_COMBINATIONS_KFV','SEGMENT28','Segment28','','','','US','');
xxeis.eis_rsc_ins.vc( 'GL_CODE_COMBINATIONS_KFV','SEGMENT27',101,'Key flexfield segment','SEGMENT27','','','','ANONYMOUS','VARCHAR2','GL_CODE_COMBINATIONS_KFV','SEGMENT27','Segment27','','','','US','');
xxeis.eis_rsc_ins.vc( 'GL_CODE_COMBINATIONS_KFV','SEGMENT26',101,'Key flexfield segment','SEGMENT26','','','','ANONYMOUS','VARCHAR2','GL_CODE_COMBINATIONS_KFV','SEGMENT26','Segment26','','','','US','');
xxeis.eis_rsc_ins.vc( 'GL_CODE_COMBINATIONS_KFV','SEGMENT25',101,'Key flexfield segment','SEGMENT25','','','','ANONYMOUS','VARCHAR2','GL_CODE_COMBINATIONS_KFV','SEGMENT25','Segment25','','','','US','');
xxeis.eis_rsc_ins.vc( 'GL_CODE_COMBINATIONS_KFV','SEGMENT24',101,'Key flexfield segment','SEGMENT24','','','','ANONYMOUS','VARCHAR2','GL_CODE_COMBINATIONS_KFV','SEGMENT24','Segment24','','','','US','');
xxeis.eis_rsc_ins.vc( 'GL_CODE_COMBINATIONS_KFV','SEGMENT23',101,'Key flexfield segment','SEGMENT23','','','','ANONYMOUS','VARCHAR2','GL_CODE_COMBINATIONS_KFV','SEGMENT23','Segment23','','','','US','');
xxeis.eis_rsc_ins.vc( 'GL_CODE_COMBINATIONS_KFV','SEGMENT22',101,'Key flexfield segment','SEGMENT22','','','','ANONYMOUS','VARCHAR2','GL_CODE_COMBINATIONS_KFV','SEGMENT22','Segment22','','','','US','');
xxeis.eis_rsc_ins.vc( 'GL_CODE_COMBINATIONS_KFV','SEGMENT21',101,'Key flexfield segment','SEGMENT21','','','','ANONYMOUS','VARCHAR2','GL_CODE_COMBINATIONS_KFV','SEGMENT21','Segment21','','','','US','');
xxeis.eis_rsc_ins.vc( 'GL_CODE_COMBINATIONS_KFV','SEGMENT20',101,'Key flexfield segment','SEGMENT20','','','','ANONYMOUS','VARCHAR2','GL_CODE_COMBINATIONS_KFV','SEGMENT20','Segment20','','','','US','');
xxeis.eis_rsc_ins.vc( 'GL_CODE_COMBINATIONS_KFV','SEGMENT19',101,'Key flexfield segment','SEGMENT19','','','','ANONYMOUS','VARCHAR2','GL_CODE_COMBINATIONS_KFV','SEGMENT19','Segment19','','','','US','');
xxeis.eis_rsc_ins.vc( 'GL_CODE_COMBINATIONS_KFV','SEGMENT18',101,'Key flexfield segment','SEGMENT18','','','','ANONYMOUS','VARCHAR2','GL_CODE_COMBINATIONS_KFV','SEGMENT18','Segment18','','','','US','');
xxeis.eis_rsc_ins.vc( 'GL_CODE_COMBINATIONS_KFV','SEGMENT17',101,'Key flexfield segment','SEGMENT17','','','','ANONYMOUS','VARCHAR2','GL_CODE_COMBINATIONS_KFV','SEGMENT17','Segment17','','','','US','');
xxeis.eis_rsc_ins.vc( 'GL_CODE_COMBINATIONS_KFV','SEGMENT16',101,'Key flexfield segment','SEGMENT16','','','','ANONYMOUS','VARCHAR2','GL_CODE_COMBINATIONS_KFV','SEGMENT16','Segment16','','','','US','');
xxeis.eis_rsc_ins.vc( 'GL_CODE_COMBINATIONS_KFV','SEGMENT15',101,'Key flexfield segment','SEGMENT15','','','','ANONYMOUS','VARCHAR2','GL_CODE_COMBINATIONS_KFV','SEGMENT15','Segment15','','','','US','');
xxeis.eis_rsc_ins.vc( 'GL_CODE_COMBINATIONS_KFV','SEGMENT14',101,'Key flexfield segment','SEGMENT14','','','','ANONYMOUS','VARCHAR2','GL_CODE_COMBINATIONS_KFV','SEGMENT14','Segment14','','','','US','');
xxeis.eis_rsc_ins.vc( 'GL_CODE_COMBINATIONS_KFV','SEGMENT13',101,'Key flexfield segment','SEGMENT13','','','','ANONYMOUS','VARCHAR2','GL_CODE_COMBINATIONS_KFV','SEGMENT13','Segment13','','','','US','');
xxeis.eis_rsc_ins.vc( 'GL_CODE_COMBINATIONS_KFV','SEGMENT12',101,'Key flexfield segment','SEGMENT12','','','','ANONYMOUS','VARCHAR2','GL_CODE_COMBINATIONS_KFV','SEGMENT12','Segment12','','','','US','');
xxeis.eis_rsc_ins.vc( 'GL_CODE_COMBINATIONS_KFV','SEGMENT11',101,'Key flexfield segment','SEGMENT11','','','','ANONYMOUS','VARCHAR2','GL_CODE_COMBINATIONS_KFV','SEGMENT11','Segment11','','','','US','');
xxeis.eis_rsc_ins.vc( 'GL_CODE_COMBINATIONS_KFV','SEGMENT10',101,'Key flexfield segment','SEGMENT10','','','','ANONYMOUS','VARCHAR2','GL_CODE_COMBINATIONS_KFV','SEGMENT10','Segment10','','','','US','');
xxeis.eis_rsc_ins.vc( 'GL_CODE_COMBINATIONS_KFV','SEGMENT9',101,'Key flexfield segment','SEGMENT9','','','','ANONYMOUS','VARCHAR2','GL_CODE_COMBINATIONS_KFV','SEGMENT9','Segment9','','','','US','');
xxeis.eis_rsc_ins.vc( 'GL_CODE_COMBINATIONS_KFV','SEGMENT8',101,'Key flexfield segment','SEGMENT8','','','','ANONYMOUS','VARCHAR2','GL_CODE_COMBINATIONS_KFV','SEGMENT8','Segment8','','','','US','');
xxeis.eis_rsc_ins.vc( 'GL_CODE_COMBINATIONS_KFV','SEGMENT7',101,'Key flexfield segment','SEGMENT7','','','','ANONYMOUS','VARCHAR2','GL_CODE_COMBINATIONS_KFV','SEGMENT7','Segment7','','','','US','');
xxeis.eis_rsc_ins.vc( 'GL_CODE_COMBINATIONS_KFV','SEGMENT6',101,'Key flexfield segment','SEGMENT6','','','','ANONYMOUS','VARCHAR2','GL_CODE_COMBINATIONS_KFV','SEGMENT6','Segment6','','','','US','');
xxeis.eis_rsc_ins.vc( 'GL_CODE_COMBINATIONS_KFV','SEGMENT5',101,'Key flexfield segment','SEGMENT5','','','','ANONYMOUS','VARCHAR2','GL_CODE_COMBINATIONS_KFV','SEGMENT5','Segment5','','','','US','');
xxeis.eis_rsc_ins.vc( 'GL_CODE_COMBINATIONS_KFV','SEGMENT4',101,'Key flexfield segment','SEGMENT4','','','','ANONYMOUS','VARCHAR2','GL_CODE_COMBINATIONS_KFV','SEGMENT4','Segment4','','','','US','');
xxeis.eis_rsc_ins.vc( 'GL_CODE_COMBINATIONS_KFV','SEGMENT3',101,'Key flexfield segment','SEGMENT3','','','','ANONYMOUS','VARCHAR2','GL_CODE_COMBINATIONS_KFV','SEGMENT3','Segment3','','','','US','');
xxeis.eis_rsc_ins.vc( 'GL_CODE_COMBINATIONS_KFV','SEGMENT2',101,'Key flexfield segment','SEGMENT2','','','','ANONYMOUS','VARCHAR2','GL_CODE_COMBINATIONS_KFV','SEGMENT2','Segment2','','','','US','');
xxeis.eis_rsc_ins.vc( 'GL_CODE_COMBINATIONS_KFV','SEGMENT1',101,'Key flexfield segment','SEGMENT1','','','','ANONYMOUS','VARCHAR2','GL_CODE_COMBINATIONS_KFV','SEGMENT1','Segment1','','','','US','');
xxeis.eis_rsc_ins.vc( 'GL_CODE_COMBINATIONS_KFV','SUMMARY_FLAG',101,'Summary account flag','SUMMARY_FLAG','','','','ANONYMOUS','VARCHAR2','GL_CODE_COMBINATIONS_KFV','SUMMARY_FLAG','Summary Flag','','','','US','');
xxeis.eis_rsc_ins.vc( 'GL_CODE_COMBINATIONS_KFV','ENABLED_FLAG',101,'Key flexfield enabled flag','ENABLED_FLAG','','','','ANONYMOUS','VARCHAR2','GL_CODE_COMBINATIONS_KFV','ENABLED_FLAG','Enabled Flag','','','','US','');
xxeis.eis_rsc_ins.vc( 'GL_CODE_COMBINATIONS_KFV','IGI_BALANCED_BUDGET_FLAG',101,'Balance budget enforcement flag','IGI_BALANCED_BUDGET_FLAG','','','','ANONYMOUS','VARCHAR2','GL_CODE_COMBINATIONS_KFV','IGI_BALANCED_BUDGET_FLAG','Igi Balanced Budget Flag','','','','US','');
xxeis.eis_rsc_ins.vc( 'GL_CODE_COMBINATIONS_KFV','REVALUATION_ID',101,'Revaluation defining column','REVALUATION_ID','','','','ANONYMOUS','NUMBER','GL_CODE_COMBINATIONS_KFV','REVALUATION_ID','Revaluation Id','','','','US','');
xxeis.eis_rsc_ins.vc( 'GL_CODE_COMBINATIONS_KFV','ALTERNATE_CODE_COMBINATION_ID',101,'','ALTERNATE_CODE_COMBINATION_ID','','','','ANONYMOUS','NUMBER','GL_CODE_COMBINATIONS_KFV','ALTERNATE_CODE_COMBINATION_ID','Alternate Code Combination Id','','','','US','');
xxeis.eis_rsc_ins.vc( 'GL_CODE_COMBINATIONS_KFV','LEDGER_SEGMENT',101,'','LEDGER_SEGMENT','','','','ANONYMOUS','VARCHAR2','GL_CODE_COMBINATIONS_KFV','LEDGER_SEGMENT','Ledger Segment','','','','US','');
xxeis.eis_rsc_ins.vc( 'GL_CODE_COMBINATIONS_KFV','COMPANY_COST_CENTER_ORG_ID',101,'Organization defining column for the organization associated with the accounts'' company/cost center combination.','COMPANY_COST_CENTER_ORG_ID','','~T~D~2','','ANONYMOUS','NUMBER','GL_CODE_COMBINATIONS_KFV','COMPANY_COST_CENTER_ORG_ID','Company Cost Center Org Id','','','','US','');
xxeis.eis_rsc_ins.vc( 'GL_CODE_COMBINATIONS_KFV','DETAIL_POSTING_ALLOWED',101,'','DETAIL_POSTING_ALLOWED','','','','ANONYMOUS','VARCHAR2','GL_CODE_COMBINATIONS_KFV','DETAIL_POSTING_ALLOWED','Detail Posting Allowed','','','','US','');
xxeis.eis_rsc_ins.vc( 'GL_CODE_COMBINATIONS_KFV','DETAIL_BUDGETING_ALLOWED',101,'','DETAIL_BUDGETING_ALLOWED','','','','ANONYMOUS','VARCHAR2','GL_CODE_COMBINATIONS_KFV','DETAIL_BUDGETING_ALLOWED','Detail Budgeting Allowed','','','','US','');
xxeis.eis_rsc_ins.vc( 'GL_CODE_COMBINATIONS_KFV','RECONCILIATION_FLAG',101,'','RECONCILIATION_FLAG','','','','ANONYMOUS','VARCHAR2','GL_CODE_COMBINATIONS_KFV','RECONCILIATION_FLAG','Reconciliation Flag','','','','US','');
xxeis.eis_rsc_ins.vc( 'GL_CODE_COMBINATIONS_KFV','GL_CONTROL_ACCOUNT',101,'','GL_CONTROL_ACCOUNT','','','','ANONYMOUS','VARCHAR2','GL_CODE_COMBINATIONS_KFV','GL_CONTROL_ACCOUNT','Gl Control Account','','','','US','');
xxeis.eis_rsc_ins.vc( 'GL_CODE_COMBINATIONS_KFV','GL_ACCOUNT_TYPE',101,'','GL_ACCOUNT_TYPE','','','','ANONYMOUS','VARCHAR2','GL_CODE_COMBINATIONS_KFV','GL_ACCOUNT_TYPE','Gl Account Type','','','','US','');
xxeis.eis_rsc_ins.vc( 'GL_CODE_COMBINATIONS_KFV','PADDED_CONCATENATED_SEGMENTS',101,'','PADDED_CONCATENATED_SEGMENTS','','','','ANONYMOUS','VARCHAR2','GL_CODE_COMBINATIONS_KFV','PADDED_CONCATENATED_SEGMENTS','Padded Concatenated Segments','','','','US','');
xxeis.eis_rsc_ins.vc( 'GL_CODE_COMBINATIONS_KFV','CONCATENATED_SEGMENTS',101,'','CONCATENATED_SEGMENTS','','','','ANONYMOUS','VARCHAR2','GL_CODE_COMBINATIONS_KFV','CONCATENATED_SEGMENTS','Concatenated Segments','','','','US','');
xxeis.eis_rsc_ins.vc( 'GL_CODE_COMBINATIONS_KFV','CHART_OF_ACCOUNTS_ID',101,'Key flexfield structure defining column','CHART_OF_ACCOUNTS_ID','','','','ANONYMOUS','NUMBER','GL_CODE_COMBINATIONS_KFV','CHART_OF_ACCOUNTS_ID','Chart Of Accounts Id','','','','US','');
xxeis.eis_rsc_ins.vc( 'GL_CODE_COMBINATIONS_KFV','CODE_COMBINATION_ID',101,'Key flexfield combination defining column','CODE_COMBINATION_ID','','','','ANONYMOUS','NUMBER','GL_CODE_COMBINATIONS_KFV','CODE_COMBINATION_ID','Code Combination Id','','','','US','');
xxeis.eis_rsc_ins.vc( 'GL_CODE_COMBINATIONS_KFV','ROW_ID',101,'','ROW_ID','','','','ANONYMOUS','ROWID','GL_CODE_COMBINATIONS_KFV','ROW_ID','Row Id','','','','US','');
xxeis.eis_rsc_ins.vc( 'GL_CODE_COMBINATIONS_KFV','LAST_UPDATED_BY',101,'Standard Who column','LAST_UPDATED_BY','','','','ANONYMOUS','NUMBER','GL_CODE_COMBINATIONS_KFV','LAST_UPDATED_BY','Last Updated By','','','','US','');
xxeis.eis_rsc_ins.vc( 'GL_CODE_COMBINATIONS_KFV','LAST_UPDATE_DATE',101,'Standard Who column','LAST_UPDATE_DATE','','','','ANONYMOUS','DATE','GL_CODE_COMBINATIONS_KFV','LAST_UPDATE_DATE','Last Update Date','','','','US','');
xxeis.eis_rsc_ins.vc( 'GL_CODE_COMBINATIONS_KFV','REFRESH_FLAG',101,'Refresh segment value attributes flag','REFRESH_FLAG','','','','ANONYMOUS','VARCHAR2','GL_CODE_COMBINATIONS_KFV','REFRESH_FLAG','Refresh Flag','','','','US','');
xxeis.eis_rsc_ins.vc( 'GL_CODE_COMBINATIONS_KFV','PRESERVE_FLAG',101,'Preserve segment value attributes flag','PRESERVE_FLAG','','','','ANONYMOUS','VARCHAR2','GL_CODE_COMBINATIONS_KFV','PRESERVE_FLAG','Preserve Flag','','','','US','');
xxeis.eis_rsc_ins.vc( 'GL_CODE_COMBINATIONS_KFV','JGZZ_RECON_CONTEXT',101,'Global reconciliation descriptive flexfield context column','JGZZ_RECON_CONTEXT','','','','ANONYMOUS','VARCHAR2','GL_CODE_COMBINATIONS_KFV','JGZZ_RECON_CONTEXT','Jgzz Recon Context','','','','US','');
xxeis.eis_rsc_ins.vc( 'GL_CODE_COMBINATIONS_KFV','REFERENCE5',101,'Reserved for localization use','REFERENCE5','','','','ANONYMOUS','VARCHAR2','GL_CODE_COMBINATIONS_KFV','REFERENCE5','Reference5','','','','US','');
xxeis.eis_rsc_ins.vc( 'GL_CODE_COMBINATIONS_KFV','REFERENCE4',101,'Reserved for localization use','REFERENCE4','','','','ANONYMOUS','VARCHAR2','GL_CODE_COMBINATIONS_KFV','REFERENCE4','Reference4','','','','US','');
xxeis.eis_rsc_ins.vc( 'GL_CODE_COMBINATIONS_KFV','REFERENCE2',101,'Reserved for localization use','REFERENCE2','','','','ANONYMOUS','VARCHAR2','GL_CODE_COMBINATIONS_KFV','REFERENCE2','Reference2','','','','US','');
xxeis.eis_rsc_ins.vc( 'GL_CODE_COMBINATIONS_KFV','REFERENCE1',101,'Reserved for localization use','REFERENCE1','','','','ANONYMOUS','VARCHAR2','GL_CODE_COMBINATIONS_KFV','REFERENCE1','Reference1','','','','US','');
xxeis.eis_rsc_ins.vc( 'GL_CODE_COMBINATIONS_KFV','SEGMENT_ATTRIBUTE42',101,'Reporting attribute key flexfield segment','SEGMENT_ATTRIBUTE42','','','','ANONYMOUS','VARCHAR2','GL_CODE_COMBINATIONS_KFV','SEGMENT_ATTRIBUTE42','Segment Attribute42','','','','US','');
xxeis.eis_rsc_ins.vc( 'GL_CODE_COMBINATIONS_KFV','SEGMENT_ATTRIBUTE41',101,'Reporting attribute key flexfield segment','SEGMENT_ATTRIBUTE41','','','','ANONYMOUS','VARCHAR2','GL_CODE_COMBINATIONS_KFV','SEGMENT_ATTRIBUTE41','Segment Attribute41','','','','US','');
xxeis.eis_rsc_ins.vc( 'GL_CODE_COMBINATIONS_KFV','SEGMENT_ATTRIBUTE40',101,'Reporting attribute key flexfield segment','SEGMENT_ATTRIBUTE40','','','','ANONYMOUS','VARCHAR2','GL_CODE_COMBINATIONS_KFV','SEGMENT_ATTRIBUTE40','Segment Attribute40','','','','US','');
xxeis.eis_rsc_ins.vc( 'GL_CODE_COMBINATIONS_KFV','SEGMENT_ATTRIBUTE39',101,'Reporting attribute key flexfield segment','SEGMENT_ATTRIBUTE39','','','','ANONYMOUS','VARCHAR2','GL_CODE_COMBINATIONS_KFV','SEGMENT_ATTRIBUTE39','Segment Attribute39','','','','US','');
xxeis.eis_rsc_ins.vc( 'GL_CODE_COMBINATIONS_KFV','SEGMENT_ATTRIBUTE38',101,'Reporting attribute key flexfield segment','SEGMENT_ATTRIBUTE38','','','','ANONYMOUS','VARCHAR2','GL_CODE_COMBINATIONS_KFV','SEGMENT_ATTRIBUTE38','Segment Attribute38','','','','US','');
xxeis.eis_rsc_ins.vc( 'GL_CODE_COMBINATIONS_KFV','SEGMENT_ATTRIBUTE37',101,'Reporting attribute key flexfield segment','SEGMENT_ATTRIBUTE37','','','','ANONYMOUS','VARCHAR2','GL_CODE_COMBINATIONS_KFV','SEGMENT_ATTRIBUTE37','Segment Attribute37','','','','US','');
xxeis.eis_rsc_ins.vc( 'GL_CODE_COMBINATIONS_KFV','SEGMENT_ATTRIBUTE36',101,'Reporting attribute key flexfield segment','SEGMENT_ATTRIBUTE36','','','','ANONYMOUS','VARCHAR2','GL_CODE_COMBINATIONS_KFV','SEGMENT_ATTRIBUTE36','Segment Attribute36','','','','US','');
xxeis.eis_rsc_ins.vc( 'GL_CODE_COMBINATIONS_KFV','SEGMENT_ATTRIBUTE35',101,'Reporting attribute key flexfield segment','SEGMENT_ATTRIBUTE35','','','','ANONYMOUS','VARCHAR2','GL_CODE_COMBINATIONS_KFV','SEGMENT_ATTRIBUTE35','Segment Attribute35','','','','US','');
xxeis.eis_rsc_ins.vc( 'GL_CODE_COMBINATIONS_KFV','SEGMENT_ATTRIBUTE34',101,'Reporting attribute key flexfield segment','SEGMENT_ATTRIBUTE34','','','','ANONYMOUS','VARCHAR2','GL_CODE_COMBINATIONS_KFV','SEGMENT_ATTRIBUTE34','Segment Attribute34','','','','US','');
xxeis.eis_rsc_ins.vc( 'GL_CODE_COMBINATIONS_KFV','SEGMENT_ATTRIBUTE33',101,'Reporting attribute key flexfield segment','SEGMENT_ATTRIBUTE33','','','','ANONYMOUS','VARCHAR2','GL_CODE_COMBINATIONS_KFV','SEGMENT_ATTRIBUTE33','Segment Attribute33','','','','US','');
xxeis.eis_rsc_ins.vc( 'GL_CODE_COMBINATIONS_KFV','SEGMENT_ATTRIBUTE32',101,'Reporting attribute key flexfield segment','SEGMENT_ATTRIBUTE32','','','','ANONYMOUS','VARCHAR2','GL_CODE_COMBINATIONS_KFV','SEGMENT_ATTRIBUTE32','Segment Attribute32','','','','US','');
xxeis.eis_rsc_ins.vc( 'GL_CODE_COMBINATIONS_KFV','SEGMENT_ATTRIBUTE31',101,'Reporting attribute key flexfield segment','SEGMENT_ATTRIBUTE31','','','','ANONYMOUS','VARCHAR2','GL_CODE_COMBINATIONS_KFV','SEGMENT_ATTRIBUTE31','Segment Attribute31','','','','US','');
xxeis.eis_rsc_ins.vc( 'GL_CODE_COMBINATIONS_KFV','SEGMENT_ATTRIBUTE30',101,'Reporting attribute key flexfield segment','SEGMENT_ATTRIBUTE30','','','','ANONYMOUS','VARCHAR2','GL_CODE_COMBINATIONS_KFV','SEGMENT_ATTRIBUTE30','Segment Attribute30','','','','US','');
xxeis.eis_rsc_ins.vc( 'GL_CODE_COMBINATIONS_KFV','SEGMENT_ATTRIBUTE29',101,'Reporting attribute key flexfield segment','SEGMENT_ATTRIBUTE29','','','','ANONYMOUS','VARCHAR2','GL_CODE_COMBINATIONS_KFV','SEGMENT_ATTRIBUTE29','Segment Attribute29','','','','US','');
xxeis.eis_rsc_ins.vc( 'GL_CODE_COMBINATIONS_KFV','SEGMENT_ATTRIBUTE28',101,'Reporting attribute key flexfield segment','SEGMENT_ATTRIBUTE28','','','','ANONYMOUS','VARCHAR2','GL_CODE_COMBINATIONS_KFV','SEGMENT_ATTRIBUTE28','Segment Attribute28','','','','US','');
xxeis.eis_rsc_ins.vc( 'GL_CODE_COMBINATIONS_KFV','SEGMENT_ATTRIBUTE27',101,'Reporting attribute key flexfield segment','SEGMENT_ATTRIBUTE27','','','','ANONYMOUS','VARCHAR2','GL_CODE_COMBINATIONS_KFV','SEGMENT_ATTRIBUTE27','Segment Attribute27','','','','US','');
xxeis.eis_rsc_ins.vc( 'GL_CODE_COMBINATIONS_KFV','SEGMENT_ATTRIBUTE26',101,'Reporting attribute key flexfield segment','SEGMENT_ATTRIBUTE26','','','','ANONYMOUS','VARCHAR2','GL_CODE_COMBINATIONS_KFV','SEGMENT_ATTRIBUTE26','Segment Attribute26','','','','US','');
xxeis.eis_rsc_ins.vc( 'GL_CODE_COMBINATIONS_KFV','SEGMENT_ATTRIBUTE25',101,'Reporting attribute key flexfield segment','SEGMENT_ATTRIBUTE25','','','','ANONYMOUS','VARCHAR2','GL_CODE_COMBINATIONS_KFV','SEGMENT_ATTRIBUTE25','Segment Attribute25','','','','US','');
xxeis.eis_rsc_ins.vc( 'GL_CODE_COMBINATIONS_KFV','SEGMENT_ATTRIBUTE24',101,'Reporting attribute key flexfield segment','SEGMENT_ATTRIBUTE24','','','','ANONYMOUS','VARCHAR2','GL_CODE_COMBINATIONS_KFV','SEGMENT_ATTRIBUTE24','Segment Attribute24','','','','US','');
xxeis.eis_rsc_ins.vc( 'GL_CODE_COMBINATIONS_KFV','SEGMENT_ATTRIBUTE23',101,'Reporting attribute key flexfield segment','SEGMENT_ATTRIBUTE23','','','','ANONYMOUS','VARCHAR2','GL_CODE_COMBINATIONS_KFV','SEGMENT_ATTRIBUTE23','Segment Attribute23','','','','US','');
xxeis.eis_rsc_ins.vc( 'GL_CODE_COMBINATIONS_KFV','SEGMENT_ATTRIBUTE22',101,'Reporting attribute key flexfield segment','SEGMENT_ATTRIBUTE22','','','','ANONYMOUS','VARCHAR2','GL_CODE_COMBINATIONS_KFV','SEGMENT_ATTRIBUTE22','Segment Attribute22','','','','US','');
xxeis.eis_rsc_ins.vc( 'GL_CODE_COMBINATIONS_KFV','SEGMENT_ATTRIBUTE21',101,'Reporting attribute key flexfield segment','SEGMENT_ATTRIBUTE21','','','','ANONYMOUS','VARCHAR2','GL_CODE_COMBINATIONS_KFV','SEGMENT_ATTRIBUTE21','Segment Attribute21','','','','US','');
xxeis.eis_rsc_ins.vc( 'GL_CODE_COMBINATIONS_KFV','SEGMENT_ATTRIBUTE20',101,'Reporting attribute key flexfield segment','SEGMENT_ATTRIBUTE20','','','','ANONYMOUS','VARCHAR2','GL_CODE_COMBINATIONS_KFV','SEGMENT_ATTRIBUTE20','Segment Attribute20','','','','US','');
xxeis.eis_rsc_ins.vc( 'GL_CODE_COMBINATIONS_KFV','SEGMENT_ATTRIBUTE19',101,'Reporting attribute key flexfield segment','SEGMENT_ATTRIBUTE19','','','','ANONYMOUS','VARCHAR2','GL_CODE_COMBINATIONS_KFV','SEGMENT_ATTRIBUTE19','Segment Attribute19','','','','US','');
xxeis.eis_rsc_ins.vc( 'GL_CODE_COMBINATIONS_KFV','SEGMENT_ATTRIBUTE18',101,'Reporting attribute key flexfield segment','SEGMENT_ATTRIBUTE18','','','','ANONYMOUS','VARCHAR2','GL_CODE_COMBINATIONS_KFV','SEGMENT_ATTRIBUTE18','Segment Attribute18','','','','US','');
xxeis.eis_rsc_ins.vc( 'GL_CODE_COMBINATIONS_KFV','SEGMENT_ATTRIBUTE17',101,'Reporting attribute key flexfield segment','SEGMENT_ATTRIBUTE17','','','','ANONYMOUS','VARCHAR2','GL_CODE_COMBINATIONS_KFV','SEGMENT_ATTRIBUTE17','Segment Attribute17','','','','US','');
xxeis.eis_rsc_ins.vc( 'GL_CODE_COMBINATIONS_KFV','SEGMENT_ATTRIBUTE16',101,'Reporting attribute key flexfield segment','SEGMENT_ATTRIBUTE16','','','','ANONYMOUS','VARCHAR2','GL_CODE_COMBINATIONS_KFV','SEGMENT_ATTRIBUTE16','Segment Attribute16','','','','US','');
xxeis.eis_rsc_ins.vc( 'GL_CODE_COMBINATIONS_KFV','SEGMENT_ATTRIBUTE15',101,'Reporting attribute key flexfield segment','SEGMENT_ATTRIBUTE15','','','','ANONYMOUS','VARCHAR2','GL_CODE_COMBINATIONS_KFV','SEGMENT_ATTRIBUTE15','Segment Attribute15','','','','US','');
xxeis.eis_rsc_ins.vc( 'GL_CODE_COMBINATIONS_KFV','SEGMENT_ATTRIBUTE14',101,'Reporting attribute key flexfield segment','SEGMENT_ATTRIBUTE14','','','','ANONYMOUS','VARCHAR2','GL_CODE_COMBINATIONS_KFV','SEGMENT_ATTRIBUTE14','Segment Attribute14','','','','US','');
xxeis.eis_rsc_ins.vc( 'GL_CODE_COMBINATIONS_KFV','SEGMENT_ATTRIBUTE13',101,'Reporting attribute key flexfield segment','SEGMENT_ATTRIBUTE13','','','','ANONYMOUS','VARCHAR2','GL_CODE_COMBINATIONS_KFV','SEGMENT_ATTRIBUTE13','Segment Attribute13','','','','US','');
xxeis.eis_rsc_ins.vc( 'GL_CODE_COMBINATIONS_KFV','SEGMENT_ATTRIBUTE12',101,'Reporting attribute key flexfield segment','SEGMENT_ATTRIBUTE12','','','','ANONYMOUS','VARCHAR2','GL_CODE_COMBINATIONS_KFV','SEGMENT_ATTRIBUTE12','Segment Attribute12','','','','US','');
xxeis.eis_rsc_ins.vc( 'GL_CODE_COMBINATIONS_KFV','SEGMENT_ATTRIBUTE11',101,'Reporting attribute key flexfield segment','SEGMENT_ATTRIBUTE11','','','','ANONYMOUS','VARCHAR2','GL_CODE_COMBINATIONS_KFV','SEGMENT_ATTRIBUTE11','Segment Attribute11','','','','US','');
xxeis.eis_rsc_ins.vc( 'GL_CODE_COMBINATIONS_KFV','SEGMENT_ATTRIBUTE10',101,'Reporting attribute key flexfield segment','SEGMENT_ATTRIBUTE10','','','','ANONYMOUS','VARCHAR2','GL_CODE_COMBINATIONS_KFV','SEGMENT_ATTRIBUTE10','Segment Attribute10','','','','US','');
xxeis.eis_rsc_ins.vc( 'GL_CODE_COMBINATIONS_KFV','SEGMENT_ATTRIBUTE9',101,'Reporting attribute key flexfield segment','SEGMENT_ATTRIBUTE9','','','','ANONYMOUS','VARCHAR2','GL_CODE_COMBINATIONS_KFV','SEGMENT_ATTRIBUTE9','Segment Attribute9','','','','US','');
xxeis.eis_rsc_ins.vc( 'GL_CODE_COMBINATIONS_KFV','SEGMENT_ATTRIBUTE8',101,'Reporting attribute key flexfield segment','SEGMENT_ATTRIBUTE8','','','','ANONYMOUS','VARCHAR2','GL_CODE_COMBINATIONS_KFV','SEGMENT_ATTRIBUTE8','Segment Attribute8','','','','US','');
xxeis.eis_rsc_ins.vc( 'GL_CODE_COMBINATIONS_KFV','SEGMENT_ATTRIBUTE7',101,'Reporting attribute key flexfield segment','SEGMENT_ATTRIBUTE7','','','','ANONYMOUS','VARCHAR2','GL_CODE_COMBINATIONS_KFV','SEGMENT_ATTRIBUTE7','Segment Attribute7','','','','US','');
xxeis.eis_rsc_ins.vc( 'GL_CODE_COMBINATIONS_KFV','SEGMENT_ATTRIBUTE6',101,'Reporting attribute key flexfield segment','SEGMENT_ATTRIBUTE6','','','','ANONYMOUS','VARCHAR2','GL_CODE_COMBINATIONS_KFV','SEGMENT_ATTRIBUTE6','Segment Attribute6','','','','US','');
xxeis.eis_rsc_ins.vc( 'GL_CODE_COMBINATIONS_KFV','SEGMENT_ATTRIBUTE5',101,'Reporting attribute key flexfield segment','SEGMENT_ATTRIBUTE5','','','','ANONYMOUS','VARCHAR2','GL_CODE_COMBINATIONS_KFV','SEGMENT_ATTRIBUTE5','Segment Attribute5','','','','US','');
xxeis.eis_rsc_ins.vc( 'GL_CODE_COMBINATIONS_KFV','SEGMENT_ATTRIBUTE4',101,'Reporting attribute key flexfield segment','SEGMENT_ATTRIBUTE4','','','','ANONYMOUS','VARCHAR2','GL_CODE_COMBINATIONS_KFV','SEGMENT_ATTRIBUTE4','Segment Attribute4','','','','US','');
xxeis.eis_rsc_ins.vc( 'GL_CODE_COMBINATIONS_KFV','SEGMENT_ATTRIBUTE3',101,'Reporting attribute key flexfield segment','SEGMENT_ATTRIBUTE3','','','','ANONYMOUS','VARCHAR2','GL_CODE_COMBINATIONS_KFV','SEGMENT_ATTRIBUTE3','Segment Attribute3','','','','US','');
xxeis.eis_rsc_ins.vc( 'GL_CODE_COMBINATIONS_KFV','SEGMENT_ATTRIBUTE2',101,'Reporting attribute key flexfield segment','SEGMENT_ATTRIBUTE2','','','','ANONYMOUS','VARCHAR2','GL_CODE_COMBINATIONS_KFV','SEGMENT_ATTRIBUTE2','Segment Attribute2','','','','US','');
xxeis.eis_rsc_ins.vc( 'GL_CODE_COMBINATIONS_KFV','SEGMENT_ATTRIBUTE1',101,'Reporting attribute key flexfield segment','SEGMENT_ATTRIBUTE1','','','','ANONYMOUS','VARCHAR2','GL_CODE_COMBINATIONS_KFV','SEGMENT_ATTRIBUTE1','Segment Attribute1','','','','US','');
xxeis.eis_rsc_ins.vc( 'GL_CODE_COMBINATIONS_KFV','CONTEXT',101,'Descriptive flex context column','CONTEXT','','','','ANONYMOUS','VARCHAR2','GL_CODE_COMBINATIONS_KFV','CONTEXT','Context','','','','US','');
xxeis.eis_rsc_ins.vc( 'GL_CODE_COMBINATIONS_KFV','ATTRIBUTE10',101,'Descriptive flexfield segment','ATTRIBUTE10','','','','ANONYMOUS','VARCHAR2','GL_CODE_COMBINATIONS_KFV','ATTRIBUTE10','Attribute10','','','','US','');
xxeis.eis_rsc_ins.vc( 'GL_CODE_COMBINATIONS_KFV','ATTRIBUTE9',101,'Descriptive flexfield segment','ATTRIBUTE9','','','','ANONYMOUS','VARCHAR2','GL_CODE_COMBINATIONS_KFV','ATTRIBUTE9','Attribute9','','','','US','');
xxeis.eis_rsc_ins.vc( 'GL_CODE_COMBINATIONS_KFV','ATTRIBUTE8',101,'Descriptive flexfield segment','ATTRIBUTE8','','','','ANONYMOUS','VARCHAR2','GL_CODE_COMBINATIONS_KFV','ATTRIBUTE8','Attribute8','','','','US','');
xxeis.eis_rsc_ins.vc( 'GL_CODE_COMBINATIONS_KFV','ATTRIBUTE7',101,'Descriptive flexfield segment','ATTRIBUTE7','','','','ANONYMOUS','VARCHAR2','GL_CODE_COMBINATIONS_KFV','ATTRIBUTE7','Attribute7','','','','US','');
xxeis.eis_rsc_ins.vc( 'GL_CODE_COMBINATIONS_KFV','ATTRIBUTE6',101,'Descriptive flexfield segment','ATTRIBUTE6','','','','ANONYMOUS','VARCHAR2','GL_CODE_COMBINATIONS_KFV','ATTRIBUTE6','Attribute6','','','','US','');
xxeis.eis_rsc_ins.vc( 'GL_CODE_COMBINATIONS_KFV','ATTRIBUTE5',101,'Descriptive flexfield segment','ATTRIBUTE5','','','','ANONYMOUS','VARCHAR2','GL_CODE_COMBINATIONS_KFV','ATTRIBUTE5','Attribute5','','','','US','');
xxeis.eis_rsc_ins.vc( 'GL_CODE_COMBINATIONS_KFV','ATTRIBUTE4',101,'Descriptive flexfield segment','ATTRIBUTE4','','','','ANONYMOUS','VARCHAR2','GL_CODE_COMBINATIONS_KFV','ATTRIBUTE4','Attribute4','','','','US','');
--Inserting Object Components for GL_CODE_COMBINATIONS_KFV
--Inserting Object Component Joins for GL_CODE_COMBINATIONS_KFV
ELSE
RAISE_APPLICATION_ERROR (-20001,'Module does not exist. Import/Create the module with id 101');
END IF;
END;
/
prompt Creating Report LOV Data for HDS-WhiteCap_Fringe
set scan off define off
DECLARE
mod_exist VARCHAR2(1);
BEGIN 
mod_exist := XXEIS.EIS_RSC_INSERT.check_if_module_exist(101);
IF mod_exist = 'Y' THEN 
--Inserting Report LOVs - HDS-WhiteCap_Fringe
xxeis.eis_rsc_ins.lov( 101,'select  per.period_name , led.name ledger_name, per.period_year, per.period_num, per.start_date, per.end_date
from    gl_periods per,gl_ledgers led
where  per.period_set_name = led.period_set_name
and     GL_SECURITY_PKG.VALIDATE_ACCESS(led.ledger_ID) = ''TRUE''
and     led.accounted_period_type = per.period_type','null','EIS_GL_PERIOD_LOV','Derives GL Periods based on the corresponding setups in the sets of books','XXEIS_RS_ADMIN',NULL,'','','','','','','','US');
xxeis.eis_rsc_ins.lov( '','SELECT ffv.flex_value, ffvtl.description, decode(ffv.summary_flag,''Y'', ''Parent'',''N'', ''Child'') Type 
				FROM 
					fnd_flex_value_sets ffvs , 
					fnd_flex_values ffv, 
					fnd_flex_values_tl ffvtl 
				WHERE   upper(ffvs.flex_value_set_name) = upper(''XXCUS_GL_ACCOUNT'') 
				 and ffv.flex_value_set_id = ffvs.flex_value_set_id 
				 and ffv.FLEX_VALUE_ID = ffvtl.FLEX_VALUE_ID 
				AND ffv.enabled_flag = upper(''Y'') 
				AND ffv.summary_flag in (''Y'',''N'') 
				/* AND ffv.summary_flag <> upper(''Y'') */ 
				AND ffvtl.LANGUAGE = USERENV(''LANG'') 
				 AND xxeis.eis_gl_security_pkg.validate_segment_value( ''SEGMENT4'' , ffv.flex_value)=''TRUE'' 
				order by ffv.flex_value	','','XXCUS_GL_ACCOUNT','XXCUS_GL_ACCOUNT','MM050208',NULL,'','','','','','','','US');
null;
ELSE
RAISE_APPLICATION_ERROR (-20001,'Module does not exist. Import/Create the module with id 101');
END IF;
END;
/
prompt Creating Report Data for HDS-WhiteCap_Fringe
set scan off define off
DECLARE
mod_exist varchar2(1);
BEGIN 
mod_exist := XXEIS.EIS_RSC_INSERT.check_if_module_exist(101);
IF mod_exist = 'Y' THEN 
--Deleting Report data - HDS-WhiteCap_Fringe
xxeis.eis_rsc_utility.delete_report_rows( 'HDS-WhiteCap_Fringe',101 );
--Inserting Report - HDS-WhiteCap_Fringe
xxeis.eis_rsc_ins.r( 101,'HDS-WhiteCap_Fringe','','','','','','KP012542','EIS_GL_JOURNAL_DETAILS_V','Y','','','KP012542','','N','HDS Standard Reports','','CSV,Pivot Excel,EXCEL,','N','','','','','','','','US','','','','','','','','','','','','','','','','');
--Inserting Report Columns - HDS-WhiteCap_Fringe
xxeis.eis_rsc_ins.rc( 'HDS-WhiteCap_Fringe',101,'ACCOUNTED_NET','Accounted Net','Accounted Net','','','','','17','N','','','','','','','','KP012542','N','N','','EIS_GL_JOURNAL_DETAILS_V','','','SUM','US','','');
xxeis.eis_rsc_ins.rc( 'HDS-WhiteCap_Fringe',101,'SEGMENT2','Segment2','Key flexfield segment','','','','','8','N','','','','','','','','KP012542','N','N','','EIS_GL_JOURNAL_DETAILS_V','GL_CODE_COMBINATIONS_KFV','GCC','GROUP_BY','US','','');
xxeis.eis_rsc_ins.rc( 'HDS-WhiteCap_Fringe',101,'JE_DESCRIPTION','Je Description','Journal entry description','','','','','5','N','','','','','','','','KP012542','N','N','','EIS_GL_JOURNAL_DETAILS_V','','','GROUP_BY','US','','');
xxeis.eis_rsc_ins.rc( 'HDS-WhiteCap_Fringe',101,'JE_ENTERED_CR','Je Entered Cr','Journal entry line credit amount in entered currency','','','','','16','N','','','','','','','','KP012542','N','N','','EIS_GL_JOURNAL_DETAILS_V','','','SUM','US','','');
xxeis.eis_rsc_ins.rc( 'HDS-WhiteCap_Fringe',101,'JE_ENTERED_DR','Je Entered Dr','Journal entry line debit amount in entered currency','','','','','15','N','','','','','','','','KP012542','N','N','','EIS_GL_JOURNAL_DETAILS_V','','','SUM','US','','');
xxeis.eis_rsc_ins.rc( 'HDS-WhiteCap_Fringe',101,'JE_NAME','Je Name','Journal entry header name','','','','','4','N','','','','','','','','KP012542','N','N','','EIS_GL_JOURNAL_DETAILS_V','','','GROUP_BY','US','','');
xxeis.eis_rsc_ins.rc( 'HDS-WhiteCap_Fringe',101,'JE_PERIOD','Je Period','Accounting period','','','','','1','N','','','','','','','','KP012542','N','N','','EIS_GL_JOURNAL_DETAILS_V','','','GROUP_BY','US','','');
xxeis.eis_rsc_ins.rc( 'HDS-WhiteCap_Fringe',101,'JE_SOURCE','Je Source','Journal entry source user defined name','','','','','3','N','','','','','','','','KP012542','N','N','','EIS_GL_JOURNAL_DETAILS_V','','','GROUP_BY','US','','');
xxeis.eis_rsc_ins.rc( 'HDS-WhiteCap_Fringe',101,'JE_STATUS','Je Status','Journal entry header status lookup code','','','','','14','N','','','','','','','','KP012542','N','N','','EIS_GL_JOURNAL_DETAILS_V','','','GROUP_BY','US','','');
xxeis.eis_rsc_ins.rc( 'HDS-WhiteCap_Fringe',101,'SEGMENT1','Segment1','Key flexfield segment','','','','','7','N','','','','','','','','KP012542','N','N','','EIS_GL_JOURNAL_DETAILS_V','GL_CODE_COMBINATIONS_KFV','GCC','GROUP_BY','US','','');
xxeis.eis_rsc_ins.rc( 'HDS-WhiteCap_Fringe',101,'SEGMENT3','Segment3','Key flexfield segment','','','','','10','N','','','','','','','','KP012542','N','N','','EIS_GL_JOURNAL_DETAILS_V','GL_CODE_COMBINATIONS_KFV','GCC','GROUP_BY','US','','');
xxeis.eis_rsc_ins.rc( 'HDS-WhiteCap_Fringe',101,'SEGMENT4','Segment4','Key flexfield segment','','','','','11','N','','','','','','','','KP012542','N','N','','EIS_GL_JOURNAL_DETAILS_V','GL_CODE_COMBINATIONS_KFV','GCC','GROUP_BY','US','','');
xxeis.eis_rsc_ins.rc( 'HDS-WhiteCap_Fringe',101,'SEGMENT5','Segment5','Key flexfield segment','','','','','13','N','','','','','','','','KP012542','N','N','','EIS_GL_JOURNAL_DETAILS_V','GL_CODE_COMBINATIONS_KFV','GCC','GROUP_BY','US','','');
xxeis.eis_rsc_ins.rc( 'HDS-WhiteCap_Fringe',101,'JE_CATEGORY','Je Category','JE Category','','','','','2','N','','','','','','','','KP012542','N','N','','EIS_GL_JOURNAL_DETAILS_V','','','GROUP_BY','US','','');
xxeis.eis_rsc_ins.rc( 'HDS-WhiteCap_Fringe',101,'JE_LINE_DESCRIPTION','Je Line Description','Journal entry line description','','','','','6','N','','','','','','','','KP012542','N','N','','EIS_GL_JOURNAL_DETAILS_V','','','GROUP_BY','US','','');
xxeis.eis_rsc_ins.rc( 'HDS-WhiteCap_Fringe',101,'GCC#50328#LOCATION#DESCR','GCC#Location Descr','Accounting Flexfield (KFF) : Segment ''Location'' under chart of accounts ''XXCUS Accounting Flexfield''','','','','','9','N','','','','','','','','KP012542','N','N','','EIS_GL_JOURNAL_DETAILS_V','','','GROUP_BY','US','','');
xxeis.eis_rsc_ins.rc( 'HDS-WhiteCap_Fringe',101,'GCC#50368#ACCOUNT#DESCR','GCC#Account Descr','Accounting Flexfield (KFF) : Segment ''Account'' under chart of accounts ''XXCUS HDS Canada Litemor - COA''','','','','','12','N','','','','','','','','KP012542','N','N','','EIS_GL_JOURNAL_DETAILS_V','','','GROUP_BY','US','','');
--Inserting Report Parameters - HDS-WhiteCap_Fringe
xxeis.eis_rsc_ins.rp( 'HDS-WhiteCap_Fringe',101,'Period','Accounting period','JE_PERIOD','IN','EIS_GL_PERIOD_LOV','','VARCHAR2','Y','Y','1','Y','Y','CONSTANT','KP012542','Y','N','','','','EIS_GL_JOURNAL_DETAILS_V','','','US','');
xxeis.eis_rsc_ins.rp( 'HDS-WhiteCap_Fringe',101,'Account','Key flexfield segment','SEGMENT4','IN','XXCUS_GL_ACCOUNT','','VARCHAR2','N','Y','2','Y','Y','CONSTANT','KP012542','Y','N','','','','EIS_GL_JOURNAL_DETAILS_V','GL_CODE_COMBINATIONS_KFV','GCC','US','');
--Inserting Dependent Parameters - HDS-WhiteCap_Fringe
--Inserting Report Conditions - HDS-WhiteCap_Fringe
xxeis.eis_rsc_ins.rcnh( 'HDS-WhiteCap_Fringe',101,'GCC#50328#PRODUCT IN ''0W'' ','ADVANCED','','1#$#','Y','','1');
xxeis.eis_rsc_ins.rcnd( '','','GCC#50328#PRODUCT','','','','','','','EIS_GL_JOURNAL_DETAILS_V','','','','','','IN','Y','N','','''0W''','','','1',101,'HDS-WhiteCap_Fringe','GCC#50328#PRODUCT IN ''0W'' ');
xxeis.eis_rsc_ins.rcnh( 'HDS-WhiteCap_Fringe',101,'JE_PERIOD IN :Period ','SIMPLE','','','Y','','2');
xxeis.eis_rsc_ins.rcnd( '','','JE_PERIOD','','Period','','','','','EIS_GL_JOURNAL_DETAILS_V','','','','','','IN','Y','Y','','','','','1',101,'HDS-WhiteCap_Fringe','JE_PERIOD IN :Period ');
xxeis.eis_rsc_ins.rcnh( 'HDS-WhiteCap_Fringe',101,'SEGMENT4 IN :Account ','ADVANCED','','1#$#','Y','','3');
xxeis.eis_rsc_ins.rcnd( '','','','','Account','','','','','','','','','','','IN','Y','Y','SEGMENT4','','','','1',101,'HDS-WhiteCap_Fringe','SEGMENT4 IN :Account ');
--Inserting Report Sorts - HDS-WhiteCap_Fringe
--Inserting Report Triggers - HDS-WhiteCap_Fringe
--inserting report templates - HDS-WhiteCap_Fringe
--Inserting Report Portals - HDS-WhiteCap_Fringe
--inserting report dashboards - HDS-WhiteCap_Fringe
--Exporting report views table information -  
xxeis.eis_rsc_ins.rviews( 'HDS-WhiteCap_Fringe','101','EIS_GL_JOURNAL_DETAILS_V','EIS_GL_JOURNAL_DETAILS_V','N','');
xxeis.eis_rsc_ins.rviews( 'HDS-WhiteCap_Fringe','101','EIS_GL_JOURNAL_DETAILS_V','GL_CODE_COMBINATIONS_KFV','N','GCC');
--inserting report security - HDS-WhiteCap_Fringe
xxeis.eis_rsc_ins.rsec( 'HDS-WhiteCap_Fringe','101','','XXCUS_GL_ACCOUNTANT_CAD',101,'KP012542','','','');
xxeis.eis_rsc_ins.rsec( 'HDS-WhiteCap_Fringe','101','','XXCUS_GL_ACCOUNTANT_CAD_PVF',101,'KP012542','','','');
xxeis.eis_rsc_ins.rsec( 'HDS-WhiteCap_Fringe','101','','XXCUS_GL_ACCOUNTANT_GLOBAL',101,'KP012542','','','');
xxeis.eis_rsc_ins.rsec( 'HDS-WhiteCap_Fringe','101','','GNRL_LDGR_LTMR_ACCNTNT',101,'KP012542','','','');
xxeis.eis_rsc_ins.rsec( 'HDS-WhiteCap_Fringe','101','','XXCUS_GL_ACCOUNTANT_USD',101,'KP012542','','','');
xxeis.eis_rsc_ins.rsec( 'HDS-WhiteCap_Fringe','101','','XXCUS_GL_ACCOUNTANT_USD_PVF',101,'KP012542','','','');
xxeis.eis_rsc_ins.rsec( 'HDS-WhiteCap_Fringe','101','','XXCUS_GL_INQUIRY',101,'KP012542','','','');
xxeis.eis_rsc_ins.rsec( 'HDS-WhiteCap_Fringe','101','','HDS GL INQUIRY',101,'KP012542','','','');
xxeis.eis_rsc_ins.rsec( 'HDS-WhiteCap_Fringe','101','','GNRL_LDGR_LTMR_NQR',101,'KP012542','','','');
xxeis.eis_rsc_ins.rsec( 'HDS-WhiteCap_Fringe','101','','XXCUS_GL_INQUIRY_PVF',101,'KP012542','','','');
xxeis.eis_rsc_ins.rsec( 'HDS-WhiteCap_Fringe','101','','XXCUS_GL_MANAGER',101,'KP012542','','','');
xxeis.eis_rsc_ins.rsec( 'HDS-WhiteCap_Fringe','101','','XXCUS_GL_MANAGER_GLOBAL',101,'KP012542','','','');
xxeis.eis_rsc_ins.rsec( 'HDS-WhiteCap_Fringe','101','','XXCUS_GL_MANAGER_PVF',101,'KP012542','','','');
xxeis.eis_rsc_ins.rsec( 'HDS-WhiteCap_Fringe','101','','HDS_CAD_MNTH_END_PROCS',101,'KP012542','','','');
xxeis.eis_rsc_ins.rsec( 'HDS-WhiteCap_Fringe','101','','GNRL_LDGR_FSS',101,'KP012542','','','');
xxeis.eis_rsc_ins.rsec( 'HDS-WhiteCap_Fringe','101','','GNRL_LDGR_LTMR_FSS',101,'KP012542','','','');
xxeis.eis_rsc_ins.rsec( 'HDS-WhiteCap_Fringe','101','','HDS_GNRL_LDGR_CAD',101,'KP012542','','','');
xxeis.eis_rsc_ins.rsec( 'HDS-WhiteCap_Fringe','101','','HDS_GNRL_LDGR_SPR_USR',101,'KP012542','','','');
xxeis.eis_rsc_ins.rsec( 'HDS-WhiteCap_Fringe','20005','','XXWC_VIEW_ALL_EIS_REPORTS',101,'KP012542','','','');
--Inserting Report Pivots - HDS-WhiteCap_Fringe
--Inserting Report Distribution Details 
--Inserting Report Distribution Details 
--Inserting Report SubTotals Details 
--Inserting Report aggregations 
--Inserting Report   Version details- HDS-WhiteCap_Fringe
xxeis.eis_rsc_ins.rv( 'HDS-WhiteCap_Fringe','','HDS-WhiteCap_Fringe','SA059956','29-JUN-2017');
ELSE
RAISE_APPLICATION_ERROR (-20001,'Module does not exist. Import/Create the module with id 101');
END IF;
END;
/
