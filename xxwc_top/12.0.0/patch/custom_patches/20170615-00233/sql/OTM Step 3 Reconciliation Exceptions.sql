--Report Name            : OTM Step 3: Reconciliation Exceptions
--Import Instructions    : Run this file in XXEIS user from SQL*Plus or any tool like Toad, SQL Navigator. 
--Creating View XXEIS_756490_HOAXEI_V
prompt Table type Objects cannot be imported
set scan off define off
prompt Creating Object APPS.XXEIS_756490_HOAXEI_V
set scan off define off
DECLARE
mod_exist varchar2(1);
stmt CLOB;
v_objlength INTEGER := 0;
v_offset INTEGER := 1;
v_sql_long DBMS_SQL.varchar2s;
v_sql_long_count INTEGER := 1;
c NUMBER;
r NUMBER;
l_stmt varchar2(32000);
l_view_cur pls_integer;
l_view_rows NUMBER;
BEGIN 
mod_exist := XXEIS.EIS_RSC_INSERT.check_if_module_exist(682);
IF mod_exist = 'Y' THEN 
dbms_lob.createtemporary(stmt,TRUE);
dbms_lob.write( lob_loc   => stmt,amount    => length('create or replace view APPS.XXEIS_756490_HOAXEI_V
 ("ACCRUAL_TYPE","ADJUSTMENT_TYPE","FISCAL_MONTH","ADJUSTMENT_TYPE_ID","RESALE_LINE_ID","ACCRUAL_AMOUNT","UTILIZATION_ID","XLA_ORG","ACCRUAL_GL_DATE","LEDGER_NAME","XLA_EVENT_STATUS_CODE","XLA_PROCESS_STATUS_CODE","XLA_GL_TRANSFER_FLAG","XLA_ACCOUNTING_DATE","XLA_FISCAL_PERIOD","XLA_ERROR_LINE_NUM","XLA_ERROR_MSG","XLA_ERROR_MSG_NO","OFFER_CODE","OFFER_NAME","OFFER_TYPE","OFFER_AUTOPAY_PARTY_ID","OFFER_BENEFICIARY_ACCT_ID","OFFER_STATUS_CODE","OFFER_ACTIVITY_TYPE","OFFER_AUTOPAY_FLAG","OFFER_CURRENCY","OFFER_AUTO_RENEWAL","OFFER_AGREEMENT_YR","PAYMENT_METHOD","PAYMENT_FREQUENCY","OFFER_GUARANTEED_AMT","QP_LIST_HEADER_ID","OFFER_GLOBAL_FLAG","PROGRAM_ID","PROGRAM_NAME","RECEIPT_PRODUCT_ID","RECEIPT_CREATION_DATE","RECEIPT_GL_DATE","PO_NUMBER","RECEIPT_LOB_UOM","RECEIPT_ORACLE_UOM","RECEIPT_ORIGINAL_PRICE","OFU_CUST_ACCOUNT_ID","OFU_MVID_NAME","OFU_MVID","RECEIPT_LOB_BRANCH_ID","BILLTO_CUST_ACCT_BR_ACCT_NAME","RECEIPT_LOB_PARTY_ID","RECEIPT_LOB_NAME","RECEIPT_DATE_RECIEVED","SELLING_PRICE","QUANTITY","SKU_NUMBER","PAY_TO_VENDOR_CODE","RECEIPT_DIVISION","SRC_SYS_NM","RECEIPT_NUMBER","BU_BRANCH_CODE","LOCATION_SEGMENT","FINANCIAL_LOCATION","FREIGHT_DISCOUNT","PAYMENT_DISCOUNT"
) as '),offset    => 1, BUFFER    => 'create or replace view APPS.XXEIS_756490_HOAXEI_V
 ("ACCRUAL_TYPE","ADJUSTMENT_TYPE","FISCAL_MONTH","ADJUSTMENT_TYPE_ID","RESALE_LINE_ID","ACCRUAL_AMOUNT","UTILIZATION_ID","XLA_ORG","ACCRUAL_GL_DATE","LEDGER_NAME","XLA_EVENT_STATUS_CODE","XLA_PROCESS_STATUS_CODE","XLA_GL_TRANSFER_FLAG","XLA_ACCOUNTING_DATE","XLA_FISCAL_PERIOD","XLA_ERROR_LINE_NUM","XLA_ERROR_MSG","XLA_ERROR_MSG_NO","OFFER_CODE","OFFER_NAME","OFFER_TYPE","OFFER_AUTOPAY_PARTY_ID","OFFER_BENEFICIARY_ACCT_ID","OFFER_STATUS_CODE","OFFER_ACTIVITY_TYPE","OFFER_AUTOPAY_FLAG","OFFER_CURRENCY","OFFER_AUTO_RENEWAL","OFFER_AGREEMENT_YR","PAYMENT_METHOD","PAYMENT_FREQUENCY","OFFER_GUARANTEED_AMT","QP_LIST_HEADER_ID","OFFER_GLOBAL_FLAG","PROGRAM_ID","PROGRAM_NAME","RECEIPT_PRODUCT_ID","RECEIPT_CREATION_DATE","RECEIPT_GL_DATE","PO_NUMBER","RECEIPT_LOB_UOM","RECEIPT_ORACLE_UOM","RECEIPT_ORIGINAL_PRICE","OFU_CUST_ACCOUNT_ID","OFU_MVID_NAME","OFU_MVID","RECEIPT_LOB_BRANCH_ID","BILLTO_CUST_ACCT_BR_ACCT_NAME","RECEIPT_LOB_PARTY_ID","RECEIPT_LOB_NAME","RECEIPT_DATE_RECIEVED","SELLING_PRICE","QUANTITY","SKU_NUMBER","PAY_TO_VENDOR_CODE","RECEIPT_DIVISION","SRC_SYS_NM","RECEIPT_NUMBER","BU_BRANCH_CODE","LOCATION_SEGMENT","FINANCIAL_LOCATION","FREIGHT_DISCOUNT","PAYMENT_DISCOUNT"
) as ');
l_stmt :=  'select utilization_type accrual_type,
adjustment_type,
fiscal_month,
adjustment_type_id,
object_id resale_line_id,
util_acctd_amount accrual_amount,
utilization_id,
oxa_org_id xla_org,
ofu_gl_date accrual_gl_date,
ozf_ledger_name ledger_name,
xlae_event_status_code xla_event_status_code,
xlae_process_status_code xla_process_status_code,
xaeh_gl_xfer_status_code xla_gl_transfer_flag,
xaeh_accounting_date xla_accounting_date,
xaeh_period_name xla_fiscal_period,
err_ae_line_num xla_error_line_num,
encoded_msg xla_error_msg,
message_number xla_error_msg_no,
offer_code,
offer_name,
offer_type,
autopay_party_id offer_autopay_party_id,
beneficiary_account_id offer_beneficiary_acct_id,
offer_status_code,
activity_media_name offer_activity_type,
offer_autopay_flag,
ofu_currency_code offer_currency,
auto_renewal offer_auto_renewal,
calendar_year offer_agreement_yr,
payment_method,
payment_frequency,
guaranteed_amount offer_guaranteed_amt,
qp_list_header_id,
qp_global_flag offer_global_flag,
fund_id program_id,
fund_name program_name,
product_id receipt_product_id,
resale_date_created receipt_creation_date,
date_invoiced receipt_gl_date,
po_number,
purchase_UOM_code receipt_lob_UOM,
uom_code receipt_oracle_UOM,
purchase_price receipt_original_price,
ofu_cust_account_id,
ofu_mvid_name,
ofu_mvid,
billto_cust_acct_id_br receipt_LOB_branch_id,
billto_cust_acct_br_acct_name,
bill_to_party_id receipt_LOB_party_id,
bill_to_party_name_lob receipt_LOB_name,
date_ordered receipt_date_recieved,
selling_price,
quantity,
item_number sku_number,
resale_line_attribute1 pay_to_vendor_code,
resale_line_attribute2 receipt_division,
resale_line_attribute3 src_sys_nm,
resale_line_attribute5 receipt_number,
resale_line_attribute6 BU_branch_code,
resale_line_attribute11 location_segment,
resale_line_attribute12 financial_location,
resale_line_attribute13 freight_discount,
resale_line_attribute14 payment_discount
from xxcus.xxcus_ozf_xla_accruals_b

';
dbms_lob.APPEND( dest_lob  => stmt,      src_lob   => l_stmt );
v_objlength := DBMS_LOB.getlength (stmt);
IF v_objlength <= 32000 THEN 
EXECUTE IMMEDIATE to_char(stmt);
ELSE
v_offset := 1; 
WHILE v_offset <= v_objlength
LOOP
-- each record is 256 bytes from the LOB
v_sql_long (v_sql_long_count) := DBMS_LOB.SUBSTR (stmt, 256, v_offset);
-- increment the record count
v_sql_long_count := v_sql_long_count + 1;
-- figure out the new offset
v_offset := v_offset + 256;
END LOOP;
BEGIN
  -- open cursor
 c := DBMS_SQL.open_cursor;
 -- parse VARCHAR2S table
 DBMS_SQL.parse (c, v_sql_long, 1, v_sql_long_count - 1, FALSE, 1);
 -- Execute the cursor
r := DBMS_SQL.EXECUTE (c);
-- Close the cursor
 DBMS_SQL.close_cursor (c);
 END;
END IF;
ELSE
RAISE_APPLICATION_ERROR (-20001,'Module does not exist. Import/Create the module with id 682');
END IF;
END;
/
prompt Creating Object Data XXEIS_756490_HOAXEI_V
set scan off define off
DECLARE
mod_exist VARCHAR2(1);
BEGIN 
mod_exist := XXEIS.EIS_RSC_INSERT.check_if_module_exist(682);
IF mod_exist = 'Y' THEN 
--Inserting Object XXEIS_756490_HOAXEI_V
xxeis.eis_rsc_ins.v( 'XXEIS_756490_HOAXEI_V',682,'Paste SQL View for OTM Step 3: Reconciliation Exceptions','','','','DV003828','APPS','OTM Step 3: Reconciliation Exceptions View','X7HV','','Y','VIEW','US','','','');
--Delete Object Columns for XXEIS_756490_HOAXEI_V
xxeis.eis_rsc_utility.delete_view_rows('XXEIS_756490_HOAXEI_V',682,FALSE);
--Inserting Object Columns for XXEIS_756490_HOAXEI_V
xxeis.eis_rsc_ins.vc( 'XXEIS_756490_HOAXEI_V','ACCRUAL_AMOUNT',682,'','','','~T~D~2','','DV003828','NUMBER','','','Accrual Amount','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_756490_HOAXEI_V','XLA_ORG',682,'','','','','','DV003828','NUMBER','','','Xla Org','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_756490_HOAXEI_V','XLA_FISCAL_PERIOD',682,'','','','','','DV003828','VARCHAR2','','','Xla Fiscal Period','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_756490_HOAXEI_V','XLA_ERROR_MSG',682,'','','','','','DV003828','VARCHAR2','','','Xla Error Msg','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_756490_HOAXEI_V','OFFER_NAME',682,'','','','','','DV003828','VARCHAR2','','','Offer Name','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_756490_HOAXEI_V','OFU_MVID_NAME',682,'','','','','','DV003828','VARCHAR2','','','Ofu Mvid Name','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_756490_HOAXEI_V','BILLTO_CUST_ACCT_BR_ACCT_NAME',682,'','','','','','DV003828','VARCHAR2','','','Billto Cust Acct Br Acct Name','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_756490_HOAXEI_V','RECEIPT_DIVISION',682,'','','','','','DV003828','VARCHAR2','','','Receipt Division','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_756490_HOAXEI_V','SRC_SYS_NM',682,'','','','','','DV003828','VARCHAR2','','','Src Sys Nm','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_756490_HOAXEI_V','BU_BRANCH_CODE',682,'','','','','','DV003828','VARCHAR2','','','Bu Branch Code','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_756490_HOAXEI_V','LOCATION_SEGMENT',682,'','','','','','DV003828','VARCHAR2','','','Location Segment','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_756490_HOAXEI_V','FINANCIAL_LOCATION',682,'','','','','','DV003828','VARCHAR2','','','Financial Location','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_756490_HOAXEI_V','ACCRUAL_TYPE',682,'','','','','','DV003828','VARCHAR2','','','Accrual Type','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_756490_HOAXEI_V','ADJUSTMENT_TYPE',682,'','','','','','DV003828','VARCHAR2','','','Adjustment Type','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_756490_HOAXEI_V','ADJUSTMENT_TYPE_ID',682,'','','','','','DV003828','NUMBER','','','Adjustment Type Id','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_756490_HOAXEI_V','RESALE_LINE_ID',682,'','','','','','DV003828','NUMBER','','','Resale Line Id','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_756490_HOAXEI_V','UTILIZATION_ID',682,'','','','','','DV003828','NUMBER','','','Utilization Id','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_756490_HOAXEI_V','ACCRUAL_GL_DATE',682,'','','','','','DV003828','DATE','','','Accrual Gl Date','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_756490_HOAXEI_V','LEDGER_NAME',682,'','','','','','DV003828','VARCHAR2','','','Ledger Name','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_756490_HOAXEI_V','XLA_EVENT_STATUS_CODE',682,'','','','','','DV003828','VARCHAR2','','','Xla Event Status Code','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_756490_HOAXEI_V','XLA_PROCESS_STATUS_CODE',682,'','','','','','DV003828','VARCHAR2','','','Xla Process Status Code','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_756490_HOAXEI_V','XLA_GL_TRANSFER_FLAG',682,'','','','','','DV003828','VARCHAR2','','','Xla Gl Transfer Flag','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_756490_HOAXEI_V','XLA_ACCOUNTING_DATE',682,'','','','','','DV003828','DATE','','','Xla Accounting Date','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_756490_HOAXEI_V','XLA_ERROR_LINE_NUM',682,'','','','','','DV003828','NUMBER','','','Xla Error Line Num','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_756490_HOAXEI_V','XLA_ERROR_MSG_NO',682,'','','','','','DV003828','NUMBER','','','Xla Error Msg No','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_756490_HOAXEI_V','OFFER_CODE',682,'','','','','','DV003828','VARCHAR2','','','Offer Code','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_756490_HOAXEI_V','OFFER_TYPE',682,'','','','','','DV003828','VARCHAR2','','','Offer Type','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_756490_HOAXEI_V','OFFER_AUTOPAY_PARTY_ID',682,'','','','','','DV003828','NUMBER','','','Offer Autopay Party Id','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_756490_HOAXEI_V','OFFER_BENEFICIARY_ACCT_ID',682,'','','','','','DV003828','NUMBER','','','Offer Beneficiary Acct Id','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_756490_HOAXEI_V','OFFER_STATUS_CODE',682,'','','','','','DV003828','VARCHAR2','','','Offer Status Code','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_756490_HOAXEI_V','OFFER_ACTIVITY_TYPE',682,'','','','','','DV003828','VARCHAR2','','','Offer Activity Type','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_756490_HOAXEI_V','OFFER_AUTOPAY_FLAG',682,'','','','','','DV003828','VARCHAR2','','','Offer Autopay Flag','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_756490_HOAXEI_V','OFFER_CURRENCY',682,'','','','','','DV003828','VARCHAR2','','','Offer Currency','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_756490_HOAXEI_V','OFFER_AUTO_RENEWAL',682,'','','','','','DV003828','VARCHAR2','','','Offer Auto Renewal','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_756490_HOAXEI_V','OFFER_AGREEMENT_YR',682,'','','','','','DV003828','VARCHAR2','','','Offer Agreement Yr','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_756490_HOAXEI_V','PAYMENT_METHOD',682,'','','','','','DV003828','VARCHAR2','','','Payment Method','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_756490_HOAXEI_V','PAYMENT_FREQUENCY',682,'','','','','','DV003828','VARCHAR2','','','Payment Frequency','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_756490_HOAXEI_V','OFFER_GUARANTEED_AMT',682,'','','','','','DV003828','VARCHAR2','','','Offer Guaranteed Amt','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_756490_HOAXEI_V','QP_LIST_HEADER_ID',682,'','','','','','DV003828','NUMBER','','','Qp List Header Id','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_756490_HOAXEI_V','OFFER_GLOBAL_FLAG',682,'','','','','','DV003828','VARCHAR2','','','Offer Global Flag','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_756490_HOAXEI_V','PROGRAM_ID',682,'','','','','','DV003828','NUMBER','','','Program Id','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_756490_HOAXEI_V','PROGRAM_NAME',682,'','','','','','DV003828','VARCHAR2','','','Program Name','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_756490_HOAXEI_V','RECEIPT_PRODUCT_ID',682,'','','','','','DV003828','NUMBER','','','Receipt Product Id','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_756490_HOAXEI_V','RECEIPT_CREATION_DATE',682,'','','','','','DV003828','DATE','','','Receipt Creation Date','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_756490_HOAXEI_V','RECEIPT_GL_DATE',682,'','','','','','DV003828','DATE','','','Receipt Gl Date','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_756490_HOAXEI_V','PO_NUMBER',682,'','','','','','DV003828','VARCHAR2','','','Po Number','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_756490_HOAXEI_V','RECEIPT_LOB_UOM',682,'','','','','','DV003828','VARCHAR2','','','Receipt Lob Uom','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_756490_HOAXEI_V','RECEIPT_ORACLE_UOM',682,'','','','','','DV003828','VARCHAR2','','','Receipt Oracle Uom','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_756490_HOAXEI_V','RECEIPT_ORIGINAL_PRICE',682,'','','','~T~D~2','','DV003828','NUMBER','','','Receipt Original Price','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_756490_HOAXEI_V','OFU_CUST_ACCOUNT_ID',682,'','','','','','DV003828','NUMBER','','','Ofu Cust Account Id','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_756490_HOAXEI_V','OFU_MVID',682,'','','','','','DV003828','VARCHAR2','','','Ofu Mvid','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_756490_HOAXEI_V','RECEIPT_LOB_BRANCH_ID',682,'','','','','','DV003828','NUMBER','','','Receipt Lob Branch Id','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_756490_HOAXEI_V','RECEIPT_LOB_PARTY_ID',682,'','','','','','DV003828','NUMBER','','','Receipt Lob Party Id','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_756490_HOAXEI_V','RECEIPT_LOB_NAME',682,'','','','','','DV003828','VARCHAR2','','','Receipt Lob Name','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_756490_HOAXEI_V','RECEIPT_DATE_RECIEVED',682,'','','','','','DV003828','DATE','','','Receipt Date Recieved','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_756490_HOAXEI_V','SELLING_PRICE',682,'','','','~T~D~2','','DV003828','NUMBER','','','Selling Price','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_756490_HOAXEI_V','QUANTITY',682,'','','','~T~D~2','','DV003828','NUMBER','','','Quantity','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_756490_HOAXEI_V','SKU_NUMBER',682,'','','','','','DV003828','VARCHAR2','','','Sku Number','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_756490_HOAXEI_V','PAY_TO_VENDOR_CODE',682,'','','','','','DV003828','VARCHAR2','','','Pay To Vendor Code','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_756490_HOAXEI_V','RECEIPT_NUMBER',682,'','','','','','DV003828','VARCHAR2','','','Receipt Number','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_756490_HOAXEI_V','FREIGHT_DISCOUNT',682,'','','','','','DV003828','VARCHAR2','','','Freight Discount','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_756490_HOAXEI_V','PAYMENT_DISCOUNT',682,'','','','','','DV003828','VARCHAR2','','','Payment Discount','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_756490_HOAXEI_V','FISCAL_MONTH',682,'','','','','','DV003828','VARCHAR2','','','Fiscal Month','','','','US','');
--Inserting Object Components for XXEIS_756490_HOAXEI_V
--Inserting Object Component Joins for XXEIS_756490_HOAXEI_V
ELSE
RAISE_APPLICATION_ERROR (-20001,'Module does not exist. Import/Create the module with id 682');
END IF;
END;
/
prompt Creating Report LOV Data for OTM Step 3: Reconciliation Exceptions
set scan off define off
DECLARE
mod_exist VARCHAR2(1);
BEGIN 
mod_exist := XXEIS.EIS_RSC_INSERT.check_if_module_exist(682);
IF mod_exist = 'Y' THEN 
--Inserting Report LOVs - OTM Step 3: Reconciliation Exceptions
xxeis.eis_rsc_ins.lov( 682,'select distinct name
from ozf.OZF_TIME_ENT_PERIOD','','LOV PERIOD','LOV NAME FROM ozf.OZF_TIME_ENT_PERIOD is the Period','ANONYMOUS',NULL,'N','','','','','','','US');
null;
ELSE
RAISE_APPLICATION_ERROR (-20001,'Module does not exist. Import/Create the module with id 682');
END IF;
END;
/
prompt Creating Report Data for OTM Step 3: Reconciliation Exceptions
set scan off define off
DECLARE
mod_exist varchar2(1);
BEGIN 
mod_exist := XXEIS.EIS_RSC_INSERT.check_if_module_exist(682);
IF mod_exist = 'Y' THEN 
--Deleting Report data - OTM Step 3: Reconciliation Exceptions
xxeis.eis_rsc_utility.delete_report_rows( 'OTM Step 3: Reconciliation Exceptions',682 );
--Inserting Report - OTM Step 3: Reconciliation Exceptions
xxeis.eis_rsc_ins.r( 682,'OTM Step 3: Reconciliation Exceptions','','Report used to reconcile fiscal period Trade Management Accrual totals with the GL based on GL balance on 131100.  Ensures all TM entries are posted to the GL for a specific fiscal period.
This report is run if there are TM entries that are in I status that have not transferred to the GL.  This report will provide the error message along with the branch and GL account information related to the accrual lines in order to determine corrective action needed to post these entries.
','','','','MC027824','XXEIS_756490_HOAXEI_V','Y','','select utilization_type accrual_type,
adjustment_type,
fiscal_month,
adjustment_type_id,
object_id resale_line_id,
util_acctd_amount accrual_amount,
utilization_id,
oxa_org_id xla_org,
ofu_gl_date accrual_gl_date,
ozf_ledger_name ledger_name,
xlae_event_status_code xla_event_status_code,
xlae_process_status_code xla_process_status_code,
xaeh_gl_xfer_status_code xla_gl_transfer_flag,
xaeh_accounting_date xla_accounting_date,
xaeh_period_name xla_fiscal_period,
err_ae_line_num xla_error_line_num,
encoded_msg xla_error_msg,
message_number xla_error_msg_no,
offer_code,
offer_name,
offer_type,
autopay_party_id offer_autopay_party_id,
beneficiary_account_id offer_beneficiary_acct_id,
offer_status_code,
activity_media_name offer_activity_type,
offer_autopay_flag,
ofu_currency_code offer_currency,
auto_renewal offer_auto_renewal,
calendar_year offer_agreement_yr,
payment_method,
payment_frequency,
guaranteed_amount offer_guaranteed_amt,
qp_list_header_id,
qp_global_flag offer_global_flag,
fund_id program_id,
fund_name program_name,
product_id receipt_product_id,
resale_date_created receipt_creation_date,
date_invoiced receipt_gl_date,
po_number,
purchase_UOM_code receipt_lob_UOM,
uom_code receipt_oracle_UOM,
purchase_price receipt_original_price,
ofu_cust_account_id,
ofu_mvid_name,
ofu_mvid,
billto_cust_acct_id_br receipt_LOB_branch_id,
billto_cust_acct_br_acct_name,
bill_to_party_id receipt_LOB_party_id,
bill_to_party_name_lob receipt_LOB_name,
date_ordered receipt_date_recieved,
selling_price,
quantity,
item_number sku_number,
resale_line_attribute1 pay_to_vendor_code,
resale_line_attribute2 receipt_division,
resale_line_attribute3 src_sys_nm,
resale_line_attribute5 receipt_number,
resale_line_attribute6 BU_branch_code,
resale_line_attribute11 location_segment,
resale_line_attribute12 financial_location,
resale_line_attribute13 freight_discount,
resale_line_attribute14 payment_discount
from xxcus.xxcus_ozf_xla_accruals_b
','MC027824','','N','MONTH-END','','CSV,EXCEL,Pivot Excel,','N','','','','','','N','APPS','US','','','','','','','','','','','','','','','','');
--Inserting Report Columns - OTM Step 3: Reconciliation Exceptions
xxeis.eis_rsc_ins.rc( 'OTM Step 3: Reconciliation Exceptions',682,'ACCRUAL_AMOUNT','Accrual Amount','','','~.~,~','default','','10','N','Y','','','','','','','MC027824','N','N','','XXEIS_756490_HOAXEI_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'OTM Step 3: Reconciliation Exceptions',682,'XLA_ERROR_MSG','Error Message','','','','default','','1','N','Y','','','','','','','MC027824','N','N','','XXEIS_756490_HOAXEI_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'OTM Step 3: Reconciliation Exceptions',682,'BILLTO_CUST_ACCT_BR_ACCT_NAME','BU Branch Account No','','','','default','','5','N','Y','','','','','','','MC027824','N','N','','XXEIS_756490_HOAXEI_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'OTM Step 3: Reconciliation Exceptions',682,'OFFER_NAME','Agreement Name','','','','default','','9','N','Y','','','','','','','MC027824','N','N','','XXEIS_756490_HOAXEI_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'OTM Step 3: Reconciliation Exceptions',682,'BU_BRANCH_CODE','Bu Branch Code','','','','default','','4','N','Y','','','','','','','MC027824','N','N','','XXEIS_756490_HOAXEI_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'OTM Step 3: Reconciliation Exceptions',682,'SRC_SYS_NM','Source System Name','','','','default','','3','N','Y','','','','','','','MC027824','N','N','','XXEIS_756490_HOAXEI_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'OTM Step 3: Reconciliation Exceptions',682,'RECEIPT_DIVISION','Receipt Division','','','','default','','2','N','Y','','','','','','','MC027824','N','N','','XXEIS_756490_HOAXEI_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'OTM Step 3: Reconciliation Exceptions',682,'LOCATION_SEGMENT','Location Product','','','','default','','6','N','Y','','','','','','','MC027824','N','N','','XXEIS_756490_HOAXEI_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'OTM Step 3: Reconciliation Exceptions',682,'OFU_MVID_NAME','Vendor Name','','','','default','','8','N','Y','','','','','','','MC027824','N','N','','XXEIS_756490_HOAXEI_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'OTM Step 3: Reconciliation Exceptions',682,'FINANCIAL_LOCATION','Financial Location','','','','default','','7','N','Y','','','','','','','MC027824','N','N','','XXEIS_756490_HOAXEI_V','','','','US','','');
--Inserting Report Parameters - OTM Step 3: Reconciliation Exceptions
xxeis.eis_rsc_ins.rp( 'OTM Step 3: Reconciliation Exceptions',682,'Fiscal Period','','XLA_FISCAL_PERIOD','IN','LOV PERIOD','','VARCHAR2','Y','Y','2','N','Y','CONSTANT','MC027824','Y','N','','','','XXEIS_756490_HOAXEI_V','','','US','');
xxeis.eis_rsc_ins.rp( 'OTM Step 3: Reconciliation Exceptions',682,'ORG','','XLA_ORG','IN','','''101'',''102''','VARCHAR2','Y','Y','1','Y','Y','CONSTANT','MC027824','Y','N','','','','XXEIS_756490_HOAXEI_V','','','US','');
--Inserting Dependent Parameters - OTM Step 3: Reconciliation Exceptions
--Inserting Report Conditions - OTM Step 3: Reconciliation Exceptions
xxeis.eis_rsc_ins.rcnh( 'OTM Step 3: Reconciliation Exceptions',682,'X7HV.XLA_PROCESS_STATUS_CODE IN ''I'' ','ADVANCED','','1#$#','Y','','1');
xxeis.eis_rsc_ins.rcnd( '','','','','','','','','','','','','','','','IN','Y','N','X7HV.XLA_PROCESS_STATUS_CODE','''I''','','','1',682,'OTM Step 3: Reconciliation Exceptions','X7HV.XLA_PROCESS_STATUS_CODE IN ''I'' ');
xxeis.eis_rsc_ins.rcnh( 'OTM Step 3: Reconciliation Exceptions',682,'XLA_FISCAL_PERIOD IN :Fiscal Period ','SIMPLE','','','Y','','2');
xxeis.eis_rsc_ins.rcnd( '','','XLA_FISCAL_PERIOD','','Fiscal Period','','','','','XXEIS_756490_HOAXEI_V','','','','','','IN','Y','Y','','','','','1',682,'OTM Step 3: Reconciliation Exceptions','XLA_FISCAL_PERIOD IN :Fiscal Period ');
xxeis.eis_rsc_ins.rcnh( 'OTM Step 3: Reconciliation Exceptions',682,'XLA_ORG IN :ORG ','SIMPLE','','','Y','','3');
xxeis.eis_rsc_ins.rcnd( '','','XLA_ORG','','ORG','','','','','XXEIS_756490_HOAXEI_V','','','','','','IN','Y','Y','','','','','1',682,'OTM Step 3: Reconciliation Exceptions','XLA_ORG IN :ORG ');
xxeis.eis_rsc_ins.rcnh( 'OTM Step 3: Reconciliation Exceptions',682,'Free Text ','FREE_TEXT','','','Y','','4');
xxeis.eis_rsc_ins.rcnd( '','','','','','','','','','','','','','','','','Y','','','','','and fiscal_month =UPPER(SUBSTR(:Fiscal Period,1,3))','1',682,'OTM Step 3: Reconciliation Exceptions','Free Text ');
--Inserting Report Sorts - OTM Step 3: Reconciliation Exceptions
--Inserting Report Triggers - OTM Step 3: Reconciliation Exceptions
--inserting report templates - OTM Step 3: Reconciliation Exceptions
--Inserting Report Portals - OTM Step 3: Reconciliation Exceptions
--inserting report dashboards - OTM Step 3: Reconciliation Exceptions
--Exporting report views table information -  
xxeis.eis_rsc_ins.rviews( 'OTM Step 3: Reconciliation Exceptions','682','XXEIS_756490_HOAXEI_V','XXEIS_756490_HOAXEI_V','N','');
--inserting report security - OTM Step 3: Reconciliation Exceptions
xxeis.eis_rsc_ins.rsec( 'OTM Step 3: Reconciliation Exceptions','682','','XXCUS_TM_USER',682,'MC027824','','','');
xxeis.eis_rsc_ins.rsec( 'OTM Step 3: Reconciliation Exceptions','682','','XXCUS_TM_ADMIN_USER',682,'MC027824','','','');
xxeis.eis_rsc_ins.rsec( 'OTM Step 3: Reconciliation Exceptions','682','','XXCUS_TM_FORMS_RESP',682,'MC027824','','','');
xxeis.eis_rsc_ins.rsec( 'OTM Step 3: Reconciliation Exceptions','682','','XXCUS_TM_ADM_FORMS_RESP',682,'MC027824','','','');
xxeis.eis_rsc_ins.rsec( 'OTM Step 3: Reconciliation Exceptions','20005','','XXWC_VIEW_ALL_EIS_REPORTS',682,'MC027824','','','');
--Inserting Report Pivots - OTM Step 3: Reconciliation Exceptions
--Inserting Report Distribution Details 
--Inserting Report Distribution Details 
--Inserting Report SubTotals Details 
--Inserting Report aggregations 
--Inserting Report   Version details- OTM Step 3: Reconciliation Exceptions
xxeis.eis_rsc_ins.rv( 'OTM Step 3: Reconciliation Exceptions','','OTM Step 3: Reconciliation Exceptions','SA059956','29-JUN-2017');
ELSE
RAISE_APPLICATION_ERROR (-20001,'Module does not exist. Import/Create the module with id 682');
END IF;
END;
/
