--Report Name            : WC User PO Approval Limit
--Import Instructions    : Run this file in XXEIS user from SQL*Plus or any tool like Toad, SQL Navigator. 
prompt Creating Object Data EIS_PO_APPROVAL_LIMIT_V
set scan off define off
DECLARE
mod_exist VARCHAR2(1);
BEGIN 
mod_exist := XXEIS.EIS_RSC_INSERT.check_if_module_exist(85000);
IF mod_exist = 'Y' THEN 
--Inserting Object EIS_PO_APPROVAL_LIMIT_V
xxeis.eis_rsc_ins.v( 'EIS_PO_APPROVAL_LIMIT_V',85000,'','','','','ANONYMOUS','XXEIS','Eis Po Approval Limit V','EPALV','','','VIEW','US','','','');
--Delete Object Columns for EIS_PO_APPROVAL_LIMIT_V
xxeis.eis_rsc_utility.delete_view_rows('EIS_PO_APPROVAL_LIMIT_V',85000,FALSE);
--Inserting Object Columns for EIS_PO_APPROVAL_LIMIT_V
xxeis.eis_rsc_ins.vc( 'EIS_PO_APPROVAL_LIMIT_V','AMOUNT_LIMIT',85000,'Amount Limit','AMOUNT_LIMIT','','~T~D~2','','ANONYMOUS','NUMBER','','','Amount Limit','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_PO_APPROVAL_LIMIT_V','GROUP_NAME',85000,'Group Name','GROUP_NAME','','','','ANONYMOUS','VARCHAR2','','','Group Name','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_PO_APPROVAL_LIMIT_V','DOCUMENT_TYPE',85000,'Document Type','DOCUMENT_TYPE','','','','ANONYMOUS','VARCHAR2','','','Document Type','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_PO_APPROVAL_LIMIT_V','BUYER_JOB',85000,'Buyer Job','BUYER_JOB','','','','ANONYMOUS','VARCHAR2','','','Buyer Job','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_PO_APPROVAL_LIMIT_V','BUYER_POSITION',85000,'Buyer Position','BUYER_POSITION','','','','ANONYMOUS','VARCHAR2','','','Buyer Position','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_PO_APPROVAL_LIMIT_V','IS_BUYER',85000,'Is Buyer','IS_BUYER','','','','ANONYMOUS','VARCHAR2','','','Is Buyer','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_PO_APPROVAL_LIMIT_V','EMPLOYEE_NUMBER',85000,'Employee Number','EMPLOYEE_NUMBER','','','','ANONYMOUS','VARCHAR2','','','Employee Number','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_PO_APPROVAL_LIMIT_V','FAX',85000,'Fax','FAX','','','','ANONYMOUS','VARCHAR2','','','Fax','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_PO_APPROVAL_LIMIT_V','EMAIL_ADDRESS',85000,'Email Address','EMAIL_ADDRESS','','','','ANONYMOUS','VARCHAR2','','','Email Address','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_PO_APPROVAL_LIMIT_V','FULL_NAME',85000,'Full Name','FULL_NAME','','','','ANONYMOUS','VARCHAR2','','','Full Name','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_PO_APPROVAL_LIMIT_V','USER_NAME',85000,'User Name','USER_NAME','','','','ANONYMOUS','VARCHAR2','','','User Name','','','','US','');
--Inserting Object Components for EIS_PO_APPROVAL_LIMIT_V
--Inserting Object Component Joins for EIS_PO_APPROVAL_LIMIT_V
ELSE
RAISE_APPLICATION_ERROR (-20001,'Module does not exist. Import/Create the module with id 85000');
END IF;
END;
/
--There are no LOV/LOV TL entries for the report WC User PO Approval Limit
prompt Creating Report Data for WC User PO Approval Limit
set scan off define off
DECLARE
mod_exist varchar2(1);
BEGIN 
mod_exist := XXEIS.EIS_RSC_INSERT.check_if_module_exist(85000);
IF mod_exist = 'Y' THEN 
--Deleting Report data - WC User PO Approval Limit
xxeis.eis_rsc_utility.delete_report_rows( 'WC User PO Approval Limit',85000 );
--Inserting Report - WC User PO Approval Limit
xxeis.eis_rsc_ins.r( 85000,'WC User PO Approval Limit','','Provide a way the support team may view the PO approval limit associated with Buyer Positions and individuals','','','','HT038687','EIS_PO_APPROVAL_LIMIT_V','Y','','','HT038687','','N','WC Audit Reports','','CSV,EXCEL,','N','','','','','','','','US','','','','','','','','','','','','','','','','');
--Inserting Report Columns - WC User PO Approval Limit
xxeis.eis_rsc_ins.rc( 'WC User PO Approval Limit',85000,'AMOUNT_LIMIT','Amount Limit','Amount Limit','','~T~D~0','default','','11','N','','','','','','','','HT038687','N','N','','EIS_PO_APPROVAL_LIMIT_V','','','SUM','US','','');
xxeis.eis_rsc_ins.rc( 'WC User PO Approval Limit',85000,'BUYER_JOB','Buyer Job','Buyer Job','','','default','','8','N','','','','','','','','HT038687','N','N','','EIS_PO_APPROVAL_LIMIT_V','','','GROUP_BY','US','','');
xxeis.eis_rsc_ins.rc( 'WC User PO Approval Limit',85000,'BUYER_POSITION','Buyer Position','Buyer Position','','','default','','7','N','','','','','','','','HT038687','N','N','','EIS_PO_APPROVAL_LIMIT_V','','','GROUP_BY','US','','');
xxeis.eis_rsc_ins.rc( 'WC User PO Approval Limit',85000,'DOCUMENT_TYPE','Document Type','Document Type','','','default','','10','N','','','','','','','','HT038687','N','N','','EIS_PO_APPROVAL_LIMIT_V','','','GROUP_BY','US','','');
xxeis.eis_rsc_ins.rc( 'WC User PO Approval Limit',85000,'EMAIL_ADDRESS','Email Address','Email Address','','','default','','3','N','','','','','','','','HT038687','N','N','','EIS_PO_APPROVAL_LIMIT_V','','','GROUP_BY','US','','');
xxeis.eis_rsc_ins.rc( 'WC User PO Approval Limit',85000,'EMPLOYEE_NUMBER','Employee Number','Employee Number','','','default','','5','N','','','','','','','','HT038687','N','N','','EIS_PO_APPROVAL_LIMIT_V','','','GROUP_BY','US','','');
xxeis.eis_rsc_ins.rc( 'WC User PO Approval Limit',85000,'FAX','Fax','Fax','','','default','','4','N','','','','','','','','HT038687','N','N','','EIS_PO_APPROVAL_LIMIT_V','','','GROUP_BY','US','','');
xxeis.eis_rsc_ins.rc( 'WC User PO Approval Limit',85000,'FULL_NAME','Full Name','Full Name','','','default','','2','N','','','','','','','','HT038687','N','N','','EIS_PO_APPROVAL_LIMIT_V','','','GROUP_BY','US','','');
xxeis.eis_rsc_ins.rc( 'WC User PO Approval Limit',85000,'GROUP_NAME','Group Name','Group Name','','','default','','9','N','','','','','','','','HT038687','N','N','','EIS_PO_APPROVAL_LIMIT_V','','','GROUP_BY','US','','');
xxeis.eis_rsc_ins.rc( 'WC User PO Approval Limit',85000,'IS_BUYER','Is Buyer','Is Buyer','','','default','','6','N','','','','','','','','HT038687','N','N','','EIS_PO_APPROVAL_LIMIT_V','','','GROUP_BY','US','','');
xxeis.eis_rsc_ins.rc( 'WC User PO Approval Limit',85000,'USER_NAME','User Name','User Name','','','default','','1','N','','','','','','','','HT038687','N','N','','EIS_PO_APPROVAL_LIMIT_V','','','GROUP_BY','US','','');
--Inserting Report Parameters - WC User PO Approval Limit
xxeis.eis_rsc_ins.rp( 'WC User PO Approval Limit',85000,'User Name (NT ID)','User Name (NT ID)','USER_NAME','LIKE','','%','VARCHAR2','Y','Y','1','Y','Y','CONSTANT','HT038687','Y','','','','','EIS_PO_APPROVAL_LIMIT_V','','','US','');
xxeis.eis_rsc_ins.rp( 'WC User PO Approval Limit',85000,'Full Name (Last, First Name)','Full Name (Last, First Name)','FULL_NAME','LIKE','','%','VARCHAR2','N','Y','2','Y','Y','CONSTANT','HT038687','Y','','','','','EIS_PO_APPROVAL_LIMIT_V','','','US','');
--Inserting Dependent Parameters - WC User PO Approval Limit
--Inserting Report Conditions - WC User PO Approval Limit
xxeis.eis_rsc_ins.rcnh( 'WC User PO Approval Limit',85000,'FULL_NAME LIKE :Full Name (Last, First Name) ','SIMPLE','','','Y','','1');
xxeis.eis_rsc_ins.rcnd( '','','FULL_NAME','','Full Name (Last, First Name)','','','','','EIS_PO_APPROVAL_LIMIT_V','','','','','','LIKE','Y','Y','','','','','1',85000,'WC User PO Approval Limit','FULL_NAME LIKE :Full Name (Last, First Name) ');
xxeis.eis_rsc_ins.rcnh( 'WC User PO Approval Limit',85000,'USER_NAME LIKE :User Name (NT ID) ','SIMPLE','','','Y','','2');
xxeis.eis_rsc_ins.rcnd( '','','USER_NAME','','User Name (NT ID)','','','','','EIS_PO_APPROVAL_LIMIT_V','','','','','','LIKE','Y','Y','','','','','1',85000,'WC User PO Approval Limit','USER_NAME LIKE :User Name (NT ID) ');
--Inserting Report Sorts - WC User PO Approval Limit
--Inserting Report Triggers - WC User PO Approval Limit
--inserting report templates - WC User PO Approval Limit
--Inserting Report Portals - WC User PO Approval Limit
--inserting report dashboards - WC User PO Approval Limit
--Exporting report views table information -  
xxeis.eis_rsc_ins.rviews( 'WC User PO Approval Limit','85000','EIS_PO_APPROVAL_LIMIT_V','EIS_PO_APPROVAL_LIMIT_V','N','');
--inserting report security - WC User PO Approval Limit
xxeis.eis_rsc_ins.rsec( 'WC User PO Approval Limit','20005','','XXWC_IT_SECURITY_ADMINISTRATOR',85000,'HT038687','','','');
xxeis.eis_rsc_ins.rsec( 'WC User PO Approval Limit','20005','','XXWC_IT_OPERATIONS_ANALYST',85000,'HT038687','','','');
xxeis.eis_rsc_ins.rsec( 'WC User PO Approval Limit','20005','','XXWC_IT_FUNC_CONFIGURATOR',85000,'HT038687','','','');
xxeis.eis_rsc_ins.rsec( 'WC User PO Approval Limit','20005','','XXWC_VIEW_ALL_EIS_REPORTS',85000,'HT038687','','','');
xxeis.eis_rsc_ins.rsec( 'WC User PO Approval Limit','401','','XXWC_DATA_MNGT_SC',85000,'HT038687','','','');
xxeis.eis_rsc_ins.rsec( 'WC User PO Approval Limit','201','','XXWC_PUR_SUPER_USER',85000,'HT038687','','','');
--Inserting Report Pivots - WC User PO Approval Limit
--Inserting Report Distribution Details 
--Inserting Report Distribution Details 
--Inserting Report SubTotals Details 
--Inserting Report aggregations 
--Inserting Report   Version details- WC User PO Approval Limit
xxeis.eis_rsc_ins.rv( 'WC User PO Approval Limit','','WC User PO Approval Limit','SA059956','29-JUN-2017');
ELSE
RAISE_APPLICATION_ERROR (-20001,'Module does not exist. Import/Create the module with id 85000');
END IF;
END;
/
