/*
 -- Author: Balaguru Seshadri
 -- Parameters: None
 -- Modification History
 -- ESMS                TMS                          Date                  Version     Comments
 -- =========== ===============  ========          =======  ========================
 -- 457326          20160826-00209  31-AUG-2016   1.0            Created.
 */ 
SET SERVEROUTPUT ON SIZE 1000000
declare
  --
  b_proceed BOOLEAN;
  --
 procedure print_log(p_message in varchar2) is
 begin
     if  fnd_global.conc_request_id >0 then
      fnd_file.put_line(fnd_file.log, p_message);
     else
      dbms_output.put_line(p_message);
     end if;
 end print_log;
 --
 --
 procedure print_output(p_message in varchar2) is
 begin
     if  fnd_global.conc_request_id >0 then
      fnd_file.put_line(fnd_file.output, p_message);
     else
      dbms_output.put_line(p_message);
     end if;
 end print_output;
 --         
begin
  --
  print_log('Start: Main');
  --
  begin 
    --
	execute immediate 'create table xxcus.xxcus_ap_esms_457326 as select * from ap.ap_exp_report_dists_all where report_header_id = 869936 and org_id =162';
	--
    b_proceed :=TRUE;
	--
  exception
   when others then
    print_log('@Failed to create backup table, error msg ='||sqlerrm);
	--
    b_proceed :=FALSE;
	--
  end;   
  -- 
  if (b_proceed) then
      --
	  begin 
		 --
		savepoint square1;
		--
		delete ap.ap_exp_report_dists_all
		where 1 =1
		and report_header_id = 869936
		and org_id =162;
		--
		print_log('Deleted records from table ap_exp_report_dists_all with report_header_id = 869936 and org_id 162, row count ='||sql%rowcount);
		--
		commit;
		--      
	  exception
	   when others then
		print_log('@Failed to delete record with report_header_id = 869936 and org_id 162, msg ='||sqlerrm);
		rollback to square1;
	  end;   
	  --   
  end if;  
 print_log('End: Main');
 -- 
exception
 when others then
  print_log('Outer block of SQL script, message ='||sqlerrm);
end;
/