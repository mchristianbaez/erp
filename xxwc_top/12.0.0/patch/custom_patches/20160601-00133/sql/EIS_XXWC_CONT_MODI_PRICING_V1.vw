--------------------------------------------------------------------------------
/******************************************************************************
   NAME       :  XXEIS.EIS_XXWC_CONT_MODI_PRICING_V1
   PURPOSE    :  It is used to create the report 'CONTRACT PRICING REPORT - INTERNAL'
   REVISIONS:
   Ver        Date        Author           Description
   ---------  ----------  ---------------  ------------------------------------
   1.0		 04/20/2016   Pramod  			--TMS#20160426-00093  Performance Tuning                                         
******************************************************************************/
DROP VIEW XXEIS.EIS_XXWC_CONT_MODI_PRICING_V1;

CREATE OR REPLACE VIEW XXEIS.EIS_XXWC_CONT_MODI_PRICING_V1 (MODIFIER_NAME, CUSTOMER_NAME, QUALIFIER_ATTRIBUTE, PRODUCT, DESCRIPTION, CAT_CLASS, GM, LIST_PRICE, APP, VALUE1, CONTRACT_PRICE, UNITS_SOLD, ACTUAL_SALES, ACTUAL_GROSS_MARGIN, CURRENT_LOCATION_COST, NEW_PRICE_OR_NEW_GM_DISC, INVENTORY_ITEM_ID, LIST_HEADER_ID, PRODUCT_ATTRIBUTE_TYPE, CATEGORY_ID, CUST_ACCOUNT_ID,
 TYPE, PROCESS_ID, ARITHMETIC_OPERATOR, OPERAND, UNIT_COST, CAT, CAT_DESC, PRICE_BY_CAT, CONTRACT_TYPE, FORMULA_NAME, SALESREP, CAT_CLASS_DESC, TRANSACTION_CODE, CREATION_DATE, PERSON_ID, ACCOUNT_NUMBER, COMMON_OUTPUT_ID, LOCATION, PARTY_SITE_NUMBER, VERSION_NO, PRICE_TYPE) AS 
  SELECT varchar2_col1 modifier_name ,
    varchar2_col2 customer_name ,
    varchar2_col3 qualifier_attribute ,
    varchar2_col4 product ,
    varchar2_col5 description ,
    varchar2_col6 cat_class ,
    number_col1 gm ,
    number_col2 list_price ,
    varchar2_col7 app ,
    number_col3 value1 ,
    number_col4 contract_price ,
    number_col5 units_sold ,
    number_col6 actual_sales ,
    number_col7 actual_gross_margin ,
    number_col8 current_location_cost ,
    number_col9 new_price_or_new_gm_disc ,
    number_col10 inventory_item_id ,
    number_col11 list_header_id ,
    varchar2_col8 product_attribute_type ,
    number_col13 category_id ,
    number_col14 cust_account_id ,
    varchar2_col9 TYPE ,
    process_id process_id ,
    varchar2_col10 arithmetic_operator ,
    number_col15 operand ,
    number_col16 unit_cost ,
    varchar2_col11 cat ,
    varchar2_col12 cat_desc ,
    varchar2_col13 price_by_cat ,
    varchar2_col14 contract_type ,
    varchar2_col15 formula_name ,
    varchar2_col16 salesrep ,
    varchar2_col17 cat_class_desc ,
    varchar2_col18 transaction_code ,
    varchar2_col19 creation_date ,
    number_col17 person_id ,
    varchar2_col20 account_number ,
    common_output_id common_output_id ,
    varchar2_col21 location ,
    varchar2_col22 party_site_number ,
    varchar2_col23 version_no ,
    varchar2_col24 price_type
  FROM XXEIS.EIS_RS_COMMON_OUTPUTS
/
