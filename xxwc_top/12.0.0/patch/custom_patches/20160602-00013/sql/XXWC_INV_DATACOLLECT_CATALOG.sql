/************************************************************************************************************************************
  $Header XXWC_INV_DATACOLLECT_CATALOG.sql $
  Module Name: TMS20160602-00013 Kiewit Phase 2 -FTP Extract files to Kiewit.

  PURPOSE: To Create DB Directory.

  REVISIONS:
  Ver        Date         Author                Description
  ---------  -----------  ------------------    ---------------------------------------------------------------------------------
  1.0        19-sep-2016  P.Vamshidhar          TMS20160602-00013 Kiewit Phase 2 -FTP Extract files to Kiewit.

***********************************************************************************************************************************/ 

SET SERVEROUTPUT ON SIZE 100000;

SET VERIFY OFF;

DECLARE
   lvc_db_name   VARCHAR2 (100);
BEGIN
   SELECT LOWER(name) INTO lvc_db_name FROM v$database;

   EXECUTE IMMEDIATE
         'CREATE OR REPLACE DIRECTORY XXWC_INV_DATACOLLECT_CATALOG AS '
      || '''/xx_iface/'
      || lvc_db_name
      || '/outbound/pdh/b2b_cat/dc_catalog''';

   EXECUTE IMMEDIATE
      'GRANT EXECUTE, READ, WRITE ON DIRECTORY XXWC_INV_DATACOLLECT_CATALOG TO APPS WITH GRANT OPTION';
END;
/











