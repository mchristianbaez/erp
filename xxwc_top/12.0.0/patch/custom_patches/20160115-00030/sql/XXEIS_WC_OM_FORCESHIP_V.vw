CREATE VIEW XXEIS.XXEIS_WC_OM_FORCESHIP_V(Branch_Number,branch,Order_Number,Line_Number,Part_Number,Order_Quantity,       FORCE_SHIP_QUANTITY,Date_of_Force_Ship,Force_Ship_User)
 /*****************************************************************************************
  $Header XXEIS.XXEIS_WC_OM_FORCESHIP_V.vw $
  Module Name: Order Management
  PURPOSE: EIS Force Ship Report - WC
  TMS Task Id : 20160115-00030 
  REVISIONS:
  Ver        Date        Author                Description
  ---------  ----------  ------------------    ----------------
  1.0        01/20/2016 Mahender Reddy            Initial Version
 
******************************************************************************************/
 AS SELECT  ood.organization_code Branch_Number,
         ood.organization_name branch,
         oeh.order_number Order_Number,
         oel.line_number Line_Number,
         msib.segment1 Part_Number,
         oel.ordered_quantity Order_Quantity,
         SUM (OEL.ATTRIBUTE11) FORCE_SHIP_QUANTITY,
         TRUNC(oel.LAST_UPDATE_DATE)  Date_of_Force_Ship,
         CASE (fu.user_name) 
           WHEN 'XXWC_INT_SALESFULFILLMENT' THEN 'AUTOMATIC'
           ELSE fu.description
         END Force_Ship_User                 
    FROM apps.oe_order_headers_all oeh,
         apps.oe_order_lines_all oel,
         apps.mtl_system_items_b msib,
         apps.org_organization_definitions ood,
         apps.fnd_user fu,
         xxwc.xxwc_WSH_shipping_stg  xws
   WHERE     oeh.header_id = oel.header_id
         AND oel.flow_status_code != 'CANCELLED'
         AND OEL.ATTRIBUTE11 IS NOT NULL
         AND oel.LINE_TYPE_ID in  (1002)  
         AND OEH.ORDER_TYPE_ID IN (1001)
         AND oel.org_id = 162
         AND ood.organization_id = oeh.ship_from_org_id
         AND ood.organization_id = oel.ship_from_org_id
         AND ood.organization_id = msib.organization_id
         AND msib.inventory_item_id = oel.inventory_item_id
         AND oeh.org_id = oel.org_id
         AND oeh.ship_from_org_id = msib.organization_id
         AND oel.ship_from_org_id = msib.organization_id
         AND xws.header_id=oeh.header_id
         AND xws.line_id = oel.line_id
         AND XWS.LAST_UPDATED_BY= FU.USER_ID
        AND xws.creation_date in (select max(xws1.creation_date) 
                                     from xxwc.xxwc_WSH_shipping_stg xws1
                                    WHERE XWS1.LINE_ID = OEL.LINE_ID
                                     AND XWS1.HEADER_ID=OEH.HEADER_ID)
         --AND TRUNC(OEL.LAST_UPDATE_DATE) = TRUNC(SYSDATE-13)--Enter date for a specific day
         --AND ood.organization_code = '858'         
GROUP BY ood.organization_name,
         ood.organization_code,
         msib.segment1,
         oel.last_updated_by,
         oel.LAST_UPDATE_DATE,
         oel.ordered_quantity,
         oeh.order_number,
          fu.description,
          fu.user_name,
          oel.line_number
ORDER BY ood.organization_code,
         OEH.ORDER_NUMBER,
         OEL.LINE_NUMBER
/