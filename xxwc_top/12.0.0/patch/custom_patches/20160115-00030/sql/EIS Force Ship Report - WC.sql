--Report Name            : EIS Force Ship Report - WC
--Import Instructions    : Run this file in XXEIS user from SQL*Plus or any tool like Toad, SQL Navigator. 
prompt Creating View Data for EIS Force Ship Report - WC
set scan off define off
DECLARE
BEGIN 
--Inserting View XXEIS_WC_OM_FORCESHIP_V
xxeis.eis_rs_ins.v( 'XXEIS_WC_OM_FORCESHIP_V',660,'','','','','MR020532','XXEIS','Xxeis Wc Om Forceship V','XWOFV','','');
--Delete View Columns for XXEIS_WC_OM_FORCESHIP_V
xxeis.eis_rs_utility.delete_view_rows('XXEIS_WC_OM_FORCESHIP_V',660,FALSE);
--Inserting View Columns for XXEIS_WC_OM_FORCESHIP_V
xxeis.eis_rs_ins.vc( 'XXEIS_WC_OM_FORCESHIP_V','FORCE_SHIP_USER',660,'Force Ship User','FORCE_SHIP_USER','','','','MR020532','VARCHAR2','','','Force Ship User','','','');
xxeis.eis_rs_ins.vc( 'XXEIS_WC_OM_FORCESHIP_V','DATE_OF_FORCE_SHIP',660,'Date Of Force Ship','DATE_OF_FORCE_SHIP','','','','MR020532','DATE','','','Date Of Force Ship','','','');
xxeis.eis_rs_ins.vc( 'XXEIS_WC_OM_FORCESHIP_V','FORCE_SHIP_QUANTITY',660,'Force Ship Quantity','FORCE_SHIP_QUANTITY','','','','MR020532','NUMBER','','','Force Ship Quantity','','','');
xxeis.eis_rs_ins.vc( 'XXEIS_WC_OM_FORCESHIP_V','ORDER_QUANTITY',660,'Order Quantity','ORDER_QUANTITY','','','','MR020532','NUMBER','','','Order Quantity','','','');
xxeis.eis_rs_ins.vc( 'XXEIS_WC_OM_FORCESHIP_V','PART_NUMBER',660,'Part Number','PART_NUMBER','','','','MR020532','VARCHAR2','','','Part Number','','','');
xxeis.eis_rs_ins.vc( 'XXEIS_WC_OM_FORCESHIP_V','LINE_NUMBER',660,'Line Number','LINE_NUMBER','','','','MR020532','NUMBER','','','Line Number','','','');
xxeis.eis_rs_ins.vc( 'XXEIS_WC_OM_FORCESHIP_V','ORDER_NUMBER',660,'Order Number','ORDER_NUMBER','','','','MR020532','NUMBER','','','Order Number','','','');
xxeis.eis_rs_ins.vc( 'XXEIS_WC_OM_FORCESHIP_V','BRANCH',660,'Branch','BRANCH','','','','MR020532','VARCHAR2','','','Branch','','','');
xxeis.eis_rs_ins.vc( 'XXEIS_WC_OM_FORCESHIP_V','BRANCH_NUMBER',660,'Branch Number','BRANCH_NUMBER','','','','MR020532','VARCHAR2','','','Branch Number','','','');
--Inserting View Components for XXEIS_WC_OM_FORCESHIP_V
--Inserting View Component Joins for XXEIS_WC_OM_FORCESHIP_V
END;
/
set scan on define on
prompt Creating Report LOV Data for EIS Force Ship Report - WC
set scan off define off
DECLARE
BEGIN 
--Inserting Report LOVs - EIS Force Ship Report - WC
xxeis.eis_rs_ins.lov( 660,'select distinct organization_code org_code,organization_name org_name from org_organization_definitions','','Branch Lov','','XXEIS_RS_ADMIN',NULL,'N','','');
END;
/
set scan on define on
prompt Creating Report Data for EIS Force Ship Report - WC
set scan off define off
DECLARE
BEGIN 
--Deleting Report data - EIS Force Ship Report - WC
xxeis.eis_rs_utility.delete_report_rows( 'EIS Force Ship Report - WC' );
--Inserting Report - EIS Force Ship Report - WC
xxeis.eis_rs_ins.r( 660,'EIS Force Ship Report - WC','','EIS Force Ship Report - WC is within the Sales and Fulfillment workspace','','','','MR020532','XXEIS_WC_OM_FORCESHIP_V','Y','','','MR020532','','N','White Cap Reports','PDF,','CSV,Pivot Excel,EXCEL,','N');
--Inserting Report Columns - EIS Force Ship Report - WC
xxeis.eis_rs_ins.rc( 'EIS Force Ship Report - WC',660,'BRANCH','Branch','Branch','','','default','','2','N','','','','','','','','MR020532','N','N','','XXEIS_WC_OM_FORCESHIP_V','','');
xxeis.eis_rs_ins.rc( 'EIS Force Ship Report - WC',660,'BRANCH_NUMBER','Branch Number','Branch Number','','','default','','1','N','','','','','','','','MR020532','N','N','','XXEIS_WC_OM_FORCESHIP_V','','');
xxeis.eis_rs_ins.rc( 'EIS Force Ship Report - WC',660,'DATE_OF_FORCE_SHIP','Date Of Force Ship','Date Of Force Ship','','','default','','8','N','','','','','','','','MR020532','N','N','','XXEIS_WC_OM_FORCESHIP_V','','');
xxeis.eis_rs_ins.rc( 'EIS Force Ship Report - WC',660,'FORCE_SHIP_QUANTITY','Force Ship Quantity','Force Ship Quantity','','~~~','default','','7','N','','','','','','','','MR020532','N','N','','XXEIS_WC_OM_FORCESHIP_V','','');
xxeis.eis_rs_ins.rc( 'EIS Force Ship Report - WC',660,'FORCE_SHIP_USER','Force Ship User','Force Ship User','','','default','','9','N','','','','','','','','MR020532','N','N','','XXEIS_WC_OM_FORCESHIP_V','','');
xxeis.eis_rs_ins.rc( 'EIS Force Ship Report - WC',660,'LINE_NUMBER','Line Number','Line Number','','~~~','default','','4','N','','','','','','','','MR020532','N','N','','XXEIS_WC_OM_FORCESHIP_V','','');
xxeis.eis_rs_ins.rc( 'EIS Force Ship Report - WC',660,'ORDER_NUMBER','Order Number','Order Number','','~~~','default','','3','N','','','','','','','','MR020532','N','N','','XXEIS_WC_OM_FORCESHIP_V','','');
xxeis.eis_rs_ins.rc( 'EIS Force Ship Report - WC',660,'ORDER_QUANTITY','Order Quantity','Order Quantity','','~~~','default','','6','N','','','','','','','','MR020532','N','N','','XXEIS_WC_OM_FORCESHIP_V','','');
xxeis.eis_rs_ins.rc( 'EIS Force Ship Report - WC',660,'PART_NUMBER','Part Number','Part Number','','','default','','5','N','','','','','','','','MR020532','N','N','','XXEIS_WC_OM_FORCESHIP_V','','');
--Inserting Report Parameters - EIS Force Ship Report - WC
xxeis.eis_rs_ins.rp( 'EIS Force Ship Report - WC',660,'Branch','Branch','BRANCH_NUMBER','IN','Branch Lov','','VARCHAR2','Y','Y','1','','Y','CONSTANT','MR020532','Y','N','','','');
xxeis.eis_rs_ins.rp( 'EIS Force Ship Report - WC',660,'Date','Date','DATE_OF_FORCE_SHIP','IN','','','DATE','N','Y','2','','Y','CURRENT_DATE','MR020532','Y','N','','','');
--Inserting Report Conditions - EIS Force Ship Report - WC
xxeis.eis_rs_ins.rcn( 'EIS Force Ship Report - WC',660,'BRANCH_NUMBER','IN',':Branch','','','Y','1','Y','MR020532');
xxeis.eis_rs_ins.rcn( 'EIS Force Ship Report - WC',660,'DATE_OF_FORCE_SHIP','IN',':Date','','','Y','2','Y','MR020532');
--Inserting Report Sorts - EIS Force Ship Report - WC
--Inserting Report Triggers - EIS Force Ship Report - WC
--Inserting Report Templates - EIS Force Ship Report - WC
--Inserting Report Portals - EIS Force Ship Report - WC
--Inserting Report Dashboards - EIS Force Ship Report - WC
--Inserting Report Security - EIS Force Ship Report - WC
--Inserting Report Pivots - EIS Force Ship Report - WC
END;
/
set scan on define on
