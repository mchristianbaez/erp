--Report Name            : WCIT Payables Account Analysis Subledger Detail Report
--Import Instructions    : Run this file in XXEIS user from SQL*Plus or any tool like Toad, SQL Navigator. 
prompt Creating Report LOV Data for WCIT Payables Account Analysis Subledger Detail Report
set scan off define off
DECLARE
mod_exist VARCHAR2(1);
BEGIN 
mod_exist := XXEIS.EIS_RSC_INSERT.check_if_module_exist(101);
IF mod_exist = 'Y' THEN 
--Inserting Report LOVs - WCIT Payables Account Analysis Subledger Detail Report
xxeis.eis_rsc_ins.lov( 101,'SELECT distinct user_je_source_name FROM gl_je_sources','','EIS_GL_JE_SOURCES_LOV','LOV of all Available Journal Sources','XXEIS_RS_ADMIN',NULL,'','','','','','','','US');
xxeis.eis_rsc_ins.lov( '','SELECT ffv.flex_value, ffvtl.description, decode(ffv.summary_flag,''Y'', ''Parent'',''N'', ''Child'') Type 
				FROM 
					fnd_flex_value_sets ffvs , 
					fnd_flex_values ffv, 
					fnd_flex_values_tl ffvtl 
				WHERE   upper(ffvs.flex_value_set_name) = upper(''XXCUS_GL_PRODUCT'') 
				 and ffv.flex_value_set_id = ffvs.flex_value_set_id 
				 and ffv.FLEX_VALUE_ID = ffvtl.FLEX_VALUE_ID 
				AND ffv.enabled_flag = upper(''Y'') 
				AND ffv.summary_flag in (''Y'',''N'') 
				/* AND ffv.summary_flag <> upper(''Y'') */ 
				AND ffvtl.LANGUAGE = USERENV(''LANG'') 
				 AND xxeis.eis_gl_security_pkg.validate_segment_value( ''SEGMENT1'' , ffv.flex_value)=''TRUE'' 
				order by ffv.flex_value	','','XXCUS_GL_PRODUCT','XXCUS_GL_PRODUCT','MM050208',NULL,'','','','','','','','US');
xxeis.eis_rsc_ins.lov( '','SELECT ffv.flex_value, ffvtl.description, decode(ffv.summary_flag,''Y'', ''Parent'',''N'', ''Child'') Type 
				FROM 
					fnd_flex_value_sets ffvs , 
					fnd_flex_values ffv, 
					fnd_flex_values_tl ffvtl 
				WHERE   upper(ffvs.flex_value_set_name) = upper(''XXCUS_GL_LOCATION'') 
				 and ffv.flex_value_set_id = ffvs.flex_value_set_id 
				 and ffv.FLEX_VALUE_ID = ffvtl.FLEX_VALUE_ID 
				AND ffv.enabled_flag = upper(''Y'') 
				AND ffv.summary_flag in (''Y'',''N'') 
				/* AND ffv.summary_flag <> upper(''Y'') */ 
				AND ffvtl.LANGUAGE = USERENV(''LANG'') 
				 AND xxeis.eis_gl_security_pkg.validate_segment_value( ''SEGMENT2'' , ffv.flex_value)=''TRUE'' 
				order by ffv.flex_value	','','XXCUS_GL_LOCATION','XXCUS_GL_LOCATION','MM050208',NULL,'','','','','','','','US');
xxeis.eis_rsc_ins.lov( '','SELECT ffv.flex_value, ffvtl.description, decode(ffv.summary_flag,''Y'', ''Parent'',''N'', ''Child'') Type 
				FROM 
					fnd_flex_value_sets ffvs , 
					fnd_flex_values ffv, 
					fnd_flex_values_tl ffvtl 
				WHERE   upper(ffvs.flex_value_set_name) = upper(''XXCUS_GL_COSTCENTER'') 
				 and ffv.flex_value_set_id = ffvs.flex_value_set_id 
				 and ffv.FLEX_VALUE_ID = ffvtl.FLEX_VALUE_ID 
				AND ffv.enabled_flag = upper(''Y'') 
				AND ffv.summary_flag in (''Y'',''N'') 
				/* AND ffv.summary_flag <> upper(''Y'') */ 
				AND ffvtl.LANGUAGE = USERENV(''LANG'') 
				 AND xxeis.eis_gl_security_pkg.validate_segment_value( ''SEGMENT3'' , ffv.flex_value)=''TRUE'' 
				order by ffv.flex_value	','','XXCUS_GL_COSTCENTER','XXCUS_GL_COSTCENTER','MM050208',NULL,'','','','','','','','US');
xxeis.eis_rsc_ins.lov( '','SELECT ffv.flex_value, ffvtl.description, decode(ffv.summary_flag,''Y'', ''Parent'',''N'', ''Child'') Type 
				FROM 
					fnd_flex_value_sets ffvs , 
					fnd_flex_values ffv, 
					fnd_flex_values_tl ffvtl 
				WHERE   upper(ffvs.flex_value_set_name) = upper(''XXCUS_GL_ACCOUNT'') 
				 and ffv.flex_value_set_id = ffvs.flex_value_set_id 
				 and ffv.FLEX_VALUE_ID = ffvtl.FLEX_VALUE_ID 
				AND ffv.enabled_flag = upper(''Y'') 
				AND ffv.summary_flag in (''Y'',''N'') 
				/* AND ffv.summary_flag <> upper(''Y'') */ 
				AND ffvtl.LANGUAGE = USERENV(''LANG'') 
				 AND xxeis.eis_gl_security_pkg.validate_segment_value( ''SEGMENT4'' , ffv.flex_value)=''TRUE'' 
				order by ffv.flex_value	','','XXCUS_GL_ACCOUNT','XXCUS_GL_ACCOUNT','MM050208',NULL,'','','','','','','','US');
xxeis.eis_rsc_ins.lov( '','SELECT ffv.flex_value, ffvtl.description, decode(ffv.summary_flag,''Y'', ''Parent'',''N'', ''Child'') Type 
				FROM 
					fnd_flex_value_sets ffvs , 
					fnd_flex_values ffv, 
					fnd_flex_values_tl ffvtl 
				WHERE   upper(ffvs.flex_value_set_name) = upper(''XXCUS_GL_PROJECT'') 
				 and ffv.flex_value_set_id = ffvs.flex_value_set_id 
				 and ffv.FLEX_VALUE_ID = ffvtl.FLEX_VALUE_ID 
				AND ffv.enabled_flag = upper(''Y'') 
				AND ffv.summary_flag in (''Y'',''N'') 
				/* AND ffv.summary_flag <> upper(''Y'') */ 
				AND ffvtl.LANGUAGE = USERENV(''LANG'') 
				 AND xxeis.eis_gl_security_pkg.validate_segment_value( ''SEGMENT5'' , ffv.flex_value)=''TRUE'' 
				order by ffv.flex_value	','','XXCUS_GL_PROJECT','XXCUS_GL_PROJECT','MM050208',NULL,'','','','','','','','US');
xxeis.eis_rsc_ins.lov( 101,'select distinct period_name
from gl_je_headers','','HDS_GL_PERIOD_NAME','GL Period Name','XXEIS_RS_ADMIN',NULL,'N','','','','','','','US');
null;
ELSE
RAISE_APPLICATION_ERROR (-20001,'Module does not exist. Import/Create the module with id 101');
END IF;
END;
/
prompt Creating Report Data for WCIT Payables Account Analysis Subledger Detail Report
set scan off define off
DECLARE
mod_exist varchar2(1);
BEGIN 
mod_exist := XXEIS.EIS_RSC_INSERT.check_if_module_exist(101);
IF mod_exist = 'Y' THEN 
--Deleting Report data - WCIT Payables Account Analysis Subledger Detail Report
xxeis.eis_rsc_utility.delete_report_rows( 'WCIT Payables Account Analysis Subledger Detail Report',101 );
--Inserting Report - WCIT Payables Account Analysis Subledger Detail Report
xxeis.eis_rsc_ins.r( 101,'WCIT Payables Account Analysis Subledger Detail Report','','This report will provide detail from the subledger for all entries posted for the account in general ledger.
Created from ESMS#281615','','','','JP016147','XXHDS_EIS_GL_SL_180_V','Y','','','JP016147','','N','White Cap Reports','','CSV,Pivot Excel,EXCEL,','N','','','','','','N','','US','','HDS Account Analysis Subledger Detail Report','','','','','','<SID> : <REPORT_NAME> with Process ID - <PROCESS_ID> completed','<B><REPORT_NAME> </B>with Process ID - <PROCESS_ID> completed at <END_TIME> <BR><BR> This request was submited by <B><USER_NAME></B>. <URL_WITH_MESSAGE> <BR><BR> <ATTACHMENT_WITH_MESSAGE>','','','URL,DashbLink,CSV,EXCEL,Pivot Excel,','','','','');
--Inserting Report Columns - WCIT Payables Account Analysis Subledger Detail Report
xxeis.eis_rsc_ins.rc( 'WCIT Payables Account Analysis Subledger Detail Report',101,'CURRENCY_CODE','Currency Code','Currency Code','','','default','','15','N','Y','','','','','','','JP016147','N','N','','XXHDS_EIS_GL_SL_180_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'WCIT Payables Account Analysis Subledger Detail Report',101,'CUSTOMER_OR_VENDOR','Customer Or Vendor','Customer Or Vendor','','','default','','11','N','Y','','','','','','','JP016147','N','N','','XXHDS_EIS_GL_SL_180_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'WCIT Payables Account Analysis Subledger Detail Report',101,'EFFECTIVE_PERIOD_NUM','Effective Period Num','Effective Period Num','','~~~','default','','16','N','Y','','','','','','','JP016147','N','N','','XXHDS_EIS_GL_SL_180_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'WCIT Payables Account Analysis Subledger Detail Report',101,'ENTERED_CR','Entered Cr','Entered Cr','','~T~D~2','default','','18','N','Y','','','','','','','JP016147','N','N','','XXHDS_EIS_GL_SL_180_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'WCIT Payables Account Analysis Subledger Detail Report',101,'ENTERED_DR','Entered Dr','Entered Dr','','~T~D~2','default','','17','N','Y','','','','','','','JP016147','N','N','','XXHDS_EIS_GL_SL_180_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'WCIT Payables Account Analysis Subledger Detail Report',101,'ENTRY','Entry','Entry','','','default','','19','N','Y','','','','','','','JP016147','N','N','','XXHDS_EIS_GL_SL_180_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'WCIT Payables Account Analysis Subledger Detail Report',101,'GL_ACCOUNT_STRING','Gl Account String','Gl Account String','','','default','','2','N','Y','','','','','','','JP016147','N','N','','XXHDS_EIS_GL_SL_180_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'WCIT Payables Account Analysis Subledger Detail Report',101,'HNUMBER','Hnumber','Hnumber','','~~~','default','','20','N','Y','','','','','','','JP016147','N','N','','XXHDS_EIS_GL_SL_180_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'WCIT Payables Account Analysis Subledger Detail Report',101,'JE_CATEGORY','Je Category','Je Category','','','default','','4','N','Y','','','','','','','JP016147','N','N','','XXHDS_EIS_GL_SL_180_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'WCIT Payables Account Analysis Subledger Detail Report',101,'JE_LINE_NUM','Je Line Num','Je Line Num','','~~~','default','','21','N','Y','','','','','','','JP016147','N','N','','XXHDS_EIS_GL_SL_180_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'WCIT Payables Account Analysis Subledger Detail Report',101,'LINE_ACCTD_CR','Line Acctd Cr','Line Acctd Cr','','~T~D~2','default','','23','N','Y','','','','','','','JP016147','N','N','','XXHDS_EIS_GL_SL_180_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'WCIT Payables Account Analysis Subledger Detail Report',101,'LINE_ACCTD_DR','Line Acctd Dr','Line Acctd Dr','','~T~D~2','default','','22','N','Y','','','','','','','JP016147','N','N','','XXHDS_EIS_GL_SL_180_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'WCIT Payables Account Analysis Subledger Detail Report',101,'LINE_DESCR','Line Descr','Line Descr','','','default','','5','N','Y','','','','','','','JP016147','N','N','','XXHDS_EIS_GL_SL_180_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'WCIT Payables Account Analysis Subledger Detail Report',101,'LINE_ENT_CR','Line Ent Cr','Line Ent Cr','','~T~D~2','default','','25','N','Y','','','','','','','JP016147','N','N','','XXHDS_EIS_GL_SL_180_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'WCIT Payables Account Analysis Subledger Detail Report',101,'LINE_ENT_DR','Line Ent Dr','Line Ent Dr','','~T~D~2','default','','24','N','Y','','','','','','','JP016147','N','N','','XXHDS_EIS_GL_SL_180_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'WCIT Payables Account Analysis Subledger Detail Report',101,'NAME','Name','Name','','','default','','26','N','Y','','','','','','','JP016147','N','N','','XXHDS_EIS_GL_SL_180_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'WCIT Payables Account Analysis Subledger Detail Report',101,'PERIOD_NAME','Period Name','Period Name','','','default','','1','N','Y','','','','','','','JP016147','N','N','','XXHDS_EIS_GL_SL_180_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'WCIT Payables Account Analysis Subledger Detail Report',101,'PO_NUMBER','Po Number','Po Number','','','default','','8','N','Y','','','','','','','JP016147','N','N','','XXHDS_EIS_GL_SL_180_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'WCIT Payables Account Analysis Subledger Detail Report',101,'SLA_DIST_ACCOUNTED_CR','Sla Dist Accounted Cr','Sla Dist Accounted Cr','','~T~D~2','default','','28','N','Y','','','','','','','JP016147','N','N','','XXHDS_EIS_GL_SL_180_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'WCIT Payables Account Analysis Subledger Detail Report',101,'SLA_DIST_ACCOUNTED_DR','Sla Dist Accounted Dr','Sla Dist Accounted Dr','','~T~D~2','default','','27','N','Y','','','','','','','JP016147','N','N','','XXHDS_EIS_GL_SL_180_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'WCIT Payables Account Analysis Subledger Detail Report',101,'SLA_DIST_ACCOUNTED_NET','Sla Dist Accounted Net','Sla Dist Accounted Net','','~T~D~2','default','','29','N','Y','','','','','','','JP016147','N','N','','XXHDS_EIS_GL_SL_180_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'WCIT Payables Account Analysis Subledger Detail Report',101,'SOURCE','Source','Source','','','default','','7','N','Y','','','','','','','JP016147','N','N','','XXHDS_EIS_GL_SL_180_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'WCIT Payables Account Analysis Subledger Detail Report',101,'TRANSACTION_NUM','Transaction Num','Transaction Num','','','default','','9','N','Y','','','','','','','JP016147','N','N','','XXHDS_EIS_GL_SL_180_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'WCIT Payables Account Analysis Subledger Detail Report',101,'TYPE','Type','Type','','','default','','10','N','Y','','','','','','','JP016147','N','N','','XXHDS_EIS_GL_SL_180_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'WCIT Payables Account Analysis Subledger Detail Report',101,'GCC50328ACCOUNT','Gcc50328account','Gcc50328account','','','default','','32','N','Y','','','','','','','JP016147','N','N','','XXHDS_EIS_GL_SL_180_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'WCIT Payables Account Analysis Subledger Detail Report',101,'SLA_DIST_ENT_NET','Sla Dist Ent Net','','NUMBER','~T~D~2','default','','30','Y','Y','','','','','','XEGS1V.SLA_DIST_ENTERED_NET*-1','JP016147','N','N','','XXHDS_EIS_GL_SL_180_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'WCIT Payables Account Analysis Subledger Detail Report',101,'SLA_EVENT_TYPE','Sla Event Type','Sla Event Type','','','default','','31','N','Y','','','','','','','JP016147','N','N','','XXHDS_EIS_GL_SL_180_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'WCIT Payables Account Analysis Subledger Detail Report',101,'ACCOUNTED_CR','Accounted Cr','Accounted Cr','','~T~D~2','default','','13','N','Y','','','','','','','JP016147','N','N','','XXHDS_EIS_GL_SL_180_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'WCIT Payables Account Analysis Subledger Detail Report',101,'ACCOUNTED_DR','Accounted Dr','Accounted Dr','','~T~D~2','default','','12','N','Y','','','','','','','JP016147','N','N','','XXHDS_EIS_GL_SL_180_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'WCIT Payables Account Analysis Subledger Detail Report',101,'ACC_DATE','Acc Date','Acc Date','','','default','','6','N','Y','','','','','','','JP016147','N','N','','XXHDS_EIS_GL_SL_180_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'WCIT Payables Account Analysis Subledger Detail Report',101,'BATCH_NAME','Batch Name','Batch Name','','','default','','3','N','Y','','','','','','','JP016147','N','N','','XXHDS_EIS_GL_SL_180_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'WCIT Payables Account Analysis Subledger Detail Report',101,'GCC50328ACCOUNTDESCR','Gcc50328accountdescr','Gcc50328accountdescr','','','default','','33','N','Y','','','','','','','JP016147','N','N','','XXHDS_EIS_GL_SL_180_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'WCIT Payables Account Analysis Subledger Detail Report',101,'GCC50328LOCATION','Gcc50328location','Gcc50328location','','','default','','34','N','Y','','','','','','','JP016147','N','N','','XXHDS_EIS_GL_SL_180_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'WCIT Payables Account Analysis Subledger Detail Report',101,'GCC50328LOCATIONDESCR','Gcc50328locationdescr','Gcc50328locationdescr','','','default','','35','N','Y','','','','','','','JP016147','N','N','','XXHDS_EIS_GL_SL_180_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'WCIT Payables Account Analysis Subledger Detail Report',101,'GCC50328PRODUCT','Gcc50328product','Gcc50328product','','','default','','36','N','Y','','','','','','','JP016147','N','N','','XXHDS_EIS_GL_SL_180_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'WCIT Payables Account Analysis Subledger Detail Report',101,'GCC50328PROJECT_CODE','Gcc50328project Code','Gcc50328project Code','','','default','','37','N','Y','','','','','','','JP016147','N','N','','XXHDS_EIS_GL_SL_180_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'WCIT Payables Account Analysis Subledger Detail Report',101,'GL_SL_LINK_ID','Gl Sl Link Id','Gl Sl Link Id','','~~~','default','','38','N','Y','','','','','','','JP016147','N','N','','XXHDS_EIS_GL_SL_180_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'WCIT Payables Account Analysis Subledger Detail Report',101,'AP_INV_DATE','Ap Inv Date','Ap Inv Date','','','default','','39','N','Y','','','','','','','JP016147','N','N','','XXHDS_EIS_GL_SL_180_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'WCIT Payables Account Analysis Subledger Detail Report',101,'BOL_NUMBER','Bol Number','Bol Number','','','default','','40','N','Y','','','','','','','JP016147','N','N','','XXHDS_EIS_GL_SL_180_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'WCIT Payables Account Analysis Subledger Detail Report',101,'CUSTOMER_OR_VENDOR_NUMBER','Customer Or Vendor Number','Customer Or Vendor Number','','','default','','41','N','Y','','','','','','','JP016147','N','N','','XXHDS_EIS_GL_SL_180_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'WCIT Payables Account Analysis Subledger Detail Report',101,'XXCUS_IMAGE_LINK','Xxcus Image Link','Xxcus Image Link','','','default','','42','N','Y','','','','','','','JP016147','N','N','','XXHDS_EIS_GL_SL_180_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'WCIT Payables Account Analysis Subledger Detail Report',101,'XXCUS_LINE_DESC','Xxcus Line Desc','Xxcus Line Desc','','','default','','43','N','Y','','','','','','','JP016147','N','N','','XXHDS_EIS_GL_SL_180_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'WCIT Payables Account Analysis Subledger Detail Report',101,'XXCUS_PO_CREATED_BY','Xxcus Po Created By','Xxcus Po Created By','','','default','','44','N','Y','','','','','','','JP016147','N','N','','XXHDS_EIS_GL_SL_180_V','','','','US','','');
--Inserting Report Parameters - WCIT Payables Account Analysis Subledger Detail Report
xxeis.eis_rsc_ins.rp( 'WCIT Payables Account Analysis Subledger Detail Report',101,'Period Name','Period Name','PERIOD_NAME','IN','HDS_GL_PERIOD_NAME','','VARCHAR2','Y','Y','1','Y','Y','CONSTANT','JP016147','Y','N','','','','XXHDS_EIS_GL_SL_180_V','','','US','');
xxeis.eis_rsc_ins.rp( 'WCIT Payables Account Analysis Subledger Detail Report',101,'Product','Gcc50328product','GCC50328PRODUCT','IN','XXCUS_GL_PRODUCT','','VARCHAR2','Y','Y','2','Y','Y','CONSTANT','JP016147','Y','N','','','','XXHDS_EIS_GL_SL_180_V','','','US','');
xxeis.eis_rsc_ins.rp( 'WCIT Payables Account Analysis Subledger Detail Report',101,'Location','Gcc50328location','GCC50328LOCATION','IN','XXCUS_GL_LOCATION','','VARCHAR2','N','Y','3','Y','Y','CONSTANT','JP016147','Y','N','','','','XXHDS_EIS_GL_SL_180_V','','','US','');
xxeis.eis_rsc_ins.rp( 'WCIT Payables Account Analysis Subledger Detail Report',101,'Account','Gcc50328account','GCC50328ACCOUNT','IN','XXCUS_GL_ACCOUNT','','VARCHAR2','Y','Y','4','Y','Y','CONSTANT','JP016147','Y','N','','','','XXHDS_EIS_GL_SL_180_V','','','US','');
xxeis.eis_rsc_ins.rp( 'WCIT Payables Account Analysis Subledger Detail Report',101,'Cost Center','Gcc50328cost Center','GCC50328COST_CENTER','IN','XXCUS_GL_COSTCENTER','','VARCHAR2','N','Y','6','Y','Y','CONSTANT','JP016147','Y','N','','','','XXHDS_EIS_GL_SL_180_V','','','US','');
xxeis.eis_rsc_ins.rp( 'WCIT Payables Account Analysis Subledger Detail Report',101,'Project Code','Gcc50328project Code','GCC50328PROJECT_CODE','IN','XXCUS_GL_PROJECT','','VARCHAR2','N','Y','7','Y','Y','CONSTANT','JP016147','Y','N','','','','XXHDS_EIS_GL_SL_180_V','','','US','');
xxeis.eis_rsc_ins.rp( 'WCIT Payables Account Analysis Subledger Detail Report',101,'Source','Source','SOURCE','IN','EIS_GL_JE_SOURCES_LOV','','VARCHAR2','N','Y','8','Y','Y','CONSTANT','JP016147','Y','N','','','','XXHDS_EIS_GL_SL_180_V','','','US','');
--Inserting Dependent Parameters - WCIT Payables Account Analysis Subledger Detail Report
--Inserting Report Conditions - WCIT Payables Account Analysis Subledger Detail Report
xxeis.eis_rsc_ins.rcnh( 'WCIT Payables Account Analysis Subledger Detail Report',101,'GCC50328ACCOUNT >= :Account Low ','SIMPLE','','','Y','','2');
xxeis.eis_rsc_ins.rcnd( '','','GCC50328ACCOUNT','','Account','','','','','XXHDS_EIS_GL_SL_180_V','','','','','','IN','Y','Y','','','','','1',101,'WCIT Payables Account Analysis Subledger Detail Report','GCC50328ACCOUNT >= :Account Low ');
xxeis.eis_rsc_ins.rcnh( 'WCIT Payables Account Analysis Subledger Detail Report',101,'GCC50328COST_CENTER IN :Cost Center ','SIMPLE','','','Y','','3');
xxeis.eis_rsc_ins.rcnd( '','','GCC50328COST_CENTER','','Cost Center','','','','','XXHDS_EIS_GL_SL_180_V','','','','','','IN','Y','Y','','','','','1',101,'WCIT Payables Account Analysis Subledger Detail Report','GCC50328COST_CENTER IN :Cost Center ');
xxeis.eis_rsc_ins.rcnh( 'WCIT Payables Account Analysis Subledger Detail Report',101,'GCC50328LOCATION IN :Location ','SIMPLE','','','Y','','4');
xxeis.eis_rsc_ins.rcnd( '','','GCC50328LOCATION','','Location','','','','','XXHDS_EIS_GL_SL_180_V','','','','','','IN','Y','Y','','','','','1',101,'WCIT Payables Account Analysis Subledger Detail Report','GCC50328LOCATION IN :Location ');
xxeis.eis_rsc_ins.rcnh( 'WCIT Payables Account Analysis Subledger Detail Report',101,'GCC50328PRODUCT IN :Product ','SIMPLE','','','Y','','5');
xxeis.eis_rsc_ins.rcnd( '','','GCC50328PRODUCT','','Product','','','','','XXHDS_EIS_GL_SL_180_V','','','','','','IN','Y','Y','','','','','1',101,'WCIT Payables Account Analysis Subledger Detail Report','GCC50328PRODUCT IN :Product ');
xxeis.eis_rsc_ins.rcnh( 'WCIT Payables Account Analysis Subledger Detail Report',101,'GCC50328PROJECT_CODE IN :Project Code ','SIMPLE','','','Y','','6');
xxeis.eis_rsc_ins.rcnd( '','','GCC50328PROJECT_CODE','','Project Code','','','','','XXHDS_EIS_GL_SL_180_V','','','','','','IN','Y','Y','','','','','1',101,'WCIT Payables Account Analysis Subledger Detail Report','GCC50328PROJECT_CODE IN :Project Code ');
xxeis.eis_rsc_ins.rcnh( 'WCIT Payables Account Analysis Subledger Detail Report',101,'PERIOD_NAME IN :Period Name ','SIMPLE','','','Y','','7');
xxeis.eis_rsc_ins.rcnd( '','','PERIOD_NAME','','Period Name','','','','','XXHDS_EIS_GL_SL_180_V','','','','','','IN','Y','Y','','','','','1',101,'WCIT Payables Account Analysis Subledger Detail Report','PERIOD_NAME IN :Period Name ');
xxeis.eis_rsc_ins.rcnh( 'WCIT Payables Account Analysis Subledger Detail Report',101,'SOURCE IN :Source ','SIMPLE','','','Y','','8');
xxeis.eis_rsc_ins.rcnd( '','','SOURCE','','Source','','','','','XXHDS_EIS_GL_SL_180_V','','','','','','IN','Y','Y','','','','','1',101,'WCIT Payables Account Analysis Subledger Detail Report','SOURCE IN :Source ');
--Inserting Report Sorts - WCIT Payables Account Analysis Subledger Detail Report
--Inserting Report Triggers - WCIT Payables Account Analysis Subledger Detail Report
--inserting report templates - WCIT Payables Account Analysis Subledger Detail Report
--Inserting Report Portals - WCIT Payables Account Analysis Subledger Detail Report
--inserting report dashboards - WCIT Payables Account Analysis Subledger Detail Report
--Exporting report views table information -  
xxeis.eis_rsc_ins.rviews( 'WCIT Payables Account Analysis Subledger Detail Report','101','XXHDS_EIS_GL_SL_180_V','XXHDS_EIS_GL_SL_180_V','N','');
--inserting report security - WCIT Payables Account Analysis Subledger Detail Report
xxeis.eis_rsc_ins.rsec( 'WCIT Payables Account Analysis Subledger Detail Report','','SA059956','',101,'JP016147','','N','');
--Inserting Report Pivots - WCIT Payables Account Analysis Subledger Detail Report
xxeis.eis_rsc_ins.rpivot( 'WCIT Payables Account Analysis Subledger Detail Report',101,'WC IT','1','0,0|0,2,0','1,1,0,0|None|1');
--Inserting Report Pivot Details For Pivot - WC IT
xxeis.eis_rsc_ins.rpivot_dtls( 'WCIT Payables Account Analysis Subledger Detail Report',101,'WC IT','GCC50328ACCOUNT','PAGE_FIELD','','','1','','','');
xxeis.eis_rsc_ins.rpivot_dtls( 'WCIT Payables Account Analysis Subledger Detail Report',101,'WC IT','SLA_DIST_ACCOUNTED_NET','DATA_FIELD','SUM','Sla Dist Accounted Net','1','','xlNormal','');
xxeis.eis_rsc_ins.rpivot_dtls( 'WCIT Payables Account Analysis Subledger Detail Report',101,'WC IT','PO_NUMBER','ROW_FIELD','','','1','1|1','','');
xxeis.eis_rsc_ins.rpivot_dtls( 'WCIT Payables Account Analysis Subledger Detail Report',101,'WC IT','SLA_EVENT_TYPE','PAGE_FIELD','','','3','','','');
xxeis.eis_rsc_ins.rpivot_dtls( 'WCIT Payables Account Analysis Subledger Detail Report',101,'WC IT','PERIOD_NAME','PAGE_FIELD','','','2','','','');
xxeis.eis_rsc_ins.rpivot_dtls( 'WCIT Payables Account Analysis Subledger Detail Report',101,'WC IT','TRANSACTION_NUM','ROW_FIELD','','','2','1|1','','');
--Inserting Report Summary Calculation Columns For Pivot- WC IT
--Inserting Report Distribution Details 
--Inserting Report Distribution Details 
--Inserting Report SubTotals Details 
--Inserting Report aggregations 
--Inserting Report   Version details- WCIT Payables Account Analysis Subledger Detail Report
xxeis.eis_rsc_ins.rv( 'WCIT Payables Account Analysis Subledger Detail Report','','WCIT Payables Account Analysis Subledger Detail Report','SA059956','30-AUG-2018');
ELSE
RAISE_APPLICATION_ERROR (-20001,'Module does not exist. Import/Create the module with id 101');
END IF;
END;
/
