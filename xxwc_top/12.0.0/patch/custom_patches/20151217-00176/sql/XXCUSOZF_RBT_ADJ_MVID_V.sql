   /* ************************************************************************
    HD Supply
    All rights reserved.
    Ticket                                         Ver          Date                 Author                                 Description
    ----------------------                  ----------   ----------          ------------------------           -------------------------
    ESMS 257200                         1.0                                                                                   Missing header 
    TMS 20151008-00085        1.1         11/19/2015   Balaguru Seshadri            EDIT SQL query to use a custom table instead of hz tables
   ************************************************************************ */ 
CREATE OR REPLACE FORCE VIEW APPS.XXCUSOZF_RBT_ADJ_MVID_V
(
   MVID_NAME,
   MVID,
   CUST_ACCOUNT_ID
)
AS
-- Begin  Ver 1.1
select party_name mvid_name,
          customer_number mvid,
          customer_id cust_account_id
from xxcus.xxcus_rebate_customers
where 1 = 1
and customer_attribute1 ='HDS_MVID';
-- End  Ver 1.1
/*  -- Ver 1.1
   SELECT hzp.party_name mvid_name,
          hzca.account_number mvid,
          hzca.cust_account_id cust_account_id
     FROM hz_parties hzp, hz_cust_accounts hzca
    WHERE     hzp.category_code = 'MASTER_VENDOR'
          AND hzca.party_id = hzp.party_id;

COMMENT ON TABLE APPS.XXCUSOZF_RBT_ADJ_MVID_V IS 'ESMS 248697 / RFC 40565';
*/ -- Ver 1.1
--
COMMENT ON TABLE APPS.XXCUSOZF_RBT_ADJ_MVID_V IS  'ESMS 301618 / TMS 20151008-00085';