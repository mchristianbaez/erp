CREATE OR REPLACE PACKAGE XXWC_ISR_DATAMART_PKG
/************************************************************************************************************************
    $Header XXWC_ISR_DATAMART_PKG.pks $
    Module Name: XXWC_ISR_DATAMART_PKG
    PURPOSE: Package to load Inventory and Sales Reorder(ISR) Data
    TMS Task Id :  20141001-00063 - EISR Rearchitecure
                   20150317-00111 - EISR Enhancements for Purchasing
    REVISIONS:
    Ver    Date         Author                Description
    ------ -----------  ------------------    ----------------------------------------------------------------------------
    1.0    16-Oct-14    Manjula Chellappan    Initial Version
    2.0    24-Sep-15    Manjula Chellappan    TMS# 20150910-00122 - Inventory Sales and Reorder Report - WC is currently
                                                                     includes direct order quantities for 1,6 and 12

    8.0    04-Feb-16    P.Vamshidhar          TMS#20160204-00054   - New ISR Datamart Performance Tuning
                                              Added function FLIPDATE_FUNC to derive Flip_date.
    11.0   22-Apr-16    Srinivas              TMS# 20160307-00147   -  Added new procedure 'report_isr_live'to get live data or are affected by cloning or same day uploads and
                                              This procedure calling from before report trigger of 'Item Attribute Validation Report - WC'
    18.0   21-Mar-18    Naveen Kalidindi      TMS#20171006-00082 - ISR vendor logic update for dummy vendor 55555. Added 
                                              Flexibility to add more dummy vendors. Tag: <18.0>
**************************************************************************************************************************/
 AS

  /**********************************************************************************************************************
  
     0. Procedure to Write log messages to the concurrent program log and log table
  
  *********************************************************************************************************************/

  PROCEDURE write_log(p_log_msg VARCHAR2);

  /*************************************************************************
  
     1. Procedure to Load Operating Unit list for ISR Datamart Refresh
  
  **************************************************************************/

  PROCEDURE load_ou(p_retcode OUT VARCHAR2);

  /*************************************************************************
  
     2. Procedure to Load inventory Organizations for ISR Datamart Refresh
  
  **************************************************************************/

  PROCEDURE load_inv_org(p_retcode OUT VARCHAR2);

  /*************************************************************************
  
     3. Procedure to Load item details for ISR Datamart Refresh
  
  **************************************************************************/

  PROCEDURE load_items(p_retcode OUT VARCHAR2);

  /*************************************************************************
  
     4. Procedure to Load item Category details for ISR Datamart Refresh
  
  **************************************************************************/

  PROCEDURE load_category(p_retcode OUT VARCHAR2);

  /*************************************************************************
  
    5. Procedure to Load item Source details for ISR Datamart Refresh
  
  **************************************************************************/

  PROCEDURE load_item_source(p_retcode OUT VARCHAR2);

  /*************************************************************************
  
     6. Procedure to Load item Sales Hit for ISR Datamart Refresh
  
  **************************************************************************/

  PROCEDURE load_sales_hit(p_retcode OUT VARCHAR2);

  /*************************************************************************
  
     7. Procedure to Load Other inv Org's item Sales Hit for ISR Datamart Refresh
  
  **************************************************************************/

  PROCEDURE load_other_sales_hit(p_retcode OUT VARCHAR2);

  /*************************************************************************
  
     8. Procedure to Load Average Cost for ISR Datamart Refresh
  
  **************************************************************************/

  PROCEDURE load_average_cost(p_retcode OUT VARCHAR2);

  /*************************************************************************
  
     9. Procedure to Load Blanket Purcahse Agreement Cost for ISR Datamart Refresh
  
  **************************************************************************/

  PROCEDURE load_bpa_cost(p_retcode OUT VARCHAR2);

  /*************************************************************************
  
      10. Procedure to Load Primary Bin Location for ISR Datamart Refresh
  
  **************************************************************************/

  PROCEDURE load_primary_binloc(p_retcode OUT VARCHAR2);

  /*************************************************************************
  
      11. Procedure to Load Sales Quantity for ISR Datamart Refresh
  
  **************************************************************************/

  PROCEDURE load_sales_qty(p_retcode OUT VARCHAR2);

  /*************************************************************************
  
      12. Procedure to Load other Inventory inv org's Sales Qty for ISR Datamart Refresh
  
  **************************************************************************/

  PROCEDURE load_other_sales_qty(p_retcode OUT VARCHAR2);

  /*************************************************************************
  
      13. Procedure to Load safety stock for ISR Datamart Refresh
  
  **************************************************************************/

  PROCEDURE load_safety_stock(p_retcode OUT VARCHAR2);

  /*************************************************************************
  
      14. Procedure to Load GL period details for ISR Datamart Refresh
  
  **************************************************************************/

  PROCEDURE load_gl_periods(p_retcode OUT VARCHAR2);

  /*************************************************************************
  
      15. Procedure to Load Monthly Sales for ISR Datamart Refresh
  
  **************************************************************************/

  PROCEDURE load_monthly_sales(p_retcode OUT VARCHAR2);

  /*************************************************************************
  
      16. Procedure to Load Monthly Sales from other inv orgs for ISR Datamart Refresh
  
  **************************************************************************/

  PROCEDURE load_monthly_osales(p_retcode OUT VARCHAR2);

  /*************************************************************************
  
      17. Procedure to Load On Order Quantity for ISR Datamart Refresh
  
  **************************************************************************/

  PROCEDURE load_on_ord_qty(p_retcode OUT VARCHAR2);

  /*************************************************************************
  
      18. Procedure to Load Internal Requisition Quantity for ISR Datamart Refresh
  
  **************************************************************************/

  PROCEDURE load_int_req(p_retcode OUT VARCHAR2);

  /*************************************************************************
  
      19. Procedure to Load Open and Dir Requisitions for ISR Datamart Refresh
  
  **************************************************************************/

  PROCEDURE load_po_req(p_retcode OUT VARCHAR2);


  /*************************************************************************
  
      20. Procedure to Load Reserved Quantity for ISR Datamart Refresh
  
  **************************************************************************/

  PROCEDURE load_reserv_qty(p_retcode OUT VARCHAR2);

  /*************************************************************************
  
      21. Procedure to Load Quantity Onhand for ISR Datamart Refresh
  
  **************************************************************************/

  PROCEDURE load_qoh(p_retcode OUT VARCHAR2);


  /*************************************************************************
  
      22. Procedure to Load Demand Quantity for ISR Datamart Refresh
  
  **************************************************************************/

  PROCEDURE load_demand(p_retcode OUT VARCHAR2);

  /*************************************************************************
  
       23. Procedure to Load Mfg Part Number for ISR Datamart Refresh
  
  **************************************************************************/

  PROCEDURE load_mfg_items(p_retcode OUT VARCHAR2);

  /*************************************************************************
  
     24. Procedure to Load Master Vendor Number for ISR Datamart Refresh
  
  **************************************************************************/

  PROCEDURE load_mst_vendor(p_retcode OUT VARCHAR2);

  /*************************************************************************
  
     25. Procedure to Load Last Receipt date for ISR Datamart Refresh
  
  **************************************************************************/

  PROCEDURE load_last_receipt(p_retcode OUT VARCHAR2);

  /*************************************************************************
  
     26. Procedure to Load Onhand Greater than 270 Days for ISR Datamart Refresh
  
  **************************************************************************/

  PROCEDURE load_oh_gt_270(p_retcode OUT VARCHAR2);

  /*************************************************************************
  
     27. Procedure to Load Supersede Item for ISR Datamart Refresh
  
  **************************************************************************/

  PROCEDURE load_supersede_items(p_retcode OUT VARCHAR2);

  /*************************************************************************
  
     Final. Procedure to Load the ISR pre final table from sub tables
  
  **************************************************************************/

  PROCEDURE load_isr(p_retcode           OUT VARCHAR2
                    ,p_load_old_isr_flag VARCHAR2); --Added new parameter p_load_old_isr_flag for Ver 2.0

  /*************************************************************************
  
     Main. Main Procedure to run the sub loads sequentially
  
  **************************************************************************/

  PROCEDURE load_isr_main(errbuf              OUT VARCHAR2
                         ,retcode             OUT NUMBER
                         ,p_load_old_isr_flag IN VARCHAR2); --Added new parameter p_load_old_isr_flag for Ver 2.0

  /*************************************************************************
  
     Report. Procedure for Reporting
  
  **************************************************************************/

  PROCEDURE report_isr(p_process_id          IN NUMBER
                      ,p_region              IN VARCHAR2
                      ,p_district            IN VARCHAR2
                      ,p_location            IN VARCHAR2
                      ,p_dc_mode             IN VARCHAR2
                      ,p_tool_repair         IN VARCHAR2
                      ,p_time_sensitive      IN VARCHAR2
                      ,p_stk_items_with_hit4 IN VARCHAR2
                      ,p_report_condition    IN VARCHAR2
                      ,p_report_criteria     IN VARCHAR2
                      ,p_report_criteria_val IN VARCHAR2
                      ,p_start_bin_loc       IN VARCHAR2
                      ,p_end_bin_loc         IN VARCHAR2
                      ,p_vendor              IN VARCHAR2
                      ,p_item                IN VARCHAR2
                      ,p_cat_class           IN VARCHAR2
                      ,p_org_list            IN VARCHAR2
                      ,p_item_list           IN VARCHAR2
                      ,p_supplier_list       IN VARCHAR2
                      ,p_cat_class_list      IN VARCHAR2
                      ,p_source_list         IN VARCHAR2
                      ,p_intangibles         IN VARCHAR2);

  -- Added below function in 8.0 Rev.
  /*************************************************************************
  
     Function to derive Flip Date.
  
  **************************************************************************/

  FUNCTION flipdate_func(p_flip_stkflag        IN VARCHAR2
                        ,p_isr_stkflag         IN VARCHAR2
                        ,p_isr_flip_date       IN DATE
                        ,p_last_sv_change_date IN DATE) RETURN DATE;

  -- Version# 11.0 > Start
  /*************************************************************************
  
     Report. Procedure for Reporting
  
  **************************************************************************/

  PROCEDURE report_isr_live(p_process_id          IN NUMBER
                           ,p_region              IN VARCHAR2
                           ,p_district            IN VARCHAR2
                           ,p_location            IN VARCHAR2
                           ,p_dc_mode             IN VARCHAR2
                           ,p_tool_repair         IN VARCHAR2
                           ,p_time_sensitive      IN VARCHAR2
                           ,p_stk_items_with_hit4 IN VARCHAR2
                           ,p_report_condition    IN VARCHAR2
                           ,p_report_criteria     IN VARCHAR2
                           ,p_report_criteria_val IN VARCHAR2
                           ,p_start_bin_loc       IN VARCHAR2
                           ,p_end_bin_loc         IN VARCHAR2
                           ,p_vendor              IN VARCHAR2
                           ,p_item                IN VARCHAR2
                           ,p_cat_class           IN VARCHAR2
                           ,p_org_list            IN VARCHAR2
                           ,p_item_list           IN VARCHAR2
                           ,p_supplier_list       IN VARCHAR2
                           ,p_cat_class_list      IN VARCHAR2
                           ,p_source_list         IN VARCHAR2
                           ,p_intangibles         IN VARCHAR2);
  -- Version# 11.0 < End

  -- Version# <18.0> > Start 
    /*************************************************************************
   Function to return dummy vendor validations.
   * This function is not intended to be called by other programs.
  **************************************************************************/
  FUNCTION check_dummy_vendor_num(p_input IN VARCHAR2) RETURN VARCHAR2;

  /*************************************************************************
   Function to return dummy vendor list as text to be used under IN condition.
   * This function is not intended to be called by other programs.
  **************************************************************************/
  FUNCTION form_dummy_vendor_list RETURN VARCHAR2;
  -- Version# <18.0> < End

END XXWC_ISR_DATAMART_PKG;
/
