/*************************************************************************
  $Header TMS_20161013-00212_Direct_20242952.sql $
  Module Name: TMS_20161013-00212  Data Fix script for Direct 20242952

  PURPOSE: Data fix script for Direct 20242952--No permanent fix in process (But patch available per oracle)

  REVISIONS:
  Ver        Date         Author                Description
  ---------  -----------  ------------------    --------------------------
  1.0        17-JAN-2017  Pattabhi Avula        TMS#20161013-00212 

**************************************************************************/ 
SET SERVEROUTPUT ON SIZE 1000000
DECLARE
BEGIN
   DBMS_OUTPUT.put_line ('TMS: 20161013-00212    , Before Update');

update apps.oe_order_lines_all
set FLOW_STATUS_CODE='CANCELLED',
CANCELLED_FLAG='Y'
where line_id = 69235658
and header_id= 41801996;
   DBMS_OUTPUT.put_line (
         'TMS: 20161013-00212  Sales order lines updated (Expected:1): '
      || SQL%ROWCOUNT);

   COMMIT;

   DBMS_OUTPUT.put_line ('TMS: 20161013-00212    , End Update');
EXCEPTION
   WHEN OTHERS
   THEN
      ROLLBACK;
      DBMS_OUTPUT.put_line ('TMS: 20161013-00212 , Errors : ' || SQLERRM);
END;
/