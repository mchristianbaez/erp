---------------------------------------------------------------------------------------------------------------------------
/*************************************************************************************************************************
  PURPOSE:   Truncate Custom tables junk data
  REVISIONS:
     Ver        	Date         Author         Description
     ---------  ----------   	--------    -----------------------------------------------------------------------------------
     1.0	    22-Feb-2017    	 Siva			TMS#20170222-00042
****************************************************************************************************************************/
BEGIN
EXECUTE IMMEDIATE 'TRUNCATE TABLE XXEIS.EIS_DATA_MANAGMNT_DTLS';
dbms_output.put_line('The EIS_DATA_MANAGMNT_DTLS table truncated data successfully');
EXECUTE IMMEDIATE 'TRUNCATE TABLE XXEIS.EIS_HEADER_LINE_TAB';
dbms_output.put_line('The EIS_HEADER_LINE_TAB table truncated data successfully');
EXECUTE IMMEDIATE 'TRUNCATE TABLE XXEIS.EIS_XXWC_AR_EOM_COMM_TBL';
dbms_output.put_line('The EIS_XXWC_AR_EOM_COMM_TBL table truncated data successfully');
EXECUTE IMMEDIATE 'TRUNCATE TABLE XXEIS.EIS_XXWC_BIN_LOC_DATA_TAB';
dbms_output.put_line('The EIS_XXWC_BIN_LOC_DATA_TAB table truncated data successfully');
EXECUTE IMMEDIATE 'TRUNCATE TABLE XXEIS.EIS_XXWC_BIN_ONHAND_TAB';
dbms_output.put_line('The EIS_XXWC_BIN_ONHAND_TAB table truncated data successfully');
EXECUTE IMMEDIATE 'TRUNCATE TABLE XXEIS.EIS_XXWC_BIN_ORGS_TAB';
dbms_output.put_line('The EIS_XXWC_BIN_ORGS_TAB table truncated data successfully');
EXECUTE IMMEDIATE 'TRUNCATE TABLE XXEIS.EIS_XXWC_BIN_SALES_ONHAND_TAB';
dbms_output.put_line('The EIS_XXWC_BIN_SALES_ONHAND_TAB table truncated data successfully');
EXECUTE IMMEDIATE 'TRUNCATE TABLE XXEIS.EIS_XXWC_BIN_SALES_ORGS_TAB';
dbms_output.put_line('The EIS_XXWC_BIN_SALES_ORGS_TAB table truncated data successfully');
EXECUTE IMMEDIATE 'TRUNCATE TABLE XXEIS.EIS_XXWC_CASHEXCE_MAIN_TAB';
dbms_output.put_line('The EIS_XXWC_CASHEXCE_MAIN_TAB table truncated data successfully');
EXECUTE IMMEDIATE 'TRUNCATE TABLE XXEIS.EIS_XXWC_CASH_DRAWN_EXCE_TBL';
dbms_output.put_line('The EIS_XXWC_CASH_DRAWN_EXCE_TBL table truncated data successfully');
EXECUTE IMMEDIATE 'TRUNCATE TABLE XXEIS.EIS_XXWC_CASH_EXCEP_TBL';
dbms_output.put_line('The EIS_XXWC_CASH_EXCEP_TBL table truncated data successfully');
EXECUTE IMMEDIATE 'TRUNCATE TABLE XXEIS.EIS_XXWC_CASH_EXCEP_TMP_TBL';
dbms_output.put_line('The EIS_XXWC_CASH_EXCEP_TMP_TBL table truncated data successfully');
EXECUTE IMMEDIATE 'TRUNCATE TABLE XXEIS.EIS_XXWC_CASH_EXCE_ORDERS';
dbms_output.put_line('The EIS_XXWC_CASH_EXCE_ORDERS table truncated data successfully');
EXECUTE IMMEDIATE 'TRUNCATE TABLE XXEIS.EIS_XXWC_CNTR_ORD_TAB';
dbms_output.put_line('The EIS_XXWC_CNTR_ORD_TAB table truncated data successfully');
EXECUTE IMMEDIATE 'TRUNCATE TABLE XXEIS.EIS_XXWC_CREDIT_INV_TAB';
dbms_output.put_line('The EIS_XXWC_CREDIT_INV_TAB table truncated data successfully');
EXECUTE IMMEDIATE 'TRUNCATE TABLE XXEIS.EIS_XXWC_CREDIT_MEMO_DET_TAB';
dbms_output.put_line('The EIS_XXWC_CREDIT_MEMO_DET_TAB table truncated data successfully');
EXECUTE IMMEDIATE 'TRUNCATE TABLE XXEIS.EIS_XXWC_DAILY_FLASH_DTL_TAB';
dbms_output.put_line('The EIS_XXWC_DAILY_FLASH_DTL_TAB table truncated data successfully');
EXECUTE IMMEDIATE 'TRUNCATE TABLE XXEIS.EIS_XXWC_FLEX_VAL_TAB';
dbms_output.put_line('The EIS_XXWC_FLEX_VAL_TAB table truncated data successfully');
EXECUTE IMMEDIATE 'TRUNCATE TABLE XXEIS.EIS_XXWC_INTERNAL_SALES_TAB';
dbms_output.put_line('The EIS_XXWC_INTERNAL_SALES_TAB table truncated data successfully');
EXECUTE IMMEDIATE 'TRUNCATE TABLE XXEIS.EIS_XXWC_INT_ORD_TAB';
dbms_output.put_line('The EIS_XXWC_INT_ORD_TAB table truncated data successfully');
EXECUTE IMMEDIATE 'TRUNCATE TABLE XXEIS.EIS_XXWC_INV_BIN_LOC_SALES_TBL';
dbms_output.put_line('The EIS_XXWC_INV_BIN_LOC_SALES_TBL table truncated data successfully');
EXECUTE IMMEDIATE 'TRUNCATE TABLE XXEIS.EIS_XXWC_INV_LIST_TBL';
dbms_output.put_line('The EIS_XXWC_INV_LIST_TBL table truncated data successfully');
EXECUTE IMMEDIATE 'TRUNCATE TABLE XXEIS.EIS_XXWC_INV_TIME_MGMT_TBL';
dbms_output.put_line('The EIS_XXWC_INV_TIME_MGMT_TBL table truncated data successfully');
EXECUTE IMMEDIATE 'TRUNCATE TABLE XXEIS.EIS_XXWC_LOW_QTY_HDR_TBL';
dbms_output.put_line('The EIS_XXWC_LOW_QTY_HDR_TBL table truncated data successfully');
EXECUTE IMMEDIATE 'TRUNCATE TABLE XXEIS.EIS_XXWC_OM_LINE_DISP_TAB';
dbms_output.put_line('The EIS_XXWC_OM_LINE_DISP_TAB table truncated data successfully');
EXECUTE IMMEDIATE 'TRUNCATE TABLE XXEIS.EIS_XXWC_ONHAND_DTLS';
dbms_output.put_line('The EIS_XXWC_ONHAND_DTLS table truncated data successfully');
EXECUTE IMMEDIATE 'TRUNCATE TABLE XXEIS.EIS_XXWC_ORDER_EXP_TAB';
dbms_output.put_line('The EIS_XXWC_ORDER_EXP_TAB table truncated data successfully');
EXECUTE IMMEDIATE 'TRUNCATE TABLE XXEIS.EIS_XXWC_SUBINV_ONHAND_DTLS';
dbms_output.put_line('The EIS_XXWC_SUBINV_ONHAND_DTLS table truncated data successfully');
EXECUTE IMMEDIATE 'TRUNCATE TABLE XXEIS.EIS_XXWC_VALID_BIN_ORGS';
dbms_output.put_line('The EIS_XXWC_VALID_BIN_ORGS table truncated data successfully');
EXECUTE IMMEDIATE 'TRUNCATE TABLE XXEIS.EIS_XXWC_VALID_BIN_SALES_ORGS';
dbms_output.put_line('The EIS_XXWC_VALID_BIN_SALES_ORGS table truncated data successfully');
EXECUTE IMMEDIATE 'TRUNCATE TABLE XXEIS.EIS_XXWC_VALID_ORGS_TAB';
dbms_output.put_line('The EIS_XXWC_VALID_ORGS_TAB table truncated data successfully');
EXECUTE IMMEDIATE 'TRUNCATE TABLE XXEIS.XXWC_GTT_EIS_PRINT_LOG_TBL';
dbms_output.put_line('The XXWC_GTT_EIS_PRINT_LOG_TBL table truncated data successfully');
EXECUTE IMMEDIATE 'TRUNCATE TABLE XXEIS.XXWC_GTT_OE_ORDER_HEADERS_TBL';
dbms_output.put_line('The XXWC_GTT_OE_ORDER_HEADERS_TBL table truncated data successfully');
EXECUTE IMMEDIATE 'TRUNCATE TABLE XXEIS.XXWC_GTT_OE_ORDER_LINES_TBL';
dbms_output.put_line('The XXWC_GTT_OE_ORDER_LINES_TBL table truncated data successfully');
EXECUTE IMMEDIATE 'TRUNCATE TABLE XXEIS.XXWC_GTT_OPEN_ORDERS_TBL';
dbms_output.put_line('The XXWC_GTT_OPEN_ORDERS_TBL table truncated data successfully');
EXECUTE IMMEDIATE 'TRUNCATE TABLE XXEIS.XXWC_GTT_WSH_SHIPPING_STG_TBL';
dbms_output.put_line('The XXWC_GTT_WSH_SHIPPING_STG_TBL table truncated data successfully');
EXECUTE IMMEDIATE 'TRUNCATE TABLE XXEIS.XXWC_INV_RF_METRIC_RPT_TBL';
dbms_output.put_line('The XXWC_INV_RF_METRIC_RPT_TBL table truncated data successfully');
EXECUTE IMMEDIATE 'TRUNCATE TABLE XXEIS.XXWC_INV_SPCL_ONHAND_TBL';
dbms_output.put_line('The XXWC_INV_SPCL_ONHAND_TBL table truncated data successfully');
EXECUTE IMMEDIATE 'TRUNCATE TABLE XXEIS.XXWC_LOW_QTY_MAN_PRIC_TBL';
dbms_output.put_line('The XXWC_LOW_QTY_MAN_PRIC_TBL table truncated data successfully');
exception
WHEN others
THEN 
dbms_output.put_line(SUBSTR(SQLERRM,1,250));
END;
/
