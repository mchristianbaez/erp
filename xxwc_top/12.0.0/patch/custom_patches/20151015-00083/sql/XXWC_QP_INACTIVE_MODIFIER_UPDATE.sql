/*  
TMS# 20151015-00083 - SF/Pricing to purge active modifiers which are end dated
By Manjula Chellappan on 19-Oct-15
*/

SET SERVEROUTPUT ON SIZE 1000000

DECLARE
   l_control_rec               QP_GLOBALS.Control_Rec_Type;
   l_return_status             VARCHAR2 (1);
   x_msg_count                 NUMBER;
   x_msg_data                  VARCHAR2 (2000);
   x_msg_index                 NUMBER;

   l_MODIFIER_LIST_rec         QP_Modifiers_PUB.Modifier_List_Rec_Type;
   l_MODIFIER_LIST_rec1        QP_Modifiers_PUB.Modifier_List_Rec_Type;
   l_MODIFIER_LIST_val_rec     QP_Modifiers_PUB.Modifier_List_Val_Rec_Type;
   l_MODIFIERS_tbl             QP_Modifiers_PUB.Modifiers_Tbl_Type;
   l_MODIFIERS_tbl1            QP_Modifiers_PUB.Modifiers_Tbl_Type;
   l_MODIFIERS_val_tbl         QP_Modifiers_PUB.Modifiers_Val_Tbl_Type;
   l_QUALIFIERS_tbl            QP_Qualifier_Rules_PUB.Qualifiers_Tbl_Type;
   l_QUALIFIERS_val_tbl        QP_Qualifier_Rules_PUB.Qualifiers_Val_Tbl_Type;
   l_PRICING_ATTR_tbl          QP_Modifiers_PUB.Pricing_Attr_Tbl_Type;
   l_PRICING_ATTR_val_tbl      QP_Modifiers_PUB.Pricing_Attr_Val_Tbl_Type;
   l_x_MODIFIER_LIST_rec       QP_Modifiers_PUB.Modifier_List_Rec_Type;
   l_x_MODIFIER_LIST_val_rec   QP_Modifiers_PUB.Modifier_List_Val_Rec_Type;
   l_x_MODIFIERS_tbl           QP_Modifiers_PUB.Modifiers_Tbl_Type;
   l_x_MODIFIERS_val_tbl       QP_Modifiers_PUB.Modifiers_Val_Tbl_Type;
   l_x_QUALIFIERS_tbl          QP_Qualifier_Rules_PUB.Qualifiers_Tbl_Type;
   l_x_QUALIFIERS_val_tbl      QP_Qualifier_Rules_PUB.Qualifiers_Val_Tbl_Type;
   l_x_PRICING_ATTR_tbl        QP_Modifiers_PUB.Pricing_Attr_Tbl_Type;
   l_x_PRICING_ATTR_val_tbl    QP_Modifiers_PUB.Pricing_Attr_Val_Tbl_Type;
   l_msg_tbl                   OE_MSG_PUB.msg_tbl_type;


   l_msg                       VARCHAR2 (240);        
   l_date                      DATE := SYSDATE;
   i                           NUMBER := 0;
   l_count                     NUMBER := NULL;

   l_msg_count                 NUMBER;
   l_msg_data                  VARCHAR2 (1000);
   l_msg_dummy                 VARCHAR2 (1000);
   l_output                    VARCHAR2 (1000);
   l_error_msg                 VARCHAR2 (4000);


   CURSOR cur_hdr
   IS
      SELECT list_header_id,
             name,
             description,
             start_date_active,
             end_date_active,
             active_flag,
             list_type_code,
			 context
        FROM qp_list_headers
       WHERE     active_flag = 'Y'
	         AND list_type_code <>'PRL'
             AND NVL (end_date_active, TRUNC (SYSDATE)) < TRUNC (SYSDATE)
             AND context = '162'
             ORDER BY 1;
BEGIN
   DBMS_OUTPUT.put_line (
      'TMS# 20151015-00083 : Before Update : ' || SYSTIMESTAMP);

   FND_GLOBAL.APPS_INITIALIZE (USER_ID        => 29036,
                               RESP_ID        => NULL,
                               RESP_APPL_ID   => NULL);

   MO_GLOBAL.init ('ONT');

   DBMS_OUTPUT.put_line ('Apps Initialize completed');

    DBMS_OUTPUT.put_line ('Record Num ,Status,Datetime,List header id,Modifier Name, Modifier description,List type code,Context');   


   FOR cur_hdr_rec IN cur_hdr
   LOOP
      i := i + 1;

      l_MODIFIER_LIST_rec.list_header_id := cur_hdr_rec.list_header_id;
      l_MODIFIER_LIST_rec.active_flag := 'N';
      l_MODIFIER_LIST_rec.last_updated_by := 29036;
      l_MODIFIER_LIST_rec.last_update_date := SYSDATE;
      l_MODIFIER_LIST_rec.operation := QP_GLOBALS.G_OPR_UPDATE;


      OE_MSG_PUB.Initialize;


      QP_Modifiers_PUB.Process_Modifiers (
         p_api_version_number      => 1.0,
         p_init_msg_list           => FND_API.G_TRUE,
         p_return_values           => FND_API.G_TRUE,
         p_commit                  => FND_API.G_TRUE,
         x_return_status           => l_return_status,
         x_msg_count               => x_msg_count,
         x_msg_data                => x_msg_data,
         p_MODIFIER_LIST_rec       => l_MODIFIER_LIST_rec
		-- ,p_MODIFIERS_tbl        => l_MODIFIERS_tbl
        -- ,p_QUALIFIERS_tbl       => l_QUALIFIERS_tbl
        -- ,p_PRICING_ATTR_tbl     => l_PRICING_ATTR_tbl 
           ,		
         x_MODIFIER_LIST_rec       => l_MODIFIER_LIST_rec1,
         x_MODIFIER_LIST_val_rec   => l_MODIFIER_LIST_val_rec,
         x_MODIFIERS_tbl           => l_MODIFIERS_tbl1,
         x_MODIFIERS_val_tbl       => l_MODIFIERS_val_tbl,
         x_QUALIFIERS_tbl          => l_QUALIFIERS_tbl,
         x_QUALIFIERS_val_tbl      => l_QUALIFIERS_val_tbl,
         x_PRICING_ATTR_tbl        => l_PRICING_ATTR_tbl,
         x_PRICING_ATTR_val_tbl    => l_PRICING_ATTR_val_tbl);

      IF l_return_status = fnd_api.g_ret_sts_success
      THEN
         COMMIT;
         DBMS_OUTPUT.put_line (
             i
            || ', Processed ,'
            || SYSTIMESTAMP
            || ','
            || cur_hdr_rec.list_header_id
            || ','
			|| cur_hdr_rec.description||','
            || cur_hdr_rec.name||','
			|| cur_hdr_rec.list_type_code
			||','
			|| cur_hdr_rec.context);
      ELSE
         DBMS_OUTPUT.put_line (
                i
            || ', Failed , '
            || SYSTIMESTAMP
            || ' ,'
            || cur_hdr_rec.list_header_id|| ','
			|| cur_hdr_rec.description||','
            || cur_hdr_rec.name||','
			|| cur_hdr_rec.list_type_code
			||','
			|| cur_hdr_rec.context);

         OE_MSG_PUB.get_msg_tbl (l_msg_tbl);

         IF l_msg_tbl.COUNT > 0
         THEN
            FOR j IN l_msg_tbl.FIRST .. l_msg_tbl.LAST
            LOOP
               DBMS_OUTPUT.PUT_LINE ('API Error : ' || l_msg_tbl (j).MESSAGE);
            END LOOP;
         END IF;

         ROLLBACK;
      END IF;
   END LOOP;

   DBMS_OUTPUT.put_line ('Records Processed : ' || i);
   DBMS_OUTPUT.put_line (
      'TMS# 20151015-00083 : After Update : ' || SYSTIMESTAMP);
EXCEPTION
   WHEN OTHERS
   THEN
      NULL;
      DBMS_OUTPUT.put_line ('Result:' || SQLCODE || SQLERRM);
END;
/