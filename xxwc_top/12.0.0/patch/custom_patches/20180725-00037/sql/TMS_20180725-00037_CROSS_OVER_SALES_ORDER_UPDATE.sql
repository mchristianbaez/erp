/*************************************************************************
  $Header TMS_20180725-00037_CROSS_OVER_SALES_ORDER_UPDATE.sql $
  Module Name: TMS_20180725-00037  CROSS OVER - SALES ORDER UPDATE SCRIPT

  PURPOSE: Data fix to update the CROSS OVER - SALES ORDER UPDATE SCRIPT
  
  ----------------------------------------------------------------
  CROSS OVER - SALES ORDER UPDATE SCRIPT
  ----------------------------------------------------------------

  REVISIONS:
  Ver        Date         Author                Description
  ---------  -----------  ------------------    --------------------------
  1.0        25-JUL-2018  Pattabhi Avula        TMS#20180725-00037 

**************************************************************************/ 
SET SERVEROUTPUT ON SIZE 100000;
SET VERIFY OFF;

DECLARE

CURSOR CUR IS 
select DISTINCT ahh_customer_number 
, ahh_customer_site_number
, ahh_outside_sales_rep_id
, order_number
, header_id
, SALESREP_ID
, orcl_salesrep_number
, orcl_salesrep_id
from
(
select DISTINCT customer_number ahh_customer_number 
, customer_site_number                   ahh_customer_site_number
, OUTSIDE_SALES_REP_ID                   ahh_outside_sales_rep_id
, ooh1.order_number
, ooh1.header_id
, ooh1.SALESREP_ID
, a.SALESREP_NUMBER                      orcl_salesrep_number
, a.salesrep_id                          orcl_salesrep_id
--, hcas1.cust_acct_site_id
from XXWC.XXWC_AR_CONV_CUST_ACCT_TBL_NEW stg
, RA_SALESREPS_ALL A
, XXWC.XXWC_AHH_AM_T B
, apps.hz_cust_acct_sites_all hcas1
, apps.hz_cust_site_uses_all  hcsu1_s
, apps.oe_order_headers_all   ooh1
WHERE 1=1
AND stg.customer_number IN (SELECT cust_num FROM xxwc.xxwc_ahh_ar_cust_cross_over_t) 
AND A.ORG_ID                = 162
AND hcas1.cust_acct_site_id = hcsu1_s.cust_acct_site_id
AND ooh1.SHIP_TO_ORG_ID     = hcsu1_s.site_use_id
AND hcsu1_s.site_use_code   = 'SHIP_TO'
AND UPPER (B.SLSREPOUT)     = UPPER (stg.OUTSIDE_SALES_REP_ID)
AND A.SALESREP_NUMBER       = B.SALESREP_ID
AND hcas1.attribute17       =  customer_number||'-'||customer_site_number
AND ooh1.SALESREP_ID       != a.salesrep_id
UNION
select DISTINCT stg.ORIG_SYSTEM_REFERENCE ahh_customer_number 
, stg.ATTRIBUTE17                   ahh_customer_site_number
, stg.PRIMARY_SALESREP_NUMBER                   ahh_outside_sales_rep_id
, ooh1.order_number
, ooh1.header_id
, ooh1.SALESREP_ID
, a.SALESREP_NUMBER                      orcl_salesrep_number
, a.salesrep_id                          orcl_salesrep_id
--, hcas1.cust_acct_site_id
from xxwc.XXWC_AR_CUST_SITE_IFACE_STG stg
, RA_SALESREPS_ALL A
, XXWC.XXWC_AHH_AM_T B
, apps.hz_cust_acct_sites_all hcas1
, apps.hz_cust_site_uses_all  hcsu1_s
, apps.oe_order_headers_all   ooh1
WHERE 1=1
AND stg.ORIG_SYSTEM_REFERENCE IN (SELECT cust_num FROM xxwc.xxwc_ahh_ar_cust_cross_over_t) 
AND A.ORG_ID                = 162
AND hcas1.cust_acct_site_id = hcsu1_s.cust_acct_site_id
AND ooh1.SHIP_TO_ORG_ID     = hcsu1_s.site_use_id
AND hcsu1_s.site_use_code   = 'SHIP_TO'
AND UPPER (B.SLSREPOUT)     = UPPER (stg.PRIMARY_SALESREP_NUMBER)
AND A.SALESREP_NUMBER       = B.SALESREP_ID
AND hcas1.attribute17       =  stg.ORIG_SYSTEM_REFERENCE||'-'||stg.ATTRIBUTE17
AND ooh1.SALESREP_ID       != a.salesrep_id
UNION
select DISTINCT customer_number ahh_customer_number 
, customer_site_number                   ahh_customer_site_number
, OUTSIDE_SALES_REP_ID                   ahh_outside_sales_rep_id
, ooh1.order_number
, ooh1.header_id
, ooh1.SALESREP_ID
, a.SALESREP_NUMBER                      orcl_salesrep_number
, a.salesrep_id                          orcl_salesrep_id
--, hcas1.cust_acct_site_id
from XXWC.XXWC_AR_CONV_CUST_ACCT_TBL_BKP stg
, RA_SALESREPS_ALL A
, XXWC.XXWC_AHH_AM_T B
, apps.hz_cust_acct_sites_all hcas1
, apps.hz_cust_site_uses_all  hcsu1_s
, apps.oe_order_headers_all   ooh1
WHERE 1=1
AND stg.customer_number IN (SELECT cust_num FROM xxwc.xxwc_ahh_ar_cust_cross_over_t) 
AND A.ORG_ID                = 162
AND hcas1.cust_acct_site_id = hcsu1_s.cust_acct_site_id
AND ooh1.SHIP_TO_ORG_ID     = hcsu1_s.site_use_id
AND hcsu1_s.site_use_code   = 'SHIP_TO'
AND UPPER (B.SLSREPOUT)     = UPPER (stg.OUTSIDE_SALES_REP_ID)
AND A.SALESREP_NUMBER       = B.SALESREP_ID
AND hcas1.attribute17       =  customer_number||'-'||customer_site_number
AND ooh1.SALESREP_ID       != a.salesrep_id
);

BEGIN

DBMS_OUTPUT.put_line ('TMS: 20180725-00037    , Before Update');

UPDATE apps.oe_order_headers_all oh
SET SALESREP_ID = 100435258
WHERE 1=1
  AND header_id IN (75772529
, 75788278
, 76022424
, 76024400
, 76025716
, 76029039
, 76052365
, 75768936
, 75769701
, 75834998
, 75961326
, 75962015
, 75978501
, 75989675
, 76008242
, 76023599
, 76048742
, 75766341
, 75768931
, 75769878
, 75773667
, 76023737
, 76023788
, 76023824
, 76023931
, 76064012
, 76024207);

DBMS_OUTPUT.put_line (
         'TMS: 20180725-00037 - number of records updated: '
      || SQL%ROWCOUNT);

COMMIT;

UPDATE apps.oe_order_headers_all oh
SET SALESREP_ID = 100415269
WHERE 1=1
  AND header_id IN (75772096, 
75961465, 
75961725, 
75768706, 
75769574, 
75776646, 
75981816, 
75984130, 
75867739, 
75917279, 
75768772, 
75769650, 
75589605, 
75766367, 
75768339, 
75768725, 
75769350, 
75771567, 
75805825, 
75834740, 
75834816, 
75834827, 
75866153, 
75898588, 
75898626, 
75901098, 
75943842, 
75961638, 
75977715, 
75773822, 
75836587, 
75898516, 
75899175, 
75981761);

DBMS_OUTPUT.put_line (
         'TMS: 20180725-00037 - number of records updated: '
      || SQL%ROWCOUNT);


COMMIT;

--FOR rec IN cur LOOP

--UPDATE apps.oe_order_headers_all oh
--SET SALESREP_ID = rec.orcl_salesrep_id
--WHERE header_id = rec.header_id
--  AND apps.xxwc_om_force_ship_pkg.is_row_locked (oh.ROWID, 'OE_ORDER_HEADERS_ALL')  = 'N';

--COMMIT;

--END LOOP;

DBMS_OUTPUT.put_line (
         'TMS: 20180725-00037 - number of records updated: '
      || SQL%ROWCOUNT);

DBMS_OUTPUT.put_line ('TMS: 20180725-00037   , End Update');
EXCEPTION
 WHEN OTHERS THEN
  DBMS_OUTPUT.put_line ('Unexpected error while executing the script: '||SQLERRM);
END; 
/ 