/*************************************************************************
  $Header TMS_20170822-00058_APEXQA_DB_LINK_UPDATE.sql $
  Module Name: 20170822-00058  WCAPEXQA db link update, currently routing to wcapexprd

  PURPOSE: Data fix  

  REVISIONS:
  Ver        Date         Author                Description
  ---------  -----------  ------------------    --------------------------
  1.0        22-AUG-2017  Pattabhi Avula        TMS#20170822-00058 

**************************************************************************/ 
DROP DATABASE LINK WCAPXPRD.HSI.HUGHESSUPPLY.COM;

CREATE PUBLIC DATABASE LINK WCAPXPRD.HSI.HUGHESSUPPLY.COM CONNECT TO WC_APPS IDENTIFIED BY apps4wcprd USING '(DESCRIPTION = (ADDRESS = (PROTOCOL = TCP)(HOST = wcapxqa.hdsupply.net)(PORT = 1521)) (CONNECT_DATA = (SERVER = DEDICATED) (SERVICE_NAME = wcapxqa)))'
/ 