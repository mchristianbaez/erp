/*************************************************************************
  $Header TMS_20160404-00299_AWAITING_INVOICE_TO_CLOSE.sql $
  Module Name: TMS_20160404-00299 Data Fix script for I667946 

  PURPOSE: Data Fix script for I667946

  REVISIONS:
  Ver        Date         Author                Description
  ---------  -----------  ------------------    --------------------------
  1.0        16-MAY-2016  Raghav Velichetti         TMS#20160404-00299

**************************************************************************/ 
SET SERVEROUTPUT ON SIZE 1000000
DECLARE
BEGIN
   DBMS_OUTPUT.put_line ('TMS: 20160404-00299   , Before Update');

update apps.oe_order_lines_all
set 
  INVOICE_INTERFACE_STATUS_CODE='YES'
,open_flag='N'
,flow_status_code='CLOSED'
,INVOICED_QUANTITY=70
where line_id=27896903
and headeR_id=15486644;

   DBMS_OUTPUT.put_line (
         'TMS: 20160404-00299 Sales order lines updated (Expected:1): '
      || SQL%ROWCOUNT);

   COMMIT;

   DBMS_OUTPUT.put_line ('TMS: 20160404-00299   , End Update');
EXCEPTION
   WHEN OTHERS
   THEN
      ROLLBACK;
      DBMS_OUTPUT.put_line ('TMS: 20160404-00299, Errors : ' || SQLERRM);
END;
/