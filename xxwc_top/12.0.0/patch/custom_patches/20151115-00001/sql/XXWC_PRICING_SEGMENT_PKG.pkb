CREATE OR REPLACE PACKAGE BODY APPS.XXWC_PRICING_SEGMENT_PKG
AS

   /*************************************************************************
      $Header XXWC_PRICING_SEGMENT_PKG.PKB $
      Module Name: XXWC_PRICING_SEGMENT_PKG.PKB

      PURPOSE:   This package is used by Pricing Segmentation process

      REVISIONS:
      Ver        Date        Author                     Description
      ---------  ----------  ---------------         -------------------------
      1.0        10/09/2015  Gopi Damuluri           Initial Version
                                                     TMS# 20151111-00161
      1.1        11/14/2015  Gopi Damuluri           TMS# 20151115-00001 
                                                     Resolve invallid number issue.
   ****************************************************************************/

   --Email Defaults
   g_dflt_email   fnd_user.email_address%TYPE    := 'WC-ITDEVALERTS-U1@HDSupply.com';

   /*************************************************************************
      PROCEDURE Name: main

      PURPOSE:   To validate B2B Customer Staging data and load the same into
                 Interface table.

      REVISIONS:
      Ver        Date        Author                     Description
      ---------  ----------  ---------------         -------------------------
      1.0        10/09/2015  Gopi Damuluri           Initial Version
                                                     TMS# 20151111-00161
      1.1        11/14/2015  Gopi Damuluri           TMS# 20151115-00001 
                                                     Resolve invallid number issue.
   ***************************************************************************/
   PROCEDURE main (p_errbuf                  OUT VARCHAR2,
                   p_retcode                 OUT NUMBER,
                   p_process_date             IN VARCHAR2,
                   p_in_seg_list_header_id    IN NUMBER,
                   p_in_csp_list_header_id    IN NUMBER,
                   p_process_method           IN VARCHAR2)
   IS

      ----------------------------------------------------------
      -- MATRIX Modifier Header - Cursor
      ----------------------------------------------------------
      CURSOR cur_exl(p_to_date             IN DATE)
      IS
      SELECT DISTINCT qlh.list_header_id                seg_list_header_id
           , qq.qualifier_context
           , qq.qualifier_attribute
           , TO_NUMBER (qq.qualifier_attr_value)
           , qlh2.attribute14                  agreement_id
           , qlh2.list_header_id               csp_list_header_id
           , xch.customer_id
           , xch.customer_site_id
           , qlh.name
        FROM qp_list_headers_all               qlh
           , qp_qualifiers                     qq
           , qp_list_headers_all               qlh2
           , xxwc.xxwc_om_contract_pricing_hdr xch
       WHERE qlh.attribute10                 = 'Segmented Price'
         AND qlh.list_header_id              = qq.list_header_id
         AND (qlh.list_header_id             = p_in_seg_list_header_id OR p_in_seg_list_header_id IS NULL)
         AND (qlh2.list_header_id            = p_in_csp_list_header_id OR p_in_csp_list_header_id IS NULL)
         AND qlh2.attribute10                = 'Contract Pricing'
         AND qq.qualifier_attribute          = 'QUALIFIER_ATTRIBUTE32'
         AND qq.qualifier_attr_value         = TO_CHAR(xch.customer_id)
         AND xch.agreement_status            = 'APPROVED'
         AND TO_NUMBER (qlh2.attribute14)    = xch.agreement_id
         AND xch.incompatability_group       = 'Exclusive'
         AND qlh.active_flag                 = 'Y'
         AND qlh2.active_flag                = 'Y'
         AND TRUNC(SYSDATE) BETWEEN TRUNC(NVL(qlh.start_date_active, SYSDATE -1))  AND TRUNC(NVL(qlh.end_date_active, SYSDATE+ 1))
         AND TRUNC(SYSDATE) BETWEEN TRUNC(NVL(qlh2.start_date_active, SYSDATE -1))  AND TRUNC(NVL(qlh2.end_date_active, SYSDATE+ 1))
         -- AND TRUNC(qlh.creation_date)       >= p_to_date
         AND EXISTS (SELECT '1'
                       FROM qp_modifier_summary_v qll -- CSP Modifier
                      WHERE 1 = 1
                        AND qll.list_header_id         = qlh2.list_header_id
                        AND TRUNC(qll.last_update_date)   >= p_to_date
                        AND TRUNC(SYSDATE) BETWEEN TRUNC(NVL(qll.start_date_active, SYSDATE -1))  AND TRUNC(NVL(qll.end_date_active, SYSDATE+ 1))
                     UNION
                     SELECT '1'
                       FROM qp_modifier_summary_v qll2 -- Segment Modifier
                      WHERE 1 = 1
                        AND qll2.list_header_id         = qlh.list_header_id
                        AND TRUNC(qll2.last_update_date)   >= p_to_date
                        AND TRUNC(SYSDATE) BETWEEN TRUNC(NVL(qll2.start_date_active, SYSDATE -1))  AND TRUNC(NVL(qll2.end_date_active, SYSDATE+ 1))
                     UNION
                     SELECT '1'
                       FROM dual
                      WHERE p_in_seg_list_header_id IS NOT NULL
                     UNION
                     SELECT '1'
                       FROM dual
                      WHERE p_in_csp_list_header_id IS NOT NULL
                    )
         ;

      ----------------------------------------------------------
      -- Derive Excluder Line - Cursor
      ----------------------------------------------------------
      CURSOR cur_excluder_line (p_csp_product_attr      IN VARCHAR2
                              , p_csp_product_attr_val  IN VARCHAR2
                              , p_seg_list_header_id    IN NUMBER
                               )
          IS
      SELECT seg.list_line_id
        FROM qp_modifier_summary_v seg
       WHERE p_csp_product_attr           = 'PRICING_ATTRIBUTE1'
         AND seg.list_header_id           = p_seg_list_header_id
         AND seg.product_attr             = 'PRICING_ATTRIBUTE1' -- Item
         AND seg.product_attr_val         = p_csp_product_attr_val
         AND NVL(seg.excluder_Flag,'N')   = 'N'
         AND SYSDATE BETWEEN TRUNC(NVL(seg.start_date_active, SYSDATE - 1)) AND TRUNC(NVL(seg.end_date_active, SYSDATE+ 1))
       UNION       
      SELECT seg.list_line_id
        FROM qp_modifier_summary_v seg
           , mtl_item_categories   mic
       WHERE p_csp_product_attr           = 'PRICING_ATTRIBUTE1'
         AND seg.list_header_id           = p_seg_list_header_id
         AND seg.product_attr             = 'PRICING_ATTRIBUTE2' -- Item Category
         -- AND TO_NUMBER(seg.product_attr_val)  = mic.category_id -- Version# 1.1
         AND seg.product_attr_val          = TO_CHAR(mic.category_id) -- Version# 1.1
         AND mic.inventory_item_id        = TO_NUMBER(p_csp_product_attr_val)
         AND NVL(seg.excluder_Flag,'N')   = 'N'
         AND mic.organization_id          = 222
         AND SYSDATE BETWEEN TRUNC(NVL(seg.start_date_active, SYSDATE - 1)) AND TRUNC(NVL(seg.end_date_active, SYSDATE+ 1))
       UNION 
      SELECT seg.list_line_id
        FROM qp_modifier_summary_v seg
       WHERE p_csp_product_attr           = 'PRICING_ATTRIBUTE2'  
         AND seg.list_header_id           = p_seg_list_header_id
         AND seg.product_attr             = 'PRICING_ATTRIBUTE2' -- Item Category
         AND seg.product_attr_val         = p_csp_product_attr_val
         AND NVL(seg.excluder_Flag,'N')   = 'N'
         AND SYSDATE BETWEEN TRUNC(NVL(seg.start_date_active, SYSDATE - 1)) AND TRUNC(NVL(seg.end_date_active, SYSDATE+ 1))
       UNION
      SELECT seg.list_line_id
        FROM qp_modifier_summary_v seg
           , mtl_item_categories   mic
       WHERE p_csp_product_attr           = 'PRICING_ATTRIBUTE2' 
         AND seg.list_header_id           = p_seg_list_header_id
         AND seg.product_attr             = 'PRICING_ATTRIBUTE1' -- Item
         AND mic.category_id              = TO_NUMBER(p_csp_product_attr_val)
         AND TO_CHAR(mic.inventory_item_id) = seg.product_attr_val -- Version# 1.1
         AND NVL(seg.excluder_Flag,'N')   = 'N'
         AND mic.organization_id = 222
         AND SYSDATE BETWEEN TRUNC(NVL(seg.start_date_active, SYSDATE - 1)) AND TRUNC(NVL(seg.end_date_active, SYSDATE+ 1));

      ----------------------------------------------------------
      -- Customer Site Cursor
      ----------------------------------------------------------
      CURSOR cur_site(p_customer_id             IN NUMBER)
      IS
      SELECT DISTINCT  hcsu.site_use_id                  customer_site_id
        FROM hz_cust_acct_sites_all            hcas
           , hz_cust_site_uses_all             hcsu
       WHERE 1=1 
         AND hcas.cust_account_id            = p_customer_id
         AND hcsu.cust_acct_site_id          = hcas.cust_acct_site_id
         AND hcsu.site_use_code              = 'SHIP_TO'
         AND hcas.status                     = 'A'
         AND hcsu.status                     = 'A'
         ;

      ----------------------------------------------------------
      -- Best Price Wins Agreement Header - Cursor
      ----------------------------------------------------------
      CURSOR cur_bpw(p_to_date             IN DATE)
      IS
      SELECT DISTINCT qlh.list_header_id   seg_list_header_id
           , qq.qualifier_context
           , qq.qualifier_attribute
           , TO_NUMBER (qq.qualifier_attr_value) 
           , qlh2.attribute14     agreement_id
           , qlh2.list_header_id  csp_list_header_id
           , xch.customer_id
           , xch.customer_site_id
        FROM qp_list_headers_all  qlh
           , qp_qualifiers        qq
           , qp_list_headers_all               qlh2
           , xxwc.xxwc_om_contract_pricing_hdr xch
       WHERE qlh.attribute10                 = 'Segmented Price'
         AND qlh.list_header_id              = qq.list_header_id
         AND (qlh.list_header_id             = p_in_seg_list_header_id OR p_in_seg_list_header_id IS NULL)
         AND (qlh2.list_header_id            = p_in_csp_list_header_id OR p_in_csp_list_header_id IS NULL)
         AND qq.qualifier_attribute          = 'QUALIFIER_ATTRIBUTE32'
         AND qq.qualifier_attr_value         = TO_CHAR(xch.customer_id)
         AND qlh2.attribute10                = 'Contract Pricing'
         AND TO_NUMBER (qlh2.attribute14)    = xch.agreement_id
         AND xch.incompatability_group       = 'Best Price Wins'
         AND xch.agreement_status            = 'APPROVED'
         AND qlh.active_flag                 = 'Y'
         AND qlh2.active_flag                = 'Y'
         AND TRUNC(SYSDATE) BETWEEN TRUNC(NVL(qlh.start_date_active, SYSDATE -1))  AND TRUNC(NVL(qlh.end_date_active, SYSDATE+ 1))
         AND TRUNC(SYSDATE) BETWEEN TRUNC(NVL(qlh2.start_date_active, SYSDATE -1))  AND TRUNC(NVL(qlh2.end_date_active, SYSDATE+ 1))
         -- AND TRUNC(qlh.creation_date)       >= p_to_date
         AND EXISTS (SELECT '1'
                       FROM qp_modifier_summary_v qll -- BPW Modifier
                      WHERE 1 = 1
                        AND qll.list_header_id         = qlh2.list_header_id
                        AND TRUNC(qll.last_update_date)   >= p_to_date
                        AND TRUNC(SYSDATE) BETWEEN TRUNC(NVL(qll.start_date_active, SYSDATE -1))  AND TRUNC(NVL(qll.end_date_active, SYSDATE+ 1))
                     UNION
                     SELECT '1'
                       FROM qp_modifier_summary_v qll2 -- Segment Modifier
                      WHERE 1 = 1
                        AND qll2.list_header_id         = qlh.list_header_id
                        AND TRUNC(qll2.last_update_date)   >= p_to_date
                        AND TRUNC(SYSDATE) BETWEEN TRUNC(NVL(qll2.start_date_active, SYSDATE -1))  AND TRUNC(NVL(qll2.end_date_active, SYSDATE+ 1))
                     UNION
                     SELECT '1'
                       FROM xxwc.xxwc_pricing_seg_gtt_tbl gtt
                      --WHERE gtt.customer_id = TO_NUMBER(qq.qualifier_attr_value) -- Version# 1.1
                      WHERE TO_CHAR(gtt.customer_id) = qq.qualifier_attr_value -- Version# 1.1
                     UNION
                     SELECT '1'
                       FROM dual
                      WHERE p_in_seg_list_header_id IS NOT NULL
                     UNION
                     SELECT '1'
                       FROM dual
                      WHERE p_in_csp_list_header_id IS NOT NULL
                    )
                        ;

      ----------------------------------------------------------
      -- Best Price Wins Agreement Header - Check if Exclusive Exists
      ----------------------------------------------------------
      CURSOR cur_bpw_exl (p_customer_id    IN NUMBER)
      IS
      SELECT DISTINCT qlh_e.list_header_id               excl_list_header_id
           , xch_e.agreement_id                 agreement_id
           , xch_e.customer_id
           , xch_e.customer_site_id
        FROM qp_list_headers_all                qlh_e
           , xxwc.xxwc_om_contract_pricing_hdr  xch_e
       WHERE xch_e.customer_id                = p_customer_id
         AND xch_e.incompatability_group      = 'Exclusive'
         AND TO_NUMBER (qlh_e.attribute14)    = xch_e.agreement_id
         AND xch_e.agreement_status           = 'APPROVED'
         AND qlh_e.attribute10                = 'Contract Pricing'
         AND qlh_e.active_flag                = 'Y'
         AND TRUNC(SYSDATE) BETWEEN TRUNC(NVL(qlh_e.start_date_active, SYSDATE -1))  AND TRUNC(NVL(qlh_e.end_date_active, SYSDATE+ 1))
         ;

      ----------------------------------------------------------
      -- PriceList Attribute - Cursor
      ----------------------------------------------------------
      CURSOR Get_Pricing_Attr_Cur (p_list_header_id IN NUMBER, p_list_line_id IN NUMBER, p_excluder_Flag IN VARCHAR2 )
      IS
      SELECT * 
        FROM qp_pricing_attributes 
       WHERE list_header_id = p_list_header_id
         AND list_line_id   = p_list_line_id
         AND NVL(excluder_Flag,'N') = p_excluder_Flag
         ;

      ----------------------------------------------------------
      -- PriceList Modifier Line - Cursor
      ----------------------------------------------------------
      CURSOR cur_seg_mod_line (p_seg_list_header_id    IN NUMBER
                             , p_seg_list_line_id      IN NUMBER
                             , p_csp_product_attr      IN VARCHAR
                             , p_csp_product_attr_val  IN VARCHAR
                             )
      IS
      SELECT *
        FROM qp_modifier_summary_v seg
       WHERE seg.list_header_id         = p_seg_list_header_id
         AND seg.list_line_id           = p_seg_list_line_id
         AND TRUNC(SYSDATE) BETWEEN TRUNC(NVL(seg.start_date_active, SYSDATE -1))  AND TRUNC(NVL(seg.end_date_active, SYSDATE+ 1))
         AND NVL(seg.excluder_Flag,'N') = 'N'
         AND NOT EXISTS (SELECT '1'
                           FROM qp_modifier_summary_v           seg2
                          WHERE seg2.list_line_id             = p_seg_list_line_id
                            AND NVL(seg2.excluder_Flag,'N')   = 'Y'
                            AND TRUNC(SYSDATE) BETWEEN TRUNC(NVL(seg2.START_DATE_ACTIVE, SYSDATE -1))  AND TRUNC(NVL(seg2.END_DATE_ACTIVE, SYSDATE+ 1))
                            AND seg2.product_attr             = p_csp_product_attr
                            AND seg2.product_attr_val         = p_csp_product_attr_val
                         );

      ----------------------------------------------------------
      -- PriceList Modifier Line - Cursor
      ----------------------------------------------------------
      CURSOR cur_csp_mod_line (p_list_header_id IN NUMBER, p_excluder_Flag IN VARCHAR2)
      IS
      SELECT * 
        FROM qp_modifier_summary_v  
       WHERE list_header_id         = p_list_header_id
         AND NVL(excluder_Flag,'N') = p_excluder_Flag
         AND TRUNC(SYSDATE) BETWEEN TRUNC(NVL(START_DATE_ACTIVE, SYSDATE -1))  AND TRUNC(NVL(END_DATE_ACTIVE, SYSDATE+ 1))
         -- AND ROWNUM < 10
      ;

      ----------------------------------------------------------
      -- PriceList Modifier Line - Cursor Best Price Wins
      ----------------------------------------------------------
      CURSOR cur_bpw_mod_line (p_seg_list_header_id IN NUMBER, p_list_header_id IN NUMBER, p_list_line_id IN NUMBER, p_incompatibility_grp IN VARCHAR2, p_line_exists IN VARCHAR2)
      IS
      SELECT *
        FROM qp_modifier_summary_v 
       WHERE list_header_id = p_list_header_id
         AND NVL(excluder_Flag,'N') = 'N'
         AND TRUNC(SYSDATE) BETWEEN TRUNC(NVL(START_DATE_ACTIVE, SYSDATE -1))  AND TRUNC(NVL(END_DATE_ACTIVE, SYSDATE+ 1))
         AND NVL(incompatibility_grp,'-1') = NVL(p_incompatibility_grp,NVL(incompatibility_grp,'-1')) -- 'Exclusive Group'
         AND get_line_exists(product_attr, product_attr_val, p_seg_list_header_id) = p_line_exists
         AND p_seg_list_header_id != p_list_header_id
      UNION
      SELECT *
        FROM qp_modifier_summary_v 
       WHERE list_header_id = p_list_header_id
         AND NVL(excluder_Flag,'N') = 'N'
         AND TRUNC(SYSDATE) BETWEEN TRUNC(NVL(START_DATE_ACTIVE, SYSDATE -1))  AND TRUNC(NVL(END_DATE_ACTIVE, SYSDATE+ 1))
         AND NVL(incompatibility_grp,'-1') = NVL(p_incompatibility_grp,NVL(incompatibility_grp,'-1')) -- 'Exclusive Group'
         AND p_seg_list_header_id = p_list_header_id
      ;

      ----------------------------------------------------------
      -- CSPs associated with In-Active Segment Modifiers
      ----------------------------------------------------------
      CURSOR cur_inactv_mod (p_to_date             IN DATE)
          IS
      -----------------------------------------------------------------------------
      -- End-Date MATRIX - Proces BPW
      -----------------------------------------------------------------------------
      SELECT DISTINCT xch_b.agreement_id         agreement_id
           , qlh_b.list_header_id               csp_list_header_id
           , xch_b.customer_id                  customer_id
           , xch_b.incompatability_group
           , xch_b.customer_site_id
           , 'Inactive_Matrx'                  SCENARIO
           , 'BPW'                             modifier_type
        FROM qp_list_headers_all               qlh_b
           , xxwc.xxwc_om_contract_pricing_hdr xch_b
       WHERE 1 = 1
         -- AND p_process_method                 = 'NEW'
         AND qlh_b.attribute10                = 'Contract Pricing'
         AND xch_b.agreement_status           = 'APPROVED'
         AND TO_NUMBER (qlh_b.attribute14)    = xch_b.agreement_id
         AND qlh_b.active_flag                = 'Y'
         AND xch_b.incompatability_group      = 'Best Price Wins'
         AND TRUNC(SYSDATE) BETWEEN TRUNC(NVL(qlh_b.start_date_active, SYSDATE -1))  AND TRUNC(NVL(qlh_b.end_date_active, SYSDATE+ 1))
         AND EXISTS (SELECT '1'
                          FROM qp_list_headers_all                             qlh_s
                             , qp_qualifiers                                   qq_s
                         WHERE 1 = 1
                           AND qq_s.qualifier_attr_value                       = TO_CHAR(xch_b.customer_id)
                           AND qq_s.qualifier_attribute                        = 'QUALIFIER_ATTRIBUTE32'
                           AND qlh_s.list_header_id                            = qq_s.list_header_id
                           AND qlh_s.attribute10                               = 'Segmented Price'
                           AND (qlh_s.list_header_id                           = p_in_seg_list_header_id OR p_in_seg_list_header_id IS NULL)
                           -- AND (qlh_s.list_header_id                           = p_in_csp_list_header_id OR p_in_csp_list_header_id IS NULL)
                           AND EXISTS (SELECT '1'
                                         FROM DUAL
                                        WHERE (TRUNC(NVL(qlh_s.end_date_active, SYSDATE+ 1))  < TRUNC(SYSDATE) OR qlh_s.active_flag = 'N') -- Matrix Header Leavel
                                          AND TRUNC(qlh_s.last_update_date)   >= p_to_date
                                       UNION
                                       SELECT '1'
                                         FROM qp_modifier_summary_v qll
                                        WHERE qlh_s.list_header_id             = qll.list_header_id
                                          AND TRUNC(NVL(qll.end_date_active, SYSDATE+ 1))  < TRUNC(SYSDATE) -- Matrix Line Leavel
                                          AND TRUNC(qll.last_update_date)     >= p_to_date
                                      )
            )
      UNION
      -----------------------------------------------------------------------------
      -- End-Date Excl CSP - Proces BPW
      -----------------------------------------------------------------------------
      SELECT DISTINCT xch_b.agreement_id         agreement_id
           , qlh_b.list_header_id               csp_list_header_id
           , xch_b.customer_id                  customer_id
           , xch_b.incompatability_group
           , xch_b.customer_site_id
           , 'Inactive_Excl_CSP'               SCENARIO
           , 'BPW'                             modifier_type
        FROM qp_list_headers_all               qlh_b
           , xxwc.xxwc_om_contract_pricing_hdr xch_b
       WHERE 1 = 1
         AND qlh_b.attribute10                 = 'Contract Pricing'
         AND xch_b.agreement_status            = 'APPROVED'
         AND TO_NUMBER (qlh_b.attribute14)     = xch_b.agreement_id
         AND qlh_b.active_flag                 = 'Y'
         AND xch_b.incompatability_group       = 'Best Price Wins'
         AND TRUNC(SYSDATE) BETWEEN TRUNC(NVL(qlh_b.start_date_active, SYSDATE -1))  AND TRUNC(NVL(qlh_b.end_date_active, SYSDATE+ 1))
         AND EXISTS (SELECT '1'
                          FROM qp_list_headers_all                             qlh_e
                             , xxwc.xxwc_om_contract_pricing_hdr               xch_e
                         WHERE 1 = 1
                           AND xch_e.customer_id                               = xch_b.customer_id
                           AND qlh_e.attribute10                               = 'Contract Pricing'
                           AND TO_NUMBER (qlh_e.attribute14)                   = xch_e.agreement_id
                           AND xch_e.incompatability_group                     = 'Exclusive'
                           AND (qlh_e.list_header_id                           = p_in_csp_list_header_id OR p_in_csp_list_header_id IS NULL)
                           AND EXISTS (SELECT '1'
                                         FROM DUAL
                                        WHERE (TRUNC(NVL(qlh_e.end_date_active, SYSDATE+ 1))  < TRUNC(SYSDATE) OR qlh_e.active_flag = 'N') -- Excl Header Level
                                          AND TRUNC(qlh_e.last_update_date)   >= p_to_date
                                       UNION
                                       SELECT '1'
                                         FROM qp_modifier_summary_v qll
                                        WHERE qlh_e.list_header_id             = qll.list_header_id
                                          AND TRUNC(NVL(qll.end_date_active, SYSDATE+ 1))  < TRUNC(SYSDATE)  -- Excl Line Level
                                          AND TRUNC(qll.last_update_date)     >= p_to_date
                                      )
            )
      UNION
      -----------------------------------------------------------------------------
      -- End-Date Excl CSP - Proces Matrix
      -----------------------------------------------------------------------------
      SELECT DISTINCT NULL                      agreement_id
           , qlh_m.list_header_id               csp_list_header_id
           , TO_NUMBER(qq_m.qualifier_attr_value) customer_id
           , 'MATRIX Price'                     incompatability_group
           , NULL                               customer_site_id
           , 'Inactive_Excl_CSP'                SCENARIO
           , 'MATRIX'                           modifier_type
        FROM qp_list_headers_all               qlh_m
           , qp_qualifiers                     qq_m
       WHERE 1 = 1
         AND qlh_m.attribute10                 = 'Segmented Price'
         AND qlh_m.active_flag                 = 'Y'
         AND qlh_m.list_header_id              = qq_m.list_header_id
         AND qq_m.qualifier_attribute          = 'QUALIFIER_ATTRIBUTE32'
         AND TRUNC(SYSDATE) BETWEEN TRUNC(NVL(qlh_m.start_date_active, SYSDATE -1))  AND TRUNC(NVL(qlh_m.end_date_active, SYSDATE+ 1))
         AND EXISTS (SELECT '1'
                          FROM qp_list_headers_all                             qlh_e
                             , xxwc.xxwc_om_contract_pricing_hdr               xch_e
                         WHERE 1 = 1
                           -- AND xch_e.customer_id                               = TO_NUMBER(qq_m.qualifier_attr_value) -- Version# 1.1
                           AND TO_CHAR(xch_e.customer_id)                      = qq_m.qualifier_attr_value -- Version# 1.1
                           AND qlh_e.attribute10                               = 'Contract Pricing'
                           AND TO_NUMBER (qlh_e.attribute14)                   = xch_e.agreement_id
                           AND xch_e.incompatability_group                     = 'Exclusive'
                           AND (qlh_e.list_header_id                           = p_in_csp_list_header_id OR p_in_csp_list_header_id IS NULL)
                           AND EXISTS (SELECT '1'
                                         FROM DUAL
                                        WHERE (TRUNC(NVL(qlh_e.end_date_active, SYSDATE+ 1))  < TRUNC(SYSDATE) OR qlh_e.active_flag = 'N') -- Excl Header Level
                                          AND TRUNC(qlh_e.last_update_date)   >= p_to_date
                                       UNION
                                       SELECT '1'
                                         FROM qp_modifier_summary_v qll
                                        WHERE qlh_e.list_header_id             = qll.list_header_id
                                          AND TRUNC(NVL(qll.end_date_active, SYSDATE+ 1))  < TRUNC(SYSDATE) -- Excl Line Level
                                          AND TRUNC(qll.last_update_date)     >= p_to_date
                                      )
            );

      ----------------------------------------------------------
      -- Qualifiers associated with the BPW Modifier
      ----------------------------------------------------------
      CURSOR cur_qualifiers (p_list_line_id   IN NUMBER)
          IS
      SELECT *
        FROM qp_qualifiers         qq
       WHERE 1 = 1
         AND qq.list_line_id    = p_list_line_id;

      ----------------------------------------------------------
      -- Qualifiers associated with the BPW Modifier
      ----------------------------------------------------------
      CURSOR cur_qual_rules (p_customer_id   IN NUMBER)
          IS
    SELECT qqr.name
             , qqr.description
             , qq.*
      FROM qp_qualifiers qq
         , qp_qualifier_rules    qqr
     WHERE 1 = 1
       AND qq.list_header_id           IS NULL
       AND qq.list_line_id             = -1
       AND qq.qualifier_rule_id        = qqr.qualifier_rule_id
       AND qq.qualifier_attribute      = 'QUALIFIER_ATTRIBUTE31'
       AND qq.qualifier_rule_id IN (SELECT qqc.qualifier_rule_id
                      FROM qp_qualifiers qqc
                     WHERE 1 = 1
                       AND qqc.qualifier_context        = 'CUSTOMER'
                       AND qqc.qualifier_attribute      = 'QUALIFIER_ATTRIBUTE11'
                       AND qqc.qualifier_attr_value     IN (SELECT hcsu.site_use_id
                                          FROM hz_cust_acct_sites_all            hcas
                                             , hz_cust_site_uses_all             hcsu
                                         WHERE 1=1 
                                           AND hcas.cust_account_id            = p_customer_id
                                           AND hcsu.cust_acct_site_id          = hcas.cust_acct_site_id
                                           AND hcsu.site_use_code              = 'SHIP_TO'
                                           AND hcas.status                     = 'A'
                                           AND hcsu.status                     = 'A')
                       AND qqc.comparison_operator_code = '='
                       AND qqc.list_header_id           IS NULL
                       AND qqc.list_line_id             = -1)
         ;

      --------------------------------------------------------------
      --- Define Local Variables here
      --------------------------------------------------------------
      l_qualifier_rec            QP_QUALIFIER_RULES_PUB.QUALIFIERS_REC_TYPE     := QP_QUALIFIER_RULES_PUB.G_MISS_QUALIFIERS_REC;
      l_modifier_line_Rec        QP_MODIFIERS_PUB.MODIFIERS_REC_TYPE            := QP_MODIFIERS_PUB.G_MISS_MODIFIERS_REC;
      l_pricing_att_rec          QP_MODIFIERS_PUB.PRICING_ATTR_REC_TYPE         := QP_MODIFIERS_PUB.G_MISS_PRICING_ATTR_REC;
      l_error_message_list       ERROR_HANDLER.ERROR_TBL_TYPE;
      l_MODIFIER_LIST_rec        QP_Modifiers_PUB.Modifier_List_Rec_Type;
      l_MODIFIER_LIST_val_rec    QP_Modifiers_PUB.Modifier_List_Val_Rec_Type;
      l_MODIFIERS_tbl            QP_Modifiers_PUB.Modifiers_Tbl_Type;
      l_MODIFIERS_val_tbl        QP_Modifiers_PUB.Modifiers_Val_Tbl_Type;
      l_QUALIFIERS_tbl           QP_Qualifier_Rules_PUB.Qualifiers_Tbl_Type;
      l_QUALIFIERS_val_tbl       QP_Qualifier_Rules_PUB.Qualifiers_Val_Tbl_Type;
      l_PRICING_ATTR_tbl         QP_Modifiers_PUB.Pricing_Attr_Tbl_Type;
      l_PRICING_ATTR_val_tbl     QP_Modifiers_PUB.Pricing_Attr_Val_Tbl_Type;
      lx_MODIFIER_LIST_rec       QP_Modifiers_PUB.Modifier_List_Rec_Type;
      lx_MODIFIER_LIST_val_rec   QP_Modifiers_PUB.Modifier_List_Val_Rec_Type;
      lx_MODIFIERS_tbl           QP_Modifiers_PUB.Modifiers_Tbl_Type;
      lx_MODIFIERS_val_tbl       QP_Modifiers_PUB.Modifiers_Val_Tbl_Type;
      lx_QUALIFIERS_tbl          QP_Qualifier_Rules_PUB.Qualifiers_Tbl_Type;
      lx_QUALIFIERS_val_tbl      QP_Qualifier_Rules_PUB.Qualifiers_Val_Tbl_Type;
      lx_PRICING_ATTR_tbl        QP_Modifiers_PUB.Pricing_Attr_Tbl_Type;
      lx_PRICING_ATTR_val_tbl    QP_Modifiers_PUB.Pricing_Attr_Val_Tbl_Type;
      l_qll_rec                  qp_list_lines%ROWTYPE;
      l_pra_rec                  qp_pricing_attributes%ROWTYPE;
      l_control_rec              QP_GLOBALS.Control_Rec_Type;
      l_mod_name                 VARCHAR2 (100)  := 'XXWC_QP_MODIFIER_WEB_ADI_PKG.IMPORT :' ;
      l_return_status            VARCHAR2(1);

      l_qualifier_rules_rec       qp_qualifier_rules_pub.Qualifier_Rules_Rec_Type  := QP_QUALIFIER_RULES_PUB.G_MISS_QUALIFIER_RULES_REC;
      lx_qualifier_rules_rec      qp_qualifier_rules_pub.Qualifier_Rules_Rec_Type;
  
      l_qualifier_rules_val_rec   qp_qualifier_rules_pub.Qualifier_Rules_Val_Rec_Type;
      lx_qualifier_rules_val_rec  qp_qualifier_rules_pub.Qualifier_Rules_Val_Rec_Type;

      l_Msg_Data                 Varchar2(2000);
      l_Msg_Count                Number ;
      L_List_Header_ID           Number;
      l_List_Line_Id             Number ;
      l_Pricing_Attribute_ID     Number ;
      l_Qualifier_ID             Number ;
      i                          Number ;
      l_Line_COunt               Number ;
      l_ql_count                 NUMBER;

      C_STATUS_VALIDATE          CONSTANT   VARCHAR2(1) := 'V';
      C_STATUS_ERROR             CONSTANT   VARCHAR2(1) := 'E';
      C_STATUS_PROCESSED         CONSTANT   VARCHAR2(1) := 'P' ;

      l_error_message            VARCHAR2 (2000);
      l_exception                EXCEPTION;

      l_errbuf                   VARCHAR2 (2000);
      l_retcode                  NUMBER;

      l_sec                      VARCHAR2 (100);
      l_ln_count                 NUMBER;
      l_pa_Count                 NUMBER;
      l_excludr_line_id          NUMBER;
      l_prev_excludr_line_id     NUMBER;
      l_prev_customer_id         NUMBER;
      l_qual_grp_num             NUMBER;
      l_qualifier_precedence     NUMBER;
      l_qual_exists              VARCHAR2 (1);
      l_qualifier_rule_id        NUMBER;
      l_addnl_qual               NUMBER;

      l_process_date             DATE := TO_DATE(p_process_date,'DD-MON-YYYY');
      l_seg_list_header_id       NUMBER;

   BEGIN
      fnd_file.put_line (fnd_file.LOG, '----------------------------------------------------------------------------------');
      fnd_file.put_line (fnd_file.LOG, 'Start of Procedure : main');
      fnd_file.put_line (fnd_file.LOG, '----------------------------------------------------------------------------------');

      fnd_file.put_line (fnd_file.LOG, 'p_process_date - '||p_process_date);
      fnd_file.put_line (fnd_file.LOG, 'l_process_date - '||l_process_date);

      -- ############################################################################## --
      ----------------------------------------------------------------------------------
      -- Check and Create Qualifier Group / Rule
      ----------------------------------------------------------------------------------
      -- ############################################################################## --

      fnd_file.put_line (fnd_file.LOG, '##############################################################################');
      fnd_file.put_line (fnd_file.LOG, ' Create Qualifier Rules > Start '||TO_CHAR(SYSDATE,'DD-MON-YY HH24:MI:SS'));
      fnd_file.put_line (fnd_file.LOG, '##############################################################################');

      FOR rec_exl IN cur_exl(l_process_date) LOOP

         --------------------------------------------------------------
         -- Add Qualifiers - Exclusive Master
         --------------------------------------------------------------
         IF rec_exl.customer_site_id IS NOT NULL THEN -- IF CSP is at Site Level

               fnd_file.put_line (fnd_file.LOG, ' ************************************************************************************ ');
               fnd_file.put_line (fnd_file.LOG, ' ************************************************************************************ ');
               fnd_file.put_line (fnd_file.LOG, ' Qualifier Rules validation for CustomerId - '||rec_exl.customer_id);
               fnd_file.put_line (fnd_file.LOG, ' ************************************************************************************ ');
               fnd_file.put_line (fnd_file.LOG, ' ************************************************************************************ ');

               IF NVL(l_prev_customer_id,-1) != rec_exl.customer_id THEN -- Perform this once per CusotmerId

                  FOR rec_site IN cur_site(rec_exl.customer_id) LOOP
                  BEGIN

                     fnd_file.put_line (fnd_file.LOG, 'CUSTOMER_SITE_ID - '|| rec_site.customer_site_id);
                     fnd_file.put_line (fnd_file.LOG, 'l_qual_grp_num - '|| l_qual_grp_num);
                     l_QUALIFIERS_tbl                       := QP_MODIFIERS_PUB.G_MISS_QUALIFIERS_TBL;
                     i              := 0;

                     l_qualifier_rule_id := NULL;
                     l_qual_grp_num      := NULL;

                     --------------------------------------------------------------
                     -- Check if Qualifier Rule exists
                     --------------------------------------------------------------
                     BEGIN
                        SELECT qqc.qualifier_rule_id
                             , MAX(qqc.qualifier_grouping_no)
                          INTO l_qualifier_rule_id
                             , l_qual_grp_num
                          FROM qp_qualifiers qqc
                         WHERE qqc.qualifier_context        = 'CUSTOMER'
                           AND qqc.qualifier_attribute      = 'QUALIFIER_ATTRIBUTE11' 
                           AND qqc.qualifier_attr_value     = rec_site.customer_site_id
                           AND qqc.comparison_operator_code = '='
                           AND qqc.list_header_id           IS NULL
                           AND qqc.list_line_id             = -1
                         GROUP BY qqc.qualifier_rule_id
                           ;

                         fnd_file.put_line (fnd_file.LOG, ' Qualifier Rules exists l_qualifier_rule_id - '||l_qualifier_rule_id);
                         fnd_file.put_line (fnd_file.LOG, ' l_qual_grp_num - '||l_qual_grp_num);
                     EXCEPTION
                     WHEN OTHERS THEN
                        fnd_file.put_line (fnd_file.LOG, ' Qualifier Rules does not exist');
                     END;

                     IF l_qualifier_rule_id IS NOT NULL THEN 

                        --------------------------------------------------------------
                        -- Check if all the items on Exclusive CSP are on Qualifier
                        --------------------------------------------------------------
                        l_addnl_qual := 0;
                        BEGIN
                           SELECT COUNT(DISTINCT qll.product_attr||'_'||qll.product_attr_val)
                             INTO l_addnl_qual
                             FROM qp_modifier_summary_v             qll -- CSP Modifier
                                , qp_list_headers_all               qlh
                                , xxwc.xxwc_om_contract_pricing_hdr xch2
                            WHERE 1 = 1
                              AND xch2.customer_site_id              = rec_site.customer_site_id
                              AND xch2.incompatability_group         = 'Exclusive'
                              AND xch2.agreement_status              = 'APPROVED'
                              AND TO_NUMBER (qlh.attribute14)        = xch2.agreement_id
                              AND qll.list_header_id                 = qlh.list_header_id
                              AND qlh.active_flag                    = 'Y'
                              AND TRUNC(SYSDATE) BETWEEN TRUNC(NVL(qlh.start_date_active, SYSDATE -1))  AND TRUNC(NVL(qlh.end_date_active, SYSDATE+ 1))
                              AND TRUNC(SYSDATE) BETWEEN TRUNC(NVL(qll.start_date_active, SYSDATE -1))  AND TRUNC(NVL(qll.end_date_active, SYSDATE+ 1))
                              AND NOT EXISTS (SELECT '1' 
                                                FROM qp_qualifiers              qqc
                                               WHERE 1 = 1
                                                 AND qqc.qualifier_rule_id    = l_qualifier_rule_id
                                                 AND qqc.qualifier_attribute  = 'QUALIFIER_ATTRIBUTE31' 
                                                 AND DECODE(qqc.qualifier_context,'ITEM_NUMBER','PRICING_ATTRIBUTE1','ITEM_CAT','PRICING_ATTRIBUTE2')  = qll.product_attr
                                                 AND qqc.qualifier_attr_value = qll.product_attr_val
                                                 );
                        EXCEPTION
                        WHEN OTHERS THEN
                           l_addnl_qual := 0;
                        END;

                        IF l_addnl_qual > 0 THEN
                           l_qualifier_rules_rec                        := QP_QUALIFIER_RULES_PUB.G_MISS_QUALIFIER_RULES_REC;
                           l_qualifier_rules_rec.operation              := QP_GLOBALS.G_OPR_UPDATE;
                           l_qualifier_rules_rec.qualifier_rule_id      := l_qualifier_rule_id;
                        ELSE
                           fnd_file.put_line (fnd_file.LOG, 'All qualifiers exist. No change - do nothing');
                           RAISE l_exception;
                        END IF;
                     ELSE  -- IF l_qualifier_rule_id IS NOT NULL THEN -- Qualifier Rule does not exist

                        --------------------------------------------------------------
                        -- Derive Maximum Grouping Number for the Customer
                        --------------------------------------------------------------
                        l_qual_grp_num := NULL;
                        BEGIN
                           SELECT NVL(MAX(QUALIFIER_GROUPING_NO), 0)
                             INTO l_qual_grp_num
                             FROM qp_qualifiers qqc
                            WHERE qqc.qualifier_context        = 'CUSTOMER'
                              AND qqc.qualifier_attribute      = 'QUALIFIER_ATTRIBUTE11' 
                              AND qqc.qualifier_attr_value     IN (SELECT DISTINCT hcsu.site_use_id
                                                                     FROM hz_cust_acct_sites_all            hcas
                                                                        , hz_cust_site_uses_all             hcsu
                                                                    WHERE 1=1 
                                                                      AND hcas.cust_account_id            = rec_exl.customer_id
                                                                      AND hcsu.cust_acct_site_id          = hcas.cust_acct_site_id
                                                                      AND hcsu.site_use_code              = 'SHIP_TO'
                                                                      AND hcas.status                     = 'A'
                                                                      AND hcsu.status                     = 'A')
                              AND qqc.comparison_operator_code = '='
                              AND qqc.list_header_id           IS NULL
                              AND qqc.list_line_id             = -1;
                        EXCEPTION
                        WHEN OTHERS THEN
                          l_qual_grp_num := 1;
                        END;

                        l_qual_grp_num                               := l_qual_grp_num + 1;
                        l_qualifier_rules_rec.operation              := QP_GLOBALS.G_OPR_CREATE;
                        l_qualifier_rules_rec.qualifier_rule_id      := FND_API.G_MISS_NUM;

                        --------------------------------------------------------------
                        -- Create Qualifier Group / Rule
                        --------------------------------------------------------------
                        i := i+1;
                        l_QUALIFIERS_tbl(i).OPERATION                      := QP_GLOBALS.G_OPR_CREATE;
                        l_QUALIFIERS_tbl(i).QUALIFIER_ID                   := FND_API.G_MISS_NUM;
                        l_QUALIFIERS_tbl(i).EXCLUDER_FLAG                  := 'N';
                        l_QUALIFIERS_tbl(i).QUALIFIER_GROUPING_NO          := l_qual_grp_num;
                        l_QUALIFIERS_tbl(i).QUALIFIER_CONTEXT              := 'CUSTOMER';
                        l_QUALIFIERS_tbl(i).QUALIFIER_ATTRIBUTE            := 'QUALIFIER_ATTRIBUTE11';
                        l_QUALIFIERS_tbl(i).COMPARISON_OPERATOR_CODE       := '=';
                        l_QUALIFIERS_tbl(i).QUALIFIER_ATTR_VALUE           := rec_site.customer_site_id;
                        l_QUALIFIERS_tbl(i).QUAL_ATTR_VALUE_FROM_NUMBER    := rec_site.customer_site_id;
                     END IF; -- IF l_qualifier_rule_id IS NOT NULL THEN 

                     -- l_qualifier_rules_rec.qualifier_rule_id            := FND_API.G_MISS_NUM;
                     l_qualifier_rules_rec.name                         := rec_exl.name||'_'||rec_site.customer_site_id;
                     l_qualifier_rules_rec.description                  := rec_exl.name||'_'||rec_site.customer_site_id;


                        --------------------------------------------------------------
                        -- Loop for all Items / Categories that are not on Qualifier Rule
                        --------------------------------------------------------------
                        FOR rec IN (SELECT DISTINCT qll.product_attr, qll.product_attr_val
                                      FROM qp_modifier_summary_v             qll -- CSP Modifier
                                         , qp_list_headers_all               qlh
                                         , xxwc.xxwc_om_contract_pricing_hdr xch2
                                     WHERE 1 = 1
                                       AND xch2.customer_site_id              = rec_site.customer_site_id
                                       AND xch2.incompatability_group         = 'Exclusive'
                                       AND xch2.agreement_status              = 'APPROVED'
                                       AND TO_NUMBER (qlh.attribute14)        = xch2.agreement_id
                                       AND qll.list_header_id                 = qlh.list_header_id
                                       AND qlh.active_flag                    = 'Y'
                                       AND TRUNC(SYSDATE) BETWEEN TRUNC(NVL(qlh.start_date_active, SYSDATE -1))  AND TRUNC(NVL(qlh.end_date_active, SYSDATE+ 1))
                                       AND TRUNC(SYSDATE) BETWEEN TRUNC(NVL(qll.start_date_active, SYSDATE -1))  AND TRUNC(NVL(qll.end_date_active, SYSDATE+ 1))
                                       AND NOT EXISTS (SELECT '1' 
                                                         FROM qp_qualifiers              qqc
                                                        WHERE 1 = 1
                                                          AND qqc.qualifier_rule_id    = NVL(l_qualifier_rule_id, qqc.qualifier_rule_id)
                                                          AND qqc.qualifier_attribute  = 'QUALIFIER_ATTRIBUTE31'
                                                          AND DECODE(qqc.qualifier_context,'ITEM_NUMBER','PRICING_ATTRIBUTE1','ITEM_CAT','PRICING_ATTRIBUTE2')  = qll.product_attr
                                                          AND qqc.qualifier_attr_value = qll.product_attr_val)
                                       ) LOOP

                            i := i+1;

                            l_QUALIFIERS_tbl(i).OPERATION                 := QP_GLOBALS.G_OPR_CREATE; 
                            l_QUALIFIERS_tbl(i).QUALIFIER_ID              := FND_API.G_MISS_NUM;
                            l_QUALIFIERS_tbl(i).EXCLUDER_FLAG             := 'N';
                            l_QUALIFIERS_tbl(i).QUALIFIER_GROUPING_NO     := l_qual_grp_num;
                            l_QUALIFIERS_tbl(i).QUALIFIER_ATTRIBUTE       := 'QUALIFIER_ATTRIBUTE31';
                            l_QUALIFIERS_tbl(i).COMPARISON_OPERATOR_CODE  := 'NOT =';
                            l_QUALIFIERS_tbl(i).QUALIFIER_ATTR_VALUE      := rec.product_attr_val;

                            IF l_qualifier_rule_id IS NOT NULL THEN
                               l_QUALIFIERS_tbl(i).qualifier_rule_id      := l_qualifier_rule_id;
                            END IF;

                            IF rec.product_attr = 'PRICING_ATTRIBUTE1' THEN
                               l_QUALIFIERS_tbl(i).QUALIFIER_CONTEXT      :=  'ITEM_NUMBER' ;
                            ELSIF rec.product_attr = 'PRICING_ATTRIBUTE2' THEN
                               l_QUALIFIERS_tbl(i).QUALIFIER_CONTEXT      :=  'ITEM_CAT' ;
                            END IF;

                            fnd_file.put_line (fnd_file.LOG, 'rec.product_attr_val - '|| rec.product_attr_val);
                            fnd_file.put_line (fnd_file.LOG, 'l_qualifiers_tbl(i).qualifier_context - '|| l_qualifiers_tbl(i).qualifier_context);

                        END LOOP;

                     -- END IF;

                     qp_qualifier_rules_pub.Process_Qualifier_Rules(
                         p_api_version_number            =>1.0
                         ,p_init_msg_list                => fnd_api.G_FALSE
                         ,p_return_values                => fnd_api.G_FALSE
                         ,p_commit                       => fnd_api.G_FALSE
                         ,x_return_status                => l_return_status
                         ,x_msg_count                    => l_msg_count
                         ,x_msg_data                     => l_msg_data
                         ,p_QUALIFIER_RULES_rec          => l_qualifier_rules_rec
                         ,p_QUALIFIER_RULES_val_rec      => l_qualifier_rules_val_rec
                         ,p_QUALIFIERS_tbl               => l_QUALIFIERS_tbl
                         ,p_QUALIFIERS_val_tbl           => l_QUALIFIERS_val_tbl
                         ,x_QUALIFIER_RULES_rec          => lx_qualifier_rules_rec
                         ,x_QUALIFIER_RULES_val_rec      => lx_qualifier_rules_val_rec
                         ,x_QUALIFIERS_tbl               => lx_QUALIFIERS_tbl
                         ,x_QUALIFIERS_val_tbl           => lx_QUALIFIERS_val_tbl
                     );


                     IF l_return_status = fnd_api.G_RET_STS_SUCCESS THEN
                        COMMIT;
                        fnd_file.put_line (fnd_file.LOG, ' Success - '||l_qualifier_rules_rec.name);
                     ELSE
                        -- ROLLBACK;
                        COMMIT;
                        -- fnd_file.put_line (fnd_file.LOG, 'Error creating Process_Qualifier_Rules is '|| l_msg_data ||' - ' || l_msg_count);
                        fnd_file.put_line (fnd_file.LOG, 'After Error Msg ');

                        FOR I IN 1 .. l_msg_count LOOP
                           l_msg_data := oe_msg_pub.get( p_msg_index => i,p_encoded => 'F');
                           fnd_file.put_line (fnd_file.LOG, 'err msg ' || i ||'is:  ' || l_msg_data);
                        END LOOP;
                     END IF; -- l_return_status = fnd_api.G_RET_STS_SUCCESS THEN

                  EXCEPTION
                  WHEN l_exception THEN
                    NULL;
                  END;
                  END LOOP; -- rec_site


               ELSE
                  l_qual_grp_num := l_qual_grp_num + 1;
                  fnd_file.put_line (fnd_file.LOG, 'l_qual_grp_num - '|| l_qual_grp_num);
                  i := 1 + 1;

                  BEGIN
                     SELECT NVL(MAX(QUALIFIER_GROUPING_NO), 0)
                       INTO l_qual_grp_num
                       FROM qp_qualifiers qqc
                      WHERE qqc.qualifier_context        = 'CUSTOMER'
                        AND qqc.qualifier_attribute      = 'QUALIFIER_ATTRIBUTE11' 
                        AND qqc.qualifier_attr_value     = rec_exl.customer_site_id
                        AND qqc.comparison_operator_code = '='
                        AND qqc.list_header_id           IS NULL
                        AND qqc.list_line_id             = -1;
                  EXCEPTION
                  WHEN OTHERS THEN
                    l_qual_grp_num := 1;
                  END;

               END IF; -- NVL(l_prev_customer_id,-1) != rec_exl.customer_id THEN

               --------------------------------------------------------------
               -- Create Qualifier Group / Rule
               --------------------------------------------------------------
               l_qualifier_rules_rec.operation               := QP_GLOBALS.G_OPR_CREATE;
               l_qualifier_rules_rec.qualifier_rule_id       := FND_API.G_MISS_NUM;
               l_qualifier_rules_rec.name                    := rec_exl.name||'_'||rec_exl.customer_site_id; -- 'Group_'||rec_exl.customer_site_id||'_'||l_qual_grp_num;
               l_qualifier_rules_rec.description             := rec_exl.name||'_'||rec_exl.customer_site_id; -- 'Group_'||rec_exl.customer_site_id||'_'||l_qual_grp_num;

               l_QUALIFIERS_tbl(i).OPERATION                 := QP_GLOBALS.G_OPR_CREATE; 
               l_QUALIFIERS_tbl(i).QUALIFIER_ID              := FND_API.G_MISS_NUM;
               l_QUALIFIERS_tbl(i).EXCLUDER_FLAG             := 'N';
               l_QUALIFIERS_tbl(i).QUALIFIER_GROUPING_NO     := l_qual_grp_num;
               l_QUALIFIERS_tbl(i).QUALIFIER_CONTEXT         := 'CUSTOMER';
               l_QUALIFIERS_tbl(i).QUALIFIER_ATTRIBUTE       := 'QUALIFIER_ATTRIBUTE11';
               l_QUALIFIERS_tbl(i).COMPARISON_OPERATOR_CODE  := '=';
               l_QUALIFIERS_tbl(i).QUALIFIER_ATTR_VALUE      := rec_exl.customer_site_id;
               l_QUALIFIERS_tbl(i).QUAL_ATTR_VALUE_FROM_NUMBER      := rec_exl.customer_site_id;


               FOR rec IN (SELECT *
                             FROM qp_modifier_summary_v qll -- CSP Modifier
                            WHERE 1 = 1
                              AND qll.list_header_id             = rec_exl.csp_list_header_id
                              AND TRUNC(qll.last_update_date)   >= l_process_date
                              AND TRUNC(SYSDATE) BETWEEN TRUNC(NVL(qll.start_date_active, SYSDATE -1))  AND TRUNC(NVL(qll.end_date_active, SYSDATE+ 1))
                         ) LOOP

                   i := i+1;

                   l_QUALIFIERS_tbl(i).OPERATION                 := QP_GLOBALS.G_OPR_CREATE; 
                   l_QUALIFIERS_tbl(i).QUALIFIER_ID              := FND_API.G_MISS_NUM;
                   l_QUALIFIERS_tbl(i).EXCLUDER_FLAG             := 'N';
                   l_QUALIFIERS_tbl(i).QUALIFIER_GROUPING_NO     := l_qual_grp_num;
                   l_QUALIFIERS_tbl(i).QUALIFIER_ATTRIBUTE       := 'QUALIFIER_ATTRIBUTE31';
                   l_QUALIFIERS_tbl(i).COMPARISON_OPERATOR_CODE  := 'NOT =';
                   l_QUALIFIERS_tbl(i).QUALIFIER_ATTR_VALUE      := rec.product_attr_val;

                   IF rec.product_attr = 'PRICING_ATTRIBUTE1' THEN
                      l_QUALIFIERS_tbl(i).QUALIFIER_CONTEXT      :=  'ITEM_NUMBER' ;
                   ELSIF rec.product_attr = 'PRICING_ATTRIBUTE2' THEN
                      l_QUALIFIERS_tbl(i).QUALIFIER_CONTEXT      :=  'ITEM_CAT' ;
                   END IF;
               END LOOP;

               qp_qualifier_rules_pub.Process_Qualifier_Rules(
                   p_api_version_number            =>1.0
                   ,p_init_msg_list                => fnd_api.G_FALSE
                   ,p_return_values                => fnd_api.G_FALSE
                   ,p_commit                       => fnd_api.G_FALSE
                   ,x_return_status                => l_return_status
                   ,x_msg_count                    => l_msg_count
                   ,x_msg_data                     => l_msg_data
                   ,p_QUALIFIER_RULES_rec          => l_qualifier_rules_rec
                   ,p_QUALIFIER_RULES_val_rec      => l_qualifier_rules_val_rec
                   ,p_QUALIFIERS_tbl               => l_QUALIFIERS_tbl
                   ,p_QUALIFIERS_val_tbl           => l_QUALIFIERS_val_tbl
                   ,x_QUALIFIER_RULES_rec          => lx_qualifier_rules_rec
                   ,x_QUALIFIER_RULES_val_rec      => lx_qualifier_rules_val_rec
                   ,x_QUALIFIERS_tbl               => lx_QUALIFIERS_tbl
                   ,x_QUALIFIERS_val_tbl           => lx_QUALIFIERS_val_tbl
               );


               IF l_return_status = fnd_api.G_RET_STS_SUCCESS THEN
                  COMMIT;
                  fnd_file.put_line (fnd_file.LOG, ' Success - '||l_qualifier_rules_rec.name);
               ELSE
                  -- ROLLBACK;
                  COMMIT;
                  -- fnd_file.put_line (fnd_file.LOG, 'Error creating Process_Qualifier_Rules is '|| l_msg_data ||' - ' || l_msg_count);
                  fnd_file.put_line (fnd_file.LOG, 'After Error Msg ');

                  --FOR I IN 1 .. l_msg_count LOOP
                  --   l_msg_data := oe_msg_pub.get( p_msg_index => i,p_encoded => 'F');
                  --   fnd_file.put_line (fnd_file.LOG, 'err msg ' || i ||'is:  ' || l_msg_data);
                  --END LOOP;

               END IF; -- l_return_status = fnd_api.G_RET_STS_SUCCESS THEN

            -- END IF; -- IF l_qualifier_rule_id IS NULL THEN
         l_prev_customer_id := rec_exl.customer_id;
         END IF; -- IF rec_exl.customer_site_id IS NOT NULL THEN

      END LOOP;

      fnd_file.put_line (fnd_file.LOG, '##############################################################################');
      fnd_file.put_line (fnd_file.LOG, ' MATRIX Modifier Header - Cursor > Start '||TO_CHAR(SYSDATE,'DD-MON-YY HH24:MI:SS'));
      fnd_file.put_line (fnd_file.LOG, '##############################################################################');


      -- ############################################################################## --
      ----------------------------------------------------------------------------------
      -- MATRIX Modifier Header - Cursor
      ----------------------------------------------------------------------------------
      -- ############################################################################## --
      FOR rec_exl IN cur_exl(l_process_date)
      LOOP
         l_sec := 'MATRIX Modifier Header - Cursor';
         fnd_file.put_line (fnd_file.LOG, l_sec);

         l_error_message := ' ';
         l_MODIFIER_LIST_rec                    := QP_MODIFIERS_PUB.G_MISS_MODIFIER_LIST_REC;
         

         l_ln_count                             := 0;

         l_MODIFIER_LIST_rec.List_Header_ID          := rec_exl.seg_list_header_id;
         l_MODIFIER_LIST_rec.operation               := QP_GLOBALS.G_OPR_UPDATE;

         ----------------------------------------------------------------------------------
         -- Update CSP lines as Exclusions
         ----------------------------------------------------------------------------------
         l_ln_count := 0;
         l_MODIFIERS_Tbl                        := QP_MODIFIERS_PUB.G_MISS_MODIFIERS_TBL;

         l_pa_Count := 0;
         l_PRICING_ATTR_Tbl                     := QP_MODIFIERS_PUB.G_MISS_PRICING_ATTR_TBL;

         l_prev_excludr_line_id := 0;
         l_QUALIFIERS_tbl                       := QP_MODIFIERS_PUB.G_MISS_QUALIFIERS_TBL;

         FOR Mod_CSP_Line_Rec IN cur_csp_mod_line(rec_exl.csp_list_header_id, 'N') LOOP

             l_sec := 'Loop Matrix Modifier CSP Lines';
             fnd_file.put_line (fnd_file.LOG, l_sec);
             
             fnd_file.put_line (fnd_file.LOG, 'csp_list_header_id - '||rec_exl.csp_list_header_id);

             l_excludr_line_id                      := NULL;

             ----------------------------------------------------------------------------------
             -- For each Excluder Line
             ----------------------------------------------------------------------------------
             FOR rec_excl_line IN cur_excluder_line(mod_csp_line_rec.product_attr
                                                      , mod_csp_line_rec.product_attr_val
                                                      , rec_exl.seg_list_header_id) LOOP
                 l_excludr_line_id := rec_excl_line.list_line_id;
--                 fnd_file.put_line (fnd_file.LOG, 'l_excludr_line_id - '||l_excludr_line_id);
                 l_ql_Count                := 0;

                 ----------------------------------------------------------------------------------
                 -- Add Qualifiers to Modifier Lines
                 ----------------------------------------------------------------------------------
                 FOR rec_qq IN (SELECT *
                                  FROM qp_qualifiers qq
                                 WHERE 1 = 1
                                   AND qq.list_header_id           IS NULL
                                   AND qq.list_line_id             = -1
                                   AND NOT EXISTS (SELECT qqc.qualifier_rule_id
                                                     FROM qp_qualifiers qqc
                                                    WHERE 1 = 1
                                                      AND qqc.list_header_id           = rec_exl.seg_list_header_id
                                                      AND qqc.list_line_id             = l_excludr_line_id
                                                      AND qqc.qualifier_grouping_no    = qq.qualifier_grouping_no -- %%%%%
                                                      AND qqc.qualifier_context        = qq.qualifier_context
                                                      AND qqc.qualifier_attribute      = qq.qualifier_attribute
                                                      AND qqc.qualifier_attr_value     = qq.qualifier_attr_value
                                                      AND qqc.comparison_operator_code = qq.comparison_operator_code)
                                   AND qq.qualifier_rule_id IN (SELECT qqc.qualifier_rule_id
                                                                  FROM qp_qualifiers qqc
                                                                 WHERE 1 = 1
                                                                   AND qqc.qualifier_context        = 'CUSTOMER'
                                                                   AND qqc.qualifier_attribute      = 'QUALIFIER_ATTRIBUTE11' 
                                                                   AND qqc.qualifier_attr_value     IN (SELECT hcsu.site_use_id
                                                                                                          FROM hz_cust_acct_sites_all            hcas
                                                                                                             , hz_cust_site_uses_all             hcsu
                                                                                                         WHERE 1=1 
                                                                                                           AND hcas.cust_account_id            = rec_exl.customer_id
                                                                                                           AND hcsu.cust_acct_site_id          = hcas.cust_acct_site_id
                                                                                                           AND hcsu.site_use_code              = 'SHIP_TO'
                                                                                                           AND hcas.status                     = 'A'
                                                                                                           AND hcsu.status                     = 'A')
                                                                   AND qqc.comparison_operator_code = '='
                                                                   AND qqc.list_header_id           IS NULL
                                                                   AND qqc.list_line_id             = -1))
                 LOOP

                    IF rec_qq.qualifier_context        = 'CUSTOMER' THEN
                       --------------------------------------------------------------
                       -- Add Ship-To Qualifier > Start
                       --------------------------------------------------------------
                       l_qualifier_rec                            := QP_QUALIFIER_RULES_PUB.G_MISS_QUALIFIERS_REC;
                       l_ql_Count                                 := l_ql_Count + 1;
                       -- l_qualifier_precedence                     := l_qualifier_precedence + 1;

                       Fnd_File.Put_Line ( Fnd_File.LOG, 'Add Ship-To Qualifier - Grouping# =>' || rec_qq.qualifier_grouping_no);
                       Fnd_File.Put_Line ( Fnd_File.LOG, 'l_ql_Count =>' || l_ql_Count);

                       l_qualifier_rec.LIST_HEADER_ID             :=  rec_exl.seg_list_header_id;
                       l_qualifier_rec.LIST_LINE_ID               :=  l_excludr_line_id;
                       l_qualifier_rec.QUALIFIER_ID               :=  qp_qualifiers_s.nextval;
                       l_qualifier_rec.CREATED_FROM_RULE_ID       :=  rec_qq.qualifier_rule_id;
                       l_qualifier_rec.QUALIFIER_GROUPING_NO      :=  rec_qq.qualifier_grouping_no;
                       l_qualifier_rec.QUALIFIER_CONTEXT          :=  rec_qq.QUALIFIER_CONTEXT;
                       l_qualifier_rec.QUALIFIER_ATTRIBUTE        :=  rec_qq.QUALIFIER_ATTRIBUTE;
                       l_qualifier_rec.QUALIFIER_ATTR_VALUE       :=  rec_qq.QUALIFIER_ATTR_VALUE;
                       l_qualifier_rec.COMPARISON_OPERATOR_CODE   :=  rec_qq.COMPARISON_OPERATOR_CODE;

                       l_qualifier_rec.QUALIFIER_PRECEDENCE       :=  rec_qq.QUALIFIER_PRECEDENCE;
                       l_qualifier_rec.Excluder_Flag              := 'N';
                       l_qualifier_rec.Operation                  := QP_GLOBALS.G_OPR_CREATE;

                       l_QUALIFIERS_tbl(l_ql_Count)               := l_qualifier_rec;

                       --------------------------------------------------------------
                       -- Add Ship-To Qualifier < End
                       --------------------------------------------------------------
                    ELSE
                      IF is_valid_qualifier(rec_qq.qualifier_context, rec_qq.qualifier_attr_value,l_excludr_line_id ) = 'Y' THEN
                         --------------------------------------------------------------
                         -- Add Item/Category Level Qualifier > Start
                         --------------------------------------------------------------
                         l_qualifier_rec                            := QP_QUALIFIER_RULES_PUB.G_MISS_QUALIFIERS_REC;
                         l_ql_Count                                 := l_ql_Count + 1;
                         -- l_qualifier_precedence                     := l_qualifier_precedence + 1;

                         Fnd_File.Put_Line ( Fnd_File.LOG, 'Add Ship-To Qualifier - Grouping# =>' || rec_qq.qualifier_grouping_no);
                         Fnd_File.Put_Line ( Fnd_File.LOG, 'l_ql_Count =>' || l_ql_Count);

                         l_qualifier_rec.LIST_HEADER_ID             :=  rec_exl.seg_list_header_id;
                         l_qualifier_rec.LIST_LINE_ID               :=  l_excludr_line_id;
                         l_qualifier_rec.QUALIFIER_ID               :=  qp_qualifiers_s.nextval;
                         l_qualifier_rec.CREATED_FROM_RULE_ID       :=  rec_qq.qualifier_rule_id;
                         l_qualifier_rec.QUALIFIER_GROUPING_NO      :=  rec_qq.qualifier_grouping_no;
                         l_qualifier_rec.QUALIFIER_CONTEXT          :=  rec_qq.QUALIFIER_CONTEXT;
                         l_qualifier_rec.QUALIFIER_ATTRIBUTE        :=  rec_qq.QUALIFIER_ATTRIBUTE;
                         l_qualifier_rec.QUALIFIER_ATTR_VALUE       :=  rec_qq.QUALIFIER_ATTR_VALUE;
                         l_qualifier_rec.COMPARISON_OPERATOR_CODE   :=  rec_qq.COMPARISON_OPERATOR_CODE;

                         l_qualifier_rec.QUALIFIER_PRECEDENCE       :=  rec_qq.QUALIFIER_PRECEDENCE;
                         l_qualifier_rec.Excluder_Flag              := 'N';
                         l_qualifier_rec.Operation                  := QP_GLOBALS.G_OPR_CREATE;

                         l_QUALIFIERS_tbl(l_ql_Count)               := l_qualifier_rec;

                         --------------------------------------------------------------
                         -- Add Item/Category Level Qualifier < End
                         --------------------------------------------------------------
                      END IF; -- IF is_valid_qualifier(rec_qq.qualifier_context, rec_qq.qualifier_attr_value,l_excludr_line_id ) = 'Y' THEN
                    END IF; -- IF rec_qq.qualifier_context        = 'CUSTOMER' THEN
                 END LOOP; -- rec_qq

                 ----------------------------------------------------------------------------------
                 -- Add Modifier Lines
                 ----------------------------------------------------------------------------------
                 IF l_ql_Count > 0 THEN
                    QP_Modifiers_PUB.Process_Modifiers
                       ( p_api_version_number    => 1.0
                       , p_init_msg_list         => FND_API.G_FALSE
                       , p_return_values         => FND_API.G_FALSE
                       , p_commit                => FND_API.G_TRUE
                       , x_return_status         => l_return_status
                       , x_msg_count             => l_msg_count
                       , x_msg_data              => l_msg_data
                       , p_MODIFIER_LIST_rec     => l_MODIFIER_LIST_rec
                       , p_MODIFIERS_tbl         => l_MODIFIERS_tbl
                       , p_QUALIFIERS_tbl        => l_QUALIFIERS_tbl
                       , p_PRICING_ATTR_tbl      => l_PRICING_ATTR_tbl
                       , x_MODIFIER_LIST_rec     => lx_MODIFIER_LIST_rec
                       , x_MODIFIER_LIST_val_rec => lx_MODIFIER_LIST_val_rec
                       , x_MODIFIERS_tbl         => lx_MODIFIERS_tbl
                       , x_MODIFIERS_val_tbl     => lx_MODIFIERS_val_tbl
                       , x_QUALIFIERS_tbl        => lx_QUALIFIERS_tbl
                       , x_QUALIFIERS_val_tbl    => lx_QUALIFIERS_val_tbl
                       , x_PRICING_ATTR_tbl      => lx_PRICING_ATTR_tbl
                       , x_PRICING_ATTR_val_tbl  => lx_PRICING_ATTR_val_tbl );
                    COMMIT;

                    IF l_return_status = fnd_api.G_RET_STS_SUCCESS THEN
                       COMMIT;
                    ELSE
                       -- ROLLBACK;
                       COMMIT;
                       -- fnd_file.put_line (fnd_file.LOG, 'Error Add Modifier Lines - '|| l_msg_data ||' - ' || l_msg_count);
                       fnd_file.put_line (fnd_file.LOG, 'After Error Msg ');

                       --FOR I IN 1 .. l_msg_count LOOP
                       --   l_msg_data := oe_msg_pub.get( p_msg_index => i,p_encoded => 'F');
                       --   fnd_file.put_line (fnd_file.LOG, 'err msg ' || i ||'is:  ' || l_msg_data);
                       --END LOOP;
                    END IF;
                    l_QUALIFIERS_tbl                       := QP_MODIFIERS_PUB.G_MISS_QUALIFIERS_TBL;
                 END IF; -- IF l_ql_Count > 0 THEN

         --------------------------------------------------------------
         -- If Exclusive CSP is at Master Level - Add exclusions to 
         -- Segment Modifier > Start
         --------------------------------------------------------------
         IF rec_exl.customer_site_id IS NULL THEN

         l_ln_count := 0;
         l_MODIFIERS_Tbl                        := QP_MODIFIERS_PUB.G_MISS_MODIFIERS_TBL;

         l_pa_Count := 0;
         l_PRICING_ATTR_Tbl                     := QP_MODIFIERS_PUB.G_MISS_PRICING_ATTR_TBL;

            FOR Mod_Line_Rec IN cur_seg_mod_line(rec_exl.seg_list_header_id
                                               , l_excludr_line_id
                                               , mod_csp_line_rec.product_attr
                                               , mod_csp_line_rec.product_attr_val
                                               ) LOOP

                l_sec := 'Loop Modifier Lines to add Exclusions';
                fnd_file.put_line (fnd_file.LOG, l_sec);

                fnd_file.put_line (fnd_file.LOG, 'seg_list_header_id - '||rec_exl.seg_list_header_id);
                l_ln_count                                               := l_ln_count + 1;

                l_MODIFIERS_tbl(l_Ln_Count).LIST_LINE_ID                 := l_excludr_line_id;       -- QP_LIST_LINES_S.NEXTVAL;

                IF Mod_Line_Rec.MODIFIER_LEVEL_CODE IS NOT NULL Then
                   l_MODIFIERS_tbl(l_Ln_Count).MODIFIER_LEVEL_CODE       := Mod_Line_Rec.MODIFIER_LEVEL_CODE ;
                END IF;

                IF Mod_Line_Rec.LIST_LINE_TYPE_CODE IS NOT NULL Then
                   l_MODIFIERS_tbl(l_Ln_Count).LIST_LINE_TYPE_CODE       := Mod_Line_Rec.LIST_LINE_TYPE_CODE;
                END IF;

                l_MODIFIERS_tbl(l_Ln_Count).START_DATE_ACTIVE            := Mod_Line_Rec.START_DATE_ACTIVE;
                l_MODIFIERS_tbl(l_Ln_Count).END_DATE_ACTIVE              := Mod_Line_Rec.END_DATE_ACTIVE;

                If Mod_Line_Rec.AUTOMATIC_FLAG IS NOT NULL Then
                   l_MODIFIERS_tbl(l_Ln_Count).AUTOMATIC_FLAG            := Mod_Line_Rec.AUTOMATIC_FLAG;
                End If;

                IF Mod_Line_Rec.OVERRIDE_FLAG  Is Not NULL Then
                   l_MODIFIERS_tbl(l_Ln_Count).OVERRIDE_FLAG             := Mod_Line_Rec.OVERRIDE_FLAG;
                END IF;

                IF Mod_Line_Rec.PRICING_PHASE_ID Is Not Null Then
                   l_MODIFIERS_tbl(l_Ln_Count).PRICING_PHASE_ID          := Mod_Line_Rec.PRICING_PHASE_ID;
                END IF;

                IF Mod_Line_Rec.PRODUCT_PRECEDENCE Is Not Null Then
                   l_MODIFIERS_tbl(l_Ln_Count).PRODUCT_PRECEDENCE        := Mod_Line_Rec.PRODUCT_PRECEDENCE;
                END IF;

                l_MODIFIERS_tbl(l_Ln_Count).PRICE_BREAK_TYPE_CODE        := Mod_Line_Rec.PRICE_BREAK_TYPE_CODE;
                l_MODIFIERS_tbl(l_Ln_Count).ARITHMETIC_OPERATOR          := Mod_Line_Rec.ARITHMETIC_OPERATOR ;
                l_MODIFIERS_tbl(l_Ln_Count).OPERAND                      := Mod_Line_Rec.OPERAND ;
                l_MODIFIERS_tbl(l_Ln_Count).ACCRUAL_FLAG                 := FND_API.G_MISS_CHAR ;--Mod_Line_Rec.ACCRUAL_FLAG ;
                l_MODIFIERS_tbl(l_Ln_Count).INCOMPATIBILITY_GRP_CODE     := Mod_Line_Rec.INCOMPATIBILITY_GRP_CODE ;

                If Mod_Line_Rec.PRICING_GROUP_SEQUENCE  Is Not Null Then
                   l_MODIFIERS_tbl(l_Ln_Count).PRICING_GROUP_SEQUENCE    := Mod_Line_Rec.PRICING_GROUP_SEQUENCE ;
                End If;

                If Mod_Line_Rec.INCLUDE_ON_RETURNS_FLAG Is Not Null Then
                   l_MODIFIERS_tbl(l_Ln_Count).INCLUDE_ON_RETURNS_FLAG   := Mod_Line_Rec.INCLUDE_ON_RETURNS_FLAG ;
                End If;

                l_MODIFIERS_tbl(l_Ln_Count).PRICE_BY_FORMULA_ID          := Mod_Line_Rec.PRICE_BY_FORMULA_ID ;
                l_MODIFIERS_tbl(l_Ln_Count).operation                    := QP_GLOBALS.G_OPR_UPDATE;   -- G_OPR_CREATE;

                ----------------------------------------------------------------------------------
                -- Create Pricing Attributes - As Exclusions
                ----------------------------------------------------------------------------------

                FOR Pricing_Attr_Rec In Get_Pricing_Attr_Cur (rec_exl.csp_list_header_id, Mod_CSP_Line_Rec.list_line_id, 'N') LOOP

                    l_sec := 'Create Pricing Attributes - As Exclusions - '||Pricing_Attr_Rec.PRODUCT_ATTR_VALUE;
                    fnd_file.put_line (fnd_file.LOG, l_sec);

                    l_pa_Count := l_pa_Count + 1;

                    l_PRICING_ATTR_tbl(l_pa_Count).LIST_HEADER_ID            :=  rec_exl.seg_list_header_id;
                    l_PRICING_ATTR_tbl(l_pa_Count).LIST_LINE_ID              :=  l_excludr_line_id;
                    l_PRICING_ATTR_tbl(l_pa_Count).PRICING_ATTRIBUTE_ID      :=  QP_PRICING_ATTRIBUTES_S.NEXTVAL;
                    l_PRICING_ATTR_tbl(l_pa_Count).PRODUCT_ATTRIBUTE_CONTEXT :=  Pricing_Attr_Rec.PRODUCT_ATTRIBUTE_CONTEXT ;
                    l_PRICING_ATTR_tbl(l_pa_Count).PRODUCT_ATTRIBUTE         :=  Pricing_Attr_Rec.PRODUCT_ATTRIBUTE ;
                    l_PRICING_ATTR_tbl(l_pa_Count).PRODUCT_ATTR_VALUE        :=  Pricing_Attr_Rec.PRODUCT_ATTR_VALUE ;

                    l_PRICING_ATTR_tbl(l_pa_Count).PRICING_ATTRIBUTE         :=  Pricing_Attr_Rec.PRICING_ATTRIBUTE ;
                    l_PRICING_ATTR_tbl(l_pa_Count).PRODUCT_UOM_CODE          :=  Pricing_Attr_Rec.PRODUCT_UOM_CODE ;
                    l_PRICING_ATTR_tbl(l_pa_Count).COMPARISON_OPERATOR_CODE  :=  Pricing_Attr_Rec.COMPARISON_OPERATOR_CODE ;
                    l_PRICING_ATTR_tbl(l_pa_Count).PRICING_ATTRIBUTE_CONTEXT :=  Pricing_Attr_Rec.PRICING_ATTRIBUTE_CONTEXT ;
                    l_PRICING_ATTR_tbl(l_pa_Count).PRICING_ATTR_VALUE_FROM   :=  Pricing_Attr_Rec.PRICING_ATTR_VALUE_FROM ;
                    l_PRICING_ATTR_tbl(l_pa_Count).PRICING_ATTR_VALUE_TO     :=  Pricing_Attr_Rec.PRICING_ATTR_VALUE_TO ;

                    l_PRICING_ATTR_tbl(l_pa_Count).ATTRIBUTE_GROUPING_NO     :=  Pricing_Attr_Rec.ATTRIBUTE_GROUPING_NO;
                    l_PRICING_ATTR_tbl(l_pa_Count).Operation                 := QP_GLOBALS.G_OPR_CREATE;
                    l_PRICING_ATTR_tbl(l_pa_Count).EXCLUDER_FLAG             :=  'Y';
                    l_PRICING_ATTR_tbl(l_pa_Count).MODIFIERS_INDEX           := l_pa_Count;
                    

                END LOOP ;     -- Pricing Attributes
            END LOOP;          -- Mod_Line_Rec
            
            ----------------------------------------------------------------------------------
            -- Add Modifier Lines
            ----------------------------------------------------------------------------------
            IF l_Ln_Count > 0 THEN
            QP_Modifiers_PUB.Process_Modifiers
               ( p_api_version_number    => 1.0
               , p_init_msg_list         => FND_API.G_FALSE
               , p_return_values         => FND_API.G_FALSE
               , p_commit                => FND_API.G_TRUE
               , x_return_status         => l_return_status
               , x_msg_count             => l_msg_count
               , x_msg_data              => l_msg_data
               , p_MODIFIER_LIST_rec     => l_MODIFIER_LIST_rec
               , p_MODIFIERS_tbl         => l_MODIFIERS_tbl
               , p_QUALIFIERS_tbl        => l_QUALIFIERS_tbl
               , p_PRICING_ATTR_tbl      => l_PRICING_ATTR_tbl
               , x_MODIFIER_LIST_rec     => lx_MODIFIER_LIST_rec
               , x_MODIFIER_LIST_val_rec => lx_MODIFIER_LIST_val_rec
               , x_MODIFIERS_tbl         => lx_MODIFIERS_tbl
               , x_MODIFIERS_val_tbl     => lx_MODIFIERS_val_tbl
               , x_QUALIFIERS_tbl        => lx_QUALIFIERS_tbl
               , x_QUALIFIERS_val_tbl    => lx_QUALIFIERS_val_tbl
               , x_PRICING_ATTR_tbl      => lx_PRICING_ATTR_tbl
               , x_PRICING_ATTR_val_tbl  => lx_PRICING_ATTR_val_tbl );
            COMMIT;

                  IF l_return_status = fnd_api.G_RET_STS_SUCCESS THEN
                     COMMIT;
                  ELSE
                     -- ROLLBACK;
                     COMMIT;
                     -- fnd_file.put_line (fnd_file.LOG, 'Error Add Modifier Lines - '|| l_msg_data ||' - ' || l_msg_count);
                     fnd_file.put_line (fnd_file.LOG, 'After Error Msg ');

                     --FOR I IN 1 .. l_msg_count LOOP
                     --   l_msg_data := oe_msg_pub.get( p_msg_index => i,p_encoded => 'F');
                     --   fnd_file.put_line (fnd_file.LOG, 'err msg ' || i ||'is:  ' || l_msg_data);
                     --END LOOP;
                  END IF;

            END IF; -- IF l_Ln_Count > 0 THEN

         END IF; -- IF rec_exl.customer_site_id IS NULL
         --------------------------------------------------------------
         -- If Exclusive CSP is at Master Level - Add exclusions to 
         -- Segment Modifier < End
         --------------------------------------------------------------

         l_prev_excludr_line_id := l_excludr_line_id;

         END LOOP;             -- rec_excl_line
         END LOOP;             -- Mod_CSP_Line_Rec

         ----------------------------------------------------------------------------------
         -- Add Modifier Lines
         ----------------------------------------------------------------------------------
         QP_Modifiers_PUB.Process_Modifiers
            ( p_api_version_number    => 1.0
            , p_init_msg_list         => FND_API.G_FALSE
            , p_return_values         => FND_API.G_FALSE
            , p_commit                => FND_API.G_TRUE
            , x_return_status         => l_return_status
            , x_msg_count             => l_msg_count
            , x_msg_data              => l_msg_data
            , p_MODIFIER_LIST_rec     => l_MODIFIER_LIST_rec
            , p_MODIFIERS_tbl         => l_MODIFIERS_tbl
            , p_QUALIFIERS_tbl        => l_QUALIFIERS_tbl
            , p_PRICING_ATTR_tbl      => l_PRICING_ATTR_tbl
            , x_MODIFIER_LIST_rec     => lx_MODIFIER_LIST_rec
            , x_MODIFIER_LIST_val_rec => lx_MODIFIER_LIST_val_rec
            , x_MODIFIERS_tbl         => lx_MODIFIERS_tbl
            , x_MODIFIERS_val_tbl     => lx_MODIFIERS_val_tbl
            , x_QUALIFIERS_tbl        => lx_QUALIFIERS_tbl
            , x_QUALIFIERS_val_tbl    => lx_QUALIFIERS_val_tbl
            , x_PRICING_ATTR_tbl      => lx_PRICING_ATTR_tbl
            , x_PRICING_ATTR_val_tbl  => lx_PRICING_ATTR_val_tbl );
         COMMIT;

         l_sec := 'After QP_Modifiers_PUB.Process_Modifiers';
         fnd_file.put_line (fnd_file.LOG, l_sec);

         fnd_file.put_line (fnd_file.LOG, l_sec);
         fnd_file.put_line (fnd_file.LOG, 'x_return_status -'||l_return_status);
         fnd_file.put_line (fnd_file.LOG, 'x_msg_count -'||l_msg_count);
         -- fnd_file.put_line (fnd_file.LOG, 'x_msg_data -'||l_msg_data);

      ----------------------------------------------------------------------------------
      -- Set CSP Segment DFF on Header Level CSP to 'Yes'
      ----------------------------------------------------------------------------------
      BEGIN
         UPDATE xxwc.xxwc_om_contract_pricing_hdr
            SET attribute2           = 'Y'
          WHERE agreement_id         = rec_exl.agreement_id
            AND NVL(attribute2, 'N') = 'N';
      EXCEPTION
      WHEN OTHERS THEN
        NULL;
      END;

      END LOOP;                                                  -- cur_exl
      COMMIT;

      fnd_file.put_line (fnd_file.LOG, '##############################################################################');
      fnd_file.put_line (fnd_file.LOG, ' Best Price Wins Agreement Header - Cursor > Start '||TO_CHAR(SYSDATE,'DD-MON-YY HH24:MI:SS'));
      fnd_file.put_line (fnd_file.LOG, '##############################################################################');


        fnd_file.put_line (fnd_file.LOG, ' GTT Insert > Start '||TO_CHAR(SYSDATE,'DD-MON-YY HH24:MI:SS'));
        INSERT INTO XXWC.XXWC_PRICING_SEG_GTT_TBL (
          LIST_HEADER_ID   ,
          CUSTOMER_ID      ,
          CUSTOMER_SITE_ID ,
          MODIFIER_TYPE    
        )
        SELECT qlh.list_header_id
             , xch_e.customer_id
             , xch_e.customer_site_id
             , 'EXCL_CSP'
          FROM qp_list_headers_all qlh
             , xxwc.xxwc_om_contract_pricing_hdr xch_e
         WHERE 1 = 1
           AND xch_e.incompatability_group     = 'Exclusive'
           AND xch_e.agreement_status          = 'APPROVED'
           AND TO_NUMBER (qlh.attribute14)     = xch_e.agreement_id
           AND qlh.active_flag                 = 'Y'
           AND TRUNC(SYSDATE) BETWEEN TRUNC(NVL(qlh.start_date_active, SYSDATE -1))  AND TRUNC(NVL(qlh.end_date_active, SYSDATE+ 1))
           AND EXISTS (SELECT '1'
                         FROM xxwc.xxwc_om_contract_pricing_lines  xcl_e
                        WHERE 1 = 1
                          AND xcl_e.agreement_id              = xch_e.agreement_id
                          AND TRUNC(xcl_e.last_update_date)   >= l_process_date
                          AND xcl_e.line_status               = 'APPROVED'
                          AND TRUNC(SYSDATE) BETWEEN TRUNC(NVL(xcl_e.start_date, SYSDATE -1))  AND TRUNC(NVL(xcl_e.end_date, SYSDATE+ 1))
                          );
        fnd_file.put_line (fnd_file.LOG, ' GTT Insert < End '||TO_CHAR(SYSDATE,'DD-MON-YY HH24:MI:SS'));

      -- ############################################################################## --
      ----------------------------------------------------------------------------------
      -- Best Price Wins Agreement Header - Cursor
      ----------------------------------------------------------------------------------
      -- ############################################################################## --
      FOR rec_bpw IN cur_bpw(l_process_date)
      LOOP

         l_sec := 'Best Price Wins Agreement Header - Cursor';
         fnd_file.put_line (fnd_file.LOG, l_sec);

         l_error_message := ' ';
         l_MODIFIER_LIST_rec                    := QP_MODIFIERS_PUB.G_MISS_MODIFIER_LIST_REC;
         l_MODIFIERS_Tbl                        := QP_MODIFIERS_PUB.G_MISS_MODIFIERS_TBL;
         l_PRICING_ATTR_Tbl                     := QP_MODIFIERS_PUB.G_MISS_PRICING_ATTR_TBL;
         l_ln_count                             := 0;

         l_ql_Count := 0 ;
         l_QUALIFIERS_tbl                       := QP_MODIFIERS_PUB.G_MISS_QUALIFIERS_TBL;

         l_MODIFIER_LIST_rec.List_Header_ID          := rec_bpw.seg_list_header_id;
         l_MODIFIER_LIST_rec.operation               := QP_GLOBALS.G_OPR_UPDATE;                 -- G_OPR_CREATE ;

         ----------------------------------------------------------------------------------
         -- Update Incompatibility_Group of Best Price Wins Modifiers
         ----------------------------------------------------------------------------------
         l_ln_count := 0;
         l_sec := 'Update Incompatibility_Group of Best Price Wins Modifiers';
         fnd_file.put_line (fnd_file.LOG, l_sec||TO_CHAR(SYSDATE,'DD-MON-YY HH24:MI:SS'));

         --FOR Mod_Line_Rec IN cur_bpw_mod_line (rec_bpw.csp_list_header_id, NULL, 'Exclusive Group') LOOP
         FOR Mod_Line_Rec IN cur_bpw_mod_line (rec_bpw.seg_list_header_id, rec_bpw.csp_list_header_id, NULL, 'Level 1 Incompatibility','Y') LOOP

             l_ln_count :=  l_ln_count + 1;

             l_MODIFIERS_tbl(l_Ln_Count).LIST_LINE_ID                := Mod_Line_Rec.LIST_LINE_ID ;
             l_MODIFIERS_tbl(l_Ln_Count).LIST_LINE_NO                := NVL(Mod_Line_Rec.LIST_LINE_NO, Mod_Line_Rec.LIST_LINE_ID)  ;

             If Mod_Line_Rec.MODIFIER_LEVEL_CODE IS NOT NULL Then
                l_MODIFIERS_tbl(l_Ln_Count).MODIFIER_LEVEL_CODE         := Mod_Line_Rec.MODIFIER_LEVEL_CODE ;
             End If;

             If Mod_Line_Rec.LIST_LINE_TYPE_CODE IS NOT NULL Then
                l_MODIFIERS_tbl(l_Ln_Count).LIST_LINE_TYPE_CODE         := Mod_Line_Rec.LIST_LINE_TYPE_CODE;

             End If;

             l_MODIFIERS_tbl(l_Ln_Count).START_DATE_ACTIVE           := Mod_Line_Rec.START_DATE_ACTIVE;
             l_MODIFIERS_tbl(l_Ln_Count).END_DATE_ACTIVE             := Mod_Line_Rec.END_DATE_ACTIVE;

             If Mod_Line_Rec.AUTOMATIC_FLAG IS NOT NULL Then
                l_MODIFIERS_tbl(l_Ln_Count).AUTOMATIC_FLAG              := Mod_Line_Rec.AUTOMATIC_FLAG;
             End If;

             If Mod_Line_Rec.OVERRIDE_FLAG  Is Not NULL Then
                l_MODIFIERS_tbl(l_Ln_Count).OVERRIDE_FLAG               := Mod_Line_Rec.OVERRIDE_FLAG;
             End If;

             If Mod_Line_Rec.PRICING_PHASE_ID Is Not Null Then
                l_MODIFIERS_tbl(l_Ln_Count).PRICING_PHASE_ID            := Mod_Line_Rec.PRICING_PHASE_ID;
             End If;

             If  Mod_Line_Rec.PRODUCT_PRECEDENCE Is Not Null Then
                l_MODIFIERS_tbl(l_Ln_Count).PRODUCT_PRECEDENCE          := Mod_Line_Rec.PRODUCT_PRECEDENCE;
             End If;

             l_MODIFIERS_tbl(l_Ln_Count).PRICE_BREAK_TYPE_CODE       := Mod_Line_Rec.PRICE_BREAK_TYPE_CODE;
             l_MODIFIERS_tbl(l_Ln_Count).ARITHMETIC_OPERATOR         := Mod_Line_Rec.ARITHMETIC_OPERATOR ;
             l_MODIFIERS_tbl(l_Ln_Count).OPERAND                     := Mod_Line_Rec.OPERAND ;
             l_MODIFIERS_tbl(l_Ln_Count).ACCRUAL_FLAG                := FND_API.G_MISS_CHAR ;
             l_MODIFIERS_tbl(l_Ln_Count).INCOMPATIBILITY_GRP_CODE    := 'EXCL'; --%%%%%

             If Mod_Line_Rec.PRICING_GROUP_SEQUENCE  Is Not Null Then
                l_MODIFIERS_tbl(l_Ln_Count).PRICING_GROUP_SEQUENCE      := Mod_Line_Rec.PRICING_GROUP_SEQUENCE ;
             End If;

             If Mod_Line_Rec.INCLUDE_ON_RETURNS_FLAG Is Not Null Then
                l_MODIFIERS_tbl(l_Ln_Count).INCLUDE_ON_RETURNS_FLAG     := Mod_Line_Rec.INCLUDE_ON_RETURNS_FLAG ;
             End If;

             l_MODIFIERS_tbl(l_Ln_Count).PRICE_BY_FORMULA_ID         := Mod_Line_Rec.PRICE_BY_FORMULA_ID ;
             l_MODIFIERS_tbl(l_Ln_Count).operation                   := QP_GLOBALS.G_OPR_UPDATE;
         END LOOP;                                               -- Mod_Line_Rec

         fnd_file.put_line (fnd_file.LOG, 'l_Ln_Count - '||l_Ln_Count||TO_CHAR(SYSDATE,'DD-MON-YY HH24:MI:SS'));

         IF l_Ln_Count > 0 THEN
         ----------------------------------------------------------------------------------
         -- Add Modifier Lines
         ----------------------------------------------------------------------------------
         QP_Modifiers_PUB.Process_Modifiers
            ( p_api_version_number    => 1.0
            , p_init_msg_list         => FND_API.G_FALSE
            , p_return_values         => FND_API.G_FALSE
            , p_commit                => FND_API.G_TRUE
            , x_return_status         => l_return_status
            , x_msg_count             => l_msg_count
            , x_msg_data              => l_msg_data
            , p_MODIFIER_LIST_rec     => l_MODIFIER_LIST_rec
            , p_MODIFIERS_tbl         => l_MODIFIERS_tbl
            , p_QUALIFIERS_tbl        => l_QUALIFIERS_tbl
            , p_PRICING_ATTR_tbl      => l_PRICING_ATTR_tbl
            , x_MODIFIER_LIST_rec     => lx_MODIFIER_LIST_rec
            , x_MODIFIER_LIST_val_rec => lx_MODIFIER_LIST_val_rec
            , x_MODIFIERS_tbl         => lx_MODIFIERS_tbl
            , x_MODIFIERS_val_tbl     => lx_MODIFIERS_val_tbl
            , x_QUALIFIERS_tbl        => lx_QUALIFIERS_tbl
            , x_QUALIFIERS_val_tbl    => lx_QUALIFIERS_val_tbl
            , x_PRICING_ATTR_tbl      => lx_PRICING_ATTR_tbl
            , x_PRICING_ATTR_val_tbl  => lx_PRICING_ATTR_val_tbl );

         l_sec := 'After calling Pricing API - BPW';
         fnd_file.put_line (fnd_file.LOG, l_sec);
         fnd_file.put_line (fnd_file.LOG, 'x_return_status -'||l_return_status);
         fnd_file.put_line (fnd_file.LOG, 'x_msg_count -'||l_msg_count);
         -- fnd_file.put_line (fnd_file.LOG, 'x_msg_data -'||l_msg_data);

         COMMIT;
         END IF; -- IF l_Ln_Count > 0 THEN

      l_sec := 'Set CSP Segment DFF on Header Level CSP to Yes';
      fnd_file.put_line (fnd_file.LOG, l_sec||TO_CHAR(SYSDATE,'DD-MON-YY HH24:MI:SS'));
      ----------------------------------------------------------------------------------
      -- Set CSP Segment DFF on Header Level CSP to 'Yes'
      ----------------------------------------------------------------------------------
      BEGIN
         UPDATE xxwc.xxwc_om_contract_pricing_hdr
            SET attribute2           = 'Y'
          WHERE agreement_id         = rec_bpw.agreement_id
            AND NVL(attribute2, 'N') = 'N';
      
      COMMIT;
      EXCEPTION
      WHEN OTHERS THEN
        NULL;
      END;

      l_sec := 'Add Exlusive CSP Modifier lines as Excluders to Best Price Wins Modifier';
      fnd_file.put_line (fnd_file.LOG, l_sec||TO_CHAR(SYSDATE,'DD-MON-YY HH24:MI:SS'));
      --------------------------------------------------------------------------------------------------------------------
      -- Add Exlusive CSP Modifier lines as Excluders to Best Price Wins Modifier
      --------------------------------------------------------------------------------------------------------------------
      FOR rec_bpw_exl IN cur_bpw_exl(rec_bpw.customer_id)
      LOOP

         l_sec := 'Add Exlusive CSP Modifier lines as Excluders to Best Price Wins Modifier';
         fnd_file.put_line (fnd_file.LOG, l_sec);

         l_error_message := ' ';
         l_MODIFIER_LIST_rec                    := QP_MODIFIERS_PUB.G_MISS_MODIFIER_LIST_REC;

         l_MODIFIER_LIST_rec.list_header_id     := rec_bpw.csp_list_header_id;
         l_MODIFIER_LIST_rec.operation          := QP_GLOBALS.G_OPR_UPDATE;

         ----------------------------------------------------------------------------------
         -- Update CSP lines as Exclusions
         ----------------------------------------------------------------------------------
         l_ln_count := 0;
         l_MODIFIERS_Tbl                        := QP_MODIFIERS_PUB.G_MISS_MODIFIERS_TBL;

         l_pa_Count := 0;
         l_PRICING_ATTR_Tbl                     := QP_MODIFIERS_PUB.G_MISS_PRICING_ATTR_TBL;

         l_ql_Count := 0 ;
         l_QUALIFIERS_tbl                       := QP_MODIFIERS_PUB.G_MISS_QUALIFIERS_TBL;

         l_prev_excludr_line_id := 0;
         FOR Mod_CSP_Line_Rec IN cur_csp_mod_line(rec_bpw_exl.excl_list_header_id, 'N') LOOP

            l_sec := 'Loop BPW Modifier Lines to add Qualifiers and Excluders';
            fnd_file.put_line (fnd_file.LOG, l_sec);
            fnd_file.put_line (fnd_file.LOG, 'excl_list_header_id - '||rec_bpw_exl.excl_list_header_id);
            fnd_file.put_line (fnd_file.LOG, 'list_line_id - '||mod_csp_line_rec.list_line_id);

             l_excludr_line_id                      := NULL;

             FOR rec_excl_line IN cur_excluder_line(mod_csp_line_rec.product_attr
                                                  , mod_csp_line_rec.product_attr_val
                                                  , rec_bpw.csp_list_header_id) LOOP
                 l_excludr_line_id := rec_excl_line.list_line_id;

                 fnd_file.put_line (fnd_file.LOG, 'l_excludr_line_id - '||l_excludr_line_id);

                 l_ql_Count                := 0;
                 l_qualifier_precedence    := 0;
                 l_qualifier_rec           := QP_QUALIFIER_RULES_PUB.G_MISS_QUALIFIERS_REC;
                 l_QUALIFIERS_tbl          := QP_MODIFIERS_PUB.G_MISS_QUALIFIERS_TBL;

                 l_ln_count := 0;
                 l_MODIFIERS_Tbl                        := QP_MODIFIERS_PUB.G_MISS_MODIFIERS_TBL;

                 l_pa_Count := 0;
                 l_PRICING_ATTR_Tbl                     := QP_MODIFIERS_PUB.G_MISS_PRICING_ATTR_TBL;

                 l_sec := 'IF BPW is at Master Level > Start';
                 fnd_file.put_line (fnd_file.LOG, l_sec||TO_CHAR(SYSDATE,'DD-MON-YY HH24:MI:SS'));
                 --------------------------------------------------------------------------------
                 -- IF BPW is at Master Level > Start
                 --------------------------------------------------------------------------------
                 IF rec_bpw.customer_site_id IS NULL THEN

                        FOR rec_qq IN (SELECT *
                                         FROM qp_qualifiers qq
                                        WHERE 1 = 1
                                          AND qq.list_header_id           IS NULL
                                          AND qq.list_line_id             = -1
                                          AND NOT EXISTS (SELECT '1'
                                                            FROM qp_qualifiers qqc
                                                           WHERE 1 = 1
                                                             AND qqc.list_header_id           = rec_bpw.csp_list_header_id -- BPW list_header_id
                                                             AND qqc.list_line_id             = l_excludr_line_id
                                                             AND qqc.qualifier_context        = qq.qualifier_context
                                                             AND qqc.qualifier_grouping_no    = qq.qualifier_grouping_no -- %%%%%
                                                             AND qqc.qualifier_attribute      = qq.qualifier_attribute
                                                             AND qqc.qualifier_attr_value     = qq.qualifier_attr_value
                                                             AND qqc.comparison_operator_code = qq.comparison_operator_code)
                                          AND qq.qualifier_rule_id IN (SELECT qqc.qualifier_rule_id
                                                                         FROM qp_qualifiers qqc
                                                                        WHERE 1 = 1
                                                                          AND qqc.qualifier_context        = 'CUSTOMER'
                                                                          AND qqc.qualifier_attribute      = 'QUALIFIER_ATTRIBUTE11' 
                                                                          -- AND qqc.qualifier_attr_value     = rec_bpw_exl.customer_site_id
                                                                          AND qqc.qualifier_attr_value     IN (SELECT hcsu.site_use_id
                                                                                                                 FROM hz_cust_acct_sites_all            hcas
                                                                                                                    , hz_cust_site_uses_all             hcsu
                                                                                                                WHERE 1=1 
                                                                                                                  AND hcas.cust_account_id            = rec_bpw.customer_id
                                                                                                                  AND hcsu.cust_acct_site_id          = hcas.cust_acct_site_id
                                                                                                                  AND hcsu.site_use_code              = 'SHIP_TO'
                                                                                                                  AND hcas.status                     = 'A'
                                                                                                                  AND hcsu.status                     = 'A')
                                                                          AND qqc.comparison_operator_code = '='
                                                                          AND qqc.list_header_id           IS NULL
                                                                          AND qqc.list_line_id             = -1))
                        LOOP

                           IF rec_qq.qualifier_context        = 'CUSTOMER' THEN
                              --------------------------------------------------------------
                              -- Add Ship-To Qualifier > Start
                              --------------------------------------------------------------
                              l_qualifier_rec                            := QP_QUALIFIER_RULES_PUB.G_MISS_QUALIFIERS_REC;
                              l_ql_Count                                 := l_ql_Count + 1;
                              l_qualifier_precedence                     := l_qualifier_precedence + 1;

                              Fnd_File.Put_Line ( Fnd_File.LOG, 'Add Ship-To Qualifier =>' || rec_qq.qualifier_grouping_no);

                              l_qualifier_rec.LIST_HEADER_ID             :=  rec_bpw.csp_list_header_id;
                              l_qualifier_rec.LIST_LINE_ID               :=  l_excludr_line_id;
                              l_qualifier_rec.QUALIFIER_ID               :=  qp_qualifiers_s.nextval;
                              l_qualifier_rec.CREATED_FROM_RULE_ID       :=  rec_qq.qualifier_rule_id;
                              l_qualifier_rec.QUALIFIER_GROUPING_NO      :=  rec_qq.qualifier_grouping_no;
                              l_qualifier_rec.QUALIFIER_CONTEXT          :=  rec_qq.QUALIFIER_CONTEXT;
                              l_qualifier_rec.QUALIFIER_ATTRIBUTE        :=  rec_qq.QUALIFIER_ATTRIBUTE;
                              l_qualifier_rec.QUALIFIER_ATTR_VALUE       :=  rec_qq.QUALIFIER_ATTR_VALUE;
                              l_qualifier_rec.COMPARISON_OPERATOR_CODE   :=  rec_qq.COMPARISON_OPERATOR_CODE;

                              l_qualifier_rec.QUALIFIER_PRECEDENCE       :=  rec_qq.QUALIFIER_PRECEDENCE;
                              l_qualifier_rec.Excluder_Flag              := 'N';
                              l_qualifier_rec.Operation                  := QP_GLOBALS.G_OPR_CREATE;

                              l_QUALIFIERS_tbl(l_ql_Count)               := l_qualifier_rec;
                              --------------------------------------------------------------
                              -- Add Ship-To Qualifier < End
                              --------------------------------------------------------------
                           ELSE
                              IF is_valid_qualifier(rec_qq.qualifier_context, rec_qq.qualifier_attr_value,l_excludr_line_id ) = 'Y' THEN
                                 --------------------------------------------------------------
                                 -- Add Item/Category Level Qualifier > Start
                                 --------------------------------------------------------------
                                 l_qualifier_rec                            := QP_QUALIFIER_RULES_PUB.G_MISS_QUALIFIERS_REC;
                                 l_ql_Count                                 := l_ql_Count + 1;
                                 l_qualifier_precedence                     := l_qualifier_precedence + 1;

                                 Fnd_File.Put_Line ( Fnd_File.LOG, 'Add Inventory Item Qualifier =>' || rec_qq.qualifier_grouping_no);

                                 l_qualifier_rec.LIST_HEADER_ID             :=  rec_bpw.csp_list_header_id;
                                 l_qualifier_rec.LIST_LINE_ID               :=  l_excludr_line_id;
                                 l_qualifier_rec.QUALIFIER_ID               :=  qp_qualifiers_s.nextval;
                                 l_qualifier_rec.CREATED_FROM_RULE_ID       :=  rec_qq.qualifier_rule_id;
                                 l_qualifier_rec.QUALIFIER_GROUPING_NO      :=  rec_qq.qualifier_grouping_no;
                                 l_qualifier_rec.QUALIFIER_CONTEXT          :=  rec_qq.QUALIFIER_CONTEXT;
                                 l_qualifier_rec.QUALIFIER_ATTRIBUTE        :=  rec_qq.QUALIFIER_ATTRIBUTE;
                                 l_qualifier_rec.QUALIFIER_ATTR_VALUE       :=  rec_qq.QUALIFIER_ATTR_VALUE;
                                 l_qualifier_rec.COMPARISON_OPERATOR_CODE   :=  rec_qq.COMPARISON_OPERATOR_CODE;

                                 l_qualifier_rec.QUALIFIER_PRECEDENCE       :=  rec_qq.QUALIFIER_PRECEDENCE;
                                 l_qualifier_rec.Excluder_Flag              := 'N';
                                 l_qualifier_rec.Operation                  := QP_GLOBALS.G_OPR_CREATE;

                                 l_QUALIFIERS_tbl(l_ql_Count)               := l_qualifier_rec;
                                 --------------------------------------------------------------
                                 -- Add Item/Category Level Qualifier < End
                                 --------------------------------------------------------------
                              END IF; -- IF is_valid_qualifier(rec_qq.qualifier_context, rec_qq.qualifier_attr_value,l_excludr_line_id ) = 'Y' THEN
                           END IF; -- IF rec_qq.qualifier_context        = 'CUSTOMER' THEN
                        END LOOP; -- FOR rec_qq

                        ----------------------------------------------------------------------------------
                        -- Add Modifier Lines
                        ----------------------------------------------------------------------------------
                        QP_Modifiers_PUB.Process_Modifiers
                           ( p_api_version_number    => 1.0
                           , p_init_msg_list         => FND_API.G_FALSE
                           , p_return_values         => FND_API.G_FALSE
                           , p_commit                => FND_API.G_TRUE
                           , x_return_status         => l_return_status
                           , x_msg_count             => l_msg_count
                           , x_msg_data              => l_msg_data
                           , p_MODIFIER_LIST_rec     => l_MODIFIER_LIST_rec
                           , p_MODIFIERS_tbl         => l_MODIFIERS_tbl
                           , p_QUALIFIERS_tbl        => l_QUALIFIERS_tbl
                           , p_PRICING_ATTR_tbl      => l_PRICING_ATTR_tbl
                           , x_MODIFIER_LIST_rec     => lx_MODIFIER_LIST_rec
                           , x_MODIFIER_LIST_val_rec => lx_MODIFIER_LIST_val_rec
                           , x_MODIFIERS_tbl         => lx_MODIFIERS_tbl
                           , x_MODIFIERS_val_tbl     => lx_MODIFIERS_val_tbl
                           , x_QUALIFIERS_tbl        => lx_QUALIFIERS_tbl
                           , x_QUALIFIERS_val_tbl    => lx_QUALIFIERS_val_tbl
                           , x_PRICING_ATTR_tbl      => lx_PRICING_ATTR_tbl
                           , x_PRICING_ATTR_val_tbl  => lx_PRICING_ATTR_val_tbl );
                        COMMIT;

                        l_sec := 'After QP_Modifiers_PUB.Process_Modifiers';
                        fnd_file.put_line (fnd_file.LOG, l_sec);

                        fnd_file.put_line (fnd_file.LOG, l_sec);
                        fnd_file.put_line (fnd_file.LOG, 'x_return_status -'||l_return_status);
                        fnd_file.put_line (fnd_file.LOG, 'x_msg_count -'||l_msg_count);
                        -- fnd_file.put_line (fnd_file.LOG, 'x_msg_data -'||l_msg_data);

                 END IF; -- IF rec_bpw.customer_site_id IS NULL THEN
                 --------------------------------------------------------------------------------
                 -- IF BPW is at Master Level < End
                 --------------------------------------------------------------------------------

                 l_sec := 'IF Exclusive is at Master Level > Start';
                 fnd_file.put_line (fnd_file.LOG, l_sec||TO_CHAR(SYSDATE,'DD-MON-YY HH24:MI:SS'));

                 --------------------------------------------------------------------------------
                 -- IF Exclusive is at Master Level > Start
                 --------------------------------------------------------------------------------
                 IF rec_bpw_exl.customer_site_id IS NULL THEN -- If Exclusive CSP is at Master Level

                    FOR Mod_Line_Rec IN cur_seg_mod_line(rec_bpw.csp_list_header_id -- bpw_list_header_id
                                                       , l_excludr_line_id
                                                       , mod_csp_line_rec.product_attr
                                                       , mod_csp_line_rec.product_attr_val
                                                       ) LOOP

                        l_sec := 'Loop Modifier Lines when Exclusive CSP is at Master Level';
                        fnd_file.put_line (fnd_file.LOG, l_sec);

                        fnd_file.put_line (fnd_file.LOG, 'bpw_list_header_id - '||rec_bpw.csp_list_header_id);
                        l_ln_count                             := l_ln_count + 1;

                        l_MODIFIERS_tbl(l_Ln_Count).LIST_LINE_ID                 := l_excludr_line_id;       -- QP_LIST_LINES_S.NEXTVAL;

                        If Mod_Line_Rec.MODIFIER_LEVEL_CODE IS NOT NULL Then
                           l_MODIFIERS_tbl(l_Ln_Count).MODIFIER_LEVEL_CODE       := Mod_Line_Rec.MODIFIER_LEVEL_CODE ;
                        End If;

                        If Mod_Line_Rec.LIST_LINE_TYPE_CODE IS NOT NULL Then
                           l_MODIFIERS_tbl(l_Ln_Count).LIST_LINE_TYPE_CODE       := Mod_Line_Rec.LIST_LINE_TYPE_CODE;

                        End If;

                        l_MODIFIERS_tbl(l_Ln_Count).START_DATE_ACTIVE            := Mod_Line_Rec.START_DATE_ACTIVE;
                        l_MODIFIERS_tbl(l_Ln_Count).END_DATE_ACTIVE              := Mod_Line_Rec.END_DATE_ACTIVE;

                        If Mod_Line_Rec.AUTOMATIC_FLAG IS NOT NULL Then
                           l_MODIFIERS_tbl(l_Ln_Count).AUTOMATIC_FLAG            := Mod_Line_Rec.AUTOMATIC_FLAG;
                        End If;

                        If Mod_Line_Rec.OVERRIDE_FLAG  Is Not NULL Then
                           l_MODIFIERS_tbl(l_Ln_Count).OVERRIDE_FLAG             := Mod_Line_Rec.OVERRIDE_FLAG;
                        End If;

                        If Mod_Line_Rec.PRICING_PHASE_ID Is Not Null Then
                           l_MODIFIERS_tbl(l_Ln_Count).PRICING_PHASE_ID          := Mod_Line_Rec.PRICING_PHASE_ID;
                        End If;

                        If  Mod_Line_Rec.PRODUCT_PRECEDENCE Is Not Null Then
                           l_MODIFIERS_tbl(l_Ln_Count).PRODUCT_PRECEDENCE        := Mod_Line_Rec.PRODUCT_PRECEDENCE;
                        End If;

                        l_MODIFIERS_tbl(l_Ln_Count).PRICE_BREAK_TYPE_CODE        := Mod_Line_Rec.PRICE_BREAK_TYPE_CODE;
                        l_MODIFIERS_tbl(l_Ln_Count).ARITHMETIC_OPERATOR          := Mod_Line_Rec.ARITHMETIC_OPERATOR ;
                        l_MODIFIERS_tbl(l_Ln_Count).OPERAND                      := Mod_Line_Rec.OPERAND ;
                        l_MODIFIERS_tbl(l_Ln_Count).ACCRUAL_FLAG                 := FND_API.G_MISS_CHAR ;--Mod_Line_Rec.ACCRUAL_FLAG ;
                        l_MODIFIERS_tbl(l_Ln_Count).INCOMPATIBILITY_GRP_CODE     := Mod_Line_Rec.INCOMPATIBILITY_GRP_CODE ;

                        If Mod_Line_Rec.PRICING_GROUP_SEQUENCE  Is Not Null Then
                           l_MODIFIERS_tbl(l_Ln_Count).PRICING_GROUP_SEQUENCE    := Mod_Line_Rec.PRICING_GROUP_SEQUENCE ;
                        End If;

                        If Mod_Line_Rec.INCLUDE_ON_RETURNS_FLAG Is Not Null Then
                           l_MODIFIERS_tbl(l_Ln_Count).INCLUDE_ON_RETURNS_FLAG   := Mod_Line_Rec.INCLUDE_ON_RETURNS_FLAG ;
                        End If;

                        l_MODIFIERS_tbl(l_Ln_Count).PRICE_BY_FORMULA_ID          := Mod_Line_Rec.PRICE_BY_FORMULA_ID ;
                        l_MODIFIERS_tbl(l_Ln_Count).operation                    := QP_GLOBALS.G_OPR_UPDATE;   -- G_OPR_CREATE;

                        ----------------------------------------------------------------------------------
                        -- Create Pricing Attributes - As Exclusions
                        ----------------------------------------------------------------------------------

                        FOR Pricing_Attr_Rec In Get_Pricing_Attr_Cur (rec_bpw_exl.excl_list_header_id, Mod_CSP_Line_Rec.list_line_id, 'N') LOOP

                            l_sec := 'Create Pricing Attributes - As Exclusions';
                            fnd_file.put_line (fnd_file.LOG, l_sec);

                            l_pa_Count := l_pa_Count + 1;

                            l_PRICING_ATTR_tbl(l_pa_Count).LIST_HEADER_ID            :=  rec_bpw.csp_list_header_id;
                            l_PRICING_ATTR_tbl(l_pa_Count).LIST_LINE_ID              :=  l_excludr_line_id;
                            l_PRICING_ATTR_tbl(l_pa_Count).PRICING_ATTRIBUTE_ID      :=  QP_PRICING_ATTRIBUTES_S.NEXTVAL;
                            l_PRICING_ATTR_tbl(l_pa_Count).PRODUCT_ATTRIBUTE_CONTEXT :=  Pricing_Attr_Rec.PRODUCT_ATTRIBUTE_CONTEXT ;
                            l_PRICING_ATTR_tbl(l_pa_Count).PRODUCT_ATTRIBUTE         :=  Pricing_Attr_Rec.PRODUCT_ATTRIBUTE ;
                            l_PRICING_ATTR_tbl(l_pa_Count).PRODUCT_ATTR_VALUE        :=  Pricing_Attr_Rec.PRODUCT_ATTR_VALUE ;

                            l_PRICING_ATTR_tbl(l_pa_Count).PRICING_ATTRIBUTE         :=  Pricing_Attr_Rec.PRICING_ATTRIBUTE ;
                            l_PRICING_ATTR_tbl(l_pa_Count).PRODUCT_UOM_CODE          :=  Pricing_Attr_Rec.PRODUCT_UOM_CODE ;
                            l_PRICING_ATTR_tbl(l_pa_Count).COMPARISON_OPERATOR_CODE  :=  Pricing_Attr_Rec.COMPARISON_OPERATOR_CODE ;
                            l_PRICING_ATTR_tbl(l_pa_Count).PRICING_ATTRIBUTE_CONTEXT :=  Pricing_Attr_Rec.PRICING_ATTRIBUTE_CONTEXT ;
                            l_PRICING_ATTR_tbl(l_pa_Count).PRICING_ATTR_VALUE_FROM   :=  Pricing_Attr_Rec.PRICING_ATTR_VALUE_FROM ;
                            l_PRICING_ATTR_tbl(l_pa_Count).PRICING_ATTR_VALUE_TO     :=  Pricing_Attr_Rec.PRICING_ATTR_VALUE_TO ;

                            l_PRICING_ATTR_tbl(l_pa_Count).ATTRIBUTE_GROUPING_NO     :=  Pricing_Attr_Rec.ATTRIBUTE_GROUPING_NO;
                            l_PRICING_ATTR_tbl(l_pa_Count).Operation                 := QP_GLOBALS.G_OPR_CREATE;
                            l_PRICING_ATTR_tbl(l_pa_Count).EXCLUDER_FLAG             :=  'Y';
                            l_PRICING_ATTR_tbl(l_pa_Count).MODIFIERS_INDEX           := l_pa_Count;
                            

                        END LOOP ;     -- Pricing Attributes
                    END LOOP;          -- Mod_Line_Rec

                    ----------------------------------------------------------------------------------
                    -- Add Modifier Lines
                    ----------------------------------------------------------------------------------
                    QP_Modifiers_PUB.Process_Modifiers
                       ( p_api_version_number    => 1.0
                       , p_init_msg_list         => FND_API.G_FALSE
                       , p_return_values         => FND_API.G_FALSE
                       , p_commit                => FND_API.G_TRUE
                       , x_return_status         => l_return_status
                       , x_msg_count             => l_msg_count
                       , x_msg_data              => l_msg_data
                       , p_MODIFIER_LIST_rec     => l_MODIFIER_LIST_rec
                       , p_MODIFIERS_tbl         => l_MODIFIERS_tbl
                       , p_QUALIFIERS_tbl        => l_QUALIFIERS_tbl
                       , p_PRICING_ATTR_tbl      => l_PRICING_ATTR_tbl
                       , x_MODIFIER_LIST_rec     => lx_MODIFIER_LIST_rec
                       , x_MODIFIER_LIST_val_rec => lx_MODIFIER_LIST_val_rec
                       , x_MODIFIERS_tbl         => lx_MODIFIERS_tbl
                       , x_MODIFIERS_val_tbl     => lx_MODIFIERS_val_tbl
                       , x_QUALIFIERS_tbl        => lx_QUALIFIERS_tbl
                       , x_QUALIFIERS_val_tbl    => lx_QUALIFIERS_val_tbl
                       , x_PRICING_ATTR_tbl      => lx_PRICING_ATTR_tbl
                       , x_PRICING_ATTR_val_tbl  => lx_PRICING_ATTR_val_tbl );
                    COMMIT;

                    l_sec := 'After QP_Modifiers_PUB.Process_Modifiers';
                    fnd_file.put_line (fnd_file.LOG, l_sec);

                    fnd_file.put_line (fnd_file.LOG, l_sec);
                    fnd_file.put_line (fnd_file.LOG, 'x_return_status -'||l_return_status);
                    fnd_file.put_line (fnd_file.LOG, 'x_msg_count -'||l_msg_count);
                    -- fnd_file.put_line (fnd_file.LOG, 'x_msg_data -'||l_msg_data);

                END IF; -- IF rec_bpw_exl.customer_site_id IS NULL THEN
                l_prev_excludr_line_id := l_excludr_line_id;
             END LOOP;             -- rec_excl_line
         END LOOP;             -- Mod_CSP_Line_Rec

/*
         ----------------------------------------------------------------------------------
         -- Add Modifier Lines
         ----------------------------------------------------------------------------------
         QP_Modifiers_PUB.Process_Modifiers
            ( p_api_version_number    => 1.0
            , p_init_msg_list         => FND_API.G_FALSE
            , p_return_values         => FND_API.G_FALSE
            , p_commit                => FND_API.G_TRUE
            , x_return_status         => l_return_status
            , x_msg_count             => l_msg_count
            , x_msg_data              => l_msg_data
            , p_MODIFIER_LIST_rec     => l_MODIFIER_LIST_rec
            , p_MODIFIERS_tbl         => l_MODIFIERS_tbl
            , p_QUALIFIERS_tbl        => l_QUALIFIERS_tbl
            , p_PRICING_ATTR_tbl      => l_PRICING_ATTR_tbl
            , x_MODIFIER_LIST_rec     => lx_MODIFIER_LIST_rec
            , x_MODIFIER_LIST_val_rec => lx_MODIFIER_LIST_val_rec
            , x_MODIFIERS_tbl         => lx_MODIFIERS_tbl
            , x_MODIFIERS_val_tbl     => lx_MODIFIERS_val_tbl
            , x_QUALIFIERS_tbl        => lx_QUALIFIERS_tbl
            , x_QUALIFIERS_val_tbl    => lx_QUALIFIERS_val_tbl
            , x_PRICING_ATTR_tbl      => lx_PRICING_ATTR_tbl
            , x_PRICING_ATTR_val_tbl  => lx_PRICING_ATTR_val_tbl );
         COMMIT;

         l_sec := 'After QP_Modifiers_PUB.Process_Modifiers';
         fnd_file.put_line (fnd_file.LOG, l_sec);

         fnd_file.put_line (fnd_file.LOG, l_sec);
         fnd_file.put_line (fnd_file.LOG, 'x_return_status -'||l_return_status);
         fnd_file.put_line (fnd_file.LOG, 'x_msg_count -'||l_msg_count);
         -- fnd_file.put_line (fnd_file.LOG, 'x_msg_data -'||l_msg_data);
*/
      END LOOP;                                                  -- cur_bpw_exl
      COMMIT;
      ----------------------------------------------------------------------------------

      END LOOP;                                                  -- cur_bpw

      fnd_file.put_line (fnd_file.LOG, '##############################################################################');
      fnd_file.put_line (fnd_file.LOG, ' Process Inactivated Segment Modifiers '||TO_CHAR(SYSDATE,'DD-MON-YY HH24:MI:SS'));
      fnd_file.put_line (fnd_file.LOG, '##############################################################################');

      -- ############################################################################## --
      ----------------------------------------------------------------------------------
      -- Process Inactivated Segment Modifiers
      ----------------------------------------------------------------------------------
      -- ############################################################################## --

      IF p_process_method = 'NEW' THEN
      FOR rec_inactv_mod IN cur_inactv_mod(l_process_date) LOOP

         l_sec := 'Inside Inactivated Segment Modifiers loop';
         fnd_file.put_line (fnd_file.LOG, l_sec);

         ----------------------------------------------------------------------------------
         -- Set Exclusive CSP Segment DFF on Header Level CSP to 'No' 
         -- when Segment Modifier is Inactive
         ----------------------------------------------------------------------------------
         BEGIN
            UPDATE xxwc.xxwc_om_contract_pricing_hdr
               SET attribute2           = 'N'
             WHERE agreement_id         = rec_inactv_mod.agreement_id
               AND NVL(attribute2, 'N') = 'Y';
            COMMIT;
         EXCEPTION
         WHEN OTHERS THEN
           NULL;
         END;

            --================================================================================================================--
            -- Update all Best Price Wins modifier lines to Incompatibility = LEVEL 1 -- Start >>
            --================================================================================================================--
            l_sec := 'Best Price Wins Agreement Header - Cursor';
            fnd_file.put_line (fnd_file.LOG, l_sec);

            l_error_message := ' ';
            l_MODIFIER_LIST_rec                    := QP_MODIFIERS_PUB.G_MISS_MODIFIER_LIST_REC;
            l_MODIFIERS_Tbl                        := QP_MODIFIERS_PUB.G_MISS_MODIFIERS_TBL;
            l_QUALIFIERS_tbl                       := QP_MODIFIERS_PUB.G_MISS_QUALIFIERS_TBL;
            l_PRICING_ATTR_Tbl                     := QP_MODIFIERS_PUB.G_MISS_PRICING_ATTR_TBL;
            l_ln_count                             := 0;

            l_MODIFIER_LIST_rec.List_Header_ID          := rec_inactv_mod.csp_list_header_id;
            l_MODIFIER_LIST_rec.operation               := QP_GLOBALS.G_OPR_UPDATE;                 -- G_OPR_CREATE ;

            ----------------------------------------------------------------------------------
            -- Update Best Price Wins CSP Modifiers INCOMPATIBILITY_GRP_CODE 
            ----------------------------------------------------------------------------------
            l_ln_count := 0;
            l_ql_Count := 0;

            l_seg_list_header_id := NULL;
            IF rec_inactv_mod.modifier_type = 'MATRIX' THEN
              l_seg_list_header_id := rec_inactv_mod.csp_list_header_id;
            ELSE
              BEGIN
              SELECT qlh_s.list_header_id
                INTO l_seg_list_header_id
                FROM qp_list_headers_all          qlh_s
                   , qp_qualifiers                qq_s
               WHERE qlh_s.list_header_id       = qq_s.list_header_id
                 AND qlh_s.attribute10      = 'Segmented Price'
                 AND qq_s.qualifier_attr_value  = TO_CHAR(rec_inactv_mod.customer_id)
                 AND qq_s.qualifier_attribute   = 'QUALIFIER_ATTRIBUTE32';
              EXCEPTION
              WHEN OTHERS THEN
                 NULL;
              END;
            END IF;

            -- FOR Mod_Line_Rec IN cur_bpw_mod_line (rec_inactv_mod.csp_list_header_id, NULL, 'Level 1 Incompatibility') LOOP
            FOR Mod_Line_Rec IN cur_bpw_mod_line (l_seg_list_header_id, rec_inactv_mod.csp_list_header_id, NULL, 'Exclusive Group','N') LOOP

                  l_ln_count :=  l_ln_count + 1;
                  l_sec := 'Update Best Price Wins CSP Modifiers INCOMPATIBILITY_GRP_CODE';
                  fnd_file.put_line (fnd_file.LOG, l_sec);
                  fnd_file.put_line (fnd_file.LOG, 'LineId - '||mod_line_rec.list_line_id);


                   l_MODIFIERS_tbl(l_Ln_Count).LIST_LINE_ID                := Mod_Line_Rec.LIST_LINE_ID ;
                   l_MODIFIERS_tbl(l_Ln_Count).LIST_LINE_NO                := NVL(Mod_Line_Rec.LIST_LINE_NO, Mod_Line_Rec.LIST_LINE_ID)  ;

                   If Mod_Line_Rec.MODIFIER_LEVEL_CODE IS NOT NULL Then
                      l_MODIFIERS_tbl(l_Ln_Count).MODIFIER_LEVEL_CODE         := Mod_Line_Rec.MODIFIER_LEVEL_CODE ;
                   End If;

                   If Mod_Line_Rec.LIST_LINE_TYPE_CODE IS NOT NULL Then
                      l_MODIFIERS_tbl(l_Ln_Count).LIST_LINE_TYPE_CODE         := Mod_Line_Rec.LIST_LINE_TYPE_CODE;

                   End If;

                   l_MODIFIERS_tbl(l_Ln_Count).START_DATE_ACTIVE           := Mod_Line_Rec.START_DATE_ACTIVE;
                   l_MODIFIERS_tbl(l_Ln_Count).END_DATE_ACTIVE             := Mod_Line_Rec.END_DATE_ACTIVE;

                   If Mod_Line_Rec.AUTOMATIC_FLAG IS NOT NULL Then
                      l_MODIFIERS_tbl(l_Ln_Count).AUTOMATIC_FLAG              := Mod_Line_Rec.AUTOMATIC_FLAG;
                   End If;

                   If Mod_Line_Rec.OVERRIDE_FLAG  Is Not NULL Then
                      l_MODIFIERS_tbl(l_Ln_Count).OVERRIDE_FLAG               := Mod_Line_Rec.OVERRIDE_FLAG;
                   End If;

                   If Mod_Line_Rec.PRICING_PHASE_ID Is Not Null Then
                      l_MODIFIERS_tbl(l_Ln_Count).PRICING_PHASE_ID            := Mod_Line_Rec.PRICING_PHASE_ID;
                   End If;

                   If  Mod_Line_Rec.PRODUCT_PRECEDENCE Is Not Null Then
                      l_MODIFIERS_tbl(l_Ln_Count).PRODUCT_PRECEDENCE          := Mod_Line_Rec.PRODUCT_PRECEDENCE;
                   End If;

                   l_MODIFIERS_tbl(l_Ln_Count).PRICE_BREAK_TYPE_CODE       := Mod_Line_Rec.PRICE_BREAK_TYPE_CODE;
                   l_MODIFIERS_tbl(l_Ln_Count).ARITHMETIC_OPERATOR         := Mod_Line_Rec.ARITHMETIC_OPERATOR ;
                   l_MODIFIERS_tbl(l_Ln_Count).OPERAND                     := Mod_Line_Rec.OPERAND ;
                   l_MODIFIERS_tbl(l_Ln_Count).ACCRUAL_FLAG                := FND_API.G_MISS_CHAR ;

                   IF rec_inactv_mod.scenario = 'Inactive_Matrx' THEN
                        l_MODIFIERS_tbl(l_Ln_Count).INCOMPATIBILITY_GRP_CODE    := 'LVL 1';
                   ELSIF rec_inactv_mod.scenario = 'Inactive_Excl_CSP' THEN
                        l_MODIFIERS_tbl(l_Ln_Count).INCOMPATIBILITY_GRP_CODE    := Mod_Line_Rec.incompatibility_grp_code;
                   END IF;

                   If Mod_Line_Rec.PRICING_GROUP_SEQUENCE  Is Not Null Then
                      l_MODIFIERS_tbl(l_Ln_Count).PRICING_GROUP_SEQUENCE      := Mod_Line_Rec.PRICING_GROUP_SEQUENCE ;
                   End If;

                   If Mod_Line_Rec.INCLUDE_ON_RETURNS_FLAG Is Not Null Then
                      l_MODIFIERS_tbl(l_Ln_Count).INCLUDE_ON_RETURNS_FLAG     := Mod_Line_Rec.INCLUDE_ON_RETURNS_FLAG ;
                   End If;

                   l_MODIFIERS_tbl(l_Ln_Count).PRICE_BY_FORMULA_ID         := Mod_Line_Rec.PRICE_BY_FORMULA_ID ;
                   l_MODIFIERS_tbl(l_Ln_Count).operation                   := QP_GLOBALS.G_OPR_UPDATE;
                   END LOOP;        -- Mod_Line_Rec

            fnd_file.put_line (fnd_file.LOG, 'l_Ln_Count - '||l_Ln_Count);

            IF l_Ln_Count > 0 THEN
            ----------------------------------------------------------------------------------
            -- Add Modifier Lines
            ----------------------------------------------------------------------------------
            QP_Modifiers_PUB.Process_Modifiers
               ( p_api_version_number    => 1.0
               , p_init_msg_list         => FND_API.G_FALSE
               , p_return_values         => FND_API.G_FALSE
               , p_commit                => FND_API.G_TRUE
               , x_return_status         => l_return_status
               , x_msg_count             => l_msg_count
               , x_msg_data              => l_msg_data
               , p_MODIFIER_LIST_rec     => l_MODIFIER_LIST_rec
               , p_MODIFIERS_tbl         => l_MODIFIERS_tbl
               , p_QUALIFIERS_tbl        => l_QUALIFIERS_tbl
               , p_PRICING_ATTR_tbl      => l_PRICING_ATTR_tbl
               , x_MODIFIER_LIST_rec     => lx_MODIFIER_LIST_rec
               , x_MODIFIER_LIST_val_rec => lx_MODIFIER_LIST_val_rec
               , x_MODIFIERS_tbl         => lx_MODIFIERS_tbl
               , x_MODIFIERS_val_tbl     => lx_MODIFIERS_val_tbl
               , x_QUALIFIERS_tbl        => lx_QUALIFIERS_tbl
               , x_QUALIFIERS_val_tbl    => lx_QUALIFIERS_val_tbl
               , x_PRICING_ATTR_tbl      => lx_PRICING_ATTR_tbl
               , x_PRICING_ATTR_val_tbl  => lx_PRICING_ATTR_val_tbl );

            l_sec := 'After calling Pricing API - '||rec_inactv_mod.modifier_type;
            fnd_file.put_line (fnd_file.LOG, l_sec);
            fnd_file.put_line (fnd_file.LOG, 'x_return_status -'||l_return_status);
            fnd_file.put_line (fnd_file.LOG, 'x_msg_count -'||l_msg_count);
            -- fnd_file.put_line (fnd_file.LOG, 'x_msg_data -'||l_msg_data);

            COMMIT;
            END IF; -- IF l_Ln_Count > 0 THEN

            --================================================================================================================--
            -- Update all Best Price Wins modifier lines to Incompatibility = LEVEL 1 -- End <<
            --================================================================================================================--

            --================================================================================================================--
            -- Delete Qualifier Rules -- Start >>
            --================================================================================================================--
            l_QUALIFIERS_tbl                            := QP_MODIFIERS_PUB.G_MISS_QUALIFIERS_TBL;
            l_qualifier_rules_rec                       := QP_QUALIFIER_RULES_PUB.G_MISS_QUALIFIER_RULES_REC;
            i := 0;
            l_qualifier_rule_id                         := NULL;
            
            
            fnd_file.put_line (fnd_file.LOG, '=================================================================');
            l_sec                                       := 'Delete Qualifier Rules > Start';
            fnd_file.put_line (fnd_file.LOG, l_sec);
            fnd_file.put_line (fnd_file.LOG, '=================================================================');
            
            
            FOR rec_qual_rules IN cur_qual_rules(rec_inactv_mod.customer_id) LOOP

            l_sec                                       := 'Delete Qualifier Rules > Inside Loop - '|| rec_qual_rules.qualifier_rule_id ;
            fnd_file.put_line (fnd_file.LOG, l_sec);

            --------------------------------------------------------------
            -- Delete Qualifier Rules > Start
            --------------------------------------------------------------

            IF l_qualifier_rule_id IS NOT NULL AND l_qualifier_rule_id != rec_qual_rules.qualifier_rule_id THEN

               fnd_file.put_line (fnd_file.LOG, ' Delete Qualifier Rules - l_qualifier_rule_id - '||l_qualifier_rule_id);

               qp_qualifier_rules_pub.Process_Qualifier_Rules(
                     p_api_version_number            =>1.0
                     ,p_init_msg_list                => fnd_api.G_FALSE
                     ,p_return_values                => fnd_api.G_FALSE
                     ,p_commit                       => fnd_api.G_FALSE
                     ,x_return_status                => l_return_status
                     ,x_msg_count                    => l_msg_count
                     ,x_msg_data                     => l_msg_data
                     ,p_QUALIFIER_RULES_rec          => l_qualifier_rules_rec
                     ,p_QUALIFIER_RULES_val_rec      => l_qualifier_rules_val_rec
                     ,p_QUALIFIERS_tbl               => l_QUALIFIERS_tbl
                     ,p_QUALIFIERS_val_tbl           => l_QUALIFIERS_val_tbl
                     ,x_QUALIFIER_RULES_rec          => lx_qualifier_rules_rec
                     ,x_QUALIFIER_RULES_val_rec      => lx_qualifier_rules_val_rec
                     ,x_QUALIFIERS_tbl               => lx_QUALIFIERS_tbl
                     ,x_QUALIFIERS_val_tbl           => lx_QUALIFIERS_val_tbl
                 );
               COMMIT;

               IF l_return_status = fnd_api.G_RET_STS_SUCCESS THEN
                  COMMIT;
                  fnd_file.put_line (fnd_file.LOG, 'Delete Qualifier Rules Success - '||l_qualifier_rule_id);
               ELSE
                  -- ROLLBACK;
                  COMMIT;
                  -- fnd_file.put_line (fnd_file.LOG, 'Error creating Process_Qualifier_Rules is '|| l_msg_data ||' - ' || l_msg_count);
                  fnd_file.put_line (fnd_file.LOG, 'Delete Qualifier Rules  - Failure - '||l_qualifier_rule_id);

                  FOR I IN 1 .. l_msg_count LOOP
                     l_msg_data := oe_msg_pub.get( p_msg_index => i,p_encoded => 'F');
                     fnd_file.put_line (fnd_file.LOG, 'err msg ' || i ||'is:  ' || l_msg_data);
                  END LOOP;
               END IF; -- l_return_status = fnd_api.G_RET_STS_SUCCESS THEN

               l_QUALIFIERS_tbl                            := QP_MODIFIERS_PUB.G_MISS_QUALIFIERS_TBL;
               l_qualifier_rules_rec                       := QP_QUALIFIER_RULES_PUB.G_MISS_QUALIFIER_RULES_REC;
               i := 0;

            END IF; -- IF l_qualifier_rule_id != rec_qual_rules.qualifier_rule_id THEN

              l_qualifier_rule_id                          := rec_qual_rules.qualifier_rule_id;
              l_qualifier_rules_rec.qualifier_rule_id      := rec_qual_rules.qualifier_rule_id;
              l_qualifier_rules_rec.name                   := rec_qual_rules.name;
              --l_qualifier_rules_rec.description            := rec_qual_rules.description;
              l_qualifier_rules_rec.operation              := QP_GLOBALS.G_OPR_UPDATE;

              --------------------------------------------------------------
              -- Create Qualifier Group / Rule
              --------------------------------------------------------------
              i := i+1;
              l_QUALIFIERS_tbl(i).OPERATION                      := QP_GLOBALS.G_OPR_DELETE;
              l_QUALIFIERS_tbl(i).QUALIFIER_ID                   := rec_qual_rules.QUALIFIER_ID;
--              l_QUALIFIERS_tbl(i).EXCLUDER_FLAG                  := rec_qual_rules.EXCLUDER_FLAG;
--              l_QUALIFIERS_tbl(i).QUALIFIER_GROUPING_NO          := rec_qual_rules.QUALIFIER_GROUPING_NO;
--              l_QUALIFIERS_tbl(i).QUALIFIER_CONTEXT              := rec_qual_rules.QUALIFIER_CONTEXT;
--              l_QUALIFIERS_tbl(i).QUALIFIER_ATTRIBUTE            := rec_qual_rules.QUALIFIER_ATTRIBUTE;
--              l_QUALIFIERS_tbl(i).COMPARISON_OPERATOR_CODE       := rec_qual_rules.COMPARISON_OPERATOR_CODE;
--              l_QUALIFIERS_tbl(i).QUALIFIER_ATTR_VALUE           := rec_qual_rules.QUALIFIER_ATTR_VALUE;
--              l_QUALIFIERS_tbl(i).QUAL_ATTR_VALUE_FROM_NUMBER    := rec_qual_rules.QUAL_ATTR_VALUE_FROM_NUMBER;

            --------------------------------------------------------------
            -- Delete Qualifier Rules < End
            --------------------------------------------------------------
            END LOOP;

            -- %%%% If there is only one Qualifier Rule to be updated
            qp_qualifier_rules_pub.Process_Qualifier_Rules(
                  p_api_version_number            =>1.0
                  ,p_init_msg_list                => fnd_api.G_FALSE
                  ,p_return_values                => fnd_api.G_FALSE
                  ,p_commit                       => fnd_api.G_FALSE
                  ,x_return_status                => l_return_status
                  ,x_msg_count                    => l_msg_count
                  ,x_msg_data                     => l_msg_data
                  ,p_QUALIFIER_RULES_rec          => l_qualifier_rules_rec
                  ,p_QUALIFIER_RULES_val_rec      => l_qualifier_rules_val_rec
                  ,p_QUALIFIERS_tbl               => l_QUALIFIERS_tbl
                  ,p_QUALIFIERS_val_tbl           => l_QUALIFIERS_val_tbl
                  ,x_QUALIFIER_RULES_rec          => lx_qualifier_rules_rec
                  ,x_QUALIFIER_RULES_val_rec      => lx_qualifier_rules_val_rec
                  ,x_QUALIFIERS_tbl               => lx_QUALIFIERS_tbl
                  ,x_QUALIFIERS_val_tbl           => lx_QUALIFIERS_val_tbl
              );
            COMMIT;

            IF l_return_status = fnd_api.G_RET_STS_SUCCESS THEN
               COMMIT;
               fnd_file.put_line (fnd_file.LOG, 'Delete Qualifier Rules Success - '||l_qualifier_rule_id);
            ELSE
               -- ROLLBACK;
               COMMIT;
               -- fnd_file.put_line (fnd_file.LOG, 'Error creating Process_Qualifier_Rules is '|| l_msg_data ||' - ' || l_msg_count);
               fnd_file.put_line (fnd_file.LOG, 'Delete Qualifier Rules  - Failure - '||l_qualifier_rule_id);

               FOR I IN 1 .. l_msg_count LOOP
                  l_msg_data := oe_msg_pub.get( p_msg_index => i,p_encoded => 'F');
                  fnd_file.put_line (fnd_file.LOG, 'err msg ' || i ||'is:  ' || l_msg_data);
               END LOOP;
            END IF; -- l_return_status = fnd_api.G_RET_STS_SUCCESS THEN


            fnd_file.put_line (fnd_file.LOG, '=================================================================');
            l_sec                                       := 'Delete Qualifier Rules < End i - '||i||' l_qualifier_rule_id - '||l_qualifier_rule_id ;
            fnd_file.put_line (fnd_file.LOG, l_sec);
            fnd_file.put_line (fnd_file.LOG, '=================================================================');

            --================================================================================================================--
            -- Delete Qualifier Rules -- End <<
            --================================================================================================================--

            --================================================================================================================--
            -- Delete all qualifiers -- Start >>
            --================================================================================================================--
            l_sec := 'Delete all qualifiers -- Start >> '||rec_inactv_mod.modifier_type;
            fnd_file.put_line (fnd_file.LOG, l_sec);

            l_error_message := ' ';
            l_MODIFIER_LIST_rec                    := QP_MODIFIERS_PUB.G_MISS_MODIFIER_LIST_REC;
            l_MODIFIERS_Tbl                        := QP_MODIFIERS_PUB.G_MISS_MODIFIERS_TBL;
            l_QUALIFIERS_tbl                       := QP_MODIFIERS_PUB.G_MISS_QUALIFIERS_TBL;
            l_PRICING_ATTR_Tbl                     := QP_MODIFIERS_PUB.G_MISS_PRICING_ATTR_TBL;
            l_ln_count                             := 0;

            l_MODIFIER_LIST_rec.List_Header_ID          := rec_inactv_mod.csp_list_header_id;
            l_MODIFIER_LIST_rec.operation               := QP_GLOBALS.G_OPR_UPDATE;                 -- G_OPR_CREATE ;

            ----------------------------------------------------------------------------------
            -- Delete all qualifiers
            ----------------------------------------------------------------------------------
            l_ln_count := 0;
            l_ql_Count := 0;

            FOR Mod_Line_Rec IN cur_bpw_mod_line (l_seg_list_header_id, rec_inactv_mod.csp_list_header_id, NULL, NULL,'N') LOOP

                  l_ln_count :=  l_ln_count + 1;
                  l_sec := 'Delete all qualifiers';
                  fnd_file.put_line (fnd_file.LOG, l_sec);


                   l_MODIFIERS_tbl(l_Ln_Count).LIST_LINE_ID                := Mod_Line_Rec.LIST_LINE_ID ;
                   --l_MODIFIERS_tbl(l_Ln_Count).LIST_LINE_NO                := NVL(Mod_Line_Rec.LIST_LINE_NO, Mod_Line_Rec.LIST_LINE_ID)  ;
                   l_MODIFIERS_tbl(l_Ln_Count).operation                   := QP_GLOBALS.G_OPR_UPDATE;

                      ----------------------------------------------------------------------------------
                      -- Delete Qualifiers > Start
                      ----------------------------------------------------------------------------------
                      FOR rec_qualifiers IN cur_qualifiers(Mod_Line_Rec.LIST_LINE_ID) LOOP

                          l_qualifier_rec                                         := QP_QUALIFIER_RULES_PUB.G_MISS_QUALIFIERS_REC;
                          l_ql_Count                                              := l_ql_Count + 1;

                          l_qualifier_rec.LIST_HEADER_ID                          :=  rec_qualifiers.list_header_id;
                          l_qualifier_rec.LIST_LINE_ID                            :=  rec_qualifiers.list_line_id;
                          l_qualifier_rec.QUALIFIER_ID                            :=  rec_qualifiers.qualifier_id;
--                          l_qualifier_rec.QUALIFIER_GROUPING_NO                   :=  rec_qualifiers.QUALIFIER_GROUPING_NO ;
--                          l_qualifier_rec.QUALIFIER_CONTEXT                       :=  rec_qualifiers.QUALIFIER_CONTEXT ;
--                          l_qualifier_rec.QUALIFIER_ATTRIBUTE                     :=  rec_qualifiers.QUALIFIER_ATTRIBUTE;
--                          l_qualifier_rec.QUALIFIER_ATTR_VALUE                    :=  rec_qualifiers.QUALIFIER_ATTR_VALUE;
--                          l_qualifier_rec.COMPARISON_OPERATOR_CODE                :=  rec_qualifiers.COMPARISON_OPERATOR_CODE;
--                          l_qualifier_rec.QUALIFIER_PRECEDENCE                    :=  rec_qualifiers.QUALIFIER_PRECEDENCE;
--                          l_qualifier_rec.Excluder_Flag                           :=  rec_qualifiers.EXCLUDER_FLAG;
                          l_qualifier_rec.Operation                               := QP_GLOBALS.G_OPR_DELETE;

                          l_QUALIFIERS_tbl(l_ql_Count)               := l_qualifier_rec;
                      END LOOP; -- rec_qualifiers
                      ----------------------------------------------------------------------------------
                      -- Delete Qualifiers < End
                      ----------------------------------------------------------------------------------

            END LOOP;        -- Mod_Line_Rec

            fnd_file.put_line (fnd_file.LOG, 'l_Ln_Count - '||l_Ln_Count);

            ----------------------------------------------------------------------------------
            -- Add Modifier Lines
            ----------------------------------------------------------------------------------
            QP_Modifiers_PUB.Process_Modifiers
               ( p_api_version_number    => 1.0
               , p_init_msg_list         => FND_API.G_FALSE
               , p_return_values         => FND_API.G_FALSE
               , p_commit                => FND_API.G_TRUE
               , x_return_status         => l_return_status
               , x_msg_count             => l_msg_count
               , x_msg_data              => l_msg_data
               , p_MODIFIER_LIST_rec     => l_MODIFIER_LIST_rec
               , p_MODIFIERS_tbl         => l_MODIFIERS_tbl
               , p_QUALIFIERS_tbl        => l_QUALIFIERS_tbl
               , p_PRICING_ATTR_tbl      => l_PRICING_ATTR_tbl
               , x_MODIFIER_LIST_rec     => lx_MODIFIER_LIST_rec
               , x_MODIFIER_LIST_val_rec => lx_MODIFIER_LIST_val_rec
               , x_MODIFIERS_tbl         => lx_MODIFIERS_tbl
               , x_MODIFIERS_val_tbl     => lx_MODIFIERS_val_tbl
               , x_QUALIFIERS_tbl        => lx_QUALIFIERS_tbl
               , x_QUALIFIERS_val_tbl    => lx_QUALIFIERS_val_tbl
               , x_PRICING_ATTR_tbl      => lx_PRICING_ATTR_tbl
               , x_PRICING_ATTR_val_tbl  => lx_PRICING_ATTR_val_tbl );

            l_sec := 'Delete all qualifiers ';
            fnd_file.put_line (fnd_file.LOG, l_sec);
            fnd_file.put_line (fnd_file.LOG, 'x_return_status -'||l_return_status);
            fnd_file.put_line (fnd_file.LOG, 'x_msg_count -'||l_msg_count);
            -- fnd_file.put_line (fnd_file.LOG, 'x_msg_data -'||l_msg_data);

            COMMIT;

            l_sec := 'Delete all qualifiers -- End >> '||rec_inactv_mod.modifier_type;
            fnd_file.put_line (fnd_file.LOG, l_sec);
            --================================================================================================================--
            -- Delete all qualifiers -- End <<
            --================================================================================================================--

            --================================================================================================================--
            -- Delete Excluders from Best Price Wins Modifier -- Start >>
            --================================================================================================================--

               l_sec := 'Delete Excluders from Best Price Wins Modifier';
               fnd_file.put_line (fnd_file.LOG, l_sec);

               l_error_message := ' ';
               l_MODIFIER_LIST_rec                    := QP_MODIFIERS_PUB.G_MISS_MODIFIER_LIST_REC;
               l_QUALIFIERS_tbl                       := QP_MODIFIERS_PUB.G_MISS_QUALIFIERS_TBL;

               l_ln_count                             := 0;
               

               l_MODIFIER_LIST_rec.list_header_id     := rec_inactv_mod.csp_list_header_id;

               l_MODIFIER_LIST_rec.operation          := QP_GLOBALS.G_OPR_UPDATE;                 -- G_OPR_CREATE ;

               ----------------------------------------------------------------------------------
               -- Update CSP lines as Exclusions
               ----------------------------------------------------------------------------------
               l_ln_count := 0;
               l_MODIFIERS_Tbl                        := QP_MODIFIERS_PUB.G_MISS_MODIFIERS_TBL;
               l_pa_Count := 0;
               l_PRICING_ATTR_Tbl                     := QP_MODIFIERS_PUB.G_MISS_PRICING_ATTR_TBL;
               l_prev_excludr_line_id                 := -1;

               FOR Mod_Line_Rec IN cur_csp_mod_line(rec_inactv_mod.csp_list_header_id, 'Y') LOOP
                  IF l_prev_excludr_line_id != Mod_Line_Rec.list_line_id THEN

                     l_sec := 'Loop Modifier BPW CSP Lines for Delete ';
                     fnd_file.put_line (fnd_file.LOG, l_sec);

                     fnd_file.put_line (fnd_file.LOG, 'bpw_list_header_id - '||rec_inactv_mod.csp_list_header_id);
                     l_ln_count                                               := l_ln_count + 1;
                     l_excludr_line_id                                        := Mod_Line_Rec.list_line_id;

                     fnd_file.put_line (fnd_file.LOG, 'l_excludr_line_id - '||l_excludr_line_id);
                     l_MODIFIERS_tbl(l_Ln_Count).LIST_LINE_ID                 := l_excludr_line_id;       -- QP_LIST_LINES_S.NEXTVAL;

                     If Mod_Line_Rec.MODIFIER_LEVEL_CODE IS NOT NULL Then
                        l_MODIFIERS_tbl(l_Ln_Count).MODIFIER_LEVEL_CODE       := Mod_Line_Rec.MODIFIER_LEVEL_CODE ;
                     End If;

                     If Mod_Line_Rec.LIST_LINE_TYPE_CODE IS NOT NULL Then
                        l_MODIFIERS_tbl(l_Ln_Count).LIST_LINE_TYPE_CODE       := Mod_Line_Rec.LIST_LINE_TYPE_CODE;

                     End If;

                     l_MODIFIERS_tbl(l_Ln_Count).START_DATE_ACTIVE            := Mod_Line_Rec.START_DATE_ACTIVE;
                     l_MODIFIERS_tbl(l_Ln_Count).END_DATE_ACTIVE              := Mod_Line_Rec.END_DATE_ACTIVE;

                     If Mod_Line_Rec.AUTOMATIC_FLAG IS NOT NULL Then
                        l_MODIFIERS_tbl(l_Ln_Count).AUTOMATIC_FLAG            := Mod_Line_Rec.AUTOMATIC_FLAG;
                     End If;

                     If Mod_Line_Rec.OVERRIDE_FLAG  Is Not NULL Then
                        l_MODIFIERS_tbl(l_Ln_Count).OVERRIDE_FLAG             := Mod_Line_Rec.OVERRIDE_FLAG;
                     End If;

                     If Mod_Line_Rec.PRICING_PHASE_ID Is Not Null Then
                        l_MODIFIERS_tbl(l_Ln_Count).PRICING_PHASE_ID          := Mod_Line_Rec.PRICING_PHASE_ID;
                     End If;

                     If  Mod_Line_Rec.PRODUCT_PRECEDENCE Is Not Null Then
                        l_MODIFIERS_tbl(l_Ln_Count).PRODUCT_PRECEDENCE        := Mod_Line_Rec.PRODUCT_PRECEDENCE;
                     End If;

                     l_MODIFIERS_tbl(l_Ln_Count).PRICE_BREAK_TYPE_CODE        := Mod_Line_Rec.PRICE_BREAK_TYPE_CODE;
                     l_MODIFIERS_tbl(l_Ln_Count).ARITHMETIC_OPERATOR          := Mod_Line_Rec.ARITHMETIC_OPERATOR ;
                     l_MODIFIERS_tbl(l_Ln_Count).OPERAND                      := Mod_Line_Rec.OPERAND ;
                     l_MODIFIERS_tbl(l_Ln_Count).ACCRUAL_FLAG                 := FND_API.G_MISS_CHAR ;--Mod_Line_Rec.ACCRUAL_FLAG ;
                     l_MODIFIERS_tbl(l_Ln_Count).INCOMPATIBILITY_GRP_CODE     := Mod_Line_Rec.INCOMPATIBILITY_GRP_CODE ;

                     If Mod_Line_Rec.PRICING_GROUP_SEQUENCE  Is Not Null Then
                        l_MODIFIERS_tbl(l_Ln_Count).PRICING_GROUP_SEQUENCE    := Mod_Line_Rec.PRICING_GROUP_SEQUENCE ;
                     End If;

                     If Mod_Line_Rec.INCLUDE_ON_RETURNS_FLAG Is Not Null Then
                        l_MODIFIERS_tbl(l_Ln_Count).INCLUDE_ON_RETURNS_FLAG   := Mod_Line_Rec.INCLUDE_ON_RETURNS_FLAG ;
                     End If;

                     l_MODIFIERS_tbl(l_Ln_Count).PRICE_BY_FORMULA_ID          := Mod_Line_Rec.PRICE_BY_FORMULA_ID ;

                       l_MODIFIERS_tbl(l_Ln_Count).operation                    := QP_GLOBALS.G_OPR_UPDATE;   -- G_OPR_CREATE;

                       ----------------------------------------------------------------------------------
                       -- Delete Exclusion Pricing Attributes
                       ----------------------------------------------------------------------------------

                       FOR Pricing_Attr_Rec In Get_Pricing_Attr_Cur (rec_inactv_mod.csp_list_header_id, Mod_Line_Rec.list_line_id, 'Y') LOOP

                           l_sec := 'Delete Exclusion Pricing Attributes ';
                           fnd_file.put_line (fnd_file.LOG, l_sec);

                           l_pa_Count := l_pa_Count + 1;

                           l_PRICING_ATTR_tbl(l_pa_Count).LIST_HEADER_ID            :=  rec_inactv_mod.csp_list_header_id;
                           l_PRICING_ATTR_tbl(l_pa_Count).LIST_LINE_ID              :=  l_excludr_line_id;


                           l_PRICING_ATTR_tbl(l_pa_Count).PRICING_ATTRIBUTE_ID      :=  Pricing_Attr_Rec.PRICING_ATTRIBUTE_ID ;
                           l_PRICING_ATTR_tbl(l_pa_Count).PRODUCT_ATTRIBUTE_CONTEXT :=  Pricing_Attr_Rec.PRODUCT_ATTRIBUTE_CONTEXT ;
                           l_PRICING_ATTR_tbl(l_pa_Count).PRODUCT_ATTRIBUTE         :=  Pricing_Attr_Rec.PRODUCT_ATTRIBUTE ;
                           l_PRICING_ATTR_tbl(l_pa_Count).PRODUCT_ATTR_VALUE        :=  Pricing_Attr_Rec.PRODUCT_ATTR_VALUE ;

                           l_PRICING_ATTR_tbl(l_pa_Count).PRICING_ATTRIBUTE         :=  Pricing_Attr_Rec.PRICING_ATTRIBUTE ;
                           l_PRICING_ATTR_tbl(l_pa_Count).PRODUCT_UOM_CODE          :=  Pricing_Attr_Rec.PRODUCT_UOM_CODE ;
                           l_PRICING_ATTR_tbl(l_pa_Count).COMPARISON_OPERATOR_CODE  :=  Pricing_Attr_Rec.COMPARISON_OPERATOR_CODE ;
                           l_PRICING_ATTR_tbl(l_pa_Count).PRICING_ATTRIBUTE_CONTEXT :=  Pricing_Attr_Rec.PRICING_ATTRIBUTE_CONTEXT ;
                           l_PRICING_ATTR_tbl(l_pa_Count).PRICING_ATTR_VALUE_FROM   :=  Pricing_Attr_Rec.PRICING_ATTR_VALUE_FROM ;
                           l_PRICING_ATTR_tbl(l_pa_Count).PRICING_ATTR_VALUE_TO     :=  Pricing_Attr_Rec.PRICING_ATTR_VALUE_TO ;

                           l_PRICING_ATTR_tbl(l_pa_Count).ATTRIBUTE_GROUPING_NO     :=  Pricing_Attr_Rec.ATTRIBUTE_GROUPING_NO;
                           l_PRICING_ATTR_tbl(l_pa_Count).MODIFIERS_INDEX           := l_pa_Count;

                           l_PRICING_ATTR_tbl(l_pa_Count).Operation                 :=  QP_GLOBALS.G_OPR_DELETE;
                           l_PRICING_ATTR_tbl(l_pa_Count).EXCLUDER_FLAG             :=  'Y';

                       END LOOP ;     -- Pricing Attributes
                  END IF; -- IF l_prev_excludr_line_id != l_excludr_line_id THEN
                  l_prev_excludr_line_id := l_excludr_line_id;
               END LOOP;          -- cur_csp_mod_line

               ----------------------------------------------------------------------------------
               -- Add Modifier Lines
               ----------------------------------------------------------------------------------
               QP_Modifiers_PUB.Process_Modifiers
                  ( p_api_version_number    => 1.0
                  , p_init_msg_list         => FND_API.G_FALSE
                  , p_return_values         => FND_API.G_FALSE
                  , p_commit                => FND_API.G_TRUE
                  , x_return_status         => l_return_status
                  , x_msg_count             => l_msg_count
                  , x_msg_data              => l_msg_data
                  , p_MODIFIER_LIST_rec     => l_MODIFIER_LIST_rec
                  , p_MODIFIERS_tbl         => l_MODIFIERS_tbl
                  , p_QUALIFIERS_tbl        => l_QUALIFIERS_tbl
                  , p_PRICING_ATTR_tbl      => l_PRICING_ATTR_tbl
                  , x_MODIFIER_LIST_rec     => lx_MODIFIER_LIST_rec
                  , x_MODIFIER_LIST_val_rec => lx_MODIFIER_LIST_val_rec
                  , x_MODIFIERS_tbl         => lx_MODIFIERS_tbl
                  , x_MODIFIERS_val_tbl     => lx_MODIFIERS_val_tbl
                  , x_QUALIFIERS_tbl        => lx_QUALIFIERS_tbl
                  , x_QUALIFIERS_val_tbl    => lx_QUALIFIERS_val_tbl
                  , x_PRICING_ATTR_tbl      => lx_PRICING_ATTR_tbl
                  , x_PRICING_ATTR_val_tbl  => lx_PRICING_ATTR_val_tbl );
               COMMIT;

               l_sec := 'After QP_Modifiers_PUB.Process_Modifiers';
               fnd_file.put_line (fnd_file.LOG, l_sec);

               fnd_file.put_line (fnd_file.LOG, 'x_return_status -'||l_return_status);
               fnd_file.put_line (fnd_file.LOG, 'x_msg_count -'||l_msg_count);

            --================================================================================================================--
            -- Delete Excluders from Best Price Wins Modifier -- End <<
            --================================================================================================================--

            xxwc_om_csp_interface_pkg.create_exclusions (rec_inactv_mod.agreement_id);
         -- END IF; -- IF rec_inactv_mod.incompatability_group = 'Best Price Wins' THEN

      END LOOP;   -- cur_inactv_mod

      COMMIT;
      main (l_errbuf
          , l_retcode
          , p_process_date
          , p_in_seg_list_header_id
          , p_in_csp_list_header_id -- BPW Modifier
          , 'RESUBMIT'
          );
      END IF; -- IF p_process_method = 'NEW' THEN

   EXCEPTION
   WHEN OTHERS THEN
      xxcus_error_pkg.xxcus_error_main_api (
         p_called_from         => 'XXWC_PRICING_SEGMENT_PKG.MAIN',
         p_calling             => l_sec,
         p_request_id          => fnd_global.conc_request_id,
         p_ora_error_msg       => SQLERRM,
         p_error_desc          => 'Error in Pricing Segment Process - Main procedure, When Others Exception',
         p_distribution_list   => g_dflt_email,
         p_module              => 'XXWC');

      fnd_file.put_line (fnd_file.LOG, 'Error in  main procedure - ' || SQLERRM);
END main;


/*************************************************************************
      FUNCTION Name: is_valid_qualifier

      PURPOSE:   Check if the qualifier if valid for Segment Modifier.

      REVISIONS:
      Ver        Date        Author                     Description
      ---------  ----------  ---------------         -------------------------
      1.0        10/09/2015  Gopi Damuluri           Initial Version
                                                     TMS# 20151111-00161
      1.1        11/14/2015  Gopi Damuluri           TMS# 20151115-00001 
                                                     Resolve invallid number issue.
   ***************************************************************************/
FUNCTION is_valid_qualifier (p_qual_product_context   IN VARCHAR2
                           , p_qual_product_attr_val  IN VARCHAR2
                           , p_seg_list_line_id       IN NUMBER
                              )
RETURN VARCHAR2 IS

is_valid_qualif_flag VARCHAR2(1) := 'N';
l_sec                VARCHAR2 (100);

BEGIN

       ----------------------------------------------------------------------------------
       -- Qualifier at Item Level
       ----------------------------------------------------------------------------------
       l_sec := 'Qualifier at Item Level';
       IF p_qual_product_context = 'ITEM_NUMBER' THEN

          ----------------------------------------------------------------------------------
          -- Check if Segment Modifier is at Item Level
          ----------------------------------------------------------------------------------
          l_sec := 'Check if Segment Modifier is at Item Level';
          BEGIN
          SELECT 'Y'
            INTO is_valid_qualif_flag
            FROM qp_modifier_summary_v seg
           WHERE seg.list_line_id             = p_seg_list_line_id
             AND seg.product_attr             = 'PRICING_ATTRIBUTE1' -- Item
             AND seg.product_attr_val         = p_qual_product_attr_val
             AND NVL(seg.excluder_Flag,'N')   = 'N'
             AND SYSDATE BETWEEN TRUNC(NVL(seg.start_date_active, SYSDATE - 1)) AND TRUNC(NVL(seg.end_date_active, SYSDATE+ 1))
             AND ROWNUM = 1
             ;
          EXCEPTION
          WHEN OTHERS THEN
            is_valid_qualif_flag := 'N';
          END;

          ----------------------------------------------------------------------------------
          -- Check if Segment Modifier is at Item Category Level
          ----------------------------------------------------------------------------------
          l_sec := 'Check if Segment Modifier is at Item Category Level';
          IF is_valid_qualif_flag = 'N' THEN
             BEGIN
             SELECT 'Y'
               INTO is_valid_qualif_flag
               FROM qp_modifier_summary_v seg
                  , mtl_item_categories   mic
              WHERE seg.list_line_id             = p_seg_list_line_id
                AND seg.product_attr             = 'PRICING_ATTRIBUTE2' -- Item Category
                -- AND TO_NUMBER(seg.product_attr_val)  = mic.category_id -- Version# 1.1 
                AND seg.product_attr_val         = TO_CHAR(mic.category_id) -- Version# 1.1
                AND mic.inventory_item_id        = TO_NUMBER(p_qual_product_attr_val)
                AND NVL(seg.excluder_Flag,'N')   = 'N'
                AND mic.organization_id          = 222
                AND SYSDATE BETWEEN TRUNC(NVL(seg.start_date_active, SYSDATE - 1)) AND TRUNC(NVL(seg.end_date_active, SYSDATE+ 1))
                AND ROWNUM = 1
                ;
             EXCEPTION
             WHEN OTHERS THEN
               is_valid_qualif_flag := 'N';
             END;
          END IF; -- IF is_valid_qualif_flag = 'N' THEN
       END IF;

       ----------------------------------------------------------------------------------
       -- Qualifier at Item Category Level
       ----------------------------------------------------------------------------------
       l_sec := 'Qualifier at Item Category Level';
       IF p_qual_product_context = 'ITEM_CAT' THEN
          ----------------------------------------------------------------------------------
          -- Check if Segment Modifier is at Item Category Level
          ----------------------------------------------------------------------------------
          l_sec := 'Check if Segment Modifier is at Item Category Level';
          BEGIN
          SELECT 'Y'
            INTO is_valid_qualif_flag
            FROM qp_modifier_summary_v seg
           WHERE seg.list_line_id             = p_seg_list_line_id
             AND seg.product_attr             = 'PRICING_ATTRIBUTE2' -- Item Category
             AND seg.product_attr_val         = p_qual_product_attr_val
             AND NVL(seg.excluder_Flag,'N')   = 'N'
             AND SYSDATE BETWEEN TRUNC(NVL(seg.start_date_active, SYSDATE - 1)) AND TRUNC(NVL(seg.end_date_active, SYSDATE+ 1))
             AND ROWNUM = 1
             ;
          EXCEPTION
          WHEN OTHERS THEN
            is_valid_qualif_flag := 'N';
          END;

          ----------------------------------------------------------------------------------
          -- Check if Segment Modifier is at Item Level
          ----------------------------------------------------------------------------------
          IF is_valid_qualif_flag = 'N' THEN
             l_sec := 'Check if Segment Modifier is at Item Level';
             BEGIN
             SELECT 'Y'
               INTO is_valid_qualif_flag
               FROM qp_modifier_summary_v seg
                  , mtl_item_categories   mic
              WHERE seg.list_line_id             = p_seg_list_line_id
                AND seg.product_attr             = 'PRICING_ATTRIBUTE1' -- Item
                AND mic.category_id              = TO_NUMBER(p_qual_product_attr_val)
                -- AND mic.inventory_item_id        = TO_NUMBER(seg.product_attr_val) -- Version# 1.1
                AND TO_CHAR(mic.inventory_item_id)  = seg.product_attr_val -- Version# 1.1
                AND NVL(seg.excluder_Flag,'N')   = 'N'
                AND mic.organization_id = 222
                AND SYSDATE BETWEEN TRUNC(NVL(seg.start_date_active, SYSDATE - 1)) AND TRUNC(NVL(seg.end_date_active, SYSDATE+ 1))
                AND ROWNUM = 1
                ;
             EXCEPTION
             WHEN OTHERS THEN
               is_valid_qualif_flag := 'N';
             END;
          END IF; -- IF is_valid_qualif_flag = 'N' THEN
       END IF;

RETURN is_valid_qualif_flag;

EXCEPTION
WHEN OTHERS THEN

   xxcus_error_pkg.xxcus_error_main_api (
      p_called_from         => 'XXWC_PRICING_SEGMENT_PKG.IS_VALID_QUALIFIER',
      p_calling             => l_sec,
      p_request_id          => fnd_global.conc_request_id,
      p_ora_error_msg       => SQLERRM,
      p_error_desc          => 'Error in Pricing Segment Process - is_valid_qualifier procedure, When Others Exception',
      p_distribution_list   => g_dflt_email,
      p_module              => 'XXWC');

   RETURN 'N';
END is_valid_qualifier;

   /*************************************************************************
      FUNCTION Name: get_line_exists

      PURPOSE:   To derive CSP lines to be Excluded from Segment Modifier.

      REVISIONS:
      Ver        Date        Author                     Description
      ---------  ----------  ---------------         -------------------------
      1.0        10/09/2015  Gopi Damuluri           Initial Version
                                                     TMS# 20151111-00161
      1.1        11/14/2015  Gopi Damuluri           TMS# 20151115-00001 
                                                     Resolve invallid number issue.
   ***************************************************************************/
FUNCTION get_line_exists (p_bpw_product_attr      IN VARCHAR2
                             , p_bpw_product_attr_val  IN VARCHAR2
                             , p_seg_list_header_id    IN NUMBER
                              )
RETURN VARCHAR2 IS

l_excludr_line_id NUMBER;
l_line_exists     VARCHAR2(1) := 'N';
l_sec             VARCHAR2 (100);

BEGIN

       ----------------------------------------------------------------------------------
       -- CSP at Item Level
       ----------------------------------------------------------------------------------
       l_sec := 'CSP at Item Level';

       IF p_bpw_product_attr = 'PRICING_ATTRIBUTE1' THEN

          ----------------------------------------------------------------------------------
          -- Check if Segment Modifier is at Item Level
          ----------------------------------------------------------------------------------
          IF l_line_exists = 'N' THEN

             l_sec := 'Check if Segment Modifier is at Item Level';
             BEGIN
             SELECT 'Y'
               INTO l_line_exists
               FROM qp_modifier_summary_v seg
              WHERE seg.list_header_id           = p_seg_list_header_id
                AND seg.product_attr             = 'PRICING_ATTRIBUTE1' -- Item
                AND seg.product_attr_val         = p_bpw_product_attr_val
                AND NVL(seg.excluder_Flag,'N')   = 'N'
                AND SYSDATE BETWEEN TRUNC(NVL(seg.start_date_active, SYSDATE - 1)) AND TRUNC(NVL(seg.end_date_active, SYSDATE+ 1))
                AND ROWNUM = 1
                ;
             EXCEPTION
             WHEN OTHERS THEN
               NULL;
             END;
          END IF;

          ----------------------------------------------------------------------------------
          -- Check if Segment Modifier is at Item Category Level
          ----------------------------------------------------------------------------------
          IF l_line_exists = 'N' THEN
             l_sec := 'Check if Segment Modifier is at Item Category Level';
             BEGIN
             SELECT 'Y'
               INTO l_line_exists
               FROM qp_modifier_summary_v seg
                  , mtl_item_categories   mic
              WHERE seg.list_header_id           = p_seg_list_header_id
                AND seg.product_attr             = 'PRICING_ATTRIBUTE2' -- Item Category
                -- AND TO_NUMBER(seg.product_attr_val)  = mic.category_id -- Version# 1.1 
                AND seg.product_attr_val         = TO_CHAR(mic.category_id) -- Version# 1.1
                AND mic.inventory_item_id        = TO_NUMBER(p_bpw_product_attr_val)
                AND NVL(seg.excluder_Flag,'N')   = 'N'
                AND mic.organization_id          = 222
                AND SYSDATE BETWEEN TRUNC(NVL(seg.start_date_active, SYSDATE - 1)) AND TRUNC(NVL(seg.end_date_active, SYSDATE+ 1))
                AND ROWNUM = 1
                ;
          EXCEPTION
          WHEN OTHERS THEN
            NULL;
          END;
          END IF;
       END IF;

       ----------------------------------------------------------------------------------
       -- CSP at Item Category Level
       ----------------------------------------------------------------------------------
       IF p_bpw_product_attr = 'PRICING_ATTRIBUTE2' THEN
          l_sec := 'CSP at Item Category Level';
          
          ----------------------------------------------------------------------------------
          -- Check if Segment Modifier is at Item Category Level
          ----------------------------------------------------------------------------------
          IF l_line_exists = 'N' THEN
             l_sec := 'Check if Segment Modifier is at Item Category Level';
             BEGIN
             SELECT 'Y'
               INTO l_line_exists
               FROM qp_modifier_summary_v seg
              WHERE seg.list_header_id           = p_seg_list_header_id
                AND seg.product_attr             = 'PRICING_ATTRIBUTE2' -- Item Category
                AND seg.product_attr_val         = p_bpw_product_attr_val
                AND NVL(seg.excluder_Flag,'N')   = 'N'
                AND SYSDATE BETWEEN TRUNC(NVL(seg.start_date_active, SYSDATE - 1)) AND TRUNC(NVL(seg.end_date_active, SYSDATE+ 1))
                AND ROWNUM = 1
                ;
             EXCEPTION
             WHEN OTHERS THEN
               NULL;
             END;
          END IF;

          ----------------------------------------------------------------------------------
          -- Check if Segment Modifier is at Item Level
          ----------------------------------------------------------------------------------
          IF l_line_exists = 'N' THEN
          
          l_sec := 'Check if Segment Modifier is at Item Level';
          BEGIN
          SELECT 'Y'
            INTO l_line_exists
            FROM qp_modifier_summary_v seg
               , mtl_item_categories   mic
           WHERE seg.list_header_id           = p_seg_list_header_id
             AND seg.product_attr             = 'PRICING_ATTRIBUTE1' -- Item
             AND mic.category_id              = TO_NUMBER(p_bpw_product_attr_val)
             -- AND mic.inventory_item_id        = TO_NUMBER(seg.product_attr_val) -- Version# 1.1
             AND TO_CHAR(mic.inventory_item_id)  = seg.product_attr_val -- Version# 1.1
             AND NVL(seg.excluder_Flag,'N')   = 'N'
             AND mic.organization_id = 222
             AND SYSDATE BETWEEN TRUNC(NVL(seg.start_date_active, SYSDATE - 1)) AND TRUNC(NVL(seg.end_date_active, SYSDATE+ 1))
             AND ROWNUM = 1
             ;
          EXCEPTION
          WHEN OTHERS THEN
            NULL;
          END;
          END IF;
       END IF;

RETURN l_line_exists;

EXCEPTION
WHEN OTHERS THEN

      fnd_file.put_line (fnd_file.LOG, 'Error in GET_LINE_EXISTS procedure - '||SQLERRM);

      xxcus_error_pkg.xxcus_error_main_api (
         p_called_from         => 'XXWC_PRICING_SEGMENT_PKG.GET_LINE_EXISTS',
         p_calling             => l_sec,
         p_request_id          => fnd_global.conc_request_id,
         p_ora_error_msg       => SQLERRM,
         p_error_desc          => 'Error in Pricing Segment Process - get_line_exists procedure, When Others Exception',
         p_distribution_list   => g_dflt_email,
         p_module              => 'XXWC');

  RETURN 'N';
END get_line_exists;

   /*************************************************************************
      PROCEDURE Name: import_csp

      PURPOSE:   To update Matrix Modifier when importing CSP.

      REVISIONS:
      Ver        Date        Author                     Description
      ---------  ----------  ---------------         -------------------------
      1.0        10/09/2015  Gopi Damuluri           Initial Version
                                                     TMS# 20151111-00161
   ***************************************************************************/
   PROCEDURE import_csp (p_agreement_id             IN NUMBER)
          IS
       l_seg_list_header_id NUMBER;
       l_errbuf             VARCHAR2 (2000);
       l_retcode            NUMBER;
       l_req_id             NUMBER;
       l_sec                VARCHAR2 (100);
   BEGIN

      l_sec := 'Derive Segment Price list_header_id';
      SELECT qlh_s.list_header_id
        INTO l_seg_list_header_id
        FROM qp_list_headers_all               qlh_s
           , qp_qualifiers                     qq_s
           , xxwc.xxwc_om_contract_pricing_hdr xch_c
       WHERE 1 = 1
         AND xch_c.agreement_id         = p_agreement_id
         AND qq_s.qualifier_attr_value  = TO_CHAR(xch_c.customer_id)
         AND qq_s.qualifier_attribute   = 'QUALIFIER_ATTRIBUTE32'
         AND qlh_s.list_header_id       = qq_s.list_header_id
         AND qlh_s.attribute10          = 'Segmented Price'
         AND ROWNUM = 1;

      l_sec := 'Submit Program - XXWC Segment Modifer Process';
      l_req_id := fnd_request.submit_request(application => 'XXWC'
                          ,program => 'XXWC_SEGMENT_MODIFIER_PROG'
                          ,description => 'XXWC Segment Modifer Process'
                          ,start_time => SYSDATE
                          ,sub_request => FALSE
                          ,argument1 => TO_CHAR(SYSDATE,'DD-MON-YYYY')
                          ,argument2 => l_seg_list_header_id
                          ,argument3 => NULL
                          ,argument4 => 'NEW');

      COMMIT;

    EXCEPTION
    WHEN OTHERS THEN
      fnd_file.put_line (fnd_file.LOG, 'Error in IMPORT_CSP procedure - '||SQLERRM);

      xxcus_error_pkg.xxcus_error_main_api (
         p_called_from         => 'XXWC_PRICING_SEGMENT_PKG.IMPORT_CSP',
         p_calling             => l_sec,
         p_request_id          => fnd_global.conc_request_id,
         p_ora_error_msg       => SQLERRM,
         p_error_desc          => 'Error in Pricing Segment Process - import_csp procedure, When Others Exception',
         p_distribution_list   => g_dflt_email,
         p_module              => 'XXWC');

      fnd_file.put_line (fnd_file.LOG, 'Error in  import_csp procedure - ' || SQLERRM);
    END import_csp;

END XXWC_PRICING_SEGMENT_PKG;
/