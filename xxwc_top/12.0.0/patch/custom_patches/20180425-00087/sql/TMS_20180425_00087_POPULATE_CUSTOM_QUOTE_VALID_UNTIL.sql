/*******************************************************************
  script name: TMS_20180425-00087_POPULATE_CUSTOM_QUOTE_VALID_UNTIL.sql
  purpose: To update valid until date for custom quote
  revision history:
  Version   Date          Aurthor           Description
  ----------------------------------------------------------------
  1.0       26-APR-2018   Niraj K Ranjan    TMS#20180425-00087   Data fix script to populate the Valid until field on WC Quotes
*******************************************************************/
SET serveroutput on size 1000000;
BEGIN
   dbms_output.put_line('TMS_20180425-00087 : '||'Start script');  
   
   update xxwc.xxwc_om_quote_headers
   set valid_until = to_date('30-MAY-2018','DD-MON-YYYY')
   WHERE valid_until is null
   AND trunc (creation_Date) >= TO_DATE('01-JAN-2017','DD-MON-YYYY') AND trunc(creation_Date) <= TO_DATE('28-FEB-2018','DD-MON-YYYY');
   
   dbms_output.put_line('TMS_20180425-00087 : '||'Total records updated for quotes created before 28-FEB-2018 : '||SQL%ROWCOUNT);
   
   update xxwc.xxwc_om_quote_headers
   set valid_until = to_date('30-JUN-2018','DD-MON-YYYY')
   WHERE valid_until is null
   AND trunc(creation_Date) >= TO_DATE('01-MAR-2018','DD-MON-YYYY');
   
   dbms_output.put_line('TMS_20180425-00087 : '||'Total records updated for quotes created after 01-MAR-2018 : '||SQL%ROWCOUNT);
   
   dbms_output.put_line('TMS_20180425-00087 : '||'End script');
   COMMIT;
EXCEPTION
   WHEN OTHERS THEN
      dbms_output.put_line('TMS_20180425-00087 : '||'ERROR - '||SQLERRM);
	  ROLLBACK;
END;
/