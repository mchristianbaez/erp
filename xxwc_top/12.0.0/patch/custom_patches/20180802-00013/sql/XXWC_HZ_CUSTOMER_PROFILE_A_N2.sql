-- ******************************************************************************
-- *   $Header XXWC_HZ_CUSTOMER_PROFILE_A_N2.sql $
-- *   Module Name: Receivables
-- *
-- *   PURPOSE:   Index on AR.XXWC_HZ_CUSTOMER_PROFILE_A_N2
-- *
-- *   REVISIONS:
-- *   Ver        Date        Author                     Description
-- *   ---------  ----------  ---------------         -------------------------
-- *   1.0        08/20/2018   Siva            			TMS#20180802-00013  - Customer Audit History Report - WC report performance
--                                                    
-- * ****************************************************************************
CREATE INDEX AR.XXWC_HZ_CUSTOMER_PROFILE_A_N2 ON AR.HZ_CUSTOMER_PROFILES_A
  (
    CUST_ACCOUNT_PROFILE_ID
  )
  TABLESPACE APPS_TS_TX_DATA
/