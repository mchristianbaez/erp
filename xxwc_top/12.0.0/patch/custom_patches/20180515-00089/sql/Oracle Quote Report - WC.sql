--Report Name            : Oracle Quote Report - WC
--Import Instructions : 1. If RTF template needs to be uploaded to report automatically then move the RTF template to physical path of APPS_DATA_FILE_DIR oracle directory. Check dba_directories view to get the physical path of oracle directory. 
--                         (If you want to use any other oracle directory other than APPS_DATA_FILE_DIR then replace APPS_DATA_FILE_DIR with your oracle directory name below.)
--                      2. Make sure XXEIS schema has read/write permissions to above used oracle directory and give full permissions to the directory at OS level.
--                      3. Run this file in XXEIS user from SQL*Plus or any tool like Toad, SQL Navigator. 
exec xxeis.eis_rsc_ins.g_rtf_dir := 'APPS_DATA_FILE_DIR' ;
prompt Creating Object Data EIS_XXWC_OM_QUOTE_DETAIL_V
set scan off define off
DECLARE
mod_exist VARCHAR2(1);
BEGIN 
mod_exist := XXEIS.EIS_RSC_INSERT.check_if_module_exist(660);
IF mod_exist = 'Y' THEN 
--Inserting Object EIS_XXWC_OM_QUOTE_DETAIL_V
xxeis.eis_rsc_ins.v( 'EIS_XXWC_OM_QUOTE_DETAIL_V',660,'','','','','SA059956','XXEIS','Eis Xxwc Om Quote Detail V','EXOQDV','','','VIEW','US','','','');
--Delete Object Columns for EIS_XXWC_OM_QUOTE_DETAIL_V
xxeis.eis_rsc_utility.delete_view_rows('EIS_XXWC_OM_QUOTE_DETAIL_V',660,FALSE);
--Inserting Object Columns for EIS_XXWC_OM_QUOTE_DETAIL_V
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_QUOTE_DETAIL_V','ORDER_NUMBER',660,'Order Number','ORDER_NUMBER','','','','SA059956','NUMBER','','','Order Number','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_QUOTE_DETAIL_V','ORDERED_DATE',660,'Ordered Date','ORDERED_DATE','','','','SA059956','DATE','','','Ordered Date','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_QUOTE_DETAIL_V','ORDER_TYPE',660,'Order Type','ORDER_TYPE','','','','SA059956','VARCHAR2','','','Order Type','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_QUOTE_DETAIL_V','ORDER_HEADER_STATUS',660,'Order Header Status','ORDER_HEADER_STATUS','','','','SA059956','VARCHAR2','','','Order Header Status','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_QUOTE_DETAIL_V','CUSTOMER_NUMBER',660,'Customer Number','CUSTOMER_NUMBER','','','','SA059956','VARCHAR2','','','Customer Number','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_QUOTE_DETAIL_V','CUSTOMER_NAME',660,'Customer Name','CUSTOMER_NAME','','','','SA059956','VARCHAR2','','','Customer Name','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_QUOTE_DETAIL_V','CUSTOMER_JOB_NAME',660,'Customer Job Name','CUSTOMER_JOB_NAME','','','','SA059956','VARCHAR2','','','Customer Job Name','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_QUOTE_DETAIL_V','ORDER_AMOUNT',660,'Order Amount','ORDER_AMOUNT','','','','SA059956','NUMBER','','','Order Amount','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_QUOTE_DETAIL_V','QUOTE_CREATION_DATE',660,'Quote Creation Date','QUOTE_CREATION_DATE','','','','SA059956','DATE','','','Quote Creation Date','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_QUOTE_DETAIL_V','QUOTE_STATUS',660,'Quote Status','QUOTE_STATUS','','','','SA059956','VARCHAR2','','','Quote Status','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_QUOTE_DETAIL_V','SHIP_TO_SITE_NUMBER',660,'Ship To Site Number','SHIP_TO_SITE_NUMBER','','','','SA059956','VARCHAR2','','','Ship To Site Number','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_QUOTE_DETAIL_V','SHIP_TO_SITE_NAME_LOCATION',660,'Ship To Site Name Location','SHIP_TO_SITE_NAME_LOCATION','','','','SA059956','VARCHAR2','','','Ship To Site Name Location','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_QUOTE_DETAIL_V','SITE_SALESREP_EEID',660,'Site Salesrep Eeid','SITE_SALESREP_EEID','','','','SA059956','VARCHAR2','','','Site Salesrep Eeid','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_QUOTE_DETAIL_V','SITE_SALESREP_NAME',660,'Site Salesrep Name','SITE_SALESREP_NAME','','','','SA059956','VARCHAR2','','','Site Salesrep Name','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_QUOTE_DETAIL_V','SHIP_TO_SITE_SALESREP_JOB_DESC',660,'Ship To Site Salesrep Job Desc','SHIP_TO_SITE_SALESREP_JOB_DESC','','','','SA059956','VARCHAR2','','','Ship To Site Salesrep Job Desc','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_QUOTE_DETAIL_V','SITE_CLASSIFICATION',660,'Site Classification','SITE_CLASSIFICATION','','','','SA059956','VARCHAR2','','','Site Classification','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_QUOTE_DETAIL_V','BILL_CONTACT',660,'Bill Contact','BILL_CONTACT','','','','SA059956','VARCHAR2','','','Bill Contact','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_QUOTE_DETAIL_V','SHIP_CONTACT',660,'Ship Contact','SHIP_CONTACT','','','','SA059956','VARCHAR2','','','Ship Contact','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_QUOTE_DETAIL_V','CUST_PO_NUMBER',660,'Cust Po Number','CUST_PO_NUMBER','','','','SA059956','VARCHAR2','','','Cust Po Number','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_QUOTE_DETAIL_V','RELATED_PO_NUMBER',660,'Related Po Number','RELATED_PO_NUMBER','','','','SA059956','VARCHAR2','','','Related Po Number','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_QUOTE_DETAIL_V','ORDER_SOURCE',660,'Order Source','ORDER_SOURCE','','','','SA059956','VARCHAR2','','','Order Source','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_QUOTE_DETAIL_V','QUOTE_NAME',660,'Quote Name','QUOTE_NAME','','','','SA059956','VARCHAR2','','','Quote Name','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_QUOTE_DETAIL_V','SUB_TOTAL',660,'Sub Total','SUB_TOTAL','','','','SA059956','NUMBER','','','Sub Total','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_QUOTE_DETAIL_V','FREIGHT_CHARGE',660,'Freight Charge','FREIGHT_CHARGE','','','','SA059956','NUMBER','','','Freight Charge','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_QUOTE_DETAIL_V','INVOICE_NUMBER',660,'Invoice Number','INVOICE_NUMBER','','','','SA059956','VARCHAR2','','','Invoice Number','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_QUOTE_DETAIL_V','CREATED_BY',660,'Created By','CREATED_BY','','','','SA059956','VARCHAR2','','','Created By','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_QUOTE_DETAIL_V','QUOTE_NUMBER',660,'Quote Number','QUOTE_NUMBER','','','','SA059956','NUMBER','','','Quote Number','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_QUOTE_DETAIL_V','REQUESTED_DATE',660,'Requested Date','REQUESTED_DATE','','','','SA059956','DATE','','','Requested Date','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_QUOTE_DETAIL_V','CREATION_DATE',660,'Creation Date','CREATION_DATE','','','','SA059956','DATE','','','Creation Date','','','','','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_QUOTE_DETAIL_V','OH_HEADER_ID',660,'Oh Header Id','OH_HEADER_ID','','','','SA059956','NUMBER','','','Oh Header Id','','','','','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_QUOTE_DETAIL_V','RCT_CUSTOMER_TRX_ID',660,'Rct Customer Trx Id','RCT_CUSTOMER_TRX_ID','','','','SA059956','NUMBER','','','Rct Customer Trx Id','','','','','');
--Inserting Object Components for EIS_XXWC_OM_QUOTE_DETAIL_V
xxeis.eis_rsc_ins.vcomp( 'EIS_XXWC_OM_QUOTE_DETAIL_V','OE_ORDER_HEADERS',660,'OE_ORDER_HEADERS_ALL','OH','OH','SA059956','SA059956','-1','Oe Order Headers All Stores Header Information For','Y','','','','','',' ',' ',' ',' ',' ');
xxeis.eis_rsc_ins.vcomp( 'EIS_XXWC_OM_QUOTE_DETAIL_V','RA_CUSTOMER_TRX',660,'RA_CUSTOMER_TRX_ALL','RCT','RCT','SA059956','SA059956','-1','Header-Level Information About Invoices, Debit Mem','Y','','','','','',' ',' ',' ',' ',' ');
--Inserting Object Component Joins for EIS_XXWC_OM_QUOTE_DETAIL_V
xxeis.eis_rsc_ins.vcj( 'EIS_XXWC_OM_QUOTE_DETAIL_V','OE_ORDER_HEADERS','OH',660,'EXOQDV.OH_HEADER_ID','=','OH.HEADER_ID(+)','','','','Y','SA059956');
xxeis.eis_rsc_ins.vcj( 'EIS_XXWC_OM_QUOTE_DETAIL_V','RA_CUSTOMER_TRX','RCT',660,'EXOQDV.RCT_CUSTOMER_TRX_ID','=','RCT.CUSTOMER_TRX_ID(+)','','','','Y','SA059956');
ELSE
RAISE_APPLICATION_ERROR (-20001,'Module does not exist. Import/Create the module with id 660');
END IF;
END;
/
prompt Creating Report LOV Data for Oracle Quote Report - WC
set scan off define off
DECLARE
mod_exist VARCHAR2(1);
BEGIN 
mod_exist := XXEIS.EIS_RSC_INSERT.check_if_module_exist(660);
IF mod_exist = 'Y' THEN 
--Inserting Report LOVs - Oracle Quote Report - WC
xxeis.eis_rsc_ins.lov( '','SELECT DISTINCT XPEA.JOB_DESCR
    FROM XXCUS.XXCUSHR_PS_EMP_ALL_TBL XPEA order by 1','','XXWC OM Position LOV','','SA059956',NULL,'N','','','N','N','','','US');
null;
ELSE
RAISE_APPLICATION_ERROR (-20001,'Module does not exist. Import/Create the module with id 660');
END IF;
END;
/
prompt Creating Report Data for Oracle Quote Report - WC
set scan off define off
DECLARE
mod_exist varchar2(1);
BEGIN 
mod_exist := XXEIS.EIS_RSC_INSERT.check_if_module_exist(660);
IF mod_exist = 'Y' THEN 
--Deleting Report data - Oracle Quote Report - WC
xxeis.eis_rsc_utility.delete_report_rows( 'Oracle Quote Report - WC',660 );
--Inserting Report - Oracle Quote Report - WC
xxeis.eis_rsc_ins.r( 660,'Oracle Quote Report - WC','','The report displays all the quotes related information.','','','','SA059956','EIS_XXWC_OM_QUOTE_DETAIL_V','Y','','','SA059956','','N','White Cap Reports','','CSV,EXCEL,','N','','','','','','N','','US','','','','','','','','','','','','','','','','');
--Inserting Report Columns - Oracle Quote Report - WC
xxeis.eis_rsc_ins.rc( 'Oracle Quote Report - WC',660,'BILL_CONTACT','Bill to Contact','Bill Contact','','','default','','19','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_OM_QUOTE_DETAIL_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'Oracle Quote Report - WC',660,'CREATED_BY','Created By','Created By','','','default','','20','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_OM_QUOTE_DETAIL_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'Oracle Quote Report - WC',660,'CUST_PO_NUMBER','Cust Po Number','Cust Po Number','','','default','','21','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_OM_QUOTE_DETAIL_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'Oracle Quote Report - WC',660,'CUSTOMER_JOB_NAME','Customer Job Name','Customer Job Name','','','default','','22','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_OM_QUOTE_DETAIL_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'Oracle Quote Report - WC',660,'CUSTOMER_NAME','Customer Name','Customer Name','','','default','','2','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_OM_QUOTE_DETAIL_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'Oracle Quote Report - WC',660,'CUSTOMER_NUMBER','Customer Number','Customer Number','','','default','','1','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_OM_QUOTE_DETAIL_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'Oracle Quote Report - WC',660,'FREIGHT_CHARGE','Charge','Freight Charge','','~T~D~2','default','','17','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_OM_QUOTE_DETAIL_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'Oracle Quote Report - WC',660,'INVOICE_NUMBER','Invoice Number','Invoice Number','','','default','','6','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_OM_QUOTE_DETAIL_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'Oracle Quote Report - WC',660,'ORDER_AMOUNT','Order Amount','Order Amount','','~T~D~2','default','','15','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_OM_QUOTE_DETAIL_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'Oracle Quote Report - WC',660,'ORDER_HEADER_STATUS','Order Header Status','Order Header Status','','','default','','7','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_OM_QUOTE_DETAIL_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'Oracle Quote Report - WC',660,'ORDER_NUMBER','Order Number','Order Number','','~~~','default','','5','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_OM_QUOTE_DETAIL_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'Oracle Quote Report - WC',660,'ORDER_SOURCE','Order Source','Order Source','','','default','','4','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_OM_QUOTE_DETAIL_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'Oracle Quote Report - WC',660,'ORDER_TYPE','Order Type','Order Type','','','default','','3','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_OM_QUOTE_DETAIL_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'Oracle Quote Report - WC',660,'ORDERED_DATE','Ordered Date','Ordered Date','','','default','','13','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_OM_QUOTE_DETAIL_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'Oracle Quote Report - WC',660,'QUOTE_CREATION_DATE','Quote Creation Date','Quote Creation Date','','','default','','10','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_OM_QUOTE_DETAIL_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'Oracle Quote Report - WC',660,'QUOTE_NAME','Quote Name','Quote Name','','','default','','9','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_OM_QUOTE_DETAIL_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'Oracle Quote Report - WC',660,'QUOTE_NUMBER','Quote Number','Quote Number','','~~~','default','','8','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_OM_QUOTE_DETAIL_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'Oracle Quote Report - WC',660,'QUOTE_STATUS','Quote Status','Quote Status','','','default','','11','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_OM_QUOTE_DETAIL_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'Oracle Quote Report - WC',660,'RELATED_PO_NUMBER','Related Po Number','Related Po Number','','','default','','23','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_OM_QUOTE_DETAIL_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'Oracle Quote Report - WC',660,'REQUESTED_DATE','Requested Date','Requested Date','','','default','','12','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_OM_QUOTE_DETAIL_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'Oracle Quote Report - WC',660,'SHIP_CONTACT','Ship to Contact','Ship Contact','','','default','','18','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_OM_QUOTE_DETAIL_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'Oracle Quote Report - WC',660,'SHIP_TO_SITE_NAME_LOCATION','Ship To Site Name Location','Ship To Site Name Location','','','default','','28','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_OM_QUOTE_DETAIL_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'Oracle Quote Report - WC',660,'SHIP_TO_SITE_NUMBER','Ship To Site Number','Ship To Site Number','','','default','','24','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_OM_QUOTE_DETAIL_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'Oracle Quote Report - WC',660,'SHIP_TO_SITE_SALESREP_JOB_DESC','Ship To Site Salesrep Job Desc','Ship To Site Salesrep Job Desc','','','default','','27','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_OM_QUOTE_DETAIL_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'Oracle Quote Report - WC',660,'SITE_CLASSIFICATION','Site Classification','Site Classification','','','default','','29','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_OM_QUOTE_DETAIL_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'Oracle Quote Report - WC',660,'SITE_SALESREP_EEID','Site Salesrep Eeid','Site Salesrep Eeid','','','default','','26','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_OM_QUOTE_DETAIL_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'Oracle Quote Report - WC',660,'SITE_SALESREP_NAME','Site Salesrep Name','Site Salesrep Name','','','default','','25','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_OM_QUOTE_DETAIL_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'Oracle Quote Report - WC',660,'SUB_TOTAL','Sub Total','Sub Total','','~T~D~2','default','','16','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_OM_QUOTE_DETAIL_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'Oracle Quote Report - WC',660,'CREATION_DATE','Order Creation Date','Creation Date','','','default','','14','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_OM_QUOTE_DETAIL_V','','','','US','','');
--Inserting Report Parameters - Oracle Quote Report - WC
xxeis.eis_rsc_ins.rp( 'Oracle Quote Report - WC',660,'Current Site Salesrep Job Desc','Ship To Site Salesrep Job Desc','SHIP_TO_SITE_SALESREP_JOB_DESC','IN','XXWC OM Position LOV','','VARCHAR2','N','Y','3','Y','Y','CONSTANT','SA059956','Y','N','','','','EIS_XXWC_OM_QUOTE_DETAIL_V','','','US','');
xxeis.eis_rsc_ins.rp( 'Oracle Quote Report - WC',660,'Creation Date From','Creation Date From','QUOTE_CREATION_DATE','>=','','','DATE','Y','Y','1','N','Y','CONSTANT','SA059956','Y','N','','Start Date','','EIS_XXWC_OM_QUOTE_DETAIL_V','','','US','');
xxeis.eis_rsc_ins.rp( 'Oracle Quote Report - WC',660,'Creation Date To','Creation Date To','QUOTE_CREATION_DATE','<=','','','DATE','Y','Y','2','N','Y','CONSTANT','SA059956','Y','N','','End Date','','EIS_XXWC_OM_QUOTE_DETAIL_V','','','US','');
--Inserting Dependent Parameters - Oracle Quote Report - WC
--Inserting Report Conditions - Oracle Quote Report - WC
xxeis.eis_rsc_ins.rcnh( 'Oracle Quote Report - WC',660,'EXOQDV.QUOTE_CREATION_DATE >= Creation Date From','SIMPLE','','','Y','','1');
xxeis.eis_rsc_ins.rcnd( '','','QUOTE_CREATION_DATE','','Creation Date From','','','','','EIS_XXWC_OM_QUOTE_DETAIL_V','','','','','','GREATER_THAN_EQUALS','Y','Y','','','','','1',660,'Oracle Quote Report - WC','EXOQDV.QUOTE_CREATION_DATE >= Creation Date From');
xxeis.eis_rsc_ins.rcnh( 'Oracle Quote Report - WC',660,'EXOQDV.QUOTE_CREATION_DATE <= Creation Date To','SIMPLE','','','Y','','2');
xxeis.eis_rsc_ins.rcnd( '','','QUOTE_CREATION_DATE','','Creation Date To','','','','','EIS_XXWC_OM_QUOTE_DETAIL_V','','','','','','LESS_THAN_EQUALS','Y','Y','','','','','1',660,'Oracle Quote Report - WC','EXOQDV.QUOTE_CREATION_DATE <= Creation Date To');
xxeis.eis_rsc_ins.rcnh( 'Oracle Quote Report - WC',660,'EXOQDV.SHIP_TO_SITE_SALESREP_JOB_DESC IN Current Site Salesrep Job Desc','SIMPLE','','','Y','','3');
xxeis.eis_rsc_ins.rcnd( '','','SHIP_TO_SITE_SALESREP_JOB_DESC','','Current Site Salesrep Job Desc','','','','','EIS_XXWC_OM_QUOTE_DETAIL_V','','','','','','IN','Y','Y','','','','','1',660,'Oracle Quote Report - WC','EXOQDV.SHIP_TO_SITE_SALESREP_JOB_DESC IN Current Site Salesrep Job Desc');
--Inserting Report Sorts - Oracle Quote Report - WC
xxeis.eis_rsc_ins.rs( 'Oracle Quote Report - WC',660,'QUOTE_NUMBER','ASC','SA059956','1','');
--Inserting Report Triggers - Oracle Quote Report - WC
--inserting report templates - Oracle Quote Report - WC
xxeis.eis_rsc_ins.r_tem( 'Oracle Quote Report - WC','Oracle Quote Report - WC','','','','','','','','','','','','Oracle Quote Report - WC.rtf','SA059956','X','','','Y','Y','N','N');
--Inserting Report Portals - Oracle Quote Report - WC
--inserting report dashboards - Oracle Quote Report - WC
--Exporting report views table information -  
xxeis.eis_rsc_ins.rviews( 'Oracle Quote Report - WC','660','EIS_XXWC_OM_QUOTE_DETAIL_V','EIS_XXWC_OM_QUOTE_DETAIL_V','N','');
--inserting report security - Oracle Quote Report - WC
xxeis.eis_rsc_ins.rsec( 'Oracle Quote Report - WC','','SB052501','',660,'SA059956','','N','');
xxeis.eis_rsc_ins.rsec( 'Oracle Quote Report - WC','660','','ORDER_MGMT_SUPER_USER',660,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'Oracle Quote Report - WC','660','','ONT_ICP_SUPER_USER',660,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'Oracle Quote Report - WC','660','','XXWC_RENTAL_OM_PRICING_SUPER',660,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'Oracle Quote Report - WC','660','','XXWC_ORDER_MGMT_SUPER_USER',660,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'Oracle Quote Report - WC','660','','XXWC_ORDER_MGMT_READ_SHIPPING',660,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'Oracle Quote Report - WC','660','','XXWC_ORDER_MGMT_READ_ONLY_COST',660,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'Oracle Quote Report - WC','660','','XXWC_ORDER_MGMT_READ_ONLY',660,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'Oracle Quote Report - WC','660','','XXWC_ORDER_MGMT_PRICING_SUPER',660,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'Oracle Quote Report - WC','660','','XXWC_ORDER_MGMT_PRICING_STD',660,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'Oracle Quote Report - WC','660','','XXWC_ORDER_MGMT_PRICING_LTD',660,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'Oracle Quote Report - WC','660','','XXWC_ORDER_MGMT_PRICING_FULL',660,'SA059956','','','');
--Inserting Report Pivots - Oracle Quote Report - WC
--Inserting Report Distribution Details 
--Inserting Report Distribution Details 
--Inserting Report SubTotals Details 
--Inserting Report aggregations 
--Inserting Report   Version details- Oracle Quote Report - WC
xxeis.eis_rsc_ins.rv( 'Oracle Quote Report - WC','','Oracle Quote Report - WC','SA059956','26-JUN-2018');
ELSE
RAISE_APPLICATION_ERROR (-20001,'Module does not exist. Import/Create the module with id 660');
END IF;
END;
/
