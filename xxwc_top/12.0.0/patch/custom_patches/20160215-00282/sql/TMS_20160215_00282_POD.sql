/*************************************************************************
  $Header TMS_20160215_00282_POD.sql $
  Module Name: TMS_20160215_00282 DELETE the UnProcessed POD staging information

  PURPOSE: Data fix to cancel Requisition 

  REVISIONS:
  Ver        Date         Author                Description
  ---------  -----------  ------------------    --------------------------
  1.0        18-FEB-2016  Gopi Damuluri         TMS#20160215_00282

**************************************************************************/ 
SET SERVEROUTPUT ON SIZE 1000000
DECLARE
BEGIN
   DBMS_OUTPUT.put_line ('TMS: 20160215-00282   , Before Update');

   DELETE FROM XXWC.XXWC_B2B_POD_SO_INFO_TBL where status = 'PROCESSED';

   DBMS_OUTPUT.put_line (
         'TMS: 20160215-00282 Requisition Lines Updated and number of records (Expected:51): '
      || SQL%ROWCOUNT);

   COMMIT;

   DBMS_OUTPUT.put_line ('TMS: 20160215-00282   , End Update');
EXCEPTION
   WHEN OTHERS
   THEN
      ROLLBACK;
      DBMS_OUTPUT.put_line ('TMS: 20160215-00282, Errors : ' || SQLERRM);
END;
/