create or replace 
package body 	XXEIS.EIS_XXWC_DAILY_FLASH_DTLS_PKG as
 --//============================================================================
--//  
--// Change Request 			:: Performance Issue  
--//
--// Object Usage 				:: This Object Referred by "DAILY FLASH DELIVERY REPORT"
--//
--// Object Name         		:: EIS_XXWC_DAILY_FLASH_DTLS_PKG
--//
--// Object Type         		:: Package body
--//
--// Object Description  		:: This Package will trigger in before report and insert the values into Table
--//
--// Version Control
--//============================================================================
--// Version    Author           Date          Description
--//----------------------------------------------------------------------------
--// 1.0        PRAMOD  	   04/20/2016      Initial Build  --TMS#20160429-00034 --performance Tuning
--//============================================================================
procedure get_daily_flash_dtls (p_process_id 	in number,
								P_period_name 	in varchar2,
								P_location 		in varchar2
								) is
--//============================================================================
--//
--// Object Name         :: get_daily_flash_dtls  
--//
--// Object Usage 		 :: This Object Referred by "DAILY FLASH DELIVERY REPORT"
--//
--// Object Type         :: Procedure
--//
--// Object Description  :: This  procedure is created based on the EIS_XXWC_AR_FLSH_SALE_DL_NEW_V View.Due to the 
--// 						performance issue ,spilt the view definition based on the driving tables and driving data.
--//
--// Version Control
--//============================================================================
--// Version    Author           Date          Description
--//----------------------------------------------------------------------------
--// 1.0        PRAMOD  	  04/22/2016       Initial Build  --TMS#20160429-00034    --performance Tuning
--//============================================================================
l_period_sql 	varchar2(32000);
l_trx_sql  		varchar2(32000);
l_main_sql 		varchar2(32000);
l_period_name 	varchar2(240);
L_PERIOD_YEAR 	number;
L_CUST_ID  varchar2(24000);
L_COUNT number;
l_start_date 	date;
l_end_date 		date;
type trx_data_rec is record 
(customer_trx_id number);
type trx_data_rec_tab is table of trx_data_rec index by binary_integer ;
trx_data_tab trx_data_rec_tab;
type report_data_rec is record
(
	current_fiscal_month 	VARCHAR2(240),
	current_fiscal_year 	varchar2(240),
	DELIVERY_TYPE 			varchar2(240),
    branch_location     varchar2(240),
	invoice_count 			NUMBER,
	sales 					NUMBER,
	process_id 				NUMBER
);
type report_data_rec_tab is table of report_data_rec index by binary_integer ;
report_data_tab report_data_rec_tab;
 L_REF_CURSOR1          CURSOR_TYPE4;
begin
trx_data_tab.delete;
report_data_tab.delete;

--fnd_file.put_line (fnd_file.LOG,'Start');


if P_period_name is not null then--TMS#20160429-00034    by PRAMOD  on 04-26-2016

--and period_name in ('||xxeis.eis_rs_utility.get_param_values(P_period_name)||' ) 
--Get Period Start Date and End Date and passed to Ra_Customer_trx Table 
l_period_sql:='	select start_date,end_date ,period_name,period_year from gl_periods 
				where  period_set_name=''4-4-QTR'' 
				and period_name in ('||xxeis.eis_rs_utility.get_param_values(P_period_name)||' ) 
			   ' ;
         else 
    l_period_sql:='	select start_date,end_date ,period_name,period_year from gl_periods 
				where  period_set_name=''4-4-QTR'' 
			   ' ;     
         end if;
	begin
	execute immediate l_period_sql into l_start_date,l_end_date,l_period_name,l_period_year;
	Exception
	when others then
	null;
	End;
	--Location always All
	if P_LOCATION ='All' then 
	L_TRX_SQL := 'and 1=1'
--  'select rct.customer_trx_id from ra_customer_trx_all	
--			  where creation_date <='''||l_start_date||'''
--			  and creation_date >='''||l_end_date||'''
			  ;
	else
	L_TRX_SQL := 'and MP.ORGANIZATION_CODE in ('||xxeis.eis_rs_utility.get_param_values(P_LOCATION)||' )'
--  'select rct.customer_trx_id from ra_customer_trx_all	
--			  where creation_date <='''||l_start_date||'''
--			  and creation_date >='''||l_end_date||'''
;
	End if;
	--Populated Driving Data...
	
  
 L_CUST_ID:='select rct.customer_trx_id from ra_customer_trx_all	rct
			  where creation_date >=TO_DATE('''||l_start_date||''')+0.25
			  and creation_date <=TO_DATE('''||l_end_date||''')+1.25
        ';
	begin
	execute immediate L_CUST_ID bulk collect into trx_data_tab;--TMS#20160429-00034    by PRAMOD  on 04-26-2016
	Exception when  others then
	null;
	end;
	--populating report data
l_main_sql:='SELECT 
		'''||l_period_name||''' CURRENT_FISCAL_MONTH,--TMS#20160429-00034    by PRAMOD  on 04-26-2016
    '''||l_period_year||''' current_fiscal_year,--TMS#20160429-00034    by PRAMOD  on 04-26-2016
    xxeis.eis_rs_xxwc_com_util_pkg.get_shipping_mthd3 ( ol.shipping_method_code) delivery_type,
    mp.organization_code branch_location,
     (  XXEIS.EIS_RS_XXWC_COM_UTIL_PKG.GET_DIST_INVOICE_NUM ( RCT.CUSTOMER_TRX_ID, RCT.TRX_NUMBER, OTLH.NAME)) INVOICE_COUNT,
		((DECODE(XXEIS.EIS_RS_XXWC_COM_UTIL_PKG.GET_PROMO_ITEM(OL.INVENTORY_ITEM_ID,OL.SHIP_FROM_ORG_ID), ''N'', ( NVL (RCTL.QUANTITY_INVOICED, RCTL.QUANTITY_CREDITED) * NVL (OL.UNIT_SELLING_PRICE, 0)), ''Y'', 0 ))+  NVL(XXEIS.EIS_RS_XXWC_COM_UTIL_PKG.GET_DELIVERY_CHARGE_AMT(RCT.CUSTOMER_TRX_ID),0)) SALES,
    '||p_process_id||' process_id--TMS#20160429-00034    by PRAMOD  on 04-26-2016
  FROM 	ra_customer_trx 		rct,
		ra_customer_trx_lines 	rctl,
		oe_order_headers 		oh,
		oe_order_lines 			ol,
		mtl_parameters 			mp,
		oe_transaction_types_vl otlh
WHERE 	rctl.customer_trx_id   			 = rct.customer_trx_id
  AND 	RCTL.INTERFACE_LINE_ATTRIBUTE6  = TO_CHAR (OL.LINE_ID)
  AND 	RCTl.INTERFACE_LINE_ATTRIBUTE1  = TO_CHAR(OH.ORDER_NUMBER)
  AND 	rctl.interface_line_context(+)  = ''ORDER ENTRY''
  AND 	RCT.INTERFACE_HEADER_CONTEXT(+) = ''ORDER ENTRY''
  and 	rct.interface_header_attribute1 = to_char(oh.order_number)
  AND 	ol.header_id                    = oh.header_id
  AND 	OTLH.TRANSACTION_TYPE_ID  = OH.ORDER_TYPE_ID
  and 	OTLH.org_id = oh.org_id
  AND 	TO_CHAR (oh.order_number)= rct.interface_header_attribute1
  AND 	mp.organization_id(+) = ol.ship_from_org_id
  AND 	ol.ordered_item  NOT IN(''CONTOFFSET'',''CONTBILL'')
  and 	rct.customer_trx_id =:1
  '||L_TRX_SQL||'--TMS#20160429-00034    by PRAMOD  on 04-26-2016
 AND NOT EXISTS
    (SELECT /*+INDEX(gcc GL_CODE_COMBINATIONS_U1)*/''Y''
    FROM MTL_SYSTEM_ITEMS_B MSI,
		 GL_CODE_COMBINATIONS_KFV GCC
    WHERE  MSI.INVENTORY_ITEM_ID   = OL.INVENTORY_ITEM_ID
    AND MSI.ORGANIZATION_ID     = OL.SHIP_FROM_ORG_ID
    AND GCC.CODE_COMBINATION_ID = MSI.COST_OF_SALES_ACCOUNT
    AND GCC.SEGMENT4            =''646080''
    )
    /* New Added*/
  AND NOT EXISTS
    (SELECT 1
    FROM hz_customer_profiles hcp,
      hz_cust_profile_classes hcpc,
      hz_cust_accounts hca
    WHERE hca.party_id       = hcp.party_id
    AND oh.sold_to_org_id    = hcp.cust_account_id
    AND hcp.site_use_id     IS NULL
    AND hcp.profile_class_id = hcpc.profile_class_id(+)
    AND hcpc.name LIKE ''WC%Branches%''
    )
';
--  L_CUST_ID
 -- FND_FILE.PUT_LINE (FND_FILE.log,'L_CUST_ID  '||L_CUST_ID);
 -- FND_FILE.PUT_LINE (FND_FILE.log,'trx_data_tab  '||trx_data_tab.COUNT);
 -- FND_FILE.PUT_LINE (FND_FILE.log,'l_main_sql  '||L_MAIN_SQL);
  
  
  
   FOR I IN 1..trx_data_tab.COUNT
  LOOP
  
   OPEN L_REF_CURSOR1 FOR l_main_sql USING trx_data_tab(i).customer_trx_id;
  
  loop
    fetch L_REF_CURSOR1 bulk collect into report_data_tab limit 10000;--TMS#20160429-00034    by PRAMOD  on 04-26-2016
  If report_data_tab.COUNT>0
  then  
--    FND_FILE.PUT_LINE (FND_FILE.log,'entered  ');
      FORALL J in 1..REPORT_DATA_TAB.COUNT 
    	Insert Into XXEIS.eis_xxwc_daily_flash_dtl_tab Values report_data_tab(J);--TMS#20160429-00034    by PRAMOD  on 04-26-2016
  
    END IF;
    exit when L_REF_CURSOR1%notfound;
    END LOOP;
       commit;
    
    close L_REF_CURSOR1;
 END LOOP; 
 
  EXCEPTION when OTHERS then
    Fnd_File.Put_Line(FND_FILE.log,'Level 7'||sqlcode||sqlerrm);
end;
		
   PROCEDURE CLEAR_TEMP_TABLES ( P_PROCESS_ID IN NUMBER)
AS
	--//============================================================================
--//
--// Object Name         :: clear_temp_tables
--//
--// Object Type         :: Procedure
--//
--// Object Description  :: This procedure will trigger in After Report level and delete all the records from the table based on that particular process_id.
--//
--// Version Control
--//============================================================================
--// Version    Author           Date          Description
--//----------------------------------------------------------------------------
--// 1.0        PRAMOD  	   04/22/2016      Initial Build --TMS#20160429-00034   --Performance tuning
--//============================================================================  
  BEGIN  
  DELETE FROM XXEIS.EIS_XXWC_DAILY_FLASH_DTL_TAB WHERE PROCESS_ID=P_PROCESS_ID;
  COMMIT;
END ;  

END EIS_XXWC_DAILY_FLASH_DTLS_PKG;
/
