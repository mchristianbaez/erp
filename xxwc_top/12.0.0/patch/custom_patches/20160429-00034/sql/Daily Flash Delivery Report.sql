--Report Name            : Daily Flash Delivery Report
--Import Instructions    : Run this file in XXEIS user from SQL*Plus or any tool like Toad, SQL Navigator. 
prompt Creating View Data for Daily Flash Delivery Report
set scan off define off
DECLARE
BEGIN 
--Inserting View EIS_XXWC_AR_FLSH_SALE_DL_NEW_V
xxeis.eis_rs_ins.v( 'EIS_XXWC_AR_FLSH_SALE_DL_NEW_V',660,'','','','','PK059658','XXEIS','Eis Xxwc Ar Flsh Sale By Del V','EXAFSBDV','','');
--Delete View Columns for EIS_XXWC_AR_FLSH_SALE_DL_NEW_V
xxeis.eis_rs_utility.delete_view_rows('EIS_XXWC_AR_FLSH_SALE_DL_NEW_V',660,FALSE);
--Inserting View Columns for EIS_XXWC_AR_FLSH_SALE_DL_NEW_V
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_FLSH_SALE_DL_NEW_V','BRANCH_LOCATION',660,'Branch Location','BRANCH_LOCATION','','','','PK059658','VARCHAR2','','','Branch Location','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_FLSH_SALE_DL_NEW_V','CURRENT_FISCAL_MONTH',660,'Current Fiscal Month','CURRENT_FISCAL_MONTH','','','','PK059658','VARCHAR2','','','Current Fiscal Month','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_FLSH_SALE_DL_NEW_V','CURRENT_FISCAL_YEAR',660,'Current Fiscal Year','CURRENT_FISCAL_YEAR','','','','PK059658','NUMBER','','','Current Fiscal Year','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_FLSH_SALE_DL_NEW_V','DELIVERY_TYPE',660,'Delivery Type','DELIVERY_TYPE','','','','PK059658','VARCHAR2','','','Delivery Type','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_FLSH_SALE_DL_NEW_V','SALES',660,'Sales','SALES','','','','PK059658','NUMBER','','','Sales','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_FLSH_SALE_DL_NEW_V','INVOICE_COUNT',660,'Invoice Count','INVOICE_COUNT','','','','PK059658','NUMBER','','','Invoice Count','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_FLSH_SALE_DL_NEW_V','PROCESS_ID',660,'Process Id','PROCESS_ID','','','','PK059658','NUMBER','','','Process Id','','','');
--Inserting View Components for EIS_XXWC_AR_FLSH_SALE_DL_NEW_V
--Inserting View Component Joins for EIS_XXWC_AR_FLSH_SALE_DL_NEW_V
END;
/
set scan on define on
prompt Creating Report LOV Data for Daily Flash Delivery Report
set scan off define off
DECLARE
BEGIN 
--Inserting Report LOVs - Daily Flash Delivery Report
xxeis.eis_rs_ins.lov( 660,'select  distinct per.period_name , per.period_year, per.period_num, per.start_date, per.end_date
from    gl_periods per,gl_ledgers led
where  per.period_set_name = led.period_set_name
and     led.accounted_period_type = per.period_type
order by per.period_num asc,
per.period_year desc','','OM PERIOD NAMES','','ANONYMOUS',NULL,'N','','');
xxeis.eis_rs_ins.lov( 660,'SELECT ORGANIZATION_CODE WAREHOUSE,ORGANIZATION_NAME FROM ORG_ORGANIZATION_DEFINITIONS OOD WHERE SYSDATE < NVL(OOD.DISABLE_DATE,SYSDATE+1) AND EXISTS (SELECT 1 FROM XXEIS.EIS_ORG_ACCESS_V  WHERE organization_id = ood.organization_id )
UNION
SELECT ''All'', ''All Organizations'' from Dual','','OM Warehouse All','','ANONYMOUS',NULL,'N','','');
END;
/
set scan on define on
prompt Creating Report Data for Daily Flash Delivery Report
set scan off define off
DECLARE
BEGIN 
--Deleting Report data - Daily Flash Delivery Report
xxeis.eis_rs_utility.delete_report_rows( 'Daily Flash Delivery Report' );
--Inserting Report - Daily Flash Delivery Report
xxeis.eis_rs_ins.r( 660,'Daily Flash Delivery Report','','The purpose of this extract is to provide Finance with a daily report of all sales by branch and aggregated by delivery type.  The selected date parameter represents the desired date of sales (e.g. For sales reported on Aug 1st, set the Date parameter = Aug. 1st.   This report is to be processed daily and intended to accompany the daily extracts, flash_charge and flash_cash.
','','','','PK059658','EIS_XXWC_AR_FLSH_SALE_DL_NEW_V','Y','','','PK059658','','N','White Cap Reports','','CSV,EXCEL,','N');
--Inserting Report Columns - Daily Flash Delivery Report
xxeis.eis_rs_ins.rc( 'Daily Flash Delivery Report',660,'BRANCH_LOCATION','Location','Branch Location','','','default','','4','N','','','','','','','','PK059658','N','N','','EIS_XXWC_AR_FLSH_SALE_DL_NEW_V','','');
xxeis.eis_rs_ins.rc( 'Daily Flash Delivery Report',660,'DELIVERY_TYPE','Delivery Type','Delivery Type','','','default','','3','N','','','','','','','','PK059658','N','N','','EIS_XXWC_AR_FLSH_SALE_DL_NEW_V','','');
xxeis.eis_rs_ins.rc( 'Daily Flash Delivery Report',660,'SALES','Net Sales Dollars','Sales','','~T~D~2','default','','6','N','','','','','','','','PK059658','N','N','','EIS_XXWC_AR_FLSH_SALE_DL_NEW_V','','');
xxeis.eis_rs_ins.rc( 'Daily Flash Delivery Report',660,'INVOICE_COUNT','Invoice Count','Invoice Count','','~~~','default','','5','N','','','','','','','','PK059658','N','N','','EIS_XXWC_AR_FLSH_SALE_DL_NEW_V','','');
xxeis.eis_rs_ins.rc( 'Daily Flash Delivery Report',660,'CURRENT_FISCAL_MONTH','Fiscal Month','Current Fiscal Month','','','default','','1','N','','','','','','','','PK059658','N','N','','EIS_XXWC_AR_FLSH_SALE_DL_NEW_V','','');
xxeis.eis_rs_ins.rc( 'Daily Flash Delivery Report',660,'CURRENT_FISCAL_YEAR','Current Fiscal Year','Current Fiscal Year','','','','','2','N','','','','','','','','PK059658','N','N','','EIS_XXWC_AR_FLSH_SALE_DL_NEW_V','','');
--Inserting Report Parameters - Daily Flash Delivery Report
xxeis.eis_rs_ins.rp( 'Daily Flash Delivery Report',660,'Period Name','Period Name','','IN','OM PERIOD NAMES','','VARCHAR2','Y','Y','1','','N','CONSTANT','PK059658','Y','N','','','');
xxeis.eis_rs_ins.rp( 'Daily Flash Delivery Report',660,'Location','Location','BRANCH_LOCATION','IN','OM Warehouse All','','VARCHAR2','Y','Y','2','','N','CONSTANT','PK059658','Y','N','','','');
--Inserting Report Conditions - Daily Flash Delivery Report
xxeis.eis_rs_ins.rcn( 'Daily Flash Delivery Report',660,'','','','','and PROCESS_ID=:SYSTEM.PROCESS_ID','Y','1','','PK059658');
--Inserting Report Sorts - Daily Flash Delivery Report
--Inserting Report Triggers - Daily Flash Delivery Report
xxeis.eis_rs_ins.rt( 'Daily Flash Delivery Report',660,'begin

xxeis.EIS_XXWC_DAILY_FLASH_DTLS_PKG.get_daily_flash_dtls(:SYSTEM.PROCESS_ID,:Period Name,:Location);

end;','B','Y','PK059658');
xxeis.eis_rs_ins.rt( 'Daily Flash Delivery Report',660,'begin
XXEIS.EIS_XXWC_DAILY_FLASH_DTLS_PKG.clear_temp_tables(:SYSTEM.PROCESS_ID);
end;','A','Y','PK059658');
--Inserting Report Templates - Daily Flash Delivery Report
--Inserting Report Portals - Daily Flash Delivery Report
--Inserting Report Dashboards - Daily Flash Delivery Report
--Inserting Report Security - Daily Flash Delivery Report
xxeis.eis_rs_ins.rsec( 'Daily Flash Delivery Report','','TB003018','',660,'PK059658','','');
xxeis.eis_rs_ins.rsec( 'Daily Flash Delivery Report','','JS020126','',660,'PK059658','','');
xxeis.eis_rs_ins.rsec( 'Daily Flash Delivery Report','','MT009628','',660,'PK059658','','');
xxeis.eis_rs_ins.rsec( 'Daily Flash Delivery Report','20005','','51207',660,'PK059658','','');
xxeis.eis_rs_ins.rsec( 'Daily Flash Delivery Report','','PP018915','',660,'PK059658','','');
xxeis.eis_rs_ins.rsec( 'Daily Flash Delivery Report','','AS000277','',660,'PK059658','','');
xxeis.eis_rs_ins.rsec( 'Daily Flash Delivery Report','','SS084202','',660,'PK059658','','');
xxeis.eis_rs_ins.rsec( 'Daily Flash Delivery Report','','SG019472','',660,'PK059658','','');
xxeis.eis_rs_ins.rsec( 'Daily Flash Delivery Report','','60002557','',660,'PK059658','','');
xxeis.eis_rs_ins.rsec( 'Daily Flash Delivery Report','','ZR023146','',660,'PK059658','','');
--Inserting Report Pivots - Daily Flash Delivery Report
END;
/
set scan on define on
