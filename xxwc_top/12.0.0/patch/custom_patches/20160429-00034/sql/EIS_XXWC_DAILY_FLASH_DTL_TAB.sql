--------------------------------------------------------------------------------------
/*******************************************************************************
  Table: EIS_XXWC_DAILY_FLASH_DTL_TAB
  Description: This table is used to get data from XXEIS.EIS_XXWC_DAILY_FLASH_DTLS_PKG Package.
  HISTORY
  ===============================================================================
  VERSION DATE               AUTHOR(S)       DESCRIPTION
  ------- -----------------  --------------- -----------------------------------------
  1.0     20-Apr-2016        PRAMOD   		TMS#20160429-00034  Performance Tuning
********************************************************************************/
CREATE TABLE XXEIS.EIS_XXWC_DAILY_FLASH_DTL_TAB
  (
    CURRENT_FISCAL_MONTH VARCHAR2(240 BYTE),
    CURRENT_FISCAL_YEAR  number,
    DELIVERY_TYPE        VARCHAR2(240 BYTE),
    BRANCH_LOCATION      VARCHAR2(240 BYTE),
    INVOICE_COUNT        NUMBER,
    SALES                NUMBER,
    PROCESS_ID           NUMBER
)
/
