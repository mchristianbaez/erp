create or replace 
package XXEIS.EIS_XXWC_DAILY_FLASH_DTLS_PKG as
 --//============================================================================
--//  
--// Change Request 			:: Performance Issue  
--//
--// Object Usage 				:: This Object Referred by "DAILY FLASH DELIVERY REPORT"
--//
--// Object Name         		:: EIS_XXWC_DAILY_FLASH_DTLS_PKG
--//
--// Object Type         		:: Package Spec
--//
--// Object Description  		:: This Package will trigger in before report and insert the values into Table
--//
--// Version Control
--//============================================================================
--// Version    Author           Date          Description
--//----------------------------------------------------------------------------
--// 1.0        PRAMOD  	   04/20/2016      Initial Build  --TMS#20160429-00034  --Performance tuning
--//============================================================================
procedure get_daily_flash_dtls (p_process_id in number,
								P_period_name in varchar2,
								P_location in varchar2);
--//============================================================================
--//
--// Object Name         :: get_daily_flash_dtls  
--//
--// Object Usage 		 :: This Object Referred by "DAILY FLASH DELIVERY REPORT"
--//
--// Object Type         :: Procedure
--//
--// Object Description  :: This  procedure is created based on the EIS_XXWC_AR_FLSH_SALE_DL_NEW_V View.Due to the 
--// 						performance issue ,spilt the view definition based on the driving tables and driving data.
--//
--// Version Control
--//============================================================================
--// Version    Author           Date          Description
--//----------------------------------------------------------------------------
--// 1.0        PRAMOD  	  04/22/2016       Initial Build  --TMS#20160429-00034  --Performance tuning
--//============================================================================
								
 type CURSOR_TYPE4 is ref cursor;
 
     PROCEDURE CLEAR_TEMP_TABLES ( P_PROCESS_ID IN NUMBER);
	 --//============================================================================
--//
--// Object Name         :: clear_temp_tables
--//
--// Object Type         :: Procedure
--//
--// Object Description  :: This procedure will trigger in After Report level and delete all the records from the table based on that particular process_id.
--//
--// Version Control
--//============================================================================
--// Version    Author           Date          Description
--//----------------------------------------------------------------------------
--// 1.0        PRAMOD  	   04/20/2016      Initial Build --TMS#20160429-00034  --Performance tuning
--//============================================================================ 
End;
/
