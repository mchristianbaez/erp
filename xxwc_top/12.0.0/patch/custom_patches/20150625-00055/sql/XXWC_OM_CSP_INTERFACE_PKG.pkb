CREATE OR REPLACE PACKAGE BODY APPS.xxwc_om_csp_interface_pkg
AS

/********************************************************************************
FILE NAME: APPS.XXWC_OM_CSP_INTERFACE_PKG.pkg

PROGRAM TYPE: PL/SQL Package

PURPOSE: This contains contains the need procedures for the Contract Pricing 
         Maintenance Extension. This includes Customer Pricing Attribute for 
         Item Category, ability to import modifiers from the CSP Maintenance 
         Form to Advanced Pricing, aonvert a Quote to a CSP Agreement, and 
         process  a mass CSP upload into the custom form.

HISTORY
===============================================================================
VERSION DATE          AUTHOR(S)              DESCRIPTION
------- -----------   --------------- -----------------------------------------
1.0     01/09/2013    Lucidity Consulting    Initial Release
2.0    8/6/2014     Lucidity Consultig-Toby  TMS 20140725-00077 --Rounding logic has been corrected in CSP_QUOTE procedure
3.0     9/29/2014   Ram Talluri           Added last_updated_date andlast_updated_by and corrected rounding  - TMS #20140915-00219 and 20140924-00158
4.0     6/30/2015   Manjula Chellappan	     TMS # 20150625-00055 - CSP Enhancement Bundle - Item #3 - CSP Maintenance Tool
********************************************************************************/


/*************************************************************************
*   Function Name: get_item_category
*
*   PURPOSE:   GET_ITEM_CATEGORY function is mapped to Pricing Attribute 26, 
*              this function is used to create a custom List of Values for 
*              Segment1 of the Item Category Structure
*****************************************************************************/

   FUNCTION get_item_category (i_inv_item_id IN NUMBER)
        RETURN CHAR
     IS
        l_segment1   VARCHAR2 (30);
     BEGIN
        SELECT c.segment1
          INTO l_segment1
          FROM MTL_DEFAULT_CATEGORY_SETS_FK_V a
             , MTL_CATEGORY_SETS_V b
             , mtl_categories c
             , mtl_item_categories d
         WHERE     a.functional_area_id = 1
               AND a.category_set_id = b.category_set_id
               AND b.structure_id = c.structure_id
               AND c.enabled_flag = 'Y'
               AND c.category_id = d.category_id
               AND d.inventory_item_id = i_inv_item_id
               AND d.organization_id = 222;
  
        RETURN (l_segment1);
     END get_item_category;


  
/*************************************************************************
*   PROCEDURE Name: create_header
*
*   PURPOSE:   CREATE_HEADER procedure is used to create Modifier Headers 
*              in Advanced Pricing from related CSP Agreement Header 
*              'i_agreement_id' and 
*              update existing CSP Agreement Headers and Modifier Lists
* ***************************************************************************/
   PROCEDURE create_header (i_agreement_id IN NUMBER)
   IS
      l_control_rec                 QP_GLOBALS.Control_Rec_Type;
      l_return_status               VARCHAR2 (1);
      x_msg_count                   NUMBER;
      x_msg_data                    VARCHAR2 (2000);
      x_msg_index                   NUMBER;

      l_MODIFIER_LIST_rec           QP_Modifiers_PUB.Modifier_List_Rec_Type;
      l_MODIFIER_LIST_val_rec       QP_Modifiers_PUB.Modifier_List_Val_Rec_Type;
      l_MODIFIERS_tbl               QP_Modifiers_PUB.Modifiers_Tbl_Type;
      l_MODIFIERS_val_tbl           QP_Modifiers_PUB.Modifiers_Val_Tbl_Type;
      l_QUALIFIERS_tbl              QP_Qualifier_Rules_PUB.Qualifiers_Tbl_Type;
      l_QUALIFIERS_val_tbl          QP_Qualifier_Rules_PUB.Qualifiers_Val_Tbl_Type;
      l_PRICING_ATTR_tbl            QP_Modifiers_PUB.Pricing_Attr_Tbl_Type;
      l_PRICING_ATTR_val_tbl        QP_Modifiers_PUB.Pricing_Attr_Val_Tbl_Type;

      l_x_MODIFIER_LIST_rec         QP_Modifiers_PUB.Modifier_List_Rec_Type;
      l_x_MODIFIER_LIST_val_rec     QP_Modifiers_PUB.Modifier_List_Val_Rec_Type;
      l_x_MODIFIERS_tbl             QP_Modifiers_PUB.Modifiers_Tbl_Type;
      l_x_MODIFIERS_val_tbl         QP_Modifiers_PUB.Modifiers_Val_Tbl_Type;
      l_x_QUALIFIERS_tbl            QP_Qualifier_Rules_PUB.Qualifiers_Tbl_Type;
      l_x_QUALIFIERS_val_tbl        QP_Qualifier_Rules_PUB.Qualifiers_Val_Tbl_Type;
      l_x_PRICING_ATTR_tbl          QP_Modifiers_PUB.Pricing_Attr_Tbl_Type;
      l_x_PRICING_ATTR_val_tbl      QP_Modifiers_PUB.Pricing_Attr_Val_Tbl_Type;

      /*Variables*/
      v_agreement_id                NUMBER := i_agreement_id;
      v_agreement_type              VARCHAR2 (30);
      v_vendor_quote_number         NUMBER;
      v_price_type                  VARCHAR2 (30);
      v_customer_id                 NUMBER;
      v_customer_site_id            NUMBER;
      v_agreement_status            VARCHAR2 (30);
      v_revision_number             NUMBER;
      v_incompatibility_group       VARCHAR2 (30);
      v_attribute_10                VARCHAR2 (25) := 'Contract Pricing'; --Variable to populate DFF Attribute 10 on Modifier Header (Contract Pricing or Vendor Quote)
      v_attribute_11                NUMBER := NULL;
      v_attribute_12                VARCHAR2 (25) := NULL; --Vendor Name for VQN
      v_attribute_13                VARCHAR2 (25) := NULL; --VEndor Number for VQN
      v_attribute_14                NUMBER; --Mapped to Vendor Agreement Number DFF on Modifier Header
      v_attribute_15                NUMBER; --Mapped to Agreement Revision Number DFF on Modifier Header
      v1_price_type                 VARCHAR2 (30);
      v_customer_name               VARCHAR2 (50);
      v_party_id                    NUMBER;
      v_party_name                  VARCHAR2 (360);
      v_party_site_number           NUMBER;
      v_qualifier_value             NUMBER;
      v_agreeid                     NUMBER;
      v_qual_context                VARCHAR2 (360);
      v_excluder_flag               VARCHAR2 (1);
      v_operator_code               VARCHAR (360);
      v_delimitier                  VARCHAR2 (1);
      v_qualifier_grouping_no       NUMBER;
      v_qualifier_precedence        NUMBER;
      v_operation                   VARCHAR2 (30);
      v_active_flag                 VARCHAR2 (30);
      v_list_header_id              NUMBER;                 
      v_list_line_id                NUMBER;
      L_MSG                         VARCHAR(240) ; 

         BEGIN     
         
         /*Initial Select to set variables with CSP Header Info*/
          BEGIN
            SELECT agreement_id
                 , price_type
                 , customer_id
                 , customer_site_id
                 , agreement_status
                 , revision_number
                 , incompatability_group
              INTO v_attribute_14
                 , v_price_type
                 , v_customer_id
                 , v_customer_site_id
                 , v_agreement_status
                 , v_attribute_15
                 , v_incompatibility_group
              FROM xxwc.xxwc_om_contract_pricing_hdr
             WHERE agreement_id = v_agreement_id;
          EXCEPTION
            WHEN OTHERS THEN 
              L_MSG := SUBSTR('Initial select failed Verifiy Customer Infomration' || SQLCODE || SQLERRM,1,240) ;
        END;  

            
     /*If CSP Header Revision Number = 0 then this is a new CSP Agreement and a new Modifier List will be created*/
        If v_attribute_15 = 0
          THEN 
         --Select PARTY_ID from Customer Master
            IF v_price_type in ('SHIPTO','MASTER')
            THEN
              BEGIN
               SELECT party_id
                 INTO v_party_id
                 FROM hz_cust_accounts
                WHERE cust_account_id = v_customer_id;
                              EXCEPTION
                WHEN OTHERS THEN 
                  L_MSG := SUBSTR('Customer Party ID not found' || SQLCODE || SQLERRM,1,240) ;
           END; 
          /*Select PARTY_NAME from Customer Master*/
              BEGIN
                SELECT party_name
                 INTO v_party_name
                 FROM hz_parties
                WHERE party_id = v_party_id;
              EXCEPTION
                WHEN OTHERS THEN 
                  L_MSG := SUBSTR('Customer Party Name not found' || SQLCODE || SQLERRM,1,240) ;
          END; 
            ELSE
               v_party_id := NULL;
               BEGIN
                SELECT description
                 INTO v_party_name
                 FROM fnd_lookup_values
                WHERE lookup_type = 'NATIONAL_ACCOUNTS'
                      AND lookup_code = v_customer_id;
                EXCEPTION
                  WHEN OTHERS THEN 
                    L_MSG := SUBSTR('National Account Does Not Exist' || SQLCODE || SQLERRM,1,240) ;
            END; 
          END IF;
           
      /*select party site number*/
          IF v_price_type = 'SHIPTO' --If Ship To Agreement select Site Number from Ship To Site
            THEN
              BEGIN
               SELECT c.party_site_number
                 INTO v_party_site_number
                 FROM hz_cust_site_uses_all a
                    , hz_cust_acct_sites_all b
                    , hz_party_sites c
                WHERE     a.cust_acct_site_id = b.cust_acct_site_id
                      AND b.party_site_id = c.party_site_id
                      AND a.site_use_id = v_customer_site_id;
                 EXCEPTION
                  WHEN OTHERS THEN 
                    L_MSG := SUBSTR('Party Site Number Not Found' || SQLCODE || SQLERRM,1,240) ;
                  END;
                  
            ELSIF v_price_type = 'MASTER' --If Bill To Agreement select Customer Account Number from the customer
            THEN
              BEGIN
               SELECT account_number
                 INTO v_party_site_number
                 FROM hz_cust_accounts
                WHERE cust_account_id = v_customer_id;
               EXCEPTION
                WHEN OTHERS THEN 
                  L_MSG := SUBSTR('Party Site Number Not Found' || SQLCODE || SQLERRM,1,240) ;
                END;
            ELSE
               v_party_site_number := NULL;
            END IF;


            /*Set variable for l_QUALIFIERS_tbl(1).qualifier_attr_value := v_qualifier_value ;*/
            IF v_price_type = 'SHIPTO'
            THEN
               v_qualifier_value := v_customer_site_id; --INSERT INTO customers VALUES(:p_if1,:p_if2);
               v1_price_type := 'QUALIFIER_ATTRIBUTE11';
               v_qual_context := 'CUSTOMER';
               v_excluder_flag := 'N';
               v_operator_code := '=';
               v_delimitier := '-'; 
               v_qualifier_grouping_no := 1;
               v_qualifier_precedence := 1;
               v_operation := QP_GLOBALS.G_OPR_CREATE; 
               v_active_flag := 'Y';
                               
            ELSIF v_price_type = 'MASTER'
            THEN
               v_qualifier_value := v_customer_id;
               v1_price_type := 'QUALIFIER_ATTRIBUTE32';
               v_qual_context := 'CUSTOMER';
               v_excluder_flag := 'N';
               v_operator_code := '=';
               v_delimitier := '-'; 
               v_qualifier_grouping_no := 1;
               v_qualifier_precedence := 1;
               v_operation := QP_GLOBALS.G_OPR_CREATE; 
               v_active_flag := 'Y';
               
            ELSE
               v_qualifier_value := NULL;
               v1_price_type := NULL;
               v_qual_context := NULL;
               v_excluder_flag := NULL;
               v_operator_code := NULL;
               v_delimitier := NULL ; 
               v_qualifier_grouping_no := NULL;
               v_qualifier_precedence := NULL;
               v_operation := NULL;
               v_active_flag := 'N';
            END IF;

            /*Set API Variables for Modifier Header*/
              l_MODIFIER_LIST_rec.currency_code := 'USD';
              l_MODIFIER_LIST_rec.list_type_code := 'DLT';
              l_MODIFIER_LIST_rec.source_system_code := 'QP';
              l_MODIFIER_LIST_rec.active_flag := v_active_flag;
              l_MODIFIER_LIST_rec.automatic_flag := 'Y';
              l_MODIFIER_LIST_rec.name := v_party_name || v_delimitier || v_party_site_number; --Concatenation for Modifier Name / Number Format. Will be preceeded by CSp or VQN based on Header Level Agreement Selection.
              l_MODIFIER_LIST_rec.description := v_party_name || v_delimitier || v_party_site_number; --Concatenation for Modifier Name / Number Format. Will be preceeded by CSp or VQN based on Header Level Agreement Selection.
              l_MODIFIER_LIST_rec.version_no := v_attribute_14 || '.' || (v_attribute_15 + 1) ;
              l_MODIFIER_LIST_rec.pte_code := 'ORDFUL';
              l_MODIFIER_LIST_rec.operation := QP_GLOBALS.G_OPR_CREATE;
              l_MODIFIER_LIST_rec.context := '162';
              l_MODIFIER_LIST_rec.attribute10 := v_attribute_10;
              l_MODIFIER_LIST_rec.attribute11 := v_attribute_11;
              l_MODIFIER_LIST_rec.attribute12 := v_attribute_12;
              l_MODIFIER_LIST_rec.attribute13 := v_attribute_13;
              l_MODIFIER_LIST_rec.attribute14 := v_attribute_14;
              l_MODIFIER_LIST_rec.attribute15 := v_attribute_15;
              
            /*  Set API variables for Qualifiers at Modifier Header */
              l_QUALIFIERS_tbl (1).excluder_flag := v_excluder_flag;
              l_QUALIFIERS_tbl (1).comparison_operator_code := v_operator_code;
              l_QUALIFIERS_tbl (1).qualifier_context := v_qual_context;  --'CUSTOMER';
              l_QUALIFIERS_tbl (1).qualifier_attribute := v1_price_type;
              l_QUALIFIERS_tbl (1).qualifier_attr_value := v_qualifier_value;
              l_QUALIFIERS_tbl (1).qualifier_grouping_no := v_qualifier_grouping_no;
              l_QUALIFIERS_tbl (1).qualifier_precedence := v_qualifier_precedence;
              l_QUALIFIERS_tbl (1).operation := v_operation;

            /* API Call */
              QP_Modifiers_PUB.Process_Modifiers (
                 p_api_version_number      => 1.0
               , p_init_msg_list           => FND_API.G_FALSE
               , p_return_values           => FND_API.G_FALSE
               , p_commit                  => FND_API.G_FALSE
               , x_return_status           => l_return_status
               , x_msg_count               => x_msg_count
               , x_msg_data                => x_msg_data
               , p_MODIFIER_LIST_rec       => l_MODIFIER_LIST_rec
               , p_MODIFIERS_tbl           => l_MODIFIERS_tbl
               , p_QUALIFIERS_tbl          => l_QUALIFIERS_tbl
               , p_PRICING_ATTR_tbl        => l_PRICING_ATTR_tbl
               , x_MODIFIER_LIST_rec       => l_x_MODIFIER_LIST_rec
               , x_MODIFIER_LIST_val_rec   => l_x_MODIFIER_LIST_val_rec
               , x_MODIFIERS_tbl           => l_x_MODIFIERS_tbl
               , x_MODIFIERS_val_tbl       => l_x_MODIFIERS_val_tbl
               , x_QUALIFIERS_tbl          => l_x_QUALIFIERS_tbl
               , x_QUALIFIERS_val_tbl      => l_x_QUALIFIERS_val_tbl
               , x_PRICING_ATTR_tbl        => l_x_PRICING_ATTR_tbl
               , x_PRICING_ATTR_val_tbl    => l_x_PRICING_ATTR_val_tbl);

              /* Update Revision Number on CSP Agreement to next revision number */
              /* Update agreement status on CSP Agreement from AWIATING APPROVAL to APPROVED */
                 UPDATE xxwc.xxwc_om_contract_pricing_hdr
                 SET revision_number = (v_attribute_15 + 1)
                   , agreement_status = 'APPROVED'  
                 WHERE agreement_id = v_attribute_14 ;

          COMMIT;
      
        ELSE 

    /*For existing CSP agreements. Revision number not '0'*/
    
      /* Update Revision Number on CSP Agreement to next revision number */
          UPDATE xxwc.xxwc_om_contract_pricing_hdr
          SET revision_number = (v_attribute_15 + 1) 
          WHERE agreement_id = v_attribute_14 ;
          
      /* Select Modifier Header_ID from Advanced Pricing */
          select list_header_id
          into v_list_header_id
          from qp_list_headers
          where attribute14 = v_attribute_14 ; 

          /*Set API Variables for Modifier Header*/
            l_MODIFIER_LIST_rec.list_header_id := v_list_header_id ;
            l_MODIFIER_LIST_rec.version_no := v_attribute_14||'.'||(v_attribute_15 + 1) ;
            l_MODIFIER_LIST_rec.pte_code := 'ORDFUL';
            l_MODIFIER_LIST_rec.operation := QP_GLOBALS.G_OPR_UPDATE;
            --l_MODIFIER_LIST_rec.context := '162';
            l_MODIFIER_LIST_rec.attribute15 := (v_attribute_15 + 1) ; 

            /* API Call */
              QP_Modifiers_PUB.Process_Modifiers
              ( p_api_version_number => 1.0
              , p_init_msg_list => FND_API.G_FALSE
              , p_return_values => FND_API.G_FALSE
              , p_commit => FND_API.G_FALSE
              , x_return_status => l_return_status
              , x_msg_count =>x_msg_count
              , x_msg_data =>x_msg_data
              ,p_MODIFIER_LIST_rec => l_MODIFIER_LIST_rec
              ,p_MODIFIERS_tbl => l_MODIFIERS_tbl
              ,p_QUALIFIERS_tbl => l_QUALIFIERS_tbl
              ,p_PRICING_ATTR_tbl => l_PRICING_ATTR_tbl
              ,x_MODIFIER_LIST_rec => l_x_MODIFIER_LIST_rec         
              ,x_MODIFIER_LIST_val_rec => l_x_MODIFIER_LIST_val_rec 
              ,x_MODIFIERS_tbl => l_x_MODIFIERS_tbl                 
              ,x_MODIFIERS_val_tbl => l_x_MODIFIERS_val_tbl         
              ,x_QUALIFIERS_tbl => l_x_QUALIFIERS_tbl               
              ,x_QUALIFIERS_val_tbl => l_x_QUALIFIERS_val_tbl       
              ,x_PRICING_ATTR_tbl => l_x_PRICING_ATTR_tbl           
              ,x_PRICING_ATTR_val_tbl => l_x_PRICING_ATTR_val_tbl   
              );
              
              /* Update agreement status on CSP Agreement from AWIATING APPROVAL to APPROVED */
                 UPDATE xxwc.xxwc_om_contract_pricing_hdr
                 SET agreement_status = 'APPROVED' 
                 WHERE agreement_id = v_attribute_14 ;
                 
              commit ; 
        END IF ;       
      END create_header;


/*************************************************************************
*   PROCEDURE Name: create_lines
*
*   PURPOSE:   CREATE_LINES procedure is used to create Modifier Lines
*              in Advanced Pricing related to the CSP Agreement Header 
*              'i_agreement_id' 
* ***************************************************************************/
  PROCEDURE create_lines (i_agreement_id IN NUMBER)
  IS
    l_control_rec                 QP_GLOBALS.Control_Rec_Type;
    l_return_status               VARCHAR2 (1);
    x_msg_count                   NUMBER;
    x_msg_data                    VARCHAR2 (2000);
    x_msg_index                   NUMBER;
    
    l_MODIFIER_LIST_rec           QP_Modifiers_PUB.Modifier_List_Rec_Type;
    l_MODIFIER_LIST_val_rec       QP_Modifiers_PUB.Modifier_List_Val_Rec_Type;
    l_MODIFIERS_tbl               QP_Modifiers_PUB.Modifiers_Tbl_Type;
    l_MODIFIERS_val_tbl           QP_Modifiers_PUB.Modifiers_Val_Tbl_Type;
    l_QUALIFIERS_tbl              QP_Qualifier_Rules_PUB.Qualifiers_Tbl_Type;
    l_QUALIFIERS_val_tbl          QP_Qualifier_Rules_PUB.Qualifiers_Val_Tbl_Type;
    l_PRICING_ATTR_tbl            QP_Modifiers_PUB.Pricing_Attr_Tbl_Type;
    l_PRICING_ATTR_val_tbl        QP_Modifiers_PUB.Pricing_Attr_Val_Tbl_Type;
    
    l_x_MODIFIER_LIST_rec         QP_Modifiers_PUB.Modifier_List_Rec_Type;
    l_x_MODIFIER_LIST_val_rec     QP_Modifiers_PUB.Modifier_List_Val_Rec_Type;
    l_x_MODIFIERS_tbl             QP_Modifiers_PUB.Modifiers_Tbl_Type;
    l_x_MODIFIERS_val_tbl         QP_Modifiers_PUB.Modifiers_Val_Tbl_Type;
    l_x_QUALIFIERS_tbl            QP_Qualifier_Rules_PUB.Qualifiers_Tbl_Type;
    l_x_QUALIFIERS_val_tbl        QP_Qualifier_Rules_PUB.Qualifiers_Val_Tbl_Type;
    l_x_PRICING_ATTR_tbl          QP_Modifiers_PUB.Pricing_Attr_Tbl_Type;
    l_x_PRICING_ATTR_val_tbl      QP_Modifiers_PUB.Pricing_Attr_Val_Tbl_Type;




    v_agreement_id                NUMBER := i_agreement_id;
    v_revision_number             NUMBER;
    v_incompatability_group       VARCHAR2 (30);
    v_incomp_group                VARCHAR2 (30);  
    v_list_header_id              NUMBER;                 --v_list_header_id
    v_atrribute_3                 NUMBER;
    v_ln_special_cost             NUMBER;
    v_ln_id                       NUMBER; --Line Level Variable for xxwc.agreement_line_id
    v_ln_agreement_type           VARCHAR2 (30); --Line Level Variable for xxwc.agreement_type
    v_ln_agree_type               VARCHAR2 (30); --Set CSP or VQN Agreement Type Variable passed to API
    v_ln_vendor_quote_number      VARCHAR2 (30); --Line Level Variable for XXWC.VENDOR_QUOTE_NUMBER
    v_ln_start_date               DATE; --Line Level Variable for XXWC.START_DATE
    v_ln_end_date                 DATE; --Line Level Variable for XXWC.END_DATE
    v_ln_mod_type                 VARCHAR2 (30); --Line Level Variable for Modifier Type
    v_arithmetic_operator         VARCHAR2 (30); --Line Level Variable for Modifier Type API Value
    v_ln_mod_value                NUMBER; --Line Level Variable for Modifier Value
    v_ln_mod_val                  NUMBER; --Line Level Modifer Value passed to API
    v_ln_vendor_id                NUMBER; --Line LEvel Variable for Vendor ID
    v_product_attribute           VARCHAR2 (50); --Line Level Variable for Product Attribute (Item/Cat Class)
    v_product_value               VARCHAR2 (50); --Line Level Variable for Product Value (SKU Number)
    v_product_attribute_api       VARCHAR2 (50); --Line Level Variable to set in API
    v_product_attribute_context   VARCHAR2 (30) := 'ITEM'; --Variable to set product_attribute_context in Pricing Attr API
    v_product_attr_value          VARCHAR2 (50);
    v_generate_using_formula_id   NUMBER;
    V9                            NUMBER;
    i                             NUMBER;
    j                             NUMBER;
    L_MSG                         VARCHAR2(240);
    l_formula_id                  NUMBER;


   
    CURSOR LINEID                                        
    IS
      select agreement_line_id 
      from xxwc.xxwc_om_contract_pricing_lines 
      where agreement_id = v_agreement_id 
      and revision_number = 0
      and interfaced_flag = 'N' ;
   


    BEGIN
    
           BEGIN
        l_formula_id:=NULL;
        SELECT Price_Formula_Id
          INTO l_formula_id
          FROM QP_PRICE_FORMULAS
         WHERE UPPER(NAME) = 'PERCENT DISCOUNT' AND ROWNUM = 1;
       EXCEPTION
        WHEN OTHERS THEN
        l_formula_id := NULL;
       END;   
    
      i := 0;

      FOR c2_rec IN LINEID 
      LOOP
         i := i + 1;

      /*Initial select to set variables with CSP Line Infomation */
        BEGIN
         SELECT agreement_id
              , agreement_line_id
              , agreement_type
              , vendor_quote_number
              , product_attribute
              , product_value
              , start_date
              , end_date
              , modifier_type
              , modifier_value
              , special_cost
              , vendor_id
              , revision_number
           INTO V9
              , v_ln_id
              ,                    --Line Level Variable for agreement_line_id
               v_ln_agreement_type
              ,                       --Line Level Variable for agreement_type
               v_ln_vendor_quote_number
              ,             --Line Level Variable for v_ln_vendor_quote_number
               v_product_attribute
              ,   --Line Level Variable for Product Attribute (Item/Cat Class)
               v_product_value
              , v_ln_start_date
              ,                           --Line Level Variable for start date
               v_ln_end_date
              ,                             --Line Level Variable for end date
               v_ln_mod_type
              ,                        --Line Level Variable for Modifier Type
               v_ln_mod_value
              ,                       --Line Level Variable for Modifier Value
               v_ln_special_cost
              ,                         --Line Level Variable for Special Cost
               v_ln_vendor_id              --Line LEvel Variable for Vendor ID
              , 
               v_revision_number
           FROM xxwc.xxwc_om_contract_pricing_lines
          WHERE agreement_line_id = c2_rec.agreement_line_id;
        EXCEPTION
          WHEN OTHERS THEN 
            L_MSG := SUBSTR('Initial select failed verify product infomration' || SQLCODE || SQLERRM,1,240) ;
        END;  


      /*Select modifier list_header_id from modifier list */
        BEGIN
         SELECT list_header_id
           INTO v_list_header_id
           FROM qp_list_headers
          WHERE attribute14 = V9;
        EXCEPTION
          WHEN OTHERS THEN 
            L_MSG := SUBSTR('No pricing Agreement Exists' || SQLCODE || SQLERRM,1,240) ;
        END;  


        /*Determine Imcompatability from CSP Agreement*/
         BEGIN
         SELECT incompatability_group
           INTO v_incompatability_group
           FROM xxwc.xxwc_om_contract_pricing_hdr
          WHERE agreement_id = v9;        
        EXCEPTION
          WHEN OTHERS THEN 
            L_MSG := SUBSTR('No incompatibility group found' || SQLCODE || SQLERRM,1,240) ;
        END; 

         --Set Incompatability Group in API
         IF v_incompatability_group = 'Best Price Wins'
         THEN
            v_incomp_group := 'LVL 1';
         ELSIF v_incompatability_group = 'Exclusive'
         THEN
            v_incomp_group := 'EXCL';
         ELSE
            v_incomp_group := NULL;
         END IF;

         --Set Product Attribute Variable to pass to API (Item, Cat Class, Category)
         IF v_product_attribute = 'ITEM'
         THEN
            v_product_attribute_api := 'PRICING_ATTRIBUTE1';
         ELSIF v_product_attribute = 'CAT_CLASS'
         THEN
            v_product_attribute_api := 'PRICING_ATTRIBUTE2';
         ELSIF v_product_attribute = 'CATEGORY'
         THEN
            v_product_attribute_api := 'PRICING_ATTRIBUTE26';
         ELSE
            v_arithmetic_operator := NULL;
         END IF;

         --Set Arithetic Operator Variable
         IF v_ln_mod_type = 'New Price'
         THEN
            v_arithmetic_operator := 'NEWPRICE';
         ELSIF v_ln_mod_type = 'Amount'
         THEN
            v_arithmetic_operator := 'AMT';
         ELSIF v_ln_mod_type = 'Percent'
         THEN
            v_arithmetic_operator := 'NEWPRICE';  /*TMS: Multi Decimal Rounding, Toby Rave, changed from % to NEW PRICE */
         ELSIF v_ln_mod_type = 'Cost Plus'
         THEN
            v_arithmetic_operator := 'NEWPRICE';
         ELSE
            v_arithmetic_operator := NULL;
         END IF;

         --IF Application Mthod = Cost Plus then set formula ID variable in API
         IF v_ln_mod_type = 'Cost Plus'
         THEN
            v_generate_using_formula_id := 7030;
         /*TMS: 20131013-00045 Multi Decimal Rounding, Toby Rave, block added to set % discounts to formual based*/
         ELSIF v_ln_mod_type = 'Percent'
         THEN
            v_generate_using_formula_id := l_formula_id ;
         ELSE
            v_generate_using_formula_id := NULL;
         END IF;
         
         


         IF v_ln_mod_type = 'New Price'
         THEN
            v_ln_mod_val := v_ln_mod_value;
         ELSIF v_ln_mod_type = 'Amount'
         THEN
            v_ln_mod_val := v_ln_mod_value;
         ELSIF v_ln_mod_type = 'Percent'
         THEN
            v_ln_mod_val := ((100 - v_ln_mod_value) / 100) ;   /*TMS: 20131013-00045 Multi Decimal Rounding, Toby Rave, calculation added to calulation operand to pass to % discount formula modifier*/
         ELSIF v_ln_mod_type = 'Cost Plus'
         THEN
            v_ln_mod_val := (v_ln_mod_value / 100);
         ELSE
            v_arithmetic_operator := NULL;
         END IF;

      /*Find Vendor ID for Vendor Quote Lines*/
         IF v_ln_agreement_type = 'VQN'
         THEN
          BEGIN 
            SELECT segment1
              INTO v_atrribute_3
              FROM ap_suppliers
             WHERE vendor_id = v_ln_vendor_id;
           EXCEPTION
            WHEN OTHERS THEN 
              L_MSG := SUBSTR('INVALID VENDOR' || SQLCODE || SQLERRM,1,240) ;
          END; 
        
         ELSE
            v_atrribute_3 := NULL;
         END IF;

        /* Set Item Numbers, Category Class Variables, and Category variables */
         IF v_product_attribute = 'ITEM'
         THEN
          BEGIN
            SELECT inventory_item_id
              INTO v_product_attr_value
              FROM mtl_system_items_b
             WHERE segment1 = v_product_value AND organization_id = '222';
          EXCEPTION
            WHEN OTHERS THEN 
              L_MSG := SUBSTR('ITEM dOES NOT EXIST' || SQLCODE || SQLERRM,1,240) ;
          END; 
         ELSIF v_product_attribute = 'CAT_CLASS'
         THEN
          BEGIN
            SELECT category_id
              INTO v_product_attr_value
              FROM mtl_categories_b
             WHERE segment1 || '.' || segment2 = v_product_value;
           EXCEPTION
            WHEN OTHERS THEN 
              L_MSG := SUBSTR('INVALID CATEGORY ID' || SQLCODE || SQLERRM,1,240) ;
          END; 
       /* Value from CSP Line Line used as variable for CATEGORY insert */   
         ELSIF v_product_attribute = 'CATEGORY'
         THEN
            v_product_attr_value := v_product_value;
         ELSE
            v_product_attr_value := NULL;
         END IF;

      /*Set Line Line DFF on Modifier to CSP or VQN */
         IF v_ln_agreement_type = 'VQN'
         THEN
            v_ln_agree_type := 'Vendor Quote';
         ELSIF v_ln_agreement_type = 'CSP'
         THEN
            v_ln_agree_type := 'Contract Pricing';
         ELSE
            v_ln_agree_type := NULL;
         END IF;

        /* Set Modifier Line API Variables */
         l_MODIFIERS_tbl (i).list_header_id := v_list_header_id;
         l_MODIFIERS_tbl (i).list_line_type_code := 'DIS';
         l_MODIFIERS_tbl (i).automatic_flag := 'Y';
         l_MODIFIERS_tbl (i).modifier_level_code := 'LINE';
         l_MODIFIERS_tbl (i).accrual_flag := 'N';
         l_MODIFIERS_tbl (i).start_date_active := v_ln_start_date;
         l_MODIFIERS_tbl (i).end_date_active := v_ln_end_date;
         l_MODIFIERS_tbl (i).arithmetic_operator := v_arithmetic_operator;
         l_MODIFIERS_tbl (i).pricing_group_sequence := 1;     --Pricing Bucket
         l_MODIFIERS_tbl (i).pricing_phase_id := 2; --Pricing Phase, '2' = List Line Adjustment
         l_MODIFIERS_tbl (i).product_precedence := 220;
         l_MODIFIERS_tbl (i).operand := v_ln_mod_val;
         l_MODIFIERS_tbl (i).INCLUDE_ON_RETURNS_FLAG := 'Y' ;
         l_MODIFIERS_tbl (i).context := '162';
         l_MODIFIERS_tbl (i).ATTRIBUTE2 := v_ln_id;
         l_MODIFIERS_tbl (i).ATTRIBUTE3 := v_atrribute_3;
         l_MODIFIERS_tbl (i).ATTRIBUTE4 := v_ln_special_cost;
         l_MODIFIERS_tbl (i).ATTRIBUTE5 := v_ln_agree_type;
         l_MODIFIERS_tbl (i).ATTRIBUTE6 := v_ln_vendor_quote_number;
         l_MODIFIERS_tbl (i).price_by_formula_id :=v_generate_using_formula_id;
         l_MODIFIERS_tbl (i).incompatibility_grp_code := v_incomp_group; --'LVL 1' or 'EXCL'
         l_MODIFIERS_tbl (i).operation := QP_GLOBALS.G_OPR_CREATE;


        /* Set Product Attribute API Variables */
         l_PRICING_ATTR_tbl (i).list_header_id := v_list_header_id;
         l_PRICING_ATTR_tbl (i).product_attribute_context := v_product_attribute_context;       --v_product_attribute_context ;
         l_PRICING_ATTR_tbl (i).product_attribute := v_product_attribute_api;
         l_PRICING_ATTR_tbl (i).product_attr_value := v_product_attr_value;
         --l_PRICING_ATTR_tbl(i).product_uom_code:= 'Ea';
         l_PRICING_ATTR_tbl (i).MODIFIERS_index := i;
         l_PRICING_ATTR_tbl (i).operation := QP_GLOBALS.G_OPR_CREATE;
         
     /*Update line level revision number to bext revision*/    
    /*Update interfaced flag to Yes * once the line has been processed*/
    /*  Update CSP line status to approved */    
        UPDATE xxwc.xxwc_om_contract_pricing_lines
        SET revision_number = (v_revision_number + 1)
          , interfaced_flag = 'Y'
          , line_status = 'APPROVED' 
        WHERE agreement_line_id = c2_rec.agreement_line_id ; 
      END LOOP;

      QP_Modifiers_PUB.Process_Modifiers (
         p_api_version_number      => 1.0
       , p_init_msg_list           => FND_API.G_FALSE
       , p_return_values           => FND_API.G_FALSE
       , p_commit                  => FND_API.G_FALSE
       , x_return_status           => l_return_status
       , x_msg_count               => x_msg_count
       , x_msg_data                => x_msg_data
       , p_MODIFIER_LIST_rec       => l_MODIFIER_LIST_rec
       , p_MODIFIERS_tbl           => l_MODIFIERS_tbl
       , p_QUALIFIERS_tbl          => l_QUALIFIERS_tbl
       , p_PRICING_ATTR_tbl        => l_PRICING_ATTR_tbl
       , x_MODIFIER_LIST_rec       => l_x_MODIFIER_LIST_rec
       , x_MODIFIER_LIST_val_rec   => l_x_MODIFIER_LIST_val_rec
       , x_MODIFIERS_tbl           => l_x_MODIFIERS_tbl
       , x_MODIFIERS_val_tbl       => l_x_MODIFIERS_val_tbl
       , x_QUALIFIERS_tbl          => l_x_QUALIFIERS_tbl
       , x_QUALIFIERS_val_tbl      => l_x_QUALIFIERS_val_tbl
       , x_PRICING_ATTR_tbl        => l_x_PRICING_ATTR_tbl
       , x_PRICING_ATTR_val_tbl    => l_x_PRICING_ATTR_val_tbl);
       commit ; 
  END create_lines;
   
/*************************************************************************
*   PROCEDURE Name: update_lines
*
*   PURPOSE:   UPDATE_LINES procedure is used to create update Lines
*              in Advanced Pricing related to the CSP Agreement Header 
*              'i_agreement_id' 
****************************************************************************/

  PROCEDURE update_lines (i_agreement_id IN NUMBER)
  IS 
    l_control_rec QP_GLOBALS.Control_Rec_Type;
    l_return_status VARCHAR2(1);
    x_msg_count number;
    x_msg_data Varchar2(2000);
    x_msg_index number;
    
    l_MODIFIER_LIST_rec QP_Modifiers_PUB.Modifier_List_Rec_Type;
    l_MODIFIER_LIST_val_rec QP_Modifiers_PUB.Modifier_List_Val_Rec_Type;
    l_MODIFIERS_tbl QP_Modifiers_PUB.Modifiers_Tbl_Type;
    l_MODIFIERS_val_tbl QP_Modifiers_PUB.Modifiers_Val_Tbl_Type;
    l_QUALIFIERS_tbl QP_Qualifier_Rules_PUB.Qualifiers_Tbl_Type;
    l_QUALIFIERS_val_tbl QP_Qualifier_Rules_PUB.Qualifiers_Val_Tbl_Type;
    l_PRICING_ATTR_tbl QP_Modifiers_PUB.Pricing_Attr_Tbl_Type;
    l_PRICING_ATTR_val_tbl QP_Modifiers_PUB.Pricing_Attr_Val_Tbl_Type;
    
    l_x_MODIFIER_LIST_rec QP_Modifiers_PUB.Modifier_List_Rec_Type;
    l_x_MODIFIER_LIST_val_rec QP_Modifiers_PUB.Modifier_List_Val_Rec_Type;
    l_x_MODIFIERS_tbl QP_Modifiers_PUB.Modifiers_Tbl_Type;
    l_x_MODIFIERS_val_tbl QP_Modifiers_PUB.Modifiers_Val_Tbl_Type;
    l_x_QUALIFIERS_tbl QP_Qualifier_Rules_PUB.Qualifiers_Tbl_Type;
    l_x_QUALIFIERS_val_tbl QP_Qualifier_Rules_PUB.Qualifiers_Val_Tbl_Type;
    l_x_PRICING_ATTR_tbl QP_Modifiers_PUB.Pricing_Attr_Tbl_Type;
    l_x_PRICING_ATTR_val_tbl QP_Modifiers_PUB.Pricing_Attr_Val_Tbl_Type;
    
    --CSP Header Level Variables
    v_agreement_id NUMBER := i_agreement_id;
    v_agreement_type VARCHAR2(30) ;
    v_vendor_quote_number NUMBER ;
    v_price_type VARCHAR2(30);
    v_customer_id NUMBER ;
    v_customer_site_id NUMBER ;
    v_agreement_status VARCHAR2(30) ;
    v_revision_number NUMBER ;
    v_incompatability_group VARCHAR2(30);
    v_incomp_group VARCHAR2(30);
    v_attribute_10 VARCHAR2(25) :='Contract Pricing'; --Variable to populate DFF Attribute 10 on Modifier Header (Contract Pricing or Vendor Quote)
    v_attribute_11 Number := NULL ;
    v_attribute_12 VARCHAR2(25) := NULL ; --Vendor Name for VQN
    v_attribute_13 VARCHAR2(25) := NULL ; --VEndor Number for VQN
    v_attribute_14 Number ; --Mapped to Vendor Agreement Number DFF on Modifier Header
    v_attribute_15 Number ; --Mapped to Agreement Revision Number DFF on Modifier Header
    v1_price_type VARCHAR2(30);
    v_customer_name VARCHAR2(50) ;
    v_party_id Number ;
    v_party_name VARCHAR2(360) ;
    v_party_site_number Number ;
    v_qualifier_value Number ;
    v_agreeid Number ;
    v_qual_context VARCHAR2(360);
    v_excluder_flag VARCHAR2(1) ;
    v_operator_code VARCHAR(360) ;
    v_delimitier VARCHAR2(1) ;
    v_qualifier_grouping_no Number ;
    v_qualifier_precedence Number ; 
    v_operation VARCHAR2(30) ; 
    v_active_flag VARCHAR2(30) ;
    v_list_header_id NUMBER ; --v_list_header_id
    v_list_line_id NUMBER ; 
    --START XXWC_CSP_LINES VARIABLES
    v_atrribute_3 NUMBER ;
    v_ln_special_cost NUMBER ;
    v_ln_id NUMBER ; --Line Level Variable for xxwc.agreement_line_id
    v_ln_agreement_type VARCHAR2(30) ; --Line Level Variable for xxwc.agreement_type
    v_ln_agree_type VARCHAR2(30) ; --Set CSP or VQN Agreement Type Variable passed to API
    v_ln_vendor_quote_number VARCHAR2(30) ;  --Line Level Variable for XXWC.VENDOR_QUOTE_NUMBER
    v_ln_start_date DATE ; --Line Level Variable for XXWC.START_DATE
    v_ln_end_date DATE;  --Line Level Variable for XXWC.END_DATE
    v_ln_mod_type  VARCHAR2(30) ; --Line Level Variable for Modifier Type
    v_arithmetic_operator VARCHAR2(30) ; --Line Level Variable for Modifier Type API Value
    v_ln_mod_value NUMBER ; --Line Level Variable for Modifier Value
    v_ln_mod_val NUMBER ; --Line Level Modifer Value passed to API
    v_ln_vendor_id NUMBER ;--Line LEvel Variable for Vendor ID
    v_product_attribute VARCHAR2(50) ;  --Line Level Variable for Product Attribute (Item/Cat Class)
    v_product_value VARCHAR2(50) ;--Line Level Variable for Product Value (SKU Number)
    v_product_attribute_api VARCHAR2(50) ; --Line Level Variable to set in API
    v_product_attribute_context VARCHAR2(30) := 'ITEM' ; --Variable to set product_attribute_context in Pricing Attr API
    v_product_attr_value VARCHAR2(50) ;
    v_generate_using_formula_id NUMBER ;
    V9 NUMBER ; 
    i NUMBER ;
    j NUMBER ;
    L_MSG VARCHAR2(240) ; 
    l_formula_id NUMBER ; 
           
    CURSOR LINEIDUPDATE 
    IS select agreement_line_id 
    from xxwc.xxwc_om_contract_pricing_lines 
    where agreement_id = v_agreement_id 
    and revision_number != 0
    and interfaced_flag = 'N' ;
        
    Begin
            
            BEGIN
        l_formula_id:=NULL;
        SELECT Price_Formula_Id
          INTO l_formula_id
          FROM QP_PRICE_FORMULAS
         WHERE UPPER(NAME) = 'PERCENT DISCOUNT' AND ROWNUM = 1;
       EXCEPTION
        WHEN OTHERS THEN
        l_formula_id := NULL;
       END;   
            
      i := 0 ;
      for c3_rec in LINEIDUPDATE 
      loop
        i:= i + 1 ;
      /*  Initial select to set line level variables */    
        BEGIN
          SELECT 
          start_date,
          end_date,
          modifier_type,
          modifier_value,
          revision_number
          INTO
          v_ln_start_date, --Line Level Variable for start date
          v_ln_end_date, --Line Level Variable for end date
          v_ln_mod_type, --Line Level Variable for Modifier Type
          v_ln_mod_value, --Line Level Variable for Modifier Value
          v_revision_number
          FROM xxwc.xxwc_om_contract_pricing_lines
          WHERE agreement_line_id = c3_rec.agreement_line_id ;           
        EXCEPTION
            WHEN OTHERS THEN 
              L_MSG := SUBSTR('Initial select failed, check modifier line' || SQLCODE || SQLERRM,1,240) ;
          END; 
          
      /*  Initial select to set line level variables */            
        BEGIN
          select list_line_id
          into v_list_line_id
          from qp_list_lines
          where attribute2 = c3_rec.agreement_line_id ;
        EXCEPTION
            WHEN OTHERS THEN 
              L_MSG := SUBSTR('No modifier list header id exists' || SQLCODE || SQLERRM,1,240) ;
          END; 
          
          IF v_ln_mod_type = 'New Price' 
          THEN v_arithmetic_operator := 'NEWPRICE' ;
          ELSIF v_ln_mod_type = 'Amount' 
          THEN v_arithmetic_operator := 'AMT' ;
          ELSIF v_ln_mod_type = 'Percent' 
          THEN v_arithmetic_operator := 'NEWPRICE';  /*TMS: 20131013-00045 Multi Decimal Rounding, Toby Rave, changed from % to NEW PRICE */
          ELSIF v_ln_mod_type = 'Cost Plus' 
          THEN v_arithmetic_operator := 'NEWPRICE' ;
          Else v_arithmetic_operator := NULL ;
          END IF;
          
          IF v_ln_mod_type = 'Cost Plus' 
          THEN v_generate_using_formula_id := 7030 ;
        /*TMS: 20131013-00045 Multi Decimal Rounding, Toby Rave, block added to set % discounts to formual based*/
         ELSIF v_ln_mod_type = 'Percent'
         THEN
            v_generate_using_formula_id := l_formula_id ;
         ELSE
            v_generate_using_formula_id := NULL;
         END IF;
          
          IF v_ln_mod_type = 'New Price'
          THEN v_ln_mod_val := v_ln_mod_value;
          ELSIF v_ln_mod_type = 'Amount'
          THEN
          v_ln_mod_val := v_ln_mod_value;
          ELSIF v_ln_mod_type = 'Percent'
          THEN
          v_ln_mod_val := ((100 - v_ln_mod_value) / 100) ;   /*TMS: 20131013-00045 Multi Decimal Rounding, Toby Rave, calculation added to calulation operand to pass to % discount formula modifier*/
          ELSIF v_ln_mod_type = 'Cost Plus'
          THEN
          v_ln_mod_val := (v_ln_mod_value / 100);
          ELSE
          v_arithmetic_operator := NULL;
          END IF;
          
          
          /*  Populate API Variables for Modifier Line Level Updates */
            l_MODIFIERS_tbl(i).list_line_id := v_list_line_id ;
            l_MODIFIERS_tbl(i).start_date_active := v_ln_start_date ;
            l_MODIFIERS_tbl(i).end_date_active := v_ln_end_date ;
            l_MODIFIERS_tbl(i).arithmetic_operator := v_arithmetic_operator ;
            l_MODIFIERS_tbl(i).operand := v_ln_mod_val ;
            l_MODIFIERS_tbl(i).price_by_formula_id := v_generate_using_formula_id ;
            l_MODIFIERS_tbl(i).operation := QP_GLOBALS.G_OPR_UPDATE; 
          
        /*Set CSP line revision to next revision */  
        /* Set CSP Line Interfaced flag to Y */
        /* Set CSP Lines status from AWAITING APPROVAL to APPROVED */        
          UPDATE xxwc.xxwc_om_contract_pricing_lines
          SET revision_number = (v_revision_number + 1) 
            , interfaced_flag = 'Y'
            , line_status = 'APPROVED'
          WHERE agreement_line_id = c3_rec.agreement_line_id ; 
      END LOOP ; 
        
      /*  API Call */
        QP_Modifiers_PUB.Process_Modifiers
        ( p_api_version_number => 1.0
        , p_init_msg_list => FND_API.G_FALSE
        , p_return_values => FND_API.G_FALSE
        , p_commit => FND_API.G_FALSE
        , x_return_status => l_return_status
        , x_msg_count =>x_msg_count
        , x_msg_data =>x_msg_data
        ,p_MODIFIER_LIST_rec => l_MODIFIER_LIST_rec
        ,p_MODIFIERS_tbl => l_MODIFIERS_tbl
        ,p_QUALIFIERS_tbl => l_QUALIFIERS_tbl
        ,p_PRICING_ATTR_tbl => l_PRICING_ATTR_tbl
        ,x_MODIFIER_LIST_rec => l_x_MODIFIER_LIST_rec         
        ,x_MODIFIER_LIST_val_rec => l_x_MODIFIER_LIST_val_rec 
        ,x_MODIFIERS_tbl => l_x_MODIFIERS_tbl                 
        ,x_MODIFIERS_val_tbl => l_x_MODIFIERS_val_tbl         
        ,x_QUALIFIERS_tbl => l_x_QUALIFIERS_tbl               
        ,x_QUALIFIERS_val_tbl => l_x_QUALIFIERS_val_tbl       
        ,x_PRICING_ATTR_tbl => l_x_PRICING_ATTR_tbl           
        ,x_PRICING_ATTR_val_tbl => l_x_PRICING_ATTR_val_tbl   
        );
      COMMIT;
    
  /*Delete all line level exclusions associated with CSP Agreement */  
    select list_header_id
    into v_list_header_id
    from qp_list_headers
    where attribute14 = v_agreement_id ; 
    
    delete from
    qp_pricing_attributes
    where list_header_id = v_list_header_id
    and excluder_flag = 'Y' ; 
    commit ; 
    
  END update_lines;

/*************************************************************************
*   PROCEDURE Name: create exclusions
*
*   PURPOSE:   create exclusions procedure is used to create 
*              'Item/Category Class' , 'Item/Category' Exclusions, 
*              and 'Category Class/Category Exclusions on modifier list lines
****************************************************************************/
   PROCEDURE create_exclusions (i_agreement_id IN NUMBER)
   IS
      /* $Header: QPXEXDS1.sql 115.6 2006/08/24 04:51:05 nirmkuma ship $ */
      l_control_rec                 QP_GLOBALS.Control_Rec_Type;
      l_return_status               VARCHAR2 (1);
      x_msg_count                   NUMBER;
      x_msg_data                    VARCHAR2 (2000);
      x_msg_index                   NUMBER;

      l_MODIFIER_LIST_rec           QP_Modifiers_PUB.Modifier_List_Rec_Type;
      l_MODIFIER_LIST_val_rec       QP_Modifiers_PUB.Modifier_List_Val_Rec_Type;
      l_MODIFIERS_tbl               QP_Modifiers_PUB.Modifiers_Tbl_Type;
      l_MODIFIERS_val_tbl           QP_Modifiers_PUB.Modifiers_Val_Tbl_Type;
      l_QUALIFIERS_tbl              QP_Qualifier_Rules_PUB.Qualifiers_Tbl_Type;
      l_QUALIFIERS_val_tbl          QP_Qualifier_Rules_PUB.Qualifiers_Val_Tbl_Type;
      l_PRICING_ATTR_tbl            QP_Modifiers_PUB.Pricing_Attr_Tbl_Type;
      l_PRICING_ATTR_val_tbl        QP_Modifiers_PUB.Pricing_Attr_Val_Tbl_Type;

      l_x_MODIFIER_LIST_rec         QP_Modifiers_PUB.Modifier_List_Rec_Type;
      l_x_MODIFIER_LIST_val_rec     QP_Modifiers_PUB.Modifier_List_Val_Rec_Type;
      l_x_MODIFIERS_tbl             QP_Modifiers_PUB.Modifiers_Tbl_Type;
      l_x_MODIFIERS_val_tbl         QP_Modifiers_PUB.Modifiers_Val_Tbl_Type;
      l_x_QUALIFIERS_tbl            QP_Qualifier_Rules_PUB.Qualifiers_Tbl_Type;
      l_x_QUALIFIERS_val_tbl        QP_Qualifier_Rules_PUB.Qualifiers_Val_Tbl_Type;
      l_x_PRICING_ATTR_tbl          QP_Modifiers_PUB.Pricing_Attr_Tbl_Type;
      l_x_PRICING_ATTR_val_tbl      QP_Modifiers_PUB.Pricing_Attr_Val_Tbl_Type;

      --Toby's Variables
      --CSP Header Level Variables
      v_agreement_id                NUMBER := i_agreement_id;
      v_agreement_type              VARCHAR2 (30);
      v_vendor_quote_number         NUMBER;
      v_price_type                  VARCHAR2 (30);
      v_customer_id                 NUMBER;
      v_customer_site_id            NUMBER;
      v_agreement_status            VARCHAR2 (30);
      v_revision_number             NUMBER;
      v_incompatibility_group       VARCHAR2 (30);
      v_attribute_10                VARCHAR2 (25) := 'Contract Pricing'; --Variable to populate DFF Attribute 10 on Modifier Header (Contract Pricing or Vendor Quote)
      v_attribute_11                NUMBER := NULL;
      v_attribute_12                VARCHAR2 (25) := NULL; --Vendor Name for VQN
      v_attribute_13                VARCHAR2 (25) := NULL; --VEndor Number for VQN
      v_attribute_14                NUMBER; --Mapped to Vendor Agreement Number DFF on Modifier Header
      v_attribute_15                NUMBER; --Mapped to Agreement Revision Number DFF on Modifier Header
      v1_price_type                 VARCHAR2 (30);
      v_customer_name               VARCHAR2 (50);
      v_party_id                    NUMBER;
      v_party_name                  VARCHAR2 (360);
      v_party_site_number           NUMBER;
      v_qualifier_value             NUMBER;
      v_agreeid                     NUMBER;
      v_qual_context                VARCHAR2 (360);
      v_excluder_flag               VARCHAR2 (1);
      v_operator_code               VARCHAR (360);
      v_delimitier                  VARCHAR2 (1);
      v_qualifier_grouping_no       NUMBER;
      v_qualifier_precedence        NUMBER;
      v_operation                   VARCHAR2 (30);
      v_active_flag                 VARCHAR2 (30);
      v_list_header_id              NUMBER;
      v_list_line_id                NUMBER;
      --START XXWC_CSP_LINES VARIABLES
      v_ln_id                       NUMBER; --Line Level Variable for xxwc.agreement_line_id
      v_ln_agreement_type           VARCHAR2 (30); --Line Level Variable for xxwc.agreement_type
      v_ln_vendor_quote_number      VARCHAR2 (30); --Line Level Variable for XXWC.VENDOR_QUOTE_NUMBER
      v_ln_start_date               DATE; --Line Level Variable for XXWC.START_DATE
      v_ln_end_date                 DATE; --Line Level Variable for XXWC.END_DATE
      v_ln_mod_type                 VARCHAR2 (30); --Line Level Variable for Modifier Type
      v_arithmetic_operator         VARCHAR2 (30); --Line Level Variable for Modifier Type API Value
      v_ln_mod_value                NUMBER; --Line Level Variable for Modifier Value
      --v_ln_special_cost, --Line Level Variable for Special Cost
      --v_ln_vendor_id --Line LEvel Variable for Vendor ID
      v_product_attribute           VARCHAR2 (50); --Line Level Variable for Product Attribute (Item/Cat Class)
      v_product_value               VARCHAR2 (50); --Line Level Variable for Product Value (SKU Number)
      v_product_attribute_api       VARCHAR2 (50); --Line Level Variable to set in API
      v_product_attribute_context   VARCHAR2 (30) := 'ITEM'; --Variable to set product_attribute_context in Pricing Attr API
      v_product_attr_value          VARCHAR2 (50);
      v_generate_using_formula_id   NUMBER;
      v1                            NUMBER;            --Needed for Exclusions
      v2                            NUMBER;            --Needed for Exclusions
      v3                            NUMBER;            --Needed for Exclusions
      v4                            VARCHAR2 (50);     --Needed for Exclusions
      v5                            NUMBER;            --Needed for Exclusions
      V6                            NUMBER;
      p_agreement_id                NUMBER;
      L_MSG                         VARCHAR2(240) ; 
      
      
      

     /***** Added condition to all cursors for end_date is NULL per TMS 20131013-00042
      Toby Rave, 11-22-2013   ****/

         CURSOR AGREE_LINE_ID
      IS
         SELECT agreement_line_id                                    --INTO V3
           FROM xxwc.xxwc_om_contract_pricing_lines
          WHERE product_attribute = 'CAT_CLASS'
                AND agreement_id = v_agreement_id
                and end_date is null ;

      CURSOR ITEM_ID
      IS
         SELECT b.inventory_item_id
           FROM xxwc.xxwc_om_contract_pricing_lines a
              , mtl_system_items_b b
              , mtl_item_categories c
          WHERE     a.product_value = b.segment1
                AND a.product_attribute = 'ITEM'
                AND b.organization_id = '222'
                AND b.organization_id = c.organization_id
                AND b.inventory_item_id = c.inventory_item_id
                AND a.agreement_id = v_agreement_id
                AND c.category_id = V1
                and a.end_date is null ;

      CURSOR CATEGORY_LINE_ID
      IS
         SELECT agreement_line_id
           FROM xxwc.xxwc_om_contract_pricing_lines
          WHERE product_attribute = 'CATEGORY'
                AND agreement_id = v_agreement_id
                AND end_date is null ; 

      CURSOR CATEGORY_ID
      IS
         SELECT b.category_id
           FROM XXWC.xxwc_om_contract_pricing_lines a, mtl_categories_b b
          WHERE     a.product_attribute = 'CAT_CLASS'
                AND b.segment1 = V4
                AND a.product_value = b.segment1 || '.' || segment2
                and agreement_id = v_agreement_id 
                and a.end_date is null ; 

      CURSOR CAT_ITEM_EXE
      IS
         SELECT b.inventory_item_id
           FROM xxwc.xxwc_om_contract_pricing_lines a
              , mtl_system_items_b b
              , mtl_item_categories c
              , mtl_categories_b d
          WHERE     a.product_value = b.segment1
                AND a.product_attribute = 'ITEM'
                AND b.organization_id = '222'
                AND d.segment1 = V4
                AND b.organization_id = c.organization_id
                AND b.inventory_item_id = c.inventory_item_id
                AND c.category_id = d.category_id
                AND a.agreement_id = v_agreement_id
                AND a.end_date is null ; 
   BEGIN
                
      FOR c2_rec IN AGREE_LINE_ID
      LOOP
      
    /**************************************
    *   Creating Category Level Exclusions
    **************************************/

       BEGIN
         SELECT list_line_id
           INTO v_list_line_id
           FROM qp_list_lines
          WHERE attribute2 = c2_rec.agreement_line_id;
        EXCEPTION
            WHEN OTHERS THEN 
              L_MSG := SUBSTR('NO MODIFIER LINE EXISTS FOR CATEGORY' || SQLCODE || SQLERRM,1,240) ;
          END;           

      /*   select category ID to query items againsts */
        BEGIN 
         SELECT category_id
           INTO V1
           FROM xxwc.xxwc_om_contract_pricing_lines a, mtl_categories_b b
          WHERE     a.product_attribute = 'CAT_CLASS'
                AND b.segment1 || '.' || b.segment2 = a.product_value
                AND a.agreement_line_id = c2_rec.agreement_line_id;
        EXCEPTION
            WHEN OTHERS THEN 
              L_MSG := SUBSTR('INVALID CATEGORY ID' || SQLCODE || SQLERRM,1,240) ;
          END;                

                  FND_FILE.Put_Line(FND_FILE.Log,'AGREEMENT LINE ID = '||c2_rec.agreement_line_id);
          
            
        /* Find Item Numbers assigned to category id from and loop them into API */
         FOR c3_rec IN ITEM_ID
         LOOP
            --Set Pricing Attributes in API
            l_PRICING_ATTR_tbl (1).list_line_id := v_list_line_id;  --173088 ;
            l_PRICING_ATTR_tbl (1).product_attribute_context := 'ITEM'; --v_product_attribute_context ; --v_product_attribute_context ;
            l_PRICING_ATTR_tbl (1).product_attribute := 'PRICING_ATTRIBUTE1'; --v_product_attribute_api ;
            l_PRICING_ATTR_tbl (1).product_attr_value :=
               c3_rec.inventory_item_id;                  --2928289 505CAMLOCK
            l_PRICING_ATTR_tbl (1).excluder_flag := 'Y';
            l_PRICING_ATTR_tbl (1).MODIFIERS_index := 1;
            l_PRICING_ATTR_tbl (1).operation := QP_GLOBALS.G_OPR_CREATE;

            --API Call
            QP_Modifiers_PUB.Process_Modifiers (
               p_api_version_number      => 1.0
             , p_init_msg_list           => FND_API.G_FALSE
             , p_return_values           => FND_API.G_FALSE
             , p_commit                  => FND_API.G_FALSE
             , x_return_status           => l_return_status
             , x_msg_count               => x_msg_count
             , x_msg_data                => x_msg_data
             , p_MODIFIER_LIST_rec       => l_MODIFIER_LIST_rec
             , p_MODIFIERS_tbl           => l_MODIFIERS_tbl
             , p_QUALIFIERS_tbl          => l_QUALIFIERS_tbl
             , p_PRICING_ATTR_tbl        => l_PRICING_ATTR_tbl
             , x_MODIFIER_LIST_rec       => l_x_MODIFIER_LIST_rec
             , x_MODIFIER_LIST_val_rec   => l_x_MODIFIER_LIST_val_rec
             , x_MODIFIERS_tbl           => l_x_MODIFIERS_tbl
             , x_MODIFIERS_val_tbl       => l_x_MODIFIERS_val_tbl
             , x_QUALIFIERS_tbl          => l_x_QUALIFIERS_tbl
             , x_QUALIFIERS_val_tbl      => l_x_QUALIFIERS_val_tbl
             , x_PRICING_ATTR_tbl        => l_x_PRICING_ATTR_tbl
             , x_PRICING_ATTR_val_tbl    => l_x_PRICING_ATTR_val_tbl);
            COMMIT;
         END LOOP;
      END LOOP;



   /************************************
    *Write Exclusions against Category
    ************************************/

      --Select Agreement Line_ID for Cateogry to write exclusions against
      FOR c4_rec IN CATEGORY_LINE_ID
      LOOP
      /* SELECT LIST LINE FROM QP LIST LINES */
        BEGIN
         SELECT list_line_id
           INTO v_list_line_id
           FROM qp_list_lines
          WHERE attribute2 = c4_rec.agreement_line_id;
        EXCEPTION
            WHEN OTHERS THEN 
              L_MSG := SUBSTR('No Modifier Line Found' || SQLCODE || SQLERRM,1,240) ;
          END;  
          
        BEGIN
         SELECT product_value
           INTO V4
           FROM XXWC.xxwc_om_contract_pricing_lines
          WHERE agreement_line_id = c4_rec.agreement_line_id;
        EXCEPTION
            WHEN OTHERS THEN 
              L_MSG := SUBSTR('INVALID PRODUCT VALUE' || SQLCODE || SQLERRM,1,240) ;
              FND_FILE.Put_Line(FND_FILE.Log,'AGREEMENT LINE ID = '||c4_rec.agreement_line_id);
          END;  
        /* Select Category Classes Assocaited to Category and call API */
         FOR c5_rec IN CATEGORY_ID
         LOOP
            l_PRICING_ATTR_tbl (1).list_line_id := v_list_line_id;  --173088 ;
            l_PRICING_ATTR_tbl (1).product_attribute_context := 'ITEM'; --v_product_attribute_context ; --v_product_attribute_context ;
            l_PRICING_ATTR_tbl (1).product_attribute := 'PRICING_ATTRIBUTE2'; --v_product_attribute_api ;
            l_PRICING_ATTR_tbl (1).product_attr_value := c5_rec.category_id; --2928289 505CAMLOCK v_product_attr_value ;
            l_PRICING_ATTR_tbl (1).excluder_flag := 'Y';
            l_PRICING_ATTR_tbl (1).MODIFIERS_index := 1;
            l_PRICING_ATTR_tbl (1).operation := QP_GLOBALS.G_OPR_CREATE;

            QP_Modifiers_PUB.Process_Modifiers (
               p_api_version_number      => 1.0
             , p_init_msg_list           => FND_API.G_FALSE
             , p_return_values           => FND_API.G_FALSE
             , p_commit                  => FND_API.G_FALSE
             , x_return_status           => l_return_status
             , x_msg_count               => x_msg_count
             , x_msg_data                => x_msg_data
             , p_MODIFIER_LIST_rec       => l_MODIFIER_LIST_rec
             , p_MODIFIERS_tbl           => l_MODIFIERS_tbl
             , p_QUALIFIERS_tbl          => l_QUALIFIERS_tbl
             , p_PRICING_ATTR_tbl        => l_PRICING_ATTR_tbl
             , x_MODIFIER_LIST_rec       => l_x_MODIFIER_LIST_rec
             , x_MODIFIER_LIST_val_rec   => l_x_MODIFIER_LIST_val_rec
             , x_MODIFIERS_tbl           => l_x_MODIFIERS_tbl
             , x_MODIFIERS_val_tbl       => l_x_MODIFIERS_val_tbl
             , x_QUALIFIERS_tbl          => l_x_QUALIFIERS_tbl
             , x_QUALIFIERS_val_tbl      => l_x_QUALIFIERS_val_tbl
             , x_PRICING_ATTR_tbl        => l_x_PRICING_ATTR_tbl
             , x_PRICING_ATTR_val_tbl    => l_x_PRICING_ATTR_val_tbl);
            COMMIT;
         END LOOP;
      END LOOP;

      /***********************************
      Assign Items to Cateogry Exclusions
      ************************************/

      FOR c4_rec IN CATEGORY_LINE_ID
      LOOP
         --SELECT LIST LINE FROM QP LIST LINES
        BEGIN
         SELECT list_line_id
           INTO v_list_line_id
           FROM qp_list_lines
          WHERE attribute2 = c4_rec.agreement_line_id;
        EXCEPTION
            WHEN OTHERS THEN 
              L_MSG := SUBSTR('INVALID PRODUCT VALUE' || SQLCODE || SQLERRM,1,240) ;
          END;  
          
       BEGIN   
         SELECT product_value
           INTO V4
           FROM XXWC.xxwc_om_contract_pricing_lines
          WHERE agreement_line_id = c4_rec.agreement_line_id;
        EXCEPTION
            WHEN OTHERS THEN 
              L_MSG := SUBSTR('INVALID PRODUCT VALUE' || SQLCODE || SQLERRM,1,240) ;
              FND_FILE.Put_Line(FND_FILE.Log,'AGREEMENT LINE ID = '||c4_rec.agreement_line_id);
          END;            

        /* Find Items associated to Category and Call API */
         FOR rec_r7 IN CAT_ITEM_EXE
         LOOP
            l_PRICING_ATTR_tbl (1).list_line_id := v_list_line_id;  --173088 ;
            l_PRICING_ATTR_tbl (1).product_attribute_context := 'ITEM'; --v_product_attribute_context ; --v_product_attribute_context ;
            l_PRICING_ATTR_tbl (1).product_attribute := 'PRICING_ATTRIBUTE1'; --v_product_attribute_api ;
            l_PRICING_ATTR_tbl (1).product_attr_value :=
               rec_r7.inventory_item_id; --2928289 505CAMLOCK v_product_attr_value ;
            l_PRICING_ATTR_tbl (1).excluder_flag := 'Y';
            l_PRICING_ATTR_tbl (1).MODIFIERS_index := 1;
            l_PRICING_ATTR_tbl (1).operation := QP_GLOBALS.G_OPR_CREATE;

            QP_Modifiers_PUB.Process_Modifiers (
               p_api_version_number      => 1.0
             , p_init_msg_list           => FND_API.G_FALSE
             , p_return_values           => FND_API.G_FALSE
             , p_commit                  => FND_API.G_FALSE
             , x_return_status           => l_return_status
             , x_msg_count               => x_msg_count
             , x_msg_data                => x_msg_data
             , p_MODIFIER_LIST_rec       => l_MODIFIER_LIST_rec
             , p_MODIFIERS_tbl           => l_MODIFIERS_tbl
             , p_QUALIFIERS_tbl          => l_QUALIFIERS_tbl
             , p_PRICING_ATTR_tbl        => l_PRICING_ATTR_tbl
             , x_MODIFIER_LIST_rec       => l_x_MODIFIER_LIST_rec
             , x_MODIFIER_LIST_val_rec   => l_x_MODIFIER_LIST_val_rec
             , x_MODIFIERS_tbl           => l_x_MODIFIERS_tbl
             , x_MODIFIERS_val_tbl       => l_x_MODIFIERS_val_tbl
             , x_QUALIFIERS_tbl          => l_x_QUALIFIERS_tbl
             , x_QUALIFIERS_val_tbl      => l_x_QUALIFIERS_val_tbl
             , x_PRICING_ATTR_tbl        => l_x_PRICING_ATTR_tbl
             , x_PRICING_ATTR_val_tbl    => l_x_PRICING_ATTR_val_tbl);
            COMMIT;
         END LOOP;
      END LOOP;
   END create_exclusions;


/*************************************************************************
*   PROCEDURE Name: import_modifiers
*
*   PURPOSE:   import modifiers procedure is used called by the CSP Maintenance 
               form and executes the four procedures above. Its is called for a 
               single CSP Agreement, i_agreement_id
****************************************************************************/
   PROCEDURE import_modifiers (errbuf              OUT VARCHAR2
                             , retcode             OUT VARCHAR2
                             , i_agreement_id   IN     NUMBER)
   IS
   BEGIN
      create_header (i_agreement_id);
      create_lines (i_agreement_id);
      update_lines (i_agreement_id);
      create_exclusions (i_agreement_id);
   END import_modifiers;
   

/*************************************************************************
*   PROCEDURE Name: csp_quote
*
*   PURPOSE:   csp_quote procedure is used to import a quote from Order 
               Management in a CSP Agreement. The program is called by 
               concurrent program 'XXWC OM Quote to CSP Import'

HISTORY
===============================================================================
VERSION DATE          AUTHOR(S)              DESCRIPTION
------- -----------   --------------- -----------------------------------------
1.0     01/09/2013    Lucidity Consulting    Initial Release
2.0    8/6/2014     Lucidity Consultig-Toby  TMS 20140725-00077 --Rounding logic has been corrected in CSP_QUOTE procedure and added error handling
********************************************************************************/

PROCEDURE csp_quote (errbuf OUT VARCHAR2, 
                     retcode OUT VARCHAR2, 
                     i_quotenumber IN NUMBER, 
                     i_price_type in VARCHAR2, 
                     i_incomp_group in VARCHAR2,
                     i_vq_number in VARCHAR2, 
                     i_vendor_number in NUMBER,
                     i_start_date in VARCHAR2) 
      IS



      l_customer_id     NUMBER;
      l_site_id         NUMBER;
      l_org_id          NUMBER;
      l_msg             VARCHAR2 (240);                    --VARCHAR2 (32000);
      l_date            DATE := SYSDATE;
      l_user_id         NUMBER := NVL (fnd_profile.VALUE ('USER_ID'), -1);
      l_prod_att        VARCHAR2 (100);
      l_vendor_id       NUMBER;
      l_desc            VARCHAR2 (240);
      l_list_price      NUMBER;
      l_average_cost    NUMBER;
      l_selling_price   NUMBER;
      l_gross_margin    NUMBER;
      l_price_type      VARCHAR2(30) := i_price_type; --Parameter Passed By User
      l_incomp_group    VARCHAR2(30):= i_incomp_group; --Parameter Pass by User
      l_quote_number    NUMBER := i_quotenumber ; --Parameter PAssed By User
      l_modifier_value  NUMBER;
      l_agreement_type  VARCHAR2(30); --CSP or VQN
      l_vq_number       VARCHAR2(30) := i_vq_number; --Parameter PAssed By User
      l_inventory_item_id  NUMBER ;
      l_unit_list_price   NUMBER ;
      l_unit_selling_price NUMBER ;
      l_unit_cost        NUMBER ;
      l_atrribute5       VARCHAR2(240);
      l_attribute13      VARCHAR2(240);
      l_agreement_id     NUMBER;
      l_vendor_number    NUMBER := i_vendor_number ; 
      l_cnt_agreement_id NUMBER;
      l_cnt_agree_id     NUMBER;
      l_exception        EXCEPTION ; 
      l_agreement_line_id NUMBER ;
      l_start_date       DATE := SYSDATE;
      l_int_flag          VARCHAR2(30) ; 
      l_start_date2    DATE ; 
      l_rounding_factor NUMBER ; /*TMS 20140725-00077 */
      l_item_type VARCHAR2(240) ; /*TMS 20140725-00077 */
      
      -- local error handling variables --/*TMS 20140725-00077 */
   l_err_callfrom             VARCHAR2 (75)
                                 := 'APPS.XXWC_OM_CSP_INTERFACE_PKG.CSP_QUOTE';
   l_err_callpoint            VARCHAR2 (75) := 'START';
   l_distro_list              VARCHAR2 (75)
                                 DEFAULT 'HDSOracleDevelopers@hdsupply.com';
   l_message                  VARCHAR2 (2000);
      
    
      
      CURSOR QUOTELINES IS
        select line_id
        from oe_order_lines_all a, 
        oe_order_headers_all b
        where b.order_number = l_quote_number
        and a.header_id = b.header_id 
        and a.Unit_Selling_Price = ( Select Min(Unit_Selling_Price) 
                                 from oe_order_lines_all a2
                                 Where a2.Header_ID = a.Header_ID 
                                 and   a2.Inventory_Item_ID = a.Inventory_Item_ID)
        and a.line_ID = ( Select Min(a3.Line_ID) 
                                 from oe_order_lines_all a3
                                 Where a3.Header_ID = a.Header_ID 
                                 and   a3.Inventory_Item_ID = a.Inventory_Item_ID 
                                 and   a3.Unit_Selling_Price = a.Unit_Selling_Price);
   BEGIN
         l_customer_id := NULL;
         l_site_id := NULL;
         l_org_id := NULL;
         l_msg := NULL;
         l_start_date := FND_DATE.CANONICAL_TO_DATE(i_start_date); --Change the p_date parameter from a varchar2 to a date
         
         l_err_callpoint := 'STAGE1'; 

         BEGIN
           SELECT 
           sold_to_org_id,
           ship_to_org_id,
           ship_from_org_id
           INTO
           l_customer_id,
           l_site_id,
           l_org_id
           FROM oe_order_headers_all
           WHERE order_number = l_quote_number ; 
           l_err_callpoint := 'STAGE2';
        EXCEPTION 
        WHEN OTHERS THEN
        L_MSG := SUBSTR('Cannot find customer ship to Info' || SQLCODE || SQLERRM,1,240) ;
        l_err_callpoint := 'STAGE3';
        RAISE L_EXCEPTION ; 
        END ; 
        
            FND_FILE.Put_Line(FND_FILE.Log,'l_quote_number = '||l_quote_number);
            FND_FILE.Put_Line(FND_FILE.Log,'l_customer_id = '||l_customer_id); 
            

         
         
         IF i_price_type = 'MASTER'
           THEN 
           Begin
             select count(agreement_id)
             INTO l_cnt_agreement_id
             FROM XXWC.xxwc_om_contract_pricing_hdr
             WHERE customer_id = l_customer_id 
             AND price_type = 'MASTER' ;
             l_err_callpoint := 'STAGE4'; 
           EXCEPTION
            WHEN OTHERS THEN
              L_MSG := SUBSTR('Agreement Count Returned NULL' || SQLCODE || SQLERRM,1,240) ;
              l_err_callpoint := 'STAGE5';
           End;
         ELSIF i_price_type = 'SHIPTO'
           THEN 
           Begin
             select count(agreement_id)
             INTO l_cnt_agreement_id
             FROM XXWC.xxwc_om_contract_pricing_hdr
             WHERE customer_id = l_customer_id
             AND customer_site_id = l_site_id 
             AND price_type = 'SHIPTO' ;
             l_err_callpoint := 'STAGE6'; 
           EXCEPTION
            WHEN OTHERS THEN
              L_MSG := SUBSTR('Agreement Count Returned NULL' || SQLCODE || SQLERRM,1,240) ;
              l_err_callpoint := 'STAGE7';
           END;
         ELSE l_cnt_agreement_id := null ;
        END IF;
        
        FND_FILE.Put_Line(FND_FILE.Log,'Agreement ID Count = '||l_cnt_agreement_id); 
        
        /*The following will execute if an existing CSP Agreement is not found for the Customer or Customer/Job Site Combo*/
        IF l_cnt_agreement_id = 0
        THEN        
         IF l_price_type = 'MASTER'
         THEN l_site_id := NULL ; 
         l_err_callpoint := 'STAGE8';
         END IF ; 
      
            /*Create CSP Header*/
            INSERT INTO xxwc_om_contract_pricing_hdr (agreement_id
                                                    , price_type
                                                    , customer_id
                                                    , customer_site_id
                                                    , agreement_status
                                                    , revision_number
                                                    , organization_id
                                                    , gross_margin
                                                    , incompatability_group
                                                    , creation_date
                                                    , created_by
                                                    , last_update_date
                                                    , last_updated_by
                                                    , last_update_login
                                                    , attribute10)
                 VALUES (
                           XXWC_OM_CONTRACT_PRICING_HDR_S.NEXTVAL
                         , l_price_type
                         , l_customer_id
                         , l_site_id
                         , 'DRAFT'
                         , 0
                         , l_org_id
                         , 0
                         , l_incomp_group
                         , l_date
                         , l_user_id
                         , l_date
                         , l_user_id
                         , 0
                         , l_quote_number);
                         
                         l_err_callpoint := 'STAGE9';
       
      COMMIT;  
      
      /*Create CSP Lines From Quote*/
        FOR c1_rec in QUOTELINES
        LOOP
        
           l_agreement_id :=  NULL ; 
           l_agreement_type :=  NULL ; 
           l_prod_att :=  NULL ;
           l_desc :=  NULL ;
           l_unit_list_price  :=  NULL ;
           l_attribute13 :=  NULL ;
           l_modifier_value :=  NULL ;
           l_atrribute5  :=  NULL ;
           l_unit_cost :=  NULL ;
           l_gross_margin :=  NULL ;
           l_unit_selling_price :=  NULL ;
           l_int_flag := NULL ; 
           l_rounding_factor := NULL ; /*TMS 20140725-00077 */
           l_item_type := NULL ; 
           
           l_err_callpoint := 'STAGE10';
             
        /*FIND CSP AGREEMENT NUMBER CREATED DURING THE IMPORT. REFERENCED QUOTE NUMBER IS STORED IN CSP HEADER DFF ATTRIBUTE10*/           
        /*BEGIN
          SELECT agreement_id
          INTO l_agreement_id
          FROM XXWC.xxwc_om_contract_pricing_hdr
          WHERE attribute10 = l_quote_number 
          AND price_type = l_price_type ;
        EXCEPTION
        WHEN OTHERS THEN 
          L_MSG := SUBSTR('AGREEMENT_ID NOT FOUND IN ATTRIBUTE10' || SQLCODE || SQLERRM,1,240) ;
        END;  */
        
       BEGIN
        select XXWC_OM_CONTRACT_PRICING_HDR_S.CURRVAL
        INTO l_agreement_id
        from dual;
        l_err_callpoint := 'STAGE11';
                EXCEPTION
        WHEN OTHERS THEN 
          L_MSG := SUBSTR('AGREEMENT_ID NOT FOUND IN ATTRIBUTE10' || SQLCODE || SQLERRM,1,240) ;
          l_err_callpoint := 'STAGE12';
        END;  
        
        FND_FILE.Put_Line(FND_FILE.Log,'l_customer_id = '||l_agreement_id); 
        
        /*Select quote line infomation for CSP Line Creation and calcutions*/
        BEGIN
          SELECT
          inventory_item_id,
          unit_list_price,
          unit_selling_price,
          unit_cost,
          attribute5, --Special Cost
          attribute13 --Modifier Type*/
          INTO
          l_inventory_item_id,
          l_unit_list_price,
          l_unit_selling_price,
          l_unit_cost,
          l_atrribute5, --Special Cost
          l_attribute13
          FROM oe_order_lines_all 
          WHERE line_id = c1_rec.line_id ;
          l_err_callpoint := 'STAGE13'; 
        EXCEPTION
        WHEN OTHERS THEN
          L_MSG := SUBSTR('QUOTE LINE INFORMATION NOT FOUND' || SQLCODE || SQLERRM,1,240) ;
          l_err_callpoint := 'STAGE14';
        END ;  
        
         /*TMS 20140725-00077 Select Rounding Factor from PDH , select item type */
         
           BEGIN
           select item_type 
           into l_item_type
           from mtl_system_items_b
           where inventory_item_id = l_inventory_item_id
           AND organization_id = 222 ;
           l_err_callpoint := 'STAGE15';
               EXCEPTION
                 WHEN OTHERS THEN
                     l_item_type := 'STOCK ITEM' ;
                     l_err_callpoint := 'STAGE16';
                     END ; 
            
         /*TMS 20140725-00077 Select Rounding Factor from PDH , select rounding factor */   
           IF l_item_type in ('SPECIAL','REPAIR','K')
                      THEN l_rounding_factor := 5 ;
                      l_err_callpoint := 'STAGE17'; 
                      ELSE
                          BEGIN 
                           select N_EXT_ATTR1 
                           into l_rounding_factor
                           from EGO_MTL_SY_ITEMS_EXT_B 
                           where N_EXT_ATTR1 is not null 
                           and attr_group_id = 861
                           and inventory_item_id = l_inventory_item_id
                          AND organization_id = 222 ;
                          l_err_callpoint := 'STAGE18';
                          EXCEPTION
                                 WHEN OTHERS THEN
                                     l_rounding_factor := 2 ;
                                     l_err_callpoint := 'STAGE19';
                                     END ; 
                      END IF ; 

 


        --Set Item and Description Variables
          IF l_atrribute5 is not null
          THEN 
            l_agreement_type := 'VQN' ; 
            l_vendor_number := i_vendor_number ;
            l_vq_number := i_vq_number ;
            l_err_callpoint := 'STAGE20';
          ELSE 
            l_agreement_type := 'CSP' ; 
            l_vendor_number := NULL ; 
            l_vq_number := NULL ; 
            l_err_callpoint := 'STAGE21';
          END IF;
                   
         BEGIN
            SELECT segment1, description
              INTO l_prod_att, l_desc
              FROM mtl_system_items
             WHERE inventory_item_id = l_inventory_item_id
             AND organization_id = 222; 
             l_err_callpoint := 'STAGE22';
         EXCEPTION
            WHEN OTHERS
            THEN
                         L_MSG := SUBSTR('MISSING ITEM' || SQLCODE || SQLERRM,1,240) ;
                         l_err_callpoint := 'STAGE23';

         END;




         /*Calculate Modifier Value based on Order_Line.ATTRIBUTE13 Selection*/
         /*TMS 20140725-00077 , add rounding factor variable to determine rounidng factor */
         l_err_callpoint := 'STAGE24';
            IF l_attribute13 = 'New Price'
            THEN
               l_modifier_value :=  ROUND(l_unit_selling_price,l_rounding_factor);
            ELSIF l_attribute13 = 'Percent'
            THEN
               l_modifier_value :=
                  ROUND(((l_unit_list_price - l_unit_selling_price) / l_unit_list_price)*100,l_rounding_factor);
            ELSIF l_attribute13 = 'Amount'
            THEN
               l_modifier_value := ROUND(l_unit_list_price - l_unit_selling_price,l_rounding_factor);
            ELSIF l_attribute13 = 'Cost Plus'
            THEN
               l_modifier_value := ROUND 
                       (( l_unit_selling_price - l_unit_cost)
                     / l_unit_selling_price
                     * 100
                   , l_rounding_factor);
            ELSE l_modifier_value :=  ROUND(l_unit_selling_price,l_rounding_factor);
            END IF;
            
            IF l_attribute13 is null
            THEN l_attribute13 := 'New Price' ; 
            END IF ; 
            l_err_callpoint := 'STAGE25';
        /*Calculate Gross Margin for CSP Lines, IF ATRIBUTER5 IS NULL*/
            IF     l_unit_selling_price IS NOT NULL
               AND l_unit_cost  IS NOT NULL
               AND l_atrribute5  IS NULL --Attribute5 Represents Special Cost
            THEN
               l_gross_margin :=
                  ROUND (
                       (l_unit_selling_price - l_unit_cost)
                     / l_unit_selling_price
                     * 100
                   , 2);
                   l_err_callpoint := 'STAGE26';
            END IF;

         /*Calculate Gross Margin for VQN Lines, IF ATRIBUTER5 IS NOT NULL*/
            IF     l_unit_selling_price IS NOT NULL
               AND l_unit_cost IS NOT NULL
               AND l_atrribute5  IS NOT NULL --Attribute5 Represents Special Cost
            THEN
               l_gross_margin :=
                  ROUND (
                       (l_unit_selling_price - l_atrribute5 )
                     / l_unit_selling_price
                     * 100
                   , 2);
                   l_err_callpoint := 'STAGE27';
            END IF; 


           INSERT INTO xxwc_om_contract_pricing_lines (AGREEMENT_LINE_ID
                                                      , AGREEMENT_ID
                                                      , AGREEMENT_TYPE
                                                      , VENDOR_QUOTE_NUMBER
                                                      , PRODUCT_ATTRIBUTE
                                                      , PRODUCT_VALUE
                                                      , PRODUCT_DESCRIPTION
                                                      , LIST_PRICE
                                                      , MODIFIER_TYPE
                                                      , MODIFIER_VALUE
                                                      , SPECIAL_COST
                                                      , VENDOR_ID
                                                      , AVERAGE_COST
                                                      , GROSS_MARGIN
                                                      , LINE_STATUS
                                                      , REVISION_NUMBER
                                                      , CREATION_DATE
                                                      , CREATED_BY
                                                      , LAST_UPDATE_DATE
                                                      , LAST_UPDATED_BY
                                                      , LAST_UPDATE_LOGIN
                                                      , INTERFACED_FLAG
                                                      , SELLING_PRICE
                                                      , LATEST_REC_FLAG
                                                      , START_DATE)
                 VALUES (XXWC_OM_CONTRACT_PRICING_LIN_S.NEXTVAL
                       , l_agreement_id
                       , l_agreement_type
                       , l_vq_number
                       , 'ITEM'
                       , l_prod_att
                       , l_desc
                       , l_unit_list_price 
                       , l_attribute13
                       , l_modifier_value /*tms: 20140206-00080*/ 
                       , l_atrribute5 
                       , l_vendor_number 
                       , ROUND(l_unit_cost,5) 
                       , l_gross_margin
                       , 'DRAFT'
                       , 0
                       , l_date
                       , l_user_id
                       , l_date
                       , l_user_id
                       , 0
                       , 'N'
                       , l_unit_selling_price
                       , 'Y'
                       , greatest(l_start_date,SYSDATE)
                       );
l_err_callpoint := 'STAGE28';

      END LOOP;
      
    COMMIT;
    
    /*If an existing CSP Agreement is found, only the lines are updated*/
      ELSIF l_cnt_agreement_id = 1 
      THEN  
        IF i_price_type = 'MASTER'
         THEN 
          BEGIN
           SELECT agreement_id
           INTO l_agreement_id
           FROM XXWC.xxwc_om_contract_pricing_hdr
           WHERE customer_id = l_customer_id 
           AND price_type = 'MASTER' ;
           l_err_callpoint := 'STAGE29';
         EXCEPTION
            WHEN OTHERS THEN
              L_MSG := SUBSTR('NO AGREEMENT FOR CUSTOMER' || SQLCODE || SQLERRM,1,240) ;
              l_err_callpoint := 'STAGE30';
           END;           
           
         ELSIF i_price_type = 'SHIPTO'
           THEN 
            BEGIN
             SELECT agreement_id
             INTO l_agreement_id
             FROM XXWC.xxwc_om_contract_pricing_hdr
             WHERE customer_id = l_customer_id
             AND customer_site_id = l_site_id 
             AND price_type = 'SHIPTO' ; 
             l_err_callpoint := 'STAGE31';
           EXCEPTION
            WHEN OTHERS THEN
              L_MSG := SUBSTR('NO AGREEMENT FOR CUSTOMER' || SQLCODE || SQLERRM,1,240) ;
              l_err_callpoint := 'STAGE32';
           END;   
         ELSE l_agreement_id := null ;
 
         END IF ; 
         
         FOR C1_REC in QUOTELINES
         LOOP
         
        
          SELECT
          inventory_item_id,
          unit_list_price,
          unit_selling_price,
          unit_cost,
          attribute5, --Special Cost
          attribute13 --Modifier Type*/
          into 
          l_inventory_item_id,
          l_unit_list_price,
          l_unit_selling_price,
          l_unit_cost,
          l_atrribute5, --Special Cost
          l_attribute13
          from oe_order_lines_all 
          where line_id = c1_rec.line_id ; 
          l_err_callpoint := 'STAGE33';
           /*TMS 20140725-00077 Select Rounding Factor from PDH , select item type */
         
           BEGIN
           select item_type 
           into l_item_type
           from mtl_system_items_b
           where inventory_item_id = l_inventory_item_id
           AND organization_id = 222 ;
           l_err_callpoint := 'STAGE34';
               EXCEPTION
                 WHEN OTHERS THEN
                     l_item_type := 'STOCK ITEM' ;
                     l_err_callpoint := 'STAGE35';
                     END ; 
            
         /*TMS 20140725-00077 Select Rounding Factor from PDH , select rounding factor */   
           IF l_item_type in ('SPECIAL','REPAIR','K')
                      THEN l_rounding_factor := 5 ; 
                      ELSE
                          BEGIN 
                           select N_EXT_ATTR1 
                           into l_rounding_factor
                           from EGO_MTL_SY_ITEMS_EXT_B 
                           where N_EXT_ATTR1 is not null 
                           and attr_group_id = 861
                           and inventory_item_id = l_inventory_item_id
                          AND organization_id = 222 ;
                          l_err_callpoint := 'STAGE36';
                          EXCEPTION
                                 WHEN OTHERS THEN
                                     l_rounding_factor := 2 ;
                                     l_err_callpoint := 'STAGE37';
                                     END ; 
                      END IF ; 

          
      --Set Item and Description Variables
          IF l_atrribute5 is not null
          THEN 
            l_agreement_type := 'VQN' ; 
            l_vendor_number := i_vendor_number ;
            l_vq_number := i_vq_number ; 
          ELSE 
            l_agreement_type := 'CSP' ; 
            l_vendor_number := NULL ;  
            l_vq_number := NULL ; 
          END IF;      
          
           BEGIN
            SELECT segment1, description
              INTO l_prod_att, l_desc
              FROM mtl_system_items
             WHERE inventory_item_id = l_inventory_item_id
             AND organization_id = 222; 
             l_err_callpoint := 'STAGE38';
         EXCEPTION
            WHEN OTHERS
            THEN
               l_msg := l_msg || ':' || 'Missing Item';
               l_err_callpoint := 'STAGE39';
         END;
          
        --determine if an Active Agreement Line exists for the SKU  
          BEGIN
            select count(agreement_line_id)
            into l_cnt_agree_id
            from XXWC.xxwc_om_contract_pricing_lines
            where product_value = l_prod_att
            and latest_rec_flag = 'Y' 
            and agreement_id = l_agreement_id ; 
            l_err_callpoint := 'STAGE40';
          EXCEPTION
            WHEN OTHERS
            THEN 
            L_MSG := SUBSTR('AGREEMENT DOES NOT EXIST' || SQLCODE || SQLERRM,1,240) ;
            l_err_callpoint := 'STAGE41';
            END ;
     
     /*If not agreement lines exist for the item then a new records will be created (l_cnt_agree_id = 0) */
     /*TMS 20140725-00077 , add rounding factor variable to determine rounidng factor */
     IF l_cnt_agree_id = 0
     THEN
     l_err_callpoint := 'STAGE42';
      IF l_attribute13 = 'New Price'
            THEN
               l_modifier_value :=  ROUND(l_unit_selling_price,l_rounding_factor);
            ELSIF l_attribute13 = 'Percent'
            THEN
               l_modifier_value :=
                  ROUND(((l_unit_list_price - l_unit_selling_price) / l_unit_list_price)*100,l_rounding_factor);
            ELSIF l_attribute13 = 'Amount'
            THEN
               l_modifier_value := ROUND(l_unit_list_price - l_unit_selling_price,l_rounding_factor);
            ELSIF l_attribute13 = 'Cost Plus'
            THEN
               l_modifier_value := ROUND 
                       (( l_unit_selling_price - l_unit_cost)
                     / l_unit_selling_price
                     * 100
                   , l_rounding_factor);
            ELSE l_modifier_value :=  ROUND(l_unit_selling_price,l_rounding_factor);
            END IF;
            
            IF l_attribute13 is null
            THEN l_attribute13 := 'New Price' ; 
            END IF ; 
            
            l_err_callpoint := 'STAGE43';
            
--Calculate Gross Margin for CSP Lines, IF ATRIBUTER5 IS NULL
            IF     l_unit_selling_price IS NOT NULL
               AND l_unit_cost  IS NOT NULL
               AND l_atrribute5  IS NULL --Attribute5 Represents Special Cost
            THEN
               l_gross_margin :=
                  ROUND (
                       (l_unit_selling_price - l_unit_cost)
                     / l_unit_selling_price
                     * 100
                   , 2);
                   l_err_callpoint := 'STAGE44';
            END IF;

--Calculate Gross Margin for VQN Lines, IF ATRIBUTER5 IS NOT NULL
            IF     l_unit_selling_price IS NOT NULL
               AND l_unit_cost IS NOT NULL
               AND l_atrribute5  IS NOT NULL --Attribute5 Represents Special Cost
            THEN
               l_gross_margin :=
                  ROUND (
                       (l_unit_selling_price - l_atrribute5 )
                     / l_unit_selling_price
                     * 100
                   , 2);
                   l_err_callpoint := 'STAGE45';
            END IF; 


           INSERT INTO xxwc_om_contract_pricing_lines (AGREEMENT_LINE_ID
                                                      , AGREEMENT_ID
                                                      , AGREEMENT_TYPE
                                                      , VENDOR_QUOTE_NUMBER
                                                      , PRODUCT_ATTRIBUTE
                                                      , PRODUCT_VALUE
                                                      , PRODUCT_DESCRIPTION
                                                      , LIST_PRICE
                                                      , MODIFIER_TYPE
                                                      , MODIFIER_VALUE
                                                      , SPECIAL_COST
                                                      , VENDOR_ID
                                                      , AVERAGE_COST
                                                      , GROSS_MARGIN
                                                      , LINE_STATUS
                                                      , REVISION_NUMBER
                                                      , CREATION_DATE
                                                      , CREATED_BY
                                                      , LAST_UPDATE_DATE
                                                      , LAST_UPDATED_BY
                                                      , LAST_UPDATE_LOGIN
                                                      , INTERFACED_FLAG
                                                      , SELLING_PRICE
                                                      , LATEST_REC_FLAG
                                                      , START_DATE)
                 VALUES (XXWC_OM_CONTRACT_PRICING_LIN_S.NEXTVAL
                       , l_agreement_id
                       , l_agreement_type
                       , l_vq_number
                       , 'ITEM'
                       , l_prod_att
                       , l_desc
                       , l_unit_list_price 
                       , l_attribute13
                       , l_modifier_value /*TMS 20140725-00077*/
                       , l_atrribute5 
                       , l_vendor_number 
                       , ROUND(l_unit_cost,5) 
                       , l_gross_margin
                       , 'DRAFT'
                       , 0
                       , l_date
                       , l_user_id
                       , l_date
                       , l_user_id
                       , 0
                       , 'N'
                       , l_unit_selling_price
                       , 'Y'
                       , greatest(l_start_date,SYSDATE)
                       );
                       l_err_callpoint := 'STAGE46';
      /*CSP Line already exists*/  
         ELSIF l_cnt_agree_id = 1
         THEN 
         
         
            BEGIN
              select agreement_line_id
              into l_agreement_line_id
              from XXWC.xxwc_om_contract_pricing_lines
              where product_value = l_prod_att
              and latest_rec_flag = 'Y' 
              and agreement_id = l_agreement_id; 
              l_err_callpoint := 'STAGE47';
          EXCEPTION
            WHEN OTHERS
            THEN 
            l_msg := l_msg || ':' || 'Missing Agreement Line ID';
            l_err_callpoint := 'STAGE48';
            END ;
            
            --Added 4/1/13 Toby Rave for same day agreement issues
            BEGIN 
            SELECT interfaced_flag, start_date
            into l_int_flag, l_start_date2
            from xxwc.xxwc_om_contract_pricing_lines
            where agreement_line_id = l_agreement_line_id ; 
            l_err_callpoint := 'STAGE49';
            EXCEPTION
            WHEN OTHERS
            THEN 
            l_msg := l_agreement_line_id || ':' || 'Duplicate row exists for item_id';
            l_err_callpoint := 'STAGE50';
            END;
            
                If l_int_flag = 'Y' and trunc(l_start_date2) < trunc(SYSDATE)
                THEN
                
                UPDATE XXWC.xxwc_om_contract_pricing_lines
                SET END_DATE = SYSDATE-1, 
                interfaced_flag = 'N', 
                latest_rec_flag = 'N',
                line_status = 'DRAFT'
                WHERE agreement_line_id = l_agreement_line_id ; 
                l_err_callpoint := 'STAGE51';
                
                IF l_attribute13 = 'New Price'
                THEN
                   l_modifier_value :=  ROUND(l_unit_selling_price,5);
                ELSIF l_attribute13 = 'Percent'
                THEN
                   l_modifier_value :=
                      ROUND(((l_unit_list_price - l_unit_selling_price) / l_unit_list_price)*100,5);
                ELSIF l_attribute13 = 'Amount'
                THEN
                   l_modifier_value := ROUND(l_unit_list_price - l_unit_selling_price,5);
                ELSIF l_attribute13 = 'Cost Plus'
                THEN
                   l_modifier_value := ROUND 
                           (( l_unit_selling_price - l_unit_cost)
                         / l_unit_selling_price
                         * 100
                       , 5);
                ELSE l_modifier_value :=  ROUND(l_unit_selling_price,5);
                END IF;
                
                IF l_attribute13 is null
                THEN l_attribute13 := 'New Price' ; 
                END IF ; 
                l_err_callpoint := 'STAGE52';
    --Calculate Gross Margin for CSP Lines, IF ATRIBUTER5 IS NULL
                IF     l_unit_selling_price IS NOT NULL
                   AND l_unit_cost  IS NOT NULL
                   AND l_atrribute5  IS NULL --Attribute5 Represents Special Cost
                THEN
                   l_gross_margin :=
                      ROUND (
                           (l_unit_selling_price - l_unit_cost)
                         / l_unit_selling_price
                         * 100
                       , 2);
                       l_err_callpoint := 'STAGE53';
                END IF;
    
    --Calculate Gross Margin for VQN Lines, IF ATRIBUTER5 IS NOT NULL
                IF     l_unit_selling_price IS NOT NULL
                   AND l_unit_cost IS NOT NULL
                   AND l_atrribute5  IS NOT NULL --Attribute5 Represents Special Cost
                THEN
                   l_gross_margin :=
                      ROUND (
                           (l_unit_selling_price - l_atrribute5 )
                         / l_unit_selling_price
                         * 100
                       , 2);
                       l_err_callpoint := 'STAGE54';
                END IF; 
    
    
               INSERT INTO xxwc_om_contract_pricing_lines (AGREEMENT_LINE_ID
                                                          , AGREEMENT_ID
                                                          , AGREEMENT_TYPE
                                                          , VENDOR_QUOTE_NUMBER
                                                          , PRODUCT_ATTRIBUTE
                                                          , PRODUCT_VALUE
                                                          , PRODUCT_DESCRIPTION
                                                          , LIST_PRICE
                                                          , START_DATE
                                                          , MODIFIER_TYPE
                                                          , MODIFIER_VALUE
                                                          , SPECIAL_COST
                                                          , VENDOR_ID
                                                          , AVERAGE_COST
                                                          , GROSS_MARGIN
                                                          , LINE_STATUS
                                                          , REVISION_NUMBER
                                                          , CREATION_DATE
                                                          , CREATED_BY
                                                          , LAST_UPDATE_DATE
                                                          , LAST_UPDATED_BY
                                                          , LAST_UPDATE_LOGIN
                                                          , INTERFACED_FLAG
                                                          , SELLING_PRICE
                                                          , LATEST_REC_FLAG)
                     VALUES (XXWC_OM_CONTRACT_PRICING_LIN_S.NEXTVAL
                           , l_agreement_id
                           , l_agreement_type
                           , l_vq_number
                           , 'ITEM'
                           , l_prod_att
                           , l_desc
                           , l_unit_list_price 
                           , greatest(l_start_date,SYSDATE)
                           , l_attribute13
                           , ROUND(l_modifier_value,2) /*tms: 20140206-00080*/
                           , l_atrribute5 
                           , l_vendor_number 
                           , ROUND(l_unit_cost,5) 
                           , l_gross_margin
                           , 'DRAFT'
                           , 0
                           , l_date
                           , l_user_id
                           , l_date
                           , l_user_id
                           , 0
                           , 'N'
                           , l_unit_selling_price
                           , 'Y'
                           ); 
                           l_err_callpoint := 'STAGE56';
             
             ELSE L_INT_FLAG := NULL ; 
             END IF ; 
         
         ELSE l_cnt_agree_id  := NULL ;
         END IF ; 
         END LOOP ;  
      COMMIT ;  
         
    END IF ;
  EXCEPTION
  WHEN NO_DATA_FOUND
   THEN
      NULL;
   WHEN OTHERS
   THEN
      NULL;
      xxcus_error_pkg.xxcus_error_main_api (
         p_called_from         => l_err_callfrom,
         p_calling             => l_err_callpoint,
         p_ora_error_msg       => SQLERRM,
         p_error_desc          => l_message,
         p_distribution_list   => l_distro_list,
         p_module              => 'XXWC');
   
  END csp_quote;
  
/*************************************************************************
*   PROCEDURE Name: csp_conversion
*
*   PURPOSE:   csp_quote conversion procedure is used for loading converted 
*              records from the xxwc.xxwc_csp_interface tables into the 
*              CSP Maintenance tables (xxwc.xxwc_om_contractpricing_hdr, 
*              xxwc_xxwc.om_contract_pricing_lines)
****************************************************************************/ 
PROCEDURE csp_conversion
  (
    errbuf OUT VARCHAR2,
    retcode OUT VARCHAR2
  )
IS
  --DECLARE
  l_customer_id NUMBER;
  l_site_id     NUMBER;
  l_org_id      NUMBER;
  l_msg         VARCHAR2 (240); --VARCHAR2 (32000);
  l_conv_flag   VARCHAR2 (1) := 'Y';
  l_date DATE                := SYSDATE;
  l_user_id       NUMBER           := NVL (fnd_profile.VALUE ('USER_ID'), -1);
  l_prod_att      VARCHAR2 (100);
  l_vendor_id     NUMBER;
  l_desc          VARCHAR2 (240);
  l_list_price    NUMBER;
  l_average_cost  NUMBER;
  l_selling_price NUMBER;
  l_gross_margin  NUMBER;
  i               NUMBER := 0;
  CURSOR c1
  IS
    SELECT a.ROWID,
      a.*
    FROM XXWC_CSP_HDR_INTERFACE a
    WHERE NVL (process_flag, 'N') IN ('N', 'E');
  CURSOR c2
  IS
    SELECT a.ROWID ,
      a.* ,
      b.agreement_id ,
      b.organization_id
    FROM XXWC_CSP_LINES_INTERFACE a,
      xxwc_om_contract_pricing_hdr b
    WHERE NVL (process_flag, 'N') <> 'Y'
    AND a.reference_id             = b.attribute10;
BEGIN
  -- set the status to not processed for duplicate reference
  UPDATE XXWC_CSP_HDR_INTERFACE
  SET process_flag   = 'N'
  WHERE process_flag = 'D';
  UPDATE XXWC_CSP_HDR_INTERFACE
  SET process_flag    = 'D'
  WHERE process_flag IN ('N', 'E')
  AND reference_id   IN
    (SELECT reference_id
    FROM XXWC_CSP_HDR_INTERFACE
    GROUP BY reference_id
    HAVING COUNT (1) > 1
    );
  FOR c1_rec IN c1
  LOOP
    l_customer_id := NULL;
    l_site_id     := NULL;
    l_org_id      := NULL;
    l_conv_flag   := 'Y';
    l_msg         := NULL;
    -- get customer id
    BEGIN
      SELECT cust_account_id
      INTO l_customer_id
      FROM hz_cust_accounts
      WHERE account_number = c1_rec.customer_number;
    EXCEPTION
    WHEN OTHERS THEN
      l_msg       := 'Missing Customer';
      l_conv_flag := 'E';
    END;
    IF c1_rec.price_type = 'SHIPTO' THEN
      BEGIN
        SELECT site_use_id
        INTO l_site_id
        FROM hz_cust_site_uses_all a,
          hz_cust_acct_sites_all b
        WHERE b.cust_account_id = l_customer_id
        AND b.cust_acct_site_id = a.cust_acct_site_id
        AND a.location          = c1_rec.job_location
        AND a.site_use_code     = 'SHIP_TO';
      EXCEPTION
      WHEN OTHERS THEN
        l_msg       := l_msg || ':' || 'Missing Site';
        l_conv_flag := 'E';
      END;
    END IF;
    BEGIN
      SELECT organization_id
      INTO l_org_id
      FROM org_organization_definitions
      WHERE organization_code = c1_rec.branch;
    EXCEPTION
    WHEN OTHERS THEN
      l_msg       := l_msg || ':''Missing Branch';
      l_conv_flag := 'E';
    END;
    IF l_conv_flag = 'Y' THEN
      INSERT
      INTO xxwc_om_contract_pricing_hdr
        (
          agreement_id ,
          price_type ,
          customer_id ,
          customer_site_id ,
          agreement_status ,
          revision_number ,
          organization_id ,
          gross_margin ,
          incompatability_group ,
          attribute10 ,
          attribute1 ,
          creation_date ,
          created_by ,
          last_update_date ,
          last_updated_by ,
          last_update_login
        )
        VALUES
        (
          XXWC_OM_CONTRACT_PRICING_HDR_S.NEXTVAL ,
          c1_rec.price_type ,
          l_customer_id ,
          l_site_id ,
          'AWAITING_APPROVAL' ,
          0 ,
          l_org_id ,
          0 ,
          DECODE (SUBSTR (c1_rec.incompatability, 1, 1) , 'E', 'Exclusive' , 'Best Price Wins') ,
          c1_rec.reference_id ,
          'YES' ,
          l_date ,
          l_user_id ,
          l_date ,
          l_user_id ,
          0
        );
      UPDATE XXWC_CSP_HDR_INTERFACE
      SET process_flag = 'Y'
      WHERE ROWID      = c1_rec.ROWID;
      l_msg           := NULL;
    ELSE
      UPDATE XXWC_CSP_HDR_INTERFACE
      SET process_flag = 'E',
        error_message  = l_msg
      WHERE ROWID      = c1_rec.ROWID;
      l_msg           := NULL;
    END IF;
  END LOOP;
  COMMIT;
  FOR c2_rec IN c2
  LOOP
    l_vendor_id                := NULL;
    l_prod_att                 := NULL;
    l_desc                     := NULL;
    l_list_price               := NULL;
    l_average_cost             := NULL;
    l_selling_price            := NULL;
    l_gross_margin             := NULL;
    l_msg                      := NULL;
    l_conv_flag                := 'Y';
    i                          := i + 1;
    IF c2_rec.product_attribute = 'ITEM' --Toby Rave
      THEN
      BEGIN
        SELECT segment1,
          description
        INTO l_prod_att,
          l_desc
        FROM mtl_system_items
        WHERE segment1      = c2_rec.product_value
        AND organization_id = 222; --c2_rec.organization_id;
      EXCEPTION
      WHEN OTHERS THEN
        l_msg       := l_msg || ':' || 'Missing Item';
        l_conv_flag := 'E';
      END;
      IF c2_rec.agreement_type = 'VQN' THEN
        BEGIN
          SELECT vendor_id
          INTO l_vendor_id
          FROM ap_suppliers
          WHERE segment1 = c2_rec.vendor_number;
        EXCEPTION
        WHEN OTHERS THEN
          l_msg       := l_msg || ':' || 'Missing Vendor';
          l_conv_flag := 'E';
        END;
      END IF;
      IF c2_rec.product_attribute = 'ITEM' THEN
        BEGIN
          SELECT ll.operand
          INTO l_list_price
          FROM qp_secu_list_headers_v lh ,
            xxwc.xxwc_qp_list_lines_mv ll --, qp_list_lines_v ll
            ,
            mtl_system_items a
          WHERE lh.list_type_code = 'PRL'
          AND lh.name LIKE ('CATEGORY %')
          AND lh.name          <> 'CATEGORY 7 - OTHER ITEMS'
          AND lh.list_header_id = ll.list_header_id
          AND SYSDATE BETWEEN NVL (ll.start_date_active , SYSDATE - 1) AND NVL (ll.end_date_active , SYSDATE + 1)
          AND ll.product_id     = a.inventory_item_id
          AND a.organization_id = 222 --c2_rec.organization_id
          AND a.segment1        = c2_rec.PRODUCT_VALUE
          AND ROWNUM            = 1;
        EXCEPTION
        WHEN OTHERS THEN
          l_msg       := l_msg || ':' || 'Missing List Price';
          l_conv_flag := 'E';
        END;
        BEGIN
          SELECT ROUND ( cst_cost_api.get_item_cost (1 , inventory_item_id , c2_rec.organization_id) , 5)
          INTO l_average_cost
          FROM mtl_system_items
          WHERE segment1      = c2_rec.PRODUCT_VALUE
          AND organization_id = 222; --c2_rec.organization_id;
        EXCEPTION
        WHEN OTHERS THEN
          l_msg       := l_msg || ':' || 'Missing Avg Cost';
          l_conv_flag := 'E';
        END;
        IF c2_rec.app_method    = 'New Price' THEN
          l_selling_price      := c2_rec.modifier_value;
        ELSIF c2_rec.app_method = 'Percent' THEN
          l_selling_price      := l_list_price * (100 - c2_rec.modifier_value) / 100;
        ELSIF c2_rec.app_method = 'Amount' THEN
          l_selling_price      := l_list_price - c2_rec.modifier_value;
        ELSIF c2_rec.app_method = 'Cost Plus' THEN
          l_selling_price      := l_average_cost + c2_rec.modifier_value;
        END IF;
        IF l_selling_price IS NOT NULL AND l_average_cost IS NOT NULL AND c2_rec.agreement_type = 'CSP' THEN
          l_gross_margin   := ROUND ( (l_selling_price - l_average_cost) / l_selling_price * 100 , 2);
        END IF;
        IF l_selling_price IS NOT NULL AND c2_rec.special_cost IS NOT NULL AND c2_rec.agreement_type = 'VQN' THEN
          l_gross_margin   := ROUND ( (l_selling_price - c2_rec.special_cost) / l_selling_price * 100 , 2);
        END IF;
      END IF;
      IF l_conv_flag = 'Y' THEN
        INSERT
        INTO xxwc_om_contract_pricing_lines
          (
            AGREEMENT_LINE_ID ,
            AGREEMENT_ID ,
            AGREEMENT_TYPE ,
            VENDOR_QUOTE_NUMBER ,
            PRODUCT_ATTRIBUTE ,
            PRODUCT_VALUE ,
            PRODUCT_DESCRIPTION ,
            LIST_PRICE ,
            START_DATE ,
            END_DATE ,
            MODIFIER_TYPE ,
            MODIFIER_VALUE ,
            SPECIAL_COST ,
            VENDOR_ID ,
            AVERAGE_COST ,
            GROSS_MARGIN ,
            LINE_STATUS ,
            REVISION_NUMBER ,
            ATTRIBUTE10 ,
            ATTRIBUTE1 ,
            CREATION_DATE ,
            CREATED_BY ,
            LAST_UPDATE_DATE ,
            LAST_UPDATED_BY ,
            LAST_UPDATE_LOGIN ,
            INTERFACED_FLAG ,
            SELLING_PRICE ,
            LATEST_REC_FLAG
          )
          VALUES
          (
            XXWC_OM_CONTRACT_PRICING_LIN_S.NEXTVAL ,
            c2_rec.agreement_id ,
            c2_rec.agreement_type ,
            c2_rec.vq_number ,
            c2_rec.product_attribute ,
            c2_rec.product_value ,
            l_desc ,
            l_list_price ,
            c2_rec.start_date ,
            c2_rec.end_date ,
            c2_rec.app_method ,
            c2_rec.modifier_value ,
            c2_rec.special_cost ,
            l_vendor_id ,
            l_average_cost ,
            l_gross_margin ,
            'AWAITING_APPROVAL' ,
            0 ,
            c2_rec.reference_id ,
            'YES' ,
            l_date ,
            l_user_id ,
            l_date ,
            l_user_id ,
            0 ,
            'N' ,
            l_selling_price ,
            'Y'
          );
        UPDATE XXWC_CSP_LINES_INTERFACE
        SET process_flag = 'Y'
        WHERE ROWID      = c2_rec.ROWID;
        IF i            >= 1000 THEN
          COMMIT;
          i :=0;
        END IF;
      ELSE
        UPDATE XXWC_CSP_LINES_INTERFACE
        SET process_flag = 'E',
          error_message  = l_msg
        WHERE ROWID      = c2_rec.ROWID;
      END IF;
      --TMS Task ID 20130605-01144; Toby Rave; Added code to convert CSP by Cat Class
    ELSIF c2_rec.product_attribute = 'CAT_CLASS' THEN
      BEGIN
        SELECT a.category_id, b.description
        INTO l_prod_att, l_desc
        FROM mtl_categories_b a,
        mtl_categories_tl b
        WHERE a.segment1
          || '.'
          || a.segment2 = c2_rec.product_value 
          and a.category_id = b.category_id ;
      EXCEPTION
      WHEN OTHERS THEN
        l_msg       := l_msg || ':' || 'Missing Category';
        l_conv_flag := 'E';
      END;
      IF c2_rec.app_method    = 'New Price' THEN
        l_selling_price      := c2_rec.modifier_value;
      ELSIF c2_rec.app_method = 'Percent' THEN
        l_selling_price      := l_list_price * (100 - c2_rec.modifier_value) / 100;
      ELSIF c2_rec.app_method = 'Amount' THEN
        l_selling_price      := l_list_price - c2_rec.modifier_value;
      ELSIF c2_rec.app_method = 'Cost Plus' THEN
        l_selling_price      := l_average_cost + c2_rec.modifier_value;
      END IF;
      IF l_conv_flag = 'Y' THEN
        INSERT
        INTO xxwc_om_contract_pricing_lines
          (
            AGREEMENT_LINE_ID ,
            AGREEMENT_ID ,
            AGREEMENT_TYPE ,
            VENDOR_QUOTE_NUMBER ,
            PRODUCT_ATTRIBUTE ,
            PRODUCT_VALUE ,
            PRODUCT_DESCRIPTION ,
            LIST_PRICE ,
            START_DATE ,
            END_DATE ,
            MODIFIER_TYPE ,
            MODIFIER_VALUE ,
            SPECIAL_COST ,
            VENDOR_ID ,
            AVERAGE_COST ,
            GROSS_MARGIN ,
            LINE_STATUS ,
            REVISION_NUMBER ,
            ATTRIBUTE10 ,
            ATTRIBUTE1 ,
            CREATION_DATE ,
            CREATED_BY ,
            LAST_UPDATE_DATE ,
            LAST_UPDATED_BY ,
            LAST_UPDATE_LOGIN ,
            INTERFACED_FLAG ,
            SELLING_PRICE ,
            LATEST_REC_FLAG
          )
          VALUES
          (
            XXWC_OM_CONTRACT_PRICING_LIN_S.NEXTVAL ,
            c2_rec.agreement_id ,
            c2_rec.agreement_type ,
            c2_rec.vq_number ,
            c2_rec.product_attribute ,
            c2_rec.product_value ,
            l_desc ,
            l_list_price ,
            c2_rec.start_date ,
            c2_rec.end_date ,
            c2_rec.app_method ,
            c2_rec.modifier_value ,
            c2_rec.special_cost ,
            l_vendor_id ,
            l_average_cost ,
            l_gross_margin ,
            'AWAITING_APPROVAL' ,
            0 ,
            c2_rec.reference_id ,
            'YES' ,
            l_date ,
            l_user_id ,
            l_date ,
            l_user_id ,
            0 ,
            'N' ,
            l_selling_price ,
            'Y'
          );
        UPDATE XXWC_CSP_LINES_INTERFACE
        SET process_flag = 'Y'
        WHERE ROWID      = c2_rec.ROWID;
        IF i            >= 1000 THEN
          COMMIT;
          i :=0;
        END IF;
      ELSE
        UPDATE XXWC_CSP_LINES_INTERFACE
        SET process_flag = 'E',
          error_message  = l_msg
        WHERE ROWID      = c2_rec.ROWID;
      END IF;
      --TMS Task ID 20130605-01144; Toby Rave; Added code to convert CSP by Category
    ELSIF c2_rec.product_attribute = 'CATEGORY' THEN
      BEGIN
        SELECT DISTINCT(segment1) --, description
        INTO l_prod_att           --, l_desc
        FROM mtl_categories_b
        WHERE segment1 = c2_rec.product_value ;
      EXCEPTION
      WHEN OTHERS THEN
        l_msg       := l_msg || ':' || 'Missing Category';
        l_conv_flag := 'E';
      END;
      IF c2_rec.app_method    = 'New Price' THEN
        l_selling_price      := c2_rec.modifier_value;
      ELSIF c2_rec.app_method = 'Percent' THEN
        l_selling_price      := l_list_price * (100 - c2_rec.modifier_value) / 100;
      ELSIF c2_rec.app_method = 'Amount' THEN
        l_selling_price      := l_list_price - c2_rec.modifier_value;
      ELSIF c2_rec.app_method = 'Cost Plus' THEN
        l_selling_price      := l_average_cost + c2_rec.modifier_value;
      END IF;
      IF l_conv_flag = 'Y' THEN
        INSERT
        INTO xxwc_om_contract_pricing_lines
          (
            AGREEMENT_LINE_ID ,
            AGREEMENT_ID ,
            AGREEMENT_TYPE ,
            VENDOR_QUOTE_NUMBER ,
            PRODUCT_ATTRIBUTE ,
            PRODUCT_VALUE ,
            PRODUCT_DESCRIPTION ,
            LIST_PRICE ,
            START_DATE ,
            END_DATE ,
            MODIFIER_TYPE ,
            MODIFIER_VALUE ,
            SPECIAL_COST ,
            VENDOR_ID ,
            AVERAGE_COST ,
            GROSS_MARGIN ,
            LINE_STATUS ,
            REVISION_NUMBER ,
            ATTRIBUTE10 ,
            ATTRIBUTE1 ,
            CREATION_DATE ,
            CREATED_BY ,
            LAST_UPDATE_DATE ,
            LAST_UPDATED_BY ,
            LAST_UPDATE_LOGIN ,
            INTERFACED_FLAG ,
            SELLING_PRICE ,
            LATEST_REC_FLAG
          )
          VALUES
          (
            XXWC_OM_CONTRACT_PRICING_LIN_S.NEXTVAL ,
            c2_rec.agreement_id ,
            c2_rec.agreement_type ,
            c2_rec.vq_number ,
            c2_rec.product_attribute ,
            c2_rec.product_value ,
            l_desc ,
            l_list_price ,
            c2_rec.start_date ,
            c2_rec.end_date ,
            c2_rec.app_method ,
            c2_rec.modifier_value ,
            c2_rec.special_cost ,
            l_vendor_id ,
            l_average_cost ,
            l_gross_margin ,
            'AWAITING_APPROVAL' ,
            0 ,
            c2_rec.reference_id ,
            'YES' ,
            l_date ,
            l_user_id ,
            l_date ,
            l_user_id ,
            0 ,
            'N' ,
            l_selling_price ,
            'Y'
          );
        UPDATE XXWC_CSP_LINES_INTERFACE
        SET process_flag = 'Y'
        WHERE ROWID      = c2_rec.ROWID;
        IF i            >= 1000 THEN
          COMMIT;
          i :=0;
        END IF;
      ELSE
        UPDATE XXWC_CSP_LINES_INTERFACE
        SET process_flag = 'E',
          error_message  = l_msg
        WHERE ROWID      = c2_rec.ROWID;
      END IF;
    ELSE
      UPDATE XXWC_CSP_LINES_INTERFACE
      SET process_flag = 'E',
        error_message  = l_msg
      WHERE ROWID      = c2_rec.ROWID;
    END IF ;
  END LOOP;
  COMMIT;
  -- update gross margin
  FOR c3_rec IN
  (SELECT agreement_id
  FROM xxwc_om_contract_pricing_hdr
  WHERE NVL(gross_margin,0)=0
  )
  LOOP
    l_gross_margin := NULL;
    SELECT ROUND ( (SUM (NVL (selling_price, 1)) - SUM ( DECODE (agreement_type , 'CSP', NVL (average_cost, 0) , NVL (special_cost, 0)))) / SUM (NVL 
(selling_price, 1)) * 100 , 2)
    INTO l_gross_margin
    FROM XXWC_OM_CONTRACT_PRICING_LINES
    WHERE agreement_id             = c3_rec.agreement_id
    AND NVL (latest_rec_flag, 'Y') = 'Y'
    AND ( (agreement_type          = 'CSP'
    AND average_cost              IS NOT NULL)
    OR (agreement_type             = 'VQN'
    AND special_cost              IS NOT NULL));
    IF l_gross_margin             IS NOT NULL THEN
      UPDATE xxwc_om_contract_pricing_hdr
      SET gross_margin   = l_gross_margin
      WHERE agreement_id = c3_rec.agreement_id;
      COMMIT;
    END IF;
  END LOOP;
END csp_conversion;
   
   
   
/*************************************************************************
*   PROCEDURE Name: csp_headers
*
*   PURPOSE:   CREATE_HEADER procedure is used to create Modifier Headers 
*              in Advanced Pricing for converted CSP Agreement Headers
****************************************************************************/   
   PROCEDURE csp_headers --(errbuf OUT VARCHAR2, retcode OUT VARCHAR2)
   IS
   
l_control_rec QP_GLOBALS.Control_Rec_Type;
l_return_status VARCHAR2(1);
x_msg_count number;
x_msg_data Varchar2(2000);
x_msg_index number;

l_MODIFIER_LIST_rec QP_Modifiers_PUB.Modifier_List_Rec_Type;
l_MODIFIER_LIST_val_rec QP_Modifiers_PUB.Modifier_List_Val_Rec_Type;
l_MODIFIERS_tbl QP_Modifiers_PUB.Modifiers_Tbl_Type;
l_MODIFIERS_val_tbl QP_Modifiers_PUB.Modifiers_Val_Tbl_Type;
l_QUALIFIERS_tbl QP_Qualifier_Rules_PUB.Qualifiers_Tbl_Type;
l_QUALIFIERS_val_tbl QP_Qualifier_Rules_PUB.Qualifiers_Val_Tbl_Type;
l_PRICING_ATTR_tbl QP_Modifiers_PUB.Pricing_Attr_Tbl_Type;
l_PRICING_ATTR_val_tbl QP_Modifiers_PUB.Pricing_Attr_Val_Tbl_Type;

l_x_MODIFIER_LIST_rec QP_Modifiers_PUB.Modifier_List_Rec_Type;
l_x_MODIFIER_LIST_val_rec QP_Modifiers_PUB.Modifier_List_Val_Rec_Type;
l_x_MODIFIERS_tbl QP_Modifiers_PUB.Modifiers_Tbl_Type;
l_x_MODIFIERS_val_tbl QP_Modifiers_PUB.Modifiers_Val_Tbl_Type;
l_x_QUALIFIERS_tbl QP_Qualifier_Rules_PUB.Qualifiers_Tbl_Type;
l_x_QUALIFIERS_val_tbl QP_Qualifier_Rules_PUB.Qualifiers_Val_Tbl_Type;
l_x_PRICING_ATTR_tbl QP_Modifiers_PUB.Pricing_Attr_Tbl_Type;
l_x_PRICING_ATTR_val_tbl QP_Modifiers_PUB.Pricing_Attr_Val_Tbl_Type;

--Toby's Variables
--CSP Header Level Variables


v_agreement_id Number;
v_agreement_type VARCHAR2(30) ;
v_vendor_quote_number NUMBER ;
v_price_type VARCHAR2(30);
v_customer_id NUMBER ;
v_customer_site_id NUMBER ;
v_agreement_status VARCHAR2(30) ;
v_revision_number NUMBER ;
v_incompatibility_group VARCHAR2(30);
v_attribute_10 VARCHAR2(25) :='Contract Pricing'; --Variable to populate DFF Attribute 10 on Modifier Header (Contract Pricing or Vendor Quote)
v_attribute_11 Number := NULL ;
v_attribute_12 VARCHAR2(25) := NULL ; --Vendor Name for VQN
v_attribute_13 VARCHAR2(25) := NULL ; --VEndor Number for VQN
v_attribute_14 Number ; --Mapped to Vendor Agreement Number DFF on Modifier Header
v_attribute_15 Number ; --Mapped to Agreement Revision Number DFF on Modifier Header
v1_price_type VARCHAR2(30);
v_customer_name VARCHAR2(50) ;
v_party_id Number ;
v_party_name VARCHAR2(360) ;
v_party_site_number Number ;
v_qualifier_value Number ;
v_agreeid Number ;
v_qual_context VARCHAR2(360);
v_excluder_flag VARCHAR2(1) ;
v_operator_code VARCHAR(360) ;
v_delimitier VARCHAR2(1) ;
v_qualifier_grouping_no Number ;
v_qualifier_precedence Number ; 
v_operation VARCHAR2(30) ; 
v_active_flag VARCHAR2(30) ;
v_list_header_id NUMBER ; --v_list_header_id
v_list_line_id NUMBER ; 
--START XXWC_CSP_LINES VARIABLES
v_atrribute_3 NUMBER ;
v_ln_special_cost NUMBER ;
v_ln_id NUMBER ; --Line Level Variable for xxwc.agreement_line_id
v_ln_agreement_type VARCHAR2(30) ; --Line Level Variable for xxwc.agreement_type
v_ln_vendor_quote_number VARCHAR2(30) ;  --Line Level Variable for XXWC.VENDOR_QUOTE_NUMBER
v_ln_start_date DATE ; --Line Level Variable for XXWC.START_DATE
v_ln_end_date DATE;  --Line Level Variable for XXWC.END_DATE
v_ln_mod_type  VARCHAR2(30) ; --Line Level Variable for Modifier Type
v_arithmetic_operator VARCHAR2(30) ; --Line Level Variable for Modifier Type API Value
v_ln_mod_value NUMBER ; --Line Level Variable for Modifier Value
v_ln_vendor_id NUMBER ;--Line LEvel Variable for Vendor ID
v_product_attribute VARCHAR2(50) ;  --Line Level Variable for Product Attribute (Item/Cat Class)
v_product_value VARCHAR2(50) ;--Line Level Variable for Product Value (SKU Number)
v_product_attribute_api VARCHAR2(50) ; --Line Level Variable to set in API
v_product_attribute_context VARCHAR2(30) := 'ITEM' ; --Variable to set product_attribute_context in Pricing Attr API
v_product_attr_value VARCHAR2(50) ;
v_generate_using_formula_id NUMBER ;
V9 NUMBER ; 
i NUMBER ;
j NUMBER ;
CURSOR AGREEID 
IS select agreement_id
from xxwc.xxwc_om_contract_pricing_hdr
WHERE attribute1 = 'YES' ;
CURSOR LINEID --(p_cv1 in number)
IS select agreement_line_id 
from xxwc.xxwc_om_contract_pricing_lines 
where attribute1 = 'YES' ;
L_MSG VARCHAR2(240) ; 
--where agreement_id = p_cv1 ;


Begin
--Set Header Level Variables
  for c1_rec in agreeid
    loop

      BEGIN
        select 
        agreement_id, 
        price_type, 
        customer_id,
        customer_site_id, 
        agreement_status, 
        revision_number, 
        incompatability_group
        into 
        v_attribute_14,
        v_price_type, 
        v_customer_id,
        v_customer_site_id, 
        v_agreement_status, 
        v_attribute_15,
        v_incompatibility_group
        from xxwc.xxwc_om_contract_pricing_hdr
        where agreement_id = c1_rec.agreement_id ;
        EXCEPTION
        WHEN OTHERS THEN
                FND_FILE.Put_Line(FND_FILE.Log,'agreement_id = '||v_attribute_14); 
                END ;


         --Select PARTY_ID from Customer Master
            IF v_price_type in ('SHIPTO','MASTER')
            THEN
              BEGIN
               SELECT party_id
                 INTO v_party_id
                 FROM hz_cust_accounts
                WHERE cust_account_id = v_customer_id;
                              EXCEPTION
                WHEN OTHERS THEN 
                  L_MSG := SUBSTR('Customer Party ID not found' || SQLCODE || SQLERRM,1,240) ;
           END; 
          /*Select PARTY_NAME from Customer Master*/
              BEGIN
                SELECT party_name
                 INTO v_party_name
                 FROM hz_parties
                WHERE party_id = v_party_id;
              EXCEPTION
                WHEN OTHERS THEN 
                  L_MSG := SUBSTR('Customer Party Name not found' || SQLCODE || SQLERRM,1,240) ;
          END; 
            ELSE
               v_party_id := NULL;
               BEGIN
                SELECT description
                 INTO v_party_name
                 FROM fnd_lookup_values
                WHERE lookup_type = 'NATIONAL_ACCOUNTS'
                      AND lookup_code = v_customer_id;
                EXCEPTION
                  WHEN OTHERS THEN 
                    L_MSG := SUBSTR('National Account Does Not Exist' || SQLCODE || SQLERRM,1,240) ;
            END; 
          END IF;
           
      /*select party site number*/
          IF v_price_type = 'SHIPTO' --If Ship To Agreement select Site Number from Ship To Site
            THEN
              BEGIN
               SELECT c.party_site_number
                 INTO v_party_site_number
                 FROM hz_cust_site_uses_all a
                    , hz_cust_acct_sites_all b
                    , hz_party_sites c
                WHERE     a.cust_acct_site_id = b.cust_acct_site_id
                      AND b.party_site_id = c.party_site_id
                      AND a.site_use_id = v_customer_site_id;
                 EXCEPTION
                  WHEN OTHERS THEN 
                    L_MSG := SUBSTR('Party Site Number Not Found' || SQLCODE || SQLERRM,1,240) ;
                  END;
                  
            ELSIF v_price_type = 'MASTER' --If Bill To Agreement select Customer Account Number from the customer
            THEN
              BEGIN
               SELECT account_number
                 INTO v_party_site_number
                 FROM hz_cust_accounts
                WHERE cust_account_id = v_customer_id;
               EXCEPTION
                WHEN OTHERS THEN 
                  L_MSG := SUBSTR('Party Site Number Not Found' || SQLCODE || SQLERRM,1,240) ;
                END;
            ELSE
               v_party_site_number := NULL;
            END IF;
            
             IF v_price_type = 'SHIPTO'
            THEN
               v_qualifier_value := v_customer_site_id; --INSERT INTO customers VALUES(:p_if1,:p_if2);
               v1_price_type := 'QUALIFIER_ATTRIBUTE11';
               v_qual_context := 'CUSTOMER';
               v_excluder_flag := 'N';
               v_operator_code := '=';
               v_delimitier := '-'; 
               v_qualifier_grouping_no := 1;
               v_qualifier_precedence := 1;
               v_operation := QP_GLOBALS.G_OPR_CREATE; 
               v_active_flag := 'Y';
                               
            ELSIF v_price_type = 'MASTER'
            THEN
               v_qualifier_value := v_customer_id;
               v1_price_type := 'QUALIFIER_ATTRIBUTE32';
               v_qual_context := 'CUSTOMER';
               v_excluder_flag := 'N';
               v_operator_code := '=';
               v_delimitier := '-'; 
               v_qualifier_grouping_no := 1;
               v_qualifier_precedence := 1;
               v_operation := QP_GLOBALS.G_OPR_CREATE; 
               v_active_flag := 'Y';
               
            ELSE
               v_qualifier_value := NULL;
               v1_price_type := NULL;
               v_qual_context := NULL;
               v_excluder_flag := NULL;
               v_operator_code := NULL;
               v_delimitier := NULL ; 
               v_qualifier_grouping_no := NULL;
               v_qualifier_precedence := NULL;
               v_operation := NULL;
               v_active_flag := 'N';
            END IF;

      --Create a Modifier header of type 'PRO' (Promotion) 
      l_MODIFIER_LIST_rec.currency_code := 'USD';
      l_MODIFIER_LIST_rec.list_type_code := 'DLT';
      l_MODIFIER_LIST_rec.source_system_code := 'QP';
      l_MODIFIER_LIST_rec.active_flag := v_active_flag ;
      l_MODIFIER_LIST_rec.automatic_flag := 'Y';
      l_MODIFIER_LIST_rec.name := v_party_name||v_delimitier||v_party_site_number ; --Concatenation for Modifier Name / Number Format. Will be preceeded by CSp or VQN based on Header Level Agreement Selection.
      l_MODIFIER_LIST_rec.description := v_party_name||v_delimitier||v_party_site_number ; --Concatenation for Modifier Name / Number Format. Will be preceeded by CSp or VQN based on Header Level Agreement Selection.
      l_MODIFIER_LIST_rec.version_no := v_attribute_14||'.'||v_attribute_15 ;
      l_MODIFIER_LIST_rec.pte_code := 'ORDFUL';
      l_MODIFIER_LIST_rec.operation := QP_GLOBALS.G_OPR_CREATE;
      l_MODIFIER_LIST_rec.context := '162';
      l_MODIFIER_LIST_rec.attribute10 := v_attribute_10 ;
      l_MODIFIER_LIST_rec.attribute11 := v_attribute_11 ;
      l_MODIFIER_LIST_rec.attribute12 := v_attribute_12 ;
      l_MODIFIER_LIST_rec.attribute13 := v_attribute_13 ;
      l_MODIFIER_LIST_rec.attribute14 := v_attribute_14 ;
      l_MODIFIER_LIST_rec.attribute15 := v_attribute_15 ; 
      
      --Modifier Header Level Qualifiers
      l_QUALIFIERS_tbl(1).excluder_flag := v_excluder_flag ;
      l_QUALIFIERS_tbl(1).comparison_operator_code := v_operator_code ;
      l_QUALIFIERS_tbl(1).qualifier_context := v_qual_context ; --'CUSTOMER';
      l_QUALIFIERS_tbl(1).qualifier_attribute := v1_price_type ;
      l_QUALIFIERS_tbl(1).qualifier_attr_value := v_qualifier_value ;
      l_QUALIFIERS_tbl(1).qualifier_grouping_no := v_qualifier_grouping_no ;
      l_QUALIFIERS_tbl(1).qualifier_precedence := v_qualifier_precedence ;
      l_QUALIFIERS_tbl(1).operation := v_operation ;
      
      QP_Modifiers_PUB.Process_Modifiers
      ( p_api_version_number => 1.0
      , p_init_msg_list => FND_API.G_FALSE
      , p_return_values => FND_API.G_FALSE
      , p_commit => FND_API.G_FALSE
      , x_return_status => l_return_status
      , x_msg_count =>x_msg_count
      , x_msg_data =>x_msg_data
      ,p_MODIFIER_LIST_rec => l_MODIFIER_LIST_rec
      ,p_MODIFIERS_tbl => l_MODIFIERS_tbl
      ,p_QUALIFIERS_tbl => l_QUALIFIERS_tbl
      ,p_PRICING_ATTR_tbl => l_PRICING_ATTR_tbl
      ,x_MODIFIER_LIST_rec => l_x_MODIFIER_LIST_rec         
      ,x_MODIFIER_LIST_val_rec => l_x_MODIFIER_LIST_val_rec 
      ,x_MODIFIERS_tbl => l_x_MODIFIERS_tbl                 
      ,x_MODIFIERS_val_tbl => l_x_MODIFIERS_val_tbl         
      ,x_QUALIFIERS_tbl => l_x_QUALIFIERS_tbl               
      ,x_QUALIFIERS_val_tbl => l_x_QUALIFIERS_val_tbl       
      ,x_PRICING_ATTR_tbl => l_x_PRICING_ATTR_tbl           
      ,x_PRICING_ATTR_val_tbl => l_x_PRICING_ATTR_val_tbl   
      );
      
      update xxwc.xxwc_om_contract_pricing_hdr
      set revision_number = 1
        , attribute1 = 'CONVERTED'
        , agreement_status = 'APPROVED'
      WHERE AGREEMENT_ID = v_attribute_14; 
    
      COMMIT ;

    END LOOP ;

  END csp_headers ; 


/*************************************************************************
*   PROCEDURE Name: csp_lines
*
*   PURPOSE:   CREATE_LINES procedure is used to create Modifier Lines
*              in Advanced Pricing for converted CSP agreements
* ***************************************************************************/
   PROCEDURE csp_lines --(errbuf OUT VARCHAR2, retcode OUT VARCHAR2)
   IS

/* $Header: QPXEXDS1.sql 115.6 2006/08/24 04:51:05 nirmkuma ship $ */
l_control_rec QP_GLOBALS.Control_Rec_Type;
l_return_status VARCHAR2(1);
x_msg_count number;
x_msg_data Varchar2(2000);
x_msg_index number;

l_MODIFIER_LIST_rec QP_Modifiers_PUB.Modifier_List_Rec_Type;
l_MODIFIER_LIST_val_rec QP_Modifiers_PUB.Modifier_List_Val_Rec_Type;
l_MODIFIERS_tbl QP_Modifiers_PUB.Modifiers_Tbl_Type;
n_MODIFIERS_tbl QP_Modifiers_PUB.Modifiers_Tbl_Type;
l_MODIFIERS_val_tbl QP_Modifiers_PUB.Modifiers_Val_Tbl_Type;
l_QUALIFIERS_tbl QP_Qualifier_Rules_PUB.Qualifiers_Tbl_Type;
l_QUALIFIERS_val_tbl QP_Qualifier_Rules_PUB.Qualifiers_Val_Tbl_Type;
l_PRICING_ATTR_tbl QP_Modifiers_PUB.Pricing_Attr_Tbl_Type;
n_PRICING_ATTR_tbl QP_Modifiers_PUB.Pricing_Attr_Tbl_Type;
l_PRICING_ATTR_val_tbl QP_Modifiers_PUB.Pricing_Attr_Val_Tbl_Type;

l_x_MODIFIER_LIST_rec QP_Modifiers_PUB.Modifier_List_Rec_Type;
l_x_MODIFIER_LIST_val_rec QP_Modifiers_PUB.Modifier_List_Val_Rec_Type;
l_x_MODIFIERS_tbl QP_Modifiers_PUB.Modifiers_Tbl_Type;
l_x_MODIFIERS_val_tbl QP_Modifiers_PUB.Modifiers_Val_Tbl_Type;
l_x_QUALIFIERS_tbl QP_Qualifier_Rules_PUB.Qualifiers_Tbl_Type;
l_x_QUALIFIERS_val_tbl QP_Qualifier_Rules_PUB.Qualifiers_Val_Tbl_Type;
l_x_PRICING_ATTR_tbl QP_Modifiers_PUB.Pricing_Attr_Tbl_Type;
l_x_PRICING_ATTR_val_tbl QP_Modifiers_PUB.Pricing_Attr_Val_Tbl_Type;

--Toby's Variables
--CSP Header Level Variables


v_agreement_id Number;
v_agreement_type VARCHAR2(30) ;
v_vendor_quote_number NUMBER ;
v_price_type VARCHAR2(30);
v_customer_id NUMBER ;
v_customer_site_id NUMBER ;
v_agreement_status VARCHAR2(30) ;
v_revision_number NUMBER ;
v_incompatability_group VARCHAR2(30);
v_incomp_group VARCHAR2(30);
v_attribute_10 VARCHAR2(25) :='Contract Pricing'; --Variable to populate DFF Attribute 10 on Modifier Header (Contract Pricing or Vendor Quote)
v_attribute_11 Number := NULL ;
v_attribute_12 VARCHAR2(25) := NULL ; --Vendor Name for VQN
v_attribute_13 VARCHAR2(25) := NULL ; --VEndor Number for VQN
v_attribute_14 Number ; --Mapped to Vendor Agreement Number DFF on Modifier Header
v_attribute_15 Number ; --Mapped to Agreement Revision Number DFF on Modifier Header
v1_price_type VARCHAR2(30);
v_customer_name VARCHAR2(50) ;
v_party_id Number ;
v_party_name VARCHAR2(360) ;
v_party_site_number Number ;
v_qualifier_value Number ;
v_agreeid Number ;
v_qual_context VARCHAR2(360);
v_excluder_flag VARCHAR2(1) ;
v_operator_code VARCHAR(360) ;
v_delimitier VARCHAR2(1) ;
v_qualifier_grouping_no Number ;
v_qualifier_precedence Number ; 
v_operation VARCHAR2(30) ; 
v_active_flag VARCHAR2(30) ;
v_list_header_id NUMBER ; --v_list_header_id
v_list_line_id NUMBER ; 
--START XXWC_CSP_LINES VARIABLES
v_atrribute_3 NUMBER ;
v_ln_special_cost NUMBER ;
v_ln_id NUMBER ; --Line Level Variable for xxwc.agreement_line_id
v_ln_agreement_type VARCHAR2(30) ; --Line Level Variable for xxwc.agreement_type
v_ln_agree_type VARCHAR2(30) ; --Set CSP or VQN Agreement Type Variable passed to API
v_ln_vendor_quote_number VARCHAR2(30) ;  --Line Level Variable for XXWC.VENDOR_QUOTE_NUMBER
v_ln_start_date DATE ; --Line Level Variable for XXWC.START_DATE
v_ln_end_date DATE;  --Line Level Variable for XXWC.END_DATE
v_ln_mod_type  VARCHAR2(30) ; --Line Level Variable for Modifier Type
v_arithmetic_operator VARCHAR2(30) ; --Line Level Variable for Modifier Type API Value
v_ln_mod_value NUMBER ; --Line Level Variable for Modifier Value
v_ln_mod_val NUMBER ; --Line Level Modifer Value passed to API
v_ln_vendor_id NUMBER ;--Line LEvel Variable for Vendor ID
v_product_attribute VARCHAR2(50) ;  --Line Level Variable for Product Attribute (Item/Cat Class)
v_product_value VARCHAR2(50) ;--Line Level Variable for Product Value (SKU Number)
v_product_attribute_api VARCHAR2(50) ; --Line Level Variable to set in API
v_product_attribute_context VARCHAR2(30) := 'ITEM' ; --Variable to set product_attribute_context in Pricing Attr API
v_product_attr_value VARCHAR2(50) ;
v_generate_using_formula_id NUMBER ;
V9 NUMBER ; 
i NUMBER ;
L_MSG VARCHAR2(240) ; 

CURSOR C1 IS
select distinct a.agreement_id
from XXWC.xxwc_om_contract_pricing_lines a,
     XXWC.xxwc_om_contract_pricing_hdr b
where b.attribute1 = 'CONVERTED'
and a.agreement_id=b.agreement_id; 

CURSOR LINEID (P1 in number) IS
select agreement_line_id 
from xxwc.xxwc_om_contract_pricing_lines 
where agreement_id = P1 
and line_status = 'AWAITING_APPROVAL'; 
   
  BEGIN
      
      For c1_rec in C1
      LOOP 
      l_modifiers_tbl := n_modifiers_tbl;
      l_PRICING_ATTR_tbl := n_PRICING_ATTR_tbl;
      i := 0;

      FOR c2_rec in LINEID (c1_rec.agreement_id)

      LOOP
        i := i + 1;
        BEGIN
         SELECT agreement_id
              ,  agreement_line_id
              , agreement_type
              , vendor_quote_number
              , product_attribute
              , product_value
              , start_date
              , end_date
              , modifier_type
              , modifier_value
              , special_cost
              , vendor_id
              , revision_number
           INTO V9
              , v_ln_id
              ,                    --Line Level Variable for agreement_line_id
               v_ln_agreement_type
              ,                       --Line Level Variable for agreement_type
               v_ln_vendor_quote_number
              ,             --Line Level Variable for v_ln_vendor_quote_number
               v_product_attribute
              ,   --Line Level Variable for Product Attribute (Item/Cat Class)
               v_product_value
              , v_ln_start_date
              ,                           --Line Level Variable for start date
               v_ln_end_date
              ,                             --Line Level Variable for end date
               v_ln_mod_type
              ,                        --Line Level Variable for Modifier Type
               v_ln_mod_value
              ,                       --Line Level Variable for Modifier Value
               v_ln_special_cost
              ,                         --Line Level Variable for Special Cost
               v_ln_vendor_id              --Line LEvel Variable for Vendor ID
              , 
               v_revision_number
           FROM xxwc.xxwc_om_contract_pricing_lines
          WHERE agreement_line_id = c2_rec.agreement_line_id ;
          EXCEPTION
          WHEN OTHERS THEN
          L_MSG := SUBSTR('INITIAL SELECT FAILED' || SQLCODE || SQLERRM,1,240) ;
               FND_FILE.Put_Line(FND_FILE.Log,'l_quote_number = '||V9);
            FND_FILE.Put_Line(FND_FILE.Log,'l_customer_id = '||v_ln_id); 
            end ; 




           --Select modifier list_header_id from modifier list
         BEGIN
         SELECT list_header_id
           INTO v_list_header_id
           FROM qp_list_headers
          WHERE attribute14 = V9 ;
                    EXCEPTION
          WHEN OTHERS THEN
          L_MSG := SUBSTR('NO LIST HEADER ID FOUND' || SQLCODE || SQLERRM,1,240) ;
               FND_FILE.Put_Line(FND_FILE.Log,'l_quote_number = '||V9); 
            end ;

         --Determine Imcompatability from CSP Agreement
         SELECT incompatability_group
           INTO v_incompatability_group
           FROM xxwc.xxwc_om_contract_pricing_hdr
          WHERE agreement_id = V9 ;

         --Set Incompatability Group in API
         IF v_incompatability_group = 'Best Price Wins'
         THEN
            v_incomp_group := 'LVL 1';
         ELSIF v_incompatability_group = 'Exclusive'
         THEN
            v_incomp_group := 'EXCL';
         ELSE
            v_incomp_group := NULL;
         END IF;

         --Set Product Attribute Variable to pass to API (Item, Cat Class, Category)
         IF v_product_attribute = 'ITEM'
         THEN
            v_product_attribute_api := 'PRICING_ATTRIBUTE1';
         ELSIF v_product_attribute = 'CAT_CLASS'
         THEN
            v_product_attribute_api := 'PRICING_ATTRIBUTE2';
         ELSIF v_product_attribute = 'CATEGORY'
         THEN
            v_product_attribute_api := 'PRICING_ATTRIBUTE26';
         ELSE
            v_arithmetic_operator := NULL;
         END IF;

         --Set Arithetic Operator Variable
         IF v_ln_mod_type = 'New Price'
         THEN
            v_arithmetic_operator := 'NEWPRICE';
         ELSIF v_ln_mod_type = 'Amount'
         THEN
            v_arithmetic_operator := 'AMT';
         ELSIF v_ln_mod_type = 'Percent'
         THEN
            v_arithmetic_operator := '%';
         ELSIF v_ln_mod_type = 'Cost Plus'
         THEN
            v_arithmetic_operator := 'NEWPRICE';
         ELSE
            v_arithmetic_operator := NULL;
         END IF;

         --IF Application Mthod = Cost Plus then set formula ID variable in API
         IF v_ln_mod_type = 'Cost Plus'
         THEN
            v_generate_using_formula_id := 7030;
         ELSE
            v_generate_using_formula_id := NULL;
         END IF;


         IF v_ln_mod_type = 'New Price'
         THEN
            v_ln_mod_val := v_ln_mod_value;
         ELSIF v_ln_mod_type = 'Amount'
         THEN
            v_ln_mod_val := v_ln_mod_value;
         ELSIF v_ln_mod_type = 'Percent'
         THEN
            v_ln_mod_val := v_ln_mod_value;
         ELSIF v_ln_mod_type = 'Cost Plus'
         THEN
            v_ln_mod_val := (v_ln_mod_value / 100);
         ELSE
            v_arithmetic_operator := NULL;
         END IF;

         IF v_ln_agreement_type = 'VQN'
         THEN
            SELECT segment1
              INTO v_atrribute_3
              FROM ap_suppliers
             WHERE vendor_id = v_ln_vendor_id;
         ELSE
            v_atrribute_3 := NULL;
         END IF;

         --Set Item Numbers, Category Numbers in API
         IF v_product_attribute = 'ITEM'
         THEN
            SELECT inventory_item_id
              INTO v_product_attr_value
              FROM mtl_system_items_b
             WHERE segment1 = v_product_value AND organization_id = '222';
         ELSIF v_product_attribute = 'CAT_CLASS'
         THEN
            SELECT category_id
              INTO v_product_attr_value
              FROM mtl_categories_b
             WHERE segment1 || '.' || segment2 = v_product_value;
         ELSIF v_product_attribute = 'CATEGORY'
         THEN
            v_product_attr_value := v_product_value;
         ELSE
            v_product_attr_value := NULL;
         END IF;

         --Set Line Line DFF on Modifier to CSP or VQN
         IF v_ln_agreement_type = 'VQN'
         THEN
            v_ln_agree_type := 'Vendor Quote';
         ELSIF v_ln_agreement_type = 'CSP'
         THEN
            v_ln_agree_type := 'Contract Pricing';
         ELSE
            v_ln_agree_type := NULL;
         END IF;

         --Create a Modifier line to specify 'Get 8% discount' condition
         l_MODIFIERS_tbl (i).list_header_id := v_list_header_id;
         l_MODIFIERS_tbl (i).list_line_type_code := 'DIS';
         l_MODIFIERS_tbl (i).automatic_flag := 'Y';
         l_MODIFIERS_tbl (i).modifier_level_code := 'LINE';
         l_MODIFIERS_tbl (i).accrual_flag := 'N';
         l_MODIFIERS_tbl (i).start_date_active := v_ln_start_date;
         l_MODIFIERS_tbl (i).end_date_active := v_ln_end_date;
         l_MODIFIERS_tbl (i).arithmetic_operator := v_arithmetic_operator;
         l_MODIFIERS_tbl (i).pricing_group_sequence := 1;     --Pricing Bucket
         l_MODIFIERS_tbl (i).pricing_phase_id := 2; --Pricing Phase, '2' = List Line Adjustment
         l_MODIFIERS_tbl (i).product_precedence := 220;
         l_MODIFIERS_tbl (i).operand := v_ln_mod_val;
         l_MODIFIERS_tbl (i).INCLUDE_ON_RETURNS_FLAG := 'Y' ;
         l_MODIFIERS_tbl (i).context := '162';
         l_MODIFIERS_tbl (i).ATTRIBUTE2 := v_ln_id;
         l_MODIFIERS_tbl (i).ATTRIBUTE3 := v_atrribute_3;
         l_MODIFIERS_tbl (i).ATTRIBUTE4 := v_ln_special_cost;
         l_MODIFIERS_tbl (i).ATTRIBUTE5 := v_ln_agree_type;
         l_MODIFIERS_tbl (i).ATTRIBUTE6 := v_ln_vendor_quote_number;
         --l_MODIFIERS_tbl(1).modifier_parent_index := 1;
         l_MODIFIERS_tbl (i).price_by_formula_id := v_generate_using_formula_id;
         l_MODIFIERS_tbl (i).incompatibility_grp_code := v_incomp_group; --'LVL 1' or 'EXCL'
         l_MODIFIERS_tbl (i).operation := QP_GLOBALS.G_OPR_CREATE;


         --Set Pricing Attributes in API
         l_PRICING_ATTR_tbl (i).list_header_id := v_list_header_id;
         l_PRICING_ATTR_tbl (i).product_attribute_context := v_product_attribute_context;       --v_product_attribute_context ;
         l_PRICING_ATTR_tbl (i).product_attribute := v_product_attribute_api;
         l_PRICING_ATTR_tbl (i).product_attr_value := v_product_attr_value;
         --l_PRICING_ATTR_tbl(i).product_uom_code:= 'Ea';
         l_PRICING_ATTR_tbl (i).MODIFIERS_index := i ;
         l_PRICING_ATTR_tbl (i).operation := QP_GLOBALS.G_OPR_CREATE;
         
       UPDATE xxwc.xxwc_om_contract_pricing_lines
          SET revision_number = (v_revision_number + 1)
            , interfaced_flag = 'Y'
            , line_status = 'APPROVED' 
        WHERE agreement_line_id = c2_rec.agreement_line_id ; 
END LOOP ;

      QP_Modifiers_PUB.Process_Modifiers (
         p_api_version_number      => 1.0
       , p_init_msg_list           => FND_API.G_FALSE
       , p_return_values           => FND_API.G_FALSE
       , p_commit                  => FND_API.G_FALSE
       , x_return_status           => l_return_status
       , x_msg_count               => x_msg_count
       , x_msg_data                => x_msg_data
       , p_MODIFIER_LIST_rec       => l_MODIFIER_LIST_rec
       , p_MODIFIERS_tbl           => l_MODIFIERS_tbl
       , p_QUALIFIERS_tbl          => l_QUALIFIERS_tbl
       , p_PRICING_ATTR_tbl        => l_PRICING_ATTR_tbl
       , x_MODIFIER_LIST_rec       => l_x_MODIFIER_LIST_rec
       , x_MODIFIER_LIST_val_rec   => l_x_MODIFIER_LIST_val_rec
       , x_MODIFIERS_tbl           => l_x_MODIFIERS_tbl
       , x_MODIFIERS_val_tbl       => l_x_MODIFIERS_val_tbl
       , x_QUALIFIERS_tbl          => l_x_QUALIFIERS_tbl
       , x_QUALIFIERS_val_tbl      => l_x_QUALIFIERS_val_tbl
       , x_PRICING_ATTR_tbl        => l_x_PRICING_ATTR_tbl
       , x_PRICING_ATTR_val_tbl    => l_x_PRICING_ATTR_val_tbl);


COMMIT ;
END LOOP ;
END csp_lines ;

/*************************************************************************
*   PROCEDURE Name: csp_exclusions
*
*   PURPOSE:   create exclusions procedure is used to create 
*              'Item/Category Class' , 'Item/Category' Exclusions, 
*              and 'Category Class/Category Exclusions on modifier list lines.
*              This process is used during the conversion process
****************************************************************************/
   PROCEDURE csp_exclusions 
   IS
      /* $Header: QPXEXDS1.sql 115.6 2006/08/24 04:51:05 nirmkuma ship $ */
      l_control_rec                 QP_GLOBALS.Control_Rec_Type;
      l_return_status               VARCHAR2 (1);
      x_msg_count                   NUMBER;
      x_msg_data                    VARCHAR2 (2000);
      x_msg_index                   NUMBER;

      l_MODIFIER_LIST_rec           QP_Modifiers_PUB.Modifier_List_Rec_Type;
      l_MODIFIER_LIST_val_rec       QP_Modifiers_PUB.Modifier_List_Val_Rec_Type;
      l_MODIFIERS_tbl               QP_Modifiers_PUB.Modifiers_Tbl_Type;
      l_MODIFIERS_val_tbl           QP_Modifiers_PUB.Modifiers_Val_Tbl_Type;
      l_QUALIFIERS_tbl              QP_Qualifier_Rules_PUB.Qualifiers_Tbl_Type;
      l_QUALIFIERS_val_tbl          QP_Qualifier_Rules_PUB.Qualifiers_Val_Tbl_Type;
      l_PRICING_ATTR_tbl            QP_Modifiers_PUB.Pricing_Attr_Tbl_Type;
      l_PRICING_ATTR_val_tbl        QP_Modifiers_PUB.Pricing_Attr_Val_Tbl_Type;
      n_PRICING_ATTR_tbl            QP_Modifiers_PUB.Pricing_Attr_Tbl_Type;

      l_x_MODIFIER_LIST_rec         QP_Modifiers_PUB.Modifier_List_Rec_Type;
      l_x_MODIFIER_LIST_val_rec     QP_Modifiers_PUB.Modifier_List_Val_Rec_Type;
      l_x_MODIFIERS_tbl             QP_Modifiers_PUB.Modifiers_Tbl_Type;
      l_x_MODIFIERS_val_tbl         QP_Modifiers_PUB.Modifiers_Val_Tbl_Type;
      l_x_QUALIFIERS_tbl            QP_Qualifier_Rules_PUB.Qualifiers_Tbl_Type;
      l_x_QUALIFIERS_val_tbl        QP_Qualifier_Rules_PUB.Qualifiers_Val_Tbl_Type;
      l_x_PRICING_ATTR_tbl          QP_Modifiers_PUB.Pricing_Attr_Tbl_Type;
      l_x_PRICING_ATTR_val_tbl      QP_Modifiers_PUB.Pricing_Attr_Val_Tbl_Type;

      --Toby's Variables
      --CSP Header Level Variables
      v_agreement_id                NUMBER;
      v_agreement_type              VARCHAR2 (30);
      v_vendor_quote_number         NUMBER;
      v_price_type                  VARCHAR2 (30);
      v_customer_id                 NUMBER;
      v_customer_site_id            NUMBER;
      v_agreement_status            VARCHAR2 (30);
      v_revision_number             NUMBER;
      v_incompatibility_group       VARCHAR2 (30);
      v_attribute_10                VARCHAR2 (25) := 'Contract Pricing'; --Variable to populate DFF Attribute 10 on Modifier Header (Contract Pricing or Vendor Quote)
      v_attribute_11                NUMBER := NULL;
      v_attribute_12                VARCHAR2 (25) := NULL; --Vendor Name for VQN
      v_attribute_13                VARCHAR2 (25) := NULL; --VEndor Number for VQN
      v_attribute_14                NUMBER; --Mapped to Vendor Agreement Number DFF on Modifier Header
      v_attribute_15                NUMBER; --Mapped to Agreement Revision Number DFF on Modifier Header
      v1_price_type                 VARCHAR2 (30);
      v_customer_name               VARCHAR2 (50);
      v_party_id                    NUMBER;
      v_party_name                  VARCHAR2 (360);
      v_party_site_number           NUMBER;
      v_qualifier_value             NUMBER;
      v_agreeid                     NUMBER;
      v_qual_context                VARCHAR2 (360);
      v_excluder_flag               VARCHAR2 (1);
      v_operator_code               VARCHAR (360);
      v_delimitier                  VARCHAR2 (1);
      v_qualifier_grouping_no       NUMBER;
      v_qualifier_precedence        NUMBER;
      v_operation                   VARCHAR2 (30);
      v_active_flag                 VARCHAR2 (30);
      v_list_header_id              NUMBER;
      v_list_line_id                NUMBER;
      --START XXWC_CSP_LINES VARIABLES
      v_ln_id                       NUMBER; --Line Level Variable for xxwc.agreement_line_id
      v_ln_agreement_type           VARCHAR2 (30); --Line Level Variable for xxwc.agreement_type
      v_ln_vendor_quote_number      VARCHAR2 (30); --Line Level Variable for XXWC.VENDOR_QUOTE_NUMBER
      v_ln_start_date               DATE; --Line Level Variable for XXWC.START_DATE
      v_ln_end_date                 DATE; --Line Level Variable for XXWC.END_DATE
      v_ln_mod_type                 VARCHAR2 (30); --Line Level Variable for Modifier Type
      v_arithmetic_operator         VARCHAR2 (30); --Line Level Variable for Modifier Type API Value
      v_ln_mod_value                NUMBER; --Line Level Variable for Modifier Value
      --v_ln_special_cost, --Line Level Variable for Special Cost
      --v_ln_vendor_id --Line LEvel Variable for Vendor ID
      v_product_attribute           VARCHAR2 (50); --Line Level Variable for Product Attribute (Item/Cat Class)
      v_product_value               VARCHAR2 (50); --Line Level Variable for Product Value (SKU Number)
      v_product_attribute_api       VARCHAR2 (50); --Line Level Variable to set in API
      v_product_attribute_context   VARCHAR2 (30) := 'ITEM'; --Variable to set product_attribute_context in Pricing Attr API
      v_product_attr_value          VARCHAR2 (50);
      v_generate_using_formula_id   NUMBER;
      v1                            NUMBER;            --Needed for Exclusions
      v2                            NUMBER;            --Needed for Exclusions
      v3                            NUMBER;            --Needed for Exclusions
      v4                            VARCHAR2 (50);     --Needed for Exclusions
      v5                            NUMBER;            --Needed for Exclusions
      V6                            NUMBER;
      p_agreement_id                NUMBER;
      L_MSG                         VARCHAR2(240);
      
      

      CURSOR AGREEID 
      IS select agreement_id
      from xxwc.xxwc_om_contract_pricing_hdr
      WHERE attribute1 = 'CONVERTED' ;
      
      CURSOR AGREE_LINE_ID (P1 in NUMBER)
      IS
         SELECT a.agreement_line_id                            
           FROM xxwc.xxwc_om_contract_pricing_lines a
          WHERE a.product_attribute = 'CAT_CLASS'
                AND a.agreement_id = P1 ; 

      CURSOR ITEM_ID (C3 IN NUMBER)
      IS
         SELECT b.inventory_item_id
           FROM xxwc.xxwc_om_contract_pricing_lines a
              , mtl_system_items_b b
              , mtl_item_categories c
          WHERE     a.product_value = b.segment1
                AND a.product_attribute = 'ITEM'
                AND b.organization_id = '222'
                AND b.organization_id = c.organization_id
                AND b.inventory_item_id = c.inventory_item_id
                AND a.agreement_id = C3
                AND c.category_id = V1;

      CURSOR CATEGORY_LINE_ID (C4 IN NUMBER)
      IS
         SELECT agreement_line_id
           FROM xxwc.xxwc_om_contract_pricing_lines
          WHERE product_attribute = 'CATEGORY'
                AND agreement_id = C4 ; 

      CURSOR CATEGORY_ID (C5 IN NUMBER)
      IS
         SELECT b.category_id 
           FROM XXWC.xxwc_om_contract_pricing_lines a, mtl_categories_b b
          WHERE     a.product_attribute = 'CAT_CLASS'
                AND b.segment1 = V4
                AND a.product_value = b.segment1 || '.' || segment2
                and agreement_id = C5 ; 

      CURSOR CAT_ITEM_EXE (C7 IN NUMBER)
      IS
         SELECT b.inventory_item_id
           FROM xxwc.xxwc_om_contract_pricing_lines a
              , mtl_system_items_b b
              , mtl_item_categories c
              , mtl_categories_b d
          WHERE     a.product_value = b.segment1
                AND a.product_attribute = 'ITEM'
                AND b.organization_id = '222'
                AND d.segment1 = V4
                AND b.organization_id = c.organization_id
                AND b.inventory_item_id = c.inventory_item_id
                AND c.category_id = d.category_id
                AND a.agreement_id = C7 ; 
   BEGIN
   
   FOR c1_rec in AGREEID
   LOOP
   
      l_PRICING_ATTR_tbl := n_PRICING_ATTR_tbl;
   
   
       UPDATE XXWC.xxwc_om_contract_pricing_hdr
       SET attribute1 = SYSDATE
       where agreement_id = c1_rec.agreement_id ; 
             
      --Select Agreement Line ID from AGREEMENT ID pull form AGREEMENT_ID
      FOR c2_rec IN AGREE_LINE_ID (c1_rec.agreement_id)
      LOOP
    /**************************************
    *   Creating Category Level Exclusions
    **************************************/
      BEGIN
         SELECT list_line_id
           INTO v_list_line_id
           FROM qp_list_lines
          WHERE attribute2 = c2_rec.agreement_line_id;
        EXCEPTION
            WHEN OTHERS THEN 
              L_MSG := SUBSTR('NO MODIFIER LINE EXISTS FOR CATEGORY' || SQLCODE || SQLERRM,1,240) ;
          END;           

      /*   select category ID to query items againsts */
        BEGIN 
         SELECT category_id
           INTO V1
           FROM xxwc.xxwc_om_contract_pricing_lines a, mtl_categories_b b
          WHERE     a.product_attribute = 'CAT_CLASS'
                AND b.segment1 || '.' || b.segment2 = a.product_value
                AND a.agreement_line_id = c2_rec.agreement_line_id;
        EXCEPTION
            WHEN OTHERS THEN 
              L_MSG := SUBSTR('INVALID CATEGORY ID' || SQLCODE || SQLERRM,1,240) ;
          END;                

                  FND_FILE.Put_Line(FND_FILE.Log,'AGREEMENT LINE ID = '||c2_rec.agreement_line_id);

       /* Find Item Numbers assigned to category id from and loop them into API */
         FOR c3_rec IN ITEM_ID (c1_rec.agreement_id)
         LOOP
            --Set Pricing Attributes in API
            l_PRICING_ATTR_tbl (1).list_line_id := v_list_line_id;  --173088 ;
            l_PRICING_ATTR_tbl (1).product_attribute_context := 'ITEM'; --v_product_attribute_context ; --v_product_attribute_context ;
            l_PRICING_ATTR_tbl (1).product_attribute := 'PRICING_ATTRIBUTE1'; --v_product_attribute_api ;
            l_PRICING_ATTR_tbl (1).product_attr_value :=
               c3_rec.inventory_item_id;                  --2928289 505CAMLOCK
            l_PRICING_ATTR_tbl (1).excluder_flag := 'Y';
            l_PRICING_ATTR_tbl (1).MODIFIERS_index := 1;
            l_PRICING_ATTR_tbl (1).operation := QP_GLOBALS.G_OPR_CREATE;

            --API Call
            QP_Modifiers_PUB.Process_Modifiers (
               p_api_version_number      => 1.0
             , p_init_msg_list           => FND_API.G_FALSE
             , p_return_values           => FND_API.G_FALSE
             , p_commit                  => FND_API.G_FALSE
             , x_return_status           => l_return_status
             , x_msg_count               => x_msg_count
             , x_msg_data                => x_msg_data
             , p_MODIFIER_LIST_rec       => l_MODIFIER_LIST_rec
             , p_MODIFIERS_tbl           => l_MODIFIERS_tbl
             , p_QUALIFIERS_tbl          => l_QUALIFIERS_tbl
             , p_PRICING_ATTR_tbl        => l_PRICING_ATTR_tbl
             , x_MODIFIER_LIST_rec       => l_x_MODIFIER_LIST_rec
             , x_MODIFIER_LIST_val_rec   => l_x_MODIFIER_LIST_val_rec
             , x_MODIFIERS_tbl           => l_x_MODIFIERS_tbl
             , x_MODIFIERS_val_tbl       => l_x_MODIFIERS_val_tbl
             , x_QUALIFIERS_tbl          => l_x_QUALIFIERS_tbl
             , x_QUALIFIERS_val_tbl      => l_x_QUALIFIERS_val_tbl
             , x_PRICING_ATTR_tbl        => l_x_PRICING_ATTR_tbl
             , x_PRICING_ATTR_val_tbl    => l_x_PRICING_ATTR_val_tbl);
            COMMIT;
         END LOOP;
      END LOOP;



      /*********************************
      Write Exclusions against Category
      *********************************/

      --Select Agreement Line_ID for Cateogry to write exclusions against
      FOR c4_rec IN CATEGORY_LINE_ID (c1_rec.agreement_id)
      LOOP
         --SELECT LIST LINE FROM QP LIST LINES
        BEGIN
         SELECT list_line_id
           INTO v_list_line_id
           FROM qp_list_lines
          WHERE attribute2 = c4_rec.agreement_line_id;
        EXCEPTION
            WHEN OTHERS THEN 
              L_MSG := SUBSTR('No Modifier Line Found' || SQLCODE || SQLERRM,1,240) ;
          END;  
          
        BEGIN
         SELECT product_value
           INTO V4
           FROM XXWC.xxwc_om_contract_pricing_lines
          WHERE agreement_line_id = c4_rec.agreement_line_id;
        EXCEPTION
            WHEN OTHERS THEN 
              L_MSG := SUBSTR('INVALID PRODUCT VALUE' || SQLCODE || SQLERRM,1,240) ;
              FND_FILE.Put_Line(FND_FILE.Log,'AGREEMENT LINE ID = '||c4_rec.agreement_line_id);
          END; 

/* Select Category Classes Assocaited to Category and call API */
         FOR c5_rec IN CATEGORY_ID (c1_rec.agreement_id)
         LOOP
            l_PRICING_ATTR_tbl (1).list_line_id := v_list_line_id;  --173088 ;
            l_PRICING_ATTR_tbl (1).product_attribute_context := 'ITEM'; --v_product_attribute_context ; --v_product_attribute_context ;
            l_PRICING_ATTR_tbl (1).product_attribute := 'PRICING_ATTRIBUTE2'; --v_product_attribute_api ;
            l_PRICING_ATTR_tbl (1).product_attr_value := c5_rec.category_id; --2928289 505CAMLOCK v_product_attr_value ;
            l_PRICING_ATTR_tbl (1).excluder_flag := 'Y';
            l_PRICING_ATTR_tbl (1).MODIFIERS_index := 1;
            l_PRICING_ATTR_tbl (1).operation := QP_GLOBALS.G_OPR_CREATE;

            QP_Modifiers_PUB.Process_Modifiers (
               p_api_version_number      => 1.0
             , p_init_msg_list           => FND_API.G_FALSE
             , p_return_values           => FND_API.G_FALSE
             , p_commit                  => FND_API.G_FALSE
             , x_return_status           => l_return_status
             , x_msg_count               => x_msg_count
             , x_msg_data                => x_msg_data
             , p_MODIFIER_LIST_rec       => l_MODIFIER_LIST_rec
             , p_MODIFIERS_tbl           => l_MODIFIERS_tbl
             , p_QUALIFIERS_tbl          => l_QUALIFIERS_tbl
             , p_PRICING_ATTR_tbl        => l_PRICING_ATTR_tbl
             , x_MODIFIER_LIST_rec       => l_x_MODIFIER_LIST_rec
             , x_MODIFIER_LIST_val_rec   => l_x_MODIFIER_LIST_val_rec
             , x_MODIFIERS_tbl           => l_x_MODIFIERS_tbl
             , x_MODIFIERS_val_tbl       => l_x_MODIFIERS_val_tbl
             , x_QUALIFIERS_tbl          => l_x_QUALIFIERS_tbl
             , x_QUALIFIERS_val_tbl      => l_x_QUALIFIERS_val_tbl
             , x_PRICING_ATTR_tbl        => l_x_PRICING_ATTR_tbl
             , x_PRICING_ATTR_val_tbl    => l_x_PRICING_ATTR_val_tbl);
            COMMIT;
         END LOOP;
      END LOOP;

      /***********************************
      Assign Items to Cateogry Exclusions
      ************************************/

      FOR c4_rec IN CATEGORY_LINE_ID (c1_rec.agreement_id)
      LOOP
         --SELECT LIST LINE FROM QP LIST LINES
        BEGIN
         SELECT list_line_id
           INTO v_list_line_id
           FROM qp_list_lines
          WHERE attribute2 = c4_rec.agreement_line_id;
        EXCEPTION
            WHEN OTHERS THEN 
              L_MSG := SUBSTR('INVALID PRODUCT VALUE' || SQLCODE || SQLERRM,1,240) ;
          END;  
          
       BEGIN   
         SELECT product_value
           INTO V4
           FROM XXWC.xxwc_om_contract_pricing_lines
          WHERE agreement_line_id = c4_rec.agreement_line_id;
        EXCEPTION
            WHEN OTHERS THEN 
              L_MSG := SUBSTR('INVALID PRODUCT VALUE' || SQLCODE || SQLERRM,1,240) ;
              FND_FILE.Put_Line(FND_FILE.Log,'AGREEMENT LINE ID = '||c4_rec.agreement_line_id);
          END;  

/* Find Items associated to Category and Call API */
         FOR rec_r7 IN CAT_ITEM_EXE (c1_rec.agreement_id)
         LOOP
            l_PRICING_ATTR_tbl (1).list_line_id := v_list_line_id;  --173088 ;
            l_PRICING_ATTR_tbl (1).product_attribute_context := 'ITEM'; --v_product_attribute_context ; --v_product_attribute_context ;
            l_PRICING_ATTR_tbl (1).product_attribute := 'PRICING_ATTRIBUTE1'; --v_product_attribute_api ;
            l_PRICING_ATTR_tbl (1).product_attr_value :=
               rec_r7.inventory_item_id; --2928289 505CAMLOCK v_product_attr_value ;
            l_PRICING_ATTR_tbl (1).excluder_flag := 'Y';
            l_PRICING_ATTR_tbl (1).MODIFIERS_index := 1;
            l_PRICING_ATTR_tbl (1).operation := QP_GLOBALS.G_OPR_CREATE;

            QP_Modifiers_PUB.Process_Modifiers (
               p_api_version_number      => 1.0
             , p_init_msg_list           => FND_API.G_FALSE
             , p_return_values           => FND_API.G_FALSE
             , p_commit                  => FND_API.G_FALSE
             , x_return_status           => l_return_status
             , x_msg_count               => x_msg_count
             , x_msg_data                => x_msg_data
             , p_MODIFIER_LIST_rec       => l_MODIFIER_LIST_rec
             , p_MODIFIERS_tbl           => l_MODIFIERS_tbl
             , p_QUALIFIERS_tbl          => l_QUALIFIERS_tbl
             , p_PRICING_ATTR_tbl        => l_PRICING_ATTR_tbl
             , x_MODIFIER_LIST_rec       => l_x_MODIFIER_LIST_rec
             , x_MODIFIER_LIST_val_rec   => l_x_MODIFIER_LIST_val_rec
             , x_MODIFIERS_tbl           => l_x_MODIFIERS_tbl
             , x_MODIFIERS_val_tbl       => l_x_MODIFIERS_val_tbl
             , x_QUALIFIERS_tbl          => l_x_QUALIFIERS_tbl
             , x_QUALIFIERS_val_tbl      => l_x_QUALIFIERS_val_tbl
             , x_PRICING_ATTR_tbl        => l_x_PRICING_ATTR_tbl
             , x_PRICING_ATTR_val_tbl    => l_x_PRICING_ATTR_val_tbl);
                         
            COMMIT;
         END LOOP;
      END LOOP;
    END LOOP;
   END csp_exclusions;

/*************************************************************************
*   PROCEDURE Name: csp_mass_conversion
*
*   PURPOSE:   csp_mass_conversion procedure is used to execute the above 
*              three procedures when running a mass load CSP conversion. 
*              The  procedure is called by teh concurrent request 'CSP Code'
****************************************************************************/
   PROCEDURE csp_mass_conversion (errbuf OUT VARCHAR2
                            ,retcode OUT VARCHAR2 )
                            
   IS
   BEGIN
      csp_headers ;
      csp_lines ;
      csp_exclusions ; 
   END csp_mass_conversion;
   
/*************************************************************************
*   PROCEDURE Name: csp_cust_quote 
*
*   PURPOSE:   csp_quote procedure is used to import a quote from Order 
               Management in a CSP Agreement. The program is called by 
               concurrent program 'XXWC OM Quote to CSP Import'
****************************************************************************/  
PROCEDURE csp_cust_quote (errbuf OUT VARCHAR2, 
                     retcode OUT VARCHAR2, 
                     i_quotenumber IN NUMBER, 
                     i_price_type in VARCHAR2, 
                     i_incomp_group in VARCHAR2,
                     i_vq_number in VARCHAR2, 
                     i_vendor_number in NUMBER,
                     i_start_date in VARCHAR2) 
      IS



      l_customer_id     NUMBER;
      l_site_id         NUMBER;
      l_org_id          NUMBER;
      l_msg             VARCHAR2 (240);                    --VARCHAR2 (32000);
      l_date            DATE := SYSDATE;
      l_user_id         NUMBER := NVL (fnd_profile.VALUE ('USER_ID'), -1);
      l_prod_att        VARCHAR2 (100);
      l_vendor_id       NUMBER;
      l_desc            VARCHAR2 (240);
      l_list_price      NUMBER;
      l_average_cost    NUMBER;
      l_selling_price   NUMBER;
      l_gross_margin    NUMBER;
      l_price_type      VARCHAR2(30) := i_price_type; --Parameter Passed By User
      l_incomp_group    VARCHAR2(30):= i_incomp_group; --Parameter Pass by User
      l_quote_number    NUMBER := i_quotenumber ; --Parameter PAssed By User
      l_modifier_value  NUMBER;
      l_agreement_type  VARCHAR2(30); --CSP or VQN
      l_vq_number       VARCHAR2(30) := i_vq_number; --Parameter PAssed By User
      l_inventory_item_id  NUMBER ;
      l_unit_list_price   NUMBER ;
      l_unit_selling_price NUMBER ;
      l_unit_cost        NUMBER ;
      l_atrribute5       VARCHAR2(240);
      l_attribute13      VARCHAR2(240);
      l_agreement_id     NUMBER;
      l_vendor_number    NUMBER := i_vendor_number ; 
      l_cnt_agreement_id NUMBER;
      l_cnt_agree_id     NUMBER;
      l_exception        EXCEPTION ; 
      l_agreement_line_id NUMBER ;
      l_start_date       DATE := SYSDATE;
      l_int_flag         VARCHAR2(30);
      l_start_date2      DATE ; 
      
    
      
      CURSOR QUOTELINES IS
      --select 
      --inventory_item_id
      --from XXWC.xxwc_om_quote_lines 
      --where quote_number = l_quote_number ; 
      select 
      a.inventory_item_id
      from XXWC.xxwc_om_quote_lines a
      where a.quote_number = l_quote_number
      and a.gm_selling_price = ( Select Min(gm_selling_price) 
                                 from XXWC.xxwc_om_quote_lines a2
                                 Where a2.quote_number = a.quote_number 
                                 and   a2.Inventory_Item_ID = a.Inventory_Item_ID)
                                 
      and line_seq = (Select min(a3.line_seq)                           
                                 from XXWC.xxwc_om_quote_lines a3
                                 Where a3.quote_number = a.quote_number 
                                 and   a3.Inventory_Item_ID = a.Inventory_Item_ID);
                    
 
   BEGIN
         l_customer_id := NULL;
         l_site_id := NULL;
         l_org_id := NULL;
         l_msg := NULL;
         l_start_date := FND_DATE.CANONICAL_TO_DATE(i_start_date); --Change the p_date parameter from a varchar2 to a date 

         BEGIN
           SELECT 
           cust_account_id,
           site_use_id,
           organization_id
           INTO
           l_customer_id,
           l_site_id,
           l_org_id
           FROM XXWC.xxwc_om_quote_headers
           WHERE quote_number = l_quote_number ; 
        EXCEPTION 
        WHEN OTHERS THEN
        L_MSG := SUBSTR('Cannot find customer ship to Info' || SQLCODE || SQLERRM,1,240) ;
        RAISE L_EXCEPTION ; 
        END ; 
        
            FND_FILE.Put_Line(FND_FILE.Log,'l_quote_number = '||l_quote_number);
            FND_FILE.Put_Line(FND_FILE.Log,'l_customer_id = '||l_customer_id); 
            

         
         
         IF i_price_type = 'MASTER'
           THEN 
           Begin
             select count(agreement_id)
             INTO l_cnt_agreement_id
             FROM XXWC.xxwc_om_contract_pricing_hdr
             WHERE customer_id = l_customer_id 
             AND price_type = 'MASTER' ; 
           EXCEPTION
            WHEN OTHERS THEN
              L_MSG := SUBSTR('Agreement Count Returned NULL' || SQLCODE || SQLERRM,1,240) ;
           End;
         ELSIF i_price_type = 'SHIPTO'
           THEN 
           Begin
             select count(agreement_id)
             INTO l_cnt_agreement_id
             FROM XXWC.xxwc_om_contract_pricing_hdr
             WHERE customer_id = l_customer_id
             AND customer_site_id = l_site_id 
             AND price_type = 'SHIPTO' ; 
           EXCEPTION
            WHEN OTHERS THEN
              L_MSG := SUBSTR('Agreement Count Returned NULL' || SQLCODE || SQLERRM,1,240) ;
           END;
         ELSE l_cnt_agreement_id := null ;
        END IF;
        
        FND_FILE.Put_Line(FND_FILE.Log,'Agreement ID Count = '||l_cnt_agreement_id); 
        
        /*The following will execute if an existing CSP Agreement is not found for the Customer or Customer/Job Site Combo*/
        IF l_cnt_agreement_id = 0
        THEN        
         IF l_price_type = 'MASTER'
         THEN l_site_id := NULL ; 
         END IF ; 
      
            /*Create CSP Header*/
            INSERT INTO xxwc_om_contract_pricing_hdr (agreement_id
                                                    , price_type
                                                    , customer_id
                                                    , customer_site_id
                                                    , agreement_status
                                                    , revision_number
                                                    , organization_id
                                                    , gross_margin
                                                    , incompatability_group
                                                    , creation_date
                                                    , created_by
                                                    , last_update_date
                                                    , last_updated_by
                                                    , last_update_login
                                                    , attribute10)
                 VALUES (
                           XXWC_OM_CONTRACT_PRICING_HDR_S.NEXTVAL
                         , l_price_type
                         , l_customer_id
                         , l_site_id
                         , 'DRAFT'
                         , 0
                         , l_org_id
                         , 0
                         , l_incomp_group
                         , l_date
                         , l_user_id
                         , l_date
                         , l_user_id
                         , 0
                         , l_quote_number);
       
      COMMIT;  
      
      /*Create CSP Lines From Quote*/
        FOR c1_rec in QUOTELINES
        LOOP
        
           l_agreement_id :=  NULL ; 
           l_agreement_type :=  NULL ; 
           l_prod_att :=  NULL ;
           l_desc :=  NULL ;
           l_unit_list_price  :=  NULL ;
           l_attribute13 :=  NULL ;
           l_modifier_value :=  NULL ;
           l_atrribute5  :=  NULL ;
           l_unit_cost :=  NULL ;
           l_gross_margin :=  NULL ;
           l_unit_selling_price :=  NULL ;
           l_int_flag := NULL ; 
             
       
       BEGIN
        select XXWC_OM_CONTRACT_PRICING_HDR_S.CURRVAL
        INTO l_agreement_id
        from dual;
                EXCEPTION
        WHEN OTHERS THEN 
          L_MSG := SUBSTR('AGREEMENT_ID NOT FOUND IN ATTRIBUTE10' || SQLCODE || SQLERRM,1,240) ;
        END;  
        
        FND_FILE.Put_Line(FND_FILE.Log,'l_customer_id = '||l_agreement_id); 
        
        /*Select quote line infomation for CSP Line Creation and calcutions*/
        BEGIN

          SELECT
          inventory_item_id,
          list_price,
          gm_selling_price,
          average_cost,
          special_cost --Special Cost
          INTO
          l_inventory_item_id,
          l_unit_list_price,
          l_unit_selling_price,
          l_unit_cost,
          l_atrribute5 --Special Cost
          FROM XXWC.xxwc_om_quote_lines a
          WHERE inventory_item_id = c1_rec.inventory_item_id 
          and quote_number = l_quote_number 
                and a.gm_selling_price = ( Select Min(gm_selling_price) 
                                 from XXWC.xxwc_om_quote_lines a2
                                 Where a2.quote_number = a.quote_number 
                                 and   a2.Inventory_Item_ID = a.Inventory_Item_ID)
                                 
                                       and line_seq = (Select min(a3.line_seq)                           
                                 from XXWC.xxwc_om_quote_lines a3
                                 Where a3.quote_number = a.quote_number 
                                 and   a3.Inventory_Item_ID = a.Inventory_Item_ID);
                                 
                                 
        EXCEPTION
        WHEN OTHERS THEN
          L_MSG := SUBSTR('QUOTE LINE INFORMATION NOT FOUND' || SQLCODE || SQLERRM,1,240) ;
        END ;  

 


        --Set Item and Description Variables
          IF l_atrribute5 is not null
          THEN 
            l_agreement_type := 'VQN' ; 
            l_vendor_number := i_vendor_number ;
            l_vq_number := i_vq_number ;
          ELSE 
            l_agreement_type := 'CSP' ; 
            l_vendor_number := NULL ; 
            l_vq_number := NULL ; 
          END IF;
                   
         BEGIN
            SELECT segment1, description
              INTO l_prod_att, l_desc
              FROM mtl_system_items
             WHERE inventory_item_id = l_inventory_item_id
             AND organization_id = 222; 
         EXCEPTION
            WHEN OTHERS
            THEN
                         L_MSG := SUBSTR('MISSING ITEM' || SQLCODE || SQLERRM,1,240) ;

         END;




         /*Calculate Modifier Value based on Order_Line.ATTRIBUTE13 Selection
            IF l_attribute13 = 'New Price'
            THEN
               l_modifier_value :=  ROUND(l_unit_selling_price,5);
            ELSIF l_attribute13 = 'Percent'
            THEN
               l_modifier_value :=
                  ROUND(((l_unit_list_price - l_unit_selling_price) / l_unit_list_price)*100,5);
            ELSIF l_attribute13 = 'Amount'
            THEN
               l_modifier_value := ROUND(l_unit_list_price - l_unit_selling_price,5);
            ELSIF l_attribute13 = 'Cost Plus'
            THEN
               l_modifier_value := ROUND 
                       (( l_unit_selling_price - l_unit_cost)
                     / l_unit_selling_price
                     * 100
                   , 5);
            ELSE l_modifier_value :=  ROUND(l_unit_selling_price,5);
            END IF;
            
            IF l_attribute13 is null
            THEN l_attribute13 := 'New Price' ; 
            END IF ; */
            
        /*Calculate Gross Margin for CSP Lines, IF ATRIBUTER5 IS NULL*/
            IF     l_unit_selling_price IS NOT NULL
               AND l_unit_cost  IS NOT NULL
               AND l_atrribute5  IS NULL --Attribute5 Represents Special Cost
            THEN
               l_gross_margin :=
                  ROUND (
                       (l_unit_selling_price - l_unit_cost)
                     / l_unit_selling_price
                     * 100
                   , 2);
            END IF;

         /*Calculate Gross Margin for VQN Lines, IF ATRIBUTER5 IS NOT NULL*/
            IF     l_unit_selling_price IS NOT NULL
               AND l_unit_cost IS NOT NULL
               AND l_atrribute5  IS NOT NULL --Attribute5 Represents Special Cost
            THEN
               l_gross_margin :=
                  ROUND (
                       (l_unit_selling_price - l_atrribute5 )
                     / l_unit_selling_price
                     * 100
                   , 2);
            END IF; 


           INSERT INTO xxwc_om_contract_pricing_lines (AGREEMENT_LINE_ID
                                                      , AGREEMENT_ID
                                                      , AGREEMENT_TYPE
                                                      , VENDOR_QUOTE_NUMBER
                                                      , PRODUCT_ATTRIBUTE
                                                      , PRODUCT_VALUE
                                                      , PRODUCT_DESCRIPTION
                                                      , LIST_PRICE
                                                      , MODIFIER_TYPE
                                                      , MODIFIER_VALUE
                                                      , SPECIAL_COST
                                                      , VENDOR_ID
                                                      , AVERAGE_COST
                                                      , GROSS_MARGIN
                                                      , LINE_STATUS
                                                      , REVISION_NUMBER
                                                      , CREATION_DATE
                                                      , CREATED_BY
                                                      , LAST_UPDATE_DATE
                                                      , LAST_UPDATED_BY
                                                      , LAST_UPDATE_LOGIN
                                                      , INTERFACED_FLAG
                                                      , SELLING_PRICE
                                                      , LATEST_REC_FLAG
                                                      , START_DATE)
                 VALUES (XXWC_OM_CONTRACT_PRICING_LIN_S.NEXTVAL
                       , l_agreement_id
                       , l_agreement_type
                       , l_vq_number
                       , 'ITEM'
                       , l_prod_att
                       , l_desc
                       , l_unit_list_price 
                       , 'New Price'
                       , ROUND(l_unit_selling_price,5) /*tms: 20140206-00080*/ /*TMS: 20131013-00045 Multi Decimal Rounding, Toby Rave*/
                       , l_atrribute5 
                       , l_vendor_number 
                       , ROUND(l_unit_cost,5) 
                       , l_gross_margin
                       , 'DRAFT'
                       , 0
                       , l_date
                       , l_user_id
                       , l_date
                       , l_user_id
                       , 0
                       , 'N'
                       , l_unit_selling_price
                       , 'Y'
                       , greatest(l_start_date,SYSDATE)
                       );


      END LOOP;
      
    COMMIT;
    
    /*If an existing CSP Agreement is found, only the lines are updated*/
      ELSIF l_cnt_agreement_id = 1 
      THEN  
        IF i_price_type = 'MASTER'
         THEN 
          BEGIN
           SELECT agreement_id
           INTO l_agreement_id
           FROM XXWC.xxwc_om_contract_pricing_hdr
           WHERE customer_id = l_customer_id 
           AND price_type = 'MASTER' ;
         EXCEPTION
            WHEN OTHERS THEN
              L_MSG := SUBSTR('NO AGREEMENT FOR CUSTOMER' || SQLCODE || SQLERRM,1,240) ;
           END;           
           
         ELSIF i_price_type = 'SHIPTO'
           THEN 
            BEGIN
             SELECT agreement_id
             INTO l_agreement_id
             FROM XXWC.xxwc_om_contract_pricing_hdr
             WHERE customer_id = l_customer_id
             AND customer_site_id = l_site_id 
             AND price_type = 'SHIPTO' ; 
           EXCEPTION
            WHEN OTHERS THEN
              L_MSG := SUBSTR('NO AGREEMENT FOR CUSTOMER' || SQLCODE || SQLERRM,1,240) ;
           END;   
         ELSE l_agreement_id := null ;
 
         END IF ; 
         
         FOR C1_REC in QUOTELINES
         LOOP
         
        
          SELECT
          inventory_item_id,
          list_price,
          gm_selling_price,
          average_cost,
          special_cost --Special Cost
          INTO
          l_inventory_item_id,
          l_unit_list_price,
          l_unit_selling_price,
          l_unit_cost,
          l_atrribute5 --Special Cost
          FROM XXWC.xxwc_om_quote_lines a
          WHERE inventory_item_id = c1_rec.inventory_item_id 
          and quote_number = l_quote_number 
          and a.gm_selling_price = ( Select Min(gm_selling_price) 
                                 from XXWC.xxwc_om_quote_lines a2
                                 Where a2.quote_number = a.quote_number 
                                 and   a2.Inventory_Item_ID = a.Inventory_Item_ID)
                                 
                                       and line_seq = (Select min(a3.line_seq)                           
                                 from XXWC.xxwc_om_quote_lines a3
                                 Where a3.quote_number = a.quote_number 
                                 and   a3.Inventory_Item_ID = a.Inventory_Item_ID);
          
      --Set Item and Description Variables
          IF l_atrribute5 is not null
          THEN 
            l_agreement_type := 'VQN' ; 
            l_vendor_number := i_vendor_number ;
            l_vq_number := i_vq_number ; 
          ELSE 
            l_agreement_type := 'CSP' ; 
            l_vendor_number := NULL ;  
            l_vq_number := NULL ; 
          END IF;      
          
           BEGIN
            SELECT segment1, description
              INTO l_prod_att, l_desc
              FROM mtl_system_items
             WHERE inventory_item_id = l_inventory_item_id
             AND organization_id = 222; 
         EXCEPTION
            WHEN OTHERS
            THEN
               l_msg := l_msg || ':' || 'Missing Item';
         END;
          
        --determine if an Active Agreement Line exists for the SKU  
          BEGIN
            select count(agreement_line_id)
            into l_cnt_agree_id
            from XXWC.xxwc_om_contract_pricing_lines
            where product_value = l_prod_att
            and latest_rec_flag = 'Y' 
            and agreement_id = l_agreement_id ; 
          EXCEPTION
            WHEN OTHERS
            THEN 
            L_MSG := SUBSTR('AGREEMENT DOES NOT EXIST' || SQLCODE || SQLERRM,1,240) ;
            END ;
     
     --If not agreement lines exist for the item then a new records will be created (l_cnt_agree_id = 0)
     IF l_cnt_agree_id = 0
     THEN
                 
--Calculate Gross Margin for CSP Lines, IF ATRIBUTER5 IS NULL
            IF     l_unit_selling_price IS NOT NULL
               AND l_unit_cost  IS NOT NULL
               AND l_atrribute5  IS NULL --Attribute5 Represents Special Cost
            THEN
               l_gross_margin :=
                  ROUND (
                       (l_unit_selling_price - l_unit_cost)
                     / l_unit_selling_price
                     * 100
                   , 2);
            END IF;

--Calculate Gross Margin for VQN Lines, IF ATRIBUTER5 IS NOT NULL
            IF     l_unit_selling_price IS NOT NULL
               AND l_unit_cost IS NOT NULL
               AND l_atrribute5  IS NOT NULL --Attribute5 Represents Special Cost
            THEN
               l_gross_margin :=
                  ROUND (
                       (l_unit_selling_price - l_atrribute5 )
                     / l_unit_selling_price
                     * 100
                   , 2);
            END IF; 


           INSERT INTO xxwc_om_contract_pricing_lines (AGREEMENT_LINE_ID
                                                      , AGREEMENT_ID
                                                      , AGREEMENT_TYPE
                                                      , VENDOR_QUOTE_NUMBER
                                                      , PRODUCT_ATTRIBUTE
                                                      , PRODUCT_VALUE
                                                      , PRODUCT_DESCRIPTION
                                                      , LIST_PRICE
                                                      , MODIFIER_TYPE
                                                      , MODIFIER_VALUE
                                                      , SPECIAL_COST
                                                      , VENDOR_ID
                                                      , AVERAGE_COST
                                                      , GROSS_MARGIN
                                                      , LINE_STATUS
                                                      , REVISION_NUMBER
                                                      , CREATION_DATE
                                                      , CREATED_BY
                                                      , LAST_UPDATE_DATE
                                                      , LAST_UPDATED_BY
                                                      , LAST_UPDATE_LOGIN
                                                      , INTERFACED_FLAG
                                                      , SELLING_PRICE
                                                      , LATEST_REC_FLAG
                                                      , START_DATE)
                 VALUES (XXWC_OM_CONTRACT_PRICING_LIN_S.NEXTVAL
                       , l_agreement_id
                       , l_agreement_type
                       , l_vq_number
                       , 'ITEM'
                       , l_prod_att
                       , l_desc
                       , l_unit_list_price 
                       , 'New Price'
                       , ROUND(l_unit_selling_price,5) /* TMS: 20140206-00080 */ /*TMS: 20131013-00045 Multi Decimal Rounding, Toby Rave*/
                       , l_atrribute5 
                       , l_vendor_number 
                       , ROUND(l_unit_cost,5) 
                       , l_gross_margin
                       , 'DRAFT'
                       , 0
                       , l_date
                       , l_user_id
                       , l_date
                       , l_user_id
                       , 0
                       , 'N'
                       , l_unit_selling_price
                       , 'Y'
                       , greatest(l_start_date,SYSDATE)
                       );
                       
      /*CSP Line already exists*/  
         ELSIF l_cnt_agree_id = 1
         THEN 
         
            BEGIN
              select agreement_line_id
              into l_agreement_line_id
              from XXWC.xxwc_om_contract_pricing_lines
              where product_value = l_prod_att
              and latest_rec_flag = 'Y' 
              and agreement_id = l_agreement_id; 
          EXCEPTION
            WHEN OTHERS
            THEN 
            l_msg := l_msg || ':' || 'Missing Agreement Line ID';
            END ;
            
            BEGIN 
            SELECT interfaced_flag, start_date
            into l_int_flag, l_start_date2
            from xxwc.xxwc_om_contract_pricing_lines
            where agreement_line_id = l_agreement_line_id ; 
            EXCEPTION
            WHEN OTHERS
            THEN 
            l_msg := l_agreement_line_id || ':' || 'Duplicate row exists for item_id';
            END;
            
            If l_int_flag = 'Y' and trunc(l_start_date2) < trunc(SYSDATE)
            THEN
            
            UPDATE XXWC.xxwc_om_contract_pricing_lines
            SET END_DATE = SYSDATE-1, 
            interfaced_flag = 'N', 
            latest_rec_flag = 'N',
            line_status = 'DRAFT'
            WHERE agreement_line_id = l_agreement_line_id ; 
            

            
--Calculate Gross Margin for CSP Lines, IF ATRIBUTER5 IS NULL
            IF     l_unit_selling_price IS NOT NULL
               AND l_unit_cost  IS NOT NULL
               AND l_atrribute5  IS NULL --Attribute5 Represents Special Cost
            THEN
               l_gross_margin :=
                  ROUND (
                       (l_unit_selling_price - l_unit_cost)
                     / l_unit_selling_price
                     * 100
                   , 2);
            END IF;

--Calculate Gross Margin for VQN Lines, IF ATRIBUTER5 IS NOT NULL
            IF     l_unit_selling_price IS NOT NULL
               AND l_unit_cost IS NOT NULL
               AND l_atrribute5  IS NOT NULL --Attribute5 Represents Special Cost
            THEN
               l_gross_margin :=
                  ROUND (
                       (l_unit_selling_price - l_atrribute5 )
                     / l_unit_selling_price
                     * 100
                   , 2);
            END IF; 


           INSERT INTO xxwc_om_contract_pricing_lines (AGREEMENT_LINE_ID
                                                      , AGREEMENT_ID
                                                      , AGREEMENT_TYPE
                                                      , VENDOR_QUOTE_NUMBER
                                                      , PRODUCT_ATTRIBUTE
                                                      , PRODUCT_VALUE
                                                      , PRODUCT_DESCRIPTION
                                                      , LIST_PRICE
                                                      , START_DATE
                                                      , MODIFIER_TYPE
                                                      , MODIFIER_VALUE
                                                      , SPECIAL_COST
                                                      , VENDOR_ID
                                                      , AVERAGE_COST
                                                      , GROSS_MARGIN
                                                      , LINE_STATUS
                                                      , REVISION_NUMBER
                                                      , CREATION_DATE
                                                      , CREATED_BY
                                                      , LAST_UPDATE_DATE
                                                      , LAST_UPDATED_BY
                                                      , LAST_UPDATE_LOGIN
                                                      , INTERFACED_FLAG
                                                      , SELLING_PRICE
                                                      , LATEST_REC_FLAG)
                 VALUES (XXWC_OM_CONTRACT_PRICING_LIN_S.NEXTVAL
                       , l_agreement_id
                       , l_agreement_type
                       , l_vq_number
                       , 'ITEM'
                       , l_prod_att
                       , l_desc
                       , l_unit_list_price 
                       , greatest(l_start_date,SYSDATE)
                       , 'New Price'
                       , ROUND(l_unit_selling_price,5) /* TMS 20140206-00080 */ /*TMS: 20131013-00045 Multi Decimal Rounding, Toby Rave*/
                       , l_atrribute5 
                       , l_vendor_number 
                       , ROUND(l_unit_cost,5) 
                       , l_gross_margin
                       , 'DRAFT'
                       , 0
                       , l_date
                       , l_user_id
                       , l_date
                       , l_user_id
                       , 0
                       , 'N'
                       , l_unit_selling_price
                       , 'Y'
                       ); 
          ELSE l_int_flag := NULL ; 
          END IF ; 
         ELSE l_cnt_agree_id  := NULL ;
         END IF ; 
         END LOOP ;  
      COMMIT ;  
         
    END IF ; 
  END csp_cust_quote ;   
  
 /*************************************************************************
*   PROCEDURE Name: csp_end_date
*
*   PURPOSE:   csp_quote conversion procedure is used for loading converted 
*              records from the xxwc.xxwc_csp_interface tables into the 
*              CSP Maintenance tables (xxwc.xxwc_om_contractpricing_hdr, 
*              xxwc_xxwc.om_contract_pricing_lines)
****************************************************************************/ 
PROCEDURE csp_end_date (errbuf OUT VARCHAR2, retcode OUT VARCHAR2)
   IS
   
    l_control_rec QP_GLOBALS.Control_Rec_Type;
    l_return_status VARCHAR2(1);
    x_msg_count number;
    x_msg_data Varchar2(2000);
    x_msg_index number;
    
    l_MODIFIER_LIST_rec QP_Modifiers_PUB.Modifier_List_Rec_Type;
    l_MODIFIER_LIST_val_rec QP_Modifiers_PUB.Modifier_List_Val_Rec_Type;
    l_MODIFIERS_tbl QP_Modifiers_PUB.Modifiers_Tbl_Type;
    n_MODIFIERS_tbl QP_Modifiers_PUB.Modifiers_Tbl_Type;
    l_MODIFIERS_val_tbl QP_Modifiers_PUB.Modifiers_Val_Tbl_Type;
    l_QUALIFIERS_tbl QP_Qualifier_Rules_PUB.Qualifiers_Tbl_Type;
    l_QUALIFIERS_val_tbl QP_Qualifier_Rules_PUB.Qualifiers_Val_Tbl_Type;
    l_PRICING_ATTR_tbl QP_Modifiers_PUB.Pricing_Attr_Tbl_Type;
    n_PRICING_ATTR_tbl QP_Modifiers_PUB.Pricing_Attr_Tbl_Type;
    l_PRICING_ATTR_val_tbl QP_Modifiers_PUB.Pricing_Attr_Val_Tbl_Type;
    
    l_x_MODIFIER_LIST_rec QP_Modifiers_PUB.Modifier_List_Rec_Type;
    l_x_MODIFIER_LIST_val_rec QP_Modifiers_PUB.Modifier_List_Val_Rec_Type;
    l_x_MODIFIERS_tbl QP_Modifiers_PUB.Modifiers_Tbl_Type;
    l_x_MODIFIERS_val_tbl QP_Modifiers_PUB.Modifiers_Val_Tbl_Type;
    l_x_QUALIFIERS_tbl QP_Qualifier_Rules_PUB.Qualifiers_Tbl_Type;
    l_x_QUALIFIERS_val_tbl QP_Qualifier_Rules_PUB.Qualifiers_Val_Tbl_Type;
    l_x_PRICING_ATTR_tbl QP_Modifiers_PUB.Pricing_Attr_Tbl_Type;
    l_x_PRICING_ATTR_val_tbl QP_Modifiers_PUB.Pricing_Attr_Val_Tbl_Type;
      
      
      l_customer_id     NUMBER;
      l_site_id         NUMBER;
      l_org_id          NUMBER;
      l_msg             VARCHAR2 (240);                    --VARCHAR2 (32000);
      l_conv_flag       VARCHAR2 (1) := 'Y';
      l_date            DATE := SYSDATE;
      l_user_id         NUMBER := NVL (fnd_profile.VALUE ('USER_ID'), -1);
      l_prod_att        VARCHAR2 (100);
      l_vendor_id       NUMBER;
      l_desc            VARCHAR2 (240);
      l_list_price      NUMBER;
      l_average_cost    NUMBER;
      l_selling_price   NUMBER;
      l_gross_margin    NUMBER;
      i                 NUMBER := 0;
      l_count           NUMBER := NULL ; 
      l_agreement_line_id NUMBER ; 
      l_list_line_id NUMBER ; 


      CURSOR c1
      IS
         SELECT c.agreement_line_id
           FROM XXWC_CSP_LINES_INTERFACE a, 
            xxwc_om_contract_pricing_lines c
          WHERE NVL (process_flag, 'N') <> 'Y'
                AND a.reference_id = c.agreement_id
                AND c.latest_rec_flag = 'Y' 
                and a.product_value = c.product_value ; 
              
              
   BEGIN
      -- set the status to not processed for duplicate reference
    

FOR c1_rec IN c1
LOOP

  l_modifiers_tbl := n_modifiers_tbl;
  l_PRICING_ATTR_tbl := n_PRICING_ATTR_tbl;
  l_vendor_id         := NULL;
  l_prod_att          := NULL;
  l_desc              := NULL;
  l_list_price        := NULL;
  l_average_cost      := NULL;
  l_selling_price     := NULL;
  l_gross_margin      := NULL;
  l_msg               := NULL;
  l_conv_flag         := 'Y';
  i                   := i + 1;
  l_count             := NULL ;
  l_agreement_line_id := NULL ;
  l_list_line_id      := NULL ; 
 

  UPDATE XXWC.xxwc_om_contract_pricing_lines
  SET END_DATE            = SYSDATE-1,
    interfaced_flag       = 'N',
    latest_rec_flag       = 'N',
    line_status           = 'DRAFT'
  WHERE agreement_line_id = c1_rec.agreement_line_id ;
  
  select list_line_id 
  into l_list_line_id
  from apps.qp_list_lines
  where attribute2 = c1_rec.agreement_line_id ; 
  
            /*  Populate API Variables for Modifier Line Level Updates */
            l_MODIFIERS_tbl(1).list_line_id := l_list_line_id ;
            --l_MODIFIERS_tbl(i).start_date_active := v_ln_start_date ;
            l_MODIFIERS_tbl(1).end_date_active := SYSDATE-1 ; 
            --l_MODIFIERS_tbl(i).arithmetic_operator := v_arithmetic_operator ;
            --l_MODIFIERS_tbl(i).operand := v_ln_mod_val ;
            --l_MODIFIERS_tbl(i).price_by_formula_id := v_generate_using_formula_id ;
            l_MODIFIERS_tbl(1).operation := QP_GLOBALS.G_OPR_UPDATE; 
          
      /*  API Call */
        QP_Modifiers_PUB.Process_Modifiers
        ( p_api_version_number => 1.0
        , p_init_msg_list => FND_API.G_FALSE
        , p_return_values => FND_API.G_FALSE
        , p_commit => FND_API.G_FALSE
        , x_return_status => l_return_status
        , x_msg_count =>x_msg_count
        , x_msg_data =>x_msg_data
        ,p_MODIFIER_LIST_rec => l_MODIFIER_LIST_rec
        ,p_MODIFIERS_tbl => l_MODIFIERS_tbl
        ,p_QUALIFIERS_tbl => l_QUALIFIERS_tbl
        ,p_PRICING_ATTR_tbl => l_PRICING_ATTR_tbl
        ,x_MODIFIER_LIST_rec => l_x_MODIFIER_LIST_rec         
        ,x_MODIFIER_LIST_val_rec => l_x_MODIFIER_LIST_val_rec 
        ,x_MODIFIERS_tbl => l_x_MODIFIERS_tbl                 
        ,x_MODIFIERS_val_tbl => l_x_MODIFIERS_val_tbl         
        ,x_QUALIFIERS_tbl => l_x_QUALIFIERS_tbl               
        ,x_QUALIFIERS_val_tbl => l_x_QUALIFIERS_val_tbl       
        ,x_PRICING_ATTR_tbl => l_x_PRICING_ATTR_tbl           
        ,x_PRICING_ATTR_val_tbl => l_x_PRICING_ATTR_val_tbl   
        );
        
  COMMIT ; 
  END LOOP ;  
END csp_end_date;  

/*************************************************************************
*   PROCEDURE Name: csp_delta
*
*   PURPOSE:   csp_quote conversion procedure is used for loading converted 
*              records from the xxwc.xxwc_csp_interface tables into the 
*              CSP Maintenance tables (xxwc.xxwc_om_contractpricing_hdr, 
*              xxwc_xxwc.om_contract_pricing_lines)
****************************************************************************/ 
PROCEDURE csp_delta (errbuf OUT VARCHAR2, retcode OUT VARCHAR2)
   IS
      --DECLARE
      l_customer_id     NUMBER;
      l_site_id         NUMBER;
      l_org_id          NUMBER;
      l_msg             VARCHAR2 (240);                    --VARCHAR2 (32000);
      l_conv_flag       VARCHAR2 (1) := 'Y';
      l_date            DATE := SYSDATE;
      l_user_id         NUMBER := NVL (fnd_profile.VALUE ('USER_ID'), -1);
      l_prod_att        VARCHAR2 (100);
      l_vendor_id       NUMBER;
      l_desc            VARCHAR2 (240);
      l_list_price      NUMBER;
      l_average_cost    NUMBER;
      l_selling_price   NUMBER;
      l_gross_margin    NUMBER;
      i                 NUMBER := 0;
      l_count           NUMBER := NULL ; 
      l_agreement_line_id NUMBER ; 


      
      CURSOR c2
      IS
      
         SELECT a.ROWID
              , a.*
              , b.agreement_id
              , b.organization_id
           FROM XXWC_CSP_LINES_INTERFACE a,
           xxwc_om_contract_pricing_hdr b
          WHERE NVL (process_flag, 'N') <> 'Y' 
                AND a.reference_id = b.agreement_id ;

   BEGIN
      -- set the status to not processed for duplicate reference
    

      
      FOR c2_rec IN c2
      LOOP
         l_vendor_id := NULL;
         l_prod_att := NULL;
         l_desc := NULL;
         l_list_price := NULL;
         l_average_cost := NULL;
         l_selling_price := NULL;
         l_gross_margin := NULL;
         l_msg := NULL;
         l_conv_flag := 'Y';
         i := i + 1;
         l_count := NULL ;
         l_agreement_line_id := NULL ; 
         
         BEGIN
            SELECT segment1, description
              INTO l_prod_att, l_desc
              FROM mtl_system_items
             WHERE segment1 = c2_rec.product_value AND organization_id = 222; --c2_rec.organization_id;
         EXCEPTION
            WHEN OTHERS
            THEN
               l_msg := l_msg || ':' || 'Missing Item';
               l_conv_flag := 'E';
         END;
         
         BEGIN
          SELECT count(1)
          into l_count
          from xxwc.xxwc_om_contract_pricing_lines
          where latest_rec_flag = 'Y'
          and agreement_id = c2_rec.agreement_id
          and product_value = c2_rec.product_value ; 
         END ;   
         
         IF l_count = 0
         THEN

         IF c2_rec.agreement_type = 'VQN'
         THEN
            BEGIN
               SELECT vendor_id
                 INTO l_vendor_id
                 FROM ap_suppliers
                WHERE segment1 = c2_rec.vendor_number;
            EXCEPTION
               WHEN OTHERS
               THEN
                  l_msg := l_msg || ':' || 'Missing Vendor';
                  l_conv_flag := 'E';
            END;
         END IF;


         IF c2_rec.product_attribute = 'ITEM'
         THEN
            BEGIN
               SELECT ll.operand
                 INTO l_list_price
                 FROM qp_secu_list_headers_v lh
                    , xxwc.xxwc_qp_list_lines_mv ll   --, qp_list_lines_v ll
                    , mtl_system_items a
                WHERE     lh.list_type_code = 'PRL'
                      AND lh.name LIKE ('CATEGORY %')
                      AND lh.name <> 'CATEGORY 7 - OTHER ITEMS'
                      AND lh.list_header_id = ll.list_header_id
                      AND SYSDATE BETWEEN NVL (ll.start_date_active
                                             , SYSDATE - 1)
                                      AND NVL (ll.end_date_active
                                             , SYSDATE + 1)
                      AND ll.product_id = a.inventory_item_id
                      AND a.organization_id = 222     --c2_rec.organization_id
                      AND a.segment1 = c2_rec.PRODUCT_VALUE
                      AND ROWNUM = 1;
            EXCEPTION
               WHEN OTHERS
               THEN
                  l_msg := l_msg || ':' || 'Missing List Price';
                  l_conv_flag := 'E';
            END;


            BEGIN
               SELECT ROUND (
                         cst_cost_api.get_item_cost (1
                                                   , inventory_item_id
                                                   , c2_rec.organization_id)
                       , 5)
                 INTO l_average_cost
                 FROM mtl_system_items
                WHERE segment1 = c2_rec.PRODUCT_VALUE
                      AND organization_id = 222;     --c2_rec.organization_id;
            EXCEPTION
               WHEN OTHERS
               THEN
                  l_msg := l_msg || ':' || 'Missing Avg Cost';
                  l_conv_flag := 'E';
            END;

            IF c2_rec.app_method = 'New Price'
            THEN
               l_selling_price := c2_rec.modifier_value;
            ELSIF c2_rec.app_method = 'Percent'
            THEN
               l_selling_price :=
                  l_list_price * (100 - c2_rec.modifier_value) / 100;
            ELSIF c2_rec.app_method = 'Amount'
            THEN
               l_selling_price := l_list_price - c2_rec.modifier_value;
            ELSIF c2_rec.app_method = 'Cost Plus'
            THEN
               l_selling_price := l_average_cost + c2_rec.modifier_value;
            END IF;

            IF     l_selling_price IS NOT NULL
               AND l_average_cost IS NOT NULL
               AND c2_rec.agreement_type = 'CSP'
            THEN
               l_gross_margin :=
                  ROUND (
                       (l_selling_price - l_average_cost)
                     / l_selling_price
                     * 100
                   , 2);
            END IF;

            IF     l_selling_price IS NOT NULL
               AND c2_rec.special_cost IS NOT NULL
               AND c2_rec.agreement_type = 'VQN'
            THEN
               l_gross_margin :=
                  ROUND (
                       (l_selling_price - c2_rec.special_cost)
                     / l_selling_price
                     * 100
                   , 2);
            END IF;
         END IF;

         IF l_conv_flag = 'Y'
         THEN
            INSERT INTO xxwc_om_contract_pricing_lines (AGREEMENT_LINE_ID
                                                      , AGREEMENT_ID
                                                      , AGREEMENT_TYPE
                                                      , VENDOR_QUOTE_NUMBER
                                                      , PRODUCT_ATTRIBUTE
                                                      , PRODUCT_VALUE
                                                      , PRODUCT_DESCRIPTION
                                                      , LIST_PRICE
                                                      , START_DATE
                                                      , END_DATE
                                                      , MODIFIER_TYPE
                                                      , MODIFIER_VALUE
                                                      , SPECIAL_COST
                                                      , VENDOR_ID
                                                      , AVERAGE_COST
                                                      , GROSS_MARGIN
                                                      , LINE_STATUS
                                                      , REVISION_NUMBER
                                                      , ATTRIBUTE10
                                                      , ATTRIBUTE1
                                                      , CREATION_DATE
                                                      , CREATED_BY
                                                      , LAST_UPDATE_DATE
                                                      , LAST_UPDATED_BY
                                                      , LAST_UPDATE_LOGIN
                                                      , INTERFACED_FLAG
                                                      , SELLING_PRICE
                                                      , LATEST_REC_FLAG)
                 VALUES (XXWC_OM_CONTRACT_PRICING_LIN_S.NEXTVAL
                       , c2_rec.agreement_id
                       , c2_rec.agreement_type
                       , c2_rec.vq_number
                       , c2_rec.product_attribute
                       , c2_rec.product_value
                       , l_desc
                       , l_list_price
                       , SYSDATE
                       , c2_rec.end_date
                       , c2_rec.app_method
                       , c2_rec.modifier_value
                       , c2_rec.special_cost
                       , l_vendor_id
                       , l_average_cost
                       , l_gross_margin
                       , 'DELTA'
                       , 0
                       , c2_rec.reference_id
                       , 'YES'
                       , l_date
                       , l_user_id
                       , l_date
                       , l_user_id
                       , 0
                       , 'N'
                       , l_selling_price
                       , 'Y');

            UPDATE XXWC_CSP_LINES_INTERFACE
               SET process_flag = 'Y'
             WHERE ROWID = c2_rec.ROWID;
             
             IF i >= 1000 then
               commit;
               i :=0;
             END IF;  
         ELSE
            UPDATE XXWC_CSP_LINES_INTERFACE
               SET process_flag = 'E', error_message = l_msg
             WHERE ROWID = c2_rec.ROWID;
         END IF;
/*IF Count >= 1 */         
        ELSIF l_count = 1
        THEN 
        
                    BEGIN
              select agreement_line_id
              into l_agreement_line_id
              from XXWC.xxwc_om_contract_pricing_lines
              where product_value = l_prod_att
              and latest_rec_flag = 'Y' 
              and agreement_id = c2_rec.agreement_id; 
          EXCEPTION
            WHEN OTHERS
            THEN 
            l_msg := l_msg || ':' || 'Missing Agreement Line ID';
            END ;
            
            UPDATE XXWC.xxwc_om_contract_pricing_lines
            SET END_DATE = SYSDATE-1, 
            interfaced_flag = 'N', 
            latest_rec_flag = 'N',
            line_status = 'DELTA'
            WHERE agreement_line_id = l_agreement_line_id ; 
        
        
        IF c2_rec.agreement_type = 'VQN'
         THEN
            BEGIN
               SELECT vendor_id
                 INTO l_vendor_id
                 FROM ap_suppliers
                WHERE segment1 = c2_rec.vendor_number;
            EXCEPTION
               WHEN OTHERS
               THEN
                  l_msg := l_msg || ':' || 'Missing Vendor';
                  l_conv_flag := 'E';
            END;
         END IF;


         IF c2_rec.product_attribute = 'ITEM'
         THEN
            BEGIN
               SELECT ll.operand
                 INTO l_list_price
                 FROM qp_secu_list_headers_v lh
                    , xxwc.xxwc_qp_list_lines_mv ll   --, qp_list_lines_v ll
                    , mtl_system_items a
                WHERE     lh.list_type_code = 'PRL'
                      AND lh.name LIKE ('CATEGORY %')
                      AND lh.name <> 'CATEGORY 7 - OTHER ITEMS'
                      AND lh.list_header_id = ll.list_header_id
                      AND SYSDATE BETWEEN NVL (ll.start_date_active
                                             , SYSDATE - 1)
                                      AND NVL (ll.end_date_active
                                             , SYSDATE + 1)
                      AND ll.product_id = a.inventory_item_id
                      AND a.organization_id = 222     --c2_rec.organization_id
                      AND a.segment1 = c2_rec.PRODUCT_VALUE
                      AND ROWNUM = 1;
            EXCEPTION
               WHEN OTHERS
               THEN
                  l_msg := l_msg || ':' || 'Missing List Price';
                  l_conv_flag := 'E';
            END;


            BEGIN
               SELECT ROUND (
                         cst_cost_api.get_item_cost (1
                                                   , inventory_item_id
                                                   , c2_rec.organization_id)
                       , 5)
                 INTO l_average_cost
                 FROM mtl_system_items
                WHERE segment1 = c2_rec.PRODUCT_VALUE
                      AND organization_id = 222;     --c2_rec.organization_id;
            EXCEPTION
               WHEN OTHERS
               THEN
                  l_msg := l_msg || ':' || 'Missing Avg Cost';
                  l_conv_flag := 'E';
            END;

            IF c2_rec.app_method = 'New Price'
            THEN
               l_selling_price := c2_rec.modifier_value;
            ELSIF c2_rec.app_method = 'Percent'
            THEN
               l_selling_price :=
                  l_list_price * (100 - c2_rec.modifier_value) / 100;
            ELSIF c2_rec.app_method = 'Amount'
            THEN
               l_selling_price := l_list_price - c2_rec.modifier_value;
            ELSIF c2_rec.app_method = 'Cost Plus'
            THEN
               l_selling_price := l_average_cost + c2_rec.modifier_value;
            END IF;

            IF     l_selling_price IS NOT NULL
               AND l_average_cost IS NOT NULL
               AND c2_rec.agreement_type = 'CSP'
            THEN
               l_gross_margin :=
                  ROUND (
                       (l_selling_price - l_average_cost)
                     / l_selling_price
                     * 100
                   , 2);
            END IF;

            IF     l_selling_price IS NOT NULL
               AND c2_rec.special_cost IS NOT NULL
               AND c2_rec.agreement_type = 'VQN'
            THEN
               l_gross_margin :=
                  ROUND (
                       (l_selling_price - c2_rec.special_cost)
                     / l_selling_price
                     * 100
                   , 2);
            END IF;
         END IF;

         IF l_conv_flag = 'Y'
         THEN
            INSERT INTO xxwc_om_contract_pricing_lines (AGREEMENT_LINE_ID
                                                      , AGREEMENT_ID
                                                      , AGREEMENT_TYPE
                                                      , VENDOR_QUOTE_NUMBER
                                                      , PRODUCT_ATTRIBUTE
                                                      , PRODUCT_VALUE
                                                      , PRODUCT_DESCRIPTION
                                                      , LIST_PRICE
                                                      , START_DATE
                                                      , END_DATE
                                                      , MODIFIER_TYPE
                                                      , MODIFIER_VALUE
                                                      , SPECIAL_COST
                                                      , VENDOR_ID
                                                      , AVERAGE_COST
                                                      , GROSS_MARGIN
                                                      , LINE_STATUS
                                                      , REVISION_NUMBER
                                                      , ATTRIBUTE10
                                                      , ATTRIBUTE1
                                                      , CREATION_DATE
                                                      , CREATED_BY
                                                      , LAST_UPDATE_DATE
                                                      , LAST_UPDATED_BY
                                                      , LAST_UPDATE_LOGIN
                                                      , INTERFACED_FLAG
                                                      , SELLING_PRICE
                                                      , LATEST_REC_FLAG)
                 VALUES (XXWC_OM_CONTRACT_PRICING_LIN_S.NEXTVAL
                       , c2_rec.agreement_id
                       , c2_rec.agreement_type
                       , c2_rec.vq_number
                       , c2_rec.product_attribute
                       , c2_rec.product_value
                       , l_desc
                       , l_list_price
                       , SYSDATE
                       , c2_rec.end_date
                       , c2_rec.app_method
                       , c2_rec.modifier_value
                       , c2_rec.special_cost
                       , l_vendor_id
                       , l_average_cost
                       , l_gross_margin
                       , 'DELTA'
                       , 0
                       , c2_rec.reference_id
                       , 'YES'
                       , l_date
                       , l_user_id
                       , l_date
                       , l_user_id
                       , 0
                       , 'N'
                       , l_selling_price
                       , 'Y');

            UPDATE XXWC_CSP_LINES_INTERFACE
               SET process_flag = 'Y'
             WHERE ROWID = c2_rec.ROWID;
             
             IF i >= 1000 then
               commit;
               i :=0;
             END IF;  
         ELSE
            UPDATE XXWC_CSP_LINES_INTERFACE
               SET process_flag = 'E', error_message = l_msg
             WHERE ROWID = c2_rec.ROWID;
         END IF;
        END IF ;  
      END LOOP;

      COMMIT;
   END csp_delta;  


/*************************************************************************
*   PROCEDURE Name: update_delta_lines
*
*   PURPOSE:   UPDATE_LINES procedure is used to create update Lines
*              in Advanced Pricing related to the CSP Agreement Header 
*              'i_agreement_id' 
****************************************************************************/

  PROCEDURE update_delta_lines 
  IS 
    l_control_rec QP_GLOBALS.Control_Rec_Type;
    l_return_status VARCHAR2(1);
    x_msg_count number;
    x_msg_data Varchar2(2000);
    x_msg_index number;
    
    l_MODIFIER_LIST_rec QP_Modifiers_PUB.Modifier_List_Rec_Type;
    l_MODIFIER_LIST_val_rec QP_Modifiers_PUB.Modifier_List_Val_Rec_Type;
    l_MODIFIERS_tbl QP_Modifiers_PUB.Modifiers_Tbl_Type;
    l_MODIFIERS_val_tbl QP_Modifiers_PUB.Modifiers_Val_Tbl_Type;
    l_QUALIFIERS_tbl QP_Qualifier_Rules_PUB.Qualifiers_Tbl_Type;
    l_QUALIFIERS_val_tbl QP_Qualifier_Rules_PUB.Qualifiers_Val_Tbl_Type;
    l_PRICING_ATTR_tbl QP_Modifiers_PUB.Pricing_Attr_Tbl_Type;
    l_PRICING_ATTR_val_tbl QP_Modifiers_PUB.Pricing_Attr_Val_Tbl_Type;
    
    l_x_MODIFIER_LIST_rec QP_Modifiers_PUB.Modifier_List_Rec_Type;
    l_x_MODIFIER_LIST_val_rec QP_Modifiers_PUB.Modifier_List_Val_Rec_Type;
    l_x_MODIFIERS_tbl QP_Modifiers_PUB.Modifiers_Tbl_Type;
    l_x_MODIFIERS_val_tbl QP_Modifiers_PUB.Modifiers_Val_Tbl_Type;
    l_x_QUALIFIERS_tbl QP_Qualifier_Rules_PUB.Qualifiers_Tbl_Type;
    l_x_QUALIFIERS_val_tbl QP_Qualifier_Rules_PUB.Qualifiers_Val_Tbl_Type;
    l_x_PRICING_ATTR_tbl QP_Modifiers_PUB.Pricing_Attr_Tbl_Type;
    l_x_PRICING_ATTR_val_tbl QP_Modifiers_PUB.Pricing_Attr_Val_Tbl_Type;
    
    --CSP Header Level Variables
    v_agreement_id NUMBER ;
    v_agreement_type VARCHAR2(30) ;
    v_vendor_quote_number NUMBER ;
    v_price_type VARCHAR2(30);
    v_customer_id NUMBER ;
    v_customer_site_id NUMBER ;
    v_agreement_status VARCHAR2(30) ;
    v_revision_number NUMBER ;
    v_incompatability_group VARCHAR2(30);
    v_incomp_group VARCHAR2(30);
    v_attribute_10 VARCHAR2(25) :='Contract Pricing'; --Variable to populate DFF Attribute 10 on Modifier Header (Contract Pricing or Vendor Quote)
    v_attribute_11 Number := NULL ;
    v_attribute_12 VARCHAR2(25) := NULL ; --Vendor Name for VQN
    v_attribute_13 VARCHAR2(25) := NULL ; --VEndor Number for VQN
    v_attribute_14 Number ; --Mapped to Vendor Agreement Number DFF on Modifier Header
    v_attribute_15 Number ; --Mapped to Agreement Revision Number DFF on Modifier Header
    v1_price_type VARCHAR2(30);
    v_customer_name VARCHAR2(50) ;
    v_party_id Number ;
    v_party_name VARCHAR2(360) ;
    v_party_site_number Number ;
    v_qualifier_value Number ;
    v_agreeid Number ;
    v_qual_context VARCHAR2(360);
    v_excluder_flag VARCHAR2(1) ;
    v_operator_code VARCHAR(360) ;
    v_delimitier VARCHAR2(1) ;
    v_qualifier_grouping_no Number ;
    v_qualifier_precedence Number ; 
    v_operation VARCHAR2(30) ; 
    v_active_flag VARCHAR2(30) ;
    v_list_header_id NUMBER ; --v_list_header_id
    v_list_line_id NUMBER ; 
    --START XXWC_CSP_LINES VARIABLES
    v_atrribute_3 NUMBER ;
    v_ln_special_cost NUMBER ;
    v_ln_id NUMBER ; --Line Level Variable for xxwc.agreement_line_id
    v_ln_agreement_type VARCHAR2(30) ; --Line Level Variable for xxwc.agreement_type
    v_ln_agree_type VARCHAR2(30) ; --Set CSP or VQN Agreement Type Variable passed to API
    v_ln_vendor_quote_number VARCHAR2(30) ;  --Line Level Variable for XXWC.VENDOR_QUOTE_NUMBER
    v_ln_start_date DATE ; --Line Level Variable for XXWC.START_DATE
    v_ln_end_date DATE;  --Line Level Variable for XXWC.END_DATE
    v_ln_mod_type  VARCHAR2(30) ; --Line Level Variable for Modifier Type
    v_arithmetic_operator VARCHAR2(30) ; --Line Level Variable for Modifier Type API Value
    v_ln_mod_value NUMBER ; --Line Level Variable for Modifier Value
    v_ln_mod_val NUMBER ; --Line Level Modifer Value passed to API
    v_ln_vendor_id NUMBER ;--Line LEvel Variable for Vendor ID
    v_product_attribute VARCHAR2(50) ;  --Line Level Variable for Product Attribute (Item/Cat Class)
    v_product_value VARCHAR2(50) ;--Line Level Variable for Product Value (SKU Number)
    v_product_attribute_api VARCHAR2(50) ; --Line Level Variable to set in API
    v_product_attribute_context VARCHAR2(30) := 'ITEM' ; --Variable to set product_attribute_context in Pricing Attr API
    v_product_attr_value VARCHAR2(50) ;
    v_generate_using_formula_id NUMBER ;
    V9 NUMBER ; 
    i NUMBER ;
    j NUMBER ;
    L_MSG VARCHAR2(240) ; 
    l_Msg_Data   Varchar2(2000) ; 
    l_formula_id NUMBER;
           
    CURSOR LINEIDUPDATE 
    IS select agreement_line_id 
    from xxwc.xxwc_om_contract_pricing_lines 
    where revision_number != 0
    and interfaced_flag = 'N' 
    and line_status = 'DELTA' ;
        
 

        
    Begin
    
           BEGIN
        l_formula_id:=NULL;
        SELECT Price_Formula_Id
          INTO l_formula_id
          FROM QP_PRICE_FORMULAS
         WHERE UPPER(NAME) = 'PERCENT DISCOUNT' AND ROWNUM = 1;
       EXCEPTION
        WHEN OTHERS THEN
        l_formula_id := NULL;
       END;   
            
      i := 0 ;
      for c3_rec in LINEIDUPDATE 
      loop
        i:= i + 1 ;
      /*  Initial select to set line level variables */    
        BEGIN
          SELECT 
          start_date,
          end_date,
          modifier_type,
          modifier_value,
          revision_number
          INTO
          v_ln_start_date, --Line Level Variable for start date
          v_ln_end_date, --Line Level Variable for end date
          v_ln_mod_type, --Line Level Variable for Modifier Type
          v_ln_mod_value, --Line Level Variable for Modifier Value
          v_revision_number
          FROM xxwc.xxwc_om_contract_pricing_lines
          WHERE agreement_line_id = c3_rec.agreement_line_id ;           
        EXCEPTION
            WHEN OTHERS THEN 
              L_MSG := SUBSTR('Initial select failed, check modifier line' || SQLCODE || SQLERRM,1,240) ;
          END; 
          
      /*  Initial select to set line level variables */            
        BEGIN
          select list_line_id
          into v_list_line_id
          from qp_list_lines
          where attribute2 = c3_rec.agreement_line_id ;
        EXCEPTION
            WHEN OTHERS THEN 
              L_MSG := SUBSTR('No modifier list header id exists' || SQLCODE || SQLERRM,1,240) ;
          END; 
          
          IF v_ln_mod_type = 'New Price' 
          THEN v_arithmetic_operator := 'NEWPRICE' ;
          ELSIF v_ln_mod_type = 'Amount' 
          THEN v_arithmetic_operator := 'AMT' ;
          ELSIF v_ln_mod_type = 'Percent' 
          THEN v_arithmetic_operator := 'NEWPRICE';  /*TMS: 20131013-00045 Multi Decimal Rounding, Toby Rave, changed from % to NEW PRICE */
          ELSIF v_ln_mod_type = 'Cost Plus' 
          THEN v_arithmetic_operator := 'NEWPRICE' ;
          Else v_arithmetic_operator := NULL ;
          END IF;
          
          IF v_ln_mod_type = 'Cost Plus' 
          THEN v_generate_using_formula_id := 7030 ;
         /*TMS: 20131013-00045 Multi Decimal Rounding, Toby Rave, block added to set % discounts to formual based*/
         ELSIF v_ln_mod_type = 'Percent'
         THEN
            v_generate_using_formula_id := l_formula_id  ;
         ELSE
            v_generate_using_formula_id := NULL;
         END IF;
          
          IF v_ln_mod_type = 'New Price'
          THEN v_ln_mod_val := v_ln_mod_value;
          ELSIF v_ln_mod_type = 'Amount'
          THEN
          v_ln_mod_val := v_ln_mod_value;
          ELSIF v_ln_mod_type = 'Percent'
          THEN
          v_ln_mod_val := ((100 - v_ln_mod_value) / 100) ;   /*TMS: 20131013-00045 Multi Decimal Rounding, Toby Rave, calculation added to calulation operand to pass to % discount formula modifier*/
          ELSIF v_ln_mod_type = 'Cost Plus'
          THEN
          v_ln_mod_val := (v_ln_mod_value / 100);
          ELSE
          v_arithmetic_operator := NULL;
          END IF;
          
          
          /*  Populate API Variables for Modifier Line Level Updates */
            l_MODIFIERS_tbl(i).list_line_id := v_list_line_id ;
            l_MODIFIERS_tbl(i).start_date_active := v_ln_start_date ;
            l_MODIFIERS_tbl(i).end_date_active := v_ln_end_date ;
            l_MODIFIERS_tbl(i).arithmetic_operator := v_arithmetic_operator ;
            l_MODIFIERS_tbl(i).operand := v_ln_mod_val ;
            l_MODIFIERS_tbl(i).price_by_formula_id := v_generate_using_formula_id ;
            l_MODIFIERS_tbl(i).operation := QP_GLOBALS.G_OPR_UPDATE; 
          
        /*Set CSP line revision to next revision */  
        /* Set CSP Line Interfaced flag to Y */
        /* Set CSP Lines status from AWAITING APPROVAL to APPROVED */        
          UPDATE xxwc.xxwc_om_contract_pricing_lines
          SET revision_number = (v_revision_number + 1) 
            , interfaced_flag = 'Y'
            , line_status = 'APPROVED'
          WHERE agreement_line_id = c3_rec.agreement_line_id ; 
      END LOOP ; 
        
      /*  API Call */
        QP_Modifiers_PUB.Process_Modifiers
        ( p_api_version_number => 1.0
        , p_init_msg_list => FND_API.G_FALSE
        , p_return_values => FND_API.G_FALSE
        , p_commit => FND_API.G_FALSE
        , x_return_status => l_return_status
        , x_msg_count =>x_msg_count
        , x_msg_data =>x_msg_data
        ,p_MODIFIER_LIST_rec => l_MODIFIER_LIST_rec
        ,p_MODIFIERS_tbl => l_MODIFIERS_tbl
        ,p_QUALIFIERS_tbl => l_QUALIFIERS_tbl
        ,p_PRICING_ATTR_tbl => l_PRICING_ATTR_tbl
        ,x_MODIFIER_LIST_rec => l_x_MODIFIER_LIST_rec         
        ,x_MODIFIER_LIST_val_rec => l_x_MODIFIER_LIST_val_rec 
        ,x_MODIFIERS_tbl => l_x_MODIFIERS_tbl                 
        ,x_MODIFIERS_val_tbl => l_x_MODIFIERS_val_tbl         
        ,x_QUALIFIERS_tbl => l_x_QUALIFIERS_tbl               
        ,x_QUALIFIERS_val_tbl => l_x_QUALIFIERS_val_tbl       
        ,x_PRICING_ATTR_tbl => l_x_PRICING_ATTR_tbl           
        ,x_PRICING_ATTR_val_tbl => l_x_PRICING_ATTR_val_tbl   
        );
        
        
        IF l_Return_Status =  G_RETURN_SUCCESS THen 
           Fnd_File.Put_Line ( Fnd_File.LOG, '320 Process Modifiers API Return Status is Successful =>' );
           Fnd_File.Put_Line ( Fnd_File.LOG, '321 Modifier List Name is  =>' || l_MODIFIER_LIST_rec.NAME  );
        Else
           Fnd_File.Put_Line ( Fnd_File.LOG, '323 Process Modifiers API Return Status is not Successful =>' );
           Fnd_File.Put_Line ( Fnd_File.LOG, '324 Modifier List Name is  =>' || l_MODIFIER_LIST_rec.NAME  );
           FOR i IN 1 .. x_msg_count loop
                 l_msg_data := oe_msg_pub.get( p_msg_index => i, p_encoded => 'F'  ); 
           End Loop ;
           Fnd_File.Put_Line ( Fnd_File.LOG, '325 Error Message is  =>' || l_msg_Data  );
           --Retcode := 1; 
        End If; 
        COMMIT;
    
  /*Delete all line level exclusions associated with CSP Agreement 
    select list_header_id
    into v_list_header_id
    from qp_list_headers
    where attribute14 = v_agreement_id ; 
    
    delete from
    qp_pricing_attributes
    where list_header_id = v_list_header_id
    and excluder_flag = 'Y' ; 
    commit ; */
  Exception 
     When Others Then 
        X_Msg_Data := Substr(SQLERRM,1,1000) ; 
        Fnd_File.Put_Line ( Fnd_File.LOG, '327:  When Others Exception in Procedure Create_Delta_Lines' );
        Fnd_File.Put_Line ( Fnd_File.LOG, '328:  Exception Message : ' || X_Msg_Data );
    
END update_delta_lines;
   
/*************************************************************************
*   PROCEDURE Name: create_delta_lines
*
*   PURPOSE:   CREATE_LINES procedure is used to create Modifier Lines
*              in Advanced Pricing related to the CSP Agreement Header 
*              'i_agreement_id' 
* ***************************************************************************/
   PROCEDURE create_delta_lines --(i_agreement_id   IN     NUMBER)
  IS
    l_control_rec                 QP_GLOBALS.Control_Rec_Type;
    l_return_status               VARCHAR2 (1);
    x_msg_count                   NUMBER;
    x_msg_data                    VARCHAR2 (2000);
    x_msg_index                   NUMBER;
    
    l_MODIFIER_LIST_rec           QP_Modifiers_PUB.Modifier_List_Rec_Type;
    l_MODIFIER_LIST_val_rec       QP_Modifiers_PUB.Modifier_List_Val_Rec_Type;
    l_MODIFIERS_tbl               QP_Modifiers_PUB.Modifiers_Tbl_Type;
    l_MODIFIERS_val_tbl           QP_Modifiers_PUB.Modifiers_Val_Tbl_Type;
    l_QUALIFIERS_tbl              QP_Qualifier_Rules_PUB.Qualifiers_Tbl_Type;
    l_QUALIFIERS_val_tbl          QP_Qualifier_Rules_PUB.Qualifiers_Val_Tbl_Type;
    l_PRICING_ATTR_tbl            QP_Modifiers_PUB.Pricing_Attr_Tbl_Type;
    l_PRICING_ATTR_val_tbl        QP_Modifiers_PUB.Pricing_Attr_Val_Tbl_Type;
    
    l_x_MODIFIER_LIST_rec         QP_Modifiers_PUB.Modifier_List_Rec_Type;
    l_x_MODIFIER_LIST_val_rec     QP_Modifiers_PUB.Modifier_List_Val_Rec_Type;
    l_x_MODIFIERS_tbl             QP_Modifiers_PUB.Modifiers_Tbl_Type;
    l_x_MODIFIERS_val_tbl         QP_Modifiers_PUB.Modifiers_Val_Tbl_Type;
    l_x_QUALIFIERS_tbl            QP_Qualifier_Rules_PUB.Qualifiers_Tbl_Type;
    l_x_QUALIFIERS_val_tbl        QP_Qualifier_Rules_PUB.Qualifiers_Val_Tbl_Type;
    l_x_PRICING_ATTR_tbl          QP_Modifiers_PUB.Pricing_Attr_Tbl_Type;
    l_x_PRICING_ATTR_val_tbl      QP_Modifiers_PUB.Pricing_Attr_Val_Tbl_Type;




    --v_agreement_id                NUMBER := i_agreement_id;
    v_revision_number             NUMBER;
    v_incompatability_group       VARCHAR2 (30);
    v_incomp_group                VARCHAR2 (30);
    v_list_header_id              NUMBER;                 --v_list_header_id
    v_atrribute_3                 NUMBER;
    v_ln_special_cost             NUMBER;
    v_ln_id                       NUMBER; --Line Level Variable for xxwc.agreement_line_id
    v_ln_agreement_type           VARCHAR2 (30); --Line Level Variable for xxwc.agreement_type
    v_ln_agree_type               VARCHAR2 (30); --Set CSP or VQN Agreement Type Variable passed to API
    v_ln_vendor_quote_number      VARCHAR2 (30); --Line Level Variable for XXWC.VENDOR_QUOTE_NUMBER
    v_ln_start_date               DATE; --Line Level Variable for XXWC.START_DATE
    v_ln_end_date                 DATE; --Line Level Variable for XXWC.END_DATE
    v_ln_mod_type                 VARCHAR2 (30); --Line Level Variable for Modifier Type
    v_arithmetic_operator         VARCHAR2 (30); --Line Level Variable for Modifier Type API Value
    v_ln_mod_value                NUMBER; --Line Level Variable for Modifier Value
    v_ln_mod_val                  NUMBER; --Line Level Modifer Value passed to API
    v_ln_vendor_id                NUMBER; --Line LEvel Variable for Vendor ID
    v_product_attribute           VARCHAR2 (50); --Line Level Variable for Product Attribute (Item/Cat Class)
    v_product_value               VARCHAR2 (50); --Line Level Variable for Product Value (SKU Number)
    v_product_attribute_api       VARCHAR2 (50); --Line Level Variable to set in API
    v_product_attribute_context   VARCHAR2 (30) := 'ITEM'; --Variable to set product_attribute_context in Pricing Attr API
    v_product_attr_value          VARCHAR2 (50);
    v_generate_using_formula_id   NUMBER;
    V9                            NUMBER;
    i                             NUMBER;
    j                             NUMBER;
    L_MSG                         VARCHAR2(240);
   l_formula_id       NUMBER;
    l_msg_data                    Varchar2(2000) ; 
   
    CURSOR LINEID                                        
    IS
      select agreement_line_id 
      from xxwc.xxwc_om_contract_pricing_lines 
      where line_status = 'DELTA'
      and revision_number = 0
      and interfaced_flag = 'N' ;
   


    BEGIN
    
    BEGIN
        l_formula_id:=NULL;
        SELECT Price_Formula_Id
          INTO l_formula_id
          FROM QP_PRICE_FORMULAS
         WHERE UPPER(NAME) = 'PERCENT DISCOUNT' AND ROWNUM = 1;
       EXCEPTION
        WHEN OTHERS THEN
        l_formula_id := NULL;
       END; 
    
      i := 0;

      FOR c2_rec IN LINEID 
      LOOP
         i := i + 1;

      /*Initial select to set variables with CSP Line Infomation */
        BEGIN
         SELECT agreement_id
              , agreement_line_id
              , agreement_type
              , vendor_quote_number
              , product_attribute
              , product_value
              , start_date
              , end_date
              , modifier_type
              , modifier_value
              , special_cost
              , vendor_id
              , revision_number
           INTO V9
              , v_ln_id
              ,                    --Line Level Variable for agreement_line_id
               v_ln_agreement_type
              ,                       --Line Level Variable for agreement_type
               v_ln_vendor_quote_number
              ,             --Line Level Variable for v_ln_vendor_quote_number
               v_product_attribute
              ,   --Line Level Variable for Product Attribute (Item/Cat Class)
               v_product_value
              , v_ln_start_date
              ,                           --Line Level Variable for start date
               v_ln_end_date
              ,                             --Line Level Variable for end date
               v_ln_mod_type
              ,                        --Line Level Variable for Modifier Type
               v_ln_mod_value
              ,                       --Line Level Variable for Modifier Value
               v_ln_special_cost
              ,                         --Line Level Variable for Special Cost
               v_ln_vendor_id              --Line LEvel Variable for Vendor ID
              , 
               v_revision_number
           FROM xxwc.xxwc_om_contract_pricing_lines
          WHERE agreement_line_id = c2_rec.agreement_line_id;
        EXCEPTION
          WHEN OTHERS THEN 
            L_MSG := SUBSTR('Initial select failed verify product infomration' || SQLCODE || SQLERRM,1,240) ;
        END;  


      /*Select modifier list_header_id from modifier list */
        BEGIN
         SELECT list_header_id
           INTO v_list_header_id
           FROM qp_list_headers
          WHERE attribute14 = V9;
        EXCEPTION
          WHEN OTHERS THEN 
            L_MSG := SUBSTR('No pricing Agreement Exists' || SQLCODE || SQLERRM,1,240) ;
        END;  


        /*Determine Imcompatability from CSP Agreement*/
         BEGIN
         SELECT incompatability_group
           INTO v_incompatability_group
           FROM xxwc.xxwc_om_contract_pricing_hdr
          WHERE agreement_id = v9;        
        EXCEPTION
          WHEN OTHERS THEN 
            L_MSG := SUBSTR('No incompatibility group found' || SQLCODE || SQLERRM,1,240) ;
        END; 

         --Set Incompatability Group in API
         IF v_incompatability_group = 'Best Price Wins'
         THEN
            v_incomp_group := 'LVL 1';
         ELSIF v_incompatability_group = 'Exclusive'
         THEN
            v_incomp_group := 'EXCL';
         ELSE
            v_incomp_group := NULL;
         END IF;

         --Set Product Attribute Variable to pass to API (Item, Cat Class, Category)
         IF v_product_attribute = 'ITEM'
         THEN
            v_product_attribute_api := 'PRICING_ATTRIBUTE1';
         ELSIF v_product_attribute = 'CAT_CLASS'
         THEN
            v_product_attribute_api := 'PRICING_ATTRIBUTE2';
         ELSIF v_product_attribute = 'CATEGORY'
         THEN
            v_product_attribute_api := 'PRICING_ATTRIBUTE26';
         ELSE
            v_arithmetic_operator := NULL;
         END IF;

         --Set Arithetic Operator Variable
         IF v_ln_mod_type = 'New Price'
         THEN
            v_arithmetic_operator := 'NEWPRICE';
         ELSIF v_ln_mod_type = 'Amount'
         THEN
            v_arithmetic_operator := 'AMT';
         ELSIF v_ln_mod_type = 'Percent'
         THEN v_arithmetic_operator := 'NEWPRICE';  /*TMS: 20131013-00045 Multi Decimal Rounding, Toby Rave, changed from % to NEW PRICE */
         ELSIF v_ln_mod_type = 'Cost Plus'
         THEN
            v_arithmetic_operator := 'NEWPRICE';
         ELSE
            v_arithmetic_operator := NULL;
         END IF;

         --IF Application Mthod = Cost Plus then set formula ID variable in API
         IF v_ln_mod_type = 'Cost Plus'
         THEN
            v_generate_using_formula_id := 7030;
         /*TMS: 20131013-00045 Multi Decimal Rounding, Toby Rave, block added to set % discounts to formual based*/
         ELSIF v_ln_mod_type = 'Percent'
         THEN
            v_generate_using_formula_id := l_formula_id ;
         ELSE
            v_generate_using_formula_id := NULL;
         END IF;


         IF v_ln_mod_type = 'New Price'
         THEN
            v_ln_mod_val := v_ln_mod_value;
         ELSIF v_ln_mod_type = 'Amount'
         THEN
            v_ln_mod_val := v_ln_mod_value;
         ELSIF v_ln_mod_type = 'Percent'
         THEN
             v_ln_mod_val := ((100 - v_ln_mod_value) / 100) ;   /*TMS: 20131013-00045 Multi Decimal Rounding, Toby Rave, calculation added to calulation operand to pass to % discount formula modifier*/
         ELSIF v_ln_mod_type = 'Cost Plus'
         THEN
            v_ln_mod_val := (v_ln_mod_value / 100);
         ELSE
            v_arithmetic_operator := NULL;
         END IF;

      /*Find Vendor ID for Vendor Quote Lines*/
         IF v_ln_agreement_type = 'VQN'
         THEN
          BEGIN 
            SELECT segment1
              INTO v_atrribute_3
              FROM ap_suppliers
             WHERE vendor_id = v_ln_vendor_id;
           EXCEPTION
            WHEN OTHERS THEN 
              L_MSG := SUBSTR('INVALID VENDOR' || SQLCODE || SQLERRM,1,240) ;
          END; 
        
         ELSE
            v_atrribute_3 := NULL;
         END IF;

        /* Set Item Numbers, Category Class Variables, and Category variables */
         IF v_product_attribute = 'ITEM'
         THEN
          BEGIN
            SELECT inventory_item_id
              INTO v_product_attr_value
              FROM mtl_system_items_b
             WHERE segment1 = v_product_value AND organization_id = '222';
          EXCEPTION
            WHEN OTHERS THEN 
              L_MSG := SUBSTR('ITEM dOES NOT EXIST' || SQLCODE || SQLERRM,1,240) ;
          END; 
         ELSIF v_product_attribute = 'CAT_CLASS'
         THEN
          BEGIN
            SELECT category_id
              INTO v_product_attr_value
              FROM mtl_categories_b
             WHERE segment1 || '.' || segment2 = v_product_value;
           EXCEPTION
            WHEN OTHERS THEN 
              L_MSG := SUBSTR('INVALID CATEGORY ID' || SQLCODE || SQLERRM,1,240) ;
          END; 
       /* Value from CSP Line Line used as variable for CATEGORY insert */   
         ELSIF v_product_attribute = 'CATEGORY'
         THEN
            v_product_attr_value := v_product_value;
         ELSE
            v_product_attr_value := NULL;
         END IF;

      /*Set Line Line DFF on Modifier to CSP or VQN */
         IF v_ln_agreement_type = 'VQN'
         THEN
            v_ln_agree_type := 'Vendor Quote';
         ELSIF v_ln_agreement_type = 'CSP'
         THEN
            v_ln_agree_type := 'Contract Pricing';
         ELSE
            v_ln_agree_type := NULL;
         END IF;

        /* Set Modifier Line API Variables */
         l_MODIFIERS_tbl (i).list_header_id := v_list_header_id;
         l_MODIFIERS_tbl (i).list_line_type_code := 'DIS';
         l_MODIFIERS_tbl (i).automatic_flag := 'Y';
         l_MODIFIERS_tbl (i).modifier_level_code := 'LINE';
         l_MODIFIERS_tbl (i).accrual_flag := 'N';
         l_MODIFIERS_tbl (i).start_date_active := v_ln_start_date;
         l_MODIFIERS_tbl (i).end_date_active := v_ln_end_date;
         l_MODIFIERS_tbl (i).arithmetic_operator := v_arithmetic_operator;
         l_MODIFIERS_tbl (i).pricing_group_sequence := 1;     --Pricing Bucket
         l_MODIFIERS_tbl (i).pricing_phase_id := 2; --Pricing Phase, '2' = List Line Adjustment
         l_MODIFIERS_tbl (i).product_precedence := 220;
         l_MODIFIERS_tbl (i).operand := v_ln_mod_val;
         l_MODIFIERS_tbl (i).INCLUDE_ON_RETURNS_FLAG := 'Y' ;
         l_MODIFIERS_tbl (i).context := '162';
         l_MODIFIERS_tbl (i).ATTRIBUTE2 := v_ln_id;
         l_MODIFIERS_tbl (i).ATTRIBUTE3 := v_atrribute_3;
         l_MODIFIERS_tbl (i).ATTRIBUTE4 := v_ln_special_cost;
         l_MODIFIERS_tbl (i).ATTRIBUTE5 := v_ln_agree_type;
         l_MODIFIERS_tbl (i).ATTRIBUTE6 := v_ln_vendor_quote_number;
         l_MODIFIERS_tbl (i).price_by_formula_id :=v_generate_using_formula_id;
         l_MODIFIERS_tbl (i).incompatibility_grp_code := v_incomp_group; --'LVL 1' or 'EXCL'
         l_MODIFIERS_tbl (i).operation := QP_GLOBALS.G_OPR_CREATE;


        /* Set Product Attribute API Variables */
         l_PRICING_ATTR_tbl (i).list_header_id := v_list_header_id;
         l_PRICING_ATTR_tbl (i).product_attribute_context := v_product_attribute_context;       --v_product_attribute_context ;
         l_PRICING_ATTR_tbl (i).product_attribute := v_product_attribute_api;
         l_PRICING_ATTR_tbl (i).product_attr_value := v_product_attr_value;
         --l_PRICING_ATTR_tbl(i).product_uom_code:= 'Ea';
         l_PRICING_ATTR_tbl (i).MODIFIERS_index := i;
         l_PRICING_ATTR_tbl (i).operation := QP_GLOBALS.G_OPR_CREATE;
         
     /*Update line level revision number to bext revision*/    
    /*Update interfaced flag to Yes * once the line has been processed*/
    /*  Update CSP line status to approved */    
        UPDATE xxwc.xxwc_om_contract_pricing_lines
        SET revision_number = (v_revision_number + 1)
          , interfaced_flag = 'Y'
          , line_status = 'APPROVED' 
        WHERE agreement_line_id = c2_rec.agreement_line_id ; 
      END LOOP;

      QP_Modifiers_PUB.Process_Modifiers (
         p_api_version_number      => 1.0
       , p_init_msg_list           => FND_API.G_FALSE
       , p_return_values           => FND_API.G_FALSE
       , p_commit                  => FND_API.G_FALSE
       , x_return_status           => l_return_status
       , x_msg_count               => x_msg_count
       , x_msg_data                => x_msg_data
       , p_MODIFIER_LIST_rec       => l_MODIFIER_LIST_rec
       , p_MODIFIERS_tbl           => l_MODIFIERS_tbl
       , p_QUALIFIERS_tbl          => l_QUALIFIERS_tbl
       , p_PRICING_ATTR_tbl        => l_PRICING_ATTR_tbl
       , x_MODIFIER_LIST_rec       => l_x_MODIFIER_LIST_rec
       , x_MODIFIER_LIST_val_rec   => l_x_MODIFIER_LIST_val_rec
       , x_MODIFIERS_tbl           => l_x_MODIFIERS_tbl
       , x_MODIFIERS_val_tbl       => l_x_MODIFIERS_val_tbl
       , x_QUALIFIERS_tbl          => l_x_QUALIFIERS_tbl
       , x_QUALIFIERS_val_tbl      => l_x_QUALIFIERS_val_tbl
       , x_PRICING_ATTR_tbl        => l_x_PRICING_ATTR_tbl
       , x_PRICING_ATTR_val_tbl    => l_x_PRICING_ATTR_val_tbl);
       
       
        IF l_Return_Status =  G_RETURN_SUCCESS THen 
           Fnd_File.Put_Line ( Fnd_File.LOG, '120 Process Modifiers API Return Status is Successful =>' );
           Fnd_File.Put_Line ( Fnd_File.LOG, '121 Modifier List Name is  =>' || l_MODIFIER_LIST_rec.NAME  );
        Else
           Fnd_File.Put_Line ( Fnd_File.LOG, '120 Process Modifiers API Return Status is not Successful =>' );
           Fnd_File.Put_Line ( Fnd_File.LOG, '121 Modifier List Name is  =>' || l_MODIFIER_LIST_rec.NAME  );
           FOR i IN 1 .. x_msg_count loop
                 l_msg_data := oe_msg_pub.get( p_msg_index => i, p_encoded => 'F'  ); 
           End Loop ;
           Fnd_File.Put_Line ( Fnd_File.LOG, '122 Error Message is  =>' || l_msg_Data  );
           --Retcode := 1; 
        End If; 
       commit ; 
  Exception 
     When Others Then 
         l_Msg_Data := Substr(SQLERRM,1,1000) ; 
         Fnd_File.Put_Line ( Fnd_File.LOG, '130:  When Others Exception in Procedure Create_Delta_Lines' );
         Fnd_File.Put_Line ( Fnd_File.LOG, '131:  Exception Message : ' || l_Msg_Data );
         
  END Create_Delta_Lines;
   
  /*************************************************************************
*   PROCEDURE Name: mass_update
*
*   PURPOSE:   import modifiers procedure is used called by the CSP Maintenance 
               form and executes the four procedures above. Its is called for a 
               single CSP Agreement, i_agreement_id
****************************************************************************/
  PROCEDURE mass_update (errbuf              OUT VARCHAR2
                             , retcode             OUT VARCHAR2)
                            
      IS
        l_Msg_Data Varchar2(2000) ; 
      BEGIN
         update_delta_lines ; 
         create_delta_lines ; 
      Exception 
         When Others Then 
            l_Msg_Data := Substr(SQLERRM,1,1000) ; 
            Fnd_File.Put_Line ( Fnd_File.LOG, '200:  When Others Exception in Procedure Create_Delta_Lines' );
            Fnd_File.Put_Line ( Fnd_File.LOG, '201:  Exception Message : ' || l_Msg_Data );
            Retcode := 1;
      END mass_update ;

PROCEDURE cleanup_csp_quotes (errbuf OUT VARCHAR2, 
                              retcode OUT VARCHAR2,
                              i_agreement_id IN NUMBER)
IS
    l_agreement_id  number;
    l_agreement_status varchar2(30);
    l_price_type varchar2(30);
    l_account_number varchar2(30);
    l_customer_name varchar2(360);
    l_list_header_id number;
    l_active_flag varchar2(1);
    l_attribute14 varchar2(240);
    l_count_lines   number;
    
    cursor cur_linked_qp_headers is
        SELECT  csp_hdr.agreement_id agreement_number
                , csp_hdr.agreement_status
                , csp_hdr.price_type
                , qlhb.list_header_id
                , qlhb.active_flag
                , qlhb.attribute14
                , qlht.name modifier_name
        FROM    xxwc.xxwc_om_contract_pricing_hdr csp_hdr
                , qp.qp_list_headers_b qlhb
                , qp.qp_list_headers_tl qlht
        WHERE   csp_hdr.agreement_id = i_agreement_id
        AND     csp_hdr.agreement_id = qlhb.attribute14
        AND     qlhb.list_header_id = qlht.list_header_id;            

BEGIN
    Fnd_File.Put_Line ( Fnd_File.output, 'Starting CSP Quote Cleanup Procedure' );
    Fnd_File.Put_Line ( Fnd_File.output, '************************************' );
    Fnd_File.Put_Line ( Fnd_File.output, ' ' );
    Fnd_File.Put_Line ( Fnd_File.output, 'Parameters: ' );
    Fnd_File.Put_Line ( Fnd_File.output, 'i_agreement_id: '||i_agreement_id );
    Fnd_File.Put_Line ( Fnd_File.output, '************************************' );
    Fnd_File.Put_Line ( Fnd_File.output, ' ' );
    
    l_agreement_id  := null;
    l_agreement_status := null;
    l_price_type := null;
    l_account_number := null;
    l_customer_name := null;
    l_list_header_id := null;
    l_active_flag := null;
    l_attribute14 := null;
    
    
    BEGIN
        SELECT  csp_hdr.agreement_id agreement_number
                , csp_hdr.agreement_status
                , csp_hdr.price_type
                , hca.account_number
                , hp.party_number customer_name
                , qlhb.list_header_id
                , qlhb.active_flag
                , qlhb.attribute14
        INTO    l_agreement_id
                , l_agreement_status
                , l_price_type
                , l_account_number
                , l_customer_name
                , l_list_header_id
                , l_active_flag
                , l_attribute14
        FROM    xxwc.xxwc_om_contract_pricing_hdr csp_hdr
                , qp.qp_list_headers_b qlhb
                , hz_cust_accounts hca
                , hz_parties hp
        WHERE   csp_hdr.agreement_id = i_agreement_id
        AND     csp_hdr.agreement_id = qlhb.attribute14
        AND     csp_hdr.customer_id = hca.cust_account_id
        AND     hca.party_id = hp.party_id;
    EXCEPTION
    WHEN NO_DATA_FOUND THEN
        Fnd_File.Put_Line ( Fnd_File.output, 'ERROR: No linked CSP/QP Records found' );
        retcode := 2;
        errbuf := 'ERROR: No linked CSP/QP Records found';
        raise;  
    WHEN TOO_MANY_ROWS THEN
        Fnd_File.Put_Line ( Fnd_File.output, 'ERROR: Multiple linked CSP/QP Records found' );
        Fnd_File.Put_Line ( Fnd_File.output, ' ' );
        Fnd_File.Put_Line ( Fnd_File.output, 'Pulling List of linked CSP/QP Records' ); 
        Fnd_File.Put_Line ( Fnd_File.output, '------------------------------------- ' );       
        FOR c1 in cur_linked_qp_headers
        LOOP
            EXIT WHEN cur_linked_qp_headers%notfound;
            Fnd_File.Put_Line ( Fnd_File.output, 'CSP Number: '||c1.agreement_number);
            Fnd_File.Put_Line ( Fnd_File.output, 'CSP Status: '||c1.agreement_status);
            Fnd_File.Put_Line ( Fnd_File.output, 'CSP Price Type: '||c1.price_type);
            Fnd_File.Put_Line ( Fnd_File.output, 'QP Modifier Name: '||c1.modifier_name);
            Fnd_File.Put_Line ( Fnd_File.output, 'QP Header Active Flag: '||c1.active_flag);
            Fnd_File.Put_Line ( Fnd_File.output, 'QP Attribute14 (CSP Agreement Number): '||c1.attribute14);
            Fnd_File.Put_Line ( Fnd_File.output, ' ' );
        END LOOP;
        
        Fnd_File.Put_Line ( Fnd_File.output, '------------------------------------- ' );
        
        retcode := 2;
        errbuf := 'ERROR: Multiple linked CSP/QP Records found';
        raise; 
    WHEN OTHERS THEN
        Fnd_File.Put_Line ( Fnd_File.output, 'ERROR: Other linked CSP/QP Records ocurred' );
        Fnd_File.Put_Line ( Fnd_File.output, 'ERROR: '||SQLERRM );
        retcode := 2;
        errbuf := 'ERROR: Other linked CSP/QP Records ocurred';
        raise;  
    END;
    
    Fnd_File.Put_Line ( Fnd_File.output, ' ' );
    Fnd_File.Put_Line ( Fnd_File.output, 'CSP Status: '||l_agreement_status );
    Fnd_File.Put_Line ( Fnd_File.output, 'Price Type: '||l_price_type);
    Fnd_File.Put_Line ( Fnd_File.output, 'Customer Number - Name: '||l_account_number||' - '||l_customer_name);
    Fnd_File.Put_Line ( Fnd_File.output, 'QP Header ID: '||l_list_header_id);
    Fnd_File.Put_Line ( Fnd_File.output, 'QP Header Active Flag: '||l_active_flag);
    Fnd_File.Put_Line ( Fnd_File.output, 'QP Attribute14 (CSP Agreement Number): '||l_attribute14);
    Fnd_File.Put_Line ( Fnd_File.output, ' ' );
    
    IF NVL(l_active_flag, 'N') = 'Y' THEN
        Fnd_File.Put_Line ( Fnd_File.output, 'Associated QP Modifier is Active, cannot remove associated CSP Quote...exiting cleanup process' );
        retcode := 2;
        errbuf := 'ERROR: QP Modifier is Active';
    ELSE
    
        Fnd_File.Put_Line ( Fnd_File.output, 'Modifier is inactive.' );
        l_count_lines := 0;
        BEGIN
            select  count(*)
            into    l_count_lines
            from    xxwc.xxwc_om_contract_pricing_lines
            where   agreement_id = i_agreement_id;
        EXCEPTION
        WHEN OTHERS THEN
            Fnd_File.Put_Line ( Fnd_File.log, 'Error counting lines. Error: '||SQLERRM );   
        END;
        
        Fnd_File.Put_Line ( Fnd_File.output, 'There are '||l_count_lines||' CSP Lines in the custom table.');
        
        Fnd_File.Put_Line ( Fnd_File.output, ' ' );
        Fnd_File.Put_Line ( Fnd_File.output, 'Starting archive process...' );
        
        -- Archive header
        BEGIN
            INSERT INTO XXWC.XXWC_OM_CSP_HEADER_ARCHIVE
            SELECT  *
            FROM    xxwc.xxwc_om_contract_pricing_hdr
            WHERE   agreement_id = i_agreement_id;
        EXCEPTION
        WHEN OTHERS THEN
            Fnd_File.Put_Line ( Fnd_File.output, 'Could not archive header - exiting program.');
            Fnd_File.Put_Line ( Fnd_File.output, 'ERROR: '||SQLERRM);
            retcode := 2;
            errbuf := 'Error Archiving header record';
            raise;
        END;
        
        -- Archive lines
        BEGIN
            INSERT INTO XXWC.XXWC_OM_CSP_LINES_ARCHIVE
            SELECT  *
            FROM    xxwc.xxwc_om_contract_pricing_lines
            WHERE   agreement_id = i_agreement_id;
        EXCEPTION
        WHEN OTHERS THEN
            Fnd_File.Put_Line ( Fnd_File.output, 'Could not archive lines - exiting program.');
            Fnd_File.Put_Line ( Fnd_File.output, 'ERROR: '||SQLERRM);
            retcode := 2;
            errbuf := 'Error Archiving line records';
            raise;
        END;
        
        Fnd_File.Put_Line ( Fnd_File.output, 'Archive process complete...starting record removal...' );
        -- Delete header
        BEGIN
            DELETE
            FROM    xxwc.xxwc_om_contract_pricing_hdr
            WHERE   agreement_id = i_agreement_id;
        EXCEPTION
        WHEN OTHERS THEN
            Fnd_File.Put_Line ( Fnd_File.output, 'Could not delete header - exiting program.');
            Fnd_File.Put_Line ( Fnd_File.output, 'ERROR: '||SQLERRM);
            retcode := 2;
            errbuf := 'Error Deleting header record';
            rollback;
            raise;
        END;
        
        -- Delete lines
        BEGIN
            DELETE
            FROM    xxwc.xxwc_om_contract_pricing_lines
            WHERE   agreement_id = i_agreement_id;
        EXCEPTION
        WHEN OTHERS THEN
            Fnd_File.Put_Line ( Fnd_File.output, 'Could not delete lines - exiting program.');
            Fnd_File.Put_Line ( Fnd_File.output, 'ERROR: '||SQLERRM);
            retcode := 2;
            errbuf := 'Error Deleting line records';
            rollback;
            raise;
        END;
        
    END IF;
    
    commit;

EXCEPTION
WHEN OTHERS THEN
    Fnd_File.Put_Line ( Fnd_File.log,'Other exception in main body of cleanup_csp_quotes');
    Fnd_File.Put_Line ( Fnd_File.log,'ERROR: '||SQLERRM);  
    retcode := 2;
    errbuf := 'Other exception in main body of cleanup_csp_quotes';
END cleanup_csp_quotes;

-- 08/28/2013 CG: TMS 20130828-00775: New procedure to be used from maintenance from for self service submission
  PROCEDURE xxwc_csp_maintenance (RETCODE         OUT NUMBER
                                   , ERRMSG         OUT VARCHAR2
                                   , P_FILE_NAME    IN VARCHAR2
                                   , P_FILE_ID      IN NUMBER)
  IS
    l_error_message2 clob;
    l_start_time    number;  
    l_end_time      number;
    l_conc_req_id   number := fnd_global.conc_request_id;
    l_login_id      number := fnd_global.login_id;
    l_file_id       number :=  P_FILE_ID;    
    l_file          BLOB;
    l_file_name     VARCHAR2 (256);
    l_filehandle    UTL_FILE.file_type;
    l_msg_data      VARCHAR2 (4000);
    x               INTEGER := 1; -- Row counter
    is_file_open    INTEGER;
    l_offset        NUMBER := 1;
    l_eol_pos       NUMBER := 32767;
    l_amount        NUMBER;
    l_file_len      NUMBER := DBMS_LOB.getlength (l_file);
    l_buffer        RAW (30000);
    
    l_dir_path      VARCHAR2(4000);
    l_req_id        NUMBER          NULL;
    
    v_phase                 VARCHAR2 (50);
    v_status                VARCHAR2 (50);
    v_dev_status            VARCHAR2 (50);
    v_dev_phase             VARCHAR2 (50);
    v_message               VARCHAR2 (250);
    v_error_message         VARCHAR2 (3000);
  BEGIN
    
    if P_FILE_NAME is not null then
    
        fnd_file.put_line(fnd_file.log,'0. Transferring file from FND Table to DB Server Location...');
        l_Start_time := DBMS_UTILITY.get_time;
        fnd_file.put_line(fnd_file.log,'l_Start_time: '||l_Start_time);
        IF P_FILE_ID IS NOT NULL THEN
            BEGIN
                SELECT  file_data
                        , file_name
                INTO    l_file
                        , l_file_name
                FROM    fnd_lobs
                WHERE   file_id = l_file_id; 
            EXCEPTION
            WHEN OTHERS THEN
                l_file := null;
                l_file_name := null;
                fnd_file.put_line(fnd_file.log,'File Not Found. Error: '||SQLERRM);
                l_msg_data := substr(SQLERRM, 1, 4000);
                errmsg := l_msg_data;
                retcode := '2';  
                raise;
            END;

            is_file_open := DBMS_LOB.ISOPEN (l_file);
            IF is_file_open = 1
            THEN
                DBMS_LOB.close (l_file);
            END IF;

            BEGIN
                DBMS_LOB.open (l_file, DBMS_LOB.lob_readonly);
                is_file_open        := DBMS_LOB.ISOPEN (l_file);
            EXCEPTION
            WHEN OTHERS  THEN
                fnd_file.put_line(fnd_file.log,'Could not open file. Error: '||SQLERRM);
                l_msg_data := substr(SQLERRM, 1, 4000);
                errmsg := l_msg_data;
                retcode := '2';  
                raise;
            END;

            -- New DB Server File
            l_filehandle := UTL_FILE.fopen (    LOCATION       => 'XXWC_QP_CSP_MAINT_DIR',
                                                filename       => l_file_name,
                                                open_mode      => 'w',
                                                max_linesize   => 32767
                                            );
            
            l_file_len  := DBMS_LOB.getlength (l_file);
            fnd_file.put_line(fnd_file.log,'File Name '||l_file_name||' and length ' ||l_file_len||'. Offset: '||l_offset);
            fnd_file.put_line(fnd_file.log,'Starting File read and parsing from LOB...');
            
            WHILE (l_offset < l_file_len)
            LOOP
                -- Finding EOL position  for new line/line feed ascii 0A
                l_eol_pos      := DBMS_LOB.INSTR (l_file, '0A', 1, x);
                -- write_log ('l_eol_pos: '||l_eol_pos);
                
                IF x = 1 THEN
                    l_amount     := l_eol_pos - 1;
                ELSE
                    l_amount     := l_eol_pos - l_offset;
                END IF;
                
                -- Reading record from LOB file
                DBMS_LOB.read (l_file, l_amount, l_offset, l_buffer);
                
                -- Writing to new file on Server
                UTL_FILE.put_line (l_filehandle, UTL_RAW.cast_to_varchar2 (l_buffer));
                
                l_offset := l_offset + l_amount + 1; -- Increasing offset for read. Starting position of next read
                x        := x + 1;
            END LOOP;
            
            UTL_FILE.fclose (l_filehandle);
            DBMS_LOB.close (l_file);
            
            fnd_file.put_line(fnd_file.log,'File Row Count: '||x);
            
            begin
                update  XXWC.XXWC_QP_MAINT_TOOL_STG
                set     status = 'Processed'
                        , last_update_date = sysdate
                        , last_updated_by = fnd_global.user_id
                        , last_update_login = l_login_id 
                where   file_id = l_file_id
                and     processing_request_id = l_conc_req_id;
            exception
            when others then
                null;
            end;
            
        END IF;
        
        fnd_file.put_line(fnd_file.log,'0. File transfer to DB Server Location complete...');
        l_end_time := DBMS_UTILITY.get_time;
        fnd_file.put_line(fnd_file.log,'l_end_time : '||l_end_time );
        fnd_file.put_line(fnd_file.log,'File Transfer processing time: '||to_char(l_end_time-l_start_time));
        commit;
        fnd_file.put_line(fnd_file.log,' ');
        fnd_file.put_line(fnd_file.log,'******');
        fnd_file.put_line(fnd_file.log,' ');
  
        fnd_file.put_line(fnd_file.log,'Calling SQL Loader concurrent program...');
        l_dir_path := null;
        begin
            -- 10/09/2013 CG: TMS 20131009-00322: Updated to use profile for path
            -- l_dir_path := '/xx_iface/ebizprd/inbound/QP/csp';
            l_dir_path := G_FILE_DIR_PATH;
        exception
        when others then
            l_dir_path := null;
        end;
        
        if l_dir_path is not null then
            l_req_id := fnd_request.submit_request (application      => 'XXWC',
                                                    program          => 'XXWC_CSP_MAINT_LOAD',
                                                    description      => NULL,
                                                    start_time       => NULL,
                                                    sub_request      => FALSE,
                                                    argument1        => l_dir_path||'/'||l_file_name
                                                   );
            
            commit;
            
            fnd_file.put_line(fnd_file.log,'Request Submitted. Request ID: '||l_req_id);
            
            IF (l_req_id != 0)
            THEN
            
                IF fnd_concurrent.wait_for_request
                                           (l_req_id,
                                            6,
                                            1800,
                                            v_phase,
                                            v_status,
                                            v_dev_phase,
                                            v_dev_status,
                                            v_message
                                           )
                THEN
                   v_error_message :='ReqID:'|| l_req_id|| '-DPhase:'|| v_dev_phase|| '-DStatus:'|| v_dev_status|| CHR (10)|| 'MSG:'|| v_message;

                   -- Error Returned
                   IF v_dev_phase != 'COMPLETE' OR v_dev_status NOT IN ('NORMAL', 'WARNING')
                   THEN
                      fnd_file.put_line(fnd_file.log,'SQL Loader Completed with errors '|| v_error_message);
                      ERRMSG := 'SQL Loader Completed with errors '|| v_error_message;
                      RETCODE := '2';                      
                   ELSE
                   
                      fnd_file.put_line(fnd_file.log,'SQL Loader processing complete...submitting processing job'); 
                      
                      l_req_id := null;
                      l_req_id := fnd_request.submit_request (application      => 'XXWC',
                                                                program          => 'XXWC_CSP_DELTA_CONVERSION',
                                                                description      => NULL,
                                                                start_time       => NULL,
                                                                sub_request      => FALSE
                                                               );
                                
                        commit;
                        
                        fnd_file.put_line(fnd_file.log,'XXWC_CSP_DELTA_CONVERSION request id is '||l_req_id);
                   
                        -- 10/04/2013 CG: Added for new program call at the end
                        IF (l_req_id != 0) THEN
                        
                            IF fnd_concurrent.wait_for_request
                                                       (l_req_id,
                                                        6,
                                                        1800,
                                                        v_phase,
                                                        v_status,
                                                        v_dev_phase,
                                                        v_dev_status,
                                                        v_message
                                                       )
                            THEN
                               v_error_message :='ReqID:'|| l_req_id|| '-DPhase:'|| v_dev_phase|| '-DStatus:'|| v_dev_status|| CHR (10)|| 'MSG:'|| v_message;
                            
                               IF v_dev_phase != 'COMPLETE' OR v_dev_status NOT IN ('NORMAL', 'WARNING')
                               THEN
                                  fnd_file.put_line(fnd_file.log,'XXWC_CSP_DELTA_CONVERSION Completed with errors '|| v_error_message);
                                  ERRMSG := 'XXWC_CSP_DELTA_CONVERSION Completed with errors '|| v_error_message;
                                  RETCODE := '2';                      
                               ELSE
                               
                                  fnd_file.put_line(fnd_file.log,'XXWC_CSP_DELTA_CONVERSION complete...submitting processing job'); 
                                  
                                  l_req_id := null;
                                  l_req_id := fnd_request.submit_request (application      => 'XXWC',
                                                                            program          => 'XXWC_CSP_DELTA_UPLOAD',
                                                                            description      => NULL,
                                                                            start_time       => NULL,
                                                                            sub_request      => FALSE
                                                                           );
                                                
                                  commit;
                                        
                                  fnd_file.put_line(fnd_file.log,'CSP Maintenance Exclusions request id is '||l_req_id);
                            
                               END IF;
                               
                            ELSE
                               fnd_file.put_line(fnd_file.log,'XXWC_CSP_DELTA_CONVERSION Conc Program Wait timed out');
                               ERRMSG := 'XXWC_CSP_DELTA_CONVERSION Conc Program Wait timed out';
                               RETCODE := '2';
                            END IF;
                        
                        ELSE
                            fnd_file.put_line(fnd_file.log,'Concurrent program XXWC CSP Maintenance Process not initiated.');
                            ERRMSG := 'Concurrent program XXWC CSP Maintenance Process not initiated.';
                            RETCODE := '2';
                        END IF;
                        
                        -- 10/04/2013 CG: Added for new program call at the end
                   
                   END IF;
                ELSE
                   fnd_file.put_line(fnd_file.log,'Conc Program Wait timed out');
                   ERRMSG := 'Conc Program Wait timed out';
                   RETCODE := '2';
                END IF;
                
            ELSE
                fnd_file.put_line(fnd_file.log,'Concurrent program not initiated.');
                ERRMSG := 'Concurrent program not initiated.';
                RETCODE := '2';
            END IF;
            
        else
            fnd_file.put_line(fnd_file.log,'File directory path not found... Directory: XXWC_QP_CSP_MAINT_DIR');
        end if;
  
    END IF;
    
    commit;
  EXCEPTION
  WHEN OTHERS THEN
    UTL_FILE.fclose (l_filehandle);
    DBMS_LOB.close (l_file);

    l_error_message2 := 'xxwc_om_csp_interface_pkg '
                || 'Error_Stack...'
                || DBMS_UTILITY.format_error_stack ()
                || ' Error_Backtrace...'
                || DBMS_UTILITY.format_error_backtrace ();

    xxcus_error_pkg.xxcus_error_main_api (
        p_called_from         => 'xxwc_om_csp_interface_pkg.xxwc_csp_maintenance'
       ,p_calling             => 'xxwc_om_csp_interface_pkg.xxwc_csp_maintenance'
       ,p_request_id          => l_conc_req_id
       ,p_ora_error_msg       => l_error_message2
       ,p_error_desc          => 'Error running CSP Maintenance Interface'
       ,p_distribution_list   => 'trave@luciditycg.com'
       ,p_module              => 'CSP');

    fnd_file.put_line(fnd_file.log,'Error in main xxwc_om_csp_interface_pkg.xxwc_csp_maintenance. Error: '||l_error_message2);
    
    ERRMSG := l_error_message2;
    RETCODE := '2';
  END xxwc_csp_maintenance;

/*************************************************************************
*   PROCEDURE Name: csp_maintenance
*
*   PURPOSE:   csp_quote conversion procedure is used for loading converted 
*              records from the xxwc.xxwc_csp_interface tables into the 
*              CSP Maintenance tables (xxwc.xxwc_om_contractpricing_hdr, 
*              xxwc_xxwc.om_contract_pricing_lines)
****************************************************************************/ 
PROCEDURE csp_maintenance (errbuf OUT VARCHAR2, retcode OUT VARCHAR2)
   IS
   
    l_control_rec QP_GLOBALS.Control_Rec_Type;
    l_return_status VARCHAR2(1);
    x_msg_count number;
    x_msg_data Varchar2(2000);
    x_msg_index number;
    
    l_MODIFIER_LIST_rec QP_Modifiers_PUB.Modifier_List_Rec_Type;
    l_MODIFIER_LIST_val_rec QP_Modifiers_PUB.Modifier_List_Val_Rec_Type;
    l_MODIFIERS_tbl QP_Modifiers_PUB.Modifiers_Tbl_Type;
    n_MODIFIERS_tbl QP_Modifiers_PUB.Modifiers_Tbl_Type;
    l_MODIFIERS_val_tbl QP_Modifiers_PUB.Modifiers_Val_Tbl_Type;
    l_QUALIFIERS_tbl QP_Qualifier_Rules_PUB.Qualifiers_Tbl_Type;
    l_QUALIFIERS_val_tbl QP_Qualifier_Rules_PUB.Qualifiers_Val_Tbl_Type;
    l_PRICING_ATTR_tbl QP_Modifiers_PUB.Pricing_Attr_Tbl_Type;
    n_PRICING_ATTR_tbl QP_Modifiers_PUB.Pricing_Attr_Tbl_Type;
    l_PRICING_ATTR_val_tbl QP_Modifiers_PUB.Pricing_Attr_Val_Tbl_Type;
    
    l_x_MODIFIER_LIST_rec QP_Modifiers_PUB.Modifier_List_Rec_Type;
    l_x_MODIFIER_LIST_val_rec QP_Modifiers_PUB.Modifier_List_Val_Rec_Type;
    l_x_MODIFIERS_tbl QP_Modifiers_PUB.Modifiers_Tbl_Type;
    l_x_MODIFIERS_val_tbl QP_Modifiers_PUB.Modifiers_Val_Tbl_Type;
    l_x_QUALIFIERS_tbl QP_Qualifier_Rules_PUB.Qualifiers_Tbl_Type;
    l_x_QUALIFIERS_val_tbl QP_Qualifier_Rules_PUB.Qualifiers_Val_Tbl_Type;
    l_x_PRICING_ATTR_tbl QP_Modifiers_PUB.Pricing_Attr_Tbl_Type;
    l_x_PRICING_ATTR_val_tbl QP_Modifiers_PUB.Pricing_Attr_Val_Tbl_Type;
      
      
      l_customer_id     NUMBER;
      l_site_id         NUMBER;
      l_org_id          NUMBER;
      l_msg             VARCHAR2 (240);                    --VARCHAR2 (32000);
      l_conv_flag       VARCHAR2 (1) := 'Y';
      l_date            DATE := SYSDATE;
      l_user_id         NUMBER := NVL (fnd_profile.VALUE ('USER_ID'), -1);
      l_prod_att        VARCHAR2 (100);
      l_vendor_id       NUMBER;
      l_desc            VARCHAR2 (240);
      l_list_price      NUMBER;
      l_average_cost    NUMBER;
      l_selling_price   NUMBER;
      l_gross_margin    NUMBER;
      i                 NUMBER := 0;
      l_count           NUMBER := NULL ; 
      l_agreement_line_id NUMBER ; 
      l_list_line_id NUMBER ; 
      v_product_attr_value NUMBER ; 


      CURSOR c1
      IS
         SELECT c.agreement_line_id
           FROM XXWC_CSP_LINES_INTERFACE a, 
            xxwc_om_contract_pricing_lines c
          WHERE NVL (process_flag, 'N') <> 'Y'
                AND a.reference_id = c.agreement_id
                AND c.latest_rec_flag = 'Y' 
                and a.product_value = c.product_value ; 
              
              
   BEGIN
      
    

FOR c1_rec IN c1
LOOP

  l_modifiers_tbl := n_modifiers_tbl;
  l_PRICING_ATTR_tbl := n_PRICING_ATTR_tbl;
  l_vendor_id         := NULL;
  l_prod_att          := NULL;
  l_desc              := NULL;
  l_list_price        := NULL;
  l_average_cost      := NULL;
  l_selling_price     := NULL;
  l_gross_margin      := NULL;
  l_msg               := NULL;
  l_conv_flag         := 'Y';
  i                   := i + 1;
  l_count             := NULL ;
  l_agreement_line_id := NULL ;
  l_list_line_id      := NULL ; 
 

  UPDATE XXWC.xxwc_om_contract_pricing_lines
  SET END_DATE            = SYSDATE-1,
    interfaced_flag       = 'N',
    latest_rec_flag       = 'N',
    line_status           = 'DRAFT'
  WHERE agreement_line_id = c1_rec.agreement_line_id ;
  
  select list_line_id 
  into l_list_line_id
  from apps.qp_list_lines
  where attribute2 = c1_rec.agreement_line_id ; 
  
            /*  Populate API Variables for Modifier Line Level Updates */
            l_MODIFIERS_tbl(1).list_line_id := l_list_line_id ;
            --l_MODIFIERS_tbl(i).start_date_active := v_ln_start_date ;
            l_MODIFIERS_tbl(1).end_date_active := SYSDATE-1 ; 
            --l_MODIFIERS_tbl(i).arithmetic_operator := v_arithmetic_operator ;
            --l_MODIFIERS_tbl(i).operand := v_ln_mod_val ;
            --l_MODIFIERS_tbl(i).price_by_formula_id := v_generate_using_formula_id ;
            l_MODIFIERS_tbl(1).operation := QP_GLOBALS.G_OPR_UPDATE; 
          
      /*  API Call */
        QP_Modifiers_PUB.Process_Modifiers
        ( p_api_version_number => 1.0
        , p_init_msg_list => FND_API.G_FALSE
        , p_return_values => FND_API.G_FALSE
        , p_commit => FND_API.G_FALSE
        , x_return_status => l_return_status
        , x_msg_count =>x_msg_count
        , x_msg_data =>x_msg_data
        ,p_MODIFIER_LIST_rec => l_MODIFIER_LIST_rec
        ,p_MODIFIERS_tbl => l_MODIFIERS_tbl
        ,p_QUALIFIERS_tbl => l_QUALIFIERS_tbl
        ,p_PRICING_ATTR_tbl => l_PRICING_ATTR_tbl
        ,x_MODIFIER_LIST_rec => l_x_MODIFIER_LIST_rec         
        ,x_MODIFIER_LIST_val_rec => l_x_MODIFIER_LIST_val_rec 
        ,x_MODIFIERS_tbl => l_x_MODIFIERS_tbl                 
        ,x_MODIFIERS_val_tbl => l_x_MODIFIERS_val_tbl         
        ,x_QUALIFIERS_tbl => l_x_QUALIFIERS_tbl               
        ,x_QUALIFIERS_val_tbl => l_x_QUALIFIERS_val_tbl       
        ,x_PRICING_ATTR_tbl => l_x_PRICING_ATTR_tbl           
        ,x_PRICING_ATTR_val_tbl => l_x_PRICING_ATTR_val_tbl   
        );
        
  COMMIT ; 
  END LOOP ;  
END csp_maintenance ;  

  
  /*************************************************************************
*   PROCEDURE Name: csp_maintenance_process
*
*   PURPOSE:   csp_quote conversion procedure is used for loading converted
*              records from the xxwc.xxwc_csp_interface tables into the
*              CSP Maintenance tables (xxwc.xxwc_om_contractpricing_hdr,
*              xxwc_xxwc.om_contract_pricing_lines)
* Version  Date         Description                                             by        
* 1.0      6/16/2014    Added a logic to populate modifier type DFF correctly. - Ram Talluri TMS # TMS #20140617-00021
* 3.0     9/29/2014     Added last_updated_date and last_updated_by and corrected rounding  - Ram Talluri TMS #20140915-00219 and 20140924-00158  
* 4.0     6/30/2015     TMS # 20150625-00055 - CSP Enhancement Bundle - Item #3 - CSP Maintenance Tool -  Manjula Chellappan	
****************************************************************************/
PROCEDURE csp_maintenance_process(
    errbuf OUT VARCHAR2,
    retcode OUT VARCHAR2)
IS
  l_control_rec QP_GLOBALS.Control_Rec_Type;
  l_return_status VARCHAR2(1);
  x_msg_count     NUMBER;
  x_msg_data      VARCHAR2(2000);
  x_msg_index     NUMBER;
  l_MODIFIER_LIST_rec QP_Modifiers_PUB.Modifier_List_Rec_Type;
  l_MODIFIER_LIST_val_rec QP_Modifiers_PUB.Modifier_List_Val_Rec_Type;
  l_MODIFIERS_tbl QP_Modifiers_PUB.Modifiers_Tbl_Type;
  n_MODIFIERS_tbl QP_Modifiers_PUB.Modifiers_Tbl_Type;
  l_MODIFIERS_val_tbl QP_Modifiers_PUB.Modifiers_Val_Tbl_Type;
  l_QUALIFIERS_tbl QP_Qualifier_Rules_PUB.Qualifiers_Tbl_Type;
  l_QUALIFIERS_val_tbl QP_Qualifier_Rules_PUB.Qualifiers_Val_Tbl_Type;
  l_PRICING_ATTR_tbl QP_Modifiers_PUB.Pricing_Attr_Tbl_Type;
  n_PRICING_ATTR_tbl QP_Modifiers_PUB.Pricing_Attr_Tbl_Type;
  l_PRICING_ATTR_val_tbl QP_Modifiers_PUB.Pricing_Attr_Val_Tbl_Type;
  l_x_MODIFIER_LIST_rec QP_Modifiers_PUB.Modifier_List_Rec_Type;
  l_x_MODIFIER_LIST_val_rec QP_Modifiers_PUB.Modifier_List_Val_Rec_Type;
  l_x_MODIFIERS_tbl QP_Modifiers_PUB.Modifiers_Tbl_Type;
  l_x_MODIFIERS_val_tbl QP_Modifiers_PUB.Modifiers_Val_Tbl_Type;
  l_x_QUALIFIERS_tbl QP_Qualifier_Rules_PUB.Qualifiers_Tbl_Type;
  l_x_QUALIFIERS_val_tbl QP_Qualifier_Rules_PUB.Qualifiers_Val_Tbl_Type;
  l_x_PRICING_ATTR_tbl QP_Modifiers_PUB.Pricing_Attr_Tbl_Type;
  l_x_PRICING_ATTR_val_tbl QP_Modifiers_PUB.Pricing_Attr_Val_Tbl_Type;
  
  l_customer_id NUMBER;
  l_site_id     NUMBER;
  l_org_id      NUMBER;
  l_msg         VARCHAR2 (240); --VARCHAR2 (32000);
  l_conv_flag   VARCHAR2 (1) := 'Y';
  l_date DATE                := SYSDATE;
  l_user_id             NUMBER           := NVL (fnd_profile.VALUE ('USER_ID'), -1);
  l_prod_att            VARCHAR2 (100);
  l_vendor_id           NUMBER;
  l_desc                VARCHAR2 (240);
  l_list_price          NUMBER;
  l_average_cost        NUMBER;
  l_selling_price       NUMBER;
  l_gross_margin        NUMBER;
  i                     NUMBER := 0;
  l_count               NUMBER := NULL ;
  l_agreement_line_id   NUMBER ;
  l_list_line_id        NUMBER ;
  v_arithmetic_operator VARCHAR2 (30);
  v_generate_using_formula_id  NUMBER;
  v_ln_mod_val NUMBER ; 
  v_ln_agree_type VARCHAR2 (240);
  v_product_attribute_context   VARCHAR2 (30) := 'ITEM'; 
  v_product_attr_value          VARCHAR2 (50);
  v_incompatability_group       VARCHAR2 (30);
  v_incomp_group                VARCHAR2 (30);  
  v_list_header_id              NUMBER;    
  v_product_attribute_api       VARCHAR2 (50); 
  v_revision_number NUMBER;  
  l_line_list_price NUMBER;  
  l_line_selling_price NUMBER;  
  l_line_gross_margin NUMBER;  
  l_line_average_cost NUMBER ; 
  l_agreement_id NUMBER ; 
  l_formula_id NUMBER; 
  
  --Cursor to select End Dated lines
  CURSOR c1
  IS
    SELECT a.ROWID, 
    a.*
    FROM XXWC.XXWC_CSP_MAINTENANCE a
    WHERE action = 'ENDDATE' ;
    
  --Cursor to select lines for Update
  CURSOR c2
  IS
    SELECT a.ROWID, 
    a.*
    FROM XXWC.XXWC_CSP_MAINTENANCE a
    WHERE action = 'UPDATE' ;
    
  CURSOR c3
  IS
    SELECT a.ROWID, 
    a.*
    FROM XXWC.XXWC_CSP_MAINTENANCE a
    WHERE action = 'ADD' ;
    
BEGIN

    BEGIN
        l_formula_id:=NULL;
        SELECT Price_Formula_Id
          INTO l_formula_id
          FROM QP_PRICE_FORMULAS
         WHERE UPPER(NAME) = 'PERCENT DISCOUNT' AND ROWNUM = 1;
       EXCEPTION
        WHEN OTHERS THEN
        l_formula_id := NULL;
       END; 


  --ENDDATE
  FOR c1_rec IN c1
  LOOP
    l_modifiers_tbl     := n_modifiers_tbl;
    l_PRICING_ATTR_tbl  := n_PRICING_ATTR_tbl;
    l_prod_att          := NULL;
    l_desc              := NULL;
    l_list_price        := NULL;
    l_average_cost      := NULL;
    l_selling_price     := NULL;
    l_gross_margin      := NULL;
    l_msg               := NULL;
    l_conv_flag         := 'Y';
    i                   := i + 1;
    l_count             := NULL ;
    l_agreement_line_id := NULL ;
    l_list_line_id      := NULL ;
    v_revision_number := NULL ; 
    
    BEGIN
      SELECT a.list_line_id, b.revision_number
      INTO l_list_line_id, v_revision_number
      FROM apps.qp_list_lines a,
        xxwc.xxwc_om_contract_pricing_lines b
      WHERE a.attribute2 = c1_rec.reference_id
      AND a.attribute2 = b.agreement_line_id
      AND b.end_date  IS NULL ;
    EXCEPTION
    WHEN OTHERS THEN
      l_conv_flag := 'E' ;
      FND_FILE.Put_Line(FND_FILE.Log, 'Agreement_Line_ID '||c1_rec.reference_id ||' is not a valid for End Date');
    END;
    
    IF l_conv_flag = 'Y' THEN
    UPDATE XXWC.xxwc_om_contract_pricing_lines
    SET END_DATE            = SYSDATE-1,
      interfaced_flag       = 'Y',
      latest_rec_flag       = 'N',
      line_status           = 'APPROVED',
      revision_number = (v_revision_number + 1),
      last_update_date      = SYSDATE,--TMS #20140915-00219
      last_updated_by       = l_user_id --TMS #20140915-00219
    WHERE agreement_line_id = c1_rec.reference_id;
    /*  Populate API Variables for Modifier Line Level Updates */
    l_MODIFIERS_tbl(1).list_line_id    := l_list_line_id ;
    l_MODIFIERS_tbl(1).end_date_active := SYSDATE-1 ;
    l_MODIFIERS_tbl(1).last_update_date := SYSDATE;--TMS #20140915-00219
    l_MODIFIERS_tbl(1).last_updated_by  := l_user_id; --TMS #20140915-00219
    l_MODIFIERS_tbl(1).operation       := QP_GLOBALS.G_OPR_UPDATE;
    /*  API Call */
    QP_Modifiers_PUB.Process_Modifiers 
    ( p_api_version_number => 1.0 , p_init_msg_list => FND_API.G_FALSE , p_return_values => FND_API.G_FALSE , p_commit => FND_API.G_FALSE , x_return_status => l_return_status , x_msg_count =>x_msg_count , x_msg_data =>x_msg_data ,p_MODIFIER_LIST_rec => l_MODIFIER_LIST_rec ,p_MODIFIERS_tbl => l_MODIFIERS_tbl ,p_QUALIFIERS_tbl => l_QUALIFIERS_tbl ,p_PRICING_ATTR_tbl => l_PRICING_ATTR_tbl ,x_MODIFIER_LIST_rec => l_x_MODIFIER_LIST_rec ,x_MODIFIER_LIST_val_rec => l_x_MODIFIER_LIST_val_rec ,x_MODIFIERS_tbl => l_x_MODIFIERS_tbl ,x_MODIFIERS_val_tbl => l_x_MODIFIERS_val_tbl ,x_QUALIFIERS_tbl => l_x_QUALIFIERS_tbl ,x_QUALIFIERS_val_tbl => l_x_QUALIFIERS_val_tbl ,x_PRICING_ATTR_tbl => l_x_PRICING_ATTR_tbl ,x_PRICING_ATTR_val_tbl => l_x_PRICING_ATTR_val_tbl );
    
    UPDATE XXWC.XXWC_CSP_MAINTENANCE 
    SET process_flag = 'Y'
    WHERE ROWID = c1_rec.ROWID;
    COMMIT ;
    
    ELSE
    UPDATE XXWC.XXWC_CSP_MAINTENANCE 
    SET process_flag = 'E'
    --error_message  = l_msg
    WHERE ROWID      = c1_rec.ROWID;
    COMMIT ;
    END IF ;
  END LOOP ;

--Update Lines
  FOR c2_rec IN c2
  LOOP
    l_modifiers_tbl     := n_modifiers_tbl;
    l_PRICING_ATTR_tbl  := n_PRICING_ATTR_tbl;
    l_prod_att          := NULL;
    l_desc              := NULL;
    l_list_price        := NULL;
    l_average_cost      := NULL;
    l_selling_price     := NULL;
    l_gross_margin      := NULL;
    l_msg               := NULL;
    l_conv_flag         := 'Y';
    i                   := i + 1;
    l_count             := NULL ;
    l_agreement_line_id := NULL ;
    l_list_line_id      := NULL ;
    v_arithmetic_operator := NULL ; 
    v_ln_mod_val  := NULL ; 
    l_line_list_price := NULL ; 
    l_line_selling_price := NULL ; 
    l_line_gross_margin := NULL ; 
    l_line_average_cost := NULL ; 
    v_list_header_id := NULL ; 
    l_prod_att := NULL ; 
    
    
-- Added for Rev 4.0 Begin <<
-- Do not process if modifier_value is NULL

	IF c2_rec.modifier_value IS NULL THEN

	l_conv_flag := 'E' ;
	FND_FILE.Put_Line(FND_FILE.Log, 'Agreement_Line_ID '||c2_rec.reference_id ||' is not valid for UPDATE as modifier value is NULL');

	END IF;

-- Added for Rev 4.0 End >>    

    
    /*Get QP List Line ID, Query will fail if agreement line ID doesn't exist in 
    CSP and QP, CSP is not LAtest Record or CSP is End Dated */
    BEGIN
      SELECT a.list_line_id, b.revision_number, b.list_price, b.selling_price, b.gross_margin, b.average_cost, b.agreement_id, b.product_value
      INTO l_list_line_id, v_revision_number, l_line_list_price, l_line_selling_price, l_line_gross_margin, l_line_average_cost, l_agreement_id, l_prod_att
      FROM apps.qp_list_lines a, 
        xxwc.xxwc_om_contract_pricing_lines b
      WHERE a.attribute2 = c2_rec.reference_id
      AND a.attribute2 = b.agreement_line_id
      AND b.latest_rec_flag = 'Y' 
      and b.end_date is NULL ;
    EXCEPTION
    WHEN OTHERS THEN
      --L_MSG := SUBSTR('Agreement Line ID not valid' || SQLCODE || SQLERRM,1,240) ;
      l_conv_flag := 'E';
      FND_FILE.Put_Line(FND_FILE.Log, 'Agreement_Line_ID '||c2_rec.reference_id ||' is not a valid for Update');
    END;
    
    

              
          /* Set Pricing Method and Modifier Value Variables */
          IF c2_rec.app_method = 'New Price' 
          THEN v_arithmetic_operator := 'NEWPRICE' ;
            v_ln_mod_val := c2_rec.modifier_value ;
          ELSIF c2_rec.app_method = 'Amount' 
          THEN v_arithmetic_operator := 'AMT' ;
            v_ln_mod_val := c2_rec.modifier_value ;
          ELSIF c2_rec.app_method = 'Percent' 
          THEN             v_arithmetic_operator := 'NEWPRICE';  /*TMS: 20131013-00045 Multi Decimal Rounding, Toby Rave, changed from % to NEW PRICE */
            -- 01/30/14 CG: TMS 20140128-00158: Added rounding to 2
            --v_ln_mod_val := round(((100 - c2_rec.modifier_value) / 100),2) ;   /*TMS: 20131013-00045 Multi Decimal Rounding, Toby Rave, calculation added to calulation operand to pass to % discount formula modifier*/
            --Above line is commented for TMS #20140924-00158
            v_ln_mod_val := round(((100 - c2_rec.modifier_value) / 100),5) ;--TMS #20140924-00158
   
          ELSIF c2_rec.app_method = 'Cost Plus' 
          THEN v_arithmetic_operator := 'NEWPRICE' ;
            -- 01/30/14 CG: TMS 20140128-00158: Added rounding to 2
            --v_ln_mod_val := round((c2_rec.modifier_value / 100),2);--TMS #20140924-00158
            v_ln_mod_val := round((c2_rec.modifier_value / 100),5);--TMS #20140924-00158
          Else l_conv_flag := 'E' ; 
            FND_FILE.Put_Line(FND_FILE.Log, 'Agreement_Line_ID '||c2_rec.reference_id ||' Modifier Type is not Valid');
          END IF;
          
          IF c2_rec.app_method = 'Cost Plus' 
          THEN v_generate_using_formula_id := 7030 ;
           /*TMS: 20131013-00045 Multi Decimal Rounding, Toby Rave, block added to set % discounts to formual based*/
         ELSIF c2_rec.app_method = 'Percent'
         THEN
            v_generate_using_formula_id := l_formula_id ;
         ELSE
            v_generate_using_formula_id := NULL;
         END IF;

          /***Added per TMS 20131013-00042 to refresh List Price, selling price, and gross margin 
          Toby Rave Nov 22, 2013 ***/
          
          begin
          select a.organization_id
          into l_org_id
          from xxwc.xxwc_om_contract_pricing_hdr a,
          xxwc.xxwc_om_contract_pricing_lines b
          where b.agreement_line_id = c2_rec.reference_id 
          and a.agreement_id = b.agreement_id ; 
           EXCEPTION
            WHEN OTHERS THEN
            l_org_id := 999999 ;
          FND_FILE.Put_Line(FND_FILE.Log, 'Agreement_ID '||c2_rec.reference_id ||' and '||c2_rec.product_value||' No Header Level Org');
          --l_conv_flag := 'E';
          END ; 
          
          
          /***Added for TMS 20131206-00109 ***/
          IF c2_rec.product_attribute = 'ITEM' THEN
            BEGIN
              SELECT a.operand
              INTO l_list_price
              FROM apps.qp_list_lines a,
                apps.qp_pricing_attributes b,
                apps.mtl_system_items_b c,
                apps.qp_list_headers d,
                qp_qualifiers e
              WHERE a.list_header_id     = b.list_header_id
              AND a.list_line_id         = b.list_line_id
              AND d.list_header_id       = e.list_header_id
              AND d.attribute10          = 'Market Price List'
              AND d.name NOT            IN ('MARKET NATIONAL','MARKET CATEGORY')
              AND d.list_header_id       = a.list_header_id
              AND d.list_header_id       = b.list_header_id
              AND b.product_attr_value   = c.inventory_item_id
              AND c.organization_id      = '222'
              AND e.list_header_id       = d.list_header_id
              AND e.qualifier_attr_value = l_org_id
              AND e.qualifier_attribute  = 'QUALIFIER_ATTRIBUTE18'
              AND c.segment1             = l_prod_att ; 
          EXCEPTION
            WHEN OTHERS THEN l_list_price := NULL ; 
          END ;
          
          IF l_list_price IS NULL THEN
            BEGIN
              SELECT a.operand
              INTO l_list_price
              FROM apps.qp_list_lines a,
                apps.qp_pricing_attributes b,
                apps.mtl_system_items_b c,
                apps.qp_list_headers d
              WHERE a.list_header_id   = b.list_header_id
              AND a.list_line_id       = b.list_line_id
              AND d.name               = 'MARKET NATIONAL'
              AND d.list_header_id     = a.list_header_id
              AND d.list_header_id     = b.list_header_id
              AND b.product_attr_value = c.inventory_item_id
              AND c.organization_id    = '222'
              AND c.segment1           = l_prod_att ; 
          EXCEPTION
            WHEN OTHERS THEN l_list_price := NULL ; 
          END ;
          END IF ; 
          
         IF l_list_price IS NULL THEN
            BEGIN
              SELECT a.operand
              INTO l_list_price
              FROM apps.qp_list_lines a,
                apps.qp_pricing_attributes b,
                apps.mtl_system_items_b c,
                apps.qp_list_headers d
              WHERE a.list_header_id   = b.list_header_id
              AND a.list_line_id       = b.list_line_id
              AND d.name               = 'MARKET CATEGORY'
              AND d.list_header_id     = a.list_header_id
              AND d.list_header_id     = b.list_header_id
              AND b.product_attr_value = c.inventory_item_id
              AND c.organization_id    = '222'
              AND c.segment1           = l_prod_att ; 
          EXCEPTION
            WHEN OTHERS THEN l_list_price := l_line_list_price ; 
          END ;
          END IF ; 
          
          
          
          
          
          
          
          
        /*** Removed for TMS 20131206-00109
        BEGIN
          SELECT ll.operand
          INTO l_list_price
          FROM qp_secu_list_headers_v lh ,
            xxwc.xxwc_qp_list_lines_mv ll,
            mtl_system_items a
          WHERE lh.list_type_code = 'PRL'
          AND lh.name =  ('MARKET NATIONAL')
          AND lh.list_header_id = ll.list_header_id
          AND SYSDATE BETWEEN NVL (ll.start_date_active , SYSDATE - 1) AND NVL (ll.end_date_active , SYSDATE + 1)
          AND ll.product_id     = a.inventory_item_id
          AND a.organization_id = 222 --c2_rec.organization_id
          AND a.segment1        = c2_rec.PRODUCT_VALUE
          AND ROWNUM            = 1;
        EXCEPTION
        WHEN OTHERS THEN
        l_list_price := l_line_list_price ;
        --FND_FILE.Put_Line(FND_FILE.Log, 'Agreement_ID '||c2_rec.reference_id ||' and '||c2_rec.product_value||' Missing List Price');
       --l_conv_flag := 'E';
        END;***/
        
        BEGIN
          SELECT ROUND ( cst_cost_api.get_item_cost (1 , inventory_item_id , l_org_id) , 5)
          INTO l_average_cost
          FROM mtl_system_items
          WHERE segment1      = c2_rec.PRODUCT_VALUE
          AND organization_id = l_org_id ;
        EXCEPTION
        WHEN OTHERS THEN
        l_average_cost := l_line_average_cost ;
        END;  
         END IF ;
          
          
          /****Removed per TMS 20131206-00109 ****/
          /***Calculate Selling Price 
           BEGIN
          SELECT average_cost, selling_price
          INTO l_average_cost, l_list_price
          FROM xxwc.xxwc_om_contract_pricing_lines
          WHERE agreement_line_id = c2_rec.reference_id ;
        EXCEPTION
        WHEN OTHERS THEN
        FND_FILE.Put_Line(FND_FILE.Log, 'Agreement_Line_ID '||c2_rec.reference_id ||' Missing Cost or List Price');
          l_conv_flag := 'E';
        END; ***/
        
        
        IF c2_rec.app_method    = 'New Price' THEN
          l_selling_price      := c2_rec.modifier_value;
        ELSIF c2_rec.app_method = 'Percent' 
        and l_list_price is not NULL
        THEN 
            -- 01/30/14 CG: TMS 20140128-00158: Added rounding to 2    
            l_selling_price      := round((l_list_price * (100 - c2_rec.modifier_value) / 100),2);
        --FND_FILE.Put_Line(FND_FILE.Log, 'Agreement_ID '||c2_rec.reference_id ||' and '||c2_rec.product_value|| l_list_price || ' Missing List Price 2');
        ELSIF c2_rec.app_method = 'Amount' THEN
          l_selling_price      := l_list_price - c2_rec.modifier_value;
        ELSIF c2_rec.app_method = 'Cost Plus' 
        and l_average_cost is not null THEN
          l_selling_price      := ROUND (l_average_cost /  ((100 - c2_rec.modifier_value)/100), 2) ;
        ELSE l_selling_price := l_line_selling_price ;
        END IF;
        
        
        
        IF l_selling_price IS NOT NULL AND l_average_cost IS NOT NULL AND c2_rec.agreement_type = 'CSP' THEN
          l_gross_margin   := ROUND ( (l_selling_price - l_average_cost) / l_selling_price * 100 , 2) ;
        END IF;
        
        
          IF l_selling_price IS NOT NULL AND c2_rec.special_cost IS NOT NULL AND c2_rec.agreement_type = 'VQN' THEN
          l_gross_margin   := ROUND ( (l_selling_price - c2_rec.special_cost) / l_selling_price * 100 , 2);
          END IF; 
          
          IF l_gross_margin IS NULL THEN
          l_gross_margin := l_line_gross_margin ;
          END IF ; 
          
        
        IF c2_rec.app_method = 'Cost Plus'
        THEN l_gross_margin := c2_rec.modifier_value ; 
        END IF ; 
        
        
    IF l_conv_flag = 'Y' THEN
    
          update xxwc.xxwc_om_contract_pricing_lines
          set modifier_type = c2_rec.app_method, 
              modifier_value = c2_rec.modifier_value,
              selling_price = l_selling_price,
              gross_margin = l_gross_margin,
              revision_number = (v_revision_number + 1),
              average_cost = l_average_cost,
              list_price = l_list_price,
              last_update_date = SYSDATE,--TMS #20140915-00219
              last_updated_by  = l_user_id --TMS #20140915-00219
        where agreement_line_id = c2_rec.reference_id ; 
        
            UPDATE XXWC.XXWC_CSP_MAINTENANCE 
            SET process_flag = 'Y'
            WHERE ROWID = c2_rec.ROWID;
        
          /*  Populate API Variables for Modifier Line Level Updates */
            l_MODIFIERS_tbl(1).list_line_id := l_list_line_id ;
            l_MODIFIERS_tbl(1).arithmetic_operator := v_arithmetic_operator ;
            l_MODIFIERS_tbl(1).operand := v_ln_mod_val ;
            l_MODIFIERS_tbl(1).price_by_formula_id := v_generate_using_formula_id ;
            l_MODIFIERS_tbl (1).ATTRIBUTE2 := c2_rec.reference_id;
            l_MODIFIERS_tbl (1).price_by_formula_id :=v_generate_using_formula_id;
            l_MODIFIERS_tbl(1).last_update_date := SYSDATE;--TMS #20140915-00219
            l_MODIFIERS_tbl(1).last_updated_by  := l_user_id; --TMS #20140915-00219
            l_MODIFIERS_tbl(1).operation := QP_GLOBALS.G_OPR_UPDATE; 
          
      /*  API Call */
        QP_Modifiers_PUB.Process_Modifiers
        ( p_api_version_number => 1.0
        , p_init_msg_list => FND_API.G_FALSE
        , p_return_values => FND_API.G_FALSE
        , p_commit => FND_API.G_FALSE
        , x_return_status => l_return_status
        , x_msg_count =>x_msg_count
        , x_msg_data =>x_msg_data
        ,p_MODIFIER_LIST_rec => l_MODIFIER_LIST_rec
        ,p_MODIFIERS_tbl => l_MODIFIERS_tbl
        ,p_QUALIFIERS_tbl => l_QUALIFIERS_tbl
        ,p_PRICING_ATTR_tbl => l_PRICING_ATTR_tbl
        ,x_MODIFIER_LIST_rec => l_x_MODIFIER_LIST_rec         
        ,x_MODIFIER_LIST_val_rec => l_x_MODIFIER_LIST_val_rec 
        ,x_MODIFIERS_tbl => l_x_MODIFIERS_tbl                 
        ,x_MODIFIERS_val_tbl => l_x_MODIFIERS_val_tbl         
        ,x_QUALIFIERS_tbl => l_x_QUALIFIERS_tbl               
        ,x_QUALIFIERS_val_tbl => l_x_QUALIFIERS_val_tbl       
        ,x_PRICING_ATTR_tbl => l_x_PRICING_ATTR_tbl           
        ,x_PRICING_ATTR_val_tbl => l_x_PRICING_ATTR_val_tbl   
        );
        
        
        
      ELSE
      UPDATE XXWC.XXWC_CSP_MAINTENANCE 
      SET process_flag = 'E'
      WHERE ROWID = c2_rec.ROWID;
      COMMIT ;
      END IF ;
    END LOOP ;
    
/***Line Additions***/    
FOR c3_rec IN c3
  LOOP
    l_modifiers_tbl     := n_modifiers_tbl;
    l_PRICING_ATTR_tbl  := n_PRICING_ATTR_tbl;
    l_prod_att          := NULL;
    l_desc              := NULL;
    l_list_price        := NULL;
    l_average_cost      := NULL;
    l_selling_price     := NULL;
    l_gross_margin      := NULL;
    l_msg               := NULL;
    l_conv_flag         := 'Y';
    i                   := i + 1;
    l_count             := NULL ;
    l_agreement_line_id := NULL ;
    l_list_line_id      := NULL ;
    v_arithmetic_operator := NULL ; 
    v_ln_mod_val  := NULL ; 
    l_org_id := NULL ; 
    v_product_attr_value := NULL ;
    v_incompatability_group := NULL ;
    v_incomp_group := NULL ;
    v_list_header_id := NULL ;
    v_product_attribute_api := NULL ;
    
    
           Begin
             select agreement_line_id, revision_number
             INTO l_count, v_revision_number
             FROM XXWC.xxwc_om_contract_pricing_lines
             WHERE product_value = c3_rec.product_value
             and agreement_id = c3_rec.reference_id 
             and end_date is null ; 
           EXCEPTION
            WHEN OTHERS THEN
            l_count := NULL ; 
          END ; 

          IF  l_count is not NULL
          THEN FND_FILE.Put_Line(FND_FILE.Log, 'Agreement_ID '||c3_rec.reference_id ||' and '||c3_rec.product_value||' has an active record');
          l_conv_flag := 'E';
          END IF ; 
		  
		  
-- Added for Rev 4.0 Begin <<
-- Do not process if modifier_value is NULL

	IF c3_rec.modifier_value IS NULL THEN

	l_conv_flag := 'E' ;
	FND_FILE.Put_Line(FND_FILE.Log, 'Agreement_Line_ID '||c3_rec.reference_id ||' is not valid for ADD as modifier value is NULL');

	END IF;

-- Added for Rev 4.0 End >>    
		  
          
          Begin
          select list_header_id
          into v_list_header_id
          from apps.qp_list_headers
          where attribute14 = c3_rec.reference_id ;
          EXCEPTION
          WHEN OTHERS THEN 
        FND_FILE.Put_Line(FND_FILE.Log, 'Agreement_ID '||c3_rec.reference_id ||' and '||c3_rec.product_value||' Modifier Does not Exist');
          l_conv_flag := 'E';
        END; 
          
          
          BEGIN
         SELECT incompatability_group
           INTO v_incompatability_group
           FROM xxwc.xxwc_om_contract_pricing_hdr
          WHERE agreement_id = c3_rec.reference_id;        
        EXCEPTION
          WHEN OTHERS THEN 
        FND_FILE.Put_Line(FND_FILE.Log, 'Agreement_ID '||c3_rec.reference_id ||' and '||c3_rec.product_value||' No Imcompatibility Group');
          l_conv_flag := 'E';
        END; 
         
         
       IF v_incompatability_group = 'Best Price Wins'
         THEN
            v_incomp_group := 'LVL 1';
         ELSIF v_incompatability_group = 'Exclusive'
         THEN
            v_incomp_group := 'EXCL';
         ELSE
            l_conv_flag := 'E';
         END IF;
          
          begin
          select organization_id
          into l_org_id
          from xxwc.xxwc_om_contract_pricing_hdr
          where agreement_id = c3_rec.reference_id ;
           EXCEPTION
            WHEN OTHERS THEN
          FND_FILE.Put_Line(FND_FILE.Log, 'Agreement_ID '||c3_rec.reference_id ||' and '||c3_rec.product_value||' No Header Level Org');
          l_conv_flag := 'E';
          END ; 
          

        IF c3_rec.product_attribute = 'ITEM'
         THEN
            v_product_attribute_api := 'PRICING_ATTRIBUTE1';
         ELSIF c3_rec.product_attribute = 'CAT_CLASS'
         THEN
            v_product_attribute_api := 'PRICING_ATTRIBUTE2';
         ELSIF c3_rec.product_attribute = 'CATEGORY'
         THEN
            v_product_attribute_api := 'PRICING_ATTRIBUTE26';
         ELSE
          FND_FILE.Put_Line(FND_FILE.Log, 'Agreement_ID '||c3_rec.reference_id ||' and '||c3_rec.product_value ||' Invalid Product Attribute');
          l_conv_flag := 'E';
         END IF;
      
      
       IF c3_rec.product_attribute = 'ITEM'
         THEN
               BEGIN
          SELECT inventory_item_id,
            description
          INTO v_product_attr_value,
            l_desc
          FROM mtl_system_items
          WHERE segment1      = c3_rec.product_value
          AND organization_id = l_org_id ;
        EXCEPTION
        WHEN OTHERS THEN
         FND_FILE.Put_Line(FND_FILE.Log, 'Agreement_ID '||c3_rec.reference_id ||' and '||c3_rec.product_value||' not a valid item for the organization');
          l_conv_flag := 'E';
        END;
  
         ELSIF c3_rec.product_attribute = 'CAT_CLASS'
         THEN
          BEGIN
            SELECT category_id
              INTO v_product_attr_value
              FROM mtl_categories_b
             WHERE segment1 || '.' || segment2 = c3_rec.product_value ;
           EXCEPTION
            WHEN OTHERS THEN 
          FND_FILE.Put_Line(FND_FILE.Log, 'Agreement_ID '||c3_rec.reference_id ||' and '||c3_rec.product_value||' not a valid Cat Class');
          l_conv_flag := 'E';
          END; 
       /* Value from CSP Line Line used as variable for CATEGORY insert */   
         ELSIF c3_rec.product_attribute = 'CATEGORY'
         THEN
            v_product_attr_value := c3_rec.product_value ;
         ELSE
          FND_FILE.Put_Line(FND_FILE.Log, 'Agreement_ID '||c3_rec.reference_id ||' and '||c3_rec.product_value||' not a valid Product Attribute');
          l_conv_flag := 'E';
         END IF;
      
      
      
       IF c3_rec.agreement_type = 'VQN' THEN
        BEGIN
          SELECT vendor_id
          INTO l_vendor_id
          FROM ap_suppliers
          WHERE segment1 = c3_rec.vendor_number;
        EXCEPTION
        WHEN OTHERS THEN
       FND_FILE.Put_Line(FND_FILE.Log, 'Agreement_ID '||c3_rec.reference_id ||' and '||c3_rec.product_value||' Invalid Vendor');
          l_conv_flag := 'E';
        END;
        ELSE l_vendor_id := NULL ; 
        END IF ; 
 
      
      IF c3_rec.product_attribute = 'ITEM' THEN
        BEGIN
          SELECT ll.operand
          INTO l_list_price
          FROM qp_secu_list_headers_v lh ,
            xxwc.xxwc_qp_list_lines_mv ll,
            mtl_system_items a
          WHERE lh.list_type_code = 'PRL'
          AND lh.name =  ('MARKET NATIONAL')
          AND lh.list_header_id = ll.list_header_id
          AND SYSDATE BETWEEN NVL (ll.start_date_active , SYSDATE - 1) AND NVL (ll.end_date_active , SYSDATE + 1)
          AND ll.product_id     = a.inventory_item_id
          AND a.organization_id = 222 --c2_rec.organization_id
          AND a.segment1        = c3_rec.PRODUCT_VALUE
          AND ROWNUM            = 1;
        EXCEPTION
        WHEN OTHERS THEN
        --l_list_price := 0 ;
       FND_FILE.Put_Line(FND_FILE.Log, 'Agreement_ID '||c3_rec.reference_id ||' and '||c3_rec.product_value||' Missing List Price');
       l_conv_flag := 'E';
        END;
        
        BEGIN
          SELECT ROUND ( cst_cost_api.get_item_cost (1 , inventory_item_id , l_org_id) , 5)
          INTO l_average_cost
          FROM mtl_system_items
          WHERE segment1      = c3_rec.PRODUCT_VALUE
          AND organization_id = l_org_id ;
        EXCEPTION
        WHEN OTHERS THEN
        --l_average_cost := 0 ;
       FND_FILE.Put_Line(FND_FILE.Log, 'Agreement_ID '||c3_rec.reference_id ||' and '||c3_rec.product_value||' Missing Average Cost');
        l_conv_flag := 'E';
        END;  
         END IF ;
        
/*Set Modifier Type, Modifier Value and Selling Price*/
        IF c3_rec.app_method    = 'New Price' THEN
          v_arithmetic_operator := 'NEWPRICE' ;
          l_selling_price      := c3_rec.modifier_value;
          v_ln_mod_val := c3_rec.modifier_value ;
        ELSIF c3_rec.app_method = 'Percent' THEN
          v_arithmetic_operator := 'NEWPRICE';  /*TMS: 20131013-00045 Multi Decimal Rounding, Toby Rave, changed from % to NEW PRICE */
          -- 01/30/14 CG: TMS 20140128-00158: Added rounding to 2
          l_selling_price      := round((l_list_price * (100 - c3_rec.modifier_value) / 100),2); 
          --v_ln_mod_val := round(((100 - c3_rec.modifier_value) / 100),2) ;   /*TMS: 20131013-00045 Multi Decimal Rounding, Toby Rave, calculation added to calulation operand to pass to % discount formula modifier*/
          --above line is commented for TMS 20140924-00158
          v_ln_mod_val := round(((100 - c3_rec.modifier_value) / 100),5) ;--TMS 20140924-00158
          
        ELSIF c3_rec.app_method = 'Amount' THEN
          l_selling_price      := l_list_price - c3_rec.modifier_value;
          v_arithmetic_operator := 'AMT' ;
          v_ln_mod_val := c3_rec.modifier_value ;
        ELSIF c3_rec.app_method = 'Cost Plus' THEN
            v_arithmetic_operator := 'NEWPRICE' ;
            l_selling_price      := ROUND (l_average_cost /  ((100 - c3_rec.modifier_value)/100), 2) ;
            -- 01/30/14 CG: TMS 20140128-00158: Added rounding to 2
           -- v_ln_mod_val := round((c3_rec.modifier_value / 100),2);--TMS 20140924-00158
            v_ln_mod_val := round((c3_rec.modifier_value / 100),5);--TMS 20140924-00158
        Else l_conv_flag := 'E' ; 
       FND_FILE.Put_Line(FND_FILE.Log, 'Agreement_ID '||c3_rec.reference_id ||' and '||c3_rec.product_value||' Invalid Modifier Type');
        END IF;
        
        
      /*Set Cost Plus Formula if Application Method is Cost Plus */         
          IF c3_rec.app_method = 'Cost Plus' 
          THEN v_generate_using_formula_id := 7030 ;
        /*TMS: 20131013-00045 Multi Decimal Rounding, Toby Rave, block added to set % discounts to formual based*/
         ELSIF c3_rec.app_method = 'Percent'
         THEN
            v_generate_using_formula_id := l_formula_id ;
         ELSE
            v_generate_using_formula_id := NULL ;
         END IF;
    
    
       IF l_selling_price IS NOT NULL AND l_average_cost IS NOT NULL AND c3_rec.agreement_type = 'CSP' THEN
          l_gross_margin   := ROUND ( (l_selling_price - l_average_cost) / l_selling_price * 100 , 2);
        END IF;
        IF l_selling_price IS NOT NULL AND c3_rec.special_cost IS NOT NULL AND c3_rec.agreement_type = 'VQN' THEN
          l_gross_margin   := ROUND ( (l_selling_price - c3_rec.special_cost) / l_selling_price * 100 , 2);
        END IF;

      
      IF l_conv_flag = 'Y' THEN
      INSERT
        INTO xxwc_om_contract_pricing_lines
          (
            AGREEMENT_LINE_ID ,
            AGREEMENT_ID ,
            AGREEMENT_TYPE ,
            VENDOR_QUOTE_NUMBER ,
            PRODUCT_ATTRIBUTE ,
            PRODUCT_VALUE ,
            PRODUCT_DESCRIPTION ,
            LIST_PRICE ,
            START_DATE ,
            END_DATE ,
            MODIFIER_TYPE ,
            MODIFIER_VALUE ,
            SPECIAL_COST ,
            VENDOR_ID ,
            AVERAGE_COST ,
            GROSS_MARGIN ,
            LINE_STATUS ,
            REVISION_NUMBER ,
            ATTRIBUTE10 ,
            ATTRIBUTE1 ,
            CREATION_DATE ,
            CREATED_BY ,
            LAST_UPDATE_DATE ,
            LAST_UPDATED_BY ,
            LAST_UPDATE_LOGIN ,
            INTERFACED_FLAG ,
            SELLING_PRICE ,
            LATEST_REC_FLAG
          )
          VALUES
          (
            XXWC_OM_CONTRACT_PRICING_LIN_S.NEXTVAL ,
            c3_rec.reference_id ,
            c3_rec.agreement_type ,
            c3_rec.vq_number ,
            c3_rec.product_attribute ,
            c3_rec.product_value ,
            l_desc ,
            l_list_price ,
            SYSDATE ,
            NULL ,
            c3_rec.app_method ,
            c3_rec.modifier_value ,
            c3_rec.special_cost ,
            l_vendor_id ,
            l_average_cost ,
            l_gross_margin ,
            'APPROVED' ,
            1 ,
            NULL ,
            NULL ,
            l_date ,
            l_user_id ,
            l_date ,
            l_user_id ,
            0 ,
            'Y' ,
            l_selling_price ,
            'Y'
          );
          
          --  6/16/2014 below IF statement added to populate modifier type DFF correctly. - Ram Talluri TMS #20140617-00021
          /*Set Line Line DFF on Modifier to CSP or VQN */
         IF c3_rec.agreement_type = 'VQN'
         THEN
            v_ln_agree_type := 'Vendor Quote';
         ELSIF c3_rec.agreement_type = 'CSP'
         THEN
            v_ln_agree_type := 'Contract Pricing';
         ELSE
            v_ln_agree_type := NULL;
         END IF;
         
          
         /* Set Modifier Line API Variables */
         l_MODIFIERS_tbl (1).list_header_id := v_list_header_id ;
         l_MODIFIERS_tbl (1).list_line_type_code := 'DIS';
         l_MODIFIERS_tbl (1).automatic_flag := 'Y';
         l_MODIFIERS_tbl (1).modifier_level_code := 'LINE';
         l_MODIFIERS_tbl (1).accrual_flag := 'N';
         l_MODIFIERS_tbl (1).start_date_active := SYSDATE ;
         l_MODIFIERS_tbl (1).end_date_active := NULL;
         l_MODIFIERS_tbl (1).arithmetic_operator := v_arithmetic_operator;
         l_MODIFIERS_tbl (1).pricing_group_sequence := 1;     --Pricing Bucket
         l_MODIFIERS_tbl (1).pricing_phase_id := 2; --Pricing Phase, '2' = List Line Adjustment
         l_MODIFIERS_tbl (1).product_precedence := 220;
         l_MODIFIERS_tbl (1).operand := v_ln_mod_val;
         l_MODIFIERS_tbl (1).INCLUDE_ON_RETURNS_FLAG := 'Y' ;
         l_MODIFIERS_tbl (1).context := '162';
         l_MODIFIERS_tbl (1).ATTRIBUTE2 := XXWC_OM_CONTRACT_PRICING_LIN_S.CURRVAL ;
         l_MODIFIERS_tbl (1).ATTRIBUTE3 := c3_rec.vendor_number;
         l_MODIFIERS_tbl (1).ATTRIBUTE4 := c3_rec.special_cost;
         l_MODIFIERS_tbl (1).ATTRIBUTE5 := v_ln_agree_type;
         l_MODIFIERS_tbl (1).ATTRIBUTE6 := c3_rec.vq_number;
         l_MODIFIERS_tbl (1).price_by_formula_id :=v_generate_using_formula_id;
         l_MODIFIERS_tbl (1).incompatibility_grp_code := v_incomp_group; --'LVL 1' or 'EXCL'
         l_MODIFIERS_tbl(1).last_update_date := SYSDATE;--TMS #20140915-00219
         l_MODIFIERS_tbl(1).last_updated_by  := l_user_id; --TMS #20140915-00219
         l_MODIFIERS_tbl (1).operation := QP_GLOBALS.G_OPR_CREATE;
         
        /* Set Product Attribute API Variables */
         l_PRICING_ATTR_tbl (1).list_header_id := v_list_header_id ;
         l_PRICING_ATTR_tbl (1).product_attribute_context := v_product_attribute_context;       --v_product_attribute_context ;
         l_PRICING_ATTR_tbl (1).product_attribute := v_product_attribute_api ;
         l_PRICING_ATTR_tbl (1).product_attr_value := v_product_attr_value;
         --l_PRICING_ATTR_tbl(i).product_uom_code:= 'Ea';
         l_PRICING_ATTR_tbl (1).MODIFIERS_index := 1;
         l_PRICING_ATTR_tbl (1).operation := QP_GLOBALS.G_OPR_CREATE;
         
     /*Update line level revision number to bext revision*/    
    /*Update interfaced flag to Yes * once the line has been processed*/
    /*  Update CSP line status to approved */    
        --UPDATE xxwc.xxwc_om_contract_pricing_lines
        --SET revision_number = (v_revision_number + 1)
         -- , interfaced_flag = 'Y'
         -- , line_status = 'APPROVED' 
       -- WHERE agreement_line_id = c2_rec.agreement_line_id ; 
  

      QP_Modifiers_PUB.Process_Modifiers (
         p_api_version_number      => 1.0
       , p_init_msg_list           => FND_API.G_FALSE
       , p_return_values           => FND_API.G_FALSE
       , p_commit                  => FND_API.G_FALSE
       , x_return_status           => l_return_status
       , x_msg_count               => x_msg_count
       , x_msg_data                => x_msg_data
       , p_MODIFIER_LIST_rec       => l_MODIFIER_LIST_rec
       , p_MODIFIERS_tbl           => l_MODIFIERS_tbl
       , p_QUALIFIERS_tbl          => l_QUALIFIERS_tbl
       , p_PRICING_ATTR_tbl        => l_PRICING_ATTR_tbl
       , x_MODIFIER_LIST_rec       => l_x_MODIFIER_LIST_rec
       , x_MODIFIER_LIST_val_rec   => l_x_MODIFIER_LIST_val_rec
       , x_MODIFIERS_tbl           => l_x_MODIFIERS_tbl
       , x_MODIFIERS_val_tbl       => l_x_MODIFIERS_val_tbl
       , x_QUALIFIERS_tbl          => l_x_QUALIFIERS_tbl
       , x_QUALIFIERS_val_tbl      => l_x_QUALIFIERS_val_tbl
       , x_PRICING_ATTR_tbl        => l_x_PRICING_ATTR_tbl
       , x_PRICING_ATTR_val_tbl    => l_x_PRICING_ATTR_val_tbl);
          
          
          
          
     ELSE
      UPDATE XXWC.XXWC_CSP_MAINTENANCE 
      SET process_flag = 'E'
      WHERE ROWID = c3_rec.ROWID;
      COMMIT ;
      END IF ;
    END LOOP ;
          
          

  END csp_maintenance_process ;

/*************************************************************************
*   PROCEDURE Name: create exclusions
*
*   PURPOSE:   create exclusions procedure is used to create 
*              'Item/Category Class' , 'Item/Category' Exclusions, 
*              and 'Category Class/Category Exclusions on modifier list lines
****************************************************************************/
   PROCEDURE csp_maintenance_exclusions(
    errbuf OUT VARCHAR2,
    retcode OUT VARCHAR2)
   IS
      /* $Header: QPXEXDS1.sql 115.6 2006/08/24 04:51:05 nirmkuma ship $ */
      l_control_rec                 QP_GLOBALS.Control_Rec_Type;
      l_return_status               VARCHAR2 (1);
      x_msg_count                   NUMBER;
      x_msg_data                    VARCHAR2 (2000);
      x_msg_index                   NUMBER;

      l_MODIFIER_LIST_rec           QP_Modifiers_PUB.Modifier_List_Rec_Type;
      l_MODIFIER_LIST_val_rec       QP_Modifiers_PUB.Modifier_List_Val_Rec_Type;
      l_MODIFIERS_tbl               QP_Modifiers_PUB.Modifiers_Tbl_Type;
      l_MODIFIERS_val_tbl           QP_Modifiers_PUB.Modifiers_Val_Tbl_Type;
      l_QUALIFIERS_tbl              QP_Qualifier_Rules_PUB.Qualifiers_Tbl_Type;
      l_QUALIFIERS_val_tbl          QP_Qualifier_Rules_PUB.Qualifiers_Val_Tbl_Type;
      l_PRICING_ATTR_tbl            QP_Modifiers_PUB.Pricing_Attr_Tbl_Type;
      l_PRICING_ATTR_val_tbl        QP_Modifiers_PUB.Pricing_Attr_Val_Tbl_Type;

      l_x_MODIFIER_LIST_rec         QP_Modifiers_PUB.Modifier_List_Rec_Type;
      l_x_MODIFIER_LIST_val_rec     QP_Modifiers_PUB.Modifier_List_Val_Rec_Type;
      l_x_MODIFIERS_tbl             QP_Modifiers_PUB.Modifiers_Tbl_Type;
      l_x_MODIFIERS_val_tbl         QP_Modifiers_PUB.Modifiers_Val_Tbl_Type;
      l_x_QUALIFIERS_tbl            QP_Qualifier_Rules_PUB.Qualifiers_Tbl_Type;
      l_x_QUALIFIERS_val_tbl        QP_Qualifier_Rules_PUB.Qualifiers_Val_Tbl_Type;
      l_x_PRICING_ATTR_tbl          QP_Modifiers_PUB.Pricing_Attr_Tbl_Type;
      l_x_PRICING_ATTR_val_tbl      QP_Modifiers_PUB.Pricing_Attr_Val_Tbl_Type;

      --Toby's Variables
      --CSP Header Level Variables
      v_agreement_id                NUMBER ;
      v_agreement_type              VARCHAR2 (30);
      v_vendor_quote_number         NUMBER;
      v_price_type                  VARCHAR2 (30);
      v_customer_id                 NUMBER;
      v_customer_site_id            NUMBER;
      v_agreement_status            VARCHAR2 (30);
      v_revision_number             NUMBER;
      v_incompatibility_group       VARCHAR2 (30);
      v_attribute_10                VARCHAR2 (25) := 'Contract Pricing'; --Variable to populate DFF Attribute 10 on Modifier Header (Contract Pricing or Vendor Quote)
      v_attribute_11                NUMBER := NULL;
      v_attribute_12                VARCHAR2 (25) := NULL; --Vendor Name for VQN
      v_attribute_13                VARCHAR2 (25) := NULL; --VEndor Number for VQN
      v_attribute_14                NUMBER; --Mapped to Vendor Agreement Number DFF on Modifier Header
      v_attribute_15                NUMBER; --Mapped to Agreement Revision Number DFF on Modifier Header
      v1_price_type                 VARCHAR2 (30);
      v_customer_name               VARCHAR2 (50);
      v_party_id                    NUMBER;
      v_party_name                  VARCHAR2 (360);
      v_party_site_number           NUMBER;
      v_qualifier_value             NUMBER;
      v_agreeid                     NUMBER;
      v_qual_context                VARCHAR2 (360);
      v_excluder_flag               VARCHAR2 (1);
      v_operator_code               VARCHAR (360);
      v_delimitier                  VARCHAR2 (1);
      v_qualifier_grouping_no       NUMBER;
      v_qualifier_precedence        NUMBER;
      v_operation                   VARCHAR2 (30);
      v_active_flag                 VARCHAR2 (30);
      v_list_header_id              NUMBER;
      v_list_line_id                NUMBER;
      --START XXWC_CSP_LINES VARIABLES
      v_ln_id                       NUMBER; --Line Level Variable for xxwc.agreement_line_id
      v_ln_agreement_type           VARCHAR2 (30); --Line Level Variable for xxwc.agreement_type
      v_ln_vendor_quote_number      VARCHAR2 (30); --Line Level Variable for XXWC.VENDOR_QUOTE_NUMBER
      v_ln_start_date               DATE; --Line Level Variable for XXWC.START_DATE
      v_ln_end_date                 DATE; --Line Level Variable for XXWC.END_DATE
      v_ln_mod_type                 VARCHAR2 (30); --Line Level Variable for Modifier Type
      v_arithmetic_operator         VARCHAR2 (30); --Line Level Variable for Modifier Type API Value
      v_ln_mod_value                NUMBER; --Line Level Variable for Modifier Value
      --v_ln_special_cost, --Line Level Variable for Special Cost
      --v_ln_vendor_id --Line LEvel Variable for Vendor ID
      v_product_attribute           VARCHAR2 (50); --Line Level Variable for Product Attribute (Item/Cat Class)
      v_product_value               VARCHAR2 (50); --Line Level Variable for Product Value (SKU Number)
      v_product_attribute_api       VARCHAR2 (50); --Line Level Variable to set in API
      v_product_attribute_context   VARCHAR2 (30) := 'ITEM'; --Variable to set product_attribute_context in Pricing Attr API
      v_product_attr_value          VARCHAR2 (50);
      v_generate_using_formula_id   NUMBER;
      v1                            NUMBER;            --Needed for Exclusions
      v2                            NUMBER;            --Needed for Exclusions
      v3                            NUMBER;            --Needed for Exclusions
      v4                            VARCHAR2 (50);     --Needed for Exclusions
      v5                            NUMBER;            --Needed for Exclusions
      V6                            NUMBER;
      p_agreement_id                NUMBER;
      L_MSG                         VARCHAR2(240) ; 
      
      
      

      CURSOR AGREEMENT_ID
      IS
      select distinct(reference_id)
      from xxwc.xxwc_csp_maintenance
      where action = 'ADD' ; 

      CURSOR AGREE_LINE_ID
      IS
         SELECT agreement_line_id                                    --INTO V3
           FROM xxwc.xxwc_om_contract_pricing_lines
          WHERE product_attribute = 'CAT_CLASS'
                AND agreement_id = v_agreement_id
                and end_date is null ;
                
                


      CURSOR ITEM_ID
      IS
         SELECT b.inventory_item_id
           FROM xxwc.xxwc_om_contract_pricing_lines a
              , mtl_system_items_b b
              , mtl_item_categories c
          WHERE     a.product_value = b.segment1
                AND a.product_attribute = 'ITEM'
                AND b.organization_id = '222'
                AND b.organization_id = c.organization_id
                AND b.inventory_item_id = c.inventory_item_id
                AND a.agreement_id = v_agreement_id
                AND c.category_id = V1
                and a.end_date is null ;

      CURSOR CATEGORY_LINE_ID
      IS
         SELECT agreement_line_id
           FROM xxwc.xxwc_om_contract_pricing_lines
          WHERE product_attribute = 'CATEGORY'
                AND agreement_id = v_agreement_id
                and end_date is null ;

      CURSOR CATEGORY_ID
      IS
         SELECT b.category_id
           FROM XXWC.xxwc_om_contract_pricing_lines a, mtl_categories_b b
          WHERE     a.product_attribute = 'CAT_CLASS'
                AND b.segment1 = V4
                AND a.product_value = b.segment1 || '.' || segment2
                and agreement_id = v_agreement_id 
                and a.end_date is null ;

      CURSOR CAT_ITEM_EXE
      IS
         SELECT b.inventory_item_id
           FROM xxwc.xxwc_om_contract_pricing_lines a
              , mtl_system_items_b b
              , mtl_item_categories c
              , mtl_categories_b d
          WHERE     a.product_value = b.segment1
                AND a.product_attribute = 'ITEM'
                AND b.organization_id = '222'
                AND d.segment1 = V4
                AND b.organization_id = c.organization_id
                AND b.inventory_item_id = c.inventory_item_id
                AND c.category_id = d.category_id
                AND a.agreement_id = v_agreement_id
                and a.end_date is null ;
   BEGIN
            
      FOR c1_rec IN AGREEMENT_ID
      Loop      
      
      Begin
      select agreement_id a, b.list_header_id
      into v_agreement_id, v_list_header_id
      from xxwc.xxwc_om_contract_pricing_hdr a, apps.qp_list_headers b
      where agreement_id = c1_rec.reference_id 
      and a.agreement_id = b.attribute14 ;
       EXCEPTION
        WHEN OTHERS THEN
       FND_FILE.Put_Line(FND_FILE.Log, 'Agreement_ID '||c1_rec.reference_id ||' Not valid agreement_id');
        END;  
        
            delete from
    qp_pricing_attributes
    where list_header_id = v_list_header_id
    and excluder_flag = 'Y' ; 
    commit ; 
                
      FOR c2_rec IN AGREE_LINE_ID
      LOOP
      
    /**************************************
    *   Creating Category Level Exclusions
    **************************************/

       BEGIN
         SELECT list_line_id
           INTO v_list_line_id
           FROM qp_list_lines
          WHERE attribute2 = c2_rec.agreement_line_id;
        EXCEPTION
            WHEN OTHERS THEN 
              L_MSG := SUBSTR('NO MODIFIER LINE EXISTS FOR CATEGORY' || SQLCODE || SQLERRM,1,240) ;
          END;           

      /*   select category ID to query items againsts */
        BEGIN 
         SELECT category_id
           INTO V1
           FROM xxwc.xxwc_om_contract_pricing_lines a, mtl_categories_b b
          WHERE     a.product_attribute = 'CAT_CLASS'
                AND b.segment1 || '.' || b.segment2 = a.product_value
                AND a.agreement_line_id = c2_rec.agreement_line_id;
        EXCEPTION
            WHEN OTHERS THEN 
              L_MSG := SUBSTR('INVALID CATEGORY ID' || SQLCODE || SQLERRM,1,240) ;
          END;                

                  FND_FILE.Put_Line(FND_FILE.Log,'AGREEMENT LINE ID = '||c2_rec.agreement_line_id);
          
            
        /* Find Item Numbers assigned to category id from and loop them into API */
         FOR c3_rec IN ITEM_ID
         LOOP
            --Set Pricing Attributes in API
            l_PRICING_ATTR_tbl (1).list_line_id := v_list_line_id;  --173088 ;
            l_PRICING_ATTR_tbl (1).product_attribute_context := 'ITEM'; --v_product_attribute_context ; --v_product_attribute_context ;
            l_PRICING_ATTR_tbl (1).product_attribute := 'PRICING_ATTRIBUTE1'; --v_product_attribute_api ;
            l_PRICING_ATTR_tbl (1).product_attr_value :=
               c3_rec.inventory_item_id;                  --2928289 505CAMLOCK
            l_PRICING_ATTR_tbl (1).excluder_flag := 'Y';
            l_PRICING_ATTR_tbl (1).MODIFIERS_index := 1;
            l_PRICING_ATTR_tbl (1).operation := QP_GLOBALS.G_OPR_CREATE;

            --API Call
            QP_Modifiers_PUB.Process_Modifiers (
               p_api_version_number      => 1.0
             , p_init_msg_list           => FND_API.G_FALSE
             , p_return_values           => FND_API.G_FALSE
             , p_commit                  => FND_API.G_FALSE
             , x_return_status           => l_return_status
             , x_msg_count               => x_msg_count
             , x_msg_data                => x_msg_data
             , p_MODIFIER_LIST_rec       => l_MODIFIER_LIST_rec
             , p_MODIFIERS_tbl           => l_MODIFIERS_tbl
             , p_QUALIFIERS_tbl          => l_QUALIFIERS_tbl
             , p_PRICING_ATTR_tbl        => l_PRICING_ATTR_tbl
             , x_MODIFIER_LIST_rec       => l_x_MODIFIER_LIST_rec
             , x_MODIFIER_LIST_val_rec   => l_x_MODIFIER_LIST_val_rec
             , x_MODIFIERS_tbl           => l_x_MODIFIERS_tbl
             , x_MODIFIERS_val_tbl       => l_x_MODIFIERS_val_tbl
             , x_QUALIFIERS_tbl          => l_x_QUALIFIERS_tbl
             , x_QUALIFIERS_val_tbl      => l_x_QUALIFIERS_val_tbl
             , x_PRICING_ATTR_tbl        => l_x_PRICING_ATTR_tbl
             , x_PRICING_ATTR_val_tbl    => l_x_PRICING_ATTR_val_tbl);
            COMMIT;
         END LOOP;
      END LOOP;



   /************************************
    *Write Exclusions against Category
    ************************************/

      --Select Agreement Line_ID for Cateogry to write exclusions against
      FOR c4_rec IN CATEGORY_LINE_ID
      LOOP
      /* SELECT LIST LINE FROM QP LIST LINES */
        BEGIN
         SELECT list_line_id
           INTO v_list_line_id
           FROM qp_list_lines
          WHERE attribute2 = c4_rec.agreement_line_id;
        EXCEPTION
            WHEN OTHERS THEN 
              L_MSG := SUBSTR('No Modifier Line Found' || SQLCODE || SQLERRM,1,240) ;
          END;  
          
        BEGIN
         SELECT product_value
           INTO V4
           FROM XXWC.xxwc_om_contract_pricing_lines
          WHERE agreement_line_id = c4_rec.agreement_line_id;
        EXCEPTION
            WHEN OTHERS THEN 
              L_MSG := SUBSTR('INVALID PRODUCT VALUE' || SQLCODE || SQLERRM,1,240) ;
              FND_FILE.Put_Line(FND_FILE.Log,'AGREEMENT LINE ID = '||c4_rec.agreement_line_id);
          END;  
        /* Select Category Classes Assocaited to Category and call API */
         FOR c5_rec IN CATEGORY_ID
         LOOP
            l_PRICING_ATTR_tbl (1).list_line_id := v_list_line_id;  --173088 ;
            l_PRICING_ATTR_tbl (1).product_attribute_context := 'ITEM'; --v_product_attribute_context ; --v_product_attribute_context ;
            l_PRICING_ATTR_tbl (1).product_attribute := 'PRICING_ATTRIBUTE2'; --v_product_attribute_api ;
            l_PRICING_ATTR_tbl (1).product_attr_value := c5_rec.category_id; --2928289 505CAMLOCK v_product_attr_value ;
            l_PRICING_ATTR_tbl (1).excluder_flag := 'Y';
            l_PRICING_ATTR_tbl (1).MODIFIERS_index := 1;
            l_PRICING_ATTR_tbl (1).operation := QP_GLOBALS.G_OPR_CREATE;

            QP_Modifiers_PUB.Process_Modifiers (
               p_api_version_number      => 1.0
             , p_init_msg_list           => FND_API.G_FALSE
             , p_return_values           => FND_API.G_FALSE
             , p_commit                  => FND_API.G_FALSE
             , x_return_status           => l_return_status
             , x_msg_count               => x_msg_count
             , x_msg_data                => x_msg_data
             , p_MODIFIER_LIST_rec       => l_MODIFIER_LIST_rec
             , p_MODIFIERS_tbl           => l_MODIFIERS_tbl
             , p_QUALIFIERS_tbl          => l_QUALIFIERS_tbl
             , p_PRICING_ATTR_tbl        => l_PRICING_ATTR_tbl
             , x_MODIFIER_LIST_rec       => l_x_MODIFIER_LIST_rec
             , x_MODIFIER_LIST_val_rec   => l_x_MODIFIER_LIST_val_rec
             , x_MODIFIERS_tbl           => l_x_MODIFIERS_tbl
             , x_MODIFIERS_val_tbl       => l_x_MODIFIERS_val_tbl
             , x_QUALIFIERS_tbl          => l_x_QUALIFIERS_tbl
             , x_QUALIFIERS_val_tbl      => l_x_QUALIFIERS_val_tbl
             , x_PRICING_ATTR_tbl        => l_x_PRICING_ATTR_tbl
             , x_PRICING_ATTR_val_tbl    => l_x_PRICING_ATTR_val_tbl);
            COMMIT;
         END LOOP;
      END LOOP;
      END LOOP ; 

      /***********************************
      Assign Items to Cateogry Exclusions
      ************************************/

      FOR c4_rec IN CATEGORY_LINE_ID
      LOOP
         --SELECT LIST LINE FROM QP LIST LINES
        BEGIN
         SELECT list_line_id
           INTO v_list_line_id
           FROM qp_list_lines
          WHERE attribute2 = c4_rec.agreement_line_id;
        EXCEPTION
            WHEN OTHERS THEN 
              L_MSG := SUBSTR('INVALID PRODUCT VALUE' || SQLCODE || SQLERRM,1,240) ;
          END;  
          
       BEGIN   
         SELECT product_value
           INTO V4
           FROM XXWC.xxwc_om_contract_pricing_lines
          WHERE agreement_line_id = c4_rec.agreement_line_id;
        EXCEPTION
            WHEN OTHERS THEN 
              L_MSG := SUBSTR('INVALID PRODUCT VALUE' || SQLCODE || SQLERRM,1,240) ;
              FND_FILE.Put_Line(FND_FILE.Log,'AGREEMENT LINE ID = '||c4_rec.agreement_line_id);
          END;            

        /* Find Items associated to Category and Call API */
         FOR rec_r7 IN CAT_ITEM_EXE
         LOOP
            l_PRICING_ATTR_tbl (1).list_line_id := v_list_line_id;  --173088 ;
            l_PRICING_ATTR_tbl (1).product_attribute_context := 'ITEM'; --v_product_attribute_context ; --v_product_attribute_context ;
            l_PRICING_ATTR_tbl (1).product_attribute := 'PRICING_ATTRIBUTE1'; --v_product_attribute_api ;
            l_PRICING_ATTR_tbl (1).product_attr_value :=
               rec_r7.inventory_item_id; --2928289 505CAMLOCK v_product_attr_value ;
            l_PRICING_ATTR_tbl (1).excluder_flag := 'Y';
            l_PRICING_ATTR_tbl (1).MODIFIERS_index := 1;
            l_PRICING_ATTR_tbl (1).operation := QP_GLOBALS.G_OPR_CREATE;

            QP_Modifiers_PUB.Process_Modifiers (
               p_api_version_number      => 1.0
             , p_init_msg_list           => FND_API.G_FALSE
             , p_return_values           => FND_API.G_FALSE
             , p_commit                  => FND_API.G_FALSE
             , x_return_status           => l_return_status
             , x_msg_count               => x_msg_count
             , x_msg_data                => x_msg_data
             , p_MODIFIER_LIST_rec       => l_MODIFIER_LIST_rec
             , p_MODIFIERS_tbl           => l_MODIFIERS_tbl
             , p_QUALIFIERS_tbl          => l_QUALIFIERS_tbl
             , p_PRICING_ATTR_tbl        => l_PRICING_ATTR_tbl
             , x_MODIFIER_LIST_rec       => l_x_MODIFIER_LIST_rec
             , x_MODIFIER_LIST_val_rec   => l_x_MODIFIER_LIST_val_rec
             , x_MODIFIERS_tbl           => l_x_MODIFIERS_tbl
             , x_MODIFIERS_val_tbl       => l_x_MODIFIERS_val_tbl
             , x_QUALIFIERS_tbl          => l_x_QUALIFIERS_tbl
             , x_QUALIFIERS_val_tbl      => l_x_QUALIFIERS_val_tbl
             , x_PRICING_ATTR_tbl        => l_x_PRICING_ATTR_tbl
             , x_PRICING_ATTR_val_tbl    => l_x_PRICING_ATTR_val_tbl);
            COMMIT;
         END LOOP;
      END LOOP;
   END csp_maintenance_exclusions ;


END xxwc_om_csp_interface_pkg;
/