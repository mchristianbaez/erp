/*************************************************************************
  $Header TMS_20170124-00253_Close_Order.sql $
  Module Name: TMS_20170124-00253


  REVISIONS:
  Ver        Date         Author                Description
  ---------  -----------  ------------------    --------------------------
   1.0        03-APR-2017  Ram Talluri      TMS#20170124-00253   
**************************************************************************/ 
SET SERVEROUTPUT ON SIZE 1000000;
DECLARE
BEGIN
   DBMS_OUTPUT.put_line ('TMS: 20170124-00253    , Before Update');
UPDATE apps.oe_order_lines_all
   SET 
       shipping_quantity = ordered_quantity,
       shipped_quantity = ordered_quantity,
       actual_shipment_date = TO_DATE('04/21/2016 11:18:48','MM/DD/YYYY HH24:MI:SS'),
       shipping_quantity_uom = order_quantity_uom,
       fulfilled_flag = 'Y',
       fulfillment_date = TO_DATE('04/21/2016 11:18:48','MM/DD/YYYY HH24:MI:SS'),
       fulfilled_quantity = ordered_quantity,
       last_updated_by = -1,
       last_update_date = SYSDATE
--       ACCEPTED_BY = 4716,
--       REVREC_SIGNATURE = 'TODD MYERS',
--       REVREC_SIGNATURE_DATE = '07-FEB-2013'
WHERE  line_id in (68521251, 68521301) 
and    headeR_id= 41861562;

  DBMS_OUTPUT.put_line (
         'TMS: 20170124-00253  Updated records count: '
      || SQL%ROWCOUNT);

   COMMIT;

   DBMS_OUTPUT.put_line ('TMS: 20170124-00253     , End Update');
EXCEPTION
   WHEN OTHERS
   THEN
      ROLLBACK;
      DBMS_OUTPUT.put_line ('TMS: 20170124-00253 , Errors : ' || SQLERRM);
END;
/