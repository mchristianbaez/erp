CREATE OR REPLACE PACKAGE BODY APPS.xxwc_ar_customer_maint_form
/* Formatted on 3/17/2013 8:36:55 PM (QP5 v5.206) */
/**************************************************************************
*
* HEADER
 *   Source control header
*
* PROGRAM NAME
*  XXWC_AR_CUSTOMER_MAINT_FORM
*
 * DESCRIPTION
*  used for WC customer maintenance form
*
*
* PARAMETERS
* ==========
* NAME              DESCRIPTION
.* ----------------- ------------------------------------------------------
*
*
* DEPENDENCIES
* XXWC_CUSTOMER_MAINT_ARCH table - keep history of updates for TCA using the custom form
* XXWC_CUSTOMER_MAINT_CURR table  - keep recent updates for TCA using the custom form
* XXWC_CUSTOMER_MAINT_CURR_S -sequence for XXWC_CUSTOMER_MAINT_ARCH table
* XXWC_CUSTOMER_MAINT_FORM -view for query in the form
* CALLED BY
* XXWC_CUSTOMER_ACC_MTNS form
*
* LAST UPDATE DATE   23-aug-2012
*
*
* HISTORY
* =======
*
* VERSION DATE        AUTHOR(S)       DESCRIPTION
* ------- ----------- --------------- ------------------------------------
* 1.00    9-aug-2012 Rasikha Galimova  Creation
* 1.10   23-aug-2012 Rasikha Galimova  added changes to bring house account and  profile update changes
* 1.11   26-nov-2012                   added procedure keep_profile_amounts_asbefore- will keep credit amount the same to prevent on hold existing orders.
* 1.12   23-jan-2013 Rasikha Galimova  added FUNCTION get_site_purposes to bring site use purpose to the XXWC custom form
* 1.13   22-oct-2013 Harsha Yedla      TMS#20130909-00777 
*                                      Added Parameters: Credit check, Mandatory Po#, Print Price and Joint Check,Category to Procedure xxwc_update_customer_profile
*                                      Added a new function - XXWC_UPDATE_CUST_SITES
* 1.14   17-sep-2014 Veera C           TMS# 20141006-00025 Modified code as per the Canada OU Test
* 1.15   30-Mar-2015 Maharajan 
*                    Shunmugam         TMS#20140516-00043 Additional fields to Customer Maintenance Form
* 1.16   18-May-2015 Maharajan         TMS#20140516-00043 Fixed Mandatory notes issue
*                    Shunmugam
* 1.17   24-Jun-2015 Maharajan         TMS#20150604-00146 Billtrust - Automatic Update of Credit Balance Statement Field
*                    Shunmugam
* 1.18   07/15/2015  Maharajan         TMS#20150123-00197 AR - Customer Flag as indicator to auto apply credits
*                    Shunmugam
*************************************************************************/
IS
    l_running_messaget           CLOB;
    l_insert_id                  NUMBER;
    l_old_cust_profile_amt_rec   hz_customer_profile_v2pub.cust_profile_amt_rec_type;

    l_cust_acct_profile_amt_id   NUMBER;
    l_object_version_number      NUMBER;
    l_profile_class_amount_id    NUMBER;
    l_currency_code              VARCHAR2 (124);
    v_error_record               NUMBER := 0;

    PROCEDURE report_errors
    IS
    BEGIN
        SELECT COUNT ('a')
          INTO v_error_record
          FROM apps.xxwc_customer_maint_curr
         WHERE insert_id = l_insert_id;

        IF v_error_record = 0
        THEN
            INSERT INTO xxwc.xxwc_ar_cust_maint_other_error
                 VALUES (SYSDATE, l_running_messaget, xxwc.xxwc_ar_cust_maint_err_s.NEXTVAL);
        END IF;

        IF l_running_messaget IS NOT NULL AND l_insert_id > 0 AND v_error_record > 0
        THEN
            UPDATE xxwc_customer_maint_curr
               SET error_message = error_message || '*' || l_running_messaget
             WHERE insert_id = l_insert_id;
        END IF;

        COMMIT;
        l_running_messaget := NULL;
    EXCEPTION
        WHEN OTHERS
        THEN
            l_running_messaget :=
                   l_running_messaget
                || 'Error_Stack...report_errors'
                || CHR (10)
                || DBMS_UTILITY.format_error_stack ()
                || CHR (10)
                || ' Error_Backtrace...'
                || CHR (10)
                || DBMS_UTILITY.format_error_backtrace ();

            INSERT INTO xxwc.xxwc_ar_cust_maint_other_error
                 VALUES (SYSDATE, l_running_messaget, xxwc.xxwc_ar_cust_maint_err_s.NEXTVAL);

            COMMIT;
            l_running_messaget := NULL;
    END;

    --*******************************************************
    PROCEDURE set_other_error
    IS
        v_error_stack       CLOB := DBMS_UTILITY.format_error_stack ();
        v_error_backtrace   CLOB := DBMS_UTILITY.format_error_backtrace ();
    BEGIN
        IF v_error_stack IS NOT NULL
        THEN
            l_running_messaget :=
                   l_running_messaget
                || 'Error_Stack.. '
                || CHR (10)
                || v_error_stack
                || CHR (10)
                || ' Error_Backtrace...'
                || CHR (10)
                || v_error_backtrace;
        END IF;

        report_errors;

        --   INSERT INTO xxwc.xxwc_ar_cust_maint_other_error
        --  VALUES (SYSDATE, l_running_messaget, xxwc.xxwc_ar_cust_maint_err_s.NEXTVAL);

        COMMIT;
        l_running_messaget := NULL;
    END;

    --*******************************************************
    PROCEDURE keep_profile_amounts_asbefore (prof_rowid VARCHAR2)
    IS
        v_return_status      VARCHAR2 (1024);
        v_msg_count          NUMBER;
        v_msg_data           VARCHAR2 (1024);
        v_running_message1   VARCHAR2 (1024);
        v_msg_dummy          VARCHAR2 (1024);
    BEGIN
        FOR r IN (SELECT *
                    FROM apps.xxwc_customer_maint_amts
                   WHERE prof_rowid = prof_rowid)
        LOOP
            FOR rinner IN (SELECT *
                             FROM hz_cust_profile_amts
                            WHERE ROWID = r.amount_rowid)
            LOOP
                IF    NVL (rinner.overall_credit_limit, 9999999999) <> NVL (r.overall_credit_limit, 9999999999)
                   OR NVL (rinner.trx_credit_limit, 9999999999) <> NVL (r.trx_credit_limit, 9999999999)
                   OR NVL (rinner.min_statement_amount, 9999999999) <> NVL (r.min_statement_amount, 9999999999)
                   OR NVL (rinner.auto_rec_min_receipt_amount, 9999999999) <>
                          NVL (r.auto_rec_min_receipt_amount, 9999999999)
                   OR NVL (rinner.interest_rate, 9999999999) <> NVL (r.interest_rate, 9999999999)
                THEN
                    UPDATE hz_cust_profile_amts
                       SET overall_credit_limit =  r.overall_credit_limit
                          ,trx_credit_limit = r.trx_credit_limit
                          ,min_statement_amount = r.min_statement_amount
                          ,auto_rec_min_receipt_amount = r.auto_rec_min_receipt_amount
                          ,interest_rate = r.interest_rate
                     WHERE ROWID = r.amount_rowid;
                END IF;
            END LOOP;
        END LOOP;

        DELETE FROM xxwc_customer_maint_amts
              WHERE prof_rowid = prof_rowid;

        COMMIT;
    END;

    --**********************************************************

    FUNCTION get_sales_rep (p_org_id NUMBER, p_salesrep_id NUMBER)
        RETURN VARCHAR2
    IS
        v_salesrep      VARCHAR2 (164);
        v_resource_id   NUMBER;
    BEGIN
        BEGIN
            SELECT resource_id
              INTO v_resource_id
              FROM jtf_rs_salesreps jrs
             WHERE salesrep_id = p_salesrep_id AND org_id = p_org_id;
        EXCEPTION
            WHEN OTHERS
            THEN
                v_resource_id := NULL;
        END;

        IF v_resource_id IS NOT NULL
        THEN
            FOR r
                IN (SELECT c.resource_id
                          ,c.resource_name name
                          ,c.source_id person_id
                          ,b.role_code
                      FROM jtf_rs_role_relations a, jtf_rs_roles_vl b, jtf_rs_resource_extns_vl c
                     WHERE     a.role_resource_type = 'RS_INDIVIDUAL'
                           AND a.role_resource_id = c.resource_id
                           AND a.role_id = b.role_id
                           AND b.role_code = 'SALES_REP'
                           --  AND c.category = 'EMPLOYEE'
                           AND NVL (a.delete_flag, 'N') <> 'Y'
                           AND resource_id = v_resource_id)
            LOOP
                v_salesrep := r.name;
            END LOOP;
        END IF;

        RETURN v_salesrep;
    END;

    --**************************************************************
    FUNCTION credit_analyst (p_credit_analyst_id NUMBER)
        RETURN VARCHAR2
    IS
        v_credit_analyst_name   VARCHAR2 (124);
    BEGIN
        FOR r
            IN (SELECT c.resource_id, c.resource_name name, c.source_id person_id
                  FROM jtf_rs_role_relations a, jtf_rs_roles_vl b, jtf_rs_resource_extns_vl c
                 WHERE     a.role_resource_type = 'RS_INDIVIDUAL'
                       AND a.role_resource_id = c.resource_id
                       AND a.role_id = b.role_id
                       AND b.role_code = 'CREDIT_ANALYST'
                       AND c.category = 'EMPLOYEE'
                       AND NVL (a.delete_flag, 'N') <> 'Y'
                       AND c.resource_id = p_credit_analyst_id)
        LOOP
            v_credit_analyst_name := r.name;
        END LOOP;

        RETURN v_credit_analyst_name;
    END;

    --**********************************************************************
    --*****************************************************************************************
    --*****************************************************************************************
    PROCEDURE xxwc_update_account (pr_cust_rowid          VARCHAR2
                                  ,pr_primary_salesrep_id NUMBER
                   		  ,p_predominant_trade    VARCHAR2     --Added for Ver 1.15<Start
                           	  ,p_legal_col_indicator  VARCHAR2
                 		  ,p_legal_placement      VARCHAR2
                  		  ,p_classification       VARCHAR2    
                                  ,p_auto_apply_cm        VARCHAR2      --Added for Ver 1.18
                   		  ,p_mandatory_note       VARCHAR2
                                  ,p_mandatory_note1      VARCHAR2
                                  ,p_mandatory_note2      VARCHAR2
                                  ,p_mandatory_note3      VARCHAR2
                                  ,p_mandatory_note4      VARCHAR2
                                  ,p_mandatory_note5      VARCHAR2     --Added for Ver 1.15<End
                                  ,p_last_updated_by      NUMBER
                                  ,p_last_update_date     DATE
                                  ,p_last_update_login    NUMBER
                                  ,p_responsibility_id    NUMBER
                                  ,p_org_id               NUMBER)
    IS
        v_status                  VARCHAR2 (12);
        v_primary_salesrep_id     NUMBER;

        v_return_status           VARCHAR2 (1024);
        v_msg_count               NUMBER;
        v_msg_data                VARCHAR2 (1024);
        v_object_version_number   NUMBER := 1;
        v_cust_account_rec        hz_cust_account_v2pub.cust_account_rec_type;
        v_auto_apply_cm           VARCHAR2(1);                        --Added for Ver 1.18 

        v_running_message1        VARCHAR2 (1024);
        l_msg_dummy               VARCHAR2 (1000);

        l_errbuf                  CLOB;
    BEGIN
        l_running_messaget := NULL;

        FOR r IN (SELECT *
                    FROM hz_cust_accounts
                   WHERE ROWID = pr_cust_rowid)
        LOOP
            v_cust_account_rec.cust_account_id := r.cust_account_id;
            v_cust_account_rec.account_number := r.account_number;
            v_cust_account_rec.attribute_category := NVL(p_mandatory_note ,r.attribute_category);  --Modified for Ver 1.15
            v_cust_account_rec.attribute1 := r.attribute1;
            v_cust_account_rec.attribute2 := r.attribute2;
            v_cust_account_rec.attribute3 := r.attribute3;
            v_cust_account_rec.attribute4 := r.attribute4;
            v_cust_account_rec.attribute5 := NVL(p_legal_col_indicator,r.attribute5);              --Modified for Ver 1.15
            v_cust_account_rec.attribute6 := r.attribute6;
            v_cust_account_rec.attribute7 := r.attribute7;
            v_cust_account_rec.attribute8 := r.attribute8;
            v_cust_account_rec.attribute9 := NVL(p_predominant_trade,r.attribute9);                --Modified for Ver 1.15
            v_cust_account_rec.attribute10 :=NVL(p_legal_placement,r.attribute10);                 --Modified for Ver 1.15
           -- v_cust_account_rec.attribute11 := r.attribute11;                                     --commented for ver 1.18
           --added below for ver 1.18 <<sTART
            IF p_auto_apply_cm = 'ON'
            THEN 
            v_cust_account_rec.attribute11 := 'Y';
            ELSIF p_auto_apply_cm = 'OFF'
            THEN
            v_cust_account_rec.attribute11 := 'N';
            ELSE
            v_cust_account_rec.attribute11 := r.attribute11;
            END IF;
            --<< END

            v_cust_account_rec.attribute12 := r.attribute12;
            v_cust_account_rec.attribute13 := r.attribute13;
            v_cust_account_rec.attribute14 := r.attribute14;
            v_cust_account_rec.attribute15 := r.attribute15;
            IF p_mandatory_note = 'Yes'
            THEN
            v_cust_account_rec.attribute16 := NVL(p_mandatory_note5,r.attribute16);           -- Modified to keep existing value if mandatory note is not entered for Ver 1.16 <<sTART
            v_cust_account_rec.attribute17 := NVL(p_mandatory_note1,r.attribute17);
            v_cust_account_rec.attribute18 := NVL(p_mandatory_note2,r.attribute18);
            v_cust_account_rec.attribute19 := NVL(p_mandatory_note3,r.attribute19);
            v_cust_account_rec.attribute20 := NVL(p_mandatory_note4,r.attribute20);           
            ELSIF p_mandatory_note = 'No'
            THEN
            v_cust_account_rec.attribute16 := ' ';          
            v_cust_account_rec.attribute17 := ' ';
            v_cust_account_rec.attribute18 := ' ';
            v_cust_account_rec.attribute19 := ' ';
            v_cust_account_rec.attribute20 := ' ';    
            ELSE
            v_cust_account_rec.attribute16 := r.attribute16;         
            v_cust_account_rec.attribute17 := r.attribute17;
            v_cust_account_rec.attribute18 := r.attribute18;
            v_cust_account_rec.attribute19 := r.attribute19;
            v_cust_account_rec.attribute20 := r.attribute20;  
            END IF;                               --Modified for Ver 1.16<<END

            v_cust_account_rec.global_attribute_category := r.global_attribute_category;
            v_cust_account_rec.global_attribute1 := r.global_attribute1;
            v_cust_account_rec.global_attribute2 := r.global_attribute2;
            v_cust_account_rec.global_attribute3 := r.global_attribute3;
            v_cust_account_rec.global_attribute4 := r.global_attribute4;
            v_cust_account_rec.global_attribute5 := r.global_attribute5;
            v_cust_account_rec.global_attribute6 := r.global_attribute6;
            v_cust_account_rec.global_attribute7 := r.global_attribute7;
            v_cust_account_rec.global_attribute8 := r.global_attribute8;
            v_cust_account_rec.global_attribute9 := r.global_attribute9;
            v_cust_account_rec.global_attribute10 := r.global_attribute10;
            v_cust_account_rec.global_attribute11 := r.global_attribute11;
            v_cust_account_rec.global_attribute12 := r.global_attribute12;
            v_cust_account_rec.global_attribute13 := r.global_attribute13;
            v_cust_account_rec.global_attribute14 := r.global_attribute14;
            v_cust_account_rec.global_attribute15 := r.global_attribute15;
            v_cust_account_rec.global_attribute16 := r.global_attribute16;
            v_cust_account_rec.global_attribute17 := r.global_attribute17;
            v_cust_account_rec.global_attribute18 := r.global_attribute18;
            v_cust_account_rec.global_attribute19 := r.global_attribute19;
            v_cust_account_rec.global_attribute20 := r.global_attribute20;
            v_cust_account_rec.orig_system_reference := r.orig_system_reference;
            -- V_CUST_ACCOUNT_REC.ORIG_SYSTEM := R.ORIG_SYSTEM;
            v_cust_account_rec.status := r.status;
            v_cust_account_rec.customer_type := r.customer_type;
            v_cust_account_rec.customer_class_code := NVL(p_classification ,r.customer_class_code);  --Modified for Ver 1.15
            v_cust_account_rec.primary_salesrep_id := NVL (pr_primary_salesrep_id, r.primary_salesrep_id); --******************
            v_cust_account_rec.sales_channel_code := r.sales_channel_code;
            v_cust_account_rec.order_type_id := r.order_type_id;
            v_cust_account_rec.price_list_id := r.price_list_id;
            v_cust_account_rec.tax_code := r.tax_code;
            v_cust_account_rec.fob_point := r.fob_point;
            v_cust_account_rec.freight_term := r.freight_term;
            v_cust_account_rec.ship_partial := r.ship_partial;
            v_cust_account_rec.ship_via := r.ship_via;
            v_cust_account_rec.warehouse_id := r.warehouse_id;
            v_cust_account_rec.tax_header_level_flag := r.tax_header_level_flag;
            v_cust_account_rec.tax_rounding_rule := r.tax_rounding_rule;
            v_cust_account_rec.coterminate_day_month := r.coterminate_day_month;
            v_cust_account_rec.primary_specialist_id := r.primary_specialist_id;
            v_cust_account_rec.secondary_specialist_id := r.secondary_specialist_id;
            v_cust_account_rec.account_liable_flag := r.account_liable_flag;
            v_cust_account_rec.current_balance := r.current_balance;
            v_cust_account_rec.account_established_date := r.account_established_date;
            v_cust_account_rec.account_termination_date := r.account_termination_date;
            v_cust_account_rec.account_activation_date := r.account_activation_date;
            v_cust_account_rec.department := r.department;
            v_cust_account_rec.held_bill_expiration_date := r.held_bill_expiration_date;
            v_cust_account_rec.hold_bill_flag := r.hold_bill_flag;
            v_cust_account_rec.realtime_rate_flag := r.realtime_rate_flag;
            v_cust_account_rec.acct_life_cycle_status := r.acct_life_cycle_status;
            v_cust_account_rec.account_name := r.account_name;
            v_cust_account_rec.deposit_refund_method := r.deposit_refund_method;
            v_cust_account_rec.dormant_account_flag := r.dormant_account_flag;
            v_cust_account_rec.npa_number := r.npa_number;
            v_cust_account_rec.suspension_date := r.suspension_date;
            v_cust_account_rec.source_code := r.source_code;
            v_cust_account_rec.comments := r.comments;
            v_cust_account_rec.dates_negative_tolerance := r.dates_negative_tolerance;
            v_cust_account_rec.dates_positive_tolerance := r.dates_positive_tolerance;
            v_cust_account_rec.date_type_preference := r.date_type_preference;
            v_cust_account_rec.over_shipment_tolerance := r.over_shipment_tolerance;
            v_cust_account_rec.under_shipment_tolerance := r.under_shipment_tolerance;
            v_cust_account_rec.over_return_tolerance := r.over_return_tolerance;
            v_cust_account_rec.under_return_tolerance := r.under_return_tolerance;
            v_cust_account_rec.item_cross_ref_pref := r.item_cross_ref_pref;
            v_cust_account_rec.ship_sets_include_lines_flag := r.ship_sets_include_lines_flag;
            v_cust_account_rec.arrivalsets_include_lines_flag := r.arrivalsets_include_lines_flag;
            v_cust_account_rec.sched_date_push_flag := r.sched_date_push_flag;
            v_cust_account_rec.invoice_quantity_rule := r.invoice_quantity_rule;
            v_cust_account_rec.pricing_event := r.pricing_event;
            v_cust_account_rec.status_update_date := r.status_update_date;
            v_cust_account_rec.autopay_flag := r.autopay_flag;
            v_cust_account_rec.notify_flag := r.notify_flag;
            v_cust_account_rec.last_batch_id := r.last_batch_id;
            v_cust_account_rec.selling_party_id := r.selling_party_id;
            v_cust_account_rec.created_by_module := r.created_by_module;
            v_cust_account_rec.application_id := r.application_id;
            v_object_version_number := r.object_version_number;
--Commented for ver 1.15<Start
--            IF     pr_primary_salesrep_id IS NOT NULL
--               AND NVL (pr_primary_salesrep_id, 999999999999) <> NVL (r.primary_salesrep_id, 999999999999)
--            THEN
--Commented for ver 1.15<End

                hz_cust_account_v2pub.update_cust_account (p_cust_account_rec        => v_cust_account_rec
                                                          ,p_object_version_number   => v_object_version_number
                                                          ,x_return_status           => v_return_status
                                                          ,x_msg_count               => v_msg_count
                                                          ,x_msg_data                => v_msg_data);
--            END IF; --Commented for ver 1.15


            COMMIT;

            IF v_msg_count > 0                                                              --AND V_RETURN_STATUS <> 'S'
            THEN
                v_running_message1 := '';

                FOR i IN 1 .. v_msg_count
                LOOP
                    fnd_msg_pub.get (i
                                    ,fnd_api.g_false
                                    ,v_msg_data
                                    ,l_msg_dummy);

                    IF NVL (v_running_message1, 'null') <> v_msg_data
                    THEN
                        l_running_messaget :=
                            l_running_messaget || ' ' || SUBSTR ('Msg' || TO_CHAR (i) || ': ' || v_msg_data, 1, 255);
                    END IF;

                    v_running_message1 := v_msg_data;
                END LOOP;
            END IF;
        END LOOP;
    --change the values
    EXCEPTION
        WHEN OTHERS
        THEN
            l_running_messaget := NULL;
            l_running_messaget := 'other error XXWC_UPDATE_ACCOUNT';
            set_other_error;
    END xxwc_update_account;

    --*****************************************************************************************
    --*****************************************************************************************
    --*****************************************************************************************
    PROCEDURE xxwc_update_customer_profile (p_prof_rowid           VARCHAR2
                                           ,p_salesrep_id          NUMBER
                                           ,p_collector_id         NUMBER
                                           ,p_remmit_to_code       VARCHAR2
                                           ,p_freight_term         VARCHAR2
                                           ,p_analyst_id           NUMBER
                                           ,p_credit_checking      VARCHAR2
                                           ,p_credit_hold          VARCHAR2
                                           ,p_payment_term_id      NUMBER
                                           ,p_profile_class_id     NUMBER
                                           ,p_account_status       VARCHAR2
                                           ,p_credit_class_code    VARCHAR2
                                           ,p_cust_rowid           VARCHAR2
                                           ,p_cust_acct_st_rowid   VARCHAR2
                                           ,p_site_uses_rowid      VARCHAR2
                                           ,p_mand_po              VARCHAR2
                                           ,p_print_price          VARCHAR2
                                           ,p_joint_check          VARCHAR2
                                           ,p_category             VARCHAR2
                                           ,p_predominant_trade    VARCHAR2     --Added below for Ver 1.15<Start
                                           ,p_legal_col_indicator  VARCHAR2
                                           ,p_legal_placement      VARCHAR2
                                           ,p_tax_exempt           VARCHAR2
                                           ,p_tax_exempt_type      VARCHAR2
                                           ,p_classification       VARCHAR2
                                           ,p_send_stmnt           VARCHAR2
					   ,p_send_cr_balance      VARCHAR2       --Added for Ver 1.17
                             		   ,p_auto_apply_cm        VARCHAR2       --Added for Ver 1.18
					   ,p_geocode_override     VARCHAR2       --Added for Ver 1.18
                                          -- ,p_credit_limit       NUMBER
                                           ,p_mandatory_note       VARCHAR2
                                           ,p_mandatory_note1      VARCHAR2
                                           ,p_mandatory_note2      VARCHAR2
                                           ,p_mandatory_note3      VARCHAR2
                                           ,p_mandatory_note4      VARCHAR2
                                           ,p_mandatory_note5      VARCHAR2  	  --Added for Ver 1.15<End
                                           ,p_last_updated_by      NUMBER
                                           ,p_last_update_date     DATE
                                           ,p_last_update_login    NUMBER
                                           ,p_responsibility_id    NUMBER
                                           ,p_org_id               NUMBER
                                           ,p_level_flag           VARCHAR2)
    IS
        v_cust_profile_rec             hz_customer_profile_v2pub.customer_profile_rec_type;
        v_object_version_number        NUMBER;
        v_amt_object_version_number    NUMBER;
        v_return_status                VARCHAR2 (1024);
        v_msg_count                    NUMBER;
        v_msg_data                     VARCHAR2 (1024);
        v_running_message1             VARCHAR2 (1024);
        l_msg_dummy                    VARCHAR2 (1024);
        v_user_name                    VARCHAR2 (164);
        v_user_id                      NUMBER := p_last_updated_by;
        v_resp_id                      NUMBER;
        v_resp_appl_id                 NUMBER;
        l_errbuf                       CLOB;
        v_appl_short_name              VARCHAR2 (164);
        v_org_id                       NUMBER;
        v_before_profile_class_id      NUMBER;
        v_after_profile_class_id       NUMBER;
        v_before_credit_analyst_id     NUMBER;
        v_before_cust_acc_profile_id   NUMBER;
        v_overall_credit_limit         NUMBER;
        v_amt_row_inserted             NUMBER;
    BEGIN
        --create application session
        BEGIN
            SELECT r.responsibility_id, r.application_id, a.application_short_name
              INTO v_resp_id, v_resp_appl_id, v_appl_short_name
              FROM fnd_responsibility_vl r, fnd_application a
             WHERE     responsibility_id = p_responsibility_id
                   AND SYSDATE BETWEEN r.start_date AND NVL (r.end_date, TRUNC (SYSDATE) + 1)
                   AND r.application_id = a.application_id;
        EXCEPTION
            WHEN NO_DATA_FOUND
            THEN
                l_running_messaget := 'Responsibility id - ' || p_responsibility_id || ' not defined in Oracle';
                set_other_error;
            WHEN OTHERS
            THEN
                l_running_messaget := 'Error deriving Responsibility_id  - ' || p_responsibility_id;

                set_other_error;
        END;

        fnd_global.apps_initialize (user_id => v_user_id, resp_id => v_resp_id,   --'HDS Credit Assoc Cash App Mgr - WC'
                                                                               resp_appl_id => v_resp_appl_id);
        -- MO_GLOBAL.INIT ('AR');
        mo_global.init (v_appl_short_name);
        --  MO_GLOBAL.SET_POLICY_CONTEXT ('S', 162);
        mo_global.set_policy_context ('S', p_org_id);
        fnd_request.set_org_id (fnd_global.org_id);

        --end of create application session
        BEGIN
            SELECT profile_class_id, cust_account_profile_id
              INTO v_before_profile_class_id, v_before_cust_acc_profile_id
              FROM hz_customer_profiles
             WHERE ROWID = p_prof_rowid;

            l_running_messaget := ' find profile_class_id ';
        EXCEPTION
            WHEN OTHERS
            THEN
                l_running_messaget := ' find profile_class_id ';
                set_other_error;
        END;

        l_profile_class_amount_id := NULL;
        l_currency_code := NULL;

        BEGIN
            SELECT profile_class_amount_id, currency_code
              INTO l_profile_class_amount_id, l_currency_code
              FROM hz_cust_prof_class_amts
             WHERE profile_class_id = v_before_profile_class_id;
        EXCEPTION
            WHEN OTHERS
            THEN
                l_running_messaget := ' find profile_class_amount_id ';
                set_other_error;
        END;

        l_cust_acct_profile_amt_id := NULL;
        v_amt_object_version_number := NULL;
        v_overall_credit_limit := NULL;

        --save profile amount values in temporary table
        BEGIN
            l_running_messaget := ' inside amount save ';

            SELECT COUNT ('a')
              INTO v_amt_row_inserted
              FROM apps.xxwc_customer_maint_amts
             WHERE prof_rowid = p_prof_rowid;

            IF v_amt_row_inserted = 0
            THEN
                INSERT INTO xxwc.xxwc_customer_maint_amts
                    SELECT *
                      FROM xxwc_customer_maint_amount_v
                     WHERE prof_rowid = p_prof_rowid;

                COMMIT;
            END IF;
        END;

        --end of save profile amount values in temporary table

        BEGIN
            SELECT credit_analyst_id
              INTO v_before_credit_analyst_id
              FROM hz_customer_profiles
             WHERE ROWID = p_prof_rowid;
        EXCEPTION
            WHEN OTHERS
            THEN
                l_running_messaget := ' find credit_analyst_id ';
                set_other_error;
        END;

        l_running_messaget := NULL;
        --code below removed by Rasikha at  3/17/2012 TMS 20130314-01171
        /* IF p_analyst_id IS NOT NULL
         THEN
             BEGIN
                 UPDATE ar_cmgt_case_folders
                    SET credit_analyst_id = p_analyst_id
                  WHERE credit_analyst_id = v_before_credit_analyst_id AND status IN ('CREATED', 'SAVED');
             EXCEPTION
                 WHEN OTHERS
                 THEN
                     l_running_messaget := ' update ar_cmgt_case_folders ';
                     set_other_error;
             END;

             --Update pending credit requests
             BEGIN
                 UPDATE ar_cmgt_credit_requests
                    SET credit_analyst_id = p_analyst_id
                  WHERE credit_analyst_id = v_before_credit_analyst_id AND status IN ('IN_PROCESS', 'SAVED', 'SUBMIT');
             EXCEPTION
                 WHEN OTHERS
                 THEN
                     l_running_messaget := ' update ar_cmgt_credit_requests ';
                     set_other_error;
             END;

             COMMIT;
         END IF;*/



         xxwc_ar_customer_maint_form.changes_log (p_prof_rowid
                                                ,p_site_uses_rowid
                                                ,p_cust_rowid
                                                ,p_last_updated_by
                                                ,p_last_update_date
                                                ,p_last_update_login
                                                ,'before'||p_cust_acct_st_rowid );
        l_running_messaget := NULL;
--Added for ver# 1.15 <Start
        IF p_cust_rowid IS NOT NULL
        THEN
            xxwc_update_account (  p_cust_rowid
                                  ,p_salesrep_id 
                   		  ,p_predominant_trade    
                            	  ,p_legal_col_indicator  
                  		  ,p_legal_placement     
                  		  ,p_classification 
                                  ,p_auto_apply_cm                --Added for ver# 1.18     
                   		  ,p_mandatory_note       
                                  ,p_mandatory_note1      
                                  ,p_mandatory_note2      
                                  ,p_mandatory_note3      
                                  ,p_mandatory_note4       
                                  ,p_mandatory_note5       
                                  ,p_last_updated_by         
                                  ,p_last_update_date        
                                  ,p_last_update_login       
                                  ,p_responsibility_id       
                                  ,p_org_id                  ); 
        END IF;
--Added for ver# 1.15 <End


        IF p_cust_acct_st_rowid  IS NOT NULL
        THEN
            xxwc_update_cust_siteS (p_cust_acct_st_rowid
                                  ,p_mand_po
                                  ,p_print_price
                                  ,p_joint_check
                                  ,p_category
                                  ,p_tax_exempt       --Added for ver 1.15
                                  ,p_tax_exempt_type  --Added for ver 1.15
                                  ,p_geocode_override  --Added for Ver 1.18
                                  ,p_last_updated_by
                                  ,p_last_update_date
                                  ,p_last_update_login
                                  ,p_responsibility_id
                                  ,p_org_id                        );
        END IF;






        xxwc_ar_customer_maint_form.changes_log (p_prof_rowid
                                                ,p_site_uses_rowid
                                                ,p_cust_rowid
                                                ,p_last_updated_by
                                                ,p_last_update_date
                                                ,p_last_update_login
                                                ,'before');
        l_running_messaget := NULL;

        IF p_site_uses_rowid IS NOT NULL
        THEN
            xxwc_ar_customer_maint_form.xxwc_update_cust_site_uses (p_site_uses_rowid
                                                                   ,p_salesrep_id
                                                                   ,p_freight_term
                                                                   ,p_last_updated_by
                                                                   ,p_last_update_date
                                                                   ,p_last_update_login
                                                                   ,p_responsibility_id
                                                                   ,p_org_id);
        END IF;

        --*****************************************************************
        l_running_messaget := NULL;

        IF p_level_flag = 'P'
        THEN
            FOR r IN (SELECT *
                        FROM hz_customer_profiles
                       WHERE ROWID = p_prof_rowid)
            LOOP
                v_cust_profile_rec.cust_account_profile_id := r.cust_account_profile_id;
                v_cust_profile_rec.cust_account_id := r.cust_account_id;
                v_cust_profile_rec.status := r.status;
                v_cust_profile_rec.collector_id := NVL (p_collector_id, r.collector_id);                       --*******
                v_cust_profile_rec.credit_analyst_id := NVL (p_analyst_id, r.credit_analyst_id);               --*******
                v_cust_profile_rec.credit_checking := NVL (p_credit_checking, r.credit_checking);              --*******
                v_cust_profile_rec.next_credit_review_date := r.next_credit_review_date;
                v_cust_profile_rec.tolerance := r.tolerance;
                v_cust_profile_rec.discount_terms := r.discount_terms;
                v_cust_profile_rec.dunning_letters := r.dunning_letters;
                v_cust_profile_rec.interest_charges := r.interest_charges;
                v_cust_profile_rec.send_statements := NVL(p_send_stmnt ,r.send_statements);              --Modified for ver 1.15
                --v_cust_profile_rec.credit_balance_statements := NVL(p_send_stmnt,r.credit_balance_statements); --Modified for ver 1.15    --commented and added below for ver#1.17
                v_cust_profile_rec.credit_balance_statements := NVL(p_send_cr_balance,r.credit_balance_statements); 
                v_cust_profile_rec.credit_hold := NVL (p_credit_hold, r.credit_hold);                          --*******
                v_cust_profile_rec.profile_class_id := NVL (p_profile_class_id, r.profile_class_id);           --*******
                v_cust_profile_rec.site_use_id := r.site_use_id;
                v_cust_profile_rec.credit_rating := r.credit_rating;
                v_cust_profile_rec.risk_code := r.risk_code;
                v_cust_profile_rec.standard_terms := NVL (p_payment_term_id, r.standard_terms);                --*******
                v_cust_profile_rec.override_terms := r.override_terms;
                v_cust_profile_rec.dunning_letter_set_id := r.dunning_letter_set_id;
                v_cust_profile_rec.interest_period_days := r.interest_period_days;
                v_cust_profile_rec.payment_grace_days := r.payment_grace_days;
                v_cust_profile_rec.discount_grace_days := r.discount_grace_days;
                --v_cust_profile_rec.statement_cycle_id := r.statement_cycle_id;
        --commented above and added below block for ver 1.15
                BEGIN
                SELECT DECODE(p_send_stmnt,'N',NULL,r.statement_cycle_id)
                INTO v_cust_profile_rec.statement_cycle_id 
                FROM DUAL;
                END;
                v_cust_profile_rec.account_status := NVL (p_account_status, r.account_status);                 --*******
                v_cust_profile_rec.percent_collectable := r.percent_collectable;
                v_cust_profile_rec.autocash_hierarchy_id := r.autocash_hierarchy_id;
                v_cust_profile_rec.attribute_category := r.attribute_category;
                v_cust_profile_rec.attribute1 := r.attribute1;
                v_cust_profile_rec.attribute2 := NVL (p_remmit_to_code, r.attribute2);                         --*******
                v_cust_profile_rec.attribute3 := r.attribute3;
                v_cust_profile_rec.attribute4 := r.attribute4;
                v_cust_profile_rec.attribute5 := r.attribute5;
                v_cust_profile_rec.attribute6 := r.attribute6;
                v_cust_profile_rec.attribute7 := r.attribute7;
                v_cust_profile_rec.attribute8 := r.attribute8;
                v_cust_profile_rec.attribute9 := r.attribute9;
                v_cust_profile_rec.attribute10 := r.attribute10;
                v_cust_profile_rec.attribute11 := r.attribute11;
                v_cust_profile_rec.attribute12 := r.attribute12;
                v_cust_profile_rec.attribute13 := r.attribute13;
                v_cust_profile_rec.attribute14 := r.attribute14;
                v_cust_profile_rec.attribute15 := r.attribute15;
                v_cust_profile_rec.auto_rec_incl_disputed_flag := r.auto_rec_incl_disputed_flag;
                v_cust_profile_rec.tax_printing_option := r.tax_printing_option;
                v_cust_profile_rec.charge_on_finance_charge_flag := r.charge_on_finance_charge_flag;
                v_cust_profile_rec.grouping_rule_id := r.grouping_rule_id;
                v_cust_profile_rec.clearing_days := r.clearing_days;
                v_cust_profile_rec.jgzz_attribute_category := r.jgzz_attribute_category;
                v_cust_profile_rec.jgzz_attribute1 := r.jgzz_attribute1;
                v_cust_profile_rec.jgzz_attribute2 := r.jgzz_attribute2;
                v_cust_profile_rec.jgzz_attribute3 := r.jgzz_attribute3;
                v_cust_profile_rec.jgzz_attribute4 := r.jgzz_attribute4;
                v_cust_profile_rec.jgzz_attribute5 := r.jgzz_attribute5;
                v_cust_profile_rec.jgzz_attribute6 := r.jgzz_attribute6;
                v_cust_profile_rec.jgzz_attribute7 := r.jgzz_attribute7;
                v_cust_profile_rec.jgzz_attribute8 := r.jgzz_attribute8;
                v_cust_profile_rec.jgzz_attribute9 := r.jgzz_attribute9;
                v_cust_profile_rec.jgzz_attribute10 := r.jgzz_attribute10;
                v_cust_profile_rec.jgzz_attribute11 := r.jgzz_attribute11;
                v_cust_profile_rec.jgzz_attribute12 := r.jgzz_attribute12;
                v_cust_profile_rec.jgzz_attribute13 := r.jgzz_attribute13;
                v_cust_profile_rec.jgzz_attribute14 := r.jgzz_attribute14;
                v_cust_profile_rec.jgzz_attribute15 := r.jgzz_attribute15;
                v_cust_profile_rec.global_attribute1 := r.global_attribute1;
                v_cust_profile_rec.global_attribute2 := r.global_attribute2;
                v_cust_profile_rec.global_attribute3 := r.global_attribute3;
                v_cust_profile_rec.global_attribute4 := r.global_attribute4;
                v_cust_profile_rec.global_attribute5 := r.global_attribute5;
                v_cust_profile_rec.global_attribute6 := r.global_attribute6;
                v_cust_profile_rec.global_attribute7 := r.global_attribute7;
                v_cust_profile_rec.global_attribute8 := r.global_attribute8;
                v_cust_profile_rec.global_attribute9 := r.global_attribute9;
                v_cust_profile_rec.global_attribute10 := r.global_attribute10;
                v_cust_profile_rec.global_attribute11 := r.global_attribute11;
                v_cust_profile_rec.global_attribute12 := r.global_attribute12;
                v_cust_profile_rec.global_attribute13 := r.global_attribute13;
                v_cust_profile_rec.global_attribute14 := r.global_attribute14;
                v_cust_profile_rec.global_attribute15 := r.global_attribute15;
                v_cust_profile_rec.global_attribute16 := r.global_attribute16;
                v_cust_profile_rec.global_attribute17 := r.global_attribute17;
                v_cust_profile_rec.global_attribute18 := r.global_attribute18;
                v_cust_profile_rec.global_attribute19 := r.global_attribute19;
                v_cust_profile_rec.global_attribute20 := r.global_attribute20;
                v_cust_profile_rec.global_attribute_category := r.global_attribute_category;
                v_cust_profile_rec.cons_inv_flag := r.cons_inv_flag;
                v_cust_profile_rec.cons_inv_type := r.cons_inv_type;
                v_cust_profile_rec.autocash_hierarchy_id_for_adr := r.autocash_hierarchy_id_for_adr;
                v_cust_profile_rec.lockbox_matching_option := r.lockbox_matching_option;
                v_cust_profile_rec.created_by_module := r.created_by_module;
                v_cust_profile_rec.application_id := r.application_id;
                v_cust_profile_rec.review_cycle := r.review_cycle;
                v_cust_profile_rec.last_credit_review_date := r.last_credit_review_date;
                v_cust_profile_rec.party_id := r.party_id;
                v_cust_profile_rec.credit_classification := NVL (p_credit_class_code, r.credit_classification); --*******
                v_cust_profile_rec.cons_bill_level := r.cons_bill_level;
                v_cust_profile_rec.late_charge_calculation_trx := r.late_charge_calculation_trx;
                v_cust_profile_rec.credit_items_flag := r.credit_items_flag;
                v_cust_profile_rec.disputed_transactions_flag := r.disputed_transactions_flag;
                v_cust_profile_rec.late_charge_type := r.late_charge_type;
                v_cust_profile_rec.late_charge_term_id := r.late_charge_term_id;
                v_cust_profile_rec.interest_calculation_period := r.interest_calculation_period;
                v_cust_profile_rec.hold_charged_invoices_flag := r.hold_charged_invoices_flag;
                v_cust_profile_rec.message_text_id := r.message_text_id;
                v_cust_profile_rec.multiple_interest_rates_flag := r.multiple_interest_rates_flag;
                v_cust_profile_rec.charge_begin_date := r.charge_begin_date;
                v_cust_profile_rec.automatch_set_id := r.automatch_set_id;
                v_object_version_number := r.object_version_number;
                l_cust_acct_profile_amt_id := NULL;

                hz_customer_profile_v2pub.update_customer_profile (p_customer_profile_rec    => v_cust_profile_rec
                                                                  ,p_object_version_number   => v_object_version_number
                                                                  ,x_return_status           => v_return_status
                                                                  ,x_msg_count               => v_msg_count
                                                                  ,x_msg_data                => v_msg_data);

                COMMIT;

                IF v_msg_count > 0 AND v_return_status <> 'S'
                THEN
                    v_running_message1 := '';

                    FOR i IN 1 .. v_msg_count
                    LOOP
                        fnd_msg_pub.get (i
                                        ,fnd_api.g_false
                                        ,v_msg_data
                                        ,l_msg_dummy);

                        IF NVL (v_running_message1, 'null') <> v_msg_data
                        THEN
                            l_running_messaget :=
                                   l_running_messaget
                                || ' '
                                || SUBSTR ('Msg' || TO_CHAR (i) || ': ' || v_msg_data, 1, 255);
                        END IF;

                        v_running_message1 := v_msg_data;
                    END LOOP;
                END IF;

                IF v_running_message1 IS NOT NULL
                THEN
                    l_running_messaget := 'hz_customer_profile_v2pub.update_customer_profile' || v_running_message1;

                    report_errors;
                END IF;

            END LOOP;
        END IF;

        COMMIT;

        l_running_messaget := NULL;

        --********************
        BEGIN
            IF p_site_uses_rowid IS NULL AND p_level_flag = 'P'
            THEN
                SELECT profile_class_id
                  INTO v_after_profile_class_id
                  FROM xxwc_customer_maint_form
                 WHERE prof_rowid = p_prof_rowid AND cust_rowid = p_cust_rowid AND site_uses_rowid IS NULL;
            ELSIF p_site_uses_rowid IS NOT NULL AND p_level_flag = 'P'
            THEN
                SELECT profile_class_id
                  INTO v_after_profile_class_id
                  FROM xxwc_customer_maint_form
                 WHERE prof_rowid = p_prof_rowid AND cust_rowid = p_cust_rowid AND site_uses_rowid = p_site_uses_rowid;
            END IF;
        END;

        l_running_messaget := NULL;

        IF v_after_profile_class_id = 0 AND v_before_profile_class_id <> 0 AND p_level_flag = 'P'
        THEN
            xxwc_update_prifile_class (px_prof_rowid          => p_prof_rowid
                                      ,px_profile_class_id    => v_before_profile_class_id
                                      ,px_cust_rowid          => p_cust_rowid
                                      ,px_site_uses_rowid     => p_site_uses_rowid
                                      ,px_send_statement      => p_send_stmnt               --Added for ver 1.15
                                      ,px_last_updated_by     => p_last_updated_by
                                      ,px_last_update_date    => p_last_update_date
                                      ,px_last_update_login   => p_last_update_login
                                      ,px_responsibility_id   => p_responsibility_id
                                      ,px_org_id              => p_org_id);
        END IF;

        keep_profile_amounts_asbefore (p_prof_rowid);

        l_running_messaget := NULL;
        COMMIT;

        --*****************

        xxwc_ar_customer_maint_form.changes_log (p_prof_rowid
                                                ,p_site_uses_rowid
                                                ,p_cust_rowid
                                                ,p_last_updated_by
                                                ,p_last_update_date
                                                ,p_last_update_login
                                                ,'after');
        COMMIT;
    EXCEPTION
        WHEN OTHERS
        THEN
            l_running_messaget := NULL;
            l_running_messaget := 'Error...XXWC_UPDATE_CUSTOMER_PROFILE';
            set_other_error;
    END xxwc_update_customer_profile;

    --*****************************************************************************************

    PROCEDURE xxwc_update_prifile_class (px_prof_rowid           VARCHAR2
                                        ,px_profile_class_id     NUMBER
                                        ,px_cust_rowid           VARCHAR2
                                        ,px_site_uses_rowid      VARCHAR2
                    ,px_send_statement       VARCHAR2 --Added for ver 1.15
                                        ,px_last_updated_by      NUMBER
                                        ,px_last_update_date     DATE
                                        ,px_last_update_login    NUMBER
                                        ,px_responsibility_id    NUMBER
                                        ,px_org_id               NUMBER)
    IS
        v_cust_profile_rec        hz_customer_profile_v2pub.customer_profile_rec_type;
        v_object_version_number   NUMBER;
        v_return_status           VARCHAR2 (1024);
        v_msg_count               NUMBER;
        v_msg_data                VARCHAR2 (1024);
        v_running_message1        VARCHAR2 (1024);
        l_msg_dummy               VARCHAR2 (1024);
    BEGIN
        l_running_messaget := NULL;

        FOR r IN (SELECT *
                    FROM hz_customer_profiles
                   WHERE ROWID = px_prof_rowid)
        LOOP
            v_cust_profile_rec.cust_account_profile_id := r.cust_account_profile_id;
            v_cust_profile_rec.cust_account_id := r.cust_account_id;
            v_cust_profile_rec.status := r.status;
            v_cust_profile_rec.collector_id := r.collector_id;                                                 --*******
            v_cust_profile_rec.credit_analyst_id := r.credit_analyst_id;                                       --*******
            v_cust_profile_rec.credit_checking := r.credit_checking;                                           --*******
            v_cust_profile_rec.next_credit_review_date := r.next_credit_review_date;
            v_cust_profile_rec.tolerance := r.tolerance;
            v_cust_profile_rec.discount_terms := r.discount_terms;
            v_cust_profile_rec.dunning_letters := r.dunning_letters;
            v_cust_profile_rec.interest_charges := r.interest_charges;
            v_cust_profile_rec.send_statements := NVL(px_send_statement,r.send_statements);              --Modified for ver# 1.15
            v_cust_profile_rec.credit_balance_statements := NVL(px_send_statement,r.credit_balance_statements);--Modified for ver# 1.15
            v_cust_profile_rec.credit_hold := r.credit_hold;                                                   --*******
            v_cust_profile_rec.profile_class_id := NVL (px_profile_class_id, r.profile_class_id);              --*******
            v_cust_profile_rec.site_use_id := r.site_use_id;
            v_cust_profile_rec.credit_rating := r.credit_rating;
            v_cust_profile_rec.risk_code := r.risk_code;
            v_cust_profile_rec.standard_terms := r.standard_terms;                                             --*******
            v_cust_profile_rec.override_terms := r.override_terms;
            v_cust_profile_rec.dunning_letter_set_id := r.dunning_letter_set_id;
            v_cust_profile_rec.interest_period_days := r.interest_period_days;
            v_cust_profile_rec.payment_grace_days := r.payment_grace_days;
            v_cust_profile_rec.discount_grace_days := r.discount_grace_days;
--            v_cust_profile_rec.statement_cycle_id := r.statement_cycle_id;
         --commented above and added below block for ver 1.15
                BEGIN
                SELECT DECODE(px_send_statement,'N',NULL,r.statement_cycle_id)
                INTO v_cust_profile_rec.statement_cycle_id 
                FROM DUAL;
                END;

            v_cust_profile_rec.account_status := r.account_status;                                             --*******
            v_cust_profile_rec.percent_collectable := r.percent_collectable;
            v_cust_profile_rec.autocash_hierarchy_id := r.autocash_hierarchy_id;
            v_cust_profile_rec.attribute_category := r.attribute_category;
            v_cust_profile_rec.attribute1 := r.attribute1;
            v_cust_profile_rec.attribute2 := r.attribute2;                                                     --*******
            v_cust_profile_rec.attribute3 := r.attribute3;
            v_cust_profile_rec.attribute4 := r.attribute4;
            v_cust_profile_rec.attribute5 := r.attribute5;
            v_cust_profile_rec.attribute6 := r.attribute6;
            v_cust_profile_rec.attribute7 := r.attribute7;
            v_cust_profile_rec.attribute8 := r.attribute8;
            v_cust_profile_rec.attribute9 := r.attribute9;
            v_cust_profile_rec.attribute10 := r.attribute10;
            v_cust_profile_rec.attribute11 := r.attribute11;
            v_cust_profile_rec.attribute12 := r.attribute12;
            v_cust_profile_rec.attribute13 := r.attribute13;
            v_cust_profile_rec.attribute14 := r.attribute14;
            v_cust_profile_rec.attribute15 := r.attribute15;
            v_cust_profile_rec.auto_rec_incl_disputed_flag := r.auto_rec_incl_disputed_flag;
            v_cust_profile_rec.tax_printing_option := r.tax_printing_option;
            v_cust_profile_rec.charge_on_finance_charge_flag := r.charge_on_finance_charge_flag;
            v_cust_profile_rec.grouping_rule_id := r.grouping_rule_id;
            v_cust_profile_rec.clearing_days := r.clearing_days;
            v_cust_profile_rec.jgzz_attribute_category := r.jgzz_attribute_category;
            v_cust_profile_rec.jgzz_attribute1 := r.jgzz_attribute1;
            v_cust_profile_rec.jgzz_attribute2 := r.jgzz_attribute2;
            v_cust_profile_rec.jgzz_attribute3 := r.jgzz_attribute3;
            v_cust_profile_rec.jgzz_attribute4 := r.jgzz_attribute4;
            v_cust_profile_rec.jgzz_attribute5 := r.jgzz_attribute5;
            v_cust_profile_rec.jgzz_attribute6 := r.jgzz_attribute6;
            v_cust_profile_rec.jgzz_attribute7 := r.jgzz_attribute7;
            v_cust_profile_rec.jgzz_attribute8 := r.jgzz_attribute8;
            v_cust_profile_rec.jgzz_attribute9 := r.jgzz_attribute9;
            v_cust_profile_rec.jgzz_attribute10 := r.jgzz_attribute10;
            v_cust_profile_rec.jgzz_attribute11 := r.jgzz_attribute11;
            v_cust_profile_rec.jgzz_attribute12 := r.jgzz_attribute12;
            v_cust_profile_rec.jgzz_attribute13 := r.jgzz_attribute13;
            v_cust_profile_rec.jgzz_attribute14 := r.jgzz_attribute14;
            v_cust_profile_rec.jgzz_attribute15 := r.jgzz_attribute15;
            v_cust_profile_rec.global_attribute1 := r.global_attribute1;
            v_cust_profile_rec.global_attribute2 := r.global_attribute2;
            v_cust_profile_rec.global_attribute3 := r.global_attribute3;
            v_cust_profile_rec.global_attribute4 := r.global_attribute4;
            v_cust_profile_rec.global_attribute5 := r.global_attribute5;
            v_cust_profile_rec.global_attribute6 := r.global_attribute6;
            v_cust_profile_rec.global_attribute7 := r.global_attribute7;
            v_cust_profile_rec.global_attribute8 := r.global_attribute8;
            v_cust_profile_rec.global_attribute9 := r.global_attribute9;
            v_cust_profile_rec.global_attribute10 := r.global_attribute10;
            v_cust_profile_rec.global_attribute11 := r.global_attribute11;
            v_cust_profile_rec.global_attribute12 := r.global_attribute12;
            v_cust_profile_rec.global_attribute13 := r.global_attribute13;
            v_cust_profile_rec.global_attribute14 := r.global_attribute14;
            v_cust_profile_rec.global_attribute15 := r.global_attribute15;
            v_cust_profile_rec.global_attribute16 := r.global_attribute16;
            v_cust_profile_rec.global_attribute17 := r.global_attribute17;
            v_cust_profile_rec.global_attribute18 := r.global_attribute18;
            v_cust_profile_rec.global_attribute19 := r.global_attribute19;
            v_cust_profile_rec.global_attribute20 := r.global_attribute20;
            v_cust_profile_rec.global_attribute_category := r.global_attribute_category;
            v_cust_profile_rec.cons_inv_flag := r.cons_inv_flag;
            v_cust_profile_rec.cons_inv_type := r.cons_inv_type;
            v_cust_profile_rec.autocash_hierarchy_id_for_adr := r.autocash_hierarchy_id_for_adr;
            v_cust_profile_rec.lockbox_matching_option := r.lockbox_matching_option;
            v_cust_profile_rec.created_by_module := r.created_by_module;
            v_cust_profile_rec.application_id := r.application_id;
            v_cust_profile_rec.review_cycle := r.review_cycle;
            v_cust_profile_rec.last_credit_review_date := r.last_credit_review_date;
            v_cust_profile_rec.party_id := r.party_id;
            v_cust_profile_rec.credit_classification := r.credit_classification;                               --*******
            v_cust_profile_rec.cons_bill_level := r.cons_bill_level;
            v_cust_profile_rec.late_charge_calculation_trx := r.late_charge_calculation_trx;
            v_cust_profile_rec.credit_items_flag := r.credit_items_flag;
            v_cust_profile_rec.disputed_transactions_flag := r.disputed_transactions_flag;
            v_cust_profile_rec.late_charge_type := r.late_charge_type;
            v_cust_profile_rec.late_charge_term_id := r.late_charge_term_id;
            v_cust_profile_rec.interest_calculation_period := r.interest_calculation_period;
            v_cust_profile_rec.hold_charged_invoices_flag := r.hold_charged_invoices_flag;
            v_cust_profile_rec.message_text_id := r.message_text_id;
            v_cust_profile_rec.multiple_interest_rates_flag := r.multiple_interest_rates_flag;
            v_cust_profile_rec.charge_begin_date := r.charge_begin_date;
            v_cust_profile_rec.automatch_set_id := r.automatch_set_id;
            v_object_version_number := r.object_version_number;

            hz_customer_profile_v2pub.update_customer_profile (p_customer_profile_rec    => v_cust_profile_rec
                                                              ,p_object_version_number   => v_object_version_number
                                                              ,x_return_status           => v_return_status
                                                              ,x_msg_count               => v_msg_count
                                                              ,x_msg_data                => v_msg_data);

            IF v_msg_count > 0 AND v_return_status <> 'S'
            THEN
                v_running_message1 := '';

                FOR i IN 1 .. v_msg_count
                LOOP
                    fnd_msg_pub.get (i
                                    ,fnd_api.g_false
                                    ,v_msg_data
                                    ,l_msg_dummy);

                    IF NVL (v_running_message1, 'null') <> v_msg_data
                    THEN
                        l_running_messaget :=
                            l_running_messaget || ' ' || SUBSTR ('Msg' || TO_CHAR (i) || ': ' || v_msg_data, 1, 255);
                    END IF;

                    v_running_message1 := v_msg_data;
                END LOOP;
            END IF;
        END LOOP;

        COMMIT;
    EXCEPTION
        WHEN OTHERS
        THEN
            l_running_messaget := NULL;
            l_running_messaget := 'Error_Stack...XXWC_UPDATE_PRIFILE_CLASS';
            set_other_error;
    END xxwc_update_prifile_class;

    --*****************************************************************************************

    PROCEDURE changes_log (pl_prof_rowid          VARCHAR2
                          ,pl_site_uses_rowid     VARCHAR2
                          ,pl_cust_rowid          VARCHAR2
                          ,p_last_updated_by      NUMBER
                          ,p_last_update_date     DATE
                          ,p_last_update_login    NUMBER
                          ,p_stage                VARCHAR2)
    IS
        v_insert_id   NUMBER;
    BEGIN
        --archive the log
        INSERT                                                                                             /* +append */
              INTO  xxwc.xxwc_customer_maint_arch
            SELECT *
              FROM apps.xxwc_customer_maint_curr
             WHERE p_last_update_date < SYSDATE - 1 / 6;

        COMMIT;

        DELETE FROM xxwc_customer_maint_curr
              WHERE p_last_update_date < SYSDATE - 1 / 6;

        COMMIT;
        v_insert_id := xxwc.xxwc_customer_maint_curr_s.NEXTVAL;
        l_insert_id := v_insert_id;

        IF pl_site_uses_rowid IS NOT NULL
        THEN

     FOR I IN (SELECT a.PROF_ROWID              ,
           a.CUST_ROWID                     ,
           a.SITE_USES_ROWID                ,
           a.CUST_ACCOUNT_ID                ,
           a.CUST_ACCOUNT_PROFILE_ID        ,
           a.CUST_ACCT_SITE_ID              ,
           a.SITE_USE_ID                    ,
           a.PARTY_SITE_PARTY_ID            ,
           a.LV                             ,
           a.ACCOUNT_NUMBER                 ,
           a.PROFILE_PARTY_ID                ,
           a.ACCOUNT_NAME                    ,
           a.ACCOUNT_ADDRESS                 ,
           a.SITE_ADDRESS                    ,
           a.PARTY_SITE_NUMBER               ,
           a.ACCOUNT_STATUS                  ,
           a.PROFILE_ACCOUNT_STATUS          ,
           a.ACCOUNT_FREIGHT_TERM            ,
           a.COLLECTOR_ID                    ,
           a.COLLECTOR_NAME                  ,
           a.CREDIT_ANALYST_ID               ,
           a.CREDIT_ANALYST_NAME             ,
           a.CREDIT_CHECKING                 ,
           a.CREDIT_HOLD                     ,
           a.REMMIT_TO_CODE_ATTRIBUTE2       ,
           a.ACCOUNT_PAYMENT_TERMS           ,
           a.STANDARD_TERMS_NAME             ,
           a.STANDARD_TERMS_DESCRIPTION      ,
           a.CREDIT_CLASSIFICATION           ,
           a.PROFILE_CLASS_ID                ,
           a.PROFILE_CLASS_NAME              ,
           a.CREDIT_CLASSIFICATION_MEANING   ,
           a.ACCOUNT_STATUS_MEANING          ,
          a.PURPOSES                        ,
           a.SITE_USE_CODE                   ,
           a.PRIMARY_FLAG                    ,
           a.ACCT_SITE_BILL_TO_FLAG          ,
           a.ACCT_SITE_SHIP_TO_FLAG          ,
           a.SITE_STATUS                     ,
           a.LOCATION                        ,
           a.ORG_ID                          ,
           a.PRIMARY_SALESREP_ID             ,
           a.SALESREPS_NAME                 ,
           a.SITE_FREIGHT_TERMS             ,
           a.ACCT_SITE_PARTY_SITE_ID        ,
           a.ACCT_SITE_STATUS               ,
           a.SITE_USES_CREATION_DATE        ,
           a.LEVEL_FLAG                     ,
           a.CUST_ACCT_SITE_ROWID           ,
           a.PRINT_PRICE                    ,
           a.MAND_PO                        ,
           a.JOINT_CHECK                    ,
           a.CATEGORY                      
                  FROM xxwc_customer_maint_form a
                 WHERE     prof_rowid = pl_prof_rowid
                       AND site_uses_rowid = pl_site_uses_rowid
                       AND cust_rowid = pl_cust_rowid)
LOOP

            INSERT INTO xxwc.xxwc_customer_maint_curr
        (PROF_ROWID              ,
           CUST_ROWID                     ,
           SITE_USES_ROWID                ,
           CUST_ACCOUNT_ID                ,
           CUST_ACCOUNT_PROFILE_ID        ,
           CUST_ACCT_SITE_ID              ,
           SITE_USE_ID                    ,
           PARTY_SITE_PARTY_ID            ,
           LV                             ,
           ACCOUNT_NUMBER                 ,
           PROFILE_PARTY_ID                ,
           ACCOUNT_NAME                    ,
           ACCOUNT_ADDRESS                 ,
           SITE_ADDRESS                    ,
           PARTY_SITE_NUMBER               ,
           ACCOUNT_STATUS                  ,
           PROFILE_ACCOUNT_STATUS          ,
           ACCOUNT_FREIGHT_TERM            ,
           COLLECTOR_ID                    ,
           COLLECTOR_NAME                  ,
           CREDIT_ANALYST_ID               ,
           CREDIT_ANALYST_NAME             ,
           CREDIT_CHECKING                 ,
           CREDIT_HOLD                     ,
           REMMIT_TO_CODE_ATTRIBUTE2       ,
           ACCOUNT_PAYMENT_TERMS           ,
           STANDARD_TERMS_NAME             ,
           STANDARD_TERMS_DESCRIPTION      ,
           CREDIT_CLASSIFICATION           ,
           PROFILE_CLASS_ID                ,
           PROFILE_CLASS_NAME              ,
           CREDIT_CLASSIFICATION_MEANING   ,
           ACCOUNT_STATUS_MEANING          ,
          PURPOSES                        ,
           SITE_USE_CODE                   ,
           PRIMARY_FLAG                    ,
           ACCT_SITE_BILL_TO_FLAG          ,
           ACCT_SITE_SHIP_TO_FLAG          ,
           SITE_STATUS                     ,
           LOCATION                        ,
           ORG_ID                          ,
           PRIMARY_SALESREP_ID             ,
           SALESREPS_NAME                 ,
           SITE_FREIGHT_TERMS             ,
           ACCT_SITE_PARTY_SITE_ID        ,
           ACCT_SITE_STATUS               ,
           SITE_USES_CREATION_DATE        ,
           LEVEL_FLAG                     ,
           LAST_UPDATED_BY                ,
           P_LAST_UPDATE_DATE             ,
           P_LAST_UPDATE_LOGIN            ,
           P_STAGE                        ,
           INSERT_ID                      ,
           ERROR_MESSAGE                  ,
           CUST_ACCT_SITE_ROWID           ,
           PRINT_PRICE                    ,
           MAND_PO                        ,
           JOINT_CHECK                    ,
           CATEGORY                       )
 values

                ( I.PROF_ROWID                ,
           i.CUST_ROWID                     ,
           i.SITE_USES_ROWID                ,
           i.CUST_ACCOUNT_ID                ,
           i.CUST_ACCOUNT_PROFILE_ID        ,
           i.CUST_ACCT_SITE_ID              ,
           i.SITE_USE_ID                    ,
           i.PARTY_SITE_PARTY_ID            ,
           i.LV                             ,
           i.ACCOUNT_NUMBER                 ,
           i.PROFILE_PARTY_ID                ,
           i.ACCOUNT_NAME                    ,
           i.ACCOUNT_ADDRESS                 ,
           i.SITE_ADDRESS                    ,
           i.PARTY_SITE_NUMBER               ,
           i.ACCOUNT_STATUS                  ,
           i.PROFILE_ACCOUNT_STATUS          ,
           i.ACCOUNT_FREIGHT_TERM            ,
           i.COLLECTOR_ID                    ,
           i.COLLECTOR_NAME                  ,
           i.CREDIT_ANALYST_ID               ,
           i.CREDIT_ANALYST_NAME             ,
           i.CREDIT_CHECKING                 ,
           i.CREDIT_HOLD                     ,
           i.REMMIT_TO_CODE_ATTRIBUTE2       ,
           i.ACCOUNT_PAYMENT_TERMS           ,
           i.STANDARD_TERMS_NAME             ,
           i.STANDARD_TERMS_DESCRIPTION      ,
           i.CREDIT_CLASSIFICATION           ,
           i.PROFILE_CLASS_ID                ,
           i.PROFILE_CLASS_NAME              ,
           i.CREDIT_CLASSIFICATION_MEANING   ,
           i.ACCOUNT_STATUS_MEANING          ,
          i.PURPOSES                        ,
           i.SITE_USE_CODE                   ,
           i.PRIMARY_FLAG                    ,
           i.ACCT_SITE_BILL_TO_FLAG          ,
           i.ACCT_SITE_SHIP_TO_FLAG          ,
           i.SITE_STATUS                     ,
           i.LOCATION                        ,
           i.ORG_ID                          ,
           i.PRIMARY_SALESREP_ID             ,
           i.SALESREPS_NAME                 ,
           i.SITE_FREIGHT_TERMS             ,
           i.ACCT_SITE_PARTY_SITE_ID        ,
           i.ACCT_SITE_STATUS               ,
           i.SITE_USES_CREATION_DATE        ,
           i.LEVEL_FLAG                     ,
           p_last_updated_by              ,
           P_LAST_UPDATE_DATE             ,
           P_LAST_UPDATE_LOGIN            ,
           P_STAGE                        ,
           v_insert_id                    ,
           null                           ,
           i.CUST_ACCT_SITE_ROWID           ,
           i.PRINT_PRICE                    ,
           i.MAND_PO                        ,
           i.JOINT_CHECK                    ,
           i.CATEGORY                      );

END LOOP;




        ELSE

for i in (select a.PROF_ROWID                      ,
           a.CUST_ROWID                     ,
           a.SITE_USES_ROWID                ,
           a.CUST_ACCOUNT_ID                ,
           a.CUST_ACCOUNT_PROFILE_ID        ,
           a.CUST_ACCT_SITE_ID              ,
           a.SITE_USE_ID                    ,
           a.PARTY_SITE_PARTY_ID            ,
           a.LV                             ,
           a.ACCOUNT_NUMBER                 ,
           a.PROFILE_PARTY_ID                ,
           a.ACCOUNT_NAME                    ,
           a.ACCOUNT_ADDRESS                 ,
           a.SITE_ADDRESS                    ,
           a.PARTY_SITE_NUMBER               ,
           a.ACCOUNT_STATUS                  ,
           a.PROFILE_ACCOUNT_STATUS          ,
           a.ACCOUNT_FREIGHT_TERM            ,
           a.COLLECTOR_ID                    ,
           a.COLLECTOR_NAME                  ,
           a.CREDIT_ANALYST_ID               ,
           a.CREDIT_ANALYST_NAME             ,
           a.CREDIT_CHECKING                 ,
           a.CREDIT_HOLD                     ,
           a.REMMIT_TO_CODE_ATTRIBUTE2       ,
           a.ACCOUNT_PAYMENT_TERMS           ,
           a.STANDARD_TERMS_NAME             ,
           a.STANDARD_TERMS_DESCRIPTION      ,
           a.CREDIT_CLASSIFICATION           ,
           a.PROFILE_CLASS_ID                ,
           a.PROFILE_CLASS_NAME              ,
           a.CREDIT_CLASSIFICATION_MEANING   ,
           a.ACCOUNT_STATUS_MEANING          ,
          a.PURPOSES                        ,
           a.SITE_USE_CODE                   ,
           a.PRIMARY_FLAG                    ,
           a.ACCT_SITE_BILL_TO_FLAG          ,
           a.ACCT_SITE_SHIP_TO_FLAG          ,
           a.SITE_STATUS                     ,
           a.LOCATION                        ,
           a.ORG_ID                          ,
           a.PRIMARY_SALESREP_ID             ,
           a.SALESREPS_NAME                 ,
           a.SITE_FREIGHT_TERMS             ,
           a.ACCT_SITE_PARTY_SITE_ID        ,
           a.ACCT_SITE_STATUS               ,
           a.SITE_USES_CREATION_DATE        ,
           a.LEVEL_FLAG                     ,
           a.CUST_ACCT_SITE_ROWID           ,
           a.PRINT_PRICE                    ,
           a.MAND_PO                        ,
           a.JOINT_CHECK                    ,
           a.CATEGORY                      
                  FROM xxwc_customer_maint_form a
                 WHERE prof_rowid = pl_prof_rowid AND cust_rowid = pl_cust_rowid)
loop

           INSERT INTO xxwc.xxwc_customer_maint_curr
        (PROF_ROWID              ,
           CUST_ROWID                     ,
           SITE_USES_ROWID                ,
           CUST_ACCOUNT_ID                ,
           CUST_ACCOUNT_PROFILE_ID        ,
           CUST_ACCT_SITE_ID              ,
           SITE_USE_ID                    ,
           PARTY_SITE_PARTY_ID            ,
           LV                             ,
           ACCOUNT_NUMBER                 ,
           PROFILE_PARTY_ID                ,
           ACCOUNT_NAME                    ,
           ACCOUNT_ADDRESS                 ,
           SITE_ADDRESS                    ,
           PARTY_SITE_NUMBER               ,
           ACCOUNT_STATUS                  ,
           PROFILE_ACCOUNT_STATUS          ,
           ACCOUNT_FREIGHT_TERM            ,
           COLLECTOR_ID                    ,
           COLLECTOR_NAME                  ,
           CREDIT_ANALYST_ID               ,
           CREDIT_ANALYST_NAME             ,
           CREDIT_CHECKING                 ,
           CREDIT_HOLD                     ,
           REMMIT_TO_CODE_ATTRIBUTE2       ,
           ACCOUNT_PAYMENT_TERMS           ,
           STANDARD_TERMS_NAME             ,
           STANDARD_TERMS_DESCRIPTION      ,
           CREDIT_CLASSIFICATION           ,
           PROFILE_CLASS_ID                ,
           PROFILE_CLASS_NAME              ,
           CREDIT_CLASSIFICATION_MEANING   ,
           ACCOUNT_STATUS_MEANING          ,
          PURPOSES                        ,
           SITE_USE_CODE                   ,
           PRIMARY_FLAG                    ,
           ACCT_SITE_BILL_TO_FLAG          ,
           ACCT_SITE_SHIP_TO_FLAG          ,
           SITE_STATUS                     ,
           LOCATION                        ,
           ORG_ID                          ,
           PRIMARY_SALESREP_ID             ,
           SALESREPS_NAME                 ,
           SITE_FREIGHT_TERMS             ,
           ACCT_SITE_PARTY_SITE_ID        ,
           ACCT_SITE_STATUS               ,
           SITE_USES_CREATION_DATE        ,
           LEVEL_FLAG                     ,
           LAST_UPDATED_BY                ,
           P_LAST_UPDATE_DATE             ,
           P_LAST_UPDATE_LOGIN            ,
           P_STAGE                        ,
           INSERT_ID                      ,
           ERROR_MESSAGE                  ,
           CUST_ACCT_SITE_ROWID           ,
           PRINT_PRICE                    ,
           MAND_PO                        ,
           JOINT_CHECK                    ,
           CATEGORY                       )
 values
                (i.PROF_ROWID               ,
           i.CUST_ROWID                     ,
           i.SITE_USES_ROWID                ,
           i.CUST_ACCOUNT_ID                ,
           i.CUST_ACCOUNT_PROFILE_ID        ,
           i.CUST_ACCT_SITE_ID              ,
           i.SITE_USE_ID                    ,
           i.PARTY_SITE_PARTY_ID            ,
           i.LV                             ,
           i.ACCOUNT_NUMBER                 ,
           i.PROFILE_PARTY_ID                ,
           i.ACCOUNT_NAME                    ,
           i.ACCOUNT_ADDRESS                 ,
           i.SITE_ADDRESS                    ,
           i.PARTY_SITE_NUMBER               ,
           i.ACCOUNT_STATUS                  ,
           i.PROFILE_ACCOUNT_STATUS          ,
           i.ACCOUNT_FREIGHT_TERM            ,
           i.COLLECTOR_ID                    ,
           i.COLLECTOR_NAME                  ,
           i.CREDIT_ANALYST_ID               ,
           i.CREDIT_ANALYST_NAME             ,
           i.CREDIT_CHECKING                 ,
           i.CREDIT_HOLD                     ,
           i.REMMIT_TO_CODE_ATTRIBUTE2       ,
           i.ACCOUNT_PAYMENT_TERMS           ,
           i.STANDARD_TERMS_NAME             ,
           i.STANDARD_TERMS_DESCRIPTION      ,
           i.CREDIT_CLASSIFICATION           ,
           i.PROFILE_CLASS_ID                ,
           i.PROFILE_CLASS_NAME              ,
           i.CREDIT_CLASSIFICATION_MEANING   ,
           i.ACCOUNT_STATUS_MEANING          ,
          i.PURPOSES                        ,
           i.SITE_USE_CODE                   ,
           i.PRIMARY_FLAG                    ,
           i.ACCT_SITE_BILL_TO_FLAG          ,
           i.ACCT_SITE_SHIP_TO_FLAG          ,
           i.SITE_STATUS                     ,
           i.LOCATION                        ,
           i.ORG_ID                          ,
           i.PRIMARY_SALESREP_ID             ,
           i.SALESREPS_NAME                 ,
           i.SITE_FREIGHT_TERMS             ,
           i.ACCT_SITE_PARTY_SITE_ID        ,
           i.ACCT_SITE_STATUS               ,
           i.SITE_USES_CREATION_DATE        ,
           i.LEVEL_FLAG                     ,
           p_last_updated_by              ,
           P_LAST_UPDATE_DATE             ,
           P_LAST_UPDATE_LOGIN            ,
           P_STAGE                        ,
           v_insert_id                    ,
           null                           ,
           i.CUST_ACCT_SITE_ROWID           ,
           i.PRINT_PRICE                    ,
           i.MAND_PO                        ,
           i.JOINT_CHECK                    ,
           i.CATEGORY                      );


end loop;
        END IF;

        IF p_stage = 'after'
        THEN
            delete_dups_log (v_insert_id);
        END IF;

        COMMIT;
    END;

    --*****************************************************************************************
    --*****************************************************************************************

    PROCEDURE xxwc_update_cust_site_uses (p_site_rowid           VARCHAR2
                                          ,p_salesrep_id          NUMBER
                                          ,p_freight_term         VARCHAR2
                                          ,p_last_updated_by      NUMBER
                                          ,p_last_update_date     DATE
                                          ,p_last_update_login    NUMBER
                                          ,p_responsibility_id    NUMBER
                                          ,p_org_id               NUMBER)
    IS
        v_cust_site_use_rec       hz_cust_account_site_v2pub.cust_site_use_rec_type;
        v_object_version_number   NUMBER;
        v_return_status           VARCHAR2 (1024);
        v_msg_count               NUMBER;
        v_msg_data                VARCHAR2 (1024);
        v_running_message1        VARCHAR2 (1024);
        l_msg_dummy               VARCHAR2 (1024);

        l_errbuf                  CLOB;
    BEGIN
        l_running_messaget := NULL;

        FOR r IN (SELECT *
                    FROM hz_cust_site_uses
                   WHERE ROWID = p_site_rowid)
        LOOP
            v_cust_site_use_rec.site_use_id := r.site_use_id;
            v_cust_site_use_rec.cust_acct_site_id := r.cust_acct_site_id;
            v_cust_site_use_rec.site_use_code := r.site_use_code;
            v_cust_site_use_rec.primary_flag := r.primary_flag;
            v_cust_site_use_rec.status := r.status;
            v_cust_site_use_rec.location := r.location;
            v_cust_site_use_rec.contact_id := r.contact_id;
            v_cust_site_use_rec.bill_to_site_use_id := r.bill_to_site_use_id;
            v_cust_site_use_rec.orig_system_reference := r.orig_system_reference;
            v_cust_site_use_rec.orig_system := NULL;
            v_cust_site_use_rec.sic_code := r.sic_code;
            v_cust_site_use_rec.payment_term_id := r.payment_term_id;
            v_cust_site_use_rec.gsa_indicator := r.gsa_indicator;
            v_cust_site_use_rec.ship_partial := r.ship_partial;
            v_cust_site_use_rec.ship_via := r.ship_via;
            v_cust_site_use_rec.fob_point := r.fob_point;
            v_cust_site_use_rec.order_type_id := r.order_type_id;
            v_cust_site_use_rec.price_list_id := r.price_list_id;
            v_cust_site_use_rec.freight_term := NVL (p_freight_term, r.freight_term);
            v_cust_site_use_rec.warehouse_id := r.warehouse_id;
            v_cust_site_use_rec.territory_id := r.territory_id;
            v_cust_site_use_rec.attribute_category := r.attribute_category;
            v_cust_site_use_rec.attribute1 := r.attribute1;
            v_cust_site_use_rec.attribute2 := r.attribute2;
            v_cust_site_use_rec.attribute3 := r.attribute3;
            v_cust_site_use_rec.attribute4 := r.attribute4;
            v_cust_site_use_rec.attribute5 := r.attribute5;
            v_cust_site_use_rec.attribute6 := r.attribute6;
            v_cust_site_use_rec.attribute7 := r.attribute7;
            v_cust_site_use_rec.attribute8 := r.attribute8;
            v_cust_site_use_rec.attribute9 := r.attribute9;
            v_cust_site_use_rec.attribute10 := r.attribute10;
            v_cust_site_use_rec.tax_reference := r.tax_reference;
            v_cust_site_use_rec.sort_priority := r.sort_priority;
            v_cust_site_use_rec.tax_code := r.tax_code;
            v_cust_site_use_rec.attribute11 := r.attribute11;
            v_cust_site_use_rec.attribute12 := r.attribute12;
            v_cust_site_use_rec.attribute13 := r.attribute13;
            v_cust_site_use_rec.attribute14 := r.attribute14;
            v_cust_site_use_rec.attribute15 := r.attribute15;
            v_cust_site_use_rec.attribute16 := r.attribute16;
            v_cust_site_use_rec.attribute17 := r.attribute17;
            v_cust_site_use_rec.attribute18 := r.attribute18;
            v_cust_site_use_rec.attribute19 := r.attribute19;
            v_cust_site_use_rec.attribute20 := r.attribute20;
            v_cust_site_use_rec.attribute21 := r.attribute21;
            v_cust_site_use_rec.attribute22 := r.attribute22;
            v_cust_site_use_rec.attribute23 := r.attribute23;
            v_cust_site_use_rec.attribute24 := r.attribute24;
            v_cust_site_use_rec.attribute25 := r.attribute25;
            v_cust_site_use_rec.demand_class_code := r.demand_class_code;
            v_cust_site_use_rec.tax_header_level_flag := r.tax_header_level_flag;
            v_cust_site_use_rec.tax_rounding_rule := r.tax_rounding_rule;
            v_cust_site_use_rec.global_attribute1 := r.global_attribute1;
            v_cust_site_use_rec.global_attribute2 := r.global_attribute2;
            v_cust_site_use_rec.global_attribute3 := r.global_attribute3;
            v_cust_site_use_rec.global_attribute4 := r.global_attribute4;
            v_cust_site_use_rec.global_attribute5 := r.global_attribute5;
            v_cust_site_use_rec.global_attribute6 := r.global_attribute6;
            v_cust_site_use_rec.global_attribute7 := r.global_attribute7;
            v_cust_site_use_rec.global_attribute8 := r.global_attribute8;
            v_cust_site_use_rec.global_attribute9 := r.global_attribute9;
            v_cust_site_use_rec.global_attribute10 := r.global_attribute10;
            v_cust_site_use_rec.global_attribute11 := r.global_attribute11;
            v_cust_site_use_rec.global_attribute12 := r.global_attribute12;
            v_cust_site_use_rec.global_attribute13 := r.global_attribute13;
            v_cust_site_use_rec.global_attribute14 := r.global_attribute14;
            v_cust_site_use_rec.global_attribute15 := r.global_attribute15;
            v_cust_site_use_rec.global_attribute16 := r.global_attribute16;
            v_cust_site_use_rec.global_attribute17 := r.global_attribute17;
            v_cust_site_use_rec.global_attribute18 := r.global_attribute18;
            v_cust_site_use_rec.global_attribute19 := r.global_attribute19;
            v_cust_site_use_rec.global_attribute20 := r.global_attribute20;
            v_cust_site_use_rec.global_attribute_category := r.global_attribute_category;
            v_cust_site_use_rec.primary_salesrep_id := NVL (p_salesrep_id, r.primary_salesrep_id);
            v_cust_site_use_rec.finchrg_receivables_trx_id := r.finchrg_receivables_trx_id;
            v_cust_site_use_rec.dates_negative_tolerance := r.dates_negative_tolerance;
            v_cust_site_use_rec.dates_positive_tolerance := r.dates_positive_tolerance;
            v_cust_site_use_rec.date_type_preference := r.date_type_preference;
            v_cust_site_use_rec.over_shipment_tolerance := r.over_shipment_tolerance;
            v_cust_site_use_rec.under_shipment_tolerance := r.under_shipment_tolerance;
            v_cust_site_use_rec.item_cross_ref_pref := r.item_cross_ref_pref;
            v_cust_site_use_rec.over_return_tolerance := r.over_return_tolerance;
            v_cust_site_use_rec.under_return_tolerance := r.under_return_tolerance;
            v_cust_site_use_rec.ship_sets_include_lines_flag := r.ship_sets_include_lines_flag;
            v_cust_site_use_rec.arrivalsets_include_lines_flag := r.arrivalsets_include_lines_flag;
            v_cust_site_use_rec.sched_date_push_flag := r.sched_date_push_flag;
            v_cust_site_use_rec.invoice_quantity_rule := r.invoice_quantity_rule;
            v_cust_site_use_rec.pricing_event := r.pricing_event;
            v_cust_site_use_rec.gl_id_rec := r.gl_id_rec;
            v_cust_site_use_rec.gl_id_rev := r.gl_id_rev;
            v_cust_site_use_rec.gl_id_tax := r.gl_id_tax;
            v_cust_site_use_rec.gl_id_freight := r.gl_id_freight;
            v_cust_site_use_rec.gl_id_clearing := r.gl_id_clearing;
            v_cust_site_use_rec.gl_id_unbilled := r.gl_id_unbilled;
            v_cust_site_use_rec.gl_id_unearned := r.gl_id_unearned;
            v_cust_site_use_rec.gl_id_unpaid_rec := r.gl_id_unpaid_rec;
            v_cust_site_use_rec.gl_id_remittance := r.gl_id_remittance;
            v_cust_site_use_rec.gl_id_factor := r.gl_id_factor;
            v_cust_site_use_rec.tax_classification := r.tax_classification;
            v_cust_site_use_rec.created_by_module := r.created_by_module;
            v_cust_site_use_rec.application_id := r.application_id;
            v_cust_site_use_rec.org_id := r.org_id;
            v_object_version_number := r.object_version_number;
        END LOOP;

        hz_cust_account_site_v2pub.update_cust_site_use (p_cust_site_use_rec       => v_cust_site_use_rec
                                                        ,p_object_version_number   => v_object_version_number
                                                        ,x_return_status           => v_return_status
                                                        ,x_msg_count               => v_msg_count
                                                        ,x_msg_data                => v_msg_data);

        COMMIT;

        IF v_msg_count > 0 AND v_return_status <> 'S'
        THEN
            v_running_message1 := '';

            FOR i IN 1 .. v_msg_count
            LOOP
                fnd_msg_pub.get (i
                                ,fnd_api.g_false
                                ,v_msg_data
                                ,l_msg_dummy);

                IF NVL (v_running_message1, 'null') <> v_msg_data
                THEN
                    l_running_messaget :=
                        l_running_messaget || ' ' || SUBSTR ('Msg' || TO_CHAR (i) || ': ' || v_msg_data, 1, 255);
                END IF;

                v_running_message1 := v_msg_data;
            END LOOP;
        END IF;
    --  END IF;
    EXCEPTION
        WHEN OTHERS
        THEN
            l_running_messaget := NULL;
            l_running_messaget := 'Error...XXWC_UPDATE_CUST_SITE_USES';
            set_other_error;
    END xxwc_update_cust_site_uses;

    --*****************************************************************************************
    --*****************************************************************************************
    --*****************************************************************************************
    --*****************************************************************************************
    --*****************************************************************************************
    --*****************************************************************************************
    --*****************************************************************************************

    FUNCTION get_acct_site_purposes (p_acct_site_id IN NUMBER)
        RETURN VARCHAR2
    IS
        l_site_use_purpose        VARCHAR2 (100);
        l_all_site_use_purposes   VARCHAR2 (1000) := '';

        CURSOR c_site_use_purposes (
            l_acct_site_id IN NUMBER)
        IS
              SELECT al.meaning
                FROM hz_cust_acct_sites s, hz_cust_site_uses u, ar_lookups al
               WHERE     s.cust_acct_site_id = l_acct_site_id
                     AND u.cust_acct_site_id = s.cust_acct_site_id                                 -- AND U.STATUS = 'A'
                     AND al.lookup_type = 'SITE_USE_CODE'
                     AND al.lookup_code = u.site_use_code
            ORDER BY al.meaning;
    BEGIN
        OPEN c_site_use_purposes (p_acct_site_id);

        LOOP
            FETCH c_site_use_purposes INTO l_site_use_purpose;

            IF c_site_use_purposes%NOTFOUND
            THEN
                EXIT;
            END IF;

            IF l_all_site_use_purposes IS NOT NULL
            THEN
                l_all_site_use_purposes := CONCAT (l_all_site_use_purposes, ', ');
            END IF;

            l_all_site_use_purposes := CONCAT (l_all_site_use_purposes, l_site_use_purpose);
        END LOOP;

        CLOSE c_site_use_purposes;

        RETURN l_all_site_use_purposes;
    END get_acct_site_purposes;

    ---*******************************************************************
    ---*******************************************************************
    ---*******************************************************************
    --added by Rasikha Galimova:23-jan-2013 - to show in the form site use purpose, not account site
    FUNCTION get_site_purposes (p_site_use_id IN NUMBER)
        RETURN VARCHAR2
    IS
        l_site_use_purpose        VARCHAR2 (100);
        l_all_site_use_purposes   VARCHAR2 (1000) := '';

        CURSOR c_site_use_purposes (
            l_site_use_id IN NUMBER)
        IS
              SELECT DECODE (u.primary_flag,  'N', '',  'Y', 'P',  '') || REPLACE (al.meaning, ' ', '')
                FROM hz_cust_acct_sites s, hz_cust_site_uses u, ar_lookups al
               WHERE     u.site_use_id = l_site_use_id
                     AND u.cust_acct_site_id = s.cust_acct_site_id                                 -- AND U.STATUS = 'A'
                     AND al.lookup_type = 'SITE_USE_CODE'
                     AND al.lookup_code = u.site_use_code
            ORDER BY u.primary_flag DESC;
    BEGIN
        OPEN c_site_use_purposes (p_site_use_id);

        LOOP
            FETCH c_site_use_purposes INTO l_site_use_purpose;

            IF c_site_use_purposes%NOTFOUND
            THEN
                EXIT;
            END IF;

            IF l_all_site_use_purposes IS NOT NULL
            THEN
                l_all_site_use_purposes := CONCAT (l_all_site_use_purposes, ',');
            END IF;

            l_all_site_use_purposes := CONCAT (l_all_site_use_purposes, l_site_use_purpose);
        END LOOP;

        CLOSE c_site_use_purposes;

        RETURN l_all_site_use_purposes;
    END get_site_purposes;

    ---*******************************************************************
    ---*******************************************************************
    ---*******************************************************************

    PROCEDURE delete_dups_log (log_id NUMBER)
    IS
    BEGIN
        FOR r IN (SELECT COUNT ('a') count_dif
                    FROM (SELECT prof_rowid
                                ,cust_rowid
                                ,site_uses_rowid
                                ,cust_account_id
                                ,cust_account_profile_id
                                ,cust_acct_site_id
                                ,site_use_id
                                ,party_site_party_id
                                ,lv
                                ,account_number
                                ,profile_party_id
                                ,account_name
                                ,account_address
                                ,site_address
                                ,party_site_number
                                ,account_status
                                ,profile_account_status
                                ,account_freight_term
                                ,collector_id
                                ,collector_name
                                ,credit_analyst_id
                                ,credit_analyst_name
                                ,credit_checking
                                ,credit_hold
                                ,remmit_to_code_attribute2
                                ,account_payment_terms
                                ,standard_terms_name
                                ,standard_terms_description
                                ,credit_classification
                                ,profile_class_id
                                ,profile_class_name
                                ,credit_classification_meaning
                                ,account_status_meaning
                                ,purposes
                                ,site_use_code
                                ,primary_flag
                                ,acct_site_bill_to_flag
                                ,acct_site_ship_to_flag
                                ,site_status
                                ,location
                                ,org_id
                                ,primary_salesrep_id
                                ,salesreps_name
                                ,site_freight_terms
                                ,acct_site_party_site_id
                                ,acct_site_status
                                ,site_uses_creation_date
                                ,level_flag
                            FROM apps.xxwc_customer_maint_curr
                           WHERE insert_id = log_id AND p_stage = 'after' AND error_message IS NULL
                          MINUS
                          SELECT prof_rowid
                                ,cust_rowid
                                ,site_uses_rowid
                                ,cust_account_id
                                ,cust_account_profile_id
                                ,cust_acct_site_id
                                ,site_use_id
                                ,party_site_party_id
                                ,lv
                                ,account_number
                                ,profile_party_id
                                ,account_name
                                ,account_address
                                ,site_address
                                ,party_site_number
                                ,account_status
                                ,profile_account_status
                                ,account_freight_term
                                ,collector_id
                                ,collector_name
                                ,credit_analyst_id
                                ,credit_analyst_name
                                ,credit_checking
                                ,credit_hold
                                ,remmit_to_code_attribute2
                                ,account_payment_terms
                                ,standard_terms_name
                                ,standard_terms_description
                                ,credit_classification
                                ,profile_class_id
                                ,profile_class_name
                                ,credit_classification_meaning
                                ,account_status_meaning
                                ,purposes
                                ,site_use_code
                                ,primary_flag
                                ,acct_site_bill_to_flag
                                ,acct_site_ship_to_flag
                                ,site_status
                                ,location
                                ,org_id
                                ,primary_salesrep_id
                                ,salesreps_name
                                ,site_freight_terms
                                ,acct_site_party_site_id
                                ,acct_site_status
                                ,site_uses_creation_date
                                ,level_flag
                            FROM apps.xxwc_customer_maint_curr
                           WHERE insert_id = (log_id - 1) AND p_stage = 'before' AND error_message IS NULL))
        LOOP
            IF r.count_dif = 0
            THEN
                DELETE FROM xxwc_customer_maint_curr
                      WHERE insert_id IN ( (log_id - 1), log_id);
            END IF;
        END LOOP;
    END;

--*****************************************************************************************
-- Version# 1.13 > Start
PROCEDURE xxwc_update_cust_siteS (p_cust_acct_site_rowid           VARCHAR2
                                  ,p_man_po                VARCHAR2
                                  ,p_print_price           VARCHAR2
                                  ,p_joint_check           VARCHAR2
                                  ,p_category              VARCHAR2
                                  ,p_tax_exempt           VARCHAR2     --Added for Ver#1.15
                                  ,p_tax_exempt_type       VARCHAR2     --Added for Ver#1.15
				  ,p_geocode_override 	   VARCHAR2	 --Added for Ver 1.18
                                  ,p_last_updated_by       NUMBER
                                  ,p_last_update_date      DATE
                                  ,p_last_update_login     NUMBER
                                  ,p_responsibility_id     NUMBER
                                  ,p_org_id                NUMBER)
    IS
        r_custacctsiterec              apps.hz_cust_account_site_v2pub.cust_acct_site_rec_type;
        r_blankcustacctsiterec         apps.hz_cust_account_site_v2pub.cust_acct_site_rec_type;
        v_returnstatus           VARCHAR2 (1024);
        V_MSGCOUNT               NUMBER;
        V_MSGDATA               VARCHAR2 (1024);
        v_running_message1        VARCHAR2 (1024);
        l_msg_dummy               VARCHAR2 (1024);

        l_errbuf                  CLOB;

--Added below for ver#1.18
  	p_location_rec          HZ_LOCATION_V2PUB.LOCATION_REC_TYPE;
        l_object_version_number NUMBER;
	l_return_status         VARCHAR2(2000);
	l_msg_count             NUMBER;
	l_msg_data              VARCHAR2(2000);

    BEGIN
        l_running_messaget := NULL;

        FOR i IN (SELECT *
                    FROM hz_cust_acct_sites
                   WHERE ROWID = p_cust_acct_site_rowid)
        LOOP
            r_custacctsiterec := r_blankcustacctsiterec;
            r_custacctsiterec.cust_account_id := i.cust_account_id;
            r_custacctsiterec.party_site_id := i.party_site_id;
            r_custacctsiterec.org_id := p_org_id;
           -- r_custacctsiterec.created_by_module := 'ARI';
            r_custacctsiterec.cust_acct_site_id :=i.cust_acct_site_id;
            r_custacctsiterec.customer_category_code:=nvl(p_category,i.customer_category_code);
            r_custacctsiterec.attribute_category :=i.attribute_category;
            r_custacctsiterec.attribute1 := nvl(p_print_price,i.attribute1);
            r_custacctsiterec.attribute3 := nvl(p_man_po,i.attribute3);
            r_custacctsiterec.attribute12 := nvl(p_joint_check,i.attribute12);
            r_custacctsiterec.attribute16 := nvl(p_tax_exempt,i.attribute16);          --Modified for Ver#1.15
            r_custacctsiterec.attribute15 := nvl(p_tax_exempt_type,i.attribute15);     --Modified for Ver#1.15
            hz_cust_account_site_v2pub.update_cust_acct_site
                                                     (apps.fnd_api.g_true,
                                                      r_custacctsiterec,
                                                      I.OBJECT_VERSION_NUMBER,
                                                      v_returnstatus,
                                                      v_msgcount,
                                                      v_msgdata
                                                     );
            fnd_file.put_line (fnd_file.LOG,
                               'v_returnstatus....' || v_returnstatus
                              );
            fnd_file.put_line (fnd_file.LOG, 'v_msgcount....' || v_msgcount);
            fnd_file.put_line (fnd_file.LOG, 'v_msgdata....' || v_msgdata);




        IF v_msgcount > 0 AND v_returnstatus <> 'S'
        THEN
            v_running_message1 := '';

            FOR i IN 1 .. v_msgcount
            LOOP
                fnd_msg_pub.get (i
                                ,fnd_api.g_false
                                ,v_msgdata
                                ,l_msg_dummy);

                IF NVL (v_running_message1, 'null') <> v_msgdata
                THEN
                    l_running_messaget :=
                        l_running_messaget || ' ' || SUBSTR ('Msg' || TO_CHAR (i) || ': ' || v_msgdata, 1, 255);
                END IF;

                v_running_message1 := v_msgdata;

        IF v_running_message1 IS NOT NULL
                THEN
                    l_running_messaget := 'hz_customer_profile_v2pub.update_customer_profile' || v_running_message1;

                    report_errors;
                END IF;


                 set_other_error;
            END LOOP;
        END IF;

--<<For ver 1.18 START>>
 IF p_geocode_override IS NOT NULL THEN
---Location API
        FOR j IN (SELECT hl.*
                    FROM hz_locations hl,
                         hz_party_sites hps
                   WHERE hl.location_id = hps.location_id
                     AND hps.party_site_id = i.party_site_id)
       LOOP
     	  p_location_rec.location_id 	   := j.location_id;
       	  p_location_rec.sales_tax_geocode := p_geocode_override ;
          l_object_version_number    	   := j.object_version_number;
      
hz_location_v2pub.update_location ( p_init_msg_list           => FND_API.G_TRUE,
             			    p_location_rec            => p_location_rec,
             			    p_object_version_number   => l_object_version_number,
             			    x_return_status           => v_returnstatus,
             			    x_msg_count               => v_msgcount,
             			    x_msg_data                => v_msgdata

                  ); 
commit;
     IF v_msgcount > 0 AND v_returnstatus <> 'S'
        THEN
            v_running_message1 := '';

            FOR i IN 1 .. v_msgcount
            LOOP
                fnd_msg_pub.get (i
                                ,fnd_api.g_false
                                ,v_msgdata
                                ,l_msg_dummy);

                IF NVL (v_running_message1, 'null') <> v_msgdata
                THEN
                    l_running_messaget :=
                        l_running_messaget || ' ' || SUBSTR ('Msg' || TO_CHAR (i) || ': ' || v_msgdata, 1, 255);
                END IF;

                v_running_message1 := v_msgdata;

        IF v_running_message1 IS NOT NULL
                THEN
                    l_running_messaget := 'hz_location_v2pub.update_location ' || v_running_message1;

                    report_errors;
                END IF;

                 set_other_error;
            END LOOP;
        END IF;
--END
    END LOOP;
  END IF;     --<< For ver 1.18 END >>

     END LOOP;   

    --  END IF;
    EXCEPTION
        WHEN OTHERS
        THEN
            l_running_messaget := NULL;
            l_running_messaget := 'Error...XXWC_UPDATE_CUST_SITE_USES';
        v_running_message1 :=l_running_messaget;

        IF v_running_message1 IS NOT NULL
                THEN
                    l_running_messaget := 'hz_customer_profile_v2pub.update_customer_profile' || v_running_message1;

                    report_errors;
                END IF;

            set_other_error;
    END xxwc_update_cust_siteS;
-- Version# 1.13 < End

END;

    --*****************************************************************************************
    --*****************************************************************************************
    --*****************************************************************************************
    --*****************************************************************************************
    --*****************************************************************************************
    --*****************************************************************************************
    --*****************************************************************************************
/
