CREATE OR REPLACE PACKAGE BODY APPS.XXWC_IEX_COLL_IND_PKG
AS
   /**************************************************************************
      *
      * FUNCTION | PROCEDURE | CURSOR
      *  XXWC_IEX_COLL_IND_PKG
      *
      * DESCRIPTION
      *  Collections agent Metrics program
      *
      * PARAMETERS
      * ==========
      * NAME              TYPE     DESCRIPTION
     .* ----------------- -------- ---------------------------------------------
      *
      *
      *
      *
      * CALLED BY
      *   Which program, if any, calls this one
      *
      * HISTORY
      * =======
      *
      * VERSION DATE        AUTHOR(S)       DESCRIPTION
      * ------- ----------- --------------- ------------------------------------
      * 1.0    01/26/2013   Maharajan S    TMS#20131016-00469 Initial Creation
      * 1.1    06/22/2015   Maharajan S    TMS# 20150123-00197 AR - Customer Flag as indicator to auto apply credits
      *************************************************************************/
l_sec                VARCHAR2(255);
l_procedure_name     VARCHAR2(75) := 'XXWC_IEX_COLL_IND_PKG';
l_distro_list    VARCHAR2(75) DEFAULT 'HDSOracleDevelopers@hdsupply.com';

   -------------------------------------------------------------------------
   --Function to get profile class name
   -------------------------------------------------------------------------
   FUNCTION GET_PROFILE_NAME (p_cust_account_id IN NUMBER)
      RETURN VARCHAR2
   AS
      l_profile_name   VARCHAR2 (100);
   BEGIN
   BEGIN
      SELECT hpc.name
        INTO l_profile_name
        FROM hz_cust_accounts hca,
             hz_customer_profiles hcp,
             hz_cust_profile_classes hpc
       WHERE     hcp.profile_class_id = hpc.profile_class_id
             AND hca.cust_account_id = hcp.cust_account_id
             AND hcp.site_use_id IS NULL
             AND hca.cust_account_id = p_cust_account_id;
   EXCEPTION
    WHEN NO_DATA_FOUND THEN
     l_profile_name := 'NA'; 
    WHEN OTHERS THEN
       xxcus_error_pkg.xxcus_error_main_api(p_called_from       => l_procedure_name
                                          ,p_calling           => 'Exception for function get_profile_name'
                                          ,p_request_id        => ' '
                                          ,p_ora_error_msg     => 'Error while getting profile name for cust account id '||p_cust_account_id
                                          ,p_error_desc        => ' '
                                          ,p_distribution_list => l_distro_list
                                          ,p_module            => 'AR');
   END;
    RETURN l_profile_name;
   END GET_PROFILE_NAME;

   -------------------------------------------------------------------------
   --Function to get ACCOUNT STATUS
   -------------------------------------------------------------------------
   FUNCTION GET_ACCOUNT_STATUS (p_cust_account_id IN NUMBER)
      RETURN VARCHAR2
   AS
      l_account_status   VARCHAR2 (100);
   BEGIN
   BEGIN
      SELECT hcp.account_status
        INTO l_account_status
        FROM hz_cust_accounts hca,
             hz_customer_profiles hcp,
             hz_cust_profile_classes hpc
       WHERE     hcp.profile_class_id = hpc.profile_class_id
             AND hca.cust_account_id = hcp.cust_account_id
             AND hcp.site_use_id IS NULL
             AND hca.cust_account_id = p_cust_account_id;
    EXCEPTION
    WHEN NO_DATA_FOUND THEN
     l_account_status := 'NA'; 
    WHEN OTHERS THEN
       xxcus_error_pkg.xxcus_error_main_api(p_called_from       => l_procedure_name
                                          ,p_calling           => 'Exception for function GET_ACCOUNT_STATUS'
                                          ,p_request_id        => ' '
                                          ,p_ora_error_msg     => 'Error while getting account status for cust account id '||p_cust_account_id
                                          ,p_error_desc        => ' '
                                          ,p_distribution_list => l_distro_list
                                          ,p_module            => 'AR');
   END;

      RETURN l_account_status;
   END GET_ACCOUNT_STATUS;

   -------------------------------------------------------------------------
   --Function to get last 12 months average sales
   -------------------------------------------------------------------------

   FUNCTION GET_AVG_SALES (p_cust_account_id IN NUMBER)
      RETURN NUMBER
   AS
      l_avg_sales   NUMBER;
   BEGIN
   BEGIN
      SELECT ROUND(SUM (amount_line_items_original)/12,2)
        INTO l_avg_sales
      FROM ar_payment_schedules
      WHERE class NOT LIKE 'PMT'
        AND TRUNC (GL_DATE) BETWEEN TRUNC(LAST_DAY (ADD_MONTHS (SYSDATE, -1)))-365
        AND TRUNC (LAST_DAY (ADD_MONTHS (SYSDATE, -1)))
        AND customer_id = p_cust_account_id;
    EXCEPTION
    WHEN NO_DATA_FOUND THEN
     l_avg_sales := 0 ; 
    WHEN OTHERS THEN
       xxcus_error_pkg.xxcus_error_main_api(p_called_from       => l_procedure_name
                                          ,p_calling           => 'Exception for function GET_AVG_SALES'
                                          ,p_request_id        => ' '
                                          ,p_ora_error_msg     => 'Error while getting account average sales for cust account id '||p_cust_account_id
                                          ,p_error_desc        => ' '
                                          ,p_distribution_list => l_distro_list
                                          ,p_module            => 'AR');
   END;

   RETURN l_avg_sales;

   END GET_AVG_SALES;

   -------------------------------------------------------------------------
   --Function to get site classification
   -------------------------------------------------------------------------

   FUNCTION GET_SITE_CLASS (p_cust_site_use_id IN NUMBER)
      RETURN VARCHAR2
   AS
      l_site_class   VARCHAR2 (30);
   BEGIN
      BEGIN
         SELECT hcsu.attribute1
           INTO l_site_class
           FROM hz_cust_site_uses hcsu,
                hz_cust_acct_sites hcas,
                hz_cust_accounts hca
          WHERE     hcsu.cust_acct_site_id = hcas.cust_acct_site_id
                AND hcas.cust_account_id = hca.cust_account_id
                AND NVL (hcas.status, 'A') = 'A'
                AND NVL (hcsu.status, 'A') = 'A'
                AND NVL (hca.status, 'A') = 'A'
                AND hcsu.site_use_id = p_cust_site_use_id;
    EXCEPTION
    WHEN NO_DATA_FOUND THEN
     l_site_class := 'NA' ; 
    WHEN OTHERS THEN
       xxcus_error_pkg.xxcus_error_main_api(p_called_from       => l_procedure_name
                                          ,p_calling           => 'Exception for function GET_SITE_CLASS'
                                          ,p_request_id        => ' '
                                          ,p_ora_error_msg     => 'Error while getting site class for site use id '||p_cust_site_use_id
                                          ,p_error_desc        => ' '
                                          ,p_distribution_list => l_distro_list
                                          ,p_module            => 'AR');
   END;


      RETURN l_site_class;
   END GET_SITE_CLASS;

   -------------------------------------------------------------------------
   --Function to get primary flag
   -------------------------------------------------------------------------

   FUNCTION GET_PRIMARY_FLAG (p_cust_site_use_id IN NUMBER)
      RETURN VARCHAR2
   AS
      l_primary_flag   VARCHAR2 (30);
   BEGIN
      BEGIN
         SELECT hcsu.primary_flag
           INTO l_primary_flag
           FROM hz_cust_site_uses hcsu,
                hz_cust_acct_sites hcas,
                hz_cust_accounts hca
          WHERE     hcsu.cust_acct_site_id = hcas.cust_acct_site_id
                AND hcas.cust_account_id = hca.cust_account_id
                AND NVL (hcas.status, 'A') = 'A'
                AND NVL (hcsu.status, 'A') = 'A'
                AND NVL (hca.status, 'A') = 'A'
                AND hcsu.site_use_code = 'BILL_TO'
                AND hcsu.site_use_id = p_cust_site_use_id;
    EXCEPTION
    WHEN NO_DATA_FOUND THEN
     l_primary_flag := 'NA' ; 
    WHEN OTHERS THEN
       xxcus_error_pkg.xxcus_error_main_api(p_called_from       => l_procedure_name
                                          ,p_calling           => 'Exception for function GET_PRIMARY_FLAG'
                                          ,p_request_id        => ' '
                                          ,p_ora_error_msg     => 'Error while getting primary flag for site use id '||p_cust_site_use_id
                                          ,p_error_desc        => ' '
                                          ,p_distribution_list => l_distro_list
                                          ,p_module            => 'AR');
   END;


      RETURN l_primary_flag;
   END GET_PRIMARY_FLAG;

   -------------------------------------------------------------------------
   --Function to get job info on file
   -------------------------------------------------------------------------

   FUNCTION GET_JOB_INFO (p_cust_site_use_id IN NUMBER)
      RETURN VARCHAR2
   AS
      l_job_info   VARCHAR2 (30);
   BEGIN
      BEGIN
         SELECT hcas.attribute14
           INTO l_job_info
           FROM hz_cust_site_uses hcsu,
                hz_cust_acct_sites hcas,
                hz_cust_accounts hca
          WHERE     hcsu.cust_acct_site_id = hcas.cust_acct_site_id
                AND hcas.cust_account_id = hca.cust_account_id
                AND NVL (hcas.status, 'A') = 'A'
                AND NVL (hcsu.status, 'A') = 'A'
                AND NVL (hca.status, 'A') = 'A'
                AND hcsu.site_use_id = p_cust_site_use_id;
    EXCEPTION
    WHEN NO_DATA_FOUND THEN
     l_job_info := 'NA' ; 
    WHEN OTHERS THEN
       xxcus_error_pkg.xxcus_error_main_api(p_called_from       => l_procedure_name
                                          ,p_calling           => 'Exception for function GET_JOB_INFO'
                                          ,p_request_id        => ' '
                                          ,p_ora_error_msg     => 'Error while getting job information for site use id '||p_cust_site_use_id
                                          ,p_error_desc        => ' '
                                          ,p_distribution_list => l_distro_list
                                          ,p_module            => 'AR');
   END;


      RETURN l_job_info;
   END GET_JOB_INFO;

   -------------------------------------------------------------------------
   --Function to get PRELIM SENT DATE
   -------------------------------------------------------------------------

   FUNCTION GET_PRELIM_DATE (p_cust_site_use_id IN NUMBER)
      RETURN VARCHAR2
   AS
      l_prelim_date   VARCHAR2 (30);
   BEGIN
      BEGIN
         SELECT hcas.attribute20
           INTO l_prelim_date
           FROM hz_cust_site_uses hcsu,
                hz_cust_acct_sites hcas,
                hz_cust_accounts hca
          WHERE     hcsu.cust_acct_site_id = hcas.cust_acct_site_id
                AND hcas.cust_account_id = hca.cust_account_id
                AND NVL (hcas.status, 'A') = 'A'
                AND NVL (hcsu.status, 'A') = 'A'
                AND NVL (hca.status, 'A') = 'A'
                AND hcsu.site_use_id = p_cust_site_use_id;
    EXCEPTION
    WHEN NO_DATA_FOUND THEN
     l_prelim_date  := 'NA' ; 
    WHEN OTHERS THEN
       xxcus_error_pkg.xxcus_error_main_api(p_called_from       => l_procedure_name
                                          ,p_calling           => 'Exception for function GET_PRELIM_DATE'
                                          ,p_request_id        => ' '
                                          ,p_ora_error_msg     => 'Error while getting job information for site use id '||p_cust_site_use_id
                                          ,p_error_desc        => ' '
                                          ,p_distribution_list => l_distro_list
                                          ,p_module            => 'AR');
   END;

      RETURN l_prelim_date;
   END GET_PRELIM_DATE;

   -------------------------------------------------------------------------
   --Function to get PRELIM NUMBER
   -------------------------------------------------------------------------

   FUNCTION GET_PRELIM_NUMBER (p_cust_site_use_id IN NUMBER)
      RETURN VARCHAR2
   AS
      l_prelim_number   VARCHAR2 (30);
   BEGIN
      BEGIN
         SELECT hcas.attribute19
           INTO l_prelim_number
           FROM hz_cust_site_uses hcsu,
                hz_cust_acct_sites hcas,
                hz_cust_accounts hca
          WHERE     hcsu.cust_acct_site_id = hcas.cust_acct_site_id
                AND hcas.cust_account_id = hca.cust_account_id
                AND NVL (hcas.status, 'A') = 'A'
                AND NVL (hcsu.status, 'A') = 'A'
                AND NVL (hca.status, 'A') = 'A'
                AND hcsu.site_use_id = p_cust_site_use_id;
    EXCEPTION
    WHEN NO_DATA_FOUND THEN
     l_prelim_number  := 'NA' ; 
    WHEN OTHERS THEN
       xxcus_error_pkg.xxcus_error_main_api(p_called_from       => l_procedure_name
                                          ,p_calling           => 'Exception for function GET_PRELIM_NUMBER'
                                          ,p_request_id        => ' '
                                          ,p_ora_error_msg     => 'Error while getting prelim number for site use id '||p_cust_site_use_id
                                          ,p_error_desc        => ' '
                                          ,p_distribution_list => l_distro_list
                                          ,p_module            => 'AR');
   END;
      RETURN l_prelim_number;
   END GET_PRELIM_NUMBER;


   -------------------------------------------------------------------------
   --Function to get TAX CERT TYPE
   -------------------------------------------------------------------------

   FUNCTION GET_TAX_CERT (p_cust_site_use_id IN NUMBER)
      RETURN VARCHAR2
   AS
      l_tax_cert   VARCHAR2 (30);
   BEGIN
      BEGIN
         SELECT hcas.attribute15
           INTO l_tax_cert
           FROM hz_cust_site_uses hcsu,
                hz_cust_acct_sites hcas,
                hz_cust_accounts hca
          WHERE     hcsu.cust_acct_site_id = hcas.cust_acct_site_id
                AND hcas.cust_account_id = hca.cust_account_id
                AND NVL (hcas.status, 'A') = 'A'
                AND NVL (hcsu.status, 'A') = 'A'
                AND NVL (hca.status, 'A') = 'A'
                AND hcsu.site_use_id = p_cust_site_use_id;
    EXCEPTION
    WHEN NO_DATA_FOUND THEN
     l_tax_cert  := 'NA' ; 
    WHEN OTHERS THEN
       xxcus_error_pkg.xxcus_error_main_api(p_called_from       => l_procedure_name
                                          ,p_calling           => 'Exception for function GET_TAX_CERT'
                                          ,p_request_id        => ' '
                                          ,p_ora_error_msg     => 'Error while getting tax cert for site use id '||p_cust_site_use_id
                                          ,p_error_desc        => ' '
                                          ,p_distribution_list => l_distro_list
                                          ,p_module            => 'AR');
   END;

      RETURN l_tax_cert;
   END GET_TAX_CERT;


   -------------------------------------------------------------------------
   --Function to get JOB START DATE
   -------------------------------------------------------------------------

   FUNCTION GET_JOB_START_DATE (p_cust_site_use_id IN NUMBER)
      RETURN DATE
   AS
      l_job_date   DATE;
   BEGIN
      BEGIN
         SELECT hps.creation_date
           INTO l_job_date
           FROM hz_cust_site_uses hcsu,
                hz_cust_acct_sites hcas,
                hz_cust_accounts hca,
                hz_parties hp,
                hz_party_sites hps
          WHERE     hcsu.cust_acct_site_id = hcas.cust_acct_site_id
                AND hcas.cust_account_id = hca.cust_account_id
                AND hp.party_id = hps.party_id
                AND hp.party_id = hca.party_id
                AND hcas.party_site_id = hps.party_site_id
                AND NVL (hcas.status, 'A') = 'A'
                AND NVL (hcsu.status, 'A') = 'A'
                AND NVL (hca.status, 'A') = 'A'
                AND hcsu.site_use_id = p_cust_site_use_id;
    EXCEPTION
    WHEN NO_DATA_FOUND THEN
     l_job_date   := 'NA' ; 
    WHEN OTHERS THEN
       xxcus_error_pkg.xxcus_error_main_api(p_called_from       => l_procedure_name
                                          ,p_calling           => 'Exception for function GET_JOB_START_DATE'
                                          ,p_request_id        => ' '
                                          ,p_ora_error_msg     => 'Error while getting job start date for site use id '||p_cust_site_use_id
                                          ,p_error_desc        => ' '
                                          ,p_distribution_list => l_distro_list
                                          ,p_module            => 'AR');
   END;

      RETURN l_job_date;
   END GET_JOB_START_DATE;

   -------------------------------------------------------------------------
   --Function to get JOINT CHECK AGREEMENT
   -------------------------------------------------------------------------

   FUNCTION GET_JOINT_CHECK_AGREEMENT (p_cust_site_use_id IN NUMBER)
      RETURN VARCHAR2
   AS
      l_joint_check   VARCHAR2 (30);
   BEGIN
      BEGIN
         SELECT hcas.attribute12
           INTO l_joint_check
           FROM hz_cust_site_uses hcsu,
                hz_cust_acct_sites hcas,
                hz_cust_accounts hca,
                hz_parties hp,
                hz_party_sites hps
          WHERE     hcsu.cust_acct_site_id = hcas.cust_acct_site_id
                AND hcas.cust_account_id = hca.cust_account_id
                AND hp.party_id = hps.party_id
                AND hp.party_id = hca.party_id
                AND hcas.party_site_id = hps.party_site_id
                AND NVL (hcas.status, 'A') = 'A'
                AND NVL (hcsu.status, 'A') = 'A'
                AND NVL (hca.status, 'A') = 'A'
                AND hcsu.site_use_id = p_cust_site_use_id;
    EXCEPTION
    WHEN NO_DATA_FOUND THEN
     l_joint_check   := 'NA' ; 
    WHEN OTHERS THEN
       xxcus_error_pkg.xxcus_error_main_api(p_called_from       => l_procedure_name
                                          ,p_calling           => 'Exception for function GET_JOINT_CHECK_AGREEMENT'
                                          ,p_request_id        => ' '
                                          ,p_ora_error_msg     => 'Error while getting joint check agreement for site use id '||p_cust_site_use_id
                                          ,p_error_desc        => ' '
                                          ,p_distribution_list => l_distro_list
                                          ,p_module            => 'AR');
   END;
      RETURN l_joint_check;
   END GET_JOINT_CHECK_AGREEMENT;


   -------------------------------------------------------------------------
   --Function to get OWNER FINANCED FLAG
   -------------------------------------------------------------------------

   FUNCTION GET_OWNER_FIN_FLAG (p_cust_site_use_id IN NUMBER)
      RETURN VARCHAR2
   AS
      l_owner_finance   VARCHAR2 (30);
   BEGIN
      BEGIN
         SELECT hcas.attribute13
           INTO l_owner_finance
           FROM hz_cust_site_uses hcsu,
                hz_cust_acct_sites hcas,
                hz_cust_accounts hca,
                hz_parties hp,
                hz_party_sites hps
          WHERE     hcsu.cust_acct_site_id = hcas.cust_acct_site_id
                AND hcas.cust_account_id = hca.cust_account_id
                AND hp.party_id = hps.party_id
                AND hp.party_id = hca.party_id
                AND hcas.party_site_id = hps.party_site_id
                AND NVL (hcas.status, 'A') = 'A'
                AND NVL (hcsu.status, 'A') = 'A'
                AND NVL (hca.status, 'A') = 'A'
                AND hcsu.site_use_id = p_cust_site_use_id;
    EXCEPTION
    WHEN NO_DATA_FOUND THEN
    l_owner_finance    := 'NA' ; 
    WHEN OTHERS THEN
       xxcus_error_pkg.xxcus_error_main_api(p_called_from       => l_procedure_name
                                          ,p_calling           => 'Exception for function GET_OWNER_FIN_FLAG'
                                          ,p_request_id        => ' '
                                          ,p_ora_error_msg     => 'Error while getting owner finance flag for site use id '||p_cust_site_use_id
                                          ,p_error_desc        => ' '
                                          ,p_distribution_list => l_distro_list
                                          ,p_module            => 'AR');
   END;

      RETURN l_owner_finance;
   END GET_OWNER_FIN_FLAG;


   -------------------------------------------------------------------------
   --Function to get CATEGORY
   -------------------------------------------------------------------------

   FUNCTION GET_CATEGORY (p_cust_site_use_id IN NUMBER)
      RETURN VARCHAR2
   AS
      l_get_category   VARCHAR2 (30);
   BEGIN
      BEGIN
         SELECT hcas.customer_category_code
           INTO l_get_category
           FROM hz_cust_site_uses hcsu,
                hz_cust_acct_sites hcas,
                hz_cust_accounts hca,
                hz_parties hp,
                hz_party_sites hps
          WHERE     hcsu.cust_acct_site_id = hcas.cust_acct_site_id
                AND hcas.cust_account_id = hca.cust_account_id
                AND hp.party_id = hps.party_id
                AND hp.party_id = hca.party_id
                AND hcas.party_site_id = hps.party_site_id
                AND NVL (hcas.status, 'A') = 'A'
                AND NVL (hcsu.status, 'A') = 'A'
                AND NVL (hca.status, 'A') = 'A'
                AND hcsu.site_use_id = p_cust_site_use_id;
    EXCEPTION
    WHEN NO_DATA_FOUND THEN
    l_get_category    := 'NA' ; 
    WHEN OTHERS THEN
       xxcus_error_pkg.xxcus_error_main_api(p_called_from       => l_procedure_name
                                          ,p_calling           => 'Exception for function GET_CATEGORY '
                                          ,p_request_id        => ' '
                                          ,p_ora_error_msg     => 'Error while getting category for site use id '||p_cust_site_use_id
                                          ,p_error_desc        => ' '
                                          ,p_distribution_list => l_distro_list
                                          ,p_module            => 'AR');
   END;

      RETURN l_get_category;
   END GET_CATEGORY;

   -------------------------------------------------------------------------
   --Function to get DSO
   -------------------------------------------------------------------------

   FUNCTION GET_DSO_ACCT (p_cust_account_id IN NUMBER)
      RETURN NUMBER
   AS
      l_ttm_amount   NUMBER;
      l_due_amount   NUMBER;
      l_dso          NUMBER := 0;
   BEGIN

 BEGIN
      SELECT NVL (SUM (amount_due_original), 0)
        INTO l_ttm_amount
        FROM ar_payment_schedules
       WHERE     customer_id = p_cust_account_id
             AND TRUNC (SYSDATE - trx_date) <= 90
             AND class <> 'PMT';
EXCEPTION
    WHEN NO_DATA_FOUND THEN
    l_ttm_amount    := 0 ; 
    WHEN OTHERS THEN
       xxcus_error_pkg.xxcus_error_main_api(p_called_from       => l_procedure_name
                                          ,p_calling           => 'Exception for function GET_DSO_ACCT'
                                          ,p_request_id        => ' '
                                          ,p_ora_error_msg     => 'Error while getting total transaction amount for cust account id '||p_cust_account_id
                                          ,p_error_desc        => ' '
                                          ,p_distribution_list => l_distro_list
                                          ,p_module            => 'AR');
   END;

BEGIN
      SELECT NVL (SUM (amount_due_remaining), 0)
        INTO l_due_amount
        FROM ar_payment_schedules
       WHERE customer_id = p_cust_account_id;
EXCEPTION
    WHEN NO_DATA_FOUND THEN
    l_due_amount   := 0 ; 
    WHEN OTHERS THEN
       xxcus_error_pkg.xxcus_error_main_api(p_called_from       => l_procedure_name
                                          ,p_calling           => 'Exception for function GET_DSO_ACCT'
                                          ,p_request_id        => ' '
                                          ,p_ora_error_msg     => 'Error while getting due amount for cust account id '||p_cust_account_id
                                          ,p_error_desc        => ' '
                                          ,p_distribution_list => l_distro_list
                                          ,p_module            => 'AR');
   END;

      IF l_ttm_amount <> 0 AND l_due_amount <> 0
      THEN
         l_dso := ROUND (l_due_amount / (l_ttm_amount / 90), 1);
      END IF;

      RETURN (l_dso);
   EXCEPTION
      WHEN OTHERS
      THEN
          xxcus_error_pkg.xxcus_error_main_api(p_called_from   => l_procedure_name
                                          ,p_calling           => 'Exception for function GET_DSO_ACCT'
                                          ,p_request_id        => ' '
                                          ,p_ora_error_msg     => 'Error while rturning DSO for cust account id '||p_cust_account_id
                                          ,p_error_desc        => ' '
                                          ,p_distribution_list => l_distro_list
                                          ,p_module            => 'AR');
   END GET_DSO_ACCT;

   -------------------------------------------------------------------------
   --Function to get Account Manager
   -------------------------------------------------------------------------

   FUNCTION GET_ACC_MGR (p_cust_account_id IN NUMBER)
      RETURN VARCHAR2
   AS
      l_acc_mgr   VARCHAR2 (100);
   BEGIN
      BEGIN
          SELECT j.resource_name
           INTO l_acc_mgr
           FROM  hz_cust_accounts hca,
                 hz_cust_acct_sites hcasa,
                 hz_cust_site_uses hcsu,
                jtf_rs_defresources_v j,
                jtf_rs_salesreps s                
          WHERE 1 = 1
            AND hca.cust_account_id = hcasa.cust_account_id
            AND hcasa.cust_acct_site_id = hcsu.cust_acct_site_id     
            AND j.resource_id = s.resource_id
            AND s.salesrep_id = hcsu.primary_salesrep_id
            AND hcsu.site_use_code = 'BILL_TO'
            AND hcsu.primary_flag = 'Y'
            AND hca.cust_account_id = p_cust_account_id;
    EXCEPTION
    WHEN NO_DATA_FOUND THEN
    l_acc_mgr   := 'NA' ; 
    WHEN OTHERS THEN
       xxcus_error_pkg.xxcus_error_main_api(p_called_from       => l_procedure_name
                                          ,p_calling           => 'Exception for function GET_ACC_MGR'
                                          ,p_request_id        => ' '
                                          ,p_ora_error_msg     => 'Error while getting account manager for cust account id '||p_cust_account_id
                                          ,p_error_desc        => ' '
                                          ,p_distribution_list => l_distro_list
                                          ,p_module            => 'AR');
   END;

      RETURN l_acc_mgr;
   END GET_ACC_MGR;

   -------------------------------------------------------------------------
   --Function to get CONSOLIDATED GROSS MARGIN LAST 12 ROLLING MONTHS
   -------------------------------------------------------------------------
   FUNCTION GET_GROSS_MARGIN (p_cust_account_id IN NUMBER)
      RETURN NUMBER
   AS
      l_gross_margin   NUMBER;
      l_gross_sales    NUMBER;
      l_gross_cost     NUMBER;
   BEGIN

   --Get sales
    BEGIN
      SELECT   SUM (aps.amount_line_items_original) 
        INTO l_gross_sales
        FROM ar_payment_schedules aps
        WHERE aps.class NOT LIKE 'PMT'
          AND TRUNC (aps.gl_date) BETWEEN TRUNC(LAST_DAY (ADD_MONTHS (SYSDATE, -1)))-365
        AND TRUNC (LAST_DAY (ADD_MONTHS (SYSDATE, -1)))
          AND aps.customer_id = p_cust_account_id;        --7103;
    EXCEPTION
    WHEN NO_DATA_FOUND THEN
    l_gross_sales    := 0 ; 
    WHEN OTHERS THEN
       xxcus_error_pkg.xxcus_error_main_api(p_called_from       => l_procedure_name
                                          ,p_calling           => 'Exception for function GET_GROSS_MARGIN '
                                          ,p_request_id        => ' '
                                          ,p_ora_error_msg     => 'Error while getting gross sales for cust account id '||p_cust_account_id
                                          ,p_error_desc        => ' '
                                          ,p_distribution_list => l_distro_list
                                          ,p_module            => 'AR');
   END;
      

  --Get cost

    BEGIN
          SELECT  SUM(ool.invoiced_quantity*ool.unit_cost) 
               INTO l_gross_cost
        FROM ar_payment_schedules aps,
             ra_customer_trx_lines rctla,
             oe_order_headers ooh,
             oe_order_lines ool    
        WHERE 1=1
          AND ooh.header_id = ool.header_id
          AND rctla.interface_line_attribute6 = to_char(ool.line_id)
          AND rctla.interface_line_attribute1 = to_char(ooh.order_number)
          AND aps.customer_trx_id = rctla.customer_trx_id 
          AND aps.class NOT LIKE 'PMT'
          AND TRUNC (aps.gl_date) BETWEEN TRUNC(LAST_DAY (ADD_MONTHS (SYSDATE, -1)))-365
        AND TRUNC (LAST_DAY (ADD_MONTHS (SYSDATE, -1)))
          AND aps.customer_id = p_cust_account_id;        --7103;
    EXCEPTION
    WHEN NO_DATA_FOUND THEN
    l_gross_cost    := 0 ; 
    WHEN OTHERS THEN
       xxcus_error_pkg.xxcus_error_main_api(p_called_from       => l_procedure_name
                                          ,p_calling           => 'Exception for function GET_GROSS_MARGIN '
                                          ,p_request_id        => ' '
                                          ,p_ora_error_msg     => 'Error while getting gross cost for cust account id '||p_cust_account_id
                                          ,p_error_desc        => ' '
                                          ,p_distribution_list => l_distro_list
                                          ,p_module            => 'AR');
   END;

      --GET GROSS MARGIN

       SELECT ROUND((DECODE(SIGN(l_gross_sales-l_gross_cost),-1,(l_gross_sales-l_gross_cost)*-1,(l_gross_sales-l_gross_cost))/l_gross_sales)*100,2)
    INTO l_gross_margin
       FROM DUAL;

      RETURN l_gross_margin;
   END GET_GROSS_MARGIN;

   -------------------------------------------------------------------------
   --Function to get CONSOLIDATED GROSS MARGIN LAST FISCAL MONTH
   -------------------------------------------------------------------------
   FUNCTION GET_GROSS_FISCAL (p_cust_account_id IN NUMBER)
      RETURN NUMBER
   AS
      l_start_date      DATE;
      l_fiscal_start    DATE;
      l_fiscal_end      DATE;
      l_fiscal_margin   NUMBER;
      l_fiscal_sales    NUMBER;
      l_fiscal_cost     NUMBER;
   BEGIN
      BEGIN
         SELECT start_date - 5
           INTO l_start_date
           FROM GL_PERIODS
          WHERE SYSDATE BETWEEN start_date AND end_date;
     EXCEPTION
     WHEN NO_DATA_FOUND THEN
     l_start_date    := 'NA' ; 
    WHEN OTHERS THEN
       xxcus_error_pkg.xxcus_error_main_api(p_called_from       => l_procedure_name
                                          ,p_calling           => 'Exception for function GET_GROSS_FISCAL'
                                          ,p_request_id        => ' '
                                          ,p_ora_error_msg     => 'Error while getting GL perriod start date for cust account id '||p_cust_account_id
                                          ,p_error_desc        => ' '
                                          ,p_distribution_list => l_distro_list
                                          ,p_module            => 'AR');
   END;

      BEGIN
         SELECT start_date, end_date
           INTO l_fiscal_start, l_fiscal_end
           FROM GL_PERIODS
          WHERE l_start_date BETWEEN start_date AND end_date;
     EXCEPTION
     WHEN NO_DATA_FOUND THEN
     l_fiscal_start    := 'NA' ; 
     l_fiscal_end      := 'NA' ;
     WHEN OTHERS THEN
       xxcus_error_pkg.xxcus_error_main_api(p_called_from       => l_procedure_name
                                          ,p_calling           => 'Exception for function GET_GROSS_FISCAL'
                                          ,p_request_id        => ' '
                                          ,p_ora_error_msg     => 'Error while getting fiscal start date and end date for cust account id '||p_cust_account_id
                                          ,p_error_desc        => ' '
                                          ,p_distribution_list => l_distro_list
                                          ,p_module            => 'AR');
   END;

--get fiscal sales

 BEGIN

        SELECT   SUM (aps.amount_line_items_original) 
               INTO l_fiscal_sales
        FROM ar_payment_schedules aps
        WHERE 1=1
          AND aps.class NOT LIKE 'PMT'
          AND TRUNC (aps.gl_date) BETWEEN l_fiscal_start AND l_fiscal_end 
          AND aps.customer_id = p_cust_account_id; 
     EXCEPTION
     WHEN NO_DATA_FOUND THEN
     l_fiscal_sales   := 0 ; 
     WHEN OTHERS THEN
       xxcus_error_pkg.xxcus_error_main_api(p_called_from       => l_procedure_name
                                          ,p_calling           => 'Exception for function GET_GROSS_FISCAL'
                                          ,p_request_id        => ' '
                                          ,p_ora_error_msg     => 'Error while getting fiscal sales for cust account id '||p_cust_account_id
                                          ,p_error_desc        => ' '
                                          ,p_distribution_list => l_distro_list
                                          ,p_module            => 'AR');
   END;


--get fiscal cost

 BEGIN

        SELECT  SUM(ool.invoiced_quantity*ool.unit_cost) 
               INTO l_fiscal_cost
        FROM ar_payment_schedules aps,
             ra_customer_trx_lines rctla,
             oe_order_headers ooh,
             oe_order_lines ool    
        WHERE 1=1
          AND ooh.header_id = ool.header_id
          AND rctla.interface_line_attribute6 = to_char(ool.line_id)
          AND rctla.interface_line_attribute1 = to_char(ooh.order_number)
          AND aps.customer_trx_id = rctla.customer_trx_id 
          AND aps.class NOT LIKE 'PMT'
          AND TRUNC (aps.gl_date) BETWEEN l_fiscal_start AND l_fiscal_end 
          AND aps.customer_id = p_cust_account_id; 
     EXCEPTION
     WHEN NO_DATA_FOUND THEN
     l_fiscal_cost   := 0 ; 
     WHEN OTHERS THEN
       xxcus_error_pkg.xxcus_error_main_api(p_called_from       => l_procedure_name
                                          ,p_calling           => 'Exception for function GET_GROSS_FISCAL'
                                          ,p_request_id        => ' '
                                          ,p_ora_error_msg     => 'Error while getting fiscal cost for cust account id '||p_cust_account_id
                                          ,p_error_desc        => ' '
                                          ,p_distribution_list => l_distro_list
                                          ,p_module            => 'AR');
   END;

  --GET FISCAL MARGIN

       SELECT ROUND((DECODE(SIGN(l_fiscal_sales-l_fiscal_cost),-1,(l_fiscal_sales-l_fiscal_cost)*-1,(l_fiscal_sales-l_fiscal_cost))/l_fiscal_sales)*100,2)
    INTO l_fiscal_margin
       FROM DUAL;
    

      RETURN l_fiscal_margin;
   END GET_GROSS_FISCAL;

   ------------------------------------------------------------------------------
   --Function to get CONSOLIDATED GROSS MARGIN LAST 12 ROLLING MONTHS, site level
   ------------------------------------------------------------------------------
   FUNCTION GET_SITE_GROSS_MARGIN (p_cust_site_use_id IN NUMBER)
      RETURN NUMBER
   AS
      l_gross_margin   NUMBER;
      l_gross_sales    NUMBER;
      l_gross_cost     NUMBER;
   BEGIN

     BEGIN
        SELECT   SUM (aps.amount_line_items_original) 
            INTO l_gross_sales
        FROM ar_payment_schedules aps
        WHERE 1=1
          AND aps.class NOT LIKE 'PMT'
          AND TRUNC (aps.gl_date) BETWEEN TRUNC(LAST_DAY (ADD_MONTHS (SYSDATE, -1)))-365
          AND TRUNC (LAST_DAY (ADD_MONTHS (SYSDATE, -1)))
          AND aps.customer_site_use_id = p_cust_site_use_id;        --7103;
      EXCEPTION
     WHEN NO_DATA_FOUND THEN
     l_gross_sales   := 0 ; 
     WHEN OTHERS THEN
       xxcus_error_pkg.xxcus_error_main_api(p_called_from       => l_procedure_name
                                          ,p_calling           => 'Exception for function GET_SITE_GROSS_MARGIN'
                                          ,p_request_id        => ' '
                                          ,p_ora_error_msg     => 'Error while getting gross sales for cust site use id '||p_cust_site_use_id
                                          ,p_error_desc        => ' '
                                          ,p_distribution_list => l_distro_list
                                          ,p_module            => 'AR');
   END;


       BEGIN
        SELECT SUM(ool.invoiced_quantity*ool.unit_cost) 
               INTO l_gross_cost
        FROM ar_payment_schedules aps,
             ra_customer_trx_lines rctla,
             oe_order_headers ooh,
             oe_order_lines ool    
        WHERE 1=1
          AND ooh.header_id = ool.header_id
          AND rctla.interface_line_attribute6 = to_char(ool.line_id)
          AND rctla.interface_line_attribute1 = to_char(ooh.order_number)
          AND aps.customer_trx_id = rctla.customer_trx_id 
          AND aps.class NOT LIKE 'PMT'
          AND TRUNC (aps.gl_date) BETWEEN TRUNC(LAST_DAY (ADD_MONTHS (SYSDATE, -1)))-365
        AND TRUNC (LAST_DAY (ADD_MONTHS (SYSDATE, -1)))
          AND aps.customer_site_use_id = p_cust_site_use_id;        --7103;
      EXCEPTION
     WHEN NO_DATA_FOUND THEN
     l_gross_cost  := 0 ; 
     WHEN OTHERS THEN
       xxcus_error_pkg.xxcus_error_main_api(p_called_from       => l_procedure_name
                                          ,p_calling           => 'Exception for function GET_SITE_GROSS_MARGIN'
                                          ,p_request_id        => ' '
                                          ,p_ora_error_msg     => 'Error while getting gross cost for cust site use id '||p_cust_site_use_id
                                          ,p_error_desc        => ' '
                                          ,p_distribution_list => l_distro_list
                                          ,p_module            => 'AR');
   END;


 --GET GROSS MARGIN

       SELECT ROUND((DECODE(SIGN(l_gross_sales-l_gross_cost),-1,(l_gross_sales-l_gross_cost)*-1,(l_gross_sales-l_gross_cost))/l_gross_sales)*100,2)
    INTO l_gross_margin
       FROM DUAL;

      RETURN l_gross_margin;
   END GET_SITE_GROSS_MARGIN;

   ------------------------------------------------------------------------------
   --Function to get CONSOLIDATED GROSS MARGIN LAST FISCAL MONTH, SITE LEVEL
   ------------------------------------------------------------------------------
   FUNCTION GET_SITE_GROSS_FISCAL (p_cust_site_use_id IN NUMBER)
      RETURN NUMBER
   AS
      l_start_date      DATE;
      l_fiscal_start    DATE;
      l_fiscal_end      DATE;
      l_fiscal_margin   NUMBER;
      l_fiscal_sales    NUMBER;
      l_fiscal_cost     NUMBER;
   BEGIN
      BEGIN
         SELECT start_date - 5
           INTO l_start_date
           FROM GL_PERIODS
          WHERE SYSDATE BETWEEN start_date AND end_date;
     EXCEPTION
     WHEN NO_DATA_FOUND THEN
     l_start_date := 'NA' ; 
     WHEN OTHERS THEN
       xxcus_error_pkg.xxcus_error_main_api(p_called_from       => l_procedure_name
                                          ,p_calling           => 'Exception for function GET_SITE_GROSS_FISCAL'
                                          ,p_request_id        => ' '
                                          ,p_ora_error_msg     => 'Error while getting period start date for cust site use id '||p_cust_site_use_id
                                          ,p_error_desc        => ' '
                                          ,p_distribution_list => l_distro_list
                                          ,p_module            => 'AR');
     END;

      BEGIN
         SELECT start_date, end_date
           INTO l_fiscal_start, l_fiscal_end
           FROM GL_PERIODS
          WHERE l_start_date BETWEEN start_date AND end_date;
     EXCEPTION
     WHEN NO_DATA_FOUND THEN
     l_fiscal_start := 'NA' ; 
     l_fiscal_end   := 'NA' ;
     WHEN OTHERS THEN
       xxcus_error_pkg.xxcus_error_main_api(p_called_from       => l_procedure_name
                                          ,p_calling           => 'Exception for function GET_SITE_GROSS_FISCAL'
                                          ,p_request_id        => ' '
                                          ,p_ora_error_msg     => 'Error while getting fiscal dates for cust site use id '||p_cust_site_use_id
                                          ,p_error_desc        => ' '
                                          ,p_distribution_list => l_distro_list
                                          ,p_module            => 'AR');
     END;

    BEGIN
      SELECT  SUM (aps.amount_line_items_original) 
               INTO l_fiscal_sales
        FROM ar_payment_schedules aps
        WHERE 1=1
          AND aps.class NOT LIKE 'PMT'
          AND TRUNC (aps.gl_date) BETWEEN l_fiscal_start AND l_fiscal_end 
          AND aps.customer_site_use_id = p_cust_site_use_id; 

     EXCEPTION
     WHEN NO_DATA_FOUND THEN
     l_fiscal_sales := 0 ; 
     WHEN OTHERS THEN
       xxcus_error_pkg.xxcus_error_main_api(p_called_from       => l_procedure_name
                                          ,p_calling           => 'Exception for function GET_SITE_GROSS_FISCAL'
                                          ,p_request_id        => ' '
                                          ,p_ora_error_msg     => 'Error while getting fiscal sale for cust site use id '||p_cust_site_use_id
                                          ,p_error_desc        => ' '
                                          ,p_distribution_list => l_distro_list
                                          ,p_module            => 'AR');
     END;


 BEGIN
      SELECT   SUM(ool.invoiced_quantity*ool.unit_cost) 
               INTO l_fiscal_cost
        FROM ar_payment_schedules aps,
             ra_customer_trx_lines rctla,
             oe_order_headers ooh,
             oe_order_lines ool    
        WHERE 1=1
          AND ooh.header_id = ool.header_id
          AND rctla.interface_line_attribute6 = to_char(ool.line_id)
          AND rctla.interface_line_attribute1 = to_char(ooh.order_number)
          AND aps.customer_trx_id = rctla.customer_trx_id 
          AND aps.class NOT LIKE 'PMT'
          AND TRUNC (aps.gl_date) BETWEEN l_fiscal_start AND l_fiscal_end 
          AND aps.customer_site_use_id = p_cust_site_use_id; 

     EXCEPTION
     WHEN NO_DATA_FOUND THEN
     l_fiscal_cost := 0 ; 
     WHEN OTHERS THEN
       xxcus_error_pkg.xxcus_error_main_api(p_called_from       => l_procedure_name
                                          ,p_calling           => 'Exception for function GET_SITE_GROSS_FISCAL'
                                          ,p_request_id        => ' '
                                          ,p_ora_error_msg     => 'Error while getting fiscal cost for cust site use id '||p_cust_site_use_id
                                          ,p_error_desc        => ' '
                                          ,p_distribution_list => l_distro_list
                                          ,p_module            => 'AR');
   END;

--GET FISCAL MARGIN

       SELECT ROUND((DECODE(SIGN(l_fiscal_sales-l_fiscal_cost),-1,(l_fiscal_sales-l_fiscal_cost)*-1,(l_fiscal_sales-l_fiscal_cost))/l_fiscal_sales)*100,2)
    INTO l_fiscal_margin
       FROM DUAL;

      RETURN l_fiscal_margin;

   END GET_SITE_GROSS_FISCAL;


 ------------------------------------------------------------------------------
   --Function to get auto apply credit flag     --Added function for ver# 1.1 
   ------------------------------------------------------------------------------
   FUNCTION GET_AUTO_APPLY_FLAG(p_cust_account_id IN NUMBER)         
   RETURN VARCHAR2
   AS   
  l_auto_apply_cr   VARCHAR2 (3);

   BEGIN

      BEGIN
          SELECT DECODE(attribute11,'Y','ON','N','OFF')
           INTO l_auto_apply_cr
           FROM  hz_cust_accounts                 
          WHERE  cust_account_id = p_cust_account_id;
     
    RETURN l_auto_apply_cr;
--Error emailing is not required for this functionality
    EXCEPTION
    WHEN NO_DATA_FOUND THEN
    l_auto_apply_cr   := 'NA' ; 
    RETURN l_auto_apply_cr;
    WHEN OTHERS THEN
       l_auto_apply_cr   := 'NA' ; 
        RETURN l_auto_apply_cr;
   END;

  END;

 -------------------------------------------------------------------------
   -- --Function to get auto apply credit flag to show at site  --Added function for ver# 1.1
   -------------------------------------------------------------------------

   FUNCTION  GET_AUTOAPPLY_FLAG_SITE (p_cust_site_use_id IN NUMBER)
      RETURN VARCHAR2
   AS
        l_autoapply_site   VARCHAR2 (3);
   BEGIN

      BEGIN
         SELECT DECODE(hca.attribute11,'Y','ON','N','OFF')
           INTO l_autoapply_site
           FROM hz_cust_site_uses hcsu,
                hz_cust_acct_sites hcas,
                hz_cust_accounts hca,
                hz_parties hp,
                hz_party_sites hps
          WHERE     hcsu.cust_acct_site_id = hcas.cust_acct_site_id
                AND hcas.cust_account_id = hca.cust_account_id
                AND hp.party_id = hps.party_id
                AND hp.party_id = hca.party_id
                AND hcas.party_site_id = hps.party_site_id
                AND NVL (hcas.status, 'A') = 'A'
                AND NVL (hcsu.status, 'A') = 'A'
                AND NVL (hca.status, 'A') = 'A'
                AND hcsu.site_use_id = p_cust_site_use_id;

RETURN l_autoapply_site;
--Error emailing is not required for this functionality
    EXCEPTION
    WHEN NO_DATA_FOUND THEN
    l_autoapply_site   := 'NA' ; 
    RETURN l_autoapply_site;
    WHEN OTHERS THEN
    l_autoapply_site   := 'NA' ; 
    RETURN l_autoapply_site;
   END;
      
   END GET_AUTOAPPLY_FLAG_SITE;


END;
/