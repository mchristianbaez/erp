/* Formatted on 8/15/2013 9:36:48 PM (QP5 v5.206) */
-- Start of DDL Script for Package Body XXEIS.XXWC_EIS_CUSTOMER_BALANCE_V2
-- Generated 8/14/2013 2:49:16 PM from XXEIS@EBIZFQA
--Commented parallelism on 12/28/2014 by Maha for performance issue ESMS# 547734

CREATE OR REPLACE PACKAGE BODY XXEIS.xxwc_eis_customer_balance_v2
IS
/******************************************************************************
   NAME     :      xxwc_eis_customer_balance_v2
   PURPOSE  :      

   REVISIONS:
   Ver        Date           Author             Description
   ---------  ----------     ---------------     ------------------------------------
   1.0        04/28/2015     Raghavendra S       TMS# 20141202-00120 -AR - Remove special characters from either the Aging Report or from the MV
   2.0        09/02/2015     Maharajan Shunmugam TMS# 20150123-00197 AR - Customer Flag as indicator to auto apply credits
  ******************************************************************************/
    l_error_message   CLOB;
    l_errbuf          CLOB;
    g_start           NUMBER;

    pl_dflt_email     fnd_user.email_address%TYPE := 'HDSOracleDevelopers@hdsupply.com';

    --elapsed_time ('', g_start, g_start)
    --*************************************************************
    --*************************************************************
    --*************************************************************
    PROCEDURE elapsed_time (p_called_from VARCHAR2, p_start IN OUT NUMBER)
    IS
        v_sec_elapsed   NUMBER;
        gr_start        NUMBER := DBMS_UTILITY.get_time;
    BEGIN
        g_start := NVL (p_start, gr_start);
        v_sec_elapsed := gr_start - g_start;
        write_log (p_called_from || '  run for ' || v_sec_elapsed / 100 || ' sec');
        p_start := gr_start;
    END;

    --*************************************************************
    --*************************************************************
    FUNCTION xxwc_add_tuning_parameter (p_table_name VARCHAR2, p_tuning_factor NUMBER)
        RETURN NUMBER
    IS
        v_string          CLOB;
        v_tuning_factor   NUMBER;
    BEGIN
        IF p_tuning_factor IS NULL
        THEN
            v_tuning_factor := 70;
        ELSE
            v_tuning_factor := p_tuning_factor;
        END IF;

        v_string := 'ALTER  TABLE ' || p_table_name || ' ADD(group_number NUMBER,line_number_r NUMBER)';

        EXECUTE IMMEDIATE v_string;

        v_string :=
               'DECLARE
    n_loop   NUMBER := 0;

BEGIN
    FOR r IN (SELECT ROWID v_rowid FROM '
            || p_table_name
            || ' )
    LOOP
        n_loop := n_loop + 1;

        UPDATE  '
            || p_table_name
            || '
           SET line_number_r = n_loop
         WHERE ROWID = r.v_rowid;
    END LOOP;

    COMMIT;
END;';

        EXECUTE IMMEDIATE v_string;

        v_string :=
               'DECLARE
    v_count          NUMBER := 0;
    v_count6         NUMBER := 0;
    v_group_number   NUMBER := 1;

BEGIN
    SELECT MAX (line_number_r) INTO v_count FROM  '
            || p_table_name
            || ' ;

    v_count6 := ROUND (v_count / :tuning_factor);

    FOR r IN (SELECT ROWID v_rowid, line_number_r FROM  '
            || p_table_name
            || ' )
    LOOP
        UPDATE  '
            || p_table_name
            || '
           SET group_number = v_group_number
         WHERE ROWID = r.v_rowid;

        IF MOD (r.line_number_r, v_count6) = 0
        THEN
            v_group_number := v_group_number + 1;
        END IF;
    END LOOP;

    COMMIT;
END;';

        EXECUTE IMMEDIATE v_string USING v_tuning_factor;

        v_tuning_factor := v_tuning_factor + 1;

        RETURN v_tuning_factor;
    END;

    --*************************************************************
    --*************************************************************
    --*************************************************************
    --*************************************************************
    --*************************************************************
    PROCEDURE write_log (p_text IN VARCHAR2)
    IS
    BEGIN
        fnd_file.put_line (fnd_file.LOG, p_text);

        DBMS_OUTPUT.put_line (p_text);
    END;

    --*************************************************************
    --*************************************************************
    --*************************************************************
    --*************************************************************
    --*************************************************************
    PROCEDURE table_create_from_old (p_schema VARCHAR2, p_old_table_name VARCHAR2, p_new_table_name VARCHAR2)
    IS
    BEGIN
        DECLARE
            vr   NUMBER := 0;
        BEGIN
            xxwc_eis_customer_balance_v2.drop_temp_table (p_schema, p_new_table_name);
            vr := xxwc_eis_customer_balance_v2.create_copy_table (p_schema, p_old_table_name, p_new_table_name);
            -- FUNCTION create_copy_table (p_owner VARCHAR2, p_table_name VARCHAR2, p_new_table_name VARCHAR2)
            --xxwc_eis_customer_balance_v2.alter_table_temp (p_schema, p_new_table_name);                         commented parallelism by Maha on 12/28/14
        END;
    END;

    --*************************************************************
    --*************************************************************
    --*************************************************************
    --*************************************************************
    --*************************************************************

    FUNCTION create_copy_table (p_owner VARCHAR2, p_table_name VARCHAR2, p_new_table_name VARCHAR2)
        RETURN NUMBER
    IS
        v_ind   NUMBER := 0;
    BEGIN
        xxwc_eis_customer_balance_v2.drop_temp_table (p_owner, p_new_table_name);

        FOR rt IN (SELECT table_name
                     FROM all_tables
                    WHERE table_name = UPPER (p_table_name) AND owner = UPPER (p_owner))
        LOOP
            EXECUTE IMMEDIATE
                   ' CREATE TABLE '
                || p_owner
                || '.'
                || p_new_table_name
                || ' AS SELECT * FROM '
                || p_owner
                || '.'
                || p_table_name
                || ' WHERE 1=2';

           -- alter_table_temp (p_owner, p_new_table_name); commented parallelism by Maha on 12/28/14
            v_ind := 1;
        END LOOP;

        RETURN v_ind;
    END;

    --*************************************************************
    --*************************************************************
    --*************************************************************
    --*************************************************************
    --*************************************************************
    PROCEDURE drop_temp_table (p_owner VARCHAR2, p_table_name VARCHAR2)
    IS
        v_ddl_string   VARCHAR2 (1024);
    BEGIN
        FOR r
            IN (SELECT object_name
                  FROM all_objects
                 WHERE     object_name = UPPER (TRIM (p_table_name))
                       AND object_type = 'TABLE'
                       AND owner = UPPER (TRIM (p_owner)))
        LOOP
            v_ddl_string := 'drop table ' || p_owner || '.' || p_table_name;

            EXECUTE IMMEDIATE v_ddl_string;
        END LOOP;
    EXCEPTION
        WHEN OTHERS
        THEN
            NULL;
    END;

    --*************************************************************
    --*************************************************************
    --*************************************************************
    --*************************************************************
    --*************************************************************
    --*************************************************************
    --      Make the table perform better for this extract
    PROCEDURE alter_table_temp (p_owner VARCHAR2, p_table_name VARCHAR2)
    IS
        v_execute_string   CLOB;
        v_tablespace       VARCHAR2 (124);
    BEGIN
        v_tablespace := 'XXEIS';

        FOR r
            IN (SELECT object_name
                  FROM all_objects
                 WHERE     object_name = UPPER (TRIM (p_table_name))
                       AND object_type = 'TABLE'
                       AND owner = UPPER (TRIM (p_owner)))
        LOOP
            v_execute_string := 'ALTER TABLE ' || p_owner || '.' || p_table_name || ' NOLOGGING';

            EXECUTE IMMEDIATE v_execute_string;

            -- Increases parallelism on this table, helps with RAC environment.
            -- May change this to a forumla based on the environment
            v_execute_string := 'ALTER TABLE ' || p_owner || '.' || p_table_name || ' parallel 32';

            EXECUTE IMMEDIATE v_execute_string;
        END LOOP;
    EXCEPTION
        WHEN OTHERS
        THEN
            l_errbuf :=
                   ' Error_Stack...'
                || CHR (10)
                || DBMS_UTILITY.format_error_stack ()
                || CHR (10)
                || ' Error_Backtrace...'
                || CHR (10)
                || DBMS_UTILITY.format_error_backtrace ();

            write_log ('error executing ' || v_execute_string || ' ' || l_errbuf);
    END;

    --*************************************************************
    --*************************************************************
    --*************************************************************
    --*************************************************************

    -------
    PROCEDURE main (p_errbuf OUT VARCHAR2, p_retcode OUT NUMBER, p_tuning_factor NUMBER)
    IS
  /******************************************************************************
   NAME     :      main
   PURPOSE  :      main

   REVISIONS:
   Ver        Date           Author             Description
   ---------  ----------     ---------------     ------------------------------------
   1.0        28/04/2015     Raghavendra S       TMS# 20141202-00120 -AR - Remove special characters from either the Aging Report or from the MV
   2.0        09/02/2015     Maharajan Shunmugam TMS# 20150123-00197 AR - Customer Flag as indicator to auto apply credits
  ******************************************************************************/
        --select * from XXEIS.XXWC_AR_CUST##AR_PAYMENT_SCH_X   where customer_trx_id=3165623
        v_tuning_factor   NUMBER;
        l_start           NUMBER;
    BEGIN
        p_retcode := 0;

        BEGIN
            xxwc_eis_customer_balance_v2.drop_temp_table ('XXEIS', 'XXWC_AR_CUST##AR_PAYMENT_SCH_X');

            -- Create temp table structure.
            EXECUTE IMMEDIATE 'CREATE TABLE XXEIS.XXWC_AR_CUST##AR_PAYMENT_SCH_X
AS
    SELECT apsa.payment_schedule_id
          ,apsa.customer_id
          ,apsa.customer_site_use_id
          ,apsa.customer_trx_id
          ,apsa.cash_receipt_id
          ,apsa.discount_original
          ,apsa.amount_due_original
          ,apsa.org_id
          ,apsa.amount_due_remaining
          ,apsa.class
          ,apsa.due_date
          ,apsa.trx_number
          ,apsa.trx_date
      FROM ar_payment_schedules_all apsa
     WHERE   1 = 2';

            -- Settings to
            --xxwc_eis_customer_balance_v2.alter_table_temp ('XXEIS', 'XXWC_AR_CUST##AR_PAYMENT_SCH_X');  commented parallelism by Maha on 12/28/14

            -- Populate table, the APPEND hint uses parallelism
            EXECUTE IMMEDIATE
                'INSERT /*+APPEND*/
          INTO  XXEIS.XXWC_AR_CUST##AR_PAYMENT_SCH_X
        SELECT apsa.payment_schedule_id
          ,apsa.customer_id
          ,apsa.customer_site_use_id
          ,apsa.customer_trx_id
          ,apsa.cash_receipt_id
          ,apsa.discount_original
          ,apsa.amount_due_original
          ,apsa.org_id
          ,apsa.amount_due_remaining
          ,apsa.class
          ,apsa.due_date
          ,apsa.trx_number
          ,apsa.trx_date
      FROM ar_payment_schedules_all apsa
     WHERE apsa.amount_due_remaining <> 0 AND apsa.customer_site_use_id IS NOT NULL
     AND apsa.org_id =(SELECT organization_id FROM apps.hr_operating_units hou WHERE name=''HDS White Cap - Org'')';

            COMMIT; -- Important to commit after using APPEND in a NO LOGGING table, won't be available to this session until this commit
        END;

        elapsed_time ('Create temp table structure', g_start);

        IF p_tuning_factor IS NULL
        THEN
            v_tuning_factor := 70;
        ELSE
            v_tuning_factor := p_tuning_factor;
        END IF;

        --create groups for long updates
        v_tuning_factor :=
            xxwc_eis_customer_balance_v2.xxwc_add_tuning_parameter ('XXEIS.XXWC_AR_CUST##AR_PAYMENT_SCH_X'
                                                                   ,v_tuning_factor);

        elapsed_time ('xxwc_add_tuning_parameter', g_start);

        --******************************************
        --******************************************
        --populate po_number
        BEGIN
            xxwc_eis_customer_balance_v2.table_create_from_old ('XXEIS'
                                                               ,'XXWC_AR_CUST##AR_PAYMENT_SCH_X'
                                                               ,'XXWC_AR_CUST##AR_PAYMENT_SCH_Z');

            EXECUTE IMMEDIATE 'alter table  XXEIS.XXWC_AR_CUST##AR_PAYMENT_SCH_Z add (purchase_order varchar2(250))';

            EXECUTE IMMEDIATE 'INSERT /*+APPEND*/
          INTO  XXEIS.XXWC_AR_CUST##AR_PAYMENT_SCH_Z
        SELECT a.*,b.purchase_order
      FROM XXEIS.XXWC_AR_CUST##AR_PAYMENT_SCH_X a,ar.ra_customer_trx_all b
     WHERE    a.customer_trx_id = b.customer_trx_id(+)';

            COMMIT;
        END;

        elapsed_time ('populate po_number', g_start);

        --******************************************
        --populate cash receipts information
        --******************************************
        BEGIN
            xxwc_eis_customer_balance_v2.table_create_from_old ('XXEIS'
                                                               ,'XXWC_AR_CUST##AR_PAYMENT_SCH_Z'
                                                               ,'XXWC_AR_CUST##AR_PAYMENT_SCH_X');

            EXECUTE IMMEDIATE
                'alter table  XXEIS.XXWC_AR_CUST##AR_PAYMENT_SCH_X add ( receipt_number varchar2(250),customer_receipt_reference varchar2(250),acra_status varchar2(100) )';

            EXECUTE IMMEDIATE 'INSERT /*+APPEND*/
          INTO  XXEIS.XXWC_AR_CUST##AR_PAYMENT_SCH_X
        SELECT a.*
       ,acra.receipt_number
,acra.customer_receipt_reference
,acra.status
      FROM XXEIS.XXWC_AR_CUST##AR_PAYMENT_SCH_Z a,ar.ar_cash_receipts_all acra
     WHERE  a.cash_receipt_id = acra.cash_receipt_id(+)';

            COMMIT;
        END;

        elapsed_time ('populate cash receipts information', g_start);

        --******************************************
        --populate trx_code_combination_id
        --******************************************
        BEGIN
            xxwc_eis_customer_balance_v2.table_create_from_old ('XXEIS'
                                                               ,'XXWC_AR_CUST##AR_PAYMENT_SCH_X'
                                                               ,'XXWC_AR_CUST##AR_PAYMENT_SCH_Z');

            EXECUTE IMMEDIATE
                'alter table  XXEIS.XXWC_AR_CUST##AR_PAYMENT_SCH_Z add ( trx_code_combination_id number )';

            EXECUTE IMMEDIATE
                'INSERT /*+APPEND*/
          INTO  XXEIS.XXWC_AR_CUST##AR_PAYMENT_SCH_Z
        SELECT a.*
       ,rctlgda.code_combination_id

      FROM XXEIS.XXWC_AR_CUST##AR_PAYMENT_SCH_X a,ar.ra_cust_trx_line_gl_dist_all rctlgda
     WHERE    a.customer_trx_id = rctlgda.customer_trx_id(+)
            AND rctlgda.customer_trx_line_id(+) IS NULL
             AND rctlgda.account_class(+) = ''REC''';

            COMMIT;
        END;

        elapsed_time ('populate trx_code_combination_id', g_start);

        --******************************************
        --populate receipt ccid
        --******************************************
        BEGIN
            xxwc_eis_customer_balance_v2.table_create_from_old ('XXEIS'
                                                               ,'XXWC_AR_CUST##AR_PAYMENT_SCH_Z'
                                                               ,'XXWC_AR_CUST##AR_PAYMENT_SCH_X');

            EXECUTE IMMEDIATE 'alter table  XXEIS.XXWC_AR_CUST##AR_PAYMENT_SCH_X add ( acra_ccid number )';

            EXECUTE IMMEDIATE 'INSERT /*+APPEND*/
          INTO  XXEIS.XXWC_AR_CUST##AR_PAYMENT_SCH_X
        SELECT a.*
       ,acrha.account_code_combination_id

      FROM XXEIS.XXWC_AR_CUST##AR_PAYMENT_SCH_Z a,ar.ar_cash_receipt_history_all acrha
     WHERE    a.cash_receipt_id = acrha.cash_receipt_id(+)
            AND acrha.first_posted_record_flag(+) = ''Y''
           ';

            COMMIT;
        END;

        elapsed_time ('populate receipt ccid', g_start);

        --******************************************
        --populate bill_to_information
        --******************************************
        BEGIN
            xxwc_eis_customer_balance_v2.table_create_from_old ('XXEIS'
                                                               ,'XXWC_AR_CUST##AR_PAYMENT_SCH_X'
                                                               ,'XXWC_AR_CUST##AR_PAYMENT_SCH_Z');

            EXECUTE IMMEDIATE
                'alter table  XXEIS.XXWC_AR_CUST##AR_PAYMENT_SCH_Z add (  billto_party_id  number, customer_account_number varchar2(250),
       prism_customer_number  varchar2(250),
       customer_name varchar2(512),
        bill_to_site_use_id number,
       bill_to_party_site_number varchar2(250),
       bill_to_prism_site_number varchar2(250),
       bill_to_party_site_name varchar2(250),
      bill_to_site_name varchar2(512),
       bill_to_address1 varchar2(250),
        bill_to_address2 varchar2(250),
        bill_to_address3 varchar2(250),
        bill_to_address4 varchar2(250),
       bill_to_city varchar2(250),
       bill_to_city_province varchar2(250),
       bill_to_zip_code varchar2(250),
       bill_to_country varchar2(250),primary_salesrep_id number  )';

    -- added replace function in below dynamic sql to overcome line broke issue for V1.0 
            EXECUTE IMMEDIATE 'INSERT /*+APPEND*/
          INTO  XXEIS.XXWC_AR_CUST##AR_PAYMENT_SCH_Z
        SELECT a.*  ,c.party_id,
  c.account_number customer_account_number
      ,c.attribute6 prism_customer_number
      ,c.account_name customer_name
      ,c.site_use_id bill_to_site_use_id
      ,c.party_site_number bill_to_party_site_number
      ,c.attribute17 bill_to_prism_site_number
      ,c.party_site_name bill_to_party_site_name
      ,replace(c.location,''"'','''') bill_to_site_name 
      ,Replace(c.address1,''"'','''') bill_to_address1
      ,Replace(c.address2,''"'','''') bill_to_address2
      ,Replace(c.address3,''"'','''') bill_to_address3
      ,Replace(c.address4,''"'','''') bill_to_address4
      ,c.city bill_to_city
      , NVL (c.state, c.province) bill_to_city_province
      ,c.postal_code bill_to_zip_code
      ,c.country bill_to_country,c.primary_salesrep_id
  FROM XXEIS.XXWC_AR_CUST##AR_PAYMENT_SCH_X a ,xxeis.xxwc_customer_sites_vrm  c
 WHERE     c.site_use_code(+) = ''BILL_TO''
       AND c.cust_account_id(+) = a.customer_id
       AND c.primary_flag(+) = ''Y''
       AND c.cust_acct_sites_bill_to_flag(+) IN (''P'', ''Y'')
       AND c.cust_accounts_status(+) = ''A''';

            COMMIT;
        END;

        elapsed_time ('populate bill_to_information', g_start);

        --*************************************************88
        --update records with null bill_to
        --***************************************************
        EXECUTE IMMEDIATE '
BEGIN
    FOR r IN (SELECT ROWID vrow, customer_site_use_id
                FROM XXEIS.XXWC_AR_CUST##AR_PAYMENT_SCH_Z
               WHERE customer_name IS NULL)
    LOOP
        FOR ri IN (SELECT *
                     FROM xxeis.xxwc_customer_sites_vrm c
                    WHERE site_use_id = r.customer_site_use_id)
        LOOP
            UPDATE XXEIS.XXWC_AR_CUST##AR_PAYMENT_SCH_Z a
               SET a.customer_account_number = ri.account_number
                  ,a.prism_customer_number = ri.attribute6
                  ,a.customer_name = ri.account_name
                  ,a.bill_to_site_use_id = ri.site_use_id
                  ,a.bill_to_party_site_number = ri.party_site_number
                  ,a.bill_to_prism_site_number = ri.attribute17
                  ,a.bill_to_party_site_name = ri.party_site_name
                  ,a.bill_to_site_name = ri.location
                  ,a.bill_to_address1 = ri.address1
                  ,a.bill_to_address2 = ri.address2
                  ,a.bill_to_address3 = ri.address3
                  ,a.bill_to_address4 = ri.address4
                  ,a.bill_to_city = ri.city
                  ,a.bill_to_city_province = NVL (ri.state, ri.province)
                  ,a.bill_to_zip_code = ri.postal_code
                  ,a.bill_to_country = ri.country
             WHERE a.ROWID = r.vrow;
        END LOOP;
    END LOOP;
END;';

        COMMIT;
        elapsed_time ('update records with null bill_to', g_start);

        --**************************************************
        --populate trx site information
        BEGIN
            xxwc_eis_customer_balance_v2.table_create_from_old ('XXEIS'
                                                               ,'XXWC_AR_CUST##AR_PAYMENT_SCH_Z'
                                                               ,'XXWC_AR_CUST##AR_PAYMENT_SCH_X');

            EXECUTE IMMEDIATE 'alter table  XXEIS.XXWC_AR_CUST##AR_PAYMENT_SCH_X add (  trx_site_use_id number,
       trx_party_site_number varchar2(250),
       trx_party_site_name varchar2(250),
       trx_bill_to_site_name varchar2(512),
       trx_bill_to_address1 varchar2(250),
        trx_bill_to_address2 varchar2(250),
        trx_bill_to_address3 varchar2(250),
        trx_bill_to_address4 varchar2(250),
       trx_bill_to_city varchar2(250),
       trx_bill_to_city_prov varchar2(250),
       trx_bill_to_zip_code varchar2(250),
       trx_bill_to_country varchar2(250)  )';

            EXECUTE IMMEDIATE 'INSERT /*+APPEND*/
          INTO  XXEIS.XXWC_AR_CUST##AR_PAYMENT_SCH_X
        SELECT a.* , c.site_use_id,--1
       c.party_site_number trx_party_site_number
       ,c.party_site_name  trx_party_site_name
      ,c.location trx_bill_to_site_name

      ,c.address1 trx_bill_to_address1
      ,c.address2 trx_bill_to_address2
      ,c.address3 trx_bill_to_address3
      ,c.address4 trx_bill_to_address4
      ,c.city trx_bill_to_city
      , NVL (c.state, c.province) trx_bill_to_city_prov
      ,c.postal_code trx_bill_to_zip_code
      ,c.country trx_bill_to_country
  FROM XXEIS.XXWC_AR_CUST##AR_PAYMENT_SCH_Z a ,xxeis.xxwc_customer_sites_vrm c
 WHERE  c.site_use_id(+) = a.customer_site_use_id ';

            COMMIT;
        END;

        elapsed_time ('populate trx site information', g_start);

        --*****************************************************************

        --******************************************
        --populate profile info
        --******************************************
        BEGIN
            xxwc_eis_customer_balance_v2.table_create_from_old ('XXEIS'
                                                               ,'XXWC_AR_CUST##AR_PAYMENT_SCH_X'
                                                               ,'XXWC_AR_CUST##AR_PAYMENT_SCH_Z');

            EXECUTE IMMEDIATE
                'alter table  XXEIS.XXWC_AR_CUST##AR_PAYMENT_SCH_Z add (  remit_to_address_code varchar2(50), stmt_by_job  varchar2(50),
    send_statement_flag varchar2(50),send_credit_bal_flag varchar2(50),account_credit_hold varchar2(50),hpc_account_status varchar2(100),
     account_status varchar2(100),profile_class_id number,collector_id number, credit_analyst_id number, standard_terms varchar2(50))';

            EXECUTE IMMEDIATE
                'INSERT /*+APPEND*/
          INTO  XXEIS.XXWC_AR_CUST##AR_PAYMENT_SCH_Z
        SELECT a.*  ,NVL (hcp.attribute2, ''2''),NVL (hcp.attribute3, ''N''),NVL (hcp.send_statements, ''N''),
        NVL (hcp.credit_balance_statements, ''N''),hcp.credit_hold,
        (SELECT meaning
  FROM fnd_lookup_values
 WHERE lookup_type = ''ACCOUNT_STATUS'' AND lookup_code = hcp.account_status),hcp.account_status,hcp.profile_class_id, hcp.collector_id,
  hcp.credit_analyst_id,hcp.standard_terms
  FROM XXEIS.XXWC_AR_CUST##AR_PAYMENT_SCH_X a ,ar.hz_customer_profiles hcp
 WHERE    hcp.site_use_id(+) IS NULL
  AND a.billto_party_id  = hcp.party_id(+)
          AND a.customer_id  = hcp.cust_account_id(+)';

            COMMIT;
        END;

        elapsed_time ('populate profile info', g_start);

        --**************************************************
        --profile class
        BEGIN
            xxwc_eis_customer_balance_v2.table_create_from_old ('XXEIS'
                                                               ,'XXWC_AR_CUST##AR_PAYMENT_SCH_Z'
                                                               ,'XXWC_AR_CUST##AR_PAYMENT_SCH_X');

            EXECUTE IMMEDIATE 'alter table  XXEIS.XXWC_AR_CUST##AR_PAYMENT_SCH_X add (
       customer_profile_class  varchar2(250)  )';

            EXECUTE IMMEDIATE 'INSERT /*+APPEND*/
          INTO  XXEIS.XXWC_AR_CUST##AR_PAYMENT_SCH_X
        SELECT a.* , hcpc.name
  FROM XXEIS.XXWC_AR_CUST##AR_PAYMENT_SCH_Z a ,ar.hz_cust_profile_classes hcpc
 WHERE  a.profile_class_id = hcpc.profile_class_id(+)';

            COMMIT;
        END;

        elapsed_time ('populate profile info', g_start);

        --*******************************************
        --terms
        BEGIN
            xxwc_eis_customer_balance_v2.table_create_from_old ('XXEIS'
                                                               ,'XXWC_AR_CUST##AR_PAYMENT_SCH_X'
                                                               ,'XXWC_AR_CUST##AR_PAYMENT_SCH_Z');

            EXECUTE IMMEDIATE
                'alter table  XXEIS.XXWC_AR_CUST##AR_PAYMENT_SCH_Z add (  cust_payment_term varchar2(50) )';

            EXECUTE IMMEDIATE 'INSERT /*+APPEND*/
          INTO  XXEIS.XXWC_AR_CUST##AR_PAYMENT_SCH_Z
        SELECT a.* , rtt.description
  FROM XXEIS.XXWC_AR_CUST##AR_PAYMENT_SCH_X a ,ar.ra_terms_tl rtt
 WHERE   a.standard_terms = rtt.term_id(+)';

            COMMIT;
        END;

        elapsed_time ('populate terms', g_start);

        --**************************************************
        --collector
        BEGIN
            xxwc_eis_customer_balance_v2.table_create_from_old ('XXEIS'
                                                               ,'XXWC_AR_CUST##AR_PAYMENT_SCH_Z'
                                                               ,'XXWC_AR_CUST##AR_PAYMENT_SCH_X');

            EXECUTE IMMEDIATE 'alter table  XXEIS.XXWC_AR_CUST##AR_PAYMENT_SCH_X add (
       collector_name  varchar2(250)  )';

            EXECUTE IMMEDIATE 'INSERT /*+APPEND*/
          INTO  XXEIS.XXWC_AR_CUST##AR_PAYMENT_SCH_X
        SELECT a.* , ac.name collector_name
  FROM XXEIS.XXWC_AR_CUST##AR_PAYMENT_SCH_Z a  ,ar.ar_collectors ac
 WHERE  a.collector_id = ac.collector_id(+)';

            COMMIT;
        END;

        elapsed_time ('populate collector info', g_start);

        --*******************************************
        --credit_analyst
        --*******************************************

        BEGIN
            xxwc_eis_customer_balance_v2.table_create_from_old ('XXEIS'
                                                               ,'XXWC_AR_CUST##AR_PAYMENT_SCH_X'
                                                               ,'XXWC_AR_CUST##AR_PAYMENT_SCH_Z');

            EXECUTE IMMEDIATE 'alter table  XXEIS.XXWC_AR_CUST##AR_PAYMENT_SCH_Z add (  credit_analyst varchar2(250) )';

            EXECUTE IMMEDIATE 'INSERT /*+APPEND*/
          INTO  XXEIS.XXWC_AR_CUST##AR_PAYMENT_SCH_Z
        SELECT a.* , jrret_cr.resource_name
  FROM XXEIS.XXWC_AR_CUST##AR_PAYMENT_SCH_X a ,jtf.jtf_rs_resource_extns_tl jrret_cr
 WHERE a.credit_analyst_id = jrret_cr.resource_id(+)
              AND jrret_cr.language(+) = ''US''
         AND jrret_cr.category(+) = ''EMPLOYEE''';

            COMMIT;
        END;

        elapsed_time ('populate credit_analyst info', g_start);

        --select * from XXEIS.XXWC_AR_CUST##AR_PAYMENT_SCH_X

        --*******************************************
        --account_manager,salesrep
        --******************************************
        --**************************************************

        BEGIN
            xxwc_eis_customer_balance_v2.table_create_from_old ('XXEIS'
                                                               ,'XXWC_AR_CUST##AR_PAYMENT_SCH_Z'
                                                               ,'XXWC_AR_CUST##AR_PAYMENT_SCH_X');

            EXECUTE IMMEDIATE 'alter table  XXEIS.XXWC_AR_CUST##AR_PAYMENT_SCH_X add (
      account_manager  varchar2(250) ,salesrep_number varchar2(250) )';

            EXECUTE IMMEDIATE
                'INSERT /*+APPEND*/
          INTO  XXEIS.XXWC_AR_CUST##AR_PAYMENT_SCH_X
        SELECT a.* , NVL (NVL (jrs.name, papf.full_name), jrret.resource_name) account_manager, jrs.salesrep_number
  FROM XXEIS.XXWC_AR_CUST##AR_PAYMENT_SCH_Z a  ,jtf.jtf_rs_salesreps jrs,hr.per_all_people_f papf ,jtf.jtf_rs_resource_extns jrre,jtf.jtf_rs_resource_extns_tl jrret
 WHERE  a.primary_salesrep_id = jrs.salesrep_id(+)
  AND a.org_id = jrs.org_id(+)
           AND jrs.person_id = papf.person_id(+)
           AND TRUNC (papf.effective_start_date(+)) <= TRUNC (SYSDATE)
           AND NVL (TRUNC (papf.effective_end_date(+)), TRUNC (SYSDATE)) >= TRUNC (SYSDATE)
           AND jrs.resource_id = jrre.resource_id(+)
           AND jrre.resource_id = jrret.resource_id(+)
           AND jrret.language(+) = ''US''
           AND jrre.category = jrret.category(+)';

            COMMIT;
        END;

        elapsed_time ('populate account_manager,salesrep info', g_start);

        --************************************************************
        --

        BEGIN
            xxwc_eis_customer_balance_v2.table_create_from_old ('XXEIS'
                                                               ,'XXWC_AR_CUST##AR_PAYMENT_SCH_X'
                                                               ,'XXWC_AR_CUST##AR_PAYMENT_SCH_Z');

            EXECUTE IMMEDIATE
                'alter table  XXEIS.XXWC_AR_CUST##AR_PAYMENT_SCH_Z add (  site_credit_hold varchar2(250) )';

            EXECUTE IMMEDIATE 'INSERT /*+APPEND*/
          INTO  XXEIS.XXWC_AR_CUST##AR_PAYMENT_SCH_Z
        SELECT a.* , hcp_trx.credit_hold site_credit_hold
  FROM XXEIS.XXWC_AR_CUST##AR_PAYMENT_SCH_X a ,ar.hz_customer_profiles hcp_trx
    where a.trx_site_use_id  = hcp_trx.site_use_id(+)';

            COMMIT;
        END;

        elapsed_time ('populate site_credit_hold info', g_start);

        --select * from XXEIS.XXWC_AR_CUST##AR_PAYMENT_SCH_X
        --******************************************
        --**************************************************

        BEGIN
            xxwc_eis_customer_balance_v2.table_create_from_old ('XXEIS'
                                                               ,'XXWC_AR_CUST##AR_PAYMENT_SCH_Z'
                                                               ,'XXWC_AR_CUST##AR_PAYMENT_SCH_X');

            EXECUTE IMMEDIATE 'alter table  XXEIS.XXWC_AR_CUST##AR_PAYMENT_SCH_X add (
      account_balance number   )';

            EXECUTE IMMEDIATE ' BEGIN
            FOR n IN 1 .. :tuning_factor
            LOOP
            INSERT /*+APPEND*/
          INTO  XXEIS.XXWC_AR_CUST##AR_PAYMENT_SCH_X
          SELECT a.* ,(SELECT SUM (apsa2.amount_due_remaining)

          FROM ar_payment_schedules_all apsa2
           WHERE apsa2.customer_id  =a.customer_id)
          FROM XXEIS.XXWC_AR_CUST##AR_PAYMENT_SCH_Z a   where group_number = n;
                 COMMIT;
            END LOOP;
        END;'
                USING v_tuning_factor;

            COMMIT;
        END;

        elapsed_time ('populate inner function account_balance', g_start);

        --************************************************************

        BEGIN
            xxwc_eis_customer_balance_v2.table_create_from_old ('XXEIS'
                                                               ,'XXWC_AR_CUST##AR_PAYMENT_SCH_X'
                                                               ,'XXWC_AR_CUST##AR_PAYMENT_SCH_Z');

            EXECUTE IMMEDIATE 'alter table  XXEIS.XXWC_AR_CUST##AR_PAYMENT_SCH_Z add (
      twelve_months_sales number  )';

            EXECUTE IMMEDIATE
                ' BEGIN
            FOR n IN 1 .. :tuning_factor
            LOOP
            INSERT /*+APPEND*/
          INTO  XXEIS.XXWC_AR_CUST##AR_PAYMENT_SCH_Z
        SELECT a.* ,nvl((SELECT SUM (amount_due_original)
  FROM ar_payment_schedules_all apsa
 WHERE     apsa.class NOT IN (''PMT'')
       AND apsa.org_id = a.org_id
       AND apsa.customer_id = a.customer_id
       AND TRUNC (apsa.trx_date) BETWEEN (ADD_MONTHS (TRUNC (SYSDATE), -12)) AND TRUNC (SYSDATE)),0)
  FROM XXEIS.XXWC_AR_CUST##AR_PAYMENT_SCH_X a  where group_number = n;
     COMMIT;
         END LOOP;
        END;'
                USING v_tuning_factor;

            COMMIT;
        END;

        elapsed_time ('populate inner function twelve_months_sales', g_start);

        --**************************************************

        BEGIN
            xxwc_eis_customer_balance_v2.table_create_from_old ('XXEIS'
                                                               ,'XXWC_AR_CUST##AR_PAYMENT_SCH_Z'
                                                               ,'XXWC_AR_CUST##AR_PAYMENT_SCH_X');

            EXECUTE IMMEDIATE 'alter table  XXEIS.XXWC_AR_CUST##AR_PAYMENT_SCH_X add (
      yard_credit_limit number   )';

            EXECUTE IMMEDIATE
                ' BEGIN
            FOR n IN 1 .. :tuning_factor
            LOOP
            INSERT /*+APPEND*/
          INTO  XXEIS.XXWC_AR_CUST##AR_PAYMENT_SCH_X
       SELECT z.*
      ,(apps.xxwc_mv_routines_pkg.get_site_type_credit_limit (z.customer_id, NULL, ''YARD''))
  FROM XXEIS.XXWC_AR_CUST##AR_PAYMENT_SCH_Z z where group_number = n;
      COMMIT;
         END LOOP;
        END;'
                USING v_tuning_factor;

            COMMIT;
        END;

        elapsed_time ('inner function yard_credit_limit', g_start);

        --*********************************** phone

        BEGIN
            xxwc_eis_customer_balance_v2.table_create_from_old ('XXEIS'
                                                               ,'XXWC_AR_CUST##AR_PAYMENT_SCH_X'
                                                               ,'XXWC_AR_CUST##AR_PAYMENT_SCH_Z');

            EXECUTE IMMEDIATE 'alter table  XXEIS.XXWC_AR_CUST##AR_PAYMENT_SCH_Z add (
      acct_phone_number varchar(200)  )';

            EXECUTE IMMEDIATE
                'INSERT /*+APPEND*/
          INTO  XXEIS.XXWC_AR_CUST##AR_PAYMENT_SCH_Z
        SELECT a.* ,(apps.xxwc_mv_routines_pkg.format_phone_number ( (apps.xxwc_mv_routines_pkg.get_phone_fax_number (
                                                                  ''PHONE''
                                                                 ,a.billto_party_id
                                                                 ,NULL
                                                                 ,''GEN''))))
  FROM XXEIS.XXWC_AR_CUST##AR_PAYMENT_SCH_X a  ';

            COMMIT;
        END;

        elapsed_time ('inner function format_phone_number', g_start);

        BEGIN
            xxwc_eis_customer_balance_v2.drop_temp_table ('XXEIS', 'xxwc_ar_customer_balance_mv##Z');

            EXECUTE IMMEDIATE 'CREATE TABLE xxeis.xxwc_ar_customer_balance_mv##Z
(
    cust_account_id                 NUMBER
   ,customer_account_number         VARCHAR2 (30 BYTE)
   ,prism_customer_number           VARCHAR2 (150 BYTE)
   ,customer_name                   VARCHAR2 (360 BYTE)
   ,bill_to_site_use_id             NUMBER
   ,bill_to_party_site_number       VARCHAR2 (30 BYTE)
   ,bill_to_prism_site_number       VARCHAR2 (150 BYTE)
   ,bill_to_party_site_name         VARCHAR2 (240 BYTE)
   ,bill_to_site_name               VARCHAR2 (100 BYTE)
   ,trx_bill_to_site_name           VARCHAR2 (100 BYTE)
   ,trx_party_site_number           VARCHAR2 (30 BYTE)
   ,trx_party_site_name             VARCHAR2 (240 BYTE)
   ,bill_to_address1                VARCHAR2 (240 BYTE)
   ,bill_to_address2                VARCHAR2 (240 BYTE)
   ,bill_to_address3                VARCHAR2 (240 BYTE)
   ,bill_to_address4                VARCHAR2 (240 BYTE)
   ,bill_to_city                    VARCHAR2 (160 BYTE)
   ,bill_to_city_province           VARCHAR2 (160 BYTE)
   ,bill_to_zip_code                VARCHAR2 (160 BYTE)
   ,bill_to_country                 VARCHAR2 (160 BYTE)
   ,payment_schedule_id             NUMBER
   ,customer_trx_id                 NUMBER
   ,rcta_ccid                       NUMBER
   ,cash_receipt_id                 NUMBER
   ,acra_ccid                       NUMBER
   ,invoice_number                  VARCHAR2 (150 BYTE)
   ,receipt_number                  VARCHAR2 (150 BYTE)
   ,branch_location                 VARCHAR2 (4000 BYTE)
   ,trx_number                      VARCHAR2 (130 BYTE)
   ,trx_date                        DATE
   ,due_date                        DATE
   ,trx_type                        VARCHAR2 (120 BYTE)
   ,transation_balance              NUMBER
   ,transaction_remaining_balance   NUMBER
   ,age                             NUMBER
   ,current_balance                 NUMBER
   ,thirty_days_bal                 NUMBER
   ,sixty_days_bal                  NUMBER
   ,ninety_days_bal                 NUMBER
   ,one_eighty_days_bal             NUMBER
   ,three_sixty_days_bal            NUMBER
   ,over_three_sixty_days_bal       NUMBER
   ,last_payment_date               DATE
   ,customer_account_status         VARCHAR2 (180 BYTE)
   ,site_credit_hold                VARCHAR2 (54 BYTE)
   ,customer_profile_class          VARCHAR2 (124 BYTE)
   ,collector_name                  VARCHAR2 (124 BYTE)
   ,credit_analyst                  VARCHAR2 (360 BYTE)
   ,account_manager                 VARCHAR2 (360 BYTE)
   ,account_balance                 NUMBER
   ,cust_payment_term               VARCHAR2 (240 BYTE)
   ,remit_to_address_code           VARCHAR2 (150 BYTE)
   ,stmt_by_job                     VARCHAR2 (150 BYTE)
   ,send_statement_flag             VARCHAR2 (1 BYTE)
   ,send_credit_bal_flag            VARCHAR2 (1 BYTE)
   ,account_credit_hold             VARCHAR2 (1 BYTE)
   ,trx_customer_id                 NUMBER
   ,trx_bill_site_use_id            NUMBER
   ,customer_po_number              VARCHAR2 (150 BYTE)
   ,pmt_status                      VARCHAR2 (130 BYTE)
   ,salesrep_number                 VARCHAR2 (130 BYTE)
   ,account_status                  VARCHAR2 (240 BYTE)
   ,twelve_months_sales             NUMBER
   ,yard_credit_limit               NUMBER
   ,acct_phone_number               VARCHAR2 (4000 BYTE)
   ,trx_bill_to_address1            VARCHAR2 (240 BYTE)
   ,trx_bill_to_address2            VARCHAR2 (240 BYTE)
   ,trx_bill_to_address3            VARCHAR2 (240 BYTE)
   ,trx_bill_to_address4            VARCHAR2 (240 BYTE)
   ,trx_bill_to_city                VARCHAR2 (160 BYTE)
   ,trx_bill_to_city_prov           VARCHAR2 (160 BYTE)
   ,trx_bill_to_zip_code            VARCHAR2 (160 BYTE)
   ,trx_bill_to_country             VARCHAR2 (160 BYTE)
   ,in_process_total                NUMBER
   ,largest_balance                 NUMBER
   ,high_credit                     NUMBER
   ,auto_apply_cm     		    VARCHAR2 (1 BYTE)              --Added for ver2.0

)';

            --xxwc_eis_customer_balance_v2.alter_table_temp ('XXEIS', 'xxwc_ar_customer_balance_mv##Z');  commented parallelism by Maha on 12/28/14
-- added replace function in below dynamic sql to overcome line broke issue for V1.0

            EXECUTE IMMEDIATE
                '
INSERT/*+append*/ into xxeis.xxwc_ar_customer_balance_mv##Z
SELECT customer_id
      ,customer_account_number
      ,prism_customer_number
      ,customer_name
      ,bill_to_site_use_id
      ,bill_to_party_site_number
      ,bill_to_prism_site_number
      ,bill_to_party_site_name
      ,Replace(bill_to_site_name,''"'','''')
      ,trx_bill_to_site_name
      ,trx_party_site_number
      ,Replace(trx_party_site_name,''"'','''')
      ,Replace(bill_to_address1,''"'','''')
      ,Replace(bill_to_address2,''"'','''')
      ,Replace(bill_to_address3,''"'','''')
      ,Replace(bill_to_address4,''"'','''')
      ,bill_to_city
      ,bill_to_city_province
      ,bill_to_zip_code
      ,bill_to_country
      ,payment_schedule_id
      ,customer_trx_id
      ,trx_code_combination_id rcta_ccid
      ,cash_receipt_id
      ,acra_ccid
      ,trx_number invoice_number
      ,receipt_number
      ,apps.xxwc_mv_routines_pkg.get_gl_code_segment (NVL (trx_code_combination_id, acra_ccid), 2) branch_location
      ,trx_number
      ,trx_date
      ,due_date
      , (CASE
             WHEN class = ''INV'' THEN ''Invoice''
             WHEN class = ''CM'' THEN ''Credit Memo''
             WHEN class = ''DM'' THEN ''Debit Memo''
             WHEN class = ''PMT'' THEN ''Payment''
             ELSE class
         END)
           trx_type
      ,amount_due_original transation_balance
      ,amount_due_remaining transaction_remaining_balance
      , (TRUNC (SYSDATE) - TRUNC (due_date)) age
      , (CASE WHEN (TRUNC (SYSDATE) - TRUNC (due_date)) <= 0 THEN amount_due_remaining ELSE 0 END) current_balance
      , (CASE WHEN (TRUNC (SYSDATE) - TRUNC (due_date)) BETWEEN 1 AND 30 THEN amount_due_remaining ELSE 0 END)
           thirty_days_bal
      , (CASE WHEN (TRUNC (SYSDATE) - TRUNC (due_date)) BETWEEN 31 AND 60 THEN amount_due_remaining ELSE 0 END)
           sixty_days_bal
      , (CASE WHEN (TRUNC (SYSDATE) - TRUNC (due_date)) BETWEEN 61 AND 90 THEN amount_due_remaining ELSE 0 END)
           ninety_days_bal
      , (CASE WHEN (TRUNC (SYSDATE) - TRUNC (due_date)) BETWEEN 91 AND 180 THEN amount_due_remaining ELSE 0 END)
           one_eighty_days_bal
      , (CASE WHEN (TRUNC (SYSDATE) - TRUNC (due_date)) BETWEEN 181 AND 360 THEN amount_due_remaining ELSE 0 END)
           three_sixty_days_bal
      , (CASE WHEN (TRUNC (SYSDATE) - TRUNC (due_date)) >= 361 THEN amount_due_remaining ELSE 0 END)
           over_three_sixty_days_bal
      ,apps.xxwc_mv_routines_pkg.get_customer_last_payment_date (customer_id) last_payment_date
      , (SELECT flv.meaning
           FROM applsys.fnd_lookup_values flv
          WHERE     account_status = flv.lookup_code(+)
                AND flv.lookup_type(+) = ''ACCOUNT_STATUS''
                AND flv.language(+) = ''US'')
           customer_account_status
      ,site_credit_hold
      ,customer_profile_class
      ,collector_name
      ,credit_analyst
      ,account_manager
      ,account_balance
      ,cust_payment_term
      ,remit_to_address_code
      ,stmt_by_job
      ,send_statement_flag
      ,send_credit_bal_flag
      ,account_credit_hold
      ,customer_id trx_customer_id
      ,customer_site_use_id trx_bill_site_use_id
      ,NVL (purchase_order, customer_receipt_reference) customer_po_number
      ,acra_status
      ,salesrep_number
      ,hpc_account_status account_status
      ,twelve_months_sales
      ,yard_credit_limit

      ,acct_phone_number
      ,trx_bill_to_address1
      ,trx_bill_to_address2
      ,trx_bill_to_address3
      ,trx_bill_to_address4
      ,trx_bill_to_city
      ,trx_bill_to_city_prov
      ,trx_bill_to_zip_code
      ,trx_bill_to_country
      ,0 in_process_total        -- (apps.xxwc_mv_routines_pkg.get_cust_in_proc_bal (apsa.customer_id)) in_process_total
      ,0 largest_balance         -- (apps.xxwc_mv_routines_pkg.get_cust_high_balance (apsa.customer_id)) largest_balance
      ,0 high_credit                   -- (apps.xxwc_mv_routines_pkg.get_high_credit_ytd (apsa.customer_id)) high_credit
      ,(SELECT hca.attribute11 from hz_cust_accounts hca WHERE hca.cust_account_id = cust_account_id and rownum = 1)          --Added for ver 2.0
  FROM XXEIS.XXWC_AR_CUST##AR_PAYMENT_SCH_Z';

            COMMIT;
        END;

        elapsed_time ('Z table', g_start);

        BEGIN
            xxwc_eis_customer_balance_v2.table_create_from_old ('XXEIS'
                                                               ,'xxwc_ar_customer_balance_mv##Z'
                                                               ,'XXWC_AR_CUSTOMER_BALANCE_MV##X');

            EXECUTE IMMEDIATE
                '
INSERT/*+append*/ into xxeis.XXWC_AR_CUSTOMER_BALANCE_MV##X
 SELECT -1 cust_account_id
          ,''-1'' customer_account_number
          ,''-1'' prism_customer_number
          ,''UNKNOWN CUSTOMER'' customer_name
          ,-1 bill_to_site_use_id
          ,''-1'' bill_to_party_site_number
          ,''-1'' bill_to_prism_site_number
          ,''UNKNOWN SITE'' bill_to_party_site_name
          ,''UNKNOWN SITE'' bill_to_site_name
          ,''UNKNOWN SITE'' trx_bill_to_site_name
          ,''-1'' trx_party_site_number
          ,''UNKNOWN SITE'' trx_party_site_name
          ,'' '' bill_to_address1
          ,'' '' bill_to_address2
          ,'' '' bill_to_address3
          ,'' '' bill_to_address4
          ,'' '' bill_to_city
          ,'' '' bill_to_city_province
          ,'' '' bill_to_zip_code
          ,'' '' bill_to_country
          ,apsa.payment_schedule_id
          ,rcta.customer_trx_id
          ,rctlgda.code_combination_id rcta_ccid
          ,acra.cash_receipt_id
          ,                                                     -- 06/14/2012 CG Corrected from using ADA to using ACRHA
           acrha.account_code_combination_id acra_ccid
          ,rcta.trx_number invoice_number
          ,acra.receipt_number
          ,apps.xxwc_mv_routines_pkg.get_gl_code_segment (
               NVL (rctlgda.code_combination_id, acrha.account_code_combination_id)
              ,2)
               branch_location
          ,apsa.trx_number
          ,apsa.trx_date
          ,apsa.due_date
          , (CASE
                 WHEN apsa.class = ''INV'' THEN ''Invoice''
                 WHEN apsa.class = ''CM'' THEN ''Credit Memo''
                 WHEN apsa.class = ''DM'' THEN ''Debit Memo''
                 WHEN apsa.class = ''PMT'' THEN ''Payment''
                 ELSE apsa.class
             END)
               trx_type
          ,apsa.amount_due_original transation_balance
          ,apsa.amount_due_remaining transaction_remaining_balance
          , (TRUNC (SYSDATE) - TRUNC (apsa.due_date)) age
          , (CASE WHEN (TRUNC (SYSDATE) - TRUNC (apsa.due_date)) <= 0 THEN apsa.amount_due_remaining ELSE 0 END)
               current_balance
          , (CASE
                 WHEN (TRUNC (SYSDATE) - TRUNC (apsa.due_date)) BETWEEN 1 AND 30 THEN apsa.amount_due_remaining
                 ELSE 0
             END)
               thirty_days_bal
          , (CASE
                 WHEN (TRUNC (SYSDATE) - TRUNC (apsa.due_date)) BETWEEN 31 AND 60 THEN apsa.amount_due_remaining
                 ELSE 0
             END)
               sixty_days_bal
          , (CASE
                 WHEN (TRUNC (SYSDATE) - TRUNC (apsa.due_date)) BETWEEN 61 AND 90 THEN apsa.amount_due_remaining
                 ELSE 0
             END)
               ninety_days_bal
          , (CASE
                 WHEN (TRUNC (SYSDATE) - TRUNC (apsa.due_date)) BETWEEN 91 AND 180 THEN apsa.amount_due_remaining
                 ELSE 0
             END)
               one_eighty_days_bal
          , (CASE
                 WHEN (TRUNC (SYSDATE) - TRUNC (apsa.due_date)) BETWEEN 181 AND 360 THEN apsa.amount_due_remaining
                 ELSE 0
             END)
               three_sixty_days_bal
          , (CASE WHEN (TRUNC (SYSDATE) - TRUNC (apsa.due_date)) >= 361 THEN apsa.amount_due_remaining ELSE 0 END)
               over_three_sixty_days_bal
          ,TO_DATE (''01/01/1952'', ''MM/DD/YYYY'') last_payment_date
          ,'' '' customer_account_status
          ,''Y'' site_credit_hold
          ,'' '' customer_profile_class
          ,'' '' collector_name
          ,'' '' credit_analyst
          ,'' '' account_manager
          ,0 account_balance
          ,'' '' cust_payment_term
          ,''2'' remit_to_address_code
          ,''N'' stmt_by_job
          ,''N'' send_statement_flag
          ,''N'' send_credit_bal_flag
          ,                                       -- 06/19/2012 CG: added account level credit hold flag per EiS Request
           ''N'' account_credit_hold
          ,                                                                                             -- 06/19/2012 CG
           apsa.customer_id trx_customer_id
          ,apsa.customer_site_use_id trx_bill_site_use_id
          ,NVL (rcta.purchase_order, acra.customer_receipt_reference) customer_po_number
          ,acra.status pmt_status
          ,                                        -- 07/20/2012 CG: Changes to add additional fields for Reserve Report
           '' '' salesrep_number
          -- 90/13/2012 CG: Added 3 new columns per Pedro Pagan and Stephen Gribbin (email 09/11/2012)
          ,'' '' account_status
          ,0 twelve_months_sales
          ,0 yard_credit_limit
          ,'' '' acct_phone_number
          ,'' '' trx_bill_to_address1
          ,'' '' trx_bill_to_address2
          ,'' '' trx_bill_to_address3
          ,'' '' trx_bill_to_address4
          ,'' '' trx_bill_to_city
          ,'' '' trx_bill_to_city_prov
          ,'' '' trx_bill_to_zip_code
          ,'' '' trx_bill_to_country
          ,0 in_process_total
          ,0 largest_balance
          ,0 high_credit
          , (SELECT hca.attribute11 from hz_cust_accounts hca WHERE hca.cust_account_id = apsa.customer_id and rownum = 1)          --Added for ver 2.0
      FROM ar.ar_payment_schedules_all apsa
          ,ar.ra_customer_trx_all rcta
          ,ar.ra_cust_trx_line_gl_dist_all rctlgda
          ,ar.ar_cash_receipts_all acra
          ,                                              -- 06/14/2012 CG: Changed to point to the cash receipts history
           -- since it contains the latest posting
           -- ar.ar_distributions_all ada,
           ar.ar_cash_receipt_history_all acrha
          ,apps.hr_operating_units hou
     WHERE     apsa.customer_id IS NULL
           AND ABS (apsa.amount_due_remaining) > 0
           AND apsa.customer_trx_id = rcta.customer_trx_id(+)
           AND rcta.customer_trx_id = rctlgda.customer_trx_id(+)
           AND rctlgda.customer_trx_line_id(+) IS NULL
           AND rctlgda.account_class(+) = ''REC''
           AND apsa.cash_receipt_id = acra.cash_receipt_id(+)
           -- 06/14/2012 CG: Changed to point to acrha
           -- AND acra.cash_receipt_id = ada.source_id(+)
           -- AND ada.source_table(+) = ''CRH''
           AND acra.cash_receipt_id = acrha.cash_receipt_id(+)
           AND acrha.first_posted_record_flag(+) = ''Y''
           AND apsa.org_id = hou.organization_id
           AND hou.name = ''HDS White Cap - Org''';

            COMMIT;
        END;

        elapsed_time ('X table', g_start);

        BEGIN
            xxwc_eis_customer_balance_v2.table_create_from_old ('XXEIS'
                                                               ,'xxwc_ar_customer_balance_mv##Z'
                                                               ,'xxwc_ar_customer_balance_mv##G');

            EXECUTE IMMEDIATE
                'INSERT/*+append*/ into xxeis.xxwc_ar_customer_balance_mv##G
    SELECT * FROM XXEIS.xxwc_ar_customer_balance_mv##Z UNION ALL SELECT * FROM XXEIS.XXWC_AR_CUSTOMER_BALANCE_MV##X';

            COMMIT;
        END;

        elapsed_time ('G table', g_start);
        --DROP ALL TEMP table to release the xxeis table space
        xxwc_eis_customer_balance_v2.drop_temp_table ('XXEIS', 'XXWC_AR_CUSTOMER_BALANCE_MV##X');
        xxwc_eis_customer_balance_v2.drop_temp_table ('XXEIS', 'XXWC_AR_CUSTOMER_BALANCE_MV##Z');
        xxwc_eis_customer_balance_v2.drop_temp_table ('XXEIS', 'XXWC_AR_CUST##AR_PAYMENT_SCH_Z');
        xxwc_eis_customer_balance_v2.drop_temp_table ('XXEIS', 'XXWC_AR_CUST##AR_PAYMENT_SCH_X');

        EXECUTE IMMEDIATE 'truncate table XXEIS.XXWC_AR_CUSTOMER_BALANCE_MV';

        --xxwc_eis_customer_balance_v2.alter_table_temp ('XXEIS', 'XXWC_AR_CUSTOMER_BALANCE_MV');  commented parallelism by Maha on 12/28/14

        EXECUTE IMMEDIATE
            'insert /*+append */ into XXEIS.XXWC_AR_CUSTOMER_BALANCE_MV select * from xxeis.xxwc_ar_customer_balance_mv##G';

        COMMIT;
        xxwc_eis_customer_balance_v2.drop_temp_table ('XXEIS', 'XXWC_AR_CUSTOMER_BALANCE_MV##G');
    EXCEPTION
        WHEN OTHERS
        THEN
            l_error_message :=
                   'xxwc_eis_customer_balance_v2 '
                || 'Error_Stack...'
                || DBMS_UTILITY.format_error_stack ()
                || ' Error_Backtrace...'
                || DBMS_UTILITY.format_error_backtrace ();

            write_log (l_error_message);
            /* xxcus_error_pkg.xxcus_error_main_api (
                p_called_from         => 'xxeis.xxwc_eis_customer_balance_v2'
               ,p_calling             => 'xxeis.xxwc_eis_customer_balance_v2'
               ,p_request_id          => fnd_global.conc_request_id
               ,p_ora_error_msg       => l_error_message
               ,p_error_desc          => 'Error running xxeis.xxwc_eis_customer_balance_v2'
               ,p_distribution_list   => pl_dflt_email
               ,p_module              => 'AR');*/
            p_retcode := 2;
            p_errbuf := l_error_message;
    END;
----**********************************************************

END;