CREATE OR REPLACE PACKAGE APPS.XXWC_IEX_COLL_IND_PKG AUTHID CURRENT_USER AS
 /**************************************************************************
    *
    * FUNCTION | PROCEDURE | CURSOR
    *  XXWC_IEX_COLL_IND_PKG
    *
    * DESCRIPTION
    *  Collections agent Metrics program
    *
    * PARAMETERS
    * ==========
    * NAME              TYPE     DESCRIPTION
   .* ----------------- -------- ---------------------------------------------
    *
    *
    *
    *
    * CALLED BY
    *   Which program, if any, calls this one
    * 
    * HISTORY
    * =======
    *
    * VERSION DATE        AUTHOR(S)       DESCRIPTION
    * ------- ----------- --------------- ------------------------------------
    * 1.0    01/26/2013   Maharajan S    TMS# 20131016-00469 Initial Creation
    * 1.1    06/22/2015   Maharajan S    TMS# 20150123-00197 AR - Customer Flag as indicator to auto apply credits
    *************************************************************************/
FUNCTION GET_PROFILE_NAME(p_cust_account_id IN NUMBER) 
RETURN VARCHAR2;
FUNCTION GET_ACCOUNT_STATUS(p_cust_account_id IN NUMBER) 
RETURN VARCHAR2;
FUNCTION GET_AVG_SALES(p_cust_account_id IN NUMBER) 
RETURN NUMBER;
FUNCTION GET_SITE_CLASS(p_cust_site_use_id IN NUMBER) 
RETURN VARCHAR2;
FUNCTION GET_PRIMARY_FLAG(p_cust_site_use_id IN NUMBER) 
RETURN VARCHAR2;
FUNCTION GET_JOB_INFO(p_cust_site_use_id IN NUMBER) 
RETURN VARCHAR2;
FUNCTION GET_PRELIM_DATE(p_cust_site_use_id IN NUMBER) 
RETURN VARCHAR2;
FUNCTION GET_PRELIM_NUMBER(p_cust_site_use_id IN NUMBER) 
RETURN VARCHAR2;
FUNCTION GET_TAX_CERT(p_cust_site_use_id IN NUMBER) 
RETURN VARCHAR2;
FUNCTION GET_JOB_START_DATE(p_cust_site_use_id IN NUMBER) 
RETURN DATE;
FUNCTION GET_JOINT_CHECK_AGREEMENT(p_cust_site_use_id IN NUMBER) 
RETURN VARCHAR2;
FUNCTION GET_OWNER_FIN_FLAG(p_cust_site_use_id IN NUMBER) 
RETURN VARCHAR2;
FUNCTION GET_CATEGORY(p_cust_site_use_id IN NUMBER) 
RETURN VARCHAR2;
FUNCTION GET_DSO_ACCT (p_cust_account_id IN NUMBER) 
RETURN NUMBER;
FUNCTION GET_ACC_MGR (p_cust_account_id IN NUMBER)  
RETURN VARCHAR2;
FUNCTION GET_GROSS_MARGIN (p_cust_account_id IN NUMBER)
RETURN NUMBER;
FUNCTION GET_GROSS_FISCAL (p_cust_account_id IN NUMBER)
RETURN NUMBER;
FUNCTION GET_SITE_GROSS_MARGIN (p_cust_site_use_id IN NUMBER)
RETURN NUMBER;
FUNCTION GET_SITE_GROSS_FISCAL(p_cust_site_use_id IN NUMBER)
RETURN NUMBER;
FUNCTION GET_AUTO_APPLY_FLAG(p_cust_account_id IN NUMBER)          --Added for ver# 1.1
RETURN VARCHAR2;
FUNCTION GET_AUTOAPPLY_FLAG_SITE (p_cust_site_use_id IN NUMBER)  --Added for ver# 1.1
RETURN VARCHAR2;
END;
/