/*
/*************************************************************************
  $Header TMS_20160315-00166_Interface_Trip_Stop_14810479.sql $
  Module Name: TMS_20160315-00166  Update the Unprocesed Shipping Transactions 
                for sales order # 19894982

  PURPOSE: Data fix to update the Delivery Details

  REVISIONS:
  Ver        Date         Author                Description
  ---------  -----------  ------------------    --------------------------
  1.0        15-MAR-2016  ANITHA MANI         TMS#20160315-00166 

**************************************************************************/ 

SET SERVEROUTPUT ON SIZE 100000;
SET VERIFY OFF;

DECLARE
BEGIN

DBMS_OUTPUT.put_line ('TMS: 20160315-00166    , Before Update');

UPDATE apps.wsh_delivery_details
SET released_status = 'D',
src_requested_quantity = 0,
requested_quantity = 0,
shipped_quantity = 0,
cycle_count_quantity = 0,
cancelled_quantity = 0,
subinventory = null,
locator_id = null,
lot_number = null,
revision = null,
inv_interfaced_flag = 'X',
oe_interfaced_flag = 'X'
WHERE delivery_detail_id =14810479;--Pass the delivery detail ID


DBMS_OUTPUT.put_line (
         'TMS: 20160315-00166 - number of records updated: '
      || SQL%ROWCOUNT);


update apps.wsh_delivery_assignments
set delivery_id = null,
parent_delivery_detail_id = null
where delivery_detail_id = 14810479;--Pass the delivery detail ID



DBMS_OUTPUT.put_line (
         'TMS: 20160315-00166 - number of records updated: '
      || SQL%ROWCOUNT);

COMMIT;

   DBMS_OUTPUT.put_line ('TMS: 20160315-00166   , End Update');
   
END;
/
