/***********************************************************************************************************************************************
   NAME:     TMS_20180920-00003_Datafix
   PURPOSE:  Data fix

   REVISIONS:
   Ver        Date        Author           Description
   ---------  ----------  ---------------  ----------------------------------------------------------------------------------------------------
   1.0        09/20/2018  Rakesh Patel     TMS#20180920-00003-Test data fix script
************************************************************************************************************************************************/
SET SERVEROUTPUT ON SIZE 1000000

BEGIN
   DBMS_OUTPUT.put_line ('Before Select');

   SELECT count(1) 
   FROM apps.oe_order_header_all 
   WHERE rownum <=10;

   DBMS_OUTPUT.put_line ('Records count -' || SQL%ROWCOUNT);

   COMMIT;

   EXCEPTION
   WHEN OTHERS
   THEN
      DBMS_OUTPUT.put_line ('Unable to get records count ' || SQLERRM);
	  ROLLBACK;
END;
/