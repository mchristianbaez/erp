/*
 TMS:  20170605-00270
 Date: 06/05/2017
 Notes:  Update table pn_space_assign_emp_all.start_date with 5/5/2006 instead of 12/31/4712 for emp_space_assign_id =998913.
*/
SET SERVEROUTPUT ON SIZE 1000000
DECLARE
 --
 n_emp_space_assign_id number :=998913;
 n_location_id number :=3507;
 n_loc number :=0;
 --
BEGIN --Main Processing...
   --
   n_loc :=101;
   --
   begin
    --
    savepoint start1;
    --
    update pn.pn_space_assign_emp_all
    set emp_assign_start_date ='05-MAY-06'
    where 1 =1
         and emp_space_assign_id =n_emp_space_assign_id
         and location_id =n_location_id
     ; 
    --
    dbms_output.put_line('Table pn.pn_space_assign_emp_all for emp_space_assign_id =  '||n_emp_space_assign_id||' updated with start_date of 5/5/06, rows updated = '||sql%rowcount);
    --
    n_loc :=102;
    --
    commit;
    --
   exception
    when others then
     --
     n_loc :=103;
     --
     dbms_output.put_line('@ n_loc ='||n_loc||', message ='||sqlerrm);
     --
     rollback to start1;
     --     
   end;
   --
   commit;
   --
EXCEPTION
   WHEN OTHERS
   THEN
      ROLLBACK;
      DBMS_OUTPUT.put_line (
         'TMS:  20170605-00270, Errors =' || SQLERRM);
END;
/