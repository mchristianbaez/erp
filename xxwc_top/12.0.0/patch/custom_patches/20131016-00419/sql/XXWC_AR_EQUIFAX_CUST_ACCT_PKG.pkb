CREATE OR REPLACE PACKAGE BODY APPS.XXWC_AR_EQUIFAX_CUST_ACCT_PKG
IS
   /******************************************************************************************************************************************************
        $Header XXWC_AR_EQUIFAX_CUST_ACCT_PKG $
        Module Name: XXWC_AR_EQUIFAX_CUST_ACCT_PKG.pkb

        PURPOSE:   EQUIFAX Project

        REVISIONS:
        Ver        Date        Author             Description
        ---------  ----------  --------------- ------------------------------------------------------------------------------------------------
        1.0        12/19/2017  P.Vamshidhar     TMS#20131016-00419 -Credit - Implement online credit application - Customer Accounts/Update File
   ******************************************************************************************************************************************************/

   g_err_callfrom      VARCHAR2 (1000) := 'XXWC_AR_EQUIFAX_CUST_ACCT_PKG';
   g_module            VARCHAR2 (100) := 'XXWC';
   g_distro_list       VARCHAR2 (80) := 'wc-itdevalerts-u1@hdsupply.com';
   gvc_debug_enabled   VARCHAR2 (1)
      := FND_PROFILE.VALUE ('XXWC_EQUIFAX_CUSTOMER_DEBUG_FLAG');

   PROCEDURE debug (P_RECORD_ID IN NUMBER, p_msg IN VARCHAR2)
   IS
      -- =====================================================================================================================================================
      -- Procedure: debug
      -- Purpose: debug procedure
      -- ====================================================================================================================================================
      --  REVISIONS:
      --   Ver        Date        Author               Description
      --  ---------  ---------- ---------------  ------------------------------------------------------------------------------------------------
      --  1.0        12/19/2017  P.Vamshidhar     TMS#20131016-00419 -Credit - Implement online credit application - Customer Accounts/Update File
      -- ====================================================================================================================================================
      PRAGMA AUTONOMOUS_TRANSACTION;
      l_sec   VARCHAR2 (1000);
   BEGIN
      IF gvc_debug_enabled = 'Y'
      THEN
         l_sec := ' Inserting Data into Log Table';

         INSERT
           INTO XXWC.XXWC_AR_EQUIFAX_LOG_TBL (LOG_SEQ_NO, RECORD_ID, LOG_MSG)
         VALUES (XXWC.XXWC_AR_EQUIFAX_LOG_SEQ.NEXTVAL, P_RECORD_ID, P_MSG);

         COMMIT;
      END IF;
   EXCEPTION
      WHEN OTHERS
      THEN
         xxcus_error_pkg.xxcus_error_main_api (
            p_called_from         => 'XXWC_AR_EQUIFAX_CUST_ACCT_PKG.DEBUG',
            p_calling             => l_sec,
            p_ora_error_msg       => SQLERRM,
            p_error_desc          =>    'Error Occured for '
                                     || p_record_id
                                     || 'record id in XXWC_AR_EQUIFAX_LOG_TBL.',
            p_distribution_list   => g_distro_list,
            p_module              => g_module);
   END;

   PROCEDURE EQUIFAX_CREATE_CUSTOMER (
      SalesforceId            IN            VARCHAR2,
      CustomerNumber          IN            VARCHAR2,
      BusinessName            IN            VARCHAR2,
      Phone                   IN            NUMBER,
      Fax                     IN            NUMBER,
      EmailAddress            IN            VARCHAR2,
      SalesRepId              IN            VARCHAR2,
      TypeOfBusiness          IN            VARCHAR2,
      ProfileClass            IN            VARCHAR2,
      CreditManager           IN            VARCHAR2,
      CreditClassification    IN            VARCHAR2,
      AccountStatus           IN            VARCHAR2,
      PredominantTrade        IN            VARCHAR2,
      Status                  IN            VARCHAR2,
      BillingAddress          IN            VARCHAR2,
      BillingCity             IN            VARCHAR2,
      BillingCounty           IN            VARCHAR2,
      BillingState            IN            VARCHAR2,
      BillingZipCode          IN            VARCHAR2,
      BillingCountry          IN            VARCHAR2,
      ShippingAddress         IN            VARCHAR2,
      ShippingCity            IN            VARCHAR2,
      ShippingCounty          IN            VARCHAR2,
      ShippingState           IN            VARCHAR2,
      ShippingZipCode         IN            VARCHAR2,
      ShippingCountry         IN            VARCHAR2,
      YardJobAccountProj      IN            VARCHAR2,
      CreditLimit             IN            VARCHAR2,
      DefaultJobCreditLimit   IN            VARCHAR2,
      CreditDecisioning       IN            VARCHAR2,
      APContactFirstName      IN            VARCHAR2,
      APContactLastName       IN            VARCHAR2,
      APPhone                 IN            VARCHAR2,
      APEmail                 IN            VARCHAR2,
      InvoiceEmailAddress     IN            VARCHAR2,
      PODEmailAddress         IN            VARCHAR2,
      SOAEmailAddress         IN            VARCHAR2,
      PurchaseOrderRequired   IN            VARCHAR2,
      DUNSNumber              IN            VARCHAR2,
      x_cust_account_id          OUT NOCOPY NUMBER,
      x_account_number           OUT NOCOPY VARCHAR2,
      x_party_id                 OUT NOCOPY NUMBER,
      x_party_number             OUT NOCOPY VARCHAR2,
      x_profile_id               OUT NOCOPY NUMBER,
      x_return_status            OUT NOCOPY VARCHAR2,
      x_msg_count                OUT NOCOPY NUMBER,
      x_msg_data                 OUT NOCOPY VARCHAR2)
   IS
      -- =====================================================================================================================================================
      -- Procedure: EQUIFAX_CREATE_CUSTOMER
      -- Purpose:  Main procedure
      -- ====================================================================================================================================================
      --  REVISIONS:
      --   Ver        Date        Author               Description
      --  ---------  ---------- ---------------  ------------------------------------------------------------------------------------------------
      --  1.0        12/19/2017  P.Vamshidhar     TMS#20131016-00419 -Credit - Implement online credit application - Customer Accounts/Update File
      -- ====================================================================================================================================================
      lvc_process_flag      VARCHAR2 (100);
      lvc_process_message   VARCHAR2 (100);
      ln_user_id            NUMBER := FND_GLOBAL.USER_ID;
      ln_new_party_flag     VARCHAR2 (1);
      ln_org_id             NUMBER := 162;
      ln_record_id          NUMBER;
      o_acct_number         HZ_CUST_ACCOUNTS.ACCOUNT_NUMBER%TYPE;
      o_cust_account_id     HZ_CUST_ACCOUNTS.CUST_ACCOUNT_ID%TYPE;
      o_party_id            HZ_PARTIES.PARTY_ID%TYPE;
      o_party_number        HZ_PARTIES.PARTY_NUMBER%TYPE;
      o_profile_id          HZ_CUSTOMER_PROFILES.CUST_ACCOUNT_PROFILE_ID%TYPE;
      o_ret_status          VARCHAR2 (100);
      o_msg_count           NUMBER;
      o_msg_data            VARCHAR2 (32767);
      lvc_procedure         VARCHAR2 (100) := 'EQUIFAX_CREATE_CUSTOMER';
      l_sec                 VARCHAR2 (32767);
      lvc_status            VARCHAR2 (1);
      ln_upd_flag           NUMBER;
	  ln_request_id         NUMBER;
	  --ln_user_id             NUMBER;
	  ln_resp_id             NUMBER;
      ln_resp_appl_id        NUMBER;
	  
   BEGIN
      l_sec := 'Procedure Start';
      mo_global.init ('AR');
      mo_global.set_org_context (ln_org_id, NULL, 'AR');
      fnd_global.set_nls_context ('AMERICAN');
      mo_global.set_policy_context ('S', ln_org_id);
	  
	  
	  BEGIN
	  
	  SELECT responsibility_id ,application_id INTO ln_resp_id, ln_resp_appl_id FROM APPS.fnd_responsibility_vl where responsibility_key='XXWC_CRE_ASSOC_CASH_APP_MGR';
      EXCEPTION 	  
	  WHEN OTHERS THEN
	  ln_resp_id :=NULL;
	  ln_resp_appl_id :=NULL;	  
	  END;
	  	  
	  fnd_global.APPS_INITIALIZE(user_id=>ln_user_id, 
                                 resp_id=>ln_resp_id, 
                                 resp_appl_id=>ln_resp_appl_id);
	  
	   
	  

      ln_record_id := XXWC.XXWC_AR_EQUIFAX_ACCT_REC_SEQ.NEXTVAL;

      l_sec := 'Insering inbound record into staging Table';

      debug (ln_record_id, l_sec);

      INSERT INTO XXWC.XXWC_AR_EQUIFAX_ACCT_WS_TBL (RECORD_ID,
                                                    SALES_FORCE_ID,
                                                    CUSTOMER_NUMBER,
                                                    BUSINESS_NAME,
                                                    BUSINESS_PHONE,
                                                    BUSINESS_FAX,
                                                    EMAIL_ADDRESS,
                                                    SALESREP_ID,
                                                    TYPE_OF_BUSINESS,
                                                    PROFILE_CLASS,
                                                    CREDIT_MANAGER,
                                                    CREDIT_CLASSIFICATION,
                                                    ACCOUNT_STATUS,
                                                    PREDOMINANT_TRADE,
                                                    STATUS,
                                                    BILLING_ADDRESS,
                                                    BILLING_CITY,
                                                    BILLING_COUNTY,
                                                    BILLING_STATE,
                                                    BILLING_ZIP_CODE,
                                                    BILLING_COUNTRY,
                                                    SHIPPING_ADDRESS,
                                                    SHIPPING_CITY,
                                                    SHIPPING_COUNTY,
                                                    SHIPPING_STATE,
                                                    SHIPPING_ZIP_CODE,
                                                    SHIPPING_COUNTRY,
                                                    YARD_JOB_ACCNT_PROJECT,
                                                    CREDIT_LIMIT,
                                                    DEFAULT_JOB_CREDIT_LIMIT,
                                                    CREDIT_DECISIONING,
                                                    AP_CONTACT_FIRST_NAME,
                                                    AP_CONTACT_LAST_NAME,
                                                    AP_PHONE,
                                                    AP_EMAIL,
                                                    INVOICE_EMAIL_ADDRESS,
                                                    POD_EMAIL_ADDRESS,
                                                    SOA_EMAIL_ADDRESS,
                                                    PURCHASE_ORDER_REQUIRED,
                                                    DUNSNUMBER,
                                                    PROCESS_FLAG,
                                                    PROCESS_MESSAGE,
                                                    CREATION_DATE,
                                                    CREATED_BY)
           VALUES (ln_record_id,
                   SalesforceId,
                   CustomerNumber,
                   UPPER (BusinessName),
                   REPLACE(REPLACE(REPLACE(Phone,'(',''),')',''),'-',''),
                   Fax,
                   EmailAddress,
                   SalesRepId,
                   TypeOfBusiness,
                   ProfileClass,
                   CreditManager,
                   CreditClassification,
                   AccountStatus,
                   PredominantTrade,
                   Status,
                   UPPER (BillingAddress),
                   UPPER (BillingCity),
                   UPPER (BillingCounty),
                   UPPER (BillingState),
                   BillingZipCode,
                   BillingCountry,
                   UPPER (ShippingAddress),
                   UPPER (ShippingCity),
                   UPPER (ShippingCounty),
                   UPPER (ShippingState),
                   ShippingZipCode,
                   ShippingCountry,
                   YardJobAccountProj,
                   CreditLimit,
                   DefaultJobCreditLimit,
                   CreditDecisioning,
                   APContactFirstName,
                   APContactLastName,
				   REPLACE(REPLACE(REPLACE(REPLACE(APPhone,'(',''),')',''),'-',''),' ',''),                   
                   APEmail,
                   InvoiceEmailAddress,
                   PODEmailAddress,
                   SOAEmailAddress,
                   PurchaseOrderRequired,
                   DUNSNumber,
                   lvc_process_flag,
                   lvc_process_message,
                   SYSDATE,
                   ln_user_id);

      debug (ln_record_id, 'Rows inserted ' || SQL%ROWCOUNT);

      COMMIT;

      IF CustomerNumber IS NULL
      THEN
         l_sec := 'Checking Mandatory Fields';
         debug (ln_record_id, l_sec);
         o_msg_count := NULL;
         o_msg_data := NULL;
         checking_mandatory_fields (ln_record_id, o_msg_count, o_msg_data);

         debug (ln_record_id,
                'Mandatory Validations completed ' || o_msg_data);

         IF O_MSG_DATA IS NOT NULL
         THEN
            l_sec := 'Mandatory Values missed in Inbound Data';
            o_acct_number := 'NULL';
            o_cust_account_id := 0;
            o_party_id := 0;
            o_party_number := 'NULL';
            o_profile_id := 0;
            o_ret_status := 'Error';
            GOTO LASTSTEP;
         END IF;
      END IF;

      l_sec := 'Validating Inbound Data';

      debug (ln_record_id, l_sec);

      o_msg_count := NULL;
      o_msg_data := NULL;
      validate_inbound_data (ln_record_id, o_msg_count, o_msg_data);

      debug (ln_record_id, 'Input Data Validation completed ' || o_msg_data);

      IF O_MSG_DATA IS NOT NULL
      THEN
         l_sec := 'Inbound Data Validation completed.';
         o_acct_number := 'NULL';
         o_cust_account_id := 0;
         o_party_id := 0;
         o_party_number := 'NULL';
         o_profile_id := 0;
         o_ret_status := 'Error';
         GOTO LASTSTEP;
      END IF;

      l_sec := 'Processing Inbound Data';

      IF NVL (CustomerNumber, 0) = 0
      THEN
         debug (ln_record_id, 'New Customer ' || l_sec);

         create_customer (p_record_id         => ln_record_id,
                          x_cust_account_id   => o_cust_account_id,
                          x_acct_number       => o_acct_number,
                          x_party_id          => o_party_id,
                          x_party_number      => o_party_number,
                          x_profile_id        => o_profile_id,
                          x_ret_status        => o_ret_status,
                          x_msg_count         => o_msg_count,
                          x_msg_data          => o_msg_data);

         debug (ln_record_id, 'New Customer Process completed ');
      ELSE
         l_sec := 'Checking Customer status in base table';
         debug (ln_record_id, l_sec);

         SELECT hca.status
           INTO lvc_status
           FROM hz_cust_accounts hca, xxwc.XXWC_AR_EQUIFAX_ACCT_WS_TBL xana
          WHERE     hca.Account_number = xana.customer_number
                AND xana.record_id = ln_record_id;

         IF lvc_status = 'A'
         THEN
            BEGIN
               SELECT hca.account_number,
                      hca.cust_account_id,
                      hca.party_id,
                      hp.party_number
                 INTO o_acct_number,
                      o_cust_account_id,
                      o_party_id,
                      o_party_number
                 FROM HZ_CUST_ACCOUNTS hca,
                      hz_parties hp,
                      xxwc.XXWC_AR_EQUIFAX_ACCT_WS_TBL xana
                WHERE     1 = 1
                      AND hp.party_id = hca.party_id
                      AND hca.ACCOUNT_NUMBER = xana.customer_number
                      AND xana.record_id = ln_record_id;
            EXCEPTION
               WHEN OTHERS
               THEN
                  debug (ln_record_id,
                         'Validating customer ' || SUBSTR (SQLERRM, 1, 250));
                  o_acct_number := 0;
                  o_cust_account_id := 0;
                  o_party_id := 0;
                  o_party_number := 0;
            END;

            o_profile_id := 0;
            o_ret_status := 'Error';
            o_msg_count := 1;
            o_msg_data := 'Customer Already Active in Oracle';

            GOTO LASTSTEP;
         END IF;

         BEGIN
            SELECT 1
              INTO ln_upd_flag
              FROM xxwc.XXWC_AR_EQUIFAX_ACCT_WS_TBL
             WHERE     (   BUSINESS_NAME IS NOT NULL
                        OR BUSINESS_PHONE IS NOT NULL
                        OR BUSINESS_FAX IS NOT NULL
                        OR EMAIL_ADDRESS IS NOT NULL
                        OR SALESREP_ID IS NOT NULL
                        OR TYPE_OF_BUSINESS IS NOT NULL
                        OR PROFILE_CLASS IS NOT NULL
                        OR CREDIT_MANAGER IS NOT NULL
                        OR ACCOUNT_STATUS IS NOT NULL
                        OR PREDOMINANT_TRADE IS NOT NULL
                        OR CREDIT_LIMIT IS NOT NULL
                        OR DEFAULT_JOB_CREDIT_LIMIT IS NOT NULL
                        OR CREDIT_DECISIONING IS NOT NULL)
                   AND RECORD_ID = ln_record_id
                   AND STATUS = 'Y';

            debug (ln_record_id,
                   'Update Customer ln_upd_flag -->' || ln_upd_flag);
         EXCEPTION
            WHEN OTHERS
            THEN
               debug (ln_record_id,
                      'Update Customer (Excep)' || SUBSTR (SQLERRM, 1, 250));
               ln_upd_flag := 0;
         END;



         IF NVL (ln_upd_flag, 0) > 0
         THEN
            update_customer (p_record_id         => ln_record_id,
                             x_cust_account_id   => o_cust_account_id,
                             x_acct_number       => o_acct_number,
                             x_party_id          => o_party_id,
                             x_party_number      => o_party_number,
                             x_profile_id        => o_profile_id,
                             x_ret_status        => o_ret_status,
                             x_msg_count         => o_msg_count,
                             x_msg_data          => o_msg_data);
            debug (ln_record_id, 'Customer Updation Process completed ');
         ELSE
            o_msg_data := 'No Columns flaged for update have values';
            debug (ln_record_id, 'Update Customer ' || o_msg_data);
         END IF;
      END IF;

      l_sec := 'Inbound Data Processed';

     <<LASTSTEP>>
      x_account_number := o_acct_number;
      x_cust_account_id := o_cust_account_id;
      x_party_id := o_party_id;
      x_party_number := o_party_number;
      x_profile_id := o_profile_id;
      x_return_status := o_ret_status;
      x_msg_count := o_msg_count;
      x_msg_data := NVL (o_msg_data, 'NULL');
      l_sec := 'Outbound Variable Initialized';

      debug (ln_record_id, 'Update Customer ' || l_sec);
	  
	  IF nvl(x_msg_count,0)>0 THEN
         UPDATE xxwc.XXWC_AR_EQUIFAX_ACCT_WS_TBL
            SET process_flag = 'E', PROCESS_MESSAGE = SUBSTR(x_msg_data,1,300)			
          WHERE record_id = ln_record_id;
	  END IF;
  	  
      ln_request_id :=fnd_request.submit_request
                      (
                        application      =>'AR'
                       ,program          =>'ARHDQMSS'
                       ,description      =>'WC DQM Sync Auto '
                       ,start_time       =>''
                       ,sub_request      =>FALSE              
                      );		 
		 
         COMMIT;		 
         debug (ln_record_id, 'ARHDQMSS Submitted:'||ln_request_id);	  	  
	  
   EXCEPTION
      WHEN OTHERS
      THEN
         debug (ln_record_id, 'Error Occurred ' || SUBSTR (SQLERRM, 1, 250));

         x_account_number := 0;
         x_cust_account_id := 0;
         x_party_id := 0;
         x_party_number := 0;
         x_profile_id := 0;
         x_msg_data := x_msg_data;
         x_return_status := 'Error';
         x_msg_count := 1;
         x_msg_data :=
               'Techinical Error Occurred, please contact HD White Cap IT Team '
            || SQLERRM;

         l_sec := ' Error occured for Record ' || ln_record_id;
         xxcus_error_pkg.xxcus_error_main_api (
            p_called_from         => g_err_callfrom || '.' || lvc_procedure,
            p_calling             => l_sec,
            p_ora_error_msg       => SQLERRM,
            p_error_desc          => NULL,
            p_distribution_list   => g_distro_list,
            p_module              => g_module);
   END;


   PROCEDURE checking_mandatory_fields (p_record_id   IN     NUMBER,
                                        x_msg_count      OUT NUMBER,
                                        x_ret_mess       OUT VARCHAR2)
   IS
      -- =====================================================================================================================================================
      -- Procedure: CHECKING_MANDATORY_FIELDS
      -- Purpose:  Checking Madatory fields
      -- ====================================================================================================================================================
      --  REVISIONS:
      --   Ver        Date        Author               Description
      --  ---------  ---------- ---------------  ------------------------------------------------------------------------------------------------
      --  1.0        12/19/2017  P.Vamshidhar     TMS#20131016-00419 -Credit - Implement online credit application - Customer Accounts/Update File
      -- ====================================================================================================================================================
      CURSOR CUR_MANDATE
      IS
         SELECT xrwt.business_name,
                xrwt.business_phone,
                xrwt.email_address,
                xrwt.salesrep_id,
                xrwt.type_of_business,
                xrwt.predominant_trade,
                xrwt.billing_address,
                xrwt.billing_city,
                xrwt.billing_county,
                xrwt.billing_state,
                xrwt.billing_country,
                xrwt.billing_zip_code,
                xrwt.shipping_address,
                xrwt.shipping_city,
                xrwt.shipping_county,
                xrwt.shipping_state,
                xrwt.shipping_country,
                xrwt.shipping_zip_code,
                xrwt.ap_contact_first_name,
                xrwt.ap_contact_last_name,
                xrwt.ap_phone,
                xrwt.ap_email,
                xrwt.invoice_email_address,
                xrwt.pod_email_address,
                xrwt.soa_email_address,
                xrwt.purchase_order_required,
                xrwt.credit_limit
           FROM XXWC.XXWC_AR_EQUIFAX_ACCT_WS_TBL xrwt
          WHERE RECORD_ID = P_RECORD_ID;

      o_ret_mess      VARCHAR2 (32767);
      ln_err_count    NUMBER;
      l_sec           VARCHAR2 (32767);
      lvc_procedure   VARCHAR2 (100) := 'CHECKING_MANDATORY_FIELDS';

      PROCEDURE err_mess (p_field_name IN VARCHAR2)
      IS
         lvc_msg   VARCHAR2 (100) := ' is Mandatory';
      BEGIN
         IF o_ret_mess IS NULL
         THEN
            o_ret_mess := p_field_name || lvc_msg;
            ln_err_count := 1;
         ELSE
            o_ret_mess := o_ret_mess || '*' || p_field_name || lvc_msg;
            ln_err_count := ln_err_count + 1;
         END IF;
      END;
   BEGIN
      l_sec := ' Validating Cursor values ';

      FOR REC_MANDATE IN CUR_MANDATE
      LOOP
         IF rec_mandate.business_name IS NULL
         THEN
            err_mess ('Business Name');
         END IF;

         IF rec_mandate.business_phone IS NULL
         THEN
            err_mess ('Business Phone');
         END IF;

         IF rec_mandate.email_address IS NULL
         THEN
            err_mess ('Eamil Address');
         END IF;

         IF rec_mandate.type_of_business IS NULL
         THEN
            err_mess ('Type of Business');
         END IF;

         IF rec_mandate.predominant_trade IS NULL
         THEN
            err_mess ('Predominant Trade');
         END IF;

         IF rec_mandate.billing_address IS NULL
         THEN
            err_mess ('Billing Address');
         END IF;

         IF rec_mandate.billing_city IS NULL
         THEN
            err_mess ('Billing City');
         END IF;

         IF rec_mandate.billing_county IS NULL
         THEN
            err_mess ('Billing County');
         END IF;

         IF rec_mandate.billing_state IS NULL
         THEN
            err_mess ('Billing State');
         END IF;

         IF rec_mandate.billing_country IS NULL
         THEN
            err_mess ('Billing Country');
         END IF;

         IF rec_mandate.billing_zip_code IS NULL
         THEN
            err_mess ('Billing Zip Code');
         END IF;


         IF rec_mandate.shipping_address IS NULL
         THEN
            err_mess ('Shipping Address');
         END IF;

         IF rec_mandate.shipping_city IS NULL
         THEN
            err_mess ('Shipping City');
         END IF;

         IF rec_mandate.shipping_county IS NULL
         THEN
            err_mess ('Shipping County');
         END IF;

         IF rec_mandate.shipping_state IS NULL
         THEN
            err_mess ('Shipping State');
         END IF;

         IF rec_mandate.shipping_country IS NULL
         THEN
            err_mess ('Shipping Country');
         END IF;

         IF rec_mandate.shipping_zip_code IS NULL
         THEN
            err_mess ('Shipping Zip Code');
         END IF;

         IF rec_mandate.credit_limit IS NULL
         THEN
            err_mess ('Credit Limit');
         END IF;

         IF rec_mandate.ap_contact_first_name IS NULL
         THEN
            err_mess ('AP Contact First Name');
         END IF;

         IF rec_mandate.ap_contact_last_name IS NULL
         THEN
            err_mess ('AP Contact Last Name');
         END IF;

         IF rec_mandate.ap_phone IS NULL
         THEN
            err_mess ('AP Phone');
         END IF;

         IF rec_mandate.ap_email IS NULL
         THEN
            err_mess ('AP Email');
         END IF;

         IF rec_mandate.invoice_email_address IS NULL
         THEN
            err_mess ('Invoice Email Address');
         END IF;

         IF rec_mandate.pod_email_address IS NULL
         THEN
            err_mess ('POD Email Address');
         END IF;

         IF rec_mandate.soa_email_address IS NULL
         THEN
            err_mess ('SOA Email Address');
         END IF;

         IF rec_mandate.purchase_order_required IS NULL
         THEN
            err_mess ('Purchase Order Required');
         END IF;
      END LOOP;

      l_sec := 'Cursor Values Validated ';
      debug (p_record_id, 'checking_mandatory_fields ' || x_ret_mess);

      x_ret_mess := o_ret_mess;
      x_msg_count := ln_err_count;
   EXCEPTION
      WHEN OTHERS
      THEN
         x_msg_count := 1;
         x_ret_mess :=
               'Techinical Error Occurred, please contact HD White Cap IT Team '
            || SQLERRM;

         debug (
            p_record_id,
               'checking_mandatory_fields (Exception)'
            || SUBSTR (SQLERRM, 1, 250));

         xxcus_error_pkg.xxcus_error_main_api (
            p_called_from         => g_err_callfrom || '.' || lvc_procedure,
            p_calling             => l_sec,
            p_ora_error_msg       => SQLERRM,
            p_error_desc          =>    'Error Occured for '
                                     || p_record_id
                                     || ' record id in XXWC_AR_EQUIFAX_ACCT_WS_TBL.',
            p_distribution_list   => g_distro_list,
            p_module              => g_module);
   END;



   PROCEDURE validate_inbound_data (p_record_id   IN     NUMBER,
                                    x_msg_count      OUT NUMBER,
                                    x_ret_mess       OUT VARCHAR2)
   IS
      -- =====================================================================================================================================================
      -- Procedure: VALIDATE_INBOUND_DATA
      -- Purpose:  Validating Inbound Data
      -- ====================================================================================================================================================
      --  REVISIONS:
      --   Ver        Date        Author               Description
      --  ---------  ---------- ---------------  ------------------------------------------------------------------------------------------------
      --  1.0        12/19/2017  P.Vamshidhar     TMS#20131016-00419 -Credit - Implement online credit application - Customer Accounts/Update File
      -- ====================================================================================================================================================
      CURSOR cur_validate
      IS
         SELECT customer_number,
                business_phone,
                business_fax,
                email_address business_email,
                salesrep_id,
                type_of_business,
                profile_class,
                credit_manager,
                credit_classification,
                account_status,
                predominant_trade,
                status,
                yard_job_accnt_project,
                credit_decisioning,
                dunsnumber,
                ap_phone,
                default_job_credit_limit,
                credit_limit
           FROM xxwc.XXWC_AR_EQUIFAX_ACCT_WS_TBL xrwt
          WHERE record_id = p_record_id;

      ln_valid_num    NUMBER;
      o_ret_mess      VARCHAR2 (32767);
      ln_err_count    NUMBER := 0;
      l_sec           VARCHAR2 (32767);
      ln_count        NUMBER;
      lvc_procedure   VARCHAR2 (100) := 'VALIDATE_INBOUND_DATA';

      PROCEDURE err_mess (p_field_name IN VARCHAR2)
      IS
      BEGIN
         IF o_ret_mess IS NULL
         THEN
            o_ret_mess := p_field_name;
            ln_err_count := 1;
         ELSE
            o_ret_mess := o_ret_mess || '*' || p_field_name;
            ln_err_count := ln_err_count + 1;
         END IF;
      END;
   BEGIN
      debug (p_record_id, 'validate_inbound_data Process Starts');



      FOR rec_validate IN cur_validate
      LOOP
         l_sec := 'Customer Number Validation';

         IF rec_validate.customer_number IS NOT NULL
         THEN
            BEGIN
               ln_valid_num := NULL;

               SELECT 1
                 INTO ln_valid_num
                 FROM DUAL
                WHERE REGEXP_LIKE (rec_validate.customer_number,
                                   '^[[:digit:]]+$');
            EXCEPTION
               WHEN OTHERS
               THEN
                  ln_valid_num := NULL;
            END;

            IF NVL (ln_valid_num, 0) = 0
            THEN
               err_mess (
                     rec_validate.customer_number
                  || ' Customer Number should be Numeric');
            END IF;

            IF ln_valid_num = 1
            THEN
               BEGIN
                  SELECT COUNT (1)
                    INTO ln_count
                    FROM hz_cust_accounts_all
                   WHERE account_number = rec_validate.customer_number;
               EXCEPTION
                  WHEN OTHERS
                  THEN
                     ln_count := 0;
               END;

               IF NVL (ln_count, 0) = 0
               THEN
                  err_mess (
                        rec_validate.customer_number
                     || ' Customer not exist in oracle');
               END IF;
            END IF;
         END IF;

         l_sec := 'Business Phone Number Validation';

         IF rec_validate.business_phone IS NOT NULL
         THEN
            BEGIN
               ln_valid_num := NULL;

               SELECT 1
                 INTO ln_valid_num
                 FROM DUAL
                WHERE REGEXP_LIKE (rec_validate.business_phone,
                                   '^[[:digit:]]+$');
            EXCEPTION
               WHEN OTHERS
               THEN
                  ln_valid_num := NULL;
            END;

            IF ln_valid_num IS NULL
            THEN
               err_mess (
                  rec_validate.business_phone || ' Invalid Business Phone');
            END IF;
         END IF;

         l_sec := 'Business Fax Number Validation';

         IF rec_validate.business_fax IS NOT NULL
         THEN
            BEGIN
               ln_valid_num := NULL;

               SELECT 1
                 INTO ln_valid_num
                 FROM DUAL
                WHERE REGEXP_LIKE (rec_validate.business_fax,
                                   '^[[:digit:]]+$');
            EXCEPTION
               WHEN OTHERS
               THEN
                  ln_valid_num := NULL;
            END;

            IF ln_valid_num IS NULL
            THEN
               err_mess (
                  rec_validate.business_fax || ' Invalid Business Fax');
            END IF;
         END IF;

         l_sec := 'Sales Repid Validation';

         IF rec_validate.salesrep_id IS NOT NULL
         THEN
            BEGIN
               ln_count := 0;

               SELECT COUNT (1)
                 INTO ln_count
                 FROM jtf_rs_salesreps_mo_v
                WHERE resource_id = rec_validate.salesrep_id;
            EXCEPTION
               WHEN OTHERS
               THEN
                  ln_count := 0;
            END;

            IF NVL (ln_count, 0) = 0
            THEN
               err_mess (
                     rec_validate.salesrep_id
                  || ' Sales Rep Does not exist in Oracle');
            END IF;
         END IF;

         l_sec := 'Type of Business Validation';

         IF rec_validate.type_of_business IS NOT NULL
         THEN
            BEGIN
               ln_count := NULL;

               SELECT COUNT (1)
                 INTO ln_count
                 FROM APPS.FND_LOOKUP_VALUES
                WHERE     LOOKUP_TYPE = 'CUSTOMER CLASS'
                      AND LOOKUP_CODE = rec_validate.type_of_business
                      AND enabled_flag = 'Y';
            EXCEPTION
               WHEN OTHERS
               THEN
                  ln_count := 0;
            END;

            IF NVL (ln_count, 0) = 0
            THEN
               err_mess (
                     rec_validate.type_of_business
                  || ' is invalid Type of Business');
            END IF;
         END IF;


         l_sec := 'Profile Class Validation';

         IF rec_validate.profile_class IS NOT NULL
         THEN
            BEGIN
               ln_count := NULL;

               SELECT COUNT (1)
                 INTO ln_count
                 FROM HZ_CUST_PROFILE_CLASSES
                WHERE NAME = rec_validate.profile_class;
            EXCEPTION
               WHEN OTHERS
               THEN
                  ln_count := 0;
            END;

            IF NVL (ln_count, 0) = 0
            THEN
               err_mess (
                  rec_validate.profile_class || ' is invalid Profile Class');
            END IF;
         END IF;

         l_sec := 'Credit Manager Validation';

         IF rec_validate.credit_manager IS NOT NULL
         THEN
            BEGIN
               ln_count := 0;

               SELECT COUNT (1)
                 INTO ln_count
                 FROM ar_collectors
                WHERE     collector_id = rec_validate.credit_manager
                      AND status = 'A';
            EXCEPTION
               WHEN OTHERS
               THEN
                  ln_count := 0;
            END;

            IF ln_count = 0
            THEN
               err_mess (
                  rec_validate.credit_manager || ' is invalid credit Manager');
            END IF;
         END IF;

         l_sec := 'Credit Classification Validation';


         IF rec_validate.credit_classification IS NOT NULL
         THEN
            BEGIN
               ln_count := 0;

               SELECT COUNT (1)
                 INTO ln_count
                 FROM ar_lookups
                WHERE     lookup_type LIKE 'AR_CMGT_CREDIT_CLASSIFICATION'
                      AND LOOKUP_CODE = rec_validate.credit_classification;
            EXCEPTION
               WHEN OTHERS
               THEN
                  ln_count := 0;
            END;

            IF NVL (ln_count, 0) = 0
            THEN
               err_mess (
                     rec_validate.credit_classification
                  || ' is invalid Credit Classification');
            END IF;
         END IF;

         l_sec := 'Predominant Trade Validation';

         IF rec_validate.predominant_trade IS NOT NULL
         THEN
            BEGIN
               SELECT COUNT (1)
                 INTO ln_count
                 FROM apps.fnd_flex_value_sets a, apps.fnd_flex_values b
                WHERE     a.flex_value_set_name LIKE
                             'XXCUS_PREDOMINANT_TRADE'
                      AND a.flex_value_set_id = b.flex_value_set_id
                      AND b.flex_value = rec_validate.predominant_trade
                      AND b.enabled_flag = 'Y';

               IF NVL (ln_count, 0) = 0
               THEN
                  err_mess (
                        rec_validate.predominant_trade
                     || ' is invalid predominant trade');
               END IF;
            END;
         END IF;

         l_sec := 'Status Validation';

         IF     rec_validate.STATUS NOT IN ('Y', 'N')
            AND rec_validate.STATUS IS NOT NULL
         THEN
            err_mess (rec_validate.status || ' is invalid Status');
         END IF;



         l_sec := 'Account Status Validation';

         IF rec_validate.account_status IS NOT NULL
         THEN
            BEGIN
               SELECT COUNT (1)
                 INTO LN_COUNT
                 FROM APPS.FND_LOOKUP_VALUES
                WHERE     LOOKUP_TYPE = 'ACCOUNT_STATUS'
                      AND lookup_code = rec_validate.account_status
                      AND enabled_flag = 'Y';
            EXCEPTION
               WHEN OTHERS
               THEN
                  LN_COUNT := 0;
            END;

            IF ln_count = 0
            THEN
               err_mess (
                  rec_validate.account_status || ' is Invalid Account Status');
            END IF;
         END IF;

         l_sec := 'duns number Validation';

         IF rec_validate.dunsnumber IS NOT NULL
         THEN
            BEGIN
               ln_valid_num := NULL;

               SELECT 1
                 INTO ln_valid_num
                 FROM DUAL
                WHERE REGEXP_LIKE (rec_validate.dunsnumber, '^[[:digit:]]+$');
            EXCEPTION
               WHEN OTHERS
               THEN
                  ln_valid_num := NULL;
            END;

            IF ln_valid_num IS NULL
            THEN
               err_mess (rec_validate.dunsnumber || ' Invalid DUNS Number');
            END IF;
         END IF;

         l_sec := 'AP phone Validation';

         IF rec_validate.ap_phone IS NOT NULL
         THEN
            BEGIN
               ln_valid_num := NULL;

               SELECT 1
                 INTO ln_valid_num
                 FROM DUAL
                WHERE REGEXP_LIKE (rec_validate.ap_phone, '^[[:digit:]]+$');
            EXCEPTION
               WHEN OTHERS
               THEN
                  ln_valid_num := NULL;
            END;

            IF ln_valid_num IS NULL
            THEN
               err_mess (rec_validate.ap_phone || ' Invalid AP Phone');
            END IF;
         END IF;

         l_sec := 'default job credit limit Validation';

         IF rec_validate.default_job_credit_limit IS NOT NULL
         THEN
            BEGIN
               ln_valid_num := NULL;

               SELECT 1
                 INTO ln_valid_num
                 FROM DUAL
                WHERE REGEXP_LIKE (rec_validate.default_job_credit_limit,
                                   '^[[:digit:]]+$');
            EXCEPTION
               WHEN OTHERS
               THEN
                  ln_valid_num := NULL;
            END;

            IF ln_valid_num IS NULL
            THEN
               err_mess (
                     rec_validate.default_job_credit_limit
                  || ' Invalid Default Job Credit Limit');
            END IF;
         END IF;

         l_sec := 'credit limit Validation';

         IF rec_validate.credit_limit IS NOT NULL
         THEN
            BEGIN
               ln_valid_num := NULL;

               SELECT 1
                 INTO ln_valid_num
                 FROM DUAL
                WHERE REGEXP_LIKE (rec_validate.credit_limit,
                                   '^[[:digit:]]+$');
            EXCEPTION
               WHEN OTHERS
               THEN
                  ln_valid_num := NULL;
            END;

            IF ln_valid_num IS NULL
            THEN
               err_mess (
                  rec_validate.credit_limit || ' Invalid Credit Limit');
            END IF;
         END IF;
      END LOOP;

      l_sec := 'Cursor Values Validated ';

      x_ret_mess := o_ret_mess;
      x_msg_count := ln_err_count;

      debug (p_record_id, 'validate_inbound_data ' || x_ret_mess);
   EXCEPTION
      WHEN OTHERS
      THEN
         debug (
            p_record_id,
            'validate_inbound_data (Exception) ' || SUBSTR (SQLERRM, 1, 250));

         x_ret_mess := o_ret_mess;
         x_msg_count := ln_err_count;
         x_msg_count := 1;
         x_ret_mess :=
               'Techinical Error Occurred, please contact HD White Cap IT Team '
            || SQLERRM;

         xxcus_error_pkg.xxcus_error_main_api (
            p_called_from         => g_err_callfrom || '.' || lvc_procedure,
            p_calling             => l_sec,
            p_ora_error_msg       => SQLERRM,
            p_error_desc          =>    'Error Occured for '
                                     || p_record_id
                                     || ' record id in XXWC_AR_EQUIFAX_ACCT_WS_TBL.',
            p_distribution_list   => g_distro_list,
            p_module              => g_module);
   END;

   PROCEDURE create_customer (P_RECORD_ID         IN     NUMBER,
                              x_cust_account_id      OUT NUMBER,
                              x_acct_number          OUT VARCHAR2,
                              x_party_id             OUT NUMBER,
                              x_party_number         OUT VARCHAR2,
                              x_profile_id           OUT NUMBER,
                              x_ret_status           OUT VARCHAR2,
                              x_msg_count            OUT NUMBER,
                              x_msg_data             OUT VARCHAR2)
   IS
      -- =====================================================================================================================================================
      -- Procedure: CREATE_CUSTOMER
      -- Purpose:  Creating customer
      -- ====================================================================================================================================================
      --  REVISIONS:
      --   Ver        Date        Author               Description
      --  ---------  ---------- ---------------  ------------------------------------------------------------------------------------------------
      --  1.0        12/19/2017  P.Vamshidhar     TMS#20131016-00419 -Credit - Implement online credit application - Customer Accounts/Update File
      -- ====================================================================================================================================================
      CURSOR CUR_CUST
      IS
         SELECT *
           FROM XXWC.XXWC_AR_EQUIFAX_ACCT_WS_TBL
          WHERE record_id = P_RECORD_ID;

      lv_country_code                  VARCHAR2 (10);
      lv_currency_code                 VARCHAR2 (10);
      porganizationrec                 hz_party_v2pub.organization_rec_type;
      l_api_name                       VARCHAR2 (30) := 'ONT_UI_ADD_CUSTOMER';
      o_party_id                       HZ_PARTIES.PARTY_ID%TYPE;
      o_ret_status                     VARCHAR2 (1000);
      o_msg_count                      NUMBER;
      o_msg_data                       VARCHAR2 (32767);
      o_party_number                   HZ_PARTIES.PARTY_NUMBER%TYPE;
      o_profile_id                     NUMBER;
      l_phone_exist                    VARCHAR2 (1);

      --contact variables
      pcontactpointrec                 hz_contact_point_v2pub.contact_point_rec_type;
      pphonerec                        hz_contact_point_v2pub.phone_rec_type;
      o_contact_pointid                NUMBER;
      o_return_msg                     VARCHAR2 (32767);
      pemailrec                        hz_contact_point_v2pub.email_rec_type;

      pcustproamtrec                   hz_customer_profile_v2pub.cust_profile_amt_rec_type;
      pcustaccountrec                  hz_cust_account_v2pub.cust_account_rec_type;
      pprofilerec                      hz_customer_profile_v2pub.customer_profile_rec_type;
      l_res_category                   jtf_rs_resource_extns_tl.category%TYPE;
      lv_primary_ship_to_flag          VARCHAR2 (10);
      o_cust_account_id                HZ_CUST_ACCOUNTS.CUST_ACCOUNT_ID%TYPE;
      o_cust_account_no                HZ_CUST_ACCOUNTS.ACCOUNT_NUMBER%TYPE;

      --location variable
      plocationrec                     hz_location_v2pub.location_rec_type;
      o_location_id                    HZ_LOCATIONS.LOCATION_ID%TYPE;

      --site variables
      ppartysiterec                    hz_party_site_v2pub.party_site_rec_type;

      --site account avriables
      pcustacctsiterec                 hz_cust_account_site_v2pub.cust_acct_site_rec_type;
      o_cust_acct_site_id              hz_cust_acct_sites.cust_acct_site_id%TYPE;
      o_party_site_no                  VARCHAR2 (100);

      --site use variables
      pcustacctsiteuserec              hz_cust_account_site_v2pub.cust_site_use_rec_type;
      pcustomerprofile                 hz_customer_profile_v2pub.customer_profile_rec_type;
      o_cust_acct_site_use_id          NUMBER;
      o_party_site_id                  hz_party_sites.party_site_id%TYPE;
      o_sprofile_id                    NUMBER;
      ln_org_id                        NUMBER := 162;
      o_cust_acct_profile_amt_id       NUMBER;
      p_object_version_number          NUMBER;
      -- Contact variables
      p_create_person_rec              hz_party_v2pub.person_rec_type;
      x_contact_party_id               NUMBER;
      x_contact_party_number           VARCHAR2 (2000);
      x_contact_profile_id             NUMBER;
      o_bill_to_site_use_id            NUMBER;

      -- contact org
      p_org_contact_rec                hz_party_contact_v2pub.org_contact_rec_type;
      x_org_contact_id                 NUMBER;
      x_party_rel_id                   NUMBER;
      x_rel_party_id                   NUMBER;
      x_rel_party_number               VARCHAR2 (2000);
      o_return_status                  VARCHAR2 (100);


      ln_credit_analyst                ar_collectors.resource_id%TYPE;
      ptelexrec                        hz_contact_point_v2pub.TELEX_REC_TYPE;
      pwebrec                          hz_contact_point_v2pub.WEB_REC_TYPE;
      pedirec                          hz_contact_point_v2pub.EDI_REC_TYPE;

      -- contact acct
      p_cr_cust_acc_role_rec           hz_cust_account_role_v2pub.cust_account_role_rec_type;
      x_cust_account_role_id           NUMBER;

      -- Role resp
      p_ROLE_RESPONSIBILITY_REC_TYPE   HZ_CUST_ACCOUNT_ROLE_V2PUB.role_responsibility_rec_type;
      o_respid                         NUMBER;
      l_acc_obj_version_number         NUMBER;
      o_contact_point_id               NUMBER;
      l_msg_data                       VARCHAR2 (2000);
      l_msg_index_out                  NUMBER;
      lvc_procedure                    VARCHAR2 (100) := 'CREATE_CUSTOMER';
      l_sec                            VARCHAR2 (32767);

      --Contact Person variables
      ln_person_party_id               HZ_PARTIES.party_id%TYPE;
      lvc_party_number                 HZ_PARTIES.PARTY_NUMBER%TYPE;
      ln_profile_id                    NUMBER;
      ln_bill_location_id              NUMBER;
      o_resp_id                        NUMBER;
      ln_ship_site_use_id              NUMBER;
      o_custacctprofileamtid           NUMBER;
      ol_profile_id                    NUMBER;



      PROCEDURE return_msg (in_msg_count     IN     NUMBER,
                            in_msg_data      IN     VARCHAR2,
                            out_return_msg      OUT VARCHAR2)
      IS
      BEGIN
         IF in_msg_count > 1
         THEN
            x_msg_count := in_msg_data;

            FOR I IN 1 .. in_msg_count
            LOOP
               Oe_Msg_Pub.get (p_msg_index       => i,
                               p_encoded         => Fnd_Api.G_FALSE,
                               p_data            => l_msg_data,
                               p_msg_index_out   => l_msg_index_out);
               out_return_msg :=
                  out_return_msg || ':' || SUBSTR (l_msg_data, 1, 200);
            END LOOP;
         ELSE
            out_return_msg := in_msg_data;
         END IF;
      END;
   BEGIN
      l_sec := 'Procedure Start';

      debug (p_record_id, l_sec);


      mo_global.init ('AR');
      mo_global.set_org_context (162, NULL, 'AR');
      fnd_global.set_nls_context ('AMERICAN');
      mo_global.set_policy_context ('S', 162);

      --====================================================================
      -- Fetching the Curency code
      --====================================================================

      l_sec := 'Deriving Currency code';

      BEGIN
         SELECT gsob.currency_code
           INTO lv_currency_code
           FROM hr_operating_units hou, gl_sets_of_books gsob
          WHERE     1 = 1
                AND gsob.set_of_books_id = hou.set_of_books_id
                AND hou.organization_id = ln_org_id;
      EXCEPTION
         WHEN OTHERS
         THEN
            lv_currency_code := 'USD';
      END;

      debug (p_record_id, l_sec || ' ' || lv_currency_code);

      --====================================================================
      -- Fetching the Country code
      --====================================================================

      l_sec := 'Deriving Country code';

      BEGIN
         SELECT SUBSTR (gsob.currency_code, 1, 2)
           INTO lv_country_code
           FROM hr_operating_units hou, gl_sets_of_books gsob
          WHERE     1 = 1
                AND gsob.set_of_books_id = hou.set_of_books_id
                AND hou.organization_id = ln_org_id;
      EXCEPTION
         WHEN OTHERS
         THEN
            lv_country_code := 'US';
      END;


      debug (p_record_id, l_sec || ' ' || lv_country_code);

      FOR REC_CUST IN CUR_CUST
      LOOP
         o_party_id := NULL;

         --====================================================================
         -- Create party if no party id passed
         --====================================================================
         --initialize the values
         o_party_id := NULL;
         porganizationrec := NULL;
         o_ret_status := NULL;
         o_msg_count := NULL;
         o_msg_data := NULL;

         l_sec := 'Party Creation Process Started';

         debug (p_record_id, l_sec);

         SAVEPOINT customer_record;


         -- creating the party organization record
         porganizationrec.organization_name := rec_cust.BUSINESS_NAME;
         porganizationrec.created_by_module := l_api_name;

         IF rec_cust.dunsnumber IS NOT NULL
         THEN
            porganizationrec.duns_number_c := rec_cust.dunsnumber;
         END IF;

         hz_party_v2pub.create_organization (
            p_init_msg_list      => 'T',
            p_organization_rec   => porganizationrec,
            x_return_status      => o_ret_status,
            x_msg_count          => o_msg_count,
            x_msg_data           => o_msg_data,
            x_party_id           => o_party_id,
            x_party_number       => o_party_number,
            x_profile_id         => o_profile_id);

         IF o_ret_status <> 'S'
         THEN
            return_msg (o_msg_count, o_msg_data, o_return_msg);
            x_msg_data := 'Party Creation: ' || o_return_msg;
            debug (p_record_id, x_msg_data);
            x_ret_status := o_ret_status;
            ROLLBACK TO customer_record;
            RETURN;
         END IF;

         l_sec :=
            'Party Creation Process Completed - API Status ' || o_ret_status;

         debug (p_record_id, l_sec);

         --====================================================================
         -- Create telephone record for the party
         --====================================================================
         IF rec_cust.business_phone IS NOT NULL
         THEN
            BEGIN
               SELECT 'Y'
                 INTO l_phone_exist
                 FROM apps.ar_phones_v
                WHERE     owner_table_name = 'HZ_PARTIES'
                      AND owner_table_id = o_party_id
                      AND phone_type = 'GEN'
                      AND area_code || phone_number = rec_cust.business_phone
                      AND ROWNUM = 1;
            EXCEPTION
               WHEN NO_DATA_FOUND
               THEN
                  l_phone_exist := 'N';
               WHEN OTHERS
               THEN
                  l_phone_exist := 'N';
            END;

            IF l_phone_exist = 'N'
            THEN
               pcontactpointrec := NULL;
               pcontactpointrec.contact_point_type := 'PHONE';
               pcontactpointrec.owner_table_name := 'HZ_PARTIES';
               pcontactpointrec.owner_table_id := o_party_id;
               pcontactpointrec.primary_flag := 'Y';
               pcontactpointrec.contact_point_purpose := 'BUSINESS';
               pphonerec.phone_country_code := NULL;
               pphonerec.phone_area_code :=
                  SUBSTR (rec_cust.business_phone, 1, 3);
               pphonerec.phone_number := SUBSTR (rec_cust.business_phone, 4);
               pphonerec.phone_extension := NULL;
               pphonerec.phone_line_type := 'GEN';
               pcontactpointrec.created_by_module := l_api_name;

               hz_contact_point_v2pub.create_contact_point (
                  p_init_msg_list       => 'T',
                  p_contact_point_rec   => pcontactpointrec,
                  p_phone_rec           => pphonerec,
                  x_contact_point_id    => o_contact_pointid,
                  x_return_status       => o_ret_status,
                  x_msg_count           => o_msg_count,
                  x_msg_data            => o_msg_data);

               IF o_ret_status <> 'S'
               THEN
                  return_msg (o_msg_count, o_msg_data, o_return_msg);
                  x_msg_data := 'Phone Creation: ' || o_return_msg;
                  debug (p_record_id, x_msg_data);
                  x_ret_status := o_ret_status;
                  ROLLBACK TO customer_record;
                  RETURN;
               END IF;
            END IF;
         END IF;

         l_sec :=
               'Business Phone Creation Process Completed - API Status '
            || o_ret_status;

         debug (p_record_id, l_sec);

         --====================================================================
         -- Create fax record for the party
         --====================================================================
         IF rec_cust.business_fax IS NOT NULL
         THEN
            BEGIN
               SELECT 'Y'
                 INTO l_phone_exist
                 FROM apps.ar_phones_v
                WHERE     owner_table_name = 'HZ_PARTIES'
                      AND owner_table_id = o_party_id
                      AND phone_type = 'FAX'
                      AND area_code || phone_number = rec_cust.business_fax
                      AND ROWNUM = 1;
            EXCEPTION
               WHEN NO_DATA_FOUND
               THEN
                  l_phone_exist := 'N';
               WHEN OTHERS
               THEN
                  l_phone_exist := 'N';
            END;

            IF l_phone_exist = 'N'
            THEN
               pcontactpointrec := NULL;
               pcontactpointrec.contact_point_type := 'PHONE';
               pcontactpointrec.owner_table_name := 'HZ_PARTIES';
               pcontactpointrec.owner_table_id := o_party_id;
               pcontactpointrec.primary_flag := 'N';
               pcontactpointrec.contact_point_purpose := 'BUSINESS';
               pphonerec.phone_area_code :=
                  SUBSTR (rec_cust.business_fax, 1, 3);
               pphonerec.phone_number := SUBSTR (rec_cust.business_fax, 4);
               pphonerec.phone_extension := NULL;
               pphonerec.phone_line_type := 'FAX';
               pcontactpointrec.created_by_module := l_api_name;

               hz_contact_point_v2pub.create_contact_point (
                  p_init_msg_list       => 'T',
                  p_contact_point_rec   => pcontactpointrec,
                  p_phone_rec           => pphonerec,
                  x_contact_point_id    => o_contact_pointid,
                  x_return_status       => o_ret_status,
                  x_msg_count           => o_msg_count,
                  x_msg_data            => o_msg_data);

               IF o_ret_status <> 'S'
               THEN
                  return_msg (o_msg_count, o_msg_data, o_return_msg);
                  x_msg_data := 'Fax Creation: ' || o_return_msg;
                  debug (p_record_id, x_msg_data);
                  x_ret_status := o_ret_status;
                  ROLLBACK TO customer_record;
                  RETURN;
               END IF;
            END IF;
         END IF;

         l_sec :=
               'Business Fax Creation Process Completed - API Status '
            || o_ret_status;


         debug (p_record_id, l_sec);



         --====================================================================
         -- Create email record for the party
         --====================================================================
         IF rec_cust.email_address IS NOT NULL
         THEN
            BEGIN
               SELECT 'Y'
                 INTO l_phone_exist
                 FROM apps.ar_phones_v
                WHERE     owner_table_name = 'HZ_PARTIES'
                      AND owner_table_id = o_party_id
                      AND CONTACT_POINT_PURPOSE = 'BUSINESS'
                      AND phone_type = 'EMAIL'
                      AND UPPER (EMAIL_ADDRESS) =
                             UPPER (rec_cust.email_address)
                      AND ROWNUM = 1;
            EXCEPTION
               WHEN NO_DATA_FOUND
               THEN
                  l_phone_exist := 'N';
               WHEN OTHERS
               THEN
                  l_phone_exist := 'N';
            END;

            IF l_phone_exist = 'N'
            THEN
               pcontactpointrec := NULL;
               pemailrec.email_address := rec_cust.email_address;
               pemailrec.email_format := 'MAILHTML';
               pcontactpointrec.contact_point_type := 'EMAIL';
               pcontactpointrec.owner_table_name := 'HZ_PARTIES';
               pcontactpointrec.owner_table_id := o_party_id;
               pcontactpointrec.primary_flag := 'Y';
               pcontactpointrec.contact_point_purpose := 'BUSINESS';
               pcontactpointrec.created_by_module := l_api_name;

               hz_contact_point_v2pub.create_contact_point (
                  p_init_msg_list       => 'T',
                  p_contact_point_rec   => pcontactpointrec,
                  p_email_rec           => pemailrec,
                  x_contact_point_id    => o_contact_pointid,
                  x_return_status       => o_ret_status,
                  x_msg_count           => o_msg_count,
                  x_msg_data            => o_msg_data);

               IF o_ret_status <> 'S'
               THEN
                  return_msg (o_msg_count, o_msg_data, o_return_msg);
                  x_msg_data := 'Email Creation: ' || o_return_msg;
                  debug (p_record_id, x_msg_data);
                  x_ret_status := o_ret_status;
                  ROLLBACK TO customer_record;
                  RETURN;
               END IF;
            END IF;
         END IF;

         l_sec :=
               'Email Address Creation Process Completed - API Status '
            || o_ret_status;

         debug (p_record_id, l_sec);

         --====================================================================
         -- Create Invoice email record for the party
         --====================================================================
         IF rec_cust.email_address IS NOT NULL
         THEN
            BEGIN
               SELECT 'Y'
                 INTO l_phone_exist
                 FROM apps.ar_phones_v
                WHERE     owner_table_name = 'HZ_PARTIES'
                      AND owner_table_id = o_party_id
                      AND phone_type = 'EMAIL'
                      AND CONTACT_POINT_PURPOSE = 'INVOICE'
                      AND UPPER (EMAIL_ADDRESS) =
                             UPPER (rec_cust.invoice_email_address)
                      AND ROWNUM = 1;
            EXCEPTION
               WHEN NO_DATA_FOUND
               THEN
                  l_phone_exist := 'N';
               WHEN OTHERS
               THEN
                  l_phone_exist := 'N';
            END;

            IF l_phone_exist = 'N'
            THEN
               pcontactpointrec := NULL;
               pemailrec.email_address := rec_cust.invoice_email_address;
               pemailrec.email_format := 'MAILHTML';
               pcontactpointrec.contact_point_type := 'EMAIL';
               pcontactpointrec.owner_table_name := 'HZ_PARTIES';
               pcontactpointrec.owner_table_id := o_party_id;
               pcontactpointrec.contact_point_purpose := 'INVOICE';
               pcontactpointrec.created_by_module := l_api_name;

               hz_contact_point_v2pub.create_contact_point (
                  p_init_msg_list       => 'T',
                  p_contact_point_rec   => pcontactpointrec,
                  p_email_rec           => pemailrec,
                  x_contact_point_id    => o_contact_pointid,
                  x_return_status       => o_ret_status,
                  x_msg_count           => o_msg_count,
                  x_msg_data            => o_msg_data);

               IF o_ret_status <> 'S'
               THEN
                  return_msg (o_msg_count, o_msg_data, o_return_msg);
                  x_msg_data := 'Invoice Email Creation: ' || o_return_msg;
                  debug (p_record_id, x_msg_data);
                  x_ret_status := o_ret_status;
                  ROLLBACK TO customer_record;
                  RETURN;
               END IF;
            END IF;
         END IF;

         l_sec :=
               'Invoice Email Address Creation Process Completed - API Status '
            || o_ret_status;

         debug (p_record_id, l_sec);

         --====================================================================
         -- Create customer account
         --====================================================================
         pcustproamtrec := NULL;
         pcustaccountrec := NULL;
         pprofilerec := NULL;
         lv_primary_ship_to_flag := NULL;
         o_ret_status := NULL;
         o_msg_count := NULL;
         o_msg_data := NULL;


         porganizationrec.party_rec.party_id := o_party_id;
         pcustaccountrec.account_name := rec_cust.business_name;
         pcustaccountrec.tax_code := NULL;
         pcustaccountrec.attribute_category := 'No';

         SELECT apps.hz_cust_accounts_s.NEXTVAL
           INTO pcustaccountrec.cust_account_id
           FROM DUAL;

         pcustaccountrec.orig_system_reference :=
            pcustaccountrec.cust_account_id;
         pcustaccountrec.account_established_date := TRUNC (SYSDATE);
         pcustaccountrec.created_by_module := l_api_name;
         pcustaccountrec.customer_type := 'R';
         pcustaccountrec.attribute4 := 'WCI';
         pcustaccountrec.attribute7 := 'N';

         IF rec_cust.yard_job_accnt_project IN ('Job')
         THEN
            pcustaccountrec.attribute_category := 'Yes';
            pcustaccountrec.attribute16 := 'JOBS ONLY';
         END IF;

         IF rec_cust.PREDOMINANT_TRADE IS NOT NULL
         THEN
            pcustaccountrec.attribute9 := rec_cust.PREDOMINANT_TRADE;
         ELSE
            pcustaccountrec.attribute9 := 'UNKNOWN';
         END IF;

         pcustaccountrec.attribute11 := 'Y';

         IF rec_cust.sales_force_id IS NOT NULL
         THEN
            pcustaccountrec.attribute6 := rec_cust.sales_force_id;
         END IF;

         IF rec_cust.status IS NOT NULL
         THEN
            IF rec_cust.status = 'Y'
            THEN
               pcustaccountrec.status := 'A';
            ELSIF rec_cust.status = 'N'
            THEN
               pcustaccountrec.status := 'I';
            END IF;
         ELSE
            pcustaccountrec.status := 'A';
         END IF;

         IF rec_cust.type_of_business IS NOT NULL
         THEN
            BEGIN
               SELECT LOOKUP_CODE
                 INTO pcustaccountrec.customer_class_code
                 FROM APPS.FND_LOOKUP_VALUES
                WHERE     LOOKUP_TYPE = 'CUSTOMER CLASS'
                      AND LOOKUP_CODE = rec_cust.type_of_business;
            EXCEPTION
               WHEN OTHERS
               THEN
                  pcustaccountrec.customer_class_code := NULL;
                  return_msg (1, o_msg_data, o_return_msg);
                  x_msg_data := 'Invalid Type of Business';
                  x_ret_status := o_ret_status;
                  ROLLBACK TO customer_record;
                  RETURN;
            END;
         ELSE
            pcustaccountrec.customer_class_code := 'UNKNOWN';
         END IF;

         BEGIN
            SELECT profile_class_id
              INTO pprofilerec.profile_class_id
              FROM HZ_CUST_PROFILE_CLASSES
             WHERE NAME = rec_cust.PROFILE_CLASS;
         EXCEPTION
            WHEN OTHERS
            THEN
               return_msg (o_msg_count, o_msg_data, o_return_msg);
               x_msg_data := 'Invalid Profile Class ' || o_return_msg;
               o_return_status := o_ret_status;
               ROLLBACK TO customer_record;
               RETURN;
         END;

         IF rec_cust.credit_decisioning IS NOT NULL
         THEN
            pprofilerec.credit_rating := rec_cust.credit_decisioning;
         ELSE
            pprofilerec.credit_rating := 'Y';
         END IF;

         BEGIN
            ln_credit_analyst := NULL;

            SELECT resource_id
              INTO ln_credit_analyst
              FROM ar_collectors
             WHERE collector_id = rec_cust.credit_manager;
         EXCEPTION
            WHEN OTHERS
            THEN
               ln_credit_analyst := NULL;
         END;

         IF ln_credit_analyst IS NOT NULL
         THEN
            pprofilerec.credit_analyst_id := ln_credit_analyst;
         END IF;

         pprofilerec.credit_checking := 'Y';
         pprofilerec.collector_id := rec_cust.credit_manager;

         IF rec_cust.account_status IS NOT NULL
         THEN
            BEGIN
               SELECT LOOKUP_CODE
                 INTO pprofilerec.account_status
                 FROM APPS.FND_LOOKUP_VALUES
                WHERE     LOOKUP_TYPE = 'ACCOUNT_STATUS'
                      AND LOOKUP_CODE = rec_cust.account_status;
            EXCEPTION
               WHEN OTHERS
               THEN
                  pprofilerec.account_status := NULL;
            END;
         ELSE
            pprofilerec.account_status := 'ACTIVE';
         END IF;

         BEGIN
            SELECT inv_remit_to_code
              INTO pprofilerec.attribute2
              FROM xxwc_ar_cm_remitcode_tbl
             WHERE area_code = SUBSTR (rec_cust.business_phone, 1, 3);
         EXCEPTION
            WHEN OTHERS
            THEN
               pprofilerec.attribute2 := 2;
         END;

         pprofilerec.attribute3 := 'Y';
         pprofilerec.tolerance := 0;

         IF rec_cust.credit_classification IS NOT NULL
         THEN
            SELECT LOOKUP_CODE
              INTO pprofilerec.credit_classification
              FROM ar_lookups
             WHERE     lookup_type LIKE 'AR_CMGT_CREDIT_CLASSIFICATION'
                   AND LOOKUP_CODE = rec_cust.credit_classification;
         ELSE
            pprofilerec.credit_classification := 'MODERATE';
         END IF;

         BEGIN
            SELECT jrre.category
              INTO l_res_category
              FROM apps.jtf_rs_salesreps_mo_v jrs,
                   jtf_rs_resource_extns_tl jrre
             WHERE     1 = 1
                   AND jrs.resource_id = jrre.resource_id
                   AND jrs.resource_id = rec_cust.salesrep_id
                   AND jrs.STATUS = 'A'
                   AND ROWNUM = 1;

            IF     l_res_category = 'OTHER'
               AND rec_cust.salesrep_id NOT IN (100000617, 100000650)
            THEN
               pcustaccountrec.sales_channel_code := 'BQU_UNKNOWN';
            ELSIF     l_res_category = 'OTHER'
                  AND rec_cust.salesrep_id IN (100000617, 100000650)
            THEN
               pcustaccountrec.sales_channel_code := 'ACCT_MGR';
            ELSIF l_res_category = 'EMPLOYEE'
            THEN
               pcustaccountrec.sales_channel_code := 'ACCT_MGR';
            ELSE
               pcustaccountrec.sales_channel_code := NULL;
            END IF;
         EXCEPTION
            WHEN OTHERS
            THEN
               pcustaccountrec.sales_channel_code := NULL;
         END;

         hz_cust_account_v2pub.create_cust_account (
            p_init_msg_list          => 'T',
            p_cust_account_rec       => pcustaccountrec,
            p_organization_rec       => porganizationrec,
            p_customer_profile_rec   => pprofilerec,
            p_create_profile_amt     => fnd_api.g_true,
            x_cust_account_id        => o_cust_account_id,
            x_account_number         => o_cust_account_no,
            x_party_id               => o_party_id,
            x_party_number           => o_party_number,
            x_profile_id             => o_profile_id,
            x_return_status          => o_ret_status,
            x_msg_count              => o_msg_count,
            x_msg_data               => o_msg_data);

         IF o_ret_status <> 'S'
         THEN
            return_msg (o_msg_count, o_msg_data, o_return_msg);
            x_msg_data := 'Account Creation: ' || o_return_msg;
            debug (p_record_id, x_msg_data);
            x_ret_status := o_ret_status;
            ROLLBACK TO customer_record;
            RETURN;
         END IF;

         l_sec :=
               'Customer Account Creation Process Completed - API Status '
            || o_ret_status;

         debug (p_record_id, l_sec);

         BEGIN
            SELECT CUST_ACCOUNT_PROFILE_ID
              INTO ol_profile_id
              FROM APPS.HZ_CUSTOMER_PROFILES
             WHERE     CUST_ACCOUNT_ID = o_cust_account_id
                   AND SITE_USE_ID IS NULL;
         EXCEPTION
            WHEN OTHERS
            THEN
               ol_profile_id := NULL;
         END;

         IF o_profile_id IS NULL
         THEN
            o_profile_id := ol_profile_id;
         END IF;


         --====================================================================
         --Update amount profile at Account Level
         --====================================================================
         pcustproamtrec := NULL;

         IF ol_profile_id IS NOT NULL
         THEN
            BEGIN
               SELECT cust_acct_profile_amt_id, object_version_number
                 INTO o_custacctprofileamtid, p_object_version_number
                 FROM apps.hz_cust_profile_amts
                WHERE     cust_account_profile_id = ol_profile_id
                      AND currency_code = lv_currency_code;
            EXCEPTION
               WHEN NO_DATA_FOUND
               THEN
                  o_custacctprofileamtid := NULL;
                  p_object_version_number := NULL;
            END;

            IF rec_cust.yard_job_accnt_project IN ('Project', 'Yard', 'Job')
            THEN
               pcustproamtrec.overall_credit_limit := 0;
            ELSIF rec_cust.yard_job_accnt_project IN ('Account')
            THEN
               pcustproamtrec.overall_credit_limit := rec_cust.credit_limit;
            ELSE
               pcustproamtrec.overall_credit_limit := 0;
            END IF;

            IF rec_cust.DEFAULT_JOB_CREDIT_LIMIT IS NOT NULL
            THEN
               pcustproamtrec.attribute1 := rec_cust.DEFAULT_JOB_CREDIT_LIMIT;
            END IF;

            IF o_custacctprofileamtid IS NOT NULL
            THEN
               --Update amount profile
               pcustproamtrec.cust_acct_profile_amt_id :=
                  o_custacctprofileamtid;
               pcustproamtrec.created_by_module := l_api_name;
               hz_customer_profile_v2pub.update_cust_profile_amt (
                  p_init_msg_list           => 'T',
                  p_cust_profile_amt_rec    => pcustproamtrec,
                  p_object_version_number   => p_object_version_number,
                  x_return_status           => o_ret_status,
                  x_msg_count               => o_msg_count,
                  x_msg_data                => o_msg_data);
            ELSE
               pcustproamtrec.cust_account_profile_id := ol_profile_id;
               pcustproamtrec.cust_account_id := o_cust_account_id;
               pcustproamtrec.currency_code := lv_currency_code;
               pcustproamtrec.created_by_module := l_api_name;
               hz_customer_profile_v2pub.create_cust_profile_amt (
                  p_init_msg_list              => 'T',
                  p_check_foreign_key          => 'T',
                  p_cust_profile_amt_rec       => pcustproamtrec,
                  x_cust_acct_profile_amt_id   => o_custacctprofileamtid,
                  x_return_status              => o_ret_status,
                  x_msg_count                  => o_msg_count,
                  x_msg_data                   => o_msg_data);
            END IF;

            IF o_ret_status <> 'S'
            THEN
               return_msg (o_msg_count, o_msg_data, o_return_msg);
               x_msg_data := 'Account Level Profile amounts: ' || o_return_msg;
               debug (p_record_id, x_msg_data);
               x_ret_status := o_ret_status;
               ROLLBACK TO customer_record;
               RETURN;
            END IF;
         END IF;



         --====================================================================
         -- create location for the primary bill to
         --====================================================================
         plocationrec := NULL;
         ppartysiterec := NULL;
         pcustacctsiterec := NULL;
         pcustacctsiteuserec := NULL;
         pcustomerprofile := NULL;
         o_ret_status := NULL;
         o_msg_count := NULL;
         o_msg_data := NULL;
         o_location_id := NULL;

         IF rec_cust.BILLING_COUNTRY IS NOT NULL
         THEN
            BEGIN
               SELECT TERRITORY_CODE
                 INTO plocationrec.country
                 FROM HR_TERRITORIES_V
                WHERE UPPER (TERRITORY_SHORT_NAME) =
                         UPPER (rec_cust.BILLING_COUNTRY);
            EXCEPTION
               WHEN OTHERS
               THEN
                  plocationrec.country := NULL;
                  return_msg (o_msg_count, o_msg_data, o_return_msg);
                  o_return_msg := 'Invalid Billing Country ' || o_return_msg;
                  o_return_status := o_ret_status;
                  ROLLBACK TO customer_record;
                  RETURN;
            END;
         ELSE
            plocationrec.country := NULL;
         END IF;

         plocationrec.postal_code := rec_cust.BILLING_ZIP_CODE;
         plocationrec.address1 := rec_cust.BILLING_ADDRESS;
         plocationrec.state := rec_cust.BILLING_STATE;
         plocationrec.city := rec_cust.BILLING_CITY;
         plocationrec.county := rec_cust.BILLING_COUNTY;
         plocationrec.created_by_module := l_api_name;
         plocationrec.sales_tax_geocode := NULL;

         hz_location_v2pub.create_location (
            p_init_msg_list   => 'T',
            p_location_rec    => plocationrec,
            x_location_id     => o_location_id,
            x_return_status   => o_ret_status,
            x_msg_count       => o_msg_count,
            x_msg_data        => o_msg_data);

         ln_bill_location_id := o_location_id;


         IF o_ret_status <> 'S'
         THEN
            return_msg (o_msg_count, o_msg_data, o_return_msg);
            x_msg_data := 'Location Creation: ' || o_return_msg;
            debug (p_record_id, x_msg_data);
            x_ret_status := o_ret_status;
            ROLLBACK TO customer_record;
            RETURN;
         END IF;

         l_sec :=
               'Location Account for Bill To Site Creation Process Completed - API Status'
            || o_ret_status;

         SAVEPOINT customer_record;

         --====================================================================
         -- Create Party Site for the primary bill to
         --====================================================================
         o_ret_status := NULL;
         o_msg_count := NULL;
         o_msg_data := NULL;
         o_party_site_id := NULL;
         o_party_site_no := NULL;
         o_sprofile_id := NULL;
         ppartysiterec.created_by_module := l_api_name;

         -- create a party site now
         ppartysiterec.party_id := o_party_id;
         ppartysiterec.location_id := o_location_id;
         ppartysiterec.identifying_address_flag := 'Y';

         hz_party_site_v2pub.create_party_site (
            p_init_msg_list       => 'T',
            p_party_site_rec      => ppartysiterec,
            x_party_site_id       => o_party_site_id,
            x_party_site_number   => o_party_site_no,
            x_return_status       => o_ret_status,
            x_msg_count           => o_msg_count,
            x_msg_data            => o_msg_data);

         IF o_ret_status <> 'S'
         THEN
            return_msg (o_msg_count, o_msg_data, o_return_msg);
            x_msg_data := 'Party Site Creation: ' || o_return_msg;
            debug (p_record_id, x_msg_data);
            x_ret_status := o_ret_status;
            ROLLBACK TO customer_record;
            RETURN;
         END IF;

         l_sec :=
               'Party Site Creation Process Completed - API Status '
            || o_ret_status;

         debug (p_record_id, l_sec);

         SAVEPOINT customer_record;

         --====================================================================
         -- Create cust acct site
         --====================================================================
         pcustacctsiterec := NULL;
         o_ret_status := NULL;
         o_msg_count := NULL;
         o_msg_data := NULL;
         o_cust_acct_site_id := NULL;
         pcustacctsiterec.cust_account_id := o_cust_account_id;
         pcustacctsiterec.party_site_id := o_party_site_id;
         pcustacctsiterec.created_by_module := l_api_name;
         pcustacctsiterec.org_id := ln_org_id;
         pcustacctsiterec.attribute3 :=
            SUBSTR (NVL(rec_cust.purchase_order_required,'N'), 1, 1);
         pcustacctsiterec.attribute_category := 'No';
         pcustacctsiterec.attribute1 := '2';
         pcustacctsiterec.attribute16 := 'N';
         pcustacctsiterec.attribute15 := 'None';
         pcustacctsiterec.attribute14 := 'N';

         BEGIN
            SELECT CASE
                      WHEN customer_class_code = 'COMMERCIAL'
                      THEN
                         'PUBLIC'
                      WHEN customer_class_code = 'GOVERNMENT'
                      THEN
                         'PRIVATE'
                      WHEN customer_class_code = 'NON-CONTRACTOR/OTHER'
                      THEN
                         'PUBLIC'
                      WHEN customer_class_code = 'RESELLER'
                      THEN
                         'PUBLIC'
                      WHEN customer_class_code = 'RESIDENTIAL_CONTRACTOR'
                      THEN
                         'RES'
                      WHEN customer_class_code = 'UNKNOWN'
                      THEN
                         'PUBLIC'
                      ELSE
                         NULL
                   END
              INTO pcustacctsiterec.customer_category_code
              FROM hz_cust_accounts
             WHERE cust_account_id = o_cust_account_id;
         EXCEPTION
            WHEN OTHERS
            THEN
               pcustacctsiterec.customer_category_code := NULL;
         END;

         hz_cust_account_site_v2pub.create_cust_acct_site (
            p_init_msg_list        => 'T',
            p_cust_acct_site_rec   => pcustacctsiterec,
            x_cust_acct_site_id    => o_cust_acct_site_id,
            x_return_status        => o_ret_status,
            x_msg_count            => o_msg_count,
            x_msg_data             => o_msg_data);

         IF o_ret_status <> 'S'
         THEN
            return_msg (o_msg_count, o_msg_data, o_return_msg);
            x_msg_data := 'Party Site Creation: ' || o_return_msg;
            debug (p_record_id, x_msg_data);
            x_ret_status := o_ret_status;
            ROLLBACK TO customer_record;
            RETURN;
         END IF;

         l_sec :=
               'Cust Acct Site Creation Process Completed - API Status '
            || o_ret_status;
         debug (p_record_id, l_sec);

         SAVEPOINT customer_record;
         --====================================================================
         -- Create site use for primary bill to
         --====================================================================
         pcustacctsiteuserec := NULL;
         o_ret_status := NULL;
         o_msg_count := NULL;
         o_msg_data := NULL;
         o_cust_acct_site_use_id := NULL;

         pcustacctsiteuserec.site_use_code := 'BILL_TO';
         pcustacctsiteuserec.cust_acct_site_id := o_cust_acct_site_id;

         pcustacctsiteuserec.attribute1 := 'MSTR';
         pcustacctsiteuserec.primary_flag := 'Y';
         pcustacctsiteuserec.location := rec_cust.business_name;
         pcustacctsiteuserec.created_by_module := l_api_name;
         pcustomerprofile.credit_checking := 'Y';

         BEGIN
            SELECT inv_remit_to_code
              INTO pcustomerprofile.attribute2
              FROM xxwc_ar_cm_remitcode_tbl
             WHERE area_code = SUBSTR (rec_cust.business_phone, 1, 3);
         EXCEPTION
            WHEN OTHERS
            THEN
               pcustomerprofile.attribute2 := 2;
         END;

         pcustomerprofile.attribute3 := 'Y';
         pcustomerprofile.collector_id := rec_cust.CREDIT_MANAGER;

         IF ln_credit_analyst IS NOT NULL
         THEN
            pcustomerprofile.credit_analyst_id := ln_credit_analyst;
         END IF;

         IF rec_cust.account_status IS NOT NULL
         THEN
            BEGIN
               SELECT LOOKUP_CODE
                 INTO pcustomerprofile.account_status
                 FROM APPS.FND_LOOKUP_VALUES
                WHERE     LOOKUP_TYPE = 'ACCOUNT_STATUS'
                      AND LOOKUP_CODE = rec_cust.account_status;
            EXCEPTION
               WHEN OTHERS
               THEN
                  pcustomerprofile.account_status := NULL;
            END;
         ELSE
            pcustomerprofile.account_status := 'ACTIVE';
         END IF;

         IF rec_cust.credit_decisioning IS NOT NULL
         THEN
            pcustomerprofile.credit_rating := rec_cust.credit_decisioning;
         ELSE
            pcustomerprofile.credit_rating := 'Y';
         END IF;

         pcustomerprofile.tolerance := 0;

         IF rec_cust.salesrep_id IS NOT NULL
         THEN
            BEGIN
               SELECT SALESREP_ID
                 INTO pcustacctsiteuserec.primary_salesrep_id
                 FROM jtf_rs_salesreps_mo_v
                WHERE resource_id = rec_cust.salesrep_id;
            EXCEPTION
               WHEN OTHERS
               THEN
                  pcustacctsiteuserec.primary_salesrep_id := NULL;
            END;
         END IF;
		 
         IF rec_cust.credit_classification IS NOT NULL
         THEN
            SELECT LOOKUP_CODE
              INTO pcustomerprofile.credit_classification
              FROM ar_lookups
             WHERE     lookup_type LIKE 'AR_CMGT_CREDIT_CLASSIFICATION'
                   AND LOOKUP_CODE = rec_cust.credit_classification;
         ELSE
            pcustomerprofile.credit_classification := 'MODERATE';
         END IF;	 

         BEGIN
            SELECT profile_class_id
              INTO pcustomerprofile.profile_class_id
              FROM HZ_CUST_PROFILE_CLASSES
             WHERE NAME = rec_cust.PROFILE_CLASS;
         EXCEPTION
            WHEN OTHERS
            THEN
               return_msg (o_msg_count, o_msg_data, o_return_msg);
               x_msg_data := 'Invalid Profile Class -  ' || o_return_msg;
               x_ret_status := o_ret_status;
               ROLLBACK TO customer_record;
               RETURN;
         END;

         hz_cust_account_site_v2pub.create_cust_site_use (
            p_init_msg_list          => 'T',
            p_cust_site_use_rec      => pcustacctsiteuserec,
            p_customer_profile_rec   => pcustomerprofile,
            p_create_profile         => 'T',
            p_create_profile_amt     => 'T',
            x_site_use_id            => o_cust_acct_site_use_id,
            x_return_status          => o_ret_status,
            x_msg_count              => o_msg_count,
            x_msg_data               => o_msg_data);


         IF o_ret_status <> 'S'
         THEN
            return_msg (o_msg_count, o_msg_data, o_return_msg);
            x_msg_data := 'Account Site Creation(Bill To): ' || o_return_msg;
            debug (p_record_id, x_msg_data);
            x_ret_status := o_ret_status;
            ROLLBACK TO customer_record;
            RETURN;
         END IF;

         l_sec :=
               'Cust Acct Site Use Creation Process Completed - API Status '
            || o_ret_status;

         debug (p_record_id, l_sec);

         SAVEPOINT customer_record;

         --====================================================================
         -- create site profile amount
         --====================================================================
         pcustproamtrec := NULL;
         o_sprofile_id := NULL;
         o_ret_status := NULL;
         o_msg_count := NULL;
         o_msg_data := NULL;


         BEGIN
            SELECT cust_account_profile_id
              INTO o_sprofile_id
              FROM apps.hz_customer_profiles
             WHERE     cust_account_id = o_cust_account_id
                   AND site_use_id = o_cust_acct_site_use_id;
         EXCEPTION
            WHEN NO_DATA_FOUND
            THEN
               o_sprofile_id := NULL;
         END;


         IF o_sprofile_id IS NOT NULL
         THEN
            BEGIN
               SELECT cust_acct_profile_amt_id, object_version_number
                 INTO o_cust_acct_profile_amt_id, p_object_version_number
                 FROM apps.hz_cust_profile_amts
                WHERE     cust_account_profile_id = o_sprofile_id
                      AND currency_code = lv_currency_code;
            EXCEPTION
               WHEN NO_DATA_FOUND
               THEN
                  o_cust_acct_profile_amt_id := NULL;
                  p_object_version_number := NULL;
            END;

            pcustproamtrec.overall_credit_limit := 0;

            IF o_cust_acct_profile_amt_id IS NOT NULL
            THEN
               --Update amount profile
               pcustproamtrec.cust_acct_profile_amt_id :=
                  o_cust_acct_profile_amt_id;
               pcustproamtrec.created_by_module := l_api_name;

               hz_customer_profile_v2pub.update_cust_profile_amt (
                  p_init_msg_list           => 'T',
                  p_cust_profile_amt_rec    => pcustproamtrec,
                  p_object_version_number   => p_object_version_number,
                  x_return_status           => o_ret_status,
                  x_msg_count               => o_msg_count,
                  x_msg_data                => o_msg_data);
            ELSE
               --Create Profile Amount
               pcustproamtrec.cust_account_profile_id := o_sprofile_id;
               pcustproamtrec.cust_account_id := o_cust_account_id;
               pcustproamtrec.site_use_id := o_cust_acct_site_use_id;
               pcustproamtrec.currency_code := lv_currency_code;
               pcustproamtrec.created_by_module := l_api_name;
               hz_customer_profile_v2pub.create_cust_profile_amt (
                  p_init_msg_list              => 'T',
                  p_check_foreign_key          => 'T',
                  p_cust_profile_amt_rec       => pcustproamtrec,
                  x_cust_acct_profile_amt_id   => o_cust_acct_profile_amt_id,
                  x_return_status              => o_ret_status,
                  x_msg_count                  => o_msg_count,
                  x_msg_data                   => o_msg_data);
            END IF;

            IF o_ret_status <> 'S'
            THEN
               return_msg (o_msg_count, o_msg_data, o_return_msg);
               x_msg_data := 'Profile Amount Creation: ' || o_return_msg;
               debug (p_record_id, x_msg_data);
               x_ret_status := o_ret_status;
               ROLLBACK TO customer_record;
               RETURN;
            END IF;

            l_sec :=
                  'Profile Amount Creation Process Completed - API Status '
               || o_ret_status;
            debug (p_record_id, l_sec);

            SAVEPOINT customer_record;
            --====================================================================
            -- Create usage rules
            --====================================================================
            hz_credit_usages_cascade_pkg.cascade_credit_usage_rules (
               p_cust_acct_profile_amt_id   => o_cust_acct_profile_amt_id,
               p_cust_profile_id            => o_sprofile_id,
               p_profile_class_amt_id       => NULL,
               p_profile_class_id           => NULL,
               x_return_status              => o_ret_status,
               x_msg_count                  => o_msg_count,
               x_msg_data                   => o_msg_data);

            IF o_ret_status <> 'S'
            THEN
               return_msg (o_msg_count, o_msg_data, o_return_msg);
               x_msg_data := 'Profile Amounts Usage Rules: ' || o_return_msg;
               debug (p_record_id, x_msg_data);
               x_ret_status := o_ret_status;
               ROLLBACK TO customer_record;
               RETURN;
            END IF;
         END IF;

         l_sec :=
               'Profile Amt uage rules Creation Process Completed - API Status '
            || o_ret_status;

         debug (p_record_id, l_sec);
         --====================================================================
         -- create location for the Ship_to
         --====================================================================
         plocationrec := NULL;
         ppartysiterec := NULL;
         pcustacctsiterec := NULL;
         pcustacctsiteuserec := NULL;
         pcustomerprofile := NULL;
         o_ret_status := NULL;
         o_msg_count := NULL;
         o_msg_data := NULL;

         o_location_id := NULL;

         BEGIN
            SELECT territory_code
              INTO plocationrec.country
              FROM hr_territories_v
             WHERE UPPER (territory_short_name) =
                      UPPER (rec_cust.shipping_country);
         EXCEPTION
            WHEN OTHERS
            THEN
               plocationrec.country := NULL;
               return_msg (o_msg_count, o_msg_data, o_return_msg);
               x_msg_data := 'Invalid Shipping Country ' || o_return_msg;
               x_ret_status := o_ret_status;
               ROLLBACK TO customer_record;
               RETURN;
         END;

         plocationrec.postal_code := rec_cust.shipping_zip_code;
         plocationrec.address1 := rec_cust.shipping_address;
         plocationrec.state := rec_cust.shipping_STATE;
         plocationrec.city := rec_cust.shipping_CITY;
         plocationrec.county := rec_cust.shipping_COUNTY;
         plocationrec.created_by_module := l_api_name;
         plocationrec.sales_tax_geocode := NULL;

         hz_location_v2pub.create_location (
            p_init_msg_list   => 'T',
            p_location_rec    => plocationrec,
            x_location_id     => o_location_id,
            x_return_status   => o_ret_status,
            x_msg_count       => o_msg_count,
            x_msg_data        => o_msg_data);


         IF o_ret_status <> 'S'
         THEN
            return_msg (o_msg_count, o_msg_data, o_return_msg);
            x_msg_data := 'Location Creation: ' || o_return_msg;
            debug (p_record_id, x_msg_data);
            x_ret_status := o_ret_status;
            ROLLBACK TO customer_record;
            RETURN;
         END IF;

         l_sec :=
               'Location for Ship to Creation Process Completed - API Status '
            || o_ret_status;

         debug (p_record_id, l_sec);

         --====================================================================
         -- Create Party Site for the Ship to
         --====================================================================
         o_ret_status := NULL;
         o_msg_count := NULL;
         o_msg_data := NULL;
         o_party_site_id := NULL;
         o_party_site_no := NULL;
         o_sprofile_id := NULL;
         ppartysiterec.created_by_module := l_api_name;
         ppartysiterec.party_id := o_party_id;
         ppartysiterec.location_id := o_location_id;
         ppartysiterec.identifying_address_flag := 'Y';

         hz_party_site_v2pub.create_party_site (
            p_init_msg_list       => 'T',
            p_party_site_rec      => ppartysiterec,
            x_party_site_id       => o_party_site_id,
            x_party_site_number   => o_party_site_no,
            x_return_status       => o_ret_status,
            x_msg_count           => o_msg_count,
            x_msg_data            => o_msg_data);

         IF o_ret_status <> 'S'
         THEN
            return_msg (o_msg_count, o_msg_data, o_return_msg);
            x_msg_data := 'Ship to Party Site Creation: ' || o_return_msg;
            debug (p_record_id, x_msg_data);
            x_ret_status := o_ret_status;
            ROLLBACK TO customer_record;
            RETURN;
         END IF;

         l_sec :=
               'Party Site for Ship to Creation Process Completed - API Status '
            || o_ret_status;
         debug (p_record_id, l_sec);

         --====================================================================
         -- Create Shipto cust acct site
         --====================================================================
         o_ret_status := NULL;
         o_msg_count := NULL;
         o_msg_data := NULL;
         o_cust_acct_site_id := NULL;
         pcustacctsiterec.cust_account_id := o_cust_account_id;
         pcustacctsiterec.party_site_id := o_party_site_id;
         pcustacctsiterec.created_by_module := l_api_name;
         pcustacctsiterec.org_id := ln_org_id;
         pcustacctsiterec.attribute3 := SUBSTR (NVL(rec_cust.purchase_order_required,'N'), 1, 1);
         --pcustacctsiterec.attribute3 := 'N';
         pcustacctsiterec.attribute_category := 'No';
         pcustacctsiterec.attribute1 := '2';
         pcustacctsiterec.attribute16 := 'N';
         pcustacctsiterec.attribute15 := 'None';
         pcustacctsiterec.attribute14 := 'N';

         BEGIN
            SELECT CASE
                      WHEN customer_class_code = 'COMMERCIAL'
                      THEN
                         'PUBLIC'
                      WHEN customer_class_code = 'GOVERNMENT'
                      THEN
                         'PRIVATE'
                      WHEN customer_class_code = 'NON-CONTRACTOR/OTHER'
                      THEN
                         'PUBLIC'
                      WHEN customer_class_code = 'RESELLER'
                      THEN
                         'PUBLIC'
                      WHEN customer_class_code = 'RESIDENTIAL_CONTRACTOR'
                      THEN
                         'RES'
                      WHEN customer_class_code = 'UNKNOWN'
                      THEN
                         'PUBLIC'
                      ELSE
                         NULL
                   END
              INTO pcustacctsiterec.customer_category_code
              FROM hz_cust_accounts
             WHERE cust_account_id = o_cust_account_id;
         EXCEPTION
            WHEN OTHERS
            THEN
               pcustacctsiterec.customer_category_code := NULL;
         END;

         hz_cust_account_site_v2pub.create_cust_acct_site (
            p_init_msg_list        => 'T',
            p_cust_acct_site_rec   => pcustacctsiterec,
            x_cust_acct_site_id    => o_cust_acct_site_id,
            x_return_status        => o_ret_status,
            x_msg_count            => o_msg_count,
            x_msg_data             => o_msg_data);

         IF o_ret_status <> 'S'
         THEN
            return_msg (o_msg_count, o_msg_data, o_return_msg);
            x_msg_data := 'Party Site Creation: ' || o_return_msg;
            debug (p_record_id, x_msg_data);
            x_ret_status := o_ret_status;
            ROLLBACK TO customer_record;
            RETURN;
         END IF;

         l_sec :=
               'Cust Acct Site for Ship to Creation Process Completed - API Status '
            || o_ret_status;

         debug (p_record_id, l_sec);
         --====================================================================
         -- Create site use (Bill to) for Ship to
         --====================================================================
         pcustacctsiteuserec := NULL;
         o_ret_status := NULL;
         o_msg_count := NULL;
         o_msg_data := NULL;
         o_cust_acct_site_use_id := NULL;

         pcustacctsiteuserec.site_use_code := 'BILL_TO';
         pcustacctsiteuserec.cust_acct_site_id := o_cust_acct_site_id;
         pcustacctsiteuserec.created_by_module := l_api_name;

         IF rec_cust.salesrep_id IS NOT NULL
         THEN
            BEGIN
               SELECT SALESREP_ID
                 INTO pcustacctsiteuserec.primary_salesrep_id
                 FROM jtf_rs_salesreps_mo_v
                WHERE resource_id = rec_cust.salesrep_id;
            EXCEPTION
               WHEN OTHERS
               THEN
                  pcustacctsiteuserec.primary_salesrep_id := NULL;
            END;
         END IF;

         pcustomerprofile.credit_checking := 'Y';

         BEGIN
            SELECT inv_remit_to_code
              INTO pcustomerprofile.attribute2
              FROM xxwc_ar_cm_remitcode_tbl
             WHERE area_code = SUBSTR (rec_cust.business_phone, 1, 3);
         EXCEPTION
            WHEN OTHERS
            THEN
               pcustomerprofile.attribute2 := 2;
         END;

         pcustomerprofile.attribute3 := 'Y';
         pcustomerprofile.collector_id := rec_cust.credit_manager;

         IF ln_credit_analyst IS NOT NULL
         THEN
            pcustomerprofile.credit_analyst_id := ln_credit_analyst;
         END IF;

         IF rec_cust.credit_decisioning IS NOT NULL
         THEN
            pcustomerprofile.credit_rating := rec_cust.credit_decisioning;
         ELSE
            pcustomerprofile.credit_rating := 'Y';
         END IF;

         IF rec_cust.account_status IS NOT NULL
         THEN
            BEGIN
               SELECT LOOKUP_CODE
                 INTO pcustomerprofile.account_status
                 FROM APPS.FND_LOOKUP_VALUES
                WHERE     LOOKUP_TYPE = 'ACCOUNT_STATUS'
                      AND LOOKUP_CODE = rec_cust.account_status;
            EXCEPTION
               WHEN OTHERS
               THEN
                  pcustomerprofile.account_status := NULL;
            END;
         ELSE
            pcustomerprofile.account_status := 'ACTIVE';
         END IF;

         pcustomerprofile.tolerance := 0;

         IF rec_cust.yard_job_accnt_project IS NOT NULL
         THEN
            IF rec_cust.yard_job_accnt_project IN ('Account', 'Yard', 'Job')
            THEN
               pcustacctsiteuserec.location :=
                  SUBSTR (rec_cust.business_name, 1, 30) || '/YARD';
               pcustacctsiteuserec.attribute1 := 'YARD';
            ELSIF rec_cust.yard_job_accnt_project IN ('Project')
            THEN
               pcustacctsiteuserec.location :=
                  SUBSTR (rec_cust.business_name, 1, 30) || '/JOB';
               pcustacctsiteuserec.attribute1 := 'JOB';
            ELSE
               pcustacctsiteuserec.location :=
                     SUBSTR (rec_cust.business_name, 1, 30)
                  || '/'
                  || UPPER (rec_cust.yard_job_accnt_project);
               pcustacctsiteuserec.attribute1 :=
                  rec_cust.yard_job_accnt_project;
            END IF;

            IF rec_cust.yard_job_accnt_project IN ('Job')
            THEN
               pcustomerprofile.credit_hold := 'Y';
            END IF;
         ELSE
            pcustacctsiteuserec.location :=
               SUBSTR (rec_cust.business_name, 1, 30);
         END IF;
		 

         IF rec_cust.credit_classification IS NOT NULL
         THEN
            SELECT LOOKUP_CODE
              INTO pcustomerprofile.credit_classification
              FROM ar_lookups
             WHERE     lookup_type LIKE 'AR_CMGT_CREDIT_CLASSIFICATION'
                   AND LOOKUP_CODE = rec_cust.credit_classification;
         ELSE
            pcustomerprofile.credit_classification := 'MODERATE';
         END IF;		 

         BEGIN
            SELECT profile_class_id
              INTO pcustomerprofile.profile_class_id
              FROM HZ_CUST_PROFILE_CLASSES
             WHERE NAME = rec_cust.PROFILE_CLASS;
         EXCEPTION
            WHEN OTHERS
            THEN
               return_msg (o_msg_count, o_msg_data, o_return_msg);
               x_msg_data := 'Invalid Profile Class ' || o_return_msg;
               o_return_status := o_ret_status;
               ROLLBACK TO customer_record;
               RETURN;
         END;


         hz_cust_account_site_v2pub.create_cust_site_use (
            p_init_msg_list          => 'T',
            p_cust_site_use_rec      => pcustacctsiteuserec,
            p_customer_profile_rec   => pcustomerprofile,
            p_create_profile         => 'T',
            p_create_profile_amt     => 'T',
            x_site_use_id            => o_bill_to_site_use_id,
            x_return_status          => o_ret_status,
            x_msg_count              => o_msg_count,
            x_msg_data               => o_msg_data);


         IF o_ret_status <> 'S'
         THEN
            return_msg (o_msg_count, o_msg_data, o_return_msg);
            x_msg_data := 'Account Site Creation(BillTo1): ' || o_return_msg;
            debug (p_record_id, x_msg_data);
            x_ret_status := o_ret_status;
            ROLLBACK TO customer_record;
            RETURN;
         END IF;

         l_sec :=
               'Cust Acct Site Use (Billto) for Ship to Creation Process Completed - API Status '
            || o_ret_status;

         debug (p_record_id, l_sec);


         --====================================================================
         -- create Shipto site profile amount
         --====================================================================
         pcustproamtrec := NULL;
         o_sprofile_id := NULL;
         o_cust_acct_profile_amt_id := NULL;

         BEGIN
            SELECT cust_account_profile_id
              INTO o_sprofile_id
              FROM apps.hz_customer_profiles
             WHERE     cust_account_id = o_cust_account_id
                   AND site_use_id = o_bill_to_site_use_id;
         EXCEPTION
            WHEN NO_DATA_FOUND
            THEN
               o_sprofile_id := NULL;
         END;

         IF o_sprofile_id IS NOT NULL
         THEN
            BEGIN
               SELECT cust_acct_profile_amt_id, object_version_number
                 INTO o_cust_acct_profile_amt_id, p_object_version_number
                 FROM apps.hz_cust_profile_amts
                WHERE     cust_account_profile_id = o_sprofile_id
                      AND currency_code = lv_currency_code;
            EXCEPTION
               WHEN NO_DATA_FOUND
               THEN
                  o_cust_acct_profile_amt_id := NULL;
                  p_object_version_number := NULL;
            END;

            IF rec_cust.yard_job_accnt_project IN ('Project', 'Yard')
            THEN
               pcustproamtrec.overall_credit_limit := rec_cust.credit_limit;
            ELSIF rec_cust.yard_job_accnt_project IN ('Account')
            THEN
               pcustproamtrec.overall_credit_limit := 0;
            ELSE
               pcustproamtrec.overall_credit_limit := 0;
            END IF;



            IF o_cust_acct_profile_amt_id IS NOT NULL
            THEN
               --Update amount profile
               pcustproamtrec.cust_acct_profile_amt_id :=
                  o_cust_acct_profile_amt_id;
               pcustproamtrec.created_by_module := l_api_name;
               hz_customer_profile_v2pub.update_cust_profile_amt (
                  p_init_msg_list           => 'T',
                  p_cust_profile_amt_rec    => pcustproamtrec,
                  p_object_version_number   => p_object_version_number,
                  x_return_status           => o_ret_status,
                  x_msg_count               => o_msg_count,
                  x_msg_data                => o_msg_data);
            ELSE
               --Create Profile Amount
               pcustproamtrec.cust_account_profile_id := o_sprofile_id;
               pcustproamtrec.cust_account_id := o_cust_account_id;
               pcustproamtrec.site_use_id := o_cust_acct_site_use_id;
               pcustproamtrec.currency_code := lv_currency_code;
               pcustproamtrec.created_by_module := l_api_name;
               hz_customer_profile_v2pub.create_cust_profile_amt (
                  p_init_msg_list              => 'T',
                  p_check_foreign_key          => 'T',
                  p_cust_profile_amt_rec       => pcustproamtrec,
                  x_cust_acct_profile_amt_id   => o_cust_acct_profile_amt_id,
                  x_return_status              => o_ret_status,
                  x_msg_count                  => o_msg_count,
                  x_msg_data                   => o_msg_data);
            END IF;

            IF o_ret_status <> 'S'
            THEN
               return_msg (o_msg_count, o_msg_data, o_return_msg);
               x_msg_data :=
                  'Shipto Profile Amount Creation: ' || o_return_msg;
               debug (p_record_id, x_msg_data);
               x_ret_status := o_ret_status;
               ROLLBACK TO customer_record;
               RETURN;
            END IF;
         END IF;


         --====================================================================
         -- Create site use for Ship to
         --====================================================================

         pcustacctsiteuserec := NULL;
         o_ret_status := NULL;
         o_msg_count := NULL;
         o_msg_data := NULL;
         o_cust_acct_site_use_id := NULL;

         pcustacctsiteuserec.site_use_code := 'SHIP_TO';
         pcustacctsiteuserec.cust_acct_site_id := o_cust_acct_site_id;
		 
         BEGIN
            SELECT inv_remit_to_code
              INTO pcustomerprofile.attribute2
              FROM xxwc_ar_cm_remitcode_tbl
             WHERE area_code = SUBSTR (rec_cust.business_phone, 1, 3);
         EXCEPTION
            WHEN OTHERS
            THEN
               pcustomerprofile.attribute2 := 2;
         END;

         pcustomerprofile.attribute3 := 'Y';
         pcustacctsiteuserec.bill_to_site_use_id := o_bill_to_site_use_id;

         pcustacctsiteuserec.primary_flag := 'Y';
         pcustacctsiteuserec.created_by_module := l_api_name;

         IF rec_cust.salesrep_id IS NOT NULL
         THEN
            BEGIN
               SELECT SALESREP_ID
                 INTO pcustacctsiteuserec.primary_salesrep_id
                 FROM jtf_rs_salesreps_mo_v
                WHERE resource_id = rec_cust.salesrep_id;
            EXCEPTION
               WHEN OTHERS
               THEN
                  pcustacctsiteuserec.primary_salesrep_id := NULL;
            END;
         END IF;

         pcustomerprofile.credit_checking := 'Y';
         pcustomerprofile.collector_id := rec_cust.credit_manager;

         IF ln_credit_analyst IS NOT NULL
         THEN
            pcustomerprofile.credit_analyst_id := ln_credit_analyst;
         END IF;


         IF rec_cust.account_status IS NOT NULL
         THEN
            BEGIN
               SELECT LOOKUP_CODE
                 INTO pcustomerprofile.account_status
                 FROM APPS.FND_LOOKUP_VALUES
                WHERE     LOOKUP_TYPE = 'ACCOUNT_STATUS'
                      AND LOOKUP_CODE = rec_cust.account_status;
            EXCEPTION
               WHEN OTHERS
               THEN
                  pcustomerprofile.account_status := NULL;
            END;
         ELSE
            pcustomerprofile.account_status := 'ACTIVE';
         END IF;

         pcustomerprofile.tolerance := 0;


         IF rec_cust.yard_job_accnt_project IS NOT NULL
         THEN
            IF rec_cust.yard_job_accnt_project IN ('Account', 'Yard', 'Job')
            THEN
               pcustacctsiteuserec.location :=
                  SUBSTR (rec_cust.business_name, 1, 30) || '/YARD';
               pcustacctsiteuserec.attribute1 := 'YARD';
            ELSIF rec_cust.yard_job_accnt_project IN ('Project')
            THEN
               pcustacctsiteuserec.location :=
                  SUBSTR (rec_cust.business_name, 1, 30) || '/JOB';
               pcustacctsiteuserec.attribute1 := 'JOB';
            ELSE
               pcustacctsiteuserec.location :=
                     SUBSTR (rec_cust.business_name, 1, 30)
                  || '/'
                  || rec_cust.yard_job_accnt_project;
               pcustacctsiteuserec.attribute1 :=
                  rec_cust.yard_job_accnt_project;
            END IF;

            IF rec_cust.yard_job_accnt_project IN ('Job')
            THEN
               pcustomerprofile.credit_hold := 'Y';
            END IF;
         ELSE
            pcustacctsiteuserec.location :=
               SUBSTR (rec_cust.business_name, 1, 30);
         END IF;

         IF rec_cust.credit_classification IS NOT NULL
         THEN
            SELECT LOOKUP_CODE
              INTO pcustomerprofile.credit_classification
              FROM ar_lookups
             WHERE     lookup_type LIKE 'AR_CMGT_CREDIT_CLASSIFICATION'
                   AND LOOKUP_CODE = rec_cust.credit_classification;
         ELSE
            pcustomerprofile.credit_classification := 'MODERATE';
         END IF;		 

         IF rec_cust.profile_class IS NOT NULL
         THEN
            BEGIN
               SELECT profile_class_id
                 INTO pcustomerprofile.profile_class_id
                 FROM HZ_CUST_PROFILE_CLASSES
                WHERE NAME = rec_cust.PROFILE_CLASS;
            EXCEPTION
               WHEN OTHERS
               THEN
                  pcustomerprofile.profile_class_id := NULL;
            END;
         ELSE
            BEGIN
               SELECT profile_class_id
                 INTO pcustomerprofile.profile_class_id
                 FROM HZ_CUST_PROFILE_CLASSES
                WHERE NAME = 'Contractor - Non Key';
            EXCEPTION
               WHEN OTHERS
               THEN
                  pcustomerprofile.profile_class_id := NULL;
            END;
         END IF;

         hz_cust_account_site_v2pub.create_cust_site_use (
            p_init_msg_list          => 'T',
            p_cust_site_use_rec      => pcustacctsiteuserec,
            p_customer_profile_rec   => pcustomerprofile,
            p_create_profile         => 'T',
            p_create_profile_amt     => 'T',
            x_site_use_id            => o_cust_acct_site_use_id,
            x_return_status          => o_ret_status,
            x_msg_count              => o_msg_count,
            x_msg_data               => o_msg_data);

         ln_ship_site_use_id := o_cust_acct_site_use_id;

         IF o_ret_status <> 'S'
         THEN
            return_msg (o_msg_count, o_msg_data, o_return_msg);
            x_msg_data := 'Account Site Creation(SHIPTO): ' || o_return_msg;
            debug (p_record_id, x_msg_data);
            x_ret_status := o_ret_status;
            ROLLBACK TO customer_record;
            RETURN;
         END IF;

         l_sec :=
               'Cust Acct Site Use for Ship to Creation Process Completed - API Status '
            || o_ret_status;
         debug (p_record_id, l_sec);

         --====================================================================
         -- create Shipto site profile amount
         --====================================================================
         pcustproamtrec := NULL;
         o_sprofile_id := NULL;
         o_cust_acct_profile_amt_id := NULL;

         BEGIN
            SELECT cust_account_profile_id
              INTO o_sprofile_id
              FROM apps.hz_customer_profiles
             WHERE     cust_account_id = o_cust_account_id
                   AND site_use_id = o_cust_acct_site_use_id;
         EXCEPTION
            WHEN NO_DATA_FOUND
            THEN
               o_sprofile_id := NULL;
         END;

         IF o_sprofile_id IS NOT NULL
         THEN
            BEGIN
               SELECT cust_acct_profile_amt_id, object_version_number
                 INTO o_cust_acct_profile_amt_id, p_object_version_number
                 FROM apps.hz_cust_profile_amts
                WHERE     cust_account_profile_id = o_sprofile_id
                      AND currency_code = lv_currency_code;
            EXCEPTION
               WHEN NO_DATA_FOUND
               THEN
                  o_cust_acct_profile_amt_id := NULL;
                  p_object_version_number := NULL;
            END;

            IF rec_cust.yard_job_accnt_project IN ('Project', 'Yard')
            THEN
               pcustproamtrec.overall_credit_limit := rec_cust.credit_limit;
            ELSIF rec_cust.yard_job_accnt_project IN ('Account')
            THEN
               pcustproamtrec.overall_credit_limit := 0;
            ELSE
               pcustproamtrec.overall_credit_limit := 0;
            END IF;



            IF o_cust_acct_profile_amt_id IS NOT NULL
            THEN
               --Update amount profile
               pcustproamtrec.cust_acct_profile_amt_id :=
                  o_cust_acct_profile_amt_id;
               pcustproamtrec.created_by_module := l_api_name;
               hz_customer_profile_v2pub.update_cust_profile_amt (
                  p_init_msg_list           => 'T',
                  p_cust_profile_amt_rec    => pcustproamtrec,
                  p_object_version_number   => p_object_version_number,
                  x_return_status           => o_ret_status,
                  x_msg_count               => o_msg_count,
                  x_msg_data                => o_msg_data);
            ELSE
               --Create Profile Amount
               pcustproamtrec.cust_account_profile_id := o_sprofile_id;
               pcustproamtrec.cust_account_id := o_cust_account_id;
               pcustproamtrec.site_use_id := o_cust_acct_site_use_id;
               pcustproamtrec.currency_code := lv_currency_code;
               pcustproamtrec.created_by_module := l_api_name;
               hz_customer_profile_v2pub.create_cust_profile_amt (
                  p_init_msg_list              => 'T',
                  p_check_foreign_key          => 'T',
                  p_cust_profile_amt_rec       => pcustproamtrec,
                  x_cust_acct_profile_amt_id   => o_cust_acct_profile_amt_id,
                  x_return_status              => o_ret_status,
                  x_msg_count                  => o_msg_count,
                  x_msg_data                   => o_msg_data);
            END IF;

            IF o_ret_status <> 'S'
            THEN
               return_msg (o_msg_count, o_msg_data, o_return_msg);
               x_msg_data :=
                  'Shipto Profile Amount Creation: ' || o_return_msg;
               debug (p_record_id, x_msg_data);
               x_ret_status := o_ret_status;
               ROLLBACK TO customer_record;
               RETURN;
            END IF;
         END IF;

         l_sec :=
               'Profile Amts for Ship to Creation Process Completed - API Status '
            || o_ret_status;
         debug (p_record_id, l_sec);

         --====================================================================
         -- Create Shipto usage rules
         --====================================================================
         o_msg_count := NULL;
         o_msg_data := NULL;
         o_ret_status := NULL;

         hz_credit_usages_cascade_pkg.cascade_credit_usage_rules (
            p_cust_acct_profile_amt_id   => o_cust_acct_profile_amt_id,
            p_cust_profile_id            => o_sprofile_id,
            p_profile_class_amt_id       => NULL,
            p_profile_class_id           => NULL,
            x_return_status              => o_ret_status,
            x_msg_count                  => o_msg_count,
            x_msg_data                   => o_msg_data);

         IF o_ret_status <> 'S'
         THEN
            return_msg (o_msg_count, o_msg_data, o_return_msg);
            x_msg_data :=
               'Shipto cascade_credit_usage_rules: ' || o_return_msg;
            debug (p_record_id, x_msg_data);
            x_ret_status := o_ret_status;
            ROLLBACK TO customer_record;
            RETURN;
         END IF;

         l_sec :=
               'Profile Amts usage Rules for Ship to Creation Process Completed - API Status '
            || o_ret_status;
         debug (p_record_id, l_sec);


         SAVEPOINT customer_record;
         --====================================================================
         -- Creating contact person
         --====================================================================
         p_create_person_rec := NULL;
         x_contact_party_id := NULL;
         x_contact_party_number := NULL;
         x_contact_profile_id := NULL;
         p_create_person_rec.person_first_name :=
            rec_cust.ap_contact_first_name;
         p_create_person_rec.person_last_name := rec_cust.ap_contact_last_name;
         p_create_person_rec.created_by_module := l_api_name;
         hz_party_v2pub.create_person ('T',
                                       p_create_person_rec,
                                       x_contact_party_id,
                                       x_contact_party_number,
                                       x_contact_profile_id,
                                       o_ret_status,
                                       o_msg_count,
                                       o_msg_data);

         IF o_ret_status <> 'S'
         THEN
            return_msg (o_msg_count, o_msg_data, o_return_msg);
            x_msg_data := 'Contact Person creation: ' || o_return_msg;
            debug (p_record_id, x_msg_data);
            x_ret_status := o_ret_status;
            ROLLBACK TO customer_record;
            RETURN;
         END IF;

         l_sec :=
               'Conact Person Creation Process Completed - API Status '
            || o_ret_status;

         debug (p_record_id, l_sec);

         SAVEPOINT customer_record;
         --====================================================================
         -- Creating Org contact
         --====================================================================
         p_org_contact_rec := NULL;
         p_org_contact_rec.created_by_module := l_api_name;
         p_org_contact_rec.party_rel_rec.subject_id := x_contact_party_id;
         p_org_contact_rec.party_rel_rec.subject_type := 'PERSON';
         p_org_contact_rec.party_rel_rec.subject_table_name := 'HZ_PARTIES';
         p_org_contact_rec.party_rel_rec.object_id := o_party_id;
         p_org_contact_rec.party_rel_rec.object_type := 'ORGANIZATION';
         p_org_contact_rec.party_rel_rec.object_table_name := 'HZ_PARTIES';
         p_org_contact_rec.party_rel_rec.relationship_code := 'CONTACT_OF';
         p_org_contact_rec.party_rel_rec.relationship_type := 'CONTACT';
         p_org_contact_rec.party_rel_rec.start_date := SYSDATE;
         hz_party_contact_v2pub.create_org_contact ('T',
                                                    p_org_contact_rec,
                                                    x_org_contact_id,
                                                    x_party_rel_id,
                                                    x_rel_party_id,
                                                    x_rel_party_number,
                                                    o_ret_status,
                                                    o_msg_count,
                                                    o_msg_data);

         IF o_ret_status <> 'S'
         THEN
            return_msg (o_msg_count, o_msg_data, o_return_msg);
            x_msg_data := 'Creating Org Contact: ' || o_return_msg;
            debug (p_record_id, x_msg_data);
            x_ret_status := o_ret_status;
            ROLLBACK TO customer_record;
            RETURN;
         END IF;


         l_sec :=
               'Creation of Org contact Process Completed - API Status '
            || o_ret_status;
         debug (p_record_id, l_sec);

         --====================================================================
         -- Creating Phone contact point by using party_id
         --====================================================================

         -- Initializing the Mandatory API parameters
         pcontactpointrec.contact_point_type := 'PHONE';
         pcontactpointrec.owner_table_name := 'HZ_PARTIES';
         pcontactpointrec.owner_table_id := x_rel_party_id;
         pcontactpointrec.primary_flag := 'Y';
         pcontactpointrec.contact_point_purpose := 'BUSINESS';
         pcontactpointrec.created_by_module := l_api_name;
         pphonerec.phone_area_code := NULL;
         pphonerec.phone_country_code := '1';
         pphonerec.phone_number := rec_cust.ap_phone;
         pphonerec.phone_line_type := 'GEN';


         HZ_CONTACT_POINT_V2PUB.CREATE_CONTACT_POINT (
            p_init_msg_list       => FND_API.G_TRUE,
            p_contact_point_rec   => pcontactpointrec,
            p_edi_rec             => pedirec,
            p_email_rec           => pemailrec,
            p_phone_rec           => pphonerec,
            p_telex_rec           => ptelexrec,
            p_web_rec             => pwebrec,
            x_contact_point_id    => o_contact_point_id,
            x_return_status       => o_ret_status,
            x_msg_count           => o_msg_count,
            x_msg_data            => o_msg_data);

         IF o_ret_status <> 'S'
         THEN
            return_msg (o_msg_count, o_msg_data, o_return_msg);
            x_msg_data :=
               'Creating Phone Contact (for Party): ' || o_return_msg;
            debug (p_record_id, x_msg_data);
            x_ret_status := o_ret_status;
            ROLLBACK TO customer_record;
            RETURN;
         END IF;

         l_sec :=
               'Creating Phone Contact (for Party) Process Completed - API Status '
            || o_ret_status;

         debug (p_record_id, l_sec);


         SAVEPOINT customer_record;
         ------------------------------------------------------------------------------------------------------------
         -- Create a Email Contact Point using party_id
         ------------------------------------------------------------------------------------------------------------
         -- Initializing the Mandatory API parameters
         pcontactpointrec.contact_point_type := 'EMAIL';
         pcontactpointrec.owner_table_name := 'HZ_PARTIES';
         pcontactpointrec.owner_table_id := x_rel_party_id;      --l_party_id;
         pcontactpointrec.primary_flag := 'Y';
         pcontactpointrec.contact_point_purpose := 'BUSINESS';
         pcontactpointrec.created_by_module := l_api_name;
         pemailrec.email_format := 'MAILHTML';
         pemailrec.email_address := rec_cust.ap_email;

         HZ_CONTACT_POINT_V2PUB.CREATE_CONTACT_POINT (
            p_init_msg_list       => FND_API.G_TRUE,
            p_contact_point_rec   => pcontactpointrec,
            p_edi_rec             => pedirec,
            p_email_rec           => pemailrec,
            p_phone_rec           => pphonerec,
            p_telex_rec           => ptelexrec,
            p_web_rec             => pwebrec,
            x_contact_point_id    => o_contact_point_id,
            x_return_status       => o_ret_status,
            x_msg_count           => o_msg_count,
            x_msg_data            => o_msg_data);

         IF o_ret_status <> 'S'
         THEN
            return_msg (o_msg_count, o_msg_data, o_return_msg);
            x_msg_data :=
               'Creating Email Contact (for Party): ' || o_return_msg;
            debug (p_record_id, x_msg_data);
            x_ret_status := o_ret_status;
            ROLLBACK TO customer_record;
            RETURN;
         END IF;

         l_sec :=
               'Creating Email Contact (for Party) Process Completed - API Status '
            || o_ret_status;


         debug (p_record_id, l_sec);

         ------------------------------------------------------------------------------------------------------------
         -- Creating Party site
         ------------------------------------------------------------------------------------------------------------
         o_ret_status := NULL;
         o_msg_count := NULL;
         o_msg_data := NULL;
         o_party_site_id := NULL;
         o_party_site_no := NULL;
         o_sprofile_id := NULL;
         ppartysiterec.created_by_module := l_api_name;

         -- create a party site now
         ppartysiterec.party_id := x_rel_party_id;
         ppartysiterec.location_id := ln_bill_location_id;
         ppartysiterec.identifying_address_flag := 'Y';

         hz_party_site_v2pub.create_party_site (
            p_init_msg_list       => 'T',
            p_party_site_rec      => ppartysiterec,
            x_party_site_id       => o_party_site_id,
            x_party_site_number   => o_party_site_no,
            x_return_status       => o_ret_status,
            x_msg_count           => o_msg_count,
            x_msg_data            => o_msg_data);


         IF o_ret_status <> 'S'
         THEN
            return_msg (o_msg_count, o_msg_data, o_return_msg);
            x_msg_data := 'Creating contact location: ' || o_return_msg;
            debug (p_record_id, x_msg_data);
            x_ret_status := o_ret_status;
            ROLLBACK TO customer_record;
            RETURN;
         END IF;

         l_sec :=
               'Creating contact location Process Completed - API Status '
            || o_ret_status;


         debug (p_record_id, l_sec);

         SAVEPOINT customer_record;
         --====================================================================
         -- creating account role
         --====================================================================

         o_ret_status := NULL;
         o_msg_count := NULL;
         o_msg_data := NULL;
         p_cr_cust_acc_role_rec := NULL;
         p_cr_cust_acc_role_rec.party_id := x_rel_party_id;
         p_cr_cust_acc_role_rec.cust_account_id := o_cust_account_id;
         p_cr_cust_acc_role_rec.primary_flag := 'N';
         p_cr_cust_acc_role_rec.role_type := 'CONTACT';
         p_cr_cust_acc_role_rec.created_by_module := l_api_name;
         hz_cust_account_role_v2pub.create_cust_account_role (
            'T',
            p_cr_cust_acc_role_rec,
            x_cust_account_role_id,
            o_ret_status,
            o_msg_count,
            o_msg_data);

         IF o_ret_status <> 'S'
         THEN
            return_msg (o_msg_count, o_msg_data, o_return_msg);
            x_msg_data := 'Creating Account role: ' || o_return_msg;
            debug (p_record_id, x_msg_data);
            x_ret_status := o_ret_status;
            ROLLBACK TO customer_record;
            RETURN;
         END IF;

         l_sec :=
               'Creating Account role Process Completed - API Status '
            || o_ret_status;


         debug (p_record_id, l_sec);

         SAVEPOINT customer_record;

         --====================================================================
         -- Assign Accounts Payables role
         --====================================================================
         o_ret_status := NULL;
         o_msg_count := NULL;
         o_msg_data := NULL;
         p_ROLE_RESPONSIBILITY_REC_TYPE := NULL;
         p_ROLE_RESPONSIBILITY_REC_TYPE.cust_account_role_id :=
            x_cust_account_role_id;
         p_ROLE_RESPONSIBILITY_REC_TYPE.responsibility_type := 'ACC_PAY';

         p_ROLE_RESPONSIBILITY_REC_TYPE.created_by_module := l_api_name;

         hz_cust_account_role_v2pub.create_role_responsibility (
            p_init_msg_list             => 'T',
            p_role_responsibility_rec   => p_ROLE_RESPONSIBILITY_REC_TYPE,
            x_responsibility_id         => o_resp_id,
            x_return_status             => o_ret_status,
            x_msg_count                 => o_msg_count,
            x_msg_data                  => o_msg_data);

         IF o_ret_status <> 'S'
         THEN
            return_msg (o_msg_count, o_msg_data, o_return_msg);
            x_msg_data := 'Assing AP role: ' || o_return_msg;
            debug (p_record_id, x_msg_data);
            x_ret_status := o_ret_status;
            ROLLBACK TO customer_record;
            RETURN;
         END IF;

         l_sec :=
            'Assing AP role Process Completed - API Status ' || o_ret_status;

         debug (p_record_id, l_sec);

         --====================================================================
         -- Adding records into XXWC.XXWC_B2B_CONFIG_TBL
         --====================================================================

         BEGIN
            INSERT INTO XXWC.XXWC_B2B_CONFIG_TBL (PARTY_ID,
                                                  PARTY_NAME,
                                                  PARTY_NUMBER,
                                                  CUSTOMER_ID,
                                                  DELIVER_SOA,
                                                  DELIVER_ASN,
                                                  DELIVER_INVOICE,
                                                  START_DATE_ACTIVE,
                                                  END_DATE_ACTIVE,
                                                  CREATION_DATE,
                                                  CREATED_BY,
                                                  LAST_UPDATE_DATE,
                                                  LAST_UPDATED_BY,
                                                  TP_NAME,
                                                  DELIVER_POA,
                                                  DEFAULT_EMAIL,
                                                  STATUS,
                                                  NOTIFICATION_EMAIL,
                                                  NOTIFY_ACCOUNT_MGR,
                                                  SOA_EMAIL,
                                                  ASN_EMAIL,
                                                  COMMENTS,
                                                  APPROVED_DATE,
                                                  DELIVER_POD,
                                                  POD_EMAIL,
                                                  POD_FREQUENCY,
                                                  ACCOUNT_NUMBER,
                                                  ACCOUNT_NAME,
                                                  CUST_ACCOUNT_ID,
                                                  POD_LAST_SENT_DATE,
                                                  POD_NEXT_SEND_DATE,
                                                  ID,
                                                  LOCATION,
                                                  SITE_USE_ID,
                                                  SOA_PRINT_PRICE,
                                                  POD_PRINT_PRICE,
                                                  BUSINESS_EVENT_ELIGIBLE)
                    VALUES (
                              o_party_id,
                              rec_cust.business_name,
                              o_party_number,
                              o_cust_account_id,
                              'Y',
                              'N',
                              'Y',
                              SYSDATE,
                              TO_DATE ('12/31/4099 00:00:00',
                                       'MM/DD/YYYY HH24:MI:SS'),
                              SYSDATE,
                              FND_GLOBAL.USER_ID,
                              SYSDATE,
                              -1,
                              NULL,
                              'N',
                              NULL,
                              'APPROVED',
                              NULL,
                              'Y',
                              rec_cust.soa_email_address,
                              rec_cust.invoice_email_address,
                              NULL,
                              NULL,
                              'Y',
                              rec_cust.pod_email_address,
                              'WEEKLY',
                              o_cust_account_no,
                              rec_cust.business_name,
                              o_cust_account_id,
                              NULL,
                              NULL,
                              NULL,
                              rec_cust.billing_city,
                              ln_ship_site_use_id,
                              'N',
                              NULL,
                              NULL);

            l_sec :=
                  'Row inserted into  XXWC.XXWC_B2B_CONFIG_TBL table  '
               || SQL%ROWCOUNT;

            debug (p_record_id, l_sec);
         EXCEPTION
            WHEN OTHERS
            THEN
               return_msg (1, SUBSTR (SQLERRM, 1, 250), o_return_msg);
               x_msg_data := 'B2B Registration: ' || o_return_msg;
               debug (p_record_id, x_msg_data);
               x_ret_status := o_ret_status;
               ROLLBACK TO customer_record;
               RETURN;
         END;

         l_sec := 'Customer Created Successfully:' || o_cust_account_no;

         debug (p_record_id, l_sec);

         UPDATE xxwc.XXWC_AR_EQUIFAX_ACCT_WS_TBL
            SET process_flag = 'Y', CUSTOMER_NUMBER = o_cust_account_no
          WHERE record_id = p_record_id;

         l_sec := 'Updating Staging table:' || SQL%ROWCOUNT;
         debug (p_record_id, l_sec);

         debug (p_record_id, 'o_profile_id ' || o_profile_id);

         x_party_id := o_party_id;
         x_party_number := o_party_number;
         x_profile_id := NVL (o_profile_id, 0);
         x_cust_account_id := o_cust_account_id;
         x_acct_number := o_cust_account_no;
         x_ret_status := 'Success';
         x_msg_count := 0;
         x_msg_data := NULL;
         COMMIT;
         debug (p_record_id, 'Return Variables Initiated');
      END LOOP;

      debug (p_record_id, 'Return Variables Initiated');
   EXCEPTION
      WHEN OTHERS
      THEN
         x_ret_status := 'Error';
         x_msg_count := 1;
         x_msg_data :=
               'Techinical Error Occurred, please contact HD White Cap IT Team '
            || SQLERRM;
         debug (p_record_id, x_msg_data);

         xxcus_error_pkg.xxcus_error_main_api (
            p_called_from         => g_err_callfrom || '.' || lvc_procedure,
            p_calling             => l_sec,
            p_ora_error_msg       => SQLERRM,
            p_error_desc          =>    'Error Occured for '
                                     || p_record_id
                                     || 'record id in XXWC_AR_EQUIFAX_ACCT_WS_TBL.',
            p_distribution_list   => g_distro_list,
            p_module              => g_module);
   END;

   PROCEDURE update_customer (P_RECORD_ID         IN     NUMBER,
                              x_cust_account_id      OUT NUMBER,
                              x_acct_number          OUT VARCHAR2,
                              x_party_id             OUT NUMBER,
                              x_party_number         OUT VARCHAR2,
                              x_profile_id           OUT NUMBER,
                              x_ret_status           OUT VARCHAR2,
                              x_msg_count            OUT NUMBER,
                              x_msg_data             OUT VARCHAR2)
   IS
      -- =====================================================================================================================================================
      -- Procedure: UPDATE_CUSTOMER
      -- Purpose:  Updating customer
      -- ====================================================================================================================================================
      --  REVISIONS:
      --   Ver        Date        Author               Description
      --  ---------  ---------- ---------------  ------------------------------------------------------------------------------------------------
      --  1.0        12/19/2017  P.Vamshidhar     TMS#20131016-00419 -Credit - Implement online credit application - Customer Accounts/Update File
      -- ====================================================================================================================================================
      -- Cursor Customer name and status purpose
      CURSOR cur_cust_mast
      IS
         SELECT hca.account_name acct_name,
                xanw.business_name stg_name,
                hca.status acct_status,
                DECODE (xanw.status,  NULL, NULL,  'Y', 'A',  'N', 'I')
                   stg_status,
                hca.customer_class_code acct_cust_class_code,
                (SELECT lookup_code
                   FROM APPS.FND_LOOKUP_VALUES
                  WHERE     LOOKUP_TYPE = 'CUSTOMER CLASS'
                        AND LOOKUP_CODE = xanw.type_of_business
                        AND enabled_flag = 'Y')
                   stg_cust_class_code,
                hca.attribute9 acct_Pred_trade,
                xanw.predominant_trade stg_Pred_trade,
                xanw.salesrep_id stg_salesrep_id,
                xanw.customer_number,
                hca.object_version_number,
                hca.party_id,
                hp.object_version_number party_object_version_number,
                hp.party_number,
                hca.cust_account_id,
                hca.sales_channel_code,
                hca.primary_salesrep_id acct_salesrep_id
           FROM apps.hz_cust_accounts hca,
                xxwc.XXWC_AR_EQUIFAX_ACCT_WS_TBL xanw,
                hz_parties hp
          WHERE     1 = 1
                AND xanw.record_id = p_record_id
                AND hca.account_number = xanw.customer_number
                AND hca.party_id = hp.party_id;


      CURSOR cur_cust_prf
      IS
         SELECT DISTINCT
                hzcp.cust_account_profile_id,
                hzcp.credit_rating acct_crd_rating,
                hzcp.collector_id acct_collector_id,
                hzcp.account_status acct_acct_status,
                xanw.credit_decisioning stg_crd_rating,
                xanw.credit_manager stg_collector_id,
                (SELECT LOOKUP_CODE
                   FROM APPS.FND_LOOKUP_VALUES
                  WHERE     LOOKUP_TYPE = 'ACCOUNT_STATUS'
                        AND LOOKUP_CODE = xanw.account_status)
                   stg_acct_status,
                hzcp.profile_class_id acct_prof_class_id,
                (SELECT profile_class_id
                   FROM apps.hz_cust_profile_classes
                  WHERE name = xanw.profile_class)
                   stg_prof_class_id,
                hzcp.object_Version_number,
                hzcp.credit_analyst_id
           FROM hz_customer_profiles hzcp,
                xxwc.XXWC_AR_EQUIFAX_ACCT_WS_TBL xanw,
                hz_cust_accounts hca
          WHERE     hca.account_number = xanw.customer_number
                AND hzcp.cust_account_id = hca.cust_account_id
                AND hzcp.SITE_USE_ID IS NULL
                AND xanw.record_id = p_record_id;


      CURSOR cur_cust_bsite_prf (
         P_SITE_USE_ID   IN NUMBER,
         p_record_id     IN NUMBER)
      IS
         SELECT DISTINCT
                hzcp.cust_account_profile_id,
                hzcp.credit_rating acct_crd_rating,
                hzcp.collector_id acct_collector_id,
                hzcp.account_status acct_acct_status,
                xanw.credit_decisioning stg_crd_rating,
                xanw.credit_manager stg_collector_id,
                (SELECT LOOKUP_CODE
                   FROM APPS.FND_LOOKUP_VALUES
                  WHERE     LOOKUP_TYPE = 'ACCOUNT_STATUS'
                        AND LOOKUP_CODE = xanw.account_status)
                   stg_acct_status,
                hzcp.profile_class_id acct_prof_class_id,
                (SELECT profile_class_id
                   FROM apps.hz_cust_profile_classes
                  WHERE name = xanw.profile_class)
                   stg_prof_class_id,
                hzcp.object_Version_number,
                hzcp.credit_analyst_id
           FROM hz_customer_profiles hzcp,
                xxwc.XXWC_AR_EQUIFAX_ACCT_WS_TBL xanw,
                hz_cust_accounts hca
          WHERE     hca.account_number = xanw.customer_number
                AND hzcp.cust_account_id = hca.cust_account_id
                AND hzcp.SITE_USE_ID = P_SITE_USE_ID
                AND xanw.record_id = p_record_id;



      CURSOR CUR_CUST
      IS
         SELECT RECORD_ID,
                SALES_FORCE_ID,
                CUSTOMER_NUMBER,
                BUSINESS_NAME,
                BUSINESS_PHONE,
                BUSINESS_FAX,
                EMAIL_ADDRESS,
                SALESREP_ID,
                TYPE_OF_BUSINESS,
                PROFILE_CLASS,
                CREDIT_MANAGER,
                CREDIT_CLASSIFICATION,
                ACCOUNT_STATUS,
                PREDOMINANT_TRADE,
                STATUS,
                NVL (BILLING_ADDRESS, SHIPPING_ADDRESS) BILLING_ADDRESS,
                NVL (BILLING_CITY, SHIPPING_CITY) BILLING_CITY,
                NVL (BILLING_COUNTY, SHIPPING_COUNTY) BILLING_COUNTY,
                NVL (BILLING_STATE, SHIPPING_STATE) BILLING_STATE,
                NVL (BILLING_COUNTRY, SHIPPING_COUNTRY) BILLING_COUNTRY,
                NVL (BILLING_ZIP_CODE, SHIPPING_ZIP_CODE) BILLING_ZIP_CODE,
                NVL (SHIPPING_ADDRESS, BILLING_ADDRESS) SHIPPING_ADDRESS,
                NVL (SHIPPING_CITY, BILLING_CITY) SHIPPING_CITY,
                NVL (SHIPPING_COUNTY, BILLING_COUNTY) SHIPPING_COUNTY,
                NVL (SHIPPING_STATE, BILLING_STATE) SHIPPING_STATE,
                NVL (SHIPPING_COUNTRY, BILLING_COUNTRY) SHIPPING_COUNTRY,
                NVL (SHIPPING_ZIP_CODE, BILLING_ZIP_CODE) SHIPPING_ZIP_CODE,
                YARD_JOB_ACCNT_PROJECT,
                CREDIT_LIMIT,
                DEFAULT_JOB_CREDIT_LIMIT,
                CREDIT_DECISIONING,
                AP_CONTACT_FIRST_NAME,
                AP_CONTACT_LAST_NAME,
                AP_PHONE,
                AP_EMAIL,
                INVOICE_EMAIL_ADDRESS,
                POD_EMAIL_ADDRESS,
                SOA_EMAIL_ADDRESS,
                NVL(PURCHASE_ORDER_REQUIRED,'N') PURCHASE_ORDER_REQUIRED,
                DUNSNUMBER
           FROM XXWC.XXWC_AR_EQUIFAX_ACCT_WS_TBL
          WHERE record_id = P_RECORD_ID;

      ln_cust_account_id               hz_cust_accounts_all.cust_account_id%TYPE;
      ln_party_id                      hz_parties.party_id%TYPE;
      lvc_party_number                 hz_parties.party_number%TYPE;
      ln_account_number                hz_cust_accounts_all.account_number%TYPE;
      ln_stg_crd_ltd                   hz_cust_profile_amts.overall_credit_limit%TYPE;
      ln_stg_dflt_crd_ltd              NUMBER;
      lvc_party_version                NUMBER;
      o_ret_status                     VARCHAR2 (100);
      o_msg_count                      NUMBER;
      o_msg_data                       VARCHAR2 (32767);
      l_msg_data                       VARCHAR2 (2000);
      l_msg_index_out                  NUMBER;
      lvc_procedure                    VARCHAR2 (100) := 'UPDATE_CUSTOMER';
      l_sec                            VARCHAR2 (32767);
      cust_exp                         EXCEPTION;
      lvc_account_name                 hz_cust_accounts.account_name%TYPE;
      lvc_upd_flag                     VARCHAR2 (1);
      l_res_category                   jtf_rs_resource_extns_tl.category%TYPE;
      l_profile_id                     hz_cust_profile_classes.profile_class_id%TYPE;
      ln_credit_analyst                ar_collectors.resource_id%TYPE;
      l_api_name                       VARCHAR2 (30) := 'ONT_UI_ADD_CUSTOMER';
      ln_org_id                        NUMBER := 162;
      ln_cust_acct_site_id             hz_cust_acct_sites.cust_acct_site_id%TYPE;
      ln_salesrep_id                   jtf_rs_salesreps_mo_v.salesrep_id%TYPE;
      ln_resource_id                   jtf_rs_salesreps_mo_v.resource_id%TYPE;
      lvc_profile_class                hz_cust_profile_classes.name%TYPE;
      ln_credit_manager                NUMBER;
      ln_credit_limit                  NUMBER;
      ln_default_job_credit_limit      NUMBER;
      lvc_credit_decisioning           VARCHAR2 (10);
      lvc_account_status               VARCHAR2 (100);
      lvc_type_of_business             VARCHAR2 (100);
      lvc_predominant_trade            VARCHAR2 (100);
      ln_business_fax                  NUMBER;
      lvc_email_address                VARCHAR2 (100);
      ln_business_phone                NUMBER;
      ln_stg_profile_id                NUMBER;
      lvc_yard_job_accnt_project       VARCHAR (150);
      ol_profile_id                    HZ_CUSTOMER_PROFILES.CUST_ACCOUNT_PROFILE_ID%TYPE;
      lv_currency_code                 VARCHAR2 (10);
      o_custacctprofileamtid           NUMBER;
      p_object_version_number          NUMBER;
      l_phone_exist                    VARCHAR2 (1);
      o_contact_pointid                NUMBER;
      o_cust_acct_site_id              NUMBER;
      o_location_id                    HZ_LOCATIONS.LOCATION_ID%TYPE;
      o_party_site_id                  hz_party_sites.party_site_id%TYPE;
      o_party_site_no                  NUMBER;
      o_sprofile_id                    NUMBER;
      ln_acct_credit_analyst_id        hz_customer_profiles.credit_analyst_id%TYPE;
      ln_acct_collector_id             NUMBER;
      ln_credit_analyst_id             NUMBER;
      lvc_acct_status                  VARCHAR2 (100);
      ln_acct_prof_class_id            NUMBER;
      o_cust_acct_site_use_id          NUMBER;
      o_cust_acct_profile_amt_id       NUMBER;
      ln_cust_account_profile_id       NUMBER;
      ln_bill_to_site_use_id           NUMBER;
      ln_bill_to_obj_ver_no            NUMBER;
      o_return_status                  VARCHAR2 (100);
      o_party_id                       NUMBER;
      o_cust_account_id                NUMBER;
      o_bill_to_site_use_id            NUMBER;
      x_contact_party_id               NUMBER;
      x_contact_party_number           NUMBER;
      x_contact_profile_id             NUMBER;
      x_org_contact_id                 NUMBER;
      x_party_rel_id                   NUMBER;
      x_rel_party_id                   NUMBER;
      x_rel_party_number               NUMBER;
      ln_bill_location_id              NUMBER;
      o_contact_point_id               NUMBER;
      x_cust_account_role_id           NUMBER;
      o_resp_id                        NUMBER;
      lvc_soa_email_address            VARCHAR2 (1000);
      lvc_invoice_email_address        VARCHAR2 (1000);
      lvc_pod_email_address            VARCHAR2 (1000);
      ln_ship_site_use_id              NUMBER;
      lvc_billing_city                 VARCHAR2 (1000);
      ln_count                         NUMBER;
      ln_acct_role_id                  NUMBER;
      obj_version                      NUMBER;


      pcustaccountrec                  hz_cust_account_v2pub.cust_account_rec_type;
      p_organization_rec               hz_party_v2pub.organization_rec_type;
      pcustomerprofile                 hz_customer_profile_v2pub.customer_profile_rec_type;
      p_party_rec                      HZ_PARTY_V2PUB.PARTY_REC_TYPE;
      pcustproamtrec                   hz_customer_profile_v2pub.cust_profile_amt_rec_type;
      pcontactpointrec                 hz_contact_point_v2pub.contact_point_rec_type;
      pphonerec                        hz_contact_point_v2pub.phone_rec_type;
      pemailrec                        hz_contact_point_v2pub.email_rec_type;
      pcustacctsiterec                 hz_cust_account_site_v2pub.cust_acct_site_rec_type;
      pcustacctsiteuserec              hz_cust_account_site_v2pub.cust_site_use_rec_type;
      plocationrec                     hz_location_v2pub.location_rec_type;
      p_create_person_rec              hz_party_v2pub.person_rec_type;
      p_org_contact_rec                hz_party_contact_v2pub.org_contact_rec_type;
      pedirec                          hz_contact_point_v2pub.EDI_REC_TYPE;
      ptelexrec                        hz_contact_point_v2pub.TELEX_REC_TYPE;
      pwebrec                          hz_contact_point_v2pub.WEB_REC_TYPE;
      p_cr_cust_acc_role_rec           hz_cust_account_role_v2pub.cust_account_role_rec_type;
      p_ROLE_RESPONSIBILITY_REC_TYPE   HZ_CUST_ACCOUNT_ROLE_V2PUB.role_responsibility_rec_type;
      p_cust_role_rec                  hz_cust_account_role_v2pub.cust_account_role_rec_type;

      --site variables
      ppartysiterec                    hz_party_site_v2pub.party_site_rec_type;


      o_return_msg                     VARCHAR2 (32767);

      PROCEDURE return_msg (in_msg_count     IN     NUMBER,
                            in_msg_data      IN     VARCHAR2,
                            out_return_msg      OUT VARCHAR2)
      IS
      BEGIN
         IF in_msg_count > 1
         THEN
            x_msg_count := in_msg_data;

            FOR I IN 1 .. in_msg_count
            LOOP
               Oe_Msg_Pub.get (p_msg_index       => i,
                               p_encoded         => Fnd_Api.G_FALSE,
                               p_data            => l_msg_data,
                               p_msg_index_out   => l_msg_index_out);
               out_return_msg :=
                  out_return_msg || ':' || SUBSTR (l_msg_data, 1, 200);
            END LOOP;
         ELSE
            out_return_msg := in_msg_data;
         END IF;
      END;
   BEGIN
      l_sec := 'Updating Customer Started';
      debug (p_record_id, l_sec);

      --  storing all input values into variables
      BEGIN
         SELECT customer_number,
                BUSINESS_NAME,
                BUSINESS_PHONE,
                BUSINESS_FAX,
                EMAIL_ADDRESS,
                SALESREP_ID,
                TYPE_OF_BUSINESS,
                PROFILE_CLASS,
                CREDIT_MANAGER,
                ACCOUNT_STATUS,
                PREDOMINANT_TRADE,
                CREDIT_LIMIT,
                DEFAULT_JOB_CREDIT_LIMIT,
                CREDIT_DECISIONING,
                soa_email_address,
                invoice_email_address,
                pod_email_address
           INTO ln_account_number,
                lvc_account_name,
                ln_business_phone,
                ln_business_fax,
                lvc_email_address,
                ln_resource_id,
                lvc_TYPE_OF_BUSINESS,
                lvc_PROFILE_CLASS,
                ln_CREDIT_MANAGER,
                lvc_ACCOUNT_STATUS,
                lvc_PREDOMINANT_TRADE,
                ln_stg_crd_ltd,
                ln_stg_dflt_crd_ltd,
                lvc_credit_decisioning,
                lvc_soa_email_address,
                lvc_invoice_email_address,
                lvc_pod_email_address
           FROM xxwc.XXWC_AR_EQUIFAX_ACCT_WS_TBL
          WHERE record_id = p_record_id;
      EXCEPTION
         WHEN OTHERS
         THEN
            x_msg_data := ' Error occured ' || SUBSTR (SQLERRM, 1, 250);
            debug (p_record_id, l_sec);
            GOTO LASTSTEP;
      END;

      l_sec := 'Deriving Account details';
      debug (p_record_id, l_sec);

      BEGIN
         SELECT hca.cust_account_id,
                hca.party_id,
                hp.party_number,
                hp.object_version_number
           INTO ln_cust_account_id,
                ln_party_id,
                lvc_party_number,
                lvc_party_version
           FROM hz_cust_accounts hca, hz_parties hp
          WHERE     hca.party_id = hp.party_id
                AND hca.account_number = ln_account_number;
      EXCEPTION
         WHEN OTHERS
         THEN
            debug (p_record_id, 'Error occured ' || SUBSTR (SQLERRM, 1, 250));
            RAISE cust_exp;
      END;

      l_sec := 'Deriving Sales Rep Id';
      debug (p_record_id, l_sec);

      IF ln_resource_id IS NOT NULL
      THEN
         BEGIN
            SELECT SALESREP_ID
              INTO ln_salesrep_id
              FROM jtf_rs_salesreps_mo_v
             WHERE resource_id = ln_resource_id;
         EXCEPTION
            WHEN OTHERS
            THEN
               ln_salesrep_id := NULL;
         END;
      ELSE
         BEGIN
            SELECT A.PRIMARY_SALESREP_ID
              INTO ln_salesrep_id
              FROM APPS.HZ_CUST_SITE_USES_ALL A,
                   APPS.HZ_CUST_ACCT_SITES_ALL B
             WHERE     A.CUST_ACCT_SITE_ID = B.CUST_ACCT_SITE_ID
                   AND B.CUST_ACCOUNT_ID = ln_cust_account_id
                   AND A.ATTRIBUTE1 IS NOT NULL
                   AND ROWNUM = 1;
         END;
      END IF;

      l_sec := 'Deriving Credit Analyst';
      debug (p_record_id, l_sec);

      IF ln_credit_manager IS NOT NULL
      THEN
         BEGIN
            ln_credit_analyst := NULL;

            SELECT resource_id
              INTO ln_credit_analyst
              FROM ar_collectors
             WHERE collector_id = ln_credit_manager;
         EXCEPTION
            WHEN OTHERS
            THEN
               ln_credit_analyst := NULL;
         END;
      END IF;

      l_sec := 'Deriving Currency code';
      debug (p_record_id, l_sec);

      BEGIN
         SELECT gsob.currency_code
           INTO lv_currency_code
           FROM hr_operating_units hou, gl_sets_of_books gsob
          WHERE     1 = 1
                AND gsob.set_of_books_id = hou.set_of_books_id
                AND hou.organization_id = ln_org_id;
      EXCEPTION
         WHEN OTHERS
         THEN
            lv_currency_code := 'USD';
      END;

      debug (p_record_id, l_sec || ' ' || lv_currency_code);

      pcustaccountrec := NULL;

      SAVEPOINT customer_record;

      FOR rec_cust IN cur_cust
      LOOP
         --====================================================================
         -- Create telephone record for the party
         --====================================================================

         l_sec := 'Creating Business phone contact for Party';
         debug (p_record_id, l_sec);


         IF rec_cust.business_phone IS NOT NULL
         THEN
            BEGIN
               SELECT 'Y'
                 INTO l_phone_exist
                 FROM apps.ar_phones_v
                WHERE     owner_table_name = 'HZ_PARTIES'
                      AND owner_table_id = ln_party_id
                      AND phone_type = 'GEN'
                      AND area_code || phone_number = rec_cust.business_phone
                      AND ROWNUM = 1;
            EXCEPTION
               WHEN NO_DATA_FOUND
               THEN
                  l_phone_exist := 'N';
               WHEN OTHERS
               THEN
                  l_phone_exist := 'N';
            END;

            IF l_phone_exist = 'N'
            THEN
               pcontactpointrec := NULL;
               pcontactpointrec.contact_point_type := 'PHONE';
               pcontactpointrec.owner_table_name := 'HZ_PARTIES';
               pcontactpointrec.owner_table_id := ln_party_id;
               pcontactpointrec.primary_flag := 'Y';
               pcontactpointrec.contact_point_purpose := 'BUSINESS';
               pphonerec.phone_country_code := NULL;
               pphonerec.phone_area_code :=
                  SUBSTR (rec_cust.business_phone, 1, 3);
               pphonerec.phone_number := SUBSTR (rec_cust.business_phone, 4);
               pphonerec.phone_extension := NULL;
               pphonerec.phone_line_type := 'GEN';
               pcontactpointrec.created_by_module := l_api_name;

               hz_contact_point_v2pub.create_contact_point (
                  p_init_msg_list       => 'T',
                  p_contact_point_rec   => pcontactpointrec,
                  p_phone_rec           => pphonerec,
                  x_contact_point_id    => o_contact_pointid,
                  x_return_status       => o_ret_status,
                  x_msg_count           => o_msg_count,
                  x_msg_data            => o_msg_data);

               IF o_ret_status <> 'S'
               THEN
                  return_msg (o_msg_count, o_msg_data, o_return_msg);
                  x_msg_data := 'Phone Updation: ' || o_return_msg;
                  debug (p_record_id, x_msg_data);
                  x_ret_status := o_ret_status;
                  ROLLBACK TO customer_record;
                  RETURN;
               END IF;
            END IF;
         END IF;


         --====================================================================
         -- Create fax record for the party
         --====================================================================
         l_sec := 'Creating Business Fax contact for Party';
         debug (p_record_id, l_sec);


         IF rec_cust.business_fax IS NOT NULL
         THEN
            l_phone_exist := NULL;

            BEGIN
               SELECT 'Y'
                 INTO l_phone_exist
                 FROM apps.ar_phones_v
                WHERE     owner_table_name = 'HZ_PARTIES'
                      AND owner_table_id = ln_party_id
                      AND phone_type = 'FAX'
                      AND area_code || phone_number = rec_cust.business_fax
                      AND ROWNUM = 1;
            EXCEPTION
               WHEN NO_DATA_FOUND
               THEN
                  l_phone_exist := 'N';
               WHEN OTHERS
               THEN
                  l_phone_exist := 'N';
            END;

            IF l_phone_exist = 'N'
            THEN
               pcontactpointrec := NULL;
               pcontactpointrec.contact_point_type := 'PHONE';
               pcontactpointrec.owner_table_name := 'HZ_PARTIES';
               pcontactpointrec.owner_table_id := ln_party_id;
               pcontactpointrec.primary_flag := 'N';
               pcontactpointrec.contact_point_purpose := 'BUSINESS';
               pphonerec.phone_area_code :=
                  SUBSTR (rec_cust.business_fax, 1, 3);
               pphonerec.phone_number := SUBSTR (rec_cust.business_fax, 4);
               pphonerec.phone_extension := NULL;
               pphonerec.phone_line_type := 'FAX';
               pcontactpointrec.created_by_module := l_api_name;

               hz_contact_point_v2pub.create_contact_point (
                  p_init_msg_list       => 'T',
                  p_contact_point_rec   => pcontactpointrec,
                  p_phone_rec           => pphonerec,
                  x_contact_point_id    => o_contact_pointid,
                  x_return_status       => o_ret_status,
                  x_msg_count           => o_msg_count,
                  x_msg_data            => o_msg_data);

               IF o_ret_status <> 'S'
               THEN
                  return_msg (o_msg_count, o_msg_data, o_return_msg);
                  x_msg_data := 'Fax Creation: ' || o_return_msg;
                  debug (p_record_id, x_msg_data);
                  x_ret_status := o_ret_status;
                  ROLLBACK TO customer_record;
                  RETURN;
               END IF;
            END IF;
         END IF;

         --====================================================================
         -- Create email record for the party
         --====================================================================

         l_sec := 'Creating Email Record contact for Party';
         debug (p_record_id, l_sec);


         IF rec_cust.email_address IS NOT NULL
         THEN
            l_phone_exist := NULL;

            BEGIN
               SELECT 'Y'
                 INTO l_phone_exist
                 FROM apps.ar_phones_v
                WHERE     owner_table_name = 'HZ_PARTIES'
                      AND owner_table_id = ln_party_id
                      AND CONTACT_POINT_PURPOSE = 'BUSINESS'
                      AND phone_type = 'EMAIL'
                      AND UPPER (EMAIL_ADDRESS) =
                             UPPER (rec_cust.email_address)
                      AND ROWNUM = 1;
            EXCEPTION
               WHEN NO_DATA_FOUND
               THEN
                  l_phone_exist := 'N';
               WHEN OTHERS
               THEN
                  l_phone_exist := 'N';
            END;

            IF l_phone_exist = 'N'
            THEN
               pcontactpointrec := NULL;
               pemailrec.email_address := rec_cust.email_address;
               pemailrec.email_format := 'MAILHTML';
               pcontactpointrec.contact_point_type := 'EMAIL';
               pcontactpointrec.owner_table_name := 'HZ_PARTIES';
               pcontactpointrec.owner_table_id := ln_party_id;
               pcontactpointrec.primary_flag := 'Y';
               pcontactpointrec.contact_point_purpose := 'BUSINESS';
               pcontactpointrec.created_by_module := l_api_name;

               hz_contact_point_v2pub.create_contact_point (
                  p_init_msg_list       => 'T',
                  p_contact_point_rec   => pcontactpointrec,
                  p_email_rec           => pemailrec,
                  x_contact_point_id    => o_contact_pointid,
                  x_return_status       => o_ret_status,
                  x_msg_count           => o_msg_count,
                  x_msg_data            => o_msg_data);

               IF o_ret_status <> 'S'
               THEN
                  return_msg (o_msg_count, o_msg_data, o_return_msg);
                  x_msg_data := 'Email Updation: ' || o_return_msg;
                  debug (p_record_id, x_msg_data);
                  x_ret_status := o_ret_status;
                  ROLLBACK TO customer_record;
                  RETURN;
               END IF;
            END IF;
         END IF;


         --====================================================================
         -- Create Invoice email record for the party
         --====================================================================

         l_sec := 'Creating Invoice Email Record contact for Party';
         debug (p_record_id, l_sec);

         IF rec_cust.invoice_email_address IS NOT NULL
         THEN
            l_phone_exist := NULL;

            BEGIN
               SELECT 'Y'
                 INTO l_phone_exist
                 FROM apps.ar_phones_v
                WHERE     owner_table_name = 'HZ_PARTIES'
                      AND owner_table_id = ln_party_id
                      AND phone_type = 'EMAIL'
                      AND CONTACT_POINT_PURPOSE = 'INVOICE'
                      AND UPPER (EMAIL_ADDRESS) =
                             UPPER (rec_cust.invoice_email_address)
                      AND ROWNUM = 1;
            EXCEPTION
               WHEN NO_DATA_FOUND
               THEN
                  l_phone_exist := 'N';
               WHEN OTHERS
               THEN
                  l_phone_exist := 'N';
            END;

            debug (p_record_id, ' Email Address Exists ' || l_phone_exist);

            IF l_phone_exist = 'N'
            THEN
               pcontactpointrec := NULL;
               pemailrec.email_address := rec_cust.invoice_email_address;
               pemailrec.email_format := 'MAILHTML';
               pcontactpointrec.contact_point_type := 'EMAIL';
               pcontactpointrec.owner_table_name := 'HZ_PARTIES';
               pcontactpointrec.owner_table_id := o_party_id;
               pcontactpointrec.contact_point_purpose := 'INVOICE';
               pcontactpointrec.created_by_module := l_api_name;

               hz_contact_point_v2pub.create_contact_point (
                  p_init_msg_list       => 'T',
                  p_contact_point_rec   => pcontactpointrec,
                  p_email_rec           => pemailrec,
                  x_contact_point_id    => o_contact_pointid,
                  x_return_status       => o_ret_status,
                  x_msg_count           => o_msg_count,
                  x_msg_data            => o_msg_data);

               IF o_ret_status <> 'S'
               THEN
                  return_msg (o_msg_count, o_msg_data, o_return_msg);
                  x_msg_data := 'Invoice Email Creation: ' || o_return_msg;
                  debug (p_record_id, x_msg_data);
                  x_ret_status := o_ret_status;
                  ROLLBACK TO customer_record;
                  RETURN;
               END IF;
            END IF;
         END IF;

         --====================================================================
         -- Customer Level Master Data Update
         --====================================================================

         l_sec := 'Customer Level Master Data Update';
         debug (p_record_id, l_sec);

         FOR rec_cust_mast IN cur_cust_mast
         LOOP
            l_sec := 'Customer Level Master Data Update inside loop';
            debug (p_record_id, l_sec);

            lvc_upd_flag := 'N';

            -- Business Name update
            IF     rec_cust_mast.acct_name <> rec_cust_mast.stg_name
               AND rec_cust_mast.stg_name IS NOT NULL
            THEN
               pcustaccountrec.account_name := rec_cust_mast.stg_name;
               lvc_account_name := rec_cust_mast.stg_name;
               lvc_upd_flag := 'Y';
            END IF;

            -- status update
            IF     rec_cust_mast.acct_status <> rec_cust_mast.stg_status
               AND rec_cust_mast.stg_status IS NOT NULL
            THEN
               pcustaccountrec.status := rec_cust_mast.stg_status;
               lvc_upd_flag := 'Y';
            END IF;

            -- Type of Business update
            IF     rec_cust_mast.acct_cust_class_code <>
                      rec_cust_mast.stg_cust_class_code
               AND rec_cust_mast.stg_cust_class_code IS NOT NULL
            THEN
               pcustaccountrec.customer_class_code :=
                  rec_cust_mast.stg_cust_class_code;
               lvc_upd_flag := 'Y';
            END IF;

            -- Predominant Trace Update
            IF     rec_cust_mast.acct_Pred_trade <>
                      rec_cust_mast.stg_Pred_trade
               AND rec_cust_mast.stg_Pred_trade IS NOT NULL
            THEN
               pcustaccountrec.attribute9 := rec_cust_mast.stg_Pred_trade;
               lvc_upd_flag := 'Y';
            END IF;

            -- Sales Rep Update
            IF     rec_cust_mast.acct_salesrep_id <>
                      rec_cust_mast.stg_salesrep_id
               AND rec_cust_mast.stg_salesrep_id IS NOT NULL
            THEN
               BEGIN
                  l_res_category := NULL;

                  SELECT jrre.category
                    INTO l_res_category
                    FROM apps.jtf_rs_salesreps_mo_v jrs,
                         jtf_rs_resource_extns_tl jrre
                   WHERE     1 = 1
                         AND jrs.resource_id = jrre.resource_id
                         AND jrs.resource_id = rec_cust_mast.stg_salesrep_id
                         AND jrs.STATUS = 'A'
                         AND ROWNUM = 1;

                  IF     l_res_category = 'OTHER'
                     AND rec_cust_mast.stg_salesrep_id NOT IN (100000617,
                                                               100000650)
                  THEN
                     pcustaccountrec.sales_channel_code := 'BQU_UNKNOWN';
                  ELSIF     l_res_category = 'OTHER'
                        AND rec_cust_mast.stg_salesrep_id IN (100000617,
                                                              100000650)
                  THEN
                     pcustaccountrec.sales_channel_code := 'ACCT_MGR';
                  ELSIF l_res_category = 'EMPLOYEE'
                  THEN
                     pcustaccountrec.sales_channel_code := 'ACCT_MGR';
                  ELSE
                     pcustaccountrec.sales_channel_code := NULL;
                  END IF;
               EXCEPTION
                  WHEN OTHERS
                  THEN
                     pcustaccountrec.sales_channel_code := NULL;
               END;

               lvc_upd_flag := 'Y';
            END IF;

            IF lvc_upd_flag = 'Y'
            THEN
               o_ret_status := NULL;
               o_msg_count := NULL;
               o_msg_data := NULL;
               pcustaccountrec.cust_account_id :=
                  rec_cust_mast.cust_account_id;

               HZ_CUST_ACCOUNT_V2PUB.update_cust_account (
                  p_init_msg_list           => 'T',
                  p_cust_account_rec        => pcustaccountrec,
                  p_object_version_number   => rec_cust_mast.object_version_number,
                  x_return_status           => o_ret_status,
                  x_msg_count               => o_msg_count,
                  x_msg_data                => o_msg_data);

               IF o_ret_status <> 'S'
               THEN
                  return_msg (o_msg_count, o_msg_data, o_return_msg);
                  x_msg_data := 'Updating Customer: ' || o_return_msg;
                  debug (p_record_id, x_msg_data);
                  x_ret_status := o_ret_status;
                  ROLLBACK TO customer_record;
                  RETURN;
               END IF;
            END IF;

            --====================================================================
            -- Party Name Update.
            --====================================================================

            l_sec := ' Party Level Data Update';
            debug (p_record_id, l_sec);

            IF (    rec_cust_mast.acct_name <> rec_cust_mast.stg_name
                AND rec_cust_mast.stg_name IS NOT NULL)
            THEN
               BEGIN
                  o_ret_status := NULL;
                  o_msg_count := NULL;
                  o_msg_data := NULL;

                  p_party_rec.party_id := rec_cust_mast.party_id;
                  p_organization_rec.party_rec := p_party_rec;

                  IF rec_cust_mast.stg_name IS NOT NULL
                  THEN
                     p_organization_rec.organization_name :=
                        rec_cust_mast.stg_name;
                  END IF;

                  BEGIN
                     HZ_PARTY_V2PUB.update_organization (
                        p_init_msg_list                 => FND_API.G_FALSE,
                        p_organization_rec              => p_organization_rec,
                        p_party_object_version_number   => rec_cust_mast.party_object_version_number,
                        x_profile_id                    => l_profile_id,
                        x_return_status                 => o_ret_status,
                        x_msg_count                     => o_msg_count,
                        x_msg_data                      => o_msg_data);
                  END;

                  IF o_ret_status <> 'S'
                  THEN
                     return_msg (o_msg_count, o_msg_data, o_return_msg);
                     x_msg_data := 'Updating Customer: ' || o_return_msg;
                     debug (p_record_id, x_msg_data);
                     x_ret_status := o_ret_status;
                     ROLLBACK TO customer_record;
                     RETURN;
                  END IF;
               END;
            END IF;
         END LOOP;
      END LOOP;


      --====================================================================
      -- Customer Profile Data Update.
      --====================================================================

      l_sec := ' Customer Profile Data Update';
      debug (p_record_id, l_sec);

      FOR rec_cust_prf IN cur_cust_prf
      LOOP
         l_sec := ' Customer Profile Data Update insde loop';
         debug (p_record_id, l_sec);

         pcustomerprofile := NULL;

         lvc_upd_flag := NULL;

         IF rec_cust_prf.stg_crd_rating IS NOT NULL
         THEN
            pcustomerprofile.credit_rating := rec_cust_prf.stg_crd_rating;
         ELSE
            pcustomerprofile.credit_rating := rec_cust_prf.acct_crd_rating;
         END IF;

         IF rec_cust_prf.stg_collector_id IS NOT NULL
         THEN
            pcustomerprofile.collector_id := rec_cust_prf.stg_collector_id;
            pcustomerprofile.credit_analyst_id := ln_credit_analyst;
         ELSE
            ln_acct_collector_id := rec_cust_prf.acct_collector_id;
            ln_credit_analyst_id := rec_cust_prf.credit_analyst_id;
            pcustomerprofile.collector_id := rec_cust_prf.acct_collector_id;
            pcustomerprofile.credit_analyst_id :=
               rec_cust_prf.credit_analyst_id;
         END IF;

         IF rec_cust_prf.stg_acct_status IS NOT NULL
         THEN
            pcustomerprofile.account_status := rec_cust_prf.stg_acct_status;
         ELSE
            pcustomerprofile.account_status := rec_cust_prf.acct_acct_status;
            lvc_acct_status := rec_cust_prf.acct_acct_status;
         END IF;

         BEGIN
            SELECT inv_remit_to_code
              INTO pcustomerprofile.attribute2
              FROM xxwc_ar_cm_remitcode_tbl
             WHERE area_code = SUBSTR (ln_business_phone, 1, 3);
         EXCEPTION
            WHEN OTHERS
            THEN
               pcustomerprofile.attribute2 := 2;
         END;

         IF rec_cust_prf.stg_prof_class_id IS NOT NULL
         THEN
            ln_stg_profile_id := rec_cust_prf.stg_prof_class_id;
            pcustomerprofile.profile_class_id := ln_stg_profile_id;
         ELSE
            pcustomerprofile.profile_class_id :=
               rec_cust_prf.acct_prof_class_id;
            ln_acct_prof_class_id := rec_cust_prf.acct_prof_class_id;
         END IF;

         pcustomerprofile.cust_account_profile_id :=
            rec_cust_prf.cust_account_profile_id;

         HZ_CUSTOMER_PROFILE_V2PUB.update_customer_profile (
            p_init_msg_list           => 'T',
            p_customer_profile_rec    => pcustomerprofile,
            p_object_version_number   => rec_cust_prf.object_Version_number,
            x_return_status           => o_ret_status,
            x_msg_count               => o_msg_count,
            x_msg_data                => o_msg_data);

         IF o_ret_status <> 'S'
         THEN
            return_msg (o_msg_count, o_msg_data, o_return_msg);
            x_msg_data := 'Updating Profile: ' || o_return_msg;
            debug (p_record_id, x_msg_data);
            x_ret_status := o_ret_status;
            ROLLBACK TO customer_record;
            RETURN;
         END IF;
      END LOOP;


      --====================================================================
      --Update amount profile at Account Level
      --====================================================================
      l_sec := ' Customer Profile Amount Data Update';
      debug (p_record_id, l_sec);

      IF ln_stg_crd_ltd IS NOT NULL OR ln_stg_dflt_crd_ltd IS NOT NULL
      THEN
         SELECT A.ATTRIBUTE1
           INTO lvc_yard_job_accnt_project
           FROM APPS.HZ_CUST_SITE_USES_ALL A, APPS.HZ_CUST_ACCT_SITES_ALL B
          WHERE     A.CUST_ACCT_SITE_ID = B.CUST_ACCT_SITE_ID
                AND B.CUST_ACCOUNT_ID = ln_cust_account_id
                AND A.ATTRIBUTE1 IS NOT NULL
                AND ROWNUM = 1;

         BEGIN
            SELECT CUST_ACCOUNT_PROFILE_ID
              INTO ol_profile_id
              FROM APPS.HZ_CUSTOMER_PROFILES
             WHERE     CUST_ACCOUNT_ID = ln_cust_account_id
                   AND SITE_USE_ID IS NULL;
         EXCEPTION
            WHEN OTHERS
            THEN
               ol_profile_id := NULL;
         END;

         pcustproamtrec := NULL;

         IF ol_profile_id IS NOT NULL
         THEN
            BEGIN
               SELECT cust_acct_profile_amt_id, object_version_number
                 INTO o_custacctprofileamtid, p_object_version_number
                 FROM apps.hz_cust_profile_amts
                WHERE     cust_account_profile_id = ol_profile_id
                      AND currency_code = lv_currency_code;
            EXCEPTION
               WHEN NO_DATA_FOUND
               THEN
                  o_custacctprofileamtid := NULL;
                  p_object_version_number := NULL;
            END;

            IF lvc_yard_job_accnt_project IN ('PROJECT', 'YARD', 'JOB')
            THEN
               pcustproamtrec.overall_credit_limit := 0;
            ELSIF lvc_yard_job_accnt_project IN ('ACCOUNT')
            THEN
               pcustproamtrec.overall_credit_limit := ln_stg_crd_ltd;
            ELSE
               pcustproamtrec.overall_credit_limit := 0;
            END IF;

            IF ln_stg_dflt_crd_ltd IS NOT NULL
            THEN
               pcustproamtrec.attribute1 := ln_stg_dflt_crd_ltd;
            END IF;


            IF o_custacctprofileamtid IS NOT NULL
            THEN
               --Update amount profile
               pcustproamtrec.cust_acct_profile_amt_id :=
                  o_custacctprofileamtid;
               pcustproamtrec.created_by_module := l_api_name;
               hz_customer_profile_v2pub.update_cust_profile_amt (
                  p_init_msg_list           => 'T',
                  p_cust_profile_amt_rec    => pcustproamtrec,
                  p_object_version_number   => p_object_version_number,
                  x_return_status           => o_ret_status,
                  x_msg_count               => o_msg_count,
                  x_msg_data                => o_msg_data);
            ELSE
               pcustproamtrec.cust_account_profile_id := ol_profile_id;
               pcustproamtrec.cust_account_id := ln_cust_account_id;
               pcustproamtrec.currency_code := lv_currency_code;
               pcustproamtrec.created_by_module := l_api_name;
               hz_customer_profile_v2pub.create_cust_profile_amt (
                  p_init_msg_list              => 'T',
                  p_check_foreign_key          => 'T',
                  p_cust_profile_amt_rec       => pcustproamtrec,
                  x_cust_acct_profile_amt_id   => o_custacctprofileamtid,
                  x_return_status              => o_ret_status,
                  x_msg_count                  => o_msg_count,
                  x_msg_data                   => o_msg_data);
            END IF;

            IF o_ret_status <> 'S'
            THEN
               return_msg (o_msg_count, o_msg_data, o_return_msg);
               x_msg_data := 'Account Level Profile amounts: ' || o_return_msg;
               debug (p_record_id, x_msg_data);
               x_ret_status := o_ret_status;
               ROLLBACK TO customer_record;
               RETURN;
            END IF;
         END IF;
      END IF;


      o_party_id := ln_party_id;
      o_cust_account_id := ln_cust_account_id;

      --====================================================================
      -- Inactivating Existing Party sites.
      --====================================================================
      l_sec := 'Inactivating Existing Party sites.';
      debug (p_record_id, l_sec);

      o_ret_status := NULL;
      o_msg_count := NULL;
      o_msg_data := NULL;

      XXWC_AR_EQUIFAX_CUST_ACCT_PKG.inactivate_party_sites (
         p_record_id    => p_record_id,
         x_ret_status   => o_ret_status,
         x_msg_count    => o_msg_count,
         x_msg_data     => o_msg_data);

      IF o_ret_status <> 'S'
      THEN
         return_msg (o_msg_count, o_msg_data, o_return_msg);
         x_msg_data := 'Inactivating Party sites: ' || o_return_msg;
         debug (p_record_id, x_msg_data);
         x_ret_status := o_ret_status;
         ROLLBACK TO customer_record;
         RETURN;
      END IF;

      --====================================================================
      -- Inactivating Existing Account Site sites.
      --====================================================================

      l_sec := 'Inactivating Existing Account sites.';
      debug (p_record_id, l_sec);

      o_ret_status := NULL;
      o_msg_count := NULL;
      o_msg_data := NULL;

      XXWC_AR_EQUIFAX_CUST_ACCT_PKG.inactivate_acct_sites (
         p_record_id    => p_record_id,
         x_ret_status   => x_ret_status,
         x_msg_count    => x_msg_count,
         x_msg_data     => x_msg_data);

      IF o_ret_status <> 'S'
      THEN
         return_msg (o_msg_count, o_msg_data, o_return_msg);
         x_msg_data := 'Inactivating Account sites: ' || o_return_msg;
         debug (p_record_id, x_msg_data);
         x_ret_status := o_ret_status;
         ROLLBACK TO customer_record;
         RETURN;
      END IF;



      FOR rec_cust IN cur_cust
      LOOP
         --====================================================================
         -- create location for the primary bill to
         --====================================================================
         l_sec := 'create location for the primary bill to';
         debug (p_record_id, l_sec);


         plocationrec := NULL;
         ppartysiterec := NULL;
         pcustacctsiterec := NULL;
         pcustacctsiteuserec := NULL;
         pcustomerprofile := NULL;
         o_ret_status := NULL;
         o_msg_count := NULL;
         o_msg_data := NULL;
         o_location_id := NULL;

         IF rec_cust.BILLING_COUNTRY IS NOT NULL
         THEN
            BEGIN
               SELECT TERRITORY_CODE
                 INTO plocationrec.country
                 FROM HR_TERRITORIES_V
                WHERE UPPER (TERRITORY_SHORT_NAME) =
                         UPPER (rec_cust.BILLING_COUNTRY);
            EXCEPTION
               WHEN OTHERS
               THEN
                  plocationrec.country := NULL;
                  return_msg (o_msg_count, o_msg_data, o_return_msg);
                  o_return_msg := 'Invalid Billing Country ' || o_return_msg;
                  o_return_status := o_ret_status;
                  ROLLBACK TO customer_record;
                  RETURN;
            END;
         ELSE
            plocationrec.country := NULL;
         END IF;


         lvc_billing_city := rec_cust.BILLING_CITY;


         plocationrec.postal_code := rec_cust.BILLING_ZIP_CODE;
         plocationrec.address1 := rec_cust.BILLING_ADDRESS;
         plocationrec.state := rec_cust.BILLING_STATE;
         plocationrec.city := rec_cust.BILLING_CITY;
         plocationrec.county := rec_cust.BILLING_COUNTY;
         plocationrec.created_by_module := l_api_name;
         plocationrec.sales_tax_geocode := NULL;

         hz_location_v2pub.create_location (
            p_init_msg_list   => 'T',
            p_location_rec    => plocationrec,
            x_location_id     => o_location_id,
            x_return_status   => o_ret_status,
            x_msg_count       => o_msg_count,
            x_msg_data        => o_msg_data);

         ln_bill_location_id := o_location_id;


         IF o_ret_status <> 'S'
         THEN
            return_msg (o_msg_count, o_msg_data, o_return_msg);
            x_msg_data := 'Location Creation: ' || o_return_msg;
            debug (p_record_id, x_msg_data);
            x_ret_status := o_ret_status;
            ROLLBACK TO customer_record;
            RETURN;
         END IF;

         l_sec :=
               'Location Account for Bill To Site Creation Process Completed - API Status'
            || o_ret_status;

         SAVEPOINT customer_record;

         --====================================================================
         -- Create Party Site for the primary bill to
         --====================================================================

         l_sec := 'Create Party Site for the primary bill to';
         debug (p_record_id, l_sec);


         o_ret_status := NULL;
         o_msg_count := NULL;
         o_msg_data := NULL;
         o_party_site_id := NULL;
         o_party_site_no := NULL;
         o_sprofile_id := NULL;
         ppartysiterec.created_by_module := l_api_name;

         -- create a party site now
         ppartysiterec.party_id := o_party_id;
         ppartysiterec.location_id := o_location_id;
         ppartysiterec.identifying_address_flag := 'Y';

         hz_party_site_v2pub.create_party_site (
            p_init_msg_list       => 'T',
            p_party_site_rec      => ppartysiterec,
            x_party_site_id       => o_party_site_id,
            x_party_site_number   => o_party_site_no,
            x_return_status       => o_ret_status,
            x_msg_count           => o_msg_count,
            x_msg_data            => o_msg_data);

         IF o_ret_status <> 'S'
         THEN
            return_msg (o_msg_count, o_msg_data, o_return_msg);
            x_msg_data := 'Party Site Creation: ' || o_return_msg;
            debug (p_record_id, x_msg_data);
            x_ret_status := o_ret_status;
            ROLLBACK TO customer_record;
            RETURN;
         END IF;

         l_sec :=
               'Party Site Creation Process Completed - API Status '
            || o_ret_status;

         debug (p_record_id, l_sec);

         --====================================================================
         -- Create cust acct Primary Bill To site
         --====================================================================

         l_sec := 'Create cust acct Primary Bill To site';
         debug (p_record_id, l_sec);

         pcustacctsiterec := NULL;
         o_ret_status := NULL;
         o_msg_count := NULL;
         o_msg_data := NULL;
         o_cust_acct_site_id := NULL;
         pcustacctsiterec.cust_account_id := o_cust_account_id;
         pcustacctsiterec.party_site_id := o_party_site_id;
         pcustacctsiterec.created_by_module := l_api_name;
         pcustacctsiterec.org_id := ln_org_id;
         pcustacctsiterec.attribute3 :=
            SUBSTR (NVL(rec_cust.purchase_order_required,'N'), 1, 1);
         pcustacctsiterec.attribute_category := 'No';
         pcustacctsiterec.attribute1 := '2';
         pcustacctsiterec.attribute16 := 'N';
         pcustacctsiterec.attribute15 := 'None';
         pcustacctsiterec.attribute14 := 'N';

         BEGIN
            SELECT CASE
                      WHEN customer_class_code = 'COMMERCIAL'
                      THEN
                         'PUBLIC'
                      WHEN customer_class_code = 'GOVERNMENT'
                      THEN
                         'PRIVATE'
                      WHEN customer_class_code = 'NON-CONTRACTOR/OTHER'
                      THEN
                         'PUBLIC'
                      WHEN customer_class_code = 'RESELLER'
                      THEN
                         'PUBLIC'
                      WHEN customer_class_code = 'RESIDENTIAL_CONTRACTOR'
                      THEN
                         'RES'
                      WHEN customer_class_code = 'UNKNOWN'
                      THEN
                         'PUBLIC'
                      ELSE
                         NULL
                   END
              INTO pcustacctsiterec.customer_category_code
              FROM hz_cust_accounts
             WHERE cust_account_id = o_cust_account_id;
         EXCEPTION
            WHEN OTHERS
            THEN
               pcustacctsiterec.customer_category_code := NULL;
         END;

         hz_cust_account_site_v2pub.create_cust_acct_site (
            p_init_msg_list        => 'T',
            p_cust_acct_site_rec   => pcustacctsiterec,
            x_cust_acct_site_id    => o_cust_acct_site_id,
            x_return_status        => o_ret_status,
            x_msg_count            => o_msg_count,
            x_msg_data             => o_msg_data);

         IF o_ret_status <> 'S'
         THEN
            return_msg (o_msg_count, o_msg_data, o_return_msg);
            x_msg_data := 'Party Site Creation: ' || o_return_msg;
            debug (p_record_id, x_msg_data);
            x_ret_status := o_ret_status;
            ROLLBACK TO customer_record;
            RETURN;
         END IF;

         l_sec :=
               'Cust Acct Site Creation Process Completed - API Status '
            || o_ret_status;
         debug (p_record_id, l_sec);


         --====================================================================
         -- Create site use for primary bill to
         --====================================================================

         l_sec := 'Create site use for primary bill to';
         debug (p_record_id, l_sec);

         pcustacctsiteuserec := NULL;
         o_ret_status := NULL;
         o_msg_count := NULL;
         o_msg_data := NULL;
         o_cust_acct_site_use_id := NULL;

         pcustacctsiteuserec.site_use_code := 'BILL_TO';
         pcustacctsiteuserec.cust_acct_site_id := o_cust_acct_site_id;

         pcustacctsiteuserec.attribute1 := 'MSTR';

         pcustacctsiteuserec.primary_flag := 'Y';

         pcustacctsiteuserec.location := rec_cust.business_name;

         pcustacctsiteuserec.created_by_module := l_api_name;
         pcustomerprofile.credit_checking := 'Y';

         BEGIN
            SELECT inv_remit_to_code
              INTO pcustomerprofile.attribute2
              FROM xxwc_ar_cm_remitcode_tbl
             WHERE area_code = SUBSTR (rec_cust.business_phone, 1, 3);
         EXCEPTION
            WHEN OTHERS
            THEN
               pcustomerprofile.attribute2 := 2;
         END;

         pcustomerprofile.attribute3 := 'Y';
         pcustomerprofile.collector_id := rec_cust.CREDIT_MANAGER;

         IF ln_credit_analyst IS NOT NULL
         THEN
            pcustomerprofile.credit_analyst_id := ln_credit_analyst;
         END IF;

         IF rec_cust.account_status IS NOT NULL
         THEN
            BEGIN
               SELECT LOOKUP_CODE
                 INTO pcustomerprofile.account_status
                 FROM APPS.FND_LOOKUP_VALUES
                WHERE     LOOKUP_TYPE = 'ACCOUNT_STATUS'
                      AND LOOKUP_CODE = rec_cust.account_status;
            EXCEPTION
               WHEN OTHERS
               THEN
                  pcustomerprofile.account_status := NULL;
            END;
         ELSE
            pcustomerprofile.account_status := 'ACTIVE';
         END IF;

         IF rec_cust.credit_decisioning IS NOT NULL
         THEN
            pcustomerprofile.credit_rating := rec_cust.credit_decisioning;
         ELSE
            pcustomerprofile.credit_rating := 'Y';
         END IF;

         pcustomerprofile.tolerance := 0;

         IF rec_cust.salesrep_id IS NOT NULL
         THEN
            BEGIN
               SELECT SALESREP_ID
                 INTO pcustacctsiteuserec.primary_salesrep_id
                 FROM jtf_rs_salesreps_mo_v
                WHERE resource_id = rec_cust.salesrep_id;
            EXCEPTION
               WHEN OTHERS
               THEN
                  pcustacctsiteuserec.primary_salesrep_id := NULL;
            END;
         END IF;

         IF rec_cust.credit_classification IS NOT NULL
         THEN
            SELECT LOOKUP_CODE
              INTO pcustomerprofile.credit_classification
              FROM ar_lookups
             WHERE     lookup_type LIKE 'AR_CMGT_CREDIT_CLASSIFICATION'
                   AND LOOKUP_CODE = rec_cust.credit_classification;
         ELSE
            pcustomerprofile.credit_classification := 'MODERATE';
         END IF;		 
	 

         BEGIN
            SELECT profile_class_id
              INTO pcustomerprofile.profile_class_id
              FROM HZ_CUST_PROFILE_CLASSES
             WHERE NAME = rec_cust.PROFILE_CLASS;
         EXCEPTION
            WHEN OTHERS
            THEN
               return_msg (o_msg_count, o_msg_data, o_return_msg);
               x_msg_data := 'Invalid Profile Class -  ' || o_return_msg;
               x_ret_status := o_ret_status;
               ROLLBACK TO customer_record;
               RETURN;
         END;

         hz_cust_account_site_v2pub.create_cust_site_use (
            p_init_msg_list          => 'T',
            p_cust_site_use_rec      => pcustacctsiteuserec,
            p_customer_profile_rec   => pcustomerprofile,
            p_create_profile         => 'T',
            p_create_profile_amt     => 'T',
            x_site_use_id            => o_cust_acct_site_use_id,
            x_return_status          => o_ret_status,
            x_msg_count              => o_msg_count,
            x_msg_data               => o_msg_data);

         ln_ship_site_use_id := o_cust_acct_site_use_id;

         IF o_ret_status <> 'S'
         THEN
            return_msg (o_msg_count, o_msg_data, o_return_msg);
            x_msg_data := 'Account Site Creation(Bill To): ' || o_return_msg;
            debug (p_record_id, x_msg_data);
            x_ret_status := o_ret_status;
            ROLLBACK TO customer_record;
            RETURN;
         END IF;

         l_sec :=
               'Cust Acct Site Use Creation Process Completed - API Status '
            || o_ret_status;

         debug (p_record_id, l_sec);

         SAVEPOINT customer_record;

         --====================================================================
         -- create site Primary Bill to profile amount
         --====================================================================

         l_sec := 'create site Primary Bill to profile amount';
         debug (p_record_id, l_sec);


         pcustproamtrec := NULL;
         o_sprofile_id := NULL;
         o_ret_status := NULL;
         o_msg_count := NULL;
         o_msg_data := NULL;


         BEGIN
            SELECT cust_account_profile_id
              INTO o_sprofile_id
              FROM apps.hz_customer_profiles
             WHERE     cust_account_id = o_cust_account_id
                   AND site_use_id = o_cust_acct_site_use_id;
         EXCEPTION
            WHEN NO_DATA_FOUND
            THEN
               o_sprofile_id := NULL;
         END;


         IF o_sprofile_id IS NOT NULL
         THEN
            BEGIN
               SELECT cust_acct_profile_amt_id, object_version_number
                 INTO o_cust_acct_profile_amt_id, p_object_version_number
                 FROM apps.hz_cust_profile_amts
                WHERE     cust_account_profile_id = o_sprofile_id
                      AND currency_code = lv_currency_code;
            EXCEPTION
               WHEN NO_DATA_FOUND
               THEN
                  o_cust_acct_profile_amt_id := NULL;
                  p_object_version_number := NULL;
            END;

            pcustproamtrec.overall_credit_limit := 0;

            IF o_cust_acct_profile_amt_id IS NOT NULL
            THEN
               --Update amount profile
               pcustproamtrec.cust_acct_profile_amt_id :=
                  o_cust_acct_profile_amt_id;
               pcustproamtrec.created_by_module := l_api_name;

               hz_customer_profile_v2pub.update_cust_profile_amt (
                  p_init_msg_list           => 'T',
                  p_cust_profile_amt_rec    => pcustproamtrec,
                  p_object_version_number   => p_object_version_number,
                  x_return_status           => o_ret_status,
                  x_msg_count               => o_msg_count,
                  x_msg_data                => o_msg_data);
            ELSE
               --Create Profile Amount
               pcustproamtrec.cust_account_profile_id := o_sprofile_id;
               pcustproamtrec.cust_account_id := o_cust_account_id;
               pcustproamtrec.site_use_id := o_cust_acct_site_use_id;
               pcustproamtrec.currency_code := lv_currency_code;
               pcustproamtrec.created_by_module := l_api_name;
               hz_customer_profile_v2pub.create_cust_profile_amt (
                  p_init_msg_list              => 'T',
                  p_check_foreign_key          => 'T',
                  p_cust_profile_amt_rec       => pcustproamtrec,
                  x_cust_acct_profile_amt_id   => o_cust_acct_profile_amt_id,
                  x_return_status              => o_ret_status,
                  x_msg_count                  => o_msg_count,
                  x_msg_data                   => o_msg_data);
            END IF;

            IF o_ret_status <> 'S'
            THEN
               return_msg (o_msg_count, o_msg_data, o_return_msg);
               x_msg_data := 'Profile Amount Creation: ' || o_return_msg;
               debug (p_record_id, x_msg_data);
               x_ret_status := o_ret_status;
               ROLLBACK TO customer_record;
               RETURN;
            END IF;

            --====================================================================
            -- Create Primary Bill to usage rules
            --====================================================================

            l_sec := 'Create Primary Bill to usage rules';
            debug (p_record_id, l_sec);

            hz_credit_usages_cascade_pkg.cascade_credit_usage_rules (
               p_cust_acct_profile_amt_id   => o_cust_acct_profile_amt_id,
               p_cust_profile_id            => o_sprofile_id,
               p_profile_class_amt_id       => NULL,
               p_profile_class_id           => NULL,
               x_return_status              => o_ret_status,
               x_msg_count                  => o_msg_count,
               x_msg_data                   => o_msg_data);

            IF o_ret_status <> 'S'
            THEN
               return_msg (o_msg_count, o_msg_data, o_return_msg);
               x_msg_data := 'Profile Amounts Usage Rules: ' || o_return_msg;
               debug (p_record_id, x_msg_data);
               x_ret_status := o_ret_status;
               ROLLBACK TO customer_record;
               RETURN;
            END IF;
         END IF;

         --====================================================================
         -- create location for the Ship_to
         --====================================================================

         l_sec := 'create location for the Ship_to';
         debug (p_record_id, l_sec);

         plocationrec := NULL;
         ppartysiterec := NULL;
         pcustacctsiterec := NULL;
         pcustacctsiteuserec := NULL;
         pcustomerprofile := NULL;
         o_ret_status := NULL;
         o_msg_count := NULL;
         o_msg_data := NULL;

         o_location_id := NULL;

         BEGIN
            SELECT territory_code
              INTO plocationrec.country
              FROM hr_territories_v
             WHERE UPPER (territory_short_name) =
                      UPPER (rec_cust.shipping_country);
         EXCEPTION
            WHEN OTHERS
            THEN
               plocationrec.country := NULL;
               return_msg (o_msg_count, o_msg_data, o_return_msg);
               x_msg_data := 'Invalid Shipping Country ' || o_return_msg;
               x_ret_status := o_ret_status;
               ROLLBACK TO customer_record;
               RETURN;
         END;

         plocationrec.postal_code := rec_cust.shipping_zip_code;
         plocationrec.address1 := rec_cust.shipping_address;
         plocationrec.state := rec_cust.shipping_STATE;
         plocationrec.city := rec_cust.shipping_CITY;
         plocationrec.county := rec_cust.shipping_COUNTY;
         plocationrec.created_by_module := l_api_name;
         plocationrec.sales_tax_geocode := NULL;

         hz_location_v2pub.create_location (
            p_init_msg_list   => 'T',
            p_location_rec    => plocationrec,
            x_location_id     => o_location_id,
            x_return_status   => o_ret_status,
            x_msg_count       => o_msg_count,
            x_msg_data        => o_msg_data);


         IF o_ret_status <> 'S'
         THEN
            return_msg (o_msg_count, o_msg_data, o_return_msg);
            x_msg_data := 'Location Creation: ' || o_return_msg;
            debug (p_record_id, x_msg_data);
            x_ret_status := o_ret_status;
            ROLLBACK TO customer_record;
            RETURN;
         END IF;

         --====================================================================
         -- Create Party Site for the Ship to
         --====================================================================

         l_sec := 'Create Party Site for the Ship to';
         debug (p_record_id, l_sec);


         o_ret_status := NULL;
         o_msg_count := NULL;
         o_msg_data := NULL;
         o_party_site_id := NULL;
         o_party_site_no := NULL;
         o_sprofile_id := NULL;
         ppartysiterec.created_by_module := l_api_name;
         ppartysiterec.party_id := o_party_id;
         ppartysiterec.location_id := o_location_id;
         ppartysiterec.identifying_address_flag := 'Y';

         hz_party_site_v2pub.create_party_site (
            p_init_msg_list       => 'T',
            p_party_site_rec      => ppartysiterec,
            x_party_site_id       => o_party_site_id,
            x_party_site_number   => o_party_site_no,
            x_return_status       => o_ret_status,
            x_msg_count           => o_msg_count,
            x_msg_data            => o_msg_data);

         IF o_ret_status <> 'S'
         THEN
            return_msg (o_msg_count, o_msg_data, o_return_msg);
            x_msg_data := 'Ship to Party Site Creation: ' || o_return_msg;
            debug (p_record_id, x_msg_data);
            x_ret_status := o_ret_status;
            ROLLBACK TO customer_record;
            RETURN;
         END IF;

         --====================================================================
         -- Create Shipto cust acct site
         --====================================================================

         l_sec := 'Create Shipto cust acct site';
         debug (p_record_id, l_sec);


         o_ret_status := NULL;
         o_msg_count := NULL;
         o_msg_data := NULL;
         o_cust_acct_site_id := NULL;
         pcustacctsiterec.cust_account_id := o_cust_account_id;
         pcustacctsiterec.party_site_id := o_party_site_id;
         pcustacctsiterec.created_by_module := l_api_name;
         pcustacctsiterec.org_id := ln_org_id;
         pcustacctsiterec.attribute3 := SUBSTR (NVL(rec_cust.purchase_order_required,'N'), 1, 1);		 
         --pcustacctsiterec.attribute3 := 'N';
         pcustacctsiterec.attribute_category := 'No';
         pcustacctsiterec.attribute1 := '2';
         pcustacctsiterec.attribute16 := 'N';
         pcustacctsiterec.attribute15 := 'None';
         pcustacctsiterec.attribute14 := 'N';

         BEGIN
            SELECT CASE
                      WHEN customer_class_code = 'COMMERCIAL'
                      THEN
                         'PUBLIC'
                      WHEN customer_class_code = 'GOVERNMENT'
                      THEN
                         'PRIVATE'
                      WHEN customer_class_code = 'NON-CONTRACTOR/OTHER'
                      THEN
                         'PUBLIC'
                      WHEN customer_class_code = 'RESELLER'
                      THEN
                         'PUBLIC'
                      WHEN customer_class_code = 'RESIDENTIAL_CONTRACTOR'
                      THEN
                         'RES'
                      WHEN customer_class_code = 'UNKNOWN'
                      THEN
                         'PUBLIC'
                      ELSE
                         NULL
                   END
              INTO pcustacctsiterec.customer_category_code
              FROM hz_cust_accounts
             WHERE cust_account_id = o_cust_account_id;
         EXCEPTION
            WHEN OTHERS
            THEN
               pcustacctsiterec.customer_category_code := NULL;
         END;

         hz_cust_account_site_v2pub.create_cust_acct_site (
            p_init_msg_list        => 'T',
            p_cust_acct_site_rec   => pcustacctsiterec,
            x_cust_acct_site_id    => o_cust_acct_site_id,
            x_return_status        => o_ret_status,
            x_msg_count            => o_msg_count,
            x_msg_data             => o_msg_data);

         IF o_ret_status <> 'S'
         THEN
            return_msg (o_msg_count, o_msg_data, o_return_msg);
            x_msg_data := 'Party Site Creation: ' || o_return_msg;
            debug (p_record_id, x_msg_data);
            x_ret_status := o_ret_status;
            ROLLBACK TO customer_record;
            RETURN;
         END IF;

         --====================================================================
         -- Create site use (Bill to) for Ship to
         --====================================================================

         l_sec := 'Create site use (Bill to) for Ship to';
         debug (p_record_id, l_sec);


         pcustacctsiteuserec := NULL;
         o_ret_status := NULL;
         o_msg_count := NULL;
         o_msg_data := NULL;
         o_cust_acct_site_use_id := NULL;

         pcustacctsiteuserec.site_use_code := 'BILL_TO';
         pcustacctsiteuserec.cust_acct_site_id := o_cust_acct_site_id;
         pcustacctsiteuserec.created_by_module := l_api_name;

         IF rec_cust.salesrep_id IS NOT NULL
         THEN
            BEGIN
               SELECT SALESREP_ID
                 INTO pcustacctsiteuserec.primary_salesrep_id
                 FROM jtf_rs_salesreps_mo_v
                WHERE resource_id = rec_cust.salesrep_id;
            EXCEPTION
               WHEN OTHERS
               THEN
                  pcustacctsiteuserec.primary_salesrep_id := NULL;
            END;
         END IF;

         pcustomerprofile.credit_checking := 'Y';

         BEGIN
            SELECT inv_remit_to_code
              INTO pcustomerprofile.attribute2
              FROM xxwc_ar_cm_remitcode_tbl
             WHERE area_code = SUBSTR (rec_cust.business_phone, 1, 3);
         EXCEPTION
            WHEN OTHERS
            THEN
               pcustomerprofile.attribute2 := 2;
         END;

         pcustomerprofile.attribute3 := 'Y';
         pcustomerprofile.collector_id := rec_cust.credit_manager;

         IF ln_credit_analyst IS NOT NULL
         THEN
            pcustomerprofile.credit_analyst_id := ln_credit_analyst;
         END IF;

         IF rec_cust.credit_decisioning IS NOT NULL
         THEN
            pcustomerprofile.credit_rating := rec_cust.credit_decisioning;
         ELSE
            pcustomerprofile.credit_rating := 'Y';
         END IF;

         IF rec_cust.account_status IS NOT NULL
         THEN
            BEGIN
               SELECT LOOKUP_CODE
                 INTO pcustomerprofile.account_status
                 FROM APPS.FND_LOOKUP_VALUES
                WHERE     LOOKUP_TYPE = 'ACCOUNT_STATUS'
                      AND LOOKUP_CODE = rec_cust.account_status;
            EXCEPTION
               WHEN OTHERS
               THEN
                  pcustomerprofile.account_status := NULL;
            END;
         ELSE
            pcustomerprofile.account_status := 'ACTIVE';
         END IF;

         pcustomerprofile.tolerance := 0;

         IF rec_cust.yard_job_accnt_project IS NOT NULL
         THEN
            IF rec_cust.yard_job_accnt_project IN ('Account', 'Yard', 'Job')
            THEN
               pcustacctsiteuserec.location :=
                  SUBSTR (rec_cust.business_name, 1, 30) || '/YARD';
               pcustacctsiteuserec.attribute1 := 'YARD';
            ELSIF rec_cust.yard_job_accnt_project IN ('Project')
            THEN
               pcustacctsiteuserec.location :=
                  SUBSTR (rec_cust.business_name, 1, 30) || '/JOB';
               pcustacctsiteuserec.attribute1 := 'JOB';
            ELSE
               pcustacctsiteuserec.location :=
                     SUBSTR (rec_cust.business_name, 1, 30)
                  || '/'
                  || UPPER (rec_cust.yard_job_accnt_project);
               pcustacctsiteuserec.attribute1 :=
                  rec_cust.yard_job_accnt_project;
            END IF;

            IF rec_cust.yard_job_accnt_project IN ('Job')
            THEN
               pcustomerprofile.credit_hold := 'Y';
            END IF;
         ELSE
            pcustacctsiteuserec.location :=
               SUBSTR (rec_cust.business_name, 1, 30);
         END IF;
		 
         IF rec_cust.credit_classification IS NOT NULL
         THEN
            SELECT LOOKUP_CODE
              INTO pcustomerprofile.credit_classification
              FROM ar_lookups
             WHERE     lookup_type LIKE 'AR_CMGT_CREDIT_CLASSIFICATION'
                   AND LOOKUP_CODE = rec_cust.credit_classification;
         ELSE
            pcustomerprofile.credit_classification := 'MODERATE';
         END IF;		 
		 

         BEGIN
            SELECT profile_class_id
              INTO pcustomerprofile.profile_class_id
              FROM HZ_CUST_PROFILE_CLASSES
             WHERE NAME = rec_cust.PROFILE_CLASS;
         EXCEPTION
            WHEN OTHERS
            THEN
               return_msg (o_msg_count, o_msg_data, o_return_msg);
               x_msg_data := 'Invalid Profile Class ' || o_return_msg;
               o_return_status := o_ret_status;
               ROLLBACK TO customer_record;
               RETURN;
         END;


         hz_cust_account_site_v2pub.create_cust_site_use (
            p_init_msg_list          => 'T',
            p_cust_site_use_rec      => pcustacctsiteuserec,
            p_customer_profile_rec   => pcustomerprofile,
            p_create_profile         => 'T',
            p_create_profile_amt     => 'T',
            x_site_use_id            => o_bill_to_site_use_id,
            x_return_status          => o_ret_status,
            x_msg_count              => o_msg_count,
            x_msg_data               => o_msg_data);


         IF o_ret_status <> 'S'
         THEN
            return_msg (o_msg_count, o_msg_data, o_return_msg);
            x_msg_data := 'Account Site Creation(BillTo1): ' || o_return_msg;
            debug (p_record_id, x_msg_data);
            x_ret_status := o_ret_status;
            ROLLBACK TO customer_record;
            RETURN;
         END IF;

         --====================================================================
         -- create Shipto site profile amount
         --====================================================================

         l_sec := 'create Shipto site profile amount';
         debug (p_record_id, l_sec);

         pcustproamtrec := NULL;
         o_sprofile_id := NULL;
         o_cust_acct_profile_amt_id := NULL;

         BEGIN
            SELECT cust_account_profile_id
              INTO o_sprofile_id
              FROM apps.hz_customer_profiles
             WHERE     cust_account_id = o_cust_account_id
                   AND site_use_id = o_bill_to_site_use_id;
         EXCEPTION
            WHEN NO_DATA_FOUND
            THEN
               o_sprofile_id := NULL;
         END;

         IF o_sprofile_id IS NOT NULL
         THEN
            BEGIN
               SELECT cust_acct_profile_amt_id, object_version_number
                 INTO o_cust_acct_profile_amt_id, p_object_version_number
                 FROM apps.hz_cust_profile_amts
                WHERE     cust_account_profile_id = o_sprofile_id
                      AND currency_code = lv_currency_code;
            EXCEPTION
               WHEN NO_DATA_FOUND
               THEN
                  o_cust_acct_profile_amt_id := NULL;
                  p_object_version_number := NULL;
            END;

            IF rec_cust.yard_job_accnt_project IN ('Project', 'Yard')
            THEN
               pcustproamtrec.overall_credit_limit := rec_cust.credit_limit;
            ELSIF rec_cust.yard_job_accnt_project IN ('Account')
            THEN
               pcustproamtrec.overall_credit_limit := 0;
            ELSE
               pcustproamtrec.overall_credit_limit := 0;
            END IF;



            IF o_cust_acct_profile_amt_id IS NOT NULL
            THEN
               --Update amount profile
               pcustproamtrec.cust_acct_profile_amt_id :=
                  o_cust_acct_profile_amt_id;

               pcustproamtrec.created_by_module := l_api_name;
               hz_customer_profile_v2pub.update_cust_profile_amt (
                  p_init_msg_list           => 'T',
                  p_cust_profile_amt_rec    => pcustproamtrec,
                  p_object_version_number   => p_object_version_number,
                  x_return_status           => o_ret_status,
                  x_msg_count               => o_msg_count,
                  x_msg_data                => o_msg_data);
            ELSE
               --Create Profile Amount
               pcustproamtrec.cust_account_profile_id := o_sprofile_id;
               pcustproamtrec.cust_account_id := o_cust_account_id;
               pcustproamtrec.site_use_id := o_cust_acct_site_use_id;
               pcustproamtrec.currency_code := lv_currency_code;
               pcustproamtrec.created_by_module := l_api_name;
               hz_customer_profile_v2pub.create_cust_profile_amt (
                  p_init_msg_list              => 'T',
                  p_check_foreign_key          => 'T',
                  p_cust_profile_amt_rec       => pcustproamtrec,
                  x_cust_acct_profile_amt_id   => o_cust_acct_profile_amt_id,
                  x_return_status              => o_ret_status,
                  x_msg_count                  => o_msg_count,
                  x_msg_data                   => o_msg_data);
            END IF;

            IF o_ret_status <> 'S'
            THEN
               return_msg (o_msg_count, o_msg_data, o_return_msg);
               x_msg_data :=
                  'Shipto Profile Amount Creation: ' || o_return_msg;
               debug (p_record_id, x_msg_data);
               x_ret_status := o_ret_status;
               ROLLBACK TO customer_record;
               RETURN;
            END IF;
         END IF;


         --====================================================================
         -- Create site use for Ship to
         --====================================================================
         l_sec := 'Create site use for Ship to';
         debug (p_record_id, l_sec);

         pcustacctsiteuserec := NULL;
         o_ret_status := NULL;
         o_msg_count := NULL;
         o_msg_data := NULL;
         o_cust_acct_site_use_id := NULL;

         pcustacctsiteuserec.site_use_code := 'SHIP_TO';
         pcustacctsiteuserec.cust_acct_site_id := o_cust_acct_site_id;

         BEGIN
            SELECT inv_remit_to_code
              INTO pcustomerprofile.attribute2
              FROM xxwc_ar_cm_remitcode_tbl
             WHERE area_code = SUBSTR (rec_cust.business_phone, 1, 3);
         EXCEPTION
            WHEN OTHERS
            THEN
               pcustomerprofile.attribute2 := 2;
         END;

         pcustomerprofile.attribute3 := 'Y';
         pcustacctsiteuserec.bill_to_site_use_id := o_bill_to_site_use_id;

         pcustacctsiteuserec.primary_flag := 'Y';
         pcustacctsiteuserec.created_by_module := l_api_name;

         IF rec_cust.salesrep_id IS NOT NULL
         THEN
            BEGIN
               SELECT SALESREP_ID
                 INTO pcustacctsiteuserec.primary_salesrep_id
                 FROM jtf_rs_salesreps_mo_v
                WHERE resource_id = rec_cust.salesrep_id;
            EXCEPTION
               WHEN OTHERS
               THEN
                  pcustacctsiteuserec.primary_salesrep_id := NULL;
            END;
         END IF;

         pcustomerprofile.credit_checking := 'Y';
         pcustomerprofile.collector_id := rec_cust.credit_manager;

         IF ln_credit_analyst IS NOT NULL
         THEN
            pcustomerprofile.credit_analyst_id := ln_credit_analyst;
         END IF;


         IF rec_cust.account_status IS NOT NULL
         THEN
            BEGIN
               SELECT LOOKUP_CODE
                 INTO pcustomerprofile.account_status
                 FROM APPS.FND_LOOKUP_VALUES
                WHERE     LOOKUP_TYPE = 'ACCOUNT_STATUS'
                      AND LOOKUP_CODE = rec_cust.account_status;
            EXCEPTION
               WHEN OTHERS
               THEN
                  pcustomerprofile.account_status := NULL;
            END;
         ELSE
            pcustomerprofile.account_status := 'ACTIVE';
         END IF;

         pcustomerprofile.tolerance := 0;


         IF rec_cust.yard_job_accnt_project IS NOT NULL
         THEN
            IF rec_cust.yard_job_accnt_project IN ('Account', 'Yard', 'Job')
            THEN
               pcustacctsiteuserec.location :=
                  SUBSTR (rec_cust.business_name, 1, 30) || '/YARD';
               pcustacctsiteuserec.attribute1 := 'YARD';
            ELSIF rec_cust.yard_job_accnt_project IN ('Project')
            THEN
               pcustacctsiteuserec.location :=
                  SUBSTR (rec_cust.business_name, 1, 30) || '/JOB';
               pcustacctsiteuserec.attribute1 := 'JOB';
            ELSE
               pcustacctsiteuserec.location :=
                     SUBSTR (rec_cust.business_name, 1, 30)
                  || '/'
                  || rec_cust.yard_job_accnt_project;
               pcustacctsiteuserec.attribute1 :=
                  rec_cust.yard_job_accnt_project;
            END IF;

            IF rec_cust.yard_job_accnt_project IN ('Job')
            THEN
               pcustomerprofile.credit_hold := 'Y';
            END IF;
         ELSE
            pcustacctsiteuserec.location :=
               SUBSTR (rec_cust.business_name, 1, 30);
         END IF;

         IF rec_cust.credit_classification IS NOT NULL
         THEN
            SELECT LOOKUP_CODE
              INTO pcustomerprofile.credit_classification
              FROM ar_lookups
             WHERE     lookup_type LIKE 'AR_CMGT_CREDIT_CLASSIFICATION'
                   AND LOOKUP_CODE = rec_cust.credit_classification;
         ELSE
            pcustomerprofile.credit_classification := 'MODERATE';
         END IF;		 

         IF rec_cust.profile_class IS NOT NULL
         THEN
            BEGIN
               SELECT profile_class_id
                 INTO pcustomerprofile.profile_class_id
                 FROM HZ_CUST_PROFILE_CLASSES
                WHERE NAME = rec_cust.PROFILE_CLASS;
            EXCEPTION
               WHEN OTHERS
               THEN
                  pcustomerprofile.profile_class_id := NULL;
            END;
         ELSE
            BEGIN
               SELECT profile_class_id
                 INTO pcustomerprofile.profile_class_id
                 FROM HZ_CUST_PROFILE_CLASSES
                WHERE NAME = 'Contractor - Non Key';
            EXCEPTION
               WHEN OTHERS
               THEN
                  pcustomerprofile.profile_class_id := NULL;
            END;
         END IF;

         hz_cust_account_site_v2pub.create_cust_site_use (
            p_init_msg_list          => 'T',
            p_cust_site_use_rec      => pcustacctsiteuserec,
            p_customer_profile_rec   => pcustomerprofile,
            p_create_profile         => 'T',
            p_create_profile_amt     => 'T',
            x_site_use_id            => o_cust_acct_site_use_id,
            x_return_status          => o_ret_status,
            x_msg_count              => o_msg_count,
            x_msg_data               => o_msg_data);

         IF o_ret_status <> 'S'
         THEN
            return_msg (o_msg_count, o_msg_data, o_return_msg);
            x_msg_data := 'Account Site Creation(SHIPTO): ' || o_return_msg;
            debug (p_record_id, x_msg_data);
            x_ret_status := o_ret_status;
            ROLLBACK TO customer_record;
            RETURN;
         END IF;

         --====================================================================
         -- create Shipto site profile amount
         --====================================================================
         l_sec := 'create Shipto site profile amount';
         debug (p_record_id, l_sec);

         pcustproamtrec := NULL;
         o_sprofile_id := NULL;
         o_cust_acct_profile_amt_id := NULL;

         BEGIN
            SELECT cust_account_profile_id
              INTO o_sprofile_id
              FROM apps.hz_customer_profiles
             WHERE     cust_account_id = o_cust_account_id
                   AND site_use_id = o_cust_acct_site_use_id;
         EXCEPTION
            WHEN NO_DATA_FOUND
            THEN
               o_sprofile_id := NULL;
         END;

         IF o_sprofile_id IS NOT NULL
         THEN
            BEGIN
               SELECT cust_acct_profile_amt_id, object_version_number
                 INTO o_cust_acct_profile_amt_id, p_object_version_number
                 FROM apps.hz_cust_profile_amts
                WHERE     cust_account_profile_id = o_sprofile_id
                      AND currency_code = lv_currency_code;
            EXCEPTION
               WHEN NO_DATA_FOUND
               THEN
                  o_cust_acct_profile_amt_id := NULL;
                  p_object_version_number := NULL;
            END;

            IF rec_cust.yard_job_accnt_project IN ('Project', 'Yard')
            THEN
               pcustproamtrec.overall_credit_limit := rec_cust.credit_limit;
            ELSIF rec_cust.yard_job_accnt_project IN ('Account')
            THEN
               pcustproamtrec.overall_credit_limit := 0;
            ELSE
               pcustproamtrec.overall_credit_limit := 0;
            END IF;



            IF o_cust_acct_profile_amt_id IS NOT NULL
            THEN
               --Update amount profile
               pcustproamtrec.cust_acct_profile_amt_id :=
                  o_cust_acct_profile_amt_id;
               pcustproamtrec.created_by_module := l_api_name;
               hz_customer_profile_v2pub.update_cust_profile_amt (
                  p_init_msg_list           => 'T',
                  p_cust_profile_amt_rec    => pcustproamtrec,
                  p_object_version_number   => p_object_version_number,
                  x_return_status           => o_ret_status,
                  x_msg_count               => o_msg_count,
                  x_msg_data                => o_msg_data);
            ELSE
               --Create Profile Amount
               pcustproamtrec.cust_account_profile_id := o_sprofile_id;
               pcustproamtrec.cust_account_id := o_cust_account_id;
               pcustproamtrec.site_use_id := o_cust_acct_site_use_id;
               pcustproamtrec.currency_code := lv_currency_code;
               pcustproamtrec.created_by_module := l_api_name;
               hz_customer_profile_v2pub.create_cust_profile_amt (
                  p_init_msg_list              => 'T',
                  p_check_foreign_key          => 'T',
                  p_cust_profile_amt_rec       => pcustproamtrec,
                  x_cust_acct_profile_amt_id   => o_cust_acct_profile_amt_id,
                  x_return_status              => o_ret_status,
                  x_msg_count                  => o_msg_count,
                  x_msg_data                   => o_msg_data);
            END IF;

            IF o_ret_status <> 'S'
            THEN
               return_msg (o_msg_count, o_msg_data, o_return_msg);
               x_msg_data :=
                  'Shipto Profile Amount Creation: ' || o_return_msg;
               debug (p_record_id, x_msg_data);
               x_ret_status := o_ret_status;
               ROLLBACK TO customer_record;
               RETURN;
            END IF;
         END IF;

         --====================================================================
         -- Create Shipto usage rules
         --====================================================================

         l_sec := 'Create Shipto usage rules';
         debug (p_record_id, l_sec);

         o_msg_count := NULL;
         o_msg_data := NULL;
         o_ret_status := NULL;

         hz_credit_usages_cascade_pkg.cascade_credit_usage_rules (
            p_cust_acct_profile_amt_id   => o_cust_acct_profile_amt_id,
            p_cust_profile_id            => o_sprofile_id,
            p_profile_class_amt_id       => NULL,
            p_profile_class_id           => NULL,
            x_return_status              => o_ret_status,
            x_msg_count                  => o_msg_count,
            x_msg_data                   => o_msg_data);

         IF o_ret_status <> 'S'
         THEN
            return_msg (o_msg_count, o_msg_data, o_return_msg);
            x_msg_data :=
               'Shipto cascade_credit_usage_rules: ' || o_return_msg;
            debug (p_record_id, x_msg_data);
            x_ret_status := o_ret_status;
            ROLLBACK TO customer_record;
            RETURN;
         END IF;

         --====================================================================
         -- Inactivating Exising role
         --====================================================================

         l_sec := 'Inactivating Customer account level role ';
         debug (p_record_id, l_sec);

         BEGIN
            SELECT cust_account_role_id, object_version_number
              INTO ln_acct_role_id, obj_version
              FROM hz_cust_account_roles
             WHERE     CUST_ACCOUNT_ID = o_cust_account_id
                   AND CURRENT_ROLE_STATE = 'A'
                   AND ROWNUM = 1;
         EXCEPTION
            WHEN OTHERS
            THEN
               ln_acct_role_id := 0;
               obj_version := NULL;
         END;

         debug (p_record_id, ' cust_account_role_id ' || ln_acct_role_id);

         IF NVL (ln_acct_role_id, 0) > 0
         THEN
            P_cust_role_rec.cust_account_role_id := ln_acct_role_id;
            P_cust_role_rec.status := 'I';

            hz_cust_account_role_v2pub.update_cust_account_role (
               p_init_msg_list           => FND_API.G_FALSE,
               p_cust_account_role_rec   => p_cust_role_rec,
               p_object_version_number   => obj_version,
               x_return_status           => o_ret_status,
               x_msg_count               => o_msg_count,
               x_msg_data                => o_msg_data);

            IF o_ret_status <> 'S'
            THEN
               return_msg (o_msg_count, o_msg_data, o_return_msg);
               x_msg_data :=
                  'Inactivating Customer Account Role: ' || o_return_msg;
               debug (p_record_id, x_msg_data);
               x_ret_status := o_ret_status;
               ROLLBACK TO customer_record;
               RETURN;
            END IF;
         END IF;

         --====================================================================
         -- Creating contact person
         --====================================================================
         l_sec := 'Creating Contact Person ';
         debug (p_record_id, l_sec);

         p_create_person_rec := NULL;
         x_contact_party_id := NULL;
         x_contact_party_number := NULL;
         x_contact_profile_id := NULL;
         p_create_person_rec.person_first_name :=
            rec_cust.ap_contact_first_name;
         p_create_person_rec.person_last_name := rec_cust.ap_contact_last_name;
         p_create_person_rec.created_by_module := l_api_name;
         hz_party_v2pub.create_person ('T',
                                       p_create_person_rec,
                                       x_contact_party_id,
                                       x_contact_party_number,
                                       x_contact_profile_id,
                                       o_ret_status,
                                       o_msg_count,
                                       o_msg_data);

         IF o_ret_status <> 'S'
         THEN
            return_msg (o_msg_count, o_msg_data, o_return_msg);
            x_msg_data := 'Contact Person creation: ' || o_return_msg;
            debug (p_record_id, x_msg_data);
            x_ret_status := o_ret_status;
            ROLLBACK TO customer_record;
            RETURN;
         END IF;

         --====================================================================
         -- Creating Org contact
         --====================================================================

         l_sec := 'Creating Org contact';
         debug (p_record_id, l_sec);

         p_org_contact_rec := NULL;
         p_org_contact_rec.created_by_module := l_api_name;
         p_org_contact_rec.party_rel_rec.subject_id := x_contact_party_id;
         p_org_contact_rec.party_rel_rec.subject_type := 'PERSON';
         p_org_contact_rec.party_rel_rec.subject_table_name := 'HZ_PARTIES';
         p_org_contact_rec.party_rel_rec.object_id := o_party_id;
         p_org_contact_rec.party_rel_rec.object_type := 'ORGANIZATION';
         p_org_contact_rec.party_rel_rec.object_table_name := 'HZ_PARTIES';
         p_org_contact_rec.party_rel_rec.relationship_code := 'CONTACT_OF';
         p_org_contact_rec.party_rel_rec.relationship_type := 'CONTACT';
         p_org_contact_rec.party_rel_rec.start_date := SYSDATE;
         hz_party_contact_v2pub.create_org_contact ('T',
                                                    p_org_contact_rec,
                                                    x_org_contact_id,
                                                    x_party_rel_id,
                                                    x_rel_party_id,
                                                    x_rel_party_number,
                                                    o_ret_status,
                                                    o_msg_count,
                                                    o_msg_data);


         IF o_ret_status <> 'S'
         THEN
            return_msg (o_msg_count, o_msg_data, o_return_msg);
            x_msg_data := 'Creating Org Contact: ' || o_return_msg;
            debug (p_record_id, x_msg_data);
            x_ret_status := o_ret_status;
            ROLLBACK TO customer_record;
            RETURN;
         END IF;

         --====================================================================
         -- Creating Phone contact point by using party_id
         --====================================================================

         l_sec := 'Creating Phone contact point by using party_id ';
         debug (p_record_id, l_sec);

         pcontactpointrec.contact_point_type := 'PHONE';
         pcontactpointrec.owner_table_name := 'HZ_PARTIES';
         pcontactpointrec.owner_table_id := x_rel_party_id;
         pcontactpointrec.primary_flag := 'Y';
         pcontactpointrec.contact_point_purpose := 'BUSINESS';
         pcontactpointrec.created_by_module := l_api_name;
         pphonerec.phone_area_code := NULL;
         pphonerec.phone_country_code := '1';
         pphonerec.phone_number := rec_cust.ap_phone;
         pphonerec.phone_line_type := 'GEN';


         HZ_CONTACT_POINT_V2PUB.CREATE_CONTACT_POINT (
            p_init_msg_list       => FND_API.G_TRUE,
            p_contact_point_rec   => pcontactpointrec,
            p_edi_rec             => pedirec,
            p_email_rec           => pemailrec,
            p_phone_rec           => pphonerec,
            p_telex_rec           => ptelexrec,
            p_web_rec             => pwebrec,
            x_contact_point_id    => o_contact_point_id,
            x_return_status       => o_ret_status,
            x_msg_count           => o_msg_count,
            x_msg_data            => o_msg_data);

         IF o_ret_status <> 'S'
         THEN
            return_msg (o_msg_count, o_msg_data, o_return_msg);
            x_msg_data :=
               'Creating Phone Contact (for Party): ' || o_return_msg;
            debug (p_record_id, x_msg_data);
            x_ret_status := o_ret_status;
            ROLLBACK TO customer_record;
            RETURN;
         END IF;

         ------------------------------------------------------------------------------------------------------------
         -- Create a Email Contact Point using party_id
         ------------------------------------------------------------------------------------------------------------
         l_sec := 'Create a Email Contact Point using party_id ';
         debug (p_record_id, l_sec);

         -- Initializing the Mandatory API parameters
         pcontactpointrec.contact_point_type := 'EMAIL';
         pcontactpointrec.owner_table_name := 'HZ_PARTIES';
         pcontactpointrec.owner_table_id := x_rel_party_id;
         pcontactpointrec.primary_flag := 'Y';
         pcontactpointrec.contact_point_purpose := 'BUSINESS';
         pcontactpointrec.created_by_module := l_api_name;
         pemailrec.email_format := 'MAILHTML';
         pemailrec.email_address := rec_cust.ap_email;

         HZ_CONTACT_POINT_V2PUB.CREATE_CONTACT_POINT (
            p_init_msg_list       => FND_API.G_TRUE,
            p_contact_point_rec   => pcontactpointrec,
            p_edi_rec             => pedirec,
            p_email_rec           => pemailrec,
            p_phone_rec           => pphonerec,
            p_telex_rec           => ptelexrec,
            p_web_rec             => pwebrec,
            x_contact_point_id    => o_contact_point_id,
            x_return_status       => o_ret_status,
            x_msg_count           => o_msg_count,
            x_msg_data            => o_msg_data);

         IF o_ret_status <> 'S'
         THEN
            return_msg (o_msg_count, o_msg_data, o_return_msg);
            x_msg_data :=
               'Creating Email Contact (for Party): ' || o_return_msg;
            debug (p_record_id, x_msg_data);
            x_ret_status := o_ret_status;
            ROLLBACK TO customer_record;
            RETURN;
         END IF;

         ------------------------------------------------------------------------------------------------------------
         -- Creating Party site
         ------------------------------------------------------------------------------------------------------------

         l_sec := ' Creating Party site ';
         debug (p_record_id, l_sec);

         o_ret_status := NULL;
         o_msg_count := NULL;
         o_msg_data := NULL;
         o_party_site_id := NULL;
         o_party_site_no := NULL;
         o_sprofile_id := NULL;
         ppartysiterec.created_by_module := l_api_name;

         -- create a party site now
         ppartysiterec.party_id := x_rel_party_id;
         ppartysiterec.location_id := ln_bill_location_id;
         ppartysiterec.identifying_address_flag := 'Y';

         hz_party_site_v2pub.create_party_site (
            p_init_msg_list       => 'T',
            p_party_site_rec      => ppartysiterec,
            x_party_site_id       => o_party_site_id,
            x_party_site_number   => o_party_site_no,
            x_return_status       => o_ret_status,
            x_msg_count           => o_msg_count,
            x_msg_data            => o_msg_data);


         IF o_ret_status <> 'S'
         THEN
            return_msg (o_msg_count, o_msg_data, o_return_msg);
            x_msg_data := 'Creating contact location: ' || o_return_msg;
            debug (p_record_id, x_msg_data);
            x_ret_status := o_ret_status;
            ROLLBACK TO customer_record;
            RETURN;
         END IF;

         --====================================================================
         -- creating account role
         --====================================================================
         l_sec := ' creating account role ';
         debug (p_record_id, l_sec);

         o_ret_status := NULL;
         o_msg_count := NULL;
         o_msg_data := NULL;
         p_cr_cust_acc_role_rec := NULL;
         p_cr_cust_acc_role_rec.party_id := x_rel_party_id;
         p_cr_cust_acc_role_rec.cust_account_id := o_cust_account_id;
         p_cr_cust_acc_role_rec.primary_flag := 'N';
         p_cr_cust_acc_role_rec.role_type := 'CONTACT';
         p_cr_cust_acc_role_rec.created_by_module := l_api_name;
         hz_cust_account_role_v2pub.create_cust_account_role (
            'T',
            p_cr_cust_acc_role_rec,
            x_cust_account_role_id,
            o_ret_status,
            o_msg_count,
            o_msg_data);

         IF o_ret_status <> 'S'
         THEN
            return_msg (o_msg_count, o_msg_data, o_return_msg);
            x_msg_data := 'Creating Account role: ' || o_return_msg;
            debug (p_record_id, x_msg_data);
            x_ret_status := o_ret_status;
            ROLLBACK TO customer_record;
            RETURN;
         END IF;

         --====================================================================
         -- Assign Accounts Payables role
         --====================================================================
         l_sec := ' Assign Accounts Payables role ';
         debug (p_record_id, l_sec);

         o_ret_status := NULL;
         o_msg_count := NULL;
         o_msg_data := NULL;
         p_ROLE_RESPONSIBILITY_REC_TYPE := NULL;
         p_ROLE_RESPONSIBILITY_REC_TYPE.cust_account_role_id :=
            x_cust_account_role_id;
         p_ROLE_RESPONSIBILITY_REC_TYPE.responsibility_type := 'ACC_PAY';

         p_ROLE_RESPONSIBILITY_REC_TYPE.created_by_module := l_api_name;

         hz_cust_account_role_v2pub.create_role_responsibility (
            p_init_msg_list             => 'T',
            p_role_responsibility_rec   => p_ROLE_RESPONSIBILITY_REC_TYPE,
            x_responsibility_id         => o_resp_id,
            x_return_status             => o_ret_status,
            x_msg_count                 => o_msg_count,
            x_msg_data                  => o_msg_data);

         IF o_ret_status <> 'S'
         THEN
            return_msg (o_msg_count, o_msg_data, o_return_msg);
            x_msg_data := 'Assing AP role: ' || o_return_msg;
            debug (p_record_id, x_msg_data);
            x_ret_status := o_ret_status;
            ROLLBACK TO customer_record;
            RETURN;
         END IF;



         --====================================================================
         -- Adding records into XXWC.XXWC_B2B_CONFIG_TBL
         --====================================================================

         l_sec := 'Updating B2B Config table.';
         debug (p_record_id, l_sec);

         BEGIN
            BEGIN
               SELECT COUNT (1)
                 INTO ln_count
                 FROM XXWC.XXWC_B2B_CONFIG_TBL
                WHERE     party_id = ln_party_id
                      AND CUSTOMER_ID = o_cust_account_id;
            EXCEPTION
               WHEN OTHERS
               THEN
                  ln_count := 0;
                  debug (p_record_id, 'Error ' || SQLERRM);
            END;

            IF ln_count = 0
            THEN
               l_sec := 'Inserting data into XXWC_B2B_CONFIG_TBL Table';
               debug (p_record_id, l_sec);

               BEGIN
                  INSERT
                    INTO XXWC.XXWC_B2B_CONFIG_TBL (PARTY_ID,
                                                   PARTY_NAME,
                                                   PARTY_NUMBER,
                                                   CUSTOMER_ID,
                                                   DELIVER_SOA,
                                                   DELIVER_ASN,
                                                   DELIVER_INVOICE,
                                                   START_DATE_ACTIVE,
                                                   END_DATE_ACTIVE,
                                                   CREATION_DATE,
                                                   CREATED_BY,
                                                   LAST_UPDATE_DATE,
                                                   LAST_UPDATED_BY,
                                                   TP_NAME,
                                                   DELIVER_POA,
                                                   DEFAULT_EMAIL,
                                                   STATUS,
                                                   NOTIFICATION_EMAIL,
                                                   NOTIFY_ACCOUNT_MGR,
                                                   SOA_EMAIL,
                                                   ASN_EMAIL,
                                                   COMMENTS,
                                                   APPROVED_DATE,
                                                   DELIVER_POD,
                                                   POD_EMAIL,
                                                   POD_FREQUENCY,
                                                   ACCOUNT_NUMBER,
                                                   ACCOUNT_NAME,
                                                   CUST_ACCOUNT_ID,
                                                   POD_LAST_SENT_DATE,
                                                   POD_NEXT_SEND_DATE,
                                                   ID,
                                                   LOCATION,
                                                   SITE_USE_ID,
                                                   SOA_PRINT_PRICE,
                                                   POD_PRINT_PRICE,
                                                   BUSINESS_EVENT_ELIGIBLE)
                     VALUES (
                               ln_party_id,
                               lvc_account_name,
                               lvc_party_number,
                               o_cust_account_id,
                               'Y',
                               'N',
                               'Y',
                               SYSDATE,
                               TO_DATE ('12/31/4099 00:00:00',
                                        'MM/DD/YYYY HH24:MI:SS'),
                               SYSDATE,
                               FND_GLOBAL.USER_ID,
                               SYSDATE,
                               -1,
                               NULL,
                               'N',
                               NULL,
                               'APPROVED',
                               NULL,
                               'Y',
                               lvc_soa_email_address,
                               lvc_invoice_email_address,
                               NULL,
                               NULL,
                               'Y',
                               lvc_pod_email_address,
                               'WEEKLY',
                               ln_account_number,
                               lvc_account_name,
                               o_cust_account_id,
                               NULL,
                               NULL,
                               NULL,
                               lvc_billing_city,
                               ln_ship_site_use_id,
                               'N',
                               NULL,
                               NULL);
               EXCEPTION
                  WHEN OTHERS
                  THEN
                     debug (
                        p_record_id,
                           ' Error occured while inserting data into B2B table '
                        || SUBSTR (SQLERRM, 1, 250));
               END;
            ELSE
               l_sec := 'Updating data into XXWC_B2B_CONFIG_TBL Table';

               BEGIN
                  UPDATE XXWC.XXWC_B2B_CONFIG_TBL
                     SET PARTY_ID = ln_party_id,
                         PARTY_NAME = lvc_account_name,
                         PARTY_NUMBER = lvc_party_number,
                         CUSTOMER_ID = o_cust_account_id,
                         DELIVER_SOA = 'Y',
                         DELIVER_ASN = 'N',
                         DELIVER_INVOICE = 'Y',
                         START_DATE_ACTIVE = SYSDATE,
                         END_DATE_ACTIVE =
                            TO_DATE ('12/31/4099 00:00:00',
                                     'MM/DD/YYYY HH24:MI:SS'),
                         CREATION_DATE = SYSDATE,
                         CREATED_BY = FND_GLOBAL.USER_ID,
                         LAST_UPDATE_DATE = SYSDATE,
                         LAST_UPDATED_BY = -1,
                         TP_NAME = NULL,
                         DELIVER_POA = 'N',
                         DEFAULT_EMAIL = NULL,
                         STATUS = 'APPROVED',
                         NOTIFICATION_EMAIL = NULL,
                         NOTIFY_ACCOUNT_MGR = 'Y',
                         SOA_EMAIL = lvc_soa_email_address,
                         ASN_EMAIL = lvc_invoice_email_address,
                         COMMENTS = NULL,
                         APPROVED_DATE = NULL,
                         DELIVER_POD = 'Y',
                         POD_EMAIL = lvc_pod_email_address,
                         POD_FREQUENCY = 'WEEKLY',
                         ACCOUNT_NUMBER = ln_account_number,
                         ACCOUNT_NAME = lvc_account_name,
                         CUST_ACCOUNT_ID = o_cust_account_id,
                         POD_LAST_SENT_DATE = NULL,
                         POD_NEXT_SEND_DATE = NULL,
                         ID = NULL,
                         LOCATION = lvc_billing_city,
                         SITE_USE_ID = ln_ship_site_use_id,
                         SOA_PRINT_PRICE = 'N',
                         POD_PRINT_PRICE = NULL,
                         BUSINESS_EVENT_ELIGIBLE = NULL
                   WHERE     1 = 1
                         AND party_id = ln_party_id
                         AND CUSTOMER_ID = o_cust_account_id;
               EXCEPTION
                  WHEN OTHERS
                  THEN
                     debug (
                        p_record_id,
                           ' Error occured while Updating data into B2B table '
                        || SUBSTR (SQLERRM, 1, 250));
               END;
            END IF;
         END;


         l_sec :=
               'Row inserted into  XXWC.XXWC_B2B_CONFIG_TBL table  '
            || SQL%ROWCOUNT;

         debug (p_record_id, l_sec);
      END LOOP;


         UPDATE xxwc.XXWC_AR_EQUIFAX_ACCT_WS_TBL
            SET process_flag = 'Y', CUSTOMER_NUMBER = ln_account_number
          WHERE record_id = p_record_id;
	  
      COMMIT;

     <<LASTSTEP>>
      x_cust_account_id := o_cust_account_id;
      x_acct_number := ln_account_number;
      x_party_id := ln_party_id;
      x_party_number := lvc_party_number;
      x_profile_id := ol_profile_id;
      x_ret_status := NVL (o_ret_status, 'Success');
      x_msg_count := NVL (o_msg_count, 0);
      x_msg_data := NVL (o_msg_data, NULL);
   EXCEPTION
      WHEN cust_exp
      THEN
         NULL;
      WHEN OTHERS
      THEN
         x_cust_account_id := 0;
         x_acct_number := 0;
         x_party_id := 0;
         x_party_number := 0;
         x_profile_id := 0;
         x_ret_status := 'Error';
         x_msg_count := 1;
         x_msg_data :=
               'Techinical Error Occurred, please contact HD White Cap IT Team '
            || SQLERRM;

         xxcus_error_pkg.xxcus_error_main_api (
            p_called_from         => g_err_callfrom || '.' || lvc_procedure,
            p_calling             => l_sec,
            p_ora_error_msg       => SQLERRM,
            p_error_desc          =>    'Error Occured for '
                                     || p_record_id
                                     || 'record id in XXWC_AR_EQUIFAX_ACCT_WS_TBL.',
            p_distribution_list   => g_distro_list,
            p_module              => g_module);
   END;


   -- =====================================================================================================================================================
   -- Procedure: inactivate_acct_sites
   -- Purpose: inactivate_acct_sites procedure
   -- ====================================================================================================================================================
   --  REVISIONS:
   --   Ver        Date        Author               Description
   --  ---------  ---------- ---------------  ------------------------------------------------------------------------------------------------
   --  1.0        12/19/2017  P.Vamshidhar     TMS#20131016-00419 -Credit - Implement online credit application - Customer Accounts/Update File
   -- ====================================================================================================================================================
   PROCEDURE inactivate_acct_sites (p_record_id    IN     NUMBER,
                                    x_ret_status      OUT VARCHAR2,
                                    x_msg_count       OUT NUMBER,
                                    x_msg_data        OUT VARCHAR2)
   IS
      CURSOR CUR_SITES
      IS
         SELECT hcas.cust_acct_site_id, hcas.object_version_number
           FROM hz_cust_acct_sites_all hcas,
                xxwc.XXWC_AR_EQUIFAX_ACCT_WS_TBL xanc,
                apps.hz_cust_accounts hca
          WHERE     hca.account_number = xanc.customer_number
                AND hca.cust_account_id = hcas.cust_account_id
                AND xanc.record_id = p_record_id
                AND hcas.status = 'A';

      l_init_msg_list           VARCHAR2 (1000) := FND_API.G_TRUE;
      l_cust_acct_site_rec      hz_cust_account_site_v2pub.cust_acct_site_rec_type;
      l_object_version_number   NUMBER;
      l_sec                     VARCHAR2 (32767);
      lvc_procedure             VARCHAR2 (100) := 'INACTIVATE_ACCT_SITES';
      l_proc_status             VARCHAR2 (1);
   BEGIN
      l_sec := 'Inactivating Account Sites Starts';

      FOR REC_SITES IN CUR_SITES
      LOOP
         l_sec :=
            'Inactivating Account Site Id ' || rec_sites.cust_acct_site_id;
         l_cust_acct_site_rec := NULL;
         l_object_version_number := NULL;
         l_cust_acct_site_rec.cust_acct_site_id := rec_sites.cust_acct_site_id;
         l_object_version_number := rec_sites.object_version_number;
         l_cust_acct_site_rec.status := 'I';

         l_sec :=
               'Inactivating Account Site Id '
            || rec_sites.cust_acct_site_id
            || ' API ';
         hz_cust_account_site_v2pub.update_cust_acct_site (
            'T',
            l_cust_acct_site_rec,
            l_object_version_number,
            x_ret_status,
            x_msg_count,
            x_msg_data);

         IF x_ret_status <> 'S'
         THEN
            l_sec :=
                  'Inactivation process for Account Site Id '
               || rec_sites.cust_acct_site_id
               || 'Errored';
            EXIT;
         END IF;

         l_sec :=
            'Inactivated Account Site Id ' || rec_sites.cust_acct_site_id;
      END LOOP;

      l_sec := 'Inactivating Account Sites Completed';
   EXCEPTION
      WHEN OTHERS
      THEN
         xxcus_error_pkg.xxcus_error_main_api (
            p_called_from         => g_err_callfrom || '.' || lvc_procedure,
            p_calling             => l_sec,
            p_ora_error_msg       => SQLERRM,
            p_error_desc          =>    'Error Occured for '
                                     || p_record_id
                                     || 'record id in Inactivating Account Sites.',
            p_distribution_list   => g_distro_list,
            p_module              => g_module);
   END;

   -- =====================================================================================================================================================
   -- Procedure: inactivate_party_sites
   -- Purpose: inactivate_party_sites procedure
   -- ====================================================================================================================================================
   --  REVISIONS:
   --   Ver        Date        Author               Description
   --  ---------  ---------- ---------------  ------------------------------------------------------------------------------------------------
   --  1.0        12/19/2017  P.Vamshidhar     TMS#20131016-00419 -Credit - Implement online credit application - Customer Accounts/Update File
   -- ====================================================================================================================================================
   PROCEDURE inactivate_party_sites (p_record_id    IN     NUMBER,
                                     x_ret_status      OUT VARCHAR2,
                                     x_msg_count       OUT NUMBER,
                                     x_msg_data        OUT VARCHAR2)
   IS
      CURSOR CUR_SITES
      IS
         SELECT hps.party_site_id, hps.object_version_number
           FROM hz_party_sites hps,
                hz_cust_accounts hca,
                xxwc.XXWC_AR_EQUIFAX_ACCT_WS_TBL xanc
          WHERE     1 = 1
                AND hps.party_id = hca.party_id
                AND hca.account_number = xanc.customer_number
                AND xanc.record_id = p_record_id
                AND hps.status = 'A';

      l_init_msg_list    VARCHAR2 (1000) := FND_API.G_TRUE;
      l_party_site_rec   hz_party_site_v2pub.party_site_rec_type;
      l_obj_num          NUMBER;
      l_sec              VARCHAR2 (32767);
      lvc_procedure      VARCHAR2 (100) := 'INACTIVATE_PARTY_SITES';
   BEGIN
      l_sec := 'Inactivating Party Sites';

      FOR REC_SITES IN CUR_SITES
      LOOP
         l_sec := 'Inactivating Party Site Id ' || rec_sites.party_site_id;
         l_party_site_rec := NULL;
         l_obj_num := NULL;
         l_party_site_rec.party_site_id := rec_sites.party_site_id;
         l_obj_num := rec_sites.object_version_number;
         l_party_site_rec.status := 'I';

         l_sec :=
               'Inactivating Party Site Id '
            || rec_sites.party_site_id
            || ' API ';
         l_party_site_rec.party_site_id := rec_sites.party_site_id;
         l_party_site_rec.status := 'I';
         l_obj_num := rec_sites.object_version_number;

         hz_party_site_v2pub.update_party_site (
            p_init_msg_list           => FND_API.G_FALSE,
            p_party_site_rec          => l_party_site_rec,
            p_object_version_number   => l_obj_num,
            x_return_status           => x_ret_status,
            x_msg_count               => x_msg_count,
            x_msg_data                => x_msg_data);

         IF x_ret_status <> 'S'
         THEN
            l_sec :=
                  'Inactivation process for Party Site Id '
               || rec_sites.party_site_id
               || 'Errored';
            EXIT;
         END IF;

         l_sec := 'Inactivated Party Site Id ' || rec_sites.party_site_id;
      END LOOP;

      l_sec := 'Inactivating Party Sites Completed';
   EXCEPTION
      WHEN OTHERS
      THEN
         xxcus_error_pkg.xxcus_error_main_api (
            p_called_from         => g_err_callfrom || '.' || lvc_procedure,
            p_calling             => l_sec,
            p_ora_error_msg       => SQLERRM,
            p_error_desc          =>    'Error Occured for '
                                     || p_record_id
                                     || 'record id in Inactivating Party Sites.',
            p_distribution_list   => g_distro_list,
            p_module              => g_module);
   END;
END XXWC_AR_EQUIFAX_CUST_ACCT_PKG;
/