---------------------------------------------------------------------------------------------------------------
/**************************************************************************************************************
  $Header XXEIS.EIS_XXWC_OM_RENTAL_INV_ERROR_V $
  Module Name : Order Management
  PURPOSE	  : Rental Recurring Invoice Error Report - WC
  TMS Task Id : 20160627-00182 
  REVISIONS   :
  VERSION DATE               AUTHOR(S)       DESCRIPTION
  ------- -----------------  --------------- -----------------------------------------
  1.0     28-Oct-2016        Siva   		 TMS#20160627-00182   
**************************************************************************************************************/
DROP VIEW XXEIS.EIS_XXWC_OM_RENTAL_INV_ERROR_V;

CREATE OR REPLACE VIEW XXEIS.EIS_XXWC_OM_RENTAL_INV_ERROR_V (ORDER_NUMBER, ERROR_MESSAGE, ORDER_TYPE, BRANCH, ORDER_CREATION_DATE)
AS
  SELECT oh.order_number,
    xx.error_message,
    ot.name order_type,
    od.organization_code branch,
    oh.creation_date order_creation_date
  FROM 
	(SELECT order_header_id,
      listagg(error_message, ' , ') within GROUP (
    ORDER BY error_message) error_message
    FROM
      (SELECT order_header_id,
        error_message,
        row_number() over (partition BY order_header_id, error_message order by order_header_id) AS rn
      FROM xxwc.xxwc_rental_engine_error_log
      ORDER BY order_header_id,
        error_message
      )
    WHERE rn = 1
    GROUP BY order_header_id
    ) xx,
    --xxwc.xxwc_rental_engine_error_log xx,
    apps.oe_order_headers_all oh,
    apps.oe_transaction_types_tl ot,
    apps.mtl_parameters od
  WHERE xx.order_header_id = oh.header_id
  AND oh.order_type_id     = ot.transaction_type_id
  AND ot.language          = userenv('LANG')
  AND oh.ship_from_org_id  = od.organization_id
  AND oh.flow_status_code  = 'BOOKED'
/
