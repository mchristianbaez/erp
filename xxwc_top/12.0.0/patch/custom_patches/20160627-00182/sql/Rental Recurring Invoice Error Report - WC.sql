--Report Name            : Rental Recurring Invoice Error Report - WC
--Import Instructions    : Run this file in XXEIS user from SQL*Plus or any tool like Toad, SQL Navigator. 
prompt Creating View Data for Rental Recurring Invoice Error Report - WC
set scan off define off
DECLARE
BEGIN 
--Inserting View EIS_XXWC_OM_RENTAL_INV_ERROR_V
xxeis.eis_rs_ins.v( 'EIS_XXWC_OM_RENTAL_INV_ERROR_V',660,'','','','','SA059956','XXEIS','Eis Xxwc Om Rental Inv Error V','EXORIEV','','');
--Delete View Columns for EIS_XXWC_OM_RENTAL_INV_ERROR_V
xxeis.eis_rs_utility.delete_view_rows('EIS_XXWC_OM_RENTAL_INV_ERROR_V',660,FALSE);
--Inserting View Columns for EIS_XXWC_OM_RENTAL_INV_ERROR_V
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_RENTAL_INV_ERROR_V','ORDER_CREATION_DATE',660,'Order Creation Date','ORDER_CREATION_DATE','','','','SA059956','DATE','','','Order Creation Date','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_RENTAL_INV_ERROR_V','BRANCH',660,'Branch','BRANCH','','','','SA059956','VARCHAR2','','','Branch','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_RENTAL_INV_ERROR_V','ORDER_TYPE',660,'Order Type','ORDER_TYPE','','','','SA059956','VARCHAR2','','','Order Type','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_RENTAL_INV_ERROR_V','ERROR_MESSAGE',660,'Error Message','ERROR_MESSAGE','','','','SA059956','VARCHAR2','','','Error Message','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_RENTAL_INV_ERROR_V','ORDER_NUMBER',660,'Order Number','ORDER_NUMBER','','','','SA059956','NUMBER','','','Order Number','','','');
--Inserting View Components for EIS_XXWC_OM_RENTAL_INV_ERROR_V
--Inserting View Component Joins for EIS_XXWC_OM_RENTAL_INV_ERROR_V
END;
/
set scan on define on
prompt Creating Report LOV Data for Rental Recurring Invoice Error Report - WC
set scan off define off
DECLARE
BEGIN 
--Inserting Report LOVs - Rental Recurring Invoice Error Report - WC
xxeis.eis_rs_ins.lov( 660,'SELECT organization_code branch,
  organization_name
FROM org_organization_definitions ood
WHERE SYSDATE < NVL (ood.disable_date, SYSDATE + 1)
AND EXISTS
  (SELECT 1
  FROM xxeis.eis_org_access_v
  WHERE organization_id = ood.organization_id
  )','','XXWC OM Organization Code','','SA059956',NULL,'Y','','');
END;
/
set scan on define on
prompt Creating Report Data for Rental Recurring Invoice Error Report - WC
set scan off define off
DECLARE
BEGIN 
--Deleting Report data - Rental Recurring Invoice Error Report - WC
xxeis.eis_rs_utility.delete_report_rows( 'Rental Recurring Invoice Error Report - WC' );
--Inserting Report - Rental Recurring Invoice Error Report - WC
xxeis.eis_rs_ins.r( 660,'Rental Recurring Invoice Error Report - WC','','Rental Invoice Error report','','','','SA059956','EIS_XXWC_OM_RENTAL_INV_ERROR_V','Y','','','SA059956','','N','White Cap Reports','','CSV,EXCEL,','N');
--Inserting Report Columns - Rental Recurring Invoice Error Report - WC
xxeis.eis_rs_ins.rc( 'Rental Recurring Invoice Error Report - WC',660,'BRANCH','Branch','Branch','','','default','','4','N','','','','','','','','SA059956','N','N','','EIS_XXWC_OM_RENTAL_INV_ERROR_V','','');
xxeis.eis_rs_ins.rc( 'Rental Recurring Invoice Error Report - WC',660,'ERROR_MESSAGE','Error Message','Error Message','','','default','','2','N','','','','','','','','SA059956','N','N','','EIS_XXWC_OM_RENTAL_INV_ERROR_V','','');
xxeis.eis_rs_ins.rc( 'Rental Recurring Invoice Error Report - WC',660,'ORDER_CREATION_DATE','Order Creation Date','Order Creation Date','','','default','','5','N','','','','','','','','SA059956','N','N','','EIS_XXWC_OM_RENTAL_INV_ERROR_V','','');
xxeis.eis_rs_ins.rc( 'Rental Recurring Invoice Error Report - WC',660,'ORDER_NUMBER','Order Number','Order Number','','~~~','default','','1','N','','','','','','','','SA059956','N','N','','EIS_XXWC_OM_RENTAL_INV_ERROR_V','','');
xxeis.eis_rs_ins.rc( 'Rental Recurring Invoice Error Report - WC',660,'ORDER_TYPE','Order Type','Order Type','','','default','','3','N','','','','','','','','SA059956','N','N','','EIS_XXWC_OM_RENTAL_INV_ERROR_V','','');
--Inserting Report Parameters - Rental Recurring Invoice Error Report - WC
xxeis.eis_rs_ins.rp( 'Rental Recurring Invoice Error Report - WC',660,'Branch','Branch','BRANCH','IN','XXWC OM Organization Code','','VARCHAR2','N','Y','1','','Y','CONSTANT','SA059956','Y','N','','','');
xxeis.eis_rs_ins.rp( 'Rental Recurring Invoice Error Report - WC',660,'Creation Date From','Creation Date From','ORDER_CREATION_DATE','>=','','','DATE','N','Y','2','','Y','CONSTANT','SA059956','Y','N','','Start Date','');
xxeis.eis_rs_ins.rp( 'Rental Recurring Invoice Error Report - WC',660,'Creation Date To','Creation Date To','ORDER_CREATION_DATE','<=','','','DATE','N','Y','3','','Y','CONSTANT','SA059956','Y','N','','End Date','');
--Inserting Report Conditions - Rental Recurring Invoice Error Report - WC
xxeis.eis_rs_ins.rcn( 'Rental Recurring Invoice Error Report - WC',660,'BRANCH','IN',':Branch','','','Y','1','Y','SA059956');
xxeis.eis_rs_ins.rcn( 'Rental Recurring Invoice Error Report - WC',660,'TRUNC(ORDER_CREATION_DATE)','>=',':Creation Date From','','','Y','2','Y','SA059956');
xxeis.eis_rs_ins.rcn( 'Rental Recurring Invoice Error Report - WC',660,'TRUNC(ORDER_CREATION_DATE)','<=',':Creation Date To','','','Y','3','Y','SA059956');
--Inserting Report Sorts - Rental Recurring Invoice Error Report - WC
xxeis.eis_rs_ins.rs( 'Rental Recurring Invoice Error Report - WC',660,'BRANCH','ASC','SA059956','','');
xxeis.eis_rs_ins.rs( 'Rental Recurring Invoice Error Report - WC',660,'ORDER_NUMBER','ASC','SA059956','','');
--Inserting Report Triggers - Rental Recurring Invoice Error Report - WC
--Inserting Report Templates - Rental Recurring Invoice Error Report - WC
--Inserting Report Portals - Rental Recurring Invoice Error Report - WC
--Inserting Report Dashboards - Rental Recurring Invoice Error Report - WC
--Inserting Report Security - Rental Recurring Invoice Error Report - WC
xxeis.eis_rs_ins.rsec( 'Rental Recurring Invoice Error Report - WC','660','','51044',660,'SA059956','','');
--Inserting Report Pivots - Rental Recurring Invoice Error Report - WC
END;
/
set scan on define on
