CREATE OR REPLACE PACKAGE BODY APPS.XXWC_CUST_PREDOMNT_TRD_UPD_PKG
AS

/**************************************************************************
File Name: PredominantTradeUpdate.sql
TYPE:      SQL Script
Description: Script to update Customer Classification AND Predominant Trade

VERSION DATE          AUTHOR(S)       DESCRIPTION
------- -----------   --------------- ---------------------------------
1.0     03-AUG-2015  Pattabhi Avula   Initial version TMS#20150331-00026
**************************************************************************/
--g_package      VARCHAR2(100) := 'xxwc_cust_predmnt_trd_upd_pkg.customer_predmnt_trd_update';
--g_exceptemail  VARCHAR2(100) := 'HDSOracleDevelopers@hdsupply.com';

PROCEDURE customer_predmnt_trd_update(p_predominant_trade varchar2,
									  p_customer_class_code varchar2,
									  p_account_number varchar2) 
  IS
     l_prof_amt_rec                 HZ_CUSTOMER_PROFILE_V2PUB.cust_profile_amt_rec_type;
     l_return_status                VARCHAR2(2000);
     l_msg_count                    NUMBER;
     l_msg_data                     VARCHAR2(2000);
     l_cust_account_id              NUMBER;
     l_err_flag                     VARCHAR2(1);
     l_error_message                VARCHAR2(2000);
     l_cust_class_cnt               NUMBER;
     l_cust_class_code              VARCHAR2(200);
	 l_object_ver_num               NUMBER;
	

     p_cust_account_rec              HZ_CUST_ACCOUNT_V2PUB.CUST_ACCOUNT_REC_TYPE;
     p_site_object_version_number    NUMBER;


     p_organization_rec              HZ_PARTY_V2PUB.ORGANIZATION_REC_TYPE;
     p_party_rec                     HZ_PARTY_V2PUB.PARTY_REC_TYPE;
     p_cust_site_use_rec             HZ_CUST_ACCOUNT_SITE_V2PUB.cust_site_use_rec_type;

  

BEGIN

         l_err_flag := 'N';
         l_error_message := NULL;
--	IF p_customer_class_code IS NOT NULL THEN
		
		  BEGIN
		    SELECT cust_account_id, object_version_number
			INTO  l_cust_account_id,l_object_ver_num
			FROM  hz_cust_accounts_all
			WHERE account_number=p_account_number
			AND (NVL(customer_class_code,'XX')!=p_customer_class_code 
			OR NVL(attribute9,'XX')!=p_predominant_trade);
          EXCEPTION
		    WHEN NO_DATA_FOUND THEN
		       l_err_flag := 'Y';
		       l_error_message := 'Error validating Customer Number - '||SQLERRM;
			  RAISE PROGRAM_ERROR;
			WHEN others THEN
		       l_err_flag := 'Y';
		       l_error_message := 'Error validating Customer Number - '||SQLERRM;
			  RAISE PROGRAM_ERROR;
		  END;
         
         
			 BEGIN
			 SELECT lookup_code
			   INTO l_cust_class_code
			   FROM ar_lookups
			  WHERE 1 = 1
				AND lookup_type = 'CUSTOMER CLASS'
				AND meaning = p_customer_class_code
				AND enabled_flag = 'Y'
				AND TRUNC(SYSDATE) BETWEEN START_DATE_ACTIVE AND NVL(END_DATE_ACTIVE, TRUNC(SYSDATE) + 1);
			 EXCEPTION
			 WHEN NO_DATA_FOUND THEN
			   BEGIN
				 SELECT lookup_code
				   INTO l_cust_class_code
				   FROM ar_lookups
				  WHERE 1 = 1
					AND lookup_type = 'CUSTOMER CLASS'
					AND lookup_code = p_customer_class_code
					AND enabled_flag = 'Y'
					AND TRUNC(SYSDATE) BETWEEN START_DATE_ACTIVE AND NVL(END_DATE_ACTIVE, TRUNC(SYSDATE) + 1);                
				EXCEPTION
				WHEN OTHERS THEN
				   l_err_flag := 'Y';
				   l_error_message := 'Error validating Customer Class - '||SQLERRM;
				   RAISE PROGRAM_ERROR;
				END;
			 WHEN OTHERS THEN
			   l_err_flag := 'Y';
			   l_error_message := 'Error validating Customer Class - '||SQLERRM;
			   RAISE PROGRAM_ERROR;
			 END;
		
         
		 IF l_err_flag = 'N' THEN
             p_cust_account_rec.cust_account_id        := l_cust_account_id;
             p_cust_account_rec.customer_class_code    := l_cust_class_code; --r_acc_name.customer_class_code;
             p_cust_account_rec.attribute9             := p_predominant_trade;
             
             
           
              --------------------------------------------------------------------
              -- Call API to update cust account name
              --------------------------------------------------------------------
                 hz_cust_account_v2pub.update_cust_account ( p_init_msg_list           => fnd_api.g_true,
                                        p_cust_account_rec        => p_cust_account_rec,
                                        p_object_version_number   => l_object_ver_num,
                                        x_return_status           => l_return_status,
                                        x_msg_count               => l_msg_count,
                                        x_msg_data                => l_msg_data);

             COMMIT;
		     IF l_msg_count > 1 THEN
                l_error_message:= SUBSTR(FND_MSG_PUB.Get(p_encoded=>FND_API.G_FALSE ), 1, 255);
                RAISE PROGRAM_ERROR;
			 END IF;
		 END IF;
		 -- COMMIT;
  EXCEPTION
      WHEN PROGRAM_ERROR
      THEN
         l_error_message := 'Invalid Data: ' || l_error_message;
         fnd_message.set_name ('XXWC', 'XXWC_ADI_ERROR_MSG');
         fnd_message.set_token ('ERROR_MESSAGE', l_error_message);
		 ROLLBACK;
      WHEN OTHERS
      THEN
         l_error_message := 'Unidentified error - ' || SQLERRM;
         fnd_message.set_name ('XXWC', 'XXWC_ADI_ERROR_MSG');
         fnd_message.set_token ('ERROR_MESSAGE', l_error_message);
		 ROLLBACK;
    END customer_predmnt_trd_update;

END XXWC_CUST_PREDOMNT_TRD_UPD_PKG;
/