/******************************************************************************
  $Header TMS_20180314_00006 _Data_Fix.sql $
  Module Name:Data Fix script for 20180314_00006 

  PURPOSE: Data fix script for 20180125-00329 Update Order

  REVISIONS:
  Ver        Date         Author                Description
  ---------  -----------  ------------------    -------------------------------
  1.0        10-Apr-2018  Krishna Kumar         20180314-00006

*******************************************************************************/

ALTER SESSION SET CURRENT_SCHEMA=apps;
/
BEGIN
   mo_global.set_policy_context ('S', '162');
END;
/
SET serveroutput ON SIZE 500000;
DECLARE

BEGIN

update  apps.oe_order_headers_all
set     flow_status_code = 'CANCELLED',
        open_flag = 'N'
where   header_id = 69113811;

dbms_output.put_line('Number of Header Rows Updated '||SQL%ROWCOUNT);

update  apps.oe_order_lines_all
set     cancelled_quantity = ordered_quantity,
        ordered_quantity = 0,
        open_flag = 'N',
        cancelled_flag = 'Y',
        flow_status_code ='CANCELLED'
where   header_id = 69113811
and     open_flag = 'Y';

dbms_output.put_line('Number of Line Rows Updated '||SQL%ROWCOUNT);

COMMIT;

EXCEPTION
    WHEN OTHERS THEN
      ROLLBACK;
      dbms_output.put_line(substr(sqlerrm, 1, 240));
END;
/