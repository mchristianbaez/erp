select 'Data After Update' from DUAL
/
SELECT inventory_item_id,
  change_id,
  oracle_change_id
FROM XXWC.XXWC_MTL_SY_ITEMS_CHG_B
WHERE ( inventory_item_id = 9116271
AND EXISTS
  (SELECT 'x' FROM v$database WHERE name ='EBIZFQA'
  ))
OR ( inventory_item_id = 9415584
AND EXISTS
  (SELECT 'x' FROM v$database WHERE name ='EBZIPRD'
  ))
/

select 'Update the data' from DUAL
/

UPDATE XXWC.XXWC_MTL_SY_ITEMS_CHG_B  a
SET CHANGE_ID=
  (SELECT change_id
  FROM XXWC.XXWC_CHG_EGO_ITEM_ATTRS
  WHERE INVENTORY_ITEM_Id= a.inventory_item_id AND rownum=1
  ),
  ORACLE_CHANGE_ID     =
  (SELECT change_id
  FROM XXWC.XXWC_CHG_EGO_ITEM_ATTRS
  WHERE INVENTORY_ITEM_Id= a.inventory_item_id AND rownum=1
  )
WHERE ( inventory_item_id = 9116271
AND EXISTS
  (SELECT 'x' FROM v$database WHERE name ='EBIZFQA'
  ))
OR ( inventory_item_id = 9415584
AND EXISTS
  (SELECT 'x' FROM v$database WHERE name ='EBZIPRD'
  ))
/

select 'Data After Update' from DUAL
/

SELECT inventory_item_id,
  change_id,
  oracle_change_id
FROM XXWC.XXWC_MTL_SY_ITEMS_CHG_B
WHERE ( inventory_item_id = 9116271
AND EXISTS
  (SELECT 'x' FROM v$database WHERE name ='EBIZFQA'
  ))
OR ( inventory_item_id = 9415584
AND EXISTS
  (SELECT 'x' FROM v$database WHERE name ='EBZIPRD'
  ))
/