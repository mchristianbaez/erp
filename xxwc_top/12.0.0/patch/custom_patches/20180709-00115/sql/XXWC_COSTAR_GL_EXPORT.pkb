CREATE OR REPLACE PACKAGE BODY APPS.XXWC_COSTAR_GL_EXPORT AS
/**************************************************************************
   $Header XXWC_COSTAR_GL_EXPORT $
   Module Name: XXWC_COSTAR_GL_EXPORT.pkb

   PURPOSE:   This package is called by the concurrent programs
              XXWC CoStar GL Journal Import for importing Journals into EBS system.
   REVISIONS:
   Ver        Date        Author             	  Description
   ---------  ----------  ---------------   	  -------------------------
    1.0       08/07/2018  Vamshi Singirikonda  	Initial Build - Task ID: 20180709-00115						  
/*************************************************************************/
--Global Variables...
g_loc                    VARCHAR2(20000);
g_qty_invoiced           NUMBER:=1;
g_line_type_lkup_code    VARCHAR2(10):='ITEM';
g_org_id                 NUMBER:=163;
g_user_id                NUMBER:=fnd_global.user_id;
g_request_id             NUMBER:=fnd_global.conc_request_id;
g_file_name              VARCHAR2(240):=NULL;
g_delimiter              VARCHAR2(1):='|';
g_inbound_loc            VARCHAR2(240):=NULL;
g_directory_path         VARCHAR2(150):='XXWC_COSTAR_GLJ_IB_DIR';
g_pkg_name               VARCHAR2(50):='XXWC_COSTAR_GL_EXPORT';
g_run_id                 NUMBER;
g_seq                    NUMBER:=0;
g_status1                VARCHAR2(240):='File Copied to custom staging';
g_fiscal_period          VARCHAR2(10):=TO_CHAR(SYSDATE,'Mon-YYYY');
                         
/*************************************************************************
  Procedure : Write_Log

  PURPOSE:   This procedure logs debug message in Concurrent Log file
  Parameter: IN  p_debug_msg      -- Debug Message
  REVISIONS:
   Ver        Date        Author             	  Description
   ---------  ----------  ---------------   	  -------------------------
    1.0       08/07/2018  Vamshi Singirikonda  	Initial Build - Task ID: 20180709-00115						  
************************************************************************/
PROCEDURE write_log (p_debug_msg IN VARCHAR2) IS

BEGIN
      
  fnd_file.put_line (fnd_file.LOG, p_debug_msg);
  DBMS_OUTPUT.put_line (p_debug_msg);

END write_log;

/*************************************************************************
  Procedure : write_error

  PURPOSE:   This procedure logs debug message in Concurrent Out file
  Parameter: IN  p_debug_msg      -- Debug Message
  REVISIONS:
   Ver        Date        Author             	  Description
   ---------  ----------  ---------------   	  -------------------------
    1.0       08/07/2018  Vamshi Singirikonda  	Initial Build - Task ID: 20180709-00115						  
************************************************************************/
--Add message to concurrent output file
PROCEDURE write_error (p_debug_msg IN VARCHAR2) IS

l_req_id          NUMBER := fnd_global.conc_request_id;
l_err_callfrom    VARCHAR2 (75) DEFAULT 'XXWC_COSTAR_GL_EXPORT';
l_err_callpoint   VARCHAR2 (75) DEFAULT 'START';
l_distro_list     VARCHAR2 (75) DEFAULT 'HDSOracleDevelopers@hdsupply.com';

BEGIN

  xxcus_error_pkg.xxcus_error_main_api (
      p_called_from         => l_err_callfrom,
      p_calling             => l_err_callpoint,
      p_request_id          => l_req_id,
      p_ora_error_msg       => SUBSTR (p_debug_msg, 1, 2000),
      p_error_desc          => 'Error running XXWC_COSTAR_GL_EXPORT with PROGRAM ERROR',
      p_distribution_list   => l_distro_list,
      p_module              => 'GL');

END write_error;

/*************************************************************************
  Function : get_run_id

  PURPOSE:   This Function return the run id for the given program run.
  Parameter: NA
  REVISIONS:
  Ver        Date        Author             	  Description
   ---------  ----------  ---------------   	  -------------------------
    1.0       08/07/2018  Vamshi Singirikonda  	Initial Build - Task ID: 20180709-00115						  
************************************************************************/
FUNCTION get_run_id RETURN NUMBER IS

  ln_run_seq NUMBER;

BEGIN

  ln_run_seq:= GL.GL_JOURNAL_IMPORT_S.NEXTVAL ;
  --XXWC.XXWC_COSTAR_GL_EXPORT_RUN_S.NEXTVAL;
  g_run_id:=ln_run_seq;
  
  RETURN ln_run_seq;

EXCEPTION 
WHEN others THEN

  RETURN 0;
  
END get_run_id;

/*************************************************************************
  Procedure : Process_Journals

  PURPOSE:   This main procedure extracts the Journaldata from data file and loads into EBS system.
             This also calls many subroutines in the process.
  Parameter: IN  p_errbuf 
             IN  p_retcode
             IN  p_file_name
             IN  p_run_id             
  REVISIONS:
  Ver        Date        Author             	  Description
   ---------  ----------  ---------------   	  -------------------------
    1.0       08/07/2018  Vamshi Singirikonda  	Initial Build - Task ID: 20180709-00115						  
************************************************************************/
PROCEDURE Process_Journals(p_errbuf    OUT VARCHAR2
                          ,p_retcode   OUT VARCHAR2
                          ,p_file_name IN  VARCHAR2
                          ,p_run_id    IN  NUMBER                          
                          ) IS

l_count            NUMBER;
g_prog_exception   EXCEPTION;
v_file             UTL_FILE.file_type;
l_err_msg          VARCHAR2 (4000);  

-- Cursors Here

BEGIN
write_log ('Inside process journals procedure');
/*
  g_file_name:=p_file_name;
  g_inbound_loc:=g_directory_path; 
  
  --Printing in Parameters
  g_loc := 'Begining sample file Extract  ';
  write_log (g_loc);
  write_log ('========================================================');
  write_log ('File Name -'||g_file_name);
  write_log ('Calling the Journal Load procedure');
  
  --Calling Procedure to Load data from data file to staging table...
  Load_Journal_Data(p_run_id);

  write_log ('Data Loading into staging completed successfully...');
  
  write_log ('Calling the Audit Table Load procedure...');
  
  --Inserting into Audit Table
  LOAD_AUDIT_DATA(p_run_id                 
                 ,'STAGING'
                 );
  
  --Looping for the records in Staging table to perform any validations
  -- Add Code Here
  
  --Inserting Interface into Audit Table
  LOAD_AUDIT_DATA(p_run_id                    
                 ,'INTERFACE'
                 );
  
  --Initialize the Out Parameter
  p_errbuf := NULL;
  p_retcode := '0';
  
  write_log ('XXWC CoStar GL Journal Import - Program Successfully completed');
*/
EXCEPTION
WHEN g_prog_exception THEN

   p_errbuf := l_err_msg;
   p_retcode := '2';
   write_error (l_err_msg);
   write_log (l_err_msg);
   
WHEN others THEN

   l_err_msg := 'Error Msg :' || SQLERRM;
   p_errbuf := l_err_msg;
   p_retcode := '2';
   write_error (l_err_msg);
   write_log (l_err_msg);

END Process_Journals;

/*************************************************************************
  Procedure : Load_Journal_Data

  PURPOSE:   This procedure imports the AP invoice data from data file and insert into staging table.
  Parameter: IN  p_run_id
  REVISIONS:
  Ver        Date        Author             	  Description
   ---------  ----------  ---------------   	  -------------------------
    1.0       08/07/2018  Vamshi Singirikonda  	Initial Build - Task ID: 20180709-00115						  
************************************************************************/
PROCEDURE Load_Journal_Data(p_run_id IN NUMBER) IS

l_err_msg       VARCHAR2 (4000);
input_file_id   UTL_FILE.FILE_TYPE;
line_read       VARCHAR2(32767):= NULL;
line_count      NUMBER :=1;
lines_processed NUMBER :=0;
l_sub_routine   VARCHAR2(30) :='Load_Journal_Data';
b_move_fwd      BOOLEAN;
l_record_type         VARCHAR2(30);  
ln_user_id            NUMBER:=fnd_global.user_id;  
ln_request_id         NUMBER:=fnd_global.conc_request_id; 
BEGIN

  input_file_id  := utl_file.fopen(g_inbound_loc, g_file_name, 'R', 32767);
  write_log ('File opened for reading, file handle succesfully initiated...');
  g_run_id:=p_run_id;
  
  LOOP
  
    BEGIN

      utl_file.get_line(input_file_id, line_read, 32767);

      IF (line_count =1) THEN
      
        NULL; --ignore the header line
        write_log('Header :'||line_count||', Ignore header record.');
        
        line_count:= line_count+1; --set the line number to the next one in the file

      ELSE

        write_log('Line :'||line_count);

        BEGIN

          -- Parse file here
		  
          b_move_fwd :=TRUE;

        EXCEPTION
        WHEN others THEN

          write_log ('Error in get fields, line_count ='||line_count||', msg  ='||sqlerrm);
          b_move_fwd :=FALSE;

        END;

        
        line_count:= line_count+1;

      END IF;

    EXCEPTION
    WHEN no_data_found THEN

      EXIT;
                 
    WHEN others THEN

      raise_application_error(-20010,'Line: '||line_read);
      raise_application_error(-20010,' Unknown Errors: '||sqlerrm);

      EXIT;

    END;
  
  END LOOP;  
  
  COMMIT;
  
  utl_file.fclose(input_file_id);
  write_log ('After file close.');

EXCEPTION
WHEN utl_file.invalid_path THEN

  write_log('File: '||g_file_name||' Invalid Path: '||sqlerrm);
  raise_application_error(-20010,'File: '||g_file_name||' Invalid Path: '||sqlerrm);
   
WHEN utl_file.invalid_mode THEN

  write_log('File: '||g_file_name||' Invalid Mode: '||sqlerrm);
  raise_application_error(-20010,'File: '||g_file_name||' Invalid Mode: '||sqlerrm);
   
WHEN utl_file.invalid_operation THEN

  write_log('File: '||g_file_name||' Invalid Operation: '||sqlerrm);
  raise_application_error(-20010,'File: '||g_file_name||' Invalid Operation: '||sqlerrm);
   
WHEN utl_file.invalid_filehandle THEN

  write_log('File: '||g_file_name||' nvalid File Handle: '||sqlerrm);
  raise_application_error(-20010,'File: '||g_file_name||' Invalid File Handle: '||sqlerrm);
   
WHEN utl_file.read_error THEN

  write_log('File: '||g_file_name||' File Read Error: '||sqlerrm);
  raise_application_error(-20010,'File: '||g_file_name||' File Read Error: '||sqlerrm);
   
WHEN utl_file.internal_error THEN

  write_log('File: '||g_file_name||' File Internal Error: '||sqlerrm);
  raise_application_error(-20010,'File: '||g_file_name||' File Internal Error: '||sqlerrm);

WHEN others THEN

  write_log('Issue in '||g_pkg_name||'.'||l_sub_routine||', message =>'||sqlerrm);
  write_error('Issue in '||g_pkg_name||'.'||l_sub_routine||', message =>'||sqlerrm);
  raise program_error;
  raise;

END Load_Journal_Data;

/*************************************************************************
  FUNCTION : get_field

  PURPOSE:   This function is used to extract the data from utl_file.
  Parameter: IN  v_delimiter    
             IN  n_field_no
             IN  v_line_read
             IN  p_which_line
  REVISIONS:
   Ver        Date        Author             	  Description
   ---------  ----------  ---------------   	  -------------------------
    1.0       08/07/2018  Vamshi Singirikonda  	Initial Build - Task ID: 20180709-00115	
************************************************************************/
FUNCTION get_field (v_delimiter  IN VARCHAR2
                   ,n_field_no   IN NUMBER 
                   ,v_line_read  IN VARCHAR2
                   ,p_which_line IN NUMBER) RETURN VARCHAR2 IS

  l_sub_routine     VARCHAR2(30) :='get_field';
  n_start_field_pos NUMBER;
  n_end_field_pos   NUMBER;
  v_get_field       VARCHAR2(2000);

BEGIN

  IF n_field_no = 1 THEN
  
    n_start_field_pos := 1;

  ELSE
  
    n_start_field_pos := INSTR(v_line_read,v_delimiter,1,n_field_no-1)+1;

  END IF;
  
  write_log('n_start_field_pos '||n_start_field_pos||', n_field_no '||n_field_no);
  n_end_field_pos   := instr(v_line_read,v_delimiter,1,n_field_no) -1;
  write_log('n_end_field_pos '||n_end_field_pos||', n_field_no '||n_field_no);

  IF n_end_field_pos > 0 THEN
  
    v_get_field := substr(v_line_read,n_start_field_pos,(n_end_field_pos - n_start_field_pos)+1);
    write_log('v_get_field when >0 is '||v_get_field||', n_field_no '||n_field_no);

  ELSE

    v_get_field := substr(v_line_read,n_start_field_pos);
    write_log('v_get_field when 0 is '||v_get_field||', n_field_no '||n_field_no);

  END IF;

  RETURN LTRIM(RTRIM(v_get_field));

EXCEPTION
WHEN others THEN
  
   write_log('Issue in '||g_pkg_name||'.'||l_sub_routine||', message =>'||SQLERRM);
   write_log ('Line# ='||p_which_line);
   write_log ('Line ='||v_line_read);
   write_log ('get field: '||SQLERRM);

END get_field;


/*************************************************************************
  Procedure : LOAD_AUDIT_DATA

  PURPOSE:   This procedure is used to load the audit data.
  Parameter: IN  p_run_id    
             IN  p_group_id
             IN  p_stage
  REVISIONS:
  Ver        Date        Author             	  Description
   ---------  ----------  ---------------   	  -------------------------
    1.0       08/07/2018  Vamshi Singirikonda  	Initial Build - Task ID: 20180709-00115						  
************************************************************************/
PROCEDURE LOAD_AUDIT_DATA(p_run_id   IN NUMBER                         
                         ,p_stage    IN VARCHAR2) IS

--Add Cursor Here

ln_seq        NUMBER:=0;
l_sub_routine VARCHAR2(50):='LOAD_AUDIT_DATA';
ln_max_seq    NUMBER;
lv_file_name  VARCHAR2(100);

BEGIN
write_log('Inside Procedure Load Audit Data');
EXCEPTION
WHEN others THEN

  write_log('Issue in '||g_pkg_name||'.'||l_sub_routine||', message =>'||sqlerrm);
  write_error('Issue in '||g_pkg_name||'.'||l_sub_routine||', message =>'||sqlerrm);
  raise program_error;
  raise;
  
END LOAD_AUDIT_DATA;


END XXWC_COSTAR_GL_EXPORT;
/