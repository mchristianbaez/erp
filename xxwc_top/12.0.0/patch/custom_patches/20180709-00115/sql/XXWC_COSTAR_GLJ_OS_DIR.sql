/**************************************************************************
   $Header XXWC_COSTAR_GLJ_OS_DIR $
   Module Name: XXWC_COSTAR_GLJ_OS_DIR.sql

   PURPOSE:   This script is used to create the directory location for 
              Importing COstar Journals 
   REVISIONS:
   Ver        Date        Author             	  Description
   ---------  ----------  ---------------   	  -------------------------
    1.0       08/07/2018  Vamshi Singirikonda  	Initial Build - Task ID: 20180709-00115						  
/*************************************************************************/
SET SERVEROUTPUT ON SIZE 1000000
DECLARE
  v_result   VARCHAR2(2000);
  v_command  VARCHAR2(2000);
  v_db       VARCHAR2(10):=NULL;
  l_folder   VARCHAR2(200);
  l_folder_a VARCHAR2(200);
BEGIN
  SELECT lower(name) INTO v_db FROM v$database;
  l_folder  := '/xx_iface/'||v_db||'/inbound/uc4/costar';
  v_command :='mkdir '||l_folder ;
  dbms_output.put_line('OS command to Create costar Directory =>'||v_command);
  BEGIN
    SELECT xxwc_edi_iface_pkg.xxwc_oscommand_run (v_command)
    INTO v_result
    FROM dual;
    dbms_output.put_line('Costar Folder Creation  Result =>'||v_result);
    v_command :='chmod '||'777 '||l_folder;
    dbms_output.put_line('OS command to change Permissions =>'||v_command);
    BEGIN
      SELECT xxwc_edi_iface_pkg.xxwc_oscommand_run (v_command)
      INTO v_result
      FROM dual;
      dbms_output.put_line('Costar Folder Permission  Result =>'||v_result);
      l_folder  := l_folder || '/gl' ;
      v_command :='mkdir '||l_folder ;
      dbms_output.put_line('OS command to Create gl Directory =>'||v_command);
      BEGIN
        SELECT xxwc_edi_iface_pkg.xxwc_oscommand_run (v_command)
        INTO v_result
        FROM dual;
        dbms_output.put_line('GL Folder Creation  Result =>'||v_result);
        v_command :='chmod '||'777 '||l_folder;
        dbms_output.put_line('OS command to change Permissions =>'||v_command);
        BEGIN
          SELECT xxwc_edi_iface_pkg.xxwc_oscommand_run (v_command)
          INTO v_result
          FROM dual;
          dbms_output.put_line('GL Folder Permission  Result =>'||v_result);
          l_folder_a := l_folder || '/archive' ;
		  l_folder   := l_folder || '/CI_WC' ;          
          v_command  :='mkdir '||l_folder ;
          dbms_output.put_line('OS command to Create CI_WC Directory =>'||v_command);
          BEGIN
            SELECT xxwc_edi_iface_pkg.xxwc_oscommand_run (v_command)
            INTO v_result
            FROM dual;
            dbms_output.put_line('CI_WC Folder Creation  Result =>'||v_result);
            v_command :='chmod '||'777 '||l_folder;
            dbms_output.put_line('OS command to change Permissions =>'||v_command);
            BEGIN
              SELECT xxwc_edi_iface_pkg.xxwc_oscommand_run (v_command)
              INTO v_result
              FROM dual;
              dbms_output.put_line('CI_WC Folder Permission  Result =>'||v_result);
              v_command :='mkdir '||l_folder_a ;
              dbms_output.put_line('OS command to Create archive Directory =>'||v_command);
              BEGIN
                SELECT xxwc_edi_iface_pkg.xxwc_oscommand_run (v_command)
                INTO v_result
                FROM dual;
                dbms_output.put_line('CI_WC Folder Creation  Result =>'||v_result);
                v_command :='chmod '||'777 '||l_folder_a;
                dbms_output.put_line('OS command to change Permissions =>'||v_command);
                BEGIN
                  SELECT xxwc_edi_iface_pkg.xxwc_oscommand_run (v_command)
                  INTO v_result
                  FROM dual;
                  dbms_output.put_line('Archive Folder Permission  Result =>'||v_result);
                EXCEPTION
                WHEN OTHERS THEN
                  dbms_output.put_line('Failed to change permissions for archive Folder, message =>'||v_result);
                END;
              EXCEPTION
              WHEN OTHERS THEN
                dbms_output.put_line('Failed to create archive folder, message =>'||v_result);
              END;
            EXCEPTION
            WHEN OTHERS THEN
              dbms_output.put_line('Failed to change permissions for CI_WC Folder, message =>'||v_result);
            END;
          EXCEPTION
          WHEN OTHERS THEN
            dbms_output.put_line('Failed to create CI_WC folder, message =>'||v_result);
          END;
        EXCEPTION
        WHEN OTHERS THEN
          dbms_output.put_line('Failed to change permissions for AP Folder, message =>'||v_result);
        END;
      EXCEPTION
      WHEN OTHERS THEN
        dbms_output.put_line('Failed to create AP folder, message =>'||v_result);
      END;
    EXCEPTION
    WHEN OTHERS THEN
      dbms_output.put_line('Failed to change permissions for Costar Folder, message =>'||v_result);
    END;
  EXCEPTION
  WHEN OTHERS THEN
    dbms_output.put_line('Failed to create costar folder, message =>'||v_result);
  END;
EXCEPTION
WHEN OTHERS THEN
  DBMS_OUTPUT.PUT_LINE('OUTER BLOCK : '||SQLERRM);
END;
/
