--Report Name            : Deduction Statments PO Report
--Import Instructions    : Run this file in XXEIS user from SQL*Plus or any tool like Toad, SQL Navigator. 
--Creating View XXEIS_1496489_HEJUWZ_V
CREATE OR REPLACE VIEW APPS.XXEIS_1496489_HEJUWZ_V (MVID, VENDOR, LOB, BU, OFFER_CODE, OFFER_NAME, STATUS_CODE, REBATE_TYPE, PO_NUMBER, PAY_TO_VENDOR_CODE, BU_BRANCH_CODE, LOCATION_SEGMENT, PRODUCT, DATE_ORDERED, RESALE_DATE_CREATED, OFU_GL_DATE, GL_CALENDAR_MONTH, AGREEMENT_YEAR, TOTAL_RECEIPTS, REBATES)
AS
  SELECT  /*+ INDEX(C XXCUS_REBATE_CUSTOMERS_N1) INDEX(qlhv QP_LIST_HEADERS_TL_PK) INDEX(oo OZF_OFFERS_U3)*/
    B.OFU_MVID MVID,
    c.party_name VENDOR,
    B.BILL_TO_PARTY_NAME_LOB LOB,
    b.resale_line_attribute2 BU,
    B.OFFER_CODE,
    QLHV.DESCRIPTION OFFER_NAME,
    OO.STATUS_CODE,
    DECODE(b.activity_media_name,'Coop','COOP','Coop Minimum','COOP','REBATE') Rebate_Type,
    po_number PO_NUMBER,
    B.RESALE_LINE_ATTRIBUTE1 PAY_TO_VENDOR_CODE,
    B.RESALE_LINE_ATTRIBUTE6 BU_BRANCH_CODE,
    B.RESALE_LINE_ATTRIBUTE12 LOCATION_SEGMENT,
    B.RESALE_LINE_ATTRIBUTE11 PRODUCT,
    B.DATE_ORDERED,
    B.RESALE_DATE_CREATED,
    B.OFU_GL_DATE OFU_GL_DATE,
    --  to_char(b.ofu_gl_date,'Mon-YYYY') gl_calendar_month, -- Commented for TMS-20170726-00092
    b.xaeh_period_name gl_calendar_month, -- Added for TMS-20170726-00092
    b.calendar_year agreement_year,
    SUM(
    CASE
      WHEN UTILIZATION_TYPE = 'ACCRUAL'
      THEN SELLING_PRICE*QUANTITY
      ELSE 0
    END) TOTAL_RECEIPTS,
    SUM(UTIL_ACCTD_AMOUNT) REBATES
  FROM XXCUS.XXCUS_OZF_XLA_ACCRUALS_B B,
    XXCUS.XXCUS_REBATE_CUSTOMERS C,
    ozf.ozf_offers oo,
    APPS.QP_LIST_HEADERS_TL qlhv  -- Changed for TMS-20170726-00092
  WHERE 1                =1
  AND C.CUSTOMER_ID      =B.OFU_CUST_ACCOUNT_ID
  AND QLHV.LIST_HEADER_ID=B.QP_LIST_HEADER_ID
  AND qlhv.LANGUAGE      = userenv('LANG') -- Added for TMS-20170726-00092
  AND OO.OFFER_CODE      = B.OFFER_CODE
  GROUP BY B.OFU_MVID,
    c.party_name ,
    B.BILL_TO_PARTY_NAME_LOB ,
    b.resale_line_attribute2,
    B.OFFER_CODE,
    QLHV.DESCRIPTION ,
    OO.STATUS_CODE,
    DECODE(b.activity_media_name,'Coop','COOP','Coop Minimum','COOP','REBATE') ,
    PO_NUMBER ,
    B.RESALE_LINE_ATTRIBUTE1,
    B.RESALE_LINE_ATTRIBUTE6 ,
    B.RESALE_LINE_ATTRIBUTE12 ,
    B.RESALE_LINE_ATTRIBUTE11 ,
    B.DATE_ORDERED,
    B.RESALE_DATE_CREATED,
    B.OFU_GL_DATE ,
    --  TO_CHAR(B.OFU_GL_DATE,'Mon-YYYY'), -- Commented for TMS-20170726-00092
    b.xaeh_period_name, -- Added for TMS-20170726-00092
    B.CALENDAR_YEAR ;
/
prompt Creating Object Data XXEIS_1496489_HEJUWZ_V
set scan off define off
DECLARE
mod_exist VARCHAR2(1);
BEGIN 
mod_exist := XXEIS.EIS_RSC_INSERT.check_if_module_exist(682);
IF mod_exist = 'Y' THEN 
--Inserting Object XXEIS_1496489_HEJUWZ_V
xxeis.eis_rsc_ins.v( 'XXEIS_1496489_HEJUWZ_V',682,'Paste SQL View for Deduction Statments PO Report','','','','DV003828','APPS','Deduction Statments PO Report View','X1HV1','','Y','VIEW','US','','','');
--Delete Object Columns for XXEIS_1496489_HEJUWZ_V
xxeis.eis_rsc_utility.delete_view_rows('XXEIS_1496489_HEJUWZ_V',682,FALSE);
--Inserting Object Columns for XXEIS_1496489_HEJUWZ_V
xxeis.eis_rsc_ins.vc( 'XXEIS_1496489_HEJUWZ_V','MVID',682,'','','','','','DV003828','VARCHAR2','','','Mvid','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_1496489_HEJUWZ_V','VENDOR',682,'','','','','','DV003828','VARCHAR2','','','Vendor','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_1496489_HEJUWZ_V','LOB',682,'','','','','','DV003828','VARCHAR2','','','Lob','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_1496489_HEJUWZ_V','OFFER_NAME',682,'','','','','','DV003828','VARCHAR2','','','Offer Name','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_1496489_HEJUWZ_V','STATUS_CODE',682,'','','','','','DV003828','VARCHAR2','','','Status Code','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_1496489_HEJUWZ_V','REBATE_TYPE',682,'','','','','','DV003828','VARCHAR2','','','Rebate Type','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_1496489_HEJUWZ_V','PO_NUMBER',682,'','','','','','DV003828','VARCHAR2','','','Po Number','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_1496489_HEJUWZ_V','DATE_ORDERED',682,'','','','','','DV003828','DATE','','','Date Ordered','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_1496489_HEJUWZ_V','RESALE_DATE_CREATED',682,'','','','','','DV003828','DATE','','','Resale Date Created','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_1496489_HEJUWZ_V','OFU_GL_DATE',682,'','','','','','DV003828','DATE','','','Ofu Gl Date','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_1496489_HEJUWZ_V','GL_CALENDAR_MONTH',682,'','','','','','DV003828','VARCHAR2','','','Gl Calendar Month','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_1496489_HEJUWZ_V','TOTAL_RECEIPTS',682,'','','','~T~D~2','','DV003828','NUMBER','','','Total Receipts','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_1496489_HEJUWZ_V','REBATES',682,'','','','','','DV003828','NUMBER','','','Rebates','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_1496489_HEJUWZ_V','AGREEMENT_YEAR',682,'','','','','','DV003828','VARCHAR2','','','Agreement Year','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_1496489_HEJUWZ_V','BU',682,'','','','','','DV003828','VARCHAR2','','','Bu','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_1496489_HEJUWZ_V','OFFER_CODE',682,'','','','','','DV003828','VARCHAR2','','','Offer Code','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_1496489_HEJUWZ_V','PAY_TO_VENDOR_CODE',682,'','','','','','DV003828','VARCHAR2','','','Pay To Vendor Code','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_1496489_HEJUWZ_V','BU_BRANCH_CODE',682,'','','','','','DV003828','VARCHAR2','','','Bu Branch Code','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_1496489_HEJUWZ_V','LOCATION_SEGMENT',682,'','','','','','DV003828','VARCHAR2','','','Location Segment','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_1496489_HEJUWZ_V','PRODUCT',682,'','','','','','DV003828','VARCHAR2','','','Product','','','','US','');
--Inserting Object Components for XXEIS_1496489_HEJUWZ_V
--Inserting Object Component Joins for XXEIS_1496489_HEJUWZ_V
ELSE
RAISE_APPLICATION_ERROR (-20001,'Module does not exist. Import/Create the module with id 682');
END IF;
END;
/
prompt Creating Report LOV Data for Deduction Statments PO Report
set scan off define off
DECLARE
mod_exist VARCHAR2(1);
BEGIN 
mod_exist := XXEIS.EIS_RSC_INSERT.check_if_module_exist(682);
IF mod_exist = 'Y' THEN 
--Inserting Report LOVs - Deduction Statments PO Report
xxeis.eis_rsc_ins.lov( 682,'SELECT DISTINCT GPS.PERIOD_YEAR
FROM gl_period_statuses gps
ORDER BY GPS.PERIOD_YEAR','','XXWC OZF Calendar Year LOV','','SA059956',NULL,'N','','','N','N','','','US');
xxeis.eis_rsc_ins.lov( 682,'select distinct party_name
from ar.HZ_PARTIES
where ATTRIBUTE1 = ''HDS_MVID''','','LOV VENDOR_NAME','VENDOR NAME FROM ar.HZ_PARTIES
','ID020048',NULL,'N','','','','','','','US');
xxeis.eis_rsc_ins.lov( 682,'select distinct name
from ozf.OZF_TIME_ENT_PERIOD','','LOV PERIOD','LOV NAME FROM ozf.OZF_TIME_ENT_PERIOD is the Period','ANONYMOUS',NULL,'N','','','','','','','US');
xxeis.eis_rsc_ins.lov( 682,'select distinct description from apps.qp_list_headers_vl','','HDS AGREEMENT_NAME','select distinct description from apps.qp_list_headers_vl','DH052844',NULL,'N','','','','','','','US');
null;
ELSE
RAISE_APPLICATION_ERROR (-20001,'Module does not exist. Import/Create the module with id 682');
END IF;
END;
/
prompt Creating Report Data for Deduction Statments PO Report
set scan off define off
DECLARE
mod_exist varchar2(1);
BEGIN 
mod_exist := XXEIS.EIS_RSC_INSERT.check_if_module_exist(682);
IF mod_exist = 'Y' THEN 
--Deleting Report data - Deduction Statments PO Report
xxeis.eis_rsc_utility.delete_report_rows( 'Deduction Statments PO Report',682 );
--Inserting Report - Deduction Statments PO Report
xxeis.eis_rsc_ins.r( 682,'Deduction Statments PO Report','','YTD deduction statements that show deductions taken and the purchase bases of the deductions.  Used to send as backup to vendors','','','','KP059700','XXEIS_1496489_HEJUWZ_V','Y','','SELECT /*+ INDEX(C XXCUS_REBATE_CUSTOMERS_N1) INDEX(qlhv QP_LIST_HEADERS_TL_PK) INDEX(oo OZF_OFFERS_U3)*/
  B.OFU_MVID MVID,
  c.party_name VENDOR,
  B.BILL_TO_PARTY_NAME_LOB LOB,
  b.resale_line_attribute2 BU,
  B.OFFER_CODE,
  QLHV.DESCRIPTION OFFER_NAME,
  OO.STATUS_CODE,
  decode(b.activity_media_name,''Coop'',''COOP'',''Coop Minimum'',''COOP'',''REBATE'') Rebate_Type,
  po_number PO_NUMBER,
  B.RESALE_LINE_ATTRIBUTE1 PAY_TO_VENDOR_CODE,
  B.RESALE_LINE_ATTRIBUTE6 BU_BRANCH_CODE,
  B.RESALE_LINE_ATTRIBUTE12 LOCATION_SEGMENT,
  B.RESALE_LINE_ATTRIBUTE11 PRODUCT,
  B.DATE_ORDERED,
  B.RESALE_DATE_CREATED,
  B.OFU_GL_DATE OFU_GL_DATE,
  --  to_char(b.ofu_gl_date,''Mon-YYYY'') gl_calendar_month, -- Commented for TMS-20170726-00092
  b.xaeh_period_name gl_calendar_month,  -- Added for TMS-20170726-00092
  b.calendar_year agreement_year,
  SUM(CASE WHEN UTILIZATION_TYPE = ''ACCRUAL'' THEN SELLING_PRICE*QUANTITY ELSE 0 end) TOTAL_RECEIPTS,
  SUM(UTIL_ACCTD_AMOUNT) REBATES
FROM
  XXCUS.XXCUS_OZF_XLA_ACCRUALS_B B,
  XXCUS.XXCUS_REBATE_CUSTOMERS C,
  ozf.ozf_offers oo,
  APPS.QP_LIST_HEADERS_TL qlhv  -- Changed for TMS-20170726-00092
WHERE 1=1
AND C.CUSTOMER_ID=B.OFU_CUST_ACCOUNT_ID
AND QLHV.LIST_HEADER_ID=B.QP_LIST_HEADER_ID 
and qlhv.LANGUAGE = userenv(''LANG'')  -- Added for TMS-20170726-00092
AND OO.OFFER_CODE =  B.OFFER_CODE
GROUP BY
    B.OFU_MVID,
  c.party_name ,
  B.BILL_TO_PARTY_NAME_LOB ,
  b.resale_line_attribute2,
  B.OFFER_CODE,
  QLHV.DESCRIPTION ,
  OO.STATUS_CODE,
  decode(b.activity_media_name,''Coop'',''COOP'',''Coop Minimum'',''COOP'',''REBATE'') ,
  PO_NUMBER ,
  B.RESALE_LINE_ATTRIBUTE1,
  B.RESALE_LINE_ATTRIBUTE6 ,
  B.RESALE_LINE_ATTRIBUTE12 ,
  B.RESALE_LINE_ATTRIBUTE11 ,
  B.DATE_ORDERED,
  B.RESALE_DATE_CREATED,
  B.OFU_GL_DATE ,
  --  TO_CHAR(B.OFU_GL_DATE,''Mon-YYYY''),  -- Commented for TMS-20170726-00092
  b.xaeh_period_name,  -- Added for TMS-20170726-00092
  B.CALENDAR_YEAR
','KP059700','','N','PAYMENTS ','','CSV,Pivot Excel,EXCEL,','','','','','','','N','APPS','US','','','','','','','','','','','','','','','','');
--Inserting Report Columns - Deduction Statments PO Report
xxeis.eis_rsc_ins.rc( 'Deduction Statments PO Report',682,'DATE_ORDERED','Date Ordered','','','','default','','9','N','','','','','','','','KP059700','N','N','','XXEIS_1496489_HEJUWZ_V','','','GROUP_BY','US','','');
xxeis.eis_rsc_ins.rc( 'Deduction Statments PO Report',682,'GL_CALENDAR_MONTH','GL Calendar Month','','','','default','','12','N','','','','','','','','KP059700','N','N','','XXEIS_1496489_HEJUWZ_V','','','GROUP_BY','US','','');
xxeis.eis_rsc_ins.rc( 'Deduction Statments PO Report',682,'OFFER_NAME','Offer Name','','','','default','','5','N','','','','','','','','KP059700','N','N','','XXEIS_1496489_HEJUWZ_V','','','GROUP_BY','US','','');
xxeis.eis_rsc_ins.rc( 'Deduction Statments PO Report',682,'MVID','MVID','','','','default','','1','N','','','','','','','','KP059700','N','N','','XXEIS_1496489_HEJUWZ_V','','','GROUP_BY','US','','');
xxeis.eis_rsc_ins.rc( 'Deduction Statments PO Report',682,'VENDOR','Vendor','','','','default','','2','N','','','','','','','','KP059700','N','N','','XXEIS_1496489_HEJUWZ_V','','','GROUP_BY','US','','');
xxeis.eis_rsc_ins.rc( 'Deduction Statments PO Report',682,'LOB','LOB','','','','default','','3','N','','','','','','','','KP059700','N','N','','XXEIS_1496489_HEJUWZ_V','','','GROUP_BY','US','','');
xxeis.eis_rsc_ins.rc( 'Deduction Statments PO Report',682,'STATUS_CODE','Status Code','','','','default','','6','N','','','','','','','','KP059700','N','N','','XXEIS_1496489_HEJUWZ_V','','','GROUP_BY','US','','');
xxeis.eis_rsc_ins.rc( 'Deduction Statments PO Report',682,'PO_NUMBER','PO Number','','','','default','','8','N','','','','','','','','KP059700','N','N','','XXEIS_1496489_HEJUWZ_V','','','GROUP_BY','US','','');
xxeis.eis_rsc_ins.rc( 'Deduction Statments PO Report',682,'TOTAL_RECEIPTS','Total Receipts','','','~,~.~2','default','','13','N','','','','','','','','KP059700','N','N','','XXEIS_1496489_HEJUWZ_V','','','SUM','US','','');
xxeis.eis_rsc_ins.rc( 'Deduction Statments PO Report',682,'REBATE_TYPE','Rebate Type','','','','default','','7','N','','','','','','','','KP059700','N','N','','XXEIS_1496489_HEJUWZ_V','','','GROUP_BY','US','','');
xxeis.eis_rsc_ins.rc( 'Deduction Statments PO Report',682,'REBATES','Rebates','','','~,~.~2','default','','14','N','','','','','','','','KP059700','N','N','','XXEIS_1496489_HEJUWZ_V','','','SUM','US','','');
xxeis.eis_rsc_ins.rc( 'Deduction Statments PO Report',682,'OFU_GL_DATE','GL Date','','','','default','','10','N','','','','','','','','KP059700','N','N','','XXEIS_1496489_HEJUWZ_V','','','GROUP_BY','US','','');
xxeis.eis_rsc_ins.rc( 'Deduction Statments PO Report',682,'BU','BU','','','','default','','4','N','','','','','','','','KP059700','N','N','','XXEIS_1496489_HEJUWZ_V','','','GROUP_BY','US','','');
xxeis.eis_rsc_ins.rc( 'Deduction Statments PO Report',682,'RESALE_DATE_CREATED','Resale Date Created','','','','default','','11','N','','','','','','','','KP059700','N','N','','XXEIS_1496489_HEJUWZ_V','','','GROUP_BY','US','','');
--Inserting Report Parameters - Deduction Statments PO Report
xxeis.eis_rsc_ins.rp( 'Deduction Statments PO Report',682,'Gl Date From','','OFU_GL_DATE','>=','','','DATE','N','Y','1','Y','Y','CONSTANT','KP059700','Y','N','','Start Date','','XXEIS_1496489_HEJUWZ_V','','','US','');
xxeis.eis_rsc_ins.rp( 'Deduction Statments PO Report',682,'Vendor','','VENDOR','IN','LOV VENDOR_NAME','','VARCHAR2','Y','Y','3','Y','Y','CONSTANT','KP059700','Y','N','','','','XXEIS_1496489_HEJUWZ_V','','','US','');
xxeis.eis_rsc_ins.rp( 'Deduction Statments PO Report',682,'GL Date To','','OFU_GL_DATE','<=','','','DATE','N','Y','2','Y','Y','CONSTANT','KP059700','Y','N','','End Date','','XXEIS_1496489_HEJUWZ_V','','','US','');
xxeis.eis_rsc_ins.rp( 'Deduction Statments PO Report',682,'Agreement Year','','AGREEMENT_YEAR','IN','XXWC OZF Calendar Year LOV','','VARCHAR2','Y','Y','4','Y','Y','CONSTANT','KP059700','Y','N','','','','XXEIS_1496489_HEJUWZ_V','','','US','');
xxeis.eis_rsc_ins.rp( 'Deduction Statments PO Report',682,'Gl Calendar Month','','GL_CALENDAR_MONTH','IN','LOV PERIOD','','VARCHAR2','Y','Y','5','Y','Y','CONSTANT','KP059700','Y','N','','','','XXEIS_1496489_HEJUWZ_V','','','US','');
xxeis.eis_rsc_ins.rp( 'Deduction Statments PO Report',682,'Offer Name','','OFFER_NAME','IN','HDS AGREEMENT_NAME','','VARCHAR2','N','Y','6','Y','Y','CONSTANT','KP059700','Y','N','','','','XXEIS_1496489_HEJUWZ_V','','','US','');
--Inserting Dependent Parameters - Deduction Statments PO Report
--Inserting Report Conditions - Deduction Statments PO Report
xxeis.eis_rsc_ins.rcnh( 'Deduction Statments PO Report',682,'X1HV1.OFU_GL_DATE >= Gl Date From','SIMPLE','','','Y','','1');
xxeis.eis_rsc_ins.rcnd( '','','OFU_GL_DATE','','Gl Date From','','','','','XXEIS_1496489_HEJUWZ_V','','','','','','GREATER_THAN_EQUALS','Y','Y','','','','','1',682,'Deduction Statments PO Report','X1HV1.OFU_GL_DATE >= Gl Date From');
xxeis.eis_rsc_ins.rcnh( 'Deduction Statments PO Report',682,'X1HV1.OFU_GL_DATE <= GL Date To','SIMPLE','','','Y','','2');
xxeis.eis_rsc_ins.rcnd( '','','OFU_GL_DATE','','GL Date To','','','','','XXEIS_1496489_HEJUWZ_V','','','','','','LESS_THAN_EQUALS','Y','Y','','','','','1',682,'Deduction Statments PO Report','X1HV1.OFU_GL_DATE <= GL Date To');
xxeis.eis_rsc_ins.rcnh( 'Deduction Statments PO Report',682,'X1HV1.VENDOR IN Vendor','SIMPLE','','','Y','','3');
xxeis.eis_rsc_ins.rcnd( '','','VENDOR','','Vendor','','','','','XXEIS_1496489_HEJUWZ_V','','','','','','IN','Y','Y','','','','','1',682,'Deduction Statments PO Report','X1HV1.VENDOR IN Vendor');
xxeis.eis_rsc_ins.rcnh( 'Deduction Statments PO Report',682,'X1HV1.AGREEMENT_YEAR IN Agreement Year','SIMPLE','','','Y','','4');
xxeis.eis_rsc_ins.rcnd( '','','AGREEMENT_YEAR','','Agreement Year','','','','','XXEIS_1496489_HEJUWZ_V','','','','','','IN','Y','Y','','','','','1',682,'Deduction Statments PO Report','X1HV1.AGREEMENT_YEAR IN Agreement Year');
xxeis.eis_rsc_ins.rcnh( 'Deduction Statments PO Report',682,'X1HV1.GL_CALENDAR_MONTH IN Gl Calendar Month','SIMPLE','','','Y','','5');
xxeis.eis_rsc_ins.rcnd( '','','GL_CALENDAR_MONTH','','Gl Calendar Month','','','','','XXEIS_1496489_HEJUWZ_V','','','','','','IN','Y','Y','','','','','1',682,'Deduction Statments PO Report','X1HV1.GL_CALENDAR_MONTH IN Gl Calendar Month');
xxeis.eis_rsc_ins.rcnh( 'Deduction Statments PO Report',682,'X1HV1.OFFER_NAME IN Offer Name','SIMPLE','','','Y','','6');
xxeis.eis_rsc_ins.rcnd( '','','OFFER_NAME','','Offer Name','','','','','XXEIS_1496489_HEJUWZ_V','','','','','','IN','Y','Y','','','','','1',682,'Deduction Statments PO Report','X1HV1.OFFER_NAME IN Offer Name');
--Inserting Report Sorts - Deduction Statments PO Report
xxeis.eis_rsc_ins.rs( 'Deduction Statments PO Report',682,'MVID','ASC','KP059700','1','');
xxeis.eis_rsc_ins.rs( 'Deduction Statments PO Report',682,'LOB','ASC','KP059700','2','');
xxeis.eis_rsc_ins.rs( 'Deduction Statments PO Report',682,'OFFER_NAME','ASC','KP059700','3','');
xxeis.eis_rsc_ins.rs( 'Deduction Statments PO Report',682,'OFU_GL_DATE','ASC','KP059700','4','');
--Inserting Report Triggers - Deduction Statments PO Report
--inserting report templates - Deduction Statments PO Report
--Inserting Report Portals - Deduction Statments PO Report
--inserting report dashboards - Deduction Statments PO Report
--Exporting report views table information -  
xxeis.eis_rsc_ins.rviews( 'Deduction Statments PO Report','682','XXEIS_1496489_HEJUWZ_V','XXEIS_1496489_HEJUWZ_V','N','');
--inserting report security - Deduction Statments PO Report
xxeis.eis_rsc_ins.rsec( 'Deduction Statments PO Report','682','','XXCUS_TM_USER',682,'KP059700','','','');
xxeis.eis_rsc_ins.rsec( 'Deduction Statments PO Report','682','','XXCUS_TM_ADMIN_USER',682,'KP059700','','','');
xxeis.eis_rsc_ins.rsec( 'Deduction Statments PO Report','682','','XXCUS_TM_FORMS_RESP',682,'KP059700','','','');
xxeis.eis_rsc_ins.rsec( 'Deduction Statments PO Report','682','','XXCUS_TM_ADM_FORMS_RESP',682,'KP059700','','','');
xxeis.eis_rsc_ins.rsec( 'Deduction Statments PO Report','20005','','XXWC_VIEW_ALL_EIS_REPORTS',682,'KP059700','','','');
--Inserting Report Pivots - Deduction Statments PO Report
--Inserting Report Distribution Details 
--Inserting Report Distribution Details 
--Inserting Report SubTotals Details 
--Inserting Report aggregations 
--Inserting Report   Version details- Deduction Statments PO Report
xxeis.eis_rsc_ins.rv( 'Deduction Statments PO Report','','Deduction Statments PO Report','SA059956','20-SEP-2017');
ELSE
RAISE_APPLICATION_ERROR (-20001,'Module does not exist. Import/Create the module with id 682');
END IF;
END;
/
