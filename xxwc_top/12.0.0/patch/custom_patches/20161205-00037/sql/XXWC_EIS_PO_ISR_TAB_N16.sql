/******************************************************************************************
    $Header XXWC_EIS_PO_ISR_TAB_N16.sql $
    Module Name: XXWC_EIS_PO_ISR_TAB_N16.sql

    PURPOSE:   Index creation script for performance improvement

    REVISIONS:
    Ver        Date        Author                     Description
    ---------  ----------  ---------------         -----------------------------------------
    1.0        08-Dec-2016 Pattabhi Avula           Initial Version - TMS#20161205-00037 - 
	                                                Performance Improvement for
	                                                XXWC QP Webber Product Catalog process  
*********************************************************************************************/
CREATE INDEX XXEIS.XXWC_EIS_PO_ISR_TAB_N16 ON XXEIS.EIS_XXWC_PO_ISR_TAB(ITEM_NUMBER);
/