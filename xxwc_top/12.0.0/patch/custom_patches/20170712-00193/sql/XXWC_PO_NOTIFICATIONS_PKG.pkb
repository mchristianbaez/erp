CREATE OR REPLACE
PACKAGE BODY      XXWC_PO_NOTIFICATIONS_PKG
AS
/*************************************************************************
      $Header XXWC_PO_NOTIFICATIONS_PKG.PKG $
      Module Name: XXWC_PO_NOTIFICATIONS_PKG.PKG

      PURPOSE:   This package is used for sending the mail for unapproved PO's
                 and not received PO's

      REVISIONS:
      Ver        Date        Author                     Description
      ---------  ----------  ---------------         -------------------------
      1.0        05/05/2016  Pattabhi Avula          Initial Version TMS#20150901-00143
	  1.1        09/19/2016  Pattabhi Avula          TMS#20160811-00004 -- Added Creation
	                                                 date parameter and added org_id 
													 condition
	  1.2        10/12/2017  Niraj K Ranjan          TMS#20170712-00193   Late PO notification
  ****************************************************************************/
  
      --------------------------------------------------------------
      -- Global Email Variables
      --------------------------------------------------------------
      g_host                     VARCHAR2 (256)
                                    := 'mailoutrelay.hdsupply.net';
      g_hostport                 VARCHAR2 (20) := '25';
      g_sender                   VARCHAR2 (100) := 'no-reply@whitecap.net';  
            
      --Email Defaults
     g_dflt_email   fnd_user.email_address%TYPE    := 'WC-ITDEVALERTS-U1@HDSupply.com';
	 
  /*************************************************************************
      PROCEDURE Name: unapproved_pos

      PURPOSE:   To send the mail notification to buyers for older than 3 
                 days created PO's

      REVISIONS:
      Ver        Date        Author                     Description
      ---------  ----------  ---------------         -------------------------
      1.0        05/05/2015  Pattabhi Avula          Initial Version
   ****************************************************************************/
   
   PROCEDURE INSERT_PO_NTF(p_porec XXWC_PO_NOTIFICATION_TBL%ROWTYPE,
                           retmsg  OUT VARCHAR2)
   IS
   BEGIN
      retmsg := NULL;
	  INSERT INTO XXWC_PO_NOTIFICATION_TBL
	  VALUES p_porec;
   EXCEPTION
      WHEN OTHERS THEN
         retmsg := substr(sqlerrm,1,2000);
   END;
      
  /*************************************************************************
      PROCEDURE Name: unapproved_pos

      PURPOSE:   To send the mail notification to buyers for older than 3 
                 days created PO's

      REVISIONS:
      Ver        Date        Author                     Description
      ---------  ----------  ---------------         -------------------------
      1.0        05/05/2015  Pattabhi Avula          Initial Version
	  1.2        10/12/2017  Niraj K Ranjan          TMS#20170712-00193   Late PO notification
   ****************************************************************************/
   
   PROCEDURE unapproved_pos(errbuf OUT VARCHAR2, retcode OUT NUMBER, p_po_num VARCHAR2)
   IS
   
      l_subject                  VARCHAR2 (32767) DEFAULT NULL;
      l_body                     VARCHAR2 (32767) DEFAULT NULL;
      l_body_header              VARCHAR2 (32767) DEFAULT NULL;
      l_body_detail              VARCHAR2 (32767) DEFAULT NULL;
      l_body_footer              VARCHAR2 (32767) DEFAULT NULL;
      l_sec                      VARCHAR2(300);
	  l_count                    NUMBER(10):=0;
	  l_total_rec_fetched        NUMBER(10):=0;
	  l_url                      VARCHAR2(200);
	  lr_porec                   XXWC.XXWC_PO_NOTIFICATION_TBL%ROWTYPE; --ver 1.2
	  l_retmsg                   VARCHAR2(2000); --ver 1.2
	  le_exception               EXCEPTION; --VER 1.2
	  l_user_id                  NUMBER := fnd_global.user_id; --Ver 1.2
	  l_email_sent_date          XXWC.XXWC_PO_NOTIFICATION_TBL.EMAIL_SENT_DATE%TYPE;
      
    CURSOR c1
      IS
       SELECT pha.segment1,
	          pha.po_header_id,
              pha.type_lookup_code,
              aps.vendor_name,
              NVL (pha.authorization_status, 'incomplete') status,
              papf.email_address
       FROM po_headers_all pha,
            ap_suppliers aps,
            per_all_people_f papf
       WHERE pha.vendor_id=aps.vendor_id
       AND pha.agent_id=papf.person_id
	   AND TRUNC(SYSDATE) BETWEEN NVL(papf.EFFECTIVE_START_DATE, TRUNC(SYSDATE)) AND NVL(papf.EFFECTIVE_END_DATE, TRUNC(SYSDATE))
       AND NVL(pha.authorization_status, 'INCOMPLETE') IN ('INCOMPLETE',
                                                          'REJECTED',
                                                          'REQUIRES REAPPROVAL',
                                                          'IN PROCESS')
       AND pha.type_lookup_code ='STANDARD'
       AND NVL (pha.cancel_flag, 'N') = 'N'
       AND NVL (pha.closed_code, 'OPEN') NOT IN ('FINALLY CLOSED', 'CLOSED')
       AND pha.org_id=162
       AND pha.segment1=NVL(p_po_num,pha.segment1)
       AND TRIM(TO_CHAR(SYSDATE, 'DAY'))         NOT IN ('SATURDAY','SUNDAY')
       AND TRUNC(pha.creation_date)<=(TRUNC(SYSDATE) - (SELECT profile_option_value 
                                                        FROM   fnd_profile_option_values fpov,
                                                               fnd_profile_options fpo
                                                        WHERE  fpov.profile_option_id=fpo.profile_option_id
                                                        AND    fpo.profile_option_name='XXWC_UNAPPROVED_PO_PAST_DAYS'));
 
 BEGIN
	   /*l_sec:='Before Loop started';
	   BEGIN
		SELECT home_url 
		INTO   l_url
		FROM   icx_parameters;
	  EXCEPTION
		WHEN NO_DATA_FOUND THEN
		l_sec:='Failed in get the URL';
		l_url:=NULL;
		WHEN others THEN
		 l_sec:=SUBSTR(SQLERRM,1,50);
		 l_url:=NULL;
	  END;*/
   l_sec:='Loop started';
   fnd_file.put_line(FND_FILE.log,l_sec);
   FOR i IN c1 
   LOOP
      BEGIN
	     --Ver 1.2 added this begin-end
	      l_sec := 'Fetch EMAIL_SENT_DATE';
		  l_total_rec_fetched := l_total_rec_fetched + 1;
	      BEGIN
	        SELECT MAX(EMAIL_SENT_DATE)
		    INTO l_email_sent_date
		    FROM XXWC_PO_NOTIFICATION_TBL
		    WHERE PO_HEADER_ID = i.po_header_id;
		  EXCEPTION
		     WHEN OTHERS THEN
			   l_email_sent_date := NULL;
		  END;
		  --Ver 1.2 Added this if condition
		  --email should be send first time
          --Reminder email should be sent only on Monday once		  
		  IF    (l_email_sent_date IS NULL)
		     OR (l_email_sent_date IS NOT NULL              AND 
			     TRUNC(l_email_sent_date) <> TRUNC(SYSDATE) AND
		         TRIM(TO_CHAR(SYSDATE, 'DAY')) = 'MONDAY')
			  
          THEN
             l_sec := 'Send Email Notification';
                        ------------------------------------------
                        -- Send Email Notification
                        ------------------------------------------
                        l_subject :=
                           ' PO UNAPPROVED, PO# '|| i.segment1
                                                 || ' '
                                                 || i.vendor_name
                                                 ||' Status '
                                                 || i.status ;
                                                 --||' past 3 days '; --commented Ver 1.2
                        l_body_header :=
                           '<style type="text/css">.style1 {font-family: Arial, Helvetica, sans-serif;}</style><p class="style1">';
                        l_body_detail :=
                              ' <BR> Purchase Order '
                           || i.segment1
                           || ' is unapproved. '
                           || CHR (10)
                           || ' Please approve the PO or cancel/delete. </BR>';

                        l_body_footer := NULL;
                           
                        l_body :=
                              l_body_header
                           || l_body_detail
                           || CHR (10)
                           || l_body_footer;

                        xxcus_misc_pkg.html_email (
                           p_to              => i.email_address,
                           p_from            => g_sender,
                           p_text            => 'UnApproved PO',
                           p_subject         => l_subject,
                           p_html            => l_body,
                           p_smtp_hostname   => g_host,
                           p_smtp_portnum    => g_hostport);
						--Ver 1.2
						lr_porec.PO_HEADER_ID       := i.po_header_id ;
                        lr_porec.EMAIL_SENT_DATE    := SYSDATE        ;
                        lr_porec.TO_EMAIL_ADDRESS   := i.email_address;
                        lr_porec.CREATION_DATE      := SYSDATE        ;
                        lr_porec.CREATED_BY         := l_user_id      ;
                        lr_porec.LAST_UPDATE_DATE   := SYSDATE        ;
                        lr_porec.LAST_UPDATE_BY     := l_user_id      ;
                        lr_porec.TYPE               := 'UNAPPROVED_PO';
						insert_po_ntf(p_porec => lr_porec,
                                      retmsg  => l_retmsg);
                        IF l_retmsg IS NOT NULL THEN
						   RAISE le_exception;
						END IF;
	         l_count:=l_count+1;
	         COMMIT;
	      END IF;
      --Ver 1.2 Exception added	  
      EXCEPTION
	     WHEN le_exception THEN
		    l_sec := 'Error for PO#'||i.segment1||'=>'||l_retmsg;
		    fnd_file.put_line(FND_FILE.log,l_sec);
			ROLLBACK;
	     WHEN OTHERS THEN
	        l_sec :=' Sending the mail to buyer for Unapproved_pos, PO#: '||i.segment1||' is failed';
		    fnd_file.put_line(FND_FILE.log,l_sec);
			ROLLBACK;
	  END;
   END LOOP;
   l_sec:='Loop Ended';
   fnd_file.put_line(FND_FILE.log,l_sec);
   fnd_file.put_line(FND_FILE.log,'Total records fetched is :'||l_total_rec_fetched);
   fnd_file.put_line(FND_FILE.log,'Total count of mails sent is :'||l_count);
 EXCEPTION  
 WHEN OTHERS
      THEN
        xxcus_error_pkg.xxcus_error_main_api (
          p_called_from         => 'XXWC_PO_NOTIFICATIONS_PKG.unapproved_pos',
          p_calling             => l_sec,
          p_request_id          => fnd_global.conc_request_id,
          p_ora_error_msg       => SQLERRM,
          p_error_desc          => 'Error in unapproved purchase order procedure, When Others Exception',
          p_distribution_list   => g_dflt_email,
          p_module              => 'PO');
END;

/*************************************************************************
      PROCEDURE Name: approved_not_received

      PURPOSE:   To send the mail notification to buyes for Approved but not 
                 received PO's

      REVISIONS:
      Ver        Date        Author                     Description
      ---------  ----------  ---------------         -------------------------
      1.0        05/05/2015  Pattabhi Avula          Initial Version
	  1.1        09/19/2016  Pattabhi Avula          TMS#20160811-00004 -- Added Creation
	                                                 date parameter and added org_id 
													 condition
      1.2        10/12/2017  Niraj K Ranjan          TMS#20170712-00193 Late PO      notification
   ****************************************************************************/
   --  PROCEDURE approved_and_not_received_pos(errbuf OUT VARCHAR2, retcode OUT NUMBER, p_po_num VARCHAR2) -- Vern#1.1
   PROCEDURE approved_and_not_received_pos(errbuf OUT VARCHAR2, retcode OUT NUMBER, p_po_num VARCHAR2, p_po_creation_date VARCHAR2)  -- Added p_po_creation_date parameter for Ver#1.1
   IS
      
      l_subject                  VARCHAR2 (32767) DEFAULT NULL;
      l_body                     VARCHAR2 (32767) DEFAULT NULL;
      l_body_header              VARCHAR2 (32767) DEFAULT NULL;
      l_body_detail              VARCHAR2 (32767) DEFAULT NULL;
      l_body_footer              VARCHAR2 (32767) DEFAULT NULL;
      l_sec                      VARCHAR2(300);
	  l_count                    NUMBER(10):=0;
	  l_total_rec_fetched        NUMBER(10):=0;
	  l_url                      VARCHAR2(200);
	  ld_from_date               DATE := FND_DATE.CANONICAL_TO_DATE(p_po_creation_date);
	  lr_porec                   XXWC.XXWC_PO_NOTIFICATION_TBL%ROWTYPE; --ver 1.2
	  l_retmsg                   VARCHAR2(2000); --ver 1.2
	  le_exception               EXCEPTION; --VER 1.2
	  l_email_sent_date          XXWC.XXWC_PO_NOTIFICATION_TBL.EMAIL_SENT_DATE%TYPE;
	  l_user_id                  NUMBER := fnd_global.user_id; --Ver 1.2
	  -- Vern# 1.1
    --Ver 1.2 select query of below cursor has changed  
    CURSOR c1
      IS
       SELECT poh.Segment1,
	          poh.po_header_id,
              min(TRUNC(NVL(poll.promised_date,TO_DATE(NVL(poh.attribute6,poh.attribute1),'YYYY/MM/DD HH24:MI:SS'))))  due_date,
              pv.vendor_name,
              hre.email_address
         FROM po_headers poh ,
             PO_LINES pol ,
             PO_LINE_LOCATIONS poll ,
             ap_suppliers pv ,
             PER_ALL_PEOPLE_F hre
           WHERE 1                                 =1
           AND POH.PO_HEADER_ID                    = POL.PO_HEADER_ID
           AND POL.PO_LINE_ID                      = POLL.PO_LINE_ID(+)
           AND poh.type_lookup_code               IN ('STANDARD') --, 'BLANKET', 'PLANNED')
           AND poh.authorization_status            ='APPROVED'
           AND NVL (POH.CLOSED_CODE, 'OPEN')  NOT IN ('FINALLY CLOSED', 'CLOSED')
           AND NVL (POH.CANCEL_FLAG, 'N')          = 'N'
           AND NVL (poll.closed_code, 'OPEN') NOT IN ('FINALLY CLOSED','CLOSED FOR RECEIVING','CLOSED')
           AND (NVL(poll.quantity,0)+NVL(poll.quantity_cancelled,0))-NVL(poll.quantity_received,0)>0
           AND NVL (pol.closed_code, 'OPEN')  NOT IN ('CLOSED', 'FINALLY CLOSED')
           AND POLL.SHIPMENT_TYPE                 IN ('STANDARD') --, 'BLANKET', 'SCHEDULED')
           AND NVL (POLL.CANCEL_FLAG, 'N')         = 'N'
           AND NVL (pol.cancel_flag, 'N')          = 'N'
           AND POH.VENDOR_ID                       = PV.VENDOR_ID
           AND poh.agent_id                        = hre.person_id
           AND TRUNC(SYSDATE) BETWEEN NVL(hre.EFFECTIVE_START_DATE, TRUNC(SYSDATE)) AND NVL(hre.EFFECTIVE_END_DATE, TRUNC(SYSDATE))
           AND    TRUNC(poh.creation_date)>=NVL(TRUNC(ld_from_date), TRUNC(poh.creation_date))
           AND    poh.segment1=NVL(p_po_num,poh.segment1)
		   AND TRIM(TO_CHAR(SYSDATE, 'DAY'))         NOT IN ('SATURDAY','SUNDAY')
           AND TRUNC(NVL(poll.promised_date,TO_DATE(NVL(poh.attribute6,poh.attribute1),'YYYY/MM/DD HH24:MI:SS'))) <> TO_DATE('12-DEC-2012','DD-MON-YYYY')
           AND TRUNC(NVL(poll.promised_date,TO_DATE(NVL(poh.attribute6,poh.attribute1),'YYYY/MM/DD HH24:MI:SS')))    
           <= (TRUNC(SYSDATE) - (SELECT  profile_option_value
                                             FROM fnd_profile_option_values fpov,
                                                  fnd_profile_options fpo
                                             WHERE   fpov.profile_option_id=fpo.profile_option_id
                                             AND    fpo.profile_option_name='XXWC_APPROVED_AND_NOT_RECD_PO_PAST_DAYS'))
         GROUP BY poh.Segment1,poh.po_header_id,pv.vendor_name,hre.email_address;
 
 BEGIN
   --Comented for Ver 1.2 because link is not required in email anymore
  /*l_sec:='Fetch application url';
  fnd_file.put_line(FND_FILE.log,l_sec);
  BEGIN
    SELECT home_url 
	INTO   l_url
	FROM   icx_parameters;
  EXCEPTION
    WHEN NO_DATA_FOUND THEN
	l_sec:='Failed in get the URL';
	l_url:=NULL;
	fnd_file.put_line(FND_FILE.log,l_sec);
	WHEN others THEN
	 l_sec:=SUBSTR(SQLERRM,1,50);
	 l_url:=NULL;
	 fnd_file.put_line(FND_FILE.log,l_sec);
  END;*/
   l_sec:='Loop started';
   fnd_file.put_line(FND_FILE.log,l_sec);
   FOR i IN c1 
   LOOP
      BEGIN
	      --Ver 1.2 added this begin-end
		  l_total_rec_fetched := l_total_rec_fetched + 1;
	      l_sec := 'Fetch EMAIL_SENT_DATE';
	      BEGIN
	        SELECT MAX(EMAIL_SENT_DATE)
		    INTO l_email_sent_date
		    FROM XXWC_PO_NOTIFICATION_TBL
		    WHERE PO_HEADER_ID = i.po_header_id;
		  EXCEPTION
		     WHEN OTHERS THEN
			   l_email_sent_date := NULL;
		  END;
		  --Ver 1.2 Added this if condition
		  --email should be send first time
          --Reminder email should be sent only on Monday once				   
		  IF    (l_email_sent_date IS NULL)
		     OR (l_email_sent_date IS NOT NULL              AND 
			     TRUNC(l_email_sent_date) <> TRUNC(SYSDATE) AND
		         TRIM(TO_CHAR(SYSDATE, 'DAY')) = 'MONDAY')
			  
          THEN
             l_sec := 'Send Email Notification';
                        ------------------------------------------
                        -- Send Email Notification
                        ------------------------------------------
                        l_subject :=
                           ' PO NOT RECEIVED, PO# '|| i.segment1
                                                 || '. '
                                                 || i.vendor_name
                                                 ||', Promise date '
                                                 || i.due_date; --ver 1.2
                                                 
                        l_body_header :=
                           '<style type="text/css">.style1 {font-family: Arial, Helvetica, sans-serif;}</style><p class="style1">';
						--Ver 1.2 changed the body text
                        l_body_detail :=
                              ' <BR> Purchase Order# '
                           || i.segment1
                           || ' is over 7 days late with a due date of '
                           || to_char(i.due_date,'DD-MON-YYYY')||'.'
                           || CHR (10)
                           ||'</BR> If the quantity due is valid contact the supplier for a ship date and update the promise date which must include in-transit and on-dock time.'
                           || CHR (10)
                           ||' </BR> If there is a receiving discrepancy or APIN resolve immediately! DO NOT PUSH OUT THE DUE DATE! '
						   || CHR (10)
                           || ' </BR> Accurate PO due dates are critical to the business - manage them daily and call the supplier before POs are late.  </BR>';
                        l_body_footer := NULL;
                           
                        l_body :=
                              l_body_header
                           || l_body_detail
                           || CHR (10)
                           || l_body_footer;

                        xxcus_misc_pkg.html_email (
                           p_to              => i.email_address,
                           p_from            => g_sender,
                           p_text            => 'Approved but not received PO',
                           p_subject         => l_subject,
                           p_html            => l_body,
                           p_smtp_hostname   => g_host,
                           p_smtp_portnum    => g_hostport);
						--Ver 1.2
						lr_porec.PO_HEADER_ID       := i.po_header_id ;
                        lr_porec.EMAIL_SENT_DATE    := SYSDATE        ;
                        lr_porec.TO_EMAIL_ADDRESS   := i.email_address;
                        lr_porec.CREATION_DATE      := SYSDATE        ;
                        lr_porec.CREATED_BY         := l_user_id      ;
                        lr_porec.LAST_UPDATE_DATE   := SYSDATE        ;
                        lr_porec.LAST_UPDATE_BY     := l_user_id      ;
                        lr_porec.TYPE               := 'LATE_PO'      ;
						insert_po_ntf(p_porec => lr_porec,
                                      retmsg  => l_retmsg);
                        IF l_retmsg IS NOT NULL THEN
						   RAISE le_exception;
						END IF;
	         l_count:=l_count+1;
	         COMMIT;
	      END IF;
      --Ver 1.2 Exception added	  
      EXCEPTION
	     WHEN le_exception THEN
		    l_sec := 'Error for PO#'||i.segment1||'=>'||l_retmsg;
		    fnd_file.put_line(FND_FILE.log,l_sec);
			ROLLBACK;
	     WHEN OTHERS THEN
	        l_sec :=' Sending the mail to buyer for approved_and_not_received_pos, PO#: '||i.segment1||' is failed';
		    fnd_file.put_line(FND_FILE.log,l_sec);
			ROLLBACK;
	  END;
   END LOOP;
   l_sec:='Loop Ended';
   fnd_file.put_line(FND_FILE.log,'Total records fetched is :'||l_total_rec_fetched);
   l_sec :='Total count of mails sent is :'||l_count;
   fnd_file.put_line(FND_FILE.log,l_sec);
 EXCEPTION
 WHEN OTHERS
      THEN
        xxcus_error_pkg.xxcus_error_main_api (
          p_called_from         => 'XXWC_PO_NOTIFICATIONS_PKG.approved_and_not_received_pos',
          p_calling             => l_sec,
          p_request_id          => fnd_global.conc_request_id,
          p_ora_error_msg       => SQLERRM,
          p_error_desc          => 'Error in approved_and_not_received_pos procedure, When Others Exception',
          p_distribution_list   => g_dflt_email,
          p_module              => 'PO');
 END;
END XXWC_PO_NOTIFICATIONS_PKG;
/