/*************************************************************************
      Script : XXWC_PO_NOTIFICATION_TBL.sql

      PURPOSE:   To create table

      REVISIONS:
      Ver        Date        Author                     Description
      ---------  ----------  ---------------         -------------------------
      1.0        17/10/2017  Niraj K Ranjan          TMS#20170712-00193   Late PO notification
 ****************************************************************************/
 CREATE TABLE XXWC.XXWC_PO_NOTIFICATION_TBL
        (PO_HEADER_ID NUMBER,
		 EMAIL_SENT_DATE DATE,
		 TO_EMAIL_ADDRESS VARCHAR2(500),
		 CREATION_DATE DATE,
		 CREATED_BY NUMBER,
		 LAST_UPDATE_DATE DATE,
		 LAST_UPDATE_BY NUMBER,
		 TYPE VARCHAR2(100));