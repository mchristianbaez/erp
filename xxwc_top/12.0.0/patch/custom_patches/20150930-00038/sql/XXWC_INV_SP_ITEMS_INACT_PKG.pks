CREATE OR REPLACE PACKAGE APPS.xxwc_inv_sp_items_inact_pkg
AS
   /*****************************************************************************************
   *
   * Package Spec
   *  XXWC_INV_SP_ITEMS_INACT_PKG
   *
   * DESCRIPTION
   *  Report Only shows Special Items Inactivate
   *
   * PARAMETERS
   * ==========
   * NAME              TYPE     DESCRIPTION
   * ----------------- -------- --------------------------------------------------------------
   * P_DAYS            <IN>       Number of Days
   * P_TYPE            <IN>       Process Type
   *
   *
   *
   *
   * CALLED BY
   *   Which program, if any, calls this one
    * HISTORY
   * =======
   *
   * VERSION DATE        AUTHOR(S)         DESCRIPTION
   * ------- ----------- ---------------  --------------------------------------------------------
   * 1.00    07/11/2014   Gajendra M      Creation(TMS#20141104-00022)
   *
   * 1.1     10/05/2015   P.Vamshidhar    TMS#20150930-00038
   *                                      -- Added cuttoff date to pickup items
   *                                         (Creation date cutoff date)
   *                                      in load_inv_sp_main Procedure and load_items Procedure
   *                                      
   **********************************************************************************************/
   /****************************************************************************

      1. Procedure to Write Log Messages

   **************************************************************************/
   PROCEDURE write_log (p_log_msg VARCHAR2);

   /*************************************************************************

      2. Procedure to Load items

   **************************************************************************/
   PROCEDURE load_items (p_retcode OUT VARCHAR2, p_cutoff_date IN DATE);  -- Added additional parameter in 1.1 V.  

   /*************************************************************************

       3. Procedure to Load On order Quantity for Invetory Special Items

   **************************************************************************/
   PROCEDURE load_on_ord_qty (p_retcode OUT VARCHAR2);

   /*************************************************************************

      4. Procedure to extract and Special Items Items

   **********************************************************************************************/
   PROCEDURE extract_special_items (p_retcode OUT NUMBER, p_days IN NUMBER);

   /**********************************************************************************************

       6. Procedure to update end dated buyers

   **************************************************************************/
   PROCEDURE update_buyers (p_retcode OUT VARCHAR2);

   /*************************************************************************

      5. Procedure to update CSP and Special Inactive Items in Interface Table

   **************************************************************************/
   PROCEDURE update_special_items_status (p_retcode      OUT NUMBER,
                                          p_type      IN     VARCHAR2,
                                          p_days      IN     NUMBER);  -- Added additional parameter in 1.1 V.                               

   /*************************************************************************
        6. Main Procedure to run the sub loads sequentially
   **************************************************************************/
   PROCEDURE load_inv_sp_main (errbuf              OUT VARCHAR2,
                               retcode             OUT NUMBER,
                               p_cutoff_date    IN     VARCHAR2, -- Added additional parameter in 1.1 V.                               
                               p_no_of_days     IN     NUMBER,
                               p_process_type   IN     VARCHAR2);
END;
/
