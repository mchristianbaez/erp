CREATE OR REPLACE PACKAGE BODY APPS.XXWC_INV_SP_ITEMS_INACT_PKG
AS
   /****************************************************************************************************
    *
    * FUNCTION | PROCEDURE | CURSOR
    *  XXWC_INV_SP_ITEMS_INACT_PKG
    *
    * DESCRIPTION
    *  Report Only shows Special Items Inactivation
    *
    * PARAMETERS
    * ==========
    * NAME              TYPE     DESCRIPTION
   .* ----------------- -------- ------------------------------------------------------------------------
    * P_DAYS            <IN>       Number of Days
    * P_TYPE            <IN>       Process Type
    *
    *
    * CALLED BY
    *   Which program, if any, calls this one
    *
    * HISTORY
    * =======
    *
    * VERSION DATE        AUTHOR(S)       DESCRIPTION
    * ------- ----------- --------------- ----------------------------------------------------------------
    * 1.0    07/11/2014   Gajendra M      Creation(TMS#20141104-00022)
    * 1.1    05/10/2015   P.Vamshidhar    TMS#20150930-00038
    *                                     -- Added cuttoff date to pickup items
    *                                       (Creation date cutt off date)
    *                                        in Main Procedure
    ******************************************************************************************************/
   g_start        NUMBER;
   g_errbuf       CLOB;
   g_sec          VARCHAR2 (200);
   g_dflt_email   fnd_user.email_address%TYPE
                     := 'HDSOracleDevelopers@hdsupply.com';
   g_line_open    VARCHAR2 (100)
      := '/*************************************************************************';
   g_line_close   VARCHAR2 (100)
      := '**************************************************************************/';

   PROCEDURE write_log (p_log_msg VARCHAR2)
   /*************************************************************************
     $Header xxwc_inv_sp_items_inact_pkg.write_log $
     PURPOSE:     Procedure to write Log messages

     REVISIONS:
     Ver    Date         Author                Description
     ------ -----------  ------------------    ----------------
     1.0    17-Nov-14    Gajendra Mavilla     Initial Version
     **************************************************************************/
   IS
      l_log_msg   VARCHAR2 (2000);
   BEGIN
      IF p_log_msg = g_line_open OR p_log_msg = g_line_close
      THEN
         l_log_msg := p_log_msg;
      ELSE
         l_log_msg :=
            p_log_msg || ' : ' || TO_CHAR (SYSDATE, 'DD-MON-YYYY HH:MI:SS');
         xxwc_common_tunning_helpers.elapsed_time ('SPECIAL-MO',
                                                   p_log_msg,
                                                   g_start);
      END IF;

      fnd_file.put_line (fnd_file.LOG, l_log_msg);
   END write_log;

   PROCEDURE copy_temp_table (p_owner         VARCHAR2,
                              p_from_table    VARCHAR2,
                              p_to_table      VARCHAR2)
   /*************************************************************************
     $Header xxwc_inv_sp_items_inact_pkg.copy_temp_table $
     PURPOSE:     Local Procedure to copy the temp table data

     REVISIONS:
     Ver    Date         Author                Description
     ------ -----------  ------------------    ----------------
     1.0    17-Oct-14    Gajendra Mavilla     Initial Version
     **************************************************************************/
   IS
      l_table_count            NUMBER := 0;
      l_drop_stmt              VARCHAR2 (2000);
      l_from_table_full_name   VARCHAR2 (60)
                                  := p_owner || '.' || p_from_table;
      l_to_table_full_name     VARCHAR2 (60) := p_owner || '.' || p_to_table;
      l_success_msg            VARCHAR2 (200)
         := l_to_table_full_name || ' Table Dropped ';
      l_error_msg              VARCHAR (200)
         := l_to_table_full_name || ' Table Drop failed ';

      l_copy_success_msg       VARCHAR2 (200)
         :=    l_from_table_full_name
            || ' Table Copied to '
            || l_to_table_full_name;
      l_copy_error_msg         VARCHAR2 (200)
         := l_from_table_full_name || ' Table Copy failed ';
   BEGIN
      g_sec :=
            'COPY TABLE '
         || l_from_table_full_name
         || ' To '
         || l_to_table_full_name;

      SELECT COUNT (*)
        INTO l_table_count
        FROM all_tables
       WHERE table_name = UPPER (p_to_table) AND owner = UPPER (p_owner);

      IF l_table_count > 0
      THEN
         l_drop_stmt := 'DROP TABLE ' || l_to_table_full_name;

         EXECUTE IMMEDIATE l_drop_stmt;

         write_log (l_success_msg);
      ELSE
         write_log (l_to_table_full_name || ' Do not exist ');
      END IF;

      l_success_msg := l_copy_success_msg;
      l_error_msg := l_copy_error_msg;

      EXECUTE IMMEDIATE
            'CREATE TABLE '
         || l_to_table_full_name
         || ' AS SELECT * FROM '
         || l_from_table_full_name;

      write_log (l_success_msg);
   EXCEPTION
      WHEN OTHERS
      THEN
         g_errbuf := l_error_msg || CHR (10) || SQLERRM;
         write_log (g_errbuf);
   END;

   PROCEDURE drop_temp_table (p_owner VARCHAR2, p_table_name VARCHAR2)
   /*************************************************************************
     $Header xxwc_inv_sp_items_inact_pkg.drop_temp_table $
     PURPOSE:     Local Procedure to drop the temp tables

     REVISIONS:
     Ver    Date         Author                Description
     ------ -----------  ------------------    ----------------
     1.0    17-Nov-14   Gajendra Mavilla      Initial Version
     **************************************************************************/
   IS
      l_table_count       NUMBER := 0;
      l_drop_stmt         VARCHAR2 (2000);
      l_table_full_name   VARCHAR2 (60) := p_owner || '.' || p_table_name;
      l_success_msg       VARCHAR2 (200)
                             := l_table_full_name || ' Table Dropped ';
      l_error_msg         VARCHAR (200)
                             := l_table_full_name || ' Table Drop failed ';
   BEGIN
      g_sec := 'DROP TABLE ' || p_owner || '.' || p_table_name;


      SELECT COUNT (*)
        INTO l_table_count
        FROM all_tables
       WHERE table_name = UPPER (p_table_name) AND owner = UPPER (p_owner);

      IF l_table_count > 0
      THEN
         l_drop_stmt := 'DROP TABLE ' || p_owner || '.' || p_table_name;

         EXECUTE IMMEDIATE l_drop_stmt;

         write_log (l_success_msg);
      ELSE
         write_log (l_table_full_name || ' Do not exist ');
      END IF;
   EXCEPTION
      WHEN OTHERS
      THEN
         g_errbuf := l_error_msg || CHR (10) || SQLERRM;
         write_log (g_errbuf);
   END;

   PROCEDURE load_items (p_retcode OUT VARCHAR2, p_cutoff_date IN DATE)  -- added Parameter in Rev 1.1
   /*************************************************************************
     $Header xxwc_inv_sp_items_inact_pkg.Load_items $
     PURPOSE:     2. Procedure to Load Item Details

     REVISIONS:
     Ver    Date         Author                Description
     ------ -----------  ------------------    ----------------
     1.0    17-Nov-14    Gajendra Mavilla      Initial Version
     1.1    05-Oct-15    P.Vamshidhar          TMS#20150930-00038
    **************************************************************************/
   IS
      l_retcode              NUMBER := 0;
      --l_master_organization_id NUMBER := fnd_profile.value('XXWC_ITEM_MASTER_ORG');
      l_owner                VARCHAR2 (10) := 'XXWC';
      l_table_name           VARCHAR2 (40) := 'XXWC_INV_ITEMS##';
      l_table_full_name      VARCHAR2 (60) := l_owner || '.' || l_table_name;
      l_index_name           VARCHAR2 (60) := l_table_full_name || '_N1';
      l_table_success_msg    VARCHAR2 (200)
         := l_table_full_name || ' Table Load completed ';
      l_table_error_msg      VARCHAR2 (200)
                                := l_table_full_name || ' Table Load failed ';
      l_success_msg          VARCHAR2 (200) := l_table_success_msg;
      l_error_msg            VARCHAR2 (200) := l_table_error_msg;
      l_index_success_msg    VARCHAR2 (200)
         := l_index_name || ' Index Creation completed ';
      l_index_error_msg      VARCHAR2 (200)
                                := l_index_name || ' Index Creation failed ';
      l_create_table_stmt    VARCHAR2 (200)
         := 'CREATE TABLE ' || l_table_full_name || ' AS ';
      l_table_name1          VARCHAR2 (40) := 'XXWC_OM_CSP_PRICING_LINES##';
      l_table_full_name1     VARCHAR2 (60) := l_owner || '.' || l_table_name1;
      l_index_name1          VARCHAR2 (60) := l_table_full_name1 || '_N1';
      l_table_success_msg1   VARCHAR2 (200)
         := l_table_full_name1 || ' Table Load completed ';
      l_table_error_msg1     VARCHAR2 (200)
         := l_table_full_name1 || ' Table Load failed ';
      l_success_msg1         VARCHAR2 (200) := l_table_success_msg1;
      l_error_msg1           VARCHAR2 (200) := l_table_error_msg1;
      l_index_success_msg1   VARCHAR2 (200)
         := l_index_name1 || ' Index Creation completed ';
      l_index_error_msg1     VARCHAR2 (200)
                                := l_index_name1 || ' Index Creation failed ';
      l_create_table_stmt1   VARCHAR2 (200)
         := 'CREATE TABLE ' || l_table_full_name1 || ' AS ';
      l_table_name2          VARCHAR2 (40) := 'XXWC_INV_MTL_TRANSACTIONS##';
      l_table_full_name2     VARCHAR2 (60) := l_owner || '.' || l_table_name2;
      l_index_name2          VARCHAR2 (60) := l_table_full_name2 || '_N1';
      l_table_success_msg2   VARCHAR2 (200)
         := l_table_full_name2 || ' Table Load completed ';
      l_table_error_msg2     VARCHAR2 (200)
         := l_table_full_name2 || ' Table Load failed ';
      l_success_msg2         VARCHAR2 (200) := l_table_success_msg2;
      l_error_msg2           VARCHAR2 (200) := l_table_error_msg2;
      l_index_success_msg2   VARCHAR2 (200)
         := l_index_name2 || ' Index Creation completed ';
      l_index_error_msg2     VARCHAR2 (200)
                                := l_index_name2 || ' Index Creation failed ';
      l_create_table_stmt2   VARCHAR2 (200)
         := 'CREATE TABLE ' || l_table_full_name2 || ' AS ';
      l_select_stmt          VARCHAR2 (4000)
         := '     SELECT msib.inventory_item_id
                                  ,msib.organization_id
                                  ,msib.segment1
                                  ,msib.creation_date
                                  ,msib.last_update_date
                                  ,msib.inventory_item_status_code
                                  ,msib.item_type
                                  ,msib.primary_uom_code
                                  ,msib.primary_unit_of_measure
                                  ,msib.buyer_id
                                  ,''Y'' Process_flag  -- Added in Rev 1.1
                              FROM apps.mtl_system_items_b msib
                              WHERE 1=1
                              AND msib.item_type=''SPECIAL''
                              AND TRUNC(msib.creation_date)<= TO_DATE('''||p_cutoff_date||''',''DD-MON-YY'')   -- Added in Rev 1.1
                              AND msib.inventory_item_status_code=''Active''
                               ';
      l_load_stmt            VARCHAR2 (4000)
                                := l_create_table_stmt || l_select_stmt;

      l_index_stmt           VARCHAR2 (200)
         :=    'CREATE INDEX '
            || l_index_name
            || ' ON '
            || l_table_full_name
            || ' ( organization_id, inventory_item_id,segment1)';
      l_select_stmt1         VARCHAR2 (4000)
         := '     SELECT  csp.agreement_line_id
                                    ,csp.agreement_id
                                    ,csp.agreement_type
                                    ,csp.vendor_quote_number
                                    ,csp.product_attribute
                                    ,csp.product_value
                                    ,csp.product_description
                                    ,csp.start_date
                                    ,csp.end_date
                                    ,csp.modifier_type
                                    ,csp.line_status
                                    ,csp.creation_date
                              FROM xxwc.xxwc_om_contract_pricing_lines csp
                               ';
      l_load_stmt1           VARCHAR2 (4000)
                                := l_create_table_stmt1 || l_select_stmt1;

      l_index_stmt1          VARCHAR2 (200)
         :=    'CREATE INDEX '
            || l_index_name1
            || ' ON '
            || l_table_full_name1
            || ' ( product_value )';
      l_select_stmt2         VARCHAR2 (4000)
         := '     SELECT mmt.inventory_item_id
                                  ,mmt.organization_id
                                  ,max(mmt.transaction_date) transaction_date
                              FROM apps.mtl_material_transactions mmt
                              group by mmt.inventory_item_id,mmt.organization_id
                              ';
      l_load_stmt2           VARCHAR2 (4000)
                                := l_create_table_stmt2 || l_select_stmt2;

      l_index_stmt2          VARCHAR2 (200)
         :=    'CREATE INDEX '
            || l_index_name2
            || ' ON '
            || l_table_full_name2
            || ' ( inventory_item_id,transaction_date,organization_id)';
   BEGIN
      g_sec := 'Process 2 : Load Special Items ';
      write_log (g_line_open);
      write_log (g_sec);

      drop_temp_table (l_owner, l_table_name);

      FND_FILE.PUT_LINE(FND_FILE.LOG,' l_select_stmt'||l_select_stmt);   -- Added in Rev 1.1

      EXECUTE IMMEDIATE l_load_stmt;

      write_log (l_success_msg);

      l_success_msg := l_index_success_msg;
      l_error_msg := l_index_error_msg;

      EXECUTE IMMEDIATE l_index_stmt;

      write_log (l_success_msg);
      g_sec := 'Process 2.1 : Load CSP Items ';
      write_log (g_sec);

      drop_temp_table (l_owner, l_table_name1);

      EXECUTE IMMEDIATE l_load_stmt1;

      write_log (l_success_msg1);

      l_success_msg1 := l_index_success_msg1;
      l_error_msg1 := l_index_error_msg1;

      EXECUTE IMMEDIATE l_index_stmt1;

      write_log (l_success_msg1);
      g_sec := 'Process 2.2 : Load Material Transactions ';
      write_log (g_sec);

      drop_temp_table (l_owner, l_table_name2);

      EXECUTE IMMEDIATE l_load_stmt2;

      write_log (l_success_msg2);

      l_success_msg2 := l_index_success_msg2;
      l_error_msg2 := l_index_error_msg2;

      EXECUTE IMMEDIATE l_index_stmt2;

      write_log (l_success_msg2);
      write_log (g_line_close);
   EXCEPTION
      WHEN OTHERS
      THEN
         l_retcode := 2;
         p_retcode := l_retcode;
         g_errbuf := l_error_msg || CHR (10) || SQLERRM;
         write_log (g_errbuf);
         write_log (g_line_close);
   END load_items;

   PROCEDURE load_on_ord_qty (p_retcode OUT VARCHAR2)
   /*************************************************************************
     $Header xxwc_inv_sp_items_inact_pkg.load_on_ord_qty $
     PURPOSE:     3. Procedure to Load On Order Quantity

     REVISIONS:
     Ver    Date         Author                Description
     ------ -----------  ------------------    ----------------
     1.0    17-Nov-14    Gajendra Mavilla    Initial Version
     **************************************************************************/
   IS
      l_retcode              NUMBER := 0;
      l_owner                VARCHAR2 (10) := 'XXWC';
      l_table_name           VARCHAR2 (40) := 'XXWC_PO_ON_ORD##';
      l_table_full_name      VARCHAR2 (60) := l_owner || '.' || l_table_name;
      l_index_name           VARCHAR2 (60) := l_table_full_name || '_N1';
      l_table_success_msg    VARCHAR2 (200)
         := l_table_full_name || ' Table Load completed ';
      l_table_error_msg      VARCHAR2 (200)
                                := l_table_full_name || ' Table Load failed ';
      l_success_msg          VARCHAR2 (200);
      l_error_msg            VARCHAR2 (200);
      l_index_success_msg    VARCHAR2 (200)
         := l_index_name || ' Index Creation completed ';
      l_index_error_msg      VARCHAR2 (200)
                                := l_index_name || ' Index Creation failed ';
      l_create_table_stmt    VARCHAR2 (200)
         := 'CREATE TABLE ' || l_table_full_name || ' AS ';
      l_load_stmt            CLOB;

      l_table_name1          VARCHAR2 (40) := 'XXWC_PO_ON_ORD_QTY##';
      l_table_full_name1     VARCHAR2 (60) := l_owner || '.' || l_table_name1;
      l_index_name1          VARCHAR2 (60) := l_table_full_name1 || '_N1';
      l_table_success_msg1   VARCHAR2 (200)
         := l_table_full_name1 || ' Table Load completed ';
      l_table_error_msg1     VARCHAR2 (200)
         := l_table_full_name1 || ' Table Load failed ';
      l_success_msg1         VARCHAR2 (200);
      l_error_msg1           VARCHAR2 (200);
      l_index_success_msg1   VARCHAR2 (200)
         := l_index_name || ' Index Creation completed ';
      l_index_error_msg1     VARCHAR2 (200)
                                := l_index_name1 || ' Index Creation failed ';
      l_create_table_stmt1   VARCHAR2 (200)
         := 'CREATE TABLE ' || l_table_full_name1 || ' AS ';


      l_select_stmt1         VARCHAR2 (4000)
         := '    SELECT org_id, inventory_item_id,
                        organization_id,
                        SUM (
                             (  quantity
                              - NVL (quantity_received, 0)
                              - NVL (quantity_cancelled, 0))
                           * po_uom_s.po_uom_convert (ordered_uom,
                                                      primary_uom,
                                                      inventory_item_id))
                           on_ord
                   FROM XXWC.XXWC_PO_ON_ORD##
               GROUP BY inventory_item_id, organization_id, org_id
   ';
      l_load_stmt1           VARCHAR2 (4000)
                                := l_create_table_stmt1 || l_select_stmt1;

      l_index_stmt           VARCHAR2 (200)
         :=    'CREATE INDEX '
            || l_index_name
            || ' ON '
            || l_table_full_name
            || ' ( organization_id, inventory_item_id,org_id)';

      l_index_stmt1          VARCHAR2 (200)
         :=    'CREATE INDEX '
            || l_index_name1
            || ' ON '
            || l_table_full_name1
            || ' ( organization_id, inventory_item_id,org_id)';
   BEGIN
      g_sec := 'Process 3 : Load On Order Qty ';
      write_log (g_line_open);
      write_log (g_sec);
      g_sec := 'Process 3.1 : Load On Order Transactions ';
      write_log (g_sec);

      l_load_stmt :=
            l_create_table_stmt
         || '  SELECT poh.org_id, pol.item_id inventory_item_id,
                      poll.ship_to_organization_id organization_id,
                      pol.unit_meas_lookup_code ordered_uom,
                      msi.primary_unit_of_measure primary_uom,
                      poll.quantity,
                      poll.quantity_received,
                      poll.quantity_cancelled
                 FROM apps.po_headers_all poh,
                      apps.po_line_locations_all poll,
                      apps.po_lines_all pol,
                      apps.po_releases_all por,
                      apps.hr_locations_all_tl hl,
                      xxwc.xxwc_inv_items## msi,
                      apps.mtl_units_of_measure mum,
                      apps.po_line_types_b polt,
                      apps.gl_daily_conversion_types dct,
                      apps.rcv_parameters rp
                WHERE     NVL (poll.approved_flag, ''N'') = ''Y''
                      AND NVL (poll.cancel_flag, ''N'') = ''N''
                      AND NVL (pol.clm_info_flag, ''N'') = ''N''
                      AND (   NVL (pol.clm_option_indicator, ''B'') <> ''O''
                           OR NVL (pol.clm_exercised_flag, ''N'') = ''Y'')
                      AND poll.closed_code IN (''OPEN'', ''CLOSED FOR INVOICE'')
                      AND poll.shipment_type IN (''STANDARD'', ''BLANKET'', ''SCHEDULED'')
                      AND poh.po_header_id = poll.po_header_id
                      AND pol.po_line_id = poll.po_line_id
                      AND poll.po_release_id = por.po_release_id(+)
                      AND poll.ship_to_location_id = hl.location_id(+)
                      AND pol.line_type_id = polt.line_type_id(+)
                      AND mum.unit_of_measure(+) = pol.unit_meas_lookup_code
                      AND NVL (msi.organization_id, poll.ship_to_organization_id) =
                             poll.ship_to_organization_id
                      AND msi.inventory_item_id(+) = pol.item_id
                      AND dct.conversion_type(+) = poh.rate_type
                      AND NVL (poh.consigned_consumption_flag, ''N'') = ''N''
                      AND NVL (por.consigned_consumption_flag, ''N'') = ''N''
                      AND NVL (poll.matching_basis, ''QUANTITY'') != ''AMOUNT''
                      AND poll.payment_type IS NULL
                      AND NVL (poll.drop_ship_flag, ''N'') = ''N''
                      AND rp.organization_id = poll.ship_to_organization_id
                      AND (   NVL (rp.pre_receive, ''N'') = ''N''
                           OR (    NVL (rp.pre_receive, ''N'') = ''Y''
                               AND NVL (poll.lcm_flag, ''N'') = ''N''))
                      AND (  poll.quantity
                           - NVL (poll.quantity_received, 0)
                           - NVL (poll.quantity_cancelled, 0)) <> 0
                      AND (   EXISTS
                                 (SELECT ''Not associated to WIP Job''
                                    FROM apps.po_distributions_all pod1,
                                         apps.po_lines_all pltv
                                   WHERE     pod1.po_line_id = pltv.po_line_id
                                         AND pod1.line_location_id =
                                                poll.line_location_id
                                         AND pod1.wip_entity_id IS NULL)
                           OR EXISTS
                                 (SELECT ''Jobs not related to EAM WO or Closed WIP Jobs''
                                    FROM apps.po_distributions_all pod1,
                                         apps.po_lines_all pltv,
                                         apps.wip_entities we
                                   WHERE     pod1.po_line_id = pltv.po_line_id
                                         AND pod1.line_location_id =
                                                poll.line_location_id
                                         AND pod1.wip_entity_id = we.wip_entity_id
                                         AND we.entity_type NOT IN (6, 7, 3))
                           OR EXISTS
                                 (SELECT ''Open EAM WO Receipts''
                                    FROM apps.po_distributions_all pod1,
                                         apps.po_lines_all pltv,
                                         apps.wip_entities we,
                                         apps.wip_discrete_jobs wdj
                                   WHERE     pod1.line_location_id =
                                                poll.line_location_id
                                         AND pod1.wip_entity_id = we.wip_entity_id
                                         AND we.wip_entity_id = wdj.wip_entity_id
                                         AND we.entity_type = 6
                                         AND wdj.status_type IN (3, 4, 6)))
UNION ALL
               SELECT b.org_id, b.item_id inventory_item_id,
                      b.destination_organization_id organization_id ,
                      d.unit_of_measure ordered_uom,
                      c.primary_unit_of_measure primary_uom,
                      b.quantity,
                      b.quantity_received,
                      b.quantity_cancelled
                 FROM apps.po_requisition_lines_all b,
                      apps.po_requisition_headers_all a,
                      xxwc.xxwc_inv_items## c,
                      apps.mtl_units_of_measure d
                WHERE     b.source_type_code = ''INVENTORY''
                      AND NVL (b.cancel_flag, ''N'') = ''N''
                      AND a.type_lookup_code = ''INTERNAL''
                      AND a.authorization_status = ''APPROVED''
                      AND a.requisition_header_id = b.requisition_header_id
                      AND b.item_id = c.inventory_item_id
                      AND b.unit_meas_lookup_code = d.unit_of_measure
                      AND (  b.quantity
                           - NVL (b.quantity_received, 0)
                           - NVL (b.quantity_cancelled, 0)) <> 0
                      AND b.destination_organization_id = c.organization_id
                      AND NVL (b.drop_ship_flag, ''N'') = ''N''
                ';



      l_success_msg := l_table_success_msg;
      l_error_msg := l_table_error_msg;

      drop_temp_table (l_owner, l_table_name);


      EXECUTE IMMEDIATE l_load_stmt;

      write_log (l_success_msg);

      ----------------------------------
      g_sec := 'Process 3.2 : Load On Order Quantity';

      write_log (g_sec);

      l_success_msg := l_table_success_msg1;
      l_error_msg := l_table_error_msg1;

      drop_temp_table (l_owner, l_table_name1);

      EXECUTE IMMEDIATE l_load_stmt1;

      write_log (l_success_msg);
      -----------------------------------

      write_log (g_line_close);
   EXCEPTION
      WHEN OTHERS
      THEN
         l_retcode := 2;
         p_retcode := l_retcode;
         g_errbuf := l_error_msg || CHR (10) || SQLERRM;
         write_log (g_errbuf);
         write_log (g_line_close);
   END load_on_ord_qty;



   PROCEDURE extract_special_items (p_retcode OUT NUMBER, p_days IN NUMBER)
   /*************************************************************************
     $Header xxwc_inv_sp_items_inact_pkg.extract_special_items $
     PURPOSE:     Procedure to Load Special Inactive Items

     REVISIONS:
     Ver    Date         Author                Description
     ------ -----------  ------------------    ----------------
     1.0    7-Nov-14    Gajendra Mavilla       Initial Version
     1.1    05-Oct-15   P.Vamshidhar           TMS#20150930-00038
     **************************************************************************/
   IS
      l_days                NUMBER := 0;
      l_retcode             NUMBER := 0;
      l_owner               VARCHAR2 (10) := 'XXWC';
      l_table_name          VARCHAR2 (40) := 'XXWC_INV_SP_INACT_ITEMS##';
      l_table_full_name     VARCHAR2 (60) := l_owner || '.' || l_table_name;
      l_index_name          VARCHAR2 (60) := l_table_full_name || '_N1';
      l_table_success_msg   VARCHAR2 (200)
         := l_table_full_name || ' Table Load completed ';
      l_table_error_msg     VARCHAR2 (200)
                               := l_table_full_name || ' Table Load failed ';
      l_success_msg         VARCHAR2 (200);
      l_error_msg           VARCHAR2 (200);
      l_index_success_msg   VARCHAR2 (200)
         := l_index_name || ' Index Creation completed ';
      l_index_error_msg     VARCHAR2 (200)
                               := l_index_name || ' Index Creation failed ';
      l_create_table_stmt   VARCHAR2 (200)
         := 'CREATE TABLE ' || l_table_full_name || ' AS ';
      l_load_stmt           CLOB;
   BEGIN
      l_days := p_days;
      g_sec := 'Process 4 : Load Special Items ';
      write_log (g_line_open);
      write_log (g_sec);
      g_sec := 'Process 4.1 : Load Special Items Details ';
      write_log (g_sec);
      l_load_stmt :=
            l_create_table_stmt
         || '  SELECT a.inventory_item_id
                      ,a.organization_id
                      ,b.organization_code
                      ,a.segment1 Part_number
                      ,a.creation_date
                      ,a.last_update_date
             ,(SELECT transaction_date
                FROM XXWC.XXWC_INV_MTL_TRANSACTIONS##
                WHERE 1=1
                AND inventory_item_id=a.inventory_item_id
                AND organization_id=a.organization_id) LAST_ACTIVITY_DATE
            ,(SELECT decode(nvl(sum(transaction_quantity),0),0,''N'',''Y'')
                FROM apps.mtl_onhand_quantities
               WHERE 1 = 1
                 AND inventory_item_id = a.inventory_item_id
                 AND organization_id   = a.organization_id
                )OH_EXISTS
            ,(SELECT decode(COUNT(*), 0, ''N'', ''Y'')
                FROM apps.oe_order_lines_all   ol
                    ,apps.oe_order_headers_all oh
               WHERE 1 = 1
                 AND ol.header_id = oh.header_id
                 AND ol.inventory_item_id = a.inventory_item_id
                 --AND ol.ship_from_org_id = a.organization_id
                 AND nvl(ordered_quantity, 0) >= 0
                 AND trunc(ol.creation_date)>=sysdate-'||p_days||'     -- Added in Rev 1.1
                 AND EXISTS (SELECT ''X''
                             FROM apps.wsh_new_deliveries wnd
                                  ,apps.wsh_delivery_assignments wda
                                  ,apps.wsh_delivery_details wdd
                             WHERE 1=1
                               AND wnd.delivery_id= wda.delivery_id(+)
                               AND wdd.source_header_id(+)=oh.header_id
                               AND wdd.delivery_detail_id(+) = wda.delivery_detail_id
                               )) so_exists
            ,(SELECT decode(COUNT(*), 0, ''N'', ''Y'')
                FROM apps.oe_order_lines_all   ol
                    ,apps.oe_order_headers_all oh
               WHERE 1 = 1
                 AND ol.header_id = oh.header_id
                 AND ol.inventory_item_id = a.inventory_item_id
                 AND ol.ship_from_org_id = a.organization_id
                 AND quote_number IS NOT NULL
                 AND nvl(ordered_quantity, 0) > 0) quote_exists
                ,decode((SELECT ord.on_ord
                   FROM XXWC.XXWC_PO_ON_ORD_QTY## ord
                      WHERE 1=1
                      AND ord.inventory_item_id=a.inventory_item_id
                      AND ord.organization_id=a.organization_id
                 AND EXISTS (SELECT ''X''
                            FROM apps.po_lines_all pl,
                                 apps.po_headers_all ph,
                                 po_line_locations_all pll
                                WHERE     1 = 1
                                 AND pl.po_header_id = ph.po_header_id
                                 AND pl.po_line_id = pll.po_line_id(+)
                                 AND pl.item_id = ord.inventory_item_id
                                 AND a.organization_id =NVL (pll.ship_to_organization_id, a.organization_id)
                                 AND (NVL (pl.closed_flag, ''X'') <> ''Y'' OR pl.closed_code <> ''CLOSED'')
                                 AND NVL (pl.cancel_flag, ''X'') <> ''Y''
                                 AND NVL (ph.cancel_flag, ''X'') <> ''Y'')
                                 ),null,''N'',0,''N'',''Y'')PO_EXISTS
                ,(SELECT decode(COUNT(*), 0, ''N'', ''Y'')
                         from XXWC.XXWC_OM_CSP_PRICING_LINES## cpl
                        where 1=1
                         AND cpl.product_attribute = ''ITEM''
                         AND nvl(trunc(cpl.end_date),trunc(sysdate)) > trunc(SYSDATE)
                         AND cpl.product_value=a.segment1)  ACTIVE_CSP_EXISTS
                        ,(SELECT decode(COUNT(*), 0, ''N'', ''Y'')
                         from XXWC.XXWC_OM_CSP_PRICING_LINES## cpl
                        where 1=1
                         AND cpl.product_attribute = ''ITEM''
                         AND nvl(trunc(cpl.end_date),trunc(sysdate)) <= trunc(SYSDATE)
                         AND cpl.product_value=a.segment1)  INACTIVE_CSP_EXISTS
                ,a.inventory_item_status_code
                ,initcap(a.item_type) item_type
                ,a.primary_uom_code
                ,a.buyer_id
                ,a.process_flag    -- Added in Rev 1.1
        FROM XXWC.XXWC_INV_ITEMS##           a
            ,apps.org_organization_definitions b
       WHERE 1=1
         AND a.organization_id = b.organization_id
         AND a.item_type = ''SPECIAL''
         AND a.inventory_item_status_code=''Active''';
-- Commented below code in Rev 1.1
--         AND NOT EXISTS
--         (SELECT ''X''
--           FROM  XXWC.XXWC_INV_MTL_TRANSACTIONS##  d
--                  WHERE 1=1
--                    AND d.inventory_item_id=a.inventory_item_id
--                    AND TRUNC (d.transaction_date) > TRUNC(SYSDATE) - '
--         || p_days
--         || ' '
--         || '
--                   AND d.organization_id =a.organization_id)
--                 ';

      l_success_msg := l_table_success_msg;
      l_error_msg := l_table_error_msg;

      drop_temp_table (l_owner, l_table_name);
      g_sec :=
         'Process 4.2 : Special Items to be deleted from XXWC.XXWC_INV_SPECIAL_ITEMS_TBL  ';
      write_log (g_sec);

      EXECUTE IMMEDIATE ' TRUNCATE TABLE XXWC.XXWC_INV_SPECIAL_ITEMS_TBL ';

      EXECUTE IMMEDIATE l_load_stmt;

      write_log (l_success_msg);

      copy_temp_table (l_owner,
                       'XXWC_INV_SP_INACT_ITEMS##',
                       'XXWC_INV_SPECIAL_ITEMS_TBL');
      write_log (g_line_close);
   EXCEPTION
      WHEN OTHERS
      THEN
         l_retcode := 2;
         p_retcode := l_retcode;
         g_errbuf := l_error_msg || CHR (10) || SQLERRM;
         write_log (g_errbuf);
         write_log (g_line_close);
   END extract_special_items;

   PROCEDURE update_buyers (p_retcode OUT VARCHAR2)
   /*************************************************************************
     $Header xxwc_inv_sp_items_inact_pkg.update_buyers $
     PURPOSE:     Procedure to Load Special Inactive Items

     REVISIONS:
     Ver    Date         Author                Description
     ------ -----------  ------------------    ----------------
     1.0    7-Nov-14    Gajendra Mavilla         Initial Version
     **************************************************************************/
   IS
      --l_buyer_id  NUMBER;
      l_retcode     NUMBER := 0;
      l_error_msg   VARCHAR2 (200);

      CURSOR buyer_cur
      IS
         SELECT pa.agent_id, inventory_item_id, organization_id
           FROM po_agents pa, XXWC.XXWC_INV_SPECIAL_ITEMS_TBL isi
          WHERE     1 = 1
                AND pa.agent_id = isi.buyer_id
                AND TRUNC (pa.end_date_active) <= TRUNC (SYSDATE);
   BEGIN
      g_sec := '6.0:Process: Update end dated buyers in Custom Table';
      write_log (g_line_open);
      write_log (g_sec);

      FOR rec_buyer IN buyer_cur
      LOOP
         BEGIN
            UPDATE XXWC.XXWC_INV_SPECIAL_ITEMS_TBL isi
               SET isi.buyer_id =
                      (SELECT pa.agent_id
                         FROM per_people_f ppf, po_agents pa
                        WHERE     full_name =
                                     'XXWC_EMP_SUPPLYCHAIN, Dummy Employee'
                              AND pa.agent_id = ppf.person_id
                              AND ROWNUM = 1
                              AND TRUNC (SYSDATE) BETWEEN TRUNC (
                                                             ppf.effective_start_date)
                                                      AND TRUNC (
                                                             ppf.effective_end_date))
             WHERE     1 = 1
                   AND isi.inventory_item_id = rec_buyer.inventory_item_id
                   AND isi.organization_id = rec_buyer.organization_id;
         EXCEPTION
            WHEN OTHERS
            THEN
               NULL;
         END;

         COMMIT;
      END LOOP;

      write_log (g_line_close);
   EXCEPTION
      WHEN OTHERS
      THEN
         l_retcode := 0;
         p_retcode := l_retcode;
         g_errbuf := l_error_msg || CHR (10) || SQLERRM;
         write_log (g_errbuf);
         write_log (g_line_close);
   END update_buyers;

   PROCEDURE update_special_items_status (p_retcode      OUT NUMBER,
                                          p_type      IN     VARCHAR2,
                                          p_days      IN     NUMBER)   -- Added Additional Parameter in Rev 1.1
   /*************************************************************************
     $Header xwc_inv_sp_items_inact_pkg.update_special_items_status $
     PURPOSE:     5.Procedure to update CSP Items in MTL_SYSTEM_ITEMS_INTERFACE

     REVISIONS:
     Ver    Date         Author                Description
     ------ -----------  ------------------    --------------------------------
     1.0    11-Nov-14    Gajendra Mavilla      Initial Version
     1.1    05-Oct-15    P.Vamshidhar          TMS#20150930-00038
     **************************************************************************/
   IS
      l_req_id             NUMBER NULL;
      v_phase              VARCHAR2 (50);
      v_status             VARCHAR2 (50);
      v_dev_status         VARCHAR2 (50);
      v_dev_phase          VARCHAR2 (50);
      v_message            VARCHAR2 (250);
      v_error_message      VARCHAR2 (3000);
      v_supplier_id        NUMBER;
      v_rec_cnt            NUMBER := 0;
      v_interval           NUMBER := 30;                         -- In seconds
      v_max_time           NUMBER := 15000;                      -- In seconds
      l_message            VARCHAR2 (150);
      l_errormessage       VARCHAR2 (3000);
      l_statement          VARCHAR2 (9000);
      l_program            VARCHAR2 (30) := 'INCOIN'; /* Concurrent Program short name */
      l_application        VARCHAR2 (30) := 'INV';
      ----------------------
      l_retcode            NUMBER := 0;



      CURSOR inact_items_cur
      IS
         SELECT *
           FROM XXWC.XXWC_INV_SPECIAL_ITEMS_TBL isi
          WHERE     1 = 1
          -- Commented below code in Rev 1.1     
              /*AND isi.oh_exists = 'N'
                AND isi.po_exists = 'N'
                AND isi.so_exists = 'N'
                AND isi.quote_exists = 'N'
                AND isi.active_csp_exists = 'N'
                AND isi.inactive_csp_exists = 'N'*/
             AND NVL(PROCESS_FLAG,'Y')='Y';               -- Added new condition in Rev 1.1

      TYPE item_success_tbl_type IS TABLE OF inact_items_cur%ROWTYPE;

      l_item_success_tbl   item_success_tbl_type;

      CURSOR error_items_cur
      IS
         SELECT msii.segment1,
                msii.description,
                msii.organization_id,
                msii.organization_code,
                mie.error_message
           FROM mtl_system_items_interface msii, mtl_interface_errors mie
          WHERE     mie.request_id = l_req_id
                AND msii.request_id = mie.request_id
                AND msii.transaction_id = mie.transaction_id;
   BEGIN
      g_sec := '5:Process : Review Inventory Special Items in Output file';
      write_log (g_line_open);
      write_log (g_sec);

   -- Below code added in Rev 1.1   -- Start
 
    UPDATE XXWC.XXWC_INV_SPECIAL_ITEMS_TBL A SET A.PROCESS_FLAG='N'
    WHERE OH_EXISTS = 'Y' OR
          SO_EXISTS = 'Y' OR
          QUOTE_EXISTS = 'Y' OR
          PO_EXISTS = 'Y' OR
          ACTIVE_CSP_EXISTS = 'Y' OR
          INACTIVE_CSP_EXISTS = 'Y';

     -- Updating data
    UPDATE XXWC.XXWC_INV_SPECIAL_ITEMS_TBL A
       SET A.PROCESS_FLAG = 'N'
     WHERE EXISTS
              (  SELECT 1
                   FROM XXWC.XXWC_INV_SPECIAL_ITEMS_TBL
                  WHERE inventory_item_id = A.INVENTORY_ITEM_ID
               GROUP BY inventory_item_id
                 HAVING (   COUNT (DISTINCT OH_EXISTS) > 1
                         OR COUNT (DISTINCT SO_EXISTS) > 1
                         OR COUNT (DISTINCT QUOTE_EXISTS) > 1
                         OR COUNT (DISTINCT PO_EXISTS) > 1
                         OR COUNT (DISTINCT ACTIVE_CSP_EXISTS) > 1
                         OR COUNT (DISTINCT INACTIVE_CSP_EXISTS) > 1));
    COMMIT;

    UPDATE  XXWC.XXWC_INV_SPECIAL_ITEMS_TBL A
SET A.PROCESS_FLAG='N' WHERE EXISTS
(SELECT 1 FROM XXWC.XXWC_INV_SPECIAL_ITEMS_TBL
              WHERE inventory_item_id = A.INVENTORY_ITEM_ID
              AND LAST_ACTIVITY_DATE >=SYSDATE- P_DAYS);


   -- Below code added in Rev 1.1   -- End

      IF p_type = 'Review'
      THEN
         BEGIN
            fnd_file.put_line (
               fnd_file.output,
                  'INVENTORY_ITEM_ID'
               || '|'
               || 'ORGANIZATION_ID'
               || '|'
               || 'ORGANIZATION_CODE'
               || '|'
               || 'PART_NUMBER'
               || '|'
               || 'CREATION_DATE'
               || '|'
               || 'LAST_UPDATE_DATE'
               || '|'
               || 'LAST_ACTIVITY_DATE'
               || '|'
               || 'OH_EXISTS'
               || '|'
               || 'PO_EXISTS'
               || '|'
               || 'SO_EXISTS'
               || '|'
               || 'QUOTE_EXISTS'
               || '|'
               || 'ACTIVE_CSP_EXISTS'
               || '|'
               || 'INACTIVE_CSP_EXISTS'
               || '|'
               || 'INVENTORY_ITEM_STATUS_CODE'
               || '|'
               || 'ITEM_TYPE'
               || '|'
               || 'PRIMARY_UOM_CODE'
               || '|'
               || 'INACTIVATE_ITEM');

            OPEN inact_items_cur;

            FETCH inact_items_cur BULK COLLECT INTO l_item_success_tbl;

            CLOSE inact_items_cur;

            FOR i IN 1 .. l_item_success_tbl.COUNT
            LOOP
               fnd_file.put_line (
                  fnd_file.output,
                     l_item_success_tbl (i).inventory_item_id
                  || '|'
                  || l_item_success_tbl (i).organization_id
                  || '|'
                  || l_item_success_tbl (i).organization_code
                  || '|'
                  || l_item_success_tbl (i).part_number
                  || '|'
                  || l_item_success_tbl (i).creation_date
                  || '|'
                  || l_item_success_tbl (i).last_update_date
                  || '|'
                  || l_item_success_tbl (i).last_activity_date
                  || '|'
                  || l_item_success_tbl (i).oh_exists
                  || '|'
                  || l_item_success_tbl (i).po_exists
                  || '|'
                  || l_item_success_tbl (i).so_exists
                  || '|'
                  || l_item_success_tbl (i).quote_exists
                  || '|'
                  || l_item_success_tbl (i).active_csp_exists
                  || '|'
                  || l_item_success_tbl (i).inactive_csp_exists
                  || '|'
                  || l_item_success_tbl (i).inventory_item_status_code
                  || '|'
                  || l_item_success_tbl (i).item_type
                  || '|'
                  || l_item_success_tbl (i).primary_uom_code
                  || '|'
                  || 'Y');
            END LOOP;

            g_sec := '5.1:Process : Special Items are updated in Output file';
            write_log (g_sec);
            write_log (g_line_close);
         END;
      END IF;

      IF p_type = 'Inactivate'
      THEN
         BEGIN
            g_sec :=
               '5.2:Process: Load Inventory Special Items into Interface Table';
            write_log (g_line_open);
            write_log (g_sec);

            EXECUTE IMMEDIATE ' INSERT INTO mtl_system_items_interface
            (inventory_item_id
            ,segment1
            ,process_flag
            ,set_process_id
            ,organization_id
            ,organization_code
            ,transaction_type
            ,source_type
            ,template_name
            ,inventory_item_status_code
            ,primary_uom_code
            ,buyer_id)
             SELECT inventory_item_id
            ,part_number
            ,1
            ,1
            ,organization_id
            ,organization_code
            ,''UPDATE''
            ,2
            ,item_type
            ,''Inactive''
            ,primary_uom_code
            ,buyer_id
          FROM XXWC.XXWC_INV_SPECIAL_ITEMS_TBL
         WHERE 1=1
          /*  Below code commented in Rev 1.1  
           AND oh_exists=''N''
           AND po_exists = ''N''
           AND so_exists = ''N''
           AND quote_exists = ''N''
           AND active_csp_exists = ''N''
           AND inactive_csp_exists=''N''
          */  
           AND PROCESS_FLAG = ''Y''';    -- Added additional condition in Rev 1.1

            COMMIT;

            fnd_file.put_line (
               fnd_file.output,
                  'INVENTORY_ITEM_ID'
               || '|'
               || 'ORGANIZATION_ID'
               || '|'
               || 'ORGANIZATION_CODE'
               || '|'
               || 'PART_NUMBER'
               || '|'
               || 'CREATION_DATE'
               || '|'
               || 'LAST_UPDATE_DATE'
               || '|'
               || 'LAST_ACTIVITY_DATE'
               || '|'
               || 'OH_EXISTS'
               || '|'
               || 'PO_EXISTS'
               || '|'
               || 'SO_EXISTS'
               || '|'
               || 'QUOTE_EXISTS'
               || '|'
               || 'ACTIVE_CSP_EXISTS'
               || '|'
               || 'INACTIVE_CSP_EXISTS'
               || '|'
               || 'INVENTORY_ITEM_STATUS_CODE'
               || '|'
               || 'ITEM_TYPE'
               || '|'
               || 'PRIMARY_UOM_CODE'
               || '|'
               || 'INACTIVATE_ITEM');

            OPEN inact_items_cur;

            FETCH inact_items_cur BULK COLLECT INTO l_item_success_tbl;

            CLOSE inact_items_cur;

            FOR j IN 1 .. l_item_success_tbl.COUNT
            LOOP
               fnd_file.put_line (
                  fnd_file.output,
                     l_item_success_tbl (j).inventory_item_id
                  || '|'
                  || l_item_success_tbl (j).organization_id
                  || '|'
                  || l_item_success_tbl (j).organization_code
                  || '|'
                  || l_item_success_tbl (j).part_number
                  || '|'
                  || l_item_success_tbl (j).creation_date
                  || '|'
                  || l_item_success_tbl (j).last_update_date
                  || '|'
                  || l_item_success_tbl (j).last_activity_date
                  || '|'
                  || l_item_success_tbl (j).oh_exists
                  || '|'
                  || l_item_success_tbl (j).po_exists
                  || '|'
                  || l_item_success_tbl (j).so_exists
                  || '|'
                  || l_item_success_tbl (j).quote_exists
                  || '|'
                  || l_item_success_tbl (j).active_csp_exists
                  || '|'
                  || l_item_success_tbl (j).inactive_csp_exists
                  || '|'
                  || l_item_success_tbl (j).inventory_item_status_code
                  || '|'
                  || l_item_success_tbl (j).item_type
                  || '|'
                  || l_item_success_tbl (j).primary_uom_code
                  || '|'
                  || 'Y');
            END LOOP;

            g_sec :=
               ' Process 5.3: Successfully Uploaded Special Active Items into Interface Table ';
            write_log (g_sec);
            write_log (g_line_close);

            /*---------------------------------------
            Item Import Request Getting Started
            ------------------------------------------*/
            BEGIN
               p_retcode := 0;
               /*---------------------------------------
               Apps Initialize
               ------------------------------------------*/

               g_sec := 'Apps Initialize ';

               fnd_global.apps_initialize (
                  user_id        => fnd_global.user_id,
                  resp_id        => fnd_global.resp_id,
                  resp_appl_id   => fnd_global.resp_appl_id);

               g_sec :=
                     'Process 5.4 : Start Submit concurrent Request '
                  || l_program;

               l_req_id :=
                  fnd_request.submit_request (application   => 'INV',
                                              program       => 'INCOIN',
                                              description   => NULL,
                                              start_time    => SYSDATE,
                                              sub_request   => FALSE,
                                              argument1     => 222,
                                              -- Organization id
                                              argument2     => 1,
                                              -- All organizations
                                              argument3     => 1,
                                              -- Validate Items
                                              argument4     => 1,
                                              -- Process Items
                                              argument5     => 1,
                                              -- Delete Processed Rows
                                              argument6     => 1,
                                              -- Process Set (Null for All)
                                              argument7     => 2,
                                              -- Create or Update Items
                                              argument8     => 1 -- Gather Statistics
                                                                );
               COMMIT;

               IF (l_req_id != 0)
               THEN
                  IF fnd_concurrent.wait_for_request (l_req_id,
                                                      v_interval,
                                                      v_max_time,
                                                      v_phase,
                                                      v_status,
                                                      v_dev_phase,
                                                      v_dev_status,
                                                      v_message)
                  THEN
                     v_error_message :=
                           CHR (10)
                        || 'ReqID='
                        || l_req_id
                        || ' DPhase '
                        || v_dev_phase
                        || ' DStatus '
                        || v_dev_status
                        || CHR (10)
                        || ' MSG - '
                        || v_message;

                     -- Error Returned
                     IF v_dev_phase != 'COMPLETE' OR v_dev_status != 'NORMAL'
                     THEN
                        BEGIN
                           g_sec :=
                              'Process 5.5:Process : Error Records Starts to Print in the Log File';
                           write_log (g_line_open);
                           write_log (g_sec);
                           write_log (g_line_close);
                           fnd_file.put_line (
                              fnd_file.LOG,
                                 'ORGANIZATION_ID'
                              || '|'
                              || 'ORGANIZATION_CODE'
                              || '|'
                              || 'ITEM_NUMBER'
                              || '|'
                              || 'ITEM_DESCRIPTION'
                              || '|'
                              || 'ERROR_MESSAGE');

                           FOR error_rec_cur IN error_items_cur
                           LOOP
                              fnd_file.put_line (
                                 fnd_file.LOG,
                                    error_rec_cur.organization_id
                                 || '|'
                                 || error_rec_cur.organization_code
                                 || '|'
                                 || error_rec_cur.segment1
                                 || '|'
                                 || error_rec_cur.description
                                 || '|'
                                 || error_rec_cur.error_message);
                           END LOOP;

                           g_sec :=
                              'Process 5.6 :Updated Error Records in the Log File ';
                           write_log (g_sec);
                           write_log (g_line_close);
                        END;

                        l_statement :=
                              'Error occured in the running of Item import Prgram'
                           || v_error_message
                           || '.';
                        fnd_file.put_line (fnd_file.LOG, l_statement);
                        fnd_file.put_line (fnd_file.output, l_statement);
                     END IF;
                  END IF;
               ELSE
                  l_statement :=
                     'Error occured when submitting Item Import Program';
                  fnd_file.put_line (fnd_file.LOG, l_statement);
                  fnd_file.put_line (fnd_file.output, l_statement);
               END IF;
            END;
         END;
      END IF;

      write_log (g_line_close);
   EXCEPTION
      WHEN OTHERS
      THEN
         l_retcode := 0;
         p_retcode := l_retcode;
         write_log (g_errbuf);
         write_log (g_line_close);
   END update_special_items_status;

   PROCEDURE load_inv_sp_main (errbuf              OUT VARCHAR2,
                               retcode             OUT NUMBER,
                               p_cutoff_date        IN VARCHAR2, -- Added in Revision 1.1
                               p_no_of_days         IN NUMBER,
                               p_process_type       IN VARCHAR2)
   /*************************************************************************
     $Header xwc_inv_sp_items_inact_pkg.Load_inv_sp_main $
     PURPOSE:     6. Main Procedure to Load Inventory Special data from all the sub loads
     REVISIONS:
     Ver    Date         Author                Description
     ------ -----------  ------------------    ----------------
     1.0    17-Nov-14    Gajendra Mavilla     Initial Version
     1.1    05-Oct-15    P.Vamshidhar         TMS#20150930-00038
                                              Added cuttoff date to pickup items
                                              (Creation date cutoff date)
     **************************************************************************/
   IS
      l_retcode   NUMBER := 0;
      p_cutoff_date1 Date;
   BEGIN
      g_sec := 'Process Main : Load INV Special Items main ';

      write_log (g_line_open);
      write_log (g_sec);
      write_log (g_line_close);

      fnd_file.put_line(fnd_file.log,'Date: '||p_cutoff_date);
      p_cutoff_date1 := to_date(p_cutoff_date,'YYYY/MM/DD HH24:MI:SS');  -- Added p_cutoff_date in Rev 1.1

      IF l_retcode = 2
      THEN
         GOTO laststep;
      END IF;

      load_items (l_retcode, p_cutoff_date1); -- Added p_cutoff_date in Rev 1.1

      IF l_retcode = 2
      THEN
         GOTO laststep;
      END IF;

      load_on_ord_qty (l_retcode);

      IF l_retcode = 2
      THEN
         GOTO laststep;
      END IF;

      extract_special_items (l_retcode, p_no_of_days);

      IF l_retcode = 2
      THEN
         GOTO laststep;
      END IF;

      update_buyers (l_retcode);

      IF l_retcode = 2
      THEN
         GOTO laststep;
      END IF;

      update_special_items_status (l_retcode, p_process_type, p_no_of_days);    -- Added additional parameter in Rev 1.1

      IF l_retcode = 2
      THEN
         GOTO laststep;
      END IF;

     <<laststep>>
      IF l_retcode = 2
      THEN
         retcode := 2;
         errbuf := g_errbuf;

         xxcus_error_pkg.xxcus_error_main_api (
            p_called_from         => 'XXWC_INV_SP_INACT_PKG.LOAD_INV_SP_MAIN',
            p_calling             => g_sec,
            p_request_id          => fnd_global.conc_request_id,
            p_ora_error_msg       => SUBSTR (
                                          ' Error_Stack...'
                                       || DBMS_UTILITY.format_error_stack ()
                                       || ' Error_Backtrace...'
                                       || DBMS_UTILITY.format_error_backtrace (),
                                       1,
                                       2000),
            p_error_desc          => SUBSTR (g_errbuf, 1, 240),
            p_distribution_list   => g_dflt_email,
            p_module              => 'INV');
      END IF;
   EXCEPTION
      WHEN OTHERS
      THEN
         xxcus_error_pkg.xxcus_error_main_api (
            p_called_from         => 'XXWC_INV_SP_INACT_PKG.LOAD_INV_SP_MAIN',
            p_calling             => g_sec,
            p_request_id          => fnd_global.conc_request_id,
            p_ora_error_msg       => SUBSTR (
                                          ' Error_Stack...'
                                       || DBMS_UTILITY.format_error_stack ()
                                       || ' Error_Backtrace...'
                                       || DBMS_UTILITY.format_error_backtrace (),
                                       1,
                                       2000),
            p_error_desc          => SUBSTR (g_errbuf, 1, 240),
            p_distribution_list   => g_dflt_email,
            p_module              => 'INV');
   END load_inv_sp_main;
END;
/
