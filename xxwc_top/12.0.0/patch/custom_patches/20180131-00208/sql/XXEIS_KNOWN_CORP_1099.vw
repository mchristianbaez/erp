-------------------------------------------------------------------------------------------
/*****************************************************************************************
  $Header XXEIS.XXEIS_KNOWN_CORP_1099.vw $
  Module Name: Payables
  PURPOSE: HDS AP 1099 Information - Known CORP Vendor
  REVISIONS:
  Ver          Date        Author                Description
  ---------  ----------  ------------------    ----------------
  1.0		 3/21/2018      Siva				TMS#20180131-00208 
******************************************************************************************/
---------------------------------------------------------------------------------------------
CREATE OR REPLACE FORCE VIEW xxeis.xxeis_known_corp_1099
(
   address_line1
  ,address_line2
  ,city
  ,creation_date
  ,last_pay_date
  ,num_1099
  ,phone_number
  ,pos_system
  ,state
  ,type_1099
  ,vendor_name
  ,vendor_name_alt
  ,vendor_number
  ,vendor_site_code
  ,zip
  ,total
  ,non_emp_sum
  ,rent_amt_sum
  ,atty_fees
)
AS
     SELECT o1634014.address_line1 address_line1
           ,o1634014.address_line2 address_line2
           ,o1634014.city city
           ,o1634014.creation_date creation_date
           ,o1634023.last_pay_date last_pay_date
           ,o1634014.num_1099 num_1099
           ,o1634014.phone_number phone_number
           ,o1634014.pos_system pos_system
           ,o1634014.state state
           ,o1634014.type_1099 type_1099
           ,o1634014.vendor_name vendor_name
           ,o1634014.vendor_name_alt vendor_name_alt
           ,o1634014.vendor_number vendor_number
           ,o1634014.vendor_site_code vendor_site_code
           ,o1634014.zip zip
           ,  NVL ( (SUM (o1634014.rent_amt)), 0)
            + NVL (
                 (CASE
                     WHEN ( (SUM (o1634014.sub_amt)) >=
                              (SUM (o1634020.atty_amt)))
                     THEN
                        ( (SUM (o1634014.sub_amt)) - (SUM (o1634020.atty_amt)))
                     ELSE
                        ( (SUM (o1634014.sub_amt)))
                  END)
                ,0)
            + NVL ( (SUM (o1634020.atty_amt)), 0)
               total
           ,CASE
               WHEN ( (SUM (o1634014.sub_amt)) >= (SUM (o1634020.atty_amt)))
               THEN
                  ( (SUM (o1634014.sub_amt)) - (SUM (o1634020.atty_amt)))
               ELSE
                  ( (SUM (o1634014.sub_amt)))
            END
               AS non_emp_sum
           ,SUM (o1634014.rent_amt) AS rent_amt_sum
           ,NVL ( (SUM (o1634020.atty_amt)), 0) atty_fees
       FROM (  SELECT TRUNC (vnd.creation_date) creation_date
                     ,vnd.num_1099
                     ,vnd.type_1099
                     ,vnd.segment1 vendor_number
                     ,sts.vendor_site_code
                     ,vnd.vendor_name
                     ,vnd.vendor_name_alt
                     ,sts.address_line1
                     ,sts.address_line2
                     ,sts.city
                     ,sts.state
                     ,SUBSTR (sts.zip, 1, 5) zip
                     ,sts.attribute1 pos_system
                     ,contact.phone_number
                     ,vnd.vendor_type_lookup_code vendor_type
                     ,SUM (
                         CASE
                            WHEN     ivc.invoice_num LIKE '%RENT%'
                                 AND (   vnd.vendor_name LIKE '% LLC%'
                                      OR vnd.vendor_name LIKE '% LP%'
                                      OR (   vnd.vendor_name NOT LIKE '% INC'
                                          OR vnd.vendor_name NOT LIKE
                                                '% CORPORATION%'
                                          OR vnd.vendor_name NOT LIKE
                                                '% INCORPORATE%'
                                          OR vnd.vendor_name NOT LIKE '% LTD%'
                                          OR vnd.vendor_name NOT LIKE '% LLP%'))
                            THEN
                               pay.amount
                            WHEN     vnd.type_1099 = 'MISC1'
                                 AND (   vnd.vendor_name LIKE '% LLC%'
                                      OR vnd.vendor_name LIKE '% LP%'
                                      OR (   vnd.vendor_name NOT LIKE '% INC'
                                          OR vnd.vendor_name NOT LIKE
                                                '% CORPORATION%'
                                          OR vnd.vendor_name NOT LIKE
                                                '% INCORPORATE%'
                                          OR vnd.vendor_name NOT LIKE '% LTD%'
                                          OR vnd.vendor_name NOT LIKE '% LLP%'))
                            THEN
                               pay.amount
                            ELSE
                               0
                         END)
                         rent_amt
                     ,SUM (
                         CASE
                            WHEN ivc.invoice_num LIKE '%RENT%'
                            THEN
                               0
                            WHEN (   vnd.vendor_name LIKE '% LLC%'
                                  OR vnd.vendor_name LIKE '% LTD%'
                                  OR vnd.vendor_name LIKE '% LLP%'
                                  OR vnd.vendor_name LIKE '% INC'
                                  OR vnd.vendor_name LIKE '% INCORPORATE%'
                                  OR vnd.vendor_name LIKE '% LP%'
                                  OR vnd.vendor_name LIKE '% CORPORATION%')
                            THEN
                               0
                            ELSE
                               pay.amount
                         END)
                         sub_amt
                 FROM ap.ap_checks_all chk
                     ,ap.ap_invoice_payments_all pay
                     ,ap.ap_invoices_all ivc
                     ,ap.ap_suppliers vnd
                     ,ap.ap_supplier_sites_all sts
                     ,(SELECT pvs.vendor_site_id
                             , (CASE
                                   WHEN TRIM (pvs.area_code || pvs.phone)
                                           IS NOT NULL
                                   THEN
                                      TRIM (pvs.area_code || pvs.phone)
                                   WHEN TRIM (pvs.area_code || pvs.phone) IS NULL
                                   THEN
                                      NVL (
                                         (SELECT NVL (
                                                    TRIM (
                                                       pvc.area_code || pvc.phone)
                                                   ,'No Phone Available')
                                                    contact_phone
                                            FROM ap.ap_supplier_contacts pvc
                                           WHERE     pvc.vendor_site_id =
                                                        pvs.vendor_site_id
                                                 AND TRIM (
                                                           pvc.area_code
                                                        || pvc.phone)
                                                        IS NOT NULL
                                                 AND ROWNUM = 1)
                                        ,'No Phone Available')
                                   ELSE
                                      'No Phone Available'
                                END)
                                 phone_number
                         FROM ap.ap_supplier_sites_all pvs) contact
                WHERE     chk.check_id = pay.check_id
                      AND pay.invoice_id = ivc.invoice_id
                      AND chk.vendor_id = vnd.vendor_id
                      AND (    chk.vendor_id = sts.vendor_id
                           AND chk.vendor_site_id = sts.vendor_site_id)
                      AND sts.vendor_site_id = contact.vendor_site_id
                      AND TRUNC (chk.check_date) BETWEEN xxeis.xxeis_fin_com_util_pkg.get_from_date
                                                     AND xxeis.xxeis_fin_com_util_pkg.get_to_date
                      AND chk.void_date IS NULL
					  --AND chk.ce_bank_acct_use_id IN (10023, 10026) --commented for version 1.0
                      AND chk.ce_bank_acct_use_id IN (10700, 10721) --added for version 1.0
                      AND ivc.attribute11 NOT IN ('CKRF', 'CKRQ')
                      AND sts.pay_group_lookup_code NOT IN ('TAX', 'UTILITIES')
             GROUP BY TRUNC (vnd.creation_date)
                     ,vnd.num_1099
                     ,vnd.type_1099
                     ,vnd.segment1
                     ,sts.vendor_site_code
                     ,vnd.vendor_name
                     ,vnd.vendor_name_alt
                     ,sts.address_line1
                     ,sts.address_line2
                     ,sts.city
                     ,sts.state
                     ,SUBSTR (sts.zip, 1, 5)
                     ,sts.attribute1
                     ,contact.phone_number
                     ,vnd.vendor_type_lookup_code) o1634014
           ,(  SELECT pv.vendor_name
                     ,pvs.vendor_site_code
                     ,NVL (SUM (aid.amount), 0) atty_amt
                 FROM ap.ap_invoice_distributions_all aid
                     ,gl.gl_code_combinations gcc
                     ,ap.ap_invoices_all ai
                     ,ap.ap_suppliers pv
                     ,ap.ap_supplier_sites_all pvs
                     ,ap.ap_invoice_payments_all aip
                     ,ap.ap_checks_all ac
                WHERE     aid.invoice_id = ai.invoice_id
                      AND ai.invoice_id = aip.invoice_id
                      AND aip.check_id = ac.check_id
                      AND ai.vendor_site_id = pvs.vendor_site_id
                      AND pvs.vendor_id = pv.vendor_id
                      AND aid.dist_code_combination_id = gcc.code_combination_id
                      AND gcc.segment4 IN
                             ('642010'
                             ,'642020'
                             ,'642100'
                             ,'642101'
                             ,'642102'
                             ,'642103'
                             ,'642104'
                             ,'642105'
                             ,'642106'
                             ,'642107'
                             ,'642108'
                             ,'642109'
                             ,'642110'
                             ,'642111'
                             ,'642112'
                             ,'642113'
                             ,'642114'
                             ,'642115'
                             ,'642116'
							 ,'256901'
							 ,'256902') --added for version 1.0
                      AND ac.void_date IS NULL
					  --AND ac.ce_bank_acct_use_id IN (10023, 10026)--commented for version 1.0
                      AND ac.ce_bank_acct_use_id IN (10700, 10721) --added for version 1.0
                      AND TRUNC (ac.check_date) BETWEEN xxeis.xxeis_fin_com_util_pkg.get_from_date
                                                    AND xxeis.xxeis_fin_com_util_pkg.get_to_date
                      AND (   pv.vendor_name NOT LIKE '% LLC%'
                           OR pv.vendor_name NOT LIKE '% LTD%'
                           OR pv.vendor_name NOT LIKE '% LLP%'
                           OR pv.vendor_name NOT LIKE '% INC'
                           OR pv.vendor_name NOT LIKE '% INCORPORATE%'
                           OR pv.vendor_name NOT LIKE '% LP%'
                           OR pv.vendor_name NOT LIKE '% CORPORATION%')
                      AND ai.attribute11 NOT IN ('CKRF', 'CKRQ')
                      AND pvs.pay_group_lookup_code NOT IN ('TAX', 'UTILITIES')
             GROUP BY pv.vendor_name, pvs.vendor_site_code) o1634020
           ,(  SELECT ac.vendor_name
                     ,pvs.vendor_site_code
                     ,MAX (ac.check_date) last_pay_date
                 FROM ap.ap_checks_all ac, ap.ap_supplier_sites_all pvs
                WHERE     ac.vendor_site_id = pvs.vendor_site_id
                      AND ac.void_date IS NULL
					  --AND ac.ce_bank_acct_use_id IN (10023, 10026) --commented for version 1.0
                      AND ac.ce_bank_acct_use_id IN (10700, 10721) --added for version 1.0
                      AND TRUNC (ac.check_date) BETWEEN xxeis.xxeis_fin_com_util_pkg.get_from_date
                                                    AND xxeis.xxeis_fin_com_util_pkg.get_to_date
                      AND (   ac.vendor_name NOT LIKE '% LLC%'
                           OR ac.vendor_name NOT LIKE '% LTD%'
                           OR ac.vendor_name NOT LIKE '% LLP%'
                           OR (    ac.vendor_name NOT LIKE '% INC%'
                               AND ac.vendor_name LIKE '% INCOM%'
                               AND ac.vendor_name LIKE '% INCAR%')
                           OR ac.vendor_name NOT LIKE '% LP%'
                           OR ac.vendor_name NOT LIKE '% CORPORATION%')
             GROUP BY ac.vendor_name, pvs.vendor_site_code) o1634023
      WHERE     (    (    o1634014.vendor_name = o1634020.vendor_name(+)
                      AND o1634014.vendor_site_code =
                             o1634020.vendor_site_code(+))
                 AND (    o1634014.vendor_name = o1634023.vendor_name
                      AND o1634014.vendor_site_code = o1634023.vendor_site_code))
            AND o1634014.vendor_type IN ('EXPENSE', 'IWO-EXP', 'REAL ESTATE') --Changed for version 1.0
            AND (o1634014.type_1099 IS NOT NULL)
   GROUP BY o1634014.address_line1
           ,o1634014.address_line2
           ,o1634014.city
           ,o1634014.creation_date
           ,o1634023.last_pay_date
           ,o1634014.num_1099
           ,o1634014.phone_number
           ,o1634014.pos_system
           ,o1634014.state
           ,o1634014.type_1099
           ,o1634014.vendor_name
           ,o1634014.vendor_name_alt
           ,o1634014.vendor_number
           ,o1634014.vendor_site_code
           ,o1634014.zip
     HAVING ( (  NVL ( (SUM (o1634014.rent_amt)), 0)
               + NVL (
                    (CASE
                        WHEN ( (SUM (o1634014.sub_amt)) >=
                                 (SUM (o1634020.atty_amt(+))))
                        THEN
                           (  (SUM (o1634014.sub_amt))
                            - (SUM (o1634020.atty_amt(+))))
                        ELSE
                           ( (SUM (o1634014.sub_amt)))
                     END)
                   ,0)
               + NVL ( (SUM (o1634020.atty_amt(+))), 0)) >= 600);




