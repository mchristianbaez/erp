 /********************************************************************************
  FILE NAME: xxwc.XXWC_OM_SO_LINE_PICK_AUD_TBL
  PROGRAM TYPE: Alter Table script

  PURPOSE: To fulfill PQC changes requirement

  HISTORY
  ===============================================================================
  VERSION DATE          AUTHOR(S)       DESCRIPTION
  ------- -----------   --------------- -----------------------------------------
  1.0     09/28/2018    Pattabhi Avula  TMS#20180928-00001  - PQC Load testing PROD code compile in QA
  ********************************************************************************/
-- DROP column 
ALTER TABLE XXWC.XXWC_OM_SO_LINE_PICK_AUD_TBL  DROP(AVAILABLE_QTY)
/