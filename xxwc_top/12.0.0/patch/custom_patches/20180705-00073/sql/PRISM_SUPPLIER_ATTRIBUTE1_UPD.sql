--******************************************************************************
--*File Name: PRISM_SUPPLIER_ATTRIBUTE1_UPD.ctl                                * 
--*Author: P.Vamshidhar                                                        *
--*Creation Date: 05-Jul-2018 - Vamshi                                         *
--*Data Source: AH Harries                                                     *
--*           TMS#20180705-00073 - Prism Suppliers Numbers Reset               *
--******************************************************************************

SET SERVEROUT ON

BEGIN
EXECUTE IMMEDIATE 'TRUNCATE TABLE AP.AP_SUPPLIERS_INT';
EXECUTE IMMEDIATE 'TRUNCATE TABLE AP.AP_SUPPLIER_SITES_INT';
EXECUTE IMMEDIATE 'TRUNCATE TABLE AP.AP_SUP_SITE_CONTACT_INT';
EXECUTE IMMEDIATE 'CREATE TABLE XXWC.AP_SUPPLIERS_PRISM_B AS SELECT *FROM AP.AP_SUPPLIERS';
UPDATE AP.AP_SUPPLIERS SET ATTRIBUTE1=NULL WHERE ATTRIBUTE1 IS NOT NULL;
DBMS_OUTPUT.PUT_LINE('Number of Records Updated:'||SQL%ROWCOUNT);
commit;
EXCEPTION
WHEN OTHERS THEN
DBMS_OUTPUT.PUT_LINE('Error Occured: '||sqlerrm);
END;
/
