/*************************************************************************
 Copyright (c) 2012 HD Supply
 All rights reserved.
**************************************************************************
   $Header xxwc.XXWC_OM_ORD_LN_MV_N1 $
  Module Name: xxwc.XXWC_OM_ORD_LN_MV_N1

  PURPOSE:

  REVISIONS:
  -- VERSION DATE          AUTHOR(S)    TMS TASK              DESCRIPTION
  -- ------- -----------   ------------ ---------             -------------------------
  -- 1.0     02-May-2017   Rakesh Patel TMS#20170501-00147    Mobile pricing app 0.2 UC4 job for XXWC_OM_ORD_LN_MV mv refresh
**************************************************************************/
CREATE INDEX "XXWC"."XXWC_OM_ORD_LN_MV_N1" ON "APPS"."XXWC_OM_ORD_LN_MV" ("INVENTORY_ITEM_ID"
                                                                        , "FLOW_STATUS_CODE"
                                                                        , "LINE_CATEGORY_CODE"
                                                                        , "LINE_CREATION_DATE"
                                                                        , "SOLD_TO_ORG_ID" 
                                                                        , "SHIP_TO_ORG_ID");

