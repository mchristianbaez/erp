/********************************************************************************
   $Header XXWC_B2B_CONFIG_TBL_N1.sql $
   Module Name: XXWC_B2B_CONFIG_TBL_N1

   PURPOSE:   This index to improve performance when quering the data from XXWC_B2B_CONFIG_TBL_N1 table

   REVISIONS:
   Ver        Date        Author                     Description
   ---------  ----------  ---------------         -------------------------
   1.0        11/07/2016  Rakesh Patel            TMS# 20161010-00244 Change OM Printer form to reflect B2B Setup page changes for Customer Level configuration
********************************************************************************/
DROP INDEX XXWC.XXWC_B2B_CONFIG_TBL_N1;

CREATE INDEX XXWC.XXWC_B2B_CONFIG_TBL_N1 ON XXWC.XXWC_B2B_CONFIG_TBL
(PARTY_ID,CUST_ACCOUNT_ID,SITE_USE_ID, START_DATE_ACTIVE, END_DATE_ACTIVE);

