/**************************************************************************************************************
  $Header XXEIS.EIS_XXWC_OM_CANC_ORDERS_V $
  Module Name : Order Management
  PURPOSE	  : Cancelled Cash Sales Orders
  TMS Task Id : 20160824-00250  
  REVISIONS   :
  VERSION DATE               AUTHOR(S)       DESCRIPTION
  ------- -----------------  --------------- -----------------------------------------
  1.1	  07-Nov-2016		 Siva   		 TMS#20160824-00250  --Performance Tuning
**************************************************************************************************************/
DROP VIEW XXEIS.EIS_XXWC_OM_CANC_ORDERS_V;

CREATE OR REPLACE VIEW xxeis.eis_xxwc_om_canc_orders_v
(
   branch
  ,ship_from_org_id
  ,organization_code
  ,branch_name
  ,region
  ,attribute9
  ,district
  ,attribute8
  ,customer_num
  ,customer_name
  ,ordeer_number
  ,order_type
  ,order_date
  ,created_time
  ,created_by_id
  ,created_by_name
  ,line_number
  ,sku
  ,sku_description
  ,qty_cancelled
  ,uom
  ,unit_sell_price
  ,extended_sell_price
  ,cancelled_date
  ,cancelled_time
  ,last_update_date
  ,cancelled_by_id
  ,cancelled_by_name
  ,amt_of_applied_cash
  ,currency
  ,order_count_indicator
  ,payment_type_code
)
AS
   SELECT oh.ship_from_org_id branch
         ,oh.ship_from_org_id ship_from_org_id
         --,ood.organization_code organization_code  --Commented for version 1.1
         --,ood.organization_name branch_name        --Commented for version 1.1
		 ,mtp.organization_code organization_code    --Added for version 1.1
         ,ood.name branch_name						 --Added for version 1.1
         ,mtp.attribute9 region
         ,mtp.attribute9 attribute9
         ,mtp.attribute8 district
         ,mtp.attribute8 attribute8
         ,hzp.party_number customer_number
         ,hzp.party_name customer_name
         ,oh.order_number order_number
         ,otyp.name order_type
         ,TO_CHAR (oh.ordered_date, 'DD-MON-YYYY') order_date
         ,TO_CHAR (ordered_date, 'HH24:MI:SS') created_time
         ,fu.user_name created_by_id                    --Added by Mahender For TMS#20150528-00241 on 06/10/15
         ,fu.description created_by_name                --Added by Mahender For TMS#20150528-00241 on 06/10/15
         /*, (SELECT user_name
              FROM fnd_user
             WHERE user_id = (SELECT created_by
                                FROM oe_order_headers_all
                               WHERE header_id = oh.header_id))
             "Created by ID"
             ,  --         XXEIS.eis_rs_xxwc_ord_can_pkg.Created_by_id (oh.header_id) "Created by ID"
         ,(SELECT description
              FROM fnd_user
             WHERE user_id = (SELECT created_by
                                FROM oe_order_headers_all
                               WHERE header_id = oh.header_id))
             "Created by Name"
             , --         XXEIS.eis_rs_xxwc_ord_can_pkg.Created_by_Name (oh.header_id) "Created by Name"*/
         --Commeneted by Mahender For TMS#20150528-00241 on 06/10/15
         ,ol.line_number || '.' || ol.shipment_number line_number
         ,ol.ordered_item sku
         , (SELECT mtl.description
              FROM mtl_system_items_b mtl
             --, oe_order_lines_all ool   --Commeneted by Mahender For TMS#20150528-00241 on 06/10/15
             WHERE --mtl.inventory_item_id = ool.inventory_item_id --Added by Mahender For TMS#20150528-00241 on 06/10/15
                   mtl.inventory_item_id = ol.inventory_item_id --Added by Mahender For TMS#20150528-00241 on 06/10/15
                                                               AND mtl.organization_id = ol.ship_from_org_id -- AND ool.line_id = ol.line_id  --Commeneted by Mahender For TMS#20150528-00241 on 06/10/15
                                                                                                            --AND ool.header_id = oh.header_id  --Commeneted by Mahender For TMS#20150528-00241 on 06/10/15
          )
             sku_description
         --         XXEIS.eis_rs_xxwc_ord_can_pkg.item_Desc (oh.order_number, ol.ship_from_org_id) "SKU Description",
         ,ol.cancelled_quantity qty_cancelled
         ,ol.order_quantity_uom uom
         ,ol.unit_selling_price unit_sell_price
         , (ol.cancelled_quantity * ol.unit_selling_price) extended_sell_price
         ,TO_CHAR (oh.last_update_date, 'DD-MON-YYYY') cancelled_date
         ,TO_CHAR (oh.last_update_date, 'HH24:MI:SS') cancelled_time
         ,oh.last_update_date last_update_date
         ,fu1.user_name cancelled_by_id                 --Added by Mahender For TMS#20150528-00241 on 06/10/15
         ,fu1.description cancelled_by_name             --Added by Mahender For TMS#20150528-00241 on 06/10/15
         /* , (SELECT user_name
               FROM fnd_user
              WHERE user_id = (SELECT last_updated_by
                                 FROM oe_order_headers_all
                                WHERE header_id = oh.header_id))
              "Cancelled by ID"
              --       ,  XXEIS.eis_rs_xxwc_ord_can_pkg.Cancelled_by_ID (oh.header_id) "Cancelled by ID"
          ,(SELECT description
               FROM fnd_user
              WHERE user_id = (SELECT last_updated_by
                                 FROM oe_order_headers_all
                                WHERE header_id = oh.header_id))
              "Cancelled by Name1"
--      ,   XXEIS.eis_rs_xxwc_ord_can_pkg.Cancelled_by_Name (oh.header_id) "Cancelled by Name"              */
         --Commeneted by Mahender For TMS#20150528-00241 on 06/10/15
         /*(SELECT DISTINCT NVL (acra.amount, 0)
              FROM ar_cash_receipts_all acra,
                   ar_receivable_applications_all araa,
                   oe_order_headers_all ooha,
                   oe_order_lines_all oola
             WHERE     acra.cash_receipt_id = araa.cash_receipt_id
                   AND araa.application_ref_id = oola.header_id
                   AND araa.application_ref_id = oh.header_id
                   AND oola.header_id = ooha.header_id
                   AND ol.line_id = (SELECT MIN (line_id)
                                       FROM oe_order_lines_all
                                      WHERE header_id = oh.header_id)
                   --AND oola.line_id = 17083318
                   --and araa.application_ref_id = 10398580
                   AND ooha.header_id = oh.header_id)*/
         --Commented by Mahesh 18-Aug-2014, TMS#20140807-00148
         , (SELECT NVL (SUM (araa.amount_applied), 0)
              FROM ar_receivable_applications_all araa, ra_customer_trx_all ract --Added by Mahender For TMS#20150528-00241 on 06/10/15
             WHERE     araa.customer_trx_id = ract.customer_trx_id --Added by Mahender For TMS#20150528-00241 on 06/10/15
                   AND ract.interface_header_attribute1 = TO_CHAR (oh.order_number) --Added by Mahender For TMS#20150528-00241 on 06/10/15
                   --            AND araa.application_ref_id = oh.header_id  --Commeneted by Mahender For TMS#20150528-00241 on 06/10/15
                   AND ol.line_number = 1
                   AND ol.shipment_number = 1)
             amt_of_applied_cash
         ,oh.transactional_curr_code
         , (SELECT 1
              FROM DUAL
             WHERE ol.line_id = (SELECT MIN (line_id)
                                   FROM oe_order_lines_all
                                  WHERE header_id = oh.header_id))
             order_count_indicator
         ,oep.payment_type_code payment_type     -- Added by Sowmya KM for TMS # 20150211-00007 on 31March2015
     FROM oe_order_headers_all oh
         ,oe_order_lines_all ol       --,oe_payments oep --Commented by Mahesh 18-Aug-2014, TMS#20140807-00148
         ,oe_payments oep                       --  Added by Sowmya KM for TMS # 20150211-00007 on 31March2015
		 ,mtl_parameters mtp         
	     --,org_organization_definitions ood  --Commented for version 1.1
         ,hr_all_organization_units ood  --Added for version 1.1
         ,hz_cust_accounts hca
         ,hz_parties hzp
         --,oe_order_types_v otyp   --Commented for version 1.1
         ,oe_transaction_types_tl otyp --Added for version 1.1
         ,fnd_user fu
         ,fnd_user fu1                                  --Added by Mahender For TMS#20150528-00241 on 06/10/15
    WHERE     oh.header_id = ol.header_id
          AND oh.header_id = oep.header_id(+) -- Uncommented by Sowmya Km on 31March2015 for TMS # 20150211-00007 --Commented by Mahesh 18-Aug-2014, TMS#20140807-00148
          AND ol.ship_from_org_id = mtp.organization_id(+)
          AND mtp.organization_id = ood.organization_id --Added by Mahender For TMS#20150528-00241 on 06/10/15
          AND oh.sold_to_org_id = hca.cust_account_id
          AND hca.party_id = hzp.party_id
		  --AND oh.order_type_id  = otyp.order_type_id     --Commented for version 1.1        
          AND oh.order_type_id  = otyp.transaction_type_id --Added for version 1.1
		  AND otyp.language(+)   = USERENV('LANG')		--Added for version 1.1  
		  AND oh.created_by = fu.user_id
          AND oh.last_updated_by = fu1.user_id          --Added by Mahender For TMS#20150528-00241 on 06/10/15
          --AND oh.ship_from_org_id = ood.organization_id  --Commeneted By Mahender For TMS#20150528-00241 on 06/10/15
          AND oh.flow_status_code = 'CANCELLED'
          AND (oep.payment_type_code IN ('CASH', 'CHECK') OR oep.payment_type_code IS NULL) -- Added by Sowmya KM for TMS # 20150211-00007 on 31March2015
          --AND oep.payment_type_code = 'CASH' --Commented by Mahesh 18-Aug-2014, TMS#20140807-00148
          /*AND EXISTS
                           (SELECT '1'
                              FROM oe_payments oep
                             WHERE     oep.header_id = oh.header_id) -- Commented for TMS # 20150211-00007 --Added by Mahesh 18-Aug-2014, TMS#20140807-00148*/
          --ORDER BY 13, 11, 17; --Commented by Mahesh 18-Aug-2014, TMS#20140807-00148
          /*AND oh.last_update_date >=
                   TO_DATE (
                      TO_CHAR (xxeis.eis_rs_xxwc_com_util_pkg.get_date_from
                              ,xxeis.eis_rs_utility.get_date_format || ' HH24:MI:SS')
                     ,xxeis.eis_rs_utility.get_date_format || ' HH24:MI:SS')
                 + 0.25                                 --Added by Mahender For TMS#20150528-00241 on 06/10/15
          AND oh.last_update_date <=
                   TO_DATE (
                      TO_CHAR (xxeis.eis_rs_xxwc_com_util_pkg.get_date_to
                              ,xxeis.eis_rs_utility.get_date_format || ' HH24:MI:SS')
                     ,xxeis.eis_rs_utility.get_date_format || ' HH24:MI:SS')
                 + 1.25 */ --Commented for version 1.1                               
          AND TRUNC(oh.last_update_date) >= xxeis.eis_rs_xxwc_com_util_pkg.get_date_from --Added for version 1.1
		  AND TRUNC(oh.last_update_date) <= xxeis.eis_rs_xxwc_com_util_pkg.get_date_to  --Added for version 1.1
/
