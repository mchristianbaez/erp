/*
 TMS: 20151109-00150    
 Date: 10/20/2015
 Notes: data fix script to process month end transactions
*/

SET SERVEROUTPUT ON SIZE 100000;
SET VERIFY OFF;


update apps.wsh_delivery_details
set OE_INTERFACED_FLAG='Y'
where delivery_detail_id =12966225
and SOURCE_LINE_ID=58325675
and source_header_id=35517137;
		  
--1 row expected to be updated

commit;

/