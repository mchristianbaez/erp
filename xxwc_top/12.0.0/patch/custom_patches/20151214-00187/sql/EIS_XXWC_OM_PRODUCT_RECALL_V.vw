CREATE OR REPLACE FORCE VIEW XXEIS.EIS_XXWC_OM_PRODUCT_RECALL_V (VENDOR_NAME, 
VENDOR_NUMBER, 
ONHAND_QTY, 
ORDERED_QTY, 
ITEM_NUMBER, 
ITEM_DESCRIPTION, 
SERIAL_NUMBER, 
LOT_NUMBER, 
ONHAND_LOCATION, 
CUSTOMER_SOLD_TO, 
SALES_ORDER_NUMBER, 
LINE_NUMBER, 
ORDERED_DATE, 
SOLD_FROM_WAREHOUSE, 
LINE_SHIPMENT, 
SOLD_ONHAND, 
HEADER_ID, 
LINE_ID, 
PARTY_ID, 
ORGANIZATION_ID, 
INVENTORY_ITEM_ID, 
MSI_ORGANIZATION_ID, 
DELIVERY_DETAIL_ID)
  /**************************************************************************************************************
  $Header XXEIS.EIS_XXWC_OM_PRODUCT_RECALL_V.vw $
  Module Name: Order Management
  PURPOSE: View for EIS Report Product Recall Report
  TMS Task Id : NA
  REVISIONS:
  Ver        Date        Author                Description
  ---------  ----------  ------------------    ----------------
  1.0        NA			 NA                    Initial Version
  1.1  	     12/21/2015  Mahender Reddy        20151214-00187 
  **************************************************************************************************************/
 AS 
  SELECT xxeis.eis_rs_xxwc_com_util_pkg.get_vendor_name(msi.inventory_item_id,msi.organization_id) vendor_name,
    XXEIS.EIS_RS_XXWC_COM_UTIL_PKG.GET_VENDOR_NUMBER(MSI.INVENTORY_ITEM_ID,MSI.ORGANIZATION_ID) VENDOR_NUMBER,
    NULL ONHAND_QTY,
    NVL(ol.ordered_quantity,0) Ordered_Qty,
    Msi.Segment1 Item_Number ,
    MSI.DESCRIPTION ITEM_DESCRIPTION,
    --wdd.serial_number,
    CASE
      WHEN UPPER(wdd.source_header_type_name) LIKE 'STANDARD ORDER'
      THEN WDD.ATTRIBUTE1
      WHEN UPPER(wdd.source_header_type_name) IN( 'WC SHORT TERM RENTAL','WC LONG TERM RENTAL')
      THEN wdd.serial_number
    END serial_number,
    Wdd.Lot_Number,
    NULL onhand_location,
    NVL(hca.account_name,hzp.party_name) customer_sold_to,
    oh.order_number sales_order_number,
    DECODE (OL.OPTION_NUMBER,'',(OL.LINE_NUMBER
    ||'.'
    ||OL.SHIPMENT_NUMBER),(OL.LINE_NUMBER
    ||'.'
    ||OL.SHIPMENT_NUMBER
    ||'.'
    ||ol.option_number)) line_number,
    decode(otl.name, 'COUNTER LINE', nvl(trunc(SCHEDULE_SHIP_DATE),trunc(ol.creation_date)),NVL(TRUNC(ol.actual_shipment_date),trunc(ol.creation_date) ))ORDERED_DATE,
    mtp.Organization_Code Sold_From_Warehouse,
    ol.line_number
    ||'.'
    ||ol.shipment_number line_shipment,
    'sold' sold_onhand,
    ---Primary Keys
    oh.header_id ,
    ol.line_id ,
    hzp.party_id ,
    mtp.organization_id ,
    msi.inventory_item_id ,
    msi.organization_id msi_organization_id ,
    wdd.delivery_detail_id
    --descr#flexfield#start
    --descr#flexfield#end
    --gl#accountff#start
    --gl#accountff#end
  FROM oe_order_headers oh,
    oe_order_lines ol,
    hz_cust_accounts hca,
    hz_parties hzp,
    mtl_parameters mtp,
    hr_operating_units hrou,
    mtl_system_items_kfv msi,
    oe_transaction_types_vl ott,
    OE_TRANSACTION_TYPES_VL otl,
    wsh_delivery_details wdd
  WHERE ol.header_id            = oh.header_id
  AND oh.sold_to_org_id         = hca.cust_account_id(+)
  AND hca.party_id              = hzp.party_id(+)
  AND ol.ship_from_org_id       = mtp.organization_id(+)
  --AND hrou.organization_id      = ol.sold_from_org_id(+)   -- Commented for Ver 1.1
  AND hrou.organization_id(+)      = ol.sold_from_org_id -- Added for Ver 1.1
  AND msi.organization_id       =ol.SHIP_from_org_id
  AND msi.inventory_item_id     =ol.inventory_item_id
  AND oh.order_type_id          = ott.transaction_type_id
  AND ol.line_type_id           = otl.transaction_type_id
  AND wdd.source_header_id(+)   = ol.header_id
  AND wdd.source_line_id(+)     = ol.line_id
  AND NVL(wdd.source_code,'OE') = 'OE'
  AND ol.flow_status_code       ='CLOSED'
  --AND msi.item_type NOT        IN('NON-STOCK','RENTAL','AOC','PTO','RE_RENT')  -- Commented for Ver 1.1
   AND msi.item_type NOT        IN('RENTAL','AOC','PTO','RE_RENT')   -- Added for Ver 1.1
  AND EXISTS
    (SELECT 1
    FROM XXEIS.EIS_ORG_ACCESS_V
    WHERE ORGANIZATION_ID = MSI.ORGANIZATION_ID
    )
UNION
  SELECT xxeis.eis_rs_xxwc_com_util_pkg.get_vendor_name(msi.inventory_item_id,msi.organization_id) vendor_name,
    xxeis.eis_rs_xxwc_com_util_pkg.get_vendor_number(msi.inventory_item_id,msi.organization_id) vendor_number,
    SUM(moqd.transaction_quantity) ONHAND_QTY,
    NULL Ordered_Qty,
    Msi.Segment1 Item_Number ,
    Msi.Description Item_Description,
    Mut.Serial_Number Serial_Number,
    mtln.lot_number Lot_Number,
    Mtp.Organization_Code Onhand_Location,
    NULL CUSTOMER_SOLD_TO,
    NULL sales_order_number,
    NULL LINE_NUMBER,
    NULL ordered_date,
    NULL Sold_From_Warehouse,
    -- MAX(Moqd.date_received) ordered_date,
    --Mtp.Organization_Code Sold_From_Warehouse,
    NULL line_shipment,
    'onhand' sold_onhand,
    ---Primary Keys
    NULL header_id ,
    NULL line_id ,
    NULL PARTY_ID ,
    NULL organization_id ,
    msi.inventory_item_id ,
    MSI.ORGANIZATION_ID MSI_ORGANIZATION_ID ,
    NULL DELIVERY_DETAIL_ID
    --descr#flexfield#start
    --descr#flexfield#end
    --gl#accountff#start
    --gl#accountff#end
  FROM Mtl_System_Items_Kfv Msi,
    Mtl_Onhand_Quantities_Detail Moqd ,
    Mtl_Unit_Transactions Mut,
    MTL_TRANSACTION_LOT_NUMBERS MTLN,
    mtl_parameters mtp,
    gl_period_statuses gps,
    org_organization_definitions ood
  WHERE 1                      =1
  AND Msi.Inventory_Item_Id    =Moqd.Inventory_Item_Id
  AND Msi.Organization_Id      =Moqd.Organization_Id
  AND Mut.Inventory_Item_Id(+) =Moqd.Inventory_Item_Id
  AND Mut.Organization_Id(+)   =Moqd.Organization_Id
  AND Mut.Transaction_Id (+)   =Moqd.Update_Transaction_Id
  AND Mtln.Inventory_Item_Id(+)=Moqd.Inventory_Item_Id
  AND Mtln.Organization_Id (+) =Moqd.Organization_Id
  AND Mtln.Transaction_Id (+)  =Moqd.Update_Transaction_Id
  AND mtp.organization_id      =msi.organization_id
  AND OOD.SET_OF_BOOKS_ID      =GPS.SET_OF_BOOKS_ID
  AND MSI.ORGANIZATION_ID      =OOD.ORGANIZATION_ID
  AND GPS.APPLICATION_ID       =101
  AND TRUNC(sysdate) BETWEEN TRUNC(GPS.START_DATE) AND TRUNC(GPS.END_DATE)
--  AND msi.item_type NOT IN('NON-STOCK','RENTAL','AOC','PTO','RE_RENT') -- Commented for Ver 1.1
  AND msi.item_type NOT IN('RENTAL','AOC','PTO','RE_RENT') -- Added for Ver 1.1
  AND EXISTS
    (SELECT 1
    FROM XXEIS.EIS_ORG_ACCESS_V
    WHERE organization_id = msi.organization_id
    )
  GROUP BY xxeis.eis_rs_xxwc_com_util_pkg.get_vendor_name(msi.inventory_item_id,msi.organization_id) ,
    xxeis.eis_rs_xxwc_com_util_pkg.get_vendor_number(msi.inventory_item_id,msi.organization_id) ,
    Msi.Segment1 ,
    Msi.Description ,
    Mut.Serial_Number ,
    mtln.lot_number ,
    Mtp.Organization_Code ,
    NULL,
    NULL ,
    NULL ,
    NULL ,
    NULL ,
    Mtp.Organization_Code ,
    NULL ,
    'onhand' ,
    NULL ,
    NULL ,
    NULL ,
    msi.inventory_item_id ,
    msi.organization_id ,
    NULL;
/