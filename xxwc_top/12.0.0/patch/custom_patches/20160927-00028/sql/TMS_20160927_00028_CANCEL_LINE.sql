/*************************************************************************
  $Header TMS_20160927_00028_CANCEL_LINE.sql $
  Module Name: TMS_20160927_00028  Data Fix script for I675908

  PURPOSE: Data fix script for I675908--No permanent fix in process (But patch available per oracle)

  REVISIONS:
  Ver        Date         Author                Description
  ---------  -----------  ------------------    --------------------------
  1.0        03-OCT-2016  Niraj K Ranjan        TMS#20160927-00028

**************************************************************************/ 
SET SERVEROUTPUT ON SIZE 1000000
DECLARE
BEGIN
   DBMS_OUTPUT.put_line ('TMS: 20160927-00028    , Before Update');

   update apps.oe_order_lines_all
   set FLOW_STATUS_CODE='CANCELLED',
   CANCELLED_FLAG='Y'
   where line_id = 75698323
   and header_id= 46265723;
   
   DBMS_OUTPUT.put_line (
         'TMS: 20160927-00028  Sales order lines updated (Expected:1): '
      || SQL%ROWCOUNT);

   COMMIT;

   DBMS_OUTPUT.put_line ('TMS:  20160927-00028    , End Update');
EXCEPTION
   WHEN OTHERS
   THEN
      ROLLBACK;
      DBMS_OUTPUT.put_line ('TMS:  20160927-00028 , Errors : ' || SQLERRM);
END;
/
