/******************************************************************************
   NAME:       XXWC_OM_CSP_DM_MAINT_N1_IDX.sql
   PURPOSE:    Index on table XXWC_CSP_DM_MAINTENANCE_TAB
   
   REVISIONS:
   Ver        Date        Author               Description
   ---------  ----------  ---------------  ------------------------------------
   1.0        09/06/2017  Niraj K Ranjan   Initial Version TMS#20150625-00057   
                                           CSP Enhance bundle - Item #4  CSP Approval Workflow
******************************************************************************/
CREATE UNIQUE INDEX XXWC.XXWC_OM_CSP_DM_MAINT_N1
ON XXWC.XXWC_CSP_DM_MAINTENANCE_TAB (REGION, DISTRICT, DM_NT_ID); 