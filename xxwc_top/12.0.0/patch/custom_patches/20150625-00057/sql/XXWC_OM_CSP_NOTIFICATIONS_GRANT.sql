/******************************************************************************
   NAME:       XXWC_OM_CSP_NOTIFICATIONS_GRANT.sql
   PURPOSE:    Grant for table XXWC_OM_CSP_NOTIFICATIONS_TBL and package XXWC_OM_CSP_INTERFACE_PKG.
   
   REVISIONS:
   Ver        Date        Author               Description
   ---------  ----------  ---------------  ------------------------------------
   1.0        16/05/2017  Niraj K Ranjan   Initial Version TMS#20150625-00057   
                                           CSP Enhance bundle - Item #4  CSP Approval Workflow
******************************************************************************/
 GRANT ALL ON XXWC.XXWC_OM_CSP_NOTIFICATIONS_TBL TO EA_APEX;
 GRANT EXECUTE ON APPS.XXWC_OM_CSP_INTERFACE_PKG TO EA_APEX;