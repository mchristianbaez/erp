/******************************************************************************
   NAME:       XXWC_OM_CSP_NOTIFICATIONS_TBL_SYN.sql
   PURPOSE:    synonym for table XXWC_OM_CSP_NOTIFICATIONS_TBL.
   
   REVISIONS:
   Ver        Date        Author               Description
   ---------  ----------  ---------------  ------------------------------------
   1.0        16/05/2017  Niraj K Ranjan   Initial Version TMS#20150625-00057   
                                           CSP Enhance bundle - Item #4  CSP Approval Workflow
******************************************************************************/
 CREATE OR REPLACE SYNONYM APPS.XXWC_OM_CSP_NOTIFICATIONS_TBL FOR XXWC.XXWC_OM_CSP_NOTIFICATIONS_TBL;