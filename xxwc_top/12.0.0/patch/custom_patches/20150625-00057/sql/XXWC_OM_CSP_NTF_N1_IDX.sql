/******************************************************************************
   NAME:       XXWC_OM_CSP_NTF_N1.sql
   PURPOSE:    index for table XXWC_OM_CSP_NOTIFICATIONS_TBL.
   
   REVISIONS:
   Ver        Date        Author               Description
   ---------  ----------  ---------------  ------------------------------------
   1.0        16/05/2017  Niraj K Ranjan   Initial Version TMS#20150625-00057   
                                           CSP Enhance bundle - Item #4  CSP Approval Workflow
******************************************************************************/
CREATE INDEX XXWC.XXWC_OM_CSP_NTF_N1 ON XXWC.XXWC_OM_CSP_NOTIFICATIONS_TBL(AGREEMENT_ID);