/*
TMS#20170824-00103 - FW: Debugging is now turned off for XML bursting programs in EBS Production instance.
Created by Vamshi on 24-Aug-2017
*/
ALTER SESSION SET CURRENT_SCHEMA=apps;
/
CREATE TABLE XXWC.XXWC_FND_REQUEST_BKP
AS
   SELECT a.request_id,
          a.parent_request_id,
          a.argument_text,
          a.argument1,
          a.argument2,
          a.argument3,
          d.argument_text parent_argument_text,
          b.user_name,
          b.description,
          b.email_address,
          a.requested_start_date,
          a.phase_code,
          a.status_code
     FROM fnd_concurrent_requests a,
          fnd_user b,
          fnd_concurrent_programs_vl c,
          FND_CONC_REQ_SUMMARY_V d
    WHERE     1 = 1
          AND c.user_concurrent_program_name LIKE '%XML%%' --IN ('Whitecap Backorder Report - Print',        'Whitecap Backorder Report')
          AND a.concurrent_program_id = c.concurrent_program_id
          AND a.phase_code = 'P'
          AND a.status_code = 'I'
          AND a.requested_by = b.user_id
          AND d.request_id = a.parent_request_id
          AND NVL (a.argument3, 'A') != 'N'
/
BEGIN
DBMS_OUTPUT.PUT_LINE('Update begin');
UPDATE apps.fnd_concurrent_requests
   SET phase_code = 'C', status_code = 'X'
 WHERE request_id IN (SELECT request_id
                        FROM xxwc.xxwc_fnd_request_bkp);
DBMS_OUTPUT.PUT_LINE('Records Updated '||SQL%ROWCOUNT);						
DBMS_OUTPUT.PUT_LINE('Update End');						

COMMIT;
END;
/