--Report Name            : WC User Roles and Responsibilities
--Import Instructions    : Run this file in XXEIS user from SQL*Plus or any tool like Toad, SQL Navigator. 
prompt Creating View Data for WC User Roles and Responsibilities
set scan off define off
DECLARE
BEGIN 
--Inserting View XXWC_USER_ROLES_VW
xxeis.eis_rs_ins.v( 'XXWC_USER_ROLES_VW',85000,'','','','','MR020532','XXEIS','Xxwc User Roles Vw','XURV','','');
--Delete View Columns for XXWC_USER_ROLES_VW
xxeis.eis_rs_utility.delete_view_rows('XXWC_USER_ROLES_VW',85000,FALSE);
--Inserting View Columns for XXWC_USER_ROLES_VW
xxeis.eis_rs_ins.vc( 'XXWC_USER_ROLES_VW','EFFECTIVE_END_DATE',85000,'Effective End Date','EFFECTIVE_END_DATE','','','','MR020532','DATE','','','Effective End Date','','','');
xxeis.eis_rs_ins.vc( 'XXWC_USER_ROLES_VW','EFFECTIVE_START_DATE',85000,'Effective Start Date','EFFECTIVE_START_DATE','','','','MR020532','DATE','','','Effective Start Date','','','');
xxeis.eis_rs_ins.vc( 'XXWC_USER_ROLES_VW','LAST_ASSIGNED_BY',85000,'Last Assigned By','LAST_ASSIGNED_BY','','','','MR020532','VARCHAR2','','','Last Assigned By','','','');
xxeis.eis_rs_ins.vc( 'XXWC_USER_ROLES_VW','SUPER_NAME',85000,'Super Name','SUPER_NAME','','','','MR020532','VARCHAR2','','','Super Name','','','');
xxeis.eis_rs_ins.vc( 'XXWC_USER_ROLES_VW','SUPER_CODE',85000,'Super Code','SUPER_CODE','','','','MR020532','VARCHAR2','','','Super Code','','','');
xxeis.eis_rs_ins.vc( 'XXWC_USER_ROLES_VW','SUB_NAME',85000,'Sub Name','SUB_NAME','','','','MR020532','VARCHAR2','','','Sub Name','','','');
xxeis.eis_rs_ins.vc( 'XXWC_USER_ROLES_VW','SUB_CODE',85000,'Sub Code','SUB_CODE','','','','MR020532','VARCHAR2','','','Sub Code','','','');
xxeis.eis_rs_ins.vc( 'XXWC_USER_ROLES_VW','FULL_NAME',85000,'Full Name','FULL_NAME','','','','MR020532','VARCHAR2','','','Full Name','','','');
xxeis.eis_rs_ins.vc( 'XXWC_USER_ROLES_VW','USER_NAME',85000,'User Name','USER_NAME','','','','MR020532','VARCHAR2','','','User Name','','','');
xxeis.eis_rs_ins.vc( 'XXWC_USER_ROLES_VW','EMAIL_ADDRESS',85000,'Email Address','EMAIL_ADDRESS','','','','MR020532','VARCHAR2','','','Email Address','','','');
xxeis.eis_rs_ins.vc( 'XXWC_USER_ROLES_VW','SUPERVISOR_EMAIL',85000,'Supervisor Email','SUPERVISOR_EMAIL','','','','MR020532','VARCHAR2','','','Supervisor Email','','','');
xxeis.eis_rs_ins.vc( 'XXWC_USER_ROLES_VW','SUPERVISOR_NAME',85000,'Supervisor Name','SUPERVISOR_NAME','','','','MR020532','VARCHAR2','','','Supervisor Name','','','');
xxeis.eis_rs_ins.vc( 'XXWC_USER_ROLES_VW','EMPLOYEE_NUMBER',85000,'Employee Number','EMPLOYEE_NUMBER','','','','MR020532','VARCHAR2','','','Employee Number','','','');
xxeis.eis_rs_ins.vc( 'XXWC_USER_ROLES_VW','IS_BUYER',85000,'Is Buyer','IS_BUYER','','','','MR020532','CHAR','','','Is Buyer','','','');
xxeis.eis_rs_ins.vc( 'XXWC_USER_ROLES_VW','BUYER_JOB',85000,'Buyer Job','BUYER_JOB','','','','MR020532','VARCHAR2','','','Buyer Job','','','');
xxeis.eis_rs_ins.vc( 'XXWC_USER_ROLES_VW','BUYER_POSITION',85000,'Buyer Position','BUYER_POSITION','','','','MR020532','VARCHAR2','','','Buyer Position','','','');
xxeis.eis_rs_ins.vc( 'XXWC_USER_ROLES_VW','ASSIGNMENT_REASON',85000,'Assignment Reason','ASSIGNMENT_REASON','','','','MR020532','VARCHAR2','','','Assignment Reason','','','');
xxeis.eis_rs_ins.vc( 'XXWC_USER_ROLES_VW','GL_SEGMENT',85000,'Gl Segment','GL_SEGMENT','','','','MR020532','VARCHAR2','','','Gl Segment','','','');
xxeis.eis_rs_ins.vc( 'XXWC_USER_ROLES_VW','SCOPE',85000,'Scope','SCOPE','','','','MR020532','VARCHAR2','','','Scope','','','');
xxeis.eis_rs_ins.vc( 'XXWC_USER_ROLES_VW','BRANCH',85000,'Branch','BRANCH','','','','MR020532','VARCHAR2','','','Branch','','','');
xxeis.eis_rs_ins.vc( 'XXWC_USER_ROLES_VW','REGION',85000,'Region','REGION','','','','MR020532','VARCHAR2','','','Region','','','');
xxeis.eis_rs_ins.vc( 'XXWC_USER_ROLES_VW','FRU',85000,'Fru','FRU','','','','MR020532','VARCHAR2','','','Fru','','','');
xxeis.eis_rs_ins.vc( 'XXWC_USER_ROLES_VW','LOB',85000,'Lob','LOB','','','','MR020532','VARCHAR2','','','Lob','','','');
xxeis.eis_rs_ins.vc( 'XXWC_USER_ROLES_VW','FRU_DESCRIPTION',85000,'Fru Description','FRU_DESCRIPTION','','','','MR020532','VARCHAR2','','','Fru Description','','','');
xxeis.eis_rs_ins.vc( 'XXWC_USER_ROLES_VW','JOB_DESCRIPTION',85000,'Job Description','JOB_DESCRIPTION','','','','MR020532','VARCHAR2','','','Job Description','','','');
xxeis.eis_rs_ins.vc( 'XXWC_USER_ROLES_VW','LAST_UPDATE_DATE',85000,'Last Update Date','LAST_UPDATE_DATE','','','','MR020532','DATE','','','Last Update Date','','','');
xxeis.eis_rs_ins.vc( 'XXWC_USER_ROLES_VW','BUYER_EMPLOYEE_ID',85000,'Buyer Employee Id','BUYER_EMPLOYEE_ID','','','','MR020532','NUMBER','','','Buyer Employee Id','','','');
xxeis.eis_rs_ins.vc( 'XXWC_USER_ROLES_VW','ROLE_ORIG_SYSTEM',85000,'Role Orig System','ROLE_ORIG_SYSTEM','','','','MR020532','VARCHAR2','','','Role Orig System','','','');
--Inserting View Components for XXWC_USER_ROLES_VW
--Inserting View Component Joins for XXWC_USER_ROLES_VW
END;
/
set scan on define on
prompt Creating Report Data for WC User Roles and Responsibilities
set scan off define off
DECLARE
BEGIN 
--Deleting Report data - WC User Roles and Responsibilities
xxeis.eis_rs_utility.delete_report_rows( 'WC User Roles and Responsibilities' );
--Inserting Report - WC User Roles and Responsibilities
xxeis.eis_rs_ins.r( 85000,'WC User Roles and Responsibilities','','Create User Role Responsibilities view for reporting.This view shows all responsibilities and roles for all WC users, or users who have been assigned a WC role or Responsibility.

The Parent column shows the role that provides access to the roles/responsibilities shown in the Child column. For responsibilities directly assigned the parent will be the same as the child.

In cases where there is a hierarchy (parent role has a sub-role, and the sub-role has children) it is flattened in this report. All items are included, but shown as rolling up under the top-level parent. Intermediate roles are also shown as rolling up to the parent.','','','','MR020532','XXWC_USER_ROLES_VW','Y','','','MR020532','','N','WC Audit Reports','','HTML,EXCEL,','N');
--Inserting Report Columns - WC User Roles and Responsibilities
xxeis.eis_rs_ins.rc( 'WC User Roles and Responsibilities',85000,'EFFECTIVE_END_DATE','Effective End Date','Effective End Date','','','','','9','N','','','','','','','','MR020532','N','N','','XXWC_USER_ROLES_VW','','');
xxeis.eis_rs_ins.rc( 'WC User Roles and Responsibilities',85000,'EFFECTIVE_START_DATE','Effective Start Date','Effective Start Date','','','','','8','N','','','','','','','','MR020532','N','N','','XXWC_USER_ROLES_VW','','');
xxeis.eis_rs_ins.rc( 'WC User Roles and Responsibilities',85000,'FULL_NAME','Full Name','Full Name','','','','','3','N','','','','','','','','MR020532','N','N','','XXWC_USER_ROLES_VW','','');
xxeis.eis_rs_ins.rc( 'WC User Roles and Responsibilities',85000,'LAST_ASSIGNED_BY','Last Assigned By','Last Assigned By','','','','','10','N','','','','','','','','MR020532','N','N','','XXWC_USER_ROLES_VW','','');
xxeis.eis_rs_ins.rc( 'WC User Roles and Responsibilities',85000,'SUB_CODE','Child Code','Sub Code','','','','','13','N','','','','','','','','MR020532','N','N','','XXWC_USER_ROLES_VW','','');
xxeis.eis_rs_ins.rc( 'WC User Roles and Responsibilities',85000,'SUB_NAME','Child Role/Resp','Sub Name','','','','','6','N','','','','','','','','MR020532','N','N','','XXWC_USER_ROLES_VW','','');
xxeis.eis_rs_ins.rc( 'WC User Roles and Responsibilities',85000,'SUPER_CODE','Parent Code','Super Code','','','','','12','N','','','','','','','','MR020532','N','N','','XXWC_USER_ROLES_VW','','');
xxeis.eis_rs_ins.rc( 'WC User Roles and Responsibilities',85000,'SUPER_NAME','Parent Role/Resp','Super Name','','','','','5','N','','','','','','','','MR020532','N','N','','XXWC_USER_ROLES_VW','','');
xxeis.eis_rs_ins.rc( 'WC User Roles and Responsibilities',85000,'USER_NAME','User Name','User Name','','','','','1','N','','','','','','','','MR020532','N','N','','XXWC_USER_ROLES_VW','','');
xxeis.eis_rs_ins.rc( 'WC User Roles and Responsibilities',85000,'EMAIL_ADDRESS','Email Address','Email Address','','','','','4','N','','','','','','','','MR020532','N','N','','XXWC_USER_ROLES_VW','','');
xxeis.eis_rs_ins.rc( 'WC User Roles and Responsibilities',85000,'SUPERVISOR_EMAIL','Supervisor Email','Supervisor Email','','','','','15','N','','','','','','','','MR020532','N','N','','XXWC_USER_ROLES_VW','','');
xxeis.eis_rs_ins.rc( 'WC User Roles and Responsibilities',85000,'SUPERVISOR_NAME','Supervisor Name','Supervisor Name','','','','','14','N','','','','','','','','MR020532','N','N','','XXWC_USER_ROLES_VW','','');
xxeis.eis_rs_ins.rc( 'WC User Roles and Responsibilities',85000,'EMPLOYEE_NUMBER','Employee Number','Employee Number','','','','','2','N','','','','','','','','MR020532','N','N','','XXWC_USER_ROLES_VW','','');
xxeis.eis_rs_ins.rc( 'WC User Roles and Responsibilities',85000,'IS_BUYER','Is Buyer','Is Buyer','','','','','18','N','','','','','','','','MR020532','N','N','','XXWC_USER_ROLES_VW','','');
xxeis.eis_rs_ins.rc( 'WC User Roles and Responsibilities',85000,'BUYER_JOB','Buyer Job','Buyer Job','','','','','16','N','','','','','','','','MR020532','N','N','','XXWC_USER_ROLES_VW','','');
xxeis.eis_rs_ins.rc( 'WC User Roles and Responsibilities',85000,'BUYER_POSITION','Buyer Position','Buyer Position','','','','','17','N','','','','','','','','MR020532','N','N','','XXWC_USER_ROLES_VW','','');
xxeis.eis_rs_ins.rc( 'WC User Roles and Responsibilities',85000,'ASSIGNMENT_REASON','Assignment Reason','Assignment Reason','','','','','11','N','','','','','','','','MR020532','N','N','','XXWC_USER_ROLES_VW','','');
xxeis.eis_rs_ins.rc( 'WC User Roles and Responsibilities',85000,'GL_SEGMENT','Gl Segment','Gl Segment','','','','','19','N','','','','','','','','MR020532','N','N','','XXWC_USER_ROLES_VW','','');
xxeis.eis_rs_ins.rc( 'WC User Roles and Responsibilities',85000,'SCOPE','Scope','Scope','','','','','20','N','','','','','','','','MR020532','N','N','','XXWC_USER_ROLES_VW','','');
xxeis.eis_rs_ins.rc( 'WC User Roles and Responsibilities',85000,'BRANCH','Branch','Branch','','','','','24','N','','','','','','','','MR020532','N','N','','XXWC_USER_ROLES_VW','','');
xxeis.eis_rs_ins.rc( 'WC User Roles and Responsibilities',85000,'REGION','Region','Region','','','','','25','N','','','','','','','','MR020532','N','N','','XXWC_USER_ROLES_VW','','');
xxeis.eis_rs_ins.rc( 'WC User Roles and Responsibilities',85000,'FRU','Fru','Fru','','','','','22','N','','','','','','','','MR020532','N','N','','XXWC_USER_ROLES_VW','','');
xxeis.eis_rs_ins.rc( 'WC User Roles and Responsibilities',85000,'LOB','Lob','Lob','','','','','26','N','','','','','','','','MR020532','N','N','','XXWC_USER_ROLES_VW','','');
xxeis.eis_rs_ins.rc( 'WC User Roles and Responsibilities',85000,'FRU_DESCRIPTION','Fru Description','Fru Description','','','','','23','N','','','','','','','','MR020532','N','N','','XXWC_USER_ROLES_VW','','');
xxeis.eis_rs_ins.rc( 'WC User Roles and Responsibilities',85000,'JOB_DESCRIPTION','Job Description','Job Description','','','','','21','N','','','','','','','','MR020532','N','N','','XXWC_USER_ROLES_VW','','');
xxeis.eis_rs_ins.rc( 'WC User Roles and Responsibilities',85000,'LAST_UPDATE_DATE','Last Update Date','Last Update Date','','','','','7','N','','','','','','','','MR020532','N','N','','XXWC_USER_ROLES_VW','','');
--Inserting Report Parameters - WC User Roles and Responsibilities
xxeis.eis_rs_ins.rp( 'WC User Roles and Responsibilities',85000,'Specific User NT ID','User''s login name, aka NT ID','USER_NAME','LIKE','','','VARCHAR2','Y','Y','1','','Y','CONSTANT','MR020532','Y','','','','');
xxeis.eis_rs_ins.rp( 'WC User Roles and Responsibilities',85000,'User Full Name','User''s full name','FULL_NAME','LIKE','','%','VARCHAR2','N','Y','2','','Y','CONSTANT','MR020532','Y','','','','');
xxeis.eis_rs_ins.rp( 'WC User Roles and Responsibilities',85000,'Parent Role/Resp Name','The parent role or responsiiblity','SUPER_NAME','LIKE','','%','VARCHAR2','N','Y','3','','Y','CONSTANT','MR020532','Y','','','','');
xxeis.eis_rs_ins.rp( 'WC User Roles and Responsibilities',85000,'Child Role/Resp Name','The child role or responsibility','SUB_NAME','LIKE','','%','VARCHAR2','N','Y','4','','Y','CONSTANT','MR020532','Y','','','','');
xxeis.eis_rs_ins.rp( 'WC User Roles and Responsibilities',85000,'Employee Number','HR Employee ID','EMPLOYEE_NUMBER','LIKE','','%','VARCHAR2','N','Y','6','','Y','CONSTANT','MR020532','Y','','','','');
xxeis.eis_rs_ins.rp( 'WC User Roles and Responsibilities',85000,'Is Buyer (Y/N)','','IS_BUYER','LIKE','','%','VARCHAR2','N','Y','5','','Y','CONSTANT','MR020532','Y','','','','');
xxeis.eis_rs_ins.rp( 'WC User Roles and Responsibilities',85000,'GL Segment','Indicates the LOB','GL_SEGMENT','LIKE','','%','VARCHAR2','N','Y','7','','Y','CONSTANT','MR020532','Y','N','','','');
xxeis.eis_rs_ins.rp( 'WC User Roles and Responsibilities',85000,'Scope','Auditing scope, either WC or GSC','SCOPE','LIKE','','WC','VARCHAR2','N','Y','8','','Y','CONSTANT','MR020532','Y','N','','','');
--Inserting Report Conditions - WC User Roles and Responsibilities
xxeis.eis_rs_ins.rcn( 'WC User Roles and Responsibilities',85000,'USER_NAME','LIKE',':Specific User NT ID','','','Y','1','Y','MR020532');
xxeis.eis_rs_ins.rcn( 'WC User Roles and Responsibilities',85000,'FULL_NAME','LIKE',':User Full Name','','','Y','2','Y','MR020532');
xxeis.eis_rs_ins.rcn( 'WC User Roles and Responsibilities',85000,'SUPER_NAME','LIKE',':Parent Role/Resp Name','','','Y','3','Y','MR020532');
xxeis.eis_rs_ins.rcn( 'WC User Roles and Responsibilities',85000,'SUB_NAME','LIKE',':Child Role/Resp Name','','','Y','4','Y','MR020532');
xxeis.eis_rs_ins.rcn( 'WC User Roles and Responsibilities',85000,'EMPLOYEE_NUMBER','LIKE',':Employee Number','','','Y','6','Y','MR020532');
xxeis.eis_rs_ins.rcn( 'WC User Roles and Responsibilities',85000,'IS_BUYER','LIKE',':Is Buyer (Y/N)','','','Y','5','Y','MR020532');
xxeis.eis_rs_ins.rcn( 'WC User Roles and Responsibilities',85000,'SCOPE','LIKE',':Scope','','','Y','8','Y','MR020532');
xxeis.eis_rs_ins.rcn( 'WC User Roles and Responsibilities',85000,'GL_SEGMENT','LIKE',':GL Segment','','','Y','7','Y','MR020532');
--Inserting Report Sorts - WC User Roles and Responsibilities
--Inserting Report Triggers - WC User Roles and Responsibilities
--Inserting Report Templates - WC User Roles and Responsibilities
--Inserting Report Portals - WC User Roles and Responsibilities
--Inserting Report Dashboards - WC User Roles and Responsibilities
--Inserting Report Security - WC User Roles and Responsibilities
xxeis.eis_rs_ins.rsec( 'WC User Roles and Responsibilities','20005','','50900',85000,'MR020532','','');
xxeis.eis_rs_ins.rsec( 'WC User Roles and Responsibilities','20005','','50861',85000,'MR020532','','');
xxeis.eis_rs_ins.rsec( 'WC User Roles and Responsibilities','20005','','50982',85000,'MR020532','','');
xxeis.eis_rs_ins.rsec( 'WC User Roles and Responsibilities','20005','','50853',85000,'MR020532','','');
xxeis.eis_rs_ins.rsec( 'WC User Roles and Responsibilities','20005','','50843',85000,'MR020532','','');
xxeis.eis_rs_ins.rsec( 'WC User Roles and Responsibilities','20005','','51207',85000,'MR020532','','');
--Inserting Report Pivots - WC User Roles and Responsibilities
xxeis.eis_rs_ins.rpivot( 'WC User Roles and Responsibilities',85000,'Pivot','1','1,0|1,2,1','1,1,0,0|None|2');
--Inserting Report Pivot Details For Pivot - Pivot
xxeis.eis_rs_ins.rpivot_dtls( 'WC User Roles and Responsibilities',85000,'Pivot','FULL_NAME','ROW_FIELD','','Full Name','1','1','xlNormal');
xxeis.eis_rs_ins.rpivot_dtls( 'WC User Roles and Responsibilities',85000,'Pivot','SUB_NAME','ROW_FIELD','','Child Role/Resp','5','1','xlNormal');
xxeis.eis_rs_ins.rpivot_dtls( 'WC User Roles and Responsibilities',85000,'Pivot','SUPER_NAME','ROW_FIELD','','Parent Role/Resp','4','1','xlNormal');
xxeis.eis_rs_ins.rpivot_dtls( 'WC User Roles and Responsibilities',85000,'Pivot','USER_NAME','ROW_FIELD','','NT ID','2','1','xlNormal');
xxeis.eis_rs_ins.rpivot_dtls( 'WC User Roles and Responsibilities',85000,'Pivot','IS_BUYER','ROW_FIELD','','','3','','');
--Inserting Report Summary Calculation Columns For Pivot- Pivot
END;
/
set scan on define on
