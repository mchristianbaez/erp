-----------------------------------------------------------------------------------------------------------------------------
/*************************************************************************************************************************
  $Header Data script for Bill to sites to update freight term to exempt
  File Name: ship to script.sql
  PURPOSE:   
  REVISIONS:
     Ver          Date         Author         Description
     ---------  ----------   --------         -----------------------------------------------------------------------------------
     1.0        06-Oct-2018    Nancy Pahwa    TMS#20181006-00001-Freight Terms Exempt Data Script for AHH Customers
****************************************************************************************************************************/
SET SERVEROUTPUT ON SIZE 1000000

declare
  x_return_status         varchar2(10);
  x_msg_count             number(10);
  x_msg_data              varchar2(1200);
  p_object_version_number number(10) := 2;
  P_CUST_SITE_USE_REC     hz_cust_account_site_v2pub.CUST_SITE_USE_REC_TYPE;
  l_count NUMBER:=0;
begin
  --Apps Initialization
  dbms_output.put_line('ORACLE_ACCOUNT_NAME,ORACLE_ACCOUNT_NUMBER,ORACLE_SITE_NUMBER,CUST_ACCT_SITE_ID,Freight_Terms');
	
  FND_GLOBAL.APPS_INITIALIZE(USER_ID      => 1290,
                             RESP_ID      => 51208,
                             RESP_APPL_ID => 222);
  MO_GLOBAL.INIT('AR');
  MO_GLOBAL.SET_POLICY_CONTEXT('S', 162);
  for i in (select * from XXWC.XXWC_CUST_FREIGHT_UPD_EXT_TBL) loop
    for p in (SELECT DISTINCT   hca.account_name,
                  hcsu.Location "Primary bill To Location Name",
                  hcsu.site_use_id cust_site_use_id,
                  hl.address1,
                  hl.address2,
                  hl.city,
                  hl.state,
                  hl.county,
                  hl.postal_code,
                  hcsu.site_use_id,
                  hcsu.FREIGHT_TERM,
                  hcsu.object_version_number,
                  hcsu.cust_acct_site_id,
				  hcsu.CREATED_BY_MODULE 
    FROM apps.hz_Cust_Accounts_all hca,
         apps.hz_party_sites hps,
         apps.hz_cust_acct_sites_All hcsa,
         apps.hz_locations hl,
         apps.hz_cust_site_uses_all hcsu
   WHERE     1 = 1
         AND hps.party_id = hca.party_id
         AND hps.party_site_id = hcsa.party_site_id
         AND hca.cust_account_id = hcsa.cust_account_id
         AND hps.LOCATION_ID = hl.location_id
         AND hcsu.cust_acct_site_id = hcsa.cust_acct_site_id
         AND hcsu.site_use_code = 'BILL_TO'
         and hcsu.cust_acct_site_id = i.cust_acct_site_id
         AND hcsu.status = 'A') loop
  P_CUST_SITE_USE_REC.site_use_id       := p.site_use_id; 
  P_CUST_SITE_USE_REC.cust_acct_site_id := p.cust_acct_site_id; 
  P_CUST_SITE_USE_REC.freight_term      := FND_API.G_MISS_CHAR;
  P_CUST_SITE_USE_REC.SITE_USE_CODE     := 'BILL_TO';
  P_CUST_SITE_USE_REC.CREATED_BY_MODULE := p.CREATED_BY_MODULE;
  p_object_version_number := p.object_version_number;
  hz_cust_account_site_v2pub.update_cust_site_use(p_init_msg_list         => 'T',
                                                  P_CUST_SITE_USE_REC     => P_CUST_SITE_USE_REC,
                                                  p_object_version_number => p_object_version_number,
                                                  x_return_status         => x_return_status,
                                                  x_msg_count             => x_msg_count,
                                                  x_msg_data              => x_msg_data);
  
  									  
  IF x_return_status = 'S' THEN
   l_count := l_count+1;
    dbms_output.put_line(i.ORACLE_ACCOUNT_NAME||','||i.ORACLE_ACCOUNT_NUMBER||','||i.ORACLE_SITE_NUMBER||','||i.CUST_ACCT_SITE_ID||','||i.FREIGHT_TERMS);
  ELSE
    dbms_output.put_line(' Error message ' || x_msg_data);
    dbms_output.put_line(i.ORACLE_ACCOUNT_NAME||','||i.ORACLE_ACCOUNT_NUMBER||','||i.ORACLE_SITE_NUMBER||','||i.CUST_ACCT_SITE_ID||','||i.FREIGHT_TERMS);
    IF NVL(x_msg_count, 0) > 1 THEN
      FOR i IN 1 .. x_msg_count LOOP
      
        dbms_output.put_line(' Error Status ' || x_return_status);
        dbms_output.put_line(' Error message ' || x_msg_data);
      END LOOP;
    ELSE
      dbms_output.put_line(' Error message ' || x_msg_data);
    END IF;
  END IF;
  commit;
   end loop;
  end loop;
  DBMS_OUTPUT.put_line ('Records updated l_count: ' || l_count);
exception
  when others then
    dbms_output.put_line(' Error Here' || sqlcode || sqlerrm);
end;
/