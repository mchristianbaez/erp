REM dbdrv: none

/*=======================================================================+
 * |  Copyright (c) 2006 Oracle Corporation Redwood Shores, California, USA|
 * |                            All rights reserved.                       |
 * +=======================================================================*/

/* $Header: poxpoaprvl.sql 120.0.12010000.2 2012/10/19 06:31:06 inagdeo noship $ */
SET SERVEROUTPUT ON SIZE UNLIMITED
SET VERIFY OFF;
REM File: poxpoaprvl.sql
REM
REM
REM Purpose: Generic Data Fix script for Cancelled Documents (PO/PA/Release)
REM          stuck in Requires Reapproval status.
REM
REM Usage: (See below)
REM
REM

/*------------------------------------------

Data Manipulation Scripts Disclaimer

---------------------------------------------

As always please run the scripts on test instance first before applying it
on production. Make sure the data is validated for correctness and related
functionality is verified after the script has been run on a test instance.
Customers are responsible to authenticate and verify correctness of data
updated by data manipulation scripts.

1. Connect to SQL plus and the run this script.
2. Once it is completed ensure there is no error reported.
3. Then validate the data.
4. If the data is satisfactory, go ahead and Commit.
5. Test the behaviour in application to ensure that the data is correct */
---------------------------------------------------------------------------

/* PLEASE READ THIS NOTE CAREFULLY BEFORE EXECUTING THIS SCRIPT.

* This script should be used for the following scenario :
* Cancelled PO/PA/Release stuck in Requires ReApproval Status.

* This script will :
* Approve the document.
* Communicate the document to the supplier based on supplier's communication method.
* Capture FND Logs.

* This script need following inputs :
* User Id
* Responsibility Id
* Responsibility Application Id
* Org Id
* Po_Header_id

* In case there is any error while running the script
* Or document is not approved then upload the following information :
* 1) Output of this script.
* 2) FND Logs using the following sql :
* SELECT * FROM fnd_log_messages
* WHERE log_sequence > Sequence_no_before_approving;
* Get Sequence_no_before_approving from output of this script.
* (Run the above sql immediately after running this script)
*/

DECLARE
   l_conterms_exist_flag   po.PO_HEADERS_ALL.conterms_exist_flag%TYPE;
   l_auth_status           VARCHAR2 (30);
   l_revision_num          NUMBER;
   l_request_id            NUMBER := 0;
   l_doc_type              VARCHAR2 (30);
   l_doc_subtype           VARCHAR2 (30);
   l_comm_doc_type         VARCHAR2 (30);
   l_document_id           NUMBER;
   l_agent_id              NUMBER;
   l_printflag             VARCHAR2 (1) := 'N';
   l_faxflag               VARCHAR2 (1) := 'N';
   l_faxnum                VARCHAR2 (30);
   l_emailflag             VARCHAR2 (1) := 'N';
   l_emailaddress          PO_VENDOR_SITES.email_address%TYPE;
   l_default_method        PO_VENDOR_SITES.supplier_notif_method%TYPE;
   l_user_id               po_lines.last_updated_by%TYPE := -1;
   l_login_id              po_lines.last_update_login%TYPE := -1;
   x_return_status         VARCHAR2 (1);
   x_msg_data              VARCHAR2 (2000);
   l_doc_num               VARCHAR2 (30);
   l_approval_path_id      NUMBER;
   l_progress              NUMBER;

   CURSOR c_pos
   IS
      SELECT org_id,
             po_header_id,
             segment1,
             authorization_status
        FROM po_headers_all
       WHERE     authorization_status = 'REQUIRES REAPPROVAL'
             AND cancel_flag = 'Y'
             AND org_id = 162;
BEGIN
   DBMS_OUTPUT.enable (NULL);

   FOR i IN c_pos
   LOOP
      l_document_id := i.po_header_id;

      fnd_global.apps_initialize (user_id        => 16991,
                                  resp_id        => 50983,
                                  resp_appl_id   => 201);

      po_moac_utils_pvt.set_org_context (162);


      /* -- Set the FND profile option values.
       FND_PROFILE.put ('AFLOG_ENABLED', 'Y');
       FND_PROFILE.put ('AFLOG_MODULE', '%');
       FND_PROFILE.put ('AFLOG_LEVEL', TO_CHAR (1));
       FND_PROFILE.put ('AFLOG_FILENAME', '');

       -- Refresh the FND cache.
       FND_LOG_REPOSITORY.init ();*/

      --Get User ID and Login ID
      l_user_id := FND_GLOBAL.USER_ID;
      l_login_id := FND_GLOBAL.LOGIN_ID;

      DBMS_OUTPUT.put_line (
            'Before Update PO|'
         || i.segment1
         || '-Status-'
         || i.authorization_status);

      BEGIN
         SELECT NVL (conterms_exist_flag, 'N'),
                revision_num,
                DECODE (type_lookup_code,
                        'BLANKET', 'PA',
                        'CONTRACT', 'PA',
                        'PO'),
                type_lookup_code,
                AGENT_ID
           INTO l_conterms_exist_flag,
                l_revision_num,
                l_doc_type,
                l_doc_subtype,
                l_agent_id
           FROM po_headers_all
          WHERE po_header_id = l_document_id;

         l_comm_doc_type := l_doc_subtype;
      EXCEPTION
         WHEN NO_DATA_FOUND
         THEN
            SELECT NVL (poh.conterms_exist_flag, 'N'),
                   por.revision_num,
                   'RELEASE',
                   poh.type_lookup_code,
                   por.AGENT_ID
              INTO l_conterms_exist_flag,
                   l_revision_num,
                   l_doc_type,
                   l_doc_subtype,
                   l_agent_id
              FROM po_releases_all por, po_headers_all poh
             WHERE     po_release_id = l_document_id
                   AND poh.po_header_id = por.po_header_id;

            l_comm_doc_type := l_doc_type;
         WHEN OTHERS
         THEN
            DBMS_OUTPUT.put_line (
               'IN EXCEPTION sqlcode: ' || SQLCODE || 'sqlerrm: ' || SQLERRM);
      END;


      SELECT podt.default_approval_path_id
        INTO l_approval_path_id
        FROM po_document_types podt
       WHERE     podt.document_type_code = l_doc_type
             AND podt.document_subtype = l_doc_subtype;

      --  SELECT MAX (LOG_SEQUENCE) INTO l_progress FROM FND_LOG_MESSAGES;

      -- DBMS_OUTPUT.put_line ('Sequence no before approving : ' || l_progress);

      PO_DOCUMENT_ACTION_PVT.do_approve (
         p_document_id        => l_document_id,
         p_document_type      => l_doc_type,
         p_document_subtype   => l_doc_subtype,
         p_note               => NULL,
         p_approval_path_id   => l_approval_path_id,
         x_return_status      => x_return_status,
         x_exception_msg      => x_msg_data);

      --  SELECT MAX (LOG_SEQUENCE) INTO l_progress FROM FND_LOG_MESSAGES;

      --  DBMS_OUTPUT.put_line ('Sequence no after approving : ' || l_progress);


      IF x_return_status = 'S'
      THEN
         DBMS_OUTPUT.put_line ('After Update -po_approved-' || i.segment1);
      /*  -- Communicate to the Supplier
      PO_VENDOR_SITES_SV.get_transmission_defaults (
        p_document_id      => l_document_id,
        p_document_type    => l_doc_type,
        p_document_subtype => l_doc_subtype,
        p_preparer_id      => l_agent_id,
        x_default_method   => l_default_method,
        x_email_address    => l_emailaddress,
        x_fax_number       => l_faxnum,
        x_document_num     => l_doc_num,
        p_retrieve_only_flag => 'Y'
        );

      IF (l_default_method  = 'EMAIL') AND (l_emailaddress IS NOT NULL) then
       l_faxnum := NULL;
     ELSIF (l_default_method  = 'FAX') AND (l_faxnum IS NOT NULL) then
        l_emailaddress := NULL;
      ELSIF (l_default_method  = 'PRINT') then
        l_emailaddress := null;
        l_faxnum := null;
      ELSE
        l_default_method := 'PRINT';
        l_emailaddress := null;
        l_faxnum := null;
      END IF;

   dbms_output.put_line('l_default_method : '|| l_default_method);


      Po_Communication_PVT.communicate(
        p_authorization_status=>PO_DOCUMENT_ACTION_PVT.g_doc_status_APPROVED,
        p_with_terms=>l_conterms_exist_flag,
        p_language_code=>FND_GLOBAL.CURRENT_LANGUAGE,
        p_mode =>l_default_method,
        p_document_id =>l_document_id,
        p_revision_number =>l_revision_num,
        p_document_type =>l_comm_doc_type,
        p_fax_number =>l_faxnum,
        p_email_address =>l_emailaddress,
        p_request_id =>l_request_id);

        SELECT Max(LOG_SEQUENCE) INTO l_progress FROM FND_LOG_MESSAGES;
         dbms_output.put_line('Sequence no after communicate : '||l_progress);

        */
      END IF;
   END LOOP;

   COMMIT;
EXCEPTION
   WHEN OTHERS
   THEN
      DBMS_OUTPUT.put_line (
         'IN EXCEPTION sqlcode: ' || SQLCODE || 'sqlerrm: ' || SQLERRM);
-- SELECT MAX (LOG_SEQUENCE) INTO l_progress FROM FND_LOG_MESSAGES;

-- DBMS_OUTPUT.put_line ('Sequence no in exception : ' || l_progress);
END;
/
