CREATE OR REPLACE PACKAGE BODY APPS.XXWC_AR_INV_PREPROCESS_PKG
IS
   /*************************************************************************
   *   $Header XXXWC_AR_INV_PREPROCESS_PKG.pkb $
   *   Module Name: XXWC_AR_INV_PREPROCESS_PKG.pkb
   *
   *   PURPOSE:   This package is used for Invoice Pre Processing
   *
   *   REVISIONS:
   *   Ver        Date         Author                Description
   *   ---------  ----------   ---------------       -------------------------
   *   1.0        09-Sep-2015  Manjula Chellappan    Initial Version
   *                                                  TMS# 20150527-00318 Forego Two AR Triggers
   * ***************************************************************************/
   --add message to concurrent output file


   PROCEDURE gen_trx_number (p_retcode        OUT NUMBER,
                             p_error_msg      OUT VARCHAR2,
                             p_region      IN     VARCHAR2)
   IS
      /* **************************************************************************
         *   Procedure Name: gen_trx_number
         *
         *   PURPOSE:   This procedure has the logic from the triggers
         *     XXWC_TAXW_INVOICE_INSERT_TRG
         *  - TO assign Invoice numbers prior to autoinvoice.
         *     Invoices are generated in Oracle Receivables in a daily bases, based on the Orders entered
         *     in Order Management.  At the time the invoice is recorded in Oracle Receivables, Taxware
         *     Audit tables are updated with the invoice�s details.
         *     It is necessary to modify the Trasaction Sources (in Oracle) to disable the Automatic Transaction
         *     Numbering, and create new sequences for the invoice's numbering by Source
         *     (STANDARD OM SOUCE, ORDER MANAGEMENT and REPAIR OM SOURCE).
         *     The last number used+1 will be the first number to be assigned , by Source, in the new sequences.
         *
         *   REVISIONS:
         *   Ver        Date        Author                     Description
         *   ---------  ----------  ---------------            -------------------------
         *   1.0        09-Sep-2015 Manjula Chellappan         Initial Version
         *                                                      TMS# 20150527-00318 Forego Two AR Triggers
         * ***************************************************************************/

      CURSOR cur_trx_lines
      IS
           SELECT ril.ROWID row_id, ril.*
             FROM ra_interface_lines ril
            WHERE     trx_number IS NULL
                  AND EXISTS
                         (SELECT 'x'
                            FROM mtl_parameters
                           WHERE    (    TO_CHAR (organization_id) =
                                            ril.interface_line_attribute10
                                     AND attribute9 = p_region)
                                 OR p_region IS NULL)
         ORDER BY 1;

      l_trx_number                    VARCHAR2 (20) DEFAULT 'xx';
      l_ref_ln_id                     NUMBER DEFAULT NULL;
      l_ref_hdr_id                    NUMBER DEFAULT NULL;
      l_order_num                     NUMBER DEFAULT NULL;
      l_cust_trx_id                   NUMBER DEFAULT NULL;

      l_purchase_order                VARCHAR2 (80) DEFAULT '12345';
      l_orig_system_bill_contact_id   NUMBER DEFAULT 12345;
      l_orig_system_ship_contact_id   NUMBER DEFAULT 12345;

      -- Error DEBUG
      l_sec                           VARCHAR2 (150);
      l_distro_list                   VARCHAR2 (75)
         DEFAULT 'hdsoracledevelopers@hdsupply.com';
      l_error_msg                     VARCHAR2 (4000);
      l_exception                     EXCEPTION;
      l_error_record                  VARCHAR2 (240);
   BEGIN
      fnd_file.put_line (
         fnd_file.LOG,
            'Gen_trx_number Start at :'
         || TO_CHAR (SYSDATE, 'DD-MON-YYYY HH:MI:SS'));

      FOR rec_trx_lines IN cur_trx_lines
      LOOP
         l_error_record :=
               'SO# '
            || rec_trx_lines.interface_line_attribute1
            || ' Line Id '
            || rec_trx_lines.interface_line_attribute6
            || ' ';

         BEGIN
            --The below code is to generate trx_number for standard orders.
            l_sec := 'STANDARD ORDER_MANAGEMENT';

            IF TRIM (rec_trx_lines.batch_source_name) = 'STANDARD OM SOURCE'
            THEN
               BEGIN
                  l_sec :=
                     'Get Max TRX Number for sales Order based on the grouping rule';

                  BEGIN
                     SELECT MAX (trx_number)
                       INTO l_trx_number
                       FROM xxwc.xxwc_taxw_invoice_num_log
                      WHERE     interface_line_attribute1 =
                                   rec_trx_lines.interface_line_attribute1
                            -- AND orig_system_ship_address_id = rec_trx_lines.orig_system_ship_address_id
                            -- AND orig_system_bill_contact_id =  NVL ( rec_trx_lines.orig_system_bill_contact_id, l_orig_system_bill_contact_id)
                            -- AND orig_system_ship_contact_id =  NVL ( rec_trx_lines.orig_system_ship_contact_id, l_orig_system_ship_contact_id)
                            -- AND purchase_order =  NVL (rec_trx_lines.purchase_order, l_purchase_order)
                            AND interface_line_attribute10 =
                                   rec_trx_lines.interface_line_attribute10
                            AND warehouse_id = rec_trx_lines.warehouse_id
                            AND sales_order = rec_trx_lines.sales_order
                            AND TRUNC (ship_date_actual) =
                                   TRUNC (
                                      NVL (rec_trx_lines.ship_date_actual,
                                           SYSDATE));
                  -- AND cust_trx_type_id = rec_trx_lines.cust_trx_type_id;

                  EXCEPTION
                     WHEN OTHERS
                     THEN
                        l_trx_number := NULL;
                  END;


                  l_sec := 'Check if Invoie Created';

                  IF l_trx_number IS NOT NULL
                  THEN
                     BEGIN
                        SELECT 'xx'
                          INTO l_trx_number
                          FROM ra_customer_trx_all cc
                         WHERE l_trx_number = cc.trx_number;
                     EXCEPTION
                        WHEN NO_DATA_FOUND
                        THEN
                           l_sec := 'Update trx_number ';

                           BEGIN
                              UPDATE ra_interface_lines
                                 SET trx_number = l_trx_number
                               WHERE     ROWID = rec_trx_lines.row_id
                                     AND trx_number IS NULL;
                           EXCEPTION
                              WHEN OTHERS
                              THEN
                                 l_error_msg :=
                                       ' Failed to Update trx_number with '
                                    || l_trx_number;

                                 RAISE l_exception;
                           END;
                     END;
                  ELSIF l_trx_number IS NULL
                  THEN
                     l_trx_number := 'xx';
                  END IF;
               END;

               IF l_trx_number = 'xx'
               THEN
                  BEGIN
                     l_trx_number := apps.xxwc_taxw_stdominvoices_seq.NEXTVAL;

                     l_sec := 'Insert into xxwc_taxw_invoice_num_log';

                     INSERT
                       INTO xxwc.xxwc_taxw_invoice_num_log (
                               creation_date,
                               batch_source_name,
                               interface_line_attribute2,
                               interface_line_attribute1,
                               interface_line_attribute3,
                               -- orig_system_ship_address_id,
                               -- cust_trx_type_id,
                               sales_order,
                               trx_number,
                               ship_date_actual,
                               --orig_system_bill_contact_id,
                               -- orig_system_ship_contact_id,
                               -- purchase_order,
                               interface_line_attribute10,
                               warehouse_id)
                        VALUES (
                                  SYSDATE,
                                  rec_trx_lines.batch_source_name,
                                  rec_trx_lines.interface_line_attribute2,
                                  rec_trx_lines.interface_line_attribute1,
                                  rec_trx_lines.interface_line_attribute3,
                                  -- rec_trx_lines.orig_system_ship_address_id,
                                  --  rec_trx_lines.cust_trx_type_id,
                                  rec_trx_lines.sales_order,
                                  l_trx_number,
                                  TRUNC (
                                     NVL (rec_trx_lines.ship_date_actual,
                                          SYSDATE)),
                                  --  NVL ( rec_trx_lines.orig_system_bill_contact_id, l_orig_system_bill_contact_id),
                                  --  NVL (rec_trx_lines.orig_system_ship_contact_id, l_orig_system_ship_contact_id),
                                  --   NVL (rec_trx_lines.purchase_order, l_purchase_order),
                                  rec_trx_lines.interface_line_attribute10,
                                  rec_trx_lines.warehouse_id);
                  EXCEPTION
                     WHEN OTHERS
                     THEN
                        l_error_msg :=
                           ' Failed to Insert into xxwc_taxw_invoice_num_log';

                        RAISE l_exception;
                  END;


                  l_sec := 'Update trx_number ';

                  BEGIN
                     UPDATE ra_interface_lines
                        SET trx_number = l_trx_number
                      WHERE     ROWID = rec_trx_lines.row_id
                            AND trx_number IS NULL;
                  EXCEPTION
                     WHEN OTHERS
                     THEN
                        l_error_msg :=
                              ' Failed to Update trx_number with '
                           || l_trx_number;

                        RAISE l_exception;
                  END;
               END IF;
            END IF;



            --the below code is to generate trx_number for return orders

            l_sec := 'ORDER_MANAGEMENT and return order';

            IF     TRIM (rec_trx_lines.batch_source_name) =
                      'ORDER MANAGEMENT'
               AND rec_trx_lines.interface_line_attribute2 = 'RETURN ORDER'
            THEN
               BEGIN
                  --return a row if you find a reference to the original sales order
                  l_sec := 'Get Reference Order Details';

                  SELECT ola.reference_line_id, ola.reference_header_id
                    INTO l_ref_ln_id, l_ref_hdr_id
                    FROM oe_order_lines_all ola, oe_order_headers_all oha
                   WHERE     ola.header_id = oha.header_id
                         AND ola.org_id = oha.org_id
                         AND ola.line_id =
                                TO_NUMBER (
                                   rec_trx_lines.interface_line_attribute6);
               EXCEPTION
                  -- no reference

                  WHEN NO_DATA_FOUND
                  THEN
                     NULL;
               END;


               IF l_ref_hdr_id IS NOT NULL
               THEN
                  l_sec := 'Get Reference Sales Order Number ';

                  SELECT order_number
                    INTO l_order_num
                    FROM oe_order_headers_all
                   WHERE header_id = l_ref_hdr_id;


                  l_sec := 'Get Invoice details of the Reference Sales Order';

                  BEGIN
                     SELECT customer_trx_id
                       INTO l_cust_trx_id
                       FROM ra_customer_trx_lines_all
                      WHERE     interface_line_attribute6 =
                                   TO_CHAR (l_ref_ln_id)
                            AND interface_line_attribute1 =
                                   TO_CHAR (l_order_num)
                            AND ROWNUM = 1;
                  EXCEPTION
                     WHEN NO_DATA_FOUND
                     THEN
                        NULL;
                  END;
               END IF;



               BEGIN
                  IF l_cust_trx_id IS NULL AND l_ref_ln_id IS NULL
                  THEN
                     BEGIN
                        l_sec :=
                           'Get Invoice Number from xxwc_taxw_invoice_num_log';

                        SELECT MAX (trx_number)
                          INTO l_trx_number
                          FROM xxwc.xxwc_taxw_invoice_num_log
                         WHERE     interface_line_attribute1 =
                                      rec_trx_lines.interface_line_attribute1
                               -- AND orig_system_ship_address_id = rec_trx_lines.orig_system_ship_address_id
                               -- AND orig_system_bill_contact_id = NVL ( rec_trx_lines.orig_system_bill_contact_id,  l_orig_system_bill_contact_id)
                               --  AND orig_system_ship_contact_id = NVL ( rec_trx_lines.orig_system_ship_contact_id, l_orig_system_ship_contact_id)
                               -- AND purchase_order =  NVL (rec_trx_lines.purchase_order,  l_purchase_order)
                               AND interface_line_attribute10 =
                                      rec_trx_lines.interface_line_attribute10
                               AND warehouse_id = rec_trx_lines.warehouse_id
                               AND sales_order = rec_trx_lines.sales_order
                               AND TRUNC (ship_date_actual) =
                                      TRUNC (
                                         NVL (rec_trx_lines.ship_date_actual,
                                              SYSDATE))
                               -- AND cust_trx_type_id = rec_trx_lines.cust_trx_type_id
                               AND cust_trx_id IS NULL;
                     EXCEPTION
                        WHEN OTHERS
                        THEN
                           l_trx_number := NULL;
                     END;
                  ELSIF l_cust_trx_id IS NULL AND l_ref_ln_id IS NOT NULL
                  THEN
                     l_trx_number := 'xx';
                     l_cust_trx_id := 123456;
                  ELSIF l_cust_trx_id IS NOT NULL AND l_ref_ln_id IS NOT NULL
                  THEN
                     l_sec :=
                        'Get Invoice Number from xxwc_taxw_invoice_num_log';

                     BEGIN
                        SELECT MAX (trx_number)
                          INTO l_trx_number
                          FROM xxwc.xxwc_taxw_invoice_num_log
                         WHERE     interface_line_attribute1 =
                                      rec_trx_lines.interface_line_attribute1
                               --   AND orig_system_ship_address_id = rec_trx_lines.orig_system_ship_address_id
                               --   AND orig_system_bill_contact_id =  NVL ( rec_trx_lines.orig_system_bill_contact_id, l_orig_system_bill_contact_id)
                               --  AND orig_system_ship_contact_id =   NVL ( rec_trx_lines.orig_system_ship_contact_id,  l_orig_system_ship_contact_id)
                               --   AND purchase_order = NVL (rec_trx_lines.purchase_order,  l_purchase_order)
                               AND interface_line_attribute10 =
                                      rec_trx_lines.interface_line_attribute10
                               AND warehouse_id = rec_trx_lines.warehouse_id
                               AND sales_order = rec_trx_lines.sales_order
                               AND TRUNC (ship_date_actual) =
                                      TRUNC (
                                         NVL (rec_trx_lines.ship_date_actual,
                                              SYSDATE))
                               -- AND cust_trx_type_id = rec_trx_lines.cust_trx_type_id
                               AND cust_trx_id = l_cust_trx_id;
                     EXCEPTION
                        WHEN OTHERS
                        THEN
                           l_trx_number := NULL;
                     END;
                  END IF;



                  IF l_trx_number IS NULL
                  THEN
                     l_trx_number := 'xx';
                  END IF;
               END;


               IF l_trx_number = 'xx'
               THEN
                  l_sec := 'Get Trx Number from sequence';

                  l_trx_number := apps.xxwc_taxw_ominvoices_seq.NEXTVAL;

                  l_sec := 'Insert into xxwc_taxw_invoice_num_log';

                  BEGIN
                     INSERT
                       INTO xxwc.xxwc_taxw_invoice_num_log (
                               creation_date,
                               batch_source_name,
                               interface_line_attribute2,
                               interface_line_attribute1, -- added by Maha for ver 1.3
                               interface_line_attribute3,
                               orig_system_ship_address_id,
                               cust_trx_type_id,
                               sales_order,
                               trx_number,
                               ship_date_actual,
                               orig_system_bill_contact_id,
                               orig_system_ship_contact_id,
                               purchase_order,
                               interface_line_attribute10,
                               warehouse_id,
                               cust_trx_id)
                        VALUES (
                                  SYSDATE,
                                  rec_trx_lines.batch_source_name,
                                  rec_trx_lines.interface_line_attribute2,
                                  rec_trx_lines.interface_line_attribute1,
                                  rec_trx_lines.interface_line_attribute3,
                                  rec_trx_lines.orig_system_ship_address_id,
                                  rec_trx_lines.cust_trx_type_id,
                                  rec_trx_lines.sales_order,
                                  l_trx_number,
                                  TRUNC (
                                     NVL (rec_trx_lines.ship_date_actual,
                                          SYSDATE)),
                                  NVL (
                                     rec_trx_lines.orig_system_bill_contact_id,
                                     l_orig_system_bill_contact_id),
                                  NVL (
                                     rec_trx_lines.orig_system_ship_contact_id,
                                     l_orig_system_ship_contact_id),
                                  NVL (rec_trx_lines.purchase_order,
                                       l_purchase_order),
                                  rec_trx_lines.interface_line_attribute10,
                                  rec_trx_lines.warehouse_id,
                                  l_cust_trx_id);
                  EXCEPTION
                     WHEN OTHERS
                     THEN
                        l_error_msg :=
                           ' Failed to insert into xxwc_taxw_invoice_num_log';

                        RAISE l_exception;
                  END;



                  IF l_cust_trx_id IS NULL AND l_ref_ln_id IS NULL
                  THEN
                     BEGIN
                        SELECT MAX (trx_number)
                          INTO l_trx_number
                          FROM xxwc.xxwc_taxw_invoice_num_log
                         WHERE     interface_line_attribute1 =
                                      rec_trx_lines.interface_line_attribute1
                               -- AND orig_system_ship_address_id =   rec_trx_lines.orig_system_ship_address_id
                               --  AND orig_system_bill_contact_id =  NVL ( rec_trx_lines.orig_system_bill_contact_id,  l_orig_system_bill_contact_id)
                               --  AND orig_system_ship_contact_id =  NVL (  rec_trx_lines.orig_system_ship_contact_id, l_orig_system_ship_contact_id)
                               --  AND purchase_order =  NVL (rec_trx_lines.purchase_order,  l_purchase_order)
                               AND interface_line_attribute10 =
                                      rec_trx_lines.interface_line_attribute10
                               AND warehouse_id = rec_trx_lines.warehouse_id
                               AND sales_order = rec_trx_lines.sales_order
                               AND TRUNC (ship_date_actual) =
                                      TRUNC (
                                         NVL (rec_trx_lines.ship_date_actual,
                                              SYSDATE))
                               --     AND cust_trx_type_id =  rec_trx_lines.cust_trx_type_id
                               AND cust_trx_id IS NULL;
                     EXCEPTION
                        WHEN OTHERS
                        THEN
                           l_trx_number := NULL;
                     END;
                  ELSIF l_cust_trx_id IS NOT NULL
                  THEN
                     BEGIN
                        SELECT MAX (trx_number)
                          INTO l_trx_number
                          FROM xxwc.xxwc_taxw_invoice_num_log
                         WHERE     interface_line_attribute1 =
                                      rec_trx_lines.interface_line_attribute1
                               -- AND orig_system_ship_address_id =  rec_trx_lines.orig_system_ship_address_id
                               -- AND orig_system_bill_contact_id =   NVL (   rec_trx_lines.orig_system_bill_contact_id, l_orig_system_bill_contact_id)
                               --  AND orig_system_ship_contact_id =  NVL (    rec_trx_lines.orig_system_ship_contact_id, l_orig_system_ship_contact_id)
                               --    AND purchase_order = NVL (rec_trx_lines.purchase_order,   l_purchase_order)
                               AND interface_line_attribute10 =
                                      rec_trx_lines.interface_line_attribute10
                               AND warehouse_id = rec_trx_lines.warehouse_id
                               AND sales_order = rec_trx_lines.sales_order
                               AND TRUNC (ship_date_actual) =
                                      TRUNC (
                                         NVL (rec_trx_lines.ship_date_actual,
                                              SYSDATE))
                               -- AND cust_trx_type_id =  rec_trx_lines.cust_trx_type_id
                               AND cust_trx_id = l_cust_trx_id;
                     EXCEPTION
                        WHEN OTHERS
                        THEN
                           l_trx_number := NULL;
                     END;
                  END IF;
               END IF;

               l_sec := 'Update trx_number ';

               BEGIN
                  UPDATE ra_interface_lines
                     SET trx_number = l_trx_number
                   WHERE ROWID = rec_trx_lines.row_id AND trx_number IS NULL;
               EXCEPTION
                  WHEN OTHERS
                  THEN
                     l_error_msg :=
                        ' Failed to Update trx_number with ' || l_trx_number;

                     RAISE l_exception;
               END;


               l_sec := 'ORDER_MANAGEMENT';
            --The below code is to generate trx_number for Counter and rental orders

            ELSIF TRIM (rec_trx_lines.batch_source_name) = 'ORDER MANAGEMENT'
            THEN
               BEGIN
                  BEGIN
                     SELECT MAX (trx_number)
                       INTO l_trx_number
                       FROM xxwc.xxwc_taxw_invoice_num_log
                      WHERE     interface_line_attribute1 =
                                   rec_trx_lines.interface_line_attribute1
                            --     AND orig_system_ship_address_id = rec_trx_lines.orig_system_ship_address_id
                            --     AND orig_system_bill_contact_id =  NVL (    rec_trx_lines.orig_system_bill_contact_id,   l_orig_system_bill_contact_id)
                            --     AND orig_system_ship_contact_id =    NVL (  rec_trx_lines.orig_system_ship_contact_id,  l_orig_system_ship_contact_id)
                            --    AND purchase_order = NVL (rec_trx_lines.purchase_order,   l_purchase_order)
                            AND interface_line_attribute10 =
                                   rec_trx_lines.interface_line_attribute10
                            AND warehouse_id = rec_trx_lines.warehouse_id
                            AND sales_order = rec_trx_lines.sales_order
                            AND TRUNC (ship_date_actual) =
                                   TRUNC (
                                      NVL (rec_trx_lines.ship_date_actual,
                                           SYSDATE));
                  --   AND cust_trx_type_id =   rec_trx_lines.cust_trx_type_id;
                  EXCEPTION
                     WHEN OTHERS
                     THEN
                        l_trx_number := NULL;
                  END;

                  IF l_trx_number IS NOT NULL
                  THEN
                     BEGIN
                        SELECT 'xx'
                          INTO l_trx_number
                          FROM ra_customer_trx_all cc
                         WHERE l_trx_number = cc.trx_number;
                     EXCEPTION
                        WHEN NO_DATA_FOUND
                        THEN
                           rec_trx_lines.trx_number := l_trx_number;
                     END;
                  ELSIF l_trx_number IS NULL
                  THEN
                     l_trx_number := 'xx';
                  END IF;
               END;


               IF l_trx_number = 'xx'
               THEN
                  BEGIN
                     l_sec := 'Generate trx number from Sequence';

                     l_trx_number := apps.xxwc_taxw_ominvoices_seq.NEXTVAL;

                     l_sec := 'Insert into xxwc_taxw_invoice_num_log';

                     INSERT
                       INTO xxwc.xxwc_taxw_invoice_num_log (
                               creation_date,
                               batch_source_name,
                               interface_line_attribute2,
                               interface_line_attribute1, -- added by Maha for ver 1.3
                               interface_line_attribute3,
                               --  orig_system_ship_address_id,
                               -- cust_trx_type_id,
                               sales_order,
                               trx_number,
                               ship_date_actual,
                               --  orig_system_bill_contact_id,
                               --  orig_system_ship_contact_id,
                               --   purchase_order,
                               interface_line_attribute10,
                               warehouse_id)
                        VALUES (
                                  SYSDATE,
                                  rec_trx_lines.batch_source_name,
                                  rec_trx_lines.interface_line_attribute2,
                                  rec_trx_lines.interface_line_attribute1, -- added by Maha for ver 1.3
                                  rec_trx_lines.interface_line_attribute3,
                                  --   rec_trx_lines.orig_system_ship_address_id,
                                  --  rec_trx_lines.cust_trx_type_id,
                                  rec_trx_lines.sales_order,
                                  l_trx_number,
                                  TRUNC (
                                     NVL (rec_trx_lines.ship_date_actual,
                                          SYSDATE)),
                                  -- NVL ( rec_trx_lines.orig_system_bill_contact_id, l_orig_system_bill_contact_id),
                                  --  NVL (  rec_trx_lines.orig_system_ship_contact_id,  l_orig_system_ship_contact_id),
                                  --   NVL (rec_trx_lines.purchase_order,    l_purchase_order),
                                  rec_trx_lines.interface_line_attribute10,
                                  rec_trx_lines.warehouse_id);
                  EXCEPTION
                     WHEN OTHERS
                     THEN
                        l_error_msg :=
                           ' Failed to Insert into xxwc_taxw_invoice_num_log';

                        RAISE l_exception;
                  END;
               END IF;


               l_sec := 'Update trx_number ';

               BEGIN
                  UPDATE ra_interface_lines
                     SET trx_number = l_trx_number
                   WHERE ROWID = rec_trx_lines.row_id AND trx_number IS NULL;
               EXCEPTION
                  WHEN OTHERS
                  THEN
                     l_error_msg :=
                        ' Failed to Update trx_number with ' || l_trx_number;

                     RAISE l_exception;
               END;
            END IF;



            -- The below code is to generate trx_number for repair orders

            l_sec := 'REPAIR OM SOURCE';

            IF TRIM (rec_trx_lines.batch_source_name) = 'REPAIR OM SOURCE'
            THEN
               BEGIN
                  BEGIN
                     l_sec := 'Get trx number from xxwc_taxw_invoice_num_log';

                     SELECT MAX (trx_number)
                       INTO l_trx_number
                       FROM xxwc.xxwc_taxw_invoice_num_log
                      WHERE     interface_line_attribute1 =
                                   rec_trx_lines.interface_line_attribute1
                            --  AND orig_system_ship_address_id =   rec_trx_lines.orig_system_ship_address_id
                            --  AND orig_system_bill_contact_id =  NVL ( rec_trx_lines.orig_system_bill_contact_id,   l_orig_system_bill_contact_id)
                            --  AND orig_system_ship_contact_id =   NVL (   rec_trx_lines.orig_system_ship_contact_id,    l_orig_system_ship_contact_id)
                            --  AND purchase_order = NVL (rec_trx_lines.purchase_order,   l_purchase_order)
                            AND interface_line_attribute10 =
                                   rec_trx_lines.interface_line_attribute10
                            AND warehouse_id = rec_trx_lines.warehouse_id
                            AND sales_order = rec_trx_lines.sales_order
                            AND TRUNC (ship_date_actual) =
                                   TRUNC (
                                      NVL (rec_trx_lines.ship_date_actual,
                                           SYSDATE));
                  --     AND cust_trx_type_id =  rec_trx_lines.cust_trx_type_id;
                  EXCEPTION
                     WHEN OTHERS
                     THEN
                        l_trx_number := NULL;
                  END;

                  l_sec := 'Check if invoice created';

                  IF l_trx_number IS NOT NULL
                  THEN
                     BEGIN
                        SELECT 'xx'
                          INTO l_trx_number
                          FROM ra_customer_trx_all cc
                         WHERE l_trx_number = cc.trx_number;
                     EXCEPTION
                        WHEN NO_DATA_FOUND
                        THEN
                           rec_trx_lines.trx_number := l_trx_number;
                     END;
                  ELSIF l_trx_number IS NULL
                  THEN
                     l_trx_number := 'xx';
                  END IF;
               END;


               IF l_trx_number = 'xx'
               THEN
                  l_sec := 'Generate trx Number from Sequence';

                  l_trx_number := apps.xxwc_taxw_repominvoices_seq.NEXTVAL;

                  l_sec := 'Insert into xxwc_taxw_invoice_num_log';

                  INSERT
                    INTO xxwc.xxwc_taxw_invoice_num_log (
                            creation_date,
                            batch_source_name,
                            interface_line_attribute2,
                            interface_line_attribute1,
                            interface_line_attribute3,
                            orig_system_ship_address_id,
                            cust_trx_type_id,
                            sales_order,
                            trx_number,
                            ship_date_actual,
                            orig_system_bill_contact_id,
                            orig_system_ship_contact_id,
                            purchase_order,
                            interface_line_attribute10,
                            warehouse_id)
                     VALUES (
                               SYSDATE,
                               rec_trx_lines.batch_source_name,
                               rec_trx_lines.interface_line_attribute2,
                               rec_trx_lines.interface_line_attribute1,
                               rec_trx_lines.interface_line_attribute3,
                               rec_trx_lines.orig_system_ship_address_id,
                               rec_trx_lines.cust_trx_type_id,
                               rec_trx_lines.sales_order,
                               l_trx_number,
                               TRUNC (
                                  NVL (rec_trx_lines.ship_date_actual,
                                       SYSDATE)),
                               NVL (
                                  rec_trx_lines.orig_system_bill_contact_id,
                                  l_orig_system_bill_contact_id),
                               NVL (
                                  rec_trx_lines.orig_system_ship_contact_id,
                                  l_orig_system_ship_contact_id),
                               NVL (rec_trx_lines.purchase_order,
                                    l_purchase_order),
                               rec_trx_lines.interface_line_attribute10,
                               rec_trx_lines.warehouse_id);
               END IF;

               l_sec := 'Update trx_number ';

               BEGIN
                  UPDATE ra_interface_lines
                     SET trx_number = l_trx_number
                   WHERE ROWID = rec_trx_lines.row_id AND trx_number IS NULL;
               EXCEPTION
                  WHEN OTHERS
                  THEN
                     l_error_msg :=
                        ' Failed to Update trx_number with ' || l_trx_number;

                     RAISE l_exception;
               END;
            END IF;
         EXCEPTION
            WHEN l_exception
            THEN
               fnd_file.put_line (
                  fnd_file.LOG,
                  'Error : ' || l_error_record || l_error_msg);
               fnd_file.put_line (fnd_file.LOG, SQLERRM);
               p_retcode := 2;
               p_error_msg := SUBSTR ( (SQLERRM || '-' || l_error_msg), 4000);
               xxcus_error_pkg.xxcus_error_main_api (
                  p_called_from         => 'XXWC_AR_INV_PREPROCESS_PKG.Gen_trx_number',
                  p_calling             => l_sec,
                  p_request_id          => fnd_global.conc_request_id,
                  p_ora_error_msg       => SUBSTR (
                                                DBMS_UTILITY.format_error_stack ()
                                             || DBMS_UTILITY.format_error_backtrace ()
                                             || SQLERRM,
                                             1,
                                             2000),
                  p_error_desc          => SUBSTR (l_error_record || l_error_msg,
                                                   1,
                                                   240),
                  p_distribution_list   => l_distro_list,
                  p_module              => 'AR');
         END;
      END LOOP;

      fnd_file.put_line (
         fnd_file.LOG,
            'Gen_trx_number End at :'
         || TO_CHAR (SYSDATE, 'DD-MON-YYYY HH:MI:SS'));
   EXCEPTION
      WHEN OTHERS
      THEN
         --submit notification to distribution list

         fnd_file.put_line (fnd_file.LOG,
                            'Error : ' || l_error_record || l_error_msg);
         fnd_file.put_line (fnd_file.LOG, SQLERRM);

         p_retcode := 2;
         p_error_msg := SUBSTR ( (SQLERRM || '-' || l_error_msg), 4000);

         xxcus_error_pkg.xxcus_error_main_api (
            p_called_from         => 'XXWC_AR_INV_PREPROCESS_PKG.Gen_trx_number',
            p_calling             => l_sec,
            p_request_id          => fnd_global.conc_request_id,
            p_ora_error_msg       => SUBSTR (
                                          DBMS_UTILITY.format_error_stack ()
                                       || DBMS_UTILITY.format_error_backtrace ()
                                       || SQLERRM,
                                       1,
                                       2000),
            p_error_desc          => SUBSTR (l_error_record || l_error_msg,
                                             1,
                                             240),
            p_distribution_list   => l_distro_list,
            p_module              => 'AR');
   END gen_trx_number;

   PROCEDURE gen_gl_account (p_retcode        OUT NUMBER,
                             p_error_msg      OUT VARCHAR2,
                             p_region      IN     VARCHAR2)
   IS
      /* **************************************************************************
         *   Procedure Name: gen_gl_account
         *
         *   PURPOSE:   This procedure has the logic from the triggers
         *              XXWC_RA_INTERFACE_LINES_ALL_BI
         *
         *  To assign the Location Segment on the Revenue account based upon an item's shipping organization
         *
         *   Logic:     1) Get the location (segment2) FROM the shipping organization's material account
         *              2) Keep the remaining segments as it is.
         *              3) Insert corrected segment2 and required values in RA_INTERFACE_DISTRIBUTIONS
         *              4) Assuming only 7 segments in chart of the account
         *              5) Assume newly derived account exits in gl_code_cobinations or dynamic insertion is turned on to create it
         *
         *   REVISIONS:
         *   Ver        Date         Author                     Description
         *   ---------  ----------   ---------------            -------------------------
         *   1.0        29-Sep-2016  Neha Saini         Initial Version TMS# 20150527-00318 Forego Two AR Triggers
         * ***************************************************************************/

      CURSOR cur_trx_lines
      IS
         SELECT ril.*
           FROM ra_interface_lines ril
          WHERE     interface_line_context = 'ORDER ENTRY'
                AND NOT EXISTS
                       (SELECT 'x'
                          FROM ra_interface_distributions rid
                         WHERE     rid.interface_line_context = 'ORDER ENTRY'
                               AND rid.interface_line_attribute1 =
                                      ril.interface_line_attribute1
                               AND rid.interface_line_attribute6 =
                                      ril.interface_line_attribute6)
                AND ril.trx_number IS NOT NULL
                AND EXISTS
                       (SELECT 'x'
                          FROM mtl_parameters
                         WHERE    (    TO_CHAR (organization_id) =
                                          ril.interface_line_attribute10
                                   AND attribute9 = p_region)
                               OR p_region IS NULL);


      l_new_segment2         VARCHAR2 (30); --segment2 FROM the material account on material parameters
      l_segment1             VARCHAR2 (30); --segment1 FROM the derived revenue account
      l_segment2             VARCHAR2 (30); --segment2 FROM the derived revenue account
      l_segment3             VARCHAR2 (30); --segment3 FROM the derived revenue account
      l_segment4             VARCHAR2 (30); --segment4 FROM the derived revenue account
      l_segment4_new         VARCHAR2 (30); --segment4 FROM the derived revenue account
      l_segment5             VARCHAR2 (30); --segment5 FROM the derived revenue account
      l_segment6             VARCHAR2 (30); --segment6 FROM the derived revenue account
      l_segment7             VARCHAR2 (30); --segment7 FROM the derived revenue account
      l_applicable           VARCHAR2 (1); --determined if trigger profile option enabled user profile name = wc: ra interface lines trigger,    --XXWC_RA_INTERFACE_LINES_TRIGGER
      l_material_account     NUMBER;    --material account FROM mtl_parameters

      l_exception            EXCEPTION;                     --custom EXCEPTION
      l_new_ccid             NUMBER;  --new code combination id for gl account
      l_account_code         VARCHAR2 (10);                     --account_code
      l_item_type            VARCHAR2 (30);
      l_exists               VARCHAR2 (1);
      l_header_segment2      VARCHAR2 (30); --segment2 from the sales order header

      --Added for Exception Handling--
      l_sec                  VARCHAR2 (5000) DEFAULT 'START';
      l_error_msg            VARCHAR2 (2000);
      l_message              VARCHAR2 (2000);

      l_direct_acct          ar_lookups.externally_visible_flag%TYPE;
      l_regular_acct         ar_lookups.description%TYPE;
      l_int_comp_acct        ar_lookups.description%TYPE;
      l_item_src             oe_order_lines_all.source_type_code%TYPE;
      l_ordr_line_category   oe_transaction_types_all.order_category_code%TYPE;
      g_dflt_cat_class       VARCHAR2 (5) := 'DFLT';

      l_status               BOOLEAN;
      l_concat_segs          VARCHAR2 (80);
      l_coa_id               NUMBER;
      l_error_record         VARCHAR2 (240);
   BEGIN
      --Obtain the 'Y' or 'N' value FROM the profile option XXWC_RA_INTERFACE_LINES_TRIGGER
      l_applicable := fnd_profile.VALUE ('XXWC_RA_INTERFACE_LINES_TRIGGER');

      fnd_file.put_line (
         fnd_file.LOG,
            'Gen_gl_account Start at :'
         || TO_CHAR (SYSDATE, 'DD-MON-YYYY HH:MI:SS'));

      FOR rec_trx_lines IN cur_trx_lines
      LOOP
         l_error_record :=
               'SO# '
            || rec_trx_lines.interface_line_attribute1
            || ' Line Id '
            || rec_trx_lines.interface_line_attribute6
            || ' ';
         fnd_file.put_line (
            fnd_file.LOG,
            'Gen_gl_account NEW LINE Starts:' || l_error_record);


         BEGIN
            --if the profile option is enabled THEN execute code, otherwise no logic to change the location code
            IF l_applicable = 'Y'
            THEN
               BEGIN
                  -- Get chart of accounts ID.
                  SELECT chart_of_accounts_id
                    INTO l_coa_id
                    FROM org_organization_definitions ood
                   WHERE     ood.operating_unit = rec_trx_lines.org_id
                         AND ROWNUM = 1;
               EXCEPTION
                  WHEN OTHERS
                  THEN
                     fnd_file.put_line (
                        fnd_file.LOG,
                           'Gen_gl_account l_message :'
                        || 'cannot find l_cao_id');
                     RAISE l_exception;
               END;

               fnd_file.put_line (fnd_file.LOG,
                                  'Gen_gl_account l_coa_id :' || l_coa_id);
               l_sec := '100';

               --get the material_account from interface_line_attribute10 or warehouse_id on ra_interface_lines_all --For the Line Level
               BEGIN
                  SELECT mp.material_account, gcc.segment2
                    INTO l_material_account, l_new_segment2
                    FROM mtl_parameters mp, gl_code_combinations gcc
                   WHERE     mp.organization_id =
                                NVL (
                                   rec_trx_lines.interface_line_attribute10,
                                   rec_trx_lines.warehouse_id)
                         AND mp.material_account = gcc.code_combination_id;
               EXCEPTION
                  WHEN OTHERS
                  THEN
                     l_error_msg := l_error_msg || ' ' || SQLERRM;
                     l_message :=
                           l_message
                        || ' '
                        || 'Unable to find material account and segment2';
                     l_material_account := NULL;
                     l_new_segment2 := NULL;
                     fnd_file.put_line (
                        fnd_file.LOG,
                        'Gen_gl_account l_message :' || l_message);
               END;

               fnd_file.put_line (
                  fnd_file.LOG,
                  'Gen_gl_account l_new_segment2 :' || l_new_segment2);
               fnd_file.put_line (
                  fnd_file.LOG,
                  'Gen_gl_account l_material_account :' || l_material_account);
               --Get the owning Warehouse Header Segment2 for the Rec account
               l_sec := 200;

               BEGIN
                  SELECT gcc.segment2
                    INTO l_header_segment2
                    FROM mtl_parameters mp,
                         gl_code_combinations gcc,
                         oe_order_headers ooha
                   WHERE     mp.organization_id =
                                NVL (
                                   ooha.ship_from_org_id,
                                   NVL (
                                      rec_trx_lines.interface_line_attribute10,
                                      rec_trx_lines.warehouse_id))
                         AND mp.material_account = gcc.code_combination_id
                         AND ooha.org_id = rec_trx_lines.org_id
                         AND ooha.order_number =
                                rec_trx_lines.interface_line_attribute1;
               EXCEPTION
                  WHEN OTHERS
                  THEN
                     l_error_msg := l_error_msg || SQLERRM;
                     l_message :=
                           l_message
                        || 'Unable to find warehouse segment2 for the rec account';
                     fnd_file.put_line (
                        fnd_file.LOG,
                        'Gen_gl_account l_message :' || l_message);
               END;

               fnd_file.put_line (
                  fnd_file.LOG,
                  'Gen_gl_account l_header_segment2 :' || l_header_segment2);
               fnd_file.put_line (
                  fnd_file.LOG,
                     'Gen_gl_account rec_trx_lines.warehouse_id :'
                  || rec_trx_lines.warehouse_id);

               BEGIN
                  SELECT a.item_type
                    INTO l_item_type
                    FROM mtl_system_items a
                   WHERE     a.organization_id =
                                NVL (
                                   rec_trx_lines.interface_line_attribute10,
                                   rec_trx_lines.warehouse_id)
                         AND a.inventory_item_id =
                                rec_trx_lines.inventory_item_id;
               EXCEPTION
                  WHEN OTHERS
                  THEN
                     l_item_type := 'LINE';
               END;

               fnd_file.put_line (
                  fnd_file.LOG,
                  'Gen_gl_account l_item_type :' || l_item_type);

               --Find the revenue account based on the transaction_type setup
               IF rec_trx_lines.line_type = 'LINE' AND l_item_type <> 'FRT'
               THEN
                  l_account_code := 'REV';

                  l_sec := 300;
                  fnd_file.put_line (fnd_file.LOG,
                                     'Gen_gl_account l_sec :' || l_sec);

                  IF rec_trx_lines.inventory_item_id IS NOT NULL
                  THEN
                     BEGIN
                        SELECT gcc.segment1,
                               gcc.segment2,
                               gcc.segment3,
                               gcc.segment4,
                               gcc.segment5,
                               gcc.segment6,
                               gcc.segment7
                          INTO l_segment1,
                               l_segment2,
                               l_segment3,
                               l_segment4,
                               l_segment5,
                               l_segment6,
                               l_segment7
                          FROM mtl_system_items_b msib,
                               gl_code_combinations gcc
                         WHERE     msib.inventory_item_id =
                                      rec_trx_lines.inventory_item_id
                               AND msib.organization_id =
                                      NVL (
                                         rec_trx_lines.interface_line_attribute10,
                                         rec_trx_lines.warehouse_id)
                               AND msib.sales_account =
                                      gcc.code_combination_id;       --REVENUE

                        l_sec := 'populating for revenue';
                        fnd_file.put_line (fnd_file.LOG,
                                           'Gen_gl_account l_sec :' || l_sec);

                        fnd_file.put_line (
                           fnd_file.LOG,
                           'Gen_gl_account l_segment1 :' || l_segment1);
                        fnd_file.put_line (
                           fnd_file.LOG,
                           'Gen_gl_account l_segment1 :' || l_segment2);
                        fnd_file.put_line (
                           fnd_file.LOG,
                           'Gen_gl_account l_segment1 :' || l_segment3);
                        fnd_file.put_line (
                           fnd_file.LOG,
                           'Gen_gl_account l_segment1 :' || l_segment4);
                        fnd_file.put_line (
                           fnd_file.LOG,
                           'Gen_gl_account l_segment1 :' || l_segment5);
                        fnd_file.put_line (
                           fnd_file.LOG,
                           'Gen_gl_account l_segment1 :' || l_segment6);
                        fnd_file.put_line (
                           fnd_file.LOG,
                           'Gen_gl_account l_segment1 :' || l_segment7);
                     EXCEPTION
                        WHEN OTHERS
                        THEN
                           l_error_msg := l_error_msg || ' ' || SQLERRM;
                           l_message :=
                                 l_message
                              || ' '
                              || 'Unable to find item revenue account for inventory_item_id '
                              || rec_trx_lines.inventory_item_id
                              || ' and organization_id '
                              || NVL (
                                    rec_trx_lines.interface_line_attribute10,
                                    rec_trx_lines.warehouse_id);
                           fnd_file.put_line (
                              fnd_file.LOG,
                              'Gen_gl_account l_message :' || l_message);
                     END;

                     -- Sub-Ledger Accounting REV> Start

                     ----------------------------------------------------------------------------------
                     -- Check if the Customer is an Intercompany Customer
                     ----------------------------------------------------------------------------------
                     BEGIN
                        SELECT ar_l.description
                          INTO l_int_comp_acct
                          FROM ar_lookups ar_l, hz_cust_accounts hca
                         WHERE     ar_l.lookup_type =
                                      'XXWC_INTERCOMPANY_ACCOUNT'
                               AND ar_l.enabled_flag = 'Y'
                               AND SYSDATE BETWEEN ar_l.start_date_active
                                               AND NVL (ar_l.end_date_active,
                                                        SYSDATE + 1)
                               AND ar_l.lookup_code = hca.account_number
                               AND hca.cust_account_id =
                                      TO_CHAR (
                                         rec_trx_lines.orig_system_bill_customer_id);
                     EXCEPTION
                        WHEN OTHERS
                        THEN
                           l_int_comp_acct := NULL;
                     END;

                     fnd_file.put_line (
                        fnd_file.LOG,
                        'Gen_gl_account l_int_comp_acct :' || l_int_comp_acct);
                     l_sec :=
                           'Check if the Customer is an Intercompany Customer l_int_comp_acct '
                        || l_int_comp_acct;
                     fnd_file.put_line (fnd_file.LOG,
                                        'Gen_gl_account l_sec :' || l_sec);


                     IF l_int_comp_acct IS NOT NULL
                     THEN
                        l_segment4_new := l_int_comp_acct;
                     ELSE
                        ----------------------------------------------------------------------------------
                        -- Derive the Order Line Category (ORDER / RETURN)
                        ----------------------------------------------------------------------------------
                        BEGIN
                           SELECT ottl.order_category_code
                             INTO l_ordr_line_category
                             FROM oe_order_lines ool,
                                  oe_transaction_types ottl
                            WHERE     1 = 1
                                  AND ool.line_id =
                                         rec_trx_lines.interface_line_attribute6
                                  AND ool.line_type_id =
                                         ottl.transaction_type_id
                                  AND ottl.transaction_type_code = 'LINE'
                                  AND ool.org_id = ottl.org_id;
                        EXCEPTION
                           WHEN OTHERS
                           THEN
                              l_item_src := 'INTERNAL';
                        END;

                        fnd_file.put_line (
                           fnd_file.LOG,
                              'Gen_gl_account l_ordr_line_category :'
                           || l_ordr_line_category);

                        ----------------------------------------------------------------------------------
                        -- Derive Order Line Source Type (INTERNAL / EXTERNAL)
                        ----------------------------------------------------------------------------------
                        IF l_ordr_line_category = 'RETURN'
                        THEN
                           BEGIN
                              SELECT ool_ref.source_type_code
                                INTO l_item_src
                                FROM oe_order_lines ool,
                                     oe_order_lines ool_ref
                               WHERE     1 = 1
                                     AND ool.line_id =
                                            rec_trx_lines.interface_line_attribute6
                                     AND ool.reference_line_id =
                                            ool_ref.line_id;
                           EXCEPTION
                              WHEN OTHERS
                              THEN
                                 l_item_src := 'INTERNAL';
                           END;

                           fnd_file.put_line (
                              fnd_file.LOG,
                              'Gen_gl_account l_item_src :' || l_item_src);
                        ELSE
                           BEGIN
                              SELECT ool.source_type_code
                                INTO l_item_src
                                FROM oe_order_lines ool
                               WHERE     1 = 1
                                     AND line_id =
                                            rec_trx_lines.interface_line_attribute6;
                           EXCEPTION
                              WHEN OTHERS
                              THEN
                                 l_item_src := 'INTERNAL';
                           END;

                           fnd_file.put_line (
                              fnd_file.LOG,
                              'Gen_gl_account l_item_src :' || l_item_src);
                        END IF;

                        l_sec :=
                              'Derive Order Line Source Type (INTERNAL / EXTERNAL) l_item_src '
                           || l_item_src;
                        fnd_file.put_line (fnd_file.LOG,
                                           'Gen_gl_account l_sec :' || l_sec);

                        ----------------------------------------------------------------------------------
                        -- Derive Revenue Account value from Lookup - XXWC_REVENUE_ACCOUNT
                        ----------------------------------------------------------------------------------
                        BEGIN
                           SELECT arl.externally_visible_flag,
                                  arl.description
                             INTO l_direct_acct, l_regular_acct
                             FROM mtl_item_categories_v micv, ar_lookups arl
                            WHERE     1 = 1
                                  AND arl.lookup_type =
                                         'XXWC_REVENUE_ACCOUNT'
                                  AND arl.lookup_code = micv.segment2
                                  AND SYSDATE BETWEEN arl.start_date_active
                                                  AND NVL (
                                                         arl.end_date_active,
                                                         SYSDATE + 1)
                                  AND NVL (arl.enabled_flag, 'Y') = 'Y'
                                  AND micv.category_set_name =
                                         'Inventory Category'
                                  AND micv.inventory_item_id =
                                         rec_trx_lines.inventory_item_id
                                  AND micv.organization_id =
                                         NVL (
                                            rec_trx_lines.interface_line_attribute10,
                                            rec_trx_lines.warehouse_id);

                           fnd_file.put_line (
                              fnd_file.LOG,
                                 'Gen_gl_account l_direct_acct :'
                              || l_direct_acct);
                           fnd_file.put_line (
                              fnd_file.LOG,
                                 'Gen_gl_account l_regular_acct :'
                              || l_regular_acct);
                        EXCEPTION
                           WHEN OTHERS
                           THEN
                              BEGIN
                                 SELECT externally_visible_flag, description
                                   INTO l_direct_acct, l_regular_acct
                                   FROM ar_lookups
                                  WHERE     lookup_type =
                                               'XXWC_REVENUE_ACCOUNT'
                                        AND lookup_code = g_dflt_cat_class
                                        AND SYSDATE BETWEEN start_date_active
                                                        AND NVL (
                                                               end_date_active,
                                                               SYSDATE + 1)
                                        AND NVL (enabled_flag, 'Y') = 'Y';

                                 fnd_file.put_line (
                                    fnd_file.LOG,
                                       'Gen_gl_account in exception l_direct_acct :'
                                    || l_direct_acct);
                                 fnd_file.put_line (
                                    fnd_file.LOG,
                                       'Gen_gl_account in exception l_regular_acct :'
                                    || l_regular_acct);
                              EXCEPTION
                                 WHEN OTHERS
                                 THEN
                                    NULL;
                              END;
                        END;



                        IF l_item_src = 'INTERNAL'
                        THEN
                           l_segment4_new := l_regular_acct;
                        ELSE
                           l_segment4_new := l_direct_acct;
                        END IF;
                     END IF;

                     fnd_file.put_line (
                        fnd_file.LOG,
                        'Gen_gl_account l_segment4_new :' || l_segment4_new); -- Intercompany
                  -- Sub-Ledger Accounting REV < End
                  ELSE
                     l_sec := 'I am in ELSE HERE if l_int_comp_acct is null ';
                     fnd_file.put_line (
                        fnd_file.LOG,
                           'Gen_gl_account l_int_comp_acct :'
                        || l_sec
                        || l_int_comp_acct);

                     BEGIN
                        SELECT gcc.segment1,
                               gcc.segment2,
                               gcc.segment3,
                               gcc.segment4,
                               gcc.segment5,
                               gcc.segment6,
                               gcc.segment7
                          INTO l_segment1,
                               l_segment2,
                               l_segment3,
                               l_segment4,
                               l_segment5,
                               l_segment6,
                               l_segment7
                          FROM ra_cust_trx_types rctta,
                               gl_code_combinations gcc
                         WHERE     rctta.cust_trx_type_id =
                                      rec_trx_lines.cust_trx_type_id
                               AND rctta.gl_id_rev = gcc.code_combination_id; --REVENUE

                        fnd_file.put_line (
                           fnd_file.LOG,
                           'Gen_gl_account l_segment1 :' || l_segment1);
                        fnd_file.put_line (
                           fnd_file.LOG,
                           'Gen_gl_account l_segment1 :' || l_segment2);
                        fnd_file.put_line (
                           fnd_file.LOG,
                           'Gen_gl_account l_segment1 :' || l_segment3);
                        fnd_file.put_line (
                           fnd_file.LOG,
                           'Gen_gl_account l_segment1 :' || l_segment4);
                        fnd_file.put_line (
                           fnd_file.LOG,
                           'Gen_gl_account l_segment1 :' || l_segment5);
                        fnd_file.put_line (
                           fnd_file.LOG,
                           'Gen_gl_account l_segment1 :' || l_segment6);
                        fnd_file.put_line (
                           fnd_file.LOG,
                           'Gen_gl_account l_segment1 :' || l_segment7);
                     EXCEPTION
                        WHEN OTHERS
                        THEN
                           l_error_msg := SQLCODE || SQLERRM;
                           l_message :=
                                 'Unable to find revenue account for cust_trx_type_id '
                              || rec_trx_lines.cust_trx_type_id;
                           fnd_file.put_line (
                              fnd_file.LOG,
                              'Gen_gl_account l_message :' || l_message);

                           RAISE l_exception;
                     END;
                  END IF;

                  --Find the freight account based on the transaction_type setup
                  l_sec := 400;
               ELSIF    rec_trx_lines.line_type = 'FREIGHT'
                     OR l_item_type = 'FRT'
               THEN
                  l_sec := 'line type is freight so here ';
                  fnd_file.put_line (fnd_file.LOG,
                                     'Gen_gl_account l_sec :' || l_sec);

                  IF rec_trx_lines.line_type = 'FREIGHT'
                  THEN
                     l_account_code := 'FREIGHT';
                  ELSE
                     l_account_code := 'REV';
                  END IF;

                  fnd_file.put_line (
                     fnd_file.LOG,
                        'Gen_gl_account freight account based on the line type  l_account_code :'
                     || l_account_code);

                  IF rec_trx_lines.inventory_item_id IS NOT NULL
                  THEN
                     l_sec :=
                        'inventory_item_id is not null so getting segments from here  based on item and warehouse';
                     fnd_file.put_line (fnd_file.LOG,
                                        'Gen_gl_account l_sec :' || l_sec);

                     BEGIN
                        SELECT gcc.segment1,
                               gcc.segment2,
                               gcc.segment3,
                               gcc.segment4,
                               gcc.segment5,
                               gcc.segment6,
                               gcc.segment7
                          INTO l_segment1,
                               l_segment2,
                               l_segment3,
                               l_segment4,
                               l_segment5,
                               l_segment6,
                               l_segment7
                          FROM mtl_system_items_b msib,
                               gl_code_combinations gcc
                         WHERE     msib.inventory_item_id =
                                      rec_trx_lines.inventory_item_id
                               AND msib.organization_id =
                                      NVL (
                                         rec_trx_lines.interface_line_attribute10,
                                         rec_trx_lines.warehouse_id)
                               AND msib.sales_account =
                                      gcc.code_combination_id;

                        fnd_file.put_line (
                           fnd_file.LOG,
                           'Gen_gl_account l_segment1 :' || l_segment1);
                        fnd_file.put_line (
                           fnd_file.LOG,
                           'Gen_gl_account l_segment1 :' || l_segment2);
                        fnd_file.put_line (
                           fnd_file.LOG,
                           'Gen_gl_account l_segment1 :' || l_segment3);
                        fnd_file.put_line (
                           fnd_file.LOG,
                           'Gen_gl_account l_segment1 :' || l_segment4);
                        fnd_file.put_line (
                           fnd_file.LOG,
                           'Gen_gl_account l_segment1 :' || l_segment5);
                        fnd_file.put_line (
                           fnd_file.LOG,
                           'Gen_gl_account l_segment1 :' || l_segment6);
                        fnd_file.put_line (
                           fnd_file.LOG,
                           'Gen_gl_account l_segment1 :' || l_segment7);
                     EXCEPTION
                        WHEN OTHERS
                        THEN
                           l_error_msg := l_error_msg || ' ' || SQLERRM;
                           l_message :=
                                 l_message
                              || ' '
                              || 'Unable to find item freight account for inventory_item_id '
                              || rec_trx_lines.inventory_item_id
                              || ' and organization_id '
                              || NVL (
                                    rec_trx_lines.interface_line_attribute10,
                                    rec_trx_lines.warehouse_id);
                           fnd_file.put_line (
                              fnd_file.LOG,
                              'Gen_gl_account l_message :' || l_message);
                     END;
                  ELSE
                     l_sec :=
                        'inventory_item_id is null so getting segments from here  ';
                     fnd_file.put_line (fnd_file.LOG,
                                        'Gen_gl_account l_sec :' || l_sec);

                     BEGIN
                        SELECT gcc.segment1,
                               gcc.segment2,
                               gcc.segment3,
                               gcc.segment4,
                               gcc.segment5,
                               gcc.segment6,
                               gcc.segment7
                          INTO l_segment1,
                               l_segment2,
                               l_segment3,
                               l_segment4,
                               l_segment5,
                               l_segment6,
                               l_segment7
                          FROM ra_cust_trx_types rctta,
                               gl_code_combinations gcc
                         WHERE     rctta.cust_trx_type_id =
                                      rec_trx_lines.cust_trx_type_id
                               AND rctta.gl_id_freight =
                                      gcc.code_combination_id;       --FREIGHT

                        fnd_file.put_line (
                           fnd_file.LOG,
                           'Gen_gl_account l_segment1 :' || l_segment1);
                        fnd_file.put_line (
                           fnd_file.LOG,
                           'Gen_gl_account l_segment1 :' || l_segment2);
                        fnd_file.put_line (
                           fnd_file.LOG,
                           'Gen_gl_account l_segment1 :' || l_segment3);
                        fnd_file.put_line (
                           fnd_file.LOG,
                           'Gen_gl_account l_segment1 :' || l_segment4);
                        fnd_file.put_line (
                           fnd_file.LOG,
                           'Gen_gl_account l_segment1 :' || l_segment5);
                        fnd_file.put_line (
                           fnd_file.LOG,
                           'Gen_gl_account l_segment1 :' || l_segment6);
                        fnd_file.put_line (
                           fnd_file.LOG,
                           'Gen_gl_account l_segment1 :' || l_segment7);
                     EXCEPTION
                        WHEN OTHERS
                        THEN
                           l_error_msg := l_error_msg || ' ' || SQLERRM;
                           l_message :=
                                 l_message
                              || ' '
                              || 'Unable to find revenue account for cust_trx_type_id '
                              || rec_trx_lines.cust_trx_type_id;
                           fnd_file.put_line (
                              fnd_file.LOG,
                              'Gen_gl_account l_message :' || l_message);
                     END;
                  END IF;
               --Find the tax account based on the transaction_type setup

               ELSIF rec_trx_lines.line_type = 'TAX'
               THEN
                  l_sec :=
                     ' Find the tax account based on the transaction_type setup --- Line type is TAX so getting segments here ';
                  fnd_file.put_line (fnd_file.LOG,
                                     'Gen_gl_account l_sec :' || l_sec);
                  l_account_code := 'TAX';

                  BEGIN
                     SELECT gcc.segment1,
                            gcc.segment2,
                            gcc.segment3,
                            gcc.segment4,
                            gcc.segment5,
                            gcc.segment6,
                            gcc.segment7
                       INTO l_segment1,
                            l_segment2,
                            l_segment3,
                            l_segment4,
                            l_segment5,
                            l_segment6,
                            l_segment7
                       FROM ra_cust_trx_types rctta, gl_code_combinations gcc
                      WHERE     rctta.cust_trx_type_id =
                                   rec_trx_lines.cust_trx_type_id
                            AND rctta.gl_id_tax = gcc.code_combination_id; --TAX

                     fnd_file.put_line (
                        fnd_file.LOG,
                        'Gen_gl_account l_segment1 :' || l_segment1);
                     fnd_file.put_line (
                        fnd_file.LOG,
                        'Gen_gl_account l_segment1 :' || l_segment2);
                     fnd_file.put_line (
                        fnd_file.LOG,
                        'Gen_gl_account l_segment1 :' || l_segment3);
                     fnd_file.put_line (
                        fnd_file.LOG,
                        'Gen_gl_account l_segment1 :' || l_segment4);
                     fnd_file.put_line (
                        fnd_file.LOG,
                        'Gen_gl_account l_segment1 :' || l_segment5);
                     fnd_file.put_line (
                        fnd_file.LOG,
                        'Gen_gl_account l_segment1 :' || l_segment6);
                     fnd_file.put_line (
                        fnd_file.LOG,
                        'Gen_gl_account l_segment1 :' || l_segment7);
                  EXCEPTION
                     WHEN OTHERS
                     THEN
                        l_error_msg := l_error_msg || ' ' || SQLERRM;
                        l_message :=
                              l_message
                           || ' '
                           || 'Unable to find revenue account for cust_trx_type_id '
                           || rec_trx_lines.cust_trx_type_id;

                        fnd_file.put_line (
                           fnd_file.LOG,
                           'Gen_gl_account l_message :' || l_message);
                  END;
               END IF;

               BEGIN
                  l_sec := 'now here the concate segment ';
                  fnd_file.put_line (fnd_file.LOG,
                                     'Gen_gl_account l_sec :' || l_sec);
                  l_concat_segs :=
                        l_segment1
                     || '.'
                     || l_new_segment2
                     || '.'
                     || l_segment3
                     || '.'
                     || NVL (l_segment4_new, l_segment4) --version 6.0  2/13/2013 nvl on segment4 KPoling
                     || '.'
                     || l_segment5
                     || '.'
                     || l_segment6
                     || '.'
                     || l_segment7;
                  fnd_file.put_line (
                     fnd_file.LOG,
                     'Gen_gl_account l_concat_segs :' || l_concat_segs);
                  l_status :=
                     fnd_flex_keyval.validate_segs ('CREATE_COMBINATION' --operation
                                                                        ,
                                                    'SQLGL'  --appl_short_name
                                                           ,
                                                    'GL#'      --key_flex_code
                                                         ,
                                                    l_coa_id --structure_number
                                                            ,
                                                    l_concat_segs --concat_segments
                                                                 ,
                                                    'V'        --values_or_ids
                                                       ,
                                                    SYSDATE  --validation_date
                                                           ,
                                                    'ALL'        --displayable
                                                         ,
                                                    NULL            --data_set
                                                        ,
                                                    NULL               --vrule
                                                        ,
                                                    NULL        --where_clause
                                                        ,
                                                    NULL         --get_columns
                                                        ,
                                                    FALSE        --allow_nulls
                                                         ,
                                                    FALSE      --allow_orphans
                                                         ,
                                                    NULL       --allow_orphans
                                                        ,
                                                    NULL             --resp_id
                                                        ,
                                                    NULL             --user_id
                                                        ,
                                                    NULL --select_comb_from_view
                                                        ,
                                                    NULL          --no_combmsg
                                                        ,
                                                    NULL    --where_clause_msg
                                                        );

                  IF l_status
                  THEN
                     l_new_ccid := fnd_flex_keyval.combination_id ();
                     fnd_file.put_line (fnd_file.LOG,
                                        'l_new_ccid' || l_new_ccid);
                  END IF;

                  l_sec := ' New  l_new_ccid HERE ' || l_new_ccid;
                  fnd_file.put_line (fnd_file.LOG,
                                     'Gen_gl_account l_sec :' || l_sec);
               EXCEPTION
                  WHEN NO_DATA_FOUND
                  THEN
                     fnd_file.put_line (
                        fnd_file.output,
                        'Cannot create ccid for REV :  ' || l_concat_segs);
               END;

               BEGIN
                  --insert INTO ra_interface_distributions table required values FROM ra_interface_lines
                  l_sec :=
                     'insert INTO ra_interface_distributions table required values FROM ra_interface_lines populating new ccid';
                  fnd_file.put_line (fnd_file.LOG,
                                     'Gen_gl_account l_sec :' || l_sec);

                  INSERT
                    INTO ra_interface_distributions (
                            interface_line_context,
                            interface_line_attribute1,
                            interface_line_attribute2,
                            interface_line_attribute3,
                            interface_line_attribute4,
                            interface_line_attribute5,
                            interface_line_attribute6,
                            interface_line_attribute7,
                            interface_line_attribute8,
                            interface_line_attribute9,
                            interface_line_attribute10,
                            interface_line_attribute11,
                            interface_line_attribute12,
                            interface_line_attribute13,
                            interface_line_attribute14,
                            interface_line_attribute15,
                            amount,
                            account_class,
                            acctd_amount,
                            code_combination_id,
                            percent,
                            org_id)
                  VALUES (rec_trx_lines.interface_line_context,
                          rec_trx_lines.interface_line_attribute1,
                          rec_trx_lines.interface_line_attribute2,
                          rec_trx_lines.interface_line_attribute3,
                          rec_trx_lines.interface_line_attribute4,
                          rec_trx_lines.interface_line_attribute5,
                          rec_trx_lines.interface_line_attribute6,
                          rec_trx_lines.interface_line_attribute7,
                          rec_trx_lines.interface_line_attribute8,
                          rec_trx_lines.interface_line_attribute9,
                          rec_trx_lines.interface_line_attribute10,
                          rec_trx_lines.interface_line_attribute11,
                          rec_trx_lines.interface_line_attribute12,
                          rec_trx_lines.interface_line_attribute13,
                          rec_trx_lines.interface_line_attribute14,
                          rec_trx_lines.interface_line_attribute15,
                          rec_trx_lines.amount,
                          l_account_code,
                          rec_trx_lines.acctd_amount,
                          l_new_ccid,
                          '100',
                          rec_trx_lines.org_id);

                  BEGIN
                     l_sec := 'check for REC account CLASS line existence';
                     fnd_file.put_line (fnd_file.LOG,
                                        'Gen_gl_account l_sec :' || l_sec);

                     SELECT 'x'
                       INTO l_exists
                       FROM ra_interface_distributions rid,
                            ra_interface_lines rila
                      WHERE     rid.interface_line_context = 'ORDER ENTRY'
                            AND rid.interface_line_attribute1 =
                                   rila.interface_line_attribute1
                            AND rid.interface_line_attribute1 =
                                   rec_trx_lines.interface_line_attribute1
						    AND rid.interface_line_attribute6 = rec_trx_lines.interface_line_attribute6
                            AND rila.trx_number = rec_trx_lines.trx_number
                            AND rid.interface_line_id IS NULL
                            AND rila.trx_number IS NOT NULL
                            AND rid.account_class = 'REC'
                            AND rila.trx_number =
                                   (SELECT trx_number
                                      FROM RA_INTERFACE_LINES
                                     WHERE interface_line_attribute6 =
                                              rec_trx_lines.interface_line_attribute6)
                            AND ROWNUM = 1;

                     l_sec :=
                        'check for REC account CLASS line and it existence- ESISTS -- :)';
                     fnd_file.put_line (fnd_file.LOG,
                                        'Gen_gl_account l_sec :' || l_sec);
                  EXCEPTION
                     WHEN OTHERS
                     THEN
                        l_sec :=
                           'check for REC account CLASS line existence- NOT EXIST so populate higher code BW080';
                        fnd_file.put_line (fnd_file.LOG,
                                           'Gen_gl_account l_sec :' || l_sec);

                        SELECT gcc.segment1,
                               gcc.segment2,
                               gcc.segment3,
                               gcc.segment4,
                               gcc.segment5,
                               gcc.segment6,
                               gcc.segment7
                          INTO l_segment1,
                               l_segment2,
                               l_segment3,
                               l_segment4,
                               l_segment5,
                               l_segment6,
                               l_segment7
                          FROM ra_cust_trx_types rctta,
                               gl_code_combinations gcc
                         WHERE     rctta.cust_trx_type_id =
                                      rec_trx_lines.cust_trx_type_id
                               AND rctta.gl_id_rec = gcc.code_combination_id; --RECEIVABLE

                        fnd_file.put_line (
                           fnd_file.LOG,
                           'Gen_gl_account l_segment1 :' || l_segment1);
                        fnd_file.put_line (
                           fnd_file.LOG,
                           'Gen_gl_account l_segment1 :' || l_segment2);
                        fnd_file.put_line (
                           fnd_file.LOG,
                           'Gen_gl_account l_segment1 :' || l_segment3);
                        fnd_file.put_line (
                           fnd_file.LOG,
                           'Gen_gl_account l_segment1 :' || l_segment4);
                        fnd_file.put_line (
                           fnd_file.LOG,
                           'Gen_gl_account l_segment1 :' || l_segment5);
                        fnd_file.put_line (
                           fnd_file.LOG,
                           'Gen_gl_account l_segment1 :' || l_segment6);
                        fnd_file.put_line (
                           fnd_file.LOG,
                           'Gen_gl_account l_segment1 :' || l_segment7);

                        BEGIN
                           l_concat_segs :=
                                 l_segment1
                              || '.'
                              || l_header_segment2
                              || '.'
                              || l_segment3
                              || '.'
                              || l_segment4
                              || '.'
                              || l_segment5
                              || '.'
                              || l_segment6
                              || '.'
                              || l_segment7;
                           fnd_file.put_line (
                              fnd_file.LOG,
                                 'Gen_gl_account now here the new  l_concat_segs for REC :'
                              || l_concat_segs);
                           l_status :=
                              fnd_flex_keyval.validate_segs (
                                 'CREATE_COMBINATION'              --operation
                                                     ,
                                 'SQLGL'                     --appl_short_name
                                        ,
                                 'GL#'                         --key_flex_code
                                      ,
                                 l_coa_id                   --structure_number
                                         ,
                                 l_concat_segs               --concat_segments
                                              ,
                                 'V'                           --values_or_ids
                                    ,
                                 SYSDATE                     --validation_date
                                        ,
                                 'ALL'                           --displayable
                                      ,
                                 NULL                               --data_set
                                     ,
                                 NULL                                  --vrule
                                     ,
                                 NULL                           --where_clause
                                     ,
                                 NULL                            --get_columns
                                     ,
                                 FALSE                           --allow_nulls
                                      ,
                                 FALSE                         --allow_orphans
                                      ,
                                 NULL                          --allow_orphans
                                     ,
                                 NULL                                --resp_id
                                     ,
                                 NULL                                --user_id
                                     ,
                                 NULL                  --select_comb_from_view
                                     ,
                                 NULL                             --no_combmsg
                                     ,
                                 NULL                       --where_clause_msg
                                     );

                           IF l_status
                           THEN
                              l_new_ccid := fnd_flex_keyval.combination_id ();
                              fnd_file.put_line (
                                 fnd_file.LOG,
                                 'l_new_ccid for REC' || l_new_ccid);
                           END IF;
                        EXCEPTION
                           WHEN NO_DATA_FOUND
                           THEN
                              fnd_file.put_line (
                                 fnd_file.output,
                                    'Cannot create ccid for REC line :  '
                                 || l_concat_segs);
                        END;

                        l_sec :=
                           'inserting all new REC line in distribution interface -- :)';
                        fnd_file.put_line (fnd_file.LOG,
                                           'Gen_gl_account l_sec :' || l_sec);

                        INSERT
                          INTO ra_interface_distributions (
                                  interface_line_context,
                                  interface_line_attribute1,
                                  interface_line_attribute2,
                                  interface_line_attribute3,
                                  interface_line_attribute4,
                                  interface_line_attribute5,
                                  interface_line_attribute6,
                                  interface_line_attribute7,
                                  interface_line_attribute8,
                                  interface_line_attribute9,
                                  interface_line_attribute10,
                                  interface_line_attribute11,
                                  interface_line_attribute12,
                                  interface_line_attribute13,
                                  interface_line_attribute14,
                                  interface_line_attribute15,
                                  account_class,
                                  code_combination_id,
                                  percent,
                                  org_id)
                        VALUES (rec_trx_lines.interface_line_context,
                                rec_trx_lines.interface_line_attribute1,
                                rec_trx_lines.interface_line_attribute2,
                                rec_trx_lines.interface_line_attribute3,
                                rec_trx_lines.interface_line_attribute4,
                                rec_trx_lines.interface_line_attribute5,
                                rec_trx_lines.interface_line_attribute6,
                                rec_trx_lines.interface_line_attribute7,
                                rec_trx_lines.interface_line_attribute8,
                                rec_trx_lines.interface_line_attribute9,
                                rec_trx_lines.interface_line_attribute10,
                                rec_trx_lines.interface_line_attribute11,
                                rec_trx_lines.interface_line_attribute12,
                                rec_trx_lines.interface_line_attribute13,
                                rec_trx_lines.interface_line_attribute14,
                                rec_trx_lines.interface_line_attribute15,
                                'REC',
                                l_new_ccid,
                                '100',
                                rec_trx_lines.org_id);
                  END;
               EXCEPTION
                  WHEN OTHERS
                  THEN
                     l_error_msg := SQLCODE || SQLERRM;
                     l_message :=
                        'Unable to insert into  ra_interface_distributions table';
                     fnd_file.put_line (
                        fnd_file.LOG,
                        'Gen_gl_account l_message :' || l_message);
                     RAISE l_exception;
               END;
            END IF;

            l_sec :=
                  'done all code combinaiton logic here check line context and populate attributes accordingly -- :) rec_trx_lines.interface_line_context '
               || rec_trx_lines.interface_line_context;
            fnd_file.put_line (fnd_file.LOG,
                               'Gen_gl_account l_sec :' || l_sec);

            IF rec_trx_lines.interface_line_context = 'ORDER ENTRY'
            THEN
               rec_trx_lines.header_attribute1 := NULL;
               rec_trx_lines.header_attribute2 := NULL;
               rec_trx_lines.header_attribute3 := NULL;
               rec_trx_lines.header_attribute12 := NULL;
               rec_trx_lines.header_attribute14 := NULL;
               rec_trx_lines.header_attribute15 := NULL;
            END IF;

            COMMIT;

            IF l_error_msg IS NOT NULL
            THEN
               fnd_file.put_line (fnd_file.LOG,
                                  'Error : ' || l_error_record || l_message);
               fnd_file.put_line (fnd_file.LOG, l_error_msg);

               l_error_msg := NULL;
               l_message := NULL;
            END IF;
         EXCEPTION
            WHEN l_exception
            THEN
               fnd_file.put_line (fnd_file.LOG,
                                  'Error : ' || l_error_record || l_message);
               fnd_file.put_line (fnd_file.LOG, l_error_msg);

               --submit notification to distribution list

               p_retcode := 2;
         END;
      END LOOP;

      fnd_file.put_line (
         fnd_file.LOG,
            'Gen_gl_account End at :'
         || TO_CHAR (SYSDATE, 'DD-MON-YYYY HH:MI:SS'));
   EXCEPTION
      WHEN OTHERS
      THEN
         fnd_file.put_line (fnd_file.LOG,
                            'Error : ' || l_error_record || l_message);
         fnd_file.put_line (fnd_file.LOG, SQLERRM);

         p_retcode := 2;
         p_error_msg := SUBSTR ( (SQLERRM || '-' || l_error_msg), 4000);
         --submit notification to distribution list
         xxcus_error_pkg.xxcus_error_main_api (
            p_called_from         => 'XXWC_AR_INV_PREPROCESS_PKG.Gen_gl_account',
            p_calling             => l_sec,
            p_request_id          => fnd_global.conc_request_id,
            p_ora_error_msg       => SUBSTR (
                                          l_error_msg
                                       || ' '
                                       || DBMS_UTILITY.format_error_stack ()
                                       || DBMS_UTILITY.format_error_backtrace (),
                                       1,
                                       2000),
            p_error_desc          => SUBSTR (l_error_record || l_message, 1, 240),
            p_distribution_list   => g_distro_list,
            p_module              => 'AR');
   END gen_gl_account;

   PROCEDURE main (errbuf        OUT VARCHAR2,
                   retcode       OUT VARCHAR2,
                   p_region   IN     VARCHAR2)
   IS
      /* **************************************************************************
         *   Procedure Name: main
         *
         *   PURPOSE:   This procedure called from the conc prog to Preprocess the invoice transactions
         *
         *   REVISIONS:
         *   Ver        Date          Author                     Description
         *   ---------  ----------    ---------------            -------------------------
         *   1.0        09-Sep-2015   Manjula Chellappan         Initial Version
         *                                                        TMS# 20150527-00318 Forego Two AR Triggers
         * ***************************************************************************/
      l_retcode     NUMBER;
      l_error_msg   VARCHAR2 (4000);
      l_sec         VARCHAR2 (100);
   BEGIN
      l_sec := 'Start gen_trx_number';

      gen_trx_number (l_retcode, l_error_msg, p_region);

      IF NVL (l_retcode, 0) = 2
      THEN
         retcode := 2;
         errbuf := l_error_msg;

         xxcus_error_pkg.xxcus_error_main_api (
            p_called_from         => 'XXWC_AR_INV_PREPROCESS_PKG.main',
            p_calling             => l_sec,
            p_request_id          => fnd_global.conc_request_id,
            p_ora_error_msg       => SUBSTR (
                                          l_error_msg
                                       || ' '
                                       || DBMS_UTILITY.format_error_stack ()
                                       || DBMS_UTILITY.format_error_backtrace (),
                                       1,
                                       2000),
            p_error_desc          => 'Error Raised from laststep',
            p_distribution_list   => g_distro_list,
            p_module              => 'AR');
      END IF;

      l_sec := 'Start gen_gl_account';

      gen_gl_account (l_retcode, l_error_msg, p_region);

      IF NVL (l_retcode, 0) = 2
      THEN
         retcode := 2;
         errbuf := l_error_msg;

         xxcus_error_pkg.xxcus_error_main_api (
            p_called_from         => 'XXWC_AR_INV_PREPROCESS_PKG.main',
            p_calling             => l_sec,
            p_request_id          => fnd_global.conc_request_id,
            p_ora_error_msg       => SUBSTR (
                                          l_error_msg
                                       || ' '
                                       || DBMS_UTILITY.format_error_stack ()
                                       || DBMS_UTILITY.format_error_backtrace (),
                                       1,
                                       2000),
            p_error_desc          => 'Error Raised from laststep',
            p_distribution_list   => g_distro_list,
            p_module              => 'AR');
      END IF;
   EXCEPTION
      WHEN OTHERS
      THEN
         retcode := 2;
         errbuf := l_error_msg;

         xxcus_error_pkg.xxcus_error_main_api (
            p_called_from         => 'XXWC_AR_INV_PREPROCESS_PKG.main',
            p_calling             => l_sec,
            p_request_id          => fnd_global.conc_request_id,
            p_ora_error_msg       => SUBSTR (
                                          l_error_msg
                                       || ' '
                                       || DBMS_UTILITY.format_error_stack ()
                                       || DBMS_UTILITY.format_error_backtrace (),
                                       1,
                                       2000),
            p_error_desc          => 'Error Raised from Final Exception',
            p_distribution_list   => g_distro_list,
            p_module              => 'AR');
         COMMIT;
   END main;
END XXWC_AR_INV_PREPROCESS_PKG;
/