/******************************************************************************
  $Header TMS_20170317-00192_REQ_23791423_DATAFIX.sql $
  Module Name: TMS_20170317-00192_REQ_23791423_DATAFIX

  PURPOSE: Data fix script for 20170317-00192

  REVISIONS:
  Ver        Date         Author                Description
  ---------  -----------  ------------------    -------------------------------
  1.0        24-MAR-2017  Pattabhi Avula        TMS#20170317-00192

*******************************************************************************/
SET SERVEROUTPUT ON SIZE 1000000


BEGIN
   DBMS_OUTPUT.put_line ('Before update');

   UPDATE rcv_transactions_interface
      SET request_id = NULL,
          processing_request_id = NULL,
          processing_status_code = 'PENDING',
          transaction_status_code = 'PENDING',
          processing_mode_code = 'BATCH',
          last_update_date = SYSDATE,
          Last_updated_by = 16991
    WHERE     requisition_line_id IN (39934343,
                                      39934383,
                                      39934376,
                                      39934371)
          AND PROCESSING_MODE_CODE = 'ONLINE';


   DBMS_OUTPUT.put_line ('Records update-' || SQL%ROWCOUNT);
   COMMIT;
EXCEPTION
   WHEN OTHERS
   THEN
      DBMS_OUTPUT.put_line ('Unable to update ' || SQLERRM);
END;
/