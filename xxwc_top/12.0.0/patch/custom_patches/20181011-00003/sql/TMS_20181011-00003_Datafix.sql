/***********************************************************************************************************************************************
   NAME:       TMS_20181011-00003_Datafix.sql
   PURPOSE:  Data fix

   REVISIONS:
   Ver        Date        Author           Description
   ---------  ----------  ---------------  ----------------------------------------------------------------------------------------------------
   1.0        10/11/2018  James Hanon      TMS#20181011-00003-Print Prices On Order DFF - Show Prices for Cash Customers- Data fix script
************************************************************************************************************************************************/
SET SERVEROUTPUT ON SIZE 1000000

DECLARE
  l_beginning_count                     BINARY_INTEGER := 0;
  l_ending_count                        BINARY_INTEGER := 0;
  l_backupsuffix                        VARCHAR2(10)   := TO_CHAR(SYSDATE,'ddhh24miss');
BEGIN

  dbms_output.put_line( 'Begin Fix TMS_20181011-00003' );
  
  dbms_output.put( 'Begin Backup ... ' );
  EXECUTE IMMEDIATE 'CREATE TABLE XXWC.HZ_CUST_ACCT_SITES_ALL' ||l_backupsuffix || ' AS SELECT * FROM AR.HZ_CUST_ACCT_SITES_ALL WHERE cust_acct_site_id IN (SELECT cust_acct_site_id FROM XXWC.XXWC_CUST_PRNPRC_UPD_EXT_TBL)';
  EXECUTE IMMEDIATE 'GRANT ALL ON XXWC.HZ_CUST_ACCT_SITES_ALL' ||l_backupsuffix || ' TO PUBLIC';
  dbms_output.put_line('Backup Account Site Table - Done, XXWC.HZ_CUST_ACCT_SITES_ALL'||l_backupsuffix);
  
  SELECT  COUNT(DISTINCT hcasa.cust_acct_site_id)
  INTO    l_ending_count
  FROM    apps.hz_cust_acct_sites_all   hcasa
  WHERE   1 = 1
  AND     hcasa.cust_acct_site_id       IN ( SELECT cust_acct_site_id FROM XXWC.XXWC_CUST_PRNPRC_UPD_EXT_TBL )
  AND     NVL(hcasa.attribute1,'-1')    != '1'
  ;
  dbms_output.put_line( 'Beginning Count:   '||TO_CHAR(l_beginning_count, '00009') );
  
  dbms_output.put_line('Before Update');
  UPDATE  apps.hz_cust_acct_sites_all   hcasa
  SET     hcasa.attribute1              =      '1'
  WHERE   hcasa.cust_acct_site_id       IN     ( SELECT cust_acct_site_id FROM XXWC.XXWC_CUST_PRNPRC_UPD_EXT_TBL )
  AND     NVL(hcasa.attribute1,'-1')    != '1'
  ;
  dbms_output.put_line('Records Updated:    ' || SQL%ROWCOUNT);
  
  SELECT  COUNT(DISTINCT hcasa.cust_acct_site_id)
  INTO    l_ending_count
  FROM    apps.hz_cust_acct_sites_all   hcasa
  WHERE   1 = 1
  AND     hcasa.cust_acct_site_id       IN ( SELECT cust_acct_site_id FROM XXWC.XXWC_CUST_PRNPRC_UPD_EXT_TBL )
  AND     NVL(hcasa.attribute1,'-1')    != '1'
  ;
  dbms_output.put_line( 'Ending Count:      '||TO_CHAR(l_ending_count, '00009') );

  dbms_output.put_line('Records Updated:   ' || TO_CHAR(l_beginning_count - l_ending_count, '00009'));

  COMMIT;
  -- ROLLBACK;

  dbms_output.put_line( 'End Fix TMS_20181011-00003' );
  
EXCEPTION
  WHEN OTHERS THEN
    dbms_output.put_line('Unable to update records ' || SQLCODE || ': ' ||SQLERRM);
    ROLLBACK;
END;
/
