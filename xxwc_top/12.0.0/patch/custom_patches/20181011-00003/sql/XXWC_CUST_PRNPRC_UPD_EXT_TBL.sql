/*******************************************************************************************************
  -- Table Name XXWC_CUST_PRNPRC_UPD_EXT_TBL.sql  
  -- ***************************************************************************************************
  --
  -- PURPOSE: External table used to store Freight Terms details
  -- HISTORY
  -- ===================================================================================================
  -- ===================================================================================================
  -- VERSION DATE          AUTHOR(S)       DESCRIPTION
  -- ------- -----------   --------------- -------------------------------------------------------------
  -- 1.0     11-Oct-2018   James W. Hanon  TMS# 20181011-00003 - Print Prices On Order DFF
********************************************************************************************************/
CREATE TABLE XXWC.XXWC_CUST_PRNPRC_UPD_EXT_TBL
(
  ORACLE_ACCOUNT_NAME                  VARCHAR2(500 BYTE),
  ORACLE_ACCOUNT_NUMBER                VARCHAR2(500 BYTE),
  ORACLE_SITE_NUMBER                   VARCHAR2(500 BYTE),  
  CUST_ACCT_SITE_ID                    NUMBER
)
ORGANIZATION EXTERNAL
  (  TYPE ORACLE_LOADER
     DEFAULT DIRECTORY XXWC_AHH_USER_CONV_DIR
     ACCESS PARAMETERS 
       ( RECORDS DELIMITED BY NEWLINE CHARACTERSET WE8MSWIN1252
      SKIP 1
    BADFILE 'XXWC_CUST_PRNPRC_UPD_EXT_TBL.bad'
    DISCARDFILE 'XXWC_CUST_PRNPRC_UPD_EXT_TBL.dsc'
    FIELDS TERMINATED BY '|'
    OPTIONALLY ENCLOSED BY '"' AND '"'
    REJECT ROWS WITH ALL NULL FIELDS
                     )
     LOCATION (XXWC_AHH_USER_CONV_DIR:'XXWC_CUST_PRNPRC_UPD_EXT_TBL.csv')
  )
REJECT LIMIT UNLIMITED;

GRANT READ, WRITE ON DIRECTORY XXWC_AHH_USER_CONV_DIR TO PUBLIC;
