-- Report created by Mahender Reddy for 20150714-00118 on 09/18/2015
--Report Name            : PO Cost Change Analysis Report - WC
--Import Instructions    : Run this file in XXEIS user from SQL*Plus or any tool like Toad, SQL Navigator.
prompt Creating View Data for PO Cost Change Analysis Report - WC
set scan off define off
DECLARE
BEGIN 
--Inserting View EIS_XXWC_PO_COST_CHNG_RPT_V
xxeis.eis_rs_ins.v( 'EIS_XXWC_PO_COST_CHNG_RPT_V',201,'','','','','MR020532','XXEIS','Eis Xxwc Po Cost Chng Rpt V','EXPCCRV','','');
--Delete View Columns for EIS_XXWC_PO_COST_CHNG_RPT_V
xxeis.eis_rs_utility.delete_view_rows('EIS_XXWC_PO_COST_CHNG_RPT_V',201,FALSE);
--Inserting View Columns for EIS_XXWC_PO_COST_CHNG_RPT_V
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_COST_CHNG_RPT_V','CATCLASS_DESC',201,'Catclass Desc','CATCLASS_DESC','','','','MR020532','VARCHAR2','','','Catclass Desc','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_COST_CHNG_RPT_V','CATEGORY_DESC',201,'Category Desc','CATEGORY_DESC','','','','MR020532','VARCHAR2','','','Category Desc','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_COST_CHNG_RPT_V','DISTRICT',201,'District','DISTRICT','','','','MR020532','VARCHAR2','','','District','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_COST_CHNG_RPT_V','PO_QTY',201,'Po Qty','PO_QTY','','','','MR020532','NUMBER','','','Po Qty','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_COST_CHNG_RPT_V','REGION',201,'Region','REGION','','','','MR020532','VARCHAR2','','','Region','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_COST_CHNG_RPT_V','SUB_CATEGORY',201,'Sub Category','SUB_CATEGORY','','','','MR020532','VARCHAR2','','','Sub Category','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_COST_CHNG_RPT_V','SUB_CAT_DESC',201,'Sub Cat Desc','SUB_CAT_DESC','','','','MR020532','VARCHAR2','','','Sub Cat Desc','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_COST_CHNG_RPT_V','PO_VENDOR_NAME',201,'Po Vendor Name','PO_VENDOR_NAME','','','','MR020532','VARCHAR2','','','Po Vendor Name','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_COST_CHNG_RPT_V','PO_VENDOR_NUMBER',201,'Po Vendor Number','PO_VENDOR_NUMBER','','','','MR020532','VARCHAR2','','','Po Vendor Number','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_COST_CHNG_RPT_V','TRANSMISSION_METHOD',201,'Transmission Method','TRANSMISSION_METHOD','','','','MR020532','VARCHAR2','','','Transmission Method','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_COST_CHNG_RPT_V','PO_SOURCE_SYSTEM',201,'Po Source System','PO_SOURCE_SYSTEM','','','','MR020532','VARCHAR2','','','Po Source System','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_COST_CHNG_RPT_V','INVOICE_COST',201,'Invoice Cost','INVOICE_COST','','','','MR020532','NUMBER','','','Invoice Cost','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_COST_CHNG_RPT_V','FINAL_COST',201,'Final Cost','FINAL_COST','','','','MR020532','NUMBER','','','Final Cost','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_COST_CHNG_RPT_V','BPA_NUMBER',201,'Bpa Number','BPA_NUMBER','','','','MR020532','VARCHAR2','','','Bpa Number','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_COST_CHNG_RPT_V','PO_COST',201,'Po Cost','PO_COST','','','','MR020532','NUMBER','','','Po Cost','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_COST_CHNG_RPT_V','PO_LINE_ITEM_NUMBER',201,'Po Line Item Number','PO_LINE_ITEM_NUMBER','','','','MR020532','NUMBER','','','Po Line Item Number','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_COST_CHNG_RPT_V','CAT_CLASS',201,'Cat Class','CAT_CLASS','','','','MR020532','VARCHAR2','','','Cat Class','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_COST_CHNG_RPT_V','CATEGORYNAME',201,'Categoryname','CATEGORYNAME','','','','MR020532','VARCHAR2','','','Categoryname','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_COST_CHNG_RPT_V','CATEGORY1D',201,'Category1d','CATEGORY1D','','','','MR020532','NUMBER','','','Category1d','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_COST_CHNG_RPT_V','WC_ITEM_COST',201,'Wc Item Cost','WC_ITEM_COST','','','','MR020532','NUMBER','','','Wc Item Cost','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_COST_CHNG_RPT_V','WC_PART_DESC',201,'Wc Part Desc','WC_PART_DESC','','','','MR020532','VARCHAR2','','','Wc Part Desc','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_COST_CHNG_RPT_V','WC_PART_#',201,'Wc Part #','WC_PART_#','','','','MR020532','VARCHAR2','','','Wc Part #','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_COST_CHNG_RPT_V','FISCAL_YEAR',201,'Fiscal Year','FISCAL_YEAR','','','','MR020532','VARCHAR2','','','Fiscal Year','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_COST_CHNG_RPT_V','FISCAL_MONTH',201,'Fiscal Month','FISCAL_MONTH','','','','MR020532','VARCHAR2','','','Fiscal Month','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_COST_CHNG_RPT_V','BUYER_NAME',201,'Buyer Name','BUYER_NAME','','','','MR020532','VARCHAR2','','','Buyer Name','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_COST_CHNG_RPT_V','SHIP_TO_DESTINATION',201,'Ship To Destination','SHIP_TO_DESTINATION','','','','MR020532','VARCHAR2','','','Ship To Destination','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_COST_CHNG_RPT_V','PO_STATUS',201,'Po Status','PO_STATUS','','','','MR020532','VARCHAR2','','','Po Status','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_COST_CHNG_RPT_V','PO_NUMBER',201,'Po Number','PO_NUMBER','','','','MR020532','VARCHAR2','','','Po Number','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_COST_CHNG_RPT_V','HEADER_ID',201,'Header Id','HEADER_ID','','','','MR020532','NUMBER','','','Header Id','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_COST_CHNG_RPT_V','BPA_PRICE',201,'Bpa Price','BPA_PRICE','','','','MR020532','NUMBER','','','Bpa Price','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_COST_CHNG_RPT_V','DELIVERED_COST_TYPE',201,'Delivered Cost Type','DELIVERED_COST_TYPE','','','','MR020532','VARCHAR2','','','Delivered Cost Type','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_COST_CHNG_RPT_V','LAST_TRANSMISSION_DATE',201,'Last Transmission Date','LAST_TRANSMISSION_DATE','','','','MR020532','VARCHAR2','','','Last Transmission Date','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_COST_CHNG_RPT_V','ORG_LIST_COST',201,'Org List Cost','ORG_LIST_COST','','','','MR020532','NUMBER','','','Org List Cost','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_COST_CHNG_RPT_V','PO_CREATED_DATE',201,'Po Created Date','PO_CREATED_DATE','','','','MR020532','DATE','','','Po Created Date','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_COST_CHNG_RPT_V','IMPACT_OF_COST',201,'Impact Of Cost','IMPACT_OF_COST','','','','MR020532','NUMBER','','','Impact Of Cost','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_COST_CHNG_RPT_V','PO_APPROVED_DATE',201,'Po Approved Date','PO_APPROVED_DATE','','','','MR020532','DATE','','','Po Approved Date','','','');
--Inserting View Components for EIS_XXWC_PO_COST_CHNG_RPT_V
--Inserting View Component Joins for EIS_XXWC_PO_COST_CHNG_RPT_V
END;
/
set scan on define on
prompt Creating Report Data for PO Cost Change Analysis Report - WC
set scan off define off
DECLARE
BEGIN 
--Deleting Report data - PO Cost Change Analysis Report - WC
xxeis.eis_rs_utility.delete_report_rows( 'PO Cost Change Analysis Report - WC' );
--Inserting Report - PO Cost Change Analysis Report - WC
xxeis.eis_rs_ins.r( 201,'PO Cost Change Analysis Report - WC','','It will show when the BPA cost or list price are manually overridden on purchase orders, and help us to understand when and why the changes are made.','','','','MR020532','EIS_XXWC_PO_COST_CHNG_RPT_V','Y','','','MR020532','','Y','White Cap Reports','PDF,','CSV,Pivot Excel,EXCEL,','N');
--Inserting Report Columns - PO Cost Change Analysis Report - WC
xxeis.eis_rs_ins.rc( 'PO Cost Change Analysis Report - WC',201,'CATCLASS_DESC','Cat Class Description','Catclass Desc','','','default','','20','N','','','','','','','','MR020532','N','N','','EIS_XXWC_PO_COST_CHNG_RPT_V','','');
xxeis.eis_rs_ins.rc( 'PO Cost Change Analysis Report - WC',201,'CATEGORY_DESC','Category Description','Category Desc','','','default','','16','N','','','','','','','','MR020532','N','N','','EIS_XXWC_PO_COST_CHNG_RPT_V','','');
xxeis.eis_rs_ins.rc( 'PO Cost Change Analysis Report - WC',201,'DISTRICT','District','District','','','default','','4','N','','','','','','','','MR020532','N','N','','EIS_XXWC_PO_COST_CHNG_RPT_V','','');
xxeis.eis_rs_ins.rc( 'PO Cost Change Analysis Report - WC',201,'PO_QTY','Quantity','Po Qty','','~~~','default','','22','N','','','','','','','','MR020532','N','N','','EIS_XXWC_PO_COST_CHNG_RPT_V','','');
xxeis.eis_rs_ins.rc( 'PO Cost Change Analysis Report - WC',201,'REGION','Region','Region','','','default','','3','N','','','','','','','','MR020532','N','N','','EIS_XXWC_PO_COST_CHNG_RPT_V','','');
xxeis.eis_rs_ins.rc( 'PO Cost Change Analysis Report - WC',201,'SUB_CATEGORY','Sub Category','Sub Category','','','default','','17','N','','','','','','','','MR020532','N','N','','EIS_XXWC_PO_COST_CHNG_RPT_V','','');
xxeis.eis_rs_ins.rc( 'PO Cost Change Analysis Report - WC',201,'SUB_CAT_DESC','Sub Category Description','Sub Cat Desc','','','default','','18','N','','','','','','','','MR020532','N','N','','EIS_XXWC_PO_COST_CHNG_RPT_V','','');
xxeis.eis_rs_ins.rc( 'PO Cost Change Analysis Report - WC',201,'BPA_NUMBER','Bpa Number','Bpa Number','','','default','','23','N','','','','','','','','MR020532','N','N','','EIS_XXWC_PO_COST_CHNG_RPT_V','','');
xxeis.eis_rs_ins.rc( 'PO Cost Change Analysis Report - WC',201,'BUYER_NAME','Buyer Name','Buyer Name','','','default','','9','N','','','','','','','','MR020532','N','N','','EIS_XXWC_PO_COST_CHNG_RPT_V','','');
xxeis.eis_rs_ins.rc( 'PO Cost Change Analysis Report - WC',201,'CATEGORYNAME','Category','Categoryname','','','default','','15','N','','','','','','','','MR020532','N','N','','EIS_XXWC_PO_COST_CHNG_RPT_V','','');
xxeis.eis_rs_ins.rc( 'PO Cost Change Analysis Report - WC',201,'CAT_CLASS','Cat Class','Cat Class','','','default','','19','N','','','','','','','','MR020532','N','N','','EIS_XXWC_PO_COST_CHNG_RPT_V','','');
xxeis.eis_rs_ins.rc( 'PO Cost Change Analysis Report - WC',201,'FISCAL_MONTH','Fiscal Month','Fiscal Month','','','default','','10','N','','','','','','','','MR020532','N','N','','EIS_XXWC_PO_COST_CHNG_RPT_V','','');
xxeis.eis_rs_ins.rc( 'PO Cost Change Analysis Report - WC',201,'FISCAL_YEAR','Fiscal Year','Fiscal Year','','','default','','11','N','','','','','','','','MR020532','N','N','','EIS_XXWC_PO_COST_CHNG_RPT_V','','');
xxeis.eis_rs_ins.rc( 'PO Cost Change Analysis Report - WC',201,'INVOICE_COST','Invoice Cost','Invoice Cost','','$~,~.~2','default','','27','N','','','','','','','','MR020532','N','N','','EIS_XXWC_PO_COST_CHNG_RPT_V','','');
xxeis.eis_rs_ins.rc( 'PO Cost Change Analysis Report - WC',201,'PO_COST','Submitted Po Cost','Po Cost','','$~,~.~2','default','','26','N','','','','','','','','MR020532','N','N','','EIS_XXWC_PO_COST_CHNG_RPT_V','','');
xxeis.eis_rs_ins.rc( 'PO Cost Change Analysis Report - WC',201,'PO_LINE_ITEM_NUMBER','Po Line Item Number','Po Line Item Number','','~~~','default','','21','N','','','','','','','','MR020532','N','N','','EIS_XXWC_PO_COST_CHNG_RPT_V','','');
xxeis.eis_rs_ins.rc( 'PO Cost Change Analysis Report - WC',201,'PO_NUMBER','Po Number','Po Number','','','default','','1','N','','','','','','','','MR020532','N','N','','EIS_XXWC_PO_COST_CHNG_RPT_V','','');
xxeis.eis_rs_ins.rc( 'PO Cost Change Analysis Report - WC',201,'PO_SOURCE_SYSTEM','Po Source System','Po Source System','','','default','','30','N','','','','','','','','MR020532','N','N','','EIS_XXWC_PO_COST_CHNG_RPT_V','','');
xxeis.eis_rs_ins.rc( 'PO Cost Change Analysis Report - WC',201,'PO_STATUS','Po Status','Po Status','','','default','','2','N','','','','','','','','MR020532','N','N','','EIS_XXWC_PO_COST_CHNG_RPT_V','','');
xxeis.eis_rs_ins.rc( 'PO Cost Change Analysis Report - WC',201,'PO_VENDOR_NAME','Po Vendor Name','Po Vendor Name','','','default','','32','N','','','','','','','','MR020532','N','N','','EIS_XXWC_PO_COST_CHNG_RPT_V','','');
xxeis.eis_rs_ins.rc( 'PO Cost Change Analysis Report - WC',201,'PO_VENDOR_NUMBER','Po Vendor Number','Po Vendor Number','','','default','','31','N','','','','','','','','MR020532','N','N','','EIS_XXWC_PO_COST_CHNG_RPT_V','','');
xxeis.eis_rs_ins.rc( 'PO Cost Change Analysis Report - WC',201,'SHIP_TO_DESTINATION','Ship To Destination','Ship To Destination','','','default','','5','N','','','','','','','','MR020532','N','N','','EIS_XXWC_PO_COST_CHNG_RPT_V','','');
xxeis.eis_rs_ins.rc( 'PO Cost Change Analysis Report - WC',201,'TRANSMISSION_METHOD','Transmission Method','Transmission Method','','','default','','8','N','','','','','','','','MR020532','N','N','','EIS_XXWC_PO_COST_CHNG_RPT_V','','');
xxeis.eis_rs_ins.rc( 'PO Cost Change Analysis Report - WC',201,'WC_ITEM_COST','Wc Item Cost','Wc Item Cost','','$~,~.~2','default','','14','N','','','','','','','','MR020532','N','N','','EIS_XXWC_PO_COST_CHNG_RPT_V','','');
xxeis.eis_rs_ins.rc( 'PO Cost Change Analysis Report - WC',201,'WC_PART_#','Wc Part #','Wc Part #','','','default','','12','N','','','','','','','','MR020532','N','N','','EIS_XXWC_PO_COST_CHNG_RPT_V','','');
xxeis.eis_rs_ins.rc( 'PO Cost Change Analysis Report - WC',201,'WC_PART_DESC','Wc Part Desc','Wc Part Desc','','','default','','13','N','','','','','','','','MR020532','N','N','','EIS_XXWC_PO_COST_CHNG_RPT_V','','');
xxeis.eis_rs_ins.rc( 'PO Cost Change Analysis Report - WC',201,'BPA_PRICE','Bpa Price','Bpa Price','','~~~','default','','24','N','','','','','','','','MR020532','N','N','','EIS_XXWC_PO_COST_CHNG_RPT_V','','');
xxeis.eis_rs_ins.rc( 'PO Cost Change Analysis Report - WC',201,'DELIVERED_COST_TYPE','Delivered Cost Type','Delivered Cost Type','','','default','','29','N','','','','','','','','MR020532','N','N','','EIS_XXWC_PO_COST_CHNG_RPT_V','','');
xxeis.eis_rs_ins.rc( 'PO Cost Change Analysis Report - WC',201,'LAST_TRANSMISSION_DATE','Last Transmission Date','Last Transmission Date','','','default','','7','N','','','','','','','','MR020532','N','N','','EIS_XXWC_PO_COST_CHNG_RPT_V','','');
xxeis.eis_rs_ins.rc( 'PO Cost Change Analysis Report - WC',201,'ORG_LIST_COST','Org List Cost','Org List Cost','','~~~','default','','25','N','','','','','','','','MR020532','N','N','','EIS_XXWC_PO_COST_CHNG_RPT_V','','');
xxeis.eis_rs_ins.rc( 'PO Cost Change Analysis Report - WC',201,'PO_CREATED_DATE','Po Created Date','Po Created Date','','','default','','6','N','','','','','','','','MR020532','N','N','','EIS_XXWC_PO_COST_CHNG_RPT_V','','');
xxeis.eis_rs_ins.rc( 'PO Cost Change Analysis Report - WC',201,'IMPACT_OF_COST','Impact Of Cost','Impact Of Cost','','','','','28','N','','','','','','','','MR020532','N','N','','EIS_XXWC_PO_COST_CHNG_RPT_V','','');
--Inserting Report Parameters - PO Cost Change Analysis Report - WC
xxeis.eis_rs_ins.rp( 'PO Cost Change Analysis Report - WC',201,'PO Creation Date From','PO Creation Date From','PO_CREATED_DATE','>=','','','DATE','Y','Y','1','','Y','CONSTANT','MR020532','Y','N','','','');
xxeis.eis_rs_ins.rp( 'PO Cost Change Analysis Report - WC',201,'PO Creation Date TO','PO Creation Date To','PO_CREATED_DATE','<=','','','DATE','Y','Y','2','','Y','CONSTANT','MR020532','Y','N','','','');
--Inserting Report Conditions - PO Cost Change Analysis Report - WC
xxeis.eis_rs_ins.rcn( 'PO Cost Change Analysis Report - WC',201,'PO_CREATED_DATE','>=',':PO Creation Date From','','','Y','1','Y','MR020532');
xxeis.eis_rs_ins.rcn( 'PO Cost Change Analysis Report - WC',201,'PO_CREATED_DATE','<=',':PO Creation Date TO','','','Y','2','Y','MR020532');
--Inserting Report Sorts - PO Cost Change Analysis Report - WC
--Inserting Report Triggers - PO Cost Change Analysis Report - WC
--Inserting Report Templates - PO Cost Change Analysis Report - WC
--Inserting Report Portals - PO Cost Change Analysis Report - WC
--Inserting Report Dashboards - PO Cost Change Analysis Report - WC
--Inserting Report Security - PO Cost Change Analysis Report - WC
xxeis.eis_rs_ins.rsec( 'PO Cost Change Analysis Report - WC','20005','','50900',201,'MR020532','','');
xxeis.eis_rs_ins.rsec( 'PO Cost Change Analysis Report - WC','201','','50983',201,'MR020532','','');
--Inserting Report Pivots - PO Cost Change Analysis Report - WC
END;
/
set scan on define on
