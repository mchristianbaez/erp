/*************************************************************************
  $Header TMS_20181026-00001_UPD_CUST_IFACE_TBLS.sql $
  Module Name: TMS20181026-00001 - Duplicate Sites created from APEX Customer Site Interface (AHH)

  PURPOSE: Duplicate Sites created from APEX Customer Site Interface (AHH)

  REVISIONS:
  Ver        Date         Author                Description
  ---------  -----------  ------------------    --------------------------
  1.0        26-OCT-2018  Pattabhi Avula         TMS#20181026-00001

**************************************************************************/ 
CREATE TABLE XXWC.XXWC_AR_CUSTOMER_IFACE_STG_BKP AS
SELECT * FROM XXWC.XXWC_AR_CUSTOMER_IFACE_STG
/
CREATE TABLE XXWC.XXWC_AR_CUST_SITE_IFACE_STGBKP AS
SELECT * FROM XXWC.XXWC_AR_CUST_SITE_IFACE_STG
/
SET SERVEROUTPUT ON SIZE 1000000;
DECLARE
BEGIN

   DBMS_OUTPUT.put_line ('TMS: 20181026-00001   , Before Update');
   
   UPDATE XXWC.XXWC_AR_CUSTOMER_IFACE_STG
      SET PROCESS_STATUS = 'P'
	      ,PROCESS_ERROR = NULL
	WHERE PROCESS_STATUS IN ('R','V');
   DBMS_OUTPUT.put_line (
         'TMS: 20181026-00001 XXWC_AR_CUSTOMER_IFACE_STG updated : '
      || SQL%ROWCOUNT);
	  
	  
	 UPDATE XXWC.XXWC_AR_CUSTOMER_IFACE_STG
        SET PROCESS_STATUS = 'P'
		   ,PROCESS_ERROR = NULL
	  WHERE PROCESS_STATUS IN ('R','V');
   DBMS_OUTPUT.put_line (
         'TMS: 20181026-00001 XXWC_AR_CUST_SITE_IFACE_STG updated : '
      || SQL%ROWCOUNT);

   COMMIT;

   DBMS_OUTPUT.put_line ('TMS: 20181026-00001   , End Update');
EXCEPTION
   WHEN OTHERS
   THEN
      ROLLBACK;
      DBMS_OUTPUT.put_line ('TMS: 20181026-00001, Errors : ' || SQLERRM);
END;
/