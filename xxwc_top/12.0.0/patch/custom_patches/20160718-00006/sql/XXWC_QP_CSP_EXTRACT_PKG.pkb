CREATE OR REPLACE PACKAGE BODY APPS.XXWC_QP_CSP_EXTRACT_PKG AS
   /*************************************************************************
   *   $Header xxwc_qp_csp_extract_pkg.PKG $
   *   Module Name: xxwc_qp_csp_extract_pkg.PKG
   *
   *   PURPOSE:   This package is used for B2B Customer SalesOrder Inbound Interface
   *
   *   REVISIONS:
   *   Ver        Date        Author                     Description
   *   ---------  ----------  ---------------         -------------------------
   *   1.0        08/01/2014  Gopi Damuluri             Initial Version   
   *   2.0        06/25/2015  Manjula Chellappan        TMS# 20150624-00229 - CSP Enhancement Bundle - Item #1 - CSP Extract 
   *   3.0        18/07/2016  Niraj K Ranjan            TMS#20160718-00006  XXWC CSP Extract Process failed
   * ***************************************************************************/

   pl_dflt_email              fnd_user.email_address%TYPE := 'HDSOracleDevelopers@hdsupply.com';

/*************************************************************************
Procedure : LOAD_STAGING

PURPOSE:   This procedure is used to load staging tables with CSP Information

REVISIONS:
Ver        Date        Author                     Description
---------  ----------  ---------------         -------------------------
1.0        08/01/2014  Gopi Damuluri             Initial Version 
2.0        06/25/2015  Manjula Chellappan        TMS# 20150624-00229 - CSP Enhancement Bundle - Item #1 - CSP Extract  
3.0        18/07/2016  Niraj K Ranjan            TMS#20160718-00006  XXWC CSP Extract Process failed 
***********************************************************************/
   PROCEDURE LOAD_STAGING (p_errbuf                 OUT  VARCHAR2,
                           p_retcode                OUT  NUMBER,
                           p_directory_path          IN  VARCHAR2,
                           p_file_name               IN  VARCHAR2,
                           p_user_name               IN  VARCHAR2)
               IS
      l_err_msg               VARCHAR2(3000);
      l_exception             EXCEPTION;
      l_sec                   VARCHAR2(150);
   BEGIN

     l_sec := 'Truncate Temporary Tables';
     --------------------------------------------------------------------
     -- Truncate Temporary Tables
     --------------------------------------------------------------------
     EXECUTE IMMEDIATE 'TRUNCATE TABLE xxwc.xxwc_qp_csp_gtt_tbl';
     EXECUTE IMMEDIATE 'TRUNCATE TABLE xxwc.xxwc_qp_qll_gtt_tbl';
     EXECUTE IMMEDIATE 'TRUNCATE TABLE xxwc.xxwc_qp_ool_gtt_tbl';
     EXECUTE IMMEDIATE 'TRUNCATE TABLE xxwc.xxwc_qp_opa_gtt_tbl';
--     EXECUTE IMMEDIATE 'TRUNCATE TABLE xxwc.xxwc_qp_csp_final_gtt_tbl';

      fnd_file.put_line(fnd_file.log,'1.0 Start - '||TO_CHAR(SYSDATE, 'YYYY-MM-DD HH24:MI:SS'));
      l_sec := 'Step# 1.0 Load XXWC_QP_CSP_GTT_TBL';
      ------------------------------------------------------------------
      -- Step# 1.0 Load XXWC_QP_CSP_GTT_TBL
      ------------------------------------------------------------------
      BEGIN
       INSERT INTO xxwc.xxwc_qp_csp_gtt_tbl ( PRICE_TYPE          
                                            , ORDER_GROSS_MARGIN  
                                            , INCOMPATABILITY     
                                            , ORGANIZATION_ID     
                                            , CUSTOMER_ID         
                                            , CUSTOMER_SITE_ID    
                                            , AGREEMENT_ID        
                                            , AGREEMENT_LINE_ID   
                                            , AGREEMENT_TYPE      
                                            , VQ_NUMBER           
                                            , ITEM_ATTRIBUTE      
                                            , ITEM_NUMBER         
                                            , ITEM_DESCRIPTION    
                                            , START_DATE          
                                            , END_DATE            
                                            , LIST_PRICE          
                                            , MODIFIER_TYPE       
                                            , MODIFIER_VALUE      
                                            , SELLING_PRICE       
                                            , SPECIAL_COST        
                                            , VENDOR_NUMBER       
                                            , AVERAGE_COST        
                                            , GROSS_MARGIN        
                                            , LAST_UPDATE_DATE    
                                            , LATEST_REC_FLAG     
                                            , CREATION_DATE       
                                            , REVISION_NUMBER     
                                            , LAST_UPDATED_BY     )
       SELECT cph.price_type ,
              cph.gross_margin order_gross_margin,
              cph.incompatability_group incompatability,
              cph.organization_id,
              cph.customer_id,
              cph.customer_site_id,
              cpl.agreement_id,
              cpl.agreement_line_id,
              DECODE (cpl.agreement_type, 'CSP', 'Contract Pricing', 'VQN', 'Vendor Quote') Agreement_Type,
              cpl.vendor_quote_number vq_number,
              cpl.product_attribute item_attribute,
              cpl.product_value item_number,
              cpl.product_description item_description,
              cpl.start_date start_date,
              cpl.end_date end_date,
              cpl.list_price list_price,
              cpl.modifier_type modifier_type,
              cpl.modifier_value modifier_value,
              cpl.selling_price selling_price,
              cpl.special_cost special_cost,
              (SELECT segment1
                 FROM apps.ap_suppliers
                WHERE vendor_id = cpl.vendor_id)
                 vendor_number,
              cpl.average_cost average_cost,
              cpl.gross_margin gross_margin,
              cpl.last_update_date,
              cpl.latest_rec_flag LATEST_REC_FLAG,
              cpl.creation_date,
              cpl.REVISION_NUMBER,
              cpl.last_updated_by
         FROM xxwc.xxwc_om_contract_pricing_hdr      cph,
              xxwc.xxwc_om_contract_pricing_lines    cpl
        WHERE cph.agreement_id = cpl.agreement_id -- AND cph.agreement_id = 16416
          AND EXISTS (SELECT /*+ XXWC_QP_LIST_HDR_N99 */ '1' FROM apps.qp_list_headers qlh WHERE 1 = 1 AND cph.agreement_id = qlh.attribute14)
        --and cph.price_type = 'MASTER'
          AND cpl.REVISION_NUMBER > 0;
      EXCEPTION
      WHEN OTHERS THEN
         l_err_msg := '1.0 Error loading XXWC_QP_CSP_GTT_TBL - '||SQLERRM;
         RAISE l_exception;
      END;

       COMMIT;
      fnd_file.put_line(fnd_file.log,'1.01 Start - '||TO_CHAR(SYSDATE, 'YYYY-MM-DD HH24:MI:SS'));
      l_sec := 'Step# 1.01 Update XXWC_QP_CSP_GTT_TBL with CSP Modifier details';
      -----------------------------------------------------------------------------
      -- Step# 1.01 Update XXWC_QP_CSP_GTT_TBL with CSP Modifier details
      -----------------------------------------------------------------------------
      BEGIN
         DBMS_STATS.gather_index_stats (ownname => 'QP', indname => 'XXWC_QP_LIST_HDR_N99');
         
         UPDATE XXWC.XXWC_QP_CSP_GTT_TBL stg
         set (modifier_name , list_header_id) = (SELECT /*+ XXWC_QP_LIST_HDR_N99 */  qlh.description, qlh.list_header_id
                                                                       FROM qp_list_headers_all qlh
                                                                      WHERE to_char(stg.agreement_id) = qlh.attribute14
                                                                          AND list_type_code = 'DLT'
                                                                          AND currency_code = 'USD'
                                                                          AND context = 162
                                                                          AND rownum = 1)
         where 1 = 1 
           and stg.modifier_name IS NULL;
      EXCEPTION
      WHEN OTHERS THEN
         l_err_msg := '1.01 Error updating XXWC_QP_CSP_GTT_TBL.MODIFIER_NAME - '||SQLERRM;
         RAISE l_exception;
      END;

       COMMIT;
      fnd_file.put_line(fnd_file.log,'1.02 Start - '||TO_CHAR(SYSDATE, 'YYYY-MM-DD HH24:MI:SS'));
      l_sec := 'Step# 1.02 Update XXWC_QP_CSP_GTT_TBL with Customer details';
      ------------------------------------------------------------------
      -- Step# 1.02 Update XXWC_QP_CSP_GTT_TBL with Customer details
      ------------------------------------------------------------------
      BEGIN
         UPDATE XXWC.XXWC_QP_CSP_GTT_TBL stg
            SET (customer_name, customer_number) = (SELECT hca.account_name, hca.account_number FROM hz_cust_accounts_all hca WHERE hca.cust_account_id = stg.customer_id AND hca.status = 'A' AND ROWNUM = 1)
          WHERE 1 = 1
            AND price_type != 'NATIONAL';
      EXCEPTION
      WHEN OTHERS THEN
         l_err_msg := '1.02 Error updating XXWC_QP_CSP_GTT_TBL.CUSTOMER_NAME - '||SQLERRM;
         RAISE l_exception;
      END;

      COMMIT;
      fnd_file.put_line(fnd_file.log,'1.03 Start - '||TO_CHAR(SYSDATE, 'YYYY-MM-DD HH24:MI:SS'));
      l_sec := 'Step# 1.03 Insert NATIONAL Price List details into XXWC_QP_CSP_GTT_TBL';
      ------------------------------------------------------------------
      -- Step# 1.03 Insert NATIONAL Price List details into XXWC_QP_CSP_GTT_TBL
      ------------------------------------------------------------------
      BEGIN
         INSERT INTO xxwc.xxwc_qp_csp_gtt_tbl ( PRICE_TYPE          
                                              , ORDER_GROSS_MARGIN  
                                              , INCOMPATABILITY     
                                              , ORGANIZATION_ID     
                                              , CUSTOMER_ID         
                                              , CUSTOMER_SITE_ID    
                                              , AGREEMENT_ID        
                                              , AGREEMENT_LINE_ID   
                                              , AGREEMENT_TYPE      
                                              , VQ_NUMBER           
                                              , ITEM_ATTRIBUTE      
                                              , ITEM_NUMBER         
                                              , ITEM_DESCRIPTION    
                                              , START_DATE          
                                              , END_DATE            
                                              , LIST_PRICE          
                                              , MODIFIER_TYPE       
                                              , MODIFIER_VALUE      
                                              , SELLING_PRICE       
                                              , SPECIAL_COST        
                                              , VENDOR_NUMBER       
                                              , AVERAGE_COST        
                                              , GROSS_MARGIN        
                                              , LAST_UPDATE_DATE    
                                              , LATEST_REC_FLAG     
                                              , CREATION_DATE       
                                              , REVISION_NUMBER     
                                              , CUSTOMER_NAME
                                              , CUSTOMER_NUMBER
                                              , MODIFIER_NAME 
                                              , LIST_HEADER_ID
                                              , LAST_UPDATED_BY     )
         SELECT stg.PRICE_TYPE
              , stg.ORDER_GROSS_MARGIN  
              , stg.INCOMPATABILITY     
              , stg.ORGANIZATION_ID     
              , hca.CUST_ACCOUNT_ID
              , stg.CUSTOMER_SITE_ID    
              , stg.AGREEMENT_ID        
              , stg.AGREEMENT_LINE_ID   
              , stg.AGREEMENT_TYPE      
              , stg.VQ_NUMBER           
              , stg.ITEM_ATTRIBUTE      
              , stg.ITEM_NUMBER         
              , stg.ITEM_DESCRIPTION    
              , stg.START_DATE          
              , stg.END_DATE            
              , stg.LIST_PRICE          
              , stg.MODIFIER_TYPE       
              , stg.MODIFIER_VALUE      
              , stg.SELLING_PRICE       
              , stg.SPECIAL_COST        
              , stg.VENDOR_NUMBER       
              , stg.AVERAGE_COST        
              , stg.GROSS_MARGIN        
              , stg.LAST_UPDATE_DATE    
              , stg.LATEST_REC_FLAG     
              , stg.CREATION_DATE       
              , stg.REVISION_NUMBER     
              , hca.ACCOUNT_NAME
              , hca.ACCOUNT_NUMBER
              , stg.MODIFIER_NAME 
              , stg.LIST_HEADER_ID
              , stg.LAST_UPDATED_BY
           FROM xxwc.xxwc_qp_csp_gtt_tbl                    stg
              , qp_qualifiers_v                             qqv
              , hz_cust_accounts_all                        hca
          WHERE stg.price_type                            = 'NATIONAL'
            AND hca.account_number                       IN ('195770','156866','91611000','167676','182265')
            AND hca.status                                = 'A'
            AND stg.list_header_id                        = qqv.list_header_id
            AND qqv.qualifier_attribute                   = 'QUALIFIER_ATTRIBUTE32'
            AND NVL(qqv.end_date_active, TRUNC(SYSDATE)) >= TRUNC(SYSDATE)
            AND qqv.qualifier_attr_value       = to_char(hca.cust_account_id);
      EXCEPTION
      WHEN OTHERS THEN
         l_err_msg := '1.03 Error inserting NATIONAL Price List details into XXWC_QP_CSP_GTT_TBL - '||SQLERRM;
         RAISE l_exception;
      END;

       COMMIT;
      fnd_file.put_line(fnd_file.log,'1.04 Start - '||TO_CHAR(SYSDATE, 'YYYY-MM-DD HH24:MI:SS'));
      l_sec := 'Step# 1.04 Delete from XXWC_QP_CSP_GTT_TBL';
      ------------------------------------------------------------------
      -- Step# 1.04 Delete from XXWC_QP_CSP_GTT_TBL
      ------------------------------------------------------------------
      BEGIN
         DELETE FROM XXWC.XXWC_QP_CSP_GTT_TBL stg
          WHERE customer_name    IS NULL
            AND customer_number  IS NULL
            -- AND price_type        = 'NATIONAL'
            ;
      EXCEPTION
      WHEN OTHERS THEN
         l_err_msg := '1.04 Error deleting from XXWC_QP_CSP_GTT_TBL - '||SQLERRM;
         RAISE l_exception;
      END;

       COMMIT;
      fnd_file.put_line(fnd_file.log,'1.05 Start - '||TO_CHAR(SYSDATE, 'YYYY-MM-DD HH24:MI:SS'));
      l_sec := 'Step# 1.05 Update XXWC_QP_CSP_GTT_TBL with SHIP_TO PartySite details';
      -----------------------------------------------------------------------
      -- Step# 1.05 Update XXWC_QP_CSP_GTT_TBL with SHIP_TO PartySite details
      -----------------------------------------------------------------------
      BEGIN
         UPDATE XXWC.XXWC_QP_CSP_GTT_TBL stg
            SET (party_site_number, location, salesrep_id) = (SELECT hps.party_site_number
                                                      , hcsu.location
                                                      , hcsu.primary_salesrep_id
                                                   FROM hz_party_sites hps
                                                      , hz_cust_acct_sites_all hcas
                                                      , hz_cust_site_uses_all hcsu 
                                                  WHERE hcsu.cust_acct_site_id = hcas.cust_acct_site_id
                                                    AND hps.party_site_id = hcas.party_site_id
                                                    AND stg.customer_site_id = hcsu.site_use_id
                                                    AND hcas.status      = 'A'
                                                    AND hcsu.status      = 'A'
                                                    AND hcas.org_id      = 162
                                                    AND hcsu.org_id      = 162
                                                    AND ROWNUM = 1)
          WHERE 1 = 1
          AND stg.price_type = 'SHIPTO';
      EXCEPTION
      WHEN OTHERS THEN
         l_err_msg := '1.05 Error updating SHIP_TO XXWC_QP_CSP_GTT_TBL.PARTY_SITE_NUMBER - '||SQLERRM;
         RAISE l_exception;
      END;

-- Delete stmt Delete where party_site_number IS NULL and SHIPTO Price Type > Start
       COMMIT;
      fnd_file.put_line(fnd_file.log,'1.04 Start - '||TO_CHAR(SYSDATE, 'YYYY-MM-DD HH24:MI:SS'));
      l_sec := 'Step# 1.051 Delete from XXWC_QP_CSP_GTT_TBL';
      ------------------------------------------------------------------
      -- Step# 1.051 Delete from XXWC_QP_CSP_GTT_TBL
      ------------------------------------------------------------------
      BEGIN
         DELETE FROM XXWC.XXWC_QP_CSP_GTT_TBL stg
          WHERE party_site_number    IS NULL
            AND price_type        = 'SHIPTO'
            ;
      EXCEPTION
      WHEN OTHERS THEN
         l_err_msg := '1.051 Error deleting from XXWC_QP_CSP_GTT_TBL - '||SQLERRM;
         RAISE l_exception;
      END;

-- Delete stmt Delete where party_site_number IS NULL and SHIPTO Price Type
       COMMIT;
      fnd_file.put_line(fnd_file.log,'1.06 Start - '||TO_CHAR(SYSDATE, 'YYYY-MM-DD HH24:MI:SS'));
      l_sec := 'Step# 1.06 Update XXWC_QP_CSP_GTT_TBL with BILL_TO PartySite details';
      -----------------------------------------------------------------------------
      -- Step# 1.06 Update XXWC_QP_CSP_GTT_TBL with BILL_TO PartySite details
      -----------------------------------------------------------------------------
      BEGIN
         UPDATE XXWC.XXWC_QP_CSP_GTT_TBL stg
            SET (salesrep_id) = (SELECT hcsu.primary_salesrep_id
                                   FROM hz_party_sites hps
                                      , hz_cust_acct_sites_all hcas
                                      , hz_cust_site_uses_all hcsu 
                                  WHERE hcsu.cust_acct_site_id = hcas.cust_acct_site_id
                                    AND hps.party_site_id = hcas.party_site_id
                                    AND stg.customer_id = hcas.cust_account_id
                                    AND hcsu.site_use_code = 'BILL_TO'
                                    AND hcsu.primary_flag = 'Y'
                                    AND hcas.status      = 'A'
                                    AND hcsu.status      = 'A'
                                    AND hcas.org_id      = 162
                                    AND hcsu.org_id      = 162
                                    AND ROWNUM = 1)
          WHERE 1 = 1
          AND stg.price_type != 'SHIPTO' ;
      EXCEPTION
      WHEN OTHERS THEN
         l_err_msg := '1.06 Error updating BILL_TO XXWC_QP_CSP_GTT_TBL.PARTY_SITE_NUMBER - '||SQLERRM;
         RAISE l_exception;
      END;

       COMMIT;
      fnd_file.put_line(fnd_file.log,'1.07 Start - '||TO_CHAR(SYSDATE, 'YYYY-MM-DD HH24:MI:SS'));
      l_sec := 'Step# 1.07 Update XXWC_QP_CSP_GTT_TBL with BILL_TO SALESREP_ID details';
      -----------------------------------------------------------------------------
      -- Step# 1.07 Update XXWC_QP_CSP_GTT_TBL with BILL_TO SALESREP_ID details
      -----------------------------------------------------------------------------
      BEGIN
         UPDATE XXWC.XXWC_QP_CSP_GTT_TBL stg
            SET (salesrep_id) = (SELECT hcsu.primary_salesrep_id
                                   FROM hz_party_sites           hps
                                      , hz_cust_acct_sites_all   hcas
                                      , hz_cust_site_uses_all    hcsu 
                                  WHERE hcsu.cust_acct_site_id = hcas.cust_acct_site_id
                                    AND hps.party_site_id      = hcas.party_site_id
                                    AND stg.party_site_number  = hps.party_site_number
                                    AND hcsu.site_use_code     = 'BILL_TO'
                                    AND hcsu.primary_flag      = 'Y'
                                    AND hcas.status      = 'A'
                                    AND hcsu.status      = 'A'
                                    AND hcas.org_id      = 162
                                    AND hcsu.org_id      = 162
                                    AND hcsu.primary_salesrep_id IS NOT NULL
                                    AND ROWNUM                 = 1)
          WHERE 1 = 1
          AND stg.salesrep_id IS NULL
          AND stg.price_type != 'SHIPTO';
      EXCEPTION
      WHEN OTHERS THEN
         l_err_msg := '1.07 Error updating BILL_TO XXWC_QP_CSP_GTT_TBL.SALESREP_ID - '||SQLERRM;
         RAISE l_exception;
      END;

       COMMIT;
      fnd_file.put_line(fnd_file.log,'1.08 Start - '||TO_CHAR(SYSDATE, 'YYYY-MM-DD HH24:MI:SS'));
      l_sec := 'Step# 1.08 Update XXWC_QP_CSP_GTT_TBL with SalesRep details';
      -----------------------------------------------------------------------------
      -- Step# 1.08 Update XXWC_QP_CSP_GTT_TBL with SalesRep details
      -----------------------------------------------------------------------------
      BEGIN
         UPDATE XXWC.XXWC_QP_CSP_GTT_TBL stg
            SET (salesrep_name) = (SELECT jtf.resource_name
                                     FROM jtf.jtf_rs_resource_extns_tl jtf
                                        , apps.ra_salesreps_all        rsa
                                    WHERE 1                          = 1
                                      AND stg.salesrep_id            = rsa.salesrep_id
                                      AND rsa.resource_id            = jtf.resource_id
                                      AND jtf.language               = 'US'
                                      AND NVL (rsa.org_id, 162)      = 162
                                      AND ROWNUM                     = 1)
         where 1 = 1 ;
      EXCEPTION
      WHEN OTHERS THEN
         l_err_msg := '1.08 Error updating XXWC_QP_CSP_GTT_TBL.SALESREP_NAME - '||SQLERRM;
         RAISE l_exception;
      END;

       COMMIT;
      fnd_file.put_line(fnd_file.log,'2.0 Start - '||TO_CHAR(SYSDATE, 'YYYY-MM-DD HH24:MI:SS'));
      l_sec := 'Step# 2.0 Insert into XXWC_QP_QLL_GTT_TBL';
      -----------------------------------------------------------------------------
      -- Step# 2.0 Insert into XXWC_QP_QLL_GTT_TBL
      -----------------------------------------------------------------------------
      DBMS_STATS.gather_index_stats (ownname => 'QP', indname => 'XXWC_QP_LIST_LN_N99');
      
      BEGIN
         INSERT INTO xxwc.xxwc_qp_qll_gtt_tbl(list_line_id
                                            , list_header_id
                                            , agreement_line_id)
         SELECT qll.list_line_id
              , qll.list_header_id
              , stg.agreement_line_id
           FROM xxwc.xxwc_qp_csp_gtt_tbl stg,
                apps.qp_list_Lines qll
          WHERE 1 = 1
            AND qll.list_line_type_code = 'DIS'
            AND qll.modifier_level_code = 'LINE'
            AND qll.list_header_id = stg.list_header_id
            AND qll.attribute2 = stg.agreement_line_id;
      EXCEPTION
      WHEN OTHERS THEN
         l_err_msg := '2.0 Error inserting into XXWC_QP_QLL_GTT_TBL - '||SQLERRM;
         RAISE l_exception;
      END;

       COMMIT;
      fnd_file.put_line(fnd_file.log,'3.0 Start - '||TO_CHAR(SYSDATE, 'YYYY-MM-DD HH24:MI:SS'));
      l_sec := 'Step# 3.0 Insert into XXWC_QP_OPA_GTT_TBL';
      -----------------------------------------------------------------------------
      -- Step# 3.0 Insert into XXWC_QP_OPA_GTT_TBL
      -----------------------------------------------------------------------------
      BEGIN
         INSERT INTO xxwc.xxwc_qp_opa_gtt_tbl(price_adjustment_id
                                            , line_id
                                            , agreement_line_id)
         SELECT opa.price_adjustment_id
              , opa.line_id
              , stg.agreement_line_id
           FROM apps.oe_price_adjustments opa
              , xxwc.xxwc_qp_qll_gtt_tbl stg
          WHERE 1 = 1
            AND stg.list_line_id   = opa.list_line_id
            AND stg.list_header_id = opa.list_header_id;

/*
         INSERT INTO xxwc.xxwc_qp_opa_gtt_tbl(price_adjustment_id
                                            , line_id)
         SELECT opa.price_adjustment_id
              , opa.line_id
           FROM apps.oe_price_adjustments opa
          WHERE 1 = 1
            AND EXISTS (SELECT  '1'
                          FROM xxwc.xxwc_qp_qll_gtt_tbl stg
                         WHERE 1 = 1
                           AND stg.list_line_id = opa.list_line_id
                           AND stg.list_header_id = opa.list_header_id);
*/
      EXCEPTION
      WHEN OTHERS THEN
         l_err_msg := '3.0 Error inserting into XXWC_QP_OPA_GTT_TBL - '||SQLERRM;
         RAISE l_exception;
      END;

       COMMIT;
      fnd_file.put_line(fnd_file.log,'4.0 Start - '||TO_CHAR(SYSDATE, 'YYYY-MM-DD HH24:MI:SS'));
      -----------------------------------------------------------------------------
      -- Step# 4.0 Insert into XXWC_QP_OOL_GTT_TBL
      -----------------------------------------------------------------------------
      l_sec := 'Step# 4.0 Insert into XXWC_QP_OOL_GTT_TBL';
      BEGIN

--       INSERT /*+ append */ INTO xxwc.xxwc_qp_ool_gtt_tbl  (unit_cost,
--              unit_selling_price,
--              shipped_quantity,
--              ordered_quantity,
--              line_type_id,
--              tax_value,
--              actual_shipment_date,
--              line_id,
--              request_date,
--              agreement_line_id)
--       SELECT /*+ index(ool XXWC_OE_ORDER_LN_LUD1, opa XXWC_QP_OPA_GTT_N1) */  ool.unit_cost,
--              ool.unit_selling_price,
--              ool.shipped_quantity,
--              ool.ordered_quantity,
--              ool.line_type_id,
--              ool.tax_value,
--              ool.actual_shipment_date,
--              ool.line_id,
--              ool.request_date,
--              opa.agreement_line_id
--         FROM apps.oe_order_lines_all ool
--            , xxwc.xxwc_qp_opa_gtt_tbl opa
--        WHERE 1 = 1
--          AND NVL (TRUNC(ool.actual_shipment_date), TRUNC(ool.request_date)) >= SYSDATE - 180
--          AND TRUNC (ool.last_update_date) >= TRUNC (SYSDATE) - 180
--          AND opa.line_id = ool.line_id;
       DBMS_STATS.gather_index_stats (ownname => 'ONT', indname => 'XXWC_OE_ORDER_LN_LUD1');

       INSERT /*+ append */ INTO xxwc.xxwc_qp_ool_gtt_tbl  (unit_cost,
              unit_selling_price,
              shipped_quantity,
              ordered_quantity,
              line_type_id,
              tax_value,
              actual_shipment_date,
              line_id,
              request_date)
       SELECT /*+ index(ool XXWC_OE_ORDER_LN_LUD1) */  ool.unit_cost,
              ool.unit_selling_price,
              ool.shipped_quantity,
              ool.ordered_quantity,
              ool.line_type_id,
              ool.tax_value,
              ool.actual_shipment_date,
              ool.line_id,
              ool.fulfillment_date request_date
         FROM apps.oe_order_lines_all ool                
        WHERE 1 = 1
          AND NVL (TRUNC(ool.actual_shipment_date), TRUNC(ool.fulfillment_date)) >= SYSDATE - 180
          AND TRUNC (ool.last_update_date) >= TRUNC (SYSDATE) - 180
          AND EXISTS (SELECT /*+ index(opa XXWC_QP_OPA_GTT_N1) */ '1' 
                        FROM xxwc.xxwc_qp_opa_gtt_tbl opa
                       WHERE 1 = 1
                         AND opa.line_id = ool.line_id);

      EXCEPTION
      WHEN OTHERS THEN
         l_err_msg := '4.0 Error inserting into XXWC_QP_OOL_GTT_TBL - '||SQLERRM;
         RAISE l_exception;
      END;

      COMMIT;
      fnd_file.put_line(fnd_file.log,'4.01 Start - '||TO_CHAR(SYSDATE, 'YYYY-MM-DD HH24:MI:SS'));
      l_sec := 'Step# 4.01 Update XXWC_QP_OOL_GTT_TBL with AGREEMENT_LINE_ID';
      -----------------------------------------------------------------------------
      -- Step# 4.01 Update XXWC_QP_OOL_GTT_TBL with AGREEMENT_LINE_ID
      -----------------------------------------------------------------------------
      BEGIN
        UPDATE xxwc.xxwc_qp_ool_gtt_tbl ool
           SET ool.agreement_line_id = (SELECT /*+ index(opa XXWC_QP_OPA_GTT_N1) */ opa.agreement_line_id
                                          FROM xxwc.xxwc_qp_opa_gtt_tbl opa
                                         WHERE 1 = 1
                                           AND opa.line_id = ool.line_id
                                           AND rownum = 1)
         WHERE 1 = 1
           AND ool.agreement_line_id IS NULL;
      EXCEPTION
      WHEN OTHERS THEN
         l_err_msg := '4.01 Error updating into XXWC_QP_OOL_GTT_TBL - '||SQLERRM;
         RAISE l_exception;
      END;

       COMMIT;
      DBMS_STATS.gather_index_stats (ownname => 'XXWC', indname => 'XXWC_QP_CSP_GTT_N1');
      DBMS_STATS.gather_index_stats (ownname => 'XXWC', indname => 'XXWC_QP_OOL_GTT_N1');
      
      fnd_file.put_line(fnd_file.log,'5.0 Start - '||TO_CHAR(SYSDATE, 'YYYY-MM-DD HH24:MI:SS'));
      l_sec := 'Step# 5.0 Update XXWC_QP_OOL_GTT_TBL';
      -----------------------------------------------------------------------------
      -- Step# 5.0 Update XXWC_QP_OOL_GTT_TBL
      -----------------------------------------------------------------------------
      BEGIN

         UPDATE /*+ index(ool XXWC_QP_OOL_GTT_N1) */ xxwc.xxwc_qp_ool_gtt_tbl ool   --Ver 3.0 replaced opa with ool
            SET ( ool.PRICE_TYPE        
                , ool.ORDER_GROSS_MARGIN
                , ool.INCOMPATABILITY   
                , ool.ORGANIZATION_ID   
                , ool.CUSTOMER_ID       
                , ool.CUSTOMER_SITE_ID  
                , ool.AGREEMENT_ID      
                , ool.AGREEMENT_TYPE    
                , ool.VQ_NUMBER         
                , ool.ITEM_ATTRIBUTE    
                , ool.ITEM_NUMBER       
                , ool.ITEM_DESCRIPTION  
                , ool.START_DATE        
                , ool.END_DATE          
                , ool.LIST_PRICE        
                , ool.MODIFIER_TYPE     
                , ool.MODIFIER_VALUE    
                , ool.SELLING_PRICE     
                , ool.SPECIAL_COST      
                , ool.VENDOR_NUMBER     
                , ool.AVERAGE_COST      
                , ool.GROSS_MARGIN      
                , ool.LAST_UPDATE_DATE  
                , ool.LATEST_REC_FLAG   
                , ool.CREATION_DATE     
                , ool.REVISION_NUMBER   
                , ool.LAST_UPDATED_BY   
                , ool.CUSTOMER_NAME     
                , ool.CUSTOMER_NUMBER   
                , ool.LOCATION          
                , ool.PARTY_SITE_NUMBER 
                , ool.MODIFIER_NAME     
                , ool.SALESREP_ID
                , ool.SALESREP_NAME
                , ool.ORG_ID
                , ool.LIST_HEADER_ID) = (SELECT /*+ index(csp XXWC_QP_CSP_GTT_N1) */ csp.PRICE_TYPE     --Ver 3.0 replaced opa with csp
                                              , csp.ORDER_GROSS_MARGIN
                                              , csp.INCOMPATABILITY   
                                              , csp.ORGANIZATION_ID   
                                              , csp.CUSTOMER_ID       
                                              , csp.CUSTOMER_SITE_ID  
                                              , csp.AGREEMENT_ID      
                                              , csp.AGREEMENT_TYPE    
                                              , csp.VQ_NUMBER         
                                              , csp.ITEM_ATTRIBUTE    
                                              , csp.ITEM_NUMBER       
                                              , csp.ITEM_DESCRIPTION  
                                              , csp.START_DATE        
                                              , csp.END_DATE          
                                              , csp.LIST_PRICE        
                                              , csp.MODIFIER_TYPE     
                                              , csp.MODIFIER_VALUE    
                                              , csp.SELLING_PRICE     
                                              , csp.SPECIAL_COST      
                                              , csp.VENDOR_NUMBER     
                                              , csp.AVERAGE_COST      
                                              , csp.GROSS_MARGIN      
                                              , csp.LAST_UPDATE_DATE  
                                              , csp.LATEST_REC_FLAG   
                                              , csp.CREATION_DATE     
                                              , csp.REVISION_NUMBER   
                                              , TO_CHAR(csp.LAST_UPDATED_BY) LAST_UPDATED_BY
                                              , csp.CUSTOMER_NAME     
                                              , csp.CUSTOMER_NUMBER   
                                              , csp.LOCATION          
                                              , csp.PARTY_SITE_NUMBER 
                                              , csp.MODIFIER_NAME     
                                              , csp.SALESREP_ID
                                              , csp.SALESREP_NAME
                                              , 162
                                              , csp.LIST_HEADER_ID    
                                               FROM xxwc.xxwc_qp_csp_gtt_tbl csp
                                              WHERE csp.agreement_line_id = ool.agreement_line_id
                                                AND rownum = 1)
           WHERE 1 = 1;

--       UPDATE xxwc.xxwc_qp_csp_gtt_tbl csp
--          SET (csp.unit_cost
--             , csp.unit_selling_price
--             , csp.shipped_quantity
--             , csp.ordered_quantity
--             , csp.line_type_id
--             , csp.tax_value
--             , csp.actual_shipment_date
--             , csp.line_id
--             , csp.request_date
--             , csp.price_adjustment_id) = (SELECT /*+ index(opa XXWC_QP_OOL_GTT_N1) */ ool.unit_cost
--                                              , ool.unit_selling_price
--                                              , ool.shipped_quantity
--                                              , ool.ordered_quantity
--                                              , ool.line_type_id
--                                              , ool.tax_value
--                                              , ool.actual_shipment_date
--                                              , ool.line_id
--                                              , ool.request_date
--                                              , ool.price_adjustment_id
--                                            FROM xxwc.xxwc_qp_ool_gtt_tbl ool  
--                                          WHERE ool.agreement_line_id = csp.agreement_line_id
--                                            AND rownum = 1)
--         WHERE 1 = 1;

--       INSERT /*+ append */ INTO  xxwc.XXWC_QP_CSP_FINAL_GTT_TBL
--       SELECT ool.unit_cost
--            , ool.unit_selling_price
--            , ool.shipped_quantity
--            , ool.ordered_quantity
--            , ool.line_type_id
--            , ool.tax_value
--            , ool.actual_shipment_date
--            , ool.line_id
--            , ool.request_date
--            , ool.price_adjustment_id
--            , ool.agreement_line_id
--            , csp.price_type
--            , csp.order_gross_margin
--            , csp.incompatability
--            , csp.organization_id
--            , csp.customer_id
--            , csp.customer_site_id
--            , csp.agreement_id
--            , csp.agreement_type
--            , csp.vq_number
--            , csp.item_attribute
--            , csp.item_number
--            , csp.item_description
--            , csp.start_date
--            , csp.end_date
--            , csp.list_price
--            , csp.modifier_type
--            , csp.modifier_value
--            , csp.selling_price
--            , csp.special_cost
--            , csp.vendor_number
--            , csp.average_cost
--            , csp.gross_margin
--            , csp.last_update_date
--            , csp.latest_rec_flag
--            , csp.creation_date
--            , csp.revision_number
--            , csp.last_updated_by
--            , csp.customer_name
--            , csp.customer_number
--            , csp.location
--            , csp.party_site_number
--            , csp.modifier_name
--            , csp.list_header_id
--         FROM xxwc.xxwc_qp_ool_gtt_tbl ool
--            , xxwc.xxwc_qp_csp_gtt_tbl csp
--        WHERE 1 = 1
--          AND csp.agreement_line_id = ool.agreement_line_id;
      EXCEPTION
      WHEN OTHERS THEN
         l_err_msg := '5.0 Error inserting into XXWC_QP_CSP_FINAL_GTT_TBL - '||SQLERRM;
         RAISE l_exception;
      END;

      COMMIT;
      fnd_file.put_line(fnd_file.log,'5.01 Start - '||TO_CHAR(SYSDATE, 'YYYY-MM-DD HH24:MI:SS'));
      -----------------------------------------------------------------------------
      -- Step# 5.01 Insert into XXWC_QP_OOL_GTT_TBL missing OOL
      -----------------------------------------------------------------------------
       l_sec := 'Step# 5.01 Insert into XXWC_QP_OOL_GTT_TBL missing OOL';
       BEGIN
       INSERT INTO xxwc.xxwc_qp_ool_gtt_tbl ool ( ool.PRICE_TYPE        
                , ool.ORDER_GROSS_MARGIN
                , ool.INCOMPATABILITY   
                , ool.ORGANIZATION_ID   
                , ool.CUSTOMER_ID       
                , ool.CUSTOMER_SITE_ID  
                , ool.AGREEMENT_ID      
                , ool.AGREEMENT_LINE_ID
                , ool.AGREEMENT_TYPE    
                , ool.VQ_NUMBER         
                , ool.ITEM_ATTRIBUTE    
                , ool.ITEM_NUMBER       
                , ool.ITEM_DESCRIPTION  
                , ool.START_DATE        
                , ool.END_DATE          
                , ool.LIST_PRICE        
                , ool.MODIFIER_TYPE     
                , ool.MODIFIER_VALUE    
                , ool.SELLING_PRICE     
                , ool.SPECIAL_COST      
                , ool.VENDOR_NUMBER     
                , ool.AVERAGE_COST      
                , ool.GROSS_MARGIN      
                , ool.LAST_UPDATE_DATE  
                , ool.LATEST_REC_FLAG   
                , ool.CREATION_DATE     
                , ool.REVISION_NUMBER   
                , ool.LAST_UPDATED_BY   
                , ool.CUSTOMER_NAME     
                , ool.CUSTOMER_NUMBER   
                , ool.LOCATION          
                , ool.PARTY_SITE_NUMBER 
                , ool.MODIFIER_NAME     
                , ool.SALESREP_ID
                , ool.SALESREP_NAME
                , ool.ORG_ID
--                , ool.ORGANIZATION_CODE
                , ool.LIST_HEADER_ID) 
                (SELECT /*+ index(csp XXWC_QP_CSP_GTT_N1) */ csp.PRICE_TYPE     --Ver 3.0 opa replaced with csp
                                              , csp.ORDER_GROSS_MARGIN
                                              , csp.INCOMPATABILITY   
                                              , csp.ORGANIZATION_ID   
                                              , csp.CUSTOMER_ID       
                                              , csp.CUSTOMER_SITE_ID  
                                              , csp.AGREEMENT_ID      
                                              , csp.AGREEMENT_LINE_ID
                                              , csp.AGREEMENT_TYPE    
                                              , csp.VQ_NUMBER         
                                              , csp.ITEM_ATTRIBUTE    
                                              , csp.ITEM_NUMBER       
                                              , csp.ITEM_DESCRIPTION  
                                              , csp.START_DATE        
                                              , csp.END_DATE          
                                              , csp.LIST_PRICE        
                                              , csp.MODIFIER_TYPE     
                                              , csp.MODIFIER_VALUE    
                                              , csp.SELLING_PRICE     
                                              , csp.SPECIAL_COST      
                                              , csp.VENDOR_NUMBER     
                                              , csp.AVERAGE_COST      
                                              , csp.GROSS_MARGIN      
                                              , csp.LAST_UPDATE_DATE  
                                              , csp.LATEST_REC_FLAG   
                                              , csp.CREATION_DATE     
                                              , csp.REVISION_NUMBER   
                                              , TO_CHAR(csp.LAST_UPDATED_BY) LAST_UPDATED_BY
                                              , csp.CUSTOMER_NAME     
                                              , csp.CUSTOMER_NUMBER   
                                              , csp.LOCATION          
                                              , csp.PARTY_SITE_NUMBER 
                                              , csp.MODIFIER_NAME     
                                              , csp.SALESREP_ID
                                              , csp.SALESREP_NAME
                                              , 162
--                                              , ool.ORGANIZATION_CODE
                                              , csp.LIST_HEADER_ID    
                                               FROM xxwc.xxwc_qp_csp_gtt_tbl csp
                                              WHERE NOT EXISTS (SELECT '1' FROM xxwc.xxwc_qp_ool_gtt_tbl ool WHERE csp.agreement_line_id = ool.agreement_line_id AND csp.customer_id = ool.customer_id));
      EXCEPTION
      WHEN OTHERS THEN
         l_err_msg := '5.01 Insert into XXWC_QP_OOL_GTT_TBL missing OOL- '||SQLERRM;
         RAISE l_exception;
      END;

      COMMIT;
      fnd_file.put_line(fnd_file.log,'5.02 Start - '||TO_CHAR(SYSDATE, 'YYYY-MM-DD HH24:MI:SS'));
      l_sec := 'Step# 5.02 Update XXWC_QP_OOL_GTT_TBL with ORGANIZATION_CODE';
      -----------------------------------------------------------------------------
      -- Step# 5.02 Update XXWC_QP_OOL_GTT_TBL with ORGANIZATION_CODE
      -----------------------------------------------------------------------------
      BEGIN
        UPDATE xxwc.xxwc_qp_ool_gtt_tbl ool
           SET ool.organization_code = (SELECT ood.organization_code 
                                          FROM org_organization_definitions ood
                                         WHERE ood.organization_id        = ool.organization_id
                                           AND rownum = 1)
         WHERE 1 = 1
           AND ool.organization_code IS NULL;
      EXCEPTION
      WHEN OTHERS THEN
         l_err_msg := '5.02 Error updating into XXWC_QP_OOL_GTT_TBL - '||SQLERRM;
         RAISE l_exception;
      END;

       COMMIT;
      fnd_file.put_line(fnd_file.log,'5.03 Start - '||TO_CHAR(SYSDATE, 'YYYY-MM-DD HH24:MI:SS'));
      l_sec := 'Step# 5.03 Update XXWC_QP_OOL_GTT_TBL with CSP Modifier details';
      -----------------------------------------------------------------------------
      -- Step# 5.03 Update XXWC_QP_OOL_GTT_TBL with CSP Modifier details
      -----------------------------------------------------------------------------
      BEGIN
         UPDATE XXWC.XXWC_QP_OOL_GTT_TBL stg
         set (modifier_name , list_header_id) = (SELECT /*+ XXWC_QP_LIST_HDR_N99 */  qlh.description, qlh.list_header_id
                                                                       FROM qp_list_headers_all qlh
                                                                      WHERE to_char(stg.agreement_id) = qlh.attribute14
                                                                          AND list_type_code = 'DLT'
                                                                          AND currency_code = 'USD'
                                                                          AND context = 162
                                                                          AND rownum = 1)
         where 1 = 1 
           and stg.modifier_name IS NULL;
      EXCEPTION
      WHEN OTHERS THEN
         l_err_msg := '5.03 Error updating XXWC_QP_OOL_GTT_TBL.MODIFIER_NAME - '||SQLERRM;
         RAISE l_exception;
      END;

      COMMIT;
      fnd_file.put_line(fnd_file.log,'5.04 Start - '||TO_CHAR(SYSDATE, 'YYYY-MM-DD HH24:MI:SS'));
      -----------------------------------------------------------------------------
      -- Step# 5.04 Update XXWC_QP_OOL_GTT_TBL with COST_AMOUNT_LAST_6MONTHS
      -----------------------------------------------------------------------------
      l_sec := 'Step# 5.04 Update XXWC_QP_OOL_GTT_TBL with COST_AMOUNT_LAST_6MONTHS';
      BEGIN
         DBMS_STATS.gather_index_stats (ownname => 'XXWC', indname => 'XXWC_QP_OOL_GTT_N3');
         DBMS_STATS.gather_index_stats (ownname => 'XXWC', indname => 'XXWC_QP_OOL_GTT_N4');
         
         UPDATE xxwc.xxwc_qp_ool_gtt_tbl ool2
            SET ool2.cost_amount_last_6months = ((SELECT /*+ XXWC_QP_OOL_GTT_N4 */ NVL (SUM ((  ool.unit_cost * NVL (ool.SHIPPED_QUANTITY, ool.ORDERED_QUANTITY))),0)  -- cost_amount_last_6months
                                                    FROM xxwc.xxwc_qp_ool_gtt_tbl ool
                                                   WHERE 1 = 1
                                                     AND ool.line_type_id NOT IN (1007, 1008)
                                                     AND ool.agreement_line_id = ool2.agreement_line_id
                                                     AND ool.customer_id       = ool2.customer_id
                                                     AND NVL (ool.actual_shipment_date, ool.request_date) >= SYSDATE - 180)
                                                   - 
                                                   (SELECT /*+ XXWC_QP_OOL_GTT_N3 */ NVL (SUM ((  ool.unit_cost * NVL (ool.SHIPPED_QUANTITY, ool.ORDERED_QUANTITY))),0)  -- cost_amount_last_6months
                                                     FROM xxwc.xxwc_qp_ool_gtt_tbl ool
                                                    WHERE 1 = 1
                                                      AND ool.line_type_id IN (1007, 1008)
                                                      AND ool.customer_id    = ool2.customer_id
                                                      AND ool.agreement_line_id = ool2.agreement_line_id
                                                      AND NVL (ool.actual_shipment_date, ool.request_date) >= SYSDATE - 180
                                                ))
          WHERE 1 = 1;
      EXCEPTION
      WHEN OTHERS THEN
         l_err_msg := '5.04 Error updating into XXWC_QP_OOL_GTT_TBL - '||SQLERRM;
         RAISE l_exception;
      END;

      COMMIT;
      fnd_file.put_line(fnd_file.log,'5.05 Start - '||TO_CHAR(SYSDATE, 'YYYY-MM-DD HH24:MI:SS'));
      -----------------------------------------------------------------------------
      -- Step# 5.05 Update XXWC_QP_OOL_GTT_TBL with UNITS_SOLD_IN_LAST_6MONTHS
      -----------------------------------------------------------------------------
      l_sec := 'Step# 5.05 Update XXWC_QP_OOL_GTT_TBL with UNITS_SOLD_IN_LAST_6MONTHS';
      BEGIN
        UPDATE xxwc.xxwc_qp_ool_gtt_tbl ool2
           SET Units_sold_in_last_6months = ((SELECT /*+ XXWC_QP_OOL_GTT_N4 */ NVL (SUM (NVL (ool.SHIPPED_QUANTITY, ool.ORDERED_QUANTITY)),0)
                                                FROM xxwc.xxwc_qp_ool_gtt_tbl ool
                                               WHERE 1 = 1
                                                 AND ool.line_type_id NOT IN (1007, 1008, 1003)
                                                 AND ool.agreement_line_id = ool2.agreement_line_id
                                                 AND ool.customer_id       = ool2.customer_id
                                                 AND NVL (ool.actual_shipment_date, ool.request_date) >= SYSDATE - 180)
                                                  -                                        --units_sold_in_last_6months,
                                              (SELECT /*+ XXWC_QP_OOL_GTT_N3 */ NVL (SUM (NVL (ool.SHIPPED_QUANTITY, ool.ORDERED_QUANTITY)),0)
                                                 FROM xxwc.xxwc_qp_ool_gtt_tbl ool
                                                WHERE 1 = 1
                                                  AND ool.line_type_id      = 1007
                                                  AND ool.agreement_line_id = ool2.agreement_line_id
                                                  AND ool.customer_id       = ool2.customer_id
                                                  AND NVL (ool.actual_shipment_date, ool.request_date) >= SYSDATE - 180
                                       ))
         WHERE 1 = 1;
      EXCEPTION
      WHEN OTHERS THEN
         l_err_msg := '5.05 Error updating into XXWC_QP_OOL_GTT_TBL - '||SQLERRM;
         RAISE l_exception;
      END;

      COMMIT;
      fnd_file.put_line(fnd_file.log,'5.06 Start - '||TO_CHAR(SYSDATE, 'YYYY-MM-DD HH24:MI:SS'));
      -----------------------------------------------------------------------------
      -- Step# 5.06 Update XXWC_QP_OOL_GTT_TBL with SALE_AMOUNT_LAST_6MONTHS
      -----------------------------------------------------------------------------
      l_sec := 'Step# 5.06 Update XXWC_QP_OOL_GTT_TBL with SALE_AMOUNT_LAST_6MONTHS';
      BEGIN
         UPDATE xxwc.xxwc_qp_ool_gtt_tbl ool2
            SET sale_amount_last_6months = ((SELECT /*+ XXWC_QP_OOL_GTT_N4 */ NVL (SUM ((ool.UNIT_SELLING_PRICE * NVL (ool.SHIPPED_QUANTITY, ool.ORDERED_QUANTITY))),0)
                                               FROM xxwc.xxwc_qp_ool_gtt_tbl ool
                                              WHERE 1 = 1
                                                AND ool.line_type_id NOT IN (1007, 1008)
                                                AND ool.agreement_line_id = ool2.agreement_line_id
                                                AND ool.customer_id    = ool2.customer_id
                                                AND NVL (ool.actual_shipment_date, ool.request_date) >= SYSDATE - 180)
                                                 -                                          --sale_amount_last_6months,
                                            (SELECT /*+ XXWC_QP_OOL_GTT_N3 */ NVL (SUM ((ool.UNIT_SELLING_PRICE * NVL (ool.SHIPPED_QUANTITY, ool.ORDERED_QUANTITY))),0)
                                               FROM xxwc.xxwc_qp_ool_gtt_tbl ool
                                              WHERE 1 = 1
                                                AND ool.line_type_id IN (1007, 1008)
                                                AND ool.agreement_line_id = ool2.agreement_line_id
                                                AND ool.customer_id    = ool2.customer_id
                                                AND NVL (ool.actual_shipment_date, ool.request_date) >= SYSDATE - 180
                                             ))
          WHERE 1 = 1;
      EXCEPTION
      WHEN OTHERS THEN
         l_err_msg := '5.06 Error updating into XXWC_QP_OOL_GTT_TBL - '||SQLERRM;
         RAISE l_exception;
      END;

      COMMIT;
      fnd_file.put_line(fnd_file.log,'5.07 Start - '||TO_CHAR(SYSDATE, 'YYYY-MM-DD HH24:MI:SS'));
      -----------------------------------------------------------------------------
      -- Step# 5.07 Update XXWC_QP_OOL_GTT_TBL with LastUpdatedBy
      -----------------------------------------------------------------------------
      l_sec := 'Step# 5.07 Update XXWC_QP_OOL_GTT_TBL with LastUpdatedBy';
      BEGIN
        UPDATE xxwc.xxwc_qp_ool_gtt_tbl ool
           SET ool.last_updated_by = (SELECT fu.description
                                        FROM fnd_user fu
                                       WHERE ool.last_updated_by = fu.user_id
                                         AND rownum = 1)
         WHERE 1 = 1;
      EXCEPTION
      WHEN OTHERS THEN
         l_err_msg := '5.07 Error updating into XXWC_QP_OOL_GTT_TBL - '||SQLERRM;
         RAISE l_exception;
      END;

      COMMIT;
      fnd_file.put_line(fnd_file.log,'5.08 Start - '||TO_CHAR(SYSDATE, 'YYYY-MM-DD HH24:MI:SS'));
      -----------------------------------------------------------------------------
      -- Step# 5.08 Update XXWC_QP_OOL_GTT_TBL with LAST_PURCHASE_DATE
      -----------------------------------------------------------------------------
      l_sec := 'Step# 5.08 Update XXWC_QP_OOL_GTT_TBL with LAST_PURCHASE_DATE';
      BEGIN
        UPDATE xxwc.xxwc_qp_ool_gtt_tbl ool2
           SET ool2.last_purchase_date = (SELECT /*+ XXWC_QP_OOL_GTT_N3 */MAX(NVL(ool.actual_shipment_date,ool.request_date))
                                           FROM xxwc.xxwc_qp_ool_gtt_tbl ool
                                          WHERE 1 = 1
                                            AND ool.line_type_id                             NOT IN (1007, 1008)
                                            AND ool.agreement_line_id                             = ool2.agreement_line_id
                                            AND ool.customer_id                                   = ool2.customer_id
                                            AND NVL (ool.actual_shipment_date, ool.request_date) >= SYSDATE - 180
                                        )
         WHERE 1 = 1;
      EXCEPTION
      WHEN OTHERS THEN
         l_err_msg := '5.08 Error updating into XXWC_QP_OOL_GTT_TBL - '||SQLERRM;
         RAISE l_exception;
      END;

-- Added for Revision 2.0 Begin <<	
  
      -----------------------------------------------------------------------------
      -- Step# 5.09 Update XXWC_QP_OOL_GTT_TBL with QP_MODIFIER_TYPE and QP_MODIFIER_VALUE
      -----------------------------------------------------------------------------
      l_sec := 'Step# 5.09 Update XXWC_QP_OOL_GTT_TBL with QP_MODIFIER_TYPE and QP_MODIFIER_VALUE';
      BEGIN
        UPDATE xxwc.xxwc_qp_ool_gtt_tbl ool
		   SET (qp_modifier_type, qp_modifier_value) =
				  (SELECT DECODE (price_by_formula_id,
								  7030, 'Cost Plus',
								  52060, 'Percent Discount',
								  'New Price')
							 qp_modifier_type,
						  qll.operand qp_modifier_value
					 FROM qp_list_lines qll
					WHERE     qll.list_header_id = ool.list_header_id
						  AND qll.attribute2 = ool.agreement_line_id
						  AND qll.arithmetic_operator = 'NEWPRICE'
						  AND qll.list_line_type_code = 'DIS'
						  AND NVL (qll.start_date_active, TRUNC (SYSDATE)) =
							  NVL (
								 (SELECT MAX (start_date_active)
									FROM qp_list_lines qll_max
								   WHERE     qll_max.list_header_id = qll.list_header_id
										 AND qll_max.attribute2 = qll.attribute2
										 AND qll_max.arithmetic_operator =
												qll.arithmetic_operator
										 AND qll_max.list_line_type_code =
												qll.list_line_type_code),
								 NVL (qll.start_date_active, TRUNC (SYSDATE)))
						  AND ROWNUM = 1);
      EXCEPTION
      WHEN OTHERS THEN
         l_err_msg := '5.09 Error updating XXWC_QP_OOL_GTT_TBL with QP_MODIFIER_TYPE and QP_MODIFIER_VALUE - '||SQLERRM;
         RAISE l_exception;
      END;

-- Added for Revision 2.0 End >>

      fnd_file.put_line(fnd_file.log,'5.07 End - '||TO_CHAR(SYSDATE, 'YYYY-MM-DD HH24:MI:SS'));
      COMMIT;
      fnd_file.put_line(fnd_file.log,'5.07 COMMIT - '||TO_CHAR(SYSDATE, 'YYYY-MM-DD HH24:MI:SS'));

      fnd_file.put_line(fnd_file.log,'6.0 FILE Creation Time Start - '||TO_CHAR(SYSDATE, 'YYYY-MM-DD HH24:MI:SS'));

        APPS.xxwc_qp_csp_extract_pkg.CREATE_FILE (p_errbuf              => p_errbuf
                                              , p_retcode             => p_retcode
                                              , p_interface_name      => 'XXWC_CSP_INTERFACE'
                                              , p_view_name           => 'XXWC_CSP_EXTRACT_VW'
                                              , p_directory_path      => p_directory_path
                                              , p_file_name           => p_file_name
                                              , p_org_name            => 'HDS White Cap - Org' );

      fnd_file.put_line(fnd_file.log,'6.0 FILE Creation Time End - '||TO_CHAR(SYSDATE, 'YYYY-MM-DD HH24:MI:SS'));


      fnd_file.put_line(fnd_file.log,'RETURNCODE - '||p_retcode);
      
      
EXCEPTION
   WHEN l_exception THEN
       COMMIT;
       p_retcode := 2;
       p_errbuf := l_err_msg;
       fnd_file.put_line(fnd_file.log,l_err_msg);
   WHEN OTHERS THEN
      COMMIT;
      l_err_msg  := substr ( (l_err_msg || ' ERROR ' || substr(SQLERRM, 1, 2000)), 1, 3000);
      dbms_output.put_line(l_err_msg);

      xxcus_error_pkg.xxcus_error_main_api(p_called_from       => 'XXWC_QP_CSP_EXTRACT_PKG.LOAD_STAGING'
                                          ,p_calling           => l_sec
                                          ,p_request_id        => fnd_global.conc_request_id
                                          ,p_ora_error_msg     => substr(SQLERRM, 1, 2000)
                                          ,p_error_desc        => 'Error in B2B Customer LOAD_STAGING procedure, When Others Exception'
                                          ,p_distribution_list => pl_dflt_email
                                          ,p_module            => 'CSP');

       p_retcode := 2;
       p_errbuf := l_err_msg;

       fnd_file.put_line(fnd_file.log,'Error in  LOAD_STAGING procedure - '||SQLERRM);
   END load_staging;

/*************************************************************************
Procedure : CREATE_FILE

PURPOSE:   This procedure creates file on XXIFACE Directory

REVISIONS:
Ver        Date        Author                     Description
---------  ----------  ---------------         -------------------------
1.0        08/01/2014  Gopi Damuluri             Initial Version
2.0        06/25/2015  Manjula Chellappan        TMS# 20150624-00229 - CSP Enhancement Bundle - Item #1 - CSP Extract      
***********************************************************************/

PROCEDURE CREATE_FILE (p_errbuf           OUT     VARCHAR2,
               p_retcode          OUT     NUMBER,
               p_interface_name    IN     VARCHAR2,
               p_view_name         IN     VARCHAR2,
               p_directory_path    IN     VARCHAR2,
               p_file_name         IN     VARCHAR2,
               p_org_name          IN     VARCHAR2
              )
       IS

    -- Intialize Variables
    l_err_msg                 VARCHAR2(2000);
    l_err_code                NUMBER;
    l_sec                     VARCHAR2(150);
    l_procedure_name          VARCHAR2(75) := 'xxwc_qp_csp_extract_pkg.create_file';

    -- Reference Cursor Variables
    TYPE ref_cur IS REF CURSOR;
    view_cur                  ref_cur;
    view_rec                  XXWC_OB_COMMON_PKG.xxcus_ob_file_rec;

    --File Variables
    l_file_handle         utl_file.file_type;
    l_file_name           VARCHAR2(150);
    l_file_dir            VARCHAR2(100);
    l_file_name_temp      VARCHAR2(150);
    l_org_id              NUMBER;
    l_org_name            VARCHAR2(240);
    l_rec_cnt             NUMBER := 0;
    l_dir_exists          NUMBER;
    l_file_hdr            VARCHAR2(2000);

    l_program_error       EXCEPTION;

BEGIN

        fnd_file.put_line(fnd_file.log,'**********************************************************************************');
        fnd_file.put_line(fnd_file.log,'----------------------------------------------------------------------------------');
        fnd_file.put_line(fnd_file.log,'Start of Procedure : create_file');
        fnd_file.put_line(fnd_file.log,'----------------------------------------------------------------------------------');
        fnd_file.put_line(fnd_file.log,'Input Parameters     :');
        fnd_file.put_line(fnd_file.log,'p_interface_name     :'||p_interface_name);
        fnd_file.put_line(fnd_file.log,'p_view_name          :'||p_view_name);
        fnd_file.put_line(fnd_file.log,'p_directory_path     :'||p_directory_path);
        fnd_file.put_line(fnd_file.log,'p_file_name          :'||p_file_name);
        fnd_file.put_line(fnd_file.log,'p_org_name           :'||p_org_name);
        fnd_file.put_line(fnd_file.log,'----------------------------------------------------------------------------------');

        l_org_name := p_org_name;
        l_sec := 'Create File concurrent request '||p_interface_name;

        -- Check if directory path exists
        l_dir_exists := 0;
        BEGIN
          SELECT COUNT(1)
            INTO l_dir_exists
            FROM all_directories
           WHERE directory_name = p_directory_path;
        EXCEPTION
        WHEN OTHERS THEN
          l_err_msg := 'Error validating directory   :'||SQLERRM;
          RAISE l_program_error;
        END;

        -- Raise ProgramError if directory path does not exist
        IF l_dir_exists = 0 THEN
          l_err_msg := 'Directory does not exist in Oracle :'||p_directory_path;
          RAISE l_program_error;
        END IF;
        
        l_sec := 'Derive Operating Unit details';
        -- Derive Operating Unit details
        BEGIN
        SELECT organization_id
          INTO l_org_id
          FROM hr_operating_units
         WHERE name = l_org_name;
        EXCEPTION
        WHEN NO_DATA_FOUND THEN
          l_err_msg := 'OperatingUnit does not exist :'||l_org_name;
          RAISE l_program_error;
        WHEN OTHERS THEN
          l_err_msg := 'Error deriving OperatingUnit details :'||l_org_name;
          RAISE l_program_error;
        END;

        fnd_file.put_line(fnd_file.log,'l_org_id   :'||l_org_id);

        l_file_name       := p_file_name;
        l_file_name_temp  := 'TEMP_'||l_file_name;

        l_sec := 'Delete if file already exists';
        -- Delete if file already exists
        BEGIN
          UTL_FILE.FREMOVE (p_directory_path,l_file_name);
          UTL_FILE.FREMOVE (p_directory_path,l_file_name_temp);
        EXCEPTION
        WHEN OTHERS THEN
          NULL;
        END;

        l_sec := 'Start creating the file';
        -- Start creating the file
        l_file_handle := utl_file.fopen(p_directory_path, l_file_name_temp, 'w', 32767);

-- Added columns qp_modifier_code and qp_modifier_value to the below statement for Revision 2.0 
		
        l_file_hdr := 'customer_name'
||'|'||'customer_number'
||'|'||'party_site_number'
||'|'||'location'
||'|'||'salesrep_name'
||'|'||'salesrep_id'
||'|'||'price_type'
||'|'||'organization_code'
||'|'||'order_gross_margin'
||'|'||'incompatability'
||'|'||'agreement_id'
||'|'||'agreement_line_id'
||'|'||'agreement_type'
||'|'||'vq_number'
||'|'||'item_attribute'
||'|'||'item_number'
||'|'||'item_description'
||'|'||'start_date'
||'|'||'end_date'
||'|'||'list_price'
||'|'||'modifier_type'
||'|'||'modifier_value'
||'|'||'selling_price'
||'|'||'special_cost'
||'|'||'vendor_number'
||'|'||'average_cost'
||'|'||'gross_margin'
||'|'||'last_updated_by'
||'|'||'last_update_date'
||'|'||'latest_rec_flag'
||'|'||'modifier_name'
||'|'||'sale_amount_last_6months'
||'|'||'units_sold_in_last_6months'
||'|'||'cost_amount_last_6months'
||'|'||'last_purchase_date'
||'|'||'creation_date'
||'|'||'revision_number'
||'|'||'qp_modifier_code'
||'|'||'qp_modifier_value';

        -- Write file header
        utl_file.put_line (l_file_handle, l_file_hdr);

        OPEN view_cur for 'SELECT * FROM '||p_view_name|| ' WHERE org_id = '||l_org_id;

        -- Write data into the File
        LOOP
           FETCH view_cur INTO view_rec;

           EXIT WHEN view_cur%NOTFOUND;

           -- Write file info
           utl_file.put_line (l_file_handle, view_rec.REC_LINE);
        END LOOP;

        l_sec := 'Close the file';
        -- Close the file
        utl_file.fclose(l_file_handle);

        l_sec := 'Rename file for pickup';
        -- 'Rename file for pickup';
        utl_file.frename(p_directory_path, l_file_name_temp, p_directory_path, l_file_name);

        fnd_file.put_line(fnd_file.output,'----------------------------------------------------------------------------------');
        fnd_file.put_line(fnd_file.output,'**********************************************************************************');

        fnd_file.put_line(fnd_file.log,'----------------------------------------------------------------------------------');
        fnd_file.put_line(fnd_file.log,'End of Procedure : create_file');
        fnd_file.put_line(fnd_file.log,'----------------------------------------------------------------------------------');

EXCEPTION -- this section traps my errors
    WHEN l_program_error THEN
      l_err_code := 2;
--      l_err_msg  := l_sec || l_err_msg || ' ERROR ' || SQLCODE ||substr(SQLERRM, 1, 2000);

      fnd_file.put_line(fnd_file.log, l_err_msg);
      fnd_file.put_line(fnd_file.output, l_err_msg);

      utl_file.fclose(l_file_handle);
      utl_file.fremove(l_file_dir, l_file_name);

      xxcus_error_pkg.xxcus_error_main_api(p_called_from => l_procedure_name,
                                           p_calling => l_sec,
                                           p_request_id => fnd_global.conc_request_id,
                                           p_ora_error_msg => l_err_msg,
                                           p_error_desc => 'Error creating CSP Extract with Program Error Exception',
                                           p_distribution_list => pl_dflt_email,
                                           p_module => 'XXWC');
    WHEN OTHERS THEN
      l_err_code := 2;
      l_err_msg  := l_sec || l_err_msg || ' ERROR ' || SQLCODE ||
                    substr(SQLERRM, 1, 2000);

      fnd_file.put_line(fnd_file.log, l_err_msg);
      fnd_file.put_line(fnd_file.output, l_err_msg);

      utl_file.fclose(l_file_handle);
      utl_file.fremove(l_file_dir, l_file_name);

      xxcus_error_pkg.xxcus_error_main_api(p_called_from => l_procedure_name,
                                           p_calling => l_sec,
                                           p_request_id => fnd_global.conc_request_id,
                                           p_ora_error_msg => SQLERRM,
                                           p_error_desc => 'Error creating CSP Extract with Others Exception',
                                           p_distribution_list => pl_dflt_email,
                                           p_module => 'XXWC');
END CREATE_FILE;

END xxwc_qp_csp_extract_pkg;
/
