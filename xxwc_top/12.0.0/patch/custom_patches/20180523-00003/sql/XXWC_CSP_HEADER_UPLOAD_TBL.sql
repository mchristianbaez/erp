/*************************************************************************
      SCRIPT Name: XXWC_CSP_HEADER_UPLOAD_TBL.sql

      PURPOSE:   This table hold the CSP header data uploaded through file

      REVISIONS:
      Ver        Date          Author             Description
      ---------  ------------  ------------       ------------------
      1.0        05/31/2018    Niraj K Ranjan     TMS#20180523-00003   AH Conversion CSP Headers
 ***************************************************************************/
CREATE TABLE XXWC.XXWC_CSP_HEADER_UPLOAD_TBL
       (PRICE_TYPE VARCHAR2(30),
	    ACCOUNT_NUMBER VARCHAR2(30),
		ACCOUNT_NAME VARCHAR2(360),
		SITE_NUMBER VARCHAR2(30),
		BRANCH VARCHAR2(3),
		PROCESS_FLAG VARCHAR2(50),
		ERROR_MESSAGE VARCHAR2(2000),
		AGREEMENT_ID  NUMBER,
		CREATION_DATE DATE,
		CREATED_BY NUMBER,
		LAST_UPDATE_DATE DATE,
		LAST_UPDATED_BY NUMBER);