---------------------------------------------------------------------------------------------------------------
/**************************************************************************************************************
  $Header XXEIS.XXWC_VQN_SUBQUERY3_DTL_TBL $
  Module Name : Order Management
  PURPOSE	  : Vendor Qoute Batch Summary Report
  VERSION 		DATE            AUTHOR(S)       	DESCRIPTION
  ------- -----------------  --------------- -----------------------------------------
  1.0 	  	 07-Jul-2017         Siva			  TMS#20170626-0030
**************************************************************************************************************/
CREATE GLOBAL TEMPORARY TABLE XXEIS.XXWC_VQN_SUBQUERY3_DTL_TBL
  (
    NAME                VARCHAR2(240) ,
    LINE_NUMBER         NUMBER ,
    LINE_ID             NUMBER ,
    ORDER_NUMBER        NUMBER ,
    VENDOR_NUMBER       VARCHAR2(30) ,
    VENDOR_NAME         VARCHAR2(240) ,
    VENDOR_ID           NUMBER ,
    INVENTORY_ITEM_ID   NUMBER ,
    ORGANIZATION_ID     NUMBER ,
    ORGANIZATION_NAME   VARCHAR2(240) ,
    SOURCE_TYPE_CODE    VARCHAR2(30) ,
    ORACLE_QUOTE_NUMBER VARCHAR2(30) ,
    PART_NUMBER         VARCHAR2(40) ,
    UOM                 VARCHAR2(25) ,
    DESCRIPTION         VARCHAR2(240) ,
    AVERAGE_COST        NUMBER ,
    SPECIAL_COST        NUMBER ,
    CREATION_DATE       DATE ,
    LOCATION            VARCHAR2(3) ,
    QTY                 NUMBER ,
    CUSTOMER_TRX_ID     NUMBER ,
    INVOICE_NUMBER      VARCHAR2(20) ,
    INVOICE_DATE        DATE ,
    LOC                 VARCHAR2(30) ,
    GL_CODING           VARCHAR2(200) ,
    GL_STRING           VARCHAR2(200) ,
    CREATED_BY          NUMBER ,
    SALESREP_ID         NUMBER ,
    CUSTOMER_ID         NUMBER ,
    CUSTOMER_SITE_ID    NUMBER ,
    HEADER_ID           NUMBER ,
    FULL_NAME           VARCHAR2(240) ,
    SALESREP_NUMBER     VARCHAR2(30) ,
    SALESREP_NAME       VARCHAR2(360),
    ACCOUNT_NAME        VARCHAR2(360) ,
    ACCOUNT_NUMBER      VARCHAR2(30) ,
    SHIP_TO             VARCHAR2(40) ,
    PARTY_SITE_NUMBER   VARCHAR2(30) ,
    LIST_PRICE_PER_UNIT NUMBER ,
    LIST_NUM            NUMBER 
  ) ON COMMIT PRESERVE ROWS
/
