---------------------------------------------------------------------------------------------------------------
/**************************************************************************************************************
  $Header XXEIS.XXWC_VQN_SUBQUERY1_DTL_N1 $
  Module Name : Order Management
  PURPOSE	  : Vendor Qoute Batch Summary Report
  VERSION 		DATE               AUTHOR(S)       DESCRIPTION
  ------- -----------------  --------------- -----------------------------------------
  1.0 	  	 07-Jul-2017         	Siva			  TMS#20170626-0030
**************************************************************************************************************/
CREATE INDEX XXEIS.XXWC_VQN_SUBQUERY1_DTL_N1 ON XXEIS.XXWC_VQN_SUBQUERY1_DTL_TBL
  (
    SHIP_FROM_ORG_ID,
    INVOICE_DATE
  ) 
/




