---------------------------------------------------------------------------------------------------------------
/**************************************************************************************************************
  $Header XXEIS.XXWC_VQN_SUBQUERY1_DTL_TBL $
  Module Name : Order Management
  PURPOSE	  : Vendor Qoute Batch Summary Report
  VERSION 		DATE            AUTHOR(S)       	DESCRIPTION
  ------- -----------------  --------------- -----------------------------------------
  1.0 	  	 07-Jul-2017         Siva			  TMS#20170626-0030
**************************************************************************************************************/
CREATE GLOBAL TEMPORARY TABLE XXEIS.XXWC_VQN_SUBQUERY1_DTL_TBL
  (
    LINE_ID              NUMBER,
    INVENTORY_ITEM_ID    NUMBER,
    SHIP_FROM_ORG_ID     NUMBER,
    LINE_NUMBER          NUMBER,
    OL_CREATION_DATE     DATE,
    SOURCE_TYPE_CODE     VARCHAR2(30 BYTE),
    GL_DATE              DATE,
    CUSTOMER_TRX_LINE_ID NUMBER,
    CODE_COMBINATION_ID  NUMBER,
    CUSTOMER_TRX_ID      NUMBER,
    INVOICE_NUMBER       VARCHAR2(20 BYTE),
    INVOICE_DATE         DATE,
    QTY                  NUMBER,
    HEADER_ID            NUMBER,
    ORDER_NUMBER         NUMBER,
    OH_CREATION_DATE     DATE,
    CREATED_BY           NUMBER,
    SALESREP_ID          NUMBER,
    FULL_NAME            VARCHAR2(240 BYTE),
    SALESREP_NUMBER      VARCHAR2(30 BYTE),
    SALESREP_NAME        VARCHAR2(360 BYTE),
    ACCOUNT_NAME         VARCHAR2(360 BYTE),
    ACCOUNT_NUMBER       VARCHAR2(30 BYTE),
    CUST_ACCOUNT_ID      NUMBER,
    SHIP_TO              VARCHAR2(40 BYTE),
    PARTY_SITE_NUMBER    VARCHAR2(30 BYTE),
    SHIP_TO_ORG_ID       NUMBER,
    SOLD_TO_ORG_ID       NUMBER
  )
  ON COMMIT PRESERVE ROWS
/
