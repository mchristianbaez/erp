/********************************************************************************************************************************
PURPOSE:   Datafix Script for Order #24737636
REVISIONS:
Ver        Date            Author                     Description
---------  ----------      ---------------         -------------------------------------------------------------------------------
1.0        04-Oct-2017     Sundar                   TMS #20171003-00066
**************************************************************************************************************************************/
clear buffer;
SET serveroutput ON size 500000

DECLARE
  
  CURSOR lines
  IS
    SELECT line_id
    FROM oe_order_lines_all
    WHERE line_id              =99101851
    AND open_flag              ='Y'
    AND flow_status_code       ='AWAITING_RETURN'
    AND NVL(shipped_quantity,0)=0;
  l_result      VARCHAR2(30);
  l_activity_id NUMBER;
  l_file_val    VARCHAR2(500);

  BEGIN
  oe_debug_pub.debug_on;
  oe_debug_pub.initialize;
  l_file_val:=OE_DEBUG_PUB.Set_Debug_Mode('FILE');
  oe_Debug_pub.setdebuglevel(5);
  
  dbms_output.put_line(' Inside the script');
  dbms_output.put_line('file path is:'||l_file_val);
  DBMS_OUTPUT.put_line ('TMS: 20171003-00066   before loop');
  
  FOR i IN lines
  LOOP
    OE_Standard_WF.OEOL_SELECTOR (p_itemtype => 'OEOL' ,p_itemkey => TO_CHAR(i.line_id) ,p_actid => 12345 ,p_funcmode => 'SET_CTX' ,p_result => l_result );
    oe_debug_pub.add('Retrun value of OEOL_selector function is'||l_result);
    
	UPDATE oe_order_lines_all SET ordered_quantity =660 WHERE line_id = i.line_id;
    
	UPDATE oe_order_lines_all
    SET shipped_quantity=ordered_quantity,
      fulfilled_quantity=ordered_quantity,
      flow_status_code  ='AWAITING_FULFILLMENT',
      last_update_date  =SYSDATE,
      last_updated_by   =-16516164
    WHERE line_id       =i.line_id
    AND flow_status_code='AWAITING_RETURN';
    
	wf_engine.handleError('OEOL', TO_CHAR(i.line_id),'RMA_WAIT_FOR_RECEIVING','SKIP','COMPLETE');
    wf_engine.handleError('OEOL', TO_CHAR(i.line_id),'RMA_WAIT_FOR_INSPECTION','SKIP','COMPLETE');
  END LOOP;
  
  oe_debug_pub.debug_off;
  
  UPDATE oe_order_lines_all
  SET UNIT_LIST_PRICE        = 0,
    UNIT_LIST_PRICE_PER_PQTY = NULL,
    link_to_line_id          = 98749402,
    RETURN_ATTRIBUTE1        = 59849069,
    return_attribute2        = 98749402
  WHERE line_id              = 106983341;
  DBMS_OUTPUT.put_line ('TMS: 20171003-00066   Script Completed');
  
  COMMIT;
EXCEPTION
WHEN OTHERS THEN
  DBMS_OUTPUT.put_line('Error occurred: ' || SQLERRM);
  Oe_debug_pub.ADD('Error occurred: ' || SQLERRM);
  ROLLBACK;
END;
/
