/********************************************************************************
   $Header XXWC_AR_RECEIVABLE_APPL_N10.sql $
   Module Name: XXWC_AR_RECEIVABLE_APPL_N10

   PURPOSE:   Pick and Pack Performance when multiple payments exists

   REVISIONS:
   Ver        Date        Author                     Description
   ---------  ----------  ---------------         -------------------------
   1.0        05/12/2017  Rakesh Patel            TMS#20170307-00153-Pick and Pack Performance when multiple payments exists
********************************************************************************/
CREATE INDEX XXWC.XXWC_AR_RECEIVABLE_APPL_N10 ON AR.AR_RECEIVABLE_APPLICATIONS_ALL (APPLICATION_REF_ID);

