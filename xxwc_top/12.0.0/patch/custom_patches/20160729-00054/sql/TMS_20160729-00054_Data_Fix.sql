/**************************************************************************************************************************
TMS#20160729-00054  On behalf of Mike Barton Store 290 - Sales Order #21023384 line 1.1 is stuck in "Booked" status

Creatd by : P.Vamshidhar

Date: 09-Aug-2016

/**************************************************************************************************************************/

SET SERVEROUTPUT ON SIZE 100000;
SET VERIFY OFF;

DECLARE
   l_file_val        VARCHAR2 (2000);
   l_result_out      VARCHAR2 (30);
   err_msg           VARCHAR2 (240);
   l_return_status   BOOLEAN := FALSE;
   v_org_id          NUMBER;
   v_line_id         NUMBER := 73378742;

   CURSOR pending_receipts
   IS
      SELECT rcv.transaction_id
        FROM rcv_transactions rcv,
             po_line_locations_all poll,
             oe_drop_ship_sources oed,
             oe_order_lines_all oel
       WHERE     oel.source_type_code = 'EXTERNAL'
             AND NVL (oel.shipped_quantity, 0) = 0
             AND oed.line_id = oel.line_id
             AND oed.line_location_id IS NOT NULL
             AND poll.line_location_id = oed.line_location_id
             AND rcv.po_line_location_id = poll.line_location_id
             AND oel.line_id = v_line_id
             AND rcv.TRANSACTION_TYPE = 'DELIVER';

   CURSOR organization (v_order_line_id NUMBER)
   IS
      SELECT org_id
        FROM oe_order_lines_all
       WHERE line_id = v_order_line_id;
BEGIN
   oe_debug_pub.debug_on;
   oe_debug_pub.initialize;
   l_file_val := OE_DEBUG_PUB.Set_Debug_Mode ('FILE');
   oe_Debug_pub.setdebuglevel (5);
   DBMS_OUTPUT.put_line (
      'Working for Line_Id : ' || v_line_id || '       Pls  wait ...');
   oe_debug_pub.add ('Working for Line_Id : ' || v_line_id);

   oe_debug_pub.add ('Updating the flow_status_code');

   UPDATE oe_order_lines_all oeol
      SET oeol.flow_status_code = 'AWAITING_RECEIPT'
    WHERE     oeol.line_id = v_line_id
          AND oeol.flow_status_code <> 'AWAITING_RECEIPT';


   OPEN organization (v_line_id);

   FETCH organization INTO v_org_id;

   CLOSE organization;

   DBMS_OUTPUT.put_line ('Organization ID set is : ' || v_org_id);
   OE_DEBUG_PUB.ADD ('Setting client info to ' || v_org_id);
   mo_global.init ('ONT');
   MO_GLOBAL.SET_POLICY_CONTEXT ('S', v_org_id);

   --fnd_client_info.set_org_context(to_char(v_org_id));

   FOR all_lines IN pending_receipts
   LOOP
      l_return_status :=
         OE_DS_PVT.DROPSHIPRECEIVE (all_lines.transaction_id, 'INV');

      IF l_return_status = TRUE
      THEN
         OE_DEBUG_PUB.ADD ('l_return_status = TRUE');
         DBMS_OUTPUT.PUT_LINE ('  Script was successfully ..... !!!   ');
         OE_DEBUG_PUB.ADD ('   Script was successfully .....  !!!! ');
      ELSE
         OE_DEBUG_PUB.ADD ('l_return_status = FALSE');
         DBMS_OUTPUT.PUT_LINE ('  Script was Un-successfully .....  !!!  ');
         OE_DEBUG_PUB.ADD (
            '   Hard Luck !!! Script was Un-successfully ..... !!!  ');
      END IF;
   END LOOP;

   COMMIT;

   DBMS_OUTPUT.put_line (
         'Debug file name and path: '
      || OE_DEBUG_PUB.G_DIR
      || '/'
      || OE_DEBUG_PUB.G_FILE);
   DBMS_OUTPUT.PUT_LINE ('.');
EXCEPTION
   WHEN OTHERS
   THEN
      ERR_MSG := 'ERROR :' || SQLERRM;
      DBMS_OUTPUT.PUT_LINE ('ERROR: Pls Rollback.................');
      DBMS_OUTPUT.PUT_LINE (SQLERRM);
      OE_DEBUG_PUB.ADD (ERR_MSG);
END;
/