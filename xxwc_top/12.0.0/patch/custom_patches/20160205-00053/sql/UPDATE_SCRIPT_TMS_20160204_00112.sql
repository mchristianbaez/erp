update xxwc.xxwc_b2b_so_hdrs_stg_tbl 
set process_flag = 'N'
where ship_to_number = '2141808'
and rownum < 4;

COMMIT;

update xxwc.xxwc_b2b_so_lines_stg_tbl
set process_flag = 'N'
, customer_po_number = customer_po_number||'A'
where ship_to_number = '2141808'
and customer_po_number IN (SELECT customer_po_number FROM xxwc.xxwc_b2b_so_hdrs_stg_tbl WHERE process_flag = 'N');

COMMIT;

update xxwc.xxwc_b2b_so_hdrs_stg_tbl 
set customer_po_number = customer_po_number||'A'
where process_flag = 'N';

COMMIT;
