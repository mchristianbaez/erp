/*
 TMS: 20151028-00036    
 Date: 10/20/2015
 Notes: data fix script to process month end transactions
*/

SET SERVEROUTPUT ON SIZE 100000;
SET VERIFY OFF;


update apps.wsh_delivery_details
set OE_INTERFACED_FLAG='Y'
where delivery_detail_id=12797890;
		  
--1 row expected to be updated

commit;

/