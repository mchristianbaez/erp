CREATE OR REPLACE PACKAGE APPS.xxwc_pim_chg_item_mgmt
AS
   /**********************************************************************************************************
   -- File Name: XXWC_PIM_CHG_ITEM_MGMT.pks
   --
   -- PROGRAM TYPE: PL/SQL Script   <API>
   --
   -- PURPOSE:
   -- HISTORY
   -- ========================================================================================================
   -- ========================================================================================================
   -- VERSION DATE          AUTHOR(S)       DESCRIPTION
   -- ------- -----------   --------------- ------------------------------------------------------------------
   -- 1.0     01-MAR-2015   Shankar Vanga   TMS#20150126-00044  - Create Single Item Workflow Tool
   -- 2.0     01-JUN-2015   P.Vamshidhar    TMS#20150519-00091 - Added new procedure XXWC_EGO_VENDOR_INSERT
   --                                       to store Vendor Number.
   -- 3.0     12-JUN-2015   P.Vamshidhar    TMS#20150608-00031  - Single Item UI Enhancements
   --                                       declared delete_item spec level
   -- 3.1     12-Oct-2015   P.vamshidhar    TMS#20150917-00149 - PCAM add validation for item check
   --                                       during WF approval
   ***********************************************************************************************************/

   PROCEDURE CREATE_CHANGE_ORDER (
      p_item_level                     VARCHAR2,
      p_new_item                       VARCHAR2 DEFAULT 'N',
      p_co_description                 VARCHAR2,
      p_revised_item                   VARCHAR2,
      P_UOM_CODE                       VARCHAR2 DEFAULT NULL,
      p_hazard_mat                     VARCHAR2 DEFAULT 'N',
      p_reson_code                     VARCHAR2,
      p_vendor_number                  VARCHAR2, -- Added by Vamshi in V2.0  TMS#20150519-00091
      x_change_id           OUT NOCOPY NUMBER,
      x_item_id             OUT NOCOPY NUMBER,
      x_change_name         OUT NOCOPY VARCHAR2,
      x_return_status       OUT NOCOPY VARCHAR2,
      x_error_msg           OUT NOCOPY VARCHAR2);

   PROCEDURE REPLACE_ITEM (p_old_item_number                 VARCHAR2,
                           p_new_item_number                 VARCHAR2,
                           p_new_uom                         VARCHAR2,
                           p_item_id                         NUMBER,
                           x_ret_mess             OUT NOCOPY VARCHAR2,
                           x_request_id           OUT NOCOPY NUMBER);

   PROCEDURE replace_item_prg (errbuf                 OUT NOCOPY VARCHAR2,
                               retcode                OUT NOCOPY VARCHAR2,
                               p_old_item_number                 VARCHAR2,
                               p_new_item_number                 VARCHAR2,
                               p_new_uom                         VARCHAR2,
                               p_item_id                         NUMBER);

   PROCEDURE xxwc_delete_constraint (p_table_name         VARCHAR2,
                                     p_constraint_name    VARCHAR2);


   -- Added by Vamshi in V2.0  TMS#20150519-00091  Introduced new procedure to store vendor info.

   PROCEDURE XXWC_EGO_VENDOR_INSERT (p_login_id        IN VARCHAR2,
                                     p_vendor_number   IN VARCHAR2,
                                     p_change_name     IN VARCHAR2,
                                     p_status          IN VARCHAR2);

   -- Added below function declaration by Vamshi in V3.0  TMS#20150608-00031
   FUNCTION delete_item (p_item_number VARCHAR2)
      RETURN NUMBER;

   -- Added below function to check item already exist in TMS#20150917-00149.

   FUNCTION Item_Number_validate (p_item_number VARCHAR2)
      RETURN VARCHAR2;

   -- Added below Procedure to update item number in temp table in TMS#20150917-00149.
   PROCEDURE Temp_table_Item_update (p_item_id    IN NUMBER,
                                     p_new_item   IN VARCHAR2);
END;
/
