CREATE OR REPLACE PACKAGE BODY APPS.xxwc_pim_chg_item_mgmt
AS
   /*****************************************************************************************************************************
   -- File Name: xxwc_pim_chg_item_mgmt.pkb
   --
   -- PROGRAM TYPE: PL/SQL Script   <API>
   --
   -- PURPOSE:
   -- HISTORY
   -- ===========================================================================================================================
   -- ===========================================================================================================================
   -- VERSION DATE          AUTHOR(S)       DESCRIPTION
   -- ------- -----------   --------------- -------------------------------------------------------------------------------------
   -- 1.0     01-MAR-2015   Shankar Vanga   TMS#20150126-00044  - Create Single Item Workflow Tool
   -- 2.0     01-JUN-2015   P.Vamshidhar     TMS#20150519-00091 - Added new procedure XXWC_EGO_VENDOR_INSERT
   --                                        to store Vendor Number.
   -- 3.0     28-JUL-2015   P.vamshidhar    TMS#20150728-00035 - added updates for full and thumbnail images while replacing
                                            item number in procedure replace_item_prg
   -- 4.0     30-Jul-2015   P.Vamshidhar    TMS#20150728-00134 - Exception handling in Producre process_change_request
   -- 4.1     12-Oct-2015   P.vamshidhar    TMS#20150917-00149 - PCAM add validation for item check
   --                                       during WF approval
    *****************************************************************************************************************************/

   G_DRAFT_ICC             VARCHAR2 (80) := 'UNCLASSIFIED';
   G_NEW_ITEM_STATUS       VARCHAR2 (80) := 'New Item';
   G_CHANGE_LINE_TYPE      VARCHAR2 (80) := 'Draft Item';
   g_Object_Display_Name   VARCHAR2 (30) := 'Item';
   g_err_callfrom          VARCHAR2 (100) := 'XXWC_PIM_CHG_ITEM_MGMT';
   g_err_callpoint         VARCHAR2 (150); -- Added variable in TMS#20150728-00134 by Vamshi
   g_distro_list           VARCHAR2 (100)
                              := 'HDSOracleDevelopers@hdsupply.com';


   PROCEDURE create_item (
      p_item_number                   VARCHAR2,
      P_UOM                           VARCHAR2,
      p_hazard_mat                    VARCHAR2 DEFAULT 'N',
      p_eng_item_flag                 VARCHAR2 DEFAULT 'N',
      x_item_id            OUT NOCOPY NUMBER,
      x_RETURN_STATUS      OUT NOCOPY VARCHAR2,
      x_MSG_DATA           OUT NOCOPY VARCHAR2)
   IS
      /*******************************************************************************************************************************
         PROCEDURE : create_item

           REVISIONS:
           Ver        Date        Author                     Description
           ---------  ----------  ---------------    ----------------------------------------------------------------------------------
           1.0        01-MAR-2015   Shankar Vanga     TMS#20150126-00044  - Create Single Item Workflow Tool
                                                      Initial Version

      *********************************************************************************************************************************/


      l_item_table           EGO_Item_PUB.Item_Tbl_Type;
      x_item_table           EGO_Item_PUB.Item_Tbl_Type;
      l_catalog_group_id     NUMBER;
      l_org_code             VARCHAR2 (10) := 'MST';
      l_status_code          VARCHAR2 (10) := G_NEW_ITEM_STATUS;
      l_error_message_list   Error_handler.error_tbl_type;
      x_MSG_COUNT            NUMBER;
      x_error_code           NUMBER;
      l_icc_name             VARCHAR2 (50) := G_DRAFT_ICC;
      l_procedure            VARCHAR2 (100) := 'create_item';
      l_err_msg              VARCHAR2 (2000);
   BEGIN
      SELECT item_catalog_group_id
        INTO l_catalog_group_id
        FROM mtl_item_catalog_groups_b
       WHERE segment1 = l_icc_name;

      l_item_table (1).Transaction_Type := 'CREATE';
      l_item_table (1).Segment1 := p_item_number;
      l_item_table (1).Description := 'Draft Item ' || p_item_number;
      l_item_table (1).Organization_Code := l_org_code;
      l_item_table (1).Primary_Uom_Code := p_uom;
      l_item_table (1).Inventory_Item_Status_Code := l_status_code;
      l_item_table (1).item_catalog_group_id := l_catalog_group_id;
      L_ITEM_TABLE (1).ENG_ITEM_FLAG := P_ENG_ITEM_FLAG;
      L_ITEM_TABLE (1).HAZARDOUS_MATERIAL_FLAG := p_hazard_mat;
      EGO_ITEM_PUB.Process_Items (
         p_api_version      => 1.0,
         p_init_msg_list    => FND_API.g_TRUE,
         p_commit           => FND_API.g_FALSE,
         p_Item_Tbl         => l_item_table,
         x_Item_Tbl         => x_item_table,
         P_ROLE_GRANT_TBL   => EGO_ITEM_PUB.G_MISS_ROLE_GRANT_TBL,
         x_return_status    => x_return_status,
         x_msg_count        => x_msg_count);

      IF (x_return_status = 'S')
      THEN
         x_item_id := x_item_table (1).inventory_item_id;
      ELSE
         Error_Handler.Get_message_list (l_error_message_list);
         x_MSG_DATA := '';

         FOR i IN 1 .. l_error_message_list.COUNT
         LOOP
            x_MSG_DATA :=
               x_MSG_DATA || l_error_message_list (i).MESSAGE_TEXT || '. ';
         END LOOP;
      END IF;
   EXCEPTION
      WHEN OTHERS
      THEN
         l_err_msg :=
               l_err_msg
            || ' ...Error_Stack...'
            || DBMS_UTILITY.format_error_stack ()
            || ' Error_Backtrace...'
            || DBMS_UTILITY.format_error_backtrace ();

         -- Calling ERROR API
         xxcus_error_pkg.xxcus_error_main_api (
            p_called_from         => g_err_callfrom || '.' || l_procedure,
            p_ora_error_msg       => SUBSTR (l_err_msg, 1, 2000),
            p_error_desc          => SUBSTR (l_err_msg, 1, 240),
            p_distribution_list   => g_distro_list,
            p_module              => 'INV');
   END;

   ----------------------------------------------------------


   PROCEDURE process_change_request (
      p_item_number                   VARCHAR2,
      p_co_tyoe                       VARCHAR2,
      x_change_id          OUT NOCOPY NUMBER,
      x_change_name        OUT NOCOPY VARCHAR2,
      x_return_status      OUT NOCOPY VARCHAR2,
      x_msg_data           OUT NOCOPY VARCHAR2)
   IS
      /*******************************************************************************************************************************
         PROCEDURE : process_change_request

           REVISIONS:
           Ver        Date           Author                     Description
           ---------  ----------    ---------------   ----------------------------------------------------------------------------------
           1.0        01-MAR-2015   Shankar Vanga     TMS#20150126-00044  - Create Single Item Workflow Tool
                                                      Initial Version
           4.0        30-Jul-2015   P.Vamshidhar      TMS#20150728-00134 - Exception handling in Producre process_change_request

      *********************************************************************************************************************************/


      l_eco_name               VARCHAR2 (20) := NULL;
      l_co_type                VARCHAR2 (80) := p_co_tyoe; --'XXWC L1-L2 Create Item Request';
      --l_Change_Notice_Number   VARCHAR2(20) := l_eco_name;  --Auto generated
      l_org_code               VARCHAR2 (3) := 'MST';
      l_eco_rec                Eng_Eco_Pub.Eco_Rec_Type := Eng_Eco_Pub.g_miss_eco_rec;
      l_eco_revision_tbl       Eng_Eco_Pub.Eco_Revision_Tbl_Type
                                  := Eng_Eco_Pub.g_miss_eco_revision_tbl;
      l_revised_item_tbl       Eng_Eco_Pub.Revised_Item_Tbl_Type
                                  := Eng_Eco_Pub.g_miss_revised_item_tbl;
      l_rev_component_tbl      Bom_Bo_Pub.Rev_Component_Tbl_Type
                                  := Eng_Eco_Pub.g_miss_rev_component_tbl;
      l_sub_component_tbl      Bom_Bo_Pub.Sub_Component_Tbl_Type
                                  := Eng_Eco_Pub.g_miss_sub_component_tbl;
      l_ref_designator_tbl     Bom_Bo_Pub.Ref_Designator_Tbl_Type
                                  := Eng_Eco_Pub.g_miss_ref_designator_tbl;
      l_rev_operation_tbl      Bom_Rtg_Pub.Rev_Operation_Tbl_Type
                                  := Eng_Eco_Pub.g_miss_rev_operation_tbl;
      l_rev_op_resource_tbl    Bom_Rtg_Pub.Rev_Op_Resource_Tbl_Type
                                  := Eng_Eco_Pub.g_miss_rev_op_resource_tbl;
      l_rev_sub_resource_tbl   Bom_Rtg_Pub.Rev_Sub_Resource_Tbl_Type
                                  := Eng_Eco_Pub.g_miss_rev_sub_resource_tbl;

      -- API output variables
      x_eco_rec                Eng_Eco_Pub.Eco_Rec_Type
                                  := Eng_Eco_Pub.g_miss_eco_rec;
      x_eco_revision_tbl       Eng_Eco_Pub.Eco_Revision_Tbl_Type
                                  := Eng_Eco_Pub.g_miss_eco_revision_tbl;
      x_revised_item_tbl       Eng_Eco_Pub.Revised_Item_Tbl_Type
                                  := Eng_Eco_Pub.g_miss_revised_item_tbl;
      x_rev_component_tbl      Bom_Bo_Pub.Rev_Component_Tbl_Type
                                  := Eng_Eco_Pub.g_miss_rev_component_tbl;
      x_sub_component_tbl      Bom_Bo_Pub.Sub_Component_Tbl_Type
                                  := Eng_Eco_Pub.g_miss_sub_component_tbl;
      x_ref_designator_tbl     Bom_Bo_Pub.Ref_Designator_Tbl_Type
                                  := Eng_Eco_Pub.g_miss_ref_designator_tbl;
      x_rev_operation_tbl      Bom_Rtg_Pub.Rev_Operation_Tbl_Type
                                  := Eng_Eco_Pub.g_miss_rev_operation_tbl;
      x_rev_op_resource_tbl    Bom_Rtg_Pub.Rev_Op_Resource_Tbl_Type
                                  := Eng_Eco_Pub.g_miss_rev_op_resource_tbl;
      x_rev_sub_resource_tbl   Bom_Rtg_Pub.Rev_Sub_Resource_Tbl_Type
                                  := Eng_Eco_Pub.g_miss_rev_sub_resource_tbl;

      -- Other API variables for return status / error handling / debugging
      l_error_table            Error_Handler.Error_Tbl_Type;
      l_msg_count              NUMBER := 0;
      l_row_cnt                NUMBER := 1;


      l_count                  NUMBER;

      l_change_line_tbl        Eng_Eco_Pub.Change_Line_Tbl_Type
                                  := Eng_Eco_Pub.G_MISS_CHANGE_LINE_TBL;
      x_change_line_tbl        Eng_Eco_Pub.Change_Line_Tbl_Type
                                  := Eng_Eco_Pub.G_MISS_CHANGE_LINE_TBL;
      l_transaction_type       VARCHAR2 (30) := 'CREATE';

      l_procedure              VARCHAR2 (100) := 'process_change_request';
      l_err_msg                VARCHAR2 (2000);
      l_sec                    VARCHAR2 (150); -- Added variable in TMS#20150728-00134 by Vamshi
   BEGIN
      l_rev_component_tbl.delete;
      l_change_line_tbl.delete;
      -- Get the user_id

      Error_Handler.Initialize;
      x_return_status := NULL;

      l_eco_rec.eco_name := l_eco_name;
      l_eco_rec.organization_code := l_org_code;
      l_eco_rec.change_type_code := l_co_type;
      l_eco_rec.Change_Management_Type := 'Change Request';
      l_eco_rec.transaction_type := 'CREATE';
      l_eco_rec.plm_or_erp_change := 'PLM';


      l_change_line_tbl (l_row_cnt).eco_name := l_eco_name;
      l_change_line_tbl (l_row_cnt).Sequence_Number := 10;

      l_change_line_tbl (l_row_cnt).organization_code := l_org_code;
      l_change_line_tbl (l_row_cnt).Change_Type_Code := G_CHANGE_LINE_TYPE;

      l_change_line_tbl (l_row_cnt).name := p_item_number;
      l_change_line_tbl (l_row_cnt).Change_Management_Type :=
         l_eco_rec.Change_Management_Type;
      l_change_line_tbl (l_row_cnt).Object_Display_Name :=
         g_Object_Display_Name;
      l_change_line_tbl (l_row_cnt).Transaction_Type := l_transaction_type;


      ENG_ECO_PUB.Process_Eco (
         p_api_version_number     => 1.0,
         p_init_msg_list          => TRUE,
         x_return_status          => x_return_status,
         x_msg_count              => l_msg_count,
         p_bo_identifier          => 'ECO',
         p_eco_rec                => l_eco_rec,
         p_eco_revision_tbl       => ENG_ECO_PUB.G_MISS_ECO_REVISION_TBL,
         p_change_line_tbl        => l_change_line_tbl,
         p_revised_item_tbl       => ENG_ECO_PUB.G_MISS_REVISED_ITEM_TBL,
         p_rev_component_tbl      => ENG_ECO_PUB.G_MISS_REV_COMPONENT_TBL,
         p_ref_designator_tbl     => ENG_ECO_PUB.G_MISS_REF_DESIGNATOR_TBL,
         p_sub_component_tbl      => ENG_ECO_PUB.G_MISS_SUB_COMPONENT_TBL,
         p_rev_operation_tbl      => Bom_Rtg_Pub.G_MISS_REV_OPERATION_TBL --L1
                                                                         ,
         p_rev_op_resource_tbl    => Bom_Rtg_Pub.G_MISS_REV_OP_RESOURCE_TBL --L1
                                                                           ,
         p_rev_sub_resource_tbl   => Bom_Rtg_Pub.G_MISS_REV_SUB_RESOURCE_TBL --L1
                                                                            ,
         x_eco_rec                => x_eco_rec,
         x_eco_revision_tbl       => x_eco_revision_tbl,
         x_change_line_tbl        => x_change_line_tbl,
         x_revised_item_tbl       => x_revised_item_tbl,
         x_rev_component_tbl      => x_rev_component_tbl,
         x_ref_designator_tbl     => x_ref_designator_tbl,
         x_sub_component_tbl      => x_sub_component_tbl,
         x_rev_operation_tbl      => x_rev_operation_tbl,
         x_rev_op_resource_tbl    => x_rev_op_resource_tbl,
         x_rev_sub_resource_tbl   => x_rev_sub_resource_tbl,
         p_debug                  => 'N');

      IF (x_return_status = FND_API.G_RET_STS_SUCCESS)
      THEN
         x_change_name := x_eco_rec.Eco_Name;

         SELECT change_id
           INTO x_change_id
           FROM eng_engineering_changes
          WHERE change_notice = x_change_name;


         /*  -- commented below code in TMS#20150728-00134.
         UPDATE eng_change_subjects
            SET pk1_value =
                   (SELECT inventory_item_id
                      FROM mtl_system_items
                     WHERE segment1 = p_item_number AND organization_id = 222),
                pk2_value = 222
          WHERE change_id = x_change_id AND change_line_id IS NULL;
          */

         -- Replaced above code with below code block to handle duplicate draft item numbers in base table.
         -- TMS#20150728-00134

         l_sec := 'Item_id Update in Custom Table';

         BEGIN
            UPDATE eng_change_subjects
               SET pk1_value =
                      (SELECT inventory_item_id
                         FROM (  SELECT inventory_item_id
                                   FROM apps.mtl_system_items
                                  WHERE     segment1 = p_item_number
                                        AND organization_id = 222
                               ORDER BY inventory_item_id DESC)
                        WHERE ROWNUM < 2),
                   pk2_value = 222
             WHERE change_id = x_change_id AND change_line_id IS NULL;
         EXCEPTION
            WHEN OTHERS
            THEN
               g_err_callpoint := l_sec;

               l_err_msg :=
                     l_err_msg
                  || ' ...Error_Stack...'
                  || DBMS_UTILITY.format_error_stack ()
                  || ' Error_Backtrace...'
                  || DBMS_UTILITY.format_error_backtrace ();

               -- Calling ERROR API
               xxcus_error_pkg.xxcus_error_main_api (
                  p_called_from         => g_err_callfrom || '.' || l_procedure,
                  p_calling             => g_err_callpoint,
                  p_ora_error_msg       => SUBSTR (l_err_msg, 1, 2000),
                  p_error_desc          => SUBSTR (l_err_msg, 1, 240),
                  p_distribution_list   => g_distro_list,
                  p_module              => 'INV');
         END;
      END IF;

      DBMS_OUTPUT.put_line ('x_eco_rec.eco_name:' || x_eco_rec.eco_name);
      DBMS_OUTPUT.put_line (
         'x_eco_rec.org_code:' || x_eco_rec.organization_code);
      DBMS_OUTPUT.put_line (
         '=======================================================');
      DBMS_OUTPUT.put_line ('Return Status: ' || x_return_status);

      IF (x_return_status <> FND_API.G_RET_STS_SUCCESS)
      THEN
         --dbms_output.put_line('x_msg_count:' || l_msg_count);

         Error_Handler.GET_MESSAGE_LIST (x_message_list => l_error_table);
         DBMS_OUTPUT.put_line (
            'Error Message Count :' || l_error_table.COUNT);
         x_msg_data := '';

         FOR i IN 1 .. l_error_table.COUNT
         LOOP
            --dbms_output.put_line(to_char(i)||':'||l_error_table(i).entity_index||':'||l_error_table(i).table_name);
            DBMS_OUTPUT.put_line (
               TO_CHAR (i) || ':' || l_error_table (i).MESSAGE_TEXT);
            x_msg_data := x_msg_data || l_error_table (i).MESSAGE_TEXT || ' ';
         END LOOP;
      --ROLLBACK;
      END IF;

      DBMS_OUTPUT.put_line (
         '=======================================================');
   EXCEPTION
      WHEN OTHERS
      THEN
         g_err_callpoint := l_sec;    -- Added in TMS#20150728-00134 by Vamshi
         l_err_msg :=
               l_err_msg
            || ' ...Error_Stack...'
            || DBMS_UTILITY.format_error_stack ()
            || ' Error_Backtrace...'
            || DBMS_UTILITY.format_error_backtrace ();

         -- Calling ERROR API
         xxcus_error_pkg.xxcus_error_main_api (
            p_called_from         => g_err_callfrom || '.' || l_procedure,
            p_calling             => g_err_callpoint, -- Added in TMS#20150728-00134 by Vamshi
            p_ora_error_msg       => SUBSTR (l_err_msg, 1, 2000),
            p_error_desc          => SUBSTR (l_err_msg, 1, 240),
            p_distribution_list   => g_distro_list,
            p_module              => 'INV');
   END;

   ---------------------------------------------------------


   PROCEDURE CREATE_CHANGE_ORDER (
      p_item_level                     VARCHAR2,
      p_new_item                       VARCHAR2 DEFAULT 'N',
      p_co_description                 VARCHAR2,
      p_revised_item                   VARCHAR2,
      P_UOM_CODE                       VARCHAR2 DEFAULT NULL,
      p_hazard_mat                     VARCHAR2 DEFAULT 'N',
      p_reson_code                     VARCHAR2,
      p_vendor_number                  VARCHAR2, -- -- Added by Vamshi in V2.0  TMS#20150519-00091
      x_change_id           OUT NOCOPY NUMBER,
      x_item_id             OUT NOCOPY NUMBER,
      x_change_name         OUT NOCOPY VARCHAR2,
      x_return_status       OUT NOCOPY VARCHAR2,
      x_error_msg           OUT NOCOPY VARCHAR2)
   IS
      /*******************************************************************************************************************************
         PROCEDURE : CREATE_CHANGE_ORDER

           REVISIONS:
           Ver        Date        Author                     Description
           ---------  ----------  ---------------    ----------------------------------------------------------------------------------
           1.0        01-MAR-2015   Shankar Vanga     TMS#20150126-00044  - Create Single Item Workflow Tool
                                                      Initial Version

      *********************************************************************************************************************************/

      l_eco_name               VARCHAR2 (20) := NULL;
      l_co_type                VARCHAR2 (80);
      --l_Change_Notice_Number   VARCHAR2(20) := l_eco_name;  --Auto generated
      l_org_code               VARCHAR2 (3) := 'MST';
      l_rev_item_number        VARCHAR2 (100) := p_revised_item;
      l_eco_rec                Eng_Eco_Pub.Eco_Rec_Type
                                  := Eng_Eco_Pub.g_miss_eco_rec;
      l_eco_revision_tbl       Eng_Eco_Pub.Eco_Revision_Tbl_Type
                                  := Eng_Eco_Pub.g_miss_eco_revision_tbl;
      l_revised_item_tbl       Eng_Eco_Pub.Revised_Item_Tbl_Type
                                  := Eng_Eco_Pub.g_miss_revised_item_tbl;
      l_rev_component_tbl      Bom_Bo_Pub.Rev_Component_Tbl_Type
                                  := Eng_Eco_Pub.g_miss_rev_component_tbl;
      l_sub_component_tbl      Bom_Bo_Pub.Sub_Component_Tbl_Type
                                  := Eng_Eco_Pub.g_miss_sub_component_tbl;
      l_ref_designator_tbl     Bom_Bo_Pub.Ref_Designator_Tbl_Type
                                  := Eng_Eco_Pub.g_miss_ref_designator_tbl;
      l_rev_operation_tbl      Bom_Rtg_Pub.Rev_Operation_Tbl_Type
                                  := Eng_Eco_Pub.g_miss_rev_operation_tbl;
      l_rev_op_resource_tbl    Bom_Rtg_Pub.Rev_Op_Resource_Tbl_Type
                                  := Eng_Eco_Pub.g_miss_rev_op_resource_tbl;
      l_rev_sub_resource_tbl   Bom_Rtg_Pub.Rev_Sub_Resource_Tbl_Type
                                  := Eng_Eco_Pub.g_miss_rev_sub_resource_tbl;

      -- API output variables
      x_eco_rec                Eng_Eco_Pub.Eco_Rec_Type
                                  := Eng_Eco_Pub.g_miss_eco_rec;
      x_eco_revision_tbl       Eng_Eco_Pub.Eco_Revision_Tbl_Type
                                  := Eng_Eco_Pub.g_miss_eco_revision_tbl;
      x_revised_item_tbl       Eng_Eco_Pub.Revised_Item_Tbl_Type
                                  := Eng_Eco_Pub.g_miss_revised_item_tbl;
      x_rev_component_tbl      Bom_Bo_Pub.Rev_Component_Tbl_Type
                                  := Eng_Eco_Pub.g_miss_rev_component_tbl;
      x_sub_component_tbl      Bom_Bo_Pub.Sub_Component_Tbl_Type
                                  := Eng_Eco_Pub.g_miss_sub_component_tbl;
      x_ref_designator_tbl     Bom_Bo_Pub.Ref_Designator_Tbl_Type
                                  := Eng_Eco_Pub.g_miss_ref_designator_tbl;
      x_rev_operation_tbl      Bom_Rtg_Pub.Rev_Operation_Tbl_Type
                                  := Eng_Eco_Pub.g_miss_rev_operation_tbl;
      x_rev_op_resource_tbl    Bom_Rtg_Pub.Rev_Op_Resource_Tbl_Type
                                  := Eng_Eco_Pub.g_miss_rev_op_resource_tbl;
      x_rev_sub_resource_tbl   Bom_Rtg_Pub.Rev_Sub_Resource_Tbl_Type
                                  := Eng_Eco_Pub.g_miss_rev_sub_resource_tbl;

      -- Other API variables for return status / error handling / debugging
      l_error_table            Error_Handler.Error_Tbl_Type;
      l_msg_count              NUMBER := 0;
      l_row_cnt                NUMBER := 1;

      l_procedure              VARCHAR2 (100) := 'CREATE_CHANGE_ORDER';
      l_err_msg                VARCHAR2 (2000);


      CURSOR check_co_exists_for_new_item (
         cp_item_number    VARCHAR2)
      IS
         SELECT EC.CHANGE_ID, EC.CHANGE_NOTICE, msi.inventory_item_id
           FROM apps.eng_change_subjects ecs,
                apps.mtl_system_items msi,
                apps.mtl_item_catalog_groups_b icc,
                apps.eng_change_order_types_vl ctypes,
                ENG_ENGINEERING_CHANGES EC
          WHERE     MSI.INVENTORY_ITEM_ID = ECS.PK1_VALUE
                AND ECS.CHANGE_LINE_ID IS NULL
                AND ECS.PK2_VALUE = '222'
                AND MSI.ORGANIZATION_ID = 222
                AND EC.status_code NOT IN (11, 8)
                AND msi.inventory_item_status_code = G_NEW_ITEM_STATUS
                AND msi.item_catalog_group_id = icc.item_catalog_group_id
                AND icc.segment1 = G_DRAFT_ICC
                AND ctypes.change_order_type_id = ec.change_order_type_id
                AND ec.change_id = ecs.change_id
                AND msi.segment1 = cp_item_number;

      l_count                  NUMBER;
   BEGIN
      IF (p_new_item != 'Y')
      THEN
         l_count := 0;

         FOR rec IN check_co_exists_for_new_item (p_revised_item)
         LOOP
            x_change_id := rec.change_id;
            X_CHANGE_NAME := REC.CHANGE_NOTICE;
            x_item_id := rec.inventory_item_id;
            l_count := l_count + 1;
         END LOOP;

         IF (l_count > 0)
         THEN
            x_RETURN_STATUS := 'S';
            XXWC_EGO_VENDOR_INSERT (fnd_profile.VALUE ('LOGIN_ID'),
                                    p_vendor_number,
                                    X_CHANGE_NAME,
                                    'U1'); --  Added by Vamshi in V2.0  TMS#20150519-00091


            RETURN;
         END IF;
      END IF;

      IF (p_new_item = 'Y')
      THEN
         create_item (P_ITEM_NUMBER     => P_REVISED_ITEM,
                      P_UOM             => P_UOM_CODE,
                      p_hazard_mat      => p_hazard_mat,
                      p_eng_item_flag   => 'N',
                      x_item_id         => x_item_id,
                      x_RETURN_STATUS   => x_RETURN_STATUS,
                      x_MSG_DATA        => x_error_msg);
      END IF;

      IF (x_RETURN_STATUS != 'S')
      THEN
         RETURN;
      END IF;


      IF (p_new_item = 'N')
      THEN
         XXWC_EGO_VENDOR_INSERT (fnd_profile.VALUE ('LOGIN_ID'),
                                 p_vendor_number,
                                 NULL,
                                 'U'); --  Added by Vamshi in V2.0  TMS#20150519-00091

         IF (p_item_level = 'L1' OR p_item_level IS NULL)
         THEN
            l_co_type := 'XXWC L1 Item Update Request';
         ELSIF p_item_level = 'L2'
         THEN
            l_co_type := 'XXWC L2 Item Update Request';
         ELSE
            l_co_type := 'XXWC L3 Item Update Request';
         END IF;
      ELSE
         XXWC_EGO_VENDOR_INSERT (fnd_profile.VALUE ('LOGIN_ID'),
                                 p_vendor_number,
                                 NULL,
                                 'I'); --  Added by Vamshi in V2.0  TMS#20150519-00091

         IF (   p_item_level = 'L1'
             OR p_item_level IS NULL
             OR p_item_level = 'L2')
         THEN
            l_co_type := 'XXWC L1-L2 Create Item Request'; --XXWC L1-L2 Create Item Request;
         --'XXWC L1-L2 Item Creation';
         ELSE
            l_co_type := 'XXWC L3 Item Creation';     ---XXWC L3 Item Creation
         END IF;
      END IF;


      IF (p_new_item = 'Y')
      THEN
         process_change_request (p_item_number     => p_revised_item,
                                 p_co_tyoe         => l_co_type,
                                 x_change_id       => x_change_id,
                                 x_change_name     => x_change_name,
                                 x_return_status   => x_return_status,
                                 x_msg_data        => x_error_msg);
         RETURN;
      END IF;

      DBMS_OUTPUT.put_line ('l_co_type:' || l_co_type);
      l_rev_component_tbl.delete;
      l_revised_item_tbl.delete;
      -- Get the user_id

      Error_Handler.Initialize;
      x_return_status := NULL;

      l_eco_rec.eco_name := l_eco_name;
      --l_eco_rec.Change_Notice_Number := l_Change_Notice_Number;
      l_eco_rec.organization_code := l_org_code;
      l_eco_rec.change_type_code := l_co_type;
      l_eco_rec.priority_code := NULL;                             --'Medium';
      l_eco_rec.reason_code := p_reson_code;
      l_eco_rec.Approval_Status_Name := NULL; --'Approved';    -- will default to Not submitted for Approval
      --l_eco_rec.Status_Name         :=  'Open';
      l_eco_rec.description := p_co_description;
      l_eco_rec.transaction_type := 'CREATE';
      l_eco_rec.plm_or_erp_change := 'PLM';                    --'ERP'or 'PLM'
      l_eco_rec.requestor := fnd_global.user_name;              --p_requestor;

      IF (fnd_global.user_name IS NULL)
      THEN
         l_eco_rec.requestor := 'SYSADMIN';
      END IF;


      l_revised_item_tbl (l_row_cnt).eco_name := l_eco_name;
      l_revised_item_tbl (l_row_cnt).organization_code := l_org_code;
      l_revised_item_tbl (l_row_cnt).revised_item_name := l_rev_item_number;
      l_revised_item_tbl (l_row_cnt).new_revised_item_revision := NULL;
      l_revised_item_tbl (l_row_cnt).start_effective_date := SYSDATE;  --null;
      l_revised_item_tbl (l_row_cnt).new_effective_date := NULL;
      l_revised_item_tbl (l_row_cnt).status_type := 1;               -- 'Open'
      --l_revised_item_tbl(l_row_cnt).change_description := null;
      l_revised_item_tbl (l_row_cnt).transaction_type := 'CREATE'; -- transaction type : CREATE / UPDATE

      ENG_ECO_PUB.Process_Eco (
         p_api_version_number     => 1.0,
         p_init_msg_list          => TRUE,
         x_return_status          => x_return_status,
         x_msg_count              => l_msg_count,
         p_bo_identifier          => 'ECO',
         p_eco_rec                => l_eco_rec,
         p_eco_revision_tbl       => l_eco_revision_tbl,
         p_revised_item_tbl       => l_revised_item_tbl,
         p_rev_component_tbl      => l_rev_component_tbl,
         p_ref_designator_tbl     => l_ref_designator_tbl,
         p_sub_component_tbl      => l_sub_component_tbl,
         p_rev_operation_tbl      => l_rev_operation_tbl,
         p_rev_op_resource_tbl    => l_rev_op_resource_tbl,
         p_rev_sub_resource_tbl   => l_rev_sub_resource_tbl,
         x_eco_rec                => x_eco_rec,
         x_eco_revision_tbl       => x_eco_revision_tbl,
         x_revised_item_tbl       => x_revised_item_tbl,
         x_rev_component_tbl      => x_rev_component_tbl,
         x_ref_designator_tbl     => x_ref_designator_tbl,
         x_sub_component_tbl      => x_sub_component_tbl,
         x_rev_operation_tbl      => x_rev_operation_tbl,
         x_rev_op_resource_tbl    => x_rev_op_resource_tbl,
         x_rev_sub_resource_tbl   => x_rev_sub_resource_tbl,
         p_debug                  => 'N');

      IF (x_return_status = FND_API.G_RET_STS_SUCCESS)
      THEN
         SELECT change_id
           INTO x_change_id
           FROM eng_engineering_changes
          WHERE change_notice = x_eco_rec.eco_name;

         --XXWC_EGO_VENDOR_INSERT(x_eco_rec.eco_name,p_vendor_number); ---- Added by Vamshi in V2.0  TMS#20150519-00091

         x_change_name := x_eco_rec.eco_name;
      END IF;

      DBMS_OUTPUT.put_line ('x_eco_rec.eco_name:' || x_eco_rec.eco_name);
      DBMS_OUTPUT.put_line (
         'x_eco_rec.org_code:' || x_eco_rec.organization_code);
      DBMS_OUTPUT.put_line (
         '=======================================================');
      DBMS_OUTPUT.put_line ('Return Status: ' || x_return_status);

      IF (x_return_status <> FND_API.G_RET_STS_SUCCESS)
      THEN
         --dbms_output.put_line('x_msg_count:' || l_msg_count);

         Error_Handler.GET_MESSAGE_LIST (x_message_list => l_error_table);
         DBMS_OUTPUT.put_line (
            'Error Message Count :' || l_error_table.COUNT);
         x_error_msg := '';

         FOR i IN 1 .. l_error_table.COUNT
         LOOP
            --dbms_output.put_line(to_char(i)||':'||l_error_table(i).entity_index||':'||l_error_table(i).table_name);
            DBMS_OUTPUT.put_line (
               TO_CHAR (i) || ':' || l_error_table (i).MESSAGE_TEXT);
            x_error_msg :=
               x_error_msg || l_error_table (i).MESSAGE_TEXT || ' ';
         END LOOP;
      --ROLLBACK;
      END IF;

      DBMS_OUTPUT.put_line (
         '=======================================================');
   EXCEPTION
      WHEN OTHERS
      THEN
         l_err_msg :=
               l_err_msg
            || ' ...Error_Stack...'
            || DBMS_UTILITY.format_error_stack ()
            || ' Error_Backtrace...'
            || DBMS_UTILITY.format_error_backtrace ();

         -- Calling ERROR API
         xxcus_error_pkg.xxcus_error_main_api (
            p_called_from         => g_err_callfrom || '.' || l_procedure,
            p_ora_error_msg       => SUBSTR (l_err_msg, 1, 2000),
            p_error_desc          => SUBSTR (l_err_msg, 1, 240),
            p_distribution_list   => g_distro_list,
            p_module              => 'INV');

         DBMS_OUTPUT.put_line ('Exception Occured :');
         DBMS_OUTPUT.put_line (SQLCODE || ':' || SQLERRM);
         DBMS_OUTPUT.put_line (
            '=======================================================');
         RAISE;
   END;

   ------------------------------------------------------



   -------------------------------------------------
   FUNCTION delete_item (p_item_number VARCHAR2)
      RETURN NUMBER
   IS
      /*******************************************************************************************************************************
         FUNCTION : delete_item

           REVISIONS:
           Ver        Date        Author                     Description
           ---------  ----------  ---------------    ----------------------------------------------------------------------------------
           1.0        01-MAR-2015   Shankar Vanga     TMS#20150126-00044  - Create Single Item Workflow Tool
                                                      Initial Version

      *********************************************************************************************************************************/

      l_org_id                     NUMBER;
      l_delete_group_name          VARCHAR2 (10) := 'XXWCITM2';
      l_delete_common_bill_flag    NUMBER := 2;
      stmt_num                     NUMBER;
      var_text                     VARCHAR2 (1000);
      l_process_flag               NUMBER; -- =1 FOR PROCESSED, =2 FOR NOT PROCESSED
      l_eng_flag                   NUMBER := 1; -- =1 FOR NON-ENG ITEMS, =2 FOR ENG ITEMS
      commit_flag                  NUMBER; -- =1 IF ON COMMIT FROM FORM, =2 IF FROM SEARCH REGION
      l_engineering_flag           NUMBER := 2;
      l_delete_org_type            NUMBER := 1;
      l_del_stat_type              NUMBER := 1;
      l_prior_commit_flag          NUMBER := 1;
      l_prior_process_flag         NUMBER := 2;
      ERRBUF                       VARCHAR2 (2000);
      RETCODE                      VARCHAR2 (200);
      l_delete_group_sequence_id   NUMBER := NULL;
      L_DELETE_GROUP_ID            NUMBER;
      l_action_type                VARCHAR2 (1);
      l_delete_type                VARCHAR2 (1);
      l_row_count                  NUMBER;
      x_request_id                 NUMBER;

      l_procedure                  VARCHAR2 (100) := 'delete_item';
      l_err_msg                    VARCHAR2 (2000);


      CURSOR check_delete_group_exists (
         cp_delete_group_name    VARCHAR2,
         cp_organization_id      NUMBER)
      IS
         SELECT delete_group_sequence_id
           FROM BOM.BOM_DELETE_GROUPS
          WHERE     delete_group_name = cp_delete_group_name
                AND organization_id = cp_organization_id;
   BEGIN
      l_org_id := 222;
      l_delete_group_sequence_id := NULL;

      OPEN check_delete_group_exists (l_delete_group_name, l_org_id);

      FETCH check_delete_group_exists INTO l_delete_group_sequence_id;

      IF (check_delete_group_exists%NOTFOUND)
      THEN
         l_delete_group_sequence_id := NULL;
      END IF;

      CLOSE check_delete_group_exists;

      l_action_type := '2';
      l_delete_type := '1';
      l_engineering_flag := 2;

      IF (l_delete_group_sequence_id IS NULL)
      THEN
         SELECT BOM_DELETE_GROUPS_s.NEXTVAL
           INTO l_delete_group_sequence_id
           FROM DUAL;

         INSERT INTO BOM.BOM_DELETE_GROUPS (delete_group_sequence_id,
                                            delete_group_name,
                                            organization_id,
                                            delete_type,
                                            action_type,
                                            date_last_submitted,
                                            engineering_flag,
                                            delete_common_bill_flag        --2
                                                                   ,
                                            delete_org_type                --1
                                                           ,
                                            last_update_date,
                                            last_updated_by,
                                            creation_date,
                                            created_by,
                                            last_update_login)
              VALUES (l_delete_group_sequence_id,
                      l_delete_group_name,
                      l_org_id,
                      l_delete_type,
                      l_action_type,
                      SYSDATE,
                      l_engineering_flag,
                      l_delete_common_bill_flag,
                      l_delete_org_type,
                      SYSDATE,
                      fnd_global.user_id,
                      SYSDATE,
                      fnd_global.user_id,
                      fnd_global.login_id);

         COMMIT;
      END IF;

      INSERT INTO BOM_DELETE_ENTITIES (DELETE_ENTITY_SEQUENCE_ID,
                                       DELETE_GROUP_SEQUENCE_ID,
                                       DELETE_ENTITY_TYPE,
                                       INVENTORY_ITEM_ID,
                                       ORGANIZATION_ID,
                                       ITEM_DESCRIPTION,
                                       ITEM_CONCAT_SEGMENTS,
                                       DELETE_STATUS_TYPE,
                                       PRIOR_PROCESS_FLAG,
                                       PRIOR_COMMIT_FLAG,
                                       LAST_UPDATE_DATE,
                                       LAST_UPDATED_BY,
                                       CREATION_DATE,
                                       CREATED_BY,
                                       LAST_UPDATE_LOGIN)
         SELECT BOM_DELETE_ENTITIES_S.NEXTVAL,
                l_delete_group_sequence_id,
                1,
                MSI.INVENTORY_ITEM_ID,
                MSI.ORGANIZATION_ID,
                MSI.DESCRIPTION,
                MSI.segment1,
                l_del_stat_type,
                l_prior_process_flag,
                l_prior_commit_flag,
                SYSDATE,
                apps.FND_GLOBAL.user_id,
                SYSDATE,
                apps.FND_GLOBAL.user_id,
                apps.FND_GLOBAL.LOGIN_ID
           FROM apps.MTL_SYSTEM_ITEMS_B MSI
          WHERE organization_id = l_org_id AND segment1 = p_item_number;

      l_row_count := SQL%ROWCOUNT;
      COMMIT;
      bom_delete_groups_api.delete_groups (
         ERRBUF,
         RETCODE,
         delete_group_id   => l_delete_group_sequence_id,
         action_type       => l_action_type,
         delete_type       => l_delete_type);

      COMMIT;
      RETURN x_request_id;
   EXCEPTION
      WHEN OTHERS
      THEN
         DBMS_OUTPUT.put_line ('Error ...' || SQLERRM);

         l_err_msg :=
               l_err_msg
            || ' ...Error_Stack...'
            || DBMS_UTILITY.format_error_stack ()
            || ' Error_Backtrace...'
            || DBMS_UTILITY.format_error_backtrace ();

         -- Calling ERROR API
         xxcus_error_pkg.xxcus_error_main_api (
            p_called_from         => g_err_callfrom || '.' || l_procedure,
            p_ora_error_msg       => SUBSTR (l_err_msg, 1, 2000),
            p_error_desc          => SUBSTR (l_err_msg, 1, 240),
            p_distribution_list   => g_distro_list,
            p_module              => 'INV');
   END;

   --------------------------------------------------------------------------
   PROCEDURE replace_item_prg (errbuf                 OUT NOCOPY VARCHAR2,
                               retcode                OUT NOCOPY VARCHAR2,
                               p_old_item_number                 VARCHAR2,
                               p_new_item_number                 VARCHAR2,
                               p_new_uom                         VARCHAR2,
                               p_item_id                         NUMBER)
   IS
      /*******************************************************************************************************************************
         procedure : replace_item_prg

           REVISIONS:
           Ver        Date        Author                     Description
           ---------  ----------  ---------------    ----------------------------------------------------------------------------------
           1.0        01-MAR-2015   Shankar Vanga     TMS#20150126-00044  - Create Single Item Workflow Tool
                                                      Initial Version

        -- 3.0     28-JUL-2015      P.vamshidhar      TMS#20150728-00035 - added updates for full and thumbnail images while replacing
                                                      item number.


      *********************************************************************************************************************************/


      CURSOR get_item_info (
         cp_item_id    NUMBER)
      IS
         --new one
         SELECT ATT.C_EXT_ATTR6 ITEM_LEVEL,
                EC.CHANGE_ID,
                EC.CHANGE_NOTICE ECO_NAME,
                EC.DESCRIPTION CO_DESC,
                item.HAZARDOUS_MATERIAL_FLAG
           FROM XXWC_MTL_SY_ITEMS_CHG_B item,
                eng_engineering_changes ec,
                xxwc.xxwc_chg_ego_item_attrs att
          WHERE     item.inventory_item_id = cp_item_id
                AND item.change_id = ec.change_id
                AND att.change_id = ec.change_id
                AND att.inventory_item_id = item.inventory_item_id
                AND item.organization_id = att.organization_id
                AND item.organization_id = 222
                AND EXISTS
                       (SELECT '1'
                          FROM apps.EGO_ATTR_GROUPS_V grp
                         WHERE     attr_group_name IN ('XXWC_INTERNAL_ATTR_AG')
                               AND grp.attr_group_id = att.attr_group_id);


      l_HAZARDOUS_MATERIAL_FLAG   VARCHAR2 (10);
      l_item_level                VARCHAR2 (30);
      l_co_desc                   VARCHAR2 (2000);
      x_change_name               VARCHAR2 (30);
      x_item_id                   NUMBER;
      x_change_id                 NUMBER;
      x_return_status             VARCHAR2 (1);
      x_error_msg                 VARCHAR2 (255);
      l_change_id                 NUMBER;
      x_MSG_DATA                  VARCHAR2 (2000);
      l_eco_name                  VARCHAR2 (30);
      x_request_id                NUMBER;

      l_procedure                 VARCHAR2 (100) := 'replace_item_prg';
      l_err_msg                   VARCHAR2 (2000);
   BEGIN
      FOR rec IN get_item_info (p_item_id)
      LOOP
         l_item_level := rec.item_level;
         l_eco_name := rec.eco_name;
         l_change_id := rec.change_id;
         L_CO_DESC := REC.CO_DESC;
         l_HAZARDOUS_MATERIAL_FLAG := rec.HAZARDOUS_MATERIAL_FLAG;
      END LOOP;

      x_change_id := l_change_id;
      -- Delete Item
      x_request_id := delete_item (p_item_number => p_old_item_number);
      COMMIT;
      create_item (P_ITEM_NUMBER     => P_NEW_ITEM_NUMBER,
                   P_UOM             => P_NEW_UOM,
                   p_hazard_mat      => l_HAZARDOUS_MATERIAL_FLAG,
                   X_ITEM_ID         => X_ITEM_ID,
                   X_RETURN_STATUS   => X_RETURN_STATUS,
                   x_MSG_DATA        => x_MSG_DATA);

      IF (x_item_id IS NULL OR x_item_id <= 0)
      THEN
         RETURN;
      END IF;


      UPDATE eng_change_subjects
         SET pk1_value = x_item_id
       WHERE change_id = l_change_id AND change_line_id IS NULL;

      UPDATE eng_change_lines_tl
         SET name = p_new_item_number
       WHERE     change_line_id = (SELECT change_line_id
                                     FROM eng_change_lines
                                    WHERE change_id = l_change_id)
             AND language = USERENV ('LANG');

      UPDATE xxwc_chg_mtl_cross_ref
         SET inventory_item_id = x_item_id
       WHERE inventory_item_id = p_item_id;

      UPDATE xxwc.xxwc_chg_ego_item_attrs
         SET inventory_item_id = x_item_id
       WHERE inventory_item_id = p_item_id;

      -- Added below 2 update commands by Vamshi on 28-Jul-2015 @TMS#20150728-00035

      UPDATE xxwc.xxwc_chg_ego_item_attrs
         SET c_ext_attr15 = 'f_' || p_new_item_number || '.jpg'
       WHERE     c_ext_attr15 IS NOT NULL
             AND inventory_item_id = x_item_id
             AND change_id = x_change_id;


      UPDATE xxwc.xxwc_chg_ego_item_attrs
         SET c_ext_attr20 = 't_' || p_new_item_number || '.jpg'
       WHERE     c_ext_attr20 IS NOT NULL
             AND inventory_item_id = x_item_id
             AND change_id = x_change_id;


      UPDATE fnd_attached_documents
         SET pk2_value = TO_CHAR (x_item_id)
       WHERE     pk2_value = TO_CHAR (p_item_id)
             AND pk1_value = TO_CHAR (222)
             AND entity_name = 'XXWC_MTL_SY_ITEMS_CHG_B';

      UPDATE XXWC_MTL_SY_ITEMS_CHG_B
         SET inventory_item_id = x_item_id
       WHERE inventory_item_id = p_item_id;

      COMMIT;
   EXCEPTION
      WHEN OTHERS
      THEN
         l_err_msg :=
               l_err_msg
            || ' ...Error_Stack...'
            || DBMS_UTILITY.format_error_stack ()
            || ' Error_Backtrace...'
            || DBMS_UTILITY.format_error_backtrace ();

         -- Calling ERROR API
         xxcus_error_pkg.xxcus_error_main_api (
            p_called_from         => g_err_callfrom || '.' || l_procedure,
            p_ora_error_msg       => SUBSTR (l_err_msg, 1, 2000),
            p_error_desc          => SUBSTR (l_err_msg, 1, 240),
            p_distribution_list   => g_distro_list,
            p_module              => 'INV');

         errbuf := SQLERRM;
         retcode := 2;
   END;

   ------------------------------------------------------------------------------



   PROCEDURE REPLACE_ITEM (p_old_item_number                 VARCHAR2,
                           p_new_item_number                 VARCHAR2,
                           p_new_uom                         VARCHAR2,
                           p_item_id                         NUMBER,
                           x_ret_mess             OUT NOCOPY VARCHAR2,
                           x_request_id           OUT NOCOPY NUMBER)
   IS
      /*******************************************************************************************************************************
         procedure : REPLACE_ITEM

           REVISIONS:
           Ver        Date            Author                     Description
           ---------  -----------  ---------------    ----------------------------------------------------------------------------------
           1.0        01-MAR-2015   Shankar Vanga     TMS#20150126-00044  - Create Single Item Workflow Tool
                                                      Initial Version
           4.1        12-Oct-2015   P.vamshidhar      TMS#20150917-00149 - PCAM add validation for item check
                                                      during WF approval -  Initial Version
      *********************************************************************************************************************************/


      errbuf         VARCHAR2 (2000);
      retcode        VARCHAR2 (200);

      l_procedure    VARCHAR2 (100) := 'REPLACE_ITEM';
      l_err_msg      VARCHAR2 (2000);
      l_errbuf       VARCHAR2 (2000);
      l_retcode      VARCHAR2 (2);
      -- Added below variables in TMS#20150917-00149 by Vamshi in Rev 4.1
      lvc_validate   VARCHAR2 (1);
      lvc_err_mess   VARCHAR2 (1000);
   BEGIN
      --    Commented below code by Vamshi in TMS#20150917-00149 by Vamshi - Rev 4.1
      --         replace_item_prg (l_errbuf,
      --                           l_retcode,
      --                           p_old_item_number,
      --                           p_new_item_number,
      --                           p_new_uom,
      --                           p_item_id);
      --         x_request_id := 44444444;

      -- Added below code in TMS#20150917-00149 by Vamshi in Rev 4.1
      lvc_validate := Item_Number_validate (p_new_item_number);

      IF lvc_validate = 'N'
      THEN
         replace_item_prg (l_errbuf,
                           l_retcode,
                           p_old_item_number,
                           p_new_item_number,
                           p_new_uom,
                           p_item_id);
         x_request_id := 44444444;
      ELSE
         BEGIN
            Temp_table_Item_update (p_item_id, p_old_item_number);

            SELECT m.MESSAGE_TEXT
              INTO lvc_err_mess
              FROM FND_NEW_MESSAGES M
             WHERE     UPPER (m.MESSAGE_TEXT) LIKE 'XXWC_ITEM_ALREADY_EXISTS'
                   AND m.language_code = 'US';

            x_ret_mess := lvc_err_mess;
         EXCEPTION
            WHEN OTHERS
            THEN
               x_ret_mess := 'Item with provided number is already exists.';
         END;
      END IF;

      /*
         x_request_id :=
            fnd_request.submit_request (application   => 'XXWC',
                                        program       => 'XXWC_PIM_REPLACE_ITEM',
                                        argument1     => p_old_item_number,
                                        argument2     => p_new_item_number,
                                        argument3     => p_new_uom,
                                        argument4     => TO_CHAR (p_item_id));
   */
      DBMS_OUTPUT.put_line (
            'Submitted request for XXWC_PIM_REPLACE_ITEM, request Id='
         || x_request_id
         || '. errbuf ='
         || errbuf);
      COMMIT;
   EXCEPTION
      WHEN OTHERS
      THEN
         l_err_msg :=
               l_err_msg
            || ' ...Error_Stack...'
            || DBMS_UTILITY.format_error_stack ()
            || ' Error_Backtrace...'
            || DBMS_UTILITY.format_error_backtrace ();

         -- Calling ERROR API
         xxcus_error_pkg.xxcus_error_main_api (
            p_called_from         => g_err_callfrom || '.' || l_procedure,
            p_ora_error_msg       => SUBSTR (SQLERRM, 1, 2000),
            p_error_desc          => SUBSTR (l_err_msg, 1, 240),
            p_distribution_list   => g_distro_list,
            p_module              => 'INV');
   END;

   --------------------------------------added by Sumit for deleting constraint--------
   PROCEDURE xxwc_delete_constraint (p_table_name         VARCHAR2,
                                     p_constraint_name    VARCHAR2)
   IS
      CURSOR c_cons
      IS
         SELECT table_name, constraint_name
           FROM dba_constraints
          WHERE     table_name = UPPER (p_table_name)
                AND constraint_name =
                       NVL (UPPER (p_constraint_name), constraint_name);

      l_table_owner   VARCHAR2 (50);

      l_procedure     VARCHAR2 (100) := 'xxwc_delete_constraint';
      l_err_msg       VARCHAR2 (2000);
   BEGIN
      DBMS_OUTPUT.put_line (
         '-----------------------------------------------------------------');
      DBMS_OUTPUT.put_line (
         'Deleting constraint of a table: ' || p_table_name);

      BEGIN
         SELECT owner
           INTO l_table_owner
           FROM dba_tables
          WHERE table_name = UPPER (p_table_name);
      EXCEPTION
         WHEN OTHERS
         THEN
            l_table_owner := NULL;
            DBMS_OUTPUT.put_line (
               'Error in derving table owner: ' || SQLERRM);
      END;

      IF l_table_owner = 'XXWC'
      THEN
         FOR rec_cons IN c_cons
         LOOP
            DBMS_OUTPUT.put_line (
               'Deleting constraint:  ' || rec_cons.constraint_name);

            BEGIN
               EXECUTE IMMEDIATE
                     'alter table '
                  || l_table_owner
                  || '.'
                  || rec_cons.table_name
                  || ' drop constraint '
                  || rec_cons.constraint_name;
            EXCEPTION
               WHEN OTHERS
               THEN
                  DBMS_OUTPUT.put_line (
                     'Error in dropping constraint: ' || SQLERRM);
            END;
         END LOOP;
      END IF;
   EXCEPTION
      WHEN OTHERS
      THEN
         l_err_msg :=
               l_err_msg
            || ' ...Error_Stack...'
            || DBMS_UTILITY.format_error_stack ()
            || ' Error_Backtrace...'
            || DBMS_UTILITY.format_error_backtrace ();

         -- Calling ERROR API
         xxcus_error_pkg.xxcus_error_main_api (
            p_called_from         => g_err_callfrom || '.' || l_procedure,
            p_ora_error_msg       => SUBSTR (l_err_msg, 1, 2000),
            p_error_desc          => SUBSTR (l_err_msg, 1, 240),
            p_distribution_list   => g_distro_list,
            p_module              => 'INV');
   END;

   --------------------------------------------------------------------------------------------------------

   /*******************************************************************************************************************************
     procedure : REPLACE_ITEM
     REVISIONS:
     Ver        Date        Author                     Description
     ---------  ----------  ---------------    ----------------------------------------------------------------------------------
    1.0        01-JUN-2015  P.Vamshidhar       TMS#20150519-00091  - Enhancements.
                                               Initial Version
   *********************************************************************************************************************************/

   PROCEDURE XXWC_EGO_VENDOR_INSERT (p_login_id        IN VARCHAR2,
                                     p_vendor_number   IN VARCHAR2,
                                     p_change_name     IN VARCHAR2,
                                     p_status          IN VARCHAR2)
   IS
      ln_count      NUMBER;
      l_procedure   VARCHAR2 (100);
      PRAGMA AUTONOMOUS_TRANSACTION;
   BEGIN
      l_procedure := 'XXWC_EGO_VENDOR_INSERT';

      SELECT COUNT (1)
        INTO ln_count
        FROM xxwc.xxwc_ego_change_vendor
       WHERE login_id = p_login_id;


      IF p_status IN ('I')
      THEN
         INSERT
           INTO XXWC.XXWC_EGO_CHANGE_VENDOR (login_id,
                                             vendor_number,
                                             creation_date)
         VALUES (p_login_id, p_vendor_number, SYSDATE);
      ELSIF p_status IN ('U')
      THEN
         IF ln_count > 0
         THEN
            UPDATE XXWC.XXWC_EGO_CHANGE_VENDOR
               SET change_name = p_change_name, last_update_date = SYSDATE
             WHERE login_id = p_login_id;
         END IF;
      ELSIF p_status IN ('U1')
      THEN
         SELECT COUNT (1)
           INTO ln_count
           FROM XXWC.XXWC_EGO_CHANGE_VENDOR
          WHERE change_name = p_change_name;

         UPDATE XXWC.XXWC_EGO_CHANGE_VENDOR
            SET VENDOR_NUMBER = NVL (p_vendor_number, VENDOR_NUMBER)
          WHERE change_name = p_change_name;
      END IF;

      COMMIT;
   EXCEPTION
      WHEN OTHERS
      THEN
         ROLLBACK;
         xxcus_error_pkg.xxcus_error_main_api (
            p_called_from         => g_err_callfrom || '.' || l_procedure,
            p_ora_error_msg       => SUBSTR (SQLERRM, 1, 2000),
            p_error_desc          => SUBSTR (SQLERRM, 1, 240),
            p_distribution_list   => g_distro_list,
            p_module              => 'INV');
   END;

   FUNCTION Item_Number_validate (p_item_number VARCHAR2)
      RETURN VARCHAR2
   /********************************************************************************************************************************
     procedure : REPLACE_ITEM
     REVISIONS:
     Ver        Date        Author                     Description
     ---------  ----------  ---------------    -------------------------------------------------------------------------------------
     4.1     12-Oct-2015   P.vamshidhar        TMS#20150917-00149 - PCAM add validation for item check
                                                during WF approval -  Initial Version
   *********************************************************************************************************************************/
   IS
      lvc_return    VARCHAR2 (1);
      l_procedure   VARCHAR2 (100) := 'Item_Number_validate';
      l_err_msg     VARCHAR2 (200);
      ln_mst_org    NUMBER := FND_PROFILE.VALUE ('XXWC_ITEM_MASTER_ORG');
   BEGIN
      lvc_return := 'N';

      SELECT 'Y'
        INTO lvc_return
        FROM apps.mtl_system_items_b
       WHERE segment1 = p_item_number AND organization_id = ln_mst_org;

      RETURN (lvc_return);
   EXCEPTION
      WHEN NO_DATA_FOUND
      THEN
         lvc_return := 'N';
         RETURN (lvc_return);
      WHEN OTHERS
      THEN
         lvc_return := 'Y';
         l_err_msg := 'Error occured while validating Item ' || p_item_number;
         xxcus_error_pkg.xxcus_error_main_api (
            p_called_from         => g_err_callfrom || '.' || l_procedure,
            p_ora_error_msg       => SUBSTR (SQLERRM, 1, 2000),
            p_error_desc          => SUBSTR (l_err_msg, 1, 240),
            p_distribution_list   => g_distro_list,
            p_module              => 'INV');
         RETURN (lvc_return);
   END;

   -- Below procedure added in Rev 4.1
   PROCEDURE Temp_table_Item_update (p_item_id    IN NUMBER,
                                     p_new_item   IN VARCHAR2)
   /********************************************************************************************************************************
     procedure : REPLACE_ITEM
     REVISIONS:
     Ver           Date          Author                     Description
     ---------  -----------  ---------------    -------------------------------------------------------------------------------------
     4.1        12-Oct-2015   P.vamshidhar      TMS#20150917-00149 - PCAM add validation for item check
                                                during WF approval -  Initial Version
   *********************************************************************************************************************************/
   IS
      l_procedure   VARCHAR2 (100) := 'Temp_table_Item_update';
      l_err_msg     VARCHAR2 (200);
      PRAGMA AUTONOMOUS_TRANSACTION;
   BEGIN
      UPDATE XXWC.XXWC_MTL_SY_ITEMS_CHG_B
         SET SEGMENT1 = p_new_item
       WHERE INVENTORY_ITEM_ID = p_item_id;

      COMMIT;
   EXCEPTION
      WHEN NO_DATA_FOUND
      THEN
         ROLLBACK;
      WHEN OTHERS
      THEN
         xxcus_error_pkg.xxcus_error_main_api (
            p_called_from         => g_err_callfrom || '.' || l_procedure,
            p_ora_error_msg       => SUBSTR (SQLERRM, 1, 2000),
            p_error_desc          => SUBSTR (l_err_msg, 1, 240),
            p_distribution_list   => g_distro_list,
            p_module              => 'INV');
         ROLLBACK;
   END;
END;
/
