CREATE OR REPLACE PACKAGE APPS.XXWC_EGO_WORKFLOW_PKG
AS
   /*******************************************************************************************************************
   -- File Name: XXWC_EGO_WORKFLOW_PKG.pks
   --
   -- PROGRAM TYPE: PL/SQL Script   <API>
   --
   -- PURPOSE:
   -- HISTORY
   -- =================================================================================================================
   -- =================================================================================================================
   -- VERSION DATE          AUTHOR(S)       DESCRIPTION
   -- ------- -----------   --------------- ---------------------------------------------------------------------------
   -- 1.0     01-MAR-2015   Shankar Vanga   TMS#20150126-00044  - Create Single Item Workflow Tool

   --2.0      30-MAY-2015   P.Vamshidhar    TMS#20150519-00091 - Single Item UI Enhancements
                                            Added new procedure to show workflow notification subject

   --3.0      09-Jun-2015   P.Vamshidhar    TMS#20150608-00031 -- Added new Procedures/function
                                            XXWC_WF_ASSIGNEE_FUNC

   --4.0      04-Aug-2015   P.Vamshidhar    TMS#20150728-00091 -- EGO workflow errors in production

   --5.0      24-Sep-2015   P.Vamshidhar    TMS#20150901-00146 - PCAM add delete functionality for Cross References
   *********************************************************************************************************************/

   PROCEDURE get_rpm (itemtype    IN            VARCHAR2,
                      itemkey     IN            VARCHAR2,
                      actid       IN            NUMBER,
                      funcmode    IN            VARCHAR2,
                      resultout   IN OUT NOCOPY VARCHAR2);

   PROCEDURE update_status (itemtype    IN            VARCHAR2,
                            itemkey     IN            VARCHAR2,
                            actid       IN            NUMBER,
                            funcmode    IN            VARCHAR2,
                            resultout   IN OUT NOCOPY VARCHAR2);


   PROCEDURE SELECT_STEP_PEOPLE (itemtype   IN            VARCHAR2,
                                 itemkey    IN            VARCHAR2,
                                 actid      IN            NUMBER,
                                 funcmode   IN            VARCHAR2,
                                 result     IN OUT NOCOPY VARCHAR2);

   PROCEDURE SELECT_HAZMAT_ITEM (itemtype   IN            VARCHAR2,
                                 itemkey    IN            VARCHAR2,
                                 actid      IN            NUMBER,
                                 funcmode   IN            VARCHAR2,
                                 result     IN OUT NOCOPY VARCHAR2);


   PROCEDURE skip_worklfow_process (itemtype    IN            VARCHAR2,
                                    itemkey     IN            VARCHAR2,
                                    actid       IN            NUMBER,
                                    funcmode    IN            VARCHAR2,
                                    resultout   IN OUT NOCOPY /* file.sql.39 change */
                                                              VARCHAR2);

   PROCEDURE AddRoleToRoleUserTable (
      p_role_name    IN            VARCHAR2,
      x_role_users   IN OUT NOCOPY WF_DIRECTORY.UserTable);

   PROCEDURE SetStepPeopleToRoleUsers2 (
      x_role_users   IN OUT NOCOPY WF_DIRECTORY.UserTable);

   PROCEDURE SetStepPeopleRole (x_return_status      OUT NOCOPY VARCHAR2,
                                x_msg_count          OUT NOCOPY NUMBER,
                                x_msg_data           OUT NOCOPY VARCHAR2,
                                p_item_type       IN            VARCHAR2,
                                p_item_key        IN            VARCHAR2);

   PROCEDURE SetWFAdhocRole2 (
      p_role_name           IN OUT NOCOPY VARCHAR2,
      p_role_display_name   IN OUT NOCOPY VARCHAR2,
      p_role_users          IN            WF_DIRECTORY.UserTable,
      p_expiration_date     IN            DATE DEFAULT SYSDATE);


   -- Added new Proceure by Vamshi@  TMS#20150519-00091 - Single Item UI Enhancements

   PROCEDURE SET_STEP_ACT_OPTIONS (itemtype   IN            VARCHAR2,
                                   itemkey    IN            VARCHAR2,
                                   actid      IN            NUMBER,
                                   funcmode   IN            VARCHAR2,
                                   result     IN OUT NOCOPY VARCHAR2);

   -- Added new Function by Vamshi @ TMS#20150608-00031 - Single Item UI Enhancements
   FUNCTION XXWC_WF_ASSIGNEE_FUNC (p_change_notice     IN VARCHAR2,
                                   p_wf_key            IN VARCHAR2,
                                   p_status_name       IN VARCHAR2,
                                   p_approval_status   IN VARCHAR2)
      RETURN VARCHAR2;

   PROCEDURE Update_Change_line (p_change_name IN VARCHAR2);



   -- Added by Vamshi @TMS#20150728-00091 V4.0
   PROCEDURE SELECT_STD_REVIEWERS (itemtype   IN            VARCHAR2,
                                   itemkey    IN            VARCHAR2,
                                   actid      IN            NUMBER,
                                   funcmode   IN            VARCHAR2,
                                   result     IN OUT NOCOPY VARCHAR2);

   -- Added by Vamshi @TMS#20150901-00146 V5.0
   PROCEDURE Cross_ref_populate (p_change_name IN VARCHAR2);

END XXWC_EGO_WORKFLOW_PKG;
/
