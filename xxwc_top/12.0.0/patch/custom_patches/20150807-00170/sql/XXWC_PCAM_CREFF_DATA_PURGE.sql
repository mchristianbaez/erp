   /*********************************************************************************************************************
   -- File Name: XXWC_PCAM_CREFF_DATA_PURGE.sql
   --
   -- PROGRAM TYPE: SQL Script   <API>
   --
   -- PURPOSE: Table to caputure Cross reference data for PCAM tool
   --
   -- HISTORY
   -- =================================================================================================================
   -- =================================================================================================================
   -- VERSION DATE          AUTHOR(S)       DESCRIPTION
   -- ------- -----------   --------------- ----------------------------------------------------------------------------
   -- 1.0     16-Oct-2015   P.Vamshidhar    TMS#20150901-00146 - PCAM add delete functionality for Cross References
   ********************************************************************************************************************/

CREATE TABLE XXWC.XXWC_PCAM_CREFF_DATA_PURGE
(
  INVENTORY_ITEM_ID   VARCHAR2(100 BYTE),
  CROSS_REF           VARCHAR2(255 BYTE),
  CHANGE_NOTICE       VARCHAR2(100 BYTE),
  CHANGE_ID           NUMBER,
  CREATION_DATE       DATE                      DEFAULT SYSDATE,
  CREATED_BY          NUMBER,
  USER_LOGIN_ID       NUMBER,
  MFG_DELETE_FLAG     VARCHAR2(100 BYTE),
  BCODE_DELETE_FLAG   VARCHAR2(100 BYTE),
  VENDOR              VARCHAR2(20 BYTE),
  CROSS_REF_TYPE      VARCHAR2(100 BYTE),
  CROSS_REFERENCE_ID  VARCHAR2(100 BYTE)
);

GRANT ALTER, DELETE, INDEX, INSERT, REFERENCES, SELECT, UPDATE, ON COMMIT REFRESH, QUERY REWRITE, DEBUG, FLASHBACK ON XXWC.XXWC_PCAM_CREFF_DATA_PURGE TO APPS;
/
CREATE OR REPLACE SYNONYM APPS.XXWC_PCAM_CREFF_DATA_PURGE FOR XXWC.XXWC_PCAM_CREFF_DATA_PURGE;
/



