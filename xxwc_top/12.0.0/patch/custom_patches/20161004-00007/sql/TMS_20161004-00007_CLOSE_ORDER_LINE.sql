/*************************************************************************
  $Header TMS_20161004-00007_CLOSE_ORDER_LINE.sql $
  Module Name: TMS_20161004-00007  Data Fix script for 17517650

  PURPOSE: Data fix script for 17517650--No permanent fix in process (But patch available per oracle)

  REVISIONS:
  Ver        Date         Author                Description
  ---------  -----------  ------------------    --------------------------
  1.0        10-OCT-2016  Niraj K Ranjan        TMS#20161004-00007

**************************************************************************/ 
SET SERVEROUTPUT ON SIZE 1000000
DECLARE
BEGIN
   DBMS_OUTPUT.put_line ('TMS: 20161004-00007    , Before Update');
   UPDATE apps.wsh_delivery_details
   SET released_status = 'D',
   src_requested_quantity = 0,
   requested_quantity = 0,
   shipped_quantity = 0,
   cycle_count_quantity = 0,
   cancelled_quantity = 0,
   subinventory = null,
   locator_id = null,
   lot_number = null,
   revision = null,
   inv_interfaced_flag = 'X',
   oe_interfaced_flag = 'X'
   WHERE delivery_detail_id in (18391365,18363740,18361919,18361885);

   COMMIT;
   DBMS_OUTPUT.put_line (
         'TMS: 20161004-00007  wsh_delivery_details lines updated (Expected:4): '
      || SQL%ROWCOUNT);


   --4 row expected to be updated
   
   update apps.wsh_delivery_assignments
   set delivery_id = null,
   parent_delivery_detail_id = null
   where delivery_detail_id in (18391365,18363740,18361919,18361885);
   
   --4 row expected to be updated
   
   COMMIT;
   DBMS_OUTPUT.put_line (
         'TMS: 20161004-00007  wsh_delivery_assignments lines updated (Expected:4): '
      || SQL%ROWCOUNT);
   
   update apps.oe_order_lines_all
   set flow_status_code='CANCELLED',
   cancelled_flag='Y'
   where line_id=80829817
   and headeR_id=49473488;
   COMMIT;
   DBMS_OUTPUT.put_line (
         'TMS: 20161004-00007  oe_order_lines_all lines updated (Expected:1): '
      || SQL%ROWCOUNT);
EXCEPTION
   WHEN OTHERS
   THEN
      ROLLBACK;
      DBMS_OUTPUT.put_line ('TMS: 20161004-00007 , Errors : ' || SQLERRM);
END;
--1 row expected to be updated
/




