--------------------------------------------------------------------------------------
/*******************************************************************************
  Table: EIS_XXWC_INV_LIST_TBL
  Description: This table is used to get data from XXEIS.EIS_XXWC_INV_LIST_RPT_PKG Package.
  HISTORY
  ===============================================================================
  VERSION 		DATE            AUTHOR(S)       DESCRIPTION
  ------- -----------------  --------------- -----------------------------------------
  1.0     	23-Jun-2016        Siva      		TMS#20160608-00125  Performance Tuning
********************************************************************************/
CREATE TABLE XXEIS.EIS_XXWC_INV_LIST_TBL
  (
    PROCESS_ID            NUMBER,
    LOCATION              VARCHAR2(240) ,
    BRANCH_NUMBER         VARCHAR2(3),
    SEGMENT_LOCATION      VARCHAR2(30) ,
    LOCATION_STATE        VARCHAR2(240) ,
    PART_NUMBER           VARCHAR2(240) ,
    MFG_NUMBER            NUMBER ,
    DESCRIPTION           VARCHAR2(240) ,
    CONSIGNED             VARCHAR2(3) ,
    CAT_CLASS             VARCHAR2(2000) ,
    CAT_CLASS_DESCRIPTION VARCHAR2(2000) ,
    UOM                   VARCHAR2(3) ,
    SELLING_PRICE         NUMBER ,
    AVERAGECOST           NUMBER ,
    WEIGHT                NUMBER ,
    ONHAND                NUMBER ,
    FINAL_END_QTY         NUMBER ,
    VENDOR_NAME           VARCHAR2(240) ,
    SHELF_LIFE            NUMBER ,
    CONTAINER_TYPE        VARCHAR2(30) ,
    UN_NUMBER             VARCHAR2(30) ,
    SALE_UNITS            NUMBER ,
    SUBINVENTORY_CODE     VARCHAR2(240) ,
    BIN0                  VARCHAR2(240) ,
    BIN1                  VARCHAR2(240) ,
    BIN2                  VARCHAR2(240) ,
    BIN3                  VARCHAR2(240) ,
    TONHAND               NUMBER ,
    ON_ORDER              NUMBER ,
    ONHAND_TYPE           VARCHAR2(30) ,
    ORGANIZATION_ID       NUMBER ,
    INVENTORY_ITEM_ID     NUMBER ,
    OPEN_DEMAND           CHAR(1) ,
    MAKE_BUY              VARCHAR2(4)
  )
/
