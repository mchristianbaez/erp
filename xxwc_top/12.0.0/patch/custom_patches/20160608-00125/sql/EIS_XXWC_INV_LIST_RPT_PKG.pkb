CREATE OR REPLACE 
PACKAGE BODY  XXEIS.EIS_XXWC_INV_LIST_RPT_PKG as
--//============================================================================
--//  
--// Change Request 			:: Performance Issue 
--//
--// Object Usage 				:: This Object Referred by "Inventory Listing Report"
--//
--// Object Name         		:: XXEIS.EIS_XXWC_INV_LIST_RPT_PKG
--//
--// Object Type         		:: Package Specification
--//
--// Object Description  		:: This Package will trigger in before report and insert the values into Table
--//
--// Version Control
--//============================================================================
--// Version    Author           Date          Description
--//----------------------------------------------------------------------------
--// 1.0       Pramod  	     05/06/2016   Initial Build --TMS#20160503-00085 --Performance Tuning
--// 1.1  	   Siva			 06/06/2016    TMS#20160524-00045
--// 1.2  	   Siva			 23/06/2016    TMS#20160608-00125 
--//============================================================================

/* --commneted for version 1.2
Function get_process_id return number as
--//============================================================================
--//
--// Object Name           :: get_process_id  
--//
--// Object Usage 		   :: This Object Referred by "Inventory Listing Report"
--//
--// Object Type           :: Function
--//
--// Object Description   ::  This  Function is created based on the EIS_XXWC_INV_LISTING_V View to populate
--//                          the process_id in view.
--// Version Control
--//============================================================================
--// Version    Author           Date          Description
--//----------------------------------------------------------------------------
--// 1.0       Pramod  	    05/06/2016   Initial Build --TMS#20160503-00085 --Performance Tuning
--//============================================================================			  
l_process_id  number;
begin
l_process_id:=g_process_id;
return l_process_id;
EXCEPTION
WHEN OTHERS THEN
RETURN NULL;
end get_process_id;
*/--commneted for version 1.2

procedure populate_Inv_List_Details(P_process_id    in number,
                                    p_organization  in varchar2,
                                    p_subinv        in varchar2,
                                    p_qty_on_hand   in varchar2
                                    ) is
--//============================================================================
--//
--// Object Name         :: POPULATE_INV_LIST_DETAILS  
--//
--// Object Usage 		 :: This Object Referred by "Inventory Listing Report"
--//
--// Object Type         :: Procedure
--//
--// Object Description  :: This  procedure is created based on the EIS_XXWC_INV_LISTING_V View.Due to the 
--// 						performance issue ,spilt the view definition based on the driving tables and driving data.
--//
--// Version Control
--//============================================================================
--// Version    Author           Date          Description
--//----------------------------------------------------------------------------
--// 1.0        PRAMOD  	  05/06/2016     Initial Build --TMS#20160503-00085 --Performance Tuning
--// 1.1  	    Siva			 06/06/2016    TMS#20160524-00045
--// 1.2  	   Siva			   23/06/2016    TMS#20160608-00125 
--//============================================================================
									
l_stmt1 		    varchar2(32000);
l_start_date 	  date;
l_end_date 		  date;
l_stmt2 		    varchar2(32000);
l_stmt3 		    varchar2(32000);
l_cond1 		    varchar2(32000);
l_ref_cursor1   cursor_type4;
l_ref_cursor2   cursor_type4;
L_REF_CURSOR3   CURSOR_TYPE4;
L_REF_CURSOR4   CURSOR_TYPE4;
L_REF_CURSOR5   CURSOR_TYPE4;  --Added for version 1.2
l_stmt4         varchar2(32000);
L_STMT6         VARCHAR2(32000);
L_STMT5         VARCHAR2(32000);
L_STMT7         VARCHAR2(32000); --Added for version 1.2
type org_list_rec is record
(
process_id number,
organization_id number
);
type org_list_rec_tab is table of org_list_rec index by binary_integer;
org_list_tab org_list_rec_tab;
type subinv_onhand_dtls_rec is record
 (
  process_id        number,
  onhand            number,
  IS_CONSIGNED      varchar2(20),
  subinventory_code varchar(240),
  ORGANIZATION_ID   number,
  inventory_item_id number
  );
  
type onhand_dtls_rec is record
 (
  process_id        number,
  ONHAND            number,
  inventory_item_id number,
  organization_id   number
);

type flex_val_rec is record
 (
  process_id number,
  FLEX_VALUE varchar2(240),
  description varchar2(2000)
);

type subinv_onhand_dtls_rec_tab is table of subinv_onhand_dtls_rec index by binary_integer;
  subinv_onhand_dtls_tab subinv_onhand_dtls_rec_tab;
type onhand_dtls_rec_tab is table of onhand_dtls_rec index by binary_integer;
  onhand_dtls_tab onhand_dtls_rec_tab;
type flex_val_rec_tab is table of flex_val_rec index by binary_integer;
  FLEX_VAL_TAB FLEX_VAL_REC_TAB;
  
TYPE INV_LISTING_REC IS TABLE OF XXEIS.EIS_XXWC_INV_LIST_TBL%ROWTYPE INDEX BY BINARY_INTEGER;
  INV_LISTING_REC_TAB INV_LISTING_REC;  --Added for version 1.2
  
begin
/*Added for version 1.2*/
org_list_tab.DELETE;
SUBINV_ONHAND_DTLS_TAB.DELETE;
onhand_dtls_tab.delete;
FLEX_VAL_TAB.DELETE;
INV_LISTING_REC_TAB.delete;

begin
  
if p_organization!='All' then

L_STMT5:=	'select '||P_PROCESS_ID||',ORGANIZATION_ID from xxeis.eis_org_access_v where  ORGANIZATION_CODE in ('||XXEIS.EIS_RS_UTILITY.GET_PARAM_VALUES(P_ORGANIZATION)||' )';

else

L_STMT5:=	'select '||P_PROCESS_ID||',ORGANIZATION_ID from xxeis.eis_org_access_v';

end if;


fnd_file.put_line(fnd_file.log,'L_STMT5 is '||L_STMT5); 

  OPEN l_ref_cursor4  FOR L_STMT5  ;
		LOOP
			FETCH l_ref_cursor4 bulk collect into org_list_tab limit 10000;
			if org_list_tab.COUNT  >= 1  then
                  FORALL J IN 1 .. org_list_tab.COUNT 
                     
                     INSERT INTO xxeis.EIS_XXWC_VALID_ORGS_TAB
                          values org_list_tab(j);    
                 commit;
       end if;

       			IF l_ref_cursor4%NOTFOUND Then
				close l_ref_cursor4;
				EXIT;
			end if;
		end LOOP;  
      


	exception when others then
	null;
	END;
  
    begin
      select max(gps.start_date),max(gps.end_date) 
      INTO  g_start_date,g_end_date  
      FROM GL_PERIOD_STATUSES GPS
      where application_id=101 
      and period_type=21 
      AND TRUNC(sysdate) BETWEEN gps.start_date AND gps.end_date;
      Exception when others then
      NULL;
      end;
      
      
    
  L_STMT2:='
	select '||P_process_id||' process_id,
          onhand,
          is_consigned,
          subinventory_code,
          organization_id,
          inventory_item_id
  from(
    SELECT /*+ index(moqd MTL_ONHAND_QUANTITIES_N4)*/
    SUM(NVL(moqd.primary_transaction_quantity,0)) 
      ONHAND,
      moqd.is_consigned,
      moqd.subinventory_code,
      moqd.organization_id,
      moqd.inventory_item_id
    from MTL_ONHAND_QUANTITIES_DETAIL MOQD
    where  moqd.organization_id not in (83,188,84,222,165,227)
   --  and MOQD.INVENTORY_ITEM_ID =2925299
    --and organization_id=329
    AND EXISTS
    ( select 1
      FROM  xxeis.EIS_XXWC_VALID_ORGS_TAB mo
      WHERE mo.ORGANIZATION_ID =  moqd.organization_id
	  and mo.process_id = '||p_process_id||'
    ) 
      GROUP BY  MOQD.ORGANIZATION_ID,
                MOQD.SUBINVENTORY_CODE,
                MOQD.INVENTORY_ITEM_ID,
                MOQD.is_consigned
  ) X  ';
  
      If p_qty_on_hand ='On Hand' then
        l_stmt2 := l_stmt2||chr(10)||'where x.ONHAND>0' ;
      elsif p_qty_on_hand ='No on Hand' then
        l_stmt2 := l_stmt2||chr(10)|| 'where x.ONHAND=0' ;
      elsif p_qty_on_hand ='Negative on hand' then
        L_STMT2 := L_STMT2||CHR(10)|| 'where x.ONHAND<0' ;
      end if;
   
      OPEN l_ref_cursor1  FOR l_stmt2  ;
		LOOP
			FETCH l_ref_cursor1 bulk collect into subinv_onhand_dtls_tab limit 10000;
			if subinv_onhand_dtls_tab.COUNT  >= 1  then
                  FORALL J IN 1 .. subinv_onhand_dtls_tab.COUNT 
                     
                     INSERT INTO xxeis.eis_xxwc_subinv_onhand_dtls
                          values subinv_onhand_dtls_tab(j);    
                 commit;
       end if;

       			IF l_ref_cursor1%NOTFOUND Then
				close L_REF_CURSOR1;
				EXIT;
			end if;
		End loop;  
      
      	
l_stmt4 := 'select process_id ,sum(ONHAND),inventory_item_id,ORGANIZATION_ID from xxeis.eis_xxwc_subinv_onhand_dtls 
			where process_id ='||p_process_id||' 
			group by  inventory_item_id,ORGANIZATION_ID,process_id';  -- added for version 1.1

	      OPEN l_ref_cursor2  FOR l_stmt4  ;
		LOOP
			FETCH l_ref_cursor2 bulk collect into onhand_dtls_tab limit 10000;
			if onhand_dtls_tab.COUNT  >= 1  then
                  FORALL J IN 1 .. onhand_dtls_tab.COUNT 
                     
                     INSERT INTO xxeis.eis_xxwc_onhand_dtls
                          values onhand_dtls_tab(j);    
                 commit;
       end if;

       			if L_REF_CURSOR2%NOTFOUND then
				close L_REF_CURSOR2;
				EXIT;
			end if;
		End loop;  
  
  L_STMT6:='select '||P_PROCESS_ID||' process_id,flex_value,description from fnd_flex_values_vl fvcc where flex_value_set_id=1015059';  -- added for version 1.1
  
-- execute immediate l_stmt6 bulk collect into flex_val_tab; --commneted for version 1.2
 
   OPEN l_ref_cursor3  FOR l_stmt6  ;
		loop
			FETCH l_ref_cursor3 bulk collect into flex_val_tab limit 10000;
			if flex_val_tab.COUNT  >= 1  then
                  FORALL J IN 1 .. flex_val_tab.COUNT 
                     
                     INSERT INTO xxeis.EIS_XXWC_FLEX_VAL_TAB
                          values flex_val_tab(j);    
                 commit;
       end if;

       			if L_REF_CURSOR3%NOTFOUND then
				close L_REF_CURSOR3;
				EXIT;
			end if;
		End loop;  

/*Added below query for version 1.2*/
If p_qty_on_hand !='ALL' then  
L_STMT7:='SELECT  /*+ ORDERED INDEX (MOQ, EIS_XXWC_SUBINV_ONHAND_N1) INDEX (FVCC EIS_XXWC_FLEX_VAL_TAB_N1) USE_NL (MOQ MOQT) USE_NL (MOQ MSI) USE_NL (MOQ tab) USE_NL (MSI GCC) */ --added for version 1.2
    '||P_PROCESS_ID||' process_id,
    SUBSTR(GCC.SEGMENT2,3) LOCATION,
    MP.ORGANIZATION_CODE BRANCH_NUMBER,
    GCC.SEGMENT2 SEGMENT_LOCATION,
    (SELECT LOC.REGION_2
    FROM HR_ORGANIZATION_UNITS HOU,
      HR_LOCATIONS_ALL LOC
    WHERE MP.ORGANIZATION_ID = HOU.ORGANIZATION_ID
    AND HOU.LOCATION_ID      = LOC.LOCATION_ID(+)
    ) LOCATION_STATE,
    --      HOU.REGION_2 LOCATION_STATE,
    --  hLA.region_2 location_state,
    MSI.SEGMENT1 PART_NUMBER,
    xxeis.eis_xxwc_inv_list_rpt_pkg.get_lhqty(msi.inventory_item_id) mfg_number,
    --   cr.cross_reference mfg_number,
    --XXEIS.EIS_RS_XXWC_COM_UTIL_PKG.Get_Item_Crossref(msi.inventory_item_id,msi.organization_id) mfg_number,
    MSI.DESCRIPTION,
    DECODE(moq.is_consigned,1,''Y'',2,''N'') CONSIGNED,
    tab.inv_cat_seg1
    ||''.''
    ||tab.cat cat_class,
    --xxeis.eis_rs_xxwc_com_util_pkg.get_inv_cat_class(msi.inventory_item_id,msi.organization_id) cat_class,
    fvcc.description CAT_CLASS_DESCRIPTION,
    --       xxeis.eis_rs_xxwc_com_util_pkg.get_inv_cat_class_desc(msi.inventory_item_id,msi.organization_id) CAT_CLASS_DESCRIPTION,
    msi.primary_uom_code uom,
    msi.list_price_per_unit selling_price,
    tab.aver_cost averagecost,
    --NVL(apps.cst_cost_api.get_item_cost(1,msi.inventory_item_id,msi.organization_id),0) averagecost,
    msi.unit_weight weight,
    NVL(MOQ.ONHAND,0) ONHAND,
    NVL(XXEIS.EIS_RS_XXWC_COM_UTIL_PKG.GET_FINAL_END_QTY(MSI.INVENTORY_ITEM_ID,MSI.ORGANIZATION_ID,xxeis.EIS_XXWC_INV_LIST_RPT_PKG.get_end_date),0) FINAL_END_QTY,
    -- **NVL(XXEIS.EIS_RS_XXWC_COM_UTIL_PKG.GET_FINAL_END_QTY(MSI.INVENTORY_ITEM_ID,MSI.ORGANIZATION_ID,GPS.END_DATE),0) FINAL_END_QTY,
    tab.vendor_name VENDOR_NAME,
    --XXEIS.EIS_RS_XXWC_COM_UTIL_PKG.GET_VENDOR_NAME(MSI.INVENTORY_ITEM_ID,MSI.ORGANIZATION_ID) VENDOR_NAME,
    MSI.SHELF_LIFE_DAYS SHELF_LIFE,
    MSI.CONTAINER_TYPE_CODE CONTAINER_TYPE,
    PUN.UN_NUMBER UN_NUMBER,
    --XXEIS.EIS_RS_XXWC_COM_UTIL_PKG.GET_MTD_SALES(MSI.INVENTORY_ITEM_ID,MSI.ORGANIZATION_ID,GPS.START_DATE,GPS.END_DATE) SALE_UNITS,
    TAB.ONE_STORE_SALE SALE_UNITS,
    moq.subinventory_code subinventory_code,
    (SELECT mil.Segment1
    FROM MTL_ITEM_LOCATIONS_KFV MIL,
      ------ diane added bin0 2/7/2014  --------
      MTL_SECONDARY_LOCATORS MSL
    WHERE MSL.INVENTORY_ITEM_ID = MSI.INVENTORY_ITEM_ID
    AND MSL.ORGANIZATION_ID     = MSI.ORGANIZATION_ID
    AND MSL.SECONDARY_LOCATOR   = MIL.INVENTORY_LOCATION_ID
    AND mil.segment1 LIKE ''0%''
    AND rownum = 1
    ) BIN0,
    (SELECT mil.Segment1
    FROM MTL_ITEM_LOCATIONS_KFV MIL,
      MTL_SECONDARY_LOCATORS MSL
    WHERE MSL.INVENTORY_ITEM_ID = MSI.INVENTORY_ITEM_ID
    AND MSL.ORGANIZATION_ID     = MSI.ORGANIZATION_ID
    AND MSL.SECONDARY_LOCATOR   = MIL.INVENTORY_LOCATION_ID
    AND mil.segment1 LIKE ''1%''
    AND rownum = 1
    ) BIN1,
    (SELECT mil.Segment1
    FROM MTL_ITEM_LOCATIONS_KFV MIL,
      MTL_SECONDARY_LOCATORS MSL
    WHERE MSL.INVENTORY_ITEM_ID = MSI.INVENTORY_ITEM_ID
    AND MSL.ORGANIZATION_ID     = MSI.ORGANIZATION_ID
    AND MSL.SECONDARY_LOCATOR   = MIL.INVENTORY_LOCATION_ID
    AND mil.segment1 LIKE ''2%''
    AND rownum = 1
    ) BIN2,
    (SELECT mil.Segment1
    FROM MTL_ITEM_LOCATIONS_KFV MIL,
      MTL_SECONDARY_LOCATORS MSL
    WHERE MSL.INVENTORY_ITEM_ID = MSI.INVENTORY_ITEM_ID
    AND MSL.ORGANIZATION_ID     = MSI.ORGANIZATION_ID
    AND MSL.SECONDARY_LOCATOR   = MIL.INVENTORY_LOCATION_ID
    AND mil.segment1 LIKE ''3%''
    AND rownum = 1
    ) BIN3,
    NVL(moqt.onhand,0) tonhand,
    tab.on_ord on_order,
    CASE
      WHEN NVL(moqt.onhand,0) > 0
      THEN ''On Hand''
      WHEN NVL(moqt.onhand,0) = 0
      THEN ''No on Hand''
      WHEN NVL(moqt.onhand,0) < 0
      THEN ''Negative on hand''
    END Onhand_Type,
    MSI.ORGANIZATION_ID,
    MSI.INVENTORY_ITEM_ID,
    CASE
      WHEN NVL (TAB.demand, 0) != 0
      THEN ''Y''
      ELSE ''N''
    END OPEN_DEMAND,                                                     --added for version 1.1
    DECODE (MSI.PLANNING_MAKE_BUY_CODE, ''2'', ''Buy'', ''1'', ''Make'') Make_Buy--added for version 1.2
  FROM XXEIS.EIS_XXWC_SUBINV_ONHAND_DTLS MOQ,
    XXEIS.EIS_XXWC_ONHAND_DTLS MOQT,
    MTL_SYSTEM_ITEMS_KFV MSI,
    XXEIS.EIS_XXWC_PO_ISR_TAB tab,
    XXEIS.EIS_XXWC_FLEX_VAL_TAB FVCC,
    MTL_PARAMETERS MP,
    --    hr_organization_units_v hou,
    GL_CODE_COMBINATIONS_KFV GCC,
    PO_UN_NUMBERS PUN
  WHERE MSI.ORGANIZATION_ID   = MOQ.ORGANIZATION_ID
  AND MSI.INVENTORY_ITEM_ID   = MOQ.INVENTORY_ITEM_ID
  AND MOQ.PROCESS_ID          = '||P_PROCESS_ID||'
  AND MOQ.ORGANIZATION_ID     = MOQT.ORGANIZATION_ID
  AND MOQ.INVENTORY_ITEM_ID   = MOQT.INVENTORY_ITEM_ID
  AND MOQT.PROCESS_ID         = '||P_PROCESS_ID||'
  AND MOQ.ORGANIZATION_ID     = TAB.ORGANIZATION_ID(+)
  AND MOQ.INVENTORY_ITEM_ID   = TAB.INVENTORY_ITEM_ID(+)
  AND TAB.CAT                 = FVCC.FLEX_VALUE(+)
  AND FVCC.PROCESS_ID(+)      = '||P_PROCESS_ID||'
  AND MOQ.ORGANIZATION_ID     = MP.ORGANIZATION_ID
  AND GCC.CODE_COMBINATION_ID = MSI.COST_OF_SALES_ACCOUNT
  AND MSI.UN_NUMBER_ID        = PUN.UN_NUMBER_ID (+)';

end if;

If p_qty_on_hand ='ALL' then
L_STMT7:='SELECT  /*+ ORDERED INDEX (MOQ, EIS_XXWC_SUBINV_ONHAND_N1) INDEX (FVCC EIS_XXWC_FLEX_VAL_TAB_N1) USE_NL (MOQ MOQT) USE_NL (MOQ MSI) USE_NL (MOQ tab) USE_NL (MSI GCC) */ --added for version 1.2
    '||P_PROCESS_ID||' process_id,
    SUBSTR(GCC.SEGMENT2,3) LOCATION,
    MP.ORGANIZATION_CODE BRANCH_NUMBER,
    GCC.SEGMENT2 SEGMENT_LOCATION,
    (SELECT LOC.REGION_2
    FROM HR_ORGANIZATION_UNITS HOU,
      HR_LOCATIONS_ALL LOC
    WHERE MP.ORGANIZATION_ID = HOU.ORGANIZATION_ID
    AND HOU.LOCATION_ID      = LOC.LOCATION_ID(+)
    ) LOCATION_STATE,
    --      HOU.REGION_2 LOCATION_STATE,
    --  hLA.region_2 location_state,
    MSI.SEGMENT1 PART_NUMBER,
    xxeis.eis_xxwc_inv_list_rpt_pkg.get_lhqty(msi.inventory_item_id) mfg_number,
    --   cr.cross_reference mfg_number,
    --XXEIS.EIS_RS_XXWC_COM_UTIL_PKG.Get_Item_Crossref(msi.inventory_item_id,msi.organization_id) mfg_number,
    MSI.DESCRIPTION,
    DECODE(moq.is_consigned,1,''Y'',2,''N'') CONSIGNED,
    tab.inv_cat_seg1
    ||''.''
    ||tab.cat cat_class,
    --xxeis.eis_rs_xxwc_com_util_pkg.get_inv_cat_class(msi.inventory_item_id,msi.organization_id) cat_class,
    fvcc.description CAT_CLASS_DESCRIPTION,
    --       xxeis.eis_rs_xxwc_com_util_pkg.get_inv_cat_class_desc(msi.inventory_item_id,msi.organization_id) CAT_CLASS_DESCRIPTION,
    msi.primary_uom_code uom,
    msi.list_price_per_unit selling_price,
    tab.aver_cost averagecost,
    --NVL(apps.cst_cost_api.get_item_cost(1,msi.inventory_item_id,msi.organization_id),0) averagecost,
    msi.unit_weight weight,
    NVL(MOQ.ONHAND,0) ONHAND,
    NVL(XXEIS.EIS_RS_XXWC_COM_UTIL_PKG.GET_FINAL_END_QTY(MSI.INVENTORY_ITEM_ID,MSI.ORGANIZATION_ID,xxeis.EIS_XXWC_INV_LIST_RPT_PKG.get_end_date),0) FINAL_END_QTY,
    -- **NVL(XXEIS.EIS_RS_XXWC_COM_UTIL_PKG.GET_FINAL_END_QTY(MSI.INVENTORY_ITEM_ID,MSI.ORGANIZATION_ID,GPS.END_DATE),0) FINAL_END_QTY,
    tab.vendor_name VENDOR_NAME,
    --XXEIS.EIS_RS_XXWC_COM_UTIL_PKG.GET_VENDOR_NAME(MSI.INVENTORY_ITEM_ID,MSI.ORGANIZATION_ID) VENDOR_NAME,
    MSI.SHELF_LIFE_DAYS SHELF_LIFE,
    MSI.CONTAINER_TYPE_CODE CONTAINER_TYPE,
    PUN.UN_NUMBER UN_NUMBER,
    --XXEIS.EIS_RS_XXWC_COM_UTIL_PKG.GET_MTD_SALES(MSI.INVENTORY_ITEM_ID,MSI.ORGANIZATION_ID,GPS.START_DATE,GPS.END_DATE) SALE_UNITS,
    TAB.ONE_STORE_SALE SALE_UNITS,
    moq.subinventory_code subinventory_code,
    (SELECT mil.Segment1
    FROM MTL_ITEM_LOCATIONS_KFV MIL,
      ------ diane added bin0 2/7/2014  --------
      MTL_SECONDARY_LOCATORS MSL
    WHERE MSL.INVENTORY_ITEM_ID = MSI.INVENTORY_ITEM_ID
    AND MSL.ORGANIZATION_ID     = MSI.ORGANIZATION_ID
    AND MSL.SECONDARY_LOCATOR   = MIL.INVENTORY_LOCATION_ID
    AND mil.segment1 LIKE ''0%''
    AND rownum = 1
    ) BIN0,
    (SELECT mil.Segment1
    FROM MTL_ITEM_LOCATIONS_KFV MIL,
      MTL_SECONDARY_LOCATORS MSL
    WHERE MSL.INVENTORY_ITEM_ID = MSI.INVENTORY_ITEM_ID
    AND MSL.ORGANIZATION_ID     = MSI.ORGANIZATION_ID
    AND MSL.SECONDARY_LOCATOR   = MIL.INVENTORY_LOCATION_ID
    AND mil.segment1 LIKE ''1%''
    AND rownum = 1
    ) BIN1,
    (SELECT mil.Segment1
    FROM MTL_ITEM_LOCATIONS_KFV MIL,
      MTL_SECONDARY_LOCATORS MSL
    WHERE MSL.INVENTORY_ITEM_ID = MSI.INVENTORY_ITEM_ID
    AND MSL.ORGANIZATION_ID     = MSI.ORGANIZATION_ID
    AND MSL.SECONDARY_LOCATOR   = MIL.INVENTORY_LOCATION_ID
    AND mil.segment1 LIKE ''2%''
    AND rownum = 1
    ) BIN2,
    (SELECT mil.Segment1
    FROM MTL_ITEM_LOCATIONS_KFV MIL,
      MTL_SECONDARY_LOCATORS MSL
    WHERE MSL.INVENTORY_ITEM_ID = MSI.INVENTORY_ITEM_ID
    AND MSL.ORGANIZATION_ID     = MSI.ORGANIZATION_ID
    AND MSL.SECONDARY_LOCATOR   = MIL.INVENTORY_LOCATION_ID
    AND mil.segment1 LIKE ''3%''
    AND rownum = 1
    ) BIN3,
    NVL(moqt.onhand,0) tonhand,
    tab.on_ord on_order,
    CASE
      WHEN NVL(moqt.onhand,0) > 0
      THEN ''On Hand''
      WHEN NVL(moqt.onhand,0) = 0
      THEN ''No on Hand''
      WHEN NVL(moqt.onhand,0) < 0
      THEN ''Negative on hand''
    END Onhand_Type,
    MSI.ORGANIZATION_ID,
    MSI.INVENTORY_ITEM_ID,
    CASE
      WHEN NVL (TAB.demand, 0) != 0
      THEN ''Y''
      ELSE ''N''
    END OPEN_DEMAND,                                                     --added for version 1.1
    DECODE (MSI.PLANNING_MAKE_BUY_CODE, ''2'', ''Buy'', ''1'', ''Make'') Make_Buy--added for version 1.2
  FROM XXEIS.EIS_XXWC_SUBINV_ONHAND_DTLS MOQ,
    XXEIS.EIS_XXWC_ONHAND_DTLS MOQT,
    MTL_SYSTEM_ITEMS_KFV MSI,
    XXEIS.EIS_XXWC_PO_ISR_TAB TAB,
    XXEIS.EIS_XXWC_FLEX_VAL_TAB FVCC,
    MTL_PARAMETERS MP,
    --    hr_organization_units_v hou,
    GL_CODE_COMBINATIONS_KFV GCC,
    PO_UN_NUMBERS PUN
  WHERE MSI.ORGANIZATION_ID   = MOQ.ORGANIZATION_ID(+)
  AND MSI.INVENTORY_ITEM_ID   = MOQ.INVENTORY_ITEM_ID(+)
  AND MOQ.PROCESS_ID(+)          = '||P_PROCESS_ID||'
  AND MOQ.ORGANIZATION_ID     = MOQT.ORGANIZATION_ID(+)
  AND MOQ.INVENTORY_ITEM_ID   = MOQT.INVENTORY_ITEM_ID(+)
  AND MOQT.PROCESS_ID(+)         = '||P_PROCESS_ID||'
  AND MSI.ORGANIZATION_ID     = TAB.ORGANIZATION_ID(+)
  AND MSI.INVENTORY_ITEM_ID   = TAB.INVENTORY_ITEM_ID(+)
  AND TAB.CAT                 = FVCC.FLEX_VALUE(+)
  AND FVCC.PROCESS_ID(+)      = '||P_PROCESS_ID||'
  AND MSI.ORGANIZATION_ID     = MP.ORGANIZATION_ID
  AND MSI.COST_OF_SALES_ACCOUNT = GCC.CODE_COMBINATION_ID 
  AND MSI.UN_NUMBER_ID        = PUN.UN_NUMBER_ID (+)
  AND EXISTS
    ( select /*+ INDEX(mo EIS_XXWC_VALID_ORGS_TAB_N1)*/ 1  --added for version 1.2
      FROM  xxeis.EIS_XXWC_VALID_ORGS_TAB mo
      WHERE mo.ORGANIZATION_ID =  MSI.organization_id
	  and MO.PROCESS_ID = '||p_process_id||'
    ) ';
end if;
--End of version 1.2
fnd_file.put_line(fnd_file.log,'L_STMT7'||L_STMT7);

OPEN L_REF_CURSOR5  FOR L_STMT7  ;
    LOOP
			FETCH L_REF_CURSOR5 bulk collect into INV_LISTING_REC_TAB limit 10000;
			if INV_LISTING_REC_TAB.COUNT  >= 1  then
                  FORALL J IN 1 .. INV_LISTING_REC_TAB.COUNT 
                     
                     INSERT INTO XXEIS.EIS_XXWC_INV_LIST_TBL
                          values INV_LISTING_REC_TAB(j);    
                 commit;
       end if;

       			if L_REF_CURSOR5%NOTFOUND then
				close L_REF_CURSOR5;
				EXIT;
			END IF;
End loop;


fnd_file.put_line(fnd_file.log,'end');
  
end POPULATE_INV_LIST_DETAILS;
  
function GET_END_DATE return date is
begin
return g_start_date;
end;
  
  FUNCTION get_lhqty(p_inventory_item_id number) RETURN number
IS
L_HASH_INDEX varchar2(100);
L_SQL varchar2(32000);
begin
G_LHQTY := NULL;


l_sql:='SELECT  MAX (cross_reference) cross_reference
               FROM inv.mtl_cross_references_b
              where CROSS_REFERENCE_TYPE = ''VENDOR''
              and inventory_item_id = :1
           GROUP BY inventory_item_id';
    BEGIN
        l_hash_index:=p_inventory_item_id;
        G_LHQTY :=G_LHQTY_VLDN_TBL(L_HASH_INDEX);
 exception
    when no_data_found
    THEN
    BEGIN      
    EXECUTE IMMEDIATE L_SQL INTO G_LHQTY USING p_inventory_item_id;
    EXCEPTION WHEN NO_DATA_FOUND THEN
    G_LHQTY :=0;
    WHEN OTHERS THEN
    G_LHQTY :=0;
    END;      
                      l_hash_index:=p_inventory_item_id;
                       G_LHQTY_VLDN_TBL(L_HASH_INDEX) := G_LHQTY;
    END;
     return  G_LHQTY;
     EXCEPTION WHEN OTHERS THEN
      G_LHQTY:=0;
      RETURN  G_LHQTY;

end get_lhqty;


PROCEDURE CLEAR_TEMP_TABLES ( P_PROCESS_ID IN NUMBER)
AS
--//============================================================================
--//
--// Object Name         :: clear_temp_tables
--//
--// Object Type         :: Procedure
--//
--// Object Description  :: This procedure will trigger in After Report level and delete all the records from the table based on that particular process_id.
--//
--// Version Control
--//============================================================================
--// Version    Author           Date          Description
--//----------------------------------------------------------------------------
--// 1.0        PRAMOD  	   05/06/2016   Initial Build --TMS#20160503-00085 --Performance Tuning
--//============================================================================ 

  BEGIN  
  DELETE FROM XXEIS.EIS_XXWC_VALID_ORGS_TAB WHERE PROCESS_ID=P_PROCESS_ID;
  DELETE FROM XXEIS.EIS_XXWC_SUBINV_ONHAND_DTLS WHERE PROCESS_ID=P_PROCESS_ID;
  DELETE FROM XXEIS.EIS_XXWC_ONHAND_DTLS WHERE PROCESS_ID=P_PROCESS_ID;
  DELETE FROM XXEIS.EIS_XXWC_FLEX_VAL_TAB where PROCESS_ID=P_PROCESS_ID;  
  DELETE FROM XXEIS.EIS_XXWC_INV_LIST_TBL WHERE PROCESS_ID=P_PROCESS_ID;
  COMMIT;
END ;  
  
END;
/
