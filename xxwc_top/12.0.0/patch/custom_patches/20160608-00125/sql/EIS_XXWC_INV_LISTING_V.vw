---------------------------------------------------------------------------------------------------------------
/**************************************************************************************************************
  $Header XXEIS.EIS_XXWC_INV_LISTING_V $
  PURPOSE	  : Inventory Listing Report
  REVISIONS   :
  VERSION 	 DATE               AUTHOR(S)       DESCRIPTION
  ------- -----------------  --------------- -----------------------------------------
  1.0      16-Apr-2016        Pramod   		 TMS#20160503-00085   Performance Tuning
  1.1 	   31-May-2016		  Siva			 TMS#20160524-00045   --added OPEN_DEMAND column
  1.2 	   31-May-2016		  Siva			 TMS#20160527-00054   --added Make_Buy column
  1.3	   23-MAY-2016		  Siva           TMS#20160608-00125   -- Changed the entire view to trigger based report and populated the
	data from XXEIS.EIS_XXWC_INV_LIST_TBL. This table is being populated from XXEIS.EIS_XXWC_INV_LIST_RPT_PKG Package.
**************************************************************************************************************/
DROP VIEW XXEIS.EIS_XXWC_INV_LISTING_V;
/* Changed the entire view for version 1.3*/
CREATE OR REPLACE VIEW XXEIS.EIS_XXWC_INV_LISTING_V (PROCESS_ID, LOCATION, BRANCH_NUMBER, SEGMENT_LOCATION, LOCATION_STATE, PART_NUMBER, MFG_NUMBER, DESCRIPTION, CONSIGNED, CAT_CLASS, CAT_CLASS_DESCRIPTION, UOM, SELLING_PRICE, AVERAGECOST, WEIGHT, ONHAND, FINAL_END_QTY, VENDOR_NAME, SHELF_LIFE, CONTAINER_TYPE, UN_NUMBER, SALE_UNITS, SUBINVENTORY_CODE, BIN0, BIN1, BIN2, BIN3, TONHAND, ON_ORDER, ONHAND_TYPE, ORGANIZATION_ID, INVENTORY_ITEM_ID, OPEN_DEMAND, MAKE_BUY)
AS
  SELECT PROCESS_ID,
    LOCATION,
    BRANCH_NUMBER,
    SEGMENT_LOCATION,
    LOCATION_STATE,
    PART_NUMBER,
    MFG_NUMBER,
    DESCRIPTION,
    CONSIGNED,
    CAT_CLASS,
    CAT_CLASS_DESCRIPTION,
    UOM,
    SELLING_PRICE,
    AVERAGECOST,
    WEIGHT,
    ONHAND,
    FINAL_END_QTY,
    VENDOR_NAME,
    SHELF_LIFE,
    CONTAINER_TYPE,
    UN_NUMBER,
    SALE_UNITS,
    SUBINVENTORY_CODE,
    BIN0,
    BIN1,
    BIN2,
    BIN3,
    TONHAND,
    ON_ORDER,
    ONHAND_TYPE,
    ORGANIZATION_ID,
    INVENTORY_ITEM_ID,
    OPEN_DEMAND,
    MAKE_BUY
  FROM XXEIS.EIS_XXWC_INV_LIST_TBL
/
