/*************************************************************************************************
$Header XXWC_PO_SEL_ITEMS_TBL.sql $

Module Name: XXWC_PO_SEL_ITEMS_TBL

PURPOSE: Table to maintain Vendor details.

REVISIONS:
Ver        Date         Author                Description
---------  -----------  ------------------    ----------------------------------------------
      1.0  01/12/2015   Kishorebabu V         TMS# 20151111-00201
                                              Added additional column to get vendor contact id details.
**************************************************************************************************/

ALTER TABLE XXWC.XXWC_PO_SEL_ITEMS_TBL ADD (vendor_contact_id NUMBER);
/