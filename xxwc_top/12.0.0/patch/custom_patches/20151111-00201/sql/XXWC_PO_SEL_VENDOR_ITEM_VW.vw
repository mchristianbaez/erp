/* Formatted on 1/10/2014 8:09:45 AM (QP5 v5.115.810.9015) */
CREATE OR REPLACE FORCE VIEW apps.xxwc_po_sel_vendor_item_vw (
   batch_id
 , vendor_id
 , vendor_number
 , vendor_name
 , vendor_site_id
 , vendor_site_code
 , vendor_min_dollar
 , freight_min_dollar
 , freight_min_uom
 , freight_terms_lookup_code
 , line_weight
 , unit_price
 , total_amount
 , organization_id
 , organization_code
 , organization_name
 , inventory_item_id
 , item_quantity
 , req_uom_code
 , segment1
 , description
 , unit_weight
 , amu
 , po_cost
 , on_hand_qty
 , item_uom_code
 , minimum_order_quantity
 , fixed_lot_multiplier
 , shelf_life
 , lead_time
 , calc_lead_time
 , velocity_class
 , target_level
 , max_level
 , open_demand
 , open_po
 , pricing_region
 , long_description
   -- 01/08/14 CG: TMS 20131212-00176: Added new Vendor Min fields
 , ppd_freight_units
 , notes
)
AS
   (SELECT	 xvm.batch_id
		   , NULL vendor_id
		   , NULL vendor_number
		   , NULL vendor_name
		   , NULL vendor_site_id
		   , NULL vendor_site_code
		   , NULL vendor_min_dollar
		   , NULL freight_min_dollar
		   , NULL freight_min_uom
		   , NULL freight_terms_lookup_code
		   ,						   --(msi.unit_weight * xvm.item_quantity)
			CASE
				WHEN NVL (br.attribute1, 'C') = 'W'
				THEN
				   (NVL (msi.attribute23, NVL (unit_weight, 0))
					* xvm.item_quantity)
				ELSE
				   (msi.unit_weight * xvm.item_quantity)
			 END
				line_weight
		   , xvm.unit_price
		   , xvm.total_amount
		   , xvm.organization_id
		   , org.organization_code
		   , org.organization_name
		   , xvm.inventory_item_id
		   , xvm.item_quantity
		   , xvm.uom_code req_uom_code
		   , msi.segment1
		   , msi.description
		   ,	  --msi.unit_weight , --Added for HI Freight Burden 06/07/2013
			CASE
				WHEN NVL (br.attribute1, 'C') = 'W'
				THEN
				   TO_NUMBER (
					  NVL (msi.attribute23, NVL (msi.unit_weight, 0))
				   )
				ELSE
				   msi.unit_weight
			 END
				unit_weight
		   , msi.attribute20 amu
		   , xxwc_ascp_scwb_pkg.get_po_cost (msi.inventory_item_id
										   , msi.organization_id
										   , xvm.vendor_id
										   , xvm.vendor_site_id)
				po_cost
		   , xxwc_ascp_scwb_pkg.get_on_hand (msi.inventory_item_id
										   , msi.organization_id
										   , 'H')
				on_hand_qty
		   , msi.primary_uom_code item_uom_code
		   , msi.minimum_order_quantity
		   , msi.fixed_lot_multiplier
		   , msi.shelf_life_days shelf_life
		   , NVL (msi.full_lead_time, 0) lead_time
		   , msi.attribute30 calc_lead_time
		   , xxwc_ascp_scwb_pkg.get_sales_velocity (msi.inventory_item_id
												  , msi.organization_id)
				velocity_class
		   , xxwc_ascp_scwb_pkg.get_inventory_level ('T'
												   , msi.inventory_item_id
												   , msi.organization_id)
				target_level
		   , xxwc_ascp_scwb_pkg.get_inventory_level ('M'
												   , msi.inventory_item_id
												   , msi.organization_id)
				max_level
		   , --20130604-00751 Added 06/25/2013 Open Demand, Open PO and Pricing Region
			 (xxwc_po_vendor_min_pkg.get_demand (
				 msi.inventory_item_id
			   , msi.organization_id
			   , --TO_DATE (fnd_profile.VALUE ('XXWC_VM_PLAN_HOR') ,'DD-MON-YYYY') ,
				TO_DATE (
					TO_CHAR (ADD_MONTHS (TRUNC (SYSDATE), 6), 'DD-MON-YYYY')
				 )
			   , 1
			   ,												  --P_NET_RSV,
				2
			   ,										   --P_INCLUDE_NONNET,
				2
			   ,											  --P_INCLUDE_WIP,
				1
			   ,												--P_NET_UNRSV,
				1
			   ,												  --P_NET_WIP,
				NULL
			  ))
				open_demand
		   ,													   -- P_SUBINV
			 -- 08/14/13 CG: TMS 20130801-01196: Updated to use function to pull Open PO + Open Internal Reqs
			 --<<Start>> 09/09/2015 Below condition commented by Pattabhi Avula for TMS#20150312-00146 
			/* (NVL (
				 xxwc_po_vendor_min_pkg.get_po_qty (
					TO_DATE(TO_CHAR (ADD_MONTHS (TRUNC (SYSDATE), 6)
								   , 'DD-MON-YYYY'))
				  , msi.organization_id
				  , msi.inventory_item_id
				  , 1
				  , 										  -- p_include_po,
				   2
				  , 									  -- p_include_nonnet,
				   2
				  , 										 -- p_include_wip,
				   2
				  , 										   --p_include_if,
				   NULL
				 )
			   , 0
			  )
			  + NVL (
				   xxwc_ascp_scwb_pkg.get_internal_req_qty (
					  msi.inventory_item_id
					, msi.organization_id
				   )
				 , 0
				)) */ --<<End>> 09/09/2015 commented by Pattabhi Avula for TMS#20150312-00146
				--<<Start>> 09/09/2015 Added Below condition by Pattabhi Avula for TMS#20150312-00146 
				NVL(
				  xxwc_inv_ais_pkg.get_onorder_qty (
				    msi.organization_id
					,msi.inventory_item_id
					 ),0
					) open_po  --<<End>> 09/09/2015 Added Pattabhi Avula for TMS#20150312-00146
		   , mp.attribute9 pricing_region
		   ,													   -- p_subinv
			msit.long_description
             -- 01/08/14 CG: TMS 20131212-00176: Added new Vendor Min fields
           , NULL ppd_freight_units
           , NULL notes
	  FROM	 xxwc_po_sel_items_tbl xvm
		   , org_organization_definitions org
		   , mtl_system_items msi
		   , bom_resources br
		   , mtl_parameters mp
		   , --20130604-00751 Added 06/25/2013 MTL_PARAMETERS to get Pricing Region
			mtl_system_items_tl msit
	 WHERE		 xvm.organization_id = org.organization_id
			 AND xvm.inventory_item_id = msi.inventory_item_id
			 AND xvm.organization_id = msi.organization_id
			 AND xvm.vendor_id IS NULL
			 AND org.organization_id = br.organization_id(+)
			 AND br.resource_code(+) = 'PO Freight'
			 AND org.organization_id = mp.organization_id --20130604-00751 Added 06/25/2013 join between org_organization_definitions to mtl_parameters
			 AND msi.inventory_item_id = msit.inventory_item_id
			 AND msi.organization_id = msit.organization_id
			 AND msit.language = 'US'
			 AND msit.source_lang = 'US'
	UNION ALL
	SELECT	 xvm.batch_id
		   , xvm.vendor_id
		   , pov.segment1 vendor_number
		   , pov.vendor_name
		   , xvm.vendor_site_id
		   , xvm.vendor_site_code
		   , xxwc_ascp_scwb_pkg.get_vendor_info ('VMD'
											   , xvm.vendor_id
											   , xvm.vendor_site_id
											   , xvm.organization_id
											   , xvm.vendor_contact_id  ---TMS 20151111-00201 by Kishorebabu V on 24-DEC-2015
											   )
				vendor_min_dollar
		   , xxwc_ascp_scwb_pkg.get_vendor_info ('FMD'
											   , xvm.vendor_id
											   , xvm.vendor_site_id
											   , xvm.organization_id
                                               , xvm.vendor_contact_id  ---TMS 20151111-00201 by Kishorebabu V on 24-DEC-2015
											   )
				freight_min_dollar
		   , xxwc_ascp_scwb_pkg.get_vendor_info ('FMU'
											   , xvm.vendor_id
											   , xvm.vendor_site_id
											   , xvm.organization_id
											   , xvm.vendor_contact_id  ---TMS 20151111-00201 by Kishorebabu V on 24-DEC-2015
											   )
				freight_min_uom
		   , pov.freight_terms_lookup_code
		   ,			 --(msi.unit_weight * xvm.item_quantity) line_weight ,
			CASE
				WHEN NVL (br.attribute1, 'C') = 'W'
				THEN
				   (NVL (msi.attribute23, NVL (unit_weight, 0))
					* xvm.item_quantity)
				ELSE
				   (msi.unit_weight * xvm.item_quantity)
			 END
				line_weight
		   , xvm.unit_price
		   , xvm.total_amount
		   , xvm.organization_id
		   , org.organization_code
		   , org.organization_name
		   , xvm.inventory_item_id
		   , xvm.item_quantity
		   , xvm.uom_code req_uom_code
		   , msi.segment1
		   , msi.description
		   ,											   --msi.unit_weight ,
			CASE
				WHEN NVL (br.attribute1, 'C') = 'W'
				THEN
				   TO_NUMBER (
					  NVL (msi.attribute23, NVL (msi.unit_weight, 0))
				   )
				ELSE
				   msi.unit_weight
			 END
				unit_weight
		   , msi.attribute20 amu
		   , xxwc_ascp_scwb_pkg.get_po_cost (msi.inventory_item_id
										   , msi.organization_id
										   , xvm.vendor_id
										   , xvm.vendor_site_id)
				po_cost
		   , xxwc_ascp_scwb_pkg.get_on_hand (msi.inventory_item_id
										   , msi.organization_id
										   , 'H')
				on_hand_qty
		   , msi.primary_uom_code item_uom_code
		   , msi.minimum_order_quantity
		   , msi.fixed_lot_multiplier
		   , msi.shelf_life_days shelf_life
		   , NVL (msi.full_lead_time, 0) lead_time
		   , msi.attribute30 calc_lead_time
		   , xxwc_ascp_scwb_pkg.get_sales_velocity (msi.inventory_item_id
												  , msi.organization_id)
				velocity_class
		   , xxwc_ascp_scwb_pkg.get_inventory_level ('T'
												   , msi.inventory_item_id
												   , msi.organization_id)
				target_level
		   , xxwc_ascp_scwb_pkg.get_inventory_level ('M'
												   , msi.inventory_item_id
												   , msi.organization_id)
				max_level
		   , --20130604-00751 Added 06/25/2013 Open Demand, Open PO and Pricing Region
			 (xxwc_po_vendor_min_pkg.get_demand (
				 msi.inventory_item_id
			   , msi.organization_id
			   , --TO_DATE (fnd_profile.VALUE ('XXWC_VM_PLAN_HOR') ,'DD-MON-YYYY') ,
				TO_DATE (
					TO_CHAR (ADD_MONTHS (TRUNC (SYSDATE), 6), 'DD-MON-YYYY')
				 )
			   , 1
			   ,												  --P_NET_RSV,
				2
			   ,										   --P_INCLUDE_NONNET,
				2
			   ,											  --P_INCLUDE_WIP,
				1
			   ,												--P_NET_UNRSV,
				1
			   ,												  --P_NET_WIP,
				NULL
			  ))
				open_demand
		   ,													   -- P_SUBINV
			 -- 08/14/13 CG: TMS 20130801-01196: Updated to use AIS function to pull Open PO + Open Internal Reqs
			 --<<Start>> 09/09/2015 Below condition commented by Pattabhi Avula for TMS#20150312-00146 
			 /*(NVL (
				 xxwc_po_vendor_min_pkg.get_po_qty (
					TO_DATE(TO_CHAR (ADD_MONTHS (TRUNC (SYSDATE), 6)
								   , 'DD-MON-YYYY'))
				  , msi.organization_id
				  , msi.inventory_item_id
				  , 1
				  , 										  -- p_include_po,
				   2
				  , 									  -- p_include_nonnet,
				   2
				  , 										 -- p_include_wip,
				   2
				  , 										   --p_include_if,
				   NULL
				 )
			   , 0
			  )
			  + NVL (
				   xxwc_ascp_scwb_pkg.get_internal_req_qty (
					  msi.inventory_item_id
					, msi.organization_id
				   )
				 , 0
				))
				*/ --<<End>> 09/09/2015 commented by Pattabhi Avula for TMS#20150312-00146
				--<<Start>> 09/09/2015 Added Below condition by Pattabhi Avula for TMS#20150312-00146 
               NVL(
				  xxwc_inv_ais_pkg.get_onorder_qty (
				    msi.organization_id
					,msi.inventory_item_id
					 ),0
					)
				open_po   --<<End>> 09/09/2015 Added Pattabhi Avula for TMS#20150312-00146
				,													   -- p_subinv
			mp.attribute9 pricing_region
		   , msit.long_description
            -- 01/08/14 CG: TMS 20131212-00176: Added new Vendor Min fields
           , xxwc_ascp_scwb_pkg.get_vendor_info ('PPDU'
                                               , xvm.vendor_id
                                               , xvm.vendor_site_id
                                               , xvm.organization_id
											   , xvm.vendor_contact_id  ---TMS 20151111-00201 by Kishorebabu V on 24-DEC-2015
											   ) ppd_freight_units
           , substr((xxwc_ascp_scwb_pkg.get_vendor_minimum_notes(   xvm.vendor_id
                                                                   , xvm.vendor_site_id
                                                                   , xvm.organization_id
																   , xvm.vendor_contact_id  ---TMS 20151111-00201 by Kishorebabu V on 21-DEC-2015
																   )),1,4000) notes
	  FROM	 xxwc_po_sel_items_tbl xvm
		   , org_organization_definitions org
		   , mtl_system_items msi
		   , ap_suppliers pov
		   , bom_resources br
		   , mtl_parameters mp
		   , --20130604-00751 Added 06/25/2013 MTL_PARAMETERS to get Pricing Region
			mtl_system_items_tl msit
	 WHERE		 xvm.organization_id = org.organization_id
			 AND xvm.inventory_item_id = msi.inventory_item_id
			 AND xvm.organization_id = msi.organization_id
			 AND xvm.vendor_id = pov.vendor_id
			 AND xvm.vendor_id IS NOT NULL
			 AND org.organization_id = br.organization_id(+)
			 AND br.resource_code(+) = 'PO Freight'
			 AND org.organization_id = mp.organization_id --20130604-00751 Added 06/25/2013 join between org_organization_definitions to mtl_parameters
			 AND msi.inventory_item_id = msit.inventory_item_id
			 AND msi.organization_id = msit.organization_id
			 AND msit.language = 'US'
			 AND msit.source_lang = 'US');