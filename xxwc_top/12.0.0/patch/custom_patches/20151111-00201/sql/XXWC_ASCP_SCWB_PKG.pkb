CREATE OR REPLACE PACKAGE BODY      xxwc_ascp_scwb_pkg
/****************************************************************************************************************************************************
*   $Header xxwc_ascp_scwb_pkg $
*   Module Name: xxwc_ascp_scwb_pkg
*
*   PURPOSE:   Package used in DB views for Supply Chain Workbench
*
*   REVISIONS:
*   Ver        Date        Author                     Description
*   ---------  ----------  ---------------         -------------------------
*   1.0        04/01/2012  Shankar Hariharan       Initial Version
*   1.1        05/27/2014  Lee Spitzer             20130917-00676 - Vendor Cost Improvements - Added Function get_vendor_contact_minimum
*   1.2        08/13/2014  Lee Spitzer             20140726-00008  Update Vendor min Web ADI to download/ update default contact @SC Update Vendor min Web ADI to download/ update default contact
*   1.3        10/28/2014  Vijaysrinivasan         TMS#20141002-00041 Multi org changes
*   1.4        05/14/2015  P.Vamshidhar            TMS#20150515-00055 - Implement fixes to PO Vendor minimum WebADI upload
*   1.5        12/01/2015  Kishorebabu V           TMS#20151111-00201  Vendor Minimum notes not populating on PO hover over or Purch Info form
******************************************************************************************************************************************************/
AS
   -- Added 1.2 8/13/2014 Error DEBUG Handling
   g_exception       EXCEPTION;
   g_err_msg         VARCHAR2 (2000);
   g_err_callfrom    VARCHAR2 (175) := 'XXWC_ASCP_SCWB_PKG';
   g_err_callpoint   VARCHAR2 (175) := 'START';
   g_distro_list     VARCHAR2 (80) := 'OracleDevelopmentGroup@hdsupply.com';
   g_module          VARCHAR2 (80);


   -- This function is used to determine the White Cap Org id as
   -- MO: Operating unit profile is not setup anymore
   FUNCTION get_wc_org_id
      RETURN NUMBER
   IS
      l_org_id   NUMBER;
   BEGIN
      l_org_id := fnd_profile.VALUE ('DEFAULT_ORG_ID');

      IF l_org_id IS NULL
      THEN
         BEGIN
            SELECT organization_id
              INTO l_org_id
              FROM hr_all_organization_units
             WHERE name = 'HDS White Cap - Org';
         EXCEPTION
            WHEN OTHERS
            THEN
               l_org_id := 162;
         END;
      END IF;

      RETURN (l_org_id);
   END get_wc_org_id;

   PROCEDURE PROCCOMMIT
   IS
   -- Used in SC Workbench from to do a silent commit to avoid additional pop up
   BEGIN
      COMMIT;
   END proccommit;

   FUNCTION get_sales_velocity (i_inventory_item_id   IN NUMBER,
                                i_organization_id     IN NUMBER)
      RETURN VARCHAR
   --Used in XXWC_PO_SEL_VENDOR_ITEM_VW as a part of the SC Workbench to display Velocity information
   IS
      l_cs_id            NUMBER;
      l_sales_velocity   VARCHAR2 (10);
   BEGIN
      SELECT category_set_id
        INTO l_cs_id
        FROM MTL_CATEGORY_SETS_V
       WHERE category_set_name = 'Sales Velocity';

      SELECT category_concat_segs
        INTO l_sales_velocity
        FROM MTL_ITEM_CATEGORIES_V
       WHERE     inventory_item_id = i_inventory_item_id
             AND organization_id = i_organization_id
             AND category_set_id = l_cs_id;

      RETURN (l_sales_velocity);
   EXCEPTION
      WHEN OTHERS
      THEN
         RETURN NULL;
   END get_sales_velocity;

   FUNCTION get_po_cost (i_inventory_item_id   IN NUMBER,
                         i_organization_id     IN NUMBER,
                         i_vendor_id           IN NUMBER,
                         i_vendor_site_id      IN NUMBER)
      RETURN NUMBER
   --Used in XXWC_PO_SEL_VENDOR_ITEM_VW as a part of the SC Workbench to display PO Cost information
   IS
      l_po_cost   NUMBER;
   BEGIN
      /*
         SELECT b.unit_price
           INTO l_po_cost
           FROM po_headers a, po_lines b, po_line_locations_all c
          WHERE     a.type_lookup_code = 'BLANKET'
                AND a.enabled_flag = 'Y'
                AND a.authorization_status = 'APPROVED'
                AND a.approved_flag = 'Y'
                AND a.po_header_id = b.po_header_id
                AND NVL (b.cancel_flag, 'N') = 'N'
                AND NVL (b.closed_code, 'x') <> 'CLOSED'
                AND NVL (b.expiration_date, SYSDATE + 1) > SYSDATE
                AND b.item_id = i_inventory_item_id
                AND a.vendor_id = i_vendor_id
                AND a.vendor_site_id = NVL (i_vendor_site_id, a.vendor_site_id)
                AND b.po_header_id = c.po_header_id
                AND b.po_line_id = c.po_line_id
                AND c.ship_to_organization_id =
                       NVL (i_organization_id, c.ship_to_organization_id)
                AND NVL (c.approved_flag, 'Y') = 'Y'
                AND NVL (b.cancel_flag, 'N') = 'N';
                */

      -- 04/25/2013 TMS 20130424-01825: CG Updated select statement to not match organization_id based on ship to location
      -- since they use global BPAs.
      -- Function used by:
      -- XXWC_PO_SEL_VENDOR_ITEM_VW as a part of the SC Workbench to display PO Cost information
      -- XXWC_PO_VENDOR_MIN_ITEM_DTL_VW as part of the Vendor Minimum form for PO Cost Data
      -- AIS

      BEGIN
         -- 1. Check for Local BPA
         SELECT b.unit_price
           INTO l_po_cost
           --Modified the below line for TMS ## TMS#20141002-00041 by VijaySrinivasan  on 10/28/2014
           FROM po_headers a, po_lines b, po_ship_to_loc_org_v c
          WHERE     a.type_lookup_code = 'BLANKET'
                AND a.enabled_flag = 'Y'
                AND a.authorization_status = 'APPROVED'
                AND a.approved_flag = 'Y'
                AND a.po_header_id = b.po_header_id
                AND NVL (b.cancel_flag, 'N') = 'N'
                AND NVL (b.closed_code, 'x') <> 'CLOSED'
                AND NVL (b.expiration_date, SYSDATE + 1) > SYSDATE
                AND b.item_id = i_inventory_item_id
                AND a.vendor_id = i_vendor_id
                AND a.vendor_site_id =
                       NVL (i_vendor_site_id, a.vendor_site_id)
                AND a.ship_to_location_id = c.ship_to_location_id
                AND c.inventory_organization_id =
                       NVL (i_organization_id, c.inventory_organization_id)
                AND NVL (b.cancel_flag, 'N') = 'N'
                -- 04/25/2013 TMS 20130424-01825: Checking only local BPAs
                AND NVL (a.global_agreement_flag, 'N') = 'N'
                AND b.creation_date =
                       (SELECT MAX (b.creation_date)
                          FROM po_headers a,
                               po_lines b,
                               po_ship_to_loc_org_v c
                         WHERE     a.type_lookup_code = 'BLANKET'
                               AND a.enabled_flag = 'Y'
                               AND a.authorization_status = 'APPROVED'
                               AND a.approved_flag = 'Y'
                               AND a.po_header_id = b.po_header_id
                               AND NVL (b.cancel_flag, 'N') = 'N'
                               AND NVL (b.closed_code, 'x') <> 'CLOSED'
                               AND NVL (b.expiration_date, SYSDATE + 1) >
                                      SYSDATE
                               AND b.item_id = i_inventory_item_id
                               AND a.vendor_id = i_vendor_id
                               AND a.vendor_site_id =
                                      NVL (i_vendor_site_id,
                                           a.vendor_site_id)
                               AND a.ship_to_location_id =
                                      c.ship_to_location_id
                               AND c.inventory_organization_id =
                                      NVL (i_organization_id,
                                           c.inventory_organization_id)
                               AND NVL (b.cancel_flag, 'N') = 'N'
                               -- 04/25/2013 TMS 20130424-01825: Checking only local BPAs
                               AND NVL (a.global_agreement_flag, 'N') = 'N');
      EXCEPTION
         WHEN OTHERS
         THEN
            BEGIN
               -- 2. Check for Global BPA
               SELECT b.unit_price
                 INTO l_po_cost
                 FROM po_headers a, po_lines b
                -- 04/25/2013 TMS 20130424-01825
                -- , po_ship_to_loc_org_v c
                WHERE     a.type_lookup_code = 'BLANKET'
                      AND a.enabled_flag = 'Y'
                      AND a.authorization_status = 'APPROVED'
                      AND a.approved_flag = 'Y'
                      AND a.po_header_id = b.po_header_id
                      AND NVL (b.cancel_flag, 'N') = 'N'
                      AND NVL (b.closed_code, 'x') <> 'CLOSED'
                      AND NVL (b.expiration_date, SYSDATE + 1) > SYSDATE
                      AND b.item_id = i_inventory_item_id
                      AND a.vendor_id = i_vendor_id
                      AND a.vendor_site_id =
                             NVL (i_vendor_site_id, a.vendor_site_id)
                      -- 04/25/2013 TMS 20130424-01825
                      --AND a.ship_to_location_id = c.ship_to_location_id
                      --AND c.inventory_organization_id = NVL (i_organization_id, c.inventory_organization_id)
                      AND NVL (b.cancel_flag, 'N') = 'N'
                      -- 04/25/2013 TMS 20130424-01825: Checking only Global BPAs
                      AND NVL (a.global_agreement_flag, 'N') = 'Y'
                      AND b.creation_date =
                             (SELECT MAX (b.creation_date)
                                FROM po_headers a, po_lines b
                               -- 04/25/2013 TMS 20130424-01825
                               --, po_ship_to_loc_org_v c
                               WHERE     a.type_lookup_code = 'BLANKET'
                                     AND a.enabled_flag = 'Y'
                                     AND a.authorization_status = 'APPROVED'
                                     AND a.approved_flag = 'Y'
                                     AND a.po_header_id = b.po_header_id
                                     AND NVL (b.cancel_flag, 'N') = 'N'
                                     AND NVL (b.closed_code, 'x') <> 'CLOSED'
                                     AND NVL (b.expiration_date, SYSDATE + 1) >
                                            SYSDATE
                                     AND b.item_id = i_inventory_item_id
                                     AND a.vendor_id = i_vendor_id
                                     AND a.vendor_site_id =
                                            NVL (i_vendor_site_id,
                                                 a.vendor_site_id)
                                     -- 04/25/2013 TMS 20130424-01825
                                     --AND a.ship_to_location_id = c.ship_to_location_id
                                     --AND c.inventory_organization_id = NVL (i_organization_id , c.inventory_organization_id)
                                     AND NVL (b.cancel_flag, 'N') = 'N'
                                     -- 04/25/2013 TMS 20130424-01825: Checking only local BPAs
                                     AND NVL (a.global_agreement_flag, 'N') =
                                            'Y');
            EXCEPTION
               WHEN OTHERS
               THEN
                  BEGIN
                     -- 3. Item List Price by Org
                     SELECT list_price_per_unit
                       INTO l_po_cost
                       FROM mtl_system_items
                      WHERE     inventory_item_id = i_inventory_item_id
                            AND organization_id = i_organization_id;

                     RETURN (l_po_cost);
                  EXCEPTION
                     WHEN OTHERS
                     THEN
                        l_po_cost := 0;
                  END;
            END;
      END;

      RETURN (l_po_cost);
   EXCEPTION
      WHEN OTHERS
      THEN
         -- Added 09-Apr-2012 Shankar Hariharan
         -- To return list price if no PO Cost is available
         BEGIN
            SELECT list_price_per_unit
              INTO l_po_cost
              FROM mtl_system_items
             WHERE     inventory_item_id = i_inventory_item_id
                   AND organization_id = i_organization_id;

            RETURN (l_po_cost);
         EXCEPTION
            WHEN OTHERS
            THEN
               RETURN (0);
         END;
   END get_po_cost;


   -- Get Vendor Minimum is used in Auto Create PO Personalization call to display
   -- vendor minimum information in the Window header
   -- Get Vendor Minimum is used in Auto Create PO Personalization call to display
   -- vendor minimum information in the Window header
   FUNCTION get_vendor_minimum (i_vendor_id         IN NUMBER,
                                i_vendor_site_id    IN NUMBER,
                                i_organization_id   IN NUMBER,
								i_vendor_contact_id IN NUMBER DEFAULT NULL) --- Ver 1.5 as per TMS# 20151111-00201 Added new Column i_vendor_contact_id
      RETURN VARCHAR
   IS
      l_disp_info          VARCHAR2 (400);
      v_vendor_site_code   VARCHAR2 (255);
   BEGIN
      -- 12/10/13 CG: TMS 20130904-00654: Updated to Add new field, nomenclature and spacing
      --10 April 2014 added note ' Prepaid values have not been entered for this supplier vendor site' when vendor information not found in custom
      --vendor minimum table - by Rasikha
      BEGIN
         SELECT ss.vendor_site_code
           INTO v_vendor_site_code
           --Modified the below line for TMS ## TMS#20141002-00041 by VijaySrinivasan  on 10/28/2014
           FROM ap_supplier_sites ss
          WHERE     ss.vendor_id = i_vendor_id
                AND ss.vendor_site_id = i_vendor_site_id;
      EXCEPTION
         WHEN OTHERS
         THEN
            v_vendor_site_code := '';
      END;

        SELECT    '-MINORD$ = '
             || vendor_min_dollar
             || '        PPD$ = '
             || freight_min_dollar
             || '        PPD# = '
             || freight_min_uom
             -- 12/10/13 CG: TMS 20130904-00654: New Field
             || '        PPDU = '
             || ppd_freight_units
        INTO l_disp_info
        FROM  xxwc_po_vendor_minimum
        WHERE vendor_id = i_vendor_id
        AND   vendor_site_id = i_vendor_site_id
        AND   organization_id = i_organization_id
        AND   NVL(vendor_contact_id,1) = NVL(i_vendor_contact_id,1);  --- Ver 1.5 as per TMS# 20151111-00201 Added new condition;

      RETURN (l_disp_info);
   EXCEPTION
      WHEN OTHERS
      THEN
         RETURN (   '-Prepaid values have not been entered for this supplier site: '
                 || v_vendor_site_code
                 || ' at this Org.');
   END get_vendor_minimum;


   FUNCTION get_vendor_info (i_return_type       IN VARCHAR,
                             i_vendor_id         IN NUMBER,
                             i_vendor_site_id    IN NUMBER,
                             i_organization_id   IN NUMBER,
							 i_vendor_contact_id IN NUMBER) --- Ver 1.5 as per TMS# 20151111-00201 Added new Column i_vendor_contact_id)
      RETURN NUMBER
   --Used in XXWC_PO_SEL_VENDOR_ITEM_VW as a part of the SC Workbench to display on hand information
   IS
      l_ven_min     NUMBER;
      l_frt_min     NUMBER;
      l_frt_uom     NUMBER;
      -- 01/08/14 CG: TMS 20131212-00176: Added to retrieve PPD Units
      l_ppd_units   NUMBER;
   BEGIN
      BEGIN
         SELECT vendor_min_dollar,
                freight_min_dollar,
                freight_min_uom,
                -- 01/08/14 CG: TMS 20131212-00176: Added to retrieve PPD Units
                ppd_freight_units
           INTO l_ven_min,
                l_frt_min,
                l_frt_uom,
                -- 01/08/14 CG: TMS 20131212-00176: Added to retrieve PPD Units
                l_ppd_units
           FROM xxwc_po_vendor_minimum
          WHERE     vendor_id = i_vendor_id
                AND vendor_site_id = i_vendor_site_id
                AND organization_id = i_organization_id
				AND   NVL(vendor_contact_id,1) = NVL(i_vendor_contact_id,1);  --- Ver 1.5 as per TMS# 20151111-00201 Added new condition;
      EXCEPTION
         WHEN OTHERS
         THEN
            BEGIN
               SELECT vendor_min_dollar,
                      freight_min_dollar,
                      freight_min_uom,
                      -- 01/08/14 CG: TMS 20131212-00176: Added to retrieve PPD Units
                      ppd_freight_units
                 INTO l_ven_min,
                      l_frt_min,
                      l_frt_uom,
                      -- 01/08/14 CG: TMS 20131212-00176: Added to retrieve PPD Units
                      l_ppd_units
                 FROM xxwc_po_vendor_minimum
                WHERE     vendor_id = i_vendor_id
                      AND organization_id = i_organization_id
					  AND   NVL(vendor_contact_id,1) = NVL(i_vendor_contact_id,1);  --- Ver 1.5 as per TMS# 20151111-00201 Added new condition;
            EXCEPTION
               WHEN OTHERS
               THEN
                  l_ven_min := -1;
                  l_frt_min := -1;
                  l_frt_uom := -1;
                  -- 01/08/14 CG: TMS 20131212-00176: Added to retrieve PPD Units
                  l_ppd_units := -1;
            END;
      END;

      IF i_return_type = 'VMD'
      THEN
         RETURN (l_ven_min);
      ELSIF i_return_type = 'FMD'
      THEN
         RETURN (l_frt_min);
      ELSIF i_return_type = 'FMU'
      THEN
         RETURN (l_frt_uom);
      -- 01/08/14 CG: TMS 20131212-00176: Added to retrieve PPD Units
      ELSIF i_return_type = 'PPDU'
      THEN
         RETURN (l_ppd_units);
      END IF;
   END get_vendor_info;

   /*
      FUNCTION get_on_hand (i_inventory_item_id   IN NUMBER
                                 , i_organization_id     IN NUMBER)
         RETURN NUMBER
      IS
      l_on_hand number;
      BEGIN
        select sum(transaction_quantity)
          into l_on_hand
          from mtl_onhand_quantities
         where inventory_item_id = i_inventory_item_id
           and organization_id = i_organization_id;
         return(nvl(l_on_hand,0));
      END get_on_hand;
   */

   FUNCTION get_on_hand (i_inventory_item_id   IN NUMBER,
                         i_organization_id     IN NUMBER,
                         i_return_type         IN VARCHAR)
      RETURN NUMBER
   --Used in XXWC_PO_SEL_VENDOR_ITEM_VW as a part of the SC Workbench to display on hand information
   -- Included RQ as a return type for reservable qoh TMS task 20130107-00851 17-Jan-2013 Shankar
   IS
      qoh                NUMBER;
      rqoh               NUMBER;
      qr                 NUMBER;
      qs                 NUMBER;
      att                NUMBER;
      atr                NUMBER;
      ls                 VARCHAR2 (1);
      mc                 NUMBER;
      md                 VARCHAR2 (1000);
      l_lot_control      BOOLEAN;
      l_serial_control   BOOLEAN;
      l_lc_code          NUMBER;
      l_sn_code          NUMBER;
      l_lot_exp_date     DATE;
      l_subinv_code      VARCHAR2 (30);        -- Added by Shankar 08-Mar-2013
      l_item_type        VARCHAR2 (100);
   BEGIN
      inv_quantity_tree_pub.clear_quantity_cache;

      SELECT lot_control_code, serial_number_control_code, item_type
        INTO l_lc_code, l_sn_code, l_item_type
        FROM mtl_system_items
       WHERE     inventory_item_id = i_inventory_item_id
             AND organization_id = i_organization_id;

      IF l_lc_code = 1
      THEN
         l_lot_control := FALSE;
      ELSE
         l_lot_control := TRUE;

         IF i_return_type IN ('R', 'RQ', 'G')
         THEN
            l_lot_exp_date := SYSDATE;
         END IF;
      END IF;

      IF l_sn_code = 1
      THEN
         l_serial_control := FALSE;
      ELSE
         l_serial_control := TRUE;
      END IF;

      -- Added by Shankar 08-Mar-2013
      IF i_return_type = 'GP'
      THEN
         l_subinv_code := 'GeneralPCK';
      ELSIF i_return_type = 'G'
      THEN
         IF l_item_type IN ('RENTAL', 'RE_RENT')
         THEN
            l_subinv_code := 'Rental';
         ELSE
            IF l_lot_control = FALSE
            THEN
               l_subinv_code := 'General';
            END IF;
         END IF;
      ELSE
         l_subinv_code := NULL;
      END IF;

      -- Added by Shankar 08-Mar-2013 End


      inv_quantity_tree_pub.query_quantities (
         p_api_version_number    => 1.0,
         p_init_msg_lst          => 'T',
         x_return_status         => ls,
         x_msg_count             => mc,
         x_msg_data              => md,
         p_organization_id       => i_organization_id,
         p_inventory_item_id     => i_inventory_item_id,
         p_tree_mode             => 1                     --'Reservation Mode'
                                     ,
         p_is_revision_control   => FALSE,
         p_is_lot_control        => l_lot_control,
         p_is_serial_control     => l_serial_control,
         p_revision              => NULL,
         p_lot_number            => NULL,
         p_lot_expiration_date   => l_lot_exp_date,
         p_subinventory_code     => l_subinv_code,
         p_locator_id            => NULL,
         x_qoh                   => qoh,
         x_rqoh                  => rqoh,
         x_qr                    => qr,
         x_qs                    => qs,
         x_att                   => att,
         x_atr                   => atr);

      IF i_return_type = 'H'
      THEN
         RETURN (qoh);
      ELSIF i_return_type = 'R'
      THEN
         RETURN (atr);
      ELSIF i_return_type IN ('RQ', 'GP', 'G') -- Added by Shankar 08-Mar-2013 -- 14-May-2013 New return type of G
      THEN
         RETURN (rqoh);
      ELSIF i_return_type = 'T'
      THEN
         RETURN (att);
      END IF;
   END get_on_hand;


   FUNCTION get_inventory_level (i_level_type          IN VARCHAR --T(Target) or M(Max)
                                                                 ,
                                 i_inventory_item_id   IN NUMBER,
                                 i_organization_id     IN NUMBER)
      RETURN NUMBER
   IS
      --Used in XXWC_PO_SEL_VENDOR_ITEM_VW as a part of the SC Workbench to display target information
      l_plan_id        NUMBER := fnd_profile.VALUE ('XXWC_ASCP_WCWB_PLAN');
      l_target_level   NUMBER;
      l_max_level      NUMBER;
   BEGIN
      /*
         SELECT NVL (a.target_quantity, 0), NVL (a.max_quantity, 0)
           INTO l_target_level, l_max_level
           FROM msc_inventory_levels_v a, msc_system_items b
          WHERE     b.sr_inventory_item_id = i_inventory_item_id
                AND b.organization_id = i_organization_id
                AND b.inventory_item_id = a.inventory_item_id
                AND b.organization_id = a.organization_id
                AND a.plan_id = l_plan_id
                AND a.plan_id=b.plan_id
                AND a.period_start_date =
                       (SELECT MAX (period_start_date)
                          FROM MSC_inventory_levels_v c
                         WHERE     c.inventory_item_id = a.inventory_item_id
                               AND c.organization_id = a.organization_id
                               AND c.plan_id = a.plan_id
                               AND SYSDATE >= period_start_date);
                               */
      SELECT NVL (min_minmax_quantity, 0), NVL (max_minmax_quantity, 0)
        INTO l_target_level, l_max_level
        FROM mtl_system_items
       WHERE     inventory_item_id = i_inventory_item_id
             AND organization_id = i_organization_id;

      IF i_level_type = 'T'
      THEN
         RETURN (l_target_level);
      ELSE
         RETURN (l_max_level);
      END IF;
   EXCEPTION
      WHEN OTHERS
      THEN
         RETURN (-1);
   END get_inventory_level;


   FUNCTION get_on_hand_gt_n (i_inventory_item_id   IN NUMBER,
                              i_organization_id     IN NUMBER,
                              i_no_of_days          IN NUMBER)
      RETURN NUMBER
   IS
      -- Used in SCWB to display aged inventory
      l_oh_qty   NUMBER;
   BEGIN
      SELECT NVL (SUM (primary_transaction_quantity), 0)
        INTO l_oh_qty
        FROM XXWCINV_INV_ONHAND_MV
       WHERE     inventory_item_id = i_inventory_item_id
             AND organization_id = i_organization_id
             AND TRUNC (orig_date_received) < TRUNC (SYSDATE - i_no_of_days);

      RETURN (l_oh_qty);
   END get_on_hand_gt_n;

   -- 12/09/CG: TMS 20130904-00654: Added 2 new Columns i_ppd_freight_units and i_notes
   PROCEDURE load_vendor_minimum (i_vendor_num          IN VARCHAR2,
                                  -- i_org_code     IN NUMBER,
                                  i_org_code            IN VARCHAR2,
                                  i_vendor_min          IN NUMBER,
                                  i_freight_min         IN NUMBER,
                                  i_freight_min_uom     IN NUMBER,
                                  i_ppd_freight_units   IN NUMBER,
                                  i_notes               IN VARCHAR2)
   IS
      lv_message          VARCHAR2 (200);
      lv_vendor_num       NUMBER;
      lv_site_num         NUMBER;
      l_exists            VARCHAR2 (1);

      l_organization_id   NUMBER;
   BEGIN
      IF i_vendor_num IS NULL
      THEN
         lv_message := 'Vendor Information is required';
         RAISE_APPLICATION_ERROR (-20001, lv_message);
      END IF;

      IF i_org_code IS NULL
      THEN
         lv_message := 'Branch Information is required';
         RAISE_APPLICATION_ERROR (-20001, lv_message);
      ELSE
         -- 12/10/13 CG:TMS 20130904-00654: Added to address org code bug
         BEGIN
            SELECT organization_id
              INTO l_organization_id
              FROM mtl_parameters
             WHERE organization_code = i_org_code;
         EXCEPTION
            WHEN OTHERS
            THEN
               lv_message := 'Invalid Branch';
               RAISE_APPLICATION_ERROR (-20001, lv_message);
         END;
      END IF;

      IF    NVL (i_vendor_min, 0) < 0
         OR NVL (i_freight_min, 0) < 0
         OR NVL (i_freight_min_uom, 0) < 0
         -- 12/09/CG: TMS 20130904-00654: Added condition to not allow negative PPDU
         OR NVL (i_ppd_freight_units, 0) < 0
      THEN
         lv_message := 'Vendor / Freight Minimum should be a positive value';
         RAISE_APPLICATION_ERROR (-20001, lv_message);
      END IF;

      --lv_message := '1';
      SELECT SUBSTR (i_vendor_num, 1, INSTR (i_vendor_num, '-', 1) - 1)
        INTO lv_vendor_num
        FROM DUAL;

      --lv_message := '2';
      SELECT SUBSTR (i_vendor_num, INSTR (i_vendor_num, '-', 1) + 1)
        INTO lv_site_num
        FROM DUAL;

      --lv_message := '3';
      IF lv_site_num = 0
      THEN
         lv_site_num := NULL;
      END IF;


      BEGIN
         --lv_message := '4';
         SELECT 'x'
           INTO l_exists
           FROM xxwc_po_vendor_minimum
          WHERE     vendor_id = lv_vendor_num
                AND NVL (vendor_site_id, -1) = NVL (lv_site_num, -1)
                AND organization_id = l_organization_id;         --i_org_code;

         --  lv_message := '5';
         UPDATE xxwc_po_vendor_minimum
            SET vendor_min_dollar = NVL (i_vendor_min, 0),
                freight_min_dollar = NVL (i_freight_min, 0),
                freight_min_uom = NVL (i_freight_min_uom, 0),
                last_update_date = SYSDATE,
                last_updated_by = fnd_global.user_id,
                last_update_login = FND_GLOBAL.LOGIN_ID,
                -- 12/09/CG: TMS 20130904-00654: Added Update of two new columns
                ppd_freight_units = NVL (i_ppd_freight_units, 0),
                notes = i_notes
          WHERE     vendor_id = lv_vendor_num
                AND NVL (vendor_site_id, -1) = NVL (lv_site_num, -1)
                AND organization_id = l_organization_id;        -- i_org_code;
      EXCEPTION
         WHEN NO_DATA_FOUND
         THEN
            --lv_message := '6';
            INSERT INTO xxwc_po_vendor_minimum (vendor_id,
                                                vendor_site_id,
                                                organization_id,
                                                vendor_min_dollar,
                                                freight_min_dollar,
                                                freight_min_uom,
                                                creation_date,
                                                created_by,
                                                last_update_date,
                                                last_updated_by,
                                                last_update_login,
                                                -- 12/09/CG: TMS 20130904-00654: Added Insert of two new columns
                                                ppd_freight_units,
                                                notes)
                 VALUES (lv_vendor_num,
                         lv_site_num,
                         l_organization_id                      /*i_org_code*/
                                          ,
                         NVL (i_vendor_min, 0),
                         NVL (i_freight_min, 0),
                         NVL (i_freight_min_uom, 0),
                         SYSDATE,
                         fnd_global.user_id,
                         SYSDATE,
                         fnd_global.user_id,
                         fnd_global.login_id,
                         -- 12/09/CG: TMS 20130904-00654: Added Insert of two new columns
                         NVL (i_ppd_freight_units, 0),
                         i_notes);
      END;

      COMMIT;
   EXCEPTION
      WHEN OTHERS
      THEN
         lv_message := lv_message; --||' Unhandled Exception '; --||i_vendor_num||' '||i_org_code;
         RAISE_APPLICATION_ERROR (-20001, lv_message);
   END load_vendor_minimum;

   -- 08/14/2013 CG: TMS 20130801-01196: Added to pull internal req quantities
   FUNCTION get_internal_req_qty (i_inventory_item_id   IN NUMBER,
                                  i_organization_id     IN NUMBER)
      RETURN NUMBER
   IS
      l_req_qty   NUMBER;
   BEGIN
      SELECT NVL (
                SUM (
                     (  b.quantity
                      - NVL (b.quantity_received, 0)
                      - NVL (b.quantity_cancelled, 0))
                   * po_uom_s.po_uom_convert (d.unit_of_measure,
                                              c.primary_unit_of_measure,
                                              b.item_id)),
                0)                   -- Shankar TMS 20130124-00974 23-Jan-2013
        INTO l_req_qty
        --Modified the below 2 lines for TMS ## TMS#20141002-00041 by VijaySrinivasan  on 10/28/2014
        FROM po_requisition_lines b,
             po_requisition_headers a,
             mtl_system_items c,
             mtl_units_of_measure d
       WHERE     b.source_type_code = 'INVENTORY'
             AND NVL (b.cancel_flag, 'N') = 'N'
             AND a.type_lookup_code = 'INTERNAL'
             AND a.authorization_status = 'APPROVED'
             AND a.requisition_header_id = b.requisition_header_id
             AND b.item_id = c.inventory_item_id
             AND b.unit_meas_lookup_code = d.unit_of_measure
             AND b.destination_organization_id = c.organization_id
             AND b.destination_organization_id = i_organization_id
             AND b.item_id = i_inventory_item_id;

      RETURN (NVL (l_req_qty, 0));
   EXCEPTION
      WHEN OTHERS
      THEN
         RETURN 0;
   END get_internal_req_qty;

   -- 12/03/2013 CG: TMS 20131121-00164: Added new function to pull concatenated cross ref
   FUNCTION xxwc_inv_get_concat_cross_ref (
      p_inventory_item_id      IN NUMBER,
      p_cross_reference_type   IN VARCHAR2)
      RETURN VARCHAR2
   IS
      l_cross_reference   VARCHAR2 (1000);
      l_count             NUMBER;
   BEGIN
      l_cross_reference := NULL;
      l_count := 0;

      FOR c1
         IN (SELECT mcrb.inventory_item_id,
                    mcrb.cross_reference_type,
                    mcrb.cross_reference
               FROM mtl_cross_references_b mcrb
              WHERE     mcrb.inventory_item_id = p_inventory_item_id
                    AND mcrb.cross_reference_type = p_cross_reference_type)
      LOOP
         IF l_count > 0 AND l_cross_reference IS NOT NULL
         THEN
            l_cross_reference := l_cross_reference || ', ';
         END IF;

         l_cross_reference :=
            SUBSTR (l_cross_reference || c1.cross_reference, 1, 1000);

         IF LENGTH (l_cross_reference) >= 1000
         THEN
            RETURN l_cross_reference;
         END IF;

         l_count := l_count + 1;
      END LOOP;

      RETURN l_cross_reference;
   EXCEPTION
      WHEN OTHERS
      THEN
         RETURN NULL;
   END xxwc_inv_get_concat_cross_ref;

   -- 12/10/2013 CG: TMS 20130904-00654: New function to retrieve vendor min notes
   FUNCTION get_vendor_minimum_notes (i_vendor_id         IN NUMBER,
                                      i_vendor_site_id    IN NUMBER,
                                      i_organization_id   IN NUMBER,
                                      i_vendor_contact_id IN NUMBER DEFAULT NULL) --- Ver 1.5 as per TMS# 20151111-00201 Added new Column i_vendor_contact_id
      RETURN VARCHAR
   IS
      l_disp_info    VARCHAR2 (4000);
      l_new_str      VARCHAR2 (201);
      l_string_len   NUMBER;
      l_cur_start    NUMBER;
   BEGIN
      l_cur_start := 0;
      l_disp_info := NULL;

      FOR c1
         IN (SELECT ('Notes: ' || notes) vm_notes,
             LENGTH ('Notes: ' || notes) str_len
             --INTO l_disp_info
             FROM   xxwc_po_vendor_minimum
             WHERE  vendor_id         = i_vendor_id
             AND    vendor_site_id    = i_vendor_site_id
             AND    organization_id   = i_organization_id
			 AND    NVL(vendor_contact_id,1) = NVL(i_vendor_contact_id,1))  --- Ver 1.5 as per TMS# 20151111-00201 Added new condition
      LOOP
         WHILE l_cur_start < c1.str_len
         LOOP
            l_new_str := NULL;
            l_new_str := SUBSTR (c1.vm_notes, l_cur_start, 200);

            IF l_cur_start = 0
            THEN
               l_disp_info := l_disp_info || l_new_str;
            ELSE
               l_disp_info := l_disp_info || CHR (10) || l_new_str;
            END IF;

            IF l_cur_start < c1.str_len
            THEN
               l_cur_start := l_cur_start + 201;
            ELSE
               l_cur_start := 0;
               EXIT;
            END IF;
         END LOOP;
      END LOOP;

      RETURN (l_disp_info);
   EXCEPTION
      WHEN OTHERS
      THEN
         BEGIN
            -- 12/10/13 CG: TMS 20130904-00654: Updated to Add new field, nomenclature and spacing
            SELECT 'Notes: ' || notes
              INTO l_disp_info
              FROM xxwc_po_vendor_minimum
             WHERE     vendor_id = i_vendor_id
                   AND organization_id = i_organization_id;

            RETURN (l_disp_info);
         EXCEPTION
            WHEN OTHERS
            THEN
               -- 12/10/13 CG: TMS 20130904-00654: Updated to Add new field, nomenclature and spacing
               RETURN ('Notes: N/A');
         END;
   END get_vendor_minimum_notes;

   /*************************************************************************************************
   *   Function get_vendor_contact_minimums                                                         *
   *   Purpose : Used in the PO Form to display the Vendor Minimums for specific Vendor Contact     *
   *                                                                                                *
   *                                                                                                *
   *    FUNCTION get_vendor_contact_minimum (i_vendor_id         IN NUMBER                          *
   *                                    , i_vendor_site_id    IN NUMBER                             *
   *                                    , i_vendor_contact_id IN NUMBER                             *
   *                                    , i_organization_id   IN NUMBER)                            *
   *   REVISIONS:                                                                                   *
   *   Ver        Date        Author                     Description                                *
   *   ---------  ----------  ---------------         -------------------------                     *
   *   1.1        05/27/2014  Lee Spitzer                TMS 20130917-00676                         *
   *                                                         Vendor Cost Management Improvements    *
   *                                                      Added Function get_vendor_contact_minimum *
   /************************************************************************************************/


   FUNCTION get_vendor_contact_minimum (i_vendor_id           IN NUMBER,
                                        i_vendor_site_id      IN NUMBER,
                                        i_vendor_contact_id   IN NUMBER,
                                        i_organization_id     IN NUMBER)
      RETURN VARCHAR
   IS
      l_disp_info          VARCHAR2 (400);
      v_vendor_site_code   VARCHAR2 (255);
   BEGIN
      -- 12/10/13 CG: TMS 20130904-00654: Updated to Add new field, nomenclature and spacing
      --10 April 2014 added note ' Prepaid values have not been entered for this supplier vendor site' when vendor information not found in custom
      --vendor minimum table - by Rasikha
      BEGIN
         SELECT    ss.last_name
                || ', '
                || ss.first_name
                || ' '
                || ss.middle_name
           INTO v_vendor_site_code
           FROM po_vendor_contacts ss
          WHERE     ss.vendor_id = i_vendor_id
                AND ss.vendor_site_id = i_vendor_site_id
                AND ss.vendor_contact_id = i_vendor_contact_id;
      EXCEPTION
         WHEN OTHERS
         THEN
            v_vendor_site_code := '';
      END;

      SELECT    '-MINORD$ = '
             || vendor_min_dollar
             || '        PPD$ = '
             || freight_min_dollar
             || '        PPD# = '
             || freight_min_uom
             -- 12/10/13 CG: TMS 20130904-00654: New Field
             || '        PPDU = '
             || ppd_freight_units
        INTO l_disp_info
        FROM xxwc_po_vendor_minimum
       WHERE     vendor_id = i_vendor_id
             AND vendor_site_id = i_vendor_site_id
             AND organization_id = i_organization_id
             AND vendor_contact_id = i_vendor_contact_id;

      RETURN (l_disp_info);
   EXCEPTION
      WHEN OTHERS
      THEN
         RETURN (   '-Prepaid values have not been entered for this supplier site: '
                 || v_vendor_site_code
                 || ' at this Org.');
   END get_vendor_contact_minimum;

   /*************************************************************************************************
    *   PROCEDURE load_vendor_minimum_contact                                                        *
    *                              (i_vendor_min   IN NUMBER,                                        *
    *                               i_freight_min  IN NUMBER,                                        *
    *                               i_freight_min_uom IN NUMBER,                                     *
    *                               i_ppd_freight_units IN NUMBER,                                   *
    *                               i_notes IN VARCHAR2,                                             *
    *                               i_default_flag IN VARCHAR2,                                      *
    *                               i_vendor_id IN NUMBER,                                           *
    *                               i_vendor_site_code IN VARCHAR2,                                  *
    *                               i_vendor_contact  IN VARCHAR2,                                   *
    *                               i_organization_id IN NUMBER                                      *
    *                               );                                                               *
    *                                                                                                *
    *   REVISIONS:                                                                                   *
    *   Ver        Date        Author                     Description                                *
    *   ---------  ----------  ---------------         -------------------------                     *
    *   1.2        08/13/2014  Lee Spitzer                20140726-00008  Update Vendor min          *
    *                                                         Web ADI to download                    *
    *                                                                                                *
    /************************************************************************************************/
   -- 12/09/CG: TMS 20130904-00654: Added 2 new Columns i_ppd_freight_units and i_notes

   PROCEDURE load_vendor_minimum_contact (i_vendor_min          IN NUMBER,
                                          i_freight_min         IN NUMBER,
                                          i_freight_min_uom     IN NUMBER,
                                          i_ppd_freight_units   IN NUMBER,
                                          i_notes               IN VARCHAR2,
                                          i_default_flag        IN VARCHAR2,
                                          i_vendor_id           IN VARCHAR2,
                                          i_vendor_name         IN VARCHAR2,
                                          --i_vendor_site_id IN NUMBER,
                                          i_vendor_site_code    IN VARCHAR2,
                                          --i_vendor_contact_id  IN NUMBER,
                                          i_vendor_contact      IN VARCHAR2,
                                          i_organization_id     IN VARCHAR2)
   IS
      lv_message             VARCHAR2 (200);
      l_exists               VARCHAR2 (1);
      lv_vendor_id           NUMBER;
      lv_organization_id     NUMBER;
      lv_vendor_site_id      NUMBER;
      lv_vendor_contact_id   NUMBER;
      lv_default_flag        VARCHAR2 (1);
      lv_count_default       NUMBER;
   BEGIN
      IF i_vendor_id IS NULL
      THEN
         lv_message := 'Vendor is required ';
         RAISE_APPLICATION_ERROR (-20001, lv_message);
      END IF;

      BEGIN
         SELECT TO_NUMBER (i_vendor_id) INTO lv_vendor_id FROM DUAL;
      EXCEPTION
         WHEN OTHERS
         THEN
            lv_message := 'Can not convert char to number for ' || i_vendor_id;
            RAISE_APPLICATION_ERROR (-20001, lv_message);
      END;

      IF i_vendor_site_code IS NULL
      THEN
         lv_message := 'Vendor Site Code is required';
         RAISE_APPLICATION_ERROR (-20001, lv_message);
      END IF;

      IF i_organization_id IS NULL
      THEN
         lv_message := 'Organization is required';
         RAISE_APPLICATION_ERROR (-20001, lv_message);
      END IF;

      BEGIN
         SELECT TO_NUMBER (i_organization_id)
           INTO lv_organization_id
           FROM DUAL;
      EXCEPTION
         WHEN OTHERS
         THEN
            lv_message :=
               'Can not convert char to number for ' || i_organization_id;
            RAISE_APPLICATION_ERROR (-20001, lv_message);
      END;


      IF    NVL (i_vendor_min, 0) < 0
         OR NVL (i_freight_min, 0) < 0
         OR NVL (i_freight_min_uom, 0) < 0
         -- 12/09/CG: TMS 20130904-00654: Added condition to not allow negative PPDU
         OR NVL (i_ppd_freight_units, 0) < 0
      THEN
         lv_message := 'Vendor / Freight Minimum should be a positive value';
         RAISE_APPLICATION_ERROR (-20001, lv_message);
      END IF;


      IF i_default_flag IS NOT NULL
      THEN
         IF i_default_flag NOT IN ('Y',
                                   'N',
                                   'y',
                                   'n')
         THEN
            lv_message := 'Default flag must be Y or N';
            RAISE_APPLICATION_ERROR (-20001, lv_message);
         ELSE
            lv_default_flag := UPPER (i_default_flag);
         END IF;
      END IF;

      BEGIN
         SELECT vendor_site_id
           INTO lv_vendor_site_id
           --Modified the below line for TMS ## TMS#20141002-00041 by VijaySrinivasan  on 10/28/2014
           FROM ap_supplier_sites_all -- Added all by Vamshi on 14-May-2015 @ 1.4 V
          WHERE     vendor_id = lv_vendor_id
                AND vendor_site_code = i_vendor_site_code
                AND org_id = fnd_profile.VALUE ('ORG_ID'); -- Added condition by Vamshi on 14-May-2015 @ 1.4 V
      EXCEPTION
         WHEN OTHERS
         THEN
            lv_message :=
                  'Could not find vendor site id for vendor site '
               || i_vendor_site_code
               || ' '
               || SQLCODE
               || SQLERRM;
            RAISE_APPLICATION_ERROR (-20001, lv_message);
      END;

      IF i_vendor_contact IS NOT NULL
      THEN
         BEGIN
            SELECT vendor_contact_id
              INTO lv_vendor_contact_id
              FROM po_vendor_contacts
             WHERE     vendor_id = lv_vendor_id
                   AND vendor_site_id = lv_vendor_site_id
                   AND NVL (inactive_date, SYSDATE + 1) >= SYSDATE -- Added condition by Vamshi on 14-May-2015 @ 1.4 V
                   AND last_name = i_vendor_contact;
         EXCEPTION
            WHEN OTHERS
            THEN
               lv_message :=
                     'Could not find vendor contact id for vendor contact '
                  || i_vendor_contact
                  || ' '
                  || SQLCODE
                  || SQLERRM;
               RAISE_APPLICATION_ERROR (-20001, lv_message);
         END;
      END IF;

      IF lv_default_flag = 'Y'
      THEN
         lv_count_default := 0;

         BEGIN
            SELECT COUNT (*)
              INTO lv_count_default
              FROM xxwc_po_vendor_minimum
             WHERE     vendor_id = lv_vendor_id
                   AND vendor_site_id = lv_vendor_site_id
                   AND organization_id = i_organization_id
                   AND NVL (default_flag, 'N') = 'Y';
         EXCEPTION
            WHEN OTHERS
            THEN
               lv_count_default := 0;
         END;

         IF lv_count_default > 0
         THEN
            lv_message :=
               'There is already a default contact for this vendor, vendor site, and organization.';
            RAISE_APPLICATION_ERROR (-20001, lv_message);
         END IF;
      END IF;

      BEGIN
         --lv_message := '4';
         SELECT 'x'
           INTO l_exists
           FROM xxwc_po_vendor_minimum
          WHERE     vendor_id = lv_vendor_id
                AND NVL (vendor_site_id, -1) = NVL (lv_vendor_site_id, -1)
                AND NVL (vendor_contact_id, -1) =
                       NVL (lv_vendor_contact_id, -1)
                AND organization_id = lv_organization_id;        --i_org_code;

         --  lv_message := '5';
         UPDATE xxwc_po_vendor_minimum
            SET vendor_min_dollar = NVL (i_vendor_min, 0),
                freight_min_dollar = NVL (i_freight_min, 0),
                freight_min_uom = NVL (i_freight_min_uom, 0),
                last_update_date = SYSDATE,
                last_updated_by = fnd_global.user_id,
                last_update_login = FND_GLOBAL.LOGIN_ID,
                -- 12/09/CG: TMS 20130904-00654: Added Update of two new columns
                ppd_freight_units = NVL (i_ppd_freight_units, 0),
                notes = i_notes,
                default_flag = lv_default_flag
          WHERE     vendor_id = lv_vendor_id
                AND NVL (vendor_site_id, -1) = NVL (lv_vendor_site_id, -1)
                AND NVL (vendor_contact_id, -1) =
                       NVL (lv_vendor_contact_id, -1)
                AND organization_id = lv_organization_id;       -- i_org_code;
      EXCEPTION
         WHEN NO_DATA_FOUND
         THEN
            --lv_message := '6';
            INSERT INTO xxwc_po_vendor_minimum (vendor_id,
                                                vendor_site_id,
                                                vendor_contact_id,
                                                organization_id,
                                                vendor_min_dollar,
                                                freight_min_dollar,
                                                freight_min_uom,
                                                creation_date,
                                                created_by,
                                                last_update_date,
                                                last_updated_by,
                                                last_update_login,
                                                ppd_freight_units,
                                                notes,
                                                default_flag)
                 VALUES (lv_vendor_id,
                         lv_vendor_site_id,
                         lv_vendor_contact_id,
                         lv_organization_id,
                         NVL (i_vendor_min, 0),
                         NVL (i_freight_min, 0),
                         NVL (i_freight_min_uom, 0),
                         SYSDATE,
                         fnd_global.user_id,
                         SYSDATE,
                         fnd_global.user_id,
                         fnd_global.login_id,
                         NVL (i_ppd_freight_units, 0),
                         i_notes,
                         NVL (lv_default_flag, 'N'));
      END;

      COMMIT;
   EXCEPTION
      WHEN OTHERS
      THEN
         lv_message := lv_message;
         RAISE_APPLICATION_ERROR (-20001, lv_message);
   END load_vendor_minimum_contact;
END xxwc_ascp_scwb_pkg;
/