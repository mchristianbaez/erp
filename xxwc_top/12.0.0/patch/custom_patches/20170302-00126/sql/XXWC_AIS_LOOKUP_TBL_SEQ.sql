/*************************************************************************
 Copyright (c) 2012 HD Supply
 All rights reserved.
**************************************************************************
   $Header XXWC."XXWC_AIS_LOOKUP_TBL_SEQ" $
  Module Name: XXWC."XXWC_AIS_LOOKUP_TBL_SEQ"

  PURPOSE:

  REVISIONS:
  -- VERSION DATE          AUTHOR(S)   TMS TASK        DESCRIPTION
  -- ------- -----------   ------------ ---------      -------------------------
  -- 1.0     15-apr-2017   Pahwa, Nancy                Initially Created 
TMS# 20170302-00126
**************************************************************************/
CREATE SEQUENCE   XXWC."XXWC_AIS_LOOKUP_TBL_SEQ"  MINVALUE 1 MAXVALUE 9999999999999999999999999999 INCREMENT BY 1 START WITH 21 CACHE 20 NOORDER  NOCYCLE;