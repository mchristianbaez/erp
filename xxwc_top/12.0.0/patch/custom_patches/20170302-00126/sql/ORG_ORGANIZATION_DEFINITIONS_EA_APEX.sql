/*************************************************************************
 Copyright (c) 2012 HD Supply
 All rights reserved.
**************************************************************************
   $Header apps.org_organization_definitions $
  Module Name: apps.org_organization_definitions

  PURPOSE:Grant on apps.org_organization_definitions to EA_APEX

  REVISIONS:
  -- VERSION DATE          AUTHOR(S)   TMS TASK        DESCRIPTION
  -- ------- -----------   ------------ ---------      -------------------------
  -- 1.0     15-apr-2017   Pahwa, Nancy                Initially Created 
TMS# 20170302-00126
**************************************************************************/
GRANT select ON apps.org_organization_definitions TO EA_APEX;