/*************************************************************************
 Copyright (c) 2012 HD Supply
 All rights reserved.
**************************************************************************
   $Header xxwc.XXWC_AIS_SEARCH_ARCH_TBL $
  Module Name: xxwc.XXWC_AIS_SEARCH_ARCH_TBL

  PURPOSE:

  REVISIONS:
  -- VERSION DATE          AUTHOR(S)   TMS TASK        DESCRIPTION
  -- ------- -----------   ------------ ---------      -------------------------
  -- 1.0     15-apr-2017   Pahwa, Nancy                Initially Created 
TMS# 20170302-00126
**************************************************************************/
declare
cursor c1 is
SELECT rowid row_id, item_desc, created_by, creation_date,
       LTRIM(MAX(SYS_CONNECT_BY_PATH(QUERY_START_TIME,','))
       KEEP (DENSE_RANK LAST ORDER BY curr),',') AS QUERY_START_TIME
FROM   (SELECT item_desc, created_by, creation_date,
ROW_NUMBER() OVER (PARTITION BY creation_date, item_desc, created_by ORDER BY creation_date asc, row_count asc) AS curr,
               ROW_NUMBER() OVER (PARTITION BY creation_date, item_desc, created_by  ORDER BY creation_date asc, row_count asc) -1 AS prev,
               QUERY_START_TIME
        FROM   xxwc.XXWC_AIS_SEARCH_ARCH_TBL where sequence_id is null)
GROUP BY creation_date, item_desc, created_by, rowid
CONNECT BY prev = PRIOR curr AND item_desc = PRIOR item_desc
CONNECT BY prev = PRIOR curr AND created_by = PRIOR created_by
CONNECT BY prev = PRIOR curr AND creation_date = PRIOR creation_date
START WITH curr = 1;
ln_seq_num NUMBER;
ln_count number;
begin
ln_count :=0;
for c2 in c1 loop
ln_seq_num:= null;
SELECT XXWC.XXWC_AIS_SEARCH_LOG_SEQ.nextval INTO ln_seq_num FROM DUAL;
update xxwc.XXWC_AIS_SEARCH_ARCH_TBL SET SEQUENCE_ID =  XXWC.XXWC_AIS_SEARCH_LOG_SEQ.currval WHERE rowid = c2.row_id;
commit;
END LOOP;
END;
/