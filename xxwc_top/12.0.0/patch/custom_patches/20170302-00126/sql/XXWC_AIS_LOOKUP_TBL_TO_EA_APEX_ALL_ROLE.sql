/*************************************************************************
 Copyright (c) 2012 HD Supply
 All rights reserved.
**************************************************************************
   $Header "XXWC"."XXWC_AIS_LOOKUP_TBL $
  Module Name: "XXWC"."XXWC_AIS_LOOKUP_TBL

  PURPOSE:Grant on "XXWC"."XXWC_AIS_LOOKUP_TBL to EA_APEX

  REVISIONS:
  -- VERSION DATE          AUTHOR(S)   TMS TASK        DESCRIPTION
  -- ------- -----------   ------------ ---------      -------------------------
  -- 1.0     15-apr-2017   Pahwa, Nancy                Initially Created 
TMS# 20170302-00126
**************************************************************************/
GRANT ALL ON "XXWC"."XXWC_AIS_LOOKUP_TBL" TO EA_APEX;