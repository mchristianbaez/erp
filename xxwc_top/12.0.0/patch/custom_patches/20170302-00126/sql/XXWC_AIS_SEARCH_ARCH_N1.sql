/*************************************************************************
 Copyright (c) 2012 HD Supply
 All rights reserved.
**************************************************************************
   $Header xxwc.XXWC_AIS_SEARCH_ARCH_N1 $
  Module Name: xxwc.XXWC_AIS_SEARCH_ARCH_N1

  PURPOSE:

  REVISIONS:
  -- VERSION DATE          AUTHOR(S)   TMS TASK        DESCRIPTION
  -- ------- -----------   ------------ ---------      -------------------------
  -- 1.0     15-apr-2017   Pahwa, Nancy                Initially Created 
TMS# 20170302-00126
**************************************************************************/
CREATE INDEX "XXWC"."XXWC_AIS_SEARCH_ARCH_N1" ON "XXWC"."XXWC_AIS_SEARCH_ARCH_TBL" ("SEQUENCE_ID");