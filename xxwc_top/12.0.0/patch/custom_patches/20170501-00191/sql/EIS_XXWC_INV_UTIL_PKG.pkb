CREATE OR REPLACE 
PACKAGE BODY  XXEIS.EIS_XXWC_INV_UTIL_PKG
AS
  --//============================================================================
  --// Object Name          :: EIS_XXWC_INV_UTIL_PKG
  --//
  --// Object Type          :: Package Body
  --//
  --// Object Description   :: This Package will trigger in view and populate the values into View.
  --//
  --// Version Control
  --//============================================================================
  --// Version    Author           Date          Description
  --//----------------------------------------------------------------------------
  --// 1.0        Siva        06/06/2016         Initial Build  -- Added for TMS#20160601-00025.
  --// 1.1        Siva        10/17/2016         TMS#20160930-00078 
  --// 1.2        Siva        10-Jan-2017        TMS#20161025-00027 
  --// 1.3		  Siva		  10-May-2017		 TMS#20170501-00191
  --//============================================================================
FUNCTION GET_ONHAND_QTY(
    P_INVENTORY_ITEM_ID NUMBER,
    P_ORGANIZATION_ID   NUMBER,
    P_SUBINVENTORY_CODE VARCHAR2)
  RETURN number
IS
  --//============================================================================
  --// Object Usage         :: This Object Referred by "Inventory - Onhand Quantity - WC"
  --//
  --// Object Name          :: GET_ONHAND_QTY
  --//
  --// Object Type          :: Function
  --//
  --// Object Description   :: This Function will populate the values in the view.
  --//
  --// Version Control
  --//============================================================================
  --// Version    Author           Date          Description
  --//----------------------------------------------------------------------------
  --// 1.0        Siva        06/06/2016      Initial Build  -- Added for TMS#20160601-00025.
  --//============================================================================
L_HASH_INDEX varchar2(100);
L_SQL varchar2(32000);
begin
g_onhand_qty := NULL;

l_sql :='SELECT SUM (NVL(moq1.Primary_Transaction_Quantity,0))
    FROM Mtl_Onhand_Quantities_Detail Moq1
    WHERE Moq1.Inventory_Item_Id = :1
    AND Moq1.Organization_Id     = :2
    AND Moq1.subinventory_code   = :3';
    BEGIN
        l_hash_index:=P_INVENTORY_ITEM_ID||'-'||P_ORGANIZATION_ID||'-'||P_SUBINVENTORY_CODE;
        g_onhand_qty := G_ONHAND_QTY_VLDN_TBL(L_HASH_INDEX);
 exception
    when no_data_found
    THEN
    begin      
    EXECUTE IMMEDIATE L_SQL INTO g_onhand_qty USING P_INVENTORY_ITEM_ID,P_ORGANIZATION_ID,P_SUBINVENTORY_CODE;
    EXCEPTION WHEN NO_DATA_FOUND THEN
    g_onhand_qty :=0;
    WHEN OTHERS THEN
    g_onhand_qty :=0;
    END;      
                      l_hash_index:=P_INVENTORY_ITEM_ID||'-'||P_ORGANIZATION_ID||'-'||P_SUBINVENTORY_CODE;
                       G_ONHAND_QTY_VLDN_TBL(L_HASH_INDEX) := g_onhand_qty;
    END;
     return  g_onhand_qty;
     EXCEPTION when OTHERS then
      g_onhand_qty:=0;
      RETURN  g_onhand_qty;

end GET_ONHAND_QTY;

FUNCTION get_invoice_url(
    p_po_distribution_id NUMBER,
    p_period_name        VARCHAR2,
    p_request_type       VARCHAR2)
  RETURN VARCHAR2
IS
  --//============================================================================
  --// Object Usage         :: This Object Referred by "HDS Account Analysis Subledger Detail Report"
  --//
  --// Object Name          :: get_invoice_url
  --//
  --// Object Type          :: Function
  --//
  --// Object Description   :: This Function will populate the values in the view.
  --//
  --// Version Control
  --//============================================================================
  --// Version    Author           Date          Description
  --//----------------------------------------------------------------------------
  --// 1.1        Siva        10/07/2016        TMS#20160930-00078
  --//============================================================================  
  L_HASH_INDEX VARCHAR2(100);
  l_sql        VARCHAR2(32000);
  CURSOR v_inv_url_cur
  IS
    SELECT DISTINCT a.url,
      ai.invoice_num
    FROM fnd_attached_docs_form_vl a,
      ap_invoice_distributions b,
      ap_invoices_all ai
    WHERE b.po_distribution_id = p_po_distribution_id
    AND b.period_name          = p_period_name
    AND b.invoice_id           = ai.invoice_id
    AND a.entity_name          = 'AP_INVOICES'
    AND a.pk1_value            = TO_CHAR (b.invoice_id)
    AND a.datatype_name        = 'Web Page'
    AND A.category_description = 'Invoice Internal'
    AND a.document_description = 'Documentum Image URL'
    AND A.function_name        = 'APXINWKB' ;
BEGIN
  g_url         := NULL;
  g_invoice_num := NULL;
  BEGIN
    l_hash_index       :=p_po_distribution_id||'-'||p_period_name;
    IF P_request_type   ='URL' THEN
      g_url            := g_url_vldn_tbl(L_HASH_INDEX);
    ELSif P_request_type='INVOICENUM' THEN
      g_invoice_num    := G_invoice_num_VLDN_TBL(L_HASH_INDEX);
    END IF;
  EXCEPTION
  WHEN no_data_found THEN
    BEGIN
      FOR rec IN v_inv_url_cur
      LOOP
        g_url         := g_url || ' , ' || rec.url;
        g_invoice_num := g_invoice_num || ' , ' || rec.invoice_num;
      END LOOP;
      g_url         := SUBSTR (g_url, 4);
      g_invoice_num := SUBSTR (g_invoice_num, 4);
    EXCEPTION
    WHEN NO_DATA_FOUND THEN
      IF P_request_type   ='URL' THEN
        g_url            := NULL;
      elsif P_request_type='INVOICENUM' THEN
        g_invoice_num    := NULL;
      END IF;
    END;
    l_hash_index                         :=p_po_distribution_id||'-'||p_period_name;
    g_url_vldn_tbl(l_hash_index)         := g_url;
    G_invoice_num_VLDN_TBL(L_HASH_INDEX) := g_invoice_num;
  END;
  IF P_request_type='URL' THEN
    RETURN g_url ;
  elsif P_request_type='INVOICENUM' THEN
    RETURN g_invoice_num ;
  END IF;
EXCEPTION
WHEN OTHERS THEN
  IF P_request_type='URL' THEN
    g_url         := NULL;
    RETURN g_url ;
  elsif P_request_type='INVOICENUM' THEN
    g_invoice_num    := NULL;
    RETURN g_invoice_num ;
  END IF;
END get_invoice_url;


FUNCTION GET_VENDOR_OWNER_NUM(
    p_inventory_item_id NUMBER,
    p_request_type       VARCHAR2)
  RETURN VARCHAR2
IS
  --//============================================================================
  --// Object Usage         :: This Object Referred by "PO Cost Change Analysis Report - WC"
  --//
  --// Object Name          :: GET_VENDOR_OWNER_NUM
  --//
  --// Object Type          :: Function
  --//
  --// Object Description   :: This Function will populate the values in the view.
  --//
  --// Version Control
  --//============================================================================
  --// Version    Author           Date             Description
  --//----------------------------------------------------------------------------
  --// 1.2        Siva          10-Jan-2017        TMS#20161025-00027 
  --//============================================================================  

L_HASH_INDEX number;
l_sql varchar2(32000);
begin
g_vendor_number := null;
g_vendor_name  := null;
g_vendor_id    := null;

l_sql :='SELECT segment1,
                vendor_name,
                vendor_id
        FROM apps.po_vendors
        WHERE segment1 = (SELECT MAX(mcr.attribute1)
                            FROM apps.mtl_cross_references_b mcr
                            WHERE mcr.inventory_item_id= :1
                              AND cross_reference_type   =''VENDOR''
                           )';
    BEGIN
        L_HASH_INDEX:=p_inventory_item_id;
        if P_request_type='NUM' then
           g_vendor_number := g_vendor_num_vldn_tbl(L_HASH_INDEX);
        ELSif P_request_type='NAME' then
            g_vendor_name := g_vendor_name_vldn_tbl(l_hash_index);
        elsif p_request_type='VEND_ID' then
            g_vendor_id := g_vendor_id_vldn_tbl(L_HASH_INDEX);
        END IF;
 
 exception
    when no_data_found
    THEN
    BEGIN      
    EXECUTE IMMEDIATE L_SQL INTO g_vendor_number,g_vendor_name,g_vendor_id USING P_INVENTORY_ITEM_ID;
    EXCEPTION WHEN NO_DATA_FOUND THEN
	if P_request_type='NUM' then
    g_vendor_number := null;
	elsif P_request_type='NAME' then
    g_vendor_name 	:= null;
  elsif p_request_type='VEND_ID' then
    g_vendor_id 	:= null;
	end if;
    end;
    
		l_hash_index:=p_inventory_item_id;
			    g_vendor_num_vldn_tbl(l_hash_index) 	  := g_vendor_number;
      		g_vendor_name_vldn_tbl(l_hash_index)  	:= g_vendor_name; 
          g_vendor_id_vldn_tbl(l_hash_index)  	  := g_vendor_id; 
          
    END;
     	if P_request_type='NUM' then
    return  g_vendor_number ;
	elsif P_request_type='NAME' then
    RETURN  g_vendor_name ;
   elsif p_request_type='VEND_ID' THEN
   RETURN  g_vendor_id ;
   end if;
    EXCEPTION when OTHERS then
  	if P_request_type='NUM' then
    g_vendor_number := null;
    return  g_vendor_number ;
	elsif P_request_type='NAME' then
    g_vendor_name 	:= null;
    RETURN  g_vendor_name ;
  elsif p_request_type='VEND_ID' then
    g_vendor_id 	:= NULL;
    RETURN  g_vendor_id ;
	end if;

END GET_VENDOR_OWNER_NUM;


FUNCTION GET_CUST_SO_ADOPTION(
    P_DATE_FROM DATE,
    P_DATE_TO DATE,
    P_BRANCH VARCHAR2,
    P_REGION   VARCHAR2,
    P_DISTRICT VARCHAR2
    )
  RETURN number
IS
  --//============================================================================
  --// Object Usage         :: This Object Referred by "Custom Sales Order Form Adoption Reporting - WC"
  --//
  --// Object Name          :: GET_CUST_SO_ADOPTION
  --//
  --// Object Type          :: Function
  --//
  --// Object Description   :: This Function will populate the values in the view.
  --//
  --// Version Control
  --//============================================================================
  --// Version    Author           Date             Description
  --//----------------------------------------------------------------------------
  --// 1.3		  Siva		  10-May-2017		 TMS#20170501-00191
  --//============================================================================  
L_HASH_INDEX varchar2(1000);
L_SQL varchar2(32000);
begin
G_SO_ADOPTION := null;


l_sql :='SELECT round((ROUND(SUM(DECODE(OH.ATTRIBUTE6,''CUSTOM_SALES_ORDER'',1,0))/COUNT(ORDER_NUMBER),4 )*100),0) so_adoption
FROM apps.oe_order_headers_all oh,
  apps.mtl_parameters mp,
  apps.oe_transaction_types_tl ot
WHERE oh.ship_from_org_id = mp.organization_id
AND oh.flow_status_code ! =''CANCELLED''
AND oh.order_type_id      = ot.transaction_type_id
AND ot.language(+)        = userenv(''LANG'')
AND ot.name !             = ''INTERNAL ORDER''
AND TRUNC(OH.CREATION_DATE) >= :1
AND TRUNC(OH.CREATION_DATE) <= :2
and mp.organization_code     = :3
and mp.attribute9 			     = :4
and MP.ATTRIBUTE8 			     = :5
 ';
    BEGIN
        l_hash_index:=P_DATE_FROM||'-'||P_DATE_TO||'-'||P_BRANCH||'-'||P_REGION||'-'||P_DISTRICT;
        g_so_adoption := G_SO_ADOPTION_VLDN_TBL(L_HASH_INDEX);
 exception
    when no_data_found
    THEN
    begin
    EXECUTE IMMEDIATE L_SQL INTO g_so_adoption USING P_DATE_FROM,P_DATE_TO,P_BRANCH,P_REGION,P_DISTRICT;
    EXCEPTION WHEN NO_DATA_FOUND THEN
    g_so_adoption :=0;
    WHEN OTHERS THEN
    g_so_adoption :=0;
    END;
                      l_hash_index:=P_DATE_FROM||'-'||P_DATE_TO||'-'||P_BRANCH||'-'||P_REGION||'-'||P_DISTRICT;
                       G_SO_ADOPTION_VLDN_TBL(L_HASH_INDEX) := g_so_adoption;
    END;
     return  g_so_adoption;
     EXCEPTION when OTHERS then
      g_so_adoption:=0;
      RETURN  g_so_adoption;

end GET_CUST_SO_ADOPTION;

function eis_get_date_from
RETURN date
IS
  --//============================================================================
  --// Object Usage         :: This Object Referred by "Custom Sales Order Form Adoption Reporting - WC"
  --//
  --// Object Name          :: EIS_GET_DATE_FROM
  --//
  --// Object Type          :: Function
  --//
  --// Object Description   :: This Function will populate the values in the view.
  --//
  --// Version Control
  --//============================================================================
  --// Version    Author           Date             Description
  --//----------------------------------------------------------------------------
  --// 1.3		  Siva		  10-May-2017		 TMS#20170501-00191
  --//============================================================================
begin
      RETURN G_DATE_FROM;
END eis_get_date_from;

FUNCTION eis_get_date_to
RETURN date
is
  --//============================================================================
  --// Object Usage         :: This Object Referred by "Custom Sales Order Form Adoption Reporting - WC"
  --//
  --// Object Name          :: EIS_GET_DATE_TO
  --//
  --// Object Type          :: Function
  --//
  --// Object Description   :: This Function will populate the values in the view.
  --//
  --// Version Control
  --//============================================================================
  --// Version    Author           Date             Description
  --//----------------------------------------------------------------------------
  --// 1.3		  Siva		  10-May-2017		 TMS#20170501-00191
  --//============================================================================  
begin
      return G_DATE_TO;
END EIS_GET_DATE_TO;


FUNCTION GET_DISTRICT_ADOPTION(
    P_DATE_FROM DATE,
    P_DATE_TO DATE,
    P_REGION   VARCHAR2,
    P_DISTRICT VARCHAR2
    )
  RETURN number
IS
  --//============================================================================
  --// Object Usage         :: This Object Referred by "Custom Sales Order Form Adoption Reporting - WC"
  --//
  --// Object Name          :: GET_DISTRICT_ADOPTION
  --//
  --// Object Type          :: Function
  --//
  --// Object Description   :: This Function will populate the values in the view.
  --//
  --// Version Control
  --//============================================================================
  --// Version    Author           Date             Description
  --//----------------------------------------------------------------------------
  --// 1.3		  Siva		  10-May-2017		 TMS#20170501-00191
  --//============================================================================ 
L_HASH_INDEX varchar2(1000);
L_SQL varchar2(32000);
begin
G_dist_ADOPTION := null;


l_sql :='SELECT round((ROUND(SUM(DECODE(OH.ATTRIBUTE6,''CUSTOM_SALES_ORDER'',1,0))/COUNT(ORDER_NUMBER),4 )*100),0) so_adoption
FROM apps.oe_order_headers_all oh,
  apps.mtl_parameters mp,
  apps.oe_transaction_types_tl ot
WHERE oh.ship_from_org_id = mp.organization_id
AND oh.flow_status_code ! =''CANCELLED''
AND oh.order_type_id      = ot.transaction_type_id
AND ot.language(+)        = userenv(''LANG'')
AND ot.name !             = ''INTERNAL ORDER''
AND TRUNC(OH.CREATION_DATE) >= :1
AND TRUNC(OH.CREATION_DATE) <= :2
and mp.attribute9 			     = :3
and MP.ATTRIBUTE8 			     = :4
 ';
    BEGIN
        l_hash_index:=P_DATE_FROM||'-'||P_DATE_TO||'-'||P_REGION||'-'||P_DISTRICT;
        G_dist_ADOPTION := G_dist_ADOPTION_VLDN_TBL(L_HASH_INDEX);
 exception
    when no_data_found
    THEN
    begin
    EXECUTE IMMEDIATE L_SQL INTO G_dist_ADOPTION USING P_DATE_FROM,P_DATE_TO,P_REGION,P_DISTRICT;
    EXCEPTION WHEN NO_DATA_FOUND THEN
    G_dist_ADOPTION :=0;
    WHEN OTHERS THEN
    G_dist_ADOPTION :=0;
    END;
                      l_hash_index:=P_DATE_FROM||'-'||P_DATE_TO||'-'||P_REGION||'-'||P_DISTRICT;
                       G_dist_ADOPTION_VLDN_TBL(L_HASH_INDEX) := G_dist_ADOPTION;
    END;
     return  G_dist_ADOPTION;
     EXCEPTION when OTHERS then
      G_dist_ADOPTION:=0;
      RETURN  G_dist_ADOPTION;

end GET_DISTRICT_ADOPTION;

FUNCTION GET_REGION_ADOPTION(
    P_DATE_FROM DATE,
    P_DATE_TO DATE,
    P_REGION   VARCHAR2
    )
  RETURN number
IS
  --//============================================================================
  --// Object Usage         :: This Object Referred by "Custom Sales Order Form Adoption Reporting - WC"
  --//
  --// Object Name          :: GET_REGION_ADOPTION
  --//
  --// Object Type          :: Function
  --//
  --// Object Description   :: This Function will populate the values in the view.
  --//
  --// Version Control
  --//============================================================================
  --// Version    Author           Date             Description
  --//----------------------------------------------------------------------------
  --// 1.3		  Siva		  10-May-2017		 TMS#20170501-00191
  --//============================================================================  
L_HASH_INDEX varchar2(1000);
L_SQL varchar2(32000);
begin
G_region_ADOPTION := null;


l_sql :='SELECT round((ROUND(SUM(DECODE(OH.ATTRIBUTE6,''CUSTOM_SALES_ORDER'',1,0))/COUNT(ORDER_NUMBER),4 )*100),0) so_adoption
FROM apps.oe_order_headers_all oh,
  apps.mtl_parameters mp,
  apps.oe_transaction_types_tl ot
WHERE oh.ship_from_org_id = mp.organization_id
AND oh.flow_status_code ! =''CANCELLED''
AND oh.order_type_id      = ot.transaction_type_id
AND ot.language(+)        = userenv(''LANG'')
AND ot.name !             = ''INTERNAL ORDER''
AND TRUNC(OH.CREATION_DATE) >= :1
AND TRUNC(OH.CREATION_DATE) <= :2
and mp.attribute9 			     = :3
 ';
    BEGIN
        l_hash_index:=P_DATE_FROM||'-'||P_DATE_TO||'-'||P_REGION;
        G_region_ADOPTION := G_region_ADOPTION_VLDN_TBL(L_HASH_INDEX);
 exception
    when no_data_found
    THEN
    begin
    EXECUTE IMMEDIATE L_SQL INTO G_region_ADOPTION USING P_DATE_FROM,P_DATE_TO,P_REGION;
    EXCEPTION WHEN NO_DATA_FOUND THEN
    G_region_ADOPTION :=0;
    WHEN OTHERS THEN
    G_region_ADOPTION :=0;
    END;
                      l_hash_index:=P_DATE_FROM||'-'||P_DATE_TO||'-'||P_REGION;
                       G_region_ADOPTION_VLDN_TBL(L_HASH_INDEX) := G_region_ADOPTION;
    END;
     return  G_region_ADOPTION;
     EXCEPTION when OTHERS then
      G_region_ADOPTION:=0;
      RETURN  G_region_ADOPTION;

end GET_REGION_ADOPTION;


END EIS_XXWC_INV_UTIL_PKG;
/
