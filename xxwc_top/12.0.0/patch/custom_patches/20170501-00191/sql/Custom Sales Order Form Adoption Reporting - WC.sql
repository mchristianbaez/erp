--Report Name            : Custom Sales Order Form Adoption Reporting - WC
--Import Instructions    : Run this file in XXEIS user from SQL*Plus or any tool like Toad, SQL Navigator. 
prompt Creating Object Data EIS_XXWC_OM_SO_FORM_ADOPTION_V
set scan off define off
DECLARE
mod_exist VARCHAR2(1);
BEGIN 
mod_exist := XXEIS.EIS_RSC_INSERT.check_if_module_exist(660);
IF mod_exist = 'Y' THEN 
--Inserting Object EIS_XXWC_OM_SO_FORM_ADOPTION_V
xxeis.eis_rsc_ins.v( 'EIS_XXWC_OM_SO_FORM_ADOPTION_V',660,'','','','','SA059956','XXEIS','Eis Xxwc Om So Form Adoption V','EXOSFAV','','','VIEW','US','','');
--Delete Object Columns for EIS_XXWC_OM_SO_FORM_ADOPTION_V
xxeis.eis_rsc_utility.delete_view_rows('EIS_XXWC_OM_SO_FORM_ADOPTION_V',660,FALSE);
--Inserting Object Columns for EIS_XXWC_OM_SO_FORM_ADOPTION_V
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_SO_FORM_ADOPTION_V','CUSTOM_SALES_ORDER',660,'Custom Sales Order','CUSTOM_SALES_ORDER','','','','SA059956','VARCHAR2','','','Custom Sales Order','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_SO_FORM_ADOPTION_V','CREATION_DATE',660,'Creation Date','CREATION_DATE','','','','SA059956','DATE','','','Creation Date','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_SO_FORM_ADOPTION_V','EMAIL_ADDRESS',660,'Email Address','EMAIL_ADDRESS','','','','SA059956','VARCHAR2','','','Email Address','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_SO_FORM_ADOPTION_V','LAST_NAME',660,'Last Name','LAST_NAME','','','','SA059956','VARCHAR2','','','Last Name','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_SO_FORM_ADOPTION_V','FIRST_NAME',660,'First Name','FIRST_NAME','','','','SA059956','VARCHAR2','','','First Name','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_SO_FORM_ADOPTION_V','NTID',660,'Ntid','NTID','','','','SA059956','VARCHAR2','','','Ntid','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_SO_FORM_ADOPTION_V','ORDER_TYPE',660,'Order Type','ORDER_TYPE','','','','SA059956','VARCHAR2','','','Order Type','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_SO_FORM_ADOPTION_V','ORDER_NUMBER',660,'Order Number','ORDER_NUMBER','','','','SA059956','NUMBER','','','Order Number','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_SO_FORM_ADOPTION_V','BRANCH_NAME',660,'Branch Name','BRANCH_NAME','','','','SA059956','VARCHAR2','','','Branch Name','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_SO_FORM_ADOPTION_V','BRANCH_NUMBER',660,'Branch Number','BRANCH_NUMBER','','','','SA059956','VARCHAR2','','','Branch Number','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_SO_FORM_ADOPTION_V','DISTRICT',660,'District','DISTRICT','','','','SA059956','VARCHAR2','','','District','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_SO_FORM_ADOPTION_V','REGION',660,'Region','REGION','','','','SA059956','VARCHAR2','','','Region','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_SO_FORM_ADOPTION_V','OPERATING_UNIT',660,'Operating Unit','OPERATING_UNIT','','','','SA059956','VARCHAR2','','','Operating Unit','','','','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_SO_FORM_ADOPTION_V','ADOPTION_PERCENTAGE',660,'Adoption Percentage','ADOPTION_PERCENTAGE','','','','SA059956','NUMBER','','','Adoption Percentage','','','','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_SO_FORM_ADOPTION_V','IMP_OVER_PREV_DAY',660,'Imp Over Prev Day','IMP_OVER_PREV_DAY','','','','SA059956','NUMBER','','','Imp Over Prev Day','','','','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_SO_FORM_ADOPTION_V','REGION_ADOPTION_PERCENTAGE',660,'Region Adoption Percentage','REGION_ADOPTION_PERCENTAGE','','','','SA059956','NUMBER','','','Region Adoption Percentage','','','','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_SO_FORM_ADOPTION_V','REGION_IMP_OVER_PREV_DAY',660,'Region Imp Over Prev Day','REGION_IMP_OVER_PREV_DAY','','','','SA059956','NUMBER','','','Region Imp Over Prev Day','','','','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_SO_FORM_ADOPTION_V','DISTRICT_ADOPTION_PERCENTAGE',660,'District Adoption Percentage','DISTRICT_ADOPTION_PERCENTAGE','','','','SA059956','NUMBER','','','District Adoption Percentage','','','','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_SO_FORM_ADOPTION_V','DISTRICT_IMP_OVER_PREV_DAY',660,'District Imp Over Prev Day','DISTRICT_IMP_OVER_PREV_DAY','','','','SA059956','NUMBER','','','District Imp Over Prev Day','','','','');
--Inserting Object Components for EIS_XXWC_OM_SO_FORM_ADOPTION_V
--Inserting Object Component Joins for EIS_XXWC_OM_SO_FORM_ADOPTION_V
ELSE
RAISE_APPLICATION_ERROR (-20001,'Module does not exist. Import/Create the module with id 660');
END IF;
END;
/
--There are no LOV/LOV TL entries for the report Custom Sales Order Form Adoption Reporting - WC
prompt Creating Report Data for Custom Sales Order Form Adoption Reporting - WC
set scan off define off
DECLARE
mod_exist varchar2(1);
BEGIN 
mod_exist := XXEIS.EIS_RSC_INSERT.check_if_module_exist(660);
IF mod_exist = 'Y' THEN 
--Deleting Report data - Custom Sales Order Form Adoption Reporting - WC
xxeis.eis_rsc_utility.delete_report_rows( 'Custom Sales Order Form Adoption Reporting - WC' );
--Inserting Report - Custom Sales Order Form Adoption Reporting - WC
xxeis.eis_rsc_ins.r( 660,'Custom Sales Order Form Adoption Reporting - WC','','Track Associate adoption of New Sales Order Form','','','','SA059956','EIS_XXWC_OM_SO_FORM_ADOPTION_V','Y','','','SA059956','','N','White Cap Reports','','Pivot Excel,','N','','','','','','N','','US','','','','');
--Inserting Report Columns - Custom Sales Order Form Adoption Reporting - WC
xxeis.eis_rsc_ins.rc( 'Custom Sales Order Form Adoption Reporting - WC',660,'BRANCH_NAME','Branch Name','Branch Name','','','default','','5','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_OM_SO_FORM_ADOPTION_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Custom Sales Order Form Adoption Reporting - WC',660,'BRANCH_NUMBER','Branch Number','Branch Number','','','default','','4','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_OM_SO_FORM_ADOPTION_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Custom Sales Order Form Adoption Reporting - WC',660,'CREATION_DATE','Creation Date','Creation Date','','','default','','12','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_OM_SO_FORM_ADOPTION_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Custom Sales Order Form Adoption Reporting - WC',660,'CUSTOM_SALES_ORDER','Custom Sales Order','Custom Sales Order','','','default','','13','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_OM_SO_FORM_ADOPTION_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Custom Sales Order Form Adoption Reporting - WC',660,'DISTRICT','District','District','','','default','','3','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_OM_SO_FORM_ADOPTION_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Custom Sales Order Form Adoption Reporting - WC',660,'EMAIL_ADDRESS','Email','Email Address','','','default','','11','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_OM_SO_FORM_ADOPTION_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Custom Sales Order Form Adoption Reporting - WC',660,'FIRST_NAME','First Name','First Name','','','default','','9','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_OM_SO_FORM_ADOPTION_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Custom Sales Order Form Adoption Reporting - WC',660,'LAST_NAME','Last Name','Last Name','','','default','','10','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_OM_SO_FORM_ADOPTION_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Custom Sales Order Form Adoption Reporting - WC',660,'NTID','NTID','Ntid','','','default','','8','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_OM_SO_FORM_ADOPTION_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Custom Sales Order Form Adoption Reporting - WC',660,'ORDER_NUMBER','Order Num','Order Number','','~~~','default','','6','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_OM_SO_FORM_ADOPTION_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Custom Sales Order Form Adoption Reporting - WC',660,'ORDER_TYPE','Order Type','Order Type','','','default','','7','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_OM_SO_FORM_ADOPTION_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Custom Sales Order Form Adoption Reporting - WC',660,'REGION','Region','Region','','','default','','2','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_OM_SO_FORM_ADOPTION_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Custom Sales Order Form Adoption Reporting - WC',660,'ADOPTION_PERCENTAGE','% Adoption','Adoption Percentage','','~~~','default','','14','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_OM_SO_FORM_ADOPTION_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Custom Sales Order Form Adoption Reporting - WC',660,'IMP_OVER_PREV_DAY','% Improvement over Previous Day','Imp Over Prev Day','','~~~','default','','15','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_OM_SO_FORM_ADOPTION_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Custom Sales Order Form Adoption Reporting - WC',660,'REGION_ADOPTION_PERCENTAGE','Region Adoption Percentage','Region Adoption Percentage','','~~~','default','','16','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_OM_SO_FORM_ADOPTION_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Custom Sales Order Form Adoption Reporting - WC',660,'REGION_IMP_OVER_PREV_DAY','Region Imp Over Prev Day','Region Imp Over Prev Day','','~~~','default','','17','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_OM_SO_FORM_ADOPTION_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Custom Sales Order Form Adoption Reporting - WC',660,'DISTRICT_ADOPTION_PERCENTAGE','District Adoption Percentage','District Adoption Percentage','','~~~','default','','18','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_OM_SO_FORM_ADOPTION_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Custom Sales Order Form Adoption Reporting - WC',660,'DISTRICT_IMP_OVER_PREV_DAY','District Imp Over Prev Day','District Imp Over Prev Day','','~~~','default','','19','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_OM_SO_FORM_ADOPTION_V','','','','US','');
--Inserting Report Parameters - Custom Sales Order Form Adoption Reporting - WC
xxeis.eis_rsc_ins.rp( 'Custom Sales Order Form Adoption Reporting - WC',660,'Branch Number','Branch Number','BRANCH_NUMBER','IN','','','VARCHAR2','N','Y','5','Y','Y','CONSTANT','SA059956','Y','N','','','','EIS_XXWC_OM_SO_FORM_ADOPTION_V','','','US','');
xxeis.eis_rsc_ins.rp( 'Custom Sales Order Form Adoption Reporting - WC',660,'District','District','DISTRICT','IN','','','VARCHAR2','N','Y','3','Y','Y','CONSTANT','SA059956','Y','N','','','','EIS_XXWC_OM_SO_FORM_ADOPTION_V','','','US','');
xxeis.eis_rsc_ins.rp( 'Custom Sales Order Form Adoption Reporting - WC',660,'Region','Region','REGION','IN','','','VARCHAR2','N','Y','4','Y','Y','CONSTANT','SA059956','Y','N','','','','EIS_XXWC_OM_SO_FORM_ADOPTION_V','','','US','');
xxeis.eis_rsc_ins.rp( 'Custom Sales Order Form Adoption Reporting - WC',660,'Fiscal Date From','Fiscal Date From','CREATION_DATE','>=','','','DATE','Y','Y','1','N','N','CONSTANT','SA059956','Y','N','','Start Date','','EIS_XXWC_OM_SO_FORM_ADOPTION_V','','','US','');
xxeis.eis_rsc_ins.rp( 'Custom Sales Order Form Adoption Reporting - WC',660,'Fiscal Date To','Fiscal Date To','CREATION_DATE','<=','','','DATE','Y','Y','2','N','N','CONSTANT','SA059956','Y','N','','End Date','','EIS_XXWC_OM_SO_FORM_ADOPTION_V','','','US','');
--Inserting Dependent Parameters - Custom Sales Order Form Adoption Reporting - WC
--Inserting Report Conditions - Custom Sales Order Form Adoption Reporting - WC
xxeis.eis_rsc_ins.rcnh( 'Custom Sales Order Form Adoption Reporting - WC',660,'EXOSFAV.BRANCH_NUMBER IN Branch Number','SIMPLE','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','BRANCH_NUMBER','','Branch Number','','','','','EIS_XXWC_OM_SO_FORM_ADOPTION_V','','','','','','IN','Y','Y','','','','','1',660,'Custom Sales Order Form Adoption Reporting - WC','EXOSFAV.BRANCH_NUMBER IN Branch Number');
xxeis.eis_rsc_ins.rcnh( 'Custom Sales Order Form Adoption Reporting - WC',660,'EXOSFAV.DISTRICT IN District','SIMPLE','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','DISTRICT','','District','','','','','EIS_XXWC_OM_SO_FORM_ADOPTION_V','','','','','','IN','Y','Y','','','','','1',660,'Custom Sales Order Form Adoption Reporting - WC','EXOSFAV.DISTRICT IN District');
xxeis.eis_rsc_ins.rcnh( 'Custom Sales Order Form Adoption Reporting - WC',660,'EXOSFAV.REGION IN Region','SIMPLE','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','REGION','','Region','','','','','EIS_XXWC_OM_SO_FORM_ADOPTION_V','','','','','','IN','Y','Y','','','','','1',660,'Custom Sales Order Form Adoption Reporting - WC','EXOSFAV.REGION IN Region');
--Inserting Report Sorts - Custom Sales Order Form Adoption Reporting - WC
xxeis.eis_rsc_ins.rs( 'Custom Sales Order Form Adoption Reporting - WC',660,'ORDER_NUMBER','ASC','SA059956','1','');
--Inserting Report Triggers - Custom Sales Order Form Adoption Reporting - WC
xxeis.eis_rsc_ins.rt( 'Custom Sales Order Form Adoption Reporting - WC',660,'begin
XXEIS.EIS_XXWC_INV_UTIL_PKG.G_DATE_FROM := :Fiscal Date From;
XXEIS.EIS_XXWC_INV_UTIL_PKG.G_DATE_TO   := :Fiscal Date To;
end;','B','Y','SA059956','BQ');
--inserting report templates - Custom Sales Order Form Adoption Reporting - WC
--Inserting Report Portals - Custom Sales Order Form Adoption Reporting - WC
--inserting report dashboards - Custom Sales Order Form Adoption Reporting - WC
--Exporting report views table information -  
xxeis.eis_rsc_ins.rviews( 'Custom Sales Order Form Adoption Reporting - WC','660','EIS_XXWC_OM_SO_FORM_ADOPTION_V','EIS_XXWC_OM_SO_FORM_ADOPTION_V','N','');
--inserting report security - Custom Sales Order Form Adoption Reporting - WC
xxeis.eis_rsc_ins.rsec( 'Custom Sales Order Form Adoption Reporting - WC','660','','XXWC_ORDER_MGMT_SUPER_USER',660,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'Custom Sales Order Form Adoption Reporting - WC','660','','XXWC_ORDER_MGMT_READ_SHIPPING',660,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'Custom Sales Order Form Adoption Reporting - WC','660','','XXWC_ORDER_MGMT_READ_ONLY_COST',660,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'Custom Sales Order Form Adoption Reporting - WC','660','','XXWC_ORDER_MGMT_READ_ONLY',660,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'Custom Sales Order Form Adoption Reporting - WC','660','','XXWC_ORDER_MGMT_PRICING_SUPER',660,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'Custom Sales Order Form Adoption Reporting - WC','660','','XXWC_ORDER_MGMT_PRICING_STD',660,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'Custom Sales Order Form Adoption Reporting - WC','660','','XXWC_ORDER_MGMT_PRICING_LTD',660,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'Custom Sales Order Form Adoption Reporting - WC','660','','XXWC_ORDER_MGMT_PRICING_FULL',660,'SA059956','','','');
--Inserting Report Pivots - Custom Sales Order Form Adoption Reporting - WC
xxeis.eis_rsc_ins.rpivot( 'Custom Sales Order Form Adoption Reporting - WC',660,'Branch Adoption Pivot','1','0,0|0,0,0','1,1,0,0|PivotStyleMedium9|2');
--Inserting Report Pivot Details For Pivot - Branch Adoption Pivot
xxeis.eis_rsc_ins.rpivot_dtls( 'Custom Sales Order Form Adoption Reporting - WC',660,'Branch Adoption Pivot','REGION','ROW_FIELD','','','1','1','');
xxeis.eis_rsc_ins.rpivot_dtls( 'Custom Sales Order Form Adoption Reporting - WC',660,'Branch Adoption Pivot','DISTRICT','ROW_FIELD','','','2','1','');
xxeis.eis_rsc_ins.rpivot_dtls( 'Custom Sales Order Form Adoption Reporting - WC',660,'Branch Adoption Pivot','ADOPTION_PERCENTAGE','DATA_FIELD','SUM','% ADOPTION','1','','xlNormal');
xxeis.eis_rsc_ins.rpivot_dtls( 'Custom Sales Order Form Adoption Reporting - WC',660,'Branch Adoption Pivot','IMP_OVER_PREV_DAY','DATA_FIELD','SUM','% IMPROVEMENT OVER PREVIOUS DAY','2','','xlNormal');
xxeis.eis_rsc_ins.rpivot_dtls( 'Custom Sales Order Form Adoption Reporting - WC',660,'Branch Adoption Pivot','BRANCH_NAME','ROW_FIELD','','','3','1','');
--Inserting Report Summary Calculation Columns For Pivot- Branch Adoption Pivot
xxeis.eis_rsc_ins.rpivot( 'Custom Sales Order Form Adoption Reporting - WC',660,'District Adoption Pivot','2','0,0|0,2,0','1,1,0,0|PivotStyleMedium9|2');
--Inserting Report Pivot Details For Pivot - District Adoption Pivot
xxeis.eis_rsc_ins.rpivot_dtls( 'Custom Sales Order Form Adoption Reporting - WC',660,'District Adoption Pivot','DISTRICT_ADOPTION_PERCENTAGE','DATA_FIELD','SUM','% ADOPTION','1','','xlNormal');
xxeis.eis_rsc_ins.rpivot_dtls( 'Custom Sales Order Form Adoption Reporting - WC',660,'District Adoption Pivot','DISTRICT_IMP_OVER_PREV_DAY','DATA_FIELD','SUM','% IMPROVEMENT OVER PREVIOUS DAY','2','','xlNormal');
xxeis.eis_rsc_ins.rpivot_dtls( 'Custom Sales Order Form Adoption Reporting - WC',660,'District Adoption Pivot','REGION','ROW_FIELD','','','1','1','');
xxeis.eis_rsc_ins.rpivot_dtls( 'Custom Sales Order Form Adoption Reporting - WC',660,'District Adoption Pivot','DISTRICT','ROW_FIELD','','','2','1','');
--Inserting Report Summary Calculation Columns For Pivot- District Adoption Pivot
xxeis.eis_rsc_ins.rpivot( 'Custom Sales Order Form Adoption Reporting - WC',660,'Region Adoption Pivot','3','0,0|0,0,0','1,1,0,0|PivotStyleMedium9|2');
--Inserting Report Pivot Details For Pivot - Region Adoption Pivot
xxeis.eis_rsc_ins.rpivot_dtls( 'Custom Sales Order Form Adoption Reporting - WC',660,'Region Adoption Pivot','REGION_ADOPTION_PERCENTAGE','DATA_FIELD','SUM','% ADOPTION','1','','xlNormal');
xxeis.eis_rsc_ins.rpivot_dtls( 'Custom Sales Order Form Adoption Reporting - WC',660,'Region Adoption Pivot','REGION_IMP_OVER_PREV_DAY','DATA_FIELD','SUM','% IMPROVEMENT OVER PREVIOUS DAY','2','','xlNormal');
xxeis.eis_rsc_ins.rpivot_dtls( 'Custom Sales Order Form Adoption Reporting - WC',660,'Region Adoption Pivot','REGION','ROW_FIELD','','','1','1','');
--Inserting Report Summary Calculation Columns For Pivot- Region Adoption Pivot
--Inserting Report   Version details- Custom Sales Order Form Adoption Reporting - WC
xxeis.eis_rsc_ins.rv( 'Custom Sales Order Form Adoption Reporting - WC','','Custom Sales Order Form Adoption Reporting - WC','SA059956');
ELSE
RAISE_APPLICATION_ERROR (-20001,'Module does not exist. Import/Create the module with id 660');
END IF;
END;
/
