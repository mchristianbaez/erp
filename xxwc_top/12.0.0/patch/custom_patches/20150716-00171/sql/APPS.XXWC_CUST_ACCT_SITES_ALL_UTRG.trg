CREATE OR REPLACE TRIGGER APPS.XXWC_CUST_ACCT_SITES_ALL_UTRG
   AFTER UPDATE
   ON ar.hz_cust_acct_sites_all
   REFERENCING NEW AS new OLD AS old
   FOR EACH ROW
DECLARE
   --PRAGMA autonomous_transaction;
   /**********************************************************************************************
    File Name: APPS.hz_cust_acct_sites_all_utrg

    PROGRAM TYPE: TRIGGER

    PURPOSE: Triiger to update tax exemptions into STEP taxware system

    HISTORY
    =============================================================================
           Last Update Date : 06/25/2012
    =============================================================================
    =============================================================================
    VERSION DATE          AUTHOR(S)               DESCRIPTION
    ------- -----------   ---------------         ----------------------------------------
    1.0     03-MAR-2012   Manny Rodriguez          Created.
    1.1     10-MAY-2012   Manny Rodriguez          blocking internationals.
    1.2     30-MAY-2013   Manny Rodriguez          lsec error message #397540
    1.3     18-AUG-2014   Manny Rodriguez          Changed variables for address to 30char   #260988
    1.4     16-OCT-2014   Maharajan Shunmugam     TMS#20141001-00161 Canada Multi Org changes
    1.5     06-APR-2015   Raghavendra S          TMS#20150317-00055 FIN xxwc_cust_acct_sites_all_utrg
                                                 trigger not having enough exceptions handled.
    1.6     10-JUN-2015   Pattabhi Avula         TMS#20150515-00081 FIN xxwc_cust_acct_sites_all_utrg
                         Commented the duplicate exception RAISE PROGRAM_ERROR
    1.7     05-02-2015   Nancy Pahwa             TMS#20150716-00171 TO_MANY-ROOWS validation
                                                need to be handled correctly
   ***********************************************************************************************/

   --initialize
   l_record_exists     VARCHAR2 (1) DEFAULT 'F';
   l_product_exists    VARCHAR2 (1) DEFAULT 'F';
   l_step_prod_exist   VARCHAR2 (1) DEFAULT 'F';
   l_prod_string       VARCHAR2 (500) DEFAULT NULL;
   l_party_name        VARCHAR2 (200) DEFAULT NULL;
   l_party_sitenum     VARCHAR2 (200) DEFAULT NULL;
   l_addr1             VARCHAR2 (30) DEFAULT NULL;
   l_addr2             VARCHAR2 (30) DEFAULT NULL;
   l_city              VARCHAR2 (100) DEFAULT NULL;
   l_state             VARCHAR2 (50) DEFAULT NULL;
   l_postal_code       VARCHAR2 (50) DEFAULT NULL;
   l_country           VARCHAR2 (10) DEFAULT NULL;
   l_specialrate       VARCHAR2 (50) DEFAULT 'F';
   l_tax_state         VARCHAR2 (50) DEFAULT NULL;
   l_tax_flex_value    VARCHAR2 (100) DEFAULT 'XXWC_TAX_EXEMPTION_TYPE';
   l_process           BOOLEAN DEFAULT TRUE;

   -- Error DEBUG
   l_sec               VARCHAR2 (150);
   l_error_msg         VARCHAR2 (2000);                    -- Added for V# 1.5
   l_err_callfrom      VARCHAR2 (75) DEFAULT 'xxwc_cust_acct_sites_all_utrg';
   l_distro_list       VARCHAR2 (75)
                          DEFAULT 'hdsoracledevelopers@hdsupply.com';
   -- l_distro_tax   VARCHAR2(75) DEFAULT 'hdstaxwaresupport@hdsupply.com';
   -- 1.7 start
   l_distro_tax        VARCHAR2 (75) DEFAULT 'credittaxexempt@whitecap.net;';
   -- 1.7 end
   l_host              VARCHAR2 (256) := 'mailoutrelay.hdsupply.net';
   l_hostport          VARCHAR2 (20) := '25';
   l_sender            VARCHAR2 (100);
   l_sid               VARCHAR2 (8);
   -- 1.7 Start
   l_party_id          NUMBER;
   l_party_number      VARCHAR (30);
   l_party_name1       VARCHAR (360);
   l_city1             VARCHAR (60);
   l_county1           VARCHAR (60);
   l_state1            VARCHAR (60);
   l_postal_code1      VARCHAR (60);
-- 1.7 end
BEGIN
   IF     (NVL (:new.attribute15, 'x') != NVL (:old.attribute15, 'x'))
      AND :new.org_id = 162
   THEN
      -- get email machine.
      SELECT LOWER (name) INTO l_sid FROM v$database;

      l_sender := 'Oracle.Applications_' || l_sid || '@hdsupply.com';

      -- 1.7 start
      IF l_sid IN ('EBSDEV', 'EBSQA')
      THEN
         l_distro_tax := 'Lenora.Allen@hdsupply.com;nancy.pahwa@hdsupply.com;neha.saini@hdsupply.com';
      ELSE
         l_distro_tax :=
            'credittaxexempt@whitecap.net;Leslie.glaize@hdsupply.com;Stacie.ganganna@hdsupply.com';
      END IF;

      -- 1.7 end


      l_sec := 'loading party site number.';

      BEGIN
         SELECT psite.party_site_number, l_party_id                      -- 1.7
           INTO l_party_sitenum, l_party_id                              -- 1.7
           FROM hz_party_sites psite
          WHERE psite.party_site_id = :new.party_site_id;
      EXCEPTION
         WHEN NO_DATA_FOUND
         THEN
            l_party_sitenum := NULL;
         --v1.7 start
         WHEN TOO_MANY_ROWS
         THEN
            SELECT party_number,
                   party_name,
                   city,
                   county,
                   state,
                   postal_code
              INTO l_party_number,
                   l_party_name1,
                   l_city1,
                   l_county1,
                   l_state1,
                   l_postal_code1
              FROM hz_parties hcaa
             WHERE party_id = l_party_id;

            l_error_msg :=
                  'Multiple/Duplicate Party Site Numbers for Party Site Id ='
               || :new.party_site_id;
            xxcus_misc_pkg.html_email (
               p_to              => l_distro_tax,
               p_from            => l_sender,
               p_text            => 'test',
               p_html            =>    '<p><strong>Multiple/Duplicate Party Site Numbers error on the Customer table related to taxware.</strong> <br>
  <br>'
                                    || '<p><strong> Customer Site Number:</strong> ('
                                    || l_party_sitenum
                                    || ') <br> <br>'
                                    || '<p><strong> Customer Account Number:</strong> ('
                                    || l_party_number
                                    || ')  <br> <br> '
                                    || '<p><strong> Customer Site Name:</strong> ('
                                    || l_party_name1
                                    || ') <br> <br> '
                                    || '<p><strong> City:</strong> ('
                                    || l_city1
                                    || ')  <br> <br> '
                                    || '<p><strong> County:</strong> ('
                                    || l_county1
                                    || ')  <br> <br> '
                                    || '<p><strong> State:</strong> ('
                                    || l_state1
                                    || ')  <br> <br> '
                                    || '<p><strong> Zip:</strong> ('
                                    || l_postal_code1
                                    || ')  </p>',
               p_subject         => 'STEPPROD_TBL errors',
               p_smtp_hostname   => l_host,
               p_smtp_portnum    => l_hostport);
         --v 1.7 end
         WHEN OTHERS
         THEN                              -- Added others Exception for V 1.5
            l_error_msg :=
                  'Multiple/Duplicate Party Site Numbers for Party Site Id ='
               || :new.party_site_id;
            RAISE PROGRAM_ERROR;
      END;


      l_sec :=
         'Checking to see if record already exists.  Will not overwrite if record exists.';

      BEGIN
         SELECT 'T'
           INTO l_record_exists
           FROM taxware.steptec_tbl
          WHERE key_1 = l_party_sitenum;
      EXCEPTION
         WHEN NO_DATA_FOUND
         THEN
            l_record_exists := 'F';
         WHEN TOO_MANY_ROWS
         THEN
            --Commented out 1.7
            /*           l_error_msg :=
                             'Multiple/Duplicate record already exists for Party in table steptec_tbl for party site Num='
                          || l_party_sitenum;
                       xxcus_misc_pkg.html_email (
                          p_to              => l_distro_tax
                         ,p_from            => l_sender
                         ,p_text            => 'test'
                         ,p_html            =>    '<p><strong>Error on the Customer table related to taxware.</strong><br>
             <br>Retreieved more certificates than expected for Party Site : </p>'
                                               || l_party_sitenum
                         ,p_subject         => 'ALERT** Tax trigger error during Certificate processing.'
                         ,p_smtp_hostname   => l_host
                         ,p_smtp_portnum    => l_hostport);*/

            --V 1.7 Start
            SELECT party_number,
                   party_name,
                   city,
                   county,
                   state,
                   postal_code
              INTO l_party_number,
                   l_party_name1,
                   l_city1,
                   l_county1,
                   l_state1,
                   l_postal_code1
              FROM hz_parties hcaa
             WHERE party_id = l_party_id;

            l_error_msg :=
                  'Multiple/Duplicate record already exists for Party in table steptec_tbl for party site Num='
               || l_party_sitenum;
            xxcus_misc_pkg.html_email (
               p_to              => l_distro_tax,
               p_from            => l_sender,
               p_text            => 'test',
               p_html            =>    '<p><strong>Multiple/Duplicate Party Site Numbers error on the Customer table related to taxware.</strong> <br>
  <br>'
                                    || '<p><strong> Customer Site Number:</strong> ('
                                    || l_party_sitenum
                                    || ') <br> <br>'
                                    || '<p><strong> Customer Account Number:</strong> ('
                                    || l_party_number
                                    || ')  <br> <br> '
                                    || '<p><strong> Customer Site Name:</strong> ('
                                    || l_party_name1
                                    || ') <br> <br> '
                                    || '<p><strong> City:</strong> ('
                                    || l_city1
                                    || ')  <br> <br> '
                                    || '<p><strong> County:</strong> ('
                                    || l_county1
                                    || ')  <br> <br> '
                                    || '<p><strong> State:</strong> ('
                                    || l_state1
                                    || ')  <br> <br> '
                                    || '<p><strong> Zip:</strong> ('
                                    || l_postal_code1
                                    || ')  </p>',
               p_subject         => 'STEPPROD_TBL errors',
               p_smtp_hostname   => l_host,
               p_smtp_portnum    => l_hostport);
         --v 1.7 end

         /*
              l_sec       := 'Tax trigger error during Certificate processing.';
                 xxcus_error_pkg.xxcus_error_main_api(p_called_from => l_err_callfrom,
                                                      p_calling => 'STEP Taxware Update Trigger',
                                                      --p_request_id => l_req_id,
                                                      p_ora_error_msg => 'Could not find a State for Partysite: '||l_party_sitenum,
                                                      p_error_desc => 'Error:  Retreieved more certificates than expected for '||l_sec||' for party site :'||l_party_sitenum,
                                                      p_distribution_list => l_distro_tax,
                                                      p_module => 'AR');       */
         --  RAISE PROGRAM_ERROR; --  Version# 1.6 Commented by Pattabhi on 10-Jun-2015 for TMS#20150515-00081
         WHEN OTHERS
         THEN                              -- Added others Exception for V 1.5
            l_error_msg :=
                  'checking record already exists for Party  site in table steptec_tbl for party site Num=='
               || l_party_sitenum;
            RAISE PROGRAM_ERROR;
      END;



      l_sec := 'Checking to see if products exists for the tax exemption.';

      BEGIN
         SELECT 'T'
           INTO l_product_exists
           FROM apps.fnd_flex_value_norm_hierarchy
          WHERE parent_flex_value = :new.attribute15 AND ROWNUM = 1;
      EXCEPTION
         WHEN NO_DATA_FOUND
         THEN
            l_product_exists := 'F';
         WHEN TOO_MANY_ROWS
         THEN
            --Comment this out v 1.7
            -- Added too_many_rows Exception for V 1.5
            /* l_error_msg :=
                  'Multiple/duplicate prodcut exists for Party in table fnd_flex_value_norm_hierarchy for Flex Value= '
               || :new.attribute15;
            RAISE PROGRAM_ERROR;*/
            --V 1.7 Start
            SELECT party_number,
                   party_name,
                   city,
                   county,
                   state,
                   postal_code
              INTO l_party_number,
                   l_party_name1,
                   l_city1,
                   l_county1,
                   l_state1,
                   l_postal_code1
              FROM hz_parties hcaa
             WHERE party_id = l_party_id;

            l_error_msg :=
                  'Multiple/duplicate product exists for Party in table fnd_flex_value_norm_hierarchy for Flex Value= '
               || :new.attribute15;
            xxcus_misc_pkg.html_email (
               p_to              => l_distro_tax,
               p_from            => l_sender,
               p_text            => 'test',
               p_html            =>    '<p><strong>Multiple/Duplicate Party Site Numbers error on the Customer table related to taxware.</strong> <br>
  <br>'
                                    || '<p><strong> Customer Site Number:</strong> ('
                                    || l_party_sitenum
                                    || ') <br> <br>'
                                    || '<p><strong> Customer Account Number:</strong> ('
                                    || l_party_number
                                    || ')  <br> <br> '
                                    || '<p><strong> Customer Site Name:</strong> ('
                                    || l_party_name1
                                    || ') <br> <br> '
                                    || '<p><strong> City:</strong> ('
                                    || l_city1
                                    || ')  <br> <br> '
                                    || '<p><strong> County:</strong> ('
                                    || l_county1
                                    || ')  <br> <br> '
                                    || '<p><strong> State:</strong> ('
                                    || l_state1
                                    || ')  <br> <br> '
                                    || '<p><strong> Zip:</strong> ('
                                    || l_postal_code1
                                    || ')  </p>',
               p_subject         => 'STEPPROD_TBL errors',
               p_smtp_hostname   => l_host,
               p_smtp_portnum    => l_hostport);
         --v 1.7 end
         WHEN OTHERS
         THEN                              -- Added others Exception for V 1.5
            l_error_msg :=
                  'checking prodcut exists for Party in table fnd_flex_value_norm_hierarchy for Flex Value='
               || :new.attribute15;
            RAISE PROGRAM_ERROR;
      END;



      l_sec :=
         'Checking to see if products exists in STEP for the tax exemption.';

      BEGIN
         SELECT 'T'
           INTO l_step_prod_exist
           FROM taxware.stepprod_tbl stepprods
          WHERE stepprods.key_1 = l_party_sitenum;
      EXCEPTION
         WHEN NO_DATA_FOUND
         THEN
            l_step_prod_exist := 'F';
         WHEN TOO_MANY_ROWS
         THEN
            -- commented out v1.7
            -- Added too_many_rows Exception for V 1.5
            /* l_error_msg :=
                  'Multiple/duplicate prodcut exists in STEP for tax exemption in STEPPROD_TBL for Party site num='
               || l_party_sitenum;
            RAISE PROGRAM_ERROR;*/
            --V 1.7 Start
            SELECT party_number,
                   party_name,
                   city,
                   county,
                   state,
                   postal_code
              INTO l_party_number,
                   l_party_name1,
                   l_city1,
                   l_county1,
                   l_state1,
                   l_postal_code1
              FROM hz_parties hcaa
             WHERE party_id = l_party_id;

            l_error_msg :=
                  'Multiple/duplicate product exists in STEP for tax exemption in STEPPROD_TBL for Party site num='
               || l_party_sitenum;
            xxcus_misc_pkg.html_email (
               p_to              => l_distro_tax,
               p_from            => l_sender,
               p_text            => 'test',
               p_html            =>    '<p><strong>Multiple/Duplicate Party Site Numbers error on the Customer table related to taxware.</strong> <br>
  <br>'
                                    || '<p><strong> Customer Site Number:</strong> ('
                                    || l_party_sitenum
                                    || ') <br> <br>'
                                    || '<p><strong> Customer Account Number:</strong> ('
                                    || l_party_number
                                    || ')  <br> <br> '
                                    || '<p><strong> Customer Site Name:</strong> ('
                                    || l_party_name1
                                    || ') <br> <br> '
                                    || '<p><strong> City:</strong> ('
                                    || l_city1
                                    || ')  <br> <br> '
                                    || '<p><strong> County:</strong> ('
                                    || l_county1
                                    || ')  <br> <br> '
                                    || '<p><strong> State:</strong> ('
                                    || l_state1
                                    || ')  <br> <br> '
                                    || '<p><strong> Zip:</strong> ('
                                    || l_postal_code1
                                    || ')  </p>',
               p_subject         => 'STEPPROD_TBL errors',
               p_smtp_hostname   => l_host,
               p_smtp_portnum    => l_hostport);
         --v 1.7 end

         WHEN OTHERS
         THEN                              -- Added others Exception for V 1.5
            l_error_msg :=
                  'Checking prodcut exists in STEP for tax exemption in STEPPROD_TBL for Party site num='
               || l_party_sitenum;
            RAISE PROGRAM_ERROR;
      END;



      l_sec := 'Check to see if this is a TEC with special rates.';

      -- l_specialrate will be ='F' when there are no special rates(most cases)
      BEGIN
         SELECT ffv.attribute1
           INTO l_specialrate
           FROM apps.fnd_flex_values ffv, apps.fnd_flex_value_sets fvs
          WHERE     ffv.flex_value_set_id = fvs.flex_value_set_id
                AND fvs.flex_value_set_name = l_tax_flex_value
                AND ffv.flex_value = :new.attribute15
                AND ffv.attribute1 IS NOT NULL;
      EXCEPTION
         WHEN NO_DATA_FOUND
         THEN
            l_specialrate := 'F';
         WHEN OTHERS
         THEN                              -- Added others Exception for V 1.5
            l_error_msg :=
                  'Checking TEC with special rates in fnd_flex_values for Party site Special rate ='
               || :new.attribute15;

            RAISE PROGRAM_ERROR;
      END;



      l_sec := 'the point where I update the main for this record to STEP.';

      SELECT SUBSTR (party.party_name, 1, 30),
             SUBSTR (loc.address1, 1, 30),
             SUBSTR (loc.address2, 1, 30),
             SUBSTR (loc.city, 1, 26),
             loc.state,
             SUBSTR (loc.postal_code, 1, 5),
             loc.country
        INTO l_party_name,
             l_addr1,
             l_addr2,
             l_city,
             l_state,
             l_postal_code,
             l_country
        FROM hz_parties party,
             hz_cust_accounts acct,
             hz_party_sites psite,
             hz_locations loc
       WHERE     party.party_id = acct.party_id
             AND acct.cust_account_id = :new.cust_account_id
             AND acct.party_id = party.party_id
             AND psite.party_site_id = :new.party_site_id
             AND psite.location_id = loc.location_id;



      BEGIN
         SELECT stcode
           INTO l_tax_state
           FROM taxware.stepstcn_tbl
          WHERE stalphacode = l_state;
      EXCEPTION
         WHEN NO_DATA_FOUND
         THEN
            l_process := FALSE;

            xxcus_misc_pkg.html_email (
               p_to              => l_distro_tax,
               p_from            => l_sender,
               p_text            => 'test',
               p_html            =>    '<p><strong>Could not find a state in STEP.</strong><br>
  <br>Could not find an associated STATE record in STEP for Party Site : </p>'
                                    || l_party_sitenum,
               p_subject         => 'ALERT** Tax trigger error during Certificate processing.',
               p_smtp_hostname   => l_host,
               p_smtp_portnum    => l_hostport);
         /*        xxcus_error_pkg.xxcus_error_main_api(p_called_from => l_err_callfrom,
                                                      p_calling => 'STEP Taxware',
                                                      --p_request_id => l_req_id,
                                                      p_ora_error_msg => SQLERRM,
                                                      p_error_desc => 'Error: could not find a state in STEP '||l_sec||' for party site :'||l_party_sitenum,
                                                      p_distribution_list => l_distro_tax,
                                                      p_module => 'AR');    */
         WHEN OTHERS
         THEN                              -- Added others Exception for V 1.5
            l_error_msg := ' Duplicate Data for St Code=' || l_state;
            RAISE PROGRAM_ERROR;
      END;



      --v1.1
      IF l_country = 'US' AND l_process
      THEN
         l_sec := 'the point where I actually do the update';

         IF l_record_exists = 'T'
         THEN
            UPDATE taxware.steptec_tbl step
               SET step.sstatecode = l_tax_state,           --step.sstatecode,
                   step.cproductflag =
                      DECODE (l_product_exists,
                              'T', DECODE (l_specialrate, 'F', 'I', NULL),
                              NULL), -- (IF products exist THEN I ELSE null)  --step.cproductflag,
                   step.scustomeraddr1 = l_addr1,       --step.scustomeraddr1,
                   step.scustomeraddr2 = l_addr2,       --step.scustomeraddr2,
                   step.scustomerctyname = l_city,    --step.scustomerctyname,
                   step.scustomerstatecode = l_state, --step.scustomerstatecode,
                   step.cactivecompflag =
                      DECODE (NVL (:new.attribute15, 'None'),
                              'None', 'D',
                              'A'),
                   step.scustomerzipcode = l_postal_code, --step.scustomerzipcode,
                   step.cspecialrateflag =
                      DECODE (l_specialrate, 'F', NULL, 'Y') -- handles special rate
             WHERE step.key_1 = l_party_sitenum;
         ELSE                                         -- new record, do insert
            INSERT INTO taxware.steptec_tbl step (step.scompanyid,
                                                  step.key_1,
                                                  step.ctransactflag,
                                                  step.key_2,
                                                  step.key_3,
                                                  step.sstatecode,
                                                  step.cjurislevel,
                                                  step.skeyoccurnum,
                                                  step.cproductflag,
                                                  step.cdeflttostcert,
                                                  step.cchkexpirdate,
                                                  step.scustomername,
                                                  step.scustomeraddr1,
                                                  step.scustomeraddr2,
                                                  step.scustomerctyname,
                                                  step.scustomerstatecode,
                                                  step.scustomerzipcode,
                                                  step.scustomeravpgeo,
                                                  step.staxcertifnum,
                                                  step.sreasoncode,
                                                  step.datecertifreceived,
                                                  step.dateeffective,
                                                  step.dateexpiration,
                                                  step.cactivecompflag,
                                                  step.keycode,
                                                  step.cspecialrateflag)
                    VALUES (
                              'WCI',                        --step.scompanyid,
                              l_party_sitenum,                   --step.key_1,
                              ' ',                      -- step.ctransactflag,
                              ' ',                              -- step.key_2,
                              ' ',                              -- step.key_3,
                              l_tax_state,                  --step.sstatecode,
                              1,                           --step.cjurislevel,
                              0,                          --step.skeyoccurnum,
                              DECODE (
                                 l_product_exists,
                                 'T', DECODE (l_specialrate, 'F', 'I', NULL),
                                 NULL), -- (IF products exist THEN I ELSE null)  --step.cproductflag,
                              'D',                      --step.cdeflttostcert,
                              NULL,                      --step.cchkexpirdate,
                              l_party_name,              --step.scustomername,
                              l_addr1,                  --step.scustomeraddr1,
                              l_addr2,                  --step.scustomeraddr2,
                              l_city,                 --step.scustomerctyname,
                              l_state,              --step.scustomerstatecode,
                              l_postal_code,          --step.scustomerzipcode,
                              NULL,                    --step.scustomeravpgeo,
                              'N/A',                     --step.staxcertifnum,
                              'BW',                        --step.sreasoncode,
                              SYSDATE,              --step.datecertifreceived,
                              SYSDATE,                   --step.dateeffective,
                              NULL,     --SYSDATE+1460,  --step.dateexpiration
                              DECODE (NVL (:new.attribute15, 'None'),
                                      'None', 'D',
                                      'A'), --step.cactivecompflag V1.1 Change to A. Cert complete =CC, Cert Active =CA || A(CC ON,CA ON), B(CC OFF,CA ON), C(CC ON,CA ON) D(CC OFF, CA OFF)
                              'R',                             --,step.keycode
                              DECODE (l_specialrate, 'F', NULL, 'Y') -- handles special rate
                                                                    );
         END IF;

         IF l_product_exists = 'T'
         THEN
            BEGIN
               DELETE FROM taxware.steprate_tbl
                     WHERE key_1 = l_party_sitenum;
            EXCEPTION
               WHEN OTHERS
               THEN
                  NULL; -- no action, no rows to delete, so must be a new change to special rate.
            END;

            FOR c_products IN (                  --query to load product codes
                               SELECT child_flex_value_low
                                 FROM apps.fnd_flex_value_norm_hierarchy
                                WHERE parent_flex_value = :new.attribute15)
            LOOP
               l_prod_string :=
                  l_prod_string || RPAD (c_products.child_flex_value_low, 25);

               --special rates are actually normalized and not rpadd'ed.  wierd.
               IF l_specialrate != 'F'
               THEN
                  -- if special rates
                  INSERT INTO taxware.steprate_tbl c (c.companyid,
                                                      c.key_1,
                                                      c.stcode,
                                                      c.steptecoccurnumber,
                                                      c.productcode,
                                                      c.basispercent,
                                                      c.federaltaxrate,
                                                      c.statetaxrate,
                                                      c.secstatetaxrate,
                                                      c.countytaxrate,
                                                      c.citytaxrate,
                                                      c.seccountytaxrate,
                                                      c.seccitytaxrate,
                                                      c.districttaxrate,
                                                      c.countytaxind)
                       VALUES ('WCI',
                               l_party_sitenum,                          --key
                               l_tax_state,                        --statecode
                               0,
                               c_products.child_flex_value_low, --product code
                               1,                                     --basis,
                               0,                                 --fedtaxrate
                               0,                               --statetaxrate
                               0,                            --secstatetaxrate
                               TO_NUMBER (l_specialrate),      --countytaxrate
                               0,                                --citytaxrate
                               0,                             --seccounty rate
                               0,                               --seccity rate
                               0,                          --district tax rate
                               1                             --county tax rate
                                );
               END IF;
            END LOOP;


            l_sec :=
               'the point where I update the STEP CHILD for this record to STEP.';

            -- update  PRODUCT CODE TABLE.
            IF l_step_prod_exist = 'T'
            THEN
               IF l_specialrate = 'F'
               THEN
                  --if not special rate
                  UPDATE taxware.stepprod_tbl prod
                     SET prod.stcode =
                            (SELECT stcode
                               FROM taxware.stepstcn_tbl
                              WHERE stalphacode = l_state),     --prod.stcode,
                         prod.productentries = l_prod_string --prod.productentries
                   WHERE prod.key_1 = l_party_sitenum;
               END IF;
            ELSE
               IF l_specialrate = 'F'
               THEN
                  -- if not special rate
                  INSERT
                    INTO taxware.stepprod_tbl prod (prod.companyid,
                                                    prod.key_1,
                                                    prod.stcode,
                                                    prod.jurisdictionlevel,
                                                    prod.steptecoccurnumber,
                                                    prod.stepprodoccurnumber,
                                                    prod.productentries)
                  VALUES ('WCI',                             --prod.companyid,
                          l_party_sitenum,                       --prod.key_1,
                          (SELECT stcode
                             FROM taxware.stepstcn_tbl
                            WHERE stalphacode = l_state),       --prod.stcode,
                          1,                         --prod.jurisdictionlevel,
                          0,                        --prod.steptecoccurnumber,
                          1,                       --prod.stepprodoccurnumber,
                          l_prod_string                  --prod.productentries
                                       );
               END IF;
            END IF;
         ELSE           -- products in oracle do not exist, make taxware match
            DELETE FROM taxware.stepprod_tbl prod
                  WHERE prod.key_1 = l_party_sitenum;
         END IF;
      END IF;
   END IF;
EXCEPTION
   WHEN PROGRAM_ERROR
   THEN                                                     -- Added for V 1.5
      xxcus_error_pkg.xxcus_error_main_api (
         p_called_from         => l_err_callfrom,
         p_calling             => l_sec,           --p_request_id => l_req_id,
         p_ora_error_msg       => l_error_msg || ' Error ' || SQLERRM,
         p_error_desc          =>    'Error insert into values into Step at '
                                  || l_sec
                                  || ' for Party Site Number='
                                  || l_party_sitenum
                                  || ' With New Party Site Id = '
                                  || :new.party_site_id,
         p_distribution_list   => l_distro_list,
         p_module              => 'AR');
   WHEN OTHERS
   THEN
      xxcus_error_pkg.xxcus_error_main_api (
         p_called_from         => l_err_callfrom,
         p_calling             => l_sec,           --p_request_id => l_req_id,
         p_ora_error_msg       => l_error_msg || ' Error ' || SQLERRM,
         p_error_desc          =>    'Error insert into values into Step at '
                                  || l_sec
                                  || ' for Party Site Number='
                                  || l_party_sitenum
                                  || ' With New Party Site Id = '
                                  || :new.party_site_id,
         p_distribution_list   => l_distro_list,
         p_module              => 'AR');
END;
/