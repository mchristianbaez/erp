/*
 TMS:   20181001-00002
 Date:  10/03/2018
 Notes: Backup the fixed asset records table with wrong period_name column,copy them into backup table and update the base table with proper period names.
 Author: Ashwin Sridhar
*/
SET SERVEROUTPUT ON SIZE 1000000
DECLARE
 --
ln_fyear_2018_count   NUMBER;
ln_fyear_2019_count   NUMBER;
ln_fiscal_2018_count  NUMBER;
ln_fiscal_2019_count  NUMBER;
ln_fiscal_2019_count1 NUMBER;
ln_total              NUMBER;
 --

BEGIN --Main Processing...

  --Updating the records for the incorrect periods and year...
  DBMS_OUTPUT.put_line ('Start Updating the records for the incorrect periods and year.');
  
  SELECT COUNT(1)
  INTO   ln_fyear_2018_count
  FROM   fa.fa_fiscal_year
  WHERE  1 = 1 
  AND    fiscal_year = 2018 
  AND    end_date    = '27-Jan-2019';
  
  DBMS_OUTPUT.put_line ('Fiscal Year 2018 records count '||ln_fyear_2018_count);
  
  IF ln_fyear_2018_count>0 THEN
  
    --Fix end_date for 2018 which is wrong
    UPDATE fa.fa_fiscal_year
    SET    end_date    = '03-Feb-2019'
    WHERE  fiscal_year = 2018 
    AND    end_date    = '27-Jan-2019';
  
  END IF;

  --
  
  SELECT COUNT(1)
  INTO   ln_fyear_2019_count
  FROM   fa.fa_fiscal_year
  WHERE  1 = 1
  AND    fiscal_year = 2019
  AND    start_date = '28-Jan-2019'
  AND    end_date = '26-Jan-2020';
  
  DBMS_OUTPUT.put_line ('Fiscal Year 2019 records count '||ln_fyear_2019_count);   
  
  IF ln_fyear_2019_count>0 THEN
  
    --Fix start_date and end_date for 2020 Since both are wrong
    UPDATE fa.fa_fiscal_year
    SET    start_date  = '04-Feb-2019'
    ,      end_date    = '02-Feb-2020'
    WHERE  fiscal_year = 2019
    AND    start_date  = '28-Jan-2019'
    AND    end_date    = '26-Jan-2020';

  END IF;
  
  SELECT COUNT(1)
  INTO   ln_fiscal_2018_count
  FROM   fa.fa_calendar_periods
  WHERE  period_name = 'Jan-2018' 
  AND    end_date    = '27-Jan-2019';
  
  DBMS_OUTPUT.put_line ('Financial Year 2018 records count '||ln_fiscal_2018_count);
  
  IF ln_fiscal_2018_count>0 THEN
  
    --Fix last Fiscal Period of 2018 which is Jan-2018
    UPDATE fa.fa_calendar_periods
    SET    end_date    = '03-Feb-2019'
    WHERE  period_name = 'Jan-2018';
 
  END IF;
  
  SELECT COUNT(1)
  INTO   ln_fiscal_2019_count
  FROM   fa.fa_calendar_periods
  WHERE  period_name = 'Feb-2019' 
  AND    start_date  = '28-Jan-2019';
  
  DBMS_OUTPUT.put_line ('Financial Year 2019 -Feb records count '||ln_fiscal_2019_count);
  
  IF ln_fiscal_2019_count>0 THEN
  
    --Fix First Fiscal Period of 2019 which is Feb-2019
    UPDATE fa.fa_calendar_periods
    SET   start_date  = '04-Feb-2019'
    WHERE period_name = 'Feb-2019' 
    AND   start_date  = '28-Jan-2019';

  END IF;
  
  SELECT COUNT(1)
  INTO  ln_fiscal_2019_count1
  FROM  fa.fa_calendar_periods
  WHERE period_name = 'Jan-2019' 
  AND   end_date = '26-Jan-2020';
  
  DBMS_OUTPUT.put_line ('Financial Year 2019 -Jan records count '||ln_fiscal_2019_count);
  
  IF ln_fiscal_2019_count1>0 THEN
  
    UPDATE fa.fa_calendar_periods
    SET    end_date    = '02-Feb-2020'
    WHERE  period_name = 'Jan-2019' 
    AND    end_date    = '26-Jan-2020';
 
  END IF;
 
  COMMIT;
  
  ln_total:=ln_fyear_2018_count+ln_fyear_2019_count+ln_fiscal_2018_count+ln_fiscal_2019_count+ln_fiscal_2019_count1;
  
  --
  DBMS_OUTPUT.put_line ('Total Records updated for incorrect period and fiscal year.'||ln_total);
  
  DBMS_OUTPUT.put_line ('End Updating the records for the incorrect period and fiscal year...');
  
EXCEPTION
WHEN others THEN
      
  DBMS_OUTPUT.put_line ('TMS: 20181001-00002, Errors =' || SQLERRM);
  
  ROLLBACK;

END;
/