----------------------------------------------------------------------------------------------------
/***************************************************************************************************
  $Header XXWC_MTL_SYSTEM_ITM_B_N32
  File Name: XXWC_MTL_SYSTEM_ITM_B_N32.sql
  PURPOSE:   
  REVISIONS:
     Ver          Date         Author         Description
     ---------  ----------   --------    ------------------------------------------------------------
     1.0        07-Jul-16    Siva        	TMS#20160426-00032  
*****************************************************************************************************/
CREATE INDEX INV.XXWC_MTL_SYSTEM_ITM_B_N32 ON 
	INV.MTL_SYSTEM_ITEMS_B (INVENTORY_ITEM_ID,ORGANIZATION_ID,SOURCE_TYPE) TABLESPACE APPS_TS_TX_DATA
/

