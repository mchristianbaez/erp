CREATE OR REPLACE FORCE VIEW APPS.XXWC_MTL_SYSTEM_ITEMS_EHS_V
/*************************************************************************
   *   $Header XXWC_MTL_SYSTEM_ITEMS_EHS_V
   *   Module Name: XXWC_MTL_SYSTEM_ITEMS_EHS_V
   *
   *   PURPOSE:   View usedfor Item and Item Attributes via Web ADI Upload and Download- TMS#20141229-00015 
   *   Added these fields for un_number_id,hazard_class_id,cas_number and hazardous_material_flag for TMS# 20141229-00015 
   *   REVISIONS:
   *   Ver        Date        Author                     Description
   *   ---------  ----------  ---------------         -------------------------
   *   1.0        11/02/2015  Gajendra M                Initial Version
   *   2.0        04/17/2018  Naveen Kalidindi          TMS#20171011-00160 - Prop 65 Warning Message 
   *													Oracle Compliance changes
   *   3.0        08/10/2018  Naveen Kalidindi          TMS#20180806-00062 - Prop 65 WebADI change for incorporating Custom Table
   * ***************************************************************************/
(
   ORGANIZATION_ID
  ,ORGANIZATION_CODE
  ,SEGMENT1
  ,ATTRIBUTE17
  ,ATTRIBUTE18
  ,ATTRIBUTE8
  ,ATTRIBUTE11
  ,ATTRIBUTE9
  ,ATTRIBUTE3
  ,ATTRIBUTE5
  ,ATTRIBUTE1
  ,ATTRIBUTE4
  ,ATTRIBUTE6
  ,ATTRIBUTE7
  ,UN_NUMBER_ID
  ,HAZARD_CLASS_ID
  ,CAS_NUMBER
  ,HAZARDOUS_MATERIAL_FLAG
  ,PROP65_CANCER_MSG
  ,PROP65_REPRODUCTIVE_MSG
  ,PROP65_SHORT_FORM_TEXT
)
AS
 SELECT a.organization_id
      ,a.organization_code
      ,b.segment1
      ,b.attribute17
      ,b.attribute18
      ,b.attribute8
      ,b.attribute11
      ,b.attribute9
      ,b.attribute3
      ,b.attribute5
      ,b.attribute1
      ,b.attribute4
      ,b.attribute6
      ,b.attribute7
      ,b.un_number_id
      ,b.hazard_class_id
      ,b.cas_number
      ,b.hazardous_material_flag
      ,prop_65_cancer   prop65_cancer_msg
      ,prop_65_reproductive prop65_reproductive_msg
      ,prop_65_short_form prop65_short_form_text
  FROM mtl_parameters a
      ,mtl_system_items b
      ,xxwc.xxwc_item_ehs_attributes ehs
 WHERE a.organization_id = b.organization_id
   AND b.inventory_item_id = ehs.inventory_item_id(+)
   AND ehs.organization_id (+) = 222;

