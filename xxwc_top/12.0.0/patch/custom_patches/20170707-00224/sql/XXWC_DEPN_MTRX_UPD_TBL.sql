/*************************************************************************
  $Header XXWC_DEPN_MTRX_UPD_TBL.sql $
  Module Name: XXWC_DEPN_MTRX_UPD_TBL

  PURPOSE: Table to maintain forms and reports modified history.

  REVISIONS:
  Ver        Date         Author                Description
  ---------  -----------  ------------------    ---------------------------------------------------------------------------------------
  1.1        15-Jun-2017  P.vamshidhar          TMS#20170707-00224 - Custom Dependency Matrix
*******************************************************************************************************************************************/ 
 CREATE TABLE XXWC.XXWC_DEPN_MTRX_UPD_TBL
(
  FILE_MONTH        VARCHAR2(64 BYTE),
  FILE_DAY          VARCHAR2(64 BYTE),
  FILE_TIME_YEAR    VARCHAR2(64 BYTE),
  FILE_NAME         VARCHAR2(612 BYTE),
  MODIFIED_DATE     DATE,
  OBJECT_NAME       VARCHAR2(100 BYTE),
  OBJECT_TYPE       VARCHAR2(100 BYTE)
);