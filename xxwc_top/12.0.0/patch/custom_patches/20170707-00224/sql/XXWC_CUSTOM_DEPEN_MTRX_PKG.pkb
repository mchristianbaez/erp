CREATE OR REPLACE PACKAGE BODY APPS.XXWC_CUSTOM_DEPEN_MTRX_PKG
IS
   g_err_callfrom         VARCHAR2 (75) DEFAULT 'XXWC_CUSTOM_DEPEN_MTRX_PKG';
   g_distro_list          VARCHAR2 (75) DEFAULT 'HDSOracleDevelopers@hdsupply.com';
   g_ln_count             NUMBER;
   gvc_txt_files_folder   VARCHAR2 (1000) := 'XXWC_CUST_DEPN_MTRX_NONDB_DIR';

   -- ==========================================================================================================
   -- Package: XXWC_CUSTOM_DEPEN_MTRX_PKG.pkb
   -- Procedure: Main
   -- ==========================================================================================================
   -- VERSION DATE          AUTHOR(S)       DESCRIPTION
   -- ------- -----------   --------------- --------------------------------------------------------------------
   -- 1.0     07-JUL-2017   P.Vamshidhar   TMS#20170707-00224 - Custom Dependency Matrix
   -- ==========================================================================================================

   PROCEDURE MAIN (x_errbuf         OUT VARCHAR2,
                   x_retcode        OUT NUMBER,
                   p_from_date   IN     VARCHAR2,
                   p_source      IN     VARCHAR2)
   IS
      l_procedure    VARCHAR2 (100) := 'MAIN';
      l_sec          VARCHAR2 (100) := '';
      l_err_msg      VARCHAR2 (1000);
      lvc_errbuf     VARCHAR2 (1000);
      lvc_retcode    VARCHAR2 (10);
      ld_from_date   DATE;
   BEGIN
      ld_from_date := TO_DATE (p_from_date, 'YYYY/MM/DD HH24:MI:SS');
      FND_FILE.PUT_LINE (FND_FILE.LOG, ' ld_from_date: ' || ld_from_date);
      --
      FND_FILE.PUT_LINE (FND_FILE.LOG, 'Deleting Data ');
      delete_data (x_errbuf      => lvc_errbuf,
                   x_retcode     => lvc_retcode,
                   p_from_date   => ld_from_date,
                   p_source      => p_source);
      FND_FILE.PUT_LINE (FND_FILE.LOG, 'Deleting Data Completed ');

      FND_FILE.PUT_LINE (FND_FILE.LOG, 'Updating Data Begin');
      data_update;
      FND_FILE.PUT_LINE (FND_FILE.LOG, 'Updating Data End');

      IF p_source = 'ALL'
      THEN
         FND_FILE.PUT_LINE (FND_FILE.LOG, 'Process started ');
         -- populating db_source objects
         FND_FILE.PUT_LINE (FND_FILE.LOG, 'Loading Objects from DBA Source ');
         load_db_source (x_errbuf, x_retcode, ld_from_date);
         -- populating database objects
         load_db_objects (x_errbuf, x_retcode, ld_from_date);
         FND_FILE.PUT_LINE (FND_FILE.LOG, 'Loading form objects ');
         load_oracle_forms (x_errbuf, x_retcode, ld_from_date);
         FND_FILE.PUT_LINE (FND_FILE.LOG, 'Loading Library objects ');
         load_oracle_plls (x_errbuf, x_retcode, ld_from_date);
         FND_FILE.PUT_LINE (FND_FILE.LOG, 'Loading report objects ');
         load_oracle_reports (x_errbuf, x_retcode, ld_from_date);
         FND_FILE.PUT_LINE (FND_FILE.LOG, 'Loading Workflow objects ');
         load_wf_objects (x_errbuf, x_retcode, ld_from_date);
         FND_FILE.PUT_LINE (FND_FILE.LOG, 'Process Completed ');
         FND_FILE.PUT_LINE (FND_FILE.LOG, 'Loading Host job objects ');
         load_host_objects (x_errbuf, x_retcode, ld_from_date);
      ELSIF p_source = 'Database Objects'
      THEN
         -- populating db_source objects
         FND_FILE.PUT_LINE (FND_FILE.LOG, 'DB Sources Started ');
         load_db_source (x_errbuf, x_retcode, ld_from_date);
         FND_FILE.PUT_LINE (FND_FILE.LOG, 'DB Objects Started ');
         -- populating database objects
         load_db_objects (x_errbuf, x_retcode, ld_from_date);
         FND_FILE.PUT_LINE (FND_FILE.LOG, 'DB Objects Completed ');
      ELSIF p_source = 'Oracle Forms'
      THEN
         FND_FILE.PUT_LINE (FND_FILE.LOG, 'Oracle Forms Started ');
         load_oracle_forms (x_errbuf, x_retcode, ld_from_date);
         FND_FILE.PUT_LINE (FND_FILE.LOG, 'Oracle Forms Completed ');
      ELSIF p_source = 'Oracle Reports'
      THEN
         FND_FILE.PUT_LINE (FND_FILE.LOG, 'Oracle Reports Started ');
         load_oracle_reports (x_errbuf, x_retcode, ld_from_date);
         FND_FILE.PUT_LINE (FND_FILE.LOG, 'Oracle Reports Completed ');
      ELSIF p_source = 'Workflows'
      THEN
         FND_FILE.PUT_LINE (FND_FILE.LOG, 'Workflows Started ');
         load_wf_objects (x_errbuf, x_retcode, ld_from_date);
         FND_FILE.PUT_LINE (FND_FILE.LOG, 'Workflows Completed ');
      ELSIF p_source = 'Libraries'
      THEN
         FND_FILE.PUT_LINE (FND_FILE.LOG, 'Libraries Started ');
         load_oracle_plls (x_errbuf, x_retcode, ld_from_date);
         FND_FILE.PUT_LINE (FND_FILE.LOG, 'Libraries Completed ');
      ELSIF p_source = 'Oracle Host'
      THEN
         FND_FILE.PUT_LINE (FND_FILE.LOG, 'Host Objects Started ');
         load_host_objects (x_errbuf, x_retcode, ld_from_date);
         FND_FILE.PUT_LINE (FND_FILE.LOG, 'Host Objects Completed ');
      END IF;

      COMMIT;
      x_retcode := 0;
   EXCEPTION
      WHEN OTHERS
      THEN
         l_err_msg := SUBSTR (SQLERRM, 1, 240);
         ROLLBACK;
         x_retcode := 2;
         x_errbuf := l_err_msg;
         xxcus_error_pkg.xxcus_error_main_api (
            p_called_from         => g_err_callfrom || '.' || l_procedure,
            p_calling             => l_sec,
            p_request_id          => NULL,
            p_ora_error_msg       => l_err_msg,
            p_error_desc          => l_err_msg,
            p_distribution_list   => g_distro_list,
            p_module              => 'XXWC');
   END;

   -- ==========================================================================================================
   -- Procedure: LOAD_DB_OBJECTS
   -- ==========================================================================================================
   -- VERSION DATE          AUTHOR(S)       DESCRIPTION
   -- ------- -----------   --------------- --------------------------------------------------------------------
   -- 1.0     07-JUL-2017   P.Vamshidhar   TMS#20170707-00224 - Custom Dependency Matrix
   -- ==========================================================================================================
   PROCEDURE LOAD_DB_OBJECTS (x_errbuf         OUT VARCHAR2,
                              x_retcode        OUT VARCHAR2,
                              p_from_date   IN     DATE)
   IS
      l_procedure   VARCHAR2 (100) := 'LOAD_DB_OBJECTS';
      l_sec         VARCHAR2 (100) := '';
      l_err_msg     VARCHAR2 (1000);
   BEGIN
      l_sec := 'DB Objects Insertion';

      INSERT INTO XXWC.XXWC_CUSTOM_DEP_MTRX_TBL (OWNER,
                                                 NAME,
                                                 TYPE,
                                                 REFERENCED_OWNER,
                                                 REFERENCED_NAME,
                                                 REFERENCED_TYPE,
                                                 REFERENCED_LINK_NAME,
                                                 DEPENDENCY_TYPE,
                                                 CREATED_BY,
                                                 CREATION_DATE,
                                                 LAST_UPDATED_BY,
                                                 LAST_UPDATE_DATE,
                                                 OBJ_TYPE)
         (SELECT A.OWNER,
                 A.NAME,
                 A.TYPE,
                 A.REFERENCED_OWNER,
                 A.REFERENCED_NAME,
                 A.REFERENCED_TYPE,
                 A.REFERENCED_LINK_NAME,
                 A.DEPENDENCY_TYPE,
                 'APPS',
                 C.CREATED,
                 'APPS',
                 C.LAST_DDL_TIME,
                 'DATABASE'
            FROM DBA_DEPENDENCIES A, DBA_OBJECTS C
           WHERE     A.NAME = C.OBJECT_NAME
                 AND A.TYPE = C.OBJECT_TYPE
                 AND A.NAME LIKE 'XX%'
                 AND C.LAST_DDL_TIME >= NVL (p_from_date, C.LAST_DDL_TIME));

      l_sec := 'DB Objects Insertion completed ' || SQL%ROWCOUNT;
      x_retcode := 0;
   EXCEPTION
      WHEN OTHERS
      THEN
         l_err_msg := SUBSTR (SQLERRM, 1, 240);
         ROLLBACK;
         x_retcode := 2;
         x_errbuf := l_err_msg;
         xxcus_error_pkg.xxcus_error_main_api (
            p_called_from         => g_err_callfrom || '.' || l_procedure,
            p_calling             => l_sec,
            p_request_id          => NULL,
            p_ora_error_msg       => l_err_msg,
            p_error_desc          => l_err_msg,
            p_distribution_list   => g_distro_list,
            p_module              => 'XXWC');
   END;

   -- ==========================================================================================================
   -- Procedure: LOAD_DB_SOURCE
   -- ==========================================================================================================
   -- VERSION DATE          AUTHOR(S)       DESCRIPTION
   -- ------- -----------   --------------- --------------------------------------------------------------------
   -- 1.0     07-JUL-2017   P.Vamshidhar   TMS#20170707-00224 - Custom Dependency Matrix
   -- ==========================================================================================================
   PROCEDURE LOAD_DB_SOURCE (x_errbuf         OUT VARCHAR2,
                             x_retcode        OUT VARCHAR2,
                             p_from_date   IN     DATE)
   IS
      l_procedure           VARCHAR2 (100) := 'LOAD_DB_SOURCE';
      l_sec                 VARCHAR2 (100) := '';
      l_err_msg             VARCHAR2 (1000);
      lvc_owner             xxwc.xxwc_custom_dep_mtrx_tbl_tmp.owner%TYPE;
      lvc_object_name       xxwc.xxwc_custom_dep_mtrx_tbl_tmp.name%TYPE;
      lvc_object_type       xxwc.xxwc_custom_dep_mtrx_tbl_tmp.TYPE%TYPE;
      lvc_dependency_type   xxwc.xxwc_custom_dep_mtrx_tbl_tmp.dependency_type%TYPE;
      ld_created            xxwc.xxwc_custom_dep_mtrx_tbl_tmp.creation_date%TYPE;
      ld_last_update_date   xxwc.xxwc_custom_dep_mtrx_tbl_tmp.last_update_date%TYPE;
      lvc_obj_type          xxwc.xxwc_custom_dep_mtrx_tbl_tmp.obj_type%TYPE
                               := 'DB_SOURCE';

      CURSOR CUR_DB_SOURCE_MST
      IS
         SELECT DISTINCT NAME
           FROM dba_source
          WHERE name LIKE 'XX%' AND UPPER (TEXT) LIKE '%EXECUTE%IMMEDIATE%';


      CURSOR CUR_DB_SOURCE_TXT (P_SOURCE_NAME VARCHAR2)
      IS
         SELECT UPPER (TEXT) TEXT
           FROM dba_source
          WHERE NAME = P_SOURCE_NAME AND UPPER (TEXT) LIKE '%XX%';

      TYPE tab_mst_db_sources IS TABLE OF CUR_DB_SOURCE_MST%ROWTYPE;

      obj_mst_db_sources    tab_mst_db_sources;
      l_obj_count           NUMBER;
   BEGIN
      l_sec := 'DB Source Objects Insertion';
      g_ln_count := 0;

      OPEN CUR_DB_SOURCE_MST;

      FETCH CUR_DB_SOURCE_MST BULK COLLECT INTO obj_mst_db_sources;

      CLOSE CUR_DB_SOURCE_MST;

      FOR l_obj_count IN obj_mst_db_sources.FIRST .. obj_mst_db_sources.LAST
      LOOP
         BEGIN
            SELECT owner,
                   object_name,
                   object_type,
                   created,
                   last_ddl_time
              INTO lvc_owner,
                   lvc_object_name,
                   lvc_object_type,
                   ld_created,
                   ld_last_update_date
              FROM dba_objects
             WHERE     object_name = obj_mst_db_sources (l_obj_count).name
                   AND object_type IN ('PROCEDURE',
                                       'PACKAGE BODY',
                                       'TRIGGER',
                                       'FUNCTION')
                   AND OWNER = 'APPS';
         EXCEPTION
            WHEN TOO_MANY_ROWS OR NO_DATA_FOUND
            THEN
               BEGIN
                  SELECT owner,
                         object_name,
                         object_type,
                         created,
                         last_ddl_time
                    INTO lvc_owner,
                         lvc_object_name,
                         lvc_object_type,
                         ld_created,
                         ld_last_update_date
                    FROM dba_objects
                   WHERE     object_name =
                                obj_mst_db_sources (l_obj_count).name
                         AND object_type IN ('PROCEDURE',
                                             'PACKAGE BODY',
                                             'TRIGGER',
                                             'FUNCTION')
                         AND OWNER <> 'APPS'
                         AND ROWNUM = 1;
               EXCEPTION
                  WHEN OTHERS
                  THEN
                     FND_FILE.PUT_LINE (
                        FND_FILE.LOG,
                           'Error occured db_sources '
                        || obj_mst_db_sources (l_obj_count).name
                        || '  '
                        || SUBSTR (SQLERRM, 1, 250));
               END;
            WHEN OTHERS
            THEN
               FND_FILE.PUT_LINE (
                  FND_FILE.LOG,
                     'Error occured db_sources '
                  || obj_mst_db_sources (l_obj_count).name
                  || '  '
                  || SUBSTR (SQLERRM, 1, 250));
         END;

         FOR rec_db_source_txt
            IN cur_db_source_txt (obj_mst_db_sources (l_obj_count).name)
         LOOP
            text_process (obj_mst_db_sources (l_obj_count).name,
                          rec_db_source_txt.text);
         END LOOP;

         insert_data (lvc_owner,
                      lvc_object_name,
                      lvc_object_type,
                      ld_created,
                      ld_last_update_date,
                      lvc_obj_type);
      END LOOP;

      -- inserting data into base tables.
      BEGIN
         INSERT INTO XXWC.XXWC_CUSTOM_DEP_MTRX_TBL (OWNER,
                                                    NAME,
                                                    TYPE,
                                                    REFERENCED_OWNER,
                                                    REFERENCED_NAME,
                                                    REFERENCED_TYPE,
                                                    REFERENCED_LINK_NAME,
                                                    DEPENDENCY_TYPE,
                                                    CREATION_DATE,
                                                    LAST_UPDATE_DATE,
                                                    OBJ_TYPE)
            SELECT DISTINCT A.OWNER,
                            A.NAME,
                            A.TYPE,
                            B.OWNER,
                            A.REFERENCED_NAME,
                            B.OBJECT_TYPE,
                            NULL,
                            NULL,
                            A.CREATION_DATE,
                            B.LAST_DDL_TIME,
                            'DB_SOURCE'
              FROM (SELECT DISTINCT owner,
                                    name,
                                    TYPE,
                                    referenced_name,
                                    CREATION_DATE,
                                    LAST_UPDATE_DATE
                      FROM XXWC.XXWC_CUSTOM_DEP_MTRX_TBL_TMP
                     WHERE obj_type = 'DB_SOURCE') A,
                   DBA_OBJECTS B
             WHERE A.REFERENCED_NAME = B.OBJECT_NAME;
      EXCEPTION
         WHEN OTHERS
         THEN
            NULL;
      END;
   END;

   -- ==========================================================================================================
   -- Procedure: load_oracle_forms
   -- ==========================================================================================================
   -- VERSION DATE          AUTHOR(S)       DESCRIPTION
   -- ------- -----------   --------------- --------------------------------------------------------------------
   -- 1.0     07-JUL-2017   P.Vamshidhar   TMS#20170707-00224 - Custom Dependency Matrix
   -- ==========================================================================================================
   PROCEDURE load_oracle_forms (x_errbuf         OUT VARCHAR2,
                                x_retcode        OUT VARCHAR2,
                                p_from_date   IN     DATE)
   IS
      l_procedure           VARCHAR2 (100) := 'LOAD_ORACLE_FORMS';
      l_sec                 VARCHAR2 (100) := '';
      l_err_msg             VARCHAR2 (1000);
      lvc_forms_unix_loc    VARCHAR2 (1000);
      lvc_owner             xxwc.xxwc_custom_dep_mtrx_tbl_tmp.owner%TYPE
                               := 'APPS';
      lvc_object_name       xxwc.xxwc_custom_dep_mtrx_tbl_tmp.name%TYPE;
      lvc_object_type       xxwc.xxwc_custom_dep_mtrx_tbl_tmp.TYPE%TYPE
                               := 'FORM';
      lvc_dependency_type   xxwc.xxwc_custom_dep_mtrx_tbl_tmp.dependency_type%TYPE;
      ld_created            xxwc.xxwc_custom_dep_mtrx_tbl_tmp.creation_date%TYPE;
      ld_last_update_date   xxwc.xxwc_custom_dep_mtrx_tbl_tmp.last_update_date%TYPE;
      lvc_obj_type          xxwc.xxwc_custom_dep_mtrx_tbl_tmp.obj_type%TYPE
                               := 'FORMS';

      CURSOR cur_frm_txt_files
      IS
         SELECT DISTINCT B.FORM_NAME,
                         B.CREATION_DATE,
                         C.MODIFIED_DATE,
                         a.file_name,
                         a.object_name,
                         a.object_type
           FROM APPS.FND_FORM B,
                XXWC.XXWC_DEPN_MTRX_UPD_TBL C,
                XXWC.XXWC_DEPN_MTRX_TXT_FILES_TBL A
          WHERE     C.OBJECT_NAME = B.FORM_NAME
                AND C.OBJECT_TYPE = 'fmb'
                AND A.OBJECT_TYPE = 'fmb'
                AND a.object_name = c.object_name
                AND c.modified_date >= NVL (p_from_date, c.modified_date);

      CURSOR cur_fmb_txt (
         P_FILE_NAME    VARCHAR2)
      IS
         SELECT UPPER (file_text) file_text
           FROM XXWC.XXWC_CUSTOM_FMB_DEP_MTRX_TBL
          WHERE     UPPER (file_text) LIKE '%XX%'
                AND UPPER (file_name) = P_FILE_NAME;


      lvc_ldr_command       VARCHAR2 (32000);
      lvc_file_handle       UTL_FILE.file_type;
      l_utl_max_linesize    NUMBER := 32767;
      lvc_line              VARCHAR2 (32767);
      ln_record_count       NUMBER := 0;
   BEGIN
      EXECUTE IMMEDIATE 'TRUNCATE TABLE XXWC.XXWC_CUSTOM_FMB_DEP_MTRX_TBL';

      g_ln_count := 0;
      l_sec := 'Forms texts Insertion';

      FOR rec_frm_txt_files IN cur_frm_txt_files
      LOOP
         FND_FILE.PUT_LINE (FND_FILE.LOG,
                            'Form file name ' || rec_frm_txt_files.file_name);
         lvc_file_handle :=
            UTL_FILE.fopen (gvc_txt_files_folder,
                            rec_frm_txt_files.file_name,
                            'R');
         ln_record_count := 0;

         LOOP
            BEGIN
               ln_record_count := ln_record_count + 1;
               UTL_FILE.get_line (lvc_file_handle, lvc_line);

               IF lvc_line IS NOT NULL
               THEN
                  INSERT
                    INTO XXWC.XXWC_CUSTOM_FMB_DEP_MTRX_TBL (file_name,
                                                            file_text)
                  VALUES (rec_frm_txt_files.form_name, lvc_line);
               END IF;
            EXCEPTION
               WHEN NO_DATA_FOUND
               THEN
                  FND_FILE.PUT_LINE (
                     FND_FILE.LOG,
                     'Form file name ' || rec_frm_txt_files.file_name);
                  UTL_FILE.fclose (lvc_file_handle);
                  EXIT;
               WHEN OTHERS
               THEN
                  DBMS_OUTPUT.PUT_LINE (
                        'form_name'
                     || rec_frm_txt_files.form_name
                     || ' '
                     || lvc_line);
                  DBMS_OUTPUT.PUT_LINE (
                        'Error occured  load_oracle_forms'
                     || SUBSTR (SQLERRM, 1, 250));
                  UTL_FILE.fclose (lvc_file_handle);
                  EXIT;
            END;
         END LOOP;

         DBMS_OUTPUT.PUT_LINE ('ln_record_count ' || ln_record_count);
      END LOOP;

      FOR rec_frm_txt_files IN cur_frm_txt_files
      LOOP
         lvc_object_name := rec_frm_txt_files.form_name;
         ld_created := rec_frm_txt_files.creation_date;
         ld_last_update_date := rec_frm_txt_files.modified_date;

         FOR REC_FMB_TXT IN CUR_FMB_TXT (rec_frm_txt_files.form_name)
         LOOP
            text_process (rec_frm_txt_files.form_name, rec_fmb_txt.file_text);
         END LOOP;

         insert_data (lvc_owner,
                      lvc_object_name,
                      lvc_object_type,
                      ld_created,
                      ld_last_update_date,
                      lvc_obj_type);
      END LOOP;

      BEGIN
         -- Form objects inserting.
         INSERT INTO XXWC.XXWC_CUSTOM_DEP_MTRX_TBL (OWNER,
                                                    NAME,
                                                    TYPE,
                                                    REFERENCED_OWNER,
                                                    REFERENCED_NAME,
                                                    REFERENCED_TYPE,
                                                    REFERENCED_LINK_NAME,
                                                    DEPENDENCY_TYPE,
                                                    CREATION_DATE,
                                                    LAST_UPDATE_DATE,
                                                    OBJ_TYPE)
            SELECT DISTINCT OWNER,
                            NAME,
                            TYPE,
                            APPLICATION_SHORT_NAME,
                            B.FORM_NAME,
                            'FORM',
                            NULL,
                            NULL,
                            A.CREATION_DATE,
                            A.LAST_UPDATE_DATE,
                            'FORMS'
              FROM XXWC.XXWC_CUSTOM_DEP_MTRX_TBL_TMP A,
                   APPS.FND_FORM B,
                   APPS.FND_APPLICATION C
             WHERE     A.obj_type = 'FORMS'
                   AND A.REFERENCED_NAME = B.FORM_NAME
                   AND B.APPLICATION_ID = C.APPLICATION_ID
                   AND A.NAME <> B.FORM_NAME
                   AND A.LAST_UPDATE_DATE =
                          NVL (P_FROM_DATE, A.LAST_UPDATE_DATE);

         -- db objects inserting.
         INSERT INTO XXWC.XXWC_CUSTOM_DEP_MTRX_TBL (OWNER,
                                                    NAME,
                                                    TYPE,
                                                    REFERENCED_OWNER,
                                                    REFERENCED_NAME,
                                                    REFERENCED_TYPE,
                                                    REFERENCED_LINK_NAME,
                                                    DEPENDENCY_TYPE,
                                                    CREATION_DATE,
                                                    LAST_UPDATE_DATE,
                                                    OBJ_TYPE)
            SELECT DISTINCT A.OWNER,
                            NAME,
                            TYPE,
                            B.OWNER,
                            B.OBJECT_NAME,
                            B.OBJECT_TYPE,
                            NULL,
                            NULL,
                            A.CREATION_DATE,
                            A.LAST_UPDATE_DATE,
                            'FORMS'
              FROM XXWC.XXWC_CUSTOM_DEP_MTRX_TBL_TMP A, DBA_OBJECTS B
             WHERE     A.obj_type = 'FORMS'
                   AND A.REFERENCED_NAME = B.OBJECT_NAME
                   AND A.LAST_UPDATE_DATE =
                          NVL (P_FROM_DATE, A.LAST_UPDATE_DATE);
      EXCEPTION
         WHEN OTHERS
         THEN
            NULL;
      END;
   EXCEPTION
      WHEN OTHERS
      THEN
         DBMS_OUTPUT.put_line ('Error occured ' || SUBSTR (SQLERRM, 1, 250));
   END;

   -- ==========================================================================================================
   -- Procedure: load_oracle_plls
   -- ==========================================================================================================
   -- VERSION DATE          AUTHOR(S)       DESCRIPTION
   -- ------- -----------   --------------- --------------------------------------------------------------------
   -- 1.0     07-JUL-2017   P.Vamshidhar   TMS#20170707-00224 - Custom Dependency Matrix
   -- ==========================================================================================================

   PROCEDURE load_oracle_plls (x_errbuf         OUT VARCHAR2,
                               x_retcode        OUT VARCHAR2,
                               p_from_date   IN     DATE)
   IS
      l_procedure           VARCHAR2 (100) := 'LOAD_ORACLE_PLLS';
      l_sec                 VARCHAR2 (100) := '';
      l_err_msg             VARCHAR2 (1000);
      lvc_forms_unix_loc    VARCHAR2 (1000);
      lvc_owner             xxwc.xxwc_custom_dep_mtrx_tbl_tmp.owner%TYPE
                               := 'APPS';
      lvc_object_name       xxwc.xxwc_custom_dep_mtrx_tbl_tmp.name%TYPE;
      lvc_object_type       xxwc.xxwc_custom_dep_mtrx_tbl_tmp.TYPE%TYPE
                               := 'PLL';
      lvc_dependency_type   xxwc.xxwc_custom_dep_mtrx_tbl_tmp.dependency_type%TYPE;
      ld_created            xxwc.xxwc_custom_dep_mtrx_tbl_tmp.creation_date%TYPE;
      ld_last_update_date   xxwc.xxwc_custom_dep_mtrx_tbl_tmp.last_update_date%TYPE;
      lvc_obj_type          xxwc.xxwc_custom_dep_mtrx_tbl_tmp.obj_type%TYPE
                               := 'PLL';

      CURSOR cur_pll_txt_files
      IS
         SELECT a.OBJECT_NAME FORM_NAME,
                A.MODIFIED_DATE CREATION_DATE,
                A.MODIFIED_DATE,
                b.file_name,
                A.OBJECT_NAME,
                A.OBJECT_TYPE
           FROM XXWC.XXWC_DEPN_MTRX_UPD_TBL A,
                XXWC.XXWC_DEPN_MTRX_TXT_FILES_TBL b
          WHERE     a.object_type = 'pll'
                AND a.object_name = b.object_name
                AND a.object_name IN ('XXWC_OEXOETEL_CUSTOM',
                                      'CUSTOM',
                                      'XXWC_POXPOEPO_CUSTOM',
                                      'XXWC_OEXOELIB',
                                      'XXWC_OEXOELIN');


      CURSOR cur_pll_txt (P_FILE_NAME VARCHAR2)
      IS
         SELECT UPPER (file_text) file_text
           FROM XXWC.XXWC_CUSTOM_PLL_DEP_MTRX_TBL
          WHERE UPPER (file_name) = P_FILE_NAME;


      lvc_ldr_command       VARCHAR2 (32000);
      lvc_file_handle       UTL_FILE.file_type;
      l_utl_max_linesize    NUMBER := 32767;
      lvc_line              VARCHAR2 (32767);
      ln_record_count       NUMBER := 0;
   BEGIN
      EXECUTE IMMEDIATE 'TRUNCATE TABLE XXWC.XXWC_CUSTOM_PLL_DEP_MTRX_TBL';

      g_ln_count := 0;
      l_sec := 'Pll texts Insertion';

      FOR rec_pll_txt_files IN cur_pll_txt_files
      LOOP
         lvc_file_handle :=
            UTL_FILE.fopen (gvc_txt_files_folder,
                            rec_pll_txt_files.file_name,
                            'R');
         ln_record_count := 0;

         LOOP
            BEGIN
               ln_record_count := ln_record_count + 1;
               UTL_FILE.get_line (lvc_file_handle, lvc_line);

               IF lvc_line IS NOT NULL
               THEN
                  INSERT
                    INTO XXWC.XXWC_CUSTOM_PLL_DEP_MTRX_TBL (file_name,
                                                            file_text)
                  VALUES (rec_pll_txt_files.form_name, lvc_line);
               END IF;
            EXCEPTION
               WHEN NO_DATA_FOUND
               THEN
                  UTL_FILE.fclose (lvc_file_handle);
                  EXIT;
               WHEN OTHERS
               THEN
                  DBMS_OUTPUT.PUT_LINE (
                        'pll_name'
                     || rec_pll_txt_files.form_name
                     || ' '
                     || lvc_line);
                  DBMS_OUTPUT.PUT_LINE (
                        'Error occured  load_oracle_plls'
                     || SUBSTR (SQLERRM, 1, 250));
                  UTL_FILE.fclose (lvc_file_handle);
                  EXIT;
            END;
         END LOOP;

         DBMS_OUTPUT.PUT_LINE ('ln_record_count ' || ln_record_count);
      END LOOP;

      FOR rec_pll_txt_files IN cur_pll_txt_files
      LOOP
         lvc_object_name := rec_pll_txt_files.form_name;
         ld_created := rec_pll_txt_files.creation_date;
         ld_last_update_date := rec_pll_txt_files.modified_date;

         FOR REC_PLL_TXT IN CUR_PLL_TXT (rec_pll_txt_files.form_name)
         LOOP
            text_process (rec_pll_txt_files.form_name, rec_pll_txt.file_text);
         END LOOP;

         insert_data (lvc_owner,
                      lvc_object_name,
                      lvc_object_type,
                      ld_created,
                      ld_last_update_date,
                      lvc_obj_type);
      END LOOP;

      BEGIN
         -- db objects inserting.
         INSERT INTO XXWC.XXWC_CUSTOM_DEP_MTRX_TBL (OWNER,
                                                    NAME,
                                                    TYPE,
                                                    REFERENCED_OWNER,
                                                    REFERENCED_NAME,
                                                    REFERENCED_TYPE,
                                                    REFERENCED_LINK_NAME,
                                                    DEPENDENCY_TYPE,
                                                    CREATION_DATE,
                                                    LAST_UPDATE_DATE,
                                                    OBJ_TYPE)
            SELECT DISTINCT A.OWNER,
                            NAME,
                            TYPE,
                            B.OWNER,
                            B.OBJECT_NAME,
                            B.OBJECT_TYPE,
                            NULL,
                            NULL,
                            A.CREATION_DATE,
                            A.LAST_UPDATE_DATE,
                            'PLL'
              FROM XXWC.XXWC_CUSTOM_DEP_MTRX_TBL_TMP A, DBA_OBJECTS B
             WHERE A.obj_type = 'PLL' AND A.REFERENCED_NAME = B.OBJECT_NAME;
      --                   AND A.LAST_UPDATE_DATE =
      --                          NVL (P_FROM_DATE, A.LAST_UPDATE_DATE);
      EXCEPTION
         WHEN OTHERS
         THEN
            NULL;
      END;
   EXCEPTION
      WHEN OTHERS
      THEN
         DBMS_OUTPUT.put_line ('Error occured ' || SUBSTR (SQLERRM, 1, 250));
   END;

   -- ==========================================================================================================
   -- Procedure: load_oracle_reports
   -- ==========================================================================================================
   -- VERSION DATE          AUTHOR(S)       DESCRIPTION
   -- ------- -----------   --------------- --------------------------------------------------------------------
   -- 1.0     07-JUL-2017   P.Vamshidhar   TMS#20170707-00224 - Custom Dependency Matrix
   -- ==========================================================================================================
   PROCEDURE LOAD_ORACLE_REPORTS (x_errbuf         OUT VARCHAR2,
                                  x_retcode        OUT VARCHAR2,
                                  p_from_date   IN     DATE)
   IS
      l_procedure           VARCHAR2 (100) := 'LOAD_ORACLE_REPORTS';
      l_sec                 VARCHAR2 (100) := '';
      l_err_msg             VARCHAR2 (1000);
      lvc_forms_unix_loc    VARCHAR2 (1000);
      lvc_owner             xxwc.xxwc_custom_dep_mtrx_tbl_tmp.owner%TYPE
                               := 'APPS';
      lvc_object_name       xxwc.xxwc_custom_dep_mtrx_tbl_tmp.name%TYPE;
      lvc_object_type       xxwc.xxwc_custom_dep_mtrx_tbl_tmp.TYPE%TYPE
                               := 'REPORT';
      lvc_dependency_type   xxwc.xxwc_custom_dep_mtrx_tbl_tmp.dependency_type%TYPE;
      ld_created            xxwc.xxwc_custom_dep_mtrx_tbl_tmp.creation_date%TYPE;
      ld_last_update_date   xxwc.xxwc_custom_dep_mtrx_tbl_tmp.last_update_date%TYPE;
      lvc_obj_type          xxwc.xxwc_custom_dep_mtrx_tbl_tmp.obj_type%TYPE
                               := 'REPORTS';

      CURSOR cur_rep_txt_files
      IS
         SELECT DISTINCT A.EXECUTION_FILE_NAME REPORT_NAME,
                         C.MODIFIED_DATE,
                         TRIM (d.file_name) file_name,
                         A.CREATION_DATE
           FROM APPS.FND_EXECUTABLES A,
                APPS.FND_APPLICATION B,
                XXWC.XXWC_DEPN_MTRX_UPD_TBL C,
                XXWC.XXWC_DEPN_MTRX_TXT_FILES_TBL d
          WHERE     A.EXECUTION_METHOD_CODE = 'P'
                AND A.APPLICATION_ID = B.APPLICATION_ID
                AND B.APPLICATION_SHORT_NAME IN ('XXWC', 'XXCUS')
                AND A.EXECUTION_FILE_NAME = C.OBJECT_NAME
                AND d.object_name = c.object_name
                AND d.object_type = 'rdf'
                AND c.object_type = 'rdf'
                AND c.modified_date >= NVL (p_from_date, c.modified_date);

      CURSOR cur_rep_txt (
         P_FILE_NAME    VARCHAR2)
      IS
         SELECT UPPER (file_text) file_text
           FROM XXWC.XXWC_CUSTOM_RDF_DEP_MTRX_TBL
          WHERE     UPPER (file_text) LIKE '%XX%'
                AND UPPER (file_name) = P_FILE_NAME;


      lvc_ldr_command       VARCHAR2 (32000);
      lvc_file_handle       UTL_FILE.file_type;
      l_utl_max_linesize    NUMBER := 32767;
      lvc_line              VARCHAR2 (32767);
      ln_record_count       NUMBER := 0;
   BEGIN
      EXECUTE IMMEDIATE 'TRUNCATE TABLE XXWC.XXWC_CUSTOM_RDF_DEP_MTRX_TBL';

      g_ln_count := 0;
      l_sec := 'Reports texts Insertion';

      FOR rec_rep_txt_files IN cur_rep_txt_files
      LOOP
         lvc_file_handle :=
            UTL_FILE.fopen (gvc_txt_files_folder,
                            rec_rep_txt_files.file_name,
                            'R');
         ln_record_count := 0;

         LOOP
            BEGIN
               ln_record_count := ln_record_count + 1;
               UTL_FILE.get_line (lvc_file_handle, lvc_line);

               IF lvc_line IS NOT NULL
               THEN
                  INSERT
                    INTO XXWC.XXWC_CUSTOM_RDF_DEP_MTRX_TBL (file_name,
                                                            file_text)
                  VALUES (rec_rep_txt_files.report_name, lvc_line);
               END IF;
            EXCEPTION
               WHEN NO_DATA_FOUND
               THEN
                  UTL_FILE.fclose (lvc_file_handle);
                  EXIT;
               WHEN OTHERS
               THEN
                  DBMS_OUTPUT.PUT_LINE (
                        'Report_name'
                     || rec_rep_txt_files.file_name
                     || ' '
                     || lvc_line);
                  DBMS_OUTPUT.PUT_LINE (
                        'Error occured  load_oracle_reports'
                     || SUBSTR (SQLERRM, 1, 250));
                  UTL_FILE.fclose (lvc_file_handle);
                  EXIT;
            END;
         END LOOP;

         DBMS_OUTPUT.PUT_LINE ('ln_record_count ' || ln_record_count);
      END LOOP;

      FOR rec_rep_txt_files IN cur_rep_txt_files
      LOOP
         lvc_object_name := rec_rep_txt_files.report_name;
         ld_created := rec_rep_txt_files.creation_date;
         ld_last_update_date := rec_rep_txt_files.modified_date;

         FOR REC_REP_TXT IN CUR_REP_TXT (rec_rep_txt_files.report_name)
         LOOP
            text_process (rec_rep_txt_files.report_name,
                          rec_rep_txt.file_text);
         END LOOP;

         insert_data (lvc_owner,
                      lvc_object_name,
                      lvc_object_type,
                      ld_created,
                      ld_last_update_date,
                      lvc_obj_type);
      END LOOP;

      BEGIN
         -- db objects inserting.
         INSERT INTO XXWC.XXWC_CUSTOM_DEP_MTRX_TBL (OWNER,
                                                    NAME,
                                                    TYPE,
                                                    REFERENCED_OWNER,
                                                    REFERENCED_NAME,
                                                    REFERENCED_TYPE,
                                                    REFERENCED_LINK_NAME,
                                                    DEPENDENCY_TYPE,
                                                    CREATION_DATE,
                                                    LAST_UPDATE_DATE,
                                                    OBJ_TYPE)
            SELECT DISTINCT A.OWNER,
                            NAME,
                            TYPE,
                            B.OWNER,
                            B.OBJECT_NAME,
                            B.OBJECT_TYPE,
                            NULL,
                            NULL,
                            A.CREATION_DATE,
                            A.LAST_UPDATE_DATE,
                            'REPORTS'
              FROM XXWC.XXWC_CUSTOM_DEP_MTRX_TBL_TMP A, DBA_OBJECTS B
             WHERE     A.obj_type = 'REPORTS'
                   AND A.REFERENCED_NAME = B.OBJECT_NAME
                   AND A.LAST_UPDATE_DATE =
                          NVL (P_FROM_DATE, A.LAST_UPDATE_DATE);
      EXCEPTION
         WHEN OTHERS
         THEN
            NULL;
      END;
   EXCEPTION
      WHEN OTHERS
      THEN
         DBMS_OUTPUT.put_line ('Error occured ' || SUBSTR (SQLERRM, 1, 250));
   END;

   -- ==========================================================================================================
   -- Procedure: DELETE_DATA
   -- ==========================================================================================================
   -- VERSION DATE          AUTHOR(S)       DESCRIPTION
   -- ------- -----------   --------------- --------------------------------------------------------------------
   -- 1.0     07-JUL-2017   P.Vamshidhar   TMS#20170707-00224 - Custom Dependency Matrix
   -- ==========================================================================================================
   PROCEDURE DELETE_DATA (x_errbuf         OUT VARCHAR2,
                          x_retcode        OUT NUMBER,
                          p_from_date   IN     DATE,
                          p_source      IN     VARCHAR2)
   IS
      l_procedure   VARCHAR2 (100) := 'DELETE_DATA';
      l_sec         VARCHAR2 (100) := '';
      l_err_msg     VARCHAR2 (1000);
      lvc_source    VARCHAR2 (100);
   BEGIN
      FND_FILE.PUT_LINE (
         FND_FILE.LOG,
            'Deleting data started '
         || TO_CHAR (SYSDATE, 'DD-MON-YYYY HH24:MI:SS'));

      EXECUTE IMMEDIATE 'TRUNCATE TABLE XXWC.XXWC_CUSTOM_DEP_MTRX_TBL_TMP';
	  
      IF p_source = 'ALL'
      THEN
         lvc_source := NULL;

         DELETE FROM XXWC.XXWC_CUSTOM_DEP_MTRX_TBL
               WHERE     DEPENDENCY_TYPE <> 'EXTERNAL'
                     AND TRUNC (LAST_UPDATE_DATE) >=
                            NVL (p_from_date, TRUNC (LAST_UPDATE_DATE));
      ELSIF p_source IN ('Database Objects')
      THEN
         DELETE FROM XXWC.XXWC_CUSTOM_DEP_MTRX_TBL
               WHERE     DEPENDENCY_TYPE <> 'EXTERNAL'
                     AND TRUNC (LAST_UPDATE_DATE) >=
                            NVL (p_from_date, TRUNC (LAST_UPDATE_DATE))
                     AND OBJ_TYPE IN ('DATABASE', 'DB_SOURCE');
      ELSIF p_source = 'Oracle Forms'
      THEN
         DELETE FROM XXWC.XXWC_CUSTOM_DEP_MTRX_TBL
               WHERE     DEPENDENCY_TYPE <> 'EXTERNAL'
                     AND TRUNC (LAST_UPDATE_DATE) >=
                            NVL (p_from_date, TRUNC (LAST_UPDATE_DATE))
                     AND OBJ_TYPE IN ('FORMS');
      ELSIF p_source = 'Oracle Reports'
      THEN
         DELETE FROM XXWC.XXWC_CUSTOM_DEP_MTRX_TBL
               WHERE     DEPENDENCY_TYPE <> 'EXTERNAL'
                     AND TRUNC (LAST_UPDATE_DATE) >=
                            NVL (p_from_date, TRUNC (LAST_UPDATE_DATE))
                     AND OBJ_TYPE IN ('REPORTS');
      ELSIF p_source = 'Workflows'
      THEN
         DELETE FROM XXWC.XXWC_CUSTOM_DEP_MTRX_TBL
               WHERE     DEPENDENCY_TYPE <> 'EXTERNAL'
                     AND TRUNC (LAST_UPDATE_DATE) >=
                            NVL (p_from_date, TRUNC (LAST_UPDATE_DATE))
                     AND OBJ_TYPE IN ('WORKFLOW');
      ELSIF p_source = 'Libraries'
      THEN
         DELETE FROM XXWC.XXWC_CUSTOM_DEP_MTRX_TBL
               WHERE     DEPENDENCY_TYPE <> 'EXTERNAL'
                     AND TRUNC (LAST_UPDATE_DATE) >=
                            NVL (p_from_date, TRUNC (LAST_UPDATE_DATE))
                     AND OBJ_TYPE IN ('PLL');
      ELSIF p_source = 'Oracle Host'
      THEN
         DELETE FROM XXWC.XXWC_CUSTOM_DEP_MTRX_TBL
               WHERE     DEPENDENCY_TYPE <> 'EXTERNAL'
                     AND TRUNC (LAST_UPDATE_DATE) >=
                            NVL (p_from_date, TRUNC (LAST_UPDATE_DATE))
                     AND OBJ_TYPE IN ('HOST');
      ELSE
         NULL;
      END IF;

      FND_FILE.PUT_LINE (FND_FILE.LOG,
                         'Number of rows delete ' || SQL%ROWCOUNT);

      x_retcode := 0;
      x_errbuf := NULL;

      FND_FILE.PUT_LINE (
         FND_FILE.LOG,
            'Deleting data Completed '
         || TO_CHAR (SYSDATE, 'DD-MON-YYYY HH24:MI:SS'));
   EXCEPTION
      WHEN OTHERS
      THEN
         l_err_msg := SUBSTR (SQLERRM, 1, 240);
         ROLLBACK;
         x_retcode := 2;
         x_errbuf := l_err_msg;
         xxcus_error_pkg.xxcus_error_main_api (
            p_called_from         => g_err_callfrom || '.' || l_procedure,
            p_calling             => l_sec,
            p_request_id          => NULL,
            p_ora_error_msg       => l_err_msg,
            p_error_desc          => l_err_msg,
            p_distribution_list   => g_distro_list,
            p_module              => 'XXWC');
   END;

   -- ==========================================================================================================
   -- Procedure: TEXT_PROCESS
   -- ==========================================================================================================
   -- VERSION DATE          AUTHOR(S)       DESCRIPTION
   -- ------- -----------   --------------- --------------------------------------------------------------------
   -- 1.0     07-JUL-2017   P.Vamshidhar   TMS#20170707-00224 - Custom Dependency Matrix
   -- ==========================================================================================================
   PROCEDURE TEXT_PROCESS (p_object_name IN VARCHAR2, p_text IN VARCHAR2)
   IS
      lvc_text       VARCHAR2 (32767) := p_text;
      lvc_new_text   VARCHAR2 (32767);
      ln_line_lgth   NUMBER := 0;

      ln_rec_count   NUMBER;
      ln_obj_pos     NUMBER;
      l_sec          VARCHAR2 (32747);
   BEGIN
      l_sec := 'Procedure started';
      ln_line_lgth := LENGTH (lvc_text);
      lvc_new_text :=
         TRANSLATE (REGEXP_REPLACE (lvc_text, '[[:cntrl:]]'),
                    '<>";/()-+?{}[]\|=*&^%$#@!~`',
                    ' ');

      l_sec := 'New Text ' || ' ' || lvc_new_text;


      lvc_new_text := REPLACE (lvc_new_text, '''', ' ');

      lvc_new_text :=
         REPLACE (
            REPLACE (
               REPLACE (
                  REPLACE (
                     REPLACE (REPLACE (lvc_new_text, 'XXWC.'), 'XXCUS.'),
                     'XXEIS.'),
                  CHR (13)),
               CHR (10)),
            'APPS.');

      l_sec := 'New Text1 ' || ' ' || lvc_new_text;

      lvc_new_text :=
         REPLACE (
            REPLACE (REPLACE (lvc_new_text || '*', ' ', '*'), ',', '*'),
            '.',
            '*');

      l_sec := 'New Text2 ' || ' ' || lvc_new_text;

      /*
            lvc_new_text :=
               REPLACE(REPLACE(REPLACE (
                  REPLACE (
                     REPLACE (
                        REPLACE (REPLACE (lvc_new_text || '*', ' ', '*'), ',', '*'),
                        '.',
                        '*'),
                     ''''),
                  ';'),'('),'(');
        */
      --ln_count := 0;

      WHILE ln_line_lgth > 0
      LOOP
         IF INSTR (lvc_new_text, 'XX', 1) = 0
         THEN
            GOTO END_LOOP;
         END IF;

         lvc_new_text := SUBSTR (lvc_new_text, INSTR (lvc_new_text, 'XX', 1));
         l_sec := 'New Text3 ' || ' ' || lvc_new_text;
         g_ln_count := g_ln_count + 1;
         obj_xxwc_custom_dep_mtrx_tbl (g_ln_count).referenced_name :=
            REPLACE (SUBSTR (lvc_new_text, 1, INSTR (lvc_new_text, '*')),
                     '*');
         l_sec := 'New Text4 ' || ' ' || lvc_new_text;
         lvc_new_text := SUBSTR (lvc_new_text, INSTR (lvc_new_text, '*') + 1);
         l_sec := 'New Text5 ' || ' ' || lvc_new_text;
         ln_line_lgth := LENGTH (lvc_new_text);
      END LOOP;

     <<END_LOOP>>
      NULL;
   EXCEPTION
      WHEN OTHERS
      THEN
         DBMS_OUTPUT.PUT_LINE (
               'Error occured text process'
            || p_object_name
            || ' '
            || SUBSTR (SQLERRM, 1, 250));
   END;

   -- ==========================================================================================================
   -- Procedure: insert_data
   -- ==========================================================================================================
   -- VERSION DATE          AUTHOR(S)       DESCRIPTION
   -- ------- -----------   --------------- --------------------------------------------------------------------
   -- 1.0     07-JUL-2017   P.Vamshidhar   TMS#20170707-00224 - Custom Dependency Matrix
   -- ==========================================================================================================
   PROCEDURE insert_data (p_owner               IN VARCHAR2,
                          p_object_name         IN VARCHAR2,
                          p_object_type         IN VARCHAR2,
                          pd_creation_date      IN DATE,
                          pd_last_update_date   IN DATE,
                          p_obj_type            IN VARCHAR2)
   IS
      lvc_object_name   VARCHAR2 (1000);
   BEGIN
      FORALL l_obj_count
          IN obj_xxwc_custom_dep_mtrx_tbl.FIRST ..
             obj_xxwc_custom_dep_mtrx_tbl.LAST
         INSERT INTO XXWC.XXWC_CUSTOM_DEP_MTRX_TBL_TMP (OWNER,
                                                        NAME,
                                                        TYPE,
                                                        REFERENCED_NAME,
                                                        CREATION_DATE,
                                                        LAST_UPDATE_DATE,
                                                        OBJ_TYPE)
                 VALUES (
                           p_owner,
                           p_object_name,
                           p_object_type,
                           obj_xxwc_custom_dep_mtrx_tbl (l_obj_count).referenced_name,
                           pd_creation_date,
                           pd_last_update_date,
                           p_obj_type);


      COMMIT;
      obj_xxwc_custom_dep_mtrx_tbl.delete;
   EXCEPTION
      WHEN OTHERS
      THEN
         DBMS_OUTPUT.PUT_LINE (
            'Error occured insert_data ' || SUBSTR (SQLERRM, 1, 250));
         obj_xxwc_custom_dep_mtrx_tbl.delete;
         ROLLBACK;
   END;

   -- ==========================================================================================================
   -- Procedure: load_wf_objects
   -- ==========================================================================================================
   -- VERSION DATE          AUTHOR(S)       DESCRIPTION
   -- ------- -----------   --------------- --------------------------------------------------------------------
   -- 1.0     07-JUL-2017   P.Vamshidhar   TMS#20170707-00224 - Custom Dependency Matrix
   -- ==========================================================================================================
   PROCEDURE load_wf_objects (x_errbuf         OUT VARCHAR2,
                              x_retcode        OUT VARCHAR2,
                              p_from_date   IN     DATE)
   IS
   BEGIN
      INSERT INTO XXWC.XXWC_CUSTOM_DEP_MTRX_TBL (OWNER,
                                                 NAME,
                                                 TYPE,
                                                 REFERENCED_OWNER,
                                                 REFERENCED_NAME,
                                                 REFERENCED_TYPE,
                                                 REFERENCED_LINK_NAME,
                                                 DEPENDENCY_TYPE,
                                                 CREATION_DATE,
                                                 LAST_UPDATE_DATE,
                                                 OBJ_TYPE)
           SELECT 'APPS' OWNER,
                  DISPLAY_NAME NAME,
                  'WORKFLOW' TYPE,
                  C.OWNER,
                  SUBSTR (A.FUNCTION, 1, INSTR (A.FUNCTION, '.', 1) - 1)
                     REFERENCED_NAME,
                  C.OBJECT_TYPE,
                  NULL REFERENCED_LINK_NAME,
                  NULL DEPENDENCY_TYPE,
                  MIN (TRUNC (BEGIN_DATE)) CREATION_DATE,
                  MAX (TRUNC (A.END_DATE)) LAST_UPDATE_DATE,
                  'WORKFLOW' OBJ_TYPE
             FROM WF_ACTIVITIES A, APPS.WF_ITEM_TYPES_VL B, DBA_OBJECTS C
            WHERE     A.FUNCTION LIKE 'XX%'
                  AND A.ITEM_TYPE = B.NAME
                  AND A.END_DATE IS NOT NULL
                  AND B.DISPLAY_NAME IN (SELECT DISPLAY_NAME
                                           FROM WF_ACTIVITIES A,
                                                APPS.WF_ITEM_TYPES_VL B
                                          WHERE     A.FUNCTION LIKE 'XX%'
                                                AND A.ITEM_TYPE = B.NAME
                                                AND A.END_DATE IS NOT NULL)
                  AND TRIM (
                         SUBSTR (UPPER (A.FUNCTION),
                                 1,
                                 INSTR (A.FUNCTION, '.', 1) - 1)) =
                         C.OBJECT_NAME
                  AND C.OBJECT_TYPE = 'PACKAGE BODY'
         GROUP BY SUBSTR (A.FUNCTION, 1, INSTR (A.FUNCTION, '.', 1) - 1),
                  DISPLAY_NAME,
                  C.OWNER,
                  C.OBJECT_TYPE
         UNION ALL
           SELECT DISTINCT
                  'APPS' OWNER,
                  DISPLAY_NAME NAME,
                  'WORKFLOW' TYPE,
                  C.OWNER,
                  SUBSTR (A.FUNCTION, 1, INSTR (A.FUNCTION, '.', 1) - 1)
                     REFERENCED_NAME,
                  C.OBJECT_TYPE,
                  NULL REFERENCED_LINK_NAME,
                  NULL DEPENDENCY_TYPE,
                  TRUNC (A.BEGIN_DATE) BEGIN_DATE,
                  NULL LAST_MODIFIED_DATE,
                  'WORKFLOW' OBJ_TYPE
             FROM WF_ACTIVITIES A, APPS.WF_ITEM_TYPES_VL B, DBA_OBJECTS C
            WHERE     A.FUNCTION LIKE 'XX%'
                  AND A.ITEM_TYPE = B.NAME
                  AND A.END_DATE IS NULL
                  AND B.DISPLAY_NAME NOT IN (SELECT DISPLAY_NAME
                                               FROM WF_ACTIVITIES A,
                                                    APPS.WF_ITEM_TYPES_VL B
                                              WHERE     A.FUNCTION LIKE 'XX%'
                                                    AND A.ITEM_TYPE = B.NAME
                                                    AND A.END_DATE IS NOT NULL)
                  AND TRIM (
                         SUBSTR (UPPER (A.FUNCTION),
                                 1,
                                 INSTR (A.FUNCTION, '.', 1) - 1)) =
                         C.OBJECT_NAME
                  AND C.OBJECT_TYPE = 'PACKAGE BODY'
         GROUP BY SUBSTR (A.FUNCTION, 1, INSTR (A.FUNCTION, '.', 1) - 1),
                  DISPLAY_NAME,
                  TRUNC (A.BEGIN_DATE),
                  C.OWNER,
                  C.OBJECT_TYPE;
   EXCEPTION
      WHEN OTHERS
      THEN
         NULL;
   END;

   -- ==========================================================================================================
   -- Procedure: data_update
   -- ==========================================================================================================
   -- VERSION DATE          AUTHOR(S)       DESCRIPTION
   -- ------- -----------   --------------- --------------------------------------------------------------------
   -- 1.0     07-JUL-2017   P.Vamshidhar   TMS#20170707-00224 - Custom Dependency Matrix
   -- ==========================================================================================================
   PROCEDURE data_update
   IS
   BEGIN
      UPDATE XXWC.XXWC_DEPN_MTRX_UPD_TBL
         SET FILE_TIME_YEAR = TO_CHAR (SYSDATE, 'YYYY')
       WHERE file_time_year LIKE '%:%';

      fnd_file.put_line (
         fnd_file.LOG,
         'XXWC_DEPN_MTRX_UPD_TBL Stage1 Updaed ' || SQL%ROWCOUNT);

      UPDATE XXWC.XXWC_DEPN_MTRX_UPD_TBL
         SET MODIFIED_DATE =
                TO_DATE (
                      TRIM (REPLACE (file_day, ' ', ''))
                   || '-'
                   || TRIM (REPLACE (file_month, ' ', ''))
                   || '-'
                   || TRIM (REPLACE (FILE_TIME_YEAR, ' ', '')),
                   'DD-Mon-YYYY'),
             OBJECT_NAME =
                DECODE (
                   SUBSTR (FILE_NAME, -3),
                   'pll', SUBSTR (file_name,
                                  INSTR (file_name, '/resource/', 1) + 10),
                   'rog', SUBSTR (file_name,
                                  INSTR (file_name, '/bin/', 1) + 5),
                   SUBSTR (file_name, INSTR (file_name, '/US/', 1) + 4)),
             OBJECT_TYPE = REPLACE (SUBSTR (FILE_NAME, -4), '.', '');

      fnd_file.put_line (
         fnd_file.LOG,
         'XXWC_DEPN_MTRX_UPD_TBL Stage2 Updaed ' || SQL%ROWCOUNT);

      UPDATE XXWC.XXWC_DEPN_MTRX_UPD_TBL
         SET OBJECT_NAME =
                SUBSTR (OBJECT_NAME, 1, INSTR (OBJECT_NAME, '.', 1) - 1);

      fnd_file.put_line (
         fnd_file.LOG,
         'XXWC_DEPN_MTRX_UPD_TBL Stage3 Updaed ' || SQL%ROWCOUNT);

      COMMIT;

      EXECUTE IMMEDIATE 'TRUNCATE TABLE XXWC.XXWC_DEPN_MTRX_TXT_FILES_TBL';

      INSERT INTO XXWC.XXWC_DEPN_MTRX_TXT_FILES_TBL (FILE_NAME)
         (SELECT file_name
            FROM XXWC.XXWC_CUST_DEPN_MTRX_EXT_TBL
           WHERE (   FILE_NAME LIKE '%rdf.rex'
                  OR FILE_NAME LIKE '%fmb.txt%'
                  OR FILE_NAME LIKE '%pll.pld%'
                  OR FILE_NAME LIKE '%prog.txt'));

      fnd_file.put_line (
         fnd_file.LOG,
         'XXWC.XXWC_DEPN_MTRX_TXT_FILES_TBL rows inserted ' || SQL%ROWCOUNT);

      UPDATE XXWC.XXWC_DEPN_MTRX_TXT_FILES_TBL
         SET OBJECT_NAME =
                SUBSTR (FILE_NAME, 1, INSTR (FILE_NAME, '.', 1) - 1),
             OBJECT_TYPE =
                SUBSTR (FILE_NAME, INSTR (FILE_NAME, '.', 1) + 1, 3);

      fnd_file.put_line (
         fnd_file.LOG,
         'XXWC.XXWC_DEPN_MTRX_TXT_FILES_TBL rows Updated ' || SQL%ROWCOUNT);

      COMMIT;
   EXCEPTION
      WHEN OTHERS
      THEN
         fnd_file.put_line (fnd_file.LOG,
                            'Error occured ' || SUBSTR (SQLERRM, 1, 250));
   END;

   PROCEDURE load_host_objects (x_errbuf         OUT VARCHAR2,
                                x_retcode        OUT VARCHAR2,
                                p_from_date   IN     DATE)
   IS
      l_procedure           VARCHAR2 (100) := 'LOAD_HOST_OBJECTS';
      l_sec                 VARCHAR2 (100) := '';
      l_err_msg             VARCHAR2 (1000);
      lvc_forms_unix_loc    VARCHAR2 (1000);
      lvc_owner             xxwc.xxwc_custom_dep_mtrx_tbl_tmp.owner%TYPE
                               := 'APPS';
      lvc_object_name       xxwc.xxwc_custom_dep_mtrx_tbl_tmp.name%TYPE;
      lvc_object_type       xxwc.xxwc_custom_dep_mtrx_tbl_tmp.TYPE%TYPE
                               := 'HOST';
      lvc_dependency_type   xxwc.xxwc_custom_dep_mtrx_tbl_tmp.dependency_type%TYPE;
      ld_created            xxwc.xxwc_custom_dep_mtrx_tbl_tmp.creation_date%TYPE;
      ld_last_update_date   xxwc.xxwc_custom_dep_mtrx_tbl_tmp.last_update_date%TYPE;
      lvc_obj_type          xxwc.xxwc_custom_dep_mtrx_tbl_tmp.obj_type%TYPE
                               := 'HOST';

      CURSOR cur_host_txt_files
      IS
         SELECT a.OBJECT_NAME FORM_NAME,
                A.MODIFIED_DATE CREATION_DATE,
                A.MODIFIED_DATE,
                b.file_name,
                A.OBJECT_NAME,
                A.OBJECT_TYPE
           FROM XXWC.XXWC_DEPN_MTRX_UPD_TBL A,
                XXWC.XXWC_DEPN_MTRX_TXT_FILES_TBL b
          WHERE     a.object_type = 'prog'
                AND a.object_name = b.object_name
                AND b.object_name NOT IN ('XXCUS_OZF_EMAIL_SLAEXCEP_STATUS',
                                          'XXWC_EBS_EDW_LINUX_DOS_10JUL2012');


      CURSOR cur_host_txt (P_FILE_NAME VARCHAR2)
      IS
         SELECT UPPER (file_text) file_text
           FROM XXWC.XXWC_CUSTOM_HOST_DEP_MTRX_TBL
          WHERE UPPER (file_name) = P_FILE_NAME;


      lvc_ldr_command       VARCHAR2 (32000);
      lvc_file_handle       UTL_FILE.file_type;
      l_utl_max_linesize    NUMBER := 32767;
      lvc_line              VARCHAR2 (32767);
      ln_record_count       NUMBER := 0;
   BEGIN
      EXECUTE IMMEDIATE 'TRUNCATE TABLE XXWC.XXWC_CUSTOM_HOST_DEP_MTRX_TBL';

      g_ln_count := 0;
      l_sec := 'Pll texts Insertion';

      FOR rec_host_txt_files IN cur_host_txt_files
      LOOP
         lvc_file_handle :=
            UTL_FILE.fopen (gvc_txt_files_folder,
                            rec_host_txt_files.file_name,
                            'R');
         ln_record_count := 0;

         LOOP
            BEGIN
               ln_record_count := ln_record_count + 1;
               UTL_FILE.get_line (lvc_file_handle, lvc_line);

               IF lvc_line IS NOT NULL
               THEN
                  INSERT
                    INTO XXWC.XXWC_CUSTOM_HOST_DEP_MTRX_TBL (file_name,
                                                             file_text)
                  VALUES (rec_host_txt_files.form_name, lvc_line);
               END IF;
            EXCEPTION
               WHEN NO_DATA_FOUND
               THEN
                  UTL_FILE.fclose (lvc_file_handle);
                  EXIT;
               WHEN OTHERS
               THEN
                  FND_FILE.PUT_LINE (
                     FND_FILE.LOG,
                        'host_name'
                     || rec_host_txt_files.form_name
                     || ' '
                     || lvc_line);
                  FND_FILE.PUT_LINE (
                     FND_FILE.LOG,
                        'Error occured  load_oracle_plls'
                     || SUBSTR (SQLERRM, 1, 250));
                  UTL_FILE.fclose (lvc_file_handle);
                  EXIT;
            END;
         END LOOP;
      END LOOP;

      FOR rec_host_txt_files IN cur_host_txt_files
      LOOP
         lvc_object_name := rec_host_txt_files.form_name;
         ld_created := rec_host_txt_files.creation_date;
         ld_last_update_date := rec_host_txt_files.modified_date;
 
         FOR REC_HOST_TXT IN CUR_HOST_TXT (rec_host_txt_files.form_name)
         LOOP
            BEGIN
               text_process (rec_host_txt_files.form_name,
                             rec_host_txt.file_text);
            EXCEPTION
               WHEN OTHERS
               THEN
                  FND_fILE.PUT_LINE (
                     FND_FILE.LOG,
                     'Host file ' || rec_host_txt_files.form_name);
                  FND_fILE.PUT_LINE (FND_FILE.LOG,
                                     'Error ' || SUBSTR (SQLERRM, 1, 250));
                  FND_fILE.PUT_LINE (
                     FND_FILE.LOG,
                     'rec_host_txt.file_text ' || rec_host_txt.file_text);
                  CONTINUE;
            END;
         END LOOP;

         insert_data (lvc_owner,
                      lvc_object_name,
                      lvc_object_type,
                      ld_created,
                      ld_last_update_date,
                      lvc_obj_type);
      END LOOP;

      BEGIN
         -- db objects inserting.
         INSERT INTO XXWC.XXWC_CUSTOM_DEP_MTRX_TBL (OWNER,
                                                    NAME,
                                                    TYPE,
                                                    REFERENCED_OWNER,
                                                    REFERENCED_NAME,
                                                    REFERENCED_TYPE,
                                                    REFERENCED_LINK_NAME,
                                                    DEPENDENCY_TYPE,
                                                    CREATION_DATE,
                                                    LAST_UPDATE_DATE,
                                                    OBJ_TYPE)
            SELECT DISTINCT A.OWNER,
                            NAME,
                            TYPE,
                            B.OWNER,
                            B.OBJECT_NAME,
                            B.OBJECT_TYPE,
                            NULL,
                            NULL,
                            A.CREATION_DATE,
                            A.LAST_UPDATE_DATE,
                            'HOST'
              FROM XXWC.XXWC_CUSTOM_DEP_MTRX_TBL_TMP A, DBA_OBJECTS B
             WHERE A.obj_type = 'HOST' AND A.REFERENCED_NAME = B.OBJECT_NAME;
      EXCEPTION
         WHEN OTHERS
         THEN
            NULL;
      END;
   EXCEPTION
      WHEN OTHERS
      THEN
         FND_FILE.PUT_LINE (FND_FILE.LOG,
                            'Error occured ' || SUBSTR (SQLERRM, 1, 250));
   END;
END XXWC_CUSTOM_DEPEN_MTRX_PKG;
/