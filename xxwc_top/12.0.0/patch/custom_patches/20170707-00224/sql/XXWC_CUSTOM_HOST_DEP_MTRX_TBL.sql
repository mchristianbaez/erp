/*************************************************************************
  $Header XXWC_CUSTOM_HOST_DEP_MTRX_TBL.sql $
  Module Name: XXWC_CUSTOM_HOST_DEP_MTRX_TBL

  PURPOSE: Table to maintain host files modified history.

  REVISIONS:
  Ver        Date         Author                Description
  ---------  -----------  ------------------    ---------------------------------------------------------------------------------------
  1.1        15-Jun-2017  P.vamshidhar          TMS#20170707-00224 - Custom Dependency Matrix
*******************************************************************************************************************************************/ 
CREATE TABLE XXWC.XXWC_CUSTOM_HOST_DEP_MTRX_TBL
(
  FILE_NAME  VARCHAR2(100 BYTE),
  FILE_TEXT  VARCHAR2(4000 BYTE)
);
/