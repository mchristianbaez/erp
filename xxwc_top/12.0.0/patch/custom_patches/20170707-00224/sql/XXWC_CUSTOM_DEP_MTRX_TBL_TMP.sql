/*************************************************************************
  $Header XXWC_CUSTOM_DEP_MTRX_TBL_TMP.sql $
  Module Name: XXWC_CUSTOM_DEP_MTRX_TBL_TMP

  PURPOSE: Table to maintain forms and reports modified history.

  REVISIONS:
  Ver        Date         Author                Description
  ---------  -----------  ------------------    ---------------------------------------------------------------------------------------
  1.1        15-Jun-2017  P.vamshidhar          TMS#20170707-00224 - Custom Dependency Matrix
*******************************************************************************************************************************************/ 
CREATE TABLE XXWC.XXWC_CUSTOM_DEP_MTRX_TBL_TMP
(
  OWNER                 VARCHAR2(30 BYTE)       NOT NULL,
  NAME                  VARCHAR2(30 BYTE)       NOT NULL,
  TYPE                  VARCHAR2(100 BYTE),
  REFERENCED_OWNER      VARCHAR2(30 BYTE),
  REFERENCED_NAME       VARCHAR2(64 BYTE),
  REFERENCED_TYPE       VARCHAR2(18 BYTE),
  REFERENCED_LINK_NAME  VARCHAR2(128 BYTE),
  DEPENDENCY_TYPE       VARCHAR2(4 BYTE),
  CREATED_BY            VARCHAR2(40 BYTE),
  LAST_UPDATED_BY       VARCHAR2(40 BYTE),
  LAST_UPDATE_DATE      DATE,
  OBJ_TYPE              VARCHAR2(100 BYTE),
  CREATION_DATE         DATE
);