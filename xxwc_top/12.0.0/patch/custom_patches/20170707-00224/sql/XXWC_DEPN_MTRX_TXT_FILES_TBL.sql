/*************************************************************************
  $Header XXWC_DEPN_MTRX_TXT_FILES_TBL.sql $
  Module Name: XXWC_DEPN_MTRX_TXT_FILES_TBL

  PURPOSE: Table to maintain forms and reports modified history.

  REVISIONS:
  Ver        Date         Author                Description
  ---------  -----------  ------------------    ---------------------------------------------------------------------------------------
  1.1        15-Jun-2017  P.vamshidhar          TMS#20170707-00224 - Custom Dependency Matrix
*******************************************************************************************************************************************/ 
CREATE TABLE XXWC.XXWC_DEPN_MTRX_TXT_FILES_TBL
(
  FILE_NAME    VARCHAR2(612 BYTE),
  OBJECT_NAME  VARCHAR2(100 BYTE),
  OBJECT_TYPE  VARCHAR2(100 BYTE)
);
/