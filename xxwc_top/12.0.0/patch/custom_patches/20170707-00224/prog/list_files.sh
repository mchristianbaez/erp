#!/bin/sh
cd /xx_iface/ebsprd/inbound/xx_depn_mtrx/nondb
/bin/ls -all *
# This file is called by the XXWC_DEPN_MTRX_IB_FILES_TBL external table to
# show the files in the directory above.

# -- Create table
# create table XXWC.XXWC_DEPN_MTRX_IB_FILES_TBL
# (
  # file_permissions  VARCHAR2(50),
  # file_type         VARCHAR2(64),
  # file_owner        VARCHAR2(64),
  # file_group        VARCHAR2(64),
  # file_size         VARCHAR2(64),
  # file_month        VARCHAR2(64),
  # file_day          VARCHAR2(64),
  # file_time  	      VARCHAR2(64),
  # file_name         VARCHAR2(612)
# )
# organization external
# (
  # type ORACLE_LOADER
  # default directory XXWC_DEPN_MTRX_IB_DIR
  # access parameters 
  # (
    # RECORDS DELIMITED BY NEWLINE
    # LOAD WHEN file_permissions != 'total'
    # PREPROCESSOR XXWC_DEPN_MTRX_IB_DIR: 'list_files.sh'
	# NOBADFILE
    # NODISCARDFILE
    # NOLOGFILE
    # FIELDS TERMINATED BY WHITESPACE
  # )
  # location (XXWC_DEPN_MTRX_IB_DIR:'list_files_dummy_source.txt')
# )
# reject limit UNLIMITED;
