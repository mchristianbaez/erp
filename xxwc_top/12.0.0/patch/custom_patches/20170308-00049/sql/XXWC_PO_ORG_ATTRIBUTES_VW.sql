/******************************************************************************************************
-- File Name: XXWC_PO_ORG_ATTRIBUTES_VW.sql
--
-- PROGRAM TYPE: View
--
-- HISTORY
-- ==========================================================================================================
-- ==========================================================================================================
-- VERSION DATE          AUTHOR(S)       DESCRIPTION
-- ------- -----------   --------------- --------------------------------------------------------------------
-- 1.0     09-Mar-2017   P.Vamshidhar    TMS#20170308-00049 Org Attributes OAF form 
--                                       Initial version
************************************************************************************************************/
/* Formatted on 3/9/2017 3:11:28 PM (QP5 v5.256.13226.35538) */
CREATE OR REPLACE VIEW APPS.XXWC_PO_ORG_ATTRIBUTES_VW
AS
   (SELECT MSIB.INVENTORY_ITEM_ID,
           MSIB.SEGMENT1 SEGMENT1,
           MSIB.ORGANIZATION_ID,
           MSIB.DESCRIPTION,
           MSIB.PRIMARY_UOM_CODE WEIGHT_UOM_CODE,
           MSIB.SOURCE_TYPE source_type,
           (SELECT ORGANIZATION_NAME
              FROM ORG_ORGANIZATION_DEFINITIONS ORG
             WHERE organization_id = msib.ORGANIZATION_ID)
              org_name,
           SOURCE_ORGANIZATION_ID,
           (SELECT ORGANIZATION_NAME
              FROM ORG_ORGANIZATION_DEFINITIONS ORG
             WHERE organization_id = msib.SOURCE_ORGANIZATION_ID)
              SOURCE_ORG_NAME,
           SOURCE_RULE.SOURCING_RULE_NAME,
           SOURCE_RULE.VENDOR_NAME,
           SOURCE_RULE.VENDOR_SITE,
           SOURCE_ORG_RULE.SOURCING_RULE_NAME SOURCE_ORG_RULE_NAME,
           SOURCE_ORG_RULE.VENDOR_NAME SOURCE_ORG_VENDOR_NAME,
           SOURCE_ORG_RULE.VENDOR_SITE source_org_site_name,
           PREPROCESSING_LEAD_TIME,
           POSTPROCESSING_LEAD_TIME,
           FIXED_LOT_MULTIPLIER,
           MSIB.ATTRIBUTE15 SALES_VELOCITY,
           INVENTORY_PLANNING_CODE,
           MIN_MINMAX_QUANTITY,
           MAX_MINMAX_QUANTITY,
           PURCHASING_ENABLED_FLAG,
           PURCHASING_ITEM_FLAG,
           attribute21 Reserve_Stock,
           BUYER_ID,
           (SELECT FULL_NAME
              FROM HR_EMPLOYEES
             WHERE EMPLOYEE_ID = MSIB.BUYER_ID)
              FULL_NAME,
           PREPROCESSING_LEAD_TIME PREV_PREPLT,
           POSTPROCESSING_LEAD_TIME PREV_POSTPLT,
           FIXED_LOT_MULTIPLIER PREV_FLM,
           MSIB.ATTRIBUTE15 PREV_SALES_VELOCITY,
           INVENTORY_PLANNING_CODE PREV_INVENTORY_PLANNING_CODE,
           MIN_MINMAX_QUANTITY PREV_MIN,
           MAX_MINMAX_QUANTITY PREV_MAX,
           PURCHASING_ENABLED_FLAG PREV_PURCHASE_ENABLED_FLAG,
           PURCHASING_ITEM_FLAG PREV_PURCHASE_ITEM_FLAG,
           attribute21 prev_Reserve_Stock,
           (SELECT FULL_NAME
              FROM HR_EMPLOYEES
             WHERE EMPLOYEE_ID = MSIB.BUYER_ID)
              prev_buyer,
           (SELECT segment1
              FROM MTL_ITEM_CATEGORIES_V
             WHERE     INVENTORY_ITEM_ID = msib.INVENTORY_ITEM_ID
                   AND CATEGORY_SET_NAME = 'Sales Velocity'
                   AND organization_id = msib.SOURCE_ORGANIZATION_ID)
              source_org_classification,
           SOURCE_RULE.SOURCING_RULE_ID,
           (SELECT segment1
              FROM MTL_ITEM_CATEGORIES_V
             WHERE     INVENTORY_ITEM_ID = msib.inventory_item_id
                   AND CATEGORY_SET_NAME = 'Purchase Flag'
                   AND organization_id = msib.organization_id)
              purchase_flag,
           (SELECT micv.segment1
              FROM MTL_ITEM_CATEGORIES_V MICV
             WHERE     MICV.INVENTORY_ITEM_ID = msib.INVENTORY_ITEM_ID
                   AND MICV.CATEGORY_SET_NAME = 'Purchase Flag'
                   AND MICV.ORGANIZATION_ID = msib.ORGANIZATION_ID)
              prev_purchase_flag,
           (SELECT meaning
              FROM APPS.FND_LOOKUP_VALUES
             WHERE     LOOKUP_TYPE = 'MTL_SOURCE_TYPES'
                   AND LANGUAGE = USERENV ('LANG')
                   AND TO_NUMBER (lookup_code) = msib.source_type)
              prev_source_type,
           (SELECT segment1
              FROM MTL_ITEM_CATEGORIES_V
             WHERE     INVENTORY_ITEM_ID = msib.INVENTORY_ITEM_ID
                   AND CATEGORY_SET_NAME = 'Sales Velocity'
                   AND organization_id = msib.ORGANIZATION_ID)
              classification,
           (SELECT segment1
              FROM MTL_ITEM_CATEGORIES_V
             WHERE     INVENTORY_ITEM_ID = msib.INVENTORY_ITEM_ID
                   AND CATEGORY_SET_NAME = 'Sales Velocity'
                   AND organization_id = msib.ORGANIZATION_ID)
              prev_classification,
           (SELECT meaning
              FROM APPS.FND_LOOKUP_VALUES
             WHERE     LOOKUP_TYPE = 'MTL_MATERIAL_PLANNING'
                   AND LANGUAGE = USERENV ('LANG')
                   AND TO_NUMBER (LOOKUP_CODE) = MSIB.INVENTORY_PLANNING_CODE)
              PREV_PLANNING_CODE,
           SOURCE_RULE.SOURCING_RULE_ID PREV_SOURCING_RULE_ID,
           SOURCE_RULE.SOURCING_RULE_NAME prev_Sourcing_rule_name,
           (SELECT ORGANIZATION_code
              FROM ORG_ORGANIZATION_DEFINITIONS ORG
             WHERE ORGANIZATION_ID = MSIB.ORGANIZATION_ID)
              ORG_CODE,
           (SELECT ORGANIZATION_code
              FROM ORG_ORGANIZATION_DEFINITIONS ORG
             WHERE ORGANIZATION_ID = MSIB.SOURCE_ORGANIZATION_ID)
              SOURCE_ORG_CODE,
           MIN_MINMAX_QUANTITY PREVIOUS_MIN,
           MAX_MINMAX_QUANTITY PREVIOUS_MAX,
           (SELECT ORGANIZATION_code
              FROM ORG_ORGANIZATION_DEFINITIONS ORG
             WHERE ORGANIZATION_ID = MSIB.SOURCE_ORGANIZATION_ID)
              PREV_SOURCE_ORG_CODE,
           FULL_LEAD_TIME,
           FULL_LEAD_TIME PREV_FLT
      FROM MTL_SYSTEM_ITEMS_B MSIB,
           (SELECT MSA.ORGANIZATION_ID,
                   MSA.INVENTORY_ITEM_ID,
                   MSA.SOURCING_RULE_ID,
                   MSR.SOURCING_RULE_NAME,
                   MSOV.VENDOR_ID,
                   MSOV.VENDOR_NAME,
                   MSOV.VENDOR_SITE_ID,
                   MSOV.VENDOR_SITE
              FROM MRP_SR_ASSIGNMENTS MSA,
                   MRP_SOURCING_RULES MSR,
                   MRP_SR_RECEIPT_ORG MSRO,
                   MRP_SR_SOURCE_ORG_V MSOV
             WHERE     MSA.SOURCING_RULE_ID = MSR.SOURCING_RULE_ID
                   AND MSA.SOURCING_RULE_ID = MSRO.SOURCING_RULE_ID
                   AND MSRO.SR_RECEIPT_ID = MSOV.SR_RECEIPT_ID) SOURCE_RULE,
           (SELECT MSA.ORGANIZATION_ID,
                   MSA.INVENTORY_ITEM_ID,
                   MSA.SOURCING_RULE_ID,
                   MSR.SOURCING_RULE_NAME,
                   MSOV.VENDOR_ID,
                   MSOV.VENDOR_NAME,
                   MSOV.VENDOR_SITE_ID,
                   MSOV.VENDOR_SITE
              FROM MRP_SR_ASSIGNMENTS MSA,
                   MRP_SOURCING_RULES MSR,
                   MRP_SR_RECEIPT_ORG MSRO,
                   MRP_SR_SOURCE_ORG_V MSOV
             WHERE     MSA.SOURCING_RULE_ID = MSR.SOURCING_RULE_ID
                   AND MSA.SOURCING_RULE_ID = MSRO.SOURCING_RULE_ID
                   AND MSRO.SR_RECEIPT_ID = MSOV.SR_RECEIPT_ID)
           SOURCE_ORG_RULE
     WHERE     MSIB.INVENTORY_ITEM_ID = SOURCE_RULE.INVENTORY_ITEM_ID(+)
           AND MSIB.ORGANIZATION_ID = SOURCE_RULE.ORGANIZATION_ID(+)
           AND MSIB.SOURCE_ORGANIZATION_ID =
                  SOURCE_ORG_RULE.ORGANIZATION_ID(+)
           AND MSIB.INVENTORY_ITEM_ID = SOURCE_ORG_RULE.INVENTORY_ITEM_ID(+))
/		   