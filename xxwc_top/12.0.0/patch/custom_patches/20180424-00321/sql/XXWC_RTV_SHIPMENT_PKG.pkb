CREATE OR REPLACE PACKAGE BODY APPS.XXWC_RTV_SHIPMENT_PKG
AS
   /********************************************************************************************************************************
      $Header XXWC_RTV_SHIPMENT_PKG.pkb $

      PURPOSE:   This package created to process RTV duplicate items allocations.

      REVISIONS:
      Ver        Date        Author                     Description
      ---------  ----------  ---------------         ----------------------------------------------------------------------------------------
      1.0        14-Apr-2018 P.Vamshidhar            Initial Version -
                                                      TMS# 20170817-00112 - Receipt Traveler Enhancement Request
	  2.0        25-Apr-2018 Naveen Kalidindi        TMS# 20180424-00321 - Fix Traveller Report to display entries when Duplicates exist and there are no open
														Orders etc. Tag: <2.0>

   ******************************************************************************************************************************************/

   PROCEDURE SHIPMENTS_DATA_POPULATE (P_CONC_REQUEST_ID   IN     NUMBER,
                                      P_SHIP_HDR_ID       IN     NUMBER,
                                      P_RET_STATUS           OUT VARCHAR2)
   IS
      /*******************************************************************************
        * Procedure:   XXWC_RTV_SHIPMENT_POPULATE_PRC
        =============================================================================================================
        VERSION DATE               AUTHOR(S)       DESCRIPTION
        ------- -----------------  --------------- ------------------------------------------------------------------
        1.0     20-Feb-2018        P.Vamshidhar    TMS#20170817-00112 - Receipt Traveler Enhancement Request
                                                   Initial creation of the procedure
        *************************************************************************************************************/
      CURSOR CUR_ITEM
      IS
           SELECT ITEM_ID, COUNT (DISTINCT shipment_line_id)
             FROM XXWC.XXWC_INV_RTV_REPORT_DATA_TBL
            WHERE CONC_REQUEST_ID = P_CONC_REQUEST_ID
         GROUP BY ITEM_ID
           HAVING COUNT (1) > 1;

      CURSOR CUR_DATA (
         P_ITEM_ID   IN NUMBER)
      IS
           SELECT *
             FROM XXWC.XXWC_INV_RTV_REPORT_DATA_TBL
            WHERE     1 = 1
                  AND CONC_REQUEST_ID = P_CONC_REQUEST_ID
                  AND ITEM_ID = P_ITEM_ID
				  AND ordered_quantity > 0 -- Version 2.0, include only those that have Ordered Qty.
         ORDER BY SHIPMENT_LINE_ID, REQ_DATE, ordered_quantity;

      CURSOR CUR_DATA1
      IS
           SELECT *
             FROM XXWC.XXWC_INV_RTV_REPORT_DATA_TBL XRT
            WHERE     1 = 1
                  AND CONC_REQUEST_ID = P_CONC_REQUEST_ID
                  AND NOT EXISTS
                         (SELECT 1
                            FROM XXWC.XXWC_INV_RTV_REPORT_DATA_B
                           WHERE     CONC_REQUEST_ID = P_CONC_REQUEST_ID
                                 AND ITEM_ID = XRT.ITEM_ID)
         ORDER BY SHIPMENT_LINE_ID, REQ_DATE;


      ln_alloc_qty        NUMBER := 0;
      lvc_sec             VARCHAR2 (32767);
      lvc_dflt_email      VARCHAR2 (75)
                             DEFAULT 'HDSORACLEDEVELOPERS@hdsupply.com';
      ln_item_adj         NUMBER;
      ln_tobe_alloc_qty   NUMBER;
      ln_count            NUMBER;
   BEGIN
      lvc_sec := 'Procedure start';

      INSERT INTO XXWC.XXWC_INV_RTV_REPORT_DATA_TBL (RECEIPT_NUM,
                                                     URL,
                                                     SHIP_TO_ORG_ID,
                                                     QTY_RECEIVED,
                                                     RECEIVED_QTY,
                                                     ORDER_QUANTITY_UOM,
                                                     ITEM_ID,
                                                     RCT_QTY_RECEIVED,
                                                     TRANSACTION_DATE,
                                                     PO_NUMBER,
                                                     CREATION_DATE,
                                                     RECEIVER,
                                                     BUYER,
                                                     EMAIL_ADDRESS,
                                                     SUPPLIER,
                                                     ORDER_NUMBER,
                                                     ORGANIZATION_NAME,
                                                     ITEM,
                                                     DESCRIPTION,
                                                     SHIP_FROM_ORG_ID,
                                                     SOLD_TO_ORG_ID,
                                                     INVENTORY_ITEM_ID,
                                                     ORGANIZATION_ID,
                                                     ORDERED_QUANTITY,
                                                     PO_LINE_ID,
                                                     SHIPMENT_HEADER_ID,
                                                     HEADER_ID,
                                                     UOM_CODE,
                                                     GROUP_ID,
                                                     ORG_ID,
                                                     SHIPMENT_LINE_ID,
                                                     REQ_DATE,
                                                     LINE_ID,
                                                     CONC_REQUEST_ID)
         SELECT rcvh.receipt_num,
                (SELECT NVL (home_url, NULL) FROM icx_parameters) URL,
                rcvh.ship_to_org_id,
                rcvl.qty_received,
                rcvl.received_qty,
                l.order_quantity_uom,
                rcvl.item_id,
                rct.rct_qty_received,
                TO_CHAR (rct.transaction_date, 'MM/DD/YYYY') transaction_date,
                TO_CHAR (poh.segment1) po_number,
                TO_CHAR (poh.creation_date, 'MM/DD/YYYY') creation_date,
                hre.full_name receiver,
                SUBSTR (hre1.full_name, INSTR (hre1.full_name, ',', -1) + 1)
                   buyer,
                hre1.email_address,
                TO_CHAR (pov.vendor_name) supplier,
                h.order_number,
                org.organization_name,
                msib.segment1 item,
                msib.description,
                h.ship_from_org_id,
                h.sold_to_org_id,
                msib.inventory_item_id,
                msib.organization_id,
                (CASE
                    WHEN xwss.status IN ('BACKORDERED',
                                         'OUT_FOR_DELIVERY/PARTIAL_BACKORDER')
                    THEN
                         NVL (xwss.ordered_quantity, 0)
                       - NVL (xwss.transaction_qty, 0)
                    WHEN xwss.status IN ('DELIVERED',
                                         'CANCELLED',
                                         'OUT_FOR_DELIVERY')
                    THEN
                       0
                    WHEN     xwss.status IS NULL
                         AND l.line_id IS NOT NULL
                         AND (  NVL (
                                   xxwc_ascp_scwb_pkg.get_on_hand (
                                      msib.inventory_item_id,
                                      msib.organization_id,
                                      'T'),
                                   0)
                              - rcvl.received_qty
                              - NVL (xxwc_po_vendor_min_pkg.get_demand (
                                        msib.inventory_item_id,
                                        msib.organization_id,
                                        TO_DATE (
                                           (   TO_CHAR (TRUNC (SYSDATE + 60),
                                                        'YYYY/MM/DD')
                                            || ' 23:59:59'),
                                           'YYYY/MM/DD HH24:MI:SS'),
                                        1,
                                        2,
                                        2,
                                        1,
                                        1,
                                        NULL),
                                     0)) < 0
                    THEN
                       NVL (l.ordered_quantity, 0)
                    WHEN     xwss.status IS NULL
                         AND l.line_id IS NOT NULL
                         AND (  NVL (
                                   xxwc_ascp_scwb_pkg.get_on_hand (
                                      msib.inventory_item_id,
                                      msib.organization_id,
                                      'T'),
                                   0)
                              - rcvl.received_qty
                              - NVL (xxwc_po_vendor_min_pkg.get_demand (
                                        msib.inventory_item_id,
                                        msib.organization_id,
                                        TO_DATE (
                                           (   TO_CHAR (TRUNC (SYSDATE + 60),
                                                        'YYYY/MM/DD')
                                            || ' 23:59:59'),
                                           'YYYY/MM/DD HH24:MI:SS'),
                                        1,
                                        2,
                                        2,
                                        1,
                                        1,
                                        NULL),
                                     0)) >= 0
                    THEN
                       0
                    ELSE
                       NVL (l.ordered_quantity, 0)
                 END)
                   ordered_quantity,
                rcvl.po_line_id,
                rcvh.shipment_header_id,
                h.header_id,
                rct.uom_code,
                msib.inventory_item_id GROUP_ID,
                msib.organization_id org_id,
                rcvl.shipment_line_id,
                (SELECT TO_CHAR (MIN (request_date), 'MM/DD/YY')
                   FROM oe_order_lines_all
                  WHERE     header_id = l.header_id
                        AND inventory_item_id = l.inventory_item_id
                        AND creation_date >= h.creation_date
                        AND flow_status_code IN ('AWAITING_SHIPPING',
                                                 'BOOKED'))
                   req_date,
                l.line_id,
                p_conc_request_id
           FROM rcv_shipment_headers rcvh,
                (  SELECT SUM (quantity_received) qty_received,
                          SUM (quantity_received) received_qty,
                          item_id,
                          shipment_header_id,
                          po_line_id,
                          po_header_id,
                          to_organization_id,
                          shipment_line_id
                     FROM rcv_shipment_lines c
                    WHERE shipment_header_id = p_ship_hdr_id
                 GROUP BY item_id,
                          shipment_header_id,
                          po_line_id,
                          po_header_id,
                          to_organization_id,
                          shipment_line_id) rcvl,
                (  SELECT SUM (quantity) rct_qty_received,
                          po_line_id,
                          transaction_date,
                          organization_id,
                          shipment_header_id,
                          po_header_id,
                          employee_id,
                          uom_code,
                          transaction_type
                     FROM rcv_transactions c
                    WHERE     shipment_header_id = p_ship_hdr_id
                          AND destination_type_code = 'RECEIVING'
                          AND transaction_type = 'RECEIVE'
                 GROUP BY po_line_id,
                          transaction_date,
                          organization_id,
                          shipment_header_id,
                          po_header_id,
                          employee_id,
                          uom_code,
                          transaction_type) rct,
                po_headers_all poh,
                per_all_people_f hre,
                per_all_people_f hre1,
                po_vendors pov,
                oe_order_headers_all h,
                oe_order_lines_all l,
                org_organization_definitions org,
                mtl_system_items_b msib,
                XXWC_WSH_SHIPPING_STG XWSS
          WHERE     rcvh.shipment_header_id = rcvl.shipment_header_id
                AND rcvh.shipment_header_id = p_ship_hdr_id
                AND rcvh.shipment_header_id = rct.shipment_header_id
                AND rcvl.po_line_id = rct.po_line_id
                AND rcvl.po_header_id = poh.po_header_id
                AND rct.po_header_id = poh.po_header_id
                AND rct.employee_id = hre.person_id(+)
                AND poh.agent_id = hre1.person_id(+)
                AND SYSDATE BETWEEN NVL (hre.effective_start_date,
                                         SYSDATE - 1)
                                AND NVL (hre.effective_end_date, SYSDATE + 1)
                AND SYSDATE BETWEEN NVL (hre1.effective_start_date,
                                         SYSDATE - 1)
                                AND NVL (hre1.effective_end_date,
                                         SYSDATE + 1)
                AND rcvh.vendor_id = pov.vendor_id
                AND rcvl.to_organization_id = l.ship_from_org_id(+)
                AND rcvl.item_id = l.inventory_item_id(+)
                AND rcvl.to_organization_id = l.ship_from_org_id(+)
                AND l.header_id = h.header_id(+)
                AND rcvl.to_organization_id = org.organization_id
                AND rcvl.item_id = msib.inventory_item_id(+)
                AND rcvl.to_organization_id = msib.organization_id(+)
                AND l.booked_flag(+) = 'Y'
                AND l.flow_status_code(+) NOT IN ('CLOSED',
                                                  'SHIPPED',
                                                  'CANCELLED',
                                                  'DRAFT',
                                                  'OFFER_EXPIRED',
                                                  'PRE-BILLING_ACCEPTANCE',
                                                  'AWAITING_RETURN',
                                                  'INVOICE_HOLD',
                                                  'ENTERED',
                                                  'BOOKED')
                AND l.header_id = xwss.header_id(+)
                AND l.line_id = xwss.line_id(+)
                AND rcvh.receipt_source_code = 'VENDOR'
         UNION
         ---------- Internal
         SELECT rcvh.receipt_num,
                (SELECT NVL (home_url, NULL) FROM icx_parameters) URL,
                rcvh.ship_to_org_id,
                rcvl.qty_received,
                rcvl.received_qty,
                l.order_quantity_uom,
                rcvl.item_id,
                rct.rct_qty_received,
                TO_CHAR (rct.transaction_date, 'MM/DD/YYYY') transaction_date,
                'REQ ' || TO_CHAR (prha.segment1) PO_NUMBER,
                TO_CHAR (prha.creation_date, 'MM/DD/YYYY') CREATION_DATE,
                hre.full_name receiver,
                SUBSTR (hre1.full_name, INSTR (hre1.full_name, ',', -1) + 1)
                   buyer,
                hre1.email_address,
                TO_CHAR (ORG2.organization_name) SUPPLIER,
                h.order_number,
                org.organization_name,
                msib.segment1 item,
                msib.description,
                h.ship_from_org_id,
                h.sold_to_org_id,
                msib.inventory_item_id,
                msib.organization_id,
                (CASE
                    WHEN xwss.status IN ('BACKORDERED',
                                         'OUT_FOR_DELIVERY/PARTIAL_BACKORDER')
                    THEN
                         NVL (xwss.ordered_quantity, 0)
                       - NVL (xwss.transaction_qty, 0)
                    WHEN xwss.status IN ('DELIVERED',
                                         'CANCELLED',
                                         'OUT_FOR_DELIVERY')
                    THEN
                       0
                    WHEN     xwss.status IS NULL
                         AND l.line_id IS NOT NULL
                         AND (  NVL (
                                   xxwc_ascp_scwb_pkg.get_on_hand (
                                      msib.inventory_item_id,
                                      msib.organization_id,
                                      'T'),
                                   0)
                              - rcvl.received_qty
                              - NVL (xxwc_po_vendor_min_pkg.get_demand (
                                        msib.inventory_item_id,
                                        msib.organization_id,
                                        TO_DATE (
                                           (   TO_CHAR (TRUNC (SYSDATE + 60),
                                                        'YYYY/MM/DD')
                                            || ' 23:59:59'),
                                           'YYYY/MM/DD HH24:MI:SS'),
                                        1,
                                        2,
                                        2,
                                        1,
                                        1,
                                        NULL),
                                     0)) < 0
                    THEN
                       NVL (l.ordered_quantity, 0)
                    WHEN     xwss.status IS NULL
                         AND l.line_id IS NOT NULL
                         AND (  NVL (
                                   xxwc_ascp_scwb_pkg.get_on_hand (
                                      msib.inventory_item_id,
                                      msib.organization_id,
                                      'T'),
                                   0)
                              - rcvl.received_qty
                              - NVL (xxwc_po_vendor_min_pkg.get_demand (
                                        msib.inventory_item_id,
                                        msib.organization_id,
                                        TO_DATE (
                                           (   TO_CHAR (TRUNC (SYSDATE + 60),
                                                        'YYYY/MM/DD')
                                            || ' 23:59:59'),
                                           'YYYY/MM/DD HH24:MI:SS'),
                                        1,
                                        2,
                                        2,
                                        1,
                                        1,
                                        NULL),
                                     0)) >= 0
                    THEN
                       0
                    ELSE
                       NVL (l.ordered_quantity, 0)
                 END)
                   ordered_quantity,
                rcvl.requisition_line_id,
                rcvh.shipment_header_id,
                h.header_id,
                rct.uom_code,
                msib.inventory_item_id GROUP_ID,
                msib.organization_id org_id,
                rcvl.shipment_line_id,
                (SELECT TO_CHAR (MIN (request_date), 'MM/DD/YY')
                   FROM oe_order_lines_all
                  WHERE     header_id = l.header_id
                        AND inventory_item_id = l.inventory_item_id
                        AND creation_date >= h.creation_date
                        AND flow_status_code IN ('AWAITING_SHIPPING',
                                                 'BOOKED'))
                   req_date,
                l.line_id,
                p_conc_request_id
           FROM rcv_shipment_headers rcvh,
                (  SELECT SUM (quantity_received) qty_received,
                          SUM (quantity_received) received_qty,
                          item_id,
                          shipment_header_id,
                          requisition_line_id,
                          to_organization_id,
                          shipment_line_id
                     FROM rcv_shipment_lines c
                    WHERE shipment_header_id = p_ship_hdr_id
                 GROUP BY item_id,
                          shipment_header_id,
                          requisition_line_id,
                          to_organization_id,
                          shipment_line_id) rcvl,
                (  SELECT SUM (quantity) rct_qty_received,
                          po_line_id,
                          transaction_date,
                          organization_id,
                          shipment_header_id,
                          shipment_line_id,
                          requisition_line_id,
                          employee_id,
                          uom_code,
                          transaction_type
                     FROM rcv_transactions c
                    WHERE     shipment_header_id = p_ship_hdr_id
                          AND destination_type_code = 'RECEIVING'
                          AND transaction_type = 'RECEIVE'
                 GROUP BY po_line_id,
                          transaction_date,
                          organization_id,
                          shipment_header_id,
                          shipment_line_id,
                          requisition_line_id,
                          employee_id,
                          uom_code,
                          transaction_type) rct,
                po_requisition_lines_all prla,
                po_requisition_headers_all prha,
                per_all_people_f hre,
                per_all_people_f hre1,
                oe_order_headers_all h,
                oe_order_lines_all l,
                org_organization_definitions org,
                mtl_system_items_b msib,
                org_organization_definitions org2,
                XXWC_WSH_SHIPPING_STG XWSS
          WHERE     rcvh.shipment_header_id = rcvl.shipment_header_id
                AND rcvh.shipment_header_id = p_ship_hdr_id
                AND rcvh.shipment_header_id = rct.shipment_header_id
                AND rcvl.shipment_line_id = rct.shipment_line_id
                AND rcvl.requisition_line_id = prla.requisition_line_id
                AND prla.REQUISITION_HEADER_ID = prha.requisition_header_id
                AND rct.employee_id = hre.person_id(+)
                AND prha.preparer_id = hre1.person_id(+)
                AND SYSDATE BETWEEN NVL (hre.effective_start_date,
                                         SYSDATE - 1)
                                AND NVL (hre.effective_end_date, SYSDATE + 1)
                AND SYSDATE BETWEEN NVL (hre1.effective_start_date,
                                         SYSDATE - 1)
                                AND NVL (hre1.effective_end_date,
                                         SYSDATE + 1)
                AND rcvl.to_organization_id = l.ship_from_org_id(+)
                AND rcvl.item_id = l.inventory_item_id(+)
                AND rcvl.to_organization_id = l.ship_from_org_id(+)
                AND l.header_id = h.header_id(+)
                AND rcvl.to_organization_id = org.organization_id
                AND rcvl.item_id = msib.inventory_item_id(+)
                AND rcvl.to_organization_id = msib.organization_id(+)
                AND l.booked_flag(+) = 'Y'
                AND l.flow_status_code(+) NOT IN ('CLOSED',
                                                  'SHIPPED',
                                                  'CANCELLED',
                                                  'DRAFT',
                                                  'OFFER_EXPIRED',
                                                  'PRE-BILLING_ACCEPTANCE',
                                                  'AWAITING_RETURN',
                                                  'INVOICE_HOLD',
                                                  'ENTERED',
                                                  'BOOKED')
                AND l.header_id = xwss.header_id(+)
                AND l.line_id = xwss.line_id(+)
                AND RCVH.receipt_source_code = 'INTERNAL ORDER'
                AND rcvh.organization_id = org2.organization_id
         UNION
         SELECT rcvh.receipt_num,
                (SELECT NVL (home_url, NULL) FROM icx_parameters) URL,
                rcvh.ship_to_org_id,
                rcvl.qty_received,
                rcvl.received_qty,
                l.order_quantity_uom,
                rcvl.item_id,
                rct.rct_qty_received,
                TO_CHAR (rct.transaction_date, 'MM/DD/YYYY') transaction_date,
                'RMA ' || TO_CHAR (ooha.order_number) PO_NUMBER,
                TO_CHAR (ooha.creation_date) CREATION_DATE,
                hre.full_name receiver,
                SUBSTR (hre1.full_name, INSTR (hre1.full_name, ',', -1) + 1)
                   buyer,
                hre1.email_address,
                TO_CHAR (HP3.party_name) SUPPLIER,
                h.order_number,
                org.organization_name,
                msib.segment1 item,
                msib.description,
                h.ship_from_org_id,
                h.sold_to_org_id,
                msib.inventory_item_id,
                msib.organization_id,
                (CASE
                    WHEN xwss.status IN ('BACKORDERED',
                                         'OUT_FOR_DELIVERY/PARTIAL_BACKORDER')
                    THEN
                         NVL (xwss.ordered_quantity, 0)
                       - NVL (xwss.transaction_qty, 0)
                    WHEN xwss.status IN ('DELIVERED',
                                         'CANCELLED',
                                         'OUT_FOR_DELIVERY')
                    THEN
                       0
                    WHEN     xwss.status IS NULL
                         AND l.line_id IS NOT NULL
                         AND (  NVL (
                                   xxwc_ascp_scwb_pkg.get_on_hand (
                                      msib.inventory_item_id,
                                      msib.organization_id,
                                      'T'),
                                   0)
                              - rcvl.received_qty
                              - NVL (xxwc_po_vendor_min_pkg.get_demand (
                                        msib.inventory_item_id,
                                        msib.organization_id,
                                        TO_DATE (
                                           (   TO_CHAR (TRUNC (SYSDATE + 60),
                                                        'YYYY/MM/DD')
                                            || ' 23:59:59'),
                                           'YYYY/MM/DD HH24:MI:SS'),
                                        1,
                                        2,
                                        2,
                                        1,
                                        1,
                                        NULL),
                                     0)) < 0
                    THEN
                       NVL (l.ordered_quantity, 0)
                    WHEN     xwss.status IS NULL
                         AND l.line_id IS NOT NULL
                         AND (  NVL (
                                   xxwc_ascp_scwb_pkg.get_on_hand (
                                      msib.inventory_item_id,
                                      msib.organization_id,
                                      'T'),
                                   0)
                              - rcvl.received_qty
                              - NVL (xxwc_po_vendor_min_pkg.get_demand (
                                        msib.inventory_item_id,
                                        msib.organization_id,
                                        TO_DATE (
                                           (   TO_CHAR (TRUNC (SYSDATE + 60),
                                                        'YYYY/MM/DD')
                                            || ' 23:59:59'),
                                           'YYYY/MM/DD HH24:MI:SS'),
                                        1,
                                        2,
                                        2,
                                        1,
                                        1,
                                        NULL),
                                     0)) >= 0
                    THEN
                       0
                    ELSE
                       NVL (l.ordered_quantity, 0)
                 END)
                   ordered_quantity,
                NULL,
                rcvh.shipment_header_id,
                h.header_id,
                rct.uom_code,
                msib.inventory_item_id GROUP_ID,
                msib.organization_id org_id,
                rcvl.shipment_line_id,
                (SELECT TO_CHAR (MIN (request_date), 'MM/DD/YY')
                   FROM oe_order_lines_all
                  WHERE     header_id = l.header_id
                        AND inventory_item_id = l.inventory_item_id
                        AND creation_date >= h.creation_date
                        AND flow_status_code IN ('AWAITING_SHIPPING',
                                                 'BOOKED'))
                   req_date,
                l.line_id,
                p_conc_request_id
           FROM rcv_shipment_headers rcvh,
                (  SELECT SUM (quantity_received) qty_received,
                          SUM (quantity_received) received_qty,
                          item_id,
                          shipment_header_id,
                          oe_order_header_id,
                          oe_order_line_id,
                          to_organization_id,
                          shipment_line_id
                     FROM rcv_shipment_lines c
                    WHERE shipment_header_id = p_ship_hdr_id
                 GROUP BY item_id,
                          shipment_header_id,
                          oe_order_header_id,
                          oe_order_line_id,
                          to_organization_id,
                          shipment_line_id) rcvl,
                (  SELECT SUM (quantity) rct_qty_received,
                          po_line_id,
                          transaction_date,
                          organization_id,
                          shipment_header_id,
                          oe_order_header_id,
                          oe_order_line_id,
                          employee_id,
                          uom_code,
                          transaction_type
                     FROM rcv_transactions c
                    WHERE     shipment_header_id = p_ship_hdr_id
                          AND destination_type_code = 'RECEIVING'
                          AND transaction_type = 'RECEIVE'
                 GROUP BY po_line_id,
                          transaction_date,
                          organization_id,
                          shipment_header_id,
                          oe_order_header_id,
                          oe_order_line_id,
                          employee_id,
                          uom_code,
                          transaction_type) rct,
                oe_order_headers_all ooha,
                oe_order_lines_all oola,
                per_all_people_f hre,
                per_all_people_f hre1,
                oe_order_headers_all h,
                oe_order_lines_all l,
                org_organization_definitions org,
                mtl_system_items_b msib,
                org_organization_definitions org2,
                hz_cust_accounts hca3,
                hz_parties hp3,
                fnd_user fu,
                XXWC_WSH_SHIPPING_STG XWSS
          WHERE     rcvh.shipment_header_id = rcvl.shipment_header_id
                AND rcvh.shipment_header_id = p_ship_hdr_id
                AND rcvh.shipment_header_id = rct.shipment_header_id
                AND rcvl.oe_order_header_id = ooha.header_id
                AND rcvl.oe_order_line_id = oola.line_id
                AND rcvl.oe_order_line_id = rct.oe_order_line_id
                AND rcvl.oe_order_header_id = rct.oe_order_header_id
                AND rct.employee_id = hre.person_id(+)
                AND fu.employee_id = hre1.person_id(+)
                AND SYSDATE BETWEEN NVL (hre.effective_start_date,
                                         SYSDATE - 1)
                                AND NVL (hre.effective_end_date, SYSDATE + 1)
                AND SYSDATE BETWEEN NVL (hre1.effective_start_date,
                                         SYSDATE - 1)
                                AND NVL (hre1.effective_end_date,
                                         SYSDATE + 1)
                AND rcvl.to_organization_id = l.ship_from_org_id(+)
                AND rcvl.item_id = l.inventory_item_id(+)
                AND rcvl.to_organization_id = l.ship_from_org_id(+)
                AND l.header_id = h.header_id(+)
                AND rcvl.to_organization_id = org.organization_id
                AND rcvl.item_id = msib.inventory_item_id(+)
                AND rcvl.to_organization_id = msib.organization_id(+)
                AND l.booked_flag(+) = 'Y'
                AND l.flow_status_code(+) NOT IN ('CLOSED',
                                                  'SHIPPED',
                                                  'CANCELLED',
                                                  'DRAFT',
                                                  'OFFER_EXPIRED',
                                                  'PRE-BILLING_ACCEPTANCE',
                                                  'AWAITING_RETURN',
                                                  'INVOICE_HOLD',
                                                  'ENTERED',
                                                  'BOOKED')
                AND l.header_id = xwss.header_id(+)
                AND l.line_id = xwss.line_id(+)
                AND RCVH.receipt_source_code = 'CUSTOMER'
                AND rcvh.organization_id = org2.organization_id
                AND hca3.cust_account_id = ooha.sold_to_org_id
                AND hca3.party_id = hp3.party_id
                AND ooha.created_by = fu.user_id;

      ln_count := SQL%ROWCOUNT;

      lvc_sec := 'Insert completed';

      IF NVL (ln_count, 0) > 0
      THEN
         -- Version 2.0, Commented this delete and put a filter at the cursor level.
		 /*DELETE FROM XXWC.XXWC_INV_RTV_REPORT_DATA_TBL
               WHERE     ORDERED_QUANTITY = 0
                     AND CONC_REQUEST_ID = P_CONC_REQUEST_ID;*/

         lvc_sec := 'Data Process start';

         FOR REC_ITEM IN CUR_ITEM
         LOOP
            lvc_sec := 'Item Process Starts ' || REC_ITEM.ITEM_ID;


            FOR REC_DATA IN CUR_DATA (REC_ITEM.ITEM_ID)
            LOOP
               ln_count := 0;
               ln_alloc_qty := 0;
               lvc_sec :=
                     'checking allocated qty shipment_line_id '
                  || REC_DATA.shipment_line_id;

               SELECT SUM (NVL (ADJ_QTY, 0))
                 INTO ln_alloc_qty
                 FROM XXWC.XXWC_INV_RTV_REPORT_DATA_B
                WHERE     1 = 1
                      AND CONC_REQUEST_ID = P_CONC_REQUEST_ID
                      AND shipment_line_id = REC_DATA.shipment_line_id;

               ln_item_adj := 0;

               SELECT SUM (NVL (adj_qty, 0))
                 INTO ln_item_adj
                 FROM XXWC.XXWC_INV_RTV_REPORT_DATA_B
                WHERE     item_id = REC_DATA.item_id
                      AND CONC_REQUEST_ID = P_CONC_REQUEST_ID
                      AND order_number = REC_DATA.order_number
                      AND LINE_ID = REC_DATA.LINE_ID
                      AND ADJ_QTY <= REC_DATA.ordered_quantity;


               IF REC_DATA.ORDERED_QUANTITY = ln_item_adj
               THEN
                  CONTINUE;
               ELSE
                  REC_DATA.ordered_quantity :=
                     REC_DATA.ordered_quantity - NVL (ln_item_adj, 0);
               END IF;

               ln_tobe_alloc_qty := 0;

               ln_tobe_alloc_qty :=
                  REC_DATA.RECEIVED_QTY - NVL (ln_alloc_qty, 0);

               IF ln_tobe_alloc_qty >= REC_DATA.ORDERED_QUANTITY
               THEN
                  ln_alloc_qty := REC_DATA.ORDERED_QUANTITY;
               ELSE
                  ln_alloc_qty := ln_tobe_alloc_qty;
               END IF;

              <<INSERT_STEP>>
               lvc_sec :=
                     'Inserting data into XXWC.XXWC_INV_RTV_REPORT_DATA_B'
                  || REC_DATA.order_number;

               INSERT
                 INTO XXWC.XXWC_INV_RTV_REPORT_DATA_B (RECEIPT_NUM,
                                                       URL,
                                                       SHIP_TO_ORG_ID,
                                                       QTY_RECEIVED,
                                                       RECEIVED_QTY,
                                                       ORDER_QUANTITY_UOM,
                                                       ITEM_ID,
                                                       RCT_QTY_RECEIVED,
                                                       TRANSACTION_DATE,
                                                       PO_NUMBER,
                                                       CREATION_DATE,
                                                       RECEIVER,
                                                       BUYER,
                                                       EMAIL_ADDRESS,
                                                       SUPPLIER,
                                                       ORDER_NUMBER,
                                                       ORGANIZATION_NAME,
                                                       ITEM,
                                                       DESCRIPTION,
                                                       SHIP_FROM_ORG_ID,
                                                       SOLD_TO_ORG_ID,
                                                       INVENTORY_ITEM_ID,
                                                       ORGANIZATION_ID,
                                                       ORDERED_QUANTITY,
                                                       PO_LINE_ID,
                                                       SHIPMENT_HEADER_ID,
                                                       HEADER_ID,
                                                       UOM_CODE,
                                                       GROUP_ID,
                                                       ORG_ID,
                                                       SHIPMENT_LINE_ID,
                                                       REQ_DATE,
                                                       LINE_ID,
                                                       CONC_REQUEST_ID,
                                                       ADJ_QTY)
               VALUES (REC_DATA.RECEIPT_NUM,
                       REC_DATA.URL,
                       REC_DATA.SHIP_TO_ORG_ID,
                       REC_DATA.QTY_RECEIVED,
                       REC_DATA.RECEIVED_QTY,
                       REC_DATA.ORDER_QUANTITY_UOM,
                       REC_DATA.ITEM_ID,
                       REC_DATA.RCT_QTY_RECEIVED,
                       REC_DATA.TRANSACTION_DATE,
                       REC_DATA.PO_NUMBER,
                       REC_DATA.CREATION_DATE,
                       REC_DATA.RECEIVER,
                       REC_DATA.BUYER,
                       REC_DATA.EMAIL_ADDRESS,
                       REC_DATA.SUPPLIER,
                       REC_DATA.ORDER_NUMBER,
                       REC_DATA.ORGANIZATION_NAME,
                       REC_DATA.ITEM,
                       REC_DATA.DESCRIPTION,
                       REC_DATA.SHIP_FROM_ORG_ID,
                       REC_DATA.SOLD_TO_ORG_ID,
                       REC_DATA.INVENTORY_ITEM_ID,
                       REC_DATA.ORGANIZATION_ID,
                       REC_DATA.ORDERED_QUANTITY,
                       REC_DATA.PO_LINE_ID,
                       REC_DATA.SHIPMENT_HEADER_ID,
                       REC_DATA.HEADER_ID,
                       REC_DATA.UOM_CODE,
                       REC_DATA.GROUP_ID,
                       REC_DATA.ORG_ID,
                       REC_DATA.SHIPMENT_LINE_ID,
                       REC_DATA.REQ_DATE,
                       REC_DATA.LINE_ID,
                       P_CONC_REQUEST_ID,
                       ln_alloc_qty);
            END LOOP;
         END LOOP;

         lvc_sec :=
            'Inserting Non dup items data into XXWC.XXWC_INV_RTV_REPORT_DATA_B';

         FOR REC_DATA1 IN CUR_DATA1
         LOOP
            lvc_sec :=
                  'Inserting into XXWC.XXWC_INV_RTV_REPORT_DATA_B for shipment line id'
               || REC_DATA1.shipment_line_id;

            INSERT INTO XXWC.XXWC_INV_RTV_REPORT_DATA_B (RECEIPT_NUM,
                                                         URL,
                                                         SHIP_TO_ORG_ID,
                                                         QTY_RECEIVED,
                                                         RECEIVED_QTY,
                                                         ORDER_QUANTITY_UOM,
                                                         ITEM_ID,
                                                         RCT_QTY_RECEIVED,
                                                         TRANSACTION_DATE,
                                                         PO_NUMBER,
                                                         CREATION_DATE,
                                                         RECEIVER,
                                                         BUYER,
                                                         EMAIL_ADDRESS,
                                                         SUPPLIER,
                                                         ORDER_NUMBER,
                                                         ORGANIZATION_NAME,
                                                         ITEM,
                                                         DESCRIPTION,
                                                         SHIP_FROM_ORG_ID,
                                                         SOLD_TO_ORG_ID,
                                                         INVENTORY_ITEM_ID,
                                                         ORGANIZATION_ID,
                                                         ORDERED_QUANTITY,
                                                         PO_LINE_ID,
                                                         SHIPMENT_HEADER_ID,
                                                         HEADER_ID,
                                                         UOM_CODE,
                                                         GROUP_ID,
                                                         ORG_ID,
                                                         SHIPMENT_LINE_ID,
                                                         REQ_DATE,
                                                         CONC_REQUEST_ID,
                                                         LINE_ID)
                 VALUES (REC_DATA1.RECEIPT_NUM,
                         REC_DATA1.URL,
                         REC_DATA1.SHIP_TO_ORG_ID,
                         REC_DATA1.QTY_RECEIVED,
                         REC_DATA1.RECEIVED_QTY,
                         REC_DATA1.ORDER_QUANTITY_UOM,
                         REC_DATA1.ITEM_ID,
                         REC_DATA1.RCT_QTY_RECEIVED,
                         REC_DATA1.TRANSACTION_DATE,
                         REC_DATA1.PO_NUMBER,
                         REC_DATA1.CREATION_DATE,
                         REC_DATA1.RECEIVER,
                         REC_DATA1.BUYER,
                         REC_DATA1.EMAIL_ADDRESS,
                         REC_DATA1.SUPPLIER,
                         REC_DATA1.ORDER_NUMBER,
                         REC_DATA1.ORGANIZATION_NAME,
                         REC_DATA1.ITEM,
                         REC_DATA1.DESCRIPTION,
                         REC_DATA1.SHIP_FROM_ORG_ID,
                         REC_DATA1.SOLD_TO_ORG_ID,
                         REC_DATA1.INVENTORY_ITEM_ID,
                         REC_DATA1.ORGANIZATION_ID,
                         REC_DATA1.ORDERED_QUANTITY,
                         REC_DATA1.PO_LINE_ID,
                         REC_DATA1.SHIPMENT_HEADER_ID,
                         REC_DATA1.HEADER_ID,
                         REC_DATA1.UOM_CODE,
                         REC_DATA1.GROUP_ID,
                         REC_DATA1.ORG_ID,
                         REC_DATA1.SHIPMENT_LINE_ID,
                         REC_DATA1.REQ_DATE,
                         P_CONC_REQUEST_ID,
                         REC_DATA1.LINE_ID);
         END LOOP;

         COMMIT;
         P_RET_STATUS := 'S';
      ELSE
         P_RET_STATUS := 'E';
      END IF;
   EXCEPTION
      WHEN OTHERS
      THEN
         P_RET_STATUS := 'E';
         xxcus_error_pkg.xxcus_error_main_api (
            p_called_from         => 'XXWC_RTV_SHIPMENT_POPULATE_PRC',
            p_calling             => ' ',
            p_request_id          => P_CONC_REQUEST_ID,
            p_ora_error_msg       => SUBSTR (SQLERRM, 1, 2000),
            p_error_desc          => lvc_sec,
            p_distribution_list   => lvc_dflt_email,
            p_module              => 'XXWC');
   END;
END XXWC_RTV_SHIPMENT_PKG;
/