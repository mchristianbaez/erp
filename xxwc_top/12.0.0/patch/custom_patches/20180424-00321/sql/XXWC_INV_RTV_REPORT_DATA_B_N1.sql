/*
-- *********************************************************************************************************************************
-- $Header XXWC_INV_RTV_REPORT_DATA_TBL $
-- Module Name: XXWC_INV_RTV_REPORT_DATA_TBL

-- REVISIONS:
-- Ver        Date        Author           Description
-- ---------  ----------  ----------       ----------------
-- 1.0       04/13/2018  P.Vamshidhar      Initial Build - Task ID: 20170817-00112 - Receipt Traveler Enhancement Request
-- 1.0       04/25/2018  K.Naveen      Deploying as part of Task ID: 20180424-00321 - Blank Receipt Traveler Report Fix
-- *********************************************************************************************************************************/
CREATE INDEX XXWC.XXWC_INV_RTV_REPORT_DATA_B_N1
   ON XXWC.XXWC_INV_RTV_REPORT_DATA_B (CONC_REQUEST_ID);


