DROP VIEW APPS.XXWC_PO_SEL_ONHAND_VW;

   /*************************************************************************
      $Header XXWC_PO_SEL_ONHAND_TBL $
      Module Name: XXWC_PO_SEL_ONHAND_TBL.SQL
   
      PURPOSE:   Table used for loading on-hand information
   
      REVISIONS:
      Ver        Date        Author                  Description
      ---------  ----------  ---------------         -------------------------
      1.0        01/13/2015  Shankar H               Initial Version
      1.1        02/12/2015  Gopi Damuluri           TMS 20150209-00077 Change the View to use XXWC_PO_SEL_ONHAND_TBL
	  1.2        04/11/2015  Pattabhi Avula          TMS 20151029-00187  -- on hand qty logic changes for PUBD
   ****************************************************************************/

CREATE OR REPLACE FORCE VIEW APPS.XXWC_PO_SEL_ONHAND_VW
(
   INVENTORY_ITEM_ID,
   ORGANIZATION_ID,
   SEGMENT1,
   DESCRIPTION,
   AMU,
   ORGANIZATION_CODE,
   PRICING_REGION,
   ON_HAND_QTY,
   AVAILABLE_TO_RESERVE,
   AVAILABLE_TO_TRANSACT,
   OPEN_SALES_ORDERS,
   AVERAGE_COST,
   ON_HAND_GT_90,
   ON_HAND_GT_180,
   ON_HAND_GT_270,
   LAST_RECEIPT_DATE,
   MIN,
   MAX
)
AS
   SELECT stg.inventory_item_id,
          stg.organization_id,
          stg.segment1,
          stg.description,
          stg.amu,
          stg.organization_code,
          stg.pricing_region,
         -- xxwc_ascp_scwb_pkg.get_on_hand (stg.inventory_item_id, stg.organization_id, 'RQ')*/  -- Commented Ver 1.2
		 NVL(XXWC_MV_ROUTINES_ADD_PKG.xxwc_inv_available(stg.organization_id,stg.inventory_item_id,'GENERAL'),0)on_hand_qty,  -- Added for Ver 1.2		  
          xxwc_ascp_scwb_pkg.get_on_hand (stg.inventory_item_id, stg.organization_id, 'R') available_to_reserve,
          xxwc_ascp_scwb_pkg.get_on_hand (stg.inventory_item_id, stg.organization_id, 'T') available_to_transact,
          (SELECT NVL (
                     SUM (
                          l.ordered_quantity
                        * po_uom_s.po_uom_convert (um.unit_of_measure,
                                                   stg.primary_unit_of_measure,
                                                   l.inventory_item_id)),
                     0)
             FROM apps.oe_order_lines l, apps.oe_order_headers h, MTL_UNITS_OF_MEASURE_VL um
            WHERE     l.header_id = h.header_id
                  AND h.flow_status_code NOT IN ('ENTERED', 'CLOSED', 'CANCELLED')
                  AND (   (l.flow_status_code = 'AWAITING_SHIPPING')
                       OR (l.flow_status_code = 'BOOKED' AND l.line_type_id = 1005))
                  AND l.ship_from_org_id = stg.organization_id
                  AND l.inventory_item_id = stg.inventory_item_id
                  AND l.order_quantity_uom = um.uom_code)
             open_sales_orders,
          cst_cost_api.get_item_cost(1,stg.inventory_item_id,stg.organization_id,null,null,5) average_cost,
          stg.on_hand_gt_90,
          stg.on_hand_gt_180,
          stg.on_hand_gt_270,
          (SELECT MAX (rtrx.transaction_date)
             FROM rcv_transactions rtrx
                , rcv_shipment_lines rsl
            WHERE rtrx.transaction_Type = 'RECEIVE'
              AND rtrx.shipment_line_id = rsl.shipment_line_id
              AND stg.inventory_item_id = rsl.item_id
              AND stg.organization_id   = rsl.to_organization_id)  last_receipt_date,
          stg.min,
          stg.max
     FROM xxwc.xxwc_po_sel_onhand_tbl stg -- TMS 20150209-00077
    WHERE 1 = 1

-- TMS 20150209-00077 - Commented > Start
--CREATE OR REPLACE FORCE VIEW APPS.XXWC_PO_SEL_ONHAND_VW
--(
--   INVENTORY_ITEM_ID,
--   ORGANIZATION_ID,
--   SEGMENT1,
--   DESCRIPTION,
--   AMU,
--   ORGANIZATION_CODE,
--   PRICING_REGION,
--   ON_HAND_QTY,
--   AVAILABLE_TO_RESERVE,
--   AVAILABLE_TO_TRANSACT,
--   OPEN_SALES_ORDERS,
--   AVERAGE_COST,
--   ON_HAND_GT_90,
--   ON_HAND_GT_180,
--   ON_HAND_GT_270,
--   LAST_RECEIPT_DATE,
--   MIN,
--   MAX
--)
--AS
--   SELECT a.inventory_item_id,
--          a.organization_id,
--          a.segment1,
--          a.description,
--          a.attribute20 amu,
--          /*b.organization_code ,
--          b.attribute9 pricing_region ,*/
--          -- Above lines Commented By Manjula chellappan on 10-Apr-2014 and added new lines below
--          (SELECT organization_code
--             FROM mtl_parameters
--            WHERE organization_id = a.organization_id)
--             organization_code,
--          (SELECT attribute9
--             FROM mtl_parameters
--            WHERE organization_id = a.organization_id)
--             pricing_region,
--          xxwc_ascp_scwb_pkg.get_on_hand (a.inventory_item_id, a.organization_id, 'RQ') on_hand_qty -- Shankar TMS 20130107-00851 17-Jan-2013 changed from H
--                                                                                                   -- to RQ
--          ,
--          xxwc_ascp_scwb_pkg.get_on_hand (a.inventory_item_id, a.organization_id, 'R')
--             available_to_reserve,
--          xxwc_ascp_scwb_pkg.get_on_hand (a.inventory_item_id, a.organization_id, 'T')
--             available_to_transact                -- Added by Shankar 12-19-2012 for AIS enhancement
--                                  --TMS 20121211-00825
--          ,
--          (SELECT NVL (
--                     SUM (
--                          l.ordered_quantity
--                        * po_uom_s.po_uom_convert (um.unit_of_measure,
--                                                   a.primary_unit_of_measure,
--                                                   l.inventory_item_id)),
--                     0)                                    -- Shankar TMS 20130124-00974 23-Jan-2013
--             FROM apps.oe_order_lines l, apps.oe_order_headers h, MTL_UNITS_OF_MEASURE_VL um
--            WHERE     l.header_id = h.header_id
--                  AND h.flow_status_code NOT IN ('ENTERED', 'CLOSED', 'CANCELLED')
--                  AND (   (l.flow_status_code = 'AWAITING_SHIPPING')
--                       OR (l.flow_status_code = 'BOOKED' AND l.line_type_id = 1005)) -- Shankar TMS 20130107-00851 17-Jan-2013
--                  AND l.ship_from_org_id = a.organization_id
--                  AND l.inventory_item_id = a.inventory_item_id
--                  AND l.order_quantity_uom = um.uom_code)
--             open_sales_orders,
--             /*
--          (SELECT item_cost
--             FROM CST_ITEM_COST_TYPE_V
--            WHERE     cost_type = 'Average'
--                  AND inventory_item_id = a.inventory_item_id
--                  AND organization_id = a.organization_id
--                  AND frozen_standard_flag = 1
--                  AND ROWNUM = 1)  */
--                  --commented and added api call for average cost by Shankar 13-Jan-15  Task ID: 20141215-00214 
--                  cst_cost_api.get_item_cost(1,a.inventory_item_id,a.organization_id,null,null,5) average_cost,
--          xxwc_ascp_scwb_pkg.get_on_hand_gt_n (a.inventory_item_id,
--                                               a.organization_id,
--                                               NVL (fnd_profile.VALUE ('XXWC_INV_AGE_DAYS1'), 90))
--             on_hand_gt_90,
--          xxwc_ascp_scwb_pkg.get_on_hand_gt_n (a.inventory_item_id,
--                                               a.organization_id,
--                                               NVL (fnd_profile.VALUE ('XXWC_INV_AGE_DAYS3'), 180))
--             on_hand_gt_180,       -- 20140819-00097  Added on 9/09/2014 by Pattabhi  for 180 column
--          xxwc_ascp_scwb_pkg.get_on_hand_gt_n (a.inventory_item_id,
--                                               a.organization_id,
--                                               NVL (fnd_profile.VALUE ('XXWC_INV_AGE_DAYS2'), 270))
--             on_hand_gt_270,
--          (SELECT MAX (rtrx.transaction_date)
--             FROM rcv_transactions rtrx, rcv_shipment_lines rsl
--            WHERE     rtrx.transaction_Type = 'RECEIVE'
--                  AND rtrx.shipment_line_id = rsl.shipment_line_id
--                  AND a.inventory_item_id = rsl.item_id
--                  AND a.organization_id = rsl.to_organization_id)
--             last_receipt_date,
--          --20130604-00751  Added 6/25/2013  Min and Max Quantity
--          NVL (A.min_minmax_quantity, 0) min_minmax_quantity,
--          NVL (a.max_minmax_quantity, 0) max_minmax_quantity
--     FROM mtl_system_items a /*,
--          mtl_parameters b*/
--    --Commented by Manjula on 10-Apr-14
--    WHERE     1 = 1
--          -- a.organization_id = b.organization_id
--          --Above line Commented by Manjula on 10-Apr-14
--          AND EXISTS
--                 (SELECT 'x'
--                    FROM mtl_onhand_quantities c
--                   WHERE     a.organization_id = c.organization_id
--                         AND a.inventory_item_id = c.inventory_item_id);
-- TMS 20150209-00077 - Commented < End
/