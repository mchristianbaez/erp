CREATE OR REPLACE PACKAGE BODY apps.xxwc_ahh_item_attr_assign_pkg IS
  /***************************************************************************
  *    Script Name: xxwc_ahh_item_attr_assign_pkg.pks
  *
  *    Interface / Conversion Name: Item conversion.
  *
  *    Functional Purpose: Convert Items using Interface for A.H Harris
  *
  *    History:
  *
  *    Version    Date             Author       Description
  ******************************************    ******************************
  *    1.0        10/01/2018       Rakesh P     TMS#20181001-00003 - AHH-Data fix program for updating attributes and other entities
  *    1.1        10/22/2018       Nancy pahwa  TMS#20181017-00002 - Added the new orgs for wave2 
  *****************************************************************************/

  g_debug    VARCHAR2(1) := 'Y';
  /**g_cmp_psts VARCHAR2(3) := 'CMP'; -- Complete
  g_inp_psts VARCHAR2(3) := 'INP'; -- Inprocess
  g_err_psts VARCHAR2(3) := 'ERR'; -- Error
  g_new_psts VARCHAR2(3) := 'NEW'; -- New/eligible for pickup.

  g_scs_sts VARCHAR2(1) := 'S'; -- Success
  g_fl_sts  VARCHAR2(1) := 'F'; -- Failed
  g_err_sts VARCHAR2(1) := 'E'; -- Errored
  g_prs_sts VARCHAR2(1) := 'P'; -- Processing
  g_new_sts VARCHAR2(1) := 'N'; -- Processings**/
  ---
  -- PROCEDURE: Prints Debug/Log Message - Checks for Debug Flag.
  ---
  PROCEDURE print_debug(p_print_str IN VARCHAR2) IS
  BEGIN
    IF g_debug = 'Y' THEN
      fnd_file.put_line(fnd_file.log
                       ,p_print_str);
      -- dbms_output.put_line(p_print_str);
    END IF;
  END print_debug;

  ---
  -- PROCEDURE: Print Log Message - Ignores Debug Flag.
  ---
  PROCEDURE print_log(p_print_str IN VARCHAR2) IS
  BEGIN
    fnd_file.put_line(fnd_file.log
                     ,p_print_str);
    --dbms_output.put_line(p_print_str);
  END print_log;

  ---
  -- PROCEDURE: Print Log Message - Ignores Debug Flag.
  ---
  PROCEDURE print_out(p_print_str IN VARCHAR2) IS
  BEGIN
    fnd_file.put_line(fnd_file.output
                     ,p_print_str);
    --dbms_output.put_line(p_print_str);
  END print_out;

  ---
  -- PROCEDURE: Convert Varchar2 into Number function
  ---
  FUNCTION convert_number(pid     IN ROWID
                         ,p_col   IN VARCHAR2
                         ,p_value IN VARCHAR2) RETURN NUMBER IS
    l_ret_value NUMBER;
  BEGIN
    SELECT to_number(p_value)
      INTO l_ret_value
      FROM dual;
    RETURN l_ret_value;
  EXCEPTION
    WHEN invalid_number THEN
      RETURN NULL;
    WHEN OTHERS THEN
      RETURN NULL;
  END convert_number;

  ---
  -- PROCEDURE: Assign_Sourcing_Rule, Create Source Rule and Assignments
  ---
  PROCEDURE assign_sourcing_rule(errbuf                OUT VARCHAR2
                                 ,retcode              OUT VARCHAR2
                                ,p_item_creation_date  IN DATE
                                ,p_item1               IN VARCHAR2
                                ,p_item2               IN VARCHAR2
                                ,p_item3               IN VARCHAR2
                                ) IS

    CURSOR c_vendor_num_org_list IS
      SELECT TRIM(nvl((SELECT segment1
                        FROM ap_suppliers
                       WHERE attribute1 = mref.icsw_arpvendno
                         AND rownum < 2)
                     ,vendor_owner_number)) vendor_owner_number
            ,vendor_name
            ,item
            ,icsp_prod
            ,msib.description
            ,msib.inventory_item_id
            ,msib.organization_id
            ,mref.icsw_arpvendno
        FROM xxwc.xxwc_item_master_ref mref
            ,mtl_system_items_b        msib
       WHERE segment1 = item
         AND organization_id <> 222
         AND EXISTS (SELECT 1
                FROM mtl_parameters                   mp
                    ,xxwc.xxwc_ap_branch_list_stg_tbl br
               WHERE mp.organization_code = br.oracle_branch_number
                 AND mp.organization_id = msib.organization_id
                 AND br.process_status = 'Y')
         AND EXISTS (SELECT  1
                      FROM apps.mtl_system_items_b msi,
                           apps.org_organization_definitions ood,
                           apps.mtl_item_categories_v mic
                     WHERE msi.organization_id = ood.organization_id
                       AND ood.organization_code IN ('189', --1.1
                                     '188',
                                     '187',
                                     '186',
                                     '149',
                                     '148',
                                     '513',
                                     '514',
                                     '516',
                                     '517',
                                     '515',
                                     '521',
                                     '518',
                                     '519',
                                     '509',
                                     '522',
                                     '510',
                                     '511',
                                     '512',
                                     '523')
                       AND msi.inventory_item_id = mic.inventory_item_id
                       AND msi.organization_id = mic.organization_id
                       AND NVL(mic.CATEGORY_CONCAT_SEGS,'000') <> 'PR.PRMO'
                       AND mic.STRUCTURE_ID = 101
                       AND TRUNC(msi.creation_date) <= TRUNC (p_item_creation_date)
                       AND msi.created_by NOT IN (15985, 50485, 15986)
                       AND msi.inventory_item_id = msib.inventory_item_id
                       AND msi.organization_id = msib.organization_id
                       AND msi.segment1 IN
                                          (p_item1
                                          ,p_item2
                                          ,p_item3
                                          ,(CASE WHEN p_item1 IS NULL AND p_item2 IS NULL AND p_item3 IS NULL THEN msi.segment1 ELSE NULL END)
                                          )
                       );

    TYPE t_vendor_num_org_list IS TABLE OF c_vendor_num_org_list%ROWTYPE;


    l_vendor_num_org_list_tbl t_vendor_num_org_list;

    l_array_size NUMBER := 5000;
    l_sec        VARCHAR2(10);

    l_sourcing_rule_id NUMBER;
    l_vendor_id        ap.ap_suppliers.vendor_id%TYPE;
    l_vendor_num       ap.ap_suppliers.segment1%TYPE;
    l_vendor_name      ap.ap_suppliers.vendor_name%TYPE;
    l_vendor_site_id   ap.ap_supplier_sites_all.vendor_site_id%TYPE;

    l_src_scs_cnt      NUMBER := 0;
    l_src_fal_cnt      NUMBER := 0;
    l_src_err_cnt      NUMBER := 0;
    l_prs_cnt          NUMBER := 0;
    l_prs_err_cnt      NUMBER := 0;
    l_batch_cnt        NUMBER := 0;
    l_src_asgn_cnt     NUMBER := 0;
    l_src_fal_asgn_cnt NUMBER := 0;
    l_src_err_asgn_cnt NUMBER := 0;
    x_msg_count        NUMBER := 0;

    l_msg_data               VARCHAR2(2000);
    l_assignment_set_id      NUMBER;
    l_sourcing_rule_rec      mrp_sourcing_rule_pub.sourcing_rule_rec_type;
    l_sourcing_rule_val_rec  mrp_sourcing_rule_pub.sourcing_rule_val_rec_type;
    l_receiving_org_tbl      mrp_sourcing_rule_pub.receiving_org_tbl_type;
    l_receiving_org_val_tbl  mrp_sourcing_rule_pub.receiving_org_val_tbl_type;
    l_shipping_org_tbl       mrp_sourcing_rule_pub.shipping_org_tbl_type;
    l_shipping_org_val_tbl   mrp_sourcing_rule_pub.shipping_org_val_tbl_type;
    lx_sourcing_rule_rec     mrp_sourcing_rule_pub.sourcing_rule_rec_type;
    lx_sourcing_rule_val_rec mrp_sourcing_rule_pub.sourcing_rule_val_rec_type;
    lx_receiving_org_tbl     mrp_sourcing_rule_pub.receiving_org_tbl_type;
    lx_receiving_org_val_tbl mrp_sourcing_rule_pub.receiving_org_val_tbl_type;
    lx_shipping_org_tbl      mrp_sourcing_rule_pub.shipping_org_tbl_type;
    lx_shipping_org_val_tbl  mrp_sourcing_rule_pub.shipping_org_val_tbl_type;
    x_return_status VARCHAR2(100);
    x_msg_data VARCHAR2(4000);
    l_sourcing_rule_success_cnt NUMBER :=0;
    l_sourcing_rule_error_cnt   NUMBER :=0;
    l_mrp_sr_assignments_cnt    NUMBER :=0;
  BEGIN

    l_sec                     := '10';
    l_vendor_num_org_list_tbl := t_vendor_num_org_list();

    print_log(rpad('***'
                  ,100
                  ,'*'));
    print_log(' *** ITEM ORG, Sourcing Rule Update  STARTED ***');

    l_sec := '20';
    -- Get the list of Vendors
    OPEN c_vendor_num_org_list;

    LOOP

      l_sec := '30';
      -- Initializations
      l_batch_cnt               := l_batch_cnt + 1;
      l_vendor_num_org_list_tbl := NULL;

      l_sec := '40';
      FETCH c_vendor_num_org_list BULK COLLECT
        INTO l_vendor_num_org_list_tbl LIMIT l_array_size;

      --l_row_identifier := 0;
      print_log('***  BATCH START *** COUNT:: ' || l_vendor_num_org_list_tbl.count);

      l_sec := '50'; -- Create Master Vendor Number
      --FOR rec_items IN c_get_vendor_num LOOP
      FOR r IN l_vendor_num_org_list_tbl.first .. l_vendor_num_org_list_tbl.last LOOP

        l_sec     := '60';
        l_prs_cnt := l_prs_cnt + 1;
        BEGIN
          -- Initialize Return Status Variable
          l_sec              := '70';
          x_return_status    := fnd_api.g_ret_sts_success;
          l_sourcing_rule_id := NULL;
          l_vendor_id        := NULL;
          l_vendor_num       := NULL;
          l_vendor_name      := NULL;
          l_vendor_site_id   := NULL;

          print_log(rpad('-'
                        ,100
                        ,'-'));

          print_log(rpad('*** OrgID.Item> : '
                        ,30
                        ,' ') || ': ' || l_vendor_num_org_list_tbl(r).organization_id || '.' || l_vendor_num_org_list_tbl(r).item || '>' ||
                    '*** START ***');

          --
          -- Get Vendor Owner Number
          --
          xxwc_ahh_item_conv_pkg.get_vendor_info
                         (l_vendor_num_org_list_tbl(r).organization_id
                         ,l_vendor_num_org_list_tbl(r).icsp_prod
                         ,l_vendor_num_org_list_tbl(r).vendor_owner_number
                         ,l_vendor_id
                         ,l_vendor_num
                         ,l_vendor_name
                         ,l_vendor_site_id);

          print_log(rpad('*** Ven#,ID,Name,SiteID'
                        ,30
                        ,' ') || ': ' || l_vendor_id || ',' || l_vendor_num || ',' ||
                    l_vendor_name || ',' || l_vendor_site_id);
          --

          IF l_vendor_id IS NOT NULL THEN
            --
            BEGIN
              l_sec := '80';
              -- Get Sourcing Rule against Vendor ID
              SELECT msr.sourcing_rule_id
                INTO l_sourcing_rule_id
                FROM mrp.mrp_sourcing_rules msr
                    ,mrp.mrp_sr_receipt_org msro
                    ,mrp.mrp_sr_source_org  msso
               WHERE msr.sourcing_rule_id = msro.sourcing_rule_id
                 AND msro.sr_receipt_id = msso.sr_receipt_id
                 AND msso.vendor_id = l_vendor_id
                 AND nvl(msro.disable_date
                        ,trunc(SYSDATE)) >= trunc(SYSDATE)
                    --AND     msr.Organization_ID = p_Organization_ID --* GLobal Sourcing Rule
                 AND rownum = 1;

            EXCEPTION
              WHEN no_data_found THEN
                l_sourcing_rule_id := NULL;
                print_log(rpad('***'
                              ,30
                              ,' ') || ': Sourcing Rule Id Not found.');

              WHEN OTHERS THEN
                l_sourcing_rule_id := NULL;
                print_log(rpad('***'
                              ,30
                              ,' ') || ': Error. Sourcing RuleID. ' ||
                          substr(SQLERRM
                                ,1
                                ,100));
            END;

            IF l_sourcing_rule_id IS NULL THEN
              l_sec := '90';
              print_log(rpad('***'
                            ,30
                            ,' ') || ':750 : Start of API');

              -- calli Public API to create Sourcing RUle for Vendor/Vendor Site

              --l_Sourcing_RUle_ID := Fnd_Profile.Value('MRP_DEFAULT_ASSIGNMENT_SET');
              l_sourcing_rule_rec := mrp_sourcing_rule_pub.g_miss_sourcing_rule_rec;
              --l_sourcing_rule_rec.sourcing_rule_id := l_Sourcing_RUle_ID ;


              l_sourcing_rule_rec.sourcing_rule_name := l_vendor_num;
              -- Satish U: Creating Global Sourcing Rule :  So commenting Folling Line
              -- l_sourcing_rule_rec.Organization_Id    := p_Organization_ID;
              l_sourcing_rule_rec.planning_active    := 1; -- Active?
              l_sourcing_rule_rec.status             := 1; -- Update New record
              l_sourcing_rule_rec.sourcing_rule_type := 1; -- 1:Sourcing Rule
              l_sourcing_rule_rec.operation          := 'CREATE';
              l_sourcing_rule_rec.description        := l_vendor_num || '_' || l_vendor_name;


              l_receiving_org_tbl := mrp_sourcing_rule_pub.g_miss_receiving_org_tbl;
              --l_receiving_org_tbl(1).disable_date := SYSDATE + 90;
              l_receiving_org_tbl(1).effective_date := trunc(SYSDATE);
              l_receiving_org_tbl(1).operation := 'CREATE';
              --l_receiving_org_tbl(1).sr_receipt_id           := 3; -- sr_receipt_id;
              -- Satish U : 14-MAR-2012 : Create Global Sourcing Rule

              -- l_receiving_org_tbl(1).receipt_organization_id := p_Organization_ID;

              l_shipping_org_tbl := mrp_sourcing_rule_pub.g_miss_shipping_org_tbl;
              l_shipping_org_tbl(1).rank := 1;
              l_shipping_org_tbl(1).allocation_percent := 100; -- Allocation 100
              l_shipping_org_tbl(1).source_type := 3; -- BUY FROM
              l_shipping_org_tbl(1).vendor_id := l_vendor_id;
              l_shipping_org_tbl(1).vendor_site_id := l_vendor_site_id;
              l_shipping_org_tbl(1).receiving_org_index := 1;
              l_shipping_org_tbl(1).operation := 'CREATE';


              l_sec := '100';
              mrp_sourcing_rule_pub.process_sourcing_rule(p_api_version_number    => 1.0
                                                         ,p_init_msg_list         => fnd_api.g_false
                                                         ,p_return_values         => fnd_api.g_true
                                                         ,p_commit                => fnd_api.g_true
                                                         ,x_return_status         => x_return_status
                                                         ,x_msg_count             => x_msg_count
                                                         ,x_msg_data              => x_msg_data
                                                         ,p_sourcing_rule_rec     => l_sourcing_rule_rec
                                                         ,p_sourcing_rule_val_rec => l_sourcing_rule_val_rec
                                                         ,p_receiving_org_tbl     => l_receiving_org_tbl
                                                         ,p_receiving_org_val_tbl => l_receiving_org_val_tbl
                                                         ,p_shipping_org_tbl      => l_shipping_org_tbl
                                                         ,p_shipping_org_val_tbl  => l_shipping_org_val_tbl
                                                         ,x_sourcing_rule_rec     => lx_sourcing_rule_rec
                                                         ,x_sourcing_rule_val_rec => lx_sourcing_rule_val_rec
                                                         ,x_receiving_org_tbl     => lx_receiving_org_tbl
                                                         ,x_receiving_org_val_tbl => lx_receiving_org_val_tbl
                                                         ,x_shipping_org_tbl      => lx_shipping_org_tbl
                                                         ,x_shipping_org_val_tbl  => lx_shipping_org_val_tbl);

              l_sec := '120';
              IF x_return_status = fnd_api.g_ret_sts_success THEN
                l_sourcing_rule_success_cnt := l_sourcing_rule_success_cnt+1;
                l_sec              := '130';
                l_sourcing_rule_id := lx_sourcing_rule_rec.sourcing_rule_id;
                l_src_scs_cnt      := l_src_scs_cnt + 1;
                print_log(rpad('***'
                              ,30
                              ,' ') || ': 731:Sourcing Rule Successful, ID :' ||
                          l_sourcing_rule_id);

                COMMIT;

              ELSE
                l_sourcing_rule_error_cnt := l_sourcing_rule_error_cnt+1;
                l_sec := '140';
                IF x_msg_count > 1 THEN
                  l_sec      := '150';
                  x_msg_data := '732 : Assign_Sourcing_Rule API Returned Error...' || chr(13) ||
                                chr(10);
                  l_msg_data := ' ';

                  FOR i IN 1 .. x_msg_count LOOP
                    l_msg_data := fnd_msg_pub.get(p_msg_index => i
                                                 ,p_encoded   => 'F');
                    x_msg_data := x_msg_data || l_msg_data;
                  END LOOP;
                ELSE
                  l_sec      := '160';
                  x_msg_data := '733: Assign_Sourcing_Rule API Errored Out :' || x_msg_data;
                END IF;

                l_src_err_cnt := l_src_err_cnt + 1;
                --
                print_log(rpad('***'
                              ,30
                              ,' ') || ': ' || l_msg_data);
              END IF;
              print_log(rpad('***'
                            ,30
                            ,' ') || ':750 : End of API');
            ELSE
              l_src_scs_cnt := l_src_scs_cnt + 1;
              l_sourcing_rule_success_cnt := l_sourcing_rule_success_cnt+1;
              print_log(rpad('***'
                            ,30
                            ,' ') || ': Sourcing Rule Id found, ID :' || l_sourcing_rule_id);
            END IF;

            IF l_sourcing_rule_id IS NOT NULL THEN

              BEGIN
                l_sec               := '170';
                l_assignment_set_id := fnd_profile.value('MRP_DEFAULT_ASSIGNMENT_SET');

                l_mrp_sr_assignments_cnt := l_mrp_sr_assignments_cnt+1;

                l_sec := '180';
                INSERT INTO mrp.mrp_sr_assignments
                  (assignment_id
                  ,assignment_type
                  ,sourcing_rule_id
                  ,sourcing_rule_type
                  ,assignment_set_id
                  ,last_update_date
                  ,last_updated_by
                  ,creation_date
                  ,created_by
                  ,organization_id
                  ,inventory_item_id)
                VALUES
                  (mrp.mrp_sr_assignments_s.nextval
                  ,6
                  ,l_sourcing_rule_id
                  ,1
                  ,l_assignment_set_id
                  ,SYSDATE
                  ,fnd_global.user_id
                  ,SYSDATE
                  ,fnd_global.user_id
                  ,l_vendor_num_org_list_tbl(r).organization_id
                  ,l_vendor_num_org_list_tbl(r).inventory_item_id);

                l_sec := '190';
                COMMIT;
                l_src_asgn_cnt := l_src_asgn_cnt + 1;
                l_msg_data     := 'Record Successfully Inserted into Sourcing Rule Assignment table.';
                print_log(rpad('***'
                              ,30
                              ,' ') || ': ' || l_msg_data);
              EXCEPTION
                WHEN OTHERS THEN
                  l_src_err_asgn_cnt := l_src_err_asgn_cnt + 1;
                  x_return_status    := fnd_api.g_ret_sts_error;
                  l_msg_data         := 'Sourcing Assignment Error :' || SQLERRM;
                  print_log(rpad('***'
                                ,30
                                ,' ') || ': ' || l_msg_data);
              END;
            ELSE
              l_sec              := '200';
              l_msg_data         := 'Sourcing Assignment Skipped,Sourcing Rule ID is NULL.';
              l_src_fal_asgn_cnt := l_src_fal_asgn_cnt + 1;
              print_log(rpad('***'
                            ,30
                            ,' ') || ': ' || l_msg_data);
            END IF;

          ELSE
            l_src_fal_cnt := l_src_fal_cnt + 1;
             l_sourcing_rule_error_cnt := l_sourcing_rule_error_cnt+1;
            print_log(rpad('***'
                          ,30
                          ,' ') || ': ' || 'Vendor# is not available');
          END IF;
          x_return_status := 'S';
          l_sec           := '210';
        EXCEPTION
          WHEN OTHERS THEN
            l_prs_err_cnt   := l_prs_err_cnt + 1;
            x_return_status := fnd_api.g_ret_sts_error;
            l_msg_data      := 'When Others Error :' || SQLERRM;
            print_log(rpad('***'
                          ,30
                          ,' ') || ': ' || l_msg_data);
        END;
        l_sec := '220';

      END LOOP;

      l_sec := '225';
      print_log(rpad('-'
                    ,100
                    ,'-'));
      --
      print_log('***  BATCH COMPLETE ***');

      l_sec := '230';
      EXIT WHEN c_vendor_num_org_list%NOTFOUND;


    END LOOP;

    l_sec := '240';
    l_sec := '170';

    print_log(rpad('***'
                  ,100
                  ,'*'));

    print_log(rpad('-'
                  ,100
                  ,'-'));
    print_log(rpad('*** Total Batches processed: '
                  ,40
                  ,' ') || ': ' || l_batch_cnt);
    print_log(rpad('*** Total Recs processed: '
                  ,40
                  ,' ') || ': ' || l_prs_cnt);
    print_log(rpad('*** Total Recs Process Unknown Errors: '
                  ,40
                  ,' ') || ': ' || l_prs_err_cnt);
    print_log(rpad('*** Total Recs Source Rules Found/Created: '
                  ,40
                  ,' ') || ': ' || l_src_scs_cnt);
    print_log(rpad('*** Total Rec Source Rule Failures: '
                  ,40
                  ,' ') || ': ' || l_src_fal_cnt);
    print_log(rpad('*** Total Rec Source Rule API Errors: '
                  ,40
                  ,' ') || ': ' || l_src_err_cnt);
    print_log(rpad('*** Total Rec Source Rule Assignments: '
                  ,40
                  ,' ') || ': ' || l_src_asgn_cnt);
    print_log(rpad('*** Total Rec Source Rule Assignments Failed: '
                  ,40
                  ,' ') || ': ' || l_src_fal_asgn_cnt);
    print_log(rpad('*** Total Rec Source Rule Assignments Errors: '
                  ,40
                  ,' ') || ': ' || l_src_err_asgn_cnt);

    print_log(rpad('-'
                  ,100
                  ,'-'));
    l_sec := '180';
    print_log(' *** ITEM ORG, Sourcing Rule Update  ENDED ***');
    print_log(rpad('***'
                  ,100
                  ,'*'));
    l_sec := '250';

     print_out('****l_sourcing_rule_success_cnt: '||l_sourcing_rule_success_cnt );
     print_out('****l_sourcing_rule_error_cnt: '||l_sourcing_rule_error_cnt );
     print_out('****l_mrp_sr_assignments_cnt    : '||l_mrp_sr_assignments_cnt );

  EXCEPTION
    WHEN OTHERS THEN
      print_log('*** Unknown Error in Assign_Sourcing_Rule at Sec: ' || l_sec || '-' ||
                substr(SQLERRM
                      ,1
                      ,100));
      print_log(' *** ITEM ORG, Sourcing Rule Update  ABORTED ***');
      print_log(rpad('***'
                    ,100
                    ,'*'));
      RAISE;
  END assign_sourcing_rule;

  PROCEDURE assign_category (errbuf                OUT VARCHAR2
                             ,retcode              OUT VARCHAR2
                             ,p_item_creation_date  IN DATE
                             ,p_item1               IN VARCHAR2
                             ,p_item2               IN VARCHAR2
                             ,p_item3               IN VARCHAR2)
  IS
      ln_api_version     NUMBER := 1.0;
      lc_return_status   VARCHAR2 (10);
      ln_msg_count       NUMBER;
      lc_msg_data        VARCHAR2 (4000) := NULL;
      ln_error_code      NUMBER;
      l_cat_set_id       NUMBER;
      l_structure_id     NUMBER;
      l_old_cat_id       NUMBER;
      l_cat_id           NUMBER;
      l_error_code       VARCHAR2 (10);
      l_err_msg          VARCHAR2 (2000);
      l_error_mesg       VARCHAR2 (4000);
      l_err_message      varchar2 (4000);
      l_category_values  varchar2(200);
      l_cate_set_name    varchar2(200):='Sales Velocity';
      l_ahh_part_number       xxwc.xxwc_item_master_ref.icsp_prod%TYPE;
      l_error_msg        varchar2 (4000);
      l_sec              varchar2(200);
      l_velocityclassification varchar2(200);
      l_salesvelocity    VARCHAR2 (10);
      l_inv_item_cat_pub_success_cnt NUMBER :=0;
      l_inv_item_cat_pub_err_cnt NUMBER :=0;
      l_ahh_warehouse_number  xxwc.xxwc_ap_branch_list_stg_tbl.ahh_warehouse_number%TYPE;

      CURSOR c_cat_assignment IS
       SELECT  UPPER(msi.segment1) item
             ,ood.organization_code
             ,msi.inventory_item_id
             ,msi.organization_id
             ,msi.source_type
             ,msi.source_organization_id
             ,msi.item_type
        FROM apps.mtl_system_items_b msi,
             apps.org_organization_definitions ood,
             apps.mtl_item_categories_v mic
       WHERE msi.organization_id = ood.organization_id
         AND ood.organization_code IN ('189',  --1.1
                                     '188',
                                     '187',
                                     '186',
                                     '149',
                                     '148',
                                     '513',
                                     '514',
                                     '516',
                                     '517',
                                     '515',
                                     '521',
                                     '518',
                                     '519',
                                     '509',
                                     '522',
                                     '510',
                                     '511',
                                     '512',
                                     '523')
       AND msi.inventory_item_id = mic.inventory_item_id
       AND msi.organization_id = mic.organization_id
       AND NVL(mic.CATEGORY_CONCAT_SEGS,'000') <> 'PR.PRMO'
       AND mic.STRUCTURE_ID = 101
       AND TRUNC(msi.creation_date) <= TRUNC (p_item_creation_date)
       AND msi.created_by NOT IN (15985, 50485, 15986)
       AND msi.segment1 IN
             (p_item1
             ,p_item2
             ,p_item3
             ,(CASE WHEN p_item1 IS NULL AND p_item2 IS NULL AND p_item3 IS NULL THEN msi.segment1 ELSE NULL END))
       ORDER BY msi.segment1;
BEGIN
   FOR r_cat_assignment IN c_cat_assignment LOOP
    BEGIN
       print_log(' ');
       print_log('*****************************************************************************************************************');
       print_log('****Processing item category assignment rec_items: '|| r_cat_assignment.item || ' rec_items.organization_code: '||r_cat_assignment.organization_code||' rec_items.organization_id: '||r_cat_assignment.organization_id);
       print_log('*****************************************************************************************************************');

       l_sec := '10';
       print_log('Category Assignment for item :'||r_cat_assignment.item);

       -- Category set id deriving:
       BEGIN
         SELECT category_set_id
           INTO l_cat_set_id
           from mtl_category_sets
          WHERE category_set_name = 'Sales Velocity';
       EXCEPTION
       WHEN OTHERS
         then
          l_cat_set_id :=null;
       END;

       -----------------------------
       --Derive the Old Category ID
       -----------------------------
       BEGIN
         SELECT category_id
           INTO l_old_cat_id
           FROM mtl_item_categories
          WHERE inventory_item_id = r_cat_assignment.inventory_item_id
            AND category_set_id = l_cat_set_id
            AND organization_id=r_cat_assignment.organization_id;
       EXCEPTION
         WHEN OTHERS
         THEN
            l_old_cat_id := NULL;
       END;

       print_log('Category Assignment for item :'||r_cat_assignment.item);
       l_sec := '20';

       BEGIN
         SELECT UPPER(icsp_prod)
           INTO l_ahh_part_number
           FROM xxwc.xxwc_item_master_ref
          WHERE UPPER(item) = r_cat_assignment.item
            AND ROWNUM=1;
       EXCEPTION
	   WHEN NO_DATA_FOUND THEN
          l_error_msg := 'Item is not found: ' ||r_cat_assignment.organization_code || '.' || r_cat_assignment.item;
       WHEN OTHERS THEN
          l_error_msg := SUBSTR(l_sec||'=>'||SQLERRM,1,2000);
       END;

       l_sec := '40';
       BEGIN
          SELECT UPPER(ahh_warehouse_number)
            INTO l_ahh_warehouse_number
            FROM xxwc.xxwc_ap_branch_list_stg_tbl
           WHERE UPPER(oracle_branch_number) = UPPER(r_cat_assignment.organization_code)
             AND process_status = 'Y'
             AND ahh_warehouse_number LIKE 'M%'
             AND rownum=1;
       EXCEPTION
	   WHEN NO_DATA_FOUND THEN
          l_error_msg := 'Item is not found: ' ||r_cat_assignment.organization_code || '.' || r_cat_assignment.item;
          -- raise l_val_exception;
       WHEN OTHERS THEN
          l_error_msg := SUBSTR(l_sec||'=>'||SQLERRM,1,2000);
               --raise l_val_exception;
       END;

       print_log('****l_ahh_part_number: '|| l_ahh_part_number||' l_ahh_warehouse_number: '|| l_ahh_warehouse_number);

       -- Category set id deriving:
       BEGIN
            SELECT velocityclassification
              INTO l_velocityclassification
              FROM xxwc.xxwc_invitem_stg xis
             WHERE UPPER (xis.ahh_partnumber) = l_ahh_part_number
               AND UPPER (xis.organization_code) = l_ahh_warehouse_number
               AND ROWNUM =1;
       EXCEPTION
       WHEN OTHERS THEN
          l_velocityclassification :=null;
       END;

       -- Rentals Default
       IF substr(r_cat_assignment.item
                         ,1
                         ,1) = 'R' THEN
                  --Rental
          l_salesvelocity := 'N';
       ELSE
          l_salesvelocity := CASE r_cat_assignment.item_type
                                       WHEN NULL THEN
                                        NULL
                                       ELSE
                                        XXWC_AHH_ITEM_CONV_PKG.derive_salesvelocity(l_velocityclassification
                                                            ,r_cat_assignment.item_type)
                                     END;
       END IF;

       ------------------------
       --Derive the Category ID
       ------------------------
       l_cat_id := XXWC_AHH_ITEM_CONV_PKG.derive_category_id(l_salesvelocity || '.'
                                                            ,l_cate_set_name --'Sales Velocity'
                                                            );




       print_log('l_cat_id : '||l_cat_id||' l_old_cat_id: '||l_old_cat_id);
       print_log('l_category_values : '||l_category_values);

       IF l_old_cat_id IS NULL AND l_cat_id IS NOT NULL THEN
          inv_item_category_pub.create_Category_Assignment (
            p_api_version         => ln_api_version,
            p_init_msg_list       => FND_API.G_TRUE,
            p_commit              => FND_API.G_TRUE,
            p_category_id         => l_cat_id,
            p_category_set_id     => l_cat_set_id,
            p_inventory_item_id   => r_cat_assignment.inventory_item_id,
            p_organization_id     => r_cat_assignment.organization_id,
            x_return_status       => lc_return_status,
            x_errorcode           => ln_error_code,
            x_msg_count           => ln_msg_count,
            x_msg_data            => lc_msg_data);

            IF (lc_return_status = 'S') THEN
               print_log('inv_item_category_pub.create_Category_Assignment lc_return_status :'||lc_return_status);
               print_log('inv_item_category_pub.create_Category_Assignment lc_msg_data :'||lc_msg_data);
               l_inv_item_cat_pub_success_cnt := l_inv_item_cat_pub_success_cnt +1;
            ELSE
               print_log('inv_item_category_pub.create_Category_Assignment Error message :'||lc_msg_data);
               l_inv_item_cat_pub_err_cnt := l_inv_item_cat_pub_err_cnt +1;
            END IF;

       END IF;

       IF l_old_cat_id IS NOT NULL THEN
          inv_item_category_pub.update_Category_Assignment (
            p_api_version         => ln_api_version,
            p_init_msg_list       => FND_API.G_TRUE,
            p_commit              => FND_API.G_TRUE,
            p_category_id         => l_cat_id,
            p_old_category_id     => l_old_cat_id,
            p_category_set_id     => l_cat_set_id,
            p_inventory_item_id   => r_cat_assignment.inventory_item_id,
            p_organization_id     => r_cat_assignment.organization_id,
            x_return_status       => lc_return_status,
            x_errorcode           => ln_error_code,
            X_MSG_COUNT           => LN_MSG_COUNT,
            x_msg_data            => lc_msg_data);
        END IF;

        IF (lc_return_status = 'S') THEN
           print_log('inv_item_category_pub.update_Category_Assignment lc_return_status :'||lc_return_status);
           print_log('inv_item_category_pub.update_Category_Assignment lc_msg_data :'||lc_msg_data);
           l_inv_item_cat_pub_success_cnt := l_inv_item_cat_pub_success_cnt +1;
        ELSE
           print_log('inv_item_category_pub.update_Category_Assignment Error message :'||lc_msg_data);
           l_inv_item_cat_pub_err_cnt := l_inv_item_cat_pub_err_cnt +1;
        END IF;
    EXCEPTION
    WHEN OTHERS THEN
        print_log('Error Occured(Category Assignment) :'||sqlerrm);
    END;
   END LOOP;

   print_out('****l_inv_item_cat_pub_success_cnt: '||l_inv_item_cat_pub_success_cnt );
   print_out('****l_inv_item_cat_pub_err_cnt    : '||l_inv_item_cat_pub_err_cnt );


 EXCEPTION
 WHEN OTHERS THEN
      retcode := 2;
      errbuf  := l_sec || '-' || SQLERRM;
      print_debug('Unknown Error - sec: ' || l_sec || '-' || SQLERRM);
END;

PROCEDURE process_item_locators(  errbuf  OUT VARCHAR2
                                 ,retcode OUT VARCHAR2
                                 ,p_item_creation_date  IN DATE
                                 ,p_item1 IN VARCHAR2
                                 ,p_item2 IN VARCHAR2
                                 ,p_item3 IN VARCHAR2) IS
    l_return_status         VARCHAR2(1) := fnd_api.g_ret_sts_success;
    l_msg_count             NUMBER;
    l_msg_data              VARCHAR2(4000);
    --l_subinventory_code     VARCHAR2(20);
    l_inventory_location_id NUMBER;
    l_locator_exists        VARCHAR2(1);
    l_itemreturn_status     VARCHAR2(1) := fnd_api.g_ret_sts_success;
    l_itemmsg_count         NUMBER DEFAULT 0;
    l_itemmsg_data          VARCHAR2(4000) DEFAULT NULL;
    l_msg_index_out         NUMBER;
    l_error_msg             VARCHAR2(2000) := NULL;
    l_err_flag              VARCHAR2(1) := 'N';
    l_err_cnt               NUMBER := 0;
    l_scs_cnt               NUMBER := 0;
    l_pcs_cnt               NUMBER := 0;
    l_sd_cnt                NUMBER := 0;
    l_sub_inv_count         NUMBER := 0;
    l_item_location         VARCHAR2(4000) := NULL;
    l_sub_inv_assign_count  NUMBER := 0;
    l_create_loc_item_tie_err_cnt NUMBER :=0;
    l_create_loc_item_tie_suc_cnt NUMBER:=0;
    l_create_locator_success_count NUMBER:=0;
    l_create_locator_error_count NUMBER:=0;

    CURSOR c_locator_stg IS
       SELECT  UPPER(msi.segment1) item
             ,ood.organization_code
             ,msi.inventory_item_id
             ,msi.organization_id
             ,msi.source_type
             ,msi.source_organization_id
             ,msi.item_type
        FROM apps.mtl_system_items_b msi,
             apps.org_organization_definitions ood,
             apps.mtl_item_categories_v mic
       WHERE msi.organization_id = ood.organization_id
         AND ood.organization_code IN ('189', --1.1
                                     '188',
                                     '187',
                                     '186',
                                     '149',
                                     '148',
                                     '513',
                                     '514',
                                     '516',
                                     '517',
                                     '515',
                                     '521',
                                     '518',
                                     '519',
                                     '509',
                                     '522',
                                     '510',
                                     '511',
                                     '512',
                                     '523')
       AND msi.inventory_item_id = mic.inventory_item_id
       AND msi.organization_id = mic.organization_id
       AND NVL(mic.CATEGORY_CONCAT_SEGS,'000') <> 'PR.PRMO'
       AND mic.STRUCTURE_ID = 101
       AND TRUNC(msi.creation_date) <= TRUNC (p_item_creation_date)
       AND msi.created_by NOT IN (15985, 50485, 15986)
       AND msi.segment1 IN
             (p_item1
             ,p_item2
             ,p_item3
             ,(CASE WHEN p_item1 IS NULL AND p_item2 IS NULL AND p_item3 IS NULL THEN msi.segment1 ELSE NULL END))
       ORDER BY msi.segment1;

    -- Select Items Locators
    CURSOR c_sel_locs(p_inventory_item_id IN NUMBER
                     ,p_organization_id   IN NUMBER )
    IS
      SELECT loc.*
            ,row_number() over(PARTITION BY organization_code, segment1 ORDER BY sort) bin_number
        FROM (SELECT mp.organization_code organization_code
                    ,segment1 segment1
                    ,stg.ahh_partnumber ahh_partnumber
                    ,br.ahh_warehouse_number ahh_organization_code
                    ,TRIM(itemlocation1) || '.' itemlocation
                    ,msib.inventory_item_id
                    ,msib.organization_id
                    ,SYSDATE
                    --,p_processed_by
                   -- ,g_new_psts
                  --  ,g_new_sts
                    ,1 sort
                FROM xxwc.xxwc_invitem_stg            stg
                    ,xxwc.xxwc_item_master_ref        refe
                    ,xxwc.xxwc_ap_branch_list_stg_tbl br
                    ,mtl_parameters                   mp
                    ,mtl_system_items_b               msib
               WHERE upper(ahh_partnumber) = upper(refe.icsp_prod) -- Version 4.0
                 AND upper(TRIM(stg.organization_code)) = upper(br.ahh_warehouse_number)
                 AND br.oracle_branch_number = mp.organization_code
                 AND br.process_status = 'Y'
                 AND refe.item = msib.segment1
                 AND mp.organization_id = msib.organization_id
                 AND TRIM(itemlocation1) IS NOT NULL
                 AND msib.inventory_item_id = p_inventory_item_id
                 AND msib.organization_id   = p_organization_id
              UNION
              SELECT mp.organization_code organization_code
                    ,segment1 segment1
                    ,stg.ahh_partnumber ahh_partnumber
                    ,br.ahh_warehouse_number ahh_organization_code
                    ,TRIM(itemlocation2) || '.' itemlocation
                    ,msib.inventory_item_id
                    ,msib.organization_id
                    ,SYSDATE
                    --,p_processed_by
                    --,g_new_psts
                    --,g_new_sts
                    ,2 sort
                FROM xxwc.xxwc_invitem_stg            stg
                    ,xxwc.xxwc_item_master_ref        refe
                    ,xxwc.xxwc_ap_branch_list_stg_tbl br
                    ,mtl_parameters                   mp
                    ,mtl_system_items_b               msib
               WHERE upper(ahh_partnumber) = upper(refe.icsp_prod) -- Version 4.0
                 AND upper(TRIM(stg.organization_code)) = upper(br.ahh_warehouse_number)
                 AND br.oracle_branch_number = mp.organization_code
                 AND br.process_status = 'Y'
                 AND refe.item = msib.segment1
                 AND mp.organization_id = msib.organization_id
                 AND TRIM(itemlocation2) IS NOT NULL
                 AND msib.inventory_item_id = p_inventory_item_id
                 AND msib.organization_id   = p_organization_id
              UNION
              SELECT mp.organization_code organization_code
                    ,segment1 segment1
                    ,stg.ahh_partnumber ahh_partnumber
                    ,br.ahh_warehouse_number ahh_organization_code
                    ,TRIM(itemlocation3) || '.' itemlocation
                    ,msib.inventory_item_id
                    ,msib.organization_id
                    ,SYSDATE
                   -- ,p_processed_by
                   -- ,g_new_psts
                   -- ,g_new_sts
                    ,3 sort
                FROM xxwc.xxwc_invitem_stg            stg
                    ,xxwc.xxwc_item_master_ref        refe
                    ,xxwc.xxwc_ap_branch_list_stg_tbl br
                    ,mtl_parameters                   mp
                    ,mtl_system_items_b               msib
               WHERE upper(ahh_partnumber) = upper(refe.icsp_prod) -- Version 4.0
                 AND upper(TRIM(stg.organization_code)) = upper(br.ahh_warehouse_number)
                 AND br.oracle_branch_number = mp.organization_code
                 AND br.process_status = 'Y'
                 AND refe.item = msib.segment1
                 AND mp.organization_id = msib.organization_id
                 AND TRIM(itemlocation3) IS NOT NULL
                 AND msib.inventory_item_id = p_inventory_item_id
                 AND msib.organization_id   = p_organization_id
              UNION
              SELECT mp.organization_code organization_code
                    ,segment1 segment1
                    ,stg.ahh_partnumber ahh_partnumber
                    ,br.ahh_warehouse_number ahh_organization_code
                    ,TRIM(itemlocation3) || '.' itemlocation
                    ,msib.inventory_item_id
                    ,msib.organization_id
                    ,SYSDATE
                   -- ,p_processed_by
                    --,g_new_psts
                    --,g_new_sts
                    ,4 sort
                FROM xxwc.xxwc_invitem_stg            stg
                    ,xxwc.xxwc_item_master_ref        refe
                    ,xxwc.xxwc_ap_branch_list_stg_tbl br
                    ,mtl_parameters                   mp
                    ,mtl_system_items_b               msib
               WHERE upper(ahh_partnumber) = upper(refe.icsp_prod) -- Version 4.0
                 AND upper(TRIM(stg.organization_code)) = upper(br.ahh_warehouse_number)
                 AND br.oracle_branch_number = mp.organization_code
                 AND br.process_status = 'Y'
                 AND refe.item = msib.segment1
                 AND mp.organization_id = msib.organization_id
                 AND TRIM(itemlocation4) IS NOT NULL
                 AND msib.inventory_item_id = p_inventory_item_id
                 AND msib.organization_id   = p_organization_id
              UNION
              SELECT mp.organization_code organization_code
                    ,segment1 segment1
                    ,stg.ahh_partnumber ahh_partnumber
                    ,br.ahh_warehouse_number ahh_organization_code
                    ,TRIM(itemlocation3) || '.' itemlocation
                    ,msib.inventory_item_id
                    ,msib.organization_id
                    ,SYSDATE
                   -- ,p_processed_by
                   -- ,g_new_psts
                    --,g_new_sts
                    ,5 sort
                FROM xxwc.xxwc_invitem_stg            stg
                    ,xxwc.xxwc_item_master_ref        refe
                    ,xxwc.xxwc_ap_branch_list_stg_tbl br
                    ,mtl_parameters                   mp
                    ,mtl_system_items_b               msib
               WHERE upper(ahh_partnumber) = upper(refe.icsp_prod) -- Version 4.0
                 AND upper(TRIM(stg.organization_code)) = upper(br.ahh_warehouse_number)
                 AND br.oracle_branch_number = mp.organization_code
                 AND br.process_status = 'Y'
                 AND refe.item = msib.segment1
                 AND mp.organization_id = msib.organization_id
                 AND TRIM(itemlocation5) IS NOT NULL
                 AND msib.inventory_item_id = p_inventory_item_id
                 AND msib.organization_id   = p_organization_id) loc
         WHERE 1=1;

    l_sec            VARCHAR2(10);
    l_log_msg        VARCHAR2(2000);

  BEGIN
    FOR r_locator_stg IN c_locator_stg LOOP
       FOR r_sel_locs IN c_sel_locs( p_inventory_item_id => r_locator_stg.inventory_item_id
                                    ,p_organization_id   => r_locator_stg.organization_id
                                    ) LOOP
          BEGIN
             l_error_msg                 := NULL;
             l_err_flag                  := NULL;
             l_inventory_location_id     := NULL;
             l_locator_exists            := NULL;
             l_msg_data                  := NULL;
             l_msg_count                 := NULL;
             l_return_status             := NULL;
             l_log_msg                   := NULL;
             l_pcs_cnt                   := l_pcs_cnt + 1;
             l_item_location             := r_sel_locs.bin_number || '-' ||r_sel_locs.itemlocation;

             print_log(rpad('-'
                         ,100
                         ,'-'));
             print_debug('Item : ' || r_locator_stg.item || ', Org: ' ||
                    r_locator_stg.organization_code || ', Locator: ' ||
                    l_item_location || '....');

             inv_loc_wms_pub.create_locator(x_return_status            => l_return_status
                                           ,x_msg_count                => l_msg_count
                                           ,x_msg_data                 => l_msg_data
                                           ,x_inventory_location_id    => l_inventory_location_id
                                           ,x_locator_exists           => l_locator_exists
                                           ,p_organization_id          => NULL
                                           ,p_organization_code        => r_locator_stg.organization_code
                                           ,p_concatenated_segments    => l_item_location
                                           ,p_description              => ''
                                           ,p_inventory_location_type  => NULL
                                           ,p_picking_order            => NULL
                                           ,p_location_maximum_units   => NULL
                                           ,p_subinventory_code        => 'General'
                                           ,p_location_weight_uom_code => NULL
                                           ,p_max_weight               => NULL
                                           ,p_volume_uom_code          => NULL
                                           ,p_max_cubic_area           => NULL
                                           ,p_x_coordinate             => NULL
                                           ,p_y_coordinate             => NULL
                                           ,p_z_coordinate             => NULL
                                           ,p_physical_location_id     => NULL
                                           ,p_pick_uom_code            => NULL
                                           ,p_dimension_uom_code       => NULL
                                           ,p_length                   => NULL
                                           ,p_width                    => NULL
                                           ,p_height                   => NULL
                                           ,p_status_id                => 1
                                           ,p_dropping_order           => NULL
                                           ,p_attribute_category       => ''
                                           ,p_attribute1               => ''
                                           ,p_attribute2               => ''
                                           ,p_attribute3               => ''
                                           ,p_attribute4               => ''
                                           ,p_attribute5               => ''
                                           ,p_attribute6               => ''
                                           ,p_attribute7               => ''
                                           ,p_attribute8               => ''
                                           ,p_attribute9               => ''
                                           ,p_attribute10              => ''
                                           ,p_attribute11              => ''
                                           ,p_attribute12              => ''
                                           ,p_attribute13              => ''
                                           ,p_attribute14              => ''
                                           ,p_attribute15              => ''
                                           ,p_alias                    => '');

             IF (l_return_status = 'S') THEN
                l_log_msg := l_log_msg || '| Locator Created';
                --commit;
                print_debug('Locator Id:' || l_inventory_location_id);
                print_debug('Locator exists:' || l_locator_exists);
                l_create_locator_success_count := l_create_locator_success_count+1;
             ELSE
                l_create_locator_error_count := l_create_locator_error_count+1;
                FOR l_index IN 1 .. l_msg_count LOOP
                   l_msg_data := l_msg_data || fnd_msg_pub.get(l_index
                                                       ,'F');
                END LOOP;
                l_error_msg := l_error_msg || '-' || substr(l_msg_data
                                                     ,1
                                                     ,100);
                l_err_flag  := 'Y';

                l_log_msg := l_log_msg || '| Locator Failed - ' || l_error_msg;
                print_debug(' Locator Failed: ' || l_error_msg);
             END IF;

             l_itemreturn_status := NULL;
             l_itemmsg_count     := NULL;
             l_itemmsg_data      := NULL;

             inv_loc_wms_pub.create_loc_item_tie(x_return_status         => l_itemreturn_status
                                                ,x_msg_count             => l_itemmsg_count
                                                ,x_msg_data              => l_itemmsg_data
                                                ,p_inventory_item_id     => NULL
                                                ,p_item                  => r_locator_stg.item
                                                ,p_organization_id       => NULL
                                                ,p_organization_code     => r_locator_stg.organization_code
                                                ,p_subinventory_code     => 'General'
                                                ,p_inventory_location_id => l_inventory_location_id
                                                ,p_locator               => l_item_location
                                                ,p_status_id             => 1);


             IF l_itemreturn_status = 'S' THEN
               l_create_loc_item_tie_suc_cnt := l_create_loc_item_tie_suc_cnt+1;
               l_log_msg := l_log_msg || '| Locator Item Tied ';
               --commit;
               print_debug('Locator Item Tied ');
             ELSE
               l_create_loc_item_tie_err_cnt := l_create_loc_item_tie_err_cnt+1;
                FOR ln_index IN 1 .. l_itemmsg_count LOOP
                   oe_msg_pub.get(p_msg_index     => ln_index
                                 ,p_encoded       => fnd_api.g_false
                                 ,p_data          => l_itemmsg_data
                                 ,p_msg_index_out => l_msg_index_out);
                END LOOP;
                l_error_msg := l_error_msg || '-' || substr(l_itemmsg_data
                                                           ,1
                                                           ,100);
                l_log_msg  := l_log_msg || '| Locator Tie Failed - ' || l_error_msg;
                print_debug(' Locator Tie Failed: ' || l_error_msg);
             END IF;
             COMMIT;
             l_log_msg := l_log_msg || '| *Complete*';
             print_debug('*Complete* ');
           EXCEPTION
           WHEN OTHERS THEN
              l_error_msg := l_error_msg || '- Sec:' || l_sec || '-' ||
                         substr(SQLERRM
                               ,1
                               ,100);
              l_log_msg   := l_log_msg || '| Unknown Err - ' || l_error_msg;
              print_debug('Unknown Error ' || l_error_msg);
           END;
       END LOOP; --r_sel_locs

       SELECT count(1)
         INTO l_sub_inv_count
         FROM mtl_item_sub_defaults d
        WHERE d.inventory_item_id = r_locator_stg.inventory_item_id
         AND d.organization_id = r_locator_stg.organization_id
         AND d.default_type = 2;
      -- FOR c1_rec IN c_sub_defaults( p_inventory_item_id => r_locator_stg.inventory_item_id
                                    -- ,p_organization_id => r_locator_stg.organization_id
                                   -- ) LOOP
       IF l_sub_inv_count >  0 THEN
         print_debug(' Sub Default Already Exists: InvItemID,Orgid' ||
                      r_locator_stg.inventory_item_id || ',' || r_locator_stg.organization_id);
         l_sub_inv_assign_count := l_sub_inv_assign_count+1;
       ELSE
         print_debug('Sub Defaults Insert : Started');
         l_sub_inv_assign_count := l_sub_inv_assign_count+1;
         BEGIN
           INSERT INTO mtl_item_sub_defaults
             ( inventory_item_id
              ,organization_id
              ,subinventory_code
              ,default_type
              ,last_update_date
              ,last_updated_by
              ,creation_date
              ,created_by
              ,last_update_login)
           VALUES
             (r_locator_stg.inventory_item_id
             ,r_locator_stg.organization_id
             ,'General'
             ,2
             ,SYSDATE
             ,fnd_global.user_id
             ,SYSDATE
             ,fnd_global.user_id
             ,-1);
           l_sd_cnt := l_sd_cnt + 1;

         EXCEPTION
            WHEN dup_val_on_index THEN
               print_debug('                    : Sub Default Already Exists: InvItemID,Orgid' ||
                      r_locator_stg.inventory_item_id || ',' || r_locator_stg.organization_id);

            WHEN OTHERS THEN
               print_debug('                    : Sub Default unknown Err: InvItemID,Orgid' ||
                      r_locator_stg.inventory_item_id || ',' || r_locator_stg.organization_id || '-' || SQLERRM);
         END;
      END IF; --c_sub_defaults

    END LOOP; --r_locator_stg

    print_debug('Locators creation Complete');

    print_log(rpad('*** Total Records Processed: '
                  ,40
                  ,' ') || ': ' || l_pcs_cnt);
    print_log(rpad('*** Total Records Errored: '
                  ,40
                  ,' ') || ': ' || l_err_cnt);
    print_log(rpad('*** Total Records Succesful: '
                  ,40
                  ,' ') || ': ' || l_scs_cnt);

    COMMIT;

    --print_debug('Sub Defaults Insert : Finished. Count: ' || l_sd_cnt);
    print_out('****l_sub_inv_assign_count: '||l_sub_inv_assign_count );

    print_out('****l_create_loc_item_tie_suc_cnt: '||l_create_loc_item_tie_suc_cnt );
    print_out('****l_create_loc_item_tie_err_cnt: '||l_create_loc_item_tie_err_cnt );

    print_out('****l_create_locator_success_count: '||l_create_locator_success_count );
    print_out('****l_create_locator_error_count: '||l_create_locator_error_count );

  EXCEPTION
    WHEN OTHERS THEN
      retcode := 2;
      errbuf  := l_sec || '-' || SQLERRM;
      print_debug('Unknown Error - sec: ' || l_sec || '-' || SQLERRM);
  END process_item_locators;


  ---
  -- PROCEDURE: main for XXWC AHH INV Existing Item attribute update program
  ---
  PROCEDURE main (errbuf                OUT VARCHAR2
                 ,retcode               OUT NUMBER
                 ,p_item_creation_date  IN VARCHAR2
                 ,p_item1               IN VARCHAR2
                 ,p_item2               IN VARCHAR2
                 ,p_item3               IN VARCHAR
                 ,p_update_item_att     IN VARCHAR2
                 ,p_update_item_att_cat IN VARCHAR2
                 ,p_assign_source_rule  IN VARCHAR2
                 ,p_assign_Locators     IN VARCHAR2
) IS
    l_sec VARCHAR2(10);

    -- Items for assignment
    CURSOR c_existing_items (p_item_creation_date IN DATE ) IS
      SELECT  UPPER(msi.segment1) item
             ,ood.organization_code
             ,msi.inventory_item_id
             ,msi.organization_id
             ,msi.source_type
             ,msi.source_organization_id
             ,msi.item_type
        FROM apps.mtl_system_items_b msi,
             apps.org_organization_definitions ood,
             apps.mtl_item_categories_v mic
       WHERE msi.organization_id = ood.organization_id
         AND ood.organization_code IN ('189',  --1.1
                                     '188',
                                     '187',
                                     '186',
                                     '149',
                                     '148',
                                     '513',
                                     '514',
                                     '516',
                                     '517',
                                     '515',
                                     '521',
                                     '518',
                                     '519',
                                     '509',
                                     '522',
                                     '510',
                                     '511',
                                     '512',
                                     '523')
       AND msi.inventory_item_id = mic.inventory_item_id
       AND msi.organization_id = mic.organization_id
       AND NVL(mic.CATEGORY_CONCAT_SEGS,'000') <> 'PR.PRMO'
       AND mic.STRUCTURE_ID = 101
       AND TRUNC(msi.creation_date) <= TRUNC (p_item_creation_date)
       AND msi.created_by NOT IN (15985, 50485, 15986)
       AND msi.segment1 IN
             (p_item1
             ,p_item2
             ,p_item3
             ,(CASE WHEN p_item1 IS NULL AND p_item2 IS NULL AND p_item3 IS NULL THEN msi.segment1 ELSE NULL END))
       ORDER BY msi.segment1;


     -- Items attribute for assignment
    CURSOR c_ahh_items_attribute( p_ahh_partnumber    VARCHAR2
                                 ,p_organization_code VARCHAR2
                                 ) IS
      SELECT UPPER(xis.ahh_partnumber) ahh_partnumber
            ,UPPER(xis.organization_code) organization_code
            ,xis.avgcost
            ,xis.sourcing_from
            ,xis.leadtime
            ,xis.minmaxmin
            ,xis.minmaxmax
            ,UPPER(xis.org_item_status_code) org_item_status_code
            ,icsw_minthresexdt
            ,threshold
            ,velocityclassification
            ,xis.inv_org
        FROM xxwc.xxwc_invitem_stg xis
       WHERE UPPER (xis.ahh_partnumber) = p_ahh_partnumber
         AND UPPER (xis.organization_code) = p_organization_code
         AND ROWNUM =1;


     l_cnt                   NUMBER;
     l_ahh_part_number       xxwc.xxwc_item_master_ref.icsp_prod%TYPE;
     l_ahh_warehouse_number  xxwc.xxwc_ap_branch_list_stg_tbl.ahh_warehouse_number%TYPE;
     l_error_msg             VARCHAR2(4000);
     l_val_exception         EXCEPTION;
     l_item_tbl_typ          ego_item_pub.item_tbl_type;
     x_item_table            ego_item_pub.item_tbl_type;
     x_inventory_item_id     mtl_system_items_b.inventory_item_id%TYPE;
     x_organization_id       mtl_system_items_b.organization_id%TYPE;
     x_return_status         VARCHAR2 (1);
     x_msg_count             NUMBER (10);
     x_msg_data              VARCHAR2 (1000);
     x_message_list          error_handler.error_tbl_type;
     l_item_creation_date    DATE;
     x_cost_of_sales_account NUMBER;
     x_sales_account         NUMBER;
     x_expense_account       NUMBER;
     l_ego_item_pub_success_cnt NUMBER :=0;
     l_ego_item_pub_error_cnt   NUMBER :=0;

     FUNCTION set_gbl_params(p_param VARCHAR2) RETURN BOOLEAN IS
    BEGIN
      RETURN(CASE nvl(p_param
                ,'N') WHEN 'Y' THEN TRUE ELSE FALSE END);
    EXCEPTION
      WHEN OTHERS THEN
        RETURN FALSE;
    END;
    --
  BEGIN
   l_sec := '10';
   fnd_global.apps_initialize (user_id        => 50485,
                               resp_id        => 50983,
                               resp_appl_id   => 201);

   mo_global.set_policy_context ('S', 162);

   mo_global.init('INV');

   IF p_item_creation_date IS NOT NULL THEN
      l_item_creation_date := TO_DATE (p_item_creation_date, 'YYYY/MM/DD HH24:MI:SS');
   END IF;

   print_log('****p_item1 : '|| p_item1);
   print_log('****p_item2 : '|| p_item2);
   print_log('****p_item3 : '|| p_item3);
   print_log('****p_update_item_att : '|| p_update_item_att);
   print_log('****p_update_item_att_cat : '|| p_update_item_att_cat);
   print_log('****p_assign_source_rule : '|| p_assign_source_rule);
   print_log('****p_assign_Locators : '|| p_assign_Locators);

   l_sec := '20';

   IF p_update_item_att = 'Y' THEN
      FOR rec_items IN c_existing_items (p_item_creation_date => l_item_creation_date ) LOOP
         BEGIN --to continue the loop if any record errors
            print_log(' ');
            print_log('*****************************************************************************************************************');
            print_log('****Processing item attribute update rec_items: '|| rec_items.item || ' rec_items.organization_code: '||rec_items.organization_code||' rec_items.organization_id: '||rec_items.organization_id);
            print_log('*****************************************************************************************************************');

            l_sec := '30';
            BEGIN
              SELECT UPPER(icsp_prod)
                INTO l_ahh_part_number
                FROM xxwc.xxwc_item_master_ref
               WHERE UPPER(item) = rec_items.item
                 AND ROWNUM=1;
            EXCEPTION
		    WHEN NO_DATA_FOUND THEN
               l_error_msg := 'Item is not found' ||rec_items.organization_code || '.' || rec_items.item;
               raise l_val_exception;
            WHEN OTHERS THEN
               l_error_msg := SUBSTR(l_sec||'=>'||SQLERRM,1,2000);
               raise l_val_exception;
            END;

            l_sec := '40';
            BEGIN
               SELECT UPPER(ahh_warehouse_number)
                 INTO l_ahh_warehouse_number
                 FROM xxwc.xxwc_ap_branch_list_stg_tbl
                WHERE UPPER(oracle_branch_number) = UPPER(rec_items.organization_code)
                  AND process_status = 'Y'
                  AND ahh_warehouse_number LIKE 'M%'
                  AND rownum=1;
            EXCEPTION
		    WHEN NO_DATA_FOUND THEN
               l_error_msg := 'Item is not found' ||rec_items.organization_code || '.' || rec_items.item;
               raise l_val_exception;
            WHEN OTHERS THEN
               l_error_msg := SUBSTR(l_sec||'=>'||SQLERRM,1,2000);
               raise l_val_exception;
            END;
            print_log('****l_ahh_part_number: '|| l_ahh_part_number||' l_ahh_warehouse_number: '|| l_ahh_warehouse_number);

            FOR rec_ahh_items_attribute IN c_ahh_items_attribute (p_ahh_partnumber    => l_ahh_part_number
                                                                 ,p_organization_code => l_ahh_warehouse_number) LOOP

               print_log('****Inside loop rec_ahh_items_attribute.ahh_partnumber: '|| rec_ahh_items_attribute.ahh_partnumber ||' rec_ahh_items_attribute.organization_code: '||rec_ahh_items_attribute.organization_code);
               print_log('****avgcost: '||rec_ahh_items_attribute.avgcost||' sourcing_from: '||rec_ahh_items_attribute.sourcing_from ||' leadtime: '||rec_ahh_items_attribute.leadtime
                    ||' minmaxmin: '|| rec_ahh_items_attribute.minmaxmin||' minmaxmax: '||rec_ahh_items_attribute.minmaxmax ||' org_item_status_code: '||rec_ahh_items_attribute.org_item_status_code
                    ||' icsw_minthresexdt: '||rec_ahh_items_attribute.icsw_minthresexdt||' threshold: '||rec_ahh_items_attribute.threshold );

               l_item_tbl_typ (1).transaction_type           := 'UPDATE'; -- Replace this with 'UPDATE' for update transaction.
               l_item_tbl_typ (1).inventory_item_id          := rec_items.inventory_item_id;
               l_item_tbl_typ (1).organization_id            := rec_items.organization_id;
               l_item_tbl_typ (1).buyer_id                   := NULL;
               l_item_tbl_typ (1).list_price_per_unit        := TO_NUMBER(rec_ahh_items_attribute.avgcost);
               l_item_tbl_typ (1).source_type                := XXWC_AHH_ITEM_CONV_PKG.derive_source_type(rec_ahh_items_attribute.sourcing_from
                                                                                                     );
               print_log('****l_item_tbl_typ (1).source_type: '||l_item_tbl_typ (1).source_type );

               l_item_tbl_typ (1).source_organization_id     := XXWC_AHH_ITEM_CONV_PKG.derive_source_org (l_item_tbl_typ (1).source_type
                                                                                                         --,l_ahh_warehouse_number
                                                                                                        ,rec_ahh_items_attribute.inv_org
                                                                                                         );
               print_log('****l_item_tbl_typ (1).source_organization_id: '||l_item_tbl_typ (1).source_organization_id );
               l_item_tbl_typ (1).source_type                := CASE
                                                                  WHEN l_item_tbl_typ (1).source_type = 1
                                                                       AND
                                                                       l_item_tbl_typ (1).source_organization_id IS NULL THEN
                                                                   XXWC_AHH_ITEM_CONV_PKG.derive_source_type('V')
                                                                ELSE
                                                                   l_item_tbl_typ (1).source_type
                                                                END;
               print_log('****l_item_tbl_typ (1).source_type #2: '||l_item_tbl_typ (1).source_type );

               l_item_tbl_typ (1).inventory_planning_code    := 2;
               l_item_tbl_typ (1).fixed_lot_multiplier       := NULL;
               l_item_tbl_typ (1).preprocessing_lead_time    := NULL;
               l_item_tbl_typ (1).full_lead_time             := TO_NUMBER(rec_ahh_items_attribute.leadtime);
               l_item_tbl_typ (1).min_minmax_quantity        := TO_NUMBER(rec_ahh_items_attribute.Minmaxmin);
               l_item_tbl_typ (1).max_minmax_quantity        := TO_NUMBER(rec_ahh_items_attribute.Minmaxmax);
               l_item_tbl_typ (1).attribute20                := NULL;
               l_item_tbl_typ (1).item_type                  := XXWC_AHH_ITEM_CONV_PKG.derive_ref_item_type ( rec_ahh_items_attribute.org_item_status_code);

               BEGIN
                  SELECT item_status
                    INTO l_item_tbl_typ(1).inventory_item_status_code
                    FROM xxwc.xxwc_item_master_ref
                   WHERE UPPER(item) = rec_items.item
                     AND ROWNUM=1;
               EXCEPTION
		       WHEN NO_DATA_FOUND THEN
                  l_error_msg := 'Item status is not found' ||rec_items.organization_code || '.' || rec_items.item;
                  print_log(l_error_msg);
                  --raise l_val_exception;
               WHEN OTHERS THEN
                  l_error_msg := SUBSTR(l_sec||'=>'||SQLERRM,1,2000);
                  print_log(l_error_msg);
                 --raise l_val_exception;
              END;
              print_log('****l_item_tbl_typ (1).inventory_item_status_code: '||l_item_tbl_typ(1).inventory_item_status_code);

              -- Rentals Default
              IF substr(rec_items.item ,1 ,1) = 'R' THEN
                 l_item_tbl_typ (1).list_price_per_unit      := 0;
                 l_item_tbl_typ (1).inventory_planning_code  := 6;
                 l_item_tbl_typ (1).min_minmax_quantity      := 0;
                 l_item_tbl_typ (1).max_minmax_quantity      := 0;
                 l_item_tbl_typ (1).item_type                := 'RENTAL';
              -- Rentals Default
              ELSIF substr(rec_items.item, 1, 2) = 'RR' THEN
                 --Rental
                 l_item_tbl_typ (1).item_type                := 'RE_RENT';
              END IF;

              print_log('****l_item_tbl_typ (1).item_type: '||l_item_tbl_typ (1).item_type );

              l_item_tbl_typ (1).attribute21               := CASE WHEN nvl(to_date(rec_ahh_items_attribute.icsw_minthresexdt,'MM/DD/RR'),trunc(SYSDATE + 1)) >= trunc(SYSDATE) THEN
                                                                rec_ahh_items_attribute.threshold
                                                           ELSE
                                                               NULL
                                                           END;

              xxwc_ahh_item_conv_pkg.populate_ccid(rec_items.item
                               ,rec_items.organization_code
                               ,x_cost_of_sales_account
                               ,x_sales_account
                               ,x_expense_account);
              print_log('****x_cost_of_sales_account: '||x_cost_of_sales_account || ' x_sales_account: '||x_sales_account || ' x_expense_account:'|| x_expense_account);

              l_item_tbl_typ (1).cost_of_sales_account := x_cost_of_sales_account;
              l_item_tbl_typ (1).sales_account := x_sales_account;
              l_item_tbl_typ (1).expense_account := x_expense_account;

              print_log('****l_item_tbl_typ (1).cost_of_sales_account: '||l_item_tbl_typ (1).cost_of_sales_account);
              print_log('****l_item_tbl_typ (1).sales_account: '||l_item_tbl_typ (1).sales_account);
              print_log('****l_item_tbl_typ (1).expense_account: '||l_item_tbl_typ(1).expense_account);

              ego_item_pub.process_items (p_api_version     => 1.0,
                                          p_init_msg_list   => fnd_api.g_true,
                                          p_commit          => fnd_api.g_true,
                                          p_item_tbl        => l_item_tbl_typ,
                                          x_item_tbl        => x_item_table,
                                          x_return_status   => x_return_status,
                                          x_msg_count       => x_msg_count);

              IF (x_return_status = fnd_api.g_ret_sts_success)
              THEN
                 print_log('****ego_item_pub.process_items api status: '||fnd_api.g_ret_sts_success);

                 COMMIT;
                 l_ego_item_pub_success_cnt := l_ego_item_pub_success_cnt+1;
              ELSE
                 DBMS_OUTPUT.put_line ('Error Messages :');
                 error_handler.get_message_list (x_message_list => x_message_list);

                 l_ego_item_pub_error_cnt := l_ego_item_pub_error_cnt+1;

                 FOR i IN 1 .. x_message_list.COUNT
                 LOOP
                   DBMS_OUTPUT.put_line (x_message_list (i).MESSAGE_TEXT);
                 END LOOP;
              END IF;
            END LOOP; --rec_ahh_items_attribute
         EXCEPTION
	     WHEN l_val_exception THEN
            print_log(l_error_msg);
         END;
      END LOOP; --rec_items
      print_out('****ego_item_pub.process_items api status success record count: '||l_ego_item_pub_success_cnt );
      print_out('****ego_item_pub.process_items api status error record count  : '||l_ego_item_pub_error_cnt );
    END IF; --p_update_item_att = 'Y'


    IF p_update_item_att_cat = 'Y' THEN
           assign_category   (  errbuf               => errbuf
                              ,retcode              => retcode
                              ,p_item_creation_date => l_item_creation_date
                              ,p_item1              => p_item1
                              ,p_item2              => p_item2
                              ,p_item3              => p_item3
                            );
    END IF;

    IF p_assign_source_rule = 'Y' THEN
        assign_sourcing_rule(  errbuf               => errbuf
                              ,retcode              => retcode
                              ,p_item_creation_date => l_item_creation_date
                              ,p_item1              => p_item1
                              ,p_item2              => p_item2
                              ,p_item3              => p_item3
                            );
    END IF;

    IF p_assign_Locators = 'Y' THEN
       process_item_locators(  errbuf               => errbuf
                              ,retcode              => retcode
                              ,p_item_creation_date => l_item_creation_date
                              ,p_item1              => p_item1
                              ,p_item2              => p_item2
                              ,p_item3              => p_item3
                            );
    END IF;

  EXCEPTION
    WHEN OTHERS THEN
      retcode := 2;
      errbuf  := SQLERRM;
      print_log('Error at section ' || l_sec || '-' || SQLERRM);
  END main;

END xxwc_ahh_item_attr_assign_pkg;
/