CREATE OR REPLACE VIEW XXEIS.EIS_XXWC_PO_ISR_LIVE_V
(
   ORG,
   ORGANIZATION_NAME,
   DISTRICT,
   REGION,
   PRE,
   ITEM_NUMBER,
   VENDOR_NAME,
   VENDOR_NUMBER,
   SOURCE,
   ST,
   DESCRIPTION,
   CAT_CLASS,
   INV_CAT_SEG1,
   PPLT,
   PLT,
   UOM,
   CL,
   STK,
   PM,
   MINN,
   MAXN,
   HIT_6,
   AVER_COST,
   ITEM_COST,
   BPA_COST,
   BPA,
   QOH,
   ON_ORD,
   AVAILABLE,
   AVAILABLEDOLLAR,
   INV$OH,
   BIN_LOC,
   MC,
   FI,
   FREEZE_DATE,
   RES,
   THIRTEEN_WK_AVG_INV,
   THIRTEEN_WK_AN_COGS,
   TURNS,
   BUYER,
   TS,
   AMU,
   SO,
   MF_FLAG,
   WT,
   SS,
   SOURCING_RULE,
   FML,
   OPEN_REQ,
   INVENTORY_ITEM_ID,
   ORGANIZATION_ID,
   SET_OF_BOOKS_ID,
   CLT,
   AVAIL2,
   DEMAND,
   INT_REQ,
   DIR_REQ,
   JAN_SALES,
   FEB_SALES,
   MAR_SALES,
   APR_SALES,
   MAY_SALES,
   JUNE_SALES,
   JUL_SALES,
   AUG_SALES,
   SEP_SALES,
   OCT_SALES,
   NOV_SALES,
   DEC_SALES,
   HIT4_SALES,
   ONE_SALES,
   SIX_SALES,
   TWELVE_SALES,
   HIT6_SALES,
   ITEM_STATUS_CODE,
   VENDOR_SITE,
   SOURCE_ORGANIZATION_ID
)
AS
   SELECT /*+index_ffs(MTL_ITEM_CATEGORIES_U1) */
          -- Added by Mahender TMS# 20140917-00097
          ood.organization_code org,
          ood.organization_name organization_name,
          mtp.attribute8 district,
          mtp.attribute9 region,
          SUBSTR (msi.segment1, 1, 3) pre,
          msi.segment1 item_number,
          pov.vendor_name vendor_name,
          pov.segment1 vendor_number,
          CASE
             WHEN item_source_type.meaning = 'Supplier'
             THEN
                pov.segment1
             WHEN item_source_type.meaning = 'Inventory'
             THEN
                (SELECT organization_code
                   FROM org_organization_definitions source_org
                  WHERE source_org.organization_id =
                           msi.source_organization_id)
             ELSE
                NULL
          END
             source,
          CASE
             WHEN item_source_type.meaning = 'Supplier' THEN 'S'
             WHEN item_source_type.meaning = 'Inventory' THEN 'I'
             ELSE NULL
          END
             st,
          msi.description description,
          mcvc.segment2 cat_class,
          mcvc.segment1 inv_cat_seg1,
          preprocessing_lead_time pplt,
          msi.full_lead_time plt,
          msi.primary_uom_code uom,
          mcvs.segment1 cl,
          CASE
             WHEN (mcvs.segment1 IN ('1',
                                     '2',
                                     '3',
                                     '4',
                                     '5',
                                     '6',
                                     '7',
                                     '8',
                                     '9',
                                     'C',
                                     'B'))
             THEN
                'Y'
             WHEN (    mcvs.segment1 IN ('E')
                   AND (min_minmax_quantity = 0 AND max_minmax_quantity = 0))
             THEN
                'N'
             WHEN (    mcvs.segment1 IN ('E')
                   AND (min_minmax_quantity > 0 AND max_minmax_quantity > 0))
             THEN
                'Y'
             WHEN (mcvs.segment1 IN ('N', 'Z'))
             --AND ITEM_TYPE  ='NON-STOCK')
             THEN
                'N'
             ELSE
                'N'
          END
             stk,
          mrp_planning_code.meaning pm,
          min_minmax_quantity MIN,
          max_minmax_quantity MAX,
          isr.hit6_store_sales hit_6,
          isr.aver_cost aver_cost,
          list_price_per_unit item_cost,
          isr.bpa_cost bpa_cost,
          isr.bpa bpa,
          isr.qoh,
          isr.on_ord,
          isr.available,
          isr.availabledollar,
          (isr.qoh * isr.aver_cost) inv$oh,
          xxeis.eis_po_xxwc_isr_util_pkg.get_primary_bin_loc (
             msi.inventory_item_id,
             msi.organization_id)
             bin_loc,
          isr.mc mc,
          mcvp.segment1 fi,
          isr.freeze_date freeze_date,
          msi.attribute21 res,
          isr.thirteen_wk_avg_inv,
          isr.thirteen_wk_an_cogs,
          isr.turns,
          ppf.full_name buyer,
          shelf_life_days ts,
          msi.attribute20 amu,
          isr.so,
          NULL mf_flag,
          msi.unit_weight wt,
          isr.ss,
          msr.sourcing_rule_name sourcing_rule,
          msi.fixed_lot_multiplier fml,
          isr.open_req,
          msi.inventory_item_id,
          msi.organization_id,
          ood.set_of_books_id,
          isr.clt,
          isr.avail2,
          isr.demand,
          isr.int_req,
          isr.dir_req,
          DECODE (
             xxeis.eis_po_xxwc_isr_util_pkg.get_isr_rpt_dc_mod_sub,
             'Yes', (NVL (jan_store_sale, 0) + NVL (jan_other_inv_sale, 0)),
             NVL (jan_store_sale, 0))
             jan_sales,
          DECODE (
             xxeis.eis_po_xxwc_isr_util_pkg.get_isr_rpt_dc_mod_sub,
             'Yes', (NVL (feb_store_sale, 0) + NVL (feb_other_inv_sale, 0)),
             NVL (feb_store_sale, 0))
             feb_sales,
          DECODE (
             xxeis.eis_po_xxwc_isr_util_pkg.get_isr_rpt_dc_mod_sub,
             'Yes', (NVL (mar_store_sale, 0) + NVL (mar_other_inv_sale, 0)),
             NVL (mar_store_sale, 0))
             mar_sales,
          DECODE (
             xxeis.eis_po_xxwc_isr_util_pkg.get_isr_rpt_dc_mod_sub,
             'Yes', (NVL (apr_store_sale, 0) + NVL (apr_other_inv_sale, 0)),
             NVL (apr_store_sale, 0))
             apr_sales,
          DECODE (
             xxeis.eis_po_xxwc_isr_util_pkg.get_isr_rpt_dc_mod_sub,
             'Yes', (NVL (may_store_sale, 0) + NVL (may_other_inv_sale, 0)),
             NVL (may_store_sale, 0))
             may_sales,
          DECODE (
             xxeis.eis_po_xxwc_isr_util_pkg.get_isr_rpt_dc_mod_sub,
             'Yes', (NVL (jun_store_sale, 0) + NVL (jun_other_inv_sale, 0)),
             NVL (jun_store_sale, 0))
             june_sales,
          DECODE (
             xxeis.eis_po_xxwc_isr_util_pkg.get_isr_rpt_dc_mod_sub,
             'Yes', (NVL (jul_store_sale, 0) + NVL (jul_other_inv_sale, 0)),
             NVL (jul_store_sale, 0))
             jul_sales,
          DECODE (
             xxeis.eis_po_xxwc_isr_util_pkg.get_isr_rpt_dc_mod_sub,
             'Yes', (NVL (aug_store_sale, 0) + NVL (aug_other_inv_sale, 0)),
             NVL (aug_store_sale, 0))
             aug_sales,
          DECODE (
             xxeis.eis_po_xxwc_isr_util_pkg.get_isr_rpt_dc_mod_sub,
             'Yes', (NVL (sep_store_sale, 0) + NVL (sep_other_inv_sale, 0)),
             NVL (sep_store_sale, 0))
             sep_sales,
          DECODE (
             xxeis.eis_po_xxwc_isr_util_pkg.get_isr_rpt_dc_mod_sub,
             'Yes', (NVL (oct_store_sale, 0) + NVL (oct_other_inv_sale, 0)),
             NVL (oct_store_sale, 0))
             oct_sales,
          DECODE (
             xxeis.eis_po_xxwc_isr_util_pkg.get_isr_rpt_dc_mod_sub,
             'Yes', (NVL (nov_store_sale, 0) + NVL (nov_other_inv_sale, 0)),
             NVL (nov_store_sale, 0))
             nov_sales,
          DECODE (
             xxeis.eis_po_xxwc_isr_util_pkg.get_isr_rpt_dc_mod_sub,
             'Yes', (NVL (dec_store_sale, 0) + NVL (dec_other_inv_sale, 0)),
             NVL (dec_store_sale, 0))
             dec_sales,
          DECODE (
             xxeis.eis_po_xxwc_isr_util_pkg.get_isr_rpt_dc_mod_sub,
             'Yes', (  NVL (hit4_store_sales, 0)
                     + NVL (hit4_other_inv_sales, 0)),
             NVL (hit4_store_sales, 0))
             hit4_sales,
          DECODE (
             xxeis.eis_po_xxwc_isr_util_pkg.get_isr_rpt_dc_mod_sub,
             'Yes', (NVL (one_store_sale, 0) + NVL (one_other_inv_sale, 0)),
             NVL (one_store_sale, 0))
             one_sales,
          DECODE (
             xxeis.eis_po_xxwc_isr_util_pkg.get_isr_rpt_dc_mod_sub,
             'Yes', (NVL (six_store_sale, 0) + NVL (six_other_inv_sale, 0)),
             NVL (six_store_sale, 0))
             six_sales,
          DECODE (
             xxeis.eis_po_xxwc_isr_util_pkg.get_isr_rpt_dc_mod_sub,
             'Yes', (  NVL (twelve_store_sale, 0)
                     + NVL (twelve_other_inv_sale, 0)),
             NVL (twelve_store_sale, 0))
             twelve_sales,
          DECODE (
             xxeis.eis_po_xxwc_isr_util_pkg.get_isr_rpt_dc_mod_sub,
             'Yes', (  NVL (hit6_store_sales, 0)
                     + NVL (hit6_other_inv_sales, 0)),
             NVL (hit6_store_sales, 0))
             hit6_sales,
          msi.inventory_item_status_code item_status_code,
          pvs.vendor_site_code vendor_site,
          msi.source_organization_id
     FROM mtl_system_items_b msi,
          org_organization_definitions ood,
          mtl_categories_kfv mcvs,
          mtl_item_categories mics,
          mtl_categories_kfv mcvp,
          mtl_item_categories micp,
          mtl_categories_kfv mcvc,
          mtl_item_categories micc,
          mrp_sr_assignments msa,
          mrp_sr_receipt_org msro,
          mrp_sr_source_org msso,
          mrp_sourcing_rules msr,
          po_vendors pov,
          po_vendor_sites pvs,
          mfg_lookups mrp_planning_code,
          mfg_lookups item_source_type,
          per_people_x ppf,
          mtl_parameters mtp,
          mfg_lookups sfty_stk,
          xxeis.eis_xxwc_po_isr_tab isr
    WHERE     msi.organization_id = ood.organization_id
          AND isr.organization_id(+) = msi.organization_id
          AND isr.inventory_item_id(+) = msi.inventory_item_id
          AND msi.buyer_id = ppf.person_id(+)
          AND msi.inventory_item_id = mics.inventory_item_id(+)
          AND msi.organization_id = mics.organization_id(+)
          AND mics.category_id = mcvs.category_id(+)
          AND mcvs.structure_id(+) = 50410
          AND mics.category_set_id(+) = 1100000044
          AND msi.inventory_item_id = micp.inventory_item_id(+)
          AND msi.organization_id = micp.organization_id(+)
          AND micp.category_id = mcvp.category_id(+)
          AND mcvp.structure_id(+) = 50408
          AND micp.category_set_id(+) = 1100000043
          AND msi.inventory_item_id = micc.inventory_item_id
          AND msi.organization_id = micc.organization_id
          AND micc.category_id = mcvc.category_id
          AND mcvc.structure_id = 101
          AND micc.category_set_id = 1100000062
          AND msi.inventory_item_id = msa.inventory_item_id(+)
          AND MSI.ORGANIZATION_ID = MSA.ORGANIZATION_ID(+)
          AND MSA.ASSIGNMENT_TYPE(+) = 6
          AND MSA.ASSIGNMENT_SET_ID(+) = 1
          AND MSA.SOURCING_RULE_ID = MSRO.SOURCING_RULE_ID(+)
          AND msa.sourcing_rule_id = msr.sourcing_rule_id(+)
          AND MSRO.SR_RECEIPT_ID = MSSO.SR_RECEIPT_ID(+)
          AND NVL (msso.source_type, 3) = 3
          AND msso.vendor_id = pov.vendor_id(+)
          AND msso.vendor_site_id = pvs.vendor_site_id(+)
          AND mrp_planning_code.lookup_type(+) = 'MTL_MATERIAL_PLANNING'
          AND mrp_planning_code.lookup_code(+) = msi.inventory_planning_code
          AND item_source_type.lookup_type(+) = 'MTL_SOURCE_TYPES'
          AND item_source_type.lookup_code(+) = msi.source_type
          AND msi.organization_id = mtp.organization_id
          AND sfty_stk.lookup_type(+) = 'MTL_SAFETY_STOCK_TYPE'
          AND sfty_stk.lookup_code(+) = msi.mrp_safety_stock_code
/
