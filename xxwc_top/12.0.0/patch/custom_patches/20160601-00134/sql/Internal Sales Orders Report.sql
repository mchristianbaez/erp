--Report Name            : Internal Sales Orders Report
--Import Instructions    : Run this file in XXEIS user from SQL*Plus or any tool like Toad, SQL Navigator. 
prompt Creating View Data for Internal Sales Orders Report
set scan off define off
DECLARE
BEGIN 
--Inserting View EIS_XXWC_OM_INT_OPEN_ORDERS_V
xxeis.eis_rs_ins.v( 'EIS_XXWC_OM_INT_OPEN_ORDERS_V',660,'','','','','SA059956','XXEIS','Eis Xxwc Om Open Orders V','EXOOOV','','');
--Delete View Columns for EIS_XXWC_OM_INT_OPEN_ORDERS_V
xxeis.eis_rs_utility.delete_view_rows('EIS_XXWC_OM_INT_OPEN_ORDERS_V',660,FALSE);
--Inserting View Columns for EIS_XXWC_OM_INT_OPEN_ORDERS_V
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_INT_OPEN_ORDERS_V','ORDER_LINE_STATUS',660,'Order Line Status','ORDER_LINE_STATUS','','','','SA059956','VARCHAR2','','','Order Line Status','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_INT_OPEN_ORDERS_V','ORDER_AMOUNT',660,'Order Amount','ORDER_AMOUNT','','~~2','','SA059956','NUMBER','','','Order Amount','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_INT_OPEN_ORDERS_V','SCHEDULE_SHIP_DATE',660,'Schedule Ship Date','SCHEDULE_SHIP_DATE','','','','SA059956','DATE','','','Schedule Ship Date','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_INT_OPEN_ORDERS_V','WAREHOUSE',660,'Warehouse','WAREHOUSE','','','','SA059956','VARCHAR2','','','Warehouse','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_INT_OPEN_ORDERS_V','ORDER_TYPE',660,'Order Type','ORDER_TYPE','','','','SA059956','VARCHAR2','','','Order Type','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_INT_OPEN_ORDERS_V','SHIPPING_METHOD',660,'Shipping Method','SHIPPING_METHOD','','','','SA059956','VARCHAR2','','','Shipping Method','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_INT_OPEN_ORDERS_V','ORDERED_DATE',660,'Ordered Date','ORDERED_DATE','','','','SA059956','DATE','','','Ordered Date','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_INT_OPEN_ORDERS_V','ORDER_NUMBER',660,'Order Number','ORDER_NUMBER','','','','SA059956','NUMBER','','','Order Number','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_INT_OPEN_ORDERS_V','CREATED_BY',660,'Created By','CREATED_BY','','','','SA059956','VARCHAR2','','','Created By','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_INT_OPEN_ORDERS_V','ORDER_HEADER_STATUS',660,'Order Header Status','ORDER_HEADER_STATUS','','','','SA059956','VARCHAR2','','','Order Header Status','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_INT_OPEN_ORDERS_V','PAYMENT_TERMS',660,'Payment Terms','PAYMENT_TERMS','','','','SA059956','VARCHAR2','','','Payment Terms','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_INT_OPEN_ORDERS_V','ITEM_DESCRIPTION',660,'Item Description','ITEM_DESCRIPTION','','','','SA059956','VARCHAR2','','','Item Description','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_INT_OPEN_ORDERS_V','ITEM_NUMBER',660,'Item Number','ITEM_NUMBER','','','','SA059956','VARCHAR2','','','Item Number','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_INT_OPEN_ORDERS_V','QTY',660,'Qty','QTY','','','','SA059956','NUMBER','','','Qty','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_INT_OPEN_ORDERS_V','BACKORDER_QTY',660,'Backorder Qty','BACKORDER_QTY','','','','SA059956','NUMBER','','','Backorder Qty','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_INT_OPEN_ORDERS_V','DISTRICT',660,'District','DISTRICT','','','','SA059956','VARCHAR2','','','District','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_INT_OPEN_ORDERS_V','REGION',660,'Region','REGION','','','','SA059956','VARCHAR2','','','Region','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_INT_OPEN_ORDERS_V','SHIP_QTY',660,'Ship Qty','SHIP_QTY','','','','SA059956','NUMBER','','','Ship Qty','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_INT_OPEN_ORDERS_V','SHIP_TO_ORG',660,'Ship To Org','SHIP_TO_ORG','','','','SA059956','VARCHAR2','','','Ship To Org','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_INT_OPEN_ORDERS_V','REQUISITION_NUMBER',660,'Requisition Number','REQUISITION_NUMBER','','','','SA059956','VARCHAR2','','','Requisition Number','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_INT_OPEN_ORDERS_V','PO_REQ_CREATED_BY',660,'Po Req Created By','PO_REQ_CREATED_BY','','','','SA059956','VARCHAR2','','','Po Req Created By','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_INT_OPEN_ORDERS_V','PO_REQ_CREATED_BY_ID',660,'Po Req Created By Id','PO_REQ_CREATED_BY_ID','','','','SA059956','NUMBER','','','Po Req Created By Id','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_INT_OPEN_ORDERS_V','REQ_APPR_DATE',660,'Req Appr Date','REQ_APPR_DATE','','','','SA059956','DATE','','','Req Appr Date','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_INT_OPEN_ORDERS_V','SALES_PERSON_NAME',660,'Sales Person Name','SALES_PERSON_NAME','','','','SA059956','VARCHAR2','','','Sales Person Name','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_INT_OPEN_ORDERS_V','ZIP_CODE',660,'Zip Code','ZIP_CODE','','','','SA059956','VARCHAR2','','','Zip Code','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_INT_OPEN_ORDERS_V','SHIP_TO_CITY',660,'Ship To City','SHIP_TO_CITY','','','','SA059956','VARCHAR2','','','Ship To City','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_INT_OPEN_ORDERS_V','CUSTOMER_JOB_NAME',660,'Customer Job Name','CUSTOMER_JOB_NAME','','','','SA059956','VARCHAR2','','','Customer Job Name','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_INT_OPEN_ORDERS_V','CUSTOMER_NAME',660,'Customer Name','CUSTOMER_NAME','','','','SA059956','VARCHAR2','','','Customer Name','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_INT_OPEN_ORDERS_V','CUSTOMER_NUMBER',660,'Customer Number','CUSTOMER_NUMBER','','','','SA059956','VARCHAR2','','','Customer Number','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_INT_OPEN_ORDERS_V','QUOTE_NUMBER',660,'Quote Number','QUOTE_NUMBER','','','','SA059956','NUMBER','','','Quote Number','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_INT_OPEN_ORDERS_V','HZPS_SHP_TO_PRT_ID',660,'Hzps Shp To Prt Id','HZPS_SHP_TO_PRT_ID','','','','SA059956','NUMBER','','','Hzps Shp To Prt Id','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_INT_OPEN_ORDERS_V','HZCS_SHIP_TO_STE_ID',660,'Hzcs Ship To Ste Id','HZCS_SHIP_TO_STE_ID','','','','SA059956','NUMBER','','','Hzcs Ship To Ste Id','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_INT_OPEN_ORDERS_V','MTP_ORGANIZATION_ID',660,'Mtp Organization Id','MTP_ORGANIZATION_ID','','','','SA059956','NUMBER','','','Mtp Organization Id','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_INT_OPEN_ORDERS_V','TRANSACTION_TYPE_ID',660,'Transaction Type Id','TRANSACTION_TYPE_ID','','','','SA059956','NUMBER','','','Transaction Type Id','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_INT_OPEN_ORDERS_V','PARTY_ID',660,'Party Id','PARTY_ID','','','','SA059956','NUMBER','','','Party Id','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_INT_OPEN_ORDERS_V','SALESREP_ID',660,'Salesrep Id','SALESREP_ID','','','','SA059956','NUMBER','','','Salesrep Id','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_INT_OPEN_ORDERS_V','CUST_ACCT_SITE_ID',660,'Cust Acct Site Id','CUST_ACCT_SITE_ID','','','','SA059956','NUMBER','','','Cust Acct Site Id','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_INT_OPEN_ORDERS_V','CUST_ACCOUNT_ID',660,'Cust Account Id','CUST_ACCOUNT_ID','','','','SA059956','NUMBER','','','Cust Account Id','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_INT_OPEN_ORDERS_V','ORDER_LINE_ID',660,'Order Line Id','ORDER_LINE_ID','','','','SA059956','NUMBER','','','Order Line Id','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_INT_OPEN_ORDERS_V','ORDER_HEADER_ID',660,'Order Header Id','ORDER_HEADER_ID','','','','SA059956','NUMBER','','','Order Header Id','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_INT_OPEN_ORDERS_V','LINE_CREATION_DATE',660,'Line Creation Date','LINE_CREATION_DATE','','','','SA059956','DATE','','','Line Creation Date','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_INT_OPEN_ORDERS_V','HEADER_STATUS',660,'Header Status','HEADER_STATUS','','','','SA059956','VARCHAR2','','','Header Status','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_INT_OPEN_ORDERS_V','LINE_NUMBER',660,'Line Number','LINE_NUMBER','','','','SA059956','VARCHAR2','','','Line Number','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_INT_OPEN_ORDERS_V','PROCESS_ID',660,'Process Id','PROCESS_ID','','','','SA059956','NUMBER','','','Process Id','','','');
--Inserting View Components for EIS_XXWC_OM_INT_OPEN_ORDERS_V
xxeis.eis_rs_ins.vcomp( 'EIS_XXWC_OM_INT_OPEN_ORDERS_V','HZ_CUST_ACCOUNTS',660,'HZ_CUST_ACCOUNTS','HCA','HCA','SA059956','SA059956','-1','Stores Information About Customer Accounts.','','','','');
xxeis.eis_rs_ins.vcomp( 'EIS_XXWC_OM_INT_OPEN_ORDERS_V','HZ_PARTIES',660,'HZ_PARTIES','HZP','HZP','SA059956','SA059956','-1','Information About Parties Such As Organizations, P','','','','');
xxeis.eis_rs_ins.vcomp( 'EIS_XXWC_OM_INT_OPEN_ORDERS_V','MTL_PARAMETERS',660,'MTL_PARAMETERS','MTP','MTP','SA059956','SA059956','-1','Inventory Control Options And Defaults','','','','');
xxeis.eis_rs_ins.vcomp( 'EIS_XXWC_OM_INT_OPEN_ORDERS_V','HZ_CUST_ACCT_SITES',660,'HZ_CUST_ACCT_SITES_ALL','HCAS_SHIP_TO','HCAS_SHIP_TO','SA059956','SA059956','-1','Stores All Customer Account Sites Across All Opera','','','','');
--Inserting View Component Joins for EIS_XXWC_OM_INT_OPEN_ORDERS_V
xxeis.eis_rs_ins.vcj( 'EIS_XXWC_OM_INT_OPEN_ORDERS_V','HZ_PARTIES','HZP',660,'EXOOOV.PARTY_ID','=','HZP.PARTY_ID(+)','','','','Y','SA059956','','');
xxeis.eis_rs_ins.vcj( 'EIS_XXWC_OM_INT_OPEN_ORDERS_V','MTL_PARAMETERS','MTP',660,'EXOOOV.MTP_ORGANIZATION_ID','=','MTP.ORGANIZATION_ID(+)','','','','Y','SA059956','','');
xxeis.eis_rs_ins.vcj( 'EIS_XXWC_OM_INT_OPEN_ORDERS_V','HZ_CUST_ACCT_SITES','HCAS_SHIP_TO',660,'EXOOOV.CUST_ACCT_SITE_ID','=','HCAS_SHIP_TO.CUST_ACCT_SITE_ID(+)','','','','Y','SA059956','','');
xxeis.eis_rs_ins.vcj( 'EIS_XXWC_OM_INT_OPEN_ORDERS_V','HZ_CUST_ACCOUNTS','HCA',660,'EXOOOV.CUST_ACCOUNT_ID','=','HCA.CUST_ACCOUNT_ID(+)','','','','Y','SA059956','','');
END;
/
set scan on define on
prompt Creating Report LOV Data for Internal Sales Orders Report
set scan off define off
DECLARE
BEGIN 
--Inserting Report LOVs - Internal Sales Orders Report
xxeis.eis_rs_ins.lov( 660,'select ott.name order_type,ott.description description,ott.transaction_type_id order_type_id from oe_transaction_types_vl ott','','OM ORDER TYPE','This gives the Order Type','XXEIS_RS_ADMIN',NULL,'N','','');
xxeis.eis_rs_ins.lov( 660,'select meaning ship_method,description from FND_LOOKUP_VALUES_vl where lookup_type=''SHIP_METHOD''','','OM SHIP METHOD','','XXEIS_RS_ADMIN',NULL,'N','','');
xxeis.eis_rs_ins.lov( 660,'SELECT ORGANIZATION_CODE WAREHOUSE,ORGANIZATION_NAME FROM ORG_ORGANIZATION_DEFINITIONS OOD WHERE SYSDATE < NVL(OOD.DISABLE_DATE,SYSDATE+1) AND EXISTS (SELECT 1 FROM XXEIS.EIS_ORG_ACCESS_V  WHERE organization_id = ood.organization_id )','','OM WAREHOUSE','','XXEIS_RS_ADMIN',NULL,'N','','');
xxeis.eis_rs_ins.lov( 660,'SELECT ORGANIZATION_CODE WAREHOUSE,ORGANIZATION_NAME FROM ORG_ORGANIZATION_DEFINITIONS OOD WHERE SYSDATE < NVL(OOD.DISABLE_DATE,SYSDATE+1) AND EXISTS (SELECT 1 FROM XXEIS.EIS_ORG_ACCESS_V  WHERE organization_id = ood.organization_id )','','OM WAREHOUSE','','XXEIS_RS_ADMIN',NULL,'N','','');
xxeis.eis_rs_ins.lov( 660,'SELECT meaning Status
  FROM OE_LOOKUPS lv
 WHERE lookup_type = ''FLOW_STATUS''','','WC OM Order Header Status','Displays distinct header status','XXEIS_RS_ADMIN',NULL,'N','','');
xxeis.eis_rs_ins.lov( 660,'SELECT meaning Status
  FROM OE_LOOKUPS lv
 WHERE lookup_type = ''LINE_FLOW_STATUS''','','WC OM Order Line Status','','XXEIS_RS_ADMIN',NULL,'N','','');
xxeis.eis_rs_ins.lov( 660,'Select  name, description from ra_terms_vl','','WC OM Payment Terms','','XXEIS_RS_ADMIN',NULL,'N','','');
xxeis.eis_rs_ins.lov( 660,'select distinct ATTRIBUTE9 region from mtl_parameters','','Region Lov','','XXEIS_RS_ADMIN',NULL,'N','','');
xxeis.eis_rs_ins.lov( 660,'select distinct ATTRIBUTE8 District from mtl_parameters','','District Lov','','ANONYMOUS',NULL,'N','','');
xxeis.eis_rs_ins.lov( 660,'select list_name from XXWC_PARAM_LIST where list_type = ''Org'' and (PUBLIC_FLAG =''Y'' OR USER_ID = fnd_global.user_id)','','XXWC Org List','','ANONYMOUS',NULL,'N','','');
xxeis.eis_rs_ins.lov( '','select distinct ppf.person_id Employee_id,ppf.full_name Buyer_Name
from per_all_people_f ppf,po_agents pa
where ppf.person_id = pa.agent_id
order by ppf.full_name','','XXWC EIS Buyer for Requisition','To Display Buyer name for requestion created','ANONYMOUS',NULL,'N','','');
END;
/
set scan on define on
prompt Creating Report Data for Internal Sales Orders Report
set scan off define off
DECLARE
BEGIN 
--Deleting Report data - Internal Sales Orders Report
xxeis.eis_rs_utility.delete_report_rows( 'Internal Sales Orders Report' );
--Inserting Report - Internal Sales Orders Report
xxeis.eis_rs_ins.r( 660,'Internal Sales Orders Report','','Open orders report by customer, by job, by salesperson, by created by, by shipping method, by promise date.','','','','SA059956','EIS_XXWC_OM_INT_OPEN_ORDERS_V','Y','','','SA059956','','N','White Cap Reports','','CSV,Pivot Excel,EXCEL,','');
--Inserting Report Columns - Internal Sales Orders Report
xxeis.eis_rs_ins.rc( 'Internal Sales Orders Report',660,'ORDER_HEADER_STATUS','Order Header Status','Order Header Status','','','default','','6','N','','','','','','','','SA059956','N','N','','EIS_XXWC_OM_INT_OPEN_ORDERS_V','','');
xxeis.eis_rs_ins.rc( 'Internal Sales Orders Report',660,'ORDERED_DATE','Ordered Date','Ordered Date','','','default','','4','N','','','','','','','','SA059956','N','N','','EIS_XXWC_OM_INT_OPEN_ORDERS_V','','');
xxeis.eis_rs_ins.rc( 'Internal Sales Orders Report',660,'ORDER_AMOUNT','Order Amount','Order Amount','','~,~.~','default','','14','N','','','','','','','','SA059956','N','N','','EIS_XXWC_OM_INT_OPEN_ORDERS_V','','');
xxeis.eis_rs_ins.rc( 'Internal Sales Orders Report',660,'ORDER_LINE_STATUS','Order Line Status','Order Line Status','','','default','','7','N','','','','','','','','SA059956','N','N','','EIS_XXWC_OM_INT_OPEN_ORDERS_V','','');
xxeis.eis_rs_ins.rc( 'Internal Sales Orders Report',660,'ORDER_NUMBER','Order Number','Order Number','VARCHAR2','','left','','3','N','','','','','','','','SA059956','N','N','','EIS_XXWC_OM_INT_OPEN_ORDERS_V','','');
xxeis.eis_rs_ins.rc( 'Internal Sales Orders Report',660,'ORDER_TYPE','Order Type','Order Type','','','default','','5','N','','','','','','','','SA059956','N','N','','EIS_XXWC_OM_INT_OPEN_ORDERS_V','','');
xxeis.eis_rs_ins.rc( 'Internal Sales Orders Report',660,'SCHEDULE_SHIP_DATE','Schedule Ship Date','Schedule Ship Date','','','default','','16','N','','','','','','','','SA059956','N','N','','EIS_XXWC_OM_INT_OPEN_ORDERS_V','','');
xxeis.eis_rs_ins.rc( 'Internal Sales Orders Report',660,'SHIPPING_METHOD','Shipping Method','Shipping Method','','','default','','15','N','','','','','','','','SA059956','N','N','','EIS_XXWC_OM_INT_OPEN_ORDERS_V','','');
xxeis.eis_rs_ins.rc( 'Internal Sales Orders Report',660,'WAREHOUSE','Ship From Org','Warehouse','','','default','','1','N','','','','','','','','SA059956','N','N','','EIS_XXWC_OM_INT_OPEN_ORDERS_V','','');
xxeis.eis_rs_ins.rc( 'Internal Sales Orders Report',660,'CREATED_BY','Created By','Created By','','','default','','2','N','','','','','','','','SA059956','N','N','','EIS_XXWC_OM_INT_OPEN_ORDERS_V','','');
xxeis.eis_rs_ins.rc( 'Internal Sales Orders Report',660,'ITEM_DESCRIPTION','Item Description','Item Description','','','default','','10','N','','','','','','','','SA059956','N','N','','EIS_XXWC_OM_INT_OPEN_ORDERS_V','','');
xxeis.eis_rs_ins.rc( 'Internal Sales Orders Report',660,'ITEM_NUMBER','Item','Item Number','','','default','','9','N','','','','','','','','SA059956','N','N','','EIS_XXWC_OM_INT_OPEN_ORDERS_V','','');
xxeis.eis_rs_ins.rc( 'Internal Sales Orders Report',660,'QTY','Order Qty','Qty','','~,~.~0','default','','11','N','','','','','','','','SA059956','N','N','','EIS_XXWC_OM_INT_OPEN_ORDERS_V','','');
xxeis.eis_rs_ins.rc( 'Internal Sales Orders Report',660,'BACKORDER_QTY','Backorder Qty','Backorder Qty','','~,~.~0','default','','13','N','','','','','','','','SA059956','N','N','','EIS_XXWC_OM_INT_OPEN_ORDERS_V','','');
xxeis.eis_rs_ins.rc( 'Internal Sales Orders Report',660,'SHIP_QTY','Ship Qty','Ship Qty','','~,~.~0','default','','12','N','','','','','','','','SA059956','N','N','','EIS_XXWC_OM_INT_OPEN_ORDERS_V','','');
xxeis.eis_rs_ins.rc( 'Internal Sales Orders Report',660,'SHIP_TO_ORG','Ship To Org','Ship To Org','','','default','','8','N','','','','','','','','SA059956','N','N','','EIS_XXWC_OM_INT_OPEN_ORDERS_V','','');
xxeis.eis_rs_ins.rc( 'Internal Sales Orders Report',660,'LATE','Late','Ship To Org','VARCHAR2','','default','','17','Y','','','','','','','case when trunc(sysdate)- trunc(EXOOOV.SCHEDULE_SHIP_DATE) >=1 Then ''Y'' ELSE  ''N''  END','SA059956','N','N','','EIS_XXWC_OM_INT_OPEN_ORDERS_V','','');
xxeis.eis_rs_ins.rc( 'Internal Sales Orders Report',660,'PO_REQ_CREATED_BY','Req Created By','Po Req Created By','','','default','','19','N','','','','','','','','SA059956','N','N','','EIS_XXWC_OM_INT_OPEN_ORDERS_V','','');
xxeis.eis_rs_ins.rc( 'Internal Sales Orders Report',660,'REQUISITION_NUMBER','Requisition Number','Requisition Number','','','default','','18','N','','','','','','','','SA059956','N','N','','EIS_XXWC_OM_INT_OPEN_ORDERS_V','','');
--Inserting Report Parameters - Internal Sales Orders Report
xxeis.eis_rs_ins.rp( 'Internal Sales Orders Report',660,'Req Appr Date From','Ordered Date From','REQ_APPR_DATE','>=','','','DATE','Y','Y','1','','N','CONSTANT','SA059956','Y','N','','','');
xxeis.eis_rs_ins.rp( 'Internal Sales Orders Report',660,'Order Type','Order Type','ORDER_TYPE','IN','OM ORDER TYPE','','VARCHAR2','N','Y','8','','N','CONSTANT','SA059956','Y','N','','','');
xxeis.eis_rs_ins.rp( 'Internal Sales Orders Report',660,'Schedule Ship Date From','Schedule Ship Date From','SCHEDULE_SHIP_DATE','>=','','','DATE','N','Y','10','','N','CONSTANT','SA059956','Y','N','','','');
xxeis.eis_rs_ins.rp( 'Internal Sales Orders Report',660,'Shipping Method','Shipping Method','SHIPPING_METHOD','IN','OM SHIP METHOD','','VARCHAR2','N','Y','9','','N','CONSTANT','SA059956','Y','N','','','');
xxeis.eis_rs_ins.rp( 'Internal Sales Orders Report',660,'Ship From Org','Ship From Org','WAREHOUSE','IN','OM WAREHOUSE','','VARCHAR2','N','Y','6','','N','CONSTANT','SA059956','Y','N','','','');
xxeis.eis_rs_ins.rp( 'Internal Sales Orders Report',660,'Req Appr Date To','Ordered Date To','REQ_APPR_DATE','<=','','','DATE','Y','Y','2','','N','CONSTANT','SA059956','Y','N','','','');
xxeis.eis_rs_ins.rp( 'Internal Sales Orders Report',660,'Schedule Ship Date To','Schedule Ship Date To','SCHEDULE_SHIP_DATE','<=','','','DATE','N','Y','11','','N','CONSTANT','SA059956','Y','N','','','');
xxeis.eis_rs_ins.rp( 'Internal Sales Orders Report',660,'Order Line Status','Order Line Status','ORDER_LINE_STATUS','IN','WC OM Order Line Status','','VARCHAR2','N','Y','12','','N','CONSTANT','SA059956','Y','N','','','');
xxeis.eis_rs_ins.rp( 'Internal Sales Orders Report',660,'Order Header Status','Order Header Status','ORDER_HEADER_STATUS','IN','WC OM Order Header Status','','VARCHAR2','N','Y','13','','N','CONSTANT','SA059956','Y','N','','','');
xxeis.eis_rs_ins.rp( 'Internal Sales Orders Report',660,'Payment Terms','Payment Terms','PAYMENT_TERMS','IN','WC OM Payment Terms','','VARCHAR2','N','Y','14','','N','CONSTANT','SA059956','Y','N','','','');
xxeis.eis_rs_ins.rp( 'Internal Sales Orders Report',660,'District','District','DISTRICT','IN','District Lov','','VARCHAR2','N','Y','4','','N','CONSTANT','SA059956','Y','N','','','');
xxeis.eis_rs_ins.rp( 'Internal Sales Orders Report',660,'Region','Region','REGION','IN','Region Lov','','VARCHAR2','N','Y','3','','N','CONSTANT','SA059956','Y','N','','','');
xxeis.eis_rs_ins.rp( 'Internal Sales Orders Report',660,'Location List','Location List','','IN','XXWC Org List','','VARCHAR2','N','Y','5','','N','CONSTANT','SA059956','Y','N','','','');
xxeis.eis_rs_ins.rp( 'Internal Sales Orders Report',660,'Ship To Org','Ship To Org','SHIP_TO_ORG','IN','OM WAREHOUSE','','VARCHAR2','N','Y','7','','N','CONSTANT','SA059956','Y','N','','','');
xxeis.eis_rs_ins.rp( 'Internal Sales Orders Report',660,'Req Created By','Req Created By','PO_REQ_CREATED_BY_ID','IN','XXWC EIS Buyer for Requisition','','NUMERIC','N','Y','15','','N','CONSTANT','SA059956','Y','N','','','');
--Inserting Report Conditions - Internal Sales Orders Report
xxeis.eis_rs_ins.rcn( 'Internal Sales Orders Report',660,'','','','','and PROCESS_ID = :SYSTEM.PROCESS_ID','Y','1','','SA059956');
--Inserting Report Sorts - Internal Sales Orders Report
xxeis.eis_rs_ins.rs( 'Internal Sales Orders Report',660,'ORDER_NUMBER','ASC','SA059956','','');
xxeis.eis_rs_ins.rs( 'Internal Sales Orders Report',660,'WAREHOUSE','ASC','SA059956','','');
--Inserting Report Triggers - Internal Sales Orders Report
xxeis.eis_rs_ins.rt( 'Internal Sales Orders Report',660,'begin
xxeis.EIS_XXWC_INT_SALES_ORDERS_PKG.process_int_sales_orders(
p_process_id   =>   :system.process_id,
P_REQ_APPR_DATE_FROM    =>   :Req Appr Date From,
P_REQ_APPR_DATE_TO        =>    :Req Appr Date To,
p_order_type             =>    :Order Type,
p_schedule_ship_date_from  =>    :Schedule Ship Date From,
p_shipping_method        =>    :Shipping Method,
p_ship_from_org          =>    :Ship From Org,
p_schedule_ship_date_to  =>    :Schedule Ship Date To,
p_order_line_status      =>    :Order Line Status,
p_order_header_status    =>    :Order Header Status,
p_payment_terms          =>    :Payment Terms,
p_district               =>    :District,
p_region                 =>    :Region,
p_location_list          =>    :Location List,
p_ship_to_org            =>    :Ship To Org,
p_req_created_by   =>    :Req Created By
);

end;','B','Y','SA059956');
xxeis.eis_rs_ins.rt( 'Internal Sales Orders Report',660,'begin
xxeis.EIS_XXWC_INT_SALES_ORDERS_PKG.CLEAR_TEMP_TABLES(P_PROCESS_ID => :SYSTEM.PROCESS_ID);
end;','A','Y','SA059956');
--Inserting Report Templates - Internal Sales Orders Report
--Inserting Report Portals - Internal Sales Orders Report
xxeis.eis_rs_ins.r_port( 'Internal Sales Orders Report','XXWC_PUR_TOP_RPTS','660','Internal Sales Orders Report','Internal Open Transfer report - Not shipped','OA.jsp?page=/eis/oracle/apps/xxeis/reporting/webui/EISLaunchPG&EisProduct=Reporting&Portal=Yes&mod=Order Management','','Pivot Excel,EXCEL,','CONC','N','SA059956');
--Inserting Report Dashboards - Internal Sales Orders Report
--Inserting Report Security - Internal Sales Orders Report
xxeis.eis_rs_ins.rsec( 'Internal Sales Orders Report','660','','51044',660,'SA059956','','');
xxeis.eis_rs_ins.rsec( 'Internal Sales Orders Report','660','','51065',660,'SA059956','','');
xxeis.eis_rs_ins.rsec( 'Internal Sales Orders Report','660','','50901',660,'SA059956','','');
xxeis.eis_rs_ins.rsec( 'Internal Sales Orders Report','660','','51025',660,'SA059956','','');
xxeis.eis_rs_ins.rsec( 'Internal Sales Orders Report','660','','50860',660,'SA059956','','');
xxeis.eis_rs_ins.rsec( 'Internal Sales Orders Report','660','','50886',660,'SA059956','','');
xxeis.eis_rs_ins.rsec( 'Internal Sales Orders Report','660','','50859',660,'SA059956','','');
xxeis.eis_rs_ins.rsec( 'Internal Sales Orders Report','660','','50858',660,'SA059956','','');
xxeis.eis_rs_ins.rsec( 'Internal Sales Orders Report','660','','50857',660,'SA059956','','');
xxeis.eis_rs_ins.rsec( 'Internal Sales Orders Report','201','','50921',660,'SA059956','','');
xxeis.eis_rs_ins.rsec( 'Internal Sales Orders Report','201','','50983',660,'SA059956','','');
xxeis.eis_rs_ins.rsec( 'Internal Sales Orders Report','201','','50621',660,'SA059956','','');
xxeis.eis_rs_ins.rsec( 'Internal Sales Orders Report','201','','50893',660,'SA059956','','');
xxeis.eis_rs_ins.rsec( 'Internal Sales Orders Report','201','','50910',660,'SA059956','','');
xxeis.eis_rs_ins.rsec( 'Internal Sales Orders Report','201','','50892',660,'SA059956','','');
xxeis.eis_rs_ins.rsec( 'Internal Sales Orders Report','','KG013546','',660,'SA059956','','');
xxeis.eis_rs_ins.rsec( 'Internal Sales Orders Report','','PK059658','',660,'SA059956','','');
--Inserting Report Pivots - Internal Sales Orders Report
xxeis.eis_rs_ins.rpivot( 'Internal Sales Orders Report',660,'Open Internal Orders','1','1,0|1,1,0','1,1,1,0|None|2');
--Inserting Report Pivot Details For Pivot - Open Internal Orders
xxeis.eis_rs_ins.rpivot_dtls( 'Internal Sales Orders Report',660,'Open Internal Orders','PO_REQ_CREATED_BY','PAGE_FIELD','','','2','','');
xxeis.eis_rs_ins.rpivot_dtls( 'Internal Sales Orders Report',660,'Open Internal Orders','ORDER_NUMBER','ROW_FIELD','','','3','0','');
xxeis.eis_rs_ins.rpivot_dtls( 'Internal Sales Orders Report',660,'Open Internal Orders','WAREHOUSE','ROW_FIELD','','','2','1','');
xxeis.eis_rs_ins.rpivot_dtls( 'Internal Sales Orders Report',660,'Open Internal Orders','ORDERED_DATE','ROW_FIELD','','','4','0','');
xxeis.eis_rs_ins.rpivot_dtls( 'Internal Sales Orders Report',660,'Open Internal Orders','SCHEDULE_SHIP_DATE','ROW_FIELD','','','5','0','');
xxeis.eis_rs_ins.rpivot_dtls( 'Internal Sales Orders Report',660,'Open Internal Orders','ORDER_LINE_STATUS','DATA_FIELD','COUNT','Lines','1','','xlNormal');
xxeis.eis_rs_ins.rpivot_dtls( 'Internal Sales Orders Report',660,'Open Internal Orders','LATE','PAGE_FIELD','','','1','','');
xxeis.eis_rs_ins.rpivot_dtls( 'Internal Sales Orders Report',660,'Open Internal Orders','SHIP_TO_ORG','ROW_FIELD','','','1','','');
--Inserting Report Summary Calculation Columns For Pivot- Open Internal Orders
END;
/
set scan on define on
