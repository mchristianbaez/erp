/*
 TMS: 20160603-00207 Negative on order.
 */
SET SERVEROUTPUT ON SIZE 1000000


BEGIN
   DBMS_OUTPUT.put_line ('Before update');

   UPDATE apps.po_requisition_lines_all
      SET quantity = QUANTITY_DELIVERED
    WHERE requisition_line_id = 1886202;

 DBMS_OUTPUT.put_line ('Records updated1-' || SQL%ROWCOUNT);
 
   UPDATE apps.po_req_distributions_all
      SET req_line_quantity =
             (SELECT QUANTITY_DELIVERED
                FROM apps.po_requisition_lines_all
               WHERE requisition_line_id = 1886202)
    WHERE requisition_line_id = 1886202;


   DBMS_OUTPUT.put_line ('Records updated2-' || SQL%ROWCOUNT);
   COMMIT;
EXCEPTION
   WHEN OTHERS
   THEN
      DBMS_OUTPUT.put_line ('Unable to update ' || SQLERRM);
END;
/