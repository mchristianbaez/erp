/*
 TMS: 20161221-00137
 Date: 12/21/2016
 Notes:  Prefix 2017 offer names with TERM- that were created in DRAFT status during mass copy on 20-DEC-16 
*/
SET SERVEROUTPUT ON SIZE 1000000
DECLARE
 --
 n_loc number :=0;
 n_count number :=0;
 l_date date :=Null;
 l_err_msg varchar2(2000) :=Null;
 v_db_name varchar2(240) :=Null;
 b_go boolean;
 l_failed number :=0;
 l_success number :=0; 
 --
 cursor offers (p_date in date) is
 select b.description, b.name, a.offer_id, a.status_code, b.list_header_id
from apps.ozf_offers a, apps.qp_list_headers_vl b
where 1 =1
     and trunc(a.creation_date) =p_date
     and b.list_header_id =a.qp_list_header_id
     and b.attribute7 ='2017'
     and a.status_code ='DRAFT'
     and b.list_header_id !=3720349
     ;
 --
BEGIN --Main Processing...
 --
 select upper(name) into v_db_name from v$database; 
 --
     if v_db_name ='EBSPRD' then 
          l_date :='20-DEC-16';
     else
          l_date :=trunc(sysdate);
     end if; 
       --
                       --dbms_output.put_line('l_date fetched '||l_date||' --196');
       --   
     for rec in offers (p_date =>l_date) loop
       --
                      -- dbms_output.put_line('inside loop --197');
       --
          begin
           --
           savepoint sqr1;
           --
             update qp_list_headers_tl
             set description ='TERM-'||DESCRIPTION
             where 1 =1
                 and list_header_id =rec.list_header_id
                 ;    
                 --dbms_output.put_line('description updated --198');
             --
             if sql%rowcount >0 then
               l_success := l_success + 1;
                           --dbms_output.put_line('l_success --199');
             else
               l_failed :=l_failed + 1;
                           --dbms_output.put_line('l_failed --199');
             end if;
             --
          exception
           when others then
            rollback to sqr1;
            --dbms_output.put_line('rolled back updates --200');
          end;
     --
     end loop;
     --
     dbms_output.put_line('Failed Count : '||l_failed);
     dbms_output.put_line('Success Count : '||l_success); 
     --dbms_output.put_line('Total offer names processed : '||l_success + l_failed); 
     --     
     commit;
     --
EXCEPTION
   WHEN OTHERS
   THEN
      ROLLBACK;
      DBMS_OUTPUT.put_line (
         'TMS: 20161221-00137-1, Errors =' || SQLERRM);
END;
/