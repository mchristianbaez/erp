/*
 TMS: 20161221-00137
 Date: 12/21/2016
 Notes:  Prefix 2017 program names with TERM- that were created in DRAFT status during mass copy on 20-DEC-16 
*/
SET SERVEROUTPUT ON SIZE 1000000
DECLARE
 --
 n_loc number :=0;
 n_count number :=0;
 l_date date :=Null;
 l_err_msg varchar2(2000) :=Null;
 v_db_name varchar2(240) :=Null;
 b_go boolean;
 l_failed number :=0;
 l_success number :=0;
 --
 cursor fund_names  (p_date in date) is
 select a.*
from apps.ozf_funds_all_tl a 
where 1 =1
and trunc(a.creation_date) =p_date
and a.short_name like 'FY2017%'
     ;
 --
BEGIN --Main Processing...
 --
 select upper(name) into v_db_name from v$database; 
 --
     if v_db_name ='EBSPRD' then 
          l_date :=to_date('20-DEC-16', 'DD-MON-YY');
     else
          l_date :=trunc(sysdate);
     end if; 
     --
     for rec in fund_names (p_date =>l_date) loop
      --
          begin
           --
           savepoint sqr1;
           --
             update apps.ozf_funds_all_tl
             set short_name ='TERM-'||short_name
             where 1 =1
                 and fund_id =rec.fund_id
                 ;    
             --
             if sql%rowcount >0 then
               --
               l_success := l_success + 1;
               --
                 update apps.ozf_funds_all_b
                 set attribute1 =Null
                 where 1 =1
                     and fund_id =rec.fund_id
                     ;
             else
               l_failed :=l_failed + 1;
             end if;
             --
          exception
           when others then
            rollback to sqr1;
          end;
     --
     end loop;
     --
    dbms_output.put_line('Failed Count : '||l_failed);
    dbms_output.put_line('Success Count : '||l_success); 
    --dbms_output.put_line('Total programs processed : '||l_success + l_failed); 
    --     
     commit;
     --
EXCEPTION
   WHEN OTHERS
   THEN
      ROLLBACK;
      DBMS_OUTPUT.put_line (
         'TMS:20161221-00137-2 , Errors =' || SQLERRM);
END;
/