/*************************************************************************
  $Header datafix_TMS_20170104_00062.sql $
  Module Name: TMS_20170104_00062


  REVISIONS:
  Ver        Date         Author                Description
  ---------  -----------  ------------------    --------------------------
   1.0       05-APR-2017  Niraj K Ranjan        TMS#20170104_00062   TRANSFER STUCK IN SHIPPED STATUS - 22873606
**************************************************************************/ 
SET SERVEROUTPUT ON SIZE 1000000
BEGIN
   DBMS_OUTPUT.put_line ('TMS: 20170104_00062   , Before Update');

   update apps.oe_order_lines_all
   set 
      INVOICE_INTERFACE_STATUS_CODE='NOT_ELIGIBLE'
     ,open_flag='N'
     ,flow_status_code='CLOSED'
   where line_id = 85432991
   and headeR_id= 52348682;

   DBMS_OUTPUT.put_line (
         'TMS: 20170104_00062  Sales order lines updated (Expected:1): '
      || SQL%ROWCOUNT);

   COMMIT;

   DBMS_OUTPUT.put_line ('TMS: 20170104_00062    , End Update');
EXCEPTION
   WHEN OTHERS
   THEN
      ROLLBACK;
      DBMS_OUTPUT.put_line ('TMS: 20170104_00062 , Errors : ' || SQLERRM);
END;
/
