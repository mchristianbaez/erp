CREATE TABLE  XXWC.XXWC_DMS_DOCLINK_TBL
/* Ver        Date         Author           Description
   ---------  -----------  ---------------  ------------------------------------
   1.0        22-SEP-2015  Pattabhi Avula   TMS 20150602-00180 - DMS - XXWC DMS Copy CFD to DocLink program bug fix
                                            Initial Version   */
(REQUEST_ID       NUMBER(20,0), 
ID                NUMBER(30,0), 
HEADER_ID         NUMBER(30,0), 
DELIVERY_ID       NUMBER(30,0), 
CREATED_BY        NUMBER(20,0),
CREATION_DATE     DATE, 
LAST_UPDATED_BY   NUMBER(20,0), 
LAST_UPDATE_DATE  DATE)
/