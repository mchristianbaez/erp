--Report Name            : Receipt Details-WC
--Import Instructions : 1. If RTF template needs to be uploaded to report automatically then move the RTF template to physical path of APPS_DATA_FILE_DIR oracle directory. Check dba_directories view to get the physical path of oracle directory. 
--                         (If you want to use any other oracle directory other than APPS_DATA_FILE_DIR then replace APPS_DATA_FILE_DIR with your oracle directory name below.)
--                      2. Make sure XXEIS schema has read/write permissions to above used oracle directory and give full permissions to the directory at OS level.
--                      3. Run this file in XXEIS user from SQL*Plus or any tool like Toad, SQL Navigator. 
exec xxeis.eis_rsc_ins.g_rtf_dir := 'APPS_DATA_FILE_DIR' ;
prompt Creating Object Data EIS_XXWC_PO_RCV_DETAILS_V
set scan off define off
DECLARE
mod_exist VARCHAR2(1);
BEGIN 
mod_exist := XXEIS.EIS_RSC_INSERT.check_if_module_exist(201);
IF mod_exist = 'Y' THEN 
--Inserting Object EIS_XXWC_PO_RCV_DETAILS_V
xxeis.eis_rsc_ins.v( 'EIS_XXWC_PO_RCV_DETAILS_V',201,'','','','','ANONYMOUS','XXEIS','Eis Xxwc Po Rcv Details V','EXPRDV','','','VIEW','US','','');
--Delete Object Columns for EIS_XXWC_PO_RCV_DETAILS_V
xxeis.eis_rsc_utility.delete_view_rows('EIS_XXWC_PO_RCV_DETAILS_V',201,FALSE);
--Inserting Object Columns for EIS_XXWC_PO_RCV_DETAILS_V
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_RCV_DETAILS_V','TX_TYPE',201,'Tx Type','TX_TYPE','','','','ANONYMOUS','VARCHAR2','','','Tx Type','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_RCV_DETAILS_V','UOM',201,'Uom','UOM','','','','ANONYMOUS','VARCHAR2','','','Uom','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_RCV_DETAILS_V','RCV_SHIPMENT_NUM',201,'Rcv Shipment Num','RCV_SHIPMENT_NUM','','','','ANONYMOUS','VARCHAR2','','','Rcv Shipment Num','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_RCV_DETAILS_V','DESTINATION_SUBINVENTORY',201,'Destination Subinventory','DESTINATION_SUBINVENTORY','','','','ANONYMOUS','VARCHAR2','','','Destination Subinventory','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_RCV_DETAILS_V','DESTINATION_TYPE',201,'Destination Type','DESTINATION_TYPE','','','','ANONYMOUS','VARCHAR2','','','Destination Type','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_RCV_DETAILS_V','DOC_NUM',201,'Doc Num','DOC_NUM','','','','ANONYMOUS','VARCHAR2','','','Doc Num','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_RCV_DETAILS_V','DOC_TYPE',201,'Doc Type','DOC_TYPE','','','','ANONYMOUS','VARCHAR2','','','Doc Type','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_RCV_DETAILS_V','EXCEPTION',201,'Exception','EXCEPTION','','','','ANONYMOUS','VARCHAR2','','','Exception','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_RCV_DETAILS_V','ITEM',201,'Item','ITEM','','','','ANONYMOUS','VARCHAR2','','','Item','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_RCV_DETAILS_V','LOCATOR',201,'Locator','LOCATOR','','','','ANONYMOUS','VARCHAR2','','','Locator','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_RCV_DETAILS_V','ORG',201,'Org','ORG','','','','ANONYMOUS','VARCHAR2','','','Org','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_RCV_DETAILS_V','PAR_TX_TYPE',201,'Par Tx Type','PAR_TX_TYPE','','','','ANONYMOUS','VARCHAR2','','','Par Tx Type','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_RCV_DETAILS_V','PRICE',201,'Price','PRICE','','~T~D~2','','ANONYMOUS','NUMBER','','','Price','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_RCV_DETAILS_V','QTY',201,'Qty','QTY','','~T~D~2','','ANONYMOUS','NUMBER','','','Qty','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_RCV_DETAILS_V','Q_CODE',201,'Q Code','Q_CODE','','','','ANONYMOUS','VARCHAR2','','','Q Code','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_RCV_DETAILS_V','RECEIPT_NUM',201,'Receipt Num','RECEIPT_NUM','','','','ANONYMOUS','VARCHAR2','','','Receipt Num','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_RCV_DETAILS_V','RECEIVER',201,'Receiver','RECEIVER','','','','ANONYMOUS','VARCHAR2','','','Receiver','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_RCV_DETAILS_V','RCV_SL_QUANTITY_SHIPPED',201,'Rcv Sl Quantity Shipped','RCV_SL_QUANTITY_SHIPPED','','~T~D~2','','ANONYMOUS','NUMBER','','','Rcv Sl Quantity Shipped','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_RCV_DETAILS_V','RCV_SL_QUANTITY_RECEIVED',201,'Rcv Sl Quantity Received','RCV_SL_QUANTITY_RECEIVED','','~T~D~2','','ANONYMOUS','NUMBER','','','Rcv Sl Quantity Received','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_RCV_DETAILS_V','RCV_SL_UNIT_OF_MEASURE',201,'Rcv Sl Unit Of Measure','RCV_SL_UNIT_OF_MEASURE','','','','ANONYMOUS','VARCHAR2','','','Rcv Sl Unit Of Measure','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_RCV_DETAILS_V','BUYER_PREPARER',201,'Buyer Preparer','BUYER_PREPARER','','','','ANONYMOUS','VARCHAR2','','','Buyer Preparer','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_RCV_DETAILS_V','CATEGORY',201,'Category','CATEGORY','','','','ANONYMOUS','VARCHAR2','','','Category','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_RCV_DETAILS_V','COMPANY',201,'Company','COMPANY','','','','ANONYMOUS','VARCHAR2','','','Company','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_RCV_DETAILS_V','DELIVER_TO_LOCATION',201,'Deliver To Location','DELIVER_TO_LOCATION','','','','ANONYMOUS','VARCHAR2','','','Deliver To Location','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_RCV_DETAILS_V','DELIVER_TO_PERSON',201,'Deliver To Person','DELIVER_TO_PERSON','','','','ANONYMOUS','VARCHAR2','','','Deliver To Person','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_RCV_DETAILS_V','DES',201,'Des','DES','','','','ANONYMOUS','VARCHAR2','','','Des','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_RCV_DETAILS_V','SRC',201,'Src','SRC','','','','ANONYMOUS','VARCHAR2','','','Src','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_RCV_DETAILS_V','SRC_TYPE',201,'Src Type','SRC_TYPE','','','','ANONYMOUS','VARCHAR2','','','Src Type','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_RCV_DETAILS_V','TX_DATE',201,'Tx Date','TX_DATE','','','','ANONYMOUS','DATE','','','Tx Date','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_RCV_DETAILS_V','TX_REASON',201,'Tx Reason','TX_REASON','','','','ANONYMOUS','VARCHAR2','','','Tx Reason','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_RCV_DETAILS_V','ON_HAND_QTY',201,'On Hand Qty','ON_HAND_QTY','','~T~D~2','','ANONYMOUS','NUMBER','','','On Hand Qty','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_RCV_DETAILS_V','UNIT_WEIGHT',201,'Unit Weight','UNIT_WEIGHT','','~T~D~2','','ANONYMOUS','NUMBER','','','Unit Weight','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_RCV_DETAILS_V','ORGANIZATION_CODE',201,'Organization Code','ORGANIZATION_CODE','','','','ANONYMOUS','VARCHAR2','','','Organization Code','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_RCV_DETAILS_V','LS_ORGANIZATION_ID',201,'Ls Organization Id','LS_ORGANIZATION_ID','','','','ANONYMOUS','NUMBER','','','Ls Organization Id','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_RCV_DETAILS_V','LS_SHIPMENT_LINE_ID',201,'Ls Shipment Line Id','LS_SHIPMENT_LINE_ID','','','','ANONYMOUS','NUMBER','','','Ls Shipment Line Id','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_RCV_DETAILS_V','LS_TRANSACTION_ID',201,'Ls Transaction Id','LS_TRANSACTION_ID','','','','ANONYMOUS','NUMBER','','','Ls Transaction Id','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_RCV_DETAILS_V','LS_ITEM_ID',201,'Ls Item Id','LS_ITEM_ID','','','','ANONYMOUS','NUMBER','','','Ls Item Id','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_RCV_DETAILS_V','CREATION_DATE',201,'Creation Date','CREATION_DATE','','','','ANONYMOUS','DATE','','','Creation Date','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_RCV_DETAILS_V','PCKING_SLIP',201,'Pcking Slip','PCKING_SLIP','','','','ANONYMOUS','VARCHAR2','','','Pcking Slip','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_RCV_DETAILS_V','REV',201,'Rev','REV','','','','ANONYMOUS','VARCHAR2','','','Rev','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_RCV_DETAILS_V','TX_ID',201,'Tx Id','TX_ID','','','','ANONYMOUS','NUMBER','','','Tx Id','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_RCV_DETAILS_V','PROJECT_ID',201,'Project Id','PROJECT_ID','','','','ANONYMOUS','VARCHAR2','','','Project Id','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_RCV_DETAILS_V','PROJECT_NAME',201,'Project Name','PROJECT_NAME','','','','ANONYMOUS','VARCHAR2','','','Project Name','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_RCV_DETAILS_V','PROJETC_NUMBER',201,'Projetc Number','PROJETC_NUMBER','','','','ANONYMOUS','VARCHAR2','','','Projetc Number','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_RCV_DETAILS_V','PROCESS_ID',201,'Process Id','PROCESS_ID','','','','ANONYMOUS','NUMBER','','','Process Id','','','','US');
--Inserting Object Components for EIS_XXWC_PO_RCV_DETAILS_V
xxeis.eis_rsc_ins.vcomp( 'EIS_XXWC_PO_RCV_DETAILS_V','HR_ORGANIZATION_UNITS',201,'HR_ALL_ORGANIZATION_UNITS','HRU','HRU','ANONYMOUS','ANONYMOUS','-1','HR_ALL_ORGANIZATION_UNITS','N','','','','','',' ',' ',' ',' ',' ');
xxeis.eis_rsc_ins.vcomp( 'EIS_XXWC_PO_RCV_DETAILS_V','RCV_TRANSACTIONS',201,'RCV_TRANSACTIONS','PAR','PAR','ANONYMOUS','ANONYMOUS','-1','Receiving Transactions','','','','','','',' ',' ',' ',' ',' ');
xxeis.eis_rsc_ins.vcomp( 'EIS_XXWC_PO_RCV_DETAILS_V','RCV_SHIPMENT_LINES',201,'RCV_SHIPMENT_LINES','RSL','RSL','ANONYMOUS','ANONYMOUS','-1','Receiving Shipment Line Information','','','','','','',' ',' ',' ',' ',' ');
--Inserting Object Component Joins for EIS_XXWC_PO_RCV_DETAILS_V
xxeis.eis_rsc_ins.vcj( 'EIS_XXWC_PO_RCV_DETAILS_V','HR_ORGANIZATION_UNITS','HRU',201,'EPRDV1.LS_ORGANIZATION_ID','=','HRU.ORGANIZATION_ID(+)','','','','Y','ANONYMOUS');
xxeis.eis_rsc_ins.vcj( 'EIS_XXWC_PO_RCV_DETAILS_V','RCV_TRANSACTIONS','PAR',201,'EPRDV1.LS_TRANSACTION_ID','=','PAR.TRANSACTION_ID(+)','','','','Y','ANONYMOUS');
xxeis.eis_rsc_ins.vcj( 'EIS_XXWC_PO_RCV_DETAILS_V','RCV_SHIPMENT_LINES','RSL',201,'EPRDV1.LS_SHIPMENT_LINE_ID','=','RSL.SHIPMENT_LINE_ID(+)','','','','Y','ANONYMOUS');
ELSE
RAISE_APPLICATION_ERROR (-20001,'Module does not exist. Import/Create the module with id 201');
END IF;
END;
/
prompt Creating Report LOV Data for Receipt Details-WC
set scan off define off
DECLARE
mod_exist VARCHAR2(1);
BEGIN 
mod_exist := XXEIS.EIS_RSC_INSERT.check_if_module_exist(201);
IF mod_exist = 'Y' THEN 
--Inserting Report LOVs - Receipt Details-WC
xxeis.eis_rsc_ins.lov( 201,'select agent_name buyer_name from po_agents_v','','EIS_PO_BUYER_LOV','List of Values for Buyer','XXEIS_RS_ADMIN',NULL,'','','','','','','','US');
xxeis.eis_rsc_ins.lov( 201,'SELECT DISTINCT rsh.RECEIPT_NUM,HOU.name OPERATING_UNIT FROM RCV_SHIPMENT_HEADERS rsh,ORG_ORGANIZATION_DEFINITIONS OOD,
HR_OPERATING_UNITS HOU
WHERE OOD.OPERATING_UNIT=HOU.ORGANIZATION_ID(+)
AND OOD.ORGANIZATION_ID(+)=RSH.ORGANIZATION_ID
AND EXISTS(SELECT 1 FROM XXEIS.EIS_MO_ORG_TMP_V
            WHERE ORG_ID=HOU.ORGANIZATION_ID)
order by receipt_num','','EIS_PO_RECEIPT_NUM_LOV','List of values for PO Receipt Number(s)','XXEIS_RS_ADMIN',NULL,'N','','','','','','','US');
xxeis.eis_rsc_ins.lov( 201,'select distinct H.FULL_NAME,HOU.name OPERATING_UNIT
from RCV_TRANSACTIONS R, HR_EMPLOYEES H,ORG_ORGANIZATION_DEFINITIONS OOD,
HR_OPERATING_UNITS HOU
where OOD.OPERATING_UNIT=HOU.ORGANIZATION_ID(+)
and OOD.ORGANIZATION_ID(+)=R.ORGANIZATION_ID
and R.EMPLOYEE_ID = H.EMPLOYEE_ID (+)
and exists(select 1 from XXEIS.EIS_MO_ORG_TMP_V
            WHERE ORG_ID=HOU.ORGANIZATION_ID)','','EIS_PO_RECEIVER_LOV','List of values for PO Receiver','XXEIS_RS_ADMIN',NULL,'N','','','','','','','US');
xxeis.eis_rsc_ins.lov( 201,'SELECT lookup_code, meaning FROM apps.mfg_lookups
WHERE LOOKUP_TYPE = ''INV_SRS_PRECISION''
and enabled_flag = ''Y''
order by LOOKUP_CODE','','EIS_PO_NUMBER_PRECISION_LOV','','XXEIS_RS_ADMIN',NULL,'','','','','','','','US');
xxeis.eis_rsc_ins.lov( 201,'select  distinct TRIM((SUBSTR(LOCATION_CODE,1,INSTR(LOCATION_CODE,''-'',1,1)-1))) deliver_location
 from HR_LOCATIONS','','Delivery Location','','ANONYMOUS',NULL,'N','','','','','','','US');
xxeis.eis_rsc_ins.lov( 201,'SELECT ood.organization_code organization_code,ood.organization_name organization_name  FROM ORG_ORGANIZATION_DEFINITIONS OOD WHERE EXISTS(SELECT 1 FROM XXEIS.EIS_ORG_ACCESS_V WHERE organization_id = ood.organization_id) ORDER BY organization_code','','PO Organization Code LOV','','SA059956',NULL,'N','','','N','N','','','US');
null;
ELSE
RAISE_APPLICATION_ERROR (-20001,'Module does not exist. Import/Create the module with id 201');
END IF;
END;
/
prompt Creating Report Data for Receipt Details-WC
set scan off define off
DECLARE
mod_exist varchar2(1);
BEGIN 
mod_exist := XXEIS.EIS_RSC_INSERT.check_if_module_exist(201);
IF mod_exist = 'Y' THEN 
--Deleting Report data - Receipt Details-WC
xxeis.eis_rsc_utility.delete_report_rows( 'Receipt Details-WC' );
--Inserting Report - Receipt Details-WC
xxeis.eis_rsc_ins.r( 201,'Receipt Details-WC','','This report lists receiving details and summary for a given receipt date Range, supplier range, purchase order range, receipt number range or by receiver. This report can be run for day to day operations by either the purchasing manager, plant managers or other authorized users for planning and execution purposes.','','','','MR020532','EIS_XXWC_PO_RCV_DETAILS_V','Y','','','MR020532','','N','White Cap Reports','','CSV,Pivot Excel,EXCEL,','N','','','','','','','','US','','','','');
--Inserting Report Columns - Receipt Details-WC
xxeis.eis_rsc_ins.rc( 'Receipt Details-WC',201,'BUYER_PREPARER','Buyer Preparer','Buyer Preparer','','','default','','10','N','','','','','','','','MR020532','N','N','','EIS_XXWC_PO_RCV_DETAILS_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'Receipt Details-WC',201,'CATEGORY','Category','Category','','','default','','11','N','','','','','','','','MR020532','N','N','','EIS_XXWC_PO_RCV_DETAILS_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'Receipt Details-WC',201,'COMPANY','Company','Company','','','default','','12','N','','','','','','','','MR020532','N','N','','EIS_XXWC_PO_RCV_DETAILS_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'Receipt Details-WC',201,'DELIVER_TO_LOCATION','Deliver To Location','Deliver To Location','','','default','','13','N','','','','','','','','MR020532','N','N','','EIS_XXWC_PO_RCV_DETAILS_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'Receipt Details-WC',201,'DELIVER_TO_PERSON','Deliver To Person','Deliver To Person','','','default','','14','N','','','','','','','','MR020532','N','N','','EIS_XXWC_PO_RCV_DETAILS_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'Receipt Details-WC',201,'DES','Des','Des','','','default','','15','N','','','','','','','','MR020532','N','N','','EIS_XXWC_PO_RCV_DETAILS_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'Receipt Details-WC',201,'DESTINATION_SUBINVENTORY','Destination Subinventory','Destination Subinventory','','','default','','16','N','','','','','','','','MR020532','N','N','','EIS_XXWC_PO_RCV_DETAILS_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'Receipt Details-WC',201,'DESTINATION_TYPE','Destination Type','Destination Type','','','default','','17','N','','','','','','','','MR020532','N','N','','EIS_XXWC_PO_RCV_DETAILS_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'Receipt Details-WC',201,'DOC_NUM','Po Number/Line','Doc Num','','','default','','18','N','','','','','','','','MR020532','N','N','','EIS_XXWC_PO_RCV_DETAILS_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'Receipt Details-WC',201,'DOC_TYPE','Doc Type','Doc Type','','','default','','19','N','','','','','','','','MR020532','N','N','','EIS_XXWC_PO_RCV_DETAILS_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'Receipt Details-WC',201,'EXCEPTION','Exception','Exception','','','default','','20','N','','','','','','','','MR020532','N','N','','EIS_XXWC_PO_RCV_DETAILS_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'Receipt Details-WC',201,'ITEM','Item','Item','','','default','','3','N','','','','','','','','MR020532','N','N','','EIS_XXWC_PO_RCV_DETAILS_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'Receipt Details-WC',201,'LOCATOR','Locator','Locator','','','default','','21','N','','','','','','','','MR020532','N','N','','EIS_XXWC_PO_RCV_DETAILS_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'Receipt Details-WC',201,'ON_HAND_QTY','On Hand Qty','On Hand Qty','','~T~D~0','default','','4','N','','','','','','','','MR020532','N','N','','EIS_XXWC_PO_RCV_DETAILS_V','','','SUM','US','');
xxeis.eis_rsc_ins.rc( 'Receipt Details-WC',201,'ORG','Org','Org','','','default','','22','N','','','','','','','','MR020532','N','N','','EIS_XXWC_PO_RCV_DETAILS_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'Receipt Details-WC',201,'PAR_TX_TYPE','Par Tx Type','Par Tx Type','','','default','','32','N','','','','','','','','MR020532','N','N','','EIS_XXWC_PO_RCV_DETAILS_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'Receipt Details-WC',201,'PRICE','Price','Price','','~,~.~2','default','','23','N','','','','','','','','MR020532','N','N','','EIS_XXWC_PO_RCV_DETAILS_V','','','SUM','US','');
xxeis.eis_rsc_ins.rc( 'Receipt Details-WC',201,'QTY','Qty','Qty','','~T~D~0','default','','5','N','','','','','','','','MR020532','N','N','','EIS_XXWC_PO_RCV_DETAILS_V','','','SUM','US','');
xxeis.eis_rsc_ins.rc( 'Receipt Details-WC',201,'Q_CODE','Q Code','Q Code','','','default','','24','N','','','','','','','','MR020532','N','N','','EIS_XXWC_PO_RCV_DETAILS_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'Receipt Details-WC',201,'RCV_SHIPMENT_NUM','Rcv Shipment Num','Rcv Shipment Num','','','default','','6','N','','','','','','','','MR020532','N','N','','EIS_XXWC_PO_RCV_DETAILS_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'Receipt Details-WC',201,'RCV_SL_QUANTITY_RECEIVED','Rcv Sl Quantity Received','Rcv Sl Quantity Received','','~T~D~0','default','','8','N','','','','','','','','MR020532','N','N','','EIS_XXWC_PO_RCV_DETAILS_V','','','SUM','US','');
xxeis.eis_rsc_ins.rc( 'Receipt Details-WC',201,'RCV_SL_QUANTITY_SHIPPED','Rcv Sl Quantity Shipped','Rcv Sl Quantity Shipped','','~T~D~0','default','','7','N','','','','','','','','MR020532','N','N','','EIS_XXWC_PO_RCV_DETAILS_V','','','SUM','US','');
xxeis.eis_rsc_ins.rc( 'Receipt Details-WC',201,'RCV_SL_UNIT_OF_MEASURE','Rcv Sl Unit Of Measure','Rcv Sl Unit Of Measure','','','default','','9','N','','','','','','','','MR020532','N','N','','EIS_XXWC_PO_RCV_DETAILS_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'Receipt Details-WC',201,'RECEIPT_NUM','Receipt Num','Receipt Num','','','default','','25','N','','','','','','','','MR020532','N','N','','EIS_XXWC_PO_RCV_DETAILS_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'Receipt Details-WC',201,'RECEIVER','Receiver','Receiver','','','default','','26','N','','','','','','','','MR020532','N','N','','EIS_XXWC_PO_RCV_DETAILS_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'Receipt Details-WC',201,'SRC','Src','Src','','','default','','1','N','','','','','','','','MR020532','N','N','','EIS_XXWC_PO_RCV_DETAILS_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'Receipt Details-WC',201,'SRC_TYPE','Src Type','Src Type','','','default','','2','N','','','','','','','','MR020532','N','N','','EIS_XXWC_PO_RCV_DETAILS_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'Receipt Details-WC',201,'TX_DATE','Tx Date','Tx Date','','','default','','27','N','','','','','','','','MR020532','N','N','','EIS_XXWC_PO_RCV_DETAILS_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'Receipt Details-WC',201,'TX_REASON','Tx Reason','Tx Reason','','','default','','28','N','','','','','','','','MR020532','N','N','','EIS_XXWC_PO_RCV_DETAILS_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'Receipt Details-WC',201,'TX_TYPE','Tx Type','Tx Type','','','default','','29','N','','','','','','','','MR020532','N','N','','EIS_XXWC_PO_RCV_DETAILS_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'Receipt Details-WC',201,'UNIT_WEIGHT','Unit Weight','Unit Weight','','~T~D~0','default','','31','N','','','','','','','','MR020532','N','N','','EIS_XXWC_PO_RCV_DETAILS_V','','','SUM','US','');
xxeis.eis_rsc_ins.rc( 'Receipt Details-WC',201,'UOM','Uom','Uom','','','default','','30','N','','','','','','','','MR020532','N','N','','EIS_XXWC_PO_RCV_DETAILS_V','','','GROUP_BY','US','');
--Inserting Report Parameters - Receipt Details-WC
xxeis.eis_rsc_ins.rp( 'Receipt Details-WC',201,'Delivery Location','Delivery Location','DELIVER_TO_LOCATION','IN','Delivery Location','','VARCHAR2','N','Y','2','Y','Y','CONSTANT','MR020532','Y','N','','','','EIS_XXWC_PO_RCV_DETAILS_V','','','US','');
xxeis.eis_rsc_ins.rp( 'Receipt Details-WC',201,'PO Receipt Date From','PO Receipt Date From','TX_DATE','>=','','','DATE','Y','Y','3','Y','Y','CONSTANT','MR020532','Y','N','','Start Date','','EIS_XXWC_PO_RCV_DETAILS_V','','','US','');
xxeis.eis_rsc_ins.rp( 'Receipt Details-WC',201,'PO Receipt Date To','PO Receipt Date To','TX_DATE','<=','','','DATE','Y','Y','4','Y','Y','CONSTANT','MR020532','Y','N','','End Date','','EIS_XXWC_PO_RCV_DETAILS_V','','','US','');
xxeis.eis_rsc_ins.rp( 'Receipt Details-WC',201,'Receipt Number','Receipt Number','RECEIPT_NUM','IN','EIS_PO_RECEIPT_NUM_LOV','','VARCHAR2','N','Y','5','Y','Y','CONSTANT','MR020532','Y','N','','','','EIS_XXWC_PO_RCV_DETAILS_V','','','US','');
xxeis.eis_rsc_ins.rp( 'Receipt Details-WC',201,'Receiver','Receiver','RECEIVER','IN','EIS_PO_RECEIVER_LOV','','VARCHAR2','N','Y','6','Y','Y','CONSTANT','MR020532','Y','N','','','','EIS_XXWC_PO_RCV_DETAILS_V','','','US','');
xxeis.eis_rsc_ins.rp( 'Receipt Details-WC',201,'Buyer','Buyer','BUYER_PREPARER','IN','EIS_PO_BUYER_LOV','','VARCHAR2','N','Y','7','Y','Y','CONSTANT','MR020532','Y','N','','','','EIS_XXWC_PO_RCV_DETAILS_V','','','US','');
xxeis.eis_rsc_ins.rp( 'Receipt Details-WC',201,'Precision','Precision','','IN','EIS_PO_NUMBER_PRECISION_LOV','SELECT nvl(fnd_profile.VALUE(''REPORT_QUANTITY_PRECISION''),2) FROM dual','NUMBER','Y','Y','8','Y','N','SQL','MR020532','Y','N','','','','EIS_XXWC_PO_RCV_DETAILS_V','','','US','');
xxeis.eis_rsc_ins.rp( 'Receipt Details-WC',201,'Organization','Organization','ORGANIZATION_CODE','IN','PO Organization Code LOV','','VARCHAR2','N','Y','1','Y','Y','CONSTANT','MR020532','Y','N','','','','EIS_XXWC_PO_RCV_DETAILS_V','','','US','');
--Inserting Dependent Parameters - Receipt Details-WC
--Inserting Report Conditions - Receipt Details-WC
xxeis.eis_rsc_ins.rcnh( 'Receipt Details-WC',201,'BUYER_PREPARER IN :Buyer ','SIMPLE','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','BUYER_PREPARER','','Buyer','','','','','EIS_XXWC_PO_RCV_DETAILS_V','','','','','','IN','Y','Y','','','','','1',201,'Receipt Details-WC','BUYER_PREPARER IN :Buyer ');
xxeis.eis_rsc_ins.rcnh( 'Receipt Details-WC',201,'DELIVER_TO_LOCATION IN :Delivery Location ','SIMPLE','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','DELIVER_TO_LOCATION','','Delivery Location','','','','','EIS_XXWC_PO_RCV_DETAILS_V','','','','','','IN','Y','Y','','','','','1',201,'Receipt Details-WC','DELIVER_TO_LOCATION IN :Delivery Location ');
xxeis.eis_rsc_ins.rcnh( 'Receipt Details-WC',201,'ORGANIZATION_CODE IN :Organization ','SIMPLE','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','ORGANIZATION_CODE','','Organization','','','','','EIS_XXWC_PO_RCV_DETAILS_V','','','','','','IN','Y','Y','','','','','1',201,'Receipt Details-WC','ORGANIZATION_CODE IN :Organization ');
xxeis.eis_rsc_ins.rcnh( 'Receipt Details-WC',201,'RECEIPT_NUM IN :Receipt Number ','SIMPLE','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','RECEIPT_NUM','','Receipt Number','','','','','EIS_XXWC_PO_RCV_DETAILS_V','','','','','','IN','Y','Y','','','','','1',201,'Receipt Details-WC','RECEIPT_NUM IN :Receipt Number ');
xxeis.eis_rsc_ins.rcnh( 'Receipt Details-WC',201,'RECEIVER IN :Receiver ','SIMPLE','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','RECEIVER','','Receiver','','','','','EIS_XXWC_PO_RCV_DETAILS_V','','','','','','IN','Y','Y','','','','','1',201,'Receipt Details-WC','RECEIVER IN :Receiver ');
xxeis.eis_rsc_ins.rcnh( 'Receipt Details-WC',201,'TX_DATE >= :PO Receipt Date From ','SIMPLE','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','TX_DATE','','PO Receipt Date From','','','','','EIS_XXWC_PO_RCV_DETAILS_V','','','','','','GREATER_THAN_EQUALS','Y','Y','','','','','1',201,'Receipt Details-WC','TX_DATE >= :PO Receipt Date From ');
xxeis.eis_rsc_ins.rcnh( 'Receipt Details-WC',201,'TX_DATE <= :PO Receipt Date To ','SIMPLE','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','TX_DATE','','PO Receipt Date To','','','','','EIS_XXWC_PO_RCV_DETAILS_V','','','','','','LESS_THAN_EQUALS','Y','Y','','','','','1',201,'Receipt Details-WC','TX_DATE <= :PO Receipt Date To ');
--Inserting Report Sorts - Receipt Details-WC
xxeis.eis_rsc_ins.rs( 'Receipt Details-WC',201,'SRC_TYPE','ASC','MR020532','1','');
xxeis.eis_rsc_ins.rs( 'Receipt Details-WC',201,'RECEIPT_NUM','ASC','MR020532','2','');
--Inserting Report Triggers - Receipt Details-WC
xxeis.eis_rsc_ins.rt( 'Receipt Details-WC',201,'begin
XXEIS.EIS_RS_PO_FIN_COM_UTIL_PKG.g_qty_precision := to_number(:Precision);
end;','B','Y','MR020532','');
--inserting report templates - Receipt Details-WC
xxeis.eis_rsc_ins.r_tem( 'Receipt Details-WC','PO Receiving Details','Seeded template for PO Receiving Details','','','','','','','','','','','Receipt Details-WC.rtf','MR020532','X','','','Y','Y','','');
--Inserting Report Portals - Receipt Details-WC
xxeis.eis_rsc_ins.r_port( 'Receipt Details-WC','XXWC_PUR_TOP_RPTS','201','Receipt Details-WC','Past Receipts','OA.jsp?page=/eis/oracle/apps/xxeis/central/reporting/webui/EISRSCLaunchPG&EisProduct=Reporting&Portal=Yes&mod=Purchasing','','Pivot Excel,EXCEL,','CONC','N','MR020532');
--inserting report dashboards - Receipt Details-WC
xxeis.eis_rsc_ins.R_dash( 'Receipt Details-WC','PO Receiving Details','PO Receiving Details','pie','large','Supplier Name','Supplier Name','PO Quantity Ordered','PO Quantity Ordered','Sum','MR020532');
--Exporting report views table information -  
xxeis.eis_rsc_ins.rviews( 'Receipt Details-WC','201','EIS_XXWC_PO_RCV_DETAILS_V','EIS_XXWC_PO_RCV_DETAILS_V','N','');
--inserting report security - Receipt Details-WC
xxeis.eis_rsc_ins.rsec( 'Receipt Details-WC','201','','PURCHASING_SUPER_USER',201,'MR020532','','','');
xxeis.eis_rsc_ins.rsec( 'Receipt Details-WC','201','','HDS_PRCHSNG_SPR_USR',201,'MR020532','','','');
xxeis.eis_rsc_ins.rsec( 'Receipt Details-WC','201','','XXWC_PURCHASING_BUYER',201,'MR020532','','','');
xxeis.eis_rsc_ins.rsec( 'Receipt Details-WC','201','','XXWC_PURCHASING_INQUIRY',201,'MR020532','','','');
xxeis.eis_rsc_ins.rsec( 'Receipt Details-WC','201','','XXWC_PURCHASING_MGR',201,'MR020532','','','');
xxeis.eis_rsc_ins.rsec( 'Receipt Details-WC','201','','XXWC_PURCHASING_SR_MRG_WC',201,'MR020532','','','');
xxeis.eis_rsc_ins.rsec( 'Receipt Details-WC','201','','XXWC_PUR_SUPER_USER',201,'MR020532','','','');
xxeis.eis_rsc_ins.rsec( 'Receipt Details-WC','200','','XXWC_PAY_NO_CALENDAR',201,'MR020532','','','');
xxeis.eis_rsc_ins.rsec( 'Receipt Details-WC','200','','XXWC_PAY_W_CALENDAR',201,'MR020532','','','');
xxeis.eis_rsc_ins.rsec( 'Receipt Details-WC','200','','HDS_PAYABLES_CLOSE_GLBL',201,'MR020532','','','');
xxeis.eis_rsc_ins.rsec( 'Receipt Details-WC','200','','XXWC_PAY_DISBURSE',201,'MR020532','','','');
xxeis.eis_rsc_ins.rsec( 'Receipt Details-WC','200','','XXWC_PAYABLES_INQUIRY',201,'MR020532','','','');
xxeis.eis_rsc_ins.rsec( 'Receipt Details-WC','200','','HDS_AP_MGR_NOSUP_US_GSC',201,'MR020532','','','');
xxeis.eis_rsc_ins.rsec( 'Receipt Details-WC','200','','HDS_AP_MGR_NOSUP_US_IWO',201,'MR020532','','','');
xxeis.eis_rsc_ins.rsec( 'Receipt Details-WC','200','','HDS_AP_MANAGER',201,'MR020532','','','');
xxeis.eis_rsc_ins.rsec( 'Receipt Details-WC','200','','HDS_PYABLS_MNGR_CAN',201,'MR020532','','','');
xxeis.eis_rsc_ins.rsec( 'Receipt Details-WC','200','','HDS_PYBLS_MNGR',201,'MR020532','','','');
xxeis.eis_rsc_ins.rsec( 'Receipt Details-WC','200','','XXWC_PAY_MANAGER',201,'MR020532','','','');
xxeis.eis_rsc_ins.rsec( 'Receipt Details-WC','200','','XXWC_PAY_VENDOR_BANK_DETAILS',201,'MR020532','','','');
xxeis.eis_rsc_ins.rsec( 'Receipt Details-WC','200','','XXWC_PAY_VENDOR_MSTR',201,'MR020532','','','');
xxeis.eis_rsc_ins.rsec( 'Receipt Details-WC','200','','XXWC_PAY_VENDOR_MSTR_INQUIRY',201,'MR020532','','','');
xxeis.eis_rsc_ins.rsec( 'Receipt Details-WC','20005','','XXWC_VIEW_ALL_EIS_REPORTS',201,'MR020532','','','');
xxeis.eis_rsc_ins.rsec( 'Receipt Details-WC','401','','XXWC_INVENTORY_SUPER_USER',201,'MR020532','','','');
xxeis.eis_rsc_ins.rsec( 'Receipt Details-WC','401','','XXWC_INVENTORY_SPEC_SCC',201,'MR020532','','','');
xxeis.eis_rsc_ins.rsec( 'Receipt Details-WC','401','','XXWC_INV_PLANNER',201,'MR020532','','','');
xxeis.eis_rsc_ins.rsec( 'Receipt Details-WC','401','','XXWC_INVENTORY_CONTROL_INQUIRY',201,'MR020532','','','');
xxeis.eis_rsc_ins.rsec( 'Receipt Details-WC','401','','XXWC_INVENTORY_CONTROL_SR_MGR',201,'MR020532','','','');
xxeis.eis_rsc_ins.rsec( 'Receipt Details-WC','401','','XXWC_INV_ACCOUNTANT',201,'MR020532','','','');
xxeis.eis_rsc_ins.rsec( 'Receipt Details-WC','401','','XXWC_AO_BIN_MTN_CYCLE_INV_ADJ',201,'MR020532','','','');
xxeis.eis_rsc_ins.rsec( 'Receipt Details-WC','201','','XXWC_PURCHASING_RPM',201,'MR020532','','','');
xxeis.eis_rsc_ins.rsec( 'Receipt Details-WC','401','','XXWC_RECEIVING_ASSOCIATE',201,'MR020532','','','');
xxeis.eis_rsc_ins.rsec( 'Receipt Details-WC','401','','XXWC_AO_REC',201,'MR020532','','','');
xxeis.eis_rsc_ins.rsec( 'Receipt Details-WC','660','','XXWC_AO_OEENTRY_REC',201,'MR020532','','','');
xxeis.eis_rsc_ins.rsec( 'Receipt Details-WC','401','','XXWC_AO_INV_ADJ_REC',201,'MR020532','','','');
xxeis.eis_rsc_ins.rsec( 'Receipt Details-WC','401','','XXWC_AO_BIN_MTN_CYCLE_REC',201,'MR020532','','','');
xxeis.eis_rsc_ins.rsec( 'Receipt Details-WC','401','','XXWC_AO_BIN_MTN_REC',201,'MR020532','','','');
--Inserting Report Pivots - Receipt Details-WC
xxeis.eis_rsc_ins.rpivot( 'Receipt Details-WC',201,'Pivot','1','0,0|1,1,0','1,1,1,1|None|2');
--Inserting Report Pivot Details For Pivot - Pivot
xxeis.eis_rsc_ins.rpivot_dtls( 'Receipt Details-WC',201,'Pivot','TX_DATE','PAGE_FIELD','','','1','','');
xxeis.eis_rsc_ins.rpivot_dtls( 'Receipt Details-WC',201,'Pivot','DOC_TYPE','PAGE_FIELD','','','2','','');
xxeis.eis_rsc_ins.rpivot_dtls( 'Receipt Details-WC',201,'Pivot','DESTINATION_TYPE','PAGE_FIELD','','','3','','');
xxeis.eis_rsc_ins.rpivot_dtls( 'Receipt Details-WC',201,'Pivot','BUYER_PREPARER','PAGE_FIELD','','','4','','');
xxeis.eis_rsc_ins.rpivot_dtls( 'Receipt Details-WC',201,'Pivot','RECEIPT_NUM','PAGE_FIELD','','','5','','');
xxeis.eis_rsc_ins.rpivot_dtls( 'Receipt Details-WC',201,'Pivot','QTY','DATA_FIELD','SUM','','1','','xlNormal');
xxeis.eis_rsc_ins.rpivot_dtls( 'Receipt Details-WC',201,'Pivot','RECEIVER','ROW_FIELD','','','2','1','');
xxeis.eis_rsc_ins.rpivot_dtls( 'Receipt Details-WC',201,'Pivot','SRC','ROW_FIELD','','','3','1','');
xxeis.eis_rsc_ins.rpivot_dtls( 'Receipt Details-WC',201,'Pivot','DOC_NUM','ROW_FIELD','','','4','1','');
xxeis.eis_rsc_ins.rpivot_dtls( 'Receipt Details-WC',201,'Pivot','ITEM','ROW_FIELD','','','5','1','');
xxeis.eis_rsc_ins.rpivot_dtls( 'Receipt Details-WC',201,'Pivot','DES','ROW_FIELD','','','6','1','');
xxeis.eis_rsc_ins.rpivot_dtls( 'Receipt Details-WC',201,'Pivot','SRC_TYPE','ROW_FIELD','','','1','','');
--Inserting Report Summary Calculation Columns For Pivot- Pivot
--Inserting Report   Version details- Receipt Details-WC
xxeis.eis_rsc_ins.rv( 'Receipt Details-WC','','Receipt Details-WC','AB063501');
ELSE
RAISE_APPLICATION_ERROR (-20001,'Module does not exist. Import/Create the module with id 201');
END IF;
END;
/
