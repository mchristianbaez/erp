/*************************************************************************
  $Header TMS_20161208-00321_DataFix.sql $
  Module Name: TMS_20161208-00321_DataFix  Data Fix script for TMS# 20161208-00321

  PURPOSE: Data Fix script for TMS# 20161208-00321

  REVISIONS:
  Ver        Date         Author                Description
  ---------  -----------  ------------------    --------------------------
  1.0        30-SEP-2016  Gopi Damuluri         TMS#20161208-00321 
**************************************************************************/ 
SET SERVEROUTPUT ON SIZE 1000000
DECLARE
BEGIN
   DBMS_OUTPUT.put_line ('TMS: 20161208-00321    , Before Update');

   UPDATE apps.hz_cust_acct_sites_all
      SET attribute1 = '1'
    WHERE attribute1 IS NULL
      AND XXWC_QP_ROUTINES_PKG.IS_ROW_LOCKED(rowid, 'HZ_CUST_ACCT_SITES_ALL') = 'N';

   DBMS_OUTPUT.put_line ('TMS: 20161208-00321  # of Customer Sites updated (Expected:109831): '|| SQL%ROWCOUNT);

   COMMIT;

   DBMS_OUTPUT.put_line ('TMS: 20161208-00321    , End Update');
EXCEPTION
   WHEN OTHERS
   THEN
      ROLLBACK;
      DBMS_OUTPUT.put_line ('TMS: 20161208-00321 , Errors : ' || SQLERRM);
END;
/