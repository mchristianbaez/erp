--RR10825    I returned 5 of 31 

declare
   CURSOR lines is
   SELECT line_id
   FROM oe_order_lines_all
   WHERE line_id=46984257
    AND open_flag='Y'
    AND flow_status_code='AWAITING_RETURN'
    AND Nvl(shipped_quantity,0)=0;

   l_result      VARCHAR2(30);
   l_activity_id  NUMBER;
   l_file_val  varchar2(500);

 BEGIN

 oe_debug_pub.debug_on;
 oe_debug_pub.initialize;
 l_file_val:=OE_DEBUG_PUB.Set_Debug_Mode('FILE');
 oe_Debug_pub.setdebuglevel(5);
 dbms_output.put_line(' Inside the script');
 dbms_output.put_line('file path is:'||l_file_val);

 FOR i IN lines LOOP
    OE_Standard_WF.OEOL_SELECTOR
    (p_itemtype => 'OEOL'
    ,p_itemkey => to_char(i.line_id)
    ,p_actid => 12345
    ,p_funcmode => 'SET_CTX'
    ,p_result => l_result
    );
  oe_debug_pub.add('Retrun value of OEOL_selector function is'||l_result);
  UPDATE oe_order_lines_all
  SET ordered_quantity =32
  WHERE line_id = i.line_id;

  UPDATE oe_order_lines_all
  SET  shipped_quantity=ordered_quantity,fulfilled_quantity=ordered_quantity,flow_status_code='AWAITING_FULFILLMENT',
      last_update_date=SYSDATE,last_updated_by=-16516164
  WHERE line_id=i.line_id
   AND flow_status_code='AWAITING_RETURN';

    wf_engine.handleError('OEOL', to_char(i.line_id),'RMA_WAIT_FOR_RECEIVING','SKIP','COMPLETE');

    wf_engine.handleError('OEOL', to_char(i.line_id),'RMA_WAIT_FOR_INSPECTION','SKIP','COMPLETE');

    END LOOP;
oe_debug_pub.debug_off;

UPDATE oe_order_lines_all
   SET UNIT_LIST_PRICE = 0,
       UNIT_LIST_PRICE_PER_PQTY = NULL,
       link_to_line_id = 42632774,
       RETURN_ATTRIBUTE1 = 25824744,
       return_attribute2 = 42632774
 WHERE line_id = 57783490;

COMMIT;

end;
/