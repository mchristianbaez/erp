--TMS#20151026-00089 Maha 10/26/2015
UPDATE taxware.taxstprod
  SET maxcuramt='1.2'
 WHERE prodcode='75316' 
  AND  stcode='05' 
  AND  maxcuramt='2.4'
/
COMMIT
/