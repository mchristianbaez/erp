CREATE OR REPLACE FUNCTION XXWC_VENDOR_PRODUCT_NUM_FUNC (
   p_vendor_name         IN VARCHAR2,
   p_inventory_item_id   IN NUMBER)
   RETURN VARCHAR2
/*************************************************************************************************************************
Function: XXWC_VENDOR_PRODUCT_NUM_FUNC
Description: Function to cross reference from item cross references
HISTORY
==========================================================================================================================
VERSION DATE               AUTHOR(S)       DESCRIPTION
------- -----------------  --------------- -------------------------------------------------------------------------------
1.0     30-Oct-2017        P.Vamshidhar    Initial version TMS#20160921-00270 
                                           BPA Enhancements Mfg Part Number Addition
*************************************************************************************************************************/
IS
   lvc_prod_num   mtl_cross_references.cross_reference%TYPE;
BEGIN
   SELECT cross_reference
     INTO lvc_prod_num
     FROM apps.mtl_cross_references mcr, apps.ap_suppliers aps
    WHERE     mcr.inventory_item_id = P_INVENTORY_ITEM_ID
          AND mcr.cross_reference_type = 'VENDOR'
          AND mcr.attribute1 = aps.segment1
          AND aps.vendor_name = P_VENDOR_NAME
          AND mcr.attribute1 IS NOT NULL
          AND ROWNUM = 1;

   RETURN lvc_prod_num;
EXCEPTION
   WHEN OTHERS
   THEN
      BEGIN
         SELECT cross_reference
           INTO lvc_prod_num
           FROM apps.mtl_cross_references mcr
          WHERE     mcr.inventory_item_id = p_inventory_item_id
                AND mcr.cross_reference_type = 'VENDOR'
                AND ROWNUM = 1;
      EXCEPTION
         WHEN OTHERS
         THEN
            lvc_prod_num := NULL;
      END;
      RETURN lvc_prod_num;
END;
/