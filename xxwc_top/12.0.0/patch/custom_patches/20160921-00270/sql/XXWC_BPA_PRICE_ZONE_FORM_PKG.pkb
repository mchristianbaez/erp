CREATE OR REPLACE PACKAGE BODY APPS.XXWC_BPA_PRICE_ZONE_FORM_PKG
IS
   /***************************************************************************************************************************
   -- File Name: APPS.XXWC_BPA_PRICE_ZONE_FORM_PKG.pkb
   --
   -- PROGRAM TYPE: PL/SQL Script   <API>
   --
   -- PURPOSE: Package is to populate data into Global temp table XXWC_BPA_PRICE_ZONE_V_TEMP_TBL
   --          and XXWC.XXWC_BPA_PRC_ZONE_WEBADI_TMP_T
   -- HISTORY
   -- =========================================================================================================================
   -- =========================================================================================================================
   -- VERSION DATE          AUTHOR(S)       DESCRIPTION
   -- ------- -----------   --------------- -----------------------------------------------------------------------------------
   -- 1.0     01-Apr-2015   P.Vamshidhar    Created this package.
   --                                       TMS#20150414-00173-2015 PLM - Implement Cost Management tool enhancements
   --                                       package created to populate data into global temp XXWC_BPA_PRICE_ZONE_V_TEMP_TBL
   --                                       and XXWC.XXWC_BPA_PRC_ZONE_WEBADI_TMP_T tables for supplier and supplier list.
   --                                       Tables will be used in BPA_PRICE_ZONE form and PRICE ZONE webadi.
   -- 1.1     21-Sep-2015   Manjula C       TMS# 20150903-00018 - BPA- Cost Management issues
   -- 1.2     29-Sep-2017   Nancy Pahwa   TMS#20160921-00270 - BPA Enhancements Mfg Part Number Addition
   ***************************************************************************************************************************/

   /***************************************************************************************************************************
   -- PROGRAM TYPE: PL/SQL Script   <API>
   --
   -- PURPOSE: procedure XXWC_BPA_ZONE_TEMP_TBL_PRC to populate date into global temp table XXWC.XXWC_BPA_PRICE_ZONE_V_TEMP_TBL
   -- HISTORY
   -- =========================================================================================================================
   -- =========================================================================================================================
   -- VERSION DATE          AUTHOR(S)       DESCRIPTION
   -- ------- -----------   --------------- -----------------------------------------------------------------------------------
   -- 1.0     01-Apr-2015   P.Vamshidhar    Created this procedure.
                                            TMS#20150414-00173-2015 PLM - Implement Cost Management tool enhancements
                                            package created to populate data into global temp
                                            table for supplier.
   ***************************************************************************************************************************/

   PROCEDURE XXWC_BPA_ZONE_TEMP_TBL_PRC (p_vendor_id        IN     NUMBER,
                                         p_vendor_site_id   IN     NUMBER,
                                         p_po_number        IN     VARCHAR2,
                                         x_err_message         OUT VARCHAR2)
   /***************************************************************************************************************************
   -- PROGRAM TYPE: PL/SQL Script   <API>
   --
   -- PURPOSE: procedure XXWC_BPA_ZONE_TEMP_TBL_PRC to populate date into global temp table XXWC.XXWC_BPA_PRICE_ZONE_V_TEMP_TBL
   -- HISTORY
   -- =========================================================================================================================
   -- =========================================================================================================================
   -- VERSION DATE          AUTHOR(S)       DESCRIPTION
   -- ------- -----------   --------------- -----------------------------------------------------------------------------------
   -- 1.0     01-Apr-2015   P.Vamshidhar    Created this procedure.
                                            TMS#20150414-00173-2015 PLM - Implement Cost Management tool enhancements
                                            package created to populate data into global temp
                                            table for supplier.
   -- 1.2    29-Sep-2017   Nancy Pahwa   TMS#20160921-00270 - BPA Enhancements Mfg Part Number Addition
   ***************************************************************************************************************************/
   IS
      l_mast_org_id       MTL_PARAMETERS.ORGANIZATION_ID%TYPE
                             := FND_PROFILE.VALUE ('XXWC_ITEM_MASTER_ORG');
      l_org_id            HR_OPERATING_UNITS.ORGANIZATION_ID%TYPE
                             := APPS.fnd_profile.VALUE ('ORG_ID');

      CURSOR cur_bpa_zone (
         p_master_org         NUMBER,
         p_org_id             NUMBER,
         p_vendor_id1         NUMBER,
         p_vendor_site_id1    NUMBER,
         p_po_header_id       NUMBER)
      IS
           SELECT DISTINCT PO_NUMBER,
                           VENDOR_NAME,
                           VENDOR_NUM,
                           VENDOR_SITE_CODE,
                           ITEM_NUMBER,
                           ITEM_DESCRIPTION,
                           VENDOR_ITEM_NUMBER,
                           EXPIRATION_DATE,
                           NATIONAL_PRICE,
                           NATIONAL_QUANTITY,
                           NATIONAL_BREAK,
                           PROMO_PRICE,
                           PROMO_START_DATE,
                           PROMO_END_DATE,
                           PRICE_ZONE_PRICE1,
                           PRICE_ZONE_QUANTITY1,
                           PRICE_ZONE_QUANTITY_PRICE1,
                           PRICE_ZONE_PRICE2,
                           PRICE_ZONE_QUANTITY2,
                           PRICE_ZONE_QUANTITY_PRICE2,
                           PRICE_ZONE_PRICE3,
                           PRICE_ZONE_QUANTITY3,
                           PRICE_ZONE_QUANTITY_PRICE3,
                           PRICE_ZONE_PRICE4,
                           PRICE_ZONE_QUANTITY4,
                           PRICE_ZONE_QUANTITY_PRICE4,
                           PRICE_ZONE_PRICE5,
                           PRICE_ZONE_QUANTITY5,
                           PRICE_ZONE_QUANTITY_PRICE5,
                           PRICE_ZONE_PRICE6,
                           PRICE_ZONE_QUANTITY6,
                           PRICE_ZONE_QUANTITY_PRICE6,
                           PRICE_ZONE_PRICE7,
                           PRICE_ZONE_QUANTITY7,
                           PRICE_ZONE_QUANTITY_PRICE7,
                           PRICE_ZONE_PRICE8,
                           PRICE_ZONE_QUANTITY8,
                           PRICE_ZONE_QUANTITY_PRICE8,
                           PRICE_ZONE_PRICE9,
                           PRICE_ZONE_QUANTITY9,
                           PRICE_ZONE_QUANTITY_PRICE9,
                           PRICE_ZONE_PRICE10,
                           PRICE_ZONE_QUANTITY10,
                           PRICE_ZONE_QUANTITY_PRICE10,
                           PRICE_ZONE_PRICE11,
                           PRICE_ZONE_QUANTITY11,
                           PRICE_ZONE_QUANTITY_PRICE11,
                           PRICE_ZONE_PRICE12,
                           PRICE_ZONE_QUANTITY12,
                           PRICE_ZONE_QUANTITY_PRICE12,
                           PRICE_ZONE_PRICE13,
                           PRICE_ZONE_QUANTITY13,
                           PRICE_ZONE_QUANTITY_PRICE13,
                           PRICE_ZONE_PRICE14,
                           PRICE_ZONE_QUANTITY14,
                           PRICE_ZONE_QUANTITY_PRICE14,
                           PRICE_ZONE_PRICE15,
                           PRICE_ZONE_QUANTITY15,
                           PRICE_ZONE_QUANTITY_PRICE15,
                           PRICE_ZONE_PRICE16,
                           PRICE_ZONE_QUANTITY16,
                           PRICE_ZONE_QUANTITY_PRICE16,
                           PRICE_ZONE_PRICE17,
                           PRICE_ZONE_QUANTITY17,
                           PRICE_ZONE_QUANTITY_PRICE17,
                           PRICE_ZONE_PRICE18,
                           PRICE_ZONE_QUANTITY18,
                           PRICE_ZONE_QUANTITY_PRICE18,
                           PRICE_ZONE_PRICE19,
                           PRICE_ZONE_QUANTITY19,
                           PRICE_ZONE_QUANTITY_PRICE19,
                           PRICE_ZONE_PRICE20,
                           PRICE_ZONE_QUANTITY20,
                           PRICE_ZONE_QUANTITY_PRICE20,
                           PRICE_ZONE_PRICE21,
                           PRICE_ZONE_QUANTITY21,
                           PRICE_ZONE_QUANTITY_PRICE21,
                           PRICE_ZONE_PRICE22,
                           PRICE_ZONE_QUANTITY22,
                           PRICE_ZONE_QUANTITY_PRICE22,
                           PRICE_ZONE_PRICE23,
                           PRICE_ZONE_QUANTITY23,
                           PRICE_ZONE_QUANTITY_PRICE23,
                           PRICE_ZONE_PRICE24,
                           PRICE_ZONE_QUANTITY24,
                           PRICE_ZONE_QUANTITY_PRICE24,
                           PRICE_ZONE_PRICE25,
                           PRICE_ZONE_QUANTITY25,
                           PRICE_ZONE_QUANTITY_PRICE25,
                           PRICE_ZONE_PRICE26,
                           PRICE_ZONE_QUANTITY26,
                           PRICE_ZONE_QUANTITY_PRICE26,
                           PRICE_ZONE_PRICE27,
                           PRICE_ZONE_QUANTITY27,
                           PRICE_ZONE_QUANTITY_PRICE27,
                           PRICE_ZONE_PRICE28,
                           PRICE_ZONE_QUANTITY28,
                           PRICE_ZONE_QUANTITY_PRICE28,
                           PRICE_ZONE_PRICE29,
                           PRICE_ZONE_QUANTITY29,
                           PRICE_ZONE_QUANTITY_PRICE29,
                           PRICE_ZONE_PRICE30,
                           PRICE_ZONE_QUANTITY30,
                           PRICE_ZONE_QUANTITY_PRICE30,
                           PO_HEADER_ID,
                           INVENTORY_ITEM_ID,
                           VENDOR_ID,
                           VENDOR_SITE_ID,
                           VENDOR_CONTACT_ID,
                           IMPLEMENT_DATE,
                           PRICE_ZONE_QUANTITY,
                           ORG_ID
             FROM (SELECT pha.segment1 po_number,
                          ass.vendor_name,
                          ass.segment1 vendor_num,
                          assa.vendor_site_code,
                          msib.segment1 item_number,
                          msib.description item_description,
                          --1.2 start
                          ( SELECT mcr.CROSS_REFERENCE
                          FROM apps.mtl_cross_references mcr, apps.ap_suppliers aps, apps.mtl_system_items_b msi
                          WHERE    1=1
                          AND msi.inventory_item_id = mcr.inventory_item_id
                          AND msi.organization_id = p_master_org
                         -- AND mcr.attribute_category = '162'
                          AND mcr.cross_reference_type = 'VENDOR'
                          AND mcr.attribute1 = aps.segment1
                          -- AND mcr.attribute1 = i_vendor_num
                          AND msi.segment1 = msib.segment1 --p_item_number
                          AND aps.vendor_name = ass.vendor_name--P_VENDOR_NAME
                          AND mcr.attribute1 is not null
                          AND ROWNUM = 1) -- 1.2 end
                             vendor_item_number,      --,msib.primary_uom_code
                          pla.expiration_date expiration_date,
                          NULL National_price,
                          NULL NATIONAL_QUANTITY,
                          NULL NATIONAL_BREAK,
                          xppt.promo_price,
                          xppt.promo_start_date,
                          xppt.promo_end_date,
                          NULL PRICE_ZONE_PRICE1,
                          NULL PRICE_ZONE_QUANTITY1,
                          NULL PRICE_ZONE_QUANTITY_PRICE1,
                          NULL PRICE_ZONE_PRICE2,
                          NULL PRICE_ZONE_QUANTITY2,
                          NULL PRICE_ZONE_QUANTITY_PRICE2,
                          NULL PRICE_ZONE_PRICE3,
                          NULL PRICE_ZONE_QUANTITY3,
                          NULL PRICE_ZONE_QUANTITY_PRICE3,
                          NULL PRICE_ZONE_PRICE4,
                          NULL PRICE_ZONE_QUANTITY4,
                          NULL PRICE_ZONE_QUANTITY_PRICE4,
                          NULL PRICE_ZONE_PRICE5,
                          NULL PRICE_ZONE_QUANTITY5,
                          NULL PRICE_ZONE_QUANTITY_PRICE5,
                          NULL PRICE_ZONE_PRICE6,
                          NULL PRICE_ZONE_QUANTITY6,
                          NULL PRICE_ZONE_QUANTITY_PRICE6,
                          NULL PRICE_ZONE_PRICE7,
                          NULL PRICE_ZONE_QUANTITY7,
                          NULL PRICE_ZONE_QUANTITY_PRICE7,
                          NULL PRICE_ZONE_PRICE8,
                          NULL PRICE_ZONE_QUANTITY8,
                          NULL PRICE_ZONE_QUANTITY_PRICE8,
                          NULL PRICE_ZONE_PRICE9,
                          NULL PRICE_ZONE_QUANTITY9,
                          NULL PRICE_ZONE_QUANTITY_PRICE9,
                          NULL PRICE_ZONE_PRICE10,
                          NULL PRICE_ZONE_QUANTITY10,
                          NULL PRICE_ZONE_QUANTITY_PRICE10,
                          NULL PRICE_ZONE_PRICE11,
                          NULL PRICE_ZONE_QUANTITY11,
                          NULL PRICE_ZONE_QUANTITY_PRICE11,
                          NULL PRICE_ZONE_PRICE12,
                          NULL PRICE_ZONE_QUANTITY12,
                          NULL PRICE_ZONE_QUANTITY_PRICE12,
                          NULL PRICE_ZONE_PRICE13,
                          NULL PRICE_ZONE_QUANTITY13,
                          NULL PRICE_ZONE_QUANTITY_PRICE13,
                          NULL PRICE_ZONE_PRICE14,
                          NULL PRICE_ZONE_QUANTITY14,
                          NULL PRICE_ZONE_QUANTITY_PRICE14,
                          NULL PRICE_ZONE_PRICE15,
                          NULL PRICE_ZONE_QUANTITY15,
                          NULL PRICE_ZONE_QUANTITY_PRICE15,
                          NULL PRICE_ZONE_PRICE16,
                          NULL PRICE_ZONE_QUANTITY16,
                          NULL PRICE_ZONE_QUANTITY_PRICE16,
                          NULL PRICE_ZONE_PRICE17,
                          NULL PRICE_ZONE_QUANTITY17,
                          NULL PRICE_ZONE_QUANTITY_PRICE17,
                          NULL PRICE_ZONE_PRICE18,
                          NULL PRICE_ZONE_QUANTITY18,
                          NULL PRICE_ZONE_QUANTITY_PRICE18,
                          NULL PRICE_ZONE_PRICE19,
                          NULL PRICE_ZONE_QUANTITY19,
                          NULL PRICE_ZONE_QUANTITY_PRICE19,
                          NULL PRICE_ZONE_PRICE20,
                          NULL PRICE_ZONE_QUANTITY20,
                          NULL PRICE_ZONE_QUANTITY_PRICE20,
                          NULL PRICE_ZONE_PRICE21,
                          NULL PRICE_ZONE_QUANTITY21,
                          NULL PRICE_ZONE_QUANTITY_PRICE21,
                          NULL PRICE_ZONE_PRICE22,
                          NULL PRICE_ZONE_QUANTITY22,
                          NULL PRICE_ZONE_QUANTITY_PRICE22,
                          NULL PRICE_ZONE_PRICE23,
                          NULL PRICE_ZONE_QUANTITY23,
                          NULL PRICE_ZONE_QUANTITY_PRICE23,
                          NULL PRICE_ZONE_PRICE24,
                          NULL PRICE_ZONE_QUANTITY24,
                          NULL PRICE_ZONE_QUANTITY_PRICE24,
                          NULL PRICE_ZONE_PRICE25,
                          NULL PRICE_ZONE_QUANTITY25,
                          NULL PRICE_ZONE_QUANTITY_PRICE25,
                          NULL PRICE_ZONE_PRICE26,
                          NULL PRICE_ZONE_QUANTITY26,
                          NULL PRICE_ZONE_QUANTITY_PRICE26,
                          NULL PRICE_ZONE_PRICE27,
                          NULL PRICE_ZONE_QUANTITY27,
                          NULL PRICE_ZONE_QUANTITY_PRICE27,
                          NULL PRICE_ZONE_PRICE28,
                          NULL PRICE_ZONE_QUANTITY28,
                          NULL PRICE_ZONE_QUANTITY_PRICE28,
                          NULL PRICE_ZONE_PRICE29,
                          NULL PRICE_ZONE_QUANTITY29,
                          NULL PRICE_ZONE_QUANTITY_PRICE29,
                          NULL PRICE_ZONE_PRICE30,
                          NULL PRICE_ZONE_QUANTITY30,
                          NULL PRICE_ZONE_QUANTITY_PRICE30,
                          pha.po_header_id,
                          msib.inventory_item_id,
                          pha.vendor_id,
                          pha.vendor_site_id,
                          pha.vendor_contact_id,
                          NULL implement_date,
                          xbpzqbv.price_zone_quantity,
                          pha.org_id
                     FROM apps.ap_suppliers ass,
                          apps.ap_supplier_sites_all assa,
                          apps.po_headers_all pha,
                          apps.po_lines_all pla,
                          apps.mtl_system_items_b msib,
                          xxwc.XXWC_BPA_PRICE_ZONE_TBL xppzt,
                          xxwc.XXWC_BPA_PROMO_TBL xppt,
                          XXWC_BPA_PRICE_ZONE_Q_BRKS_VW xbpzqbv
                    WHERE     ass.vendor_id = p_vendor_id1             --45374
                          AND assa.vendor_site_id =
                                 NVL (p_vendor_site_id1, assa.vendor_site_id)
                          AND ass.vendor_id = assa.vendor_id
                          AND pha.vendor_id = ass.vendor_id
                          AND pha.po_header_id =
                                 NVL (p_po_header_id, pha.po_header_id)
                          AND pha.vendor_site_id = assa.vendor_site_id
                          AND pha.org_id = p_org_id
                          AND pha.org_id = assa.org_id
                          AND SYSDATE BETWEEN NVL (pha.start_date, SYSDATE - 1)
                                          AND NVL (pha.end_date, SYSDATE + 1)
                          AND pha.po_header_id = pla.po_header_id
                          AND NVL (pha.cancel_flag, 'N') = 'N'
                          AND NVL (pla.cancel_flag, 'N') = 'N'
                          AND pla.item_id = msib.inventory_item_id
                          AND msib.organization_id = p_master_org
                          AND pla.po_header_id = xppzt.po_header_id(+)
                          AND pla.item_id = xppzt.inventory_item_id(+)
                          AND pla.org_id = xppzt.org_id(+)
                          AND xppzt.po_header_id = xppt.po_header_id(+)
                          AND xppzt.inventory_item_id =
                                 xppt.inventory_item_id(+)
                          AND xppzt.org_id = xppt.org_id(+)
                          AND xppzt.po_header_id = xbpzqbv.po_header_id(+)
                          AND xppzt.inventory_item_id =
                                 xbpzqbv.inventory_item_id(+)
                   UNION ALL
                   SELECT pha.segment1 po_number,
                          ass.vendor_name,
                          ass.segment1 vendor_num,
                          assa.vendor_site_code,
                          NULL,                    --msib.segment1 item_number
                          NULL,            --msib.description item_description
                          NULL,                           --vendor_item_number
                          NULL expiration_date,          --pla.expiration_date
                          NULL National_price,
                          NULL NATIONAL_QUANTITY,
                          NULL NATIONAL_BREAK,
                          NULL,                             --xppt.promo_price
                          NULL,                        --xppt.promo_start_date
                          NULL,                          --xppt.promo_end_date
                          NULL PRICE_ZONE_PRICE1,
                          NULL PRICE_ZONE_QUANTITY1,
                          NULL PRICE_ZONE_QUANTITY_PRICE1,
                          NULL PRICE_ZONE_PRICE2,
                          NULL PRICE_ZONE_QUANTITY2,
                          NULL PRICE_ZONE_QUANTITY_PRICE2,
                          NULL PRICE_ZONE_PRICE3,
                          NULL PRICE_ZONE_QUANTITY3,
                          NULL PRICE_ZONE_QUANTITY_PRICE3,
                          NULL PRICE_ZONE_PRICE4,
                          NULL PRICE_ZONE_QUANTITY4,
                          NULL PRICE_ZONE_QUANTITY_PRICE4,
                          NULL PRICE_ZONE_PRICE5,
                          NULL PRICE_ZONE_QUANTITY5,
                          NULL PRICE_ZONE_QUANTITY_PRICE5,
                          NULL PRICE_ZONE_PRICE6,
                          NULL PRICE_ZONE_QUANTITY6,
                          NULL PRICE_ZONE_QUANTITY_PRICE6,
                          NULL PRICE_ZONE_PRICE7,
                          NULL PRICE_ZONE_QUANTITY7,
                          NULL PRICE_ZONE_QUANTITY_PRICE7,
                          NULL PRICE_ZONE_PRICE8,
                          NULL PRICE_ZONE_QUANTITY8,
                          NULL PRICE_ZONE_QUANTITY_PRICE8,
                          NULL PRICE_ZONE_PRICE9,
                          NULL PRICE_ZONE_QUANTITY9,
                          NULL PRICE_ZONE_QUANTITY_PRICE9,
                          NULL PRICE_ZONE_PRICE10,
                          NULL PRICE_ZONE_QUANTITY10,
                          NULL PRICE_ZONE_QUANTITY_PRICE10,
                          NULL PRICE_ZONE_PRICE11,
                          NULL PRICE_ZONE_QUANTITY11,
                          NULL PRICE_ZONE_QUANTITY_PRICE11,
                          NULL PRICE_ZONE_PRICE12,
                          NULL PRICE_ZONE_QUANTITY12,
                          NULL PRICE_ZONE_QUANTITY_PRICE12,
                          NULL PRICE_ZONE_PRICE13,
                          NULL PRICE_ZONE_QUANTITY13,
                          NULL PRICE_ZONE_QUANTITY_PRICE13,
                          NULL PRICE_ZONE_PRICE14,
                          NULL PRICE_ZONE_QUANTITY14,
                          NULL PRICE_ZONE_QUANTITY_PRICE14,
                          NULL PRICE_ZONE_PRICE15,
                          NULL PRICE_ZONE_QUANTITY15,
                          NULL PRICE_ZONE_QUANTITY_PRICE15,
                          NULL PRICE_ZONE_PRICE16,
                          NULL PRICE_ZONE_QUANTITY16,
                          NULL PRICE_ZONE_QUANTITY_PRICE16,
                          NULL PRICE_ZONE_PRICE17,
                          NULL PRICE_ZONE_QUANTITY17,
                          NULL PRICE_ZONE_QUANTITY_PRICE17,
                          NULL PRICE_ZONE_PRICE18,
                          NULL PRICE_ZONE_QUANTITY18,
                          NULL PRICE_ZONE_QUANTITY_PRICE18,
                          NULL PRICE_ZONE_PRICE19,
                          NULL PRICE_ZONE_QUANTITY19,
                          NULL PRICE_ZONE_QUANTITY_PRICE19,
                          NULL PRICE_ZONE_PRICE20,
                          NULL PRICE_ZONE_QUANTITY20,
                          NULL PRICE_ZONE_QUANTITY_PRICE20,
                          NULL PRICE_ZONE_PRICE21,
                          NULL PRICE_ZONE_QUANTITY21,
                          NULL PRICE_ZONE_QUANTITY_PRICE21,
                          NULL PRICE_ZONE_PRICE22,
                          NULL PRICE_ZONE_QUANTITY22,
                          NULL PRICE_ZONE_QUANTITY_PRICE22,
                          NULL PRICE_ZONE_PRICE23,
                          NULL PRICE_ZONE_QUANTITY23,
                          NULL PRICE_ZONE_QUANTITY_PRICE23,
                          NULL PRICE_ZONE_PRICE24,
                          NULL PRICE_ZONE_QUANTITY24,
                          NULL PRICE_ZONE_QUANTITY_PRICE24,
                          NULL PRICE_ZONE_PRICE25,
                          NULL PRICE_ZONE_QUANTITY25,
                          NULL PRICE_ZONE_QUANTITY_PRICE25,
                          NULL PRICE_ZONE_PRICE26,
                          NULL PRICE_ZONE_QUANTITY26,
                          NULL PRICE_ZONE_QUANTITY_PRICE26,
                          NULL PRICE_ZONE_PRICE27,
                          NULL PRICE_ZONE_QUANTITY27,
                          NULL PRICE_ZONE_QUANTITY_PRICE27,
                          NULL PRICE_ZONE_PRICE28,
                          NULL PRICE_ZONE_QUANTITY28,
                          NULL PRICE_ZONE_QUANTITY_PRICE28,
                          NULL PRICE_ZONE_PRICE29,
                          NULL PRICE_ZONE_QUANTITY29,
                          NULL PRICE_ZONE_QUANTITY_PRICE29,
                          NULL PRICE_ZONE_PRICE30,
                          NULL PRICE_ZONE_QUANTITY30,
                          NULL PRICE_ZONE_QUANTITY_PRICE30,
                          pha.po_header_id,                     --po_header_id
                          NULL,                       --msib.inventory_item_id
                          pha.vendor_id,
                          pha.vendor_site_id,
                          pha.vendor_contact_id,
                          NULL Implement_date,
                          NULL Price_zone_quantity,
                          pha.org_id
                     FROM apps.po_headers_all pha,
                          ap_suppliers ass,
                          apps.ap_supplier_sites_all assa
                    WHERE     pha.vendor_id = ass.vendor_id
                          AND ass.vendor_id = p_vendor_id1
                          AND pha.po_header_id =
                                 NVL (p_po_header_id, pha.po_header_id)
                          AND pha.vendor_id = assa.vendor_id
                          AND pha.org_id = assa.org_id -- -- This condition added by Raghavendra on 19-Nov-2014 As per Canada OU WEB ADI Test
                          AND pha.org_id = p_org_id -- This condition added by Raghavendra on 19-Nov-2014 As per Canada OU WEB ADI Test
                          AND pha.vendor_site_id = assa.vendor_site_id
                          AND assa.vendor_site_id =
                                 NVL (p_vendor_site_id1, assa.vendor_site_id)
                          AND SYSDATE BETWEEN NVL (pha.start_date, SYSDATE - 1)
                                          AND NVL (pha.end_date, SYSDATE + 1)
                          AND NVL (pha.cancel_flag, 'N') = 'N'
                          AND NOT EXISTS
                                     (SELECT 1
                                        FROM apps.po_lines_all pla
                                       WHERE     pla.po_header_id =
                                                    pha.po_header_id
                                             AND pla.org_id = pha.org_id -- This condition added by Raghavendra on 19-Nov-2014 As per Canada OU WEB ADI Test
                                             AND pla.org_id = p_org_id -- This condition added by Raghavendra on 19-Nov-2014 As per Canada OU WEB ADI Test
                                             AND NVL (pla.cancel_flag, 'N') =
                                                    'N'
                                             AND SYSDATE <=
                                                    NVL (pla.expiration_date,
                                                         SYSDATE + 1)))
         ORDER BY po_header_id,
                  CASE WHEN expiration_date IS NULL THEN 0 ELSE 1 END,
                  item_number;

      CURSOR CUR_ZONE_PRICE (
         p_po_header_id    NUMBER,
         p_item_id         NUMBER,
         p_org_id          NUMBER)
      IS
         SELECT price_zone_price, price_zone
           FROM XXWC.xxwc_bpa_price_zone_tbl
          WHERE     po_header_id = p_po_header_id
                AND inventory_item_id = p_item_id
                AND org_id = p_org_id;

      CURSOR CUR_PRICE_BREAK (
         p_po_header_id    NUMBER,
         p_item_id         NUMBER,
         p_org_id          NUMBER)
      IS
         SELECT price_zone_quantity, price_zone_quantity_price, price_zone
           FROM XXWC.XXWC_BPA_PRICE_ZONE_BREAKS_TBL
          WHERE     po_header_id = p_po_header_id
                AND inventory_item_id = p_item_id
                AND org_id = p_org_id;

      TYPE type_bpa_zone IS TABLE OF cur_bpa_zone%ROWTYPE;

      type_bpa_zone_obj   type_bpa_zone;
      l_stage             NUMBER;

      --x_err_message       VARCHAR2 (1000);

      l_zone_num          NUMBER := NULL;
      l_zone_price        NUMBER := NULL;
      l_po_header_id      PO_HEADERS_ALL.PO_HEADER_ID%TYPE;
   BEGIN
      IF p_po_number IS NOT NULL
      THEN
         BEGIN
            SELECT po_header_id
              INTO l_po_header_id
              FROM PO_HEADERS_ALL
             WHERE segment1 = p_po_number;
         EXCEPTION
            WHEN OTHERS
            THEN
               l_po_header_id := NULL;
         END;
      ELSE
         l_po_header_id := NULL;
      END IF;

      DELETE FROM XXWC_BPA_PRICE_ZONE_V_TEMP_TBL
            WHERE     INSERT_DATE < SYSDATE - 1
                  AND CREATED_BY = FND_GLOBAL.USER_ID;

      DELETE FROM XXWC_BPA_PRICE_ZONE_V_TEMP_TBL
            WHERE     CREATED_BY = FND_GLOBAL.USER_ID
                  AND VENDOR_ID = P_VENDOR_ID
                  AND VENDOR_SITE_ID = NVL (P_VENDOR_SITE_ID, VENDOR_SITE_ID)
                  AND PO_HEADER_ID = NVL (l_po_header_id, PO_HEADER_ID);

      COMMIT;

      OPEN cur_bpa_zone (l_mast_org_id,
                         l_org_id,
                         p_vendor_id,
                         p_vendor_site_id,
                         l_po_header_id);

      LOOP
         FETCH cur_bpa_zone BULK COLLECT INTO type_bpa_zone_obj LIMIT 10000;

         FOR l_stage IN type_bpa_zone_obj.FIRST .. type_bpa_zone_obj.LAST
         LOOP
            IF type_bpa_zone_obj (l_stage).inventory_item_id IS NOT NULL
            THEN
               OPEN CUR_ZONE_PRICE (type_bpa_zone_obj (l_stage).po_header_id,
                                    type_bpa_zone_obj (l_stage).inventory_item_id,
                                    l_org_id);

               LOOP
                  l_zone_num := NULL;
                  l_zone_price := NULL;

                  FETCH CUR_ZONE_PRICE INTO l_zone_price, l_zone_num;

                  IF l_zone_num = 0
                  THEN
                     type_bpa_zone_obj (l_stage).national_price :=
                        l_zone_price;
                  ELSIF l_zone_num = 1
                  THEN
                     type_bpa_zone_obj (l_stage).price_zone_price1 :=
                        l_zone_price;
                  ELSIF l_zone_num = 2
                  THEN
                     type_bpa_zone_obj (l_stage).price_zone_price2 :=
                        l_zone_price;
                  ELSIF l_zone_num = 3
                  THEN
                     type_bpa_zone_obj (l_stage).price_zone_price3 :=
                        l_zone_price;
                  ELSIF l_zone_num = 4
                  THEN
                     type_bpa_zone_obj (l_stage).price_zone_price4 :=
                        l_zone_price;
                  ELSIF l_zone_num = 5
                  THEN
                     type_bpa_zone_obj (l_stage).price_zone_price5 :=
                        l_zone_price;
                  ELSIF l_zone_num = 6
                  THEN
                     type_bpa_zone_obj (l_stage).price_zone_price6 :=
                        l_zone_price;
                  ELSIF l_zone_num = 7
                  THEN
                     type_bpa_zone_obj (l_stage).price_zone_price7 :=
                        l_zone_price;
                  ELSIF l_zone_num = 8
                  THEN
                     type_bpa_zone_obj (l_stage).price_zone_price8 :=
                        l_zone_price;
                  ELSIF l_zone_num = 9
                  THEN
                     type_bpa_zone_obj (l_stage).price_zone_price9 :=
                        l_zone_price;
                  ELSIF l_zone_num = 10
                  THEN
                     type_bpa_zone_obj (l_stage).price_zone_price10 :=
                        l_zone_price;
                  ELSIF l_zone_num = 11
                  THEN
                     type_bpa_zone_obj (l_stage).price_zone_price11 :=
                        l_zone_price;
                  ELSIF l_zone_num = 12
                  THEN
                     type_bpa_zone_obj (l_stage).price_zone_price12 :=
                        l_zone_price;
                  ELSIF l_zone_num = 13
                  THEN
                     type_bpa_zone_obj (l_stage).price_zone_price13 :=
                        l_zone_price;
                  ELSIF l_zone_num = 14
                  THEN
                     type_bpa_zone_obj (l_stage).price_zone_price14 :=
                        l_zone_price;
                  ELSIF l_zone_num = 15
                  THEN
                     type_bpa_zone_obj (l_stage).price_zone_price15 :=
                        l_zone_price;
                  ELSIF l_zone_num = 16
                  THEN
                     type_bpa_zone_obj (l_stage).price_zone_price16 :=
                        l_zone_price;
                  ELSIF l_zone_num = 17
                  THEN
                     type_bpa_zone_obj (l_stage).price_zone_price17 :=
                        l_zone_price;
                  ELSIF l_zone_num = 18
                  THEN
                     type_bpa_zone_obj (l_stage).price_zone_price18 :=
                        l_zone_price;
                  ELSIF l_zone_num = 19
                  THEN
                     type_bpa_zone_obj (l_stage).price_zone_price19 :=
                        l_zone_price;
                  ELSIF l_zone_num = 20
                  THEN
                     type_bpa_zone_obj (l_stage).price_zone_price20 :=
                        l_zone_price;
                  ELSIF l_zone_num = 21
                  THEN
                     type_bpa_zone_obj (l_stage).price_zone_price21 :=
                        l_zone_price;
                  ELSIF l_zone_num = 22
                  THEN
                     type_bpa_zone_obj (l_stage).price_zone_price22 :=
                        l_zone_price;
                  ELSIF l_zone_num = 23
                  THEN
                     type_bpa_zone_obj (l_stage).price_zone_price23 :=
                        l_zone_price;
                  ELSIF l_zone_num = 24
                  THEN
                     type_bpa_zone_obj (l_stage).price_zone_price24 :=
                        l_zone_price;
                  ELSIF l_zone_num = 25
                  THEN
                     type_bpa_zone_obj (l_stage).price_zone_price25 :=
                        l_zone_price;
                  ELSIF l_zone_num = 26
                  THEN
                     type_bpa_zone_obj (l_stage).price_zone_price26 :=
                        l_zone_price;
                  ELSIF l_zone_num = 27
                  THEN
                     type_bpa_zone_obj (l_stage).price_zone_price27 :=
                        l_zone_price;
                  ELSIF l_zone_num = 28
                  THEN
                     type_bpa_zone_obj (l_stage).price_zone_price28 :=
                        l_zone_price;
                  ELSIF l_zone_num = 29
                  THEN
                     type_bpa_zone_obj (l_stage).price_zone_price29 :=
                        l_zone_price;
                  ELSIF l_zone_num = 30
                  THEN
                     type_bpa_zone_obj (l_stage).price_zone_price30 :=
                        l_zone_price;
                  ELSE
                     NULL;
                  END IF;

                  EXIT WHEN CUR_ZONE_PRICE%NOTFOUND;
               END LOOP;

               CLOSE CUR_ZONE_PRICE;

               FOR REC_PRICE_BREAK
                  IN CUR_PRICE_BREAK (
                        type_bpa_zone_obj (l_stage).po_header_id,
                        type_bpa_zone_obj (l_stage).inventory_item_id,
                        l_org_id)
               LOOP
                  IF NVL (REC_PRICE_BREAK.price_zone_quantity, 0) =
                        NVL (type_bpa_zone_obj (l_stage).price_zone_quantity,
                             0)
                  THEN
                     IF REC_PRICE_BREAK.price_zone = 0
                     THEN
                        type_bpa_zone_obj (l_stage).NATIONAL_QUANTITY :=
                           NVL (REC_PRICE_BREAK.price_zone_quantity, 0);
                        type_bpa_zone_obj (l_stage).NATIONAL_BREAK :=
                           rec_price_break.price_zone_quantity_price;
                     ELSIF REC_PRICE_BREAK.price_zone = 1
                     THEN
                        type_bpa_zone_obj (l_stage).PRICE_ZONE_QUANTITY1 :=
                           NVL (REC_PRICE_BREAK.price_zone_quantity, 0);
                        type_bpa_zone_obj (l_stage).PRICE_ZONE_QUANTITY_PRICE1 :=
                           rec_price_break.price_zone_quantity_price;
                     ELSIF REC_PRICE_BREAK.price_zone = 2
                     THEN
                        type_bpa_zone_obj (l_stage).PRICE_ZONE_QUANTITY2 :=
                           NVL (REC_PRICE_BREAK.price_zone_quantity, 0);
                        type_bpa_zone_obj (l_stage).PRICE_ZONE_QUANTITY_PRICE2 :=
                           rec_price_break.price_zone_quantity_price;
                     ELSIF REC_PRICE_BREAK.price_zone = 3
                     THEN
                        type_bpa_zone_obj (l_stage).PRICE_ZONE_QUANTITY3 :=
                           NVL (REC_PRICE_BREAK.price_zone_quantity, 0);
                        type_bpa_zone_obj (l_stage).PRICE_ZONE_QUANTITY_PRICE3 :=
                           rec_price_break.price_zone_quantity_price;
                     ELSIF REC_PRICE_BREAK.price_zone = 4
                     THEN
                        type_bpa_zone_obj (l_stage).PRICE_ZONE_QUANTITY4 :=
                           NVL (REC_PRICE_BREAK.price_zone_quantity, 0);
                        type_bpa_zone_obj (l_stage).PRICE_ZONE_QUANTITY_PRICE4 :=
                           rec_price_break.price_zone_quantity_price;
                     ELSIF REC_PRICE_BREAK.price_zone = 5
                     THEN
                        type_bpa_zone_obj (l_stage).PRICE_ZONE_QUANTITY5 :=
                           NVL (REC_PRICE_BREAK.price_zone_quantity, 0);
                        type_bpa_zone_obj (l_stage).PRICE_ZONE_QUANTITY_PRICE5 :=
                           rec_price_break.price_zone_quantity_price;
                     ELSIF REC_PRICE_BREAK.price_zone = 6
                     THEN
                        type_bpa_zone_obj (l_stage).PRICE_ZONE_QUANTITY6 :=
                           NVL (REC_PRICE_BREAK.price_zone_quantity, 0);
                        type_bpa_zone_obj (l_stage).PRICE_ZONE_QUANTITY_PRICE6 :=
                           rec_price_break.price_zone_quantity_price;
                     ELSIF REC_PRICE_BREAK.price_zone = 7
                     THEN
                        type_bpa_zone_obj (l_stage).PRICE_ZONE_QUANTITY7 :=
                           NVL (REC_PRICE_BREAK.price_zone_quantity, 0);
                        type_bpa_zone_obj (l_stage).PRICE_ZONE_QUANTITY_PRICE7 :=
                           rec_price_break.price_zone_quantity_price;
                     ELSIF REC_PRICE_BREAK.price_zone = 8
                     THEN
                        type_bpa_zone_obj (l_stage).PRICE_ZONE_QUANTITY8 :=
                           NVL (REC_PRICE_BREAK.price_zone_quantity, 0);
                        type_bpa_zone_obj (l_stage).PRICE_ZONE_QUANTITY_PRICE8 :=
                           rec_price_break.price_zone_quantity_price;
                     ELSIF REC_PRICE_BREAK.price_zone = 9
                     THEN
                        type_bpa_zone_obj (l_stage).PRICE_ZONE_QUANTITY9 :=
                           NVL (REC_PRICE_BREAK.price_zone_quantity, 0);
                        type_bpa_zone_obj (l_stage).PRICE_ZONE_QUANTITY_PRICE9 :=
                           rec_price_break.price_zone_quantity_price;
                     ELSIF REC_PRICE_BREAK.price_zone = 10
                     THEN
                        type_bpa_zone_obj (l_stage).PRICE_ZONE_QUANTITY10 :=
                           NVL (REC_PRICE_BREAK.price_zone_quantity, 0);
                        type_bpa_zone_obj (l_stage).PRICE_ZONE_QUANTITY_PRICE10 :=
                           rec_price_break.price_zone_quantity_price;
                     ELSIF REC_PRICE_BREAK.price_zone = 11
                     THEN
                        type_bpa_zone_obj (l_stage).PRICE_ZONE_QUANTITY11 :=
                           NVL (REC_PRICE_BREAK.price_zone_quantity, 0);
                        type_bpa_zone_obj (l_stage).PRICE_ZONE_QUANTITY_PRICE11 :=
                           rec_price_break.price_zone_quantity_price;
                     ELSIF REC_PRICE_BREAK.price_zone = 12
                     THEN
                        type_bpa_zone_obj (l_stage).PRICE_ZONE_QUANTITY12 :=
                           NVL (REC_PRICE_BREAK.price_zone_quantity, 0);
                        type_bpa_zone_obj (l_stage).PRICE_ZONE_QUANTITY_PRICE12 :=
                           rec_price_break.price_zone_quantity_price;
                     ELSIF REC_PRICE_BREAK.price_zone = 13
                     THEN
                        type_bpa_zone_obj (l_stage).PRICE_ZONE_QUANTITY13 :=
                           NVL (REC_PRICE_BREAK.price_zone_quantity, 0);
                        type_bpa_zone_obj (l_stage).PRICE_ZONE_QUANTITY_PRICE13 :=
                           rec_price_break.price_zone_quantity_price;
                     ELSIF REC_PRICE_BREAK.price_zone = 14
                     THEN
                        type_bpa_zone_obj (l_stage).PRICE_ZONE_QUANTITY14 :=
                           NVL (REC_PRICE_BREAK.price_zone_quantity, 0);
                        type_bpa_zone_obj (l_stage).PRICE_ZONE_QUANTITY_PRICE14 :=
                           rec_price_break.price_zone_quantity_price;
                     ELSIF REC_PRICE_BREAK.price_zone = 15
                     THEN
                        type_bpa_zone_obj (l_stage).PRICE_ZONE_QUANTITY15 :=
                           NVL (REC_PRICE_BREAK.price_zone_quantity, 0);
                        type_bpa_zone_obj (l_stage).PRICE_ZONE_QUANTITY_PRICE15 :=
                           rec_price_break.price_zone_quantity_price;
                     ELSIF REC_PRICE_BREAK.price_zone = 16
                     THEN
                        type_bpa_zone_obj (l_stage).PRICE_ZONE_QUANTITY16 :=
                           NVL (REC_PRICE_BREAK.price_zone_quantity, 0);
                        type_bpa_zone_obj (l_stage).PRICE_ZONE_QUANTITY_PRICE16 :=
                           rec_price_break.price_zone_quantity_price;
                     ELSIF REC_PRICE_BREAK.price_zone = 17
                     THEN
                        type_bpa_zone_obj (l_stage).PRICE_ZONE_QUANTITY17 :=
                           NVL (REC_PRICE_BREAK.price_zone_quantity, 0);
                        type_bpa_zone_obj (l_stage).PRICE_ZONE_QUANTITY_PRICE17 :=
                           rec_price_break.price_zone_quantity_price;
                     ELSIF REC_PRICE_BREAK.price_zone = 18
                     THEN
                        type_bpa_zone_obj (l_stage).PRICE_ZONE_QUANTITY18 :=
                           NVL (REC_PRICE_BREAK.price_zone_quantity, 0);
                        type_bpa_zone_obj (l_stage).PRICE_ZONE_QUANTITY_PRICE18 :=
                           rec_price_break.price_zone_quantity_price;
                     ELSIF REC_PRICE_BREAK.price_zone = 19
                     THEN
                        type_bpa_zone_obj (l_stage).PRICE_ZONE_QUANTITY19 :=
                           NVL (REC_PRICE_BREAK.price_zone_quantity, 0);
                        type_bpa_zone_obj (l_stage).PRICE_ZONE_QUANTITY_PRICE19 :=
                           rec_price_break.price_zone_quantity_price;
                     ELSIF REC_PRICE_BREAK.price_zone = 20
                     THEN
                        type_bpa_zone_obj (l_stage).PRICE_ZONE_QUANTITY20 :=
                           NVL (REC_PRICE_BREAK.price_zone_quantity, 0);
                        type_bpa_zone_obj (l_stage).PRICE_ZONE_QUANTITY_PRICE20 :=
                           rec_price_break.price_zone_quantity_price;
                     ELSIF REC_PRICE_BREAK.price_zone = 21
                     THEN
                        type_bpa_zone_obj (l_stage).PRICE_ZONE_QUANTITY21 :=
                           NVL (REC_PRICE_BREAK.price_zone_quantity, 0);
                        type_bpa_zone_obj (l_stage).PRICE_ZONE_QUANTITY_PRICE21 :=
                           rec_price_break.price_zone_quantity_price;
                     ELSIF REC_PRICE_BREAK.price_zone = 22
                     THEN
                        type_bpa_zone_obj (l_stage).PRICE_ZONE_QUANTITY22 :=
                           NVL (REC_PRICE_BREAK.price_zone_quantity, 0);
                        type_bpa_zone_obj (l_stage).PRICE_ZONE_QUANTITY_PRICE22 :=
                           rec_price_break.price_zone_quantity_price;
                     ELSIF REC_PRICE_BREAK.price_zone = 23
                     THEN
                        type_bpa_zone_obj (l_stage).PRICE_ZONE_QUANTITY23 :=
                           NVL (REC_PRICE_BREAK.price_zone_quantity, 0);
                        type_bpa_zone_obj (l_stage).PRICE_ZONE_QUANTITY_PRICE23 :=
                           rec_price_break.price_zone_quantity_price;
                     ELSIF REC_PRICE_BREAK.price_zone = 24
                     THEN
                        type_bpa_zone_obj (l_stage).PRICE_ZONE_QUANTITY24 :=
                           NVL (REC_PRICE_BREAK.price_zone_quantity, 0);
                        type_bpa_zone_obj (l_stage).PRICE_ZONE_QUANTITY_PRICE24 :=
                           rec_price_break.price_zone_quantity_price;
                     ELSIF REC_PRICE_BREAK.price_zone = 25
                     THEN
                        type_bpa_zone_obj (l_stage).PRICE_ZONE_QUANTITY25 :=
                           NVL (REC_PRICE_BREAK.price_zone_quantity, 0);
                        type_bpa_zone_obj (l_stage).PRICE_ZONE_QUANTITY_PRICE25 :=
                           rec_price_break.price_zone_quantity_price;
                     ELSIF REC_PRICE_BREAK.price_zone = 26
                     THEN
                        type_bpa_zone_obj (l_stage).PRICE_ZONE_QUANTITY26 :=
                           NVL (REC_PRICE_BREAK.price_zone_quantity, 0);
                        type_bpa_zone_obj (l_stage).PRICE_ZONE_QUANTITY_PRICE26 :=
                           rec_price_break.price_zone_quantity_price;
                     ELSIF REC_PRICE_BREAK.price_zone = 27
                     THEN
                        type_bpa_zone_obj (l_stage).PRICE_ZONE_QUANTITY27 :=
                           NVL (REC_PRICE_BREAK.price_zone_quantity, 0);
                        type_bpa_zone_obj (l_stage).PRICE_ZONE_QUANTITY_PRICE27 :=
                           rec_price_break.price_zone_quantity_price;
                     ELSIF REC_PRICE_BREAK.price_zone = 28
                     THEN
                        type_bpa_zone_obj (l_stage).PRICE_ZONE_QUANTITY28 :=
                           NVL (REC_PRICE_BREAK.price_zone_quantity, 0);
                        type_bpa_zone_obj (l_stage).PRICE_ZONE_QUANTITY_PRICE28 :=
                           rec_price_break.price_zone_quantity_price;
                     ELSIF REC_PRICE_BREAK.price_zone = 29
                     THEN
                        type_bpa_zone_obj (l_stage).PRICE_ZONE_QUANTITY29 :=
                           NVL (REC_PRICE_BREAK.price_zone_quantity, 0);
                        type_bpa_zone_obj (l_stage).PRICE_ZONE_QUANTITY_PRICE29 :=
                           rec_price_break.price_zone_quantity_price;
                     ELSIF REC_PRICE_BREAK.price_zone = 30
                     THEN
                        type_bpa_zone_obj (l_stage).PRICE_ZONE_QUANTITY30 :=
                           NVL (REC_PRICE_BREAK.price_zone_quantity, 0);
                        type_bpa_zone_obj (l_stage).PRICE_ZONE_QUANTITY_PRICE30 :=
                           rec_price_break.price_zone_quantity_price;
                     ELSE
                        NULL;
                     END IF;
                  END IF;
               END LOOP;
            END IF;
         END LOOP;

         FORALL l_stage IN 1 .. type_bpa_zone_obj.COUNT
            INSERT
              INTO XXWC_BPA_PRICE_ZONE_V_TEMP_TBL (PO_NUMBER,
                                                   VENDOR_NAME,
                                                   VENDOR_NUM,
                                                   VENDOR_SITE_CODE,
                                                   ITEM_NUMBER,
                                                   ITEM_DESCRIPTION,
                                                   VENDOR_ITEM_NUMBER,
                                                   EXPIRATION_DATE,
                                                   NATIONAL_PRICE,
                                                   NATIONAL_QUANTITY,
                                                   NATIONAL_BREAK,
                                                   PROMO_PRICE,
                                                   PROMO_START_DATE,
                                                   PROMO_END_DATE,
                                                   PRICE_ZONE_PRICE1,
                                                   PRICE_ZONE_QUANTITY1,
                                                   PRICE_ZONE_QUANTITY_PRICE1,
                                                   PRICE_ZONE_PRICE2,
                                                   PRICE_ZONE_QUANTITY2,
                                                   PRICE_ZONE_QUANTITY_PRICE2,
                                                   PRICE_ZONE_PRICE3,
                                                   PRICE_ZONE_QUANTITY3,
                                                   PRICE_ZONE_QUANTITY_PRICE3,
                                                   PRICE_ZONE_PRICE4,
                                                   PRICE_ZONE_QUANTITY4,
                                                   PRICE_ZONE_QUANTITY_PRICE4,
                                                   PRICE_ZONE_PRICE5,
                                                   PRICE_ZONE_QUANTITY5,
                                                   PRICE_ZONE_QUANTITY_PRICE5,
                                                   PRICE_ZONE_PRICE6,
                                                   PRICE_ZONE_QUANTITY6,
                                                   PRICE_ZONE_QUANTITY_PRICE6,
                                                   PRICE_ZONE_PRICE7,
                                                   PRICE_ZONE_QUANTITY7,
                                                   PRICE_ZONE_QUANTITY_PRICE7,
                                                   PRICE_ZONE_PRICE8,
                                                   PRICE_ZONE_QUANTITY8,
                                                   PRICE_ZONE_QUANTITY_PRICE8,
                                                   PRICE_ZONE_PRICE9,
                                                   PRICE_ZONE_QUANTITY9,
                                                   PRICE_ZONE_QUANTITY_PRICE9,
                                                   PRICE_ZONE_PRICE10,
                                                   PRICE_ZONE_QUANTITY10,
                                                   PRICE_ZONE_QUANTITY_PRICE10,
                                                   PRICE_ZONE_PRICE11,
                                                   PRICE_ZONE_QUANTITY11,
                                                   PRICE_ZONE_QUANTITY_PRICE11,
                                                   PRICE_ZONE_PRICE12,
                                                   PRICE_ZONE_QUANTITY12,
                                                   PRICE_ZONE_QUANTITY_PRICE12,
                                                   PRICE_ZONE_PRICE13,
                                                   PRICE_ZONE_QUANTITY13,
                                                   PRICE_ZONE_QUANTITY_PRICE13,
                                                   PRICE_ZONE_PRICE14,
                                                   PRICE_ZONE_QUANTITY14,
                                                   PRICE_ZONE_QUANTITY_PRICE14,
                                                   PRICE_ZONE_PRICE15,
                                                   PRICE_ZONE_QUANTITY15,
                                                   PRICE_ZONE_QUANTITY_PRICE15,
                                                   PRICE_ZONE_PRICE16,
                                                   PRICE_ZONE_QUANTITY16,
                                                   PRICE_ZONE_QUANTITY_PRICE16,
                                                   PRICE_ZONE_PRICE17,
                                                   PRICE_ZONE_QUANTITY17,
                                                   PRICE_ZONE_QUANTITY_PRICE17,
                                                   PRICE_ZONE_PRICE18,
                                                   PRICE_ZONE_QUANTITY18,
                                                   PRICE_ZONE_QUANTITY_PRICE18,
                                                   PRICE_ZONE_PRICE19,
                                                   PRICE_ZONE_QUANTITY19,
                                                   PRICE_ZONE_QUANTITY_PRICE19,
                                                   PRICE_ZONE_PRICE20,
                                                   PRICE_ZONE_QUANTITY20,
                                                   PRICE_ZONE_QUANTITY_PRICE20,
                                                   PRICE_ZONE_PRICE21,
                                                   PRICE_ZONE_QUANTITY21,
                                                   PRICE_ZONE_QUANTITY_PRICE21,
                                                   PRICE_ZONE_PRICE22,
                                                   PRICE_ZONE_QUANTITY22,
                                                   PRICE_ZONE_QUANTITY_PRICE22,
                                                   PRICE_ZONE_PRICE23,
                                                   PRICE_ZONE_QUANTITY23,
                                                   PRICE_ZONE_QUANTITY_PRICE23,
                                                   PRICE_ZONE_PRICE24,
                                                   PRICE_ZONE_QUANTITY24,
                                                   PRICE_ZONE_QUANTITY_PRICE24,
                                                   PRICE_ZONE_PRICE25,
                                                   PRICE_ZONE_QUANTITY25,
                                                   PRICE_ZONE_QUANTITY_PRICE25,
                                                   PRICE_ZONE_PRICE26,
                                                   PRICE_ZONE_QUANTITY26,
                                                   PRICE_ZONE_QUANTITY_PRICE26,
                                                   PRICE_ZONE_PRICE27,
                                                   PRICE_ZONE_QUANTITY27,
                                                   PRICE_ZONE_QUANTITY_PRICE27,
                                                   PRICE_ZONE_PRICE28,
                                                   PRICE_ZONE_QUANTITY28,
                                                   PRICE_ZONE_QUANTITY_PRICE28,
                                                   PRICE_ZONE_PRICE29,
                                                   PRICE_ZONE_QUANTITY29,
                                                   PRICE_ZONE_QUANTITY_PRICE29,
                                                   PRICE_ZONE_PRICE30,
                                                   PRICE_ZONE_QUANTITY30,
                                                   PRICE_ZONE_QUANTITY_PRICE30,
                                                   PO_HEADER_ID,
                                                   INVENTORY_ITEM_ID,
                                                   VENDOR_ID,
                                                   VENDOR_SITE_ID,
                                                   VENDOR_CONTACT_ID,
                                                   IMPLEMENT_DATE,
                                                   PRICE_ZONE_QUANTITY,
                                                   ORG_ID,
                                                   CREATED_BY,
                                                   INSERT_DATE)
            VALUES (type_bpa_zone_obj (l_stage).PO_NUMBER,
                    type_bpa_zone_obj (l_stage).VENDOR_NAME,
                    type_bpa_zone_obj (l_stage).VENDOR_NUM,
                    type_bpa_zone_obj (l_stage).VENDOR_SITE_CODE,
                    type_bpa_zone_obj (l_stage).ITEM_NUMBER,
                    type_bpa_zone_obj (l_stage).ITEM_DESCRIPTION,
                    type_bpa_zone_obj (l_stage).VENDOR_ITEM_NUMBER,
                    type_bpa_zone_obj (l_stage).EXPIRATION_DATE,
                    type_bpa_zone_obj (l_stage).NATIONAL_PRICE,
                    type_bpa_zone_obj (l_stage).NATIONAL_QUANTITY,
                    type_bpa_zone_obj (l_stage).NATIONAL_BREAK,
                    type_bpa_zone_obj (l_stage).PROMO_PRICE,
                    type_bpa_zone_obj (l_stage).PROMO_START_DATE,
                    type_bpa_zone_obj (l_stage).PROMO_END_DATE,
                    type_bpa_zone_obj (l_stage).PRICE_ZONE_PRICE1,
                    type_bpa_zone_obj (l_stage).PRICE_ZONE_QUANTITY1,
                    type_bpa_zone_obj (l_stage).PRICE_ZONE_QUANTITY_PRICE1,
                    type_bpa_zone_obj (l_stage).PRICE_ZONE_PRICE2,
                    type_bpa_zone_obj (l_stage).PRICE_ZONE_QUANTITY2,
                    type_bpa_zone_obj (l_stage).PRICE_ZONE_QUANTITY_PRICE2,
                    type_bpa_zone_obj (l_stage).PRICE_ZONE_PRICE3,
                    type_bpa_zone_obj (l_stage).PRICE_ZONE_QUANTITY3,
                    type_bpa_zone_obj (l_stage).PRICE_ZONE_QUANTITY_PRICE3,
                    type_bpa_zone_obj (l_stage).PRICE_ZONE_PRICE4,
                    type_bpa_zone_obj (l_stage).PRICE_ZONE_QUANTITY4,
                    type_bpa_zone_obj (l_stage).PRICE_ZONE_QUANTITY_PRICE4,
                    type_bpa_zone_obj (l_stage).PRICE_ZONE_PRICE5,
                    type_bpa_zone_obj (l_stage).PRICE_ZONE_QUANTITY5,
                    type_bpa_zone_obj (l_stage).PRICE_ZONE_QUANTITY_PRICE5,
                    type_bpa_zone_obj (l_stage).PRICE_ZONE_PRICE6,
                    type_bpa_zone_obj (l_stage).PRICE_ZONE_QUANTITY6,
                    type_bpa_zone_obj (l_stage).PRICE_ZONE_QUANTITY_PRICE6,
                    type_bpa_zone_obj (l_stage).PRICE_ZONE_PRICE7,
                    type_bpa_zone_obj (l_stage).PRICE_ZONE_QUANTITY7,
                    type_bpa_zone_obj (l_stage).PRICE_ZONE_QUANTITY_PRICE7,
                    type_bpa_zone_obj (l_stage).PRICE_ZONE_PRICE8,
                    type_bpa_zone_obj (l_stage).PRICE_ZONE_QUANTITY8,
                    type_bpa_zone_obj (l_stage).PRICE_ZONE_QUANTITY_PRICE8,
                    type_bpa_zone_obj (l_stage).PRICE_ZONE_PRICE9,
                    type_bpa_zone_obj (l_stage).PRICE_ZONE_QUANTITY9,
                    type_bpa_zone_obj (l_stage).PRICE_ZONE_QUANTITY_PRICE9,
                    type_bpa_zone_obj (l_stage).PRICE_ZONE_PRICE10,
                    type_bpa_zone_obj (l_stage).PRICE_ZONE_QUANTITY10,
                    type_bpa_zone_obj (l_stage).PRICE_ZONE_QUANTITY_PRICE10,
                    type_bpa_zone_obj (l_stage).PRICE_ZONE_PRICE11,
                    type_bpa_zone_obj (l_stage).PRICE_ZONE_QUANTITY11,
                    type_bpa_zone_obj (l_stage).PRICE_ZONE_QUANTITY_PRICE11,
                    type_bpa_zone_obj (l_stage).PRICE_ZONE_PRICE12,
                    type_bpa_zone_obj (l_stage).PRICE_ZONE_QUANTITY12,
                    type_bpa_zone_obj (l_stage).PRICE_ZONE_QUANTITY_PRICE12,
                    type_bpa_zone_obj (l_stage).PRICE_ZONE_PRICE13,
                    type_bpa_zone_obj (l_stage).PRICE_ZONE_QUANTITY13,
                    type_bpa_zone_obj (l_stage).PRICE_ZONE_QUANTITY_PRICE13,
                    type_bpa_zone_obj (l_stage).PRICE_ZONE_PRICE14,
                    type_bpa_zone_obj (l_stage).PRICE_ZONE_QUANTITY14,
                    type_bpa_zone_obj (l_stage).PRICE_ZONE_QUANTITY_PRICE14,
                    type_bpa_zone_obj (l_stage).PRICE_ZONE_PRICE15,
                    type_bpa_zone_obj (l_stage).PRICE_ZONE_QUANTITY15,
                    type_bpa_zone_obj (l_stage).PRICE_ZONE_QUANTITY_PRICE15,
                    type_bpa_zone_obj (l_stage).PRICE_ZONE_PRICE16,
                    type_bpa_zone_obj (l_stage).PRICE_ZONE_QUANTITY16,
                    type_bpa_zone_obj (l_stage).PRICE_ZONE_QUANTITY_PRICE16,
                    type_bpa_zone_obj (l_stage).PRICE_ZONE_PRICE17,
                    type_bpa_zone_obj (l_stage).PRICE_ZONE_QUANTITY17,
                    type_bpa_zone_obj (l_stage).PRICE_ZONE_QUANTITY_PRICE17,
                    type_bpa_zone_obj (l_stage).PRICE_ZONE_PRICE18,
                    type_bpa_zone_obj (l_stage).PRICE_ZONE_QUANTITY18,
                    type_bpa_zone_obj (l_stage).PRICE_ZONE_QUANTITY_PRICE18,
                    type_bpa_zone_obj (l_stage).PRICE_ZONE_PRICE19,
                    type_bpa_zone_obj (l_stage).PRICE_ZONE_QUANTITY19,
                    type_bpa_zone_obj (l_stage).PRICE_ZONE_QUANTITY_PRICE19,
                    type_bpa_zone_obj (l_stage).PRICE_ZONE_PRICE20,
                    type_bpa_zone_obj (l_stage).PRICE_ZONE_QUANTITY20,
                    type_bpa_zone_obj (l_stage).PRICE_ZONE_QUANTITY_PRICE20,
                    type_bpa_zone_obj (l_stage).PRICE_ZONE_PRICE21,
                    type_bpa_zone_obj (l_stage).PRICE_ZONE_QUANTITY21,
                    type_bpa_zone_obj (l_stage).PRICE_ZONE_QUANTITY_PRICE21,
                    type_bpa_zone_obj (l_stage).PRICE_ZONE_PRICE22,
                    type_bpa_zone_obj (l_stage).PRICE_ZONE_QUANTITY22,
                    type_bpa_zone_obj (l_stage).PRICE_ZONE_QUANTITY_PRICE22,
                    type_bpa_zone_obj (l_stage).PRICE_ZONE_PRICE23,
                    type_bpa_zone_obj (l_stage).PRICE_ZONE_QUANTITY23,
                    type_bpa_zone_obj (l_stage).PRICE_ZONE_QUANTITY_PRICE23,
                    type_bpa_zone_obj (l_stage).PRICE_ZONE_PRICE24,
                    type_bpa_zone_obj (l_stage).PRICE_ZONE_QUANTITY24,
                    type_bpa_zone_obj (l_stage).PRICE_ZONE_QUANTITY_PRICE24,
                    type_bpa_zone_obj (l_stage).PRICE_ZONE_PRICE25,
                    type_bpa_zone_obj (l_stage).PRICE_ZONE_QUANTITY25,
                    type_bpa_zone_obj (l_stage).PRICE_ZONE_QUANTITY_PRICE25,
                    type_bpa_zone_obj (l_stage).PRICE_ZONE_PRICE26,
                    type_bpa_zone_obj (l_stage).PRICE_ZONE_QUANTITY26,
                    type_bpa_zone_obj (l_stage).PRICE_ZONE_QUANTITY_PRICE26,
                    type_bpa_zone_obj (l_stage).PRICE_ZONE_PRICE27,
                    type_bpa_zone_obj (l_stage).PRICE_ZONE_QUANTITY27,
                    type_bpa_zone_obj (l_stage).PRICE_ZONE_QUANTITY_PRICE27,
                    type_bpa_zone_obj (l_stage).PRICE_ZONE_PRICE28,
                    type_bpa_zone_obj (l_stage).PRICE_ZONE_QUANTITY28,
                    type_bpa_zone_obj (l_stage).PRICE_ZONE_QUANTITY_PRICE28,
                    type_bpa_zone_obj (l_stage).PRICE_ZONE_PRICE29,
                    type_bpa_zone_obj (l_stage).PRICE_ZONE_QUANTITY29,
                    type_bpa_zone_obj (l_stage).PRICE_ZONE_QUANTITY_PRICE29,
                    type_bpa_zone_obj (l_stage).PRICE_ZONE_PRICE30,
                    type_bpa_zone_obj (l_stage).PRICE_ZONE_QUANTITY30,
                    type_bpa_zone_obj (l_stage).PRICE_ZONE_QUANTITY_PRICE30,
                    type_bpa_zone_obj (l_stage).PO_HEADER_ID,
                    type_bpa_zone_obj (l_stage).INVENTORY_ITEM_ID,
                    type_bpa_zone_obj (l_stage).VENDOR_ID,
                    type_bpa_zone_obj (l_stage).VENDOR_SITE_ID,
                    type_bpa_zone_obj (l_stage).VENDOR_CONTACT_ID,
                    type_bpa_zone_obj (l_stage).IMPLEMENT_DATE,
                    type_bpa_zone_obj (l_stage).PRICE_ZONE_QUANTITY,
                    type_bpa_zone_obj (l_stage).ORG_Id,
                    FND_GLOBAL.USER_ID,
                    SYSDATE);

         type_bpa_zone_obj.delete;

         EXIT WHEN cur_bpa_zone%NOTFOUND;
      END LOOP;

      COMMIT;
      x_err_message := NULL;
   EXCEPTION
      WHEN OTHERS
      THEN
         x_err_message := SUBSTR (SQLERRM, 1, 500);
   --DBMS_OUTPUT.PUT_LINE ('Error Occured :' || SUBSTR (SQLERRM, 1, 500));
   END;


   PROCEDURE XXWC_BPA_SUPPLIST_TEMP_TBL_PRC (
      p_supp_list_id   IN     NUMBER,
      x_err_mess          OUT VARCHAR2)
   /***************************************************************************************************************************
   -- PROGRAM TYPE: PL/SQL Script   <API>
   --
   -- PURPOSE: procedure XXWC_BPA_SUPPLIST_TEMP_TBL_PRC to populate date into global temp table XXWC.XXWC_BPA_PRICE_ZONE_V_TEMP_TBL
   -- HISTORY
   -- =========================================================================================================================
   -- =========================================================================================================================
   -- VERSION DATE          AUTHOR(S)       DESCRIPTION
   -- ------- -----------   --------------- -----------------------------------------------------------------------------------
   -- 1.0     01-Apr-2015   P.Vamshidhar    Created this procedure.
                                            TMS#20150414-00173-2015 PLM - Implement Cost Management tool enhancements
                                            package created to populate data into global temp
                                            table for supplier list.
   ***************************************************************************************************************************/
   IS
      CURSOR cur_vendor_list (
         p_supp_list_id1    NUMBER)
      IS
         SELECT ass.vendor_id
           FROM xxwc.xxwc_param_list_values lst, APPS.AP_SUPPLIERS ASS
          WHERE     lst.list_value = ass.segment1
                AND lst.list_id = p_supp_list_id1;

      x_err_mess1   VARCHAR2 (1000) := NULL;
   BEGIN
      FOR rec_vendor_list IN cur_vendor_list (p_supp_list_id)
      LOOP
         XXWC_BPA_ZONE_TEMP_TBL_PRC (rec_vendor_list.vendor_id,
                                     NULL,
                                     NULL,
                                     x_err_mess1);

         x_err_mess := x_err_mess || '' || x_err_mess1;
      END LOOP;
   EXCEPTION
      WHEN OTHERS
      THEN
         x_err_mess := SUBSTR (SQLERRM, 1, 500);
   END;

   PROCEDURE XXWC_BPA_PRC_ZONE_WEBADI_PRC (
      p_vendor_id        IN     NUMBER,
      p_vendor_site_id   IN     NUMBER,
      p_po_header_id     IN     NUMBER,
      x_err_message         OUT VARCHAR2)
   /***************************************************************************************************************************
   -- PROGRAM TYPE: PL/SQL Script   <API>
   --
   -- PURPOSE: procedure XXWC_BPA_PRC_ZONE_WEBADI_PRC to populate date into table XXWC.XXWC_BPA_PRC_ZONE_WEBADI_TMP_T
   -- HISTORY
   -- =========================================================================================================================
   -- =========================================================================================================================
   -- VERSION DATE          AUTHOR(S)       DESCRIPTION
   -- ------- -----------   --------------- -----------------------------------------------------------------------------------
   -- 1.0     01-Apr-2015   P.Vamshidhar    Created this procedure.
                                            TMS#20150414-00173-2015 PLM - Implement Cost Management tool enhancements
                                            package created to populate data into table XXWC.XXWC_BPA_PRC_ZONE_WEBADI_TMP_T.
   -- 1.2    29-Sep-2017   Nancy Pahwa   TMS#20160921-00270 - BPA Enhancements Mfg Part Number Addition
   ***************************************************************************************************************************/
   IS
      CURSOR cur_bpa_zone (
         p_master_org         NUMBER,
         p_org_id             NUMBER,
         p_vendor_id1         NUMBER,
         p_vendor_site_id1    NUMBER,
         p_po_header_id1      NUMBER)
      IS
           SELECT DISTINCT PO_NUMBER,
                           VENDOR_NAME,
                           VENDOR_NUM,
                           VENDOR_SITE_CODE,
                           ITEM_NUMBER,
                           ITEM_DESCRIPTION,
                           VENDOR_ITEM_NUMBER,
                           EXPIRATION_DATE,
                           NATIONAL_PRICE,
                           NATIONAL_QUANTITY,
                           NATIONAL_BREAK,
                           PROMO_PRICE,
                           PROMO_START_DATE,
                           PROMO_END_DATE,
                           PRICE_ZONE_PRICE1,
                           PRICE_ZONE_QUANTITY1,
                           PRICE_ZONE_QUANTITY_PRICE1,
                           PRICE_ZONE_PRICE2,
                           PRICE_ZONE_QUANTITY2,
                           PRICE_ZONE_QUANTITY_PRICE2,
                           PRICE_ZONE_PRICE3,
                           PRICE_ZONE_QUANTITY3,
                           PRICE_ZONE_QUANTITY_PRICE3,
                           PRICE_ZONE_PRICE4,
                           PRICE_ZONE_QUANTITY4,
                           PRICE_ZONE_QUANTITY_PRICE4,
                           PRICE_ZONE_PRICE5,
                           PRICE_ZONE_QUANTITY5,
                           PRICE_ZONE_QUANTITY_PRICE5,
                           PRICE_ZONE_PRICE6,
                           PRICE_ZONE_QUANTITY6,
                           PRICE_ZONE_QUANTITY_PRICE6,
                           PRICE_ZONE_PRICE7,
                           PRICE_ZONE_QUANTITY7,
                           PRICE_ZONE_QUANTITY_PRICE7,
                           PRICE_ZONE_PRICE8,
                           PRICE_ZONE_QUANTITY8,
                           PRICE_ZONE_QUANTITY_PRICE8,
                           PRICE_ZONE_PRICE9,
                           PRICE_ZONE_QUANTITY9,
                           PRICE_ZONE_QUANTITY_PRICE9,
                           PRICE_ZONE_PRICE10,
                           PRICE_ZONE_QUANTITY10,
                           PRICE_ZONE_QUANTITY_PRICE10,
                           PRICE_ZONE_PRICE11,
                           PRICE_ZONE_QUANTITY11,
                           PRICE_ZONE_QUANTITY_PRICE11,
                           PRICE_ZONE_PRICE12,
                           PRICE_ZONE_QUANTITY12,
                           PRICE_ZONE_QUANTITY_PRICE12,
                           PRICE_ZONE_PRICE13,
                           PRICE_ZONE_QUANTITY13,
                           PRICE_ZONE_QUANTITY_PRICE13,
                           PRICE_ZONE_PRICE14,
                           PRICE_ZONE_QUANTITY14,
                           PRICE_ZONE_QUANTITY_PRICE14,
                           PRICE_ZONE_PRICE15,
                           PRICE_ZONE_QUANTITY15,
                           PRICE_ZONE_QUANTITY_PRICE15,
                           PRICE_ZONE_PRICE16,
                           PRICE_ZONE_QUANTITY16,
                           PRICE_ZONE_QUANTITY_PRICE16,
                           PRICE_ZONE_PRICE17,
                           PRICE_ZONE_QUANTITY17,
                           PRICE_ZONE_QUANTITY_PRICE17,
                           PRICE_ZONE_PRICE18,
                           PRICE_ZONE_QUANTITY18,
                           PRICE_ZONE_QUANTITY_PRICE18,
                           PRICE_ZONE_PRICE19,
                           PRICE_ZONE_QUANTITY19,
                           PRICE_ZONE_QUANTITY_PRICE19,
                           PRICE_ZONE_PRICE20,
                           PRICE_ZONE_QUANTITY20,
                           PRICE_ZONE_QUANTITY_PRICE20,
                           PRICE_ZONE_PRICE21,
                           PRICE_ZONE_QUANTITY21,
                           PRICE_ZONE_QUANTITY_PRICE21,
                           PRICE_ZONE_PRICE22,
                           PRICE_ZONE_QUANTITY22,
                           PRICE_ZONE_QUANTITY_PRICE22,
                           PRICE_ZONE_PRICE23,
                           PRICE_ZONE_QUANTITY23,
                           PRICE_ZONE_QUANTITY_PRICE23,
                           PRICE_ZONE_PRICE24,
                           PRICE_ZONE_QUANTITY24,
                           PRICE_ZONE_QUANTITY_PRICE24,
                           PRICE_ZONE_PRICE25,
                           PRICE_ZONE_QUANTITY25,
                           PRICE_ZONE_QUANTITY_PRICE25,
                           PRICE_ZONE_PRICE26,
                           PRICE_ZONE_QUANTITY26,
                           PRICE_ZONE_QUANTITY_PRICE26,
                           PRICE_ZONE_PRICE27,
                           PRICE_ZONE_QUANTITY27,
                           PRICE_ZONE_QUANTITY_PRICE27,
                           PRICE_ZONE_PRICE28,
                           PRICE_ZONE_QUANTITY28,
                           PRICE_ZONE_QUANTITY_PRICE28,
                           PRICE_ZONE_PRICE29,
                           PRICE_ZONE_QUANTITY29,
                           PRICE_ZONE_QUANTITY_PRICE29,
                           PRICE_ZONE_PRICE30,
                           PRICE_ZONE_QUANTITY30,
                           PRICE_ZONE_QUANTITY_PRICE30,
                           ORG_PRICE_ZONE_1,
                           ORG_PRICE_ZONE_2,
                           ORG_PRICE_ZONE_3,
                           ORG_PRICE_ZONE_4,
                           ORG_PRICE_ZONE_5,
                           ORG_PRICE_ZONE_6,
                           ORG_PRICE_ZONE_7,
                           ORG_PRICE_ZONE_8,
                           ORG_PRICE_ZONE_9,
                           ORG_PRICE_ZONE_10,
                           ORG_PRICE_ZONE_11,
                           ORG_PRICE_ZONE_12,
                           ORG_PRICE_ZONE_13,
                           ORG_PRICE_ZONE_14,
                           ORG_PRICE_ZONE_15,
                           ORG_PRICE_ZONE_16,
                           ORG_PRICE_ZONE_17,
                           ORG_PRICE_ZONE_18,
                           ORG_PRICE_ZONE_19,
                           ORG_PRICE_ZONE_20,
                           ORG_PRICE_ZONE_21,
                           ORG_PRICE_ZONE_22,
                           ORG_PRICE_ZONE_23,
                           ORG_PRICE_ZONE_24,
                           ORG_PRICE_ZONE_25,
                           ORG_PRICE_ZONE_26,
                           ORG_PRICE_ZONE_27,
                           ORG_PRICE_ZONE_28,
                           ORG_PRICE_ZONE_29,
                           ORG_PRICE_ZONE_30,
                           PO_HEADER_ID,
                           INVENTORY_ITEM_ID,
                           VENDOR_ID,
                           VENDOR_SITE_ID,
                           VENDOR_CONTACT_ID,
                           IMPLEMENT_DATE,
                           PRICE_ZONE_QUANTITY,
                           ORG_ID
             FROM (SELECT pha.segment1 po_number,
                          ass.vendor_name,
                          ass.segment1 vendor_num,
                          assa.vendor_site_code,
                          msib.segment1 item_number,
                          msib.description item_description,
                          --1.2 start
                          (SELECT mcr.CROSS_REFERENCE
                          FROM apps.mtl_cross_references mcr, apps.ap_suppliers aps, apps.mtl_system_items_b msi
                          WHERE    1=1
                          AND msi.inventory_item_id = mcr.inventory_item_id
                          AND msi.organization_id = p_master_org
                         -- AND mcr.attribute_category = '162'
                          AND mcr.cross_reference_type = 'VENDOR'
                          AND mcr.attribute1 = aps.segment1
                          -- AND mcr.attribute1 = i_vendor_num
                          AND msi.segment1 = msib.segment1 --p_item_number
                          AND aps.vendor_name = ass.vendor_name--P_VENDOR_NAME
                          AND mcr.attribute1 is not null
                          AND ROWNUM = 1) --1.2 end
                             vendor_item_number,      --,msib.primary_uom_code
                          pla.expiration_date expiration_date,
                          NULL National_price,
                          NULL NATIONAL_QUANTITY,
                          NULL NATIONAL_BREAK,
                          xppt.promo_price,
                          xppt.promo_start_date,
                          xppt.promo_end_date,
                          NULL PRICE_ZONE_PRICE1,
                          NULL PRICE_ZONE_QUANTITY1,
                          NULL PRICE_ZONE_QUANTITY_PRICE1,
                          NULL PRICE_ZONE_PRICE2,
                          NULL PRICE_ZONE_QUANTITY2,
                          NULL PRICE_ZONE_QUANTITY_PRICE2,
                          NULL PRICE_ZONE_PRICE3,
                          NULL PRICE_ZONE_QUANTITY3,
                          NULL PRICE_ZONE_QUANTITY_PRICE3,
                          NULL PRICE_ZONE_PRICE4,
                          NULL PRICE_ZONE_QUANTITY4,
                          NULL PRICE_ZONE_QUANTITY_PRICE4,
                          NULL PRICE_ZONE_PRICE5,
                          NULL PRICE_ZONE_QUANTITY5,
                          NULL PRICE_ZONE_QUANTITY_PRICE5,
                          NULL PRICE_ZONE_PRICE6,
                          NULL PRICE_ZONE_QUANTITY6,
                          NULL PRICE_ZONE_QUANTITY_PRICE6,
                          NULL PRICE_ZONE_PRICE7,
                          NULL PRICE_ZONE_QUANTITY7,
                          NULL PRICE_ZONE_QUANTITY_PRICE7,
                          NULL PRICE_ZONE_PRICE8,
                          NULL PRICE_ZONE_QUANTITY8,
                          NULL PRICE_ZONE_QUANTITY_PRICE8,
                          NULL PRICE_ZONE_PRICE9,
                          NULL PRICE_ZONE_QUANTITY9,
                          NULL PRICE_ZONE_QUANTITY_PRICE9,
                          NULL PRICE_ZONE_PRICE10,
                          NULL PRICE_ZONE_QUANTITY10,
                          NULL PRICE_ZONE_QUANTITY_PRICE10,
                          NULL PRICE_ZONE_PRICE11,
                          NULL PRICE_ZONE_QUANTITY11,
                          NULL PRICE_ZONE_QUANTITY_PRICE11,
                          NULL PRICE_ZONE_PRICE12,
                          NULL PRICE_ZONE_QUANTITY12,
                          NULL PRICE_ZONE_QUANTITY_PRICE12,
                          NULL PRICE_ZONE_PRICE13,
                          NULL PRICE_ZONE_QUANTITY13,
                          NULL PRICE_ZONE_QUANTITY_PRICE13,
                          NULL PRICE_ZONE_PRICE14,
                          NULL PRICE_ZONE_QUANTITY14,
                          NULL PRICE_ZONE_QUANTITY_PRICE14,
                          NULL PRICE_ZONE_PRICE15,
                          NULL PRICE_ZONE_QUANTITY15,
                          NULL PRICE_ZONE_QUANTITY_PRICE15,
                          NULL PRICE_ZONE_PRICE16,
                          NULL PRICE_ZONE_QUANTITY16,
                          NULL PRICE_ZONE_QUANTITY_PRICE16,
                          NULL PRICE_ZONE_PRICE17,
                          NULL PRICE_ZONE_QUANTITY17,
                          NULL PRICE_ZONE_QUANTITY_PRICE17,
                          NULL PRICE_ZONE_PRICE18,
                          NULL PRICE_ZONE_QUANTITY18,
                          NULL PRICE_ZONE_QUANTITY_PRICE18,
                          NULL PRICE_ZONE_PRICE19,
                          NULL PRICE_ZONE_QUANTITY19,
                          NULL PRICE_ZONE_QUANTITY_PRICE19,
                          NULL PRICE_ZONE_PRICE20,
                          NULL PRICE_ZONE_QUANTITY20,
                          NULL PRICE_ZONE_QUANTITY_PRICE20,
                          NULL PRICE_ZONE_PRICE21,
                          NULL PRICE_ZONE_QUANTITY21,
                          NULL PRICE_ZONE_QUANTITY_PRICE21,
                          NULL PRICE_ZONE_PRICE22,
                          NULL PRICE_ZONE_QUANTITY22,
                          NULL PRICE_ZONE_QUANTITY_PRICE22,
                          NULL PRICE_ZONE_PRICE23,
                          NULL PRICE_ZONE_QUANTITY23,
                          NULL PRICE_ZONE_QUANTITY_PRICE23,
                          NULL PRICE_ZONE_PRICE24,
                          NULL PRICE_ZONE_QUANTITY24,
                          NULL PRICE_ZONE_QUANTITY_PRICE24,
                          NULL PRICE_ZONE_PRICE25,
                          NULL PRICE_ZONE_QUANTITY25,
                          NULL PRICE_ZONE_QUANTITY_PRICE25,
                          NULL PRICE_ZONE_PRICE26,
                          NULL PRICE_ZONE_QUANTITY26,
                          NULL PRICE_ZONE_QUANTITY_PRICE26,
                          NULL PRICE_ZONE_PRICE27,
                          NULL PRICE_ZONE_QUANTITY27,
                          NULL PRICE_ZONE_QUANTITY_PRICE27,
                          NULL PRICE_ZONE_PRICE28,
                          NULL PRICE_ZONE_QUANTITY28,
                          NULL PRICE_ZONE_QUANTITY_PRICE28,
                          NULL PRICE_ZONE_PRICE29,
                          NULL PRICE_ZONE_QUANTITY29,
                          NULL PRICE_ZONE_QUANTITY_PRICE29,
                          NULL PRICE_ZONE_PRICE30,
                          NULL PRICE_ZONE_QUANTITY30,
                          NULL PRICE_ZONE_QUANTITY_PRICE30,
                          NULL ORG_PRICE_ZONE_1,
                          NULL ORG_PRICE_ZONE_2,
                          NULL ORG_PRICE_ZONE_3,
                          NULL ORG_PRICE_ZONE_4,
                          NULL ORG_PRICE_ZONE_5,
                          NULL ORG_PRICE_ZONE_6,
                          NULL ORG_PRICE_ZONE_7,
                          NULL ORG_PRICE_ZONE_8,
                          NULL ORG_PRICE_ZONE_9,
                          NULL ORG_PRICE_ZONE_10,
                          NULL ORG_PRICE_ZONE_11,
                          NULL ORG_PRICE_ZONE_12,
                          NULL ORG_PRICE_ZONE_13,
                          NULL ORG_PRICE_ZONE_14,
                          NULL ORG_PRICE_ZONE_15,
                          NULL ORG_PRICE_ZONE_16,
                          NULL ORG_PRICE_ZONE_17,
                          NULL ORG_PRICE_ZONE_18,
                          NULL ORG_PRICE_ZONE_19,
                          NULL ORG_PRICE_ZONE_20,
                          NULL ORG_PRICE_ZONE_21,
                          NULL ORG_PRICE_ZONE_22,
                          NULL ORG_PRICE_ZONE_23,
                          NULL ORG_PRICE_ZONE_24,
                          NULL ORG_PRICE_ZONE_25,
                          NULL ORG_PRICE_ZONE_26,
                          NULL ORG_PRICE_ZONE_27,
                          NULL ORG_PRICE_ZONE_28,
                          NULL ORG_PRICE_ZONE_29,
                          NULL ORG_PRICE_ZONE_30,
                          pha.po_header_id,
                          msib.inventory_item_id,
                          pha.vendor_id,
                          pha.vendor_site_id,
                          pha.vendor_contact_id,
                          NULL implement_date,
                          xbpzqbv.price_zone_quantity,
                          pha.org_id
                     FROM apps.ap_suppliers ass,
                          apps.ap_supplier_sites_all assa,
                          apps.po_headers_all pha,
                          apps.po_lines_all pla,
                          apps.mtl_system_items_b msib,
                          xxwc.XXWC_BPA_PRICE_ZONE_TBL xppzt,
                          xxwc.XXWC_BPA_PROMO_TBL xppt,
                          XXWC_BPA_PRICE_ZONE_Q_BRKS_VW xbpzqbv
                    WHERE     ass.vendor_id = p_vendor_id1             --45374
                          AND assa.vendor_site_id =
                                 NVL (p_vendor_site_id1, assa.vendor_site_id)
                          AND ass.vendor_id = assa.vendor_id
                          AND pha.vendor_id = ass.vendor_id
                          AND pha.vendor_site_id = assa.vendor_site_id
                          AND pha.org_id = p_org_id
                          AND pha.org_id = assa.org_id
                          AND SYSDATE BETWEEN NVL (pha.start_date, SYSDATE - 1)
                                          AND NVL (pha.end_date, SYSDATE + 1)
                          AND pha.po_header_id = pla.po_header_id
                          AND pha.po_header_id = p_po_header_id1
                          AND NVL (pha.cancel_flag, 'N') = 'N'
                          AND NVL (pla.cancel_flag, 'N') = 'N'
                          AND pla.item_id = msib.inventory_item_id
                          AND msib.organization_id = p_master_org
                          AND pla.po_header_id = xppzt.po_header_id(+)
                          AND pla.item_id = xppzt.inventory_item_id(+)
                          AND pla.org_id = xppzt.org_id(+)
                          AND xppzt.po_header_id = xppt.po_header_id(+)
                          AND xppzt.inventory_item_id =
                                 xppt.inventory_item_id(+)
                          AND xppzt.org_id = xppt.org_id(+)
                          AND xppzt.po_header_id = xbpzqbv.po_header_id(+)
                          AND xppzt.inventory_item_id =
                                 xbpzqbv.inventory_item_id(+)
                   UNION ALL
                   SELECT pha.segment1 po_number,
                          ass.vendor_name,
                          ass.segment1 vendor_num,
                          assa.vendor_site_code,
                          NULL,                    --msib.segment1 item_number
                          NULL,            --msib.description item_description
                          NULL,                           --vendor_item_number
                          NULL expiration_date,          --pla.expiration_date
                          NULL National_price,
                          NULL NATIONAL_QUANTITY,
                          NULL NATIONAL_BREAK,
                          NULL,                             --xppt.promo_price
                          NULL,                        --xppt.promo_start_date
                          NULL,                          --xppt.promo_end_date
                          NULL PRICE_ZONE_PRICE1,
                          NULL PRICE_ZONE_QUANTITY1,
                          NULL PRICE_ZONE_QUANTITY_PRICE1,
                          NULL PRICE_ZONE_PRICE2,
                          NULL PRICE_ZONE_QUANTITY2,
                          NULL PRICE_ZONE_QUANTITY_PRICE2,
                          NULL PRICE_ZONE_PRICE3,
                          NULL PRICE_ZONE_QUANTITY3,
                          NULL PRICE_ZONE_QUANTITY_PRICE3,
                          NULL PRICE_ZONE_PRICE4,
                          NULL PRICE_ZONE_QUANTITY4,
                          NULL PRICE_ZONE_QUANTITY_PRICE4,
                          NULL PRICE_ZONE_PRICE5,
                          NULL PRICE_ZONE_QUANTITY5,
                          NULL PRICE_ZONE_QUANTITY_PRICE5,
                          NULL PRICE_ZONE_PRICE6,
                          NULL PRICE_ZONE_QUANTITY6,
                          NULL PRICE_ZONE_QUANTITY_PRICE6,
                          NULL PRICE_ZONE_PRICE7,
                          NULL PRICE_ZONE_QUANTITY7,
                          NULL PRICE_ZONE_QUANTITY_PRICE7,
                          NULL PRICE_ZONE_PRICE8,
                          NULL PRICE_ZONE_QUANTITY8,
                          NULL PRICE_ZONE_QUANTITY_PRICE8,
                          NULL PRICE_ZONE_PRICE9,
                          NULL PRICE_ZONE_QUANTITY9,
                          NULL PRICE_ZONE_QUANTITY_PRICE9,
                          NULL PRICE_ZONE_PRICE10,
                          NULL PRICE_ZONE_QUANTITY10,
                          NULL PRICE_ZONE_QUANTITY_PRICE10,
                          NULL PRICE_ZONE_PRICE11,
                          NULL PRICE_ZONE_QUANTITY11,
                          NULL PRICE_ZONE_QUANTITY_PRICE11,
                          NULL PRICE_ZONE_PRICE12,
                          NULL PRICE_ZONE_QUANTITY12,
                          NULL PRICE_ZONE_QUANTITY_PRICE12,
                          NULL PRICE_ZONE_PRICE13,
                          NULL PRICE_ZONE_QUANTITY13,
                          NULL PRICE_ZONE_QUANTITY_PRICE13,
                          NULL PRICE_ZONE_PRICE14,
                          NULL PRICE_ZONE_QUANTITY14,
                          NULL PRICE_ZONE_QUANTITY_PRICE14,
                          NULL PRICE_ZONE_PRICE15,
                          NULL PRICE_ZONE_QUANTITY15,
                          NULL PRICE_ZONE_QUANTITY_PRICE15,
                          NULL PRICE_ZONE_PRICE16,
                          NULL PRICE_ZONE_QUANTITY16,
                          NULL PRICE_ZONE_QUANTITY_PRICE16,
                          NULL PRICE_ZONE_PRICE17,
                          NULL PRICE_ZONE_QUANTITY17,
                          NULL PRICE_ZONE_QUANTITY_PRICE17,
                          NULL PRICE_ZONE_PRICE18,
                          NULL PRICE_ZONE_QUANTITY18,
                          NULL PRICE_ZONE_QUANTITY_PRICE18,
                          NULL PRICE_ZONE_PRICE19,
                          NULL PRICE_ZONE_QUANTITY19,
                          NULL PRICE_ZONE_QUANTITY_PRICE19,
                          NULL PRICE_ZONE_PRICE20,
                          NULL PRICE_ZONE_QUANTITY20,
                          NULL PRICE_ZONE_QUANTITY_PRICE20,
                          NULL PRICE_ZONE_PRICE21,
                          NULL PRICE_ZONE_QUANTITY21,
                          NULL PRICE_ZONE_QUANTITY_PRICE21,
                          NULL PRICE_ZONE_PRICE22,
                          NULL PRICE_ZONE_QUANTITY22,
                          NULL PRICE_ZONE_QUANTITY_PRICE22,
                          NULL PRICE_ZONE_PRICE23,
                          NULL PRICE_ZONE_QUANTITY23,
                          NULL PRICE_ZONE_QUANTITY_PRICE23,
                          NULL PRICE_ZONE_PRICE24,
                          NULL PRICE_ZONE_QUANTITY24,
                          NULL PRICE_ZONE_QUANTITY_PRICE24,
                          NULL PRICE_ZONE_PRICE25,
                          NULL PRICE_ZONE_QUANTITY25,
                          NULL PRICE_ZONE_QUANTITY_PRICE25,
                          NULL PRICE_ZONE_PRICE26,
                          NULL PRICE_ZONE_QUANTITY26,
                          NULL PRICE_ZONE_QUANTITY_PRICE26,
                          NULL PRICE_ZONE_PRICE27,
                          NULL PRICE_ZONE_QUANTITY27,
                          NULL PRICE_ZONE_QUANTITY_PRICE27,
                          NULL PRICE_ZONE_PRICE28,
                          NULL PRICE_ZONE_QUANTITY28,
                          NULL PRICE_ZONE_QUANTITY_PRICE28,
                          NULL PRICE_ZONE_PRICE29,
                          NULL PRICE_ZONE_QUANTITY29,
                          NULL PRICE_ZONE_QUANTITY_PRICE29,
                          NULL PRICE_ZONE_PRICE30,
                          NULL PRICE_ZONE_QUANTITY30,
                          NULL PRICE_ZONE_QUANTITY_PRICE30,
                          NULL ORG_PRICE_ZONE_1,
                          NULL ORG_PRICE_ZONE_2,
                          NULL ORG_PRICE_ZONE_3,
                          NULL ORG_PRICE_ZONE_4,
                          NULL ORG_PRICE_ZONE_5,
                          NULL ORG_PRICE_ZONE_6,
                          NULL ORG_PRICE_ZONE_7,
                          NULL ORG_PRICE_ZONE_8,
                          NULL ORG_PRICE_ZONE_9,
                          NULL ORG_PRICE_ZONE_10,
                          NULL ORG_PRICE_ZONE_11,
                          NULL ORG_PRICE_ZONE_12,
                          NULL ORG_PRICE_ZONE_13,
                          NULL ORG_PRICE_ZONE_14,
                          NULL ORG_PRICE_ZONE_15,
                          NULL ORG_PRICE_ZONE_16,
                          NULL ORG_PRICE_ZONE_17,
                          NULL ORG_PRICE_ZONE_18,
                          NULL ORG_PRICE_ZONE_19,
                          NULL ORG_PRICE_ZONE_20,
                          NULL ORG_PRICE_ZONE_21,
                          NULL ORG_PRICE_ZONE_22,
                          NULL ORG_PRICE_ZONE_23,
                          NULL ORG_PRICE_ZONE_24,
                          NULL ORG_PRICE_ZONE_25,
                          NULL ORG_PRICE_ZONE_26,
                          NULL ORG_PRICE_ZONE_27,
                          NULL ORG_PRICE_ZONE_28,
                          NULL ORG_PRICE_ZONE_29,
                          NULL ORG_PRICE_ZONE_30,
                          pha.po_header_id,                     --po_header_id
                          NULL,                       --msib.inventory_item_id
                          pha.vendor_id,
                          pha.vendor_site_id,
                          pha.vendor_contact_id,
                          NULL Implement_date,
                          NULL Price_zone_quantity,
                          pha.org_id
                     FROM apps.po_headers_all pha,
                          ap_suppliers ass,
                          apps.ap_supplier_sites_all assa
                    WHERE     pha.vendor_id = ass.vendor_id
                          AND ass.vendor_id = p_vendor_id1
                          AND pha.po_header_id = p_po_header_id1
                          AND pha.vendor_id = assa.vendor_id
                          AND pha.org_id = assa.org_id -- -- This condition added by Raghavendra on 19-Nov-2014 As per Canada OU WEB ADI Test
                          AND pha.org_id = p_org_id -- This condition added by Raghavendra on 19-Nov-2014 As per Canada OU WEB ADI Test
                          AND pha.vendor_site_id = assa.vendor_site_id
                          AND assa.vendor_site_id =
                                 NVL (p_vendor_site_id1, assa.vendor_site_id)
                          AND SYSDATE BETWEEN NVL (pha.start_date, SYSDATE - 1)
                                          AND NVL (pha.end_date, SYSDATE + 1)
                          AND NVL (pha.cancel_flag, 'N') = 'N'
                          AND NOT EXISTS
                                     (SELECT 1
                                        FROM apps.po_lines_all pla
                                       WHERE     pla.po_header_id =
                                                    pha.po_header_id
                                             AND pla.org_id = pha.org_id -- This condition added by Raghavendra on 19-Nov-2014 As per Canada OU WEB ADI Test
                                             AND pla.org_id = p_org_id -- This condition added by Raghavendra on 19-Nov-2014 As per Canada OU WEB ADI Test
                                             AND NVL (pla.cancel_flag, 'N') =
                                                    'N'
                                             AND SYSDATE <=
                                                    NVL (pla.expiration_date,
                                                         SYSDATE + 1)))
         ORDER BY po_header_id,
                  CASE WHEN expiration_date IS NULL THEN 0 ELSE 1 END,
                  item_number;

      CURSOR CUR_ZONE_PRICE (
         p_po_header_id    NUMBER,
         p_item_id         NUMBER,
         p_org_id          NUMBER)
      IS
         SELECT price_zone_price, price_zone
           FROM XXWC.xxwc_bpa_price_zone_tbl
          WHERE     po_header_id = p_po_header_id
                AND inventory_item_id = p_item_id
                AND org_id = p_org_id;

      CURSOR CUR_PRICE_BREAK (
         p_po_header_id    NUMBER,
         p_item_id         NUMBER,
         p_org_id          NUMBER)
      IS
         SELECT price_zone_quantity, price_zone_quantity_price, price_zone
           FROM XXWC.XXWC_BPA_PRICE_ZONE_BREAKS_TBL
          WHERE     po_header_id = p_po_header_id
                AND inventory_item_id = p_item_id
                AND org_id = p_org_id;

      CURSOR CUR_PRICE_ZONE_ORG (
         p_vendor_id         NUMBER,
         p_vendor_site_id    NUMBER)
      IS
           SELECT mp.organization_code, xvpz.price_zone
             FROM mtl_parameters mp, XXWC.XXWC_VENDOR_PRICE_ZONE_TBL xvpz
            WHERE     xvpz.vendor_id = p_vendor_id
                  AND xvpz.vendor_site_id = p_vendor_site_id
                  AND xvpz.organization_id = mp.organization_id
         ORDER BY mp.organization_code;

      TYPE type_bpa_zone IS TABLE OF cur_bpa_zone%ROWTYPE;

      type_bpa_zone_obj   type_bpa_zone;
      l_stage             NUMBER;

      --x_err_message       VARCHAR2 (1000);

      l_zone_num          NUMBER := NULL;
      l_zone_price        NUMBER := NULL;


      l_mast_org_id       MTL_PARAMETERS.ORGANIZATION_ID%TYPE
                             := FND_PROFILE.VALUE ('XXWC_ITEM_MASTER_ORG');
      l_org_id            HR_OPERATING_UNITS.ORGANIZATION_ID%TYPE
                             := APPS.fnd_profile.VALUE ('ORG_ID');
   BEGIN
      DELETE FROM XXWC_BPA_PRC_ZONE_WEBADI_TMP_T
            WHERE     vendor_id = p_vendor_id
                  AND vendor_site_id = p_vendor_site_id
                  AND po_header_id = p_po_header_id;

      OPEN cur_bpa_zone (l_mast_org_id,
                         l_org_id,
                         p_vendor_id,
                         p_vendor_site_id,
                         p_po_header_id);

      LOOP
         FETCH cur_bpa_zone BULK COLLECT INTO type_bpa_zone_obj LIMIT 10000;

         FOR l_stage IN type_bpa_zone_obj.FIRST .. type_bpa_zone_obj.LAST
         LOOP
            IF type_bpa_zone_obj (l_stage).inventory_item_id IS NOT NULL
            THEN
               OPEN CUR_ZONE_PRICE (type_bpa_zone_obj (l_stage).po_header_id,
                                    type_bpa_zone_obj (l_stage).inventory_item_id,
                                    l_org_id);

               LOOP
                  l_zone_num := NULL;
                  l_zone_price := NULL;

                  FETCH CUR_ZONE_PRICE INTO l_zone_price, l_zone_num;

                  IF l_zone_num = 0
                  THEN
                     type_bpa_zone_obj (l_stage).national_price :=
                        l_zone_price;
                  ELSIF l_zone_num = 1
                  THEN
                     type_bpa_zone_obj (l_stage).price_zone_price1 :=
                        l_zone_price;
                  ELSIF l_zone_num = 2
                  THEN
                     type_bpa_zone_obj (l_stage).price_zone_price2 :=
                        l_zone_price;
                  ELSIF l_zone_num = 3
                  THEN
                     type_bpa_zone_obj (l_stage).price_zone_price3 :=
                        l_zone_price;
                  ELSIF l_zone_num = 4
                  THEN
                     type_bpa_zone_obj (l_stage).price_zone_price4 :=
                        l_zone_price;
                  ELSIF l_zone_num = 5
                  THEN
                     type_bpa_zone_obj (l_stage).price_zone_price5 :=
                        l_zone_price;
                  ELSIF l_zone_num = 6
                  THEN
                     type_bpa_zone_obj (l_stage).price_zone_price6 :=
                        l_zone_price;
                  ELSIF l_zone_num = 7
                  THEN
                     type_bpa_zone_obj (l_stage).price_zone_price7 :=
                        l_zone_price;
                  ELSIF l_zone_num = 8
                  THEN
                     type_bpa_zone_obj (l_stage).price_zone_price8 :=
                        l_zone_price;
                  ELSIF l_zone_num = 9
                  THEN
                     type_bpa_zone_obj (l_stage).price_zone_price9 :=
                        l_zone_price;
                  ELSIF l_zone_num = 10
                  THEN
                     type_bpa_zone_obj (l_stage).price_zone_price10 :=
                        l_zone_price;
                  ELSIF l_zone_num = 11
                  THEN
                     type_bpa_zone_obj (l_stage).price_zone_price11 :=
                        l_zone_price;
                  ELSIF l_zone_num = 12
                  THEN
                     type_bpa_zone_obj (l_stage).price_zone_price12 :=
                        l_zone_price;
                  ELSIF l_zone_num = 13
                  THEN
                     type_bpa_zone_obj (l_stage).price_zone_price13 :=
                        l_zone_price;
                  ELSIF l_zone_num = 14
                  THEN
                     type_bpa_zone_obj (l_stage).price_zone_price14 :=
                        l_zone_price;
                  ELSIF l_zone_num = 15
                  THEN
                     type_bpa_zone_obj (l_stage).price_zone_price15 :=
                        l_zone_price;
                  ELSIF l_zone_num = 16
                  THEN
                     type_bpa_zone_obj (l_stage).price_zone_price16 :=
                        l_zone_price;
                  ELSIF l_zone_num = 17
                  THEN
                     type_bpa_zone_obj (l_stage).price_zone_price17 :=
                        l_zone_price;
                  ELSIF l_zone_num = 18
                  THEN
                     type_bpa_zone_obj (l_stage).price_zone_price18 :=
                        l_zone_price;
                  ELSIF l_zone_num = 19
                  THEN
                     type_bpa_zone_obj (l_stage).price_zone_price19 :=
                        l_zone_price;
                  ELSIF l_zone_num = 20
                  THEN
                     type_bpa_zone_obj (l_stage).price_zone_price20 :=
                        l_zone_price;
                  ELSIF l_zone_num = 21
                  THEN
                     type_bpa_zone_obj (l_stage).price_zone_price21 :=
                        l_zone_price;
                  ELSIF l_zone_num = 22
                  THEN
                     type_bpa_zone_obj (l_stage).price_zone_price22 :=
                        l_zone_price;
                  ELSIF l_zone_num = 23
                  THEN
                     type_bpa_zone_obj (l_stage).price_zone_price23 :=
                        l_zone_price;
                  ELSIF l_zone_num = 24
                  THEN
                     type_bpa_zone_obj (l_stage).price_zone_price24 :=
                        l_zone_price;
                  ELSIF l_zone_num = 25
                  THEN
                     type_bpa_zone_obj (l_stage).price_zone_price25 :=
                        l_zone_price;
                  ELSIF l_zone_num = 26
                  THEN
                     type_bpa_zone_obj (l_stage).price_zone_price26 :=
                        l_zone_price;
                  ELSIF l_zone_num = 27
                  THEN
                     type_bpa_zone_obj (l_stage).price_zone_price27 :=
                        l_zone_price;
                  ELSIF l_zone_num = 28
                  THEN
                     type_bpa_zone_obj (l_stage).price_zone_price28 :=
                        l_zone_price;
                  ELSIF l_zone_num = 29
                  THEN
                     type_bpa_zone_obj (l_stage).price_zone_price29 :=
                        l_zone_price;
                  ELSIF l_zone_num = 30
                  THEN
                     type_bpa_zone_obj (l_stage).price_zone_price30 :=
                        l_zone_price;
                  ELSE
                     NULL;
                  END IF;

                  EXIT WHEN CUR_ZONE_PRICE%NOTFOUND;
               END LOOP;

               CLOSE CUR_ZONE_PRICE;

               FOR REC_PRICE_BREAK
                  IN CUR_PRICE_BREAK (
                        type_bpa_zone_obj (l_stage).po_header_id,
                        type_bpa_zone_obj (l_stage).inventory_item_id,
                        l_org_id)
               LOOP
                  IF NVL (REC_PRICE_BREAK.price_zone_quantity, 0) =
                        NVL (type_bpa_zone_obj (l_stage).price_zone_quantity,
                             0)
                  THEN
                     IF REC_PRICE_BREAK.price_zone = 0
                     THEN
                        type_bpa_zone_obj (l_stage).NATIONAL_QUANTITY :=
                           NVL (REC_PRICE_BREAK.price_zone_quantity, 0);
                        type_bpa_zone_obj (l_stage).NATIONAL_BREAK :=
                           rec_price_break.price_zone_quantity_price;
                     ELSIF REC_PRICE_BREAK.price_zone = 1
                     THEN
                        type_bpa_zone_obj (l_stage).PRICE_ZONE_QUANTITY1 :=
                           NVL (REC_PRICE_BREAK.price_zone_quantity, 0);
                        type_bpa_zone_obj (l_stage).PRICE_ZONE_QUANTITY_PRICE1 :=
                           rec_price_break.price_zone_quantity_price;
                     ELSIF REC_PRICE_BREAK.price_zone = 2
                     THEN
                        type_bpa_zone_obj (l_stage).PRICE_ZONE_QUANTITY2 :=
                           NVL (REC_PRICE_BREAK.price_zone_quantity, 0);
                        type_bpa_zone_obj (l_stage).PRICE_ZONE_QUANTITY_PRICE2 :=
                           rec_price_break.price_zone_quantity_price;
                     ELSIF REC_PRICE_BREAK.price_zone = 3
                     THEN
                        type_bpa_zone_obj (l_stage).PRICE_ZONE_QUANTITY3 :=
                           NVL (REC_PRICE_BREAK.price_zone_quantity, 0);
                        type_bpa_zone_obj (l_stage).PRICE_ZONE_QUANTITY_PRICE3 :=
                           rec_price_break.price_zone_quantity_price;
                     ELSIF REC_PRICE_BREAK.price_zone = 4
                     THEN
                        type_bpa_zone_obj (l_stage).PRICE_ZONE_QUANTITY4 :=
                           NVL (REC_PRICE_BREAK.price_zone_quantity, 0);
                        type_bpa_zone_obj (l_stage).PRICE_ZONE_QUANTITY_PRICE4 :=
                           rec_price_break.price_zone_quantity_price;
                     ELSIF REC_PRICE_BREAK.price_zone = 5
                     THEN
                        type_bpa_zone_obj (l_stage).PRICE_ZONE_QUANTITY5 :=
                           NVL (REC_PRICE_BREAK.price_zone_quantity, 0);
                        type_bpa_zone_obj (l_stage).PRICE_ZONE_QUANTITY_PRICE5 :=
                           rec_price_break.price_zone_quantity_price;
                     ELSIF REC_PRICE_BREAK.price_zone = 6
                     THEN
                        type_bpa_zone_obj (l_stage).PRICE_ZONE_QUANTITY6 :=
                           NVL (REC_PRICE_BREAK.price_zone_quantity, 0);
                        type_bpa_zone_obj (l_stage).PRICE_ZONE_QUANTITY_PRICE6 :=
                           rec_price_break.price_zone_quantity_price;
                     ELSIF REC_PRICE_BREAK.price_zone = 7
                     THEN
                        type_bpa_zone_obj (l_stage).PRICE_ZONE_QUANTITY7 :=
                           NVL (REC_PRICE_BREAK.price_zone_quantity, 0);
                        type_bpa_zone_obj (l_stage).PRICE_ZONE_QUANTITY_PRICE7 :=
                           rec_price_break.price_zone_quantity_price;
                     ELSIF REC_PRICE_BREAK.price_zone = 8
                     THEN
                        type_bpa_zone_obj (l_stage).PRICE_ZONE_QUANTITY8 :=
                           NVL (REC_PRICE_BREAK.price_zone_quantity, 0);
                        type_bpa_zone_obj (l_stage).PRICE_ZONE_QUANTITY_PRICE8 :=
                           rec_price_break.price_zone_quantity_price;
                     ELSIF REC_PRICE_BREAK.price_zone = 9
                     THEN
                        type_bpa_zone_obj (l_stage).PRICE_ZONE_QUANTITY9 :=
                           NVL (REC_PRICE_BREAK.price_zone_quantity, 0);
                        type_bpa_zone_obj (l_stage).PRICE_ZONE_QUANTITY_PRICE9 :=
                           rec_price_break.price_zone_quantity_price;
                     ELSIF REC_PRICE_BREAK.price_zone = 10
                     THEN
                        type_bpa_zone_obj (l_stage).PRICE_ZONE_QUANTITY10 :=
                           NVL (REC_PRICE_BREAK.price_zone_quantity, 0);
                        type_bpa_zone_obj (l_stage).PRICE_ZONE_QUANTITY_PRICE10 :=
                           rec_price_break.price_zone_quantity_price;
                     ELSIF REC_PRICE_BREAK.price_zone = 11
                     THEN
                        type_bpa_zone_obj (l_stage).PRICE_ZONE_QUANTITY11 :=
                           NVL (REC_PRICE_BREAK.price_zone_quantity, 0);
                        type_bpa_zone_obj (l_stage).PRICE_ZONE_QUANTITY_PRICE11 :=
                           rec_price_break.price_zone_quantity_price;
                     ELSIF REC_PRICE_BREAK.price_zone = 12
                     THEN
                        type_bpa_zone_obj (l_stage).PRICE_ZONE_QUANTITY12 :=
                           NVL (REC_PRICE_BREAK.price_zone_quantity, 0);
                        type_bpa_zone_obj (l_stage).PRICE_ZONE_QUANTITY_PRICE12 :=
                           rec_price_break.price_zone_quantity_price;
                     ELSIF REC_PRICE_BREAK.price_zone = 13
                     THEN
                        type_bpa_zone_obj (l_stage).PRICE_ZONE_QUANTITY13 :=
                           NVL (REC_PRICE_BREAK.price_zone_quantity, 0);
                        type_bpa_zone_obj (l_stage).PRICE_ZONE_QUANTITY_PRICE13 :=
                           rec_price_break.price_zone_quantity_price;
                     ELSIF REC_PRICE_BREAK.price_zone = 14
                     THEN
                        type_bpa_zone_obj (l_stage).PRICE_ZONE_QUANTITY14 :=
                           NVL (REC_PRICE_BREAK.price_zone_quantity, 0);
                        type_bpa_zone_obj (l_stage).PRICE_ZONE_QUANTITY_PRICE14 :=
                           rec_price_break.price_zone_quantity_price;
                     ELSIF REC_PRICE_BREAK.price_zone = 15
                     THEN
                        type_bpa_zone_obj (l_stage).PRICE_ZONE_QUANTITY15 :=
                           NVL (REC_PRICE_BREAK.price_zone_quantity, 0);
                        type_bpa_zone_obj (l_stage).PRICE_ZONE_QUANTITY_PRICE15 :=
                           rec_price_break.price_zone_quantity_price;
                     ELSIF REC_PRICE_BREAK.price_zone = 16
                     THEN
                        type_bpa_zone_obj (l_stage).PRICE_ZONE_QUANTITY16 :=
                           NVL (REC_PRICE_BREAK.price_zone_quantity, 0);
                        type_bpa_zone_obj (l_stage).PRICE_ZONE_QUANTITY_PRICE16 :=
                           rec_price_break.price_zone_quantity_price;
                     ELSIF REC_PRICE_BREAK.price_zone = 17
                     THEN
                        type_bpa_zone_obj (l_stage).PRICE_ZONE_QUANTITY17 :=
                           NVL (REC_PRICE_BREAK.price_zone_quantity, 0);
                        type_bpa_zone_obj (l_stage).PRICE_ZONE_QUANTITY_PRICE17 :=
                           rec_price_break.price_zone_quantity_price;
                     ELSIF REC_PRICE_BREAK.price_zone = 18
                     THEN
                        type_bpa_zone_obj (l_stage).PRICE_ZONE_QUANTITY18 :=
                           NVL (REC_PRICE_BREAK.price_zone_quantity, 0);
                        type_bpa_zone_obj (l_stage).PRICE_ZONE_QUANTITY_PRICE18 :=
                           rec_price_break.price_zone_quantity_price;
                     ELSIF REC_PRICE_BREAK.price_zone = 19
                     THEN
                        type_bpa_zone_obj (l_stage).PRICE_ZONE_QUANTITY19 :=
                           NVL (REC_PRICE_BREAK.price_zone_quantity, 0);
                        type_bpa_zone_obj (l_stage).PRICE_ZONE_QUANTITY_PRICE19 :=
                           rec_price_break.price_zone_quantity_price;
                     ELSIF REC_PRICE_BREAK.price_zone = 20
                     THEN
                        type_bpa_zone_obj (l_stage).PRICE_ZONE_QUANTITY20 :=
                           NVL (REC_PRICE_BREAK.price_zone_quantity, 0);
                        type_bpa_zone_obj (l_stage).PRICE_ZONE_QUANTITY_PRICE20 :=
                           rec_price_break.price_zone_quantity_price;
                     ELSIF REC_PRICE_BREAK.price_zone = 21
                     THEN
                        type_bpa_zone_obj (l_stage).PRICE_ZONE_QUANTITY21 :=
                           NVL (REC_PRICE_BREAK.price_zone_quantity, 0);
                        type_bpa_zone_obj (l_stage).PRICE_ZONE_QUANTITY_PRICE21 :=
                           rec_price_break.price_zone_quantity_price;
                     ELSIF REC_PRICE_BREAK.price_zone = 22
                     THEN
                        type_bpa_zone_obj (l_stage).PRICE_ZONE_QUANTITY22 :=
                           NVL (REC_PRICE_BREAK.price_zone_quantity, 0);
                        type_bpa_zone_obj (l_stage).PRICE_ZONE_QUANTITY_PRICE22 :=
                           rec_price_break.price_zone_quantity_price;
                     ELSIF REC_PRICE_BREAK.price_zone = 23
                     THEN
                        type_bpa_zone_obj (l_stage).PRICE_ZONE_QUANTITY23 :=
                           NVL (REC_PRICE_BREAK.price_zone_quantity, 0);
                        type_bpa_zone_obj (l_stage).PRICE_ZONE_QUANTITY_PRICE23 :=
                           rec_price_break.price_zone_quantity_price;
                     ELSIF REC_PRICE_BREAK.price_zone = 24
                     THEN
                        type_bpa_zone_obj (l_stage).PRICE_ZONE_QUANTITY24 :=
                           NVL (REC_PRICE_BREAK.price_zone_quantity, 0);
                        type_bpa_zone_obj (l_stage).PRICE_ZONE_QUANTITY_PRICE24 :=
                           rec_price_break.price_zone_quantity_price;
                     ELSIF REC_PRICE_BREAK.price_zone = 25
                     THEN
                        type_bpa_zone_obj (l_stage).PRICE_ZONE_QUANTITY25 :=
                           NVL (REC_PRICE_BREAK.price_zone_quantity, 0);
                        type_bpa_zone_obj (l_stage).PRICE_ZONE_QUANTITY_PRICE25 :=
                           rec_price_break.price_zone_quantity_price;
                     ELSIF REC_PRICE_BREAK.price_zone = 26
                     THEN
                        type_bpa_zone_obj (l_stage).PRICE_ZONE_QUANTITY26 :=
                           NVL (REC_PRICE_BREAK.price_zone_quantity, 0);
                        type_bpa_zone_obj (l_stage).PRICE_ZONE_QUANTITY_PRICE26 :=
                           rec_price_break.price_zone_quantity_price;
                     ELSIF REC_PRICE_BREAK.price_zone = 27
                     THEN
                        type_bpa_zone_obj (l_stage).PRICE_ZONE_QUANTITY27 :=
                           NVL (REC_PRICE_BREAK.price_zone_quantity, 0);
                        type_bpa_zone_obj (l_stage).PRICE_ZONE_QUANTITY_PRICE27 :=
                           rec_price_break.price_zone_quantity_price;
                     ELSIF REC_PRICE_BREAK.price_zone = 28
                     THEN
                        type_bpa_zone_obj (l_stage).PRICE_ZONE_QUANTITY28 :=
                           NVL (REC_PRICE_BREAK.price_zone_quantity, 0);
                        type_bpa_zone_obj (l_stage).PRICE_ZONE_QUANTITY_PRICE28 :=
                           rec_price_break.price_zone_quantity_price;
                     ELSIF REC_PRICE_BREAK.price_zone = 29
                     THEN
                        type_bpa_zone_obj (l_stage).PRICE_ZONE_QUANTITY29 :=
                           NVL (REC_PRICE_BREAK.price_zone_quantity, 0);
                        type_bpa_zone_obj (l_stage).PRICE_ZONE_QUANTITY_PRICE29 :=
                           rec_price_break.price_zone_quantity_price;
                     ELSIF REC_PRICE_BREAK.price_zone = 30
                     THEN
                        type_bpa_zone_obj (l_stage).PRICE_ZONE_QUANTITY30 :=
                           NVL (REC_PRICE_BREAK.price_zone_quantity, 0);
                        type_bpa_zone_obj (l_stage).PRICE_ZONE_QUANTITY_PRICE30 :=
                           rec_price_break.price_zone_quantity_price;
                     ELSE
                        NULL;
                     END IF;
                  END IF;
               END LOOP;
            END IF;

            FOR REC_PRICE_ZONE_ORG
               IN CUR_PRICE_ZONE_ORG (
                     type_bpa_zone_obj (l_stage).vendor_id,
                     type_bpa_zone_obj (l_stage).vendor_site_id)
            LOOP
               IF REC_PRICE_ZONE_ORG.price_zone = 1
               THEN
                  IF type_bpa_zone_obj (l_stage).ORG_PRICE_ZONE_1 IS NULL
                  THEN
                     type_bpa_zone_obj (l_stage).ORG_PRICE_ZONE_1 :=
                        REC_PRICE_ZONE_ORG.organization_code;
                  ELSE
                     type_bpa_zone_obj (l_stage).ORG_PRICE_ZONE_1 :=
                           type_bpa_zone_obj (l_stage).ORG_PRICE_ZONE_1
                        || ', '
                        || REC_PRICE_ZONE_ORG.organization_code;
                  END IF;
               ELSIF REC_PRICE_ZONE_ORG.price_zone = 2
               THEN
                  IF type_bpa_zone_obj (l_stage).ORG_PRICE_ZONE_2 IS NULL
                  THEN
                     type_bpa_zone_obj (l_stage).ORG_PRICE_ZONE_2 :=
                        REC_PRICE_ZONE_ORG.organization_code;
                  ELSE
                     type_bpa_zone_obj (l_stage).ORG_PRICE_ZONE_2 :=
                           type_bpa_zone_obj (l_stage).ORG_PRICE_ZONE_2
                        || ', '
                        || REC_PRICE_ZONE_ORG.organization_code;
                  END IF;
               ELSIF REC_PRICE_ZONE_ORG.price_zone = 3
               THEN
                  IF type_bpa_zone_obj (l_stage).org_price_zone_3 IS NULL
                  THEN
                     type_bpa_zone_obj (l_stage).org_price_zone_3 :=
                        REC_PRICE_ZONE_ORG.organization_code;
                  ELSE
                     type_bpa_zone_obj (l_stage).org_price_zone_3 :=
                           type_bpa_zone_obj (l_stage).ORG_price_zone_3
                        || ', '
                        || REC_PRICE_ZONE_ORG.organization_code;
                  END IF;
               ELSIF REC_PRICE_ZONE_ORG.price_zone = 4
               THEN
                  IF type_bpa_zone_obj (l_stage).ORG_price_zone_4 IS NULL
                  THEN
                     type_bpa_zone_obj (l_stage).ORG_price_zone_4 :=
                        REC_PRICE_ZONE_ORG.organization_code;
                  ELSE
                     type_bpa_zone_obj (l_stage).org_price_zone_4 :=
                           type_bpa_zone_obj (l_stage).org_price_zone_4
                        || ', '
                        || REC_PRICE_ZONE_ORG.organization_code;
                  END IF;
               ELSIF REC_PRICE_ZONE_ORG.price_zone = 5
               THEN
                  IF type_bpa_zone_obj (l_stage).org_price_zone_5 IS NULL
                  THEN
                     type_bpa_zone_obj (l_stage).org_price_zone_5 :=
                        REC_PRICE_ZONE_ORG.organization_code;
                  ELSE
                     type_bpa_zone_obj (l_stage).org_price_zone_5 :=
                           type_bpa_zone_obj (l_stage).org_price_zone_5
                        || ', '
                        || REC_PRICE_ZONE_ORG.organization_code;
                  END IF;
               ELSIF REC_PRICE_ZONE_ORG.price_zone = 6
               THEN
                  IF type_bpa_zone_obj (l_stage).org_price_zone_6 IS NULL
                  THEN
                     type_bpa_zone_obj (l_stage).org_price_zone_6 :=
                        REC_PRICE_ZONE_ORG.organization_code;
                  ELSE
                     type_bpa_zone_obj (l_stage).org_price_zone_6 :=
                           type_bpa_zone_obj (l_stage).org_price_zone_6
                        || ', '
                        || REC_PRICE_ZONE_ORG.organization_code;
                  END IF;
               ELSIF REC_PRICE_ZONE_ORG.price_zone = 7
               THEN
                  IF type_bpa_zone_obj (l_stage).org_price_zone_7 IS NULL
                  THEN
                     type_bpa_zone_obj (l_stage).org_price_zone_7 :=
                        REC_PRICE_ZONE_ORG.organization_code;
                  ELSE
                     type_bpa_zone_obj (l_stage).org_price_zone_7 :=
                           type_bpa_zone_obj (l_stage).org_price_zone_7
                        || ', '
                        || REC_PRICE_ZONE_ORG.organization_code;
                  END IF;
               ELSIF REC_PRICE_ZONE_ORG.price_zone = 8
               THEN
                  IF type_bpa_zone_obj (l_stage).org_price_zone_8 IS NULL
                  THEN
                     type_bpa_zone_obj (l_stage).org_price_zone_8 :=
                        REC_PRICE_ZONE_ORG.organization_code;
                  ELSE
                     type_bpa_zone_obj (l_stage).org_price_zone_8 :=
                           type_bpa_zone_obj (l_stage).org_price_zone_8
                        || ', '
                        || REC_PRICE_ZONE_ORG.organization_code;
                  END IF;
               ELSIF REC_PRICE_ZONE_ORG.price_zone = 9
               THEN
                  IF type_bpa_zone_obj (l_stage).org_price_zone_9 IS NULL
                  THEN
                     type_bpa_zone_obj (l_stage).org_price_zone_9 :=
                        REC_PRICE_ZONE_ORG.organization_code;
                  ELSE
                     type_bpa_zone_obj (l_stage).org_price_zone_9 :=
                           type_bpa_zone_obj (l_stage).org_price_zone_9
                        || ', '
                        || REC_PRICE_ZONE_ORG.organization_code;
                  END IF;
               ELSIF REC_PRICE_ZONE_ORG.price_zone = 10
               THEN
                  IF type_bpa_zone_obj (l_stage).ORG_PRICE_ZONE_10 IS NULL
                  THEN
                     type_bpa_zone_obj (l_stage).ORG_PRICE_ZONE_10 :=
                        REC_PRICE_ZONE_ORG.organization_code;
                  ELSE
                     type_bpa_zone_obj (l_stage).ORG_PRICE_ZONE_10 :=
                           type_bpa_zone_obj (l_stage).ORG_PRICE_ZONE_10
                        || ', '
                        || REC_PRICE_ZONE_ORG.organization_code;
                  END IF;
               ELSIF REC_PRICE_ZONE_ORG.price_zone = 11
               THEN
                  IF type_bpa_zone_obj (l_stage).ORG_PRICE_ZONE_11 IS NULL
                  THEN
                     type_bpa_zone_obj (l_stage).ORG_PRICE_ZONE_11 :=
                        REC_PRICE_ZONE_ORG.organization_code;
                  ELSE
                     type_bpa_zone_obj (l_stage).ORG_PRICE_ZONE_11 :=
                           type_bpa_zone_obj (l_stage).ORG_PRICE_ZONE_11
                        || ', '
                        || REC_PRICE_ZONE_ORG.organization_code;
                  END IF;
               ELSIF REC_PRICE_ZONE_ORG.price_zone = 12
               THEN
                  IF type_bpa_zone_obj (l_stage).ORG_PRICE_ZONE_12 IS NULL
                  THEN
                     type_bpa_zone_obj (l_stage).ORG_PRICE_ZONE_12 :=
                        REC_PRICE_ZONE_ORG.organization_code;
                  ELSE
                     type_bpa_zone_obj (l_stage).ORG_PRICE_ZONE_12 :=
                           type_bpa_zone_obj (l_stage).ORG_PRICE_ZONE_12
                        || ', '
                        || REC_PRICE_ZONE_ORG.organization_code;
                  END IF;
               ELSIF REC_PRICE_ZONE_ORG.price_zone = 13
               THEN
                  IF type_bpa_zone_obj (l_stage).ORG_PRICE_ZONE_13 IS NULL
                  THEN
                     type_bpa_zone_obj (l_stage).ORG_PRICE_ZONE_13 :=
                        REC_PRICE_ZONE_ORG.organization_code;
                  ELSE
                     type_bpa_zone_obj (l_stage).ORG_PRICE_ZONE_13 :=
                           type_bpa_zone_obj (l_stage).ORG_PRICE_ZONE_13
                        || ', '
                        || REC_PRICE_ZONE_ORG.organization_code;
                  END IF;
               ELSIF REC_PRICE_ZONE_ORG.price_zone = 14
               THEN
                  IF type_bpa_zone_obj (l_stage).ORG_PRICE_ZONE_14 IS NULL
                  THEN
                     type_bpa_zone_obj (l_stage).ORG_PRICE_ZONE_14 :=
                        REC_PRICE_ZONE_ORG.organization_code;
                  ELSE
                     type_bpa_zone_obj (l_stage).ORG_PRICE_ZONE_14 :=
                           type_bpa_zone_obj (l_stage).ORG_PRICE_ZONE_14
                        || ', '
                        || REC_PRICE_ZONE_ORG.organization_code;
                  END IF;
               ELSIF REC_PRICE_ZONE_ORG.price_zone = 15
               THEN
                  IF type_bpa_zone_obj (l_stage).ORG_PRICE_ZONE_15 IS NULL
                  THEN
                     type_bpa_zone_obj (l_stage).ORG_PRICE_ZONE_15 :=
                        REC_PRICE_ZONE_ORG.organization_code;
                  ELSE
                     type_bpa_zone_obj (l_stage).ORG_PRICE_ZONE_15 :=
                           type_bpa_zone_obj (l_stage).ORG_PRICE_ZONE_15
                        || ', '
                        || REC_PRICE_ZONE_ORG.organization_code;
                  END IF;
               ELSIF REC_PRICE_ZONE_ORG.price_zone = 16
               THEN
                  IF type_bpa_zone_obj (l_stage).ORG_PRICE_ZONE_16 IS NULL
                  THEN
                     type_bpa_zone_obj (l_stage).ORG_PRICE_ZONE_16 :=
                        REC_PRICE_ZONE_ORG.organization_code;
                  ELSE
                     type_bpa_zone_obj (l_stage).ORG_PRICE_ZONE_16 :=
                           type_bpa_zone_obj (l_stage).ORG_PRICE_ZONE_16
                        || ', '
                        || REC_PRICE_ZONE_ORG.organization_code;
                  END IF;
               ELSIF REC_PRICE_ZONE_ORG.price_zone = 17
               THEN
                  IF type_bpa_zone_obj (l_stage).ORG_PRICE_ZONE_17 IS NULL
                  THEN
                     type_bpa_zone_obj (l_stage).ORG_PRICE_ZONE_17 :=
                        REC_PRICE_ZONE_ORG.organization_code;
                  ELSE
                     type_bpa_zone_obj (l_stage).ORG_PRICE_ZONE_17 :=
                           type_bpa_zone_obj (l_stage).ORG_PRICE_ZONE_17
                        || ', '
                        || REC_PRICE_ZONE_ORG.organization_code;
                  END IF;
               ELSIF REC_PRICE_ZONE_ORG.price_zone = 18
               THEN
                  IF type_bpa_zone_obj (l_stage).ORG_PRICE_ZONE_18 IS NULL
                  THEN
                     type_bpa_zone_obj (l_stage).ORG_PRICE_ZONE_18 :=
                        REC_PRICE_ZONE_ORG.organization_code;
                  ELSE
                     type_bpa_zone_obj (l_stage).ORG_PRICE_ZONE_18 :=
                           type_bpa_zone_obj (l_stage).ORG_PRICE_ZONE_18
                        || ', '
                        || REC_PRICE_ZONE_ORG.organization_code;
                  END IF;
               ELSIF REC_PRICE_ZONE_ORG.price_zone = 19
               THEN
                  IF type_bpa_zone_obj (l_stage).ORG_PRICE_ZONE_19 IS NULL
                  THEN
                     type_bpa_zone_obj (l_stage).ORG_PRICE_ZONE_19 :=
                        REC_PRICE_ZONE_ORG.organization_code;
                  ELSE
                     type_bpa_zone_obj (l_stage).ORG_PRICE_ZONE_19 :=
                           type_bpa_zone_obj (l_stage).ORG_PRICE_ZONE_19
                        || ', '
                        || REC_PRICE_ZONE_ORG.organization_code;
                  END IF;
               ELSIF REC_PRICE_ZONE_ORG.price_zone = 20
               THEN
                  IF type_bpa_zone_obj (l_stage).ORG_PRICE_ZONE_20 IS NULL
                  THEN
                     type_bpa_zone_obj (l_stage).ORG_PRICE_ZONE_20 :=
                        REC_PRICE_ZONE_ORG.organization_code;
                  ELSE
                     type_bpa_zone_obj (l_stage).ORG_PRICE_ZONE_20 :=
                           type_bpa_zone_obj (l_stage).ORG_PRICE_ZONE_20
                        || ', '
                        || REC_PRICE_ZONE_ORG.organization_code;
                  END IF;
               ELSIF REC_PRICE_ZONE_ORG.price_zone = 21
               THEN
                  IF type_bpa_zone_obj (l_stage).ORG_PRICE_ZONE_21 IS NULL
                  THEN
                     type_bpa_zone_obj (l_stage).ORG_PRICE_ZONE_21 :=
                        REC_PRICE_ZONE_ORG.organization_code;
                  ELSE
                     type_bpa_zone_obj (l_stage).ORG_PRICE_ZONE_21 :=
                           type_bpa_zone_obj (l_stage).ORG_PRICE_ZONE_21
                        || ', '
                        || REC_PRICE_ZONE_ORG.organization_code;
                  END IF;
               ELSIF REC_PRICE_ZONE_ORG.price_zone = 22
               THEN
                  IF type_bpa_zone_obj (l_stage).ORG_PRICE_ZONE_22 IS NULL
                  THEN
                     type_bpa_zone_obj (l_stage).ORG_PRICE_ZONE_22 :=
                        REC_PRICE_ZONE_ORG.organization_code;
                  ELSE
                     type_bpa_zone_obj (l_stage).ORG_PRICE_ZONE_22 :=
                           type_bpa_zone_obj (l_stage).ORG_PRICE_ZONE_22
                        || ', '
                        || REC_PRICE_ZONE_ORG.organization_code;
                  END IF;
               ELSIF REC_PRICE_ZONE_ORG.price_zone = 23
               THEN
                  IF type_bpa_zone_obj (l_stage).ORG_PRICE_ZONE_23 IS NULL
                  THEN
                     type_bpa_zone_obj (l_stage).ORG_PRICE_ZONE_23 :=
                        REC_PRICE_ZONE_ORG.organization_code;
                  ELSE
                     type_bpa_zone_obj (l_stage).ORG_PRICE_ZONE_23 :=
                           type_bpa_zone_obj (l_stage).ORG_PRICE_ZONE_23
                        || ', '
                        || REC_PRICE_ZONE_ORG.organization_code;
                  END IF;
               ELSIF REC_PRICE_ZONE_ORG.price_zone = 24
               THEN
                  IF type_bpa_zone_obj (l_stage).ORG_PRICE_ZONE_24 IS NULL
                  THEN
                     type_bpa_zone_obj (l_stage).ORG_PRICE_ZONE_24 :=
                        REC_PRICE_ZONE_ORG.organization_code;
                  ELSE
                     type_bpa_zone_obj (l_stage).ORG_PRICE_ZONE_24 :=
                           type_bpa_zone_obj (l_stage).ORG_PRICE_ZONE_24
                        || ', '
                        || REC_PRICE_ZONE_ORG.organization_code;
                  END IF;
               ELSIF REC_PRICE_ZONE_ORG.price_zone = 25
               THEN
                  IF type_bpa_zone_obj (l_stage).ORG_PRICE_ZONE_25 IS NULL
                  THEN
                     type_bpa_zone_obj (l_stage).ORG_PRICE_ZONE_25 :=
                        REC_PRICE_ZONE_ORG.organization_code;
                  ELSE
                     type_bpa_zone_obj (l_stage).ORG_PRICE_ZONE_25 :=
                           type_bpa_zone_obj (l_stage).ORG_PRICE_ZONE_25
                        || ', '
                        || REC_PRICE_ZONE_ORG.organization_code;
                  END IF;
               ELSIF REC_PRICE_ZONE_ORG.price_zone = 26
               THEN
                  IF type_bpa_zone_obj (l_stage).ORG_PRICE_ZONE_26 IS NULL
                  THEN
                     type_bpa_zone_obj (l_stage).ORG_PRICE_ZONE_26 :=
                        REC_PRICE_ZONE_ORG.organization_code;
                  ELSE
                     type_bpa_zone_obj (l_stage).ORG_PRICE_ZONE_26 :=
                           type_bpa_zone_obj (l_stage).ORG_PRICE_ZONE_26
                        || ', '
                        || REC_PRICE_ZONE_ORG.organization_code;
                  END IF;
               ELSIF REC_PRICE_ZONE_ORG.price_zone = 27
               THEN
                  IF type_bpa_zone_obj (l_stage).ORG_PRICE_ZONE_27 IS NULL
                  THEN
                     type_bpa_zone_obj (l_stage).ORG_PRICE_ZONE_27 :=
                        REC_PRICE_ZONE_ORG.organization_code;
                  ELSE
                     type_bpa_zone_obj (l_stage).ORG_PRICE_ZONE_27 :=
                           type_bpa_zone_obj (l_stage).ORG_PRICE_ZONE_27
                        || ', '
                        || REC_PRICE_ZONE_ORG.organization_code;
                  END IF;
               ELSIF REC_PRICE_ZONE_ORG.price_zone = 28
               THEN
                  IF type_bpa_zone_obj (l_stage).ORG_PRICE_ZONE_28 IS NULL
                  THEN
                     type_bpa_zone_obj (l_stage).ORG_PRICE_ZONE_28 :=
                        REC_PRICE_ZONE_ORG.organization_code;
                  ELSE
                     type_bpa_zone_obj (l_stage).ORG_PRICE_ZONE_28 :=
                           type_bpa_zone_obj (l_stage).ORG_PRICE_ZONE_28
                        || ', '
                        || REC_PRICE_ZONE_ORG.organization_code;
                  END IF;
               ELSIF REC_PRICE_ZONE_ORG.price_zone = 29
               THEN
                  IF type_bpa_zone_obj (l_stage).ORG_PRICE_ZONE_29 IS NULL
                  THEN
                     type_bpa_zone_obj (l_stage).ORG_PRICE_ZONE_29 :=
                        REC_PRICE_ZONE_ORG.organization_code;
                  ELSE
                     type_bpa_zone_obj (l_stage).ORG_PRICE_ZONE_29 :=
                           type_bpa_zone_obj (l_stage).ORG_PRICE_ZONE_29
                        || ', '
                        || REC_PRICE_ZONE_ORG.organization_code;
                  END IF;
               ELSIF REC_PRICE_ZONE_ORG.price_zone = 30
               THEN
                  IF type_bpa_zone_obj (l_stage).org_price_zone_30 IS NULL
                  THEN
                     type_bpa_zone_obj (l_stage).org_price_zone_30 :=
                        REC_PRICE_ZONE_ORG.organization_code;
                  ELSE
                     type_bpa_zone_obj (l_stage).org_price_zone_30 :=
                           type_bpa_zone_obj (l_stage).org_price_zone_30
                        || ', '
                        || REC_PRICE_ZONE_ORG.organization_code;
                  END IF;
               ELSE
                  NULL;
               END IF;
            END LOOP;
         END LOOP;

         FORALL l_stage IN 1 .. type_bpa_zone_obj.COUNT
            INSERT
              INTO XXWC_BPA_PRC_ZONE_WEBADI_TMP_T (PO_NUMBER,
                                                   VENDOR_NAME,
                                                   VENDOR_NUM,
                                                   VENDOR_SITE_CODE,
                                                   ITEM_NUMBER,
                                                   ITEM_DESCRIPTION,
                                                   VENDOR_ITEM_NUMBER,
                                                   EXPIRATION_DATE,
                                                   NATIONAL_PRICE,
                                                   NATIONAL_QUANTITY,
                                                   NATIONAL_BREAK,
                                                   PROMO_PRICE,
                                                   PROMO_START_DATE,
                                                   PROMO_END_DATE,
                                                   PRICE_ZONE_PRICE1,
                                                   PRICE_ZONE_QUANTITY1,
                                                   PRICE_ZONE_QUANTITY_PRICE1,
                                                   PRICE_ZONE_PRICE2,
                                                   PRICE_ZONE_QUANTITY2,
                                                   PRICE_ZONE_QUANTITY_PRICE2,
                                                   PRICE_ZONE_PRICE3,
                                                   PRICE_ZONE_QUANTITY3,
                                                   PRICE_ZONE_QUANTITY_PRICE3,
                                                   PRICE_ZONE_PRICE4,
                                                   PRICE_ZONE_QUANTITY4,
                                                   PRICE_ZONE_QUANTITY_PRICE4,
                                                   PRICE_ZONE_PRICE5,
                                                   PRICE_ZONE_QUANTITY5,
                                                   PRICE_ZONE_QUANTITY_PRICE5,
                                                   PRICE_ZONE_PRICE6,
                                                   PRICE_ZONE_QUANTITY6,
                                                   PRICE_ZONE_QUANTITY_PRICE6,
                                                   PRICE_ZONE_PRICE7,
                                                   PRICE_ZONE_QUANTITY7,
                                                   PRICE_ZONE_QUANTITY_PRICE7,
                                                   PRICE_ZONE_PRICE8,
                                                   PRICE_ZONE_QUANTITY8,
                                                   PRICE_ZONE_QUANTITY_PRICE8,
                                                   PRICE_ZONE_PRICE9,
                                                   PRICE_ZONE_QUANTITY9,
                                                   PRICE_ZONE_QUANTITY_PRICE9,
                                                   PRICE_ZONE_PRICE10,
                                                   PRICE_ZONE_QUANTITY10,
                                                   PRICE_ZONE_QUANTITY_PRICE10,
                                                   PRICE_ZONE_PRICE11,
                                                   PRICE_ZONE_QUANTITY11,
                                                   PRICE_ZONE_QUANTITY_PRICE11,
                                                   PRICE_ZONE_PRICE12,
                                                   PRICE_ZONE_QUANTITY12,
                                                   PRICE_ZONE_QUANTITY_PRICE12,
                                                   PRICE_ZONE_PRICE13,
                                                   PRICE_ZONE_QUANTITY13,
                                                   PRICE_ZONE_QUANTITY_PRICE13,
                                                   PRICE_ZONE_PRICE14,
                                                   PRICE_ZONE_QUANTITY14,
                                                   PRICE_ZONE_QUANTITY_PRICE14,
                                                   PRICE_ZONE_PRICE15,
                                                   PRICE_ZONE_QUANTITY15,
                                                   PRICE_ZONE_QUANTITY_PRICE15,
                                                   PRICE_ZONE_PRICE16,
                                                   PRICE_ZONE_QUANTITY16,
                                                   PRICE_ZONE_QUANTITY_PRICE16,
                                                   PRICE_ZONE_PRICE17,
                                                   PRICE_ZONE_QUANTITY17,
                                                   PRICE_ZONE_QUANTITY_PRICE17,
                                                   PRICE_ZONE_PRICE18,
                                                   PRICE_ZONE_QUANTITY18,
                                                   PRICE_ZONE_QUANTITY_PRICE18,
                                                   PRICE_ZONE_PRICE19,
                                                   PRICE_ZONE_QUANTITY19,
                                                   PRICE_ZONE_QUANTITY_PRICE19,
                                                   PRICE_ZONE_PRICE20,
                                                   PRICE_ZONE_QUANTITY20,
                                                   PRICE_ZONE_QUANTITY_PRICE20,
                                                   PRICE_ZONE_PRICE21,
                                                   PRICE_ZONE_QUANTITY21,
                                                   PRICE_ZONE_QUANTITY_PRICE21,
                                                   PRICE_ZONE_PRICE22,
                                                   PRICE_ZONE_QUANTITY22,
                                                   PRICE_ZONE_QUANTITY_PRICE22,
                                                   PRICE_ZONE_PRICE23,
                                                   PRICE_ZONE_QUANTITY23,
                                                   PRICE_ZONE_QUANTITY_PRICE23,
                                                   PRICE_ZONE_PRICE24,
                                                   PRICE_ZONE_QUANTITY24,
                                                   PRICE_ZONE_QUANTITY_PRICE24,
                                                   PRICE_ZONE_PRICE25,
                                                   PRICE_ZONE_QUANTITY25,
                                                   PRICE_ZONE_QUANTITY_PRICE25,
                                                   PRICE_ZONE_PRICE26,
                                                   PRICE_ZONE_QUANTITY26,
                                                   PRICE_ZONE_QUANTITY_PRICE26,
                                                   PRICE_ZONE_PRICE27,
                                                   PRICE_ZONE_QUANTITY27,
                                                   PRICE_ZONE_QUANTITY_PRICE27,
                                                   PRICE_ZONE_PRICE28,
                                                   PRICE_ZONE_QUANTITY28,
                                                   PRICE_ZONE_QUANTITY_PRICE28,
                                                   PRICE_ZONE_PRICE29,
                                                   PRICE_ZONE_QUANTITY29,
                                                   PRICE_ZONE_QUANTITY_PRICE29,
                                                   PRICE_ZONE_PRICE30,
                                                   PRICE_ZONE_QUANTITY30,
                                                   PRICE_ZONE_QUANTITY_PRICE30,
                                                   ORG_PRICE_ZONE_1,
                                                   ORG_PRICE_ZONE_2,
                                                   ORG_PRICE_ZONE_3,
                                                   ORG_PRICE_ZONE_4,
                                                   ORG_PRICE_ZONE_5,
                                                   ORG_PRICE_ZONE_6,
                                                   ORG_PRICE_ZONE_7,
                                                   ORG_PRICE_ZONE_8,
                                                   ORG_PRICE_ZONE_9,
                                                   ORG_PRICE_ZONE_10,
                                                   ORG_PRICE_ZONE_11,
                                                   ORG_PRICE_ZONE_12,
                                                   ORG_PRICE_ZONE_13,
                                                   ORG_PRICE_ZONE_14,
                                                   ORG_PRICE_ZONE_15,
                                                   ORG_PRICE_ZONE_16,
                                                   ORG_PRICE_ZONE_17,
                                                   ORG_PRICE_ZONE_18,
                                                   ORG_PRICE_ZONE_19,
                                                   ORG_PRICE_ZONE_20,
                                                   ORG_PRICE_ZONE_21,
                                                   ORG_PRICE_ZONE_22,
                                                   ORG_PRICE_ZONE_23,
                                                   ORG_PRICE_ZONE_24,
                                                   ORG_PRICE_ZONE_25,
                                                   ORG_PRICE_ZONE_26,
                                                   ORG_PRICE_ZONE_27,
                                                   ORG_PRICE_ZONE_28,
                                                   ORG_PRICE_ZONE_29,
                                                   ORG_PRICE_ZONE_30,
                                                   PO_HEADER_ID,
                                                   INVENTORY_ITEM_ID,
                                                   VENDOR_ID,
                                                   VENDOR_SITE_ID,
                                                   VENDOR_CONTACT_ID,
                                                   IMPLEMENT_DATE,
                                                   PRICE_ZONE_QUANTITY,
                                                   ORG_ID)
            VALUES (type_bpa_zone_obj (l_stage).PO_NUMBER,
                    type_bpa_zone_obj (l_stage).VENDOR_NAME,
                    type_bpa_zone_obj (l_stage).VENDOR_NUM,
                    type_bpa_zone_obj (l_stage).VENDOR_SITE_CODE,
                    type_bpa_zone_obj (l_stage).ITEM_NUMBER,
                    type_bpa_zone_obj (l_stage).ITEM_DESCRIPTION,
                    type_bpa_zone_obj (l_stage).VENDOR_ITEM_NUMBER,
                    type_bpa_zone_obj (l_stage).EXPIRATION_DATE,
                    type_bpa_zone_obj (l_stage).NATIONAL_PRICE,
                    type_bpa_zone_obj (l_stage).NATIONAL_QUANTITY,
                    type_bpa_zone_obj (l_stage).NATIONAL_BREAK,
                    type_bpa_zone_obj (l_stage).PROMO_PRICE,
                    type_bpa_zone_obj (l_stage).PROMO_START_DATE,
                    type_bpa_zone_obj (l_stage).PROMO_END_DATE,
                    type_bpa_zone_obj (l_stage).PRICE_ZONE_PRICE1,
                    type_bpa_zone_obj (l_stage).PRICE_ZONE_QUANTITY1,
                    type_bpa_zone_obj (l_stage).PRICE_ZONE_QUANTITY_PRICE1,
                    type_bpa_zone_obj (l_stage).PRICE_ZONE_PRICE2,
                    type_bpa_zone_obj (l_stage).PRICE_ZONE_QUANTITY2,
                    type_bpa_zone_obj (l_stage).PRICE_ZONE_QUANTITY_PRICE2,
                    type_bpa_zone_obj (l_stage).PRICE_ZONE_PRICE3,
                    type_bpa_zone_obj (l_stage).PRICE_ZONE_QUANTITY3,
                    type_bpa_zone_obj (l_stage).PRICE_ZONE_QUANTITY_PRICE3,
                    type_bpa_zone_obj (l_stage).PRICE_ZONE_PRICE4,
                    type_bpa_zone_obj (l_stage).PRICE_ZONE_QUANTITY4,
                    type_bpa_zone_obj (l_stage).PRICE_ZONE_QUANTITY_PRICE4,
                    type_bpa_zone_obj (l_stage).PRICE_ZONE_PRICE5,
                    type_bpa_zone_obj (l_stage).PRICE_ZONE_QUANTITY5,
                    type_bpa_zone_obj (l_stage).PRICE_ZONE_QUANTITY_PRICE5,
                    type_bpa_zone_obj (l_stage).PRICE_ZONE_PRICE6,
                    type_bpa_zone_obj (l_stage).PRICE_ZONE_QUANTITY6,
                    type_bpa_zone_obj (l_stage).PRICE_ZONE_QUANTITY_PRICE6,
                    type_bpa_zone_obj (l_stage).PRICE_ZONE_PRICE7,
                    type_bpa_zone_obj (l_stage).PRICE_ZONE_QUANTITY7,
                    type_bpa_zone_obj (l_stage).PRICE_ZONE_QUANTITY_PRICE7,
                    type_bpa_zone_obj (l_stage).PRICE_ZONE_PRICE8,
                    type_bpa_zone_obj (l_stage).PRICE_ZONE_QUANTITY8,
                    type_bpa_zone_obj (l_stage).PRICE_ZONE_QUANTITY_PRICE8,
                    type_bpa_zone_obj (l_stage).PRICE_ZONE_PRICE9,
                    type_bpa_zone_obj (l_stage).PRICE_ZONE_QUANTITY9,
                    type_bpa_zone_obj (l_stage).PRICE_ZONE_QUANTITY_PRICE9,
                    type_bpa_zone_obj (l_stage).PRICE_ZONE_PRICE10,
                    type_bpa_zone_obj (l_stage).PRICE_ZONE_QUANTITY10,
                    type_bpa_zone_obj (l_stage).PRICE_ZONE_QUANTITY_PRICE10,
                    type_bpa_zone_obj (l_stage).PRICE_ZONE_PRICE11,
                    type_bpa_zone_obj (l_stage).PRICE_ZONE_QUANTITY11,
                    type_bpa_zone_obj (l_stage).PRICE_ZONE_QUANTITY_PRICE11,
                    type_bpa_zone_obj (l_stage).PRICE_ZONE_PRICE12,
                    type_bpa_zone_obj (l_stage).PRICE_ZONE_QUANTITY12,
                    type_bpa_zone_obj (l_stage).PRICE_ZONE_QUANTITY_PRICE12,
                    type_bpa_zone_obj (l_stage).PRICE_ZONE_PRICE13,
                    type_bpa_zone_obj (l_stage).PRICE_ZONE_QUANTITY13,
                    type_bpa_zone_obj (l_stage).PRICE_ZONE_QUANTITY_PRICE13,
                    type_bpa_zone_obj (l_stage).PRICE_ZONE_PRICE14,
                    type_bpa_zone_obj (l_stage).PRICE_ZONE_QUANTITY14,
                    type_bpa_zone_obj (l_stage).PRICE_ZONE_QUANTITY_PRICE14,
                    type_bpa_zone_obj (l_stage).PRICE_ZONE_PRICE15,
                    type_bpa_zone_obj (l_stage).PRICE_ZONE_QUANTITY15,
                    type_bpa_zone_obj (l_stage).PRICE_ZONE_QUANTITY_PRICE15,
                    type_bpa_zone_obj (l_stage).PRICE_ZONE_PRICE16,
                    type_bpa_zone_obj (l_stage).PRICE_ZONE_QUANTITY16,
                    type_bpa_zone_obj (l_stage).PRICE_ZONE_QUANTITY_PRICE16,
                    type_bpa_zone_obj (l_stage).PRICE_ZONE_PRICE17,
                    type_bpa_zone_obj (l_stage).PRICE_ZONE_QUANTITY17,
                    type_bpa_zone_obj (l_stage).PRICE_ZONE_QUANTITY_PRICE17,
                    type_bpa_zone_obj (l_stage).PRICE_ZONE_PRICE18,
                    type_bpa_zone_obj (l_stage).PRICE_ZONE_QUANTITY18,
                    type_bpa_zone_obj (l_stage).PRICE_ZONE_QUANTITY_PRICE18,
                    type_bpa_zone_obj (l_stage).PRICE_ZONE_PRICE19,
                    type_bpa_zone_obj (l_stage).PRICE_ZONE_QUANTITY19,
                    type_bpa_zone_obj (l_stage).PRICE_ZONE_QUANTITY_PRICE19,
                    type_bpa_zone_obj (l_stage).PRICE_ZONE_PRICE20,
                    type_bpa_zone_obj (l_stage).PRICE_ZONE_QUANTITY20,
                    type_bpa_zone_obj (l_stage).PRICE_ZONE_QUANTITY_PRICE20,
                    type_bpa_zone_obj (l_stage).PRICE_ZONE_PRICE21,
                    type_bpa_zone_obj (l_stage).PRICE_ZONE_QUANTITY21,
                    type_bpa_zone_obj (l_stage).PRICE_ZONE_QUANTITY_PRICE21,
                    type_bpa_zone_obj (l_stage).PRICE_ZONE_PRICE22,
                    type_bpa_zone_obj (l_stage).PRICE_ZONE_QUANTITY22,
                    type_bpa_zone_obj (l_stage).PRICE_ZONE_QUANTITY_PRICE22,
                    type_bpa_zone_obj (l_stage).PRICE_ZONE_PRICE23,
                    type_bpa_zone_obj (l_stage).PRICE_ZONE_QUANTITY23,
                    type_bpa_zone_obj (l_stage).PRICE_ZONE_QUANTITY_PRICE23,
                    type_bpa_zone_obj (l_stage).PRICE_ZONE_PRICE24,
                    type_bpa_zone_obj (l_stage).PRICE_ZONE_QUANTITY24,
                    type_bpa_zone_obj (l_stage).PRICE_ZONE_QUANTITY_PRICE24,
                    type_bpa_zone_obj (l_stage).PRICE_ZONE_PRICE25,
                    type_bpa_zone_obj (l_stage).PRICE_ZONE_QUANTITY25,
                    type_bpa_zone_obj (l_stage).PRICE_ZONE_QUANTITY_PRICE25,
                    type_bpa_zone_obj (l_stage).PRICE_ZONE_PRICE26,
                    type_bpa_zone_obj (l_stage).PRICE_ZONE_QUANTITY26,
                    type_bpa_zone_obj (l_stage).PRICE_ZONE_QUANTITY_PRICE26,
                    type_bpa_zone_obj (l_stage).PRICE_ZONE_PRICE27,
                    type_bpa_zone_obj (l_stage).PRICE_ZONE_QUANTITY27,
                    type_bpa_zone_obj (l_stage).PRICE_ZONE_QUANTITY_PRICE27,
                    type_bpa_zone_obj (l_stage).PRICE_ZONE_PRICE28,
                    type_bpa_zone_obj (l_stage).PRICE_ZONE_QUANTITY28,
                    type_bpa_zone_obj (l_stage).PRICE_ZONE_QUANTITY_PRICE28,
                    type_bpa_zone_obj (l_stage).PRICE_ZONE_PRICE29,
                    type_bpa_zone_obj (l_stage).PRICE_ZONE_QUANTITY29,
                    type_bpa_zone_obj (l_stage).PRICE_ZONE_QUANTITY_PRICE29,
                    type_bpa_zone_obj (l_stage).PRICE_ZONE_PRICE30,
                    type_bpa_zone_obj (l_stage).PRICE_ZONE_QUANTITY30,
                    type_bpa_zone_obj (l_stage).PRICE_ZONE_QUANTITY_PRICE30,
                    type_bpa_zone_obj (l_stage).ORG_PRICE_ZONE_1,
                    type_bpa_zone_obj (l_stage).ORG_PRICE_ZONE_2,
                    type_bpa_zone_obj (l_stage).ORG_PRICE_ZONE_3,
                    type_bpa_zone_obj (l_stage).ORG_PRICE_ZONE_4,
                    type_bpa_zone_obj (l_stage).ORG_PRICE_ZONE_5,
                    type_bpa_zone_obj (l_stage).ORG_PRICE_ZONE_6,
                    type_bpa_zone_obj (l_stage).ORG_PRICE_ZONE_7,
                    type_bpa_zone_obj (l_stage).ORG_PRICE_ZONE_8,
                    type_bpa_zone_obj (l_stage).ORG_PRICE_ZONE_9,
                    type_bpa_zone_obj (l_stage).ORG_PRICE_ZONE_10,
                    type_bpa_zone_obj (l_stage).ORG_PRICE_ZONE_11,
                    type_bpa_zone_obj (l_stage).ORG_PRICE_ZONE_12,
                    type_bpa_zone_obj (l_stage).ORG_PRICE_ZONE_13,
                    type_bpa_zone_obj (l_stage).ORG_PRICE_ZONE_14,
                    type_bpa_zone_obj (l_stage).ORG_PRICE_ZONE_15,
                    type_bpa_zone_obj (l_stage).ORG_PRICE_ZONE_16,
                    type_bpa_zone_obj (l_stage).ORG_PRICE_ZONE_17,
                    type_bpa_zone_obj (l_stage).ORG_PRICE_ZONE_18,
                    type_bpa_zone_obj (l_stage).ORG_PRICE_ZONE_19,
                    type_bpa_zone_obj (l_stage).ORG_PRICE_ZONE_20,
                    type_bpa_zone_obj (l_stage).ORG_PRICE_ZONE_21,
                    type_bpa_zone_obj (l_stage).ORG_PRICE_ZONE_22,
                    type_bpa_zone_obj (l_stage).ORG_PRICE_ZONE_23,
                    type_bpa_zone_obj (l_stage).ORG_PRICE_ZONE_24,
                    type_bpa_zone_obj (l_stage).ORG_PRICE_ZONE_25,
                    type_bpa_zone_obj (l_stage).ORG_PRICE_ZONE_26,
                    type_bpa_zone_obj (l_stage).ORG_PRICE_ZONE_27,
                    type_bpa_zone_obj (l_stage).ORG_PRICE_ZONE_28,
                    type_bpa_zone_obj (l_stage).ORG_PRICE_ZONE_29,
                    type_bpa_zone_obj (l_stage).ORG_PRICE_ZONE_30,
                    type_bpa_zone_obj (l_stage).PO_HEADER_ID,
                    type_bpa_zone_obj (l_stage).INVENTORY_ITEM_ID,
                    type_bpa_zone_obj (l_stage).VENDOR_ID,
                    type_bpa_zone_obj (l_stage).VENDOR_SITE_ID,
                    type_bpa_zone_obj (l_stage).VENDOR_CONTACT_ID,
                    type_bpa_zone_obj (l_stage).IMPLEMENT_DATE,
                    type_bpa_zone_obj (l_stage).PRICE_ZONE_QUANTITY,
                    type_bpa_zone_obj (l_stage).ORG_Id);

         COMMIT;

         type_bpa_zone_obj.delete;

         EXIT WHEN cur_bpa_zone%NOTFOUND;
      END LOOP;

      INSERT INTO XXWC.XXWC_BPA_PRICE_WEBADI_PO_TBL
           VALUES (p_po_header_id, fnd_profile.VALUE ('USER_ID'), SYSDATE);

      COMMIT;

      x_err_message := NULL;
   EXCEPTION
      WHEN OTHERS
      THEN
         x_err_message := SUBSTR (SQLERRM, 1, 500);
   END;

   PROCEDURE XXWC_BPA_PZ_STAGING_TBL_LOAD (
      P_CREATION_DATE                 IN DATE,
      P_CREATED_BY                    IN NUMBER,
      P_LAST_UPDATE_DATE              IN DATE,
      P_LAST_UPDATED_BY               IN NUMBER,
      P_VENDOR_NAME                   IN VARCHAR2,
      P_VENDOR_NUM                    IN VARCHAR2,
      P_VENDOR_SITE_CODE              IN VARCHAR2,
      P_PO_NUMBER                     IN VARCHAR2,
      P_ITEM_NUMBER                   IN VARCHAR2,
      P_ITEM_DESCRIPTION              IN VARCHAR2,
      P_VENDOR_ITEM_NUMBER            IN VARCHAR2,
      P_EXPIRATION_DATE               IN DATE,
      P_IMPLEMENT_DATE                IN DATE,
      P_BPA_TABLE_FLAG                IN NUMBER,
      P_NATIONAL_PRICE                IN NUMBER,
      P_NATIONAL_QUANTITY             IN NUMBER,
      P_NATIONAL_BREAK                IN NUMBER,
      P_PROMO_PRICE                   IN NUMBER,
      P_PROMO_START_DATE              IN DATE,
      P_PROMO_END_DATE                IN DATE,
      P_PROMO_TABLE_FLAG              IN NUMBER,
      P_PRICE_ZONE_PRICE1             IN NUMBER,
      P_PRICE_ZONE_QUANTITY1          IN NUMBER,
      P_PRICE_ZONE_QUANTITY_PRICE1    IN NUMBER,
      P_PRICE_ZONE_PRICE2             IN NUMBER,
      P_PRICE_ZONE_QUANTITY2          IN NUMBER,
      P_PRICE_ZONE_QUANTITY_PRICE2    IN NUMBER,
      P_PRICE_ZONE_PRICE3             IN NUMBER,
      P_PRICE_ZONE_QUANTITY3          IN NUMBER,
      P_PRICE_ZONE_QUANTITY_PRICE3    IN NUMBER,
      P_PRICE_ZONE_PRICE4             IN NUMBER,
      P_PRICE_ZONE_QUANTITY4          IN NUMBER,
      P_PRICE_ZONE_QUANTITY_PRICE4    IN NUMBER,
      P_PRICE_ZONE_PRICE5             IN NUMBER,
      P_PRICE_ZONE_QUANTITY5          IN NUMBER,
      P_PRICE_ZONE_QUANTITY_PRICE5    IN NUMBER,
      P_PRICE_ZONE_PRICE6             IN NUMBER,
      P_PRICE_ZONE_QUANTITY6          IN NUMBER,
      P_PRICE_ZONE_QUANTITY_PRICE6    IN NUMBER,
      P_PRICE_ZONE_PRICE7             IN NUMBER,
      P_PRICE_ZONE_QUANTITY7          IN NUMBER,
      P_PRICE_ZONE_QUANTITY_PRICE7    IN NUMBER,
      P_PRICE_ZONE_PRICE8             IN NUMBER,
      P_PRICE_ZONE_QUANTITY8          IN NUMBER,
      P_PRICE_ZONE_QUANTITY_PRICE8    IN NUMBER,
      P_PRICE_ZONE_PRICE9             IN NUMBER,
      P_PRICE_ZONE_QUANTITY9          IN NUMBER,
      P_PRICE_ZONE_QUANTITY_PRICE9    IN NUMBER,
      P_PRICE_ZONE_PRICE10            IN NUMBER,
      P_PRICE_ZONE_QUANTITY10         IN NUMBER,
      P_PRICE_ZONE_QUANTITY_PRICE10   IN NUMBER,
      P_PRICE_ZONE_PRICE11            IN NUMBER,
      P_PRICE_ZONE_QUANTITY11         IN NUMBER,
      P_PRICE_ZONE_QUANTITY_PRICE11   IN NUMBER,
      P_PRICE_ZONE_PRICE12            IN NUMBER,
      P_PRICE_ZONE_QUANTITY12         IN NUMBER,
      P_PRICE_ZONE_QUANTITY_PRICE12   IN NUMBER,
      P_PRICE_ZONE_PRICE13            IN NUMBER,
      P_PRICE_ZONE_QUANTITY13         IN NUMBER,
      P_PRICE_ZONE_QUANTITY_PRICE13   IN NUMBER,
      P_PRICE_ZONE_PRICE14            IN NUMBER,
      P_PRICE_ZONE_QUANTITY14         IN NUMBER,
      P_PRICE_ZONE_QUANTITY_PRICE14   IN NUMBER,
      P_PRICE_ZONE_PRICE15            IN NUMBER,
      P_PRICE_ZONE_QUANTITY15         IN NUMBER,
      P_PRICE_ZONE_QUANTITY_PRICE15   IN NUMBER,
      P_PRICE_ZONE_PRICE16            IN NUMBER,
      P_PRICE_ZONE_QUANTITY16         IN NUMBER,
      P_PRICE_ZONE_QUANTITY_PRICE16   IN NUMBER,
      P_PRICE_ZONE_PRICE17            IN NUMBER,
      P_PRICE_ZONE_QUANTITY17         IN NUMBER,
      P_PRICE_ZONE_QUANTITY_PRICE17   IN NUMBER,
      P_PRICE_ZONE_PRICE18            IN NUMBER,
      P_PRICE_ZONE_QUANTITY18         IN NUMBER,
      P_PRICE_ZONE_QUANTITY_PRICE18   IN NUMBER,
      P_PRICE_ZONE_PRICE19            IN NUMBER,
      P_PRICE_ZONE_QUANTITY19         IN NUMBER,
      P_PRICE_ZONE_QUANTITY_PRICE19   IN NUMBER,
      P_PRICE_ZONE_PRICE20            IN NUMBER,
      P_PRICE_ZONE_QUANTITY20         IN NUMBER,
      P_PRICE_ZONE_QUANTITY_PRICE20   IN NUMBER,
      P_PRICE_ZONE_PRICE21            IN NUMBER,
      P_PRICE_ZONE_QUANTITY21         IN NUMBER,
      P_PRICE_ZONE_QUANTITY_PRICE21   IN NUMBER,
      P_PRICE_ZONE_PRICE22            IN NUMBER,
      P_PRICE_ZONE_QUANTITY22         IN NUMBER,
      P_PRICE_ZONE_QUANTITY_PRICE22   IN NUMBER,
      P_PRICE_ZONE_PRICE23            IN NUMBER,
      P_PRICE_ZONE_QUANTITY23         IN NUMBER,
      P_PRICE_ZONE_QUANTITY_PRICE23   IN NUMBER,
      P_PRICE_ZONE_PRICE24            IN NUMBER,
      P_PRICE_ZONE_QUANTITY24         IN NUMBER,
      P_PRICE_ZONE_QUANTITY_PRICE24   IN NUMBER,
      P_PRICE_ZONE_PRICE25            IN NUMBER,
      P_PRICE_ZONE_QUANTITY25         IN NUMBER,
      P_PRICE_ZONE_QUANTITY_PRICE25   IN NUMBER,
      P_PRICE_ZONE_PRICE26            IN NUMBER,
      P_PRICE_ZONE_QUANTITY26         IN NUMBER,
      P_PRICE_ZONE_QUANTITY_PRICE26   IN NUMBER,
      P_PRICE_ZONE_PRICE27            IN NUMBER,
      P_PRICE_ZONE_QUANTITY27         IN NUMBER,
      P_PRICE_ZONE_QUANTITY_PRICE27   IN NUMBER,
      P_PRICE_ZONE_PRICE28            IN NUMBER,
      P_PRICE_ZONE_QUANTITY28         IN NUMBER,
      P_PRICE_ZONE_QUANTITY_PRICE28   IN NUMBER,
      P_PRICE_ZONE_PRICE29            IN NUMBER,
      P_PRICE_ZONE_QUANTITY29         IN NUMBER,
      P_PRICE_ZONE_QUANTITY_PRICE29   IN NUMBER,
      P_PRICE_ZONE_PRICE30            IN NUMBER,
      P_PRICE_ZONE_QUANTITY30         IN NUMBER,
      P_PRICE_ZONE_QUANTITY_PRICE30   IN NUMBER,
      P_PRICE_ZONE_TABLE_FLAG         IN NUMBER,
      P_ORG_PRICE_ZONE_1              IN VARCHAR2,
      P_ORG_PRICE_ZONE_2              IN VARCHAR2,
      P_ORG_PRICE_ZONE_3              IN VARCHAR2,
      P_ORG_PRICE_ZONE_4              IN VARCHAR2,
      P_ORG_PRICE_ZONE_5              IN VARCHAR2,
      P_ORG_PRICE_ZONE_6              IN VARCHAR2,
      P_ORG_PRICE_ZONE_7              IN VARCHAR2,
      P_ORG_PRICE_ZONE_8              IN VARCHAR2,
      P_ORG_PRICE_ZONE_9              IN VARCHAR2,
      P_ORG_PRICE_ZONE_10             IN VARCHAR2,
      P_ORG_PRICE_ZONE_11             IN VARCHAR2,
      P_ORG_PRICE_ZONE_12             IN VARCHAR2,
      P_ORG_PRICE_ZONE_13             IN VARCHAR2,
      P_ORG_PRICE_ZONE_14             IN VARCHAR2,
      P_ORG_PRICE_ZONE_15             IN VARCHAR2,
      P_ORG_PRICE_ZONE_16             IN VARCHAR2,
      P_ORG_PRICE_ZONE_17             IN VARCHAR2,
      P_ORG_PRICE_ZONE_18             IN VARCHAR2,
      P_ORG_PRICE_ZONE_19             IN VARCHAR2,
      P_ORG_PRICE_ZONE_20             IN VARCHAR2,
      P_ORG_PRICE_ZONE_21             IN VARCHAR2,
      P_ORG_PRICE_ZONE_22             IN VARCHAR2,
      P_ORG_PRICE_ZONE_23             IN VARCHAR2,
      P_ORG_PRICE_ZONE_24             IN VARCHAR2,
      P_ORG_PRICE_ZONE_25             IN VARCHAR2,
      P_ORG_PRICE_ZONE_26             IN VARCHAR2,
      P_ORG_PRICE_ZONE_27             IN VARCHAR2,
      P_ORG_PRICE_ZONE_28             IN VARCHAR2,
      P_ORG_PRICE_ZONE_29             IN VARCHAR2,
      P_ORG_PRICE_ZONE_30             IN VARCHAR2,
      P_APPROVE_FLAG                  IN NUMBER,
      P_ORG_ID                        IN NUMBER)
   IS
      /***************************************************************************************************************************
      -- PROGRAM TYPE: PL/SQL Script   <API>
      --
      -- PURPOSE: Procedure XXWC_BPA_PZ_STAGING_TBL_LOAD To insert date from webadi to BPA Price staging table.
      -- HISTORY
      -- =========================================================================================================================
      -- =========================================================================================================================
      -- VERSION DATE          AUTHOR(S)       DESCRIPTION
      -- ------- -----------   --------------- -----------------------------------------------------------------------------------
      -- 1.0     01-Apr-2015   P.Vamshidhar    Created this Procedure.
                                               TMS#20150414-00173-2015 PLM - Implement Cost Management tool enhancements
      -- 1.2    29-Sep-2017   Nancy Pahwa   TMS#20160921-00270 - BPA Enhancements Mfg Part Number Addition
      ***************************************************************************************************************************/


      l_error_message    VARCHAR2 (500);
      l_error_message1   VARCHAR2 (500);
      ln_item_exist      NUMBER;  --v1.2
      lvc_cross_ref      MTL_CROSS_REFERENCES.CROSS_REFERENCE%TYPE;

   BEGIN

/*
 l_error_message1 :=
         BPA_PRICE_VALIDATE ('0',
                             P_NATIONAL_PRICE,
                             P_NATIONAL_QUANTITY,
                             P_NATIONAL_BREAK);
                             */
--/* --Uncommented for Rev 1.1

      l_error_message1 :=
         BPA_PRICE_VALIDATE ('1',
                             P_PRICE_ZONE_PRICE1,
                             P_PRICE_ZONE_QUANTITY1,
                             P_PRICE_ZONE_QUANTITY_PRICE1);

      l_error_message := l_error_message || ' ' || l_error_message1;

      l_error_message1 :=
         BPA_PRICE_VALIDATE ('2',
                             P_PRICE_ZONE_PRICE2,
                             P_PRICE_ZONE_QUANTITY2,
                             P_PRICE_ZONE_QUANTITY_PRICE2);
      l_error_message := l_error_message || ' ' || l_error_message1;

      l_error_message1 :=
         BPA_PRICE_VALIDATE ('3',
                             P_PRICE_ZONE_PRICE3,
                             P_PRICE_ZONE_QUANTITY3,
                             P_PRICE_ZONE_QUANTITY_PRICE3);
      l_error_message := l_error_message || ' ' || l_error_message1;
      l_error_message1 :=
         BPA_PRICE_VALIDATE ('4',
                             P_PRICE_ZONE_PRICE4,
                             P_PRICE_ZONE_QUANTITY4,
                             P_PRICE_ZONE_QUANTITY_PRICE4);
      l_error_message := l_error_message || ' ' || l_error_message1;
      l_error_message1 :=
         BPA_PRICE_VALIDATE ('5',
                             P_PRICE_ZONE_PRICE5,
                             P_PRICE_ZONE_QUANTITY5,
                             P_PRICE_ZONE_QUANTITY_PRICE5);
      l_error_message := l_error_message || ' ' || l_error_message1;
      l_error_message1 :=
         BPA_PRICE_VALIDATE ('6',
                             P_PRICE_ZONE_PRICE6,
                             P_PRICE_ZONE_QUANTITY6,
                             P_PRICE_ZONE_QUANTITY_PRICE6);
      l_error_message := l_error_message || ' ' || l_error_message1;
      l_error_message1 :=
         BPA_PRICE_VALIDATE ('7',
                             P_PRICE_ZONE_PRICE7,
                             P_PRICE_ZONE_QUANTITY7,
                             P_PRICE_ZONE_QUANTITY_PRICE7);
      l_error_message := l_error_message || ' ' || l_error_message1;
      l_error_message1 :=
         BPA_PRICE_VALIDATE ('8',
                             P_PRICE_ZONE_PRICE8,
                             P_PRICE_ZONE_QUANTITY8,
                             P_PRICE_ZONE_QUANTITY_PRICE8);
      l_error_message := l_error_message || ' ' || l_error_message1;
      l_error_message1 :=
         BPA_PRICE_VALIDATE ('9',
                             P_PRICE_ZONE_PRICE9,
                             P_PRICE_ZONE_QUANTITY9,
                             P_PRICE_ZONE_QUANTITY_PRICE9);
      l_error_message := l_error_message || ' ' || l_error_message1;
      l_error_message1 :=
         BPA_PRICE_VALIDATE ('10',
                             P_PRICE_ZONE_PRICE10,
                             P_PRICE_ZONE_QUANTITY10,
                             P_PRICE_ZONE_QUANTITY_PRICE10);
      l_error_message := l_error_message || ' ' || l_error_message1;
      l_error_message1 :=
         BPA_PRICE_VALIDATE ('11',
                             P_PRICE_ZONE_PRICE11,
                             P_PRICE_ZONE_QUANTITY11,
                             P_PRICE_ZONE_QUANTITY_PRICE11);
      l_error_message := l_error_message || ' ' || l_error_message1;
      l_error_message1 :=
         BPA_PRICE_VALIDATE ('12',
                             P_PRICE_ZONE_PRICE12,
                             P_PRICE_ZONE_QUANTITY12,
                             P_PRICE_ZONE_QUANTITY_PRICE12);
      l_error_message := l_error_message || ' ' || l_error_message1;
      l_error_message1 :=
         BPA_PRICE_VALIDATE ('13',
                             P_PRICE_ZONE_PRICE13,
                             P_PRICE_ZONE_QUANTITY13,
                             P_PRICE_ZONE_QUANTITY_PRICE13);
      l_error_message := l_error_message || ' ' || l_error_message1;
      l_error_message1 :=
         BPA_PRICE_VALIDATE ('14',
                             P_PRICE_ZONE_PRICE14,
                             P_PRICE_ZONE_QUANTITY14,
                             P_PRICE_ZONE_QUANTITY_PRICE14);
      l_error_message := l_error_message || ' ' || l_error_message1;
      l_error_message1 :=
         BPA_PRICE_VALIDATE ('15',
                             P_PRICE_ZONE_PRICE15,
                             P_PRICE_ZONE_QUANTITY15,
                             P_PRICE_ZONE_QUANTITY_PRICE15);
      l_error_message := l_error_message || ' ' || l_error_message1;
      l_error_message1 :=
         BPA_PRICE_VALIDATE ('16',
                             P_PRICE_ZONE_PRICE16,
                             P_PRICE_ZONE_QUANTITY16,
                             P_PRICE_ZONE_QUANTITY_PRICE16);
      l_error_message := l_error_message || ' ' || l_error_message1;
      l_error_message1 :=
         BPA_PRICE_VALIDATE ('17',
                             P_PRICE_ZONE_PRICE17,
                             P_PRICE_ZONE_QUANTITY17,
                             P_PRICE_ZONE_QUANTITY_PRICE17);
      l_error_message := l_error_message || ' ' || l_error_message1;
      l_error_message1 :=
         BPA_PRICE_VALIDATE ('18',
                             P_PRICE_ZONE_PRICE18,
                             P_PRICE_ZONE_QUANTITY18,
                             P_PRICE_ZONE_QUANTITY_PRICE18);
      l_error_message := l_error_message || ' ' || l_error_message1;
      l_error_message1 :=
         BPA_PRICE_VALIDATE ('19',
                             P_PRICE_ZONE_PRICE19,
                             P_PRICE_ZONE_QUANTITY19,
                             P_PRICE_ZONE_QUANTITY_PRICE19);
      l_error_message := l_error_message || ' ' || l_error_message1;
      l_error_message1 :=
         BPA_PRICE_VALIDATE ('20',
                             P_PRICE_ZONE_PRICE20,
                             P_PRICE_ZONE_QUANTITY20,
                             P_PRICE_ZONE_QUANTITY_PRICE20);
      l_error_message := l_error_message || ' ' || l_error_message1;
      l_error_message1 :=
         BPA_PRICE_VALIDATE ('21',
                             P_PRICE_ZONE_PRICE21,
                             P_PRICE_ZONE_QUANTITY21,
                             P_PRICE_ZONE_QUANTITY_PRICE21);
      l_error_message := l_error_message || ' ' || l_error_message1;
      l_error_message1 :=
         BPA_PRICE_VALIDATE ('22',
                             P_PRICE_ZONE_PRICE22,
                             P_PRICE_ZONE_QUANTITY22,
                             P_PRICE_ZONE_QUANTITY_PRICE22);
      l_error_message := l_error_message || ' ' || l_error_message1;
      l_error_message1 :=
         BPA_PRICE_VALIDATE ('23',
                             P_PRICE_ZONE_PRICE23,
                             P_PRICE_ZONE_QUANTITY23,
                             P_PRICE_ZONE_QUANTITY_PRICE23);
      l_error_message := l_error_message || ' ' || l_error_message1;
      l_error_message1 :=
         BPA_PRICE_VALIDATE ('24',
                             P_PRICE_ZONE_PRICE24,
                             P_PRICE_ZONE_QUANTITY24,
                             P_PRICE_ZONE_QUANTITY_PRICE24);
      l_error_message := l_error_message || ' ' || l_error_message1;
      l_error_message1 :=
         BPA_PRICE_VALIDATE ('25',
                             P_PRICE_ZONE_PRICE25,
                             P_PRICE_ZONE_QUANTITY25,
                             P_PRICE_ZONE_QUANTITY_PRICE25);
      l_error_message := l_error_message || ' ' || l_error_message1;
      l_error_message1 :=
         BPA_PRICE_VALIDATE ('26',
                             P_PRICE_ZONE_PRICE26,
                             P_PRICE_ZONE_QUANTITY26,
                             P_PRICE_ZONE_QUANTITY_PRICE26);
      l_error_message := l_error_message || ' ' || l_error_message1;
      l_error_message1 :=
         BPA_PRICE_VALIDATE ('27',
                             P_PRICE_ZONE_PRICE27,
                             P_PRICE_ZONE_QUANTITY27,
                             P_PRICE_ZONE_QUANTITY_PRICE27);
      l_error_message := l_error_message || ' ' || l_error_message1;
      l_error_message1 :=
         BPA_PRICE_VALIDATE ('28',
                             P_PRICE_ZONE_PRICE28,
                             P_PRICE_ZONE_QUANTITY28,
                             P_PRICE_ZONE_QUANTITY_PRICE28);
      l_error_message := l_error_message || ' ' || l_error_message1;
      l_error_message1 :=
         BPA_PRICE_VALIDATE ('29',
                             P_PRICE_ZONE_PRICE29,
                             P_PRICE_ZONE_QUANTITY29,
                             P_PRICE_ZONE_QUANTITY_PRICE29);
      l_error_message := l_error_message || ' ' || l_error_message1;
      l_error_message1 :=
         BPA_PRICE_VALIDATE ('30',
                             P_PRICE_ZONE_PRICE30,
                             P_PRICE_ZONE_QUANTITY30,
                             P_PRICE_ZONE_QUANTITY_PRICE30);

--*/   --Uncommented for Rev 1.1

--Added for Ver 1.1 Begin
 l_error_message1 :=
         BPA_PRICE_VALIDATE ('0',
                             P_NATIONAL_PRICE,
                             P_NATIONAL_QUANTITY,
                             P_NATIONAL_BREAK);
--Added for Ver 1.1 End;

      l_error_message := l_error_message || ' ' || l_error_message1;

      IF REPLACE (l_error_message, ' ', NULL) IS NOT NULL
      THEN
         RAISE PROGRAM_ERROR;
      END IF;
 -- v1.2 Start

      BEGIN
         ln_item_exist := 0;

         SELECT COUNT (1)
           INTO ln_item_exist
           FROM apps.po_headers_all pha,
                apps.po_lines_all pla,
                apps.mtl_system_items_b msi
          WHERE     pha.po_header_id = pla.po_header_id
                AND pla.item_id = msi.inventory_item_id
                AND msi.organization_id = 222
                AND msi.segment1 = P_ITEM_NUMBER
                AND pha.segment1 = P_PO_NUMBER;
      EXCEPTION
         WHEN OTHERS
         THEN
            ln_item_exist := 0;
      END;

      IF ln_item_exist = 0
      THEN
         BEGIN
           /* SELECT mcr.CROSS_REFERENCE
              INTO lvc_cross_ref
              FROM apps.mtl_cross_references mcr, apps.mtl_system_items_b msi
             WHERE    1=1
                   AND msi.inventory_item_id = mcr.inventory_item_id
                   AND msi.organization_id = 222
                   AND mcr.attribute_category = '162'
                   AND mcr.cross_reference_type = 'VENDOR'
                   AND mcr.attribute1 = P_VENDOR_NUM
                   AND msi.segment1 = P_ITEM_NUMBER
                   AND ROWNUM = 1;*/
            SELECT mcr.CROSS_REFERENCE
              INTO lvc_cross_ref
              FROM apps.mtl_cross_references mcr, apps.ap_suppliers aps, apps.mtl_system_items_b msi
             WHERE    1=1
                   AND msi.inventory_item_id = mcr.inventory_item_id
                   AND msi.organization_id = 222
                 --  AND mcr.attribute_category = '162'
                   AND mcr.cross_reference_type = 'VENDOR'
                   AND mcr.attribute1 = aps.segment1
                  -- AND mcr.attribute1 = i_vendor_num
                   AND msi.segment1 = p_item_number
                   AND aps.vendor_name = P_VENDOR_NAME
                   AND mcr.attribute1 is not null
                   AND ROWNUM = 1;
         EXCEPTION
            WHEN OTHERS
            THEN
              null;
         END;
      END IF;
-- v 1.2 end

      INSERT INTO XXWC.XXWC_BPA_PZ_STAGING_TBL (CREATION_DATE,
                                                CREATED_BY,
                                                LAST_UPDATE_DATE,
                                                LAST_UPDATED_BY,
                                                VENDOR_NAME,
                                                VENDOR_NUM,
                                                VENDOR_SITE_CODE,
                                                PO_NUMBER,
                                                ITEM_NUMBER,
                                                ITEM_DESCRIPTION,
                                                VENDOR_ITEM_NUMBER,
                                                EXPIRATION_DATE,
                                                IMPLEMENT_DATE,
                                                BPA_TABLE_FLAG,
                                                NATIONAL_PRICE,
                                                NATIONAL_QUANTITY,
                                                NATIONAL_BREAK,
                                                PROMO_PRICE,
                                                PROMO_START_DATE,
                                                PROMO_END_DATE,
                                                PROMO_TABLE_FLAG,
                                                PRICE_ZONE_PRICE1,
                                                PRICE_ZONE_QUANTITY1,
                                                PRICE_ZONE_QUANTITY_PRICE1,
                                                PRICE_ZONE_PRICE2,
                                                PRICE_ZONE_QUANTITY2,
                                                PRICE_ZONE_QUANTITY_PRICE2,
                                                PRICE_ZONE_PRICE3,
                                                PRICE_ZONE_QUANTITY3,
                                                PRICE_ZONE_QUANTITY_PRICE3,
                                                PRICE_ZONE_PRICE4,
                                                PRICE_ZONE_QUANTITY4,
                                                PRICE_ZONE_QUANTITY_PRICE4,
                                                PRICE_ZONE_PRICE5,
                                                PRICE_ZONE_QUANTITY5,
                                                PRICE_ZONE_QUANTITY_PRICE5,
                                                PRICE_ZONE_PRICE6,
                                                PRICE_ZONE_QUANTITY6,
                                                PRICE_ZONE_QUANTITY_PRICE6,
                                                PRICE_ZONE_PRICE7,
                                                PRICE_ZONE_QUANTITY7,
                                                PRICE_ZONE_QUANTITY_PRICE7,
                                                PRICE_ZONE_PRICE8,
                                                PRICE_ZONE_QUANTITY8,
                                                PRICE_ZONE_QUANTITY_PRICE8,
                                                PRICE_ZONE_PRICE9,
                                                PRICE_ZONE_QUANTITY9,
                                                PRICE_ZONE_QUANTITY_PRICE9,
                                                PRICE_ZONE_PRICE10,
                                                PRICE_ZONE_QUANTITY10,
                                                PRICE_ZONE_QUANTITY_PRICE10,
                                                PRICE_ZONE_PRICE11,
                                                PRICE_ZONE_QUANTITY11,
                                                PRICE_ZONE_QUANTITY_PRICE11,
                                                PRICE_ZONE_PRICE12,
                                                PRICE_ZONE_QUANTITY12,
                                                PRICE_ZONE_QUANTITY_PRICE12,
                                                PRICE_ZONE_PRICE13,
                                                PRICE_ZONE_QUANTITY13,
                                                PRICE_ZONE_QUANTITY_PRICE13,
                                                PRICE_ZONE_PRICE14,
                                                PRICE_ZONE_QUANTITY14,
                                                PRICE_ZONE_QUANTITY_PRICE14,
                                                PRICE_ZONE_PRICE15,
                                                PRICE_ZONE_QUANTITY15,
                                                PRICE_ZONE_QUANTITY_PRICE15,
                                                PRICE_ZONE_PRICE16,
                                                PRICE_ZONE_QUANTITY16,
                                                PRICE_ZONE_QUANTITY_PRICE16,
                                                PRICE_ZONE_PRICE17,
                                                PRICE_ZONE_QUANTITY17,
                                                PRICE_ZONE_QUANTITY_PRICE17,
                                                PRICE_ZONE_PRICE18,
                                                PRICE_ZONE_QUANTITY18,
                                                PRICE_ZONE_QUANTITY_PRICE18,
                                                PRICE_ZONE_PRICE19,
                                                PRICE_ZONE_QUANTITY19,
                                                PRICE_ZONE_QUANTITY_PRICE19,
                                                PRICE_ZONE_PRICE20,
                                                PRICE_ZONE_QUANTITY20,
                                                PRICE_ZONE_QUANTITY_PRICE20,
                                                PRICE_ZONE_PRICE21,
                                                PRICE_ZONE_QUANTITY21,
                                                PRICE_ZONE_QUANTITY_PRICE21,
                                                PRICE_ZONE_PRICE22,
                                                PRICE_ZONE_QUANTITY22,
                                                PRICE_ZONE_QUANTITY_PRICE22,
                                                PRICE_ZONE_PRICE23,
                                                PRICE_ZONE_QUANTITY23,
                                                PRICE_ZONE_QUANTITY_PRICE23,
                                                PRICE_ZONE_PRICE24,
                                                PRICE_ZONE_QUANTITY24,
                                                PRICE_ZONE_QUANTITY_PRICE24,
                                                PRICE_ZONE_PRICE25,
                                                PRICE_ZONE_QUANTITY25,
                                                PRICE_ZONE_QUANTITY_PRICE25,
                                                PRICE_ZONE_PRICE26,
                                                PRICE_ZONE_QUANTITY26,
                                                PRICE_ZONE_QUANTITY_PRICE26,
                                                PRICE_ZONE_PRICE27,
                                                PRICE_ZONE_QUANTITY27,
                                                PRICE_ZONE_QUANTITY_PRICE27,
                                                PRICE_ZONE_PRICE28,
                                                PRICE_ZONE_QUANTITY28,
                                                PRICE_ZONE_QUANTITY_PRICE28,
                                                PRICE_ZONE_PRICE29,
                                                PRICE_ZONE_QUANTITY29,
                                                PRICE_ZONE_QUANTITY_PRICE29,
                                                PRICE_ZONE_PRICE30,
                                                PRICE_ZONE_QUANTITY30,
                                                PRICE_ZONE_QUANTITY_PRICE30,
                                                PRICE_ZONE_TABLE_FLAG,
                                                ORG_PRICE_ZONE_1,
                                                ORG_PRICE_ZONE_2,
                                                ORG_PRICE_ZONE_3,
                                                ORG_PRICE_ZONE_4,
                                                ORG_PRICE_ZONE_5,
                                                ORG_PRICE_ZONE_6,
                                                ORG_PRICE_ZONE_7,
                                                ORG_PRICE_ZONE_8,
                                                ORG_PRICE_ZONE_9,
                                                ORG_PRICE_ZONE_10,
                                                ORG_PRICE_ZONE_11,
                                                ORG_PRICE_ZONE_12,
                                                ORG_PRICE_ZONE_13,
                                                ORG_PRICE_ZONE_14,
                                                ORG_PRICE_ZONE_15,
                                                ORG_PRICE_ZONE_16,
                                                ORG_PRICE_ZONE_17,
                                                ORG_PRICE_ZONE_18,
                                                ORG_PRICE_ZONE_19,
                                                ORG_PRICE_ZONE_20,
                                                ORG_PRICE_ZONE_21,
                                                ORG_PRICE_ZONE_22,
                                                ORG_PRICE_ZONE_23,
                                                ORG_PRICE_ZONE_24,
                                                ORG_PRICE_ZONE_25,
                                                ORG_PRICE_ZONE_26,
                                                ORG_PRICE_ZONE_27,
                                                ORG_PRICE_ZONE_28,
                                                ORG_PRICE_ZONE_29,
                                                ORG_PRICE_ZONE_30,
                                                APPROVE_FLAG,
                                                ORG_ID)
           VALUES (P_CREATION_DATE,
                   P_CREATED_BY,
                   P_LAST_UPDATE_DATE,
                   P_LAST_UPDATED_BY,
                   P_VENDOR_NAME,
                   P_VENDOR_NUM,
                   P_VENDOR_SITE_CODE,
                   P_PO_NUMBER,
                   P_ITEM_NUMBER,
                    P_ITEM_DESCRIPTION,
                   --P_VENDOR_ITEM_NUMBER, commented v 1.2
                   nvl (lvc_cross_ref,p_vendor_item_number), --v 1.2.
                   P_EXPIRATION_DATE,
                   P_IMPLEMENT_DATE,
                   '1',                                    --P_BPA_TABLE_FLAG,
                   P_NATIONAL_PRICE,
                   P_NATIONAL_QUANTITY,
                   P_NATIONAL_BREAK,
                   P_PROMO_PRICE,
                   P_PROMO_START_DATE,
                   P_PROMO_END_DATE,
                   '1',                                  --P_PROMO_TABLE_FLAG,
                   P_PRICE_ZONE_PRICE1,
                   P_PRICE_ZONE_QUANTITY1,
                   P_PRICE_ZONE_QUANTITY_PRICE1,
                   P_PRICE_ZONE_PRICE2,
                   P_PRICE_ZONE_QUANTITY2,
                   P_PRICE_ZONE_QUANTITY_PRICE2,
                   P_PRICE_ZONE_PRICE3,
                   P_PRICE_ZONE_QUANTITY3,
                   P_PRICE_ZONE_QUANTITY_PRICE3,
                   P_PRICE_ZONE_PRICE4,
                   P_PRICE_ZONE_QUANTITY4,
                   P_PRICE_ZONE_QUANTITY_PRICE4,
                   P_PRICE_ZONE_PRICE5,
                   P_PRICE_ZONE_QUANTITY5,
                   P_PRICE_ZONE_QUANTITY_PRICE5,
                   P_PRICE_ZONE_PRICE6,
                   P_PRICE_ZONE_QUANTITY6,
                   P_PRICE_ZONE_QUANTITY_PRICE6,
                   P_PRICE_ZONE_PRICE7,
                   P_PRICE_ZONE_QUANTITY7,
                   P_PRICE_ZONE_QUANTITY_PRICE7,
                   P_PRICE_ZONE_PRICE8,
                   P_PRICE_ZONE_QUANTITY8,
                   P_PRICE_ZONE_QUANTITY_PRICE8,
                   P_PRICE_ZONE_PRICE9,
                   P_PRICE_ZONE_QUANTITY9,
                   P_PRICE_ZONE_QUANTITY_PRICE9,
                   P_PRICE_ZONE_PRICE10,
                   P_PRICE_ZONE_QUANTITY10,
                   P_PRICE_ZONE_QUANTITY_PRICE10,
                   P_PRICE_ZONE_PRICE11,
                   P_PRICE_ZONE_QUANTITY11,
                   P_PRICE_ZONE_QUANTITY_PRICE11,
                   P_PRICE_ZONE_PRICE12,
                   P_PRICE_ZONE_QUANTITY12,
                   P_PRICE_ZONE_QUANTITY_PRICE12,
                   P_PRICE_ZONE_PRICE13,
                   P_PRICE_ZONE_QUANTITY13,
                   P_PRICE_ZONE_QUANTITY_PRICE13,
                   P_PRICE_ZONE_PRICE14,
                   P_PRICE_ZONE_QUANTITY14,
                   P_PRICE_ZONE_QUANTITY_PRICE14,
                   P_PRICE_ZONE_PRICE15,
                   P_PRICE_ZONE_QUANTITY15,
                   P_PRICE_ZONE_QUANTITY_PRICE15,
                   P_PRICE_ZONE_PRICE16,
                   P_PRICE_ZONE_QUANTITY16,
                   P_PRICE_ZONE_QUANTITY_PRICE16,
                   P_PRICE_ZONE_PRICE17,
                   P_PRICE_ZONE_QUANTITY17,
                   P_PRICE_ZONE_QUANTITY_PRICE17,
                   P_PRICE_ZONE_PRICE18,
                   P_PRICE_ZONE_QUANTITY18,
                   P_PRICE_ZONE_QUANTITY_PRICE18,
                   P_PRICE_ZONE_PRICE19,
                   P_PRICE_ZONE_QUANTITY19,
                   P_PRICE_ZONE_QUANTITY_PRICE19,
                   P_PRICE_ZONE_PRICE20,
                   P_PRICE_ZONE_QUANTITY20,
                   P_PRICE_ZONE_QUANTITY_PRICE20,
                   P_PRICE_ZONE_PRICE21,
                   P_PRICE_ZONE_QUANTITY21,
                   P_PRICE_ZONE_QUANTITY_PRICE21,
                   P_PRICE_ZONE_PRICE22,
                   P_PRICE_ZONE_QUANTITY22,
                   P_PRICE_ZONE_QUANTITY_PRICE22,
                   P_PRICE_ZONE_PRICE23,
                   P_PRICE_ZONE_QUANTITY23,
                   P_PRICE_ZONE_QUANTITY_PRICE23,
                   P_PRICE_ZONE_PRICE24,
                   P_PRICE_ZONE_QUANTITY24,
                   P_PRICE_ZONE_QUANTITY_PRICE24,
                   P_PRICE_ZONE_PRICE25,
                   P_PRICE_ZONE_QUANTITY25,
                   P_PRICE_ZONE_QUANTITY_PRICE25,
                   P_PRICE_ZONE_PRICE26,
                   P_PRICE_ZONE_QUANTITY26,
                   P_PRICE_ZONE_QUANTITY_PRICE26,
                   P_PRICE_ZONE_PRICE27,
                   P_PRICE_ZONE_QUANTITY27,
                   P_PRICE_ZONE_QUANTITY_PRICE27,
                   P_PRICE_ZONE_PRICE28,
                   P_PRICE_ZONE_QUANTITY28,
                   P_PRICE_ZONE_QUANTITY_PRICE28,
                   P_PRICE_ZONE_PRICE29,
                   P_PRICE_ZONE_QUANTITY29,
                   P_PRICE_ZONE_QUANTITY_PRICE29,
                   P_PRICE_ZONE_PRICE30,
                   P_PRICE_ZONE_QUANTITY30,
                   P_PRICE_ZONE_QUANTITY_PRICE30,
                   '1',                             --P_PRICE_ZONE_TABLE_FLAG,
                   P_ORG_PRICE_ZONE_1,
                   P_ORG_PRICE_ZONE_2,
                   P_ORG_PRICE_ZONE_3,
                   P_ORG_PRICE_ZONE_4,
                   P_ORG_PRICE_ZONE_5,
                   P_ORG_PRICE_ZONE_6,
                   P_ORG_PRICE_ZONE_7,
                   P_ORG_PRICE_ZONE_8,
                   P_ORG_PRICE_ZONE_9,
                   P_ORG_PRICE_ZONE_10,
                   P_ORG_PRICE_ZONE_11,
                   P_ORG_PRICE_ZONE_12,
                   P_ORG_PRICE_ZONE_13,
                   P_ORG_PRICE_ZONE_14,
                   P_ORG_PRICE_ZONE_15,
                   P_ORG_PRICE_ZONE_16,
                   P_ORG_PRICE_ZONE_17,
                   P_ORG_PRICE_ZONE_18,
                   P_ORG_PRICE_ZONE_19,
                   P_ORG_PRICE_ZONE_20,
                   P_ORG_PRICE_ZONE_21,
                   P_ORG_PRICE_ZONE_22,
                   P_ORG_PRICE_ZONE_23,
                   P_ORG_PRICE_ZONE_24,
                   P_ORG_PRICE_ZONE_25,
                   P_ORG_PRICE_ZONE_26,
                   P_ORG_PRICE_ZONE_27,
                   P_ORG_PRICE_ZONE_28,
                   P_ORG_PRICE_ZONE_29,
                   P_ORG_PRICE_ZONE_30,
                   '1',                                      --P_APPROVE_FLAG,
                   P_ORG_ID);
   EXCEPTION
      WHEN PROGRAM_ERROR
      THEN
         l_error_message := 'Invalid Data: ' || l_error_message;
         fnd_message.set_name ('XXWC', 'XXWC_ADI_ERROR_MSG');
         fnd_message.set_token ('ERROR_MESSAGE', l_error_message);
      WHEN OTHERS
      THEN
         l_error_message := 'Unidentified error - ' || SQLERRM;
         fnd_message.set_name ('XXWC', 'XXWC_ADI_ERROR_MSG');
         fnd_message.set_token ('ERROR_MESSAGE', l_error_message);
   END;

   FUNCTION BPA_PRICE_VALIDATE (P_ZONE             IN NUMBER,
                                P_ZONE_PRICE       IN NUMBER,
                                P_ZONE_PRICE_QTY   IN NUMBER,
                                P_ZONE_QTY_PRICE   IN NUMBER)
      RETURN VARCHAR2
   IS
      /***************************************************************************************************************************
      -- PROGRAM TYPE: PL/SQL Script   <API>
      --
      -- PURPOSE: Function BPA_PRICE_VALIDATE To validate price zone, prize_zone_qty, and price_zone_price values in webadi.
      -- HISTORY
      -- =========================================================================================================================
      -- =========================================================================================================================
      -- VERSION DATE          AUTHOR(S)       DESCRIPTION
      -- ------- -----------   --------------- -----------------------------------------------------------------------------------
      -- 1.0     01-Apr-2015   P.Vamshidhar    Created this Function.
                                               TMS#20150414-00173-2015 PLM - Implement Cost Management tool enhancements
      -- 1.1     21-Sep-2015   Manjula C       TMS# 20150903-00018 - BPA- Cost Management issues
      ***************************************************************************************************************************/
      l_error_message   VARCHAR2 (500) := NULL;
      P_ZONE1           VARCHAR2 (20);
   BEGIN


--        IF P_ZONE=0 AND P_ZONE_PRICE IS NULL THEN -- Commented for Rev 1.1

        IF P_ZONE=0 AND ( P_ZONE_PRICE IS NULL OR P_ZONE_PRICE = 0) THEN -- Added for Rev 1.1

--           l_error_message := 'NATIONAL PRICE SHOULD NOT BE NULL';-- Commented for Rev 1.1
           l_error_message := 'NATIONAL PRICE SHOULD NOT BE NULL OR 0';-- Added for Rev 1.1

        END IF;

         IF (   (P_ZONE_PRICE_QTY IS NULL AND P_ZONE_QTY_PRICE IS NULL)
--           OR (    P_ZONE_PRICE_QTY IS NOT NULL -- Commented for Rev 1.1
--               AND P_ZONE_QTY_PRICE IS NOT NULL)) -- Commented for Rev 1.1
-- Added for Rev 1.1 Begin
             OR (    NVL(P_ZONE_PRICE_QTY,0) <> 0
                 AND P_ZONE_QTY_PRICE = 0)
       OR (    NVL(P_ZONE_PRICE_QTY,0) <> 0
                 AND NVL(P_ZONE_QTY_PRICE,0) <> 0))
-- Added for Rev 1.1 End
         THEN
            NULL;
         ELSE
            IF P_ZONE <> 0
            THEN
               l_error_message :=
                     'PRICE_ZONE_QTY'
                  || P_ZONE
                  || '/PRICE_ZONE_QTY_PRICE'
                  || P_ZONE;
            ELSE
               l_error_message := 'NATIONAL_QTY/NATIONAL_QTY_PRICE';
            END IF;
         END IF;
      RETURN l_error_message;
   END;
END XXWC_BPA_PRICE_ZONE_FORM_PKG;
/