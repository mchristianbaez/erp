---------------------------------------------------------------------------------------------------------------------------
/*************************************************************************************************************************
  $Header XXWC_MTL_ITEM_LOCATIONS_N12
  File Name: XXWC_MTL_ITEM_LOCATIONS_N12.sql
  PURPOSE:   
  REVISIONS:
     Ver        Date         Author         Description
     ---------  ----------   --------    -----------------------------------------------------------------------------------
     1.0        16-May-2016  Venu        --TMS#20160503-00088,20160429-00037 Bin Location With Sales History Program Running for longer time
****************************************************************************************************************************/
CREATE INDEX INV.XXWC_MTL_ITEM_LOCATIONS_N12 ON INV.MTL_ITEM_LOCATIONS (SUBSTR(SEGMENT1,3)) TABLESPACE APPS_TS_TX_DATA 
/
