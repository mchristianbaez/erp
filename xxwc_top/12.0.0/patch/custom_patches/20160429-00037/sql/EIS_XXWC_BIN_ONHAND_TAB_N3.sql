---------------------------------------------------------------------------------------------------------------------------
/*************************************************************************************************************************
  $Header EIS_XXWC_BIN_ONHAND_TAB_N3
  File Name: EIS_XXWC_BIN_ONHAND_TAB_N3.sql
  PURPOSE:   
  REVISIONS:
     Ver        Date         Author         Description
     ---------  ----------   --------    -----------------------------------------------------------------------------------
     1.0        16-May-2016  Siva        --TMS#20160429-00037 Bin Location Program Running for longer time
****************************************************************************************************************************/
CREATE INDEX XXEIS.EIS_XXWC_BIN_ONHAND_TAB_N3 ON XXEIS.EIS_XXWC_BIN_ONHAND_TAB
  (
    INVENTORY_ITEM_ID,
    ORGANIZATION_ID,
    PROCESS_ID
  )
  TABLESPACE APPS_TS_TX_DATA 
/

