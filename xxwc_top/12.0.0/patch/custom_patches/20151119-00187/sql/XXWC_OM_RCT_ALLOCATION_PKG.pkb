CREATE OR REPLACE PACKAGE BODY APPS.XXWC_OM_RCT_ALLOCATION_PKG
AS
   /*************************************************************************
   *   $Header XXWC_OM_RCT_ALLOCATION_PKG.pkb $
   *   Module Name: xxwc OM Automatic Receipt allocation package
   *
   *   PURPOSE:   Used in conc program to auto allocate on receipt of material
   *
   *   REVISIONS:
   *   Ver        Date        Author                     Description
   *   ---------  ----------  ---------------         -------------------------
   *   1.0        08/01/2013  Shankar Hariharan       Initial Version
   *   1.1        09/17/2013  Shankar Hariharan       Changed the default printer
   *   1.2        05/28/2014  Raghav Velichetti       Changed to ignore the force shipped lines TMS # 20140430-00253
   *   1.3        10/21/2014  Maharajan Shunmugam     TMS#20141001-00162 Canada Multi Org changes
   *   1.4        11/20/2015  Kishorebabu V           TMS#20151119-00187 XXWC_OM_RCT_ALLOCATION_PKG Cursor C2 starting at line 276 
   *                                                  add condition to SELECT, WHERE Clause b.subinventory != 'PUBD'
   * ***************************************************************************/

   PROCEDURE print_batch (i_request_id IN NUMBER)
   IS
      P_GROUP_ID                    NUMBER;
      P_COPIES                      NUMBER := 1;
      P_BATCH_ID                    NUMBER;
      P_PRINTER_NAME                VARCHAR2 (30);
      p_user_id                     NUMBER := fnd_global.USER_ID;
      p_resp_id                     NUMBER := fnd_global.RESP_ID;
      p_resp_appl_id                NUMBER := fnd_global.RESP_APPL_ID;
      p_print_price                 VARCHAR2 (10);
      p_return                      NUMBER;
      p_message                     VARCHAR2 (2000);

      CURSOR c1
      IS
           SELECT DISTINCT header_id, ship_from_org_id, created_by
             FROM XXWC_OM_RCT_ALLOCATE_TBL
            WHERE request_id = i_request_id
         ORDER BY header_id;



   BEGIN
      FOR c1_rec IN c1
      LOOP
         SELECT xxwc_print_request_groups_s.NEXTVAL INTO p_group_id FROM DUAL;

         SELECT xxwc_print_requests_s.NEXTVAL INTO p_batch_id FROM DUAL;

         SELECT xxwc_ont_routines_pkg.check_print_price (c1_rec.HEADER_ID)
                                                      -- , 'PICK')
           INTO p_print_price
           FROM DUAL;
        
         p_user_id := c1_rec.created_by;
         
         -- Added by Shankar 17-Sep-2013
         select xxwc_ont_routines_pkg.get_user_profile_printer (p_user_id)
           into p_printer_name
           from dual;
           
         IF p_printer_name is null then
         SELECT xxwc_ont_routines_pkg.get_printer ('XXWC'
                                                 , 'XXWC_OM_PICK_SLIP'
                                                 , c1_rec.ship_from_org_id
                                                 , p_user_id
                                                 , P_RESP_ID
                                                 , P_RESP_APPL_ID)
           INTO P_PRINTER_NAME
           FROM DUAL;
         END IF;
         
         INSERT INTO XXWC.XXWC_PRINT_REQUESTS_TEMP (CREATED_BY
                                                  , CREATION_DATE
                                                  , LAST_UPDATED_BY
                                                  , LAST_UPDATE_DATE
                                                  , BATCH_ID
                                                  , PROCESS_FLAG
                                                  , GROUP_ID
                                                  , APPLICATION
                                                  , PROGRAM
                                                  , DESCRIPTION
                                                  , START_TIME
                                                  , SUB_REQUEST
                                                  , PRINTER
                                                  , STYLE
                                                  , COPIES
                                                  , SAVE_OUTPUT
                                                  , PRINT_TOGETHER
                                                  , VALIDATE_PRINTER
                                                  , TEMPLATE_APPL_NAME
                                                  , TEMPLATE_CODE
                                                  , TEMPLATE_LANGUAGE
                                                  , TEMPLATE_TERRITORY
                                                  , OUTPUT_FORMAT
                                                  , NLS_LANGUAGE)
              VALUES (P_USER_ID
                    , SYSDATE
                    , P_USER_ID
                    , SYSDATE
                    , p_batch_id
                    , 1
                    , p_group_id
                    , 'XXWC'
                    , 'XXWC_OM_PICK_SLIP'
                    , 'XXWC OM Pick Slip Receport'
                    , NULL
                    , 'FALSE'
                    , P_PRINTER_NAME
                    , NULL
                    , P_COPIES
                    , 'TRUE'
                    , 'N'
                    , 'RESOLVE'
                    , 'XXWC'
                    , 'XXWC_OM_PICK_SLIP'
                    , 'en'
                    , 'US'
                    , 'PDF'
                    , 'en');

         INSERT INTO XXWC_PRINT_REQUESTS_ARG_TEMP
              VALUES (p_batch_id
                    , 1
                    , p_group_id
                    , 1
                    , c1_rec.ship_from_org_id);

         INSERT INTO XXWC_PRINT_REQUESTS_ARG_TEMP
              VALUES (p_batch_id
                    , 1
                    , p_group_id
                    , 2
                    , c1_rec.HEADER_ID);

         INSERT INTO XXWC_PRINT_REQUESTS_ARG_TEMP
              VALUES (p_batch_id
                    , 1
                    , p_group_id
                    , 3
                    , p_print_price);

         INSERT INTO XXWC_PRINT_REQUESTS_ARG_TEMP
              VALUES (p_batch_id
                    , 1
                    , p_group_id
                    , 4
                    , NULL);

         INSERT INTO XXWC_PRINT_REQUESTS_ARG_TEMP
              VALUES (p_batch_id
                    , 1
                    , p_group_id
                    , 5
                    , NULL);

         INSERT INTO XXWC_PRINT_REQUESTS_ARG_TEMP
              VALUES (p_batch_id
                    , 1
                    , p_group_id
                    , 6
                    , 2);

         INSERT INTO XXWC_PRINT_REQUESTS_ARG_TEMP
              VALUES (p_batch_id
                    , 1
                    , p_group_id
                    , 7
                    , NULL);

         INSERT INTO XXWC_PRINT_REQUESTS_ARG_TEMP
              VALUES (p_batch_id
                    , 1
                    , p_group_id
                    , 8
                    , NULL);

         INSERT INTO XXWC_PRINT_REQUESTS_ARG_TEMP
              VALUES (p_batch_id
                    , 1
                    , p_group_id
                    , 9
                    , 2);

         INSERT INTO XXWC_PRINT_REQUESTS_ARG_TEMP
              VALUES (p_batch_id
                    , 1
                    , p_group_id
                    , 10
                    , NULL);

        INSERT INTO XXWC_PRINT_REQUESTS_ARG_TEMP
              VALUES (p_batch_id
                    , 1
                    , p_group_id
                    , 11
                    , 1);

         --Commit Updates
         xxwc_ascp_scwb_pkg.proccommit;
         --fnd_file.put(fnd_file.log,'After inserting records');
         p_return :=
            XXWC_ONT_ROUTINES_PKG.SUBMIT_PRINT_BATCH (P_BATCH_ID
                                                    , P_GROUP_ID
                                                    , 1
                                                    , p_user_id
                                                    , p_resp_id
                                                    , p_resp_appl_id);
         fnd_file.put(fnd_file.log,'After calling print batch '||p_return);                                           
           --IF nvl(p_return,1) <> 0 then
             update apps.XXWC_OM_RCT_ALLOCATE_TBL
                set process_flag = 'Y'
              where request_id=i_request_id
                and header_id=c1_rec.header_id;
           --END IF;                                         
      END LOOP;
   EXCEPTION
      WHEN OTHERS
      THEN
         p_message :=
               'Error running XXWC_ONT_ROUTINES_PKG.SUBMIT_PRINT_BATCH '
            || SQLCODE
            || SQLERRM;
         fnd_file.put (fnd_file.output, p_message);
   END print_batch;


   PROCEDURE print_pick_ticket (errbuf                    OUT VARCHAR2
                              , retcode                   OUT VARCHAR2)
                  --            , i_shipment_header_id   IN     NUMBER)
   IS
      l_rec_qty             NUMBER;
      l_request_id          NUMBER := fnd_global.conc_request_id;
      l_ship_qty            NUMBER;
      l_user_id             NUMBER := fnd_global.user_id;
      l_resp_id             NUMBER := fnd_global.resp_id;
      l_resp_appl_id        NUMBER := fnd_global.resp_appl_id;
      l_order_exist         VARCHAR2(1) := 'N';


      CURSOR c1
      IS
         SELECT a.receipt_source_code
              , a.shipment_num
              , a.receipt_num
              , a.bill_of_lading
              , a.packing_slip
              , b.organization_code
              , c.line_num
              , c.quantity_shipped
              , c.quantity_received
              , c.shipment_line_status_code
              , d.segment1
              , d.description
              , a.shipment_header_id
              , c.shipment_line_id
              , c.item_id
              , a.ship_to_org_id
              , e.created_by
           FROM apps.rcv_shipment_headers a
              , apps.org_organization_definitions b
              , apps.rcv_shipment_lines c
              , apps.mtl_system_items d
              , apps.rcv_transactions e
          WHERE     receipt_source_code IN ('VENDOR', 'INTERNAL ORDER')
                AND a.ship_to_org_id = b.organization_id
                AND a.shipment_header_id = c.shipment_header_id
                AND c.item_id = d.inventory_item_id
                AND a.ship_to_org_id = d.organization_id
                AND c.shipment_line_status_code IN
                       ('FULLY RECEIVED', 'PARTIALLY RECEIVED')
                --AND a.shipment_header_id = i_shipment_header_id
                AND c.creation_date > sysdate -1
                AND NVL (c.attribute10, 'N') = 'N'
                AND e.shipment_header_id = c.shipment_header_id
                AND e.shipment_line_id = c.shipment_line_id
                AND e.transaction_type='RECEIVE';

      CURSOR c2 (
         i_ship_from_org_id    NUMBER
       , i_item_id             NUMBER)
      IS
           SELECT a.header_id
                , a.order_number
                , a.ordered_date
                , a.request_date
                , b.line_id
                , b.inventory_item_id
                , b.ordered_quantity
                , b.user_item_description
                , b.flow_status_code
                --, a.flow_status_code
             FROM apps.oe_order_headers a, apps.oe_order_lines b
            WHERE     a.header_id = b.header_id
                 -- AND a.order_type_id IN (1001, 1011)                                 --commented and added below for ver#1.3
                  AND a.order_type_id IN (fnd_profile.VALUE('XXWC_STANDARD_ORDER_TYPE'),fnd_profile.VALUE('XXWC_INTERNAL_ORDER_TYPE'))
                  AND b.ship_from_org_id = i_ship_from_org_id
                  AND b.inventory_item_id = i_item_id
                  AND UPPER (b.user_item_description) IN
                         ('BACKORDERED', 'OUT_FOR_DELIVERY/PARTIAL_BACKORDER','PARTIAL BACKORDERED')
                  AND b.flow_status_code IN ('BOOKED', 'AWAITING_SHIPPING')
                  AND a.flow_Status_code NOT IN
                         ('ENTERED', 'CANCELLED', 'CLOSED')
                  AND b.attribute11 is null --TMS # 20140430-00253 1.2 code Change to ignore the force shipped lines
                  AND xxwc_ont_routines_pkg.count_order_holds (a.header_id) = 0
				  AND b.subinventory <> 'PUBD'    ---TMS# 20151119-00187 1.4
         ORDER BY a.request_date ASC;

      CURSOR c3
      IS
         SELECT *
           FROM XXWC_OM_RCT_ALLOCATE_TBL
          WHERE request_id = l_request_id;

      CURSOR c4
      IS
         SELECT distinct (request_id)
           FROM XXWC_OM_RCT_ALLOCATE_TBL
          WHERE process_flag='N';


   BEGIN
      -- select all applicable order lines for the given item/branch
      apps.fnd_global.apps_initialize (l_user_id, l_resp_id, l_resp_appl_id);
      apps.mo_global.init ('ONT');

      FOR c1_rec IN c1
      LOOP
         fnd_file.put_line(fnd_file.log, 'Shipment '||c1_rec.shipment_num||' - Receipt '|| c1_rec.receipt_num);
         fnd_file.put_line(fnd_file.log, 'Item  '||c1_rec.segment1||' - Org '|| c1_rec.organization_code);
         fnd_file.put_line(fnd_file.log, 'Qty Shipped  '||c1_rec.quantity_shipped||' - Qty Received '|| c1_rec.quantity_shipped);
         
         l_rec_qty := c1_rec.quantity_received;
         l_order_exist := 'N' ;
         FOR c2_rec IN c2 (c1_rec.ship_to_org_id, c1_rec.item_id)
         LOOP
            l_order_exist := 'Y';
            
            fnd_file.put_line(fnd_file.log,'In Order Query...');
            fnd_file.put_line(fnd_file.log,'Order '||c2_rec.order_number||' - Line ID: '||c2_rec.line_id);
            fnd_file.put_line(fnd_file.log,'Ord Qty '||c2_rec.ordered_quantity);
            fnd_file.put_line(fnd_file.log,'user_item_description '||c2_rec.user_item_description||' - flow_status_code '||c2_rec.flow_status_code);
            
            IF l_rec_qty <= c2_rec.ordered_quantity
            THEN
               l_ship_qty := l_rec_qty;
            ELSE
               l_ship_qty := c2_rec.ordered_quantity;
            END IF;

            INSERT INTO apps.XXWC_OM_RCT_ALLOCATE_TBL (REQUEST_ID
                                                , SHIPMENT_HEADER_ID
                                                , SHIPMENT_LINE_ID
                                                , HEADER_ID
                                                , LINE_ID
                                                , SHIP_QTY
                                                , REQUEST_DATE
                                                , CREATION_DATE
                                                , CREATED_BY
                                                , SHIP_FROM_ORG_ID
                                                , PROCESS_FLAG)
                 VALUES (l_request_id
                       , c1_rec.shipment_header_id
                       , c1_rec.shipment_line_id
                       , c2_rec.header_id
                       , c2_rec.line_id
                       , l_ship_qty
                       , c2_rec.request_date
                       , SYSDATE
                       , c1_rec.created_by
                       , c1_rec.ship_to_org_id
                       , 'N');

            fnd_file.put_line(fnd_file.log,'Inserted into allocation table');
            
            l_rec_qty := l_rec_qty - c2_rec.ordered_quantity;
            
            fnd_file.put_line(fnd_file.log,'New l_rec_qty '||l_rec_qty);

            IF l_rec_qty <= 0
            THEN
               EXIT;
            END IF;
         END LOOP;

         UPDATE apps.rcv_shipment_lines
            SET attribute10 = 'Y'
          WHERE shipment_line_id = c1_rec.shipment_line_id;
      END LOOP;
      commit;
      -- update the force ship qty for the selected line
      FOR c3_rec IN c3
      LOOP
         UPDATE oe_order_lines
            SET attribute11 = c3_rec.ship_qty
                , last_update_date = sysdate
          WHERE line_id = c3_rec.line_id;
      END LOOP;
      COMMIT;
       
      -- Kick off all request in unprocessed state
      FOR c4_rec in c4
        LOOP
        print_batch (c4_rec.request_id);
        END LOOP; 
/*
     IF l_order_exist = 'Y' then
      print_batch (l_request_id);
      END IF;*/
   EXCEPTION
   WHEN OTHERS THEN
        fnd_file.put_line(fnd_file.log,'Main Exception: '||SQLERRM);
   END print_pick_ticket;
END;
/