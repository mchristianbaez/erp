CREATE OR REPLACE FORCE VIEW APPS.XXWC_PO_RECEIPTS_VW
(
/******************************************************************************
   NAME        :  APPS.XXWC_PO_RECEIPTS_VW
   PURPOSE     :  This view is created for extracting the Supplier and Supplier site details

   REVISIONS:
   Ver        Date        Author           Description
   ---------  ----------  ---------------  ------------------------------------
   1.0        10/01/2014   Pattabhi Avula     TMS#20141001-00057 Multi Org Changes
   2.0       03/04/2015   Raghavendra S       TMS# 20150303-00075 - FIN - Addition of Code Snippet for Line Breaks in All EDW Extract Programs
                                              and Views for all free entry test fields.
******************************************************************************/
   BUSINESS_UNIT,
   OPERATING_UNIT_ID,
   OPERATING_UNIT_NAME,
   SOURCE_SYSTEM,
   SHIP_FROM_VENDOR_NUM,
   VENDOR_ADDRESS_NAME,
   VENDOR_ADDRESS_LN1,
   VENDOR_ADDRESS_LN2,
   VENDOR_ADDRESS_LN3,
   VENDOR_CITY,
   VENDOR_STATE_PROVINCE,
   VENDOR_POSTAL_CODE,
   VENDOR_FAX,
   VENDOR_COUNTRY,
   VENDOR_TELEPHONE,
   DROP_SHIP_FLAG,
   PO_NUMBER,
   PO_CREATE_DATE,
   RECEIPT_DATE,
   BILL_TO_BRANCH_NUMBER,
   BILL_TO_BRANCH_NAME,
   BILL_TO_ADDRESS1,
   BILL_TO_ADDRESS2,
   BILL_TO_ADDRESS3,
   BILL_TO_CITY,
   BILL_TO_STATE_PROVINCE,
   BILL_TO_POSTAL_CODE,
   BILL_TO_COUNTRY,
   SHIP_TO_BRANCH_NUMBER,
   SHIP_TO_BRANCH_NAME,
   SHIP_TO_ADDRESS1,
   SHIP_TO_ADDRESS2,
   SHIP_TO_ADDRESS3,
   SHIP_TO_CITY,
   SHIP_TO_STATE_PROVINCE,
   SHIP_TO_POSTAL_CODE,
   SHIP_TO_COUNTRY,
   SKU_NUMBER,
   VENDOR_PART_NUMBER,
   ITEM_DESCRIPTION,
   ORDERED_QTY,
   RECEIVED_QTY,
   INVOICE_COST,
   PURCHASE_UOM,
   CONV_FACT_TO_EACH,
   INVOICE_NUM,
   UNIQUE_PO_IDENTIFIER,
   LAST_UPDATE_DATE,
   PO_HEADER_ID,
   PO_LINE_ID,
   LINE_LOCATION_ID,
   SHIPMENT_HEADER_ID,
   SHIPMENT_LINE_ID,
   TRANSACTION_ID,
   SHIPMENT_NUM,
   RECEIPT_NUM,
   SHIP_LINE_NUM,
   LINE_NUM,
   VENDOR_SITE_CODE,
   ITEM_ID,
   PLA_LAST_UPDATE_DATE,
   PLLA_LAST_UPDATE_DATE,
   RT_UOM,
   SOURCE_DOC_UOM,
   TRANSACTION_TYPE,
   INV_TRANSACTION_ID,
   QUANTITY,
   PRIMARY_QUANTITY
)
AS
   SELECT (SELECT NAME 
        FROM hr_operating_units 
        WHERE organization_id=fnd_global.org_id) business_unit,
          hou.organization_id operating_unit_id,
          hou.name operating_unit_name,
          'Oracle EBS' source_system-- 10/31/2012 CG: Changed to match Supplier Extract
                           -- , (asa.segment1 || '-' || assa.vendor_site_code)
                    --(asa.segment1 || '-' || assa.vendor_site_id) ship_from_vendor_num, -- Commented for V 2.0
          , REGEXP_REPLACE((asa.segment1 || '-' || assa.vendor_site_id),  '[[:cntrl:]]', ' ') ship_from_vendor_num -- Added for V 2.0
          , asa.vendor_name vendor_address_name
       --   assa.address_line1 vendor_address_ln1, -- Commented for V 2.0
       --   assa.address_line2 vendor_address_ln2, -- Commented for V 2.0
       --  (   assa.address_line3 -- Commented for V 2.0
       --    || DECODE (assa.address_line4, NULL, NULL, ' ') -- Commented for V 2.0
       --    || assa.address_line4) -- Commented for V 2.0
       --      vendor_address_ln3, -- Commented for V 2.0
        , REGEXP_REPLACE(assa.address_line1,'[[:cntrl:]]', ' ') vendor_address_ln1 -- Added for V 2.0
        , REGEXP_REPLACE(assa.address_line2,'[[:cntrl:]]', ' ') vendor_address_ln2 -- Added for V 2.0
        , REGEXP_REPLACE((   assa.address_line3
           || DECODE (assa.address_line4, NULL, NULL, ' ')
           || assa.address_line4),'[[:cntrl:]]', ' ') vendor_address_ln3, -- Added for V 2.0
          assa.city vendor_city,
          NVL (assa.state, assa.province) vendor_state_province,
          assa.zip vendor_postal_code,
          SUBSTR (
             xxwc_mv_routines_pkg.format_phone_number (
                assa.fax_area_code || assa.fax),
             1,
             30)
             vendor_fax,
          assa.country vendor_country,
          SUBSTR (
             xxwc_mv_routines_pkg.format_phone_number (
                assa.area_code || assa.phone),
             1,
             30)
             vendor_telephone,
          NVL (plla.drop_ship_flag, 'N') drop_ship_flag
          --, pha.segment1 po_number, -- 05/22/12 CG Changed to the PO true create date per Email 05/17
          , REGEXP_REPLACE(pha.segment1,'[[:cntrl:]]', ' ') po_number, -- Added for V 2.0
          -- pha.approved_date po_create_date,
          pha.creation_date po_create_date,
          rt.transaction_date receipt_date,                -- Bill to Location
          -- 05/31/2012 CG changed to display code only
          -- hl_bill.location_code bill_to_branch_number,
         -- NVL (SUBSTR (hl_bill.location_code,
         --              1,
         --               INSTR (hl_bill.location_code,
         --                       '-',
         --                      1,
         --                     1)
         --            - 1),
         --    hl_bill.location_code)
         -- bill_to_branch_number -- Commented for V 2.0
          REGEXP_REPLACE(NVL (SUBSTR (hl_bill.location_code,
                       1,
                         INSTR (hl_bill.location_code,
                                '-',
                                1,
                                1)
                       - 1),
               hl_bill.location_code),'[[:cntrl:]]', ' ') bill_to_branch_number, -- Added for V 2.0
          -- hl_bill.description bill_to_branch_name, -- Commented for V 2.0
          -- hl_bill.address_line_1 bill_to_address1, -- Commented for V 2.0
          -- hl_bill.address_line_2 bill_to_address2, -- Commented for V 2.0
          -- hl_bill.address_line_3 bill_to_address3, -- Commented for V 2.0
          REGEXP_REPLACE(hl_bill.description,'[[:cntrl:]]', ' ') bill_to_branch_name, -- Added for V 2.0
          REGEXP_REPLACE(hl_bill.address_line_1,'[[:cntrl:]]', ' ') bill_to_address1, -- Added for V 2.0
          REGEXP_REPLACE(hl_bill.address_line_2,'[[:cntrl:]]', ' ') bill_to_address2, -- Added for V 2.0
          REGEXP_REPLACE(hl_bill.address_line_3,'[[:cntrl:]]', ' ') bill_to_address3, -- Added for V 2.0
          hl_bill.town_or_city bill_to_city,
          hl_bill.region_2 bill_to_state_province,
          hl_bill.postal_code bill_to_postal_code,
          hl_bill.country bill_to_country,                 -- Ship To Location
          -- 05/31/2012 CG: Changed to adjust for DS and NonDS
          -- hl_ship.location_code ship_to_branch_number,
          mp_ship.organization_code ship_to_branch_number, -- hl_ship.description ship_to_branch_name,
         -- (CASE
         --     WHEN NVL (plla.drop_ship_flag, 'N') = 'Y'
         --     THEN
         --        hl_ship.description
         --     ELSE
         --        (SUBSTR (hl_ship.location_code,
         --                   INSTR (hl_ship.location_code,
         --                          '-',
         --                          1,
         --                          1)
         --                 + 1))
         --  END)
          --   ship_to_branch_name, -- Commented for V 2.0
          -- hl_ship.address1 ship_to_address1, -- Commented for V 2.0
          -- hl_ship.address2 ship_to_address2, -- Commented for V 2.0
          -- hl_ship.address3 ship_to_address3, -- Commented for V 2.0
          REGEXP_REPLACE((CASE
              WHEN NVL (plla.drop_ship_flag, 'N') = 'Y'
              THEN
                 hl_ship.description
              ELSE
                 (SUBSTR (hl_ship.location_code,
                            INSTR (hl_ship.location_code,
                                   '-',
                                   1,
                                   1)
                          + 1))
           END),'[[:cntrl:]]', ' ')  ship_to_branch_name, -- Added for V 2.0
          REGEXP_REPLACE(hl_ship.address1,'[[:cntrl:]]', ' ') ship_to_address1, -- Added for V 2.0
          REGEXP_REPLACE(hl_ship.address2,'[[:cntrl:]]', ' ') ship_to_address2, -- Added for V 2.0
          REGEXP_REPLACE(hl_ship.address3,'[[:cntrl:]]', ' ') ship_to_address3, -- Added for V 2.0
          hl_ship.city ship_to_city,
          hl_ship.state_province ship_to_state_province,
          hl_ship.postal_code ship_to_postal_code,
          hl_ship.country ship_to_country,                -- Item Receipt Info
          -- msib.segment1 sku_number -- CG: 12/06/12 CG Changed to use function to pull unique vendor part number -- -- Commented for V 2.0
          REGEXP_REPLACE(msib.segment1,'[[:cntrl:]]', ' ') sku_number, -- Added for V 2.0
                                   -- , mcr.cross_reference vendor_part_number
                   -- SUBSTR (
         --    apps.xxwc_mv_routines_pkg.get_item_cross_reference (rsl.item_id,
         --                                                       'VENDOR',
         --                                                        1),
         --    1,
         --    255)
         --    vendor_part_number, -- Commented for V 2.0
             REGEXP_REPLACE(SUBSTR(
             apps.xxwc_mv_routines_pkg.get_item_cross_reference (rsl.item_id,
                                                                 'VENDOR',
                                                                 1),
             1,
             255),'[[:cntrl:]]', ' ') vendor_part_number, -- Added for V 2.0
          -- NVL (pla.item_description, msib.description) item_description, -- Commented for V 2.0
          REGEXP_REPLACE(NVL (pla.item_description, msib.description),'[[:cntrl:]]', ' ') item_description, -- Added for V 2.0
          plla.quantity ordered_qty -- 11/20/2012 CG: Changed to make return negative
           -- 11/27/2012 CG: Changed to use primary quantity since it's signed
          ,
          (CASE
              WHEN rt.transaction_type = 'RETURN TO VENDOR'
              THEN
                 (NVL (rt.quantity, 0) * (-1))
              ELSE
                 rt.quantity
           END)
             received_qty,
          plla.price_override invoice_cost,
          pla.unit_meas_lookup_code purchase_uom, -- 05/31/2012 CG Changed to retrieve true conv rate to base
          -- pla.unit_meas_lookup_code conv_fact_to_each,
          -- 11/20/2012 CG: Changed to present factor based on receiving and source doc UOM
          /*xxwc_mv_routines_pkg.get_uom_conv_rate_to_base (
              pla.unit_meas_lookup_code
              )
            conv_fact_to_each*/
          apps.xxwc_mv_routines_pkg.get_uom_conv_rate_to_base (
             rt.unit_of_measure,
             rt.source_doc_unit_of_measure)
             conv_fact_to_each,
          SUBSTR (apps.xxwc_mv_routines_pkg.get_ap_invoice_num (
                     pha.po_header_id,
                     pla.po_line_id,
                     plla.line_location_id,
                     rsl.shipment_line_id,
                     rt.transaction_id),
                  1,
                  50)
             invoice_num,
          (   pha.segment1
           || pla.line_num
           || pra.release_num
           || plla.shipment_num
           || rsh.receipt_num
           || rsl.line_num
           || rt.transaction_id)
             unique_po_identifier,
          plla.last_update_date last_update_date,         -- Additional Fields
          pha.po_header_id,
          pla.po_line_id,
          plla.line_location_id,
          rsh.shipment_header_id,
          rsl.shipment_line_id,
          rt.transaction_id,
          rsh.shipment_num,
          rsh.receipt_num,
          rsl.line_num ship_line_num,
          pla.line_num,
          assa.vendor_site_code,
          pla.item_id,
          pla.last_update_date pla_last_update_date,
          plla.last_update_date plla_last_update_date,
          rt.unit_of_measure,
          rt.source_doc_unit_of_measure,
          rt.transaction_type,
          rt.inv_transaction_id,
          rt.quantity,
          rt.primary_quantity
     FROM rcv_transactions rt,
          rcv_shipment_headers rsh,
          rcv_shipment_lines rsl,
          apps.po_headers pha,
          apps.po_lines pla,
          apps.po_line_locations plla,
          (  SELECT po_header_id, MAX (release_num) release_num
               FROM apps.po_releases
           GROUP BY po_header_id) pra,
          ap_suppliers asa,
          apps.ap_supplier_sites assa,
          mtl_system_items_b msib -- CG: 12/06/12 CG Changed to use function to pull unique vendor part number
                                                  --, mtl_cross_references mcr
          ,
          hr_locations hl_bill,
          (SELECT location_id,
                  location_code,
                  description,
                  address_line_1 address1,
                  address_line_2 address2,
                  address_line_3 address3,
                  town_or_city city,
                  region_2 state_province,
                  postal_code,
                  country
             FROM hr_locations
           UNION
           SELECT location_id,
                  TO_CHAR (location_id) location_code,
                  orig_system_reference description,
                  address1,
                  address2,
                  (address3 || ' ' || address4) address3,
                  city,
                  NVL (state, province) state_province,
                  postal_code,
                  country
             FROM hz_locations) hl_ship,
          mtl_parameters mp_ship,
          hr_operating_units hou
    WHERE                             -- 10/29/12 CG: Aded RTV and Corrections
              -- 10/30/12 CG: Changed to user RECEIVE instead of DELIVER
              rt.transaction_type IN
                 ('RECEIVE', 'RETURN TO VENDOR', 'CORRECT') -- 11/06/12 CG: Added condition to only look at Receiving transactions
          AND rt.destination_type_code = 'RECEIVING'            -- 11/06/12 CG
          AND rt.shipment_header_id = rsh.shipment_header_id
          AND rt.shipment_line_id = rsl.shipment_line_id
          AND rt.po_header_id = pha.po_header_id
          AND rt.po_header_id = pla.po_header_id
          AND rt.po_line_id = pla.po_line_id
          AND rt.po_header_id = plla.po_header_id
          AND rt.po_line_id = plla.po_line_id
          AND rt.po_line_location_id = plla.line_location_id
          AND pha.vendor_id = asa.vendor_id
          AND pha.vendor_id = assa.vendor_id
          AND pha.vendor_site_id = assa.vendor_site_id
          AND pha.org_id = hou.organization_id
          AND pha.po_header_id = pra.po_header_id(+)
          AND rsl.item_id = msib.inventory_item_id(+)
          AND rsl.to_organization_id = msib.organization_id(+)
          -- CG: 12/06/12 CG Changed to use function to pull unique vendor part number
          --AND rsl.item_id = mcr.inventory_item_id(+)
          --AND mcr.cross_reference_type(+) = 'VENDOR'
          AND pha.bill_to_location_id = hl_bill.location_id
          AND plla.ship_to_location_id = hl_ship.location_id(+)
          AND plla.ship_to_organization_id = mp_ship.organization_id(+)
-- TMS# 20130608-00263 > Start (Added to include ReturnToVendor transactions)
UNION
   SELECT (SELECT NAME 
    FROM hr_operating_units 
    WHERE organization_id=fnd_global.org_id)         business_unit,
          hou.organization_id             operating_unit_id,
          hou.name                 operating_unit_name,
          'Oracle EBS'                 source_system
         -- (asa.segment1 || '-' || assa.vendor_site_id)     ship_from_vendor_num, -- commented for V 2.0
          , REGEXP_REPLACE((asa.segment1 || '-' || assa.vendor_site_id),  '[[:cntrl:]]', ' ') ship_from_vendor_num,  -- Added for V 2.0
          -- asa.vendor_name                 vendor_address_name, -- commented for V 2.0
          -- assa.address_line1              vendor_address_ln1, -- commented for V 2.0
          -- assa.address_line2              vendor_address_ln2, -- commented for V 2.0
          -- (   assa.address_line3
          -- || DECODE (assa.address_line4, NULL, NULL, ' ')
          -- || assa.address_line4)        vendor_address_ln3, -- commented for V 2.0
           REGEXP_REPLACE(asa.vendor_name,  '[[:cntrl:]]', ' ') vendor_address_name  -- Added for V 2.0
           , REGEXP_REPLACE(assa.address_line1,  '[[:cntrl:]]', ' ') vendor_address_ln1  -- Added for V 2.0
           , REGEXP_REPLACE(assa.address_line2,  '[[:cntrl:]]', ' ') vendor_address_ln2  -- Added for V 2.0
           , REGEXP_REPLACE((assa.address_line3
           || DECODE (assa.address_line4, NULL, NULL, ' ')
           || assa.address_line4),  '[[:cntrl:]]', ' ') vendor_address_ln3  -- Added for V 2.0
          , assa.city                      vendor_city,
          NVL (assa.state, assa.province)         vendor_state_province,
          assa.zip                       vendor_postal_code,
          SUBSTR (
             xxwc_mv_routines_pkg.format_phone_number (
                assa.fax_area_code || assa.fax),
             1,
             30)                         vendor_fax,
          assa.country                   vendor_country,
          SUBSTR (
             xxwc_mv_routines_pkg.format_phone_number (
                assa.area_code || assa.phone),
             1,
             30)                         vendor_telephone,
          NULL                         drop_ship_flag,
          to_char(mmt.transaction_id)  po_number,
          mmt.creation_date            po_create_date,
          mmt.creation_date            receipt_date,
          mp.organization_code         bill_to_branch_number,
          hl.location_code             bill_to_branch_name,
         -- hl.address_line_1            bill_to_address1, -- commented for V 2.0
         -- hl.address_line_2            bill_to_address2, -- commented for V 2.0
         -- hl.address_line_3            bill_to_address3, -- commented for V 2.0
           REGEXP_REPLACE(hl.address_line_1,  '[[:cntrl:]]', ' ') bill_to_address1  -- Added for V 2.0
          , REGEXP_REPLACE(hl.address_line_2,  '[[:cntrl:]]', ' ') bill_to_address2  -- Added for V 2.0
          , REGEXP_REPLACE(hl.address_line_3,  '[[:cntrl:]]', ' ') bill_to_address3,  -- Added for V 2.0
          hl.town_or_city              bill_to_city,
          hl.region_2                  bill_to_state_province,
          hl.postal_code               bill_to_postal_code,
          hl.country                   bill_to_country,
          mp.organization_code         ship_to_branch_number,
          hl.location_code             ship_to_branch_name,
       --   hl.address_line_1            ship_to_address1, -- commented for V 2.0
       --   hl.address_line_2            ship_to_address2, -- commented for V 2.0
       --   hl.address_line_3            ship_to_address3, -- commented for V 2.0
           REGEXP_REPLACE(hl.address_line_1,  '[[:cntrl:]]', ' ') ship_to_address1  -- Added for V 2.0
          , REGEXP_REPLACE(hl.address_line_2,  '[[:cntrl:]]', ' ') ship_to_address2  -- Added for V 2.0
          , REGEXP_REPLACE(hl.address_line_3,  '[[:cntrl:]]', ' ') ship_to_address3,  -- Added for V 2.0
          hl.town_or_city              ship_to_city,
          hl.region_2                  ship_to_state_province,
          hl.postal_code               ship_to_postal_code,
          hl.country                   ship_to_country,
        --  msib.segment1                sku_number,
          REGEXP_REPLACE(msib.segment1 ,  '[[:cntrl:]]', ' ') sku_number,  -- Added for V 2.0
        --  SUBSTR (apps.xxwc_mv_routines_pkg.get_item_cross_reference (mmt.inventory_item_id, -- commented for V 2.0
        --                                                         'VENDOR',                     -- commented for V 2.0
        --                                                         1),                        -- commented for V 2.0    
        --     1,                                                                                -- commented for V 2.0
        --     255)                      vendor_part_number,                                    -- commented for V 2.0
          REGEXP_REPLACE( (SUBSTR (apps.xxwc_mv_routines_pkg.get_item_cross_reference (mmt.inventory_item_id,
                                                                 'VENDOR',
                                                                 1),
             1,
             255)),  '[[:cntrl:]]', ' ') vendor_part_number,  -- Added for V 2.0
          --msib.description             item_description,    -- commented for V 2.0
          REGEXP_REPLACE(msib.description,  '[[:cntrl:]]', ' ') item_description,  -- Added for V 2.0
          mmt.transaction_quantity     ordered_qty,
          mmt.transaction_quantity     received_qty,
--          to_number(mmt.attribute3)    invoice_cost,
          mmt.actual_cost              invoice_cost,       -- TMS# 20131111-00291 
          mmt.transaction_uom          purchase_uom, 
          1                            conv_fact_to_each,
          REPLACE(REPLACE(transaction_reference,CHR(10),''),CHR(13),'')   invoice_num,
          'RTV-'||mmt.transaction_id  unique_po_identifier,
          mmt.last_update_date        last_update_date,
      NULL  po_header_id,
      NULL  po_line_id,
      NULL  line_location_id,
      NULL  shipment_header_id,
      NULL  shipment_line_id,
      NULL  transaction_id,
      NULL  shipment_num,
      NULL  receipt_num,
      NULL  ship_line_num,
      NULL  line_num,
      NULL  vendor_site_code,
      NULL  item_id,
      NULL  pla_last_update_date,
      NULL  plla_last_update_date,
      NULL  unit_of_measure,
      NULL  source_doc_unit_of_measure,
      NULL  transaction_type,
      NULL  inv_transaction_id,
      NULL  quantity,
      NULL  primary_quantity
     FROM xxwc.xxwc_mtl_material_trx_tbl mmt, -- TMS# 20131108-00103  
         --apps.MTL_MATERIAL_TRANSACTIONS mmt,-- TMS# 20131108-00103
         --apps.MTL_TRX_TYPES_VIEW mttv,      -- TMS# 20131108-00103
          ap_suppliers asa,
          apps.ap_supplier_sites assa,
          mtl_system_items_b msib,
          hr_locations hl,
          mtl_parameters mp,
          hr_operating_units hou
    WHERE 1=1 
--      AND mmt.transaction_type_id    =  mttv.transaction_type_id  -- TMS# 20131108-00103
--      AND mttv.transaction_type_name = 'WC Return to Vendor'      -- TMS# 20131108-00103
      AND mmt.attribute1             = asa.segment1
      AND mmt.attribute5             = to_char(assa.vendor_site_id)
      AND asa.vendor_id              = assa.vendor_id
      AND mmt.organization_id        = msib.organization_id
      AND mmt.inventory_item_id      = msib.inventory_item_id
      AND mmt.organization_id        = hl.inventory_organization_id
      AND hl.bill_to_site_flag       = 'Y'
      AND mmt.organization_id        = mp.organization_id
    --  AND assa.ORG_ID                = hou.organization_id  -- TMS# 20130608-00263 < End -- commented by Pattabhi on 10/01/2014 for TMS#20141001-00057 Multi Org Changes
	;