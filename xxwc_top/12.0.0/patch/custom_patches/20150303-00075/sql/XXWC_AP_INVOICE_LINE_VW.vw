CREATE OR REPLACE FORCE VIEW apps.xxwc_ap_invoice_line_vw (
/******************************************************************************
   NAME        :  APPS.xxwc_ap_invoice_line_vw
   PURPOSE    :  This view is created for extracting the PAYABLE INVOICE lINES details

   REVISIONS:
   Ver        Date        Author           Description
   ---------  ----------  ---------------  ------------------------------------
   1.0        31/07/2012  Consuelo        Initial Version
   2.0        3/31/2015   Raghavendra s   TMS# 20150303-00075 - FIN - Addition of Code Snippet for Line Breaks in All EDW Extract Programs
                                           and Views for all free entry text fields.
******************************************************************************/
                                                           business_unit,
                                                           operating_unit_id,
                                                           operating_unit_name,
                                                           source_system,
                                                           document_number,
                                                           invoice_line_number,
                                                           invoice_type,
                                                           inv_line_gl_post_date,
                                                           invoice_line_type,
                                                           last_update_date,
                                                           invoice_line_amount,
                                                           gl_account_number,
                                                           po_number,
                                                           branch,
                                                           invoice_date,
                                                           invoice_id,
                                                           invoice_num,
                                                           line_description,
                                                           overlay_dist_code_concat
                                                          )
AS
   SELECT   'WHITECAP' business_unit, hou.organization_id operating_unit_id,
            hou.NAME operating_unit_name, 'Oracle EBS' source_system,
            -- aia.invoice_num document_number --  Commented for V 2.0
            REGEXP_REPLACE((aia.invoice_num || '-' || aia.invoice_id),  '[[:cntrl:]]', ' ') document_number, -- Added for V 2.0
            aila.line_number invoice_line_number,
            
            -- 05/30/2012 CG Not needed pulled based on vendor type
            --inv_tp.meaning invoice_type,
            (CASE
                WHEN asa.vendor_type_lookup_code = 'VENDOR'
                   THEN 'I'
                ELSE 'E'
             END
            ) invoice_type,
            aila.accounting_date inv_line_gl_post_date,
            
            -- 05/30/2012 CG Not needed determined based on line level values
            -- ln_tp.meaning invoice_line_type,
            (CASE
                WHEN aila.line_type_lookup_code = 'ITEM'
                   THEN 'M'
                WHEN aila.line_type_lookup_code = 'FREIGHT'
                   THEN 'F'
                WHEN aila.line_type_lookup_code = 'TAX'
                   THEN 'T'
                WHEN aila.line_type_lookup_code = 'MISCELLANEOUS'
                   THEN 'O'
                ELSE 'M'
             END
            ) invoice_line_type,
            aila.last_update_date, aila.amount invoice_line_amount,
            gcc.segment4 gl_account_number,
            --NVL (pha.segment1, aila.description) po_number --  Commented for V 2.0
            REGEXP_REPLACE(NVL (pha.segment1, aila.description),  '[[:cntrl:]]', ' ') po_number,  -- Added for V 2.0
            gcc.segment2 branch,
                                -- additional fields
                                aia.invoice_date, aia.invoice_id,
            aia.invoice_num, 
            -- aila.description  line_description --  Commented for V 2.0
            REGEXP_REPLACE(aila.description,  '[[:cntrl:]]', ' ') line_description, -- Added for V 2.0
            aila.overlay_dist_code_concat
       FROM apps.ap_invoice_lines aila,
            apps.ap_invoices aia,
            hr_operating_units hou,
            gl_code_combinations gcc,
            -- 05/30/2012 CG Not needed pulled based on vendor type
            -- fnd_lookup_values inv_tp,
            -- fnd_lookup_values ln_tp,
            apps.po_headers pha,
            ap_suppliers asa
      WHERE aila.invoice_id = aia.invoice_id
        AND aila.org_id = hou.organization_id
        AND aila.default_dist_ccid = gcc.code_combination_id
        --AND aia.invoice_type_lookup_code = inv_tp.lookup_code
        --AND inv_tp.lookup_type = 'INVOICE TYPE'
        --AND inv_tp.LANGUAGE = 'US'
        --AND inv_tp.view_application_id = 200
        --AND aila.line_type_lookup_code = ln_tp.lookup_code
        --AND ln_tp.lookup_type = 'INVOICE LINE TYPE'
        --AND ln_tp.LANGUAGE = 'US'
        --AND ln_tp.view_application_id = 200
        AND aila.po_header_id = pha.po_header_id(+)
        AND aia.vendor_id = asa.vendor_id
   ORDER BY aia.invoice_id, aila.line_number;