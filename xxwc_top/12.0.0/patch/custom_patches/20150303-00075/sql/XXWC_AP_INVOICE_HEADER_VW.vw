CREATE OR REPLACE FORCE VIEW apps.xxwc_ap_invoice_header_vw (
/******************************************************************************
   NAME        :  APPS.xxwc_ap_invoice_header_vw
   PURPOSE    :  This view is created for extracting the PAYABLE INVOICE HEADER details

   REVISIONS:
   Ver        Date        Author           Description
   ---------  ----------  ---------------  ------------------------------------
   1.0        22/06/2012  Consuelo        Initial Version
   2.0        3/31/2015    Raghavendra s   TMS# 20150303-00075 - FIN - Addition of Code Snippet for Line Breaks in All EDW Extract Programs
                                           and Views for all free entry text fields.
******************************************************************************/
                                                             business_unit,
                                                             operating_unit_id,
                                                             operating_unit_name,
                                                             source_system,
                                                             document_number,
                                                             vendor_number,
                                                             vendor_invoice_number,
                                                             invoice_type_code,
                                                             vendor_invoice_date,
                                                             vendor_invoice_entered_date,
                                                             vendor_invoice_last_upd_date,
                                                             vendor_invoice_status,
                                                             payment_terms_id,
                                                             payment_due_date,
                                                             payment_date,
                                                             payment_amount,
                                                             cash_discount_amount,
                                                             inv_merch_amount,
                                                             inv_freight_charges,
                                                             inv_tax_amount,
                                                             inv_misc_charges,
                                                             net_invoice_total,
                                                             invoice_id,
                                                             invoice_num,
                                                             payment_status_flag,
                                                             payment_term,
                                                             payment_num,
                                                             check_num,
                                                             base_amount,
                                                             freight_amount,
                                                             tax_amount,
                                                             description,
                                                             vendor_id,
                                                             vendor_site_id,
                                                             vendor_name
                                                            )
AS
   SELECT 'WHITECAP' business_unit, hou.organization_id operating_unit_id,
          hou.NAME operating_unit_name, 'Oracle EBS' source_system,
          -- aia.invoice_num document_number --  Commented for V 2.0
          REGEXP_REPLACE((aia.invoice_num || '-' || aia.invoice_id),  '[[:cntrl:]]', ' ') document_number, -- Added for V 2.0
          asa.segment1 vendor_number, aia.invoice_num vendor_invoice_number,
          
          -- 05/30/2012 CG Changed to be based on vendor type
          -- aia.invoice_type_lookup_code invoice_type_code,
          (CASE
              WHEN asa.vendor_type_lookup_code = 'VENDOR'
                 THEN 'I'
              ELSE 'E'
           END
          ) invoice_type_code,
          aia.invoice_date vendor_invoice_date,
          aia.creation_date vendor_invoice_entered_date,
          aia.last_update_date vendor_invoice_last_upd_date,
          
          -- 05/30/2012 CG: Changed to use a combination of field and
          -- function to retrieve a status
          /*ap_invoices_utility_pkg.get_invoice_status
                          (aia.invoice_id,
                           aia.invoice_amount,
                           aia.payment_status_flag,
                           aia.invoice_type_lookup_code
                          ) vendor_invoice_status,*/
          (CASE
              WHEN aia.cancelled_date IS NOT NULL
                 THEN 'CANCELLED'
              WHEN (    aia.cancelled_date IS NULL
                    AND ap_invoices_utility_pkg.get_posting_status
                                                               (aia.invoice_id) IN
                                                                   ('N', 'S')
                   )
                 THEN 'NOT POSTED'
              WHEN (    aia.cancelled_date IS NULL
                    AND ap_invoices_utility_pkg.get_posting_status
                                                               (aia.invoice_id) =
                                                                           'P'
                   )
                 THEN 'PARTIAL POST'
              WHEN (    aia.cancelled_date IS NULL
                    AND ap_invoices_utility_pkg.get_posting_status
                                                               (aia.invoice_id) =
                                                                           'Y'
                    AND ap_invoices_utility_pkg.get_payment_status
                                                               (aia.invoice_id) =
                                                                           'N'
                   )
                 THEN 'POSTED-NOT PAID'
              WHEN (    aia.cancelled_date IS NULL
                    AND ap_invoices_utility_pkg.get_posting_status
                                                               (aia.invoice_id) =
                                                                           'Y'
                    AND ap_invoices_utility_pkg.get_payment_status
                                                               (aia.invoice_id) =
                                                                           'P'
                   )
                 THEN 'PARTIALLY-PAID'
              WHEN (    aia.cancelled_date IS NULL
                    AND ap_invoices_utility_pkg.get_posting_status
                                                               (aia.invoice_id) =
                                                                           'Y'
                    AND ap_invoices_utility_pkg.get_payment_status
                                                               (aia.invoice_id) =
                                                                           'Y'
                   )
                 THEN 'PAID'
              ELSE 'UNK'
           END
          ) vendor_invoice_status,
          aia.terms_id payment_terms_id, apsa.due_date payment_due_date,
          aip.accounting_date payment_date, aip.amount payment_amount,
          aip.discount_taken cash_discount_amount,
          NVL
             (ROUND (ap_invoices_utility_pkg.get_item_total (aia.invoice_id,
                                                             aia.org_id
                                                            ),
                     2
                    ),
              0
             ) inv_merch_amount,
          NVL
             (ROUND
                  (ap_invoices_utility_pkg.get_freight_total (aia.invoice_id,
                                                              aia.org_id
                                                             ),
                   2
                  ),
              0
             ) inv_freight_charges,
          NVL
             (ROUND (xxwc_mv_routines_pkg.get_ap_invoice_tax (aia.invoice_id,
                                                              aia.org_id
                                                             ),
                     2
                    ),
              0
             ) inv_tax_amount,
          NVL
             (ROUND (ap_invoices_utility_pkg.get_misc_total (aia.invoice_id,
                                                             aia.org_id
                                                            ),
                     2
                    ),
              0
             ) inv_misc_charges,
          ROUND (aia.invoice_amount, 2) net_invoice_total,
                                                          ---- Additional column data
                                                          aia.invoice_id,
          aia.invoice_num, aia.payment_status_flag,
          -- att.NAME payment_term  -- Commented for V 2.0
          REGEXP_REPLACE(att.NAME,  '[[:cntrl:]]', ' ') payment_term, -- Added for V 2.0
          aip.payment_num, aca.check_number, aia.base_amount,
          aia.freight_amount, aia.tax_amount, 
          -- aia.description  -- Commented for V 2.0
          REGEXP_REPLACE(aia.description,  '[[:cntrl:]]', ' '),  -- Added for V 2.0
          asa.vendor_id, 
          aia.vendor_site_id, asa.vendor_name 
     FROM apps.ap_invoices aia,
          apps.ap_payment_schedules apsa,
          apps.ap_invoice_payments aip,
          -- 05/22/12 CG Added to excludded cancelled payments
          apps.ap_checks aca,
          ap_terms_tl att,
          ap_suppliers asa,
          hr_operating_units hou
    WHERE aia.invoice_id = apsa.invoice_id(+)
      AND apsa.invoice_id = aip.invoice_id(+)
      AND NVL (aip.reversal_flag(+), 'N') = 'N'
      -- 05/22/12 CG Added to excludded cancelled payments
      AND aip.check_id = aca.check_id(+)
      --AND apsa.checkrun_id = aca.checkrun_id(+)
      AND aca.status_lookup_code(+) != 'VOIDED'
      AND aca.void_date(+) IS NULL
      -- 05/22/12 CG
      AND aia.org_id = hou.organization_id
      AND aia.vendor_id = asa.vendor_id
      AND aia.terms_id = att.term_id(+);