CREATE OR REPLACE FORCE VIEW apps.xxwc_ap_supplier_sites_vw (
/******************************************************************************
   NAME        :  APPS.XXWC_AP_SUPPLIER_SITES_VW
   PURPOSE    :  This view is created for extracting the Supplier and Supplier site details

   REVISIONS:
   Ver        Date        Author           Description
   ---------  ----------  ---------------  ------------------------------------
   1.0        31/07/2012  Consuelo        Initial Version
   2.0        3/4/2015    Raghavendra s   TMS# 20150303-00075 - FIN - Addition of Code Snippet for Line Breaks in All EDW Extract Programs
                                           and Views for all free entry text fields.
******************************************************************************/
   business_unit
 , source_system
 , operating_unit_id
 , operating_unit_name
 , oracle_id
 , vendor_number
 , pay_to_tax_payer_id
 , ship_from_tax_id
 , duns1
 , duns2
 , duns3
 , vendor_name
 , vendor_enabled
 , vendor_site_id
 , vendor_site_code
 , pay_to_vendor_name
 , site_identifier
 , purchasing_site_flag
 , pay_site_flag
 , address1
 , address2
 , address3
 , address23
 , city
 , state_province
 , postal_code
 , country
 , fax_number
 , phone_number
 , vendor_type_lookup_code
 , payment_term_id
 , payment_term
 , discount_percent
 , discount_days_or_day
 , supplier_creation_date
 , site_creation_date
 , site_inactive_date
 , site_status
 , ultimate_duns
 , hq_duns
 , duns
 , edw_vendor_type_code
 , primary_pay_site
)
AS
     SELECT   'WHITECAP' business_unit
            , 'Oracle EBS' source_system
            , assa.org_id operating_unit_id
            , hou.name operating_unit_name
            , asa.vendor_id oracle_id
            , asa.segment1 vendor_number
            , asa.num_1099 pay_to_tax_payer_id
            , hp.jgzz_fiscal_code ship_from_tax_id
            , NULL duns1
            , NULL duns2
            , NULL duns3
            -- ,asa.vendor_name  -- Commented for V 2.0
            , REGEXP_REPLACE(asa.vendor_name,  '[[:cntrl:]]', ' ') vendor_name -- Added for V 2.0
            , asa.enabled_flag vendor_enabled
            , assa.vendor_site_id
            --, assa.vendor_site_code -- Commented for V 2.0
            , REGEXP_REPLACE(assa.vendor_site_code,  '[[:cntrl:]]', ' ') vendor_site_code -- Added for V 2.0
            , assa.vendor_site_code_alt pay_to_vendor_name
            -- 10/29/2012 CG: Changed to be the site id
            --, (asa.segment1 || '-' || assa.vendor_site_code) site_identifier
            --, (asa.segment1 || '-' || assa.vendor_site_id) site_identifier -- Commented for V 2.0
            , REGEXP_REPLACE((asa.segment1 || '-' || assa.vendor_site_id),  '[[:cntrl:]]', ' ') site_identifier -- Added for V 2.0
            , assa.purchasing_site_flag
            , assa.pay_site_flag
        --    , assa.address_line1 address1 -- Commented for V 2.0
        --    , assa.address_line2 address2 -- Commented for V 2.0
        --    , assa.address_line3 address3 -- Commented for V 2.0
        --    , (   assa.address_line2
        --       || DECODE (assa.address_line3, NULL, NULL, ' ')
        --       || assa.address_line3)
        --         address23 -- Commented for V 2.0
            , REGEXP_REPLACE(assa.address_line1 ,  '[[:cntrl:]]', ' ') address1 -- Added for V 2.0
            , REGEXP_REPLACE(assa.address_line2 ,  '[[:cntrl:]]', ' ') address2 -- Added for V 2.0
            , REGEXP_REPLACE(assa.address_line3 ,  '[[:cntrl:]]', ' ') address3 -- Added for V 2.0
            , REGEXP_REPLACE((assa.address_line2
               || DECODE (assa.address_line3, NULL, NULL, ' ')
               || assa.address_line3),  '[[:cntrl:]]', ' ') 
                 address23
            , assa.city
            , NVL (assa.state, assa.province) state_province
            , assa.zip postal_code
            , assa.country
            , (assa.fax_area_code || ' ' || assa.fax) fax_number
            , (assa.area_code || ' ' || assa.phone) phone_number
            , asa.vendor_type_lookup_code
            , NVL (assa.terms_id, asa.terms_id) payment_term_id
            , att.name payment_term
            , atl.discount_percent
            , NVL (atl.discount_days, atl.discount_day_of_month)
                 discount_days_or_day
            , asa.creation_date supplier_creation_date
            , assa.creation_date site_creation_date
            , assa.inactive_date site_inactive_date
            , (CASE
                  WHEN NVL (assa.inactive_date, SYSDATE + 1) > SYSDATE
                  THEN
                     'Active'
                  ELSE
                     'Inactive'
               END)
                 site_status
            , NULL ultimate_duns
            , NULL hq_duns
            , NULL duns
            , (CASE
                  WHEN asa.vendor_type_lookup_code = 'EXPENSE' THEN 'EMPLOYEE'
                  WHEN asa.vendor_type_lookup_code = 'FREIGHT' THEN 'FREIGHT'
                  ELSE NULL
               END)
                 edw_vendor_type_code
            -- 10/31/2012 CG
            -- 01/14/2013: Corrected to use Vendor Num instead of ID
            , SUBSTR (
                 xxwc_mv_routines_pkg.get_edw_supp_prim_pay_site (asa.segment1 --asa.vendor_id
                                                                , assa.org_id)
               , 1
               , 80
              )
                 primary_pay_site
       FROM   ap_suppliers asa
            , ap_supplier_sites_all assa
            , hr_operating_units hou
            , ap_terms_tl att
            , ap_terms_lines atl
            , hz_parties hp
      WHERE       asa.vendor_id = assa.vendor_id(+)
              AND asa.party_id = hp.party_id(+)
              AND hp.application_id(+) = 200
              AND assa.org_id = hou.organization_id(+)
              AND NVL (assa.terms_id, asa.terms_id) = att.term_id
              AND att.term_id = atl.term_id(+)
   ORDER BY   asa.vendor_name, assa.vendor_site_code;


