CREATE OR REPLACE PACKAGE BODY APPS.xxwc_ap_ind_srcg_pkg AS

  /********************************************************************************
  
  FILE NAME: xxwc_ap_ind_srcg_pkg.pkb
  
  PROGRAM TYPE: PL/SQL Package Body
  
  PURPOSE: Extract Oracle Account Payables Sourcing data for the given input parameters and
           generate a Pipe delimited file. The file will be FTPed to UC4 and
           finally to Sourcing Data Mart.
  
  SERVICE TICKET: XXXXX
  
  HISTORY
  ===============================================================================
  VERSION DATE          AUTHOR(S)       DESCRIPTION
  ------- -----------   --------------- -----------------------------------------
  1.0     10/20/2011    Gopi Damuluri    Initial creation of the procedure
  1.1     03/03/2014    Gopi Damuluri    TMS# 20140225-00075
                                         Process is modified to consider last 6 week 
                                         invoices irrespective whether GL Period is open/closed
  1.2     06/24/2014    Gopi Damuluri    TMS#  20140624-00015 
                                         To update TAX_ID column in PAY_TO_VENDOR extract for both CRP and WC feed
  1.3     25/09/2014	Veera C			 TMS# 20141001-00163 Modified code as per the Canada OU Test	
  1.4     03/12/2015    Raghavendra S    TMS# 20150303-00075 - FIN - Addition of Code Snippet for Line Breaks in All EDW Extract Programs
                                         and Views for all free entry text fields.  
  ********************************************************************************/

  l_calling_proc_name     VARCHAR2(75) := 'xxwc_ap_ind_srcg_pkg.create_payables_file';
  l_pay_group_lookup_code VARCHAR2(100) := 'INVENTORY';
  --l_org_id                NUMBER := 162;                          --25/09/2014 Commented by Veera as per the Canada OU Test
  l_org_id                 NUMBER :=fnd_profile.VALUE('ORG_ID');    --25/09/2014 Added by Veera as per the Canada OU Test
  l_creation_date         VARCHAR2(15) := '01-JAN-2005';
  l_lookup_type           VARCHAR2(50) := 'XXCUSAP_BUSINESS_UNIT';
--  l_start_date            DATE := to_date('28-MAY-2012', 'DD-MON-YYYY');
--  l_end_date              DATE := to_date('28-OCT-2012', 'DD-MON-YYYY');
  l_start_date            DATE := TRUNC(SYSDATE) - 41; -- Version# 1.1
  l_end_date              DATE := TRUNC(SYSDATE);      -- Version# 1.1
  --  l_period_name                VARCHAR2(15)  := get_curr_period;

  --Email Defaults
  pl_dflt_email fnd_user.email_address%TYPE := 'HDSOracleDevelopers@hdsupply.com';

  --------------------------------------------------------------------------------
  -- Function to get Current Open Period
  --------------------------------------------------------------------------------
  FUNCTION get_curr_period RETURN VARCHAR2 IS
    l_period_name VARCHAR2(15);
  BEGIN
    SELECT gps.period_name
      INTO l_period_name
      FROM gl_period_statuses gps
          ,hr_operating_units hou
          ,fnd_application    fa
     WHERE 1 = 1
       AND SYSDATE BETWEEN gps.start_date AND gps.end_date
       AND hou.organization_id = l_org_id
       AND hou.set_of_books_id = gps.set_of_books_id
       AND fa.application_short_name LIKE 'SQLGL'
       AND fa.application_id = gps.application_id
       AND gps.closing_status = 'O';
  
    RETURN l_period_name;
  EXCEPTION
    WHEN OTHERS THEN
      RETURN NULL;
  END;

  --------------------------------------------------------------------------------
  -- Function to derive Invoice Amount Remaining
  --------------------------------------------------------------------------------
  FUNCTION inv_amt_remaining(p_invoice_id IN NUMBER) RETURN NUMBER IS
    l_amount_remaining NUMBER;
  BEGIN
    SELECT SUM(ps.amount_remaining) amount_remaining
      INTO l_amount_remaining
      FROM  apps.ap_payment_schedules ps 
     WHERE 1 = 1
       AND ps.invoice_id = p_invoice_id
     GROUP BY ps.invoice_id;
  
    RETURN l_amount_remaining;
  
  EXCEPTION
    WHEN OTHERS THEN
      RETURN 0;
  END;

  --------------------------------------------------------------------------------
  -- Function to derive Minimum Invoice Date
  --------------------------------------------------------------------------------
  FUNCTION inv_min_due_date(p_invoice_id IN NUMBER) RETURN VARCHAR2 IS
    l_due_date VARCHAR2(10) := '01/01/0001';
  BEGIN
    SELECT to_char(MIN(ps.due_date), 'MM/DD/YYYY')
      INTO l_due_date
       FROM  apps.ap_payment_schedules ps 
     WHERE 1 = 1
       AND ps.invoice_id = p_invoice_id
     GROUP BY ps.invoice_id;
  
    RETURN l_due_date;
  
  EXCEPTION
    WHEN OTHERS THEN
      RETURN l_due_date;
  END;

  --------------------------------------------------------------------------------
  -- Function to derive Invoice Payment Date
  --------------------------------------------------------------------------------
  FUNCTION inv_payment_date(p_invoice_id IN NUMBER) RETURN VARCHAR2 IS
    l_payment_date VARCHAR2(10) := '01/01/0001';
  BEGIN
    SELECT to_char(MIN(b.creation_date), 'MM/DD/YYYY')
      INTO l_payment_date
      FROM  apps.ap_invoice_payments b        
			,apps.ap_checks    ac             
     WHERE b.invoice_id = p_invoice_id
       AND b.check_id = ac.check_id
       AND ac.status_lookup_code NOT IN ('VOIDED', 'OVERFLOW')
    --AND i.invoice_id = '5l_org_id9203'
     GROUP BY b.invoice_id;
  
    RETURN l_payment_date;
  
  EXCEPTION
    WHEN OTHERS THEN
      RETURN l_payment_date;
  END;

  --------------------------------------------------------------------------------
  -- Function to derive appended and pipe delimited value of
  -- InvoiceMerchendiseAmount, FreightAmount and TaxAmount
  --------------------------------------------------------------------------------
  FUNCTION disc_mrc_frt_amnt(p_invoice_id IN NUMBER) RETURN VARCHAR2 IS
    l_disc_mrc_frt_amnt VARCHAR2(200);
  BEGIN
    SELECT SUM(CASE
                 WHEN pd.line_type_lookup_code IN ('ITEM', 'MISCELLANEOUS') THEN
                  pd.amount
               END) || ' | ' || SUM(CASE
                                      WHEN pd.line_type_lookup_code = 'FREIGHT' THEN
                                       pd.amount
                                    END) || ' | ' ||
           SUM(CASE
                 WHEN pd.line_type_lookup_code = 'TAX' THEN
                  pd.amount
               END) || ' | ' || ''
      INTO l_disc_mrc_frt_amnt
      FROM apps.ap_invoice_distributions pd   
	  WHERE pd.invoice_id = p_invoice_id
     GROUP BY pd.invoice_id;
  
    RETURN l_disc_mrc_frt_amnt;
  
  EXCEPTION
    WHEN OTHERS THEN
      RETURN NULL;
  END disc_mrc_frt_amnt;

  --------------------------------------------------------------------------------
  -- Procedure to Payment Terms File
  --------------------------------------------------------------------------------
  -- Version   Date             Developer        Description                    
  -- -------   -----            ----------       ---------------                
  --  1.1      10-Apr-2013      Luong Vu         Change percentage to decimal   
  --                                             Incident# 198070               
  --                                             RFC 36566                      
  --  1.4      03/12/2015       Raghavendra S    TMS# 20150303-00075 - FIN - Addition of Code Snippet for Line Breaks in All EDW Extract Programs
  --                                             and Views for all free entry text fields.
  --------------------------------------------------------------------------------  

  PROCEDURE CREATE_PAYMENT_TERMS_FILE(errbuf  OUT VARCHAR2
                                     ,retcode OUT NUMBER) IS
  
    ----------------------------------------------------------------------
    -- Cursor to derive Payment Term Details
    ----------------------------------------------------------------------
    CURSOR cur_payment_terms IS
      SELECT 'WHITECAP' || ' | ' || -- "BUSINESS_UNIT"
              'ORACLE EBS' || ' | ' || -- "SOURCE SYSTEMS"
              apt.term_id || ' | ' || -- "TERM ID"
              -- apt.description commented for V 1.4
              REGEXP_REPLACE(apt.description,'[[:cntrl:]]', ' ')|| ' | ' || -- "TERM DESCRIPTION" -- Added for V 1.4
              (aptl.discount_percent / 100) || ' | ' || -- "PERCENT DISCOUNT"
              aptl.discount_days || ' | ' || -- "DISCOUNT DAYS"
              aptl.due_days || ' | ' || -- "NET DAYS"
             --         aptl.discount_day_of_month  || ' | ' ||   -- "PROXY TERM"
             --         aptl.due_day_of_month       || ' | ' ||   -- "PROXY DAY"
              decode(aptl.discount_day_of_month, NULL, '', 'Y') || ' | ' || -- "PROXY FLAG"
              aptl.discount_day_of_month || ' | ' || -- "PROXY DAY"
             --         aptl.due_day_of_month       || ' | ' ||   -- "PROXY DAY"
              aptl.due_months_forward -- "PROXY MONTH"
              "PAYMENT_TERM_DTLS"
        FROM ap.ap_terms_tl    apt
            ,ap.ap_terms_lines aptl
       WHERE apt.term_id = aptl.term_id;
  
    --File Variables
    l_file_name      VARCHAR2(150);
    l_file_name_temp VARCHAR2(150);
    l_file_handle    utl_file.file_type;
    l_procedure_name VARCHAR2(75) := 'xxwc_ap_ind_srcg_pkg.create_payment_terms_file';
  
  BEGIN
    fnd_file.put_line(fnd_file.log,
                      '**********************************************************************************');
    fnd_file.put_line(fnd_file.log,
                      '----------------------------------------------------------------------------------');
    fnd_file.put_line(fnd_file.log,
                      'Start of Procedure : CREATE_PAYMENT_TERMS_FILE');
    fnd_file.put_line(fnd_file.log,
                      '----------------------------------------------------------------------------------');
  
    l_file_name := 'WHITECAP_PAYMENTTERMS_' || to_char(SYSDATE, 'YYYYMMDD') ||
                   '.ld';
  
    l_file_name_temp := 'TEMP_' || l_file_name;
  
    -- Start creating the Payment Terms file
    l_file_handle := utl_file.fopen('XXWC_EBS_TO_EDW_MONTHLY_OB_DIR',
                                    l_file_name_temp, 'w');
  
    FOR rec_payment_terms IN cur_payment_terms
    LOOP
      -- Write Payment Terms info into file
      utl_file.put_line(l_file_handle, rec_payment_terms.payment_term_dtls);
    END LOOP;
  
    -- Close the file
    utl_file.fclose(l_file_handle);
  
    -- 'Rename file for pickup';
    utl_file.frename('XXWC_EBS_TO_EDW_MONTHLY_OB_DIR', l_file_name_temp,
                     'XXWC_EBS_TO_EDW_MONTHLY_OB_DIR', l_file_name);
  
    fnd_file.put_line(fnd_file.log,
                      '----------------------------------------------------------------------------------');
    fnd_file.put_line(fnd_file.log,
                      'End of Procedure : CREATE_PAYMENT_TERMS_FILE');
    fnd_file.put_line(fnd_file.log,
                      '----------------------------------------------------------------------------------');
    fnd_file.put_line(fnd_file.log,
                      '**********************************************************************************');
  
  EXCEPTION
    WHEN OTHERS THEN
      errbuf := 'Error in procedure - CREATE_PAYMENT_TERMS_FILE. Error - ' ||
                SQLERRM;
    
      xxcus_error_pkg.xxcus_error_main_api(p_called_from => l_calling_proc_name,
                                           p_calling => l_procedure_name,
                                           p_request_id => fnd_global.conc_request_id,
                                           p_ora_error_msg => SQLERRM,
                                           p_error_desc => errbuf,
                                           p_distribution_list => pl_dflt_email,
                                           p_module => 'AP');
  END create_payment_terms_file;

  /********************************************************************************
  PROCEDURE NAME: create_pay_to_vendor_file
  
  PURPOSE: Generate Pay To Vendor File
  
  HISTORY
  ===============================================================================
  VERSION DATE          AUTHOR(S)       DESCRIPTION
  ------- -----------   --------------- -----------------------------------------
  1.0     10/20/2011    Gopi Damuluri    Initial creation of the procedure
  1.2     06/24/2014    Gopi Damuluri    TMS#  20140624-00015 
                                         To update TAX_ID column in PAY_TO_VENDOR 
                                         extract for both CRP and WC feed
  1.4     03/12/2015    Raghavendra S    TMS# 20150303-00075 - FIN - Addition of Code Snippet for Line Breaks in All EDW Extract Programs
                                         and Views for all free entry text fields.
  ********************************************************************************/
  --------------------------------------------------------------------------------
  -- Procedure to create Pay To Vendor File
  --------------------------------------------------------------------------------
  PROCEDURE create_pay_to_vendor_file(errbuf  OUT VARCHAR2
                                     ,retcode OUT NUMBER) IS
  
    ----------------------------------------------------------------------
    -- Cursor to derive Pay To Vendor Details
    ----------------------------------------------------------------------
    CURSOR cur_pay_to_vendor IS
      SELECT 'WHITECAP' || ' | ' || -- "BUSINESS_UNIT"
             'ORACLE EBS' || ' | ' || --"SOURCE SYSTEMS"
             CASE povs.pay_group_lookup_code
               WHEN 'AP SUPPLIER' THEN
                ''
               WHEN 'ELECTRICAL' THEN
                ''
               WHEN 'PLANS SPECS' THEN
                ''
               WHEN 'FINAL APPROVED' THEN
                ''
               WHEN 'PLUMBING' THEN
                ''
               WHEN 'HDS INTERC' THEN
                ''
               WHEN 'LOCAL CHECKS' THEN
                ''
               WHEN 'INTERCOMPANY INVOICE' THEN
                ''
               WHEN 'AP WIRE TRANSFER' THEN
                ''
               WHEN 'RENT' THEN
                ''
               WHEN 'IWO' THEN
                ''
               WHEN 'HDSOOP' THEN
                ''
               WHEN 'ZERO CHECK' THEN
                ''
               WHEN 'HDS INTERCO' THEN
                ''
               WHEN 'REBATE' THEN
                ''
               WHEN 'UTILITIES' THEN
                ''
               WHEN 'TERM EMPLOYEE' THEN
                ''
               WHEN 'UTILITY' THEN
                ''
               WHEN 'VENDOR' THEN
                ''
               WHEN 'EXPENSE' THEN
                ''
               WHEN 'REAL ESTATE' THEN
                ''
               WHEN 'TAX AUTHORITY' THEN
                ''
               WHEN 'CUSTOMER REFUND' THEN
                ''
               WHEN 'IWO' THEN
                ''
               WHEN 'TAX' THEN
                ''
               WHEN 'EMPLOYEE' THEN
                'EMPLOYEE'
               WHEN 'FREIGHT' THEN
                'FREIGHT'
             END || ' | ' || -- "VENDOR_TYPE" -- Added to clean up vendor type - Sbreland
             CASE
               WHEN povs.inactive_date IS NULL THEN
                'ACTIVE'
               WHEN povs.inactive_date IS NOT NULL THEN
                'INACTIVE'
             END || ' | ' || -- "VENDOR_STATUS"
             -- pov.segment1 -- commented for V 1.4
             -- pov.vendor_name -- commented  for V 1.4
             -- pov.vendor_name -- commented  for V 1.4
             -- povs.address_line1 -- commented for V 1.4
             -- povs.address_line2 -- commented for V 1.4
             -- povs.address_line3 -- commented for V 1.4
             REGEXP_REPLACE(pov.segment1,'[[:cntrl:]]', ' ') || '-' || povs.vendor_site_id || ' | ' || -- "VENDOR_SITE_ID" -- Added for V 1.4
             REGEXP_REPLACE(pov.vendor_name,'[[:cntrl:]]', ' ') || ' | ' || -- "Vendor Mailing Address Name" -- Added for V 1.4
             REGEXP_REPLACE(pov.vendor_name,'[[:cntrl:]]', ' ') || ' | ' || -- "Pay to Vendor Name" -- Added for V 1.4
             REPLACE(REPLACE(NVL(pov.num_1099, pov.individual_1099), '-', ''), '/', '') || ' | ' || -- "TAX_ID"  -- Version# 1.2
             REGEXP_REPLACE(povs.address_line1,'[[:cntrl:]]', ' ') || ' | ' || -- ADDRESS LINE 1 (Pay-to Mailing Address 1) -- Added for V 1.4
             REGEXP_REPLACE(povs.address_line2,'[[:cntrl:]]', ' ') || ' | ' || -- ADDRESS LINE 2 (Pay-to Mailing Address 2) -- Added for V 1.4
             REGEXP_REPLACE(povs.address_line3,'[[:cntrl:]]', ' ') || ' | ' || -- ADDRESS LINE 3 (Pay-to Mailing Address 3) -- Added for V 1.4
             povs.city || ' | ' || -- VENDOR CITY (Pay-to Maling City)
             povs.state || ' | ' || -- VENDOR STATE (Pay-to Maling State/Province)
             povs.zip || ' | ' || -- VENDOR ZIP CODE (Pay-to Maling Postal Code)
             decode(povs.fax_area_code, NULL, povs.fax,
                    povs.fax_area_code || '-' || substr(povs.fax, 1, 3) || '-' ||
                     substr(povs.fax, 4, 7)) || ' | ' || -- VENDOR FAX NUMBER (Pay-to Mailing Fax Number)
             povs.country || ' | ' || -- VENDOR COUNTRY (Pay-to Mailing Country)
             decode(povs.area_code, NULL, povs.phone,
                    povs.area_code || '-' || substr(povs.phone, 1, 3) || '-' ||
                     substr(povs.phone, 4, 7)) || ' | ' || -- VENDOR TELEPHONE NUMBER (Pay-to Maling Phone Number)
             '' || ' | ' || -- "Pay-To Physical Address Name"
             '' || ' | ' || -- "Pay-To Physical Address Line 1"
             '' || ' | ' || -- "Pay-To Physical Address Line 2"
             '' || ' | ' || -- "Pay-To Physical City"
             '' || ' | ' || -- "Pay-To Physical State/Province"
             '' || ' | ' || -- "Pay-To Physical Postal Code"
             '' || ' | ' || -- "Pay-To Physical Fax"
             '' || ' | ' || -- "Pay-To Physical Country"
             '' || ' | ' || -- "Pay-To Physical Phone Number"
             apt.term_id || ' | ' || -- "Payment Term ID"
             to_char(povs.creation_date, 'MM/DD/YYYY') -- "Creation Date"
             "PAY_TO_VENDOR_DTLS"
        FROM ap_suppliers          pov
          	 ,apps.ap_supplier_sites povs  
             ,ap.ap_terms_tl        apt
       WHERE pov.vendor_id = povs.vendor_id
         AND nvl(povs.terms_id, pov.terms_id) = apt.term_id
        -- AND povs.org_id = l_org_id   --25/09/2014 Commented by Veera as per the Canada OU Test
         AND povs.creation_date >= l_creation_date
         AND povs.pay_group_lookup_code != l_pay_group_lookup_code
         AND povs.pay_site_flag = 'Y'
       ORDER BY povs.vendor_site_id;
  
    --File Variables
    l_file_name      VARCHAR2(150);
    l_file_name_temp VARCHAR2(150);
    l_file_handle    utl_file.file_type;
    l_procedure_name VARCHAR2(75) := 'xxwc_ap_ind_srcg_pkg.create_pay_to_vendor_file';
  
  BEGIN
    fnd_file.put_line(fnd_file.log,
                      '**********************************************************************************');
    fnd_file.put_line(fnd_file.log,
                      '----------------------------------------------------------------------------------');
    fnd_file.put_line(fnd_file.log,
                      'Start of Procedure : CREATE_PAY_TO_VENDOR_FILE');
    fnd_file.put_line(fnd_file.log,
                      '----------------------------------------------------------------------------------');
  
    l_file_name := 'WHITECAP_PAYTOVENDOR_' || to_char(SYSDATE, 'YYYYMMDD') ||
                   '.ld';
  
    l_file_name_temp := 'TEMP_' || l_file_name;
  
    -- Start creating the Pay To Vendor file
    l_file_handle := utl_file.fopen('XXWC_EBS_TO_EDW_MONTHLY_OB_DIR',
                                    l_file_name_temp, 'w');
  
    FOR rec_pay_to_vendor IN cur_pay_to_vendor
    LOOP
      -- Write Pay To Vendor info into file
      utl_file.put_line(l_file_handle, rec_pay_to_vendor.pay_to_vendor_dtls);
    END LOOP;
  
    -- Close the file
    utl_file.fclose(l_file_handle);
  
    -- 'Rename file for pickup';
    utl_file.frename('XXWC_EBS_TO_EDW_MONTHLY_OB_DIR', l_file_name_temp,
                     'XXWC_EBS_TO_EDW_MONTHLY_OB_DIR', l_file_name);
  
    fnd_file.put_line(fnd_file.log,
                      '----------------------------------------------------------------------------------');
    fnd_file.put_line(fnd_file.log,
                      'End of Procedure : CREATE_PAY_TO_VENDOR_FILE');
    fnd_file.put_line(fnd_file.log,
                      '----------------------------------------------------------------------------------');
    fnd_file.put_line(fnd_file.log,
                      '**********************************************************************************');
  
  EXCEPTION
    WHEN OTHERS THEN
      errbuf := 'Error in procedure - CREATE_PAY_TO_VENDOR_FILE. Error - ' ||
                SQLERRM;
    
      xxcus_error_pkg.xxcus_error_main_api(p_called_from => l_calling_proc_name,
                                           p_calling => l_procedure_name,
                                           p_request_id => fnd_global.conc_request_id,
                                           p_ora_error_msg => SQLERRM,
                                           p_error_desc => errbuf,
                                           p_distribution_list => pl_dflt_email,
                                           p_module => 'AP');
  END create_pay_to_vendor_file;

  --------------------------------------------------------------------------------
  -- Procedure to Native GL To Khalix File
  --------------------------------------------------------------------------------
  PROCEDURE create_khalix_file(errbuf  OUT VARCHAR2
                              ,retcode OUT NUMBER) IS
/********************************************************************************
  PROCEDURE NAME: create_khalix_file
  
  PURPOSE: Generate kahlix File
  
  HISTORY
  ===============================================================================
  VERSION DATE          AUTHOR(S)       DESCRIPTION
  ------- -----------   --------------- -----------------------------------------
  1.4     03/12/2015    Raghavendra S    TMS# 20150303-00075 - FIN - Addition of Code Snippet for Line Breaks in All EDW Extract Programs
                                         and Views for all free entry text fields.
  ********************************************************************************/
  
    ----------------------------------------------------------------------
    -- Cursor to derive Native GL To Khalix Details
    ----------------------------------------------------------------------
    CURSOR cur_kahlix IS
      SELECT 'WHITECAP' || ' | ' || -- "BUSINESS_UNIT"
             'ORACLE EBS' || ' | ' || -- "FINANCIAL_SOURCE_SYSTEM"
             -- fvv.flex_value -- Commented for V 1.4
             -- fvv.description -- Commented for V 1.4
             -- fvv.flex_value-- Commented for V 1.4
             REGEXP_REPLACE(fvv.flex_value,'[[:cntrl:]]', ' ') || ' | ' || -- "NATIVE_GL_ACCT" -- Added for V 1.4
             REGEXP_REPLACE(fvv.description,'[[:cntrl:]]', ' ') || ' | ' || 'A' || REGEXP_REPLACE(fvv.flex_value,'[[:cntrl:]]', ' ') -- "KHALIX_ACCT" -- Added for V 1.4
             "KAHLIX_DETAILS"
        FROM apps.fnd_flex_values_vl  fvv
            ,apps.fnd_flex_value_sets fvs
       WHERE fvv.flex_value_set_id = fvs.flex_value_set_id
         AND fvs.flex_value_set_name = 'XXCUS_GL_ACCOUNT'
       ORDER BY fvv.flex_value;
  
    --File Variables
    l_file_name      VARCHAR2(150);
    l_file_name_temp VARCHAR2(150);
    l_file_handle    utl_file.file_type;
    l_procedure_name VARCHAR2(75) := 'xxwc_ap_ind_srcg_pkg.create_khalix_file';
  
  BEGIN
    fnd_file.put_line(fnd_file.log,
                      '**********************************************************************************');
    fnd_file.put_line(fnd_file.log,
                      '----------------------------------------------------------------------------------');
    fnd_file.put_line(fnd_file.log,
                      'Start of Procedure : CREATE_KHALIX_FILE');
    fnd_file.put_line(fnd_file.log,
                      '----------------------------------------------------------------------------------');
  
    l_file_name := 'WHITECAP_NATIVEGLTOKHALIX_' ||
                   to_char(SYSDATE, 'YYYYMMDD') || '.ld';
  
    fnd_file.put_line(fnd_file.log, 'l_file_name          :' || l_file_name);
  
    l_file_name_temp := 'TEMP_' || l_file_name;
  
    -- Start creating the Native GL To Khalix file
    l_file_handle := utl_file.fopen('XXWC_EBS_TO_EDW_MONTHLY_OB_DIR',
                                    l_file_name_temp, 'w');
  
    FOR rec_kahlix IN cur_kahlix
    LOOP
      -- Write Native GL To Khalix info into file
      utl_file.put_line(l_file_handle, rec_kahlix.kahlix_details);
    END LOOP;
  
    -- Close the file
    utl_file.fclose(l_file_handle);
  
    -- 'Rename file for pickup';
    utl_file.frename('XXWC_EBS_TO_EDW_MONTHLY_OB_DIR', l_file_name_temp,
                     'XXWC_EBS_TO_EDW_MONTHLY_OB_DIR', l_file_name);
  
    fnd_file.put_line(fnd_file.log,
                      '----------------------------------------------------------------------------------');
    fnd_file.put_line(fnd_file.log, 'End of Procedure : CREATE_KHALIX_FILE');
    fnd_file.put_line(fnd_file.log,
                      '----------------------------------------------------------------------------------');
    fnd_file.put_line(fnd_file.log,
                      '**********************************************************************************');
  
  EXCEPTION
    WHEN OTHERS THEN
      errbuf := 'Error in procedure - CREATE_KHALIX_FILE. Error - ' ||
                SQLERRM;
    
      xxcus_error_pkg.xxcus_error_main_api(p_called_from => l_calling_proc_name,
                                           p_calling => l_procedure_name,
                                           p_request_id => fnd_global.conc_request_id,
                                           p_ora_error_msg => SQLERRM,
                                           p_error_desc => errbuf,
                                           p_distribution_list => pl_dflt_email,
                                           p_module => 'AP');
    
  END create_khalix_file;

  PROCEDURE create_payables_file(errbuf        OUT VARCHAR2
                                ,retcode       OUT NUMBER
                                ,p_period_name IN VARCHAR2) IS
    ----------------------------------------------------------------------
    -- Cursor to derive Payable Invoice Header details
    ----------------------------------------------------------------------
/********************************************************************************
  PROCEDURE NAME: create_payables_file
  
  PURPOSE: Generate Payables File
  
  HISTORY
  ===============================================================================
  VERSION DATE          AUTHOR(S)       DESCRIPTION
  ------- -----------   --------------- -----------------------------------------
  1.4     03/12/2015    Raghavendra S    TMS# 20150303-00075 - FIN - Addition of Code Snippet for Line Breaks in All EDW Extract Programs
                                         and Views for all free entry text fields.
  ********************************************************************************/
    CURSOR cur_ap_inv_hdr(p_period_name IN VARCHAR2) IS
      SELECT 'WHITECAP' || ' | ' || -- "BUSINESS_UNIT"
             'ORACLE EBS' || ' | ' || -- "FINANCIAL SOURCE_SYSTEM"
             i.invoice_id || ' | ' || -- "Document Number"
             v.segment1 || '-' || vs.vendor_site_id || ' | ' ||
             i.invoice_num || ' | ' || -- "Vendor Invoice Number"
             decode(nvl(v.vendor_type_lookup_code, 'E'), 'VENDOR', 'I', 'E') ||
             ' | ' || -- "Invoice Type Code"
             to_char(i.invoice_date, 'MM/DD/YYYY') || ' | ' || -- "Vendor Invoice Date"
             to_char(i.creation_date, 'MM/DD/YYYY') || ' | ' || -- "Vendor Invoice Entered Date"
             to_char(i.last_update_date, 'MM/DD/YYYY') || ' | ' || -- "Invoice Last Updated Date"
             CASE
               WHEN i.cancelled_date IS NOT NULL THEN
                'CANCELLED'
               WHEN abs(i.amount_paid) > 0
                    AND (i.invoice_amount - i.amount_paid) != 0 THEN
                'PARTIAL PAID'
               WHEN i.payment_status_flag = 'Y' THEN
                'PAID'
               WHEN i.payment_status_flag = 'N' THEN
                'NOT PAID'
               WHEN xxwc_ap_ind_srcg_pkg.inv_amt_remaining(i.invoice_id) = 0 THEN
                'PAID'
             END || ' | ' || -- "Vendor Invoice Status"
             apt.term_id || ' | ' || -- "Payment Term ID"
             decode(xxwc_ap_ind_srcg_pkg.inv_min_due_date(i.invoice_id),
                    '01010001', '',
                    xxwc_ap_ind_srcg_pkg.inv_min_due_date(i.invoice_id)) ||
             ' | ' || -- "Payment Due Date"
             decode(xxwc_ap_ind_srcg_pkg.inv_payment_date(i.invoice_id),
                    '01010001', '',
                    xxwc_ap_ind_srcg_pkg.inv_payment_date(i.invoice_id)) ||
             ' | ' || -- "Payment Date"
             i.amount_paid || ' | ' || --"Payment Amount"
             i.discount_amount_taken || ' | ' || -- "Cash Discount Amount"
             xxwc_ap_ind_srcg_pkg.disc_mrc_frt_amnt(i.invoice_id) || ' | ' || -- "Inv_Misc_Disc_Chg_Adj"
             i.invoice_amount -- "Net Invoice Total"
             invoice_header
        FROM apps.ap_invoices i          
            ,ap_suppliers          v
        	,apps.ap_supplier_sites vs   
            ,ap.ap_terms_tl        apt
       WHERE 1 = 1
         AND i.vendor_id = v.vendor_id
         AND v.vendor_id = vs.vendor_id
         AND i.vendor_site_id = vs.vendor_site_id
         AND i.terms_id = apt.term_id
         AND i.org_id = l_org_id
         AND nvl(i.attribute10, 'N') = 'N'
         AND vs.pay_group_lookup_code != l_pay_group_lookup_code
         AND i.invoice_id IN
             (SELECT pd.invoice_id
              FROM apps.ap_invoice_distributions pd       
               WHERE pd.invoice_id = i.invoice_id
                 AND ((p_period_name IS NULL AND
                     pd.accounting_date BETWEEN l_start_date AND
                     l_end_date) OR p_period_name = pd.period_name));
  
    ----------------------------------------------------------------------
    -- Cursor to derive Payable Invoice Header Totals
    ----------------------------------------------------------------------
    CURSOR cur_ap_inv_hdr_ttl(p_period_name IN VARCHAR2) IS
      SELECT SUM(nvl(i.invoice_amount, 0)) "INVOICE_AMOUNT"
             ,SUM(nvl(i.amount_paid, 0)) "AMOUNT_PAID"
        FROM apps.ap_invoices i    
            ,ap_suppliers          v
            ,apps.ap_supplier_sites vs  
            ,ap.ap_terms_tl        apt
       WHERE 1 = 1
         AND i.vendor_id = v.vendor_id
         AND v.vendor_id = vs.vendor_id
         AND i.vendor_site_id = vs.vendor_site_id
         AND i.terms_id = apt.term_id
         AND i.org_id = l_org_id
         AND nvl(i.attribute10, 'N') = 'N'
         AND vs.pay_group_lookup_code != l_pay_group_lookup_code
         AND i.invoice_id IN
             (SELECT pd.invoice_id
			   FROM apps.ap_invoice_distributions pd    
               WHERE pd.invoice_id = i.invoice_id
                 AND ((p_period_name IS NULL AND
                     pd.accounting_date BETWEEN l_start_date AND
                     l_end_date) OR p_period_name = pd.period_name));
  
    ----------------------------------------------------------------------
    -- Cursor to derive Payable Invoice Line and Distribution details
    ----------------------------------------------------------------------
    CURSOR cur_ap_inv_dtl(p_period_name IN VARCHAR2) IS
      SELECT 'WHITECAP' || ' | ' || -- "BUSINESS_UNIT"
             'ORACLE EBS' || ' | ' || -- "FINANCIAL SOURCE_SYSTEM"
             i.invoice_id || ' | ' || -- "DOCUMENT_NUMBER"
             pd.invoice_line_number ||
             lpad(pd.distribution_line_number, 4, '0') || ' | ' || -- "INVOICE_LINE_NUMBER"
             'E' || ' | ' || -- "INVOICE_TYPE"
             to_char(pd.accounting_date, 'MM/DD/YYYY') || ' | ' || -- "GL_DATE"
             CASE pd.line_type_lookup_code
               WHEN 'MISCELLANEOUS' THEN
                'M'
               WHEN 'ITEM' THEN
                'I'
               WHEN 'FREIGHT' THEN
                'F'
               WHEN 'TAX' THEN
                'T'
             END || ' | ' || -- "INVOICE_LINE_TYPE"
             pd.amount || ' | ' || -- "INVOICE_LINE_AMOUNT"
             to_char(i.last_update_date, 'MM/DD/YYYY') || ' | ' || -- "LAST_UPDT_DT"
             cc.segment4 || ' | ' || -- "ACCOUNT"
             -- substr(i.description, 1, 50) -- commented for V 1.4
             REGEXP_REPLACE(substr(i.description, 1, 50),'[[:cntrl:]]', ' ') || ' | ' || -- "PO NUMBER" -- Added for V 1.4
             loc.lob_branch -- "LOCATION"
             invoice_details
        FROM apps.ap_invoices   i                      
		    ,apps.ap_supplier_sites vs             		   
			,apps.ap_invoice_distributions   pd    
            ,gl.gl_code_combinations         cc
--            ,apps.fnd_lookup_values_vl       flv                    -- Version# 1.1
            ,xxcus.xxcus_location_code_tbl   loc   
       WHERE i.invoice_id = pd.invoice_id
         AND pd.dist_code_combination_id = cc.code_combination_id
         AND i.vendor_site_id = vs.vendor_site_id
         AND i.org_id = l_org_id
         AND vs.pay_group_lookup_code != l_pay_group_lookup_code
         AND nvl(i.attribute10, 'N') = 'N'
--         AND cc.segment1 = flv.lookup_code                          -- Version# 1.1
--         AND flv.lookup_type = l_lookup_type                        -- Version# 1.1
         AND loc.business_unit = 'WC1US'
         AND loc.entrp_loc = cc.segment2
            --      and pd.period_name              = NVL(p_period_name, pd.period_name)
            --      AND (p_period_name IS NOT NULL OR pd.accounting_date BETWEEN l_start_date AND l_end_date)
         AND EXISTS (SELECT pd_s.invoice_id
                FROM apps.ap_invoice_distributions pd_s         
               WHERE pd_s.invoice_id = i.invoice_id
                 AND ((p_period_name IS NULL AND pd_s.accounting_date BETWEEN
                     l_start_date AND l_end_date) OR
                     p_period_name = pd_s.period_name))
       ORDER BY vs.vendor_site_id DESC;
  
    ----------------------------------------------------------------------
    -- Cursor to derive Payable Invoice Line and Distribution details
    ----------------------------------------------------------------------
    CURSOR cur_ap_inv_dtl_ttl(p_period_name IN VARCHAR2) IS
      SELECT pd.period_name "PERIOD"
             ,cc.segment4 "GL_ACCOUNT"
             ,SUM(nvl(pd.amount, 0)) "INVOICE_LINE_AMOUNT"
        FROM apps.ap_invoices   i                       
		    ,apps.ap_supplier_sites vs             		   
			,apps.ap_invoice_distributions   pd    
            ,gl.gl_code_combinations         cc
--            ,apps.fnd_lookup_values_vl       flv          -- Version# 1.1
       WHERE i.invoice_id = pd.invoice_id
         AND pd.dist_code_combination_id = cc.code_combination_id
         AND i.vendor_site_id = vs.vendor_site_id
         AND i.org_id = l_org_id
         AND vs.pay_group_lookup_code != l_pay_group_lookup_code
         AND nvl(i.attribute10, 'N') = 'N'
--         AND cc.segment1 = flv.lookup_code                -- Version# 1.1
--         AND flv.lookup_type = l_lookup_type              -- Version# 1.1
            --      and pd.period_name              = NVL(p_period_name, pd.period_name)
            --      AND (p_period_name IS NOT NULL OR pd.accounting_date BETWEEN l_start_date AND l_end_date)
         AND EXISTS (SELECT pd_s.invoice_id
                FROM apps.ap_invoice_distributions   pd_s      
               WHERE pd_s.invoice_id = i.invoice_id
                 AND ((p_period_name IS NULL AND pd_s.accounting_date BETWEEN
                     l_start_date AND l_end_date) OR
                     p_period_name = pd_s.period_name))
       GROUP BY cc.segment4
               ,pd.period_name
       ORDER BY pd.period_name;
  
    -- Intialize Variables
    l_err_msg          VARCHAR2(2000);
    l_err_code         NUMBER;
    l_sec              VARCHAR2(150);
    l_procedure_name   VARCHAR2(75) := 'xxwc_ap_ind_srcg_pkg.create_payables_file';
    l_from_date        DATE;
    l_to_date          DATE;
    l_inv_header_total NUMBER;
    l_inv_paid_total   NUMBER;
    l_inv_line_total   NUMBER;
    l_gl_account       VARCHAR2(30);
    l_period           VARCHAR2(10);
    l_errbuf           VARCHAR2(2000);
    l_retcode          NUMBER;
  
    --File Variables
    l_file_name_hdr      VARCHAR2(150);
    l_file_name_dtl      VARCHAR2(150);
    l_file_dir           VARCHAR2(100);
    l_file_handle_hdr    utl_file.file_type;
    l_file_handle_dtl    utl_file.file_type;
    l_file_name_hdr_temp VARCHAR2(150);
    l_file_name_dtl_temp VARCHAR2(150);
  
  BEGIN

 mo_global.set_policy_context('S',162);
  
    fnd_file.put_line(fnd_file.log,
                      '**********************************************************************************');
    fnd_file.put_line(fnd_file.log,
                      '----------------------------------------------------------------------------------');
    fnd_file.put_line(fnd_file.log,
                      'Start of Procedure : CREATE_PAYABLES_FILE');
    fnd_file.put_line(fnd_file.log,
                      '----------------------------------------------------------------------------------');
    fnd_file.put_line(fnd_file.log, 'Input Parameters   :');
    fnd_file.put_line(fnd_file.log,
                      'p_period_name          :' || p_period_name);
    fnd_file.put_line(fnd_file.log,
                      '----------------------------------------------------------------------------------');
  
    l_file_name_hdr := 'WHITECAP_APINVOICEHEADER_' ||
                       to_char(SYSDATE, 'YYYYMMDD') || '.ld';
    l_file_name_dtl := 'WHITECAP_APINVOICELINE_' ||
                       to_char(SYSDATE, 'YYYYMMDD') || '.ld';
  
    l_file_name_hdr_temp := 'TEMP_' || l_file_name_hdr;
    l_file_name_dtl_temp := 'TEMP_' || l_file_name_dtl;
  
    -- Delete the Temp and Data Files before generating the new files - have a flag(Free Text) for this
  
    -->Header Start
    -- Start creating the Invoice Header file
    l_file_handle_hdr := utl_file.fopen('XXWC_EBS_TO_EDW_MONTHLY_OB_DIR',
                                        l_file_name_hdr_temp, 'w');
  
    FOR rec_ap_inv_hdr IN cur_ap_inv_hdr(p_period_name)
    LOOP
    
      -- Write header info
      utl_file.put_line(l_file_handle_hdr, rec_ap_inv_hdr.invoice_header);
    END LOOP;
  
    -- Close the file
    utl_file.fclose(l_file_handle_hdr);
  
    -- 'Rename file for pickup';
    utl_file.frename('XXWC_EBS_TO_EDW_MONTHLY_OB_DIR', l_file_name_hdr_temp,
                     'XXWC_EBS_TO_EDW_MONTHLY_OB_DIR', l_file_name_hdr);
  
    -->Details Start
    -- Start creating the Invoice Details file
    l_file_handle_dtl := utl_file.fopen('XXWC_EBS_TO_EDW_MONTHLY_OB_DIR',
                                        l_file_name_dtl_temp, 'w');
  
    FOR rec_ap_inv_dtl IN cur_ap_inv_dtl(p_period_name)
    LOOP
    
      -- Write line info
      utl_file.put_line(l_file_handle_dtl, rec_ap_inv_dtl.invoice_details);
    END LOOP;
  
    -- Close the file
    utl_file.fclose(l_file_handle_dtl);
  
    -- 'Rename file for pickup';
    utl_file.frename('XXWC_EBS_TO_EDW_MONTHLY_OB_DIR', l_file_name_dtl_temp,
                     'XXWC_EBS_TO_EDW_MONTHLY_OB_DIR', l_file_name_dtl);
  
    --> Header Total
    FOR rec_ap_inv_hdr_ttl IN cur_ap_inv_hdr_ttl(p_period_name)
    LOOP
      l_inv_paid_total   := rec_ap_inv_hdr_ttl.amount_paid;
      l_inv_header_total := rec_ap_inv_hdr_ttl.invoice_amount;
    END LOOP;
  
    fnd_file.put_line(fnd_file.output,
                      '**********************************************************************************');
    fnd_file.put_line(fnd_file.output,
                      '----------------------------------------------------------------------------------');
    fnd_file.put_line(fnd_file.output, '*** Input Parameters ***');
    fnd_file.put_line(fnd_file.output,
                      'Period Name                         :' ||
                       p_period_name);
    fnd_file.put_line(fnd_file.output,
                      '----------------------------------------------------------------------------------');
    fnd_file.put_line(fnd_file.output,
                      '----------------------------------------------------------------------------------');
    fnd_file.put_line(fnd_file.output,
                      '*** Invoice Header Output Totals ***');
    fnd_file.put_line(fnd_file.output,
                      'Total Inovice Amount                :' ||
                       l_inv_header_total);
    fnd_file.put_line(fnd_file.output,
                      'Total Paid Amount Total             :' ||
                       l_inv_paid_total);
    fnd_file.put_line(fnd_file.output,
                      '----------------------------------------------------------------------------------');
    fnd_file.put_line(fnd_file.output,
                      '----------------------------------------------------------------------------------');
    fnd_file.put_line(fnd_file.output,
                      '*** Distribution Totals Per Account ***');
    fnd_file.put_line(fnd_file.output,
                      '----------------------------------------------------------------------------------');
    fnd_file.put_line(fnd_file.output,
                      'Period              GL Account                    Invoice Total');
    fnd_file.put_line(fnd_file.output,
                      '----------------------------------------------------------------------------------');
  
    --> Line Total
    FOR rec_ap_inv_dtl_ttl IN cur_ap_inv_dtl_ttl(p_period_name)
    LOOP
      l_period         := rec_ap_inv_dtl_ttl.period;
      l_gl_account     := rec_ap_inv_dtl_ttl.gl_account;
      l_inv_line_total := rec_ap_inv_dtl_ttl.invoice_line_amount;
      fnd_file.put_line(fnd_file.output,
                        rpad(l_period, 20, ' ') ||
                         rpad(l_gl_account, 30, ' ') || l_inv_line_total);
    END LOOP;
  
    COMMIT;
  
    fnd_file.put_line(fnd_file.output,
                      '----------------------------------------------------------------------------------');
    fnd_file.put_line(fnd_file.output,
                      '**********************************************************************************');
  
    fnd_file.put_line(fnd_file.log,
                      '----------------------------------------------------------------------------------');
    fnd_file.put_line(fnd_file.log,
                      'End of Procedure : CREATE_PAYABLES_FILE');
    fnd_file.put_line(fnd_file.log,
                      '----------------------------------------------------------------------------------');
  
    --> Create Payment Terms File
    create_payment_terms_file(l_errbuf, l_retcode);
  
    --> Create Pay To Vendor File
    create_pay_to_vendor_file(l_errbuf, l_retcode);
  
    --> Create Khalix File
    create_khalix_file(l_errbuf, l_retcode);
  
  EXCEPTION
    -- this section traps my errors
    WHEN OTHERS THEN
      l_err_code := 2;
      l_err_msg  := l_sec || l_err_msg || ' ERROR ' || SQLCODE ||
                    substr(SQLERRM, 1, 2000);
    
      fnd_file.put_line(fnd_file.log, l_err_msg);
      fnd_file.put_line(fnd_file.output, l_err_msg);
    
      utl_file.fclose(l_file_handle_hdr);
      utl_file.fremove(l_file_dir, l_file_name_hdr);
    
      utl_file.fclose(l_file_handle_dtl);
      utl_file.fremove(l_file_dir, l_file_name_dtl);
    
      xxcus_error_pkg.xxcus_error_main_api(p_called_from => l_procedure_name,
                                           p_calling => l_sec,
                                           p_request_id => fnd_global.conc_request_id,
                                           p_ora_error_msg => SQLERRM,
                                           p_error_desc => 'Error running Direct Sourcing package with Program Error Exception',
                                           p_distribution_list => pl_dflt_email,
                                           p_module => 'AP');
  END create_payables_file;

  /********************************************************************************
  ProcedureName : UC4_CALL
  Purpose       : API which is called from UC4 job. It submits the Concurrent
                  Program - "XXWC Prism To EBS AR Invoice Interface"
  
  HISTORY
  ===============================================================================
  VERSION DATE          AUTHOR(S)       DESCRIPTION
  ------- -----------   --------------- -----------------------------------------
  1.0     12/13/2011    Gopi Damuluri    Initial version.
  ********************************************************************************/
  PROCEDURE uc4_call(p_errbuf              OUT VARCHAR2
                    ,p_retcode             OUT NUMBER
                    ,p_conc_prg_name       IN VARCHAR2
                    ,p_conc_prg_arg1       IN VARCHAR2
                    ,p_user_name           IN VARCHAR2
                    ,p_responsibility_name IN VARCHAR2
                    ,p_org_name            IN VARCHAR2) IS
  
    --
    -- Package Variables
    --
    l_package VARCHAR2(50) := 'XXWC_AP_IND_SRCG_PKG';
    l_email   fnd_user.email_address%TYPE;
  
    l_req_id              NUMBER NULL;
    v_phase               VARCHAR2(50);
    v_status              VARCHAR2(50);
    v_dev_status          VARCHAR2(50);
    v_dev_phase           VARCHAR2(50);
    v_message             VARCHAR2(250);
    v_error_message       VARCHAR2(3000);
    v_supplier_id         NUMBER;
    v_rec_cnt             NUMBER := 0;
    l_message             VARCHAR2(150);
    l_errormessage        VARCHAR2(3000);
    pl_errorstatus        NUMBER;
    l_can_submit_request  BOOLEAN := TRUE;
    l_globalset           VARCHAR2(100);
    l_err_msg             VARCHAR2(3000);
    l_err_code            NUMBER;
    l_sec                 VARCHAR2(255);
    l_statement           VARCHAR2(9000);
    l_user                fnd_user.user_id%TYPE;
    l_user_id             NUMBER;
    l_responsibility_id   NUMBER;
    l_resp_application_id NUMBER;
    l_rej_rec_cnt         NUMBER;
    l_totl_rec_cnt        NUMBER;
    l_rej_percent         NUMBER;
    l_prc_inv_amt         NUMBER;
    l_prc_tax_amt         NUMBER;
    l_prc_frt_amt         NUMBER;
    l_rej_inv_amt         NUMBER;
    l_rej_tax_amt         NUMBER;
    l_rej_frt_amt         NUMBER;
    l_totl_inv_amt        NUMBER;
    l_totl_tax_amt        NUMBER;
    l_totl_frt_amt        NUMBER;
    l_conc_prg_arg1       VARCHAR2(900);
  
    -- Error DEBUG
    l_err_callfrom  VARCHAR2(75) DEFAULT 'XXWC_AP_IND_SRCG_PKG.UC4_CALL';
    l_err_callpoint VARCHAR2(75) DEFAULT 'START';
  BEGIN
    p_retcode := 0;
    --------------------------------------------------------------------------
    -- Deriving UserId
    --------------------------------------------------------------------------
    BEGIN
      SELECT user_id
        INTO l_user_id
        FROM fnd_user
       WHERE 1 = 1
         AND user_name = upper(p_user_name)
         AND SYSDATE BETWEEN start_date AND
             nvl(end_date, trunc(SYSDATE) + 1);
    EXCEPTION
      WHEN no_data_found THEN
        l_err_msg := 'UserName - ' || p_user_name ||
                     ' not defined in Oracle';
        RAISE program_error;
      WHEN OTHERS THEN
        l_err_msg := 'Error deriving user_id for UserName - ' ||
                     p_user_name;
        RAISE program_error;
    END;
  
    --------------------------------------------------------------------------
    -- Deriving ResponsibilityId and ResponsibilityApplicationId
    --------------------------------------------------------------------------
    BEGIN
      SELECT responsibility_id
            ,application_id
        INTO l_responsibility_id
            ,l_resp_application_id
        FROM fnd_responsibility_vl
       WHERE responsibility_name = p_responsibility_name
         AND SYSDATE BETWEEN start_date AND
             nvl(end_date, trunc(SYSDATE) + 1);
    EXCEPTION
      WHEN no_data_found THEN
        l_err_msg := 'Responsibility - ' || p_responsibility_name ||
                     ' not defined in Oracle';
        RAISE program_error;
      WHEN OTHERS THEN
        l_err_msg := 'Error deriving Responsibility_id for ResponsibilityName - ' ||
                     p_responsibility_name;
        RAISE program_error;
    END;
    
	-- 25/09/2014 Added by Veera as per the Canada OU Test --Start
	BEGIN  
      SELECT organization_id 
      INTO   l_org_id
      FROM   apps.hr_operating_units 
      WHERE   name =p_org_name;
      EXCEPTION
         WHEN NO_DATA_FOUND
         THEN
            l_sec :=
                  'Operating unit '
               || p_org_name
               || ' is invalid in Oracle';
            RAISE PROGRAM_ERROR;
         WHEN OTHERS
         THEN
            l_sec  :=
                  'Other Operating unit validation error for '
               || p_org_name
               || '. Error: '
               || SUBSTR (SQLERRM, 1, 250);
            RAISE PROGRAM_ERROR;
    END;	 
    
	-- 25/09/2014 Added by Veera as per the Canada OU Test  --Ends 

	 
    fnd_file.put_line(fnd_file.log,
                      'Concurrent Program Name - ' || p_conc_prg_name);
    l_sec           := 'UC4 call to run concurrent request - XXWC AP Indirect Sourcing Data Outbound Interface';
    l_conc_prg_arg1 := get_curr_period;
  
    --------------------------------------------------------------------------
    -- Apps Initialize
    --------------------------------------------------------------------------
    fnd_global.apps_initialize(l_user_id, l_responsibility_id,
                               l_resp_application_id);
							   
    mo_global.set_policy_context('S',l_org_id);  -- 25/09/2014 Added by Veera as per the Canada OU Test 
	
    l_err_callpoint := 'Before submitting Concurrent request';
    --------------------------------------------------------------------------
    -- Submit "XXWC AP Indirect Sourcing Data Outbound Interface" Program
    --------------------------------------------------------------------------
    l_req_id        := fnd_request.submit_request(application => 'XXWC',
                                                  program => p_conc_prg_name,
                                                  description => NULL,
                                                  start_time => SYSDATE,
                                                  sub_request => FALSE,
                                                  -- argument1 => l_conc_prg_arg1
                                                  argument1 => NULL -- Version# 1.1
                                                  );
    l_err_callpoint := 'After submitting Concurrent request';
  
    COMMIT;
  
    dbms_output.put_line('After fnd_request');
    IF (l_req_id != 0) THEN
      IF fnd_concurrent.wait_for_request(l_req_id, 30, 15000, v_phase,
                                         v_status, v_dev_phase, v_dev_status,
                                         v_message) THEN
        v_error_message := chr(10) || 'ReqID=' || l_req_id || ' DPhase ' ||
                           v_dev_phase || ' DStatus ' || v_dev_status ||
                           chr(10) || ' MSG - ' || v_message;
        -- Error Returned
        IF v_dev_phase != 'COMPLETE'
           OR v_dev_status != 'NORMAL' THEN
          l_statement := 'An error occured running the XXWC_AP_IND_SRCG_PKG ' ||
                         v_error_message || '.';
          fnd_file.put_line(fnd_file.log, l_statement);
          fnd_file.put_line(fnd_file.output, l_statement);
          RAISE program_error;
        END IF;
        -- Then Success!
      ELSE
        l_statement := 'An error occured running the XXWC_AP_IND_SRCG_PKG ' ||
                       v_error_message || '.';
        fnd_file.put_line(fnd_file.log, l_statement);
        fnd_file.put_line(fnd_file.output, l_statement);
        RAISE program_error;
      END IF;
    
    ELSE
      l_statement := 'An error occured running the XXWC_AP_IND_SRCG_PKG ';
      fnd_file.put_line(fnd_file.log, l_statement);
      fnd_file.put_line(fnd_file.output, l_statement);
      RAISE program_error;
    END IF;
  
    dbms_output.put_line(l_sec);
    dbms_output.put_line('Request ID:  ' || l_req_id);
  EXCEPTION
    WHEN program_error THEN
      ROLLBACK;
      l_err_code := 2;
      l_err_msg  := l_sec;
      l_err_msg  := l_err_msg || ' ERROR ' || substr(SQLERRM, 1, 2000);
      dbms_output.put_line(l_err_msg);
      fnd_file.put_line(fnd_file.log, l_err_msg);
      fnd_file.put_line(fnd_file.output, l_err_msg);
      --        l_err_callpoint := l_message;
    
      xxcus_error_pkg.xxcus_error_main_api(p_called_from => l_err_callfrom,
                                           p_calling => l_err_callpoint,
                                           p_request_id => l_req_id,
                                           p_ora_error_msg => SQLERRM,
                                           p_error_desc => 'Error running XXWC_AP_IND_SRCG_PKG package with PROGRAM ERROR',
                                           p_distribution_list => pl_dflt_email,
                                           p_module => 'AR');
    
      p_retcode := 2;
    WHEN OTHERS THEN
      fnd_file.put_line(fnd_file.log, l_sec);
      fnd_file.put_line(fnd_file.output, l_sec);
      l_err_code := 2;
      l_err_msg  := l_sec;
      l_err_msg  := l_sec || l_err_msg || ' ERROR ' || SQLCODE ||
                    substr(SQLERRM, 1, 2000);
      dbms_output.put_line(l_err_msg);
      fnd_file.put_line(fnd_file.log, l_err_msg);
      fnd_file.put_line(fnd_file.output, l_err_msg);
      --        l_err_callpoint := l_message;
    
      p_retcode := 2;
    
      xxcus_error_pkg.xxcus_error_main_api(p_called_from => l_err_callfrom,
                                           p_calling => l_err_callpoint,
                                           p_request_id => l_req_id,
                                           p_ora_error_msg => substr(l_err_msg,
                                                                      1, 2000),
                                           p_error_desc => 'Error running XXWC_AP_IND_SRCG_PKG package with OTHERS Exception',
                                           p_distribution_list => pl_dflt_email,
                                           p_module => 'AR');
  END uc4_call;

END XXWC_AP_IND_SRCG_PKG;
/
