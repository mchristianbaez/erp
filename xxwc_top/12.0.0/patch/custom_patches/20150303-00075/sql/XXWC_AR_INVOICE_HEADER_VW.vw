CREATE OR REPLACE FORCE VIEW APPS.XXWC_AR_INVOICE_HEADER_VW
(
/******************************************************************************
   NAME:       APPS.XXWC_AR_INVOICE_HEADER_VW
   PURPOSE:  This view is used to extract all the Receivable invoice header detais

   REVISIONS:
   Ver        Date        Author           Description
   ---------  ----------  ---------------  ------------------------------------
   1.0        2/26/2015    Raghavendra s   TMS# 20150217-00203  Add Function to remove the line breaks for ordered_by_name column
   2.0        04/04/2015   Raghavendra S   TMS# 20150303-00075 - FIN - Addition of Code Snippet for Line Breaks in All EDW Extract Programs
                                            and Views for all free entry text fields.
******************************************************************************/
   BUSINESS_UNIT,
   SOURCE_SYSTEM,
   OPERATING_UNIT_ID,
   OPERATING_UNIT_NAME,
   INVOICE_UNIQUE_ID,
   INVOICE_NUMBER,
   ORDER_NUMBER,
   RELEASE_IDENTIFIER,
   INVOICE_POST_DATE,
   INVOICE_CREATE_DATE,
   ORDER_CREATE_DATE,
   ORDER_DATE,
   ORDER_LAST_UPDATE_DATE,
   ORDER_PRICE_DATE,
   INVOICE_TYPE,
   BILL_TO_CUST_ACCT_ID,
   SOLD_TO_CUST_ACCT_ID,
   SHIP_TO_CUST_ACCT_ID,
   SHIP_TO_ADDR_NAME,
   SHIP_TO_ADDRESS1,
   SHIP_TO_ADDRESS2,
   SHIP_TO_ADDRESS3,
   SHIP_TO_CITY,
   SHIP_TO_STATE,
   SHIP_TO_ZIP,
   SHIP_TO_COUNTRY,
   SALES_BRANCH,
   PRICE_BRANCH,
   INSIDE_SALESREP_ID,
   OUTSIDE_SALESREP_ID,
   ORDER_WHITE_SALESREP_ID,
   ORDERED_BY_NAME,
   ORDER_METHOD,
   PROJECT_JOB_ID,
   BACKORDER_FLAG,
   TOTAL_INB_FREIGHT_BILLED_AMT,
   TOTAL_INB_FREIGHT_EXPENSED_AMT,
   TOTAL_OUT_FREIGHT_BILLED_AMT,
   TOTAL_OUT_FREIGHT_EXPENSED_AMT,
   TOTAL_TAX_AMT,
   TOTAL_INVOICE_AMT,
   INVOICE_DISCOUNT_AMOUNT,
   PMT_TERM_DESCRIPTION,
   PMT_DISC_PERCENT,
   INVOICE_SOURCE_CODE,
   SALES_CHANNEL,
   ORDER_TYPE,
   QUOTE_NUMBER,
   DIRECT_INVOICE_FLAG,
   DIRECT_VENDOR_ID,
   PO_ID,   
   CUSTOMER_CLASS_DESC,
   MISCELLANEOUS_CHARGE,
   INTERFACE_HEADER_CONTEXT,
   INVOICE_CREATION_DATE,
   INVOICE_PURCHASE_ORDER,
   CUSTOMER_PROFILE,
   CUST_PRIMARY_BT_SITE,
   ORDER_SOURCE_REFERENCE         --Added by Maha on 4/30/2014 for TMS#20140318-00095 
)
AS
     SELECT /*+ INDEX(apsa XXWC_AR_PAYMENT_SCHEDULES_N1) */
           'WHITECAP' business_unit,
            'Oracle EBS' source_system,
            hou.organization_id operating_unit_id,
            hou.name operating_unit_name,
            rcta.customer_trx_id invoice_unique_id,
            --rcta.trx_number invoice_number, -- Commented for V 2.0
            REGEXP_REPLACE(rcta.trx_number,  '[[:cntrl:]]', ' ') invoice_number, -- Added for V 2.0
            --NVL (TO_CHAR (ooha.order_number), rcta.interface_header_attribute1) order_number, -- Commented for V 2.0
            REGEXP_REPLACE((NVL (TO_CHAR (ooha.order_number), rcta.interface_header_attribute1)),  '[[:cntrl:]]', ' ') order_number, -- Added for V 2.0
            NULL release_identifier, -- 08/01/2012 CG: Changed per email from Rich and business decision as to what is used for reporting
            --TRUNC (rcta_gd.gl_date) invoice_post_date,
            --TRUNC (rcta.trx_date) invoice_create_date,
            (CASE
                WHEN rbsa.name IN ('WC MANUAL', 'MANUAL-OTHER')
                THEN
                   TRUNC (rcta.creation_date)
                ELSE
                   (TRUNC (rcta.creation_date) - 1)
             END)
               invoice_post_date,
            TRUNC (rcta_gd.gl_date) invoice_create_date,
            TRUNC (ooha.creation_date) order_create_date,
            TRUNC (ooha.booked_date) order_date, --TRUNC (ooha.ordered_date) orderdate,
            TRUNC (ooha.last_update_date) order_last_update_date,
            TRUNC (ooha.booked_date) order_price_date, -- 06/06/2012 CG: Updated to match valid values
            DECODE (rccta.TYPE,
                    'CM', 'RET',
                    'CB', 'CGB',
                    'DM', 'ADJ',
                    rccta.TYPE)
               invoice_type,    -- 08/06/2012 CG: Changed to match customer MV
            --rcta.bill_to_customer_id bill_to_cust_acct_id,
            --rcta.sold_to_customer_id sold_to_cust_acct_id,
            --rcta.ship_to_customer_id ship_to_cust_acct_id,
            (hps_bill.party_site_number || rcta.bill_to_site_use_id)
               bill_to_cust_acct_id,
            (hps_bill.party_site_number || rcta.bill_to_site_use_id)
               sold_to_cust_acct_id,
            (hps_ship.party_site_number || rcta.ship_to_site_use_id)
               ship_to_cust_acct_id,
            hps_ship.party_site_name ship_to_addr_name,
            -- hl_ship.address1 ship_to_address1, -- Commented for V 2.0
            -- hl_ship.address2 ship_to_address2, -- Commented for V 2.0
            -- hl_ship.address3 ship_to_address3, -- Commented for V 2.0
            REGEXP_REPLACE(hl_ship.address1,  '[[:cntrl:]]', ' ') ship_to_address1, -- Added for V 2.0
            REGEXP_REPLACE(hl_ship.address2,  '[[:cntrl:]]', ' ') ship_to_address2, -- Added for V 2.0
            REGEXP_REPLACE(hl_ship.address3,  '[[:cntrl:]]', ' ') ship_to_address3, -- Added for V 2.0
            hl_ship.city ship_to_city,
            hl_ship.state ship_to_state,
            hl_ship.postal_code ship_to_zip,
            hl_ship.country ship_to_country -- 03/25/2013 CG: TMS 20130308-01451: Update header sales and price branch to use
                                           -- GL Segment2 in case it's a manual invoice
                                           /*, NVL ( (SELECT  mp.organization_code
                                                FROM   mtl_parameters mp
                                                 WHERE   organization_id = ooha.ship_from_org_id), 'WCC')
                                              sales_branch
                                              , NVL ( (SELECT mp.organization_code
                                              FROM mtl_parameters mp
                                                 WHERE   organization_id = ooha.ship_from_org_id), 'WCC')
                                              price_branch*/
            ,
            REGEXP_REPLACE(NVL (
               (SELECT mp.organization_code
                  FROM mtl_parameters mp
                 -- 08/25/2013 CG: TMS 20130822-00908: Updated to use Interface Header Attribute10 from Invoice
                 -- WHERE   organization_id = ooha.ship_from_org_id)
                 WHERE TO_CHAR (organization_id) =
                          rcta.interface_header_attribute10),
               NVL (
                  (apps.xxwc_mv_routines_pkg.get_branch_num_from_lob (
                      apps.xxwc_mv_routines_pkg.get_gl_code_segment (
                         rcta_gd.code_combination_id,
                         2))),
                   --'WCC')) --01/10/2014 Commented by Veera As per the Canada OU Test
                  fnd_profile.VALUE ('XXWC_CORPORATE_ORGANIZATION'))),  '[[:cntrl:]]', ' ') --01/10/2014 Added By Veera As per the Canada OU Test
               sales_branch, -- Added for V 2.0
            REGEXP_REPLACE(NVL (
               (SELECT mp.organization_code
                  FROM mtl_parameters mp
                 WHERE organization_id = ooha.ship_from_org_id),
               NVL (
                  (apps.xxwc_mv_routines_pkg.get_branch_num_from_lob (
                      apps.xxwc_mv_routines_pkg.get_gl_code_segment (
                         rcta_gd.code_combination_id,
                         2))),
                  --'WCC')) --01/10/2014 Commented by Veera As per the Canada OU Test
                  fnd_profile.VALUE ('XXWC_CORPORATE_ORGANIZATION'))),  '[[:cntrl:]]', ' ')  --01/10/2014 Added By Veera As per the Canada OU Test
               price_branch     -- Added for V 2.0
               -- 03/25/2013 CG: TMS 20130308-01451
                           -- 08/16/12 CG: Updated per email
                           -- Outside Salesrep will be the account manager
                           -- Inside sales rep and order writter will be the created by NTID
                           -- jrs.salesrep_number inside_salesrep_id,
                           -- NULL outside_salesrep_id,
                           -- jrs.salesrep_number order_white_salesrep_id,
            ,
            REGEXP_REPLACE(SUBSTR (
               (xxwc_mv_routines_pkg.get_username (
                   NVL (ooha.created_by, rcta.created_by))),
               1,
               100),  '[[:cntrl:]]', ' ') 
               inside_salesrep_id, -- Added for V 2.0
            jrs.salesrep_number outside_salesrep_id,
            REGEXP_REPLACE(SUBSTR (
               (xxwc_mv_routines_pkg.get_username (
                   NVL (ooha.created_by, rcta.created_by))),
               1,
               100),  '[[:cntrl:]]', ' ')  -- Added for V 2.0
               order_white_salesrep_id,    --ooha.created_by orderwriterrepid,
            REGEXP_REPLACE(SUBSTR (
               xxwc_mv_routines_pkg.get_contact_name (ooha.sold_to_contact_id),
               1,
               240),'[[:cntrl:]]',' ') --  added REGEXP_REPLACE by Raghavendra for TMS# 20150217-00203 V 1.0
               ordered_by_name,
            --ooha.attribute2 order_method, commented by harsha for TMS# 20140130-00206
            REGEXP_REPLACE(xxwc_mv_routines_pkg.XXWC_order_method_FNC (ooha.order_source_id,
                                                        ooha.attribute2),'[[:cntrl:]]',' ') 
               order_method,     -- Added for V 2.0   -- added by harsha for TMS# 20140130-00206
            hca.account_number project_job_id,
            'N' backorder_flag,
            0 total_inb_freight_billed_amt,
            0 total_inb_freight_expensed_amt,
            apsa.freight_original total_out_freight_billed_amt, /*NVL
                                                                   ((SELECT SUM (rctla.extended_amount)
                                                                    FROM apps.ra_customer_trx_lines rctla
                                                                   WHERE rctla.line_type = 'FREIGHT'
                                                                     AND rctla.customer_trx_id = rcta.customer_trx_id),
                                                                   0
                                                                  ) totaloutbfreightbilled,*/
            0 total_out_freight_expensed_amt,
            apsa.tax_original total_tax_amt, /*NVL ((SELECT SUM (rctla.extended_amount)
                                                  FROM apps.ra_customer_trx_lines rctla
                                                 WHERE rctla.line_type = 'TAX'
                                                AND rctla.customer_trx_id = rcta.customer_trx_id),
                                                0
                                                 ) totaltaxamount,*/
            apsa.amount_due_original total_invoice_amt, /*NVL ((SELECT SUM (rctla.extended_amount)
                                                             FROM apps.ra_customer_trx_lines rctla
                                                            WHERE rctla.customer_trx_id = rcta.customer_trx_id),
                                                           0
                                                            ) totalinvoiceamoun,*/
            xxwc_mv_routines_pkg.get_inv_adj_amount (rcta.customer_trx_id,
                                                     NULL,
                                                     rcta.org_id)
               invoice_discount_amount,
            REGEXP_REPLACE(rtt.payment_term_description,'[[:cntrl:]]',' ')  pmt_term_description, -- Added for V 2.0 
            rtt.perc_discount pmt_disc_percent,
            rcta.interface_header_context invoice_source_code,
            ooha.sales_channel_code sales_channel, -- 06/07/12 CG Updated to look at the order type
            -- rcta.ship_via order_type,
            REGEXP_REPLACE((CASE
                WHEN UPPER (hcpc.name) = UPPER ('WC Branches') THEN 'INTR'
                WHEN UPPER (ottt.name) LIKE '%STANDARD%' THEN 'STD'
                WHEN UPPER (ottt.name) LIKE '%COUNTER%' THEN 'CNTR'
                WHEN UPPER (ottt.name) LIKE '%INTERNAL%' THEN 'INTR'
                WHEN UPPER (ottt.name) LIKE '%LONG%RENTAL%' THEN 'RLLT'
                WHEN UPPER (ottt.name) LIKE '%SHORT%RENTAL%' THEN 'RLST'
                WHEN UPPER (ottt.name) LIKE '%REPAIR%' THEN 'REPR'
                WHEN UPPER (ottt.name) LIKE '%RETURN%' THEN 'RTRN'
                ELSE 'STD'
             END),'[[:cntrl:]]',' ') 
               order_type, -- Added for V 2.0 
            REGEXP_REPLACE(DECODE (interface_header_context,
                    'CONVERSION', NULL,
                    rcta.interface_header_attribute1),'[[:cntrl:]]',' ') 
               quote_number, -- Added for V 2.0 
            --SUBSTR (
            --  xxwc_mv_routines_pkg.get_order_has_dropships (ooha.header_id),
            --  1, COMMENTED BY HARSHA FOR TMS#20131118-00053 AND PASSING NULL VALUE IN DIRECT INVOICE FLAG
            --  1)
            NULL direct_invoice_flag,
            NULL direct_vendor_id,            
            NULL po_id,            
            cust_class.meaning customer_class_desc,
            NULL miscellaneous_charge,
            rcta.interface_header_context,
            rcta.creation_date,
            rcta.purchase_order,
            hcpc.name customer_profile -- 10/19/2012 CG: Added for new mapping
                                      ,
            REGEXP_REPLACE(SUBSTR (
               apps.xxwc_mv_routines_pkg.get_edw_cust_pb_site (
                  apsa.customer_id,
                  rcta.org_id),
               1,
               50),'[[:cntrl:]]',' ')  cust_primary_bt_site, -- Added for V 2.0 
               ooha.orig_sys_document_ref
       FROM apps.ra_customer_trx rcta,
            hr_operating_units hou,
            apps.ra_cust_trx_line_gl_dist rcta_gd,
            apps.ar_payment_schedules apsa,
            apps.hz_cust_site_uses hcasua_ship,
            apps.hz_cust_acct_sites hcasa_ship,
            hz_party_sites hps_ship,
            hz_locations hl_ship,
            hz_cust_accounts hca,
            apps.oe_order_headers ooha,                    --mtl_parameters mp,
            xxwc_ar_payment_terms_vw rtt,
            fnd_lookup_values cust_class,
            apps.ra_cust_trx_types rccta,
            jtf.jtf_rs_salesreps jrs,                     -- 05/22/12 CG Added
            apps.ra_batch_sources rbsa,                    -- 06/06/12 CG Added
            oe_transaction_types_tl ottt, -- 07/11/12 CG Added to identify internal accounts
            hz_customer_profiles hcp,
            hz_cust_profile_classes hcpc, -- 08/06/12 CG Added Bill to site info
            apps.hz_cust_site_uses hcasua_bill,
            apps.hz_cust_acct_sites hcasa_bill,
            hz_party_sites hps_bill
      WHERE     rcta.org_id = hou.organization_id
            AND rcta.customer_trx_id = rcta_gd.customer_trx_id(+)
            AND rcta_gd.customer_trx_line_id(+) IS NULL
            AND rcta.ship_to_site_use_id = hcasua_ship.site_use_id(+)
            AND hcasua_ship.cust_acct_site_id = hcasa_ship.cust_acct_site_id(+)
            AND hcasa_ship.party_site_id = hps_ship.party_site_id(+)
            AND hps_ship.location_id = hl_ship.location_id(+)
            AND rcta.sold_to_customer_id = hca.cust_account_id
            AND rcta.term_id = rtt.payterm_id(+)
            AND rcta.org_id = rtt.operating_unit_id(+)
            -- Payment Schedule
            AND rcta.customer_trx_id = apsa.customer_trx_id
            AND rcta.org_id = apsa.org_id
            -- Customer Class Lookup
            AND hca.customer_class_code = cust_class.lookup_code(+)
            AND cust_class.lookup_type(+) = 'CUSTOMER CLASS'
            AND cust_class.language(+) = 'US'
            AND cust_class.view_application_id(+) = 222 -- Receivables Application
            -- Order Information
            AND rcta.interface_header_attribute1 =
                   TO_CHAR (ooha.order_number(+))
            --AND ooha.ship_from_org_id = mp.organization_id(+)
            -- Trx Type
            AND rcta.cust_trx_type_id = rccta.cust_trx_type_id
            AND rcta.org_id = rccta.org_id
            -- Sales Rep Data
            AND rcta.primary_salesrep_id = jrs.salesrep_id(+)
            AND rcta.org_id = jrs.org_id(+)
            -- Added 05/22/2012 CG To exclude Prism/Conversion Lines
            AND rcta.batch_source_id = rbsa.batch_source_id
            AND rcta.org_id = rbsa.org_id
            AND rbsa.name NOT IN ('PRISM', 'PRISM REFUND', 'CONVERSION')
            -- 06/06/2012 CG Added to pull order type to classify
            AND ooha.order_type_id = ottt.transaction_type_id(+)
            AND ottt.language(+) = 'US'
            -- 07/11/2012 CG Added to pull customer profile
            AND hca.cust_account_id = hcp.cust_account_id
            AND hca.party_id = hcp.party_id
            AND hcp.site_use_id IS NULL
            AND hcp.status = 'A'
            AND hcp.profile_class_id = hcpc.profile_class_id
            -- 08/06/2012 CG Added to pull bill to info
            AND rcta.bill_to_site_use_id = hcasua_bill.site_use_id(+)
            AND hcasua_bill.cust_acct_site_id = hcasa_bill.cust_acct_site_id(+)
            AND hcasa_bill.party_site_id = hps_bill.party_site_id(+)
   ORDER BY rcta.customer_trx_id;
