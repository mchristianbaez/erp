/* Ver        Date         Author           Description
   ---------  -----------  ---------------  ------------------------------------
   1.0        18-DEC-2015  Kishorebabu V    TMS 20151207-00185 - 504 BranchRelo Stock Locators
                                            Table is created to backup the Branchrelo locator values
                                            Initial Version   */

CREATE TABLE  XXWC.XXWC_ITEM_LOCATIONS_BKP_TBL AS
(SELECT *
FROM apps.mtl_item_locations
WHERE     organization_id = 350
AND subinventory_code = 'BranchRelo'
AND inventory_location_id NOT IN (SELECT secondary_locator
                                  FROM APPS.MTL_SECONDARY_LOCATORS MSL
                                  WHERE organization_id = 350))
/
/* Ver        Date         Author           Description
   ---------  -----------  ---------------  ------------------------------------
   1.0        18-DEC-2015  Kishorebabu V    TMS 20151207-00185 - 504 BranchRelo Stock Locators
                                            Deleting the Branchrelo locators and assigning the same locators to General subinventory
                                            Initial Version   */

SET serveroutput ON SIZE 1000000;


DECLARE

      -- Variables Declaration

      l_new_subinventory            VARCHAR2 (100) := 'General';
      l_procedure                   VARCHAR2 (100) := 'CREATE_ASSIGN_LOCATORS_PRC';
      l_return_status               VARCHAR2 (10);
      l_msg_count                   NUMBER := 0;
      l_msg_data                    VARCHAR2 (2000);
      l_new_inventory_location_id   MTL_ITEM_LOCATIONS.INVENTORY_LOCATION_ID%TYPE;
      l_locator_exists              VARCHAR2 (10);
      l_disable_date                DATE := TRUNC (SYSDATE);
      l_error_mess                  VARCHAR2 (1000);
      l_ora_err_mess                VARCHAR2 (1000);
      l_update_status               VARCHAR2 (1000);
      l_msg_data1                   VARCHAR2 (1000);
	  ln_count                      NUMBER;
      ln_total_count NUMBER;

BEGIN

SELECT count(*)
INTO   ln_total_count
FROM   mtl_item_locations mil
WHERE  organization_id    = 350
AND    subinventory_code  = 'BranchRelo';

IF ln_total_count >0 THEN

DELETE
FROM   mtl_item_locations mil
WHERE  organization_id    = 350
AND    subinventory_code  = 'BranchRelo'
AND    inventory_location_id IN (SELECT inventory_location_id
                                 FROM   XXWC.XXWC_ITEM_LOCATIONS_BKP_TBL
								);
COMMIT;
FOR rec_locators_bkp IN (SELECT organization_id
                         ,      segment1
                         FROM   XXWC.XXWC_ITEM_LOCATIONS_BKP_TBL
                         WHERE  organization_id   = 350
                         AND    subinventory_code = 'BranchRelo'
						) LOOP

l_return_status := NULL;
l_msg_count     := 0;
l_msg_data      := NULL;

INV_LOC_WMS_PUB.CREATE_LOCATOR (
                  x_return_status              => l_return_status,
                  x_msg_count                  => l_msg_count,
                  x_msg_data                   => l_msg_data,
                  x_inventory_location_id      => l_new_inventory_location_id,
                  x_locator_exists             => l_locator_exists,
                  p_organization_id            => rec_locators_bkp.organization_id,
                  p_organization_code          => NULL,
                  p_concatenated_segments      => rec_locators_bkp.segment1 || '.',
                  p_description                => NULL,
                  p_inventory_location_type    => 3,
                  p_picking_order              => NULL,
                  p_location_maximum_units     => NULL,
                  p_SUBINVENTORY_CODE          => l_new_subinventory,
                  p_LOCATION_WEIGHT_UOM_CODE   => NULL,
                  p_mAX_WEIGHT                 => NULL,
                  p_vOLUME_UOM_CODE            => NULL,
                  p_mAX_CUBIC_AREA             => NULL,
                  p_x_COORDINATE               => NULL,
                  p_Y_COORDINATE               => NULL,
                  p_Z_COORDINATE               => NULL,
                  p_PHYSICAL_LOCATION_ID       => NULL,
                  p_PICK_UOM_CODE              => NULL,
                  p_DIMENSION_UOM_CODE         => NULL,
                  p_LENGTH                     => NULL,
                  p_WIDTH                      => NULL,
                  p_HEIGHT                     => NULL,
                  p_STATUS_ID                  => 1,
                  p_dropping_order             => NULL);
               COMMIT;

               IF  l_return_status = 'S' AND l_new_inventory_location_id IS NOT NULL THEN
			   DBMS_OUTPUT.PUT_LINE(rec_locators_bkp.segment1
                  || ' Locator Creation Status '
                  || l_return_status
                  || ' Error Count '
                  || NVL (l_msg_count, 0)
                  || ' New Locator Id '
                  || l_new_inventory_location_id);
			   ELSE
                  DBMS_OUTPUT.PUT_LINE(rec_locators_bkp.segment1
                                       || ' Stock Locator Creation process Failed '
                                       || l_return_status
                                       || ' '
                                       || l_msg_count);
		       END IF;
END LOOP;
ELSE
DBMS_OUTPUT.PUT_LINE('No Stock Locators found for Branchrelo');
END IF;
EXCEPTION
  WHEN others THEN
  DBMS_OUTPUT.PUT_LINE('Exception Occured'||SQLERRM);
END;
/