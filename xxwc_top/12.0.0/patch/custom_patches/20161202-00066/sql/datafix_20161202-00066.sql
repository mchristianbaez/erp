/* Formatted on 12/2/2016 1:07:48 PM (QP5 v5.265.14096.38000) */
  /*************************************************************************
   *  Copyright (c) 2016 HD Supply Inc.
   *  All rights reserved.
   **************************************************************************
   *   $Header daatfix_20161202-00066.sql$
   *   Module Name: daatfix_20161202-00066.sql
   *
   *   PURPOSE:    Data fix to delete report line id 8635620 from the ap.ap_exp_report_dists_all table
   *
   *   REVISIONS:
   *   Ver        Date        Author                     Description
   *   ---------  ----------  ---------------         -------------------------
   *   1.0        11/15/2016  Neha Saini               Initial Version TMS# 20161202-00066 
   * ***************************************************************************/
DELETE FROM ap.ap_exp_report_dists_all
      WHERE report_header_id = 886540 AND report_line_id = 8635620;

COMMIT;