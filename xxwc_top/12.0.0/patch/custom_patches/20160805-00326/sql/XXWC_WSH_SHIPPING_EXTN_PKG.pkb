CREATE OR REPLACE PACKAGE BODY apps.xxwc_wsh_shipping_extn_pkg
as
/*************************************************************************
  $Header xxwc_wsh_shipping_extn_pkg $
  Module Name: xxwc_wsh_shipping_extn_pkg.pks

  PURPOSE:   This package is called by the concurrent programs
             XXWC WSH Backordered Transactions Processing

  REVISIONS:
  Ver        Date        Author                     Description
  ---------  ----------  ---------------         -------------------------
  1.0        01/21/2013  Consuelo Gonzalez       Initial Version
  2.0        11/12/2013  Ram and Rasikha         TMS 20131009-00330
  3.0        10/15/2014  Ram Talluri             TMS 20141015-00143 Fix for clear delivery action is failing if the delivery includes intangible items
  3.1        10/01/2014  Gopi Damuluri           TMS20141001-00059 Multi Org Changes
  3.2        04/17/2015  Gopi Damuluri           TMS 20150226-00083 DMS User item description and the perforamance fix for the program
  3.3        10/02/2015  Pattabhi Avula         TMS 20150526-00214 -- Intangibles and SubInv Transfers issues fixed
  3.4        11/19/2015  Manjula Chellappan      TMS 20151023-00037 - Shipping Extension Modification for Counter Order Removal and PUBD
  3.4.1      04/04/2016  Rakesh Patel            TMS 20160404-00014- Order workflow status fix to exclude SOURCE_TYPE_COD!=EXTERNAL
  3.4.2      04/14/2016  Rakesh Patel            TMS 20160414-00047 - Bug fix related to counter order workflow changes
  3.5        03/09/2016  Manjula Chellappan      TMS 20150622-00156 - Shipping Extension modification for PUBD
  3.6        07/06/2016  Rakesh Patel            TMS 20160624-00205-Subinventory code is either not entered or not valid for the given organization
  3.7        01/20/2017  P.Vamshidhar            TMS#20161130-00181 - Standard Order Delivery documents causing inventory to drive negative
  3.8        02/02/2017  Nancy Pahwa             TMS 20161102-00131 - AIS Open SO Quantity Issue when branch changed
  3.9        02/14/2017  Rakesh Patel            TMS# 20160805-00326 Create Prototype Sales Order from - Fix for error record has been changed by another user
**************************************************************************/

procedure write_log (p_header_id in number
                    , p_line_id in number
                    , p_log_message in varchar2
                    , p_log_sequence in number)
is
begin

    if NVL(G_ONT_DEBUG_LEVEL, 0) > 0 then

       fnd_file.put_line(fnd_file.log, (p_header_id||'-'||p_line_id||'. '||p_log_sequence||': '||p_log_message));

       insert into xxwc.xxwc_wsh_shipping_log (
            header_id
            , line_id
            , log_message
            , creation_date
            , log_sequence
        ) values (
            p_header_id
            , p_line_id
            , substr(p_log_message, 1, 4000)
            , sysdate
            , p_log_sequence
        );
    end if;

exception
when others then
    null; -- noop
end;


procedure delete_line(i_delivery_id in number, i_line_id in number)
is
begin
    delete from XXWC_WSH_SHIPPING_STG where
     line_id=i_line_id
     and delivery_id=i_delivery_id;
  commit;
end delete_line;


PROCEDURE split_line(i_header_id in number,
                     i_ship_from_org_id in number,
                      i_user_id in number,
                      i_resp_id in number,
                      i_appl_id in number,
                      o_return_status out varchar2,
                      o_return_msg out varchar2)
  /**************************************************************************
   $Header xxwc_wsh_shipping_extn_pkg $
   Module Name: split_line
   PURPOSE:   This Procedure is used to split the order line
   REVISIONS:
   Ver        Date        Author                     Description
   ---------  ----------  ---------------         -------------------------
   1.0        04/17/2013  Consuelo Gonzalez      Initial Version -TMS 20130422-01089
   3.4        03/03/2016  Manjula Chellappan    TMS #20151023-00037 - Shipping Extension Modification
                                                    for Counter Order Removal and PUBD
   3.5        03/17/2016  Rakesh Patel          TMS 20150622-00156 - Shipping Extension modification for PUBD
   3.9        02/14/2017  Rakesh Patel          TMS# 20160805-00326 Create Prototype Sales Order from - Fix for error record has been changed by another user
   **************************************************************************/
is
 l_header_rec OE_ORDER_PUB.Header_Rec_Type;
 l_line_tbl OE_ORDER_PUB.Line_Tbl_Type;
 l_action_request_tbl OE_ORDER_PUB.Request_Tbl_Type;
 l_header_adj_tbl OE_ORDER_PUB.Header_Adj_Tbl_Type;
 l_line_adj_tbl OE_ORDER_PUB.line_adj_tbl_Type;
 l_header_scr_tbl OE_ORDER_PUB.Header_Scredit_Tbl_Type;
 l_line_scredit_tbl OE_ORDER_PUB.Line_Scredit_Tbl_Type;
 l_request_rec OE_ORDER_PUB.Request_Rec_Type ;
 l_return_status VARCHAR2(1000);
 l_msg_count NUMBER;
 l_msg_data VARCHAR2(1000);
 p_api_version_number NUMBER :=1.0;
 p_init_msg_list VARCHAR2(10) := FND_API.G_FALSE;
 p_return_values VARCHAR2(10) := FND_API.G_FALSE;
 p_action_commit VARCHAR2(10) := FND_API.G_FALSE;
 x_return_status VARCHAR2(1);
 x_msg_count NUMBER;
 x_msg_data VARCHAR2(100);
 p_header_rec OE_ORDER_PUB.Header_Rec_Type := OE_ORDER_PUB.G_MISS_HEADER_REC;
 p_old_header_rec OE_ORDER_PUB.Header_Rec_Type :=  OE_ORDER_PUB.G_MISS_HEADER_REC;
 p_header_val_rec OE_ORDER_PUB.Header_Val_Rec_Type := OE_ORDER_PUB.G_MISS_HEADER_VAL_REC;
 p_old_header_val_rec OE_ORDER_PUB.Header_Val_Rec_Type := OE_ORDER_PUB.G_MISS_HEADER_VAL_REC;
 p_Header_Adj_tbl OE_ORDER_PUB.Header_Adj_Tbl_Type :=  OE_ORDER_PUB.G_MISS_HEADER_ADJ_TBL;
 p_old_Header_Adj_tbl OE_ORDER_PUB.Header_Adj_Tbl_Type := OE_ORDER_PUB.G_MISS_HEADER_ADJ_TBL;
 p_Header_Adj_val_tbl OE_ORDER_PUB.Header_Adj_Val_Tbl_Type := OE_ORDER_PUB.G_MISS_HEADER_ADJ_VAL_TBL;
 p_old_Header_Adj_val_tbl OE_ORDER_PUB.Header_Adj_Val_Tbl_Type := OE_ORDER_PUB.G_MISS_HEADER_ADJ_VAL_TBL;
 p_Header_price_Att_tbl OE_ORDER_PUB.Header_Price_Att_Tbl_Type := OE_ORDER_PUB.G_MISS_HEADER_PRICE_ATT_TBL;
 p_old_Header_Price_Att_tbl OE_ORDER_PUB.Header_Price_Att_Tbl_Type := OE_ORDER_PUB.G_MISS_HEADER_PRICE_ATT_TBL;
 p_Header_Adj_Att_tbl OE_ORDER_PUB.Header_Adj_Att_Tbl_Type := OE_ORDER_PUB.G_MISS_HEADER_ADJ_ATT_TBL;
 p_old_Header_Adj_Att_tbl OE_ORDER_PUB.Header_Adj_Att_Tbl_Type :=  OE_ORDER_PUB.G_MISS_HEADER_ADJ_ATT_TBL;
 p_Header_Adj_Assoc_tbl OE_ORDER_PUB.Header_Adj_Assoc_Tbl_Type :=  OE_ORDER_PUB.G_MISS_HEADER_ADJ_ASSOC_TBL;
 p_old_Header_Adj_Assoc_tbl OE_ORDER_PUB.Header_Adj_Assoc_Tbl_Type := OE_ORDER_PUB.G_MISS_HEADER_ADJ_ASSOC_TBL;
 p_Header_Scredit_tbl OE_ORDER_PUB.Header_Scredit_Tbl_Type := OE_ORDER_PUB.G_MISS_HEADER_SCREDIT_TBL;
 p_old_Header_Scredit_tbl OE_ORDER_PUB.Header_Scredit_Tbl_Type := OE_ORDER_PUB.G_MISS_HEADER_SCREDIT_TBL;
 p_Header_Scredit_val_tbl OE_ORDER_PUB.Header_Scredit_Val_Tbl_Type := OE_ORDER_PUB.G_MISS_HEADER_SCREDIT_VAL_TBL;
 p_old_Header_Scredit_val_tbl OE_ORDER_PUB.Header_Scredit_Val_Tbl_Type := OE_ORDER_PUB.G_MISS_HEADER_SCREDIT_VAL_TBL;
 p_line_tbl OE_ORDER_PUB.Line_Tbl_Type := OE_ORDER_PUB.G_MISS_LINE_TBL;
 p_old_line_tbl OE_ORDER_PUB.Line_Tbl_Type := OE_ORDER_PUB.G_MISS_LINE_TBL;
 p_line_val_tbl OE_ORDER_PUB.Line_Val_Tbl_Type := OE_ORDER_PUB.G_MISS_LINE_VAL_TBL;
 p_old_line_val_tbl OE_ORDER_PUB.Line_Val_Tbl_Type := OE_ORDER_PUB.G_MISS_LINE_VAL_TBL;
 p_Line_Adj_tbl OE_ORDER_PUB.Line_Adj_Tbl_Type := OE_ORDER_PUB.G_MISS_LINE_ADJ_TBL;
 p_old_Line_Adj_tbl OE_ORDER_PUB.Line_Adj_Tbl_Type := OE_ORDER_PUB.G_MISS_LINE_ADJ_TBL;
 p_Line_Adj_val_tbl OE_ORDER_PUB.Line_Adj_Val_Tbl_Type := OE_ORDER_PUB.G_MISS_LINE_ADJ_VAL_TBL;
 p_old_Line_Adj_val_tbl OE_ORDER_PUB.Line_Adj_Val_Tbl_Type := OE_ORDER_PUB.G_MISS_LINE_ADJ_VAL_TBL;
 p_Line_price_Att_tbl OE_ORDER_PUB.Line_Price_Att_Tbl_Type := OE_ORDER_PUB.G_MISS_LINE_PRICE_ATT_TBL;
 p_old_Line_Price_Att_tbl OE_ORDER_PUB.Line_Price_Att_Tbl_Type := OE_ORDER_PUB.G_MISS_LINE_PRICE_ATT_TBL;
 p_Line_Adj_Att_tbl OE_ORDER_PUB.Line_Adj_Att_Tbl_Type := OE_ORDER_PUB.G_MISS_LINE_ADJ_ATT_TBL;
 p_old_Line_Adj_Att_tbl OE_ORDER_PUB.Line_Adj_Att_Tbl_Type := OE_ORDER_PUB.G_MISS_LINE_ADJ_ATT_TBL;
 p_Line_Adj_Assoc_tbl OE_ORDER_PUB.Line_Adj_Assoc_Tbl_Type := OE_ORDER_PUB.G_MISS_LINE_ADJ_ASSOC_TBL;
 p_old_Line_Adj_Assoc_tbl OE_ORDER_PUB.Line_Adj_Assoc_Tbl_Type := OE_ORDER_PUB.G_MISS_LINE_ADJ_ASSOC_TBL;
 p_Line_Scredit_tbl OE_ORDER_PUB.Line_Scredit_Tbl_Type := OE_ORDER_PUB.G_MISS_LINE_SCREDIT_TBL;
 p_old_Line_Scredit_tbl OE_ORDER_PUB.Line_Scredit_Tbl_Type := OE_ORDER_PUB.G_MISS_LINE_SCREDIT_TBL;
 p_Line_Scredit_val_tbl OE_ORDER_PUB.Line_Scredit_Val_Tbl_Type := OE_ORDER_PUB.G_MISS_LINE_SCREDIT_VAL_TBL;
 p_old_Line_Scredit_val_tbl OE_ORDER_PUB.Line_Scredit_Val_Tbl_Type := OE_ORDER_PUB.G_MISS_LINE_SCREDIT_VAL_TBL;
 p_Lot_Serial_tbl OE_ORDER_PUB.Lot_Serial_Tbl_Type := OE_ORDER_PUB.G_MISS_LOT_SERIAL_TBL;
 p_old_Lot_Serial_tbl OE_ORDER_PUB.Lot_Serial_Tbl_Type := OE_ORDER_PUB.G_MISS_LOT_SERIAL_TBL;
 p_Lot_Serial_val_tbl OE_ORDER_PUB.Lot_Serial_Val_Tbl_Type := OE_ORDER_PUB.G_MISS_LOT_SERIAL_VAL_TBL;
 p_old_Lot_Serial_val_tbl OE_ORDER_PUB.Lot_Serial_Val_Tbl_Type := OE_ORDER_PUB.G_MISS_LOT_SERIAL_VAL_TBL;
 p_action_request_tbl OE_ORDER_PUB.Request_Tbl_Type := OE_ORDER_PUB.G_MISS_REQUEST_TBL;
 x_header_val_rec OE_ORDER_PUB.Header_Val_Rec_Type;
 x_Header_Adj_tbl OE_ORDER_PUB.Header_Adj_Tbl_Type;
 x_Header_Adj_val_tbl OE_ORDER_PUB.Header_Adj_Val_Tbl_Type;
 x_Header_price_Att_tbl OE_ORDER_PUB.Header_Price_Att_Tbl_Type;
 x_Header_Adj_Att_tbl OE_ORDER_PUB.Header_Adj_Att_Tbl_Type;
 x_Header_Adj_Assoc_tbl OE_ORDER_PUB.Header_Adj_Assoc_Tbl_Type;
 x_Header_Scredit_tbl OE_ORDER_PUB.Header_Scredit_Tbl_Type;
 x_Header_Scredit_val_tbl OE_ORDER_PUB.Header_Scredit_Val_Tbl_Type;
 x_line_val_tbl OE_ORDER_PUB.Line_Val_Tbl_Type;
 x_Line_Adj_tbl OE_ORDER_PUB.Line_Adj_Tbl_Type;
 x_Line_Adj_val_tbl OE_ORDER_PUB.Line_Adj_Val_Tbl_Type;
 x_Line_price_Att_tbl OE_ORDER_PUB.Line_Price_Att_Tbl_Type;
 x_Line_Adj_Att_tbl OE_ORDER_PUB.Line_Adj_Att_Tbl_Type;
 x_Line_Adj_Assoc_tbl OE_ORDER_PUB.Line_Adj_Assoc_Tbl_Type;
 x_Line_Scredit_tbl OE_ORDER_PUB.Line_Scredit_Tbl_Type;
 x_Line_Scredit_val_tbl OE_ORDER_PUB.Line_Scredit_Val_Tbl_Type;
 x_Lot_Serial_tbl OE_ORDER_PUB.Lot_Serial_Tbl_Type;
 x_Lot_Serial_val_tbl OE_ORDER_PUB.Lot_Serial_Val_Tbl_Type;
 x_action_request_tbl OE_ORDER_PUB.Request_Tbl_Type;
 X_DEBUG_FILE VARCHAR2(100);
 l_line_tbl_index NUMBER :=0;
 l_msg_index_out NUMBER(10);
 l_org_id  number := xxwc_ascp_scwb_pkg.get_wc_org_id;

 cursor split_cur
     is
 select a.line_id, a.header_id, a.ordered_quantity, a.inventory_item_id, b.transaction_qty, b.rowid, b.force_ship_qty
   from oe_order_lines a, xxwc_wsh_shipping_stg b
  where a.header_id=i_header_id
    and a.header_id = b.header_id
    and a.line_id=b.line_id
    --and a.user_item_description in ('OUT_FOR_DELIVERY/PARTIAL_BACKORDER','UNKNOWN STAT BASED ON NUMBERS', 'BACKORDERED')
    --and a.user_item_description=b.status
    and b.status in ('OUT_FOR_DELIVERY/PARTIAL_BACKORDER','UNKNOWN STAT BASED ON NUMBERS', 'BACKORDERED')
    and a.ordered_quantity<> nvl(b.force_ship_qty,nvl(b.transaction_qty,0))
    and b.delivery_id is not null;
BEGIN
 fnd_global.apps_initialize(i_user_id,i_resp_id,i_appl_id,null);
 mo_global.set_policy_context('S',l_org_id);
 mo_global.init('ONT');
     write_log ( i_header_id
                , -1
                , substr('Debug:User '||i_user_id||' Resp:'||i_resp_id||' appl_id:'||i_appl_id, 1, 4000)
                , 99999999);

 for split_rec in split_cur
 loop
     write_log ( i_header_id
                , split_rec.line_id
                , substr('Inside Loop', 1, 4000)
                , 99999999);

 l_line_tbl_index := l_line_tbl_index+1;
-- Changed attributes
 l_header_rec := OE_ORDER_PUB.G_MISS_HEADER_REC;
 l_header_rec.header_id := split_rec.header_id;
 l_header_rec.operation := OE_GLOBALS.G_OPR_NONE; --TMS# 20160805-00326 Create Prototype Sales Order from - Fix for error record has been changed by another user

 l_line_tbl(l_line_tbl_index) := OE_ORDER_PUB.G_MISS_LINE_REC;
 l_line_tbl(l_line_tbl_index).operation := OE_GLOBALS.G_OPR_UPDATE;
 l_line_tbl(l_line_tbl_index).split_by := fnd_global.user_name; --i_user_id; -- user_id
 l_line_tbl(l_line_tbl_index).split_action_code := 'SPLIT';
 l_line_tbl(l_line_tbl_index).header_id := split_rec.header_id; -- header_id of the order
 l_line_tbl(l_line_tbl_index).line_id := split_rec.line_id; -- line_id of the order line
 --l_line_tbl(l_line_tbl_index).inventory_item_id := split_rec.inventory_item_id;
 l_line_tbl(l_line_tbl_index).ordered_quantity := nvl(split_rec.force_ship_qty, split_rec.transaction_qty); -- new ordered quantity
 l_line_tbl(l_line_tbl_index).change_reason := 'MISC'; -- change reason code
 --l_line_tbl(l_line_tbl_index).user_item_description := 'OUT_FOR_DELIVERY';

 l_line_tbl_index := l_line_tbl_index+1;
 l_line_tbl(l_line_tbl_index ) := OE_ORDER_PUB.G_MISS_LINE_REC;
 l_line_tbl(l_line_tbl_index ).operation := OE_GLOBALS.G_OPR_CREATE;
 l_line_tbl(l_line_tbl_index ).split_by := fnd_global.user_name; --i_user_id; -- user_id
 l_line_tbl(l_line_tbl_index ).split_action_code := 'SPLIT';
 l_line_tbl(l_line_tbl_index ).split_from_line_id := split_rec.line_id; -- line_id of  original line
 l_line_tbl(l_line_tbl_index ).inventory_item_id := split_rec.inventory_item_id; -- inventory item id
 l_line_tbl(l_line_tbl_index ).ordered_quantity := split_rec.ordered_quantity - NVL(split_rec.force_ship_qty, split_rec.transaction_qty); -- ordered quantity -- split_rec.ordered_quantity-split_rec.transaction_qty; -- ordered quantity
 --l_line_tbl(l_line_tbl_index ).user_item_description := null;
 --l_line_tbl(l_line_tbl_index ).attribute11 := 0 ;
 --l_line_tbl(l_line_tbl_index ).subinventory := 'General';

     write_log ( i_header_id
                , split_rec.line_id
                , substr('Looping', 1, 4000)
                , 99999999);

  update xxwc_wsh_shipping_stg
     set ordered_quantity = nvl(split_rec.force_ship_qty, split_rec.transaction_qty) -- split_rec.transaction_qty
       , status = 'OUT_FOR_DELIVERY'
   where rowid=split_rec.rowid;

--Added for Rev 3.4 Begin
  BEGIN
    DELETE FROM XXWC.XXWC_OE_OPEN_ORDER_LINES
     WHERE line_id = split_rec.line_id;
  EXCEPTION WHEN OTHERS THEN
      NULL;
  END;
--Added for Rev 3.4 End

 END LOOP;

      write_log ( i_header_id
                , -1
                , substr('Before API Call', 1, 4000)
                , 99999999);
IF l_line_tbl_index > 0 then
-- CALL TO PROCESS ORDER
 OE_ORDER_PUB.process_order (
    --p_org_id => l_org_id
    p_api_version_number => 1.0
  , p_init_msg_list => fnd_api.g_true
  , p_return_values => fnd_api.g_true
  , p_action_commit => fnd_api.g_false
  , x_return_status => l_return_status
  , x_msg_count => l_msg_count
  , x_msg_data => l_msg_data
  , p_header_rec => l_header_rec
  , p_line_tbl => l_line_tbl
  , p_action_request_tbl => l_action_request_tbl
-- OUT PARAMETERS
  , x_header_rec => p_header_rec
  , x_header_val_rec => x_header_val_rec
  , x_Header_Adj_tbl => x_Header_Adj_tbl
  , x_Header_Adj_val_tbl => x_Header_Adj_val_tbl
  , x_Header_price_Att_tbl => x_Header_price_Att_tbl
  , x_Header_Adj_Att_tbl => x_Header_Adj_Att_tbl
  , x_Header_Adj_Assoc_tbl => x_Header_Adj_Assoc_tbl
  , x_Header_Scredit_tbl => x_Header_Scredit_tbl
  , x_Header_Scredit_val_tbl => x_Header_Scredit_val_tbl
  , x_line_tbl => p_line_tbl
  , x_line_val_tbl => x_line_val_tbl
  , x_Line_Adj_tbl => x_Line_Adj_tbl
  , x_Line_Adj_val_tbl => x_Line_Adj_val_tbl
  , x_Line_price_Att_tbl => x_Line_price_Att_tbl
  , x_Line_Adj_Att_tbl => x_Line_Adj_Att_tbl
  , x_Line_Adj_Assoc_tbl => x_Line_Adj_Assoc_tbl
  , x_Line_Scredit_tbl => x_Line_Scredit_tbl
  , x_Line_Scredit_val_tbl => x_Line_Scredit_val_tbl
  , x_Lot_Serial_tbl => x_Lot_Serial_tbl
  , x_Lot_Serial_val_tbl => x_Lot_Serial_val_tbl
  , x_action_request_tbl => x_action_request_tbl
 );

     write_log ( i_header_id
                , -1
                , substr('After API Call:'||l_return_status, 1, 4000)
                , 99999999);

-- Retrieve messages
 FOR i IN 1 .. l_msg_count
 LOOP
  Oe_Msg_Pub.get( p_msg_index => i
  , p_encoded => Fnd_Api.G_FALSE
  , p_data => l_msg_data
  , p_msg_index_out => l_msg_index_out);
  o_return_msg := o_return_msg||':'||l_msg_data;
 END LOOP;
-- Check the return status
o_return_status := l_return_status;

--Added for ver 3.4 Begin
  BEGIN
  INSERT
    INTO XXWC.xxwc_oe_open_order_lines
      (
      line_id ,
      header_id ,
      line_type_id ,
      inventory_item_id ,
      organization_id ,
      ordered_quantity ,
      org_id ,
      created_by ,
      last_updated_by ,
      creation_date ,
      last_update_date ,
      last_update_login
      )
    SELECT ol.line_id ,
      ol.header_id ,
      ol.line_type_id ,
      ol.inventory_item_id ,
      ol.ship_from_org_id ,
      (ol.ordered_quantity
                     * po_uom_s.po_uom_convert (um.unit_of_measure
                                                , msib.primary_unit_of_measure
                                                , ol.inventory_item_id
                                               )
          ),
      ol.org_id ,
      fnd_global.user_id ,
      fnd_global.user_id ,
      sysdate ,
      sysdate ,
      fnd_global.login_id
    FROM oe_order_lines_all ol
       , mtl_system_items_b msib
       , mtl_units_of_measure_vl um
    WHERE ol.flow_status_code = ('AWAITING_SHIPPING')
    AND ol.line_type_id       IN (1002,1012)
        AND ol.split_from_line_id IS NOT NULL
      AND ol.header_id  = i_header_id
      AND ol.subinventory != 'PUBD' --Added for ver 3.5
      AND ol.inventory_item_id = msib.inventory_item_id
      AND ol.ship_from_org_id = msib.organization_id
      AND ol.order_quantity_uom = um.uom_code
      AND NOT EXISTS
      ( SELECT 'x'
       FROM XXWC.xxwc_wsh_shipping_stg
      WHERE line_id = ol.line_id
        AND status IN ('DELIVERED','OUT_FOR_DELIVERY')
      )
      AND NOT EXISTS
      ( SELECT 'x'
       FROM XXWC.xxwc_oe_open_order_lines
      WHERE line_id = ol.line_id
      );

  EXCEPTION WHEN OTHERS THEN
     l_return_status := 'E';
     o_return_status := 'E';
     o_return_msg := o_return_msg ||' Failed to Update xxwc_oe_open_order_lines';

  END ;
-- Added for Ver 3.4 End

IF l_return_status = 'S' then
 commit;
ELSE
 rollback;
END IF;
ELSE
  o_return_status := 'W';
  o_return_msg := 'No Lines Processed';
END IF;

END split_line;

procedure subinv_transfer_by_qty (  p_header_id in number
                                   , p_line_id in number
                                   , p_delivery_id in number
                                   , p_from_subinv in varchar2
                                   , p_to_subinv in varchar2
                                   , p_trx_qty  in number
                                   , p_cur_log_seq  in number
                                   , p_ret_status   out varchar
                                   , p_ret_msg  out varchar2
                                   , p_last_log_seq out number)
/*************************************************************************
  $Header xxwc_wsh_shipping_extn_pkg $
  Module Name: subinv_transfer_by_qty

  PURPOSE:   Perform subinventory Transfer

  REVISIONS:
  Ver        Date        Author                     Description
  ---------  ----------  ---------------         -------------------------
  1.0        01/21/2013  Consuelo Gonzalez       Initial Version
  3.5        03/09/2016  Manjula Chellappan      TMS 20150622-00156 - Shipping Extension modification for PUBD
**************************************************************************/
is
    cursor cur_transfer (p_header_id number, p_line_id number, p_delivery_id number) is
            select  x1.rowid rid
                    , x1.header_id
                    , x1.line_id
                    , x1.delivery_id
                    , x1.inventory_item_id
                    , x1.ship_from_org_id
                    , x1.transaction_qty
                    , msib.lot_control_code -- 1: no control, 2: full control
                    , msib.primary_uom_code  -- 03/27/13 CG: Added
                    , oh.order_number
                    , (ol.line_number||'.'||ol.shipment_number) line_num
          -- 10/02/2015 Ver 3.3 below two columns Added for TMS20150526-00214
          ,msib.stock_enabled_flag
          ,msib.mtl_transactions_enabled_flag
            from    xxwc_wsh_shipping_stg x1
                    , mtl_system_items_b msib
                    -- 03/27/13 CG: Added
                    , oe_order_lines ol
                    , oe_order_headers oh
            where   x1.header_id = p_header_id
            and     x1.line_id = p_line_id
            and     x1.delivery_id = p_delivery_id
            and     x1.inventory_item_id = msib.inventory_item_id
            and     x1.ship_from_org_id = msib.organization_id
            -- 03/27/13 CG: Added
            and     x1.line_id = ol.line_id
            and     x1.header_id = ol.header_Id
            and     ol.header_id = oh.header_id
            -- 09/25/2013 GC: TMS 20130910-00906: Added to address transactions with 0 qty
            and     nvl(x1.transaction_qty,0) > 0;


    cursor cur_reserved_lots (p_subinventory varchar2) is
            select  'Y'
                    , mr.reservation_id
                    , mr.lot_number
                    , mr.reservation_quantity
                    , mr.subinventory_code
                    , mr.demand_source_header_id
                    , mr.demand_source_line_id
            from    mtl_reservations mr
                    , mtl_sales_orders mso
                    , oe_order_headers oh
            where   mr.demand_source_type_id in (inv_reservation_global.g_source_type_oe, inv_reservation_global.g_source_type_internal_ord)
            and     mr.supply_source_type_id = inv_reservation_global.g_source_type_inv
            and     mr.demand_source_line_id = p_line_id
            and     mr.subinventory_code = p_subinventory
            and     mr.demand_source_header_id = mso.sales_order_id
            and     mso.segment3 = 'ORDER ENTRY'
            and     mso.segment1 = oh.order_number
            and     oh.header_id = p_header_id
            and     not exists (select  'Already Loaded'
                                from    XXWC.XXWC_WSH_SHIPPING_LOTS_STG x1
                                where   x1.reservation_id = mr.reservation_id);

    cursor cur_remove_lot_reservation is
            select  x1.rowid rid
                    , x1.header_id
                    , x1.line_id
                    , x1.delivery_id
                    , x1.inventory_item_id
                    , x1.ship_from_org_id
                    , x1.lot_number
                    , x1.lot_quantity
                    , mr.reservation_id
                    , mr.demand_source_header_id
                    , mr.demand_source_line_id
                    , mr.subinventory_code
                    , mr.reservation_quantity
            from    XXWC.XXWC_WSH_SHIPPING_LOTS_STG x1
                    , mtl_reservations mr
            where   x1.header_id = p_header_id
            and     x1.line_id = p_line_id
            and     x1.delivery_id = p_delivery_id
            and     x1.reservation_id = mr.reservation_id
            and     mr.subinventory_code = p_from_subinv;
            --and     x1.request_id = -9999;

    cursor cur_update_lot_reservation (i_header_id number, i_line_id number, i_delivery_id number, i_subinv_code varchar2) is
            select  x1.rowid rid
                    , x1.header_id
                    , x1.line_id
                    , x1.delivery_id
                    , x1.inventory_item_id
                    , x1.ship_from_org_id
                    , x1.lot_number
                    , mr.reservation_id
                    , mr.demand_source_name
                    , mr.demand_source_header_id
                    , mr.demand_source_line_id
                    , mr.subinventory_code
                    , mr.reservation_quantity
                    , sum(mr.reservation_quantity) running_reserve_qty
            from    XXWC.XXWC_WSH_SHIPPING_LOTS_STG x1
                    , mtl_reservations mr
            where   x1.header_id = i_header_id
            and     x1.line_id = i_line_id
            and     x1.delivery_id = i_delivery_id
            and     x1.reservation_id = mr.reservation_id
            and     mr.subinventory_code = i_subinv_code
            group by x1.rowid
                    , x1.header_id
                    , x1.line_id
                    , x1.delivery_id
                    , x1.inventory_item_id
                    , x1.ship_from_org_id
                    , x1.lot_number
                    , mr.reservation_id
                    , mr.demand_source_name
                    , mr.demand_source_header_id
                    , mr.demand_source_line_id
                    , mr.subinventory_code
                    , mr.reservation_quantity
            order by x1.rowid
                    , x1.header_id
                    , x1.line_id
                    , x1.delivery_id
                    , x1.inventory_item_id
                    , x1.ship_from_org_id
                    , x1.lot_number
                    , mr.reservation_id
                    , mr.demand_source_name
                    , mr.demand_source_header_id
                    , mr.demand_source_line_id
                    , mr.subinventory_code
                    , mr.reservation_quantity;

    cursor cur_transf_lots is
            select  x1.rowid rid
                    , x1.header_id
                    , x1.line_id
                    , x1.delivery_id
                    , x1.inventory_item_id
                    , x1.ship_from_org_id
                    , x1.lot_number cust_lot_number
                    , x1.lot_quantity
                    , mr.reservation_id
                    , mr.lot_number res_lot_number
                    , mr.reservation_quantity
            from    XXWC.XXWC_WSH_SHIPPING_LOTS_STG x1
                    , mtl_reservations mr
            where   x1.header_id = p_header_id
            and     x1.line_id = p_line_id
            and     x1.delivery_id = p_delivery_id
            and     x1.reservation_id = mr.reservation_id(+);
            -- and     x1.reservation_id is null;

    l_log_sequence          number;

    -- Reservation Handling
    l_rsv               inv_reservation_global.mtl_reservation_rec_type;
    l_new_rsv_rec       INV_RESERVATION_GLOBAL.MTL_RESERVATION_REC_TYPE;
    l_serial_number     INV_RESERVATION_GLOBAL.SERIAL_NUMBER_TBL_TYPE;
    l_new_serial_number INV_RESERVATION_GLOBAL.SERIAL_NUMBER_TBL_TYPE;
    l_validation_flag   VARCHAR2(2) := FND_API.G_TRUE;
    l_msg_count NUMBER;
    l_rsv_id    NUMBER;
    l_dummy_sn  inv_reservation_global.serial_number_tbl_type;
    l_status    VARCHAR2(1);
    g_miss_rsv_rec                  inv_reservation_global.mtl_reservation_rec_type;
    g_miss_serial_number            inv_reservation_global.serial_number_tbl_type;
    g_miss_mtl_reservations_tbl     inv_reservation_global.mtl_reservation_tbl_type;

    -- Subinventory Transfer
    l_transaction_header_id      NUMBER;
    l_interface_transaction_id   NUMBER;
    l_count_serials              NUMBER;
    l_transaction_type_id        NUMBER;
    l_transaction_uom            VARCHAR2 (3);
    l_transaction_qty            NUMBER;
    l_gral_transaction_qty       NUMBER;
    l_processing_return          NUMBER;
    l_return_status              VARCHAR2 (240);
    l_msg_data                   VARCHAR2 (4000);
    l_trans_count                NUMBER;
    l_dummy_num                  NUMBER;
    l_new_remaining_qty          NUMBER;

begin
    l_log_sequence := p_cur_log_seq;
    write_log ( p_header_id
                , p_line_id
                , substr('Starting subinv_transfer_by_qty for: '||p_line_id, 1, 4000)
                , p_cur_log_seq  + 1);

    for c1 in cur_transfer (p_header_id, p_line_id, p_delivery_id )
    loop
        exit when cur_transfer%notfound;

        if c1.lot_control_code = 2 then

--          if p_from_subinv = 'General' and p_to_subinv = 'GeneralPCK' -- Commented for Ver 3.5
            if p_from_subinv IN ('General','PUBD') and p_to_subinv = 'GeneralPCK' -- Added for Ver 3.5
            then

                -- Loop to add new lot reservations
                for c3 in cur_reserved_lots (p_from_subinv)
                loop
                    exit when cur_reserved_lots%notfound;

                    begin
                        insert into XXWC.XXWC_WSH_SHIPPING_LOTS_STG (
                            header_id
                            , line_id
                            , delivery_id
                            , inventory_item_id
                            , ship_from_org_id
                            , lot_number
                            , lot_quantity
                            , reservation_id
                            , created_by
                            , creation_date
                            , last_updated_by
                            , last_update_date
                            , last_update_login
                            , request_id
                        ) values (
                            p_header_id
                            , p_line_id
                            , c1.delivery_id -- 03/27/13 CG: Commented to prevent cross delivery issues when splitting to diff deliveries p_delivery_id
                            , c1.inventory_item_id
                            , c1.ship_from_org_id
                            , c3.lot_number
                            , c3.reservation_quantity
                            , c3.reservation_id
                            , 0
                            , sysdate
                            , 0
                            , sysdate
                            , 0
                            , -9999
                        );

                        -- Moved call to for c2 in cur_remove_lot_reservation to be outside this loop 03/18/2013

                    exception
                    when others then
                        l_log_sequence := l_log_sequence + 1;
                        write_log ( c1.header_id
                                    , c1.line_id
                                    , substr('Could not insert into lots custom table' , 1, 4000)
                                    , l_log_sequence);
                    end;

                END LOOP;

                for c2 in cur_remove_lot_reservation
                loop
                    exit when cur_remove_lot_reservation%notfound;

                    -- before removing reservation verify lot quantity
                    if c2.lot_quantity != c2.reservation_quantity then

                        l_log_sequence := l_log_sequence + 1;
                        write_log ( c1.header_id
                                    , c1.line_id
                                    , substr('lot_quantity: '||c2.lot_quantity||'. reservation_quantity: '||c2.reservation_quantity||'. Updating Lot Quantity in Staging Table.', 1, 4000)
                                    , l_log_sequence);

                        begin
                            update  XXWC.XXWC_WSH_SHIPPING_LOTS_STG
                            set     lot_quantity = c2.reservation_quantity
                            where   rowid = c2.rid;
                        exception
                        when others then
                            null;
                        end;

                    end if;

                    l_log_sequence := l_log_sequence + 1;
                    write_log ( c1.header_id
                                , c1.line_id
                                , substr('Deleting reservation: '||c2.reservation_id, 1, 4000)
                                , l_log_sequence);

                    l_rsv := g_miss_rsv_rec;

                    l_rsv.demand_source_type_id        := inv_reservation_global.g_source_type_oe; -- order entry
                    l_rsv.supply_source_type_id        := inv_reservation_global.g_source_type_inv;
                    l_rsv.reservation_id               := c2.reservation_id;
                    l_rsv.demand_source_header_id      := c2.demand_source_header_id;
                    l_rsv.demand_source_line_id        := c2.demand_source_line_id;
                    l_rsv.organization_id              := c1.inventory_item_id;
                    l_rsv.inventory_item_id            := c1.ship_from_org_id;
                    l_rsv.subinventory_code            := c2.subinventory_code;

                    inv_reservation_pub.delete_reservation (
                                                               p_api_version_number        => 1.0
                                                             , p_init_msg_lst              => fnd_api.g_true
                                                             , x_return_status             => l_status
                                                             , x_msg_count                 => l_msg_count
                                                             , x_msg_data                  => l_msg_data
                                                             , p_rsv_rec                   => l_rsv
                                                             , p_serial_number             => l_dummy_sn
                                                           );

                    l_log_sequence := l_log_sequence + 1;
                    write_log ( c1.header_id
                                , c1.line_id
                                , substr('l_status: '||l_status||'. l_msg_count: '||l_msg_count , 1, 4000)
                                , l_log_sequence);

                    IF l_status = fnd_api.g_ret_sts_success THEN
                        l_log_sequence := l_log_sequence + 1;
                        write_log ( c1.header_id
                                    , c1.line_id
                                    , substr('Reservation deleted: '||c2.reservation_id, 1, 4000)
                                    , l_log_sequence);

                        -- Removing old reservation id
                        begin
                            update  XXWC.XXWC_WSH_SHIPPING_LOTS_STG
                            set     reservation_id = null
                            where   rowid = c2.rid;
                        exception
                        when others then
                            null;
                        end;
                    ELSE
                        IF l_msg_count = 1 THEN
                            l_log_sequence := l_log_sequence + 1;
                            write_log ( c1.header_id
                                        , c1.line_id
                                        , substr('Error Deleting reservation: '||c2.reservation_id||'. l_msg_data: '||l_msg_data, 1, 4000)
                                        , l_log_sequence);
                        ELSE
                            FOR l_index IN 1..l_msg_count LOOP
                                fnd_msg_pub.get(l_index, 'T', l_msg_data, l_dummy_num);
                                l_log_sequence := l_log_sequence + 1;
                                write_log ( c1.header_id
                                            , c1.line_id
                                            , substr('Error Deleting reservation: '||c2.reservation_id||'.l_msg_data: '||l_msg_data, 1, 4000)
                                            , l_log_sequence);
                            END LOOP;
                        END IF;
                    END IF;

                END LOOP;

            ELSE

                -- Moving from GeneralPCK to General so only updating a reservation quantity and subinventory transferring
                -- When quantity is reduced it reduces the reservation quantity immediately.
                null;
                /*l_log_sequence := l_log_sequence + 1;
                write_log ( c1.header_id
                            , c1.line_id
                            , substr('Transferring from GeneralPCK to General...Updating reservation quantity', 1, 4000)
                            , l_log_sequence);

                l_new_remaining_qty := p_trx_qty;
                for c2 in cur_update_lot_reservation (c1.header_id, c1.line_id, c1.delivery_id, p_from_subinv)
                loop
                    exit when cur_update_lot_reservation%notfound;

                    l_rsv := g_miss_rsv_rec;
                    l_new_rsv_rec := g_miss_rsv_rec;

                    l_log_sequence := l_log_sequence + 1;
                    write_log ( c1.header_id
                                , c1.line_id
                                , substr('c2.reservation_id '||c2.reservation_id||'. c2.reservation_quantity: '||c2.reservation_quantity||'. l_new_remaining_qty: '||l_new_remaining_qty, 1, 4000)
                                , l_log_sequence);

                    if l_new_remaining_qty > 0 then

                        if c2.reservation_quantity <= l_new_remaining_qty then

                            l_rsv.reservation_id       := c2.reservation_id;
                            l_rsv.reservation_quantity := c2.reservation_quantity;

                            l_new_rsv_rec.reservation_id       := c2.reservation_id;
                            l_new_rsv_rec.reservation_quantity := 0;

                            l_new_remaining_qty := l_new_remaining_qty - c2.reservation_id;

                        else

                            l_rsv.reservation_id       := c2.reservation_id;
                            l_rsv.reservation_quantity := c2.reservation_quantity;

                            l_new_rsv_rec.reservation_id       := c2.reservation_id;
                            l_new_rsv_rec.reservation_quantity := (c2.reservation_quantity - l_new_remaining_qty);

                            l_new_remaining_qty := 0;

                        end if;

                        l_log_sequence := l_log_sequence + 1;
                        write_log ( c1.header_id
                                    , c1.line_id
                                    , substr('l_new_rsv_rec.reservation_quantity: '||l_new_rsv_rec.reservation_quantity, 1, 4000)
                                    , l_log_sequence);

                        l_log_sequence := l_log_sequence + 1;
                        write_log ( c1.header_id
                                    , c1.line_id
                                    , substr('After API assignment l_new_remaining_qty: '||l_new_remaining_qty||'. Calling API to update reservation', 1, 4000)
                                    , l_log_sequence);

                        INV_RESERVATION_PUB.UPDATE_RESERVATION(
                                                                    P_API_VERSION_NUMBER    =>  1.0
                                                                  , P_INIT_MSG_LST          =>  fnd_api.g_true
                                                                  , X_RETURN_STATUS         =>  l_status
                                                                  , X_MSG_COUNT             =>  l_msg_count
                                                                  , X_MSG_DATA              =>  l_msg_data
                                                                  , P_ORIGINAL_RSV_REC      =>  l_rsv
                                                                  , P_TO_RSV_REC            =>  l_new_rsv_rec
                                                                  , P_ORIGINAL_SERIAL_NUMBER=>  l_serial_number
                                                                  , P_TO_SERIAL_NUMBER      =>  l_new_serial_number
                                                                  , P_VALIDATION_FLAG       =>  l_validation_flag
                                                                  , P_CHECK_AVAILABILITY    =>  FND_API.G_FALSE
                                                            );

                        IF l_status = fnd_api.g_ret_sts_success THEN
                            l_log_sequence := l_log_sequence + 1;
                            write_log ( c1.header_id
                                        , c1.line_id
                                        , substr('Updated Reservation: '||c2.reservation_id, 1, 4000)
                                        , l_log_sequence);

                            -- Removing old reservation id
                            begin
                                update  XXWC.XXWC_WSH_SHIPPING_LOTS_STG
                                set     lot_quantity = l_new_rsv_rec.reservation_quantity
                                where   rowid = c2.rid;
                            exception
                            when others then
                                null;
                            end;

                        ELSE
                            IF l_msg_count = 1 THEN
                                l_log_sequence := l_log_sequence + 1;
                                write_log ( c1.header_id
                                            , c1.line_id
                                            , substr('Error updating reservation: '||c2.reservation_id||'. l_msg_data: '||l_msg_data, 1, 4000)
                                            , l_log_sequence);
                            ELSE
                                FOR l_index IN 1..l_msg_count LOOP
                                    fnd_msg_pub.get(l_index, 'T', l_msg_data, l_dummy_num);
                                    l_log_sequence := l_log_sequence + 1;
                                    write_log ( c1.header_id
                                                , c1.line_id
                                                , substr('Error updating reservation: '||c2.reservation_id||'.l_msg_data: '||l_msg_data, 1, 4000)
                                                , l_log_sequence);
                                END LOOP;
                            END IF;
                        END IF;

                    end if;

                end loop; */

            END IF;

        END IF;

        -- 5. Perform Subinventory Transfer
        -- Finding Transaction Type ID
        BEGIN
           SELECT transaction_type_id
             INTO l_transaction_type_id
             FROM mtl_transaction_types
            WHERE transaction_type_name = 'Subinventory Transfer';
        EXCEPTION
           WHEN OTHERS THEN
              dbms_output.put_line('Err Could not find transaction type information for Subinventory Transfer');
        END;

        SELECT mtl_material_transactions_s.NEXTVAL
        INTO l_transaction_header_id
        FROM DUAL;

        SELECT mtl_material_transactions_s.NEXTVAL
        INTO l_interface_transaction_id
        FROM DUAL;

        l_log_sequence := l_log_sequence + 1;
        write_log ( c1.header_id
                    , c1.line_id
                    , substr('Inserting into MTI...', 1, 4000)
                    , l_log_sequence);

    -- Added IF condition for Ver3.3 on 10/02/2015
    IF c1.stock_enabled_flag='Y' AND c1.mtl_transactions_enabled_flag='Y' THEN

        BEGIN
            INSERT INTO mtl_transactions_interface
                   (transaction_interface_id,
                    transaction_header_id,
                    source_code,
                    source_line_id,
                    source_header_id,
                    process_flag,
                    transaction_mode,
                    lock_flag,
                    last_update_date,
                    last_updated_by,
                    creation_date,
                    created_by,
                    transaction_type_id,
                    transaction_date,
                    transaction_uom,
                    inventory_item_id,
                    subinventory_code,
                    organization_id,
                    transfer_subinventory,
                    transfer_organization,
                    transaction_quantity,
                    primary_quantity,
                    -- 03/27/13 CG: Added
                    transaction_reference
                   )
            VALUES (l_interface_transaction_id,
                    l_transaction_header_id,
                    'Subinventory Transfer',        --source code
                    l_interface_transaction_id,     --source line id
                    l_interface_transaction_id,     --source header id
                    1,                              --process flag
                    3,                              --transaction mode
                    2,                              --lock flag
                    SYSDATE,                        --last update date
                    0,                              --last updated by
                    SYSDATE,                        --created date
                    0,                              --created by
                    l_transaction_type_id,          -- transaction type
                    SYSDATE,                        -- transaction date
                    c1.primary_uom_code,            -- uom
                    c1.inventory_item_id,           -- inventory item id
                    p_from_subinv,                  -- from subinventory
                    c1.ship_from_org_id,
                    p_to_subinv,                    -- to subinventory
                    c1.ship_from_org_id,
                    p_trx_qty,
                    p_trx_qty,
                    -- 03/27/13 CG: Added
                    (c1.order_number||'-'||c1.line_num)
            );
        COMMIT;--Added on 11/13/2013 Ram Talluri and Rasikha Galimova TMS 20131009-00330
        EXCEPTION
        WHEN OTHERS THEN
            dbms_output.put_line('Could not insert into MTI');
        END;

    END IF;  -- Added End IF condition for Ver3.3 on 10/02/2015

        if c1.lot_control_code = 2 then

            l_log_sequence := l_log_sequence + 1;
            write_log ( c1.header_id
                        , c1.line_id
                        , substr('Inserting into MTLI', 1, 4000)
                        , l_log_sequence);

            l_log_sequence := l_log_sequence + 1;
            write_log ( c1.header_id
                        , c1.line_id
                        , substr('p_trx_qty: '||p_trx_qty, 1, 4000)
                        , l_log_sequence);

            l_new_remaining_qty := p_trx_qty;
            l_transaction_qty   := 0;
            for c2 in cur_transf_lots
            loop
                exit when cur_transf_lots%notfound;

                l_log_sequence := l_log_sequence + 1;
                write_log ( c1.header_id
                            , c1.line_id
                            , substr('Reservation: '||c2.reservation_id||'. l_new_remaining_qty: '||l_new_remaining_qty, 1, 4000)
                            , l_log_sequence);

                if l_new_remaining_qty > 0 then

                    if c2.reservation_id is not null then

                        if c2.lot_quantity > nvl(c2.reservation_quantity,0) then
                            l_transaction_qty := c2.lot_quantity - nvl(c2.reservation_quantity,0);
                            l_new_remaining_qty := l_new_remaining_qty - (c2.lot_quantity - nvl(c2.reservation_quantity,0));
                        else
                            l_new_remaining_qty := 0;
                        end if;

                    else

                        if c2.lot_quantity < l_new_remaining_qty then
                            l_transaction_qty := c2.lot_quantity;
                            l_new_remaining_qty := l_new_remaining_qty - c2.lot_quantity;
                        elsif c2.lot_quantity = l_new_remaining_qty then
                            l_transaction_qty := c2.lot_quantity;
                            l_new_remaining_qty := l_new_remaining_qty - c2.lot_quantity;
                        else
                            l_transaction_qty := l_new_remaining_qty;
                            l_new_remaining_qty := 0;
                        end if;

                    end if;

                    l_log_sequence := l_log_sequence + 1;
                    write_log ( c1.header_id
                                , c1.line_id
                                , substr('l_transaction_qty: '||l_transaction_qty||'. l_new_remaining_qty: '||l_new_remaining_qty, 1, 4000)
                                , l_log_sequence);

                    if l_transaction_qty > 0 then

                        begin
                            INSERT INTO mtl_transaction_lots_interface
                                    (transaction_interface_id,
                                     last_update_date,
                                     last_updated_by,
                                     creation_date,
                                     created_by,
                                     lot_number,
                                     transaction_quantity
                                    )
                            VALUES (l_interface_transaction_id,
                                     SYSDATE,               --last update date
                                     0,              --last updated by
                                     SYSDATE,                   --created date
                                     0,                      --created
                                     c2.cust_lot_number,
                                     l_transaction_qty -- c2.lot_quantity
                            );
                        COMMIT;--Added on 11/13/2013 Ram Talluri and Rasikha Galimova TMS 20131009-00330
                        exception
                        when others then
                            dbms_output.put_line('Could not insert into MTLI');
                        end;

                    end if;

                end if;

            END LOOP;

        end if;

        l_log_sequence := l_log_sequence + 1;
        write_log ( c1.header_id
                    , c1.line_id
                    , substr('Calling inv trx manager processor...', 1, 4000)
                    , l_log_sequence);

        -- Calling API to transact record
        l_processing_return := NULL;
        l_return_status := NULL;
        l_msg_count := NULL;
        l_msg_data := NULL;
        l_trans_count := NULL;
        l_processing_return := inv_txn_manager_pub.process_transactions (p_api_version           => 1.0,
                                                                         p_init_msg_list         => fnd_api.g_true,
                                                                         p_commit                => fnd_api.g_true,
                                                                         p_validation_level      => fnd_api.g_valid_level_full,
                                                                         x_return_status         => l_return_status,
                                                                         x_msg_count             => l_msg_count,
                                                                         x_msg_data              => l_msg_data,
                                                                         x_trans_count           => l_trans_count,
                                                                         p_table                 => 1,
                                                                         p_header_id             => l_transaction_header_id
                                                                        );

        dbms_output.put_line('  l_processing_return: ' || l_processing_return);
        dbms_output.put_line('  l_return_status: ' || l_return_status);
        dbms_output.put_line('  l_msg_count: ' || l_msg_count);
        dbms_output.put_line('  l_msg_data: ' || l_msg_data);
        dbms_output.put_line('  l_trans_count: ' || l_trans_count);
        COMMIT;--Added on 11/13/2013 Ram Talluri and Rasikha Galimova TMS 20131009-00330

        IF l_return_status <> fnd_api.g_ret_sts_success then

            -- 04/17/2013: CG Added temporarily to log inventory manager failed transactions
            begin
                insert into xxwc.xxwc_mti_log_bkp
                select  *
                from mtl_transactions_interface
                where  transaction_interface_id = l_interface_transaction_id;
            exception
            when others then
                null;
            end;

--            begin
--                insert into xxwc.xxwc_mtli_log_bkp
--                select  *
--                from mtl_transaction_lots_interface
--                where  transaction_interface_id = l_interface_transaction_id;
--            exception
--            when others then
--                null;
--            end;

            IF l_msg_count = 1 THEN
                begin
                    l_log_sequence := l_log_sequence + 1;
                    insert into xxwc.xxwc_wsh_shipping_log (
                            header_id
                            , line_id
                            , log_message
                            , creation_date
                            , log_sequence
                        ) values (
                            c1.header_id
                            , c1.line_id
                            , substr('Inv Mgr Err. '||replace(l_msg_data, chr(10), ' '), 1, 4000)
                            , sysdate
                            , l_log_sequence
                        );
                exception
                when others then
                    null;
                end;
            ELSE
                FOR l_index IN 1..l_msg_count
                LOOP
                    fnd_msg_pub.get(l_index, 'T', l_msg_data, l_dummy_num);

                    begin
                        l_log_sequence := l_log_sequence + 1;
                        insert into xxwc.xxwc_wsh_shipping_log (
                                header_id
                                , line_id
                                , log_message
                                , creation_date
                                , log_sequence
                            ) values (
                                c1.header_id
                                , c1.line_id
                                , substr('Inv Mgr Err. '||replace(l_msg_data, chr(10), ' '), 1, 4000)
                                , sysdate
                                , l_log_sequence
                            );
                    exception
                    when others then
                        null;
                    end;
                END LOOP;
            END IF;

            -- 04/17/2013

            -- clear out interface table for errored records
            begin
                delete from mtl_transactions_interface
                where  transaction_interface_id = l_interface_transaction_id;
                COMMIT;--Added on 11/13/2013 Ram Talluri and Rasikha Galimova TMS 20131009-00330
            exception
            when others then
                null;
            end;

            begin
                delete from mtl_transaction_lots_interface
                where  transaction_interface_id = l_interface_transaction_id;
                COMMIT;--Added on 11/13/2013 Ram Talluri and Rasikha Galimova TMS 20131009-00330
            exception
            when others then
                null;
            end;

        else

            if c1.lot_control_code = 2 then

                for c2 in cur_transf_lots
                loop
                    exit when cur_transf_lots%notfound;

                    l_log_sequence := l_log_sequence + 1;
                    write_log ( c1.header_id
                                , c1.line_id
                                , substr('Reservation: '||c2.reservation_id||'. c2.lot_quantity: '||c2.lot_quantity||'. c2.reservation_quantity: '||c2.reservation_quantity, 1, 4000)
                                , l_log_sequence);

                    if c2.reservation_id is not null then

                        --if c2.lot_quantity > nvl(c2.reservation_quantity,0) then
                        --    l_transaction_qty := c2.lot_quantity - nvl(c2.reservation_quantity,0);

                            l_log_sequence := l_log_sequence + 1;
                            write_log ( c1.header_id
                                        , c1.line_id
                                        , substr('New Lot Quantity: '||c2.reservation_quantity, 1, 4000)
                                        , l_log_sequence);

                            begin
                                update  XXWC.XXWC_WSH_SHIPPING_LOTS_STG
                                set     lot_quantity = c2.reservation_quantity
                                where   rowid = c2.rid;
                            exception
                            when others then
                                l_log_sequence := l_log_sequence + 1;
                                write_log ( c1.header_id
                                            , c1.line_id
                                            , substr('Could not update lot staging table '||c1.line_id, 1, 4000)
                                            , l_log_sequence);
                            end;

                        --else
                        --    l_new_remaining_qty := 0;
                        --end if;

                    end if;

                END LOOP;
            end if;

        end if;

        l_log_sequence := l_log_sequence + 1;
        write_log ( c1.header_id
                    , c1.line_id
                    , substr('l_processing_return: ' || l_processing_return||'. l_return_status: ' || l_return_status||'. l_msg_count: ' || l_msg_count||'. l_trans_count: ' || l_trans_count , 1, 4000)
                    , l_log_sequence);

        p_ret_status    := l_return_status;
        p_ret_msg       := l_msg_data;
        p_last_log_seq  := l_log_sequence;

    end loop;

exception
when others then
    p_ret_status    := null;
    p_ret_msg       := null;
    l_msg_data      := substr(SQLERRM, 1, 4000);
    write_log ( p_header_id
                , 99999999999999999
                , substr('Error in subinventory_transfer: '||l_msg_data, 1, 4000)
                , p_cur_log_seq  + 1);
end subinv_transfer_by_qty;

procedure subinventory_transfer (  p_header_id in number
                                   , p_line_id in number
                                   , p_delivery_id in number
                                   , p_from_subinv in varchar2
                                   , p_to_subinv in varchar2
                                   , p_cur_log_seq  in number
                                   , p_ret_status   out varchar
                                   , p_ret_msg  out varchar2
                                   , p_last_log_seq out number)
is
    cursor cur_transfer (p_header_id number, p_line_id number, p_delivery_id number) is
            select  x1.rowid rid
                    , x1.header_id
                    , x1.line_id
                    , x1.delivery_id
                    , x1.inventory_item_id
                    , x1.ship_from_org_id
                    , x1.transaction_qty
                    , msib.lot_control_code -- 1: no control, 2: full control
                    , msib.primary_uom_code
                    -- 03/27/13 CG: Added
                    , oh.order_number
                    , (ol.line_number||'.'||ol.shipment_number) line_num
          -- 10/02/2015 Ver 3.3 below two columns Added for TMS20150526-00214
          ,msib.stock_enabled_flag
          ,msib.mtl_transactions_enabled_flag
            from    xxwc_wsh_shipping_stg x1
                    , mtl_system_items_b msib
                    -- 03/27/13 CG: Added
                    , oe_order_lines ol
                    , oe_order_headers oh
            where   x1.header_id = p_header_id
            and     x1.line_id = p_line_id
            and     x1.delivery_id = p_delivery_id
            and     x1.inventory_item_id = msib.inventory_item_id
            and     x1.ship_from_org_id = msib.organization_id
            -- 03/27/13 CG: Added
            and     x1.line_id = ol.line_id
            and     x1.header_id = ol.header_Id
            and     ol.header_id = oh.header_id
            -- 09/25/2013 GC: TMS 20130910-00906: Added to address transactions with 0 qty
            and     nvl(x1.transaction_qty,0) > 0;

    cursor cur_remove_lot_reservation (p_header_id number, p_line_id number, p_delivery_id number) is
            select  x1.rowid rid
                    , x1.header_id
                    , x1.line_id
                    , x1.delivery_id
                    , x1.inventory_item_id
                    , x1.ship_from_org_id
                    , x1.lot_number
                    , mr.reservation_id
                    , mr.demand_source_header_id
                    , mr.demand_source_line_id
                    , mr.subinventory_code
                    , mr.reservation_quantity
            from    XXWC.XXWC_WSH_SHIPPING_LOTS_STG x1
                    , mtl_reservations mr
            where   x1.header_id = p_header_id
            and     x1.line_id = p_line_id
            and     x1.delivery_id = p_delivery_id
            and     x1.reservation_id = mr.reservation_id
            and     mr.subinventory_code = p_from_subinv;

    cursor cur_transf_lots (p_header_id number, p_line_id number, p_delivery_id number) is
            select  x1.rowid rid
                    , x1.*
            from    XXWC.XXWC_WSH_SHIPPING_LOTS_STG x1
            where   x1.header_id = p_header_id
            and     x1.line_id = p_line_id
            and     x1.delivery_id = p_delivery_id
            and     x1.reservation_id is null;

    l_log_sequence          number;

    -- Reservation Handling
    l_rsv       inv_reservation_global.mtl_reservation_rec_type;
    l_msg_count NUMBER;
    l_rsv_id    NUMBER;
    l_dummy_sn  inv_reservation_global.serial_number_tbl_type;
    l_status    VARCHAR2(1);
    g_miss_rsv_rec                  inv_reservation_global.mtl_reservation_rec_type;
    g_miss_serial_number            inv_reservation_global.serial_number_tbl_type;
    g_miss_mtl_reservations_tbl     inv_reservation_global.mtl_reservation_tbl_type;

    -- Subinventory Transfer
    l_transaction_header_id      NUMBER;
    l_interface_transaction_id   NUMBER;
    l_count_serials              NUMBER;
    l_transaction_type_id        NUMBER;
    l_transaction_uom            VARCHAR2 (3);
    l_transaction_qty            NUMBER;
    l_gral_transaction_qty       NUMBER;
    l_processing_return          NUMBER;
    l_return_status              VARCHAR2 (240);
    l_msg_data                   VARCHAR2 (4000);
    l_trans_count                NUMBER;
    l_dummy_num                  NUMBER;

begin
    l_log_sequence := p_cur_log_seq;
    write_log ( p_header_id
                , p_line_id
                , substr('Starting subinventory_transfer for: '||p_line_id, 1, 4000)
                , p_cur_log_seq  + 1);

    for c1 in cur_transfer (p_header_id, p_line_id, p_delivery_id )
    loop
        exit when cur_transfer%notfound;

        if c1.lot_control_code = 2 then

            l_log_sequence := l_log_sequence + 1;
            write_log ( c1.header_id
                        , c1.line_id
                        , substr('Lot controlled item...', 1, 4000)
                        , l_log_sequence);

            for c2 in cur_remove_lot_reservation (p_header_id, p_line_id, p_delivery_id )
            loop
                exit when cur_remove_lot_reservation%notfound;

                l_log_sequence := l_log_sequence + 1;
                write_log ( c1.header_id
                            , c1.line_id
                            , substr('Deleting reservation: '||c2.reservation_id, 1, 4000)
                            , l_log_sequence);

                l_rsv := g_miss_rsv_rec;

                l_rsv.demand_source_type_id        := inv_reservation_global.g_source_type_oe; -- order entry
                l_rsv.supply_source_type_id        := inv_reservation_global.g_source_type_inv;
                l_rsv.reservation_id               := c2.reservation_id;
                l_rsv.demand_source_header_id      := c2.demand_source_header_id;
                l_rsv.demand_source_line_id        := c2.demand_source_line_id;
                l_rsv.organization_id              := c1.inventory_item_id;
                l_rsv.inventory_item_id            := c1.ship_from_org_id;
                l_rsv.subinventory_code            := c2.subinventory_code;

                inv_reservation_pub.delete_reservation (
                                                           p_api_version_number        => 1.0
                                                         , p_init_msg_lst              => fnd_api.g_true
                                                         , x_return_status             => l_status
                                                         , x_msg_count                 => l_msg_count
                                                         , x_msg_data                  => l_msg_data
                                                         , p_rsv_rec                   => l_rsv
                                                         , p_serial_number             => l_dummy_sn
                                                       );

                l_log_sequence := l_log_sequence + 1;
                write_log ( c1.header_id
                            , c1.line_id
                            , substr('l_status: '||l_status||'. l_msg_count: '||l_msg_count , 1, 4000)
                            , l_log_sequence);

                IF l_status = fnd_api.g_ret_sts_success THEN
                    l_log_sequence := l_log_sequence + 1;
                    write_log ( c1.header_id
                                , c1.line_id
                                , substr('Reservation deleted: '||c2.reservation_id, 1, 4000)
                                , l_log_sequence);

                    -- Removing old reservation id
                    begin
                        update  XXWC.XXWC_WSH_SHIPPING_LOTS_STG
                        set     reservation_id = null
                        where   rowid = c2.rid;
                    exception
                    when others then
                        null;
                    end;
                ELSE
                    IF l_msg_count = 1 THEN
                        l_log_sequence := l_log_sequence + 1;
                        write_log ( c1.header_id
                                    , c1.line_id
                                    , substr('Error Deleting reservation: '||c2.reservation_id||'. l_msg_data: '||l_msg_data, 1, 4000)
                                    , l_log_sequence);
                    ELSE
                        FOR l_index IN 1..l_msg_count LOOP
                            fnd_msg_pub.get(l_index, 'T', l_msg_data, l_dummy_num);
                            l_log_sequence := l_log_sequence + 1;
                            write_log ( c1.header_id
                                        , c1.line_id
                                        , substr('Error Deleting reservation: '||c2.reservation_id||'.l_msg_data: '||l_msg_data, 1, 4000)
                                        , l_log_sequence);
                        END LOOP;
                    END IF;
                END IF;

            END LOOP;

        END IF;

        -- 5. Perform Subinventory Transfer
        -- Finding Transaction Type ID
        BEGIN
           SELECT transaction_type_id
             INTO l_transaction_type_id
             FROM mtl_transaction_types
            WHERE transaction_type_name = 'Subinventory Transfer';
        EXCEPTION
           WHEN OTHERS THEN
              dbms_output.put_line('Err Could not find transaction type information for Subinventory Transfer');
        END;

        SELECT mtl_material_transactions_s.NEXTVAL
        INTO l_transaction_header_id
        FROM DUAL;

        SELECT mtl_material_transactions_s.NEXTVAL
        INTO l_interface_transaction_id
        FROM DUAL;

        l_log_sequence := l_log_sequence + 1;
        write_log ( c1.header_id
                    , c1.line_id
                    , substr('Inserting into MTI...', 1, 4000)
                    , l_log_sequence);
     -- Added IF condition for Ver3.3 on 10/02/2015
    IF c1.stock_enabled_flag='Y' AND c1.mtl_transactions_enabled_flag='Y' THEN

        BEGIN
            INSERT INTO mtl_transactions_interface
                   (transaction_interface_id,
                    transaction_header_id,
                    source_code,
                    source_line_id,
                    source_header_id,
                    process_flag,
                    transaction_mode,
                    lock_flag,
                    last_update_date,
                    last_updated_by,
                    creation_date,
                    created_by,
                    transaction_type_id,
                    transaction_date,
                    transaction_uom,
                    inventory_item_id,
                    subinventory_code,
                    organization_id,
                    transfer_subinventory,
                    transfer_organization,
                    transaction_quantity,
                    primary_quantity,
                    -- 03/27/13 CG: Added
                    transaction_reference
                   )
            VALUES (l_interface_transaction_id,
                    l_transaction_header_id,
                    'Subinventory Transfer',        --source code
                    l_interface_transaction_id,     --source line id
                    l_interface_transaction_id,     --source header id
                    1,                              --process flag
                    3,                              --transaction mode
                    2,                              --lock flag
                    SYSDATE,                        --last update date
                    0,                              --last updated by
                    SYSDATE,                        --created date
                    0,                              --created by
                    l_transaction_type_id,          -- transaction type
                    SYSDATE,                        -- transaction date
                    c1.primary_uom_code,            -- uom
                    c1.inventory_item_id,           -- inventory item id
                    p_from_subinv,                  -- from subinventory
                    c1.ship_from_org_id,
                    p_to_subinv,                    -- to subinventory
                    c1.ship_from_org_id,
                    c1.transaction_qty,
                    c1.transaction_qty,
                    -- 03/27/13 CG: Added
                    (c1.order_number||'-'||c1.line_num)
            );
        COMMIT;--Added on 11/13/2013 Ram Talluri and Rasikha Galimova TMS 20131009-00330
        EXCEPTION
        WHEN OTHERS THEN
            dbms_output.put_line('Could not insert into MTI');
        END;

       END IF;  -- Added END IF condition for Ver3.3 on 10/02/2015

        if c1.lot_control_code = 2 then

            l_log_sequence := l_log_sequence + 1;
            write_log ( c1.header_id
                        , c1.line_id
                        , substr('Inserting into MTLI', 1, 4000)
                        , l_log_sequence);

            for c2 in cur_transf_lots (p_header_id, p_line_id, p_delivery_id )
            loop
                exit when cur_transf_lots%notfound;

                begin
                    INSERT INTO mtl_transaction_lots_interface
                            (transaction_interface_id,
                             last_update_date,
                             last_updated_by,
                             creation_date,
                             created_by,
                             lot_number,
                             transaction_quantity
                            )
                    VALUES (l_interface_transaction_id,
                             SYSDATE,               --last update date
                             0,              --last updated by
                             SYSDATE,                   --created date
                             0,                      --created
                             c2.lot_number,
                             c2.lot_quantity
                    );
                COMMIT;--Added on 11/13/2013 Ram Talluri and Rasikha Galimova TMS 20131009-00330
                exception
                when others then
                    dbms_output.put_line('Could not insert into MTLI');
                end;

            END LOOP;

        end if;

        l_log_sequence := l_log_sequence + 1;
        write_log ( c1.header_id
                    , c1.line_id
                    , substr('Calling inv trx manager processor...', 1, 4000)
                    , l_log_sequence);

        -- Calling API to transact record
        l_processing_return := NULL;
        l_return_status := NULL;
        l_msg_count := NULL;
        l_msg_data := NULL;
        l_trans_count := NULL;
        l_processing_return := inv_txn_manager_pub.process_transactions (p_api_version           => 1.0,
                                                                         p_init_msg_list         => fnd_api.g_true,
                                                                         p_commit                => fnd_api.g_true,
                                                                         p_validation_level      => fnd_api.g_valid_level_full,
                                                                         x_return_status         => l_return_status,
                                                                         x_msg_count             => l_msg_count,
                                                                         x_msg_data              => l_msg_data,
                                                                         x_trans_count           => l_trans_count,
                                                                         p_table                 => 1,
                                                                         p_header_id             => l_transaction_header_id
                                                                        );

        dbms_output.put_line('  l_processing_return: ' || l_processing_return);
        dbms_output.put_line('  l_return_status: ' || l_return_status);
        dbms_output.put_line('  l_msg_count: ' || l_msg_count);
        dbms_output.put_line('  l_msg_data: ' || l_msg_data);
        dbms_output.put_line('  l_trans_count: ' || l_trans_count);
        COMMIT;--Added on 11/13/2013 Ram Talluri and Rasikha Galimova TMS 20131009-00330

        IF l_return_status <> fnd_api.g_ret_sts_success then

            -- 04/17/2013: CG Added temporarily to log inventory manager failed transactions
            begin
                insert into xxwc.xxwc_mti_log_bkp
                select  *
                from mtl_transactions_interface
                where  transaction_interface_id = l_interface_transaction_id;
            exception
            when others then
                null;
            end;

--            begin
--                insert into xxwc.xxwc_mtli_log_bkp
--                select  *
--                from mtl_transaction_lots_interface
--                where  transaction_interface_id = l_interface_transaction_id;
--            exception
--            when others then
--                null;
--            end;

            IF l_msg_count = 1 THEN
                begin
                    l_log_sequence := l_log_sequence + 1;
                    insert into xxwc.xxwc_wsh_shipping_log (
                            header_id
                            , line_id
                            , log_message
                            , creation_date
                            , log_sequence
                        ) values (
                            c1.header_id
                            , c1.line_id
                            , substr('Inv Mgr Err. '||replace(l_msg_data, chr(10), ' '), 1, 4000)
                            , sysdate
                            , l_log_sequence
                        );
                exception
                when others then
                    null;
                end;
            ELSE
                FOR l_index IN 1..l_msg_count
                LOOP
                    fnd_msg_pub.get(l_index, 'T', l_msg_data, l_dummy_num);

                    begin
                        l_log_sequence := l_log_sequence + 1;
                        insert into xxwc.xxwc_wsh_shipping_log (
                                header_id
                                , line_id
                                , log_message
                                , creation_date
                                , log_sequence
                            ) values (
                                c1.header_id
                                , c1.line_id
                                , substr('Inv Mgr Err. '||replace(l_msg_data, chr(10), ' '), 1, 4000)
                                , sysdate
                                , l_log_sequence
                            );
                    exception
                    when others then
                        null;
                    end;
                END LOOP;
            END IF;

            -- 04/17/2013

            -- clear out interface table for errored records
            begin
                delete from mtl_transactions_interface
                where  transaction_interface_id = l_interface_transaction_id;
                COMMIT;--Added on 11/13/2013 Ram Talluri and Rasikha Galimova TMS 20131009-00330
            exception
            when others then
                null;
            end;

            begin
                delete from mtl_transaction_lots_interface
                where  transaction_interface_id = l_interface_transaction_id;
                COMMIT;--Added on 11/13/2013 Ram Talluri and Rasikha Galimova TMS 20131009-00330
            exception
            when others then
                null;
            end;

        end if;

        l_log_sequence := l_log_sequence + 1;
        write_log ( c1.header_id
                    , c1.line_id
                    , substr('l_processing_return: ' || l_processing_return||'. l_return_status: ' || l_return_status||'. l_msg_count: ' || l_msg_count||'. l_trans_count: ' || l_trans_count , 1, 4000)
                    , l_log_sequence);

        p_ret_status    := l_return_status;
        p_ret_msg       := replace(l_msg_data, chr(10), ' ');
        p_last_log_seq  := l_log_sequence;

    end loop;

exception
when others then
    p_ret_status    := null;
    p_ret_msg       := null;
    l_msg_data      := substr(SQLERRM, 1, 4000);
    write_log ( p_header_id
                , 99999999999999999
                , substr('Error in subinventory_transfer: '||l_msg_data, 1, 4000)
                , p_cur_log_seq  + 1);
end subinventory_transfer;


PROCEDURE create_reservation (p_header_id in number
                               , p_line_id in number
                               , p_delivery_id in number
                               , p_subinv_code in varchar2
                               , p_cur_log_seq  in number
                               , p_last_log_seq out number)
IS

    cursor cur_reserve_lots is
            select  x1.rowid rid
                    , x1.*
            from    XXWC.XXWC_WSH_SHIPPING_LOTS_STG x1
            where   x1.header_id = p_header_id
            and     x1.line_id = p_line_id
            and     x1.delivery_id = p_delivery_id
            -- 03/10/2013 CG added
            and     x1.reservation_id is null;

    -- Common Declarations
    l_api_version                   NUMBER                            := 1.0;
    l_init_msg_list                 VARCHAR2 (2)           := fnd_api.g_true;
    x_return_status                 VARCHAR2 (2);
    x_msg_count                     NUMBER                              := 0;
    x_msg_data                      VARCHAR2 (255);
    l_log_sequence                  NUMBER;

    -- WHO columns
    l_user_id                       NUMBER                             := -1;
    l_resp_id                       NUMBER                             := -1;
    l_application_id                NUMBER                             := -1;
    l_row_cnt                       NUMBER                              := 1;
    l_user_name                     VARCHAR2 (30)             := 'SETUPUSER';
    l_resp_name                     VARCHAR2 (30)             := 'INVENTORY';
    -- API specific declarations
    l_rsv_rec                       inv_reservation_global.mtl_reservation_rec_type;
    l_serial_number                 inv_reservation_global.serial_number_tbl_type;
    l_partial_reservation_flag      VARCHAR2 (2)          := fnd_api.g_false;
    l_force_reservation_flag        VARCHAR2 (2)          := fnd_api.g_false;
    l_validation_flag               VARCHAR2 (2)           := fnd_api.g_true;
    l_partial_reservation_exists    BOOLEAN                         := FALSE;
    x_serial_number                 inv_reservation_global.serial_number_tbl_type;
    x_quantity_reserved             NUMBER                              := 0;
    x_reservation_id                NUMBER                              := 0;
    l_primary_reservation_qty       NUMBER                              := 0;
                                                                 -- total qty
    l_subinventory_code             VARCHAR2 (40)                  := 'WHSE';
    l_count_sns                     NUMBER                              := 0;
    v_count                         NUMBER                              := 0;
    l_serial_reservation_quantity   NUMBER                              := 0;
    l_trx_uom                       VARCHAR2 (15)                    := NULL;
    l_demand_source_header_id       NUMBER;
    l_lot_number                    VARCHAR2 (80)                    := NULL;
    l_lot_quantity                  NUMBER;
    l_order_line_qty                NUMBER;

    l_order_source_id               NUMBER;

    -- Record set clearing
    g_miss_rsv_rec                  inv_reservation_global.mtl_reservation_rec_type;
    g_miss_serial_number            inv_reservation_global.serial_number_tbl_type;
    g_miss_mtl_reservations_tbl     inv_reservation_global.mtl_reservation_tbl_type;
BEGIN

    l_log_sequence := p_cur_log_seq;
    write_log ( p_header_id
                , p_line_id
                , substr('Starting create_reservation for: '||p_line_id, 1, 4000)
                , l_log_sequence);

    FOR c1 IN cur_reserve_lots
    LOOP
        EXIT WHEN cur_reserve_lots%NOTFOUND;

        l_lot_number := NULL;
        l_primary_reservation_qty := NULL;
        l_serial_reservation_quantity := NULL;

        l_serial_reservation_quantity := 0;
        l_primary_reservation_qty := c1.lot_quantity;
        l_lot_number := c1.lot_number;

        l_trx_uom := NULL;
        BEGIN
            SELECT order_quantity_uom
            INTO l_trx_uom
            FROM oe_order_lines
            WHERE line_id = c1.line_id;
        EXCEPTION
        WHEN OTHERS THEN
            l_trx_uom := 'EA';
        END;

        l_demand_source_header_id := NULL;
        BEGIN
            SELECT mso.sales_order_id
              INTO l_demand_source_header_id
              FROM oe_order_headers ooha, mtl_sales_orders mso
             WHERE ooha.header_id = c1.header_id
               AND ooha.order_number = mso.segment1
               AND mso.segment3 = 'ORDER ENTRY';
        EXCEPTION
        WHEN OTHERS THEN
            l_demand_source_header_id := NULL;
        END;

        l_order_source_id := NULL;
        BEGIN
            select  order_source_id
            into    l_order_source_id
            from    oe_order_headers
            where   header_id = c1.header_id;
        EXCEPTION
        WHEN OTHERS THEN
            l_order_source_id := null;
        END;

        l_log_sequence := l_log_sequence + 1;
        write_log ( p_header_id
                    , p_line_id
                    , substr('l_primary_reservation_qty: '||l_primary_reservation_qty||'. l_lot_number: '||l_lot_number||'. l_demand_source_header_id: '||l_demand_source_header_id, 1, 4000)
                    , l_log_sequence);

        IF l_demand_source_header_id IS NOT NULL
        THEN
            l_rsv_rec := g_miss_rsv_rec;

            l_rsv_rec.organization_id := c1.ship_from_org_id;
            l_rsv_rec.inventory_item_id := c1.inventory_item_id;
            l_rsv_rec.requirement_date := SYSDATE;     --c3.requirement_date;
            -- l_rsv_rec.demand_source_type_id := inv_reservation_global.g_source_type_oe;
            if l_order_source_id = 10 then
                l_rsv_rec.demand_source_type_id := inv_reservation_global.g_source_type_internal_ord;
            else
                l_rsv_rec.demand_source_type_id := inv_reservation_global.g_source_type_oe;
            end if;
            l_rsv_rec.supply_source_type_id := inv_reservation_global.g_source_type_inv;
            l_rsv_rec.demand_source_name := NULL;
            l_rsv_rec.primary_reservation_quantity := l_primary_reservation_qty;
            l_rsv_rec.primary_uom_code := l_trx_uom;
            l_rsv_rec.subinventory_code := p_subinv_code;
            l_rsv_rec.demand_source_header_id := l_demand_source_header_id;
            l_rsv_rec.demand_source_line_id := c1.line_id;
            l_rsv_rec.reservation_uom_code := NULL;
            l_rsv_rec.reservation_quantity := NULL;
            l_rsv_rec.supply_source_header_id := NULL;
            l_rsv_rec.supply_source_line_id := NULL;
            l_rsv_rec.supply_source_name := NULL;
            l_rsv_rec.supply_source_line_detail := NULL;
            l_rsv_rec.lot_number := l_lot_number;
            l_rsv_rec.serial_number := NULL;
            l_rsv_rec.ship_ready_flag := NULL;
            l_rsv_rec.attribute15 := NULL;
            l_rsv_rec.attribute14 := NULL;
            l_rsv_rec.attribute13 := NULL;
            l_rsv_rec.attribute12 := NULL;
            l_rsv_rec.attribute11 := NULL;
            l_rsv_rec.attribute10 := NULL;
            l_rsv_rec.attribute9 := NULL;
            l_rsv_rec.attribute8 := NULL;
            l_rsv_rec.attribute7 := NULL;
            l_rsv_rec.attribute6 := NULL;
            l_rsv_rec.attribute5 := NULL;
            l_rsv_rec.attribute4 := NULL;
            l_rsv_rec.attribute3 := NULL;
            l_rsv_rec.attribute2 := NULL;
            l_rsv_rec.attribute1 := NULL;
            l_rsv_rec.attribute_category := NULL;
            l_rsv_rec.lpn_id := NULL;
            l_rsv_rec.pick_slip_number := NULL;
            l_rsv_rec.lot_number_id := NULL;
            l_rsv_rec.locator_id := NULL;
            l_rsv_rec.subinventory_id := NULL;
            l_rsv_rec.revision := NULL;
            l_rsv_rec.external_source_line_id := NULL;
            l_rsv_rec.external_source_code := NULL;
            l_rsv_rec.autodetail_group_id := NULL;
            l_rsv_rec.reservation_uom_id := NULL;
            l_rsv_rec.primary_uom_id := NULL;
            l_rsv_rec.demand_source_delivery := NULL;
            l_rsv_rec.serial_reservation_quantity := l_serial_reservation_quantity;
            l_rsv_rec.project_id := NULL;
            l_rsv_rec.task_id := NULL;

            inv_reservation_pub.create_reservation
                   (p_api_version_number            => l_api_version,
                    p_init_msg_lst                  => l_init_msg_list,
                    p_rsv_rec                       => l_rsv_rec,
                    p_serial_number                 => l_serial_number,
                    p_partial_reservation_flag      => l_partial_reservation_flag,
                    p_force_reservation_flag        => l_force_reservation_flag,
                    p_partial_rsv_exists            => l_partial_reservation_exists,
                    p_validation_flag               => l_validation_flag,
                    x_serial_number                 => x_serial_number,
                    x_return_status                 => x_return_status,
                    x_msg_count                     => x_msg_count,
                    x_msg_data                      => x_msg_data,
                    x_quantity_reserved             => x_quantity_reserved,
                    x_reservation_id                => x_reservation_id
                   );

            l_log_sequence := p_cur_log_seq;
            write_log ( p_header_id
                        , p_line_id
                        , substr('x_return_status: '||x_return_status||'. x_msg_count: '||x_msg_count||'. x_quantity_reserved: '||x_quantity_reserved||'. x_reservation_id: '||x_reservation_id, 1, 4000)
                        , l_log_sequence);

            IF (x_return_status <> fnd_api.g_ret_sts_success)
            THEN

                begin
                    update  XXWC.XXWC_WSH_SHIPPING_LOTS_STG
                    set     status = 'LOT_RESERVE_ERR'
                    where   rowid = c1.rid;
                    -- where   header_id = c1.header_id
                    -- and     line_id = c1.line_id
                    -- and     delivery_id = c1.delivery_id;
                exception
                when others then
                    l_log_sequence := p_cur_log_seq;
                    write_log ( p_header_id
                                , p_line_id
                                , substr('Could not update line '||c1.line_id||' with lot reservation error', 1, 4000)
                                , l_log_sequence);
                end;

                IF (NVL (x_msg_count, 0) = 0) THEN
                    l_log_sequence := p_cur_log_seq;
                    write_log ( p_header_id
                                , p_line_id
                                , substr('Returned in erorr without messages. x_msg_data: '||x_msg_data, 1, 4000)
                                , l_log_sequence);
                ELSIF NVL (x_msg_count, 0) = 1 THEN
                    l_log_sequence := p_cur_log_seq;
                    write_log ( p_header_id
                                , p_line_id
                                , substr('x_msg_data: '||x_msg_data, 1, 4000)
                                , l_log_sequence);

                ELSE
                    FOR i IN 1 .. x_msg_count
                    LOOP
                        x_msg_data := fnd_msg_pub.get (i, 'F');
                        l_log_sequence := p_cur_log_seq;
                        write_log ( p_header_id
                                    , p_line_id
                                    , substr('x_msg_data('||i||'): '||x_msg_data, 1, 4000)
                                    , l_log_sequence);
                    END LOOP;
                END IF;

            ELSE
                l_log_sequence := p_cur_log_seq;
                write_log ( p_header_id
                            , p_line_id
                            , substr('Created reservation '||x_reservation_id||' for qty of '||x_quantity_reserved, 1, 4000)
                            , l_log_sequence);

                begin
                    update  XXWC.XXWC_WSH_SHIPPING_LOTS_STG
                    set     reservation_id = x_reservation_id
                    where   rowid = c1.rid;
                    -- where   header_id = c1.header_id
                    -- and     line_id = c1.line_id
                    -- and     delivery_id = c1.delivery_id;
                exception
                when others then
                    l_log_sequence := p_cur_log_seq;
                    write_log ( p_header_id
                                , p_line_id
                                , substr('Could not lot table with new reservation id for '||c1.line_id, 1, 4000)
                                , l_log_sequence);
                end;

            END IF;

        ELSE
            l_log_sequence := p_cur_log_seq;
            write_log ( p_header_id
                        , p_line_id
                        , substr('Could not find MTL_SALES_ORDER id for header_id '||c1.header_id, 1, 4000)
                        , l_log_sequence);
        END IF;

    END LOOP;

    p_last_log_seq := l_log_sequence;

END create_reservation;


/*************************************************************************
  Procedure : Update_SO_Line

  PURPOSE:
  Parameter:
         IN

************************************************************************/
   PROCEDURE update_so_line (
      p_header_id in number
      , p_line_id in number
      , p_subinventory in varchar
      , p_cur_log_seq in number
      , p_ret_status   out varchar
      , p_last_log_seq out number
   )
IS

      l_header_rec               oe_order_pub.header_rec_type;
      l_line_tbl                 oe_order_pub.line_tbl_type;
      l_action_request_tbl       oe_order_pub.request_tbl_type;
      l_header_adj_tbl           oe_order_pub.header_adj_tbl_type;
      l_line_adj_tbl             oe_order_pub.line_adj_tbl_type;
      l_header_scr_tbl           oe_order_pub.header_scredit_tbl_type;
      l_line_scredit_tbl         oe_order_pub.line_scredit_tbl_type;
      l_request_rec              oe_order_pub.request_rec_type;
      l_return_status            VARCHAR2 (1000);
      l_msg_count                NUMBER;
      l_msg_data                 VARCHAR2 (1000);
      p_api_version_number       NUMBER                                 := 1.0;
      p_init_msg_list            VARCHAR2 (10)              := fnd_api.g_false;
      p_return_values            VARCHAR2 (10)              := fnd_api.g_false;
      p_action_commit            VARCHAR2 (10)              := fnd_api.g_false;
      x_return_status            VARCHAR2 (1);
      x_msg_count                NUMBER;
      x_msg_data                 VARCHAR2 (100);
      p_header_rec               oe_order_pub.header_rec_type
                                             := oe_order_pub.g_miss_header_rec;
      p_header_val_rec           oe_order_pub.header_val_rec_type
                                         := oe_order_pub.g_miss_header_val_rec;
      p_header_adj_tbl           oe_order_pub.header_adj_tbl_type
                                         := oe_order_pub.g_miss_header_adj_tbl;
      p_header_adj_val_tbl       oe_order_pub.header_adj_val_tbl_type
                                     := oe_order_pub.g_miss_header_adj_val_tbl;
      p_header_price_att_tbl     oe_order_pub.header_price_att_tbl_type
                                   := oe_order_pub.g_miss_header_price_att_tbl;
      p_header_adj_att_tbl       oe_order_pub.header_adj_att_tbl_type
                                     := oe_order_pub.g_miss_header_adj_att_tbl;
      p_header_adj_assoc_tbl     oe_order_pub.header_adj_assoc_tbl_type
                                   := oe_order_pub.g_miss_header_adj_assoc_tbl;
      p_header_scredit_tbl       oe_order_pub.header_scredit_tbl_type
                                     := oe_order_pub.g_miss_header_scredit_tbl;
      p_header_scredit_val_tbl   oe_order_pub.header_scredit_val_tbl_type
                                 := oe_order_pub.g_miss_header_scredit_val_tbl;
      p_line_tbl                 oe_order_pub.line_tbl_type
                                               := oe_order_pub.g_miss_line_tbl;
      p_line_val_tbl             oe_order_pub.line_val_tbl_type
                                           := oe_order_pub.g_miss_line_val_tbl;
      p_line_adj_tbl             oe_order_pub.line_adj_tbl_type
                                           := oe_order_pub.g_miss_line_adj_tbl;
      p_line_adj_val_tbl         oe_order_pub.line_adj_val_tbl_type
                                       := oe_order_pub.g_miss_line_adj_val_tbl;
      p_line_price_att_tbl       oe_order_pub.line_price_att_tbl_type
                                     := oe_order_pub.g_miss_line_price_att_tbl;
      p_line_adj_att_tbl         oe_order_pub.line_adj_att_tbl_type
                                       := oe_order_pub.g_miss_line_adj_att_tbl;
      p_line_adj_assoc_tbl       oe_order_pub.line_adj_assoc_tbl_type
                                     := oe_order_pub.g_miss_line_adj_assoc_tbl;
      p_line_scredit_tbl         oe_order_pub.line_scredit_tbl_type
                                       := oe_order_pub.g_miss_line_scredit_tbl;
      p_line_scredit_val_tbl     oe_order_pub.line_scredit_val_tbl_type
                                   := oe_order_pub.g_miss_line_scredit_val_tbl;
      p_lot_serial_tbl           oe_order_pub.lot_serial_tbl_type
                                         := oe_order_pub.g_miss_lot_serial_tbl;
      p_lot_serial_val_tbl       oe_order_pub.lot_serial_val_tbl_type
                                     := oe_order_pub.g_miss_lot_serial_val_tbl;
      p_action_request_tbl       oe_order_pub.request_tbl_type
                                            := oe_order_pub.g_miss_request_tbl;
      x_header_rec               oe_order_pub.header_rec_type;
      x_header_val_rec           oe_order_pub.header_val_rec_type;
      x_header_adj_tbl           oe_order_pub.header_adj_tbl_type;
      x_header_adj_val_tbl       oe_order_pub.header_adj_val_tbl_type;
      x_header_price_att_tbl     oe_order_pub.header_price_att_tbl_type;
      x_header_adj_att_tbl       oe_order_pub.header_adj_att_tbl_type;
      x_header_adj_assoc_tbl     oe_order_pub.header_adj_assoc_tbl_type;
      x_header_scredit_tbl       oe_order_pub.header_scredit_tbl_type;
      x_header_scredit_val_tbl   oe_order_pub.header_scredit_val_tbl_type;
      x_line_tbl                 oe_order_pub.line_tbl_type;
      x_line_val_tbl             oe_order_pub.line_val_tbl_type;
      x_line_adj_tbl             oe_order_pub.line_adj_tbl_type;
      x_line_adj_val_tbl         oe_order_pub.line_adj_val_tbl_type;
      x_line_price_att_tbl       oe_order_pub.line_price_att_tbl_type;
      x_line_adj_att_tbl         oe_order_pub.line_adj_att_tbl_type;
      x_line_adj_assoc_tbl       oe_order_pub.line_adj_assoc_tbl_type;
      x_line_scredit_tbl         oe_order_pub.line_scredit_tbl_type;
      x_line_scredit_val_tbl     oe_order_pub.line_scredit_val_tbl_type;
      x_lot_serial_tbl           oe_order_pub.lot_serial_tbl_type;
      x_lot_serial_val_tbl       oe_order_pub.lot_serial_val_tbl_type;
      x_action_request_tbl       oe_order_pub.request_tbl_type;
      x_debug_file               VARCHAR2 (100);
      l_line_tbl_index           NUMBER;
      l_msg_index_out            NUMBER (10);
      l_order_line_qty           NUMBER;
      l_org_id                   NUMBER;
      l_log_sequence             NUMBER;
   BEGIN
      l_log_sequence := p_cur_log_seq;
      write_log ( p_header_id
                , p_line_id
                , substr('Starting update_so_line for: '||p_line_id, 1, 4000)
                , l_log_sequence);

      oe_msg_pub.initialize;
      mo_global.set_policy_context ('S', 162);
      mo_global.init ('ONT');
      l_org_id := NULL;

      BEGIN
         SELECT organization_id
           INTO l_org_id
           FROM hr_operating_units
          WHERE NAME = 'HDS White Cap - Org';
      EXCEPTION
         WHEN OTHERS
         THEN
            l_org_id := fnd_global.org_id;
      END;

      l_line_tbl_index := 1;

      l_line_tbl (l_line_tbl_index) := oe_order_pub.g_miss_line_rec;
      l_line_tbl (l_line_tbl_index).subinventory := p_subinventory;
      l_line_tbl (l_line_tbl_index).header_id := p_header_id;
      l_line_tbl (l_line_tbl_index).line_id := p_line_id;
      l_line_tbl (l_line_tbl_index).operation := oe_globals.g_opr_update;

      -- CALL TO PROCESS ORDER
         oe_order_pub.process_order
                        (p_org_id                      => l_org_id,
                         p_api_version_number          => 1.0,
                         x_return_status               => l_return_status,
                         x_msg_count                   => l_msg_count,
                         x_msg_data                    => l_msg_data,
                         p_header_rec                  => l_header_rec,
                         p_line_tbl                    => l_line_tbl
                                                                    -- OUT PARAMETERS
         ,
                         x_header_rec                  => x_header_rec,
                         x_header_val_rec              => x_header_val_rec,
                         x_header_adj_tbl              => x_header_adj_tbl,
                         x_header_adj_val_tbl          => x_header_adj_val_tbl,
                         x_header_price_att_tbl        => x_header_price_att_tbl,
                         x_header_adj_att_tbl          => x_header_adj_att_tbl,
                         x_header_adj_assoc_tbl        => x_header_adj_assoc_tbl,
                         x_header_scredit_tbl          => x_header_scredit_tbl,
                         x_header_scredit_val_tbl      => x_header_scredit_val_tbl,
                         x_line_tbl                    => x_line_tbl,
                         x_line_val_tbl                => x_line_val_tbl,
                         x_line_adj_tbl                => x_line_adj_tbl,
                         x_line_adj_val_tbl            => x_line_adj_val_tbl,
                         x_line_price_att_tbl          => x_line_price_att_tbl,
                         x_line_adj_att_tbl            => x_line_adj_att_tbl,
                         x_line_adj_assoc_tbl          => x_line_adj_assoc_tbl,
                         x_line_scredit_tbl            => x_line_scredit_tbl,
                         x_line_scredit_val_tbl        => x_line_scredit_val_tbl,
                         x_lot_serial_tbl              => x_lot_serial_tbl,
                         x_lot_serial_val_tbl          => x_lot_serial_val_tbl,
                         x_action_request_tbl          => x_action_request_tbl
                        );

         l_log_sequence := l_log_sequence + 1;
         write_log ( p_header_id
                    , p_line_id
                    , substr('l_return_status: '||l_return_status||'. l_msg_count: '||l_msg_count, 1, 4000)
                    , l_log_sequence);

         -- Check the return status
         IF l_return_status <> fnd_api.g_ret_sts_success
         THEN
            if l_msg_count = 1 then
                 l_log_sequence := l_log_sequence + 1;
                 write_log ( p_header_id
                            , p_line_id
                            , substr('l_msg_data: '||l_msg_data, 1, 4000)
                            , l_log_sequence);
            else
                -- Retrieve messages
                 FOR i IN 1 .. l_msg_count
                 LOOP
                    oe_msg_pub.get (p_msg_index          => i,
                                    p_encoded            => fnd_api.g_false,
                                    p_data               => l_msg_data,
                                    p_msg_index_out      => l_msg_index_out
                                   );

                    IF l_msg_data IS NOT NULL
                    THEN
                       l_log_sequence := l_log_sequence + 1;
                         write_log ( p_header_id
                                    , p_line_id
                                    , substr('l_msg_data: '||l_msg_data, 1, 4000)
                                    , l_log_sequence);
                    END IF;
                 END LOOP;
            end if;
         END IF;

      p_ret_status := l_return_status;
      p_last_log_seq := l_log_sequence;

   END update_so_line;


procedure order_subinv_transf (p_header_id in number
                               , p_organization_id in number
                               , p_user_id  in number
                               , p_resp_id  in number
                               , p_resp_appl_id in number
                               , p_update_login in number
                               , p_line_id IN NUMBER
                               , p_delivery_id in out number
                               , p_create_delivery in varchar2
                               , p_proc_subinv_transf in varchar2)
  /**************************************************************************
   $Header xxwc_wsh_shipping_extn_pkg $
   Module Name: order_subinv_transf
   PURPOSE:   This Procedure is used to split the Subinventory
   REVISIONS:
   Ver        Date        Author                     Description
   ---------  ----------  ---------------         -------------------------
   1.0        04/17/2013  Consuelo Gonzalez      Initial Version -TMS 20130422-01089
   3.4        03/03/2016  Manjula Chellappan    TMS #20151023-00037 - Shipping Extension Modification
                                                    for Counter Order Removal and PUBD
   3.5        03/09/2016  Manjula Chellappan    TMS #20150622-00156 - Shipping Extension modification for PUBD
   3.7        01/20/2017  P.Vamshidhar            TMS#20161130-00181 - Standard Order Delivery documents causing inventory to drive negative
   **************************************************************************/
is
    cursor cur_order_lines is
        select  ol.header_id
                , ol.line_id
                , oh.order_number
                , ol.line_number
                , ol.ordered_item
                , ol.inventory_item_id
                , ol.ship_from_org_id
                -- 06/27/2013 CG: TMS 20130627-01130: Changed to handle UOM Conversion
                -- , ol.ordered_quantity
                , (ol.ordered_quantity
                     * po_uom_s.po_uom_convert (um.unit_of_measure
                                                , msib.primary_unit_of_measure
                                                , ol.inventory_item_id
                                               )
                  ) ordered_quantity
                , ol.shipped_quantity
                , ol.cancelled_quantity
                -- 06/27/2013 CG: TMS 20130627-01130: Changed to handle UOM Conversion
                -- , to_number(ol.attribute11) force_ship_qty
-- Commented for Ver 3.5 Begin
/*                , ( to_number(ol.attribute11)
                    * po_uom_s.po_uom_convert (um.unit_of_measure
                                                , msib.primary_unit_of_measure
                                                , ol.inventory_item_id
                                               )
                   )
           force_ship_qty
*/
-- Commented for Ver 3.5 End
-- Added for Ver 3.5 Begin
                ,(  DECODE (ol.subinventory,
                           'PUBD', ol.ordered_quantity,
                           TO_NUMBER (ol.attribute11))
                 * po_uom_s.po_uom_convert (um.unit_of_measure,
                                            msib.primary_unit_of_measure,
                                            ol.inventory_item_id))
                   force_ship_qty
-- Added for Ver 3.5 End
                , NVL(ol.subinventory,'General') subinventory
                , msib.lot_control_code -- 1: no control, 2: full control
                , msib.primary_uom_code
                , xxwc_mv_routines_pkg.get_onhand_qty (ol.inventory_item_id,
                                                       ol.ship_from_org_id,
                                                       NVL(ol.subinventory,'General'),
                                                       'QOH') Available_Onhand
                , msib_mst.item_type mst_item_type
                -- 06/27/2013 CG: TMS 20130627-01130: Added to handle UOM Conversion
                , um.unit_of_measure ordered_uom
        from    oe_order_lines ol
                , oe_order_headers oh
                , mtl_system_items_b msib
                , mtl_parameters mp
                , mtl_system_items_b msib_mst
                -- 06/27/2013 CG: TMS 20130627-01130: Added to handle UOM Conversion
                , mtl_units_of_measure_vl um
        where   ol.header_id = p_header_id
        and     ol.line_id = nvl(p_line_id, ol.line_id)
        and     ol.ship_from_org_id = p_organization_id
        and     ol.header_id = oh.header_id
        and     nvl(ol.cancelled_flag, 'N') = 'N'
        and     ol.inventory_item_id = msib.inventory_item_id
        and     ol.ship_from_org_id = msib.organization_id
--      and     nvl(ol.subinventory, 'ZZZ') = 'General' -- Commented for Ver 3.5
        and     nvl(ol.subinventory, 'ZZZ') IN ('General','PUBD') -- Added for Ver 3.5
        and     msib.organization_id = mp.organization_id
        and     ol.inventory_item_id = msib_mst.inventory_item_id
        and     mp.master_organization_id = msib_mst.organization_id
        -- 06/27/2013 CG: TMS 20130627-01130: Added to handle UOM Conversion
        and     ol.order_quantity_uom = um.uom_code
    Order by ol.ordered_item, ol.LINE_ID; -- Added in Rev 3.7.
        -- and     not exists (select 'Not in staging table'
        --                    from    xxwc_wsh_shipping_stg x1
        --                    where   x1.header_id = ol.header_id
        --                    and     x1.line_id = ol.line_id);

    cursor cur_reserved_lots (p_line_id number, p_order_number varchar2, p_subinventory varchar2) is
            select  'Y'
                    , mr.reservation_id
                    , mr.lot_number
                    , mr.reservation_quantity
                    , mr.subinventory_code
                    , mr.demand_source_header_id
                    , mr.demand_source_line_id
            from    mtl_reservations mr
                    , mtl_sales_orders mso
            where   mr.demand_source_type_id in (inv_reservation_global.g_source_type_oe, inv_reservation_global.g_source_type_internal_ord)
            and     mr.supply_source_type_id = inv_reservation_global.g_source_type_inv
            and     mr.demand_source_line_id = p_line_id
            and     mr.subinventory_code = p_subinventory
            and     mr.demand_source_header_id = mso.sales_order_id
            and     mso.segment1 = p_order_number
            and     mso.segment3 = 'ORDER ENTRY';

    cursor cur_transf_lots (p_header_id number, p_line_id number, p_delivery_id number) is
            select  x1.rowid rid
                    , x1.*
            from    XXWC.XXWC_WSH_SHIPPING_LOTS_STG x1
            where   header_id = p_header_id
            and     line_id = p_line_id
            and     delivery_id = p_delivery_id;

    l_has_lot_reservations  number;
    l_reservation_id        number;
    l_lot_number            varchar2(80);
    l_lot_quantity          number;
    l_subinventory_code     varchar2(10);
    l_new_delivery          number;
    l_new_transaction_qty   number;
    l_perf_subinv_transf    varchar2(1);
    l_log_sequence          number;
    l_cur_status            varchar2(240);
    l_cur_delivery          number;
    l_record_in_stg         varchar2(1);
    l_orig_trx_qty          number;
  l_item_onhand           number; -- Added in Rev 3.7.

    -- Reservation Handling
    l_rsv       inv_reservation_global.mtl_reservation_rec_type;
    l_msg_count NUMBER;
    l_rsv_id    NUMBER;
    l_dummy_sn  inv_reservation_global.serial_number_tbl_type;
    l_status    VARCHAR2(1);
    g_miss_rsv_rec                  inv_reservation_global.mtl_reservation_rec_type;
    g_miss_serial_number            inv_reservation_global.serial_number_tbl_type;
    g_miss_mtl_reservations_tbl     inv_reservation_global.mtl_reservation_tbl_type;

    -- Subinventory Transfer
    l_transaction_header_id      NUMBER;
    l_interface_transaction_id   NUMBER;
    l_count_serials              NUMBER;
    l_transaction_type_id        NUMBER;
    l_transaction_uom            VARCHAR2 (3);
    l_transaction_qty            NUMBER;
    l_gral_transaction_qty       NUMBER;
    l_processing_return          NUMBER;
    l_return_status              VARCHAR2 (240);
    l_msg_data                   VARCHAR2 (4000);
    l_trans_count                NUMBER;
    l_dummy_num                  NUMBER;
    l_shipment_status            VARCHAR2(240);
    l_lot_count                  NUMBER;
    l_return_msg                 varchar2(2000);

begin

    write_log (p_header_id
                , 0
                , substr('Starting order_subinv_transf processing for: '||p_header_id, 1, 4000)
                , 99999995);

    --g_request_id := fnd_global.conc_request_id;
    fnd_global.apps_initialize (user_id           => p_user_id, --fnd_global.user_id,
                                resp_id           => p_resp_id,
                                resp_appl_id      => p_resp_appl_id
                               );

    l_new_delivery          := null;
    if nvl(p_create_delivery, 'N') = 'Y' then
        select  xxwc.xxwc_wsh_deliveries_s.nextval
        into    l_new_delivery
        from    dual;
    else
        if p_delivery_id is not null then
            l_new_delivery := p_delivery_id;
        else
            select  xxwc.xxwc_wsh_deliveries_s.nextval
            into    l_new_delivery
            from    dual;
        end if;
    end if;

    write_log (p_header_id
                , 0
                , substr('p_create_delivery: '||p_create_delivery||'. l_new_delivery: '||l_new_delivery, 1, 4000)
                , 99999996);

    for c1 in cur_order_lines
    loop
        exit when cur_order_lines%notfound;
        l_new_transaction_qty   := null;
        l_has_lot_reservations  := null;
        l_reservation_id        := null;
        l_lot_number            := null;
        l_lot_quantity          := null;
        l_subinventory_code     := null;
        l_perf_subinv_transf    := null;
        l_log_sequence          := 0;
        l_shipment_status       := null;
    l_item_onhand           := 0; -- Added in Rev 3.7.

        l_log_sequence := l_log_sequence + 1;
        write_log ( c1.header_id
                    , c1.line_id
                    , 'Starting line processing. Line Num: '||c1.line_number||'. Item: '||c1.ordered_item||'. Line ID: '||c1.line_id
                    , l_log_sequence);

    -- Added below code in Rev 3.7 - Begin
        -- Determine item onhand quantity.
        l_item_onhand := xxwc_mv_routines_pkg.get_onhand_qty (C1.inventory_item_id,
                                                              C1.ship_from_org_id,
                                                              NVL(C1.subinventory,'General'),
                                                              'QOH');
    -- Added above code in Rev 3.7 - End

        -- 1. Determine quantity to transfer (lot vs non-lot)
        if c1.force_ship_qty is not null and c1.force_ship_qty > 0 then
            l_new_transaction_qty := c1.force_ship_qty;
            l_perf_subinv_transf := 'Y';
        elsif c1.force_ship_qty is not null and c1.force_ship_qty = 0 then
            l_new_transaction_qty := 0;
            l_perf_subinv_transf := 'N';
        else
            --if nvl(c1.Available_Onhand,0) > 0 and c1.ordered_quantity >= nvl(c1.Available_Onhand,0) then  --Commented in Rev 3.7
        if nvl(l_item_onhand,0) > 0 and c1.ordered_quantity >= nvl(l_item_onhand,0) then  --Added in Rev 3.7
                --l_new_transaction_qty := c1.Available_Onhand;  -- Commented in Rev 3.7.
        l_new_transaction_qty := l_item_onhand;    -- Added in Rev 3.7
                l_perf_subinv_transf := 'Y';
            --elsif nvl(c1.Available_Onhand,0) > 0 and c1.ordered_quantity < nvl(c1.Available_Onhand,0) then  --Commented in Rev 3.7
            elsif nvl(l_item_onhand,0) > 0 and c1.ordered_quantity < nvl(l_item_onhand,0) then    --Added in Rev 3.7
                l_new_transaction_qty := c1.ordered_quantity;
                l_perf_subinv_transf := 'Y';
            else
                l_new_transaction_qty := 0;
                l_perf_subinv_transf := 'N';
            end if;
        end if;

        l_log_sequence := l_log_sequence + 1;
--        write_log ( c1.header_id
--                    , c1.line_id
--                    , substr('force_ship_qty: '||c1.force_ship_qty||' . Available_Onhand: '||c1.Available_Onhand||'. ordered_quantity: '||c1.ordered_quantity , 1, 4000)
--                    , l_log_sequence);

        write_log ( c1.header_id
                    , c1.line_id
                    , substr('force_ship_qty: '||c1.force_ship_qty||' . Available_Onhand: '||l_item_onhand||'. ordered_quantity: '||c1.ordered_quantity , 1, 4000)
                    , l_log_sequence);


        -- 2. Determine if there are any reservations for lot controlled
        -- Can there be more than one lot reserved?
        if c1.lot_control_code = 2 then

            l_lot_quantity := 0;
            for c2 in cur_reserved_lots (c1.line_id, to_char(c1.order_number), c1.subinventory)
            loop
                exit when cur_reserved_lots%notfound;
                l_lot_quantity := l_lot_quantity + c2.reservation_quantity;

                l_log_sequence := l_log_sequence + 1;
                write_log ( c1.header_id
                            , c1.line_id
                            , substr('Lot ctrl '||c1.lot_control_code||'. Reserv ID: '||c2.reservation_id||'. Lot: '||c2.lot_number||'. lot qty: '||c2.reservation_quantity||'. Subinv: '||c2.subinventory_code , 1, 4000)
                            , l_log_sequence);

            end loop;

            if c1.force_ship_qty is not null then
                if c1.force_ship_qty = 0 then
                    l_new_transaction_qty := 0;
                    l_perf_subinv_transf := 'N';
                elsif c1.force_ship_qty != l_lot_quantity then
                    l_new_transaction_qty := 0;
                    l_perf_subinv_transf := 'N';
                else
                    l_new_transaction_qty := l_lot_quantity;
                    l_perf_subinv_transf := 'Y';
                end if;
            else
                if l_lot_quantity = 0 then
                    l_new_transaction_qty := 0;
                    l_perf_subinv_transf := 'N';
                else
                    l_new_transaction_qty := l_lot_quantity;
                    l_perf_subinv_transf := 'Y';
                end if;
            end if;

            l_log_sequence := l_log_sequence + 1;
            write_log ( c1.header_id
                        , c1.line_id
                        , substr('Perf Subinv flag: '||l_perf_subinv_transf||'. lot_quantity: '||l_lot_quantity||'. new trx qty: '||l_new_transaction_qty , 1, 4000)
                        , l_log_sequence);

        end if;

        -- This may need to change depending on what is passed from the form?
        -- 3. Insert into custom table
                -- 3.1 Determine if order has open new delivery
                -- 3.2 Assign new delivery ID if one does not exist

        begin
            l_record_in_stg := null;
            l_cur_status    := null;
            l_orig_trx_qty  := null;
            l_cur_delivery  := null;
            select  'Y'
                    , status
                    , transaction_qty
                    -- 03/27/13 CG Added
                    , delivery_id
            into    l_record_in_stg
                    , l_cur_status
                    , l_orig_trx_qty
                    -- 03/27/13 CG Added
                    , l_cur_delivery
            from    xxwc_wsh_shipping_stg
            where   header_id = c1.header_id
            and     line_id = c1.line_id;
        exception
        when no_data_found then
            l_record_in_stg := 'N';
            l_cur_status    := null;
            l_orig_trx_qty  := 0;
            -- 03/27/13 CG Added
            l_cur_delivery  := null;
        when too_many_rows then
            l_record_in_stg := 'Y';
            l_cur_status    := 'UNKNOWN';
            l_orig_trx_qty  := 0;
            -- 03/27/13 CG Added
            l_cur_delivery  := null;
        when others then
            l_record_in_stg := 'N';
            l_cur_status    := null;
            l_orig_trx_qty  := 0;
            -- 03/27/13 CG Added
            l_cur_delivery  := null;
        end;

        l_log_sequence := l_log_sequence + 1;
        write_log ( c1.header_id
                    , c1.line_id
                    , substr('l_record_in_stg: '||l_record_in_stg||'. l_cur_status: '||l_cur_status||'. l_orig_trx_qty: '||l_orig_trx_qty, 1, 4000)
                    , l_log_sequence);

        if l_cur_status = 'OUT_FOR_DELIVERY' then
            l_perf_subinv_transf := 'N';
        elsif l_cur_status = 'OUT_FOR_DELIVERY/PARTIAL_BACKORDER' then
            -- Need to move only remaining balance if applicable
            if c1.force_ship_qty = nvl(l_orig_trx_qty, 0) then
                l_new_transaction_qty := 0;
                l_perf_subinv_transf := 'N';
            else
                if l_perf_subinv_transf = 'Y' then
                    l_new_transaction_qty := l_new_transaction_qty - l_orig_trx_qty;
                end if;
            end if;
        end if;

        l_log_sequence := l_log_sequence + 1;
        write_log ( c1.header_id
                    , c1.line_id
                    , 'p_proc_subinv_transf: '||p_proc_subinv_transf
                    , l_log_sequence);

        l_log_sequence := l_log_sequence + 1;
        write_log ( c1.header_id
                    , c1.line_id
                    , 'mst_item_type: '||c1.mst_item_type
                    , l_log_sequence);

        if nvl(p_proc_subinv_transf, 'Y') = 'N' or nvl(c1.mst_item_type,'UNKNOWN') IN ('INTANGIBLE','FRT','PTO') then
            l_new_transaction_qty := 0;
            l_perf_subinv_transf := 'N';
        end if;

        -- 03/04/2013 CG: Changed to accomodate new parameter for creating new deliveries or using existing ones
        /*IF l_record_in_stg = 'N' then
            if p_delivery_id is not null then
                l_new_delivery := p_delivery_id;
            else
                begin
                    select  delivery_id
                    into    l_new_delivery
                    from    xxwc_wsh_shipping_stg
                    where   header_id = c1.header_id
                    and     ship_from_org_id = c1.ship_from_org_id
                    and     rownum = 1;
                exception
                when others then
                    select  xxwc.xxwc_wsh_deliveries_s.nextval
                    into    l_new_delivery
                    from    dual;
                end;
            end if;
        else
            select  delivery_id
            into    l_new_delivery
            from    xxwc_wsh_shipping_stg
            where   header_id = c1.header_id
            and     line_id = c1.line_id;

            if l_cur_status = 'OUT_FOR_DELIVERY' then
                l_perf_subinv_transf := 'N';
            end if;
        end if;*/

        IF l_record_in_stg = 'N' then

            begin
                insert into xxwc_wsh_shipping_stg (
                    header_id
                    , line_id
                    , delivery_id
                    , inventory_item_id
                    , ship_from_org_id
                    , ordered_quantity
                    , force_ship_qty
                    , transaction_qty
                    , lot_number
                    , reservation_id
                    , created_by
                    , creation_date
                    , last_updated_by
                    , last_update_date
                    , last_update_login
          , subinventory -- Added for Ver 3.5
                ) values (
                    c1.header_id
                    , c1.line_id
                    , l_new_delivery
                    , c1.inventory_item_id
                    , c1.ship_from_org_id
                    , c1.ordered_quantity
                    , c1.force_ship_qty
                    , l_new_transaction_qty
                    , l_lot_number
                    , l_reservation_id
                    , p_user_id
                    , sysdate
                    , p_user_id
                    , sysdate
                    , p_update_login
          , c1.subinventory -- Added for Ver 3.5
                );

                -- Inserting lots into custom staging table
                if c1.lot_control_code = 2 and l_lot_quantity > 0 then

                    for c2 in cur_reserved_lots (c1.line_id, to_char(c1.order_number), c1.subinventory)
                    loop
                        exit when cur_reserved_lots%notfound;

                        begin
                            insert into XXWC.XXWC_WSH_SHIPPING_LOTS_STG (
                                header_id
                                , line_id
                                , delivery_id
                                , inventory_item_id
                                , ship_from_org_id
                                , lot_number
                                , lot_quantity
                                , reservation_id
                                , created_by
                                , creation_date
                                , last_updated_by
                                , last_update_date
                                , last_update_login
                            ) values (
                                c1.header_id
                                , c1.line_id
                                , l_new_delivery
                                , c1.inventory_item_id
                                , c1.ship_from_org_id
                                , c2.lot_number
                                , c2.reservation_quantity
                                , c2.reservation_id
                                , p_user_id
                                , sysdate
                                , p_user_id
                                , sysdate
                                , p_update_login
                            );
                        exception
                        when others then
                            l_log_sequence := l_log_sequence + 1;
                            write_log ( c1.header_id
                                        , c1.line_id
                                        , substr('Could not insert into lots custom table' , 1, 4000)
                                        , l_log_sequence);
                        end;

                    end loop;

                end if;

            exception
            when others then
                l_log_sequence := l_log_sequence + 1;
                write_log ( c1.header_id
                            , c1.line_id
                            , substr('Could not insert into main custom table' , 1, 4000)
                            , l_log_sequence);
            end;

        else

            if (
                (l_cur_status = 'BACKORDERED' and l_perf_subinv_transf = 'Y')
                -- 03/27/13 CG: Added
                OR
                (l_cur_status = 'BACKORDERED' and c1.lot_control_code = 2 AND c1.force_ship_qty is not null)
               )then
                l_log_sequence := l_log_sequence + 1;
                write_log ( c1.header_id
                            , c1.line_id
                            , substr('Resetting backordered line delivery id for '||c1.line_id, 1, 4000)
                            , l_log_sequence);
                begin
                    update  xxwc_wsh_shipping_stg
                    set     delivery_id = l_new_delivery
                            , transaction_qty = l_new_transaction_qty
                            -- 04/04/2013 CG: Updated to address bug where the branch id is not updating
                            , ship_from_org_id = c1.ship_from_org_id
                    where   header_id = c1.header_id
                    and     line_id = c1.line_id;
                exception
                when others then
                    null;
                end;

                -- 04/23/2013 CG: 20130423-01713 To correct issue with Lot SUBINV_TRANSFER_ERR error across warehosue and within warehouse (covers previous issue)
                /*begin
                    update  XXWC.XXWC_WSH_SHIPPING_LOTS_STG
                    set     -- 04/04/2013 CG: Updated to address bug where the branch id is not updating
                            ship_from_org_id = c1.ship_from_org_id
                            -- 04/22/2013 CG: TMS 20130422-00895: To correct issue with Lot SUBINV_TRANSFER_ERR errors
                            , delivery_id = l_new_delivery
                    where   header_id = c1.header_id
                    and     line_id = c1.line_id;
                exception
                when others then
                    null;
                end;*/
                begin
                    delete from XXWC.XXWC_WSH_SHIPPING_LOTS_STG
                    where   header_id = c1.header_id
                    and     line_id = c1.line_id;
                exception
                when others then
                    null;
                end;
            end if;

            -- Item is in staging checking to see if it's a lot controlled and if the lots are reserved
            -- Inserting lots into custom staging table
            if c1.lot_control_code = 2 and l_lot_quantity > 0 then

                -- Determine if there are lots
                l_lot_count := 0;
                begin
                    select  count(*)
                    into    l_lot_count
                    from    XXWC.XXWC_WSH_SHIPPING_LOTS_STG
                    where   header_id = c1.header_id
                    and     line_id = c1.line_id
                    -- 03/27/13 CG: Changed
                    -- and     delivery_id = l_new_delivery;
                    and     delivery_id = nvl(l_cur_delivery,l_new_delivery);
                exception
                when no_data_found then
                    l_lot_count := 0;
                when others then
                    l_lot_count := 0;
                end;

                l_log_sequence := l_log_sequence + 1;
                write_log ( c1.header_id
                            , c1.line_id
                            , substr('Lot item count and proc for header_id '||c1.header_id||'. line_id: '||c1.line_id||'. nvl(l_cur_delivery,l_new_delivery): '||nvl(l_cur_delivery,l_new_delivery)||'. l_lot_count: '||l_lot_count, 1, 4000)
                            , l_log_sequence);

                if l_lot_count = 0 then
                    for c2 in cur_reserved_lots (c1.line_id, to_char(c1.order_number), c1.subinventory)
                    loop
                        exit when cur_reserved_lots%notfound;

                        begin
                            insert into XXWC.XXWC_WSH_SHIPPING_LOTS_STG (
                                header_id
                                , line_id
                                , delivery_id
                                , inventory_item_id
                                , ship_from_org_id
                                , lot_number
                                , lot_quantity
                                , reservation_id
                                , created_by
                                , creation_date
                                , last_updated_by
                                , last_update_date
                                , last_update_login
                            ) values (
                                c1.header_id
                                , c1.line_id
                                -- 03/27/13 CG: Changed
                                --, l_new_delivery
                                , nvl(l_cur_delivery,l_new_delivery)
                                , c1.inventory_item_id
                                , c1.ship_from_org_id
                                , c2.lot_number
                                , c2.reservation_quantity
                                , c2.reservation_id
                                , p_user_id
                                , sysdate
                                , p_user_id
                                , sysdate
                                , p_update_login
                            );
                        exception
                        when others then
                            l_log_sequence := l_log_sequence + 1;
                            write_log ( c1.header_id
                                        , c1.line_id
                                        , substr('Could not insert into lots custom table' , 1, 4000)
                                        , l_log_sequence);
                        end;

                    end loop;
                end if;
            end if;

        end if;

        l_log_sequence := l_log_sequence + 1;
        write_log ( c1.header_id
                    , c1.line_id
                    , substr('l_perf_subinv_transf: '||l_perf_subinv_transf, 1, 4000)
                    , l_log_sequence);

        if NVL(l_perf_subinv_transf, 'N') = 'Y' then

            subinventory_transfer (  c1.header_id
                                   , c1.line_id
                                   , l_new_delivery
                                   , c1.subinventory
                                   , 'GeneralPCK'
                                   , l_log_sequence + 1
                                   , l_return_status
                                   , l_msg_data
                                   , l_log_sequence);

            IF l_return_status = fnd_api.g_ret_sts_success THEN
                l_log_sequence := l_log_sequence + 1;
                write_log ( c1.header_id
                            , c1.line_id
                            , substr('Subinv transfer successfully completed...', 1, 4000)
                            , l_log_sequence);

                IF l_new_transaction_qty > 1 and l_new_transaction_qty < c1.ordered_quantity then
                    begin
                        update  xxwc_wsh_shipping_stg
                        set     status = 'OUT_FOR_DELIVERY/PARTIAL_BACKORDER'
                        where   header_id = c1.header_id
                        and     line_id = c1.line_id
                        and     delivery_id = l_new_delivery;
                    exception
                    when others then
                        null;
                    end;

--Added for Rev 3.4 Begin
                    BEGIN
            UPDATE XXWC.XXWC_OE_OPEN_ORDER_LINES
               SET ordered_quantity = c1.ordered_quantity - l_new_transaction_qty,
                   last_update_Date = sysdate,
                 last_updated_by = fnd_global.user_id,
                 last_update_login = fnd_global.login_id
               WHERE line_id = c1.line_id;

          EXCEPTION WHEN OTHERS THEN
            NULL;
          END;
--Added for Rev 3.4 End

                elsif l_new_transaction_qty >= c1.ordered_quantity then
                    begin
                        update  xxwc_wsh_shipping_stg
                        set     status = 'OUT_FOR_DELIVERY'
                        where   header_id = c1.header_id
                        and     line_id = c1.line_id
                        and     delivery_id = l_new_delivery;
                    exception
                    when others then
                        null;
                    end;

--Added for Rev 3.4 Begin
        BEGIN
          DELETE FROM XXWC.XXWC_OE_OPEN_ORDER_LINES
           WHERE line_id = c1.line_id;
        EXCEPTION WHEN OTHERS THEN
          NULL;
        END;
--Added for Rev 3.4 End
                else
                    begin
                        update  xxwc_wsh_shipping_stg
                        set     status = 'UNKNOWN STAT BASED ON NUMBERS'
                        where   header_id = c1.header_id
                        and     line_id = c1.line_id
                        and     delivery_id = l_new_delivery;
                    exception
                    when others then
                        null;
                    end;
                end if;

                -- Update Order Line
                l_log_sequence := l_log_sequence + 1;
                update_so_line ( c1.header_id
                                  , c1.line_id
                                  , 'GeneralPCK'
                                  , l_log_sequence
                                  , l_return_status
                                  , l_log_sequence );

                -- Call function to recreate the reservation
                if c1.lot_control_code = 2 then
                    l_log_sequence := l_log_sequence + 1;

                    create_reservation (c1.header_id
                                       , c1.line_id
                                       , l_new_delivery
                                       , 'GeneralPCK'
                                       , l_log_sequence
                                       , l_log_sequence);
                end if;

            ELSE

                begin
                    update  xxwc_wsh_shipping_stg
                    set     status = 'SUBINV_TRANSFER_ERR'
                    where   header_id = c1.header_id
                    and     line_id = c1.line_id
                    and     delivery_id = l_new_delivery;
                exception
                when others then
                    null;
                end;

                l_log_sequence := l_log_sequence + 1;
                write_log ( c1.header_id
                            , c1.line_id
                            , substr('Subinv Transfer Err. l_msg_data: '||replace(l_msg_data,chr(10), ' '), 1, 4000)
                            , l_log_sequence);

            END IF;

        ELSE

            IF c1.lot_control_code = 2 AND c1.force_ship_qty is not null then

                IF c1.force_ship_qty = 0 THEN

                    -- 04/22/2013 CG: TMS 20130422-00895 To correct issue with Lot SUBINV_TRANSFER_ERR errors
                    begin
                        update  xxwc_wsh_shipping_stg
                        set     status = 'BACKORDERED'
                                , delivery_id = null
                        where   header_id = c1.header_id
                        and     line_id = c1.line_id
                        and     delivery_id = l_new_delivery;
                    exception
                    when others then
                        null;
                    end;

                    begin
                        update  XXWC.XXWC_WSH_SHIPPING_LOTS_STG
                        set     delivery_id = null
                        where   header_id = c1.header_id
                        and     line_id = c1.line_id
                        and     delivery_id = l_new_delivery;
                    exception
                    when others then
                        null;
                    end;
                    -- 04/22/2013 CG
                ELSIF c1.force_ship_qty != l_lot_quantity and c1.force_ship_qty != c1.ordered_quantity THEN
                    begin
                        update  xxwc_wsh_shipping_stg
                        set     status = 'OUT_FOR_DELIVERY/PARTIAL_BACKORDER'
                        where   header_id = c1.header_id
                        and     line_id = c1.line_id
                        and     delivery_id = l_new_delivery;
                    exception
                    when others then
                        null;
                    end;

--Added for Rev 3.4 Begin
          BEGIN
            UPDATE XXWC.XXWC_OE_OPEN_ORDER_LINES
               SET ordered_quantity = c1.ordered_quantity - c1.force_ship_qty,
                 last_update_Date = sysdate,
                          last_updated_by = fnd_global.user_id,
                     last_update_login = fnd_global.login_id
               WHERE line_id = c1.line_id;
          EXCEPTION WHEN OTHERS THEN
            NULL;
          END;
--Added for Rev 3.4 End
                ELSIF c1.force_ship_qty != l_lot_quantity and c1.force_ship_qty = c1.ordered_quantity THEN
                    begin
                        update  xxwc_wsh_shipping_stg
                        set     status = 'OUT_FOR_DELIVERY'
                        where   header_id = c1.header_id
                        and     line_id = c1.line_id
                        and     delivery_id = l_new_delivery;
                    exception
                    when others then
                        null;
                    end;
--Added for Rev 3.4 Begin
        BEGIN
          DELETE FROM XXWC.XXWC_OE_OPEN_ORDER_LINES
           WHERE line_id = c1.line_id;
        EXCEPTION WHEN OTHERS THEN
          NULL;
        END;
--Added for Rev 3.4 End
                END IF;
            ELSE

                IF nvl(c1.mst_item_type,'UNKNOWN') in ('INTANGIBLE','FRT','PTO') THEN

                    begin
                        update  xxwc_wsh_shipping_stg
                        set     status = 'OUT_FOR_DELIVERY'
                                , transaction_qty = nvl(c1.force_ship_qty, c1.ordered_quantity)
                        where   header_id = c1.header_id
                        and     line_id = c1.line_id
                        and     delivery_id = l_new_delivery;
                    exception
                    when others then
                        null;
                    end;
--Added for Rev 3.4 Begin
        BEGIN
          DELETE FROM XXWC.XXWC_OE_OPEN_ORDER_LINES
           WHERE line_id = c1.line_id;
        EXCEPTION WHEN OTHERS THEN
          NULL;
        END;
--Added for Rev 3.4 End

                    commit;

                    -- Update Order Line
                    l_log_sequence := l_log_sequence + 1;
                    update_so_line ( c1.header_id
                                      , c1.line_id
                                      , 'GeneralPCK'
                                      , l_log_sequence
                                      , l_return_status
                                      , l_log_sequence );

                ELSE

                    if l_new_transaction_qty = 0 then
                        begin
                            update  xxwc_wsh_shipping_stg
                            set     status = 'BACKORDERED'
                                    , delivery_id = null
                            where   header_id = c1.header_id
                            and     line_id = c1.line_id
                            and     delivery_id = l_new_delivery;
                        exception
                        when others then
                            null;
                        end;

                        begin
                            update  XXWC.XXWC_WSH_SHIPPING_LOTS_STG
                            set     delivery_id = null
                            where   header_id = c1.header_id
                            and     line_id = c1.line_id
                            and     delivery_id = l_new_delivery;
                        exception
                        when others then
                            null;
                        end;
                    end if;

                END IF;

            END IF;

        END IF;

    end loop;

    commit;

    p_delivery_id := l_new_delivery;

    commit;
exception
when others then
    l_msg_data := substr(SQLERRM, 1, 4000);
    write_log ( p_header_id
                , 99999999999999999
                , substr('Error in main procedure: '||l_msg_data, 1, 4000)
                , 99999999);
end order_subinv_transf;


   PROCEDURE ship_confirm_order (p_header_id       IN     NUMBER
                               , p_from_org_id     IN     NUMBER
                               , p_delivery_id     IN     NUMBER
                               , p_pick_rule       IN     VARCHAR2
                               , p_return_status      OUT VARCHAR2
                               , p_return_msg         OUT VARCHAR2)
/*************************************************************************
   Module Name: ship_confirm_order
   PURPOSE:   Ship Confirm the delivery
   REVISIONS:
   Ver        Date        Author                     Description
   ---------  ----------  ---------------         -------------------------
   1.0        01/21/2013  Consuelo Gonzalez       Initial Version
   3.4        02/25/2015  Manjula Chellappan      TMS #20151023-00037 - Shipping Extension Modification
                                                      for Counter Order Removal and PUBD
   **************************************************************************/
   IS
      l_return_status             VARCHAR2 (1);
      l_msg_count                 NUMBER (15);
      l_msg_data                  VARCHAR2 (2000);
      l_count                     NUMBER (15);
      l_msg_data_out              VARCHAR2 (2000);
      l_mesg                      VARCHAR2 (2000);
      p_count                     NUMBER (15);
      p_new_batch_id              NUMBER;
      l_rule_id                   NUMBER;
      l_rule_name                 VARCHAR2 (2000);
      l_batch_prefix              VARCHAR2 (2000);
      l_batch_info_rec            wsh_picking_batches_pub.batch_info_rec;
      l_request_id                NUMBER;
      v_phase                     VARCHAR2 (50);
      v_status                    VARCHAR2 (50);
      v_dev_status                VARCHAR2 (50);
      v_dev_phase                 VARCHAR2 (50);
      v_message                   VARCHAR2 (250);
      v_error_message             VARCHAR2 (3000);
      v_wait                      BOOLEAN;
      l_open_deliveries           NUMBER;
      changed_attributes          WSH_DELIVERY_DETAILS_PUB.ChangedAttributeTabType;
      i                           NUMBER := 0;
      x_msg_summary               VARCHAR2 (2000);
      x_msg_details               VARCHAR2 (2000);
      x_msg_count                 NUMBER;
      l_delivery_id               NUMBER;


      v_released_status           wsh_delivery_details.released_status%TYPE;
      v_inv_interfaced_flag       wsh_delivery_details.inv_interfaced_flag%TYPE;
      v_oe_interfaced_flag        wsh_delivery_details.oe_interfaced_flag%TYPE;
      v_source_code               wsh_delivery_details.source_code%TYPE;
      v_pending_interface_flag    wsh_trip_stops.pending_interface_flag%TYPE;
      -- Parameters for WSH_DELIVERIES_PUB
      p_delivery_name             VARCHAR2 (30);
      p_action_code               VARCHAR2 (15);
      p_asg_trip_id               NUMBER;
      p_asg_trip_name             VARCHAR2 (30);
      p_asg_pickup_stop_id        NUMBER;
      p_asg_pickup_loc_id         NUMBER;
      p_asg_pickup_loc_code       VARCHAR2 (30);
      p_asg_pickup_arr_date       DATE;
      p_asg_pickup_dep_date       DATE;
      p_asg_dropoff_stop_id       NUMBER;
      p_asg_dropoff_loc_id        NUMBER;
      p_asg_dropoff_loc_code      VARCHAR2 (30);
      p_asg_dropoff_arr_date      DATE;
      p_asg_dropoff_dep_date      DATE;
      p_sc_action_flag            VARCHAR2 (10);
      p_sc_intransit_flag         VARCHAR2 (10);
      p_sc_close_trip_flag        VARCHAR2 (10);
      p_sc_create_bol_flag        VARCHAR2 (10);
      p_sc_stage_del_flag         VARCHAR2 (10);
      p_sc_trip_ship_method       VARCHAR2 (30);
      p_sc_actual_dep_date        VARCHAR2 (30);
      p_sc_report_set_id          NUMBER;
      p_sc_report_set_name        VARCHAR2 (60);
      p_sc_defer_interface_flag   VARCHAR2 (60);
      p_sc_send_945_flag          VARCHAR2 (60);
      p_sc_rule_id                NUMBER;
      p_sc_rule_name              VARCHAR2 (60);
      p_wv_override_flag          VARCHAR2 (10);
      p_asg_pickup_stop_seq       NUMBER;
      p_asg_dropoff_stop_seq      NUMBER;
      x_trip_id                   VARCHAR2 (30);
      x_trip_name                 VARCHAR2 (30);
      fail_api                    EXCEPTION;
      x_debug_file                VARCHAR2 (100);
      l_ship_method_code          VARCHAR2 (100);
      l_user_id                   NUMBER;
      l_resp_id                   NUMBER;
      l_appl_id                   NUMBER;
      l_debug_data                VARCHAR2 (2000);
      l_log_seq                   NUMBER := 0;

      -- 09/24/2013 CG: TMS 20130910-00906: Added to handle multiple delivery detail lines per order line
      l_prev_line_id              NUMBER;
      l_remaining_qty             NUMBER;

       --Added Cursor 10/3/2013 by Lee Spitzer TMS Ticket 20131011-00343
      CURSOR c_on_hand (p_delivery_name IN VARCHAR2) IS
        SELECT DISTINCT moqd.onhand_quantities_id
                       ,moqd.inventory_item_id               -- inventory_item_id
                       ,moqd.organization_id                 -- organization_id
                       ,moqd.subinventory_code               -- subinventory_code
                       ,moqd.locator_id                      -- location_id
                       ,moqd.revision                        -- revision
                       ,moqd.orig_date_received              -- orig_date_received
                       ,moqd.primary_transaction_quantity    -- quantity
          FROM   mtl_onhand_quantities_detail moqd,
                 wsh_delivery_details wdd,
                 wsh_delivery_assignments wda,
                 wsh_new_deliveries wnd
          WHERE  wdd.delivery_detail_id = wda.delivery_detail_id
          AND    wdd.inventory_item_id = moqd.inventory_item_id
          AND    wdd.organization_id = moqd.organization_id
          AND    wdd.subinventory = moqd.subinventory_code
          AND    nvl(wdd.locator_id,-1) = nvl(moqd.locator_id,-1)
          AND    nvl(wdd.LOT_NUMBER,'~') = nvl(moqd.lot_number,'~')
          AND    nvl(wdd.revision,'~') = nvl(moqd.revision,'~')
          AND    wda.delivery_id = wnd.delivery_id
          AND    wnd.name = p_delivery_name;


   BEGIN
      write_log (
         p_header_id
       , NULL
       , SUBSTR ('Entering pick_release_order for header_id: ' || p_header_id
               , 1
               , 4000)
       , l_log_seq + 1);

      -- Mandatory initialization for R12
      mo_global.set_policy_context ('S', 162);
      mo_global.init ('ONT');


      BEGIN
         SELECT wpr.picking_rule_id, wpr.NAME
           INTO l_rule_id, l_rule_name
           FROM wsh_picking_rules wpr
          WHERE     wpr.organization_id = p_from_org_id
                AND wpr.NAME LIKE p_pick_rule
                AND TRUNC (wpr.start_date_active) <= TRUNC (SYSDATE)
                AND NVL (TRUNC (wpr.end_date_active), TRUNC (SYSDATE + 1)) >
                       TRUNC (SYSDATE)
                AND ROWNUM = 1;
      EXCEPTION
         WHEN OTHERS
         THEN
            l_rule_id := NULL;
            l_rule_name := NULL;
      END;

      l_debug_data := 'PR Exist';

      IF l_rule_id IS NOT NULL
      THEN
         -- l_batch_info_rec.backorders_only_flag := 'I';
         -- l_batch_info_rec.existing_rsvs_only_flag := 'N';
         l_batch_info_rec.order_header_id := p_header_id;
         --         l_batch_info_rec.from_scheduled_ship_date := NULL;
         l_batch_info_rec.organization_id := p_from_org_id;
         --         l_batch_info_rec.include_planned_lines := 'N';
         --         l_batch_info_rec.autocreate_delivery_flag := 'Y';
         --         l_batch_info_rec.autodetail_pr_flag := 'Y';
         --         l_batch_info_rec.allocation_method := 'I';
         --         l_batch_info_rec.pick_from_locator_id := NULL;
         --         l_batch_info_rec.auto_pick_confirm_flag := 'Y';
         --         l_batch_info_rec.autopack_flag := 'Y';
         --l_batch_info_rec.append_flag := 'Y';
         l_batch_prefix := NULL;

         write_log (
            p_header_id
          , NULL
          , SUBSTR ('Creating Batch for header_id: ' || p_header_id, 1, 4000)
          , l_log_seq + 1);

         wsh_picking_batches_pub.create_batch (
            p_api_version     => 1.0
          , p_init_msg_list   => fnd_api.g_true
          , p_commit          => fnd_api.g_true
          , x_return_status   => l_return_status
          , x_msg_count       => l_msg_count
          , x_msg_data        => l_msg_data
          , p_rule_id         => l_rule_id
          , p_rule_name       => l_rule_name
          , p_batch_rec       => l_batch_info_rec
          , p_batch_prefix    => l_batch_prefix
          , x_batch_id        => p_new_batch_id);

         IF l_return_status = 'S'
         THEN
            write_log (
               p_header_id
             , NULL
             , SUBSTR (
                  'Pick Release Batch Got Created Sucessfully '
                  || p_new_batch_id
                , 1
                , 4000)
             , l_log_seq + 1);

            l_debug_Data := l_debug_data || '::create batch S';
            commit;
         ELSE
            write_log (p_header_id
                     , NULL
                     , SUBSTR ('Message count ' || l_msg_count, 1, 4000)
                     , l_log_seq + 1);

            IF l_msg_count = 1
            THEN
               write_log (p_header_id
                        , NULL
                        , SUBSTR ('Error Message ' || l_msg_data, 1, 4000)
                        , l_log_seq + 1);
            ELSIF l_msg_count > 1
            THEN
               LOOP
                  p_count := p_count + 1;
                  l_msg_data :=
                     fnd_msg_pub.get (fnd_msg_pub.g_next, fnd_api.g_false);
                  write_log (
                     p_header_id
                   , NULL
                   , SUBSTR (
                        'Error Message (' || p_count || '): ' || l_msg_data
                      , 1
                      , 4000)
                   , l_log_seq + 1);
               END LOOP;
            END IF;

            l_debug_Data := l_debug_data || '::create batch F';
            RAISE fail_api;
         END IF;

         write_log (
            p_header_id
          , NULL
          , SUBSTR ('Submitting Pick Release for Batch ' || p_new_batch_id
                  , 1
                  , 4000)
          , l_log_seq + 1);

         -- Release the batch Created Above
         IF l_return_status = 'S'
         THEN
            wsh_picking_batches_pub.release_batch (
               p_api_version     => 1.0
             , p_init_msg_list   => fnd_api.g_true
             , p_commit          => fnd_api.g_true
             , x_return_status   => l_return_status
             , x_msg_count       => l_msg_count
             , x_msg_data        => l_msg_data
             , p_batch_id        => p_new_batch_id
             , p_batch_name      => NULL
             , p_log_level       => 1
             , p_release_mode    => 'ONLINE'--'CONCURRENT'
             ,                                       -- (ONLINE or CONCURRENT)
              x_request_id       => l_request_id);

            IF l_return_status = 'S'
            THEN
               l_debug_Data := l_debug_data || '::Rel batch S';
               commit;
               write_log (
                  p_header_id
                , NULL
                , SUBSTR ('Pick Selection List Generation ' || l_request_id||':'||p_new_batch_id
                        , 1
                        , 4000)
                , l_log_seq + 1);
               /*
               v_wait :=
                  fnd_concurrent.wait_for_request (l_request_id
                                                 , 6
                                                 , 3600
                                                 , v_phase
                                                 , v_status
                                                 , v_dev_phase
                                                 , v_dev_status
                                                 , v_message);
               */
               write_log (
                  p_header_id
                , NULL
                , SUBSTR ('Pick Program Completion Status: ' || v_status
                        , 1
                        , 4000)
                , l_log_seq + 1);


               -- 09/24/2013 CG: TMS 20130910-00906: Added to handle multiple delivery detail lines per order line
               l_prev_line_id   := null;
               l_remaining_qty  := null;
               FOR del_rec
                  IN (SELECT a.ROWID
                           , a.header_id
                           , a.delivery_id
                           , a.line_id
                           , a.transaction_qty
                           , c.lot_quantity
                           , b.delivery_detail_id
                           -- 09/24/2013 CG: TMS 20130910-00906: Added to handle multiple delivery detail lines per order line
                           , b.requested_quantity
                        FROM XXWC_WSH_SHIPPING_STG a
                             , wsh_delivery_details b
                             , XXWC.XXWC_WSH_SHIPPING_LOTS_STG c
                       WHERE     a.delivery_id = p_delivery_id
                             AND a.header_id = p_header_id
                             AND a.header_id = b.source_header_id
                             AND a.line_id = b.source_line_id
                             AND b.source_code = 'OE'
                             AND b.batch_id = p_new_batch_id
                             AND b.released_status = 'Y'
                             AND a.status <> 'DELIVERED'
                             AND b.lot_number = c.lot_number(+)
                             AND b.source_header_id = c.header_id(+)
                             AND b.source_line_id = c.line_id (+)
                        order by a.line_id)
               LOOP

                  -- 09/24/2013 CG: TMS 20130910-00906: Added to handle multiple delivery detail lines per order line
                  if l_prev_line_id is null or l_prev_line_id != del_rec.line_id
                  then
                    l_remaining_qty := nvl(del_rec.lot_quantity, del_rec.transaction_qty);
                  end if;

                  if del_rec.requested_quantity < l_remaining_qty then
                    i := i + 1;
                    changed_attributes (i).delivery_detail_id := del_rec.delivery_detail_id; -- Ship All quantities in this detail.
                    changed_attributes (i).shipped_quantity := del_rec.requested_quantity;
                    l_remaining_qty := l_remaining_qty - del_rec.requested_quantity;
                  else
                    i := i + 1;
                    changed_attributes (i).delivery_detail_id := del_rec.delivery_detail_id; -- Ship All quantities in this detail.
                    changed_attributes (i).shipped_quantity := l_remaining_qty;
                  end if;

                  l_prev_line_id := del_rec.line_id;

                  /*
                  i := i + 1;
                  changed_attributes (i).delivery_detail_id :=
                     del_rec.delivery_detail_id; -- Ship All quantities in this detail.
                  -- changed_attributes (i).shipped_quantity := del_rec.transaction_qty;
                  changed_attributes (i).shipped_quantity := nvl(del_rec.lot_quantity, del_rec.transaction_qty);*/

                  UPDATE XXWC_WSH_SHIPPING_STG
                     SET delivery_detail_id = del_rec.delivery_detail_id
                   WHERE ROWID = del_rec.ROWID;
               END LOOP;

               write_log (
                  p_header_id
                , NULL
                , SUBSTR ('After Getting WDD:Line Count ' || i, 1, 4000)
                , l_log_seq + 1);

               IF i <> 0
               THEN
                  l_debug_Data := l_debug_data || '::Call chgatt';
                  write_log (
                     p_header_id
                   , NULL
                   , SUBSTR ('Before Call WDD Update:Line Count ' || i
                           , 1
                           , 4000)
                   , l_log_seq + 1);

                  WSH_DELIVERY_DETAILS_PUB.Update_Shipping_Attributes (
                     p_api_version_number   => 1.0
                   , p_init_msg_list        => fnd_api.g_true
                   , p_commit               => fnd_api.g_true
                   , x_return_status        => l_return_status
                   , x_msg_count            => l_msg_count
                   , x_msg_data             => l_msg_data
                   , p_changed_attributes   => changed_attributes
                   , p_source_code          => 'OE'
                   , p_container_flag       => 'N');

                   commit;
                  write_log (
                     p_header_id
                   , NULL
                   , SUBSTR (
                        'After Call WDD Update:Status ' || l_return_status
                      , 1
                      , 4000)
                   , l_log_seq + 1);

                  IF (l_return_status <> WSH_UTIL_CORE.G_RET_STS_SUCCESS)
                  THEN
                     RAISE fail_api;
                  END IF;

                  BEGIN
                     SELECT shipping_method_code
                       INTO l_ship_method_code
                       FROM oe_order_headers
                      WHERE header_id = p_header_id;
                  EXCEPTION
                     WHEN OTHERS
                     THEN
                        l_ship_method_code := NULL;
                  END;

                  write_log (p_header_id
                           , NULL
                           , SUBSTR ('Before Call SC ', 1, 4000)
                           , l_log_seq + 1);



                  FOR sc_rec
                     IN (SELECT DISTINCT d.delivery_id, c.name
                           FROM XXWC_WSH_SHIPPING_STG a
                              , wsh_delivery_details b
                              , wsh_new_deliveries c
                              , wsh_delivery_Assignments d
                          WHERE     a.delivery_id = p_delivery_id
                                AND a.header_id = p_header_id
                                AND a.header_id = b.source_header_id
                                AND a.line_id = b.source_line_id
                                AND b.source_code = 'OE'
                                AND b.batch_id = p_new_batch_id
                                AND b.released_status = 'Y'
                                AND b.delivery_detail_id =
                                       d.delivery_detail_id
                                AND d.delivery_id = c.delivery_id
                                AND ROWNUM = 1)
                  LOOP
                     l_debug_Data := l_debug_data || '::Call SC';
                     p_action_code := 'CONFIRM'; -- The action code for ship confirm
                     p_sc_action_flag := 'B';        -- Ship entered quantity.
                     p_sc_intransit_flag := 'Y';
                     --In transit flag is set to 'Y' closes the pickup stop and sets the delivery in transit.
                     p_sc_close_trip_flag := 'Y'; -- Close the trip after ship confirm
                     p_sc_trip_ship_method := l_ship_method_code; -- The ship method code
                     p_sc_defer_interface_flag := 'N';
                     p_sc_stage_del_flag := 'Y';
                     p_sc_create_bol_flag := 'N';
                     p_wv_override_flag := 'N';

                     write_log (
                        p_header_id
                      , NULL
                      , SUBSTR (
                           'Inside Call SC:Delivery=' || sc_rec.delivery_id
                         , 1
                         , 4000)
                      , l_log_seq + 1);


                     wsh_deliveries_pub.delivery_action (
                        p_api_version_number        => 1.0
                      , p_init_msg_list             => fnd_api.g_true
                      , x_return_status             => l_return_status
                      , x_msg_count                 => l_msg_count
                      , x_msg_data                  => l_msg_data
                      , p_action_code               => p_action_code
                      , p_delivery_id               => sc_rec.delivery_id
                      , p_delivery_name             => sc_rec.name
                      , p_asg_trip_id               => p_asg_trip_id
                      , p_asg_trip_name             => p_asg_trip_name
                      , p_asg_pickup_stop_id        => p_asg_pickup_stop_id
                      , p_asg_pickup_loc_id         => p_asg_pickup_loc_id
                      , p_asg_pickup_stop_seq       => p_asg_pickup_stop_seq
                      , p_asg_pickup_loc_code       => p_asg_pickup_loc_code
                      , p_asg_pickup_arr_date       => p_asg_pickup_arr_date
                      , p_asg_pickup_dep_date       => p_asg_pickup_dep_date
                      , p_asg_dropoff_stop_id       => p_asg_dropoff_stop_id
                      , p_asg_dropoff_loc_id        => p_asg_dropoff_loc_id
                      , p_asg_dropoff_stop_seq      => p_asg_dropoff_stop_seq
                      , p_asg_dropoff_loc_code      => p_asg_dropoff_loc_code
                      , p_asg_dropoff_arr_date      => p_asg_dropoff_arr_date
                      , p_asg_dropoff_dep_date      => p_asg_dropoff_dep_date
                      , p_sc_action_flag            => p_sc_action_flag
                      , p_sc_intransit_flag         => p_sc_intransit_flag
                      , p_sc_close_trip_flag        => p_sc_close_trip_flag
                      , p_sc_create_bol_flag        => p_sc_create_bol_flag
                      , p_sc_stage_del_flag         => p_sc_stage_del_flag
                      , p_sc_trip_ship_method       => p_sc_trip_ship_method
                      , p_sc_actual_dep_date        => p_sc_actual_dep_date
                      , p_sc_report_set_id          => p_sc_report_set_id
                      , p_sc_report_set_name        => p_sc_report_set_name
                      , p_sc_defer_interface_flag   => p_sc_defer_interface_flag
                      , p_sc_send_945_flag          => p_sc_send_945_flag
                      , p_sc_rule_id                => p_sc_rule_id
                      , p_sc_rule_name              => p_sc_rule_name
                      , p_wv_override_flag          => p_wv_override_flag
                      , x_trip_id                   => x_trip_id
                      , x_trip_name                 => x_trip_name);

                     write_log (
                        p_header_id
                      , NULL
                      , SUBSTR ('Inside Call SC Status=' || l_return_status
                              , 1
                              , 4000)
                      , l_log_seq + 1);

                     IF (l_return_status NOT IN ('S', 'W'))
                     THEN

                         RAISE fail_api;

                     ELSE

                       IF upper(l_rule_name) like ('%INTERNAL%') THEN
                  --  TMS Ticket  20130826-00733  Correct issue with Born On Dates on internal orders Added 8/26/2013 by Lee Spitzer to resolve issue with Born on Date (BOD) Extension
                   -- Start Added 8/26/2013 by Lee Spitzer for BOD Issue
                       write_log (
                        p_header_id
                      , NULL
                      , SUBSTR (
                           'Inserting records into intercompany table with p_delivery_id =' || p_delivery_id
                         , 1
                         , 4000)
                      , l_log_seq + 1);

                            --Updating this to use a cursor instead to get distinct values 10/3/2013 TMS Ticket 20130826-00733

                             FOR r_on_hand IN c_on_hand(p_delivery_name => sc_rec.NAME)

                              LOOP

                             /*-- insert records into staging table for shipments removed TMS Ticket 20131011-00343
                                 Insert Into XXWCINV_INTERCO_TRANSFERS
                                         (transaction_id
                                         ,inventory_item_id
                                         ,organization_id
                                         ,subinventory_code
                                         ,locator_id
                                         ,revision
                                         ,orig_date_received
                                         ,quantity
                                         ,creation_date
                                         ,shipment_number
                                         )
                                SELECT NULL
                                       ,moqd.inventory_item_id               -- inventory_item_id
                                       ,moqd.organization_id                 -- organization_id
                                       ,moqd.subinventory_code               -- subinventory_code
                                       ,moqd.locator_id                      -- location_id
                                       ,moqd.revision                        -- revision
                                       ,moqd.orig_date_received              -- orig_date_received
                                       ,moqd.primary_transaction_quantity    -- quantity
                                       ,SYSDATE
                                       ,sc_rec.name
                                from   mtl_onhand_quantities_detail moqd,
                                       wsh_delivery_details wdd,
                                       wsh_delivery_assignments wda,
                                       wsh_new_deliveries wnd
                                where  wdd.delivery_detail_id = wda.delivery_detail_id
                                and    wdd.inventory_item_id = moqd.inventory_item_id
                                and    wdd.organization_id = moqd.organization_id
                                and    wdd.subinventory = moqd.subinventory_code
                                and    nvl(wdd.locator_id,-1) = nvl(moqd.locator_id,-1)
                                and    nvl(wdd.LOT_NUMBER,'~') = nvl(moqd.lot_number,'~')
                                and    nvl(wdd.revision,'~') = nvl(moqd.revision,'~')
                                and    wda.delivery_id = wnd.delivery_id
                                and    wnd.name = sc_rec.name;

                               */

                            -- Added TMS Ticket 20131011-00343
                            BEGIN

                                 Insert Into XXWCINV_INTERCO_TRANSFERS
                                         (transaction_id
                                         ,inventory_item_id
                                         ,organization_id
                                         ,subinventory_code
                                         ,locator_id
                                         ,revision
                                         ,orig_date_received
                                         ,quantity
                                         ,creation_date
                                         ,shipment_number
                                         )
                                VALUES (NULL
                                       ,r_on_hand.inventory_item_id               -- inventory_item_id
                                       ,r_on_hand.organization_id                 -- organization_id
                                       ,r_on_hand.subinventory_code               -- subinventory_code
                                       ,r_on_hand.locator_id                      -- location_id
                                       ,r_on_hand.revision                        -- revision
                                       ,r_on_hand.orig_date_received              -- orig_date_received
                                       ,r_on_hand.primary_transaction_quantity    -- quantity
                                       ,SYSDATE
                                       ,sc_rec.NAME
                                        );



                                Exception
                                  When OTHERS Then
                                    ROLLBACK;
                                    NULL;
                                  write_log (
                                        p_header_id
                                      , NULL
                                      , SUBSTR (
                                           'When others exception inserting records into intercompany table =' || SQLCODE || SQLERRM
                                         , 1
                                         , 4000)
                                      , l_log_seq + 1);


                             End;

                           -- COMMIT;

                        --End TMS Ticket  20130826-00733 8/26/2013 by Lee Spitzer for BOD Issue

                        END LOOP;

                        COMMIT;

                        END IF;


                        FOR upd_rec
                           IN (SELECT a.ROWID
                                 FROM XXWC_WSH_SHIPPING_STG a
                                    , wsh_delivery_details b
                                    , wsh_new_deliveries c
                                    , wsh_delivery_Assignments d
                                WHERE     a.delivery_id = p_delivery_id
                                      AND a.header_id = p_header_id
                                      AND a.header_id = b.source_header_id
                                      AND a.line_id = b.source_line_id
                                      AND b.source_code = 'OE'
                                      AND b.batch_id = p_new_batch_id
                                      AND b.released_status = 'C'
                                      AND b.delivery_detail_id =
                                             d.delivery_detail_id
                                      AND d.delivery_id = c.delivery_id)
                        LOOP
                           UPDATE XXWC_WSH_SHIPPING_STG
                              SET status = 'DELIVERED'
                            WHERE ROWID = upd_rec.ROWID;
--Added for Rev 3.4 Begin
            BEGIN
              DELETE FROM XXWC.XXWC_OE_OPEN_ORDER_LINES
               WHERE line_id = (SELECT line_id
                                  FROM xxwc.xxwc_wsh_shipping_stg
                         WHERE ROWID = upd_rec.ROWID);
            EXCEPTION WHEN OTHERS THEN
              NULL;
            END;
--Added for Rev 3.4 End
                        END LOOP;

                        COMMIT;
                     END IF;
                  END LOOP;
               ELSE
                  p_return_status := 'E';
                  p_return_msg :=
                     l_Debug_Data || '::' || 'No Lines to Ship Confirm';
               END IF;
            ELSE
               IF l_msg_count = 1
               THEN
                  write_log (
                     p_header_id
                   , NULL
                   , SUBSTR ('Error Message: ' || l_msg_data, 1, 4000)
                   , l_log_seq + 1);
               ELSIF l_msg_count > 1
               THEN
                  LOOP
                     p_count := p_count + 1;
                     l_msg_data :=
                        fnd_msg_pub.get (fnd_msg_pub.g_next, fnd_api.g_false);

                     write_log (
                        p_header_id
                      , NULL
                      , SUBSTR (
                              'Error Message ('
                           || p_count
                           || '): '
                           || l_msg_data
                         , 1
                         , 4000)
                      , l_log_seq + 1);
                  END LOOP;
               END IF;
            END IF;
         ELSE
            write_log (
               p_header_id
             , NULL
             , SUBSTR (
                  'Could not find pick rule for header_id ' || p_header_id
                , 1
                , 4000)
             , l_log_seq + 1);
         END IF;
      END IF;

      write_log (
         p_header_id
       , NULL
       , SUBSTR ('Exiting pick_release_order for header_id ' || p_header_id
               , 1
               , 4000)
       , l_log_seq + 1);

      p_return_status := l_return_status;
      p_return_msg := l_Debug_Data || '::' || l_msg_data;
   EXCEPTION
      WHEN fail_api
      THEN
         WSH_UTIL_CORE.get_messages ('Y'
                                   , x_msg_summary
                                   , x_msg_details
                                   , x_msg_count);

         IF x_msg_count > 1
         THEN
            l_msg_data := x_msg_summary || x_msg_details;
         ELSE
            l_msg_data := x_msg_summary;
         END IF;

         p_return_status := l_return_status;
         p_return_msg := l_Debug_Data || '::' || l_msg_data;
      WHEN NO_DATA_FOUND
      THEN
         p_return_status := l_return_status;
         p_return_msg := 'NDF::' || l_Debug_Data || '::' || l_msg_data;
      WHEN OTHERS
      THEN
         p_return_status := l_return_status;
         p_return_msg := 'OTH::' || l_Debug_Data || '::' || l_msg_data;
   END ship_confirm_order;


procedure sync_order_staging (  p_header_id         IN NUMBER
                              , p_delivery_id       IN NUMBER
                              , p_ship_from_org_id  IN NUMBER
                              , p_return_status      OUT VARCHAR2
                              , p_return_msg        OUT VARCHAR2)
/**************************************************************************
   $Header xxwc_wsh_shipping_extn_pkg $
   Module Name: split_line
   PURPOSE:   This Procedure is used to Sync the order line and the staging table
   REVISIONS:
   Ver        Date        Author                     Description
   ---------  ----------  ---------------         -------------------------
   1.0        04/17/2013  Consuelo Gonzalez      Initial Version -TMS 20130422-01089
   3.4        03/03/2016  Manjula Chellappan    TMS #20151023-00037 - Shipping Extension Modification
                                                    for Counter Order Removal and PUBD
   3.5        03/09/2016  Manjula Chellappan    TMS #20150622-00156 - Shipping Extension modification for PUBD
   **************************************************************************/
is
    cursor cur_deliver_data is
            select  x1.delivery_id
                    , x1.header_id
                    , x1.line_id
                    , x1.inventory_item_id cust_inventory_item_id
                    , x1.ship_from_org_id cust_ship_from_org_id
                    , x1.ordered_quantity cust_ord_qty
                    , x1.force_ship_qty cust_force_ship_qty
                    , x1.transaction_qty cust_trans_qty
                    , x1.status
                    , oh.order_number
                    , ol.line_number
                    , ol.flow_status_code
                    , nvl(ol.cancelled_flag, 'N') ol_cancelled_flag
                    , ol.ordered_item
                    , ol.inventory_item_id
                    , ol.ship_from_org_id ol_ship_from_org_id
                    -- 06/27/2013 CG: TMS 20130627-01130: Changed to handle UOM Conversion
                    -- , ol.ordered_quantity ol_ordered_quantity
                    , (ol.ordered_quantity
                         * po_uom_s.po_uom_convert (um.unit_of_measure
                                                    , msib.primary_unit_of_measure
                                                    , ol.inventory_item_id
                                                   )
                      ) ol_ordered_quantity
                    , ol.shipped_quantity ol_shipped_quantity
                    , ol.cancelled_quantity ol_cancelled_quantity
                    -- 06/27/2013 CG: TMS 20130627-01130: Changed to handle UOM Conversion
                    -- , to_number(ol.attribute11) ol_force_ship_qty
-- Commented for Ver 3.5 Begin
/*
                    , ( to_number(ol.attribute11)
                        * po_uom_s.po_uom_convert (um.unit_of_measure
                                                    , msib.primary_unit_of_measure
                                                    , ol.inventory_item_id
                                                   )
                       ) ol_force_ship_qty
*/
-- Commented for Ver 3.5 End
-- Added for Ver 3.5 Begin
                    , ( DECODE(NVL (x1.subinventory, ol.subinventory),'PUBD', ol.ordered_quantity,
             ( to_number(ol.attribute11)
                        * po_uom_s.po_uom_convert (um.unit_of_measure
                                                    , msib.primary_unit_of_measure
                                                    , ol.inventory_item_id
                                                   )
                       ))) ol_force_ship_qty
-- Added for Ver 3.5 End
                    , NVL(ol.subinventory,'General') ol_subinventory
                    , msib.lot_control_code -- 1: no control, 2: full control
                    , msib.primary_uom_code
                    , xxwc_mv_routines_pkg.get_onhand_qty (ol.inventory_item_id,
                                                           ol.ship_from_org_id,
                                                           NVL(ol.subinventory,'General'),
                                                           'QOH') Available_Onhand
                    , ( nvl( (select  sum(mr.reservation_quantity)
                            from    mtl_reservations mr
                                    , mtl_sales_orders mso
                            where   mr.demand_source_type_id in (inv_reservation_global.g_source_type_oe, inv_reservation_global.g_source_type_internal_ord)
                            and     mr.supply_source_type_id = inv_reservation_global.g_source_type_inv
                            and     mr.demand_source_line_id = ol.line_id
                            and     mr.subinventory_code = NVL(ol.subinventory,'General')
                            and     mr.demand_source_header_id = mso.sales_order_id
                            and     mso.segment1 = oh.order_number
                            and     mso.segment3 = 'ORDER ENTRY'),0) ) reservation_quantity
                    -- 06/27/2013 CG: TMS 20130627-01130: Added to handle UOM Conversion
                    , um.unit_of_measure ordered_uom
                    , x1.subinventory source_subinventory -- Added for Ver 3.5
            from    xxwc_wsh_shipping_stg x1
                    , oe_order_lines ol
                    , oe_order_headers oh
                    , mtl_system_items_b msib
                    -- 06/27/2013 CG: TMS 20130627-01130: Added to handle UOM Conversion
                    , mtl_units_of_measure_vl um
            where   x1.delivery_id = p_delivery_id
            and     x1.status in ('OUT_FOR_DELIVERY','OUT_FOR_DELIVERY/PARTIAL_BACKORDER')
            and     x1.line_id = ol.line_id
            and     x1.header_id = ol.header_id
            and     ol.header_id = oh.header_id
            and     ol.inventory_item_id = msib.inventory_item_id
            and     ol.ship_from_org_id = msib.organization_id
            -- 06/27/2013 CG: TMS 20130627-01130: Added to handle UOM Conversion
            and     ol.order_quantity_uom = um.uom_code;

    cursor cur_non_rel_delivery_data is
            select  x1.delivery_id
                    , x1.header_id
                    , x1.line_id
                    , x1.inventory_item_id cust_inventory_item_id
                    , x1.ship_from_org_id cust_ship_from_org_id
                    , x1.ordered_quantity cust_ord_qty
                    , x1.force_ship_qty cust_force_ship_qty
                    , x1.transaction_qty cust_trans_qty
                    , x1.status
                    , oh.order_number
                    , ol.line_number
                    , ol.flow_status_code
                    , nvl(ol.cancelled_flag, 'N') ol_cancelled_flag
                    , ol.ordered_item
                    , ol.inventory_item_id
                    , ol.ship_from_org_id ol_ship_from_org_id
                    -- 06/27/2013 CG: TMS 20130627-01130: Changed to handle UOM Conversion
                    -- , ol.ordered_quantity ol_ordered_quantity
                    , (ol.ordered_quantity
                         * po_uom_s.po_uom_convert (um.unit_of_measure
                                                    , msib.primary_unit_of_measure
                                                    , ol.inventory_item_id
                                                   )
                      ) ol_ordered_quantity
                    , ol.shipped_quantity ol_shipped_quantity
                    , ol.cancelled_quantity ol_cancelled_quantity
                    -- , to_number(ol.attribute11) ol_force_ship_qty
-- Commented for Ver 3.5 Begin
/*
                    , ( to_number(ol.attribute11)
                        * po_uom_s.po_uom_convert (um.unit_of_measure
                                                    , msib.primary_unit_of_measure
                                                    , ol.inventory_item_id
                                                   )
                       ) ol_force_ship_qty
*/
-- Commented for Ver 3.5 End
-- Added for Ver 3.5 Begin
                    , DECODE (NVL(x1.subinventory,ol.subinventory),'PUBD',ol.ordered_quantity,
              ( to_number(ol.attribute11)
                        * po_uom_s.po_uom_convert (um.unit_of_measure
                                                    , msib.primary_unit_of_measure
                                                    , ol.inventory_item_id
                                                   )
                       )) ol_force_ship_qty
-- Added for Ver 3.5 End
                    , NVL(ol.subinventory,'General') ol_subinventory
                    , msib.lot_control_code -- 1: no control, 2: full control
                    , msib.primary_uom_code
                    -- 06/27/2013 CG: TMS 20130627-01130: Added to handle UOM Conversion
                    , um.unit_of_measure ordered_uom
                    ,x1.subinventory source_subinventory -- Added for Ver 3.5
            from    xxwc_wsh_shipping_stg x1
                    , oe_order_lines ol
                    , oe_order_headers oh
                    , mtl_system_items_b msib
                    -- 06/27/2013 CG: TMS 20130627-01130: Added to handle UOM Conversion
                    , mtl_units_of_measure_vl um
            where   x1.header_id = p_header_id
            and     x1.delivery_id != p_delivery_id
            and     x1.status in ('OUT_FOR_DELIVERY','OUT_FOR_DELIVERY/PARTIAL_BACKORDER')
            and     x1.line_id = ol.line_id
            and     x1.header_id = ol.header_id
            and     ol.header_id = oh.header_id
            and     ol.inventory_item_id = msib.inventory_item_id
            and     ol.ship_from_org_id = msib.organization_id
            -- 06/27/2013 CG: TMS 20130627-01130: Added to handle UOM Conversion
            and     ol.order_quantity_uom = um.uom_code;

    cursor cur_remove_lot_reservation (i_header_id number, i_line_id number, i_delivery_id number) is
            select  x1.rowid rid
                    , x1.header_id
                    , x1.line_id
                    , x1.delivery_id
                    , x1.inventory_item_id
                    , x1.ship_from_org_id
                    , x1.lot_number
                    , x1.lot_quantity
                    , mr.reservation_id
                    , mr.demand_source_header_id
                    , mr.demand_source_line_id
                    , mr.subinventory_code
                    , mr.reservation_quantity
            from    XXWC.XXWC_WSH_SHIPPING_LOTS_STG x1
                    , mtl_reservations mr
            where   x1.header_id = i_header_id
            and     x1.line_id = i_line_id
            and     x1.delivery_id = i_delivery_id
            and     x1.reservation_id = mr.reservation_id
            and     mr.subinventory_code = 'GeneralPCK';

    l_return_status              VARCHAR2 (240);
    l_msg_data                   VARCHAR2 (4000);
    l_log_sequence               NUMBER;
    l_cur_status                 VARCHAR2(240);
    l_dummy_num                  NUMBER;
    l_perf_subvinv_transf        VARCHAR2(1);
    l_delivery_id                NUMBER;

    l_rsv       inv_reservation_global.mtl_reservation_rec_type;
    l_msg_count NUMBER;
    l_rsv_id    NUMBER;
    l_dummy_sn  inv_reservation_global.serial_number_tbl_type;
    l_status    VARCHAR2(1);
    g_miss_rsv_rec                  inv_reservation_global.mtl_reservation_rec_type;
    g_miss_serial_number            inv_reservation_global.serial_number_tbl_type;
    g_miss_mtl_reservations_tbl     inv_reservation_global.mtl_reservation_tbl_type;
begin

    write_log (p_header_id
                , 0
                , substr('Starting sync_order_staging processing for: '||p_header_id, 1, 4000)
                , 99999995);

    write_log (p_header_id
                , 0
                , substr('p_delivery_id: '||p_delivery_id||'. p_header_id : '||p_header_id ||'. p_ship_from_org_id: '||p_ship_from_org_id, 1, 4000)
                , 99999996);


    for c1 in cur_deliver_data
    loop
        exit when cur_deliver_data%notfound;
        l_log_sequence := 0;

        l_log_sequence := l_log_sequence + 1;
        write_log ( c1.header_id
                    , c1.line_id
                    , 'Starting sync process. Line Num: '||c1.line_number||'. Item: '||c1.ordered_item||'. Line ID: '||c1.line_id
                    , l_log_sequence);

        l_log_sequence := l_log_sequence + 1;
        write_log ( c1.header_id
                    , c1.line_id
                    , 'cust_ship_from_org_id: '||c1.cust_ship_from_org_id||'. ol_ship_from_org_id: '||c1.ol_ship_from_org_id
                    , l_log_sequence);

        l_log_sequence := l_log_sequence + 1;
        write_log ( c1.header_id
                    , c1.line_id
                    , 'cust_ord_qty: '||c1.cust_ord_qty||'. ol_ordered_quantity: '||c1.ol_ordered_quantity
                    , l_log_sequence);

        l_log_sequence := l_log_sequence + 1;
        write_log ( c1.header_id
                    , c1.line_id
                    , 'cust_force_ship_qty: '||c1.cust_force_ship_qty||'. ol_force_ship_qty: '||c1.ol_force_ship_qty
                    , l_log_sequence);

        l_log_sequence := l_log_sequence + 1;
        write_log ( c1.header_id
                    , c1.line_id
                    , 'cust_trans_qty: '||c1.cust_trans_qty
                    , l_log_sequence);

        -- Code for scenarios 1
        -- 1. If Force Ship DFF is not null then If Force Ship DFF and Transaction Quantity in Custom Table do not match then need to
        --    a. If Force Ship Less than Cust Transaction Qty then update transaction quantity to match force ship quantity and move additional material back to general
        --    b. If force ship quantity is greater than the transaction quantity transfer the additional quantity and update the cust rxn quantity
        -- 2. If Force Ship DFF is null then check to see if Ordered Quantity and Cust Tranaction Quantity match
        --    a. If ordered quantity not the same as transaction quantity then true up the inventory transactions and update cust transaction quantity
        --       i. NOTE: This only applies to lines where status = 'Out for Delivery'
        if (
            (c1.ol_ship_from_org_id = p_ship_from_org_id)
            AND
            (c1.ol_cancelled_flag = 'N')
            )
        then
            if c1.ol_force_ship_qty is not null then

                -- Point 1
                -- 04/04/13 CG: Added to remove transactions that were force shipped 0 before ship confirming
                if c1.ol_force_ship_qty = 0 then
                    l_log_sequence := l_log_sequence + 1;
                    write_log ( c1.header_id
                                , c1.line_id
                                , substr('Scenario 1c, Force ship quantity = 0, removing from delivery, updating quantity in staging, moving from GeneralPCK to General', 1, 4000)
                                , l_log_sequence);

                    l_log_sequence := l_log_sequence + 1;
                    write_log ( c1.header_id
                                , c1.line_id
                                , substr('Transferring quantity '||c1.cust_trans_qty, 1, 4000)
                                , l_log_sequence);

                    if c1.ol_subinventory = 'GeneralPCK' then


                        for c3 in cur_remove_lot_reservation (c1.header_id, c1.line_id, p_delivery_id)
                        loop
                            exit when cur_remove_lot_reservation%notfound;

                            l_log_sequence := l_log_sequence + 1;
                            write_log ( c1.header_id
                                        , c1.line_id
                                        , substr('Deleting reservation: '||c3.reservation_id||'. Lot: '||c3.lot_number||'. Subinv: '||c3.subinventory_code||'. Qty: '||c3.reservation_quantity, 1, 4000)
                                        , l_log_sequence);

                            l_rsv := g_miss_rsv_rec;

                            l_rsv.demand_source_type_id        := inv_reservation_global.g_source_type_oe; -- order entry
                            l_rsv.supply_source_type_id        := inv_reservation_global.g_source_type_inv;
                            l_rsv.reservation_id               := c3.reservation_id;
                            l_rsv.demand_source_header_id      := c3.demand_source_header_id;
                            l_rsv.demand_source_line_id        := c3.demand_source_line_id;
                            l_rsv.organization_id              := c1.cust_inventory_item_id;
                            l_rsv.inventory_item_id            := c1.cust_ship_from_org_id;
                            l_rsv.subinventory_code            := c3.subinventory_code;

                            inv_reservation_pub.delete_reservation (
                                                                       p_api_version_number        => 1.0
                                                                     , p_init_msg_lst              => fnd_api.g_true
                                                                     , x_return_status             => l_status
                                                                     , x_msg_count                 => l_msg_count
                                                                     , x_msg_data                  => l_msg_data
                                                                     , p_rsv_rec                   => l_rsv
                                                                     , p_serial_number             => l_dummy_sn
                                                                   );

                            l_log_sequence := l_log_sequence + 1;
                            write_log ( c1.header_id
                                        , c1.line_id
                                        , substr('l_status: '||l_status||'. l_msg_count: '||l_msg_count , 1, 4000)
                                        , l_log_sequence);

                            IF l_status = fnd_api.g_ret_sts_success THEN
                                l_log_sequence := l_log_sequence + 1;
                                write_log ( c1.header_id
                                            , c1.line_id
                                            , substr('Reservation deleted: '||c3.reservation_id, 1, 4000)
                                            , l_log_sequence);

                                -- Removing old reservation id
                                begin
                                    update  XXWC.XXWC_WSH_SHIPPING_LOTS_STG
                                    set     reservation_id = null
                                    where   reservation_id = c3.reservation_id;
                                exception
                                when others then
                                    null;
                                end;

                            ELSE
                                IF l_msg_count = 1 THEN
                                    l_log_sequence := l_log_sequence + 1;
                                    write_log ( c1.header_id
                                                , c1.line_id
                                                , substr('Error Deleting reservation: '||c3.reservation_id||'. l_msg_data: '||l_msg_data, 1, 4000)
                                                , l_log_sequence);
                                ELSE
                                    FOR l_index IN 1..l_msg_count LOOP
                                        fnd_msg_pub.get(l_index, 'T', l_msg_data, l_dummy_num);
                                        l_log_sequence := l_log_sequence + 1;
                                        write_log ( c1.header_id
                                                    , c1.line_id
                                                    , substr('Error Deleting reservation: '||c3.reservation_id||'.l_msg_data: '||l_msg_data, 1, 4000)
                                                    , l_log_sequence);
                                    END LOOP;
                                END IF;
                            END IF;

                        END LOOP; -- End loop for lots

                        subinv_transfer_by_qty (  c1.header_id
                                               , c1.line_id
                                               , p_delivery_id
                                               , 'GeneralPCK'
--                                             , 'General' -- Commented for Ver 3.5
                                               , c1.source_subinventory -- Added for Ver 3.5
                                               , c1.cust_trans_qty
                                               , l_log_sequence + 1
                                               , l_return_status
                                               , l_msg_data
                                               , l_log_sequence);

                        COMMIT;--Added on 11/13/2013 Ram Talluri and Rasikha Galimova TMS 20131009-00330

                        IF l_return_status = fnd_api.g_ret_sts_success THEN

                            l_log_sequence := l_log_sequence + 1;
                            write_log ( c1.header_id
                                        , c1.line_id
                                        , substr('Subinv transfer successfully completed...Updating transaction quantity to 0', 1, 4000)
                                        , l_log_sequence);

                            -- Update Order Line
                            l_log_sequence := l_log_sequence + 1;
                            update_so_line ( c1.header_id
                                              , c1.line_id
--                                            , 'General' -- Commented for Ver 3.5
                                              , c1.source_subinventory -- Added for Ver 3.5
                                              , l_log_sequence
                                              , l_return_status
                                              , l_log_sequence );

                            begin
                                update  xxwc_wsh_shipping_stg
                                set     status = 'BACKORDERED'
                                        , delivery_id = null
                                        , force_ship_qty = 0
                                        , transaction_qty = 0
                                where   header_id = c1.header_id
                                and     line_id = c1.line_id
                                and     delivery_id = p_delivery_id;
                            exception
                            when others then
                                null;
                            end;

                            begin
                                update  XXWC.XXWC_WSH_SHIPPING_LOTS_STG
                                set     delivery_id = null
                                where   header_id = c1.header_id
                                and     line_id = c1.line_id
                                and     delivery_id = p_delivery_id;
                            exception
                            when others then
                                null;
                            end;



                        ELSE
                            l_log_sequence := l_log_sequence + 1;
                            write_log ( c1.header_id
                                        , c1.line_id
                                        , substr('Subinv Transfer Err. l_msg_data: '||replace(l_msg_data,chr(10), ' '), 1, 4000)
                                        , l_log_sequence);

                            begin
                                update  xxwc_wsh_shipping_stg
                                set     status = 'ERR_SYNC_SUBINV_TRANSF_1C'
                                where   header_id = c1.header_id
                                and     line_id = c1.line_id
                                and     delivery_id = p_delivery_id;
                            exception
                            when others then
                                null;
                            end;
                        END IF;

                    end if;

                elsif c1.ol_force_ship_qty < c1.cust_trans_qty then
                    -- Point (a)

                    l_log_sequence := l_log_sequence + 1;
                    write_log ( c1.header_id
                                , c1.line_id
                                , substr('Scenario 1a, moving extra quantity from GeneralPCK to General', 1, 4000)
                                , l_log_sequence);

                    l_log_sequence := l_log_sequence + 1;
                    write_log ( c1.header_id
                                , c1.line_id
                                , substr('Transferring quantity '||(c1.cust_trans_qty - c1.ol_force_ship_qty), 1, 4000)
                                , l_log_sequence);

                    subinv_transfer_by_qty (  c1.header_id
                                               , c1.line_id
                                               , p_delivery_id
                                               , 'GeneralPCK'
--                                             , 'General' -- Commented for Ver 3.5
                                               , c1.source_subinventory -- Added for Ver 3.5
                                               , (c1.cust_trans_qty - c1.ol_force_ship_qty)
                                               , l_log_sequence + 1
                                               , l_return_status
                                               , l_msg_data
                                               , l_log_sequence);

                    IF l_return_status = fnd_api.g_ret_sts_success THEN

                        l_log_sequence := l_log_sequence + 1;
                        write_log ( c1.header_id
                                    , c1.line_id
                                    , substr('Subinv transfer successfully completed...Updating transaction quantity to '||c1.ol_force_ship_qty, 1, 4000)
                                    , l_log_sequence);

                        begin
                            update  xxwc_wsh_shipping_stg
                            set     transaction_qty = c1.ol_force_ship_qty
                                    -- Added to sync
                                    , ordered_quantity = c1.ol_ordered_quantity
                                    , force_ship_qty = c1.ol_force_ship_qty
                            where   header_id = c1.header_id
                            and     line_id = c1.line_id
                            and     delivery_id = p_delivery_id;
                        exception
                        when others then
                            null;
                        end;

                    ELSE

                        l_log_sequence := l_log_sequence + 1;
                        write_log ( c1.header_id
                                    , c1.line_id
                                    , substr('Subinv Transfer Err. l_msg_data: '||replace(l_msg_data,chr(10), ' '), 1, 4000)
                                    , l_log_sequence);

                        begin
                            update  xxwc_wsh_shipping_stg
                            set     status = 'ERR_SYNC_SUBINV_TRANSF_1A'
                            where   header_id = c1.header_id
                            and     line_id = c1.line_id
                            and     delivery_id = p_delivery_id;
                        exception
                        when others then
                            null;
                        end;

                    END IF;

                elsif c1.ol_force_ship_qty > c1.cust_trans_qty then
                    -- Point (b)

                    l_log_sequence := l_log_sequence + 1;
                    write_log ( c1.header_id
                                , c1.line_id
                                , substr('Scenario 1b, moving extra quantity from General to GeneralPCK', 1, 4000)
                                , l_log_sequence);

                    l_log_sequence := l_log_sequence + 1;
                    write_log ( c1.header_id
                                , c1.line_id
                                , substr('Transferring quantity '||(c1.ol_force_ship_qty - c1.cust_trans_qty), 1, 4000)
                                , l_log_sequence);

                    subinv_transfer_by_qty (  c1.header_id
                                               , c1.line_id
                                               , p_delivery_id
--                                             , 'General' -- Commented for Ver 3.5
                                               , c1.source_subinventory -- Added for Ver 3.5
                                               , 'GeneralPCK'
                                               , (c1.ol_force_ship_qty - c1.cust_trans_qty)
                                               , l_log_sequence + 1
                                               , l_return_status
                                               , l_msg_data
                                               , l_log_sequence);

                    IF l_return_status = fnd_api.g_ret_sts_success THEN

                        l_log_sequence := l_log_sequence + 1;
                        write_log ( c1.header_id
                                    , c1.line_id
                                    , substr('Subinv transfer successfully completed...Updating transaction quantity to '||c1.ol_force_ship_qty, 1, 4000)
                                    , l_log_sequence);

                        begin
                            update  xxwc_wsh_shipping_stg
                            set     transaction_qty = c1.ol_force_ship_qty
                                    -- Added to sync
                                    , ordered_quantity = c1.ol_ordered_quantity
                                    , force_ship_qty = c1.ol_force_ship_qty
                            where   header_id = c1.header_id
                            and     line_id = c1.line_id
                            and     delivery_id = p_delivery_id;
                        exception
                        when others then
                            null;
                        end;

                        -- Update Order Line
                        l_log_sequence := l_log_sequence + 1;
                        update_so_line ( c1.header_id
                                          , c1.line_id
                                          , 'GeneralPCK'
                                          , l_log_sequence
                                          , l_return_status
                                          , l_log_sequence );

                        -- Call function to recreate the reservation
                        if c1.lot_control_code = 2 then
                            l_log_sequence := l_log_sequence + 1;

                            create_reservation (c1.header_id
                                               , c1.line_id
                                               , p_delivery_id
                                               , 'GeneralPCK'
                                               , l_log_sequence
                                               , l_log_sequence);
                        end if;

                    ELSE

                        l_log_sequence := l_log_sequence + 1;
                        write_log ( c1.header_id
                                    , c1.line_id
                                    , substr('Subinv Transfer Err. l_msg_data: '||replace(l_msg_data,chr(10), ' '), 1, 4000)
                                    , l_log_sequence);

                        begin
                            update  xxwc_wsh_shipping_stg
                            set     status = 'ERR_SYNC_SUBINV_TRANSF_1B'
                            where   header_id = c1.header_id
                            and     line_id = c1.line_id
                            and     delivery_id = p_delivery_id;
                        exception
                        when others then
                            null;
                        end;

                    END IF;

                end if;

            else
                -- Point 2
                if c1.status = 'OUT_FOR_DELIVERY' then
                    l_log_sequence := l_log_sequence + 1;
                    write_log ( c1.header_id
                                , c1.line_id
                                , substr('Scenario 2, Force ship is null and it is out for delivery', 1, 4000)
                                , l_log_sequence);

                    if c1.ol_ordered_quantity > c1.cust_trans_qty then

                        l_log_sequence := l_log_sequence + 1;
                        write_log ( c1.header_id
                                    , c1.line_id
                                    , substr('Scenario 2A, Order Quantity > Custom Table Transaction Qty. Transferring from General to GeneralPCK', 1, 4000)
                                    , l_log_sequence);

                        l_log_sequence := l_log_sequence + 1;
                        write_log ( c1.header_id
                                    , c1.line_id
                                    , substr('Transferring quantity '||(c1.ol_ordered_quantity - c1.cust_trans_qty), 1, 4000)
                                    , l_log_sequence);

                        -- Added to handle lots where the reservation quantity has been updated for something already in GeneralPCK
                        l_perf_subvinv_transf := null;
                        if c1.lot_control_code = 2 then

                            for c2 in cur_remove_lot_reservation (c1.header_id, c1.line_id, p_delivery_id)
                            loop
                                exit when cur_remove_lot_reservation%notfound;

                                l_log_sequence := l_log_sequence + 1;
                                write_log ( c1.header_id
                                            , c1.line_id
                                            , substr('Checking lot quantity against reservation quantity. lot_quantity: '||c2.lot_quantity||'. Reservation Qty: '||c2.reservation_quantity, 1, 4000)
                                            , l_log_sequence);

                                if c2.lot_quantity != c2.reservation_quantity then
                                    l_perf_subvinv_transf := 'N';

                                    begin
                                        update  xxwc_wsh_shipping_stg
                                        set     transaction_qty = c1.ol_ordered_quantity
                                                -- Added to sync
                                                , ordered_quantity = c1.ol_ordered_quantity
                                                , force_ship_qty = c1.ol_force_ship_qty
                                        where   header_id = c1.header_id
                                        and     line_id = c1.line_id
                                        and     delivery_id = p_delivery_id;
                                    exception
                                    when others then
                                        null;
                                    end;

                                    begin
                                        update  XXWC.XXWC_WSH_SHIPPING_LOTS_STG
                                        set     lot_quantity = c2.reservation_quantity
                                        where   rowid = c2.rid;
                                    exception
                                    when others then
                                        null;
                                    end;

                                else
                                    l_perf_subvinv_transf := 'Y';
                                end if;

                            end loop;

                        end if;

                        l_log_sequence := l_log_sequence + 1;
                        write_log ( c1.header_id
                                    , c1.line_id
                                    , substr('l_perf_subvinv_transf: '||l_perf_subvinv_transf, 1, 4000)
                                    , l_log_sequence);

                        if NVL (l_perf_subvinv_transf, 'Y') = 'Y' then

                            subinv_transfer_by_qty (  c1.header_id
                                                       , c1.line_id
                                                       , p_delivery_id
--                                                     , 'General' -- Commented for Ver 3.5
                             , c1.source_subinventory -- Added for Ver 3.5
                                                       , 'GeneralPCK'
                                                       , (c1.ol_ordered_quantity - c1.cust_trans_qty)
                                                       , l_log_sequence + 1
                                                       , l_return_status
                                                       , l_msg_data
                                                       , l_log_sequence);

                            IF l_return_status = fnd_api.g_ret_sts_success THEN

                                l_log_sequence := l_log_sequence + 1;
                                write_log ( c1.header_id
                                            , c1.line_id
                                            , substr('Subinv transfer successfully completed...Updating transaction quantity to '||c1.ol_ordered_quantity, 1, 4000)
                                            , l_log_sequence);

                                begin
                                    update  xxwc_wsh_shipping_stg
                                    set     transaction_qty = c1.ol_ordered_quantity
                                            -- Added to sync
                                            , ordered_quantity = c1.ol_ordered_quantity
                                            , force_ship_qty = c1.ol_force_ship_qty
                                    where   header_id = c1.header_id
                                    and     line_id = c1.line_id
                                    and     delivery_id = p_delivery_id;
                                exception
                                when others then
                                    null;
                                end;

                                -- Call function to recreate the reservation
                                if c1.lot_control_code = 2 then
                                    l_log_sequence := l_log_sequence + 1;

                                    create_reservation (c1.header_id
                                                       , c1.line_id
                                                       , p_delivery_id
                                                       , 'GeneralPCK'
                                                       , l_log_sequence
                                                       , l_log_sequence);
                                end if;

                            ELSE

                                l_log_sequence := l_log_sequence + 1;
                                write_log ( c1.header_id
                                            , c1.line_id
                                            , substr('Subinv Transfer Err. l_msg_data: '||replace(l_msg_data,chr(10), ' '), 1, 4000)
                                            , l_log_sequence);

                                begin
                                    update  xxwc_wsh_shipping_stg
                                    set     status = 'ERR_SYNC_SUBINV_TRANSF_2A'
                                    where   header_id = c1.header_id
                                    and     line_id = c1.line_id
                                    and     delivery_id = p_delivery_id;
                                exception
                                when others then
                                    null;
                                end;

                            END IF;

                        end if;

                    elsif c1.ol_ordered_quantity < c1.cust_trans_qty then

                        l_log_sequence := l_log_sequence + 1;
                        write_log ( c1.header_id
                                    , c1.line_id
                                    , substr('Scenario 2B, Order Quantity < Custom Table Transaction Qty. Transferring from GeneralPCK to General', 1, 4000)
                                    , l_log_sequence);

                        l_log_sequence := l_log_sequence + 1;
                        write_log ( c1.header_id
                                    , c1.line_id
                                    , substr('Transfering quantity '||(c1.cust_trans_qty - c1.ol_ordered_quantity), 1, 4000)
                                    , l_log_sequence);

                        subinv_transfer_by_qty (  c1.header_id
                                                   , c1.line_id
                                                   , p_delivery_id
                                                   , 'GeneralPCK'
--                                                 , 'General' -- Commented for Ver 3.5
                                                   , c1.source_subinventory -- Added for Ver 3.5
                                                   , (c1.cust_trans_qty - c1.ol_ordered_quantity)
                                                   , l_log_sequence + 1
                                                   , l_return_status
                                                   , l_msg_data
                                                   , l_log_sequence);

                        IF l_return_status = fnd_api.g_ret_sts_success THEN

                            l_log_sequence := l_log_sequence + 1;
                            write_log ( c1.header_id
                                        , c1.line_id
                                        , substr('Subinv transfer successfully completed...Updating transaction qty to '||c1.ol_ordered_quantity, 1, 4000)
                                        , l_log_sequence);

                            begin
                                update  xxwc_wsh_shipping_stg
                                set     transaction_qty = c1.ol_ordered_quantity
                                        -- Added to sync
                                        , ordered_quantity = c1.ol_ordered_quantity
                                        , force_ship_qty = c1.ol_force_ship_qty
                                where   header_id = c1.header_id
                                and     line_id = c1.line_id
                                and     delivery_id = p_delivery_id;
                            exception
                            when others then
                                null;
                            end;

                        ELSE

                            l_log_sequence := l_log_sequence + 1;
                            write_log ( c1.header_id
                                        , c1.line_id
                                        , substr('Subinv Transfer Err. l_msg_data: '||replace(l_msg_data,chr(10), ' '), 1, 4000)
                                        , l_log_sequence);

                            begin
                                update  xxwc_wsh_shipping_stg
                                set     status = 'ERR_SYNC_SUBINV_TRANSF_2B'
                                where   header_id = c1.header_id
                                and     line_id = c1.line_id
                                and     delivery_id = p_delivery_id;
                            exception
                            when others then
                                null;
                            end;

                        END IF;

                    elsif c1.lot_control_code = 2 and c1.cust_trans_qty > c1.reservation_quantity then

                        l_log_sequence := l_log_sequence + 1;
                        write_log ( c1.header_id
                                    , c1.line_id
                                    , substr('Scenario 2C, Lots Reservation Qty < Custom Table Transaction Qty. Transferring from GeneralPCK to General', 1, 4000)
                                    , l_log_sequence);

                        l_log_sequence := l_log_sequence + 1;
                        write_log ( c1.header_id
                                    , c1.line_id
                                    , substr('Transfering quantity '||(c1.cust_trans_qty - c1.reservation_quantity), 1, 4000)
                                    , l_log_sequence);

                        subinv_transfer_by_qty (  c1.header_id
                                                   , c1.line_id
                                                   , p_delivery_id
                                                   , 'GeneralPCK'
--                                                 , 'General' -- Commented for Ver 3.5
                                                   , c1.source_subinventory -- Added for Ver 3.5
                                                   , (c1.cust_trans_qty - c1.reservation_quantity)
                                                   , l_log_sequence + 1
                                                   , l_return_status
                                                   , l_msg_data
                                                   , l_log_sequence);

                        IF l_return_status = fnd_api.g_ret_sts_success THEN

                            l_log_sequence := l_log_sequence + 1;
                            write_log ( c1.header_id
                                        , c1.line_id
                                        , substr('Subinv transfer successfully completed...Updating transaction qty to '||c1.reservation_quantity, 1, 4000)
                                        , l_log_sequence);

                            begin
                                update  xxwc_wsh_shipping_stg
                                set     transaction_qty = c1.reservation_quantity
                                        -- Added to sync
                                        , ordered_quantity = c1.ol_ordered_quantity
                                        , force_ship_qty = c1.ol_force_ship_qty
                                        , status = 'OUT_FOR_DELIVERY/PARTIAL_BACKORDER'
                                where   header_id = c1.header_id
                                and     line_id = c1.line_id
                                and     delivery_id = p_delivery_id;
                            exception
                            when others then
                                null;
                            end;
--Added for Rev 3.4 Begin
            BEGIN
              UPDATE XXWC.XXWC_OE_OPEN_ORDER_LINES
                 SET ordered_quantity = c1.ol_ordered_quantity - c1.ol_force_ship_qty,
                   last_update_Date = sysdate,
                   last_updated_by = fnd_global.user_id,
                   last_update_login = fnd_global.login_id
               WHERE line_id = c1.line_id;
            EXCEPTION WHEN OTHERS THEN
              NULL;
            END;
--Added for Rev 3.4 End

                            -- need to update by lot true reservation quantity
                            for c2 in (select  x1.rowid rid
                                                , x1.header_id
                                                , x1.line_id
                                                , x1.delivery_id
                                                , x1.inventory_item_id
                                                , x1.ship_from_org_id
                                                , x1.lot_number
                                                , x1.lot_quantity
                                                , mr.reservation_id
                                                , mr.demand_source_header_id
                                                , mr.demand_source_line_id
                                                , mr.subinventory_code
                                                , mr.reservation_quantity
                                        from    XXWC.XXWC_WSH_SHIPPING_LOTS_STG x1
                                                , mtl_reservations mr
                                        where   x1.header_id = c1.header_id
                                        and     x1.line_id = c1.line_id
                                        and     x1.delivery_id = p_delivery_id
                                        and     x1.reservation_id = mr.reservation_id)
                            loop
                                --exit when cur_remove_lot_reservation%notfound;

                                l_log_sequence := l_log_sequence + 1;
                                write_log ( c1.header_id
                                            , c1.line_id
                                            , substr('Updating reservation_id: '||c2.reservation_id||'. lot_number: '||c2.lot_number||'. lot_quantity: '||c2.lot_quantity||'. reservation_quantity: '||c2.reservation_quantity, 1, 4000)
                                            , l_log_sequence);

                                begin
                                    update  XXWC.XXWC_WSH_SHIPPING_LOTS_STG
                                    set     lot_quantity = c2.reservation_quantity
                                    where   rowid = c2.rid;
                                exception
                                when others then
                                    null;
                                end;

                            end loop;

                            commit;

                            l_log_sequence := l_log_sequence + 1;
                            write_log ( c1.header_id
                                        , c1.line_id
                                        , substr('Calling split line procedure', 1, 4000)
                                        , l_log_sequence);

                            split_line(c1.header_id,
                                       c1.ol_ship_from_org_id,
                                       fnd_global.user_id,
                                       fnd_global.resp_id,
                                       fnd_global.resp_appl_id,
                                       l_return_status,
                                       l_msg_data);

                            l_delivery_id := null;
                            IF l_return_status = 'S' THEN
                                l_delivery_id := p_delivery_id;
                                order_subinv_transf (
                                                     c1.header_id
                                                     , c1.ol_ship_from_org_id
                                                     , fnd_global.user_id
                                                     , fnd_global.resp_id
                                                     , fnd_global.resp_appl_id
                                                     , fnd_global.login_id
                                                     , NULL
                                                     , l_delivery_id
                                                     , 'N'
                                                     , 'N'
                                                    );

                                commit;
                            END IF;

                        ELSE

                            l_log_sequence := l_log_sequence + 1;
                            write_log ( c1.header_id
                                        , c1.line_id
                                        , substr('Subinv Transfer Err. l_msg_data: '||replace(l_msg_data,chr(10), ' '), 1, 4000)
                                        , l_log_sequence);

                            begin
                                update  xxwc_wsh_shipping_stg
                                set     status = 'ERR_SYNC_SUBINV_TRANSF_2C'
                                where   header_id = c1.header_id
                                and     line_id = c1.line_id
                                and     delivery_id = p_delivery_id;
                            exception
                            when others then
                                null;
                            end;

                        END IF;

                    end if;

                end if;

            end if;
        end if;

        -- Code for scenarios 3 and 4
        -- 3. If line level warehouse doesn't = user xxwc_shipping profile delink from cust reservation and transfer quantity back to general
        -- 4. If line status = canceled then delink from cust reservation and transfer quantity back to general
        if (
            (c1.ol_ship_from_org_id != p_ship_from_org_id)
            OR
            (c1.ol_cancelled_flag = 'Y')
            )
        then
            l_log_sequence := l_log_sequence + 1;
            write_log ( c1.header_id
                        , c1.line_id
                        , substr('Scenario 3 or 4, cancelling and returning all quantities to General', 1, 4000)
                        , l_log_sequence);

            -- Remove the reservation data from the lot staging table to be able too transfer
            if c1.lot_control_code = 2 then
                l_log_sequence := l_log_sequence + 1;
                write_log ( c1.header_id
                            , c1.line_id
                            , substr('Removing reservation ID to be able to transfer to General...', 1, 4000)
                            , l_log_sequence);

                begin
                    update  XXWC.XXWC_WSH_SHIPPING_LOTS_STG
                    set     reservation_id = null
                    where   header_id = c1.header_id
                    and     line_id = c1.line_id
                    and     delivery_id = p_delivery_id;
                exception
                when others then
                    null;
                end;
            end if;

            subinventory_transfer (  c1.header_id
                                   , c1.line_id
                                   , p_delivery_id
                                   , 'GeneralPCK'
--                                 , 'General' -- Commented for Ver 3.5
                                   , c1.source_subinventory -- Added for Ver 3.5
                                   , l_log_sequence + 1
                                   , l_return_status
                                   , l_msg_data
                                   , l_log_sequence);

            IF l_return_status = fnd_api.g_ret_sts_success THEN

                l_log_sequence := l_log_sequence + 1;
                write_log ( c1.header_id
                            , c1.line_id
                            , substr('Subinv transfer successfully completed...', 1, 4000)
                            , l_log_sequence);

                begin
                    update  xxwc_wsh_shipping_stg
                    set     status = 'CANCELLED'
                            , ordered_quantity = 0
                            , transaction_qty = 0
                            , last_update_date = sysdate
                    where   header_id = c1.header_id
                    and     line_id = c1.line_id
                    and     delivery_id = p_delivery_id;
                exception
                when others then
                    null;
                end;

            ELSE

                l_log_sequence := l_log_sequence + 1;
                write_log ( c1.header_id
                            , c1.line_id
                            , substr('Subinv Transfer Err. l_msg_data: '||replace(l_msg_data,chr(10), ' '), 1, 4000)
                            , l_log_sequence);

            END IF;

        end if;

    end loop;

    -- Remove Reservations for Lot Items associated to the same order for the current delivery
    write_log (p_header_id
                , 0
                , substr('starting portion to remove lot reservations for items on order but different delivery', 1, 4000)
                , 99999997);

    write_log (p_header_id
                , 0
                , substr('p_delivery_id: '||p_delivery_id||'. p_header_id : '||p_header_id ||'. p_ship_from_org_id: '||p_ship_from_org_id, 1, 4000)
                , 99999998);

    for c2 in cur_non_rel_delivery_data
    loop
        exit when cur_non_rel_delivery_data%notfound;

        l_log_sequence := 0;

        if c2.lot_control_code = 2 then

            l_log_sequence := l_log_sequence + 1;
            write_log ( c2.header_id
                        , c2.line_id
                        , 'Starting reservation removal for. Line Num: '||c2.line_number||'. Item: '||c2.ordered_item||'. Line ID: '||c2.line_id||'. Delivery ID: '||c2.delivery_id
                        , l_log_sequence);

            for c3 in cur_remove_lot_reservation (c2.header_id, c2.line_id, c2.delivery_id)
            loop
                exit when cur_remove_lot_reservation%notfound;

                l_log_sequence := l_log_sequence + 1;
                write_log ( c2.header_id
                            , c2.line_id
                            , substr('Deleting reservation: '||c3.reservation_id||'. Lot: '||c3.lot_number||'. Subinv: '||c3.subinventory_code||'. Qty: '||c3.reservation_quantity, 1, 4000)
                            , l_log_sequence);

                l_rsv := g_miss_rsv_rec;

                l_rsv.demand_source_type_id        := inv_reservation_global.g_source_type_oe; -- order entry
                l_rsv.supply_source_type_id        := inv_reservation_global.g_source_type_inv;
                l_rsv.reservation_id               := c3.reservation_id;
                l_rsv.demand_source_header_id      := c3.demand_source_header_id;
                l_rsv.demand_source_line_id        := c3.demand_source_line_id;
                l_rsv.organization_id              := c2.cust_inventory_item_id;
                l_rsv.inventory_item_id            := c2.cust_ship_from_org_id;
                l_rsv.subinventory_code            := c3.subinventory_code;

                inv_reservation_pub.delete_reservation (
                                                           p_api_version_number        => 1.0
                                                         , p_init_msg_lst              => fnd_api.g_true
                                                         , x_return_status             => l_status
                                                         , x_msg_count                 => l_msg_count
                                                         , x_msg_data                  => l_msg_data
                                                         , p_rsv_rec                   => l_rsv
                                                         , p_serial_number             => l_dummy_sn
                                                       );

                l_log_sequence := l_log_sequence + 1;
                write_log ( c2.header_id
                            , c2.line_id
                            , substr('l_status: '||l_status||'. l_msg_count: '||l_msg_count , 1, 4000)
                            , l_log_sequence);

                IF l_status = fnd_api.g_ret_sts_success THEN
                    l_log_sequence := l_log_sequence + 1;
                    write_log ( c2.header_id
                                , c2.line_id
                                , substr('Reservation deleted: '||c3.reservation_id, 1, 4000)
                                , l_log_sequence);

                    -- Removing old reservation id
                    begin
                        update  XXWC.XXWC_WSH_SHIPPING_LOTS_STG
                        set     reservation_id = null
                                , status = 'SYNC_REM_RESERVE_SUCCESS'
                        -- where   rowid = c3.rid;
                        where   reservation_id = c3.reservation_id;
                    exception
                    when others then
                        null;
                    end;

                ELSE
                    IF l_msg_count = 1 THEN
                        l_log_sequence := l_log_sequence + 1;
                        write_log ( c2.header_id
                                    , c2.line_id
                                    , substr('Error Deleting reservation: '||c3.reservation_id||'. l_msg_data: '||l_msg_data, 1, 4000)
                                    , l_log_sequence);
                    ELSE
                        FOR l_index IN 1..l_msg_count LOOP
                            fnd_msg_pub.get(l_index, 'T', l_msg_data, l_dummy_num);
                            l_log_sequence := l_log_sequence + 1;
                            write_log ( c2.header_id
                                        , c2.line_id
                                        , substr('Error Deleting reservation: '||c3.reservation_id||'.l_msg_data: '||l_msg_data, 1, 4000)
                                        , l_log_sequence);
                        END LOOP;
                    END IF;
                END IF;

            END LOOP; -- End loop for lots

            -- Update Order Line
            l_log_sequence := l_log_sequence + 1;
            update_so_line ( c2.header_id
                              , c2.line_id
--                            , 'General' -- Commented for Ver 3.5
                              , c2.source_subinventory -- Added for Ver 3.5
                              , l_log_sequence
                              , l_return_status
                              , l_log_sequence );

        end if;

    end loop;

    write_log (p_header_id
                , 0
                , substr('Finishing sync_order_staging procedure', 1, 4000)
                , 99999998);
    commit;
    p_return_status := 'S';
    p_return_msg   := null;

exception
when others then
    l_msg_data := substr(SQLERRM, 1, 4000);
    write_log ( p_header_id
                , 99999999999999999
                , substr('Error in sync_order_staging: '||l_msg_data, 1, 4000)
                , 99999999);
    p_return_status := 'E';
    p_return_msg   := l_msg_data;
end sync_order_staging;




procedure rebuild_lot_reservation (  p_header_id         IN NUMBER
                                      , p_delivery_id       IN NUMBER
                                      , p_ship_from_org_id  IN NUMBER
                                      , p_return_status      OUT VARCHAR2
                                      , p_return_msg        OUT VARCHAR2)
is

    cursor cur_non_rel_delivery_data is
            select  x1.delivery_id
                    , x1.header_id
                    , x1.line_id
                    , x1.inventory_item_id cust_inventory_item_id
                    , x1.ship_from_org_id cust_ship_from_org_id
                    , x1.ordered_quantity cust_ord_qty
                    , x1.force_ship_qty cust_force_ship_qty
                    , x1.transaction_qty cust_trans_qty
                    , x1.status
                    , oh.order_number
                    , ol.line_number
                    , ol.flow_status_code
                    , nvl(ol.cancelled_flag, 'N') ol_cancelled_flag
                    , ol.ordered_item
                    , ol.inventory_item_id
                    , ol.ship_from_org_id ol_ship_from_org_id
                    , ol.ordered_quantity ol_ordered_quantity
                    , ol.shipped_quantity ol_shipped_quantity
                    , ol.cancelled_quantity ol_cancelled_quantity
--                  , to_number(ol.attribute11) ol_force_ship_qty -- Commented for Ver 3.5
-- Added for Ver 3.5 Begin
                    , DECODE (NVL (x1.subinventory, ol.subinventory),
                        'PUBD', ol.ordered_quantity,
                        TO_NUMBER (ol.attribute11))
                   ol_force_ship_qty
-- Added for Ver 3.5 End
                    , NVL(ol.subinventory,'General') ol_subinventory
                    , msib.lot_control_code -- 1: no control, 2: full control
                    , msib.primary_uom_code
            from    xxwc_wsh_shipping_stg x1
                    , oe_order_lines ol
                    , oe_order_headers oh
                    , mtl_system_items_b msib
            where   x1.header_id = p_header_id
            and     x1.delivery_id != p_delivery_id
            and     x1.status in ('OUT_FOR_DELIVERY','OUT_FOR_DELIVERY/PARTIAL_BACKORDER')
            and     x1.line_id = ol.line_id
            and     x1.header_id = ol.header_id
            and     ol.header_id = oh.header_id
            and     ol.inventory_item_id = msib.inventory_item_id
            and     ol.ship_from_org_id = msib.organization_id
            and     exists (select  'Has Lots'
                            from    XXWC.XXWC_WSH_SHIPPING_LOTS_STG x2
                            where   x2.header_id = x1.header_id
                            and     x2.line_id = x1.line_id
                            and     x2.delivery_id = x1.delivery_id
                            and     x2.status = 'SYNC_REM_RESERVE_SUCCESS'
                            and     x2.reservation_id is null);



    l_return_status              VARCHAR2 (240);
    l_msg_data                   VARCHAR2 (4000);
    l_log_sequence          number;
    l_cur_status            varchar2(240);
begin

    write_log (p_header_id
                , 0
                , substr('Starting rebuild_lot_reservation processing for: '||p_header_id, 1, 4000)
                , 99999995);

    write_log (p_header_id
                , 0
                , substr('p_delivery_id: '||p_delivery_id||'. p_header_id : '||p_header_id ||'. p_ship_from_org_id: '||p_ship_from_org_id, 1, 4000)
                , 99999996);

    for c1 in cur_non_rel_delivery_data
    loop
        exit when cur_non_rel_delivery_data%notfound;

        l_log_sequence := 0;

        l_log_sequence := l_log_sequence + 1;
        write_log ( c1.header_id
                    , c1.line_id
                    , 'Starting reservation rebuild process. Line Num: '||c1.line_number||'. Item: '||c1.ordered_item||'. Line ID: '||c1.line_id||'. DeliveryID: '||c1.delivery_id
                    , l_log_sequence);

        -- Call function to recreate the reservation
        if c1.lot_control_code = 2 then
            -- Update Order Line
            l_log_sequence := l_log_sequence + 1;
            update_so_line ( c1.header_id
                              , c1.line_id
                              , 'GeneralPCK'
                              , l_log_sequence
                              , l_return_status
                              , l_log_sequence );

            l_log_sequence := l_log_sequence + 1;
            create_reservation (c1.header_id
                               , c1.line_id
                               , c1.delivery_id
                               , 'GeneralPCK'
                               , l_log_sequence
                               , l_log_sequence);
        end if;

    end loop;

    write_log (p_header_id
                , 0
                , substr('Finishing rebuild_lot_reservation procedure', 1, 4000)
                , 99999998);

    commit;

    p_return_status := 'S';
    p_return_msg   := null;


exception
when others then
    l_msg_data := substr(SQLERRM, 1, 4000);
    write_log ( p_header_id
                , 99999999999999999
                , substr('Error in rebuild_lot_reservation: '||l_msg_data, 1, 4000)
                , 99999999);
    p_return_status := 'E';
    p_return_msg   := l_msg_data;
end rebuild_lot_reservation;

  /********************************************************************************
  -- PROCEDURE: cancelled_orders_upd
  --
  -- PURPOSE: Procedure to update User Item Description of Cancelled Order Lines
  --
  -- HISTORY
  -- ===========================================================================
  -- VERSION DATE          AUTHOR(S)       DESCRIPTION
  -- ------- -----------   --------------- -------------------------------------
     3.2     04/17/2015    Gopi Damuluri   TMS 20150226-00083 DMS User item description and the perforamance fix for the program
  *******************************************************************************/
procedure cancelled_orders_upd (errbuf      OUT VARCHAR2,
                                retcode     OUT VARCHAR2,
                                p_header_id IN  NUMBER,
                                p_last_delta_date VARCHAR2)--11/13/2013 Ram Talluri and Rasikha Galimova TMS 20131009-00330
is
    /*cursor cur_cancelled_orders is
            select  distinct x1.header_id
                    , oh.order_number
                    , x1.delivery_id
                    , x1.ship_from_org_id
                    , x1.status
            from    xxwc_wsh_shipping_stg x1
                    , oe_order_lines ol
                    , oe_order_headers oh
            where   x1.header_id = nvl(p_header_id, x1.header_id)
            and     x1.status in ('OUT_FOR_DELIVERY','OUT_FOR_DELIVERY/PARTIAL_BACKORDER')
            and     x1.header_id = ol.header_id
            and     x1.line_id = ol.line_id
            and     ol.header_id = oh.header_id
            and     (nvl(ol.cancelled_flag, 'N') = 'Y' OR nvl(oh.cancelled_flag, 'N') = 'Y');*/--11/13/2013 Ram Talluri and Rasikha Galimova TMS 20131009-00330


    l_return_status VARCHAR2 (240);
    l_msg_data      VARCHAR2 (4000);
    l_log_sequence  NUMBER;
    v_last_delta_date   DATE;--added on 11/13/2013 Ram Talluri and Rasikha Galimova TMS
begin

    v_last_delta_date := TRUNC (TO_DATE (p_last_delta_date, 'YYYY/MM/DD HH24:MI:SS'));--added on 11/13/2013 Ram Talluri and Rasikha Galimova TMS 20131009-00330
    write_log ( p_header_id
                , 99999999999999999
                , substr('Starting cancelled_orders_upd...', 1, 4000)
                , 99999991);

    --FOR c1 IN cur_cancelled_orders--11/13/2013 Ram Talluri and Rasikha Galimova TMS
    FOR c1 IN (SELECT /*+ index(ol XXWC_OE_ORDER_LN_LUD1) */--Ram Talluri 7/30/2014 TMS 20140609-00203 index hint updated to reflect the latest index name changes.
                DISTINCT x1.header_id
               ,oh.order_number
               ,x1.delivery_id
               ,x1.ship_from_org_id
               ,x1.status
               ,ol.line_number
               ,x1.ROWID stg_rid
               ,ol.ROWID ol_rid
               ,oh.ROWID oh_rid
               ,apps.xxwc_om_force_ship_pkg.is_row_locked (oh.ROWID, 'OE_ORDER_HEADERS_ALL') header_lock
               ,apps.xxwc_om_force_ship_pkg.is_row_locked (ol.ROWID, 'OE_ORDER_LINES_ALL') line_lock
               ,apps.xxwc_om_force_ship_pkg.is_row_locked (x1.ROWID, 'XXWC_WSH_SHIPPING_STG') shipping_lock
                FROM xxwc_wsh_shipping_stg x1,
                oe_order_lines ol,
                oe_order_headers oh
                WHERE     x1.header_id = NVL (p_header_id, x1.header_id)
                AND x1.status IN
                ('OUT_FOR_DELIVERY', 'OUT_FOR_DELIVERY/PARTIAL_BACKORDER')
                AND x1.header_id = ol.header_id
                AND x1.line_id = ol.line_id
                AND ol.header_id = oh.header_id
                AND oh.header_id =NVL(p_header_id,oh.header_id)
                AND (   NVL (ol.cancelled_flag, 'N') = 'Y'
                OR NVL (oh.cancelled_flag, 'N') = 'Y')
                AND TRUNC (ol.last_update_date) BETWEEN TRUNC (NVL (v_last_delta_date, SYSDATE)) - 1.01
                                                             AND TRUNC (NVL (v_last_delta_date, SYSDATE) + 0.9)
--                AND apps.xxwc_om_force_ship_pkg.is_row_locked (x1.ROWID, 'XXWC_WSH_SHIPPING_STG') = 'N' -- Version 3.2
--                AND apps.xxwc_om_force_ship_pkg.is_row_locked (ol.ROWID, 'OE_ORDER_LINES_ALL')  = 'N' )--11/13/2013 Ram Talluri and Rasikha Galimova TMS 20131009-00330  -- Version 3.2
    ) -- Version 3.2
    loop
        --exit when cur_cancelled_orders%notfound;--11/13/2013 Ram Talluri and Rasikha Galimova TMS 20131009-00330

        l_log_sequence := 0;

        l_log_sequence := l_log_sequence + 1;
        write_log ( c1.header_id
                    , 0
                    , 'Calling cancelled_orders_upd for order '||c1.order_number||'. header_id: '||c1.header_id||'. delivery_id: '||c1.delivery_id
                    , l_log_sequence);
-- Version 3.2 > Start
--        IF
--                apps.xxwc_om_force_ship_pkg.is_row_locked (c1.stg_rid, 'XXWC_WSH_SHIPPING_STG') = 'N'
--            AND apps.xxwc_om_force_ship_pkg.is_row_locked (c1.ol_rid, 'OE_ORDER_LINES_ALL')  = 'N'
--            AND apps.xxwc_om_force_ship_pkg.is_row_locked (c1.oh_rid, 'OE_ORDER_HEADERS_ALL') = 'N'
        IF c1.header_lock = 'N' AND c1.line_lock = 'N' AND c1.shipping_lock = 'N' THEN
-- Version 3.2 < End
--        THEN--IF Satement Added on 11/13/2013 Ram Talluri and Rasikha Galimova TMS 20131009-00330
        fnd_file.put_line(fnd_file.log, 'Calling sync_order_staging API for: '||c1.delivery_id||'/'||c1.order_number||'/'||c1.line_number);---Added on 11/13/2013 Ram Talluri and Rasikha Galimova TMS 20131009-00330
            sync_order_staging (  c1.header_id
                              , c1.delivery_id
                              , c1.ship_from_org_id
                              , l_return_status
                              , l_msg_data);
            COMMIT;--Added on 11/13/2013 Ram Talluri and Rasikha Galimova TMS 20131009-00330
        END IF;


        IF l_return_status = fnd_api.g_ret_sts_success THEN

            l_log_sequence := l_log_sequence + 1;
            write_log ( c1.header_id
                        , 0
                        , substr('Sync_order_staging process completed successfully for order '||c1.order_number||'. header_id: '||c1.header_id||'. delivery_id: '||c1.delivery_id, 1, 4000)
                        , l_log_sequence);

        ELSE

            l_log_sequence := l_log_sequence + 1;
            write_log ( c1.header_id
                        , 0
                        , substr('Error in sync process for order '||c1.order_number||'. header_id: '||c1.header_id||'. delivery_id: '||c1.delivery_id, 1, 4000)
                        , l_log_sequence);

            l_log_sequence := l_log_sequence + 1;

                            fnd_file.put_line (
                            fnd_file.LOG
                           ,   'Could not update status for Delivery/Order/Line: '
                            || c1.delivery_id
                            || '/'
                            || c1.order_number
                            || '/'
                            || c1.line_number);--Added on 11/13/2013 Ram Talluri and Rasikha Galimova TMS 20131009-00330

            write_log ( c1.header_id
                        , 0
                        , substr('Error message: '||l_msg_data, 1, 4000)
                        , l_log_sequence);

        END IF;

    end loop;

    commit;

    write_log ( p_header_id
                , 99999999999999999
                , substr('Completing cancelled_orders_upd.', 1, 4000)
                , 99999999);
exception
when others then
    --l_msg_data := substr(SQLERRM, 1, 4000);-- on 11/13/2013 Ram Talluri and Rasikha Galimova TMS 20131009-00330
    l_msg_data :=
                   'cancelled_orders_upd'
                || 'Error_Stack...'
                || DBMS_UTILITY.format_error_stack ()
                || ' Error_Backtrace...'
                || DBMS_UTILITY.format_error_backtrace ();--Added on 11/13/2013 Ram Talluri and Rasikha Galimova TMS 20131009-00330
    write_log ( p_header_id
                , 99999999999999999
                , substr('Error in cancelled_orders_upd: '||l_msg_data, 1, 4000)
                , 99999999);
    errbuf := l_msg_data;
    retcode := '2';
end cancelled_orders_upd;

   PROCEDURE check_lot_fs_reservation (
      p_header_id   IN       NUMBER,
      p_organization_id IN   NUMBER,
      p_delivery_id   IN NUMBER,
      x_lot_count   OUT      NUMBER,
      x_check       OUT      VARCHAR2,
      x_message     OUT      VARCHAR2
   )
   IS
      CURSOR c_lots
      IS
         SELECT   oola.ROWID, oola.line_id, oola.line_number,
                  oola.shipment_number, oola.ordered_quantity,
                  oola.inventory_item_id, oola.attribute11,
                  oola.order_quantity_uom, oola.ship_from_org_id
             FROM oe_order_lines oola, XXWC_WSH_SHIPPING_STG stg
            WHERE oola.header_id = p_header_id
              AND oola.ship_from_org_id = nvl(p_organization_id,oola.ship_from_org_id)
              AND oola.flow_status_code = 'AWAITING_SHIPPING'
              AND oola.attribute11 IS NOT NULL
              AND oola.header_id=stg.header_id
              AND oola.line_id=stg.line_id
              AND stg.status='OUT_FOR_DELIVERY'
              AND stg.delivery_id=p_delivery_id
              AND EXISTS (
                     SELECT 1
                       FROM mtl_system_items_b msib
                      WHERE oola.inventory_item_id = msib.inventory_item_id
                        AND oola.ship_from_org_id = msib.organization_id
                        AND msib.lot_control_code = 2)
         ORDER BY oola.line_number, oola.shipment_number;

      l_count              NUMBER           DEFAULT 0;
      l_message            VARCHAR2 (10000) DEFAULT '';
      l_cursor_count       NUMBER;
      l_check              VARCHAR2 (1)     DEFAULT 'N';
      r_lots               c_lots%ROWTYPE;
      l_ordered_qty        NUMBER;
      l_force_ship_qty     NUMBER;
      l_reserved_qty       NUMBER;
      l_back_ordered_qty   NUMBER;
      l_primary_uom_code   VARCHAR2 (3);
   BEGIN
      IF c_lots%ISOPEN
      THEN
         CLOSE c_lots;
      END IF;

      OPEN c_lots;

      FETCH c_lots
       INTO r_lots;

      l_cursor_count := c_lots%ROWCOUNT;

      WHILE c_lots%FOUND
      LOOP
         BEGIN
            SELECT primary_uom_code
              INTO l_primary_uom_code
              FROM mtl_system_items_b
             WHERE inventory_item_id = r_lots.inventory_item_id
               AND organization_id = r_lots.ship_from_org_id;
         EXCEPTION
            WHEN OTHERS
            THEN
               l_primary_uom_code := r_lots.order_quantity_uom;
         END;

         BEGIN
            l_force_ship_qty :=
               NVL
                  (inv_convert.inv_um_convert
                                      (item_id            => r_lots.inventory_item_id,
                                       PRECISION          => 5,
                                       from_quantity      => r_lots.attribute11,
                                       from_unit          => r_lots.order_quantity_uom,
                                       to_unit            => l_primary_uom_code,
                                       from_name          => NULL,
                                       to_name            => NULL
                                      ),
                   0
                  );
         EXCEPTION
            WHEN OTHERS
            THEN
               l_force_ship_qty := 0;
         END;

         SELECT NVL (COUNT (mr.reservation_id), 0),
                NVL (SUM (mr.primary_reservation_quantity), 0)
           INTO l_count,
                l_reserved_qty
           FROM mtl_reservations mr
          WHERE EXISTS (
                   SELECT 1
                     FROM oe_order_lines oola
                    WHERE mr.demand_source_line_id = r_lots.line_id
                      AND mr.inventory_item_id = r_lots.inventory_item_id
                      AND mr.organization_id = r_lots.ship_from_org_id
                      AND oola.line_id = r_lots.line_id);

         l_back_ordered_qty := l_force_ship_qty - l_reserved_qty;

         IF l_back_ordered_qty > 0
         THEN
            l_check := 'Y';

            IF l_message IS NULL
            THEN
               l_message :=
                     r_lots.line_number
                  || '.'
                  || r_lots.shipment_number
                  || ' BO Qty : '
                  || l_back_ordered_qty
                  || ' ';
            ELSE
               l_message :=
                     l_message
                  || ', '
                  || r_lots.line_number
                  || '.'
                  || r_lots.shipment_number
                  || ' BO Qty : '
                  || l_back_ordered_qty
                  || ' ';
            END IF;
         END IF;

         FETCH c_lots
          INTO r_lots;
      END LOOP;

      x_lot_count := l_cursor_count;
      x_check := l_check;
      x_message := l_message;

      CLOSE c_lots;
   END check_lot_fs_reservation;



   PROCEDURE check_lot_reservation (
      p_header_id   IN       NUMBER,
      p_organization_id IN   NUMBER,
      p_delivery_id   IN NUMBER,
      x_lot_count   OUT      NUMBER,
      x_check       OUT      VARCHAR2,
      x_partial_check OUT     VARCHAR2,
      x_message     OUT      VARCHAR2
   )
   IS
      CURSOR c_lots
      IS
         SELECT   oola.ROWID, oola.line_id, oola.line_number,
                  oola.shipment_number, oola.ordered_quantity,
                  oola.inventory_item_id, oola.attribute11,
                  oola.order_quantity_uom, oola.ship_from_org_id
             FROM oe_order_lines oola, XXWC_WSH_SHIPPING_STG stg
            WHERE oola.header_id = p_header_id
              AND oola.ship_from_org_id = nvl(p_organization_id,oola.ship_from_org_id)
              AND oola.flow_status_code = 'AWAITING_SHIPPING'
              AND oola.attribute11 IS NULL
              AND oola.header_id=stg.header_id
              AND oola.line_id=stg.line_id
              AND stg.status='OUT_FOR_DELIVERY'
              AND stg.delivery_id=p_delivery_id
              AND EXISTS (
                     SELECT 1
                       FROM mtl_system_items_b msib
                      WHERE oola.inventory_item_id = msib.inventory_item_id
                        AND oola.ship_from_org_id = msib.organization_id
                        AND msib.lot_control_code = 2)
         ORDER BY oola.line_number, oola.shipment_number;

      l_count              NUMBER           DEFAULT 0;
      l_message            VARCHAR2 (10000) DEFAULT '';
      l_cursor_count       NUMBER;
      l_check              VARCHAR2 (1)     DEFAULT 'N';
      l_partial_check              VARCHAR2 (1)     DEFAULT 'N';
      r_lots               c_lots%ROWTYPE;
      l_ordered_qty        NUMBER;
      l_force_ship_qty     NUMBER;
      l_reserved_qty       NUMBER;
      l_back_ordered_qty   NUMBER;
      l_primary_uom_code   VARCHAR2 (3);
   BEGIN
      IF c_lots%ISOPEN
      THEN
         CLOSE c_lots;
      END IF;

      OPEN c_lots;

      FETCH c_lots
       INTO r_lots;

      l_cursor_count := c_lots%ROWCOUNT;

      WHILE c_lots%FOUND
      LOOP
         BEGIN
            SELECT primary_uom_code
              INTO l_primary_uom_code
              FROM mtl_system_items_b
             WHERE inventory_item_id = r_lots.inventory_item_id
               AND organization_id = r_lots.ship_from_org_id;
         EXCEPTION
            WHEN OTHERS
            THEN
               l_primary_uom_code := r_lots.order_quantity_uom;
         END;

         BEGIN
            l_force_ship_qty :=
               NVL
                  (inv_convert.inv_um_convert
                                      (item_id            => r_lots.inventory_item_id,
                                       PRECISION          => 5,
                                       from_quantity      => r_lots.ordered_quantity,
                                       from_unit          => r_lots.order_quantity_uom,
                                       to_unit            => l_primary_uom_code,
                                       from_name          => NULL,
                                       to_name            => NULL
                                      ),
                   0
                  );
         EXCEPTION
            WHEN OTHERS
            THEN
               l_force_ship_qty := 0;
         END;

         SELECT NVL (COUNT (mr.reservation_id), 0),
                NVL (SUM (mr.primary_reservation_quantity), 0)
           INTO l_count,
                l_reserved_qty
           FROM mtl_reservations mr
          WHERE EXISTS (
                   SELECT 1
                     FROM oe_order_lines oola
                    WHERE mr.demand_source_line_id = r_lots.line_id
                      AND mr.inventory_item_id = r_lots.inventory_item_id
                      AND mr.organization_id = r_lots.ship_from_org_id
                      AND oola.line_id = r_lots.line_id);

         l_back_ordered_qty := l_force_ship_qty - l_reserved_qty;

         IF l_back_ordered_qty > 0
         THEN
            l_partial_check := 'Y';

            IF l_count = 0 then
              l_check := 'Y';
            END IF;

            IF l_message IS NULL
            THEN
               l_message :=
                     r_lots.line_number
                  || '.'
                  || r_lots.shipment_number
                  || ' BO Qty : '
                  || l_back_ordered_qty
                  || ' ';
            ELSE
               l_message :=
                     l_message
                  || ', '
                  || r_lots.line_number
                  || '.'
                  || r_lots.shipment_number
                  || ' BO Qty : '
                  || l_back_ordered_qty
                  || ' ';
            END IF;
         END IF;

         FETCH c_lots
          INTO r_lots;
      END LOOP;

      x_lot_count := l_cursor_count;
      x_check := l_check;
      x_partial_check := l_partial_check;
      x_message := l_message;

      CLOSE c_lots;
   END check_lot_reservation;

/*************************************************************************
  Procedure name: xxwc_wsh_shipping_extn_pkg.clear_delivery

  PURPOSE:   This procedure clear out a delivery from the staging table

  REVISIONS:
  Ver        Date        Author                     Description
  ---------  ----------  ---------------         -------------------------
  1.0        04/17/2013  Consuelo Gonzalez      Initial Version -TMS 20130422-01089
 3.0        10/15/2014  Ram Talluri            TMS 20141015-00143 Fix for clear delivery action is failing if the delivery includes intangible items
 3.4        03/03/2016  Manjula Chellappan     TMS #20151023-00037 - Shipping Extension Modification
                                                    for Counter Order Removal and PUBD
 3.5        03/09/2016  Manjula C./Rakesh P.   TMS #20150622-00156 - Shipping Extension modification for PUBD
**************************************************************************/
procedure clear_delivery (p_header_id         IN NUMBER
                          , p_delivery_id         IN NUMBER
                          , p_ship_from_org_id  IN NUMBER
                          , p_return_status      OUT VARCHAR2
                          , p_return_msg        OUT VARCHAR2)
is
    cursor cur_deliver_data is
            select  x1.delivery_id
                    , x1.header_id
                    , x1.line_id
                    , x1.inventory_item_id cust_inventory_item_id
                    , x1.ship_from_org_id cust_ship_from_org_id
                    , x1.ordered_quantity cust_ord_qty
                    , x1.force_ship_qty cust_force_ship_qty
                    , x1.transaction_qty cust_trans_qty
                    , x1.status
                    , oh.order_number
                    , ol.line_number
                    , ol.flow_status_code
                    , nvl(ol.cancelled_flag, 'N') ol_cancelled_flag
                    , ol.ordered_item
                    , ol.inventory_item_id
                    , ol.ship_from_org_id ol_ship_from_org_id
                    , ol.ordered_quantity ol_ordered_quantity
                    , ol.shipped_quantity ol_shipped_quantity
                    , ol.cancelled_quantity ol_cancelled_quantity
--                  , to_number(ol.attribute11) ol_force_ship_qty -- Commented for Ver 3.5
-- Added for Ver 3.5 Begin
                    , DECODE (NVL (x1.subinventory, ol.subinventory),
                        'PUBD', ol.ordered_quantity,
                        TO_NUMBER (ol.attribute11))
                   ol_force_ship_qty
-- Added for Ver 3.5 End
                    , NVL(ol.subinventory,'General') ol_subinventory
                    , msib.lot_control_code -- 1: no control, 2: full control
                    , msib.primary_uom_code
                    , xxwc_mv_routines_pkg.get_onhand_qty (ol.inventory_item_id,
                                                           ol.ship_from_org_id,
                                                           NVL(ol.subinventory,'General'),
                                                           'QOH') Available_Onhand
                    , ( nvl( (select  sum(mr.reservation_quantity)
                            from    mtl_reservations mr
                                    , mtl_sales_orders mso
                            where   mr.demand_source_type_id in (inv_reservation_global.g_source_type_oe, inv_reservation_global.g_source_type_internal_ord)
                            and     mr.supply_source_type_id = inv_reservation_global.g_source_type_inv
                            and     mr.demand_source_line_id = ol.line_id
                            and     mr.subinventory_code = NVL(ol.subinventory,'General')
                            and     mr.demand_source_header_id = mso.sales_order_id
                            and     mso.segment1 = oh.order_number
                            and     mso.segment3 = 'ORDER ENTRY'),0) ) reservation_quantity
                    , msib.MTL_TRANSACTIONS_ENABLED_FLAG--3.0
                    , msib.STOCK_ENABLED_FLAG--3.0
          , NVL (x1.subinventory, 'General') source_subinventory -- Added for Ver 3.5
            from    xxwc_wsh_shipping_stg x1
                    , oe_order_lines ol
                    , oe_order_headers oh
                    , mtl_system_items_b msib
            where   x1.delivery_id = p_delivery_id
            and     x1.status in ('OUT_FOR_DELIVERY','OUT_FOR_DELIVERY/PARTIAL_BACKORDER')
            and     x1.line_id = ol.line_id
            and     x1.header_id = ol.header_id
            and     ol.header_id = oh.header_id
            and     ol.inventory_item_id = msib.inventory_item_id
            and     ol.ship_from_org_id = msib.organization_id;

    cursor cur_remove_lot_reservation (i_header_id number, i_line_id number, i_delivery_id number) is
            select  x1.rowid rid
                    , x1.header_id
                    , x1.line_id
                    , x1.delivery_id
                    , x1.inventory_item_id
                    , x1.ship_from_org_id
                    , x1.lot_number
                    , x1.lot_quantity
                    , mr.reservation_id
                    , mr.demand_source_header_id
                    , mr.demand_source_line_id
                    , mr.subinventory_code
                    , mr.reservation_quantity
                    , mr.demand_source_type_id
                    , mr.supply_source_type_id
            from    XXWC.XXWC_WSH_SHIPPING_LOTS_STG x1
                    , mtl_reservations mr
            where   x1.header_id = i_header_id
            and     x1.line_id = i_line_id
            and     x1.delivery_id = i_delivery_id
            and     x1.reservation_id = mr.reservation_id
            and     mr.subinventory_code = 'GeneralPCK';

    l_return_status              VARCHAR2 (240);
    l_msg_data                   VARCHAR2 (4000);
    l_log_sequence               NUMBER;
    l_cur_status                 VARCHAR2(240);
    l_dummy_num                  NUMBER;
    l_perf_subvinv_transf        VARCHAR2(1);
    l_delivery_id                NUMBER;

    l_rsv       inv_reservation_global.mtl_reservation_rec_type;
    l_msg_count NUMBER;
    l_rsv_id    NUMBER;
    l_dummy_sn  inv_reservation_global.serial_number_tbl_type;
    l_status    VARCHAR2(1);
    g_miss_rsv_rec                  inv_reservation_global.mtl_reservation_rec_type;
    g_miss_serial_number            inv_reservation_global.serial_number_tbl_type;
    g_miss_mtl_reservations_tbl     inv_reservation_global.mtl_reservation_tbl_type;

          -- local error handling variables --/*3.0 TMS 20141015-00143 */
   l_err_callfrom             VARCHAR2 (75)
                                 := 'APPS.XXWC_WSH_SHIPPING_EXTN_PKG.CLEAR_DELIVERY';
   l_err_callpoint            VARCHAR2 (75) := 'START';
   l_distro_list              VARCHAR2 (75)
                                 DEFAULT 'HDSOracleDevelopers@hdsupply.com';
   l_message                  VARCHAR2 (2000);

begin

    write_log (p_header_id
                , 0
                , substr('Starting clear_delivery processing for: '||p_header_id, 1, 4000)
                , 99999995);

    write_log (p_header_id
                , 0
                , substr('p_delivery_id: '||p_delivery_id||'. p_header_id : '||p_header_id ||'. p_ship_from_org_id: '||p_ship_from_org_id, 1, 4000)
                , 99999996);

    for c1 in cur_deliver_data
    loop
        exit when cur_deliver_data%notfound;
        l_log_sequence := 0;

        l_err_callpoint := 'STAGE1';

        l_log_sequence := l_log_sequence + 1;
        write_log ( c1.header_id
                    , c1.line_id
                    , 'Starting clearing process. Line Num: '||c1.line_number||'. Item: '||c1.ordered_item||'. Line ID: '||c1.line_id
                    , l_log_sequence);

        l_log_sequence := l_log_sequence + 1;
        write_log ( c1.header_id
                    , c1.line_id
                    , 'cust_ship_from_org_id: '||c1.cust_ship_from_org_id||'. ol_ship_from_org_id: '||c1.ol_ship_from_org_id
                    , l_log_sequence);

        l_log_sequence := l_log_sequence + 1;
        write_log ( c1.header_id
                    , c1.line_id
                    , 'cust_ord_qty: '||c1.cust_ord_qty||'. ol_ordered_quantity: '||c1.ol_ordered_quantity
                    , l_log_sequence);

        l_log_sequence := l_log_sequence + 1;
        write_log ( c1.header_id
                    , c1.line_id
                    , 'cust_force_ship_qty: '||c1.cust_force_ship_qty||'. ol_force_ship_qty: '||c1.ol_force_ship_qty
                    , l_log_sequence);

        l_log_sequence := l_log_sequence + 1;
        write_log ( c1.header_id
                    , c1.line_id
                    , 'cust_trans_qty: '||c1.cust_trans_qty
                    , l_log_sequence);

        -- Only transact if line is sourced from GeneralPCK
        if c1.ol_subinventory = 'GeneralPCK' then

            -- If item is lot controlled then remove reservations
            if c1.lot_control_code = 2 then

            l_err_callpoint := 'STAGE2';

                l_log_sequence := l_log_sequence + 1;
                write_log ( c1.header_id
                            , c1.line_id
                            , substr('Lot controlled item...deleting reservations: ', 1, 4000)
                            , l_log_sequence);

                for c3 in cur_remove_lot_reservation (c1.header_id, c1.line_id, p_delivery_id)
                loop
                    exit when cur_remove_lot_reservation%notfound;

                    l_err_callpoint := 'STAGE3';

                    l_log_sequence := l_log_sequence + 1;
                    write_log ( c1.header_id
                                , c1.line_id
                                , substr('Deleting reservation: '||c3.reservation_id||'. Lot: '||c3.lot_number||'. Subinv: '||c3.subinventory_code||'. Qty: '||c3.reservation_quantity, 1, 4000)
                                , l_log_sequence);

                    l_rsv := g_miss_rsv_rec;

                    l_rsv.demand_source_type_id        := c3.demand_source_type_id; -- inv_reservation_global.g_source_type_oe; -- order entry
                    l_rsv.supply_source_type_id        := c3.supply_source_type_id; -- inv_reservation_global.g_source_type_inv;
                    l_rsv.reservation_id               := c3.reservation_id;
                    l_rsv.demand_source_header_id      := c3.demand_source_header_id;
                    l_rsv.demand_source_line_id        := c3.demand_source_line_id;
                    l_rsv.organization_id              := c1.cust_inventory_item_id;
                    l_rsv.inventory_item_id            := c1.cust_ship_from_org_id;
                    l_rsv.subinventory_code            := c3.subinventory_code;

                    inv_reservation_pub.delete_reservation (
                                                               p_api_version_number        => 1.0
                                                             , p_init_msg_lst              => fnd_api.g_true
                                                             , x_return_status             => l_status
                                                             , x_msg_count                 => l_msg_count
                                                             , x_msg_data                  => l_msg_data
                                                             , p_rsv_rec                   => l_rsv
                                                             , p_serial_number             => l_dummy_sn
                                                           );

                    l_log_sequence := l_log_sequence + 1;
                    write_log ( c1.header_id
                                , c1.line_id
                                , substr('l_status: '||l_status||'. l_msg_count: '||l_msg_count , 1, 4000)
                                , l_log_sequence);

                    IF l_status = fnd_api.g_ret_sts_success THEN
                        l_log_sequence := l_log_sequence + 1;
                        write_log ( c1.header_id
                                    , c1.line_id
                                    , substr('Reservation deleted: '||c3.reservation_id, 1, 4000)
                                    , l_log_sequence);

                        -- Removing old reservation id
                        begin
                            update  XXWC.XXWC_WSH_SHIPPING_LOTS_STG
                            set     reservation_id = null
                            where   reservation_id = c3.reservation_id;
                        exception
                        when others then
                            null;
                        end;

                    ELSE
                    l_err_callpoint := 'STAGE4';
                        IF l_msg_count = 1 THEN
                            l_log_sequence := l_log_sequence + 1;
                            write_log ( c1.header_id
                                        , c1.line_id
                                        , substr('Error Deleting reservation: '||c3.reservation_id||'. l_msg_data: '||l_msg_data, 1, 4000)
                                        , l_log_sequence);
                        ELSE
                            FOR l_index IN 1..l_msg_count LOOP
                                fnd_msg_pub.get(l_index, 'T', l_msg_data, l_dummy_num);
                                l_log_sequence := l_log_sequence + 1;
                                write_log ( c1.header_id
                                            , c1.line_id
                                            , substr('Error Deleting reservation: '||c3.reservation_id||'.l_msg_data: '||l_msg_data, 1, 4000)
                                            , l_log_sequence);
                            END LOOP;
                        END IF;
                    END IF;

                END LOOP; -- End loop for lots

            end if;

         IF c1.MTL_TRANSACTIONS_ENABLED_FLAG='Y' AND c1.STOCK_ENABLED_FLAG='Y' THEN --3.0 intangible items not eligible for subinv transfer
         l_err_callpoint := 'STAGE5';
            -- perform subinventory transfer
            subinv_transfer_by_qty (  c1.header_id
                                   , c1.line_id
                                   , p_delivery_id
                                   , 'GeneralPCK'
--                                 , 'General' -- Commented for Ver 3.5
                                   , c1.source_subinventory -- Added for Ver 3.5
                                   , c1.cust_trans_qty
                                   , l_log_sequence + 1
                                   , l_return_status
                                   , l_msg_data
                                   , l_log_sequence);

            IF l_return_status = fnd_api.g_ret_sts_success THEN

                l_log_sequence := l_log_sequence + 1;
                write_log ( c1.header_id
                            , c1.line_id
                            , substr('Subinv transfer successfully completed...Updating transaction quantity to 0', 1, 4000)
                            , l_log_sequence);

                -- Update SO Line Subinventory to General
                l_log_sequence := l_log_sequence + 1;
                update_so_line ( c1.header_id
                                  , c1.line_id
--                                , 'General' -- Commented for Ver 3.5
                  , c1.source_subinventory  -- Added for Ver 3.5
                                  , l_log_sequence
                                  , l_return_status
                                  , l_log_sequence );

                l_err_callpoint := 'STAGE6';
                -- Remove Records from Lot Staging table and general staging table
                begin
                    delete from  XXWC.XXWC_WSH_SHIPPING_LOTS_STG
                    where   header_id = c1.header_id
                    and     line_id = c1.line_id
                    and     delivery_id = p_delivery_id;
                exception
                when others then
                    null;
                    l_err_callpoint := 'STAGE7';
                end;

                begin
                    delete from  xxwc_wsh_shipping_stg
                    where   header_id = c1.header_id
                    and     line_id = c1.line_id
                    and     delivery_id = p_delivery_id;
                exception
                when others then
                    null;
                    l_err_callpoint := 'STAGE8';
                end;

--Added for Rev 3.4 Begin
    BEGIN

           UPDATE XXWC.xxwc_oe_open_order_lines
            SET ordered_quantity =
             (SELECT(ol.ordered_quantity
                             * po_uom_s.po_uom_convert (um.unit_of_measure
                                                , msib.primary_unit_of_measure
                                                , ol.inventory_item_id
                                               )
                             )
              FROM oe_order_lines_all ol
              , mtl_system_items_b msib
              , mtl_units_of_measure_vl um
             WHERE ol.line_id            = c1.line_id
               and ol.inventory_item_id  = msib.inventory_item_id
                         and ol.ship_from_org_id   = msib.organization_id
                         and ol.order_quantity_uom = um.uom_code
                  ),
              last_update_Date = sysdate,
                 last_updated_by = fnd_global.user_id,
            last_update_login = fnd_global.login_id
          WHERE line_id = c1.line_id ;

       IF SQL%ROWCOUNT = 0 THEN
          BEGIN
        INSERT
        INTO XXWC.xxwc_oe_open_order_lines
          (
          line_id ,
          header_id ,
          line_type_id ,
          inventory_item_id ,
          organization_id ,
          ordered_quantity ,
          org_id ,
          created_by ,
          last_updated_by ,
          creation_date ,
          last_update_date ,
          last_update_login
          )
        SELECT ol.line_id ,
          ol.header_id ,
          ol.line_type_id ,
          ol.inventory_item_id ,
          ol.ship_from_org_id ,
          (ol.ordered_quantity
                     * po_uom_s.po_uom_convert (um.unit_of_measure
                                                , msib.primary_unit_of_measure
                                                , ol.inventory_item_id
                                                )
                   ) ,
          ol.org_id ,
          fnd_global.user_id ,
          fnd_global.user_id ,
          sysdate ,
          sysdate ,
          fnd_global.login_id
        FROM oe_order_lines ol
            , mtl_system_items_b msib
          , mtl_units_of_measure_vl um
         WHERE ol.line_type_id IN (fnd_profile.value('XXWC_STANDARD_LINE_TYPE'), fnd_profile.value('XXWC_INTERNAL_ORDER_LINE_TYPE'))
         AND ol.line_id            = c1.line_id
                 AND ol.inventory_item_id  = msib.inventory_item_id
                 AND ol.ship_from_org_id   = msib.organization_id
             AND ol.order_quantity_uom = um.uom_code
         AND subinventory         != 'PUBD'; --Added for ver 3.5
       EXCEPTION WHEN OTHERS THEN
                NULL;
                l_err_callpoint := 'STAGE8.1';
       END;
      END IF;

    EXCEPTION WHEN OTHERS THEN
        NULL;
      l_err_callpoint := 'STAGE8.2';
    END;
--Added for Rev 3.4 End

            ELSE
                l_log_sequence := l_log_sequence + 1;
                write_log ( c1.header_id
                            , c1.line_id
                            , substr('Subinv Transfer Err. l_msg_data: '||replace(l_msg_data,chr(10), ' '), 1, 4000)
                            , l_log_sequence);

                begin
                    update  xxwc_wsh_shipping_stg
                    set     status = 'ERR_CLEAR_SUBINV_TRANSF'
                    where   header_id = c1.header_id
                    and     line_id = c1.line_id
                    and     delivery_id = p_delivery_id;
                exception
                when others then
                    null;
                    l_err_callpoint := 'STAGE9';
                end;
            END IF;

        ELSE --3.0--this section will handle intangible items.

           l_log_sequence := l_log_sequence + 1;
                write_log ( c1.header_id
                            , c1.line_id
                            , substr('Item is non transactable, subinv transfer not applicable...', 1, 4000)
                            , l_log_sequence);

                -- Update SO Line Subinventory to General
                l_log_sequence := l_log_sequence + 1;
                update_so_line ( c1.header_id
                                  , c1.line_id
--                                , 'General' -- Commented for Ver 3.5
                                  , c1.source_subinventory -- Added for Ver 3.5
                                  , l_log_sequence
                                  , l_return_status
                                  , l_log_sequence );

                l_err_callpoint := 'STAGE10';


                -- Remove Records from Lot Staging table and general staging table
                begin
                    delete from  XXWC.XXWC_WSH_SHIPPING_LOTS_STG
                    where   header_id = c1.header_id
                    and     line_id = c1.line_id
                    and     delivery_id = p_delivery_id;
                exception
                when others then
                    null;
                    l_err_callpoint := 'STAGE11';
                end;

                begin
                    delete from  xxwc_wsh_shipping_stg
                    where   header_id = c1.header_id
                    and     line_id = c1.line_id
                    and     delivery_id = p_delivery_id;
                exception
                when others then
                    null;
                    l_err_callpoint := 'STAGE12';
                end;


         END IF;--3.0

      end if;


    end loop;

    l_err_callpoint := 'STAGE13';

    write_log (p_header_id
                , 0
                , substr('Finishing clear_delivery procedure', 1, 4000)
                , 99999998);
    commit;
    p_return_status := 'S';
    p_return_msg   := null;

exception
when others then
    l_msg_data := substr(SQLERRM, 1, 4000);
    write_log ( p_header_id
                , 99999999999999999
                , substr('Error in clear_delivery: '||l_msg_data, 1, 4000)
                , 99999999);
    p_return_status := 'E';
    p_return_msg   := l_msg_data;

    xxcus_error_pkg.xxcus_error_main_api (
         p_called_from         => l_err_callfrom,
         p_calling             => l_err_callpoint,
         p_ora_error_msg       => SQLERRM,
         p_error_desc          => l_message,
         p_distribution_list   => l_distro_list,
         p_module              => 'XXWC');

end clear_delivery;

-- 05/06/2013 CG: TMS 20130509-01444: Added new procedure to sync delivery statuses at the end of the day
--11/13/2013 Ram Talluri and Rasikha Galimova TMS

  /********************************************************************************
  -- PROCEDURE: sync_del_line_status
  --
  -- PURPOSE: Procedure to Sync Delivered Order Line Status
  --
  -- HISTORY
  -- ===========================================================================
  -- VERSION DATE          AUTHOR(S)       DESCRIPTION
  -- ------- -----------   --------------- -------------------------------------
     3.2     04/17/2015    Gopi Damuluri   TMS 20150226-00083 DMS User item description and the perforamance fix for the program
  *******************************************************************************/
procedure sync_del_line_status (errbuf      OUT VARCHAR2,
                                retcode     OUT VARCHAR2,
                                p_header_id IN NUMBER,
                                p_last_delta_date VARCHAR2)--11/13/2013 Ram Talluri and Rasikha Galimova TMS 20131009-00330
is

    /*cursor cur_delivered_lines is
            select  x1.delivery_id
                    , x1.rowid stg_rid
                    , ol.header_id
                    , ol.line_id
                    , ottt.name order_type
                    , oh.order_number
                    , (ol.line_number||'.'||ol.shipment_number) line_number
                    , mp.organization_code
                    , ol.ordered_item
                    , ol.ordered_quantity
                    , to_number(ol.attribute11) force_ship_qty
                    , ol.subinventory
                    , ol.flow_status_code
                    , x1.status
                    , x1.transaction_qty
                    , nvl(x1.force_ship_qty,0) stg_force_ship_qty
            from xxwc_wsh_shipping_stg x1
                    , oe_order_lines ol
                    , oe_order_headers oh
                    , oe_transaction_types ottt
                    , mtl_parameters mp
            where   x1.line_id = ol.line_id
            and     x1.header_id = ol.header_id
            and     x1.header_id = oh.header_id
            and     oh.order_type_id = ottt.transaction_type_id
            and     ol.ship_from_org_id = mp.organization_id
            and     ol.flow_status_code in ('CLOSED','PRE-BILLING_ACCEPTANCE','INVOICE_HOLD','CANCELLED')
            and     x1.status not in ('DELIVERED','CANCELLED')
            order by 3,4;*/--commented on 11/13/2013 Ram Talluri and Rasikha Galimova TMS 20131009-00330

    l_count number;
    l_msg_data      VARCHAR2 (4000);
    v_last_delta_date   DATE;--added on 11/13/2013 Ram Talluri and Rasikha Galimova TMS 20131009-00330

begin

    v_last_delta_date := TRUNC (TO_DATE (p_last_delta_date, 'YYYY/MM/DD HH24:MI:SS'));--added on 11/13/2013 Ram Talluri and Rasikha Galimova TMS 20131009-00330
    fnd_file.put_line(fnd_file.log, 'Starting Delivery/Lines Status Sync');
    fnd_file.put_line(fnd_file.log, '====================================');
    fnd_file.put_line(fnd_file.log, 'Lines being updated to delivered/cancelled in staging:');
    l_count := 0;
    --for c1 in cur_delivered_lines--commented on 11/13/2013 Ram Talluri and Rasikha Galimova TMS 20131009-00330
    FOR c1 IN (  SELECT /*+ index(ol XXWC_OE_ORDER_LN_LUD1) */--Ram Talluri 7/30/2014 TMS 20140609-00203 index hint updated to reflect the latest index name changes.
                        x1.delivery_id
                        ,x1.ROWID stg_rid
                        ,ol.header_id
                        ,ol.line_id
                        ,ottt.name order_type
                        ,oh.order_number
                        , (ol.line_number || '.' || ol.shipment_number) line_number
                        --,mp.organization_code
                        ,ol.ordered_item
                        ,ol.ordered_quantity
                        ,TO_NUMBER (ol.attribute11) force_ship_qty
                        ,ol.subinventory
                        ,ol.flow_status_code
                        ,x1.status
                        ,x1.transaction_qty
                        ,NVL (x1.force_ship_qty, 0) stg_force_ship_qty
--                        ,apps.xxwc_om_force_ship_pkg.is_row_locked (oh.ROWID, 'OE_ORDER_HEADERS_ALL') header_lock -- Version 3.2
--                        ,apps.xxwc_om_force_ship_pkg.is_row_locked (ol.ROWID, 'OE_ORDER_LINES_ALL') line_lock -- Version 3.2
                        ,apps.xxwc_om_force_ship_pkg.is_row_locked (x1.ROWID, 'XXWC_WSH_SHIPPING_STG') shipping_lock
                    FROM xxwc_wsh_shipping_stg x1
                        ,oe_order_lines ol
                        ,oe_order_headers oh
                        ,oe_transaction_types ottt
                   --   ,mtl_parameters mp
                   WHERE     x1.line_id = ol.line_id
                         AND x1.header_id = ol.header_id
                         AND x1.header_id = oh.header_id
                         AND oh.header_id =NVL(p_header_id,oh.header_id)
                         AND oh.order_type_id = ottt.transaction_type_id
                         --    AND ol.ship_from_org_id = mp.organization_id
                         AND TRUNC (ol.last_update_date) BETWEEN TRUNC (NVL (v_last_delta_date, SYSDATE)) - 1.01
                                                             AND TRUNC (NVL (v_last_delta_date, SYSDATE) + 0.9)
                         AND ol.flow_status_code IN ('CLOSED', 'PRE-BILLING_ACCEPTANCE', 'INVOICE_HOLD', 'CANCELLED')
                         AND x1.status NOT IN ('DELIVERED', 'CANCELLED')
--                         AND apps.xxwc_om_force_ship_pkg.is_row_locked (x1.ROWID, 'XXWC_WSH_SHIPPING_STG') = 'N'  -- Version 3.2
--                         and apps.xxwc_om_force_ship_pkg.is_row_locked (ol.ROWID, 'OE_ORDER_LINES_ALL')  = 'N'    -- Version 3.2
                ORDER BY 3, 4)--Added on 11/13/2013 Ram Talluri and Rasikha Galimova TMS 20131009-00330
    loop
        --exit when cur_delivered_lines%notfound;--11/13/2013 Ram Talluri and Rasikha Galimova TMS 20131009-00330

        if c1.flow_status_code in ('CLOSED','PRE-BILLING_ACCEPTANCE','INVOICE_HOLD')
            and c1.status != 'DELIVERED'
        then

            --fnd_file.put_line(fnd_file.log, 'Updating to Delivered. Delivery/Order/Line: '||c1.delivery_id||'/'||c1.order_number||'/'||c1.line_number);

            begin
                -- IF apps.xxwc_om_force_ship_pkg.is_row_locked (c1.stg_rid, 'XXWC_WSH_SHIPPING_STG') = 'N' THEN--IF Satement Added on 11/13/2013 Ram Talluri and Rasikha Galimova TMS 20131009-00330 -- Version 3.2
                IF c1.shipping_lock = 'N' THEN -- Version 3.2
                fnd_file.put_line(fnd_file.log, 'Updating to Delivered. Delivery/Order/Line: '||c1.delivery_id||'/'||c1.order_number||'/'||c1.line_number);---Added on 11/13/2013 Ram Talluri and Rasikha Galimova TMS 20131009-00330
                update  xxwc_wsh_shipping_stg
                set     status = 'DELIVERED'
                where   rowid = c1.stg_rid;
                END IF;--Added on 11/13/2013 Ram Talluri and Rasikha Galimova TMS 20131009-00330
                l_count := l_count + 1;
            exception
            when others then
                l_msg_data :=
                               'sync_del_line_status'
                            || 'Error_Stack...'
                            || DBMS_UTILITY.format_error_stack ()
                            || ' Error_Backtrace...'
                            || DBMS_UTILITY.format_error_backtrace ();
                        fnd_file.put_line (
                            fnd_file.LOG
                           ,   'Could not update status for Delivery/Order/Line: '
                            || c1.delivery_id
                            || '/'
                            || c1.order_number
                            || '/'
                            || c1.line_number);--Added on 11/13/2013 Ram Talluri and Rasikha Galimova TMS 20131009-00330
                --fnd_file.put_line(fnd_file.log, 'Could not update status for Delivery/Order/Line: '||c1.delivery_id||'/'||c1.order_number||'/'||c1.line_number);
                fnd_file.put_line(fnd_file.log, 'Error: '||SQLERRM);
                fnd_file.put_line(fnd_file.log, ' ');
            end;

        elsif c1.flow_status_code = 'CANCELLED' and c1.status != 'CANCELLED' and c1.transaction_qty = 0 and c1.stg_force_ship_qty = 0
        then

            --fnd_file.put_line(fnd_file.log, 'Updating to Cancelled. Delivery/Order/Line: '||c1.delivery_id||'/'||c1.order_number||'/'||c1.line_number);

            begin
                -- IF apps.xxwc_om_force_ship_pkg.is_row_locked (c1.stg_rid, 'XXWC_WSH_SHIPPING_STG') = 'N' THEN--IF statement Added on 11/13/2013 Ram Talluri and Rasikha Galimova TMS 20131009-00330 -- Version 3.2
                IF c1.shipping_lock = 'N' THEN -- Version 3.2
                fnd_file.put_line(fnd_file.log, 'Updating to Cancelled. Delivery/Order/Line: '||c1.delivery_id||'/'||c1.order_number||'/'||c1.line_number);--Added on 11/13/2013 Ram Talluri and Rasikha Galimova TMS 20131009-00330
                update  xxwc_wsh_shipping_stg
                set     status = 'CANCELLED'
                where   rowid = c1.stg_rid;
                END IF;

                l_count := l_count + 1;
            exception
            when others then
                l_msg_data :=
                               'sync_del_line_status'
                            || 'Error_Stack...'
                            || DBMS_UTILITY.format_error_stack ()
                            || ' Error_Backtrace...'
                            || DBMS_UTILITY.format_error_backtrace ();
                        fnd_file.put_line (
                            fnd_file.LOG
                           ,   'Could not update status for Delivery/Order/Line: '
                            || c1.delivery_id
                            || '/'
                            || c1.order_number
                            || '/'
                            || c1.line_number);--Added on 11/13/2013 Ram Talluri and Rasikha Galimova TMS 20131009-00330
                --fnd_file.put_line(fnd_file.log, 'Could not update status for Delivery/Order/Line: '||c1.delivery_id||'/'||c1.order_number||'/'||c1.line_number);
                fnd_file.put_line(fnd_file.log, 'Error: '||SQLERRM);
                fnd_file.put_line(fnd_file.log, ' ');
            end;

        else
            fnd_file.put_line(fnd_file.log, 'Invalid combination of statuses and quantities, not updating line. Delivery/Order/Line: '||c1.delivery_id||'/'||c1.order_number||'/'||c1.line_number);
        end if;

    end loop;

    --fnd_file.put_line(fnd_file.log,'Updated '||l_count||' lines in the staging table to DELIVERED');

    commit;

exception
when others then
    --l_msg_data := substr(SQLERRM, 1, 4000);
    l_msg_data :=
                   'sync_del_line_status'
                || 'Error_Stack...'
                || DBMS_UTILITY.format_error_stack ()
                || ' Error_Backtrace...'
                || DBMS_UTILITY.format_error_backtrace ();
            DBMS_OUTPUT.put_line (l_msg_data);--Added on 11/13/2013 Ram Talluri and Rasikha Galimova TMS 20131009-00330
    fnd_file.put_line(fnd_file.log, 'Error in sync_del_line_status: '||l_msg_data);
    errbuf := l_msg_data;
    retcode := '2';
end sync_del_line_status;


--Added for Ver 3.4 Begin

   PROCEDURE Counter_subinv_xfer (p_header_id       IN     NUMBER,
                                  p_return_status      OUT VARCHAR2,
                                  p_error_msg          OUT VARCHAR2)
   IS
      /**************************************************************************
      $Header xxwc_wsh_shipping_extn_pkg $
      Module Name: Counter_subinv_xfer
      PURPOSE:   This Procedure is used to do subinventory transfer From OM Printer Form
      REVISIONS:
      Ver        Date        Author                     Description
      ---------  ----------  ---------------         -------------------------
      3.4        12/17/2015  Manjula Chellappan    TMS #20151023-00037 - Shipping Extension Modification
                                                       for Counter Order Removal and PUBD
      3.4.2      04/14/2016  Rakesh Patel          TMS 20160414-00047- Bug fix related to counter order workflow changes
      3.6        07/06/2016  Rakesh Patel          TMS 20160624-00205-Subinventory code is either not entered or not valid for the given organization
      **************************************************************************/
      CURSOR cur_transfer
      IS
         SELECT ol.ROWID rid,
                ol.header_id,
                ol.line_id,
                ol.line_type_id,
                ol.inventory_item_id,
                ol.ship_from_org_id,
                ol.ordered_quantity,
                ol.subinventory,
                msib.lot_control_code,       -- 1: no control, 2: full control
                msib.primary_uom_code,
                oh.order_number,
                (ol.line_number || '.' || ol.shipment_number) line_num,
                msib.stock_enabled_flag,
                msib.mtl_transactions_enabled_flag
           FROM mtl_system_items_b msib,
                oe_order_lines ol,
                oe_order_headers oh
          WHERE     oh.header_id = p_header_id
                AND ol.inventory_item_id = msib.inventory_item_id
                AND ol.ship_from_org_id = msib.organization_id
                AND ol.header_id = oh.header_id
                AND NVL (ol.ordered_quantity, 0) > 0
    AND ol.flow_status_code = 'BOOKED' --Added by rakesh patel for TMS 20160414-00047 on 14-Apr-2016
    AND oh.order_type_id = 1004 --Added by rakesh patel for TMS 20160624-00205 on 05-Jul-2016
    AND NOT EXISTS
        ( SELECT 'x'
          FROM xxwc.xxwc_oe_counter_lines_tbl
         WHERE line_id = ol.line_id
           AND ship_from_org_id = ol.ship_from_org_id
      );

      l_line_id                    NUMBER;
      l_header_id                  NUMBER;
      l_transaction_type           VARCHAR2 (40) := 'Subinventory Transfer';
      l_transaction_type_id        NUMBER;
      l_transaction_header_id      NUMBER;
      l_interface_transaction_id   NUMBER;
      l_to_subinventory            VARCHAR2 (15) := 'GeneralPCK';
      l_sec                        VARCHAR2 (100);
      l_processing_return          NUMBER;
      l_return_status              VARCHAR2 (240);
      l_msg_count                  NUMBER;
      l_msg_data                   VARCHAR2 (4000);
      l_trans_count                NUMBER;
      l_log_sequence               NUMBER;
      l_error_msg                  VARCHAR2 (4000);
   BEGIN
      p_return_status := 'S';

      l_sec := 'Get Transaction_type_id';

      BEGIN
         SELECT transaction_type_id
           INTO l_transaction_type_id
           FROM mtl_transaction_types
          WHERE transaction_type_name = l_transaction_type;
      EXCEPTION
         WHEN OTHERS
         THEN
            l_transaction_type_id := NULL;
      END;

      IF l_transaction_type_id IS NOT NULL
      THEN
         FOR rec_transfer IN cur_transfer
         LOOP
            l_sec := 'Get l_transaction_header_id';

            SELECT mtl_material_transactions_s.NEXTVAL
              INTO l_transaction_header_id
              FROM DUAL;

            l_sec := 'Get l_interface_transaction_id';

            SELECT mtl_material_transactions_s.NEXTVAL
              INTO l_interface_transaction_id
              FROM DUAL;

            l_sec := 'Insert into mtl_transactions_interface';

            IF     rec_transfer.stock_enabled_flag = 'Y'
               AND rec_transfer.mtl_transactions_enabled_flag = 'Y'
            THEN
               BEGIN

                  INSERT
                    INTO mtl_transactions_interface (
                            transaction_interface_id,
                            transaction_header_id,
                            source_code,
                            source_line_id,
                            source_header_id,
                            process_flag,
                            transaction_mode,
                            lock_flag,
                            last_update_date,
                            last_updated_by,
                            creation_date,
                            created_by,
                            transaction_type_id,
                            transaction_date,
                            transaction_uom,
                            inventory_item_id,
                            subinventory_code,
                            organization_id,
                            transfer_subinventory,
                            transfer_organization,
                            transaction_quantity,
                            primary_quantity,
                            transaction_reference)
                     VALUES (
                               l_interface_transaction_id,
                               l_transaction_header_id,
                               l_transaction_type,               --source code
                               l_interface_transaction_id,    --source line id
                               l_interface_transaction_id,  --source header id
                               1,                               --process flag
                               3,                           --transaction mode
                               2,                                  --lock flag
                               SYSDATE,                     --last update date
                               0,                            --last updated by
                               SYSDATE,                         --created date
                               0,                                 --created by
                               l_transaction_type_id,      -- transaction type
                               SYSDATE,                    -- transaction date
                               rec_transfer.primary_uom_code,           -- uom
                               rec_transfer.inventory_item_id, -- inventory item id
                               rec_transfer.subinventory, -- from subinventory
                               rec_transfer.ship_from_org_id,
                               l_to_subinventory,           -- to subinventory
                               rec_transfer.ship_from_org_id,
                               rec_transfer.ordered_quantity,
                               rec_transfer.ordered_quantity,
                               (   rec_transfer.order_number
                                || '-'
                                || rec_transfer.line_num));

                  COMMIT;
               EXCEPTION
                  WHEN OTHERS
                  THEN
                     l_error_msg :=
                           l_error_msg
                        || '-'
                        || rec_transfer.line_num
                        || '-'
                        || SUBSTR (SQLERRM, 100);
                     p_return_status := 'E';
                     p_error_msg := l_error_msg;
               END;

               l_sec := 'Call API for subnventory Transfer';

               l_processing_return := NULL;
               l_return_status := NULL;
               l_msg_count := NULL;
               l_msg_data := NULL;
               l_trans_count := NULL;

               l_processing_return :=
                  inv_txn_manager_pub.process_transactions (
                     p_api_version        => 1.0,
                     p_init_msg_list      => fnd_api.g_true,
                     p_commit             => fnd_api.g_true,
                     p_validation_level   => fnd_api.g_valid_level_full,
                     x_return_status      => l_return_status,
                     x_msg_count          => l_msg_count,
                     x_msg_data           => l_msg_data,
                     x_trans_count        => l_trans_count,
                     p_table              => 1,
                     p_header_id          => l_transaction_header_id);


               IF l_return_status = fnd_api.g_ret_sts_success
               THEN
                  l_return_status := NULL;

                  l_sec := 'Update SO Line with subinventory name';

                  update_so_line (rec_transfer.header_id,
                                  rec_transfer.line_id,
                                  'GeneralPCK',
                                  1,
                                  l_return_status,
                                  l_log_sequence);

                  IF l_return_status = fnd_api.g_ret_sts_success
                  THEN
                     BEGIN

                        l_sec := 'Insert into audit Table';

                        INSERT
                          INTO xxwc.xxwc_oe_counter_lines_tbl (
                                  line_id,
                                  line_num,
                                  header_id,
                                  order_number,
                                  inventory_item_id,
                                  ordered_quantity,
                                  primary_uom_code,
                                  line_type_id,
                                  ship_from_org_id,
                                  subinventory,
                                  stock_enabled_flag,
                                  mtl_transactions_enabled_flag,
                                  created_by,
                                  last_updated_by,
                                  creation_date,
                                  last_update_date,
                                  last_update_login)
                        VALUES (rec_transfer.line_id,
                                rec_transfer.line_num,
                                rec_transfer.header_id,
                                rec_transfer.order_number,
                                rec_transfer.inventory_item_id,
                                rec_transfer.ordered_quantity,
                                rec_transfer.primary_uom_code,
                                rec_transfer.line_type_id,
                                rec_transfer.ship_from_org_id,
                                rec_transfer.subinventory,
                                rec_transfer.stock_enabled_flag,
                                rec_transfer.mtl_transactions_enabled_flag,
                                fnd_global.user_id,
                                fnd_global.user_id,
                                SYSDATE,
                                SYSDATE,
                                fnd_global.login_id);
                     EXCEPTION
                        WHEN OTHERS
                        THEN
                           l_error_msg :=
                                 l_error_msg
                              || '-'
                              || rec_transfer.line_num
                              || '-'
                              || SUBSTR (SQLERRM, 100);
                           p_return_status := 'E';
                           p_error_msg := l_error_msg;
                     END;
                  END IF;
               END IF;
            END IF;
         END LOOP;
      END IF;
   EXCEPTION
      WHEN OTHERS
      THEN
         l_error_msg := l_error_msg || '-' || SUBSTR (SQLERRM, 100);
         p_error_msg := l_error_msg;
         p_return_status := 'E';

         xxcus_error_pkg.xxcus_error_main_api (
            p_called_from         => 'XXWC_WSH_SHIPPING_EXTN_PKG.Counter_subinv_xfer',
            p_calling             => l_sec,
            p_request_id          => fnd_global.conc_request_id,
            p_ora_error_msg       => l_error_msg,
            p_error_desc          => 'Error in Subinventory Transfer',
            p_distribution_list   => g_dflt_email,
            p_module              => 'OM');
   END Counter_subinv_xfer;

  FUNCTION sync_Counter_line (p_subscription_guid   IN     RAW,
                               p_event               IN OUT wf_event_t)
      RETURN VARCHAR2
   /**************************************************************************
   $Header xxwc_wsh_shipping_extn_pkg $
   Module Name: sync_Counter_line
   PURPOSE:   This Procedure is called from the business event
   REVISIONS:
   Ver        Date        Author                     Description
   ---------  ----------  ---------------         -------------------------
   3.4        12/17/2015  Manjula Chellappan    TMS #20151023-00037 - Shipping Extension Modification
                                                    for Counter Order Removal and PUBD
   **************************************************************************/
   IS
      l_plist                      wf_parameter_list_t := p_event.getparameterlist ();
      l_error_msg                  VARCHAR2 (500);

      CURSOR cur_transfer (p_line_id NUMBER)
      IS
         SELECT ol.ROWID rid,
                ol.header_id,
                ol.line_id,
                ol.line_type_id,
                ol.inventory_item_id,
                ol.ship_from_org_id,
                ol.ordered_quantity,
                ol.subinventory,
                ol.primary_uom_code,
                ol.order_number,
                ol.line_num,
                ol.stock_enabled_flag,
                ol.mtl_transactions_enabled_flag
           FROM xxwc.xxwc_oe_counter_lines_tbl ol,
            oe_order_lines_all ool
          WHERE ol.line_id = p_line_id
        AND ol.line_id = ool.line_id
      AND ol.ship_from_org_id = ool.ship_from_org_id;

      l_line_id                    NUMBER;
      l_transfer_qty               NUMBER;
      l_header_id                  NUMBER;
      l_transaction_type           VARCHAR2 (40) := 'Subinventory Transfer';
      l_transaction_type_id        NUMBER;
      l_transaction_header_id      NUMBER;
      l_interface_transaction_id   NUMBER;
      l_to_subinventory            VARCHAR2 (15);
      l_from_subinventory          VARCHAR2 (15);
      l_sec                        VARCHAR2 (100);
      l_processing_return          NUMBER;
      l_return_status              VARCHAR2 (240);
      l_msg_count                  NUMBER;
      l_msg_data                   VARCHAR2 (4000);
      l_trans_count                NUMBER;
      l_log_sequence               NUMBER;
   BEGIN
      l_sec := 'Initialize values';

      l_line_id := wf_event.getvalueforparameter ('P_LINE_ID', l_plist);
      l_transfer_qty :=
         wf_event.getvalueforparameter ('P_TRANSFER_QTY', l_plist);

      FOR rec_transfer IN cur_transfer (l_line_id)
      LOOP
         IF l_transfer_qty <> 0
         THEN
            l_sec := 'Set the subinventory based on Quantity';

            IF l_transfer_qty < 0
            THEN
               l_from_subinventory := rec_transfer.subinventory;
               l_to_subinventory := 'GeneralPCK';
            ELSIF l_transfer_qty > 0
            THEN
               l_from_subinventory := 'GeneralPCK';
               l_to_subinventory := rec_transfer.subinventory;
            END IF;

            l_sec := 'Get Transaction Type Id';

            BEGIN
               SELECT transaction_type_id
                 INTO l_transaction_type_id
                 FROM mtl_transaction_types
                WHERE transaction_type_name = l_transaction_type;
            EXCEPTION
               WHEN OTHERS
               THEN
                  DBMS_OUTPUT.put_line (
                     'Err Could not find transaction type information for Subinventory Transfer');
            END;

            l_sec := 'Get Transaction Header Id';

            SELECT mtl_material_transactions_s.NEXTVAL
              INTO l_transaction_header_id
              FROM DUAL;

            l_sec := 'Get Interface Transaction Id';

            SELECT mtl_material_transactions_s.NEXTVAL
              INTO l_interface_transaction_id
              FROM DUAL;

            l_sec := 'Insert into mtl_transactions_interface';

            IF     rec_transfer.stock_enabled_flag = 'Y'
               AND rec_transfer.mtl_transactions_enabled_flag = 'Y'
            THEN

                  INSERT
                    INTO mtl_transactions_interface (
                            transaction_interface_id,
                            transaction_header_id,
                            source_code,
                            source_line_id,
                            source_header_id,
                            process_flag,
                            transaction_mode,
                            lock_flag,
                            last_update_date,
                            last_updated_by,
                            creation_date,
                            created_by,
                            transaction_type_id,
                            transaction_date,
                            transaction_uom,
                            inventory_item_id,
                            subinventory_code,
                            organization_id,
                            transfer_subinventory,
                            transfer_organization,
                            transaction_quantity,
                            primary_quantity,
                            transaction_reference)
                     VALUES (
                               l_interface_transaction_id,
                               l_transaction_header_id,
                               l_transaction_type,               --source code
                               l_interface_transaction_id,    --source line id
                               l_interface_transaction_id,  --source header id
                               1,                               --process flag
                               3,                           --transaction mode
                               2,                                  --lock flag
                               SYSDATE,                     --last update date
                               0,                            --last updated by
                               SYSDATE,                         --created date
                               0,                                 --created by
                               l_transaction_type_id,      -- transaction type
                               SYSDATE,                    -- transaction date
                               rec_transfer.primary_uom_code,           -- uom
                               rec_transfer.inventory_item_id, -- inventory item id
                               l_from_subinventory,       -- from subinventory
                               rec_transfer.ship_from_org_id,
                               l_to_subinventory,           -- to subinventory
                               rec_transfer.ship_from_org_id,
                               ABS (l_transfer_qty),
                               ABS (l_transfer_qty),
                               (   rec_transfer.order_number
                                || '-'
                                || rec_transfer.line_num));


               l_sec := 'Call API for subinventory Transfer';

               l_processing_return := NULL;
               l_return_status := NULL;
               l_msg_count := NULL;
               l_msg_data := NULL;
               l_trans_count := NULL;
               l_processing_return :=
                  inv_txn_manager_pub.process_transactions (
                     p_api_version        => 1.0,
                     p_init_msg_list      => fnd_api.g_true,
                     p_commit             => fnd_api.g_false,
                     p_validation_level   => fnd_api.g_valid_level_full,
                     x_return_status      => l_return_status,
                     x_msg_count          => l_msg_count,
                     x_msg_data           => l_msg_data,
                     x_trans_count        => l_trans_count,
                     p_table              => 1,
                     p_header_id          => l_transaction_header_id);

               IF l_return_status = fnd_api.g_ret_sts_success
               THEN

                  IF l_return_status = fnd_api.g_ret_sts_success
                  THEN
                     BEGIN
                        l_sec := 'Update Quantity in Staging Table';

                        UPDATE xxwc.xxwc_oe_counter_lines_tbl
                           SET ordered_quantity =
                                  ordered_quantity - l_transfer_qty,
                               last_updated_by = fnd_global.user_id,
                               last_update_date = SYSDATE,
                               last_update_login = fnd_global.login_id
                         WHERE line_id = l_line_id
               AND ship_from_org_id = rec_transfer.ship_from_org_id;

                     EXCEPTION
                        WHEN NO_DATA_FOUND
                        THEN
                           ROLLBACK;

                        WHEN OTHERS
                        THEN
                           wf_core.context ('xxwc_wsh_shipping_extn_pkg',
                                            'sync_Counter_line',
                                            p_event.geteventname (),
                                            p_subscription_guid);
                           wf_event.seterrorinfo (p_event,
                                                  SUBSTR (SQLERRM, 1, 240));
                           RETURN 'ERROR';
                     END;
                  END IF;
               END IF;
            END IF;
         END IF;

     COMMIT;

      END LOOP;

      RETURN ('SUCCESS');
   EXCEPTION
      WHEN OTHERS
      THEN
         l_error_msg := SUBSTR (SQLERRM, 1, 240);

         wf_core.context ('xxwc_wsh_shipping_extn_pkg',
                          'sync_Counter_line',
                          p_event.geteventname (),
                          p_subscription_guid);
         wf_event.seterrorinfo (p_event, SUBSTR (SQLERRM, 1, 240));
         RETURN 'ERROR';

         xxcus_error_pkg.xxcus_error_main_api (
            p_called_from         => 'XXWC_WSH_SHIPPING_EXTN_PKG.sync_Counter_line',
            p_calling             => l_sec,
            p_request_id          => fnd_global.conc_request_id,
            p_ora_error_msg       => SUBSTR (
                                          ' Error_Stack...'
                                       || DBMS_UTILITY.format_error_stack ()
                                       || ' Error_Backtrace...'
                                       || DBMS_UTILITY.format_error_backtrace (),
                                       1,
                                       2000),
            p_error_desc          => SUBSTR (SQLERRM, 1, 240),
            p_distribution_list   => g_dflt_email,
            p_module              => 'OM');
   END sync_Counter_line;

PROCEDURE raise_sync_counter_line (p_line_id  IN NUMBER,
                                    p_line_qty IN NUMBER,
                                    p_error_msg  OUT VARCHAR2)

   /**************************************************************************
     $Header xxwc_wsh_shipping_extn_pkg $
     Module Name: raise_sync_counter_line2
     PURPOSE:   This Procedure is used to raise the business event
                to sync the Counter line subinv transfer for quantity update
     REVISIONS:
     Ver        Date        Author                     Description
     ---------  ----------  ---------------         -------------------------
     3.4        12/17/2015  Manjula Chellappan      TMS #20151023-00037 - Shipping Extension Modification
                                                     for Counter Order Removal and PUBD
     **************************************************************************/
   IS PRAGMA AUTONOMOUS_TRANSACTION;

      l_line_id                 NUMBER;
      l_error_msg               VARCHAR2 (4000);
      l_parameter_list          wf_parameter_list_t;
      l_event_data              CLOB;
      l_line_type_id            NUMBER;
      l_line_flow_status_code   VARCHAR2 (20);
    l_ship_from_org_id        NUMBER;
      l_sync_flag               NUMBER := 0;
      l_ordered_quantity        NUMBER := 0;
      l_orig_ordered_quantity   NUMBER;
      l_transfer_qty            NUMBER;
      l_sec                     VARCHAR2 (100);
   BEGIN
      l_sec := 'Initialize Values';

      l_line_id := p_line_id;
      l_ordered_quantity := p_line_qty;


      l_sec := 'Get Original Ordered Quantity from xxwc_oe_counter_lines_tbl';

         BEGIN
            SELECT NVL (ol.ordered_quantity, 0)
            ,ol.ship_from_org_id
              INTO l_orig_ordered_quantity
            ,l_ship_from_org_id
              FROM xxwc.xxwc_oe_counter_lines_tbl ol ,
             oe_order_lines_all ool
             WHERE ol.line_id = l_line_id
         AND ol.line_id = ool.line_id
         AND ol.ship_from_org_id = ool.ship_from_org_id;
         EXCEPTION
            WHEN OTHERS
            THEN
               l_orig_ordered_quantity := 0;
         END;

         l_transfer_qty := l_orig_ordered_quantity - l_ordered_quantity;

         IF l_transfer_qty <> 0
         THEN
            l_sec := 'Set values for Parameter List';

            l_parameter_list :=
               wf_parameter_list_t (
                  wf_parameter_t ('SEND_DATE',
                                  fnd_date.date_to_canonical (SYSDATE)),
                  wf_parameter_t ('P_LINE_ID', l_line_id),
                  wf_parameter_t ('P_TRANSFER_QTY', l_transfer_qty)
                            );

            l_sec := 'Raise business Event';

            wf_event.raise (
               p_event_name   => 'xxwc.oracle.apps.ont.order.syncCounterline',
               p_event_key    => SYS_GUID (),
               p_event_data   => l_event_data,
               p_parameters   => l_parameter_list);

      COMMIT;

         END IF;

   EXCEPTION
      WHEN OTHERS
      THEN
         xxcus_error_pkg.xxcus_error_main_api (
            p_called_from         => 'XXWC_WSH_SHIPPING_EXTN_PKG.raise_sync_counter_line',
            p_calling             => l_sec,
            p_request_id          => fnd_global.conc_request_id,
            p_ora_error_msg       => SUBSTR (
                                          ' Error_Stack...'
                                       || DBMS_UTILITY.format_error_stack ()
                                       || ' Error_Backtrace...'
                                       || DBMS_UTILITY.format_error_backtrace (),
                                       1,
                                       2000),
            p_error_desc          => SUBSTR (SQLERRM, 1, 240),
            p_distribution_list   => g_dflt_email,
            p_module              => 'OM');

     p_error_msg := 'Subinventory Transfer Failed '||SUBSTR (SQLERRM, 1, 240);
   END raise_sync_counter_line;

PROCEDURE audit_line_qty  (
    itemtype  IN VARCHAR2 ,
    itemkey   IN VARCHAR2 ,
    actid     IN NUMBER ,
    funcmode  IN VARCHAR2 ,
    resultout IN OUT NOCOPY VARCHAR2)
 /**************************************************************************
     $Header xxwc_wsh_shipping_extn_pkg $
     Module Name: audit_line_qty
     PURPOSE:   This Procedure is used to insert new record to the custom table
              at the time of order booking
     REVISIONS:
     Ver        Date        Author                     Description
     ---------  ----------  ---------------         -------------------------
     3.4        2/25/2016  Manjula Chellappan      TMS #20151023-00037 - Shipping Extension Modification
                                                     for Counter Order Removal and PUBD
     3.4.1      04/04/2016 Rakesh Patel            TMS 20160404-00014- Order workflow status fix to exclude SOURCE_TYPE_COD!=EXTERNAL
     3.5        03/17/2016 Rakesh Patel            TMS 20150622-00156 - Shipping Extension modification for PUBD
     **************************************************************************/
   IS
   l_sec          VARCHAR2(100);
   l_line_id    NUMBER;
   l_line_count   NUMBER;

   CURSOR cur_order_line IS
   SELECT ol.line_id
         ,ol.header_id
       ,ol.line_type_id
       ,ol.inventory_item_id
       ,ol.ship_from_org_id organization_id
       ,(ol.ordered_quantity
                     * po_uom_s.po_uom_convert (um.unit_of_measure
                                                , msib.primary_unit_of_measure
                                                , ol.inventory_item_id
                                               )
             ) ordered_quantity
       ,ol.org_id
    FROM oe_order_lines ol
       , mtl_system_items_b msib
     , mtl_units_of_measure_vl um
     WHERE line_id = itemkey
       AND ol.inventory_item_id = msib.inventory_item_id
     AND ol.ship_from_org_id = msib.organization_id
     AND ol.order_quantity_uom = um.uom_code
     AND ol.source_type_code = 'INTERNAL' -- Added by Rakesh Patel on 04-Apr-2016 for TMS 20160404-00014
           AND subinventory != 'PUBD'; --Added for ver 3.5

   BEGIN

   IF (funcmode = 'RUN') THEN
    oe_standard_wf.set_msg_context (actid);

  FOR rec_order_line IN cur_order_line
  LOOP

  IF   rec_order_line.line_type_id
        IN (fnd_profile.value('XXWC_STANDARD_LINE_TYPE'), fnd_profile.value('XXWC_INTERNAL_ORDER_LINE_TYPE')) THEN

      SELECT count(*)
        INTO l_line_count
        FROM xxwc.xxwc_oe_open_order_lines
       WHERE line_id = rec_order_line.line_id;

       IF l_line_count = 0 THEN

        INSERT INTO
             XXWC.xxwc_oe_open_order_lines
             (line_id
             ,header_id
             ,line_type_id
             ,inventory_item_id
             ,organization_id
             ,ordered_quantity
             ,org_id
             ,created_by
             ,last_updated_by
             ,creation_date
             ,last_update_date
             ,last_update_login)
        VALUES (rec_order_line.line_id
             ,rec_order_line.header_id
             ,rec_order_line.line_type_id
             ,rec_order_line.inventory_item_id
             ,rec_order_line.organization_id
             ,rec_order_line.ordered_quantity
             ,rec_order_line.org_id
             ,fnd_global.user_id
             ,fnd_global.user_id
             ,sysdate
             ,sysdate
             ,fnd_global.login_id);

       ELSE
        UPDATE XXWC.xxwc_oe_open_order_lines
           SET ordered_quantity = rec_order_line.ordered_quantity,
             organization_id = rec_order_line.organization_id,
             last_updated_by = fnd_global.user_id,
             last_update_login = fnd_global.login_id,
             last_update_date = sysdate
         WHERE line_id = rec_order_line.line_id;

       END IF;
  END IF;

  END LOOP;

  END IF;

    resultout := 'COMPLETE';
    EXCEPTION
    WHEN OTHERS THEN
      wf_core.CONTEXT ('XXWC_WSH_SHIPPING_EXTN_PKG', 'AUDIT_LINE_QTY', itemtype, itemkey, TO_CHAR (actid), funcmode );
      RAISE;

  END audit_line_qty;


PROCEDURE delete_audit_line  (
    itemtype  IN VARCHAR2 ,
    itemkey   IN VARCHAR2 ,
    actid     IN NUMBER ,
    funcmode  IN VARCHAR2 ,
    resultout IN OUT NOCOPY VARCHAR2)
 /**************************************************************************
     $Header xxwc_wsh_shipping_extn_pkg $
     Module Name: audit_line_qty
     PURPOSE:   This Procedure is used to Delete the audit line from the custom table
              When order line cancelled
     REVISIONS:
     Ver        Date        Author                     Description
     ---------  ----------  ---------------         -------------------------
     3.4        2/25/2016  Manjula Chellappan      TMS #20151023-00037 - Shipping Extension Modification
                                                     for Counter Order Removal and PUBD
     **************************************************************************/
   IS
   l_sec          VARCHAR2(100);
   l_line_id    NUMBER;
   l_line_count   NUMBER;

   BEGIN

   IF (funcmode = 'RUN') THEN
    oe_standard_wf.set_msg_context (actid);

    DELETE FROM XXWC.xxwc_oe_open_order_lines
       WHERE line_id = itemkey;

  END IF;

    resultout := 'COMPLETE';
    EXCEPTION
    WHEN OTHERS THEN
      wf_core.CONTEXT ('XXWC_WSH_SHIPPING_EXTN_PKG', 'DELETE_AUDIT_LINE', itemtype, itemkey, TO_CHAR (actid), funcmode );
      RAISE;

  END delete_audit_line;

PROCEDURE update_audit_line_qty (
      p_application_id                 IN            NUMBER,
      p_entity_short_name              IN            VARCHAR2,
      p_validation_entity_short_name   IN            VARCHAR2,
      p_validation_tmplt_short_name    IN            VARCHAR2,
      p_record_set_short_name          IN            VARCHAR2,
      p_scope                          IN            VARCHAR2,
      x_result                            OUT NOCOPY NUMBER)
   /**************************************************************************
     $Header xxwc_wsh_shipping_extn_pkg $
     Module Name: update_audit_line_qty
     PURPOSE:   This Procedure is used in processing constraint
                to sync the Standard line quantity update
     REVISIONS:
     Ver        Date        Author                     Description
     ---------  ----------  ---------------         -------------------------
     3.4        02/29/2016  Manjula Chellappan      TMS #20151023-00037 - Shipping Extension Modification
                                                     for Counter Order Removal and PUBD
     3.4.1      04/04/2016  Rakesh Patel            TMS 20160404-00014- Order workflow status fix to exclude SOURCE_TYPE_COD!=EXTERNAL
     3.5        03/17/2016  Rakesh Patel            TMS 20150622-00156 - Shipping Extension modification for PUBD
     3.8        02/02/2017  Nancy Pahwa             TMS 20161102-00131 - AIS Open SO Quantity Issue when branch changed
     **************************************************************************/
   IS
      l_line_id                 NUMBER;
      l_error_msg               VARCHAR2 (240);
      l_line_type_id            NUMBER;
      l_line_flow_status_code   oe_order_lines_all.flow_status_code%TYPE; -- Changed by Rakesh Patel on 04-Apr-2016 for TMS 20160404-00014
      l_sync_flag               NUMBER := 0;
      l_ordered_quantity        NUMBER := 0;
      l_organization_id         NUMBER ;
      l_subinventory            oe_order_lines_all.subinventory%TYPE; --Added for ver 3.5
      l_new_order_quantity_uom  oe_order_lines_all.order_quantity_uom%TYPE;
      l_inventory_item_id       oe_order_lines_all.inventory_item_id%TYPE;
      l_new_ordered_quantity    oe_order_lines_all.ordered_quantity%TYPE;
      l_sec                     VARCHAR2 (100);
    l_pubd_line_count         NUMBER;

    --Added below cursor for ver 3.5
     CURSOR cur_order_line IS
   SELECT header_id
         ,inventory_item_id
         ,org_id
         ,subinventory
         ,ship_from_org_id
         ,ordered_quantity
    FROM oe_order_lines
     WHERE line_id = l_line_id;

   BEGIN

      l_sec := 'Initialize Values';

      l_line_id := oe_line_security.g_record.line_id;
      l_line_type_id := oe_line_security.g_record.line_type_id;
      l_line_flow_status_code := oe_line_security.g_record.flow_status_code;
      l_new_ordered_quantity  := oe_line_security.g_record.ordered_quantity;
      l_organization_id := oe_line_security.g_record.ship_from_org_id;
      l_subinventory := oe_line_security.g_record.subinventory; --Added for ver 3.5
      l_new_order_quantity_uom := oe_line_security.g_record.order_quantity_uom;
      l_inventory_item_id :=  oe_line_security.g_record.inventory_item_id;

      BEGIN
         SELECT(l_new_ordered_quantity
                  * po_uom_s.po_uom_convert (um.unit_of_measure
                                             , msib.primary_unit_of_measure
                                             , l_inventory_item_id
                                            )
                )
           INTO l_ordered_quantity
           FROM  mtl_system_items_b msib
         , mtl_units_of_measure_vl um
          WHERE msib.inventory_item_id  = l_inventory_item_id
            AND msib.organization_id    = l_organization_id
            AND um.uom_code             = l_new_order_quantity_uom;
      EXCEPTION
            WHEN OTHERS
            THEN
               l_ordered_quantity := 0;
      END;

    -- Version 3.5 > Start
    FOR rec_order_line IN cur_order_line
    LOOP
       IF l_line_type_id IN (fnd_profile.value('XXWC_STANDARD_LINE_TYPE'), fnd_profile.value('XXWC_INTERNAL_ORDER_LINE_TYPE'))
            AND l_line_flow_status_code <> 'ENTERED' AND l_ordered_quantity <> rec_order_line.ordered_quantity
         THEN
      -- Version 3.5 < End
            UPDATE XXWC.xxwc_oe_open_order_lines
           SET ordered_quantity = l_ordered_quantity,
               organization_id = l_organization_id,
             last_update_date = sysdate,
                   last_updated_by = fnd_global.user_id,
                   last_update_login = fnd_global.login_id
           WHERE line_id = l_line_id;
      -- Version 3.5 > Start
         END IF;

        IF l_line_type_id IN (fnd_profile.value('XXWC_STANDARD_LINE_TYPE'), fnd_profile.value('XXWC_INTERNAL_ORDER_LINE_TYPE'))
      THEN
      SELECT count(*)
        INTO l_pubd_line_count
        FROM xxwc.xxwc_wsh_shipping_stg
       WHERE line_id = l_line_id
         AND NVL(subinventory, 'XXXX') = 'PUBD';

       IF rec_order_line.subinventory <> l_subinventory AND l_subinventory = 'PUBD' THEN
          DELETE FROM XXWC.xxwc_oe_open_order_lines
          WHERE line_id = l_line_id;
       ELSIF ( (rec_order_line.subinventory <> l_subinventory AND rec_order_line.subinventory = 'PUBD'
             )
               OR
         rec_order_line.ship_from_org_id <> l_organization_id AND (rec_order_line.subinventory = 'PUBD' AND l_subinventory = 'PUBD')
         )
              AND l_pubd_line_count =0 THEN
          INSERT
        INTO XXWC.xxwc_oe_open_order_lines
          (
          line_id ,
          header_id ,
          line_type_id ,
          inventory_item_id ,
          organization_id ,
          ordered_quantity ,
          org_id ,
          created_by ,
          last_updated_by ,
          creation_date ,
          last_update_date ,
          last_update_login
          )
         VALUES
          (l_line_id,
          rec_order_line.header_id ,
          l_line_type_id ,
          rec_order_line.inventory_item_id ,
          l_organization_id ,
          l_ordered_quantity ,
          rec_order_line.org_id ,
          fnd_global.user_id ,
          fnd_global.user_id ,
          sysdate ,
          sysdate ,
          fnd_global.login_id
        );
        END IF;
     END IF;
            -- Version 3.8 > Start
        IF l_line_type_id IN (fnd_profile.value('XXWC_STANDARD_LINE_TYPE'), fnd_profile.value('XXWC_INTERNAL_ORDER_LINE_TYPE'))
            AND l_line_flow_status_code <> 'ENTERED' AND l_organization_id <> rec_order_line.ship_from_org_id
         THEN

            UPDATE XXWC.xxwc_oe_open_order_lines
               SET ordered_quantity = l_ordered_quantity,
                   organization_id = l_organization_id,
                   last_update_date = sysdate,
                   last_updated_by = fnd_global.user_id,
                   last_update_login = fnd_global.login_id
             WHERE line_id = l_line_id;
          END IF;
          -- Version 3.8 < End
    END LOOP;
    -- Version 3.5 < End

    x_result := 0;

   EXCEPTION

      WHEN NO_DATA_FOUND THEN
         NULL;
      WHEN OTHERS
      THEN
         xxcus_error_pkg.xxcus_error_main_api (
            p_called_from         => 'XXWC_WSH_SHIPPING_EXTN_PKG.update_audit_line_qty',
            p_calling             => l_sec,
            p_request_id          => fnd_global.conc_request_id,
            p_ora_error_msg       => SUBSTR (
                                          ' Error_Stack...'
                                       || DBMS_UTILITY.format_error_stack ()
                                       || ' Error_Backtrace...'
                                       || DBMS_UTILITY.format_error_backtrace (),
                                       1,
                                       2000),
            p_error_desc          => SUBSTR (SQLERRM, 1, 240),
            p_distribution_list   => g_dflt_email,
            p_module              => 'OM');

         x_result := 1;

   END update_audit_line_qty;


PROCEDURE audit_split_int_line_qty  (
    p_line_id    IN   NUMBER,
  p_error_msg  OUT  VARCHAR2)
 /**************************************************************************
     $Header xxwc_wsh_shipping_extn_pkg $
     Module Name: audit_split_int_line_qty
     PURPOSE:   This Procedure is used to insert new record to the custom table
              at the time of new delivery line creation for internal order lines
     REVISIONS:
     Ver        Date        Author                     Description
     ---------  ----------  ---------------         -------------------------
     3.4        3/4/2016  Manjula Chellappan      TMS #20151023-00037 - Shipping Extension Modification
                                                     for Counter Order Removal and PUBD
     3.5        03/17/2016  Rakesh Patel          TMS 20150622-00156 - Shipping Extension modification for PUBD
     **************************************************************************/
  IS
   l_sec          VARCHAR2(100);
   l_line_id    NUMBER;
   l_line_count   NUMBER;

   CURSOR cur_order_line IS
   SELECT ol.line_id
         ,ol.header_id
       ,ol.line_type_id
       ,ol.inventory_item_id
       ,ol.ship_from_org_id organization_id
       ,(ol.ordered_quantity
                     * po_uom_s.po_uom_convert (um.unit_of_measure
                                                , msib.primary_unit_of_measure
                                                , ol.inventory_item_id
                                               )
            ) ordered_quantity
       ,ol.org_id
    FROM oe_order_lines ol
       , mtl_system_items_b msib
       , mtl_units_of_measure_vl um
     WHERE ol.line_id = p_line_id
       AND ol.inventory_item_id  = msib.inventory_item_id
     AND ol.ship_from_org_id   = msib.organization_id
     AND ol.order_quantity_uom = um.uom_code
       AND subinventory         != 'PUBD'; --Added for ver 3.5

   BEGIN

   l_sec := 'Initialize Values';
  FOR rec_order_line IN cur_order_line
  LOOP

  IF   rec_order_line.line_type_id =1012 THEN

      SELECT count(*)
        INTO l_line_count
        FROM xxwc.xxwc_oe_open_order_lines
       WHERE line_id = rec_order_line.line_id;

       IF l_line_count = 0 THEN
          l_sec := 'Insert into xxwc_oe_open_order_lines';
        INSERT INTO
             XXWC.xxwc_oe_open_order_lines
             (line_id
             ,header_id
             ,line_type_id
             ,inventory_item_id
             ,organization_id
             ,ordered_quantity
             ,org_id
             ,created_by
             ,last_updated_by
             ,creation_date
             ,last_update_date
             ,last_update_login)
        VALUES (rec_order_line.line_id
             ,rec_order_line.header_id
             ,rec_order_line.line_type_id
             ,rec_order_line.inventory_item_id
             ,rec_order_line.organization_id
             ,rec_order_line.ordered_quantity
             ,rec_order_line.org_id
             ,fnd_global.user_id
             ,fnd_global.user_id
             ,sysdate
             ,sysdate
             ,fnd_global.login_id);

       END IF;
  END IF;

  END LOOP;

    EXCEPTION
    WHEN OTHERS THEN
      p_error_msg := 'Record not created in the audit table '||SQLERRM;

      xxcus_error_pkg.xxcus_error_main_api (
            p_called_from         => 'XXWC_WSH_SHIPPING_EXTN_PKG.audit_split_int_line_qty',
            p_calling             => l_sec,
            p_request_id          => fnd_global.conc_request_id,
            p_ora_error_msg       => SUBSTR (
                                          ' Error_Stack...'
                                       || DBMS_UTILITY.format_error_stack ()
                                       || ' Error_Backtrace...'
                                       || DBMS_UTILITY.format_error_backtrace (),
                                       1,
                                       2000),
            p_error_desc          => SUBSTR (p_error_msg, 1, 240),
            p_distribution_list   => g_dflt_email,
            p_module              => 'OM');

   END audit_split_int_line_qty;

--Added for Ver 3.4 End

end xxwc_wsh_shipping_extn_pkg;
/