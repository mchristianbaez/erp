  /****************************************************************************************************
  Table: FND_USERS & PER_ALL_PEOPLE_F  	
  Description: Updating email address for all users/employees with 'HDSHRITUNITTesting@hdsupply.com' 
               email id
  HISTORY
  =====================================================================================================
  VERSION DATE               AUTHOR(S)       DESCRIPTION
  ------- -----------------  --------------- ----------------------------------------------------------
  1.0     30-Mar-2016        P.Vamshidhar    Initial version TMS#20160330-00070 
                                             Update script to update all users email addresses in test environments
  *****************************************************************************************************/
SET SERVEROUTPUT ON SIZE 1000000

BEGIN
   DBMS_OUTPUT.put_line ('Before Update');

   UPDATE apps.fnd_user
      SET email_address = 'HDSHRITUNITTesting@hdsupply.com'
    WHERE email_address IS NOT NULL;

   DBMS_OUTPUT.put_line ('Records updated (FND_USERS): ' || SQL%ROWCOUNT);

   UPDATE apps.per_all_people_f
      SET email_address = 'HDSHRITUNITTesting@hdsupply.com'
    WHERE email_address IS NOT NULL;

   DBMS_OUTPUT.put_line (
      'Records updated (PER_ALL_PEOPLE_F): ' || SQL%ROWCOUNT);

   COMMIT;

   DBMS_OUTPUT.put_line ('After Update');
EXCEPTION
   WHEN OTHERS
   THEN
      DBMS_OUTPUT.put_line ('Unable to update ' || SQLERRM);
END;
/