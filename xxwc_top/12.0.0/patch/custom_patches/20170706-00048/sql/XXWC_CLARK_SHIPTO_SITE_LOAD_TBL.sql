/*******************************************************************************************************
  -- Table Name XXWC_CLARK_SHIPTOSITE_LOAD_TBL
  -- ***************************************************************************************************
  --
  -- PURPOSE: External table used to store account and site details of Clark
  -- HISTORY
  -- ===================================================================================================
  -- ===================================================================================================
  -- VERSION DATE          AUTHOR(S)       DESCRIPTION
  -- ------- -----------   --------------- -------------------------------------------------------------
  -- 1.0     14-Jul-2017   Niraj K Ranjan   TMS#20170706-00048   Jobsite codes need to be added for Clark Construction Group
********************************************************************************************************/
DROP TABLE XXWC.XXWC_CLARK_SHIPTOSITE_LOAD_TBL;
CREATE TABLE XXWC.XXWC_CLARK_SHIPTOSITE_LOAD_TBL
   ( CUSTOMER_ACCOUNT_NAME  VARCHAR2(500 BYTE),
     PARTY_ID NUMBER,
     ACCOUNT_NUMBER  NUMBER,
	 SHIP_TO_SITE_LOCATION VARCHAR2(500 BYTE),
	 SHIP_TO_SITE_NUMBER VARCHAR2(500 BYTE),
	 SHIP_TO_SITE_CITY VARCHAR2(500 BYTE),
	 SHIP_TO_SITE_STATE_CODE VARCHAR2(50 BYTE),
	 SHIP_TO_SITE_ZIP_CODE VARCHAR2(50 BYTE),
	 SALES$ VARCHAR2(500 BYTE),
	 DEFAULT_SHIP_BRANCH VARCHAR2(50 BYTE)
   )
   ORGANIZATION EXTERNAL
    ( TYPE ORACLE_LOADER
      DEFAULT DIRECTORY "XXWC_CLARK_SHIPTO_SITE_LOAD"
      ACCESS PARAMETERS
      ( RECORDS DELIMITED BY NEWLINE
        FIELDS TERMINATED BY '|'
      )
      LOCATION
       ( 'xxwc_clark_shipto_sites_ship_from_branch_data.txt'
       )
    )
   REJECT LIMIT UNLIMITED ;
