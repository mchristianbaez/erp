/*******************************************************************************************************
  -- Script Name XXWC_TMS20170706_00048_CLARK_SHIPTO_SITE_UPDATE.sql
  -- ***************************************************************************************************
  --
  -- PURPOSE: Script to update attribute10 and attribute11 of hz_cust_site_uses_all
  -- HISTORY
  -- ===================================================================================================
  -- ===================================================================================================
  -- VERSION DATE          AUTHOR(S)       DESCRIPTION
  -- ------- -----------   --------------- -------------------------------------------------------------
  -- 1.0     14-Jul-2017   Niraj K Ranjan   TMS#20170706-00048   Jobsite codes need to be added for Clark Construction Group
********************************************************************************************************/
SET SERVEROUTPUT ON SIZE 1000000;
drop table xxwc.xxwc_hz_cust_site_uses_all_bkp;
CREATE TABLE xxwc.xxwc_hz_cust_site_uses_all_bkp AS
(SELECT hcsu.*
      FROM xxwc.XXWC_CLARK_SHIPTOSITE_LOAD_TBL xcsl
           ,apps.hz_cust_acct_sites_all hcas
           ,apps.hz_cust_site_uses_all hcsu
           ,apps.hz_cust_accounts_all hca
           ,apps.hz_party_sites hps
           ,apps.hz_locations hl
      WHERE     1 = 1
      AND TO_CHAR(xcsl.ACCOUNT_NUMBER) = hca.ACCOUNT_NUMBER
      AND xcsl.SHIP_TO_SITE_NUMBER = HPS.party_site_number
      AND hca.cust_account_id = hcas.cust_account_id
      AND hcas.cust_acct_site_id = hcsu.cust_acct_site_id
      AND hcsu.site_use_code = 'SHIP_TO'
      AND hps.location_id = hl.location_id
      AND hcas.party_site_id = hps.party_site_id
      --AND UPPER (hl.state) = 'AZ'
      AND hcas.status = 'A'
      AND hcsu.status = 'A'
      AND hca.status  = 'A');
/

DECLARE
   l_errmsg VARCHAR2(2000);
   l_rec_updated NUMBER;
   CURSOR cr_clark_site
   IS
      SELECT  XCSL.ACCOUNT_NUMBER,
	          XCSL.SHIP_TO_SITE_NUMBER,
			  XCSL.DEFAULT_SHIP_BRANCH,
			  HCSU.SITE_USE_ID
      FROM xxwc.XXWC_CLARK_SHIPTOSITE_LOAD_TBL xcsl
           ,apps.hz_cust_acct_sites_all hcas
           ,apps.hz_cust_site_uses_all hcsu
           ,apps.hz_cust_accounts_all hca
           ,apps.hz_party_sites hps
           ,apps.hz_locations hl
      WHERE     1 = 1
      AND TO_CHAR(xcsl.ACCOUNT_NUMBER) = hca.ACCOUNT_NUMBER
      AND xcsl.SHIP_TO_SITE_NUMBER = HPS.party_site_number
      AND hca.cust_account_id = hcas.cust_account_id
      AND hcas.cust_acct_site_id = hcsu.cust_acct_site_id
      AND hcsu.site_use_code = 'SHIP_TO'
      AND hps.location_id = hl.location_id
      AND hcas.party_site_id = hps.party_site_id
      --AND UPPER (hl.state) = 'AZ'
      AND hcas.status = 'A'
      AND hcsu.status = 'A'
      AND hca.status  = 'A';
BEGIN
   DBMS_OUTPUT.put_line ('TMS: 20170706-00048   , Start of site update');
   FOR rec_clark_site IN cr_clark_site
   LOOP
      UPDATE apps.hz_cust_site_uses_all hcsu
	  SET ATTRIBUTE10 = rec_clark_site.SHIP_TO_SITE_NUMBER,
	      ATTRIBUTE11 = rec_clark_site.DEFAULT_SHIP_BRANCH
      WHERE hcsu.SITE_USE_ID = rec_clark_site.SITE_USE_ID;
	  l_rec_updated := SQL%ROWCOUNT;
	  DBMS_OUTPUT.put_line ('TMS: 20170706-00048   , Total record updated for Account# '||rec_clark_site.ACCOUNT_NUMBER||' Site# '||rec_clark_site.SHIP_TO_SITE_NUMBER||' SiteUseId# '||rec_clark_site.SITE_USE_ID||' is '||l_rec_updated);
   END LOOP;
   COMMIT; 
   DBMS_OUTPUT.put_line ('TMS: 20170706-00048   , End of site process');
EXCEPTION
   WHEN OTHERS THEN
      l_errmsg := SUBSTR(SQLERRM,1,2000);
	  ROLLBACK;
	  DBMS_OUTPUT.put_line ('TMS: 20170706-00048   , Unexpected Error=>'||l_errmsg);
END;
/
