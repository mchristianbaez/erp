/*************************************************************************
  $Header TMS_20160224_00038.sql $
  Module Name: TMS_20160224_00038

  PURPOSE: Datascript to delete the delivery id

  REVISIONS:
  Ver        Date         Author                Description
  ---------  -----------  ------------------    --------------------------
  1.0        24-FEB-2016  Gopi Damuluri         TMS#20160224_00038

**************************************************************************/ 
SET SERVEROUTPUT ON SIZE 1000000
DECLARE
BEGIN
   DBMS_OUTPUT.put_line ('TMS: 20160224_00038   , Before Update');

   DELETE FROM XXWC.XXWC_WSH_SHIPPING_STG
    WHERE delivery_id = 4483698
      AND header_id = 38740729;

   DBMS_OUTPUT.put_line (
         'TMS: 20160224_00038 Number of records deleted (Expected:11): '
      || SQL%ROWCOUNT);

   COMMIT;

   DBMS_OUTPUT.put_line ('TMS: 20160224_00038   , End Update');
EXCEPTION
   WHEN OTHERS
   THEN
      ROLLBACK;
      DBMS_OUTPUT.put_line ('TMS: 20160224_00038, Errors : ' || SQLERRM);
END;
/