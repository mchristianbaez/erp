CREATE OR REPLACE PACKAGE BODY apps.xxwc_opsgeine_pkg AS
  /*************************************************************************
    $Header xxwc_opsgeine_pkg $
    Module Name: xxwc_opsgeine_pkg.pkb
  
    PURPOSE:   integrate with grid to send emails to right groups
  
    REVISIONS:
    Ver        Date        Author                     Description
    ---------  ----------  ---------------         -------------------------
    1.0        03/16/2018  Nancy Pahwa          Initial Version TMS#20180319-00169
  
  **************************************************************************/
  procedure XXWC_OPSGEINE_PROC(p_request_id IN NUMBER) IS
    l_user_name                    applsys.fnd_user.user_name%TYPE;
    l_notify_on_error              fnd_lookup_values.attribute1%TYPE;
    l_notify_on_warning            fnd_lookup_values.attribute1%TYPE;
    l_dflt_email                   fnd_lookup_values.attribute3%TYPE := 'WC-ITBSAAlerts-U1@HDSupply.com';
    l_sender                       VARCHAR2(100);
    lvc_status_code                VARCHAR2(10);
    l_sid                          VARCHAR2(8);
    l_body                         VARCHAR2(32767);
    l_body_header                  VARCHAR2(32767);
    l_body_detail                  VARCHAR2(32767);
    l_body_footer                  VARCHAR2(32767);
    l_sql_hint                     VARCHAR2(32767);
    l_hostname                     VARCHAR2(64);
    l_version                      VARCHAR2(17);
    l_instance_name                VARCHAR2(16);
    l_subject                      VARCHAR2(32767);
    g_host                         VARCHAR2(256) := 'mailoutrelay.hdsupply.net';
    g_hostport                     VARCHAR2(20) := '25';
    l_concurrent_program_id        applsys.FND_CONCURRENT_REQUESTS.concurrent_program_id%TYPE;
    l_concurrent_program_name      fnd_concurrent_programs_vl.CONCURRENT_PROGRAM_NAME%TYPE;
    l_description                  fnd_concurrent_programs_vl.DESCRIPTION%TYPE;
    l_user_concurrent_program_name fnd_concurrent_programs_vl.USER_CONCURRENT_PROGRAM_NAME%TYPE;
    l_completion_text              applsys.FND_CONCURRENT_REQUESTS.completion_text%TYPE;
    l_request_date                 applsys.FND_CONCURRENT_REQUESTS.request_date%TYPE;
    l_status                        varchar2(100);
    FATAL_ERROR EXCEPTION;
  BEGIN
    SELECT a.PROGRAM_SHORT_NAME,
           a.DESCRIPTION,
           a.USER_CONCURRENT_PROGRAM_NAME,
           a.completion_text,
           a.request_date,
           a.status_code,
           case when a.status_code = 'E' then 'Error'
             when a.status_code = 'G' then 'Warning'
               end Status
      INTO l_concurrent_program_name,
           l_description,
           l_user_concurrent_program_name,
           l_completion_text,
           l_request_date,
           lvc_status_code,
           l_status
      FROM FND_CONC_REQ_SUMMARY_V a
     WHERE 1 = 1
       AND a.request_id = p_request_id
       AND a.phase_code = 'C'
       AND a.status_code IN ('E', 'G');
  
    SELECT LOWER(NAME) INTO l_sid FROM v$database;
  
    IF lvc_status_code IS NOT NULL AND l_sid = 'ebsprd' THEN
      l_sender := 'Oracle.Applications_' || l_sid || '@hdsupply.com';
    
      SELECT host_name, instance_name, version
        INTO l_hostname, l_instance_name, l_version
        FROM v$instance;
    
      BEGIN
        SELECT u.user_name, t.concurrent_program_id
          INTO l_user_name, l_concurrent_program_id
          FROM applsys.FND_CONCURRENT_REQUESTS t, fnd_user u
         WHERE u.user_id = t.requested_by
           AND t.request_id = p_request_id
           AND TRUNC(SYSDATE) BETWEEN TRUNC(NVL(START_DATE, SYSDATE - 1)) AND
               TRUNC(NVL(END_DATE, SYSDATE - 1));
      EXCEPTION
        WHEN OTHERS THEN
          l_user_name := NULL;
      END;
    
      BEGIN
        SELECT attribute1, attribute2, attribute3
          INTO l_notify_on_error, l_notify_on_warning, l_dflt_email
          FROM fnd_lookup_values flv
         WHERE flv.lookup_type = 'XXWC_OPSGENIE_NOTIF_MATRIX'
           AND flv.lookup_code = l_user_name
           AND flv.enabled_flag = 'Y'
           AND TRUNC(SYSDATE) BETWEEN
               TRUNC(nvl(flv.start_date_active, sysdate - 1)) AND
               TRUNC(NVL(flv.end_date_active, SYSDATE + 1));
      EXCEPTION
        WHEN NO_DATA_FOUND THEN
          BEGIN
            SELECT attribute1, attribute2, attribute3
              INTO l_notify_on_error, l_notify_on_warning, l_dflt_email
              FROM fnd_lookup_values flv
             WHERE flv.lookup_type = 'XXWC_OPSGENIE_NOTIF_MATRIX'
               AND flv.lookup_code = 'OTHER'
               AND flv.enabled_flag = 'Y'
               AND TRUNC(SYSDATE) BETWEEN
                   TRUNC(nvl(flv.start_date_active, sysdate - 1)) AND
                   TRUNC(NVL(flv.end_date_active, SYSDATE + 1));
          EXCEPTION
            WHEN OTHERS THEN
              l_notify_on_error   := NULL;
              l_notify_on_warning := NULL;
              l_dflt_email        := NULL;
          END;
      END;
    
      IF (l_notify_on_error = 'Y' OR l_notify_on_warning = 'Y') THEN
        BEGIN
          l_subject     := 'WC Program Event:' || 'Critical' || '-' || l_status || ':' ||
                           l_sid || ':' || l_user_concurrent_program_name || ':' ||
                           l_concurrent_program_name;
          l_body_detail := l_body_detail || 'Host: ' || l_hostname ||
                           '<BR>' || 'Instance: ' || l_instance_name ||
                           '<BR>' || 'Database: ' || l_sid || '<BR>' ||
                           'Version: ' || l_version || '<BR>' ||
                           'Severity: ' || 'Critical' || '<BR>' ||
                           'Event Type: ' || l_description || '<BR>' ||
                           'Event Name: ' || l_concurrent_program_name ||
                           '<BR>' || 'Message: ' || l_completion_text ||
                           '<BR>' ||
                          /*REGEXP_REPLACE(l_completion_text,
                                                                                                                      '[[:cntrl:]]',
                                                                                                                      NULL) || '<BR>' ||*/
                           'Event Reported Time: ' || l_request_date ||
                           '<BR>' || 'Request ID: ' || p_request_id ||
                           '<BR>' || 'Metric Status: ' || NULL || '<BR>' ||
                           'Metric Value: ' || NULL || '<BR>' ||
                           'Metric High Value: ' || NULL || '<BR>' ||
                           'Metric Low Value: ' || NULL || '<BR>' || '<BR>';
          l_body_footer := 'Metric Debug Info:<BR>' || l_sql_hint;
          l_body        := l_body_header || l_body_detail || l_body_footer;
          xxcus_misc_pkg.html_email(p_to            => l_dflt_email,
                                    p_from          => l_sender,
                                    p_text          => 'test',
                                    p_subject       => l_subject,
                                    p_html          => l_body,
                                    p_smtp_hostname => g_host,
                                    p_smtp_portnum  => g_hostport);
        
        END;
      END IF;
    END IF;
  EXCEPTION
    WHEN OTHERS THEN
      ROLLBACK;
      xxcus_misc_pkg.html_email(p_to            => l_dflt_email,
                                p_from          => l_sender,
                                p_text          => 'test',
                                p_subject       => l_subject,
                                p_html          => l_body,
                                p_smtp_hostname => g_host,
                                p_smtp_portnum  => g_hostport);
      raise_application_error(-20001, 'Error reporting the error table.');
  END XXWC_OPSGEINE_PROC;
  PROCEDURE XXWC_BSA_GRID_CONCURRENT IS
      /*************************************************************************
    $Header xxwc_opsgeine_pkg $
    Module Name: XXWC_BSA_GRID_CONCURRENT
  
    PURPOSE:   check the error concurrent program
  
    REVISIONS:
    Ver        Date        Author                     Description
    ---------  ----------  ---------------         -------------------------
    1.0        03/16/2018  Nancy Pahwa          Initial Version TMS#20180319-00169
  
  **************************************************************************/
    l_request_id  xxwc.XXWC_GRID_CONCURRENT_TBL.request_id%type;
    l_status_flag xxwc.XXWC_GRID_CONCURRENT_TBL.status_flag%type;
  begin
    for i in (SELECT request_id
                FROM apps.FND_CONC_REQ_SUMMARY_V a
               WHERE 1 = 1
                 AND a.phase_code = 'C'
                 AND a.status_code IN ('E')
                 AND a.ACTUAL_COMPLETION_DATE > sysdate - 5 / (24 * 60)
              --  and rownum < 2
              ) loop
      begin
        select request_id, status_flag
          into l_request_id, l_status_flag
          from xxwc.XXWC_GRID_CONCURRENT_TBL
         where request_id = i.request_id
         and status = 'E';
      exception
        when no_data_found then
          l_status_flag := 'P';
      end;
      if l_status_flag = 'P' then
        insert into xxwc.XXWC_GRID_CONCURRENT_TBL
          (request_id, status_flag, status)
        values
          (i.request_id, 'N','E');
        commit;
        l_status_flag := 'N';
      end if;
      if l_status_flag = 'N' then
        for a in (select request_id, status_flag
                    from xxwc.XXWC_GRID_CONCURRENT_TBL where status = 'E' and request_id = i.request_id ) loop
          if a.status_flag = 'N' then
            apps.xxwc_opsgeine_pkg.XXWC_OPSGEINE_PROC(a.request_id);
            update xxwc.XXWC_GRID_CONCURRENT_TBL set status_flag = 'Y' where request_id = a.request_id;
            commit;
          end if;
        end loop;
      end if;
    end loop;
   /* begin
      delete from xxwc.XXWC_GRID_CONCURRENT_TBL;
      commit;
    end;*/
  exception
    when others then
      null;
  end XXWC_BSA_GRID_CONCURRENT;
  PROCEDURE XXWC_BSA_CONCURRENT_WARNING IS
      /*************************************************************************
    $Header xxwc_opsgeine_pkg $
    Module Name: XXWC_BSA_GRID_CONCURRENT
  
    PURPOSE:   check the error concurrent program
  
    REVISIONS:
    Ver        Date        Author                     Description
    ---------  ----------  ---------------         -------------------------
    1.0        03/16/2018  Nancy Pahwa          Initial Version TMS#20180319-00169
  
  **************************************************************************/
    l_request_id  xxwc.XXWC_GRID_CONCURRENT_TBL.request_id%type;
    l_status_flag xxwc.XXWC_GRID_CONCURRENT_TBL.status_flag%type;
  begin
    for i in (SELECT request_id
                FROM apps.FND_CONC_REQ_SUMMARY_V a
               WHERE 1 = 1
                 AND a.phase_code = 'C'
                 AND a.status_code IN ('G')
                 AND a.ACTUAL_COMPLETION_DATE > sysdate - 5 / (24 * 60)
              --  and rownum < 2
              ) loop
      begin
        select request_id, status_flag
          into l_request_id, l_status_flag
          from xxwc.XXWC_GRID_CONCURRENT_TBL
         where request_id = i.request_id
         and status = 'G';
      exception
        when no_data_found then
          l_status_flag := 'P';
      end;
      if l_status_flag = 'P' then
        insert into xxwc.XXWC_GRID_CONCURRENT_TBL
          (request_id, status_flag, status)
        values
          (i.request_id, 'N','G');
        commit;
        l_status_flag := 'N';
      end if;
      if l_status_flag = 'N' then
        for a in (select request_id, status_flag
                    from xxwc.XXWC_GRID_CONCURRENT_TBL where status = 'G' and request_id = i.request_id ) loop
          if a.status_flag = 'N' then
            apps.xxwc_opsgeine_pkg.XXWC_OPSGEINE_PROC(a.request_id);
            update xxwc.XXWC_GRID_CONCURRENT_TBL set status_flag = 'Y' where request_id = a.request_id;
            commit;
          end if;
        end loop;
      end if;
    end loop;
   /* begin
      delete from xxwc.XXWC_GRID_CONCURRENT_TBL;
      commit;
    end;*/
  exception
    when others then
      null;
  end XXWC_BSA_CONCURRENT_WARNING;
  /*PROCEDURE XXWC_BSA_CONCURRENT_WARNING IS
  begin
    delete from xxwc.XXWC_GRID_CONCURRENT_TBL;
    commit;
    for i in (SELECT request_id
                FROM apps.FND_CONC_REQ_SUMMARY_V a
               WHERE 1 = 1
                 AND a.phase_code = 'C'
                 AND a.status_code IN ('G')
                 AND a.ACTUAL_COMPLETION_DATE > sysdate - 5 / (24 * 60)
              --  and rownum < 2
              ) loop
    
      insert into xxwc.XXWC_GRID_CONCURRENT_TBL
        (request_id)
      values
        (i.request_id);
      commit;
      for a in (select request_id from xxwc.XXWC_GRID_CONCURRENT_TBL) loop
        apps.xxwc_opsgeine_pkg.XXWC_OPSGEINE_PROC(a.request_id);
      end loop;
    
    end loop;
  end XXWC_BSA_CONCURRENT_WARNING;*/
end XXWC_OPSGEINE_pkg;
/