  /****************************************************************************************************
  Table: XXWC_UMT_SKU_BRANCH_DISP_TBL.sql   	
  Description: SKU Branch Disposition 
  HISTORY
  =====================================================================================================
  VERSION DATE               AUTHOR(S)       DESCRIPTION
  ------- -----------------  --------------- ----------------------------------------------------------
  1.0     11-Feb-2016        P.Vamshidhar    Initial version TMS#20160209-00194
                                             Reporting Automation � Disposition   
  *****************************************************************************************************/

DROP TABLE XXWC.XXWC_UMT_SKU_BRANCH_DISP_TBL;

CREATE TABLE XXWC.XXWC_UMT_SKU_BRANCH_DISP_TBL
(
  ID                     NUMBER,
  SKU#                   VARCHAR2(30 BYTE),
  BRANCH#                VARCHAR2(30 BYTE),
  SUGGESTED_DISPOSITION  VARCHAR2(100 BYTE),
  FINAL_DISPOSITION      VARCHAR2(200 BYTE),
  DISPOSITION_ACTION     VARCHAR2(200 BYTE),
  WAS_PRICE              VARCHAR2(20 BYTE),
  CLEARANCE_PRICE        VARCHAR2(20 BYTE),
  CREATED_BY             VARCHAR2(20 BYTE),
  CREATEION_DATE         DATE                   DEFAULT SYSDATE
);

GRANT ALTER, DELETE, INDEX, INSERT, REFERENCES, SELECT, UPDATE, ON COMMIT REFRESH, QUERY REWRITE, DEBUG, FLASHBACK ON XXWC.XXWC_UMT_SKU_BRANCH_DISP_TBL TO INTERFACE_APEXWC;
/
GRANT ALTER, DELETE, INDEX, INSERT, REFERENCES, SELECT, UPDATE, ON COMMIT REFRESH, QUERY REWRITE, DEBUG, FLASHBACK ON XXWC.XXWC_UMT_SKU_BRANCH_DISP_TBL TO INTERFACE_DSTAGE;
/ 

