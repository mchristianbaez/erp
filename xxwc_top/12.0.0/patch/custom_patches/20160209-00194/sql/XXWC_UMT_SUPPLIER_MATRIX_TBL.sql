  /****************************************************************************************************
  Table: XXWC_UMT_SUPPLIER_MATRIX_TBL.sql  	
  Description: Supplier Matrix information.
  HISTORY
  =====================================================================================================
  VERSION DATE               AUTHOR(S)       DESCRIPTION
  ------- -----------------  --------------- ----------------------------------------------------------
  1.0     11-Feb-2016        P.Vamshidhar    Initial version TMS#20160209-00194
                                             Reporting Automation � Disposition   
  *****************************************************************************************************/

DROP TABLE XXWC.XXWC_UMT_SUPPLIER_MATRIX_TBL;

CREATE TABLE XXWC.XXWC_UMT_SUPPLIER_MATRIX_TBL
(
  ID                NUMBER,
  INITIATIVE        VARCHAR2(1000 BYTE),
  SKU_CAT_DESC      VARCHAR2(100 BYTE),
  SKU_SUB_CAT_DESC  VARCHAR2(1000 BYTE),
  PRODUCT_GROUP     VARCHAR2(100 BYTE),
  SUPPLIER_NUM      VARCHAR2(30 BYTE),
  SUPPLIER_NAME     VARCHAR2(100 BYTE),
  SUPPLIER_FLAG     VARCHAR2(1 BYTE),
  CREATED_BY        VARCHAR2(30 BYTE),
  CREATION_DATE     DATE                        DEFAULT SYSDATE
);


GRANT ALTER, DELETE, INDEX, INSERT, REFERENCES, SELECT, UPDATE, ON COMMIT REFRESH, QUERY REWRITE, DEBUG, FLASHBACK ON XXWC.XXWC_UMT_SUPPLIER_MATRIX_TBL TO INTERFACE_APEXWC;
/
GRANT ALTER, DELETE, INDEX, INSERT, REFERENCES, SELECT, UPDATE, ON COMMIT REFRESH, QUERY REWRITE, DEBUG, FLASHBACK ON XXWC.XXWC_UMT_SUPPLIER_MATRIX_TBL TO INTERFACE_DSTAGE;
/