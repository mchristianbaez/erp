  /****************************************************************************************************
  Table: XXWC_UMT_MARKETS_TBL	
  Description: To Maintain marketS information
  HISTORY
  File Name: XXWC_UMT_MARKETS_TBL.sql
  =====================================================================================================
  VERSION DATE               AUTHOR(S)       DESCRIPTION
  ------- -----------------  --------------- ----------------------------------------------------------
  1.0     11-Feb-2016        P.Vamshidhar    Initial version TMS#20160209-00194
                                             Reporting Automation � Disposition   
  *****************************************************************************************************/

DROP TABLE XXWC.XXWC_UMT_MARKETS_TBL;

CREATE TABLE XXWC.XXWC_UMT_MARKETS_TBL
(
  MARKET_ID      NUMBER,
  MARKET_NAME    VARCHAR2(100 BYTE),
  CREATED_BY     VARCHAR2(20 BYTE),
  CREATION_DATE  DATE                           DEFAULT SYSDATE
);

GRANT ALTER, DELETE, INDEX, INSERT, REFERENCES, SELECT, UPDATE, ON COMMIT REFRESH, QUERY REWRITE, DEBUG, FLASHBACK ON XXWC.XXWC_UMT_MARKETS_TBL TO INTERFACE_APEXWC;
/
GRANT ALTER, DELETE, INDEX, INSERT, REFERENCES, SELECT, UPDATE, ON COMMIT REFRESH, QUERY REWRITE, DEBUG, FLASHBACK ON XXWC.XXWC_UMT_MARKETS_TBL TO INTERFACE_DSTAGE;
/