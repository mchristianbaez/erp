/*************************************************************************
  $Header TMS_20161017-00223_Datafix_script_for_old_records.sql $
  Module Name: TMS#20161017-00223  --Data fix Script 

  PURPOSE: Data fix script for OM 1 year old records.

  REVISIONS:
  Ver        Date         Author                Description
  ---------  -----------  ------------------    --------------------------
  1.0        09-NOV-2016 Pattabhi Avula         TMS#20161017-00223 

**************************************************************************/ 
SET SERVEROUTPUT ON SIZE 100000;
SET VERIFY OFF;
DECLARE
      e_user_exception         EXCEPTION;
      l_err_msg                VARCHAR2(2000);
      l_err_code               VARCHAR2(200);
	  l_sec                    VARCHAR2(150);
	  l_delivery_detail_id     NUMBER;
	  l_updated_rowcount       NUMBER;
	  l_org_id                 NUMBER;
	  l_return_status          BOOLEAN;
	  l_locked_line            VARCHAR2(2);


--ITS_DELIVERY_CLOSED
 CURSOR cur_its_del_closed
	  IS
	     SELECT ool.header_id,ool.line_id,ool.flow_status_code,ool.rowid line_rowid
          FROM  apps.oe_order_lines_all ool
          WHERE  1=1
		  AND TRUNC(ool.last_update_date) >= '01-JAN-2015' --TO_DATE(p_exc_date,'DD-MON-YY')
          AND ool.ordered_quantity = 0
          AND ool.flow_status_code IN ('CANCELLED', 'AWAITING_SHIPPING','PICKED')
		  AND EXISTS(SELECT 1
                     FROM apps.wsh_delivery_Details wdd
                     WHERE wdd.source_line_id = ool.line_id
					 AND   wdd.released_status = 'C');
					 
--ITS_DELIVERY_CANCELED			   
 CURSOR cur_its_del_canceled
	  IS
	     SELECT ool.header_id,ool.line_id,ool.flow_status_code,ool.rowid line_rowid
          FROM apps.oe_order_lines_all ool
          WHERE  1=1
		  AND TRUNC(ool.last_update_date)  >= '01-JAN-2015' -- TO_DATE(p_exc_date,'DD-MON-YY')
          AND ool.ordered_quantity = 0
          AND ool.flow_status_code IN ('AWAITING_SHIPPING')
		  AND EXISTS(SELECT 1
                     FROM apps.wsh_delivery_Details wdd
                     WHERE wdd.source_line_id = ool.line_id
					 AND   wdd.released_status = 'D');
					 
--ITS_INTERFACED_FLAG
 CURSOR cur_interfaced_flag
	  IS
	     SELECT ool.header_id,ool.line_id,ool.flow_status_code,ool.rowid line_rowid
         FROM  apps.oe_order_lines_all ool
         WHERE 1=1
         AND   TRUNC(ool.last_update_date)  >= '01-JAN-2015' --= TO_DATE(p_exc_date,'DD-MON-YY')
         AND   ool.flow_status_code NOT IN ('AWAITING_SHIPPING',
                                            'BOOKED',
                                            'SHIPPED',
											'CANCELLED')
         AND   EXISTS(SELECT 1 FROM apps.wsh_delivery_Details wdd
                      WHERE wdd.source_line_id   = ool.line_id
                      AND wdd.oe_interfaced_flag = 'N'
                      AND wdd.released_status    = 'C'
                     );
 --DROP_SHIP_LINE_FIX
      CURSOR cur_drop_ship_line_fix
	  IS
	      SELECT ool.header_id,ool.line_id,ool.rowid line_rowid
          FROM apps.oe_order_lines_all ool
          WHERE 1 = 1
          AND   TRUNC(ool.last_update_date)  >= '01-JAN-2015' -- = TO_DATE(p_exc_date,'DD-MON-YY')
          AND   ool.flow_status_code = 'BOOKED'
          AND   EXISTS(SELECT 1
                    FROM rcv_transactions rcv,
                         apps.po_line_locations_all poll,
                         oe_drop_ship_sources oed
                   WHERE    ool.source_type_code = 'EXTERNAL'
                         AND NVL (ool.shipped_quantity, 0) = 0
                         AND oed.line_id = ool.line_id
                         AND oed.line_location_id IS NOT NULL
                         AND poll.line_location_id = oed.line_location_id
                         AND rcv.po_line_location_id = poll.line_location_id
                         AND rcv.TRANSACTION_TYPE = 'DELIVER');

      CURSOR cur_pending_receipts(p_order_line_id NUMBER)
      IS
         SELECT rcv.transaction_id
           FROM rcv_transactions rcv,
                po_line_locations_all poll,
                oe_drop_ship_sources oed,
                oe_order_lines_all oel
          WHERE     oel.source_type_code = 'EXTERNAL'
                AND NVL (oel.shipped_quantity, 0) = 0
                AND oed.line_id = oel.line_id
                AND oed.line_location_id IS NOT NULL
                AND poll.line_location_id = oed.line_location_id
                AND rcv.po_line_location_id = poll.line_location_id
                AND oel.line_id = p_order_line_id
                AND rcv.TRANSACTION_TYPE = 'DELIVER';
		  
      --AWAITING_INVOICE_FIX
	  CURSOR cur_awaiting_invoice_fix
      IS
	  SELECT ool.header_id,ool.line_id,ool.rowid line_rowid
      FROM   apps.oe_order_lines_all ool,apps.oe_order_headers_all ooh
      WHERE  1 = 1
      AND    ool.header_id = ooh.header_id
      AND   TRUNC(ool.last_update_date)  >= '01-JAN-2015' -- >= TO_DATE(p_exc_date,'DD-MON-YY')
      AND   ool.flow_status_code = 'INVOICE_HOLD'
      AND   EXISTS(SELECT  /*+ INDEX(rctl XXWC_RA_CUST_TRX_LINES_N13)*/ 1
                   FROM apps.ra_customer_trx_lines_all rctl
                   WHERE rctl.interface_line_attribute6 = to_char(ool.line_id)
                   AND   rctl.interface_line_attribute1 = to_char(ooh.order_number)
                  );
BEGIN
 --ITS_DELIVERY_CLOSED
  	  FOR rec_its_del_closed IN cur_its_del_closed
	  LOOP
	     BEGIN
            dbms_output.put_line('Executing the script for DELIVERY_CLOSED');
			l_locked_line := xxwc_om_force_ship_pkg.is_row_locked (rec_its_del_closed.line_rowid, 'OE_ORDER_LINES_ALL');
			IF l_locked_line = 'N' THEN
	           BEGIN			     
	              SELECT delivery_detail_id INTO l_delivery_detail_id
                    FROM wsh_delivery_details wdd
                   WHERE wdd.source_line_id = rec_its_del_closed.line_id;
		       EXCEPTION
		          WHEN NO_DATA_FOUND THEN
		  	         l_err_msg := l_sec||' - '||'Delivery not found for line id:'||rec_its_del_closed.line_id;
			   	     RAISE e_user_exception;
				  WHEN TOO_MANY_ROWS THEN
				     l_err_msg := l_sec||' - '||'More than one delivery id found for line id:'||rec_its_del_closed.line_id;
			   	     RAISE e_user_exception;
		       END;
			   l_updated_rowcount := 0;
			   l_sec := 'UPDATE apps.wsh_delivery_details';
               UPDATE apps.wsh_delivery_details
                  SET released_status = 'D',
                      src_requested_quantity = 0,
                      requested_quantity = 0,
                      shipped_quantity = 0,
                      cycle_count_quantity = 0,
                      cancelled_quantity = 0,
                      subinventory = NULL,
                      locator_id = NULL,
                      lot_number = NULL,
                      revision = NULL,
                      inv_interfaced_flag = 'X',
                      oe_interfaced_flag = 'X'
                WHERE delivery_detail_id = l_delivery_detail_id;
			    l_updated_rowcount := SQL%ROWCOUNT;
				
			    dbms_output.put_line('wsh_delivery_details records updated count is :'||l_updated_rowcount);
				
			    IF l_updated_rowcount > 0 THEN
			       l_updated_rowcount := 0;
			       l_sec := 'UPDATE apps.wsh_delivery_assignments';
				   
			       UPDATE apps.wsh_delivery_assignments
                     SET delivery_id = NULL, parent_delivery_detail_id = NULL
                   WHERE delivery_detail_id = l_delivery_detail_id;
			       l_updated_rowcount := SQL%ROWCOUNT;
                END IF;
				
				dbms_output.put_line('wsh_delivery_assignments records updated count is :'||l_updated_rowcount);
               
                IF rec_its_del_closed.flow_status_code = 'AWAITING_SHIPPING' AND l_updated_rowcount > 0 THEN
                   l_sec := 'UPDATE apps.oe_order_lines_all';
                   UPDATE apps.oe_order_lines_all
                    SET flow_status_code = 'CANCELLED', cancelled_flag = 'Y'
                    WHERE line_id = rec_its_del_closed.line_id
			   	 AND header_id = rec_its_del_closed.header_id
                    AND ordered_quantity = 0
                    AND flow_status_code <> 'CANCELLED';
			   	   l_updated_rowcount := SQL%ROWCOUNT;
			    END IF;
				dbms_output.put_line('oe_order_lines_all records updated count is :'||l_updated_rowcount);
			    
			    COMMIT;
            END IF;
         EXCEPTION
		    WHEN e_user_exception THEN
		       ROLLBACK;
			    DBMS_OUTPUT.PUT_LINE('Error details are : '||l_err_msg);
		    WHEN OTHERS THEN
			   ROLLBACK;
			   l_err_msg := SUBSTR(SQLERRM,1,2000);
			  DBMS_OUTPUT.PUT_LINE('Error details are : '||l_err_msg);
		 END;
	  END LOOP; -- 1st cursor closed
	  
	   ---------------------------------------------------------------------------------------------------------------------------
	  --1.2	Line has been cancelled, however the delivery has been created and trying to ship for ZERO quantity, line status in 
	  --    Picked/Cancelled status (Interface trip stops completes warning) - ITS_DELIVERY_CANCELED
	  ---------------------------------------------------------------------------------------------------------------------------
		
        DBMS_OUTPUT.PUT_LINE('Cursor executiong for  ITS_DELIVERY_CANCELED script');	  
	  FOR rec_its_del_canceled IN cur_its_del_canceled
	  LOOP
	     BEGIN

		    l_locked_line := xxwc_om_force_ship_pkg.is_row_locked (rec_its_del_canceled.line_rowid, 'OE_ORDER_LINES_ALL');
			IF l_locked_line = 'N' THEN
		       
               UPDATE apps.oe_order_lines_all
               SET flow_status_code = 'CANCELLED', cancelled_flag = 'Y'
               WHERE line_id = rec_its_del_canceled.line_id
               AND ordered_quantity = 0
               AND flow_status_code <> 'CANCELLED';
			   l_updated_rowcount := SQL%ROWCOUNT;			   
			  COMMIT;
            END IF;
         EXCEPTION
		    WHEN OTHERS THEN
			   ROLLBACK;
			   l_err_msg := SUBSTR(SQLERRM,1,2000);
			   DBMS_OUTPUT.PUT_LINE('Error details are :'||l_err_msg);	 
			   
		 END;
	  END LOOP;
	  
	  ------------------------------------------------------------------------------------------------------------------------------
	  --2.	Line stuck in Picked status and the associated delivery detailed id has the OE_INTERFACED_FLAG set to N 
	  --    (Interface trip stops completes warning) (ITS_INTERFACED_FLAG)
	  ------------------------------------------------------------------------------------------------------------------------------

	  DBMS_OUTPUT.PUT_LINE('Cursor executing for ITS_INTERFACED_FLAG');

	  FOR rec_interfaced_flag IN cur_interfaced_flag
	  LOOP
	     BEGIN
		    l_locked_line := xxwc_om_force_ship_pkg.is_row_locked (rec_interfaced_flag.line_rowid, 'OE_ORDER_LINES_ALL');
			IF l_locked_line = 'N' THEN
		       
			   l_updated_rowcount := 0;
				 
               UPDATE apps.wsh_delivery_Details
               SET oe_interfaced_flag = 'Y'
               WHERE source_line_id     = rec_interfaced_flag.line_id
               AND oe_interfaced_flag = 'N'
               AND released_status    = 'C';
               l_updated_rowcount := SQL%ROWCOUNT;
			   
			   COMMIT;
            END IF;
         EXCEPTION
		    WHEN OTHERS THEN
		       ROLLBACK;
			   l_err_msg := SUBSTR(SQLERRM,1,2000);
			   DBMS_OUTPUT.PUT_LINE('Error details are :'||l_err_msg);	
		 END;
      END LOOP;
	  
	   ---------------------------------------------------------------------------------------------------------------------------
       --3. Line has been received, however the associated sales order line has been stuck in BOOKED status - DROP_SHIP_LINE_FIX
      ---------------------------------------------------------------------------------------------------------------------------
	   DBMS_OUTPUT.PUT_LINE('Cursor executing for DROP_SHIP_LINE_FIX');


	  FOR rec_drop_ship_line_fix IN cur_drop_ship_line_fix
	  LOOP
	     BEGIN
		    l_locked_line := xxwc_om_force_ship_pkg.is_row_locked (rec_drop_ship_line_fix.line_rowid, 'OE_ORDER_LINES_ALL');
			IF l_locked_line = 'N' THEN

               UPDATE oe_order_lines_all oeol
                  SET oeol.flow_status_code = 'AWAITING_RECEIPT'
                WHERE     oeol.line_id = rec_drop_ship_line_fix.line_id
                  AND oeol.flow_status_code <> 'AWAITING_RECEIPT';
			   
			   l_updated_rowcount := SQL%ROWCOUNT; 
			   
			   IF l_updated_rowcount > 0 THEN
                 
			      FOR all_lines IN cur_pending_receipts(rec_drop_ship_line_fix.line_id)
                  LOOP
                     l_return_status := OE_DS_PVT.DROPSHIPRECEIVE (all_lines.transaction_id, 'INV');
                  END LOOP;
			   END IF;
			   IF  l_return_status = TRUE THEN
			       
			      DBMS_OUTPUT.PUT_LINE('update_record_status - Success');
               ELSE
			      DBMS_OUTPUT.PUT_LINE('update_record_status - Error is '||SQLERRM);
			   END IF;
               COMMIT;
            END IF;
         EXCEPTION
		    WHEN OTHERS THEN
		       ROLLBACK;
			   l_err_msg := SUBSTR(SQLERRM,1,2000);
			   DBMS_OUTPUT.PUT_LINE('Exception details is '||l_err_msg);
		 END;      
      END LOOP;
	  
	   ------------------------------------------------------------------------------------------------------------------------
	  --4.	Line is shipped, invoiced and the workflow is closed, however the line status is not closed - AWAITING_INVOICE_FIX
	  ------------------------------------------------------------------------------------------------------------------------

	   DBMS_OUTPUT.PUT_LINE('Cursor executing for AWAITING_INVOICE_FIX script');

	  FOR rec_awaiting_invoice_fix IN cur_awaiting_invoice_fix
	  LOOP
	     BEGIN

		    l_locked_line := xxwc_om_force_ship_pkg.is_row_locked (rec_awaiting_invoice_fix.line_rowid, 'OE_ORDER_LINES_ALL');
			IF l_locked_line = 'N' THEN
		                      
			   l_updated_rowcount := 0;

               UPDATE apps.oe_order_lines_all ool
                  SET ool.invoice_interface_status_code = 'YES',
                      ool.open_flag = 'N',
                      ool.flow_status_code = 'CLOSED',
                      ool.invoiced_quantity = ool.shipped_quantity
                WHERE     line_id = rec_awaiting_invoice_fix.line_id
                AND ool.flow_status_code = 'INVOICE_HOLD';
			   
			   l_updated_rowcount := SQL%ROWCOUNT;
			    DBMS_OUTPUT.PUT_LINE('Records updated for AWAITING_INVOICE_FIX script is '||l_updated_rowcount);

			   COMMIT;
            END IF;
         EXCEPTION
		    WHEN OTHERS THEN
		       ROLLBACK;
			  DBMS_OUTPUT.PUT_LINE('Exception details is '||SQLERRM);
		END;	  
      END LOOP;
	  
   EXCEPTION -- Main block exception
       WHEN OTHERS THEN
       DBMS_OUTPUT.PUT_LINE('Error details are : '||SQLERRM);
	   ROLLBACK;
       
   END;
/