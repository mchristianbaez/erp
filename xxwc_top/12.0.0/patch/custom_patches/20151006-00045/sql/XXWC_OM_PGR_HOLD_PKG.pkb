CREATE OR REPLACE PACKAGE BODY APPS.XXWC_OM_PGR_HOLD_PKG
IS
   /*************************************************************************
   *   $Header XXXWC_OM_PGR_HOLD_PKG.pkb $
   *   Module Name: XXWC_OM_PGR_HOLD_PKG.pkb
   *
   *   PURPOSE:   This package is used for Invoice Pre Processing
   *
   *   REVISIONS:
   *   Ver        Date         Author                Description
   *   ---------  ----------   ---------------       -------------------------
   *   1.0        4-Nov-2015   Manjula Chellappan    Initial Version
   *                                                  TMS# 20151006-00045 Pricing guard rail project (Removal of pricing guard rail holds)
   * ***************************************************************************/

   PROCEDURE Apply_hold_program (errbuf           OUT VARCHAR2,
                                 retcode          OUT VARCHAR2,
                                 p_from_date   IN     VARCHAR2,
                                 p_emailid     IN     VARCHAR2,
                                 p_filename    IN     VARCHAR2)
   IS
      /* **************************************************************************
         *   Procedure Name: Apply_hold_program
         *
         *   PURPOSE:   This procedure called from a conc prog to Preprocess the invoice transactions
         *
         *   REVISIONS:
         *   Ver        Date          Author               Description
         *   ---------  ----------    ---------------      -------------------------
         *   1.0        4-Nov-2015   Manjula Chellappan    Initial Version
         *                                                  TMS# 20151006-00045 Pricing guard rail project (Removal of pricing guard rail holds)
         * ***************************************************************************/

      l_sec                  VARCHAR2 (100);

      l_pgr_order_amount     NUMBER
         := fnd_profile.VALUE ('XXWC GUARD RAIL ORDER AMOUNT');
      l_pgr_extended_price   NUMBER
         := fnd_profile.VALUE ('XXWC GUARD RAIL EXTENDED PRICE');
      l_pgr_min_margin       NUMBER
         := fnd_profile.VALUE ('XXWC GUARD RAIL MIN MARGIN');
      l_pgr_max_margin       NUMBER
         := fnd_profile.VALUE ('XXWC GUARD RAIL MAX MARGIN');

      l_apply_hold_flag      NUMBER := 0;
      l_hold                 VARCHAR2 (50)
                                := fnd_profile.VALUE ('XXWC_OM_PG_HOLD');
      l_header_id            NUMBER := 0;
      l_prev_order_amount    NUMBER := 0;

      l_hold_source_rec      OE_HOLDS_PVT.HOLD_SOURCE_REC_TYPE;
      l_return_status        VARCHAR2 (30);
      l_msg_data             VARCHAR2 (4);
      l_msg_count            NUMBER;
      l_error_msg            VARCHAR2 (4000);

      l_order_amount         NUMBER;
      l_rec_count            NUMBER;

      l_add_layout           BOOLEAN;
      l_program_name         VARCHAR2 (30) := 'XXWC_OM_PGR_ERR_REP';
      l_request_id           NUMBER;
      l_exception            EXCEPTION;


      CURSOR cur_order_lines
      IS
           SELECT header_id,
                  line_id,
                     line_number
                  || DECODE (shipment_number, NULL, NULL, '.')
                  || shipment_number
                     line_number,
                  inventory_item_id,
                  ship_from_org_id,
                  org_id,
                  ordered_item,
                  ordered_quantity,
                  unit_selling_price,
                  unit_cost,
                  NVL (ordered_quantity, 0) * NVL (unit_selling_price, 0)
                     extended_price,
                  (SELECT order_number
                     FROM OE_ORDER_HEADERS
                    WHERE header_id = ool.header_id)
                     order_number,
                  DECODE (
                     unit_cost,
                     NULL, NULL,
                     (DECODE (
                         unit_selling_price,
                         NULL, NULL,
                         0, NULL,
                         ROUND (
                              (  (unit_selling_price - unit_cost)
                               / unit_selling_price)
                            * 100,
                            2))))
                     margin,
                  flow_status_code,
                  user_item_description,
                  created_by,
                  last_updated_by,
                  last_update_login,
                  creation_date,
                  last_update_date,
                  fulfillment_date,
                  actual_shipment_date
             FROM oe_order_lines ool
            WHERE     1 = 1
                  AND last_update_date >=
                         TO_DATE (p_from_date, 'YYYY/MM/DD HH24:MI:SS')
                  AND flow_status_code NOT IN ('CLOSED', 'ENTERED', 'CANCELLED')
                  AND line_category_code = 'ORDER'
                  AND line_type_id IN (1002, 1005) -- Standard Line and Counter Line
                  AND NOT EXISTS
                             (SELECT 'x'
                                FROM apps.oe_order_holds ooh,
                                     apps.oe_hold_sources ohs
                               WHERE     ooh.Header_Id = ool.header_id
                                     AND ooh.Line_ID = ool.line_id
                                     AND ohs.Hold_Id = TO_NUMBER (l_hold)
                                     AND ooh.hold_source_ID =
                                            ohs.Hold_Source_id)
         ORDER BY 1, 2;
   BEGIN
      EXECUTE IMMEDIATE 'TRUNCATE TABLE XXWC.XXWC_OM_PGR_HOLD_ERR_TBL ';
	  
            fnd_file.put_line (
               fnd_file.LOG,
               'p_date : ' || TO_DATE (p_from_date, 'YYYY/MM/DD HH24:MI:SS'));
            
            fnd_file.put_line (fnd_file.LOG,
                               'l_pgr_order_amount ' || l_pgr_order_amount);
            fnd_file.put_line (
               fnd_file.LOG,
               'l_pgr_extended_price ' || l_pgr_extended_price);
            fnd_file.put_line (fnd_file.LOG,
                               'l_pgr_min_margin ' || l_pgr_min_margin);
            fnd_file.put_line (fnd_file.LOG,
                               'l_pgr_max_margin ' || l_pgr_max_margin);	  

      FOR rec_order_lines IN cur_order_lines
      LOOP
         IF rec_order_lines.margin IS NOT NULL
         THEN
            IF l_header_id <> rec_order_lines.header_id
            THEN
               BEGIN
                  l_order_amount := 0;

                  SELECT SUM (
                              NVL (ordered_quantity, 0)
                            * NVL (unit_selling_price, 0))
                    INTO l_order_amount
                    FROM oe_order_lines
                   WHERE     header_id = rec_order_lines.header_id
                         AND NVL (cancelled_flag, 'N') = 'N';
               EXCEPTION
                  WHEN OTHERS
                  THEN
                     l_order_amount := 0;
               END;
            END IF;


            l_hold_source_rec := OE_HOLDS_PVT.G_MISS_HOLD_SOURCE_REC;
            l_hold_source_rec.hold_id := l_hold;
            l_hold_source_rec.hold_entity_code := 'O';
            l_hold_source_rec.hold_entity_id := rec_order_lines.header_id;
            l_hold_source_rec.header_id := rec_order_lines.header_id;
            l_hold_source_rec.line_id := NULL;
            l_hold_source_rec.hold_until_date := NULL;
            l_return_status := NULL;
            l_msg_data := NULL;
            l_msg_count := NULL;

            l_sec :=
                  'Check if Order Amount more than PGR Order Amount for Order'
               || rec_order_lines.order_number;

            IF     l_prev_order_amount <> l_order_amount
               AND l_order_amount >= l_pgr_order_amount
            THEN
               l_sec :=
                     'Apply Order Header Hold for Order Amount > PGR Order Amount'
                  || rec_order_lines.order_number;

               OE_Holds_PUB.Apply_Holds (
                  p_api_version       => 1.0,
                  p_init_msg_list     => FND_API.G_FALSE,
                  p_commit            => FND_API.G_TRUE,
                  p_hold_source_rec   => l_hold_source_rec,
                  x_return_status     => l_return_status,
                  x_msg_count         => l_msg_count,
                  x_msg_data          => l_msg_data);

               IF l_return_status = FND_API.G_RET_STS_SUCCESS
               THEN
                  COMMIT;

                  fnd_file.put_line (
                     fnd_file.output,
                        'Hold applied on Sales Order : '
                     || rec_order_lines.order_number);
               ELSE
                  l_error_msg := l_msg_data;

                  IF NVL (l_msg_count, 0) > 0
                  THEN
                     FOR i IN 1 .. l_msg_count
                     LOOP
                        l_error_msg :=
                              l_error_msg
                           || ' - '
                           || oe_msg_pub.get (p_msg_index   => i,
                                              p_encoded     => 'F');
                     END LOOP;
                  END IF;

                  ROLLBACK;

                  fnd_file.put_line (
                     fnd_file.output,
                        'Hold not applied on Sales Order : '
                     || rec_order_lines.order_number);

                  INSERT
                    INTO XXWC.XXWC_OM_PGR_HOLD_ERR_TBL (LINE_ID,
                                                        LINE_NUMBER,
                                                        HEADER_ID,
                                                        ORDER_NUMBER,
                                                        ORG_ID,
                                                        INVENTORY_ITEM_ID,
                                                        ORDERED_ITEM,
                                                        LINE_FLOW_STATUS_CODE,
                                                        ORDERED_QUANTITY,
                                                        UNIT_SELLING_PRICE,
                                                        EXTENDED_PRICE,
                                                        MARGIN,
                                                        USER_ITEM_DESCRIPTION,
                                                        HOLD_STATUS,
                                                        ERROR,
                                                        CREATED_BY,
                                                        LAST_UPDATED_BY,
                                                        CREATION_DATE,
                                                        LAST_UPDATE_DATE,
                                                        LAST_UPDATE_LOGIN)
                  VALUES (rec_order_lines.LINE_ID,
                          rec_order_lines.LINE_NUMBER,
                          rec_order_lines.HEADER_ID,
                          rec_order_lines.ORDER_NUMBER,
                          rec_order_lines.ORG_ID,
                          rec_order_lines.INVENTORY_ITEM_ID,
                          rec_order_lines.ORDERED_ITEM,
                          rec_order_lines.FLOW_STATUS_CODE,
                          rec_order_lines.ORDERED_QUANTITY,
                          rec_order_lines.UNIT_SELLING_PRICE,
                          rec_order_lines.EXTENDED_PRICE,
                          rec_order_lines.MARGIN,
                          rec_order_lines.USER_ITEM_DESCRIPTION,
                          'Error',
                          l_error_msg,
                          rec_order_lines.CREATED_BY,
                          rec_order_lines.LAST_UPDATED_BY,
                          rec_order_lines.CREATION_DATE,
                          rec_order_lines.LAST_UPDATE_DATE,
                          rec_order_lines.LAST_UPDATE_LOGIN);
               END IF;
            ELSE
               l_sec :=
                     'Check if margin is erraneous for Line_id '
                  || rec_order_lines.line_id;

               IF (    rec_order_lines.extended_price > l_pgr_extended_price
                   AND (   rec_order_lines.margin <= l_pgr_min_margin
                        OR rec_order_lines.margin >= l_pgr_max_margin))
               THEN
                  IF l_header_id <> rec_order_lines.header_id
                  THEN
                     l_sec :=
                           'Apply Hold for Order Number '
                        || rec_order_lines.Order_number;

                     OE_Holds_PUB.Apply_Holds (
                        p_api_version       => 1.0,
                        p_init_msg_list     => FND_API.G_FALSE,
                        p_commit            => FND_API.G_TRUE,
                        p_hold_source_rec   => l_hold_source_rec,
                        x_return_status     => l_return_status,
                        x_msg_count         => l_msg_count,
                        x_msg_data          => l_msg_data);


                     IF l_return_status = FND_API.G_RET_STS_SUCCESS
                     THEN
                        COMMIT;

                        fnd_file.put_line (
                           fnd_file.output,
                              'Hold applied on Sales Order : '
                           || rec_order_lines.order_number);
                     ELSE
                        l_error_msg := l_msg_data;

                        IF NVL (l_msg_count, 0) > 0
                        THEN
                           FOR i IN 1 .. l_msg_count
                           LOOP
                              l_error_msg :=
                                    l_error_msg
                                 || ' - '
                                 || oe_msg_pub.get (p_msg_index   => i,
                                                    p_encoded     => 'F');
                           END LOOP;
                        END IF;

                        ROLLBACK;

                        fnd_file.put_line (
                           fnd_file.output,
                              'Hold not applied on Sales Order : '
                           || rec_order_lines.order_number);

                        INSERT
                          INTO XXWC.XXWC_OM_PGR_HOLD_ERR_TBL (
                                  LINE_ID,
                                  LINE_NUMBER,
                                  HEADER_ID,
                                  ORDER_NUMBER,
                                  ORG_ID,
                                  INVENTORY_ITEM_ID,
                                  ORDERED_ITEM,
                                  LINE_FLOW_STATUS_CODE,
                                  ORDERED_QUANTITY,
                                  UNIT_SELLING_PRICE,
                                  EXTENDED_PRICE,
                                  MARGIN,
                                  USER_ITEM_DESCRIPTION,
                                  HOLD_STATUS,
                                  ERROR,
                                  CREATED_BY,
                                  LAST_UPDATED_BY,
                                  CREATION_DATE,
                                  LAST_UPDATE_DATE,
                                  LAST_UPDATE_LOGIN)
                        VALUES (rec_order_lines.LINE_ID,
                                rec_order_lines.LINE_NUMBER,
                                rec_order_lines.HEADER_ID,
                                rec_order_lines.ORDER_NUMBER,
                                rec_order_lines.ORG_ID,
                                rec_order_lines.INVENTORY_ITEM_ID,
                                rec_order_lines.ORDERED_ITEM,
                                rec_order_lines.FLOW_STATUS_CODE,
                                rec_order_lines.ORDERED_QUANTITY,
                                rec_order_lines.UNIT_SELLING_PRICE,
                                rec_order_lines.EXTENDED_PRICE,
                                rec_order_lines.MARGIN,
                                rec_order_lines.USER_ITEM_DESCRIPTION,
                                'Error',
                                l_error_msg,
                                rec_order_lines.CREATED_BY,
                                rec_order_lines.LAST_UPDATED_BY,
                                rec_order_lines.CREATION_DATE,
                                rec_order_lines.LAST_UPDATE_DATE,
                                rec_order_lines.LAST_UPDATE_LOGIN);
                     END IF;
                  END IF;

                  l_hold_source_rec.line_id := rec_order_lines.line_id;

                  l_sec :=
                        'Apply Line level Hold for Line Id'
                     || rec_order_lines.line_id;

                  l_return_status := NULL;
                  l_msg_count := NULL;
                  l_msg_data := NULL;

                  OE_Holds_PUB.Apply_Holds (
                     p_api_version       => 1.0,
                     p_init_msg_list     => FND_API.G_FALSE,
                     p_commit            => FND_API.G_TRUE,
                     p_hold_source_rec   => l_hold_source_rec,
                     x_return_status     => l_return_status,
                     x_msg_count         => l_msg_count,
                     x_msg_data          => l_msg_data);

                  IF l_return_status = FND_API.G_RET_STS_SUCCESS
                  THEN
                     COMMIT;

                     fnd_file.put_line (
                        fnd_file.output,
                           'Hold applied on Sales Order : '
                        || rec_order_lines.order_number
                        || ' Line Number : '
                        || rec_order_lines.Line_number);
                  ELSE
                     l_error_msg := l_msg_data;

                     IF NVL (l_msg_count, 0) > 0
                     THEN
                        FOR i IN 1 .. l_msg_count
                        LOOP
                           l_error_msg :=
                                 l_error_msg
                              || ' - '
                              || oe_msg_pub.get (p_msg_index   => i,
                                                 p_encoded     => 'F');
                        END LOOP;
                     END IF;

                     ROLLBACK;

                     fnd_file.put_line (
                        fnd_file.output,
                           'Hold not applied on Sales Order : '
                        || rec_order_lines.order_number
                        || ' Line Number : '
                        || rec_order_lines.Line_number);

                     INSERT
                       INTO XXWC.XXWC_OM_PGR_HOLD_ERR_TBL (
                               LINE_ID,
                               LINE_NUMBER,
                               HEADER_ID,
                               ORDER_NUMBER,
                               ORG_ID,
                               INVENTORY_ITEM_ID,
                               ORDERED_ITEM,
                               LINE_FLOW_STATUS_CODE,
                               ORDERED_QUANTITY,
                               UNIT_SELLING_PRICE,
                               EXTENDED_PRICE,
                               MARGIN,
                               USER_ITEM_DESCRIPTION,
                               HOLD_STATUS,
                               CREATED_BY,
                               LAST_UPDATED_BY,
                               CREATION_DATE,
                               LAST_UPDATE_DATE,
                               LAST_UPDATE_LOGIN)
                     VALUES (rec_order_lines.LINE_ID,
                             rec_order_lines.LINE_NUMBER,
                             rec_order_lines.HEADER_ID,
                             rec_order_lines.ORDER_NUMBER,
                             rec_order_lines.ORG_ID,
                             rec_order_lines.INVENTORY_ITEM_ID,
                             rec_order_lines.ORDERED_ITEM,
                             rec_order_lines.FLOW_STATUS_CODE,
                             rec_order_lines.ORDERED_QUANTITY,
                             rec_order_lines.UNIT_SELLING_PRICE,
                             rec_order_lines.EXTENDED_PRICE,
                             rec_order_lines.MARGIN,
                             rec_order_lines.USER_ITEM_DESCRIPTION,
                             'Error',
                             rec_order_lines.CREATED_BY,
                             rec_order_lines.LAST_UPDATED_BY,
                             rec_order_lines.CREATION_DATE,
                             rec_order_lines.LAST_UPDATE_DATE,
                             rec_order_lines.LAST_UPDATE_LOGIN);

                     COMMIT;
                  END IF;
               END IF;
            END IF;

            l_prev_order_amount := l_order_amount;
            l_header_id := rec_order_lines.header_id;
         END IF;
      END LOOP;


      BEGIN
         l_sec := 'Get Error Record Count ';

         SELECT COUNT (*) INTO l_rec_count FROM XXWC.XXWC_OM_PGR_HOLD_ERR_TBL;

         IF l_rec_count > 0
         THEN
            l_sec := 'Add Layout ';

            l_add_layout :=
               fnd_request.add_layout (template_appl_name   => 'XXWC',
                                       template_code        => l_program_name,
                                       template_language    => 'en', --Use language from template definition
                                       template_territory   => 'US', --Use territory from template definition
                                       output_format        => 'EXCEL' --Use output format from template definition
                                                                      );

            IF l_add_layout = TRUE
            THEN
               l_sec := 'Submit Error Report ';

               l_request_id :=
                  fnd_request.submit_request ('XXWC',
                                              l_program_name,
                                              NULL,
                                              NULL,
                                              FALSE,
                                              p_emailid,
                                              p_filename);

               IF l_request_id = 0
               THEN
                  l_error_msg :=
                        l_error_msg
                     || ' '
                     || l_sec
                     || ' Failed to submit the concurrent Program ';
                  RAISE l_exception;
               ELSE
                  COMMIT;
               END IF;
            ELSIF l_add_layout = FALSE
            THEN
               l_error_msg :=
                     l_error_msg
                  || ' '
                  || l_sec
                  || ' Failed to associate the Template to the concurrent Program ';
               RAISE l_exception;
            END IF;
         END IF;
      EXCEPTION
         WHEN OTHERS
         THEN
            NULL;
      END;
   EXCEPTION
      WHEN l_exception
      THEN
         xxcus_error_pkg.xxcus_error_main_api (
            p_called_from         => 'XXWC_OM_PGR_HOLD_PKG.Apply_hold_program',
            p_calling             => l_sec,
            p_request_id          => fnd_global.conc_request_id,
            p_ora_error_msg       => SUBSTR (
                                          DBMS_UTILITY.format_error_stack ()
                                       || DBMS_UTILITY.format_error_backtrace (),
                                       1,
                                       2000),
            p_error_desc          => SUBSTR (l_error_msg, 1, 240),
            p_distribution_list   => g_distro_list,
            p_module              => 'OM');
      WHEN OTHERS
      THEN
         xxcus_error_pkg.xxcus_error_main_api (
            p_called_from         => 'XXWC_OM_PGR_HOLD_PKG.Apply_hold_program',
            p_calling             => l_sec,
            p_request_id          => fnd_global.conc_request_id,
            p_ora_error_msg       => SUBSTR (
                                          DBMS_UTILITY.format_error_stack ()
                                       || DBMS_UTILITY.format_error_backtrace (),
                                       1,
                                       2000),
            p_error_desc          => SUBSTR (l_error_msg, 1, 240),
            p_distribution_list   => g_distro_list,
            p_module              => 'OM');
   END Apply_hold_program;
END XXWC_OM_PGR_HOLD_PKG;
/