/*
 Ticket# 20160627-00032 / ESMS 308989
 Author: Balaguru Seshadri
 Date: 06/30/2016
 Report Name            : HDS Inactive Vendor Listing
 Import Instructions    : Run this file in XXEIS user from SQL*Plus or any tool like Toad, SQL Navigator.   
*/ 
prompt Creating View Data for HDS Inactive Vendor Listing
set scan off define off
DECLARE
BEGIN 
--Inserting View XXCUS_GSC_HSD_AP_SUPPLIERS_V 
xxeis.eis_rs_ins.v( 'XXCUS_GSC_HSD_AP_SUPPLIERS_V ',200,'Paste SQL View for ESMS ticket 308989 Inactive Vendors','1.0','','','BS006141','APPS','View ESMS ticket 308989','X1AV12345','','');
--Delete View Columns for XXCUS_GSC_HSD_AP_SUPPLIERS_V 
xxeis.eis_rs_utility.delete_view_rows('XXCUS_GSC_HSD_AP_SUPPLIERS_V ',200,FALSE);
--Inserting View Columns for XXCUS_GSC_HSD_AP_SUPPLIERS_V 
xxeis.eis_rs_ins.vc( 'XXCUS_GSC_HSD_AP_SUPPLIERS_V ','GLOBAL_ATTRIBUTE15',200,'','','','','','BS006141','VARCHAR2','','','Global Attribute15','','','');
xxeis.eis_rs_ins.vc( 'XXCUS_GSC_HSD_AP_SUPPLIERS_V ','GLOBAL_ATTRIBUTE16',200,'','','','','','BS006141','VARCHAR2','','','Global Attribute16','','','');
xxeis.eis_rs_ins.vc( 'XXCUS_GSC_HSD_AP_SUPPLIERS_V ','GLOBAL_ATTRIBUTE17',200,'','','','','','BS006141','VARCHAR2','','','Global Attribute17','','','');
xxeis.eis_rs_ins.vc( 'XXCUS_GSC_HSD_AP_SUPPLIERS_V ','GLOBAL_ATTRIBUTE18',200,'','','','','','BS006141','VARCHAR2','','','Global Attribute18','','','');
xxeis.eis_rs_ins.vc( 'XXCUS_GSC_HSD_AP_SUPPLIERS_V ','GLOBAL_ATTRIBUTE19',200,'','','','','','BS006141','VARCHAR2','','','Global Attribute19','','','');
xxeis.eis_rs_ins.vc( 'XXCUS_GSC_HSD_AP_SUPPLIERS_V ','GLOBAL_ATTRIBUTE20',200,'','','','','','BS006141','VARCHAR2','','','Global Attribute20','','','');
xxeis.eis_rs_ins.vc( 'XXCUS_GSC_HSD_AP_SUPPLIERS_V ','GLOBAL_ATTRIBUTE_CATEGORY',200,'','','','','','BS006141','VARCHAR2','','','Global Attribute Category','','','');
xxeis.eis_rs_ins.vc( 'XXCUS_GSC_HSD_AP_SUPPLIERS_V ','EDI_TRANSACTION_HANDLING',200,'','','','','','BS006141','VARCHAR2','','','Edi Transaction Handling','','','');
xxeis.eis_rs_ins.vc( 'XXCUS_GSC_HSD_AP_SUPPLIERS_V ','EDI_PAYMENT_METHOD',200,'','','','','','BS006141','VARCHAR2','','','Edi Payment Method','','','');
xxeis.eis_rs_ins.vc( 'XXCUS_GSC_HSD_AP_SUPPLIERS_V ','EDI_PAYMENT_FORMAT',200,'','','','','','BS006141','VARCHAR2','','','Edi Payment Format','','','');
xxeis.eis_rs_ins.vc( 'XXCUS_GSC_HSD_AP_SUPPLIERS_V ','EDI_REMITTANCE_METHOD',200,'','','','','','BS006141','VARCHAR2','','','Edi Remittance Method','','','');
xxeis.eis_rs_ins.vc( 'XXCUS_GSC_HSD_AP_SUPPLIERS_V ','EDI_REMITTANCE_INSTRUCTION',200,'','','','','','BS006141','VARCHAR2','','','Edi Remittance Instruction','','','');
xxeis.eis_rs_ins.vc( 'XXCUS_GSC_HSD_AP_SUPPLIERS_V ','BANK_CHARGE_BEARER',200,'','','','','','BS006141','VARCHAR2','','','Bank Charge Bearer','','','');
xxeis.eis_rs_ins.vc( 'XXCUS_GSC_HSD_AP_SUPPLIERS_V ','BANK_BRANCH_TYPE',200,'','','','','','BS006141','VARCHAR2','','','Bank Branch Type','','','');
xxeis.eis_rs_ins.vc( 'XXCUS_GSC_HSD_AP_SUPPLIERS_V ','MATCH_OPTION',200,'','','','','','BS006141','VARCHAR2','','','Match Option','','','');
xxeis.eis_rs_ins.vc( 'XXCUS_GSC_HSD_AP_SUPPLIERS_V ','FUTURE_DATED_PAYMENT_CCID',200,'','','','','','BS006141','NUMBER','','','Future Dated Payment Ccid','','','');
xxeis.eis_rs_ins.vc( 'XXCUS_GSC_HSD_AP_SUPPLIERS_V ','CREATE_DEBIT_MEMO_FLAG',200,'','','','','','BS006141','VARCHAR2','','','Create Debit Memo Flag','','','');
xxeis.eis_rs_ins.vc( 'XXCUS_GSC_HSD_AP_SUPPLIERS_V ','OFFSET_TAX_FLAG',200,'','','','','','BS006141','VARCHAR2','','','Offset Tax Flag','','','');
xxeis.eis_rs_ins.vc( 'XXCUS_GSC_HSD_AP_SUPPLIERS_V ','PARTY_ID',200,'','','','','','BS006141','NUMBER','','','Party Id','','','');
xxeis.eis_rs_ins.vc( 'XXCUS_GSC_HSD_AP_SUPPLIERS_V ','PARENT_PARTY_ID',200,'','','','','','BS006141','NUMBER','','','Parent Party Id','','','');
xxeis.eis_rs_ins.vc( 'XXCUS_GSC_HSD_AP_SUPPLIERS_V ','NI_NUMBER',200,'','','','','','BS006141','VARCHAR2','','','Ni Number','','','');
xxeis.eis_rs_ins.vc( 'XXCUS_GSC_HSD_AP_SUPPLIERS_V ','TCA_SYNC_NUM_1099',200,'','','','','','BS006141','VARCHAR2','','','Tca Sync Num 1099','','','');
xxeis.eis_rs_ins.vc( 'XXCUS_GSC_HSD_AP_SUPPLIERS_V ','TCA_SYNC_VENDOR_NAME',200,'','','','','','BS006141','VARCHAR2','','','Tca Sync Vendor Name','','','');
xxeis.eis_rs_ins.vc( 'XXCUS_GSC_HSD_AP_SUPPLIERS_V ','TCA_SYNC_VAT_REG_NUM',200,'','','','','','BS006141','VARCHAR2','','','Tca Sync Vat Reg Num','','','');
xxeis.eis_rs_ins.vc( 'XXCUS_GSC_HSD_AP_SUPPLIERS_V ','INDIVIDUAL_1099',200,'','','','','','BS006141','VARCHAR2','','','Individual 1099','','','');
xxeis.eis_rs_ins.vc( 'XXCUS_GSC_HSD_AP_SUPPLIERS_V ','UNIQUE_TAX_REFERENCE_NUM',200,'','','','','','BS006141','NUMBER','','','Unique Tax Reference Num','','','');
xxeis.eis_rs_ins.vc( 'XXCUS_GSC_HSD_AP_SUPPLIERS_V ','PARTNERSHIP_UTR',200,'','','','','','BS006141','NUMBER','','','Partnership Utr','','','');
xxeis.eis_rs_ins.vc( 'XXCUS_GSC_HSD_AP_SUPPLIERS_V ','PARTNERSHIP_NAME',200,'','','','','','BS006141','VARCHAR2','','','Partnership Name','','','');
xxeis.eis_rs_ins.vc( 'XXCUS_GSC_HSD_AP_SUPPLIERS_V ','CIS_ENABLED_FLAG',200,'','','','','','BS006141','VARCHAR2','','','Cis Enabled Flag','','','');
xxeis.eis_rs_ins.vc( 'XXCUS_GSC_HSD_AP_SUPPLIERS_V ','FIRST_NAME',200,'','','','','','BS006141','VARCHAR2','','','First Name','','','');
xxeis.eis_rs_ins.vc( 'XXCUS_GSC_HSD_AP_SUPPLIERS_V ','SECOND_NAME',200,'','','','','','BS006141','VARCHAR2','','','Second Name','','','');
xxeis.eis_rs_ins.vc( 'XXCUS_GSC_HSD_AP_SUPPLIERS_V ','LAST_NAME',200,'','','','','','BS006141','VARCHAR2','','','Last Name','','','');
xxeis.eis_rs_ins.vc( 'XXCUS_GSC_HSD_AP_SUPPLIERS_V ','SALUTATION',200,'','','','','','BS006141','VARCHAR2','','','Salutation','','','');
xxeis.eis_rs_ins.vc( 'XXCUS_GSC_HSD_AP_SUPPLIERS_V ','TRADING_NAME',200,'','','','','','BS006141','VARCHAR2','','','Trading Name','','','');
xxeis.eis_rs_ins.vc( 'XXCUS_GSC_HSD_AP_SUPPLIERS_V ','WORK_REFERENCE',200,'','','','','','BS006141','VARCHAR2','','','Work Reference','','','');
xxeis.eis_rs_ins.vc( 'XXCUS_GSC_HSD_AP_SUPPLIERS_V ','COMPANY_REGISTRATION_NUMBER',200,'','','','','','BS006141','VARCHAR2','','','Company Registration Number','','','');
xxeis.eis_rs_ins.vc( 'XXCUS_GSC_HSD_AP_SUPPLIERS_V ','NATIONAL_INSURANCE_NUMBER',200,'','','','','','BS006141','VARCHAR2','','','National Insurance Number','','','');
xxeis.eis_rs_ins.vc( 'XXCUS_GSC_HSD_AP_SUPPLIERS_V ','VERIFICATION_NUMBER',200,'','','','','','BS006141','VARCHAR2','','','Verification Number','','','');
xxeis.eis_rs_ins.vc( 'XXCUS_GSC_HSD_AP_SUPPLIERS_V ','VERIFICATION_REQUEST_ID',200,'','','','','','BS006141','NUMBER','','','Verification Request Id','','','');
xxeis.eis_rs_ins.vc( 'XXCUS_GSC_HSD_AP_SUPPLIERS_V ','MATCH_STATUS_FLAG',200,'','','','','','BS006141','VARCHAR2','','','Match Status Flag','','','');
xxeis.eis_rs_ins.vc( 'XXCUS_GSC_HSD_AP_SUPPLIERS_V ','CIS_VERIFICATION_DATE',200,'','','','','','BS006141','DATE','','','Cis Verification Date','','','');
xxeis.eis_rs_ins.vc( 'XXCUS_GSC_HSD_AP_SUPPLIERS_V ','PAY_AWT_GROUP_ID',200,'','','','','','BS006141','NUMBER','','','Pay Awt Group Id','','','');
xxeis.eis_rs_ins.vc( 'XXCUS_GSC_HSD_AP_SUPPLIERS_V ','CIS_PARENT_VENDOR_ID',200,'','','','','','BS006141','NUMBER','','','Cis Parent Vendor Id','','','');
xxeis.eis_rs_ins.vc( 'XXCUS_GSC_HSD_AP_SUPPLIERS_V ','BUS_CLASS_LAST_CERTIFIED_DATE',200,'','','','','','BS006141','DATE','','','Bus Class Last Certified Date','','','');
xxeis.eis_rs_ins.vc( 'XXCUS_GSC_HSD_AP_SUPPLIERS_V ','BUS_CLASS_LAST_CERTIFIED_BY',200,'','','','','','BS006141','NUMBER','','','Bus Class Last Certified By','','','');
xxeis.eis_rs_ins.vc( 'XXCUS_GSC_HSD_AP_SUPPLIERS_V ','SET_OF_BOOKS_ID',200,'','','','','','BS006141','NUMBER','','','Set Of Books Id','','','');
xxeis.eis_rs_ins.vc( 'XXCUS_GSC_HSD_AP_SUPPLIERS_V ','CREDIT_STATUS_LOOKUP_CODE',200,'','','','','','BS006141','VARCHAR2','','','Credit Status Lookup Code','','','');
xxeis.eis_rs_ins.vc( 'XXCUS_GSC_HSD_AP_SUPPLIERS_V ','CREDIT_LIMIT',200,'','','','','','BS006141','NUMBER','','','Credit Limit','','','');
xxeis.eis_rs_ins.vc( 'XXCUS_GSC_HSD_AP_SUPPLIERS_V ','ALWAYS_TAKE_DISC_FLAG',200,'','','','','','BS006141','VARCHAR2','','','Always Take Disc Flag','','','');
xxeis.eis_rs_ins.vc( 'XXCUS_GSC_HSD_AP_SUPPLIERS_V ','PAY_DATE_BASIS_LOOKUP_CODE',200,'','','','','','BS006141','VARCHAR2','','','Pay Date Basis Lookup Code','','','');
xxeis.eis_rs_ins.vc( 'XXCUS_GSC_HSD_AP_SUPPLIERS_V ','PAY_GROUP_LOOKUP_CODE',200,'','','','','','BS006141','VARCHAR2','','','Pay Group Lookup Code','','','');
xxeis.eis_rs_ins.vc( 'XXCUS_GSC_HSD_AP_SUPPLIERS_V ','PAYMENT_PRIORITY',200,'','','','','','BS006141','NUMBER','','','Payment Priority','','','');
xxeis.eis_rs_ins.vc( 'XXCUS_GSC_HSD_AP_SUPPLIERS_V ','INVOICE_CURRENCY_CODE',200,'','','','','','BS006141','VARCHAR2','','','Invoice Currency Code','','','');
xxeis.eis_rs_ins.vc( 'XXCUS_GSC_HSD_AP_SUPPLIERS_V ','PAYMENT_CURRENCY_CODE',200,'','','','','','BS006141','VARCHAR2','','','Payment Currency Code','','','');
xxeis.eis_rs_ins.vc( 'XXCUS_GSC_HSD_AP_SUPPLIERS_V ','INVOICE_AMOUNT_LIMIT',200,'','','','','','BS006141','NUMBER','','','Invoice Amount Limit','','','');
xxeis.eis_rs_ins.vc( 'XXCUS_GSC_HSD_AP_SUPPLIERS_V ','EXCHANGE_DATE_LOOKUP_CODE',200,'','','','','','BS006141','VARCHAR2','','','Exchange Date Lookup Code','','','');
xxeis.eis_rs_ins.vc( 'XXCUS_GSC_HSD_AP_SUPPLIERS_V ','HOLD_ALL_PAYMENTS_FLAG',200,'','','','','','BS006141','VARCHAR2','','','Hold All Payments Flag','','','');
xxeis.eis_rs_ins.vc( 'XXCUS_GSC_HSD_AP_SUPPLIERS_V ','HOLD_FUTURE_PAYMENTS_FLAG',200,'','','','','','BS006141','VARCHAR2','','','Hold Future Payments Flag','','','');
xxeis.eis_rs_ins.vc( 'XXCUS_GSC_HSD_AP_SUPPLIERS_V ','HOLD_REASON',200,'','','','','','BS006141','VARCHAR2','','','Hold Reason','','','');
xxeis.eis_rs_ins.vc( 'XXCUS_GSC_HSD_AP_SUPPLIERS_V ','DISTRIBUTION_SET_ID',200,'','','','','','BS006141','NUMBER','','','Distribution Set Id','','','');
xxeis.eis_rs_ins.vc( 'XXCUS_GSC_HSD_AP_SUPPLIERS_V ','ACCTS_PAY_CODE_COMBINATION_ID',200,'','','','','','BS006141','NUMBER','','','Accts Pay Code Combination Id','','','');
xxeis.eis_rs_ins.vc( 'XXCUS_GSC_HSD_AP_SUPPLIERS_V ','DISC_LOST_CODE_COMBINATION_ID',200,'','','','','','BS006141','NUMBER','','','Disc Lost Code Combination Id','','','');
xxeis.eis_rs_ins.vc( 'XXCUS_GSC_HSD_AP_SUPPLIERS_V ','DISC_TAKEN_CODE_COMBINATION_ID',200,'','','','','','BS006141','NUMBER','','','Disc Taken Code Combination Id','','','');
xxeis.eis_rs_ins.vc( 'XXCUS_GSC_HSD_AP_SUPPLIERS_V ','EXPENSE_CODE_COMBINATION_ID',200,'','','','','','BS006141','NUMBER','','','Expense Code Combination Id','','','');
xxeis.eis_rs_ins.vc( 'XXCUS_GSC_HSD_AP_SUPPLIERS_V ','PREPAY_CODE_COMBINATION_ID',200,'','','','','','BS006141','NUMBER','','','Prepay Code Combination Id','','','');
xxeis.eis_rs_ins.vc( 'XXCUS_GSC_HSD_AP_SUPPLIERS_V ','NUM_1099',200,'','','','','','BS006141','VARCHAR2','','','Num 1099','','','');
xxeis.eis_rs_ins.vc( 'XXCUS_GSC_HSD_AP_SUPPLIERS_V ','TYPE_1099',200,'','','','','','BS006141','VARCHAR2','','','Type 1099','','','');
xxeis.eis_rs_ins.vc( 'XXCUS_GSC_HSD_AP_SUPPLIERS_V ','WITHHOLDING_STATUS_LOOKUP_CODE',200,'','','','','','BS006141','VARCHAR2','','','Withholding Status Lookup Code','','','');
xxeis.eis_rs_ins.vc( 'XXCUS_GSC_HSD_AP_SUPPLIERS_V ','WITHHOLDING_START_DATE',200,'','','','','','BS006141','DATE','','','Withholding Start Date','','','');
xxeis.eis_rs_ins.vc( 'XXCUS_GSC_HSD_AP_SUPPLIERS_V ','ORGANIZATION_TYPE_LOOKUP_CODE',200,'','','','','','BS006141','VARCHAR2','','','Organization Type Lookup Code','','','');
xxeis.eis_rs_ins.vc( 'XXCUS_GSC_HSD_AP_SUPPLIERS_V ','VAT_CODE',200,'','','','','','BS006141','VARCHAR2','','','Vat Code','','','');
xxeis.eis_rs_ins.vc( 'XXCUS_GSC_HSD_AP_SUPPLIERS_V ','START_DATE_ACTIVE',200,'','','','','','BS006141','DATE','','','Start Date Active','','','');
xxeis.eis_rs_ins.vc( 'XXCUS_GSC_HSD_AP_SUPPLIERS_V ','END_DATE_ACTIVE',200,'','','','','','BS006141','DATE','','','End Date Active','','','');
xxeis.eis_rs_ins.vc( 'XXCUS_GSC_HSD_AP_SUPPLIERS_V ','MINORITY_GROUP_LOOKUP_CODE',200,'','','','','','BS006141','VARCHAR2','','','Minority Group Lookup Code','','','');
xxeis.eis_rs_ins.vc( 'XXCUS_GSC_HSD_AP_SUPPLIERS_V ','PAYMENT_METHOD_LOOKUP_CODE',200,'','','','','','BS006141','VARCHAR2','','','Payment Method Lookup Code','','','');
xxeis.eis_rs_ins.vc( 'XXCUS_GSC_HSD_AP_SUPPLIERS_V ','BANK_ACCOUNT_NAME',200,'','','','','','BS006141','VARCHAR2','','','Bank Account Name','','','');
xxeis.eis_rs_ins.vc( 'XXCUS_GSC_HSD_AP_SUPPLIERS_V ','BANK_ACCOUNT_NUM',200,'','','','','','BS006141','VARCHAR2','','','Bank Account Num','','','');
xxeis.eis_rs_ins.vc( 'XXCUS_GSC_HSD_AP_SUPPLIERS_V ','BANK_NUM',200,'','','','','','BS006141','VARCHAR2','','','Bank Num','','','');
xxeis.eis_rs_ins.vc( 'XXCUS_GSC_HSD_AP_SUPPLIERS_V ','BANK_ACCOUNT_TYPE',200,'','','','','','BS006141','VARCHAR2','','','Bank Account Type','','','');
xxeis.eis_rs_ins.vc( 'XXCUS_GSC_HSD_AP_SUPPLIERS_V ','WOMEN_OWNED_FLAG',200,'','','','','','BS006141','VARCHAR2','','','Women Owned Flag','','','');
xxeis.eis_rs_ins.vc( 'XXCUS_GSC_HSD_AP_SUPPLIERS_V ','SMALL_BUSINESS_FLAG',200,'','','','','','BS006141','VARCHAR2','','','Small Business Flag','','','');
xxeis.eis_rs_ins.vc( 'XXCUS_GSC_HSD_AP_SUPPLIERS_V ','STANDARD_INDUSTRY_CLASS',200,'','','','','','BS006141','VARCHAR2','','','Standard Industry Class','','','');
xxeis.eis_rs_ins.vc( 'XXCUS_GSC_HSD_AP_SUPPLIERS_V ','HOLD_FLAG',200,'','','','','','BS006141','VARCHAR2','','','Hold Flag','','','');
xxeis.eis_rs_ins.vc( 'XXCUS_GSC_HSD_AP_SUPPLIERS_V ','PURCHASING_HOLD_REASON',200,'','','','','','BS006141','VARCHAR2','','','Purchasing Hold Reason','','','');
xxeis.eis_rs_ins.vc( 'XXCUS_GSC_HSD_AP_SUPPLIERS_V ','HOLD_BY',200,'','','','','','BS006141','NUMBER','','','Hold By','','','');
xxeis.eis_rs_ins.vc( 'XXCUS_GSC_HSD_AP_SUPPLIERS_V ','HOLD_DATE',200,'','','','','','BS006141','DATE','','','Hold Date','','','');
xxeis.eis_rs_ins.vc( 'XXCUS_GSC_HSD_AP_SUPPLIERS_V ','TERMS_DATE_BASIS',200,'','','','','','BS006141','VARCHAR2','','','Terms Date Basis','','','');
xxeis.eis_rs_ins.vc( 'XXCUS_GSC_HSD_AP_SUPPLIERS_V ','PRICE_TOLERANCE',200,'','','','','','BS006141','NUMBER','','','Price Tolerance','','','');
xxeis.eis_rs_ins.vc( 'XXCUS_GSC_HSD_AP_SUPPLIERS_V ','INSPECTION_REQUIRED_FLAG',200,'','','','','','BS006141','VARCHAR2','','','Inspection Required Flag','','','');
xxeis.eis_rs_ins.vc( 'XXCUS_GSC_HSD_AP_SUPPLIERS_V ','RECEIPT_REQUIRED_FLAG',200,'','','','','','BS006141','VARCHAR2','','','Receipt Required Flag','','','');
xxeis.eis_rs_ins.vc( 'XXCUS_GSC_HSD_AP_SUPPLIERS_V ','QTY_RCV_TOLERANCE',200,'','','','','','BS006141','NUMBER','','','Qty Rcv Tolerance','','','');
xxeis.eis_rs_ins.vc( 'XXCUS_GSC_HSD_AP_SUPPLIERS_V ','QTY_RCV_EXCEPTION_CODE',200,'','','','','','BS006141','VARCHAR2','','','Qty Rcv Exception Code','','','');
xxeis.eis_rs_ins.vc( 'XXCUS_GSC_HSD_AP_SUPPLIERS_V ','ENFORCE_SHIP_TO_LOCATION_CODE',200,'','','','','','BS006141','VARCHAR2','','','Enforce Ship To Location Code','','','');
xxeis.eis_rs_ins.vc( 'XXCUS_GSC_HSD_AP_SUPPLIERS_V ','DAYS_EARLY_RECEIPT_ALLOWED',200,'','','','','','BS006141','NUMBER','','','Days Early Receipt Allowed','','','');
xxeis.eis_rs_ins.vc( 'XXCUS_GSC_HSD_AP_SUPPLIERS_V ','DAYS_LATE_RECEIPT_ALLOWED',200,'','','','','','BS006141','NUMBER','','','Days Late Receipt Allowed','','','');
xxeis.eis_rs_ins.vc( 'XXCUS_GSC_HSD_AP_SUPPLIERS_V ','RECEIPT_DAYS_EXCEPTION_CODE',200,'','','','','','BS006141','VARCHAR2','','','Receipt Days Exception Code','','','');
xxeis.eis_rs_ins.vc( 'XXCUS_GSC_HSD_AP_SUPPLIERS_V ','RECEIVING_ROUTING_ID',200,'','','','','','BS006141','NUMBER','','','Receiving Routing Id','','','');
xxeis.eis_rs_ins.vc( 'XXCUS_GSC_HSD_AP_SUPPLIERS_V ','ALLOW_SUBSTITUTE_RECEIPTS_FLAG',200,'','','','','','BS006141','VARCHAR2','','','Allow Substitute Receipts Flag','','','');
xxeis.eis_rs_ins.vc( 'XXCUS_GSC_HSD_AP_SUPPLIERS_V ','ALLOW_UNORDERED_RECEIPTS_FLAG',200,'','','','','','BS006141','VARCHAR2','','','Allow Unordered Receipts Flag','','','');
xxeis.eis_rs_ins.vc( 'XXCUS_GSC_HSD_AP_SUPPLIERS_V ','HOLD_UNMATCHED_INVOICES_FLAG',200,'','','','','','BS006141','VARCHAR2','','','Hold Unmatched Invoices Flag','','','');
xxeis.eis_rs_ins.vc( 'XXCUS_GSC_HSD_AP_SUPPLIERS_V ','EXCLUSIVE_PAYMENT_FLAG',200,'','','','','','BS006141','VARCHAR2','','','Exclusive Payment Flag','','','');
xxeis.eis_rs_ins.vc( 'XXCUS_GSC_HSD_AP_SUPPLIERS_V ','AP_TAX_ROUNDING_RULE',200,'','','','','','BS006141','VARCHAR2','','','Ap Tax Rounding Rule','','','');
xxeis.eis_rs_ins.vc( 'XXCUS_GSC_HSD_AP_SUPPLIERS_V ','AUTO_TAX_CALC_FLAG',200,'','','','','','BS006141','VARCHAR2','','','Auto Tax Calc Flag','','','');
xxeis.eis_rs_ins.vc( 'XXCUS_GSC_HSD_AP_SUPPLIERS_V ','AUTO_TAX_CALC_OVERRIDE',200,'','','','','','BS006141','VARCHAR2','','','Auto Tax Calc Override','','','');
xxeis.eis_rs_ins.vc( 'XXCUS_GSC_HSD_AP_SUPPLIERS_V ','AMOUNT_INCLUDES_TAX_FLAG',200,'','','','','','BS006141','VARCHAR2','','','Amount Includes Tax Flag','','','');
xxeis.eis_rs_ins.vc( 'XXCUS_GSC_HSD_AP_SUPPLIERS_V ','TAX_VERIFICATION_DATE',200,'','','','','','BS006141','DATE','','','Tax Verification Date','','','');
xxeis.eis_rs_ins.vc( 'XXCUS_GSC_HSD_AP_SUPPLIERS_V ','NAME_CONTROL',200,'','','','','','BS006141','VARCHAR2','','','Name Control','','','');
xxeis.eis_rs_ins.vc( 'XXCUS_GSC_HSD_AP_SUPPLIERS_V ','STATE_REPORTABLE_FLAG',200,'','','','','','BS006141','VARCHAR2','','','State Reportable Flag','','','');
xxeis.eis_rs_ins.vc( 'XXCUS_GSC_HSD_AP_SUPPLIERS_V ','FEDERAL_REPORTABLE_FLAG',200,'','','','','','BS006141','VARCHAR2','','','Federal Reportable Flag','','','');
xxeis.eis_rs_ins.vc( 'XXCUS_GSC_HSD_AP_SUPPLIERS_V ','ATTRIBUTE_CATEGORY',200,'','','','','','BS006141','VARCHAR2','','','Attribute Category','','','');
xxeis.eis_rs_ins.vc( 'XXCUS_GSC_HSD_AP_SUPPLIERS_V ','ATTRIBUTE1',200,'','','','','','BS006141','VARCHAR2','','','Attribute1','','','');
xxeis.eis_rs_ins.vc( 'XXCUS_GSC_HSD_AP_SUPPLIERS_V ','ATTRIBUTE2',200,'','','','','','BS006141','VARCHAR2','','','Attribute2','','','');
xxeis.eis_rs_ins.vc( 'XXCUS_GSC_HSD_AP_SUPPLIERS_V ','ATTRIBUTE3',200,'','','','','','BS006141','VARCHAR2','','','Attribute3','','','');
xxeis.eis_rs_ins.vc( 'XXCUS_GSC_HSD_AP_SUPPLIERS_V ','ATTRIBUTE4',200,'','','','','','BS006141','VARCHAR2','','','Attribute4','','','');
xxeis.eis_rs_ins.vc( 'XXCUS_GSC_HSD_AP_SUPPLIERS_V ','ATTRIBUTE5',200,'','','','','','BS006141','VARCHAR2','','','Attribute5','','','');
xxeis.eis_rs_ins.vc( 'XXCUS_GSC_HSD_AP_SUPPLIERS_V ','ATTRIBUTE6',200,'','','','','','BS006141','VARCHAR2','','','Attribute6','','','');
xxeis.eis_rs_ins.vc( 'XXCUS_GSC_HSD_AP_SUPPLIERS_V ','ATTRIBUTE7',200,'','','','','','BS006141','VARCHAR2','','','Attribute7','','','');
xxeis.eis_rs_ins.vc( 'XXCUS_GSC_HSD_AP_SUPPLIERS_V ','ATTRIBUTE8',200,'','','','','','BS006141','VARCHAR2','','','Attribute8','','','');
xxeis.eis_rs_ins.vc( 'XXCUS_GSC_HSD_AP_SUPPLIERS_V ','ATTRIBUTE9',200,'','','','','','BS006141','VARCHAR2','','','Attribute9','','','');
xxeis.eis_rs_ins.vc( 'XXCUS_GSC_HSD_AP_SUPPLIERS_V ','ATTRIBUTE10',200,'','','','','','BS006141','VARCHAR2','','','Attribute10','','','');
xxeis.eis_rs_ins.vc( 'XXCUS_GSC_HSD_AP_SUPPLIERS_V ','ATTRIBUTE11',200,'','','','','','BS006141','VARCHAR2','','','Attribute11','','','');
xxeis.eis_rs_ins.vc( 'XXCUS_GSC_HSD_AP_SUPPLIERS_V ','ATTRIBUTE12',200,'','','','','','BS006141','VARCHAR2','','','Attribute12','','','');
xxeis.eis_rs_ins.vc( 'XXCUS_GSC_HSD_AP_SUPPLIERS_V ','ATTRIBUTE13',200,'','','','','','BS006141','VARCHAR2','','','Attribute13','','','');
xxeis.eis_rs_ins.vc( 'XXCUS_GSC_HSD_AP_SUPPLIERS_V ','ATTRIBUTE14',200,'','','','','','BS006141','VARCHAR2','','','Attribute14','','','');
xxeis.eis_rs_ins.vc( 'XXCUS_GSC_HSD_AP_SUPPLIERS_V ','ATTRIBUTE15',200,'','','','','','BS006141','VARCHAR2','','','Attribute15','','','');
xxeis.eis_rs_ins.vc( 'XXCUS_GSC_HSD_AP_SUPPLIERS_V ','REQUEST_ID',200,'','','','','','BS006141','NUMBER','','','Request Id','','','');
xxeis.eis_rs_ins.vc( 'XXCUS_GSC_HSD_AP_SUPPLIERS_V ','PROGRAM_APPLICATION_ID',200,'','','','','','BS006141','NUMBER','','','Program Application Id','','','');
xxeis.eis_rs_ins.vc( 'XXCUS_GSC_HSD_AP_SUPPLIERS_V ','PROGRAM_ID',200,'','','','','','BS006141','NUMBER','','','Program Id','','','');
xxeis.eis_rs_ins.vc( 'XXCUS_GSC_HSD_AP_SUPPLIERS_V ','PROGRAM_UPDATE_DATE',200,'','','','','','BS006141','DATE','','','Program Update Date','','','');
xxeis.eis_rs_ins.vc( 'XXCUS_GSC_HSD_AP_SUPPLIERS_V ','OFFSET_VAT_CODE',200,'','','','','','BS006141','VARCHAR2','','','Offset Vat Code','','','');
xxeis.eis_rs_ins.vc( 'XXCUS_GSC_HSD_AP_SUPPLIERS_V ','VAT_REGISTRATION_NUM',200,'','','','','','BS006141','VARCHAR2','','','Vat Registration Num','','','');
xxeis.eis_rs_ins.vc( 'XXCUS_GSC_HSD_AP_SUPPLIERS_V ','AUTO_CALCULATE_INTEREST_FLAG',200,'','','','','','BS006141','VARCHAR2','','','Auto Calculate Interest Flag','','','');
xxeis.eis_rs_ins.vc( 'XXCUS_GSC_HSD_AP_SUPPLIERS_V ','VALIDATION_NUMBER',200,'','','','','','BS006141','NUMBER','','','Validation Number','','','');
xxeis.eis_rs_ins.vc( 'XXCUS_GSC_HSD_AP_SUPPLIERS_V ','EXCLUDE_FREIGHT_FROM_DISCOUNT',200,'','','','','','BS006141','VARCHAR2','','','Exclude Freight From Discount','','','');
xxeis.eis_rs_ins.vc( 'XXCUS_GSC_HSD_AP_SUPPLIERS_V ','TAX_REPORTING_NAME',200,'','','','','','BS006141','VARCHAR2','','','Tax Reporting Name','','','');
xxeis.eis_rs_ins.vc( 'XXCUS_GSC_HSD_AP_SUPPLIERS_V ','CHECK_DIGITS',200,'','','','','','BS006141','VARCHAR2','','','Check Digits','','','');
xxeis.eis_rs_ins.vc( 'XXCUS_GSC_HSD_AP_SUPPLIERS_V ','BANK_NUMBER',200,'','','','','','BS006141','VARCHAR2','','','Bank Number','','','');
xxeis.eis_rs_ins.vc( 'XXCUS_GSC_HSD_AP_SUPPLIERS_V ','ALLOW_AWT_FLAG',200,'','','','','','BS006141','VARCHAR2','','','Allow Awt Flag','','','');
xxeis.eis_rs_ins.vc( 'XXCUS_GSC_HSD_AP_SUPPLIERS_V ','AWT_GROUP_ID',200,'','','','','','BS006141','NUMBER','','','Awt Group Id','','','');
xxeis.eis_rs_ins.vc( 'XXCUS_GSC_HSD_AP_SUPPLIERS_V ','GLOBAL_ATTRIBUTE1',200,'','','','','','BS006141','VARCHAR2','','','Global Attribute1','','','');
xxeis.eis_rs_ins.vc( 'XXCUS_GSC_HSD_AP_SUPPLIERS_V ','GLOBAL_ATTRIBUTE2',200,'','','','','','BS006141','VARCHAR2','','','Global Attribute2','','','');
xxeis.eis_rs_ins.vc( 'XXCUS_GSC_HSD_AP_SUPPLIERS_V ','GLOBAL_ATTRIBUTE3',200,'','','','','','BS006141','VARCHAR2','','','Global Attribute3','','','');
xxeis.eis_rs_ins.vc( 'XXCUS_GSC_HSD_AP_SUPPLIERS_V ','GLOBAL_ATTRIBUTE4',200,'','','','','','BS006141','VARCHAR2','','','Global Attribute4','','','');
xxeis.eis_rs_ins.vc( 'XXCUS_GSC_HSD_AP_SUPPLIERS_V ','GLOBAL_ATTRIBUTE5',200,'','','','','','BS006141','VARCHAR2','','','Global Attribute5','','','');
xxeis.eis_rs_ins.vc( 'XXCUS_GSC_HSD_AP_SUPPLIERS_V ','GLOBAL_ATTRIBUTE6',200,'','','','','','BS006141','VARCHAR2','','','Global Attribute6','','','');
xxeis.eis_rs_ins.vc( 'XXCUS_GSC_HSD_AP_SUPPLIERS_V ','GLOBAL_ATTRIBUTE7',200,'','','','','','BS006141','VARCHAR2','','','Global Attribute7','','','');
xxeis.eis_rs_ins.vc( 'XXCUS_GSC_HSD_AP_SUPPLIERS_V ','GLOBAL_ATTRIBUTE8',200,'','','','','','BS006141','VARCHAR2','','','Global Attribute8','','','');
xxeis.eis_rs_ins.vc( 'XXCUS_GSC_HSD_AP_SUPPLIERS_V ','GLOBAL_ATTRIBUTE9',200,'','','','','','BS006141','VARCHAR2','','','Global Attribute9','','','');
xxeis.eis_rs_ins.vc( 'XXCUS_GSC_HSD_AP_SUPPLIERS_V ','GLOBAL_ATTRIBUTE10',200,'','','','','','BS006141','VARCHAR2','','','Global Attribute10','','','');
xxeis.eis_rs_ins.vc( 'XXCUS_GSC_HSD_AP_SUPPLIERS_V ','GLOBAL_ATTRIBUTE11',200,'','','','','','BS006141','VARCHAR2','','','Global Attribute11','','','');
xxeis.eis_rs_ins.vc( 'XXCUS_GSC_HSD_AP_SUPPLIERS_V ','GLOBAL_ATTRIBUTE12',200,'','','','','','BS006141','VARCHAR2','','','Global Attribute12','','','');
xxeis.eis_rs_ins.vc( 'XXCUS_GSC_HSD_AP_SUPPLIERS_V ','GLOBAL_ATTRIBUTE13',200,'','','','','','BS006141','VARCHAR2','','','Global Attribute13','','','');
xxeis.eis_rs_ins.vc( 'XXCUS_GSC_HSD_AP_SUPPLIERS_V ','GLOBAL_ATTRIBUTE14',200,'','','','','','BS006141','VARCHAR2','','','Global Attribute14','','','');
xxeis.eis_rs_ins.vc( 'XXCUS_GSC_HSD_AP_SUPPLIERS_V ','VENDOR_ID',200,'','','','','','BS006141','NUMBER','','','Vendor Id','','','');
xxeis.eis_rs_ins.vc( 'XXCUS_GSC_HSD_AP_SUPPLIERS_V ','LAST_UPDATE_DATE',200,'','','','','','BS006141','DATE','','','Last Update Date','','','');
xxeis.eis_rs_ins.vc( 'XXCUS_GSC_HSD_AP_SUPPLIERS_V ','LAST_UPDATED_BY',200,'','','','','','BS006141','NUMBER','','','Last Updated By','','','');
xxeis.eis_rs_ins.vc( 'XXCUS_GSC_HSD_AP_SUPPLIERS_V ','VENDOR_NAME',200,'','','','','','BS006141','VARCHAR2','','','Vendor Name','','','');
xxeis.eis_rs_ins.vc( 'XXCUS_GSC_HSD_AP_SUPPLIERS_V ','VENDOR_NAME_ALT',200,'','','','','','BS006141','VARCHAR2','','','Vendor Name Alt','','','');
xxeis.eis_rs_ins.vc( 'XXCUS_GSC_HSD_AP_SUPPLIERS_V ','SEGMENT1',200,'','','','','','BS006141','VARCHAR2','','','Segment1','','','');
xxeis.eis_rs_ins.vc( 'XXCUS_GSC_HSD_AP_SUPPLIERS_V ','SUMMARY_FLAG',200,'','','','','','BS006141','VARCHAR2','','','Summary Flag','','','');
xxeis.eis_rs_ins.vc( 'XXCUS_GSC_HSD_AP_SUPPLIERS_V ','ENABLED_FLAG',200,'','','','','','BS006141','VARCHAR2','','','Enabled Flag','','','');
xxeis.eis_rs_ins.vc( 'XXCUS_GSC_HSD_AP_SUPPLIERS_V ','SEGMENT2',200,'','','','','','BS006141','VARCHAR2','','','Segment2','','','');
xxeis.eis_rs_ins.vc( 'XXCUS_GSC_HSD_AP_SUPPLIERS_V ','SEGMENT3',200,'','','','','','BS006141','VARCHAR2','','','Segment3','','','');
xxeis.eis_rs_ins.vc( 'XXCUS_GSC_HSD_AP_SUPPLIERS_V ','SEGMENT4',200,'','','','','','BS006141','VARCHAR2','','','Segment4','','','');
xxeis.eis_rs_ins.vc( 'XXCUS_GSC_HSD_AP_SUPPLIERS_V ','SEGMENT5',200,'','','','','','BS006141','VARCHAR2','','','Segment5','','','');
xxeis.eis_rs_ins.vc( 'XXCUS_GSC_HSD_AP_SUPPLIERS_V ','LAST_UPDATE_LOGIN',200,'','','','','','BS006141','NUMBER','','','Last Update Login','','','');
xxeis.eis_rs_ins.vc( 'XXCUS_GSC_HSD_AP_SUPPLIERS_V ','CREATION_DATE',200,'','','','','','BS006141','DATE','','','Creation Date','','','');
xxeis.eis_rs_ins.vc( 'XXCUS_GSC_HSD_AP_SUPPLIERS_V ','CREATED_BY',200,'','','','','','BS006141','NUMBER','','','Created By','','','');
xxeis.eis_rs_ins.vc( 'XXCUS_GSC_HSD_AP_SUPPLIERS_V ','EMPLOYEE_ID',200,'','','','','','BS006141','NUMBER','','','Employee Id','','','');
xxeis.eis_rs_ins.vc( 'XXCUS_GSC_HSD_AP_SUPPLIERS_V ','VENDOR_TYPE_LOOKUP_CODE',200,'','','','','','BS006141','VARCHAR2','','','Vendor Type Lookup Code','','','');
xxeis.eis_rs_ins.vc( 'XXCUS_GSC_HSD_AP_SUPPLIERS_V ','CUSTOMER_NUM',200,'','','','','','BS006141','VARCHAR2','','','Customer Num','','','');
xxeis.eis_rs_ins.vc( 'XXCUS_GSC_HSD_AP_SUPPLIERS_V ','ONE_TIME_FLAG',200,'','','','','','BS006141','VARCHAR2','','','One Time Flag','','','');
xxeis.eis_rs_ins.vc( 'XXCUS_GSC_HSD_AP_SUPPLIERS_V ','PARENT_VENDOR_ID',200,'','','','','','BS006141','NUMBER','','','Parent Vendor Id','','','');
xxeis.eis_rs_ins.vc( 'XXCUS_GSC_HSD_AP_SUPPLIERS_V ','MIN_ORDER_AMOUNT',200,'','','','','','BS006141','NUMBER','','','Min Order Amount','','','');
xxeis.eis_rs_ins.vc( 'XXCUS_GSC_HSD_AP_SUPPLIERS_V ','SHIP_TO_LOCATION_ID',200,'','','','','','BS006141','NUMBER','','','Ship To Location Id','','','');
xxeis.eis_rs_ins.vc( 'XXCUS_GSC_HSD_AP_SUPPLIERS_V ','BILL_TO_LOCATION_ID',200,'','','','','','BS006141','NUMBER','','','Bill To Location Id','','','');
xxeis.eis_rs_ins.vc( 'XXCUS_GSC_HSD_AP_SUPPLIERS_V ','SHIP_VIA_LOOKUP_CODE',200,'','','','','','BS006141','VARCHAR2','','','Ship Via Lookup Code','','','');
xxeis.eis_rs_ins.vc( 'XXCUS_GSC_HSD_AP_SUPPLIERS_V ','FREIGHT_TERMS_LOOKUP_CODE',200,'','','','','','BS006141','VARCHAR2','','','Freight Terms Lookup Code','','','');
xxeis.eis_rs_ins.vc( 'XXCUS_GSC_HSD_AP_SUPPLIERS_V ','FOB_LOOKUP_CODE',200,'','','','','','BS006141','VARCHAR2','','','Fob Lookup Code','','','');
xxeis.eis_rs_ins.vc( 'XXCUS_GSC_HSD_AP_SUPPLIERS_V ','TERMS_ID',200,'','','','','','BS006141','NUMBER','','','Terms Id','','','');
--Inserting View Components for XXCUS_GSC_HSD_AP_SUPPLIERS_V 
xxeis.eis_rs_ins.vcomp( 'XXCUS_GSC_HSD_AP_SUPPLIERS_V ','AP_SUPPLIER_SITES_ALL',200,'AP_SUPPLIER_SITES_ALL','ASSA','','BS006141','BS006141','-1','AP_SUPPLIER_SITES_ALL','','','','');
--Inserting View Component Joins for XXCUS_GSC_HSD_AP_SUPPLIERS_V 
xxeis.eis_rs_ins.vcj( 'XXCUS_GSC_HSD_AP_SUPPLIERS_V ','AP_SUPPLIER_SITES_ALL','ASSA',200,'X1AV12345.VENDOR_ID','IN','ASSA.VENDOR_ID','','','','','BS006141','','');
END;
/
set scan on define on
prompt Creating Report Data for HDS Inactive Vendor Listing
set scan off define off
DECLARE
BEGIN 
--Deleting Report data - HDS Inactive Vendor Listing
xxeis.eis_rs_utility.delete_report_rows( 'HDS Inactive Vendor Listing' );
--Inserting Report - HDS Inactive Vendor Listing
xxeis.eis_rs_ins.r( 200,'HDS Inactive Vendor Listing','','AP Vendor listing of deactivated Vendors (Service Ticket #308989)','','','','BS006141','XXCUS_GSC_HSD_AP_SUPPLIERS_V ','Y','','','BS006141','','N','HDS Standard Reports','','CSV,EXCEL,','N');
--Inserting Report Columns - HDS Inactive Vendor Listing
xxeis.eis_rs_ins.rc( 'HDS Inactive Vendor Listing',200,'ATTRIBUTE2','Attribute2','','','','default','','4','N','','','','','','','','BS006141','N','N','','XXCUS_GSC_HSD_AP_SUPPLIERS_V ','','');
xxeis.eis_rs_ins.rc( 'HDS Inactive Vendor Listing',200,'END_DATE_ACTIVE','End Date Active','','','','default','','3','N','','','','','','','','BS006141','N','N','','XXCUS_GSC_HSD_AP_SUPPLIERS_V ','','');
xxeis.eis_rs_ins.rc( 'HDS Inactive Vendor Listing',200,'NUM_1099','Num 1099','','','','default','','9','N','','','','','','','','BS006141','N','N','','XXCUS_GSC_HSD_AP_SUPPLIERS_V ','','');
xxeis.eis_rs_ins.rc( 'HDS Inactive Vendor Listing',200,'PAYMENT_METHOD_LOOKUP_CODE','Payment Method Lookup Code','','','','default','','5','N','','','','','','','','BS006141','N','N','','XXCUS_GSC_HSD_AP_SUPPLIERS_V ','','');
xxeis.eis_rs_ins.rc( 'HDS Inactive Vendor Listing',200,'TERMS_DATE_BASIS','Terms Date Basis','','','','default','','6','N','','','','','','','','BS006141','N','N','','XXCUS_GSC_HSD_AP_SUPPLIERS_V ','','');
xxeis.eis_rs_ins.rc( 'HDS Inactive Vendor Listing',200,'TYPE_1099','Type 1099','','','','default','','10','N','','','','','','','','BS006141','N','N','','XXCUS_GSC_HSD_AP_SUPPLIERS_V ','','');
xxeis.eis_rs_ins.rc( 'HDS Inactive Vendor Listing',200,'ADDRESS_LINE1','Address Line1','','','','default','','12','N','','','','','','','','BS006141','N','N','','XXCUS_GSC_HSD_AP_SUPPLIERS_V ','AP_SUPPLIER_SITES_ALL','ASSA');
xxeis.eis_rs_ins.rc( 'HDS Inactive Vendor Listing',200,'ADDRESS_LINE2','Address Line2','','','','default','','13','N','','','','','','','','BS006141','N','N','','XXCUS_GSC_HSD_AP_SUPPLIERS_V ','AP_SUPPLIER_SITES_ALL','ASSA');
xxeis.eis_rs_ins.rc( 'HDS Inactive Vendor Listing',200,'STATE','State','','','','default','','16','N','','','','','','','','BS006141','N','N','','XXCUS_GSC_HSD_AP_SUPPLIERS_V ','AP_SUPPLIER_SITES_ALL','ASSA');
xxeis.eis_rs_ins.rc( 'HDS Inactive Vendor Listing',200,'ZIP','Zip','','','','default','','15','N','','','','','','','','BS006141','N','N','','XXCUS_GSC_HSD_AP_SUPPLIERS_V ','AP_SUPPLIER_SITES_ALL','ASSA');
xxeis.eis_rs_ins.rc( 'HDS Inactive Vendor Listing',200,'VENDOR_SITE_CODE','Vendor Site Code','','','','default','','8','N','','','','','','','','BS006141','N','N','','XXCUS_GSC_HSD_AP_SUPPLIERS_V ','AP_SUPPLIER_SITES_ALL','ASSA');
xxeis.eis_rs_ins.rc( 'HDS Inactive Vendor Listing',200,'CITY','City','','','','default','','14','N','','','','','','','','BS006141','N','N','','XXCUS_GSC_HSD_AP_SUPPLIERS_V ','AP_SUPPLIER_SITES_ALL','ASSA');
xxeis.eis_rs_ins.rc( 'HDS Inactive Vendor Listing',200,'COUNTY','County','','','','default','','17','N','','','','','','','','BS006141','N','N','','XXCUS_GSC_HSD_AP_SUPPLIERS_V ','AP_SUPPLIER_SITES_ALL','ASSA');
xxeis.eis_rs_ins.rc( 'HDS Inactive Vendor Listing',200,'CREATION_DATE','Creation Date','','','','default','','2','N','','','','','','','','BS006141','N','N','','XXCUS_GSC_HSD_AP_SUPPLIERS_V ','AP_SUPPLIER_SITES_ALL','ASSA');
xxeis.eis_rs_ins.rc( 'HDS Inactive Vendor Listing',200,'ORG_ID','Org Id','','','~~~','default','','7','N','','','','','','','','BS006141','N','N','','XXCUS_GSC_HSD_AP_SUPPLIERS_V ','AP_SUPPLIER_SITES_ALL','ASSA');
xxeis.eis_rs_ins.rc( 'HDS Inactive Vendor Listing',200,'PAYMENT_METHOD_LOOKUP_CODE','Payment Method Lookup Code','','','','default','','11','N','','','','','','','','BS006141','N','N','','XXCUS_GSC_HSD_AP_SUPPLIERS_V ','AP_SUPPLIER_SITES_ALL','ASSA');
xxeis.eis_rs_ins.rc( 'HDS Inactive Vendor Listing',200,'REMITTANCE_EMAIL','Remittance Email','','','','default','','18','N','','','','','','','','BS006141','N','N','','XXCUS_GSC_HSD_AP_SUPPLIERS_V ','AP_SUPPLIER_SITES_ALL','ASSA');
xxeis.eis_rs_ins.rc( 'HDS Inactive Vendor Listing',200,'VENDOR_NAME','Vendor Name','','','','default','','1','N','','','','','','','','BS006141','N','N','','XXCUS_GSC_HSD_AP_SUPPLIERS_V ','','');
xxeis.eis_rs_ins.rc( 'HDS Inactive Vendor Listing',200,'VENDOR_ID','Vendor Id','','','~~~','default','','19','N','','','','','','','','BS006141','N','N','','XXCUS_GSC_HSD_AP_SUPPLIERS_V ','','');
xxeis.eis_rs_ins.rc( 'HDS Inactive Vendor Listing',200,'INACTIVE_DATE','Inactive Date','','','','default','','20','N','','','','','','','','BS006141','N','N','','XXCUS_GSC_HSD_AP_SUPPLIERS_V ','AP_SUPPLIER_SITES_ALL','ASSA');
--Inserting Report Parameters - HDS Inactive Vendor Listing
xxeis.eis_rs_ins.rp( 'HDS Inactive Vendor Listing',200,'End Date To','End Date To','END_DATE_ACTIVE','>=','','','DATE','N','Y','1','','Y','CONSTANT','BS006141','Y','N','','Start Date','');
xxeis.eis_rs_ins.rp( 'HDS Inactive Vendor Listing',200,'End Date From','End Date From','END_DATE_ACTIVE','<=','','','DATE','N','Y','2','','Y','CONSTANT','BS006141','Y','N','','End Date','');
--Inserting Report Conditions - HDS Inactive Vendor Listing
xxeis.eis_rs_ins.rcn( 'HDS Inactive Vendor Listing',200,'X1AV12345.PAY_GROUP_LOOKUP_CODE','<>','''EMPLOYEE''','','','Y','4','N','BS006141');
xxeis.eis_rs_ins.rcn( 'HDS Inactive Vendor Listing',200,'ASSA.INACTIVE_DATE','IS','not null','','','Y','3','N','BS006141');
xxeis.eis_rs_ins.rcn( 'HDS Inactive Vendor Listing',200,'END_DATE_ACTIVE','>=',':End Date To','','','Y','1','Y','BS006141');
xxeis.eis_rs_ins.rcn( 'HDS Inactive Vendor Listing',200,'END_DATE_ACTIVE','<=',':End Date From','','','Y','2','Y','BS006141');
xxeis.eis_rs_ins.rcn( 'HDS Inactive Vendor Listing',200,'ASSA.ORG_ID','<>','''162''','','','Y','5','N','BS006141');
--Inserting Report Sorts - HDS Inactive Vendor Listing
--Inserting Report Triggers - HDS Inactive Vendor Listing
--Inserting Report Templates - HDS Inactive Vendor Listing
--Inserting Report Portals - HDS Inactive Vendor Listing
--Inserting Report Dashboards - HDS Inactive Vendor Listing
--Inserting Report Security - HDS Inactive Vendor Listing
--Inserting Report Pivots - HDS Inactive Vendor Listing
END;
/
set scan on define on
