/* ************************************************************************
  $Header TMS_20170608-00089_without_source_in_order.sql $
  Module Name: TMS_20170608-00089 Data Fix script

  PURPOSE: Data Fix script 

  REVISIONS:
  Ver        Date         Author                Description
  ---------  -----------  ------------------    --------------------------
  1.0        06-JUL-2017  Pattabhi Avula        TMS#20170608-00089

************************************************************************* */ 
SET SERVEROUTPUT ON SIZE 1000000;
DECLARE
BEGIN
   DBMS_OUTPUT.put_line ('TMS: 20170608-00089   , Before Update');
   
UPDATE po_requisition_lines_all
SET quantity = 0
WHERE requisition_header_id = 11189150;

 DBMS_OUTPUT.put_line (
         'TMS: 20170608-00089 Sales order lines updated (Expected:1): '
      || SQL%ROWCOUNT);

UPDATE po_req_distributions_all
SET REQ_LINE_QUANTITY = 0
WHERE requisition_line_id = 26179479;

 DBMS_OUTPUT.put_line (
         'TMS: 20170608-00089 Sales order lines updated (Expected:1): '
      || SQL%ROWCOUNT);

   COMMIT;

   DBMS_OUTPUT.put_line ('TMS: 20170608-00089   , End Update');
EXCEPTION
   WHEN OTHERS
   THEN
      ROLLBACK;
      DBMS_OUTPUT.put_line ('TMS: 20170608-00089, Errors : ' || SQLERRM);
END;
/