---------------------------------------------------------------------------------------------------------------
/**************************************************************************************************************
  $Header XXEIS.XXWC_GTT_OE_ORDER_LINES_TBL $
  Module Name : Order Management
  PURPOSE	  : Open Sales Orders Report
  VERSION DATE               AUTHOR(S)       DESCRIPTION
  ------- -----------------  --------------- -----------------------------------------
  1.0     11-Aug-2017		Siva			  TMS#20170727-00135 
**************************************************************************************************************/
ALTER TABLE XXEIS.XXWC_GTT_OE_ORDER_LINES_TBL ADD REGION VARCHAR2(150)
/
