/*
 TMS: 20150921-00010  
 Date: 09/21/2015
 Notes: data fix script to process the delivery id. Sales order # 17978247, delivery # 
 4540317 is stuck in the interface and need to be processed to progress the delivery
*/

SET SERVEROUTPUT ON SIZE 100000;
SET VERIFY OFF;


update apps.wsh_delivery_Details
set OE_INTERFACED_FLAG='Y'
where delivery_detail_id=11861267
and source_line_id=53868027;

commit;

/