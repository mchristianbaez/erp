--Report Name            : Specials Report Test
--Import Instructions    : Run this file in XXEIS user from SQL*Plus or any tool like Toad, SQL Navigator. 
prompt Creating View Data for Specials Report Test
set scan off define off
DECLARE
BEGIN 
--Inserting View EIS_XXWC_INV_SPECIAL_TEST_V
xxeis.eis_rs_ins.v( 'EIS_XXWC_INV_SPECIAL_TEST_V',401,'','','','','SA059956','XXEIS','Eis Rs Xxwc Inv Special V','EXISV','','');
--Delete View Columns for EIS_XXWC_INV_SPECIAL_TEST_V
xxeis.eis_rs_utility.delete_view_rows('EIS_XXWC_INV_SPECIAL_TEST_V',401,FALSE);
--Inserting View Columns for EIS_XXWC_INV_SPECIAL_TEST_V
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_SPECIAL_TEST_V','BUYER_CODE',401,'Buyer Code','BUYER_CODE','','','','SA059956','VARCHAR2','','','Buyer Code','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_SPECIAL_TEST_V','ONHAND',401,'Onhand','ONHAND','','','','SA059956','NUMBER','','','Onhand','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_SPECIAL_TEST_V','WEIGHT',401,'Weight','WEIGHT','','','','SA059956','NUMBER','','','Weight','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_SPECIAL_TEST_V','AVERAGECOST',401,'Averagecost','AVERAGECOST','','~T~D~2','','SA059956','NUMBER','','','Averagecost','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_SPECIAL_TEST_V','UOM',401,'Uom','UOM','','','','SA059956','VARCHAR2','','','Uom','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_SPECIAL_TEST_V','CAT',401,'Cat','CAT','','','','SA059956','VARCHAR2','','','Cat','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_SPECIAL_TEST_V','DESCRIPTION',401,'Description','DESCRIPTION','','','','SA059956','VARCHAR2','','','Description','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_SPECIAL_TEST_V','PART_NUMBER',401,'Part Number','PART_NUMBER','','','','SA059956','VARCHAR2','','','Part Number','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_SPECIAL_TEST_V','VENDOR_NAME',401,'Vendor Name','VENDOR_NAME','','','','SA059956','VARCHAR2','','','Vendor Name','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_SPECIAL_TEST_V','VENDOR_NUMBER',401,'Vendor Number','VENDOR_NUMBER','','','','SA059956','VARCHAR2','','','Vendor Number','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_SPECIAL_TEST_V','GP',401,'Gp','GP','','','','SA059956','NUMBER','','','Gp','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_SPECIAL_TEST_V','ORGANIZATION_CODE',401,'Organization Code','ORGANIZATION_CODE','','','','SA059956','VARCHAR2','','','Organization Code','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_SPECIAL_TEST_V','NO_OF_ITEM_PURCHASED',401,'No Of Item Purchased','NO_OF_ITEM_PURCHASED','','','','SA059956','NUMBER','','','No Of Item Purchased','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_SPECIAL_TEST_V','SELLING_PRICE',401,'Selling Price','SELLING_PRICE','','','','SA059956','NUMBER','','','Selling Price','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_SPECIAL_TEST_V','CREATION_DATE',401,'Creation Date','CREATION_DATE','','','','SA059956','DATE','','','Creation Date','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_SPECIAL_TEST_V','AGENT_ID',401,'Agent Id','AGENT_ID','','','','SA059956','NUMBER','','','Agent Id','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_SPECIAL_TEST_V','ORG_ORGANIZATION_ID',401,'Org Organization Id','ORG_ORGANIZATION_ID','','','','SA059956','NUMBER','','','Org Organization Id','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_SPECIAL_TEST_V','INV_ORGANIZATION_ID',401,'Inv Organization Id','INV_ORGANIZATION_ID','','','','SA059956','NUMBER','','','Inv Organization Id','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_SPECIAL_TEST_V','INVENTORY_ITEM_ID',401,'Inventory Item Id','INVENTORY_ITEM_ID','','','','SA059956','NUMBER','','','Inventory Item Id','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_SPECIAL_TEST_V','MSI#HDS#LOB',401,'Descriptive flexfield: Items Column Name: LOB Context: HDS','MSI#HDS#LOB','','','','SA059956','VARCHAR2','MTL_SYSTEM_ITEMS_B','ATTRIBUTE1','Msi#Hds#Lob','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_SPECIAL_TEST_V','MSI#HDS#DROP_SHIPMENT',401,'Descriptive flexfield: Items Column Name: Drop Shipment Context: HDS','MSI#HDS#Drop_Shipment','','','','SA059956','VARCHAR2','MTL_SYSTEM_ITEMS_B','ATTRIBUTE10','Msi#Hds#Drop Shipment','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_SPECIAL_TEST_V','MSI#HDS#INVOICE_UOM',401,'Descriptive flexfield: Items Column Name: Invoice UOM Context: HDS','MSI#HDS#Invoice_UOM','','','','SA059956','VARCHAR2','MTL_SYSTEM_ITEMS_B','ATTRIBUTE15','Msi#Hds#Invoice Uom','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_SPECIAL_TEST_V','MSI#HDS#PRODUCT_ID',401,'Descriptive flexfield: Items Column Name: Product ID Context: HDS','MSI#HDS#Product_ID','','','','SA059956','VARCHAR2','MTL_SYSTEM_ITEMS_B','ATTRIBUTE2','Msi#Hds#Product Id','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_SPECIAL_TEST_V','MSI#HDS#VENDOR_PART_NUMBER',401,'Descriptive flexfield: Items Column Name: Vendor Part Number Context: HDS','MSI#HDS#Vendor_Part_Number','','','','SA059956','VARCHAR2','MTL_SYSTEM_ITEMS_B','ATTRIBUTE3','Msi#Hds#Vendor Part Number','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_SPECIAL_TEST_V','MSI#HDS#UNSPSC_CODE',401,'Descriptive flexfield: Items Column Name: UNSPSC Code Context: HDS','MSI#HDS#UNSPSC_Code','','','','SA059956','VARCHAR2','MTL_SYSTEM_ITEMS_B','ATTRIBUTE4','Msi#Hds#Unspsc Code','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_SPECIAL_TEST_V','MSI#HDS#UPC_PRIMARY',401,'Descriptive flexfield: Items Column Name: UPC Primary Context: HDS','MSI#HDS#UPC_Primary','','','','SA059956','VARCHAR2','MTL_SYSTEM_ITEMS_B','ATTRIBUTE5','Msi#Hds#Upc Primary','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_SPECIAL_TEST_V','MSI#HDS#SKU_DESCRIPTION',401,'Descriptive flexfield: Items Column Name: SKU Description Context: HDS','MSI#HDS#SKU_Description','','','','SA059956','VARCHAR2','MTL_SYSTEM_ITEMS_B','ATTRIBUTE7','Msi#Hds#Sku Description','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_SPECIAL_TEST_V','MSI#WC#CA_PROP_65',401,'Descriptive flexfield: Items Column Name: CA Prop 65 Context: WC','MSI#WC#CA_Prop_65','','','','SA059956','VARCHAR2','MTL_SYSTEM_ITEMS_B','ATTRIBUTE1','Msi#Wc#Ca Prop 65','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_SPECIAL_TEST_V','MSI#WC#COUNTRY_OF_ORIGIN',401,'Descriptive flexfield: Items Column Name: Country of Origin Context: WC','MSI#WC#Country_of_Origin','','','','SA059956','VARCHAR2','MTL_SYSTEM_ITEMS_B','ATTRIBUTE10','Msi#Wc#Country Of Origin','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_SPECIAL_TEST_V','MSI#WC#ORM_D_FLAG',401,'Descriptive flexfield: Items Column Name: ORM-D Flag Context: WC','MSI#WC#ORM_D_Flag','','','','SA059956','VARCHAR2','MTL_SYSTEM_ITEMS_B','ATTRIBUTE11','Msi#Wc#Orm-D Flag','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_SPECIAL_TEST_V','MSI#WC#STORE_VELOCITY',401,'Descriptive flexfield: Items Column Name: Store Velocity Context: WC','MSI#WC#Store_Velocity','','','','SA059956','VARCHAR2','MTL_SYSTEM_ITEMS_B','ATTRIBUTE12','Msi#Wc#Store Velocity','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_SPECIAL_TEST_V','MSI#WC#DC_VELOCITY',401,'Descriptive flexfield: Items Column Name: DC Velocity Context: WC','MSI#WC#DC_Velocity','','','','SA059956','VARCHAR2','MTL_SYSTEM_ITEMS_B','ATTRIBUTE13','Msi#Wc#Dc Velocity','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_SPECIAL_TEST_V','MSI#WC#YEARLY_STORE_VELOCITY',401,'Descriptive flexfield: Items Column Name: Yearly Store Velocity Context: WC','MSI#WC#Yearly_Store_Velocity','','','','SA059956','VARCHAR2','MTL_SYSTEM_ITEMS_B','ATTRIBUTE14','Msi#Wc#Yearly Store Velocity','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_SPECIAL_TEST_V','MSI#WC#YEARLY_DC_VELOCITY',401,'Descriptive flexfield: Items Column Name: Yearly DC Velocity Context: WC','MSI#WC#Yearly_DC_Velocity','','','','SA059956','VARCHAR2','MTL_SYSTEM_ITEMS_B','ATTRIBUTE15','Msi#Wc#Yearly Dc Velocity','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_SPECIAL_TEST_V','MSI#WC#PRISM_PART_NUMBER',401,'Descriptive flexfield: Items Column Name: PRISM Part Number Context: WC','MSI#WC#PRISM_Part_Number','','','','SA059956','VARCHAR2','MTL_SYSTEM_ITEMS_B','ATTRIBUTE16','Msi#Wc#Prism Part Number','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_SPECIAL_TEST_V','MSI#WC#HAZMAT_DESCRIPTION',401,'Descriptive flexfield: Items Column Name: Hazmat Description Context: WC','MSI#WC#Hazmat_Description','','','','SA059956','VARCHAR2','MTL_SYSTEM_ITEMS_B','ATTRIBUTE17','Msi#Wc#Hazmat Description','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_SPECIAL_TEST_V','MSI#WC#HAZMAT_CONTAINER',401,'Descriptive flexfield: Items Column Name: Hazmat Container Context: WC','MSI#WC#Hazmat_Container','','','','SA059956','VARCHAR2','MTL_SYSTEM_ITEMS_B','ATTRIBUTE18','Msi#Wc#Hazmat Container','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_SPECIAL_TEST_V','MSI#WC#GTP_INDICATOR',401,'Descriptive flexfield: Items Column Name: GTP Indicator Context: WC','MSI#WC#GTP_Indicator','','','','SA059956','VARCHAR2','MTL_SYSTEM_ITEMS_B','ATTRIBUTE19','Msi#Wc#Gtp Indicator','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_SPECIAL_TEST_V','MSI#WC#LAST_LEAD_TIME',401,'Descriptive flexfield: Items Column Name: Last Lead Time Context: WC','MSI#WC#Last_Lead_Time','','','','SA059956','VARCHAR2','MTL_SYSTEM_ITEMS_B','ATTRIBUTE2','Msi#Wc#Last Lead Time','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_SPECIAL_TEST_V','MSI#WC#AMU',401,'Descriptive flexfield: Items Column Name: AMU Context: WC','MSI#WC#AMU','','','','SA059956','VARCHAR2','MTL_SYSTEM_ITEMS_B','ATTRIBUTE20','Msi#Wc#Amu','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_SPECIAL_TEST_V','MSI#WC#RESERVE_STOCK',401,'Descriptive flexfield: Items Column Name: Reserve Stock Context: WC','MSI#WC#Reserve_Stock','','','','SA059956','VARCHAR2','MTL_SYSTEM_ITEMS_B','ATTRIBUTE21','Msi#Wc#Reserve Stock','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_SPECIAL_TEST_V','MSI#WC#TAXWARE_CODE',401,'Descriptive flexfield: Items Column Name: Taxware Code Context: WC','MSI#WC#Taxware_Code','','','','SA059956','VARCHAR2','MTL_SYSTEM_ITEMS_B','ATTRIBUTE22','Msi#Wc#Taxware Code','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_SPECIAL_TEST_V','MSI#WC#AVERAGE_UNITS',401,'Descriptive flexfield: Items Column Name: Average Units Context: WC','MSI#WC#Average_Units','','','','SA059956','VARCHAR2','MTL_SYSTEM_ITEMS_B','ATTRIBUTE25','Msi#Wc#Average Units','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_SPECIAL_TEST_V','MSI#WC#PRODUCT_CODE',401,'Descriptive flexfield: Items Column Name: Product code Context: WC','MSI#WC#Product_code','','','','SA059956','VARCHAR2','MTL_SYSTEM_ITEMS_B','ATTRIBUTE26','Msi#Wc#Product Code','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_SPECIAL_TEST_V','MSI#WC#IMPORT_DUTY_',401,'Descriptive flexfield: Items Column Name: Import Duty % Context: WC','MSI#WC#Import_Duty_','','','','SA059956','VARCHAR2','MTL_SYSTEM_ITEMS_B','ATTRIBUTE27','Msi#Wc#Import Duty %','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_SPECIAL_TEST_V','MSI#WC#KEEP_ITEM_ACTIVE',401,'Descriptive flexfield: Items Column Name: Keep Item Active Context: WC','MSI#WC#Keep_Item_Active','','','','SA059956','VARCHAR2','MTL_SYSTEM_ITEMS_B','ATTRIBUTE29','Msi#Wc#Keep Item Active','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_SPECIAL_TEST_V','MSI#WC#PESTICIDE_FLAG',401,'Descriptive flexfield: Items Column Name: Pesticide Flag Context: WC','MSI#WC#Pesticide_Flag','','','','SA059956','VARCHAR2','MTL_SYSTEM_ITEMS_B','ATTRIBUTE3','Msi#Wc#Pesticide Flag','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_SPECIAL_TEST_V','MSI#WC#CALC_LEAD_TIME',401,'Descriptive flexfield: Items Column Name: Calc Lead Time Context: WC','MSI#WC#Calc_Lead_Time','','','','SA059956','VARCHAR2','MTL_SYSTEM_ITEMS_B','ATTRIBUTE30','Msi#Wc#Calc Lead Time','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_SPECIAL_TEST_V','MSI#WC#VOC_GL',401,'Descriptive flexfield: Items Column Name: VOC G/L Context: WC','MSI#WC#VOC_GL','','','','SA059956','VARCHAR2','MTL_SYSTEM_ITEMS_B','ATTRIBUTE4','Msi#Wc#Voc G/L','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_SPECIAL_TEST_V','MSI#WC#PESTICIDE_FLAG_STATE',401,'Descriptive flexfield: Items Column Name: Pesticide Flag State Context: WC','MSI#WC#Pesticide_Flag_State','','','','SA059956','VARCHAR2','MTL_SYSTEM_ITEMS_B','ATTRIBUTE5','Msi#Wc#Pesticide Flag State','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_SPECIAL_TEST_V','MSI#WC#VOC_CATEGORY',401,'Descriptive flexfield: Items Column Name: VOC Category Context: WC','MSI#WC#VOC_Category','','','','SA059956','VARCHAR2','MTL_SYSTEM_ITEMS_B','ATTRIBUTE6','Msi#Wc#Voc Category','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_SPECIAL_TEST_V','MSI#WC#VOC_SUB_CATEGORY',401,'Descriptive flexfield: Items Column Name: VOC Sub Category Context: WC','MSI#WC#VOC_Sub_Category','','','','SA059956','VARCHAR2','MTL_SYSTEM_ITEMS_B','ATTRIBUTE7','Msi#Wc#Voc Sub Category','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_SPECIAL_TEST_V','MSI#WC#MSDS_#',401,'Descriptive flexfield: Items Column Name: MSDS # Context: WC','MSI#WC#MSDS_#','','','','SA059956','VARCHAR2','MTL_SYSTEM_ITEMS_B','ATTRIBUTE8','Msi#Wc#Msds #','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_SPECIAL_TEST_V','MSI#WC#HAZMAT_PACKAGING_GROU',401,'Descriptive flexfield: Items Column Name: Hazmat Packaging Group Context: WC','MSI#WC#Hazmat_Packaging_Grou','','','','SA059956','VARCHAR2','MTL_SYSTEM_ITEMS_B','ATTRIBUTE9','Msi#Wc#Hazmat Packaging Group','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_SPECIAL_TEST_V','DATE_RECEIVED',401,'Date Received','DATE_RECEIVED','','','','SA059956','DATE','','','Date Received','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_SPECIAL_TEST_V','OLDEST_BORN_DATE',401,'Oldest Born Date','OLDEST_BORN_DATE','','','','SA059956','DATE','','','Oldest Born Date','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_SPECIAL_TEST_V','OPEN_SALES_ORDERS',401,'Open Sales Orders','OPEN_SALES_ORDERS','','','','SA059956','NUMBER','','','Open Sales Orders','','','');
--Inserting View Components for EIS_XXWC_INV_SPECIAL_TEST_V
xxeis.eis_rs_ins.vcomp( 'EIS_XXWC_INV_SPECIAL_TEST_V','PO_AGENTS_V',401,'PO_AGENTS','POA','POA','SA059956','SA059956','-1','Buyers Table','','','','');
xxeis.eis_rs_ins.vcomp( 'EIS_XXWC_INV_SPECIAL_TEST_V','GL_PERIOD_STATUSES',401,'GL_PERIOD_STATUSES','GPS','GPS','SA059956','SA059956','-1','Calendar Period Statuses','','','','');
xxeis.eis_rs_ins.vcomp( 'EIS_XXWC_INV_SPECIAL_TEST_V','MTL_SYSTEM_ITEMS_KFV',401,'MTL_SYSTEM_ITEMS_B','MSI','MSI','SA059956','SA059956','-1','Inventory Item Definitions','','','','');
xxeis.eis_rs_ins.vcomp( 'EIS_XXWC_INV_SPECIAL_TEST_V','MTL_CATEGORIES',401,'MTL_CATEGORIES_B','MCV','MCV','SA059956','SA059956','-1','Categories','','','','');
--Inserting View Component Joins for EIS_XXWC_INV_SPECIAL_TEST_V
xxeis.eis_rs_ins.vcj( 'EIS_XXWC_INV_SPECIAL_TEST_V','PO_AGENTS_V','POA',401,'EXISV.AGENT_ID','=','POA.AGENT_ID(+)','','','','Y','SA059956','','');
xxeis.eis_rs_ins.vcj( 'EIS_XXWC_INV_SPECIAL_TEST_V','GL_PERIOD_STATUSES','GPS',401,'EXISV.APPLICATION_ID','=','GPS.APPLICATION_ID(+)','','','','Y','SA059956','','');
xxeis.eis_rs_ins.vcj( 'EIS_XXWC_INV_SPECIAL_TEST_V','GL_PERIOD_STATUSES','GPS',401,'EXISV.SET_OF_BOOKS_ID','=','GPS.SET_OF_BOOKS_ID(+)','','','','Y','SA059956','','');
xxeis.eis_rs_ins.vcj( 'EIS_XXWC_INV_SPECIAL_TEST_V','GL_PERIOD_STATUSES','GPS',401,'EXISV.PERIOD_NAME','=','GPS.PERIOD_NAME(+)','','','','Y','SA059956','','');
xxeis.eis_rs_ins.vcj( 'EIS_XXWC_INV_SPECIAL_TEST_V','MTL_CATEGORIES','MCV',401,'EXISV.CATEGORY_ID','=','MCV.CATEGORY_ID(+)','','','','','SA059956','','');
xxeis.eis_rs_ins.vcj( 'EIS_XXWC_INV_SPECIAL_TEST_V','MTL_SYSTEM_ITEMS_KFV','MSI',401,'EXISV.INVENTORY_ITEM_ID','=','MSI.INVENTORY_ITEM_ID(+)','','','','','SA059956','','');
xxeis.eis_rs_ins.vcj( 'EIS_XXWC_INV_SPECIAL_TEST_V','MTL_SYSTEM_ITEMS_KFV','MSI',401,'EXISV.INV_ORGANIZATION_ID','=','MSI.ORGANIZATION_ID(+)','','','','','SA059956','','');
END;
/
set scan on define on
prompt Creating Report LOV Data for Specials Report Test
set scan off define off
DECLARE
BEGIN 
--Inserting Report LOVs - Specials Report Test
xxeis.eis_rs_ins.lov( 401,'SELECT ''Include Specials with Oh Hand > 7 days'' onhand_type FROM dual
UNION
SELECT ''Include Specials with Oh Hand < 7 days'' onhand_type FROM dual
union
SELECT ''ALL specials'' onhand_type FROM dual','','INV SPECIALONHAND TYPE','','XXEIS_RS_ADMIN',NULL,'N','','');
xxeis.eis_rs_ins.lov( '','SELECT ORGANIZATION_CODE WAREHOUSE,ORGANIZATION_NAME
FROM ORG_ORGANIZATION_DEFINITIONS OOD
WHERE SYSDATE < NVL(OOD.DISABLE_DATE,SYSDATE+1) AND EXISTS (SELECT 1 FROM XXEIS.EIS_ORG_ACCESS_V  WHERE organization_id = ood.organization_id )
UNION
SELECT ''All'', ''All Organizations'' FROM DUAL','','XXWC All ORG LIST','XXWC All ORG LIST','ANONYMOUS',NULL,'N','','');
END;
/
set scan on define on
prompt Creating Report Data for Specials Report Test
set scan off define off
DECLARE
BEGIN 
--Deleting Report data - Specials Report Test
xxeis.eis_rs_utility.delete_report_rows( 'Specials Report Test' );
--Inserting Report - Specials Report Test
xxeis.eis_rs_ins.r( 401,'Specials Report Test','','report on all special parts ordered; special parts on hand with or without demand; report to flag special parts to become standard parts (i.e. they are used a lot)','','','','SA059956','EIS_XXWC_INV_SPECIAL_TEST_V','Y','','','SA059956','','N','White Cap Reports','','EXCEL,','N');
--Inserting Report Columns - Specials Report Test
xxeis.eis_rs_ins.rc( 'Specials Report Test',401,'AVERAGECOST','Avg Cost','Averagecost','','~T~D~2','default','','7','N','','','','','','','','SA059956','N','N','','EIS_XXWC_INV_SPECIAL_TEST_V','','');
xxeis.eis_rs_ins.rc( 'Specials Report Test',401,'BUYER_CODE','Buyer','Buyer Code','','','default','','15','N','','','','','','','','SA059956','N','N','','EIS_XXWC_INV_SPECIAL_TEST_V','','');
xxeis.eis_rs_ins.rc( 'Specials Report Test',401,'CAT','Cat Class','Cat','','','default','','4','N','','','','','','','','SA059956','N','N','','EIS_XXWC_INV_SPECIAL_TEST_V','','');
xxeis.eis_rs_ins.rc( 'Specials Report Test',401,'DESCRIPTION','Description','Description','','','default','','3','N','','','','','','','','SA059956','N','N','','EIS_XXWC_INV_SPECIAL_TEST_V','','');
xxeis.eis_rs_ins.rc( 'Specials Report Test',401,'ONHAND','On Hand','Onhand','','~~~','default','','11','N','','','','','','','','SA059956','N','N','','EIS_XXWC_INV_SPECIAL_TEST_V','','');
xxeis.eis_rs_ins.rc( 'Specials Report Test',401,'PART_NUMBER','Part Number','Part Number','','','default','','2','N','','','','','','','','SA059956','N','N','','EIS_XXWC_INV_SPECIAL_TEST_V','','');
xxeis.eis_rs_ins.rc( 'Specials Report Test',401,'UOM','UOM','Uom','','','default','','5','N','','','','','','','','SA059956','N','N','','EIS_XXWC_INV_SPECIAL_TEST_V','','');
xxeis.eis_rs_ins.rc( 'Specials Report Test',401,'WEIGHT','Weight','Weight','','~~~','default','','10','N','','','','','','','','SA059956','N','N','','EIS_XXWC_INV_SPECIAL_TEST_V','','');
xxeis.eis_rs_ins.rc( 'Specials Report Test',401,'VENDOR_NAME','Vendor Name','Vendor Name','','','default','','9','N','','','','','','','','SA059956','N','N','','EIS_XXWC_INV_SPECIAL_TEST_V','','');
xxeis.eis_rs_ins.rc( 'Specials Report Test',401,'VENDOR_NUMBER','Vendor Number','Vendor Number','','','default','','8','N','','','','','','','','SA059956','N','N','','EIS_XXWC_INV_SPECIAL_TEST_V','','');
xxeis.eis_rs_ins.rc( 'Specials Report Test',401,'GP','GP%','Gp','','~T~D~2','default','','16','N','','','','','','','','SA059956','N','N','','EIS_XXWC_INV_SPECIAL_TEST_V','','');
xxeis.eis_rs_ins.rc( 'Specials Report Test',401,'ORGANIZATION_CODE','Org Number','Organization Code','','','default','','1','N','','','','','','','','SA059956','N','N','','EIS_XXWC_INV_SPECIAL_TEST_V','','');
xxeis.eis_rs_ins.rc( 'Specials Report Test',401,'EXTENDED_VALUE','Extended Value','Organization Code','NUMBER','~,~.~2','default','','12','Y','','','','','','','(EXISV.ONHAND)*(EXISV.AVERAGECOST)','SA059956','N','N','','EIS_XXWC_INV_SPECIAL_TEST_V','','');
xxeis.eis_rs_ins.rc( 'Specials Report Test',401,'NO_OF_ITEM_PURCHASED','No Of Times Purchased','No Of Item Purchased','','~~~','default','','17','N','','','','','','','','SA059956','N','N','','EIS_XXWC_INV_SPECIAL_TEST_V','','');
xxeis.eis_rs_ins.rc( 'Specials Report Test',401,'SELLING_PRICE','Selling Price','Selling Price','','~~~','default','','6','N','','','','','','','','SA059956','N','N','','EIS_XXWC_INV_SPECIAL_TEST_V','','');
xxeis.eis_rs_ins.rc( 'Specials Report Test',401,'OLDEST_BORN_DATE','Oldest Born Date','Oldest Born Date','','','','','13','N','','','','','','','','SA059956','N','N','','EIS_XXWC_INV_SPECIAL_TEST_V','','');
xxeis.eis_rs_ins.rc( 'Specials Report Test',401,'OPEN_SALES_ORDERS','Open Sales Orders','Open Sales Orders','','','','','14','N','','','','','','','','SA059956','N','N','','EIS_XXWC_INV_SPECIAL_TEST_V','','');
--Inserting Report Parameters - Specials Report Test
xxeis.eis_rs_ins.rp( 'Specials Report Test',401,'Organization','Organization','ORGANIZATION_CODE','IN','XXWC All ORG LIST','','VARCHAR2','Y','Y','1','','N','CONSTANT','SA059956','Y','N','','','');
xxeis.eis_rs_ins.rp( 'Specials Report Test',401,'Select Items for report','Select Items for report','','IN','INV SPECIALONHAND TYPE','''ALL specials''','VARCHAR2','N','Y','2','','N','CONSTANT','SA059956','Y','N','','','');
--Inserting Report Conditions - Specials Report Test
xxeis.eis_rs_ins.rcn( 'Specials Report Test',401,'ORGANIZATION_CODE','IN',':Organization','','','N','2','N','SA059956');
xxeis.eis_rs_ins.rcn( 'Specials Report Test',401,'','','','','AND ((:Select Items for report = ''Include Specials with Oh Hand > 7 days''
          AND  (sysdate -EXISV.date_received) >7 AnD  EXISV.ONHAND > 0)
     OR (:Select Items for report =''Include Specials with Oh Hand < 7 days''
          AND  (sysdate -EXISV.date_received) <= 7 AnD  EXISV.ONHAND > 0)
     OR (:Select Items for report =''ALL specials'')
     )
AND ( ''All'' IN (:Organization) OR (ORGANIZATION_CODE IN (:Organization)))','Y','0','','SA059956');
--Inserting Report Sorts - Specials Report Test
xxeis.eis_rs_ins.rs( 'Specials Report Test',401,'ORGANIZATION_CODE','ASC','SA059956','','');
xxeis.eis_rs_ins.rs( 'Specials Report Test',401,'PART_NUMBER','ASC','SA059956','','');
--Inserting Report Triggers - Specials Report Test
--Inserting Report Templates - Specials Report Test
--Inserting Report Portals - Specials Report Test
xxeis.eis_rs_ins.r_port( 'Specials Report Test','XXWC_PUR_TOP_RPTS','401','Specials Report Test','Specials On-Hand','OA.jsp?page=/eis/oracle/apps/xxeis/reporting/webui/EISLaunchPG&EisProduct=Reporting&Portal=Yes&mod=Inventory','','EXCEL,','CONC','N','SA059956');
--Inserting Report Dashboards - Specials Report Test
--Inserting Report Security - Specials Report Test
xxeis.eis_rs_ins.rsec( 'Specials Report Test','401','','50619',401,'SA059956','','');
xxeis.eis_rs_ins.rsec( 'Specials Report Test','401','','50924',401,'SA059956','','');
xxeis.eis_rs_ins.rsec( 'Specials Report Test','401','','51052',401,'SA059956','','');
xxeis.eis_rs_ins.rsec( 'Specials Report Test','401','','50879',401,'SA059956','','');
xxeis.eis_rs_ins.rsec( 'Specials Report Test','401','','50851',401,'SA059956','','');
xxeis.eis_rs_ins.rsec( 'Specials Report Test','401','','50852',401,'SA059956','','');
xxeis.eis_rs_ins.rsec( 'Specials Report Test','401','','50821',401,'SA059956','','');
xxeis.eis_rs_ins.rsec( 'Specials Report Test','20005','','50880',401,'SA059956','','');
xxeis.eis_rs_ins.rsec( 'Specials Report Test','401','','51029',401,'SA059956','','');
xxeis.eis_rs_ins.rsec( 'Specials Report Test','401','','50882',401,'SA059956','','');
xxeis.eis_rs_ins.rsec( 'Specials Report Test','401','','50883',401,'SA059956','','');
xxeis.eis_rs_ins.rsec( 'Specials Report Test','401','','50981',401,'SA059956','','');
xxeis.eis_rs_ins.rsec( 'Specials Report Test','401','','50855',401,'SA059956','','');
xxeis.eis_rs_ins.rsec( 'Specials Report Test','401','','50884',401,'SA059956','','');
xxeis.eis_rs_ins.rsec( 'Specials Report Test','401','','20634',401,'SA059956','','');
xxeis.eis_rs_ins.rsec( 'Specials Report Test','20005','','50900',401,'SA059956','','');
xxeis.eis_rs_ins.rsec( 'Specials Report Test','401','','50895',401,'SA059956','','');
xxeis.eis_rs_ins.rsec( 'Specials Report Test','401','','50865',401,'SA059956','','');
xxeis.eis_rs_ins.rsec( 'Specials Report Test','401','','50864',401,'SA059956','','');
xxeis.eis_rs_ins.rsec( 'Specials Report Test','401','','50849',401,'SA059956','','');
xxeis.eis_rs_ins.rsec( 'Specials Report Test','660','','50871',401,'SA059956','','');
xxeis.eis_rs_ins.rsec( 'Specials Report Test','401','','50862',401,'SA059956','','');
xxeis.eis_rs_ins.rsec( 'Specials Report Test','707','','51104',401,'SA059956','','');
xxeis.eis_rs_ins.rsec( 'Specials Report Test','401','','50990',401,'SA059956','','');
xxeis.eis_rs_ins.rsec( 'Specials Report Test','401','','50868',401,'SA059956','','');
xxeis.eis_rs_ins.rsec( 'Specials Report Test','201','','50892',401,'SA059956','','');
xxeis.eis_rs_ins.rsec( 'Specials Report Test','201','','50921',401,'SA059956','','');
--Inserting Report Pivots - Specials Report Test
xxeis.eis_rs_ins.rpivot( 'Specials Report Test',401,'Onhand By Born date and Item','1','1,0|1,2,1','1,1,0,0|None|2');
--Inserting Report Pivot Details For Pivot - Onhand By Born date and Item
xxeis.eis_rs_ins.rpivot_dtls( 'Specials Report Test',401,'Onhand By Born date and Item','ORGANIZATION_CODE','PAGE_FIELD','','','1','','');
xxeis.eis_rs_ins.rpivot_dtls( 'Specials Report Test',401,'Onhand By Born date and Item','ONHAND','DATA_FIELD','SUM','','1','','xlNormal');
xxeis.eis_rs_ins.rpivot_dtls( 'Specials Report Test',401,'Onhand By Born date and Item','PART_NUMBER','ROW_FIELD','','','2','1','');
--Inserting Report Summary Calculation Columns For Pivot- Onhand By Born date and Item
END;
/
set scan on define on
