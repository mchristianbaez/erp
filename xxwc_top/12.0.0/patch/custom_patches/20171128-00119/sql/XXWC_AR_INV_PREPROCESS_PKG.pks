CREATE OR REPLACE PACKAGE APPS.XXWC_AR_INV_PREPROCESS_PKG
IS
   /*************************************************************************
   *   $Header XXXWC_AR_INV_PREPROCESS_PKG.pkb $
   *   Module Name: XXWC_AR_INV_PREPROCESS_PKG.pkb
   *
   *   PURPOSE:   This package is used for Invoice Pre Processing
   *
   *   REVISIONS:
   *   Ver        Date         Author                Description
   *   ---------  ----------   ---------------       -------------------------
   *   1.0        09-Sep-2015  Manjula Chellappan    Initial Version
   *                                                  TMS# 20150527-00318 Forego Two AR Triggers
   * ***************************************************************************/

   g_distro_list   VARCHAR2 (75) DEFAULT 'hdsoracledevelopers@hdsupply.com';

   /* **************************************************************************
      *   Procedure Name: main
      *
      *   PURPOSE:   This procedure called from the conc prog to Preprocess the invoice transactions
      *
      *   REVISIONS:
      *   Ver        Date         Author                Description
      *   ---------  ----------   ---------------       -------------------------
      *   1.0        09-Sep-2015  Manjula Chellappan    Initial Version
      *                                                  TMS# 20150527-00318 Forego Two AR Triggers
      * ***************************************************************************/
   PROCEDURE main (errbuf        OUT VARCHAR2,
                   retcode       OUT VARCHAR2,
                   p_region   IN     VARCHAR2);

   /* **************************************************************************
      *   Procedure Name: gen_trx_number
      *
      *   PURPOSE:   This procedure has the logic from the trigger
      *              XXWC_TAXW_INVOICE_INSERT_TRG
      *   REVISIONS:
      *   Ver        Date         Author                Description
      *   ---------  ----------   ---------------       -------------------------
      *   1.0        09-Sep-2015  Manjula Chellappan    Initial Version
      *                                                  TMS# 20150527-00318 Forego Two AR Triggers
      * ***************************************************************************/
   PROCEDURE gen_trx_number (p_retcode        OUT NUMBER,
                             p_error_msg      OUT VARCHAR2,
                             p_region      IN     VARCHAR2);

   /* **************************************************************************
   *   Procedure Name: gen_gl_account
   *
   *   PURPOSE:   This procedure has the logic from the trigger
   *              XXWC_RA_INTERFACE_LINES_ALL_BI
   *
   *   REVISIONS:
   *   Ver        Date         Author                Description
   *   ---------  ----------   ---------------       -------------------------
   *   1.0        09-Sep-2015  Manjula Chellappan    Initial Version
   *                                                  TMS# 20150527-00318 Forego Two AR Triggers
   * ***************************************************************************/
   PROCEDURE gen_gl_account (p_retcode        OUT NUMBER,
                             p_error_msg      OUT VARCHAR2,
                             p_region      IN     VARCHAR2);
   /****************************************************************************************************************
      $Header XXWC_AR_FIX_PKG $
      Module Name: XXWC_AR_FIX_PKG.pks
  
      PURPOSE:    Convert existing modifiers to new pricing form
  
      REVISIONS:
      Ver        Date        Author                     Description
      ---------  ----------  ---------------    -------------------------------------------------------------------
      1.0        01/15/2018  Nancy Pahwa                 Initial Version TMS #20171128-00119
  
  *****************************************************************************************************************/
   procedure update_missing_shipto_addr(errbuf OUT VARCHAR2, retcode OUT NUMBER);

END XXWC_AR_INV_PREPROCESS_PKG;
/