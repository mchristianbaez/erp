CREATE OR REPLACE PACKAGE BODY XXEIS.EIS_XXWC_INV_PRE_REG_RPT_PKG AS
 --//============================================================================
--//  
--// Change Request 			:: Performance Issue  
--//
--// Object Usage 				:: This Object Referred by "Invoice Pre-Register report"
--//
--// Object Name         		:: XXEIS.EIS_XXWC_INV_PRE_REG_RPT_PKG
--//
--// Object Type         		:: Package Specification
--//
--// Object Description  		:: This Package will trigger in before report and insert the values into Table
--//
--// Version Control
--//============================================================================
--// Version    Author           Date          Description
--//----------------------------------------------------------------------------
--// 1.0        Siva  	      04/12/2016        Initial Build --TMS#20150921-00332 --performance tuning
--//============================================================================

PROCEDURE SET_DATE_FROM (P_DATE_FROM IN DATE) IS
--//============================================================================
--//
--// Object Name         :: SET_DATE_FROM  
--//
--// Object Usage 		 :: This Object Referred by "Invoice Pre-Register report"
--//
--// Object Type         :: PROCEDURE
--//
--// Object Description  :: This procedure gets the record based on parameter and insert into table.
--//
--// Version Control
--//============================================================================
--// Version    Author           Date          Description
--//----------------------------------------------------------------------------
--// 1.0         Siva        	04/12/2016       Initial Build --TMS#20150921-00332  --performance tuning
--//============================================================================
BEGIN
g_DATE_FROM := P_DATE_FROM;
END;

FUNCTION GET_DATE_FROM  RETURN DATE IS
--//============================================================================
--//
--// Object Name         :: GET_DATE_FROM  
--//
--// Object Usage 		 :: This Object Referred by "Invoice Pre-Register report"
--//
--// Object Type         :: FUNCTION
--//
--// Object Description  :: This function gets the record based on parameter and insert into table.
--//
--// Version Control
--//============================================================================
--// Version    Author           Date          Description
--//----------------------------------------------------------------------------
--// 1.0         Siva        	04/12/2016       Initial Build --TMS#20150921-00332 --performance tuning
--//============================================================================
BEGIN
 return G_DATE_FROM;
End;


FUNCTION Get_Charge_amount (P_HEADER_ID IN NUMBER) RETURN NUMBER IS
--//============================================================================
--//
--// Object Name         :: GET_CHARGE_AMOUNT  
--//
--// Object Usage 		 :: This Object Referred by "Invoice Pre-Register report"
--//
--// Object Type         :: FUNCTION
--//
--// Object Description  :: This function gets the record based on parameter and insert into table.
--//
--// Version Control
--//============================================================================
--// Version    Author           Date          Description
--//----------------------------------------------------------------------------
--// 1.0         Siva        	04/12/2016       Initial Build --TMS#20150921-00332 --performance tuning
--//============================================================================
   L_HASH_INDEX NUMBER;
  l_sql varchar2(32000);
  BEGIN
  g_charger_amt := NULL;
l_sql :=  'SELECT NVL (oclv.charge_amount, 0)
              FROM oe_charge_lines_v oclv
             WHERE oclv.header_id = :1';
     begin
         l_hash_index:=P_HEADER_ID;
        g_charger_amt := G_charge_amt_TAB (l_hash_index);
    exception
    when no_data_found
    THEN
    BEGIN
    EXECUTE IMMEDIATE L_SQL INTO g_charger_amt USING P_HEADER_ID;
    EXCEPTION WHEN NO_DATA_FOUND THEN
    g_charger_amt :='0';
    WHEN OTHERS THEN
    g_charger_amt :='0';
    End;

           l_hash_index:= P_HEADER_ID;
           G_charge_amt_TAB(l_hash_index) := g_charger_amt;
    end;

    return g_charger_amt;
  EXCEPTION WHEN OTHERS THEN
  g_charger_amt:=0;
      RETURN  g_charger_amt;

  End Get_Charge_amount;
  
  
  
FUNCTION GET_ORDER_LINE_COST (p_line_id IN NUMBER) RETURN NUMBER IS
--//============================================================================
--//
--// Object Name         :: GET_ORDER_LINE_COST  
--//
--// Object Usage 		 :: This Object Referred by "Invoice Pre-Register report"
--//
--// Object Type         :: FUNCTION
--//
--// Object Description  :: This function gets the record based on parameter and insert into table.
--//
--// Version Control
--//============================================================================
--// Version    Author           Date          Description
--//----------------------------------------------------------------------------
--// 1.0         Siva        	04/12/2016       Initial Build --TMS#20150921-00332 --performance tuning
--//============================================================================
   L_HASH_INDEX NUMBER;
  l_sql varchar2(32000);
  BEGIN
  g_order_line_cost := NULL;
l_sql :=  ' select  actual_cost
   
    from    (
                select  nvl(mmt.transaction_cost, mmt.actual_cost) actual_cost
                        , mmt.trx_source_line_id
                        , ol.line_id
                from    mtl_material_transactions mmt
                        , mtl_transaction_types mtt
                        , oe_order_lines_all ol
                        , oe_order_headers_all oh
                        , oe_transaction_types_tl ott
                        -- , wsh_delivery_details wdd
                where   mmt.transaction_type_id = mtt.transaction_type_id
                and     mtt.transaction_type_name in (''RMA Receipt'',''RMA Return'',''Sales Order Pick'',''Sales order issue'')
                and     ol.line_category_code = ''ORDER''
                and     ol.header_id = oh.header_id
                and     oh.order_type_id = ott.transaction_type_id
                and     ott.name not in (''COUNTER LINE''
                                        ,''COUNTER ORDER''
                                        ,''REPAIR ORDER'')
                and     ott.language = ''US''
                --and   ol.line_id = wdd.source_line_id
                --and   wdd.transaction_id = mmt.transaction_id
                and     ol.line_id = mmt.trx_source_line_id
                and     ol.line_id = mmt.source_line_id
                and     mmt.source_code = ''ORDER ENTRY''
                UNION
                select  nvl(mmt.transaction_cost, mmt.actual_cost) actual_cost
                        , mmt.trx_source_line_id
                        , ol.line_id
                from    mtl_material_transactions mmt
                        , mtl_transaction_types mtt
                        , oe_order_lines_all ol
                        , oe_order_headers_all oh
                        , oe_transaction_types_tl ott
                where   mmt.transaction_type_id = mtt.transaction_type_id
                and     mtt.transaction_type_name in (''RMA Receipt'',''RMA Return'',''Sales Order Pick'',''Sales order issue'')
                and     ol.line_category_code = ''ORDER''
                and     ol.header_id = oh.header_id
                and     oh.order_type_id = ott.transaction_type_id
                and     ott.name in (''COUNTER LINE''
                                    ,''COUNTER ORDER''
                                    ,''REPAIR ORDER'')
                and     ott.language = ''US''
                and     ol.line_id = mmt.trx_source_line_id
                UNION
                select  nvl(mmt.transaction_cost, mmt.actual_cost) actual_cost
                        , mmt.trx_source_line_id
                        , ol.line_id
                from    mtl_material_transactions mmt
                        , mtl_transaction_types mtt
                        , oe_order_lines_all ol
                where   mmt.transaction_type_id = mtt.transaction_type_id
                and     mtt.transaction_type_name in (''RMA Receipt'',''RMA Return'',''Sales Order Pick'',''Sales order issue'')
                and     mmt.trx_source_line_id = ol.line_id
                and     ol.line_category_code = ''RETURN''
            ) x1
    where   x1.line_id =:1
    and     rownum = 1';
     begin
         l_hash_index:=p_line_id;
        g_order_line_cost := G_order_line_cost_TAB (l_hash_index);
    exception
    when no_data_found
    THEN
    BEGIN
    EXECUTE IMMEDIATE L_SQL INTO g_order_line_cost USING p_line_id;
    EXCEPTION WHEN NO_DATA_FOUND THEN
    g_order_line_cost :='0';
    WHEN OTHERS THEN
    g_order_line_cost :='0';
    End;

           l_hash_index:= p_line_id;
           G_order_line_cost_TAB(l_hash_index) := g_order_line_cost;
    end;

    return g_order_line_cost;
  EXCEPTION WHEN OTHERS THEN
  g_order_line_cost:=0;
      RETURN  g_order_line_cost;

  End get_order_line_cost;
  
  
  
FUNCTION GET_LINE_MODIFIER_NAME (p_header_id NUMBER, p_line_id NUMBER) RETURN VARCHAR2 is
--//============================================================================
--//
--// Object Name         :: GET_LINE_MODIFIER_NAME  
--//
--// Object Usage 		 :: This Object Referred by "Invoice Pre-Register report"
--//
--// Object Type         :: FUNCTION
--//
--// Object Description  :: This function gets the record based on parameter and insert into table.
--//
--// Version Control
--//============================================================================
--// Version    Author           Date          Description
--//----------------------------------------------------------------------------
--// 1.0         Siva        	04/12/2016       Initial Build --TMS#20150921-00332 --performance tuning
--//============================================================================
   L_HASH_INDEX NUMBER;
  l_sql varchar2(32000);
  l_sql2 varchar2(32000);
  l_price_adjustment_id number;
  BEGIN
  G_line_modifier_name := NULL;
l_sql :=  'SELECT MAX (price_adjustment_id)
           FROM oe_price_adjustments_v adj1
           WHERE  adj1.header_id =:1
            AND adj1.line_id = :2
            AND adj1.list_line_type_code =''DIS''
            AND adj1.applied_flag = ''Y''';
  l_sql2 :='SELECT MAX (adjustment_name)   
        FROM oe_price_adjustments_v adj
       WHERE     adj.header_id = :1
             AND adj.line_id = :2
             AND price_adjustment_id=:3';
         BEGIN
            EXECUTE IMMEDIATE L_SQL INTO l_price_adjustment_id USING p_header_id,p_line_id;
          End;
          
     begin
     dbms_output.put_line('l_price_adjustment_id 1 '||l_price_adjustment_id);
         l_hash_index:=l_price_adjustment_id;
        G_line_modifier_name := G_line_modifier_name_tab(l_hash_index);
    exception
    when no_data_found
    THEN
    begin
    EXECUTE IMMEDIATE l_sql2 INTO G_line_modifier_name USING p_header_id,p_line_id,l_price_adjustment_id;
    EXCEPTION when NO_DATA_FOUND then
    G_line_modifier_name :=null;
    when OTHERS then
    G_LINE_MODIFIER_NAME :=null;
    End;
           l_hash_index:= l_price_adjustment_id;
           G_LINE_MODIFIER_NAME_TAB(L_HASH_INDEX) := G_LINE_MODIFIER_NAME;
           dbms_output.put_line('G_LINE_MODIFIER_NAME 1 '||G_LINE_MODIFIER_NAME);
    end;
    return G_line_modifier_name;
  EXCEPTION WHEN OTHERS THEN
  G_line_modifier_name:=null;
      RETURN  G_line_modifier_name;

  End get_line_modifier_name;
 

FUNCTION GET_SPECIAL_MODIFER(P_HEADER_ID IN NUMBER,
                              P_LINE_ID IN NUMBER,
                              P_SEGMENT IN VARCHAR2,
                              P_TYPE IN VARCHAR2) return varchar2 is
--//============================================================================
--//
--// Object Name         :: GET_SPECIAL_MODIFER  
--//
--// Object Usage 		 :: This Object Referred by "Invoice Pre-Register report"
--//
--// Object Type         :: FUNCTION
--//
--// Object Description  :: This function gets the record based on parameter and insert into table.
--//
--// Version Control
--//============================================================================
--// Version    Author           Date          Description
--//----------------------------------------------------------------------------
--// 1.0         Siva        	04/12/2016       Initial Build --TMS#20150921-00332 --performance tuning
--//============================================================================
   L_HASH_INDEX NUMBER;
  l_sql varchar2(32000);
  BEGIN
  g_charger_amt := NULL;
l_sql :=  'SELECT xxcpl.special_cost,qlh.name
              FROM apps.oe_price_adjustments adj
                  ,apps.qp_list_lines qll
                  ,apps.qp_list_headers qlh
                  ,apps.xxwc_om_contract_pricing_hdr xxcph
                  ,apps.xxwc_om_contract_pricing_lines xxcpl
                 
             WHERE     xxcph.agreement_id = TO_CHAR (qlh.attribute14)
                   AND xxcpl.agreement_id = xxcph.agreement_id
                   AND xxcpl.agreement_type = ''VQN''
                   AND qlh.list_header_id = qll.list_header_id
                   AND qlh.list_header_id = adj.list_header_id
                   AND qll.list_line_id = adj.list_line_id
                   AND adj.list_line_type_code = ''DIS''
                   AND adj.applied_flag = ''Y''
                   AND adj.header_id =:1
                   AND adj.line_id = :2
                  
                   AND xxcpl.product_value = :3
                   AND xxcpl.agreement_line_id = TO_CHAR (qll.attribute2)
                   AND ROWNUM = 1';
     begin
         l_hash_index:=P_HEADER_ID||p_line_id;
         if p_type='SPECIAL' then
        G_special_cost:= G_special_cost_tab(l_hash_index);
        Else
            g_modifier_name:= g_modifier_name_tab(l_hash_index);
          End if;
         
  
    exception
    when no_data_found
    THEN
    BEGIN
    EXECUTE IMMEDIATE L_SQL INTO G_special_cost,g_modifier_name USING P_HEADER_ID,P_line_id,P_SEGMENT;
    EXCEPTION WHEN NO_DATA_FOUND THEN
      if P_TYPE='SPECIAL' then
        G_special_cost:= null;
        Else
            g_modifier_name:= null;
          End if;
    WHEN OTHERS THEN
   if P_TYPE='SPECIAL' then
        G_special_cost:= null;
        Else
            g_modifier_name:= null;
          End if;
    End;

           l_hash_index:=P_HEADER_ID||p_line_id;
            if p_type='SPECIAL' then
        G_special_cost_tab(l_hash_index) :=G_special_cost;
        Else
           g_modifier_name_tab(l_hash_index)  := g_modifier_name;
          End if;
          
    end;

 if p_type='SPECIAL' then
       return G_special_cost;
        Else
         return    g_modifier_name;
          End if;
  EXCEPTION WHEN OTHERS THEN
   if p_type='SPECIAL' then
        G_special_cost:= 0;
          return G_special_cost;
        Else
            g_modifier_name:= null;
             return    g_modifier_name;
          End if;

  End Get_Special_Modifer;
  
  Function GET_CUSTOMER_NUMBER(P_sold_to_org_id in NUMBER,P_type in varchar2) return varchar2 is
--//============================================================================
--//
--// Object Name         :: GET_CUSTOMER_NUMBER  
--//
--// Object Usage 		 :: This Object Referred by "Invoice Pre-Register report"
--//
--// Object Type         :: FUNCTION
--//
--// Object Description  :: This function gets the record based on parameter and insert into table.
--//
--// Version Control
--//============================================================================
--// Version    Author           Date          Description
--//----------------------------------------------------------------------------
--// 1.0         Siva        	04/12/2016       Initial Build --TMS#20150921-00332 --performance tuning
--//============================================================================
   L_HASH_INDEX NUMBER;
  l_sql varchar2(32000);
  BEGIN
g_cust_number  := null;
g_cust_name := null;
l_sql :=  'select cust_acct.account_number ,NVL (cust_acct.account_name, party.party_name)
          from
          hz_parties party
         ,hz_cust_accounts cust_acct
         where   cust_acct.party_id = party.party_id
         and  :1 = cust_acct.cust_account_id';
     begin
         l_hash_index:=P_sold_to_org_id;
         if p_type='ACCOUNTNUM' then
        g_cust_number:= g_cust_account_num_tab(l_hash_index);
        Else
            g_cust_name:= g_cust_name_tab(l_hash_index);
          End if;
         
  
    exception
    when no_data_found
    THEN
    BEGIN
    EXECUTE IMMEDIATE L_SQL INTO g_cust_number,g_cust_name USING P_sold_to_org_id;
    EXCEPTION WHEN NO_DATA_FOUND THEN
      if p_type='ACCOUNTNUM' then
        g_cust_number:= 0;
        Else
            g_cust_name:= null;
          End if;
    WHEN OTHERS THEN
  if p_type='ACCOUNTNUM' then
        g_cust_number:= 0;
        Else
            g_cust_name:= null;
          End if;
    End;

           l_hash_index:=P_sold_to_org_id;
            if p_type='ACCOUNTNUM' then
        g_cust_account_num_tab(l_hash_index) :=g_cust_number;
        Else
           g_cust_name_tab(l_hash_index)  := g_cust_name;
          End if;
          
    end;

 if p_type='ACCOUNTNUM' then
       return g_cust_number;
        Else
         return    g_cust_name;
          End if;
  EXCEPTION WHEN OTHERS THEN
   if p_type='ACCOUNTNUM' then
        g_cust_number:= 0;
          return g_cust_number;
        Else
            g_cust_name:= null;
             return    g_cust_name;
          End if;

  End Get_Customer_number;
  
 
  
  
Function GET_PARTY_SITE_NUM_F (P_invoice_to_org_id in NUMBER) return varchar2 is 
  --//============================================================================
--//
--// Object Name         :: GET_PARTY_SITE_NUM_F  
--//
--// Object Usage 		 :: This Object Referred by "Invoice Pre-Register report"
--//
--// Object Type         :: FUNCTION
--//
--// Object Description  :: This function gets the record based on parameter and insert into table.
--//
--// Version Control
--//============================================================================
--// Version    Author           Date          Description
--//----------------------------------------------------------------------------
--// 1.0         Siva        	04/12/2016       Initial Build --TMS#20150921-00332 --performance tuning
--//============================================================================
   L_HASH_INDEX NUMBER;
  l_sql varchar2(32000);
  BEGIN
  g_party_site_number := NULL;
l_sql :=  'SELECT  hps.party_site_number
              FROM hz_cust_site_uses_all hcsu                                                      
                  ,hz_cust_acct_sites_all hcas
                  ,hz_party_sites hps
             WHERE   hcsu.site_use_id =:1                                      
          AND hcas.cust_acct_site_id = hcsu.cust_acct_site_id
          AND hps.party_site_id = hcas.party_site_id';
     begin
         l_hash_index:=P_invoice_to_org_id;
        g_party_site_number := g_party_site_num (l_hash_index);
    exception
    when no_data_found
    THEN
    BEGIN
    EXECUTE IMMEDIATE L_SQL INTO g_party_site_number USING P_invoice_to_org_id;
    EXCEPTION WHEN NO_DATA_FOUND THEN
    g_party_site_number :='0';
    WHEN OTHERS THEN
    g_party_site_number :='0';
    End;

           l_hash_index:= P_invoice_to_org_id;
           g_party_site_num(l_hash_index) := g_party_site_number;
    end;

    return g_party_site_number;
  EXCEPTION WHEN OTHERS THEN
  g_party_site_number:=0;
      RETURN  g_party_site_number;

  End Get_Party_Site_num_f;

   
  FUNCTION GET_DISCOUNT_AMT(P_HEADER_ID number, P_LINE_ID number) RETURN NUMBER
IS
  --//============================================================================
--//
--// Object Name         :: GET_DISCOUNT_AMT  
--//
--// Object Usage 		 :: This Object Referred by "Invoice Pre-Register report"
--//
--// Object Type         :: FUNCTION
--//
--// Object Description  :: This function gets the record based on parameter and insert into table.
--//
--// Version Control
--//============================================================================
--// Version    Author           Date          Description
--//----------------------------------------------------------------------------
--// 1.0         Siva        	04/12/2016       Initial Build --TMS#20150921-00332 --performance tuning
--//============================================================================
L_HASH_INDEX varchar2(100);
L_SQL varchar2(32000);
begin
G_DISCOUNT_AMT := NULL;
--      SELECT employee_id
--        INTO l_employee_id
--        FROM fnd_user
--       WHERE user_id = p_employee_id;

l_sql :='       SELECT SUM (NVL (adjusted_amount, 0))
        FROM oe_price_adjustments_v
       WHERE     list_line_type_code = ''DIS''
             AND header_id = :1
             AND line_id = NVL (:2, line_id)';
    BEGIN
        l_hash_index:=P_HEADER_ID||'-'||P_LINE_ID;
        G_DISCOUNT_AMT := G_DISCOUNT_AMT_VLDN_TBL(L_HASH_INDEX);
 exception
    when no_data_found
    THEN
    begin      
    EXECUTE IMMEDIATE L_SQL INTO G_DISCOUNT_AMT USING p_header_id,P_LINE_ID;
    EXCEPTION WHEN NO_DATA_FOUND THEN
    G_DISCOUNT_AMT :=null;
    WHEN OTHERS THEN
    G_DISCOUNT_AMT :=null;
    END;      
                      l_hash_index:=p_header_id||'-'||P_LINE_ID;
                      G_DISCOUNT_AMT_VLDN_TBL(L_HASH_INDEX) := G_DISCOUNT_AMT;
    END;
     return  G_DISCOUNT_AMT;
     EXCEPTION WHEN OTHERS THEN
      G_DISCOUNT_AMT:=0;
      RETURN  G_DISCOUNT_AMT;

end get_discount_amt;

  FUNCTION GET_FREIGHT_AMT(P_HEADER_ID number, P_LINE_ID number) RETURN NUMBER
IS
  --//============================================================================
--//
--// Object Name         :: GET_FREIGHT_AMT  
--//
--// Object Usage 		 :: This Object Referred by "Invoice Pre-Register report"
--//
--// Object Type         :: FUNCTION
--//
--// Object Description  :: This function gets the record based on parameter and insert into table.
--//
--// Version Control
--//============================================================================
--// Version    Author           Date          Description
--//----------------------------------------------------------------------------
--// 1.0         Siva        	04/12/2016       Initial Build --TMS#20150921-00332 --performance tuning
--//============================================================================
L_HASH_INDEX varchar2(100);
L_SQL varchar2(32000);
begin
G_FREIGHT_AMT := NULL;
--      SELECT employee_id
--        INTO l_employee_id
--        FROM fnd_user
--       WHERE user_id = p_employee_id;

l_sql :='       SELECT SUM (NVL (adjusted_amount, 0))
        FROM oe_price_adjustments_v
       WHERE     list_line_type_code = ''DIS''
             AND header_id = :1
             AND line_id = NVL (:2, line_id)';
    BEGIN
        l_hash_index:=P_HEADER_ID||'-'||P_LINE_ID;
        G_FREIGHT_AMT := G_FREIGHT_AMT_VLDN_TBL(L_HASH_INDEX);
 exception
    when no_data_found
    THEN
    begin      
    EXECUTE IMMEDIATE L_SQL INTO G_FREIGHT_AMT USING p_header_id,P_LINE_ID;
    EXCEPTION WHEN NO_DATA_FOUND THEN
    G_FREIGHT_AMT :=null;
    WHEN OTHERS THEN
    G_FREIGHT_AMT :=null;
    END;      
                      l_hash_index:=p_header_id||'-'||P_LINE_ID;
                      G_FREIGHT_AMT_VLDN_TBL(L_HASH_INDEX) := G_FREIGHT_AMT;
    END;
     return  G_FREIGHT_AMT;
     EXCEPTION WHEN OTHERS THEN
      G_FREIGHT_AMT:=0;
      RETURN  G_FREIGHT_AMT;

end get_freight_amt;
 

END EIS_XXWC_INV_PRE_REG_RPT_PKG;
/
