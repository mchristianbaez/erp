---------------------------------------------------------------------------------------------------------------------------
/*************************************************************************************************************************
  $Header XXWC_OE_ORDER_LN_AL_N13
  File Name: XXWC_OE_ORDER_LN_AL_N13.sql
  PURPOSE:   
  REVISIONS:
     Ver        Date         Author         Description
     ---------  ----------   --------    -----------------------------------------------------------------------------------
     1.0        04-Apr-2016  Siva        --TMS#20150921-00332 Invoice Pre-Register report - Program Running for longer time
****************************************************************************************************************************/
CREATE INDEX ONT.XXWC_OE_ORDER_LN_AL_N13
 ON ONT.OE_ORDER_LINES_ALL (SOURCE_TYPE_CODE,FLOW_STATUS_CODE,HEADER_ID) TABLESPACE APPS_TS_TX_DATA
/
 