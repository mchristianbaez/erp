---------------------------------------------------------------------------------------------------------------------------
/*************************************************************************************************************************
  $Header XXWC_OE_ORDER_HEADERS_ALL_N8
  File Name: XXWC_OE_ORDER_HEADERS_ALL_N8.sql
  PURPOSE:   
  REVISIONS:
     Ver        Date         Author         Description
     ---------  ----------   --------    -----------------------------------------------------------------------------------
     1.0        04-Apr-2016  Siva        --TMS#20150921-00332 Invoice Pre-Register report - Program Running for longer time
****************************************************************************************************************************/
CREATE INDEX ONT.XXWC_OE_ORDER_HEADERS_ALL_N8
 ON ONT.OE_ORDER_HEADERS_ALL (TRUNC(ORDERED_DATE)) TABLESPACE APPS_TS_TX_DATA
/
 