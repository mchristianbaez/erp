/******************************************************************************
  $Header TMS_20170217-00084_XXWC_DROP_OE_MSGS_TBLS.sql $
  Module Name: 20170217-00084  Data Fix script for 20170217-00084

  PURPOSE: Data fix script for 20170217-00084 This script will drop the backup
           tables which we have taken backup while deleting the OE workflow 
		   messages from oe workflow --No permanent fix in process 
		  (But patch available per oracle)

  REVISIONS:
  Ver        Date         Author                Description
  ---------  -----------  ------------------    -------------------------------
  1.0        16-AUG-2017  Pattabhi Avula        TMS#20170217-00084

*******************************************************************************/
DROP TABLE XXWC.xxwc_oe_process_msgs_t_bkup14;
DROP TABLE XXWC.xxwc_oe_process_msgs_bkup14;
DROP TABLE XXWC.XXWC_OE_PROCESS_MSGS_BKUP;
DROP TABLE XXWC.XXWC_OE_PROCESS_MSGS_T_BKUP;
/
