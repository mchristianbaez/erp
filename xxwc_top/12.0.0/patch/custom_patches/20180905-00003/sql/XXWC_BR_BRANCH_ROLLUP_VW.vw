CREATE OR REPLACE VIEW apps.XXWC_BR_BRANCH_ROLLUP_VW
/******************************************************************************
--   NAME:       apps.XXWC_BR_BRANCH_ROLLUP_VW
--   PURPOSE:  Branch Rollup
-- REVISIONS:
--   Ver        Date        Author           Description
--   ---------  ----------  ---------------  ------------------------------------
--   1.0        04/06/2018   Nancy Pahwa       Added the union statement
--   2.0        04/06/2018   Nancy Pahwa       Change the where clause
--   3.0        09/05/2018   P.Vamshidhar      TMS#20180905-00003 -  XXWC_BR_BRANCH_ROLLUP_VW view should show only active orgs
******************************************************************************/
(
   BR_NUMBER,
   BR_ORG_ID,
   BR_NAME,
   BR_TYPE,
   MARKET_CODE,
   MARKET_NAME,
   DIST_CODE,
   DIST_NAME,
   REGION_CODE,
   REGION_NAME,
   BM_NAME,
   BM_EEID,
   BR_STATUS
)
AS
   SELECT b.organization_code,
          b.organization_id,
          b.organization_name,
          c.attribute7,
          a.SYM_NM5,
          a.SYM_DESC_E5,
          a.sym_nm6,
          a.sym_desc_e6,
          a.sym_nm4,
          a.SYM_DESC_E4,
          d.resource_name,
          c.attribute13,
          'A'
     FROM xxwc.XXWC_LONGVIEW_BRANCH_HIERARCHY a,
          apps.org_organization_definitions b,
          apps.mtl_parameters c,
          apps.jtf_rs_defresources_v d
    WHERE     b.organization_id = c.organization_id
          AND c.attribute8 = a.sym_desc_e6
          AND c.attribute9 = a.sym_desc_e4
          AND d.source_id(+) = c.attribute13
          AND a.leaf_fru = c.attribute10
		  AND nvl(b.disable_date,SYSDATE+1)>=SYSDATE 
UNION       
SELECT SUBSTR(SYM_DESC_E7,LENGTH(SYM_DESC_E7)-2) organization_code, 
       null organization_id,
       a.SYM_DESC_E7 organization_name,
       'Store' attribute7,
       a.SYM_NM5,
       a.SYM_DESC_E5,
       a.sym_nm6,
       a.sym_desc_e6,
       a.sym_nm4,
       a.SYM_DESC_E4,
       NULL resource_name,
       NULL attribute13,
       'A'
  FROM xxwc.XXWC_LONGVIEW_BRANCH_HIERARCHY a  
 WHERE (SYM_DESC_E7 like '%(AHH)%'OR
SYM_DESC_E7 like '%(KCP)%'OR
SYM_DESC_E7 like '%(HM)%')
AND NOT EXISTS (SELECT '1' FROM apps.org_organization_definitions c WHERE SUBSTR(SYM_DESC_E7,LENGTH(SYM_DESC_E7)-2) = c.organization_code and NVL(disable_date,sysdate)>= sysdate);
/