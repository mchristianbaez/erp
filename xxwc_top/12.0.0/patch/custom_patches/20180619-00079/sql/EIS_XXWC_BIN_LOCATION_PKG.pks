CREATE OR REPLACE PACKAGE XXEIS.EIS_XXWC_BIN_LOCATION_PKG
AS
--//============================================================================
--//  
--// Change Request 			:: Performance Issue  
--//
--// Object Usage 				:: This Object Referred by "Bin location"
--//
--// Object Name         		:: XXEIS.EIS_XXWC_BIN_LOCATION_PKG
--//
--// Object Type         		:: Package Specification
--//
--// Object Description  		:: This Package will trigger in before report and insert the values into Table
--//
--// Version Control
--//============================================================================
--// Version    Author           Date          Description
--//----------------------------------------------------------------------------
--// 1.0        Siva  	      18-may-2016       Initial Build TMS#20160429-00037
--//  1.1		 Siva			17-Jun-2016		 TMS#20160602-00001	
--// 1.2     	Siva 			15-Mar-2017     TMS#20170224-00065
--// 1.3	  	Siva			13-Feb-2018		TMS#20180125-00274 
--// 1.4      	Siva			 4-Jul-2018  	TMS#20180619-00079
--//============================================================================

/*  
FUNCTION get_last_received_date(
      p_inventory_item_id IN NUMBER,
      p_organization_id   IN NUMBER)
    RETURN DATE;
--//============================================================================
--//
--// Object Name         :: get_last_received_date  
--//
--// Object Usage 		 :: This Object Referred by "Bin Location"
--//
--// Object Type         :: FUNCTION
--//
--// Object Description  :: This function gets the record based on parameter and insert into table.
--//
--// Version Control
--//============================================================================
--// Version    Author           Date          Description
--//----------------------------------------------------------------------------
--// 1.0         Siva        	18-May-2016       Initial Build 
--//============================================================================	
  FUNCTION GET_ITEM_COST(
      P_NUM               IN NUMBER,
      P_INVENTORY_ITEM_ID IN NUMBER,
      P_ORGANIZATION_ID   IN NUMBER)
    RETURN NUMBER;
--//============================================================================
--//
--// Object Name         :: GET_ITEM_COST  
--//
--// Object Usage 		 :: This Object Referred by "Bin Location"
--//
--// Object Type         :: FUNCTION
--//
--// Object Description  :: This function gets the record based on parameter and insert into table.
--//
--// Version Control
--//============================================================================
--// Version    Author           Date          Description
--//----------------------------------------------------------------------------
--// 1.0         Siva        	18-May-2016       Initial Build 
--//============================================================================	
*/ --commented for version 1.1  

  FUNCTION GET_PRIMARY_BIN_LOC(
      P_INVENTORY_ITEM_ID IN NUMBER,
      P_ORGANIZATION_ID   IN NUMBER)
    RETURN VARCHAR2;
--//============================================================================
--//
--// Object Name         :: GET_PRIMARY_BIN_LOC  
--//
--// Object Usage 		 :: This Object Referred by "Bin Location"
--//
--// Object Type         :: FUNCTION
--//
--// Object Description  :: This function gets the record based on parameter and insert into table.
--//
--// Version Control
--//============================================================================
--// Version    Author           Date          Description
--//----------------------------------------------------------------------------
--// 1.0         Siva        	18-May-2016       Initial Build 
--//============================================================================	

/*
  FUNCTION GET_ALTERNATE_BIN_LOC(
      P_INVENTORY_ITEM_ID IN NUMBER,
      P_ORGANIZATION_ID   IN NUMBER)
    RETURN VARCHAR2;
--//============================================================================
--//
--// Object Name         :: GET_ALTERNATE_BIN_LOC  
--//
--// Object Usage 		 :: This Object Referred by "Bin Location"
--//
--// Object Type         :: FUNCTION
--//
--// Object Description  :: This function gets the record based on parameter and insert into table.
--//
--// Version Control
--//============================================================================
--// Version    Author           Date          Description
--//----------------------------------------------------------------------------
--// 1.0         Siva        	18-May-2016       Initial Build 
--//============================================================================	
*/ --commented for version 1.1  

  FUNCTION GET_START_BIN
    RETURN VARCHAR2;
--//============================================================================
--//
--// Object Name         :: GET_START_BIN  
--//
--// Object Usage 		 :: This Object Referred by "Bin Location"
--//
--// Object Type         :: FUNCTION
--//
--// Object Description  :: This function gets the record based on parameter and insert into table.
--//
--// Version Control
--//============================================================================
--// Version    Author           Date          Description
--//----------------------------------------------------------------------------
--// 1.0         Siva        	18-May-2016       Initial Build 
--//============================================================================	
  FUNCTION GET_END_BIN
    RETURN VARCHAR2;
--//============================================================================
--//
--// Object Name         :: GET_END_BIN  
--//
--// Object Usage 		 :: This Object Referred by "Bin Location"
--//
--// Object Type         :: FUNCTION
--//
--// Object Description  :: This function gets the record based on parameter and insert into table.
--//
--// Version Control
--//============================================================================
--// Version    Author           Date          Description
--//----------------------------------------------------------------------------
--// 1.0         Siva        	18-May-2016       Initial Build 
--//============================================================================	
 
 FUNCTION GET_ALTERNATE_BIN_LOC(
      P_INVENTORY_ITEM_ID IN NUMBER,
      P_ORGANIZATION_ID   IN NUMBER)
    RETURN VARCHAR2;
--//============================================================================
--//
--// Object Name         :: GET_ALTERNATE_BIN_LOC
--//
--// Object Usage 		 :: This Object Referred by "Bin Location"
--//
--// Object Type         :: FUNCTION
--//
--// Object Description  :: This function gets the record based on parameter and insert into table.
--//
--// Version Control
--//============================================================================
--// Version    Author           Date          Description
--//----------------------------------------------------------------------------
--// 1.0         Siva        	4-Jul-2018       20180619-00079 
--//============================================================================
  PROCEDURE get_bin_location_info(
      p_process_id       IN NUMBER,
      p_category_from    IN VARCHAR2 DEFAULT NULL,
      p_category_to      IN VARCHAR2 DEFAULT NULL,
      p_organization     IN VARCHAR2 DEFAULT NULL,
      p_category_set     IN VARCHAR2 DEFAULT NULL,
      p_quantity_on_hand IN VARCHAR2,
      p_subinventory 	 in varchar2 default null); --added for version 1.2
--//============================================================================
--//
--// Object Name         :: get_bin_location_info  
--//
--// Object Usage 		 :: This Object Referred by "Bin lOcation"
--//
--// Object Type         :: Procedure
--//
--// Object Description  :: This Procedure will call in before report and insert the values into Table
--//
--// Version Control
--//============================================================================
--// Version    Author           Date          Description
--//----------------------------------------------------------------------------
--// 1.0     Siva  			05-May-2016 		Initial Build  
--//  1.1		 Siva			17-Jun-2016		 TMS#20160602-00001	
--// 1.2	  	Siva			13-Feb-2018		TMS#20180125-00274  
--//============================================================================	  
type t_cashtype_vldn_tbl
IS
  TABLE OF VARCHAR2(100) INDEX BY VARCHAR2(100);
type t_number_tab
IS
  TABLE OF NUMBER INDEX BY binary_integer;
  g_item_cost_vldn_tbl t_cashtype_vldn_tbl;
  g_primary_bin_loc_vldn_tbl t_cashtype_vldn_tbl;
  g_alternate_bin_loc_vldn_tbl t_cashtype_vldn_tbl;
  g_last_received_date_vldn_tbl t_cashtype_vldn_tbl;
  g_item_cost          NUMBER;
  G_PRIMARY_BIN_LOC    VARCHAR2(200);
  G_ALTERNATE_BIN_LOC  VARCHAR2(200);
  G_LAST_RECEIVED_DATE DATE;
  G_START_BIN          VARCHAR2(100);
  g_end_bin            VARCHAR2(100);
type CURSOR_TYPE4
IS
  ref
  CURSOR;

/* --Commented code for version 1.2  
    PROCEDURE CLEAR_TEMP_TABLES(
        P_PROCESS_ID IN NUMBER);
--//============================================================================
--//
--// Object Name         :: CLEAR_TEMP_TABLES  
--//
--// Object Usage 		 :: This Object Referred by "Bin Location"
--//
--// Object Type         :: Procedure
--//
--// Object Description  :: This Procedure will call in Afteer report and delete data from Table
--//
--// Version Control
--//============================================================================
--// Version    Author           Date          Description
--//----------------------------------------------------------------------------
--// 1.0     Siva  				05-May-2016 	Initial Build   
--//============================================================================	
*/ --Commented code for version 1.2
		
  END EIS_XXWC_BIN_LOCATION_PKG;
/
