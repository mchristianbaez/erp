---------------------------------------------------------------------------------------------------------------------------
/*************************************************************************************************************************
  File Name: EIS_XXWC_BIN_LOC_DATA_TAB.sql
  PURPOSE:   
  REVISIONS:
		Ver        Date         Author         Description
     ---------  ----------   --------    -----------------------------------------------------------------------------------
		1.0      4-Jul-2018  	Siva        --TMS#20180619-00079
****************************************************************************************************************************/
ALTER TABLE xxeis.EIS_XXWC_BIN_LOC_DATA_TAB ADD (bin0 VARCHAR2(4000),bin1 VARCHAR2(4000),bin2 VARCHAR2(4000),bin3 VARCHAR2(4000))
/

