/*************************************************************************
  $Header TMS_20160906-00247_AWAITING_INVOICE_TO_CLOSE.sql $
  Module Name: TMS_20160906-00247  Data Fix script 

  PURPOSE: Data fix script for Stuck awaiting receipt

  REVISIONS:
  Ver        Date         Author                Description
  ---------  -----------  ------------------    --------------------------
  1.0        03-OCT-2016  Pattabhi Avula        TMS#20160906-00247

**************************************************************************/ 

SET SERVEROUTPUT ON SIZE 100000;
SET VERIFY OFF;

DECLARE
BEGIN
   DBMS_OUTPUT.put_line ('TMS: 20160906-00247    , Before Update');

update apps.oe_order_lines_all
set FLOW_STATUS_CODE='CANCELLED',
CANCELLED_FLAG='Y'
where line_id = 77120883
and header_id= 47136285;

   DBMS_OUTPUT.put_line (
         'TMS: 20160906-00247  Sales order lines updated (Expected:1): '
      || SQL%ROWCOUNT);

   COMMIT;

   DBMS_OUTPUT.put_line ('TMS: 20160906-00247    , End Update');
EXCEPTION
   WHEN OTHERS
   THEN
      ROLLBACK;
      DBMS_OUTPUT.put_line ('TMS: 20160906-00247 , Errors : ' || SQLERRM);
END;
/