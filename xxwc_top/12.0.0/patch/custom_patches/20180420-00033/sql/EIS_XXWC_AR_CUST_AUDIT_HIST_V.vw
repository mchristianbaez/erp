-------------------------------------------------------------------------------------------
/*****************************************************************************************
  $Header XXEIS.EIS_XXWC_AR_CUST_AUDIT_HIST_V.vw $
  Module Name: Receivables
  PURPOSE: Customer Audit History Report - WC
  REVISIONS:
  Ver          Date        Author                Description
  ---------  ----------  ------------------    ----------------
  1.0		 11/22/2017      Siva				TMS#20170303-00041
  1.1		 4/23/2018		 Siva				TMS#20180420-00033 
******************************************************************************************/
---------------------------------------------------------------------------------------------
CREATE OR REPLACE VIEW XXEIS.EIS_XXWC_AR_CUST_AUDIT_HIST_V (CREDIT_REPORT_FLAG, TRUNC_AUDIT_DATE, AUDIT_DATE, AUDIT_TYPE, USER_NTID, USER_NAME, ACCOUNT_NUMBER, ACCOUNT_NAME, SITE_NUMBER, SITE_NAME, ATRIBUTE_NAME, OLD_VALUE, NEW_VALUE, TABLE_NAME, DESCRIPTION, PROFILE_CLASS)
AS
  SELECT 'No' credit_report_flag,
    TRUNC(Hcaa.Audit_Timestamp) trunc_Audit_Date,
    Hcaa.Audit_Timestamp Audit_Date,
    DECODE(Hcaa.Audit_Transaction_Type,'U','Update') Audit_Type,
    Hcaa.Audit_User_Name User_Ntid,
    NVL(
    (SELECT Ppf.Full_Name
    FROM Fnd_User Fu,
      Per_All_People_F Ppf
    WHERE Fu.User_Name=Hcaa.Audit_User_Name
    AND Fu.Employee_Id=Ppf.person_id
    AND TRUNC(Sysdate) BETWEEN Ppf.Effective_Start_Date AND Ppf.Effective_End_Date
	AND rownum      =1 --added for version 1.1
    ),Hcaa.Audit_User_Name) User_Name,
    Hca.ACCOUNT_Number,
    HCA.ACCOUNT_NAME,
    HPS.PARTY_SITE_NUMBER site_number,
    HCSU.location SITE_NAME,
    'Customer Source' ATRIBUTE_NAME,
    HCAAV2.ATTRIBUTE4 OLD_VALUE,
    xxeis.EIS_XXWC_CUST_AUDIT_HIST_PKG.get_cust_new_value('APPS.HZ_CUST_ACCOUNTS_AV7','ATTRIBUTE4',HCAA.ROW_KEY,HCAAV2.CUST_ACCOUNT_ID,'CUST_ACCOUNT_ID') new_value,
    'HZ_CUST_ACCOUNTS' TABLE_NAME,
    (SELECT DESCRIPTION
    FROM FND_TABLES
    WHERE TABLE_NAME='HZ_CUST_ACCOUNTS'
    AND rownum      =1
    ) DESCRIPTION,
    DECODE(HCSU.site_use_id,NULL,
    (SELECT hcpcs.name
    FROM HZ_CUSTOMER_PROFILES hcpf,
      HZ_CUST_PROFILE_CLASSES hcpcs
    WHERE hcpf.profile_class_id=hcpcs.profile_class_id
    AND hcpf.cust_account_id   = HCA.CUST_ACCOUNT_ID
    AND hcpf.SITE_USE_ID      IS NULL
    AND rownum                 =1
    ),
    (SELECT hcpcs.name
    FROM HZ_CUSTOMER_PROFILES hcpf,
      HZ_CUST_PROFILE_CLASSES hcpcs
    WHERE hcpf.profile_class_id=hcpcs.profile_class_id
    AND hcpf.site_use_id       = HCSU.site_use_id
    AND hcpf.cust_account_id   = HCA.CUST_ACCOUNT_ID
    AND rownum                 =1
    )) PROFILE_CLASS
  FROM APPS.HZ_CUST_ACCOUNTS_A HCAA,
    APPS.HZ_CUST_ACCOUNTS_AV7 HCAAV2,
    APPS.HZ_CUST_ACCOUNTS HCA ,
    HZ_CUST_ACCT_SITES_ALL HCS,
    HZ_CUST_SITE_USES_ALL HCSU,
    Hz_Party_Sites Hps
  WHERE HCAA.ROW_KEY             = HCAAV2.ROW_KEY
  AND HCAAV2.CUST_ACCOUNT_ID     = HCA.CUST_ACCOUNT_ID
  AND HCA.CUST_ACCOUNT_ID        = HCS.CUST_ACCOUNT_ID
  AND hcs.cust_acct_site_id      = hcsu.cust_acct_site_id(+)
  AND HCS.PARTY_SITE_ID          = HPS.PARTY_SITE_ID(+)
  AND NVL(HCSU.PRIMARY_FLAG,'N') ='Y'
  AND HCSU.SITE_USE_CODE(+)      = 'BILL_TO'
  AND HCAA.AUDIT_TRANSACTION_TYPE='U'
  UNION ALL
  SELECT 'No' credit_report_flag,
    TRUNC(Hcaa.Audit_Timestamp) trunc_Audit_Date,
    Hcaa.Audit_Timestamp Audit_Date,
    DECODE(Hcaa.Audit_Transaction_Type,'I','Insert') Audit_Type,
    Hcaa.Audit_User_Name User_Ntid,
    NVL(
    (SELECT Ppf.Full_Name
    FROM Fnd_User Fu,
      Per_All_People_F Ppf
    WHERE Fu.User_Name=Hcaa.Audit_User_Name
    AND Fu.Employee_Id=Ppf.person_id
    AND TRUNC(Sysdate) BETWEEN Ppf.Effective_Start_Date AND Ppf.Effective_End_Date
	AND rownum      =1  --added for version 1.1
    ),Hcaa.Audit_User_Name) User_Name,
    Hca.ACCOUNT_Number,
    HCA.ACCOUNT_NAME,
    HPS.PARTY_SITE_NUMBER site_number,
    HCSU.location SITE_NAME,
    'Customer Source' ATRIBUTE_NAME,
    HCAA.Attribute4 Old_Value,
    xxeis.EIS_XXWC_CUST_AUDIT_HIST_PKG.get_cust_insert_value('APPS.HZ_CUST_ACCOUNTS_AV7','ATTRIBUTE4',HCAA.CUST_ACCOUNT_ID,'CUST_ACCOUNT_ID') new_value,
    'HZ_CUST_ACCOUNTS' TABLE_NAME,
    (SELECT DESCRIPTION
    FROM FND_TABLES
    WHERE TABLE_NAME='HZ_CUST_ACCOUNTS'
    AND rownum      =1
    ) DESCRIPTION,
    DECODE(HCSU.site_use_id,NULL,
    (SELECT hcpcs.name
    FROM HZ_CUSTOMER_PROFILES hcpf,
      HZ_CUST_PROFILE_CLASSES hcpcs
    WHERE hcpf.profile_class_id=hcpcs.profile_class_id
    AND hcpf.cust_account_id   = HCA.CUST_ACCOUNT_ID
    AND hcpf.SITE_USE_ID      IS NULL
    AND rownum                 =1
    ),
    (SELECT hcpcs.name
    FROM HZ_CUSTOMER_PROFILES hcpf,
      HZ_CUST_PROFILE_CLASSES hcpcs
    WHERE hcpf.profile_class_id=hcpcs.profile_class_id
    AND hcpf.site_use_id       = HCSU.site_use_id
    AND hcpf.cust_account_id   = HCA.CUST_ACCOUNT_ID
    AND rownum                 =1
    )) profile_class
  FROM APPS.HZ_CUST_ACCOUNTS_A HCAA,
    APPS.HZ_CUST_ACCOUNTS HCA ,
    HZ_CUST_ACCT_SITES_ALL HCS,
    HZ_CUST_SITE_USES_ALL HCSU,
    HZ_PARTY_SITES HPS
  WHERE HCAA.CUST_ACCOUNT_ID                                                                                                                     = HCA.CUST_ACCOUNT_ID
  AND HCA.CUST_ACCOUNT_ID                                                                                                                        = HCS.CUST_ACCOUNT_ID
  AND hcs.cust_acct_site_id                                                                                                                      = hcsu.cust_acct_site_id(+)
  AND HCS.PARTY_SITE_ID                                                                                                                          = HPS.PARTY_SITE_ID(+)
  AND NVL(HCSU.PRIMARY_FLAG,'N')                                                                                                                 ='Y'
  AND HCSU.SITE_USE_CODE(+)                                                                                                                      = 'BILL_TO'
  AND HCAA.AUDIT_TRANSACTION_TYPE                                                                                                                ='I'
  AND xxeis.EIS_XXWC_CUST_AUDIT_HIST_PKG.get_cust_insert_value('APPS.HZ_CUST_ACCOUNTS_AV7','ATTRIBUTE4',HCAA.CUST_ACCOUNT_ID,'CUST_ACCOUNT_ID') IS NOT NULL
  UNION ALL
  SELECT 'No' credit_report_flag,
    TRUNC(Hcaa.Audit_Timestamp) trunc_Audit_Date,
    Hcaa.Audit_Timestamp Audit_Date,
    DECODE(Hcaa.Audit_Transaction_Type,'U','Update') Audit_Type,
    Hcaa.Audit_User_Name User_Ntid,
    NVL(
    (SELECT Ppf.Full_Name
    FROM Fnd_User Fu,
      Per_All_People_F Ppf
    WHERE Fu.User_Name=Hcaa.Audit_User_Name
    AND Fu.Employee_Id=Ppf.person_id
    AND TRUNC(Sysdate) BETWEEN Ppf.Effective_Start_Date AND Ppf.Effective_End_Date
	AND rownum      =1  --added for version 1.1
    ),Hcaa.Audit_User_Name) User_Name,
    Hca.ACCOUNT_Number,
    HCA.ACCOUNT_NAME,
    HPS.PARTY_SITE_NUMBER site_number,
    HCSU.location SITE_NAME,
    'Authorized Buyer Required' ATRIBUTE_NAME,
    HCAAV3.ATTRIBUTE7 OLD_VALUE,
    xxeis.EIS_XXWC_CUST_AUDIT_HIST_PKG.get_cust_new_value('APPS.HZ_CUST_ACCOUNTS_AV6','ATTRIBUTE7',HCAA.ROW_KEY,HCAAV3.CUST_ACCOUNT_ID,'CUST_ACCOUNT_ID') new_value,
    'HZ_CUST_ACCOUNTS' TABLE_NAME,
    (SELECT DESCRIPTION
    FROM FND_TABLES
    WHERE TABLE_NAME='HZ_CUST_ACCOUNTS'
    AND rownum      =1
    ) DESCRIPTION,
    DECODE(HCSU.site_use_id,NULL,
    (SELECT hcpcs.name
    FROM HZ_CUSTOMER_PROFILES hcpf,
      HZ_CUST_PROFILE_CLASSES hcpcs
    WHERE hcpf.profile_class_id=hcpcs.profile_class_id
    AND hcpf.cust_account_id   = HCA.CUST_ACCOUNT_ID
    AND hcpf.SITE_USE_ID      IS NULL
    AND rownum                 =1
    ),
    (SELECT hcpcs.name
    FROM HZ_CUSTOMER_PROFILES hcpf,
      HZ_CUST_PROFILE_CLASSES hcpcs
    WHERE hcpf.profile_class_id=hcpcs.profile_class_id
    AND hcpf.site_use_id       = HCSU.site_use_id
    AND hcpf.cust_account_id   = HCA.CUST_ACCOUNT_ID
    AND rownum                 =1
    )) profile_class
  FROM apps.HZ_CUST_ACCOUNTS_A HCAA,
    APPS.HZ_CUST_ACCOUNTS_AV6 HCAAV3,
    APPS.HZ_CUST_ACCOUNTS HCA ,
    HZ_CUST_ACCT_SITES_ALL HCS,
    HZ_CUST_SITE_USES_ALL HCSU,
    HZ_PARTY_SITES HPS
  WHERE HCAA.ROW_KEY             =HCAAV3.ROW_KEY
  AND HCAAV3.CUST_ACCOUNT_ID     = HCA.CUST_ACCOUNT_ID
  AND HCA.CUST_ACCOUNT_ID        = HCS.CUST_ACCOUNT_ID
  AND hcs.cust_acct_site_id      = hcsu.cust_acct_site_id(+)
  AND HCS.PARTY_SITE_ID          = HPS.PARTY_SITE_ID(+)
  AND NVL(HCSU.PRIMARY_FLAG,'N') ='Y'
  AND HCSU.SITE_USE_CODE(+)      = 'BILL_TO'
  AND HCAA.AUDIT_TRANSACTION_TYPE='U'
  UNION ALL
  SELECT 'No' credit_report_flag,
    TRUNC(Hcaa.Audit_Timestamp) trunc_Audit_Date,
    Hcaa.Audit_Timestamp Audit_Date,
    DECODE(Hcaa.Audit_Transaction_Type,'I','Insert') Audit_Type,
    Hcaa.Audit_User_Name User_Ntid,
    NVL(
    (SELECT Ppf.Full_Name
    FROM Fnd_User Fu,
      Per_All_People_F Ppf
    WHERE Fu.User_Name=Hcaa.Audit_User_Name
    AND Fu.Employee_Id=Ppf.person_id
    AND TRUNC(Sysdate) BETWEEN Ppf.Effective_Start_Date AND Ppf.Effective_End_Date
	AND rownum      =1	--added for version 1.1
    ),Hcaa.Audit_User_Name) User_Name,
    Hca.ACCOUNT_Number,
    HCA.ACCOUNT_NAME,
    HPS.PARTY_SITE_NUMBER site_number,
    HCSU.location SITE_NAME,
    'Authorized Buyer Required' ATRIBUTE_NAME,
    HCAA.ATTRIBUTE7 OLD_VALUE,
    xxeis.EIS_XXWC_CUST_AUDIT_HIST_PKG.get_cust_insert_value('APPS.HZ_CUST_ACCOUNTS_AV6','ATTRIBUTE7',HCAA.CUST_ACCOUNT_ID,'CUST_ACCOUNT_ID') new_value,
    'HZ_CUST_ACCOUNTS' TABLE_NAME,
    (SELECT DESCRIPTION
    FROM FND_TABLES
    WHERE TABLE_NAME='HZ_CUST_ACCOUNTS'
    AND rownum      =1
    ) DESCRIPTION,
    DECODE(HCSU.site_use_id,NULL,
    (SELECT hcpcs.name
    FROM HZ_CUSTOMER_PROFILES hcpf,
      HZ_CUST_PROFILE_CLASSES hcpcs
    WHERE hcpf.profile_class_id=hcpcs.profile_class_id
    AND hcpf.cust_account_id   = HCA.CUST_ACCOUNT_ID
    AND hcpf.SITE_USE_ID      IS NULL
    AND rownum                 =1
    ),
    (SELECT hcpcs.name
    FROM HZ_CUSTOMER_PROFILES hcpf,
      HZ_CUST_PROFILE_CLASSES hcpcs
    WHERE hcpf.profile_class_id=hcpcs.profile_class_id
    AND hcpf.site_use_id       = HCSU.site_use_id
    AND hcpf.cust_account_id   = HCA.CUST_ACCOUNT_ID
    AND rownum                 =1
    )) profile_class
  FROM apps.HZ_CUST_ACCOUNTS_A HCAA,
    APPS.HZ_CUST_ACCOUNTS HCA ,
    HZ_CUST_ACCT_SITES_ALL HCS,
    HZ_CUST_SITE_USES_ALL HCSU,
    HZ_PARTY_SITES HPS
  WHERE HCAA.CUST_ACCOUNT_ID                                                                                                                     = HCA.CUST_ACCOUNT_ID
  AND HCA.CUST_ACCOUNT_ID                                                                                                                        = HCS.CUST_ACCOUNT_ID
  AND hcs.cust_acct_site_id                                                                                                                      = hcsu.cust_acct_site_id(+)
  AND HCS.PARTY_SITE_ID                                                                                                                          = HPS.PARTY_SITE_ID(+)
  AND NVL(HCSU.PRIMARY_FLAG,'N')                                                                                                                 ='Y'
  AND HCSU.SITE_USE_CODE(+)                                                                                                                      = 'BILL_TO'
  AND HCAA.AUDIT_TRANSACTION_TYPE                                                                                                                ='I'
  AND Xxeis.Eis_Xxwc_Cust_Audit_Hist_Pkg.Get_Cust_Insert_Value('APPS.HZ_CUST_ACCOUNTS_AV6','ATTRIBUTE7',Hcaa.Cust_Account_Id,'CUST_ACCOUNT_ID') IS NOT NULL
  UNION ALL
  SELECT 'No' credit_report_flag,
    TRUNC(Hcaa.Audit_Timestamp) trunc_Audit_Date,
    Hcaa.Audit_Timestamp Audit_Date,
    DECODE(Hcaa.Audit_Transaction_Type,'U','Update') Audit_Type,
    Hcaa.Audit_User_Name User_Ntid,
    NVL(
    (SELECT Ppf.Full_Name
    FROM Fnd_User Fu,
      Per_All_People_F Ppf
    WHERE Fu.User_Name=Hcaa.Audit_User_Name
    AND Fu.Employee_Id=Ppf.person_id
    AND TRUNC(Sysdate) BETWEEN Ppf.Effective_Start_Date AND Ppf.Effective_End_Date
	AND rownum      =1	--added for version 1.1
    ),Hcaa.Audit_User_Name) User_Name,
    Hca.ACCOUNT_Number,
    HCA.ACCOUNT_NAME,
    HPS.PARTY_SITE_NUMBER site_number,
    HCSU.location SITE_NAME,
    'Predominant Trade' ATRIBUTE_NAME,
    HCAAV4.ATTRIBUTE9 OLD_VALUE,
    xxeis.EIS_XXWC_CUST_AUDIT_HIST_PKG.get_cust_new_value('APPS.HZ_CUST_ACCOUNTS_AV5','ATTRIBUTE9',HCAA.ROW_KEY,HCAAV4.CUST_ACCOUNT_ID,'CUST_ACCOUNT_ID') new_value,
    'HZ_CUST_ACCOUNTS' TABLE_NAME,
    (SELECT DESCRIPTION
    FROM FND_TABLES
    WHERE TABLE_NAME='HZ_CUST_ACCOUNTS'
    AND rownum      =1
    ) DESCRIPTION,
    DECODE(HCSU.site_use_id,NULL,
    (SELECT hcpcs.name
    FROM HZ_CUSTOMER_PROFILES hcpf,
      HZ_CUST_PROFILE_CLASSES hcpcs
    WHERE hcpf.profile_class_id=hcpcs.profile_class_id
    AND hcpf.cust_account_id   = HCA.CUST_ACCOUNT_ID
    AND hcpf.SITE_USE_ID      IS NULL
    AND rownum                 =1
    ),
    (SELECT hcpcs.name
    FROM HZ_CUSTOMER_PROFILES hcpf,
      HZ_CUST_PROFILE_CLASSES hcpcs
    WHERE hcpf.profile_class_id=hcpcs.profile_class_id
    AND hcpf.site_use_id       = HCSU.site_use_id
    AND hcpf.cust_account_id   = HCA.CUST_ACCOUNT_ID
    AND rownum                 =1
    )) profile_class
  FROM apps.HZ_CUST_ACCOUNTS_A HCAA,
    APPS.HZ_CUST_ACCOUNTS_AV5 HCAAV4,
    APPS.HZ_CUST_ACCOUNTS HCA ,
    HZ_CUST_ACCT_SITES_ALL HCS,
    HZ_CUST_SITE_USES_ALL HCSU,
    HZ_PARTY_SITES HPS
  WHERE HCAA.ROW_KEY             =HCAAV4.ROW_KEY
  AND HCAAV4.CUST_ACCOUNT_ID     = HCA.CUST_ACCOUNT_ID
  AND HCA.CUST_ACCOUNT_ID        = HCS.CUST_ACCOUNT_ID
  AND hcs.cust_acct_site_id      = hcsu.cust_acct_site_id(+)
  AND HCS.PARTY_SITE_ID          = HPS.PARTY_SITE_ID(+)
  AND NVL(HCSU.PRIMARY_FLAG,'N') ='Y'
  AND HCSU.SITE_USE_CODE(+)      = 'BILL_TO'
  AND HCAA.AUDIT_TRANSACTION_TYPE='U'
  UNION ALL
  SELECT 'No' credit_report_flag,
    TRUNC(Hcaa.Audit_Timestamp) trunc_Audit_Date,
    Hcaa.Audit_Timestamp Audit_Date,
    DECODE(Hcaa.Audit_Transaction_Type,'I','Insert') Audit_Type,
    Hcaa.Audit_User_Name User_Ntid,
    NVL(
    (SELECT Ppf.Full_Name
    FROM Fnd_User Fu,
      Per_All_People_F Ppf
    WHERE Fu.User_Name=Hcaa.Audit_User_Name
    AND Fu.Employee_Id=Ppf.person_id
    AND TRUNC(Sysdate) BETWEEN Ppf.Effective_Start_Date AND Ppf.Effective_End_Date
	AND rownum      =1	--added for version 1.1
    ),Hcaa.Audit_User_Name) User_Name,
    Hca.ACCOUNT_Number,
    HCA.ACCOUNT_NAME,
    HPS.PARTY_SITE_NUMBER site_number,
    HCSU.location SITE_NAME,
    'Predominant Trade' ATRIBUTE_NAME,
    HCAA.ATTRIBUTE9 OLD_VALUE,
    xxeis.EIS_XXWC_CUST_AUDIT_HIST_PKG.get_cust_insert_value('APPS.HZ_CUST_ACCOUNTS_AV5','ATTRIBUTE9',HCAA.CUST_ACCOUNT_ID,'CUST_ACCOUNT_ID') new_value,
    'HZ_CUST_ACCOUNTS' TABLE_NAME,
    (SELECT DESCRIPTION
    FROM FND_TABLES
    WHERE TABLE_NAME='HZ_CUST_ACCOUNTS'
    AND rownum      =1
    ) DESCRIPTION,
    DECODE(HCSU.site_use_id,NULL,
    (SELECT hcpcs.name
    FROM HZ_CUSTOMER_PROFILES hcpf,
      HZ_CUST_PROFILE_CLASSES hcpcs
    WHERE hcpf.profile_class_id=hcpcs.profile_class_id
    AND hcpf.cust_account_id   = HCA.CUST_ACCOUNT_ID
    AND hcpf.SITE_USE_ID      IS NULL
    AND rownum                 =1
    ),
    (SELECT hcpcs.name
    FROM HZ_CUSTOMER_PROFILES hcpf,
      HZ_CUST_PROFILE_CLASSES hcpcs
    WHERE hcpf.profile_class_id=hcpcs.profile_class_id
    AND hcpf.site_use_id       = HCSU.site_use_id
    AND hcpf.cust_account_id   = HCA.CUST_ACCOUNT_ID
    AND rownum                 =1
    )) profile_class
  FROM apps.HZ_CUST_ACCOUNTS_A HCAA,
    APPS.HZ_CUST_ACCOUNTS HCA ,
    HZ_CUST_ACCT_SITES_ALL HCS,
    HZ_CUST_SITE_USES_ALL HCSU,
    HZ_PARTY_SITES HPS
  WHERE HCAA.CUST_ACCOUNT_ID                                                                                                                     = HCA.CUST_ACCOUNT_ID
  AND HCA.CUST_ACCOUNT_ID                                                                                                                        = HCS.CUST_ACCOUNT_ID
  AND hcs.cust_acct_site_id                                                                                                                      = hcsu.cust_acct_site_id(+)
  AND HCS.PARTY_SITE_ID                                                                                                                          = HPS.PARTY_SITE_ID(+)
  AND NVL(HCSU.PRIMARY_FLAG,'N')                                                                                                                 ='Y'
  AND HCSU.SITE_USE_CODE(+)                                                                                                                      = 'BILL_TO'
  AND HCAA.AUDIT_TRANSACTION_TYPE                                                                                                                ='I'
  AND Xxeis.Eis_Xxwc_Cust_Audit_Hist_Pkg.Get_Cust_Insert_Value('APPS.HZ_CUST_ACCOUNTS_AV5','ATTRIBUTE9',Hcaa.Cust_Account_Id,'CUST_ACCOUNT_ID') IS NOT NULL
  UNION ALL
  SELECT 'No' credit_report_flag,
    TRUNC(Hcaa.Audit_Timestamp) trunc_Audit_Date,
    Hcaa.Audit_Timestamp Audit_Date,
    DECODE(Hcaa.Audit_Transaction_Type,'U','Update') Audit_Type,
    Hcaa.Audit_User_Name User_Ntid,
    NVL(
    (SELECT Ppf.Full_Name
    FROM Fnd_User Fu,
      Per_All_People_F Ppf
    WHERE Fu.User_Name=Hcaa.Audit_User_Name
    AND Fu.Employee_Id=Ppf.person_id
    AND TRUNC(Sysdate) BETWEEN Ppf.Effective_Start_Date AND Ppf.Effective_End_Date
	AND rownum      =1	--added for version 1.1
    ),Hcaa.Audit_User_Name) User_Name,
    Hca.ACCOUNT_Number,
    HCA.ACCOUNT_NAME,
    HPS.PARTY_SITE_NUMBER site_number,
    Hcsu.Location Site_Name,
    'Status' Atribute_Name,
    DECODE(HCAAV5.STATUS,'A','Active','I','Inactive') OLD_VALUE,
    DECODE(xxeis.EIS_XXWC_CUST_AUDIT_HIST_PKG.get_cust_new_value('APPS.HZ_CUST_ACCOUNTS_AV4','STATUS',HCAA.ROW_KEY,HCAAV5.CUST_ACCOUNT_ID,'CUST_ACCOUNT_ID'),'A','Active','I','Inactive') new_value,
    'HZ_CUST_ACCOUNTS' TABLE_NAME,
    (SELECT DESCRIPTION
    FROM FND_TABLES
    WHERE TABLE_NAME='HZ_CUST_ACCOUNTS'
    AND rownum      =1
    ) DESCRIPTION,
    DECODE(HCSU.site_use_id,NULL,
    (SELECT hcpcs.name
    FROM HZ_CUSTOMER_PROFILES hcpf,
      HZ_CUST_PROFILE_CLASSES hcpcs
    WHERE hcpf.profile_class_id=hcpcs.profile_class_id
    AND hcpf.cust_account_id   = HCA.CUST_ACCOUNT_ID
    AND hcpf.SITE_USE_ID      IS NULL
    AND rownum                 =1
    ),
    (SELECT hcpcs.name
    FROM HZ_CUSTOMER_PROFILES hcpf,
      HZ_CUST_PROFILE_CLASSES hcpcs
    WHERE hcpf.profile_class_id=hcpcs.profile_class_id
    AND hcpf.site_use_id       = HCSU.site_use_id
    AND hcpf.cust_account_id   = HCA.CUST_ACCOUNT_ID
    AND rownum                 =1
    )) profile_class
  FROM apps.HZ_CUST_ACCOUNTS_A HCAA,
    APPS.HZ_CUST_ACCOUNTS_AV4 HCAAV5,
    APPS.HZ_CUST_ACCOUNTS HCA ,
    HZ_CUST_ACCT_SITES_ALL HCS,
    HZ_CUST_SITE_USES_ALL HCSU,
    HZ_PARTY_SITES HPS
  WHERE HCAA.ROW_KEY             =HCAAV5.ROW_KEY
  AND HCAAV5.CUST_ACCOUNT_ID     = HCA.CUST_ACCOUNT_ID
  AND HCA.CUST_ACCOUNT_ID        = HCS.CUST_ACCOUNT_ID
  AND hcs.cust_acct_site_id      = hcsu.cust_acct_site_id(+)
  AND HCS.PARTY_SITE_ID          = HPS.PARTY_SITE_ID(+)
  AND NVL(HCSU.PRIMARY_FLAG,'N') ='Y'
  AND HCSU.SITE_USE_CODE(+)      = 'BILL_TO'
  AND HCAA.AUDIT_TRANSACTION_TYPE='U'
  UNION ALL
  SELECT 'No' credit_report_flag,
    TRUNC(Hcaa.Audit_Timestamp) trunc_Audit_Date,
    Hcaa.Audit_Timestamp Audit_Date,
    DECODE(Hcaa.Audit_Transaction_Type,'I','Insert') Audit_Type,
    Hcaa.Audit_User_Name User_Ntid,
    NVL(
    (SELECT Ppf.Full_Name
    FROM Fnd_User Fu,
      Per_All_People_F Ppf
    WHERE Fu.User_Name=Hcaa.Audit_User_Name
    AND Fu.Employee_Id=Ppf.person_id
    AND TRUNC(Sysdate) BETWEEN Ppf.Effective_Start_Date AND Ppf.Effective_End_Date
	AND rownum      =1	--added for version 1.1
    ),Hcaa.Audit_User_Name) User_Name,
    Hca.ACCOUNT_Number,
    HCA.ACCOUNT_NAME,
    HPS.PARTY_SITE_NUMBER site_number,
    HCSU.location SITE_NAME,
    'Status' Atribute_Name,
    DECODE(HCAA.STATUS,'A','Active','I','Inactive') OLD_VALUE,
    DECODE(xxeis.EIS_XXWC_CUST_AUDIT_HIST_PKG.get_cust_insert_value('APPS.HZ_CUST_ACCOUNTS_AV4','STATUS',HCAA.CUST_ACCOUNT_ID,'CUST_ACCOUNT_ID'),'A','Active','I','Inactive') new_value,
    'HZ_CUST_ACCOUNTS' TABLE_NAME,
    (SELECT DESCRIPTION
    FROM FND_TABLES
    WHERE TABLE_NAME='HZ_CUST_ACCOUNTS'
    AND rownum      =1
    ) DESCRIPTION,
    DECODE(HCSU.site_use_id,NULL,
    (SELECT hcpcs.name
    FROM HZ_CUSTOMER_PROFILES hcpf,
      HZ_CUST_PROFILE_CLASSES hcpcs
    WHERE hcpf.profile_class_id=hcpcs.profile_class_id
    AND hcpf.cust_account_id   = HCA.CUST_ACCOUNT_ID
    AND hcpf.SITE_USE_ID      IS NULL
    AND rownum                 =1
    ),
    (SELECT hcpcs.name
    FROM HZ_CUSTOMER_PROFILES hcpf,
      HZ_CUST_PROFILE_CLASSES hcpcs
    WHERE hcpf.profile_class_id=hcpcs.profile_class_id
    AND hcpf.site_use_id       = HCSU.site_use_id
    AND hcpf.cust_account_id   = HCA.CUST_ACCOUNT_ID
    AND rownum                 =1
    )) profile_class
  FROM apps.HZ_CUST_ACCOUNTS_A HCAA,
    APPS.HZ_CUST_ACCOUNTS HCA ,
    HZ_CUST_ACCT_SITES_ALL HCS,
    HZ_CUST_SITE_USES_ALL HCSU,
    HZ_PARTY_SITES HPS
  WHERE HCAA.CUST_ACCOUNT_ID                                                                                                                 = HCA.CUST_ACCOUNT_ID
  AND HCA.CUST_ACCOUNT_ID                                                                                                                    = HCS.CUST_ACCOUNT_ID
  AND hcs.cust_acct_site_id                                                                                                                  = hcsu.cust_acct_site_id(+)
  AND HCS.PARTY_SITE_ID                                                                                                                      = HPS.PARTY_SITE_ID(+)
  AND NVL(HCSU.PRIMARY_FLAG,'N')                                                                                                             ='Y'
  AND HCSU.SITE_USE_CODE(+)                                                                                                                  = 'BILL_TO'
  AND HCAA.AUDIT_TRANSACTION_TYPE                                                                                                            ='I'
  AND Xxeis.Eis_Xxwc_Cust_Audit_Hist_Pkg.Get_Cust_Insert_Value('APPS.HZ_CUST_ACCOUNTS_AV4','STATUS',Hcaa.Cust_Account_Id,'CUST_ACCOUNT_ID') IS NOT NULL
  UNION ALL
  SELECT 'No' credit_report_flag,
    TRUNC(Hcaa.Audit_Timestamp) trunc_Audit_Date,
    Hcaa.Audit_Timestamp Audit_Date,
    DECODE(Hcaa.Audit_Transaction_Type,'U','Update') Audit_Type,
    Hcaa.Audit_User_Name User_Ntid,
    NVL(
    (SELECT Ppf.Full_Name
    FROM Fnd_User Fu,
      Per_All_People_F Ppf
    WHERE Fu.User_Name=Hcaa.Audit_User_Name
    AND Fu.Employee_Id=Ppf.person_id
    AND TRUNC(Sysdate) BETWEEN Ppf.Effective_Start_Date AND Ppf.Effective_End_Date
	AND rownum      =1	--added for version 1.1
    ),Hcaa.Audit_User_Name) User_Name,
    Hca.ACCOUNT_Number,
    HCA.ACCOUNT_NAME,
    HPS.PARTY_SITE_NUMBER site_number,
    HCSU.location SITE_NAME,
    'Classification' ATRIBUTE_NAME,
    XXEIS.EIS_XXWC_CUST_AUDIT_HIST_PKG.GET_LOOKUP_VALUE(HCAAV6.CUSTOMER_CLASS_CODE,'CUSTOMER CLASS') OLD_VALUE,
    XXEIS.EIS_XXWC_CUST_AUDIT_HIST_PKG.GET_LOOKUP_VALUE(xxeis.EIS_XXWC_CUST_AUDIT_HIST_PKG.get_cust_new_value('APPS.HZ_CUST_ACCOUNTS_AV3','CUSTOMER_CLASS_CODE',HCAA.ROW_KEY,HCAAV6.CUST_ACCOUNT_ID,'CUST_ACCOUNT_ID'),'CUSTOMER CLASS') new_value,
    'HZ_CUST_ACCOUNTS' TABLE_NAME,
    (SELECT DESCRIPTION
    FROM FND_TABLES
    WHERE TABLE_NAME='HZ_CUST_ACCOUNTS'
    AND rownum      =1
    ) DESCRIPTION,
    DECODE(HCSU.site_use_id,NULL,
    (SELECT hcpcs.name
    FROM HZ_CUSTOMER_PROFILES hcpf,
      HZ_CUST_PROFILE_CLASSES hcpcs
    WHERE hcpf.profile_class_id=hcpcs.profile_class_id
    AND hcpf.cust_account_id   = HCA.CUST_ACCOUNT_ID
    AND hcpf.SITE_USE_ID      IS NULL
    AND rownum                 =1
    ),
    (SELECT hcpcs.name
    FROM HZ_CUSTOMER_PROFILES hcpf,
      HZ_CUST_PROFILE_CLASSES hcpcs
    WHERE hcpf.profile_class_id=hcpcs.profile_class_id
    AND hcpf.site_use_id       = HCSU.site_use_id
    AND hcpf.cust_account_id   = HCA.CUST_ACCOUNT_ID
    AND rownum                 =1
    )) profile_class
  FROM Apps.Hz_Cust_Accounts_A Hcaa,
    APPS.HZ_CUST_ACCOUNTS_AV3 HCAAV6,
    APPS.HZ_CUST_ACCOUNTS HCA ,
    HZ_CUST_ACCT_SITES_ALL HCS,
    HZ_CUST_SITE_USES_ALL HCSU,
    Hz_Party_Sites Hps
  WHERE HCAA.ROW_KEY             = HCAAV6.ROW_KEY
  AND HCAAV6.CUST_ACCOUNT_ID     = HCA.CUST_ACCOUNT_ID
  AND HCA.CUST_ACCOUNT_ID        = HCS.CUST_ACCOUNT_ID
  AND hcs.cust_acct_site_id      = hcsu.cust_acct_site_id(+)
  AND HCS.PARTY_SITE_ID          = HPS.PARTY_SITE_ID(+)
  AND NVL(HCSU.PRIMARY_FLAG,'N') ='Y'
  AND HCSU.SITE_USE_CODE(+)      = 'BILL_TO'
  AND HCAA.AUDIT_TRANSACTION_TYPE='U'
  UNION ALL
  SELECT 'No' credit_report_flag,
    TRUNC(Hcaa.Audit_Timestamp) trunc_Audit_Date,
    Hcaa.Audit_Timestamp Audit_Date,
    DECODE(Hcaa.Audit_Transaction_Type,'I','Insert') Audit_Type,
    Hcaa.Audit_User_Name User_Ntid,
    NVL(
    (SELECT Ppf.Full_Name
    FROM Fnd_User Fu,
      Per_All_People_F Ppf
    WHERE Fu.User_Name=Hcaa.Audit_User_Name
    AND Fu.Employee_Id=Ppf.person_id
    AND TRUNC(Sysdate) BETWEEN Ppf.Effective_Start_Date AND Ppf.Effective_End_Date
	AND rownum                 =1	--added for version 1.1
    ),Hcaa.Audit_User_Name) User_Name,
    Hca.ACCOUNT_Number,
    HCA.ACCOUNT_NAME,
    HPS.PARTY_SITE_NUMBER site_number,
    HCSU.location SITE_NAME,
    'Classification' ATRIBUTE_NAME,
    XXEIS.EIS_XXWC_CUST_AUDIT_HIST_PKG.GET_LOOKUP_VALUE(HCAA.CUSTOMER_CLASS_CODE,'CUSTOMER CLASS') OLD_VALUE,
    Xxeis.Eis_Xxwc_Cust_Audit_Hist_Pkg.Get_Lookup_Value(xxeis.EIS_XXWC_CUST_AUDIT_HIST_PKG.get_cust_insert_value('APPS.HZ_CUST_ACCOUNTS_AV3','CUSTOMER_CLASS_CODE',HCAA.CUST_ACCOUNT_ID,'CUST_ACCOUNT_ID'),'CUSTOMER CLASS') new_value,
    'HZ_CUST_ACCOUNTS' TABLE_NAME,
    (SELECT DESCRIPTION
    FROM FND_TABLES
    WHERE TABLE_NAME='HZ_CUST_ACCOUNTS'
    AND rownum      =1
    ) DESCRIPTION,
    DECODE(HCSU.site_use_id,NULL,
    (SELECT hcpcs.name
    FROM HZ_CUSTOMER_PROFILES hcpf,
      HZ_CUST_PROFILE_CLASSES hcpcs
    WHERE hcpf.profile_class_id=hcpcs.profile_class_id
    AND hcpf.cust_account_id   = HCA.CUST_ACCOUNT_ID
    AND hcpf.SITE_USE_ID      IS NULL
    AND rownum                 =1
    ),
    (SELECT hcpcs.name
    FROM HZ_CUSTOMER_PROFILES hcpf,
      HZ_CUST_PROFILE_CLASSES hcpcs
    WHERE hcpf.profile_class_id=hcpcs.profile_class_id
    AND hcpf.site_use_id       = HCSU.site_use_id
    AND hcpf.cust_account_id   = HCA.CUST_ACCOUNT_ID
    AND rownum                 =1
    )) profile_class
  FROM apps.HZ_CUST_ACCOUNTS_A HCAA,
    APPS.HZ_CUST_ACCOUNTS HCA ,
    HZ_CUST_ACCT_SITES_ALL HCS,
    HZ_CUST_SITE_USES_ALL HCSU,
    HZ_PARTY_SITES HPS
  WHERE HCAA.CUST_ACCOUNT_ID                                                                                                                              = HCA.CUST_ACCOUNT_ID
  AND HCA.CUST_ACCOUNT_ID                                                                                                                                 = HCS.CUST_ACCOUNT_ID
  AND hcs.cust_acct_site_id                                                                                                                               = hcsu.cust_acct_site_id(+)
  AND HCS.PARTY_SITE_ID                                                                                                                                   = HPS.PARTY_SITE_ID(+)
  AND NVL(HCSU.PRIMARY_FLAG,'N')                                                                                                                          ='Y'
  AND HCSU.SITE_USE_CODE(+)                                                                                                                               = 'BILL_TO'
  AND HCAA.AUDIT_TRANSACTION_TYPE                                                                                                                         ='I'
  AND Xxeis.Eis_Xxwc_Cust_Audit_Hist_Pkg.Get_Cust_Insert_Value('APPS.HZ_CUST_ACCOUNTS_AV3','CUSTOMER_CLASS_CODE',Hcaa.Cust_Account_Id,'CUST_ACCOUNT_ID') IS NOT NULL
  UNION ALL
  SELECT 'No' credit_report_flag,
    TRUNC(Hcaa.Audit_Timestamp) trunc_Audit_Date,
    Hcaa.Audit_Timestamp Audit_Date,
    DECODE(Hcaa.Audit_Transaction_Type,'U','Update') Audit_Type,
    Hcaa.Audit_User_Name User_Ntid,
    NVL(
    (SELECT Ppf.Full_Name
    FROM Fnd_User Fu,
      Per_All_People_F Ppf
    WHERE Fu.User_Name=Hcaa.Audit_User_Name
    AND Fu.Employee_Id=Ppf.person_id
    AND TRUNC(Sysdate) BETWEEN Ppf.Effective_Start_Date AND Ppf.Effective_End_Date
	AND rownum                 =1	--added for version 1.1
    ),Hcaa.Audit_User_Name) User_Name,
    Hca.ACCOUNT_Number,
    HCA.ACCOUNT_NAME,
    HPS.PARTY_SITE_NUMBER site_number,
    HCSU.location SITE_NAME,
    'Account Name' Atribute_Name,
    HCAAV7.ACCOUNT_NAME Old_Value,
    xxeis.EIS_XXWC_CUST_AUDIT_HIST_PKG.get_cust_new_value('APPS.HZ_CUST_ACCOUNTS_AV2','ACCOUNT_NAME',HCAA.ROW_KEY,HCAAV7.CUST_ACCOUNT_ID,'CUST_ACCOUNT_ID') new_value,
    'HZ_CUST_ACCOUNTS' TABLE_NAME,
    (SELECT DESCRIPTION
    FROM FND_TABLES
    WHERE TABLE_NAME='HZ_CUST_ACCOUNTS'
    AND rownum      =1
    ) DESCRIPTION,
    DECODE(HCSU.site_use_id,NULL,
    (SELECT hcpcs.name
    FROM HZ_CUSTOMER_PROFILES hcpf,
      HZ_CUST_PROFILE_CLASSES hcpcs
    WHERE hcpf.profile_class_id=hcpcs.profile_class_id
    AND hcpf.cust_account_id   = HCA.CUST_ACCOUNT_ID
    AND hcpf.SITE_USE_ID      IS NULL
    AND rownum                 =1
    ),
    (SELECT hcpcs.name
    FROM HZ_CUSTOMER_PROFILES hcpf,
      HZ_CUST_PROFILE_CLASSES hcpcs
    WHERE hcpf.profile_class_id=hcpcs.profile_class_id
    AND hcpf.site_use_id       = HCSU.site_use_id
    AND hcpf.cust_account_id   = HCA.CUST_ACCOUNT_ID
    AND rownum                 =1
    )) profile_class
  FROM apps.HZ_CUST_ACCOUNTS_A HCAA,
    APPS.HZ_CUST_ACCOUNTS_AV2 HCAAV7,
    APPS.HZ_CUST_ACCOUNTS HCA ,
    HZ_CUST_ACCT_SITES_ALL HCS,
    HZ_CUST_SITE_USES_ALL HCSU,
    HZ_PARTY_SITES HPS
  WHERE HCAA.ROW_KEY             =HCAAV7.ROW_KEY
  AND HCAAV7.CUST_ACCOUNT_ID     = HCA.CUST_ACCOUNT_ID
  AND HCA.CUST_ACCOUNT_ID        = HCS.CUST_ACCOUNT_ID
  AND hcs.cust_acct_site_id      = hcsu.cust_acct_site_id(+)
  AND HCS.PARTY_SITE_ID          = HPS.PARTY_SITE_ID(+)
  AND NVL(HCSU.PRIMARY_FLAG,'N') ='Y'
  AND HCSU.SITE_USE_CODE(+)      = 'BILL_TO'
  AND HCAA.AUDIT_TRANSACTION_TYPE='U'
  UNION ALL
  SELECT 'No' credit_report_flag,
    TRUNC(Hcaa.Audit_Timestamp) trunc_Audit_Date,
    Hcaa.Audit_Timestamp Audit_Date,
    DECODE(Hcaa.Audit_Transaction_Type,'I','Insert') Audit_Type,
    Hcaa.Audit_User_Name User_Ntid,
    NVL(
    (SELECT Ppf.Full_Name
    FROM Fnd_User Fu,
      Per_All_People_F Ppf
    WHERE Fu.User_Name=Hcaa.Audit_User_Name
    AND Fu.Employee_Id=Ppf.person_id
    AND TRUNC(Sysdate) BETWEEN Ppf.Effective_Start_Date AND Ppf.Effective_End_Date
	AND rownum                 =1	--added for version 1.1
    ),Hcaa.Audit_User_Name) User_Name,
    Hca.ACCOUNT_Number,
    HCA.ACCOUNT_NAME,
    HPS.PARTY_SITE_NUMBER site_number,
    HCSU.location SITE_NAME,
    'Account Name' ATRIBUTE_NAME,
    Hcaa.ACCOUNT_NAME Old_Value,
    xxeis.EIS_XXWC_CUST_AUDIT_HIST_PKG.get_cust_insert_value('APPS.HZ_CUST_ACCOUNTS_AV2','ACCOUNT_NAME',HCAA.CUST_ACCOUNT_ID,'CUST_ACCOUNT_ID') new_value,
    'HZ_CUST_ACCOUNTS' TABLE_NAME,
    (SELECT DESCRIPTION
    FROM FND_TABLES
    WHERE TABLE_NAME='HZ_CUST_ACCOUNTS'
    AND rownum      =1
    ) DESCRIPTION,
    DECODE(HCSU.site_use_id,NULL,
    (SELECT hcpcs.name
    FROM HZ_CUSTOMER_PROFILES hcpf,
      HZ_CUST_PROFILE_CLASSES hcpcs
    WHERE hcpf.profile_class_id=hcpcs.profile_class_id
    AND hcpf.cust_account_id   = HCA.CUST_ACCOUNT_ID
    AND hcpf.SITE_USE_ID      IS NULL
    AND rownum                 =1
    ),
    (SELECT hcpcs.name
    FROM HZ_CUSTOMER_PROFILES hcpf,
      HZ_CUST_PROFILE_CLASSES hcpcs
    WHERE hcpf.profile_class_id=hcpcs.profile_class_id
    AND hcpf.site_use_id       = HCSU.site_use_id
    AND hcpf.cust_account_id   = HCA.CUST_ACCOUNT_ID
    AND rownum                 =1
    )) profile_class
  FROM apps.HZ_CUST_ACCOUNTS_A HCAA,
    APPS.HZ_CUST_ACCOUNTS HCA ,
    HZ_CUST_ACCT_SITES_ALL HCS,
    HZ_CUST_SITE_USES_ALL HCSU,
    HZ_PARTY_SITES HPS
  WHERE HCAA.CUST_ACCOUNT_ID                                                                                                                       = HCA.CUST_ACCOUNT_ID
  AND HCA.CUST_ACCOUNT_ID                                                                                                                          = HCS.CUST_ACCOUNT_ID
  AND hcs.cust_acct_site_id                                                                                                                        = hcsu.cust_acct_site_id(+)
  AND HCS.PARTY_SITE_ID                                                                                                                            = HPS.PARTY_SITE_ID(+)
  AND NVL(HCSU.PRIMARY_FLAG,'N')                                                                                                                   ='Y'
  AND HCSU.SITE_USE_CODE(+)                                                                                                                        = 'BILL_TO'
  AND HCAA.AUDIT_TRANSACTION_TYPE                                                                                                                  ='I'
  AND XXEIS.EIS_XXWC_CUST_AUDIT_HIST_PKG.GET_CUST_INSERT_VALUE('APPS.HZ_CUST_ACCOUNTS_AV2','ACCOUNT_NAME',HCAA.CUST_ACCOUNT_ID,'CUST_ACCOUNT_ID') IS NOT NULL
  UNION ALL
  SELECT 'No' credit_report_flag,
    TRUNC(Hcpa.Audit_Timestamp) trunc_Audit_Date,
    Hcpa.Audit_Timestamp Audit_Date,
    DECODE(Hcpa.Audit_Transaction_Type,'U','Update') Audit_Type,
    Hcpa.Audit_User_Name User_Ntid,
    NVL(
    (SELECT Ppf.Full_Name
    FROM Fnd_User Fu,
      Per_All_People_F Ppf
    WHERE Fu.User_Name=Hcpa.Audit_User_Name
    AND Fu.Employee_Id=Ppf.person_id
    AND TRUNC(Sysdate) BETWEEN Ppf.Effective_Start_Date AND Ppf.Effective_End_Date
	AND rownum                 =1	--added for version 1.1
    ),Hcpa.Audit_User_Name) User_Name,
    Hca.ACCOUNT_Number,
    HCA.ACCOUNT_NAME,
    HPS.PARTY_SITE_NUMBER site_number,
    HCSU.location SITE_NAME,
    'Collector' ATRIBUTE_NAME,
    (SELECT AC.NAME
    FROM APPS.AR_COLLECTORS AC
    WHERE AC.COLLECTOR_ID= HCPAV3.COLLECTOR_ID
    ) OLD_VALUE,
    (SELECT AC.NAME
    FROM APPS.AR_COLLECTORS AC
    WHERE AC.COLLECTOR_ID= xxeis.EIS_XXWC_CUST_AUDIT_HIST_PKG.get_cust_new_value('APPS.HZ_CUSTOMER_PROFILES_AV4','COLLECTOR_ID',HCPA.ROW_KEY,HCPAV3.CUST_ACCOUNT_PROFILE_ID,'CUST_ACCOUNT_PROFILE_ID')
    ) new_value,
    'HZ_CUSTOMER_PROFILES' TABLE_NAME,
    (SELECT DESCRIPTION
    FROM FND_TABLES
    WHERE TABLE_NAME='HZ_CUSTOMER_PROFILES'
    AND ROWNUM      =1
    ) DESCRIPTION,
    DECODE(HCSU.site_use_id,NULL,
    (SELECT hcpcs.name
    FROM HZ_CUSTOMER_PROFILES hcpf,
      HZ_CUST_PROFILE_CLASSES hcpcs
    WHERE hcpf.profile_class_id=hcpcs.profile_class_id
    AND hcpf.cust_account_id   = HCA.CUST_ACCOUNT_ID
    AND hcpf.SITE_USE_ID      IS NULL
    AND rownum                 =1
    ),
    (SELECT hcpcs.name
    FROM HZ_CUSTOMER_PROFILES hcpf,
      HZ_CUST_PROFILE_CLASSES hcpcs
    WHERE hcpf.profile_class_id=hcpcs.profile_class_id
    AND hcpf.site_use_id       = HCSU.site_use_id
    AND hcpf.cust_account_id   = HCA.CUST_ACCOUNT_ID
    AND rownum                 =1
    )) profile_class
  FROM APPS.HZ_CUSTOMER_PROFILES_A HCPA,
    APPS.HZ_CUSTOMER_PROFILES_AV4 HCPAV3,
    APPS.HZ_CUSTOMER_PROFILES HCAA,
    APPS.HZ_CUST_ACCOUNTS HCA ,
    HZ_CUST_ACCT_SITES_ALL HCS,
    HZ_CUST_SITE_USES_ALL HCSU,
    HZ_PARTY_SITES HPS
  WHERE Hcpa.ROW_KEY                 = HCPAV3.ROW_KEY
  AND HCPAV3.CUST_ACCOUNT_PROFILE_ID = HCAA.CUST_ACCOUNT_PROFILE_ID
  AND HCAA.SITE_USE_ID              IS NULL
  AND HCAA.CUST_ACCOUNT_ID           = HCA.CUST_ACCOUNT_ID
  AND HCA.CUST_ACCOUNT_ID            = HCS.CUST_ACCOUNT_ID
  AND HCS.CUST_ACCT_SITE_ID          = HCSU.CUST_ACCT_SITE_ID(+)
  AND HCS.PARTY_SITE_ID              = HPS.PARTY_SITE_ID(+)
  AND NVL(HCSU.PRIMARY_FLAG,'N')     ='Y'
  AND HCSU.SITE_USE_CODE(+)          = 'BILL_TO'
  AND HCPA.AUDIT_TRANSACTION_TYPE    ='U'
  UNION ALL
  SELECT 'No' credit_report_flag,
    TRUNC(Hcpa.Audit_Timestamp) trunc_Audit_Date,
    HCPA.AUDIT_TIMESTAMP AUDIT_DATE,
    DECODE(Hcpa.Audit_Transaction_Type,'I','Insert') Audit_Type,
    Hcpa.Audit_User_Name User_Ntid,
    NVL(
    (SELECT Ppf.Full_Name
    FROM Fnd_User Fu,
      Per_All_People_F Ppf
    WHERE Fu.User_Name=Hcpa.Audit_User_Name
    AND Fu.Employee_Id=Ppf.person_id
    AND TRUNC(Sysdate) BETWEEN Ppf.Effective_Start_Date AND Ppf.Effective_End_Date
	AND rownum                 =1 --added for version 1.1
    ),Hcpa.Audit_User_Name) User_Name,
    Hca.ACCOUNT_Number,
    HCA.ACCOUNT_NAME,
    HPS.PARTY_SITE_NUMBER site_number,
    HCSU.LOCATION SITE_NAME,
    'Collector' ATRIBUTE_NAME,
    (SELECT AC.NAME
    FROM APPS.AR_COLLECTORS AC
    WHERE AC.COLLECTOR_ID= HCPA.COLLECTOR_ID
    ) OLD_VALUE,
    (SELECT AC.NAME
    FROM APPS.AR_COLLECTORS AC
    WHERE AC.COLLECTOR_ID= xxeis.EIS_XXWC_CUST_AUDIT_HIST_PKG.get_cust_insert_value('APPS.HZ_CUSTOMER_PROFILES_AV4','COLLECTOR_ID',HCPA.CUST_ACCOUNT_PROFILE_ID,'CUST_ACCOUNT_PROFILE_ID')
    ) new_value,
    'HZ_CUSTOMER_PROFILES' TABLE_NAME,
    (SELECT DESCRIPTION
    FROM FND_TABLES
    WHERE TABLE_NAME='HZ_CUSTOMER_PROFILES'
    AND ROWNUM      =1
    ) DESCRIPTION,
    DECODE(HCSU.site_use_id,NULL,
    (SELECT hcpcs.name
    FROM HZ_CUSTOMER_PROFILES hcpf,
      HZ_CUST_PROFILE_CLASSES hcpcs
    WHERE hcpf.profile_class_id=hcpcs.profile_class_id
    AND hcpf.cust_account_id   = HCA.CUST_ACCOUNT_ID
    AND hcpf.SITE_USE_ID      IS NULL
    AND rownum                 =1
    ),
    (SELECT hcpcs.name
    FROM HZ_CUSTOMER_PROFILES hcpf,
      HZ_CUST_PROFILE_CLASSES hcpcs
    WHERE hcpf.profile_class_id=hcpcs.profile_class_id
    AND hcpf.site_use_id       = HCSU.site_use_id
    AND hcpf.cust_account_id   = HCA.CUST_ACCOUNT_ID
    AND rownum                 =1
    )) profile_class
  FROM APPS.HZ_CUSTOMER_PROFILES_A HCPA,
    APPS.HZ_CUSTOMER_PROFILES HCAA,
    APPS.HZ_CUST_ACCOUNTS HCA ,
    HZ_CUST_ACCT_SITES_ALL HCS,
    HZ_CUST_SITE_USES_ALL HCSU,
    HZ_PARTY_SITES HPS
  WHERE HCPA.CUST_ACCOUNT_PROFILE_ID                                                                                                                                   = HCAA.CUST_ACCOUNT_PROFILE_ID
  AND HCAA.SITE_USE_ID                                                                                                                                                IS NULL
  AND HCAA.CUST_ACCOUNT_ID                                                                                                                                             = HCA.CUST_ACCOUNT_ID
  AND HCA.CUST_ACCOUNT_ID                                                                                                                                              = HCS.CUST_ACCOUNT_ID
  AND HCS.CUST_ACCT_SITE_ID                                                                                                                                            = HCSU.CUST_ACCT_SITE_ID(+)
  AND HCS.PARTY_SITE_ID                                                                                                                                                = HPS.PARTY_SITE_ID(+)
  AND NVL(HCSU.PRIMARY_FLAG,'N')                                                                                                                                       ='Y'
  AND HCSU.SITE_USE_CODE(+)                                                                                                                                            = 'BILL_TO'
  AND HCPA.AUDIT_TRANSACTION_TYPE                                                                                                                                      ='I'
  AND XXEIS.EIS_XXWC_CUST_AUDIT_HIST_PKG.GET_CUST_INSERT_VALUE('APPS.HZ_CUSTOMER_PROFILES_AV4','COLLECTOR_ID',HCPA.CUST_ACCOUNT_PROFILE_ID,'CUST_ACCOUNT_PROFILE_ID') IS NOT NULL
  UNION ALL
  SELECT 'No' credit_report_flag,
    TRUNC(Hcpa.Audit_Timestamp) trunc_Audit_Date,
    Hcpa.Audit_Timestamp Audit_Date,
    DECODE(Hcpa.Audit_Transaction_Type,'U','Update') Audit_Type,
    Hcpa.Audit_User_Name User_Ntid,
    NVL(
    (SELECT Ppf.Full_Name
    FROM Fnd_User Fu,
      Per_All_People_F Ppf
    WHERE Fu.User_Name=Hcpa.Audit_User_Name
    AND Fu.Employee_Id=Ppf.person_id
    AND TRUNC(Sysdate) BETWEEN Ppf.Effective_Start_Date AND Ppf.Effective_End_Date
	AND rownum                 =1 --added for version 1.1
    ),Hcpa.Audit_User_Name) User_Name,
    Hca.ACCOUNT_Number,
    HCA.ACCOUNT_NAME,
    HPS.PARTY_SITE_NUMBER site_number,
    HCSU.location SITE_NAME,
    'Send Statement' ATRIBUTE_NAME,
    DECODE(HCPAV5.SEND_STATEMENTS,'Y','Yes','N','No') OLD_VALUE,
    DECODE(xxeis.EIS_XXWC_CUST_AUDIT_HIST_PKG.get_cust_new_value('APPS.HZ_CUSTOMER_PROFILES_AV10','SEND_STATEMENTS',HCPA.ROW_KEY,HCPAV5.CUST_ACCOUNT_PROFILE_ID,'CUST_ACCOUNT_PROFILE_ID'),'Y','Yes','N','No') new_value,
    'HZ_CUSTOMER_PROFILES' TABLE_NAME,
    (SELECT DESCRIPTION
    FROM FND_TABLES
    WHERE TABLE_NAME='HZ_CUSTOMER_PROFILES'
    AND ROWNUM      =1
    ) DESCRIPTION,
    DECODE(HCSU.site_use_id,NULL,
    (SELECT hcpcs.name
    FROM HZ_CUSTOMER_PROFILES hcpf,
      HZ_CUST_PROFILE_CLASSES hcpcs
    WHERE hcpf.profile_class_id=hcpcs.profile_class_id
    AND hcpf.cust_account_id   = HCA.CUST_ACCOUNT_ID
    AND hcpf.SITE_USE_ID      IS NULL
    AND rownum                 =1
    ),
    (SELECT hcpcs.name
    FROM HZ_CUSTOMER_PROFILES hcpf,
      HZ_CUST_PROFILE_CLASSES hcpcs
    WHERE hcpf.profile_class_id=hcpcs.profile_class_id
    AND hcpf.site_use_id       = HCSU.site_use_id
    AND hcpf.cust_account_id   = HCA.CUST_ACCOUNT_ID
    AND rownum                 =1
    )) profile_class
  FROM APPS.HZ_CUSTOMER_PROFILES_A HCPA,
    APPS.HZ_CUSTOMER_PROFILES_AV10 HCPAV5,
    APPS.HZ_CUSTOMER_PROFILES HCAA,
    APPS.HZ_CUST_ACCOUNTS HCA ,
    HZ_CUST_ACCT_SITES_ALL HCS,
    HZ_CUST_SITE_USES_ALL HCSU,
    HZ_PARTY_SITES HPS
  WHERE Hcpa.ROW_KEY                 = HCPAV5.ROW_KEY
  AND HCPAV5.CUST_ACCOUNT_PROFILE_ID = HCAA.CUST_ACCOUNT_PROFILE_ID
  AND HCAA.SITE_USE_ID              IS NULL
  AND HCAA.CUST_ACCOUNT_ID           = HCA.CUST_ACCOUNT_ID
  AND HCA.CUST_ACCOUNT_ID            = HCS.CUST_ACCOUNT_ID
  AND HCS.CUST_ACCT_SITE_ID          = HCSU.CUST_ACCT_SITE_ID(+)
  AND HCS.PARTY_SITE_ID              = HPS.PARTY_SITE_ID(+)
  AND NVL(HCSU.PRIMARY_FLAG,'N')     ='Y'
  AND HCSU.SITE_USE_CODE(+)          = 'BILL_TO'
  AND HCPA.AUDIT_TRANSACTION_TYPE    ='U'
  UNION ALL
  SELECT 'No' credit_report_flag,
    TRUNC(Hcpa.Audit_Timestamp) trunc_Audit_Date,
    HCPA.AUDIT_TIMESTAMP AUDIT_DATE,
    DECODE(Hcpa.Audit_Transaction_Type,'I','Insert') Audit_Type,
    Hcpa.Audit_User_Name User_Ntid,
    NVL(
    (SELECT Ppf.Full_Name
    FROM Fnd_User Fu,
      Per_All_People_F Ppf
    WHERE Fu.User_Name=Hcpa.Audit_User_Name
    AND Fu.Employee_Id=Ppf.person_id
    AND TRUNC(Sysdate) BETWEEN Ppf.Effective_Start_Date AND Ppf.Effective_End_Date
	AND rownum                 =1 --added for version 1.1
    ),Hcpa.Audit_User_Name) User_Name,
    Hca.ACCOUNT_Number,
    HCA.ACCOUNT_NAME,
    HPS.PARTY_SITE_NUMBER site_number,
    HCSU.LOCATION SITE_NAME,
    'Send Statement' ATRIBUTE_NAME,
    DECODE(HCPA.SEND_STATEMENTS,'Y','Yes','N','No') OLD_VALUE,
    DECODE(xxeis.EIS_XXWC_CUST_AUDIT_HIST_PKG.get_cust_insert_value('APPS.HZ_CUSTOMER_PROFILES_AV10','SEND_STATEMENTS',HCPA.CUST_ACCOUNT_PROFILE_ID,'CUST_ACCOUNT_PROFILE_ID'),'Y','Yes','N','No') new_value,
    'HZ_CUSTOMER_PROFILES' TABLE_NAME,
    (SELECT DESCRIPTION
    FROM FND_TABLES
    WHERE TABLE_NAME='HZ_CUSTOMER_PROFILES'
    AND ROWNUM      =1
    ) DESCRIPTION,
    DECODE(HCSU.site_use_id,NULL,
    (SELECT hcpcs.name
    FROM HZ_CUSTOMER_PROFILES hcpf,
      HZ_CUST_PROFILE_CLASSES hcpcs
    WHERE hcpf.profile_class_id=hcpcs.profile_class_id
    AND hcpf.cust_account_id   = HCA.CUST_ACCOUNT_ID
    AND hcpf.SITE_USE_ID      IS NULL
    AND rownum                 =1
    ),
    (SELECT hcpcs.name
    FROM HZ_CUSTOMER_PROFILES hcpf,
      HZ_CUST_PROFILE_CLASSES hcpcs
    WHERE hcpf.profile_class_id=hcpcs.profile_class_id
    AND hcpf.site_use_id       = HCSU.site_use_id
    AND hcpf.cust_account_id   = HCA.CUST_ACCOUNT_ID
    AND rownum                 =1
    )) profile_class
  FROM APPS.HZ_CUSTOMER_PROFILES_A HCPA,
    APPS.HZ_CUSTOMER_PROFILES HCAA,
    APPS.HZ_CUST_ACCOUNTS HCA ,
    HZ_CUST_ACCT_SITES_ALL HCS,
    HZ_CUST_SITE_USES_ALL HCSU,
    HZ_PARTY_SITES HPS
  WHERE HCPA.CUST_ACCOUNT_PROFILE_ID                                                                                                                                      = HCAA.CUST_ACCOUNT_PROFILE_ID
  AND HCAA.SITE_USE_ID                                                                                                                                                   IS NULL
  AND HCAA.CUST_ACCOUNT_ID                                                                                                                                                = HCA.CUST_ACCOUNT_ID
  AND HCA.CUST_ACCOUNT_ID                                                                                                                                                 = HCS.CUST_ACCOUNT_ID
  AND HCS.CUST_ACCT_SITE_ID                                                                                                                                               = HCSU.CUST_ACCT_SITE_ID(+)
  AND HCS.PARTY_SITE_ID                                                                                                                                                   = HPS.PARTY_SITE_ID(+)
  AND NVL(HCSU.PRIMARY_FLAG,'N')                                                                                                                                          ='Y'
  AND HCSU.SITE_USE_CODE(+)                                                                                                                                               = 'BILL_TO'
  AND HCPA.AUDIT_TRANSACTION_TYPE                                                                                                                                         ='I'
  AND XXEIS.EIS_XXWC_CUST_AUDIT_HIST_PKG.GET_CUST_INSERT_VALUE('APPS.HZ_CUSTOMER_PROFILES_AV10','SEND_STATEMENTS',HCPA.CUST_ACCOUNT_PROFILE_ID,'CUST_ACCOUNT_PROFILE_ID') IS NOT NULL
  UNION ALL
  SELECT 'No' credit_report_flag,
    TRUNC(Hcpa.Audit_Timestamp) trunc_Audit_Date,
    Hcpa.Audit_Timestamp Audit_Date,
    DECODE(Hcpa.Audit_Transaction_Type,'U','Update') Audit_Type,
    Hcpa.Audit_User_Name User_Ntid,
    NVL(
    (SELECT Ppf.Full_Name
    FROM Fnd_User Fu,
      Per_All_People_F Ppf
    WHERE Fu.User_Name=Hcpa.Audit_User_Name
    AND Fu.Employee_Id=Ppf.person_id
    AND TRUNC(Sysdate) BETWEEN Ppf.Effective_Start_Date AND Ppf.Effective_End_Date
	AND rownum                 =1 --added for version 1.1
    ),Hcpa.Audit_User_Name) User_Name,
    Hca.ACCOUNT_Number,
    HCA.ACCOUNT_NAME,
    HPS.PARTY_SITE_NUMBER site_number,
    HCSU.location SITE_NAME,
    'Credit Hold' ATRIBUTE_NAME,
    DECODE(HCPAV6.credit_hold,'Y','Yes','N','No') OLD_VALUE,
    DECODE(xxeis.EIS_XXWC_CUST_AUDIT_HIST_PKG.get_cust_new_value('APPS.HZ_CUSTOMER_PROFILES_AV9','CREDIT_HOLD',HCPA.ROW_KEY,HCPAV6.CUST_ACCOUNT_PROFILE_ID,'CUST_ACCOUNT_PROFILE_ID'),'Y','Yes','N','No') new_value,
    'HZ_CUSTOMER_PROFILES' TABLE_NAME,
    (SELECT DESCRIPTION
    FROM FND_TABLES
    WHERE TABLE_NAME='HZ_CUSTOMER_PROFILES'
    AND ROWNUM      =1
    ) DESCRIPTION,
    DECODE(HCSU.site_use_id,NULL,
    (SELECT hcpcs.name
    FROM HZ_CUSTOMER_PROFILES hcpf,
      HZ_CUST_PROFILE_CLASSES hcpcs
    WHERE hcpf.profile_class_id=hcpcs.profile_class_id
    AND hcpf.cust_account_id   = HCA.CUST_ACCOUNT_ID
    AND hcpf.SITE_USE_ID      IS NULL
    AND rownum                 =1
    ),
    (SELECT hcpcs.name
    FROM HZ_CUSTOMER_PROFILES hcpf,
      HZ_CUST_PROFILE_CLASSES hcpcs
    WHERE hcpf.profile_class_id=hcpcs.profile_class_id
    AND hcpf.site_use_id       = HCSU.site_use_id
    AND hcpf.cust_account_id   = HCA.CUST_ACCOUNT_ID
    AND rownum                 =1
    )) profile_class
  FROM APPS.HZ_CUSTOMER_PROFILES_A HCPA,
    APPS.HZ_CUSTOMER_PROFILES_AV9 HCPAV6,
    APPS.HZ_CUSTOMER_PROFILES HCAA,
    APPS.HZ_CUST_ACCOUNTS HCA ,
    HZ_CUST_ACCT_SITES_ALL HCS,
    HZ_CUST_SITE_USES_ALL HCSU,
    HZ_PARTY_SITES HPS
  WHERE Hcpa.ROW_KEY                 = HCPAV6.ROW_KEY
  AND HCPAV6.CUST_ACCOUNT_PROFILE_ID = HCAA.CUST_ACCOUNT_PROFILE_ID
  AND HCAA.SITE_USE_ID              IS NULL
  AND HCAA.CUST_ACCOUNT_ID           = HCA.CUST_ACCOUNT_ID
  AND HCA.CUST_ACCOUNT_ID            = HCS.CUST_ACCOUNT_ID
  AND HCS.CUST_ACCT_SITE_ID          = HCSU.CUST_ACCT_SITE_ID(+)
  AND HCS.PARTY_SITE_ID              = HPS.PARTY_SITE_ID(+)
  AND NVL(HCSU.PRIMARY_FLAG,'N')     ='Y'
  AND HCSU.SITE_USE_CODE(+)          = 'BILL_TO'
  AND HCPA.AUDIT_TRANSACTION_TYPE    ='U'
  UNION ALL
  SELECT 'No' credit_report_flag,
    TRUNC(Hcpa.Audit_Timestamp) trunc_Audit_Date,
    HCPA.AUDIT_TIMESTAMP AUDIT_DATE,
    DECODE(Hcpa.Audit_Transaction_Type,'I','Insert') Audit_Type,
    Hcpa.Audit_User_Name User_Ntid,
    NVL(
    (SELECT Ppf.Full_Name
    FROM Fnd_User Fu,
      Per_All_People_F Ppf
    WHERE Fu.User_Name=Hcpa.Audit_User_Name
    AND Fu.Employee_Id=Ppf.person_id
    AND TRUNC(Sysdate) BETWEEN Ppf.Effective_Start_Date AND Ppf.Effective_End_Date
	AND rownum                 =1 --added for version 1.1
    ),Hcpa.Audit_User_Name) User_Name,
    Hca.ACCOUNT_Number,
    HCA.ACCOUNT_NAME,
    HPS.PARTY_SITE_NUMBER site_number,
    HCSU.LOCATION SITE_NAME,
    'Credit Hold' ATRIBUTE_NAME,
    DECODE(HCPA.credit_hold,'Y','Yes','N','No') OLD_VALUE,
    DECODE(xxeis.EIS_XXWC_CUST_AUDIT_HIST_PKG.get_cust_insert_value('APPS.HZ_CUSTOMER_PROFILES_AV9','CREDIT_HOLD',HCPA.CUST_ACCOUNT_PROFILE_ID,'CUST_ACCOUNT_PROFILE_ID'),'Y','Yes','N','No') new_value,
    'HZ_CUSTOMER_PROFILES' TABLE_NAME,
    (SELECT DESCRIPTION
    FROM FND_TABLES
    WHERE TABLE_NAME='HZ_CUSTOMER_PROFILES'
    AND ROWNUM      =1
    ) DESCRIPTION,
    DECODE(HCSU.site_use_id,NULL,
    (SELECT hcpcs.name
    FROM HZ_CUSTOMER_PROFILES hcpf,
      HZ_CUST_PROFILE_CLASSES hcpcs
    WHERE hcpf.profile_class_id=hcpcs.profile_class_id
    AND hcpf.cust_account_id   = HCA.CUST_ACCOUNT_ID
    AND hcpf.SITE_USE_ID      IS NULL
    AND rownum                 =1
    ),
    (SELECT hcpcs.name
    FROM HZ_CUSTOMER_PROFILES hcpf,
      HZ_CUST_PROFILE_CLASSES hcpcs
    WHERE hcpf.profile_class_id=hcpcs.profile_class_id
    AND hcpf.site_use_id       = HCSU.site_use_id
    AND hcpf.cust_account_id   = HCA.CUST_ACCOUNT_ID
    AND rownum                 =1
    )) profile_class
  FROM APPS.HZ_CUSTOMER_PROFILES_A HCPA,
    APPS.HZ_CUSTOMER_PROFILES HCAA,
    APPS.HZ_CUST_ACCOUNTS HCA ,
    HZ_CUST_ACCT_SITES_ALL HCS,
    HZ_CUST_SITE_USES_ALL HCSU,
    HZ_PARTY_SITES HPS
  WHERE HCPA.CUST_ACCOUNT_PROFILE_ID                                                                                                                                  = HCAA.CUST_ACCOUNT_PROFILE_ID
  AND HCAA.SITE_USE_ID                                                                                                                                               IS NULL
  AND HCAA.CUST_ACCOUNT_ID                                                                                                                                            = HCA.CUST_ACCOUNT_ID
  AND HCA.CUST_ACCOUNT_ID                                                                                                                                             = HCS.CUST_ACCOUNT_ID
  AND HCS.CUST_ACCT_SITE_ID                                                                                                                                           = HCSU.CUST_ACCT_SITE_ID(+)
  AND HCS.PARTY_SITE_ID                                                                                                                                               = HPS.PARTY_SITE_ID(+)
  AND NVL(HCSU.PRIMARY_FLAG,'N')                                                                                                                                      ='Y'
  AND HCSU.SITE_USE_CODE(+)                                                                                                                                           = 'BILL_TO'
  AND HCPA.AUDIT_TRANSACTION_TYPE                                                                                                                                     ='I'
  AND XXEIS.EIS_XXWC_CUST_AUDIT_HIST_PKG.GET_CUST_INSERT_VALUE('APPS.HZ_CUSTOMER_PROFILES_AV9','credit_hold',HCPA.CUST_ACCOUNT_PROFILE_ID,'CUST_ACCOUNT_PROFILE_ID') IS NOT NULL
  UNION ALL
  SELECT 'No' credit_report_flag,
    TRUNC(Hcpa.Audit_Timestamp) trunc_Audit_Date,
    Hcpa.Audit_Timestamp Audit_Date,
    DECODE(Hcpa.Audit_Transaction_Type,'U','Update') Audit_Type,
    Hcpa.Audit_User_Name User_Ntid,
    NVL(
    (SELECT Ppf.Full_Name
    FROM Fnd_User Fu,
      Per_All_People_F Ppf
    WHERE Fu.User_Name=Hcpa.Audit_User_Name
    AND Fu.Employee_Id=Ppf.person_id
    AND TRUNC(Sysdate) BETWEEN Ppf.Effective_Start_Date AND Ppf.Effective_End_Date
	AND rownum                 =1 --added for version 1.1
    ),Hcpa.Audit_User_Name) User_Name,
    Hca.ACCOUNT_Number,
    HCA.ACCOUNT_NAME,
    HPS.PARTY_SITE_NUMBER site_number,
    HCSU.location SITE_NAME,
    'Credit Rating' ATRIBUTE_NAME,
    Xxeis.Eis_Xxwc_Cust_Audit_Hist_Pkg.Get_Lookup_Value(HCPAV7.CREDIT_RATING,'CREDIT_RATING') OLD_VALUE,
    Xxeis.Eis_Xxwc_Cust_Audit_Hist_Pkg.Get_Lookup_Value(xxeis.EIS_XXWC_CUST_AUDIT_HIST_PKG.get_cust_new_value('APPS.HZ_CUSTOMER_PROFILES_AV8','CREDIT_RATING',HCPA.ROW_KEY,HCPAV7.CUST_ACCOUNT_PROFILE_ID,'CUST_ACCOUNT_PROFILE_ID'),'CREDIT_RATING') new_value,
    'HZ_CUSTOMER_PROFILES' TABLE_NAME,
    (SELECT DESCRIPTION
    FROM FND_TABLES
    WHERE TABLE_NAME='HZ_CUSTOMER_PROFILES'
    AND ROWNUM      =1
    ) DESCRIPTION,
    DECODE(HCSU.site_use_id,NULL,
    (SELECT hcpcs.name
    FROM HZ_CUSTOMER_PROFILES hcpf,
      HZ_CUST_PROFILE_CLASSES hcpcs
    WHERE hcpf.profile_class_id=hcpcs.profile_class_id
    AND hcpf.cust_account_id   = HCA.CUST_ACCOUNT_ID
    AND hcpf.SITE_USE_ID      IS NULL
    AND rownum                 =1
    ),
    (SELECT hcpcs.name
    FROM HZ_CUSTOMER_PROFILES hcpf,
      HZ_CUST_PROFILE_CLASSES hcpcs
    WHERE hcpf.profile_class_id=hcpcs.profile_class_id
    AND hcpf.site_use_id       = HCSU.site_use_id
    AND hcpf.cust_account_id   = HCA.CUST_ACCOUNT_ID
    AND rownum                 =1
    )) profile_class
  FROM APPS.HZ_CUSTOMER_PROFILES_A HCPA,
    APPS.HZ_CUSTOMER_PROFILES_AV8 HCPAV7,
    APPS.HZ_CUSTOMER_PROFILES HCAA,
    APPS.HZ_CUST_ACCOUNTS HCA ,
    HZ_CUST_ACCT_SITES_ALL HCS,
    HZ_CUST_SITE_USES_ALL HCSU,
    HZ_PARTY_SITES HPS
  WHERE Hcpa.ROW_KEY                 = HCPAV7.ROW_KEY
  AND HCPAV7.CUST_ACCOUNT_PROFILE_ID = HCAA.CUST_ACCOUNT_PROFILE_ID
  AND HCAA.SITE_USE_ID              IS NULL
  AND HCAA.CUST_ACCOUNT_ID           = HCA.CUST_ACCOUNT_ID
  AND HCA.CUST_ACCOUNT_ID            = HCS.CUST_ACCOUNT_ID
  AND HCS.CUST_ACCT_SITE_ID          = HCSU.CUST_ACCT_SITE_ID(+)
  AND HCS.PARTY_SITE_ID              = HPS.PARTY_SITE_ID(+)
  AND NVL(HCSU.PRIMARY_FLAG,'N')     ='Y'
  AND HCSU.SITE_USE_CODE(+)          = 'BILL_TO'
  AND HCPA.AUDIT_TRANSACTION_TYPE    ='U'
  UNION ALL
  SELECT 'No' credit_report_flag,
    TRUNC(Hcpa.Audit_Timestamp) trunc_Audit_Date,
    HCPA.AUDIT_TIMESTAMP AUDIT_DATE,
    DECODE(Hcpa.Audit_Transaction_Type,'I','Insert') Audit_Type,
    Hcpa.Audit_User_Name User_Ntid,
    NVL(
    (SELECT Ppf.Full_Name
    FROM Fnd_User Fu,
      Per_All_People_F Ppf
    WHERE Fu.User_Name=Hcpa.Audit_User_Name
    AND Fu.Employee_Id=Ppf.person_id
    AND TRUNC(Sysdate) BETWEEN Ppf.Effective_Start_Date AND Ppf.Effective_End_Date
	AND rownum                 =1 --added for version 1.1
    ),Hcpa.Audit_User_Name) User_Name,
    Hca.ACCOUNT_Number,
    HCA.ACCOUNT_NAME,
    HPS.PARTY_SITE_NUMBER site_number,
    HCSU.LOCATION SITE_NAME,
    'Credit Rating' ATRIBUTE_NAME,
    Xxeis.Eis_Xxwc_Cust_Audit_Hist_Pkg.Get_Lookup_Value(HCPA.CREDIT_RATING,'CREDIT_RATING') OLD_VALUE,
    Xxeis.Eis_Xxwc_Cust_Audit_Hist_Pkg.Get_Lookup_Value(xxeis.EIS_XXWC_CUST_AUDIT_HIST_PKG.get_cust_insert_value('APPS.HZ_CUSTOMER_PROFILES_AV8','CREDIT_RATING',HCPA.CUST_ACCOUNT_PROFILE_ID,'CUST_ACCOUNT_PROFILE_ID'),'CREDIT_RATING') new_value,
    'HZ_CUSTOMER_PROFILES' TABLE_NAME,
    (SELECT DESCRIPTION
    FROM FND_TABLES
    WHERE TABLE_NAME='HZ_CUSTOMER_PROFILES'
    AND ROWNUM      =1
    ) DESCRIPTION,
    DECODE(HCSU.site_use_id,NULL,
    (SELECT hcpcs.name
    FROM HZ_CUSTOMER_PROFILES hcpf,
      HZ_CUST_PROFILE_CLASSES hcpcs
    WHERE hcpf.profile_class_id=hcpcs.profile_class_id
    AND hcpf.cust_account_id   = HCA.CUST_ACCOUNT_ID
    AND hcpf.SITE_USE_ID      IS NULL
    AND rownum                 =1
    ),
    (SELECT hcpcs.name
    FROM HZ_CUSTOMER_PROFILES hcpf,
      HZ_CUST_PROFILE_CLASSES hcpcs
    WHERE hcpf.profile_class_id=hcpcs.profile_class_id
    AND hcpf.site_use_id       = HCSU.site_use_id
    AND hcpf.cust_account_id   = HCA.CUST_ACCOUNT_ID
    AND rownum                 =1
    )) profile_class
  FROM APPS.HZ_CUSTOMER_PROFILES_A HCPA,
    APPS.HZ_CUSTOMER_PROFILES HCAA,
    APPS.HZ_CUST_ACCOUNTS HCA ,
    HZ_CUST_ACCT_SITES_ALL HCS,
    HZ_CUST_SITE_USES_ALL HCSU,
    HZ_PARTY_SITES HPS
  WHERE HCPA.CUST_ACCOUNT_PROFILE_ID                                                                                                                                    = HCAA.CUST_ACCOUNT_PROFILE_ID
  AND HCAA.SITE_USE_ID                                                                                                                                                 IS NULL
  AND HCAA.CUST_ACCOUNT_ID                                                                                                                                              = HCA.CUST_ACCOUNT_ID
  AND HCA.CUST_ACCOUNT_ID                                                                                                                                               = HCS.CUST_ACCOUNT_ID
  AND HCS.CUST_ACCT_SITE_ID                                                                                                                                             = HCSU.CUST_ACCT_SITE_ID(+)
  AND HCS.PARTY_SITE_ID                                                                                                                                                 = HPS.PARTY_SITE_ID(+)
  AND NVL(HCSU.PRIMARY_FLAG,'N')                                                                                                                                        ='Y'
  AND HCSU.SITE_USE_CODE(+)                                                                                                                                             = 'BILL_TO'
  AND HCPA.AUDIT_TRANSACTION_TYPE                                                                                                                                       ='I'
  AND XXEIS.EIS_XXWC_CUST_AUDIT_HIST_PKG.GET_CUST_INSERT_VALUE('APPS.HZ_CUSTOMER_PROFILES_AV8','CREDIT_RATING',HCPA.CUST_ACCOUNT_PROFILE_ID,'CUST_ACCOUNT_PROFILE_ID') IS NOT NULL
  UNION ALL
  SELECT 'No' credit_report_flag,
    TRUNC(Hcpa.Audit_Timestamp) trunc_Audit_Date,
    Hcpa.Audit_Timestamp Audit_Date,
    DECODE(Hcpa.Audit_Transaction_Type,'U','Update') Audit_Type,
    Hcpa.Audit_User_Name User_Ntid,
    NVL(
    (SELECT Ppf.Full_Name
    FROM Fnd_User Fu,
      Per_All_People_F Ppf
    WHERE Fu.User_Name=Hcpa.Audit_User_Name
    AND Fu.Employee_Id=Ppf.person_id
    AND TRUNC(Sysdate) BETWEEN Ppf.Effective_Start_Date AND Ppf.Effective_End_Date
	AND rownum                 =1 --added for version 1.1
    ),Hcpa.Audit_User_Name) User_Name,
    Hca.ACCOUNT_Number,
    HCA.ACCOUNT_NAME,
    HPS.PARTY_SITE_NUMBER site_number,
    HCSU.location SITE_NAME,
    'Risk Code' ATRIBUTE_NAME,
    HCPAV8.RISK_CODE OLD_VALUE,
    xxeis.EIS_XXWC_CUST_AUDIT_HIST_PKG.get_cust_new_value('APPS.HZ_CUSTOMER_PROFILES_AV7','RISK_CODE',HCPA.ROW_KEY,HCPAV8.CUST_ACCOUNT_PROFILE_ID,'CUST_ACCOUNT_PROFILE_ID') new_value,
    'HZ_CUSTOMER_PROFILES' TABLE_NAME,
    (SELECT DESCRIPTION
    FROM FND_TABLES
    WHERE TABLE_NAME='HZ_CUSTOMER_PROFILES'
    AND ROWNUM      =1
    ) DESCRIPTION,
    DECODE(HCSU.site_use_id,NULL,
    (SELECT hcpcs.name
    FROM HZ_CUSTOMER_PROFILES hcpf,
      HZ_CUST_PROFILE_CLASSES hcpcs
    WHERE hcpf.profile_class_id=hcpcs.profile_class_id
    AND hcpf.cust_account_id   = HCA.CUST_ACCOUNT_ID
    AND hcpf.SITE_USE_ID      IS NULL
    AND rownum                 =1
    ),
    (SELECT hcpcs.name
    FROM HZ_CUSTOMER_PROFILES hcpf,
      HZ_CUST_PROFILE_CLASSES hcpcs
    WHERE hcpf.profile_class_id=hcpcs.profile_class_id
    AND hcpf.site_use_id       = HCSU.site_use_id
    AND hcpf.cust_account_id   = HCA.CUST_ACCOUNT_ID
    AND rownum                 =1
    )) profile_class
  FROM APPS.HZ_CUSTOMER_PROFILES_A HCPA,
    APPS.HZ_CUSTOMER_PROFILES_AV7 HCPAV8,
    APPS.HZ_CUSTOMER_PROFILES HCAA,
    APPS.HZ_CUST_ACCOUNTS HCA ,
    HZ_CUST_ACCT_SITES_ALL HCS,
    HZ_CUST_SITE_USES_ALL HCSU,
    HZ_PARTY_SITES HPS
  WHERE Hcpa.ROW_KEY                 = HCPAV8.ROW_KEY
  AND HCPAV8.CUST_ACCOUNT_PROFILE_ID = HCAA.CUST_ACCOUNT_PROFILE_ID
  AND HCAA.SITE_USE_ID              IS NULL
  AND HCAA.CUST_ACCOUNT_ID           = HCA.CUST_ACCOUNT_ID
  AND HCA.CUST_ACCOUNT_ID            = HCS.CUST_ACCOUNT_ID
  AND HCS.CUST_ACCT_SITE_ID          = HCSU.CUST_ACCT_SITE_ID(+)
  AND HCS.PARTY_SITE_ID              = HPS.PARTY_SITE_ID(+)
  AND NVL(HCSU.PRIMARY_FLAG,'N')     ='Y'
  AND HCSU.SITE_USE_CODE(+)          = 'BILL_TO'
  AND HCPA.AUDIT_TRANSACTION_TYPE    ='U'
  UNION ALL
  SELECT 'No' credit_report_flag,
    TRUNC(Hcpa.Audit_Timestamp) trunc_Audit_Date,
    HCPA.AUDIT_TIMESTAMP AUDIT_DATE,
    DECODE(Hcpa.Audit_Transaction_Type,'I','Insert') Audit_Type,
    Hcpa.Audit_User_Name User_Ntid,
    NVL(
    (SELECT Ppf.Full_Name
    FROM Fnd_User Fu,
      Per_All_People_F Ppf
    WHERE Fu.User_Name=Hcpa.Audit_User_Name
    AND Fu.Employee_Id=Ppf.person_id
    AND TRUNC(Sysdate) BETWEEN Ppf.Effective_Start_Date AND Ppf.Effective_End_Date
	AND rownum                 =1 --added for version 1.1
    ),Hcpa.Audit_User_Name) User_Name,
    Hca.ACCOUNT_Number,
    HCA.ACCOUNT_NAME,
    HPS.PARTY_SITE_NUMBER site_number,
    HCSU.LOCATION SITE_NAME,
    'Risk Code' ATRIBUTE_NAME,
    HCPA.RISK_CODE OLD_VALUE,
    xxeis.EIS_XXWC_CUST_AUDIT_HIST_PKG.get_cust_insert_value('APPS.HZ_CUSTOMER_PROFILES_AV7','RISK_CODE',HCPA.CUST_ACCOUNT_PROFILE_ID,'CUST_ACCOUNT_PROFILE_ID') new_value,
    'HZ_CUSTOMER_PROFILES' TABLE_NAME,
    (SELECT DESCRIPTION
    FROM FND_TABLES
    WHERE TABLE_NAME='HZ_CUSTOMER_PROFILES'
    AND ROWNUM      =1
    ) DESCRIPTION,
    DECODE(HCSU.site_use_id,NULL,
    (SELECT hcpcs.name
    FROM HZ_CUSTOMER_PROFILES hcpf,
      HZ_CUST_PROFILE_CLASSES hcpcs
    WHERE hcpf.profile_class_id=hcpcs.profile_class_id
    AND hcpf.cust_account_id   = HCA.CUST_ACCOUNT_ID
    AND hcpf.SITE_USE_ID      IS NULL
    AND rownum                 =1
    ),
    (SELECT hcpcs.name
    FROM HZ_CUSTOMER_PROFILES hcpf,
      HZ_CUST_PROFILE_CLASSES hcpcs
    WHERE hcpf.profile_class_id=hcpcs.profile_class_id
    AND hcpf.site_use_id       = HCSU.site_use_id
    AND hcpf.cust_account_id   = HCA.CUST_ACCOUNT_ID
    AND rownum                 =1
    )) profile_class
  FROM APPS.HZ_CUSTOMER_PROFILES_A HCPA,
    APPS.HZ_CUSTOMER_PROFILES HCAA,
    APPS.HZ_CUST_ACCOUNTS HCA ,
    HZ_CUST_ACCT_SITES_ALL HCS,
    HZ_CUST_SITE_USES_ALL HCSU,
    HZ_PARTY_SITES HPS
  WHERE HCPA.CUST_ACCOUNT_PROFILE_ID                                                                                                                                = HCAA.CUST_ACCOUNT_PROFILE_ID
  AND HCAA.SITE_USE_ID                                                                                                                                             IS NULL
  AND HCAA.CUST_ACCOUNT_ID                                                                                                                                          = HCA.CUST_ACCOUNT_ID
  AND HCA.CUST_ACCOUNT_ID                                                                                                                                           = HCS.CUST_ACCOUNT_ID
  AND HCS.CUST_ACCT_SITE_ID                                                                                                                                         = HCSU.CUST_ACCT_SITE_ID(+)
  AND HCS.PARTY_SITE_ID                                                                                                                                             = HPS.PARTY_SITE_ID(+)
  AND NVL(HCSU.PRIMARY_FLAG,'N')                                                                                                                                    ='Y'
  AND HCSU.SITE_USE_CODE(+)                                                                                                                                         = 'BILL_TO'
  AND HCPA.AUDIT_TRANSACTION_TYPE                                                                                                                                   ='I'
  AND Xxeis.Eis_Xxwc_Cust_Audit_Hist_Pkg.Get_Cust_Insert_Value('APPS.HZ_CUSTOMER_PROFILES_AV7','RISK_CODE',Hcpa.Cust_Account_Profile_Id,'CUST_ACCOUNT_PROFILE_ID') IS NOT NULL
  UNION ALL
  SELECT 'No' credit_report_flag,
    TRUNC(Hcpa.Audit_Timestamp) trunc_Audit_Date,
    Hcpa.Audit_Timestamp Audit_Date,
    DECODE(Hcpa.Audit_Transaction_Type,'U','Update') Audit_Type,
    Hcpa.Audit_User_Name User_Ntid,
    NVL(
    (SELECT Ppf.Full_Name
    FROM Fnd_User Fu,
      Per_All_People_F Ppf
    WHERE Fu.User_Name=Hcpa.Audit_User_Name
    AND Fu.Employee_Id=Ppf.person_id
    AND TRUNC(Sysdate) BETWEEN Ppf.Effective_Start_Date AND Ppf.Effective_End_Date
	AND rownum                 =1 --added for version 1.1
    ),Hcpa.Audit_User_Name) User_Name,
    Hca.ACCOUNT_Number,
    HCA.ACCOUNT_NAME,
    HPS.PARTY_SITE_NUMBER site_number,
    Hcsu.Location Site_Name,
    'Payment Terms' Atribute_Name,
    (SELECT Rt.Name
    FROM Apps.Ra_Terms Rt
    WHERE RT.TERM_ID =HCPAV9.STANDARD_TERMS
    ) OLD_VALUE,
    (SELECT Rt.Name
    FROM Apps.Ra_Terms Rt
    WHERE RT.TERM_ID = xxeis.EIS_XXWC_CUST_AUDIT_HIST_PKG.get_cust_new_value('APPS.HZ_CUSTOMER_PROFILES_AV2','STANDARD_TERMS',HCPA.ROW_KEY,HCPAV9.CUST_ACCOUNT_PROFILE_ID,'CUST_ACCOUNT_PROFILE_ID')
    ) new_value,
    'HZ_CUSTOMER_PROFILES' TABLE_NAME,
    (SELECT DESCRIPTION
    FROM FND_TABLES
    WHERE TABLE_NAME='HZ_CUSTOMER_PROFILES'
    AND ROWNUM      =1
    ) DESCRIPTION,
    DECODE(HCSU.site_use_id,NULL,
    (SELECT hcpcs.name
    FROM HZ_CUSTOMER_PROFILES hcpf,
      HZ_CUST_PROFILE_CLASSES hcpcs
    WHERE hcpf.profile_class_id=hcpcs.profile_class_id
    AND hcpf.cust_account_id   = HCA.CUST_ACCOUNT_ID
    AND hcpf.SITE_USE_ID      IS NULL
    AND rownum                 =1
    ),
    (SELECT hcpcs.name
    FROM HZ_CUSTOMER_PROFILES hcpf,
      HZ_CUST_PROFILE_CLASSES hcpcs
    WHERE hcpf.profile_class_id=hcpcs.profile_class_id
    AND hcpf.site_use_id       = HCSU.site_use_id
    AND hcpf.cust_account_id   = HCA.CUST_ACCOUNT_ID
    AND rownum                 =1
    )) profile_class
  FROM APPS.HZ_CUSTOMER_PROFILES_A HCPA,
    APPS.HZ_CUSTOMER_PROFILES_AV2 HCPAV9,
    APPS.HZ_CUSTOMER_PROFILES HCAA,
    APPS.HZ_CUST_ACCOUNTS HCA ,
    HZ_CUST_ACCT_SITES_ALL HCS,
    HZ_CUST_SITE_USES_ALL HCSU,
    HZ_PARTY_SITES HPS
  WHERE Hcpa.ROW_KEY                 = HCPAV9.ROW_KEY
  AND HCPAV9.CUST_ACCOUNT_PROFILE_ID = HCAA.CUST_ACCOUNT_PROFILE_ID
  AND HCAA.SITE_USE_ID              IS NULL
  AND HCAA.CUST_ACCOUNT_ID           = HCA.CUST_ACCOUNT_ID
  AND HCA.CUST_ACCOUNT_ID            = HCS.CUST_ACCOUNT_ID
  AND HCS.CUST_ACCT_SITE_ID          = HCSU.CUST_ACCT_SITE_ID(+)
  AND HCS.PARTY_SITE_ID              = HPS.PARTY_SITE_ID(+)
  AND NVL(HCSU.PRIMARY_FLAG,'N')     ='Y'
  AND HCSU.SITE_USE_CODE(+)          = 'BILL_TO'
  AND HCPA.AUDIT_TRANSACTION_TYPE    ='U'
  UNION ALL
  SELECT 'No' credit_report_flag,
    TRUNC(Hcpa.Audit_Timestamp) trunc_Audit_Date,
    HCPA.AUDIT_TIMESTAMP AUDIT_DATE,
    DECODE(Hcpa.Audit_Transaction_Type,'I','Insert') Audit_Type,
    Hcpa.Audit_User_Name User_Ntid,
    NVL(
    (SELECT Ppf.Full_Name
    FROM Fnd_User Fu,
      Per_All_People_F Ppf
    WHERE Fu.User_Name=Hcpa.Audit_User_Name
    AND Fu.Employee_Id=Ppf.person_id
    AND TRUNC(Sysdate) BETWEEN Ppf.Effective_Start_Date AND Ppf.Effective_End_Date
	AND rownum                 =1 --added for version 1.1
    ),Hcpa.Audit_User_Name) User_Name,
    Hca.ACCOUNT_Number,
    HCA.ACCOUNT_NAME,
    HPS.PARTY_SITE_NUMBER site_number,
    HCSU.LOCATION SITE_NAME,
    'Payment Terms' ATRIBUTE_NAME,
    ( SELECT Rt.Name FROM Apps.Ra_Terms Rt WHERE RT.TERM_ID =HCPA.STANDARD_TERMS
    ) OLD_VALUE,
    (SELECT Rt.Name
    FROM Apps.Ra_Terms Rt
    WHERE RT.TERM_ID = xxeis.EIS_XXWC_CUST_AUDIT_HIST_PKG.get_cust_insert_value('APPS.HZ_CUSTOMER_PROFILES_AV2','STANDARD_TERMS',HCPA.CUST_ACCOUNT_PROFILE_ID,'CUST_ACCOUNT_PROFILE_ID')
    ) new_value,
    'HZ_CUSTOMER_PROFILES' TABLE_NAME,
    (SELECT DESCRIPTION
    FROM FND_TABLES
    WHERE TABLE_NAME='HZ_CUSTOMER_PROFILES'
    AND ROWNUM      =1
    ) DESCRIPTION,
    DECODE(HCSU.site_use_id,NULL,
    (SELECT hcpcs.name
    FROM HZ_CUSTOMER_PROFILES hcpf,
      HZ_CUST_PROFILE_CLASSES hcpcs
    WHERE hcpf.profile_class_id=hcpcs.profile_class_id
    AND hcpf.cust_account_id   = HCA.CUST_ACCOUNT_ID
    AND hcpf.SITE_USE_ID      IS NULL
    AND rownum                 =1
    ),
    (SELECT hcpcs.name
    FROM HZ_CUSTOMER_PROFILES hcpf,
      HZ_CUST_PROFILE_CLASSES hcpcs
    WHERE hcpf.profile_class_id=hcpcs.profile_class_id
    AND hcpf.site_use_id       = HCSU.site_use_id
    AND hcpf.cust_account_id   = HCA.CUST_ACCOUNT_ID
    AND rownum                 =1
    )) profile_class
  FROM APPS.HZ_CUSTOMER_PROFILES_A HCPA,
    APPS.HZ_CUSTOMER_PROFILES HCAA,
    APPS.HZ_CUST_ACCOUNTS HCA ,
    HZ_CUST_ACCT_SITES_ALL HCS,
    HZ_CUST_SITE_USES_ALL HCSU,
    HZ_PARTY_SITES HPS
  WHERE HCPA.CUST_ACCOUNT_PROFILE_ID                                                                                                                                     = HCAA.CUST_ACCOUNT_PROFILE_ID
  AND HCAA.SITE_USE_ID                                                                                                                                                  IS NULL
  AND HCAA.CUST_ACCOUNT_ID                                                                                                                                               = HCA.CUST_ACCOUNT_ID
  AND HCA.CUST_ACCOUNT_ID                                                                                                                                                = HCS.CUST_ACCOUNT_ID
  AND HCS.CUST_ACCT_SITE_ID                                                                                                                                              = HCSU.CUST_ACCT_SITE_ID(+)
  AND HCS.PARTY_SITE_ID                                                                                                                                                  = HPS.PARTY_SITE_ID(+)
  AND NVL(HCSU.PRIMARY_FLAG,'N')                                                                                                                                         ='Y'
  AND HCSU.SITE_USE_CODE(+)                                                                                                                                              = 'BILL_TO'
  AND Hcpa.Audit_Transaction_Type                                                                                                                                        ='I'
  AND XXEIS.EIS_XXWC_CUST_AUDIT_HIST_PKG.GET_CUST_INSERT_VALUE('APPS.HZ_CUSTOMER_PROFILES_AV2','STANDARD_TERMS',HCPA.CUST_ACCOUNT_PROFILE_ID,'CUST_ACCOUNT_PROFILE_ID') IS NOT NULL
  UNION ALL
  SELECT 'No' credit_report_flag,
    TRUNC(Hcpa.Audit_Timestamp) trunc_Audit_Date,
    Hcpa.Audit_Timestamp Audit_Date,
    DECODE(Hcpa.Audit_Transaction_Type,'U','Update') Audit_Type,
    Hcpa.Audit_User_Name User_Ntid,
    NVL(
    (SELECT Ppf.Full_Name
    FROM Fnd_User Fu,
      Per_All_People_F Ppf
    WHERE Fu.User_Name=Hcpa.Audit_User_Name
    AND Fu.Employee_Id=Ppf.person_id
    AND TRUNC(Sysdate) BETWEEN Ppf.Effective_Start_Date AND Ppf.Effective_End_Date
	AND rownum                 =1 --added for version 1.1
    ),Hcpa.Audit_User_Name) User_Name,
    Hca.ACCOUNT_Number,
    HCA.ACCOUNT_NAME,
    HPS.PARTY_SITE_NUMBER site_number,
    Hcsu.Location Site_Name,
    'Account Status' Atribute_Name,
    Xxeis.Eis_Xxwc_Cust_Audit_Hist_Pkg.Get_Lookup_Value(Hcpav9.Account_Status,'ACCOUNT_STATUS') Old_Value,
    Xxeis.Eis_Xxwc_Cust_Audit_Hist_Pkg.Get_Lookup_Value(xxeis.EIS_XXWC_CUST_AUDIT_HIST_PKG.get_cust_new_value('APPS.HZ_CUSTOMER_PROFILES_AV6','ACCOUNT_STATUS',HCPA.ROW_KEY,HCPAV9.CUST_ACCOUNT_PROFILE_ID,'CUST_ACCOUNT_PROFILE_ID'),'ACCOUNT_STATUS' ) new_value,
    'HZ_CUSTOMER_PROFILES' TABLE_NAME,
    (SELECT DESCRIPTION
    FROM FND_TABLES
    WHERE TABLE_NAME='HZ_CUSTOMER_PROFILES'
    AND ROWNUM      =1
    ) DESCRIPTION,
    DECODE(HCSU.site_use_id,NULL,
    (SELECT hcpcs.name
    FROM HZ_CUSTOMER_PROFILES hcpf,
      HZ_CUST_PROFILE_CLASSES hcpcs
    WHERE hcpf.profile_class_id=hcpcs.profile_class_id
    AND hcpf.cust_account_id   = HCA.CUST_ACCOUNT_ID
    AND hcpf.SITE_USE_ID      IS NULL
    AND rownum                 =1
    ),
    (SELECT hcpcs.name
    FROM HZ_CUSTOMER_PROFILES hcpf,
      HZ_CUST_PROFILE_CLASSES hcpcs
    WHERE hcpf.profile_class_id=hcpcs.profile_class_id
    AND hcpf.site_use_id       = HCSU.site_use_id
    AND hcpf.cust_account_id   = HCA.CUST_ACCOUNT_ID
    AND rownum                 =1
    )) profile_class
  FROM APPS.HZ_CUSTOMER_PROFILES_A HCPA,
    APPS.HZ_CUSTOMER_PROFILES_AV6 HCPAV9,
    APPS.HZ_CUSTOMER_PROFILES HCAA,
    APPS.HZ_CUST_ACCOUNTS HCA ,
    HZ_CUST_ACCT_SITES_ALL HCS,
    HZ_CUST_SITE_USES_ALL HCSU,
    HZ_PARTY_SITES HPS
  WHERE Hcpa.ROW_KEY                 = HCPAV9.ROW_KEY
  AND HCPAV9.CUST_ACCOUNT_PROFILE_ID = HCAA.CUST_ACCOUNT_PROFILE_ID
  AND HCAA.SITE_USE_ID              IS NULL
  AND HCAA.CUST_ACCOUNT_ID           = HCA.CUST_ACCOUNT_ID
  AND HCA.CUST_ACCOUNT_ID            = HCS.CUST_ACCOUNT_ID
  AND HCS.CUST_ACCT_SITE_ID          = HCSU.CUST_ACCT_SITE_ID(+)
  AND HCS.PARTY_SITE_ID              = HPS.PARTY_SITE_ID(+)
  AND NVL(HCSU.PRIMARY_FLAG,'N')     ='Y'
  AND HCSU.SITE_USE_CODE(+)          = 'BILL_TO'
  AND HCPA.AUDIT_TRANSACTION_TYPE    ='U'
  UNION ALL
  SELECT 'No' credit_report_flag,
    TRUNC(Hcpa.Audit_Timestamp) trunc_Audit_Date,
    HCPA.AUDIT_TIMESTAMP AUDIT_DATE,
    DECODE(Hcpa.Audit_Transaction_Type,'I','Insert') Audit_Type,
    Hcpa.Audit_User_Name User_Ntid,
    NVL(
    (SELECT Ppf.Full_Name
    FROM Fnd_User Fu,
      Per_All_People_F Ppf
    WHERE Fu.User_Name=Hcpa.Audit_User_Name
    AND Fu.Employee_Id=Ppf.person_id
    AND TRUNC(Sysdate) BETWEEN Ppf.Effective_Start_Date AND Ppf.Effective_End_Date
	AND rownum                 =1 --added for version 1.1
    ),Hcpa.Audit_User_Name) User_Name,
    Hca.ACCOUNT_Number,
    HCA.ACCOUNT_NAME,
    HPS.PARTY_SITE_NUMBER site_number,
    HCSU.LOCATION SITE_NAME,
    'Account Status' Atribute_Name,
    Xxeis.Eis_Xxwc_Cust_Audit_Hist_Pkg.Get_Lookup_Value(Hcpa.Account_Status ,'ACCOUNT_STATUS' ) Old_Value,
    Xxeis.Eis_Xxwc_Cust_Audit_Hist_Pkg.Get_Lookup_Value(xxeis.EIS_XXWC_CUST_AUDIT_HIST_PKG.get_cust_insert_value('APPS.HZ_CUSTOMER_PROFILES_AV6','ACCOUNT_STATUS',HCPA.CUST_ACCOUNT_PROFILE_ID,'CUST_ACCOUNT_PROFILE_ID'),'ACCOUNT_STATUS' ) new_value,
    'HZ_CUSTOMER_PROFILES' TABLE_NAME,
    (SELECT DESCRIPTION
    FROM FND_TABLES
    WHERE TABLE_NAME='HZ_CUSTOMER_PROFILES'
    AND ROWNUM      =1
    ) DESCRIPTION,
    DECODE(HCSU.site_use_id,NULL,
    (SELECT hcpcs.name
    FROM HZ_CUSTOMER_PROFILES hcpf,
      HZ_CUST_PROFILE_CLASSES hcpcs
    WHERE hcpf.profile_class_id=hcpcs.profile_class_id
    AND hcpf.cust_account_id   = HCA.CUST_ACCOUNT_ID
    AND hcpf.SITE_USE_ID      IS NULL
    AND rownum                 =1
    ),
    (SELECT hcpcs.name
    FROM HZ_CUSTOMER_PROFILES hcpf,
      HZ_CUST_PROFILE_CLASSES hcpcs
    WHERE hcpf.profile_class_id=hcpcs.profile_class_id
    AND hcpf.site_use_id       = HCSU.site_use_id
    AND hcpf.cust_account_id   = HCA.CUST_ACCOUNT_ID
    AND rownum                 =1
    )) profile_class
  FROM APPS.HZ_CUSTOMER_PROFILES_A HCPA,
    APPS.HZ_CUSTOMER_PROFILES HCAA,
    APPS.HZ_CUST_ACCOUNTS HCA ,
    HZ_CUST_ACCT_SITES_ALL HCS,
    HZ_CUST_SITE_USES_ALL HCSU,
    HZ_PARTY_SITES HPS
  WHERE HCPA.CUST_ACCOUNT_PROFILE_ID                                                                                                                                      = HCAA.CUST_ACCOUNT_PROFILE_ID
  AND HCAA.SITE_USE_ID                                                                                                                                                   IS NULL
  AND HCAA.CUST_ACCOUNT_ID                                                                                                                                                = HCA.CUST_ACCOUNT_ID
  AND HCA.CUST_ACCOUNT_ID                                                                                                                                                 = HCS.CUST_ACCOUNT_ID
  AND HCS.CUST_ACCT_SITE_ID                                                                                                                                               = HCSU.CUST_ACCT_SITE_ID(+)
  AND HCS.PARTY_SITE_ID                                                                                                                                                   = HPS.PARTY_SITE_ID(+)
  AND NVL(HCSU.PRIMARY_FLAG,'N')                                                                                                                                          ='Y'
  AND HCSU.SITE_USE_CODE(+)                                                                                                                                               = 'BILL_TO'
  AND HCPA.AUDIT_TRANSACTION_TYPE                                                                                                                                         ='I'
  AND XXEIS.EIS_XXWC_CUST_AUDIT_HIST_PKG.GET_CUST_INSERT_VALUE('APPS.HZ_CUSTOMER_PROFILES_AV6','ACCOUNT_STATUS',HCPA.CUST_ACCOUNT_PROFILE_ID,'CUST_ACCOUNT_PROFILE_ID') IS NOT NULL
  UNION ALL
  SELECT 'No' credit_report_flag,
    TRUNC(HCSUA.Audit_Timestamp) trunc_Audit_Date,
    HCSUA.Audit_Timestamp Audit_Date,
    DECODE(HCSUA.Audit_Transaction_Type,'U','Update') Audit_Type,
    HCSUA.Audit_User_Name User_Ntid,
    NVL(
    (SELECT Ppf.Full_Name
    FROM Fnd_User Fu,
      Per_All_People_F Ppf
    WHERE Fu.User_Name=HCSUA.Audit_User_Name
    AND Fu.Employee_Id=Ppf.person_id
    AND TRUNC(Sysdate) BETWEEN Ppf.Effective_Start_Date AND Ppf.Effective_End_Date
	AND rownum                 =1 --added for version 1.1
    ),HCSUA.Audit_User_Name) User_Name,
    Hca.ACCOUNT_Number,
    HCA.ACCOUNT_NAME,
    HPS.PARTY_SITE_NUMBER site_number,
    Hcsu.Location Site_Name,
    'Primary Flag' ATRIBUTE_NAME,
    DECODE(HCSUA3.PRIMARY_FLAG,'Y','Yes','N','No') Old_Value,
    DECODE(xxeis.EIS_XXWC_CUST_AUDIT_HIST_PKG.get_cust_new_value('APPS.HZ_CUST_SITE_USES_ALL_AV6','PRIMARY_FLAG',HCSUA.ROW_KEY,HCSUA3.SITE_USE_ID,'SITE_USE_ID'),'Y','Yes','N','No') new_value,
    'HZ_CUST_SITE_USES_ALL' TABLE_NAME,
    (SELECT DESCRIPTION
    FROM FND_TABLES
    WHERE TABLE_NAME='HZ_CUST_SITE_USES_ALL'
    AND ROWNUM      =1
    ) DESCRIPTION,
    DECODE(HCSU.site_use_id,NULL,
    (SELECT hcpcs.name
    FROM HZ_CUSTOMER_PROFILES hcpf,
      HZ_CUST_PROFILE_CLASSES hcpcs
    WHERE hcpf.profile_class_id=hcpcs.profile_class_id
    AND hcpf.cust_account_id   = HCA.CUST_ACCOUNT_ID
    AND hcpf.SITE_USE_ID      IS NULL
    AND rownum                 =1
    ),
    (SELECT hcpcs.name
    FROM HZ_CUSTOMER_PROFILES hcpf,
      HZ_CUST_PROFILE_CLASSES hcpcs
    WHERE hcpf.profile_class_id=hcpcs.profile_class_id
    AND hcpf.site_use_id       = HCSU.site_use_id
    AND hcpf.cust_account_id   = HCA.CUST_ACCOUNT_ID
    AND rownum                 =1
    )) profile_class
  FROM APPS.HZ_CUST_SITE_USES_ALL_A HCSUA,
    APPS.HZ_CUST_SITE_USES_ALL_AV6 HCSUA3,
    HZ_CUST_SITE_USES_ALL HCSU ,
    APPS.HZ_CUST_ACCOUNTS HCA ,
    HZ_CUST_ACCT_SITES_ALL HCS,
    HZ_PARTY_SITES HPS
  WHERE HCSUA.ROW_KEY              = HCSUA3.ROW_KEY
  AND HCSUA3.SITE_USE_ID           = HCSU.site_use_id
  AND HCSU.CUST_ACCT_SITE_ID       = HCS.CUST_ACCT_SITE_ID
  AND HCS.CUST_ACCOUNT_ID          = HCA.CUST_ACCOUNT_ID
  AND HCS.PARTY_SITE_ID            = HPS.PARTY_SITE_ID(+)
  AND Hcsu.Site_Use_Code(+)        = 'BILL_TO'
  AND HCSUA.AUDIT_TRANSACTION_TYPE = 'U'
  UNION ALL
  SELECT 'No' credit_report_flag,
    TRUNC(HCSUA.Audit_Timestamp) trunc_Audit_Date,
    HCSUA.Audit_Timestamp Audit_Date,
    DECODE(HCSUA.Audit_Transaction_Type,'I','Insert') Audit_Type,
    HCSUA.Audit_User_Name User_Ntid,
    NVL(
    (SELECT Ppf.Full_Name
    FROM Fnd_User Fu,
      Per_All_People_F Ppf
    WHERE Fu.User_Name=HCSUA.Audit_User_Name
    AND Fu.Employee_Id=Ppf.person_id
    AND TRUNC(Sysdate) BETWEEN Ppf.Effective_Start_Date AND Ppf.Effective_End_Date
	AND rownum                 =1  --added for version 1.1
    ),HCSUA.Audit_User_Name) User_Name,
    Hca.ACCOUNT_Number,
    HCA.ACCOUNT_NAME,
    HPS.PARTY_SITE_NUMBER site_number,
    Hcsu.Location Site_Name,
    'Primary Flag' ATRIBUTE_NAME,
    DECODE(HCSUA.PRIMARY_FLAG,'Y','Yes','N','No') OLD_VALUE,
    DECODE(xxeis.EIS_XXWC_CUST_AUDIT_HIST_PKG.get_cust_insert_value('APPS.HZ_CUST_SITE_USES_ALL_AV6','PRIMARY_FLAG',HCSUA.SITE_USE_ID,'SITE_USE_ID'),'Y','Yes','N','No') new_value,
    'HZ_CUST_SITE_USES_ALL' TABLE_NAME,
    (SELECT DESCRIPTION
    FROM FND_TABLES
    WHERE TABLE_NAME='HZ_CUST_SITE_USES_ALL'
    AND ROWNUM      =1
    ) DESCRIPTION,
    DECODE(HCSU.site_use_id,NULL,
    (SELECT hcpcs.name
    FROM HZ_CUSTOMER_PROFILES hcpf,
      HZ_CUST_PROFILE_CLASSES hcpcs
    WHERE hcpf.profile_class_id=hcpcs.profile_class_id
    AND hcpf.cust_account_id   = HCA.CUST_ACCOUNT_ID
    AND hcpf.SITE_USE_ID      IS NULL
    AND rownum                 =1
    ),
    (SELECT hcpcs.name
    FROM HZ_CUSTOMER_PROFILES hcpf,
      HZ_CUST_PROFILE_CLASSES hcpcs
    WHERE hcpf.profile_class_id=hcpcs.profile_class_id
    AND hcpf.site_use_id       = HCSU.site_use_id
    AND hcpf.cust_account_id   = HCA.CUST_ACCOUNT_ID
    AND rownum                 =1
    )) profile_class
  FROM APPS.HZ_CUST_SITE_USES_ALL_A HCSUA,
    HZ_CUST_SITE_USES_ALL HCSU ,
    APPS.HZ_CUST_ACCOUNTS HCA ,
    HZ_CUST_ACCT_SITES_ALL HCS,
    HZ_PARTY_SITES HPS
  WHERE HCSUA.site_use_id                                                                                                                        = HCSU.site_use_id
  AND HCSU.CUST_ACCT_SITE_ID                                                                                                                     = HCS.CUST_ACCT_SITE_ID
  AND HCS.CUST_ACCOUNT_ID                                                                                                                        = HCA.CUST_ACCOUNT_ID
  AND HCS.PARTY_SITE_ID                                                                                                                          = HPS.PARTY_SITE_ID(+)
  AND Hcsu.Site_Use_Code(+)                                                                                                                      = 'BILL_TO'
  AND HCSUA.Audit_Transaction_Type                                                                                                               = 'I'
  AND XXEIS.EIS_XXWC_CUST_AUDIT_HIST_PKG.GET_CUST_INSERT_VALUE('APPS.HZ_CUST_SITE_USES_ALL_AV6','PRIMARY_FLAG',HCSUA.SITE_USE_ID,'SITE_USE_ID') IS NOT NULL
  UNION ALL
  SELECT 'No' credit_report_flag,
    TRUNC(HCSUA.Audit_Timestamp) trunc_Audit_Date,
    HCSUA.Audit_Timestamp Audit_Date,
    DECODE(HCSUA.Audit_Transaction_Type,'U','Update') Audit_Type,
    HCSUA.Audit_User_Name User_Ntid,
    NVL(
    (SELECT Ppf.Full_Name
    FROM Fnd_User Fu,
      Per_All_People_F Ppf
    WHERE Fu.User_Name=HCSUA.Audit_User_Name
    AND Fu.Employee_Id=Ppf.person_id
    AND TRUNC(Sysdate) BETWEEN Ppf.Effective_Start_Date AND Ppf.Effective_End_Date
	AND rownum                 =1	--added for version 1.1
    ),HCSUA.Audit_User_Name) User_Name,
    Hca.ACCOUNT_Number,
    HCA.ACCOUNT_NAME,
    HPS.PARTY_SITE_NUMBER site_number,
    Hcsu.Location Site_Name,
    'Location' ATRIBUTE_NAME,
    HCSUA4.LOCATION Old_Value,
    xxeis.EIS_XXWC_CUST_AUDIT_HIST_PKG.get_cust_new_value('APPS.HZ_CUST_SITE_USES_ALL_AV5','LOCATION',HCSUA.ROW_KEY,HCSUA4.SITE_USE_ID,'SITE_USE_ID') new_value,
    'HZ_CUST_SITE_USES_ALL' TABLE_NAME,
    (SELECT DESCRIPTION
    FROM FND_TABLES
    WHERE TABLE_NAME='HZ_CUST_SITE_USES_ALL'
    AND ROWNUM      =1
    ) DESCRIPTION,
    DECODE(HCSU.site_use_id,NULL,
    (SELECT hcpcs.name
    FROM HZ_CUSTOMER_PROFILES hcpf,
      HZ_CUST_PROFILE_CLASSES hcpcs
    WHERE hcpf.profile_class_id=hcpcs.profile_class_id
    AND hcpf.cust_account_id   = HCA.CUST_ACCOUNT_ID
    AND hcpf.SITE_USE_ID      IS NULL
    AND rownum                 =1
    ),
    (SELECT hcpcs.name
    FROM HZ_CUSTOMER_PROFILES hcpf,
      HZ_CUST_PROFILE_CLASSES hcpcs
    WHERE hcpf.profile_class_id=hcpcs.profile_class_id
    AND hcpf.site_use_id       = HCSU.site_use_id
    AND hcpf.cust_account_id   = HCA.CUST_ACCOUNT_ID
    AND rownum                 =1
    )) profile_class
  FROM APPS.HZ_CUST_SITE_USES_ALL_A HCSUA,
    APPS.HZ_CUST_SITE_USES_ALL_AV5 HCSUA4,
    HZ_CUST_SITE_USES_ALL HCSU ,
    APPS.HZ_CUST_ACCOUNTS HCA ,
    HZ_CUST_ACCT_SITES_ALL HCS,
    HZ_PARTY_SITES HPS
  WHERE HCSUA.ROW_KEY              = HCSUA4.ROW_KEY
  AND HCSUA4.SITE_USE_ID           = HCSU.site_use_id
  AND HCSU.CUST_ACCT_SITE_ID       = HCS.CUST_ACCT_SITE_ID
  AND HCS.CUST_ACCOUNT_ID          = HCA.CUST_ACCOUNT_ID
  AND HCS.PARTY_SITE_ID            = HPS.PARTY_SITE_ID(+)
  AND HCSU.SITE_USE_CODE(+)        = 'BILL_TO'
  AND HCSUA.AUDIT_TRANSACTION_TYPE = 'U'
  UNION ALL
  SELECT 'No' credit_report_flag,
    TRUNC(HCSUA.Audit_Timestamp) trunc_Audit_Date,
    HCSUA.Audit_Timestamp Audit_Date,
    DECODE(HCSUA.Audit_Transaction_Type,'I','Insert') Audit_Type,
    HCSUA.Audit_User_Name User_Ntid,
    NVL(
    (SELECT Ppf.Full_Name
    FROM Fnd_User Fu,
      Per_All_People_F Ppf
    WHERE Fu.User_Name=HCSUA.Audit_User_Name
    AND Fu.Employee_Id=Ppf.person_id
    AND TRUNC(Sysdate) BETWEEN Ppf.Effective_Start_Date AND Ppf.Effective_End_Date
	AND rownum                 =1	--added for version 1.1
    ),HCSUA.Audit_User_Name) User_Name,
    Hca.ACCOUNT_Number,
    HCA.ACCOUNT_NAME,
    HPS.PARTY_SITE_NUMBER site_number,
    Hcsu.Location Site_Name,
    'Location' ATRIBUTE_NAME,
    HCSUA.LOCATION OLD_VALUE,
    xxeis.EIS_XXWC_CUST_AUDIT_HIST_PKG.get_cust_insert_value('APPS.HZ_CUST_SITE_USES_ALL_AV5','LOCATION',HCSUA.SITE_USE_ID,'SITE_USE_ID') new_value,
    'HZ_CUST_SITE_USES_ALL' TABLE_NAME,
    (SELECT DESCRIPTION
    FROM FND_TABLES
    WHERE TABLE_NAME='HZ_CUST_SITE_USES_ALL'
    AND ROWNUM      =1
    ) DESCRIPTION,
    DECODE(HCSU.site_use_id,NULL,
    (SELECT hcpcs.name
    FROM HZ_CUSTOMER_PROFILES hcpf,
      HZ_CUST_PROFILE_CLASSES hcpcs
    WHERE hcpf.profile_class_id=hcpcs.profile_class_id
    AND hcpf.cust_account_id   = HCA.CUST_ACCOUNT_ID
    AND hcpf.SITE_USE_ID      IS NULL
    AND rownum                 =1
    ),
    (SELECT hcpcs.name
    FROM HZ_CUSTOMER_PROFILES hcpf,
      HZ_CUST_PROFILE_CLASSES hcpcs
    WHERE hcpf.profile_class_id=hcpcs.profile_class_id
    AND hcpf.site_use_id       = HCSU.site_use_id
    AND hcpf.cust_account_id   = HCA.CUST_ACCOUNT_ID
    AND rownum                 =1
    )) profile_class
  FROM APPS.HZ_CUST_SITE_USES_ALL_A HCSUA,
    HZ_CUST_SITE_USES_ALL HCSU ,
    APPS.HZ_CUST_ACCOUNTS HCA ,
    HZ_CUST_ACCT_SITES_ALL HCS,
    HZ_PARTY_SITES HPS
  WHERE HCSUA.site_use_id                                                                                                                    = HCSU.site_use_id
  AND HCSU.CUST_ACCT_SITE_ID                                                                                                                 = HCS.CUST_ACCT_SITE_ID
  AND HCS.CUST_ACCOUNT_ID                                                                                                                    = HCA.CUST_ACCOUNT_ID
  AND HCS.PARTY_SITE_ID                                                                                                                      = HPS.PARTY_SITE_ID(+)
  AND Hcsu.Site_Use_Code(+)                                                                                                                  = 'BILL_TO'
  AND HCSUA.Audit_Transaction_Type                                                                                                           = 'I'
  AND XXEIS.EIS_XXWC_CUST_AUDIT_HIST_PKG.GET_CUST_INSERT_VALUE('APPS.HZ_CUST_SITE_USES_ALL_AV5','LOCATION',HCSUA.SITE_USE_ID,'SITE_USE_ID') IS NOT NULL
  UNION ALL
  SELECT 'No' credit_report_flag,
    TRUNC(HCSUA.Audit_Timestamp) trunc_Audit_Date,
    HCSUA.Audit_Timestamp Audit_Date,
    DECODE(HCSUA.Audit_Transaction_Type,'U','Update') Audit_Type,
    HCSUA.Audit_User_Name User_Ntid,
    NVL(
    (SELECT Ppf.Full_Name
    FROM Fnd_User Fu,
      Per_All_People_F Ppf
    WHERE Fu.User_Name=HCSUA.Audit_User_Name
    AND Fu.Employee_Id=Ppf.person_id
    AND TRUNC(Sysdate) BETWEEN Ppf.Effective_Start_Date AND Ppf.Effective_End_Date
	AND rownum                 =1	--added for version 1.1
    ),HCSUA.Audit_User_Name) User_Name,
    Hca.ACCOUNT_Number,
    HCA.ACCOUNT_NAME,
    HPS.PARTY_SITE_NUMBER site_number,
    Hcsu.Location Site_Name,
    'Customer Site Classification' ATRIBUTE_NAME,
    HCSUA5.ATTRIBUTE1 Old_Value,
    xxeis.EIS_XXWC_CUST_AUDIT_HIST_PKG.get_cust_new_value('APPS.HZ_CUST_SITE_USES_ALL_AV4','ATTRIBUTE1',HCSUA.ROW_KEY,HCSUA5.SITE_USE_ID,'SITE_USE_ID') new_value,
    'HZ_CUST_SITE_USES_ALL' TABLE_NAME,
    (SELECT DESCRIPTION
    FROM FND_TABLES
    WHERE TABLE_NAME='HZ_CUST_SITE_USES_ALL'
    AND ROWNUM      =1
    ) DESCRIPTION,
    DECODE(HCSU.site_use_id,NULL,
    (SELECT hcpcs.name
    FROM HZ_CUSTOMER_PROFILES hcpf,
      HZ_CUST_PROFILE_CLASSES hcpcs
    WHERE hcpf.profile_class_id=hcpcs.profile_class_id
    AND hcpf.cust_account_id   = HCA.CUST_ACCOUNT_ID
    AND hcpf.SITE_USE_ID      IS NULL
    AND rownum                 =1
    ),
    (SELECT hcpcs.name
    FROM HZ_CUSTOMER_PROFILES hcpf,
      HZ_CUST_PROFILE_CLASSES hcpcs
    WHERE hcpf.profile_class_id=hcpcs.profile_class_id
    AND hcpf.site_use_id       = HCSU.site_use_id
    AND hcpf.cust_account_id   = HCA.CUST_ACCOUNT_ID
    AND rownum                 =1
    )) profile_class
  FROM APPS.HZ_CUST_SITE_USES_ALL_A HCSUA,
    APPS.HZ_CUST_SITE_USES_ALL_AV4 HCSUA5,
    HZ_CUST_SITE_USES_ALL HCSU ,
    APPS.HZ_CUST_ACCOUNTS HCA ,
    HZ_CUST_ACCT_SITES_ALL HCS,
    HZ_PARTY_SITES HPS
  WHERE HCSUA.ROW_KEY              = HCSUA5.ROW_KEY
  AND HCSUA5.SITE_USE_ID           = HCSU.site_use_id
  AND HCSU.CUST_ACCT_SITE_ID       = HCS.CUST_ACCT_SITE_ID
  AND HCS.CUST_ACCOUNT_ID          = HCA.CUST_ACCOUNT_ID
  AND HCS.PARTY_SITE_ID            = HPS.PARTY_SITE_ID(+)
  AND HCSU.SITE_USE_CODE(+)        = 'BILL_TO'
  AND HCSUA.AUDIT_TRANSACTION_TYPE = 'U'
  UNION ALL
  SELECT 'No' credit_report_flag,
    TRUNC(HCSUA.Audit_Timestamp) trunc_Audit_Date,
    HCSUA.Audit_Timestamp Audit_Date,
    DECODE(HCSUA.Audit_Transaction_Type,'I','Insert') Audit_Type,
    HCSUA.Audit_User_Name User_Ntid,
    NVL(
    (SELECT Ppf.Full_Name
    FROM Fnd_User Fu,
      Per_All_People_F Ppf
    WHERE Fu.User_Name=HCSUA.Audit_User_Name
    AND Fu.Employee_Id=Ppf.person_id
    AND TRUNC(Sysdate) BETWEEN Ppf.Effective_Start_Date AND Ppf.Effective_End_Date
	AND rownum                 =1	--added for version 1.1
    ),HCSUA.Audit_User_Name) User_Name,
    Hca.ACCOUNT_Number,
    HCA.ACCOUNT_NAME,
    HPS.PARTY_SITE_NUMBER site_number,
    Hcsu.Location Site_Name,
    'Customer Site Classification' ATRIBUTE_NAME,
    HCSUA.ATTRIBUTE1 OLD_VALUE,
    xxeis.EIS_XXWC_CUST_AUDIT_HIST_PKG.get_cust_insert_value('APPS.HZ_CUST_SITE_USES_ALL_AV4','ATTRIBUTE1',HCSUA.SITE_USE_ID,'SITE_USE_ID') new_value,
    'HZ_CUST_SITE_USES_ALL' TABLE_NAME,
    (SELECT DESCRIPTION
    FROM FND_TABLES
    WHERE TABLE_NAME='HZ_CUST_SITE_USES_ALL'
    AND ROWNUM      =1
    ) DESCRIPTION,
    DECODE(HCSU.site_use_id,NULL,
    (SELECT hcpcs.name
    FROM HZ_CUSTOMER_PROFILES hcpf,
      HZ_CUST_PROFILE_CLASSES hcpcs
    WHERE hcpf.profile_class_id=hcpcs.profile_class_id
    AND hcpf.cust_account_id   = HCA.CUST_ACCOUNT_ID
    AND hcpf.SITE_USE_ID      IS NULL
    AND rownum                 =1
    ),
    (SELECT hcpcs.name
    FROM HZ_CUSTOMER_PROFILES hcpf,
      HZ_CUST_PROFILE_CLASSES hcpcs
    WHERE hcpf.profile_class_id=hcpcs.profile_class_id
    AND hcpf.site_use_id       = HCSU.site_use_id
    AND hcpf.cust_account_id   = HCA.CUST_ACCOUNT_ID
    AND rownum                 =1
    )) profile_class
  FROM APPS.HZ_CUST_SITE_USES_ALL_A HCSUA,
    HZ_CUST_SITE_USES_ALL HCSU ,
    APPS.HZ_CUST_ACCOUNTS HCA ,
    HZ_CUST_ACCT_SITES_ALL HCS,
    HZ_PARTY_SITES HPS
  WHERE HCSUA.site_use_id                                                                                                                      = HCSU.site_use_id
  AND HCSU.CUST_ACCT_SITE_ID                                                                                                                   = HCS.CUST_ACCT_SITE_ID
  AND HCS.CUST_ACCOUNT_ID                                                                                                                      = HCA.CUST_ACCOUNT_ID
  AND HCS.PARTY_SITE_ID                                                                                                                        = HPS.PARTY_SITE_ID(+)
  AND Hcsu.Site_Use_Code(+)                                                                                                                    = 'BILL_TO'
  AND HCSUA.AUDIT_TRANSACTION_TYPE                                                                                                             = 'I'
  AND Xxeis.Eis_Xxwc_Cust_Audit_Hist_Pkg.Get_Cust_Insert_Value('APPS.HZ_CUST_SITE_USES_ALL_AV4','ATTRIBUTE1',Hcsua.Site_Use_Id,'SITE_USE_ID') IS NOT NULL
  UNION ALL
  SELECT 'No' credit_report_flag,
    TRUNC(HCSUA.Audit_Timestamp) trunc_Audit_Date,
    HCSUA.Audit_Timestamp Audit_Date,
    DECODE(HCSUA.Audit_Transaction_Type,'U','Update') Audit_Type,
    HCSUA.Audit_User_Name User_Ntid,
    NVL(
    (SELECT Ppf.Full_Name
    FROM Fnd_User Fu,
      Per_All_People_F Ppf
    WHERE Fu.User_Name=HCSUA.Audit_User_Name
    AND Fu.Employee_Id=Ppf.person_id
    AND TRUNC(Sysdate) BETWEEN Ppf.Effective_Start_Date AND Ppf.Effective_End_Date
	AND rownum                 =1	--added for version 1.1
    ),HCSUA.Audit_User_Name) User_Name,
    Hca.ACCOUNT_Number,
    HCA.ACCOUNT_NAME,
    HPS.PARTY_SITE_NUMBER site_number,
    Hcsu.Location Site_Name,
    'Sales Person' ATRIBUTE_NAME,
    (SELECT JRET.RESOURCE_NAME
    FROM JTF_RS_SALESREPS JRS,
      JTF_RS_RESOURCE_EXTNS_TL JRET
    WHERE JRS.SALESREP_ID= HCSUA6.PRIMARY_SALESREP_ID
    AND JRS.RESOURCE_ID  = JRET.RESOURCE_ID
    AND JRET.LANGUAGE    = USERENV('LANG')
	AND rownum                 =1	--added for version 1.1
    ) Old_Value,
    (SELECT JRET.RESOURCE_NAME
    FROM JTF_RS_SALESREPS JRS,
      JTF_RS_RESOURCE_EXTNS_TL JRET
    WHERE JRS.SALESREP_ID= xxeis.EIS_XXWC_CUST_AUDIT_HIST_PKG.get_cust_new_value('APPS.HZ_CUST_SITE_USES_ALL_AV2','PRIMARY_SALESREP_ID',HCSUA.ROW_KEY,HCSUA6.SITE_USE_ID,'SITE_USE_ID')
    AND JRS.RESOURCE_ID  = JRET.RESOURCE_ID
    AND JRET.LANGUAGE    = USERENV('LANG')
	AND rownum                 =1
    ) new_value,
    'HZ_CUST_SITE_USES_ALL' TABLE_NAME,
    (SELECT DESCRIPTION
    FROM FND_TABLES
    WHERE TABLE_NAME='HZ_CUST_SITE_USES_ALL'
    AND ROWNUM      =1
    ) DESCRIPTION,
    DECODE(HCSU.site_use_id,NULL,
    (SELECT hcpcs.name
    FROM HZ_CUSTOMER_PROFILES hcpf,
      HZ_CUST_PROFILE_CLASSES hcpcs
    WHERE hcpf.profile_class_id=hcpcs.profile_class_id
    AND hcpf.cust_account_id   = HCA.CUST_ACCOUNT_ID
    AND hcpf.SITE_USE_ID      IS NULL
    AND rownum                 =1
    ),
    (SELECT hcpcs.name
    FROM HZ_CUSTOMER_PROFILES hcpf,
      HZ_CUST_PROFILE_CLASSES hcpcs
    WHERE hcpf.profile_class_id=hcpcs.profile_class_id
    AND hcpf.site_use_id       = HCSU.site_use_id
    AND hcpf.cust_account_id   = HCA.CUST_ACCOUNT_ID
    AND rownum                 =1
    )) profile_class
  FROM APPS.HZ_CUST_SITE_USES_ALL_A HCSUA,
    APPS.HZ_CUST_SITE_USES_ALL_AV2 HCSUA6,
    HZ_CUST_SITE_USES_ALL HCSU ,
    APPS.HZ_CUST_ACCOUNTS HCA ,
    HZ_CUST_ACCT_SITES_ALL HCS,
    HZ_PARTY_SITES HPS
  WHERE HCSUA.ROW_KEY              = HCSUA6.ROW_KEY
  AND HCSUA6.SITE_USE_ID           = HCSU.site_use_id
  AND HCSU.CUST_ACCT_SITE_ID       = HCS.CUST_ACCT_SITE_ID
  AND HCS.CUST_ACCOUNT_ID          = HCA.CUST_ACCOUNT_ID
  AND HCS.PARTY_SITE_ID            = HPS.PARTY_SITE_ID(+)
  AND HCSU.SITE_USE_CODE(+)        = 'BILL_TO'
  AND HCSUA.AUDIT_TRANSACTION_TYPE = 'U'
  UNION ALL
  SELECT 'No' credit_report_flag,
    TRUNC(HCSUA.Audit_Timestamp) trunc_Audit_Date,
    HCSUA.Audit_Timestamp Audit_Date,
    DECODE(HCSUA.Audit_Transaction_Type,'I','Insert') Audit_Type,
    HCSUA.Audit_User_Name User_Ntid,
    NVL(
    (SELECT Ppf.Full_Name
    FROM Fnd_User Fu,
      Per_All_People_F Ppf
    WHERE Fu.User_Name=HCSUA.Audit_User_Name
    AND Fu.Employee_Id=Ppf.person_id
    AND TRUNC(Sysdate) BETWEEN Ppf.Effective_Start_Date AND Ppf.Effective_End_Date
	AND rownum                 =1	--added for version 1.1
    ),HCSUA.Audit_User_Name) User_Name,
    Hca.ACCOUNT_Number,
    HCA.ACCOUNT_NAME,
    HPS.PARTY_SITE_NUMBER site_number,
    Hcsu.location Site_Name,
    'Sales Person' ATRIBUTE_NAME,
    (SELECT JRET.RESOURCE_NAME
    FROM JTF_RS_SALESREPS JRS,
      JTF_RS_RESOURCE_EXTNS_TL JRET
    WHERE JRS.SALESREP_ID= HCSUA.PRIMARY_SALESREP_ID
    AND JRS.RESOURCE_ID  = JRET.RESOURCE_ID
    AND JRET.LANGUAGE    = USERENV('LANG')
	AND rownum                 =1	--added for version 1.1
    ) Old_Value,
    (SELECT JRET.RESOURCE_NAME
    FROM JTF_RS_SALESREPS JRS,
      JTF_RS_RESOURCE_EXTNS_TL JRET
    WHERE JRS.SALESREP_ID= xxeis.EIS_XXWC_CUST_AUDIT_HIST_PKG.get_cust_insert_value('APPS.HZ_CUST_SITE_USES_ALL_AV2','PRIMARY_SALESREP_ID',HCSUA.SITE_USE_ID,'SITE_USE_ID')
    AND JRS.RESOURCE_ID  = JRET.RESOURCE_ID
    AND JRET.LANGUAGE    = USERENV('LANG')
	AND rownum                 =1	--added for version 1.1
    ) new_value,
    'HZ_CUST_SITE_USES_ALL' TABLE_NAME,
    (SELECT DESCRIPTION
    FROM FND_TABLES
    WHERE TABLE_NAME='HZ_CUST_SITE_USES_ALL'
    AND ROWNUM      =1
    ) DESCRIPTION,
    DECODE(HCSU.site_use_id,NULL,
    (SELECT hcpcs.name
    FROM HZ_CUSTOMER_PROFILES hcpf,
      HZ_CUST_PROFILE_CLASSES hcpcs
    WHERE hcpf.profile_class_id=hcpcs.profile_class_id
    AND hcpf.cust_account_id   = HCA.CUST_ACCOUNT_ID
    AND hcpf.SITE_USE_ID      IS NULL
    AND rownum                 =1
    ),
    (SELECT hcpcs.name
    FROM HZ_CUSTOMER_PROFILES hcpf,
      HZ_CUST_PROFILE_CLASSES hcpcs
    WHERE hcpf.profile_class_id=hcpcs.profile_class_id
    AND hcpf.site_use_id       = HCSU.site_use_id
    AND hcpf.cust_account_id   = HCA.CUST_ACCOUNT_ID
    AND rownum                 =1
    )) profile_class
  FROM APPS.HZ_CUST_SITE_USES_ALL_A HCSUA,
    HZ_CUST_SITE_USES_ALL HCSU ,
    APPS.HZ_CUST_ACCOUNTS HCA ,
    HZ_CUST_ACCT_SITES_ALL HCS,
    HZ_PARTY_SITES HPS
  WHERE HCSUA.site_use_id                                                                                                                               = HCSU.site_use_id
  AND HCSU.CUST_ACCT_SITE_ID                                                                                                                            = HCS.CUST_ACCT_SITE_ID
  AND HCS.CUST_ACCOUNT_ID                                                                                                                               = HCA.CUST_ACCOUNT_ID
  AND HCS.PARTY_SITE_ID                                                                                                                                 = HPS.PARTY_SITE_ID(+)
  AND Hcsu.Site_Use_Code(+)                                                                                                                             = 'BILL_TO'
  AND HCSUA.Audit_Transaction_Type                                                                                                                      = 'I'
  AND XXEIS.EIS_XXWC_CUST_AUDIT_HIST_PKG.GET_CUST_INSERT_VALUE('APPS.HZ_CUST_SITE_USES_ALL_AV2','PRIMARY_SALESREP_ID',HCSUA.SITE_USE_ID,'SITE_USE_ID') IS NOT NULL
  UNION ALL
  SELECT 'No' credit_report_flag,
    TRUNC(HZLA.Audit_Timestamp) trunc_Audit_Date,
    HZLA.Audit_Timestamp Audit_Date,
    DECODE(HZLA.Audit_Transaction_Type,'U','Update') Audit_Type,
    HZLA.Audit_User_Name User_Ntid,
    NVL(
    (SELECT Ppf.Full_Name
    FROM Fnd_User Fu,
      Per_All_People_F Ppf
    WHERE Fu.User_Name=HZLA.Audit_User_Name
    AND Fu.Employee_Id=Ppf.person_id
    AND TRUNC(Sysdate) BETWEEN Ppf.Effective_Start_Date AND Ppf.Effective_End_Date
	AND rownum                 =1	--added for version 1.1
    ),HZLA.Audit_User_Name) User_Name,
    Hca.ACCOUNT_Number,
    HCA.ACCOUNT_NAME,
    HPS.PARTY_SITE_NUMBER site_number,
    Hcsu.Location Site_Name,
    'Address Line 1' ATRIBUTE_NAME,
    HZLAV2.ADDRESS1 Old_Value,
    xxeis.EIS_XXWC_CUST_AUDIT_HIST_PKG.get_cust_new_value('APPS.HZ_LOCATIONS_AV8','ADDRESS1',HZLA.ROW_KEY,HZLAV2.LOCATION_ID,'LOCATION_ID') new_value,
    'HZ_LOCATIONS' TABLE_NAME,
    (SELECT DESCRIPTION
    FROM FND_TABLES
    WHERE TABLE_NAME='HZ_LOCATIONS'
    AND ROWNUM      =1
    ) DESCRIPTION,
    DECODE(HCSU.site_use_id,NULL,
    (SELECT hcpcs.name
    FROM HZ_CUSTOMER_PROFILES hcpf,
      HZ_CUST_PROFILE_CLASSES hcpcs
    WHERE hcpf.profile_class_id=hcpcs.profile_class_id
    AND hcpf.cust_account_id   = HCA.CUST_ACCOUNT_ID
    AND hcpf.SITE_USE_ID      IS NULL
    AND rownum                 =1
    ),
    (SELECT hcpcs.name
    FROM HZ_CUSTOMER_PROFILES hcpf,
      HZ_CUST_PROFILE_CLASSES hcpcs
    WHERE hcpf.profile_class_id=hcpcs.profile_class_id
    AND hcpf.site_use_id       = HCSU.site_use_id
    AND hcpf.cust_account_id   = HCA.CUST_ACCOUNT_ID
    AND rownum                 =1
    )) profile_class
  FROM APPS.HZ_LOCATIONS_A HZLA,
    APPS.HZ_LOCATIONS_AV8 HZLAV2,
    HZ_PARTY_SITES HPS,
    HZ_CUST_SITE_USES_ALL HCSU ,
    APPS.HZ_CUST_ACCOUNTS HCA ,
    HZ_CUST_ACCT_SITES_ALL HCS
  WHERE HZLA.ROW_KEY              = HZLAV2.ROW_KEY
  AND HZLAV2.LOCATION_ID          = Hps.Location_Id
  AND Hps.Party_Site_Id           = Hcs.Party_Site_Id
  AND HCS.CUST_ACCT_SITE_ID       = HCSU.CUST_ACCT_SITE_ID
  AND Hcs.Cust_Account_Id         = Hca.Cust_Account_Id
  AND Hcsu.Site_Use_Code(+)       = 'BILL_TO'
  AND HZLA.AUDIT_TRANSACTION_TYPE = 'U'
  UNION ALL
  SELECT 'No' credit_report_flag,
    TRUNC(HZLA.Audit_Timestamp) trunc_Audit_Date,
    HZLA.Audit_Timestamp Audit_Date,
    DECODE(HZLA.Audit_Transaction_Type,'I','Insert') Audit_Type,
    HZLA.Audit_User_Name User_Ntid,
    NVL(
    (SELECT Ppf.Full_Name
    FROM Fnd_User Fu,
      Per_All_People_F Ppf
    WHERE Fu.User_Name=HZLA.Audit_User_Name
    AND Fu.Employee_Id=Ppf.person_id
    AND TRUNC(Sysdate) BETWEEN Ppf.Effective_Start_Date AND Ppf.Effective_End_Date
	AND rownum                 =1	--added for version 1.1
    ),HZLA.Audit_User_Name) User_Name,
    Hca.ACCOUNT_Number,
    HCA.ACCOUNT_NAME,
    HPS.PARTY_SITE_NUMBER site_number,
    Hcsu.location Site_Name,
    'Address Line 1' ATRIBUTE_NAME,
    HZLA.ADDRESS1 Old_Value,
    xxeis.EIS_XXWC_CUST_AUDIT_HIST_PKG.get_cust_insert_value('APPS.HZ_LOCATIONS_AV8','ADDRESS1',HZLA.LOCATION_ID,'LOCATION_ID') new_value,
    'HZ_LOCATIONS' TABLE_NAME,
    (SELECT DESCRIPTION
    FROM FND_TABLES
    WHERE TABLE_NAME='HZ_LOCATIONS'
    AND ROWNUM      =1
    ) DESCRIPTION,
    DECODE(HCSU.site_use_id,NULL,
    (SELECT hcpcs.name
    FROM HZ_CUSTOMER_PROFILES hcpf,
      HZ_CUST_PROFILE_CLASSES hcpcs
    WHERE hcpf.profile_class_id=hcpcs.profile_class_id
    AND hcpf.cust_account_id   = HCA.CUST_ACCOUNT_ID
    AND hcpf.SITE_USE_ID      IS NULL
    AND rownum                 =1
    ),
    (SELECT hcpcs.name
    FROM HZ_CUSTOMER_PROFILES hcpf,
      HZ_CUST_PROFILE_CLASSES hcpcs
    WHERE hcpf.profile_class_id=hcpcs.profile_class_id
    AND hcpf.site_use_id       = HCSU.site_use_id
    AND hcpf.cust_account_id   = HCA.CUST_ACCOUNT_ID
    AND rownum                 =1
    )) profile_class
  FROM Apps.HZ_LOCATIONS_A HZLA,
    HZ_PARTY_SITES HPS,
    HZ_CUST_SITE_USES_ALL HCSU ,
    APPS.HZ_CUST_ACCOUNTS HCA ,
    HZ_CUST_ACCT_SITES_ALL HCS
  WHERE Hzla.Location_Id                                                                                                           = Hps.Location_Id
  AND Hps.Party_Site_Id                                                                                                            = Hcs.Party_Site_Id
  AND HCS.CUST_ACCT_SITE_ID                                                                                                        = HCSU.CUST_ACCT_SITE_ID
  AND Hcs.Cust_Account_Id                                                                                                          = Hca.Cust_Account_Id
  AND Hcsu.Site_Use_Code(+)                                                                                                        = 'BILL_TO'
  AND HZLA.AUDIT_TRANSACTION_TYPE                                                                                                  = 'I'
  AND XXEIS.EIS_XXWC_CUST_AUDIT_HIST_PKG.GET_CUST_INSERT_VALUE('APPS.HZ_LOCATIONS_AV8','ADDRESS1',HZLA.LOCATION_ID,'LOCATION_ID') IS NOT NULL
  UNION ALL
  SELECT 'No' credit_report_flag,
    TRUNC(HZLA.Audit_Timestamp) trunc_Audit_Date,
    HZLA.Audit_Timestamp Audit_Date,
    DECODE(HZLA.Audit_Transaction_Type,'U','Update') Audit_Type,
    HZLA.Audit_User_Name User_Ntid,
    NVL(
    (SELECT Ppf.Full_Name
    FROM Fnd_User Fu,
      Per_All_People_F Ppf
    WHERE Fu.User_Name=HZLA.Audit_User_Name
    AND Fu.Employee_Id=Ppf.person_id
    AND TRUNC(Sysdate) BETWEEN Ppf.Effective_Start_Date AND Ppf.Effective_End_Date
	AND rownum                 =1	--added for version 1.1
    ),HZLA.Audit_User_Name) User_Name,
    Hca.ACCOUNT_Number,
    HCA.ACCOUNT_NAME,
    HPS.PARTY_SITE_NUMBER site_number,
    HCSU.LOCATION SITE_NAME,
    'Address Line 2' ATRIBUTE_NAME,
    HZLAV3.ADDRESS2 Old_Value,
    xxeis.EIS_XXWC_CUST_AUDIT_HIST_PKG.get_cust_new_value('APPS.HZ_LOCATIONS_AV7','ADDRESS2',HZLA.ROW_KEY,HZLAV3.LOCATION_ID,'LOCATION_ID') new_value,
    'HZ_LOCATIONS' TABLE_NAME,
    (SELECT DESCRIPTION
    FROM FND_TABLES
    WHERE TABLE_NAME='HZ_LOCATIONS'
    AND ROWNUM      =1
    ) DESCRIPTION,
    DECODE(HCSU.site_use_id,NULL,
    (SELECT hcpcs.name
    FROM HZ_CUSTOMER_PROFILES hcpf,
      HZ_CUST_PROFILE_CLASSES hcpcs
    WHERE hcpf.profile_class_id=hcpcs.profile_class_id
    AND hcpf.cust_account_id   = HCA.CUST_ACCOUNT_ID
    AND hcpf.SITE_USE_ID      IS NULL
    AND rownum                 =1
    ),
    (SELECT hcpcs.name
    FROM HZ_CUSTOMER_PROFILES hcpf,
      HZ_CUST_PROFILE_CLASSES hcpcs
    WHERE hcpf.profile_class_id=hcpcs.profile_class_id
    AND hcpf.site_use_id       = HCSU.site_use_id
    AND hcpf.cust_account_id   = HCA.CUST_ACCOUNT_ID
    AND rownum                 =1
    )) profile_class
  FROM APPS.HZ_LOCATIONS_A HZLA,
    APPS.HZ_LOCATIONS_AV7 HZLAV3,
    HZ_PARTY_SITES HPS,
    HZ_CUST_SITE_USES_ALL HCSU ,
    APPS.HZ_CUST_ACCOUNTS HCA ,
    HZ_CUST_ACCT_SITES_ALL HCS
  WHERE HZLA.ROW_KEY              = HZLAV3.ROW_KEY
  AND HZLAV3.LOCATION_ID          = Hps.Location_Id
  AND Hps.Party_Site_Id           = Hcs.Party_Site_Id
  AND HCS.CUST_ACCT_SITE_ID       = HCSU.CUST_ACCT_SITE_ID
  AND Hcs.Cust_Account_Id         = Hca.Cust_Account_Id
  AND Hcsu.Site_Use_Code(+)       = 'BILL_TO'
  AND HZLA.AUDIT_TRANSACTION_TYPE = 'U'
  UNION ALL
  SELECT 'No' credit_report_flag,
    TRUNC(HZLA.Audit_Timestamp) trunc_Audit_Date,
    HZLA.Audit_Timestamp Audit_Date,
    DECODE(HZLA.Audit_Transaction_Type,'I','Insert') Audit_Type,
    HZLA.Audit_User_Name User_Ntid,
    NVL(
    (SELECT Ppf.Full_Name
    FROM Fnd_User Fu,
      Per_All_People_F Ppf
    WHERE Fu.User_Name=HZLA.Audit_User_Name
    AND Fu.Employee_Id=Ppf.person_id
    AND TRUNC(Sysdate) BETWEEN Ppf.Effective_Start_Date AND Ppf.Effective_End_Date
	AND rownum                 =1	--added for version 1.1
    ),HZLA.Audit_User_Name) User_Name,
    Hca.ACCOUNT_Number,
    HCA.ACCOUNT_NAME,
    HPS.PARTY_SITE_NUMBER site_number,
    HCSU.LOCATION SITE_NAME,
    'Address Line 2' ATRIBUTE_NAME,
    HZLA.ADDRESS2 Old_Value,
    xxeis.EIS_XXWC_CUST_AUDIT_HIST_PKG.get_cust_insert_value('APPS.HZ_LOCATIONS_AV7','ADDRESS2',HZLA.LOCATION_ID,'LOCATION_ID') new_value,
    'HZ_LOCATIONS' TABLE_NAME,
    (SELECT DESCRIPTION
    FROM FND_TABLES
    WHERE TABLE_NAME='HZ_LOCATIONS'
    AND ROWNUM      =1
    ) DESCRIPTION,
    DECODE(HCSU.site_use_id,NULL,
    (SELECT hcpcs.name
    FROM HZ_CUSTOMER_PROFILES hcpf,
      HZ_CUST_PROFILE_CLASSES hcpcs
    WHERE hcpf.profile_class_id=hcpcs.profile_class_id
    AND hcpf.cust_account_id   = HCA.CUST_ACCOUNT_ID
    AND hcpf.SITE_USE_ID      IS NULL
    AND rownum                 =1
    ),
    (SELECT hcpcs.name
    FROM HZ_CUSTOMER_PROFILES hcpf,
      HZ_CUST_PROFILE_CLASSES hcpcs
    WHERE hcpf.profile_class_id=hcpcs.profile_class_id
    AND hcpf.site_use_id       = HCSU.site_use_id
    AND hcpf.cust_account_id   = HCA.CUST_ACCOUNT_ID
    AND rownum                 =1
    )) profile_class
  FROM Apps.HZ_LOCATIONS_A HZLA,
    HZ_PARTY_SITES HPS,
    HZ_CUST_SITE_USES_ALL HCSU ,
    APPS.HZ_CUST_ACCOUNTS HCA ,
    HZ_CUST_ACCT_SITES_ALL HCS
  WHERE Hzla.Location_Id                                                                                                           = Hps.Location_Id
  AND Hps.Party_Site_Id                                                                                                            = Hcs.Party_Site_Id
  AND HCS.CUST_ACCT_SITE_ID                                                                                                        = HCSU.CUST_ACCT_SITE_ID
  AND Hcs.Cust_Account_Id                                                                                                          = Hca.Cust_Account_Id
  AND Hcsu.Site_Use_Code(+)                                                                                                        = 'BILL_TO'
  AND HZLA.AUDIT_TRANSACTION_TYPE                                                                                                  = 'I'
  AND XXEIS.EIS_XXWC_CUST_AUDIT_HIST_PKG.GET_CUST_INSERT_VALUE('APPS.HZ_LOCATIONS_AV7','ADDRESS2',HZLA.LOCATION_ID,'LOCATION_ID') IS NOT NULL
  UNION ALL
  SELECT 'No' credit_report_flag,
    TRUNC(HZLA.Audit_Timestamp) trunc_Audit_Date,
    HZLA.Audit_Timestamp Audit_Date,
    DECODE(HZLA.Audit_Transaction_Type,'U','Update') Audit_Type,
    HZLA.Audit_User_Name User_Ntid,
    NVL(
    (SELECT Ppf.Full_Name
    FROM Fnd_User Fu,
      Per_All_People_F Ppf
    WHERE Fu.User_Name=HZLA.Audit_User_Name
    AND Fu.Employee_Id=Ppf.person_id
    AND TRUNC(Sysdate) BETWEEN Ppf.Effective_Start_Date AND Ppf.Effective_End_Date
	AND rownum                 =1	--added for version 1.1
    ),HZLA.Audit_User_Name) User_Name,
    Hca.ACCOUNT_Number,
    HCA.ACCOUNT_NAME,
    HPS.PARTY_SITE_NUMBER site_number,
    HCSU.LOCATION SITE_NAME,
    'City' ATRIBUTE_NAME,
    HZLAV4.CITY Old_Value,
    xxeis.EIS_XXWC_CUST_AUDIT_HIST_PKG.get_cust_new_value('APPS.HZ_LOCATIONS_AV6','CITY',HZLA.ROW_KEY,HZLAV4.LOCATION_ID,'LOCATION_ID') new_value,
    'HZ_LOCATIONS' TABLE_NAME,
    (SELECT DESCRIPTION
    FROM FND_TABLES
    WHERE TABLE_NAME='HZ_LOCATIONS'
    AND ROWNUM      =1
    ) DESCRIPTION,
    DECODE(HCSU.site_use_id,NULL,
    (SELECT hcpcs.name
    FROM HZ_CUSTOMER_PROFILES hcpf,
      HZ_CUST_PROFILE_CLASSES hcpcs
    WHERE hcpf.profile_class_id=hcpcs.profile_class_id
    AND hcpf.cust_account_id   = HCA.CUST_ACCOUNT_ID
    AND hcpf.SITE_USE_ID      IS NULL
    AND rownum                 =1
    ),
    (SELECT hcpcs.name
    FROM HZ_CUSTOMER_PROFILES hcpf,
      HZ_CUST_PROFILE_CLASSES hcpcs
    WHERE hcpf.profile_class_id=hcpcs.profile_class_id
    AND hcpf.site_use_id       = HCSU.site_use_id
    AND hcpf.cust_account_id   = HCA.CUST_ACCOUNT_ID
    AND rownum                 =1
    )) profile_class
  FROM APPS.HZ_LOCATIONS_A HZLA,
    APPS.HZ_LOCATIONS_AV6 HZLAV4,
    HZ_PARTY_SITES HPS,
    HZ_CUST_SITE_USES_ALL HCSU ,
    APPS.HZ_CUST_ACCOUNTS HCA ,
    HZ_CUST_ACCT_SITES_ALL HCS
  WHERE HZLA.ROW_KEY              = HZLAV4.ROW_KEY
  AND HZLAV4.LOCATION_ID          = Hps.Location_Id
  AND Hps.Party_Site_Id           = Hcs.Party_Site_Id
  AND HCS.CUST_ACCT_SITE_ID       = HCSU.CUST_ACCT_SITE_ID
  AND Hcs.Cust_Account_Id         = Hca.Cust_Account_Id
  AND Hcsu.Site_Use_Code(+)       = 'BILL_TO'
  AND HZLA.AUDIT_TRANSACTION_TYPE = 'U'
  UNION ALL
  SELECT 'No' credit_report_flag,
    TRUNC(HZLA.Audit_Timestamp) trunc_Audit_Date,
    HZLA.Audit_Timestamp Audit_Date,
    DECODE(HZLA.Audit_Transaction_Type,'I','Insert') Audit_Type,
    HZLA.Audit_User_Name User_Ntid,
    NVL(
    (SELECT Ppf.Full_Name
    FROM Fnd_User Fu,
      Per_All_People_F Ppf
    WHERE Fu.User_Name=HZLA.Audit_User_Name
    AND Fu.Employee_Id=Ppf.person_id
    AND TRUNC(Sysdate) BETWEEN Ppf.Effective_Start_Date AND Ppf.Effective_End_Date
	AND rownum                 =1	--added for version 1.1
    ),HZLA.Audit_User_Name) User_Name,
    Hca.ACCOUNT_Number,
    HCA.ACCOUNT_NAME,
    HPS.PARTY_SITE_NUMBER site_number,
    Hcsu.location Site_Name,
    'City' ATRIBUTE_NAME,
    HZLA.CITY Old_Value,
    xxeis.EIS_XXWC_CUST_AUDIT_HIST_PKG.get_cust_insert_value('APPS.HZ_LOCATIONS_AV6','CITY',HZLA.LOCATION_ID,'LOCATION_ID') new_value,
    'HZ_LOCATIONS' TABLE_NAME,
    (SELECT DESCRIPTION
    FROM FND_TABLES
    WHERE TABLE_NAME='HZ_LOCATIONS'
    AND ROWNUM      =1
    ) DESCRIPTION,
    DECODE(HCSU.site_use_id,NULL,
    (SELECT hcpcs.name
    FROM HZ_CUSTOMER_PROFILES hcpf,
      HZ_CUST_PROFILE_CLASSES hcpcs
    WHERE hcpf.profile_class_id=hcpcs.profile_class_id
    AND hcpf.cust_account_id   = HCA.CUST_ACCOUNT_ID
    AND hcpf.SITE_USE_ID      IS NULL
    AND rownum                 =1
    ),
    (SELECT hcpcs.name
    FROM HZ_CUSTOMER_PROFILES hcpf,
      HZ_CUST_PROFILE_CLASSES hcpcs
    WHERE hcpf.profile_class_id=hcpcs.profile_class_id
    AND hcpf.site_use_id       = HCSU.site_use_id
    AND hcpf.cust_account_id   = HCA.CUST_ACCOUNT_ID
    AND rownum                 =1
    )) profile_class
  FROM Apps.HZ_LOCATIONS_A HZLA,
    HZ_PARTY_SITES HPS,
    HZ_CUST_SITE_USES_ALL HCSU ,
    APPS.HZ_CUST_ACCOUNTS HCA ,
    HZ_CUST_ACCT_SITES_ALL HCS
  WHERE Hzla.Location_Id                                                                                                       = Hps.Location_Id
  AND Hps.Party_Site_Id                                                                                                        = Hcs.Party_Site_Id
  AND HCS.CUST_ACCT_SITE_ID                                                                                                    = HCSU.CUST_ACCT_SITE_ID
  AND Hcs.Cust_Account_Id                                                                                                      = Hca.Cust_Account_Id
  AND Hcsu.Site_Use_Code(+)                                                                                                    = 'BILL_TO'
  AND HZLA.AUDIT_TRANSACTION_TYPE                                                                                              = 'I'
  AND Xxeis.Eis_Xxwc_Cust_Audit_Hist_Pkg.Get_Cust_Insert_Value('APPS.HZ_LOCATIONS_AV6','CITY',Hzla.Location_Id,'LOCATION_ID') IS NOT NULL
  UNION ALL
  SELECT 'No' credit_report_flag,
    TRUNC(HZLA.Audit_Timestamp) trunc_Audit_Date,
    HZLA.Audit_Timestamp Audit_Date,
    DECODE(HZLA.Audit_Transaction_Type,'U','Update') Audit_Type,
    HZLA.Audit_User_Name User_Ntid,
    NVL(
    (SELECT Ppf.Full_Name
    FROM Fnd_User Fu,
      Per_All_People_F Ppf
    WHERE Fu.User_Name=HZLA.Audit_User_Name
    AND Fu.Employee_Id=Ppf.person_id
    AND TRUNC(Sysdate) BETWEEN Ppf.Effective_Start_Date AND Ppf.Effective_End_Date
	AND rownum                 =1	--added for version 1.1
    ),HZLA.Audit_User_Name) User_Name,
    Hca.ACCOUNT_Number,
    HCA.ACCOUNT_NAME,
    HPS.PARTY_SITE_NUMBER site_number,
    HCSU.LOCATION SITE_NAME,
    'Postal Code' ATRIBUTE_NAME,
    HZLAV5.POSTAL_CODE Old_Value,
    xxeis.EIS_XXWC_CUST_AUDIT_HIST_PKG.get_cust_new_value('APPS.HZ_LOCATIONS_AV5','POSTAL_CODE',HZLA.ROW_KEY,HZLAV5.LOCATION_ID,'LOCATION_ID') new_value,
    'HZ_LOCATIONS' TABLE_NAME,
    (SELECT DESCRIPTION
    FROM FND_TABLES
    WHERE TABLE_NAME='HZ_LOCATIONS'
    AND ROWNUM      =1
    ) DESCRIPTION,
    DECODE(HCSU.site_use_id,NULL,
    (SELECT hcpcs.name
    FROM HZ_CUSTOMER_PROFILES hcpf,
      HZ_CUST_PROFILE_CLASSES hcpcs
    WHERE hcpf.profile_class_id=hcpcs.profile_class_id
    AND hcpf.cust_account_id   = HCA.CUST_ACCOUNT_ID
    AND hcpf.SITE_USE_ID      IS NULL
    AND rownum                 =1
    ),
    (SELECT hcpcs.name
    FROM HZ_CUSTOMER_PROFILES hcpf,
      HZ_CUST_PROFILE_CLASSES hcpcs
    WHERE hcpf.profile_class_id=hcpcs.profile_class_id
    AND hcpf.site_use_id       = HCSU.site_use_id
    AND hcpf.cust_account_id   = HCA.CUST_ACCOUNT_ID
    AND rownum                 =1
    )) profile_class
  FROM APPS.HZ_LOCATIONS_A HZLA,
    APPS.HZ_LOCATIONS_AV5 HZLAV5,
    HZ_PARTY_SITES HPS,
    HZ_CUST_SITE_USES_ALL HCSU ,
    APPS.HZ_CUST_ACCOUNTS HCA ,
    HZ_CUST_ACCT_SITES_ALL HCS
  WHERE HZLA.ROW_KEY              = HZLAV5.ROW_KEY
  AND HZLAV5.LOCATION_ID          = Hps.Location_Id
  AND Hps.Party_Site_Id           = Hcs.Party_Site_Id
  AND HCS.CUST_ACCT_SITE_ID       = HCSU.CUST_ACCT_SITE_ID
  AND Hcs.Cust_Account_Id         = Hca.Cust_Account_Id
  AND Hcsu.Site_Use_Code(+)       = 'BILL_TO'
  AND HZLA.AUDIT_TRANSACTION_TYPE = 'U'
  UNION ALL
  SELECT 'No' credit_report_flag,
    TRUNC(HZLA.Audit_Timestamp) trunc_Audit_Date,
    HZLA.Audit_Timestamp Audit_Date,
    DECODE(HZLA.Audit_Transaction_Type,'I','Insert') Audit_Type,
    HZLA.Audit_User_Name User_Ntid,
    NVL(
    (SELECT Ppf.Full_Name
    FROM Fnd_User Fu,
      Per_All_People_F Ppf
    WHERE Fu.User_Name=HZLA.Audit_User_Name
    AND Fu.Employee_Id=Ppf.person_id
    AND TRUNC(Sysdate) BETWEEN Ppf.Effective_Start_Date AND Ppf.Effective_End_Date
	AND rownum                 =1	--added for version 1.1
    ),HZLA.Audit_User_Name) User_Name,
    Hca.ACCOUNT_Number,
    HCA.ACCOUNT_NAME,
    HPS.PARTY_SITE_NUMBER site_number,
    Hcsu.location Site_Name,
    'Postal Code' ATRIBUTE_NAME,
    HZLA.POSTAL_CODE Old_Value,
    xxeis.EIS_XXWC_CUST_AUDIT_HIST_PKG.get_cust_insert_value('APPS.HZ_LOCATIONS_AV5','POSTAL_CODE',HZLA.LOCATION_ID,'LOCATION_ID') new_value,
    'HZ_LOCATIONS' TABLE_NAME,
    (SELECT DESCRIPTION
    FROM FND_TABLES
    WHERE TABLE_NAME='HZ_LOCATIONS'
    AND ROWNUM      =1
    ) DESCRIPTION,
    DECODE(HCSU.site_use_id,NULL,
    (SELECT hcpcs.name
    FROM HZ_CUSTOMER_PROFILES hcpf,
      HZ_CUST_PROFILE_CLASSES hcpcs
    WHERE hcpf.profile_class_id=hcpcs.profile_class_id
    AND hcpf.cust_account_id   = HCA.CUST_ACCOUNT_ID
    AND hcpf.SITE_USE_ID      IS NULL
    AND rownum                 =1
    ),
    (SELECT hcpcs.name
    FROM HZ_CUSTOMER_PROFILES hcpf,
      HZ_CUST_PROFILE_CLASSES hcpcs
    WHERE hcpf.profile_class_id=hcpcs.profile_class_id
    AND hcpf.site_use_id       = HCSU.site_use_id
    AND hcpf.cust_account_id   = HCA.CUST_ACCOUNT_ID
    AND rownum                 =1
    )) profile_class
  FROM Apps.HZ_LOCATIONS_A HZLA,
    HZ_PARTY_SITES HPS,
    HZ_CUST_SITE_USES_ALL HCSU ,
    APPS.HZ_CUST_ACCOUNTS HCA ,
    HZ_CUST_ACCT_SITES_ALL HCS
  WHERE Hzla.Location_Id                                                                                                              = Hps.Location_Id
  AND Hps.Party_Site_Id                                                                                                               = Hcs.Party_Site_Id
  AND HCS.CUST_ACCT_SITE_ID                                                                                                           = HCSU.CUST_ACCT_SITE_ID
  AND Hcs.Cust_Account_Id                                                                                                             = Hca.Cust_Account_Id
  AND Hcsu.Site_Use_Code(+)                                                                                                           = 'BILL_TO'
  AND HZLA.AUDIT_TRANSACTION_TYPE                                                                                                     = 'I'
  AND Xxeis.Eis_Xxwc_Cust_Audit_Hist_Pkg.Get_Cust_Insert_Value('APPS.HZ_LOCATIONS_AV5','POSTAL_CODE',Hzla.Location_Id,'LOCATION_ID') IS NOT NULL
  UNION ALL
  SELECT 'No' credit_report_flag,
    TRUNC(HZLA.Audit_Timestamp) trunc_Audit_Date,
    HZLA.Audit_Timestamp Audit_Date,
    DECODE(HZLA.Audit_Transaction_Type,'U','Update') Audit_Type,
    HZLA.Audit_User_Name User_Ntid,
    NVL(
    (SELECT Ppf.Full_Name
    FROM Fnd_User Fu,
      Per_All_People_F Ppf
    WHERE Fu.User_Name=HZLA.Audit_User_Name
    AND Fu.Employee_Id=Ppf.person_id
    AND TRUNC(Sysdate) BETWEEN Ppf.Effective_Start_Date AND Ppf.Effective_End_Date
	AND rownum                 =1	--added for version 1.1
    ),HZLA.Audit_User_Name) User_Name,
    Hca.ACCOUNT_Number,
    HCA.ACCOUNT_NAME,
    HPS.PARTY_SITE_NUMBER site_number,
    HCSU.LOCATION SITE_NAME,
    'State' ATRIBUTE_NAME,
    HZLAV6.STATE Old_Value,
    xxeis.EIS_XXWC_CUST_AUDIT_HIST_PKG.get_cust_new_value('APPS.HZ_LOCATIONS_AV4','STATE',HZLA.ROW_KEY,HZLAV6.LOCATION_ID,'LOCATION_ID') new_value,
    'HZ_LOCATIONS' TABLE_NAME,
    (SELECT DESCRIPTION
    FROM FND_TABLES
    WHERE TABLE_NAME='HZ_LOCATIONS'
    AND ROWNUM      =1
    ) DESCRIPTION,
    DECODE(HCSU.site_use_id,NULL,
    (SELECT hcpcs.name
    FROM HZ_CUSTOMER_PROFILES hcpf,
      HZ_CUST_PROFILE_CLASSES hcpcs
    WHERE hcpf.profile_class_id=hcpcs.profile_class_id
    AND hcpf.cust_account_id   = HCA.CUST_ACCOUNT_ID
    AND hcpf.SITE_USE_ID      IS NULL
    AND rownum                 =1
    ),
    (SELECT hcpcs.name
    FROM HZ_CUSTOMER_PROFILES hcpf,
      HZ_CUST_PROFILE_CLASSES hcpcs
    WHERE hcpf.profile_class_id=hcpcs.profile_class_id
    AND hcpf.site_use_id       = HCSU.site_use_id
    AND hcpf.cust_account_id   = HCA.CUST_ACCOUNT_ID
    AND rownum                 =1
    )) profile_class
  FROM APPS.HZ_LOCATIONS_A HZLA,
    APPS.HZ_LOCATIONS_AV4 HZLAV6,
    HZ_PARTY_SITES HPS,
    HZ_CUST_SITE_USES_ALL HCSU ,
    APPS.HZ_CUST_ACCOUNTS HCA ,
    HZ_CUST_ACCT_SITES_ALL HCS
  WHERE HZLA.ROW_KEY              = HZLAV6.ROW_KEY
  AND HZLAV6.LOCATION_ID          = Hps.Location_Id
  AND Hps.Party_Site_Id           = Hcs.Party_Site_Id
  AND HCS.CUST_ACCT_SITE_ID       = HCSU.CUST_ACCT_SITE_ID
  AND Hcs.Cust_Account_Id         = Hca.Cust_Account_Id
  AND Hcsu.Site_Use_Code(+)       = 'BILL_TO'
  AND HZLA.AUDIT_TRANSACTION_TYPE = 'U'
  UNION ALL
  SELECT 'No' credit_report_flag,
    TRUNC(HZLA.Audit_Timestamp) trunc_Audit_Date,
    HZLA.Audit_Timestamp Audit_Date,
    DECODE(HZLA.Audit_Transaction_Type,'I','Insert') Audit_Type,
    HZLA.Audit_User_Name User_Ntid,
    NVL(
    (SELECT Ppf.Full_Name
    FROM Fnd_User Fu,
      Per_All_People_F Ppf
    WHERE Fu.User_Name=HZLA.Audit_User_Name
    AND Fu.Employee_Id=Ppf.person_id
    AND TRUNC(Sysdate) BETWEEN Ppf.Effective_Start_Date AND Ppf.Effective_End_Date
	AND rownum                 =1	--added for version 1.1
    ),HZLA.Audit_User_Name) User_Name,
    Hca.ACCOUNT_Number,
    HCA.ACCOUNT_NAME,
    HPS.PARTY_SITE_NUMBER site_number,
    Hcsu.location Site_Name,
    'State' ATRIBUTE_NAME,
    HZLA.STATE Old_Value,
    xxeis.EIS_XXWC_CUST_AUDIT_HIST_PKG.get_cust_insert_value('APPS.HZ_LOCATIONS_AV4','STATE',HZLA.LOCATION_ID,'LOCATION_ID') new_value,
    'HZ_LOCATIONS' TABLE_NAME,
    (SELECT DESCRIPTION
    FROM FND_TABLES
    WHERE TABLE_NAME='HZ_LOCATIONS'
    AND ROWNUM      =1
    ) DESCRIPTION,
    DECODE(HCSU.site_use_id,NULL,
    (SELECT hcpcs.name
    FROM HZ_CUSTOMER_PROFILES hcpf,
      HZ_CUST_PROFILE_CLASSES hcpcs
    WHERE hcpf.profile_class_id=hcpcs.profile_class_id
    AND hcpf.cust_account_id   = HCA.CUST_ACCOUNT_ID
    AND hcpf.SITE_USE_ID      IS NULL
    AND rownum                 =1
    ),
    (SELECT hcpcs.name
    FROM HZ_CUSTOMER_PROFILES hcpf,
      HZ_CUST_PROFILE_CLASSES hcpcs
    WHERE hcpf.profile_class_id=hcpcs.profile_class_id
    AND hcpf.site_use_id       = HCSU.site_use_id
    AND hcpf.cust_account_id   = HCA.CUST_ACCOUNT_ID
    AND rownum                 =1
    )) profile_class
  FROM Apps.HZ_LOCATIONS_A HZLA,
    HZ_PARTY_SITES HPS,
    HZ_CUST_SITE_USES_ALL HCSU ,
    APPS.HZ_CUST_ACCOUNTS HCA ,
    HZ_CUST_ACCT_SITES_ALL HCS
  WHERE Hzla.Location_Id                                                                                                        = Hps.Location_Id
  AND Hps.Party_Site_Id                                                                                                         = Hcs.Party_Site_Id
  AND HCS.CUST_ACCT_SITE_ID                                                                                                     = HCSU.CUST_ACCT_SITE_ID
  AND Hcs.Cust_Account_Id                                                                                                       = Hca.Cust_Account_Id
  AND Hcsu.Site_Use_Code(+)                                                                                                     = 'BILL_TO'
  AND HZLA.AUDIT_TRANSACTION_TYPE                                                                                               = 'I'
  AND XXEIS.EIS_XXWC_CUST_AUDIT_HIST_PKG.GET_CUST_INSERT_VALUE('APPS.HZ_LOCATIONS_AV4','STATE',HZLA.LOCATION_ID,'LOCATION_ID') IS NOT NULL
  UNION ALL
  SELECT 'No' credit_report_flag,
    TRUNC(HZLA.Audit_Timestamp) trunc_Audit_Date,
    HZLA.Audit_Timestamp Audit_Date,
    DECODE(HZLA.Audit_Transaction_Type,'U','Update') Audit_Type,
    HZLA.Audit_User_Name User_Ntid,
    NVL(
    (SELECT Ppf.Full_Name
    FROM Fnd_User Fu,
      Per_All_People_F Ppf
    WHERE Fu.User_Name=HZLA.Audit_User_Name
    AND Fu.Employee_Id=Ppf.person_id
    AND TRUNC(Sysdate) BETWEEN Ppf.Effective_Start_Date AND Ppf.Effective_End_Date
	AND rownum                 =1	--added for version 1.1
    ),HZLA.Audit_User_Name) User_Name,
    Hca.ACCOUNT_Number,
    HCA.ACCOUNT_NAME,
    HPS.PARTY_SITE_NUMBER site_number,
    HCSU.LOCATION SITE_NAME,
    'County' ATRIBUTE_NAME,
    HZLAV7.COUNTY Old_Value,
    xxeis.EIS_XXWC_CUST_AUDIT_HIST_PKG.get_cust_new_value('APPS.HZ_LOCATIONS_AV3','COUNTY',HZLA.ROW_KEY,HZLAV7.LOCATION_ID,'LOCATION_ID') new_value,
    'HZ_LOCATIONS' TABLE_NAME,
    (SELECT DESCRIPTION
    FROM FND_TABLES
    WHERE TABLE_NAME='HZ_LOCATIONS'
    AND ROWNUM      =1
    ) DESCRIPTION,
    DECODE(HCSU.site_use_id,NULL,
    (SELECT hcpcs.name
    FROM HZ_CUSTOMER_PROFILES hcpf,
      HZ_CUST_PROFILE_CLASSES hcpcs
    WHERE hcpf.profile_class_id=hcpcs.profile_class_id
    AND hcpf.cust_account_id   = HCA.CUST_ACCOUNT_ID
    AND hcpf.SITE_USE_ID      IS NULL
    AND rownum                 =1
    ),
    (SELECT hcpcs.name
    FROM HZ_CUSTOMER_PROFILES hcpf,
      HZ_CUST_PROFILE_CLASSES hcpcs
    WHERE hcpf.profile_class_id=hcpcs.profile_class_id
    AND hcpf.site_use_id       = HCSU.site_use_id
    AND hcpf.cust_account_id   = HCA.CUST_ACCOUNT_ID
    AND rownum                 =1
    )) profile_class
  FROM APPS.HZ_LOCATIONS_A HZLA,
    APPS.HZ_LOCATIONS_AV3 HZLAV7,
    HZ_PARTY_SITES HPS,
    HZ_CUST_SITE_USES_ALL HCSU ,
    APPS.HZ_CUST_ACCOUNTS HCA ,
    HZ_CUST_ACCT_SITES_ALL HCS
  WHERE HZLA.ROW_KEY              = HZLAV7.ROW_KEY
  AND HZLAV7.LOCATION_ID          = Hps.Location_Id
  AND Hps.Party_Site_Id           = Hcs.Party_Site_Id
  AND HCS.CUST_ACCT_SITE_ID       = HCSU.CUST_ACCT_SITE_ID
  AND Hcs.Cust_Account_Id         = Hca.Cust_Account_Id
  AND Hcsu.Site_Use_Code(+)       = 'BILL_TO'
  AND HZLA.AUDIT_TRANSACTION_TYPE = 'U'
  UNION ALL
  SELECT 'No' credit_report_flag,
    TRUNC(HZLA.Audit_Timestamp) trunc_Audit_Date,
    HZLA.Audit_Timestamp Audit_Date,
    DECODE(HZLA.Audit_Transaction_Type,'I','Insert') Audit_Type,
    HZLA.Audit_User_Name User_Ntid,
    NVL(
    (SELECT Ppf.Full_Name
    FROM Fnd_User Fu,
      Per_All_People_F Ppf
    WHERE Fu.User_Name=HZLA.Audit_User_Name
    AND Fu.Employee_Id=Ppf.person_id
    AND TRUNC(Sysdate) BETWEEN Ppf.Effective_Start_Date AND Ppf.Effective_End_Date
	AND rownum                 =1	--added for version 1.1
    ),HZLA.Audit_User_Name) User_Name,
    Hca.ACCOUNT_Number,
    HCA.ACCOUNT_NAME,
    HPS.PARTY_SITE_NUMBER site_number,
    Hcsu.location Site_Name,
    'County' ATRIBUTE_NAME,
    HZLA.COUNTY Old_Value,
    xxeis.EIS_XXWC_CUST_AUDIT_HIST_PKG.get_cust_insert_value('APPS.HZ_LOCATIONS_AV3','COUNTY',HZLA.LOCATION_ID,'LOCATION_ID') new_value,
    'HZ_LOCATIONS' TABLE_NAME,
    (SELECT DESCRIPTION
    FROM FND_TABLES
    WHERE TABLE_NAME='HZ_LOCATIONS'
    AND ROWNUM      =1
    ) DESCRIPTION,
    DECODE(HCSU.site_use_id,NULL,
    (SELECT hcpcs.name
    FROM HZ_CUSTOMER_PROFILES hcpf,
      HZ_CUST_PROFILE_CLASSES hcpcs
    WHERE hcpf.profile_class_id=hcpcs.profile_class_id
    AND hcpf.cust_account_id   = HCA.CUST_ACCOUNT_ID
    AND hcpf.SITE_USE_ID      IS NULL
    AND rownum                 =1
    ),
    (SELECT hcpcs.name
    FROM HZ_CUSTOMER_PROFILES hcpf,
      HZ_CUST_PROFILE_CLASSES hcpcs
    WHERE hcpf.profile_class_id=hcpcs.profile_class_id
    AND hcpf.site_use_id       = HCSU.site_use_id
    AND hcpf.cust_account_id   = HCA.CUST_ACCOUNT_ID
    AND rownum                 =1
    )) profile_class
  FROM Apps.HZ_LOCATIONS_A HZLA,
    HZ_PARTY_SITES HPS,
    HZ_CUST_SITE_USES_ALL HCSU ,
    APPS.HZ_CUST_ACCOUNTS HCA ,
    HZ_CUST_ACCT_SITES_ALL HCS
  WHERE Hzla.Location_Id                                                                                                         = Hps.Location_Id
  AND Hps.Party_Site_Id                                                                                                          = Hcs.Party_Site_Id
  AND HCS.CUST_ACCT_SITE_ID                                                                                                      = HCSU.CUST_ACCT_SITE_ID
  AND Hcs.Cust_Account_Id                                                                                                        = Hca.Cust_Account_Id
  AND Hcsu.Site_Use_Code(+)                                                                                                      = 'BILL_TO'
  AND Hzla.Audit_Transaction_Type                                                                                                = 'I'
  AND Xxeis.Eis_Xxwc_Cust_Audit_Hist_Pkg.Get_Cust_Insert_Value('APPS.HZ_LOCATIONS_AV3','COUNTY',Hzla.Location_Id,'LOCATION_ID') IS NOT NULL
  UNION ALL
  SELECT 'No' credit_report_flag,
    TRUNC(HZLA.Audit_Timestamp) trunc_Audit_Date,
    HZLA.Audit_Timestamp Audit_Date,
    DECODE(HZLA.Audit_Transaction_Type,'U','Update') Audit_Type,
    HZLA.Audit_User_Name User_Ntid,
    NVL(
    (SELECT Ppf.Full_Name
    FROM Fnd_User Fu,
      Per_All_People_F Ppf
    WHERE Fu.User_Name=HZLA.Audit_User_Name
    AND Fu.Employee_Id=Ppf.person_id
    AND TRUNC(Sysdate) BETWEEN Ppf.Effective_Start_Date AND Ppf.Effective_End_Date
	AND rownum                 =1	--added for version 1.1
    ),HZLA.Audit_User_Name) User_Name,
    Hca.ACCOUNT_Number,
    HCA.ACCOUNT_NAME,
    HPS.PARTY_SITE_NUMBER site_number,
    Hcsu.Location Site_Name,
    'Geography Code Override' ATRIBUTE_NAME,
    HZLAV8.SALES_TAX_GEOCODE Old_Value,
    xxeis.EIS_XXWC_CUST_AUDIT_HIST_PKG.get_cust_new_value('APPS.HZ_LOCATIONS_AV2','SALES_TAX_GEOCODE',HZLA.ROW_KEY,HZLAV8.LOCATION_ID,'LOCATION_ID') new_value,
    'HZ_LOCATIONS' TABLE_NAME,
    (SELECT DESCRIPTION
    FROM FND_TABLES
    WHERE TABLE_NAME='HZ_LOCATIONS'
    AND ROWNUM      =1
    ) DESCRIPTION,
    DECODE(HCSU.site_use_id,NULL,
    (SELECT hcpcs.name
    FROM HZ_CUSTOMER_PROFILES hcpf,
      HZ_CUST_PROFILE_CLASSES hcpcs
    WHERE hcpf.profile_class_id=hcpcs.profile_class_id
    AND hcpf.cust_account_id   = HCA.CUST_ACCOUNT_ID
    AND hcpf.SITE_USE_ID      IS NULL
    AND rownum                 =1
    ),
    (SELECT hcpcs.name
    FROM HZ_CUSTOMER_PROFILES hcpf,
      HZ_CUST_PROFILE_CLASSES hcpcs
    WHERE hcpf.profile_class_id=hcpcs.profile_class_id
    AND hcpf.site_use_id       = HCSU.site_use_id
    AND hcpf.cust_account_id   = HCA.CUST_ACCOUNT_ID
    AND rownum                 =1
    )) profile_class
  FROM APPS.HZ_LOCATIONS_A HZLA,
    APPS.HZ_LOCATIONS_AV2 HZLAV8,
    HZ_PARTY_SITES HPS,
    HZ_CUST_SITE_USES_ALL HCSU ,
    APPS.HZ_CUST_ACCOUNTS HCA ,
    HZ_CUST_ACCT_SITES_ALL HCS
  WHERE HZLA.ROW_KEY              = HZLAV8.ROW_KEY
  AND HZLAV8.LOCATION_ID          = Hps.Location_Id
  AND Hps.Party_Site_Id           = Hcs.Party_Site_Id
  AND HCS.CUST_ACCT_SITE_ID       = HCSU.CUST_ACCT_SITE_ID
  AND Hcs.Cust_Account_Id         = Hca.Cust_Account_Id
  AND Hcsu.Site_Use_Code(+)       = 'BILL_TO'
  AND HZLA.AUDIT_TRANSACTION_TYPE = 'U'
  UNION ALL
  SELECT 'No' credit_report_flag,
    TRUNC(HZLA.Audit_Timestamp) trunc_Audit_Date,
    HZLA.Audit_Timestamp Audit_Date,
    DECODE(HZLA.Audit_Transaction_Type,'I','Insert') Audit_Type,
    HZLA.Audit_User_Name User_Ntid,
    NVL(
    (SELECT Ppf.Full_Name
    FROM Fnd_User Fu,
      Per_All_People_F Ppf
    WHERE Fu.User_Name=HZLA.Audit_User_Name
    AND Fu.Employee_Id=Ppf.person_id
    AND TRUNC(Sysdate) BETWEEN Ppf.Effective_Start_Date AND Ppf.Effective_End_Date
	AND rownum                 =1	--added for version 1.1
    ),HZLA.Audit_User_Name) User_Name,
    Hca.ACCOUNT_Number,
    HCA.ACCOUNT_NAME,
    HPS.PARTY_SITE_NUMBER site_number,
    Hcsu.location Site_Name,
    'Geography Code Override' ATRIBUTE_NAME,
    HZLA.SALES_TAX_GEOCODE Old_Value,
    xxeis.EIS_XXWC_CUST_AUDIT_HIST_PKG.get_cust_insert_value('APPS.HZ_LOCATIONS_AV2','SALES_TAX_GEOCODE',HZLA.LOCATION_ID,'LOCATION_ID') new_value,
    'HZ_LOCATIONS' TABLE_NAME,
    (SELECT DESCRIPTION
    FROM FND_TABLES
    WHERE TABLE_NAME='HZ_LOCATIONS'
    AND ROWNUM      =1
    ) DESCRIPTION,
    DECODE(HCSU.site_use_id,NULL,
    (SELECT hcpcs.name
    FROM HZ_CUSTOMER_PROFILES hcpf,
      HZ_CUST_PROFILE_CLASSES hcpcs
    WHERE hcpf.profile_class_id=hcpcs.profile_class_id
    AND hcpf.cust_account_id   = HCA.CUST_ACCOUNT_ID
    AND hcpf.SITE_USE_ID      IS NULL
    AND rownum                 =1
    ),
    (SELECT hcpcs.name
    FROM HZ_CUSTOMER_PROFILES hcpf,
      HZ_CUST_PROFILE_CLASSES hcpcs
    WHERE hcpf.profile_class_id=hcpcs.profile_class_id
    AND hcpf.site_use_id       = HCSU.site_use_id
    AND hcpf.cust_account_id   = HCA.CUST_ACCOUNT_ID
    AND rownum                 =1
    )) profile_class
  FROM Apps.HZ_LOCATIONS_A HZLA,
    HZ_PARTY_SITES HPS,
    HZ_CUST_SITE_USES_ALL HCSU ,
    APPS.HZ_CUST_ACCOUNTS HCA ,
    HZ_CUST_ACCT_SITES_ALL HCS
  WHERE Hzla.Location_Id                                                                                                                    = Hps.Location_Id
  AND Hps.Party_Site_Id                                                                                                                     = Hcs.Party_Site_Id
  AND HCS.CUST_ACCT_SITE_ID                                                                                                                 = HCSU.CUST_ACCT_SITE_ID
  AND Hcs.Cust_Account_Id                                                                                                                   = Hca.Cust_Account_Id
  AND Hcsu.Site_Use_Code(+)                                                                                                                 = 'BILL_TO'
  AND HZLA.AUDIT_TRANSACTION_TYPE                                                                                                           = 'I'
  AND XXEIS.EIS_XXWC_CUST_AUDIT_HIST_PKG.GET_CUST_INSERT_VALUE('APPS.HZ_LOCATIONS_AV2','SALES_TAX_GEOCODE',HZLA.LOCATION_ID,'LOCATION_ID') IS NOT NULL
  UNION ALL
  SELECT 'Yes' credit_report_flag,
    TRUNC(Hcaa.Audit_Timestamp) trunc_Audit_Date,
    Hcaa.Audit_Timestamp Audit_Date,
    DECODE(Hcaa.Audit_Transaction_Type,'U','Update') Audit_Type,
    Hcaa.Audit_User_Name User_Ntid,
    NVL(
    (SELECT Ppf.Full_Name
    FROM Fnd_User Fu,
      Per_All_People_F Ppf
    WHERE Fu.User_Name=Hcaa.Audit_User_Name
    AND Fu.Employee_Id=Ppf.person_id
    AND TRUNC(Sysdate) BETWEEN Ppf.Effective_Start_Date AND Ppf.Effective_End_Date
	AND rownum                 =1	--added for version 1.1
    ),Hcaa.Audit_User_Name) User_Name,
    Hca.ACCOUNT_Number,
    HCA.ACCOUNT_NAME,
    HPS.PARTY_SITE_NUMBER site_number,
    HCSU.location SITE_NAME,
    '(Overall) Credit Limit' ATRIBUTE_NAME,
    TO_CHAR(HCAAV2.OVERALL_CREDIT_LIMIT) OLD_VALUE,
    TO_CHAR(xxeis.EIS_XXWC_CUST_AUDIT_HIST_PKG.get_cust_new_value('APPS.HZ_CUST_PROFILE_AMTS_AV2','OVERALL_CREDIT_LIMIT',HCAA.ROW_KEY,HCAAV2.CUST_ACCT_PROFILE_AMT_ID,'CUST_ACCT_PROFILE_AMT_ID')) new_value,
    'HZ_CUST_PROFILE_AMTS' TABLE_NAME,
    (SELECT DESCRIPTION
    FROM FND_TABLES
    WHERE TABLE_NAME='HZ_CUST_PROFILE_AMTS'
    AND rownum      =1
    ) DESCRIPTION,
    DECODE(HCSU.site_use_id,NULL,
    (SELECT hcpcs.name
    FROM HZ_CUSTOMER_PROFILES hcpf,
      HZ_CUST_PROFILE_CLASSES hcpcs
    WHERE hcpf.profile_class_id=hcpcs.profile_class_id
    AND hcpf.cust_account_id   = HCA.CUST_ACCOUNT_ID
    AND hcpf.SITE_USE_ID      IS NULL
    AND rownum                 =1
    ),
    (SELECT hcpcs.name
    FROM HZ_CUSTOMER_PROFILES hcpf,
      HZ_CUST_PROFILE_CLASSES hcpcs
    WHERE hcpf.profile_class_id=hcpcs.profile_class_id
    AND hcpf.site_use_id       = HCSU.site_use_id
    AND hcpf.cust_account_id   = HCA.CUST_ACCOUNT_ID
    AND rownum                 =1
    )) profile_class
  FROM APPS.HZ_CUST_PROFILE_AMTS_A HCAA,
    APPS.HZ_CUST_PROFILE_AMTS_AV2 HCAAV2,
    HZ_CUST_PROFILE_AMTS HCPA,
    APPS.HZ_CUST_ACCOUNTS HCA ,
    HZ_CUST_SITE_USES_ALL HCSU,
    HZ_CUST_ACCT_SITES_ALL HCS,
    Hz_Party_Sites Hps
  WHERE HCAA.ROW_KEY                 = HCAAV2.ROW_KEY
  AND HCAAV2.CUST_ACCT_PROFILE_AMT_ID=HCPA.CUST_ACCT_PROFILE_AMT_ID
  AND HCPA.CUST_ACCOUNT_ID           = HCA.CUST_ACCOUNT_ID
  AND HCPA.SITE_USE_ID               = HCSU.SITE_USE_ID(+)
  AND hcsu.cust_acct_site_id         = hcs.cust_acct_site_id (+)
  AND HCS.PARTY_SITE_ID              = HPS.PARTY_SITE_ID(+)
  AND HCSU.SITE_USE_CODE(+)          = 'BILL_TO'
  AND HCAA.AUDIT_TRANSACTION_TYPE    ='U'
  UNION ALL
  SELECT 'Yes' credit_report_flag,
    TRUNC(Hcaa.Audit_Timestamp) trunc_Audit_Date,
    Hcaa.Audit_Timestamp Audit_Date,
    DECODE(Hcaa.Audit_Transaction_Type,'I','Insert') Audit_Type,
    Hcaa.Audit_User_Name User_Ntid,
    NVL(
    (SELECT Ppf.Full_Name
    FROM Fnd_User Fu,
      Per_All_People_F Ppf
    WHERE Fu.User_Name=Hcaa.Audit_User_Name
    AND Fu.Employee_Id=Ppf.person_id
    AND TRUNC(Sysdate) BETWEEN Ppf.Effective_Start_Date AND Ppf.Effective_End_Date
	AND rownum                 =1	--added for version 1.1
    ),Hcaa.Audit_User_Name) User_Name,
    Hca.ACCOUNT_Number,
    HCA.ACCOUNT_NAME,
    HPS.PARTY_SITE_NUMBER site_number,
    HCSU.location SITE_NAME,
    '(Overall) Credit Limit' ATRIBUTE_NAME,
    TO_CHAR(HCAA.OVERALL_CREDIT_LIMIT) OLD_VALUE,
    TO_CHAR(xxeis.EIS_XXWC_CUST_AUDIT_HIST_PKG.get_cust_insert_value('APPS.HZ_CUST_PROFILE_AMTS_AV2','OVERALL_CREDIT_LIMIT',HCAA.CUST_ACCT_PROFILE_AMT_ID,'CUST_ACCT_PROFILE_AMT_ID')) new_value,
    'HZ_CUST_PROFILE_AMTS' TABLE_NAME,
    (SELECT DESCRIPTION
    FROM FND_TABLES
    WHERE TABLE_NAME='HZ_CUST_PROFILE_AMTS'
    AND rownum      =1
    ) DESCRIPTION,
    DECODE(HCSU.site_use_id,NULL,
    (SELECT hcpcs.name
    FROM HZ_CUSTOMER_PROFILES hcpf,
      HZ_CUST_PROFILE_CLASSES hcpcs
    WHERE hcpf.profile_class_id=hcpcs.profile_class_id
    AND hcpf.cust_account_id   = HCA.CUST_ACCOUNT_ID
    AND hcpf.SITE_USE_ID      IS NULL
    AND rownum                 =1
    ),
    (SELECT hcpcs.name
    FROM HZ_CUSTOMER_PROFILES hcpf,
      HZ_CUST_PROFILE_CLASSES hcpcs
    WHERE hcpf.profile_class_id=hcpcs.profile_class_id
    AND hcpf.site_use_id       = HCSU.site_use_id
    AND hcpf.cust_account_id   = HCA.CUST_ACCOUNT_ID
    AND rownum                 =1
    )) profile_class
  FROM APPS.HZ_CUST_PROFILE_AMTS_A HCAA,
    HZ_CUST_PROFILE_AMTS HCPA,
    APPS.HZ_CUST_ACCOUNTS HCA ,
    HZ_CUST_SITE_USES_ALL HCSU,
    HZ_CUST_ACCT_SITES_ALL HCS,
    Hz_Party_Sites Hps
  WHERE HCAA.CUST_ACCT_PROFILE_AMT_ID                                                                                                                                            =HCPA.CUST_ACCT_PROFILE_AMT_ID
  AND HCPA.CUST_ACCOUNT_ID                                                                                                                                                       = HCA.CUST_ACCOUNT_ID
  AND HCPA.SITE_USE_ID                                                                                                                                                           = HCSU.SITE_USE_ID(+)
  AND hcsu.cust_acct_site_id                                                                                                                                                     = hcs.cust_acct_site_id (+)
  AND HCS.PARTY_SITE_ID                                                                                                                                                          = HPS.PARTY_SITE_ID(+)
  AND HCSU.SITE_USE_CODE(+)                                                                                                                                                      = 'BILL_TO'
  AND HCAA.AUDIT_TRANSACTION_TYPE                                                                                                                                                ='I'
  AND XXEIS.EIS_XXWC_CUST_AUDIT_HIST_PKG.GET_CUST_INSERT_VALUE('APPS.HZ_CUST_PROFILE_AMTS_AV2','OVERALL_CREDIT_LIMIT',HCAA.CUST_ACCT_PROFILE_AMT_ID,'CUST_ACCT_PROFILE_AMT_ID') IS NOT NULL
  UNION ALL
  SELECT 'Yes' credit_report_flag,
    TRUNC(Hcaa.Audit_Timestamp) trunc_Audit_Date,
    Hcaa.Audit_Timestamp Audit_Date,
    DECODE(Hcaa.Audit_Transaction_Type,'U','Update') Audit_Type,
    Hcaa.Audit_User_Name User_Ntid,
    NVL(
    (SELECT Ppf.Full_Name
    FROM Fnd_User Fu,
      Per_All_People_F Ppf
    WHERE Fu.User_Name=Hcaa.Audit_User_Name
    AND Fu.Employee_Id=Ppf.person_id
    AND TRUNC(Sysdate) BETWEEN Ppf.Effective_Start_Date AND Ppf.Effective_End_Date
	AND rownum                 =1	--added for version 1.1
    ),Hcaa.Audit_User_Name) User_Name,
    Hca.ACCOUNT_Number,
    HCA.ACCOUNT_NAME,
    HPS.PARTY_SITE_NUMBER site_number,
    HCSU.LOCATION SITE_NAME,
    'Trx Credit Limit' ATRIBUTE_NAME,
    TO_CHAR(HCAAV3.TRX_CREDIT_LIMIT) OLD_VALUE,
    TO_CHAR(xxeis.EIS_XXWC_CUST_AUDIT_HIST_PKG.get_cust_new_value('APPS.HZ_CUST_PROFILE_AMTS_AV3','TRX_CREDIT_LIMIT',HCAA.ROW_KEY,HCAAV3.CUST_ACCT_PROFILE_AMT_ID,'CUST_ACCT_PROFILE_AMT_ID')) new_value,
    'HZ_CUST_PROFILE_AMTS' TABLE_NAME,
    (SELECT DESCRIPTION
    FROM FND_TABLES
    WHERE TABLE_NAME='HZ_CUST_PROFILE_AMTS'
    AND rownum      =1
    ) DESCRIPTION,
    DECODE(HCSU.site_use_id,NULL,
    (SELECT hcpcs.name
    FROM HZ_CUSTOMER_PROFILES hcpf,
      HZ_CUST_PROFILE_CLASSES hcpcs
    WHERE hcpf.profile_class_id=hcpcs.profile_class_id
    AND hcpf.cust_account_id   = HCA.CUST_ACCOUNT_ID
    AND hcpf.SITE_USE_ID      IS NULL
    AND rownum                 =1
    ),
    (SELECT hcpcs.name
    FROM HZ_CUSTOMER_PROFILES hcpf,
      HZ_CUST_PROFILE_CLASSES hcpcs
    WHERE hcpf.profile_class_id=hcpcs.profile_class_id
    AND hcpf.site_use_id       = HCSU.site_use_id
    AND hcpf.cust_account_id   = HCA.CUST_ACCOUNT_ID
    AND rownum                 =1
    )) profile_class
  FROM APPS.HZ_CUST_PROFILE_AMTS_A HCAA,
    APPS.HZ_CUST_PROFILE_AMTS_AV3 HCAAV3,
    HZ_CUST_PROFILE_AMTS HCPA,
    APPS.HZ_CUST_ACCOUNTS HCA ,
    HZ_CUST_SITE_USES_ALL HCSU,
    HZ_CUST_ACCT_SITES_ALL HCS,
    Hz_Party_Sites Hps
  WHERE HCAA.ROW_KEY                 = HCAAV3.ROW_KEY
  AND HCAAV3.CUST_ACCT_PROFILE_AMT_ID=HCPA.CUST_ACCT_PROFILE_AMT_ID
  AND HCPA.CUST_ACCOUNT_ID           = HCA.CUST_ACCOUNT_ID
  AND HCPA.SITE_USE_ID               = HCSU.SITE_USE_ID(+)
  AND hcsu.cust_acct_site_id         = hcs.cust_acct_site_id (+)
  AND HCS.PARTY_SITE_ID              = HPS.PARTY_SITE_ID(+)
  AND HCSU.SITE_USE_CODE(+)          = 'BILL_TO'
  AND HCAA.AUDIT_TRANSACTION_TYPE    ='U'
  UNION ALL
  SELECT 'Yes' credit_report_flag,
    TRUNC(Hcaa.Audit_Timestamp) trunc_Audit_Date,
    Hcaa.Audit_Timestamp Audit_Date,
    DECODE(Hcaa.Audit_Transaction_Type,'I','Insert') Audit_Type,
    Hcaa.Audit_User_Name User_Ntid,
    NVL(
    (SELECT Ppf.Full_Name
    FROM Fnd_User Fu,
      Per_All_People_F Ppf
    WHERE Fu.User_Name=Hcaa.Audit_User_Name
    AND Fu.Employee_Id=Ppf.person_id
    AND TRUNC(Sysdate) BETWEEN Ppf.Effective_Start_Date AND Ppf.Effective_End_Date
	AND rownum                 =1	--added for version 1.1
    ),Hcaa.Audit_User_Name) User_Name,
    Hca.ACCOUNT_Number,
    HCA.ACCOUNT_NAME,
    HPS.PARTY_SITE_NUMBER site_number,
    HCSU.location SITE_NAME,
    'Trx Credit Limit' ATRIBUTE_NAME,
    TO_CHAR(HCAA.TRX_CREDIT_LIMIT) OLD_VALUE,
    TO_CHAR(xxeis.EIS_XXWC_CUST_AUDIT_HIST_PKG.get_cust_insert_value('APPS.HZ_CUST_PROFILE_AMTS_AV3','TRX_CREDIT_LIMIT',HCAA.CUST_ACCT_PROFILE_AMT_ID,'CUST_ACCT_PROFILE_AMT_ID')) new_value,
    'HZ_CUST_PROFILE_AMTS' TABLE_NAME,
    (SELECT DESCRIPTION
    FROM FND_TABLES
    WHERE TABLE_NAME='HZ_CUST_PROFILE_AMTS'
    AND rownum      =1
    ) DESCRIPTION,
    DECODE(HCSU.site_use_id,NULL,
    (SELECT hcpcs.name
    FROM HZ_CUSTOMER_PROFILES hcpf,
      HZ_CUST_PROFILE_CLASSES hcpcs
    WHERE hcpf.profile_class_id=hcpcs.profile_class_id
    AND hcpf.cust_account_id   = HCA.CUST_ACCOUNT_ID
    AND hcpf.SITE_USE_ID      IS NULL
    AND rownum                 =1
    ),
    (SELECT hcpcs.name
    FROM HZ_CUSTOMER_PROFILES hcpf,
      HZ_CUST_PROFILE_CLASSES hcpcs
    WHERE hcpf.profile_class_id=hcpcs.profile_class_id
    AND hcpf.site_use_id       = HCSU.site_use_id
    AND hcpf.cust_account_id   = HCA.CUST_ACCOUNT_ID
    AND rownum                 =1
    )) profile_class
  FROM APPS.HZ_CUST_PROFILE_AMTS_A HCAA,
    HZ_CUST_PROFILE_AMTS HCPA,
    APPS.HZ_CUST_ACCOUNTS HCA ,
    HZ_CUST_SITE_USES_ALL HCSU,
    HZ_CUST_ACCT_SITES_ALL HCS,
    Hz_Party_Sites Hps
  WHERE HCAA.CUST_ACCT_PROFILE_AMT_ID                                                                                                                                        =HCPA.CUST_ACCT_PROFILE_AMT_ID
  AND HCPA.CUST_ACCOUNT_ID                                                                                                                                                   = HCA.CUST_ACCOUNT_ID
  AND HCPA.SITE_USE_ID                                                                                                                                                       = HCSU.SITE_USE_ID(+)
  AND hcsu.cust_acct_site_id                                                                                                                                                 = hcs.cust_acct_site_id (+)
  AND HCS.PARTY_SITE_ID                                                                                                                                                      = HPS.PARTY_SITE_ID(+)
  AND HCSU.SITE_USE_CODE(+)                                                                                                                                                  = 'BILL_TO'
  AND HCAA.AUDIT_TRANSACTION_TYPE                                                                                                                                            ='I'
  AND XXEIS.EIS_XXWC_CUST_AUDIT_HIST_PKG.GET_CUST_INSERT_VALUE('APPS.HZ_CUST_PROFILE_AMTS_AV3','TRX_CREDIT_LIMIT',HCAA.CUST_ACCT_PROFILE_AMT_ID,'CUST_ACCT_PROFILE_AMT_ID') IS NOT NULL
  UNION ALL
  SELECT 'No' credit_report_flag,
    TRUNC(HCASA.Audit_Timestamp) trunc_Audit_Date,
    HCASA.Audit_Timestamp Audit_Date,
    DECODE(HCASA.Audit_Transaction_Type,'U','Update') Audit_Type,
    HCASA.Audit_User_Name User_Ntid,
    NVL(
    (SELECT Ppf.Full_Name
    FROM Fnd_User Fu,
      Per_All_People_F Ppf
    WHERE Fu.User_Name=HCASA.Audit_User_Name
    AND Fu.Employee_Id=Ppf.person_id
    AND TRUNC(Sysdate) BETWEEN Ppf.Effective_Start_Date AND Ppf.Effective_End_Date
	AND rownum                 =1	--added for version 1.1
    ),HCASA.Audit_User_Name) User_Name,
    Hca.ACCOUNT_Number,
    HCA.ACCOUNT_NAME,
    HPS.PARTY_SITE_NUMBER site_number,
    Hcsu.Location Site_Name,
    'Tax Exemption Type' ATRIBUTE_NAME,
    HCASA3.ATTRIBUTE15 Old_Value,
    xxeis.EIS_XXWC_CUST_AUDIT_HIST_PKG.get_cust_new_value('APPS.HZ_CUST_ACCT_SITES_ALL_AV3','ATTRIBUTE15',HCASA.ROW_KEY,HCASA3.CUST_ACCT_SITE_ID,'CUST_ACCT_SITE_ID') new_value,
    'HZ_CUST_ACCT_SITES_ALL' TABLE_NAME,
    (SELECT DESCRIPTION
    FROM FND_TABLES
    WHERE TABLE_NAME='HZ_CUST_ACCT_SITES_ALL'
    AND ROWNUM      =1
    ) DESCRIPTION,
    DECODE(HCSU.site_use_id,NULL,
    (SELECT hcpcs.name
    FROM HZ_CUSTOMER_PROFILES hcpf,
      HZ_CUST_PROFILE_CLASSES hcpcs
    WHERE hcpf.profile_class_id=hcpcs.profile_class_id
    AND hcpf.cust_account_id   = HCA.CUST_ACCOUNT_ID
    AND hcpf.SITE_USE_ID      IS NULL
    AND rownum                 =1
    ),
    (SELECT hcpcs.name
    FROM HZ_CUSTOMER_PROFILES hcpf,
      HZ_CUST_PROFILE_CLASSES hcpcs
    WHERE hcpf.profile_class_id=hcpcs.profile_class_id
    AND hcpf.site_use_id       = HCSU.site_use_id
    AND hcpf.cust_account_id   = HCA.CUST_ACCOUNT_ID
    AND rownum                 =1
    )) profile_class
  FROM APPS.HZ_CUST_ACCT_SITES_ALL_A HCASA,
    APPS.HZ_CUST_ACCT_SITES_ALL_AV3 HCASA3,
    HZ_CUST_ACCT_SITES_ALL HCS,
    HZ_CUST_SITE_USES_ALL HCSU ,
    APPS.HZ_CUST_ACCOUNTS HCA ,
    HZ_PARTY_SITES HPS
  WHERE HCASA.ROW_KEY              = HCASA3.ROW_KEY
  AND HCASA3.CUST_ACCT_SITE_ID     = HCS.CUST_ACCT_SITE_ID
  AND HCS.CUST_ACCT_SITE_ID        = HCSU.CUST_ACCT_SITE_ID
  AND HCS.CUST_ACCOUNT_ID          = HCA.CUST_ACCOUNT_ID
  AND HCS.PARTY_SITE_ID            = HPS.PARTY_SITE_ID(+)
  AND HCSU.SITE_USE_CODE(+)        = 'BILL_TO'
  AND HCASA.AUDIT_TRANSACTION_TYPE = 'U'
  UNION ALL
  SELECT 'No' credit_report_flag,
    TRUNC(HCASA.Audit_Timestamp) trunc_Audit_Date,
    HCASA.Audit_Timestamp Audit_Date,
    DECODE(HCASA.Audit_Transaction_Type,'I','Insert') Audit_Type,
    HCASA.Audit_User_Name User_Ntid,
    NVL(
    (SELECT Ppf.Full_Name
    FROM Fnd_User Fu,
      Per_All_People_F Ppf
    WHERE Fu.User_Name=HCASA.Audit_User_Name
    AND Fu.Employee_Id=Ppf.person_id
    AND TRUNC(Sysdate) BETWEEN Ppf.Effective_Start_Date AND Ppf.Effective_End_Date
	AND rownum                 =1	--added for version 1.1
    ),HCASA.Audit_User_Name) User_Name,
    Hca.ACCOUNT_Number,
    HCA.ACCOUNT_NAME,
    HPS.PARTY_SITE_NUMBER site_number,
    Hcsu.Location Site_Name,
    'Tax Exemption Type' ATRIBUTE_NAME,
    HCASA.ATTRIBUTE15 Old_Value,
    xxeis.EIS_XXWC_CUST_AUDIT_HIST_PKG.get_cust_insert_value('APPS.HZ_CUST_ACCT_SITES_ALL_AV3','ATTRIBUTE15',HCASA.CUST_ACCT_SITE_ID,'CUST_ACCT_SITE_ID') new_value,
    'HZ_CUST_ACCT_SITES_ALL' TABLE_NAME,
    (SELECT DESCRIPTION
    FROM FND_TABLES
    WHERE TABLE_NAME='HZ_CUST_ACCT_SITES_ALL'
    AND ROWNUM      =1
    ) DESCRIPTION,
    DECODE(HCSU.site_use_id,NULL,
    (SELECT hcpcs.name
    FROM HZ_CUSTOMER_PROFILES hcpf,
      HZ_CUST_PROFILE_CLASSES hcpcs
    WHERE hcpf.profile_class_id=hcpcs.profile_class_id
    AND hcpf.cust_account_id   = HCA.CUST_ACCOUNT_ID
    AND hcpf.SITE_USE_ID      IS NULL
    AND rownum                 =1
    ),
    (SELECT hcpcs.name
    FROM HZ_CUSTOMER_PROFILES hcpf,
      HZ_CUST_PROFILE_CLASSES hcpcs
    WHERE hcpf.profile_class_id=hcpcs.profile_class_id
    AND hcpf.site_use_id       = HCSU.site_use_id
    AND hcpf.cust_account_id   = HCA.CUST_ACCOUNT_ID
    AND rownum                 =1
    )) profile_class
  FROM APPS.HZ_CUST_ACCT_SITES_ALL_A HCASA,
    HZ_CUST_ACCT_SITES_ALL HCS,
    HZ_CUST_SITE_USES_ALL HCSU ,
    APPS.HZ_CUST_ACCOUNTS HCA ,
    HZ_PARTY_SITES HPS
  WHERE HCASA.CUST_ACCT_SITE_ID                                                                                                                              = HCS.CUST_ACCT_SITE_ID
  AND HCS.CUST_ACCT_SITE_ID                                                                                                                                  = HCSU.CUST_ACCT_SITE_ID
  AND HCS.CUST_ACCOUNT_ID                                                                                                                                    = HCA.CUST_ACCOUNT_ID
  AND HCS.PARTY_SITE_ID                                                                                                                                      = HPS.PARTY_SITE_ID(+)
  AND Hcsu.Site_Use_Code(+)                                                                                                                                  = 'BILL_TO'
  AND HCASA.Audit_Transaction_Type                                                                                                                           = 'I'
  AND xxeis.EIS_XXWC_CUST_AUDIT_HIST_PKG.get_cust_insert_value('APPS.HZ_CUST_ACCT_SITES_ALL_AV3','ATTRIBUTE15',HCASA.CUST_ACCT_SITE_ID,'CUST_ACCT_SITE_ID') IS NOT NULL
  UNION ALL
  SELECT 'No' credit_report_flag,
    TRUNC(HCASA.Audit_Timestamp) trunc_Audit_Date,
    HCASA.Audit_Timestamp Audit_Date,
    DECODE(HCASA.Audit_Transaction_Type,'U','Update') Audit_Type,
    HCASA.Audit_User_Name User_Ntid,
    NVL(
    (SELECT Ppf.Full_Name
    FROM Fnd_User Fu,
      Per_All_People_F Ppf
    WHERE Fu.User_Name=HCASA.Audit_User_Name
    AND Fu.Employee_Id=Ppf.person_id
    AND TRUNC(Sysdate) BETWEEN Ppf.Effective_Start_Date AND Ppf.Effective_End_Date
	AND rownum                 =1	--added for version 1.1
    ),HCASA.Audit_User_Name) User_Name,
    Hca.ACCOUNT_Number,
    HCA.ACCOUNT_NAME,
    HPS.PARTY_SITE_NUMBER site_number,
    Hcsu.Location Site_Name,
    'Tax Exempt' ATRIBUTE_NAME,
    DECODE(HCASA3.ATTRIBUTE16,'Y','Yes','N','No') Old_Value,
    DECODE(xxeis.EIS_XXWC_CUST_AUDIT_HIST_PKG.get_cust_new_value('APPS.HZ_CUST_ACCT_SITES_ALL_AV2','ATTRIBUTE16',HCASA.ROW_KEY,HCASA3.CUST_ACCT_SITE_ID,'CUST_ACCT_SITE_ID'),'Y','Yes','N','No') new_value,
    'HZ_CUST_ACCT_SITES_ALL' TABLE_NAME,
    (SELECT DESCRIPTION
    FROM FND_TABLES
    WHERE TABLE_NAME='HZ_CUST_ACCT_SITES_ALL'
    AND ROWNUM      =1
    ) DESCRIPTION,
    DECODE(HCSU.site_use_id,NULL,
    (SELECT hcpcs.name
    FROM HZ_CUSTOMER_PROFILES hcpf,
      HZ_CUST_PROFILE_CLASSES hcpcs
    WHERE hcpf.profile_class_id=hcpcs.profile_class_id
    AND hcpf.cust_account_id   = HCA.CUST_ACCOUNT_ID
    AND hcpf.SITE_USE_ID      IS NULL
    AND rownum                 =1
    ),
    (SELECT hcpcs.name
    FROM HZ_CUSTOMER_PROFILES hcpf,
      HZ_CUST_PROFILE_CLASSES hcpcs
    WHERE hcpf.profile_class_id=hcpcs.profile_class_id
    AND hcpf.site_use_id       = HCSU.site_use_id
    AND hcpf.cust_account_id   = HCA.CUST_ACCOUNT_ID
    AND rownum                 =1
    )) profile_class
  FROM APPS.HZ_CUST_ACCT_SITES_ALL_A HCASA,
    APPS.HZ_CUST_ACCT_SITES_ALL_AV2 HCASA3,
    HZ_CUST_ACCT_SITES_ALL HCS,
    HZ_CUST_SITE_USES_ALL HCSU ,
    APPS.HZ_CUST_ACCOUNTS HCA ,
    HZ_PARTY_SITES HPS
  WHERE HCASA.ROW_KEY              = HCASA3.ROW_KEY
  AND HCASA3.CUST_ACCT_SITE_ID     = HCS.CUST_ACCT_SITE_ID
  AND HCS.CUST_ACCT_SITE_ID        = HCSU.CUST_ACCT_SITE_ID
  AND HCS.CUST_ACCOUNT_ID          = HCA.CUST_ACCOUNT_ID
  AND HCS.PARTY_SITE_ID            = HPS.PARTY_SITE_ID(+)
  AND HCSU.SITE_USE_CODE(+)        = 'BILL_TO'
  AND HCASA.AUDIT_TRANSACTION_TYPE = 'U'
  UNION ALL
  SELECT 'No' credit_report_flag,
    TRUNC(HCASA.Audit_Timestamp) trunc_Audit_Date,
    HCASA.Audit_Timestamp Audit_Date,
    DECODE(HCASA.Audit_Transaction_Type,'I','Insert') Audit_Type,
    HCASA.Audit_User_Name User_Ntid,
    NVL(
    (SELECT Ppf.Full_Name
    FROM Fnd_User Fu,
      Per_All_People_F Ppf
    WHERE Fu.User_Name=HCASA.Audit_User_Name
    AND Fu.Employee_Id=Ppf.person_id
    AND TRUNC(Sysdate) BETWEEN Ppf.Effective_Start_Date AND Ppf.Effective_End_Date
	AND rownum                 =1	--added for version 1.1
    ),HCASA.Audit_User_Name) User_Name,
    Hca.ACCOUNT_Number,
    HCA.ACCOUNT_NAME,
    HPS.PARTY_SITE_NUMBER site_number,
    Hcsu.Location Site_Name,
    'Tax Exempt' ATRIBUTE_NAME,
    DECODE(HCASA.ATTRIBUTE16,'Y','Yes','N','No') Old_Value,
    DECODE(xxeis.EIS_XXWC_CUST_AUDIT_HIST_PKG.get_cust_insert_value('APPS.HZ_CUST_ACCT_SITES_ALL_AV2','ATTRIBUTE16',HCASA.CUST_ACCT_SITE_ID,'CUST_ACCT_SITE_ID'),'Y','Yes','N','No') new_value,
    'HZ_CUST_ACCT_SITES_ALL' TABLE_NAME,
    (SELECT DESCRIPTION
    FROM FND_TABLES
    WHERE TABLE_NAME='HZ_CUST_ACCT_SITES_ALL'
    AND ROWNUM      =1
    ) DESCRIPTION,
    DECODE(HCSU.site_use_id,NULL,
    (SELECT hcpcs.name
    FROM HZ_CUSTOMER_PROFILES hcpf,
      HZ_CUST_PROFILE_CLASSES hcpcs
    WHERE hcpf.profile_class_id=hcpcs.profile_class_id
    AND hcpf.cust_account_id   = HCA.CUST_ACCOUNT_ID
    AND hcpf.SITE_USE_ID      IS NULL
    AND rownum                 =1
    ),
    (SELECT hcpcs.name
    FROM HZ_CUSTOMER_PROFILES hcpf,
      HZ_CUST_PROFILE_CLASSES hcpcs
    WHERE hcpf.profile_class_id=hcpcs.profile_class_id
    AND hcpf.site_use_id       = HCSU.site_use_id
    AND hcpf.cust_account_id   = HCA.CUST_ACCOUNT_ID
    AND rownum                 =1
    )) profile_class
  FROM APPS.HZ_CUST_ACCT_SITES_ALL_A HCASA,
    HZ_CUST_ACCT_SITES_ALL HCS,
    HZ_CUST_SITE_USES_ALL HCSU ,
    APPS.HZ_CUST_ACCOUNTS HCA ,
    HZ_PARTY_SITES HPS
  WHERE HCASA.CUST_ACCT_SITE_ID                                                                                                                              = HCS.CUST_ACCT_SITE_ID
  AND HCS.CUST_ACCT_SITE_ID                                                                                                                                  = HCSU.CUST_ACCT_SITE_ID
  AND HCS.CUST_ACCOUNT_ID                                                                                                                                    = HCA.CUST_ACCOUNT_ID
  AND HCS.PARTY_SITE_ID                                                                                                                                      = HPS.PARTY_SITE_ID(+)
  AND Hcsu.Site_Use_Code(+)                                                                                                                                  = 'BILL_TO'
  AND HCASA.Audit_Transaction_Type                                                                                                                           = 'I'
  AND xxeis.EIS_XXWC_CUST_AUDIT_HIST_PKG.get_cust_insert_value('APPS.HZ_CUST_ACCT_SITES_ALL_AV2','ATTRIBUTE16',HCASA.CUST_ACCT_SITE_ID,'CUST_ACCT_SITE_ID') IS NOT NULL
/
