/*
 TMS: 20160114-00025     
 Date: 01/14/2016
 Notes: @SF Data fix script for I643850
*/

SET SERVEROUTPUT ON SIZE 100000;
SET VERIFY OFF;


update apps.wsh_delivery_Details
set OE_INTERFACED_FLAG='Y'
where DELIVERY_DETAIL_ID=13873238
and source_header_id=37987612
and source_line_id=62270416;
		  
--1 row expected to be updated

commit;

/
