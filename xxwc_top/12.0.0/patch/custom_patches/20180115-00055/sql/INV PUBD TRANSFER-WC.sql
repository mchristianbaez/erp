--Report Name            : INV PUBD TRANSFER-WC
--Import Instructions    : Run this file in XXEIS user from SQL*Plus or any tool like Toad, SQL Navigator. 
prompt Creating Object Data EIS_XXWC_INV_PUBD_TRANSFER_V
set scan off define off
DECLARE
mod_exist VARCHAR2(1);
BEGIN 
mod_exist := XXEIS.EIS_RSC_INSERT.check_if_module_exist(401);
IF mod_exist = 'Y' THEN 
--Inserting Object EIS_XXWC_INV_PUBD_TRANSFER_V
xxeis.eis_rsc_ins.v( 'EIS_XXWC_INV_PUBD_TRANSFER_V',401,'','','','','SA059956','XXEIS','Eis Xxwc Inv Pubd Transfer V','EXIPTV','','','VIEW','US','','','');
--Delete Object Columns for EIS_XXWC_INV_PUBD_TRANSFER_V
xxeis.eis_rsc_utility.delete_view_rows('EIS_XXWC_INV_PUBD_TRANSFER_V',401,FALSE);
--Inserting Object Columns for EIS_XXWC_INV_PUBD_TRANSFER_V
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_INV_PUBD_TRANSFER_V','ITEM_EXTENDED_COST',401,'Item Extended Cost','ITEM_EXTENDED_COST','','~T~D~2','','SA059956','NUMBER','','','Item Extended Cost','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_INV_PUBD_TRANSFER_V','DAYS_IN_PUBD',401,'Days In Pubd','DAYS_IN_PUBD','','','','SA059956','NUMBER','','','Days In Pubd','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_INV_PUBD_TRANSFER_V','SHELF_LIFE_DAYS',401,'Shelf Life Days','SHELF_LIFE_DAYS','','','','SA059956','NUMBER','','','Shelf Life Days','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_INV_PUBD_TRANSFER_V','UOM',401,'Uom','UOM','','','','SA059956','VARCHAR2','','','Uom','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_INV_PUBD_TRANSFER_V','QUANTITY',401,'Quantity','QUANTITY','','~T~D~2','','SA059956','NUMBER','','','Quantity','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_INV_PUBD_TRANSFER_V','DESCRIPTION',401,'Description','DESCRIPTION','','','','SA059956','VARCHAR2','','','Description','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_INV_PUBD_TRANSFER_V','ITEM_NUMBER',401,'Item Number','ITEM_NUMBER','','','','SA059956','VARCHAR2','','','Item Number','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_INV_PUBD_TRANSFER_V','ORG',401,'Org','ORG','','','','SA059956','VARCHAR2','','','Org','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_INV_PUBD_TRANSFER_V','ITEM_COST',401,'Item Cost','ITEM_COST','','~T~D~2','','SA059956','NUMBER','','','Item Cost','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_INV_PUBD_TRANSFER_V','VENDOR_NAME',401,'Vendor Name','VENDOR_NAME','','','','SA059956','VARCHAR2','','','Vendor Name','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_INV_PUBD_TRANSFER_V','MMT_TRANSACTION_ID',401,'Mmt Transaction Id','MMT_TRANSACTION_ID','','','','SA059956','NUMBER','','','Mmt Transaction Id','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_INV_PUBD_TRANSFER_V','MSIB_INVENTORY_ITEM_ID',401,'Msib Inventory Item Id','MSIB_INVENTORY_ITEM_ID','','','','SA059956','NUMBER','','','Msib Inventory Item Id','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_INV_PUBD_TRANSFER_V','MSIB_ORGANIZATION_ID',401,'Msib Organization Id','MSIB_ORGANIZATION_ID','','','','SA059956','NUMBER','','','Msib Organization Id','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_INV_PUBD_TRANSFER_V','BORN_ON_DATE',401,'Born On Date','BORN_ON_DATE','','','','SA059956','DATE','','','Born On Date','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_INV_PUBD_TRANSFER_V','DATE_MOVED_TO_PUBD',401,'Date Moved To Pubd','DATE_MOVED_TO_PUBD','','','','SA059956','DATE','','','Date Moved To Pubd','','','','','');
--Inserting Object Components for EIS_XXWC_INV_PUBD_TRANSFER_V
--Inserting Object Component Joins for EIS_XXWC_INV_PUBD_TRANSFER_V
ELSE
RAISE_APPLICATION_ERROR (-20001,'Module does not exist. Import/Create the module with id 401');
END IF;
END;
/
prompt Creating Report LOV Data for INV PUBD TRANSFER-WC
set scan off define off
DECLARE
mod_exist VARCHAR2(1);
BEGIN 
mod_exist := XXEIS.EIS_RSC_INSERT.check_if_module_exist(401);
IF mod_exist = 'Y' THEN 
--Inserting Report LOVs - INV PUBD TRANSFER-WC
xxeis.eis_rsc_ins.lov( '','SELECT ORGANIZATION_CODE WAREHOUSE,ORGANIZATION_NAME
FROM ORG_ORGANIZATION_DEFINITIONS OOD
WHERE SYSDATE < NVL(OOD.DISABLE_DATE,SYSDATE+1) AND EXISTS (SELECT 1 FROM XXEIS.EIS_ORG_ACCESS_V  WHERE organization_id = ood.organization_id )
UNION
SELECT ''All'', ''All Organizations'' FROM DUAL','','XXWC All ORG LIST','XXWC All ORG LIST','SA059956',NULL,'N','','','','','','','US');
xxeis.eis_rsc_ins.lov( 401,'','''Show All'' ,''Show Only > 90 Days'',''Show Only > 180 Days''','EIS INV PUBD Days LOV','','SA059956',NULL,'N','','','N','','','','US');
null;
ELSE
RAISE_APPLICATION_ERROR (-20001,'Module does not exist. Import/Create the module with id 401');
END IF;
END;
/
prompt Creating Report Data for INV PUBD TRANSFER-WC
set scan off define off
DECLARE
mod_exist varchar2(1);
BEGIN 
mod_exist := XXEIS.EIS_RSC_INSERT.check_if_module_exist(401);
IF mod_exist = 'Y' THEN 
--Deleting Report data - INV PUBD TRANSFER-WC
xxeis.eis_rsc_utility.delete_report_rows( 'INV PUBD TRANSFER-WC',401 );
--Inserting Report - INV PUBD TRANSFER-WC
xxeis.eis_rsc_ins.r( 401,'INV PUBD TRANSFER-WC','','Create new EIS Report that displays the date of manual sub-inventory transfer to the PUBD subinventory','','','','SA059956','EIS_XXWC_INV_PUBD_TRANSFER_V','Y','','','SA059956','','N','Transaction Reports','','CSV,EXCEL,','N','','','','','','N','','US','','','','','','','','','','','','','','','','');
--Inserting Report Columns - INV PUBD TRANSFER-WC
xxeis.eis_rsc_ins.rc( 'INV PUBD TRANSFER-WC',401,'DAYS_IN_PUBD','Days In PUBD','Days In Pubd','','~~~','default','','7','N','Y','','','','','','','SA059956','N','N','','EIS_XXWC_INV_PUBD_TRANSFER_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'INV PUBD TRANSFER-WC',401,'DESCRIPTION','Description','Description','','','default','','3','N','Y','','','','','','','SA059956','N','N','','EIS_XXWC_INV_PUBD_TRANSFER_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'INV PUBD TRANSFER-WC',401,'ITEM_EXTENDED_COST','Item Extended Cost','Item Extended Cost','','~,~.~2','default','','11','N','Y','','','','','','','SA059956','N','N','','EIS_XXWC_INV_PUBD_TRANSFER_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'INV PUBD TRANSFER-WC',401,'ITEM_NUMBER','Item Number','Item Number','','','default','','2','N','Y','','','','','','','SA059956','N','N','','EIS_XXWC_INV_PUBD_TRANSFER_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'INV PUBD TRANSFER-WC',401,'ORG','Org','Org','','','default','','1','N','Y','','','','','','','SA059956','N','N','','EIS_XXWC_INV_PUBD_TRANSFER_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'INV PUBD TRANSFER-WC',401,'QUANTITY','Quantity(in PUBD)','Quantity','','~T~D~2','default','','4','N','Y','','','','','','','SA059956','N','N','','EIS_XXWC_INV_PUBD_TRANSFER_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'INV PUBD TRANSFER-WC',401,'SHELF_LIFE_DAYS','Shelf Life Days','Shelf Life Days','','~~~','default','','6','N','Y','','','','','','','SA059956','N','N','','EIS_XXWC_INV_PUBD_TRANSFER_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'INV PUBD TRANSFER-WC',401,'UOM','UOM','Uom','','','default','','5','N','Y','','','','','','','SA059956','N','N','','EIS_XXWC_INV_PUBD_TRANSFER_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'INV PUBD TRANSFER-WC',401,'ITEM_COST','Item Cost','Item Cost','','~,~.~2','default','','10','N','Y','','','','','','','SA059956','N','N','','EIS_XXWC_INV_PUBD_TRANSFER_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'INV PUBD TRANSFER-WC',401,'VENDOR_NAME','Vendor Name','Vendor Name','','','default','','12','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_INV_PUBD_TRANSFER_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'INV PUBD TRANSFER-WC',401,'BORN_ON_DATE','Born On Date','Born On Date','','','default','','9','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_INV_PUBD_TRANSFER_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'INV PUBD TRANSFER-WC',401,'DATE_MOVED_TO_PUBD','Date Moved To PUBD','Date Moved To Pubd','','','default','','8','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_INV_PUBD_TRANSFER_V','','','','US','','');
--Inserting Report Parameters - INV PUBD TRANSFER-WC
xxeis.eis_rsc_ins.rp( 'INV PUBD TRANSFER-WC',401,'Date','Date','','IN','EIS INV PUBD Days LOV','''Show All''','VARCHAR2','Y','Y','1','Y','N','CONSTANT','SA059956','Y','N','','','','EIS_XXWC_INV_PUBD_TRANSFER_V','','','US','');
xxeis.eis_rsc_ins.rp( 'INV PUBD TRANSFER-WC',401,'Org','Org','ORG','IN','XXWC All ORG LIST','''All''','VARCHAR2','Y','Y','2','Y','N','CONSTANT','SA059956','Y','N','','','','EIS_XXWC_INV_PUBD_TRANSFER_V','','','US','');
--Inserting Dependent Parameters - INV PUBD TRANSFER-WC
--Inserting Report Conditions - INV PUBD TRANSFER-WC
xxeis.eis_rsc_ins.rcnh( 'INV PUBD TRANSFER-WC',401,'Free Text ','FREE_TEXT','','','Y','','1');
xxeis.eis_rsc_ins.rcnd( '','','','','','','','','','','','','','','','','Y','','','','','and ((:Date= ''Show Only > 180 Days''
          AND  EXIPTV.DAYS_IN_PUBD > 180)
     or (:Date=''Show Only > 90 Days''
          AND  EXIPTV.DAYS_IN_PUBD > 90)
     or (:Date=''Show All'')
     )
AND ( ''All'' IN (:Org) OR (EXIPTV.ORG IN (:Org)))','1',401,'INV PUBD TRANSFER-WC','Free Text ');
--Inserting Report Sorts - INV PUBD TRANSFER-WC
xxeis.eis_rsc_ins.rs( 'INV PUBD TRANSFER-WC',401,'ITEM_NUMBER','ASC','SA059956','1','');
--Inserting Report Triggers - INV PUBD TRANSFER-WC
--inserting report templates - INV PUBD TRANSFER-WC
--Inserting Report Portals - INV PUBD TRANSFER-WC
--inserting report dashboards - INV PUBD TRANSFER-WC
--Exporting report views table information -  
xxeis.eis_rsc_ins.rviews( 'INV PUBD TRANSFER-WC','401','EIS_XXWC_INV_PUBD_TRANSFER_V','EIS_XXWC_INV_PUBD_TRANSFER_V','N','');
--inserting report security - INV PUBD TRANSFER-WC
xxeis.eis_rsc_ins.rsec( 'INV PUBD TRANSFER-WC','660','','ORDER_MGMT_SUPER_USER',401,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'INV PUBD TRANSFER-WC','660','','ONT_ICP_SUPER_USER',401,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'INV PUBD TRANSFER-WC','660','','XXWC_RENTAL_OM_PRICING_SUPER',401,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'INV PUBD TRANSFER-WC','660','','XXWC_ORDER_MGMT_SUPER_USER',401,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'INV PUBD TRANSFER-WC','660','','XXWC_ORDER_MGMT_READ_SHIPPING',401,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'INV PUBD TRANSFER-WC','660','','XXWC_ORDER_MGMT_READ_ONLY_COST',401,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'INV PUBD TRANSFER-WC','660','','XXWC_ORDER_MGMT_READ_ONLY',401,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'INV PUBD TRANSFER-WC','660','','XXWC_ORDER_MGMT_PRICING_SUPER',401,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'INV PUBD TRANSFER-WC','660','','XXWC_ORDER_MGMT_PRICING_STD',401,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'INV PUBD TRANSFER-WC','660','','XXWC_ORDER_MGMT_PRICING_LTD',401,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'INV PUBD TRANSFER-WC','660','','XXWC_ORDER_MGMT_PRICING_FULL',401,'SA059956','','','');
--Inserting Report Pivots - INV PUBD TRANSFER-WC
--Inserting Report Distribution Details 
--Inserting Report Distribution Details 
--Inserting Report SubTotals Details 
--Inserting Report aggregations 
--Inserting Report   Version details- INV PUBD TRANSFER-WC
xxeis.eis_rsc_ins.rv( 'INV PUBD TRANSFER-WC','','INV PUBD TRANSFER-WC','SA059956','24-JAN-2018');
ELSE
RAISE_APPLICATION_ERROR (-20001,'Module does not exist. Import/Create the module with id 401');
END IF;
END;
/
