/* Formatted on 12/19/2016 10:41:11 AM (QP5 v5.265.14096.38000) */
--
-- XXWC_ARS_OB_VW  (View)
--

 /********************************************************************************
  FILE NAME: xxwc_ars_ob_vw
  
  PROGRAM TYPE: view
  
  PURPOSE: ARS Outbound Interfaces view;
  ===============================================================================
  VERSION DATE          AUTHOR(S)       DESCRIPTION
  ------- -----------   --------------- -----------------------------------------
  1.0     07/16/2012   Luong Vu        Initial creation 
  1.1    12/19/2016    Neha Saini       TMS# 20160126-00238 New changes for ARS Extract
  1.2    02/12/2017    Sundaramoorthy   TMS# 20170419-00115 ARS - FW: Non-Printable Characters 
  ********************************************************************************/

CREATE OR REPLACE FORCE VIEW apps.xxwc_ars_ob_vw
(
   rec_line,
   org_id
)
AS
   SELECT (   REGEXP_REPLACE (
                 REGEXP_REPLACE (
                    REPLACE (REPLACE (a.account_name, ',', ' '), '"', ' '),
                    '[^[:alnum:]|[:blank:]|[:punct:]]+',
                    NULL),
                 '[^' || CHR (32) || '-' || CHR (127) || ']',
                 ' ')--ver1.1
           || ','
           || REGEXP_REPLACE (
                 REGEXP_REPLACE (
                    REPLACE (REPLACE (a.bill_addr, ',', ' '), '"', ' '),
                    '[^[:alnum:]|[:blank:]|[:punct:]]+',
                    NULL),
                 '[^' || CHR (32) || '-' || CHR (127) || ']',
                 ' ')                                               -- bill to  --ver1.1
           || ','
           || REPLACE (REPLACE (a.bill_city, ',', ' '), '"', ' ')
           || ','
           || REPLACE (REPLACE (a.bill_state, ',', ' '), '"', ' ')
           || ','
           || REPLACE (REPLACE (a.bill_postal_code, ',', ' '), '"', ' ')
           || ','
           || REGEXP_REPLACE (
                 REGEXP_REPLACE ( REPLACE (REPLACE (a.ship_addr, ',', ' '), '"', ' '),
                    '[^[:alnum:]|[:blank:]|[:punct:]]+',
                    NULL),
                 '[^' || CHR (32) || '-' || CHR (127) || ']',
                 ' ')                --  SHIP_TO  --ver1.1
           || ','
           || REPLACE (REPLACE (a.ship_city, ',', ' '), '"', ' ')
           || ','
           || REPLACE (REPLACE (a.ship_state, ',', ' '), '"', ' ')
           || ','
           || REPLACE (REPLACE (a.ship_postal_code, ',', ' '), '"', ' ')
           || ','
           || REGEXP_REPLACE (
                 REGEXP_REPLACE ( REPLACE (REPLACE (a.location, ',', ' '), '"', ' '),
                    '[^[:alnum:]|[:blank:]|[:punct:]]+',
                    NULL),
                 '[^' || CHR (32) || '-' || CHR (127) || ']',
                 ' ')                                                  --LOCATION Ver1.2
		   || ','
           || REPLACE (a.party_site_number, ',', ' ')
           || ','
           || REPLACE (a.balance, ',', ' ')
           || ','
           || REPLACE (a.amount_due_original, ',', ' ')
           || ','
           || REPLACE (REPLACE (a.trx_number, ',', ' '), '"', ' ')
           || ','
           || REPLACE (a.trx_date, ',', ' ')
           || ','
           || REPLACE (a.account_number, ',', ' ')
           || ','
           ||REGEXP_REPLACE (
                 REGEXP_REPLACE (  REPLACE (REPLACE (a.description, ',', ' '), '"', ' '),
                    '[^[:alnum:]|[:blank:]|[:punct:]]+',
                    NULL),
                 '[^' || CHR (32) || '-' || CHR (127) || ']',
                 ' ')    --ver1.1
           || ','
           || REGEXP_REPLACE (
                 REGEXP_REPLACE ( REPLACE (REPLACE (a.attribute1, ',', ' '), '"', ' '),
                    '[^[:alnum:]|[:blank:]|[:punct:]]+',
                    NULL),
                 '[^' || CHR (32) || '-' || CHR (127) || ']',
                 ' ')  --ver1.1  
           || ','
           || REPLACE (a.contact_id, ',', ' ')
           || ','
           || REGEXP_REPLACE (
                 REGEXP_REPLACE ( REPLACE (REPLACE (a.firstname, ',', ' '), '"', ' '),
                    '[^[:alnum:]|[:blank:]|[:punct:]]+',
                    NULL),
                 '[^' || CHR (32) || '-' || CHR (127) || ']',
                 ' ')         --FIRSTNAME Ver1.2
		   || ','
           || REGEXP_REPLACE (
                 REGEXP_REPLACE ( REPLACE (REPLACE (a.lastname, ',', ' '), '"', ' '),
                    '[^[:alnum:]|[:blank:]|[:punct:]]+',
                    NULL),
                 '[^' || CHR (32) || '-' || CHR (127) || ']',
                 ' ')         --LASTNAME Ver1.2
		   || ','
           || REGEXP_REPLACE (
                 REGEXP_REPLACE ( REPLACE (REPLACE (a.dear, ',', ' '), '"', ' '),
                    '[^[:alnum:]|[:blank:]|[:punct:]]+',
                    NULL),
                 '[^' || CHR (32) || '-' || CHR (127) || ']',
                 ' ')    --ver1.1
           || ','
           ||REGEXP_REPLACE (
                 REGEXP_REPLACE (  REPLACE (REPLACE (a.title, ',', ' '), '"', ' '),
                    '[^[:alnum:]|[:blank:]|[:punct:]]+',
                    NULL),
                 '[^' || CHR (32) || '-' || CHR (127) || ']',
                 ' ')   --ver1.1 
           || ','
           || REGEXP_REPLACE (
                 REGEXP_REPLACE (  REPLACE (REPLACE (a.fax, ',', ' '), '"', ' '),
                    '[^[:alnum:]|[:blank:]|[:punct:]]+',
                    NULL),
                 '[^' || CHR (32) || '-' || CHR (127) || ']',
                 ' ')   --FAX Ver1.2
		   || ','
           || REGEXP_REPLACE (
                 REGEXP_REPLACE (  REPLACE (REPLACE (a.phone, ',', ' '), '"', ' '),
                    '[^[:alnum:]|[:blank:]|[:punct:]]+',
                    NULL),
                 '[^' || CHR (32) || '-' || CHR (127) || ']',
                 ' ')  --Phone Ver1.2
		   || ','
           || REGEXP_REPLACE (
                 REGEXP_REPLACE (  REPLACE (REPLACE (a.hphone, ',', ' '), '"', ' '),
                    '[^[:alnum:]|[:blank:]|[:punct:]]+',
                    NULL),
                 '[^' || CHR (32) || '-' || CHR (127) || ']',
                 ' ')   --HPHONE ver1.2
		   || ','
           || REGEXP_REPLACE (
                 REGEXP_REPLACE ( REPLACE (REPLACE (a.address1, ',', ' '), '"', ' '),
                    '[^[:alnum:]|[:blank:]|[:punct:]]+',
                    NULL),
                 '[^' || CHR (32) || '-' || CHR (127) || ']',
                 ' ')    --ver1.1
           || ','
           ||REGEXP_REPLACE (
                 REGEXP_REPLACE (  REPLACE (REPLACE (a.address2, ',', ' '), '"', ' '),
                    '[^[:alnum:]|[:blank:]|[:punct:]]+',
                    NULL),
                 '[^' || CHR (32) || '-' || CHR (127) || ']',
                 ' ')   --ver1.1 
           || ','
           || REGEXP_REPLACE (
                 REGEXP_REPLACE ( REPLACE (REPLACE (a.address3, ',', ' '), '"', ' '),
                    '[^[:alnum:]|[:blank:]|[:punct:]]+',
                    NULL),
                 '[^' || CHR (32) || '-' || CHR (127) || ']',
                 ' ')  --ver1.1  
           || ','
           || REPLACE (REPLACE (a.city, ',', ' '), '"', ' ')
           || ','
           || REPLACE (REPLACE (a.state, ',', ' '), '"', ' ')
           || ','
           || REPLACE (a.zip, ',', ' ')
           || ','
           || REGEXP_REPLACE (
                 REGEXP_REPLACE (REPLACE (REPLACE (a.email, ',', ' '), '"', ' '),
                    '[^[:alnum:]|[:blank:]|[:punct:]]+',
                    NULL),
                 '[^' || CHR (32) || '-' || CHR (127) || ']',
                 ' ')  --ver1.1  
           || ','
           || REGEXP_REPLACE (
                 REGEXP_REPLACE ( REPLACE (REPLACE (a.gc_con_first_name, ',', ' '), '"', ' '),
                    '[^[:alnum:]|[:blank:]|[:punct:]]+',
                    NULL),
                 '[^' || CHR (32) || '-' || CHR (127) || ']',
                 ' ')     -- GENERAL CONTRACTOR CONTACT INFO --ver1.1
           || ','
           || REGEXP_REPLACE (
                 REGEXP_REPLACE ( REPLACE (REPLACE (a.gc_con_last_name, ',', ' '), '"', ' '),
                    '[^[:alnum:]|[:blank:]|[:punct:]]+',
                    NULL),
                 '[^' || CHR (32) || '-' || CHR (127) || ']',
                 ' ')    --ver1.1
           || ','
           || REGEXP_REPLACE (
                 REGEXP_REPLACE ( REPLACE (REPLACE (a.gc_email_address, ',', ' '), '"', ' '),
                    '[^[:alnum:]|[:blank:]|[:punct:]]+',
                    NULL),
                 '[^' || CHR (32) || '-' || CHR (127) || ']',
                 ' ')    --ver1.1
           || ','
           || REPLACE (a.gc_phone, ',', ' ')
           || ','
           || REPLACE (a.gc_fax, ',', ' ')
           || ','
           ||REGEXP_REPLACE (
                 REGEXP_REPLACE ( REPLACE (REPLACE (a.gc_address1, ',', ' '), '"', ' '),
                    '[^[:alnum:]|[:blank:]|[:punct:]]+',
                    NULL),
                 '[^' || CHR (32) || '-' || CHR (127) || ']',
                 ' ')   --ver1.1 
           || ','
           ||REGEXP_REPLACE (
                 REGEXP_REPLACE ( REPLACE (REPLACE (a.gc_address2, ',', ' '), '"', ' '),
                    '[^[:alnum:]|[:blank:]|[:punct:]]+',
                    NULL),
                 '[^' || CHR (32) || '-' || CHR (127) || ']',
                 ' ')    --ver1.1
           || ','
           || REPLACE (REPLACE (a.gc_city, ',', ' '), '"', ' ')
           || ','
           || REPLACE (REPLACE (a.gc_county, ',', ' '), '"', ' ')
           || ','
           || REPLACE (REPLACE (a.gc_state, ',', ' '), '"', ' ')
           || ','
           || REPLACE (a.gc_postal_code, ',', ' ')
           || ','
           || REPLACE (REPLACE (a.gc_resp_type, ',', ' '), '"', ' ')
           || ','
           || REGEXP_REPLACE (
                 REGEXP_REPLACE ( REPLACE (REPLACE (a.ownr_con_first_name, ',', ' '), '"', ' '),
                    '[^[:alnum:]|[:blank:]|[:punct:]]+',
                    NULL),
                 '[^' || CHR (32) || '-' || CHR (127) || ']',
                 ' ')    --ownr_con_first_name ver1.2
		   || ','
           || REGEXP_REPLACE (
                 REGEXP_REPLACE ( REPLACE (REPLACE (a.ownr_con_last_name, ',', ' '), '"', ' '),
                    '[^[:alnum:]|[:blank:]|[:punct:]]+',
                    NULL),
                 '[^' || CHR (32) || '-' || CHR (127) || ']',
                 ' ')    --ownr_con_last_name Ver1.2
		   || ','
           || REGEXP_REPLACE (
                 REGEXP_REPLACE (REPLACE (REPLACE (a.ownr_email_address, ',', ' '), '"', ' '),
                    '[^[:alnum:]|[:blank:]|[:punct:]]+',
                    NULL),
                 '[^' || CHR (32) || '-' || CHR (127) || ']',
                 ' ')   --ver1.1 
           || ','
           || REGEXP_REPLACE (
                 REGEXP_REPLACE (REPLACE (REPLACE (a.ownr_phone, ',', ' '), '"', ' '),
                    '[^[:alnum:]|[:blank:]|[:punct:]]+',
                    NULL),
                 '[^' || CHR (32) || '-' || CHR (127) || ']',
                 ' ')   --ownr_phone Ver1.2
		   || ','
           || REGEXP_REPLACE (
                 REGEXP_REPLACE (REPLACE (REPLACE (a.ownr_fax, ',', ' '), '"', ' '),
                    '[^[:alnum:]|[:blank:]|[:punct:]]+',
                    NULL),
                 '[^' || CHR (32) || '-' || CHR (127) || ']',
                 ' ')    --ownr_fax Ver1.2
		   || ','
           || REGEXP_REPLACE (
                 REGEXP_REPLACE (REPLACE (REPLACE (a.ownr_address1, ',', ' '), '"', ' '),
                    '[^[:alnum:]|[:blank:]|[:punct:]]+',
                    NULL),
                 '[^' || CHR (32) || '-' || CHR (127) || ']',
                 ' ')    --ver1.1
           || ','
           || REGEXP_REPLACE (
                 REGEXP_REPLACE (REPLACE (REPLACE (a.ownr_address2, ',', ' '), '"', ' '),
                    '[^[:alnum:]|[:blank:]|[:punct:]]+',
                    NULL),
                 '[^' || CHR (32) || '-' || CHR (127) || ']',
                 ' ')  --ver1.1  
           || ','
           || REPLACE (REPLACE (a.ownr_city, ',', ' '), '"', ' ')
           || ','
           || REPLACE (REPLACE (a.ownr_county, ',', ' '), '"', ' ')
           || ','
           || REPLACE (REPLACE (a.ownr_state, ',', ' '), '"', ' ')
           || ','
           || REPLACE (a.ownr_postal_code, ',', ' ')
           || ','
           || REPLACE (REPLACE (a.ownr_resp_type, ',', ' '), '"', ' ')
           || ','
           || REGEXP_REPLACE (
                 REGEXP_REPLACE (REPLACE (REPLACE (a.bc_con_first_name, ',', ' '), '"', ' '),
                    '[^[:alnum:]|[:blank:]|[:punct:]]+',
                    NULL),
                 '[^' || CHR (32) || '-' || CHR (127) || ']',
                 ' ')        --bc_con_first_name Ver1.2
		   || ','
           || REGEXP_REPLACE (
                 REGEXP_REPLACE (REPLACE (REPLACE (a.bc_con_last_name, ',', ' '), '"', ' '),
                    '[^[:alnum:]|[:blank:]|[:punct:]]+',
                    NULL),
                 '[^' || CHR (32) || '-' || CHR (127) || ']',
                 ' ')   --bc_con_last_name ver1.2
		   || ','
           || REGEXP_REPLACE (
                 REGEXP_REPLACE ( REPLACE (REPLACE (a.bc_email_address, ',', ' '), '"', ' '),
                    '[^[:alnum:]|[:blank:]|[:punct:]]+',
                    NULL),
                 '[^' || CHR (32) || '-' || CHR (127) || ']',
                 ' ')    --ver1.1
           || ','
           || REGEXP_REPLACE (
                 REGEXP_REPLACE ( REPLACE (REPLACE (a.bc_phone, ',', ' '), '"', ' '),
                    '[^[:alnum:]|[:blank:]|[:punct:]]+',
                    NULL),
                 '[^' || CHR (32) || '-' || CHR (127) || ']',
                 ' ')  --bc_phone Ver1.2
		   || ','
           || REGEXP_REPLACE (
                 REGEXP_REPLACE ( REPLACE (REPLACE (a.bc_fax, ',', ' '), '"', ' '),
                    '[^[:alnum:]|[:blank:]|[:punct:]]+',
                    NULL),
                 '[^' || CHR (32) || '-' || CHR (127) || ']',
                 ' ')   --bc_fax Ver1.2
		   || ','
           || REGEXP_REPLACE (
                 REGEXP_REPLACE (REPLACE (REPLACE (a.bc_address1, ',', ' '), '"', ' '),
                    '[^[:alnum:]|[:blank:]|[:punct:]]+',
                    NULL),
                 '[^' || CHR (32) || '-' || CHR (127) || ']',
                 ' ')   --ver1.1 
           || ','
           || REGEXP_REPLACE (
                 REGEXP_REPLACE (REPLACE (REPLACE (a.bc_address2, ',', ' '), '"', ' '),
                    '[^[:alnum:]|[:blank:]|[:punct:]]+',
                    NULL),
                 '[^' || CHR (32) || '-' || CHR (127) || ']',
                 ' ')   --ver1.1 
           || ','
           || REPLACE (REPLACE (a.bc_city, ',', ' '), '"', ' ')
           || ','
           || REPLACE (REPLACE (a.bc_county, ',', ' '), '"', ' ')
           || ','
           || REPLACE (REPLACE (a.bc_state, ',', ' '), '"', ' ')
           || ','
           || REPLACE (a.bc_postal_code, ',', ' ')
           || ','
           || REPLACE (REPLACE (a.bc_resp_type, ',', ' '), '"', ' ')
           || ','
           || REGEXP_REPLACE (
                 REGEXP_REPLACE (REPLACE (REPLACE (a.fi_con_first_name, ',', ' '), '"', ' '),
                    '[^[:alnum:]|[:blank:]|[:punct:]]+',
                    NULL),
                 '[^' || CHR (32) || '-' || CHR (127) || ']',
                 ' ')  --fi_con_first_name Ver1.2
		   || ','
           || REGEXP_REPLACE (
                 REGEXP_REPLACE (REPLACE (REPLACE (a.fi_con_last_name, ',', ' '), '"', ' '),
                    '[^[:alnum:]|[:blank:]|[:punct:]]+',
                    NULL),
                 '[^' || CHR (32) || '-' || CHR (127) || ']',
                 ' ')  --fi_con_last_name Ver1.2
		   || ','
           || REGEXP_REPLACE (
                 REGEXP_REPLACE (REPLACE (REPLACE (a.fi_email_address, ',', ' '), '"', ' '),
                    '[^[:alnum:]|[:blank:]|[:punct:]]+',
                    NULL),
                 '[^' || CHR (32) || '-' || CHR (127) || ']',
                 ' ')    --ver1.1
           || ','
           || REGEXP_REPLACE (
                 REGEXP_REPLACE (REPLACE (REPLACE (a.fi_phone, ',', ' '), '"', ' '),
                    '[^[:alnum:]|[:blank:]|[:punct:]]+',
                    NULL),
                 '[^' || CHR (32) || '-' || CHR (127) || ']',
                 ' ')   --fi_phone Ver1.2
		   || ','
           || REGEXP_REPLACE (
                 REGEXP_REPLACE (REPLACE (REPLACE (a.fi_fax, ',', ' '), '"', ' '),
                    '[^[:alnum:]|[:blank:]|[:punct:]]+',
                    NULL),
                 '[^' || CHR (32) || '-' || CHR (127) || ']',
                 ' ')   --fi_fax Ver1.2
		   || ','
           || REGEXP_REPLACE (
                 REGEXP_REPLACE (REPLACE (REPLACE (a.fi_address1, ',', ' '), '"', ' '),
                    '[^[:alnum:]|[:blank:]|[:punct:]]+',
                    NULL),
                 '[^' || CHR (32) || '-' || CHR (127) || ']',
                 ' ')  --ver1.1  
           || ','
           ||REGEXP_REPLACE (
                 REGEXP_REPLACE ( REPLACE (REPLACE (a.fi_address2, ',', ' '), '"', ' '),
                    '[^[:alnum:]|[:blank:]|[:punct:]]+',
                    NULL),
                 '[^' || CHR (32) || '-' || CHR (127) || ']',
                 ' ') --ver1.1   
           || ','
           || REPLACE (REPLACE (a.fi_city, ',', ' '), '"', ' ')
           || ','
           || REPLACE (REPLACE (a.fi_county, ',', ' '), '"', ' ')
           || ','
           || REPLACE (REPLACE (a.fi_state, ',', ' '), '"', ' ')
           || ','
           || REPLACE (a.fi_postal_code, ',', ' ')
           || ','
           || REPLACE (REPLACE (a.fi_resp_type, ',', ' '), '"', ' ')
           || ','
           || a.account_status)
             rec_line                                                 --ver1.1
                     ,
          a.org_id
     FROM apps.xxwc_ars_ob_tbl a;