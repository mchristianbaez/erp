CREATE OR REPLACE FORCE VIEW APPS.XXWC_INV_ITEM_SEARCH_LITE_V
   /*************************************************************************
   *   $Header XXWC_INV_ITEM_SEARCH_LITE_V.sql $
   *   Module Name: xxwc Advance Item Search Lite Form
   *
   *   PURPOSE:   View create script for XXWC Advanced Item Search Lite Form
   *
   *   REVISIONS:
   *   Ver        Date        Author                     Description
   *   ---------  ----------  ---------------         -------------------------
   *   1.0        10-Mar-2015 Shankar Hariharan       Initial Version 20150302-00435
   *   1.1        30-Oct-2015 Pattabhi Avula          TMS 20150930-00197 Update AIS to 
   *                                                  filter our Inactivated specials   
   * ***************************************************************************/
(
   INVENTORY_ITEM_ID,
   ORGANIZATION_ID,
   SEGMENT1,
   DESCRIPTION,
   ON_HAND_QTY,
   CATEGORY_DESCRIPTION,
   SALES_VELOCITY,
--   COUNTER_ORDER_QTY,
   ITEM_TYPE,
   PRIMARY_UOM
)
AS
   SELECT a.inventory_item_id,
          a.organization_id,
          UPPER (a.segment1) segment1,
          UPPER (a.description) description,
          xxwc_ascp_scwb_pkg.get_on_hand (a.inventory_item_id, a.organization_id, 'G')  on_hand_qty,
          UPPER (e.description) category_description ,
          xxwc_mv_routines_pkg.get_item_category (a.inventory_item_id,
                                                  a.organization_id,
                                                  'Sales Velocity',
                                                  1)
             sales_velocity ,
             /*
          (SELECT NVL (
                     SUM (
                          l.ordered_quantity
                        * po_uom_s.po_uom_convert (um.unit_of_measure,
                                                   a.primary_unit_of_measure,
                                                   l.inventory_item_id)),
                     0)                                    -- Shankar TMS 20130124-00974 23-Jan-2013
             FROM apps.oe_order_lines l, apps.oe_order_headers h, mtl_units_of_measure_vl um
            WHERE     l.header_id = h.header_id
                  AND h.flow_status_code NOT IN ('ENTERED', 'CLOSED', 'CANCELLED')
                  AND l.line_type_id = 1005
                  AND l.ship_from_org_id = a.organization_id
                  AND l.inventory_item_id = a.inventory_item_id
                  AND l.order_quantity_uom = um.uom_code)
             counter_order_qty  ,*/
          a.item_type,
          a.primary_uom_code
     FROM mtl_system_items_b a,
          mtl_parameters b,
          mtl_item_categories d,
          mtl_categories e
    WHERE     a.organization_id = b.organization_id
          AND b.master_organization_id = fnd_profile.VALUE ('XXWC_ITEM_MASTER_ORG')
          AND a.inventory_item_id = d.inventory_item_id
          AND a.organization_id = d.organization_id
          AND d.category_id = e.category_id
          AND E.STRUCTURE_ID = 101
		  -- Added below condition for Version 1.1
		  AND a.inventory_item_status_code!='Inactive'
/