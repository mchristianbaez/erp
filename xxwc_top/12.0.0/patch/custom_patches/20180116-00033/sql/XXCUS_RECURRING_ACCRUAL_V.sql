/*
   Version Ticket              Date       Author                                     Notes
   ======  =================   ========== =========================================  ===================================================
   1.0    TMS 20180116-00033   02/03/2018 Balaguru Seshadri                          HDSupply Rebates Recurring Accruals mostly for COOP offers
*/
CREATE OR REPLACE VIEW XXCUS.XXCUS_RECURRING_ACCRUAL_V AS
SELECT 
     A.RUN_ID
    ,A.RECORD_SEQ
    ,A.VALIDATED_FLAG
    ,B.API_STATUS
    ,B.LINE_NUM
    ,A.CUST_ACCOUNT_ID
    ,A.MVID
    ,A.MVID_NAME
    ,A.PLAN_ID
    ,A.OFFER_NAME
    ,A.CURRENCY
    ,A.AMOUNT TOTAL_ACCRUAL_AMOUNT
    ,A.TOTAL_PURCHASES
    ,B.PURCHASE_PERCENT
    ,B.RECURRING_ACCRUAL_AMOUNT LINE_ACCRUAL_AMOUNT
    ,B.UTILIZATION_ID
    ,A.REASON_CODE
    ,A.REASON_NAME
    ,A.LOB_PARTY_ID
    ,A.LOB_NAME
    ,A.BU_PARTY_ID
    ,A.BU_NAME
    ,B.BRANCH_CUST_ACCT_ID
    ,B.BRANCH_NAME
    ,A.RECEIPT_START_DATE
    ,A.RECEIPT_END_DATE
    ,A.STATUS_CODE HEADER_STATUS_CODE
    ,A.STATUS_DESC HEADER_STATUS_DESC	
    ,B.STATUS_CODE LINE_STATUS_CODE
    ,B.STATUS_DESC LINE_STATUS_DESC
    ,A.FUND_ID
    ,A.APEX_CREATED_BY
    ,A.APEX_CREATION_DATE
    ,A.EBS_UPDATED_BY
    ,A.EBS_UPDATE_DATE
FROM
    XXCUS.XXCUS_OZF_RECURRING_ACCRUALS A
   ,XXCUS.XXCUS_OZF_RECURR_ACCR_UTILS B
WHERE 1 =1
      AND A.RUN_ID =B.RUN_ID
      AND A.RECORD_SEQ =B.RECORD_SEQ
;
--
COMMENT ON TABLE XXCUS.XXCUS_RECURRING_ACCRUAL_V IS 'TMS 20180116-00033';
--