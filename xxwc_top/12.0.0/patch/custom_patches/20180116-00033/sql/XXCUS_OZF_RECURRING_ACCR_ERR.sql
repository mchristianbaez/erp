/*
   Version Ticket                                      Date                  Author                                                                                     Notes
   ======  ============                     ===========    =========================================       ===================================================
   1.0         TMS 20180116-00033    02/03/2018     Balaguru Seshadri                                                                HDSupply Rebates Recurring Accruals mostly for COOP offers
*/
--
DROP TABLE XXCUS.XXCUS_OZF_RECURRING_ACCR_ERR;
--
CREATE TABLE XXCUS.XXCUS_OZF_RECURRING_ACCR_ERR
(
  RECORD_SEQ     NUMBER,
  ERR_SEQ        NUMBER,
  ERR_MESSAGE    VARCHAR2(4000 BYTE),
  LINE_NUM       NUMBER,
  RUN_ID         NUMBER,
  CREATED_BY     NUMBER,
  CREATION_DATE  DATE                           DEFAULT sysdate
)
;
--
COMMENT ON TABLE XXCUS.XXCUS_OZF_RECURRING_ACCR_ERR IS 'TMS ????';
--
