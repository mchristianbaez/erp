/*
   Version Ticket                                      Date                  Author                                                                                     Notes
   ======  ============                     ===========    =========================================       ===================================================
   1.0         TMS 20180116-00033    02/03/2018     Balaguru Seshadri                                                                HDSupply Rebates Recurring Accruals mostly for COOP offers
*/
--
DROP TABLE XXCUS.XXCUS_OZF_RECURR_ACCR_UTILS;
--
CREATE TABLE XXCUS.XXCUS_OZF_RECURR_ACCR_UTILS
(
  RECORD_SEQ                NUMBER,
  LINE_NUM                  NUMBER,
  CUST_ACCOUNT_ID           NUMBER,
  PLAN_ID                   NUMBER,
  BRANCH_NAME               VARCHAR2(150 BYTE),
  PURCHASE_PERCENT          NUMBER,
  RECURRING_ACCRUAL_AMOUNT  NUMBER,
  STATUS_CODE               VARCHAR2(150 BYTE)  DEFAULT 'NEW'                 NOT NULL,
  STATUS_DESC               VARCHAR2(240 BYTE),
  UTILIZATION_ID            NUMBER              DEFAULT 0,
  EBS_UPDATED_BY            NUMBER,
  EBS_UPDATE_DATE           DATE,
  BRANCH_CUST_ACCT_ID       NUMBER              DEFAULT 0,
  RUN_ID                    NUMBER              DEFAULT 0,
  API_STATUS                VARCHAR2(1 BYTE)
)
;
--
comment on table xxcus.xxcus_ozf_recurr_accr_utils is 'TMS 20180116-00033';
--
