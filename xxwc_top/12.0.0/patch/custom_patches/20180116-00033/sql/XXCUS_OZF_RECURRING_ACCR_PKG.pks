/*
   Version Ticket              Date       Author                                     Notes
   ======  =================   ========== =========================================  ===================================================
   1.0    TMS 20180116-00033   02/03/2018 Balaguru Seshadri                          HDSupply Rebates Recurring Accruals mostly for COOP offers
*/
CREATE OR REPLACE package APPS.xxcus_ozf_recurring_accr_pkg is
 --
 procedure Validate (p_ok OUT boolean, p_rec_seq IN number);
 --
 procedure Main
   ( 
      errbuff         out      varchar2,
      retcode         out      number,
      p_record_seq in number default null,
      p_debug in varchar2 default 'N'      
   );
 --
end xxcus_ozf_recurring_accr_pkg;
/