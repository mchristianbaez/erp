/*
   Version Ticket                      Date                                  Author                                                                                     Notes
   ======  ============     ===========                   =========================================       ===================================================
   1.0         TMS 20180116-00033    02/03/2018     Balaguru Seshadri                                                                HDSupply Rebates Recurring Accruals mostly for COOP offers
*/
--
DROP TABLE XXCUS.XXCUS_OZF_RECURRING_ACCRUALS;
--
CREATE TABLE XXCUS.XXCUS_OZF_RECURRING_ACCRUALS
(
  MVID                 VARCHAR2(30 BYTE)        NOT NULL,
  MVID_NAME            VARCHAR2(360 BYTE)       NOT NULL,
  OFFER_NAME           VARCHAR2(255 BYTE)       NOT NULL,
  CURRENCY             VARCHAR2(3 BYTE)         NOT NULL,
  REASON_NAME          VARCHAR2(150 BYTE)       NOT NULL,
  AMOUNT               NUMBER                   NOT NULL,
  LOB_NAME             VARCHAR2(360 BYTE),
  BU_NAME              VARCHAR2(360 BYTE)       NOT NULL,
  BRANCH_NAME          VARCHAR2(150 BYTE),
  RECEIPT_START_DATE   DATE                     NOT NULL,
  RECEIPT_END_DATE     DATE                     NOT NULL,
  VALIDATED_FLAG       VARCHAR2(1 BYTE)         DEFAULT 'N'                   NOT NULL,
  STATUS_CODE          VARCHAR2(240 BYTE)       DEFAULT 'NEW'                 NOT NULL,
  CUST_ACCOUNT_ID      NUMBER,
  PLAN_ID              NUMBER,
  REASON_CODE          VARCHAR2(30 BYTE),
  APEX_CREATED_BY      VARCHAR2(30 BYTE),
  APEX_CREATION_DATE   DATE                     DEFAULT SYSDATE,
  EBS_UPDATED_BY       NUMBER,
  EBS_UPDATE_DATE      DATE,
  RECORD_SEQ           NUMBER,
  TOTAL_PURCHASES      NUMBER                   DEFAULT 0,
  LOB_PARTY_ID         NUMBER                   DEFAULT 0,
  BU_PARTY_ID          NUMBER                   DEFAULT 0,
  BRANCH_CUST_ACCT_ID  NUMBER                   DEFAULT 0,
  RUN_ID               NUMBER                   DEFAULT 0,
  FUND_ID              NUMBER                   DEFAULT 0,
  STATUS_DESC          VARCHAR2(240 BYTE)
)
;
--
GRANT INSERT,SELECT ON XXCUS.XXCUS_OZF_RECURRING_ACCRUALS TO KP059700, INTERFACE_APEXHDSORACLE ;
--
COMMENT ON TABLE XXCUS.XXCUS_OZF_RECURRING_ACCRUALS IS 'TMS 20180116-00033';
--
