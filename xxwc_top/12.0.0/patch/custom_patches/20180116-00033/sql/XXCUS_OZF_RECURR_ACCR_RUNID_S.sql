/*
   Version Ticket                                      Date                  Author                                                                                     Notes
   ======  ============                     ===========    =========================================       ===================================================
   1.0         TMS 20180116-00033    02/03/2018     Balaguru Seshadri                                                                HDSupply Rebates Recurring Accruals mostly for COOP offers
*/
--
DROP SEQUENCE XXCUS.XXCUS_OZF_RECURR_ACCR_RUNID_S;
--
CREATE SEQUENCE XXCUS.XXCUS_OZF_RECURR_ACCR_RUNID_S START WITH 100 INCREMENT BY 1;
--

