/*
   Version Ticket              Date       Author                                     Notes
   ======  =================   ========== =========================================  ===================================================
   1.0    TMS 20180116-00033   02/03/2018 Balaguru Seshadri                          HDSupply Rebates Recurring Accruals mostly for COOP offers
*/
CREATE OR REPLACE package body APPS.xxcus_ozf_recurring_accr_pkg is
  --
  g_run_id Number :=0;
  g_debug varchar2(1) :=Null;
  --
  g_validated_No varchar2(1) :='N';
  g_validated_Yes  varchar2(1) :='Y'; 
  g_Zero Number :=0;
  --
  g_TRUE boolean :=TRUE;
  g_FALSE boolean :=FALSE;
  g_err_msg varchar2(2000) :=Null;
  --
  g_status_New xxcus.xxcus_ozf_recurring_accruals.status_code%type :='NEW';
  g_status_Ok    xxcus.xxcus_ozf_recurring_accruals.status_code%type :='COMPLETE';
  g_status_Err   xxcus.xxcus_ozf_recurring_accruals.status_code%type :='ERROR'; 
  --
  g_stage1 varchar2(240) :='EBS: Stage1 : Picked up for processing';
  g_stage2 varchar2(240) :='EBS: Stage2 : Total purchases fetch complete.';  
  g_stage3 varchar2(240) :='EBS: Stage3 : Branch level allocated accruals fetch complete.';
  g_stage40 varchar2(240) :='EBS: Stage4 : Recurring accrual created.';
  g_stage41 varchar2(240) :='EBS: Stage4 : Recurring accrual failed.';
  --
  type g_hdr_tbl is table of xxcus.xxcus_ozf_recurring_accruals%rowtype index by pls_integer;
  g_hdr_rec    g_hdr_tbl;
  --
  type g_line_util_tbl is table of xxcus.xxcus_ozf_recurr_accr_utils%rowtype index by pls_integer;
  g_line_rec    g_line_util_tbl;
  --  
  g_err_Email1 varchar2(200) := 'HDSOracleDevelopers@hdsupply.com';
  g_err_Email2 varchar2(200) :='HDSRebateManagementSystemNotifications@hdsupply.com'; --1.8
  --
  procedure print_log(p_message in varchar2) is
  begin
   if g_debug =g_validated_Yes then
       if fnd_global.conc_request_id >0 then
        fnd_file.put_line(fnd_file.log, p_message);
       else
        dbms_output.put_line(p_message);
       end if;   
   else 
     Null;
   end if;
  exception
   when others then
    dbms_output.put_line( 'Issue in xxcus_ozf_recurring_accr_pkg.print_log routine ='||sqlerrm);
  end print_log;
  --
  procedure update_header_status
      (
          p_rec_seq in number
         ,p_status_code in varchar2
         ,p_status_desc in varchar2
         ,p_ebs_updated_by in number   
      ) is  
   --PRAGMA AUTONOMOUS_TRANSACTION;
  begin
        --
         update xxcus.xxcus_ozf_recurring_accruals
         set
                ebs_update_date =sysdate
               ,status_code = case when p_status_code is not null then p_status_code else status_code end 
               ,status_desc = case when p_status_desc is not null then p_status_desc else status_desc end               
               ,ebs_updated_by = case when p_ebs_updated_by is not null then p_ebs_updated_by else ebs_updated_by end       
         where 1 =1
               and record_seq =p_rec_seq
         ;        
         --
         commit;
         --  
  exception
   when others then
    dbms_output.put_line( 'Issue in xxcus_ozf_recurring_accr_pkg.update_header_status, routine ='||sqlerrm);
  end update_header_status;
  --  
  procedure update_alloc_util_status 
      (
          p_rec_seq in number
         ,p_line_num in number
         ,p_status_code in varchar2
         ,p_status_desc in varchar2
         ,p_ebs_updated_by in number
         ,p_api_status in varchar2
         ,p_util_id in number  
      ) is
   --PRAGMA AUTONOMOUS_TRANSACTION;
  begin
        --
         update xxcus.xxcus_ozf_recurr_accr_utils
         set
                ebs_update_date =sysdate
               ,status_code = case when p_status_code is not null then p_status_code else status_code end 
               ,status_desc = case when p_status_desc is not null then p_status_desc else status_desc end               
               ,ebs_updated_by = case when p_ebs_updated_by is not null then p_ebs_updated_by else ebs_updated_by end       
               ,api_status = case when p_api_status is not null then p_api_status else api_status end               
               ,utilization_id = case when p_util_id is not null then p_util_id else utilization_id end               
         where 1 =1
               and record_seq =p_rec_seq
               and line_num =p_line_num
         ;        
         --
         commit;
         --      
  exception
   when others then
    dbms_output.put_line( 'Issue in xxcus_ozf_recurring_accr_pkg.update_alloc_util_status, routine ='||sqlerrm);
  end update_alloc_util_status;
  -- 
  procedure log_error (p_error_rec in xxcus.xxcus_ozf_recurring_accr_err%rowtype) is
   PRAGMA AUTONOMOUS_TRANSACTION;
  begin
        insert into xxcus.xxcus_ozf_recurring_accr_err
         (
           record_seq
           ,line_num
          ,err_seq
          ,err_message
          ,run_id
          ,created_by
         )
        values
         (
           p_error_rec.record_seq
          ,p_error_rec.line_num
          ,p_error_rec.err_seq
          ,p_error_rec.err_message
          ,p_error_rec.run_id
          ,p_error_rec.created_by
         )
        ;
         --
         commit;
         --        
  exception
   when others then
    dbms_output.put_line( 'Issue in xxcus_ozf_recurring_accr_pkg.log_error, routine ='||sqlerrm);
  end log_error;
  --     
  function get_offer_fund_id (p_offer_id in number, p_offer_name in varchar2, p_rec_seq in number) return number is
    l_fund_id number :=0;
  begin  
         --
       select budget_source_id
       into     l_fund_id
       from  ozf_offers
       where 1 =1
            and offer_id =p_offer_id;   
        --
        return l_fund_id;
        --
  exception
   when no_data_found then
    print_log('Recurring accrual record seq : '||p_rec_seq||', failed to get fund id from function get_offer_fund_id for offer id =>'||p_offer_id||', name ='||p_offer_name);   
    return l_fund_id;
   when others then
    print_log( 'Issue in function get_offer_fund_id : msg =>'||sqlerrm);
    return l_fund_id;
  end get_offer_fund_id;
  --  
 procedure Validate (p_ok OUT boolean, p_rec_seq IN number) is
  --
  cursor c_new is
  select *
  from   xxcus.xxcus_ozf_recurring_accruals
  where 1 =1
      and  validated_flag =g_validated_No
      and  status_code =g_status_New      
      and  p_rec_seq is not null
      and  record_seq =p_rec_seq
   union all
  select *
  from   xxcus.xxcus_ozf_recurring_accruals
  where 1 =1
      and  validated_flag =g_validated_No
      and  status_code =g_status_New
      and  p_rec_seq is null    
  ; 
  --
  type c_new_type is table of c_new%rowtype index by binary_integer;
  c_new_recs c_new_type;
  --
  l_user_id number :=fnd_global.user_id;
  l_bu_rec xxcus.xxcus_pam_bu_name_tbl%rowtype :=Null;
  --
  l_customer_id    xxcus.xxcus_ozf_recurring_accruals.cust_account_id%type :=0;
  l_plan_id              xxcus.xxcus_ozf_recurring_accruals.plan_id%type :=0;
  l_reason_code   xxcus.xxcus_ozf_recurring_accruals.reason_code%type :=Null;
  --
  l_mvid_ok BOOLEAN :=Null;
  l_offer_ok BOOLEAN :=Null;
  l_currency_ok BOOLEAN :=Null;
  --
  l_prereq_status  xxcus.xxcus_ozf_recurring_accruals.status_code%type :=Null;
  --
  l_reason_ok BOOLEAN :=Null;
  l_date_ok BOOLEAN :=Null;        
  l_BU_ok BOOLEAN :=Null;  
  --
  l_seq number :=0;
  l_currency_code varchar2(10) :=Null;
  --
  l_USD  varchar2(10) :='USD';   --US currency code
  l_CAD  varchar2(10) :='CAD';   --Canadian currency code
  --
  l_from_date date :=Null;
  l_to_date date :=Null;  
  --
 begin
      --
      begin 
        --
        open c_new;
        fetch c_new bulk collect into c_new_recs;
        close c_new;
        --
        if (c_new_recs.count >g_Zero) then
             --
             print_log( 'Check pre requisites and update critical information, found new records, total : '||c_new_recs.count);
             --  
             for idx1 in 1 .. c_new_recs.count loop  
                  --
                  l_seq :=idx1;
                  --
                  begin 
                        --
                        savepoint sqr1; -- 02/02/2018
                        --
                        -- Check if mvid is correct
                        --
                        begin
                                select customer_id 
                                into    l_customer_id
                                from xxcus.xxcus_rebate_customers
                                where 1 =1
                                    and  customer_attribute1 ='HDS_MVID'
                                    and  customer_attribute2 =c_new_recs(idx1).mvid
                                ;
                                --
                                update xxcus.xxcus_ozf_recurring_accruals a 
                                set a.cust_account_id =l_customer_id
                                where 1 =1
                                and a.record_seq =c_new_recs(idx1).record_seq   
                                and l_customer_id >0
                                ; 
                            --
                            if (sql%rowcount >0) then 
                                --
                                l_prereq_status :=substr('MVID valid ', 1, 240);
                                l_mvid_ok :=TRUE;
                                print_log('l_prereq_status : '||l_prereq_status);
                                --                        
                            else
                                --
                                l_prereq_status :=substr('MVID invalid ', 1, 240);
                                l_mvid_ok :=FALSE;                            
                                --
                            end if;
                            --
                        exception
                         when no_data_found then
                            --
                            l_prereq_status :=substr('MVID check : Invalid MVID ', 1, 240);
                            l_mvid_ok :=FALSE;                            
                            --                                   
                         when too_many_rows then  
                            --
                            l_prereq_status :=substr('MVID check : More than 1 MVID ', 1, 240);
                            l_mvid_ok :=FALSE;                            
                            --                                                         
                         when others then
                            --
                            l_prereq_status :=substr('MVID check : Misc error ', 1, 240);
                            l_mvid_ok :=FALSE;
                            --                                                             
                        end;
                        --      
                        --print_log('@ MVID check, l_prereq_status : '||l_prereq_status);                                    
                        --
                        -- Check if offer name is correct
                        --
                        begin
                                select plan_id 
                                into    l_plan_id
                                from xxcus.xxcus_pam_program_list_tbl
                                where 1 =1
                                and agreement_name =c_new_recs(idx1).offer_name
                                and cust_id                     =l_customer_id
                                ;
                                --
                                update xxcus.xxcus_ozf_recurring_accruals a 
                                set a.plan_id =l_plan_id
                                where 1 =1
                                     and a.record_seq =c_new_recs(idx1).record_seq  
                                     and l_plan_id >0
                                ;
                                --
                                if (sql%rowcount >0) then
                                    --
                                    l_prereq_status :=substr(l_prereq_status||', Offer valid ', 1, 240);
                                    l_offer_ok :=TRUE;
                                    --                        
                                else
                                    --
                                    l_prereq_status :=substr(l_prereq_status||', Offer invalid ', 1, 240);
                                    l_offer_ok :=FALSE;
                                    --                         
                                end if;
                                --
                        exception
                         when no_data_found then
                            --
                            l_prereq_status :=substr('Offer check : Invalid ', 1, 240);
                            l_offer_ok :=FALSE;                            
                            --                                   
                         when too_many_rows then  
                            --
                            l_prereq_status :=substr('Offer check : More than 1 Offer ', 1, 240);
                            l_offer_ok :=FALSE;                            
                            --                                                         
                         when others then
                            --
                            l_prereq_status :=substr('Offer check : Misc error ', 1, 240);
                            l_offer_ok :=FALSE;
                            --                                                             
                        end;
                        --
                        --print_log('@ Offer check, l_prereq_status : '||l_prereq_status);                                    
                        --                        
                        -- Check if currency code is correct
                        --
                        begin       
                            --
                            if (c_new_recs(idx1).currency IN (l_USD, l_CAD)) then
                                --
                                l_prereq_status :=substr(l_prereq_status||', Currency valid, ', 1, 240);
                                l_currency_ok :=TRUE;
                                --                        
                            else
                                --
                                l_prereq_status :=substr(l_prereq_status||', Currency invalid, ', 1, 240);
                                l_currency_ok :=FALSE;                            
                                --                         
                            end if;                     
                            --                                              
                        exception
                         when others then
                            --
                            l_prereq_status :=substr(l_prereq_status||'Currency : Misc error ', 1, 240);
                            l_currency_ok :=FALSE;
                            --
                        end;
                        --
                        --print_log('@ Currency check, l_prereq_status : '||l_prereq_status);                                    
                        --                        
                        -- Check if reason name is correct
                        --
                        begin
                                select lookup_code
                                into    l_reason_code
                                from  fnd_lookup_values_vl
                                where 1 =1
                                and lookup_type ='XXCUS_REB_ADJ_REASON'
                                and meaning =c_new_recs(idx1).reason_name
                                ;
                                --
                                update xxcus.xxcus_ozf_recurring_accruals a 
                                set a.reason_code =l_reason_code
                                where 1 =1
                                     and a.record_seq =c_new_recs(idx1).record_seq   
                                     and l_reason_code is not null
                                ;
                                --
                                if (sql%rowcount >0) then
                                    --
                                    l_prereq_status :=substr(l_prereq_status||', Reason valid ', 1, 240);
                                    l_reason_ok :=TRUE;
                                    --                        
                                else
                                    --
                                    l_prereq_status :=substr(l_prereq_status||', Reason invalid ', 1, 240);
                                    l_reason_ok :=FALSE;
                                    --                  
                                end if;
                                --
                        exception
                         when no_data_found then
                            --
                            l_prereq_status :=substr(l_prereq_status||'Reason name check : Invalid ', 1, 240);
                            l_reason_ok :=FALSE;                            
                            --                                   
                         when too_many_rows then
                            --
                            l_prereq_status :=substr(l_prereq_status||'Reason name check : More than 1 reason ', 1, 240);
                            l_reason_ok :=FALSE;                            
                            --                                                         
                         when others then
                            --
                            l_prereq_status :=substr(l_prereq_status||'Reason name check : Misc error ', 1, 240);
                            l_reason_ok :=FALSE;
                            --                                                             
                        end;
                        --
                        --print_log('@ reason check, l_prereq_status : '||l_prereq_status);                    
                        --
                        -- Check if receipt end date is less than receipt start date
                        --
                        if ( c_new_recs(idx1).receipt_end_date >= c_new_recs(idx1).receipt_start_date ) then
                            --
                            l_prereq_status :=substr(l_prereq_status||', End date check : Ok, ', 1, 240);
                            l_date_ok :=TRUE;
                            --                        
                        else
                            --
                            l_prereq_status :=substr(l_prereq_status||', End date check : Not ok, ', 1, 240);
                            l_date_ok :=FALSE;                            
                            --                         
                        end if;                     
                        --     
                        --print_log('@ end date check, l_prereq_status : '||l_prereq_status);                                    
                        --                        
                        -- Check if BU name is correct
                        --
                        begin
                            select *
                            into     l_bu_rec
                            from   xxcus.xxcus_pam_bu_name_tbl
                            where 1 =1
                                 and bu_name =c_new_recs(idx1).bu_name
                            ;  
                            --
                            l_prereq_status :=substr(l_prereq_status||', BU valid ', 1, 240);
                            l_BU_ok :=TRUE;
                            --
                        exception
                         when no_data_found then
                            --
                            l_prereq_status :=substr('BU check : Invalid ', 1, 240);
                            l_BU_ok :=FALSE;                            
                            --                                   
                         when too_many_rows then  
                            --
                            l_prereq_status :=substr('BU check : More than 1 BU ', 1, 240);
                            l_BU_ok :=FALSE;                            
                            --                                                         
                         when others then
                            --
                            l_prereq_status :=substr('BU check : Misc error ', 1, 240);
                            l_BU_ok :=FALSE;
                            --                                                             
                        end;                  
                        --                   
                        --   
                        print_log('Prereq Status : '||l_prereq_status);                                    
                        --                        
                        --
                        -- if all good update validated_flag with Y else leave it as it is because the default value is always N
                        if 
                           (
                                      (l_BU_ok) -- BU Ok
                             AND (l_date_ok) -- Dates Ok 
                             AND (l_reason_ok) -- Reason Ok                             
                             AND (l_currency_ok) -- Currency Ok                             
                             AND (l_offer_ok) -- Offer Ok                             
                             AND (l_mvid_ok) -- MVID Ok                             
                           ) then 
                            --
                             begin
                               savepoint precheck;
                               update xxcus.xxcus_ozf_recurring_accruals a
                                    set status_desc =l_prereq_status -- status_code =l_prereq_status IMPORTANT 
                                          ,status_code ='STAGE_1'
                                          ,validated_flag =g_validated_Yes
                                          ,lob_party_id =l_bu_rec.lob_id
                                          ,bu_party_id =l_bu_rec.bu_id
                                          ,lob_name =l_bu_rec.lob_name
                                          ,run_id =g_run_id
                                         ,fund_id =
                                                             (
                                                                   select budget_source_id
                                                                   from   ozf_offers
                                                                   where 1 =1
                                                                         and qp_list_header_id =a.plan_id                                                                         
                                                             )
                               where 1 =1
                                    and record_seq =c_new_recs(idx1).record_seq
                                ;
                             exception
                              when others then
                               print_log('Error in updating validated flag for record seq :'||c_new_recs(idx1).record_seq||', msg ='||sqlerrm);
                               rollback to precheck;
                             end;
                        else
                          print_log('prereq status failed : '); 
                            --
                             begin
                               savepoint precheck;
                               update xxcus.xxcus_ozf_recurring_accruals a
                                    set status_desc =l_prereq_status
                                          ,lob_party_id =l_bu_rec.lob_id
                                          ,bu_party_id =l_bu_rec.bu_id
                                          ,lob_name =l_bu_rec.lob_name
                                         ,fund_id =
                                                             (
                                                                   select budget_source_id
                                                                   from   ozf_offers
                                                                   where 1 =1
                                                                         and qp_list_header_id =a.plan_id                                                                         
                                                             )
                               where 1 =1
                                    and record_seq =c_new_recs(idx1).record_seq
                                ;
                             exception
                              when others then
                               print_log('Error in updating validated flag for record seq :'||c_new_recs(idx1).record_seq||', msg ='||sqlerrm);
                               rollback to precheck;
                             end;                          
                        end if; 
                        --
                        --                                                            
                  exception
                   when others then
                        --
                        g_err_msg :=substr(sqlerrm, 1, 2000);
                        --
                        insert into xxcus.xxcus_ozf_recurring_accr_err
                         (
                           record_seq
                          ,err_seq
                          ,err_message
                          ,run_id
                          ,created_by
                         )
                        values
                         (
                           c_new_recs(idx1).record_seq
                          ,l_seq
                          ,'Cannot update stage1 status : '||g_err_msg
                          ,g_run_id
                          ,l_user_id
                         )
                        ;
                        --
                        --rollback to sqr1; 02/02/2018
                        --
                  end;
                  --
             end loop;
             --  
             p_ok :=g_TRUE; 
             --                 
        else
             --
             print_log('No new records, total : '||c_new_recs.count);
             --     
             p_ok :=g_FALSE;
             --            
        end if;
        --        
         p_ok :=g_TRUE; 
         --
      exception
       when no_data_found then
        p_ok :=g_FALSE;
       when others then
        p_ok :=g_FALSE;
      end;
     --
     commit;
     --
 exception
  when others then
   print_log('Issue in xxcus_ozf_recurring_accr_pkg.validate routine, msg ='||sqlerrm);  
   p_ok :=g_FALSE;   
 end Validate;
  --
 procedure extract_recurring_accruals 
   ( 
      p_rec_seq in number
   ) is
  --
  cursor c_recurring_accruals is
  select 
             mvid
            ,mvid_name
            ,offer_name
            ,currency
            ,reason_name
            ,round(amount, 2) amount
            ,lob_name
            ,bu_name
            ,branch_name
            ,receipt_start_date
            ,receipt_end_date
            ,validated_flag
            ,status_code
            ,cust_account_id
            ,plan_id
            ,reason_code
            ,apex_created_by
            ,apex_creation_date
            ,ebs_updated_by
            ,ebs_update_date
            ,record_seq
            ,total_purchases 
  from   xxcus.xxcus_ozf_recurring_accruals
  where 1 =1
      and p_rec_seq is not null
      and  validated_flag =g_validated_Yes
      and  record_seq =p_rec_seq
      and status_code ='STAGE_1'
  union all
  select 
             mvid
            ,mvid_name
            ,offer_name
            ,currency
            ,reason_name
            ,round(amount, 2) amount
            ,lob_name
            ,bu_name
            ,branch_name
            ,receipt_start_date
            ,receipt_end_date
            ,validated_flag
            ,status_code
            ,cust_account_id
            ,plan_id
            ,reason_code
            ,apex_created_by
            ,apex_creation_date
            ,ebs_updated_by
            ,ebs_update_date
            ,record_seq
            ,total_purchases  
  from   xxcus.xxcus_ozf_recurring_accruals
  where 1 =1
      and p_rec_seq is null
      and  validated_flag =g_validated_Yes  
      and status_code ='STAGE_1'            
  ; 
  --
  type c_recurring_accruals_type is table of c_recurring_accruals%rowtype index by binary_integer;
  c_recurring_accruals_rec   c_recurring_accruals_type;
  --   
  cursor c_get_purchases (p_cust_id in number, p_bu in varchar2, p_branch in varchar2, p_from_date in date, p_to_date in date) is
    select line_attribute6 branch,
                bill_to_cust_account_id branch_cust_acct_id, 
                sum(quantity * selling_price) purchases
                --round(sum(quantity * selling_price), 2) purchases
     from ozf.ozf_resale_lines_all
     where 1 = 1
       and p_branch is not null -- for a specific ship to branch code
       and line_attribute2 =p_bu
       and date_ordered between p_from_date and p_to_date
       and sold_from_cust_account_id = p_cust_id
       and line_attribute6 =p_branch
    group by line_attribute6, bill_to_cust_account_id
    union all
    select line_attribute6 branch, 
                bill_to_cust_account_id branch_cust_acct_id,
                sum(quantity * selling_price) purchases
                --round(sum(quantity * selling_price), 2) purchases         
     from ozf.ozf_resale_lines_all
     where 1 = 1
       and p_branch is null -- for all branches
       and line_attribute2 =p_bu       
       and date_ordered between p_from_date and p_to_date
       and sold_from_cust_account_id = p_cust_id
    group by line_attribute6, bill_to_cust_account_id
    ;     
  --
  type c_purchases_type is table of c_get_purchases%rowtype index by binary_integer;
  c_purchases_rec   c_purchases_type;
  --    
  l_move_fwd boolean :=Null; 
  l_line_num number :=0;
  l_user_id number :=fnd_global.user_id;
  --
  l_total_purchases number :=0;
  l_purchase_percent number :=0;
  l_recurr_accrual_amt number :=0;
  --  
 begin 
   --
   execute immediate 'alter session set nls_date_format ='||''''||'DD-MON-YY'||'''';
   --
   print_log('Begin :  extract_recurring_accruals');
   --
   begin 
    --
     open c_recurring_accruals;
     fetch c_recurring_accruals bulk collect into c_recurring_accruals_rec;
     close c_recurring_accruals;
    --
     if c_recurring_accruals_rec.count >0 then
          --
          for idx1 in 1 .. c_recurring_accruals_rec.count loop
               -- Derive branch level accrual and purchases
               begin
                --
                print_log ('c_recurring_accruals_rec(idx1).cust_account_id ='||c_recurring_accruals_rec(idx1).cust_account_id);
                print_log ('c_recurring_accruals_rec(idx1).bu_name ='||c_recurring_accruals_rec(idx1).bu_name);
                print_log ('c_recurring_accruals_rec(idx1).branch_name ='||c_recurring_accruals_rec(idx1).branch_name);
                print_log ('c_recurring_accruals_rec(idx1).receipt_start_date ='||c_recurring_accruals_rec(idx1).receipt_start_date);
                print_log ('c_recurring_accruals_rec(idx1).receipt_end_date ='||c_recurring_accruals_rec(idx1).receipt_end_date);                                                                
                --
                open c_get_purchases
                  (
                     p_cust_id =>c_recurring_accruals_rec(idx1).cust_account_id,
                     p_bu =>c_recurring_accruals_rec(idx1).bu_name,
                     p_branch =>c_recurring_accruals_rec(idx1).branch_name,
                     p_from_date =>c_recurring_accruals_rec(idx1).receipt_start_date,
                     p_to_date =>c_recurring_accruals_rec(idx1).receipt_end_date
                  )
                  ;
                fetch c_get_purchases bulk collect into c_purchases_rec;
                     print_log('01....Purchases found for customer :'||c_purchases_rec.count);                                
                close c_get_purchases;
                --
                    if c_purchases_rec.count >0 then
                     -- get the total purchases first so we can derive the purchase percent by branch further and the actual accrual amount
                        for idx2 in 1 .. c_purchases_rec.count loop
                              --
                              l_total_purchases := l_total_purchases + c_purchases_rec(idx2).purchases; 
                              --
                              begin
                                  --
                                  savepoint sqr2;
                                  --
                                  update xxcus.xxcus_ozf_recurring_accruals 
                                  set total_purchases =l_total_purchases
                                       ,branch_cust_acct_id =case when branch_name is not null then c_purchases_rec(idx2).branch_cust_acct_id else null end
                                       ,status_code ='STAGE_2'
                                       ,status_desc =g_stage2                                       
                                       ,ebs_updated_by=l_user_id
                                       ,ebs_update_date =sysdate
                                  where 1 =1
                                        and record_seq =c_recurring_accruals_rec(idx1).record_seq
                                  ;
                                  --                     
                              exception
                               when others then
                                print_log('Stage 2....error :'||c_recurring_accruals_rec(idx1).record_seq||', '||sqlerrm);                                     
                                rollback to sqr2;
                              end;
                              --
                        end loop;
                        --
                        print_log('Move to IDX3 :  c_purchases_rec.count = '|| c_purchases_rec.count);
                        -- run through each branch to get the individual recurring amount based on purchase percent
                        --
                             for idx3 in 1 .. c_purchases_rec.count loop 
                                 --                                 
                                  l_line_num := l_line_num + 1;
                                  --
                                  begin 
                                    --
                                    --l_purchase_percent :=round((c_purchases_rec(idx3).purchases/l_total_purchases), 2);
                                    l_purchase_percent :=(c_purchases_rec(idx3).purchases/l_total_purchases);                                    
                                    --
                                    --l_recurr_accrual_amt :=round((c_recurring_accruals_rec(idx1).amount * (c_purchases_rec(idx3).purchases/l_total_purchases) * 100)/100, 2);                                    
                                    l_recurr_accrual_amt :=(c_recurring_accruals_rec(idx1).amount * (c_purchases_rec(idx3).purchases/l_total_purchases) * 100)/100;
                                    --
                                    print_log('Inside IDX3 - get % and recurring amount, Line# '||idx3||', c_recurring_accruals_rec(idx3).branch_name ='||c_purchases_rec(idx3).branch
                                                       ||', l_purchase_percent ='||l_purchase_percent||', l_recurr_accrual_amt ='||l_recurr_accrual_amt
                                                     );
                                    --                                  
                                  exception
                                   when others then
                                    l_purchase_percent :=0;
                                    l_recurr_accrual_amt :=0;
                                    print_log('ERROR Inside IDX3 - get % and recurring amount : '||idx3||', c_recurring_accruals_rec(idx3).branch_name ='||c_purchases_rec(idx3).branch);                                    
                                  end;
                                  --
                                  begin
                                       Null;
                                   savepoint sqr3;
                                    insert into xxcus.xxcus_ozf_recurr_accr_utils
                                    (
                                      record_seq,  
                                      line_num,
                                      cust_account_id,
                                      plan_id,
                                      branch_cust_acct_id,
                                      branch_name,
                                      purchase_percent,
                                      recurring_accrual_amount, 
                                      status_code,
                                      status_desc,  
                                      ebs_updated_by,
                                      ebs_update_date,
                                      run_id
                                    )
                                    values
                                    (
                                      c_recurring_accruals_rec(idx1).record_seq,  
                                      l_line_num,
                                      c_recurring_accruals_rec(idx1).cust_account_id,
                                      c_recurring_accruals_rec(idx1).plan_id,
                                      c_purchases_rec(idx3).branch_cust_acct_id,
                                      c_purchases_rec(idx3).branch,
                                      l_purchase_percent, --round((c_purchases_rec(idx3).purchases/l_total_purchases) * 100, 2),
                                      l_recurr_accrual_amt, --(c_recurring_accruals_rec(idx1).amount * round((c_purchases_rec(idx3).purchases/l_total_purchases) * 100, 2))/100, 
                                      'STAGE_3',
                                      g_stage3,  
                                      l_user_id,
                                      sysdate,
                                      g_run_id                            
                                    );     
                                        --
                                        if sql%rowcount >0 then 
                                             --
                                              begin
                                                  --
                                                  savepoint sqr4;
                                                  --
                                                  update xxcus.xxcus_ozf_recurring_accruals 
                                                  set status_desc =g_stage3
                                                       ,status_code ='STAGE_3'
                                                       ,ebs_updated_by=l_user_id
                                                       ,ebs_update_date =sysdate                                                  
                                                  where 1 =1
                                                        and record_seq =c_recurring_accruals_rec(idx1).record_seq
                                                  ;
                                                  --                     
                                              exception
                                               when others then
                                                print_log('Stage 3 status code update failed....error :'||c_recurring_accruals_rec(idx1).record_seq||', '||sqlerrm);                                     
                                                rollback to sqr4;
                                              end;
                                              --
                                        end if;
                                        --                              
                                  exception
                                   when others then
                                    rollback to sqr3;
                                    print_log('Stage 3 status code update failed...record seq '||c_recurring_accruals_rec(idx1).record_seq);                                                                      
                                    print_log('Stage 3 status code update failed...error ='||sqlerrm);
                                  end;
                              --
                             end loop;
                         --
                    else
                     print_log('No purchase found for customer :'||c_recurring_accruals_rec(idx1).cust_account_id);
                              --
                              begin
                                  --
                                  savepoint sqr2;
                                  --
                                  update xxcus.xxcus_ozf_recurring_accruals 
                                  set status_code ='STAGE_2'
                                       ,status_desc ='No purchases for customer.'                                       
                                       ,ebs_updated_by=l_user_id
                                       ,ebs_update_date =sysdate
                                  where 1 =1
                                        and record_seq =c_recurring_accruals_rec(idx1).record_seq
                                  ;
                                  --                     
                              exception
                               when others then
                                print_log('Stage 2....error :'||c_recurring_accruals_rec(idx1).record_seq||', '||sqlerrm);                                     
                                rollback to sqr2;
                              end;
                              --                     
                    end if;
                --
                --
               exception
                when no_data_found then
                 print_log('Purchases not found for the recurring accrual record seq :'||c_recurring_accruals_rec(idx1).record_seq);
                when others then
                 print_log('When other errrors in extract_recurring_accrulals, record seq '||c_recurring_accruals_rec(idx1).record_seq);
                 raise program_error;           
               end;
               --
          end loop;      
          --
     end if;
    --
    --
   exception
    when no_data_found then
     print_log('No data found in extract_recurring_accrulals');
     raise program_error;
    when others then
     print_log('When other errrors in extract_recurring_accrulals');
     raise program_error;
   end;
   --
   print_log('End :  extract_recurring_accruals');
   --   
 exception
  when others then  
   print_log('Issue in xxcus_ozf_recurring_accr_pkg.extract_recurring_accruals routine, msg ='||sqlerrm);
   raise program_error;
 end extract_recurring_accruals;
  --
 procedure process_adj (p_rec_seq in number) is
 --
 l_adj_rec OZF_FUND_UTILIZED_PUB.adjustment_rec_type; --adj_rec_type; 
  --
  cursor c_accruals_master_set is  
  select 
             mvid
            ,mvid_name
            ,offer_name
            ,currency
            ,reason_name
            --,round(amount, 2) amount            
            ,amount 
            ,lob_name
            ,bu_name
            ,branch_name
            ,receipt_start_date
            ,receipt_end_date
            ,validated_flag
            ,status_code
            ,cust_account_id
            ,plan_id
            ,reason_code
            ,apex_created_by
            ,apex_creation_date
            ,ebs_updated_by
            ,ebs_update_date
            ,record_seq
            ,total_purchases
            ,lob_party_id
            ,bu_party_id
            ,branch_cust_acct_id
            ,fund_id
  from   xxcus.xxcus_ozf_recurring_accruals
  where 1 =1
      and  run_id =g_run_id  
      and  validated_flag =g_validated_Yes
      and  status_code ='STAGE_3'
      and  record_seq =p_rec_seq                  
      and p_rec_seq is not null
      and total_purchases >0
  union all
  select 
             mvid
            ,mvid_name
            ,offer_name
            ,currency
            ,reason_name
            --,round(amount, 2) amount
            ,amount            
            ,lob_name
            ,bu_name
            ,branch_name
            ,receipt_start_date
            ,receipt_end_date
            ,validated_flag
            ,status_code
            ,cust_account_id
            ,plan_id
            ,reason_code
            ,apex_created_by
            ,apex_creation_date
            ,ebs_updated_by
            ,ebs_update_date
            ,record_seq
            ,total_purchases
            ,lob_party_id
            ,bu_party_id
            ,branch_cust_acct_id
            ,fund_id
  from   xxcus.xxcus_ozf_recurring_accruals
  where 1 =1
      and  run_id =g_run_id
      and  validated_flag =g_validated_Yes  
      and  status_code ='STAGE_3'        
      and p_rec_seq is null       
      and total_purchases >0                 
  ; 
  --
  type c_recurring_accruals_mst  is table of c_accruals_master_set%rowtype index by binary_integer;
  c_accruals_header   c_recurring_accruals_mst;
  --   
  cursor c_accrual_lines_set  (p_rec_seq in number) is
    select 
             line_num
            ,cust_account_id
            ,plan_id
            ,branch_name
            ,purchase_percent
            ,recurring_accrual_amount recurring_amount
            ,status_code
            ,status_desc
            ,branch_cust_acct_id    
    from xxcus.xxcus_ozf_recurr_accr_utils 
    where 1 =1
         and run_id =g_run_id  
         and status_code ='STAGE_3'
         and record_seq =p_rec_seq
         --and line_num =2
    ;     
  --
  type c_accrual_lines_type is table of c_accrual_lines_set%rowtype index by binary_integer;
  c_accrual_lines   c_accrual_lines_type;
  --    
  l_move_fwd boolean :=Null; 
  l_line_num number :=0;
  l_user_id number :=fnd_global.user_id;
  --
  l_total_purchases number :=0;
  l_purchase_percent number :=0;
  l_recurr_accrual_amt number :=0;
  --
  l_adjustment_type varchar2(30) :=Null;
  --
  l_error_rec xxcus.xxcus_ozf_recurring_accr_err%rowtype :=Null;  
  --
  v_message          VARCHAR2(250);
  l_message          VARCHAR2(150);
  l_err_msg          VARCHAR2(3000);
  l_err_code         NUMBER;
  l_sec              VARCHAR2(500);
  l_req_id           NUMBER := FND_GLOBAL.CONC_REQUEST_ID;

   l_return_status             VARCHAR2(2000) :=Null;
   l_msg_count                 NUMBER :=Null;
   l_msg_data                 VARCHAR2(2000) :=Null;
   --  
   l_idx number :=0; 
   l_api_failed number :=0;
   l_api_success number :=0;
   l_utilization_id number :=0;
  --
 begin 
   --
   print_log('Begin :  process_adj (p_rec_seq =>'||p_rec_seq||');');
   --
   begin 
    --
     open c_accruals_master_set;
     fetch c_accruals_master_set bulk collect into c_accruals_header;
     close c_accruals_master_set;
    --
     if c_accruals_header.count >0 then
          --
          for idx1 in 1 .. c_accruals_header.count loop
               --
               savepoint start_here;
               --               
               l_api_failed :=0; 
               l_api_success :=0;
               --
               begin
                --
                print_log ('c_accruals_header(idx1).record_seq ='||c_accruals_header(idx1).record_seq);                                                                
                --
                open c_accrual_lines_set (p_rec_seq =>c_accruals_header(idx1).record_seq);
                fetch c_accrual_lines_set bulk collect into c_accrual_lines;
                print_log('01....Found '||c_accrual_lines.count||' accrual utilization lines for record_seq :'||c_accruals_header(idx1).record_seq);                                
                close c_accrual_lines_set;
                --
                    if c_accrual_lines.count >0 then
                        --       
                        for idx2 in 1 .. c_accrual_lines.count loop 
                              --
                              print_log('02....accrual utilization line# :'||c_accrual_lines(idx2).line_num);                              
                              --
                              if sign(c_accrual_lines(idx2).recurring_amount) = 1  then
                                    l_adj_rec.adjustment_type := 'STANDARD';
                              else
                                    l_adj_rec.adjustment_type := 'DECREASE_EARNED';
                              end if;                              
                              --
                              begin -- Begin assign variables 
                                     l_adj_rec.fund_id :=c_accruals_header(idx1).fund_id;
                                     l_adj_rec.amount :=c_accrual_lines(idx2).recurring_amount;
                                     l_adj_rec.currency_code :=c_accruals_header(idx1).currency;
                                     l_adj_rec.adjustment_date :=sysdate;
                                     l_adj_rec.gl_date:=sysdate;                                     
                                     l_adj_rec.activity_id :=c_accrual_lines(idx2).plan_id;
                                     l_adj_rec.activity_type  :='OFFR';
                                     l_adj_rec.cust_account_id :=c_accrual_lines(idx2).cust_account_id;
                                     l_adj_rec.attribute1 :='LOB : '||c_accruals_header(idx1).lob_name;
                                     l_adj_rec.attribute2 :='BU : '||c_accruals_header(idx1).bu_name;                                                                          
                                     l_adj_rec.attribute3 :='Branch : '||c_accrual_lines(idx2).branch_name;                                     
                                     l_adj_rec.attribute4 :='Rcpt Start Date : '||to_char(c_accruals_header(idx1).receipt_start_date, 'MM/DD/YYYY');
                                     l_adj_rec.attribute5 :='Rcpt End Date : '||to_char(c_accruals_header(idx1).receipt_end_date, 'MM/DD/YYYY');
                                     l_adj_rec.attribute6 :='RUN ID : '||to_char(g_run_id);
                                     l_adj_rec.attribute10 :=c_accruals_header(idx1).reason_code;                                     
                                     l_adj_rec.attribute11 :='Reason Name : '||c_accruals_header(idx1).reason_name;
                                     l_adj_rec.attribute13 :='Note : '||'Recurring accruals - mass upload for record seq : '||c_accruals_header(idx1).record_seq;                                     
                              exception
                               when others then
                                print_log ('Error in assigning adjusment collection table variables, msg ='||sqlerrm);                                                          
                              end; -- End assign variables
                              --
                              begin
                                   /*
                                   print_log(' l_adj_rec.adjustment_type ='|| l_adj_rec.adjustment_type);
                                   print_log(' l_adj_rec.fund_id ='|| l_adj_rec.fund_id);
                                   print_log(' l_adj_rec.amount ='|| l_adj_rec.amount);
                                   print_log(' l_adj_rec.currency_code ='|| l_adj_rec.currency_code);
                                   print_log(' l_adj_rec.adjustment_date ='|| l_adj_rec.adjustment_date);
                                   print_log(' l_adj_rec.gl_date ='|| l_adj_rec.gl_date);
                                   print_log(' l_adj_rec.activity_id ='|| l_adj_rec.activity_id);                                                                                                         
                                   print_log(' l_adj_rec.activity_type ='|| l_adj_rec.activity_type);                                   
                                   print_log(' l_adj_rec.cust_account_id ='|| l_adj_rec.cust_account_id);
                                   print_log(' l_adj_rec.attribute1 ='|| l_adj_rec.attribute1);
                                   print_log(' l_adj_rec.attribute2 ='|| l_adj_rec.attribute2);
                                   print_log(' l_adj_rec.attribute3 ='|| l_adj_rec.attribute3);
                                   print_log(' l_adj_rec.attribute4 ='|| l_adj_rec.attribute4);
                                   print_log(' l_adj_rec.attribute5 ='|| l_adj_rec.attribute5);
                                   print_log(' l_adj_rec.attribute6 ='|| l_adj_rec.attribute6);
                                   print_log(' l_adj_rec.attribute10 ='|| l_adj_rec.attribute10);
                                   print_log(' l_adj_rec.attribute11 ='|| l_adj_rec.attribute11);
                                   print_log(' l_adj_rec.attribute13 ='|| l_adj_rec.attribute13);
                                   */
                                   Null;                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                
                              end;
                              --
                              begin
                                  --
                                  fnd_msg_pub.Delete_Msg; -- Clear message stack
                                  --
                                      ozf_fund_utilized_pub.create_fund_adjustment
                                              (
                                                  p_api_version      => 1.0,
                                                  p_init_msg_list    => fnd_api.g_true,
                                                  p_commit           => fnd_api.g_false,
                                                  p_validation_level => fnd_api.g_valid_level_full,
                                                  p_adj_rec          => l_adj_rec,
                                                  x_return_status    => l_return_status,
                                                  x_msg_count        => l_msg_count,
                                                  x_msg_data         => l_msg_data,
                                                  x_utilization_id       =>l_utilization_id
                                              );
                                  --
                                      if l_return_status =fnd_api.g_ret_sts_success then
                                         print_log('Recurring accrual created for line num : '||c_accrual_lines(idx2).line_num);
                                         l_api_success :=l_api_success+1;                                         
                                       else
                                         print_log('Recurring accrual failed for line num : '||c_accrual_lines(idx2).line_num);
                                         l_api_failed :=l_api_failed+1;                                         
                                      end if;
                                  --                     
                              exception
                               when others then            
                                l_api_failed :=l_api_failed+1;
                                print_log('@process_adj, l_api_failed variable updated, stage 4 -api call....error :'||c_accruals_header(idx1).record_seq||', msg =>'||sqlerrm);
                              end;
                              --
                              begin
                                  -- gather line utils table record status for each record seq and line number
                                  g_line_rec(idx2).record_seq :=c_accruals_header(idx1).record_seq;
                                  g_line_rec(idx2).line_num :=c_accrual_lines(idx2).line_num;                                  
                                  g_line_rec(idx2).status_code :='STAGE_4';
                                  g_line_rec(idx2).status_desc :=case when l_return_status =fnd_api.g_ret_sts_success  then g_stage40 else g_stage41 end;
                                  g_line_rec(idx2).ebs_updated_by :=l_user_id;
                                  g_line_rec(idx2).ebs_update_date :=sysdate;                                  
                                  g_line_rec(idx2).api_status :=l_return_status;                                                                    
                                  g_line_rec(idx2).utilization_id :=l_utilization_id;                                  
                                  --               
                              exception
                               when others then
                                print_log('@process_adj, stage4 collect utilizations -api_status_catch....error :'
                                                      ||c_accruals_header(idx1).record_seq
                                                      ||', Line# '
                                                      ||c_accrual_lines(idx2).line_num
                                                      ||',  '
                                                      ||sqlerrm
                                                 );                                     
                                rollback to api_status_catch;
                              end;
                              --
                              --                              
                              begin
                                  --
                                  if (l_msg_count >0 and l_return_status <>fnd_api.g_ret_sts_success)   then
                                       fnd_msg_pub.count_and_get(p_encoded => fnd_api.g_false,
                                                          p_count   => l_msg_count,
                                                          p_data    => l_msg_data);
                                        for err_idx in 1 .. l_msg_count loop 
                                            --
                                            l_error_rec :=Null;
                                            --
                                            l_error_rec.record_seq :=c_accruals_header(idx1).record_seq;
                                            l_error_rec.line_num :=c_accrual_lines(idx2).line_num;
                                            l_error_rec.err_seq :=err_idx;
                                            l_error_rec.err_message :=substr(fnd_msg_pub.get(p_msg_index => err_idx, p_encoded => 'F'), 1, 254);
                                            l_error_rec.run_id :=g_run_id;
                                            l_error_rec.created_by :=l_user_id;
                                            --
                                            log_error(l_error_rec);
                                            --
                                        end loop;
                                          --                                   
                                  end if;
                                  --                       
                              exception
                               when others then
                                print_log('@process_adj, stage4 - log error. insert :'||c_accruals_header(idx1).record_seq||', '||sqlerrm);
                              end;
                              --                              
                        end loop;
                        --
                    else
                     print_log('No accrual utilizations found for record seq  :'||c_accruals_header(idx1).record_seq);
                    end if;
                --
                --
               exception
                when no_data_found then
                 print_log('Purchases not found for the recurring accrual record seq :'||c_accruals_header(idx1).record_seq);
                when others then
                 print_log('When other errrors in extract_recurring_accrulals, record seq '||c_accruals_header(idx1).record_seq);
                 raise program_error;           
               end;
               --                    
               -- gather header table record status for each record seq
              begin
                  --
                  g_hdr_rec(idx1).record_seq :=c_accruals_header(idx1).record_seq;
                  g_hdr_rec(idx1).status_code :='STAGE_4';
                  g_hdr_rec(idx1).status_desc :='All Branches, Success : '||l_api_success||', Failed : '||l_api_failed;
                  g_hdr_rec(idx1).ebs_updated_by :=l_user_id;
                  --                    
              exception
               when others then
                print_log('@process_adj, stage4 -collect header status....error :'||c_accruals_header(idx1).record_seq||', '||sqlerrm);                                     
                rollback to upd_hdr_status;
              end;
              --
             if l_api_failed >0 then
              rollback to start_here; -- all lines for the header will be rolled back..does not matter if we had some lines with api status of success.
             end if;   
               --                                                                     
          end loop;      
          --
     end if;
        --
        -- update header table status
        --
        if g_hdr_rec.count >0 then
             for idx5 in 1 .. g_hdr_rec.count loop
                update_header_status
                      (
                          p_rec_seq =>g_hdr_rec(idx5).record_seq
                         ,p_status_code =>g_hdr_rec(idx5).status_code
                         ,p_status_desc =>g_hdr_rec(idx5).status_desc
                         ,p_ebs_updated_by =>g_hdr_rec(idx5).ebs_updated_by   
                      ) 
                      ;
             end loop;
        end if;
    --
    -- update line accrual utils table status
    --
        if g_line_rec.count >0 then
             for idx6 in 1 .. g_line_rec.count loop
                update_alloc_util_status 
                      (
                          p_rec_seq =>g_line_rec(idx6).record_seq
                         ,p_line_num =>g_line_rec(idx6).line_num
                         ,p_status_code =>g_line_rec(idx6).status_code
                         ,p_status_desc =>g_line_rec(idx6).status_desc
                         ,p_ebs_updated_by =>g_line_rec(idx6).ebs_updated_by
                         ,p_api_status =>g_line_rec(idx6).api_status
                         ,p_util_id =>g_line_rec(idx6).utilization_id
                      )
                      ;     
             end loop;
        end if;
    --    
   exception
    when no_data_found then
     print_log('No data found in process_adj');
     raise program_error;
    when others then
     print_log('When other errrors in process_adj');
     raise program_error;
   end;
   --
   print_log('End :  process_adj (p_rec_seq =>'||p_rec_seq||');');
   --
 exception
  when others then  
   print_log('Issue in xxcus_ozf_recurring_accr_pkg.process_adj routine, msg ='||sqlerrm);
   raise program_error;
 end process_adj;
  --  
 procedure Main
   ( 
      errbuff         out      varchar2,
      retcode         out      number,
      p_record_seq in number default null,
      p_debug in varchar2 default 'N'
   ) is
  --
  l_move_fwd boolean :=Null; 
  --l_rec_seq number :=p_record_seq;
  --  
 begin 
      --
      g_debug :=p_debug;
      --
      begin -- Get run id
        --
        select xxcus.xxcus_ozf_recurr_accr_runid_s.nextval
        into    g_run_id
        from  dual;
        --
        print_log('In Validate routine, successfully fetched run id : '||g_run_id);
        --
      exception
       when others then
        g_run_id :=to_char(sysdate, 'YYYYMMDDHH24MISS');
      end;
      --    
      begin -- update  run id
        --
        update xxcus.xxcus_ozf_recurring_accruals set run_id =g_run_id where run_id =0; -- only for the current batch that was just uploaded
        --
        print_log('In Validate routine, successfully updated run id from default 0 to the latest sequence value : '||g_run_id);
        --
        commit;
        --
      exception
       when others then
        rollback;
        g_run_id :=to_char(sysdate, 'YYYYMMDDHH24MISS');
      end;
      --
      Validate (p_ok =>l_move_fwd, p_rec_seq =>p_record_seq);
      --
      if (l_move_fwd) then
           --
           print_log('Validate routine returned true');  
           extract_recurring_accruals (p_rec_seq =>p_record_seq);
           l_move_fwd :=TRUE;
           --
      else
           --
           print_log('Validate routine returned false');
           l_move_fwd :=FALSE;           
           --
      end if;
      --
      if (l_move_fwd) then
           --
           print_log('Extract accruals routine returned true, Begin: Adjustments');  
           process_adj (p_rec_seq =>p_record_seq);
           --
      else
           --
           print_log('Extract accruals routine returned false, failed to proceed to adjustments.');
           --
      end if;
      -- 
 exception
  when others then
   print_log('Issue in xxcus_ozf_recurring_accr_pkg.Main routine, msg ='||sqlerrm);
 end Main;
  --
end xxcus_ozf_recurring_accr_pkg;
/