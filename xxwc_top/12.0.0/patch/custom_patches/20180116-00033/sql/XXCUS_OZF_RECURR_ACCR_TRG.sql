/*
   Version Ticket                                      Date                  Author                                                                                     Notes
   ======  ============                     ===========    =========================================       ===================================================
   1.0         TMS 20180116-00033    02/03/2018     Balaguru Seshadri                                                                HDSupply Rebates Recurring Accruals mostly for COOP offers
*/
DROP TRIGGER XXCUS.XXCUS_OZF_RECURR_ACCR_TRG;
CREATE TRIGGER XXCUS.XXCUS_OZF_RECURR_ACCR_TRG
BEFORE INSERT ON XXCUS.XXCUS_OZF_RECURRING_ACCRUALS 
REFERENCING NEW AS NEW OLD AS OLD
FOR EACH ROW
BEGIN
  :NEW.RECORD_SEQ := XXCUS.XXCUS_OZF_RECURR_ACCR_SEQ_S.NEXTVAL;
END XXCUS_OZF_RECURR_ACCR_TRG;
/