CREATE OR REPLACE PACKAGE BODY APPS.xxwc_ar_cash_refund_pkg
AS
   /*************************************************************************
   *   $Header xxwc_om_cash_refund_pkg.sql $
   *   Module Name: xxwc OM Cash Refund package
   *
   *   PURPOSE:   Used in extension Cash Refunds process
   *
   *   REVISIONS:
   *   Ver        Date        Author                     Description
   *   ---------  ----------  ---------------         -------------------------
   *   1.0        04/01/2012  Shankar Hariharan             Initial Version
   *   1.1        06/17/2012  Shankar Hariharan             Added copies to 0 when submitting autoinvoice
   *   1.2        09/10/2012  Shankar Hariharan             Changed the batch source derive logic while submitting auto invoice
   *   1.3        01/03/2013  Shankar Hariharan             Bug fix for multiple invoice tied to a single SO return
   *   1.4        01/31/2013  Shankar Hariharan             Performance Issue fix
   *   1.5        04/11/2013  Shankar Hariharan             Oracle Credit Card Disconnect Project
   *   1.6        07/08/2013  Shankar Hariharan             TMS Task 20130703-00901 Release holds before auto invoice call
   *   1.7        02/01/2014  Gopi Damuluri                 TMS# 20130912-00853
   *                                                        Performance improvement for "XXWC: OM Procedure to Release Pricing Change Hold" process.
   *   1.8        09/25/2014  Maharajan Shunmugam           TMS# 20141001-00155 Canada Multi org changes
   *   1.9        03/07/2016  Manjula Chellappan            TMS# 20150527-00318 Forego Two AR Triggers
   * ***************************************************************************/
PROCEDURE launch_auto_remittance (l_cash_receipt_id IN NUMBER,o_return_message out varchar2)
IS
/*
Launched from the "Process Refund" button action, if the receipt is a credit card receipt
and not in remitted status. Oracle do not allow a refund to a unremitted credit card receipt
*/
   l_request_id          NUMBER;
   l_receipt_class_id    NUMBER;
   l_receipt_method_id   NUMBER;
   l_branch_party_id     NUMBER;
   l_bank_acct_use_id    NUMBER;
   l_curr_code           VARCHAR2(30);                                --Added for version 1.8
BEGIN
  -- Derive parameter values for concurrent program submission
   SELECT rm.receipt_method_id, rm.receipt_class_id
     INTO l_receipt_method_id, l_receipt_class_id
     FROM apps.ar_receipt_methods rm, apps.ar_cash_receipts cr
    WHERE rm.receipt_method_id =cr.receipt_method_id
      AND cr.cash_receipt_id=l_cash_receipt_id;   

   SELECT ba.bank_acct_use_id, br.branch_party_id
     INTO l_bank_acct_use_id , l_branch_party_id
     FROM apps.ce_bank_acct_uses ba
        , apps.ce_bank_accounts cba
        , apps.ce_bank_branches_v br
        , apps.ar_cash_receipts cr
    WHERE ba.bank_account_id = cba.bank_account_id
      AND cba.bank_branch_id = br.branch_party_id
      AND ba.bank_acct_use_id=cr.remit_bank_acct_use_id
      and cr.cash_receipt_id=l_cash_receipt_id;
    
   --Added for version 1.8

SELECT gsob.currency_code 
     INTO l_curr_code
  FROM apps.hr_operating_units hou
     , apps.gl_sets_of_books gsob
WHERE 1 = 1
   and hou.organization_id = MO_GLOBAL.GET_CURRENT_ORG_ID
   and gsob.set_of_books_id = hou.set_of_books_id;


   l_request_id :=
      fnd_request.submit_request (
         --REMIT, 2012/04/06 00:00:00, 2012/04/06 00:00:00, Y, Y, Y, , , USD, , , , STANDARD, 2003, 5000, , 8103, 10081, , , , , , , , , , , , , , , , , , , , , , , , , 1
         application   => 'AR'
       , program       => 'ARAREMMB'
       , description   => 'Automatic Remittances Master Program from OM'
       , start_time    => SYSDATE
       , sub_request   => NULL
       , argument1     => 'REMIT'
       , argument2     => TO_CHAR (TRUNC (SYSDATE), 'YYYY/MM/DD HH24:MI:SS')
       , argument3     => TO_CHAR (TRUNC (SYSDATE), 'YYYY/MM/DD HH24:MI:SS')
       , argument4     => 'Y'
       , argument5     => 'Y'
       , argument6     => 'Y'
       , argument7     => NULL
       , argument8     => NULL
       , argument9     => l_curr_code				--'USD'         --Added for version 1.8
       , argument10    => NULL
       , argument11    => NULL
       , argument12    => NULL
       , argument13    => 'STANDARD'
       , argument14    => TO_CHAR (l_receipt_class_id)
       , argument15    => TO_CHAR (l_receipt_method_id)
       , argument16    => NULL
       , argument17    => TO_CHAR (l_branch_party_id)
       , argument18    => TO_CHAR (l_bank_acct_use_id)
       , argument19    => NULL
       , argument20    => NULL
       , argument21    => NULL
       , argument22    => NULL
       , argument23    => NULL
       , argument24    => NULL
       , argument25    => NULL
       , argument26    => NULL
       , argument27    => NULL
       , argument28    => NULL
       , argument29    => NULL
       , argument30    => NULL
       , argument31    => NULL
       , argument32    => NULL
       , argument33    => NULL
       , argument34    => NULL
       , argument35    => NULL
       , argument36    => NULL
       , argument37    => NULL
       , argument38    => NULL
       , argument39    => NULL
       , argument40    => NULL
       , argument41    => NULL
       , argument42    => NULL
       , argument43    => '1');

    IF l_request_id <> 0 then
      o_return_message := 'Remittance Process submitted successfully with request id='||l_request_id||
      ' Do not press this button again until the process has completed. Monitor processing through the Requests Concurrent Manager form';
      commit;
      return;
    ELSE
      o_return_message :=  'Issue submitting Remittance Process';
      rollback;
      return; 
    END IF;          

END launch_auto_remittance;


PROCEDURE check_rec_in_doubt(p_cash_receipt_id IN NUMBER,
                             x_rec_in_doubt OUT NOCOPY VARCHAR2,
                             x_rid_reason OUT NOCOPY VARCHAR2) IS
BEGIN
   ---Shankar-- Copied from ar_prepayments package
   ---Called from the XXWC_OM_REFUND refund from to check whether a credit card receipt
   -- is in a remitted state
   x_rec_in_doubt := 'N';
   x_rid_reason   := null;
   ---
   --- For CC receipts, receipt should be remitted
   ---
    BEGIN
      SELECT 'Y', 'Receipt is not in Remitted or Cleared Status. Please wait for few minutes '--arp_standard.fnd_message('AR_RID_NOT_REMITTED_OR_CLEARED')
      INTO   x_rec_in_doubt, x_rid_reason
      FROM   dual
      WHERE
         (
           NOT EXISTS
           (
             SELECT 1
             FROM  apps.AR_CASH_RECEIPT_HISTORY crh
             WHERE crh.cash_receipt_id = p_cash_receipt_id
             AND   crh.status IN ('REMITTED', 'CLEARED')
           )
         );
   EXCEPTION
      WHEN NO_DATA_FOUND THEN
         NULL;
      WHEN OTHERS THEN
         arp_standard.debug('Unexpected error '||sqlerrm||
            ' occurred in ar_prepayments.check_rec_in_doubt');
         RAISE;
   END;

   ---
   --- There should not be any Claims Investigation or CB special application
   ---
   BEGIN
      SELECT 'Y', arp_standard.fnd_message('AR_RID_CLAIM_OR_CB_APP_EXISTS')
      INTO   x_rec_in_doubt, x_rid_reason
      FROM   dual
      WHERE
           EXISTS
           (
             SELECT 1
             FROM   apps.ar_receivable_applications ra
             WHERE  ra.cash_receipt_id = p_cash_receipt_id
             AND    applied_payment_schedule_id IN (-4,  -5)
             AND    display = 'Y'
           );
   EXCEPTION
      WHEN NO_DATA_FOUND THEN
         NULL;
      WHEN OTHERS THEN
         arp_standard.debug('Unexpected error '||sqlerrm||
            ' occurred in ar_prepayments.check_rec_in_doubt');
         RAISE;
   END;

   ---
   --- Receipt should not be reversed
   ---
    BEGIN
      SELECT 'Y', arp_standard.fnd_message('AR_RID_RECEIPT_REVERSED')
      INTO   x_rec_in_doubt, x_rid_reason
      FROM   dual
      WHERE
           EXISTS
           (
             SELECT 1
             FROM   apps.ar_cash_receipts cr1
             WHERE  cr1.cash_receipt_id = p_cash_receipt_id
             AND    cr1.reversal_date is not null
           );
   EXCEPTION
      WHEN NO_DATA_FOUND THEN
         NULL;
      WHEN OTHERS THEN
         arp_standard.debug('Unexpected error '||sqlerrm||
            ' occurred in ar_prepayments.check_rec_in_doubt');
         RAISE;
   END;

   ---
EXCEPTION
   WHEN OTHERS THEN
      arp_standard.debug('Unexpected error '||sqlerrm||
         ' occurred in arp_process_returns.check_rec_in_doubt');
      RAISE;
END check_rec_in_doubt;
   PROCEDURE cash_refund_process (i_header_id           IN     NUMBER
                                , i_return_header_id    IN     NUMBER
                                , i_cash_receipt_id     IN     NUMBER
                                , i_refund_amount       IN     NUMBER
                                , i_check_refund_amount IN     NUMBER
                                , i_payment_type_code   IN     VARCHAR2
                                , o_return_status          OUT VARCHAR2
                                , o_return_message         OUT VARCHAR2)
   IS
   /*
   Cash Refund Core process -- Handles cash refunds against a single order
   Returns for multilple orders -- one RMA order should be created for each order returned 
   Handles One receipt multiple Invoice and one Invoice multiple receipts
   The process DO NOT HANDLE multiple invoice / multiple receipts.
   
   Credit CArd Refund -- Invoice is unapplied from receipt, net amount (receipt amount minus refund) is applied
                      back and a receivable activity of refund is applied against the receipt and Oracle Payments
                      automatically refunds the money back to the original credit card
   
  Check Refund --Invoice is unapplied from receipt, net amount (receipt amount minus refund) is applied back
                Remaining amount is left as unapplied against the receipt and a notification sent for manual
                check refund processing
                
 Cash Refund --Invoice is unapplied from receipt, net amount (receipt amount minus refund) is applied back
              A receivable activity identified by custom profile of type "receipt writeoff" is applied against
              the receipt to record the actual cash refund. Since the activity is of type "reciept writeoff", 
              approval limits needs to be setup on a individual user basis for this to work.                                        
   */
      l_applied_amt                    NUMBER;
      l_line_amt                       NUMBER;
      l_tax_amt                        NUMBER;
      l_new_applied_amt                NUMBER;
      l_ps_applied_amt                 NUMBER;
      l_return_status                  VARCHAR2 (1);
      l_msg_count                      NUMBER;
      l_msg_data                       VARCHAR2 (32000);
      l_app_ps_id                      NUMBER;
      p_count                          NUMBER;
      l_application_ref_type           ar_receivable_applications.application_ref_type%TYPE;
      l_application_ref_id             ar_receivable_applications.application_ref_id%TYPE;
      l_application_ref_num            ar_receivable_applications.application_ref_num%TYPE;
      l_secondary_application_ref_id   ar_receivable_applications.secondary_application_ref_id%TYPE;
      l_receivable_application_id      ar_receivable_applications.receivable_application_id%TYPE;
      l_cc_refund_ps_id                NUMBER := -6;
      l_rec_wo_ps_id                   NUMBER := -3;
      l_refund_rec_trx_id              NUMBER;
      l_rec_wo_rec_trx_id              NUMBER;
      l_acc_app_ps_id                  NUMBER;
      l_acc_app_rec_trx_id             NUMBER;
      l_debug                          VARCHAR2 (240);
      l_adj_ccid                       NUMBER;
      l_pay_count                      NUMBER;
      l_inv_count                      NUMBER;
      l_cust_trx_line_id               NUMBER;
      l_return_Number                  NUMBER; -- used in return value of email
      l_refund_email                   VARCHAR2 (100) := fnd_Profile.VALUE ('XXWC_OM_REFUND_REQ_EMAIL');
      --Variables for Notification
      l_orig_order_number              VARCHAR2 (100);
      l_orig_order_date                DATE;
      l_return_order_number            VARCHAR2 (100);
      l_return_order_date              DATE;
      l_receipt_number                 VARCHAR2 (100);
      l_receipt_date                   DATE;
      l_receipt_amount                 NUMBER;
      l_customer_number                VARCHAR2 (100);
      l_customer_name                  VARCHAR2 (240);
      l_cc_type                        VARCHAR2 (1);
      l_cc_rec_wo_rec_trx_id           NUMBER;
      l_return_data                    VARCHAR2(2000);
      l_attribute_rec                  AR_RECEIPT_API_PUB.attribute_rec_type;
   BEGIN
   /*
      update AR_SYSTEM_PARAMETERS
         set rule_set_id = 3;
     */
     
     -- Go through the process, only if a refund is requested
      IF i_refund_amount IS NOT NULL or i_check_refund_amount IS NOT NULL                                   -- IF 1
      THEN
         -- Get refund rec trx id
         BEGIN
            SELECT receivables_trx_id
              INTO l_refund_rec_trx_id
              FROM apps.ar_receivables_trx
             WHERE TYPE = 'CCREFUND' AND status = 'A' AND ROWNUM = 1;

            l_rec_wo_rec_trx_id := fnd_profile.value('XXWC_OM_CASH_REFUND_ACTIVITY');    
            l_cc_rec_wo_rec_trx_id := fnd_profile.value('XXWC_OM_CC_REFUND_ACTIVITY');

         EXCEPTION
            WHEN OTHERS
            THEN
               o_return_message :=
                  'Missing Receivable Trx Setup or Branch Specific CCID';
               o_return_status := 'E';
               RETURN;
         END;
        -- Check if the receipt is applied against an invoice
         SELECT NVL (SUM (amount_applied), 0)
           INTO l_applied_amt
           FROM apps.ar_receivable_applications_v
          WHERE cash_receipt_id = i_cash_receipt_id
                AND (applied_payment_schedule_id > 0); 
                
         IF l_applied_amt = 0 then
               o_return_message :=
                  'Sold Order is not invoiced. Running Auto Invoice Program. Please try after few minutes.';
               o_return_status := 'I';
               RETURN;
         END IF;
         --=====================================================
         -- IF return line is created before the Sold line is auto Invoiced then
         -- reference_customer_Trx_line_id is blank causing issue.Code updates the ref
         -- column for those order lines that are blank.
         for blank_rec in (select line_id,reference_line_id from apps.oe_order_lines_v 
                            where header_id=i_return_header_id
                              and reference_customer_trx_line_id is null
                              and reference_line_id is not null) -- Added by Shankar 31-Jan-2013 for performance issue 20130201-01010
          loop
            BEGIN
            select a.customer_trx_line_id
              into l_cust_trx_line_id
              from apps.ra_customer_Trx_lines a, apps.oe_order_headers b
             where a.line_type='LINE'
               and a.interface_line_attribute1=b.order_number
               and b.header_id=i_header_id
               and interface_line_attribute6=to_char(blank_rec.reference_line_id)
               and rownum=1;
               
             update oe_order_lines_all
                set reference_customer_trx_line_id=l_cust_trx_line_id, credit_invoice_line_id=l_cust_trx_line_id
              where line_id=blank_rec.line_id;  
             EXCEPTION
               when others then null;
             END;
          end loop;
          
          -- Oracle CC Disconnect Project Shankar Hariharan 04/11/2013
             IF i_payment_type_code = 'CHECK' then
               BEGIN
               select 'Y'
                 into l_cc_type
                 from apps.ar_cash_receipts a, apps.ar_receipt_methods b
                where a.receipt_method_id=b.receipt_method_id
                  and a.cash_receipt_id=i_cash_receipt_id
                  and (b.name like '%AMX%' or b.name like '%VMD%');
               EXCEPTION
                 when others then
                   l_cc_type := 'N';               
               END;  
             END IF;
          -- Oracle CC Disconnect Project Shankar Hariharan 04/11/2013 END
          --=====================================================
         -- Amount available for unapply and reapply
         IF l_applied_amt >= nvl(i_refund_amount,0)+nvl(i_check_refund_amount,0)
         THEN                                                          -- IF 2
         -- Check the number of payment method
           select count(1)
             into l_pay_count
             from apps.oe_payments
            where header_id=i_header_id;
            
         -- Check the number of invoices applied   
           SELECT COUNT (DISTINCT b.trx_number)
             INTO l_inv_count
             FROM apps.oe_order_lines_v a, apps.ra_customer_trx b, apps.ra_customer_trx_lines c
            WHERE a.reference_customer_trx_line_id IS NOT NULL
              AND c.customer_Trx_line_id = a.reference_customer_trx_line_id
              AND c.customer_Trx_id = b.customer_trx_id
              AND a.header_id = i_return_header_id;

           -- Get information from id for reporting
           SELECT order_number, ordered_date
             INTO l_return_order_number, l_return_order_date
             FROM apps.oe_order_headers
            WHERE header_id=i_return_header_id;

           SELECT order_number, ordered_date, customer_number, sold_to
             INTO l_orig_order_number, l_orig_order_date,l_customer_number, l_customer_name 
             FROM apps.oe_order_headers_v
            WHERE header_id=i_header_id;

           select receipt_number, receipt_date, amount
             into l_receipt_number, l_receipt_date, l_receipt_amount 
             from apps.ar_cash_receipts_v
            where cash_receipt_id=i_cash_receipt_id;
            -- If invoice count is 1, the process handles it
            -- if multiple invoice, then handled in the else condition
            -- multiple invoice / multiple payment method is restricted in the refund from
            -- and do not get into this process
            IF l_inv_count=1 THEN     --  IF 2.1
            -- Get the PS ID for the invoice applied to the receipt
            BEGIN
            SELECT applied_payment_schedule_id, amount_applied
              INTO l_app_ps_id, l_ps_applied_amt
              FROM apps.ar_receivable_applications_v
             WHERE     cash_receipt_id = i_cash_receipt_id
                   AND applied_payment_schedule_id > 0
                   AND applied_payment_schedule_id = 
                   (select distinct b.payment_schedule_id
                      from apps.ar_payment_schedules b, apps.ra_customer_trx c
                                ,ra_customer_trx_lines d, oe_order_lines_v e
                           where b.customer_trx_id=c.customer_trx_id
                             and c.customer_trx_id=d.customer_trx_id
                             and d.customer_trx_line_id=e.reference_customer_trx_line_id
                             and e.header_id=i_return_header_id
                   )
                   AND ROWNUM = 1;
             EXCEPTION
              when no_data_found then
                     o_return_status := 'E';
                     o_return_message := 'Unable to determine the Sold Order Invoice';
             END;
            -- Unapply the invoice from the receipt 
            AR_RECEIPT_API_PUB.UNAPPLY (
               p_api_version                   => 1.0
             , p_init_msg_list                 => 'T'
             , p_commit                        => 'F'
             , p_validation_level              => 100
             , x_return_status                 => l_return_status
             , x_msg_count                     => l_msg_count
             , x_msg_data                      => l_msg_data
             , p_cash_receipt_id               => i_cash_receipt_id
             , p_applied_payment_schedule_id   => l_app_ps_id
             , p_reversal_gl_date              => SYSDATE);

            l_debug := l_debug || 'Unapply status=' || l_return_status;

            -- IF success then proceed further
            IF l_return_status = 'S'
            THEN                                                        --IF 3
               l_new_applied_amt := l_ps_applied_amt - nvl(i_refund_amount,0)- nvl(i_check_refund_amount,0);
               -- Reapply the net amount 
               AR_RECEIPT_API_PUB.APPLY (
                  p_api_version                   => 1.0
                , p_init_msg_list                 => 'T'
                , p_commit                        => 'F'
                , p_validation_level              => 100
                , p_cash_receipt_id               => i_cash_receipt_id
                , p_applied_payment_schedule_id   => l_app_ps_id
                , p_amount_applied                => l_new_applied_amt
                , x_return_status                 => l_return_status
                , x_msg_count                     => l_msg_count
                , x_msg_data                      => l_msg_data);
               
               l_debug := l_debug || 'Reapply status=' || l_return_status;

               IF l_return_status = 'S'
               THEN                                                    -- IF 4
                  IF i_payment_type_code in ('CASH','CHECK')
                  THEN                                                 -- IF 5
                    -- Oracle CC Disconnect Project Shankar Hariharan 04/11/2013
                      IF i_payment_type_code = 'CHECK' and l_cc_type = 'Y' then
                       l_acc_app_ps_id := l_rec_wo_ps_id;
                       l_acc_app_rec_trx_id := l_cc_rec_wo_rec_trx_id;
                      ELSE
                    -- Oracle CC Disconnect Project Shankar Hariharan 04/11/2013 END
                       l_acc_app_ps_id := l_rec_wo_ps_id;
                       l_acc_app_rec_trx_id := l_rec_wo_rec_trx_id;
                      END IF;
                  ELSIF i_payment_type_code = 'CREDIT_CARD'
                  THEN                                               -- ELSE 5
                     l_acc_app_ps_id := l_cc_refund_ps_id;
                     l_acc_app_rec_trx_id := l_refund_rec_trx_id;
                  END IF;                                          -- END IF 5
                  -- if refund type is cash or credit card, apply the receivable trx type
                  -- No need to do anything for check refund request
                  -- Shankar 23-OCT-2012 CP 966
                  IF i_payment_type_code IN ('CASH','CHECK','CREDIT_CARD') and nvl(i_refund_amount,0)<> 0
                  THEN
                     l_attribute_rec.attribute1 := i_header_id;
                     l_attribute_rec.attribute2 := l_orig_order_number;
                     AR_RECEIPT_API_PUB.ACTIVITY_APPLICATION (
                        p_api_version                    => 1.0
                      , p_init_msg_list                  => 'T'
                      , p_commit                         => 'F'
                      , p_validation_level               => 100
                      , x_return_status                  => l_return_status
                      , x_msg_count                      => l_msg_count
                      , x_msg_data                       => l_msg_data
                      , p_cash_receipt_id                => i_cash_receipt_id
                      , p_applied_payment_schedule_id    => l_acc_app_ps_id
                      , p_receivables_trx_id             => l_acc_app_rec_trx_id
                      , p_amount_applied                 => i_refund_amount
                      , p_attribute_rec                  => l_attribute_rec
                      , p_receivable_application_id      => l_receivable_application_id
                      , p_application_ref_type           => l_application_ref_type
                      , p_application_ref_id             => l_application_ref_id
                      , p_application_ref_num            => l_application_ref_num
                      , p_secondary_application_ref_id   => l_secondary_application_ref_id);

                     l_debug :=
                        l_debug || 'Acc app status=' || l_return_status;
                  END IF;
                  -- If refund type is check or original payment method is check then notify 
                  -- Shankar CP 966 23-OCT-2012
                  IF --i_payment_type_code = 'CHECK' or 
                     nvl(i_check_refund_amount,0) <> 0 THEN
                   utl_mail.send(sender         => l_refund_email,
                                 recipients  => l_refund_email,
                                 cc          =>  NULL,
                                 bcc         => NULL,
                                 subject     => 'Check Refund Request for Order '|| l_return_order_number,
                                 message     => 'A Check Refund Request has been processed by '||fnd_global.user_name||' for Customer '||l_customer_number||':'||l_customer_name||chr(10)||chr(10)||
                                                'Order Information:'||chr(10)||'    Original Order Number: '||l_orig_order_number||chr(10)||
                                                '    Original Order Date: '||l_orig_order_date||chr(10)||'    Return Order Number: '||l_return_order_number||chr(10)||
                                                '    Return Order Date: '||l_return_order_date||chr(10)||chr(10)||'Receipt Information:'||chr(10)||
                                                '    Receipt Number: '||l_receipt_number||chr(10)||'    Receipt Date: '||l_receipt_date||chr(10)||
                                                '    Receipt Amount: '||l_receipt_amount||chr(10)||chr(10)||'Requested Check Refund: '||nvl(i_check_refund_amount,i_refund_amount),
                                 mime_type   => 'text/plain; charset=us-ascii',
                                 priority    => 3,
                                 replyto     => NULL);
                  END IF;
                  
                 -- Record the refund activity in the custom table
                  IF l_return_status = 'S'
                  THEN                                                 -- IF 6
                     INSERT INTO xxwc_om_cash_refund_tbl (header_id
                                                        , return_header_id
                                                        , cash_receipt_id
                                                        , refund_date
                                                        , refund_amount
                                                        , check_refund_amount
                                                        , payment_type_code
                                                        , process_flag
                                                        , creation_date
                                                        , created_by
                                                        , last_update_date
                                                        , last_updated_by
                                                        , last_update_login)
                          VALUES (i_header_id
                                , i_return_header_id
                                , i_cash_receipt_id
                                , SYSDATE
                                , nvl(i_refund_amount,0)
                                , nvl(i_check_refund_amount,0) 
                                , i_payment_type_code
                                , 'Y'
                                , SYSDATE
                                , fnd_global.user_id
                                , SYSDATE
                                , fnd_global.user_id
                                , -1);

                     o_return_status := 'S';
                     o_return_message := 'Cash Refund Processed Sucessfully';
                  ELSE                                               -- ELSE 6
                     IF l_msg_count = 1
                     THEN
                        NULL;
                     ELSIF l_msg_count > 1
                     THEN
                        LOOP
                           p_count := p_count + 1;
                           l_msg_data :=
                              FND_MSG_PUB.Get (FND_MSG_PUB.G_NEXT, 'F');
                           l_return_data := l_return_data||':'||l_msg_data;   

                           IF l_msg_data IS NULL
                           THEN
                              EXIT;
                           END IF;
                        END LOOP;
                     END IF;

                     o_return_status := l_return_status;
                     o_return_message := l_debug || ' ' || l_msg_data ||' '||l_return_data;
                  END IF;                                          -- END IF 6
               ELSE                                                  -- ELSE 4
                  --===============================================
                  IF l_msg_count = 1
                  THEN
                     NULL;
                  ELSIF l_msg_count > 1
                  THEN
                     LOOP
                        p_count := p_count + 1;
                        l_msg_data :=
                           FND_MSG_PUB.Get (FND_MSG_PUB.G_NEXT, 'F');

                        IF l_msg_data IS NULL
                        THEN
                           EXIT;
                        END IF;
                     END LOOP;
                  END IF;

                  --===============================================
                  o_return_status := l_return_status;
                  o_return_message := l_debug || ' ' || l_msg_data;
               END IF;                                             -- END IF 4
            ELSE                                                     -- ELSE 3
               --===============================================
               IF l_msg_count = 1
               THEN
                  NULL;
               ELSIF l_msg_count > 1
               THEN
                  LOOP
                     p_count := p_count + 1;
                     l_msg_data := FND_MSG_PUB.Get (FND_MSG_PUB.G_NEXT, 'F');

                     IF l_msg_data IS NULL
                     THEN
                        EXIT;
                     END IF;
                  --dbms_output.put_line('Message ' || p_count ||'. '||l_msg_data);
                  END LOOP;
               END IF;

               --===============================================
               o_return_status := l_return_status;
               o_return_message := l_debug || ' ' || l_msg_data;
            END IF;                                                -- END IF 3

          ELSE   -- Else 2.1
          -- IF more than one invoice then get the ps_id for those invoices
--===================================================================================
           for c1_rec in (select applied_payment_schedule_id, sum((e.ordered_quantity*e.unit_selling_Price)+e.tax_value) app_amt
                            from apps.ar_receivable_applications_v a, apps.ar_payment_schedules b, apps.ra_customer_trx c
                                ,ra_customer_trx_lines d, oe_order_lines_v e
                           where a.cash_receipt_id = i_cash_receipt_id
                             and a.applied_payment_schedule_id > 0
                             and a.applied_payment_schedule_id=b.payment_schedule_id
                             and b.customer_trx_id=c.customer_trx_id
                             and c.customer_trx_id=d.customer_trx_id
                             and d.customer_trx_line_id=e.reference_customer_trx_line_id
                             and e.header_id=i_return_header_id
                             group by applied_payment_schedule_id -- added by Shankar 01-04-2013
           )
           loop
           
            SELECT sum(amount_applied)
              INTO l_ps_applied_amt
              FROM apps.ar_receivable_applications_v
             WHERE cash_receipt_id = i_cash_receipt_id
               AND applied_payment_schedule_id =c1_rec.applied_payment_schedule_id;
            -- Unapply the original amount
            AR_RECEIPT_API_PUB.UNAPPLY (
               p_api_version                   => 1.0
             , p_init_msg_list                 => 'T'
             , p_commit                        => 'F'
             , p_validation_level              => 100
             , x_return_status                 => l_return_status
             , x_msg_count                     => l_msg_count
             , x_msg_data                      => l_msg_data
             , p_cash_receipt_id               => i_cash_receipt_id
             , p_applied_payment_schedule_id   => c1_rec.applied_payment_schedule_id --l_app_ps_id
             , p_reversal_gl_date              => SYSDATE);

            l_debug := l_debug || 'Unapply status=' || l_return_status;

            -- IF success then proceed further
            IF l_return_status = 'S'
            THEN                                                        --IF 3
               -- Reapply the net amount
               l_new_applied_amt := l_ps_applied_amt - c1_rec.app_amt;
               -- i_refund_amount- nvl(i_check_refund_amount,0);
               
               AR_RECEIPT_API_PUB.APPLY (
                  p_api_version                   => 1.0
                , p_init_msg_list                 => 'T'
                , p_commit                        => 'F'
                , p_validation_level              => 100
                , p_cash_receipt_id               => i_cash_receipt_id
                , p_applied_payment_schedule_id   => c1_rec.applied_payment_schedule_id --l_app_ps_id
                , p_amount_applied                => l_new_applied_amt
                , x_return_status                 => l_return_status
                , x_msg_count                     => l_msg_count
                , x_msg_data                      => l_msg_data);
               

               l_debug := l_debug || 'Reapply status=' || l_return_status;

               IF l_return_status = 'S'
               THEN                                                    -- IF 4
                  IF i_payment_type_code in ( 'CASH','CHECK')
                  THEN                                                 -- IF 5
                   -- Oracle CC Disconnect Project Shankar Hariharan 04/11/2013
                      IF i_payment_type_code = 'CHECK' and l_cc_type = 'Y' then
                       l_acc_app_ps_id := l_rec_wo_ps_id;
                       l_acc_app_rec_trx_id := l_cc_rec_wo_rec_trx_id;
                      ELSE
                    -- Oracle CC Disconnect Project Shankar Hariharan 04/11/2013 END
                       l_acc_app_ps_id := l_rec_wo_ps_id;
                       l_acc_app_rec_trx_id := l_rec_wo_rec_trx_id;
                      END IF; 
                  ELSIF i_payment_type_code = 'CREDIT_CARD'
                  THEN                                               -- ELSE 5
                     l_acc_app_ps_id := l_cc_refund_ps_id;
                     l_acc_app_rec_trx_id := l_refund_rec_trx_id;
                  END IF;                                          -- END IF 5

               ELSE                                                  -- ELSE 4
                  --===============================================
                  IF l_msg_count = 1
                  THEN
                     NULL;
                  ELSIF l_msg_count > 1
                  THEN
                     LOOP
                        p_count := p_count + 1;
                        l_msg_data :=
                           FND_MSG_PUB.Get (FND_MSG_PUB.G_NEXT, 'F');

                        IF l_msg_data IS NULL
                        THEN
                           EXIT;
                        END IF;
                     END LOOP;
                  END IF;

                  --===============================================
                  o_return_status := l_return_status;
                  o_return_message := l_debug || ' ' || l_msg_data;
                  RETURN;
               END IF;                                             -- END IF 4
            ELSE                                                     -- ELSE 3
               --===============================================
               IF l_msg_count = 1
               THEN
                  NULL;
               ELSIF l_msg_count > 1
               THEN
                  LOOP
                     p_count := p_count + 1;
                     l_msg_data := FND_MSG_PUB.Get (FND_MSG_PUB.G_NEXT, 'F');

                     IF l_msg_data IS NULL
                     THEN
                        EXIT;
                     END IF;
                  --dbms_output.put_line('Message ' || p_count ||'. '||l_msg_data);
                  END LOOP;
               END IF;

               --===============================================
               o_return_status := l_return_status;
               o_return_message := l_debug || ' ' || l_msg_data;
               --ROLLBACK;
               RETURN;
            END IF;                                                -- END IF 3
           END LOOP;
            -- Apply activity for the refund amount
            --Shankar CP 966 23-OCT-2012
              IF i_payment_type_code IN ('CASH','CHECK','CREDIT_CARD') and nvl(i_refund_amount,0) <>0
                  THEN
                     l_attribute_rec.attribute1 := i_header_id;
                     l_attribute_rec.attribute2 := l_orig_order_number;
                     AR_RECEIPT_API_PUB.ACTIVITY_APPLICATION (
                        p_api_version                    => 1.0
                      , p_init_msg_list                  => 'T'
                      , p_commit                         => 'F'
                      , p_validation_level               => 100
                      , x_return_status                  => l_return_status
                      , x_msg_count                      => l_msg_count
                      , x_msg_data                       => l_msg_data
                      , p_cash_receipt_id                => i_cash_receipt_id
                      , p_applied_payment_schedule_id    => l_acc_app_ps_id
                      , p_receivables_trx_id             => l_acc_app_rec_trx_id
                      , p_amount_applied                 => i_refund_amount
                      , p_attribute_rec                  => l_attribute_rec
                      , p_receivable_application_id      => l_receivable_application_id
                      , p_application_ref_type           => l_application_ref_type
                      , p_application_ref_id             => l_application_ref_id
                      , p_application_ref_num            => l_application_ref_num
                      , p_secondary_application_ref_id   => l_secondary_application_ref_id);
                     l_debug :=
                        l_debug || 'Acc app status=' || l_return_status;
                  END IF;
                  -- Notify for check refund
                  --Shankar CP 966 23-OCT-2012
                  IF --i_payment_type_code = 'CHECK' or
                   nvl(i_check_refund_amount,0) <> 0 THEN
                   utl_mail.send(sender         => l_refund_email,
                                 recipients  => l_refund_email,
                                 cc          =>  NULL,
                                 bcc         => NULL,
                                 subject     => 'Check Refund Request for Order '|| l_return_order_number,
                                 message     => 'A Check Refund Request has been processed by '||fnd_global.user_name||' for Customer '||l_customer_number||':'||l_customer_name||chr(10)||chr(10)||
                                                'Order Information:'||chr(10)||'    Original Order Number: '||l_orig_order_number||chr(10)||
                                                '    Original Order Date: '||l_orig_order_date||chr(10)||'    Return Order Number: '||l_return_order_number||chr(10)||
                                                '    Return Order Date: '||l_return_order_date||chr(10)||chr(10)||'Receipt Information:'||chr(10)||
                                                '    Receipt Number: '||l_receipt_number||chr(10)||'    Receipt Date: '||l_receipt_date||chr(10)||
                                                '    Receipt Amount: '||l_receipt_amount||chr(10)||chr(10)||'Requested Check Refund: '||nvl(i_check_refund_amount,i_refund_amount),
                                 mime_type   => 'text/plain; charset=us-ascii',
                                 priority    => 3,
                                 replyto     => NULL);
                  END IF;

                 -- Record refund in custom table
                  IF l_return_status = 'S'
                  THEN                                                 -- IF 6
                     INSERT INTO xxwc_om_cash_refund_tbl (header_id
                                                        , return_header_id
                                                        , cash_receipt_id
                                                        , refund_date
                                                        , refund_amount
                                                        , check_refund_amount
                                                        , payment_type_code
                                                        , process_flag
                                                        , creation_date
                                                        , created_by
                                                        , last_update_date
                                                        , last_updated_by
                                                        , last_update_login)
                          VALUES (i_header_id
                                , i_return_header_id
                                , i_cash_receipt_id
                                , SYSDATE
                                , i_refund_amount
                                , nvl(i_check_refund_amount,0) 
                                , i_payment_type_code
                                , 'Y'
                                , SYSDATE
                                , fnd_global.user_id
                                , SYSDATE
                                , fnd_global.user_id
                                , -1);

                     o_return_status := 'S';
                     o_return_message := 'Cash refund process sucessful';
                  ELSE                                               -- ELSE 6
                     IF l_msg_count = 1
                     THEN
                        NULL;
                     ELSIF l_msg_count > 1
                     THEN
                        LOOP
                           p_count := p_count + 1;
                           l_msg_data :=
                              FND_MSG_PUB.Get (FND_MSG_PUB.G_NEXT, 'F');

                           IF l_msg_data IS NULL
                           THEN
                              EXIT;
                           END IF;
                        END LOOP;
                     END IF;

                     o_return_status := l_return_status;
                     o_return_message := l_debug || ' ' || l_msg_data;
                  END IF;                                          -- END IF 6
--===================================================================================
          END IF; -- End If 2.1
         ELSE                                                        -- ELSE 2
            o_return_message :=
                  'Refund Amount of '
               || i_refund_amount
               || ' is greater than receipt applied amount '
               || l_applied_amt;
            o_return_status := 'E';
         END IF;                                                   -- END IF 2
      END IF;                                                      -- END IF 1
/*    
      update AR_SYSTEM_PARAMETERS
         set rule_set_id = 2;  */   
   END cash_refund_process;
   
PROCEDURE launch_auto_invoice(i_sales_order in VARCHAR2, o_return_message out varchar2)
   /*************************************************************************
   *   $Header xxwc_om_cash_refund_pkg.sql $
   *   Module Name: launch_auto_invoice
   *
   *   PURPOSE:   Used in extension Cash Refunds process
   *
   *   REVISIONS:
   *   Ver        Date        Author                     Description
   *   ---------  ----------  ---------------         -------------------------
   *   1.0        04/01/2012  Shankar Hariharan             Initial Version
   *   1.9        03/07/2016  Manjula Chellappan            TMS# 20150527-00318 Forego Two AR Triggers
   * ***************************************************************************/
is
l_request_id number;
l_source_id number;
l_set_print_options boolean;
l_batch_source_name varchar2(100);
l_header_id number;
l_errbuf  varchar2(2000);
l_retcode  varchar2(2000);
l_pre_request_id NUMBER; -- Added for Ver 1.9 
-- Launched from XXWC_OM_REFUND from for an order that is being returned the same day
-- Invoices need to be created first before the refunds can be processed.
BEGIN
/*
  begin
    select batch_source_id
      into l_source_id
      from RA_BATCH_SOURCES_ALL
     where name = 'ORDER MANAGEMENT'
       and org_id=xxwc_ascp_scwb_pkg.get_wc_org_id; */
  -- Added by Shankar 09-Sep-2012   

--Added for Ver 1.9 Begin
     l_pre_request_id :=  fnd_request.submit_request 
                                      ('XXWC',
                                        'XXWC_AR_INV_PREPROCESS',
                                        NULL,
                                        NULL,
                                        FALSE,
                                        NULL);

    IF l_pre_request_id	= 0 THEN 
		o_return_message := 'Failed to Submit XXWC AR Invoice Preprocessor, Autoinoice will not be submitted';
    ELSE 		
        commit; 
--Added for Ver 1.9 End
  
  begin
   select a.invoice_source_id, c.name,b.header_id
     into l_source_id, l_batch_source_name, l_header_id
     from apps.oe_transaction_types a, apps.oe_order_headers b, apps.ra_batch_sources c
    where b.order_number=i_sales_order
      and a.transaction_type_id=b.order_type_id
      and a.invoice_source_id=c.batch_source_id;
  exception
   when others then 
     l_source_id := null;
  end;
 -- TMS Task 20130703-00901  Shankar Hariharan 08-Jul-2013
  IF l_header_id is not null then
--  xxwc_ont_routines_pkg.xxwc_rel_price_chg_hold_prc(l_errbuf, l_retcode,l_header_id);
  xxwc_ont_routines_pkg.xxwc_rel_price_chg_hold_prc(l_errbuf, l_retcode,l_header_id,NULL,NULL,NULL); -- Version# 1.7
  END IF;
-- ADded to avoid printing invoices at the branch 
l_set_print_options := FND_Request.set_print_options(printer=>'noprint',copies=>0);  
  
l_request_id := fnd_request.submit_request(
--1, -99, 1002, ORDER MANAGEMENT, 2012/03/28 00:00:00, , , , , , , , , , , , , 10001627, 10001627, , , , , , , Y,
              application => 'AR',
              program     => 'RAXMTR',
              description => 'Autoinvoice master program from OM',
              start_time  => sysdate,
              sub_request => null,
              argument1   => '1',
              argument2   => '-99',
              argument3   => to_char(l_source_id),
              argument4   => l_batch_source_name,
              argument5   => to_char(sysdate,'YYYY/MM/DD HH24:MI:SS'),
              argument6   => null,
              argument7   => null,
              argument8   => null,
              argument9   => null,
              argument10  => null,
              argument11  => null,
              argument12  => null,
              argument13  => null,
              argument14  => null,
              argument15  => null,
              argument16  => null,
              argument17  => null,
              argument18  => i_sales_order,
              argument19  => i_sales_order,
              argument20  => null,
              argument21  => null,
              argument22  => null,
              argument23  => null,
              argument24  => null,
              argument25  => null,
              argument26  => 'Y',
              argument27  => null);
    IF l_request_id <> 0 then
      o_return_message := 'Invoice Process submitted successfully with request id='||l_request_id||
      '. Do not press this button again until the process has completed. Monitor processing through the Requests Concurrent Manager form';
      commit;
      return;
    ELSE
      o_return_message :=  'Issue submitting Invoice Process';
      rollback;
      return; 
    END IF;   

    END IF ; --Added for Ver 1.9 	
END launch_auto_invoice;

-- Function called from personalization the sales order from to restrict the 
-- message to appear only for those orders for which prepayment exists
FUNCTION prepayment_exists (i_header_id in number)
RETURN VARCHAR
is
l_header_id number;
l_line_id number;
l_exist varchar2(1);
BEGIN
      BEGIN
         SELECT return_attribute1, return_attribute2
           INTO l_header_id, l_line_id
           FROM apps.oe_order_lines
          WHERE     header_id = i_header_id
                AND return_attribute1 IS NOT NULL
                AND return_attribute2 IS NOT NULL
                AND ROWNUM = 1;
      EXCEPTION
         WHEN OTHERS
         THEN
            null;
      END;
      
      IF l_header_id is null then 
        return('N');
      ELSE
       BEGIN
       select 'Y'
         into l_exist
         from apps.oe_payments
        where header_id=l_header_id
          and rownum=1;
        return('Y');
       EXCEPTION
         when others then
           return('N');
       END;
      END IF;
END prepayment_exists;    
END xxwc_ar_cash_refund_pkg;
/