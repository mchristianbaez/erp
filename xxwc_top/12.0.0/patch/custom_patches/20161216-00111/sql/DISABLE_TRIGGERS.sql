   /************************************************************************* 
   *    Date         Author                Description
   *    ----------   ---------------       -------------------------
   *    09-Sep-2015  Manjula Chellappan    TMS# 20150527-00318 Forego Two AR Triggers
   * ***************************************************************************/
   
DROP TRIGGER XXWC_TAXW_INVOICE_INSERT_TRG ;
   
DROP TRIGGER XXWC_RA_INTERFACE_LINES_ALL_BI ;