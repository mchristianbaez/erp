/*
 TMS: 20170908-00063
 Scope: i-expense data fix
 Date: 09/08/2017
*/
set serveroutput on size 1000000;
declare
 --
 l_program_1 varchar2(150) :='XXWC Send Notifications for Purchasing Documents'; 
 l_conc_program_id number :=0;
 l_app_id number :=0;
 l_sc_userid number :=15985; --Supply chain generic user XXWC_INT_SUPPLYCHAIN
 l_om_userid number :=15986; --Order managenent generic user XXWC_INT_SALESFULFILLMENT
 l_fin_userid number :=1290; --WC Finance generic user XXWC_INT_FINANCE
 --
 procedure print_log(p_message in varchar2) is
 begin
  if apps.fnd_global.conc_request_id >0 then
   apps.fnd_file.put_line(fnd_file.log, p_message);
  else
   dbms_output.put_line(p_message);
  end if;
 end;
 --
begin
     --
     savepoint square1;
     --         
     begin 
            --
            print_log('Before update of ap expense report lines for report header id 795756.');
            --
            update ap.ap_expense_report_lines_all
            set org_id =163
            where 1 =1
            and report_header_id =795756;
            --
            print_log(' ');
            print_log('After update of ap expense report lines for report header id 795756, rows updated : '||sql%rowcount);
            --
            commit;
            --                        
     exception      
      when others then
       rollback to square1;
       print_log('When-others, error ='||sqlerrm);       
     end;
    --  
exception
 when others then
  rollback to square1;
  print_log('Outer block, message ='||sqlerrm);
end;
/