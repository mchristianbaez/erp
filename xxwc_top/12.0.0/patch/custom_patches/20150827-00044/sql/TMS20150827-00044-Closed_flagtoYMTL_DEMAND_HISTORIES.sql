/*
 TMS: 20150827-00044 
 Date: 08/27/2015
 */
SET SERVEROUTPUT ON SIZE 1000000
DECLARE
v_count NUMBER;

BEGIN
   DBMS_OUTPUT.put_line ('TMS:20150827-00044 , Script 1 -Before Update');

   BEGIN
      SELECT COUNT (1)
        INTO v_count
        FROM mtl_demand_histories
       WHERE closed_flag IS NULL;

      UPDATE mtl_demand_histories
         SET closed_flag = 'Y'
       WHERE closed_flag IS NULL;
   EXCEPTION
      WHEN OTHERS
      THEN
         DBMS_OUTPUT.PUT_LINE (
               'Error in updating closed_flag to mtl_demand_histories-'
            || SQLERRM);
   END;

   COMMIT;

   DBMS_OUTPUT.put_line (
         'TMS: 20150827-00044, Script 1 -After update, rows updated: '
      || v_count);
EXCEPTION
   WHEN OTHERS
   THEN
      DBMS_OUTPUT.PUT_LINE ('Error is' || SQLERRM);
END;
/
