CREATE OR REPLACE PACKAGE BODY APPS.XXWC_EGO_CUSTOMER_CATALOG_PKG
/******************************************************************************************************
-- File Name: XXWC_EGO_CUSTOMER_CATALOG_PKG.pkb
--
-- PROGRAM TYPE: PL/SQL Script   <API>
--
-- PURPOSE: Developed to Extract PDH Catalog data for required customer
-- HISTORY
-- ==========================================================================================================
-- ==========================================================================================================
-- VERSION DATE          AUTHOR(S)       DESCRIPTION
-- ------- -----------   --------------- --------------------------------------------------------------------
-- 1.0     17-FEB-2016   P.Vamshidhar   TMS#20160219-00063 - External Customer Catalog Extract
   1.1     06-JUN-2016   P.Vamshidhar   TMS#20160219-00063 - External Customer Catalog Extract
                                        Post Production issues fixed.
************************************************************************************************************/
IS
   g_sec            VARCHAR2 (1000);
   g_err_callfrom   VARCHAR2 (100) := 'XXWC_EGO_CUSTOMER_CATALOG_PKG';
   g_distro_list    VARCHAR2 (100) := 'HDSOracleDevelopers@hdsupply.com';
   g_procedure      VARCHAR2 (100);

   PROCEDURE Generate_txt (x_err_buf               OUT VARCHAR2,
                           x_ret_code              OUT VARCHAR2,
                           p_export_type        IN     VARCHAR2,
                           p_customer_id        IN     NUMBER,
                           p_customer_catalog   IN     VARCHAR2,
                           p_extract_type       IN     VARCHAR2,
                           p_from_date          IN     VARCHAR2)
   /******************************************************************************************************
   -- PROGRAM TYPE: Procedure
   -- Name: Generate_txt
   -- PURPOSE: Developed to Extract PDH Catalog data for required customer
   -- HISTORY
   -- ==========================================================================================================
   -- ==========================================================================================================
   -- VERSION DATE          AUTHOR(S)       DESCRIPTION
   -- ------- -----------   --------------- --------------------------------------------------------------------
   -- 1.0     17-FEB-2016   P.Vamshidhar   TMS#20160219-00063 - External Customer Catalog Extract
   -- 1.1     06-JUN-2016   P.Vamshidhar   TMS#20160219-00063 - External Customer Catalog Extract
                                           Post Production issues fixed.
   ************************************************************************************************************/

   IS
      -- re-wrote below query in 1.1
      CURSOR CUR_ITEM_CATLOG (
         cp_customer_id        VARCHAR2,
         p_customer_catalog    VARCHAR2,
         ld_from_date          DATE)
      IS
         SELECT DISTINCT ITEM_NUMBER
           FROM XXWC.XXWC_B2B_CAT_CUSTOMER_TBL xct
          WHERE     NVL (cp_customer_id, xct.CUSTOMER_ID) = xct.CUSTOMER_ID
                AND NVL (UPPER (P_CUSTOMER_CATALOG),
                         UPPER (xct.CUST_CATALOG_NAME)) =
                       UPPER (xct.CUST_CATALOG_NAME)
                AND EXISTS
                       (SELECT 1
                          FROM APPS.MTL_SYSTEM_ITEMS_B msib
                         WHERE     msib.LAST_UPDATE_DATE >=
                                      NVL (ld_from_date,
                                           msib.LAST_UPDATE_DATE)
                               AND msib.organization_id = 222
                               AND msib.inventory_item_id =
                                      xct.inventory_item_id);


      CURSOR CUR_EGO_DATA (
         P_ITEM_NUMBER    VARCHAR2,
         P_IMAGE_URL      VARCHAR2,
         p_extrat_type    VARCHAR2,
         p_customer_id    NUMBER)
      IS
           SELECT B.INVENTORY_ITEM_ID,
                  2 SORTORDER,
                  B.SEGMENT1 item_number,
                  C.SEGMENT1 Catalog_name,
                  A.ATTR_GROUP_DISP_NAME,
                  a.ATTRIBUTE_GROUP_NAME,
                  a.ATTR_DISPLAY_NAME,
                  a.ATTRIBUTE_NAME,
                  COALESCE (
                     DECODE (
                        a.ATTRIBUTE_NAME,
                        'XXWC_A_F_1_URL_ATTR',    P_IMAGE_URL
                                               || '/'
                                               || SUBSTR (
                                                     ATTRIBUTE_CHAR_VALUE,
                                                       INSTR (
                                                          ATTRIBUTE_CHAR_VALUE,
                                                          'FullImage',
                                                          1)
                                                     + 10),
                        ATTRIBUTE_CHAR_VALUE),
                     TO_CHAR (ATTRIBUTE_NUMBER_VALUE),
                     to_char(TO_date(ATTRIBUTE_DATE_VALUE, 'mm/dd/yyyy'),'DD-MON-YYYY'))
                     ATTRIBUTE_VALUE
             FROM APPS.XXWC_EGO_EXTN_CUST_ATTR_V A,
                  APPS.MTL_SYSTEM_ITEMS_B B,
                  MTL_ITEM_CATALOG_GROUPS_B_KFV C
            WHERE     A.INVENTORY_ITEM_ID = B.INVENTORY_ITEM_ID
                  AND A.ORGANIZATION_ID = B.ORGANIZATION_ID
                  AND b.ITEM_CATALOG_GROUP_ID = C.ITEM_CATALOG_GROUP_ID
                  AND B.SEGMENT1 = P_ITEM_NUMBER
                  AND (   (p_extrat_type = 'Full Extract')
                       OR (    p_extrat_type = 'Data Extract'
                           AND (   A.ATTRIBUTE_CHAR_VALUE IS NOT NULL
                                OR A.ATTRIBUTE_NUMBER_VALUE IS NOT NULL
                                OR A.ATTRIBUTE_DATE_VALUE IS NOT NULL))
                       OR (    p_extrat_type = 'Blank Extract'
                           AND A.ATTRIBUTE_CHAR_VALUE IS NULL
                           AND A.ATTRIBUTE_NUMBER_VALUE IS NULL
                           AND A.ATTRIBUTE_DATE_VALUE IS NULL))
                  AND (   p_customer_id IS NULL
                       OR (    p_customer_id IS NOT NULL
                           AND NOT EXISTS
                                  (SELECT 1
                                     FROM XXWC.XXWC_B2B_CAT_ATTR_TBL XCAT
                                    WHERE     p_customer_id = XCAT.CUSTOMER_ID
                                          AND A.ATTRIBUTE_ID = XCAT.ATTR_ID)))
         ORDER BY B.INVENTORY_ITEM_ID,
                  a.ATTRIBUTE_GROUP_NAME,
                  a.ATTRIBUTE_NAME;

      CURSOR CUR_ITEM_MAST (
         P_ITEM_NUMBER    VARCHAR2)
      IS
         SELECT MSIB.INVENTORY_ITEM_ID,
                MSIB.DESCRIPTION,
                MSIB.SEGMENT1 ITEM_NUMBER,
                MSIB.PRIMARY_UOM_CODE UOM,
                REPLACE (REPLACE (MSIB.LONG_DESCRIPTION, CHR (10), NULL),
                         CHR (13),
                         NULL)
                   LONG_DESCRIPTION,
                MSIB.SHELF_LIFE_DAYS,
                MSIB.ATTRIBUTE22 TAXWARE_CODE,
                MSIB.SEGMENT1 MSDS,
                MSIB.UNIT_WEIGHT SHIPPING_WEIGHT,
                MSIB.HAZARDOUS_MATERIAL_FLAG HAZMAT_FLAG,
                DECODE (MSIB.HAZARDOUS_MATERIAL_FLAG, 'Y', 'Yes', 'No')
                   Hazmat,
                MSIB.FIXED_LOT_MULTIPLIER,
                (SELECT CATEGORY_CONCAT_SEGS
                   FROM apps.MTL_ITEM_CATEGORIES_V
                  WHERE     inventory_item_id = msib.inventory_item_id
                        AND organization_id = 222
                        AND CATEGORY_SET_NAME = 'Inventory Category')
                   Cat_Class,
                (SELECT CATEGORY_CONCAT_SEGS
                   FROM apps.MTL_ITEM_CATEGORIES_V
                  WHERE     inventory_item_id = msib.inventory_item_id
                        AND organization_id = 222
                        AND CATEGORY_SET_NAME = 'WC Web Hierarchy')
                   WC_Web_Hierarchy,
                (SELECT CATEGORY_CONCAT_SEGS
                   FROM apps.MTL_ITEM_CATEGORIES_V
                  WHERE     inventory_item_id = msib.inventory_item_id
                        AND organization_id = 222
                        AND CATEGORY_SET_NAME = 'Time Sensitive')
                   Time_Sensitive
           FROM APPS.MTL_SYSTEM_ITEMS_VL MSIB
          WHERE     MSIB.ORGANIZATION_ID = 222
                AND MSIB.INVENTORY_ITEM_STATUS_CODE NOT IN ('Inactive',
                                                            'Discontinu',
                                                            'New Item')
                AND MSIB.SEGMENT1 = P_ITEM_NUMBER;

      CURSOR CUR_CREFF_MFG (
         p_inventory_item_id    NUMBER)
      IS
         SELECT MCR.CROSS_REFERENCE, MCR.ATTRIBUTE1, aps.vendor_name
           FROM APPS.MTL_CROSS_REFERENCES mcr, apps.ap_suppliers aps
          WHERE     CROSS_REFERENCE_TYPE = 'VENDOR'
                AND mcr.INVENTORY_ITEM_ID = p_inventory_item_id
                AND MCR.ATTRIBUTE1 = aps.segment1(+);

      CURSOR CUR_CREF_UPC (
         p_inventory_item_id    NUMBER)
      IS
         SELECT MCR.CROSS_REFERENCE, MCR.ATTRIBUTE1
           FROM APPS.MTL_CROSS_REFERENCES mcr
          WHERE     mcr.CROSS_REFERENCE_TYPE = 'UPC'
                AND mcr.INVENTORY_ITEM_ID = p_inventory_item_id;

      x_file_path        VARCHAR2 (100);
      lvc_file_name      VARCHAR2 (100);
      lvc_file           UTL_FILE.FILE_TYPE;
      lvc_outbound_dir   VARCHAR2 (100) := 'XXWC_INV_CUSTOMER_CATALOG';
      MAX_LINE_LENGTH    BINARY_INTEGER := 32767;
      lvc_string         VARCHAR2 (3000) := NULL;
      ln_count           NUMBER := 1;
      ln_mast_org        NUMBER;
      EXECUTION_ERROR    EXCEPTION;
      ln_upc_count       NUMBER;
      ln_vendor_count    NUMBER;
      l_err_msg          VARCHAR2 (2000);
      l_customer_name    VARCHAR2 (2000);
      lvc_msds_url       VARCHAR2 (1000)
         := FND_PROFILE.VALUE ('XXWC_CUST_CATALOG_SITEHAWK_URL');
      lvc_images_url     VARCHAR2 (1000)
         := FND_PROFILE.VALUE ('XXWC_INV_WC_WEBSITE_IMAGE_URL');
      lp_from_date       DATE
                            := TO_DATE (P_FROM_DATE, 'YYYY/MM/DD HH24:MI:SS');
      lvc_conc_program   VARCHAR2 (100)
                            := 'XXWC Customer Catalog Extract Program';
      ln_request_id      NUMBER := FND_GLOBAL.CONC_REQUEST_ID;
   BEGIN
      -- Initializing variables
      g_sec := 'Generate_txt - Procedure  - Start';
      g_procedure := 'Generate_txt';

      Fnd_file.put_line (fnd_file.LOG, g_sec);
      fnd_file.put_line (fnd_file.LOG, 'Parameters ');
      fnd_file.put_line (fnd_file.LOG, 'Export Type  : ' || p_export_type);
      fnd_file.put_line (fnd_file.LOG, 'Customer Id  : ' || p_customer_id);
      fnd_file.put_line (fnd_file.LOG,
                         'Cust Catalog : ' || p_customer_catalog);
      fnd_file.put_line (fnd_file.LOG, 'Extract Type : ' || p_extract_type);
      fnd_file.put_line (fnd_file.LOG, 'From Date    : ' || p_from_date);
      fnd_file.put_line (fnd_file.LOG, 'lp_from_date : ' || lp_from_date);

      -- Date validation

      IF P_FROM_DATE IS NULL
      THEN
         BEGIN
            SELECT MAX (ACTUAL_COMPLETION_DATE)
              INTO lp_from_date
              FROM fnd_conc_req_summary_v
             WHERE     USER_CONCURRENT_PROGRAM_NAME = lvc_conc_program
                   AND STATUS_CODE = 'C'
                   AND request_id < ln_request_id;
         EXCEPTION
            WHEN OTHERS
            THEN
               lp_from_date := NULL;
         END;
      END IF;

      fnd_file.put_line (fnd_file.LOG,
                         'Final lp_from_date : ' || lp_from_date);

      IF p_customer_id IS NOT NULL
      THEN
         BEGIN
            SELECT SUBSTR (party_name, 1, 50)
              INTO l_customer_name
              FROM apps.hz_parties
             WHERE party_id = p_customer_id;

            fnd_file.put_line (fnd_file.LOG,
                               'Customer Name ' || l_customer_name);
         EXCEPTION
            WHEN OTHERS
            THEN
               l_customer_name := 'Customer_Catalog_Extract';
         END;
      ELSE
         l_customer_name := 'Customer_Catalog_Extract';
      END IF;

      lvc_file_name :=
            REPLACE (TRANSLATE (l_customer_name, '()&/*-+', '_'), ' ', '_')
         || TO_CHAR (SYSDATE, 'MMDDYYYYHH24MISS')
         || '.dat';

      fnd_file.put_line (fnd_file.LOG, 'lvc_file_name ' || lvc_file_name);

      g_sec := 'Opening file at OS level';
      -- Opening File
      lvc_file :=
         UTL_FILE.FOPEN (lvc_outbound_dir,
                         lvc_file_name,
                         'W',
                         MAX_LINE_LENGTH);

      -- Writing Header
      lvc_string :=
         '|ITEM_NUMBER|ICC|ATTR_GROUP_DISP_NAME|ATTR_DISPLAY_NAME|ATTRIBUTE_VALUE|';
      UTL_FILE.PUT_LINE (lvc_file, lvc_string);

      g_sec := 'Data Extraction start';


      FOR REC_ITEM_CATLOG
         IN CUR_ITEM_CATLOG (p_customer_id, p_customer_catalog, lp_from_date)
      LOOP
         g_sec := 'Catalog level';

         FOR REC_ITEM_MAST IN CUR_ITEM_MAST (REC_ITEM_CATLOG.ITEM_NUMBER)
         LOOP
            g_sec := REC_ITEM_MAST.ITEM_NUMBER || ' Item Stated';
            fnd_file.put_line (fnd_file.LOG, g_sec);


            IF p_extract_type IN ('Full Extract', 'Data Extract')
            THEN
               -- Description attribute
               lvc_string := NULL;
               lvc_string :=
                     '|'
                  || REC_ITEM_MAST.ITEM_NUMBER
                  || '|MASTER|ATTRIBUTE|Description|'
                  || REC_ITEM_MAST.DESCRIPTION
                  || '|';
               UTL_FILE.PUT_LINE (lvc_file, lvc_string);
            END IF;

            --UOM Attribute
            IF p_extract_type = 'Full Extract'
            THEN
               lvc_string := NULL;
               lvc_string :=
                     '|'
                  || REC_ITEM_MAST.ITEM_NUMBER
                  || '|MASTER|ATTRIBUTE|UOM|'
                  || REC_ITEM_MAST.UOM
                  || '|';
               UTL_FILE.PUT_LINE (lvc_file, lvc_string);

               --LONG_DESCRIPTION Attribute
               lvc_string := NULL;
               lvc_string :=
                     '|'
                  || REC_ITEM_MAST.ITEM_NUMBER
                  || '|MASTER|ATTRIBUTE|Long Description|'
                  || REC_ITEM_MAST.LONG_DESCRIPTION
                  || '|';
               UTL_FILE.PUT_LINE (lvc_file, lvc_string);

               -- Shelf life days Attribute
               lvc_string := NULL;
               lvc_string :=
                     '|'
                  || REC_ITEM_MAST.ITEM_NUMBER
                  || '|MASTER|ATTRIBUTE|Shelf Life Days|'
                  || REC_ITEM_MAST.SHELF_LIFE_DAYS
                  || '|';
               UTL_FILE.PUT_LINE (lvc_file, lvc_string);

               --TAXWARE_CODE Attribute
               lvc_string := NULL;
               lvc_string :=
                     '|'
                  || REC_ITEM_MAST.ITEM_NUMBER
                  || '|MASTER|ATTRIBUTE|Taxware Code|'
                  || REC_ITEM_MAST.TAXWARE_CODE
                  || '|';
               UTL_FILE.PUT_LINE (lvc_file, lvc_string);

               -- SHIPPING_WEIGHT Attribute
               lvc_string := NULL;
               lvc_string :=
                     '|'
                  || REC_ITEM_MAST.ITEM_NUMBER
                  || '|MASTER|ATTRIBUTE|Shipping Weight|'
                  || REC_ITEM_MAST.SHIPPING_WEIGHT
                  || '|';
               UTL_FILE.PUT_LINE (lvc_file, lvc_string);

               -- HAZMAT Attribute
               lvc_string := NULL;
               lvc_string :=
                     '|'
                  || REC_ITEM_MAST.ITEM_NUMBER
                  || '|MASTER|ATTRIBUTE|Hazardous Material|'
                  || REC_ITEM_MAST.Hazmat
                  || '|';
               UTL_FILE.PUT_LINE (lvc_file, lvc_string);

               -- Fixed Lot Multiplier Attribute
               lvc_string := NULL;
               lvc_string :=
                     '|'
                  || REC_ITEM_MAST.ITEM_NUMBER
                  || '|MASTER|ATTRIBUTE|Fixed Lot Multiplier|'
                  || REC_ITEM_MAST.FIXED_LOT_MULTIPLIER
                  || '|';
               UTL_FILE.PUT_LINE (lvc_file, lvc_string);

               -- Category Class
               lvc_string := NULL;
               lvc_string :=
                     '|'
                  || REC_ITEM_MAST.ITEM_NUMBER
                  || '|MASTER|ATTRIBUTE|Category Class|'
                  || REC_ITEM_MAST.Cat_Class
                  || '|';
               UTL_FILE.PUT_LINE (lvc_file, lvc_string);

               -- WC Web Hierarchy Attribute
               lvc_string := NULL;
               lvc_string :=
                     '|'
                  || REC_ITEM_MAST.ITEM_NUMBER
                  || '|MASTER|ATTRIBUTE|WC Web Hierarchy|'
                  || REC_ITEM_MAST.WC_Web_Hierarchy
                  || '|';
               UTL_FILE.PUT_LINE (lvc_file, lvc_string);

               -- TIME SENSITIVE Attribute
               lvc_string := NULL;
               lvc_string :=
                     '|'
                  || REC_ITEM_MAST.ITEM_NUMBER
                  || '|MASTER|ATTRIBUTE|Time Sensitive|'
                  || REC_ITEM_MAST.Time_Sensitive
                  || '|';
               UTL_FILE.PUT_LINE (lvc_file, lvc_string);
            ELSIF p_extract_type = 'Data Extract'
            THEN
               -- UOM Attribute
               IF REC_ITEM_MAST.UOM IS NOT NULL
               THEN
                  lvc_string := NULL;
                  lvc_string :=
                        '|'
                     || REC_ITEM_MAST.ITEM_NUMBER
                     || '|MASTER|ATTRIBUTE|UOM|'
                     || REC_ITEM_MAST.UOM
                     || '|';
                  UTL_FILE.PUT_LINE (lvc_file, lvc_string);
               END IF;

               -- Long Description Attribute
               IF REC_ITEM_MAST.LONG_DESCRIPTION IS NOT NULL
               THEN
                  lvc_string := NULL;
                  lvc_string :=
                        '|'
                     || REC_ITEM_MAST.ITEM_NUMBER
                     || '|MASTER|ATTRIBUTE|Long Description|'
                     || REC_ITEM_MAST.LONG_DESCRIPTION
                     || '|';
                  UTL_FILE.PUT_LINE (lvc_file, lvc_string);
               END IF;

               --SHELF_LIFE_DAYS Attribute
               IF REC_ITEM_MAST.SHELF_LIFE_DAYS IS NOT NULL
               THEN
                  lvc_string := NULL;
                  lvc_string :=
                        '|'
                     || REC_ITEM_MAST.ITEM_NUMBER
                     || '|MASTER|ATTRIBUTE|Shelf Life Days|'
                     || REC_ITEM_MAST.SHELF_LIFE_DAYS
                     || '|';
                  UTL_FILE.PUT_LINE (lvc_file, lvc_string);
               END IF;

               -- Taxware code Attribute

               IF REC_ITEM_MAST.TAXWARE_CODE IS NOT NULL
               THEN
                  lvc_string := NULL;
                  lvc_string :=
                        '|'
                     || REC_ITEM_MAST.ITEM_NUMBER
                     || '|MASTER|ATTRIBUTE|Taxware Code|'
                     || REC_ITEM_MAST.TAXWARE_CODE
                     || '|';
                  UTL_FILE.PUT_LINE (lvc_file, lvc_string);
               END IF;

               -- SHIPPING_WEIGHT Attribute
               IF REC_ITEM_MAST.SHIPPING_WEIGHT IS NOT NULL
               THEN
                  lvc_string := NULL;
                  lvc_string :=
                        '|'
                     || REC_ITEM_MAST.ITEM_NUMBER
                     || '|MASTER|ATTRIBUTE|Shipping Weight|'
                     || REC_ITEM_MAST.SHIPPING_WEIGHT
                     || '|';
                  UTL_FILE.PUT_LINE (lvc_file, lvc_string);
               END IF;


               -- HAZMAT Attribute
               IF REC_ITEM_MAST.Hazmat IS NOT NULL
               THEN
                  lvc_string := NULL;
                  lvc_string :=
                        '|'
                     || REC_ITEM_MAST.ITEM_NUMBER
                     || '|MASTER|ATTRIBUTE|Hazardous Material|'
                     || REC_ITEM_MAST.Hazmat
                     || '|';
                  UTL_FILE.PUT_LINE (lvc_file, lvc_string);
               END IF;

               -- Fixed Lot Multiplier Attribute
               IF REC_ITEM_MAST.FIXED_LOT_MULTIPLIER IS NOT NULL
               THEN
                  lvc_string := NULL;
                  lvc_string :=
                        '|'
                     || REC_ITEM_MAST.ITEM_NUMBER
                     || '|MASTER|ATTRIBUTE|Fixed Lot Multiplier|'
                     || REC_ITEM_MAST.FIXED_LOT_MULTIPLIER
                     || '|';
                  UTL_FILE.PUT_LINE (lvc_file, lvc_string);
               END IF;

               -- Category Class
               IF REC_ITEM_MAST.Cat_Class IS NOT NULL
               THEN
                  lvc_string := NULL;
                  lvc_string :=
                        '|'
                     || REC_ITEM_MAST.ITEM_NUMBER
                     || '|MASTER|ATTRIBUTE|Category Class|'
                     || REC_ITEM_MAST.Cat_Class
                     || '|';
                  UTL_FILE.PUT_LINE (lvc_file, lvc_string);
               END IF;

               -- WC Web Hierarchy Attribute
               IF REC_ITEM_MAST.WC_Web_Hierarchy IS NOT NULL
               THEN
                  lvc_string := NULL;
                  lvc_string :=
                        '|'
                     || REC_ITEM_MAST.ITEM_NUMBER
                     || '|MASTER|ATTRIBUTE|WC Web Hierarchy|'
                     || REC_ITEM_MAST.WC_Web_Hierarchy
                     || '|';
                  UTL_FILE.PUT_LINE (lvc_file, lvc_string);
               END IF;


               -- TIME SENSITIVE Attribute
               IF REC_ITEM_MAST.Time_Sensitive IS NOT NULL
               THEN
                  lvc_string := NULL;
                  lvc_string :=
                        '|'
                     || REC_ITEM_MAST.ITEM_NUMBER
                     || '|MASTER|ATTRIBUTE|Time Sensitive|'
                     || REC_ITEM_MAST.Time_Sensitive
                     || '|';
                  UTL_FILE.PUT_LINE (lvc_file, lvc_string);
               END IF;
            ELSIF p_extract_type = 'Blank Extract'
            THEN
               -- UOM Attribute
               IF REC_ITEM_MAST.UOM IS NULL
               THEN
                  lvc_string := NULL;
                  lvc_string :=
                        '|'
                     || REC_ITEM_MAST.ITEM_NUMBER
                     || '|MASTER|ATTRIBUTE|UOM|'
                     || REC_ITEM_MAST.UOM
                     || '|';
                  UTL_FILE.PUT_LINE (lvc_file, lvc_string);
               END IF;

               -- Long Description Attribute
               IF REC_ITEM_MAST.LONG_DESCRIPTION IS NULL
               THEN
                  lvc_string := NULL;
                  lvc_string :=
                        '|'
                     || REC_ITEM_MAST.ITEM_NUMBER
                     || '|MASTER|ATTRIBUTE|Long Description|'
                     || REC_ITEM_MAST.LONG_DESCRIPTION
                     || '|';
                  UTL_FILE.PUT_LINE (lvc_file, lvc_string);
               END IF;

               --SHELF_LIFE_DAYS Attribute
               IF REC_ITEM_MAST.SHELF_LIFE_DAYS IS NULL
               THEN
                  lvc_string := NULL;
                  lvc_string :=
                        '|'
                     || REC_ITEM_MAST.ITEM_NUMBER
                     || '|MASTER|ATTRIBUTE|Shelf Life Days|'
                     || REC_ITEM_MAST.SHELF_LIFE_DAYS
                     || '|';
                  UTL_FILE.PUT_LINE (lvc_file, lvc_string);
               END IF;

               -- Taxware code Attribute

               IF REC_ITEM_MAST.TAXWARE_CODE IS NULL
               THEN
                  lvc_string := NULL;
                  lvc_string :=
                        '|'
                     || REC_ITEM_MAST.ITEM_NUMBER
                     || '|MASTER|ATTRIBUTE|Taxware Code|'
                     || REC_ITEM_MAST.TAXWARE_CODE
                     || '|';
                  UTL_FILE.PUT_LINE (lvc_file, lvc_string);
               END IF;

               --Shipping Weight Attribute.
               IF REC_ITEM_MAST.SHIPPING_WEIGHT IS NULL
               THEN
                  lvc_string := NULL;
                  lvc_string :=
                        '|'
                     || REC_ITEM_MAST.ITEM_NUMBER
                     || '|MASTER|ATTRIBUTE|Shipping Weight|'
                     || REC_ITEM_MAST.SHIPPING_WEIGHT
                     || '|';
                  UTL_FILE.PUT_LINE (lvc_file, lvc_string);
               END IF;


               -- HAZMAT Attribute
               IF REC_ITEM_MAST.Hazmat IS NULL
               THEN
                  lvc_string := NULL;
                  lvc_string :=
                        '|'
                     || REC_ITEM_MAST.ITEM_NUMBER
                     || '|MASTER|ATTRIBUTE|Hazardous Material|'
                     || REC_ITEM_MAST.Hazmat
                     || '|';
                  UTL_FILE.PUT_LINE (lvc_file, lvc_string);
               END IF;

               -- Fixed Lot Multiplier Attribute
               IF REC_ITEM_MAST.FIXED_LOT_MULTIPLIER IS NULL
               THEN
                  lvc_string := NULL;
                  lvc_string :=
                        '|'
                     || REC_ITEM_MAST.ITEM_NUMBER
                     || '|MASTER|ATTRIBUTE|Fixed Lot Multiplier|'
                     || REC_ITEM_MAST.FIXED_LOT_MULTIPLIER
                     || '|';
                  UTL_FILE.PUT_LINE (lvc_file, lvc_string);
               END IF;


               -- Category Class
               IF REC_ITEM_MAST.Cat_Class IS NULL
               THEN
                  lvc_string := NULL;
                  lvc_string :=
                        '|'
                     || REC_ITEM_MAST.ITEM_NUMBER
                     || '|MASTER|ATTRIBUTE|Category Class|'
                     || REC_ITEM_MAST.Cat_Class
                     || '|';
                  UTL_FILE.PUT_LINE (lvc_file, lvc_string);
               END IF;


               -- WC Web Hierarchy Attribute
               IF REC_ITEM_MAST.WC_Web_Hierarchy IS NULL
               THEN
                  lvc_string := NULL;
                  lvc_string :=
                        '|'
                     || REC_ITEM_MAST.ITEM_NUMBER
                     || '|MASTER|ATTRIBUTE|WC Web Hierarchy|'
                     || REC_ITEM_MAST.WC_Web_Hierarchy
                     || '|';
                  UTL_FILE.PUT_LINE (lvc_file, lvc_string);
               END IF;

               -- TIME SENSITIVE Attribute
               IF REC_ITEM_MAST.Time_Sensitive IS NULL
               THEN
                  lvc_string := NULL;
                  lvc_string :=
                        '|'
                     || REC_ITEM_MAST.ITEM_NUMBER
                     || '|MASTER|ATTRIBUTE|Time Sensitive|'
                     || REC_ITEM_MAST.Time_Sensitive
                     || '|';
                  UTL_FILE.PUT_LINE (lvc_file, lvc_string);
               END IF;
            END IF;

            g_sec := 'UPC Extract';

            -- UPC Attribute
            ln_upc_count := 0;

            SELECT COUNT (1)
              INTO ln_upc_count
              FROM APPS.MTL_CROSS_REFERENCES mcr
             WHERE     mcr.CROSS_REFERENCE_TYPE = 'UPC'
                   AND mcr.INVENTORY_ITEM_ID =
                          REC_ITEM_MAST.inventory_item_id;



            IF ln_upc_count > 0
            THEN
               IF p_extract_type IN ('Full Extract', 'Data Extract')
               THEN
                  ln_count := 1;

                  FOR REC_CREF_UPC
                     IN CUR_CREF_UPC (REC_ITEM_MAST.INVENTORY_ITEM_ID)
                  LOOP
                     lvc_string := NULL;
                     lvc_string :=
                           '|'
                        || REC_ITEM_MAST.ITEM_NUMBER
                        || '|MASTER|ATTRIBUTE|UPC'
                        || ln_count
                        || '|'
                        || REC_CREF_UPC.CROSS_REFERENCE
                        || '|';
                     UTL_FILE.PUT_LINE (lvc_file, lvc_string);
                     lvc_string := NULL;
                     lvc_string :=
                           '|'
                        || REC_ITEM_MAST.ITEM_NUMBER
                        || '|MASTER|ATTRIBUTE|UPC'
                        || ln_count
                        || ' Owner|'
                        || REC_CREF_UPC.ATTRIBUTE1
                        || '|';
                     UTL_FILE.PUT_LINE (lvc_file, lvc_string);
                     ln_count := ln_count + 1;
                  END LOOP;
               END IF;
            ELSE
               IF p_extract_type IN ('Full Extract', 'Blank Extract')
               THEN
                  lvc_string := NULL;
                  lvc_string :=
                        '|'
                     || REC_ITEM_MAST.ITEM_NUMBER
                     || '|MASTER|ATTRIBUTE|UPC||';
                  UTL_FILE.PUT_LINE (lvc_file, lvc_string);
                  lvc_string := NULL;
                  lvc_string :=
                        '|'
                     || REC_ITEM_MAST.ITEM_NUMBER
                     || '|MASTER|ATTRIBUTE|UPC Owner||';
                  UTL_FILE.PUT_LINE (lvc_file, lvc_string);
               END IF;
            END IF;

            g_sec := 'MFG Item Number';
            -- MFG Item Number Attribute
            ln_vendor_count := 0;

            SELECT COUNT (1)
              INTO ln_vendor_count
              FROM APPS.MTL_CROSS_REFERENCES mcr
             WHERE     mcr.CROSS_REFERENCE_TYPE = 'VENDOR'
                   AND mcr.INVENTORY_ITEM_ID =
                          REC_ITEM_MAST.inventory_item_id;

            IF ln_vendor_count > 0
            THEN
               IF p_extract_type IN ('Full Extract', 'Data Extract')
               THEN
                  ln_count := 1;

                  FOR REC_CREFF_MFG
                     IN CUR_CREFF_MFG (REC_ITEM_MAST.INVENTORY_ITEM_ID)
                  LOOP
                     lvc_string := NULL;
                     lvc_string :=
                           '|'
                        || REC_ITEM_MAST.ITEM_NUMBER
                        || '|MASTER|ATTRIBUTE|Vendor Part Number'
                        || ln_count
                        || '|'
                        || REC_CREFF_MFG.CROSS_REFERENCE
                        || '|';
                     UTL_FILE.PUT_LINE (lvc_file, lvc_string);

                     lvc_string := NULL;
                     lvc_string :=
                           '|'
                        || REC_ITEM_MAST.ITEM_NUMBER
                        || '|MASTER|ATTRIBUTE|Vendor'
                        || ln_count
                        || ' Owner|'
                        || REC_CREFF_MFG.vendor_name
                        || '|';
                     UTL_FILE.PUT_LINE (lvc_file, lvc_string);
                     ln_count := ln_count + 1;
                  END LOOP;
               END IF;
            ELSE
               IF p_extract_type IN ('Full Extract', 'Blank Extract')
               THEN
                  lvc_string := NULL;
                  lvc_string :=
                        '|'
                     || REC_ITEM_MAST.ITEM_NUMBER
                     || '|MASTER|ATTRIBUTE|Vendor Part Number||';
                  UTL_FILE.PUT_LINE (lvc_file, lvc_string);

                  lvc_string := NULL;
                  lvc_string :=
                        '|'
                     || REC_ITEM_MAST.ITEM_NUMBER
                     || '|MASTER|ATTRIBUTE|Vendor Owner||';
                  UTL_FILE.PUT_LINE (lvc_file, lvc_string);
               END IF;
            END IF;

            -- MSDS sheets priting.
            IF REC_ITEM_MAST.HAZMAT_FLAG = 'Y'
            THEN
               -- MSDS details
               IF p_extract_type IN ('Full Extract', 'Data Extract')
               THEN
                  lvc_string := NULL;
                  lvc_string :=
                        '|'
                     || REC_ITEM_MAST.ITEM_NUMBER
                     || '|MASTER|ATTACHMENT|MSDS|'
                     || lvc_msds_url
                     || REC_ITEM_MAST.MSDS
                     || '|';
                  UTL_FILE.PUT_LINE (lvc_file, lvc_string);
               ELSE
                  IF REC_ITEM_MAST.MSDS IS NULL
                  THEN
                     lvc_string := NULL;
                     lvc_string :=
                           '|'
                        || REC_ITEM_MAST.ITEM_NUMBER
                        || '|MASTER|ATTACHMENT|MSDS|'
                        || lvc_msds_url
                        || REC_ITEM_MAST.MSDS
                        || '|';
                     UTL_FILE.PUT_LINE (lvc_file, lvc_string);
                  END IF;
               END IF;
            END IF;


            FOR REC_EGO_DATA IN CUR_EGO_DATA (REC_ITEM_CATLOG.ITEM_NUMBER,
                                              lvc_images_url,
                                              p_extract_type,
                                              p_customer_id)
            LOOP
               lvc_string := NULL;
               lvc_string :=
                     '|'
                  || REC_ITEM_MAST.ITEM_NUMBER
                  || '|'
                  || REC_EGO_DATA.Catalog_name
                  || '|'
                  || REC_EGO_DATA.ATTR_GROUP_DISP_NAME
                  || '|'
                  || REC_EGO_DATA.ATTR_DISPLAY_NAME
                  || '|'
                  || REC_EGO_DATA.ATTRIBUTE_VALUE
                  || '|';

               UTL_FILE.PUT_LINE (lvc_file, lvc_string);
            END LOOP;
         END LOOP;
      END LOOP;

      COMMIT;

      UTL_FILE.FCLOSE (lvc_file);
      FND_FILE.PUT_LINE (FND_FILE.LOG, 'Extract Completed');
   EXCEPTION
      WHEN EXECUTION_ERROR
      THEN
         x_ret_code := 2;
         fnd_file.put_line (fnd_file.LOG, 'Error Occured ' || g_sec);

         IF UTL_FILE.is_open (LVC_FILE)
         THEN
            UTL_FILE.FCLOSE (LVC_FILE);
         END IF;
      WHEN OTHERS
      THEN
         IF UTL_FILE.is_open (LVC_FILE)
         THEN
            UTL_FILE.FCLOSE (LVC_FILE);
         END IF;

         l_err_msg :=
               l_err_msg
            || ' ...Error_Stack...'
            || DBMS_UTILITY.format_error_stack ()
            || ' Error_Backtrace...'
            || DBMS_UTILITY.format_error_backtrace ();

         -- Calling ERROR API
         xxcus_error_pkg.xxcus_error_main_api (
            p_called_from         => g_err_callfrom || '.' || g_procedure,
            p_ora_error_msg       => SUBSTR (l_err_msg, 1, 2000),
            p_error_desc          => g_sec,
            p_distribution_list   => g_distro_list,
            p_module              => 'INV');
         x_ret_code := 2;
   END;


   PROCEDURE PDH_ATTRIBUTES_UPDATE (P_BATCH_NUMBER IN NUMBER)
   /******************************************************************************************************
   -- PROGRAM TYPE: Procedure
   -- Name: PDH_ATTRIBUTES_UPDATE
   -- PURPOSE: Developed to update PDH Data from staging table to standard tables by using standard API.
   -- HISTORY
   -- ==========================================================================================================
   -- ==========================================================================================================
   -- VERSION DATE          AUTHOR(S)       DESCRIPTION
   -- ------- -----------   --------------- --------------------------------------------------------------------
   -- 1.0     17-FEB-2016   P.Vamshidhar   TMS#20160219-00063 - External Customer Catalog Extract
   ************************************************************************************************************/

   IS
      CURSOR CUR_STG_ITEM (
         CP_BATCH_NUMBER    NUMBER)
      IS
           SELECT XCST.ITEM_NUMBER, MSIB.INVENTORY_ITEM_ID
             FROM XXWC.XXWC_B2B_CAT_STG_TBL XCST, APPS.MTL_SYSTEM_ITEMS_B MSIB
            WHERE     BATCH_NUMBER = NVL (CP_BATCH_NUMBER, BATCH_NUMBER)
                  AND XCST.ITEM_NUMBER = MSIB.SEGMENT1
                  AND MSIB.ORGANIZATION_ID = 222
                  AND XCST.ITEM_NUMBER NOT IN (SELECT ITEM_NUMBER
                                                 FROM XXWC.XXWC_B2B_CAT_STG_TBL
                                                WHERE PROCESS_STATUS IN ('Error',
                                                                         'Success'))
         GROUP BY XCST.ITEM_NUMBER, MSIB.INVENTORY_ITEM_ID;

      CURSOR CUR_STG_ITEM_GROUP (
         CP_BATCH_NUMBER    NUMBER,
         CP_ITEM_NUMBER     VARCHAR2)
      IS
           SELECT XCST.ITEM_NUMBER,
                  EAV.APPLICATION_ID,
                  EAGV.ATTR_GROUP_ID,
                  EAGV.ATTR_GROUP_NAME,
                  EAGV.ATTR_GROUP_TYPE
             FROM APPS.EGO_ATTR_GROUPS_V EAGV,
                  APPS.EGO_ATTRS_V EAV,
                  XXWC.XXWC_B2B_CAT_STG_TBL XCST
            WHERE     EAV.ATTR_GROUP_NAME = EAGV.ATTR_GROUP_NAME
                  AND XCST.ATTR_GROUP_DISP_NAME = EAGV.ATTR_GROUP_DISP_NAME
                  AND EAV.ATTR_DISPLAY_NAME = XCST.ATTR_DISPLAY_NAME
                  AND XCST.BATCH_NUMBER =
                         NVL (CP_BATCH_NUMBER, XCST.BATCH_NUMBER)
                  AND XCST.ITEM_NUMBER = CP_ITEM_NUMBER
                  AND XCST.PROCESS_STATUS IN ('New')
         GROUP BY XCST.ITEM_NUMBER,
                  EAV.APPLICATION_ID,
                  EAGV.ATTR_GROUP_ID,
                  EAGV.ATTR_GROUP_NAME,
                  EAGV.ATTR_GROUP_TYPE;

      CURSOR CUR_STG_ITEM_GROUP_ATTR (
         CP_BATCH_NUMBER    NUMBER,
         CP_ITEM_NUMBER     VARCHAR2,
         CP_GROUP_NAME      VARCHAR2)
      IS
           SELECT XCST.ITEM_NUMBER,
                  EAV.APPLICATION_ID,
                  EAGV.ATTR_GROUP_ID,
                  EAGV.ATTR_GROUP_NAME,
                  EAGV.ATTR_GROUP_TYPE,
                  EAV.DATABASE_COLUMN,
                  EAV.DATA_TYPE_CODE,
                  EAV.ATTR_NAME,
                  XCST.ATTRIBUTE_VALUE
             FROM APPS.EGO_ATTR_GROUPS_V EAGV,
                  APPS.EGO_ATTRS_V EAV,
                  XXWC.XXWC_B2B_CAT_STG_TBL XCST
            WHERE     EAV.ATTR_GROUP_NAME = EAGV.ATTR_GROUP_NAME
                  AND XCST.ATTR_GROUP_DISP_NAME = EAGV.ATTR_GROUP_DISP_NAME
                  AND EAV.ATTR_DISPLAY_NAME = XCST.ATTR_DISPLAY_NAME
                  AND XCST.BATCH_NUMBER =
                         NVL (CP_BATCH_NUMBER, XCST.BATCH_NUMBER)
                  AND XCST.ITEM_NUMBER = CP_ITEM_NUMBER
                  AND EAGV.ATTR_GROUP_NAME = CP_GROUP_NAME
                  AND EAV.DATA_TYPE_CODE <> 'A'
                  AND XCST.PROCESS_STATUS IN ('New')
         ORDER BY EAV.ATTR_GROUP_NAME;

      l_attr_row_table       EGO_USER_ATTR_ROW_TABLE;
      l_attr_data_table      EGO_USER_ATTR_DATA_TABLE;

      l_error_message_list   Error_handler.error_tbl_type;

      x_failed_row_id_list   VARCHAR2 (1000);
      x_return_status        VARCHAR2 (100);
      x_msg_count            NUMBER;
      x_msg_data             VARCHAR2 (10000);
      x_error_code           NUMBER;
      l_err_msg              VARCHAR2 (1000);
      l_err_message          VARCHAR2 (1000);
      l_err_message1         VARCHAR2 (1000);

      ---------------------------------------------------
      -- UDA
      ---------------------------------------------------
      l_row_identifier       NUMBER;
      l_attr_row_index       NUMBER := 1;
      l_attr_data_index      NUMBER := 1;
      l_org_id               NUMBER := 222;
      l_attribute_value      VARCHAR2 (1000);
      l_sql                  VARCHAR2 (1000);
      g_procedure            VARCHAR2 (100) := 'PDH_ATTRIBUTES_UPDATE';
      l_user_id              NUMBER := fnd_global.user_id;
      p_batch_number1        NUMBER := NVL (p_batch_number, 99999999999);

      EXECUTION_ERROR        EXCEPTION;
      P_PROCESS_MESS         VARCHAR2 (1000);
   BEGIN
      BEGIN
         APPS.MO_GLOBAL.SET_POLICY_CONTEXT ('S', 162);
      END;

      BEGIN
         APPS.fnd_global.apps_initialize (33710, 51229, 431);
      END;

      g_sec := 'Process Start';

      g_sec := 'Re-Setting Process Status Flag';

      -- Resetting Process status flag for current back for new and error records.
      BEGIN
         UPDATE XXWC.XXWC_B2B_CAT_STG_TBL XCST
            SET XCST.PROCESS_STATUS = 'New', XCST.Error_message = NULL
          WHERE     XCST.batch_number = p_batch_number1
                AND XCST.PROCESS_STATUS = 'Error';

         COMMIT;
      EXCEPTION
         WHEN OTHERS
         THEN
            l_err_msg :=
                  ' Error occured, While Resetting Process Status '
               || SUBSTR (SQLERRM, 1, 200);
            RAISE EXECUTION_ERROR;
      END;

      -- Validating Items from Stating table to base tables.
      g_sec := 'Checking for Inactive Items';

      BEGIN
         UPDATE XXWC.XXWC_B2B_CAT_STG_TBL XCST
            SET XCST.PROCESS_STATUS = 'Error',
                XCST.Error_message =
                   'Item does not exist in Oracle Or Item Inactive'
          WHERE     XCST.ITEM_NUMBER NOT IN (SELECT SEGMENT1
                                               FROM APPS.MTL_SYSTEM_ITEMS_B MSIB
                                              WHERE     MSIB.ORGANIZATION_ID =
                                                           222
                                                    AND INVENTORY_ITEM_STATUS_CODE IN ('Active',
                                                                                       'Intangible',
                                                                                       'Repair'))
                AND XCST.batch_number = p_batch_number1;

         COMMIT;
      EXCEPTION
         WHEN OTHERS
         THEN
            l_err_msg :=
                  ' Error occured, While updating items '
               || SUBSTR (SQLERRM, 1, 200);
            RAISE EXECUTION_ERROR;
      END;

      -- Validating ICC
      g_sec := 'Validating ICC';

      BEGIN
         UPDATE XXWC.XXWC_B2B_CAT_STG_TBL XCST
            SET XCST.PROCESS_STATUS = 'Error',
                XCST.Error_message =
                   XCST.Error_message || ' - ' || 'Invalid ICC'
          WHERE     XCST.batch_number = p_batch_number1
                AND UPPER (XCST.ICC) NOT IN (SELECT UPPER (
                                                       CONCATENATED_SEGMENTS)
                                               FROM APPS.MTL_ITEM_CATALOG_GROUPS_KFV
                                              WHERE ENABLED_FLAG = 'Y');


         COMMIT;
      EXCEPTION
         WHEN OTHERS
         THEN
            l_err_msg :=
                  ' Error occured, While validating ICC'
               || SUBSTR (SQLERRM, 1, 200);
            RAISE EXECUTION_ERROR;
      END;

      -- Validating ATTRIBUTE Groups
      g_sec := 'Validating Attribute Groups';

      BEGIN
         UPDATE XXWC.XXWC_B2B_CAT_STG_TBL XCST
            SET XCST.PROCESS_STATUS = 'Error',
                XCST.Error_message =
                   XCST.Error_message || ' - ' || 'Invalid Attribute Group'
          WHERE     XCST.batch_number = p_batch_number1
                AND UPPER (XCST.ATTR_GROUP_DISP_NAME) NOT IN (SELECT UPPER (
                                                                        ATTR_GROUP_DISP_NAME)
                                                                FROM APPS.EGO_ATTR_GROUPS_V EAGV);

         COMMIT;
      EXCEPTION
         WHEN OTHERS
         THEN
            l_err_msg :=
                  'Error occured, While validating Attribute Groups'
               || SUBSTR (SQLERRM, 1, 200);
            RAISE EXECUTION_ERROR;
      END;

      -- Validating ATTRIBUTEs
      g_sec := 'Validating Attributes';

      BEGIN
         UPDATE XXWC.XXWC_B2B_CAT_STG_TBL XCST
            SET XCST.PROCESS_STATUS = 'Error',
                XCST.Error_message =
                   XCST.Error_message || ' - ' || 'Invalid Attribute'
          WHERE     XCST.batch_number = p_batch_number1
                AND UPPER (XCST.ATTR_DISPLAY_NAME) NOT IN (SELECT UPPER (
                                                                     ATTR_DISPLAY_NAME)
                                                             FROM APPS.EGO_ATTRS_V EAV
                                                            WHERE EAV.ENABLED_FLAG =
                                                                     'Y');

         COMMIT;
      EXCEPTION
         WHEN OTHERS
         THEN
            l_err_msg :=
                  'Error occured, While Validating Attributes'
               || SUBSTR (SQLERRM, 1, 200);
            RAISE EXECUTION_ERROR;
      END;

      g_sec := 'Active Items Process start';


      FOR REC_STG_ITEM IN CUR_STG_ITEM (p_batch_number1)
      LOOP
         l_attr_row_index := 0;
         l_attr_data_index := 0;
         l_attr_data_table := EGO_USER_ATTR_DATA_TABLE ();
         l_attr_row_table := EGO_USER_ATTR_ROW_TABLE ();

         g_sec := 'Attribute Group Process Start';


         FOR REC_STG_ITEM_GROUP
            IN CUR_STG_ITEM_GROUP (p_batch_number1, REC_STG_ITEM.ITEM_NUMBER)
         LOOP
            l_attr_row_table.EXTEND;
            l_attr_row_index := l_attr_row_table.COUNT;
            l_row_identifier := l_attr_row_index;
            l_attr_row_table (l_attr_row_index) :=
               ego_user_attr_row_obj (l_row_identifier,
                                      REC_STG_ITEM_GROUP.attr_group_id,
                                      REC_STG_ITEM_GROUP.APPLICATION_ID,
                                      REC_STG_ITEM_GROUP.ATTR_GROUP_TYPE,
                                      REC_STG_ITEM_GROUP.ATTR_GROUP_NAME,
                                      'ITEM_LEVEL',
                                      NULL,
                                      NULL,
                                      NULL,
                                      NULL,
                                      NULL,
                                      'SYNC');

            g_sec := 'Attribute values Process Start';

            FOR REC_STG_ITEM_GROUP_ATTR
               IN CUR_STG_ITEM_GROUP_ATTR (
                     P_BATCH_NUMBER1,
                     REC_STG_ITEM_GROUP.ITEM_NUMBER,
                     REC_STG_ITEM_GROUP.ATTR_GROUP_NAME)
            LOOP
               l_attr_data_table.EXTEND;
               l_attr_data_index := l_attr_data_table.COUNT;

               l_sql := NULL;
               l_sql :=
                     'SELECT '
                  || REC_STG_ITEM_GROUP_ATTR.DATABASE_COLUMN
                  || ' FROM APPS.EGO_MTL_SY_ITEMS_EXT_B WHERE ORGANIZATION_ID=222 AND INVENTORY_ITEM_ID= :1 AND ATTR_GROUP_ID = :2';

               g_sec := 'SQL Command Execution.';

               BEGIN
                  EXECUTE IMMEDIATE l_sql
                     INTO l_attribute_value
                     USING REC_STG_ITEM.inventory_item_id,
                           REC_STG_ITEM_GROUP_ATTR.attr_group_id;
               EXCEPTION
                  WHEN OTHERS
                  THEN
                     l_attribute_value := NULL;
               END;

               g_sec := 'Comparing Old and New Values';

               IF NVL (TO_CHAR (l_attribute_value), 'ABC') =
                     NVL (TO_CHAR (REC_STG_ITEM_GROUP_ATTR.ATTRIBUTE_VALUE),
                          'ABC')
               THEN
                  CONTINUE;
               END IF;

               g_sec := 'Adding value to virtual table based on data type';


               IF REC_STG_ITEM_GROUP_ATTR.DATA_TYPE_CODE IN ('C')
               THEN                                               --,'A') THEN
                  l_attr_data_table (l_attr_data_index) :=
                     ego_user_attr_data_obj (
                        l_row_identifier,
                        REC_STG_ITEM_GROUP_ATTR.ATTR_NAME,
                        REC_STG_ITEM_GROUP_ATTR.ATTRIBUTE_VALUE,
                        NULL,
                        NULL,
                        NULL,
                        NULL,
                        l_row_identifier);
               ELSIF REC_STG_ITEM_GROUP_ATTR.DATA_TYPE_CODE = 'N'
               THEN
                  l_attr_data_table (l_attr_data_index) :=
                     ego_user_attr_data_obj (
                        l_row_identifier,
                        REC_STG_ITEM_GROUP_ATTR.ATTR_NAME,
                        NULL,
                        REC_STG_ITEM_GROUP_ATTR.ATTRIBUTE_VALUE,
                        NULL,
                        NULL,
                        NULL,
                        l_row_identifier);
               ELSIF REC_STG_ITEM_GROUP_ATTR.DATA_TYPE_CODE = 'X'
               THEN
                  l_attr_data_table (l_attr_data_index) :=
                     ego_user_attr_data_obj (
                        l_row_identifier,
                        REC_STG_ITEM_GROUP_ATTR.ATTR_NAME,
                        NULL,
                        NULL,
                        REC_STG_ITEM_GROUP_ATTR.ATTRIBUTE_VALUE,
                        NULL,
                        NULL,
                        l_row_identifier);
               END IF;
            END LOOP;
         END LOOP;

         g_sec := 'Calling Oracle API';

         EGO_ITEM_PUB.PROCESS_USER_ATTRS_FOR_ITEM (
            P_API_VERSION             => '1.0',
            P_INVENTORY_ITEM_ID       => REC_STG_ITEM.INVENTORY_ITEM_ID,
            P_ORGANIZATION_ID         => l_org_id,
            P_ATTRIBUTES_ROW_TABLE    => l_attr_row_table,
            P_ATTRIBUTES_DATA_TABLE   => l_attr_data_table,
            X_FAILED_ROW_ID_LIST      => x_failed_row_id_list,
            X_RETURN_STATUS           => x_return_status,
            X_ERRORCODE               => x_error_code,
            X_MSG_COUNT               => x_msg_count,
            X_MSG_DATA                => x_msg_data);

         g_sec := 'Validting Oracle API Results';

         IF x_return_status <> 'S'
         THEN
            Error_Handler.Get_message_list (l_error_message_list);

            FOR i IN 1 .. l_error_message_list.COUNT
            LOOP
               l_err_msg := NULL;
               l_err_msg := l_error_message_list (i).MESSAGE_TEXT;
            END LOOP;

            g_sec := 'Updating API Results to custom table';

            UPDATE XXWC.XXWC_B2B_CAT_STG_TBL
               SET PROCESS_STATUS = 'Error',
                   PROCESSED_DATE = SYSDATE,
                   ERROR_MESSAGE = SUBSTR (l_err_msg, 1, 200),
                   LAST_UPDATED_BY = l_user_id,
                   LAST_UPDATED_DATE = SYSDATE
             WHERE     ITEM_NUMBER = REC_STG_ITEM.ITEM_NUMBER
                   AND BATCH_NUMBER = P_BATCH_NUMBER1;
         ELSE
            UPDATE XXWC.XXWC_B2B_CAT_STG_TBL
               SET PROCESS_STATUS = 'Success',
                   PROCESSED_DATE = SYSDATE,
                   ERROR_MESSAGE = NULL,
                   LAST_UPDATED_BY = l_user_id,
                   LAST_UPDATED_DATE = SYSDATE
             WHERE     ITEM_NUMBER = REC_STG_ITEM.ITEM_NUMBER
                   AND BATCH_NUMBER = P_BATCH_NUMBER1;
         END IF;

         -- Deleting data from PL/SQL tables.
         l_attr_data_table.delete;
         l_attr_row_table.delete;

         COMMIT;
      END LOOP;

      P_PROCESS_MESS := 'Process Successfully Completed';
   EXCEPTION
      WHEN EXECUTION_ERROR
      THEN
         P_PROCESS_MESS := ' Error Occured:' || SUBSTR (l_err_msg, 1, 200);
      WHEN OTHERS
      THEN
         l_err_msg :=
               l_err_msg
            || ' ...Error_Stack...'
            || DBMS_UTILITY.format_error_stack ()
            || ' Error_Backtrace...'
            || DBMS_UTILITY.format_error_backtrace ();

         -- Calling ERROR API
         xxcus_error_pkg.xxcus_error_main_api (
            p_called_from         => g_err_callfrom || '.' || g_procedure,
            p_ora_error_msg       => SUBSTR (l_err_msg, 1, 2000),
            p_error_desc          => SUBSTR (l_err_msg, 1, 240),
            p_distribution_list   => g_distro_list,
            p_module              => 'INV');

         P_PROCESS_MESS := 'Error occured: ' || SUBSTR (l_err_msg, 1, 200);
   END;

   PROCEDURE CUST_CATALOG_STG_EXTRACT (x_err_buf               OUT VARCHAR2,
                                       x_ret_code              OUT VARCHAR2,
                                       p_export_type        IN     VARCHAR2,
                                       p_customer_id        IN     NUMBER,
                                       p_customer_catalog   IN     VARCHAR2,
                                       p_extract_type       IN     VARCHAR2)
   /******************************************************************************************************
   -- PROGRAM TYPE: Procedure
   -- Name: CUST_CATALOG_STG_EXTRACT
   -- PURPOSE: Developed to extract data from stanging table.
   -- HISTORY
   -- ==========================================================================================================
   -- ==========================================================================================================
   -- VERSION DATE          AUTHOR(S)       DESCRIPTION
   -- ------- -----------   --------------- --------------------------------------------------------------------
   -- 1.0     17-FEB-2016   P.Vamshidhar   TMS#20160219-00063 - External Customer Catalog Extract
   -- 1.1     06-JUN-2016   P.Vamshidhar   TMS#20160219-00063 - External Customer Catalog Extract
                                        Post Production issues fixed.

   ************************************************************************************************************/
   IS
      CURSOR CUR_ITEM_CATLOG (
         cp_customer_id        VARCHAR2,
         p_customer_catalog    VARCHAR2,
         ld_from_date          DATE)
      IS
         SELECT DISTINCT ITEM_NUMBER
           FROM XXWC.XXWC_B2B_CAT_CUSTOMER_TBL xct
          WHERE     NVL (cp_customer_id, xct.CUSTOMER_ID) = xct.CUSTOMER_ID
                AND NVL (UPPER (P_CUSTOMER_CATALOG),
                         UPPER (xct.CUST_CATALOG_NAME)) = -- Added upper in 1.1
                       UPPER (xct.CUST_CATALOG_NAME)
                AND EXISTS
                       (SELECT 1
                          FROM XXWC.XXWC_B2B_CAT_STG_TBL msib
                         WHERE msib.ITEM_NUMBER = xct.item_number);


      CURSOR CUR_STG_DATA (
         p_extrat_type         VARCHAR2,
         p_customer_id         NUMBER,
         p_customer_catalog    VARCHAR2)
      IS
         SELECT    '|'
                || ITEM_NUMBER
                || '|'
                || ICC
                || '|'
                || ATTR_GROUP_DISP_NAME
                || '|'
                || ATTR_DISPLAY_NAME
                || '|'
                || REPLACE (ATTRIBUTE_VALUE, CHR (13), '')
                || '|'
                   STG_DATA
           FROM XXWC.XXWC_B2B_CAT_STG_TBL xbct
          WHERE     (   (p_extrat_type = 'Full Extract')
                     OR (    p_extrat_type = 'Data Extract'
                         AND xbct.ATTRIBUTE_VALUE IS NOT NULL)
                     OR (    p_extrat_type = 'Blank Extract'
                         AND xbct.ATTRIBUTE_VALUE IS NULL))
                AND (   p_customer_id IS NULL
                     OR (    p_customer_id IS NOT NULL
                         AND EXISTS
                                (SELECT 1
                                   FROM XXWC.XXWC_B2B_CAT_CUSTOMER_TBL XBCCT
                                  WHERE     p_customer_id = XBCCT.CUSTOMER_ID
                                        AND NVL (
                                               UPPER (p_customer_catalog),
                                               UPPER (
                                                  XBCCT.CUST_CATALOG_NAME)) =
                                               UPPER (
                                                  XBCCT.CUST_CATALOG_NAME))));

      lvc_file_name      VARCHAR2 (1000);
      l_customer_name    VARCHAR2 (1000);
      lvc_file           UTL_FILE.FILE_TYPE;
      lvc_outbound_dir   VARCHAR2 (100) := 'XXWC_INV_CUSTOMER_CATALOG';
      g_sec              VARCHAR2 (1000);
      max_line_length    BINARY_INTEGER := 32767;
      lvc_string         VARCHAR2 (3000) := NULL;
      execution_error    EXCEPTION;
      g_procedure        VARCHAR2 (1000) := 'cust_catalog_stg_extract';
      l_err_msg          VARCHAR2 (1000);
      g_distro_list      VARCHAR2 (1000);
   BEGIN
      g_sec := 'Opening file at OS level';
      Fnd_file.put_line (fnd_file.LOG, g_sec);
      fnd_file.put_line (fnd_file.LOG, 'Parameters ');
      fnd_file.put_line (fnd_file.LOG, 'Export Type  : ' || p_export_type);
      fnd_file.put_line (fnd_file.LOG, 'Customer Id  : ' || p_customer_id);
      fnd_file.put_line (fnd_file.LOG,
                         'Cust Catalog : ' || p_customer_catalog);
      fnd_file.put_line (fnd_file.LOG, 'Extract Type : ' || p_extract_type);

      BEGIN
         SELECT REPLACE (party_name, ' ', '_')
           INTO l_customer_name
           FROM apps.hz_parties
          WHERE party_id = p_customer_id;
      EXCEPTION
         WHEN OTHERS
         THEN
            l_customer_name := NULL;
      END;

      lvc_file_name :=
            REPLACE (TRANSLATE (l_customer_name, '()&/*-+', '_'), ' ', '_')
         || TO_CHAR (SYSDATE, 'MMDDYYYYHH24MISS')
         || '.dat';

      fnd_file.put_line (fnd_file.LOG, 'File Name: ' || lvc_file_name);
      -- Opening File
      lvc_file :=
         UTL_FILE.FOPEN (lvc_outbound_dir,
                         lvc_file_name,
                         'W',
                         max_line_length);

      fnd_file.put_line (fnd_file.LOG, 'File Extract Started ');

      -- Writing Header
      lvc_string :=
         '|ITEM_NUMBER|ICC|ATTR_GROUP_DISP_NAME|ATTR_DISPLAY_NAME|ATTRIBUTE_VALUE|';
      UTL_FILE.PUT_LINE (lvc_file, lvc_string);


      FOR rec_stg_data
         IN cur_stg_data (p_extract_type, p_customer_id, p_customer_catalog)
      LOOP
         lvc_string := rec_stg_data.stg_data;
         UTL_FILE.PUT_LINE (lvc_file, lvc_string);
      END LOOP;

      UTL_FILE.FCLOSE (lvc_file);

      FND_FILE.PUT_LINE (FND_FILE.LOG, 'Extract Completed');
   EXCEPTION
      WHEN EXECUTION_ERROR
      THEN
         x_ret_code := 2;
         x_err_buf := 'Execution Error Occured';
         fnd_file.put_line (fnd_file.LOG, 'Error Occured ' || g_sec);

         IF UTL_FILE.is_open (LVC_FILE)
         THEN
            UTL_FILE.FCLOSE (LVC_FILE);
         END IF;
      WHEN OTHERS
      THEN
         IF UTL_FILE.is_open (LVC_FILE)
         THEN
            UTL_FILE.FCLOSE (LVC_FILE);
         END IF;

         l_err_msg :=
               l_err_msg
            || ' ...Error_Stack...'
            || DBMS_UTILITY.format_error_stack ()
            || ' Error_Backtrace...'
            || DBMS_UTILITY.format_error_backtrace ();

         fnd_file.put_line (fnd_file.LOG, l_err_msg);

         -- Calling ERROR API
         xxcus_error_pkg.xxcus_error_main_api (
            p_called_from         => g_err_callfrom || '.' || g_procedure,
            p_ora_error_msg       => SUBSTR (l_err_msg, 1, 2000),
            p_error_desc          => g_sec,
            p_distribution_list   => g_distro_list,
            p_module              => 'INV');
         x_ret_code := 2;
         x_err_buf := l_err_msg;
   END;
END XXWC_EGO_CUSTOMER_CATALOG_PKG;
/
