   /***********************************************************************************************************************
   *   $Header XXWC_INV_CUST_CATALOG_DB.sql $
   *   Module Name: XXWC_INV_CUST_CATALOG_DB.sql
   *
   *   PURPOSE:   This package is used for Invoice Pre Processing
   *
   *   REVISIONS:
   *   Ver        Date         Author                Description
   *   ---------  ----------   ---------------       -----------------------------------------------------------------
   *   1.0        16-Mar-2016  P.Vamshidhar          Initial Version
   *                                                 TMS#20160219-00063 Attribute Enrichment and Extract for Kiewit
   * *********************************************************************************************************************/

DECLARE
   l_db_name   VARCHAR2 (20);
BEGIN
   SELECT name INTO l_db_name FROM v$database;

   IF l_db_name = 'EBSQA'
   THEN
      EXECUTE IMMEDIATE
         'CREATE OR REPLACE DIRECTORY XXWC_INV_CUSTOMER_CATALOG as ''/xx_iface/ebsqa/outbound/pdh/b2b_cat/customer_catalog''';
   ELSIF l_db_name = 'EBSPRD'
   THEN
      EXECUTE IMMEDIATE
         'CREATE OR REPLACE DIRECTORY XXWC_INV_CUSTOMER_CATALOG as ''/xx_iface/ebsprd/outbound/pdh/b2b_cat/customer_catalog''';
   END IF;
END;
/
