CREATE OR REPLACE PACKAGE APPS.XXWC_EGO_CUSTOMER_CATALOG_PKG
/******************************************************************************************************
-- File Name: XXWC_EGO_CUSTOMER_CATALOG_PKG.pks
--
-- PROGRAM TYPE: PL/SQL Script   <API>
--
-- PURPOSE: Developed to Extract PDH Catalog data for required customer
-- HISTORY
-- ==========================================================================================================
-- ==========================================================================================================
-- VERSION DATE          AUTHOR(S)       DESCRIPTION
-- ------- -----------   --------------- --------------------------------------------------------------------
-- 1.0     17-FEB-2016   P.Vamshidhar   TMS#20160219-00063 - External Customer Catalog Extract
************************************************************************************************************/
IS
   PROCEDURE Generate_txt (x_err_buf               OUT VARCHAR2,
                           x_ret_code              OUT VARCHAR2,
                           p_export_type        IN     VARCHAR2,
                           p_customer_id        IN     NUMBER,
                           p_customer_catalog   IN     VARCHAR2,
                           p_extract_type       IN     VARCHAR2,
                           p_from_date          IN     VARCHAR2);


   PROCEDURE PDH_ATTRIBUTES_UPDATE (P_BATCH_NUMBER IN NUMBER);

   PROCEDURE CUST_CATALOG_STG_EXTRACT (x_err_buf               OUT VARCHAR2,
                                       x_ret_code              OUT VARCHAR2,
                                       p_export_type        IN     VARCHAR2,
                                       p_customer_id        IN     NUMBER,
                                       p_customer_catalog   IN     VARCHAR2,
                                       p_extract_type       IN     VARCHAR2);
END XXWC_EGO_CUSTOMER_CATALOG_PKG;
/

GRANT ALL ON APPS.XXWC_EGO_CUSTOMER_CATALOG_PKG TO EA_APEX;
/