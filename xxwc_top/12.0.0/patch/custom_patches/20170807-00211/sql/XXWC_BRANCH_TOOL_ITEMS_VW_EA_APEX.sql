  /*******************************************************************************
  Table: APPS.XXWC_BRANCH_TOOL_ITEMS_VW  
  Description: "APPS.XXWC_BRANCH_TOOL_ITEMS_VW grants" 
  HISTORY
  ===============================================================================
  VERSION DATE               AUTHOR(S)       DESCRIPTION
  ------- -----------------  --------------- -----------------------------------------
  1.0     10-AUG-2017        Pahwa Nancy   Task ID: 20170807-00211
  ********************************************************************************/
-- Grants
grant select on APPS.XXWC_BRANCH_TOOL_ITEMS_VW to ea_apex
/