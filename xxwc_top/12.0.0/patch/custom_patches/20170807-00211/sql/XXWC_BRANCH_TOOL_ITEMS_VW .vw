CREATE OR REPLACE VIEW APPS.XXWC_BRANCH_TOOL_ITEMS_VW AS
-----------------------------------------------------------------------------
  -- � Copyright 2008, Nancy Pahwa
  -- All Rights Reserved
  --
  -- Name           : APPS.XXWC_BRANCH_TOOL_ITEMS_VW 
  -- Date Written   : 10-AUG-2017
  -- Author         : Nancy Pahwa
  --
  -- Modification History:
  --
  -- Version When         Who        Did what
  -- ------- -----------  --------   -----------------------------------------------------
  -- 1.1    10-AUG-2017  nancypahwa   Initially Created TMS# 20170807-00211
  ---------------------------------------------------------------------------------
select m.partnumber || ' - ' || shortdescription d , m.partnumber r from apps.XXWC_MD_SEARCH_PRODUCTS_MV m
where m.partnumber not like '#%'
and m.inventory_item_id in
       (select c.inventory_item_id
          from apps.mtl_item_categories_v c, apps.mtl_categories mc
         where c.CATEGORY_ID = mc.CATEGORY_ID and c.STRUCTURE_ID = mc.STRUCTURE_ID 
         and mc.ATTRIBUTE5 = '0007' and c.CATEGORY_SET_NAME = 'Inventory Category');