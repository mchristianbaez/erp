/*
 TMS: 20160424-00005 Invalid/No Account alias for transaction causing costing error.
 */
SET SERVEROUTPUT ON SIZE 1000000;


BEGIN
   DBMS_OUTPUT.put_line ('Before Update');

   UPDATE MTL_MATERIAL_TRANSACTIONS
      SET TRANSACTION_SOURCE_ID = 3296,
          costed_flag = 'N',
          transaction_group_id = NULL,
          ERROR_CODE = NULL,
          error_explanation = NULL
    WHERE transaction_id = 393449277 AND organization_id = 240;


   DBMS_OUTPUT.put_line ('Records update-' || SQL%ROWCOUNT);
   COMMIT;
EXCEPTION
   WHEN OTHERS
   THEN
      DBMS_OUTPUT.put_line ('Unable to update record ' || SQLERRM);
END;
/