/*****************************************************************************
Table: XXWC.XXWC_AP_EXPENSE_PO_WRT_OFF_TBL 
Description: Dropping the table which we used to track expense 
             PO closure process. 
 
=============================================================================
VERSION DATE               AUTHOR(S)         DESCRIPTION 
------- -----------------  ---------------   ---------------------------------
 1.0     31-May-2017        Pattabhi Avula   TMS#20161118-00163 Mass Closure 
                                             of Uninvoiced Received Intangibles
                                             custom table dropping											 
*******************************************************************************/
SET SERVEROUT ON 

DROP TABLE XXWC.XXWC_AP_EXPENSE_PO_WRT_OFF_TBL
/