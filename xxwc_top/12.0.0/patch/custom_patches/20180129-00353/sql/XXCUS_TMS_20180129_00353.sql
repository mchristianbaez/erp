/*
 TMS:  20180129-000353
 Date: 01/29/2018
 Notes:  update GL INTERFACE reference8 (reversal period) with Feb-2018
*/
SET SERVEROUTPUT ON SIZE 1000000
DECLARE
 --
 n_loc number :=0;
 l_sql varchar2(2000) :=Null;
 --
BEGIN --Main Processing...
   --
   n_loc :=101;
   --
   --
   begin
    --
    n_loc :=104;
    --        
    UPDATE GL_INTERFACE SET REFERENCE8 ='Feb-2018'
    where 1 =1
    and user_je_source_name ='Concur'
    and user_je_category_name ='Expenses Accrual'
    ;
    --
    n_loc :=105;  
    --        
    dbms_output.put_line('Rows updated :'||sql%rowcount);
    --
    n_loc :=107;
    --        
   exception
    when others then
     --
     n_loc :=108;
     --
     dbms_output.put_line('@ n_loc ='||n_loc||', message ='||sqlerrm);
     --  
   end;
   --
   commit;
   --
EXCEPTION
   WHEN OTHERS THEN
      ROLLBACK;
      DBMS_OUTPUT.put_line ('TMS: 20170607-00083, Errors =' || SQLERRM);
END;
/