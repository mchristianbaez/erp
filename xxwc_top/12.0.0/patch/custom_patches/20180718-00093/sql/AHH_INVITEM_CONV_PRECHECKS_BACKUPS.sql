-- 20180718-00093(AH Harris Items Conversions - PRE CHECKS - BACKUP)
set serveroutput ON
DECLARE
  l_schema VARCHAR2(30);
  l_schema_status VARCHAR2(1);
  l_industry VARCHAR2(1);
  l_backupsuffix VARCHAR2(10) := to_char(SYSDATE,'ddhh24miss');

BEGIN
  dbms_output.put_line('... Started');
  
  EXECUTE IMMEDIATE 'CREATE TABLE XXWC.MTL_SYS_ITEMS_INT_AHH' ||l_backupsuffix || ' AS SELECT * FROM INV.MTL_SYSTEM_ITEMS_INTERFACE';
  EXECUTE IMMEDIATE 'CREATE TABLE XXWC.MTL_INTERFACE_ERR_AHH' ||l_backupsuffix || ' AS SELECT * FROM INV.MTL_INTERFACE_ERRORS';
  EXECUTE IMMEDIATE 'CREATE TABLE XXWC.MTL_ITEM_REVS_INT_AHH' ||l_backupsuffix || ' AS SELECT * FROM INV.MTL_ITEM_REVISIONS_INTERFACE';
  EXECUTE IMMEDIATE 'CREATE TABLE XXWC.MTL_ITEM_CATS_INT_AHH' ||l_backupsuffix || ' AS SELECT * FROM INV.MTL_ITEM_CATEGORIES_INTERFACE';
  EXECUTE IMMEDIATE 'CREATE TABLE XXWC.EGO_ITM_USRATTR_AHH' ||l_backupsuffix || ' AS SELECT * FROM EGO.EGO_ITM_USR_ATTR_INTRFC';
  dbms_output.put_line('... Backup Interface Tables - Done, suffix: '||l_backupsuffix);
  
  EXECUTE IMMEDIATE 'TRUNCATE TABLE INV.MTL_SYSTEM_ITEMS_INTERFACE';
  EXECUTE IMMEDIATE 'TRUNCATE TABLE INV.MTL_INTERFACE_ERRORS';
  EXECUTE IMMEDIATE 'TRUNCATE TABLE INV.MTL_ITEM_REVISIONS_INTERFACE';
  EXECUTE IMMEDIATE 'TRUNCATE TABLE INV.MTL_ITEM_CATEGORIES_INTERFACE';
  EXECUTE IMMEDIATE 'TRUNCATE TABLE EGO.EGO_ITM_USR_ATTR_INTRFC';
  dbms_output.put_line('... Truncate Interface Tables - Done');

  
  DBMS_STATS.Unlock_Table_Stats('EGO', 'EGO_ITM_USR_ATTR_INTRFC');
  DBMS_STATS.Unlock_Table_Stats('INV', 'MTL_SYSTEM_ITEMS_INTERFACE');
  DBMS_STATS.Unlock_Table_Stats('INV', 'MTL_ITEM_REVISIONS_INTERFACE');

  FND_STATS.Load_Histogram_Cols('DELETE',401,'MTL_SYSTEM_ITEMS_INTERFACE','PROCESS_FLAG') ;
  FND_STATS.Load_Histogram_Cols('DELETE',401,'MTL_SYSTEM_ITEMS_INTERFACE','SET_PROCESS_ID') ;

  IF (FND_INSTALLATION.GET_APP_INFO('EGO',l_schema_status,l_industry,l_schema)) Then
    FND_STATS.GATHER_TABLE_STATS(OWNNAME => l_schema,
                                 TABNAME => 'EGO_ITM_USR_ATTR_INTRFC',
                                 CASCADE => True);
  End If;

  IF (FND_INSTALLATION.Get_App_Info('INV',l_schema_status, l_industry,l_schema)) Then
    FND_STATS.Gather_Table_Stats(ownname => l_schema,
                                 tabname => 'MTL_SYSTEM_ITEMS_INTERFACE',
                                 cascade => True);
    FND_STATS.Gather_Table_Stats(ownname => l_schema,
                                 tabname => 'MTL_ITEM_REVISIONS_INTERFACE',
                                 cascade => True);
  End If;

  DBMS_STATS.Set_Column_Stats('EGO','EGO_ITM_USR_ATTR_INTRFC','PROCESS_STATUS',distcnt=>4,density=>0.25);

  DBMS_STATS.Lock_Table_Stats('EGO', 'EGO_ITM_USR_ATTR_INTRFC');
  DBMS_STATS.Lock_Table_Stats('INV', 'MTL_SYSTEM_ITEMS_INTERFACE');
  DBMS_STATS.Lock_Table_Stats('INV', 'MTL_ITEM_REVISIONS_INTERFACE');
  dbms_output.put_line('... Gather Stats on Interface tables');

  dbms_output.put_line('... <Ended');
EXCEPTION
  WHEN OTHERS THEN
      dbms_output.put_line('... Error Occurred: '||sqlerrm);

END;
/