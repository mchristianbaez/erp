/*
 TMS: 20140715-00036 
 Date: 09/16/2015
 */
SET SERVEROUTPUT ON SIZE 1000000

DECLARE
   v_count   NUMBER;
BEGIN
   DBMS_OUTPUT.put_line ('TMS:20140715-00036  , Script 1 -Before Update');

   BEGIN
      SELECT COUNT (1)
        INTO v_count
        FROM po_headers_all
       WHERE     1 = 1
             AND org_id = 162
             AND NVL (ACCEPTANCE_REQUIRED_FLAG, 'S') <> 'N';

      UPDATE po_headers_all
         SET ACCEPTANCE_REQUIRED_FLAG = 'N', ACCEPTANCE_DUE_DATE = NULL
       WHERE     1 = 1
             AND org_id = 162
             AND NVL (ACCEPTANCE_REQUIRED_FLAG, 'S') <> 'N';
   EXCEPTION
      WHEN OTHERS
      THEN
         ROLLBACK;
         DBMS_OUTPUT.PUT_LINE (
               'Error in updating ACCEPTANCE_REQUIRED_FLAG to po_headers_all-'
            || SQLERRM);
   END;

   COMMIT;

   DBMS_OUTPUT.put_line (
         'TMS: 20140715-00036, Script 1 -After update, rows updated: '
      || v_count);

   DBMS_OUTPUT.put_line ('TMS:20140715-00036  , Script 2 -Before Update');

   BEGIN
      v_count := 0;

      SELECT COUNT (1)
        INTO v_count
        FROM po_headers_archive_all
       WHERE     1 = 1
             AND org_id = 162
             AND NVL (ACCEPTANCE_REQUIRED_FLAG, 'S') <> 'N';

      UPDATE po_headers_archive_all
         SET ACCEPTANCE_REQUIRED_FLAG = 'N', ACCEPTANCE_DUE_DATE = NULL
       WHERE     1 = 1
             AND org_id = 162
             AND NVL (ACCEPTANCE_REQUIRED_FLAG, 'S') <> 'N';
   EXCEPTION
      WHEN OTHERS
      THEN
         ROLLBACK;
         DBMS_OUTPUT.PUT_LINE (
               'Error in updating ACCEPTANCE_REQUIRED_FLAG to po_headers_archive_all-'
            || SQLERRM);
   END;

   COMMIT;

   DBMS_OUTPUT.put_line (
         'TMS: 20140715-00036, Script 2 -After update, rows updated: '
      || v_count);
EXCEPTION
   WHEN OTHERS
   THEN
      DBMS_OUTPUT.PUT_LINE ('Error is' || SQLERRM);
END;
/
