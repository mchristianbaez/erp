/*************************************************************************
  $Header TMS_20160725-00044_Change_Oracle_Default_Printer.sql $
  Module Name: TMS_20160725-00044  Data Fix script  

  PURPOSE: Data Fix script 

  REVISIONS:
  Ver        Date         Author                Description
  ---------  -----------  ------------------    --------------------------
  1.0        26-AUG-2016  Pattabhi Avula        TMS#20160725-00044

**************************************************************************/ 
SET SERVEROUTPUT ON SIZE 100000;
SET VERIFY OFF;

declare
ERR_MSG   varchar2(200);
BEGIN
    delete  from apps.WSH_REPORT_PRINTERS
     where printer_name='P2126';

    COMMIT;

    dbms_output.put_line('Delete statement status message is: '||SQLERRM);
EXCEPTION
 WHEN OTHERS THEN
         ERR_MSG := 'ERROR :'||SQLERRM;
         DBMS_OUTPUT.PUT_LINE(ERR_MSG);

END;
/