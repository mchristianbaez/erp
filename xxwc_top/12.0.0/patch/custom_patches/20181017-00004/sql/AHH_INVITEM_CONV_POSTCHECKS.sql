--TMS task 20181017-00004 
--set serveroutput on gather stats
DECLARE
  l_schema        VARCHAR2(30);
  l_schema_status VARCHAR2(1);
  l_industry      VARCHAR2(1);

BEGIN

  dbms_stats.unlock_table_stats('EGO'
                               ,'EGO_ITM_USR_ATTR_INTRFC');
 dbms_stats.unlock_table_stats('INV'
                               ,'MTL_SYSTEM_ITEMS_INTERFACE');
 dbms_stats.unlock_table_stats('INV'
                               ,'MTL_ITEM_REVISIONS_INTERFACE');
 dbms_stats.unlock_table_stats('INV'
                               ,'MTL_ITEM_CATEGORIES_INTERFACE');

  IF (fnd_installation.get_app_info('EGO'
                                   ,l_schema_status
                                   ,l_industry
                                   ,l_schema)) THEN
    fnd_stats.gather_table_stats(ownname          => 'EGO'
                                 ,tabname          => 'EGO_MTL_SY_ITEMS_EXT_B'
                                 ,PERCENT => 100
                                 ,cascade          => TRUE
				,degree=>12);
                                 
    fnd_stats.gather_table_stats(ownname          => 'EGO'
                                 ,tabname          => 'EGO_MTL_SY_ITEMS_EXT_TL'
                                 ,PERCENT => 100
                                 ,cascade          => TRUE
				,degree=>12);
                                 
    fnd_stats.gather_table_stats(ownname          => l_schema
                                 ,tabname          => 'EGO_ITM_USR_ATTR_INTRFC'
                                 ,PERCENT => 100
                                  --,method_opt       => 'for all columns size skewonly'
                                 ,cascade => TRUE
                                  ,degree           => 12
                                  );
    --DBMS_STATS.GATHER_TABLE_STATS( ownname, tabname,PERCENT=>100,cascade=>true,no_invalidate=>false)
  END IF;

  IF (fnd_installation.get_app_info('INV'
                                   ,l_schema_status
                                   ,l_industry
                                   ,l_schema)) THEN
    fnd_stats.gather_table_stats(ownname          => l_schema
                                 ,tabname          => 'MTL_SYSTEM_ITEMS_INTERFACE'
                                 ,PERCENT => 100
                                  --,method_opt       => 'for all columns size skewonly'
                                 ,cascade => TRUE
                                  ,degree           => 12
                                  );
    fnd_stats.gather_table_stats(ownname          => l_schema
                                 ,tabname          => 'MTL_ITEM_REVISIONS_INTERFACE'
                                 ,PERCENT => 100
                                  --,method_opt       => 'for all columns size skewonly'
                                 ,cascade => TRUE
                                  ,degree           => 12
                                  );
    fnd_stats.gather_table_stats(ownname          => l_schema
                                 ,tabname          => 'MTL_ITEM_CATEGORIES_INTERFACE'
                                 ,PERCENT => 100
                                  --,method_opt       => 'for all columns size skewonly'
                                 ,cascade => TRUE
                                  ,degree           => 12
                                  );
  END IF;
 dbms_stats.lock_table_stats('EGO'
                             ,'EGO_ITM_USR_ATTR_INTRFC');
 dbms_stats.lock_table_stats('INV'
                             ,'MTL_SYSTEM_ITEMS_INTERFACE');
 dbms_stats.lock_table_stats('INV'
                             ,'MTL_ITEM_CATEGORIES_INTERFACE');
 dbms_stats.lock_table_stats('INV'
                             ,'MTL_ITEM_REVISIONS_INTERFACE');
  dbms_output.put_line('... Gather Stats on Interface tables');

  dbms_output.put_line('... <Ended');
EXCEPTION
  WHEN OTHERS THEN
    dbms_output.put_line('... Error Occurred: ' || SQLERRM);
  
END;
/