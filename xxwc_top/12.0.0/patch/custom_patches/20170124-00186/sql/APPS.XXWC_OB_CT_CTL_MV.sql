/*************************************************************************
  $Header XXWC_OB_CT_CTL_MV $
  Module Name: XXWC_OB_CT_CTL_MV

  REVISIONS:
  Ver        Date        Author                     Description
  ---------  ----------  ---------------         -------------------------
  1.0        01/24/2017  Steve                   Initial Version
**************************************************************************/


DROP MATERIALIZED VIEW APPS.XXWC_OB_CT_CTL_MV;

CREATE MATERIALIZED VIEW APPS.XXWC_OB_CT_CTL_MV (CREATED_BY,CREATION_DATE,CUSTOMER_TRX_ID,CUSTOMER_TRX_LINE_ID,DESCRIPTION,EXTENDED_AMOUNT,INTERFACE_LINE_ATTRIBUTE6,INTERFACE_LINE_CONTEXT,ACTUAL_INVENTORY_ITEM_ID,LAST_UPDATE_DATE,LAST_UPDATED_BY,LINE_NUMBER,LINE_TYPE,LINK_TO_CUST_TRX_LINE_ID,ORG_ID,QUANTITY_CREDITED,QUANTITY_INVOICED,QUANTITY_ORDERED,SALES_ORDER,SALES_ORDER_DATE,UNIT_SELLING_PRICE,UOM_CODE,WAREHOUSE_ID,TAX_AMOUNT,RN)
TABLESPACE APPS_TS_TX_DATA
NOCACHE
NOCOMPRESS
BUILD IMMEDIATE
REFRESH COMPLETE ON DEMAND
WITH PRIMARY KEY
AS 
SELECT created_by
      ,creation_date
      ,customer_trx_id
      ,customer_trx_line_id
      ,description
      ,extended_amount
      ,interface_line_attribute6
      ,interface_line_context
      ,inventory_item_id AS actual_inventory_item_id
      ,last_update_date
      ,last_updated_by
      ,line_number
      ,line_type
      ,link_to_cust_trx_line_id
      ,org_id
      ,quantity_credited
      ,quantity_invoiced
      ,quantity_ordered
      ,sales_order
      ,sales_order_date
      ,unit_selling_price
      ,uom_code
      ,warehouse_id
      ,CASE
          WHEN line_type = 'TAX'
          THEN
             SUM(extended_amount) OVER (PARTITION BY customer_trx_id, link_to_cust_trx_line_id)
          ELSE
             0
       END
          AS tax_amount
      ,ROW_NUMBER() OVER (PARTITION BY customer_trx_id, link_to_cust_trx_line_id ORDER BY ROWNUM) rn
  FROM ar.ra_customer_trx_lines_all
 WHERE     line_type IN ('LINE', 'TAX')
       AND last_update_date > SYSDATE - 2;


COMMENT ON MATERIALIZED VIEW APPS.XXWC_OB_CT_CTL_MV IS 'snapshot table for snapshot APPS.XXWC_OB_CT_CTL_MV';

CREATE BITMAP INDEX APPS.XXWC_OB_CT_CTL_MV_B1 ON APPS.XXWC_OB_CT_CTL_MV
(LINE_TYPE)
TABLESPACE APPS_TS_TX_IDX;

CREATE BITMAP INDEX APPS.XXWC_OB_CT_CTL_MV_B2 ON APPS.XXWC_OB_CT_CTL_MV
(LAST_UPDATE_DATE)
TABLESPACE APPS_TS_TX_IDX;

CREATE BITMAP INDEX APPS.XXWC_OB_CT_CTL_MV_B3 ON APPS.XXWC_OB_CT_CTL_MV
(INTERFACE_LINE_ATTRIBUTE6)
TABLESPACE APPS_TS_TX_IDX;

CREATE INDEX APPS.XXWC_OB_CT_CTL_MV_N1 ON APPS.XXWC_OB_CT_CTL_MV
(LINK_TO_CUST_TRX_LINE_ID)
TABLESPACE APPS_TS_TX_IDX
COMPRESS 1;
/