/*************************************************************************************************************************
  $Header XXWC_EQUIFAX_SFORCE_UPDATE
  File Name: XXWC_EQUIFAX_SFORCE_UPDATE.sql
  PURPOSE:   
  REVISIONS:
     Ver        Date           Author         Description
     ---------  -----------  ------------ -----------------------------------------------------------------------------------
     1.0        22-Feb-2018  P.Vamshidhar  TMS# 20180222-00302 - Customer - Data Script for Sales Force ID - Child
****************************************************************************************************************************/
 
SET SERVEROUTPUT ON
 
CREATE TABLE XXWC.XXWC_HZ_CUST_ACCOUNTS_TMP AS 
      SELECT HCA.CUST_ACCOUNT_ID,
             HCA.ACCOUNT_NUMBER,
             HCA.ATTRIBUTE6 ACCT_ATTRIBUTE6,
             HCA.ROWID ACCT_ROW_ID
        FROM APPS.HZ_CUST_ACCOUNTS HCA;
/
DECLARE
   CURSOR C1
   IS
      SELECT XEST.SFORCE_id,
             HCA.ATTRIBUTE6 ACCT_SALESFORCE,
             HCA.ROWID ACCT_ROWID,
             XEST.ROWID XEST_ROWID
        FROM XXWC.XXWC_EQUIFAX_SFORCE_TBL XEST, APPS.HZ_CUST_ACCOUNTS HCA
       WHERE XEST.ACCT_NUM = HCA.ACCOUNT_NUMBER AND XEST.msg IS NULL;

   ln_count   NUMBER := 0;
BEGIN
   FOR C2 IN C1
   LOOP
      IF C2.SFORCE_ID IS NOT NULL OR C2.SFORCE_ID <> C2.ACCT_SALESFORCE
      THEN
         ln_count := ln_count + 1;


         UPDATE APPS.HZ_CUST_ACCOUNTS
            SET ATTRIBUTE6 = C2.SFORCE_ID
          WHERE ROWID = C2.ACCT_ROWID;

         IF SQL%ROWCOUNT > 0
         THEN
            UPDATE XXWC.XXWC_EQUIFAX_SFORCE_TBL
               SET MSG = 'PROCESSED'
             WHERE ROWID = C2.XEST_ROWID;
         END IF;

         IF ln_count >= 1000
         THEN
            COMMIT;
            ln_count := 0;
         END IF;
      END IF;
   END LOOP;

   COMMIT;
END;
/