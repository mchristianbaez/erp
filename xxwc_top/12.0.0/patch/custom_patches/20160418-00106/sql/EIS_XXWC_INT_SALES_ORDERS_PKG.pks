CREATE OR REPLACE PACKAGE XXEIS.EIS_XXWC_INT_SALES_ORDERS_PKG
AS
 --//============================================================================
--//  
--// Change Request 			:: Performance Issue  --TMS#20160418-00106   by Pramod on 04-16-2016
--//
--// Object Usage 				:: This Object Referred by "Internal Sales Orders Report"
--//
--// Object Name         		:: XXEIS.EIS_XXWC_INT_SALES_ORDERS_PKG
--//
--// Object Type         		:: Package Specification
--//
--// Object Description  		:: This Package will trigger in before report and insert the values into Table
--//
--// Version Control
--//============================================================================
--// Version    Author           Date          Description
--//----------------------------------------------------------------------------
--// 1.0     Pramod  	04/16/2016   Initial Build --TMS#20160418-00106   by Pramod on 04-16-2016
--//============================================================================
TYPE CURSOR_TYPE4 IS REF  CURSOR;

    PROCEDURE process_int_sales_orders(
        p_process_id              in number,
        p_req_appr_date_from      in date,
        p_req_appr_date_to        in date,
        p_order_type              in varchar2,
        p_schedule_ship_date_from in date,
        p_shipping_method         in varchar2,
        p_ship_from_org           in varchar2,
        p_schedule_ship_date_to   in date,
        p_order_line_status       in varchar2,
        p_order_header_status     in varchar2,
        p_payment_terms           in varchar2,
        p_district                in varchar2,
        p_region                  in varchar2,
        p_location_list           in varchar2,
        p_ship_to_org             in varchar2,
        p_req_created_by          in number ) ;
--//============================================================================
--//
--// Object Name         :: process_int_sales_orders  
--//
--// Object Usage 		 :: This Object Referred by "Internal Sales Orders Report"
--//
--// Object Type         :: Procedure
--//
--// Object Description  :: This  procedure is created based on the EIS_XXWC_OM_INT_OPEN_ORDERS_V View.Due to the 
--// 						performance issue ,spilt the view definition based on the driving tables and driving data.
--//
--// Version Control
--//============================================================================
--// Version    Author           Date          Description
--//----------------------------------------------------------------------------
--// 1.0     Pramod  	04/16/2016   Initial Build --TMS#20160418-00106   by Pramod on 04-16-2016
--//============================================================================
        
    PROCEDURE clear_temp_tables(
        p_process_id IN NUMBER);
--//============================================================================
--//
--// Object Name         :: clear_temp_tables
--//
--// Object Type         :: Procedure
--//
--// Object Description  :: This procedure will trigger in After Report level and delete all the records from the table based on that particular process_id.
--//
--// Version Control
--//============================================================================
--// Version    Author           Date          Description
--//----------------------------------------------------------------------------
--// 1.0    Pramod  	04/16/2016   Initial Build --TMS#20160418-00106   by Pramod on 04-16-2016
--//============================================================================   		
  END;
/
