CREATE OR REPLACE PACKAGE apps.xxwc_qp_modifier_iface_pkg
as
/*************************************************************************
 Copyright (c) 2013 Lucidity Consulting Group
 All rights reserved.
**************************************************************************
  $Header xxwc_qp_modifier_iface_pkg$
  Module Name: xxwc_qp_modifier_iface_pkg.pks

  PURPOSE:   This package is used for modifier upload interface

  REVISIONS:
  Ver        Date        Author                     Description
  ---------  ----------  ---------------         -------------------------
  1.0        05/31/2013  Consuelo Gonzalez      Initial Version
  2.0        08/12/2013  Consuelo Gonzalez      TMS 20130809-01306: Changes to make
                                                standalone by piggy backing on the
                                                pricelist upload form. Updates to
                                                status and error handling during API Call
  3.0        10/29/2013  Consuelo Gonzalez      TMS 20131013-00041: Bug fix to inherit active
                                                flag from modifier
  7.0        4/12/2018   Nancy Pahwa            TMS 20180216-00104 - Added the concurrent submit request procedure to
                                                 call the conversion program after modifier upload
**************************************************************************/

TYPE XXWC_QP_MODIFIERS_REC_TYPE IS RECORD (
    MODIFIER_NAME           VARCHAR2(240)   := FND_API.G_MISS_CHAR,
    LIST_LINE_ID            NUMBER          := FND_API.G_MISS_NUM,
    START_DATE              DATE            := FND_API.G_MISS_DATE,
    END_DATE                DATE            := FND_API.G_MISS_DATE,
    PRODUCT_ATTRIBUTE       VARCHAR2(240)   := FND_API.G_MISS_CHAR,
    PRODUCT_VALUE           VARCHAR2(240)   := FND_API.G_MISS_CHAR,
    APPLICATION_METHOD      VARCHAR2(240)   := FND_API.G_MISS_CHAR,
    LINE_VALUE              NUMBER          := FND_API.G_MISS_NUM,
    INCOMPATIBILITY         VARCHAR2(80)    := FND_API.G_MISS_CHAR,
    PRICE_BREAK_TYPE        VARCHAR2(240)   := FND_API.G_MISS_CHAR,
    PRODUCT_UOM             VARCHAR2(3)     := FND_API.G_MISS_CHAR,
    PRICE_BREAK_VALUE_FROM  NUMBER          := FND_API.G_MISS_NUM,
    PRICE_BREAK_VALUE_TO    NUMBER          := FND_API.G_MISS_NUM,
    LIST_HEADER_ID          NUMBER          := FND_API.G_MISS_NUM,
    PRODUCT_ATTRIBUTE_CODE  VARCHAR2(240)   := FND_API.G_MISS_CHAR,
    PRODUCT_ATTR_VALUE_ID   NUMBER          := FND_API.G_MISS_NUM,
    FORMULA_ID              NUMBER          := FND_API.G_MISS_NUM,
    PROD_PRECEDENCE         NUMBER          := FND_API.G_MISS_NUM,
    STATUS                  VARCHAR2(50)    := FND_API.G_MISS_CHAR,
    ERR_MESSAGE             VARCHAR2(4000)  := FND_API.G_MISS_CHAR,
    REQUEST_ID              NUMBER          := FND_API.G_MISS_NUM/*,*/
  --  ORG_ID                  NUMBER          := FND_API.G_MISS_NUM
);

TYPE XXWC_QP_MODIFIERS_TBL_TYPE IS TABLE OF XXWC_QP_MODIFIERS_REC_TYPE
INDEX BY BINARY_INTEGER;

G_MISS_XXWC_QP_MODIFIERS_REC XXWC_QP_MODIFIERS_REC_TYPE;
G_MISS_XXWC_QP_MODIFIERS_TBL XXWC_QP_MODIFIERS_TBL_TYPE;

procedure   xxwc_qp_modifier_iface (RETCODE         OUT NUMBER
                                   , ERRMSG         OUT VARCHAR2
                                   , P_FILE_NAME    IN VARCHAR2
                                   , P_FILE_ID      IN NUMBER);
procedure submit_request(l_start_date in date);   --7.0
end xxwc_qp_modifier_iface_pkg;
/