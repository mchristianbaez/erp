CREATE OR REPLACE PACKAGE BODY apps.XXWC_OM_PRICING_CONV_PKG AS
  /****************************************************************************************************************
      $Header XXXWC_OM_PRICING_CONV_PKG $
      Module Name: XXWC_OM_PRICING_CONV_PKG.pks
  
      PURPOSE:    Convert existing modifiers to new pricing form
  
      REVISIONS:
      Ver        Date        Author                     Description
      ---------  ----------  ---------------    -------------------------------------------------------------------
      1.0        01/15/2018  Nancy Pahwa                 Initial Version TMS #  20180216-00104
 
  *****************************************************************************************************************/
  procedure pricing_conv(errbuf      OUT VARCHAR2,
                         retcode     OUT NUMBER,
                         p_from_date varchar2,
                         p_to_date   varchar2) as
    l_QUALIFIER_CONTEXT    varchar2(30);
    l_qualifier_attribute  varchar2(30);
    l_price_type           varchar2(30);
    l_agreement_id         number;
    l_agreement_line_id    number;
    l_partnumber           varchar2(40);
    l_description          varchar2(240);
    l_product_attr_val     varchar2(240);
    l_mod_value            number;
    l_mod_value1           number;
    l_modifier_value       number;
    l_QUALIFIER_ATTR_VALUE varchar2(240);
    l_product_attr_value   varchar2(4000);
    l_product_attribute    varchar2(30);
    l_organization_id      number;
    v_from_date            date;
    v_to_date              date;
    l_count                number;
    l_agreement_id1        number;
    l_count1               number;
    l_product_attr_val_num number;
    l_header_count         number;
    l_lines_count          number;
    l_header_to_count      number;
    l_lines_to_count       number;
    lvc_row_lock           VARCHAR2(1);
    l_list_header_id1      number;
    l_count_l              number;
    l_end_date             date;
    l_modifier_type_dff    varchar2(25) := 'Segmented Price';
    cursor header_to_be_processed is
    
      select count(1)
        from apps.qp_list_headers_all h
       where attribute10 = 'Segmented Price'
         and attribute14 is null
         and active_flag = 'Y'
            --    and list_header_id IN (3658769)--,3648317, 3648315)
         and creation_date between v_from_date and v_to_date;
  
    cursor lines_to_be_processed(p_header_id number) is
    
      select count(1)
        from apps.qp_list_lines l
       where l.list_header_id = p_header_id; --fnd_global.CONC_REQUEST_ID;
  
    cursor header_processed is
    
      select count(xocph.agreement_id)
        from apps.qp_list_headers_all          qlha,
             xxwc.XXWC_OM_CONTRACT_PRICING_HDR xocph
       where qlha.attribute14 = xocph.agreement_id
            --   and qlha.list_header_id IN (3658769);--,3648317, 3648315)
            
         and xocph.request_id = fnd_global.CONC_REQUEST_ID;
  
    cursor lines_processed is
      select count(xocpl.agreement_line_id)
        from apps.qp_list_headers_all            qlha,
             xxwc.XXWC_OM_CONTRACT_PRICING_HDR   xocph,
             apps.qp_list_lines                  qll,
             xxwc.XXWC_OM_CONTRACT_PRICING_LINES xocpl
       where qlha.LIST_HEADER_ID = qll.list_header_id
         and qlha.attribute14 = xocph.agreement_id
            --  and xocph.agreement_id = xocpl.agreement_id
         and qll.attribute2 = xocpl.agreement_line_id
            -- and qlha.list_header_id IN (3658769);--,3648317, 3648315)
         and xocph.request_id = fnd_global.CONC_REQUEST_ID;
  begin
    errbuf  := NULL;
    retcode := '0';
    /*  v_from_date := nvl(to_date(p_from_date, 'YYYY/MM/DD HH24:MI:SS'),
                       to_date(sysdate, 'YYYY/MM/DD HH24:MI:SS'));
    v_to_date   := nvl(to_date(p_to_date, 'YYYY/MM/DD HH24:MI:SS'),
                       to_date(sysdate, 'YYYY/MM/DD HH24:MI:SS'));*/
    v_from_date := nvl(to_date(trim(SUBSTR(p_from_date,1,10)), 'YYYY/MM/DD'),
                       to_date(sysdate, 'YYYY/MM/DD'));
    v_to_date   := nvl(to_date(trim(SUBSTR(p_to_date,1,10)), 'YYYY/MM/DD'),
                       to_date(sysdate, 'YYYY/MM/DD'));
    OPEN header_to_be_processed;
    -- LOOP
    FETCH header_to_be_processed
      into l_header_to_count;
    --   EXIT WHEN header_processed%notfound;
    --dbms_output.put_line('Header to be Processed' || ' - ' || l_header_to_count);
    fnd_file.put_line(fnd_file.log,
                      'Header to be Processed' || ' - ' ||
                      l_header_to_count);
    -- END LOOP;
    CLOSE header_to_be_processed;
  
    --read base table header data
    for i in (select rowid,
                     context,
                     list_header_id,
                     creation_date,
                     start_date_active,
                     end_date_active,
                     automatic_flag,
                     created_by,
                     last_update_date,
                     last_updated_by,
                     last_update_login,
                     name,
                     description,
                     version_no,
                     comments,
                     active_flag,
                     orig_system_header_ref,
                     global_flag,
                     orig_org_id,
                     view_flag,
                     update_flag
                from apps.qp_list_headers_all
               where attribute10 = 'Segmented Price'
                 and active_flag = 'Y'
                    /* and list_header_id IN (3678801,
                    3676039,
                    3688032,
                    3697967,
                    3701870,
                    3706752,
                    3706759,
                    \*3712877,*\
                    \*3735385,
                    3752310,*\
                    3793502,
                    3784535,
                    3798390,
                    3798391,
                    3784473)*/
                    /* and nvl(to_date(to_char(creation_date, 'YYYY/MM/DD'),
                                'YYYY/MM/DD HH24:MI:SS'),
                        to_date(sysdate, 'YYYY/MM/DD HH24:MI:SS')) between
                    v_from_date and v_to_date)*/
                 and nvl(to_date(to_char(creation_date, 'YYYY/MM/DD'),
                                 'YYYY/MM/DD'),
                         to_date(sysdate, 'YYYY/MM/DD')) between v_from_date and
                     v_to_date) loop
      begin
        OPEN lines_to_be_processed(i.list_header_id);
        -- LOOP
        FETCH lines_to_be_processed
          into l_lines_to_count;
        --   EXIT WHEN header_processed%notfound;
        -- dbms_output.put_line('Lines to be Processed' || ' - ' || l_lines_to_count);
        /* fnd_file.put_line(fnd_file.log,
        'Lines to be Processed' || ' - ' || l_lines_to_count);*/
        -- END LOOP;
        CLOSE lines_to_be_processed;
        --  l_organization_id :=0;
        -- get the new sequence id for custom table
        select XXWC_OM_CONTRACT_PRICING_HDR_S.nextval
          into l_agreement_id
          from dual;
        /* dbms_output.put_line('l_agreement_id:' || l_agreement_id ||
        'TIME: ' ||
        TO_CHAR(SYSTIMESTAMP,
                'DD-MON-YYYY HH24:MI:SS'));*/
        -- get additional required parameters to fill in custom header table
        -- here we need to check the price type
        begin
          select QUALIFIER_CONTEXT,
                 QUALIFIER_ATTRIBUTE,
                 QUALIFIER_ATTR_VALUE
            into l_QUALIFIER_CONTEXT,
                 l_qualifier_attribute,
                 l_QUALIFIER_ATTR_VALUE
            from apps.qp_qualifiers_v
           where list_header_id = i.list_header_id
             and rownum = 1;
          /*  dbms_output.put_line('l_QUALIFIER_CONTEXT:' || l_QUALIFIER_CONTEXT ||
                               'TIME: ' ||
                               TO_CHAR(SYSTIMESTAMP,
                                       'DD-MON-YYYY HH24:MI:SS'));
          dbms_output.put_line('l_qualifier_attribute:' ||
                               l_qualifier_attribute);*/
        
          if l_QUALIFIER_CONTEXT = 'CUSTOMER' and
             l_qualifier_attribute = 'QUALIFIER_ATTRIBUTE32' then
            l_price_type := 'MASTER';
          end if;
          /* dbms_output.put_line('l_price_type:' || l_price_type);*/
        
          if l_QUALIFIER_CONTEXT = 'CUSTOMER' and
             l_qualifier_attribute = 'QUALIFIER_ATTRIBUTE11' then
            l_price_type := 'SHIP TO';
          end if;
        exception
          when others then
            l_price_type := 'MASTER';
        end;
      end;
      ---check the data if it already exist or not at header level
      begin
        select count(1)
          into l_count
          from xxwc.XXWC_OM_CONTRACT_PRICING_HDR
         where customer_id = l_QUALIFIER_ATTR_VALUE
           and price_type = l_price_type
           and agreement_type = 'MTX';
        -- if it doesn't exist then insert header and lines
        if l_count = 0 then
          -- now insert the header level information in custom header table
          insert into xxwc.XXWC_OM_CONTRACT_PRICING_HDR
            (agreement_id,
             price_type,
             customer_id,
             customer_site_id,
             agreement_status,
             revision_number,
             organization_id,
             gross_margin,
             incompatability_group,
             attribute_category,
             attribute1,
             attribute2,
             attribute3,
             attribute4,
             attribute5,
             attribute6,
             attribute7,
             attribute8,
             attribute9,
             attribute10,
             attribute11,
             attribute12,
             attribute13,
             attribute14,
             attribute15,
             creation_date,
             created_by,
             last_update_date,
             last_updated_by,
             last_update_login,
             agreement_type,
             conversion_status)
          values
            (l_agreement_id,
             l_price_type,
             l_QUALIFIER_ATTR_VALUE,
             null,
             'APPROVED',
             1,
             630,
             null,
             'Best Price Wins',
             null,
             null,
             null,
             null,
             null,
             null,
             null,
             null,
             null,
             null,
             null,
             null,
             null,
             null,
             null,
             null,
             i.creation_date,
             i.created_by,
             i.last_update_date,
             i.last_updated_by,
             i.last_update_login,
             'MTX',
             'Y');
          -- insert into archive tables
          insert into XXWC.XXWC_OM_CSP_HEADER_ARCHIVE
            (agreement_id,
             price_type,
             customer_id,
             customer_site_id,
             agreement_status,
             revision_number,
             organization_id,
             gross_margin,
             incompatability_group,
             attribute_category,
             attribute1,
             attribute2,
             attribute3,
             attribute4,
             attribute5,
             attribute6,
             attribute7,
             attribute8,
             attribute9,
             attribute10,
             attribute11,
             attribute12,
             attribute13,
             attribute14,
             attribute15,
             creation_date,
             created_by,
             last_update_date,
             last_updated_by,
             last_update_login,
             agreement_type,
             conversion_status)
          values
            (l_agreement_id,
             l_price_type,
             l_QUALIFIER_ATTR_VALUE,
             null,
             'APPROVED',
             1,
             630,
             null,
             'Best Price Wins',
             null,
             null,
             null,
             null,
             null,
             null,
             null,
             null,
             null,
             null,
             null,
             null,
             null,
             null,
             null,
             null,
             i.creation_date,
             i.created_by,
             i.last_update_date,
             i.last_updated_by,
             i.last_update_login,
             'MTX',
             'Y');
          commit;
          /* dbms_output.put_line('TIME: ' ||
          TO_CHAR(SYSTIMESTAMP,
                  'DD-MON-YYYY HH24:MI:SS'));*/
          /* dbms_output.put_line('agreement id check:' || l_agreement_id);*/
          -- now update the base header table with the id to create a relationship
          lvc_row_lock := NULL;
          lvc_row_lock := apps.xxwc_om_force_ship_pkg.is_row_locked(i.rowid,
                                                                    'qp_list_headers_all');
        
          IF lvc_row_lock = 'N' THEN
          
            update qp_list_headers_all
               set attribute14 = to_char(l_agreement_id), attribute15 = '0'
             where list_header_id = i.list_header_id;
            /* dbms_output.put_line('no of records updated for i.list_header_id' ||
            i.list_header_id || ' ' || l_agreement_id || ' ' ||
            sql%rowcount || 'TIME: ' ||
            TO_CHAR(SYSTIMESTAMP,
                    'DD-MON-YYYY HH24:MI:SS'));*/
            update qp_list_headers_all
               set VERSION_NO = l_agreement_id || '.0'
             where list_header_id = i.list_header_id;
            /*  dbms_output.put_line('no of records updated for i.list_header_id' ||
            i.list_header_id || ' ' || l_agreement_id || ' ' ||
            sql%rowcount || 'TIME: ' ||
            TO_CHAR(SYSTIMESTAMP,
                    'DD-MON-YYYY HH24:MI:SS'));*/
            commit;
          end if;
          -- dbms_output.put_line('update complete to qp_list_headers_all');
          -- Pull the line level information from base table
          for a in (select rowid,
                           list_line_id,
                           creation_date,
                           created_by,
                           last_update_date,
                           last_updated_by,
                           last_update_login,
                           program_application_id,
                           program_id,
                           program_update_date,
                           request_id,
                           list_header_id,
                           list_line_type_code,
                           start_date_active,
                           end_date_active,
                           automatic_flag,
                           modifier_level_code,
                           price_by_formula_id,
                           list_price,
                           list_price_uom_code,
                           primary_uom_flag,
                           inventory_item_id,
                           organization_id,
                           related_item_id,
                           relationship_type_id,
                           substitution_context,
                           substitution_attribute,
                           substitution_value,
                           revision,
                           revision_date,
                           revision_reason_code,
                           price_break_type_code,
                           percent_price,
                           number_effective_periods,
                           effective_period_uom,
                           arithmetic_operator,
                           operand,
                           override_flag,
                           print_on_invoice_flag,
                           rebate_transaction_type_code,
                           base_qty,
                           base_uom_code,
                           accrual_qty,
                           accrual_uom_code,
                           estim_accrual_rate,
                           comments,
                           generate_using_formula_id,
                           reprice_flag,
                           list_line_no,
                           estim_gl_value,
                           benefit_price_list_line_id,
                           expiration_period_start_date,
                           number_expiration_periods,
                           expiration_period_uom,
                           expiration_date,
                           accrual_flag,
                           pricing_phase_id,
                           pricing_group_sequence,
                           incompatibility_grp_code,
                           product_precedence,
                           proration_type_code,
                           accrual_conversion_rate,
                           benefit_qty,
                           benefit_uom_code,
                           recurring_flag,
                           benefit_limit,
                           charge_type_code,
                           charge_subtype_code,
                           context,
                           attribute1,
                           attribute2,
                           attribute3,
                           attribute4,
                           attribute5,
                           attribute6,
                           attribute7,
                           attribute8,
                           attribute9,
                           attribute10,
                           attribute11,
                           attribute12,
                           attribute13,
                           attribute14,
                           attribute15,
                           include_on_returns_flag,
                           qualification_ind,
                           limit_exists_flag,
                           group_count,
                           net_amount_flag,
                           recurring_value,
                           accum_context,
                           accum_attribute,
                           accum_attr_run_src_flag,
                           customer_item_id,
                           break_uom_code,
                           break_uom_context,
                           break_uom_attribute,
                           pattern_id,
                           product_uom_code,
                           pricing_attribute_count,
                           hash_key,
                           cache_key,
                           orig_sys_line_ref,
                           orig_sys_header_ref,
                           continuous_price_break_flag,
                           eq_flag,
                           currency_header_id,
                           null_other_oprt_count,
                           pte_code,
                           source_system_code,
                           service_duration,
                           service_period
                      from qp_list_lines
                     where list_header_id = i.list_header_id
                    /*and end_date_active is null*/
                    ) loop
            begin
              -- get the seqeunce to go in custom line level table
              select XXWC_OM_CONTRACT_PRICING_LIN_S.nextval
                into l_agreement_line_id
                from dual;
              /* dbms_output.put_line('l_agreement_line_id:' ||
              l_agreement_line_id || 'TIME: ' ||
              TO_CHAR(SYSTIMESTAMP,
                      'DD-MON-YYYY HH24:MI:SS'));*/
              -- get additional paramters to fill in custom line level table
              select product_attr_val, OPERAND, product_attr_value
                into l_product_attr_val, l_mod_value, l_product_attr_value
                from qp_modifier_summary_v
               where list_line_id = a.list_line_id
                 and rownum = 1;
              /* dbms_output.put_line('a.list_line_id:' || a.list_line_id ||
              'TIME: ' ||
              TO_CHAR(SYSTIMESTAMP,
                      'DD-MON-YYYY HH24:MI:SS'));*/
              /*  dbms_output.put_line('l_product_attr_val:' ||
                                     l_product_attr_val);
                dbms_output.put_line('l_mod_value:' || l_mod_value);
                dbms_output.put_line('l_product_attr_value:' ||
                                     l_product_attr_value);
              */
              l_mod_value1 := 100 - (l_mod_value * 100);
              --  dbms_output.put_line('l_mod_value1:' || l_mod_value1);
              IF l_product_attr_val <> 'ALL' THEN
                l_product_attr_val_num := l_product_attr_val;
              
                begin
                  select b.partnumber, b.shortdescription --b.segment1, b.description
                    into l_partnumber, l_description
                    from XXWC_MD_SEARCH_PRODUCTS_MV_S b --mtl_system_items_b b
                   where b.inventory_item_id = l_product_attr_val_num --l_product_attr_val
                     and ORGANIZATION_ID = 222;
                
                  /*  dbms_output.put_line('l_partnumber:' || l_partnumber ||
                  'TIME: ' ||
                  TO_CHAR(SYSTIMESTAMP,
                          'DD-MON-YYYY HH24:MI:SS'));
                   dbms_output.put_line('l_description:' || l_description);*/
                  -- if l_organization_id = 0 then
                  select b.organization_id
                    into l_organization_id
                    from mtl_system_items_b b
                   where b.segment1 = l_partnumber
                     and ORGANIZATION_ID not in 222
                     and rownum = 1;
                  --end if;
                  /*dbms_output.put_line('l_organization_id:' ||
                  l_organization_id || 'TIME: ' ||
                  TO_CHAR(SYSTIMESTAMP,
                          'DD-MON-YYYY HH24:MI:SS'));*/
                
                  l_product_attribute := 'ITEM';
                exception
                  when no_data_found then
                    begin
                      -- it will come here when its not sku level but cat class level
                      select item.partnumber, m_c.DESCRIPTION--item.shortdescription
                        into l_partnumber, l_description
                        from XXWC_MD_SEARCH_PRODUCTS_MV_S item,
                             mtl_item_categories          item_c,
                             mtl_categories               m_c
                       where item.ORGANIZATION_ID = item_c.ORGANIZATION_ID
                         and item.INVENTORY_ITEM_ID =
                             item_c.INVENTORY_ITEM_ID
                         and item_c.CATEGORY_ID = m_c.CATEGORY_ID
                         and item.ORGANIZATION_ID = 222
                         and item_c.category_id = l_product_attr_val_num
                         AND rownum = 1; -- l_product_attr_val;
                      /*
                      dbms_output.put_line('l_partnumber1:' ||
                      l_partnumber || 'TIME: ' ||
                      TO_CHAR(SYSTIMESTAMP,
                              'DD-MON-YYYY HH24:MI:SS'));
                      dbms_output.put_line('l_description1:' ||
                      l_description);*/
                      -- if l_organization_id = 0 then
                    
                      select b.organization_id
                        into l_organization_id
                        from mtl_system_items_b b
                       where b.segment1 = l_partnumber
                         and ORGANIZATION_ID not in 222
                         and rownum = 1;
                    
                      --  end if;
                      /* dbms_output.put_line('l_organization_id1:' ||
                      l_organization_id || 'TIME: ' ||
                      TO_CHAR(SYSTIMESTAMP,
                              'DD-MON-YYYY HH24:MI:SS'));*/
                      /*dbms_output.put_line('l_product_attr_value1:' ||
                      l_product_attr_value);*/
                      if l_product_attr_value is not null then
                        l_partnumber := l_product_attr_value;
                      end if;
                      l_product_attribute := 'CAT_CLASS';
                    exception
                      when no_data_found then
                        -- this is to handle exceptions
                        l_partnumber        := l_product_attr_val;
                        l_product_attribute := 'CAT_CLASS';
                        l_description       := l_product_attr_val;
                    end;
                end;
              ELSE
                l_partnumber        := l_product_attr_val;
                l_product_attribute := 'CAT_CLASS';
                l_description       := l_product_attr_val;
              END IF;
            end;
            begin
              insert into xxwc.XXWC_OM_CONTRACT_PRICING_LINES
                (agreement_line_id,
                 agreement_id,
                 agreement_type,
                 vendor_quote_number,
                 product_attribute,
                 product_value,
                 product_description,
                 list_price,
                 start_date,
                 end_date,
                 modifier_type,
                 modifier_value,
                 selling_price,
                 special_cost,
                 vendor_id,
                 max_quantity_terms,
                 average_cost,
                 gross_margin,
                 line_status,
                 interfaced_flag,
                 revision_number,
                 latest_rec_flag,
                 attribute_category,
                 attribute1,
                 attribute2,
                 attribute3,
                 attribute4,
                 attribute5,
                 attribute6,
                 attribute7,
                 attribute8,
                 attribute9,
                 attribute10,
                 attribute11,
                 attribute12,
                 attribute13,
                 attribute14,
                 attribute15,
                 creation_date,
                 created_by,
                 last_update_date,
                 last_updated_by,
                 last_update_login,
                 calc_selling_price,
                 list_header_id,
                 conversion_status,
                 CATEGORY_ID)
              values
                (l_agreement_line_id,
                 l_agreement_id,
                 'MTX',
                 null,
                 l_product_attribute,
                 l_partnumber,
                 l_description,
                 null,
                 a.start_date_active,
                 a.end_date_active,
                 'Percent',
                 l_mod_value1,
                 null,
                 null,
                 null,
                 null,
                 null,
                 null,
                 'APPROVED',
                 'Y',
                 1,
                 'Y',
                 null,
                 null,
                 null,
                 null,
                 null,
                 null,
                 null,
                 null,
                 null,
                 null,
                 null,
                 null,
                 null,
                 null,
                 null,
                 null,
                 a.creation_date,
                 a.created_by,
                 a.last_update_date,
                 a.last_updated_by,
                 a.last_update_login,
                 null,
                 i.list_header_id,
                 'Y',
                 l_product_attr_val);
            exception
              when others then
                -- dbms_output.put_line('line inserting error: list header id:' || a.list_header_id ||'- list line id:' || a.list_line_id);
                fnd_file.put_line(fnd_file.log,
                                  'line inserting error: list header id:' ||
                                  a.list_header_id || '- list line id:' ||
                                  a.list_line_id);
            end;
            --inserting data into archive tables
            insert into XXWC.XXWC_OM_CSP_LINES_ARCHIVE
              (agreement_line_id,
               agreement_id,
               agreement_type,
               vendor_quote_number,
               product_attribute,
               product_value,
               product_description,
               list_price,
               start_date,
               end_date,
               modifier_type,
               modifier_value,
               selling_price,
               special_cost,
               vendor_id,
               max_quantity_terms,
               average_cost,
               gross_margin,
               line_status,
               interfaced_flag,
               revision_number,
               latest_rec_flag,
               attribute_category,
               attribute1,
               attribute2,
               attribute3,
               attribute4,
               attribute5,
               attribute6,
               attribute7,
               attribute8,
               attribute9,
               attribute10,
               attribute11,
               attribute12,
               attribute13,
               attribute14,
               attribute15,
               creation_date,
               created_by,
               last_update_date,
               last_updated_by,
               last_update_login,
               calc_selling_price,
               list_header_id,
               conversion_status,
               CATEGORY_ID)
            values
              (l_agreement_line_id,
               l_agreement_id,
               'MTX',
               null,
               l_product_attribute,
               l_partnumber,
               l_description,
               null,
               a.start_date_active,
               a.end_date_active,
               'Percent',
               l_mod_value1,
               null,
               null,
               null,
               null,
               null,
               null,
               'APPROVED',
               'Y',
               1,
               'Y',
               null,
               null,
               null,
               null,
               null,
               null,
               null,
               null,
               null,
               null,
               null,
               null,
               null,
               null,
               null,
               null,
               a.creation_date,
               a.created_by,
               a.last_update_date,
               a.last_updated_by,
               a.last_update_login,
               null,
               i.list_header_id,
               'Y',
               l_product_attr_val);
            /*dbms_output.put_line('l_agreement_line_id1:' ||
            l_agreement_line_id || 'TIME: ' ||
            TO_CHAR(SYSTIMESTAMP,
                    'DD-MON-YYYY HH24:MI:SS'));*/
            lvc_row_lock := NULL;
            lvc_row_lock := apps.xxwc_om_force_ship_pkg.is_row_locked(a.rowid,
                                                                      'qp_list_lines');
          
            IF lvc_row_lock = 'N' THEN
              update qp_list_lines
                 set attribute2 = l_agreement_line_id,
                     attribute5 = l_modifier_type_dff
               where list_line_id = a.list_line_id;
            end if;
          end loop;
        
          /*dbms_output.put_line('l_agreement_line_id2:' ||
          l_agreement_line_id || 'TIME: ' ||
          TO_CHAR(SYSTIMESTAMP,
                  'DD-MON-YYYY HH24:MI:SS'));*/
        
          update xxwc.XXWC_OM_CONTRACT_PRICING_HDR
             set organization_id = l_organization_id
           where agreement_id = l_agreement_id;
        
          /*dbms_output.put_line('l_agreement_line_id3:' ||
          l_agreement_line_id || 'TIME: ' ||
          TO_CHAR(SYSTIMESTAMP,
                  'DD-MON-YYYY HH24:MI:SS'));*/
        
          update xxwc.XXWC_OM_CSP_HEADER_ARCHIVE
             set organization_id = l_organization_id
           where agreement_id = l_agreement_id;
        
          /*dbms_output.put_line('l_agreement_line_id2:' ||
          l_agreement_line_id || 'TIME: ' ||
          TO_CHAR(SYSTIMESTAMP,
                  'DD-MON-YYYY HH24:MI:SS'));*/
          -- now if header exist so we have to check if lines exist or not if doesn't exist then insert
        
          commit;
        else
          begin
            select c.agreement_id, h.list_header_id
              into l_agreement_id1, l_list_header_id1
              from xxwc.XXWC_OM_CONTRACT_PRICING_HDR c,
                   qp_list_headers_All               h
             where customer_id = l_QUALIFIER_ATTR_VALUE
               and price_type = l_price_type
               and h.ATTRIBUTE14 = c.agreement_id
               and agreement_type = 'MTX'
               and h.LIST_HEADER_ID = i.list_header_id;
            /* select agreement_id
             into l_agreement_id1
             from xxwc.XXWC_OM_CONTRACT_PRICING_HDR
            where customer_id = l_QUALIFIER_ATTR_VALUE
              and price_type = l_price_type
              and agreement_type = 'MTX';*/
            begin
              select count(1)
                into l_count_l
                from qp_list_lines
               where list_header_id = l_list_header_id1
                 and attribute2 is null;
            exception
              when others then
                l_count := 0;
            end;
            if l_count_l > 0 then
              for a in (select list_line_id,
                               creation_date,
                               created_by,
                               last_update_date,
                               last_updated_by,
                               last_update_login,
                               program_application_id,
                               program_id,
                               program_update_date,
                               request_id,
                               list_header_id,
                               list_line_type_code,
                               start_date_active,
                               end_date_active,
                               automatic_flag,
                               modifier_level_code,
                               price_by_formula_id,
                               list_price,
                               list_price_uom_code,
                               primary_uom_flag,
                               inventory_item_id,
                               organization_id,
                               related_item_id,
                               relationship_type_id,
                               substitution_context,
                               substitution_attribute,
                               substitution_value,
                               revision,
                               revision_date,
                               revision_reason_code,
                               price_break_type_code,
                               percent_price,
                               number_effective_periods,
                               effective_period_uom,
                               arithmetic_operator,
                               operand,
                               override_flag,
                               print_on_invoice_flag,
                               rebate_transaction_type_code,
                               base_qty,
                               base_uom_code,
                               accrual_qty,
                               accrual_uom_code,
                               estim_accrual_rate,
                               comments,
                               generate_using_formula_id,
                               reprice_flag,
                               list_line_no,
                               estim_gl_value,
                               benefit_price_list_line_id,
                               expiration_period_start_date,
                               number_expiration_periods,
                               expiration_period_uom,
                               expiration_date,
                               accrual_flag,
                               pricing_phase_id,
                               pricing_group_sequence,
                               incompatibility_grp_code,
                               product_precedence,
                               proration_type_code,
                               accrual_conversion_rate,
                               benefit_qty,
                               benefit_uom_code,
                               recurring_flag,
                               benefit_limit,
                               charge_type_code,
                               charge_subtype_code,
                               context,
                               attribute1,
                               attribute2,
                               attribute3,
                               attribute4,
                               attribute5,
                               attribute6,
                               attribute7,
                               attribute8,
                               attribute9,
                               attribute10,
                               attribute11,
                               attribute12,
                               attribute13,
                               attribute14,
                               attribute15,
                               include_on_returns_flag,
                               qualification_ind,
                               limit_exists_flag,
                               group_count,
                               net_amount_flag,
                               recurring_value,
                               accum_context,
                               accum_attribute,
                               accum_attr_run_src_flag,
                               customer_item_id,
                               break_uom_code,
                               break_uom_context,
                               break_uom_attribute,
                               pattern_id,
                               product_uom_code,
                               pricing_attribute_count,
                               hash_key,
                               cache_key,
                               orig_sys_line_ref,
                               orig_sys_header_ref,
                               continuous_price_break_flag,
                               eq_flag,
                               currency_header_id,
                               null_other_oprt_count,
                               pte_code,
                               source_system_code,
                               service_duration,
                               service_period
                          from qp_list_lines
                         where list_header_id = l_list_header_id1
                           and attribute2 is null
                        /*and end_date_active is null*/
                        ) loop
              
                begin
                  -- get the seqeunce to go in custom line level table
                  select XXWC_OM_CONTRACT_PRICING_LIN_S.nextval
                    into l_agreement_line_id
                    from dual;
                  /*dbms_output.put_line('l_agreement_line_id:' ||
                  l_agreement_line_id || 'TIME: ' ||
                  TO_CHAR(SYSTIMESTAMP,
                          'DD-MON-YYYY HH24:MI:SS'));*/
                  -- get additional paramters to fill in custom line level table
                  select product_attr_val, OPERAND, product_attr_value
                    into l_product_attr_val,
                         l_mod_value,
                         l_product_attr_value
                    from qp_modifier_summary_v
                   where list_line_id = a.list_line_id
                     and rownum = 1;
                  /* dbms_output.put_line('a.list_line_id:' || a.list_line_id ||
                                       'TIME: ' ||
                                       TO_CHAR(SYSTIMESTAMP,
                                               'DD-MON-YYYY HH24:MI:SS'));
                  dbms_output.put_line('l_product_attr_val:' ||
                                       l_product_attr_val);
                  dbms_output.put_line('l_mod_value:' || l_mod_value);
                  dbms_output.put_line('l_product_attr_value:' ||
                                       l_product_attr_value);*/
                
                  IF l_product_attr_val <> 'ALL' THEN
                    l_product_attr_val_num := l_product_attr_val;
                  
                    l_mod_value1 := 100 - (l_mod_value * 100);
                    /*dbms_output.put_line('l_mod_value1:' || l_mod_value1);*/
                  
                    begin
                      select b.partnumber, b.shortdescription --b.segment1, b.description
                        into l_partnumber, l_description
                        from XXWC_MD_SEARCH_PRODUCTS_MV_S b --mtl_system_items_b b
                       where b.inventory_item_id = l_product_attr_val_num --l_product_attr_val
                         and ORGANIZATION_ID = 222;
                    
                      /*dbms_output.put_line('l_partnumber:' || l_partnumber ||
                                           'TIME: ' ||
                                           TO_CHAR(SYSTIMESTAMP,
                                                   'DD-MON-YYYY HH24:MI:SS'));
                      dbms_output.put_line('l_description:' ||
                                           l_description);*/
                      --   if l_organization_id = 0 then
                    
                      select b.organization_id
                        into l_organization_id
                        from mtl_system_items_b b
                       where b.segment1 = l_partnumber
                         and ORGANIZATION_ID not in 222
                         and rownum = 1;
                      --  end if;
                      /* dbms_output.put_line('l_organization_id:' ||
                      l_organization_id || 'TIME: ' ||
                      TO_CHAR(SYSTIMESTAMP,
                              'DD-MON-YYYY HH24:MI:SS'));*/
                    
                      l_product_attribute := 'ITEM';
                    exception
                      when no_data_found then
                        begin
                          select item.partnumber,m_c.DESCRIPTION -- item.shortdescription
                            into l_partnumber, l_description
                            from XXWC_MD_SEARCH_PRODUCTS_MV_S item,
                                 mtl_item_categories          item_c,
                                 mtl_categories               m_c
                           where item.ORGANIZATION_ID =
                                 item_c.ORGANIZATION_ID
                             and item.INVENTORY_ITEM_ID =
                                 item_c.INVENTORY_ITEM_ID
                             and item_c.CATEGORY_ID = m_c.CATEGORY_ID
                             and item.ORGANIZATION_ID = 222
                             and item_c.category_id =
                                 l_product_attr_val_num
                             AND rownum = 1; -- l_product_attr_val;
                        
                          /*dbms_output.put_line('l_partnumber1:' ||
                                               l_partnumber || 'TIME: ' ||
                                               TO_CHAR(SYSTIMESTAMP,
                                                       'DD-MON-YYYY HH24:MI:SS'));
                          dbms_output.put_line('l_description1:' ||
                                               l_description);*/
                          -- if l_organization_id = 0 then
                        
                          select b.organization_id
                            into l_organization_id
                            from mtl_system_items_b b
                           where b.segment1 = l_partnumber
                             and ORGANIZATION_ID not in 222
                             and rownum = 1;
                          --   end if;
                          /*  dbms_output.put_line('l_organization_id1:' ||
                          l_organization_id ||
                          'TIME: ' ||
                          TO_CHAR(SYSTIMESTAMP,
                                  'DD-MON-YYYY HH24:MI:SS'));*/
                          /* dbms_output.put_line('l_product_attr_value1:' ||
                          l_product_attr_value);*/
                          if l_product_attr_value is not null then
                            l_partnumber := l_product_attr_value;
                          end if;
                          l_product_attribute := 'CAT_CLASS';
                        exception
                          when no_data_found then
                            -- this is to handle exceptions
                            l_partnumber        := l_product_attr_val;
                            l_product_attribute := 'CAT_CLASS';
                            l_description       := l_product_attr_val;
                        end;
                    end;
                  ELSE
                    l_partnumber        := l_product_attr_val;
                    l_product_attribute := 'CAT_CLASS';
                    l_description       := l_product_attr_val;
                  END IF;
                end;
                begin
                  insert into xxwc.XXWC_OM_CONTRACT_PRICING_LINES
                    (agreement_line_id,
                     agreement_id,
                     agreement_type,
                     vendor_quote_number,
                     product_attribute,
                     product_value,
                     product_description,
                     list_price,
                     start_date,
                     end_date,
                     modifier_type,
                     modifier_value,
                     selling_price,
                     special_cost,
                     vendor_id,
                     max_quantity_terms,
                     average_cost,
                     gross_margin,
                     line_status,
                     interfaced_flag,
                     revision_number,
                     latest_rec_flag,
                     attribute_category,
                     attribute1,
                     attribute2,
                     attribute3,
                     attribute4,
                     attribute5,
                     attribute6,
                     attribute7,
                     attribute8,
                     attribute9,
                     attribute10,
                     attribute11,
                     attribute12,
                     attribute13,
                     attribute14,
                     attribute15,
                     creation_date,
                     created_by,
                     last_update_date,
                     last_updated_by,
                     last_update_login,
                     calc_selling_price,
                     list_header_id,
                     conversion_status,
                     CATEGORY_ID)
                  values
                    (l_agreement_line_id,
                     l_agreement_id1,
                     'MTX',
                     null,
                     l_product_attribute,
                     l_partnumber,
                     l_description,
                     null,
                     a.start_date_active,
                     a.end_date_active,
                     'Percent',
                     l_mod_value1,
                     null,
                     null,
                     null,
                     null,
                     null,
                     null,
                     'APPROVED',
                     'Y',
                     1,
                     'Y',
                     null,
                     null,
                     null,
                     null,
                     null,
                     null,
                     null,
                     null,
                     null,
                     null,
                     null,
                     null,
                     null,
                     null,
                     null,
                     null,
                     a.creation_date,
                     a.created_by,
                     a.last_update_date,
                     a.last_updated_by,
                     a.last_update_login,
                     null,
                     i.list_header_id,
                     'Y',
                     --  l_partnumber
                     l_product_attr_val);
                  commit;
                exception
                  when others then
                    --  dbms_output.put_line('line inserting error: list header id:' || a.list_header_id ||'- list line id:' || a.list_line_id);
                    fnd_file.put_line(fnd_file.log,
                                      'line inserting error: list header id:' ||
                                      a.list_header_id || '- list line id:' ||
                                      a.list_line_id);
                end;
                --inserting data into archive tables
                insert into XXWC.XXWC_OM_CSP_LINES_ARCHIVE
                  (agreement_line_id,
                   agreement_id,
                   agreement_type,
                   vendor_quote_number,
                   product_attribute,
                   product_value,
                   product_description,
                   list_price,
                   start_date,
                   end_date,
                   modifier_type,
                   modifier_value,
                   selling_price,
                   special_cost,
                   vendor_id,
                   max_quantity_terms,
                   average_cost,
                   gross_margin,
                   line_status,
                   interfaced_flag,
                   revision_number,
                   latest_rec_flag,
                   attribute_category,
                   attribute1,
                   attribute2,
                   attribute3,
                   attribute4,
                   attribute5,
                   attribute6,
                   attribute7,
                   attribute8,
                   attribute9,
                   attribute10,
                   attribute11,
                   attribute12,
                   attribute13,
                   attribute14,
                   attribute15,
                   creation_date,
                   created_by,
                   last_update_date,
                   last_updated_by,
                   last_update_login,
                   calc_selling_price,
                   list_header_id,
                   conversion_status,
                   CATEGORY_ID)
                values
                  (l_agreement_line_id,
                   l_agreement_id1,
                   'MTX',
                   null,
                   l_product_attribute,
                   l_partnumber,
                   l_description,
                   null,
                   a.start_date_active,
                   a.end_date_active,
                   'Percent',
                   l_mod_value1,
                   null,
                   null,
                   null,
                   null,
                   null,
                   null,
                   'APPROVED',
                   'Y',
                   1,
                   'Y',
                   null,
                   null,
                   null,
                   null,
                   null,
                   null,
                   null,
                   null,
                   null,
                   null,
                   null,
                   null,
                   null,
                   null,
                   null,
                   null,
                   a.creation_date,
                   a.created_by,
                   a.last_update_date,
                   a.last_updated_by,
                   a.last_update_login,
                   null,
                   i.list_header_id,
                   'Y',
                   --l_partnumber
                   l_product_attr_val);
                /*dbms_output.put_line('l_agreement_line_id1:' ||
                l_agreement_line_id || 'TIME: ' ||
                TO_CHAR(SYSTIMESTAMP,
                        'DD-MON-YYYY HH24:MI:SS'));*/
                update qp_list_lines
                   set attribute2 = l_agreement_line_id,
                       attribute5 = l_modifier_type_dff
                 where list_line_id = a.list_line_id;
                commit;
              end loop;
              update xxwc.XXWC_OM_CONTRACT_PRICING_HDR
                 set organization_id = l_organization_id
               where agreement_id = l_agreement_id;
            
              update xxwc.XXWC_OM_CSP_HEADER_ARCHIVE
                 set organization_id = l_organization_id
               where agreement_id = l_agreement_id;
            
              commit;
            else
              --new changes for update
              for e in (select attribute2,
                               end_date_active,
                               100 - (operand * 100) modifier_value
                          from qp_list_lines
                         where list_header_id = l_list_header_id1) loop
                begin
                  select nvl(l.modifier_value, 0),
                         nvl(l.end_date, sysdate - 365) end_date
                    into l_modifier_value, l_end_date
                    from xxwc.XXWC_OM_CONTRACT_PRICING_LINES l
                   where l.agreement_line_id = e.attribute2;
                end;
                if l_modifier_value != e.modifier_value then
                  update xxwc.XXWC_OM_CONTRACT_PRICING_LINES
                     set modifier_value = e.modifier_value
                   where agreement_line_id = e.attribute2;
                  update XXWC.XXWC_OM_CSP_LINES_ARCHIVE
                     set modifier_value = e.modifier_value
                   where agreement_line_id = e.attribute2;
                  commit;
                end if;
                if l_end_date != e.end_date_active then
                  update xxwc.XXWC_OM_CONTRACT_PRICING_LINES
                     set end_date = e.end_date_active
                   where agreement_line_id = e.attribute2;
                  update XXWC.XXWC_OM_CSP_LINES_ARCHIVE
                     set end_date = e.end_date_active
                   where agreement_line_id = e.attribute2;
                  commit;
                end if;
              end loop;
            
            end if;
            --   end if;
          exception
            when others then
              null;
          end;
        end if;
      exception
        when others then
          null;
      end;
    end loop;
    OPEN header_processed;
    -- LOOP
    FETCH header_processed
      into l_header_count;
    --   EXIT WHEN header_processed%notfound;
    --   dbms_output.put_line('Header Processed' || ' - ' || l_header_count);
    fnd_file.put_line(fnd_file.log,
                      'Header Processed' || ' - ' || l_header_count);
  
    -- END LOOP;
    CLOSE header_processed;
    OPEN lines_processed;
    --  LOOP
    FETCH lines_processed
      into l_lines_count;
    --       dbms_output.put_line('lines Processed' || ' - ' || l_lines_count);
    fnd_file.put_line(fnd_file.LOG,
                      'Lines Processed' || ' - ' || l_lines_count);
    --  END LOOP;
    CLOSE lines_processed;
  exception
    when others then
      retcode := '1';
      errbuf  := sqlerrm;
    
  end;
end XXWC_OM_PRICING_CONV_PKG;
/