/*************************************************************************
    *   SCRIPT Name: XXWC_OM_CONTRACT_PRICING_HDR_ALTER
    *
    *   PURPOSE:   Alter table XXWC_OM_CONTRACT_PRICING_HDR
	
	HISTORY
    ===============================================================================
    VERSION DATE          AUTHOR(S)              DESCRIPTION
    ------- -----------   --------------- -----------------------------------------
    1.0     01/16/2018     Niraj K ranjan   TMS#20180216-00104   CSP Form Should be modified to accommodate Matrix modifiers--Matrix Maintenance
*****************************************************************************/
UPDATE xxwc.xxwc_om_contract_pricing_hdr
SET agreement_type = decode(INCOMPATABILITY_GROUP,'Best Price Wins','MTX','CSP')
WHERE agreement_type IS NULL;
COMMIT;