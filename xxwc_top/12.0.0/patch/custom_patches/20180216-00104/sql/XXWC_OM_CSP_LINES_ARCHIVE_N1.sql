/*************************************************************************
    *   SCRIPT Name: xxwc.XXWC_OM_CSP_LINES_ARCHIVE
    *
    *   PURPOSE:   Indexes on xxwc.XXWC_OM_CSP_LINES_ARCHIVE
	
	HISTORY
    ===============================================================================
    VERSION DATE          AUTHOR(S)              DESCRIPTION
    ------- -----------   --------------- -----------------------------------------
    1.0     01/16/2018     Nancy Pahwa   TMS#20170201-00276   CSP Form Should be modified to accommodate Matrix modifiers--Matrix Maintenance
*****************************************************************************/
create index xxwc.XXWC_AGREEMENT_ID_N1 on xxwc.XXWC_OM_CSP_LINES_ARCHIVE (agreement_id);
