/*************************************************************************
    *   SCRIPT Name: XXWC.XXWC_OM_CONTRACT_PRICING_HDR 
    *
    *   PURPOSE:   Create Table XXWC.XXWC_OM_CONTRACT_PRICING_HDR 
	
	HISTORY
    ===============================================================================
    VERSION DATE          AUTHOR(S)              DESCRIPTION
    ------- -----------   --------------- -----------------------------------------
    1.0     01/16/2018     Nancy Pahwa   TMS#20170201-00276  grants
*****************************************************************************/
grant select on XXWC.XXWC_OM_CONTRACT_PRICING_HDR to EA_APEX;
/