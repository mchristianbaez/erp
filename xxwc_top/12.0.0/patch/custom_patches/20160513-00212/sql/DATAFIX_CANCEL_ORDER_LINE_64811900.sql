REM  $Header: ontd0008.sql 115.0.1159.8 2009/11/25 08:55:27 sahvivek noship $
REM +=======================================================================+
REM |    Copyright (c) 2003 Oracle Corporation, Redwood Shores, CA, USA     |
REM |                         All rights reserved.                          |
REM +=======================================================================+
REM | FILENAME                                                              |
REM |     ontd0008.sql                                                      |
REM |                                                                       |
REM | DESCRIPTION                                                           |
REM |     This script has been created to allow cancelling those lines      |
REM |     which are stuck due to some data corruption  and can neither      |
REM |     be progressed nor cancelled from the application. You may only use|
REM |     it on order lines as authorized by Oracle Support.                |
REM |     This script will do the following:                                |
REM |                                                                       |
REM |     1. Update Lines to show as if Cancelled.                          |
REM |     2. Cancel Delivery Details(if any) associated to order lines.     |
REM |     3. Marks the Move Order Lines as 'Cancelled by Source' for any    |
REM |        'Released to Warehouse' details (for INV / OPM Orgs)           |
REM |     4. Deletes any Serial Number(s) associated with Staged/Shipped    |
REM |        Delivery Details and Unmarks them                              |
REM |     5. Cancel OPM Inventory data in case of OPM Org                   |
REM |     6. Progress flow(if exists) to CLOSE.                             |
REM |     7. Removes the line from any fulfillment set.                     |
REM |                                                                       |
REM |     Note that this script DOES NOT:                                   |
REM |                                                                       |
REM |     1. Cancel Line if it belongs to (a)WMS Organization; and          |
REM |        (b)has open delivery details                                   |
REM |     2. Cancel Line if OTM Integration is enabled for Organization     |
REM |     3. Delink the Configured Items  that  may exist on  an order.     |
REM |     4. Update the  history tables with  cancellation information.     |
REM |     5. Unschedule the Lines. Inventory  patch # 2471362 should be     |
REM |        applied  after running this  script to relieve the demand.     |
REM |     6. Update Move Order Lines, log a clean bug against inventory     |
REM |     7. Update Supply.                                                 |
REM |                                                                       |
REM | DISCLAIMER                                                            |
REM |     Do not use  this script as a replacement for OM Cancellation      |
REM |     functionality as it is  strictly meant for those stuck  order     |
REM |     lines which can neither be progressed nor cancelled from the      |
REM |     application.                                                      |
REM |                                                                       |
REM |     Use this script at your own risk. The script has been tested      |
REM |     and appears to works as intended. However, you should always      |
REM |     test any script before relying on it.                             |
REM |                                                                       |
REM |     Proofread the script prior to running it. Due to differences      |
REM |     in the way text editors,email packages and operating systems      |
REM |     handle text  formatting (spaces, tabs and carriage returns),      |
REM |     this script may not be in an executable state when you first      |
REM |     receive it.  Check over the script to  ensure that errors of      |
REM |     this type are corrected.                                          |
REM |                                                                       |
REM |     Do not remove disclaimer paragraph.                               |
REM |                                                                       |
REM | INPUT/OUTPUT                                                          |
REM |     Inputs : Line Id(Required)                                        |
REM |                                                                       |
REM |     Output : Report is printed to an O/S file named line_id.lst       |
REM |                                                                       |
REM | NOTE                                                                  |
REM |     This script should be tested in TEST Instance first.              |
REM |     If results are  satisfactory in TEST, then only use in            |
REM |     PRODUCTION Instance.                                              |
REM |                                                                       |
REM | HISTORY                                                               |
REM |     14-MAR-2002              Tarun Bharti                 Created     |
REM +=======================================================================+

REM dbdrv:none

SET SERVEROUTPUT ON SIZE 500000


DECLARE
   l_lin_id                 NUMBER := 64811900; --line id for line #2.1 of SO 19684109
   l_lin_key                VARCHAR2 (30) := TO_CHAR (l_lin_id);

   l_ordered_qty            NUMBER;
   l_flow_exists            VARCHAR2 (1);
   l_user_id                NUMBER;
   l_resp_id                NUMBER;
   l_resp_appl_id           NUMBER;

   CURSOR line_check
   IS
      SELECT line_id
        FROM oe_order_lines_all
       WHERE     line_id = l_lin_id
             AND (   open_flag = 'N'
                  OR NVL (invoiced_quantity, 0) > 0
                  OR INVOICE_INTERFACE_STATUS_CODE = 'YES');

   CURSOR service_check
   IS
      SELECT COUNT (*)
        FROM oe_order_lines_all
       WHERE     service_reference_type_code = 'ORDER'
             AND service_reference_line_id = l_lin_id
             AND open_flag = 'Y';

   -- Cursors to fetch details and their Serial Number(s)
   CURSOR wdd
   IS
      SELECT delivery_detail_id,
             transaction_temp_id,
             serial_number,
             inventory_item_id,
             to_serial_number
        FROM wsh_delivery_details
       WHERE     source_line_id = l_lin_id
             AND source_code = 'OE'
             AND released_status IN ('Y', 'C');

   CURSOR msnt (txn_temp_id NUMBER)
   IS
      SELECT fm_serial_number, to_serial_number
        FROM mtl_serial_numbers_temp
       WHERE transaction_temp_id = txn_temp_id;

   l_line_check             NUMBER;
   l_oe_interfaced          NUMBER;
   l_wms_org                VARCHAR2 (1);
   l_otm_installed          VARCHAR2 (1);
   l_otm_enabled            VARCHAR2 (1);
   l_cursor                 INTEGER;
   l_stmt                   VARCHAR2 (4000);
   l_up_cursor              INTEGER;
   l_up_stmt                VARCHAR2 (4000);
   l_ignore                 NUMBER;
   l_ship_from_org_id       NUMBER;
   l_opm_enabled            BOOLEAN;
   l_fm_serial              VARCHAR2 (30);
   l_to_serial              VARCHAR2 (30);

   Line_Cannot_Be_Updated   EXCEPTION;

   l_heading                VARCHAR2 (1) := 'N';

   CURSOR wsh_ifaced
   IS
      SELECT SUBSTR (wdd.source_line_number, 1, 15) line_num,
             SUBSTR (wdd.item_description, 1, 30) item_name,
             wdd.shipped_quantity,
             wdd.source_line_id line_id
        FROM wsh_delivery_details wdd, oe_order_lines_all oel
       WHERE     wdd.inv_interfaced_flag = 'Y'
             AND NVL (wdd.shipped_quantity, 0) > 0
             AND oel.line_id = wdd.source_line_id
             AND oel.open_flag = 'N'
             AND oel.ordered_quantity = 0
             AND wdd.source_code = 'OE'
             AND oel.line_id = l_lin_id
             AND EXISTS
                    (SELECT 'x'
                       FROM mtl_material_transactions mmt
                      WHERE     wdd.delivery_detail_id = mmt.picking_line_id
                            AND mmt.trx_source_line_id = wdd.source_line_id
                            AND mmt.transaction_source_type_id IN (2, 8));
BEGIN
   DBMS_OUTPUT.put_line ('Updating Line ID: ' || l_lin_id);

   -- Check if line can be canceled
   OPEN line_check;

   FETCH line_check INTO l_line_check;

   IF line_check%FOUND
   THEN
      CLOSE line_check;

      DBMS_OUTPUT.put_line ('Line is closed or Invoiced');
      RAISE Line_Cannot_Be_Updated;
   END IF;

   CLOSE line_check;

   OPEN service_check;

   FETCH service_check INTO l_line_check;

   IF l_line_check > 0
   THEN
      CLOSE service_check;

      DBMS_OUTPUT.put_line (
         'There exist open service lines referencing this order line.');
      RAISE Line_Cannot_Be_Updated;
   END IF;

   CLOSE service_check;

   -- Check if line belongs to WMS Org
   BEGIN
      SELECT WSH_UTIL_VALIDATE.CHECK_WMS_ORG (ship_from_org_id),
             ship_from_org_id
        INTO l_wms_org, l_ship_from_org_id
        FROM oe_order_lines_all
       WHERE line_id = l_lin_id;
   EXCEPTION
      WHEN NO_DATA_FOUND
      THEN
         DBMS_OUTPUT.put_line ('Unable to get the Organization');
         RAISE Line_Cannot_Be_Updated;
   END;

   IF l_wms_org = 'Y'
   THEN
      -- Disallow cancellation if and only if there exist open delivery detail(s) for line
      -- under consideration. (Bug 6196723)
      DECLARE
         l_exist_count   NUMBER := -1;
      BEGIN
         SELECT COUNT (*)
           INTO l_exist_count
           FROM wsh_delivery_details wdd, oe_order_lines_all line
          WHERE     line.line_id = wdd.source_line_id
                AND wdd.source_code = 'OE'
                AND line.line_id = l_lin_id
                AND NVL (wdd.released_status, 'N') <> 'D';

         IF (l_exist_count > 0)
         THEN
            DBMS_OUTPUT.put_line ('This line belongs to a WMS Organization');
            RAISE Line_Cannot_Be_Updated;
         END IF;
      END;
   END IF;

   -- Check if OTM is installed or OTM is enabled for the Organization
   --{
   l_otm_installed := NVL (FND_PROFILE.VALUE ('WSH_OTM_INSTALLED'), 'N');

   BEGIN
      l_cursor := DBMS_SQL.open_cursor;
      l_stmt :=
            'select wsp.otm_enabled from wsh_shipping_parameters wsp, oe_order_lines_all ol '
         || 'where wsp.organization_id = ol.ship_from_org_id '
         || 'and   ol.line_id = :line_id ';

      DBMS_SQL.parse (l_cursor, l_stmt, DBMS_SQL.v7);
      DBMS_SQL.define_column (l_cursor,
                              1,
                              l_otm_enabled,
                              1);
      DBMS_SQL.bind_variable (l_cursor, ':line_id', l_lin_id);
      l_ignore := DBMS_SQL.execute (l_cursor);

      IF DBMS_SQL.fetch_rows (l_cursor) > 0
      THEN
         DBMS_SQL.COLUMN_VALUE (l_cursor, 1, l_otm_enabled);
         DBMS_OUTPUT.put_line ('l_otm_enabled ' || l_otm_enabled);
      END IF;

      DBMS_SQL.close_cursor (l_cursor);
   EXCEPTION
      WHEN OTHERS
      THEN
         IF DBMS_SQL.is_open (l_cursor)
         THEN
            DBMS_SQL.close_cursor (l_cursor);
         END IF;

         IF l_otm_installed IN ('Y', 'O')
         THEN
            DBMS_OUTPUT.put_line ('l_otm_installed ' || l_otm_installed);
            DBMS_OUTPUT.put_line (
               'OTM: Integration Enabled is enabled for Order Management');
            RAISE Line_Cannot_Be_Updated;
         END IF;
   END;

   IF NVL (l_otm_enabled, 'N') = 'Y' AND l_otm_installed IN ('Y', 'O')
   THEN
      DBMS_OUTPUT.put_line (
            'l_otm_installed '
         || l_otm_installed
         || ' , l_otm_enabled '
         || l_otm_enabled);
      DBMS_OUTPUT.put_line (
         'OTM: Integration Enabled is enabled for Organization for which this Line belongs');
      RAISE Line_Cannot_Be_Updated;
   END IF;

   --}

   l_flow_exists := 'Y';

   UPDATE oe_order_lines_all
      SET flow_status_code = 'CANCELLED',
          open_flag = 'N',
          cancelled_flag = 'Y',
          ordered_quantity = 0,
          ordered_quantity2 = DECODE (ordered_quantity2, NULL, NULL, 0) -- OPM related
                                                                       ,
          cancelled_quantity = ordered_quantity + NVL (cancelled_quantity, 0),
          visible_demand_flag = NULL,
          last_updated_by = -6156992,
          last_update_date = SYSDATE
    WHERE line_id = l_lin_id;

   -- Added for bug 8532859
   DELETE FROM oe_line_sets
         WHERE line_id = l_lin_id;

   BEGIN
      SELECT number_value
        INTO l_user_id
        FROM wf_item_attribute_values
       WHERE item_type = 'OEOL' AND item_key = l_lin_key AND name = 'USER_ID';

      SELECT number_value
        INTO l_resp_id
        FROM wf_item_attribute_values
       WHERE     item_type = 'OEOL'
             AND item_key = l_lin_key
             AND name = 'RESPONSIBILITY_ID';

      SELECT number_value
        INTO l_resp_appl_id
        FROM wf_item_attribute_values
       WHERE     item_type = 'OEOL'
             AND item_key = l_lin_key
             AND name = 'APPLICATION_ID';
   EXCEPTION
      WHEN NO_DATA_FOUND
      THEN
         l_flow_exists := 'N';
   END;

   IF l_flow_exists = 'Y'
   THEN
      fnd_global.apps_initialize (l_user_id, l_resp_id, l_resp_appl_id);

      wf_engine.handleerror (OE_Globals.G_WFI_LIN,
                             l_lin_key,
                             'CLOSE_LINE',
                             'RETRY',
                             'CANCEL');
   END IF;

   FOR wsh_ifaced_rec IN wsh_ifaced
   LOOP
      IF l_heading = 'N'
      THEN
         DBMS_OUTPUT.put_line (' ');
         DBMS_OUTPUT.put_line (
            'Following Cancelled Lines have already been Interfaced to Inventory.');
         DBMS_OUTPUT.put_line (
            'Onhand Qty must be manually adjusted for these Items and Quantities.');
         DBMS_OUTPUT.put_line (' ');
         DBMS_OUTPUT.put_line (
            '+---------------+------------------------------+---------------+---------------+');
         DBMS_OUTPUT.put_line (
            '|Line No.       |Item Name                     |    Shipped Qty|        Line Id|');
         DBMS_OUTPUT.put_line (
            '+---------------+------------------------------+---------------+---------------+');
         l_heading := 'Y';
      END IF;

      DBMS_OUTPUT.put_line (
            '|'
         || RPAD (wsh_ifaced_rec.line_num, 15)
         || '|'
         || RPAD (wsh_ifaced_rec.item_name, 30)
         || '|'
         || LPAD (TO_CHAR (wsh_ifaced_rec.shipped_quantity), 15)
         || '|'
         || LPAD (TO_CHAR (wsh_ifaced_rec.line_id), 15)
         || '|');
   END LOOP;



   UPDATE wsh_delivery_assignments
      SET delivery_id = NULL,
          parent_delivery_detail_id = NULL,
          last_updated_by = -1,
          last_update_date = SYSDATE
    WHERE delivery_detail_id IN (SELECT wdd.delivery_detail_id
                                   FROM wsh_delivery_details wdd,
                                        oe_order_lines_all oel
                                  WHERE     wdd.source_line_id = oel.line_id
                                        AND wdd.source_code = 'OE'
                                        AND oel.cancelled_flag = 'Y'
                                        AND oel.line_id = l_lin_id
                                        AND released_status <> 'D');

   -- Check if Org is an OPM or Inventory Org
   l_opm_enabled :=
      INV_GMI_RSV_BRANCH.PROCESS_BRANCH (
         p_organization_id   => l_ship_from_org_id);

   IF NOT l_opm_enabled
   THEN
      --{
      -- Inventory Org
      -- Updating Move Order lines for Released To Warehouse details as 'Cancelled by Source'
      UPDATE mtl_txn_request_lines
         SET line_status = 9
       WHERE line_id IN (SELECT move_order_line_id
                           FROM wsh_delivery_details
                          WHERE     source_line_id = l_lin_id
                                AND released_status = 'S'
                                AND source_code = 'OE');

      -- Removing Serial Number(s) and Unmarking them
      FOR rec IN wdd
      LOOP
         --{
         IF rec.serial_number IS NOT NULL
         THEN
            UPDATE mtl_serial_numbers
               SET group_mark_id = NULL,
                   line_mark_id = NULL,
                   lot_line_mark_id = NULL
             WHERE     inventory_item_id = rec.inventory_item_id
                   AND serial_number BETWEEN rec.serial_number
                                         AND NVL (rec.to_serial_number,
                                                  rec.serial_number);
         ELSIF rec.transaction_temp_id IS NOT NULL
         THEN
            --{
            FOR msnt_rec IN msnt (rec.transaction_temp_id)
            LOOP
               UPDATE mtl_serial_numbers
                  SET group_mark_id = NULL,
                      line_mark_id = NULL,
                      lot_line_mark_id = NULL
                WHERE     inventory_item_id = rec.inventory_item_id
                      AND serial_number BETWEEN msnt_rec.fm_serial_number
                                            AND NVL (
                                                   msnt_rec.to_serial_number,
                                                   msnt_rec.fm_serial_number);
            END LOOP;

            DELETE FROM mtl_serial_numbers_temp
                  WHERE transaction_temp_id = rec.transaction_temp_id;

            BEGIN
               --{
               l_cursor := DBMS_SQL.open_cursor;
               l_stmt :=
                     'select fm_serial_number, to_serial_number '
                  || 'from   wsh_serial_numbers '
                  || 'where  delivery_detail_id = :delivery_detail_id ';
               DBMS_SQL.parse (l_cursor, l_stmt, DBMS_SQL.v7);
               DBMS_SQL.define_column (l_cursor,
                                       1,
                                       l_fm_serial,
                                       1);
               DBMS_SQL.define_column (l_cursor,
                                       2,
                                       l_to_serial,
                                       1);
               DBMS_SQL.bind_variable (l_cursor,
                                       ':delivery_detail_id',
                                       rec.delivery_detail_id);
               l_ignore := DBMS_SQL.execute (l_cursor);

               LOOP
                  IF DBMS_SQL.fetch_rows (l_cursor) > 0
                  THEN
                     DBMS_SQL.COLUMN_VALUE (l_cursor, 1, l_fm_serial);
                     DBMS_SQL.COLUMN_VALUE (l_cursor, 2, l_to_serial);
                     l_up_cursor := DBMS_SQL.open_cursor;
                     l_up_stmt :=
                           'update mtl_serial_numbers msn '
                        || 'set    msn.group_mark_id    = null,  '
                        || '       msn.line_mark_id     = null,  '
                        || '       msn.lot_line_mark_id = null   '
                        || 'where  msn.inventory_item_id = :inventory_item_id '
                        || 'and    msn.serial_number between :fm_serial and :to_serial ';
                     DBMS_SQL.parse (l_up_cursor, l_up_stmt, DBMS_SQL.v7);
                     DBMS_SQL.bind_variable (l_up_cursor,
                                             ':inventory_item_id',
                                             rec.inventory_item_id);
                     DBMS_SQL.bind_variable (l_up_cursor,
                                             ':fm_serial',
                                             l_fm_serial);
                     DBMS_SQL.bind_variable (l_up_cursor,
                                             ':to_serial',
                                             NVL (l_to_serial, l_fm_serial));
                     l_ignore := DBMS_SQL.execute (l_up_cursor);
                     DBMS_SQL.close_cursor (l_up_cursor);
                  ELSE
                     EXIT;
                  END IF;
               END LOOP;

               DBMS_SQL.close_cursor (l_cursor);
               l_cursor := DBMS_SQL.open_cursor;
               l_stmt :=
                     'delete from wsh_serial_numbers '
                  || 'where delivery_detail_id = :delivery_detail_id ';
               DBMS_SQL.parse (l_cursor, l_stmt, DBMS_SQL.v7);
               DBMS_SQL.bind_variable (l_cursor,
                                       ':delivery_detail_id',
                                       rec.delivery_detail_id);
               l_ignore := DBMS_SQL.execute (l_cursor);
               DBMS_SQL.close_cursor (l_cursor);
            EXCEPTION
               WHEN OTHERS
               THEN
                  IF DBMS_SQL.is_open (l_up_cursor)
                  THEN
                     DBMS_SQL.close_cursor (l_up_cursor);
                  END IF;

                  IF DBMS_SQL.is_open (l_cursor)
                  THEN
                     DBMS_SQL.close_cursor (l_cursor);
                  END IF;
            --}
            END;
         --}
         END IF;
      --}
      END LOOP;
   --}
   ELSE
      --{
      -- OPM Org
      UPDATE ic_txn_request_lines
         SET line_status = 9
       WHERE line_id IN (SELECT move_order_line_id
                           FROM wsh_delivery_details
                          WHERE     source_line_id = l_lin_id
                                AND released_status = 'S'
                                AND source_code = 'OE');

      UPDATE ic_tran_pnd
         SET delete_mark = 1
       WHERE     line_id = l_lin_id
             AND doc_type = 'OMSO'
             AND trans_qty < 0
             AND delete_mark = 0
             AND completed_ind = 0;
   --}
   END IF;

   UPDATE wsh_delivery_details
      SET released_status = 'D',
          src_requested_quantity = 0,
          src_requested_quantity2 =
             DECODE (src_requested_quantity2, NULL, NULL, 0),
          requested_quantity = 0,
          requested_quantity2 = DECODE (requested_quantity2, NULL, NULL, 0),
          shipped_quantity = 0,
          shipped_quantity2 = DECODE (shipped_quantity2, NULL, NULL, 0),
          picked_quantity = 0,
          picked_quantity2 = DECODE (picked_quantity2, NULL, NULL, 0),
          cycle_count_quantity = 0,
          cycle_count_quantity2 =
             DECODE (src_requested_quantity2, NULL, NULL, 0),
          cancelled_quantity =
             DECODE (requested_quantity,
                     0, cancelled_quantity,
                     requested_quantity),
          cancelled_quantity2 =
             DECODE (requested_quantity2,
                     NULL, NULL,
                     0, cancelled_quantity2,
                     requested_quantity2),
          subinventory = NULL,
          locator_id = NULL,
          lot_number = NULL,
          serial_number = NULL,
          to_serial_number = NULL,
          transaction_temp_id = NULL,
          revision = NULL,
          ship_set_id = NULL,
          inv_interfaced_flag = 'X',
          oe_interfaced_flag = 'X',
          last_updated_by = -1,
          last_update_date = SYSDATE
    WHERE     source_line_id = l_lin_id
          AND source_code = 'OE'
          AND released_status <> 'D'
          AND EXISTS
                 (SELECT 'x'
                    FROM oe_order_lines_all oel
                   WHERE     source_line_id = oel.line_id
                         AND oel.cancelled_flag = 'Y');

   COMMIT;
EXCEPTION
   WHEN Line_Cannot_Be_Updated
   THEN
      ROLLBACK;
      DBMS_OUTPUT.put_line (
         'This script cannot cancel this Order Line, please contact Oracle Support');
   WHEN OTHERS
   THEN
      ROLLBACK;
      DBMS_OUTPUT.put_line (SUBSTR (SQLERRM, 1, 240));
END;
/