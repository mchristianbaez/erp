DECLARE
  p_errbuf   VARCHAR2(2000);
  p_retcode  NUMBER;
  v_rec_cnt  NUMBER;
  v_filename VARCHAR2(100);

BEGIN
    p_retcode   := NULL;
    p_errbuf    := NULL;
    v_filename  := NULL;
    BEGIN
        --v_filename := c_file.file_name;
        --dbms_output.put_line('v_file_name - ' || v_filename);
        apps.xxwc_ar_bill_trust_intf_pkg.submit_job(p_errbuf
                                                   ,p_retcode
                                                   ,'XXWC_INT_FINANCE'
                                                   ,'HDS Credit Assoc Cash App Mgr - WC'
                                                   ,'ARLPLB'
                                                   ,'XXWC_AR_BILL_TRUST_IB_DIR'
                                                   ,'HDS White Cap - Org'
                                                   ,HDSUPPLYWHITECAP20170309_210000.txt);
        dbms_output.put_line('ReturnCode - ' || p_retcode);
		dbms_output.put_line('p_errbuf - ' || p_errbuf);
    END;
END
/
