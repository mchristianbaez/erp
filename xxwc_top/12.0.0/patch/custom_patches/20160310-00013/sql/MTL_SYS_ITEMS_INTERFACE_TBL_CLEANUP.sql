  /****************************************************************************************************
  Table:   	
  Description: Production Disposition 
  HISTORY
  =====================================================================================================
  VERSION DATE               AUTHOR(S)       DESCRIPTION
  ------- -----------------  --------------- ----------------------------------------------------------
  1.0     11-Feb-2016        P.Vamshidhar    Initial version TMS#20160209-00194
                                             Reporting Automation � Disposition   
  *****************************************************************************************************/
SET SERVEROUTPUT ON SIZE 1000000
BEGIN
   DBMS_OUTPUT.put_line ('Before Delete');

   DELETE FROM apps.mtl_system_items_interface
         WHERE     organization_id IN (SELECT organization_id
                                         FROM apps.org_organization_definitions
                                        WHERE operating_unit = 162)
               AND inventory_item_id IS NULL
               AND process_flag = 3;

   DBMS_OUTPUT.put_line ('Records Deleted: ' || SQL%ROWCOUNT);

   COMMIT;

   DBMS_OUTPUT.put_line ('After Delete');
EXCEPTION
   WHEN OTHERS
   THEN
      DBMS_OUTPUT.put_line ('Unable to delete ' || SQLERRM);
END;
/
 
