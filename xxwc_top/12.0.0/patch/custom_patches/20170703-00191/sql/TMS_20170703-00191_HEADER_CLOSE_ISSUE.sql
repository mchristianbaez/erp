/*************************************************************************
  $Header TMS_20170703-00191_HEADER_CLOSE_ISSUE.sql $
  Module Name: 20170703-00191  Header closed issue

  PURPOSE: Data fix  

  REVISIONS:
  Ver        Date         Author                Description
  ---------  -----------  ------------------    --------------------------
  1.0        01-AUG-2017  Pattabhi Avula        TMS#20170703-00191 

**************************************************************************/ 
set serveroutput on;
DECLARE
l_line_id NUMBER;
l_result VARCHAR2(30);
l_file_name VARCHAR2(500);
l_return_status	VARCHAR2(30);
l_msg_count		NUMBER;
l_msg_data		VARCHAR2(2000);




CURSOR lines IS
select DISTINCT OEOL.line_id , oeol.org_id
from oe_order_headers_all OEOH
, oe_order_lines_all OEOL
where ---OEOL.open_flag = 'N'
 oeol.line_id in (69235658)
--AND oeol.cancelled_flag  = 'N'
AND oeol.flow_status_code ='CANCELLED'
and OEOH.header_id = OEOL.header_id
AND EXISTS ( select 1 from wf_items itm
             where itm.item_type = 'OEOL'
             and   itm.item_key = to_char(oeol.line_id)
             AND  itm.end_date IS NULL ) ;



BEGIN

 -- Oe_debug_pub.debug_ON;
 -- Oe_debug_pub.initialize;
 -- Oe_debug_pub.setdebuglevel(5);
 -- l_file_name := Oe_debug_pub.set_debug_mode('FILE');
 -- Dbms_output.put_line('Debug file: ' || l_file_name);

      Dbms_output.put_line ('Checking Line information');

     FOR c IN lines LOOP

          l_line_id := c.line_id;
          mo_global.set_policy_context('S',c.org_id)   ;

       -- Oe_debug_pub.ADD('Closing line '||l_line_id );
        Dbms_Output.put_line('Closing line '||l_line_id );

       BEGIN
       update oe_order_lines_all
        set    cancelled_flag      = 'Y'
  ,      ordered_quantity    = 0
  ,      ordered_quantity2   = decode(ordered_quantity2,NULL,NULL,0)  -- OPM related
  ,      cancelled_quantity  = ordered_quantity + nvl(cancelled_quantity, 0)
  ,      visible_demand_flag = NULL
  ,      last_updated_by     = -6156992
  ,      last_update_date    = sysdate
  where  line_id             = l_line_id;

          wf_engine.handleerror('OEOL', to_char(l_line_id), 'CLOSE_LINE_PROCESS:CLOSE_LINE', 'RETRY', NULL);


          --  Oe_debug_pub.ADD('Result: '||l_return_status);
		  Dbms_output.put_line('Result: '||l_return_status);


         EXCEPTION
         WHEN Others THEN
            --  Oe_debug_pub.ADD('Error inside loop: '|| SQLERRM);			
			Dbms_Output.put_line('Error inside loop: '||SQLERRM);
       END;

     END LOOP;

      Dbms_Output.put_line('Script Completed');
 COMMIT;
     -- Oe_debug_pub.debug_OFF;
EXCEPTION
WHEN OTHERS THEN
      --  Oe_debug_pub.ADD('ERROR from script: '||SQLERRM);
        Dbms_Output.put_line('ERROR from script: '||SQLERRM);
         -- Oe_debug_pub.debug_OFF;
          ROLLBACK;
END;
/