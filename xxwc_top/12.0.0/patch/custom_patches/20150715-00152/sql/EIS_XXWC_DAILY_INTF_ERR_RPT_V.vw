CREATE OR REPLACE FORCE VIEW "XXEIS"."EIS_XXWC_DAILY_INTF_ERR_RPT_V" ("INV_TRANS_OP_IF", "INV_TRANS_OP_IF_OE", "PENDING_MTL_TRANS", "ORDER_IMPORT_HEADERS", "ORDER_IMPORT_LINES", "SPEEDBUILD_STUCK_ORDERS", "SO_HEADER_WF_ERRORS", "SO_LINE_WF_ERRORS", "UNPROC_SHIP_TRANS", "INVOICE_COUNT", "INVOICE_AMOUNT", "INVOICE_INTERFACE_ERRORS", "COST_MANAGER", "TRANSACTION_PENDING_COSTING", "REQUISITION_IMPORT", "CNTR_ORD_HDR_CLS_CNT", "WORKFLOW_DEFERRED", "DUPLICATE_SO")
AS
  SELECT
    --------Inventory Transaction Open Interface--------
    (
    SELECT SUM(COUNT(*))
    FROM apps.mtl_transactions_interface mti
    GROUP BY mti.organization_id ,
      source_code ,
      process_flag ,
      ERROR_CODE ,
      error_explanation
    ) INV_TRANS_OP_IF
    --------Inventory Transaction Open Interface Order Entry-------
    ,
    (SELECT SUM(COUNT(*))
    FROM apps.mtl_transactions_interface mti
    WHERE source_code = 'ORDER ENTRY'
    GROUP BY mti.organization_id ,
      source_code ,
      process_flag ,
      ERROR_CODE ,
      error_explanation
    ) INV_TRANS_OP_IF_OE
    --------Pending Material Transactions--------
    ,
    (SELECT COUNT(*)
    FROM APPS.MTL_MATERIAL_TRANSACTIONS_TEMP
    WHERE process_flag = 'E'
    ) PENDING_MTL_TRANS
    --------Order Import--------
    ,
    (SELECT COUNT(*) FROM apps.oe_headers_iface_all WHERE error_flag = 'Y'
    ) Order_Import_Headers ,
    (SELECT COUNT(*) FROM apps.oe_lines_iface_all WHERE error_flag = 'Y'
    ) ORDER_IMPORT_LINES
    ---SpeedBuild Stuck Orders-------
    ,
    (SELECT COUNT(1) FROM apps.oe_headers_iface_all WHERE order_source_id=1001
    ) SPEEDBUILD_STUCK_ORDERS
    --------------Sales order Header Workflow errors--------
    ,
    (SELECT COUNT(1)
    FROM apps.oe_order_headers_all ooh ,
      apps.wf_item_activity_statuses wias
    WHERE wias.item_type = 'OEOH'
    AND ooh.header_id    = wias.item_key
    AND activity_status  = 'ERROR'
    ) SO_Header_WF_errors
    ------------Sales order Line Workflow errors--------
    ,
    (SELECT COUNT(1)
    FROM apps.oe_order_headers_all ooh ,
      apps.oe_order_lines_all ool ,
      apps.wf_item_activity_statuses wias
    WHERE ooh.header_id           = ool.header_id
    AND wias.item_type            = 'OEOL'
    AND ool.line_id               = wias.item_key
    AND activity_status           = 'ERROR'
    AND ool.flow_status_code NOT IN ('CLOSED', 'CANCELLED')
    ) SO_LINE_WF_ERRORS
    --------------Unprocessed Shipping transaction--------
    ,
    (SELECT COUNT(1)
    FROM apps.wsh_delivery_details wdd ,
      apps.wsh_delivery_assignments wda ,
      apps.wsh_new_deliveries wnd ,
      apps.wsh_delivery_legs wdl ,
      apps.wsh_trip_stops wts ,
      apps.oe_order_headers_all ooh ,
      apps.oe_order_lines_all ool ,
      apps.gl_periods prd
    WHERE wdd.source_code        = 'OE'
    AND wdd.released_status      = 'C'
    AND wdd.inv_interfaced_flag IN ('N', 'P')
    AND wda.delivery_detail_id   = wdd.delivery_detail_id
    AND wnd.delivery_id          = wda.delivery_id
    AND wnd.status_code         IN ('CL', 'IT')
    AND wdl.delivery_id          = wnd.delivery_id
    AND TRUNC(SYSDATE) BETWEEN start_date AND end_date
    AND TRUNC(wts.actual_departure_date) BETWEEN start_date AND end_date
    AND wdl.pick_up_stop_id  = wts.stop_id
    AND WDD.SOURCE_HEADER_ID = OOH.HEADER_ID
    AND wdd.source_line_id   = ool.line_id
    ) UNPROC_SHIP_TRANS
    -----------Invoice count ------------------
    ,
    (SELECT SUM(CNT)
      --,SUM(AMOUNT) AMOUNT
      -- creation_date,
      -- batch_source_name
    FROM
      (SELECT COUNT(1) cnt ,
        SUM(EXTENDED_AMOUNT) AMOUNT ,
        TRUNC(rah.creation_date)-1 creation_date ,
        name batch_source_name
      FROM apps.ra_customer_trx_all rah,
        apps.ra_customer_trx_lines_all ral,
        apps.ra_batch_sources_all rbs
      WHERE rah.customer_trx_id = ral.customer_trx_id
      AND rah.batch_source_id   = rbs.batch_source_id
      AND ((to_date(TO_CHAR(rah.creation_date, 'DD-MM-YYYY HH24:MI:SS'), 'DD-MM-YYYY HH24:MI:SS') BETWEEN TO_DATE((TO_CHAR(sysdate,'DD-MON-YYYY')
        ||'00:00:01'), 'DD-MM-YYYY HH24:MI:SS')
      AND --Yesterday?s date
        to_date((TO_CHAR(sysdate,'DD-MON-YYYY')
        ||'00:45:01'), 'DD-MM-YYYY HH24:MI:SS'))) --Today?s date/time as per request set completion
      AND rah.org_id    = 162
      AND ral.line_type = 'LINE'
      GROUP BY TRUNC(rah.creation_date),
        name
      UNION
      SELECT COUNT(1) ,
        SUM(amount) ,
        TRUNC(creation_date) ,
        batch_source_name
      FROM apps.ra_interface_lines_all
      WHERE ((to_date(TO_CHAR(creation_date, 'DD-MM-YYYY HH24:MI:SS'), 'DD-MM-YYYY HH24:MI:SS') BETWEEN TO_DATE((TO_CHAR(sysdate-1,'DD-MON-YYYY')
        ||' 00:00:01'), 'DD-MM-YYYY HH24:MI:SS')
      AND --Yesterday?s date
        TO_DATE((TO_CHAR(sysdate,'DD-MON-YYYY')
        ||' 00:45:01'), 'DD-MM-YYYY HH24:MI:SS'))) --Today?s date/time as per request set completion
      AND org_id = 162
      GROUP BY TRUNC(creation_date),
        batch_source_name
        --order by batch_source_name
      )
    ) Invoice_Count
    -----------Invoice Amount -----------------
    ,
    (SELECT --sum(cnt),
      SUM(AMOUNT) AMOUNT
      -- creation_date,
      -- batch_source_name
    FROM
      (SELECT COUNT(1) cnt ,
        SUM(EXTENDED_AMOUNT) AMOUNT ,
        TRUNC(rah.creation_date)-1 creation_date ,
        name batch_source_name
      FROM apps.ra_customer_trx_all rah,
        apps.ra_customer_trx_lines_all ral,
        apps.ra_batch_sources_all rbs
      WHERE rah.customer_trx_id = ral.customer_trx_id
      AND rah.batch_source_id   = rbs.batch_source_id
      AND ((to_date(TO_CHAR(rah.creation_date, 'DD-MM-YYYY HH24:MI:SS'), 'DD-MM-YYYY HH24:MI:SS') BETWEEN TO_DATE((TO_CHAR(sysdate,'DD-MON-YYYY')
        ||' 00:00:01'), 'DD-MM-YYYY HH24:MI:SS')
      AND --Yesterday?s date
        to_date((TO_CHAR(sysdate,'DD-MON-YYYY')
        ||' 00:45:01'), 'DD-MM-YYYY HH24:MI:SS'))) --Today?s date/time as per request set completion
      AND rah.org_id    = 162
      AND ral.line_type = 'LINE'
      GROUP BY TRUNC(rah.creation_date),
        name
      UNION
      SELECT COUNT(1) ,
        SUM(amount) ,
        TRUNC(creation_date) ,
        batch_source_name
      FROM apps.ra_interface_lines_all
      WHERE ((to_date(TO_CHAR(creation_date, 'DD-MM-YYYY HH24:MI:SS'), 'DD-MM-YYYY HH24:MI:SS') BETWEEN TO_DATE((TO_CHAR(sysdate-1,'DD-MON-YYYY')
        ||' 00:00:01'), 'DD-MM-YYYY HH24:MI:SS')
      AND --Yesterday?s date
        TO_DATE((TO_CHAR(sysdate,'DD-MON-YYYY')
        ||' 00:45:01'), 'DD-MM-YYYY HH24:MI:SS'))) --Today?s date/time as per request set completion
      AND org_id = 162
      GROUP BY TRUNC(creation_date),
        batch_source_name
        --order by batch_source_name
      )
    ) Invoice_Amount
    -----------Invoice interface errors--------
    ,
    (SELECT COUNT(1)
    FROM apps.ra_interface_lines_all
    where ((to_date(TO_CHAR(creation_date, 'DD-MM-YYYY HH24:MI:SS'), 'DD-MM-YYYY HH24:MI:SS') BETWEEN TO_DATE((TO_CHAR(sysdate-1,'DD-MON-YYYY')
        ||' 00:00:01'), 'DD-MM-YYYY HH24:MI:SS')
      AND --Yesterday?s date
        TO_DATE((TO_CHAR(sysdate,'DD-MON-YYYY')
        ||' 00:45:01'), 'DD-MM-YYYY HH24:MI:SS'))) --Today?s date/time as per request set completion
      AND org_id = 162                                                                                        --Today?s date/time as per request set completion
      --GROUP BY TRUNC(CREATION_DATE),RIL.BATCH_SOURCE_NAME
    ) Invoice_interface_errors
    /*--Do not run the below sql unless the invoice count is above thresold.
    --Run the Auto invoice program again for the errored batch source.
    --Check the count and the error message.
    --If still the count doe not decrease then no use running the autoinvoice again.
    --Check the sales amount. If it is above threshold of $3 million then go ahead and release the break point
    --Add the output of the below query in the daily sales report just above the auto invoice summary.
    select    ril.batch_source_name,
    ril.trx_number,
    ria.message_text,
    count(1) cnt
    from     apps.ra_interface_errors_all ria,
    apps.ra_interface_lines_all  ril
    where    ria.interface_line_id = ril.interface_line_id
    and      ril.org_id = 162
    and      ril.line_type = 'LINE'
    and      ((to_date(to_char(ril.creation_date, 'DD-MM-YYYY HH24:MI:SS'),
    'DD-MM-YYYY HH24:MI:SS') BETWEEN
    to_date('27-01-2014 00:00:01', 'DD-MM-YYYY HH24:MI:SS') AND --Yesterday?s date
    to_date('28-01-2014 02:30:00', 'DD-MM-YYYY HH24:MI:SS')))   --Today?s date/time as per request set completion
    group by ril.batch_source_name,ril.trx_number, ria.message_text  */
    ------------Cost Manager--------
    ,
    (SELECT COUNT(*)
    FROM APPS.MTL_MATERIAL_TRANSACTIONS
    WHERE COSTED_FLAG = 'E'
    ) COST_MANAGER
    -----------Transaction Pending Costing---------
    ,
    (SELECT COUNT(*)
    FROM inv.mtl_material_transactions
    WHERE costed_flag IN ('N')
    ) TRANSACTION_PENDING_COSTING
    ------------Requisition Import--------
    ,
    (SELECT COUNT(1)
    FROM apps.po_requisitions_interface_all pri ,
      apps.mtl_system_items msi
    WHERE pri.destination_type_code NOT IN 'EXPENSE'
    AND pri.item_description NOT LIKE 'Rental%'
    AND TRUNC(pri.creation_date)        = TRUNC(SYSDATE)
    AND pri.item_id                     = msi.inventory_item_id
    AND PRI.DESTINATION_ORGANIZATION_ID = MSI.ORGANIZATION_ID
      -- ORDER BY pri.request_id DESC
    ) REQUISITION_IMPORT
    ------------Counter Order Header Close Count--------
    ,
    (SELECT COUNT(1)
    FROM apps.oe_ordeR_headers_all
    WHERE order_type_id      = 1004
    AND flow_Status_code     = 'CLOSED'
    AND TRUNC(CREATION_DATE) > SYSDATE-2
    GROUP BY TRUNC(creation_Date)
    ) CNTR_ORD_HDR_CLS_CNT
    ------------Workflow Deferred--------
    ,
    (SELECT COUNT(*)
    FROM apps.wf_deferred
    WHERE corrid                                                  = 'APPS:oracle.apps.ar.cmgt.CreditRequestRecommendation.implement'
    AND enq_time                                                 >= (SYSDATE          - 1)
    AND TRUNC((CAST(deq_time AS DATE) - CAST(enq_time AS DATE))) >= 60
    ) WORKFLOW_DEFERRED
    -----------------Duplicate SO--------------------------
    ,
    (SELECT COUNT(order_num)
    FROM
      (SELECT
        (SELECT order_number
        FROM apps.oe_order_headers_all
        WHERE header_id = ol.header_id
        ) order_num,
        ol.line_number
        || '.'
        || ol.shipment_number line_num,
        ol.ordered_item,
        (SELECT organization_name
        FROM apps.org_organization_definitions
        WHERE organization_id = ol.ship_from_org_id
        ) orgn,
        ol.line_id,
        temp.trx_qty * -1 transacted_qty,
        ol.ordered_quantity,
        ol.shipped_quantity order_line_shipped_quantity,
        NVL(
        (SELECT MAX (INITIAL_PICKUP_DATE)
        FROM apps.wsh_new_deliveries a,
          apps.wsh_delivery_details b,
          apps.wsh_delivery_assignments c
        WHERE 1                  = 1
        AND b.delivery_detail_id = c.delivery_detail_id
        AND c.delivery_id        = a.delivery_id
        AND b.source_line_id     = ol.line_id
        ),ol.FULFILLMENT_DATE) latest_shipped_date
      FROM apps.oe_order_lines_all ol,
        (SELECT
          /* INDEX(b MTL_MATERIAL_TRANSACTIONS_N1) */
          a.trx_source_line_id,
          SUM (transaction_quantity) trx_qty
        FROM apps.mtl_material_transactions a
        WHERE 1                          = 1
        AND A.TRANSACTION_ACTION_ID      = 1
        AND A.TRANSACTION_TYPE_ID        = 33
        AND A.TRANSACTION_SOURCE_TYPE_ID = 2
        AND A.TRANSACTION_DATE          >= SYSDATE - 31
        AND EXISTS
          (SELECT 'Found'
          FROM apps.mtl_material_transactions b
          WHERE 1                              = 1
          AND a.organization_id                = b.organization_id
          AND b.TRANSACTION_DATE              >= SYSDATE - 31
          AND a.inventory_item_id              = b.inventory_item_id
          AND a.subinventory_code              = b.subinventory_code
          AND NVL (a.revision, 0)              = NVL (b.revision, 0)
          AND NVL (a.locator_id, 0)            = NVL (b.locator_id, 0)
          AND a.primary_quantity               = b.primary_quantity
          AND a.transaction_quantity           = b.transaction_quantity
          AND a.transaction_source_type_id     = b.transaction_source_type_id
          AND a.transaction_type_id            = b.transaction_type_id
          AND a.transaction_action_id          = b.transaction_action_id
          AND NVL (a.transaction_source_id, 0) = NVL (b.transaction_source_id, 0)
          AND NVL (a.trx_source_line_id, 0)    = NVL (b.trx_source_line_id, 0)
          AND a.ROWID                         <> b.ROWID
          )
        GROUP BY a.trx_source_line_id
        ) temp
      WHERE ol.line_id        = temp.trx_source_line_id
      AND (                           -1 * temp.trx_qty) > ol.ordered_quantity
      AND ol.creation_date   >=sysdate-1
      UNION
      SELECT
        (SELECT order_number
        FROM apps.oe_order_headers_all
        WHERE header_id = ol.header_id
        ) order_num,
        ol.line_number
        || '.'
        || ol.shipment_number line_num,
        ol.ordered_item,
        (SELECT organization_name
        FROM apps.org_organization_definitions
        WHERE organization_id = ol.ship_from_org_id
        ) orgn,
        ol.line_id,
        temp.trx_qty * -1 transacted_qty,
        ol.ordered_quantity,
        ol.shipped_quantity order_line_shipped_quantity,
        NVL(
        (SELECT MAX (INITIAL_PICKUP_DATE)
        FROM apps.wsh_new_deliveries a,
          apps.wsh_delivery_details b,
          apps.wsh_delivery_assignments c
        WHERE 1                  = 1
        AND b.delivery_detail_id = c.delivery_detail_id
        AND c.delivery_id        = a.delivery_id
        AND b.source_line_id     = ol.line_id
        ),ol.FULFILLMENT_DATE) latest_shipped_date
      FROM apps.oe_order_lines_all ol,
        (SELECT
          /* INDEX(b MTL_MATERIAL_TRANSACTIONS_N1) */
          a.trx_source_line_id,
          SUM (transaction_quantity) trx_qty
        FROM apps.mtl_material_transactions a
        WHERE 1                          = 1
        AND A.TRANSACTION_ACTION_ID      = 1
        AND A.TRANSACTION_TYPE_ID        = 62--internal order issue
        AND A.TRANSACTION_SOURCE_TYPE_ID = 2
        AND A.TRANSACTION_DATE          >= SYSDATE - 31
        AND EXISTS
          (SELECT 'Found'
          FROM apps.mtl_material_transactions b
          WHERE 1                              = 1
          AND a.organization_id                = b.organization_id
          AND b.TRANSACTION_DATE              >= SYSDATE - 31
          AND a.inventory_item_id              = b.inventory_item_id
          AND a.subinventory_code              = b.subinventory_code
          AND NVL (a.revision, 0)              = NVL (b.revision, 0)
          AND NVL (a.locator_id, 0)            = NVL (b.locator_id, 0)
          AND a.primary_quantity               = b.primary_quantity
          AND a.transaction_quantity           = b.transaction_quantity
          AND a.transaction_source_type_id     = b.transaction_source_type_id
          AND a.transaction_type_id            = b.transaction_type_id
          AND a.transaction_action_id          = b.transaction_action_id
          AND NVL (a.transaction_source_id, 0) = NVL (b.transaction_source_id, 0)
          AND NVL (a.trx_source_line_id, 0)    = NVL (b.trx_source_line_id, 0)
          AND a.ROWID                         <> b.ROWID
          )
        GROUP BY a.trx_source_line_id
        ) temp
      WHERE ol.line_id        = temp.trx_source_line_id
      AND (                           -1 * temp.trx_qty) > ol.ordered_quantity
      AND ol.creation_date   >=sysdate-1
      ORDER BY 9 DESC
      )
    ) DUPLICATE_SO
  FROM DUAL;
  /