--Report Name            : Daily Oracle Interface Errors Report - WC
--Import Instructions : 1. If RTF template needs to be uploaded to report automatically then move the RTF template to physical path of APPS_DATA_FILE_DIR oracle directory. Check dba_directories view to get the physical path of oracle directory. 
--                         (If you want to use any other oracle directory other than APPS_DATA_FILE_DIR then replace APPS_DATA_FILE_DIR with your oracle directory name below.)
--                      2. Make sure XXEIS schema has read/write permissions to above used oracle directory and give full permissions to the directory at OS level.
--                      3. Run this file in XXEIS user from SQL*Plus or any tool like Toad, SQL Navigator. 
exec xxeis.eis_rs_ins.g_rtf_dir := 'APPS_DATA_FILE_DIR' ;
prompt Creating View Data for Daily Oracle Interface Errors Report - WC
set scan off define off
DECLARE
BEGIN 
--Inserting View EIS_XXWC_DAILY_IF_ERROR_RPT_V
xxeis.eis_rs_ins.v( 'EIS_XXWC_DAILY_IF_ERROR_RPT_V',85000,'','','','','MR020532','XXEIS','Eis Xxwc Daily If Error Rpt V','EXDIERV','','');
--Delete View Columns for EIS_XXWC_DAILY_IF_ERROR_RPT_V
xxeis.eis_rs_utility.delete_view_rows('EIS_XXWC_DAILY_IF_ERROR_RPT_V',85000,FALSE);
--Inserting View Columns for EIS_XXWC_DAILY_IF_ERROR_RPT_V
xxeis.eis_rs_ins.vc( 'EIS_XXWC_DAILY_IF_ERROR_RPT_V','INTERNAL_ORDERS_WITH_NO_LINES',85000,'Internal Orders With No Lines','INTERNAL_ORDERS_WITH_NO_LINES','','','','MR020532','NUMBER','','','Internal Orders With No Lines','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_DAILY_IF_ERROR_RPT_V','DUPLICATE_SO',85000,'Duplicate So','DUPLICATE_SO','','','','MR020532','NUMBER','','','Duplicate So','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_DAILY_IF_ERROR_RPT_V','WORKFLOW_DEFERRED',85000,'Workflow Deferred','WORKFLOW_DEFERRED','','','','MR020532','NUMBER','','','Workflow Deferred','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_DAILY_IF_ERROR_RPT_V','CNTR_ORD_HDR_CLS_CNT',85000,'Cntr Ord Hdr Cls Cnt','CNTR_ORD_HDR_CLS_CNT','','','','MR020532','NUMBER','','','Cntr Ord Hdr Cls Cnt','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_DAILY_IF_ERROR_RPT_V','REQUISITION_IMPORT',85000,'Requisition Import','REQUISITION_IMPORT','','','','MR020532','NUMBER','','','Requisition Import','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_DAILY_IF_ERROR_RPT_V','TRANSACTION_PENDING_COSTING',85000,'Transaction Pending Costing','TRANSACTION_PENDING_COSTING','','','','MR020532','NUMBER','','','Transaction Pending Costing','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_DAILY_IF_ERROR_RPT_V','COST_MANAGER',85000,'Cost Manager','COST_MANAGER','','','','MR020532','NUMBER','','','Cost Manager','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_DAILY_IF_ERROR_RPT_V','INVOICE_INTERFACE_ERRORS',85000,'Invoice Interface Errors','INVOICE_INTERFACE_ERRORS','','','','MR020532','NUMBER','','','Invoice Interface Errors','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_DAILY_IF_ERROR_RPT_V','INVOICE_AMOUNT',85000,'Invoice Amount','INVOICE_AMOUNT','','','','MR020532','NUMBER','','','Invoice Amount','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_DAILY_IF_ERROR_RPT_V','INVOICE_COUNT',85000,'Invoice Count','INVOICE_COUNT','','','','MR020532','NUMBER','','','Invoice Count','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_DAILY_IF_ERROR_RPT_V','UNPROC_SHIP_TRANS',85000,'Unproc Ship Trans','UNPROC_SHIP_TRANS','','','','MR020532','NUMBER','','','Unproc Ship Trans','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_DAILY_IF_ERROR_RPT_V','SO_LINE_WF_ERRORS',85000,'So Line Wf Errors','SO_LINE_WF_ERRORS','','','','MR020532','NUMBER','','','So Line Wf Errors','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_DAILY_IF_ERROR_RPT_V','SO_HEADER_WF_ERRORS',85000,'So Header Wf Errors','SO_HEADER_WF_ERRORS','','','','MR020532','NUMBER','','','So Header Wf Errors','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_DAILY_IF_ERROR_RPT_V','SPEEDBUILD_STUCK_ORDERS',85000,'Speedbuild Stuck Orders','SPEEDBUILD_STUCK_ORDERS','','','','MR020532','NUMBER','','','Speedbuild Stuck Orders','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_DAILY_IF_ERROR_RPT_V','ORDER_IMPORT_LINES',85000,'Order Import Lines','ORDER_IMPORT_LINES','','','','MR020532','NUMBER','','','Order Import Lines','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_DAILY_IF_ERROR_RPT_V','ORDER_IMPORT_HEADERS',85000,'Order Import Headers','ORDER_IMPORT_HEADERS','','','','MR020532','NUMBER','','','Order Import Headers','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_DAILY_IF_ERROR_RPT_V','PENDING_MTL_TRANS',85000,'Pending Mtl Trans','PENDING_MTL_TRANS','','','','MR020532','NUMBER','','','Pending Mtl Trans','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_DAILY_IF_ERROR_RPT_V','INV_TRANS_OP_IF_OE',85000,'Inv Trans Op If Oe','INV_TRANS_OP_IF_OE','','','','MR020532','NUMBER','','','Inv Trans Op If Oe','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_DAILY_IF_ERROR_RPT_V','INV_TRANS_OP_IF',85000,'Inv Trans Op If','INV_TRANS_OP_IF','','','','MR020532','NUMBER','','','Inv Trans Op If','','','');
--Inserting View Components for EIS_XXWC_DAILY_IF_ERROR_RPT_V
--Inserting View Component Joins for EIS_XXWC_DAILY_IF_ERROR_RPT_V
END;
/
set scan on define on
prompt Creating Report Data for Daily Oracle Interface Errors Report - WC
set scan off define off
DECLARE
BEGIN 
--Deleting Report data - Daily Oracle Interface Errors Report - WC
xxeis.eis_rs_utility.delete_report_rows( 'Daily Oracle Interface Errors Report - WC' );
--Inserting Report - Daily Oracle Interface Errors Report - WC
xxeis.eis_rs_ins.r( 85000,'Daily Oracle Interface Errors Report - WC','','EIS report to report out all interface transactions.','','','','MR020532','EIS_XXWC_DAILY_IF_ERROR_RPT_V','Y','','','MR020532','','N','WC Knowledge Management Reports','RTF,PDF,','CSV,HTML,Html Summary,XML,Pivot Excel,EXCEL,','N');
--Inserting Report Columns - Daily Oracle Interface Errors Report - WC
xxeis.eis_rs_ins.rc( 'Daily Oracle Interface Errors Report - WC',85000,'CNTR_ORD_HDR_CLS_CNT','Cntr Ord Hdr Cls Cnt','Cntr Ord Hdr Cls Cnt','','~~~','default','','16','N','','','','','','','','MR020532','N','N','','EIS_XXWC_DAILY_IF_ERROR_RPT_V','','');
xxeis.eis_rs_ins.rc( 'Daily Oracle Interface Errors Report - WC',85000,'COST_MANAGER','Cost Manager','Cost Manager','','~~~','default','','13','N','','','','','','','','MR020532','N','N','','EIS_XXWC_DAILY_IF_ERROR_RPT_V','','');
xxeis.eis_rs_ins.rc( 'Daily Oracle Interface Errors Report - WC',85000,'DUPLICATE_SO','Duplicate So','Duplicate So','','~~~','default','','18','N','','','','','','','','MR020532','N','N','','EIS_XXWC_DAILY_IF_ERROR_RPT_V','','');
xxeis.eis_rs_ins.rc( 'Daily Oracle Interface Errors Report - WC',85000,'INTERNAL_ORDERS_WITH_NO_LINES','Internal Orders With No Lines','Internal Orders With No Lines','','~~~','default','','19','N','','','','','','','','MR020532','N','N','','EIS_XXWC_DAILY_IF_ERROR_RPT_V','','');
xxeis.eis_rs_ins.rc( 'Daily Oracle Interface Errors Report - WC',85000,'INVOICE_AMOUNT','Invoice Amount','Invoice Amount','','~~~','default','','11','N','','','','','','','','MR020532','N','N','','EIS_XXWC_DAILY_IF_ERROR_RPT_V','','');
xxeis.eis_rs_ins.rc( 'Daily Oracle Interface Errors Report - WC',85000,'INVOICE_COUNT','Invoice Count','Invoice Count','','~~~','default','','10','N','','','','','','','','MR020532','N','N','','EIS_XXWC_DAILY_IF_ERROR_RPT_V','','');
xxeis.eis_rs_ins.rc( 'Daily Oracle Interface Errors Report - WC',85000,'INVOICE_INTERFACE_ERRORS','Invoice Interface Errors','Invoice Interface Errors','','~~~','default','','12','N','','','','','','','','MR020532','N','N','','EIS_XXWC_DAILY_IF_ERROR_RPT_V','','');
xxeis.eis_rs_ins.rc( 'Daily Oracle Interface Errors Report - WC',85000,'INV_TRANS_OP_IF','INV_TRANS_OP_IF','Inv Trans Op If','','~~~','default','','1','N','','','','','','','','MR020532','N','N','','EIS_XXWC_DAILY_IF_ERROR_RPT_V','','');
xxeis.eis_rs_ins.rc( 'Daily Oracle Interface Errors Report - WC',85000,'INV_TRANS_OP_IF_OE','Order Entries','Inv Trans Op If Oe','','~~~','default','','2','N','','','','','','','','MR020532','N','N','','EIS_XXWC_DAILY_IF_ERROR_RPT_V','','');
xxeis.eis_rs_ins.rc( 'Daily Oracle Interface Errors Report - WC',85000,'ORDER_IMPORT_HEADERS','Order Import Headers','Order Import Headers','','~~~','default','','4','N','','','','','','','','MR020532','N','N','','EIS_XXWC_DAILY_IF_ERROR_RPT_V','','');
xxeis.eis_rs_ins.rc( 'Daily Oracle Interface Errors Report - WC',85000,'ORDER_IMPORT_LINES','Order Import Lines','Order Import Lines','','~~~','default','','5','N','','','','','','','','MR020532','N','N','','EIS_XXWC_DAILY_IF_ERROR_RPT_V','','');
xxeis.eis_rs_ins.rc( 'Daily Oracle Interface Errors Report - WC',85000,'PENDING_MTL_TRANS','Pending Mtl Trans','Pending Mtl Trans','','~~~','default','','3','N','','','','','','','','MR020532','N','N','','EIS_XXWC_DAILY_IF_ERROR_RPT_V','','');
xxeis.eis_rs_ins.rc( 'Daily Oracle Interface Errors Report - WC',85000,'REQUISITION_IMPORT','Requisition Import','Requisition Import','','~~~','default','','15','N','','','','','','','','MR020532','N','N','','EIS_XXWC_DAILY_IF_ERROR_RPT_V','','');
xxeis.eis_rs_ins.rc( 'Daily Oracle Interface Errors Report - WC',85000,'SO_HEADER_WF_ERRORS','So Header Wf Errors','So Header Wf Errors','','~~~','default','','7','N','','','','','','','','MR020532','N','N','','EIS_XXWC_DAILY_IF_ERROR_RPT_V','','');
xxeis.eis_rs_ins.rc( 'Daily Oracle Interface Errors Report - WC',85000,'SO_LINE_WF_ERRORS','So Line Wf Errors','So Line Wf Errors','','~~~','default','','8','N','','','','','','','','MR020532','N','N','','EIS_XXWC_DAILY_IF_ERROR_RPT_V','','');
xxeis.eis_rs_ins.rc( 'Daily Oracle Interface Errors Report - WC',85000,'SPEEDBUILD_STUCK_ORDERS','Speedbuild Stuck Orders','Speedbuild Stuck Orders','','~~~','default','','6','N','','','','','','','','MR020532','N','N','','EIS_XXWC_DAILY_IF_ERROR_RPT_V','','');
xxeis.eis_rs_ins.rc( 'Daily Oracle Interface Errors Report - WC',85000,'TRANSACTION_PENDING_COSTING','Transaction Pending Costing','Transaction Pending Costing','','~~~','default','','14','N','','','','','','','','MR020532','N','N','','EIS_XXWC_DAILY_IF_ERROR_RPT_V','','');
xxeis.eis_rs_ins.rc( 'Daily Oracle Interface Errors Report - WC',85000,'UNPROC_SHIP_TRANS','Unproc Ship Trans','Unproc Ship Trans','','~~~','default','','9','N','','','','','','','','MR020532','N','N','','EIS_XXWC_DAILY_IF_ERROR_RPT_V','','');
xxeis.eis_rs_ins.rc( 'Daily Oracle Interface Errors Report - WC',85000,'WORKFLOW_DEFERRED','Workflow Deferred','Workflow Deferred','','~~~','default','','17','N','','','','','','','','MR020532','N','N','','EIS_XXWC_DAILY_IF_ERROR_RPT_V','','');
--Inserting Report Parameters - Daily Oracle Interface Errors Report - WC
--Inserting Report Conditions - Daily Oracle Interface Errors Report - WC
--Inserting Report Sorts - Daily Oracle Interface Errors Report - WC
--Inserting Report Triggers - Daily Oracle Interface Errors Report - WC
--Inserting Report Templates - Daily Oracle Interface Errors Report - WC
xxeis.eis_rs_ins.R_Tem( 'Daily Oracle Interface Errors Report - WC','Daily Oracle Interface Errors Report - WC','','','','','','','','1','2','3','','','MR020532');
--Inserting Report Portals - Daily Oracle Interface Errors Report - WC
--Inserting Report Dashboards - Daily Oracle Interface Errors Report - WC
--Inserting Report Security - Daily Oracle Interface Errors Report - WC
--Inserting Report Pivots - Daily Oracle Interface Errors Report - WC
END;
/
set scan on define on
