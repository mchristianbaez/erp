----------------------------------------------------------------------------------------------------------------------------
/*************************************************************************************************************************
  $Header XXWC_AP_INVOICES_ALL_N1
  File Name: XXWC_AP_INVOICES_ALL_N1.sql
  PURPOSE:   
  REVISIONS:
     Ver        Date         Author         Description
     ---------  ----------   --------    -----------------------------------------------------------------------------------
     1.0        18-Apr-2016  Siva        --TMS#20160426-00098 WC-Freight Detail Subledger Report - Program Running for longer time
	 1.1        18-Apr-2016  Siva        --TMS#20160503-00101 HDS Account Analysis Subledger Detail Report - Program Running for longer time
****************************************************************************************************************************/
CREATE INDEX AP.XXWC_AP_INVOICES_ALL_N1 ON AP.AP_INVOICES_ALL (QUICK_PO_HEADER_ID) TABLESPACE APPS_TS_TX_DATA
/
 