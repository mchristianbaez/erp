/**************************************************************************************************************************
TMS#20160805-00261 Abort OMERROR child workflow type records created by Sales order line workflow
Creatd by : P.Vamshidhar
Date: 28-Jul-2016
Purpose:  Data fix to abort workflow notifications.
/**************************************************************************************************************************/

SET SERVEROUTPUT ON SIZE 1000000

DECLARE
   lncount   NUMBER := 0;

   CURSOR CUR_WF_ITEMS
   IS
        SELECT item_key
          FROM apps.wf_items
         WHERE     item_type = 'OMERROR'
               AND ROOT_ACTIVITY = 'R_ERROR_RETRY'
               AND end_date IS NULL
               AND TRUNC (begin_date) >= TRUNC (SYSDATE) - 100
               AND PARENT_ITEM_KEY IN (SELECT LINE_ID
                                         FROM OE_ORDER_LINES_ALL
                                        WHERE FLOW_STATUS_CODE IN ('CLOSED',
                                                                   'CANCELLED'))
               AND ROWNUM <= 300000
      ORDER BY begin_date ASC;

BEGIN
   DBMS_OUTPUT.put_line (
      'Starting abort script.' || TO_CHAR (SYSDATE, 'DD-MON-YYYY HH24:MI:SS'));

   FOR I IN CUR_WF_ITEMS
   LOOP
      lncount := lncount + 1;
      WF_ENGINE.abortProcess ('OMERROR', i.item_key);

      IF MOD (lncount, 5000) = 0
      THEN
         DBMS_OUTPUT.put_line (
               'Processed a thousand rows. Committing. Current count: '
            || lncount);
         COMMIT;
      END IF;
   END LOOP;


   DBMS_OUTPUT.put_line (
         'Finished abort script. Final commit. Processed this many lines: '
      || lncount
      || ' - '
      || TO_CHAR (SYSDATE, 'DD-MON-YYYY HH24:MI:SS'));


   COMMIT;

EXCEPTION
   WHEN OTHERS
   THEN
      DBMS_OUTPUT.put_line ('Error Occured ' || SUBSTR (SQLERRM, 1, 500));
END;
/