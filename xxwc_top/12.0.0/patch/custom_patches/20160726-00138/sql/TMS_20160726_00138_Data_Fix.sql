SET SERVEROUTPUT ON SIZE 1000000

/**************************************************************************************************************************
TMS#20160726-00138  - Data fix script to progress lines which are stuck in Awaiting Invoice Interface - On Hold status
Creatd by : P.Vamshidhar
Date: 28-Jul-2016
Purpose:  Data fix script to progress lines which are stuck in Awaiting Invoice Interface - On Hold status

****************************************************************************************************************************
Creating table
****************************************************************************************************************************/


CREATE TABLE XXWC.XXWC_OEOL_WF_CLEAN_TBL (HEADER_ID NUMBER)
/

/**************************************************************************************************************************
clearning table and inserting tobe data processed
****************************************************************************************************************************/

DECLARE
   ln_count   NUMBER := 0;
BEGIN
   DELETE FROM XXWC.XXWC_OEOL_WF_CLEAN_TBL;

   COMMIT;


   INSERT INTO XXWC.XXWC_OEOL_WF_CLEAN_TBL (HEADER_ID)
      SELECT DISTINCT ool.header_id
        FROM apps.oe_order_lines_all ool, apps.oe_order_headers_all oh
       WHERE     ool.line_category_code IN ('ORDER', 'RETURN')
             AND ool.cancelled_flag = 'N'
             AND ool.flow_status_code = 'INVOICE_HOLD'
             AND ool.ordered_quantity > 0
             AND ool.header_id = oh.header_id
             AND oh.order_type_id = 1001
             AND TRUNC (ool.last_update_date) >= TRUNC (SYSDATE) - 1000
             AND EXISTS
                    (SELECT 'exists'
                       FROM wf_item_activity_statuses a,
                            wf_process_activities b
                      WHERE     a.item_type = 'OEOL'
                            AND a.process_activity = b.instance_id
                            AND b.activity_name =
                                   'INVOICE_INTERFACE_ELIGIBLE'
                            AND b.Process_Name =
                                   'WC_LINE_INVOICE_INTERFACE_SUB'
                            AND activity_status <> 'COMPLETE'
                            AND TO_NUMBER (a.item_key) = ool.line_id)
             AND NOT EXISTS
                    (SELECT 1
                       FROM apps.oe_order_holds x
                      WHERE     x.header_id = ool.header_id
                            AND ool.line_id = NVL (x.line_id, ool.line_id)
                            AND released_flag = 'N')
             AND TRUNC (ool.actual_shipment_date) < TRUNC (SYSDATE)
             AND ROWNUM < 201;

   DBMS_OUTPUT.put_line ('Number of Rows Inserted :' || SQL%ROWCOUNT);

   COMMIT;
EXCEPTION
   WHEN OTHERS
   THEN
      DBMS_OUTPUT.PUT_LINE ('Error Occured ' || SUBSTR (SQLERRM, 200));
END;
/

/**************************************************************************************************************************
Processing Data
****************************************************************************************************************************/

DECLARE
   CURSOR order_cur
   IS
        SELECT ool.line_id, ool.header_id
          FROM apps.oe_order_lines_all ool, apps.oe_order_headers_all oh
         WHERE     ool.line_category_code IN ('ORDER', 'RETURN')
               AND ool.cancelled_flag = 'N'
               AND ool.flow_status_code = 'INVOICE_HOLD'
               AND ool.ordered_quantity > 0
               AND ool.header_id = oh.header_id
               AND oh.order_type_id = 1001
               AND TRUNC (ool.last_update_date) >= TRUNC (SYSDATE) - 1000
               AND EXISTS
                      (SELECT 'exists'
                         FROM wf_item_activity_statuses a,
                              wf_process_activities b
                        WHERE     a.item_type = 'OEOL'
                              AND a.process_activity = b.instance_id
                              AND b.activity_name =
                                     'INVOICE_INTERFACE_ELIGIBLE'
                              AND b.Process_Name =
                                     'WC_LINE_INVOICE_INTERFACE_SUB'
                              AND activity_status <> 'COMPLETE'
                              AND TO_NUMBER (a.item_key) = ool.line_id)
               AND NOT EXISTS
                      (SELECT 1
                         FROM apps.oe_order_holds x
                        WHERE     x.header_id = ool.header_id
                              AND ool.line_id = NVL (x.line_id, ool.line_id)
                              AND released_flag = 'N')
               AND EXISTS
                      (SELECT 1
                         FROM XXWC.XXWC_OEOL_WF_CLEAN_TBL xow
                        WHERE xow.header_id = oh.header_id)
               AND TRUNC (ool.actual_shipment_date) < TRUNC (SYSDATE)
      ORDER BY ool.header_id, ool.line_number;

   l_line_id        VARCHAR2 (50);
   l_activity       VARCHAR2 (80) := 'INVOICE_INTERFACE_ELIGIBLE';
   l_user_id        NUMBER := 5200;                          -- Raghav User Id
   l_resp_id        NUMBER := 50857;
   l_resp_appl_id   NUMBER := 660;
BEGIN
   DBMS_OUTPUT.put_line ('100 : Begin of API ');

   apps.mo_global.set_policy_context ('S', 162);
   fnd_global.apps_initialize (l_user_id, l_resp_id, l_resp_appl_id);

   DBMS_OUTPUT.put_line ('110 : Loop Through the Cursor ');

   FOR i IN order_cur
   LOOP
      BEGIN
         l_line_id := TO_CHAR (i.line_id);

         DBMS_OUTPUT.PUT_LINE (
            'Header id-->' || i.header_id || ' Line id-->' || i.line_id);

         wf_engine.completeactivity ('OEOL',
                                     l_line_id,
                                     l_activity,
                                     NULL);
      EXCEPTION
         WHEN OTHERS
         THEN
            DBMS_OUTPUT.put_line (
                  '150 : When others Exception while completing acivity for SO Line ID  : '
               || i.line_id);
            DBMS_OUTPUT.put_line ('151 : Error Message : ' || SQLERRM);
      END;
   END LOOP;

   wf_engine.background (itemtype           => 'OEOL',
                         minthreshold       => NULL,
                         maxthreshold       => NULL,
                         process_deferred   => TRUE,
                         process_timeout    => TRUE,
                         process_stuck      => FALSE);
EXCEPTION
   WHEN OTHERS
   THEN
      DBMS_OUTPUT.put_line ('160 : When others Exception ');
      DBMS_OUTPUT.put_line ('161 : Error Message : ' || SQLERRM);
END;
/