/*
 -- Author: Balaguru Seshadri
 -- Parameters: None
 -- Modification History
 -- ESMS                TMS                           Date                     Version  Comments
 -- =========== ===============  ========             ======   ========================
 -- 352930          20160622-00031   23-JUN-2016     1.0          Created.
 */ 
SET SERVEROUTPUT ON SIZE 1000000
declare
  --
  v_project_desc     varchar2(150) :=Null;
  v_nt_id                    fnd_user.user_name%type :=Null;
  --
 procedure print_log(p_message in varchar2) is
 begin
     if  fnd_global.conc_request_id >0 then
      fnd_file.put_line(fnd_file.log, p_message);
     else
      dbms_output.put_line(p_message);
     end if;
 end print_log;
 --
 --
 procedure print_output(p_message in varchar2) is
 begin
     if  fnd_global.conc_request_id >0 then
      fnd_file.put_line(fnd_file.output, p_message);
     else
      dbms_output.put_line(p_message);
     end if;
 end print_output;
 --         
begin
  --
  print_log('Start: Main');
  --
  begin 
     --
    savepoint square1;
    --
    delete ap.ap_exp_report_dists_all
    where 1 =1
    and report_header_id =853667
    and report_line_id =7961340
    and report_distribution_id =5635569;
    --
    print_log('Deleted one record from table ap_exp_report_dists_all with report_distribution_id 5635569, deleted count ='||sql%rowcount);
    --
  exception
   when others then
    print_log('@Failed to delete record with report_distribution_id 5635569, msg ='||sqlerrm);
    rollback to square1;
  end;   
  -- 
  begin 
     --
    savepoint square2;
    --
    delete ap.ap_expense_report_lines_all
    where 1 =1
    and report_header_id =853667
    and report_line_id =7961340;
    --
    print_log('Deleted one record from table ap_expense_report_lines_all with report_line_id : 7961340, deleted count ='||sql%rowcount);
    --
  exception
   when others then
    print_log('@Failed to delete record with report_line_id 7961340, msg ='||sqlerrm);
    rollback to square2;
  end;   
  --   
 commit;
  --  
 print_log('End: Main');
 -- 
exception
 when others then
  print_log('Outer block of SQL script, message ='||sqlerrm);
end;
/