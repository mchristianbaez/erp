/*
 TMS: 20160509-00109
 Date: 04/08/2016
 Notes: Script to  update table xxcus.xxcus_monitor_cp_instance, field db_name from EBSPRD to EBIZPRD. 
*/
set serveroutput on size 1000000
declare
 --
 n_loc number;
 --
begin --Main Processing...
   --
   n_loc :=101;
   --
   begin 
    --
    update xxcus.xxcus_monitor_cp_instance set db_name ='EBIZPRD';
    --
    n_loc :=102;
    --
    commit;
    --
   exception
    when others then
     --
     n_loc :=103;
     --
     dbms_output.put_line('@ Failed to update table xxcus.xxcus_monitor_cp_instance, @'||n_loc||', message ='||sqlerrm);
     --   
   end;
   --
exception
   when others then
      dbms_output.put_line ('TMS: 20160509-00109 , Outer block errors =' || sqlerrm);
end;
/