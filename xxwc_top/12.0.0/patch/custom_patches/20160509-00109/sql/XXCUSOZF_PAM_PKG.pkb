CREATE OR REPLACE PACKAGE BODY APPS."XXCUSOZF_PAM_PKG" IS

  -- -----------------------------------------------------------------------------
  -- |----------------------------< XXCUSOZF_PAM_PKG >--------------------------------|
  -- -----------------------------------------------------------------------------
  --
  --   VERSION DATE          AUTHOR(S)       DESCRIPTION
  -- ------- -----------   --------------- ---------------------------------
  -- 1.0     17-APR-2013   Dragan           Created this package.  RFC 37489
  --                                       Package used by the PAM project
  -- 1.1     14-Oct-2013   Dragan          Added parms to allow concurrent request
  --                                       to run RFC 38620 SR 228763
  -- 1.2    10/14/2014    Bala Seshadri  ESMS 265235. Invoke email notifiction at the end of the repop_main routine
  --  RFC 42038 / ESMS 267539 10/25/2014   Bala Seshadri Remove logic to send email. This logic is moved out as a new stage
  --                                                     within the custom sunday request set 
  -- 1.3    03/03/2015   Bala Seshadri    ESMS  261553. Add logic to point the eaapxprd db link to a non prod instance
  --                                     or PRD instance depending on the instance the script runs
  -- 1.4   06/02/2015    Bala Seshadri  ESMS 290358.  Add contract_nbr and plan_id to the repop_program routine. 
  --1.5   10/07/2015    Bala Seshadri  TMS 20151001-00046 / ESMS 303620  
  --1.6   04/08/2016    Bala Seshadri  TMS 20160407-00172 / ESMS 322629, EBSPRD migration related fixes FOR GSC objects
 --1.7   05/09/2016   Balaguru Seshadri TMS 20160509-00109 / ESMS 322629, Rollback edits to ver 1.6 FOR GSC objects  
  --error handling
  g_err_email  VARCHAR2(200) := 'HDSOracleDevelopers@hdsupply.com';
  g_err_email2 VARCHAR2(200) := 'Dragan.Velimirovic@hdsupply.com';
  --
  procedure print_log(p_message in varchar2) is
  begin
   if fnd_global.conc_request_id >0 then
    fnd_file.put_line(fnd_file.log, p_message);
   else
    dbms_output.put_line(p_message);
   end if;
  exception
   when others then
    fnd_file.put_line(fnd_file.log, 'Issue in print_log routine ='||sqlerrm);
  end print_log;
  --  
  PROCEDURE repop_main (pam_err out varchar2,
                        pam_sec out number)
                        IS
    -- -----------------------------------------------------------------------------
    -- Name           : repop_vend_lov
    -- Date Written   : 16-JAN-2013
    -- Author         : Manny Rodriguez
    --
    -- Description : delete and Repopulate tables
    --
    --
    -- Modification History:
    --
    -- When         Who             Comments
    -- -----------  --------        -----------------------------------------------------
    -- 15-JAN-2013  Manny           Created
    -- 20-MAR-2013  Dragan          Added repop_pam_lob_purchase.
    -- 03-MAR-2015  Bala Seshadri   Logic to verify db links and drop /recreate if necessary
    ---------------------------------------------------------------------------------
    --error handling
    l_err_program           VARCHAR2(75) := 'XXCUSOZF_PAM_PKG.repop_main';
    l_sec                   VARCHAR2(4000) DEFAULT 'Starting';
    --
    ln_request_id           NUMBER :=0;
    v_email                 VARCHAR2(60) :=Null;
    --
    v_curent_db_name        VARCHAR2(20) :='NA';
    v_prod_db_name          VARCHAR2(20) :='EBIZPRD'; --1.6 --1.7
    --v_prod_db_name          VARCHAR2(20) :='EBSPRD'; --1.6    --1.7
    v_prod_rbt_email_id     VARCHAR2(40) :='hdsoraclerebates@hdsupply.com';    
    --parms
    b_proceed               BOOLEAN;
    v_db_prd                VARCHAR2(15) :='EBIZPRD';  --1.6 --1.7
    --v_db_prd                VARCHAR2(15) :='EBSPRD';  --1.6     --1.7
    --
    v_current_db            VARCHAR2(15) :=Null;
    n_count                 NUMBER       :=0;
    --
    v_db_link_sql           VARCHAR2(2000) :=Null;
    --
    v_error_msg             VARCHAR2(240) :=Null;
    --
  BEGIN
   --
   -- get current database 
   --
   begin 
    --
    select upper(name) 
    into   v_current_db 
    from   v$database;
    --
   exception
    when others then
     v_current_db :='NONE';
   end;
   --
   --If the current instance is other than EBIZPRD, we will check if the existing db link eaapxprd points to eaapxprd database.
   --When the db link eaapxprd points to eaapxprd database, we will drop the existing link and recreate it to point to eaapxqa
   --
   if (v_current_db =v_db_prd) then 
    -- 
    -- when the instance is EBIZPRD, the existing db link eaapxprd will need to be pointing to eaapxprd database.
    -- We don't need to check further and lets move on.
    --
    b_proceed :=TRUE; 
    --
   else
    -- 
    -- when the instance is non EBIZPRD, then check if the  existing db link eaapxprd 
    -- host entries point to eaapxprd. If so drop and recreate it using eaapxqa. If it's already pointing to eaapxqa
    -- just move on. 
    -- If we drop and recreate the db link make sure we can query it. If we can query then the db link is active and good to go
    -- else email to the developer group and also error the concurrent program
    --   
    begin 
     --
      select count(1)
      into   n_count
      from   sys.user_db_links
      where  1 =1
        and  db_link like 'EAAPXPRD%'
        and  upper(host) like '%EAAPXQA%';
     --
     if n_count >=1 then  
      --
      -- The existing db link eaapxprd is already pointing to eaapxqa database. No need to check further.
      --
      b_proceed :=TRUE;
      --
     else
      --
      -- First: Check to see if the DB link exists and then drop the db link. If no link exists then move on
      -- Second: Create the db link
      -- Third: Query a sample SQL using the db link to make sure the link is active and works fine.
      -- If not something is wrong, send an email to the developer group and also error the concurrent program
      --
      begin
       --
       begin
        --
        select count(1)
        into   n_count
        from   sys.user_db_links
        where  1 =1
          and  db_link ='EAAPXPRD.HSI.HUGHESSUPPLY.COM';
        --
        if n_count >0 then
         --
         execute immediate 'DROP DATABASE LINK EAAPXPRD.HSI.HUGHESSUPPLY.COM';
         --  
         print_log('After dropping db link EAAPXPRD.HSI.HUGHESSUPPLY.COM');        
         --                        
        end if;
        --
        begin
        --
        v_db_link_sql :=Null;
        --
        v_db_link_sql :='create database link EAAPXPRD
                          connect to INTERFACE_ORACLER12 identified by pa$$w0rd
                          using ''(DESCRIPTION =    (ADDRESS = (PROTOCOL = TCP)(HOST = eaapxqa.hdsupply.net)(PORT = 1521))    
                                  (CONNECT_DATA =      (SERVER = DEDICATED)      (SERVICE_NAME = eaapxqa)))''
                                ';
        --
        print_log('v_db_link_sql ='||v_db_link_sql);
        --
        execute immediate v_db_link_sql;
        --
        print_log('After creating db link EAAPXPRD.HSI.HUGHESSUPPLY.COM');
        --
            begin
             --
             select count(1) 
             into   n_count
             from   ea_apps.pam_vend_lov_tbl@eaapxprd;
             --
             print_log(' ');             
             print_log(' ****** RULE: In a NON EBIZPRD EBS instance, DB LINK EAAPXPRD must point to EAAPXQA apex database.');
             print_log(' ');                        
             print_log(' ****** BEGIN CHECK TO SEE IF THE DB LINK EAAPXPRD WORKS *******');             
             print_log(' ');
             print_log('@ea_apps.pam_vend_lov_tbl@eaapxprd, n_count ='||n_count);
             print_log(' ');
             print_log('Note: If n_count >0 then the DB link eaapxprd works by pointing to eaapxqa apex database');                          
             print_log(' ');
             print_log(' ****** END CHECK TO SEE IF THE DB LINK EAAPXPRD WORKS *******');                          
             --
             b_proceed :=TRUE;
             --
            exception
             when others then
                 --
                 print_log(' ');             
                 print_log(' ****** RULE: In a NON EBIZPRD EBS instance, DB LINK EAAPXPRD must point to EAAPXQA apex database.');
                 print_log(' ');                        
                 print_log(' ****** BEGIN CHECK TO SEE IF THE DB LINK EAAPXPRD WORKS *******');             
                 print_log(' ');
                 print_log('@ea_apps.pam_vend_lov_tbl@eaapxprd, n_count =0');
                 print_log(' ');
                 print_log('Note: If n_count =0 then the DB link eaapxprd failed in SQL. Please check the db link creation.');                          
                 print_log(' ');
                 print_log(' ****** END CHECK TO SEE IF THE DB LINK EAAPXPRD WORKS *******');                          
                 --
              print_log(' ');              
              print_log('@103, msg ='||sqlerrm);
              print_log(' ');
              b_proceed :=FALSE;
            end;
        --
        exception
         when others then
           print_log('@102, msg ='||sqlerrm);
           b_proceed :=FALSE;
        end;
        --        
       exception
        when others then
         b_proceed :=FALSE;         
         print_log('@104, msg ='||sqlerrm);
       end;
       --
      exception
       when others then
        b_proceed :=FALSE;
        print_log('@105, msg ='||sqlerrm);
      end;
      --
     end if;
     --
    exception
     --
     when others then
       b_proceed :=FALSE;
       print_log('@106, msg ='||sqlerrm);
       --      
    end;
    --
   end if;
   --
   -- Doesn't matter which instance, the variable b_proceed needs to be TRUE to start populating the APEX tables
   --
   if (b_proceed) then 
    --
    Null;
    --
    repop_vend_lov;
    repop_program;
    repop_pam_purch;
    repop_pam_income;
    repop_pam_payments;
    repop_pam_incomelob;
    repop_pam_paydet;
    repop_pam_lob_purchase;
    repop_pam_sv_report;
    --    
   else
    --
    print_log('Variable b_proceed was set to FALSE in a non EBIZPRD instance, exit now');
    --
    xxcus_error_pkg.xxcus_error_main_api
      (
       p_called_from       => l_err_program
      ,p_calling           => 'Check db link EAAPXPRD'
      ,p_ora_error_msg     => SQLERRM
      ,p_error_desc        => '@Check db link EAAPXPRD to verify if its pointing to EAAPXQA apex database.'
      ,p_distribution_list => g_err_email
      ,p_module            => 'Rebates PAM Refresh'
      );
    --                                              
    v_error_msg :='Error found in using the db link eaapxprd in a non prod instance. Please check the concurrent log in full';
    --
    b_proceed :=FND_CONCURRENT.SET_COMPLETION_STATUS('ERROR', v_error_msg);
    --
   end if;
   --
  EXCEPTION
    WHEN OTHERS THEN
      xxcus_error_pkg.xxcus_error_main_api(p_called_from       => l_err_program
                                          ,p_calling           => l_sec
                                          ,p_ora_error_msg     => SQLERRM
                                          ,p_error_desc        => 'Error running package with PROGRAM ERROR'
                                          ,p_distribution_list => g_err_email
                                          ,p_module            => 'PAM');

  END repop_main;

  PROCEDURE repop_vend_lov IS
    -- -----------------------------------------------------------------------------
    -- Name           : repop_vend_lov
    -- Date Written   : 16-JAN-2013
    -- Author         : Manny Rodriguez
    --
    -- Description : delete and Repopulate tables
    --
    --
    -- Modification History:
    --
    -- When         Who        Did what
    -- -----------  --------   -----------------------------------------------------
    -- 15-JAN-2013  Manny      Created
    ---------------------------------------------------------------------------------
    --error handling
    l_err_program VARCHAR2(75) := 'XXCUSOZF_PAM_PKG.repop_vend_lov';
    l_sec         VARCHAR2(4000) DEFAULT 'Starting';
    --parms
    l_count_sql NUMBER;

  BEGIN

    l_sec := 'running count query';

    SELECT COUNT(*) INTO l_count_sql FROM xxcus_vend_lov_v;

    l_sec := 'running delete';

    IF l_count_sql > 0
    THEN
      ea_apps.pam_pkg.repop_vend_lov@eaapxprd;

      l_sec := 'running insert of ' || l_count_sql || ' rows';

      FOR c_loop IN (SELECT mvid, mv_name, cust_id, 'ORACLE' source FROM xxcus_vend_lov_v)
      LOOP

        INSERT INTO ea_apps.pam_vend_lov_tbl@eaapxprd
        VALUES
          (NULL, c_loop.mvid, c_loop.mv_name, c_loop.cust_id, c_loop.source);

        COMMIT;
      END LOOP;

    ELSE

      xxcus_error_pkg.xxcus_error_main_api(p_called_from       => l_err_program
                                          ,p_calling           => 'Didnt get rows from this query for this PROCEDURE'
                                          ,p_ora_error_msg     => SQLERRM
                                          ,p_error_desc        => 'row count was 0 for this query'
                                          ,p_distribution_list => g_err_email2
                                          ,p_module            => 'PAM');

    END IF;

  EXCEPTION
    WHEN OTHERS THEN
      xxcus_error_pkg.xxcus_error_main_api(p_called_from       => l_err_program
                                          ,p_calling           => l_sec
                                          ,p_ora_error_msg     => SQLERRM
                                          ,p_error_desc        => 'Error running package with PROGRAM ERROR'
                                          ,p_distribution_list => g_err_email
                                          ,p_module            => 'PAM');

  END repop_vend_lov;

  PROCEDURE repop_program IS
    -- -----------------------------------------------------------------------------
    -- Name           : repop_program
    -- Date Written   : 16-JAN-2013
    -- Author         : Manny Rodriguez
    --
    -- Description : delete and Repopulate tables
    --
    --
    -- Modification History:
    --
    -- When         Who              Did what
    -- -----------  --------         -----------------------------------------------------
    -- 15-JAN-2013  Manny            Created
    --06/02/2015   Bala Seshadri     Add contract_nbr and plan_id --ESMS 290358
    ---------------------------------------------------------------------------------
    --error handling
    l_err_program VARCHAR2(75) := 'XXCUSOZF_PAM_PKG.repop_program';
    l_sec         VARCHAR2(4000) DEFAULT 'Starting';
    --parms
    l_count_sql NUMBER;

  BEGIN
    l_sec := 'running count query';

    SELECT COUNT(*) INTO l_count_sql FROM xxcus_pam_program_v;

    l_sec := 'running delete';
    IF l_count_sql > 0
    THEN
      ea_apps.pam_pkg.repop_program@eaapxprd;

      l_sec := 'running insert of ' || l_count_sql || ' rows';

      FOR c_loop IN (SELECT mvid
                           ,mv_name
                           ,agreement_yr
                           ,agreement_status
                           ,program_name
                           ,program_description
                           ,rebate_type
                           ,agreement_name
                           ,agreement_description
                           ,payment_method
                           ,payment_freq
                           ,auto_renewal
                           ,until_year
                           ,activity_type
                           ,autopay_enabled_flag
                           ,global_flag
                           ,back_to_or_min_guarantee_amt
                           ,cust_id
                           ,'ORACLE' SOURCE
                           ,contract_nbr --ESMS 290358
                           ,plan_id --ESMS 290358
                       FROM xxcus_pam_program_v)

      LOOP

        INSERT INTO ea_apps.pam_program_list_tbl@eaapxprd
        VALUES
          (NULL
          ,c_loop.mvid
          ,c_loop.mv_name
          ,c_loop.agreement_yr
          ,c_loop.agreement_status
          ,c_loop.program_name
          ,c_loop.program_description
          ,c_loop.rebate_type
          ,c_loop.agreement_name
          ,c_loop.agreement_description
          ,c_loop.payment_method
          ,c_loop.payment_freq
          ,c_loop.auto_renewal
          ,c_loop.until_year
          ,c_loop.activity_type
          ,c_loop.autopay_enabled_flag
          ,c_loop.global_flag
          ,c_loop.back_to_or_min_guarantee_amt
          ,c_loop.cust_id
          ,c_loop.source
          ,c_loop.contract_nbr --ESMS 290358
          ,c_loop.plan_id      --ESMS 290358
           );

        COMMIT;
      END LOOP;
    ELSE
      --email user that no rows exists.
      xxcus_error_pkg.xxcus_error_main_api(p_called_from       => l_err_program
                                          ,p_calling           => 'Didnt get rows from this query for this PROCEDURE'
                                          ,p_ora_error_msg     => SQLERRM
                                          ,p_error_desc        => 'row count was 0 for this query'
                                          ,p_distribution_list => g_err_email2
                                          ,p_module            => 'PAM');

    END IF;

  EXCEPTION
    WHEN OTHERS THEN
      xxcus_error_pkg.xxcus_error_main_api(p_called_from       => l_err_program
                                          ,p_calling           => l_sec
                                          ,p_ora_error_msg     => SQLERRM
                                          ,p_error_desc        => 'Error running package with PROGRAM ERROR'
                                          ,p_distribution_list => g_err_email
                                          ,p_module            => 'PAM');

  END repop_program;

  PROCEDURE repop_pam_purch IS
    -- -----------------------------------------------------------------------------
    -- Name           : repop_pam_purch
    -- Date Written   : 16-JAN-2013
    -- Author         : Manny Rodriguez
    --
    -- Description : delete and Repopulate tables
    --
    --
    -- Modification History:
    --
    -- When         Who        Did what
    -- -----------  --------   -----------------------------------------------------
    -- 15-JAN-2013  Manny      Created
    ---------------------------------------------------------------------------------
    --error handling
    l_err_program VARCHAR2(75) := 'XXCUSOZF_PAM_PKG.repop_pam_purch';
    l_sec         VARCHAR2(4000) DEFAULT 'Starting';
    --parms
    l_count_sql NUMBER;

  BEGIN

    l_sec := 'running count query';
    SELECT COUNT(*) INTO l_count_sql FROM xxcus_pam_ytd_purch_v;
    --  and grp_cal_period=1;

    l_sec := 'running delete';

    IF l_count_sql > 0
    THEN
      ea_apps.pam_pkg.repop_pam_purch@eaapxprd;

      l_sec := 'running insert of ' || l_count_sql || ' rows';

      FOR c_loop IN (SELECT mvid
                           ,lob_id
                           ,calendar_year
                           ,total_purchases
                           ,lob_name
                           ,cust_id
                           ,'ORACLE' SOURCE
                       FROM xxcus_pam_ytd_purch_v)
      LOOP

        INSERT INTO ea_apps.pam_ytd_purch_tbl@eaapxprd
        VALUES
          (NULL
          ,c_loop.mvid
          ,c_loop.lob_id
          ,c_loop.calendar_year
          ,c_loop.total_purchases
          ,c_loop.lob_name
          ,c_loop.cust_id
          ,c_loop.source);
        COMMIT;
      END LOOP;

    ELSE
      xxcus_error_pkg.xxcus_error_main_api(p_called_from       => l_err_program
                                          ,p_calling           => 'Didnt get rows from this query for this PROCEDURE'
                                          ,p_ora_error_msg     => SQLERRM
                                          ,p_error_desc        => 'row count was 0 for this query'
                                          ,p_distribution_list => g_err_email2
                                          ,p_module            => 'PAM');

    END IF;

  EXCEPTION
    WHEN OTHERS THEN
      xxcus_error_pkg.xxcus_error_main_api(p_called_from       => l_err_program
                                          ,p_calling           => l_sec
                                          ,p_ora_error_msg     => SQLERRM
                                          ,p_error_desc        => 'Error running package with PROGRAM ERROR'
                                          ,p_distribution_list => g_err_email
                                          ,p_module            => 'PAM');

  END repop_pam_purch;

  PROCEDURE repop_pam_income IS
    -- -----------------------------------------------------------------------------
    -- Name           : repop_pam_income
    -- Date Written   : 16-JAN-2013
    -- Author         : Manny Rodriguez
    --
    -- Description : delete and Repopulate tables
    --
    --
    -- Modification History:
    --
    -- When         Who        Did what
    -- -----------  --------   -----------------------------------------------------
    -- 15-JAN-2013  Manny      Created
    ---------------------------------------------------------------------------------
    --error handling
    l_err_program VARCHAR2(75) := 'XXCUSOZF_PAM_PKG.repop_pam_income';
    l_sec         VARCHAR2(4000) DEFAULT 'Starting';
    --parms
    l_count_sql NUMBER;

  BEGIN

    l_sec := 'running count query';
    SELECT COUNT(*) INTO l_count_sql FROM xxcus_pam_ytd_income_v;

    l_sec := 'running delete';
    IF l_count_sql > 0
    THEN
      ea_apps.pam_pkg.repop_pam_income@eaapxprd;

      l_sec := 'running insert of ' || l_count_sql || ' rows';
      FOR c_loop IN (SELECT mvid
                           ,lob_id
                           ,rebate_type
                           ,calendar_year
                           ,accrued_amt
                           ,lob_name
                           ,cust_id
                           ,'ORACLE' SOURCE
                       FROM xxcus_pam_ytd_income_v)
      LOOP

        INSERT INTO ea_apps.pam_ytd_income_tbl@eaapxprd
        VALUES
          (NULL
          ,c_loop.mvid
          ,c_loop.lob_id
          ,c_loop.rebate_type
          ,c_loop.calendar_year
          ,c_loop.accrued_amt
          ,c_loop.lob_name
          ,c_loop.cust_id
          ,c_loop.source);
        COMMIT;
      END LOOP;

    ELSE
      xxcus_error_pkg.xxcus_error_main_api(p_called_from       => l_err_program
                                          ,p_calling           => 'Didnt get rows from this query for this PROCEDURE'
                                          ,p_ora_error_msg     => SQLERRM
                                          ,p_error_desc        => 'row count was 0 for this query'
                                          ,p_distribution_list => g_err_email2
                                          ,p_module            => 'PAM');
    END IF;

  EXCEPTION
    WHEN OTHERS THEN
      xxcus_error_pkg.xxcus_error_main_api(p_called_from       => l_err_program
                                          ,p_calling           => l_sec
                                          ,p_ora_error_msg     => SQLERRM
                                          ,p_error_desc        => 'Error running package with PROGRAM ERROR'
                                          ,p_distribution_list => g_err_email
                                          ,p_module            => 'PAM');

  END repop_pam_income;

  PROCEDURE repop_pam_payments IS
    -- -----------------------------------------------------------------------------
    -- Name           : repop_pam_payments
    -- Date Written   : 16-JAN-2013
    -- Author         : Manny Rodriguez
    --
    -- Description : delete and Repopulate tables
    --
    --
    -- Modification History:
    --
    -- When         Who        Did what
    -- -----------  --------   -----------------------------------------------------
    -- 15-JAN-2013  Manny      Created
    ---------------------------------------------------------------------------------
    --error handling
    l_err_program VARCHAR2(75) := 'XXCUSOZF_PAM_PKG.repop_pam_payments';
    l_sec         VARCHAR2(4000) DEFAULT 'Starting';
    --parms
    l_count_sql NUMBER;

  BEGIN
    l_sec := 'running count query';

    SELECT COUNT(*) INTO l_count_sql FROM xxcus_pam_ytd_payments_v;

    l_sec := 'running delete';
    IF l_count_sql > 0
    THEN
      ea_apps.pam_pkg.repop_pam_payments@eaapxprd;

      l_sec := 'running insert of ' || l_count_sql || ' rows';
      FOR c_loop IN (SELECT mvid
                           ,cust_id
                           ,mv_name
                           ,cal_year
                           ,rebate_type
                           ,lob_id
                           ,lob_name
                           ,invoice_amt
                           ,payment_amt
                           ,'ORACLE' SOURCE
                       FROM xxcus_pam_ytd_payments_v)
      LOOP

        INSERT INTO ea_apps.pam_ytd_payments_tbl@eaapxprd
        VALUES
          (NULL
          ,c_loop.mvid
          ,c_loop.cust_id
          ,c_loop.mv_name
          ,c_loop.cal_year
          ,c_loop.rebate_type
          ,c_loop.lob_id
          ,c_loop.lob_name
          ,c_loop.invoice_amt
          ,c_loop.payment_amt
          ,c_loop.source);

        COMMIT;
      END LOOP;

    ELSE
      xxcus_error_pkg.xxcus_error_main_api(p_called_from       => l_err_program
                                          ,p_calling           => 'Didnt get rows from this query for this PROCEDURE'
                                          ,p_ora_error_msg     => SQLERRM
                                          ,p_error_desc        => 'row count was 0 for this query'
                                          ,p_distribution_list => g_err_email2
                                          ,p_module            => 'PAM');

    END IF;

  EXCEPTION
    WHEN OTHERS THEN
      xxcus_error_pkg.xxcus_error_main_api(p_called_from       => l_err_program
                                          ,p_calling           => l_sec
                                          ,p_ora_error_msg     => SQLERRM
                                          ,p_error_desc        => 'Error running package with PROGRAM ERROR'
                                          ,p_distribution_list => g_err_email
                                          ,p_module            => 'PAM');

  END repop_pam_payments;

  PROCEDURE repop_pam_incomelob IS
    -- -----------------------------------------------------------------------------
    -- Name           : repop_pam_incomelob
    -- Date Written   : 16-JAN-2013
    -- Author         : Manny Rodriguez
    --
    -- Description : delete and Repopulate tables
    --
    --
    -- Modification History:
    --
    -- When         Who        Did what
    -- -----------  --------   -----------------------------------------------------
    -- 15-JAN-2013  Manny      Created
    ---------------------------------------------------------------------------------
    --error handling
    l_err_program VARCHAR2(75) := 'XXCUSOZF_PAM_PKG.repop_pam_incomelob';
    l_sec         VARCHAR2(4000) DEFAULT 'Starting';
    --parms
    l_count_sql NUMBER DEFAULT 1; --bypassing for now.

  BEGIN

    l_sec := 'running count query';

    SELECT COUNT(*) INTO l_count_sql FROM xxcus_pam_income_lob_v;

    l_sec := 'running delete';
    IF l_count_sql > 0
    THEN
      ea_apps.pam_pkg.repop_pam_incomelob@eaapxprd;

      l_sec := 'running insert of ' || l_count_sql || ' rows';

      FOR c_loop IN (SELECT mvid
                           ,mv_name
                           ,lob_name
                           ,cal_year
                           ,rebate_type
                           ,month_name
                           ,amount
                           ,cust_id
                           ,month_id
                           ,agreement_year
                           ,'ORACLE' SOURCE
                       FROM xxcus_pam_income_lob_v)
      LOOP

        INSERT INTO ea_apps.pam_income_lob_tbl@eaapxprd
        VALUES
          (NULL
          ,c_loop.mvid
          ,c_loop.mv_name
          ,c_loop.lob_name
          ,c_loop.cal_year
          ,c_loop.rebate_type
          ,c_loop.month_name
          ,c_loop.amount
          ,c_loop.cust_id
          ,c_loop.month_id
          ,c_loop.agreement_year
          ,c_loop.source);

        COMMIT;
      END LOOP;

    ELSE
      xxcus_error_pkg.xxcus_error_main_api(p_called_from       => l_err_program
                                          ,p_calling           => 'Didnt get rows from this query for this PROCEDURE'
                                          ,p_ora_error_msg     => SQLERRM
                                          ,p_error_desc        => 'row count was 0 for this query'
                                          ,p_distribution_list => g_err_email2
                                          ,p_module            => 'PAM');

    END IF;

  EXCEPTION
    WHEN OTHERS THEN
      xxcus_error_pkg.xxcus_error_main_api(p_called_from       => l_err_program
                                          ,p_calling           => l_sec
                                          ,p_ora_error_msg     => SQLERRM
                                          ,p_error_desc        => 'Error running package with PROGRAM ERROR'
                                          ,p_distribution_list => g_err_email
                                          ,p_module            => 'PAM');

  END repop_pam_incomelob;

  PROCEDURE repop_pam_paydet IS
    -- -----------------------------------------------------------------------------
    -- Name           : repop_pam_paydet
    -- Date Written   : 16-JAN-2013
    -- Author         : Manny Rodriguez
    --
    -- Description : delete and Repopulate tables
    --
    --
    -- Modification History:
    --
    -- When         Who        Did what
    -- -----------  --------   -----------------------------------------------------
    -- 15-JAN-2013  Manny      Created
    -- 10-FEB-2014  Dragan     Added GL_DATE SR 238847 RfC 39357
    -- 10/07/2015   Balaguru Seshadri TMS 20151001-00046 [Ver 1.5] 
    ---------------------------------------------------------------------------------
    --error handling
    l_err_program VARCHAR2(75) := 'XXCUSOZF_PAM_PKG.repop_pam_paydet';
    l_sec         VARCHAR2(4000) DEFAULT 'Starting';
    --parms
    l_count_sql NUMBER;

  BEGIN

    l_sec := 'running count query';

    SELECT COUNT(*) INTO l_count_sql FROM xxcus_pam_payment_det_v;

    l_sec := 'running delete';
    IF l_count_sql > 0
    THEN
      ea_apps.pam_pkg.repop_pam_paydet@eaapxprd;

      l_sec := 'running insert of ' || l_count_sql || ' rows';
      FOR c_loop IN (SELECT cust_id
                           ,mvid
                           ,mv_name
                           ,payment_date
                           ,creation_date
                           ,receipt_timeframe
                           ,payment_amount
                           ,currency_code
                           ,business_unit
                           ,payment_method
                           ,receipt_reference
                           ,payment_number
                           ,image_url
                           ,amount_due_remaining
                           ,'ORACLE' SOURCE
                           ,gl_date
                           ,agreement_code --TMS 20151001-00046 
                           ,agreement_name --TMS 20151001-00046 
                           ,agreement_year --TMS 20151001-00046 
                           ,rebate_type --TMS 20151001-00046 
                       FROM xxcus_pam_payment_det_v
                     )
      LOOP

        INSERT INTO ea_apps.pam_payment_det_tbl@eaapxprd
        --Begin Ver 1.5        
        (
            id
          ,cust_id
          ,mvid
          ,mv_name
          ,payment_date
          ,creation_date
          ,receipt_timeframe
          ,payment_amount
          ,currency_code
          ,business_unit
          ,payment_method
          ,receipt_reference
          ,payment_number
          ,image_url
          ,amount_due_remaining
          ,source
          ,gl_date
          ,agreement_year
          ,rebate_type
          ,agreement_code
          ,agreement_name 
          )      
        --End Ver 1.5
        VALUES
          (NULL
          ,c_loop.cust_id
          ,c_loop.mvid
          ,c_loop.mv_name
          ,c_loop.payment_date
          ,c_loop.creation_date
          ,c_loop.receipt_timeframe
          ,c_loop.payment_amount
          ,c_loop.currency_code
          ,c_loop.business_unit
          ,c_loop.payment_method
          ,c_loop.receipt_reference
          ,c_loop.payment_number
          ,c_loop.image_url
          ,c_loop.amount_due_remaining
          ,C_LOOP.SOURCE
          --,c_loop.gl_date); -- TMS 20151001-00046
          ,c_loop.gl_date -- TMS 20151001-00046
          ,c_loop.agreement_year -- TMS 20151001-00046
          ,c_loop.rebate_type -- TMS 20151001-00046
          ,c_loop.agreement_code -- TMS 20151001-00046
          ,c_loop.agreement_name -- TMS 20151001-00046                                        
          ); 
        COMMIT;
      END LOOP;

    ELSE
      xxcus_error_pkg.xxcus_error_main_api(p_called_from       => l_err_program
                                          ,p_calling           => 'Didnt get rows from this query for this PROCEDURE'
                                          ,p_ora_error_msg     => SQLERRM
                                          ,p_error_desc        => 'row count was 0 for this query'
                                          ,p_distribution_list => g_err_email2
                                          ,p_module            => 'PAM');

    END IF;

  EXCEPTION
    WHEN OTHERS THEN
      xxcus_error_pkg.xxcus_error_main_api(p_called_from       => l_err_program
                                          ,p_calling           => l_sec
                                          ,p_ora_error_msg     => SQLERRM
                                          ,p_error_desc        => 'Error running package with PROGRAM ERROR'
                                          ,p_distribution_list => g_err_email
                                          ,p_module            => 'PAM');

  END repop_pam_paydet;

  PROCEDURE repop_pam_lob_purchase IS
    -- -----------------------------------------------------------------------------
    -- Name           : repop_vend_lov
    -- Date Written   : 20-MAR-2013
    -- Author         : Dragan Velimirovic
    --
    -- Description : delete and Repopulate tables
    --
    --
    -- Modification History:
    --
    -- When         Who        Did what
    -- -----------  --------   -----------------------------------------------------
    -- 20-MAR-2013  Dragan      Created
    ---------------------------------------------------------------------------------
    --error handling
    l_err_program VARCHAR2(75) := 'XXCUSOZF_PAM_PKG.repop_pam_lob_purchase';
    l_sec         VARCHAR2(4000) DEFAULT 'Starting';
    --parms
    l_count_sql NUMBER;

  BEGIN

    l_sec := 'running count query';

    SELECT COUNT(*) INTO l_count_sql FROM xxcus_pam_purchase_lob_v;

    l_sec := 'running delete';

    IF l_count_sql > 0
    THEN
      ea_apps.pam_pkg.repop_pam_lob_purchase@eaapxprd;

      l_sec := 'running insert of ' || l_count_sql || ' rows';

      FOR c_loop IN (SELECT mvid
                           ,cust_id
                           ,mv_name
                           ,lob_name
                           ,cal_year
                           ,month_name
                           ,pur_amount
                           ,month_id
                           ,'ORACLE' SOURCE
                       FROM xxcus_pam_purchase_lob_v)
      LOOP

        INSERT INTO ea_apps.pam_purchase_lob_tbl@eaapxprd
        VALUES
          (NULL
          ,c_loop.mvid
          ,c_loop.cust_id
          ,c_loop.mv_name
          ,c_loop.lob_name
          ,c_loop.cal_year
          ,c_loop.month_name
          ,c_loop.pur_amount
          ,c_loop.month_id
          ,c_loop.source);

        COMMIT;
      END LOOP;

    ELSE

      xxcus_error_pkg.xxcus_error_main_api(p_called_from       => l_err_program
                                          ,p_calling           => 'Didnt get rows from this query for this PROCEDURE'
                                          ,p_ora_error_msg     => SQLERRM
                                          ,p_error_desc        => 'row count was 0 for this query'
                                          ,p_distribution_list => g_err_email2
                                          ,p_module            => 'PAM');

    END IF;

  EXCEPTION
    WHEN OTHERS THEN
      xxcus_error_pkg.xxcus_error_main_api(p_called_from       => l_err_program
                                          ,p_calling           => l_sec
                                          ,p_ora_error_msg     => SQLERRM
                                          ,p_error_desc        => 'Error running package with PROGRAM ERROR'
                                          ,p_distribution_list => g_err_email
                                          ,p_module            => 'PAM');

  END repop_pam_lob_purchase;

  PROCEDURE repop_pam_sv_report IS
    -- -----------------------------------------------------------------------------
    -- Name           : repop_pam_sv_report
    -- Date Written   : 09-JUL-2013
    -- Author         : Dragan Velimirovic
    --
    -- Description : delete and Repopulate tables
    --
    --
    -- Modification History:
    --
    -- When         Who        Did what
    -- -----------  --------   -----------------------------------------------------
    -- 09-JUL-2013  Dragan      Created
    -- 10-FEB-2013  Dragan      Added FISCAL_PYTD_INC and CALENDAR_PYTD_INC
    --                          SR 238847 RfC 39357
    ---------------------------------------------------------------------------------
    --error handling
    l_err_program VARCHAR2(75) := 'XXCUSOZF_PAM_PKG.repop_pam_sv_report';
    l_sec         VARCHAR2(4000) DEFAULT 'Starting';
    --parms
    l_count_sql NUMBER;

  BEGIN

    l_sec := 'running count query';

    SELECT COUNT(*)
      INTO l_count_sql
      FROM TABLE(xxeis_rebates_com_util_pkg.get_sv_data(xxeis_rebates_com_util_pkg.get_period));

    l_sec := 'running delete';

    IF l_count_sql > 0
    THEN
      ea_apps.pam_pkg.repop_pam_sv_report@eaapxprd;

      l_sec := 'running insert of ' || l_count_sql || ' rows';

      FOR c_loop IN (SELECT mvid
                           ,mvid_name
                           ,lob
                           ,fiscal_cytd_purch
                           ,fiscal_cytd_inc
                           ,calendar_cytd_purch
                           ,calendar_cytd_inc
                           ,report_fiscal_period
                           ,calendar_pytd_purch
                           ,FISCAL_PYTD_PURCH
                           ,FISCAL_PYTD_INC
                           ,calendar_pytd_inc

                       FROM TABLE(xxeis_rebates_com_util_pkg.get_sv_data(xxeis_rebates_com_util_pkg.get_period)))
      LOOP

        INSERT INTO ea_apps.pam_sv_report_tbl@eaapxprd
        VALUES
          (NULL
          ,c_loop.mvid
          ,c_loop.mvid_name
          ,c_loop.lob
          ,c_loop.fiscal_cytd_purch
          ,c_loop.fiscal_cytd_inc
          ,c_loop.calendar_cytd_purch
          ,c_loop.calendar_cytd_inc
          ,c_loop.report_fiscal_period
          ,c_loop.calendar_pytd_purch
          ,C_LOOP.FISCAL_PYTD_PURCH
          ,C_LOOP.FISCAL_PYTD_INC
          ,C_LOOP.CALENDAR_PYTD_INC);

        COMMIT;
      END LOOP;

    ELSE

      xxcus_error_pkg.xxcus_error_main_api(p_called_from       => l_err_program
                                          ,p_calling           => 'Didnt get rows from this query for this PROCEDURE'
                                          ,p_ora_error_msg     => SQLERRM
                                          ,p_error_desc        => 'row count was 0 for this query'
                                          ,p_distribution_list => g_err_email2
                                          ,p_module            => 'PAM');

    END IF;

  EXCEPTION
    WHEN OTHERS THEN
      xxcus_error_pkg.xxcus_error_main_api(p_called_from       => l_err_program
                                          ,p_calling           => l_sec
                                          ,p_ora_error_msg     => SQLERRM
                                          ,p_error_desc        => 'Error running package with PROGRAM ERROR'
                                          ,p_distribution_list => g_err_email
                                          ,p_module            => 'PAM');

  END repop_pam_sv_report;
END xxcusozf_pam_pkg;
/
show errors
/