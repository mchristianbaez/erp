CREATE OR REPLACE PACKAGE BODY APPS.xxcus_ozf_rcpt_imp_status_pkg as
/*
 -- ESMS / RFC /TMS                                                Date                 Author                         Notes
 -- ========================================================================================================     
    265235                                                                  10/12/2014   Balaguru Seshadri   Initial version   
    TMS 20160407-00172 / ESMS 322629       04/08/2016   Balaguru Seshadri   EBSPRD migration related fixes FOR GSC objects    
   TMS 20160509-00109 / ESMS 322629        05/09/2016   Balaguru Seshadri , Rollback edits to MS 20160407-00172 / ESMS 322629  FOR GSC objects      
 -- ========================================================================================================   
*/
   --
    n_excep_req_id NUMBER :=0;
   --
    procedure print_log(p_message in varchar2) is
    begin
     if  fnd_global.conc_request_id >0 then
      fnd_file.put_line(fnd_file.log, p_message);
     else
      dbms_output.put_line(p_message);
     end if;
    end print_log;       
    --
    function get_total_receipts return number as       
    begin 
     return xxcus_ozf_rcpt_imp_status_pkg.g_total_receipts;
    exception
     when others then
      print_log('@get_total_receipts, error message ='||sqlerrm);
      return null;
    end get_total_receipts;  
    --
    function get_exception_flag return varchar2 as       
    begin 
     return xxcus_ozf_rcpt_imp_status_pkg.g_exception_flag;
    exception
     when others then
       print_log('@get_exception_flag, error message ='||sqlerrm);
      return null;
    end get_exception_flag;  
    --
    function get_email_text return varchar2 as       
    begin 
     if xxcus_ozf_rcpt_imp_status_pkg.g_exception_flag ='Y' then
      return
       (
        fnd_profile.value('HDS_REBATES_RCPTS_EXCEPTION_FOUND_NOTIF')
        ||' Please contact IT team to research issue and resolve:  hds-SourcingOpsIT-u1@hdsupply.com'
       );
     else
      return
       (
        fnd_profile.value('HDS_REBATES_RCPTS_SUCCESSFUL_NOTIF')
       );      
     end if;
     return xxcus_ozf_rcpt_imp_status_pkg.g_exception_flag;
    exception
     when others then
       print_log('@get_email_text, error message ='||sqlerrm);
      return null;
    end get_email_text;  
    --            
    function beforereport return boolean as  
     cursor RBT_Unprocessed_Receipts 
        (
            p_fiscal_period in varchar2
           ,p_fiscal_start  in date
           ,p_fiscal_end    in date
           ,p_user_id       in number
           ,p_sysdate       in date
           ,p_req_id        in number
        ) is
     select 
        p_fiscal_period        fiscal_period
       ,fiscal_per_id          fiscal_period_id  
       ,p_fiscal_start         fiscal_start_date
       ,p_fiscal_end           fiscal_end_date              
       ,bu_nm                  bu_name
       ,rebt_vndr_cd           mvid
       ,rebt_vndr_nm           mvid_name
       ,comments               receipt_load_comments
       ,nvl(recpt_qty, 0)      recpt_qty
       ,round(nvl(item_cost_amt, 0), 2)  item_cost_amt
       ,p_user_id              created_by
       ,p_sysdate              creation_date
       ,p_req_id               request_id
     from xxcus.xxcus_rebate_receipt_curr_tbl
     where 1 = 1
        and status_flag = 'N'
        and recpt_crt_dt between p_fiscal_start and p_fiscal_end
        and comments is null;     
     --     
     type rbt_unprocessed_rctps_tbl is table of RBT_Unprocessed_Receipts%rowtype index by binary_integer; 
     rbt_unprocessed_rctps_rec rbt_unprocessed_rctps_tbl;
     --
     v_hds_divisions varchar2(240) :=Null;
     --
     p_limit number :=5000;
     -- ========================
      b_move_fwd         boolean :=FALSE;
      v_detail_exists    varchar2(1) :=Null;
      n_header_no        number :=0;
      n_total_ytd        number :=0;
      n_total_rebates    number :=0;
      d_fiscal_start     date;
      d_fiscal_end       date;
      b_exception_found  boolean :=FALSE;
      b_no_receipts      boolean;
      n_count            number :=0;
     -- ========================  
    begin 
     --  
     Execute Immediate 'truncate table xxcus.xxcus_ozf_unprocessed_receipts';
     --
     Commit;
     --    
      Begin
        -- 
        begin 
         --
         select start_date
               ,end_date
         into   d_fiscal_start
               ,d_fiscal_end
         from   gl_periods
         where  1 =1
           and  period_name =xxcus_ozf_rcpt_imp_status_pkg.g_fiscal_period
           and  adjustment_period_flag ='N';
         --
         b_move_fwd :=TRUE;
         --
        exception
         when no_data_found then
          print_log('Failed to fetch start and end dates for fiscal period ='||xxcus_ozf_rcpt_imp_status_pkg.g_fiscal_period);         
          b_move_fwd :=FALSE;
         when others then
          print_log('Other errors in fetch of start and end dates for fiscal period ='||xxcus_ozf_rcpt_imp_status_pkg.g_fiscal_period);
          b_move_fwd :=FALSE;         
        end;
        -- 
          print_log('');
          print_log('Parameters:');
          print_log('===========');                    
          print_log('g_fiscal_period ='||xxcus_ozf_rcpt_imp_status_pkg.g_fiscal_period);
          print_log('d_fiscal_start ='||to_char(d_fiscal_start, 'mm/dd/yyyy'));
          print_log('d_fiscal_end ='||to_char(d_fiscal_end, 'mm/dd/yyyy'));                    
          print_log('request_id ='||fnd_global.conc_request_id);
          n_excep_req_id :=fnd_global.conc_request_id;      
          print_log('');        
        --
        EXECUTE IMMEDIATE 'TRUNCATE TABLE XXCUS.XXCUS_OZF_UNPROCESSED_RECEIPTS';
        --   
        if (b_move_fwd) then 
         --
         begin 
            --
            begin 
             select count(1)
             into   n_count 
             from   xxcus.xxcus_rebate_receipt_curr_tbl
             where 1 = 1
                and recpt_crt_dt between d_fiscal_start and d_fiscal_end;
            exception
             when others then
               print_log('@get receipts for fiscal period, error ='||sqlerrm);          
            end;
            --
            if n_count >0 then 
                print_log('Before open of cursor RBT_Unprocessed_Receipts');
                --  
                open RBT_Unprocessed_Receipts 
                    (
                        p_fiscal_period =>xxcus_ozf_rcpt_imp_status_pkg.g_fiscal_period
                       ,p_fiscal_start  =>d_fiscal_start
                       ,p_fiscal_end    =>d_fiscal_end
                       ,p_user_id       =>fnd_global.user_id
                       ,p_sysdate       =>trunc(sysdate)
                       ,p_req_id        =>fnd_global.conc_request_id
                    );
                --
                loop
                  fetch RBT_Unprocessed_Receipts bulk collect into rbt_unprocessed_rctps_rec limit p_limit;
                  exit when rbt_unprocessed_rctps_rec.count =0;
                  if rbt_unprocessed_rctps_rec.count >0 then
                   --
                   xxcus_ozf_rcpt_imp_status_pkg.g_total_receipts 
                      :=xxcus_ozf_rcpt_imp_status_pkg.g_total_receipts + rbt_unprocessed_rctps_rec.count; 
                   --
                    if Not (b_exception_found) then
                     --
                     b_exception_found :=TRUE;
                     --
                    end if;
                   --
                     begin
                      forall idx in rbt_unprocessed_rctps_rec.first .. rbt_unprocessed_rctps_rec.last
                        insert into xxcus.xxcus_ozf_unprocessed_receipts values rbt_unprocessed_rctps_rec (idx);
                       --
                       commit;
                       --
                     exception
                       when others then
                         print_log('Issue in bulk insert ,message ='||sqlerrm);
                         rollback;
                     end;
                   -- 
                  else
                   print_log('rbt_unprocessed_rctps_rec.count ='||rbt_unprocessed_rctps_rec.count);    
                  end if;  --my_hds_hdr_rec.count >0
                end loop;
                --
                close RBT_Unprocessed_Receipts;
                --
                print_log('After close of cursor RBT_Unprocessed_Receipts');
                --
                  if (b_exception_found) then  
                   --
                   xxcus_ozf_rcpt_imp_status_pkg.g_exception_flag :='Y';
                   --
                  else
                   --
                   xxcus_ozf_rcpt_imp_status_pkg.g_exception_flag :='N';
                   --
                  end if;
                  --                            
            else --there are no receipts in first place for the fiscal period
             --
             xxcus_ozf_rcpt_imp_status_pkg.g_exception_flag :='NA';
             --
            end if;
            --         
         exception        
          when others then
           print_log('@ open cursor RBT_Unprocessed_Receipts, msg ='||sqlerrm);
         end;        
         --
        end if;
        --        
      Exception        
       When Others Then
        print_log('Others: Error in getting RBT_Unprocessed_Receipts information, msg ='||sqlerrm);
      End;
      --
      print_log('xxcus_ozf_rcpt_imp_status_pkg.g_exception_flag ='||xxcus_ozf_rcpt_imp_status_pkg.g_exception_flag);
      print_log('xxcus_ozf_rcpt_imp_status_pkg.g_total_receipts ='||xxcus_ozf_rcpt_imp_status_pkg.g_total_receipts);
      --
      return TRUE;
      --
    exception
     when others then
      print_log('Error in beforereport '||sqlerrm);
      return FALSE;
    end beforereport;
    --
    function afterreport return boolean as 
     --
     ln_request_id Number :=0;
     --
     v_file varchar2(60) :=Null;
     v_email varchar2(60) :=Null;
     --
     v_curent_db_name        VARCHAR2(20) :='NA';
     v_prod_db_name          VARCHAR2(20) :='EBIZPRD'; --TMS 20160407-00172 / ESMS 322629 --TMS: 20160509-00109
     --v_prod_db_name          VARCHAR2(20) :='EBSPRD'; --TMS 20160407-00172 / ESMS 322629     --TMS: 20160509-00109
     v_prod_rbt_email_id     VARCHAR2(40) :='hdsoraclerebates@hdsupply.com'; 
     --     
     v_fiscal_period varchar2(10) :=replace(xxcus_ozf_rcpt_imp_status_pkg.g_fiscal_period, '-', '');
     v_exception_flag varchar2(2) :=xxcus_ozf_rcpt_imp_status_pkg.g_exception_flag;
     --      
    begin 
      --
      begin
        select upper(name) 
        into   v_curent_db_name
        from v$database;            
      exception
       when others then
        v_curent_db_name :='NA';
        print_log('Unable to get current DB Name from v$database, message ='||sqlerrm);
      end;
      --
      if (v_curent_db_name =v_prod_db_name) then 
       v_email :=v_prod_rbt_email_id;
      else
       begin
         select email_address
         into   v_email
         from   fnd_user
         where  1 =1
           and  user_id =fnd_global.user_id;
       exception
        when others then 
         v_email :='NA';            
         print_log('@get v_email, error message ='||sqlerrm);                          
       end;            
      end if;    
      --
     if (v_email <>'NA') then
        --
        v_file :='XXCUS_OZF_RBT_RCPTS_IMP_STATUS'||'_'||n_excep_req_id||'_1.PDF';
        --     
        ln_request_id :=fnd_request.submit_request
              (
               application      =>'XXCUS',
               program          =>'XXCUS_OZF_EMAIL_EXCEP_RPT',
               description      =>'',
               start_time       =>'',
               sub_request      =>FALSE,
               argument1        =>v_file,
               argument2        =>v_email,
               argument3        =>v_fiscal_period,
               argument4        =>v_exception_flag                                                                           
              );
          
          if ln_request_id >0 then 
             print_log('Submitted Rebates import exception report status email, request_id ='||ln_request_id);
             commit work; 
          else
             Null; 
             print_log('Failed to submit Rebates import exception report status email');
          end if;     
     else
      print_log('Cannot email the output because email address of the user is blank.');
     end if;
     --     
     return TRUE;
    exception
     when others then
      print_log('Error in afterreport '||sqlerrm);
      return FALSE;
    end afterreport;  
    --
end xxcus_ozf_rcpt_imp_status_pkg;
/
show errors
/