---------------------------------------------------------------------------------------------------------------
/**************************************************************************************************************
  $Header XXEIS.EIS_XXWC_AR_CUST_RECON_TRX_V $
  Module Name : Receivables
  PURPOSE	  : Customer Account Recon - Transaction View
  TMS Task Id : 20130909-00776 
  REVISIONS   :
  VERSION 			DATE               AUTHOR(S)       DESCRIPTION
  ------- -----------------  --------------- -----------------------------------------
   1.0     		03-Oct-2016       		Siva   		 TMS#20130909-00776 
**************************************************************************************************************/
DROP VIEW XXEIS.EIS_XXWC_AR_CUST_RECON_TRX_V;

CREATE OR REPLACE VIEW XXEIS.EIS_XXWC_AR_CUST_RECON_TRX_V
AS
  SELECT CUSTOMER_NAME,
    CUSTOMER_NUMBER,
    PHONE_NUMBER,
    CREDIT_MANAGER,
    ACCOUNT_MANAGER,
    PAYMENT_TERMS,
    ACCOUNT_STATUS,
    PARTY_SITE_NUMBER,
    LOCATION,
    TRANSACTION_NUMBER,
    TRX_DATE,
    PO_NUMBER,
    TRX_AMOUNT_DUE_ORIGINAL,
    TRANSACTION_BALANCE_DUE,
    TAX_BALANCE_DUE,
    EARNED_DISCOUNT,
    DAYS_LATE,
    TRX_PAYMENT_TERMS,
    APPLIED_TRX_TYPE,
    PAYMENT_METHOD ,
    APPLIED_TRX_NUMBER,
    APPLIED_AMOUNT,
    STATE,
    ACTIVITY_DATE,
    CREATED_BY ,
    REFERENCE
  FROM XXEIS.XXWC_CUST_RECON_TRX_TMP_TBL
/
