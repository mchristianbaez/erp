--Report Name            : Customer Account Recon - Receipt View - WC
--Import Instructions    : Run this file in XXEIS user from SQL*Plus or any tool like Toad, SQL Navigator. 
prompt Creating View Data for Customer Account Recon - Receipt View - WC
set scan off define off
DECLARE
BEGIN 
--Inserting View EIS_XXWC_AR_CUST_RECON_RECPT_V
xxeis.eis_rs_ins.v( 'EIS_XXWC_AR_CUST_RECON_RECPT_V',222,'','','','','SA059956','XXEIS','EIS XXWC Ar Cust Acc Recon V','EXACARV','','');
--Delete View Columns for EIS_XXWC_AR_CUST_RECON_RECPT_V
xxeis.eis_rs_utility.delete_view_rows('EIS_XXWC_AR_CUST_RECON_RECPT_V',222,FALSE);
--Inserting View Columns for EIS_XXWC_AR_CUST_RECON_RECPT_V
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_CUST_RECON_RECPT_V','ON_ACCOUNT_AMOUNT',222,'On Account Amount','ON_ACCOUNT_AMOUNT','','','','SA059956','NUMBER','','','On Account Amount','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_CUST_RECON_RECPT_V','PREPAYMENT_AMOUNT',222,'Prepayment Amount','PREPAYMENT_AMOUNT','','','','SA059956','NUMBER','','','Prepayment Amount','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_CUST_RECON_RECPT_V','UNAPPLIED_AMOUNT',222,'Unapplied Amount','UNAPPLIED_AMOUNT','','','','SA059956','NUMBER','','','Unapplied Amount','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_CUST_RECON_RECPT_V','STATE',222,'State','STATE','','','','SA059956','VARCHAR2','','','State','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_CUST_RECON_RECPT_V','APPLIED_AMOUNT',222,'Applied Amount','APPLIED_AMOUNT','','','','SA059956','NUMBER','','','Applied Amount','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_CUST_RECON_RECPT_V','RECEIPT_AMOUNT',222,'Receipt Amount','RECEIPT_AMOUNT','','','','SA059956','NUMBER','','','Receipt Amount','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_CUST_RECON_RECPT_V','PAYMENT_METHOD',222,'Payment Method','PAYMENT_METHOD','','','','SA059956','VARCHAR2','','','Payment Method','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_CUST_RECON_RECPT_V','DISCREPANCY_CODE',222,'Discrepancy Code','DISCREPANCY_CODE','','','','SA059956','VARCHAR2','','','Discrepancy Code','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_CUST_RECON_RECPT_V','RECEIPT_COMMENTS',222,'Receipt Comments','RECEIPT_COMMENTS','','','','SA059956','VARCHAR2','','','Receipt Comments','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_CUST_RECON_RECPT_V','UNEARNED_DISCOUNT',222,'Unearned Discount','UNEARNED_DISCOUNT','','','','SA059956','NUMBER','','','Unearned Discount','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_CUST_RECON_RECPT_V','EARNED_DISCOUNT',222,'Earned Discount','EARNED_DISCOUNT','','','','SA059956','NUMBER','','','Earned Discount','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_CUST_RECON_RECPT_V','APPLIED_DATE',222,'Applied Date','APPLIED_DATE','','','','SA059956','DATE','','','Applied Date','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_CUST_RECON_RECPT_V','DEPOSIT_DATE',222,'Deposit Date','DEPOSIT_DATE','','','','SA059956','DATE','','','Deposit Date','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_CUST_RECON_RECPT_V','TRX_AMOUNT_DUE_ORIGINAL',222,'Trx Amount Due Original','TRX_AMOUNT_DUE_ORIGINAL','','','','SA059956','NUMBER','','','Trx Amount Due Original','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_CUST_RECON_RECPT_V','TRX_DATE',222,'Trx Date','TRX_DATE','','','','SA059956','DATE','','','Trx Date','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_CUST_RECON_RECPT_V','TRANSACTION_NUMBER',222,'Transaction Number','TRANSACTION_NUMBER','','','','SA059956','VARCHAR2','','','Transaction Number','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_CUST_RECON_RECPT_V','RECEIPT_DATE',222,'Receipt Date','RECEIPT_DATE','','','','SA059956','DATE','','','Receipt Date','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_CUST_RECON_RECPT_V','RECEIPT_NUMBER',222,'Receipt Number','RECEIPT_NUMBER','','','','SA059956','VARCHAR2','','','Receipt Number','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_CUST_RECON_RECPT_V','LOCATION',222,'Location','LOCATION','','','','SA059956','VARCHAR2','','','Location','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_CUST_RECON_RECPT_V','PARTY_SITE_NUMBER',222,'Party Site Number','PARTY_SITE_NUMBER','','','','SA059956','VARCHAR2','','','Party Site Number','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_CUST_RECON_RECPT_V','ACCOUNT_STATUS',222,'Account Status','ACCOUNT_STATUS','','','','SA059956','VARCHAR2','','','Account Status','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_CUST_RECON_RECPT_V','PAYMENT_TERMS',222,'Payment Terms','PAYMENT_TERMS','','','','SA059956','VARCHAR2','','','Payment Terms','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_CUST_RECON_RECPT_V','ACCOUNT_MANAGER',222,'Account Manager','ACCOUNT_MANAGER','','','','SA059956','VARCHAR2','','','Account Manager','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_CUST_RECON_RECPT_V','CREDIT_MANAGER',222,'Credit Manager','CREDIT_MANAGER','','','','SA059956','VARCHAR2','','','Credit Manager','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_CUST_RECON_RECPT_V','PHONE_NUMBER',222,'Phone Number','PHONE_NUMBER','','','','SA059956','VARCHAR2','','','Phone Number','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_CUST_RECON_RECPT_V','CUSTOMER_NUMBER',222,'Customer Number','CUSTOMER_NUMBER','','','','SA059956','VARCHAR2','','','Customer Number','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_CUST_RECON_RECPT_V','CUSTOMER_NAME',222,'Customer Name','CUSTOMER_NAME','','','','SA059956','VARCHAR2','','','Customer Name','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_CUST_RECON_RECPT_V','TRANSACTION_BALANCE_DUE',222,'Transaction Balance Due','TRANSACTION_BALANCE_DUE','','','','SA059956','NUMBER','','','Transaction Balance Due','','','');
--Inserting View Components for EIS_XXWC_AR_CUST_RECON_RECPT_V
--Inserting View Component Joins for EIS_XXWC_AR_CUST_RECON_RECPT_V
END;
/
set scan on define on
prompt Creating Report LOV Data for Customer Account Recon - Receipt View - WC
set scan off define off
DECLARE
BEGIN 
--Inserting Report LOVs - Customer Account Recon - Receipt View - WC
xxeis.eis_rs_ins.lov( 222,'SELECT DISTINCT receipt_number FROM ar_cash_receipts','','RECEIPT NUMBER','','XXEIS_RS_ADMIN',NULL,'','','');
xxeis.eis_rs_ins.lov( 222,'SELECT NVL(hca.account_name, hp.party_name) customer_name,hca.account_number customer_number
FROM hz_cust_accounts hca,
  hz_parties hp
where hca.party_id=hp.party_id
ORDER BY 1','','XXWC Customer Name LOV','Display List of Values for Customer Name','ANONYMOUS',NULL,'N','','');
xxeis.eis_rs_ins.lov( 222,'SELECT hca.account_number customer_number,NVL(hca.account_name, hp.party_name) customer_name
FROM hz_cust_accounts hca,
  hz_parties hp
where hca.party_id=hp.party_id
ORDER BY lpad(hca.account_number,10)','','XXWC Customer Number LOV','Displays List of Values for Customer Number','ANONYMOUS',NULL,'N','','');
END;
/
set scan on define on
prompt Creating Report Data for Customer Account Recon - Receipt View - WC
set scan off define off
DECLARE
BEGIN 
--Deleting Report data - Customer Account Recon - Receipt View - WC
xxeis.eis_rs_utility.delete_report_rows( 'Customer Account Recon - Receipt View - WC' );
--Inserting Report - Customer Account Recon - Receipt View - WC
xxeis.eis_rs_ins.r( 222,'Customer Account Recon - Receipt View - WC','','The report will provide transaction and/or receipt activity for a specific customer within a specific required time frame.','','','','SA059956','EIS_XXWC_AR_CUST_RECON_RECPT_V','Y','','','SA059956','','N','White Cap Reports','','CSV,EXCEL,','N');
--Inserting Report Columns - Customer Account Recon - Receipt View - WC
xxeis.eis_rs_ins.rc( 'Customer Account Recon - Receipt View - WC',222,'DISCREPANCY_CODE','Discrepancy Code','Discrepancy Code','','','default','','14','N','','','','','','','','SA059956','N','N','','EIS_XXWC_AR_CUST_RECON_RECPT_V','','');
xxeis.eis_rs_ins.rc( 'Customer Account Recon - Receipt View - WC',222,'EARNED_DISCOUNT','Earned Discount','Earned Discount','','~T~D~2','default','','9','N','','','','','','','','SA059956','N','N','','EIS_XXWC_AR_CUST_RECON_RECPT_V','','');
xxeis.eis_rs_ins.rc( 'Customer Account Recon - Receipt View - WC',222,'LOCATION','Location','Location','','','default','','2','N','','','','','','','','SA059956','N','N','','EIS_XXWC_AR_CUST_RECON_RECPT_V','','');
xxeis.eis_rs_ins.rc( 'Customer Account Recon - Receipt View - WC',222,'ON_ACCOUNT_AMOUNT','On Account Amount','On Account Amount','','~T~D~2','default','','19','N','','','','','','','','SA059956','N','N','','EIS_XXWC_AR_CUST_RECON_RECPT_V','','');
xxeis.eis_rs_ins.rc( 'Customer Account Recon - Receipt View - WC',222,'PARTY_SITE_NUMBER','Party Site Number','Party Site Number','','','default','','1','N','','','','','','','','SA059956','N','N','','EIS_XXWC_AR_CUST_RECON_RECPT_V','','');
xxeis.eis_rs_ins.rc( 'Customer Account Recon - Receipt View - WC',222,'PAYMENT_METHOD','Payment Method','Payment Method','','','default','','15','N','','','','','','','','SA059956','N','N','','EIS_XXWC_AR_CUST_RECON_RECPT_V','','');
xxeis.eis_rs_ins.rc( 'Customer Account Recon - Receipt View - WC',222,'PAYMENT_TERMS','Payment Terms','Payment Terms','','','default','','26','N','','','','','','','','SA059956','N','N','','EIS_XXWC_AR_CUST_RECON_RECPT_V','','');
xxeis.eis_rs_ins.rc( 'Customer Account Recon - Receipt View - WC',222,'PHONE_NUMBER','Phone Number','Phone Number','','','default','','23','N','','','','','','','','SA059956','N','N','','EIS_XXWC_AR_CUST_RECON_RECPT_V','','');
xxeis.eis_rs_ins.rc( 'Customer Account Recon - Receipt View - WC',222,'PREPAYMENT_AMOUNT','Prepayment Amount','Prepayment Amount','','~T~D~2','default','','18','N','','','','','','','','SA059956','N','N','','EIS_XXWC_AR_CUST_RECON_RECPT_V','','');
xxeis.eis_rs_ins.rc( 'Customer Account Recon - Receipt View - WC',222,'RECEIPT_AMOUNT','Receipt Amount','Receipt Amount','','~T~D~2','default','','16','N','','','','','','','','SA059956','N','N','','EIS_XXWC_AR_CUST_RECON_RECPT_V','','');
xxeis.eis_rs_ins.rc( 'Customer Account Recon - Receipt View - WC',222,'RECEIPT_COMMENTS','Receipt Comments','Receipt Comments','','','default','','13','N','','','','','','','','SA059956','N','N','','EIS_XXWC_AR_CUST_RECON_RECPT_V','','');
xxeis.eis_rs_ins.rc( 'Customer Account Recon - Receipt View - WC',222,'RECEIPT_DATE','Receipt Date','Receipt Date','','','default','','4','N','','','','','','','','SA059956','N','N','','EIS_XXWC_AR_CUST_RECON_RECPT_V','','');
xxeis.eis_rs_ins.rc( 'Customer Account Recon - Receipt View - WC',222,'RECEIPT_NUMBER','Receipt Number','Receipt Number','','','default','','3','N','','','','','','','','SA059956','N','N','','EIS_XXWC_AR_CUST_RECON_RECPT_V','','');
xxeis.eis_rs_ins.rc( 'Customer Account Recon - Receipt View - WC',222,'ACCOUNT_MANAGER','Account Manager','Account Manager','','','default','','25','N','','','','','','','','SA059956','N','N','','EIS_XXWC_AR_CUST_RECON_RECPT_V','','');
xxeis.eis_rs_ins.rc( 'Customer Account Recon - Receipt View - WC',222,'ACCOUNT_STATUS','Account Status','Account Status','','','default','','27','N','','','','','','','','SA059956','N','N','','EIS_XXWC_AR_CUST_RECON_RECPT_V','','');
xxeis.eis_rs_ins.rc( 'Customer Account Recon - Receipt View - WC',222,'APPLIED_AMOUNT','Applied Amount','Applied Amount','','~T~D~2','default','','8','N','','','','','','','','SA059956','N','N','','EIS_XXWC_AR_CUST_RECON_RECPT_V','','');
xxeis.eis_rs_ins.rc( 'Customer Account Recon - Receipt View - WC',222,'APPLIED_DATE','Applied Date','Applied Date','','','default','','7','N','','','','','','','','SA059956','N','N','','EIS_XXWC_AR_CUST_RECON_RECPT_V','','');
xxeis.eis_rs_ins.rc( 'Customer Account Recon - Receipt View - WC',222,'CREDIT_MANAGER','Credit Manager','Credit Manager','','','default','','24','N','','','','','','','','SA059956','N','N','','EIS_XXWC_AR_CUST_RECON_RECPT_V','','');
xxeis.eis_rs_ins.rc( 'Customer Account Recon - Receipt View - WC',222,'CUSTOMER_NAME','Customer Name','Customer Name','','','default','','22','N','','','','','','','','SA059956','N','N','','EIS_XXWC_AR_CUST_RECON_RECPT_V','','');
xxeis.eis_rs_ins.rc( 'Customer Account Recon - Receipt View - WC',222,'CUSTOMER_NUMBER','Customer Number','Customer Number','','','default','','21','N','','','','','','','','SA059956','N','N','','EIS_XXWC_AR_CUST_RECON_RECPT_V','','');
xxeis.eis_rs_ins.rc( 'Customer Account Recon - Receipt View - WC',222,'TRX_AMOUNT_DUE_ORIGINAL','Transaction Amount Due Original','Trx Amount Due Original','','~T~D~2','default','','12','N','','','','','','','','SA059956','N','N','','EIS_XXWC_AR_CUST_RECON_RECPT_V','','');
xxeis.eis_rs_ins.rc( 'Customer Account Recon - Receipt View - WC',222,'TRX_DATE','Transaction Date','Trx Date','','','default','','6','N','','','','','','','','SA059956','N','N','','EIS_XXWC_AR_CUST_RECON_RECPT_V','','');
xxeis.eis_rs_ins.rc( 'Customer Account Recon - Receipt View - WC',222,'UNAPPLIED_AMOUNT','Unapplied Amount','Unapplied Amount','','~T~D~2','default','','17','N','','','','','','','','SA059956','N','N','','EIS_XXWC_AR_CUST_RECON_RECPT_V','','');
xxeis.eis_rs_ins.rc( 'Customer Account Recon - Receipt View - WC',222,'UNEARNED_DISCOUNT','Unearned Discount','Unearned Discount','','~T~D~2','default','','10','N','','','','','','','','SA059956','N','N','','EIS_XXWC_AR_CUST_RECON_RECPT_V','','');
xxeis.eis_rs_ins.rc( 'Customer Account Recon - Receipt View - WC',222,'STATE','State','State','','','default','','20','N','','','','','','','','SA059956','N','N','','EIS_XXWC_AR_CUST_RECON_RECPT_V','','');
xxeis.eis_rs_ins.rc( 'Customer Account Recon - Receipt View - WC',222,'TRANSACTION_NUMBER','Transaction Number','Transaction Number','','','default','','5','N','','','','','','','','SA059956','N','N','','EIS_XXWC_AR_CUST_RECON_RECPT_V','','');
xxeis.eis_rs_ins.rc( 'Customer Account Recon - Receipt View - WC',222,'TRANSACTION_BALANCE_DUE','Transaction Balance Due','Transaction Balance Due','','~T~D~2','default','','11','N','','','','','','','','SA059956','N','N','','EIS_XXWC_AR_CUST_RECON_RECPT_V','','');
--Inserting Report Parameters - Customer Account Recon - Receipt View - WC
xxeis.eis_rs_ins.rp( 'Customer Account Recon - Receipt View - WC',222,'Customer Account Number','Customer Account Number','','IN','XXWC Customer Number LOV','','VARCHAR2','Y','Y','1','','N','CONSTANT','SA059956','Y','N','','','');
xxeis.eis_rs_ins.rp( 'Customer Account Recon - Receipt View - WC',222,'Customer Account Name','Customer Account Name','','IN','XXWC Customer Name LOV','','VARCHAR2','N','Y','2','','N','CONSTANT','SA059956','Y','N','','','');
xxeis.eis_rs_ins.rp( 'Customer Account Recon - Receipt View - WC',222,'Start Date','Start Date','','>=','','select add_months(sysdate,-3) from dual','DATE','Y','Y','3','','N','SQL','SA059956','Y','N','','','');
xxeis.eis_rs_ins.rp( 'Customer Account Recon - Receipt View - WC',222,'End Date','End Date','','<=','','','DATE','N','Y','4','','N','CONSTANT','SA059956','Y','N','','','');
xxeis.eis_rs_ins.rp( 'Customer Account Recon - Receipt View - WC',222,'Receipt Number','Receipt Number','','IN','RECEIPT NUMBER','','VARCHAR2','N','Y','5','','N','CONSTANT','SA059956','Y','N','','','');
--Inserting Report Conditions - Customer Account Recon - Receipt View - WC
--Inserting Report Sorts - Customer Account Recon - Receipt View - WC
xxeis.eis_rs_ins.rs( 'Customer Account Recon - Receipt View - WC',222,'RECEIPT_NUMBER','ASC','SA059956','','1');
xxeis.eis_rs_ins.rs( 'Customer Account Recon - Receipt View - WC',222,'TRANSACTION_NUMBER','ASC','SA059956','','');
--Inserting Report Triggers - Customer Account Recon - Receipt View - WC
xxeis.eis_rs_ins.rt( 'Customer Account Recon - Receipt View - WC',222,'BEGIN
xxeis.EIS_XXWC_CUST_ACC_RECON_PKG.cust_acc_receipt_proc(
      P_CUST_ACC_NUMBER => :Customer Account Number,
      P_CUST_ACC_NAME   => :Customer Account Name,
      P_START_DATE      => :Start Date,
      P_END_DATE        => :End Date,
      p_Receipt_Number  => :Receipt Number);
END;','B','Y','SA059956');
--Inserting Report Templates - Customer Account Recon - Receipt View - WC
--Inserting Report Portals - Customer Account Recon - Receipt View - WC
--Inserting Report Dashboards - Customer Account Recon - Receipt View - WC
--Inserting Report Security - Customer Account Recon - Receipt View - WC
xxeis.eis_rs_ins.rsec( 'Customer Account Recon - Receipt View - WC','222','','50993',222,'SA059956','','');
xxeis.eis_rs_ins.rsec( 'Customer Account Recon - Receipt View - WC','222','','50854',222,'SA059956','','');
xxeis.eis_rs_ins.rsec( 'Customer Account Recon - Receipt View - WC','222','','50853',222,'SA059956','','');
xxeis.eis_rs_ins.rsec( 'Customer Account Recon - Receipt View - WC','222','','50878',222,'SA059956','','');
--Inserting Report Pivots - Customer Account Recon - Receipt View - WC
END;
/
set scan on define on
