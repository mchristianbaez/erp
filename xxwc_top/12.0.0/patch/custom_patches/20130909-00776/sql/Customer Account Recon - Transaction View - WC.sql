--Report Name            : Customer Account Recon - Transaction View - WC
--Import Instructions    : Run this file in XXEIS user from SQL*Plus or any tool like Toad, SQL Navigator. 
prompt Creating View Data for Customer Account Recon - Transaction View - WC
set scan off define off
DECLARE
BEGIN 
--Inserting View EIS_XXWC_AR_CUST_RECON_TRX_V
xxeis.eis_rs_ins.v( 'EIS_XXWC_AR_CUST_RECON_TRX_V',222,'','','','','SA059956','XXEIS','EIS XXWC Ar Cust Acc Recon V','EXACARV','','');
--Delete View Columns for EIS_XXWC_AR_CUST_RECON_TRX_V
xxeis.eis_rs_utility.delete_view_rows('EIS_XXWC_AR_CUST_RECON_TRX_V',222,FALSE);
--Inserting View Columns for EIS_XXWC_AR_CUST_RECON_TRX_V
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_CUST_RECON_TRX_V','REFERENCE',222,'Reference','REFERENCE','','','','SA059956','VARCHAR2','','','Reference','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_CUST_RECON_TRX_V','CREATED_BY',222,'Created By','CREATED_BY','','','','SA059956','VARCHAR2','','','Created By','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_CUST_RECON_TRX_V','ACTIVITY_DATE',222,'Activity Date','ACTIVITY_DATE','','','','SA059956','DATE','','','Activity Date','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_CUST_RECON_TRX_V','STATE',222,'State','STATE','','','','SA059956','VARCHAR2','','','State','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_CUST_RECON_TRX_V','APPLIED_AMOUNT',222,'Applied Amount','APPLIED_AMOUNT','','','','SA059956','NUMBER','','','Applied Amount','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_CUST_RECON_TRX_V','APPLIED_TRX_NUMBER',222,'Applied Trx Number','APPLIED_TRX_NUMBER','','','','SA059956','VARCHAR2','','','Applied Trx Number','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_CUST_RECON_TRX_V','PAYMENT_METHOD',222,'Payment Method','PAYMENT_METHOD','','','','SA059956','VARCHAR2','','','Payment Method','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_CUST_RECON_TRX_V','APPLIED_TRX_TYPE',222,'Applied Trx Type','APPLIED_TRX_TYPE','','','','SA059956','VARCHAR2','','','Applied Trx Type','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_CUST_RECON_TRX_V','TRX_PAYMENT_TERMS',222,'Trx Payment Terms','TRX_PAYMENT_TERMS','','','','SA059956','VARCHAR2','','','Trx Payment Terms','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_CUST_RECON_TRX_V','DAYS_LATE',222,'Days Late','DAYS_LATE','','','','SA059956','NUMBER','','','Days Late','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_CUST_RECON_TRX_V','EARNED_DISCOUNT',222,'Earned Discount','EARNED_DISCOUNT','','','','SA059956','NUMBER','','','Earned Discount','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_CUST_RECON_TRX_V','TAX_BALANCE_DUE',222,'Tax Balance Due','TAX_BALANCE_DUE','','','','SA059956','NUMBER','','','Tax Balance Due','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_CUST_RECON_TRX_V','TRANSACTION_BALANCE_DUE',222,'Transaction Balance Due','TRANSACTION_BALANCE_DUE','','','','SA059956','NUMBER','','','Transaction Balance Due','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_CUST_RECON_TRX_V','TRX_AMOUNT_DUE_ORIGINAL',222,'Trx Amount Due Original','TRX_AMOUNT_DUE_ORIGINAL','','','','SA059956','NUMBER','','','Trx Amount Due Original','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_CUST_RECON_TRX_V','PO_NUMBER',222,'Po Number','PO_NUMBER','','','','SA059956','VARCHAR2','','','Po Number','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_CUST_RECON_TRX_V','TRX_DATE',222,'Trx Date','TRX_DATE','','','','SA059956','DATE','','','Trx Date','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_CUST_RECON_TRX_V','TRANSACTION_NUMBER',222,'Transaction Number','TRANSACTION_NUMBER','','','','SA059956','VARCHAR2','','','Transaction Number','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_CUST_RECON_TRX_V','LOCATION',222,'Location','LOCATION','','','','SA059956','VARCHAR2','','','Location','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_CUST_RECON_TRX_V','PARTY_SITE_NUMBER',222,'Party Site Number','PARTY_SITE_NUMBER','','','','SA059956','VARCHAR2','','','Party Site Number','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_CUST_RECON_TRX_V','ACCOUNT_STATUS',222,'Account Status','ACCOUNT_STATUS','','','','SA059956','VARCHAR2','','','Account Status','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_CUST_RECON_TRX_V','PAYMENT_TERMS',222,'Payment Terms','PAYMENT_TERMS','','','','SA059956','VARCHAR2','','','Payment Terms','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_CUST_RECON_TRX_V','ACCOUNT_MANAGER',222,'Account Manager','ACCOUNT_MANAGER','','','','SA059956','VARCHAR2','','','Account Manager','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_CUST_RECON_TRX_V','CREDIT_MANAGER',222,'Credit Manager','CREDIT_MANAGER','','','','SA059956','VARCHAR2','','','Credit Manager','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_CUST_RECON_TRX_V','PHONE_NUMBER',222,'Phone Number','PHONE_NUMBER','','','','SA059956','VARCHAR2','','','Phone Number','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_CUST_RECON_TRX_V','CUSTOMER_NUMBER',222,'Customer Number','CUSTOMER_NUMBER','','','','SA059956','VARCHAR2','','','Customer Number','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_CUST_RECON_TRX_V','CUSTOMER_NAME',222,'Customer Name','CUSTOMER_NAME','','','','SA059956','VARCHAR2','','','Customer Name','','','');
--Inserting View Components for EIS_XXWC_AR_CUST_RECON_TRX_V
--Inserting View Component Joins for EIS_XXWC_AR_CUST_RECON_TRX_V
END;
/
set scan on define on
prompt Creating Report LOV Data for Customer Account Recon - Transaction View - WC
set scan off define off
DECLARE
BEGIN 
--Inserting Report LOVs - Customer Account Recon - Transaction View - WC
xxeis.eis_rs_ins.lov( 222,'SELECT NVL(hca.account_name, hp.party_name) customer_name,hca.account_number customer_number
FROM hz_cust_accounts hca,
  hz_parties hp
where hca.party_id=hp.party_id
ORDER BY 1','','XXWC Customer Name LOV','Display List of Values for Customer Name','ANONYMOUS',NULL,'N','','');
xxeis.eis_rs_ins.lov( 222,'SELECT hca.account_number customer_number,NVL(hca.account_name, hp.party_name) customer_name
FROM hz_cust_accounts hca,
  hz_parties hp
where hca.party_id=hp.party_id
ORDER BY lpad(hca.account_number,10)','','XXWC Customer Number LOV','Displays List of Values for Customer Number','ANONYMOUS',NULL,'N','','');
xxeis.eis_rs_ins.lov( 222,'select distinct /*+ INDEX(rct RA_CUSTOMER_TRX_N1)*/ rct.trx_number
FROM ra_customer_trx rct
ORDER BY 1','','XXWC Transaction Numbers LOV','Displays List of Transaction Numbers','ANONYMOUS',NULL,'N','','');
END;
/
set scan on define on
prompt Creating Report Data for Customer Account Recon - Transaction View - WC
set scan off define off
DECLARE
BEGIN 
--Deleting Report data - Customer Account Recon - Transaction View - WC
xxeis.eis_rs_utility.delete_report_rows( 'Customer Account Recon - Transaction View - WC' );
--Inserting Report - Customer Account Recon - Transaction View - WC
xxeis.eis_rs_ins.r( 222,'Customer Account Recon - Transaction View - WC','','The report will provide transaction activity for a specific customer within a specific required time frame.','','','','SA059956','EIS_XXWC_AR_CUST_RECON_TRX_V','Y','','','SA059956','','N','White Cap Reports','','CSV,EXCEL,','N');
--Inserting Report Columns - Customer Account Recon - Transaction View - WC
xxeis.eis_rs_ins.rc( 'Customer Account Recon - Transaction View - WC',222,'EARNED_DISCOUNT','Earned Discount','Earned Discount','','~T~D~2','default','','9','N','','','','','','','','SA059956','N','N','','EIS_XXWC_AR_CUST_RECON_TRX_V','','');
xxeis.eis_rs_ins.rc( 'Customer Account Recon - Transaction View - WC',222,'LOCATION','Location','Location','','','default','','2','N','','','','','','','','SA059956','N','N','','EIS_XXWC_AR_CUST_RECON_TRX_V','','');
xxeis.eis_rs_ins.rc( 'Customer Account Recon - Transaction View - WC',222,'PARTY_SITE_NUMBER','Party Site Number','Party Site Number','','','default','','1','N','','','','','','','','SA059956','N','N','','EIS_XXWC_AR_CUST_RECON_TRX_V','','');
xxeis.eis_rs_ins.rc( 'Customer Account Recon - Transaction View - WC',222,'PAYMENT_METHOD','Payment Method','Payment Method','','','default','','11','N','','','','','','','','SA059956','N','N','','EIS_XXWC_AR_CUST_RECON_TRX_V','','');
xxeis.eis_rs_ins.rc( 'Customer Account Recon - Transaction View - WC',222,'PAYMENT_TERMS','Payment Terms','Payment Terms','','','default','','25','N','','','','','','','','SA059956','N','N','','EIS_XXWC_AR_CUST_RECON_TRX_V','','');
xxeis.eis_rs_ins.rc( 'Customer Account Recon - Transaction View - WC',222,'PHONE_NUMBER','Phone Number','Phone Number','','','default','','22','N','','','','','','','','SA059956','N','N','','EIS_XXWC_AR_CUST_RECON_TRX_V','','');
xxeis.eis_rs_ins.rc( 'Customer Account Recon - Transaction View - WC',222,'PO_NUMBER','PO Number','Po Number','','','default','','5','N','','','','','','','','SA059956','N','N','','EIS_XXWC_AR_CUST_RECON_TRX_V','','');
xxeis.eis_rs_ins.rc( 'Customer Account Recon - Transaction View - WC',222,'REFERENCE','Reference','Reference','','','default','','19','N','','','','','','','','SA059956','N','N','','EIS_XXWC_AR_CUST_RECON_TRX_V','','');
xxeis.eis_rs_ins.rc( 'Customer Account Recon - Transaction View - WC',222,'ACCOUNT_MANAGER','Account Manager','Account Manager','','','default','','24','N','','','','','','','','SA059956','N','N','','EIS_XXWC_AR_CUST_RECON_TRX_V','','');
xxeis.eis_rs_ins.rc( 'Customer Account Recon - Transaction View - WC',222,'ACCOUNT_STATUS','Account Status','Account Status','','','default','','26','N','','','','','','','','SA059956','N','N','','EIS_XXWC_AR_CUST_RECON_TRX_V','','');
xxeis.eis_rs_ins.rc( 'Customer Account Recon - Transaction View - WC',222,'ACTIVITY_DATE','Activity Date','Activity Date','','','default','','14','N','','','','','','','','SA059956','N','N','','EIS_XXWC_AR_CUST_RECON_TRX_V','','');
xxeis.eis_rs_ins.rc( 'Customer Account Recon - Transaction View - WC',222,'APPLIED_AMOUNT','Applied Amount','Applied Amount','','~T~D~2','default','','13','N','','','','','','','','SA059956','N','N','','EIS_XXWC_AR_CUST_RECON_TRX_V','','');
xxeis.eis_rs_ins.rc( 'Customer Account Recon - Transaction View - WC',222,'APPLIED_TRX_NUMBER','Applied Transaction Number','Applied Trx Number','','','default','','12','N','','','','','','','','SA059956','N','N','','EIS_XXWC_AR_CUST_RECON_TRX_V','','');
xxeis.eis_rs_ins.rc( 'Customer Account Recon - Transaction View - WC',222,'APPLIED_TRX_TYPE','Applied Transaction Type','Applied Trx Type','','','default','','10','N','','','','','','','','SA059956','N','N','','EIS_XXWC_AR_CUST_RECON_TRX_V','','');
xxeis.eis_rs_ins.rc( 'Customer Account Recon - Transaction View - WC',222,'CREATED_BY','Created By','Created By','','','default','','15','N','','','','','','','','SA059956','N','N','','EIS_XXWC_AR_CUST_RECON_TRX_V','','');
xxeis.eis_rs_ins.rc( 'Customer Account Recon - Transaction View - WC',222,'CREDIT_MANAGER','Credit Manager','Credit Manager','','','default','','23','N','','','','','','','','SA059956','N','N','','EIS_XXWC_AR_CUST_RECON_TRX_V','','');
xxeis.eis_rs_ins.rc( 'Customer Account Recon - Transaction View - WC',222,'CUSTOMER_NAME','Customer Name','Customer Name','','','default','','21','N','','','','','','','','SA059956','N','N','','EIS_XXWC_AR_CUST_RECON_TRX_V','','');
xxeis.eis_rs_ins.rc( 'Customer Account Recon - Transaction View - WC',222,'CUSTOMER_NUMBER','Customer Number','Customer Number','','','default','','20','N','','','','','','','','SA059956','N','N','','EIS_XXWC_AR_CUST_RECON_TRX_V','','');
xxeis.eis_rs_ins.rc( 'Customer Account Recon - Transaction View - WC',222,'DAYS_LATE','Days Late','Days Late','','~~~','default','','16','N','','','','','','','','SA059956','N','N','','EIS_XXWC_AR_CUST_RECON_TRX_V','','');
xxeis.eis_rs_ins.rc( 'Customer Account Recon - Transaction View - WC',222,'TRX_AMOUNT_DUE_ORIGINAL','Transaction Amount Due Original','Trx Amount Due Original','','~T~D~2','default','','6','N','','','','','','','','SA059956','N','N','','EIS_XXWC_AR_CUST_RECON_TRX_V','','');
xxeis.eis_rs_ins.rc( 'Customer Account Recon - Transaction View - WC',222,'TRX_DATE','Transaction Date','Trx Date','','','default','','4','N','','','','','','','','SA059956','N','N','','EIS_XXWC_AR_CUST_RECON_TRX_V','','');
xxeis.eis_rs_ins.rc( 'Customer Account Recon - Transaction View - WC',222,'TRX_PAYMENT_TERMS','Transaction Payment Terms','Trx Payment Terms','','','default','','17','N','','','','','','','','SA059956','N','N','','EIS_XXWC_AR_CUST_RECON_TRX_V','','');
xxeis.eis_rs_ins.rc( 'Customer Account Recon - Transaction View - WC',222,'STATE','State','State','','','default','','18','N','','','','','','','','SA059956','N','N','','EIS_XXWC_AR_CUST_RECON_TRX_V','','');
xxeis.eis_rs_ins.rc( 'Customer Account Recon - Transaction View - WC',222,'TAX_BALANCE_DUE','Tax Balance Due','Tax Balance Due','','~T~D~2','default','','8','N','','','','','','','','SA059956','N','N','','EIS_XXWC_AR_CUST_RECON_TRX_V','','');
xxeis.eis_rs_ins.rc( 'Customer Account Recon - Transaction View - WC',222,'TRANSACTION_BALANCE_DUE','Transaction Balance Due','Transaction Balance Due','','~T~D~2','default','','7','N','','','','','','','','SA059956','N','N','','EIS_XXWC_AR_CUST_RECON_TRX_V','','');
xxeis.eis_rs_ins.rc( 'Customer Account Recon - Transaction View - WC',222,'TRANSACTION_NUMBER','Transaction Number','Transaction Number','','','default','','3','N','','','','','','','','SA059956','N','N','','EIS_XXWC_AR_CUST_RECON_TRX_V','','');
--Inserting Report Parameters - Customer Account Recon - Transaction View - WC
xxeis.eis_rs_ins.rp( 'Customer Account Recon - Transaction View - WC',222,'Customer Account Number','Customer Account Number','','IN','XXWC Customer Number LOV','','VARCHAR2','Y','Y','1','','N','CONSTANT','SA059956','Y','N','','','');
xxeis.eis_rs_ins.rp( 'Customer Account Recon - Transaction View - WC',222,'Customer Account Name','Customer Account Name','','IN','XXWC Customer Name LOV','','VARCHAR2','N','Y','2','','N','CONSTANT','SA059956','Y','N','','','');
xxeis.eis_rs_ins.rp( 'Customer Account Recon - Transaction View - WC',222,'Start Date','Start Date','','>=','','select add_months(sysdate,-3) from dual','DATE','Y','Y','3','','N','SQL','SA059956','Y','N','','','');
xxeis.eis_rs_ins.rp( 'Customer Account Recon - Transaction View - WC',222,'End Date','End Date','','<=','','','DATE','N','Y','4','','N','CONSTANT','SA059956','Y','N','','','');
xxeis.eis_rs_ins.rp( 'Customer Account Recon - Transaction View - WC',222,'Transaction Number','Transaction Number','','IN','XXWC Transaction Numbers LOV','','VARCHAR2','N','Y','5','','N','CONSTANT','SA059956','Y','N','','','');
--Inserting Report Conditions - Customer Account Recon - Transaction View - WC
--Inserting Report Sorts - Customer Account Recon - Transaction View - WC
xxeis.eis_rs_ins.rs( 'Customer Account Recon - Transaction View - WC',222,'TRANSACTION_NUMBER','ASC','SA059956','1','');
--Inserting Report Triggers - Customer Account Recon - Transaction View - WC
xxeis.eis_rs_ins.rt( 'Customer Account Recon - Transaction View - WC',222,'BEGIN
xxeis.EIS_XXWC_CUST_ACC_RECON_PKG.cust_acc_trx_proc(
      P_CUST_ACC_NUMBER => :Customer Account Number,
      P_CUST_ACC_NAME   => :Customer Account Name,
      P_START_DATE      => :Start Date,
      P_END_DATE        => :End Date,
      P_TRX_NUMBER      => :Transaction Number);
END;','B','Y','SA059956');
--Inserting Report Templates - Customer Account Recon - Transaction View - WC
--Inserting Report Portals - Customer Account Recon - Transaction View - WC
--Inserting Report Dashboards - Customer Account Recon - Transaction View - WC
--Inserting Report Security - Customer Account Recon - Transaction View - WC
xxeis.eis_rs_ins.rsec( 'Customer Account Recon - Transaction View - WC','222','','50993',222,'SA059956','','');
xxeis.eis_rs_ins.rsec( 'Customer Account Recon - Transaction View - WC','222','','50854',222,'SA059956','','');
xxeis.eis_rs_ins.rsec( 'Customer Account Recon - Transaction View - WC','222','','50853',222,'SA059956','','');
xxeis.eis_rs_ins.rsec( 'Customer Account Recon - Transaction View - WC','222','','50879',222,'SA059956','','');
xxeis.eis_rs_ins.rsec( 'Customer Account Recon - Transaction View - WC','222','','50878',222,'SA059956','','');
--Inserting Report Pivots - Customer Account Recon - Transaction View - WC
END;
/
set scan on define on
