CREATE OR REPLACE PACKAGE   XXEIS.EIS_XXWC_CUST_ACC_RECON_PKG AUTHID CURRENT_USER
  --//============================================================================
  --//
  --// Object Name           :: xxeis.EIS_XXWC_CUST_ACC_RECON_PKG
  --//
  --// Object Type           :: Package Specification
  --//
  --// Object Description    :: This Package will trigger in before report and insert the values into Temp Table
  --//
  --// Version Control
  --//============================================================================
  --// Vers     Author           Date          Description
  --//----------------------------------------------------------------------------
  --// 1.0      07-Sep-2016        Siva      TMS#20130909-00776
  --//============================================================================
is
  procedure cust_acc_receipt_proc(
      P_CUST_ACC_NUMBER IN VARCHAR2,
      P_CUST_ACC_NAME   IN VARCHAR2,
      P_START_DATE      IN DATE,
      P_END_DATE        IN DATE,
      p_Receipt_Number  IN VARCHAR2 );
  --//============================================================================
  --//
  --// Object Name         :: cust_acc_receipt_proc
  --//
  --// Object Type         :: Procedure
  --//
  --// Object Description  :: This procedure gets the record based on parameter and insert into temp table
  --//
  --// Version Control
  --//============================================================================
  --// Vers    Author             Date            Description
  --//----------------------------------------------------------------------------
  --// 1.0      07-Sep-2016        Siva      TMS#20130909-00776
  --//============================================================================
  procedure cust_acc_trx_proc(
      P_CUST_ACC_NUMBER IN VARCHAR2,
      P_CUST_ACC_NAME   IN VARCHAR2,
      P_START_DATE      IN DATE,
      P_END_DATE        IN DATE,
      P_TRX_NUMBER      IN VARCHAR2 );
  --//============================================================================
  --//
  --// Object Name         :: cust_acc_trx_proc
  --//
  --// Object Type         :: Procedure
  --//
  --// Object Description  :: This procedure gets the record based on parameter and insert into temp table
  --//
  --// Version Control
  --//============================================================================
  --// Vers    Author             Date            Description
  --//----------------------------------------------------------------------------
  --// 1.0      07-Sep-2016        Siva      TMS#20130909-00776
  --//============================================================================ 

END EIS_XXWC_CUST_ACC_RECON_PKG;
/
