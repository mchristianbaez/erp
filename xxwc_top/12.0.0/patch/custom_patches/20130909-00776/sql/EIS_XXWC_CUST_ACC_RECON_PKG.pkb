CREATE OR REPLACE PACKAGE BODY   XXEIS.EIS_XXWC_CUST_ACC_RECON_PKG 
  --//============================================================================
  --//
  --// Object Name           :: xxeis.EIS_XXWC_CUST_ACC_RECON_PKG
  --//
  --// Object Type           :: Package Specification
  --//
  --// Object Description    :: This Package will trigger in before report and insert the values into Temp Table
  --//
  --// Version Control
  --//============================================================================
  --// Vers     Author           Date          Description
  --//----------------------------------------------------------------------------
  --// 1.0      07-Sep-2016        Siva      TMS#20130909-00776
  --//============================================================================
IS
procedure cust_acc_receipt_proc(
      P_CUST_ACC_NUMBER IN VARCHAR2,
      P_CUST_ACC_NAME   IN VARCHAR2,
      P_START_DATE      IN DATE,
      P_END_DATE        IN DATE,
      p_Receipt_Number  IN VARCHAR2 )
  --//============================================================================
  --//
  --// Object Name         :: cust_acc_receipt_proc
  --//
  --// Object Type         :: Procedure
  --//
  --// Object Description  :: This procedure gets the record based on parameter and insert into temp table
  --//
  --// Version Control
  --//============================================================================
  --// Vers    Author             Date            Description
  --//----------------------------------------------------------------------------
  --// 1.0      07-Sep-2016        Siva      TMS#20130909-00776
  --//============================================================================
AS
 L_CUST_ACC_RECPT_CSR SYS_REFCURSOR;
 lc_where_cond varchar2(32000);
  
    type cust_acc_receipt_type
is
  table of xxeis.xxwc_cust_rec_receipt_tmp_tbl%rowtype index by binary_integer;
  Cust_acc_receipt_tab cust_acc_receipt_type;
  
L_RECEIPT_MAIN_QRY VARCHAR2(32000):='SELECT NVL(hca.account_name, HZP.party_name) customer_name,
  HCA.ACCOUNT_NUMBER CUSTOMER_NUMBER,
  (DECODE(hcp.Phone_number,NULL,NULL, ''(''
  ||hcp.Phone_area_code
  ||'') ''
  ||hcp.Phone_number)
  ||DECODE(HCP.PHONE_EXTENSION,NULL,NULL, '' EX ''
  ||NVL(HCP.PHONE_EXTENSION,''-''))) PHONE_NUMBER,
  AC.NAME CREDIT_MANAGER,
  (SELECT res.resource_name Account_Manager
  FROM APPS.JTF_RS_SALESREPS SR,
    apps.JTF_RS_RESOURCE_EXTNS_VL RES
  WHERE SR.SALESREP_ID = HCSU.PRIMARY_SALESREP_ID
  AND HCSU.ORG_ID      = SR.ORG_ID
  AND SR.RESOURCE_ID   = RES.RESOURCE_ID
  ) Account_Manager,
  rt.name payment_terms ,
  arl.meaning account_status,
  NVL(aps_party_siteb.party_site_number, party_siteb.party_site_number) party_site_number,
  NVL(aps_HCSU.location,hcsu.location) location,
  CR.RECEIPT_NUMBER,
  Cr.Receipt_Date Receipt_Date,
  ct.trx_date,
  CT.trx_number,  
  APS.AMOUNT_DUE_ORIGINAL TRX_AMOUNT_DUE_ORIGINAL,
  APS.AMOUNT_DUE_REMAINING TRANSACTION_BALANCE_DUE ,
  NVL(APP.AMOUNT_APPLIED,NVL(-(ps.amount_applied),0)) applied_amount ,
  arls.meaning state,
  NVL(App.Earned_Discount_Taken,0) EARNED_DISCOUNT,
  NVL(APP.UNEARNED_DISCOUNT_TAKEN,0) Unearned_Discount,
  cr.deposit_date,
  App.Apply_Date Applied_Date,
  rec_method.name payment_method,
  Cr.Comments Receipt_Comments,
  aPs.Attribute1 Discrepancy_Code,
  cr.amount receipt_amount,
  (SELECT NVL(SUM (DECODE (arap.status, ''UNAPP'',NVL(arap.amount_applied,0) ,''UNID'', NVL(arap.amount_applied,0) )),0) unapplied_amt
  FROM Apps.Ar_Receivable_Applications arap
  WHERE arap.Cash_Receipt_Id           = cr.Cash_Receipt_Id
  AND NVL (Arap.Confirmed_Flag, ''Y'') = ''Y''
  AND Arap.Application_Type            = ''CASH''
  AND arap.Status                   IN (''UNAPP'',''UNID'')
  ) UNAPPLIED_AMOUNT ,
  0 Prepayment_Amount,
  (SELECT NVL(SUM(NVL(arap.AMOUNT_APPLIED,0)),0) On_Account_Amt
  FROM Apps.Ar_Receivable_Applications arap
  WHERE arap.Cash_Receipt_Id           = cr.Cash_Receipt_Id
  AND NVL (Arap.Confirmed_Flag, ''Y'') = ''Y''
  AND Arap.Application_Type            = ''CASH''
  AND Arap.Status                      = ''ACC''
  ) On_Account_Amount
FROM Apps.Ar_Cash_Receipts Cr,
  (SELECT *
  FROM Apps.Ar_Receivable_Applications
  WHERE Display        = ''Y''
  AND Application_Type = ''CASH''
  AND Status           = ''APP''
  ) App,
  Apps.Ar_Payment_Schedules Aps,
  apps.RA_CUSTOMER_TRX CT ,
  APPS.HZ_CUST_SITE_USES_ALL aps_HCSU,
  APPS.HZ_CUST_ACCT_SITES aps_CUST_SITEB ,
  APPS.hz_party_sites aps_party_siteb ,
  APPS.HZ_CUST_ACCOUNTS HCA,
  APPS.HZ_CUST_SITE_USES_ALL HCSU,
  APPS.HZ_CUST_ACCT_SITES CUST_SITEB ,
  APPS.hz_party_sites party_siteb ,
  apps.ar_receipt_methods rec_method,
  apps.ar_payment_schedules_all ps,
  apps.ar_cash_receipt_history_all crh_current,
  apps.Ar_Lookups arls,
  APPS.HZ_PARTIES HZP,
  APPS.HZ_CONTACT_POINTS HCP,
  APPS.HZ_CUSTOMER_PROFILES HZCP,
  APPS.AR_COLLECTORS AC,
  Apps.Ra_Terms Rt,
  Apps.Ar_Lookups Arl
WHERE 1                             =1
AND Cr.Type                         = ''CASH''
AND Cr.Cash_Receipt_Id              = App.Cash_Receipt_Id(+)
AND App.Applied_Payment_Schedule_Id = Aps.Payment_Schedule_Id(+)
AND Aps.CUSTOMER_TRX_ID             = CT.CUSTOMER_TRX_ID(+)
AND Aps.CUSTOMER_SITE_USE_ID        = aps_HCSU.SITE_USE_ID(+)
AND APS.ORG_ID                      = aps_HCSU.ORG_ID(+)
AND aps_HCSU.CUST_ACCT_SITE_ID      = aps_CUST_SITEB.CUST_ACCT_SITE_ID(+)
AND aps_CUST_SITEB.party_site_id    = aps_party_siteb.party_site_id(+)
AND cr.pay_from_customer            = hca.cust_account_id(+)
AND cr.customer_site_use_id         = hcsu.site_use_id(+)
AND cr.ORG_ID                       = HCSU.ORG_ID(+)
AND hcsu.cust_acct_site_id          = cust_siteb.cust_acct_site_id(+)
AND cust_siteb.party_site_id        = party_siteb.party_site_id(+)
AND cr.receipt_method_id            = rec_method.receipt_method_id(+)
AND cr.cash_receipt_id              = ps.cash_receipt_id(+)
AND cr.org_id                       = ps.org_id(+)
AND cr.cash_receipt_id              = crh_current.cash_receipt_id
AND cr.org_id                       = crh_current.org_id
AND NVL(''Y'', cr.receipt_number)   = crh_current.current_record_flag
AND crh_current.status              = arls.LOOKUP_CODE(+)
AND arls.Lookup_Type(+)             = ''RECEIPT_CREATION_STATUS''
AND HCA.PARTY_ID                    = HZP.PARTY_ID
AND HZP.PARTY_ID                    = HCP.OWNER_TABLE_ID(+)
AND HCP.OWNER_TABLE_NAME(+)         = ''HZ_PARTIES''
AND HCP.CONTACT_POINT_TYPE(+)       = ''PHONE''
AND HCP.PRIMARY_FLAG(+)             = ''Y''
AND HCA.CUST_ACCOUNT_ID             = HZCP.CUST_ACCOUNT_ID
AND HZCP.SITE_USE_ID               IS NULL
AND HZCP.COLLECTOR_ID               = AC.COLLECTOR_ID(+)
AND HZCP.STANDARD_TERMS             = RT.TERM_ID(+)
AND HZCP.ACCOUNT_STATUS             = ARL.LOOKUP_CODE(+)
AND Arl.Lookup_Type(+)              = ''ACCOUNT_STATUS''
';


begin

  IF P_CUST_ACC_NUMBER IS NOT NULL THEN
    lc_where_cond      := lc_where_cond || ' AND HCA.ACCOUNT_NUMBER IN (' || REPLACE (xxeis.eis_rs_utility.get_param_values (p_param_value => trim (P_CUST_ACC_NUMBER ) ), '%%', ', ' ) || ')';
  END IF;
  
  IF P_CUST_ACC_NAME IS NOT NULL THEN
    lc_where_cond      := lc_where_cond || ' AND NVL(hca.account_name, HZP.party_name) IN (' || REPLACE (xxeis.eis_rs_utility.get_param_values (p_param_value => trim (P_CUST_ACC_NAME ) ), '%%', ', ' ) || ')';
  END IF;
  
	if p_start_date is not null then
		lc_where_cond      := lc_where_cond || ' AND trunc(Cr.Receipt_Date) >= (' || REPLACE (xxeis.eis_rs_utility.get_param_values (p_param_value => trim (P_START_DATE ) ), '%%', ', ' ) || ')';
	END IF;
  
	if p_end_date is not null then
		lc_where_cond      := lc_where_cond || ' AND trunc(Cr.Receipt_Date) <= (' || REPLACE (xxeis.eis_rs_utility.get_param_values (p_param_value => trim (P_END_DATE ) ), '%%', ', ' ) || ')';
	END IF;
  
	IF p_Receipt_Number IS NOT NULL THEN
		lc_where_cond      := lc_where_cond || ' AND CR.RECEIPT_NUMBER IN (' || REPLACE (xxeis.eis_rs_utility.get_param_values (p_param_value => trim (p_Receipt_Number ) ), '%%', ', ' ) || ')';
	END IF;  

  
  Cust_acc_receipt_tab.delete;
  
  L_RECEIPT_MAIN_QRY := L_RECEIPT_MAIN_QRY||' '||LC_WHERE_COND;
	fnd_file.put_line(fnd_file.log,'L_RECEIPT_MAIN_QRY'||l_receipt_main_qry);	
  
  OPEN L_CUST_ACC_RECPT_CSR FOR L_RECEIPT_MAIN_QRY ;
  LOOP
	FETCH L_CUST_ACC_RECPT_CSR BULK COLLECT INTO Cust_acc_receipt_tab LIMIT 10000;
	fnd_file.put_line(fnd_file.log,'Cust_acc_receipt_tab'||Cust_acc_receipt_tab.count);
	IF Cust_acc_receipt_tab.count > 0 THEN
		forall j IN 1 .. Cust_acc_receipt_tab.count
		INSERT INTO xxeis.XXWC_CUST_REC_RECEIPT_TMP_TBL VALUES Cust_acc_receipt_tab(j);
		--COMMIT;
	END IF;
    IF L_CUST_ACC_RECPT_CSR%notfound THEN
    CLOSE L_CUST_ACC_RECPT_CSR;
    EXIT;
    end if;
  end loop; 
  
EXCEPTION
WHEN OTHERS THEN
  fnd_file.put_line(fnd_file.log,'CUST_ACC_REC_PROC procedure has the following error'||SQLCODE||'    '||sqlerrm);
END cust_acc_receipt_proc;



  procedure cust_acc_trx_proc(
      P_CUST_ACC_NUMBER IN VARCHAR2,
      P_CUST_ACC_NAME   IN VARCHAR2,
      P_START_DATE      IN DATE,
      P_END_DATE        IN DATE,
      P_TRX_NUMBER      IN VARCHAR2 )
  --//============================================================================
  --//
  --// Object Name         :: cust_acc_trx_proc
  --//
  --// Object Type         :: Procedure
  --//
  --// Object Description  :: This procedure gets the record based on parameter and insert into temp table
  --//
  --// Version Control
  --//============================================================================
  --// Vers    Author             Date            Description
  --//----------------------------------------------------------------------------
  --// 1.0      07-Sep-2016        Siva      TMS#20130909-00776
  --//============================================================================
as
 l_cust_acc_trx_csr SYS_REFCURSOR;
 lc_where_cond varchar2(32000);
  
    type cust_acc_trx_type
is
  table of xxeis.XXWC_CUST_RECON_TRX_TMP_TBL%rowtype index by binary_integer;
  cust_acc_trx_tab cust_acc_trx_type;
  
  L_TRX_MAIN_QRY VARCHAR2(32000):='SELECT NVL(hca.account_name, HZP.party_name) customer_name,
  HCA.ACCOUNT_NUMBER CUSTOMER_NUMBER,
  (DECODE(hcp.Phone_number,NULL,NULL, ''(''
  ||hcp.Phone_area_code
  ||'') ''
  ||hcp.Phone_number)
  ||DECODE(HCP.PHONE_EXTENSION,NULL,NULL, '' EX ''
  ||NVL(HCP.PHONE_EXTENSION,''-''))) PHONE_NUMBER,
  AC.NAME CREDIT_MANAGER,
  (SELECT res.resource_name Account_Manager
  FROM APPS.JTF_RS_SALESREPS SR,
    apps.JTF_RS_RESOURCE_EXTNS_VL RES
  WHERE SR.SALESREP_ID = HCSU.PRIMARY_SALESREP_ID
  AND HCSU.ORG_ID      = SR.ORG_ID
  AND SR.RESOURCE_ID   = RES.RESOURCE_ID
  ) Account_Manager,
  RT.NAME PAYMENT_TERMS ,
  ARL.MEANING ACCOUNT_STATUS ,
  PARTY_SITEB.PARTY_SITE_NUMBER,
  HCSU.LOCATION ,
  RCT.TRX_NUMBER TRANSACTION_NUMBER,
  RCT.TRX_DATE,
  REPLACE (RCT.PURCHASE_ORDER, ''~'', '' '') PO_NUMBER,
  APS.AMOUNT_DUE_ORIGINAL TRX_AMOUNT_DUE_ORIGINAL,
  APS.AMOUNT_DUE_REMAINING TRANSACTION_BALANCE_DUE ,
  APS.TAX_REMAINING TAX_BALANCE_DUE ,
  APS.DISCOUNT_TAKEN_EARNED EARNED_DISCOUNT ,
  DECODE(APS.AMOUNT_DUE_REMAINING, 0, TO_NUMBER(NULL), TRUNC(sysdate) - APS.DUE_DATE) DAYS_LATE,
  TRXRT.name TRX_PAYMENT_TERMS ,
  apav.APPLIED_TRX_TYPE,
  apav.Payment_Method ,
  apav.Applied_trx_Number,
  APAV.APPLIED_AMOUNT,
  APAV.STATE,
  APAV.activity_date,
  APAV.created_by ,
  APAV.REFERENCE
FROM APPS.ar_payment_schedules aps,
  APPS.ra_customer_trx RCT,
  APPS.HZ_CUST_ACCOUNTS HCA,
  APPS.HZ_CUST_SITE_USES_ALL HCSU,
  APPS.HZ_CUST_ACCT_SITES CUST_SITEB ,
  APPS.hz_party_sites party_siteb ,
  APPS.HZ_PARTIES HZP,
  APPS.HZ_CONTACT_POINTS HCP,
  APPS.HZ_CUSTOMER_PROFILES HZCP,
  APPS.AR_COLLECTORS AC,
  APPS.RA_TERMS RT,
  APPS.AR_LOOKUPS ARL,
  APPS.RA_TERMS TRXRT,
  (SELECT app.applied_payment_schedule_id payment_schedule_id,
    apps.arpt_sql_func_util.get_lookup_meaning(''INV/CM'',ps.class) applied_trx_type,
    DECODE(app.application_type, ''CM'', ctt.name, rm.name) payment_method,
    ps.trx_number applied_trx_number,
    -NVL(app.amount_applied_from, app.amount_applied) applied_amount,
    apps.arpt_sql_func_util.get_lookup_meaning(''RECEIPT_CREATION_STATUS'', crh.status) state,
    app.creation_date activity_date,
    fu.user_name created_by,
    app.application_ref_num REFERENCE
  FROM apps.ra_customer_trx ct,
    apps.ra_customer_trx_lines ctl,
    apps.ra_cust_trx_types ctt,
    apps.ar_receipt_methods rm,
    apps.fnd_user fu,
    apps.ar_cash_receipts cr,
    apps.ar_payment_schedules ps,
    apps.ar_receivable_applications app,
    apps.ar_cash_receipt_history crh
  WHERE app.status                     = ''APP''
  AND app.cash_receipt_id              = cr.cash_receipt_id(+)
  AND cr.receipt_method_id             = rm.receipt_method_id(+)
  AND app.payment_schedule_id          = ps.payment_schedule_id
  AND ps.cust_trx_type_id              = ctt.cust_trx_type_id(+)
  AND app.applied_customer_trx_id      = ct.customer_trx_id(+)
  AND app.applied_customer_trx_line_id = ctl.customer_trx_line_id(+)
  AND cr.cash_receipt_id               = crh.cash_receipt_id(+)
  AND crh.current_record_flag(+)       = ''Y''
  AND app.created_by                   = fu.user_id
  AND NVL(ctt.org_id,ps.org_id)        = ps.org_id
  UNION ALL
  SELECT app.payment_schedule_id payment_schedule_id,
    DECODE ( SIGN(ps.payment_schedule_id) , -1 , NULL , apps.arpt_sql_func_util.get_lookup_meaning(''INV/CM'',ps.class)) applied_trx_type,
    ctt.name payment_method,
    DECODE ( SIGN(ps.payment_schedule_id) , -1 , apps.arpt_sql_func_util.get_lookup_meaning(''PAYMENT_TYPE'', app.status) , ps.trx_number ) applied_trx_number,
    NVL(app.amount_applied_from, app.amount_applied) applied_amount,
    TO_CHAR(NULL) state,
    app.creation_date activity_date,
    fu.user_name created_by,
    app.application_ref_num REFERENCE
  FROM apps.ra_customer_trx ct,
    apps.ra_customer_trx_lines rtl,
    apps.ra_cust_trx_types ctt,
    apps.fnd_user fu,
    apps.ar_payment_schedules ps,
    apps.ar_receivable_applications app
  WHERE app.applied_payment_schedule_id = ps.payment_schedule_id
  AND ps.cust_trx_type_id               = ctt.cust_trx_type_id(+)
  AND app.applied_customer_trx_line_id  = rtl.customer_trx_line_id(+)
  AND app.status NOT                   IN (''ACC'',''ACTIVITY'',''OTHER ACC'')
  AND app.applied_customer_trx_id       = ct.customer_trx_id(+)
    /* data for On-Account and Claim investigion lines */
  AND app.created_by            = fu.user_id
  AND NVL(ctt.org_id,ps.org_id) = ps.org_id
  UNION ALL
  SELECT app.payment_schedule_id payment_schedule_id,
    NULL applied_trx_type,
    psa.trx_number payment_method,
    DECODE ( SIGN(ps.payment_schedule_id) , -1 , apps.arpt_sql_func_util.get_lookup_meaning(''PAYMENT_TYPE'', app.status) , NULL) applied_trx_number,
    NVL(app.amount_applied_from, app.amount_applied) applied_amount,
    TO_CHAR(NULL) state,
    app.creation_date activity_date,
    fu.user_name created_by,
    app.application_ref_num REFERENCE
  FROM apps.ar_payment_schedules ps,
    apps.ar_payment_schedules psa,
    apps.fnd_user fu,
    apps.ar_receivable_applications app,
    apps.ar_receivables_trx art
  WHERE app.payment_schedule_id       = ps.payment_schedule_id
  AND app.applied_payment_schedule_id = psa.payment_schedule_id
  AND app.status                     IN (''ACC'',''OTHER ACC'')
  AND art.receivables_trx_id(+)       = app.receivables_trx_id
  AND app.created_by                  = fu.user_id
  AND app.org_id                      = art.org_id
  UNION ALL
  SELECT adj.payment_schedule_id payment_schedule_id,
    apps.arpt_sql_func_util.get_lookup_meaning(''MISC_PHRASES'', DECODE (adj.receivables_trx_id,                                                         -15, ''ASSIGNMENT'',''ADJUSTMENT'')) applied_trx_type,
    DECODE(NVL(rt.type,''ADJUST''), ''ENDORSEMENT'', apps.arpt_sql_func_util.get_lookup_meaning(''RECEIVABLES_TRX'',rt.type), DECODE (adj.receivables_trx_id,-15, apps.arpt_sql_func_util.get_lookup_meaning(''MISC_PHRASES'', DECODE (adj.receivables_trx_id,-15, ''ASSIGNMENT'',''ADJUSTMENT'')), apps.arpt_sql_func_util.get_lookup_meaning(''ADJUSTMENT_TYPE'',adj.type))) Payment_Method,
    DECODE(adj.receivables_trx_id,                                                                                                                     -15,
    (SELECT trx.trx_number
    FROM apps.ra_customer_trx trx,
      apps.ra_customer_trx_lines lines
    WHERE trx.customer_trx_id        = lines.customer_trx_id
    AND lines.br_ref_customer_trx_id = ct.customer_trx_id
    AND lines.br_adjustment_id       = adj.adjustment_id
    ), adj.adjustment_number) applied_trx_number,
    adj.amount applied_amount,
    TO_CHAR(NULL) state,
    adj.creation_date activity_date,
    fu.user_name created_by,
    NULL REFERENCE
  FROM apps.ar_adjustments adj,
    apps.fnd_user fu,
    apps.ar_receivables_trx rt,
    apps.ra_customer_trx ct
  WHERE adj.status NOT      IN (''R'',''U'')
  AND adj.receivables_trx_id = rt.receivables_trx_id(+)
  AND adj.customer_trx_id    = ct.customer_trx_id(+)
  AND adj.created_by         = fu.user_id
  UNION ALL
  SELECT app.payment_schedule_id payment_schedule_id,
    NULL applied_trx_type,
    apps.arpt_sql_func_util.get_lookup_meaning(''RECEIVABLES_TRX'',rt.type) Payment_Method,
    NULL trx_rec_number,
    NVL(app.amount_applied_from, app.amount_applied) applied_amount,
    TO_CHAR(NULL) state,
    app.creation_date activity_date,
    fu.user_name created_by,
    app.application_ref_num REFERENCE
  FROM apps.ar_payment_schedules ps,
    apps.fnd_user fu,
    apps.ar_receivable_applications app,
    apps.ar_receivables_trx rt
  WHERE app.payment_schedule_id             = ps.payment_schedule_id
  AND app.receivables_trx_id                = rt.receivables_trx_id
  AND app.status                            = ''ACTIVITY''
  AND SIGN(app.applied_payment_schedule_id) < 0
  AND app.created_by                        = fu.user_id
  AND app.org_id                            = rt.org_id
  UNION ALL
  SELECT app.payment_schedule_id payment_schedule_id,
    apps.arpt_sql_func_util.get_lookup_meaning(''INV/CM'',ps.class) applied_trx_type,
    rt.name payment_method,
    ps_dummy.trx_number applied_trx_number,
    NVL(app.amount_applied_from, app.amount_applied) applied_amount,
    apps.arpt_sql_func_util.get_lookup_meaning( ''RECEIPT_CREATION_STATUS'', crh.status) state,
    app.creation_date activity_date,
    fu.user_name created_by,
    NULL REFERENCE
  FROM apps.ar_payment_schedules ps,
    apps.ar_payment_schedules ps_dummy,
    apps.fnd_user fu,
    apps.ar_receivable_applications app,
    apps.ar_receivables_trx rt,
    apps.ar_cash_receipt_history crh
  WHERE app.payment_schedule_id             = ps.payment_schedule_id
  AND app.applied_payment_schedule_id       = ps_dummy.payment_schedule_id
  AND ps_dummy.cash_receipt_id              = crh.cash_receipt_id
  AND crh.current_record_flag               = ''Y''
  AND app.receivables_trx_id                = rt.receivables_trx_id
  AND app.status                            = ''ACTIVITY''
  AND SIGN(app.applied_payment_schedule_id) > 0
  AND app.created_by                        = fu.user_id
  AND app.org_id                            = rt.org_id
  ) apav
WHERE APS.CUSTOMER_TRX_ID     = RCT.CUSTOMER_TRX_ID
AND APS.ORG_ID                = RCT.ORG_ID
AND APS.CUSTOMER_ID           = HCA.CUST_ACCOUNT_ID(+)
AND APS.CUSTOMER_SITE_USE_ID  = hcsu.SITE_USE_ID(+)
AND APS.ORG_ID                = HCSU.ORG_ID(+)
AND HCSU.CUST_ACCT_SITE_ID    = CUST_SITEB.CUST_ACCT_SITE_ID(+)
AND cust_siteb.party_site_id  = party_siteb.party_site_id(+)
AND HCA.PARTY_ID              = HZP.PARTY_ID
AND HZP.PARTY_ID              = HCP.OWNER_TABLE_ID(+)
AND HCP.OWNER_TABLE_NAME(+)   = ''HZ_PARTIES''
AND HCP.CONTACT_POINT_TYPE(+) = ''PHONE''
AND HCP.PRIMARY_FLAG(+)       = ''Y''
AND HCA.CUST_ACCOUNT_ID       = HZCP.CUST_ACCOUNT_ID
AND HZCP.SITE_USE_ID         IS NULL
AND HZCP.COLLECTOR_ID         = AC.COLLECTOR_ID(+)
AND HZCP.STANDARD_TERMS       = RT.TERM_ID(+)
AND HZCP.ACCOUNT_STATUS       = ARL.LOOKUP_CODE(+)
AND ARL.LOOKUP_TYPE(+)        = ''ACCOUNT_STATUS''
AND APS.TERM_ID               = TRXRT.TERM_ID(+)
AND APS.PAYMENT_SCHEDULE_ID   = APAV.PAYMENT_SCHEDULE_ID(+)
';


begin

  IF P_CUST_ACC_NUMBER IS NOT NULL THEN
    lc_where_cond      := lc_where_cond || ' AND HCA.ACCOUNT_NUMBER IN (' || REPLACE (xxeis.eis_rs_utility.get_param_values (p_param_value => trim (P_CUST_ACC_NUMBER ) ), '%%', ', ' ) || ')';
  END IF;
  
  IF P_CUST_ACC_NAME IS NOT NULL THEN
    lc_where_cond      := lc_where_cond || ' AND NVL(hca.account_name, HZP.party_name) IN (' || REPLACE (xxeis.eis_rs_utility.get_param_values (p_param_value => trim (P_CUST_ACC_NAME ) ), '%%', ', ' ) || ')';
  END IF;

	IF P_START_DATE IS NOT NULL THEN
		lc_where_cond      := lc_where_cond || ' AND trunc(RCT.TRX_DATE) >= (' || REPLACE (xxeis.eis_rs_utility.get_param_values (p_param_value => trim (P_START_DATE ) ), '%%', ', ' ) || ')';
	END IF;
  
	IF P_END_DATE IS NOT NULL THEN
		lc_where_cond      := lc_where_cond || ' AND trunc(RCT.TRX_DATE) <= (' || REPLACE (xxeis.eis_rs_utility.get_param_values (p_param_value => trim (P_END_DATE ) ), '%%', ', ' ) || ')';
	END IF;
  
	IF P_TRX_NUMBER IS NOT NULL THEN
		lc_where_cond      := lc_where_cond || ' AND RCT.TRX_NUMBER IN (' || REPLACE (xxeis.eis_rs_utility.get_param_values (p_param_value => trim (P_TRX_NUMBER ) ), '%%', ', ' ) || ')';
	END IF;

  
  
  cust_acc_trx_tab.delete;
  

		L_TRX_MAIN_QRY := L_TRX_MAIN_QRY||' '||LC_WHERE_COND;
	fnd_file.put_line(fnd_file.log,'L_TRX_MAIN_QRY'||l_trx_main_qry);
  	
  OPEN l_cust_acc_trx_csr FOR L_TRX_MAIN_QRY ;
  LOOP
	FETCH l_cust_acc_trx_csr BULK COLLECT INTO cust_acc_trx_tab LIMIT 10000;
	fnd_file.put_line(fnd_file.log,'cust_acc_trx_tab'||cust_acc_trx_tab.count);
	IF cust_acc_trx_tab.count > 0 THEN
		forall j IN 1 .. cust_acc_trx_tab.count
		INSERT INTO xxeis.XXWC_CUST_RECON_TRX_TMP_TBL VALUES cust_acc_trx_tab(j);
		--COMMIT;
	END IF;
    IF l_cust_acc_trx_csr%notfound THEN
    CLOSE l_cust_acc_trx_csr;
    exit;
    END IF;
  end loop; 
  
EXCEPTION
WHEN OTHERS THEN
  fnd_file.put_line(fnd_file.log,'cust_acc_trx_proc procedure has the following error'||sqlcode||'    '||sqlerrm);
END cust_acc_trx_proc;

END EIS_XXWC_CUST_ACC_RECON_PKG;
/
