/*************************************************************************
  $Header TMS_20180613-00046_RSH_FOR_ASN_ASBN_REQ_19136234.sql $
  Module Name: TMS_20180613-00046 Data Fix for REQNUM-19136234  

  PURPOSE: Data Fix for REQNUM-19136234

Always please ask customer to run the scripts on their TEST instance first BEFORE applying it on 
PRODUCTION. Make sure the data is validated for correctness and related functionality is verified 
after the script has been run on a test instance. Customer is responsible to authenticate and 
verify correctness of data manipulation scripts.
----------------------------------------------------

ALTER TRIGGER  APPS.ADS_RCV_SHIPMNT_HDRS_TRG1 DISABLE;

----------------------------------------------------

  REVISIONS:
  Ver        Date         Author                Description
  ---------  -----------  ------------------    --------------------------
  1.0        09-JUL-2018  Pattabhi Avula         TMS#20180613-00046

**************************************************************************/ 
SET SERVEROUTPUT ON SIZE 1000000;
DECLARE 
        X_shipment_header_id NUMBER := to_number(null);
        X_receipt_num        VARCHAR2(30);
        X_rowid              VARCHAR2(40);
        X_prev_shid          NUMBER := -999;
        X_curr_shid          NUMBER;
        x_receipt_src_code   VARCHAR2(20);
        x_org_id             NUMBER;
	x_vendor_id	     NUMBER;
	
	x_asn_type	     VARCHAR2(25);
	x_shipment_num	     VARCHAR2(30);
	x_shipment_date	     DATE;
	x_currency_code	     VARCHAR2(15);	
	x_inv_status_code    VARCHAR2(25);
	x_customer_id	     NUMBER;
	x_invoice_num	     VARCHAR2(50);
	x_notice_create_dt   DATE;
	x_exp_receipt_dt     DATE;
	x_remit_to_site	     NUMBER;
	x_invoice_date	     DATE;
	x_invoice_amount     NUMBER;	




        CURSOR headers
        IS
                SELECT  rsl.*
                FROM    rcv_shipment_lines rsl
                WHERE   rsl.source_document_code IN ('PO','RMA','REQ','INVENTORY')
                    AND rsl.ASN_LINE_FLAG='Y'
		    AND NOT EXISTS
                        (SELECT '1'
                        FROM    rcv_shipment_headers rsh
                        WHERE   rsh.shipment_header_id = rsl.shipment_header_id
                        )
		    AND EXISTS
                        (SELECT '1'
                        FROM    rcv_headers_interface rhi
                        WHERE   rhi.receipt_header_id = rsl.shipment_header_id
                        )	
                ORDER BY rsl.shipment_header_id; 

                sline headers%ROWTYPE; 

        BEGIN 
                /* Insert into shipment headers */ 
                OPEN headers; 
                LOOP 
                        FETCH headers INTO sline; 
                        EXIT 
                WHEN headers%NOTFOUND; 

                        X_curr_shid         := sline.shipment_header_id; 
                        X_receipt_num       := NULL; 

                        IF (X_prev_shid     <> X_curr_shid) THEN 
                                X_prev_shid := X_curr_shid; 
				
				BEGIN	
                                 SELECT vendor_id 
                                 INTO x_vendor_id 
                                 FROM po_headers_all 
                                 WHERE po_header_id = sline.po_header_id;
				EXCEPTION
				 WHEN OTHERS THEN
				  x_vendor_id := NULL;
				END;
				
				SELECT
				asn_type,
				shipment_num,
				shipped_date,
				currency_code,
				invoice_status_code,
				customer_id,
				invoice_num,
				notice_creation_date,
				expected_receipt_date,
				remit_to_site_id,
				invoice_date,
				total_invoice_amount
				INTO
				x_asn_type,
				x_shipment_num,
				x_shipment_date,
				x_currency_code,
				x_inv_status_code,
				x_customer_id,
				x_invoice_num,
				x_notice_create_dt,
				x_exp_receipt_dt,
				x_remit_to_site,
				x_invoice_date,
				x_invoice_amount
				FROM rcv_headers_interface
				WHERE receipt_header_id=X_curr_shid;

                                IF (sline.source_document_code ='PO') THEN
                                    x_receipt_src_code := 'VENDOR';

                                    SELECT org_id 
                                    INTO x_org_id 
                                    FROM po_headers_all 
                                    WHERE po_header_id = sline.po_header_id;

				END IF;
				
                                IF (sline.source_document_code ='RMA') THEN
				    x_receipt_src_code := 'CUSTOMER';

                                    SELECT org_id 
                                    INTO x_org_id 
                                    FROM oe_order_headers_all 
                                    WHERE header_id = sline.oe_order_header_id;

                                END IF;

                                IF (sline.source_document_code ='REQ') THEN
				    x_receipt_src_code := 'INTERNAL ORDER';

                                    SELECT org_id 
                                    INTO x_org_id 
                                    FROM po_requisition_lines_all
                                    WHERE requisition_line_id = sline.requisition_line_id;

                                END IF;
                                IF (sline.source_document_code ='INVENTORY') THEN
				    x_receipt_src_code := 'INVENTORY';

                                    SELECT operating_unit 
                                    INTO x_org_id 
                                    FROM ORG_ORGANIZATION_DEFINITIONS 
                                    WHERE organization_id = sline.to_organization_id
				    AND ROWNUM=1;

                                END IF;
                                

                                Dbms_Application_Info.set_client_info(x_org_id);

                                rcv_shipment_headers_pkg.insert_row ( X_rowid,                                 /* Row ID */ 
                                X_curr_shid,                                                                   /* Shipment Header ID */ 
                                sline.last_update_date,                                                        /* Last Update Date */ 
                                sline.last_updated_by,                                                         /*  Last Updated By */ 
                                sline.creation_date,                                                           /* Creation Date */ 
                                sline.created_by,                                                              /* Created By */ 
                                sline.last_update_login,                                                       /* Last Update Login */ 
                                x_receipt_src_code,                                                                      /* Receipt Source Code */ 
                                x_vendor_id,                                                               /* Vendor ID */ 
                                NULL,                                                          /* Vendor Site ID */ 
                                NULL,                                                         /* Organizatin ID */ 
                                sline.to_organization_id,                                                         /* destination org id */ 
                                NULL,                                                                          /* Shipment Number */ 
                                X_receipt_num,                                                                 /* Receipt Number */ 
                                sline.deliver_to_location_id,                                                  /* Ship-To Location ID */ 
                                NULL,                                                                          /* Bill of Lading */ 
                                NULL,                                                                          /* Packing Slip */ 
                                NULL,                                                                          /* Shipped Date */ 
                                NULL,                                                                          /* Freight Carrier Code */ 
                                sline.creation_date,                                                           /* Expected Receipt Date */ 
                                sline.employee_id,                                                             /* Employee ID */ 
                                TO_NUMBER(NULL),                                                               /* Number of Containers */ 
                                NULL,                                                                          /* Waybill/Aibill Number */ 
                                NULL,                                                                          /* Comments */ 
                                NULL,                                                                          /* Attribute Category */ 
                                NULL,                                                                          /* Attribute1 */ 
                                NULL,                                                                          /* Attribute2 */ 
                                NULL,                                                                          /* Attribute3 */ 
                                NULL,                                                                          /* Attribute4 */ 
                                NULL,                                                                          /* Attribute5 */ 
                                NULL,                                                                          /* Attribute6 */ 
                                NULL,                                                                          /* Attribute7 */ 
                                NULL,                                                                          /* Attribute8 */ 
                                NULL,                                                                          /* Attribute9 */ 
                                NULL,                                                                          /* Attribute10 */ 
                                NULL,                                                                          /* Attribute11 */ 
                                NULL,                                                                          /* Attribute12 */ 
                                NULL,                                                                          /* Attribute13 */ 
                                NULL,                                                                          /* Attribute14 */ 
                                NULL,                                                                          /* Attribute15 */ 
                                NULL,                                                                          /* USSGL Transaction Code */ 
                                NULL,                                                                          /* Government Context */ 
                                NULL,                                                                          /* Request ID */ 
                                NULL,                                                                          /* Program Appliation ID */ 
                                NULL,                                                                          /* Program ID */ 
                                NULL,                                                                          /* Program Update Date */ 
                                NULL,                                                                          /* customer id */ 
                                NULL                                                                           /* Customer site id */ 
                                );                                  
				
			/*UPDATING THE RSH RECORD WITH SUITABLE DATA FROM RHI TABLE*/
				
				UPDATE rcv_shipment_headers
				SET asn_type=x_asn_type,        
				shipment_num=x_shipment_num,    
				shipped_date=x_shipment_date,   
				currency_code=x_currency_code,   
				invoice_status_code=x_inv_status_code, 
				customer_id=x_customer_id,     
				invoice_num=x_invoice_num,     
				notice_creation_date=x_notice_create_dt,
				remit_to_site_id=x_remit_to_site,
				expected_receipt_date=x_exp_receipt_dt,
				invoice_date=x_invoice_date,
				invoice_amount=x_invoice_amount
				WHERE shipment_header_id=X_curr_shid; 				
				

                        END IF; 
                END LOOP; 
                CLOSE headers; 
        EXCEPTION 
        WHEN OTHERS THEN 
                RAISE; 
        END;
/
----------------------------------------------------

COMMIT;

/*ALTER TRIGGER  APPS.ADS_RCV_SHIPMNT_HDRS_TRG1 ENABLE;*/

----------------------------------------------------
/
