create or replace 
PACKAGE BODY         XXEIS.EIS_PO_XXWC_OPEN_PUR_LIST_PKG
IS
--//============================================================================
--//  
--// Change Request 			:: Performance Issue  
--//
--// Object Usage 				:: This Object Referred by "Open Purchase Orders Listing - WC"
--//
--// Object Name         		:: XXEIS.EIS_PO_XXWC_OPEN_PUR_LIST_PKG
--//
--// Object Type         		:: Package Specification
--//
--// Object Description  		:: This Package programs will call in EIS_XXWC_PO_OPEN_ORDERS_V view to populate data
--//
--// Version Control
--//============================================================================
--// Version    Author           Date          Description
--//----------------------------------------------------------------------------
--// 1.0        Siva  	      18-may-2016       Initial Build TMS#20160503-00092
--// 1.1        Siva   		  07-Feb-2017	    TMS#20170105-00209 
--//============================================================================
	 FUNCTION GET_PO_TYPE (P_TYPE_LOOKUP_CODE in VARCHAR2,P_org_id in number) RETURN VARCHAR2
	IS
--//============================================================================
--//
--// Object Name         :: get_po_type  
--//
--// Object Usage 		 :: This Object Referred by "Open Purchase Orders Listing - WC"
--//
--// Object Type         :: FUNCTION
--//
--// Object Description  :: This function gets the record based on parameter and insert into table.
--//
--// Version Control
--//============================================================================
--// Version    Author           Date          Description
--//----------------------------------------------------------------------------
--// 1.0         Siva        	18-May-2016       Initial Build 
--//============================================================================	
	L_HASH_INDEX varchar2(100);
	l_sql varchar2(32000);
	begin
	G_PO_TYPE := NULL;
	l_sql :=' SELECT PDT.TYPE_NAME
	  FROM po_document_types pdt
	  WHERE pdt.document_type_code IN (''PO'', ''PA'')
	  AND PDT.DOCUMENT_SUBTYPE      = :1
	  AND PDT.ORG_ID                = :2';
		begin
			l_hash_index:=P_TYPE_LOOKUP_CODE||'-'||P_org_id;
			G_PO_TYPE := G_PO_TYPE_VLDN_TBL(l_hash_index);
		exception
		when no_data_found
		THEN
		begin      
		EXECUTE IMMEDIATE L_SQL INTO G_PO_TYPE USING P_TYPE_LOOKUP_CODE,P_org_id;
		EXCEPTION WHEN NO_DATA_FOUND THEN
		G_PO_TYPE :=null;
		WHEN OTHERS THEN
		G_PO_TYPE :=null;
		END;      
						   l_hash_index:=P_TYPE_LOOKUP_CODE||'-'||P_org_id;
						   G_PO_TYPE_VLDN_TBL(L_HASH_INDEX) := G_PO_TYPE;
		end;
		RETURN  G_PO_TYPE;
		EXCEPTION WHEN OTHERS THEN
		  G_PO_TYPE:=null;
		  RETURN  G_PO_TYPE;

	END GET_PO_TYPE;

	FUNCTION GET_authorization_status (P_LOOKUP_CODE IN VARCHAR2) RETURN VARCHAR2
	IS
--//============================================================================
--//
--// Object Name         :: get_authorization_status  
--//
--// Object Usage 		 :: This Object Referred by "Open Purchase Orders Listing - WC"
--//
--// Object Type         :: FUNCTION
--//
--// Object Description  :: This function gets the record based on parameter and insert into table.
--//
--// Version Control
--//============================================================================
--// Version    Author           Date          Description
--//----------------------------------------------------------------------------
--// 1.0         Siva        	18-May-2016       Initial Build 
--//============================================================================	
	L_HASH_INDEX varchar2(100);
	l_sql varchar2(32000);
	begin
	G_authorization_status := NULL;
	L_SQL :='SELECT plc.DISPLAYED_FIELD
		FROM PO_LOOKUP_CODES PLC
		WHERE PLC.LOOKUP_CODE = :1
		AND plc.lookup_type in (''PO APPROVAL'', ''DOCUMENT STATE'')'; 
		begin
			L_HASH_INDEX:=P_LOOKUP_CODE;
			G_authorization_status := G_author_status_VLDN_TBL(l_hash_index);
		exception
		when no_data_found
		THEN
		begin      
		EXECUTE IMMEDIATE L_SQL INTO G_authorization_status USING P_LOOKUP_CODE;
		EXCEPTION WHEN NO_DATA_FOUND THEN
		G_authorization_status :=null;
		WHEN OTHERS THEN
		G_authorization_status :=null;
		END;      
						   l_hash_index:=P_LOOKUP_CODE;
						   G_author_status_VLDN_TBL(L_HASH_INDEX) := G_authorization_status;
		end;
		RETURN  G_authorization_status;
		EXCEPTION WHEN OTHERS THEN
		  G_authorization_status:=null;
		  RETURN  G_authorization_status;

	END GET_authorization_status;

	FUNCTION GET_LOOKUP_MEANING (P_LOOKUP_CODE in VARCHAR2,p_lookup_type in VARCHAR2) RETURN VARCHAR2
	IS
--//============================================================================
--//
--// Object Name         :: get_lookup_meaning  
--//
--// Object Usage 		 :: This Object Referred by "Open Purchase Orders Listing - WC"
--//
--// Object Type         :: FUNCTION
--//
--// Object Description  :: This function gets the record based on parameter and insert into table.
--//
--// Version Control
--//============================================================================
--// Version    Author           Date          Description
--//----------------------------------------------------------------------------
--// 1.0         Siva        	18-May-2016       Initial Build 
--//============================================================================	
	L_HASH_INDEX varchar2(100);
	l_sql varchar2(32000);
	begin
	G_lookup_meaning := NULL;
	L_SQL :='SELECT plc.DISPLAYED_FIELD
		FROM PO_LOOKUP_CODES PLC
		WHERE PLC.LOOKUP_CODE = :1
		AND plc.lookup_type = :2'; 
		begin
			L_HASH_INDEX:=P_LOOKUP_CODE||'-'||P_LOOKUP_TYPE;
			G_lookup_meaning := G_lookup_meaning_VLDN_TBL(l_hash_index);
		exception
		when no_data_found
		THEN
		begin      
		EXECUTE IMMEDIATE L_SQL INTO G_lookup_meaning USING P_LOOKUP_CODE,p_lookup_type;
		EXCEPTION WHEN NO_DATA_FOUND THEN
		G_lookup_meaning :=null;
		WHEN OTHERS THEN
		G_lookup_meaning :=null;
		END;      
						   l_hash_index:=P_LOOKUP_CODE||'-'||p_lookup_type;
						   G_PO_TYPE_VLDN_TBL(L_HASH_INDEX) := G_lookup_meaning;
		end;
		RETURN  G_lookup_meaning;
		EXCEPTION WHEN OTHERS THEN
		  G_lookup_meaning:=null;
		  RETURN  G_lookup_meaning;

	end GET_LOOKUP_MEANING;


	function GET_PO_LINE_COUNT (P_HEADER_ID in number) return number
	is
--//============================================================================
--//
--// Object Name         :: get_po_line_count  
--//
--// Object Usage 		 :: This Object Referred by "Open Purchase Orders Listing - WC"
--//
--// Object Type         :: FUNCTION
--//
--// Object Description  :: This function gets the record based on parameter and insert into table.
--//
--// Version Control
--//============================================================================
--// Version    Author           Date          Description
--//----------------------------------------------------------------------------
--// 1.0         Siva        	18-May-2016       Initial Build 
--//============================================================================
	L_HASH_INDEX NUMBER;
	l_sql varchar2(32000);
	BEGIN
	g_po_lines_count := NULL;
	L_SQL :=' (SELECT COUNT (*)
		FROM po_lines pol1
		WHERE pol1.po_header_id = :1
		)'; 
		begin
			L_HASH_INDEX:=P_HEADER_ID;
			g_po_lines_count := G_PO_LINE_COUNT_VLDN_TBL(l_hash_index);
		exception
		when no_data_found
		THEN
		begin      
		EXECUTE IMMEDIATE L_SQL INTO g_po_lines_count USING P_HEADER_ID;
		EXCEPTION WHEN NO_DATA_FOUND THEN
		g_po_lines_count :=0;
		WHEN OTHERS THEN
		g_po_lines_count :=0;
		END;      
						   l_hash_index:=P_HEADER_ID;
						   G_PO_LINE_COUNT_VLDN_TBL(L_HASH_INDEX) := g_po_lines_count;
		end;
		RETURN  g_po_lines_count;
		EXCEPTION WHEN OTHERS THEN
		  g_po_lines_count:=null;
		  return  g_po_lines_count;

	end GET_PO_LINE_COUNT;



FUNCTION GET_ACCEPTANCE_DETAILS (P_po_header_id  IN NUMBER,P_REQUEST_TYPE IN VARCHAR2) RETURN VARCHAR2 IS
--//============================================================================
--//
--// Object Name         :: get_acceptance_details  
--//
--// Object Usage 		 :: This Object Referred by "Open Purchase Orders Listing - WC"
--//
--// Object Type         :: FUNCTION
--//
--// Object Description  :: This function gets the record based on parameter and insert into table.
--//
--// Version Control
--//============================================================================
--// Version    Author           Date          Description
--//----------------------------------------------------------------------------
--// 1.0         Siva        	18-May-2016       Initial Build 
--//============================================================================
L_HASH_INDEX NUMBER;
l_sql varchar2(32000);
	BEGIN
	G_ACTION_DATE	  := NULL; 
	G_ACCEPTED_FLAG	  := NULL;
	G_ACCEPTANCE_TYPE := NULL;
	G_ACCEPTED_BY     := NULL;

	L_SQL :='select  
			PA1.ACTION_DATE ACTION_DATE,
			NVL(PA1.ACCEPTED_FLAG,''N'') ACCEPTED_FLAG,
			POC.DISPLAYED_FIELD ACCEPTANCE_TYPE,
			HR.FULL_NAME ACCEPTED_BY      
		from 
		  PO_ACCEPTANCES pa1,
		  (SELECT po_header_id,
			MAX(acceptance_id) max_acceptance_id
		  FROM po_acceptances
		  GROUP BY po_header_id
		  ) PA2,
		  PO_LOOKUP_CODES POC,
		  PO_AGENTS PA,
		  PER_all_PEOPLE_F HR 
		WHERE pa1.po_header_id = pa2.po_header_id
		and PA1.ACCEPTANCE_ID  = PA2.MAX_ACCEPTANCE_ID
		and pa1.po_header_id   = :1
		AND POC.LOOKUP_CODE(+) = PA1.ACCEPTANCE_LOOKUP_CODE
		AND POC.LOOKUP_TYPE(+) = ''ACCEPTANCE TYPE''
		and PA1.EMPLOYEE_ID  = PA.AGENT_ID(+)
		and PA.AGENT_ID      = HR.PERSON_ID(+)
		and TRUNC(sysdate) between NVL(HR.EFFECTIVE_START_DATE, TRUNC(sysdate)) and NVL(HR.EFFECTIVE_END_DATE, TRUNC(sysdate))
		AND pa1.PO_LINE_LOCATION_ID IS NULL'; 
		
		
		   BEGIN
			 L_HASH_INDEX:=P_po_header_id;
			if P_request_type='ACTION_DATE' then
				G_ACTION_DATE := G_ACTN_DATE_VLDN_TBL(L_HASH_INDEX);
			ELSif P_request_type='ACCEPTED_FLAG' then
				G_ACCEPTED_FLAG := G_ACCEP_FLAG_VLDN_TBL(L_HASH_INDEX);
			ELSif P_request_type='ACCEPTANCE_TYPE' then
				G_ACCEPTANCE_TYPE := G_ACCEP_TYPE_VLDN_TBL(L_HASH_INDEX);
			ELSif P_request_type='ACCEPTANCE_BY' then
				G_ACCEPTED_BY := G_ACCEP_BY_VLDN_TBL(L_HASH_INDEX);
			END IF;

		exception
		when no_data_found
		THEN
		begin      
		EXECUTE IMMEDIATE L_SQL INTO G_ACTION_DATE,G_ACCEPTED_FLAG,G_ACCEPTANCE_TYPE,G_ACCEPTED_BY USING P_po_header_id;
		EXCEPTION WHEN NO_DATA_FOUND THEN
		if P_request_type='ACTION_DATE' then
		G_ACTION_DATE := null;
		elsif P_request_type='ACCEPTED_FLAG' then
		G_ACCEPTED_FLAG := null;
		elsif P_request_type='ACCEPTANCE_TYPE' then
		G_ACCEPTANCE_TYPE := null;
		elsif P_request_type='ACCEPTANCE_BY' then
		G_ACCEPTED_BY := null;
		end if;
		 end;
						   l_hash_index:=P_po_header_id;
						   G_ACTN_DATE_VLDN_TBL(L_HASH_INDEX) := G_ACTION_DATE;
						   G_ACCEP_FLAG_VLDN_TBL(L_HASH_INDEX):= G_ACCEPTED_FLAG;
						   G_ACCEP_TYPE_VLDN_TBL(L_HASH_INDEX):= G_ACCEPTANCE_TYPE;
						   G_ACCEP_BY_VLDN_TBL(L_HASH_INDEX)  := G_ACCEPTED_BY;
						   
						   
		end;
		   IF P_REQUEST_TYPE='ACTION_DATE' THEN
			return  G_ACTION_DATE ;
			ELSIF P_REQUEST_TYPE='ACCEPTED_FLAG' THEN
			return   G_ACCEPTED_FLAG ;
			ELSIF P_REQUEST_TYPE='ACCEPTANCE_TYPE' THEN
			return   G_ACCEPTANCE_TYPE ;
			ELSIF P_REQUEST_TYPE='ACCEPTANCE_BY' THEN
			return   G_ACCEPTED_BY ;
		   END IF;
		EXCEPTION WHEN OTHERS THEN
		 IF P_REQUEST_TYPE='ACTION_DATE' THEN
			G_ACTION_DATE := null;
			return  G_ACTION_DATE ;
			ELSIF P_REQUEST_TYPE='ACCEPTED_FLAG' THEN
			G_ACCEPTED_FLAG := null;
			return   G_ACCEPTED_FLAG ;
			ELSIF P_REQUEST_TYPE='ACCEPTANCE_TYPE' THEN
			G_ACCEPTANCE_TYPE := null;
			return   G_ACCEPTANCE_TYPE ;
			ELSIF P_REQUEST_TYPE='ACCEPTANCE_BY' THEN
			G_ACCEPTED_BY := null;
			return   G_ACCEPTED_BY ;
		   END IF;

	end GET_ACCEPTANCE_DETAILS;

 FUNCTION get_buyer_group( p_agent_id IN number )  RETURN VARCHAR2
  IS
--//============================================================================
--//
--// Object Name         :: get_buyer_group
--//
--// Object Usage 		 :: This Object Referred by "Open Purchase Orders Listing - WC"
--//
--// Object Type         :: FUNCTION
--//
--// Object Description  :: This function gets the record based on parameter. 
--//
--// Version Control
--//============================================================================
--// Version    Author           Date          Description
--//----------------------------------------------------------------------------
--// 1.1        Siva   		  02/07/2017	    TMS#20170105-00209 
--//============================================================================
	L_HASH_INDEX number;
	l_sql varchar2(32000);
	begin
	g_buyer_group := NULL;
	L_SQL :='SELECT  
      CASE
      WHEN ( ppos.NAME LIKE ''%Branch Manager%''
      OR ppos.NAME LIKE ''%Sales%'')
      THEN ''Sales''
      WHEN ppos.NAME LIKE ''%Branch Buyer%''
      THEN ''Branch Buyer''
      WHEN ppos.NAME LIKE ''%Rental%''
      THEN ''Rental''
      else ''Purchasing''
    end buyer_group 
  FROM apps.po_employee_hierarchies_all peha,
    apps.per_positions ppos,
    apps.per_all_people_f papf,
    apps.per_position_structures pps
  WHERE pps.business_group_id   = peha.business_group_id
  AND pps.position_structure_id = peha.position_structure_id
  AND papf.PERSON_ID            = peha.employee_id
  AND papf.EFFECTIVE_END_DATE   > SYSDATE
  AND ppos.position_id          = peha.EMPLOYEE_POSITION_ID
  and peha.superior_level      <>0
  and pps.name                  = ''WC Purchasing Master Reporting''
  AND peha.superior_level=
  (select min(superior_level)
  FROM apps.po_employee_hierarchies_all peha2
  WHERE superior_level<>0
  and peha2.employee_id =papf.person_id
  )
  and  papf.person_id    = :1   ';
		begin
			L_HASH_INDEX:=p_agent_id;
			g_buyer_group := g_buyer_group_vldn_tbl(l_hash_index);
		exception
		when no_data_found
		THEN
		begin
		EXECUTE IMMEDIATE L_SQL INTO g_buyer_group USING p_agent_id;
		EXCEPTION WHEN NO_DATA_FOUND THEN
		g_buyer_group :=null;
		WHEN OTHERS THEN
		g_buyer_group :=null;
		END;
						   l_hash_index:=p_agent_id;
						   g_buyer_group_vldn_tbl(L_HASH_INDEX) := g_buyer_group;
		end;
		RETURN  g_buyer_group;
		EXCEPTION WHEN OTHERS THEN
		  g_buyer_group:=null;
		  RETURN  g_buyer_group;
END get_buyer_group;

END EIS_PO_XXWC_OPEN_PUR_LIST_PKG;
/
