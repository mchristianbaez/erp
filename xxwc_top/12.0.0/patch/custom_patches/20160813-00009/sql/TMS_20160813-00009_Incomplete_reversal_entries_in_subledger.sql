/*************************************************************************
  $Header TMS_20160813-00009_Incomplete_reversal_entries_in_subledger.sql $
  Module Name: TMS#20160813-00009  --Data fix Script 

  PURPOSE: Data fix to update the Period End Accrual issue records

  REVISIONS:
  Ver        Date         Author                Description
  ---------  -----------  ------------------    --------------------------
  1.0        02-NOV-2016 Pattabhi Avula         TMS#20160813-00009 

**************************************************************************/ 
SET SERVEROUTPUT ON SIZE 100000;
SET VERIFY OFF;
DECLARE
BEGIN
   DBMS_OUTPUT.put_line ('TMS: 20160813-00009    , Before Update');
   
UPDATE xla_ae_headers
   SET gl_transfer_status_code = 'NT'  -- (Not Transferable)
 WHERE     application_id = 707
       AND gl_transfer_status_code = 'N'--Incomplete
       AND event_type_code IN ('PERIOD_END_ACCRUAL')
       AND ledger_id = 2061
       AND GL_TRANSFER_STATUS_CODE = 'N'
       AND accounting_date BETWEEN TO_DATE ('01-MAY-2012', 'DD-MON-YYYY')
                               AND TRUNC (SYSDATE);

 DBMS_OUTPUT.put_line (
         'TMS: 20160813-00009  Period End Accrual Fix updated Records: '
      || SQL%ROWCOUNT);

   COMMIT;

   DBMS_OUTPUT.put_line ('TMS: 20160813-00009    , End Update');
EXCEPTION
   WHEN OTHERS
   THEN
      ROLLBACK;
      DBMS_OUTPUT.put_line ('TMS: 20160813-00009 , Errors : ' || SQLERRM);
END;
/