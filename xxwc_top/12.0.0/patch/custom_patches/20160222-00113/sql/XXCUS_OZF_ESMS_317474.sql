/*
 ESMS: 317474 
 Date: 02/21/2016
 Notes:  GSC - Place all 2015 DEDUCTION  agreements on hold
*/
SET SERVEROUTPUT ON SIZE 1000000
DECLARE
  CURSOR onhold_offers IS
select oo.offer_id
          ,oo.org_id
          ,oo.offer_code
          ,oo.qp_list_header_id
          ,oo.object_version_number obj_ver_num
from ozf_offers oo, qp_list_headers_b qlhv
where 1 =1
and qlhv.list_header_id =oo.qp_list_header_id
and qlhv.attribute7 ='2015'
and qlhv.attribute5 ='DEDUCTION'
and oo.user_status_id =1604
and oo.status_code ='ACTIVE';
--
  x_offer_adjustment_id   NUMBER;
  x_offer_adjst_tier_id   NUMBER;
  x_offer_adj_line_id     NUMBER;
  x_object_version_number NUMBER;

  x_qp_list_header_id NUMBER;
  x_err_location      NUMBER;

  l_offer_adj_rec        ozf_offer_adjustment_pvt.offer_adj_rec_type;
  l_offadj_tier_rec_type ozf_offer_adj_tier_pvt.offadj_tier_rec_type;

  l_offadj_line_rec_type ozf_offer_adj_line_pvt.offadj_line_rec_type;

  l_modifier_list_rec ozf_offer_pub.modifier_list_rec_type;
  l_modifier_line_tbl ozf_offer_pub.modifier_line_tbl_type;
  l_na_qualifier_tbl  ozf_offer_pub.na_qualifier_tbl_type;
  l_prod_rec_tbl      ozf_offer_pub.prod_rec_tbl_type;
  l_offer_tier_tbl    ozf_offer_pub.offer_tier_tbl_type;
  l_excl_rec_tbl      ozf_offer_pub.excl_rec_tbl_type;
  l_discount_line_tbl ozf_offer_pub.discount_line_tbl_type;
  l_act_product_tbl   ozf_offer_pub.act_product_tbl_type;
  l_vo_pbh_tbl        ozf_offer_pub.vo_disc_tbl_type;
  l_vo_dis_tbl        ozf_offer_pub.vo_disc_tbl_type;
  l_vo_prod_tbl       ozf_offer_pub.vo_prod_tbl_type;
  l_qualifier_tbl     ozf_offer_pub.qualifiers_tbl_type;
  l_vo_mo_tbl         ozf_offer_pub.vo_mo_tbl_type;
  l_budget_tbl        ozf_offer_pub.budget_tbl_type;

  l_object_version_number NUMBER;
  l_obj_version           NUMBER;
  l_start_date            DATE;
  l_offer_id              NUMBER;
  l_description           qp_list_headers_vl.description%TYPE;
  l_offer_type            VARCHAR2(50);
  l_user_status_id        NUMBER;

  x_errbuf  VARCHAR2(2000);
  x_retcode VARCHAR2(2000);

  x_return_status VARCHAR2(1);
  x_msg_count     NUMBER;
  x_msg_data      VARCHAR2(4000);

BEGIN 
  dbms_output.put_line('Begin Time: '||TO_CHAR(SYSDATE, 'DD-MON-YYYY HH24:MI:SS'));
  dbms_output.put_line(' ');    
  FOR rec IN onhold_offers
  LOOP
    mo_global.set_policy_context('S', rec.org_id);
    SAVEPOINT here_we_start;
  
    l_modifier_list_rec.qp_list_header_id     := rec.qp_list_header_id;
    l_modifier_list_rec.object_version_number := rec.obj_ver_num;
    l_modifier_list_rec.status_code           := 'ONHOLD';
    l_modifier_list_rec.user_status_id        := 1607;
    l_modifier_list_rec.modifier_operation    := 'UPDATE';
    l_modifier_list_rec.offer_operation       := 'UPDATE';
    l_modifier_list_rec.offer_id              := rec.offer_id;
  
    ozf_offer_pub.process_modifiers(p_init_msg_list     => fnd_api.g_false
                                   ,p_api_version       => 1.0
                                   ,p_commit            => fnd_api.g_false
                                   ,x_return_status     => x_return_status
                                   ,x_msg_count         => x_msg_count
                                   ,x_msg_data          => x_msg_data
                                   ,p_offer_type        => l_offer_type
                                   ,p_modifier_list_rec => l_modifier_list_rec
                                   ,p_modifier_line_tbl => l_modifier_line_tbl
                                   ,p_qualifier_tbl     => l_qualifier_tbl
                                   ,p_budget_tbl        => l_budget_tbl
                                   ,p_act_product_tbl   => l_act_product_tbl
                                   ,p_discount_tbl      => l_discount_line_tbl
                                   ,p_excl_tbl          => l_excl_rec_tbl
                                   ,p_offer_tier_tbl    => l_offer_tier_tbl
                                   ,p_prod_tbl          => l_prod_rec_tbl
                                   ,p_na_qualifier_tbl  => l_na_qualifier_tbl
                                   ,x_qp_list_header_id => x_qp_list_header_id
                                   ,x_error_location    => x_err_location);
  
    IF ((x_return_status = fnd_api.g_ret_sts_error) OR
       (x_return_status = fnd_api.g_ret_sts_unexp_error))
    THEN
      dbms_output.put_line('offer id =' || rec.offer_id ||
                           ', failed to update');
      ROLLBACK TO here_we_start;
    ELSE
      Null;
    END IF;
  END LOOP;
  COMMIT;
  dbms_output.put_line('Commit Complete...');
  dbms_output.put_line(' ');  
  dbms_output.put_line('End Time: '||TO_CHAR(SYSDATE, 'DD-MON-YYYY HH24:MI:SS'));  
EXCEPTION
  WHEN OTHERS THEN
    dbms_output.put_line('Outer block, message =>' || SQLERRM);
    ROLLBACK;
END;
/