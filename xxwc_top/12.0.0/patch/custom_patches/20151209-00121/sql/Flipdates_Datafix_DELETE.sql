/*************************************************************************
  $Header TMS 20151209-00121 Flip dates Datafix-DELETE.sql $
  Module Name: TMS 20151209-00121 Flip dates Datafix-DELETE

  PURPOSE: Delete records from XXWC.XXWC_ISR_FLIP_DATE_UPD_STG

  REVISIONS:
  Ver        Date         Author                Description
  ---------  -----------  ------------------    ---------------------
  1.0        09-Dec-2015  Ram Talluri          TMS # 20151209-00121 

**************************************************************************/ 
SET SERVEROUTPUT ON SIZE 1000000
DECLARE
BEGIN

   DBMS_OUTPUT.put_line ('TMS: 20151209-00121   , Before delete');
   
   DELETE FROM xxwc.XXWC_ISR_FLIP_DATE_UPD_STG;
   
   DBMS_OUTPUT.put_line ('TMS: 20151209-00121  , After delete : Rows deleted '||SQL%ROWCOUNT);
   
   COMMIT;
   
EXCEPTION WHEN OTHERS THEN
   
    ROLLBACK;
    DBMS_OUTPUT.put_line ('TMS: 20151209-00121, Errors : '||SQLERRM);   
END;
/