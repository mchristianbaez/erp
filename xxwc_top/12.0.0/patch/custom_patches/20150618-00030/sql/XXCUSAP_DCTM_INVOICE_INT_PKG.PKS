create or replace
PACKAGE xxcusap_dctm_invoice_int_pkg IS

  /**************************************************************************
  File Name: XXCUSAP_DCTM_INVOICE_INT_PKG
  
  PROGRAM TYPE: SQL Script
  
  PURPOSE:      package is used to import invoices loaded into the interface
                by Documentum
  HISTORY
  =============================================================================
         Last Update Date : 09/17/2008
  =============================================================================
  =============================================================================
  VERSION DATE          AUTHOR(S)          DESCRIPTION
  ------- -----------   -----------------  ------------------------------------
  1.0     27-Jul-2011   Kathy Poling       Creation this package 
  2.0     11-Aug-2015   M Hari Prasad      Added procedure for invoice imaage
  
  =============================================================================
  *****************************************************************************/

  g_request_id NUMBER := fnd_global.conc_request_id;
  g_user_id    NUMBER := fnd_global.user_id;
  g_login_id   NUMBER := fnd_profile.value('LOGIN_ID');

  PROCEDURE import_inv(p_errbuf OUT VARCHAR2, p_retcode OUT NUMBER);

  PROCEDURE load_inv(p_errbuf   OUT VARCHAR2
                    ,p_retcode  OUT NUMBER
                    ,p_group_id IN VARCHAR2
                    ,p_source   IN VARCHAR2);

  PROCEDURE exp_dctm_image(p_errbuf OUT VARCHAR2, p_retcode OUT NUMBER);
  
  

  PROCEDURE wc_load_inv(p_errbuf   OUT VARCHAR2
                       ,p_retcode  OUT NUMBER
                       ,p_group_id IN VARCHAR2);
	---Added Below for v 2.0				   
  PROCEDURE XXCUSAP_INV_INT_PRC (
              p_request_id IN NUMBER
                   );				   
   ---Added End for v 2.0
END xxcusap_dctm_invoice_int_pkg;
/