-----------------------------------------------------------------------------------------------
/*************************************************************************************************************************
  $Header EIS_XXWC_CASH_EXCE_TBL1_N11
  File Name: EIS_XXWC_CASH_EXCE_TBL1_N11.sql
  PURPOSE:   
  REVISIONS:
     Ver        Date         Author         Description
     ---------  ----------   --------    -----------------------------------------------------------------------------------
     1.0        12-Apr-2016  Siva        TMS#20160411-00103 Cash Drawer Exception Report - Program Running for longer time
****************************************************************************************************************************/
CREATE INDEX XXEIS.EIS_XXWC_CASH_EXCE_TBL1_N11
 ON XXEIS.EIS_XXWC_CASH_EXCEP_TBL (PROCESS_ID) TABLESPACE APPS_TS_TX_DATA
/
