create or replace 
PACKAGE BODY   XXEIS.EIS_XXWC_CASH_EXCEPTION_PKG AS
 --//============================================================================
--//  
--// Change Request 			:: Performance Issue 
--//
--// Object Usage 				:: This Object Referred by "Cash Drawer Exception Report
--//
--// Object Name         		:: XXEIS.EIS_XXWC_CASH_EXCEPTION_PKG
--//
--// Object Type         		:: Package Body
--//
--// Object Description  		:: This Package will trigger in before report and insert the values into Table
--//
--// Version Control
--//============================================================================
--// Version    Author           Date          		Description
--//----------------------------------------------------------------------------
--// 1.0     Siva  	04/12/2016   Initial Build  --TMS#20160411-00103   -- Performance tuning
--//============================================================================

  procedure process_cash_exception_orders (p_process_id in number,
                                          p_location in  varchar2,
                                          p_as_of_date in date
                                            )  as
--//============================================================================
--//
--// Object Name         :: process_cash_exception_orders  
--//
--// Object Usage 		 :: This Object Referred by "Cash Drawer Exception Report
--//
--// Object Type         :: Procedure
--//
--// Object Description  :: This  procedure is created based on the EIS_XXWC_CASH_DRAWN_EXCE_V View.Due to the 
--// 						performance issue ,spilt the view definition based on the driving tables and driving data.
--//
--// Version Control
--//============================================================================
--// Version    Author           Date          Description
--//----------------------------------------------------------------------------
--// 1.0     Siva  	04/12/2016   Initial Build  --TMS#20160411-00103 -- Performance tuning
--//============================================================================												
  l_orgainzation_id 			number;
  l_org_sql 					varchar2(32000);
  l_std_order_sql 			  	varchar2(32000);
  l_counter_order_sql 			varchar2(32000);
  l_other_order_sql 			varchar2(32000);
  l_main_sql					varchar2(32000);
  l_sql2                  		varchar2(32000);
  L_SQL3                  		varchar2(32000);
  L_MAIN_HEADER_SQL      		VARCHAR2(32000);
  l_delete_stmt           VARCHAR2(32000);
  
type organization_rec is record
(	process_id  	number,
	organization_id number
);
  type exception_order_rec is record
  (  process_id 		    	number,
    header_id 		      		number,
    ship_from_org_id 	  		number,
    payment_term_id     		number,
    order_type_id 	    		number,
    sold_to_org_id 	    		number,
    order_number 	      		number,
    ordered_date 	      		date,
    ATTRIBUTE8 		      		VARCHAR2(240),
	  PAYMENT_SET_ID  			NUMBER,
    USER_ID  					NUMBER,
    org_id  number
  );
	
  l_header_sql  varchar2(4000);
  type main_order_rec is record
  (
    process_id 			  number,
    header_id 			  number,
    ship_from_org_id 	number,
    customer_trx_id 	number,
    trx_number 			  varchar2(240),
    trx_date 			    date,
    inv_creation_date date,
    PAYMENT_TERM_ID 	number,
    order_type_id 		number,
    SOLD_TO_ORG_ID 		number,
    order_number 		  number,
    ordered_date 		  date,
    attribute8 			  varchar2(240),
	PAYMENT_SET_ID  		number,
	USER_ID  				number,
  ORG_ID number
  );
  
  type onacc_temp_rec is record 
  (
    PROCESS_ID      number,
    cash_receipt_id number,
    PAYMENT_SET_ID  number
  );
  type onacc_main_rec is record 
  (
  process_id 		    number,
  payment_set_id 	  number,
  on_account 		    number
  );
    type header_data_type is record 
  (
  process_id 		    number,
  header_id     number
  );
  
  
  type exception_order_rec_tab is table of exception_order_rec Index By Binary_Integer;
  type main_order_rec_tab is table of main_order_rec index by binary_integer;
  type onacc_temp_rec_tab is table of onacc_temp_rec index by binary_integer;
  type onacc_main_rec_tab is table of onacc_main_rec index by binary_integer;
  type header_rec_tab is table of header_data_type index by binary_integer;
  type organization_rec_tab is table of organization_rec index by binary_integer;--Added
  
  
  
   standrad_order_tab 	  exception_order_rec_tab;
   counter_order_tab 	  	exception_order_rec_tab;
   others_order_tab 		  exception_order_rec_tab;
   main_order_tab 		    main_order_rec_tab;
   onacc_temp_tab         onacc_temp_rec_tab;
   onacc_main_tab         onacc_main_rec_tab;
   main_header_rec_tab    header_rec_tab;
   organization_tab 		organization_rec_tab; ---Added
   L_REF_CURSOR1          CURSOR_TYPE4;
   L_REF_CURSOR2          CURSOR_TYPE4;
   L_REF_CURSOR3          CURSOR_TYPE4;
   l_ref_cursor4          cursor_type4;
   l_ref_cursor5          cursor_type4;
   L_REF_CURSOR6          CURSOR_TYPE4;
   l_ref_cursor7          cursor_type4;
   L_REF_CURSOR8          CURSOR_TYPE4;
   
   
   type cash_drawn_main_tab
is
  table of XXEIS.EIS_XXWC_CASH_DRAWN_EXCE_TBL%rowtype index by binary_integer;
  cash_drawn_main_rec_tab cash_drawn_main_tab;
  
  
   
   
                                         
  begin
  organization_tab.delete; --added
  standrad_order_tab.delete;
  counter_order_tab.delete;
  others_order_tab.delete;
  MAIN_ORDER_TAB.delete;
  cash_drawn_main_rec_tab.delete;
  --Get Organization id
  l_header_sql :='select process_id,header_id from  xxeis.eis_xxwc_cashexce_main_tab where process_id =:1';

--l_org_sql:= 'select organization_id from mtl_parameters where organization_code=:1';
l_org_sql:= 'select '||p_process_id||' process_id, organization_id from mtl_parameters 
			 where organization_code in ('||XXEIS.EIS_RS_UTILITY.GET_PARAM_VALUES(p_location)||' )';
   
 begin 
    execute immediate l_org_sql into l_orgainzation_id using p_location;
    exception when others then
    null;
    end;
	---Added
	  begin 
    execute immediate l_org_sql  bulk collect into organization_tab;
    exception when others then
    null;
    end;
   --Process Standard Orders 
    begin
    L_STD_ORDER_SQL := 'select /*+INDEX(oe OE_PAYMENTS_U1)*/ '||P_PROCESS_ID ||' process_id,
              l.header_id header_id,
							oh.ship_from_org_id ship_from_org_id,
              oh.PAYMENT_TERM_ID PAYMENT_TERM_ID,
              oh.order_type_id order_type_id,
              oh.SOLD_TO_ORG_ID SOLD_TO_ORG_ID,
              oh.order_number order_number,
              oh.ordered_date ordered_date,
              oh.ATTRIBUTE8	ATTRIBUTE8	,
              oe.payment_set_id payment_set_id,
              oh.created_by user_id,
              OH.ORG_ID ORG_ID			
					from 	oe_order_lines 		l,
							oe_order_headers 	oh,
							oe_payments 		oe
					where oh.header_id =l.header_id
					and oe.header_id (+)=oh.header_id
					and oh.flow_status_code in(''BOOKED'',''CLOSED'')
					and oh.order_type_id =1001
				   	and (ORDERED_ITEM like ''%DEPOSIT%'' or (TRUNC(L.ACTUAL_SHIPMENT_DATE)    <='''||p_as_of_date||''') or (TRUNC(oe.creation_date)    <='''||p_as_of_date||'''))
					and  oh.ship_from_org_id=:1
					and (trunc(oh.ordered_date)<='''||p_as_of_date||''' or trunc(oe.creation_date)<='''||p_as_of_date||''')
				';
				
  
 FOR I IN 1..organization_tab.COUNT LOOP--Added
	--OPEN l_ref_cursor1  FOR l_std_order_sql using l_orgainzation_id ;
	OPEN l_ref_cursor1  FOR l_std_order_sql using organization_tab(i).organization_id ;--Added
		LOOP
			FETCH L_REF_CURSOR1 bulk collect into STANDRAD_ORDER_TAB limit 10000;
		
			  if standrad_order_tab.COUNT  >= 1  then
                  FORALL J IN 1 .. standrad_order_tab.COUNT 
                     
                     INSERT INTO xxeis.eis_xxwc_cash_exce_orders
                          values standrad_order_tab(j);    
         commit;       
       end if; 
       			IF l_ref_cursor1%NOTFOUND Then
				close L_REF_CURSOR1;
				EXIT;
			end if;
		End loop;  
End loop; --Added
	   
     exception when others then
     Fnd_File.Put_Line(FND_FILE.log,'level 1 '||sqlcode||sqlerrm);
     End;


--Removing duplicates from header table eis_xxwc_cash_exce_orders
BEGIN
  l_delete_stmt:= 'DELETE FROM xxeis.EIS_XXWC_CASH_EXCE_ORDERS
   WHERE ROWID IN (
              SELECT rid
                FROM (SELECT ROWID RID,
                             ROW_NUMBER () OVER (PARTITION BY HEADER_ID ORDER BY ROWID) RN
                        FROM xxeis.eis_xxwc_cash_exce_orders where process_id='||P_PROCESS_ID ||')
               WHERE RN <> 1)
   AND PROCESS_ID ='||P_PROCESS_ID ||'';
   
   EXECUTE IMMEDIATE L_DELETE_STMT;
   
--   dbms_output.put_line('l_delete_statement is' ||l_delete_stmt);
   commit;  
        EXCEPTION WHEN OTHERS THEN
     FND_FILE.PUT_LINE(FND_FILE.LOG,'Removing Duplicates '||SQLCODE||SQLERRM);

END;   

   ---Process counter orders
begin
l_counter_order_sql:=   'select '||p_process_id ||' process_id,
								oh.header_id,
								oh.ship_from_org_id,
								oh.PAYMENT_TERM_ID ,
								oh.order_type_id ,	
								oh.SOLD_TO_ORG_ID ,	
								oh.order_number ,	
								oh.ordered_date ,	
								oh.ATTRIBUTE8 ,
								oe.payment_set_id,
								oh.created_by user_id,
								oh.org_id
						from 	oe_order_headers_all oh,
								oe_payments oe
						where oe.header_id (+)=oh.header_id
						and oh.flow_status_code in(''BOOKED'',''CLOSED'')
						and oh.order_type_id=1004
						and oh.ship_from_org_id=:1
						and (trunc(oh.ordered_date)<='''||p_as_of_date||''' or trunc(oe.creation_date)<='''||p_as_of_date||''')
						 and NOT EXISTS
						  (SELECT 1
						  FROM XXWC_OM_CASH_REFUND_TBL XOC
						  where xoc.return_header_id=oh.header_id
						  )';
              
--      dbms_output.put_line ('l_counter_order_sql'||l_counter_order_sql);	          
              
  FOR I IN 1..organization_tab.COUNT LOOP--Added
           
		--OPEN l_ref_cursor2  FOR l_counter_order_sql using l_orgainzation_id ;
		OPEN l_ref_cursor2  FOR l_counter_order_sql using organization_tab(i).organization_id ;-- Added

		LOOP
			FETCH L_REF_CURSOR2 bulk collect into counter_order_tab limit 10000;
			
	
--		dbms_output.put_line ('counter_order_tab'||counter_order_tab.count);	 
		if counter_order_tab.COUNT  >= 1  then
                  FORALL J IN 1 .. counter_order_tab.COUNT 
                     
                     INSERT INTO xxeis.eis_xxwc_cash_exce_orders
                          values counter_order_tab(j);    
             commit;    
       end if; 
       
       IF l_ref_cursor2%NOTFOUND Then
				close L_REF_CURSOR2;
				EXIT;
			END IF;
       	end LOOP;  
End loop; ---Added	
	  
     EXCEPTION
     when others then
     Fnd_File.Put_Line(FND_FILE.log,'level 2 is'||sqlcode||sqlerrm);	 
end;



--Process Other Orders and Not Consider the Standard Orders,Counter Order , Internal and Return Orders .
begin
l_other_order_sql:= 'select /*+INDEX(oe OE_PAYMENTS_U1)*/ '||p_process_id ||' process_id,
							oh.header_id,
							oh.ship_from_org_id,
							oh.PAYMENT_TERM_ID ,
							oh.order_type_id ,	
							oh.SOLD_TO_ORG_ID ,	
							oh.order_number ,	
							oh.ordered_date ,	
							oh.ATTRIBUTE8 ,
							oe.payment_set_id,
							oh.created_by,
              oh.org_id
					 from oe_order_headers_all oh,
						  oe_payments oe
					where oe.header_id (+)=oh.header_id
					and oh.flow_status_code in(''BOOKED'',''CLOSED'')
					and oh.order_type_id not in (1001,1004,1006,1011)
					and oh.ship_from_org_id=:1
					and (trunc(oh.ordered_date)<='''||p_as_of_date||''' or trunc(oe.creation_date)<='''||p_as_of_date||''')';

-- dbms_output.put_line ('l_other_order_sql'||l_other_order_sql);	
 
FOR I IN 1..organization_tab.COUNT LOOP --Added
--	OPEN l_ref_cursor3  FOR l_other_order_sql using l_orgainzation_id ;
OPEN l_ref_cursor3  FOR l_other_order_sql using organization_tab(i).organization_id ;--Added

		LOOP
			FETCH L_REF_CURSOR3 bulk collect into others_order_tab limit 10000;
			if others_order_tab.COUNT  >= 1  then
                  FORALL J IN 1 .. others_order_tab.COUNT 
                     
                     INSERT INTO xxeis.eis_xxwc_cash_exce_orders
                          values others_order_tab(j);    
                 commit;
       end if;
--       dbms_output.put_line ('counter_order_tab'||counter_order_tab.count);	
       			IF l_ref_cursor3%NOTFOUND Then
				close L_REF_CURSOR3;
				EXIT;
			end if;
		End loop;  
	End loop; --Added 
	  
      exception when others then
      Fnd_File.Put_Line(FND_FILE.log,'Level 3'||sqlcode||sqlerrm);
    End;

	 --Check at invoice level and Customer Balance and load final view data.
BEGIN	 
l_main_sql:='select  /*+index(trx XXWC_RA_CUSTOMER_TRX_ALL_N1) index(oh EIS_XXWC_CASHEXCE_N1) */
			oh.process_id,
			oh.header_id,
			oh.ship_from_org_id,
			max(trx.customer_trx_id) customer_trx_id,
			max(trx.trx_number) trx_number,
			max(trx.trx_date) trx_date,
			max(trx.creation_date) INV_CREATION_DATE,
			OH.PAYMENT_TERM_ID,
			OH.order_type_id,
			OH.SOLD_TO_ORG_ID,
			oh.order_number,
			oh.ordered_date,
			OH.ATTRIBUTE8,
			oh.payment_set_id,
			oh.user_id,
      oh.org_id
    from xxeis.eis_xxwc_cash_exce_orders oh,
			   ra_customer_trx trx
    where oh.process_id =:1
      and trx.interface_header_context  =''ORDER ENTRY''
      and trx.interface_header_attribute1 = to_char(oh.order_number)
    AND EXISTS
      (SELECT  /*+INDEX(mv XXWC_AR_CUST_BAL_MV_N1)*/ 1
      FROM xxeis.XXWC_AR_CUSTOMER_BALANCE_MV mv
      where oh.sold_to_org_id = mv.cust_account_id
      )
      and NOT EXISTS
      (SELECT 1
      FROM XXWC_OM_CASH_REFUND_TBL XOC
      WHERE XOC.RETURN_HEADER_ID=OH.HEADER_ID
      )  
       group by 
       oh.process_id,
			oh.header_id,
			oh.ship_from_org_id,
      OH.PAYMENT_TERM_ID,
			OH.order_type_id,
			OH.SOLD_TO_ORG_ID,
			oh.order_number,
			oh.ordered_date,
			OH.ATTRIBUTE8,
				oh.payment_set_id,
			oh.user_id,
      oh.org_id
       ';
       
--        DBMS_OUTPUT.PUT_LINE ('l_main_sql'||L_MAIN_SQL);	
        
 OPEN l_ref_cursor4 FOR l_main_sql using p_process_id ;
		LOOP
			FETCH l_ref_cursor4 bulk collect into main_order_tab limit 10000;

    		if main_order_tab.COUNT  >= 1  then
                  FORALL J IN 1 .. main_order_tab.COUNT 
                     
                     INSERT INTO xxeis.eis_xxwc_cashexce_main_tab
                          values main_order_tab(j);    
                commit;
       end if; 
       			IF l_ref_cursor4%NOTFOUND Then
				close L_REF_CURSOR4;
				EXIT;
			end if;
		end LOOP;  
	   
     exception when others then
      Fnd_File.Put_Line(FND_FILE.log,'Level 4'||sqlcode||sqlerrm);
    End; 

-- Process ON Account Data 
     Begin	 
	 L_SQL2 := 
          'select /*+index(oe OE_PAYMENTS_U1) index(oh EIS_XXWC_CASHEXCE_MAIN_TAB_N1)*/ '||p_process_id ||' process_id, max(cash_receipt_id) cash_receipt_id, 
                  max(oe.payment_set_id) payment_set_id
             FROM 	ar_receivable_applications_all rap1,
					oe_payments oe,
					xxeis.eis_xxwc_cashexce_main_tab oh,
					OE_TRANSACTION_TYPES_VL 	OTL
            WHERE  oh.process_id =:1
            and rap1.payment_set_id = oe.payment_set_id   
              and 	oh.header_id        = oe.header_id
					and oh.order_type_id    	= 1004
					group by cash_receipt_id';
          
--           DBMS_OUTPUT.PUT_LINE ('l_sql2'||l_sql2);
	 l_sql3 :=    'select '||p_process_id ||' process_id,rap2.payment_set_id,sum(nvl(amount_applied,0)) on_account
					from ar_receivable_applications rap,
						 xxeis.EIS_XXWC_CASH_EXCEP_TBL rap2
					where rap2.cash_receipt_id = rap.cash_receipt_id
						and rap.applied_payment_schedule_id = -1
						and rap.display                       = ''Y''
						and rap2.process_id =:1
				   AND NOT EXISTS
						(SELECT 1
						   FROM XXWC_OM_CASH_REFUND_TBL XOC
						  where xoc.cash_receipt_id=rap.cash_receipt_id
						)
				   group by rap2.payment_set_id'; 
	
--  DBMS_OUTPUT.PUT_LINE ('l_sql3'||l_sql3);
  begin
		OPEN l_ref_cursor5  FOR l_sql2 using p_process_id;
      loop
       FETCH L_REF_CURSOR5 bulk collect into ONACC_TEMP_TAB limit 10000;
--        DBMS_OUTPUT.PUT_LINE ('ONACC_TEMP_TAB'||ONACC_TEMP_TAB.count);
             if onacc_temp_tab.count  >= 1  then    
                  FORALL J IN 1 .. onacc_temp_tab.COUNT                      
                     INSERT INTO xxeis.EIS_XXWC_CASH_EXCEP_TBL
                          values onacc_temp_tab(j);   
                 commit;
               end if; 
--               DBMS_OUTPUT.PUT_LINE ('ONACC_TEMP_TAB'||ONACC_TEMP_TAB.count);
            IF l_ref_cursor5%NOTFOUND Then
               close L_REF_CURSOR5;
               EXIT;
            end if;
              
   end loop; 
    exception when others then
    Fnd_File.Put_Line(FND_FILE.log,'Level 5'||sqlcode||sqlerrm);
   end ;
     
  

	--Populating driving Data
  begin
			OPEN l_ref_cursor6  FOR l_sql3 using p_process_id;
      loop
       fetch l_ref_cursor6 bulk collect into onacc_main_tab limit 10000;
       
	  if onacc_main_tab.count  >= 1  then
    
                  FORALL J IN 1 .. onacc_main_tab.COUNT 
                     
                     INSERT INTO xxeis.EIS_XXWC_CASH_EXCEP_TMP_TBL
                          values onacc_main_tab(j);    
              commit;  
       end if;
              IF l_ref_cursor6%NOTFOUND Then
               close L_REF_CURSOR6;
               EXIT;
   END IF;
    End loop;  
    
    exception when others then
    Fnd_File.Put_Line(FND_FILE.log,'Level 6'||sqlcode||sqlerrm);
    end;
 ---Populate Report Data    
   begin 
 L_main_header_sql:=' SELECT ORDER_NUMBER,
  ORG_ID,
  PAYMENT_TYPE_CODE,
  PAYMENT_TYPE_CODE_NEW,
  TAKEN_BY,
  CASH_TYPE,
  CHK_CARDNO,
  CARD_NUMBER,
  CARD_ISSUER_CODE,
  CARD_ISSUER_NAME,
  NVL(CASH_AMOUNT,0) CASH_AMOUNT,
  CUSTOMER_NUMBER,
  CUSTOMER_NAME,
  BRANCH_NUMBER,
  CHECK_NUMBER,
  ORDER_DATE,
  CASH_DATE,
  PAYMENT_CHANNEL_NAME,
  name,
  CASE
    WHEN Payment_Number=1
    THEN NVL(ORDER_AMOUNT,0)+charge_amt
    WHEN Payment_Number IS NULL
    THEN NVL(ORDER_AMOUNT,0)+charge_amt
    ELSE 0
  END ORDER_AMOUNT,
  CASE
    WHEN PAYMENT_TYPE_CODE =''Prism Return''
    THEN 0
    WHEN Payment_Number IS NULL
    OR PAYMENT_NUMBER    =1
      --  THEN NVL(PAYMENT_AMOUNT,0)-(NVL(ORDER_AMOUNT,0)+charge_amt)
      -- THEN (NVL(ORDER_AMOUNT,0)+charge_amt)-NVL(PAYMENT_AMOUNT,0)
    THEN NVL(PAYMENT_AMOUNT,0)
    ELSE 0
  END diff,
  --DECODE(PAYMENT_TYPE_CODE,''Prism Return'',0,NVL(PAYMENT_AMOUNT,0)-NVL(ORDER_AMOUNT,0)) diff,
  PARTY_ID,
  CUST_ACCOUNT_ID,
  INVOICE_NUMBER,
  INVOICE_DATE,
  SEGMENT_NUMBER,
  HEADER_ID,
  ON_ACCOUNT,
  DIST_DATE,
  PROCESS_ID
FROM
  (SELECT XXEIS.EIS_RS_XXWC_COM_UTIL_PKG.GET_CHARGER_AMT(OH.HEADER_ID) charge_amt,
    OH.ORDER_NUMBER,
    oh.org_id,
    CASE
      WHEN ((OE.PAYMENT_TYPE_CODE IN(''CHECK'',''CREDIT_CARD'')
      AND upper(ARC.NAME) LIKE UPPER(''%Card%''))
      OR OH.ATTRIBUTE8 =''CREDIT_CARD'')
      THEN ''Credit Card''
      WHEN ((OE.PAYMENT_TYPE_CODE IN (''CHECK'')
      AND UPPER(ARC.NAME) NOT LIKE UPPER(''%Card%''))
      OR OH.ATTRIBUTE8 = ''CHECK'')
      THEN ''Check''
      WHEN (OE.PAYMENT_TYPE_CODE IN (''CASH'')
      OR OH.ATTRIBUTE8            = ''CASH'')
      THEN ''Cash''
      ELSE NULL
    END PAYMENT_TYPE_CODE,
    CASE
      WHEN OE.PAYMENT_TYPE_CODE IN( ''CHECK'',''CREDIT_CARD'')
      AND upper(ARC.NAME) LIKE UPPER(''%Card%'')
      THEN ''Credit Card''
      WHEN OE.PAYMENT_TYPE_CODE IN (''CHECK'')
      AND UPPER(ARC.NAME) NOT LIKE UPPER(''%Card%'')
      THEN ''Check''
      ELSE olkp.meaning
    END PAYMENT_TYPE_CODE_NEW,
    (SELECT PPF.LAST_NAME
    FROM PER_ALL_PEOPLE_F PPF,
      FND_USER FU
    WHERE FU.USER_ID   =OH.user_id
    AND FU.EMPLOYEE_ID =PPF.PERSON_ID(+)
    AND TRUNC(OH.ORDERED_DATE) BETWEEN NVL(TRUNC(PPF.EFFECTIVE_START_DATE),TRUNC(OH.ORDERED_DATE)) AND NVL(TRUNC(PPF.EFFECTIVE_END_DATE),TRUNC(OH.ORDERED_DATE))
    ) TAKEN_BY,
    --   PPF.LAST_NAME TAKEN_BY,
    CASE
      WHEN OTL.name = ''RETURN ORDER''
      THEN ''PRISM RETURN''
      WHEN OE.PAYMENT_TYPE_CODE IN (''CASH'',''CHECK'',''CREDIT_CARD'')
      AND NOT EXISTS
        (SELECT ORDERED_ITEM
        FROM OE_ORDER_LINES OL
        WHERE OL.HEADER_ID=OH.HEADER_ID
        AND ORDERED_ITEM LIKE ''%DEPOSIT%''
        )
        /*AND NOT EXISTS
        (SELECT header_id
        FROM xxwc_om_cash_refund_tbl re
        WHERE re.RETURN_HEADER_ID=oh.header_id
        )*/
      THEN ''CASH SALES''
      WHEN OE.PAYMENT_TYPE_CODE IN (''CASH'',''CHECK'',''CREDIT_CARD'')
      AND EXISTS
        (SELECT ORDERED_ITEM
        FROM OE_ORDER_LINES OL
        WHERE OL.HEADER_ID=OH.HEADER_ID
        AND ORDERED_ITEM LIKE ''%DEPOSIT%''
        )
        /*AND NOT EXISTS
        (SELECT header_id
        FROM xxwc_om_cash_refund_tbl re
        WHERE re.RETURN_HEADER_ID=oh.header_id
        )*/
      THEN ''DEPOSIT''
    END CASH_TYPE,
    DECODE(OE.PAYMENT_TYPE_CODE,''CHECK'',OE.CHECK_NUMBER,''CREDIT_CARD'',SUBSTR(ITE.CARD_NUMBER,-4)) CHK_CARDNO,
    SUBSTR(ITE.CARD_NUMBER,                                                                  -4) CARD_NUMBER,
    ITE.CARD_ISSUER_CODE,
    ITE.CARD_ISSUER_NAME,
    --  NVL(OE.PAYMENT_AMOUNT,0) CASH_AMOUNT,
    CASE
      WHEN Payment_Number IS NOT NULL
      THEN NVL(OE.PAYMENT_AMOUNT,0)
      ELSE
        (SELECT SUM(ARA.AMOUNT_APPLIED)
        FROM AR_RECEIVABLE_APPLICATIONS ARA,
          RA_CUSTOMER_TRX RCT
        WHERE rct.INTERFACE_header_CONTEXT =''ORDER ENTRY''
        AND RCT.CUSTOMER_TRX_ID            = ARA.APPLIED_CUSTOMER_TRX_ID
        AND ara.display                    =''Y''
        AND RCT.CUSTOMER_TRX_ID            =oh.CUSTOMER_TRX_ID
        )
    END CASH_AMOUNT,
    /* ( select SUM(ARA.AMOUNT_APPLIED) from
    AR_RECEIVABLE_APPLICATIONS  ARA,
    RA_CUSTOMER_TRX             RCT
    where RCT.CUSTOMER_TRX_ID    =  ARA.APPLIED_CUSTOMER_TRX_ID
    and TO_CHAR(OH.ORDER_NUMBER) =  RCT.INTERFACE_HEADER_ATTRIBUTE1
    ) CASH_AMOUNT,*/
    hca.account_number customer_number,
    HZP.PARTY_NAME CUSTOMER_NAME,
    OOD.ORGANIZATION_CODE BRANCH_NUMBER,
    OE.CHECK_NUMBER,
    TRUNC(OH.ORDERED_DATE) ORDER_DATE,
    --  TRUNC(OH.ORDERED_DATE) CASH_DATE,
    -- TRUNC(oe.creation_date) CASH_DATE,
    DECODE(OTL.name ,''RETURN ORDER'',TRUNC(OH.ORDERED_DATE),
    (SELECT MAX(TRUNC(ARA.apply_date))
    FROM AR_RECEIVABLE_APPLICATIONS ARA,
      RA_CUSTOMER_TRX RCT
    WHERE rct.INTERFACE_header_CONTEXT =''ORDER ENTRY''
    AND RCT.CUSTOMER_TRX_ID            = ARA.APPLIED_CUSTOMER_TRX_ID
    AND RCT.CUSTOMER_TRX_ID            =oh.CUSTOMER_TRX_ID
    )) CASH_DATE,
    ITE.PAYMENT_CHANNEL_NAME,
    ARC.name,
    PAYMENT_NUMBER,
    /*(
    CASE
    WHEN Payment_Number IS NULL
    THEN*
    (SELECT SUM(NVL(aps.amount_due_original,0)-NVL(aps.amount_due_remaining,0))
    FROM ar_payment_schedules aps,
    RA_CUSTOMER_TRX RCT
    WHERE rct.INTERFACE_header_CONTEXT =''ORDER ENTRY''
    AND RCT.CUSTOMER_TRX_ID            = aps.CUSTOMER_TRX_ID
    and TO_CHAR(OH.ORDER_NUMBER)       = RCT.INTERFACE_HEADER_ATTRIBUTE1
    ) )PAYMENT_AMOUNT,*/
    -- XXEIS.EIS_RS_XXWC_COM_UTIL_PKG.GET_PAYMENT_AMOUNT(OH.ORDER_NUMBER,OE.PAYMENT_SET_ID,OH.HEADER_ID) PAYMENT_AMOUNT,
    XXEIS.EIS_RS_XXWC_COM_UTIL_PKG.GET_BALANCE_AMOUNT(OH.ORDER_NUMBER,OE.PAYMENT_SET_ID,OH.HEADER_ID)*-1 PAYMENT_AMOUNT,
    CASE
      WHEN (OTL.NAME <> ''STANDARD ORDER''
      OR EXISTS
        (SELECT 1
        FROM OE_ORDER_LINES OL1
        WHERE OL1.header_id = OH.HEADER_ID
        AND OL1.ORDERED_ITEM LIKE ''%DEPOSIT%''
        ))
      THEN
        (SELECT NVL(ROUND(SUM(ROUND(OL.ORDERED_QUANTITY*OL.UNIT_SELLING_PRICE,2)+ NVL(OL.TAX_VALUE,0)),2),0) ORDER_AMOUNT
        FROM OE_ORDER_LINES OL
        WHERE OL.HEADER_ID=OH.HEADER_ID
        )
      WHEN OTL.NAME =''STANDARD ORDER''
      THEN
        (SELECT NVL (ROUND(SUM(ROUND(NVL(DECODE(OTL.name,''BILL ONLY'',OL.fulfilled_quantity,OL.SHIPPED_QUANTITY),0)*OL.UNIT_SELLING_PRICE,2)+ NVL(OL.TAX_VALUE,0)),2),0) ORDER_AMOUNT
        FROM OE_ORDER_LINES OL,
          OE_TRANSACTION_TYPES_VL OTL
        WHERE OL.HEADER_ID                                                                         =OH.HEADER_ID
        AND OL.LINE_TYPE_ID                                                                        = OTL.TRANSACTION_TYPE_ID
        AND NVL(DECODE(OTL.name,''BILL ONLY'',OL.fulfilled_quantity,OL.SHIPPED_QUANTITY),0)         <> 0
        AND ((NVL(TRUNC(OL.ACTUAL_SHIPMENT_DATE),XXEIS.EIS_RS_XXWC_COM_UTIL_PKG.GET_CASH_TO_DATE) <=XXEIS.EIS_RS_XXWC_COM_UTIL_PKG.GET_CASH_TO_DATE)
        OR ((OTL.name                                                                              =''BILL ONLY'')
        AND (NVL(TRUNC(OL.FULFILLMENT_DATE),XXEIS.EIS_RS_XXWC_COM_UTIL_PKG.GET_CASH_TO_DATE)      <=XXEIS.EIS_RS_XXWC_COM_UTIL_PKG.GET_CASH_TO_DATE) ) )
        AND OL.ORDERED_ITEM NOT LIKE ''%DEPOSIT%''
        )
      ELSE 0
    END ORDER_AMOUNT,
    HZP.PARTY_ID,
    HCA.CUST_ACCOUNT_ID,
    oh.trx_number Invoice_Number,
    oh.trx_date INVOICE_DATE,
    OH.INV_CREATION_DATE DIST_DATE,
    (SELECT MAX(GCC.SEGMENT2)
    FROM OE_ORDER_LINES L,
      RA_CUSTOMER_TRX_LINES RCTL,
      RA_CUSTOMER_TRX RCT,
      RA_CUST_TRX_LINE_GL_DIST RCTG,
      GL_CODE_COMBINATIONS_KFV GCC
    WHERE OH.HEADER_ID               =L.HEADER_ID
    AND RCT.INTERFACE_HEADER_CONTEXT =''ORDER ENTRY''
    AND RCT.customer_trx_id          =oh.customer_trx_id
    AND TO_CHAR(OH.ORDER_NUMBER)     =RCTL.INTERFACE_LINE_ATTRIBUTE1
    AND TO_CHAR(L.LINE_ID)           =RCTL.INTERFACE_LINE_ATTRIBUTE6
    AND RCT.CUSTOMER_TRX_ID          =RCTL.CUSTOMER_TRX_ID
    AND RCTL.CUSTOMER_TRX_ID         =RCTG.CUSTOMER_TRX_ID
    AND RCTL.CUSTOMER_TRX_LINE_ID    =RCTG.CUSTOMER_TRX_LINE_ID
    AND rctg.code_combination_id     =gcc.code_combination_id
    )Segment_Number,
    oh.header_id header_id,
    0 cash_receipt_id,
    O.ON_ACCOUNT ON_ACCOUNT,
    oh.process_id
  FROM xxeis.eis_xxwc_cashexce_main_tab oh,
    oe_payments oe,
    xxeis.eis_xxwc_cash_excep_tmp_tbl o,
    oe_lookups olkp,
    hz_parties hzp,
    hz_cust_accounts hca,
    xxeis.xxwc_iby_trxn_extensions_v ite,
    ar_receipt_methods arc,
    oe_transaction_types_vl otl,
    ra_terms ra,
    mtl_parameters ood
  WHERE oe.header_id(+)=oh.header_id
    --and oh.header_id=35799509
  AND OH.PAYMENT_SET_ID              =O.PAYMENT_SET_ID(+)
  AND OH.PROCESS_ID                  =O.PROCESS_ID(+)
  AND oe.payment_type_code           = olkp.lookup_code(+)
  AND oe.payment_collection_event(+) =''PREPAY''
  AND oh.sold_to_org_id              =hca.cust_account_id
  AND hzp.party_id                   =hca.party_id
  AND oe.trxn_extension_id           =ite.trxn_extension_id(+)
  AND (ARC.NAME                     IS NULL
  OR upper(ARC.NAME) NOT LIKE ''%POA%'')
  AND olkp.lookup_type(+)  = ''PAYMENT TYPE''
  AND oe.receipt_method_id =arc.receipt_method_id(+)
  AND oh.order_type_id     = otl.transaction_type_id
  AND (Payment_Number     IS NOT NULL
  OR RA.name              IN (''COD'',''PRFRDCASH''))
  AND ra.attribute1        = ''Y''
  AND oh.payment_term_id   = ra.term_id
  AND OH.SHIP_FROM_ORG_ID  = OOD.ORGANIZATION_ID
  and OH.PROCESS_ID        = :1
  and oh.header_id         = :2
  AND NOT EXISTS
    (SELECT 1
    FROM hz_customer_profiles hcp,
      hz_cust_profile_classes hcpc
    WHERE hca.party_id       = hcp.party_id
    AND hca.cust_account_id  = hcp.cust_account_id
    AND hcp.site_use_id     IS NULL
    AND hcp.profile_class_id = hcpc.profile_class_id(+)
    AND hcpc.name LIKE ''WC%Branches%''
    )
  order by OH.HEADER_ID
  )
where (NVL(PAYMENT_AMOUNT,0)<>0
OR ON_ACCOUNT               <> 0)' ;



open l_ref_cursor8 for l_header_sql using p_process_id ;
		
			fetch l_ref_cursor8 bulk collect into main_header_rec_tab;
      
loop exit when l_ref_cursor8%notfound;
    END LOOP;
close L_REF_CURSOR8;


  FOR I IN 1..main_header_rec_tab.COUNT
  LOOP
  
   OPEN L_REF_CURSOR7 FOR L_main_header_sql USING main_header_rec_tab(i).process_id,
  main_header_rec_tab(i).header_id;
  loop
    fetch l_ref_cursor7 bulk collect into cash_drawn_main_rec_tab limit 10000;
  If cash_drawn_main_rec_tab.COUNT>0
  Then  
    
      FORALL J IN 1..cash_drawn_main_rec_tab.COUNT 
  Insert Into XXEIS.EIS_XXWC_CASH_DRAWN_EXCE_TBL Values cash_drawn_main_rec_tab(J);
  

    END IF;
    exit when L_REF_CURSOR7%notfound;
    END LOOP;
       commit;
    
    close L_REF_CURSOR7;
 END LOOP; 

	        EXCEPTION when OTHERS then
    Fnd_File.Put_Line(FND_FILE.log,'Level 7'||sqlcode||sqlerrm);
end;
		
End;

END PROCESS_CASH_EXCEPTION_ORDERS;
  
PROCEDURE CLEAR_TEMP_TABLES ( P_PROCESS_ID IN NUMBER)
AS
--//============================================================================
--//
--// Object Name         :: clear_temp_tables
--//
--// Object Type         :: Procedure
--//
--// Object Description  :: This procedure will trigger in After Report level and delete all the records from the table based on that particular process_id.
--//
--// Version Control
--//============================================================================
--// Version    Author           Date          Description
--//----------------------------------------------------------------------------
--// 1.0     Siva  	04/12/2016   Initial Build --TMS#20160411-00103 -- Performance tuning
--//============================================================================ 
  BEGIN  
  DELETE FROM XXEIS.EIS_XXWC_CASH_EXCE_ORDERS WHERE PROCESS_ID=P_PROCESS_ID;
  DELETE FROM XXEIS.EIS_XXWC_CASHEXCE_MAIN_TAB WHERE PROCESS_ID=P_PROCESS_ID;
  DELETE FROM XXEIS.EIS_XXWC_CASH_EXCEP_TBL WHERE PROCESS_ID=P_PROCESS_ID;
  delete from XXEIS.EIS_XXWC_CASH_EXCEP_TMP_TBL where PROCESS_ID=P_PROCESS_ID;
  delete from XXEIS.EIS_XXWC_CASH_DRAWN_EXCE_TBL where PROCESS_ID=P_PROCESS_ID;
  COMMIT;
END ;

END EIS_XXWC_CASH_EXCEPTION_PKG;
/
