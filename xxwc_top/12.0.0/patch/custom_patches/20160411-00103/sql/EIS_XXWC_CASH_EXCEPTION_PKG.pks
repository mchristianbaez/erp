CREATE OR REPLACE package   XXEIS.EIS_XXWC_CASH_EXCEPTION_PKG as
 --//============================================================================
--//  
--// Change Request 			:: Performance Issue  
--//
--// Object Usage 				:: This Object Referred by "Cash Drawer Exception Report
--//
--// Object Name         		:: XXEIS.EIS_XXWC_CASH_EXCEPTION_PKG
--//
--// Object Type         		:: Package Specification
--//
--// Object Description  		:: This Package will trigger in before report and insert the values into Table
--//
--// Version Control
--//============================================================================
--// Version    Author           Date          Description
--//----------------------------------------------------------------------------
--// 1.0     Siva  	04/12/2016   Initial Build  --TMS#20160411-00103 -- Performance tuning
--//============================================================================
 type CURSOR_TYPE4 is ref cursor;
procedure process_cash_exception_orders (p_process_id in number,
                                          p_location in  varchar2,
                                          p_as_of_date in date
                                          ) ;
--//============================================================================
--//
--// Object Name         :: process_cash_exception_orders  
--//
--// Object Usage 		 :: This Object Referred by "Cash Drawer Exception Report
--//
--// Object Type         :: Procedure
--//
--// Object Description  :: This  procedure is created based on the EIS_XXWC_CASH_DRAWN_EXCE_V View.Due to the 
--// 						performance issue ,spilt the view definition based on the driving tables and driving data.
--//
--// Version Control
--//============================================================================
--// Version    Author           Date          Description
--//----------------------------------------------------------------------------
--// 1.0     Siva  	04/12/2016   Initial Build  --TMS#20160411-00103  -- Performance tuning
--//============================================================================											  
                                          
procedure clear_temp_tables ( p_process_id in number); 
--//============================================================================
--//
--// Object Name         :: clear_temp_tables
--//
--// Object Type         :: Procedure
--//
--// Object Description  :: This procedure will trigger in After Report level and delete all the records from the table based on that particular process_id.
--//
--// Version Control
--//============================================================================
--// Version    Author           Date          Description
--//----------------------------------------------------------------------------
--// 1.0     Siva  	04/12/2016   Initial Build --TMS#20160411-00103 -- Performance tuning
--//============================================================================                                           
End;
/
