---------------------------------------------------------------------------------------
/*******************************************************************************
  Table: EIS_XXWC_CASH_EXCEP_TMP_TBL
  Description: This table is used to get data from XXEIS.EIS_XXWC_CASH_EXCEPTION_PKG Package.
  HISTORY
  ===============================================================================
  VERSION DATE               AUTHOR(S)       DESCRIPTION
  ------- -----------------  --------------- -----------------------------------------
  1.0     12-Apr-2016        Siva   TMS#20160411-00103  Performance Tuning
********************************************************************************/
CREATE TABLE XXEIS.EIS_XXWC_CASH_EXCEP_TMP_TBL 
   (PROCESS_ID NUMBER, 
	PAYMENT_SET_ID NUMBER, 
	ON_ACCOUNT NUMBER
)
/
