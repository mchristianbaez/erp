---------------------------------------------------------------------------------------
/*******************************************************************************
  Table: EIS_XXWC_CASH_EXCE_ORDERS
  Description: This table is used to get data from XXEIS.EIS_XXWC_CASH_EXCEPTION_PKG Package.
  HISTORY
  ===============================================================================
  VERSION DATE               AUTHOR(S)       DESCRIPTION
  ------- -----------------  --------------- -----------------------------------------
  1.0     12-Apr-2016        Siva   		TMS#20160411-00103 -- Performance Tuning
********************************************************************************/
CREATE TABLE XXEIS.EIS_XXWC_CASH_EXCE_ORDERS 
   ( PROCESS_ID NUMBER, 
	HEADER_ID NUMBER, 
	SHIP_FROM_ORG_ID NUMBER, 
	PAYMENT_TERM_ID NUMBER, 
	ORDER_TYPE_ID NUMBER, 
	SOLD_TO_ORG_ID NUMBER, 
	ORDER_NUMBER NUMBER, 
	ORDERED_DATE DATE, 
	ATTRIBUTE8 VARCHAR2(240 BYTE), 
	PAYMENT_SET_ID NUMBER, 
	USER_ID NUMBER, 
	ORG_ID NUMBER
)
/
