  /* *************************************************************************************************************
  $Header XXEIS.XXWC_VENDOR_QUOTE_TEMP1 $
  Module Name: Order Management
  PURPOSE: This Table is used in a view  xxeis.eis_xxwc_om_vendor_quote_v for Vendor Quote Batch Summary Report
  TMS Task Id : 20150814-00022 
  REVISIONS:
  Ver        Date        Author                Description
  ---------  ----------  ------------------    ----------------
  1.0        11/18/2015  Mahender Reddy         20150814-00022 
  ************************************************************************************************************* */
alter table XXEIS.XXWC_VENDOR_QUOTE_TEMP1 add list_price_per_unit number;
/