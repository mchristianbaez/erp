/*
 TMS: 20160615-00116 Process stuck receiving transactions.
 */
SET SERVEROUTPUT ON SIZE 1000000

BEGIN
   DBMS_OUTPUT.put_line ('Before update1');

   UPDATE rcv_transactions_interface
      SET request_id = NULL,
          processing_request_id = NULL,
          --from_subinventory = 'Xfer Disc',
          subinventory = 'General',
          processing_status_code = 'PENDING',
          transaction_status_code = 'PENDING',
          processing_mode_code = 'BATCH',
          TRANSACTION_DATE = SYSDATE,
          last_update_date = SYSDATE,
          Last_updated_by = 16991,
          GROUP_ID = 12345
    WHERE     processing_status_code = 'ERROR'
          AND transaction_status_code = 'PENDING'
          AND processing_mode_code = 'BATCH'
          AND INTERFACE_TRANSACTION_ID = 9516900
          AND GROUP_ID = 3644364
          AND item_num = '113TC612WL'
          AND SHIPMENT_NUM = '5827941-RETURN';


   DBMS_OUTPUT.put_line ('Records update-' || SQL%ROWCOUNT);
   COMMIT;

   DBMS_OUTPUT.put_line ('Before update2');

   UPDATE rcv_headers_interface
      SET processing_request_id = NULL,
          receipt_header_id = NULL,
          validation_flag = 'Y',
          processing_status_code = 'PENDING',
          GROUP_ID = 12345,
          receipt_num = NULL    -- (ONLY if using Automatic Receipt Numbering)
    WHERE GROUP_ID = 3644364 AND header_interface_id = 3576908;

   DBMS_OUTPUT.put_line ('Records updated-' || SQL%ROWCOUNT);
   COMMIT;
EXCEPTION
   WHEN OTHERS
   THEN
      DBMS_OUTPUT.put_line ('Unable to perform DML action ' || SQLERRM);
END;
/