/*************************************************************************
$Header Update_Matrx_Modifier_Lines_Exclusive.sql $
Module Name: Update_Matrx_Modifier_Lines_Exclusive.sql

PURPOSE:   Script to update Matrx Modifier lines to have Exclusive Incompatibility Group and UOM Code = NULL

REVISIONS:
Ver        Date        Author                  Description
---------  ----------  ---------------         -------------------------
1.0        11/19/2015  Gopi Damuluri           Initial Version
                                               TMS# 20151119-00020
****************************************************************************/

SET SERVEROUTPUT ON SIZE 100000;
SET VERIFY OFF;

DECLARE

      l_MODIFIER_LIST_rec        QP_Modifiers_PUB.Modifier_List_Rec_Type;
      l_MODIFIER_LIST_val_rec    QP_Modifiers_PUB.Modifier_List_Val_Rec_Type;
      l_MODIFIERS_tbl            QP_Modifiers_PUB.Modifiers_Tbl_Type;
      l_MODIFIERS_val_tbl        QP_Modifiers_PUB.Modifiers_Val_Tbl_Type;
      l_QUALIFIERS_tbl           QP_Qualifier_Rules_PUB.Qualifiers_Tbl_Type;
      l_QUALIFIERS_val_tbl       QP_Qualifier_Rules_PUB.Qualifiers_Val_Tbl_Type;
      l_PRICING_ATTR_tbl         QP_Modifiers_PUB.Pricing_Attr_Tbl_Type;
      l_PRICING_ATTR_val_tbl     QP_Modifiers_PUB.Pricing_Attr_Val_Tbl_Type;
      lx_MODIFIER_LIST_rec       QP_Modifiers_PUB.Modifier_List_Rec_Type;
      lx_MODIFIER_LIST_val_rec   QP_Modifiers_PUB.Modifier_List_Val_Rec_Type;
      lx_MODIFIERS_tbl           QP_Modifiers_PUB.Modifiers_Tbl_Type;
      lx_MODIFIERS_val_tbl       QP_Modifiers_PUB.Modifiers_Val_Tbl_Type;
      lx_QUALIFIERS_tbl          QP_Qualifier_Rules_PUB.Qualifiers_Tbl_Type;
      lx_QUALIFIERS_val_tbl      QP_Qualifier_Rules_PUB.Qualifiers_Val_Tbl_Type;
      lx_PRICING_ATTR_tbl        QP_Modifiers_PUB.Pricing_Attr_Tbl_Type;
      lx_PRICING_ATTR_val_tbl    QP_Modifiers_PUB.Pricing_Attr_Val_Tbl_Type;

      l_return_status            VARCHAR2(1);

      l_qualifier_rules_rec       qp_qualifier_rules_pub.Qualifier_Rules_Rec_Type  := QP_QUALIFIER_RULES_PUB.G_MISS_QUALIFIER_RULES_REC;
      lx_qualifier_rules_rec      qp_qualifier_rules_pub.Qualifier_Rules_Rec_Type;
  
      l_qualifier_rules_val_rec   qp_qualifier_rules_pub.Qualifier_Rules_Val_Rec_Type;
      lx_qualifier_rules_val_rec  qp_qualifier_rules_pub.Qualifier_Rules_Val_Rec_Type;

      l_Msg_Data                 Varchar2(2000);
      l_Msg_Count                Number ;
      L_List_Header_ID           Number;
      l_List_Line_Id             Number ;
      l_Pricing_Attribute_ID     Number ;
      l_Qualifier_ID             Number ;
      i                          Number ;
      l_ln_count                 Number ;
      l_ql_count                 NUMBER;
      l_pa_Count                 NUMBER;
      l_sec                      VARCHAR2 (100);

      CURSOR cur_seg_hdr IS
      SELECT list_header_id FROM (
      SELECT distinct list_header_id
        FROM qp_modifier_summary_v qql
       WHERE 1 = 1
         AND qql.incompatibility_grp_code  = 'LVL 1'
         AND EXISTS (SELECT '1'
                       FROM qp_list_headers_all   qlh_s
                      WHERE 1 = 1
                        AND qlh_s.list_header_id = qql.list_header_id
                        AND qlh_s.attribute10    = 'Segmented Price'))
         WHERE ROWNUM < 100;

      CURSOR cur_seg_line(p_list_header_id IN NUMBER) IS
      SELECT *
        FROM qp_modifier_summary_v qql
       WHERE 1 = 1
         AND qql.incompatibility_grp_code  = 'LVL 1'
         AND list_header_id                = p_list_header_id;

      ----------------------------------------------------------
      -- PriceList Attribute - Cursor
      ----------------------------------------------------------
      CURSOR Get_Pricing_Attr_Cur (p_list_header_id IN NUMBER, p_list_line_id IN NUMBER, p_excluder_Flag IN VARCHAR2 )
      IS
      SELECT * 
        FROM qp_pricing_attributes 
       WHERE list_header_id = p_list_header_id
         AND list_line_id   = p_list_line_id
         AND NVL(excluder_Flag,'N') = p_excluder_Flag
         ;

BEGIN

  BEGIN
    mo_global.set_policy_context('S',162);
  END;
  FND_GLOBAL.APPS_INITIALIZE (15986, 50891, 661);

dbms_output.put_line('**********************************************');
dbms_output.put_line('************** Start Of Process **************');
dbms_output.put_line('**********************************************');

  FOR rec_seg_hdr IN cur_seg_hdr LOOP
     l_sec := 'Loop Modifier headers - '||rec_seg_hdr.list_header_id;
     dbms_output.put_line(l_sec);

     l_MODIFIER_LIST_rec                    := QP_MODIFIERS_PUB.G_MISS_MODIFIER_LIST_REC;
     l_QUALIFIERS_tbl                       := QP_MODIFIERS_PUB.G_MISS_QUALIFIERS_TBL;

     l_MODIFIER_LIST_rec.list_header_id     := rec_seg_hdr.list_header_id;
     l_MODIFIER_LIST_rec.operation          := QP_GLOBALS.G_OPR_UPDATE;

     l_ln_count := 0;
     l_MODIFIERS_Tbl                        := QP_MODIFIERS_PUB.G_MISS_MODIFIERS_TBL;
     l_pa_Count := 0;
     l_PRICING_ATTR_Tbl                     := QP_MODIFIERS_PUB.G_MISS_PRICING_ATTR_TBL;

     FOR Mod_Line_Rec IN cur_seg_line(rec_seg_hdr.list_header_id) LOOP
     ----------------------------------------------------------------------------------
     -- Update CSP lines as Exclusions
     ----------------------------------------------------------------------------------
         l_sec := 'Loop Modifier Lines ';
         dbms_output.put_line(l_sec);

         l_ln_count                                               := l_ln_count + 1;

         l_MODIFIERS_tbl(l_Ln_Count).LIST_LINE_ID                 := Mod_Line_Rec.list_line_id;

         If Mod_Line_Rec.MODIFIER_LEVEL_CODE IS NOT NULL Then
            l_MODIFIERS_tbl(l_Ln_Count).MODIFIER_LEVEL_CODE       := Mod_Line_Rec.MODIFIER_LEVEL_CODE ;
         End If;

         If Mod_Line_Rec.LIST_LINE_TYPE_CODE IS NOT NULL Then
            l_MODIFIERS_tbl(l_Ln_Count).LIST_LINE_TYPE_CODE       := Mod_Line_Rec.LIST_LINE_TYPE_CODE;

         End If;

         l_MODIFIERS_tbl(l_Ln_Count).START_DATE_ACTIVE            := Mod_Line_Rec.START_DATE_ACTIVE;
         l_MODIFIERS_tbl(l_Ln_Count).END_DATE_ACTIVE              := Mod_Line_Rec.END_DATE_ACTIVE;

         If Mod_Line_Rec.AUTOMATIC_FLAG IS NOT NULL Then
            l_MODIFIERS_tbl(l_Ln_Count).AUTOMATIC_FLAG            := Mod_Line_Rec.AUTOMATIC_FLAG;
         End If;

         If Mod_Line_Rec.OVERRIDE_FLAG  Is Not NULL Then
            l_MODIFIERS_tbl(l_Ln_Count).OVERRIDE_FLAG             := Mod_Line_Rec.OVERRIDE_FLAG;
         End If;

         If Mod_Line_Rec.PRICING_PHASE_ID Is Not Null Then
            l_MODIFIERS_tbl(l_Ln_Count).PRICING_PHASE_ID          := Mod_Line_Rec.PRICING_PHASE_ID;
         End If;

         If  Mod_Line_Rec.PRODUCT_PRECEDENCE Is Not Null Then
            l_MODIFIERS_tbl(l_Ln_Count).PRODUCT_PRECEDENCE        := Mod_Line_Rec.PRODUCT_PRECEDENCE;
         End If;

         l_MODIFIERS_tbl(l_Ln_Count).PRICE_BREAK_TYPE_CODE        := Mod_Line_Rec.PRICE_BREAK_TYPE_CODE;
         l_MODIFIERS_tbl(l_Ln_Count).ARITHMETIC_OPERATOR          := Mod_Line_Rec.ARITHMETIC_OPERATOR ;
         l_MODIFIERS_tbl(l_Ln_Count).OPERAND                      := Mod_Line_Rec.OPERAND ;
         l_MODIFIERS_tbl(l_Ln_Count).ACCRUAL_FLAG                 := FND_API.G_MISS_CHAR ;
         l_MODIFIERS_tbl(l_Ln_Count).INCOMPATIBILITY_GRP_CODE     := 'EXCL';

         If Mod_Line_Rec.PRICING_GROUP_SEQUENCE  Is Not Null Then
            l_MODIFIERS_tbl(l_Ln_Count).PRICING_GROUP_SEQUENCE    := Mod_Line_Rec.PRICING_GROUP_SEQUENCE ;
         End If;

         If Mod_Line_Rec.INCLUDE_ON_RETURNS_FLAG Is Not Null Then
            l_MODIFIERS_tbl(l_Ln_Count).INCLUDE_ON_RETURNS_FLAG   := Mod_Line_Rec.INCLUDE_ON_RETURNS_FLAG ;
         End If;

         l_MODIFIERS_tbl(l_Ln_Count).PRICE_BY_FORMULA_ID          := Mod_Line_Rec.PRICE_BY_FORMULA_ID ;

         l_MODIFIERS_tbl(l_Ln_Count).operation                  := QP_GLOBALS.G_OPR_UPDATE;   -- G_OPR_CREATE;

         ----------------------------------------------------------------------------------
         -- Create Pricing Attributes - As Exclusions
         ----------------------------------------------------------------------------------

         FOR Pricing_Attr_Rec In Get_Pricing_Attr_Cur (rec_seg_hdr.list_header_id, mod_line_rec.list_line_id, mod_line_rec.excluder_flag) LOOP
             --l_sec := 'Create Pricing Attributes - As Exclusions - '||Pricing_Attr_Rec.PRODUCT_ATTR_VALUE;
             fnd_file.put_line (fnd_file.LOG, l_sec);

             l_pa_Count := l_pa_Count + 1;

             l_PRICING_ATTR_tbl(l_pa_Count).LIST_HEADER_ID            :=  rec_seg_hdr.list_header_id;
             l_PRICING_ATTR_tbl(l_pa_Count).LIST_LINE_ID              :=  Mod_Line_Rec.list_line_id;
             l_PRICING_ATTR_tbl(l_pa_Count).PRICING_ATTRIBUTE_ID      :=  Pricing_Attr_Rec.PRICING_ATTRIBUTE_ID;
             l_PRICING_ATTR_tbl(l_pa_Count).PRODUCT_ATTRIBUTE_CONTEXT :=  Pricing_Attr_Rec.PRODUCT_ATTRIBUTE_CONTEXT ;
             l_PRICING_ATTR_tbl(l_pa_Count).PRODUCT_ATTRIBUTE         :=  Pricing_Attr_Rec.PRODUCT_ATTRIBUTE ;
             l_PRICING_ATTR_tbl(l_pa_Count).PRODUCT_ATTR_VALUE        :=  Pricing_Attr_Rec.PRODUCT_ATTR_VALUE ;
             l_PRICING_ATTR_tbl(l_pa_Count).PRICING_ATTRIBUTE         :=  Pricing_Attr_Rec.PRICING_ATTRIBUTE ;
             l_PRICING_ATTR_tbl(l_pa_Count).PRODUCT_UOM_CODE          :=  NULL;
             l_PRICING_ATTR_tbl(l_pa_Count).COMPARISON_OPERATOR_CODE  :=  Pricing_Attr_Rec.COMPARISON_OPERATOR_CODE ;
             l_PRICING_ATTR_tbl(l_pa_Count).PRICING_ATTRIBUTE_CONTEXT :=  Pricing_Attr_Rec.PRICING_ATTRIBUTE_CONTEXT ;
             l_PRICING_ATTR_tbl(l_pa_Count).PRICING_ATTR_VALUE_FROM   :=  Pricing_Attr_Rec.PRICING_ATTR_VALUE_FROM ;
             l_PRICING_ATTR_tbl(l_pa_Count).PRICING_ATTR_VALUE_TO     :=  Pricing_Attr_Rec.PRICING_ATTR_VALUE_TO ;

             l_PRICING_ATTR_tbl(l_pa_Count).ATTRIBUTE_GROUPING_NO     :=  Pricing_Attr_Rec.ATTRIBUTE_GROUPING_NO;
             l_PRICING_ATTR_tbl(l_pa_Count).Operation                 :=  QP_GLOBALS.G_OPR_UPDATE;
             l_PRICING_ATTR_tbl(l_pa_Count).EXCLUDER_FLAG             :=  Pricing_Attr_Rec.EXCLUDER_FLAG;
             l_PRICING_ATTR_tbl(l_pa_Count).MODIFIERS_INDEX           :=  l_pa_Count;
         END LOOP ;     -- Pricing Attributes
     END LOOP; 

   ----------------------------------------------------------------------------------
   -- Add Modifier Lines
   ----------------------------------------------------------------------------------
--   IF l_Ln_Count > 0 THEN
   QP_Modifiers_PUB.Process_Modifiers
      ( p_api_version_number    => 1.0
      , p_init_msg_list         => FND_API.G_FALSE
      , p_return_values         => FND_API.G_FALSE
      , p_commit                => FND_API.G_TRUE
      , x_return_status         => l_return_status
      , x_msg_count             => l_msg_count
      , x_msg_data              => l_msg_data
      , p_MODIFIER_LIST_rec     => l_MODIFIER_LIST_rec
      , p_MODIFIERS_tbl         => l_MODIFIERS_tbl
      , p_QUALIFIERS_tbl        => l_QUALIFIERS_tbl
      , p_PRICING_ATTR_tbl      => l_PRICING_ATTR_tbl
      , x_MODIFIER_LIST_rec     => lx_MODIFIER_LIST_rec
      , x_MODIFIER_LIST_val_rec => lx_MODIFIER_LIST_val_rec
      , x_MODIFIERS_tbl         => lx_MODIFIERS_tbl
      , x_MODIFIERS_val_tbl     => lx_MODIFIERS_val_tbl
      , x_QUALIFIERS_tbl        => lx_QUALIFIERS_tbl
      , x_QUALIFIERS_val_tbl    => lx_QUALIFIERS_val_tbl
      , x_PRICING_ATTR_tbl      => lx_PRICING_ATTR_tbl
      , x_PRICING_ATTR_val_tbl  => lx_PRICING_ATTR_val_tbl );
   COMMIT;

         IF l_return_status = fnd_api.G_RET_STS_SUCCESS THEN
            COMMIT;
            dbms_output.put_line('Success ');
         ELSE
            -- ROLLBACK;
            COMMIT;
            -- fnd_file.put_line (fnd_file.LOG, 'Error Add Modifier Lines - '|| l_msg_data ||' - ' || l_msg_count);
            fnd_file.put_line (fnd_file.LOG, 'After Error Msg ');
            dbms_output.put_line('Error ');

            --FOR I IN 1 .. l_msg_count LOOP
            --   l_msg_data := oe_msg_pub.get( p_msg_index => i,p_encoded => 'F');
            --   fnd_file.put_line (fnd_file.LOG, 'err msg ' || i ||'is:  ' || l_msg_data);
            --END LOOP;
         END IF;

  END LOOP; -- FOR rec_seg_hdr IN cur_seg_hdr LOOP

dbms_output.put_line('**********************************************');
dbms_output.put_line('************** End Of Process **************');
dbms_output.put_line('**********************************************');
   
EXCEPTION
WHEN OTHERS THEN
  NULL;
END;
/