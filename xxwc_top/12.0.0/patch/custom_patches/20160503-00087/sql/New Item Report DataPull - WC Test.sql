--Report Name            : New Item Report DataPull - WC Test
--Import Instructions    : Run this file in XXEIS user from SQL*Plus or any tool like Toad, SQL Navigator. 
prompt Creating View Data for New Item Report DataPull - WC Test
set scan off define off
DECLARE
BEGIN 
--Inserting View EIS_XXWC_NEW_ITEM_DATA_TEST_V
xxeis.eis_rs_ins.v( 'EIS_XXWC_NEW_ITEM_DATA_TEST_V',401,'','','','','PK059658','XXEIS','Eis Xxwc New Item Data Rpt V','EXNIDRV','','');
--Delete View Columns for EIS_XXWC_NEW_ITEM_DATA_TEST_V
xxeis.eis_rs_utility.delete_view_rows('EIS_XXWC_NEW_ITEM_DATA_TEST_V',401,FALSE);
--Inserting View Columns for EIS_XXWC_NEW_ITEM_DATA_TEST_V
xxeis.eis_rs_ins.vc( 'EIS_XXWC_NEW_ITEM_DATA_TEST_V','ITEM_CREATED_BY',401,'Item Created By','ITEM_CREATED_BY','','','','PK059658','VARCHAR2','','','Item Created By','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_NEW_ITEM_DATA_TEST_V','CREATION_DATE',401,'Creation Date','CREATION_DATE','','','','PK059658','DATE','','','Creation Date','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_NEW_ITEM_DATA_TEST_V','WEIGHT_UOM',401,'Weight Uom','WEIGHT_UOM','','','','PK059658','VARCHAR2','','','Weight Uom','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_NEW_ITEM_DATA_TEST_V','WEIGHT',401,'Weight','WEIGHT','','','','PK059658','NUMBER','','','Weight','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_NEW_ITEM_DATA_TEST_V','SHELF_LIFE_DAYS',401,'Shelf Life Days','SHELF_LIFE_DAYS','','','','PK059658','NUMBER','','','Shelf Life Days','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_NEW_ITEM_DATA_TEST_V','COUNTRY_OF_ORIGIN',401,'Country Of Origin','COUNTRY_OF_ORIGIN','','','','PK059658','VARCHAR2','','','Country Of Origin','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_NEW_ITEM_DATA_TEST_V','HAZMAT_FLAG',401,'Hazmat Flag','HAZMAT_FLAG','','','','PK059658','VARCHAR2','','','Hazmat Flag','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_NEW_ITEM_DATA_TEST_V','SELLING_UOM',401,'Selling Uom','SELLING_UOM','','','','PK059658','VARCHAR2','','','Selling Uom','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_NEW_ITEM_DATA_TEST_V','SUGGESTED_RETAIL_PRICE',401,'Suggested Retail Price','SUGGESTED_RETAIL_PRICE','','','','PK059658','VARCHAR2','','','Suggested Retail Price','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_NEW_ITEM_DATA_TEST_V','XPAR',401,'Xpar','XPAR','','','','PK059658','VARCHAR2','','','Xpar','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_NEW_ITEM_DATA_TEST_V','TIME_SENSITIVE',401,'Time Sensitive','TIME_SENSITIVE','','','','PK059658','VARCHAR2','','','Time Sensitive','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_NEW_ITEM_DATA_TEST_V','CATMGT_SUBCATEGORY_DESC',401,'Catmgt Subcategory Desc','CATMGT_SUBCATEGORY_DESC','','','','PK059658','VARCHAR2','','','Catmgt Subcategory Desc','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_NEW_ITEM_DATA_TEST_V','CATMGT_CATEGORY_DESC',401,'Catmgt Category Desc','CATMGT_CATEGORY_DESC','','','','PK059658','VARCHAR2','','','Catmgt Category Desc','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_NEW_ITEM_DATA_TEST_V','CATEGORY_CLASS',401,'Category Class','CATEGORY_CLASS','','','','PK059658','VARCHAR2','','','Category Class','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_NEW_ITEM_DATA_TEST_V','PO_COST',401,'Po Cost','PO_COST','','','','PK059658','NUMBER','','','Po Cost','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_NEW_ITEM_DATA_TEST_V','VENDOR_TIER',401,'Vendor Tier','VENDOR_TIER','','','','PK059658','VARCHAR2','','','Vendor Tier','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_NEW_ITEM_DATA_TEST_V','VENDOR_NAME',401,'Vendor Name','VENDOR_NAME','','','','PK059658','VARCHAR2','','','Vendor Name','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_NEW_ITEM_DATA_TEST_V','MASTER_VENDOR_#',401,'Master Vendor #','MASTER_VENDOR_#','','','','PK059658','VARCHAR2','','','Master Vendor #','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_NEW_ITEM_DATA_TEST_V','VENDOR_PART_#',401,'Vendor Part #','VENDOR_PART_#','','','','PK059658','VARCHAR2','','','Vendor Part #','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_NEW_ITEM_DATA_TEST_V','PRIMARY_UOM',401,'Primary Uom','PRIMARY_UOM','','','','PK059658','VARCHAR2','','','Primary Uom','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_NEW_ITEM_DATA_TEST_V','ITEM_LEVEL',401,'Item Level','ITEM_LEVEL','','','','PK059658','VARCHAR2','','','Item Level','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_NEW_ITEM_DATA_TEST_V','DESCRIPTION',401,'Description','DESCRIPTION','','','','PK059658','VARCHAR2','','','Description','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_NEW_ITEM_DATA_TEST_V','ITEM_NUM',401,'Item Num','ITEM_NUM','','','','PK059658','VARCHAR2','','','Item Num','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_NEW_ITEM_DATA_TEST_V','ITEM_STATUS',401,'Item Status','ITEM_STATUS','','','','PK059658','VARCHAR2','','','Item Status','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_NEW_ITEM_DATA_TEST_V','ITEM_TYPE',401,'Item Type','ITEM_TYPE','','','','PK059658','VARCHAR2','','','Item Type','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_NEW_ITEM_DATA_TEST_V','LOCATION_NAME',401,'Location Name','LOCATION_NAME','','','','PK059658','VARCHAR2','','','Location Name','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_NEW_ITEM_DATA_TEST_V','APPROVAL_DATE',401,'Approval Date','APPROVAL_DATE','','','','PK059658','DATE','','','Approval Date','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_NEW_ITEM_DATA_TEST_V','APPROVAL_STATUS',401,'Approval Status','APPROVAL_STATUS','','','','PK059658','VARCHAR2','','','Approval Status','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_NEW_ITEM_DATA_TEST_V','APPROVAL_STATUS_TYPE',401,'Approval Status Type','APPROVAL_STATUS_TYPE','','','','PK059658','NUMBER','','','Approval Status Type','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_NEW_ITEM_DATA_TEST_V','CHANGE_ID',401,'Change Id','CHANGE_ID','','','','PK059658','NUMBER','','','Change Id','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_NEW_ITEM_DATA_TEST_V','CHANGE_ORDER_NUMBER',401,'Change Order Number','CHANGE_ORDER_NUMBER','','','','PK059658','VARCHAR2','','','Change Order Number','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_NEW_ITEM_DATA_TEST_V','STATUS_NAME',401,'Status Name','STATUS_NAME','','','','PK059658','VARCHAR2','','','Status Name','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_NEW_ITEM_DATA_TEST_V','STATUS_TYPE',401,'Status Type','STATUS_TYPE','','','','PK059658','NUMBER','','','Status Type','','','');
--Inserting View Components for EIS_XXWC_NEW_ITEM_DATA_TEST_V
--Inserting View Component Joins for EIS_XXWC_NEW_ITEM_DATA_TEST_V
END;
/
set scan on define on
prompt Creating Report Data for New Item Report DataPull - WC Test
set scan off define off
DECLARE
BEGIN 
--Deleting Report data - New Item Report DataPull - WC Test
xxeis.eis_rs_utility.delete_report_rows( 'New Item Report DataPull - WC Test' );
--Inserting Report - New Item Report DataPull - WC Test
xxeis.eis_rs_ins.r( 401,'New Item Report DataPull - WC Test','','The  purpose of this report is to provide a listing of new items(Daily) built into the system to determine if any further action is needed by EH&S, Safety, Pricing, and Category Managers.','','','','PK059658','EIS_XXWC_NEW_ITEM_DATA_TEST_V','Y','','','PK059658','','Y','White Cap Reports','','CSV,Pivot Excel,EXCEL,','N');
--Inserting Report Columns - New Item Report DataPull - WC Test
xxeis.eis_rs_ins.rc( 'New Item Report DataPull - WC Test',401,'CATEGORY_CLASS','Category Class','Category Class','','','','','12','N','','','','','','','','PK059658','N','N','','EIS_XXWC_NEW_ITEM_DATA_TEST_V','','');
xxeis.eis_rs_ins.rc( 'New Item Report DataPull - WC Test',401,'CATMGT_CATEGORY_DESC','Catmgt Category Desc','Catmgt Category Desc','','','','','13','N','','','','','','','','PK059658','N','N','','EIS_XXWC_NEW_ITEM_DATA_TEST_V','','');
xxeis.eis_rs_ins.rc( 'New Item Report DataPull - WC Test',401,'CATMGT_SUBCATEGORY_DESC','Catmgt Subcategory Desc','Catmgt Subcategory Desc','','','','','14','N','','','','','','','','PK059658','N','N','','EIS_XXWC_NEW_ITEM_DATA_TEST_V','','');
xxeis.eis_rs_ins.rc( 'New Item Report DataPull - WC Test',401,'COUNTRY_OF_ORIGIN','Country Of Origin','Country Of Origin','','','','','20','N','','','','','','','','PK059658','N','N','','EIS_XXWC_NEW_ITEM_DATA_TEST_V','','');
xxeis.eis_rs_ins.rc( 'New Item Report DataPull - WC Test',401,'CREATION_DATE','Creation Date','Creation Date','','','','','24','N','','','','','','','','PK059658','N','N','','EIS_XXWC_NEW_ITEM_DATA_TEST_V','','');
xxeis.eis_rs_ins.rc( 'New Item Report DataPull - WC Test',401,'DESCRIPTION','Description','Description','','','','','4','N','','','','','','','','PK059658','N','N','','EIS_XXWC_NEW_ITEM_DATA_TEST_V','','');
xxeis.eis_rs_ins.rc( 'New Item Report DataPull - WC Test',401,'HAZMAT_FLAG','Hazmat Flag','Hazmat Flag','','','','','19','N','','','','','','','','PK059658','N','N','','EIS_XXWC_NEW_ITEM_DATA_TEST_V','','');
xxeis.eis_rs_ins.rc( 'New Item Report DataPull - WC Test',401,'ITEM_CREATED_BY','Item Created By','Item Created By','','','','','25','N','','','','','','','','PK059658','N','N','','EIS_XXWC_NEW_ITEM_DATA_TEST_V','','');
xxeis.eis_rs_ins.rc( 'New Item Report DataPull - WC Test',401,'ITEM_LEVEL','Item Level','Item Level','','','','','5','N','','','','','','','','PK059658','N','N','','EIS_XXWC_NEW_ITEM_DATA_TEST_V','','');
xxeis.eis_rs_ins.rc( 'New Item Report DataPull - WC Test',401,'ITEM_NUM','Item Num','Item Num','','','','','3','N','','','','','','','','PK059658','N','N','','EIS_XXWC_NEW_ITEM_DATA_TEST_V','','');
xxeis.eis_rs_ins.rc( 'New Item Report DataPull - WC Test',401,'ITEM_STATUS','Item Status','Item Status','','','','','2','N','','','','','','','','PK059658','N','N','','EIS_XXWC_NEW_ITEM_DATA_TEST_V','','');
xxeis.eis_rs_ins.rc( 'New Item Report DataPull - WC Test',401,'ITEM_TYPE','Item Type','Item Type','','','','','1','N','','','','','','','','PK059658','N','N','','EIS_XXWC_NEW_ITEM_DATA_TEST_V','','');
xxeis.eis_rs_ins.rc( 'New Item Report DataPull - WC Test',401,'MASTER_VENDOR_#','Master Vendor #','Master Vendor #','','','','','8','N','','','','','','','','PK059658','N','N','','EIS_XXWC_NEW_ITEM_DATA_TEST_V','','');
xxeis.eis_rs_ins.rc( 'New Item Report DataPull - WC Test',401,'PO_COST','Po Cost','Po Cost','','','','','11','N','','','','','','','','PK059658','N','N','','EIS_XXWC_NEW_ITEM_DATA_TEST_V','','');
xxeis.eis_rs_ins.rc( 'New Item Report DataPull - WC Test',401,'PRIMARY_UOM','Primary Uom','Primary Uom','','','','','6','N','','','','','','','','PK059658','N','N','','EIS_XXWC_NEW_ITEM_DATA_TEST_V','','');
xxeis.eis_rs_ins.rc( 'New Item Report DataPull - WC Test',401,'SELLING_UOM','Selling Uom','Selling Uom','','','','','18','N','','','','','','','','PK059658','N','N','','EIS_XXWC_NEW_ITEM_DATA_TEST_V','','');
xxeis.eis_rs_ins.rc( 'New Item Report DataPull - WC Test',401,'SHELF_LIFE_DAYS','Shelf Life Days','Shelf Life Days','','','','','21','N','','','','','','','','PK059658','N','N','','EIS_XXWC_NEW_ITEM_DATA_TEST_V','','');
xxeis.eis_rs_ins.rc( 'New Item Report DataPull - WC Test',401,'SUGGESTED_RETAIL_PRICE','Suggested Retail Price','Suggested Retail Price','','','','','17','N','','','','','','','','PK059658','N','N','','EIS_XXWC_NEW_ITEM_DATA_TEST_V','','');
xxeis.eis_rs_ins.rc( 'New Item Report DataPull - WC Test',401,'TIME_SENSITIVE','Time Sensitive','Time Sensitive','','','','','15','N','','','','','','','','PK059658','N','N','','EIS_XXWC_NEW_ITEM_DATA_TEST_V','','');
xxeis.eis_rs_ins.rc( 'New Item Report DataPull - WC Test',401,'VENDOR_NAME','Vendor Name','Vendor Name','','','','','9','N','','','','','','','','PK059658','N','N','','EIS_XXWC_NEW_ITEM_DATA_TEST_V','','');
xxeis.eis_rs_ins.rc( 'New Item Report DataPull - WC Test',401,'VENDOR_PART_#','Vendor Part #','Vendor Part #','','','','','7','N','','','','','','','','PK059658','N','N','','EIS_XXWC_NEW_ITEM_DATA_TEST_V','','');
xxeis.eis_rs_ins.rc( 'New Item Report DataPull - WC Test',401,'VENDOR_TIER','Vendor Tier','Vendor Tier','','','','','10','N','','','','','','','','PK059658','N','N','','EIS_XXWC_NEW_ITEM_DATA_TEST_V','','');
xxeis.eis_rs_ins.rc( 'New Item Report DataPull - WC Test',401,'WEIGHT','Weight','Weight','','','','','22','N','','','','','','','','PK059658','N','N','','EIS_XXWC_NEW_ITEM_DATA_TEST_V','','');
xxeis.eis_rs_ins.rc( 'New Item Report DataPull - WC Test',401,'WEIGHT_UOM','Weight Uom','Weight Uom','','','','','23','N','','','','','','','','PK059658','N','N','','EIS_XXWC_NEW_ITEM_DATA_TEST_V','','');
xxeis.eis_rs_ins.rc( 'New Item Report DataPull - WC Test',401,'XPAR','Xpar','Xpar','','','','','16','N','','','','','','','','PK059658','N','N','','EIS_XXWC_NEW_ITEM_DATA_TEST_V','','');
xxeis.eis_rs_ins.rc( 'New Item Report DataPull - WC Test',401,'LOCATION_NAME','Location Name','Location Name','','','','','26','N','','','','','','','','PK059658','N','N','','EIS_XXWC_NEW_ITEM_DATA_TEST_V','','');
xxeis.eis_rs_ins.rc( 'New Item Report DataPull - WC Test',401,'APPROVAL_DATE','Approval Date','Approval Date','','','','','27','N','','','','','','','','PK059658','N','N','','EIS_XXWC_NEW_ITEM_DATA_TEST_V','','');
--Inserting Report Parameters - New Item Report DataPull - WC Test
--Inserting Report Conditions - New Item Report DataPull - WC Test
--Inserting Report Sorts - New Item Report DataPull - WC Test
xxeis.eis_rs_ins.rs( 'New Item Report DataPull - WC Test',401,'ITEM_TYPE','ASC','PK059658','1','');
xxeis.eis_rs_ins.rs( 'New Item Report DataPull - WC Test',401,'CREATION_DATE','ASC','PK059658','2','');
xxeis.eis_rs_ins.rs( 'New Item Report DataPull - WC Test',401,'ITEM_NUM','ASC','PK059658','3','');
--Inserting Report Triggers - New Item Report DataPull - WC Test
--Inserting Report Templates - New Item Report DataPull - WC Test
--Inserting Report Portals - New Item Report DataPull - WC Test
--Inserting Report Dashboards - New Item Report DataPull - WC Test
--Inserting Report Security - New Item Report DataPull - WC Test
xxeis.eis_rs_ins.rsec( 'New Item Report DataPull - WC Test','20005','','50900',401,'PK059658','','');
xxeis.eis_rs_ins.rsec( 'New Item Report DataPull - WC Test','401','','50884',401,'PK059658','','');
xxeis.eis_rs_ins.rsec( 'New Item Report DataPull - WC Test','401','','50883',401,'PK059658','','');
xxeis.eis_rs_ins.rsec( 'New Item Report DataPull - WC Test','661','','50891',401,'PK059658','','');
xxeis.eis_rs_ins.rsec( 'New Item Report DataPull - WC Test','20005','','51713',401,'PK059658','','');
xxeis.eis_rs_ins.rsec( 'New Item Report DataPull - WC Test','401','','50990',401,'PK059658','','');
xxeis.eis_rs_ins.rsec( 'New Item Report DataPull - WC Test','20005','','51711',401,'PK059658','','');
xxeis.eis_rs_ins.rsec( 'New Item Report DataPull - WC Test','201','','50892',401,'PK059658','','');
xxeis.eis_rs_ins.rsec( 'New Item Report DataPull - WC Test','20005','','50897',401,'PK059658','','');
--Inserting Report Pivots - New Item Report DataPull - WC Test
END;
/
set scan on define on
