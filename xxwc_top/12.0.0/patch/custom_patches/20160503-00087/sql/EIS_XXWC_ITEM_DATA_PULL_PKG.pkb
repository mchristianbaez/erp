create or replace 
package BODY       XXEIS.EIS_XXWC_ITEM_DATA_PULL_PKG
is
--//============================================================================
--//  
--// Change Request 			:: Performance Issue  
--//
--// Object Usage 				:: This Object Referred by "New Item Report DataPull - WC"
--//
--// Object Name         		:: XXEIS.EIS_XXWC_ITEM_DATA_PULL_PKG
--//
--// Object Type         		:: Package Specification
--//
--// Object Description  		:: This Package programs will call in EIS_XXWC_NEW_ITEM_DATA_RPT_V view to populate data
--//
--// Version Control
--//============================================================================
--// Version    Author           Date          Description
--//----------------------------------------------------------------------------
--// 1.0        Siva  	      18-may-2016       Initial Build TMS#20160503-00087
--//============================================================================
FUNCTION get_vendor_part_num(p_inventory_item_id number) RETURN varchar2
IS
--//============================================================================
--//
--// Object Name         :: get_vendor_part_num  
--//
--// Object Usage 		 :: This Object Referred by "New Item Report DataPull - WC"
--//
--// Object Type         :: FUNCTION
--//
--// Object Description  :: This function gets the record based on parameter and insert into table.
--//
--// Version Control
--//============================================================================
--// Version    Author           Date          Description
--//----------------------------------------------------------------------------
--// 1.0         Siva        	18-May-2016       Initial Build 
--//============================================================================
L_HASH_INDEX number;
L_SQL varchar2(32000);
begin
g_vendor_part_num := NULL;

l_sql:='SELECT  MAX (cross_reference) cross_reference
               FROM inv.mtl_cross_references_b
              where CROSS_REFERENCE_TYPE = ''VENDOR''
              and inventory_item_id = :1
           GROUP BY inventory_item_id';
    BEGIN
        l_hash_index:=p_inventory_item_id;
        g_vendor_part_num :=g_vendor_part_VLDN_TBL(L_HASH_INDEX);
 exception
    when no_data_found
    THEN
    BEGIN      
    EXECUTE IMMEDIATE L_SQL INTO g_vendor_part_num USING p_inventory_item_id;
    EXCEPTION WHEN NO_DATA_FOUND THEN
    g_vendor_part_num :=null;
    WHEN OTHERS THEN
    g_vendor_part_num :=null;
    END;      
                      l_hash_index:=p_inventory_item_id;
                       g_vendor_part_VLDN_TBL(L_HASH_INDEX) := g_vendor_part_num;
    END;
     return  g_vendor_part_num;
     EXCEPTION WHEN OTHERS THEN
      g_vendor_part_num:=null;
      RETURN  g_vendor_part_num;

end get_vendor_part_num;

FUNCTION GET_VENDOR_DETAILS (P_SEGMENT1 IN VARCHAR2,P_REQUEST_TYPE IN VARCHAR2)
 RETURN VARCHAR2 IS
--//============================================================================
--//
--// Object Name         :: GET_VENDOR_DETAILS  
--//
--// Object Usage 		 :: This Object Referred by "New Item Report DataPull - WC"
--//
--// Object Type         :: FUNCTION
--//
--// Object Description  :: This function gets the record based on parameter and insert into table.
--//
--// Version Control
--//============================================================================
--// Version    Author           Date          Description
--//----------------------------------------------------------------------------
--// 1.0         Siva        	18-May-2016       Initial Build 
--//============================================================================ 
L_HASH_INDEX VARCHAR2(200);
l_sql varchar2(32000);
BEGIN
G_VENDOR_NAME	  := NULL; 
G_VENDOR_TIER	  := NULL;

L_SQL :='select APS.VENDOR_NAME,
       APS.ATTRIBUTE3 VENDOR_TIER
	   from apps.ap_suppliers aps
	   where APS.SEGMENT1 = :1'; 

	   BEGIN
         L_HASH_INDEX:=P_SEGMENT1;
        if P_request_type='VENDOR_NAME' then
            G_VENDOR_NAME := G_VENDOR_NAME_VLDN_TBL(L_HASH_INDEX);
        ELSif P_request_type='VENDOR_TIER' then
            G_VENDOR_TIER := G_VENDOR_TIER_VLDN_TBL(L_HASH_INDEX);
        END IF;

    exception
    when no_data_found
    THEN
    begin      
    EXECUTE IMMEDIATE L_SQL INTO G_VENDOR_NAME,G_VENDOR_TIER USING P_SEGMENT1;
    EXCEPTION WHEN NO_DATA_FOUND THEN
	if P_request_type='VENDOR_NAME' then
    G_VENDOR_NAME := null;
	elsif P_request_type='VENDOR_TIER' then
    G_VENDOR_TIER := null;
	end if;
    end;
                       l_hash_index:=P_SEGMENT1;
                       G_VENDOR_NAME_VLDN_TBL(L_HASH_INDEX) := G_VENDOR_NAME;
					   G_VENDOR_TIER_VLDN_TBL(L_HASH_INDEX):= G_VENDOR_TIER;					   
					   
    end;
	   IF P_REQUEST_TYPE='VENDOR_NAME' THEN
        return  G_VENDOR_NAME ;
        ELSIF P_REQUEST_TYPE='VENDOR_TIER' THEN
		return   G_VENDOR_TIER ;
       END IF;
    EXCEPTION WHEN OTHERS THEN
     IF P_REQUEST_TYPE='VENDOR_NAME' THEN
	    G_VENDOR_NAME := null;
        return  G_VENDOR_NAME ;
        ELSIF P_REQUEST_TYPE='VENDOR_TIER' THEN
		G_VENDOR_TIER := null;
		return   G_VENDOR_TIER ;
       END IF;

end GET_VENDOR_DETAILS;   
	   
FUNCTION get_time_sensitive_cat(p_inventory_item_id number) RETURN varchar2
IS
--//============================================================================
--//
--// Object Name         :: get_time_sensitive_cat  
--//
--// Object Usage 		 :: This Object Referred by "New Item Report DataPull - WC"
--//
--// Object Type         :: FUNCTION
--//
--// Object Description  :: This function gets the record based on parameter and insert into table.
--//
--// Version Control
--//============================================================================
--// Version    Author           Date          Description
--//----------------------------------------------------------------------------
--// 1.0         Siva        	18-May-2016       Initial Build 
--//============================================================================
L_HASH_INDEX number;
L_SQL varchar2(32000);
begin
g_time_sensitive_cat := NULL;

l_sql:='SELECT MCK.CONCATENATED_SEGMENTS
			FROM MTL_ITEM_CATEGORIES MICS ,
				 MTL_CATEGORIES_B_KFV MCK
		WHERE   MICS.CATEGORY_ID     = MCK.CATEGORY_ID
			AND CATEGORY_SET_ID        = 1100000124  
			AND MICS.INVENTORY_ITEM_ID = :1
			AND MICS.ORGANIZATION_ID   = 222
			AND ROWNUM                 = 1';
    BEGIN
        l_hash_index:= p_inventory_item_id;
        g_time_sensitive_cat :=G_TIME_SENSITIVE_VLDN_TBL(L_HASH_INDEX);
 exception
    when no_data_found
    THEN
    BEGIN      
    EXECUTE IMMEDIATE L_SQL INTO g_time_sensitive_cat USING p_inventory_item_id;
    EXCEPTION WHEN NO_DATA_FOUND THEN
    g_time_sensitive_cat :=null;
    WHEN OTHERS THEN
    g_time_sensitive_cat :=null;
    END;      
                      l_hash_index:=p_inventory_item_id;
                       G_TIME_SENSITIVE_VLDN_TBL(L_HASH_INDEX) := g_time_sensitive_cat;
    END;
     return  g_time_sensitive_cat;
     EXCEPTION WHEN OTHERS THEN
      g_time_sensitive_cat:=null;
      RETURN  g_time_sensitive_cat;

end get_time_sensitive_cat;
	   
	   
FUNCTION GET_LOCATION_NAME(p_employee_id number) RETURN varchar2
IS
--//============================================================================
--//
--// Object Name         :: GET_LOCATION_NAME  
--//
--// Object Usage 		 :: This Object Referred by "New Item Report DataPull - WC"
--//
--// Object Type         :: FUNCTION
--//
--// Object Description  :: This function gets the record based on parameter and insert into table.
--//
--// Version Control
--//============================================================================
--// Version    Author           Date          Description
--//----------------------------------------------------------------------------
--// 1.0         Siva        	18-May-2016       Initial Build 
--//============================================================================
L_HASH_INDEX number;
L_SQL varchar2(32000);
begin
g_location_code := NULL;

l_sql:='SELECT hrl.location_code
	FROM apps.per_people_f ppf,
		 apps.PER_ASSIGNMENTS_F per,
		 apps.hr_locations_all hrl
	where PER.PERSON_ID        	   = PPF.PERSON_ID
		AND ppf.person_id          = :1
		AND ppf.effective_end_date > SYSDATE
		AND PER.LOCATION_ID        = HRL.LOCATION_ID
		AND ROWNUM                 = 1';
    BEGIN
        l_hash_index:= p_employee_id;
        g_location_code :=G_LOC_CODE_VLDN_TBL(L_HASH_INDEX);
 exception
    when no_data_found
    THEN
    BEGIN      
    EXECUTE IMMEDIATE L_SQL INTO g_location_code USING p_employee_id;
    EXCEPTION WHEN NO_DATA_FOUND THEN
    g_location_code :=null;
    WHEN OTHERS THEN
    g_location_code :=null;
    END;      
                      l_hash_index:=p_employee_id;
                       G_LOC_CODE_VLDN_TBL(L_HASH_INDEX) := g_location_code;
    END;
     return  g_location_code;
     EXCEPTION WHEN OTHERS THEN
      g_location_code:=null;
      RETURN  g_location_code;

end GET_LOCATION_NAME;	   

FUNCTION GET_APPROVAL_DATE(p_inventory_item_id number,p_organization_id number) RETURN date
IS
--//============================================================================
--//
--// Object Name         :: GET_APPROVAL_DATE  
--//
--// Object Usage 		 :: This Object Referred by "New Item Report DataPull - WC"
--//
--// Object Type         :: FUNCTION
--//
--// Object Description  :: This function gets the record based on parameter and insert into table.
--//
--// Version Control
--//============================================================================
--// Version    Author           Date          Description
--//----------------------------------------------------------------------------
--// 1.0         Siva        	18-May-2016       Initial Build 
--//============================================================================
L_HASH_INDEX Varchar2(200);
L_SQL varchar2(32000);
begin
G_APPROVAL_DATE := NULL;

l_sql:='SELECT eec.approval_date
  FROM apps.eng_engineering_changes eec,
    apps.eng_change_subjects ecs1
  WHERE eec.change_id = ecs1.change_id
  AND ecs1.pk1_value  = :1
  AND ECS1.PK2_VALUE  = :2';
    BEGIN
        l_hash_index:= p_inventory_item_id||'-'||p_organization_id;
        G_APPROVAL_DATE :=G_APPROVAL_DATE_VLDN_TBL(L_HASH_INDEX);
 exception
    when no_data_found
    THEN
    BEGIN      
    EXECUTE IMMEDIATE L_SQL INTO G_APPROVAL_DATE USING p_inventory_item_id,p_organization_id;
    EXCEPTION WHEN NO_DATA_FOUND THEN
    G_APPROVAL_DATE := null;
    WHEN OTHERS THEN
    G_APPROVAL_DATE := null;
    END;      
                      l_hash_index:= p_inventory_item_id||'-'||p_organization_id;
                       G_APPROVAL_DATE_VLDN_TBL(L_HASH_INDEX) := G_APPROVAL_DATE;
    END;
     return  G_APPROVAL_DATE;
     EXCEPTION WHEN OTHERS THEN
      G_APPROVAL_DATE:= null;
      RETURN  G_APPROVAL_DATE;

end GET_APPROVAL_DATE;

FUNCTION GET_ITEM_CATALOG_GROUP(p_item_catalog_group_id number) RETURN varchar2
IS
--//============================================================================
--//
--// Object Name         :: GET_ITEM_CATALOG_GROUP  
--//
--// Object Usage 		 :: This Object Referred by "New Item Report DataPull - WC"
--//
--// Object Type         :: FUNCTION
--//
--// Object Description  :: This function gets the record based on parameter and insert into table.
--//
--// Version Control
--//============================================================================
--// Version    Author           Date          Description
--//----------------------------------------------------------------------------
--// 1.0         Siva        	18-May-2016       Initial Build 
--//============================================================================
L_HASH_INDEX number;
L_SQL varchar2(32000);
begin
G_catalog_group := NULL;

l_sql:='SELECT micg.segment1
  FROM apps.mtl_item_catalog_groups_b micg
  WHERE  micg.item_catalog_group_id = :1
  AND ROWNUM                      = 1';
    BEGIN
        l_hash_index:= p_item_catalog_group_id;
        G_catalog_group :=G_CATLOG_GROUP_VLDN_TBL(L_HASH_INDEX);
 exception
    when no_data_found
    THEN
    BEGIN      
    EXECUTE IMMEDIATE L_SQL INTO G_catalog_group USING p_item_catalog_group_id;
    EXCEPTION WHEN NO_DATA_FOUND THEN
    G_catalog_group :=null;
    WHEN OTHERS THEN
    G_catalog_group :=null;
    END;      
                      l_hash_index:= p_item_catalog_group_id;
                       G_CATLOG_GROUP_VLDN_TBL(L_HASH_INDEX) := G_catalog_group;
    END;
     return  G_catalog_group;
     EXCEPTION WHEN OTHERS THEN
      G_catalog_group:=null;
      RETURN  G_catalog_group;

end GET_ITEM_CATALOG_GROUP;
	   
end EIS_XXWC_ITEM_DATA_PULL_PKG;
/
