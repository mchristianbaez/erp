---------------------------------------------------------------------------------------------------------------------------
/*************************************************************************************************************************
  $Header XXWC_MTL_CATEGORIES_B_N7
  File Name: XXWC_MTL_CATEGORIES_B_N7.sql
  PURPOSE:   
  REVISIONS:
     Ver        Date         Author         Description
     ---------  ----------   --------    -----------------------------------------------------------------------------------
     1.0        16-May-2016  Venu        --TMS#20160503-00087 New Item Report DataPull - WC Program Running for longer time
****************************************************************************************************************************/
CREATE INDEX INV.XXWC_MTL_CATEGORIES_B_N7 ON INV.MTL_CATEGORIES_B (ATTRIBUTE6) TABLESPACE APPS_TS_TX_DATA 
/
