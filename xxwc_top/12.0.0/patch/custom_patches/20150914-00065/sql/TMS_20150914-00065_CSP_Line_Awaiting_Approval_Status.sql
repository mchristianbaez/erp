/*
 TMS: 20150914-00065 
 Date: 08/01/2015
 Notes: CSP Set lines to awaiting approval status
*/

SET SERVEROUTPUT ON SIZE 100000;
SET VERIFY OFF;



      UPDATE xxwc.xxwc_om_contract_pricing_lines cpl
         SET line_status='AWAITING_APPROVAL',
            revision_number=revision_number +1,
             interfaced_flag = 'N',
             latest_rec_flag='Y',
             last_update_date=sysdate,
             last_updated_by=5200
             WHERE cpl.agreement_id=17272 and nvl(end_date, trunc(sysdate+1))> trunc(sysdate);
		
		        
        update xxwc.xxwc_om_contract_pricing_hdr
        set AGREEMENT_STATUS='AWAITING_APPROVAL',
        revision_number=revision_number +1
        where agreement_id=17272;

commit;

/