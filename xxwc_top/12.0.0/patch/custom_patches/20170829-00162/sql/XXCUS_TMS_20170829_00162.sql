/*
 TMS:  20170829-00162
 Date: 08/30/2017
 Notes:  Run jdr_utils.list_contents
*/
SET SERVEROUTPUT ON SIZE 1000000
SPOOL /xx_iface/ebsqa/outbound/TMS_20170829_00162.log
DECLARE
 --
 n_loc number :=0;
 --
BEGIN --Main Processing...
   --
   n_loc :=101;
   --
   jdr_utils.listContents('/oracle/apps/ap/oie',true);
   --
EXCEPTION
   WHEN OTHERS
   THEN
      ROLLBACK;
      DBMS_OUTPUT.put_line (
         'TMS:  20170605-00270, Errors =' || SQLERRM);
END;
/
