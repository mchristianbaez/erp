CREATE OR REPLACE PACKAGE BODY APPS.XXWC_TAXWARE_TAX_PKG
AS
   g_distro_list   VARCHAR2 (75) DEFAULT 'HDSOracleDevelopers@hdsupply.com';

   /********************************************************************************************************************************
   -- Package XXWC_TAXWARE_TAX_PKG
   -- ******************************************************************************************************************************
   --
   -- PURPOSE: Package is to caliculate tax
   -- HISTORY
   -- ==============================================================================================================================
   -- ==============================================================================================================================
   -- VERSION DATE          AUTHOR(S)       DESCRIPTION
   -- ------- -----------   --------------- ----------------------------------------------------------------------------------------
   -- 1.0     05-Mar-2017   P.Vamshidhar    TMS#20170306-00204  - Tax  - Custom Wrapper Package to Caliculate Order Line tax .
                                            Package Created.
   *********************************************************************************************************************************/
   PROCEDURE caliculate_tax (p_tax40_grossamt          IN     NUMBER, --  := '123.00';
                             p_tax40_calctype          IN     VARCHAR2, --  := 'G';
                             p_tax40_numitems          IN     NUMBER, --  := 1;
                             p_tax40_prodcode          IN     VARCHAR2, --  := 'PCODE';
                             p_tax40_invoicedate       IN     DATE, --  := '02-MAR-2017';
                             p_tax40_companyid         IN     VARCHAR2, --  := '01';
                             p_tax40_custno            IN     VARCHAR2, --  := '11381000';
                             p_tax40_invoiceno         IN     VARCHAR2, --  := 'ABCDE';
                             p_tax40_usenexproind      IN     VARCHAR2, --  := 'Y';
                             p_tax40_critflg           IN     VARCHAR2, --  := 'R';
                             p_tax40_usestep           IN     VARCHAR2, --  := 'Y';
                             p_tax40_stepprocflg       IN     NUMBER, --  := 1;
                             p_Jur40_ShipFr_State      IN     VARCHAR2, --  := 'NY';
                             p_Jur40_ShipFr_City       IN     VARCHAR2, --  := 'NEW YORK';
                             p_Jur40_ShipFr_Zip        IN     VARCHAR2, --  := '10001';
                             p_Jur40_ShipFr_Geo        IN     VARCHAR2, --  := '00';
                             p_Jur40_ShipTo_State      IN     VARCHAR2, --  := 'NY';
                             p_Jur40_ShipTo_City       IN     VARCHAR2, --  := 'NEW YORK';
                             p_Jur40_ShipTo_Zip        IN     VARCHAR2, --  := '10001';
                             p_Jur40_ShipTo_Geo        IN     VARCHAR2, --  := '00';
                             p_Jur40_POA_State         IN     VARCHAR2, --  := 'NY';
                             p_Jur40_POA_City          IN     VARCHAR2, --  := 'NEW YORK';
                             p_Jur40_POA_Zip           IN     VARCHAR2, --  := '10001';
                             p_Jur40_POA_Geo           IN     VARCHAR2, --  := '00';
                             p_Jur40_POO_State         IN     VARCHAR2, --  := 'NY';
                             p_Jur40_POO_City          IN     VARCHAR2, --  := 'NEW YORK';
                             p_Jur40_POO_Zip           IN     VARCHAR2, --  := '10001';
                             p_Jur40_POO_Geo           IN     VARCHAR2, --  := '00';
                             x_return_status              OUT VARCHAR2,
                             x_return_mess                OUT VARCHAR2,
                             x_federal_tax_amt            OUT NUMBER,
                             x_state_tax_amt              OUT NUMBER,
                             x_county_tax_amt             OUT NUMBER,
                             x_local_tax_amt              OUT NUMBER,
                             x_second_st_tax_amt          OUT NUMBER,
                             x_second_county_tax_amt      OUT NUMBER,
                             X_second_local_tax_amt       OUT NUMBER,
                             x_fedtxrate                  OUT NUMBER,
                             x_sttx_rate                  OUT NUMBER,
                             x_cntxrate                   OUT NUMBER,
                             x_lotxrate                   OUT NUMBER,
                             x_scsttxrate                 OUT NUMBER,
                             x_sccntxrate                 OUT NUMBER,
                             x_sclotxrate                 OUT NUMBER)
   IS
      SelParm     TAXWARE.TAXPKG_GEN.SELPARMTYP%TYPE;
      Ora40       TAXWARE.TAXPKG_GEN.T_ORAPARM;
      Tax40       TAXWARE.TAXPKG_GEN.TAXPARM;
      Jur40       TAXWARE.TAXPKG_GEN.JURPARM;
      ireturn     BOOLEAN;
      return_ok   VARCHAR2 (10);
      l_calling   VARCHAR2 (100);
   BEGIN
      BEGIN
         MO_GLOBAL.SET_POLICY_CONTEXT ('S', 162);

         fnd_global.APPS_INITIALIZE (user_id        => 15986,
                                     resp_id        => 50857,
                                     resp_appl_id   => 660);
      END;


      DBMS_OUTPUT.put_line ('Initializing Tax Link structure');
      SelParm := 3;
      -- Ora40.OracleID Reserved for use by Oracle Financials only!!!!
      -- Make sure this is a negative number for custom
      -- interfaces!!
      Ora40.OracleID := -565555;
      Ora40.Oracle_Msg_Text := NULL;
      Ora40.Oracle_Msg_Label := NULL;
      Ora40.Taxware_Msg_Text := NULL;
      Ora40.Reserved_Text_1 := NULL;
      Ora40.Reserved_Text_2 := NULL;
      Ora40.Reserved_Text_3 := NULL;
      Ora40.Reserved_BOOL_1 := NULL;
      Ora40.Reserved_BOOL_2 := NULL;
      Ora40.Reserved_BOOL_3 := NULL;
      Ora40.Reserved_CHAR_1 := NULL;
      Ora40.Reserved_CHAR_2 := NULL;
      Ora40.Reserved_CHAR_3 := NULL;
      Ora40.Reserved_NUM_1 := NULL;
      Ora40.Reserved_NUM_2 := NULL;
      Ora40.Reserved_BIGNUM_1 := NULL;
      Ora40.Reserved_DATE_1 := NULL;

      Taxware.Taxpkg_gen.Stepinstalled := TRUE;
      Tax40.CountryCode := NULL;
      Tax40.StateCode := NULL;
      Tax40.PriZip := NULL;
      Tax40.PriGeo := NULL;
      Tax40.PriZipExt := NULL;
      Tax40.SecZip := NULL;
      Tax40.SecGeo := NULL;
      Tax40.SecZipExt := NULL;
      Tax40.CntyCode := NULL;
      Tax40.CntyName := NULL;
      Tax40.LoclName := NULL;
      Tax40.SecCntyCode := NULL;
      Tax40.SecCntyName := NULL;
      Tax40.SecCityName := NULL;
      Tax40.JurLocTp := NULL;
      Tax40.GrossAmt := p_tax40_grossamt;                          --'123.00';
      Tax40.TaxAmt := NULL;
      Tax40.FedExemptAmt := NULL;
      Tax40.StExemptAmt := NULL;
      Tax40.CntyExemptAmt := NULL;
      Tax40.CityExemptAmt := NULL;
      Tax40.DistExemptAmt := NULL;
      Tax40.SecStExemptAmt := NULL;
      Tax40.SecCnExemptAmt := NULL;
      Tax40.SecLoExemptAmt := NULL;
      Tax40.ContractAmt := NULL;
      Tax40.InstallAmt := NULL;
      Tax40.FrghtAmt := NULL;
      Tax40.DiscountAmt := NULL;
      Tax40.CalcType := 'G';
      Tax40.NumItems := 1;     -- Very important Field in (AZ,CN,MO,NC,OK,TN).
      Tax40.ProdCode := 'PCODE';
      Tax40.BasisPerc := NULL;
      Tax40.MovementCode := NULL;
      Tax40.StorageCode := NULL;
      Tax40.ProdCodeConv := 'Y';                                       -- Y/N.
      Tax40.ProdCodeType := NULL;
      Tax40.FedSlsUse := NULL;
      Tax40.StaSlsUse := 'S';
      Tax40.CnSlsUse := 'S';
      Tax40.LoSlsUse := 'S';
      Tax40.SecStSlsUse := 'S';
      Tax40.SecCnSlsUse := 'S';
      Tax40.SecLoSlsUse := 'S';
      Tax40.DistSlsUse := NULL;
      Tax40.FedOvAmt := NULL;
      Tax40.InvoiceDate := p_tax40_invoicedate;
      Tax40.FiscalDate := NULL;
      Tax40.DeliveryDate := NULL;
      Tax40.CustNo := p_tax40_custno;
      Tax40.CustName := NULL;
      Tax40.AFEWorkOrd := NULL;
      Tax40.InvoiceNo := p_tax40_invoiceno;
      Tax40.InvoiceLineNo := NULL;
      Tax40.PartNumber := NULL;
      Tax40.InOutCityLimits := NULL;
      Tax40.FedReasonCode := NULL;
      Tax40.StReasonCode := NULL;
      Tax40.CntyReasonCode := NULL;
      Tax40.CityReasonCode := NULL;
      Tax40.FedTaxCertNo := NULL;
      Tax40.StTaxCertNo := NULL;
      Tax40.CnTaxCertNo := NULL;
      Tax40.LoTaxCertNo := NULL;
      Tax40.FromState := NULL;
      Tax40.CompanyID := '01';
      Tax40.DivCode := NULL;
      Tax40.MiscInfo := NULL;
      Tax40.LocnCode := NULL;
      Tax40.CostCenter := NULL;
      Tax40.CurrencyCd1 := NULL;
      Tax40.CurrencyCd2 := NULL;
      Tax40.CurrConvFact := NULL;
      Tax40.UseNexproInd := 'Y';
      Tax40.AudFileType := NULL;
      Tax40.GenCmplCd := NULL;
      Tax40.FedCmplCd := NULL;
      Tax40.StaCmplCd := NULL;
      Tax40.CnCmplCd := NULL;
      Tax40.LoCmplCd := NULL;
      Tax40.ScStCmplCd := NULL;
      Tax40.ScCnCmplCd := NULL;
      Tax40.ScLoCmplCd := NULL;
      Tax40.DistCmplCd := NULL;
      Tax40.ExtraCmplCd1 := NULL;
      Tax40.ExtraCmplCd2 := NULL;
      Tax40.ExtraCmplCd3 := NULL;
      Tax40.ExtraCmplCd4 := NULL;
      Tax40.FedTxAmt := NULL;
      Tax40.StaTxAmt := NULL;
      Tax40.CnTxAmt := NULL;
      Tax40.LoTxAmt := NULL;
      Tax40.ScCnTxAmt := NULL;
      Tax40.ScLoTxAmt := NULL;
      Tax40.ScStTxAmt := NULL;
      Tax40.DistTxAmt := NULL;
      Tax40.FedTxRate := NULL;
      Tax40.StaTxRate := NULL;
      Tax40.CnTxRate := NULL;
      Tax40.LoTxRate := NULL;
      Tax40.ScCnTxRate := NULL;
      Tax40.ScLoTxRate := NULL;
      Tax40.ScStTxRate := NULL;
      Tax40.DistTxRate := NULL;
      Tax40.FedBasisAmt := NULL;
      Tax40.StBasisAmt := NULL;
      Tax40.CntyBasisAmt := NULL;
      Tax40.CityBasisAmt := NULL;
      Tax40.ScStBasisAmt := NULL;
      Tax40.ScCntyBasisAmt := NULL;
      Tax40.ScCityBasisAmt := NULL;
      Tax40.DistBasisAmt := NULL;
      Tax40.FedOvAmt := NULL;
      Tax40.FedOvAmt := NULL;
      Tax40.FedOvPer := NULL;
      Tax40.StOvAmt := NULL;
      Tax40.StOvPer := NULL;
      Tax40.CnOvAmt := NULL;
      Tax40.CnOvPer := NULL;
      Tax40.LoOvAmt := NULL;
      Tax40.LoOvPer := NULL;
      Tax40.ScCnOvAmt := NULL;
      Tax40.ScCnOvPer := NULL;
      Tax40.ScLoOvAmt := NULL;
      Tax40.ScLoOvPer := NULL;
      Tax40.ScStOvAmt := NULL;
      Tax40.ScStOvPer := NULL;
      Tax40.DistOvAmt := NULL;
      Tax40.DistOvPer := NULL;
      Tax40.JobNo := NULL;
      Tax40.CritFlg := 'R';
      Tax40.UseStep := 'Y';
      Tax40.StepProcFlg := 1;
      Tax40.FedStatus := NULL;
      Tax40.StaStatus := NULL;
      Tax40.CnStatus := NULL;
      Tax40.LoStatus := NULL;
      Tax40.FedComment := NULL;
      Tax40.StComment := NULL;
      Tax40.CnComment := NULL;
      Tax40.LoComment := NULL;
      Tax40.Volume := NULL;
      Tax40.VolExp := NULL;
      Tax40.UOM := NULL;
      Tax40.BillToCustName := NULL;
      Tax40.BillToCustId := NULL;
      Tax40.OptFiles := NULL;
      Tax40.EndInvoiceInd := FALSE;
      Tax40.ShortLoNameInd := FALSE;
      Tax40.GenInd := FALSE;
      Tax40.InvoiceSumInd := FALSE;
      Tax40.DropShipInd := FALSE;
      Tax40.ExtraInd1 := FALSE;
      Tax40.ExtraInd2 := FALSE;
      Tax40.ExtraInd3 := FALSE;
      Tax40.ReptInd := FALSE;                -- Will write to the audit table.
      Tax40.CreditInd := FALSE;
      Tax40.RoundInd := FALSE;
      Tax40.NoTaxInd := FALSE;
      Tax40.Exempt := FALSE;
      Tax40.NoFedTax := FALSE;
      Tax40.NoCnTax := FALSE;
      Tax40.NoStaTax := FALSE;
      Tax40.NoLoTax := FALSE;
      Tax40.NoSecCnTax := FALSE;
      Tax40.NoSecLoTax := FALSE;
      Tax40.NoSecStTax := FALSE;
      Tax40.NoDistTax := FALSE;
      Tax40.FedExempt := FALSE;
      Tax40.StaExempt := FALSE;
      Tax40.CnExempt := FALSE;
      Tax40.LoExempt := FALSE;
      Tax40.SecStExempt := FALSE;
      Tax40.SecCnExempt := FALSE;
      Tax40.SecLoExempt := FALSE;
      Tax40.DistExempt := FALSE;

      --      DBMS_OUTPUT.put_line ('Initializing Jur Link structure');

      Jur40.ShipFr.Country := NULL;
      Jur40.ShipFr.State := p_Jur40_ShipFr_State;
      Jur40.ShipFr.Cnty := NULL;
      Jur40.ShipFr.City := p_Jur40_ShipFr_City;
      Jur40.ShipFr.Zip := p_Jur40_ShipFr_Zip;
      Jur40.ShipFr.Geo := p_Jur40_ShipFr_Geo;
      Jur40.ShipFr.ZipExt := NULL;
      Jur40.ShipTo.Country := NULL;
      Jur40.ShipTo.State := p_Jur40_ShipTo_State;
      Jur40.ShipTo.Cnty := NULL;
      Jur40.ShipTo.City := p_Jur40_ShipTo_City;
      Jur40.ShipTo.Zip := p_Jur40_ShipTo_Zip;
      Jur40.ShipTo.Geo := p_Jur40_ShipTo_Geo;
      Jur40.ShipTo.ZipExt := NULL;
      Jur40.POA.Country := NULL;
      Jur40.POA.State := p_Jur40_POA_State;
      Jur40.POA.Cnty := NULL;
      Jur40.POA.City := p_Jur40_POA_City;
      Jur40.POA.Zip := p_Jur40_POA_Zip;
      Jur40.POA.Geo := p_Jur40_POA_Geo;
      Jur40.POA.ZipExt := NULL;
      Jur40.POO.Country := NULL;
      Jur40.POO.State := p_Jur40_POO_State;
      Jur40.POO.Cnty := NULL;
      Jur40.POO.City := p_Jur40_POO_City;
      Jur40.POO.Zip := p_Jur40_POO_Zip;
      Jur40.POO.Geo := p_Jur40_POO_Geo;
      Jur40.POO.ZipExt := NULL;
      Jur40.BillTo.Country := NULL;
      Jur40.BillTo.State := NULL;
      Jur40.BillTo.Cnty := NULL;
      Jur40.BillTo.City := NULL;
      Jur40.BillTo.Zip := NULL;
      Jur40.BillTo.Geo := NULL;
      Jur40.BillTo.ZipExt := NULL;
      Jur40.POT := 'D';
      Jur40.ServInd := NULL;
      Jur40.InOutCiLimShTo := NULL;
      Jur40.InOutCiLimShFr := NULL;
      Jur40.InOutCiLimPOO := NULL;
      Jur40.InOutCiLimPOA := NULL;
      Jur40.InOutCiLimBiTo := NULL;
      Jur40.PlaceBusnShTo := NULL;
      Jur40.PlaceBusnShFr := NULL;
      Jur40.PlaceBusnPOO := NULL;
      Jur40.PlaceBusnPOA := NULL;
      Jur40.JurLocType := NULL;
      Jur40.JurState := NULL;
      Jur40.JurCity := NULL;
      Jur40.JurZip := NULL;
      Jur40.JurGeo := NULL;
      Jur40.JurZipExt := NULL;
      Jur40.TypState := NULL;
      Jur40.TypCnty := NULL;
      Jur40.TypCity := NULL;
      Jur40.TypDist := NULL;
      Jur40.SecCity := NULL;
      Jur40.SecZip := NULL;
      Jur40.SecGeo := NULL;
      Jur40.SecZipExt := NULL;
      Jur40.SecCounty := NULL;
      Jur40.TypFed := NULL;
      Jur40.TypSecState := NULL;
      Jur40.TypSecCnty := NULL;
      Jur40.TypSecCity := NULL;
      Jur40.ReturnCode := NULL;
      Jur40.POOJurRC := NULL;
      Jur40.POAJurRC := NULL;
      Jur40.ShpToJurRC := NULL;
      Jur40.ShpFrJurRC := NULL;
      Jur40.BillToJurRC := NULL;
      Jur40.EndLink := NULL;
      --DBMS_OUTPUT.put_line ('Calling Tax010');
      l_calling := 'Calling Tax010';
      ireturn :=
         taxware.taxpkg_10.TAXFN_Tax010 (Ora40,
                                         Tax40,
                                         SelParm,
                                         Jur40);

      IF ireturn = TRUE
      THEN
         return_ok := 'SUCCESS';
      ELSE
         return_ok := 'FAILURE';
      END IF;

      /*
         DBMS_OUTPUT.put_line ('Return Status: ' || return_ok);
         DBMS_OUTPUT.put_line ('************ Location Information *****');
         DBMS_OUTPUT.put_line ('State Name: ' || Tax40.StateCode);
         DBMS_OUTPUT.put_line ('County Name:  ' || tax40.CntyName);
         DBMS_OUTPUT.put_line ('Local Name:  ' || tax40.loclname);
         DBMS_OUTPUT.put_line ('Secondary county name:  ' || tax40.SecCntyName);
         DBMS_OUTPUT.put_line ('Secondary local Name:  ' || tax40.SecCityName);
         DBMS_OUTPUT.put_line ('Geocode: ' || tax40.PriGeo);
         DBMS_OUTPUT.put_line ('Zipcode:  ' || tax40.PriZip);
         DBMS_OUTPUT.put_line ('County Code: ' || Tax40.CntyCode);
         DBMS_OUTPUT.put_line ('************ MISC Information *********');
         DBMS_OUTPUT.put_line ('Company ID = ' || Tax40.CompanyID);
         DBMS_OUTPUT.put_line ('Business Location = ' || Tax40.LocnCode);
         DBMS_OUTPUT.put_line ('Division Code = ' || Tax40.DivCode);
         DBMS_OUTPUT.put_line ('Customer ID = ' || Tax40.CustNo);
         DBMS_OUTPUT.put_line ('Job Number = ' || tax40.JobNo);
         DBMS_OUTPUT.put_Line ('************* Amounts *****************');
         DBMS_OUTPUT.put_line ('Gross Amt: ' || TO_CHAR (tax40.GrossAmt));
         DBMS_OUTPUT.put_line ('Tax Amt: ' || TO_CHAR (tax40.TaxAmt));
         DBMS_OUTPUT.put_line ('Basis %: ' || TO_CHAR (Tax40.BasisPerc));
         DBMS_OUTPUT.put_Line ('************* Tax Amounts *************');
         DBMS_OUTPUT.put_line ('Federal: ' || TO_CHAR (tax40.FedTxAmt));
         DBMS_OUTPUT.put_line ('State: ' || TO_CHAR (tax40.StaTxAmt));
         DBMS_OUTPUT.put_line ('County: ' || TO_CHAR (tax40.CnTxAmt));
         DBMS_OUTPUT.put_line ('Local: ' || TO_CHAR (tax40.LoTxAmt));
         DBMS_OUTPUT.put_line ('Secondary State: ' || TO_CHAR (tax40.ScStTxAmt));
         DBMS_OUTPUT.put_line ('Secondary county: ' || TO_CHAR (tax40.ScCnTxAmt));
         DBMS_OUTPUT.put_line ('Secondary local: ' || TO_CHAR (tax40.ScLoTxAmt));

      DBMS_OUTPUT.put_line ('************* Rates *******************');
      DBMS_OUTPUT.put_line ('Federal: ' || TO_CHAR (tax40.FedTxRate));
      DBMS_OUTPUT.put_line ('State: ' || TO_CHAR (tax40.StaTxRate));
      DBMS_OUTPUT.put_line ('County: ' || TO_CHAR (tax40.CnTxRate));
      DBMS_OUTPUT.put_line ('Local: ' || TO_CHAR (tax40.LoTxRate));
      DBMS_OUTPUT.put_line ('Secondary State: ' || TO_CHAR (tax40.ScStTxRate));
      DBMS_OUTPUT.put_line ('Secondary County: ' || TO_CHAR (tax40.ScCnTxRate));
      DBMS_OUTPUT.put_line ('Secondary Local: ' || TO_CHAR (tax40.ScLoTxRate));
      DBMS_OUTPUT.put_line ('************* Exemptions ***************');
      DBMS_OUTPUT.put_line ('Federal: ' || TO_CHAR (tax40.FedExemptAmt));
      DBMS_OUTPUT.put_line ('State: ' || TO_CHAR (tax40.StExemptAmt));
      DBMS_OUTPUT.put_line ('County: ' || TO_CHAR (tax40.CntyExemptAmt));
      DBMS_OUTPUT.put_line ('Local: ' || TO_CHAR (tax40.CityExemptAmt));
      DBMS_OUTPUT.put_line ('Sec State: ' || TO_CHAR (Tax40.SecStExemptAmt));
      DBMS_OUTPUT.put_line ('Sec Cnty: ' || TO_CHAR (Tax40.SecCnExemptAmt));
      DBMS_OUTPUT.put_line ('Sec Locl: ' || TO_CHAR (Tax40.SecLoExemptAmt));
      DBMS_OUTPUT.put_line ('************* Overrides ***************');
      DBMS_OUTPUT.put_line (
         'Federal override Amnt: ' || TO_CHAR (Tax40.FedOvAmt));
      DBMS_OUTPUT.put_line ('Federal override %: ' || TO_CHAR (Tax40.FedOvPer));
      DBMS_OUTPUT.put_line ('State override Amnt: ' || TO_CHAR (Tax40.StOvAmt));
      DBMS_OUTPUT.put_line ('State override %: ' || TO_CHAR (Tax40.StOvPer));
      DBMS_OUTPUT.put_line ('Cnty override Amnt: ' || TO_CHAR (Tax40.CnOvAmt));
      DBMS_OUTPUT.put_line ('Cnty override %: ' || TO_CHAR (Tax40.CnOvPer));
      DBMS_OUTPUT.put_line ('Local override Amnt: ' || TO_CHAR (Tax40.LoOvAmt));
      DBMS_OUTPUT.put_line ('Local override %: ' || TO_CHAR (Tax40.LoOvPer));
      DBMS_OUTPUT.put_line (
         'SC Cnty override Amnt: ' || TO_CHAR (Tax40.ScCnOvAmt));
      DBMS_OUTPUT.put_line ('SC Cnty override %: ' || TO_CHAR (Tax40.ScCnOvPer));
      DBMS_OUTPUT.put_line (
         'SC Local override Amnt: ' || TO_CHAR (Tax40.ScLoOvAmt));
      DBMS_OUTPUT.put_line (
         'SC Local override %: ' || TO_CHAR (Tax40.ScLoOvPer));
      DBMS_OUTPUT.put_line (
         'SC State override Amnt: ' || TO_CHAR (Tax40.ScStOvAmt));
      DBMS_OUTPUT.put_line (
         'SC State override %: ' || TO_CHAR (Tax40.ScStOvPer));
      DBMS_OUTPUT.put_line ('Dist override Amnt: ' || TO_CHAR (Tax40.DistOvAmt));
      DBMS_OUTPUT.put_line ('Dist override %: ' || TO_CHAR (Tax40.DistOvPer));
      DBMS_OUTPUT.put_line ('*********** completion codes ************');
      DBMS_OUTPUT.put_line (
         'Gen Comp Code = ' || TAXWARE.TAXPKG_10.TaxLink.GenCmplCd);
      DBMS_OUTPUT.put_line (
         'Fed Comp Code = ' || TAXWARE.TAXPKG_10.TaxLink.FedCmplCd);
      DBMS_OUTPUT.put_line (
         'State Comp Code = ' || TAXWARE.TAXPKG_10.TaxLink.StaCmplCd);
      DBMS_OUTPUT.put_line (
         'Cnty Comp Code = ' || TAXWARE.TAXPKG_10.TaxLink.CnCmplCd);
      DBMS_OUTPUT.put_line (
         'Locl Comp Code = ' || TAXWARE.TAXPKG_10.TaxLink.LoCmplCd);
      DBMS_OUTPUT.put_line ('Secondary State = ' || Tax40.ScStCmplCd);
      DBMS_OUTPUT.put_line (
         'Secondary Cnty Comp Code = ' || TAXWARE.TAXPKG_10.TaxLink.ScCnCmplCd);
      DBMS_OUTPUT.put_line (
         'Secondary Locl Comp Code = ' || TAXWARE.TAXPKG_10.TaxLink.ScLoCmplCd);

      IF (Tax40.StateCode = 'TX') OR (Tax40.StateCode = '44')
      THEN
         DBMS_OUTPUT.put_line ('****** TEXAS Completion Codes ********');
         DBMS_OUTPUT.put_line (
            'Extra Compl Code 3 = ' || TAXWARE.TAXPKG_10.TaxLink.ExtraCmplCd3);
         DBMS_OUTPUT.put_line (
            'Extra Compl Code 4 = ' || TAXWARE.TAXPKG_10.TaxLink.ExtraCmplCd4);
      END IF;

      IF (Tax40.UseStep = 'Y')
      THEN
         DBMS_OUTPUT.put_line ('****** Step Completion Codes ************');
         DBMS_OUTPUT.put_line (
            'FedStat  = ' || TAXWARE.TAXPKG_10.TaxLink.FedStatus);
         DBMS_OUTPUT.put_line (
            'StaStatus = ' || TAXWARE.TAXPKG_10.TaxLink.StaStatus);
         DBMS_OUTPUT.put_line (
            'CnStatus = ' || TAXWARE.TAXPKG_10.TaxLink.CnStatus);
         DBMS_OUTPUT.put_line (
            'LoStatus = ' || TAXWARE.TAXPKG_10.TaxLink.LoStatus);
         DBMS_OUTPUT.put_line (
            'FedComment = ' || TAXWARE.TAXPKG_10.TaxLink.FedComment);
         DBMS_OUTPUT.put_line (
            'StComment = ' || TAXWARE.TAXPKG_10.TaxLink.StComment);
         DBMS_OUTPUT.put_line (
            'CnComment = ' || TAXWARE.TAXPKG_10.TaxLink.CnComment);
         DBMS_OUTPUT.put_line (
            'LoComment = ' || TAXWARE.TAXPKG_10.TaxLink.LoComment);
         DBMS_OUTPUT.put_line ('****** Step Certificates Numbers *********');
         DBMS_OUTPUT.put_line ('Fed Cert Number = ' || Tax40.FedTaxCertNo);
         DBMS_OUTPUT.put_line ('St Cert Number = ' || Tax40.StTaxCertNo);
         DBMS_OUTPUT.put_line ('Cn Cert Number = ' || Tax40.CnTaxCertNo);
         DBMS_OUTPUT.put_line ('Lo Cert Number = ' || Tax40.LoTaxCertNo);
      END IF;

      DBMS_OUTPUT.put_line ('************** Product Code **************');
      DBMS_OUTPUT.put_line ('Product Code = ' || Tax40.ProdCode);
      DBMS_OUTPUT.put_line ('*********** FLAGS AND INDICATORS *********');
      DBMS_OUTPUT.put_line ('Use NexPro Ind = ' || Tax40.UseNexproInd);
      DBMS_OUTPUT.put_line ('Use Step Ind = ' || Tax40.UseStep);
      DBMS_OUTPUT.put_line ('Crit Flag = ' || Tax40.CritFlg);
      DBMS_OUTPUT.put_line ('*************** Reason Codes *************');
      DBMS_OUTPUT.put_line (
         'State reason code = ' || TAXWARE.TAXPKG_10.TaxLink.StReasonCode);
      DBMS_OUTPUT.put_line (
         'County reason code = ' || TAXWARE.TAXPKG_10.TaxLink.CntyReasonCode);
      DBMS_OUTPUT.put_line (
         'City reason code = ' || TAXWARE.TAXPKG_10.TaxLink.CityReasonCode);
      DBMS_OUTPUT.put_line ('**************** Tax Types ***************');
      DBMS_OUTPUT.put_line (
         'State Sales/Use = ' || TAXWARE.TAXPKG_10.TaxLink.StaSlsUse);
      DBMS_OUTPUT.put_line (
         'County Sales/Use = ' || TAXWARE.TAXPKG_10.TaxLink.CnSlsUse);
      DBMS_OUTPUT.put_line (
         'City Sales/Use = ' || TAXWARE.TAXPKG_10.TaxLink.LoSlsUse);
      DBMS_OUTPUT.put_line (
         'Sec. State Sales/Use = ' || TAXWARE.TAXPKG_10.TaxLink.SecStSlsUse);
      DBMS_OUTPUT.put_line (
         'Sec. County Sales/Use = ' || TAXWARE.TAXPKG_10.TaxLink.SecCnSlsUse);
      DBMS_OUTPUT.put_line (
         'Sec. City Sales/Use = ' || TAXWARE.TAXPKG_10.TaxLink.SecLoSlsUse);
   */

      x_federal_tax_amt := tax40.FedTxAmt;
      x_state_tax_amt := tax40.StaTxAmt;
      x_county_tax_amt := tax40.CnTxAmt;
      x_local_tax_amt := tax40.LoTxAmt;
      x_second_st_tax_amt := tax40.ScStTxAmt;
      x_second_county_tax_amt := tax40.ScCnTxAmt;
      X_second_local_tax_amt := tax40.ScLoTxAmt;

      x_fedtxrate := NVL (tax40.FedTxRate, 0) * 100;
      x_sttx_rate := NVL (tax40.StaTxRate, 0) * 100;
      x_cntxrate := NVL (tax40.CnTxRate, 0) * 100;
      x_lotxrate := NVL (tax40.LoTxRate, 0) * 100;
      x_scsttxrate := NVL (tax40.ScStTxRate, 0) * 100;
      x_sccntxrate := NVL (tax40.ScCnTxRate, 0) * 100;
      x_sclotxrate := NVL (tax40.ScLoTxRate, 0) * 100;
      x_return_status := return_ok;
   EXCEPTION
      WHEN OTHERS
      THEN
         return_ok := 'FAILURE';
         x_return_mess := SUBSTR (SQLERRM, 1, 240);
         x_return_status := return_ok;
         xxcus_error_pkg.xxcus_error_main_api (
            p_called_from         => 'XXWC_TAXWARE_TAX_PKG.CALICULATE_TAX',
            p_calling             => l_calling,
            p_request_id          => -1,
            p_ora_error_msg       => SUBSTR (
                                          'Error_Stack...'
                                       || DBMS_UTILITY.format_error_stack ()
                                       || 'Error_Backtrace...'
                                       || DBMS_UTILITY.format_error_backtrace (),
                                       1,
                                       2000),
            p_error_desc          => SUBSTR (SQLERRM, 1, 240),
            p_distribution_list   => g_distro_list,
            p_module              => 'XXWC');
   END;
END XXWC_TAXWARE_TAX_PKG;
/
GRANT EXECUTE ON APPS.XXWC_TAXWARE_TAX_PKG TO INTERFACE_DF;
/