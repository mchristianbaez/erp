/******************************************************************************
  $Header TMS_20180322_00370_Data_Fix.sql $
  Module Name:Data Fix script for 20180322_00370

  PURPOSE: Data fix script for 20180322-00370 Update Order

  REVISIONS:
  Ver        Date         Author                Description
  ---------  -----------  ------------------    -------------------------------
  1.0        10-Apr-2018  Krishna Kumar         20180322-00370 

*******************************************************************************/

ALTER SESSION SET CURRENT_SCHEMA=apps;
/
BEGIN
   mo_global.set_policy_context ('S', '162');
END;
/
SET serveroutput ON SIZE 500000;
DECLARE

BEGIN

UPDATE 	apps.oe_order_headers_all 
SET 	flow_status_code='CANCELLED',
	open_flag='N'
WHERE 	order_number = 27827679;

dbms_output.put_line('Number of Rows Updated '||SQL%ROWCOUNT);

COMMIT;

EXCEPTION
    WHEN OTHERS THEN
      ROLLBACK;
      dbms_output.put_line(substr(sqlerrm, 1, 240));
END;
/