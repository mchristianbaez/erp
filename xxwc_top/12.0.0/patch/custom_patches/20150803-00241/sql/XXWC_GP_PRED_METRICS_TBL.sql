/********************************************************************************
FILE NAME: XXWC_GP_PRED_METRICS_TBL.sql
PROGRAM TYPE: sql

PURPOSE: to update org id 162 in XXWC.XXWC_GETPAID_PRED_METRICS_TBL 

HISTORY
===============================================================================
VERSION DATE          AUTHOR(S)       DESCRIPTION
------- -----------   --------------- -----------------------------------------
1.0     08/04/2015   Maharajan Shunmugam    TMS#20150803-00241 Initial version.
********************************************************************************/
UPDATE XXWC.XXWC_GETPAID_PRED_METRICS_TBL 
SET ORG_ID = 162;
/
COMMIT;
/