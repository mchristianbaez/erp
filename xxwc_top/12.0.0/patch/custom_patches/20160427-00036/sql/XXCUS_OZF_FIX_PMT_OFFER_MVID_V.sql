   /*************************************************************************  
     $Header XXCUS_OZF_FIX_PMT_OFFER_MVID_V.sql $
     Module Name: XXCUS_OZF_FIX_PMT_OFFER_MVID_V
     --
     PURPOSE:   This package is called by the concurrent program HDS Rebates: Auto Clear Rebate Payments
     --
     REVISIONS:
     Ver        Date           Author             Ticket                       Description
     ---------  ----------     --------------     -------------------------    -------------------------------------------------
     1.0        04/26/2016     Balaguru Seshadri  TMS:  20160427-00036         Created
                                                  ESMS: 256550
                                                                                      
   **************************************************************************/
CREATE OR REPLACE FORCE VIEW APPS.XXCUS_OZF_FIX_PMT_OFFER_MVID_V
(
   CUST_ACCOUNT_ID,
   CUSTOMER
)
AS
     SELECT 
            HCA.CUST_ACCOUNT_ID,
            HZP.PARTY_NAME||'-'||HCA.ACCOUNT_NUMBER
       FROM QP_LIST_HEADERS_VL QLHV,
            OZF_OFFERS OO,
            QP_QUALIFIERS QQ,
            HZ_CUST_ACCOUNTS HCA,
            HZ_PARTIES HZP
      WHERE     1 = 1
            AND OO.QP_LIST_HEADER_ID = QLHV.LIST_HEADER_ID
            AND OO.STATUS_CODE <> 'COMPLETED' 
            AND QQ.LIST_HEADER_ID(+) = QLHV.LIST_HEADER_ID
            AND QQ.QUALIFIER_CONTEXT(+) = 'SOLD_BY'
            AND QQ.QUALIFIER_ATTRIBUTE(+) = 'QUALIFIER_ATTRIBUTE2'
            AND HCA.CUST_ACCOUNT_ID(+) = TO_NUMBER (QQ.QUALIFIER_ATTR_VALUE)
            AND HZP.PARTY_ID =HCA.PARTY_ID
GROUP BY
 HCA.CUST_ACCOUNT_ID
,HZP.PARTY_NAME||'-'||HCA.ACCOUNT_NUMBER;            
COMMENT ON TABLE APPS.XXCUS_OZF_FIX_PMT_OFFER_MVID_V IS 'TMS 20160427-00036';
