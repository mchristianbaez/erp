/*************************************************************************
$Header TMS_20171127-00332 On Order
PURPOSE: Data Script Fix to Update Quantity
REVISIONS:
Ver        Date         Author                Description
---------  -----------  ------------------    --------------------------
1.0        18-DEC-2017  Sundaramoorthy        TMS#20171127-00332
**************************************************************************/
SET SERVEROUTPUT ON SIZE 1000000


BEGIN
 DBMS_OUTPUT.put_line ('Before update');

 UPDATE apps.po_requisition_lines_all
 SET quantity = 0
 WHERE requisition_header_id = 18060184;

 DBMS_OUTPUT.put_line ('Records updated1-' || SQL%ROWCOUNT);

 UPDATE apps.po_req_distributions_all
 SET req_line_quantity = 0
 WHERE requisition_line_id IN (SELECT REQUISITION_LINE_ID
 FROM po_requisition_lines_all
 WHERE requisition_header_id = 18060184);


 DBMS_OUTPUT.put_line ('Records updated2-' || SQL%ROWCOUNT);
 COMMIT;
EXCEPTION
 WHEN OTHERS
 THEN
 DBMS_OUTPUT.put_line ('Unable to update ' || SQLERRM);
END;
/