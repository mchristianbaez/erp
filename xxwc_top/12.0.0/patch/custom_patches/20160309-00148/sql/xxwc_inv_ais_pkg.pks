CREATE OR REPLACE PACKAGE APPS.xxwc_inv_ais_pkg
AUTHID CURRENT_USER AS
   /*************************************************************************
     $Header: xxwc_inv_ais_pkg $
     Module Name: xxwc_inv_ais_pkg.pks

     PURPOSE:   This package is used AIS 

     REVISIONS:
     Ver        Date        Author                     Description
     ---------  ----------  ---------------         -------------------------
     1.0        11/05/2012  Shankar Hariharan       Initial Version
     2.0        01/23/2013  Shankar Hariharan       UOM Code fix TMS 20130124-00974
     3.0        02/18/2013  Shankar Hariharan       Closed Codeto include "Closed for Invoice" 
                                                   status. TMS 20130206-00890   
     4.0        08/22/2013  Consuelo Gonzalez       TMS 20130821-00995: Update to 
                                                    exclude drop ships in the
                                                    get_onorder_qty function
     4.1        03/09/2016  Gopi Damuluri           TMS# 20160309-00148 - Resolve LastPricePaid issue for new AIS form
   **************************************************************************/
   FUNCTION get_onorder_qty (i_organization_id     IN NUMBER
                           , i_inventory_item_id   IN NUMBER)
      RETURN NUMBER;
      
   FUNCTION get_last_price_paid (p_header_id IN NUMBER, p_quote_number IN NUMBER, p_inventory_item_id IN NUMBER)
   RETURN NUMBER;
   
/*************************************************************************
     Module Name: shipto_last_price_paid

     PURPOSE:   This function is used AIS Lite Form  

     REVISIONS:
     Ver        Date        Author                     Description
     ---------  ----------  ---------------         -------------------------
     8.1        03/09/2016  Gopi Damuluri           TMS# 20160309-00148 - Resolve LastPricePaid issue for new AIS form
**************************************************************************/

FUNCTION shipto_last_price_paid (p_site_use_id         IN NUMBER,
                                 p_inventory_item_id   IN NUMBER)
   RETURN NUMBER;

END xxwc_inv_ais_pkg;
/


