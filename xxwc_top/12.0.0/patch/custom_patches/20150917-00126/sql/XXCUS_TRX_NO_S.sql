  /*
  ==================================================================================
  Version   Date                 Author                          Comments
  -------       -----------          -----------------              -----------------------------------------
  1.0        09/18/2015    Balaguru Seshadri   TMS 20150917-00126 / ESMS 301371 - Reset custom sequence back to 1
  ********************************************************************************
  */
DROP SEQUENCE  XXCUS.XXCUS_TRX_NO_S;
CREATE SEQUENCE XXCUS.XXCUS_TRX_NO_S
 START WITH 1
 INCREMENT BY 1
 NOCACHE
 NOCYCLE
 NOORDER;