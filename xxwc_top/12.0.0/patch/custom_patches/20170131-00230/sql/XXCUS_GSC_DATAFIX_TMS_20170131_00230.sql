/*
 -- Author: Balaguru Seshadri
 -- Parameters: None
 -- Modification History
 -- ESMS        TMS                                   Date                   Version    Comments
 -- =========== ===============  =========         =======  ========================
 -- 538170      20170131-00230       01-FEB-2017   1.0            Created.
 */ 
SET SERVEROUTPUT ON SIZE 1000000
declare
  --
  b_proceed BOOLEAN;
  --
 procedure print_log(p_message in varchar2) is
 begin
     if  fnd_global.conc_request_id >0 then
      fnd_file.put_line(fnd_file.log, p_message);
     else
      dbms_output.put_line(p_message);
     end if;
 end print_log;
 --
 --
 procedure print_output(p_message in varchar2) is
 begin
     if  fnd_global.conc_request_id >0 then
      fnd_file.put_line(fnd_file.output, p_message);
     else
      dbms_output.put_line(p_message);
     end if;
 end print_output;
 --         
begin
  --
  print_log('Start: Main');
  --
  begin 
    --
	execute immediate 'create table XXCUS.XXCUS_TMS_20170131_00230_TBL_1 as select * from ap.ap_exp_report_dists_all where 1 =1
                       and report_distribution_id IN 
                        (
                             5947980
                            ,5947981
                            ,5947982
                            ,5947983
                            ,5947984
                            ,5947985
                            ,5947986
                            ,5947987
                            ,5947988
                            ,5947989
                            ,5947990
                            ,5947991
                            ,5947992
                            ,5947993
                            ,5947994
                            ,5947995
                            ,5947996
                            ,5947997
                            ,5947998
                            ,5934688
                            ,5934689
                            ,5934690
                            ,5934691                            
                        )';
	--
    print_log('Back up table XXCUS.XXCUS_TMS_20170131_00230_TBL_1 created to hold the ap expense distribution data for report_distribution_id [several]');
    --
    b_proceed :=TRUE;
	--
  exception
   when others then
    print_log('@Failed to create backup table XXCUS.XXCUS_TMS_20170131_00230_TBL_1, error msg ='||sqlerrm);
	--
    b_proceed :=FALSE;
	--
  end;   
  -- 
  if (b_proceed) then
      --
	  begin 
		 --
		savepoint square1;
		--
		delete ap.ap_exp_report_dists_all
		where 1 =1
		and report_distribution_id IN 
                        (
                             5947980
                            ,5947981
                            ,5947982
                            ,5947983
                            ,5947984
                            ,5947985
                            ,5947986
                            ,5947987
                            ,5947988
                            ,5947989
                            ,5947990
                            ,5947991
                            ,5947992
                            ,5947993
                            ,5947994
                            ,5947995
                            ,5947996
                            ,5947997
                            ,5947998
                            ,5934688
                            ,5934689
                            ,5934690
                            ,5934691                            
                        );
		--
		print_log('Deleted records from table ap_exp_report_dists_all, row count ='||sql%rowcount);
		--
		commit;
		-- 
	  exception
	   when others then
		print_log('@Failed to delete distribution records from ap_exp_report_dists_all, msg ='||sqlerrm);
		rollback to square1;
	  end;   
	  --   
  end if;  
  --
  begin 
    --
	execute immediate 'create table XXCUS.XXCUS_TMS_20170131_00230_TBL_2 as select * from ap.ap_expense_report_lines_all where report_line_id = 8805177';
    --
    print_log('Back up table XXCUS.XXCUS_TMS_20170131_00230_TBL_2 created to hold the ap expense lines data for report_line_id 8805177');
    --    
    b_proceed :=TRUE;
	--
  exception
   when others then
    print_log('@Failed to create backup table XXCUS.XXCUS_TMS_20170131_00230_TBL_2, error msg ='||sqlerrm);
	--
    b_proceed :=FALSE;
	--
  end;   
  -- 
    -- 
  if (b_proceed) then
      --
	  begin 
		 --
		savepoint square1;
		--
		delete ap.ap_expense_report_lines_all 
		where 1 =1
		      and report_line_id = 8805177 
		;
		--
		print_log('Deleted records from table ap.ap_expense_report_lines_all with report_line_id = 8805177 , row count ='||sql%rowcount);
		--
		commit;
		--      
	  exception
	   when others then
		print_log('@Failed to delete records from table ap.ap_expense_report_lines_all with report_line_id = 8805177, msg ='||sqlerrm);
		rollback to square1;
	  end;   
	  --   
  end if;  
  --
 print_log('End: Main');
 -- 
exception
 when others then
  print_log('Outer block of SQL script, message ='||sqlerrm);
end;
/