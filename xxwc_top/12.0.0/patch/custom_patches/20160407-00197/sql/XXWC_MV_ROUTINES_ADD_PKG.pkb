CREATE OR REPLACE PACKAGE BODY APPS.xxwc_mv_routines_add_pkg
IS
/*************************************************************************
     $Header xxwc_mv_routines_add_pkg
     Module Name: xxwc_mv_routines_add_pkg

     PURPOSE: This Package is created for EDW Invoice Line Extract

     REVISIONS:

     Ver        Date        Author          Description
     ---------  ----------  ----------      ----------------
     1.0        07/31/2014  Veera C         Modified for TMS#20140710-00426 EDW Invoice Line
                                            Extract Change - Promo SKUs
     2.0        23/04/2015  Raghavendra S   TMS# 20150303-00075 - FIN - Addition of Code Snippet for Line Breaks in All EDW Extract Programs
                                            and Views for all free entry text fields.
     2.1        19-APR-2016 Rakesh Patel    TMS-20160407-00197 -Remove Parallelism 								
    **************************************************************************/
    -- created by Rasikha 3/13/2013
    l_error_message   CLOB;

    FUNCTION get_sales_unit_price(p_cost_type IN VARCHAR2,p_line_id IN  NUMBER) RETURN NUMBER IS
  
    /*************************************************************************
     $Header get_sales_unit_price $
     Module Name: get_sales_unit_price

     PURPOSE: This Function created for EDW Invoice Line Extract Chage -Promo SKU's.

     REVISIONS:
     Ver        Date        Author         Description
     ---------  ----------  -----------    ----------------
     1.0        07/31/2014  Veera C        TMS#20140710-00426 EDW Invoice Line Extract Change - Promo SKUs
    **************************************************************************/

      l_catclass_code      mtl_categories_b.segment2%TYPE;
      l_sales_unit_price   ra_customer_trx_lines_all.unit_selling_price%TYPE;
      l_quantity_invoiced  ra_customer_trx_lines_all.quantity_invoiced%TYPE;
      l_quantity_credited  ra_customer_trx_lines_all.quantity_credited%TYPE;
      l_quantity_ordered   ra_customer_trx_lines_all.quantity_ordered %TYPE;
      l_extended_amount    ra_customer_trx_lines_all.extended_amount%TYPE;
      l_unit_selling_price ra_customer_trx_lines_all.unit_selling_price%TYPE;
      l_inventory_item_id  oe_order_lines_all.inventory_item_id%TYPE;
      l_distro_list            VARCHAR2 (75) DEFAULT 'HDSOracleDevelopers@hdsupply.com';
      l_req_id                 NUMBER        := fnd_global.conc_request_id;
      l_user_id                NUMBER        := fnd_global.user_id;
      l_sec                    VARCHAR2(1000);
BEGIN

    l_sec:=' Getting inventorty item id and quantities';

     BEGIN
     SELECT rctla.quantity_invoiced
           ,rctla.quantity_credited
           ,rctla.quantity_ordered
           ,rctla.extended_amount
           ,rctla.unit_selling_price
           ,rctla.inventory_item_id
     INTO
         l_quantity_invoiced
            ,l_quantity_credited
            ,l_quantity_ordered
            ,l_extended_amount
            ,l_unit_selling_price
            ,l_inventory_item_id
     FROM   AR.ra_customer_trx_lines_all rctla
     WHERE  rctla.customer_trx_line_id   = p_line_id;
     EXCEPTION
     WHEN OTHERS THEN
             l_quantity_invoiced := NULL;
             l_quantity_credited := NULL;
             l_quantity_ordered     := NULL;
             l_extended_amount   := NULL;
             l_unit_selling_price := NULL;
             l_inventory_item_id  := NULL;
    END;

l_sec := ' Getting category class';

    BEGIN
    SELECT mcb.segment2
      INTO l_catclass_code
    FROM   INV.mtl_item_categories mic,
           INV.mtl_categories_b mcb,
           INV.mtl_category_sets_tl mcsb,
           INV.mtl_parameters mp,
           INV.mtl_system_items_b msib
    WHERE  mcb.category_id        = mic.category_id
    AND    mcb.disable_Date IS NULL
    AND    mp.organization_id         = msib.organization_id
    AND    mic.organization_id        = msib.organization_id
    AND    msib.inventory_item_id     = mic.inventory_item_id
    AND    mp.master_organization_id  = msib.organization_id
    AND    mp.master_organization_id  = msib.organization_id
    AND    mcsb.category_set_id       = mic.category_set_id
    AND    mic.inventory_item_id      = l_inventory_item_id
    AND    mcsb.category_set_name     = 'Inventory Category';
    EXCEPTION
    WHEN OTHERS THEN
          l_catclass_code := NULL;
    END;


  IF p_cost_type = 'SALES' THEN

      IF l_catclass_code='PRMO' THEN

       l_sales_unit_price := 0;
      ELSE
        l_sales_unit_price:= NVL (l_unit_selling_price,
             (CASE
                 WHEN NVL (
                         NVL (
                            NVL (l_quantity_invoiced ,
                                 l_quantity_credited ),
                                 l_quantity_ordered),
                         1) = 0
                 THEN
                    0
                 ELSE
                    (  NVL (l_extended_amount, 0)
                     / NVL (
                          NVL (
                             NVL (l_quantity_invoiced ,
                                  l_quantity_credited),
                                  l_quantity_ordered),
                          1))
              END));
      END IF;
 END IF;

  IF p_cost_type = 'COST' THEN

       IF l_catclass_code='PRMO' THEN

        l_sales_unit_price :=  ABS(NVL (l_unit_selling_price,
             (CASE
                 WHEN NVL (
                         NVL (
                            NVL (l_quantity_invoiced ,
                                 l_quantity_credited ),
                                 l_quantity_ordered),
                         1) = 0
                 THEN
                    0
                 ELSE
                    (  NVL (l_extended_amount, 0)
                     / NVL (
                          NVL (
                             NVL (l_quantity_invoiced ,
                                  l_quantity_credited),
                                  l_quantity_ordered),
                          1))
              END))) ;
     ELSE
         l_sales_unit_price:= NULL;

      END IF;
   END IF;

  RETURN  l_sales_unit_price;

    EXCEPTION
      WHEN OTHERS THEN

        l_error_message  := 'Error: ' || SQLERRM;
         fnd_file.put_line (FND_FILE.LOG,'Error in xxwc_mv_routines_add_pkg');
         xxcus_error_pkg.xxcus_error_main_api
         (p_called_from            => 'get_sales_unit_price',
          p_calling                => l_sec,
          p_request_id             => l_req_id,
          p_ora_error_msg          => l_error_message,
          p_error_desc             => 'Error running in xxwc_mv_routines_add_pkg in WHEN OTHERS',
          p_distribution_list      => l_distro_list,
          p_module                 => 'AR'
         );
      RETURN  NVL(l_sales_unit_price,0);
    END get_sales_unit_price;

    /*************************************************************************
     $Header populate_xxwc_ar_inv_line_temp $
     Module Name: populate_xxwc_ar_inv_line_temp

     PURPOSE: Modifed the Insert script


     REVISIONS:
     Ver        Date        Author         Description
     ---------  ----------  -----------    ----------------

     1.0        07/31/2014  Veera C        TMS#20140710-00426 EDW Invoice Line Extract Change - Promo SKUs

    **************************************************************************/
    PROCEDURE populate_xxwc_ar_inv_line_temp (p_from_date DATE, p_to_date DATE)
    IS
        l_from_date   DATE;
        l_to_date     DATE;
    BEGIN
        EXECUTE IMMEDIATE 'truncate table xxwc.ar_invoice_line_temp';

        l_from_date := p_from_date - 1;
        l_to_date := p_to_date + 1;

        INSERT /*+ append */
              INTO  xxwc.ar_invoice_line_temp
            SELECT /*+ rule */ th.customer_trx_id    --hint added by harsha for tuning invoice lines--TMS#20131108-00117
                  ,tl.creation_date
                  ,th.trx_date
                  ,th.purchase_order
                  ,th.org_id
                  ,th.batch_source_id
                  ,tl.line_number
                  ,tl.line_type
                  ,tl.sales_order_date
                  ,tl.description
                  ,tl.uom_code
                  ,tl.quantity_invoiced
                  ,tl.quantity_credited
                  ,tl.quantity_ordered
                --,tl.unit_selling_price                                             -- 07/30/2014 Commented by Veera for TMS#20140710-00426
                  ,get_sales_unit_price('SALES',tl.customer_trx_line_id) unit_selling_price     -- 07/30/2014 Added  by Veera for TMS#20140710-00426
                  ,tl.extended_amount
                  ,tl.customer_trx_line_id
                  ,tl.interface_line_attribute10
                  ,tl.inventory_item_id
                  ,tl.interface_line_attribute6
                  ,tl.interface_line_context
                  ,tl.warehouse_id
                  ,'I' insert_flag
                  ,xxwc_mv_routines_pkg.get_mst_organization_id (tl.org_id) mst_organization_id
                  ,rctla_gd.customer_trx_id rctla_gd_customer_trx_id
                  ,rctla_gd.customer_trx_line_id rctla_gd_customer_trx_line_id
                  , (CASE
                         WHEN rbsa.name IN ('WC MANUAL', 'MANUAL-OTHER') THEN TRUNC (th.creation_date)
                         ELSE (TRUNC (th.creation_date) - 1)
                     END)
                       invoice_post_date
                  -- 10/07/2013 CG: TMS 20130912-00744: Added manual invoice warehouse id
                  , tl.interface_line_attribute7
                  , rbsa.name
              FROM ra_customer_trx_all th
                  ,ra_customer_trx_lines_all tl
                  ,ra_cust_trx_line_gl_dist_all rctla_gd
                  ,ra_batch_sources_all rbsa
             WHERE     th.customer_trx_id = tl.customer_trx_id
                   AND th.customer_trx_id = rctla_gd.customer_trx_id(+)
                   AND rctla_gd.customer_trx_line_id(+) IS NULL
                   AND tl.creation_date BETWEEN l_from_date AND l_to_date
                   AND tl.line_type != 'TAX'
                   AND th.batch_source_id = rbsa.batch_source_id
                   AND th.org_id = rbsa.org_id
                   AND rbsa.name NOT IN ('PRISM', 'PRISM REFUND', 'CONVERSION');

        COMMIT;

        INSERT /*+ append */
              INTO  xxwc.ar_invoice_line_temp
            SELECT a.customer_trx_id
                  ,a.creation_date
                  ,a.trx_date
                  ,a.purchase_order
                  ,a.org_id
                  ,a.batch_source_id
                  ,a.line_number
                  ,a.line_type
                  ,a.sales_order_date
                  ,a.description
                  ,a.uom_code
                  ,a.quantity_invoiced
                  ,a.quantity_credited
                  ,a.quantity_ordered
                  ,a.unit_selling_price
                  ,a.extended_amount
                  ,a.customer_trx_line_id
                  ,a.interface_line_attribute10
                  ,a.inventory_item_id
                  ,a.interface_line_attribute6
                  ,a.interface_line_context
                  ,a.warehouse_id
                  ,'F' insert_flag
                  ,mst_organization_id
                  ,rctla_gd_customer_trx_id
                  ,rctla_gd_customer_trx_line_id
                  ,invoice_post_date
                  -- 10/07/2013 CG: TMS 20130912-00744: Added manual invoice warehouse id
                  , a.interface_line_attribute7
                  , a.batch_name
              FROM xxwc.ar_invoice_line_temp a
             WHERE a.insert_flag = 'I' AND TRUNC (a.creation_date) BETWEEN p_from_date AND p_to_date;

        COMMIT;

        DELETE FROM xxwc.ar_invoice_line_temp
              WHERE insert_flag = 'I';

        COMMIT;

        COMMIT;
    EXCEPTION
        WHEN OTHERS
        THEN
            l_error_message :=
                   'xxwc_mv_routines_add_pkg.populate_xxwc_ar_inv_line_temp '
                || 'Error_Stack...'
                || DBMS_UTILITY.format_error_stack ()
                || ' Error_Backtrace...'
                || DBMS_UTILITY.format_error_backtrace ();

            xxwc_helpers.xxwc_log_debug (l_error_message);
            xxwc_helpers.xxwc_log_error (l_error_message
                                        ,'xxwc_mv_routines_add_pkg by Rasikha'
                                        ,'populate_xxwc_ar_inv_line_temp'
                                        ,'EDW');
    END;

    --********************************************************
    --********************************************************
    --********************************************************
    --********************************************************
    FUNCTION get_original_modifier (p_header_id NUMBER, p_line_id NUMBER)
        RETURN VARCHAR2
    IS
        n                             NUMBER := 0;
        v_first_modifier              VARCHAR2 (164);
        v_count_auto_line_modifiers   NUMBER := 0;
        v_count_manual_modifiers      NUMBER := 0;
        v_count_all_modifiers         NUMBER := 0;
    BEGIN
        -- populate_temp_adj_table (p_header_id, p_line_id);
        v_count_manual_modifiers := 0;
        v_count_all_modifiers := 0;

        BEGIN
            SELECT COUNT ('a')
              INTO v_count_manual_modifiers
              FROM oe_price_adjustments_v
             WHERE     header_id = p_header_id
                   AND (   (    (line_id = p_line_id OR (line_id IS NULL AND list_line_type_code <> 'CIE'))
                            AND modifier_level_code = 'LINE')
                        OR (line_id IS NULL AND modifier_level_code = 'ORDER'))
                   AND applied_flag = 'Y'
                   AND list_line_type_code <> 'FREIGHT_CHARGE'
                   AND change_reason_code = 'MANUAL';
        EXCEPTION
            WHEN OTHERS
            THEN
                NULL;
        END;

        BEGIN
            SELECT COUNT ('a')
              INTO v_count_all_modifiers
              FROM oe_price_adjustments_v
             WHERE     header_id = p_header_id
                   AND (   (    (line_id = p_line_id OR (line_id IS NULL AND list_line_type_code <> 'CIE'))
                            AND modifier_level_code = 'LINE')
                        OR (line_id IS NULL AND modifier_level_code = 'ORDER'))
                   AND applied_flag = 'Y'
                   AND list_line_type_code <> 'FREIGHT_CHARGE';
        EXCEPTION
            WHEN OTHERS
            THEN
                NULL;
        END;

        FOR ri
            IN (SELECT COUNT ('a') count_auto_line_modifiers
                  FROM oe_price_adjustments_v
                 WHERE     header_id = p_header_id
                       AND (   (    (line_id = p_line_id OR (line_id IS NULL AND list_line_type_code <> 'CIE'))
                                AND modifier_level_code = 'LINE')
                            OR (line_id IS NULL AND modifier_level_code = 'ORDER'))
                       AND applied_flag = 'Y'
                       AND list_line_type_code <> 'FREIGHT_CHARGE'
                       AND automatic_flag = 'Y')
        LOOP
            v_count_auto_line_modifiers := ri.count_auto_line_modifiers;
        END LOOP;

        IF v_count_auto_line_modifiers = 0 AND v_count_all_modifiers = v_count_manual_modifiers --No automaatic modifiers
        THEN
              --v_first_modifier := 'BASE PRICE';   -- Commented by harsha for TMS#20140121-00086  EDW Invoice Lines - change Base Price to Market Price
               v_first_modifier := 'MARKET PRICE';       -- Added by harsha for TMS#20140121-00086  EDW Invoice Lines - change Base Price to Market Price
            RETURN v_first_modifier;
        END IF;

        FOR r
            IN (  SELECT price_adjustment_id
                        ,creation_date
                        ,created_by
                        ,last_update_date
                        ,last_updated_by
                        ,last_update_login
                        ,program_application_id
                        ,program_id
                        ,program_update_date
                        ,request_id
                        ,header_id
                        ,discount_id
                        ,discount_line_id
                        ,line_id
                        ,modifier_level_code
                        ,adjustment_name
                        ,adjustment_description
                        ,list_line_type_code
                        ,list_line_no
                        ,automatic_flag
                        ,override_allowed_flag
                        ,change_reason_code
                        ,change_reason_text
                        ,modified_from
                        ,modified_to
                        ,list_header_id
                        ,list_line_id
                        ,operand
                        ,arithmetic_operator
                        ,updated_flag
                        ,applied_flag
                        ,adjusted_amount
                        ,pricing_group_sequence
                        ,pricing_phase_id
                        ,source_system_code
                        ,price_break_type_code
                        ,substitution_attribute
                        ,proration_type_code
                        ,percent
                        ,modifier_mechanism_type_code
                    FROM oe_price_adjustments_v
                   WHERE     header_id = p_header_id
                         AND (line_id = p_line_id OR (line_id IS NULL AND list_line_type_code <> 'CIE'))
                         AND applied_flag = 'Y'
                         AND list_line_type_code <> 'FREIGHT_CHARGE'
                ORDER BY pricing_group_sequence
                        ,1
                        ,2
                        ,3
                        ,4
                        ,5
                        ,6
                        ,7
                        ,8
                        ,9)
        LOOP
            n := n + 1;

            IF n = 1
            THEN
                v_first_modifier := r.adjustment_name;
                RETURN v_first_modifier;
            END IF;

            EXIT WHEN v_first_modifier IS NOT NULL;
        END LOOP;

        IF v_first_modifier IS NULL
        THEN
              --v_first_modifier := 'BASE PRICE';   -- Commented by harsha for TMS#20140121-00086  EDW Invoice Lines - change Base Price to Market Price
          v_first_modifier := 'MARKET PRICE';       -- Added by harsha for TMS#20140121-00086  EDW Invoice Lines - change Base Price to Market Price
        END IF;

        RETURN v_first_modifier;
    END;

    --********************************************************
    --********************************************************
    --********************************************************
    --********************************************************
    --********************************************************
    --********************************************************

    FUNCTION get_last_modifier (p_header_id NUMBER, p_line_id NUMBER)
        RETURN VARCHAR2
    IS
        n                 NUMBER := 0;
        v_last_modifier   VARCHAR2 (164);
    BEGIN
        --  populate_temp_adj_table (p_header_id, p_line_id);

        FOR r
            IN (  SELECT price_adjustment_id
                        ,creation_date
                        ,created_by
                        ,last_update_date
                        ,last_updated_by
                        ,last_update_login
                        ,program_application_id
                        ,program_id
                        ,program_update_date
                        ,request_id
                        ,header_id
                        ,discount_id
                        ,discount_line_id
                        ,line_id
                        ,modifier_level_code
                        ,adjustment_name
                        ,adjustment_description
                        ,list_line_type_code
                        ,list_line_no
                        ,automatic_flag
                        ,override_allowed_flag
                        ,change_reason_code
                        ,change_reason_text
                        ,modified_from
                        ,modified_to
                        ,list_header_id
                        ,list_line_id
                        ,operand
                        ,arithmetic_operator
                        ,updated_flag
                        ,applied_flag
                        ,adjusted_amount
                        ,pricing_group_sequence
                        ,pricing_phase_id
                        ,source_system_code
                        ,price_break_type_code
                        ,substitution_attribute
                        ,proration_type_code
                        ,percent
                        ,modifier_mechanism_type_code
                    FROM oe_price_adjustments_v
                   WHERE     header_id = p_header_id
                         AND (line_id = p_line_id OR (line_id IS NULL AND list_line_type_code <> 'CIE'))
                         AND applied_flag = 'Y'
                         AND list_line_type_code <> 'FREIGHT_CHARGE'
                ORDER BY pricing_group_sequence DESC
                        ,1 DESC
                        ,2 DESC
                        ,3 DESC
                        ,4 DESC
                        ,5 DESC
                        ,6 DESC
                        ,7 DESC
                        ,8 DESC
                        ,9 DESC)
        LOOP
            n := n + 1;

            IF n = 1
            THEN
                v_last_modifier := r.adjustment_name;
            END IF;

            EXIT WHEN v_last_modifier IS NOT NULL;
        END LOOP;

        IF v_last_modifier IS NULL
        THEN
            --v_last_modifier := 'BASE PRICE';   -- Commented by harsha for TMS#20140121-00086  EDW Invoice Lines - change Base Price to Market Price
          v_last_modifier := 'MARKET PRICE';       -- Added by harsha for TMS#20140121-00086  EDW Invoice Lines - change Base Price to Market Price
        END IF;

        RETURN v_last_modifier;
    END;

    --********************************************************
    --********************************************************
    -- Overridden value for Sell price of a line item.
    -- (i.e. original price the item would be sold for before
    -- a special discount is applied.)
    --********************************************************
    --********************************************************

    FUNCTION get_overridden_sell_price (p_header_id NUMBER, p_line_id NUMBER)
        RETURN NUMBER
    IS
        n                         NUMBER := 0;
        v_overridden_sell_price   NUMBER;
        v_list_price              NUMBER;
    BEGIN
        --find list price
        FOR rp IN (SELECT unit_list_price
                     FROM oe_order_lines_all l
                    WHERE l.header_id = p_header_id AND l.line_id = p_line_id)
        LOOP
            v_list_price := rp.unit_list_price;
        END LOOP;

        FOR r
            IN (  SELECT price_adjustment_id
                        ,creation_date
                        ,created_by
                        ,last_update_date
                        ,last_updated_by
                        ,last_update_login
                        ,program_application_id
                        ,program_id
                        ,program_update_date
                        ,request_id
                        ,header_id
                        ,discount_id
                        ,discount_line_id
                        ,line_id
                        ,modifier_level_code
                        ,adjustment_name
                        ,adjustment_description
                        ,list_line_type_code
                        ,list_line_no
                        ,automatic_flag
                        ,override_allowed_flag
                        ,change_reason_code
                        ,change_reason_text
                        ,modified_from
                        ,modified_to
                        ,list_header_id
                        ,list_line_id
                        ,operand
                        ,arithmetic_operator
                        ,updated_flag
                        ,applied_flag
                        ,adjusted_amount
                        ,pricing_group_sequence
                        ,pricing_phase_id
                        ,source_system_code
                        ,price_break_type_code
                        ,substitution_attribute
                        ,proration_type_code
                        ,percent
                        ,modifier_mechanism_type_code
                    FROM oe_price_adjustments_v
                   WHERE     header_id = p_header_id
                         AND (line_id = p_line_id OR (line_id IS NULL AND list_line_type_code <> 'CIE'))
                         AND applied_flag = 'Y'
                         AND list_line_type_code <> 'FREIGHT_CHARGE'
                         AND automatic_flag = 'Y'
                --  AND modifier_level_code = 'LINE'-- 5/22/2013 commented to consider both line and order level auto modifiers
                -- AND NVL (change_reason_code, 'x') <> 'MANUAL'
                ORDER BY pricing_group_sequence DESC
                        ,1 DESC
                        ,2 DESC
                        ,3 DESC
                        ,4 DESC
                        ,5 DESC
                        ,6 DESC
                        ,7 DESC
                        ,8 DESC
                        ,9 DESC)
        LOOP
            n := n + 1;

            IF n = 1
            THEN
                IF r.arithmetic_operator = 'NEWPRICE'
                THEN
                    v_overridden_sell_price := ROUND (r.operand, 4);
                ELSIF r.arithmetic_operator = '%' AND r.list_line_type_code = 'DIS'
                THEN
                    v_overridden_sell_price := (v_list_price * (100 - r.operand)) / 100;
                ELSIF r.arithmetic_operator = 'AMT' AND r.list_line_type_code = 'DIS'
                THEN
                    v_overridden_sell_price := v_list_price - r.operand;
                END IF;
            END IF;

            EXIT WHEN v_overridden_sell_price IS NOT NULL;
        END LOOP;

        IF v_overridden_sell_price IS NULL -- IF WE HAVE THE ONLY  MANUAL ADJ                                                                          --
        THEN
            FOR rg IN (SELECT unit_list_price
                         FROM oe_order_lines_all l
                        WHERE l.header_id = p_header_id AND l.line_id = p_line_id)
            LOOP
                v_overridden_sell_price := rg.unit_list_price;
            END LOOP;
        END IF;

        RETURN v_overridden_sell_price;
    END;

    --********************************************************
    --********************************************************
    --********************************************************
    --********************************************************

    FUNCTION get_price_type (p_header_id NUMBER, p_line_id NUMBER)
        RETURN VARCHAR2
    IS
        n                      NUMBER := 0;
        v_change_reason_code   VARCHAR2 (124);
        v_price_type           VARCHAR2 (124) := 'SYSTEM';
    BEGIN
        FOR r
            IN (  SELECT price_adjustment_id
                        ,creation_date
                        ,created_by
                        ,last_update_date
                        ,last_updated_by
                        ,last_update_login
                        ,program_application_id
                        ,program_id
                        ,program_update_date
                        ,request_id
                        ,header_id
                        ,discount_id
                        ,discount_line_id
                        ,line_id
                        ,modifier_level_code
                        ,adjustment_name
                        ,adjustment_description
                        ,list_line_type_code
                        ,list_line_no
                        ,automatic_flag
                        ,override_allowed_flag
                        ,change_reason_code
                        ,change_reason_text
                        ,modified_from
                        ,modified_to
                        ,list_header_id
                        ,list_line_id
                        ,operand
                        ,arithmetic_operator
                        ,updated_flag
                        ,applied_flag
                        ,adjusted_amount
                        ,pricing_group_sequence
                        ,pricing_phase_id
                        ,source_system_code
                        ,price_break_type_code
                        ,substitution_attribute
                        ,proration_type_code
                        ,percent
                        ,modifier_mechanism_type_code
                    FROM oe_price_adjustments_v
                   WHERE     header_id = p_header_id
                         AND (line_id = p_line_id OR (line_id IS NULL AND list_line_type_code <> 'CIE'))
                         AND applied_flag = 'Y'
                         AND list_line_type_code <> 'FREIGHT_CHARGE'
                -- AND automatic_flag = 'Y'
                ORDER BY pricing_group_sequence DESC
                        ,1 DESC
                        ,2 DESC
                        ,3 DESC
                        ,4 DESC
                        ,5 DESC
                        ,6 DESC
                        ,7 DESC
                        ,8 DESC
                        ,9 DESC)
        LOOP
            n := n + 1;

            IF n = 1
            THEN
                v_change_reason_code := NVL (r.change_reason_code, 'x');

                IF r.automatic_flag = 'N'                                             -- v_change_reason_code = 'MANUAL'
                THEN
                    v_price_type := 'MANUAL';
                ELSE
                    FOR ri IN (SELECT attribute10, name
                                 FROM qp_secu_list_headers_vl
                                WHERE list_header_id = r.list_header_id AND context = 162 -- AND NVL (end_date_active, SYSDATE + 1) >= SYSDATE
                                                                                         AND automatic_flag = 'Y')
                    LOOP
                        IF ri.attribute10 = 'Contract Pricing' OR ri.name LIKE 'CSP%'
                        THEN
                            v_price_type := 'CONTRACT';
                        ELSIF ri.attribute10 = 'Vendor Quote'
                        THEN
                            v_price_type := 'VQN';
                        ELSE
                            v_price_type := 'SYSTEM';
                        END IF;
                    END LOOP;
                END IF;
            END IF;

            EXIT WHEN n = 1;
        END LOOP;

        RETURN v_price_type;
    END;

    --******************************************************
    PROCEDURE drop_temp_table (p_owner VARCHAR2, p_table_name VARCHAR2)
    IS
        v_ddl_string   VARCHAR2 (1024);
    BEGIN
        FOR r
            IN (SELECT object_name
                  FROM all_objects
                 WHERE     object_name = UPPER (TRIM (p_table_name))
                       AND object_type = 'TABLE'
                       AND owner = UPPER (TRIM (p_owner)))
        LOOP
            v_ddl_string := 'drop table ' || p_owner || '.' || p_table_name;

            EXECUTE IMMEDIATE v_ddl_string;
        END LOOP;
    EXCEPTION
        WHEN OTHERS
        THEN
            NULL;
    END;

    --*************************************************************
    --*************************************************************
    --*************************************************************
    --*************************************************************
    FUNCTION create_trunc_temp_table (p_owner VARCHAR2, p_table_name VARCHAR2)
        RETURN VARCHAR2
    IS
        v_ddl_string   VARCHAR2 (1024);
        v_return       VARCHAR2 (24) := 'TRUNC';
    BEGIN
        FOR r
            IN (SELECT object_name
                  FROM all_objects
                 WHERE     object_name = UPPER (TRIM (p_table_name))
                       AND object_type = 'TABLE'
                       AND owner = UPPER (TRIM (p_owner)))
        LOOP
            v_ddl_string := 'truncate table ' || p_owner || '.' || p_table_name;

            EXECUTE IMMEDIATE v_ddl_string;

            RETURN v_return;
        END LOOP;

        v_return := 'CREATE';
        RETURN v_return;
    EXCEPTION
        WHEN OTHERS
        THEN
            NULL;
    END;

    --*************************************************************
    --*************************************************************
    --*************************************************************
    --*************************************************************
    --  Find the most free tablespace that start with XX%. This is
    -- to avoid using XXWC if it is not the right place to store this
    -- temporary data. This will most likely find and use the XXEIS
    -- tablespace.
    FUNCTION running_tablespace
        RETURN VARCHAR2
    IS
        v_tablespacename   VARCHAR2 (124);
    BEGIN
        SELECT tablespace
          INTO v_tablespacename
          FROM (  SELECT df.tablespace_name tablespace
                        ,df.total_space total_space
                        ,fs.free_space free_space
                        ,df.total_space_mb total_space_mb
                        , (df.total_space_mb - fs.free_space_mb) used_space_mb
                        ,fs.free_space_mb free_space_mb
                        ,ROUND (100 * (fs.free_space / df.total_space), 2) pct_free
                    FROM (  SELECT tablespace_name, SUM (bytes) total_space, ROUND (SUM (bytes) / 1048576) total_space_mb
                              FROM dba_data_files
                          GROUP BY tablespace_name) df
                        ,(  SELECT tablespace_name, SUM (bytes) free_space, ROUND (SUM (bytes) / 1048576) free_space_mb
                              FROM dba_free_space
                          GROUP BY tablespace_name) fs
                   WHERE df.tablespace_name = fs.tablespace_name(+) AND df.tablespace_name LIKE 'XX%'
                ORDER BY free_space_mb DESC)
         WHERE ROWNUM = 1;

        RETURN v_tablespacename;
    END;

   /****************************************************************************************************************
    Module Name: alter_table_temp

    PURPOSE: Make the table perform better for this extract

    REVISIONS:
    Ver        Date        Author             Description
    ---------  ----------  -------------    ---------------------------------------------------------------------
    2.2        04/19/2016  Rakesh Patel     TMS#20160407-00197 - Enabling/Disabling parallelism at seesion level
   ****************************************************************************************************************/
    PROCEDURE alter_table_temp (p_owner VARCHAR2, p_table_name VARCHAR2)
    IS
        v_execute_string   CLOB;
        v_tablespace       VARCHAR2 (124);
    BEGIN
        v_tablespace := 'XXEIS_DATA';

        FOR r
            IN (SELECT object_name
                  FROM all_objects
                 WHERE     object_name = UPPER (TRIM (p_table_name))
                       AND object_type = 'TABLE'
                       AND owner = UPPER (TRIM (p_owner)))
        LOOP
            v_execute_string := 'ALTER TABLE ' || p_owner || '.' || p_table_name || ' NOLOGGING';

            EXECUTE IMMEDIATE v_execute_string;

            v_execute_string := 'ALTER TABLE ' || p_owner || '.' || p_table_name || ' MOVE TABLESPACE ' || v_tablespace;

            EXECUTE IMMEDIATE v_execute_string;

            -- Increases parallelism on this table, helps with RAC environment.
            -- May change this to a forumla based on the environment
            v_execute_string := 'ALTER TABLE ' || p_owner || '.' || p_table_name;
			--|| ' parallel 8'; -- Commented by Rakesh Patel on 04/19/2016 for TMS#20160407-00197

            EXECUTE IMMEDIATE v_execute_string;
        END LOOP;
    EXCEPTION
        WHEN OTHERS
        THEN
            l_error_message :=
                   ' Error_Stack...'
                || CHR (10)
                || DBMS_UTILITY.format_error_stack ()
                || CHR (10)
                || ' Error_Backtrace...'
                || CHR (10)
                || DBMS_UTILITY.format_error_backtrace ();
    --  xxwc_helpers.xxwc_log_debug ('error executing ' || v_execute_string || ' ' || l_errbuf);
    END;

    --*************************************************************
    --*************************************************************
    --*************************************************************
    --*************************************************************
    --*************************************************************

    --*************************************************************
    --******helper function

    FUNCTION create_copy_table (p_owner VARCHAR2, p_table_name VARCHAR2, p_new_table_name VARCHAR2)
        RETURN NUMBER
    IS
        v_ind   NUMBER := 0;
    BEGIN
        drop_temp_table (p_owner, p_new_table_name);

        FOR rt IN (SELECT table_name
                     FROM all_tables
                    WHERE table_name = UPPER (p_table_name) AND owner = UPPER (p_owner))
        LOOP
            EXECUTE IMMEDIATE
                   ' CREATE TABLE '
                || p_owner
                || '.'
                || p_new_table_name
                || ' AS SELECT * FROM '
                || p_owner
                || '.'
                || p_table_name
                || ' WHERE 1=2';

            alter_table_temp (p_owner, p_new_table_name);
            v_ind := 1;
        END LOOP;

        RETURN v_ind;
    END;

    --*************************************************************

    --*************************************************************
    --*************************************************************
    -- helper function
    PROCEDURE table_create_from_old (p_owner VARCHAR2, p_old_table_name VARCHAR2, p_new_table_name VARCHAR2)
    IS
    BEGIN
        DECLARE
            vr   NUMBER := 0;
        BEGIN
            drop_temp_table (p_owner, p_new_table_name);
            vr := create_copy_table (p_owner, p_old_table_name, p_new_table_name);
            -- FUNCTION create_copy_table (p_owner VARCHAR2, p_table_name VARCHAR2, p_new_table_name VARCHAR2)
            alter_table_temp (p_owner, p_new_table_name);
        END;
    END;

    --******************************************************
    PROCEDURE populate_onhand_mv (p_errbuf OUT VARCHAR2, p_retcode OUT VARCHAR2)
    IS
/******************************************************************************
   NAME:      populate_onhand_mv
   PURPOSE:  populate_onhand_mv

   REVISIONS:
   Ver        Date        Author           Description
   ---------  ----------  ---------------  ------------------------------------
   2.0        23/04/2015   Raghavendra S   TMS# 20150303-00075 - FIN - Addition of Code Snippet for Line Breaks in All EDW Extract Programs
                                            and Views for all free entry text fields.
   2.2        04/19/2016  Rakesh Patel     TMS#20160407-00197 - Enabling/Disabling parallelism at seesion level
******************************************************************************/
        v_sql_string   CLOB;
        v_sysdate      DATE;
        g_start        NUMBER;

        PROCEDURE elapsed_time (p_called_from VARCHAR2, p_start IN OUT NUMBER)
        IS
            v_sec_elapsed   NUMBER;
            gr_start        NUMBER := DBMS_UTILITY.get_time;
        BEGIN
            g_start := NVL (p_start, gr_start);
            v_sec_elapsed := gr_start - g_start;
            DBMS_OUTPUT.put_line (p_called_from || '  run for ' || v_sec_elapsed / 100 || ' sec');
            p_start := gr_start;
        END;
    BEGIN
	    enable_parallelism; --Added for 2.2 - TMS#20160407-00197
		
        xxwc_mv_routines_add_pkg.drop_temp_table ('XXWC', 'XXWC_INV_ONHAND_MV##ZD');
        v_sysdate := SYSDATE;
        -- ''WHITECAP'' business_unit
        --   ,''Oracle EBS'' source_system
        -- ,SYSDATE interface_date
        -- ,SYSDATE snapshotdate

        -- Added REGEXP_REPLACE funtion in below Dynamic SQL for V 2.0

        v_sql_string :=
            'CREATE TABLE xxwc.xxwc_inv_onhand_mv##zD
AS
    SELECT hou.organization_id operating_unit_id
          ,hou.name operating_unit_name
          ,moqd.inventory_item_id
          ,moqd.organization_id
          ,REGEXP_REPLACE(ood.organization_name,''[[:cntrl:]]'', '' '') branch_name
          ,REGEXP_REPLACE(mp.organization_code,''[[:cntrl:]]'', '' '') branch_loc_num
          ,REGEXP_REPLACE(msib.segment1,''[[:cntrl:]]'', '' '') bu_sku
          ,REGEXP_REPLACE(msit.long_description,''[[:cntrl:]]'', '' '') long_description
          ,REGEXP_REPLACE(msib.description,''[[:cntrl:]]'', '' '') short_description
          ,msib.creation_date origindate
          ,cst_cost_api.get_item_cost (1.0, moqd.inventory_item_id, moqd.organization_id) moving_avg_cost
          , moqd.primary_transaction_quantity  onhand_qty
      FROM mtl_onhand_quantities_detail moqd
          ,mtl_system_items_b msib
          ,mtl_system_items_tl msit
          ,mtl_parameters mp
          ,org_organization_definitions ood
          ,hr_operating_units hou
     WHERE     moqd.organization_id = mp.organization_id
           AND moqd.inventory_item_id = msib.inventory_item_id
           AND moqd.organization_id = msib.organization_id
           AND moqd.inventory_item_id = msit.inventory_item_id
           AND moqd.organization_id = msit.organization_id
           AND mp.organization_id = ood.organization_id
           AND ood.operating_unit = hou.organization_id
           AND 1 = 2';

        EXECUTE IMMEDIATE v_sql_string;

        elapsed_time ('populate main table start', g_start);
        xxwc_mv_routines_add_pkg.alter_table_temp ('XXWC', 'XXWC_INV_ONHAND_MV##ZD');

        -- Added REGEXP_REPLACE funtion in below Dynamic SQL for V 2.0

        v_sql_string :=
            'BEGIN
    FOR r IN (SELECT organization_id
                FROM org_organization_definitions
               WHERE operating_unit = 162 AND NVL (disable_date, SYSDATE + 1) > SYSDATE)
    LOOP
        INSERT /*+ APPEND */
              INTO  xxwc.xxwc_inv_onhand_mv##zD
            SELECT hou.organization_id operating_unit_id
                  ,hou.name operating_unit_name
                  ,moqd.inventory_item_id
                  ,moqd.organization_id
                  ,REGEXP_REPLACE(ood.organization_name,''[[:cntrl:]]'', '' '')  branch_name
                  ,REGEXP_REPLACE(mp.organization_code,''[[:cntrl:]]'', '' '')  branch_loc_num
                  ,REGEXP_REPLACE(msib.segment1,''[[:cntrl:]]'', '' '')  bu_sku
                  ,REGEXP_REPLACE(msit.long_description,''[[:cntrl:]]'', '' '')  long_description
                  ,REGEXP_REPLACE(msib.description,''[[:cntrl:]]'', '' '')  short_description
                  ,msib.creation_date origindate
                  ,ROUND (cst_cost_api.get_item_cost (1.0, moqd.inventory_item_id, moqd.organization_id), 2)
                       moving_avg_cost
                  , moqd.primary_transaction_quantity  onhand_qty
              FROM mtl_onhand_quantities_detail moqd
                  ,mtl_system_items_b msib
                  ,mtl_system_items_tl msit
                  ,mtl_parameters mp
                  ,org_organization_definitions ood
                  ,hr_operating_units hou
             WHERE     moqd.organization_id = mp.organization_id
                   AND moqd.inventory_item_id = msib.inventory_item_id
                   AND moqd.organization_id = msib.organization_id
                   AND moqd.inventory_item_id = msit.inventory_item_id
                   AND moqd.organization_id = msit.organization_id
                   AND mp.organization_id = ood.organization_id
                   AND ood.operating_unit = hou.organization_id
                   AND 162 = hou.organization_id
                   AND msib.organization_id = r.organization_id;

        COMMIT;
    END LOOP;
END;';
        elapsed_time ('populate main table values', g_start);

        EXECUTE IMMEDIATE v_sql_string;

        COMMIT;

        xxwc_mv_routines_add_pkg.table_create_from_old ('XXWC', 'XXWC_INV_ONHAND_MV##ZD', 'XXWC_INV_ONHAND_MV##XD');
        v_sql_string := '
        BEGIN
    FOR r IN (SELECT organization_id
                FROM org_organization_definitions
               WHERE operating_unit = 162 AND NVL (disable_date, SYSDATE + 1) > SYSDATE)
    LOOP
    INSERT /*+ APPEND */ INTO
       xxwc.xxwc_inv_onhand_mv##XD
        SELECT operating_unit_id
        ,operating_unit_name
        ,inventory_item_id
        ,organization_id
        ,branch_name
        ,branch_loc_num
        ,bu_sku
        ,long_description
        ,short_description
        ,origindate
        ,moving_avg_cost
        ,round(SUM (onhand_qty),2) onhand_qty1
    FROM xxwc.xxwc_inv_onhand_mv##zD
      WHERE  organization_id = r.organization_id
GROUP BY operating_unit_id
        ,operating_unit_name
        ,inventory_item_id
        ,organization_id
        ,branch_name
        ,branch_loc_num
        ,bu_sku
        ,long_description
        ,short_description
        ,origindate
        ,moving_avg_cost;

        COMMIT;
    END LOOP;
END;';

        --XXWC_INV_ONHAND_MV##XD
        EXECUTE IMMEDIATE v_sql_string;

        COMMIT;

        elapsed_time ('populate main table totals', g_start);
        --********************************************
        --********************SUBMIT PARALLEL JOBS
        apps.xxwc_mv_routines_add_pkg.drop_temp_table ('XXWC', 'XXWC_INV_ONHAND_MV##Zr');

        EXECUTE IMMEDIATE
            'create table xxwc.XXWC_INV_ONHAND_MV##Zr as select distinct organization_id, inventory_item_id from xxwc.XXWC_INV_ONHAND_MV##zD where 1=2';

        xxwc_mv_routines_add_pkg.alter_table_temp ('XXWC', 'XXWC_INV_ONHAND_MV##Zr');

        EXECUTE IMMEDIATE
            'insert /*+append*/ into  xxwc.XXWC_INV_ONHAND_MV##Zr  select distinct organization_id, inventory_item_id from xxwc.XXWC_INV_ONHAND_MV##zD ';

        COMMIT;
        apps.xxwc_mv_routines_add_pkg.drop_temp_table ('XXWC', 'populate_get_onhand##');

        EXECUTE IMMEDIATE 'CREATE TABLE XXWC.populate_get_onhand##
    AS
    SELECT organization_id, ''populate_get_onhand##z''||organization_id table_name
      FROM (SELECT DISTINCT ORGANIZATION_ID FROM mtl_onhand_quantities_detail)     ';

        BEGIN
            FOR r IN (SELECT job
                        FROM user_jobs
                       WHERE what LIKE '%populate_get_onhand_qty%')
            LOOP
                DBMS_JOB.remove (r.job);
                COMMIT;
            END LOOP;
        END;

        EXECUTE IMMEDIATE 'DECLARE
            v_ex   VARCHAR2 (250);
        BEGIN
             FOR r IN (SELECT * FROM XXWC.populate_get_onhand##)

            LOOP
                v_ex :=
                       ''BEGIN APPS.xxwc_mv_routines_add_pkg.populate_get_onhand_qty (''''''
                    || r.table_name
                    || '''''',''
                    || r.organization_id
                    || ''); END;'';

                DECLARE
                    v_job   NUMBER;
                BEGIN
                    DBMS_JOB.submit (v_job
                                    ,v_ex
                                    ,SYSDATE
                                    ,NULL);
                    COMMIT;
                END;
            END LOOP;
        END;';

        --*************************************************
        ---POPULATE on_order_qty
        --********************************************
        xxwc_mv_routines_add_pkg.drop_temp_table ('XXWC', 'XXWC_INV_ONHAND_MV##Z');
        v_sql_string := 'CREATE TABLE xxwc.xxwc_inv_onhand_mv##Z
(
    organization_id     NUMBER
   ,inventory_item_id   NUMBER
   ,on_order_qty           NUMBER
)';

        EXECUTE IMMEDIATE v_sql_string;

        xxwc_mv_routines_add_pkg.alter_table_temp ('XXWC', 'XXWC_INV_ONHAND_MV##Z');
        v_sql_string :=
            'BEGIN
    FOR r IN (SELECT organization_id
                FROM org_organization_definitions
               WHERE operating_unit = 162 AND NVL (disable_date, SYSDATE + 1) > SYSDATE)
    LOOP
        INSERT /*+ APPEND */ INTO XXWC.XXWC_INV_ONHAND_MV##Z
        SELECT plla.ship_to_organization_id
      ,pll.item_id
      ,plla.quantity - NVL (plla.quantity_received, 0) - NVL (plla.quantity_cancelled, 0) on_order_qty
  FROM po_lines_all pll, po_line_locations_all plla, xxwc.XXWC_INV_ONHAND_MV##XD dr
 WHERE     pll.item_id = dr.inventory_item_id
       AND NVL (pll.cancel_flag, ''N'') = ''N''
       AND NVL (pll.closed_flag, ''N'') = ''N''
       AND pll.po_line_id = plla.po_line_id
       AND plla.ship_to_organization_id = dr.organization_id
       AND dr.organization_id=R.organization_id;
      COMMIT;
    END LOOP;
END;';

        EXECUTE IMMEDIATE v_sql_string;

        COMMIT;

        xxwc_mv_routines_add_pkg.drop_temp_table ('XXWC', 'XXWC_INV_ONHAND_MV##X');
        xxwc_mv_routines_add_pkg.table_create_from_old ('XXWC', 'XXWC_INV_ONHAND_MV##Z', 'XXWC_INV_ONHAND_MV##X');

        v_sql_string := 'INSERT /*+ APPEND */ INTO XXWC.XXWC_INV_ONHAND_MV##X
        SELECT  organization_id,inventory_item_id,round(SUM(on_order_qty),2)
  FROM XXWC.XXWC_INV_ONHAND_MV##Z
  GROUP BY organization_id,inventory_item_id';

        EXECUTE IMMEDIATE v_sql_string;

        COMMIT;

        xxwc_mv_routines_add_pkg.drop_temp_table ('XXWC', 'XXWC_INV_ONHAND_MV##Z');

        --********************************************
        ---POPULATE on_order_qty INTO DRIVER TABLE
        --********************************************
        xxwc_mv_routines_add_pkg.drop_temp_table ('XXWC', 'XXWC_INV_ONHAND_MV##ZD');
        xxwc_mv_routines_add_pkg.table_create_from_old ('XXWC', 'XXWC_INV_ONHAND_MV##XD', 'XXWC_INV_ONHAND_MV##ZD');

        EXECUTE IMMEDIATE 'ALTER TABLE XXWC.XXWC_INV_ONHAND_MV##ZD ADD(on_order_qty NUMBER)';

        v_sql_string :=
            'INSERT/*+ APPEND */ INTO XXWC.XXWC_INV_ONHAND_MV##ZD
   SELECT A.*,NVL(B.on_order_qty,0) FROM XXWC.XXWC_INV_ONHAND_MV##XD A,XXWC.XXWC_INV_ONHAND_MV##X B
   WHERE A.INVENTORY_ITEM_ID=B.INVENTORY_ITEM_ID(+) AND A.ORGANIZATION_ID=B.ORGANIZATION_ID(+)';

        EXECUTE IMMEDIATE v_sql_string;

        COMMIT;
        elapsed_time ('populate on_order_qty', g_start);
        xxwc_mv_routines_add_pkg.drop_temp_table ('XXWC', 'XXWC_INV_ONHAND_MV##XD');
        xxwc_mv_routines_add_pkg.drop_temp_table ('XXWC', 'XXWC_INV_ONHAND_MV##X');
        --DRIVER TABLE XXWC.XXWC_INV_ONHAND_MV##R NOW, ALL OTHER TABLES WERE DROPPED;
        --********************************************
        ---POPULATE service_qty
        --********************************************
        xxwc_mv_routines_add_pkg.drop_temp_table ('XXWC', 'XXWC_INV_ONHAND_MV##Z');
        v_sql_string := 'CREATE TABLE xxwc.xxwc_inv_onhand_mv##Z

(    organization_id     NUMBER
   ,inventory_item_id   NUMBER
   ,service_qty           NUMBER

)';

        EXECUTE IMMEDIATE v_sql_string;

        xxwc_mv_routines_add_pkg.alter_table_temp ('XXWC', 'XXWC_INV_ONHAND_MV##Z');
        v_sql_string := 'BEGIN
    FOR r IN (SELECT organization_id
                FROM org_organization_definitions
               WHERE operating_unit = 162 AND NVL (disable_date, SYSDATE + 1) > SYSDATE)
    LOOP
    INSERT /*+ APPEND */ INTO XXWC.XXWC_INV_ONHAND_MV##Z
        SELECT oola.ship_from_org_id organization_id
      ,oola.inventory_item_id
      , (  oola.ordered_quantity
                  - NVL (oola.shipped_quantity, 0)
                  - NVL (oola.cancelled_quantity, 0))
           service_qty

   FROM oe_order_lines_all oola, wsh_delivery_details wdd
       WHERE         oola.line_category_code = ''ORDER''
         AND NVL (oola.cancelled_flag, ''N'') = ''N''
         AND NVL (oola.booked_flag, ''N'') = ''Y''
         AND oola.line_id = wdd.source_line_id
         AND wdd.source_code = ''OE''
         AND wdd.released_status = ''B''
         and oola.ship_from_org_id=r.organization_id ;
       COMMIT;
    END LOOP;
END;';

        EXECUTE IMMEDIATE v_sql_string;

        COMMIT;

        xxwc_mv_routines_add_pkg.drop_temp_table ('XXWC', 'XXWC_INV_ONHAND_MV##X');
        xxwc_mv_routines_add_pkg.table_create_from_old ('XXWC', 'XXWC_INV_ONHAND_MV##Z', 'XXWC_INV_ONHAND_MV##X');

        v_sql_string := 'BEGIN
    FOR r IN (SELECT organization_id
                FROM org_organization_definitions
               WHERE operating_unit = 162 AND NVL (disable_date, SYSDATE + 1) > SYSDATE)
    LOOP
    INSERT /*+ APPEND */ INTO XXWC.XXWC_INV_ONHAND_MV##X
       SELECT  organization_id, inventory_item_id, round(SUM (service_qty),2) service_qty
  FROM XXWC.XXWC_INV_ONHAND_MV##Z where organization_id =r.organization_id
  GROUP BY organization_id,inventory_item_id;
   COMMIT;
    END LOOP;
END;';

        EXECUTE IMMEDIATE v_sql_string;

        COMMIT;

        xxwc_mv_routines_add_pkg.drop_temp_table ('XXWC', 'XXWC_INV_ONHAND_MV##Z');
        --POPULATE DRIVER TABLE WITH service_qty
        xxwc_mv_routines_add_pkg.drop_temp_table ('XXWC', 'XXWC_INV_ONHAND_MV##XD');
        xxwc_mv_routines_add_pkg.table_create_from_old ('XXWC', 'XXWC_INV_ONHAND_MV##ZD', 'XXWC_INV_ONHAND_MV##XD');

        EXECUTE IMMEDIATE 'ALTER TABLE XXWC.XXWC_INV_ONHAND_MV##XD ADD(service_qty NUMBER)';

        v_sql_string := 'INSERT/*+ APPEND */ INTO XXWC.XXWC_INV_ONHAND_MV##XD
   SELECT A.*, B.service_qty  FROM XXWC.XXWC_INV_ONHAND_MV##ZD A,XXWC.XXWC_INV_ONHAND_MV##X B
   WHERE A.INVENTORY_ITEM_ID=B.INVENTORY_ITEM_ID(+) AND A.ORGANIZATION_ID=B.ORGANIZATION_ID(+)';

        EXECUTE IMMEDIATE v_sql_string;

        COMMIT;
        elapsed_time ('populate service_qty', g_start);
        xxwc_mv_routines_add_pkg.drop_temp_table ('XXWC', 'XXWC_INV_ONHAND_MV##ZD');
        xxwc_mv_routines_add_pkg.drop_temp_table ('XXWC', 'XXWC_INV_ONHAND_MV##X');
        --****************************************************************
        --*****************************************************************
        --*****************************************************************
        --********************************************
        ---POPULATE transit_qty
        --********************************************
        xxwc_mv_routines_add_pkg.drop_temp_table ('XXWC', 'XXWC_INV_ONHAND_MV##Z');
        v_sql_string := 'CREATE TABLE xxwc.xxwc_inv_onhand_mv##z
(
    from_org             NUMBER
   ,organization_id      NUMBER
   ,inventory_item_id    NUMBER
   ,ordered_quantity     NUMBER
   ,shipped_quantity     NUMBER
   ,cancelled_quantity   NUMBER
   ,line_status          VARCHAR2 (25)
   ,order_date           DATE
   ,shipped_date         DATE
   ,requested_qty        NUMBER
   ,received_qty         NUMBER
   ,po_ref_num           VARCHAR2 (225)
)';

        EXECUTE IMMEDIATE v_sql_string;

        xxwc_mv_routines_add_pkg.alter_table_temp ('XXWC', 'XXWC_INV_ONHAND_MV##Z');
        v_sql_string := 'INSERT /*+ APPEND */ INTO XXWC.XXWC_INV_ONHAND_MV##Z
     SELECT mp_from.organization_id
      ,mp_to.organization_id organization_id
      ,msib.inventory_item_id
      ,NVL (oola.ordered_quantity, 0) ordered_quantity
      ,NVL (oola.shipped_quantity, 0) shipped_quantity
      ,NVL (oola.cancelled_quantity, 0) cancelled_quantity
      ,oola.flow_status_code
      ,ooha.creation_date
      ,oola.actual_shipment_date
      ,req_ln.quantity
      ,NVL (req_ln.quantity_received, 0)
      ,req_hdr.segment1
  FROM po_requisition_headers_all req_hdr
      ,po_requisition_lines_all req_ln
      ,oe_order_headers_all ooha
      ,oe_order_sources oos
      ,oe_order_lines_all oola
      ,mtl_parameters mp_from
      ,org_organization_definitions ood_from
      ,mtl_parameters mp_to
      ,org_organization_definitions ood_to
      ,mtl_system_items_b msib
      ,hr_operating_units hou
 WHERE     req_hdr.type_lookup_code = ''INTERNAL''
       AND req_hdr.requisition_header_id = req_ln.requisition_header_id
       AND req_hdr.requisition_header_id = ooha.source_document_id(+)
       AND ooha.order_source_id = oos.order_source_id(+)
       AND oos.name(+) = ''Internal''
       AND req_ln.requisition_header_id = oola.source_document_id(+)
       AND req_ln.requisition_line_id = oola.source_document_line_id(+)
       AND NVL (oola.cancelled_flag(+), ''N'') = ''N''
       AND NVL (oola.booked_flag(+), ''N'') = ''Y''
       AND req_ln.source_organization_id = mp_from.organization_id
       AND mp_from.organization_id = ood_from.organization_id
       AND req_ln.destination_organization_id = mp_to.organization_id
       AND mp_to.organization_id = ood_to.organization_id
       AND req_ln.item_id = msib.inventory_item_id
       AND req_ln.destination_organization_id = msib.organization_id
       AND ood_from.operating_unit = hou.organization_id';

        EXECUTE IMMEDIATE v_sql_string;

        COMMIT;

        xxwc_mv_routines_add_pkg.drop_temp_table ('XXWC', 'XXWC_INV_ONHAND_MV##t');
        xxwc_mv_routines_add_pkg.table_create_from_old ('XXWC', 'XXWC_INV_ONHAND_MV##Z', 'XXWC_INV_ONHAND_MV##t');
        v_sql_string := 'INSERT /*+ APPEND */ INTO XXWC.XXWC_INV_ONHAND_MV##t select distinct a.* from
            XXWC.XXWC_INV_ONHAND_MV##z a';

        EXECUTE IMMEDIATE v_sql_string;

        COMMIT;
        xxwc_mv_routines_add_pkg.drop_temp_table ('XXWC', 'XXWC_INV_ONHAND_MV##z');

        xxwc_mv_routines_add_pkg.drop_temp_table ('XXWC', 'XXWC_INV_ONHAND_MV##X');
        v_sql_string := 'CREATE TABLE xxwc.xxwc_inv_onhand_mv##x

(    organization_id     NUMBER
   ,inventory_item_id   NUMBER
   ,transit_qty           NUMBER

)';

        EXECUTE IMMEDIATE v_sql_string;

        v_sql_string :=
            'INSERT /*+ APPEND */ INTO XXWC.XXWC_INV_ONHAND_MV##X
       SELECT  organization_id, inventory_item_id, round(SUM (ordered_quantity -shipped_quantity-cancelled_quantity ),2) transit_qty
  FROM XXWC.XXWC_INV_ONHAND_MV##t
  GROUP BY organization_id,inventory_item_id';

        EXECUTE IMMEDIATE v_sql_string;

        COMMIT;
        xxwc_mv_routines_add_pkg.drop_temp_table ('XXWC', 'XXWC_INV_ONHAND_MV##t');
        elapsed_time ('populate transit_qty_table', g_start);
        --POPULATE DRIVER TABLE WITH transit_qty
        xxwc_mv_routines_add_pkg.drop_temp_table ('XXWC', 'XXWC_INV_ONHAND_MV##ZD');
        xxwc_mv_routines_add_pkg.table_create_from_old ('XXWC', 'XXWC_INV_ONHAND_MV##XD', 'XXWC_INV_ONHAND_MV##ZD');

        EXECUTE IMMEDIATE 'ALTER TABLE XXWC.XXWC_INV_ONHAND_MV##ZD ADD(transit_qty NUMBER)';

        v_sql_string :=
            'INSERT/*+ APPEND */ INTO XXWC.XXWC_INV_ONHAND_MV##ZD
   SELECT A.*,NVL(B.transit_qty,0) FROM XXWC.XXWC_INV_ONHAND_MV##XD A,XXWC.XXWC_INV_ONHAND_MV##X B
   WHERE A.INVENTORY_ITEM_ID=B.INVENTORY_ITEM_ID(+) AND A.ORGANIZATION_ID=B.ORGANIZATION_ID(+)';

        EXECUTE IMMEDIATE v_sql_string;

        COMMIT;
        elapsed_time ('populate transit_qty', g_start);
        xxwc_mv_routines_add_pkg.drop_temp_table ('XXWC', 'XXWC_INV_ONHAND_MV##XD');
        xxwc_mv_routines_add_pkg.drop_temp_table ('XXWC', 'XXWC_INV_ONHAND_MV##X');
        --****************************************************************
        --*****************************************************************
        --*****************************************************************

        --********************************************
        ---POPULATE vendor_consigned_qty
        --********************************************
        xxwc_mv_routines_add_pkg.drop_temp_table ('XXWC', 'XXWC_INV_ONHAND_MV##Z');
        v_sql_string := 'CREATE TABLE xxwc.xxwc_inv_onhand_mv##Z

(    organization_id     NUMBER
   ,inventory_item_id   NUMBER
   ,vendor_consigned_qty           NUMBER

)';

        EXECUTE IMMEDIATE v_sql_string;

        xxwc_mv_routines_add_pkg.alter_table_temp ('XXWC', 'XXWC_INV_ONHAND_MV##Z');
        v_sql_string :=
            'INSERT /*+ APPEND */ INTO XXWC.XXWC_INV_ONHAND_MV##Z
       SELECT organization_id, inventory_item_id, ROUND (SUM (primary_transaction_quantity), 2) vendor_consigned_qty
    FROM mtl_onhand_quantities_detail
   WHERE NVL (is_consigned, 2) != 2
GROUP BY organization_id, inventory_item_id';

        EXECUTE IMMEDIATE v_sql_string;

        COMMIT;

        --POPULATE DRIVER TABLE WITH vendor_consigned_qty
        xxwc_mv_routines_add_pkg.drop_temp_table ('XXWC', 'XXWC_INV_ONHAND_MV##RD');
        xxwc_mv_routines_add_pkg.table_create_from_old ('XXWC', 'XXWC_INV_ONHAND_MV##ZD', 'XXWC_INV_ONHAND_MV##RD');

        EXECUTE IMMEDIATE 'ALTER TABLE XXWC.XXWC_INV_ONHAND_MV##RD ADD(vendor_consigned_qty NUMBER)';

        v_sql_string :=
            'INSERT/*+ APPEND */ INTO XXWC.XXWC_INV_ONHAND_MV##RD
   SELECT A.*,NVL(B.vendor_consigned_qty,0) FROM XXWC.XXWC_INV_ONHAND_MV##ZD A,XXWC.XXWC_INV_ONHAND_MV##Z B
   WHERE A.INVENTORY_ITEM_ID=B.INVENTORY_ITEM_ID(+) AND A.ORGANIZATION_ID=B.ORGANIZATION_ID(+)';

        EXECUTE IMMEDIATE v_sql_string;

        COMMIT;
        elapsed_time ('populate vendor_consigned_qty', g_start);
        xxwc_mv_routines_add_pkg.drop_temp_table ('XXWC', 'XXWC_INV_ONHAND_MV##ZD');
        xxwc_mv_routines_add_pkg.drop_temp_table ('XXWC', 'XXWC_INV_ONHAND_MV##Z');
        --****************************************************************
        --POPULATE DRIVER TABLE WITH Sales_Velocity_CAT
        xxwc_mv_routines_add_pkg.drop_temp_table ('XXWC', 'XXWC_INV_ONHAND_MV##XD');
        xxwc_mv_routines_add_pkg.table_create_from_old ('XXWC', 'XXWC_INV_ONHAND_MV##RD', 'XXWC_INV_ONHAND_MV##XD');

        EXECUTE IMMEDIATE 'ALTER TABLE XXWC.XXWC_INV_ONHAND_MV##XD ADD(Sales_Velocity_CAT VARCHAR2(56))';

        v_sql_string :=
            'INSERT/*+ APPEND */ INTO XXWC.XXWC_INV_ONHAND_MV##XD
   SELECT A.*, NVL(B.segment1,''X'')  FROM XXWC.XXWC_INV_ONHAND_MV##RD A,mtl_item_categories_v B
   WHERE A.INVENTORY_ITEM_ID=B.INVENTORY_ITEM_ID(+) AND A.ORGANIZATION_ID=B.ORGANIZATION_ID(+)
   AND B.category_set_name(+) = ''Sales Velocity''';

        EXECUTE IMMEDIATE v_sql_string;

        COMMIT;
        elapsed_time ('populate Sales_Velocity_CAT', g_start);
        xxwc_mv_routines_add_pkg.drop_temp_table ('XXWC', 'XXWC_INV_ONHAND_MV##RD');

        --****************************************************************
        --*****************************************************************
        --on_hand_tree_values
        --*****************************************************************

        --HERE WAIT for jobs to finish
        BEGIN
            xxwc_mv_routines_add_pkg.wait_for_jobs;
        END;

        BEGIN
            EXECUTE IMMEDIATE
                'BEGIN
    APPS.xxwc_mv_routines_add_pkg.drop_temp_table (''XXWC'', ''xxwc_inv_onhand_mv##h'');

    EXECUTE IMMEDIATE
        ''create table xxwc.xxwc_inv_onhand_mv##h (
organization_id number,
inventory_item_id number,
available_qty number,
defective_qty number,
run_quantity number,
general_quantity number)'';

    APPS.xxwc_mv_routines_add_pkg.alter_table_temp (''XXWC'', ''xxwc_inv_onhand_mv##h'');

    FOR r IN (SELECT * FROM XXWC.populate_get_onhand##)
    LOOP
        EXECUTE IMMEDIATE '' INSERT /*+ append */
                  INTO  XXWC.xxwc_inv_onhand_mv##h
                  select * from XXWC.'' || r.table_name;

        COMMIT;
     END LOOP;
     END;';
        END;

        --select count('a') from xxwc.xxwc_inv_onhand_mv##h
        ----clean after

        BEGIN
            EXECUTE IMMEDIATE 'BEGIN
    FOR r IN (SELECT * FROM XXWC.populate_get_onhand##)
    LOOP
        APPS.xxwc_mv_routines_add_pkg.drop_temp_table (''XXWC'', r.table_name);
    END LOOP;
    APPS.xxwc_mv_routines_add_pkg.drop_temp_table (''XXWC'', ''populate_get_onhand##'');
END;';
        END;

        --******************************************************

        elapsed_time ('populate quantity tree in parallel', g_start);
        --****************************************************************
        --*****************************************************************
        --POPULATE FINAL TABLE  xxwc.xxwc_inv_onhand_mv##XX
        xxwc_mv_routines_add_pkg.drop_temp_table ('XXWC', 'XXWC_INV_ONHAND_MV##XX');
        v_sql_string := 'CREATE TABLE xxwc.xxwc_inv_onhand_mv##xx
AS
    SELECT ''WHITECAP'' business_unit
          ,''Oracle EBS'' source_system
          ,x1.operating_unit_id
          ,x1.operating_unit_name
          ,x1.inventory_item_id
          ,x1.organization_id
          ,SYSDATE interface_date
          ,x1.branch_name
          ,x1.branch_loc_num
          ,x1.bu_sku
          ,x1.long_description
          ,x1.short_description
          ,x1.origindate
          ,SYSDATE snapshotdate
          ,x1.moving_avg_cost
          ,x1.onhand_qty
          ,x1.on_order_qty
          ,x1.service_qty
          ,x1.transit_qty
          ,0 available_qty
          ,0 cust_consigned_qty
          ,x1.vendor_consigned_qty
          ,0 review_qty
          ,0 defective_qty
          ,0 overship_qty
          ,0 display_qty
          ,0 stock_qty
      FROM xxwc.xxwc_inv_onhand_mv##xd x1
     WHERE 1 = 2';

        EXECUTE IMMEDIATE v_sql_string;

        xxwc_mv_routines_add_pkg.alter_table_temp ('XXWC', 'XXWC_INV_ONHAND_MV##XX');

        v_sql_string := 'INSERT /*+ APPEND */ INTO
       xxwc.xxwc_inv_onhand_mv##XX  SELECT ''WHITECAP'' business_unit
          ,''Oracle EBS'' source_system
          ,x1.operating_unit_id
          ,x1.operating_unit_name
          ,x1.inventory_item_id
          ,x1.organization_id
          ,SYSDATE
          ,x1.branch_name
          ,x1.branch_loc_num
          ,x1.bu_sku
          ,x1.long_description
          ,x1.short_description
          ,x1.origindate
          ,SYSDATE
          ,x1.moving_avg_cost
          ,x1.onhand_qty
          ,x1.on_order_qty
          ,x1.service_qty
          ,x1.transit_qty
          ,h.available_qty
          ,0 cust_consigned_qty
          ,x1.vendor_consigned_qty
          ,0 review_qty
          ,h.defective_qty
          ,ROUND ( (CASE
                        WHEN h.run_quantity< 0
                        THEN
                            h.run_quantity
                        ELSE
                            0
                    END)
                 ,2)
               overship_qty
          ,0 display_qty
          ,ROUND ( (CASE
                        WHEN x1.sales_velocity_cat != ''N''
                        THEN
                           h.general_quantity
                        ELSE
                            0
                    END)
                 ,2)
               stock_qty
      FROM xxwc.xxwc_inv_onhand_mv##xd x1,xxwc.xxwc_inv_onhand_mv##h h
      where x1.organization_id =h.organization_id and x1.inventory_item_id =h.inventory_item_id';

        EXECUTE IMMEDIATE v_sql_string;

        COMMIT;
        elapsed_time ('populate final table', g_start);
        xxwc_mv_routines_add_pkg.drop_temp_table ('XXWC', 'XXWC_INV_ONHAND_MV##h');
        xxwc_mv_routines_add_pkg.drop_temp_table ('XXWC', 'XXWC_INV_ONHAND_MV##xd');
        xxwc_mv_routines_add_pkg.drop_temp_table ('XXWC', 'XXWC_INV_ONHAND_MV##Zr');
        xxwc_mv_routines_add_pkg.drop_temp_table ('XXWC', 'USER_ITEM_DESCRIPTION##');

        EXECUTE IMMEDIATE 'truncate table APPS.XXWC_INV_ONHAND_QUANTITY_MV';
        commit;

        xxwc_mv_routines_add_pkg.alter_table_temp ('APPS', 'XXWC_INV_ONHAND_QUANTITY_MV');

        EXECUTE IMMEDIATE
            'insert /*+append */ into APPS.XXWC_INV_ONHAND_QUANTITY_MV select * from XXWC.XXWC_INV_ONHAND_MV##XX';

        COMMIT;

        xxwc_mv_routines_add_pkg.drop_temp_table ('XXWC', 'XXWC_INV_ONHAND_MV##XX');

		disable_parallelism; --Added for 2.2 - TMS#20160407-00197
		
    EXCEPTION
        WHEN OTHERS
        THEN
            l_error_message :=
                   'xxwc_arexti '
                || 'Error_Stack...'
                || DBMS_UTILITY.format_error_stack ()
                || ' Error_Backtrace...'
                || DBMS_UTILITY.format_error_backtrace ();
            DBMS_OUTPUT.put_line ('Error when running populate onhand mv' || l_error_message);
            p_retcode := '2';
            p_errbuf := l_error_message;
    END;

    ---******************************************************************************

    PROCEDURE populate_get_onhand_qty (p_table_name VARCHAR2, p_organization_id NUMBER)
    IS
    BEGIN
        xxwc_mv_routines_add_pkg.drop_temp_table ('XXWC', p_table_name);

        EXECUTE IMMEDIATE 'create table XXWC.' || p_table_name || ' (
organization_id number,
inventory_item_id number,
available_qty number,
defective_qty number,
run_quantity number,
general_quantity number) ';

        xxwc_mv_routines_add_pkg.alter_table_temp ('XXWC', p_table_name);

        EXECUTE IMMEDIATE
               '
            INSERT /*+ append */
                  INTO XXWC.'
            || p_table_name
            || '  SELECT x1.organization_id
      ,x1.inventory_item_id
      ,ROUND ( xxwc_mv_routines_pkg.get_onhand_qty (x1.inventory_item_id
                                                  ,x1.organization_id
                                                  ,NULL
                                                  ,''ATR'')
             ,2)
           available_qty
      ,ROUND ( xxwc_mv_routines_pkg.get_onhand_qty (x1.inventory_item_id
                                                  ,x1.organization_id
                                                  ,''Damage RTV''
                                                  ,''QOH'')
             ,2)
           defective_qty
      , xxwc_mv_routines_pkg.get_onhand_qty (x1.inventory_item_id
                                           ,x1.organization_id
                                           ,NULL
                                           ,''QOH'')
           run_quantity
      , xxwc_mv_routines_pkg.get_onhand_qty (x1.inventory_item_id
                                                                ,x1.organization_id
                                                                ,''General''
                                                                ,''QOH'')
           general_quantity
  FROM   xxwc.XXWC_INV_ONHAND_MV##Zr  X1 WHERE
                        X1.organization_id ='
            || p_organization_id;

        COMMIT;
    END;

    -- PROCEDURE wait_for_jobs
    PROCEDURE wait_for_jobs
    IS
    BEGIN
        FOR r IN (SELECT '1'
                    FROM user_jobs
                   WHERE what LIKE '%populate_get_onhand_qty%' AND ROWNUM = 1)
        LOOP
            apps.xxwc_sleep (10);
            wait_for_jobs;
        END LOOP;
    END;

/****************************************************************************************************************
    Module Name: enable_parallelism

    PURPOSE: Procedure to enable parallelism

    REVISIONS:
    Ver        Date        Author             Description
    ---------  ----------  -------------    ---------------------------------------------------------------------
    2.1        04/19/2016  Rakesh Patel     TMS#20160407-00197 - Enabling/Disabling parallelism at seesion level
   ****************************************************************************************************************/
   PROCEDURE enable_parallelism
   IS
      l_distro_list   VARCHAR2 (75)
                         DEFAULT 'HDSOracleDevelopers@hdsupply.com';
   BEGIN
      EXECUTE IMMEDIATE 'ALTER SESSION FORCE PARALLEL DML PARALLEL 8';

      EXECUTE IMMEDIATE 'ALTER SESSION FORCE PARALLEL DDL PARALLEL 8';
   EXCEPTION
      WHEN OTHERS
      THEN
         l_error_message :=
               'Error_Stack...'
            || DBMS_UTILITY.format_error_stack ()
            || ' Error_Backtrace...'
            || DBMS_UTILITY.format_error_backtrace ();

         xxcus_error_pkg.xxcus_error_main_api (
            p_called_from         => 'XXWC_MV_ROUTINES_ADD_PKG',
            p_calling             => 'ENABLE_PARALLELISM',
            p_request_id          => NULL,
            p_ora_error_msg       => l_error_message,
            p_error_desc          => 'Error running in xxwc_mv_routines_add_pkg in WHEN OTHERS',
            p_distribution_list   => l_distro_list,
            p_module              => 'FND');
   END;

   /****************************************************************************************************************
    Module Name: disable_parallelism

    PURPOSE: Procedure to disable parallelism

    REVISIONS:
    Ver        Date        Author             Description
    ---------  ----------  -------------    ---------------------------------------------------------------------
    2.2        04/19/2016  Rakesh Patel     TMS#20160407-00197 - Enabling/Disabling parallelism at seesion level
   ****************************************************************************************************************/
   PROCEDURE disable_parallelism
   IS
      l_distro_list   VARCHAR2 (75)
                         DEFAULT 'HDSOracleDevelopers@hdsupply.com';
   BEGIN
      EXECUTE IMMEDIATE 'ALTER SESSION DISABLE PARALLEL DML';

      EXECUTE IMMEDIATE 'ALTER SESSION DISABLE PARALLEL DDL';
   EXCEPTION
      WHEN OTHERS
      THEN
         l_error_message :=
               'Error_Stack...'
            || DBMS_UTILITY.format_error_stack ()
            || ' Error_Backtrace...'
            || DBMS_UTILITY.format_error_backtrace ();

         xxcus_error_pkg.xxcus_error_main_api (
            p_called_from         => 'XXWC_MV_ROUTINES_ADD_PKG',
            p_calling             => 'DISABLE_PARALLELISM',
            p_request_id          => NULL,
            p_ora_error_msg       => l_error_message,
            p_error_desc          => 'Error running in xxwc_mv_routines_add_pkg in WHEN OTHERS',
            p_distribution_list   => l_distro_list,
            p_module              => 'FND');
   END;
END xxwc_mv_routines_add_pkg;
/