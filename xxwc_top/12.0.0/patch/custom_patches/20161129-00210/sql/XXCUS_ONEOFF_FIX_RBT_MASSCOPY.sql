   /*
      ************************************************************************
    HD Supply
    All rights reserved.
   **************************************************************************
     PURPOSE:   GSC Current day cash positioning BAI file process 

     REVISIONS:
     Ver        Date                Author                           Description
     ---------  ----------         ---------------                  -------------------------------------------------------------------------------------------------------------
     1.0       11/15/2016  Balaguru Seshadri     TMS:  20161129-00210  Update Mass Copy Flag                                                                                       
   ************************************************************************* 
  */
DECLARE
   l_db_name    VARCHAR2 (20) := NULL;
   l_path_inb   VARCHAR2 (240) := NULL;
   l_path_oub   VARCHAR2 (240) := NULL;
   l_sql        VARCHAR2 (240) := NULL;
   g_command    VARCHAR2 (2000) := NULL; 
   l_result     VARCHAR2 (2000) := NULL;    
--
cursor c is
select b.qp_list_header_id list_header_id, a.name, a.description, a.attribute7 offer_cal_year, attribute3 curr_mass_copy_value  
from apps.qp_list_headers_vl a, ozf.ozf_offers b
 where 1 = 1
   and b.qp_list_header_id = a.list_header_id
   and a.attribute7 = '2016'
   and a.LIST_HEADER_ID in 
    (
    3675924,
    3670014,
    3675918,
    3670013
    ); 
--
BEGIN
   --
   SELECT LOWER (name) INTO l_db_name FROM v$database;
   --
   DBMS_OUTPUT.PUT_LINE(' ');   
   DBMS_OUTPUT.PUT_LINE('******* INSTANCE: '||l_db_name||' **********');
   DBMS_OUTPUT.PUT_LINE(' ');
   --
    begin   
                  --
                  for rec in c loop
                     --
                    dbms_output.put_line('Before update, List header id :'||rec.list_header_id||', Offer Name :'||rec.description||' ['||rec.name||']'||', Cal  Year :'||rec.offer_cal_year||', curr_mass_copy_value :'||rec.curr_mass_copy_value);
                    --
                    savepoint sqr1;
                    --
                    update qp_list_headers_b
                    set attribute3 =Null
                    where 1 =1
                    and list_header_id =rec.list_header_id;
                    --
                    if (sql%rowcount >0) then
                         dbms_output.put_line('After update, List header id :'||rec.list_header_id||', Offer Name :'||rec.description||' ['||rec.name||']'||', Cal  Year :'||rec.offer_cal_year||', curr_mass_copy_value is null');                    
                    else
                      rollback to sqr1;
                    end if;           
                    --
                  end loop;    
                 --
                 commit;
                 --
    exception
        when others then
         dbms_output.put_line('Error in cursor loop thru to update mass copy flag..., message =>'||sqlerrm);
         rollback;
    end;
    -- 
EXCEPTION
 WHEN OTHERS THEN
  DBMS_OUTPUT.PUT_LINE('OUTER BLOCK : '||SQLERRM);
END;
/