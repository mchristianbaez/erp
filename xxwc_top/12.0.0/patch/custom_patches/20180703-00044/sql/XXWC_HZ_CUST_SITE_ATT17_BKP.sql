   /***************************************************************************
*    script name: XXWC_HZ_CUST_SITE_ATT17_BKP.sql
*    version    date              author             description
**********************************************************************
*    1.0        04-Jul-2018    Pattabhi        TMS#20180703-00044 initial development.
*********************************************************************/
CREATE TABLE XXWC.XXWC_HZ_CUST_SITES_ALL_BKP AS (SELECT * FROM APPS.HZ_CUST_ACCT_SITES_ALL WHERE ORG_ID=162)
/