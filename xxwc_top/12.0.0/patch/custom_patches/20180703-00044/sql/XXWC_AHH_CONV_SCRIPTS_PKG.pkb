CREATE OR REPLACE PACKAGE BODY APPS.XXWC_AHH_CONV_SCRIPTS_PKG
IS
   /******************************************************************************************************************************************************
        $Header XXWC_AHH_CONV_SCRIPTS_PKG $
        Module Name: XXWC_AHH_CONV_SCRIPTS_PKG.pkb

        PURPOSE:   AH Harries Conversion Project

        REVISIONS:
        Ver        Date        Author           Description
        ---------  ----------  ---------------  ------------------------------------------------------------------------------------------------
        1.0        07/03/2018  Pattabhi Avula   TMS#20180703-00044 - XXWC AHH Script � Prism Site Number
   ******************************************************************************************************************************************************/

   g_err_callfrom      VARCHAR2 (1000) := 'XXWC_AHH_CONV_SCRIPTS_PKG.update_prism_site_dff';
   g_module            VARCHAR2 (100)  := 'XXWC';
   g_distro_list       VARCHAR2 (80)   := 'wc-itdevalerts-u1@hdsupply.com'; 
   l_sec               VARCHAR2 (240);   

   PROCEDURE Update_Prism_Site_DFF (errbuff       OUT VARCHAR2,
                                    retcode       OUT VARCHAR2,
									p_to_date     IN  VARCHAR2)
   IS
      -- =====================================================================================================================================================
      -- Procedure: Update_Prism_Site_DFF
      -- Purpose: Update_Prism_Site_DFF
      -- ====================================================================================================================================================
      --  REVISIONS:
      --   Ver        Date        Author            Description
      --  ---------  ---------- ---------------     ------------------------------------------------------------------------------------------------
      --  1.0        07/03/2018  Pattabhi Avula     TMS#20180703-00044 - XXWC AHH Script � Prism Site Number
      -- ====================================================================================================================================================
	  
	l_count           NUMBER:=0;
	l_locked_line     VARCHAR2(2);
    CURSOR cur
      IS
       SELECT ROWID, cust_acct_site_id
         FROM apps.hz_cust_acct_sites_all
        WHERE org_id = 162
          AND attribute17 IS NOT NULL
          AND TRUNC (creation_date) < FND_DATE.CANONICAL_TO_DATE(p_to_date);
		  
BEGIN
 l_sec:='Before loop';
 fnd_file.put_line(fnd_file.log, 'Starting of log file '||p_to_date);
   FOR rec IN cur
     LOOP
	 l_locked_line := xxwc_om_force_ship_pkg.is_row_locked (rec.ROWID, 'HZ_CUST_ACCT_SITES_ALL'); 
     		
		IF l_locked_line = 'N' 
		 THEN
		   l_count:=l_count+1; 
           UPDATE /*+ HZ_CUST_ACCT_SITES_U1 */ apps.hz_cust_acct_sites_all
              SET attribute17 = NULL
            WHERE cust_acct_site_id = rec.cust_acct_site_id;			
		
		    IF l_count >= 10000 THEN
		    COMMIT;
		    l_count:=0;
		    END IF;
		END IF;
		
   END LOOP;

 l_sec:='After loop';
   COMMIT;
EXCEPTION
   WHEN OTHERS
   THEN
      COMMIT;	
fnd_file.put_line(fnd_file.log, 'Unexpected error : '||SQLERRM);	  
xxcus_error_pkg.xxcus_error_main_api(p_called_from       => g_err_callfrom,
                                     p_calling           => l_sec,
                                     p_request_id        => fnd_global.conc_request_id,
                                     p_ora_error_msg     => SQLERRM,
                                     p_error_desc        => 'Error while updating the hz_cust_acct_sites_all table for Prism Site Number',
                                     p_distribution_list => g_distro_list,
                                     p_module            => g_module);

END Update_Prism_Site_DFF;
PROCEDURE update_dup_cust_locations(errbuff       OUT VARCHAR2,
                                    retcode       OUT VARCHAR2,
									p_to_date     IN  VARCHAR2)
   IS
      -- =====================================================================================================================================================
      -- Procedure: update_dup_cust_locations
      -- Purpose: update_dup_cust_locations
      -- ====================================================================================================================================================
      --  REVISIONS:
      --   Ver        Date        Author            Description
      --  ---------  ---------- ---------------     ------------------------------------------------------------------------------------------------
      --  1.0        07/04/2018  Pattabhi Avula     TMS#20180703-00044 - XXWC AHH Script � Prism Site Number
      -- ====================================================================================================================================================
	  
	
	l_locked_line     VARCHAR2(2);
    CURSOR cur
     IS
     SELECT  DISTINCT LOCATION
     FROM apps.HZ_CUST_SITE_USES_ALL
     WHERE TRUNC(creation_date) >  FND_DATE.CANONICAL_TO_DATE(p_to_date)
     GROUP BY LOCATION HAVING COUNT(DISTINCT cust_acct_site_id) > 1;
     
   BEGIN
     FOR rec IN CUR LOOP
       UPDATE apps.HZ_CUST_SITE_USES_ALL
          SET location = SUBSTR(location,1,32)||'.'||cust_acct_site_id
        WHERE location = rec.location;
     END LOOP;
     fnd_file.put_line(fnd_file.log,'Records updated is :'||SQL%ROWCOUNT);
     COMMIT;
     
     EXCEPTION
     WHEN OTHERS THEN
     fnd_file.put_line(fnd_file.log,'Exception - '||SQLERRM);
END update_dup_cust_locations;
END XXWC_AHH_CONV_SCRIPTS_PKG;
/
