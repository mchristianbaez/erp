/*************************************************************************
   *   $Header TMS20160601_00216_CREATE_DBDIR.sql $
   *   Module Name: TMS20160601_00216_CREATE_DBDIR.sql
   *
   *   PURPOSE:   This script is used to create db directory for TMS20160601_00216
   *
   *   REVISIONS:
   * Created By           TMS              Date          Version  Comments
   * ===========         ===============  ===========    =======  ===========
   * Niraj K Ranjan      20160601_00216   23-JUN-2016    2.4      Initial
 *******************************************************************************/
DECLARE
l_db_name VARCHAR2(20) :=NULL;
l_path VARCHAR2(240) :=NULL;
l_sql VARCHAR2(240) :=NULL;
l_dir_name VARCHAR2(30) := 'XXWC_AD_INACTIVE_USERS';
--
BEGIN

SELECT lower(name) 
  INTO l_db_name
  FROM v$database;

  l_path :='/xx_iface/'||l_db_name||'/inbound/ad/termed_contractors';

  l_sql :='CREATE OR REPLACE DIRECTORY '||l_dir_name||' as'||' '||''''||l_path||'''';

  dbms_output.put_line('DBA Directory Path: '||l_path);
  dbms_output.put_line(' ');
  dbms_output.put_line('SQL: '||l_sql);
  dbms_output.put_line(' ');  
  dbms_output.put_line('Begin setup of directory '||l_dir_name);  
  EXECUTE IMMEDIATE l_sql;
  dbms_output.put_line('End setup of directory '||l_dir_name);    
  
END;
/
GRANT ALL ON DIRECTORY XXWC_AD_INACTIVE_USERS TO PUBLIC WITH GRANT OPTION;
/

