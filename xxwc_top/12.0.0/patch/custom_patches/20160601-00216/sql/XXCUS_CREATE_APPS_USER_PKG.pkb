CREATE OR REPLACE PACKAGE BODY "APPS"."XXCUS_CREATE_APPS_USER_PKG" IS

  -- Error DEBUG
  l_err_callfrom  VARCHAR2(75) DEFAULT 'XXCUS_CREATE_APPS_USER_PKG';
  l_err_callpoint VARCHAR2(75) DEFAULT 'START';
  l_distro_list   VARCHAR2(75) DEFAULT 'HDSOracleDevelopers@hdsupply.com';
  g_dflt_email   fnd_user.email_address%TYPE    := 'WC-ITDEVALERTS-U1@HDSupply.com'; --Ver 2.4
  TYPE l_user_tab IS TABLE OF XXWC_INACTIVE_USER_LOAD_TBL%ROWTYPE   --Ver 2.4 
      INDEX BY BINARY_INTEGER;

  -- -----------------------------------------------------------------------------
  -- |----------------< MAIN Procedure to  Process Users  >-----------------------|
  -- -----------------------------------------------------------------------------
  --   VERSION DATE          AUTHOR(S)       DESCRIPTION
  -- ------- -----------   --------------- ---------------------------------
  -- 1.0     24-Apr-2011   Kathy Poling    Copied from R11 and changed for R12.
  -- 1.1     28-Mar-2013   Kathy Poling    SR 194120 FND User not being end dated
  --                                       also removed the max object_version_number 
  --                                       from the new emp cursor.       
  -- 2.0     19-Jun-2013   Luong Vu        Add business_unit to pass from xxcus_process_users
  --                                       to xxcus_create_a_user proc.
  --                                       Required for Canada OIE project. 
  -- 2.1     27-Mar-2014   Kathy Poling    Change to cursor cu_get_employees_to_delete, with the
  --                                       cleanup of employee tables for the shared install 
  --                                       correcting profiles the assignments will only have
  --                                       one record per employee and when they are terminated
  --                                       the assignment end date is the termination date. 
  --                                       SR 133025                             
  -- 2.2    29-Sep-2014   Vijay Srinivasan ESMS 260727 - HR interface update to remove indirect
  --                                       access from terminated employees (Project #20268)
  --                                       RFC 42489
  -- 2.3    06-May-2015  Maharajan Shunmugam ESMS#278322 HR Interface not adding Justification when terminating access
  -- 2.4    28-Jun-2016  Niraj K Ranjan    TMS#20160601-00216   Automate End-Dating Contractors in Oracle EBS
  -- 2.5    03-Aug-2016  Niraj K Ranjan    TMS#20160601-00216   Automate End-Dating Contractors in Oracle EBS
  -- -----------------------------------------------------------------------------

  PROCEDURE xxcus_process_users(errbuf_p  OUT VARCHAR2
                               ,retcode_p OUT NUMBER) IS
  
    CURSOR cu_get_new_employees IS
      SELECT papf.first_name
            ,papf.last_name
            ,papf.employee_number
            ,papf.full_name
            ,papf.effective_start_date
            ,papf.effective_end_date
            ,bd.nt_login               user_id
            ,pea.process_lev           business_unit -- v 2.0            
        FROM per_all_people_f             papf
            ,xxcus.xxcus_usr_login_tbl    bd
            ,xxcus.xxcushr_ps_emp_all_tbl pea -- v 2.0            
       WHERE
      /*papf.object_version_number =
      (SELECT MAX(papf1.object_version_number)
         FROM per_all_people_f papf1
        WHERE papf.person_id = papf1.person_id) */ --Version 1.1 this picks inactive employees to create a user
       bd.employee_number = papf.employee_number        
       AND papf.employee_number = pea.employee_number -- v 2.0         
       AND papf.employee_number IS NOT NULL
      /* AND (papf.effective_end_date = to_date('31-DEC-4712') OR
      papf.effective_end_date IS NULL)  */ --Version 1.1 this is date column and will never be null            
       AND papf.effective_end_date = '31-DEC-4712' --Version 1.1
       AND papf.person_type_id IN
       (SELECT person_type_id
          FROM per_person_types
         WHERE system_person_type = 'EMP'
           AND user_person_type = 'Employee')
       AND EXISTS (SELECT 1
          FROM hz_parties hzpa
         WHERE hzpa.party_id = papf.party_id
           AND hzpa.status = 'A')
       AND NOT EXISTS (SELECT 1
          FROM fnd_user fusr
         WHERE fusr.employee_id = papf.person_id);
  
    CURSOR cu_get_employees_to_delete IS
    --Version 1.1 changes   5/10/2013
    /*  SELECT papf.first_name
                                                                                                                                ,papf.person_id
                                                                                                                                ,papf.last_name
                                                                                                                                ,papf.employee_number
                                                                                                                                ,papf.full_name
                                                                                                                                ,papf.effective_start_date
                                                                                                                                ,papf.effective_end_date
                                                                                                                                ,bd.nt_login user_id
                                                                                                                                ,pos.final_process_date
                                                                                                                                ,users.user_name
                                                                                                                            FROM per_all_people_f          papf
                                                                                                                                ,per_periods_of_service    pos
                                                                                                                                ,apps.fnd_user             users
                                                                                                                                ,xxcus.xxcus_usr_login_tbl bd
                                                                                                                           WHERE papf.person_id = pos.person_id
                                                                                                                             AND papf.person_id = users.employee_id
                                                                                                                             AND bd.employee_number = papf.employee_number
                                                                                                                             AND pos.final_process_date =
                                                                                                                                 (SELECT MAX(a.final_process_date)
                                                                                                                                    FROM per_periods_of_service a
                                                                                                                                   WHERE a.person_id = pos.person_id
                                                                                                                                     AND a.creation_date =
                                                                                                                                         (SELECT MAX(ccx.creation_date)
                                                                                                                                            FROM per_periods_of_service ccx
                                                                                                                                           WHERE ccx.person_id = a.person_id))
                                                                                                                             AND papf.object_version_number =
                                                                                                                                 (SELECT MAX(papf1.object_version_number)
                                                                                                                                    FROM per_all_people_f papf1
                                                                                                                                   WHERE papf.person_id = papf1.person_id)
                                                                                                                             AND pos.attribute2 IS NOT NULL
                                                                                                                             AND EXISTS (SELECT 1
                                                                                                                                    FROM hz_parties hzpa
                                                                                                                                   WHERE hzpa.party_id = papf.party_id)
                                                                                                                             AND EXISTS (SELECT 1
                                                                                                                                    FROM fnd_user fusr
                                                                                                                                   WHERE fusr.employee_id = papf.person_id
                                                                                                                                     AND fusr.end_date IS NULL);   */
      SELECT papf.first_name
            ,papf.person_id
            ,papf.last_name
            ,papf.employee_number
            ,papf.full_name
            ,papf.effective_start_date - 1 effective_start_date
            ,papf.effective_end_date
            ,bd.nt_login user_id
            --,pos.final_process_date    --Version 2.1
            ,users.user_name
            ,users.user_guid
        FROM per_all_people_f          papf
            --,per_all_assignments_f     paaf   --Version 2.1
            --,per_periods_of_service    pos    --Version 2.1
            ,apps.fnd_user             users
            ,xxcus.xxcus_usr_login_tbl bd
       WHERE papf.person_type_id = 9 --Ex-employee
         --AND papf.person_id = paaf.person_id  --Version 2.1
         --AND paaf.person_id = pos.person_id   --Version 2.1
         --AND paaf.period_of_service_id = pos.period_of_service_id  --Version 2.1
         AND papf.person_id = users.employee_id
         AND bd.employee_number = papf.employee_number
         AND papf.effective_end_date = '31-Dec-4712'         
         --AND paaf.assignment_status_type_id = 3  --Version 2.1
         AND EXISTS (SELECT 1
                FROM hz_parties hzpa
               WHERE hzpa.party_id = papf.party_id)
         AND EXISTS (SELECT 1
                FROM fnd_user fusr
               WHERE fusr.employee_id = papf.person_id
                 AND fusr.end_date IS NULL
                 );
    --end cursor changes    5/10/2013        
  
    pl_error_code  VARCHAR2(1);
    pl_error_msg   VARCHAR2(4000);
    pl_count       NUMBER;
    pl_error_count NUMBER;
    l_request_id   NUMBER := fnd_global.conc_request_id;   --Version 2.2
  
  BEGIN
  
    fnd_file.put_line(fnd_file.output,
                      ' Start Processing : ' ||
                       to_char(SYSDATE, 'DD-MON-RRRR HH24:MI:SS'));
  
    fnd_file.put_line(fnd_file.output,
                      '      Begin - Creation of New User IDs : ' ||
                       to_char(SYSDATE, 'DD-MON-RRRR HH24:MI:SS'));
  
    BEGIN
      SELECT application_id
        INTO pl_application_id
        FROM fnd_application_tl
       WHERE application_name = 'Oracle iProcurement';
    EXCEPTION
      WHEN OTHERS THEN
        fnd_file.put_line(fnd_file.output,
                          '      Cannot find application id "for Oracle iProcurement" ');
    END;
  
    BEGIN
    
      -- DEBUG NAME
      l_err_callpoint := 'Creating USERIDs';
      -- DEBUG NAME
    
      EXECUTE IMMEDIATE 'TRUNCATE TABLE xxcus.xxcus_usr_login_tbl';
    
      INSERT /*+ APPEND */
      INTO xxcus.xxcus_usr_login_tbl
        (SELECT DISTINCT employee_number
                        ,REPLACE(nt_id, ' ')
           FROM xxcus.xxcushr_ps_emp_all_tbl
          WHERE REPLACE(nt_id, ' ') IS NOT NULL);
      COMMIT;
    
      FOR i IN cu_get_new_employees
      LOOP
        pl_error_code := '';
        pl_error_msg  := '';
      
        xxcus_create_a_user(i.employee_number, i.business_unit,
                            pl_error_code, pl_error_msg); --v2.0 add i.product_code param
        IF pl_error_code IS NULL THEN
          fnd_file.put_line(fnd_file.output,
                            '            Created User ID for Employee : ' ||
                             i.full_name || ' ID : ' || i.user_id);
          COMMIT;
          pl_count := pl_count + 1;
        ELSE
          ROLLBACK;
          pl_error_count := pl_error_count + 1;
          fnd_file.put_line(fnd_file.output,
                            '            ******* Cannot Create User ID : ' ||
                             i.user_id || ' for Employee : ' || i.full_name ||
                             chr(10) || '       Reason : ' || pl_error_msg);
        END IF;
      
      END LOOP;
    EXCEPTION
      WHEN OTHERS THEN
        xxcus_error_pkg.xxcus_error_main_api(p_called_from => l_err_callfrom,
                                             p_calling => l_err_callpoint,
                                             p_ora_error_msg => SQLERRM,
                                             p_error_desc => 'Gathering employees to CREATE USERSIDs',
                                             p_distribution_list => l_distro_list,
                                             p_module => 'HR');
    END;
  
    fnd_file.put_line(fnd_file.output,
                      '      Completed Creation of New User IDs : ' ||
                       to_char(SYSDATE, 'DD-MON-RRRR HH24:MI:SS'));
  
    fnd_file.put_line(fnd_file.output,
                      '      Begin - Disabling of User IDs : ' ||
                       to_char(SYSDATE, 'DD-MON-RRRR HH24:MI:SS'));
  
    -- DELETE EMPLOYEES READY ALREADY TERMINATED
    BEGIN
    
      -- DEBUG NAME
      l_err_callpoint := 'End Dating USER_ID';
      -- DEBUG NAME
    
      FOR xx IN cu_get_employees_to_delete
      LOOP
        pl_error_code := '';
        pl_error_msg  := '';
      
        --Version 1.1 5/10/2013  
        --need to clear user_guid or we get error ORA-20001: Unabled to call fnd_ldap_wrapp when updating any user
        IF xx.user_guid IS NOT NULL THEN
          UPDATE apps.fnd_user
             SET user_guid = NULL
           WHERE user_name = xx.user_name;
          COMMIT;
        END IF;
        -- end  5/10/2013
        -- fnd_file.put_line(fnd_file.output, '    calling disable user for ' || i.first_name || ' ' || i.last_name || ' ' || i.employee_number );
        xxcus_disable_a_user(xx.first_name, xx.last_name,
                             xx.employee_number, xx.effective_start_date,
                             xx.user_name, pl_error_code, pl_error_msg);
      
        -- fnd_file.put_line(fnd_file.output, '    Error Code is : ' || pl_error_code );
      

        IF pl_error_code IS NULL THEN
        

          COMMIT;
          fnd_file.put_line(fnd_file.output,
                            '             User Disabled :  ' ||

                             xx.full_name || '  User ID : ' || xx.user_id);
          pl_count := pl_count + 1;
        ELSE

          ROLLBACK;
          pl_error_count := pl_error_count + 1;
          fnd_file.put_line(fnd_file.output,
                            '           Unable to disable User ID : ' ||
                             xx.user_id || ' for Employee : ' ||
                             xx.full_name || chr(10) || '      Reason : ' ||
                             pl_error_msg);
        


        END IF;
      
        -- Disable the responsibilities.
      
        -- DEBUG NAME
        l_err_callpoint := 'Disabling Responsibilities';
        -- DEBUG NAME
      
        BEGIN
          FOR c_respon IN (SELECT a.user_id
                                 ,a.responsibility_id
                                 ,a.responsibility_application_id
                                 ,a.security_group_id
                                 ,a.start_date
                                 ,a.end_date
                                 ,b.employee_id
                                 ,lur.effective_start_date
                                 ,lur.effective_end_date
                                 ,lur.role_name
                                 ,b.user_name
                                 ,lur.assignment_type
                                 ,lur.role_orig_system
                             FROM apps.XXCUS_FND_USER_RESP_GROUPS_VW a  --Version 2.2 --VIJAY
                                  -- apps.fnd_user_resp_groups_all a    
                                 ,apps.fnd_user                 b
                                 ,apps.wf_local_user_roles      lur
                            WHERE b.employee_id = xx.person_id
                              AND b.user_id = a.user_id
                              AND nvl(a.end_date, sysdate + 1) > sysdate  --Version 2.2 --Kathy
                              --AND lur.role_end_date IS NULL        --commented for version#2.3
                              AND lur.user_name = b.user_name
                              AND lur.role_orig_system in ('FND_RESP','UMX') --Version 2.2 --VIJAY   
                              AND lur.role_orig_system_id =
                                  a.responsibility_id)
          LOOP
           
                               
          IF c_respon.role_orig_system = 'FND_RESP' then --Version 2.2 --VIJAY          
           fnd_file.put_line(fnd_file.output,
                              'End Dating DIRECT :' || c_respon.role_name ||
                               ' for ' || c_respon.user_name);
            BEGIN -- begin for Ver 2.3
            fnd_user_resp_groups_api.update_assignment(user_id => c_respon.user_id,
                                                       responsibility_id => c_respon.responsibility_id,
                                                       responsibility_application_id => c_respon.responsibility_application_id,
                                                       security_group_id => c_respon.security_group_id,
                                                       start_date => c_respon.effective_start_date,
                                                       end_date => xx.effective_start_date,
                                                       description => 'Terminated via Create APPS PKG');
--added below exception part for ver# 2.3
           EXCEPTION
            WHEN OTHERS THEN
            fnd_file.put_line(fnd_file.output,
                              'Exception while end dating rsp id :' || c_respon.responsibility_id ||
                               ' for ' || c_respon.user_name);
           END;
          
          ELSIF c_respon.role_orig_system = 'UMX' then  --Version 2.2 --VIJAY
          fnd_file.put_line(fnd_file.output,
                              'End Dating INDIRECT :' || c_respon.role_name ||
                               ' for ' || c_respon.user_name);
            BEGIN      -- begin for Ver 2.3       
            wf_local_synch.propagateuserrole(p_user_name => c_respon.user_name, 
                                 p_role_name => c_respon.role_name,
                                 p_start_date =>  c_respon.effective_start_date,
                                 --p_expiration_date=> xx.effective_start_date,                  --commented and added below for ver# 2.3
                                 p_expiration_date=> xx.effective_start_date-90,                 
                                 p_assignmentReason => 'Terminated via Create APPS PKG');        --Added p_assignmentReason parameter for ver# 2.3
--added below exception part for ver# 2.3
           EXCEPTION
            WHEN OTHERS THEN
            fnd_file.put_line(fnd_file.output,
                              'Exception while end dating role :' || c_respon.role_name ||
                               ' for ' || c_respon.user_name);
           END;
          END IF;  --Version 2.2 complete elsif part added for revoking indirect responsibility.
          
          END LOOP;
          COMMIT;    --Version 2.2 --VIJAY
        EXCEPTION
          WHEN OTHERS THEN
            xxcus_error_pkg.xxcus_error_main_api(p_called_from => l_err_callfrom,
                                                 p_calling => l_err_callpoint,
                                                 p_ora_error_msg => SQLERRM,
                                                 p_error_desc => 'Trying to disable responsibilities',
                                                 p_distribution_list => l_distro_list,
                                                 p_module => 'HR');
        END;
          
      
      END LOOP;
    
    EXCEPTION
      WHEN OTHERS THEN
        xxcus_error_pkg.xxcus_error_main_api(p_called_from => l_err_callfrom,
                                             p_calling => l_err_callpoint,
                                             p_request_id        => l_request_id,   --Version 2.2
                                             p_ora_error_msg => substr(SQLERRM,1,2000),  --Version 2.2 added substr
                                             p_error_desc => 'Gathering employees to delete',
                                             p_distribution_list => l_distro_list,
                                             p_module => 'HR');
    END;
  
    fnd_file.put_line(fnd_file.output,
                      ' Completed Processing : ' ||
                       to_char(SYSDATE, 'DD-MON-RRRR HH24:MI:SS'));
  
  END xxcus_process_users;

  FUNCTION xxcus_check_user_exists(p_user_number IN VARCHAR2) RETURN VARCHAR2 IS
    CURSOR cu_check_user IS
      SELECT '1'
        FROM apps.fnd_user        a
            ,apps.per_employees_x b
       WHERE b.employee_num = p_user_number
         AND a.employee_id = b.employee_id
         AND a.end_date IS NULL;
  
    pl_flag VARCHAR2(1) := 'Y';
  BEGIN
    IF cu_check_user%ISOPEN THEN
      CLOSE cu_check_user;
    END IF;
  
    OPEN cu_check_user;
    FETCH cu_check_user
      INTO pl_flag;
    IF cu_check_user%FOUND THEN
      pl_flag := 'Y';
    ELSE
      pl_flag := 'N';
    END IF;
    CLOSE cu_check_user;
    RETURN pl_flag;
  END xxcus_check_user_exists;

  --Procedure to create user
  --Version 1.0 

  PROCEDURE xxcus_create_a_user(p_employee_number IN VARCHAR2
                               ,p_business_unit   IN VARCHAR2
                               ,p_error_code      IN OUT VARCHAR2
                               ,p_error_msg       IN OUT VARCHAR2) IS
  
    pl_check_user    VARCHAR2(1);
    pl_user_name     VARCHAR2(20);
    pl_employee_id   NUMBER;
    pl_full_name     per_all_people_f.full_name%TYPE;
    pl_email         per_all_people_f.email_address%TYPE;
    pl_return_status VARCHAR2(1000);
    pl_msg_count     NUMBER;
    pl_msg_data      VARCHAR2(1000);
    l_msg            VARCHAR2(2000);
    pl_role          VARCHAR2(100);
  
  BEGIN
    -- DEBUG NAME
    l_err_callpoint := 'Creating USER procedure';
    -- DEBUG NAME
  
    fnd_profile.get('USER_ID', pl_created_by_id);
  
    -- Gather employee details for creation
    BEGIN
      SELECT person_id
            ,full_name
            ,email_address
        INTO pl_employee_id
            ,pl_full_name
            ,pl_email
        FROM per_all_people_f
       WHERE employee_number = p_employee_number
         AND effective_end_date = to_date('31-DEC-4712');
    EXCEPTION
      WHEN no_data_found THEN
        l_msg := 'Missing employee data of full_name or email address';
        RAISE program_error;
    END;
  
    -- Gather NT Login for creation
    BEGIN
      SELECT upper(nt_login)
        INTO pl_user_name
        FROM xxcus.xxcus_usr_login_tbl
       WHERE employee_number = p_employee_number;
    EXCEPTION
      WHEN no_data_found THEN
        l_msg := 'No employee number found';
        RAISE program_error;
    END;
  
    pl_user_id := fnd_user_pkg.createuserid(x_user_name => pl_user_name,
                                            x_owner => NULL,
                                            x_unencrypted_password => 'Oracle01',
                                            x_session_number => NULL,
                                            x_start_date => SYSDATE,
                                            x_end_date => NULL,
                                            x_last_logon_date => NULL,
                                            x_description => pl_full_name,
                                            x_password_date => NULL,
                                            x_password_accesses_left => NULL,
                                            x_password_lifespan_accesses => NULL,
                                            x_password_lifespan_days => 90,
                                            x_employee_id => pl_employee_id,
                                            x_email_address => pl_email,
                                            x_fax => NULL,
                                            x_customer_id => NULL,
                                            x_supplier_id => NULL);
  
    BEGIN
      SELECT description -- v2.0 begin
        INTO pl_role
        FROM apps.fnd_lookup_values flv
       WHERE flv.lookup_type = 'HDS_HR-INT PS-ORA'
         AND flv.enabled_flag = 'Y'
         AND flv.meaning = p_business_unit
         AND nvl(end_date_active, SYSDATE) >= SYSDATE;
    
      fnd_user_pkg.addresp(pl_user_name, 'SQLAP', pl_role
                           --'HDS_INTERNET_EXPENSES_USER' 2/20 production setup different the ebizpch
                          , 'STANDARD', 'DESCRIPTION', trunc(SYSDATE), NULL); -- v2.0 end
    
    EXCEPTION
      WHEN OTHERS THEN
        p_error_code := 'E';
        p_error_msg  := substr(SQLERRM, 1, 200);
    END;
  
    IF nvl(p_error_code, '@') != 'E' THEN
    
      --fnd_file.put_line(fnd_file.output, 'inside create security info....') ;
    
      icx_user_sec_attr_pub.create_user_sec_attr(p_api_version_number => 1,
                                                 p_init_msg_list => fnd_api.g_false,
                                                 p_simulate => fnd_api.g_false,
                                                 p_commit => fnd_api.g_false,
                                                 p_validation_level => fnd_api.g_valid_level_full,
                                                 p_return_status => pl_return_status,
                                                 p_msg_count => pl_msg_count,
                                                 p_msg_data => pl_msg_data,
                                                 p_web_user_id => pl_user_id,
                                                 p_attribute_code => 'ICX_HR_PERSON_ID',
                                                 p_attribute_appl_id => pl_application_id,
                                                 p_varchar2_value => NULL,
                                                 p_date_value => NULL,
                                                 p_number_value => pl_employee_id,
                                                 p_created_by => pl_created_by_id,
                                                 p_creation_date => trunc(SYSDATE),
                                                 p_last_updated_by => pl_created_by_id,
                                                 p_last_update_date => trunc(SYSDATE),
                                                 p_last_update_login => pl_created_by_id);
    
      --dbms_output.put_line('Status for icx hr person id is : ' || pl_return_status ) ;
      --fnd_file.put_line(fnd_file.output, 'Status for icx hr person id is : ' || pl_return_status ) ;
    
      icx_user_sec_attr_pub.create_user_sec_attr(p_api_version_number => 1,
                                                 p_init_msg_list => fnd_api.g_false,
                                                 p_simulate => fnd_api.g_false,
                                                 p_commit => fnd_api.g_false,
                                                 p_validation_level => fnd_api.g_valid_level_full,
                                                 p_return_status => pl_return_status,
                                                 p_msg_count => pl_msg_count,
                                                 p_msg_data => pl_msg_data,
                                                 p_web_user_id => pl_user_id,
                                                 p_attribute_code => 'TO_PERSON_ID',
                                                 p_attribute_appl_id => pl_application_id,
                                                 p_varchar2_value => NULL,
                                                 p_date_value => NULL,
                                                 p_number_value => pl_employee_id,
                                                 p_created_by => pl_created_by_id,
                                                 p_creation_date => trunc(SYSDATE),
                                                 p_last_updated_by => pl_created_by_id,
                                                 p_last_update_date => trunc(SYSDATE),
                                                 p_last_update_login => pl_created_by_id);
      --fnd_file.put_line(fnd_file.output, 'Status for to person id is : ' || pl_return_status ) ;
    END IF;
  
  EXCEPTION
  
    WHEN program_error THEN
      p_error_code := 'E';
      p_error_msg  := substr(SQLERRM, 1, 200);
      fnd_file.put_line(fnd_file.output, l_msg || ' ' || p_error_msg);
    
    WHEN OTHERS THEN
      p_error_code := 'E';
      p_error_msg  := substr(SQLERRM, 1, 200);
      fnd_file.put_line(fnd_file.output, l_msg || ' ' || p_error_msg);
    
  END xxcus_create_a_user;

  PROCEDURE xxcus_disable_a_user(p_employee_first_name IN VARCHAR2
                                ,p_employee_last_name  IN VARCHAR2
                                ,p_employee_number     IN VARCHAR2
                                ,p_fp_date             IN DATE DEFAULT NULL
                                ,p_user_name           IN VARCHAR2
                                ,p_error_code          IN OUT VARCHAR2
                                ,p_error_msg           IN OUT VARCHAR2) IS
    pl_user_name VARCHAR2(20);
    pl_fp_date   DATE;
  
  BEGIN
  
    -- DEBUG NAME
    l_err_callpoint := 'Disabling USER procedure';
    -- DEBUG NAME
  
    pl_fp_date   := nvl(p_fp_date, SYSDATE); --Final process date or null which is sysdate.
    pl_user_name := p_user_name;
  
    fnd_user_pkg.updateuser(x_user_name => pl_user_name, x_owner => NULL,
                            x_end_date => pl_fp_date);
  
  EXCEPTION
    WHEN OTHERS THEN
      p_error_code := 'E';
      p_error_msg  := substr(SQLERRM, 1, 200);
    
  END xxcus_disable_a_user;

  --Procedure to Update user
  PROCEDURE xxcus_update_a_user(p_user_name  IN VARCHAR2
                               ,p_error_code IN OUT VARCHAR2
                               ,p_error_msg  IN OUT VARCHAR2) IS
  
    pl_user_name VARCHAR2(100); --Ver 2.4 - changed the data type size from 20 to 100
    pl_fp_date   DATE;
  BEGIN
    p_error_code := NULL;    --Ver 2.4
    p_error_msg  := NULL;    --Ver 2.4
    pl_fp_date   := SYSDATE; --Final process date or null which is sysdate.
    pl_user_name := p_user_name;
  
    fnd_user_pkg.updateuser(x_user_name => pl_user_name, x_owner => NULL,
                            x_end_date => pl_fp_date);
    COMMIT; --Ver 2.4
  EXCEPTION
    WHEN OTHERS THEN
      p_error_code := 'E';
      p_error_msg  := substr(SQLERRM, 1, 2000);  --Ver 2.4 - Changed the substr end index size
	  ROLLBACK; --Ver 2.4
    
  END xxcus_update_a_user;
  /********************************************************************************
   ProcedureName : WRITE_OUTPUT
   Purpose       : write output in output file

   HISTORY
   ===============================================================================
   VERSION DATE          AUTHOR(S)       DESCRIPTION
   ------- -----------   --------------- -----------------------------------------
   2.4     29-JUN-2016   Niraj K Ranjan  Initial Version
   ********************************************************************************/
   PROCEDURE write_output(p_user_rec       IN l_user_tab
                         ,p_retcode        OUT VARCHAR2
                         ,p_retmsg         OUT VARCHAR2
                         )
   IS
   BEGIN
      fnd_file.put_line (fnd_file.LOG ,'----------------------------------------------------------------------------------');
      fnd_file.put_line (fnd_file.LOG , 'Start of Procedure : write_output'||' @ '||TO_CHAR(SYSDATE, 'DD-MON-YYYY HH:MI:SS'));

      fnd_file.put_line (fnd_file.output,
                           'USER'
                        || '|'
                        || 'FIRST NAME'
                        || '|'
                        || 'LAST NAME'
						);				
					
      FOR i IN 1 .. p_user_rec.COUNT
      LOOP
            fnd_file.put_line (fnd_file.output,
                                p_user_rec (i).USER_NAME
                              || '|'
                              || p_user_rec (i).FIRST_NAME
                              || '|'
                              || p_user_rec (i).LAST_NAME
		    				  );
      END LOOP;
      fnd_file.put_line (fnd_file.LOG , 'End of Procedure : write_output'||' @ '||TO_CHAR(SYSDATE, 'DD-MON-YYYY HH:MI:SS'));
      fnd_file.put_line (fnd_file.LOG ,'----------------------------------------------------------------------------------');
   EXCEPTION
      WHEN OTHERS THEN
         p_retcode := SQLCODE;
         p_retmsg  := SQLERRM;
   END write_output;
  /********************************************************************************
   ProcedureName : XXCUS_INACTIVATE_USERS
   Purpose       : Check User if it is active

   HISTORY
   ===============================================================================
   VERSION DATE          AUTHOR(S)       DESCRIPTION
   ------- -----------   --------------- -----------------------------------------
   2.4     28-JUN-2016   Niraj K Ranjan  Initial Version
   ********************************************************************************/ 
   FUNCTION xxwc_check_user_active(p_user_name IN VARCHAR2) 
   RETURN VARCHAR2 IS
      l_user_count NUMBER;
      l_active_flag VARCHAR2(1) := 'Y';
   BEGIN
     SELECT COUNT(1) INTO l_user_count
        FROM apps.fnd_user usr
       WHERE usr.user_name = UPPER(p_user_name)
         AND NVL(usr.end_date,SYSDATE+1) > SYSDATE;

     IF l_user_count > 0 THEN
        l_active_flag := 'Y';
     ELSE
        l_active_flag := 'N';
     END IF;
     RETURN l_active_flag;
   EXCEPTION
     WHEN OTHERS THEN
        l_active_flag := 'E';
        RETURN l_active_flag;
   END xxwc_check_user_active;
  /********************************************************************************
   ProcedureName : XXWC_INACTIVATE_USERS
   Purpose       : Inactivate contractor user which are still active inspite of role off.

   HISTORY
   ===============================================================================
   VERSION DATE          AUTHOR(S)       DESCRIPTION
   ------- -----------   --------------- -----------------------------------------
   2.4     28-JUN-2016   Niraj K Ranjan  Initial Version
   2.5     03-Aug-2016   Niraj K Ranjan  TMS#20160601-00216   Automate End-Dating Contractors in Oracle EBS
   ********************************************************************************/ 
  PROCEDURE xxwc_inactivate_users(p_errbuf  OUT VARCHAR2
                                 ,p_retcode OUT NUMBER) 
  IS
     l_active_user_ind        VARCHAR2(1);
     l_bad_rec_exception      EXCEPTION;
     l_err_msg                VARCHAR2 (2000);
     l_err_code               VARCHAR2(200);
     l_sec                    VARCHAR2 (150);
     l_rec_count              NUMBER := 0;
	 l_user_rec               l_user_tab;
	 l_profile_set            BOOLEAN;                  --Ver 2.5
     l_user_id                NUMBER;	                --Ver 2.5
     l_user_guid              FND_USER.USER_GUID%TYPE;  --Ver 2.5
     --l_user_id                NUMBER := FND_GLOBAL.USER_ID;
	 --l_responsibility_id      NUMBER := FND_GLOBAL.RESP_ID;
	 --l_resp_application_id    NUMBER := FND_GLOBAL.RESP_APPL_ID;
	 --l_login_id               NUMBER := FND_GLOBAL.LOGIN_ID;
     CURSOR cr_users
     IS
        SELECT user_name,first_name,last_name
        FROM   xxwc_inactive_user_load_tbl;	 
  BEGIN
     p_errbuf   := 'Success';
     p_retcode  := '0';
     l_err_code := NULL;
	 l_err_msg  := NULL;
     /*FND_GLOBAL.APPS_INITIALIZE (l_user_id,
                                 l_responsibility_id,
                                 l_resp_application_id
								);*/
     fnd_file.put_line (fnd_file.LOG ,'**********************************************************************************');
     fnd_file.put_line (fnd_file.LOG ,'----------------------------------------------------------------------------------');
     fnd_file.put_line (fnd_file.LOG , 'Start of Procedure : xxwc_inactivate_users'||' @ '||TO_CHAR(SYSDATE, 'DD-MON-YYYY HH:MI:SS'));
     fnd_file.put_line (fnd_file.LOG ,'----------------------------------------------------------------------------------');
     l_user_rec.DELETE;
     l_sec := 'Start Loop for cursor cr_users';
     fnd_file.put_line (fnd_file.LOG , 'Start Loop for cursor cr_users'||' @ '||TO_CHAR(SYSDATE, 'DD-MON-YYYY HH:MI:SS'));
     FOR rec_user IN cr_users
     LOOP
        BEGIN
           l_user_id   := NULL;  --Ver 2.5
           l_user_guid := NULL;  --Ver 2.5
           l_sec := 'Calling func xxwc_check_user_active for user:'||rec_user.user_name;
           l_active_user_ind := xxwc_check_user_active(rec_user.user_name);
           IF l_active_user_ind = 'N' THEN
		      fnd_file.put_line(fnd_file.LOG , 'User:'||rec_user.user_name||' is either inactive or does not exist');
		   ELSIF l_active_user_ind = 'Y' THEN
		      --Start Ver 2.5
              --This select statement is not handled error because user name is already validated above.
		      SELECT user_id,user_guid INTO l_user_id,l_user_guid FROM FND_USER WHERE user_name = UPPER(rec_user.user_name);
			  IF l_user_guid IS NOT NULL THEN
                 l_sec := 'Set profile option value at user level';
                 l_profile_set := fnd_profile.save( x_name                 => 'APPS_SSO_LDAP_SYNC'
                                                  , x_value                => 'N'
                                                  , x_level_name           => 'USER'
                                                  , x_level_value          => l_user_id
                                                  , x_level_value_app_id   => NULL
                                                  ) ;
                 IF l_profile_set THEN
                    COMMIT;
                    fnd_file.put_line(fnd_file.LOG , 'Profile option value is set at user level for profile APPS_SSO_LDAP_SYNC and user - '||rec_user.user_name);
                 ELSE
                    fnd_file.put_line(fnd_file.LOG , 'Profile option value is not set at user level for profile APPS_SSO_LDAP_SYNC');
                 END IF;
              END IF;
			  --End Ver 2.5
              l_sec := 'Calling xxcus_update_a_user for User:'||rec_user.user_name;
              xxcus_update_a_user(p_user_name  => rec_user.user_name
                                 ,p_error_code => l_err_code
                                 ,p_error_msg  => l_err_msg);
              IF l_err_msg IS NOT NULL THEN
                 fnd_file.put_line(fnd_file.LOG , 'Error while inactivating user:'||rec_user.user_name||' =>'||l_err_msg);
                 RAISE l_bad_rec_exception;
              ELSE
                 l_rec_count := l_rec_count + 1;
                 l_sec := 'Populate collection l_user_rec for User:'||rec_user.user_name;
                 l_user_rec(l_rec_count).USER_NAME    := rec_user.user_name    ;
		         l_user_rec(l_rec_count).FIRST_NAME   := rec_user.first_name        ;
		         l_user_rec(l_rec_count).LAST_NAME    := rec_user.last_name         ;
              END IF;
           ELSIF l_active_user_ind = 'E' THEN
              fnd_file.put_line(fnd_file.LOG , 'Error inside function xxwc_check_user_active for user:'||rec_user.user_name);
              RAISE l_bad_rec_exception;
           END IF;
        EXCEPTION
           WHEN l_bad_rec_exception THEN
              p_errbuf := 'Warning';
              p_retcode := '1';
        END;
     END LOOP;
     l_sec := 'End Loop for cursor cr_users';
     fnd_file.put_line (fnd_file.LOG , 'End Loop for cursor cr_users'||' @ '||TO_CHAR(SYSDATE, 'DD-MON-YYYY HH:MI:SS'));
	 --Call to generate text output
      l_sec := 'Call write_output';
      write_output(p_user_rec       => l_user_rec
                   ,p_retcode       => l_err_code
                   ,p_retmsg        => l_err_msg
                  );
      IF l_err_msg IS NOT NULL THEN
         fnd_file.put_line (fnd_file.LOG, l_sec||'=>'||l_err_code||'=>'||l_err_msg);
		 fnd_file.put_line (fnd_file.output, l_sec||'=>'||l_err_code||'=>'||l_err_msg);
         p_errbuf := 'Error';
         p_retcode := '2';
      END IF;
     fnd_file.put_line (fnd_file.LOG ,'**********************************************************************************');
     fnd_file.put_line (fnd_file.LOG ,'----------------------------------------------------------------------------------');
     fnd_file.put_line (fnd_file.LOG , 'End of Procedure : xxwc_inactivate_users'||' @ '||TO_CHAR(SYSDATE, 'DD-MON-YYYY HH:MI:SS'));
     fnd_file.put_line (fnd_file.LOG ,'----------------------------------------------------------------------------------');
  EXCEPTION
     WHEN OTHERS THEN
       p_errbuf := 'Error';
       p_retcode := '2';
	   l_err_code := SQLCODE;
       l_err_msg  := SUBSTR(SQLERRM,1,2000);
       fnd_file.put_line (fnd_file.LOG, l_sec||'=>'||l_err_code||'=>'||l_err_msg);
       fnd_file.put_line (fnd_file.output, l_sec||'=>'||l_err_code||'=>'||l_err_msg);
       xxcus_error_pkg.xxcus_error_main_api (
            p_called_from         => 'XXCUS_CREATE_APPS_USER_PKG.XXWC_INACTIVATE_USERS'
           ,p_calling             => l_sec
           ,p_request_id          => fnd_global.conc_request_id
           ,p_ora_error_msg       => SQLERRM
           ,p_error_desc          => 'Error while Inactivating User'
           ,p_distribution_list   => g_dflt_email
           ,p_module              => 'XXWC');
  END xxwc_inactivate_users;
END xxcus_create_apps_user_pkg;
/
