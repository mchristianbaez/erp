--Report Name            : White Cap ALL Credit Memo Report - WC
--Import Instructions    : Run this file in XXEIS user from SQL*Plus or any tool like Toad, SQL Navigator. 
prompt Creating Object Data EIS_XXWC_AR_CREDIT_MEMO_ALL_V
set scan off define off
DECLARE
mod_exist VARCHAR2(1);
BEGIN 
mod_exist := XXEIS.EIS_RSC_INSERT.check_if_module_exist(222);
IF mod_exist = 'Y' THEN 
--Inserting Object EIS_XXWC_AR_CREDIT_MEMO_ALL_V
xxeis.eis_rsc_ins.v( 'EIS_XXWC_AR_CREDIT_MEMO_ALL_V',222,'','','','','SA059956','XXEIS','EIS XXWC Ar Credit Memo All Dtls V','EXACMAV','','','VIEW','US','','EIS_XXWC_AR_CREDIT_MEMO_DTLS_V');
--Delete Object Columns for EIS_XXWC_AR_CREDIT_MEMO_ALL_V
xxeis.eis_rsc_utility.delete_view_rows('EIS_XXWC_AR_CREDIT_MEMO_ALL_V',222,FALSE);
--Inserting Object Columns for EIS_XXWC_AR_CREDIT_MEMO_ALL_V
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_AR_CREDIT_MEMO_ALL_V','TAKEN_BY',222,'Taken By','TAKEN_BY','','','','SA059956','VARCHAR2','','','Taken By','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_AR_CREDIT_MEMO_ALL_V','MASTER_NAME',222,'Master Name','MASTER_NAME','','','','SA059956','VARCHAR2','','','Master Name','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_AR_CREDIT_MEMO_ALL_V','CUSTOMER_NAME',222,'Customer Name','CUSTOMER_NAME','','','','SA059956','VARCHAR2','','','Customer Name','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_AR_CREDIT_MEMO_ALL_V','CUSTOMER_TERRITORY',222,'Customer Territory','CUSTOMER_TERRITORY','','','','SA059956','VARCHAR2','','','Customer Territory','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_AR_CREDIT_MEMO_ALL_V','SALES_REP_NO',222,'Sales Rep No','SALES_REP_NO','','','','SA059956','VARCHAR2','','','Sales Rep No','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_AR_CREDIT_MEMO_ALL_V','QTY_SHIPPED',222,'Qty Shipped','QTY_SHIPPED','','','','SA059956','NUMBER','','','Qty Shipped','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_AR_CREDIT_MEMO_ALL_V','INVOICE_LINE_AMOUNT',222,'Invoice Line Amount','INVOICE_LINE_AMOUNT','','','','SA059956','NUMBER','','','Invoice Line Amount','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_AR_CREDIT_MEMO_ALL_V','CREDIT_REASON_SALE',222,'Credit Reason Sale','CREDIT_REASON_SALE','','','','SA059956','VARCHAR2','','','Credit Reason Sale','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_AR_CREDIT_MEMO_ALL_V','PART_NUMBER',222,'Part Number','PART_NUMBER','','','','SA059956','VARCHAR2','','','Part Number','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_AR_CREDIT_MEMO_ALL_V','ORIGINAL_INVOICE_DATE',222,'Original Invoice Date','ORIGINAL_INVOICE_DATE','','','','SA059956','DATE','','','Original Invoice Date','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_AR_CREDIT_MEMO_ALL_V','ORIGINAL_INVOICE_NUMBER',222,'Original Invoice Number','ORIGINAL_INVOICE_NUMBER','','','','SA059956','VARCHAR2','','','Original Invoice Number','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_AR_CREDIT_MEMO_ALL_V','ORDER_NUMBER',222,'Order Number','ORDER_NUMBER','','','','SA059956','NUMBER','','','Order Number','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_AR_CREDIT_MEMO_ALL_V','INVOICE_NUMBER',222,'Invoice Number','INVOICE_NUMBER','','','','SA059956','VARCHAR2','','','Invoice Number','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_AR_CREDIT_MEMO_ALL_V','TAX_AMT',222,'Tax Amt','TAX_AMT','','','','SA059956','NUMBER','','','Tax Amt','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_AR_CREDIT_MEMO_ALL_V','AMOUNT_PAID',222,'Amount Paid','AMOUNT_PAID','','','','SA059956','NUMBER','','','Amount Paid','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_AR_CREDIT_MEMO_ALL_V','LOCATION',222,'Location','LOCATION','','','','SA059956','VARCHAR2','','','Location','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_AR_CREDIT_MEMO_ALL_V','INVOICE_TOTAL',222,'Invoice Total','INVOICE_TOTAL','','','','SA059956','NUMBER','','','Invoice Total','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_AR_CREDIT_MEMO_ALL_V','FREIGHT_REMAINING',222,'Freight Remaining','FREIGHT_REMAINING','','','','SA059956','NUMBER','','','Freight Remaining','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_AR_CREDIT_MEMO_ALL_V','FREIGHT_ORIGINAL',222,'Freight Original','FREIGHT_ORIGINAL','','','','SA059956','NUMBER','','','Freight Original','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_AR_CREDIT_MEMO_ALL_V','TAX_RATE',222,'Tax Rate','TAX_RATE','','','','SA059956','NUMBER','','','Tax Rate','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_AR_CREDIT_MEMO_ALL_V','BUSINESS_DATE',222,'Business Date','BUSINESS_DATE','','','','SA059956','DATE','','','Business Date','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_AR_CREDIT_MEMO_ALL_V','INVOICE_DATE',222,'Invoice Date','INVOICE_DATE','','','','SA059956','DATE','','','Invoice Date','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_AR_CREDIT_MEMO_ALL_V','SHIP_NAME',222,'Ship Name','SHIP_NAME','','','','SA059956','VARCHAR2','','','Ship Name','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_AR_CREDIT_MEMO_ALL_V','CUSTOMER_NUMBER',222,'Customer Number','CUSTOMER_NUMBER','','','','SA059956','VARCHAR2','','','Customer Number','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_AR_CREDIT_MEMO_ALL_V','FISCAL_MONTH',222,'Fiscal Month','FISCAL_MONTH','','','','SA059956','VARCHAR2','','','Fiscal Month','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_AR_CREDIT_MEMO_ALL_V','FISCAL_YEAR',222,'Fiscal Year','FISCAL_YEAR','','','','SA059956','NUMBER','','','Fiscal Year','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_AR_CREDIT_MEMO_ALL_V','ORIGIN_DATE',222,'Origin Date','ORIGIN_DATE','','','','SA059956','DATE','','','Origin Date','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_AR_CREDIT_MEMO_ALL_V','RESTOCK_FEE_PERCENT',222,'Restock Fee Percent','RESTOCK_FEE_PERCENT','','','','SA059956','NUMBER','','','Restock Fee Percent','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_AR_CREDIT_MEMO_ALL_V','AR_CUSTOMER_NUMBER',222,'Ar Customer Number','AR_CUSTOMER_NUMBER','','','','SA059956','VARCHAR2','','','Ar Customer Number','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_AR_CREDIT_MEMO_ALL_V','ORDER_DATE',222,'Order Date','ORDER_DATE','','','','SA059956','DATE','','','Order Date','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_AR_CREDIT_MEMO_ALL_V','SHIP_DATE',222,'Ship Date','SHIP_DATE','','','','SA059956','DATE','','','Ship Date','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_AR_CREDIT_MEMO_ALL_V','CREATION_DATE',222,'Creation Date','CREATION_DATE','','','','SA059956','DATE','','','Creation Date','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_AR_CREDIT_MEMO_ALL_V','LINE_TYPE',222,'Line Type','LINE_TYPE','','','','SA059956','VARCHAR2','','','Line Type','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_AR_CREDIT_MEMO_ALL_V','ORGINAL_ORDER_NUMBER',222,'Orginal Order Number','ORGINAL_ORDER_NUMBER','','','','SA059956','NUMBER','','','Orginal Order Number','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_AR_CREDIT_MEMO_ALL_V','C_LINE_NUMBER',222,'C Line Number','C_LINE_NUMBER','','','','SA059956','NUMBER','','','C Line Number','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_AR_CREDIT_MEMO_ALL_V','O_AMOUNT_APPLIED',222,'O Amount Applied','O_AMOUNT_APPLIED','','','','SA059956','NUMBER','','','O Amount Applied','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_AR_CREDIT_MEMO_ALL_V','TRX_DATE',222,'Trx Date','TRX_DATE','','','','SA059956','DATE','','','Trx Date','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_AR_CREDIT_MEMO_ALL_V','PROCESS_ID',222,'Process Id','PROCESS_ID','','','','SA059956','NUMBER','','','Process Id','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_AR_CREDIT_MEMO_ALL_V','ORDER_AMOUNT',222,'Order Amount','ORDER_AMOUNT','','','','SA059956','NUMBER','','','Order Amount','','','','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_AR_CREDIT_MEMO_ALL_V','ORDER_LINE_COST',222,'Order Line Cost','ORDER_LINE_COST','','','','SA059956','NUMBER','','','Order Line Cost','','','','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_AR_CREDIT_MEMO_ALL_V','TRANSACTION_SOURCE',222,'Transaction Source','TRANSACTION_SOURCE','','','','SA059956','VARCHAR2','','','Transaction Source','','','','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_AR_CREDIT_MEMO_ALL_V','TRX_CREATION_DATE',222,'Trx Creation Date','TRX_CREATION_DATE','','','','SA059956','DATE','','','Trx Creation Date','','','','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_AR_CREDIT_MEMO_ALL_V','ORIG_ORDER_REF',222,'Orig Order Ref','ORIG_ORDER_REF','','','','SA059956','VARCHAR2','','','Orig Order Ref','','','','');
--Inserting Object Components for EIS_XXWC_AR_CREDIT_MEMO_ALL_V
--Inserting Object Component Joins for EIS_XXWC_AR_CREDIT_MEMO_ALL_V
ELSE
RAISE_APPLICATION_ERROR (-20001,'Module does not exist. Import/Create the module with id 222');
END IF;
END;
/
prompt Creating Report LOV Data for White Cap ALL Credit Memo Report - WC
set scan off define off
DECLARE
mod_exist VARCHAR2(1);
BEGIN 
mod_exist := XXEIS.EIS_RSC_INSERT.check_if_module_exist(222);
IF mod_exist = 'Y' THEN 
--Inserting Report LOVs - White Cap ALL Credit Memo Report - WC
xxeis.eis_rsc_ins.lov( 222,'SELECT organization_code,organization_id,organization_name FROM org_organization_definitions','','AR Organizaion Code LOV','','XXEIS_RS_ADMIN',NULL,'N','','','','','','','US');
xxeis.eis_rsc_ins.lov( 222,'SELECT DISTINCT GPS.PERIOD_YEAR
FROM      HR_OPERATING_UNITS          HOU,
          GL_LEDGERS                  GL,
          gl_period_statuses          gps
WHERE
 GL.LEDGER_ID                      = HOU.SET_OF_BOOKS_ID
AND GL.LEDGER_ID                      = GPS.SET_OF_BOOKS_ID
AND GL.ACCOUNTED_PERIOD_TYPE          = GPS.PERIOD_TYPE
order by GPS.PERIOD_YEAR
','','Fiscal Year','','ANONYMOUS',NULL,'N','','','','','','','US');
xxeis.eis_rsc_ins.lov( 222,'select PERIOD_NAME,PERIOD_YEAR from(
SELECT DISTINCT GPS.PERIOD_NAME,GPS.PERIOD_YEAR,gps.period_num
FROM HR_OPERATING_UNITS HOU,
  GL_LEDGERS GL,
  gl_period_statuses gps
WHERE GL.LEDGER_ID           = HOU.SET_OF_BOOKS_ID
AND GL.LEDGER_ID             = GPS.SET_OF_BOOKS_ID
AND GL.ACCOUNTED_PERIOD_TYPE = GPS.PERIOD_TYPE
ORDER BY GPS.PERIOD_YEAR,gps.period_num)','','XXWC Fiscal Month LOV','','SA059956',NULL,'N','','','N','N','','','US');
null;
ELSE
RAISE_APPLICATION_ERROR (-20001,'Module does not exist. Import/Create the module with id 222');
END IF;
END;
/
prompt Creating Report Data for White Cap ALL Credit Memo Report - WC
set scan off define off
DECLARE
mod_exist varchar2(1);
BEGIN 
mod_exist := XXEIS.EIS_RSC_INSERT.check_if_module_exist(222);
IF mod_exist = 'Y' THEN 
--Deleting Report data - White Cap ALL Credit Memo Report - WC
xxeis.eis_rsc_utility.delete_report_rows( 'White Cap ALL Credit Memo Report - WC' );
--Inserting Report - White Cap ALL Credit Memo Report - WC
xxeis.eis_rsc_ins.r( 222,'White Cap ALL Credit Memo Report - WC','','All “Credit Memo” transaction type will be displayed and any Rebill or Bill Only lines corresponding to the credit memo processed.  This report will serve the purpose of the Days Lag and SR&A calculation performed by the Accounting Team and the SOX control by the District / Regional Managers.','','','','SA059956','EIS_XXWC_AR_CREDIT_MEMO_ALL_V','Y','','','SA059956','','N','White Cap Reports','','CSV,EXCEL,','N','','','','','','N','','US','','White Cap Credit Memo Report','','');
--Inserting Report Columns - White Cap ALL Credit Memo Report - WC
xxeis.eis_rsc_ins.rc( 'White Cap ALL Credit Memo Report - WC',222,'INVOICE_NUMBER','Invoice Number','Invoice Number','','','default','','20','N','Y','','','','','','','SA059956','N','N','','EIS_XXWC_AR_CREDIT_MEMO_ALL_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'White Cap ALL Credit Memo Report - WC',222,'INVOICE_TOTAL','Invoice Total','Invoice Total','','~T~D~2','default','','14','N','Y','','','','','','','SA059956','N','N','','EIS_XXWC_AR_CREDIT_MEMO_ALL_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'White Cap ALL Credit Memo Report - WC',222,'LOCATION','Location','Location','','','default','','18','N','Y','','','','','','','SA059956','N','N','','EIS_XXWC_AR_CREDIT_MEMO_ALL_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'White Cap ALL Credit Memo Report - WC',222,'MASTER_NAME','Account Name','Master Name','','','default','','37','N','Y','','','','','','','SA059956','N','N','','EIS_XXWC_AR_CREDIT_MEMO_ALL_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'White Cap ALL Credit Memo Report - WC',222,'ORDER_NUMBER','Order Number','Order Number','','~~~','default','','21','N','Y','','','','','','','SA059956','N','N','','EIS_XXWC_AR_CREDIT_MEMO_ALL_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'White Cap ALL Credit Memo Report - WC',222,'ORIGINAL_INVOICE_DATE','Original Invoice Date','Original Invoice Date','','','default','','27','N','Y','','','','','','','SA059956','N','N','','EIS_XXWC_AR_CREDIT_MEMO_ALL_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'White Cap ALL Credit Memo Report - WC',222,'ORIGINAL_INVOICE_NUMBER','Original Invoice Number','Original Invoice Number','','','default','','26','N','Y','','','','','','','SA059956','N','N','','EIS_XXWC_AR_CREDIT_MEMO_ALL_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'White Cap ALL Credit Memo Report - WC',222,'PART_NUMBER','Part Number','Part Number','','','default','','29','N','Y','','','','','','','SA059956','N','N','','EIS_XXWC_AR_CREDIT_MEMO_ALL_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'White Cap ALL Credit Memo Report - WC',222,'QTY_SHIPPED','Qty Shipped','Qty Shipped','','~~~','default','','33','N','Y','','','','','','','SA059956','N','N','','EIS_XXWC_AR_CREDIT_MEMO_ALL_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'White Cap ALL Credit Memo Report - WC',222,'SALES_REP_NO','Sales Rep No','Sales Rep No','','','default','','34','N','Y','','','','','','','SA059956','N','N','','EIS_XXWC_AR_CREDIT_MEMO_ALL_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'White Cap ALL Credit Memo Report - WC',222,'SHIP_NAME','Ship Name','Ship Name','','','default','','5','N','Y','','','','','','','SA059956','N','N','','EIS_XXWC_AR_CREDIT_MEMO_ALL_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'White Cap ALL Credit Memo Report - WC',222,'TAKEN_BY','Taken By','Taken By','','','default','','38','N','Y','','','','','','','SA059956','N','N','','EIS_XXWC_AR_CREDIT_MEMO_ALL_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'White Cap ALL Credit Memo Report - WC',222,'TAX_AMT','Tax Amt','Tax Amt','','~T~D~2','default','','19','N','Y','','','','','','','SA059956','N','N','','EIS_XXWC_AR_CREDIT_MEMO_ALL_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'White Cap ALL Credit Memo Report - WC',222,'TAX_RATE','Tax Rate','Tax Rate','','~T~D~2','default','','11','N','Y','','','','','','','SA059956','N','N','','EIS_XXWC_AR_CREDIT_MEMO_ALL_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'White Cap ALL Credit Memo Report - WC',222,'ORIGIN_DATE','Appl Date','Origin Date','','','default','','4','N','Y','','','','','','','SA059956','N','N','','EIS_XXWC_AR_CREDIT_MEMO_ALL_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'White Cap ALL Credit Memo Report - WC',222,'RESTOCK_FEE_PERCENT','Restock Fee Percent','Restock Fee Percent','','~~~','default','','17','N','Y','','','','','','','SA059956','N','N','','EIS_XXWC_AR_CREDIT_MEMO_ALL_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'White Cap ALL Credit Memo Report - WC',222,'AR_CUSTOMER_NUMBER','Customer Acct #','Ar Customer Number','','','default','','16','N','Y','','','','','','','SA059956','N','N','','EIS_XXWC_AR_CREDIT_MEMO_ALL_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'White Cap ALL Credit Memo Report - WC',222,'ORDER_DATE','Booked Date','Order Date','','','default','','22','N','Y','','','','','','','SA059956','N','N','','EIS_XXWC_AR_CREDIT_MEMO_ALL_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'White Cap ALL Credit Memo Report - WC',222,'SHIP_DATE','Original Shipment Date','Ship Date','','','default','','24','N','Y','','','','','','','SA059956','N','N','','EIS_XXWC_AR_CREDIT_MEMO_ALL_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'White Cap ALL Credit Memo Report - WC',222,'CREATION_DATE','Order Creation Date','Creation Date','','','default','','23','N','Y','','','','','','','SA059956','N','N','','EIS_XXWC_AR_CREDIT_MEMO_ALL_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'White Cap ALL Credit Memo Report - WC',222,'LINE_TYPE','Line Type','Line Type','','','default','','25','N','Y','','','','','','','SA059956','N','N','','EIS_XXWC_AR_CREDIT_MEMO_ALL_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'White Cap ALL Credit Memo Report - WC',222,'BUSINESS_DATE','Business Date','Business Date','','','default','','10','N','Y','','','','','','','SA059956','N','N','','EIS_XXWC_AR_CREDIT_MEMO_ALL_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'White Cap ALL Credit Memo Report - WC',222,'CREDIT_REASON_SALE','Credit Reason Sale','Credit Reason Sale','','','default','','30','N','Y','','','','','','','SA059956','N','N','','EIS_XXWC_AR_CREDIT_MEMO_ALL_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'White Cap ALL Credit Memo Report - WC',222,'CUSTOMER_NAME','Site Name','Customer Name','','','default','','36','N','Y','','','','','','','SA059956','N','N','','EIS_XXWC_AR_CREDIT_MEMO_ALL_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'White Cap ALL Credit Memo Report - WC',222,'CUSTOMER_NUMBER','Customer Site #','Customer Number','','','default','','3','N','Y','','','','','','','SA059956','N','N','','EIS_XXWC_AR_CREDIT_MEMO_ALL_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'White Cap ALL Credit Memo Report - WC',222,'CUSTOMER_TERRITORY','Collector','Customer Territory','','','default','','35','N','Y','','','','','','','SA059956','N','N','','EIS_XXWC_AR_CREDIT_MEMO_ALL_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'White Cap ALL Credit Memo Report - WC',222,'FISCAL_MONTH','Fiscal Month','Fiscal Month','','','default','','2','N','Y','','','','','','','SA059956','N','N','','EIS_XXWC_AR_CREDIT_MEMO_ALL_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'White Cap ALL Credit Memo Report - WC',222,'FISCAL_YEAR','Fiscal Year','Fiscal Year','','~~~','default','','1','N','Y','','','','','','','SA059956','N','N','','EIS_XXWC_AR_CREDIT_MEMO_ALL_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'White Cap ALL Credit Memo Report - WC',222,'FREIGHT_ORIGINAL','Freight Original','Freight Original','','~T~D~2','default','','12','N','Y','','','','','','','SA059956','N','N','','EIS_XXWC_AR_CREDIT_MEMO_ALL_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'White Cap ALL Credit Memo Report - WC',222,'FREIGHT_REMAINING','Freight Remaining','Freight Remaining','','~T~D~2','default','','13','N','Y','','','','','','','SA059956','N','N','','EIS_XXWC_AR_CREDIT_MEMO_ALL_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'White Cap ALL Credit Memo Report - WC',222,'INVOICE_DATE','Invoice Date','Invoice Date','','','default','','7','N','Y','','','','','','','SA059956','N','N','','EIS_XXWC_AR_CREDIT_MEMO_ALL_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'White Cap ALL Credit Memo Report - WC',222,'INVOICE_LINE_AMOUNT','Invoice Line Amount','Invoice Line Amount','','~T~D~2','default','','31','N','Y','','','','','','','SA059956','N','N','','EIS_XXWC_AR_CREDIT_MEMO_ALL_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'White Cap ALL Credit Memo Report - WC',222,'ORDER_AMOUNT','Order Amount','Order Amount','','~T~D~2','default','','15','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_AR_CREDIT_MEMO_ALL_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'White Cap ALL Credit Memo Report - WC',222,'ORDER_LINE_COST','Order Line Cost','Order Line Cost','','~T~D~2','default','','32','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_AR_CREDIT_MEMO_ALL_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'White Cap ALL Credit Memo Report - WC',222,'TRANSACTION_SOURCE','Trx Source','Transaction Source','','','default','','9','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_AR_CREDIT_MEMO_ALL_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'White Cap ALL Credit Memo Report - WC',222,'TRX_CREATION_DATE','Trx Date','Trx Creation Date','','','default','','8','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_AR_CREDIT_MEMO_ALL_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'White Cap ALL Credit Memo Report - WC',222,'ORIG_ORDER_REF','Orig Order Ref','Orig Order Ref','','','default','','6','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_AR_CREDIT_MEMO_ALL_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'White Cap ALL Credit Memo Report - WC',222,'ORGINAL_ORDER_NUMBER','Orginal Order Number','Orginal Order Number','','~~~','default','','28','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_AR_CREDIT_MEMO_ALL_V','','','','US','');
--Inserting Report Parameters - White Cap ALL Credit Memo Report - WC
xxeis.eis_rsc_ins.rp( 'White Cap ALL Credit Memo Report - WC',222,'Fiscal Year','Fiscal Year','FISCAL_YEAR','IN','Fiscal Year','SELECT distinct period_year FROM GL_PERIOD_STATUSES WHERE  APPLICATION_ID=101 AND CLOSING_STATUS=''O'' and sysdate between start_date and end_date','NUMBER','Y','Y','1','N','N','SQL','SA059956','Y','N','','','','EIS_XXWC_AR_CREDIT_MEMO_ALL_V','','','US','');
xxeis.eis_rsc_ins.rp( 'White Cap ALL Credit Memo Report - WC',222,'Location','Location','LOCATION','IN','AR Organizaion Code LOV','','VARCHAR2','N','Y','5','Y','N','CONSTANT','SA059956','Y','N','','','','EIS_XXWC_AR_CREDIT_MEMO_ALL_V','','','US','');
xxeis.eis_rsc_ins.rp( 'White Cap ALL Credit Memo Report - WC',222,'Fiscal Month','Fiscal Month','','IN','XXWC Fiscal Month LOV','SELECT distinct period_name FROM GL_PERIOD_STATUSES WHERE  APPLICATION_ID=101 AND CLOSING_STATUS=''O'' and sysdate between start_date and end_date','VARCHAR2','N','Y','2','Y','N','SQL','SA059956','Y','N','','','','EIS_XXWC_AR_CREDIT_MEMO_ALL_V','','','US','');
xxeis.eis_rsc_ins.rp( 'White Cap ALL Credit Memo Report - WC',222,'Transaction Date From','Transaction Date From','TRX_CREATION_DATE','>=','','','DATE','N','Y','3','N','N','CONSTANT','SA059956','Y','N','','Start Date','','EIS_XXWC_AR_CREDIT_MEMO_ALL_V','','','US','');
xxeis.eis_rsc_ins.rp( 'White Cap ALL Credit Memo Report - WC',222,'Transaction Date To','Transaction Date To','TRX_CREATION_DATE','<=','','','DATE','N','Y','4','N','N','CONSTANT','SA059956','Y','N','','End Date','','EIS_XXWC_AR_CREDIT_MEMO_ALL_V','','','US','');
--Inserting Dependent Parameters - White Cap ALL Credit Memo Report - WC
xxeis.eis_rsc_ins.rdp( 'White Cap ALL Credit Memo Report - WC',222,'PERIOD_YEAR','Fiscal Month','Fiscal Year','IN','N','');
--Inserting Report Conditions - White Cap ALL Credit Memo Report - WC
xxeis.eis_rsc_ins.rcnh( 'White Cap ALL Credit Memo Report - WC',222,'Free Text ','FREE_TEXT','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','','','','','','','','','','','','','','','Y','','','','','AND PROCESS_ID = :SYSTEM.PROCESS_ID','1',222,'White Cap ALL Credit Memo Report - WC','Free Text ');
--Inserting Report Sorts - White Cap ALL Credit Memo Report - WC
xxeis.eis_rsc_ins.rs( 'White Cap ALL Credit Memo Report - WC',222,'INVOICE_NUMBER','ASC','SA059956','2','');
xxeis.eis_rsc_ins.rs( 'White Cap ALL Credit Memo Report - WC',222,'ORDER_NUMBER','ASC','SA059956','1','');
xxeis.eis_rsc_ins.rs( 'White Cap ALL Credit Memo Report - WC',222,'TRANSACTION_SOURCE','ASC','SA059956','3','');
--Inserting Report Triggers - White Cap ALL Credit Memo Report - WC
xxeis.eis_rsc_ins.rt( 'White Cap ALL Credit Memo Report - WC',222,'begin
XXEIS.EIS_XXWC_CREDIT_ALL_DET_PKG.POPULATE_CREDIT_INVOICES( P_PROCESS_ID => :SYSTEM.PROCESS_ID,
P_PERIOD_YEAR => :Fiscal Year,
P_PERIOD_MONTH => :Fiscal Month,
P_TRX_DATE_FROM => :Transaction Date From,
P_TRX_DATE_TO => :Transaction Date To,
p_location => :Location);
end;','B','Y','SA059956','AQ');
--inserting report templates - White Cap ALL Credit Memo Report - WC
--Inserting Report Portals - White Cap ALL Credit Memo Report - WC
--inserting report dashboards - White Cap ALL Credit Memo Report - WC
--Exporting report views table information -  
xxeis.eis_rsc_ins.rviews( 'White Cap ALL Credit Memo Report - WC','222','EIS_XXWC_AR_CREDIT_MEMO_ALL_V','EIS_XXWC_AR_CREDIT_MEMO_ALL_V','N','');
--inserting report security - White Cap ALL Credit Memo Report - WC
xxeis.eis_rsc_ins.rsec( 'White Cap ALL Credit Memo Report - WC','401','','XXWC_AO_INV_ADJ_REC',222,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'White Cap ALL Credit Memo Report - WC','222','','HDS_RCVBLS_MNGR',222,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'White Cap ALL Credit Memo Report - WC','222','','HDS_RCVBLS_MNGR_CAN',222,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'White Cap ALL Credit Memo Report - WC','222','','XXWC_RECEIVABLES_INQUIRY_WC',222,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'White Cap ALL Credit Memo Report - WC','401','','XXWC_RECEIVING_ASSOCIATE',222,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'White Cap ALL Credit Memo Report - WC','222','','RECEIVABLES_MANAGER',222,'SA059956','','','');
--Inserting Report Pivots - White Cap ALL Credit Memo Report - WC
--Inserting Report   Version details- White Cap ALL Credit Memo Report - WC
xxeis.eis_rsc_ins.rv( 'White Cap ALL Credit Memo Report - WC','','White Cap ALL Credit Memo Report - WC','SA059956');
ELSE
RAISE_APPLICATION_ERROR (-20001,'Module does not exist. Import/Create the module with id 222');
END IF;
END;
/
