---------------------------------------------------------------------------------------------------------------
/**************************************************************************************************************
  $Header XXEIS.EIS_XXWC_CREDIT_ALL_DET_TBL $
  Module Name : Receivables
  PURPOSE	  : White Cap ALL Credit Memo Report - WC
  VERSION 		  DATE            AUTHOR(S)       		DESCRIPTION
  ------- -----------------  --------------- -----------------------------------------
  1.0     	    01-May-2017      	  Siva   		  TMS#20161121-00172 
**************************************************************************************************************/
DROP TABLE XXEIS.EIS_XXWC_CREDIT_ALL_DET_TBL CASCADE CONSTRAINTS  
/

CREATE GLOBAL TEMPORARY TABLE XXEIS.EIS_XXWC_CREDIT_ALL_DET_TBL
  (
    PROCESS_ID              NUMBER,
    FISCAL_YEAR             NUMBER(15,0),
    FISCAL_MONTH            VARCHAR2(15 BYTE),
    TRX_DATE                DATE,
    CUSTOMER_NUMBER         VARCHAR2(30 BYTE),
    AR_CUSTOMER_NUMBER      VARCHAR2(150 BYTE),
    SHIP_NAME               VARCHAR2(150 BYTE),
    ORIG_ORDER_REF          VARCHAR2(30 BYTE),
    CREATION_DATE           DATE,
    INVOICE_DATE            DATE,
    BUSINESS_DATE           DATE,
    TAX_RATE                NUMBER,
    FREIGHT_ORIGINAL        NUMBER,
    FREIGHT_REMAINING       NUMBER,
    INVOICE_TOTAL           NUMBER,
    LOCATION                VARCHAR2(30 BYTE),
    O_AMOUNT_APPLIED        NUMBER,
    AMOUNT_PAID             NUMBER,
    C_LINE_NUMBER           NUMBER,
    TAX_AMT                 NUMBER,
    INVOICE_NUMBER          VARCHAR2(100 BYTE),
    ORDER_NUMBER            NUMBER,
    ORIGINAL_INVOICE_NUMBER VARCHAR2(100 BYTE),
    ORIGINAL_INVOICE_DATE   DATE,
    PART_NUMBER             VARCHAR2(2000 BYTE),
    CREDIT_REASON_SALE      VARCHAR2(150 BYTE),
    INVOICE_LINE_AMOUNT     NUMBER,
    QTY_SHIPPED             NUMBER,
    SALES_REP_NO            VARCHAR2(150 BYTE),
    CUSTOMER_TERRITORY      VARCHAR2(30 BYTE),
    CUSTOMER_NAME           VARCHAR2(150 BYTE),
    MASTER_NAME             VARCHAR2(240 BYTE),
    ORGINAL_ORDER_NUMBER    NUMBER,
    TAKEN_BY                VARCHAR2(100 BYTE),
    RESTOCK_FEE_PERCENT     NUMBER,
    ORIGIN_DATE             DATE,
    SHIP_DATE               DATE,
    ORDER_DATE              DATE,
    LINE_TYPE               VARCHAR2(150 BYTE),
    TRX_CREATION_DATE       DATE,
    TRANSACTION_SOURCE      VARCHAR2(50 BYTE),
    ORDER_LINE_COST         NUMBER,
    ORDER_AMOUNT            NUMBER
  )
  ON COMMIT preserve rows
/
