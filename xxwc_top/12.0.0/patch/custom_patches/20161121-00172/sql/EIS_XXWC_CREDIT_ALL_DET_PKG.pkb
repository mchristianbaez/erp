CREATE OR REPLACE PACKAGE BODY  XXEIS.EIS_XXWC_CREDIT_ALL_DET_PKG as
 --//============================================================================
--//  
--// Change Request 			:: Performance Issue  
--//
--// Object Usage 				:: This Object Referred by "White Cap ALL Credit Memo Report - WC"
--//
--// Object Name         		:: XXEIS.EIS_XXWC_CREDIT_ALL_DET_PKG
--//
--// Object Type         		:: Package Body
--//
--// Object Description  		:: This Package will trigger in before report and insert the values into Table
--//
--// Version Control
--//============================================================================
--// Vers    Author           Date          Description
--//----------------------------------------------------------------------------
--// 1.0     Siva  			01-May-2017    Initial Build -- TMS#20161121-00172 
--//============================================================================
PROCEDURE POPULATE_CREDIT_INVOICES(P_PROCESS_ID in number,
                          P_PERIOD_YEAR in number,
                          P_PERIOD_MONTH in varchar2,
                          P_TRX_DATE_FROM in date,
                          P_TRX_DATE_TO in date,
                          p_location in varchar2
                          ) is
--//============================================================================
--//
--// Object Name         :: POPULATE_CREDIT_INVOICES
--//
--// Object Usage 		 :: This Object Referred by "White Cap ALL Credit Memo Report - WC"
--//
--// Object Type         :: Procedure
--//
--// Object Description  :: This procedure will insert the values into global temporary Table.
--// 						
--//
--// Version Control
--//============================================================================
--// Version    Author           Date          Description
--//----------------------------------------------------------------------------
--// 1.0     	Siva  			01-May-2017    Initial Build -- TMS#20161121-00172 
--//============================================================================							  
l_stmt 				varchar2(32000);
L_PERIOD_COND 		VARCHAR2(32000);
l_stmt_hdr_qry1		VARCHAR2(32000);
l_stmt_hdr_qry2  VARCHAR2(32000);
l_stmt_hdr_qry5  varchar2(32000);

LC_WHERE_COND       VARCHAR2(32000);
LC_PERIOD_NAME   VARCHAR2(32000);
L_REF_CURSOR1       CURSOR_TYPE4;
L_REF_CURSOR2       CURSOR_TYPE4;
L_REF_CURSOR3       CURSOR_TYPE4;
L_REF_CURSOR6       CURSOR_TYPE4;


  LP_START_DATE varchar2(32);
  lp_end_date  varchar2(32);

TYPE TRX_REC IS RECORD
(CUSTOMER_TRX_ID NUMBER);

type trx_rec_tab is table of trx_rec Index By Binary_Integer;
	trx_tab trx_rec_tab;

type l_credit_memo_det_type
is
  table of XXEIS.EIS_XXWC_CREDIT_ALL_DET_TBL%rowtype index by BINARY_INTEGER;
  l_credit_memo_det_tbl l_credit_memo_det_type;



 Begin
 TRX_TAB.DELETE;
 l_credit_memo_det_tbl.DELETE;
 
    IF P_PERIOD_YEAR      IS NOT NULL THEN
    LC_WHERE_COND:= LC_WHERE_COND||'and gp.period_year in ('||XXEIS.EIS_RS_UTILITY.GET_PARAM_VALUES(P_PERIOD_YEAR)||' )';
  end if;
  
      IF P_PERIOD_MONTH      IS NOT NULL THEN
    LC_WHERE_COND:= LC_WHERE_COND||'and gp.period_name in ('||XXEIS.EIS_RS_UTILITY.GET_PARAM_VALUES(P_PERIOD_MONTH)||' )';
  END IF;
 
   IF p_location      IS NOT NULL THEN
    LC_WHERE_COND:= LC_WHERE_COND||'and loc.organization_code in ('||XXEIS.EIS_RS_UTILITY.GET_PARAM_VALUES(p_location)||' )';
  end if;
  
--  fnd_file.put_line(fnd_file.log,'Start');

-- Identify the driving table and Filtering the data based on the parameters
if P_TRX_DATE_FROM is  null and P_TRX_DATE_TO is null then
if P_PERIOD_MONTH is not null then 
select min(START_DATE),max(END_DATE) 
into lp_start_date,lp_end_date
from GL_PERIODS 
where PERIOD_NAME=P_PERIOD_MONTH;

else if P_PERIOD_YEAR is not null then
select min(START_DATE),max(END_DATE) 
into LP_START_DATE,LP_END_DATE
from GL_PERIODS 
where PERIOD_YEAR=P_PERIOD_YEAR;
end if;
end if;
end if;

if P_TRX_DATE_FROM is not null and P_TRX_DATE_TO is not null then

L_STMT := 'SELECT customer_trx_id FROM RA_CUSTOMER_TRX RCT WHERE 
    TRUNC(rct.creation_date) between '''||P_TRX_DATE_FROM||''' and '''||P_TRX_DATE_TO||'''
    ';

elsif P_TRX_DATE_FROM is  null and P_TRX_DATE_TO is null then
		 		l_stmt :='select max(rctgr.customer_trx_id) from
ra_cust_trx_line_gl_dist     rctgr
where   1=1
and ''REC''                           = RCTGR.ACCOUNT_CLASS
AND ''Y''                             = rctgr.latest_rec_flag
AND rctgr.gl_date between '''||lp_start_date||''' and '''||LP_END_DATE||'''
group by rctgr.customer_trx_id';

end if;


l_stmt_hdr_qry1 :='SELECT '||P_Process_id||' process_id,
  gp.period_year FISCAL_YEAR,
  gp.period_name FISCAL_MONTH,
  RCTO.TRX_DATE trx_date,
  PARTY_SITES.PARTY_SITE_NUMBER CUSTOMER_NUMBER,
  custs.account_number AR_CUSTOMER_NUMBER,
  sites.location ship_name,
  DECODE(olr.reference_line_id,NULL,''N'',''Y'') ORIG_ORDER_REF,
  TRUNC(ohr.creation_date) creation_date,
  TRUNC(rctr.trx_date) invoice_date,
  TRUNC(RCTGO.GL_DATE) BUSINESS_DATE,
  XXEIS.EIS_RS_XXWC_COM_UTIL_PKG.GET_CREDIT_MEMO_TAX_VAL (''TAXRATE'',rctlr.customer_trx_id,rctlr.customer_trx_line_id) tax_rate,
  ps.freight_original freight_original,
  PS.FREIGHT_REMAINING FREIGHT_REMAINING,
  NVL(rctgr.acctd_amount,rctgr.amount) invoice_total,
  loc.organization_code location,
  PSO.AMOUNT_APPLIED O_AMOUNT_APPLIED,
  CASE
    WHEN PS.AMOUNT_APPLIED IS NOT NULL
    THEN PS.AMOUNT_APPLIED      *-1
    ELSE PSO.AMOUNT_DUE_ORIGINAL-PSO.AMOUNT_DUE_REMAINING
  END AMOUNT_PAID,
  RCTLR.LINE_NUMBER C_LINE_NUMBER,
  XXEIS.EIS_RS_XXWC_COM_UTIL_PKG.GET_CREDIT_MEMO_TAX_VAL (''TAXAMT'',rctlr.customer_trx_id,rctlr.customer_trx_line_id) tax_amt,
  rctr.trx_number invoice_number,
  ohr.order_number order_number,
  rcto.trx_number original_invoice_number,
  rcto.trx_date original_invoice_date,
  olr.ordered_item part_number,
  olr.Return_Reason_Code credit_reason_sale,
  rctlr.extended_amount invoice_line_amount,
  NVL(rctlr.quantity_invoiced,rctlr.quantity_credited) qty_shipped,
  jsr.salesrep_number sales_rep_no,
  (SELECT ac.name
  FROM HZ_CUSTOMER_PROFILES HZP,
    AR_COLLECTORS AC
  WHERE hzp.cust_account_id = custb.cust_account_id
  AND hzp.site_use_id       = siteb.site_use_id
  AND AC.COLLECTOR_ID       = HZP.COLLECTOR_ID
  AND rownum                =1
  ) customer_territory,
  CASE
    WHEN instr(sites.location,''-'') > 0
    THEN SUBSTR(sites.location,1,(instr(sites.location,''-'')-1))
    ELSE sites.location
  END customer_name,
  custb.account_name master_name,
  oho.order_number orginal_order_number,
  FU.USER_NAME TAKEN_BY,
  xxeis.eis_rs_xxwc_com_util_pkg.xxwc_cal_restock_per(olr.header_id) restock_fee_percent,
  (SELECT MAX(apply_date)
  FROM ar_receivable_applications
  WHERE application_type=''CM''
  AND customer_trx_id   =rctr.customer_trx_id
  ) origin_date,
  DECODE(oho.order_type_id,1004,TRUNC(olo.schedule_ship_date), TRUNC(olo.actual_shipment_date)) Ship_date,
  TRUNC(ohr.ordered_date) order_date,
  ott.name line_type,
  TRUNC(rctr.creation_date) trx_creation_date,
  bs.name transaction_source,
  DECODE(olr.line_category_code,''RETURN'',(                           -1)*olr.unit_cost,olr.unit_cost) order_line_cost,
  (SELECT NVL ( SUM ( ( NVL (DECODE(oel.line_category_code,''RETURN'',(-1)* oel.ORDERED_QUANTITY,oel.ORDERED_QUANTITY), 0) * NVL (oel.UNIT_SELLING_PRICE, 0)) + NVL (DECODE(oel.line_category_code,''RETURN'',(-1)*oel.TAX_VALUE,oel.TAX_VALUE), 0) + NVL (DECODE(oel.line_category_code,''RETURN'',(-1)*ocl.charge_amount,ocl.charge_amount), 0)), 0)
  FROM oe_order_lines oel,
    oe_charge_lines_v ocl
  WHERE oel.header_id = ohr.header_id
  AND oel.header_id   = ocl.header_id(+)
  AND oel.line_id     = ocl.line_id(+)
  GROUP BY oel.header_id
  ) order_amount
FROM ra_customer_trx rctr,
  ra_customer_trx_lines rctlr,
  ra_cust_trx_line_gl_dist rctgr,
  oe_order_headers ohr,
  oe_order_lines olr,
  ar_payment_schedules ps,
  ra_cust_trx_types ctt,
  oe_order_lines olo,
  oe_order_headers oho,
  ra_customer_trx rcto,
  ra_customer_trx_lines rctlo,
  ra_cust_trx_line_gl_dist rctgo,
  ar_payment_schedules pso,
  hz_cust_accounts custs,
  hz_parties partys,
  hz_cust_site_uses sites,
  hz_cust_acct_sites cust_sites,
  hz_party_sites party_sites,
  hz_cust_accounts custb,
  hz_parties partyb,
  hz_cust_site_uses siteb,
  jtf_rs_salesreps jsr,
  org_organization_definitions loc,
  oe_transaction_types_vl ott,
  ra_batch_sources bs,
  fnd_user fu,
  gl_periods gp
WHERE rctr.customer_trx_id           = rctlr.customer_trx_id
AND rctgr.customer_trx_id            = rctr.customer_trx_id
AND rctgr.account_class              =''REC''
AND rctgr.latest_rec_flag            =''Y''
--AND rctr.interface_header_context    = ''ORDER ENTRY''
AND rctr.interface_header_attribute1 = ohr.order_number(+)
AND rctlr.interface_line_context     = ''ORDER ENTRY''
AND rctlr.interface_line_attribute6  = olr.line_id(+)
AND rctr.customer_trx_id             = ps.customer_trx_id(+)
AND rctr.CUST_TRX_TYPE_ID            = CTT.CUST_TRX_TYPE_ID
AND rctr.org_id                      = ctt.org_id
AND ctt.name                         = ''Credit Memo''
AND olr.reference_header_id          = olo.header_id(+)
AND olr.reference_line_id            = olo.line_id(+)
AND olo.header_id                    = oho.header_id(+)
AND TO_CHAR(olo.line_id)             = rctlo.interface_line_attribute6(+)
AND rctlo.interface_line_context(+)  = ''ORDER ENTRY''
AND rctlo.customer_trx_id            = rcto.customer_trx_id(+)
AND rcto.customer_trx_id             = rctgo.customer_trx_id(+)
AND rctgo.account_class (+)          = ''REC''
AND rctgo.latest_rec_flag (+)        = ''Y''
AND rcto.customer_trx_id             = pso.customer_trx_id(+)
AND rctr.ship_to_customer_id         = custs.cust_account_id(+)
AND custs.party_id                   = partys.party_id(+)
AND RCTR.SHIP_TO_SITE_USE_ID         = SITES.SITE_USE_ID(+)
AND CUST_SITES.CUST_ACCT_SITE_ID(+)  = SITES.CUST_ACCT_SITE_ID
AND cust_sites.party_site_id         = party_sites.party_site_id(+)
AND rctr.bill_to_customer_id         = custb.cust_account_id(+)
AND custb.party_id                   = partyb.party_id(+)
AND rctr.bill_to_site_use_id         = siteb.site_use_id(+)
AND rctr.primary_salesrep_id         = jsr.salesrep_id (+)
AND rctr.org_id                      = jsr.org_id (+)
AND olr.ship_from_org_id             = loc.organization_id(+)
AND olr.line_type_id                 = ott.transaction_type_id(+)
AND rctr.batch_source_id             = bs.batch_source_id
AND rctr.org_id                      = bs.org_id
AND bs.name NOT                     IN (''WC MANUAL'',''MANUAL-OTHER'',''REBILL'',''REBILL-CM'')
AND ohr.created_by                   = fu.user_id(+)
AND gp.period_set_name               = ''4-4-QTR''
AND rctgr.gl_date BETWEEN gp.start_date AND gp.end_date
and rctr.customer_trx_id = :1 
';
      
   l_stmt_hdr_qry2:='SELECT '||P_Process_id||' process_id,
  gp.period_year FISCAL_YEAR,
  gp.period_name FISCAL_MONTH,
  RCTO.TRX_DATE trx_date,
  PARTY_SITES.PARTY_SITE_NUMBER CUSTOMER_NUMBER,
  custs.account_number AR_CUSTOMER_NUMBER,
  sites.location ship_name,
  DECODE(olr.reference_line_id,NULL,''N'',''Y'') ORIG_ORDER_REF,
  TRUNC(ohr.creation_date) creation_date,
  TRUNC(rctr.trx_date) invoice_date,
  TRUNC(RCTGO.GL_DATE) BUSINESS_DATE,
  XXEIS.EIS_RS_XXWC_COM_UTIL_PKG.GET_CREDIT_MEMO_TAX_VAL (''TAXRATE'',rctlr.customer_trx_id,rctlr.customer_trx_line_id) tax_rate,
  ps.freight_original freight_original,
  PS.FREIGHT_REMAINING FREIGHT_REMAINING,
  NVL(rctgr.acctd_amount,rctgr.amount) invoice_total,
  loc.organization_code location,
  PSO.AMOUNT_APPLIED O_AMOUNT_APPLIED,
  CASE
    WHEN PS.AMOUNT_APPLIED IS NOT NULL
    THEN PS.AMOUNT_APPLIED      *-1
    ELSE PSO.AMOUNT_DUE_ORIGINAL-PSO.AMOUNT_DUE_REMAINING
  END AMOUNT_PAID,
  RCTLR.LINE_NUMBER C_LINE_NUMBER,
  XXEIS.EIS_RS_XXWC_COM_UTIL_PKG.GET_CREDIT_MEMO_TAX_VAL (''TAXAMT'',rctlr.customer_trx_id,rctlr.customer_trx_line_id) tax_amt,
  rctr.trx_number invoice_number,
  ohr.order_number order_number,
  rcto.trx_number original_invoice_number,
  rcto.trx_date original_invoice_date,
  olr.ordered_item part_number,
  olr.Return_Reason_Code credit_reason_sale,
  rctlr.extended_amount invoice_line_amount,
  NVL(rctlr.quantity_invoiced,rctlr.quantity_credited) qty_shipped,
  jsr.salesrep_number sales_rep_no,
  (SELECT ac.name
  FROM HZ_CUSTOMER_PROFILES HZP,
    AR_COLLECTORS AC
  WHERE hzp.cust_account_id = custb.cust_account_id
  AND hzp.site_use_id       = siteb.site_use_id
  AND AC.COLLECTOR_ID       = HZP.COLLECTOR_ID
  AND rownum                =1
  ) customer_territory,
  CASE
    WHEN instr(sites.location,''-'') > 0
    THEN SUBSTR(sites.location,1,(instr(sites.location,''-'')-1))
    ELSE sites.location
  END customer_name,
  custb.account_name master_name,
  oho.order_number orginal_order_number,
  FU.USER_NAME TAKEN_BY,
  xxeis.eis_rs_xxwc_com_util_pkg.xxwc_cal_restock_per(olr.header_id) restock_fee_percent,
  (SELECT MAX(apply_date)
  FROM ar_receivable_applications
  WHERE application_type=''CM''
  AND customer_trx_id   =rctr.customer_trx_id
  ) origin_date,
  DECODE(oho.order_type_id,1004,TRUNC(olo.schedule_ship_date), TRUNC(olo.actual_shipment_date)) Ship_date,
  TRUNC(ohr.ordered_date) order_date,
  ott.name line_type,
  TRUNC(rctr.creation_date) trx_creation_date,
  bs.name transaction_source,
  DECODE(olr.line_category_code,''RETURN'',(                           -1)*olr.unit_cost,olr.unit_cost) order_line_cost,
  (SELECT NVL ( SUM ( ( NVL (DECODE(oel.line_category_code,''RETURN'',(-1)* oel.ORDERED_QUANTITY,oel.ORDERED_QUANTITY), 0) * NVL (oel.UNIT_SELLING_PRICE, 0)) + NVL (DECODE(oel.line_category_code,''RETURN'',(-1)*oel.TAX_VALUE,oel.TAX_VALUE), 0) + NVL (DECODE(oel.line_category_code,''RETURN'',(-1)*ocl.charge_amount,ocl.charge_amount), 0)), 0)
  FROM oe_order_lines oel,
    oe_charge_lines_v ocl
  WHERE oel.header_id = ohr.header_id
  AND oel.header_id   = ocl.header_id(+)
  AND oel.line_id     = ocl.line_id(+)
  GROUP BY oel.header_id
  ) order_amount
FROM ra_customer_trx rctr,
  ra_customer_trx_lines rctlr,
  ra_cust_trx_line_gl_dist rctgr,
  oe_order_headers ohr,
  oe_order_lines olr,
  ar_payment_schedules ps,
  oe_order_lines olo,
  oe_order_headers oho,
  ra_customer_trx rcto,
  ra_customer_trx_lines rctlo,
  ra_cust_trx_line_gl_dist rctgo,
  ar_payment_schedules pso,
  hz_cust_accounts custs,
  hz_parties partys,
  hz_cust_site_uses sites,
  hz_cust_acct_sites cust_sites,
  hz_party_sites party_sites,
  hz_cust_accounts custb,
  hz_parties partyb,
  hz_cust_site_uses siteb,
  jtf_rs_salesreps jsr,
  org_organization_definitions loc,
  oe_transaction_types_vl ott,
  ra_batch_sources bs,
  fnd_user fu,
  gl_periods gp
WHERE rctr.customer_trx_id           = rctlr.customer_trx_id
AND rctgr.customer_trx_id            = rctr.customer_trx_id
AND rctgr.account_class              =''REC''
AND rctgr.latest_rec_flag            =''Y''
--AND rctr.interface_header_context    = ''ORDER ENTRY''
AND rctr.interface_header_attribute1 = ohr.order_number(+)
AND rctlr.interface_line_context     = ''ORDER ENTRY''
AND rctlr.interface_line_attribute6  = olr.line_id(+)
AND rctr.customer_trx_id             = ps.customer_trx_id(+)
AND olr.reference_header_id          = olo.header_id(+)
AND olr.reference_line_id            = olo.line_id(+)
AND olo.header_id                    = oho.header_id(+)
AND TO_CHAR(olo.line_id)             = rctlo.interface_line_attribute6(+)
AND rctlo.interface_line_context(+)  = ''ORDER ENTRY''
AND rctlo.customer_trx_id            = rcto.customer_trx_id(+)
AND rcto.customer_trx_id             = rctgo.customer_trx_id(+)
AND rctgo.account_class (+)          = ''REC''
AND rctgo.latest_rec_flag (+)        = ''Y''
AND rcto.customer_trx_id             = pso.customer_trx_id(+)
AND rctr.ship_to_customer_id         = custs.cust_account_id(+)
AND custs.party_id                   = partys.party_id(+)
AND RCTR.SHIP_TO_SITE_USE_ID         = SITES.SITE_USE_ID(+)
AND CUST_SITES.CUST_ACCT_SITE_ID(+)  = SITES.CUST_ACCT_SITE_ID
AND cust_sites.party_site_id         = party_sites.party_site_id(+)
AND rctr.bill_to_customer_id         = custb.cust_account_id(+)
AND custb.party_id                   = partyb.party_id(+)
AND rctr.bill_to_site_use_id         = siteb.site_use_id(+)
AND rctr.primary_salesrep_id         = jsr.salesrep_id (+)
AND rctr.org_id                      = jsr.org_id (+)
AND olr.ship_from_org_id             = loc.organization_id(+)
AND olr.line_type_id                 = ott.transaction_type_id(+)
AND rctr.batch_source_id             = bs.batch_source_id
AND rctr.org_id                      = bs.org_id
AND bs.name  in (''REBILL'',''REBILL-CM'')
AND ohr.created_by                   = fu.user_id(+)
AND gp.period_set_name               = ''4-4-QTR''
AND rctgr.gl_date BETWEEN gp.start_date AND gp.end_date
and rctr.customer_trx_id = :1 
';
   

l_stmt_hdr_qry5:='SELECT '||P_Process_id||' process_id,
  gp.period_year FISCAL_YEAR,
  gp.period_name FISCAL_MONTH,
  RCTO.TRX_DATE trx_date,
  PARTY_SITES.PARTY_SITE_NUMBER CUSTOMER_NUMBER,
  custs.account_number AR_CUSTOMER_NUMBER,
  sites.location ship_name,
  DECODE(olr.reference_line_id,NULL,''N'',''Y'') ORIG_ORDER_REF,
  TRUNC(ohr.creation_date) creation_date,
  TRUNC(rctr.trx_date) invoice_date,
  TRUNC(RCTGO.GL_DATE) BUSINESS_DATE,
  XXEIS.EIS_RS_XXWC_COM_UTIL_PKG.GET_CREDIT_MEMO_TAX_VAL (''TAXRATE'',rctlr.customer_trx_id,rctlr.customer_trx_line_id) tax_rate,
  ps.freight_original freight_original,
  PS.FREIGHT_REMAINING FREIGHT_REMAINING,
  NVL(rctgr.acctd_amount,rctgr.amount) invoice_total,
  loc.organization_code location,
  PSO.AMOUNT_APPLIED O_AMOUNT_APPLIED,
  CASE
    WHEN PS.AMOUNT_APPLIED IS NOT NULL
    THEN PS.AMOUNT_APPLIED      *-1
    ELSE PSO.AMOUNT_DUE_ORIGINAL-PSO.AMOUNT_DUE_REMAINING
  END AMOUNT_PAID,
  RCTLR.LINE_NUMBER C_LINE_NUMBER,
  XXEIS.EIS_RS_XXWC_COM_UTIL_PKG.GET_CREDIT_MEMO_TAX_VAL (''TAXAMT'',rctlr.customer_trx_id,rctlr.customer_trx_line_id) tax_amt,
  rctr.trx_number invoice_number,
  ohr.order_number order_number,
  rcto.trx_number original_invoice_number,
  rcto.trx_date original_invoice_date,
  olr.ordered_item part_number,
  olr.Return_Reason_Code credit_reason_sale,
  rctlr.extended_amount invoice_line_amount,
  NVL(rctlr.quantity_invoiced,rctlr.quantity_credited) qty_shipped,
  jsr.salesrep_number sales_rep_no,
  (SELECT ac.name
  FROM HZ_CUSTOMER_PROFILES HZP,
    AR_COLLECTORS AC
  WHERE hzp.cust_account_id = custb.cust_account_id
  AND hzp.site_use_id       = siteb.site_use_id
  AND AC.COLLECTOR_ID       = HZP.COLLECTOR_ID
  AND rownum                =1
  ) customer_territory,
  CASE
    WHEN instr(sites.location,''-'') > 0
    THEN SUBSTR(sites.location,1,(instr(sites.location,''-'')-1))
    ELSE sites.location
  END customer_name,
  custb.account_name master_name,
  oho.order_number orginal_order_number,
  FU.USER_NAME TAKEN_BY,
  xxeis.eis_rs_xxwc_com_util_pkg.xxwc_cal_restock_per(olr.header_id) restock_fee_percent,
  (SELECT MAX(apply_date)
  FROM ar_receivable_applications
  WHERE application_type=''CM''
  AND customer_trx_id   =rctr.customer_trx_id
  ) origin_date,
  DECODE(oho.order_type_id,1004,TRUNC(olo.schedule_ship_date), TRUNC(olo.actual_shipment_date)) Ship_date,
  TRUNC(ohr.ordered_date) order_date,
  ott.name line_type,
  TRUNC(rctr.creation_date) trx_creation_date,
  bs.name transaction_source,
  DECODE(olr.line_category_code,''RETURN'',(                           -1)*olr.unit_cost,olr.unit_cost) order_line_cost,
  (SELECT NVL ( SUM ( ( NVL (DECODE(oel.line_category_code,''RETURN'',(-1)* oel.ORDERED_QUANTITY,oel.ORDERED_QUANTITY), 0) * NVL (oel.UNIT_SELLING_PRICE, 0)) + NVL (DECODE(oel.line_category_code,''RETURN'',(-1)*oel.TAX_VALUE,oel.TAX_VALUE), 0) + NVL (DECODE(oel.line_category_code,''RETURN'',(-1)*ocl.charge_amount,ocl.charge_amount), 0)), 0)
  FROM oe_order_lines oel,
    oe_charge_lines_v ocl
  WHERE oel.header_id = ohr.header_id
  AND oel.header_id   = ocl.header_id(+)
  AND oel.line_id     = ocl.line_id(+)
  GROUP BY oel.header_id
  ) order_amount
FROM ra_customer_trx rctr,
  ra_customer_trx_lines rctlr,
  ra_cust_trx_line_gl_dist rctgr,
  oe_order_headers ohr,
  oe_order_lines olr,
  ar_payment_schedules ps,
  ra_cust_trx_types ctt,
  oe_order_lines olo,
  oe_order_headers oho,
  ra_customer_trx rcto,
  ra_customer_trx_lines rctlo,
  ra_cust_trx_line_gl_dist rctgo,
  ar_payment_schedules pso,
  hz_cust_accounts custs,
  hz_parties partys,
  hz_cust_site_uses sites,
  hz_cust_acct_sites cust_sites,
  hz_party_sites party_sites,
  hz_cust_accounts custb,
  hz_parties partyb,
  hz_cust_site_uses siteb,
  jtf_rs_salesreps jsr,
  org_organization_definitions loc,
  oe_transaction_types_vl ott,
  ra_batch_sources bs,
  fnd_user fu,
  gl_periods gp
WHERE rctr.customer_trx_id           = rctlr.customer_trx_id
AND rctgr.customer_trx_id            = rctr.customer_trx_id
AND rctgr.account_class              =''REC''
AND rctgr.latest_rec_flag            =''Y''
--AND rctr.interface_header_context    = ''ORDER ENTRY''
AND rctr.interface_header_attribute1 = ohr.order_number(+)
AND rctlr.interface_line_context     = ''ORDER ENTRY''
AND rctlr.interface_line_attribute6  = olr.line_id(+)
AND rctr.customer_trx_id             = ps.customer_trx_id(+)
AND rctr.CUST_TRX_TYPE_ID            = CTT.CUST_TRX_TYPE_ID
AND rctr.org_id                      = ctt.org_id
AND ctt.name                        != ''Credit Memo''
AND olr.reference_header_id          = olo.header_id(+)
AND olr.reference_line_id            = olo.line_id(+)
AND olo.header_id                    = oho.header_id(+)
AND TO_CHAR(olo.line_id)             = rctlo.interface_line_attribute6(+)
AND rctlo.interface_line_context(+)  = ''ORDER ENTRY''
AND rctlo.customer_trx_id            = rcto.customer_trx_id(+)
AND rcto.customer_trx_id             = rctgo.customer_trx_id(+)
AND rctgo.account_class (+)          = ''REC''
AND rctgo.latest_rec_flag (+)        = ''Y''
AND rcto.customer_trx_id             = pso.customer_trx_id(+)
AND rctr.ship_to_customer_id         = custs.cust_account_id(+)
AND custs.party_id                   = partys.party_id(+)
AND RCTR.SHIP_TO_SITE_USE_ID         = SITES.SITE_USE_ID(+)
AND CUST_SITES.CUST_ACCT_SITE_ID(+)  = SITES.CUST_ACCT_SITE_ID
AND cust_sites.party_site_id         = party_sites.party_site_id(+)
AND rctr.bill_to_customer_id         = custb.cust_account_id(+)
AND custb.party_id                   = partyb.party_id(+)
AND rctr.bill_to_site_use_id         = siteb.site_use_id(+)
AND rctr.primary_salesrep_id         = jsr.salesrep_id (+)
AND rctr.org_id                      = jsr.org_id (+)
AND olr.ship_from_org_id             = loc.organization_id(+)
AND olr.line_type_id                 = ott.transaction_type_id(+)
AND ott.name	                       = ''BILL ONLY''
and ohr.order_type_id                = 1006
AND rctr.batch_source_id             = bs.batch_source_id
AND rctr.org_id                      = bs.org_id
AND bs.name NOT      IN (''WC MANUAL'',''MANUAL-OTHER'',''REBILL'',''REBILL-CM'')
AND ohr.created_by                   = fu.user_id(+)
AND gp.period_set_name               = ''4-4-QTR''
AND rctgr.gl_date BETWEEN gp.start_date AND gp.end_date
and rctr.customer_trx_id = :1 
';



   
      l_stmt_hdr_qry1:= l_stmt_hdr_qry1||' '||LC_WHERE_COND;  
      
      l_stmt_hdr_qry2:= l_stmt_hdr_qry2||' '||LC_WHERE_COND; 
      
      l_stmt_hdr_qry5:= l_stmt_hdr_qry5||' '||LC_WHERE_COND;
      
      
      fnd_file.put_line(fnd_file.LOG,'l_stmt_hdr_qry1'||l_stmt_hdr_qry1);
       fnd_file.put_line(fnd_file.LOG,'l_stmt_hdr_qry2'||l_stmt_hdr_qry2);
       fnd_file.put_line(fnd_file.LOG,'l_stmt_hdr_qry5'||l_stmt_hdr_qry5);
       


  OPEN L_REF_CURSOR1 FOR l_stmt;
    FETCH L_REF_CURSOR1 BULK COLLECT INTO TRX_TAB;
  CLOSE L_REF_CURSOR1;

l_credit_memo_det_tbl.DELETE;

 IF TRX_TAB.COUNT>0
  THEN
  
  FOR I IN 1..TRX_TAB.COUNT
  LOOP
     OPEN L_REF_CURSOR2 FOR l_stmt_hdr_qry1 USING 
  TRX_TAB(i).CUSTOMER_TRX_ID;
  loop
    FETCH L_REF_CURSOR2 BULK COLLECT INTO l_credit_memo_det_tbl limit 10000;
   
  if l_credit_memo_det_tbl.COUNT>0
  Then  
       FORALL J IN 1..l_credit_memo_det_tbl.COUNT 
  Insert Into xxeis.EIS_XXWC_CREDIT_ALL_DET_TBL Values l_credit_memo_det_tbl(J);
 
   END IF;
    exit when L_REF_CURSOR2%notfound;
    END LOOP;
    CLOSE L_REF_CURSOR2;
 END LOOP; 
 END IF;


l_credit_memo_det_tbl.DELETE;
    
 IF TRX_TAB.COUNT>0
  THEN
   FOR x IN 1..TRX_TAB.COUNT
  LOOP   
   OPEN L_REF_CURSOR3 FOR l_stmt_hdr_qry2 USING 
  TRX_TAB(x).CUSTOMER_TRX_ID;
  loop
    FETCH L_REF_CURSOR3 BULK COLLECT INTO l_credit_memo_det_tbl LIMIT 10000;
   
  if l_credit_memo_det_tbl.COUNT>0
  Then  
       FORALL K IN 1..l_credit_memo_det_tbl.COUNT 
  Insert Into xxeis.EIS_XXWC_CREDIT_ALL_DET_TBL Values l_credit_memo_det_tbl(K);
 
   END IF;
    exit when L_REF_CURSOR3%notfound;
    END LOOP; 
    CLOSE L_REF_CURSOR3;   
 END LOOP; 
 END IF;

l_credit_memo_det_tbl.delete;

 IF TRX_TAB.COUNT>0
  THEN
   FOR x IN 1..TRX_TAB.COUNT
  LOOP   
   OPEN L_REF_CURSOR6 FOR l_stmt_hdr_qry5 USING 
  TRX_TAB(x).CUSTOMER_TRX_ID;
  loop
    FETCH L_REF_CURSOR6 BULK COLLECT INTO l_credit_memo_det_tbl LIMIT 10000;
   
  if l_credit_memo_det_tbl.COUNT>0
  Then  
       FORALL K IN 1..l_credit_memo_det_tbl.COUNT 
  Insert Into xxeis.EIS_XXWC_CREDIT_ALL_DET_TBL Values l_credit_memo_det_tbl(K);
 
   END IF;
    exit when L_REF_CURSOR6%notfound;
    END LOOP; 
    CLOSE L_REF_CURSOR6;   
 END LOOP; 
 END IF;
 
l_credit_memo_det_tbl.DELETE;


exception when others then
Fnd_File.Put_Line(FND_FILE.log,'The ERROR '||sqlcode||sqlerrm);
END;
         

END  EIS_XXWC_CREDIT_ALL_DET_PKG;
/
