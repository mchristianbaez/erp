/*************************************************************************
  $Header TMS_20161012-00018_sales_order_22126355.sql $
  Module Name: TMS_20161012-00018  


  REVISIONS:
  Ver        Date         Author                Description
  ---------  -----------  ------------------    --------------------------
  1.0        17-JAN-2017  Pattabhi Avula         TMS#20161012-00018 

**************************************************************************/ 
SET SERVEROUTPUT ON SIZE 1000000
DECLARE
BEGIN
   DBMS_OUTPUT.put_line ('TMS: 20161012-00018    , Before Update');

update apps.oe_order_lines_all
set 
  INVOICE_INTERFACE_STATUS_CODE='NOT_ELIGIBLE'
,open_flag='N'
,flow_status_code='CLOSED'
--,INVOICED_QUANTITY=1
where line_id =80573117;

   DBMS_OUTPUT.put_line (
         'TMS: 20161012-00018  Sales order lines updated (Expected:1): '
      || SQL%ROWCOUNT);

   COMMIT;

   DBMS_OUTPUT.put_line ('TMS: 20161012-00018    , End Update');
EXCEPTION
   WHEN OTHERS
   THEN
      ROLLBACK;
      DBMS_OUTPUT.put_line ('TMS: 20161012-00018 , Errors : ' || SQLERRM);
END;
/