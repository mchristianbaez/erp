--Report Name            : Aged Items with PO Number Report - WC
--Import Instructions    : Run this file in XXEIS user from SQL*Plus or any tool like Toad, SQL Navigator. 
prompt Creating Object Data EIS_XXWC_INV_AGED_ITEM_PO_V
set scan off define off
DECLARE
mod_exist VARCHAR2(1);
BEGIN 
mod_exist := XXEIS.EIS_RSC_INSERT.check_if_module_exist(401);
IF mod_exist = 'Y' THEN 
--Inserting Object EIS_XXWC_INV_AGED_ITEM_PO_V
xxeis.eis_rsc_ins.v( 'EIS_XXWC_INV_AGED_ITEM_PO_V',401,'','','','','SA059956','XXEIS','Eis Xxwc Inv Aged Item Po V','EXIAIPV','','','VIEW','US','','','');
--Delete Object Columns for EIS_XXWC_INV_AGED_ITEM_PO_V
xxeis.eis_rsc_utility.delete_view_rows('EIS_XXWC_INV_AGED_ITEM_PO_V',401,FALSE);
--Inserting Object Columns for EIS_XXWC_INV_AGED_ITEM_PO_V
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_INV_AGED_ITEM_PO_V','PO_BUYER_NAME',401,'Po Buyer Name','PO_BUYER_NAME','','','','SA059956','VARCHAR2','','','Po Buyer Name','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_INV_AGED_ITEM_PO_V','AVG_COST',401,'Avg Cost','AVG_COST','','','','SA059956','NUMBER','','','Avg Cost','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_INV_AGED_ITEM_PO_V','PO_COST',401,'Po Cost','PO_COST','','','','SA059956','NUMBER','','','Po Cost','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_INV_AGED_ITEM_PO_V','PO_LINE',401,'Po Line','PO_LINE','','','','SA059956','NUMBER','','','Po Line','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_INV_AGED_ITEM_PO_V','PO_NUMBER',401,'Po Number','PO_NUMBER','','','','SA059956','VARCHAR2','','','Po Number','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_INV_AGED_ITEM_PO_V','VENDOR_NAME',401,'Vendor Name','VENDOR_NAME','','','','SA059956','VARCHAR2','','','Vendor Name','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_INV_AGED_ITEM_PO_V','VENDOR_NUMBER',401,'Vendor Number','VENDOR_NUMBER','','','','SA059956','VARCHAR2','','','Vendor Number','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_INV_AGED_ITEM_PO_V','CATCLASS_DESC',401,'Catclass Desc','CATCLASS_DESC','','','','SA059956','VARCHAR2','','','Catclass Desc','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_INV_AGED_ITEM_PO_V','CAT_CLASS',401,'Cat Class','CAT_CLASS','','','','SA059956','VARCHAR2','','','Cat Class','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_INV_AGED_ITEM_PO_V','SUB_CATEGORY',401,'Sub Category','SUB_CATEGORY','','','','SA059956','VARCHAR2','','','Sub Category','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_INV_AGED_ITEM_PO_V','CATEGORY_NAME',401,'Category Name','CATEGORY_NAME','','','','SA059956','VARCHAR2','','','Category Name','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_INV_AGED_ITEM_PO_V','ORGANIZATION_NAME',401,'Organization Name','ORGANIZATION_NAME','','','','SA059956','VARCHAR2','','','Organization Name','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_INV_AGED_ITEM_PO_V','BRANCH_NUMBER',401,'Branch Number','BRANCH_NUMBER','','','','SA059956','VARCHAR2','','','Branch Number','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_INV_AGED_ITEM_PO_V','DISTRICT',401,'District','DISTRICT','','','','SA059956','VARCHAR2','','','District','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_INV_AGED_ITEM_PO_V','REGION',401,'Region','REGION','','','','SA059956','VARCHAR2','','','Region','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_INV_AGED_ITEM_PO_V','SKU_NUMBER',401,'Sku Number','SKU_NUMBER','','','','SA059956','VARCHAR2','','','Sku Number','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_INV_AGED_ITEM_PO_V','SKU_DESCRIPTION',401,'Sku Description','SKU_DESCRIPTION','','','','SA059956','VARCHAR2','','','Sku Description','','','','','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_INV_AGED_ITEM_PO_V','PO_CREATION_DATE',401,'Po Creation Date','PO_CREATION_DATE','','','','SA059956','DATE','','','Po Creation Date','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_INV_AGED_ITEM_PO_V','AGED_QUANTITY',401,'Aged Quantity','AGED_QUANTITY','','','','SA059956','NUMBER','','','Aged Quantity','','','','','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_INV_AGED_ITEM_PO_V','AGE_IN_DAYS',401,'Age In Days','AGE_IN_DAYS','','','','SA059956','NUMBER','','','Age In Days','','','','','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_INV_AGED_ITEM_PO_V','PO_QUANTITY',401,'Po Quantity','PO_QUANTITY','','','','SA059956','NUMBER','','','Po Quantity','','','','','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_INV_AGED_ITEM_PO_V','RECEIVED_DATE',401,'Received Date','RECEIVED_DATE','','','','SA059956','DATE','','','Received Date','','','','','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_INV_AGED_ITEM_PO_V','SHIP_TO_LOCATION',401,'Ship To Location','SHIP_TO_LOCATION','','','','SA059956','VARCHAR2','','','Ship To Location','','','','','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_INV_AGED_ITEM_PO_V','UOM',401,'Uom','UOM','','','','SA059956','VARCHAR2','','','Uom','','','','','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_INV_AGED_ITEM_PO_V','MC_CATEGORY_ID',401,'Mc Category Id','MC_CATEGORY_ID','','','','SA059956','NUMBER','','','Mc Category Id','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_INV_AGED_ITEM_PO_V','MSI_ORGANIZATION_ID',401,'Msi Organization Id','MSI_ORGANIZATION_ID','','','','SA059956','NUMBER','','','Msi Organization Id','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_INV_AGED_ITEM_PO_V','MSI_INVENTORY_ITEM_ID',401,'Msi Inventory Item Id','MSI_INVENTORY_ITEM_ID','','','','SA059956','NUMBER','','','Msi Inventory Item Id','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_INV_AGED_ITEM_PO_V','TRANSACTION_ID',401,'Transaction Id','TRANSACTION_ID','','','','SA059956','NUMBER','','','Transaction Id','','','','','');
--Inserting Object Components for EIS_XXWC_INV_AGED_ITEM_PO_V
xxeis.eis_rsc_ins.vcomp( 'EIS_XXWC_INV_AGED_ITEM_PO_V','MTL_SYSTEM_ITEMS',401,'MTL_SYSTEM_ITEMS_B','MSI','MSI','SA059956','SA059956','-1','Inventory Item Definitions','Y','','','','','',' ',' ',' ',' ',' ');
xxeis.eis_rsc_ins.vcomp( 'EIS_XXWC_INV_AGED_ITEM_PO_V','MTL_CATEGORIES_B',401,'MTL_CATEGORIES_B','MC','MC','SA059956','SA059956','-1','Code Combinations Table For Item Category','Y','','','','','',' ',' ',' ',' ',' ');
--Inserting Object Component Joins for EIS_XXWC_INV_AGED_ITEM_PO_V
xxeis.eis_rsc_ins.vcj( 'EIS_XXWC_INV_AGED_ITEM_PO_V','MTL_SYSTEM_ITEMS','MSI',401,'EXIAIPV.MSI_INVENTORY_ITEM_ID','=','MSI.INVENTORY_ITEM_ID(+)','','','','Y','SA059956');
xxeis.eis_rsc_ins.vcj( 'EIS_XXWC_INV_AGED_ITEM_PO_V','MTL_SYSTEM_ITEMS','MSI',401,'EXIAIPV.MSI_ORGANIZATION_ID','=','MSI.ORGANIZATION_ID(+)','','','','Y','SA059956');
xxeis.eis_rsc_ins.vcj( 'EIS_XXWC_INV_AGED_ITEM_PO_V','MTL_CATEGORIES_B','MC',401,'EXIAIPV.MC_CATEGORY_ID','=','MC.CATEGORY_ID(+)','','','','Y','SA059956');
ELSE
RAISE_APPLICATION_ERROR (-20001,'Module does not exist. Import/Create the module with id 401');
END IF;
END;
/
prompt Creating Report LOV Data for Aged Items with PO Number Report - WC
set scan off define off
DECLARE
mod_exist VARCHAR2(1);
BEGIN 
mod_exist := XXEIS.EIS_RSC_INSERT.check_if_module_exist(401);
IF mod_exist = 'Y' THEN 
--Inserting Report LOVs - Aged Items with PO Number Report - WC
xxeis.eis_rsc_ins.lov( 401,'select distinct ATTRIBUTE9 Region from mtl_parameters','','Region Lov','','ANONYMOUS',NULL,'N','','','','','','','US');
xxeis.eis_rsc_ins.lov( 401,'select distinct organization_code
from apps.mtl_parameters','','XXWC Organization Code','Organization Code from the mtl_parameters table','ID020048',NULL,'N','','','','','','','US');
xxeis.eis_rsc_ins.lov( 401,'select distinct ATTRIBUTE8 District from mtl_parameters','','District Lov','District Lov','ANONYMOUS',NULL,'N','','','','','','','US');
xxeis.eis_rsc_ins.lov( '','select distinct vendor_name,segment1 vendor_number from po_vendors order by 1','','EIS XXWC Vendor Name LOV','','SA059956',NULL,'N','','','N','N','','','US');
xxeis.eis_rsc_ins.lov( '','select distinct segment1 vendor_number,vendor_name from po_vendors order by lpad(segment1,10)','','EIS XXWC Vendor Num LOV','','SA059956',NULL,'N','','','N','N','','','US');
xxeis.eis_rsc_ins.lov( '','select distinct msi.segment1 part_number from apps.mtl_system_items_b msi order by 1','','XXWC ITEM NAME LOV','','SA059956',NULL,'N','','','N','N','','','US');
null;
ELSE
RAISE_APPLICATION_ERROR (-20001,'Module does not exist. Import/Create the module with id 401');
END IF;
END;
/
prompt Creating Report Data for Aged Items with PO Number Report - WC
set scan off define off
DECLARE
mod_exist varchar2(1);
BEGIN 
mod_exist := XXEIS.EIS_RSC_INSERT.check_if_module_exist(401);
IF mod_exist = 'Y' THEN 
--Deleting Report data - Aged Items with PO Number Report - WC
xxeis.eis_rsc_utility.delete_report_rows( 'Aged Items with PO Number Report - WC',401 );
--Inserting Report - Aged Items with PO Number Report - WC
xxeis.eis_rsc_ins.r( 401,'Aged Items with PO Number Report - WC','','Aged Items with PO Number Report - WC','','','','SA059956','EIS_XXWC_INV_AGED_ITEM_PO_V','Y','','','SA059956','','N','White Cap Reports','','CSV,EXCEL,','N','','','','','','N','','US','','','','','','','','','','','','','','','','');
--Inserting Report Columns - Aged Items with PO Number Report - WC
xxeis.eis_rsc_ins.rc( 'Aged Items with PO Number Report - WC',401,'AVG_COST','Avg Cost','Avg Cost','','~T~D~2','default','','22','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_INV_AGED_ITEM_PO_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'Aged Items with PO Number Report - WC',401,'BRANCH_NUMBER','Branch Number','Branch Number','','','default','','3','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_INV_AGED_ITEM_PO_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'Aged Items with PO Number Report - WC',401,'CAT_CLASS','Cat Class','Cat Class','','','default','','9','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_INV_AGED_ITEM_PO_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'Aged Items with PO Number Report - WC',401,'CATCLASS_DESC','Cat Class Description','Catclass Desc','','','default','','10','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_INV_AGED_ITEM_PO_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'Aged Items with PO Number Report - WC',401,'CATEGORY_NAME','Category','Category Name','','','default','','7','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_INV_AGED_ITEM_PO_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'Aged Items with PO Number Report - WC',401,'DISTRICT','District','District','','','default','','2','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_INV_AGED_ITEM_PO_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'Aged Items with PO Number Report - WC',401,'ORGANIZATION_NAME','Branch Name','Organization Name','','','default','','4','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_INV_AGED_ITEM_PO_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'Aged Items with PO Number Report - WC',401,'PO_BUYER_NAME','PO Buyer','Po Buyer Name','','','default','','13','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_INV_AGED_ITEM_PO_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'Aged Items with PO Number Report - WC',401,'PO_COST','PO Cost','Po Cost','','~T~D~2','default','','17','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_INV_AGED_ITEM_PO_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'Aged Items with PO Number Report - WC',401,'PO_LINE','PO Line','Po Line','','~~~','default','','15','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_INV_AGED_ITEM_PO_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'Aged Items with PO Number Report - WC',401,'PO_NUMBER','PO #','Po Number','','','default','','14','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_INV_AGED_ITEM_PO_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'Aged Items with PO Number Report - WC',401,'REGION','Region','Region','','','default','','1','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_INV_AGED_ITEM_PO_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'Aged Items with PO Number Report - WC',401,'SKU_NUMBER','SKU Number','Sku Number','','','default','','5','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_INV_AGED_ITEM_PO_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'Aged Items with PO Number Report - WC',401,'SUB_CATEGORY','Sub Category','Sub Category','','','default','','8','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_INV_AGED_ITEM_PO_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'Aged Items with PO Number Report - WC',401,'VENDOR_NAME','Vendor Name','Vendor Name','','','default','','12','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_INV_AGED_ITEM_PO_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'Aged Items with PO Number Report - WC',401,'VENDOR_NUMBER','Vendor Number','Vendor Number','','','default','','11','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_INV_AGED_ITEM_PO_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'Aged Items with PO Number Report - WC',401,'SKU_DESCRIPTION','SKU Description','Sku Description','','','default','','6','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_INV_AGED_ITEM_PO_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'Aged Items with PO Number Report - WC',401,'AGE_IN_DAYS','Age In Days','Age In Days','','~~~','default','','24','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_INV_AGED_ITEM_PO_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'Aged Items with PO Number Report - WC',401,'AGED_QUANTITY','Aged Quantity','Aged Quantity','','~~~','default','','21','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_INV_AGED_ITEM_PO_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'Aged Items with PO Number Report - WC',401,'PO_CREATION_DATE','Po Creation Date','Po Creation Date','','','default','','18','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_INV_AGED_ITEM_PO_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'Aged Items with PO Number Report - WC',401,'PO_QUANTITY','Po Quantity','Po Quantity','','~~~','default','','16','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_INV_AGED_ITEM_PO_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'Aged Items with PO Number Report - WC',401,'RECEIVED_DATE','Received Date','Received Date','','','default','','25','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_INV_AGED_ITEM_PO_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'Aged Items with PO Number Report - WC',401,'SHIP_TO_LOCATION','Ship To Location','Ship To Location','','','default','','20','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_INV_AGED_ITEM_PO_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'Aged Items with PO Number Report - WC',401,'UOM','Uom','Uom','','','default','','19','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_INV_AGED_ITEM_PO_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'Aged Items with PO Number Report - WC',401,'Extend_$','Extend $','Extended Cost','NUMBER','~T~D~2','default','','23','Y','Y','','','','','','(nvl(EXIAIPV.AGED_QUANTITY,0)* (EXIAIPV.AVG_COST))','SA059956','N','N','','EIS_XXWC_INV_AGED_ITEM_PO_V','','','','US','','');
--Inserting Report Parameters - Aged Items with PO Number Report - WC
xxeis.eis_rsc_ins.rp( 'Aged Items with PO Number Report - WC',401,'Organization','Branch Number','BRANCH_NUMBER','IN','XXWC Organization Code','','VARCHAR2','Y','Y','1','Y','Y','CONSTANT','SA059956','Y','N','','','','EIS_XXWC_INV_AGED_ITEM_PO_V','','','US','');
xxeis.eis_rsc_ins.rp( 'Aged Items with PO Number Report - WC',401,'District','District','DISTRICT','IN','District Lov','','VARCHAR2','N','Y','7','Y','Y','CONSTANT','SA059956','Y','N','','','','EIS_XXWC_INV_AGED_ITEM_PO_V','','','US','');
xxeis.eis_rsc_ins.rp( 'Aged Items with PO Number Report - WC',401,'Region','Region','REGION','IN','Region Lov','','VARCHAR2','N','Y','6','Y','Y','CONSTANT','SA059956','Y','N','','','','EIS_XXWC_INV_AGED_ITEM_PO_V','','','US','');
xxeis.eis_rsc_ins.rp( 'Aged Items with PO Number Report - WC',401,'SKU Number','Sku Number','SKU_NUMBER','IN','XXWC ITEM NAME LOV','','VARCHAR2','N','Y','5','Y','Y','CONSTANT','SA059956','Y','N','','','','EIS_XXWC_INV_AGED_ITEM_PO_V','','','US','');
xxeis.eis_rsc_ins.rp( 'Aged Items with PO Number Report - WC',401,'Vendor Name','Vendor Name','VENDOR_NAME','IN','EIS XXWC Vendor Name LOV','','VARCHAR2','N','Y','3','Y','Y','CONSTANT','SA059956','Y','N','','','','EIS_XXWC_INV_AGED_ITEM_PO_V','','','US','');
xxeis.eis_rsc_ins.rp( 'Aged Items with PO Number Report - WC',401,'Vendor Num','Vendor Number','VENDOR_NUMBER','IN','EIS XXWC Vendor Num LOV','','VARCHAR2','N','Y','4','Y','Y','CONSTANT','SA059956','Y','N','','','','EIS_XXWC_INV_AGED_ITEM_PO_V','','','US','');
xxeis.eis_rsc_ins.rp( 'Aged Items with PO Number Report - WC',401,'Age In Days','Age In Days','','IN','','365','NUMBER','Y','Y','2','Y','N','CONSTANT','SA059956','Y','N','','','','EIS_XXWC_INV_AGED_ITEM_PO_V','','','US','');
--Inserting Dependent Parameters - Aged Items with PO Number Report - WC
xxeis.eis_rsc_ins.rdp( 'Aged Items with PO Number Report - WC',401,'VENDOR_NUMBER','Vendor Name','Vendor Num','IN','N','');
xxeis.eis_rsc_ins.rdp( 'Aged Items with PO Number Report - WC',401,'VENDOR_NAME','Vendor Num','Vendor Name','IN','N','');
--Inserting Report Conditions - Aged Items with PO Number Report - WC
xxeis.eis_rsc_ins.rcnh( 'Aged Items with PO Number Report - WC',401,'EXIAIPV.BRANCH_NUMBER IN Organization','SIMPLE','','','Y','','1');
xxeis.eis_rsc_ins.rcnd( '','','BRANCH_NUMBER','','Organization','','','','','EIS_XXWC_INV_AGED_ITEM_PO_V','','','','','','IN','Y','Y','','','','','1',401,'Aged Items with PO Number Report - WC','EXIAIPV.BRANCH_NUMBER IN Organization');
xxeis.eis_rsc_ins.rcnh( 'Aged Items with PO Number Report - WC',401,'EXIAIPV.VENDOR_NAME IN Vendor Name','SIMPLE','','','Y','','2');
xxeis.eis_rsc_ins.rcnd( '','','VENDOR_NAME','','Vendor Name','','','','','EIS_XXWC_INV_AGED_ITEM_PO_V','','','','','','IN','Y','Y','','','','','1',401,'Aged Items with PO Number Report - WC','EXIAIPV.VENDOR_NAME IN Vendor Name');
xxeis.eis_rsc_ins.rcnh( 'Aged Items with PO Number Report - WC',401,'EXIAIPV.VENDOR_NUMBER IN Vendor Num','SIMPLE','','','Y','','3');
xxeis.eis_rsc_ins.rcnd( '','','VENDOR_NUMBER','','Vendor Num','','','','','EIS_XXWC_INV_AGED_ITEM_PO_V','','','','','','IN','Y','Y','','','','','1',401,'Aged Items with PO Number Report - WC','EXIAIPV.VENDOR_NUMBER IN Vendor Num');
xxeis.eis_rsc_ins.rcnh( 'Aged Items with PO Number Report - WC',401,'EXIAIPV.SKU_NUMBER IN SKU Number','SIMPLE','','','Y','','4');
xxeis.eis_rsc_ins.rcnd( '','','SKU_NUMBER','','SKU Number','','','','','EIS_XXWC_INV_AGED_ITEM_PO_V','','','','','','IN','Y','Y','','','','','1',401,'Aged Items with PO Number Report - WC','EXIAIPV.SKU_NUMBER IN SKU Number');
xxeis.eis_rsc_ins.rcnh( 'Aged Items with PO Number Report - WC',401,'EXIAIPV.REGION IN Region','SIMPLE','','','Y','','5');
xxeis.eis_rsc_ins.rcnd( '','','REGION','','Region','','','','','EIS_XXWC_INV_AGED_ITEM_PO_V','','','','','','IN','Y','Y','','','','','1',401,'Aged Items with PO Number Report - WC','EXIAIPV.REGION IN Region');
xxeis.eis_rsc_ins.rcnh( 'Aged Items with PO Number Report - WC',401,'EXIAIPV.DISTRICT IN District','SIMPLE','','','Y','','6');
xxeis.eis_rsc_ins.rcnd( '','','DISTRICT','','District','','','','','EIS_XXWC_INV_AGED_ITEM_PO_V','','','','','','IN','Y','Y','','','','','1',401,'Aged Items with PO Number Report - WC','EXIAIPV.DISTRICT IN District');
xxeis.eis_rsc_ins.rcnh( 'Aged Items with PO Number Report - WC',401,'FreeText','FREE_TEXT','','','Y','','7');
xxeis.eis_rsc_ins.rcnd( '','','','','','','','','','','','','','','','','Y','Y','','','','and EXIAIPV.RECEIVED_DATE < sysdate - :Age In Days','1',401,'Aged Items with PO Number Report - WC','FreeText');
--Inserting Report Sorts - Aged Items with PO Number Report - WC
xxeis.eis_rsc_ins.rs( 'Aged Items with PO Number Report - WC',401,'','ASC','SA059956','2','');
xxeis.eis_rsc_ins.rs( 'Aged Items with PO Number Report - WC',401,'PO_NUMBER','ASC','SA059956','3','');
xxeis.eis_rsc_ins.rs( 'Aged Items with PO Number Report - WC',401,'PO_LINE','ASC','SA059956','4','');
xxeis.eis_rsc_ins.rs( 'Aged Items with PO Number Report - WC',401,'BRANCH_NUMBER','ASC','SA059956','1','');
--Inserting Report Triggers - Aged Items with PO Number Report - WC
--inserting report templates - Aged Items with PO Number Report - WC
--Inserting Report Portals - Aged Items with PO Number Report - WC
--inserting report dashboards - Aged Items with PO Number Report - WC
--Exporting report views table information -  
xxeis.eis_rsc_ins.rviews( 'Aged Items with PO Number Report - WC','401','EIS_XXWC_INV_AGED_ITEM_PO_V','EIS_XXWC_INV_AGED_ITEM_PO_V','N','');
--inserting report security - Aged Items with PO Number Report - WC
xxeis.eis_rsc_ins.rsec( 'Aged Items with PO Number Report - WC','','SS084202','',401,'SA059956','','N','');
xxeis.eis_rsc_ins.rsec( 'Aged Items with PO Number Report - WC','401','','XXWC_INVENTORY_SUPER_USER',401,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'Aged Items with PO Number Report - WC','401','','XXWC_INVENTORY_SPEC_SCC',401,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'Aged Items with PO Number Report - WC','201','','XXWC_SALES_PURCHASING_MGR',401,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'Aged Items with PO Number Report - WC','201','','XXWC_PUR_SUPER_USER',401,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'Aged Items with PO Number Report - WC','201','','HDS_PRCHSNG_SPR_USR',401,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'Aged Items with PO Number Report - WC','201','','XXWC_PURCHASING_SR_MRG_WC',401,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'Aged Items with PO Number Report - WC','201','','XXWC_PURCHASING_RPM',401,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'Aged Items with PO Number Report - WC','201','','XXWC_PURCHASING_MGR',401,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'Aged Items with PO Number Report - WC','201','','XXWC_PURCHASING_INQUIRY',401,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'Aged Items with PO Number Report - WC','201','','XXWC_PURCHASING_BUYER',401,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'Aged Items with PO Number Report - WC','401','','XXWC_INV_PLANNER',401,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'Aged Items with PO Number Report - WC','401','','XXWC_INVENTORY_CONTROL_INQUIRY',401,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'Aged Items with PO Number Report - WC','401','','XXWC_INVENTORY_CONTROL_SR_MGR',401,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'Aged Items with PO Number Report - WC','401','','XXWC_INV_ACCOUNTANT',401,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'Aged Items with PO Number Report - WC','','MA058569','',401,'SA059956','','N','');
xxeis.eis_rsc_ins.rsec( 'Aged Items with PO Number Report - WC','','JS064388','',401,'SA059956','','N','');
xxeis.eis_rsc_ins.rsec( 'Aged Items with PO Number Report - WC','','PP018915','',401,'SA059956','','N','');
--Inserting Report Pivots - Aged Items with PO Number Report - WC
--Inserting Report Distribution Details 
--Inserting Report Distribution Details 
--Inserting Report SubTotals Details 
--Inserting Report aggregations 
--Inserting Report   Version details- Aged Items with PO Number Report - WC
xxeis.eis_rsc_ins.rv( 'Aged Items with PO Number Report - WC','','Aged Items with PO Number Report - WC','SA059956','27-FEB-2018');
ELSE
RAISE_APPLICATION_ERROR (-20001,'Module does not exist. Import/Create the module with id 401');
END IF;
END;
/
