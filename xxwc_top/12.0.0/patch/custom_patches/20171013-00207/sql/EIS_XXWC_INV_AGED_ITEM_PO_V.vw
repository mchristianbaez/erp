-------------------------------------------------------------------------------------------
/*****************************************************************************************
  $Header XXEIS.EIS_XXWC_INV_AGED_ITEM_PO_V.vw $
  Module Name: Inventory
  PURPOSE: Aged Items with PO Number Report - WC
  REVISIONS:
  Ver          Date        Author                Description
  ---------  ----------  ------------------    ----------------
  1.0		 7/11/2017      Siva				TMS#20171013-00207 
******************************************************************************************/
---------------------------------------------------------------------------------------------
CREATE OR REPLACE VIEW XXEIS.EIS_XXWC_INV_AGED_ITEM_PO_V (SKU_NUMBER, SKU_DESCRIPTION, REGION, DISTRICT, BRANCH_NUMBER, ORGANIZATION_NAME, CATEGORY_NAME, SUB_CATEGORY, CAT_CLASS, CATCLASS_DESC, VENDOR_NUMBER, VENDOR_NAME, PO_NUMBER, PO_LINE, PO_COST, AVG_COST, PO_BUYER_NAME, PO_CREATION_DATE, AGED_QUANTITY, UOM, AGE_IN_DAYS, RECEIVED_DATE, SHIP_TO_LOCATION, PO_QUANTITY, MSI_INVENTORY_ITEM_ID, MSI_ORGANIZATION_ID, MC_CATEGORY_ID, TRANSACTION_ID)
AS
  SELECT msi.segment1 sku_number,
    msit.description sku_description,
    mp.attribute9 region,
    mp.attribute8 district,
    mp.organization_code branch_number,
    ood.name organization_name,
    mc.segment1 category_name,
    mc.attribute6 sub_category ,
    mc.segment2 cat_class ,
    mc.segment1
    ||'.'
    ||mc.segment2
    ||' '
    ||SUBSTR(mct.description, instr(mct.description,'.')+1) catclass_desc,
    pv.segment1 vendor_number,
    pv.vendor_name,
    phv.segment1 po_number,
    plv.line_num po_line,
    plv.unit_price po_cost,
    NVL ( APPS.CST_COST_API.GET_ITEM_COST (1, MSI.INVENTORY_ITEM_ID, MSI.ORGANIZATION_ID), 0) AVG_COST,
    (SELECT PAPF.full_name AGENT_NAME
    FROM PO_AGENTS POAV,
      PER_ALL_PEOPLE_F PAPF
    WHERE POAV.AGENT_ID = PHV.AGENT_ID
    AND TRUNC(sysdate) BETWEEN PAPF.EFFECTIVE_START_DATE AND PAPF.EFFECTIVE_END_DATE
    AND rownum=1
    ) PO_BUYER_NAME,
    TRUNC(phv.creation_date) po_creation_date,
    moqd.transaction_quantity aged_quantity,
    moqd.transaction_uom_code uom,
    ROUND(sysdate-moqd.orig_date_received) age_in_days,
    TRUNC(moqd.orig_date_received) received_date,
    hrl1.location_code ship_to_location,
    plv.quantity po_quantity,
    --Primary Keys
    msi.inventory_item_id msi_inventory_item_id ,
    msi.organization_id msi_organization_id,
    mc.category_id mc_category_id,
    mmt.transaction_id
    --descr#flexfield#start
    --descr#flexfield#end
  FROM mtl_onhand_quantities_detail moqd,
    mtl_material_transactions mmt,
    mtl_system_items_b msi,
    apps.mtl_system_items_tl msit,
    RCV_TRANSACTIONS RT,
    apps.po_headers_all phv,
    hr_locations_all_tl hrl1,
    APPS.AP_SUPPLIERS PV,
    PO_LINES_ALL PLV,
    po_line_locations_all pll,
    apps.mtl_item_categories micat,
    apps.mtl_categories_b mc,
    apps.mtl_categories_tl mct,
    apps.mtl_parameters mp,
    apps.hr_all_organization_units ood
  WHERE 1                       =1
  AND moqd.inventory_item_id    =mmt.inventory_item_id
  AND moqd.organization_id      =mmt.organization_id
  AND moqd.create_transaction_id=mmt.transaction_id
  AND moqd.inventory_item_id    =msi.inventory_item_id
  AND moqd.organization_id      =msi.organization_id
  AND mmt.transaction_type_id   =18
  AND mmt.rcv_transaction_id    = rt.transaction_id
  AND rt.po_header_id           = phv.po_header_id
  AND rt.po_line_id             = plv.po_line_id
  AND RT.PO_LINE_LOCATION_ID    = PLL.LINE_LOCATION_ID
  AND phv.org_id                =162
  AND phv.vendor_id             = pv.vendor_id(+)
  AND phv.ship_to_location_id   = hrl1.location_id (+)
  AND hrl1.language(+)          = userenv('LANG')
  AND msi.inventory_item_id     = plv.item_id
  AND msi.organization_id       = pll.ship_to_organization_id
    --  AND moqd.orig_date_received   < sysdate-365
  AND msi.inventory_item_id    = msit.inventory_item_id(+)
  AND msi.organization_id      = msit.organization_id(+)
  AND msit.language (+)        = userenv('LANG')
  AND msi.inventory_item_id    = micat.inventory_item_id(+)
  AND msi.organization_id      = micat.organization_id(+)
  AND micat.category_set_id(+) = 1100000062
  AND micat.category_id        = mc.category_id(+)
  AND mc.category_id           = mct.category_id(+)
  AND mct.language (+)         = userenv('LANG')
  AND msi.organization_id      = mp.organization_id
  AND MP.ORGANIZATION_ID       = OOD.ORGANIZATION_ID
  AND MSI.ITEM_TYPE       <> 'SPECIAL'
  AND MSI.SEGMENT1 NOT LIKE 'R%'
  UNION ALL
  SELECT msi.segment1 sku_number,
    msit.description sku_description,
    mp.attribute9 region,
    mp.attribute8 district,
    mp.organization_code branch_number,
    ood.name organization_name,
    mc.segment1 category_name,
    mc.attribute6 sub_category ,
    mc.segment2 cat_class ,
    mc.segment1
    ||'.'
    ||mc.segment2
    ||' '
    ||SUBSTR(mct.description, instr(mct.description,'.')+1) catclass_desc,
    xida.vendor_num vendor_number,
    xida.vendor_name,
    NULL po_number,
    to_number(NULL) po_line,
    to_number(NULL) po_cost,
    NVL ( APPS.CST_COST_API.GET_ITEM_COST (1, MSI.INVENTORY_ITEM_ID, MSI.ORGANIZATION_ID), 0) AVG_COST,
    xida.buyer PO_BUYER_NAME,
    to_date(NULL) po_creation_date,
    moqd.transaction_quantity aged_quantity,
    moqd.transaction_uom_code uom,
    ROUND(sysdate-moqd.orig_date_received) age_in_days,
    TRUNC(moqd.orig_date_received) received_date,
    NULL ship_to_location,
    to_number(NULL) po_quantity,
    --Primary Keys
    msi.inventory_item_id msi_inventory_item_id ,
    msi.organization_id msi_organization_id,
    mc.category_id mc_category_id,
    mmt.transaction_id
    --descr#flexfield#start
    --descr#flexfield#end
  FROM mtl_onhand_quantities_detail moqd,
    mtl_material_transactions mmt,
    mtl_system_items_b msi,
    apps.mtl_system_items_tl msit,
    xxwc.XXWC_ISR_DETAILS_ALL xida,
    apps.mtl_item_categories micat,
    apps.mtl_categories_b mc,
    apps.mtl_categories_tl mct,
    apps.mtl_parameters mp,
    apps.hr_all_organization_units ood
  WHERE 1                       =1
  AND moqd.inventory_item_id    =mmt.inventory_item_id
  AND moqd.organization_id      =mmt.organization_id
  AND moqd.create_transaction_id=mmt.transaction_id
  AND moqd.inventory_item_id    =msi.inventory_item_id
  AND moqd.organization_id      =msi.organization_id
  AND msi.inventory_item_id     =xida.inventory_item_id
  AND msi.organization_id       =xida.organization_id
    --AND moqd.orig_date_received   < sysdate-365
  AND msi.inventory_item_id    = msit.inventory_item_id(+)
  AND msi.organization_id      = msit.organization_id(+)
  AND msit.language (+)        = userenv('LANG')
  AND msi.inventory_item_id    = micat.inventory_item_id(+)
  AND msi.organization_id      = micat.organization_id(+)
  AND micat.category_set_id(+) = 1100000062
  AND micat.category_id        = mc.category_id(+)
  AND mc.category_id           = mct.category_id(+)
  AND mct.language (+)         = userenv('LANG')
  AND msi.organization_id      = mp.organization_id
  AND MP.ORGANIZATION_ID       = OOD.ORGANIZATION_ID
  AND MSI.ITEM_TYPE       <> 'SPECIAL'
  AND MSI.SEGMENT1 NOT LIKE 'R%'
  AND NOT EXISTS
    (SELECT 1
    FROM RCV_TRANSACTIONS RT
    WHERE rt.transaction_id     = mmt.rcv_transaction_id
    AND mmt.transaction_type_id =18
    )
/
  