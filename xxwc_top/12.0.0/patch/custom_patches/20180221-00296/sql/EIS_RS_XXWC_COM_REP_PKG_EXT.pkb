CREATE OR REPLACE PACKAGE BODY XXEIS.EIS_RS_XXWC_COM_REP_PKG_EXT
  --//============================================================================
  --//
  --// Reference Object Name    :: xxeis.eis_rs_xxwc_com_rep_pkg
  --// Change Request 			:: Performance Issue
  --// Object Name         		:: xxeis.eis_rs_xxwc_com_rep_pkg_ext
  --//
  --// Object Type         		:: Package Body
  --//
  --// Object Description       :: This package will trigger at before report level and insert the data into a temp table.
  --//
  --// Version Control
  --//============================================================================
  --// Vers    Author           Date          Description
  --//----------------------------------------------------------------------------
  --// 1.0     siva  			04/12/2016   Initial Build --TMS#20160412-00032	--Performance tuning
  --// 1.1     siva  			09/12/2016  		20160831-00069 
  --// 1.2     Siva  			27-Oct-2016        TMS#20161024-00080
  --// 1.3     Siva				24-Oct-2017        TMS#20171023-00217
  --// 1.4	   Siva  			22-Feb-2018		   TMS#20180221-00296 
  --//============================================================================
AS
  g_dflt_email VARCHAR2 (50) := 'HDSOracleDevelopers@hdsupply.com'; 
PROCEDURE comm_rep_par(
    p_period_name    IN VARCHAR2 ,
    p_operating_unit IN VARCHAR2 ,
    p_invoice_source IN VARCHAR2)
IS
  --//============================================================================
  --//
  --// Object Name         :: comm_rep_par
  --//
  --// Object Type         :: Procedure
  --//
  --// Object Description  :: This procedure will insert the values into a temp table
  --//
  --// Version Control
  --//============================================================================
  --// Vers    Author           Date          Description
  --//----------------------------------------------------------------------------
  --// 1.0     siva  	04/12/2016   Initial Build --TMS#20160412-00032	--Performance tuning
  --// 1.1     siva  			09/12/2016  		20160831-00069 
  --// 1.2     Siva  			27-Oct-2016        TMS#20161024-00080
  --// 1.3     Siva				24-Oct-2017        TMS#20171023-00217 
  --//============================================================================
        l_err_msg         VARCHAR2 (3000);
        L_Err_Callfrom    Varchar2 (50);
        L_Err_Callpoint   Varchar2 (50);
        lc_where_cond VARCHAR2 (10000);
        L_Cnt Number :=0;
        
  L_COM_REP_CSR SYS_REFCURSOR;
  
TYPE l_com_sub_rec_type
IS
  RECORD
  (
  CUSTOMER_TRX_ID  XXEIS.XXWC_EOM_COMM_REP_TBL.CUSTOMER_TRX_ID%TYPE,
  ORG_ID XXEIS.XXWC_EOM_COMM_REP_TBL.ORG_ID%TYPE,
  BILL_TO_CUSTOMER_ID NUMBER ,
  bill_to_site_use_id number,
  INVOICENUMBER XXEIS.XXWC_EOM_COMM_REP_TBL.INVOICENUMBER%TYPE ,
  BUSINESSDATE XXEIS.XXWC_EOM_COMM_REP_TBL.BUSINESSDATE%TYPE ,
  OPERATING_UNIT xxeis.XXWC_EOM_COMM_REP_TBL.OPERATING_UNIT%type ,
  INVOICE_SOURCE XXEIS.XXWC_EOM_COMM_REP_TBL.INVOICE_SOURCE%TYPE ,
  PERIOD_NAME XXEIS.XXWC_EOM_COMM_REP_TBL.PERIOD_NAME%TYPE  
  );
  
  TYPE L_COM_SUB_TYPE_TBL IS
  table of l_com_sub_rec_type index by BINARY_INTEGER;
 L_COM_SUB_REC_TBL  L_COM_SUB_TYPE_TBL;
 l_com_sub_csr SYS_REFCURSOR;
 
 -- Identify the driving tables and Filtering the data based on the parameters 
 L_COM_SUB_QRY VARCHAR2(32000):='SELECT 
  CT.CUSTOMER_TRX_ID,
  HOU.ORGANIZATION_ID ORG_ID ,
  CT.BILL_TO_CUSTOMER_ID ,
  ct.bill_to_site_use_id,
  CT.TRX_NUMBER INVOICENUMBER ,
  GD.GL_DATE BUSINESSDATE ,
  HOU.NAME OPERATING_UNIT ,
  BS.NAME INVOICE_SOURCE ,
  GP.PERIOD_NAME PERIOD_NAME
FROM RA_CUSTOMER_TRX CT ,
  RA_CUST_TRX_LINE_GL_DIST GD ,
  HR_OPERATING_UNITS HOU,
  ra_batch_sources bs ,
  GL_LEDGERS GL ,
  GL_PERIODS GP
WHERE 1                =1
AND GD.CUSTOMER_TRX_ID = CT.CUSTOMER_TRX_ID
AND GD.ACCOUNT_CLASS   = ''REC''
AND CT.ORG_ID          = HOU.ORGANIZATION_ID
and GD.ORG_ID          = HOU.ORGANIZATION_ID
AND CT.BATCH_SOURCE_ID = BS.BATCH_SOURCE_ID
AND ct.org_id          = bs.org_id
AND GD.SET_OF_BOOKS_ID = GL.LEDGER_ID
AND GL.PERIOD_SET_NAME = GP.PERIOD_SET_NAME
AND GL.PERIOD_SET_NAME = ''4-4-QTR''
AND TRUNC(GD.GL_DATE) BETWEEN TRUNC (GP.START_DATE) AND TRUNC (GP.END_DATE)
' ;

  
  -- Process the driving data to below query
  type l_com_eom_rep_type_tbl
is
  table of xxeis.XXWC_EOM_COMM_REP_TBL%rowtype index by BINARY_INTEGER;
  L_COM_EOM_REC_TBL l_com_eom_rep_type_tbl;
  
  L_COM_HDR_QRY VARCHAR2(32000):=
  ' SELECT :businessdate               
,CASE                   
WHEN msi.segment1 = ''CONTBILL''                   
THEN                      
''FabRebar''                   
WHEN (mic.category_concat_segs IN (''99.99E1''                                                     
,''99.99E2''                                                     
,''99.99F1''                                                     
,''99.99F2''                                                     
,''99.99RR''                                                     
,''99.99RT''                                                     
,''99.99S1''                                                     
,''99.99S2''                                                     
,''99.99SP''                                                     
,''99.99T1''                                                     
,''99.99T2''))                  
THEN                      
''Rental''                   
WHEN mic.segment1 IN (''BB''                                        
,''BS''                                        
,''DC''                                        
,''EC''                                        
,''EP''                                        
,''FA''                                        
,''FC''                                        
,''JS''                                        
,''ME''                                        
,''ML''                                        
,''MR''                                        
,''NS''                                        
,''PS''                                        
,''RE''                                        
,''RL''                                        
,''RP''                                        
,''SA''                                        
,''SC''                                        
,''SH''                                        
,''TH'')                   
THEN                      
''Intangibles''
WHEN msi.segment1 IN(''POSTPAINTCHG'',''BEAMPAINTCHG'')   --added for version 1.1
THEN ''Intangibles''     --added for version 1.1                
WHEN mc.segment1 IN (''GC''                                       
,''MD''                                       
,''PC''                                       
,''PD''                                       
,''PR''                                       
,''RR''                                       
,''RV''                                       
,''ST''                                       
,''XX'')                   
THEN                      
''Non Comm.''                   
WHEN    (mic.category_concat_segs IN (''FB.OSPI''                                                        
,''FB.MODL''                                                        
,''FB.SUBA''                                                        
,''FB.OPTC''                                                        
,''RF.OSPI''                                                        
,''RF.SERV''))                        
OR mic.category_concat_segs IN (''60.6021'', ''60.6022'', ''60.6035'',''60.6007'')  --added for version 1.4                
THEN                      
''FabRebar''                   
WHEN ( (ol.attribute3 = ''Y'') OR (ol.attribute15 = ''Y''))                   
THEN                      
''Blow and Expired''                   
ELSE                      
''Product''                
END                   
report_type               
,oh.order_number order_number               
,:invoicenumber               
,ctl.line_number lineno               
,DECODE (                   
msi.segment1                  
,''Rental Charge'', DECODE (                                       
ol.line_category_code                                      
,''RETURN'', xxeis.eis_rs_xxwc_com_util_pkg.get_rental_item_desc (                                                    
ol.reference_line_id)                                      
,xxeis.eis_rs_xxwc_com_util_pkg.get_rental_item_desc (                                          
ol.link_to_line_id))                  
,ctl.description)                   
description               
,NVL (ctl.quantity_invoiced, ctl.quantity_credited) qty               
,NVL (ctl.unit_selling_price, 0) unitprice               
,NVL (                   
ctl.extended_amount                  
,DECODE (                      
NVL (ctl.unit_selling_price, 0)                     
,0, 0                     
, (  NVL (ctl.quantity_invoiced, ctl.quantity_credited)                        
* (  NVL (ctl.unit_selling_price, 0)                           
+ xxeis.eis_rs_xxwc_com_util_pkg.get_mfgadjust (ol.header_id, ol.line_id)))))                   
extsale               
,hca.account_number masternumber               
,hca.account_name mastername               
,hps.party_site_number jobnumber               
,hcsu.location customername               
,jrs.salesrep_number salesrepnumber               
,NVL (jrse.source_name, jrse.resource_name) salesrepname               
,mp.organization_code loc               
,mp.organization_id mp_organization_id               
,mp.attribute9 regionname               
,DECODE (                   
msi.segment1                  
,''Rental Charge'', DECODE (                                       
ol.line_category_code                                      
,''RETURN'', xxeis.eis_rs_xxwc_com_util_pkg.get_rental_item (                                                    
ol.reference_line_id)                                      
,xxeis.eis_rs_xxwc_com_util_pkg.get_rental_item (ol.link_to_line_id))                  
,msi.segment1)                   
partno               
,mc.segment2 catclass               
,:invoice_source               
,NVL (ol.shipped_quantity, 0) shipped_quantity               
,DECODE (ol.source_type_code,  ''INTERNAL'', 0,  ''EXTERNAL'', 1,  ol.source_type_code) directflag               
,CASE                   
WHEN ol.line_type_id = 1008                   
THEN                      
NVL (apps.xxwc_mv_routines_pkg.get_order_line_cost (ol.reference_line_id), 0)                   
WHEN ol.line_type_id = 1003                   
THEN                      
NVL (                         
xxeis.eis_rs_xxwc_com_util_pkg.get_bill_line_cost (oh.header_id                                                                           
,ol.inventory_item_id)                        
,0)                   
ELSE                      
NVL (apps.xxwc_mv_routines_pkg.get_order_line_cost (ol.line_id), 0)                
END                   
averagecost               
,DECODE (xxeis.eis_rs_xxwc_com_util_pkg.get_trader (ol.header_id, ol.line_id)                       
,0, ''-''                       
,''Trader'')                   
trader               
,NVL (                   
ROUND (                      
apps.xxwc_mv_routines_pkg.get_vendor_quote_cost(ol.line_id)-- modified by sowmya on 31march2015 for tms task 20150220-00115                     
,9)-- modified by sowmya on 31march2015 for tms task 20150220-00115                  
,0)                   
special_cost               
,:operating_unit               
,msi.list_price_per_unit list_price               
,:org_id               
,msi.inventory_item_id               
,msi.organization_id               
,:customer_trx_id               
,hp.party_id               
,:period_name               
,xxeis.eis_rs_xxwc_com_util_pkg.get_mfgadjust (ol.header_id, ol.line_id) mfgadjust               
,DECODE (jrse.category,  ''EMPLOYEE'', ''Employee'',  ''OTHER'', ''House Acct'',  ''House Acct'')                   
salesrep_type               
,jrse.category sales_type               
,ctl.interface_line_attribute2               
--,fu.description insidesalesrep --commented for version 1.3
,OH.attribute7 insidesalesrep   --Added for version 1.3           
,fu.employee_id insidesrnum               
,srep.prism_num prismsrnum               
,ott.name linetype               
,NVL ( (SELECT emp.full_name                         
FROM apps.per_all_people_f emp, apps.fnd_user fu                        
WHERE ol.created_by = fu.user_id AND ROWNUM = 1 AND fu.employee_id = emp.person_id)                    
,'''')                   
hrinsidesrname               
,NVL ( (SELECT emp.employee_number                         
FROM apps.per_all_people_f emp, apps.fnd_user fu                        
WHERE ol.created_by = fu.user_id AND ROWNUM = 1 AND fu.employee_id = emp.person_id)                    
,'''')                   
hrinsidesrnum
,ol.user_item_description  --added for version 1.2           
FROM
  RA_CUSTOMER_TRX_LINES CTL ,
  hz_cust_accounts hca ,
  hz_parties hp , 
  hz_party_sites hps ,
  hz_cust_acct_sites hcas ,
  hz_cust_site_uses hcsu ,
  OE_ORDER_LINES OL ,
  OE_ORDER_HEADERS OH ,
  JTF_RS_SALESREPS JRS ,
  jtf_rs_resource_extns_vl jrse ,
  MTL_SYSTEM_ITEMS_KFV MSI ,
  mtl_parameters mp ,
  MTL_ITEM_CATEGORIES_V MIC ,
  mtl_categories_b_kfv mc ,
  mtl_category_sets mdcs ,
  oe_transaction_types_vl oel ,
  oe_transaction_types_vl oeh ,
  fnd_user fu ,
  xxeis.eis_xxwc_ar_prism_salesrep srep ,
  oe_transaction_types_tl ott
WHERE 1                        = 1
AND srep.employee_num(+)       = jrs.salesrep_number
AND OL.LINE_TYPE_ID            = OTT.TRANSACTION_TYPE_ID
AND CTL.CUSTOMER_TRX_ID = :cust_trx_id
AND hca.cust_account_id = :bill_to_cust_id
--AND ctl.customer_trx_id        = ct.customer_trx_id
--AND ct.bill_to_customer_id     = hca.cust_account_id
AND hca.party_id               = hp.party_id(+)
AND hp.party_id                = hps.party_id
AND hcas.party_site_id         = hps.party_site_id
AND hcsu.cust_acct_site_id     = hcas.cust_acct_site_id
--AND hcsu.site_use_id           = ct.bill_to_site_use_id
AND hcsu.site_use_id = :bill_to_site_id
AND HCA.CUST_ACCOUNT_ID        = HCAS.CUST_ACCOUNT_ID
AND HCA.ACCOUNT_NUMBER        <> ''70200000''
AND ol.salesrep_id             = jrs.salesrep_id
AND oh.org_id                  = jrs.org_id
AND jrs.resource_id            = jrse.resource_id
AND CTL.INTERFACE_LINE_CONTEXT = ''ORDER ENTRY''
AND TO_CHAR (oh.order_number)  = ctl.interface_line_attribute1
AND TO_CHAR (ol.line_id)       = ctl.interface_line_attribute6
AND oh.header_id               = ol.header_id
AND msi.inventory_item_id      = ol.inventory_item_id
AND msi.organization_id        = ol.ship_from_org_id
AND mic.category_id            = mc.category_id
AND msi.organization_id        = mic.organization_id
AND msi.inventory_item_id      = mic.inventory_item_id
AND msi.organization_id        = mp.organization_id
AND mic.category_set_id        = mdcs.category_set_id
AND ctl.extended_amount       <> 0
AND mdcs.category_set_name     = ''Inventory Category''
AND mc.structure_id            = 101
AND oel.transaction_type_id    = ol.line_type_id
AND oeh.transaction_type_id    = oh.order_type_id
AND oeh.org_id                 = oh.org_id
AND oel.org_id                 = ol.org_id
--AND oh.header_id               = ol.header_id
AND ol.created_by             = fu.user_id'
;

BEGIN
--Dbms_Output.Put_Line('sart time '||To_Char(Sysdate,'DD-MON-YY HH24:MI:SS'));

      L_ERR_CALLFROM := 'eis_rs_xxwc_com_rep_pkg_ext.comm_rep_par';
      l_err_callpoint := 'comm rep par';

  IF p_operating_unit      IS NOT NULL THEN
    lc_where_cond:= lc_where_cond||' and hou.name in ('||xxeis.eis_rs_utility.get_param_values(p_operating_unit)||' )';
  END IF;

  IF p_period_name      IS NOT NULL THEN
    lc_where_cond:= lc_where_cond||' and gp.period_name in ('||xxeis.eis_rs_utility.get_param_values(p_period_name)||' )';
  END IF;

  IF p_invoice_source      IS NOT NULL THEN
    lc_where_cond:= lc_where_cond||' and bs.name in ('||Xxeis.Eis_Rs_Utility.Get_Param_Values(P_Invoice_Source)||' )';
  END IF;


  L_COM_SUB_REC_TBL.delete;
  L_COM_EOM_REC_TBL.DELETE;
  
  
  
    L_COM_SUB_QRY := L_COM_SUB_QRY||' '||lc_where_cond;
  
  Fnd_File.Put_Line(Fnd_File.Log,'P_OPERATING_UNIT'||P_OPERATING_UNIT);
  Fnd_File.Put_Line(Fnd_File.Log,'P_PERIOD_NAME'||P_PERIOD_NAME);
  FND_FILE.PUT_LINE(FND_FILE.LOG,'P_INVOICE_SOURCE'||P_INVOICE_SOURCE);
  
  OPEN L_COM_SUB_CSR FOR L_COM_SUB_QRY;
    FETCH L_COM_SUB_CSR BULK COLLECT INTO L_COM_SUB_REC_TBL;
  CLOSE L_COM_SUB_CSR;
  

  IF L_COM_SUB_REC_TBL.COUNT>0
  THEN
  FOR I IN 1..L_COM_SUB_REC_TBL.COUNT
  LOOP
   OPEN L_COM_REP_CSR FOR L_COM_HDR_QRY USING 
  L_COM_SUB_REC_TBL(i).BUSINESSDATE,
  L_COM_SUB_REC_TBL(I).INVOICENUMBER,
  L_COM_SUB_REC_TBL(I).INVOICE_SOURCE,
  L_COM_SUB_REC_TBL(I).OPERATING_UNIT,
  L_COM_SUB_REC_TBL(I).ORG_ID,
  L_COM_SUB_REC_TBL(i).CUSTOMER_TRX_ID,
  L_COM_SUB_REC_TBL(I).PERIOD_NAME,  
  L_COM_SUB_REC_TBL(i).CUSTOMER_TRX_ID,
  L_COM_SUB_REC_TBL(i).BILL_TO_CUSTOMER_ID,
  L_COM_SUB_REC_TBL(i).bill_to_site_use_id ;
  loop
  FETCH l_com_rep_csr BULK COLLECT INTO L_COM_EOM_REC_TBL limit 10000;
 
  
 --FND_FILE.PUT_LINE(FND_FILE.LOG,'L_COM_EOM_REC_TBL'||L_Com_Eom_Rec_Tbl.Count);

   
  if L_COM_EOM_REC_TBL.COUNT>0
  Then  
    
    
 
    FORALL J IN 1..L_COM_EOM_REC_TBL.COUNT 
  Insert Into Xxeis.Xxwc_Eom_Comm_Rep_Tbl Values L_Com_Eom_Rec_Tbl(J);
  

    END IF;
    exit when L_COM_REP_CSR%notfound;
    END LOOP;
       commit;
    
    close L_COM_REP_CSR;
 END LOOP; 
END IF;



--Dbms_Output.Put_Line('end time '||To_Char(Sysdate,'DD-MON-YY HH24:MI:SS'));

EXCEPTION
      WHEN OTHERS
      THEN
      FND_FILE.PUT_LINE(FND_FILE.LOG,'Exception....');

         apps.xxcus_error_pkg.xxcus_error_main_api (p_called_from         => l_err_callfrom
                                                   ,p_calling             => l_err_callpoint
                                                   ,p_request_id          => -1
                                                   ,p_ora_error_msg       => SQLERRM
                                                   ,p_error_desc          => l_err_msg
                                                   ,p_distribution_list   => g_dflt_email
                                                   ,p_module              => 'APPS');
   end COMM_REP_PAR;
end eis_rs_xxwc_com_rep_pkg_ext;
/
