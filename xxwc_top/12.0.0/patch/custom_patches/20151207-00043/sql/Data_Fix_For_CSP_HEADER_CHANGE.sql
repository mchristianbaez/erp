/* Ver        Date         Author           Description
   ---------  -----------  ---------------  ------------------------------------
   1.0        09-Dec-2015  Kishorebabu V    TMS#20151207-00043 @SF data Fix script for I310021
                                            For agreement# 18189, could you change the Branch in the header to 858 (from 207)
											per request from district manager Aaron Blake
                                            Initial Version   */

SET SERVEROUTPUT ON SIZE 100000;

BEGIN
  UPDATE xxwc.xxwc_om_contract_pricing_hdr
  SET    organization_id = 630
  WHERE  agreement_id    = 18189
  AND    organization_id = 309;
  COMMIT;
EXCEPTION
  WHEN others THEN
    dbms_output.put_line('Error occured while updating organization_id: '||SQLERRM);
END;
/