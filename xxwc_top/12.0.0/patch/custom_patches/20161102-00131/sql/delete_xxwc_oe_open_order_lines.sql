/*
 TMS: 20161102-00131 
 Date: 02/03/2017
 Notes: PUBD one time data script to delete records
*/
SET SERVEROUTPUT ON SIZE 1000000
DECLARE
BEGIN
     DBMS_OUTPUT.put_line ('TMS: 20161102-00131 , Script Before Delete');
  DELETE FROM XXWC.xxwc_oe_open_order_lines a
  WHERE EXISTS( select 1
  from apps.oe_order_headers_all     oh,
       apps.oe_order_lines_all       ol,
       xxwc.XXWC_OE_OPEN_ORDER_LINES xo
 where oh.headeR_id = ol.header_id
   and oh.header_id = xo.header_id   
   and xo.line_id = ol.line_id
   and ol.flow_Status_Code in ('CANCELLED', 'CLOSED')
   AND a.header_id = xo.header_id
   and a.line_id = xo.line_id
              ); 
  DBMS_OUTPUT.put_line ('TMS: 20161102-00131, After Delete, records Deleted : '||sql%rowcount);
  COMMIT;
EXCEPTION
   WHEN OTHERS
   THEN
      ROLLBACK;
      DBMS_OUTPUT.put_line ('TMS: 20161102-00131, Errors ='||SQLERRM);
END;
/