/*****************************************************************************************************
Copyright (c) 2017 HD Supply Group
All rights reserved.
******************************************************************************************************
$Header TMS_20170406-00344_direct_ship.sql $
Module Name: TMS_20170406-00344_direct_ship.sql
PURPOSE:
REVISIONS:
Ver        Date        Author               Description
---------  ----------  ---------------      -------------------------
1.0        05/30/2017  Pattabhi Avula       Initial Version(TMS#20170406-00344)
*****************************************************************************************************/
DECLARE
  CURSOR Cur_so_lines
  IS
    SELECT Line_id
    FROM oe_order_lines_all
    WHERE flow_status_code='AWAITING_RECEIPT'
    AND header_id        IN
      (SELECT header_id FROM oe_order_headers_all WHERE order_number=24042629
      );
  l_lin_key      VARCHAR2(30); -- := to_char(l_lin_id);
  l_ordered_qty  NUMBER;
  l_flow_exists  VARCHAR2(1);
  l_user_id      NUMBER;
  l_resp_id      NUMBER;
  l_resp_appl_id NUMBER;
  CURSOR line_check(l_lin_id NUMBER)
  IS
    SELECT line_id
    FROM oe_order_lines_all
    WHERE line_id                    = l_lin_id
    AND ( open_flag                  = 'N'
    OR NVL(invoiced_quantity,0)      > 0
    OR INVOICE_INTERFACE_STATUS_CODE = 'YES' ) ;
  CURSOR service_check(l_lin_id NUMBER)
  IS
    SELECT COUNT(*)
    FROM oe_order_lines_all
    WHERE service_reference_type_code = 'ORDER'
    AND service_reference_line_id     = l_lin_id
    AND open_flag                     = 'Y';
  -- Cursors to fetch details and their Serial Number(s)
  CURSOR wdd(l_lin_id NUMBER)
  IS
    SELECT delivery_detail_id,
      transaction_temp_id,
      serial_number,
      inventory_item_id,
      to_serial_number
    FROM wsh_delivery_details
    WHERE source_line_id = l_lin_id
    AND source_code      = 'OE'
    AND released_status IN ('Y','C');
  CURSOR msnt(txn_temp_id NUMBER)
  IS
    SELECT fm_serial_number,
      to_serial_number
    FROM mtl_serial_numbers_temp
    WHERE transaction_temp_id = txn_temp_id;
  l_line_check           NUMBER;
  l_oe_interfaced        NUMBER;
  l_wms_org              VARCHAR2(1);
  l_otm_installed        VARCHAR2(1);
  l_otm_enabled          VARCHAR2(1);
  l_cursor               INTEGER;
  l_stmt                 VARCHAR2(4000);
  l_up_cursor            INTEGER;
  l_up_stmt              VARCHAR2(4000);
  l_ignore               NUMBER;
  l_ship_from_org_id     NUMBER;
  l_opm_enabled          BOOLEAN;
  l_fm_serial            VARCHAR2(30);
  l_to_serial            VARCHAR2(30);
  Line_Cannot_Be_Updated EXCEPTION;
  l_heading              VARCHAR2(1) := 'N';
  CURSOR wsh_ifaced(p_lin_id NUMBER)
  IS
    SELECT SUBSTR(wdd.source_line_number, 1, 15) line_num ,
      SUBSTR(wdd.item_description, 1, 30) item_name ,
      wdd.shipped_quantity ,
      wdd.source_line_id line_id
    FROM wsh_delivery_details wdd,
      oe_order_lines_all oel
    WHERE wdd.inv_interfaced_flag   = 'Y'
    AND NVL(wdd.shipped_quantity,0) > 0
    AND oel.line_id                 = wdd.source_line_id
    AND oel.open_flag               = 'N'
    AND oel.ordered_quantity        = 0
    AND wdd.source_code             = 'OE'
    AND oel.line_id                 = p_lin_id
    AND EXISTS
      (SELECT 'x'
      FROM mtl_material_transactions mmt
      WHERE wdd.delivery_detail_id        = mmt.picking_line_id
      AND mmt.trx_source_line_id          = wdd.source_line_id
      AND mmt.transaction_source_type_id IN ( 2,8 )
      );
BEGIN
 -- dbms_output.put_line('Updating Line ID: '||i.line_id);
  FOR I IN Cur_so_lines
  LOOP
    l_lin_key := TO_CHAR(i.line_id);
    -- Check if line can be canceled
    OPEN line_check(I.line_id) ;
    FETCH line_check INTO l_line_check ;
    IF line_check%found THEN
      CLOSE line_check;
      dbms_output.put_line('Line is closed or Invoiced');
      raise Line_Cannot_Be_Updated;
    END IF;
    CLOSE line_check;
    OPEN service_check(i.line_id) ;
    FETCH service_check INTO l_line_check ;
    IF l_line_check > 0 THEN
      CLOSE service_check;
      dbms_output.put_line('There exist open service lines referencing this order line.');
      raise Line_Cannot_Be_Updated;
    END IF;
    CLOSE service_check;
    -- Check if line belongs to WMS Org
    BEGIN
      SELECT WSH_UTIL_VALIDATE.CHECK_WMS_ORG(ship_from_org_id),
        ship_from_org_id
      INTO l_wms_org,
        l_ship_from_org_id
      FROM oe_order_lines_all
      WHERE line_id = I.line_id;
    EXCEPTION
    WHEN no_data_found THEN
      dbms_output.put_line('Unable to get the Organization');
      raise Line_Cannot_Be_Updated;
    END;
    IF l_wms_org = 'Y' THEN
      -- Disallow cancellation if and only if there exist open delivery detail(s) for line
      -- under consideration. (Bug 6196723)
      DECLARE
        l_exist_count NUMBER := -1;
      BEGIN
        SELECT COUNT(*)
        INTO l_exist_count
        FROM wsh_delivery_details wdd,
          oe_order_lines_all line
        WHERE line.line_id                 = wdd.source_line_id
        AND wdd.source_code                = 'OE'
        AND line.line_id                   = i.line_id
        AND NVL(wdd.released_status, 'N') <> 'D' ;
        IF ( l_exist_count                 > 0 ) THEN
          dbms_output.put_line('This line belongs to a WMS Organization');
          raise Line_Cannot_Be_Updated;
        END IF;
      END;
    END IF;
    -- Check if OTM is installed or OTM is enabled for the Organization
    --{
    l_otm_installed := NVL(FND_PROFILE.VALUE('WSH_OTM_INSTALLED'), 'N');
    BEGIN
      l_cursor := dbms_sql.open_cursor;
      l_stmt   := 'select wsp.otm_enabled from wsh_shipping_parameters wsp, oe_order_lines_all ol '|| 'where wsp.organization_id = ol.ship_from_org_id '|| 'and   ol.line_id = :line_id ';
      dbms_sql.parse(l_cursor, l_stmt, dbms_sql.v7);
      dbms_sql.define_column(l_cursor, 1, l_otm_enabled, 1);
      dbms_sql.bind_variable(l_cursor, ':line_id', i.line_id);
      l_ignore                        := dbms_sql.execute(l_cursor);
      IF dbms_sql.fetch_rows(l_cursor) > 0 THEN
        dbms_sql.column_value(l_cursor, 1, l_otm_enabled);
        dbms_output.put_line('l_otm_enabled '||l_otm_enabled);
      END IF;
      dbms_sql.close_cursor(l_cursor);
    EXCEPTION
    WHEN OTHERS THEN
      IF dbms_sql.is_open(l_cursor) THEN
        dbms_sql.close_cursor(l_cursor);
      END IF;
      IF l_otm_installed IN ('Y','O') THEN
        dbms_output.put_line('l_otm_installed '||l_otm_installed);
        dbms_output.put_line('OTM: Integration Enabled is enabled for Order Management');
        raise Line_Cannot_Be_Updated;
      END IF;
    END;
    IF NVL(l_otm_enabled, 'N') = 'Y' AND l_otm_installed IN ('Y','O') THEN
      dbms_output.put_line('l_otm_installed '||l_otm_installed||' , l_otm_enabled '||l_otm_enabled);
      dbms_output.put_line('OTM: Integration Enabled is enabled for Organization for which this Line belongs');
      raise Line_Cannot_Be_Updated;
    END IF;
    --}
    l_flow_exists := 'Y';
    UPDATE oe_order_lines_all
    SET flow_status_code = 'CANCELLED' ,
      open_flag          = 'N' ,
      cancelled_flag     = 'Y' ,
      ordered_quantity   = 0 ,
      ordered_quantity2  = DECODE(ordered_quantity2,NULL,NULL,0) -- OPM related
      ,
      cancelled_quantity  = ordered_quantity + NVL(cancelled_quantity, 0) ,
      visible_demand_flag = NULL ,
      last_updated_by     = -6156992 ,
      last_update_date    = sysdate
    WHERE line_id         = i.line_id;
    -- Added for bug 8532859
    DELETE FROM oe_line_sets WHERE line_id = i.line_id;
    BEGIN
      SELECT number_value
      INTO l_user_id
      FROM wf_item_attribute_values
      WHERE item_type = 'OEOL'
      AND item_key    = l_lin_key
      AND name        = 'USER_ID';
      SELECT number_value
      INTO l_resp_id
      FROM wf_item_attribute_values
      WHERE item_type = 'OEOL'
      AND item_key    = l_lin_key
      AND name        = 'RESPONSIBILITY_ID';
      SELECT number_value
      INTO l_resp_appl_id
      FROM wf_item_attribute_values
      WHERE item_type = 'OEOL'
      AND item_key    = l_lin_key
      AND name        = 'APPLICATION_ID';
    EXCEPTION
    WHEN No_Data_Found THEN
      l_flow_exists := 'N';
    END;
    IF l_flow_exists = 'Y' THEN
      fnd_global.apps_initialize(l_user_id, l_resp_id, l_resp_appl_id);
      wf_engine.handleerror( OE_Globals.G_WFI_LIN , l_lin_key , 'CLOSE_LINE' , 'RETRY' , 'CANCEL' );
    END IF;
    FOR wsh_ifaced_rec IN wsh_ifaced(i.line_id)
    LOOP
      IF l_heading = 'N' THEN
        dbms_output.put_line(' ');
        dbms_output.put_line('Following Cancelled Lines have already been Interfaced to Inventory.');
        dbms_output.put_line('Onhand Qty must be manually adjusted for these Items and Quantities.');
        dbms_output.put_line(' ');
        dbms_output.put_line('+---------------+------------------------------+---------------+---------------+');
        dbms_output.put_line('|Line No.       |Item Name                     |    Shipped Qty|        Line Id|');
        dbms_output.put_line('+---------------+------------------------------+---------------+---------------+');
        l_heading := 'Y';
      END IF;
      dbms_output.put_line('|'||rpad(wsh_ifaced_rec.line_num, 15)|| '|'||rpad(wsh_ifaced_rec.item_name, 30)|| '|'||lpad(TO_CHAR(wsh_ifaced_rec.shipped_quantity), 15)|| '|'||lpad(TO_CHAR(wsh_ifaced_rec.line_id), 15)||'|');
    END LOOP;
    UPDATE wsh_delivery_assignments
    SET delivery_id             = NULL ,
      parent_delivery_detail_id = NULL ,
      last_updated_by           = -1 ,
      last_update_date          = sysdate
    WHERE delivery_detail_id   IN
      (SELECT wdd.delivery_detail_id
      FROM wsh_delivery_details wdd,
        oe_order_lines_all oel
      WHERE wdd.source_line_id = oel.line_id
      AND wdd.source_code      = 'OE'
      AND oel.cancelled_flag   = 'Y'
      AND oel.line_id          = i.line_id
      AND released_status     <> 'D'
      );
    -- Check if Org is an OPM or Inventory Org
    l_opm_enabled := INV_GMI_RSV_BRANCH.PROCESS_BRANCH(p_organization_id => l_ship_from_org_id);
    IF NOT l_opm_enabled THEN
      --{
      -- Inventory Org
      -- Updating Move Order lines for Released To Warehouse details as 'Cancelled by Source'
      UPDATE mtl_txn_request_lines
      SET line_status = 9
      WHERE line_id  IN
        (SELECT move_order_line_id
        FROM wsh_delivery_details
        WHERE source_line_id = i.line_id
        AND released_status  = 'S'
        AND source_code      = 'OE'
        );
      -- Removing Serial Number(s) and Unmarking them
      FOR rec IN wdd(i.line_id)
      LOOP
        --{
        IF rec.serial_number IS NOT NULL THEN
          UPDATE mtl_serial_numbers
          SET group_mark_id       = NULL,
            line_mark_id          = NULL,
            lot_line_mark_id      = NULL
          WHERE inventory_item_id = rec.inventory_item_id
          AND serial_number BETWEEN rec.serial_number AND NVL(rec.to_serial_number, rec.serial_number);
        elsif rec.transaction_temp_id IS NOT NULL THEN
          --{
          FOR msnt_rec IN msnt(rec.transaction_temp_id)
          LOOP
            UPDATE mtl_serial_numbers
            SET group_mark_id       = NULL,
              line_mark_id          = NULL,
              lot_line_mark_id      = NULL
            WHERE inventory_item_id = rec.inventory_item_id
            AND serial_number BETWEEN msnt_rec.fm_serial_number AND NVL(msnt_rec.to_serial_number, msnt_rec.fm_serial_number);
          END LOOP;
          DELETE
          FROM mtl_serial_numbers_temp
          WHERE transaction_temp_id = rec.transaction_temp_id;
          BEGIN
            --{
            l_cursor := dbms_sql.open_cursor;
            l_stmt   := 'select fm_serial_number, to_serial_number '|| 'from   wsh_serial_numbers '|| 'where  delivery_detail_id = :delivery_detail_id ';
            dbms_sql.parse(l_cursor, l_stmt, dbms_sql.v7);
            dbms_sql.define_column(l_cursor, 1, l_fm_serial, 1);
            dbms_sql.define_column(l_cursor, 2, l_to_serial, 1);
            dbms_sql.bind_variable(l_cursor, ':delivery_detail_id', rec.delivery_detail_id);
            l_ignore := dbms_sql.execute(l_cursor);
            LOOP
              IF dbms_sql.fetch_rows(l_cursor) > 0 THEN
                dbms_sql.column_value(l_cursor, 1, l_fm_serial);
                dbms_sql.column_value(l_cursor, 2, l_to_serial);
                l_up_cursor := dbms_sql.open_cursor;
                l_up_stmt   := 'update mtl_serial_numbers msn '|| 'set    msn.group_mark_id    = null,  '|| '       msn.line_mark_id     = null,  '|| '       msn.lot_line_mark_id = null   '|| 'where  msn.inventory_item_id = :inventory_item_id '|| 'and    msn.serial_number between :fm_serial and :to_serial ';
                dbms_sql.parse(l_up_cursor, l_up_stmt, dbms_sql.v7);
                dbms_sql.bind_variable(l_up_cursor, ':inventory_item_id', rec.inventory_item_id);
                dbms_sql.bind_variable(l_up_cursor, ':fm_serial', l_fm_serial);
                dbms_sql.bind_variable(l_up_cursor, ':to_serial', NVL(l_to_serial, l_fm_serial));
                l_ignore := dbms_sql.execute(l_up_cursor);
                dbms_sql.close_cursor(l_up_cursor);
              ELSE
                EXIT;
              END IF;
            END LOOP;
            dbms_sql.close_cursor(l_cursor);
            l_cursor := dbms_sql.open_cursor;
            l_stmt   := 'delete from wsh_serial_numbers '|| 'where delivery_detail_id = :delivery_detail_id ';
            dbms_sql.parse(l_cursor, l_stmt, dbms_sql.v7);
            dbms_sql.bind_variable(l_cursor, ':delivery_detail_id', rec.delivery_detail_id);
            l_ignore := dbms_sql.execute(l_cursor);
            dbms_sql.close_cursor(l_cursor);
          EXCEPTION
          WHEN OTHERS THEN
            IF dbms_sql.is_open(l_up_cursor) THEN
              dbms_sql.close_cursor(l_up_cursor);
            END IF;
            IF dbms_sql.is_open(l_cursor) THEN
              dbms_sql.close_cursor(l_cursor);
            END IF;
            --}
          END;
          --}
        END IF;
        --}
      END LOOP;
      --}
    ELSE
      --{
      -- OPM Org
      UPDATE ic_txn_request_lines
      SET line_status = 9
      WHERE line_id  IN
        (SELECT move_order_line_id
        FROM wsh_delivery_details
        WHERE source_line_id = i.line_id
        AND released_status  = 'S'
        AND source_code      = 'OE'
        );
      UPDATE ic_tran_pnd
      SET delete_mark   = 1
      WHERE line_id     = i.line_id
      AND doc_type      = 'OMSO'
      AND trans_qty     < 0
      AND delete_mark   = 0
      AND completed_ind = 0;
      --}
    END IF;
    UPDATE wsh_delivery_details
    SET released_status       = 'D' ,
      src_requested_quantity  = 0 ,
      src_requested_quantity2 = DECODE(src_requested_quantity2,NULL,NULL,0) ,
      requested_quantity      = 0 ,
      requested_quantity2     = DECODE(requested_quantity2,NULL,NULL,0) ,
      shipped_quantity        = 0 ,
      shipped_quantity2       = DECODE(shipped_quantity2,NULL,NULL,0) ,
      picked_quantity         = 0 ,
      picked_quantity2        = DECODE(picked_quantity2,NULL,NULL,0) ,
      cycle_count_quantity    = 0 ,
      cycle_count_quantity2   = DECODE(src_requested_quantity2,NULL,NULL,0) ,
      cancelled_quantity      = DECODE(requested_quantity,0,cancelled_quantity,requested_quantity) ,
      cancelled_quantity2     = DECODE(requested_quantity2,NULL,NULL,0,cancelled_quantity2,requested_quantity2) ,
      subinventory            = NULL ,
      locator_id              = NULL ,
      lot_number              = NULL ,
      serial_number           = NULL ,
      to_serial_number        = NULL ,
      transaction_temp_id     = NULL ,
      revision                = NULL ,
      ship_set_id             = NULL ,
      inv_interfaced_flag     = 'X' ,
      oe_interfaced_flag      = 'X' ,
      last_updated_by         = -1 ,
      last_update_date        = SYSDATE
    WHERE source_line_id      = i.line_id
    AND source_code           = 'OE'
    AND released_status      <> 'D'
    AND EXISTS
      (SELECT 'x'
      FROM oe_order_lines_all oel
      WHERE source_line_id   = oel.line_id
      AND oel.cancelled_flag = 'Y'
      );
  END LOOP;
EXCEPTION
WHEN Line_Cannot_Be_Updated THEN
  ROLLBACK;
  dbms_output.put_line('This script cannot cancel this Order Line, please contact Oracle Support');
WHEN OTHERS THEN
  ROLLBACK;
  dbms_output.put_line(SUBSTR(sqlerrm, 1, 240));
END;
/