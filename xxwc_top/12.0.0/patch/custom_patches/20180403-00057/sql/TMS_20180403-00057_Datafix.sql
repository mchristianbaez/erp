/***********************************************************************************************************************************************
   NAME:     TMS_20180403-00057_Datafix.sql
   PURPOSE:  Data fix

   REVISIONS:
   Ver        Date         Author           Description
   ---------  ----------   ---------------  ----------------------------------------------------------------------------------------------------
   1.0        04/03/2018   Rakesh Patel     TMS#20180403-00057
************************************************************************************************************************************************/
SET SERVEROUTPUT ON SIZE 1000000

   CREATE TABLE XXWC.mtl_transactions_interface_bkp     AS SELECT * FROM APPS.mtl_transactions_interface
   /
   
   COMMENT ON TABLE XXWC.mtl_transactions_interface_bkp  is 'Created for TMS_20180403-00057_Datafix'  
   /
   
   CREATE TABLE XXWC.mtl_serial_numbers_intf_bkp   AS SELECT * FROM APPS.mtl_serial_numbers_interface
   /
   
   COMMENT ON TABLE XXWC.mtl_serial_numbers_intf_bkp  is 'Created for TMS_20180403-00057_Datafix'
   /
   
   CREATE TABLE XXWC.mtl_transaction_lots_intf_bkp AS SELECT * FROM APPS.mtl_transaction_lots_interface
   /
   
   COMMENT ON TABLE XXWC.mtl_transaction_lots_intf_bkp  is 'Created for TMS_20180403-00057_Datafix'
   /
  
   CREATE TABLE XXWC.mtl_material_trx_temp_bkp AS SELECT * FROM APPS.mtl_material_transactions_temp
   /
   
   COMMENT ON TABLE XXWC.mtl_material_trx_temp_bkp  is 'Created for TMS_20180403-00057_Datafix'
   /
   
   CREATE TABLE XXWC.mtl_serial_numbers_temp_bkp        AS SELECT * FROM APPS.mtl_serial_numbers_temp
   /

   COMMENT ON TABLE XXWC.mtl_serial_numbers_temp_bkp  is 'Created for TMS_20180403-00057_Datafix'
   /
   
   CREATE TABLE XXWC.mtl_transaction_lots_temp_bkp      AS SELECT * FROM APPS.mtl_transaction_lots_temp
   /
   
   COMMENT ON TABLE XXWC.mtl_transaction_lots_temp_bkp  is 'Created for TMS_20180403-00057_Datafix'
   /
   
   create or replace view mmtt_mti_records_v as
   select a.transaction_interface_id from
   mtl_material_transactions_temp b, mtl_transactions_interface a
   where b.transaction_temp_id = a.transaction_interface_id
   and b.transaction_mode = 8
   /
     
   create table xxwc.mti_dup_backup_mmtt as (select * from mtl_transactions_interface
   where transaction_interface_id in (select transaction_interface_id from mmtt_mti_records_v))
   /
   
   COMMENT ON TABLE xxwc.mti_dup_backup_mmtt  is 'Created for TMS_20180403-00057_Datafix'
   /

   create table xxwc.msni_dup_backup_mmtt as (select * from mtl_serial_numbers_interface
   where transaction_interface_id in (select transaction_interface_id from mmtt_mti_records_v))
   union
   (select * from mtl_serial_numbers_interface msni where msni.transaction_interface_id in (
   select mtli.serial_transaction_temp_id from mtl_transaction_lots_interface mtli
   where mtli.transaction_interface_id in (select transaction_interface_id from mmtt_mti_records_v )))
   /
   
   COMMENT ON TABLE xxwc.msni_dup_backup_mmtt is 'Created for TMS_20180403-00057_Datafix'
   /
   
   create table xxwc.mtli_dup_backup_mmtt as (select * from mtl_transaction_lots_interface where transaction_interface_id 
   in (select transaction_interface_id from mmtt_mti_records_v))
   /

   COMMENT ON TABLE xxwc.mtli_dup_backup_mmtt  is 'Created for TMS_20180403-00057_Datafix';  
   /
BEGIN

   DBMS_OUTPUT.put_line ('Before creating backup table');
   
   delete from mtl_serial_numbers_interface where transaction_interface_id
   in (select transaction_interface_id from mmtt_mti_records_v );
   
   DBMS_OUTPUT.put_line ('Records Deleted from mtl_serial_numbers_interface -' || SQL%ROWCOUNT);

   delete from mtl_serial_numbers_interface msni
   where msni.transaction_interface_id in
   (select mtli.serial_transaction_temp_id from mtl_transaction_lots_interface mtli
   where mtli.transaction_interface_id in (select transaction_interface_id from mmtt_mti_records_v ));
   
   DBMS_OUTPUT.put_line ('Records Deleted from mtl_serial_numbers_interface -' || SQL%ROWCOUNT);

   delete from mtl_transaction_lots_interface where transaction_interface_id in
   (select transaction_interface_id from mmtt_mti_records_v );
   
   DBMS_OUTPUT.put_line ('Records Deleted from mtl_transaction_lots_interface -' || SQL%ROWCOUNT);
   
   delete from mtl_transactions_interface where transaction_interface_id in
   (select transaction_interface_id from mmtt_mti_records_v );
   
   DBMS_OUTPUT.put_line ('Records Deleted from mtl_transactions_interface -' || SQL%ROWCOUNT);
   
   update MTL_MATERIAL_TRANSACTIONS_TEMP
   set TRANSACTION_MODE = 3,
   LOCK_FLAG = 'N',
   PROCESS_FLAG = 'Y',
   ERROR_CODE = NULL,
   ERROR_EXPLANATION = NULL
   where transaction_mode = 8;
  
   DBMS_OUTPUT.put_line ('Records updated in MTL_MATERIAL_TRANSACTIONS_TEMP -' || SQL%ROWCOUNT);

   COMMIT;
   EXCEPTION
   WHEN OTHERS
   THEN
      DBMS_OUTPUT.put_line ('Error executing the script ' || SQLERRM);
	  ROLLBACK;
END;
/