/*************************************************************************
  $Header TMS FlipdatesDatafix_Updates.sql $
  Module Name: TMS 20151102-00019 Flip dates Datafix-Updates

  PURPOSE: Update the Flip date in XXWC_ISR_DETAILS_ALL

  REVISIONS:
  Ver        Date         Author                Description
  ---------  -----------  ------------------    ----------------
  1.0        09-Dec-2015  Ram Talluri    TMS # 20151102-00019 

**************************************************************************/ 
SET SERVEROUTPUT ON SIZE 1000000
DECLARE
BEGIN
     
   DBMS_OUTPUT.put_line ('TMS: 20151102-00019  , Before Flip Date Update');
   UPDATE XXWC.XXWC_ISR_DETAILS_ALL isr 
      SET flip_date =
          (SELECT flip_date 
             FROM XXWC.XXWC_ISR_FLIP_DATE_UPD_STG
            WHERE org= isr.org
              AND item_number = isr.item_number)
    WHERE EXISTS
          (SELECT 1
             FROM XXWC.XXWC_ISR_FLIP_DATE_UPD_STG
            WHERE org= isr.org
              AND item_number = isr.item_number);

    DBMS_OUTPUT.put_line ('TMS: 20151102-00019  , After Flip Date Update : Rows Updated '||SQL%ROWCOUNT);
              
   COMMIT;    

EXCEPTION WHEN OTHERS THEN
   
    ROLLBACK;
    DBMS_OUTPUT.put_line ('TMS: 20151102-00019, Errors : '||SQLERRM);   
END;
/