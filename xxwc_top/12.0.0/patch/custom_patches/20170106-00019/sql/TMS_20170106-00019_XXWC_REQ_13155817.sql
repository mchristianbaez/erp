/*************************************************************************
  $Header TMS_20170106-00019_XXWC_REQ_13155817.sql $
  Module Name: 20170106-00019  Data Fix script for 20161123-00201

  PURPOSE: Data fix script for 20170106-00019--No permanent fix in process (But patch available per oracle)

  REVISIONS:
  Ver        Date         Author                Description
  ---------  -----------  ------------------    --------------------------
  1.0        12-JAN-2017  Ashwin Sridhar        TMS#20170106-00019

**************************************************************************/ 
SET SERVEROUTPUT ON SIZE 1000000

BEGIN

   DBMS_OUTPUT.put_line ('Before update');

   UPDATE rcv_transactions_interface
      SET request_id = NULL,
          processing_request_id = NULL,
          processing_status_code = 'PENDING',
          transaction_status_code = 'PENDING',
          processing_mode_code = 'BATCH',
          last_update_date = SYSDATE,
          Last_updated_by = 16991
    WHERE     creation_date >
              TO_DATE ('05-JAN-2017 00:00:00', 'DD-MON-YYYY HH24:MI:SS')
       AND PROCESSING_STATUS_CODE = 'PENDING'
       AND PROCESSING_MODE_CODE = 'ONLINE'
       AND TRANSACTION_STATUS_CODE = 'PENDING'
       AND creation_date NOT LIKE SYSDATE;

   DBMS_OUTPUT.put_line ('Records update-' || SQL%ROWCOUNT);
 
   COMMIT;
   
EXCEPTION
WHEN OTHERS THEN

  DBMS_OUTPUT.put_line ('Unable to update ' || SQLERRM);

END;
/