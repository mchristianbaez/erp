/******************************************************************************
  $Header TMS_20171214_00342_Order_stuck_in_Close.sql $
  Module Name:Data Fix script for 20171214-00342

  PURPOSE: Data fix script for 20171214-00342 Order stuck in Close

  REVISIONS:
  Ver        Date         Author                Description
  ---------  -----------  ------------------    -------------------------------
  1.0        22-Jan-2018  Krishna Kumar        TMS#20171214-00342

*******************************************************************************/
SET SERVEROUTPUT ON SIZE 1000000

BEGIN
   DBMS_OUTPUT.put_line ('Before Update');

   UPDATE apps.oe_order_lines_all
      SET flow_status_Code='CLOSED'
    WHERE header_id=58089177
    AND   line_id = 94527072;


   DBMS_OUTPUT.put_line ('Records update-' || SQL%ROWCOUNT);
   COMMIT;
EXCEPTION
   WHEN OTHERS
   THEN
      DBMS_OUTPUT.put_line ('Unable to update record ' || SQLERRM);
END;
/