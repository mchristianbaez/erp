/*
TMS#20171218-00287 - Changes to the Rental Asset Transfer Report and the Asset Register Report
Created by Vamshi on 22-Dec-2017
*/
SET SERVEROUT ON

CREATE TABLE XXWC.XXWC_MTL_MATERIAL_TRNS_TMP
AS
   SELECT *
     FROM mtl_material_transactions_temp
    WHERE     TRANSACTION_HEADER_ID = 675433257
          AND TRANSACTION_TEMP_ID = 675433258;
/
DELETE FROM mtl_material_transactions_temp
      WHERE     TRANSACTION_HEADER_ID = 675433257
            AND TRANSACTION_TEMP_ID = 675433258;

COMMIT;
/