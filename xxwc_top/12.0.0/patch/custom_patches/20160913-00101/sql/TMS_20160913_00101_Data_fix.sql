/*
 TMS: 20160913-00101 - ISO# 21149855
 Date: 10/06/2016
 */

SET SERVEROUTPUT ON SIZE 1000000

DROP TABLE xxwc.xxwc_ms_backup_table;

create table xxwc.xxwc_ms_backup_table as
SELECT ms.ROWID rwid, ms.*
  FROM mtl_supply ms
 WHERE quantity < 0
UNION
SELECT ms.ROWID rwid, ms.*
  FROM mtl_supply ms, rcv_shipment_lines rsl
 WHERE     ms.shipment_line_id = rsl.shipment_line_id
       AND rsl.shipment_line_status_code = 'CANCELLED'
UNION
SELECT ms.ROWID rwid, ms.*
  FROM mtl_supply ms
 WHERE     ms.shipment_line_id IS NOT NULL
       AND NOT EXISTS
              (SELECT 1
                 FROM rcv_shipment_lines rsl
                WHERE rsl.shipment_line_id = ms.shipment_line_id)
UNION
SELECT ms.ROWID rwid, ms.*
  FROM mtl_supply ms, po_line_locations_all pll
 WHERE     pll.line_location_id = ms.po_line_location_id
       AND ms.supply_type_code IN ('SHIPMENT', 'RECEIVING')
       AND (   NVL (pll.closed_code, 'OPEN') IN ('FINALLY CLOSED')
            OR NVL (pll.cancel_flag, 'N') = 'Y')
UNION
SELECT ms.ROWID rwid, ms.*
  FROM mtl_supply ms, po_requisition_headers_all prh
 WHERE     ms.req_header_id = prh.requisition_header_id
       AND ms.supply_type_code IN ('SHIPMENT', 'RECEIVING')
       AND prh.closed_code = 'FINALLY CLOSED'
UNION
SELECT ms.ROWID rwid, ms.*
  FROM mtl_supply ms, po_requisition_lines_all prl
 WHERE     ms.req_line_id IS NOT NULL
       AND ms.req_line_id = prl.requisition_line_id
       AND ms.supply_type_code IN ('REQ', 'SHIPMENT')
       AND prl.quantity_received >= prl.quantity
       AND prl.quantity_delivered >= prl.quantity
UNION
SELECT ms.ROWID rwid, ms.*
  FROM mtl_supply ms, po_line_locations_all pll
 WHERE     ms.po_line_location_id IS NOT NULL
       AND ms.po_line_location_id = pll.line_location_id
       AND ms.supply_type_code IN ('PO')
       AND pll.quantity_received >= pll.quantity;



delete from mtl_supply
where  rowid in (select rwid from xxwc.xxwc_ms_backup_table);

commit
/