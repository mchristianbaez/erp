/**************************************************************************
      *
      * PACKAGE
      * XXCUS_AP_FREIGHT_DATA_FIX
      *
      * DESCRIPTION
      *  To process stucked freight invoices from staging
      *
      *
      * HISTORY
      * =======
      *
      * VERSION DATE        AUTHOR(S)       DESCRIPTION
      * ------- ----------- --------------- ------------------------------------
      * 1.00    08/14/2015   Maharajan S     Initial creation TMS#20150814-00053
      *
      *************************************************************************/
update xxcus.XXCUSAP_TMS_INV_STG_TBL
set status = 'NEW'
WHERE status = 'NEW_1'
/
commit
/