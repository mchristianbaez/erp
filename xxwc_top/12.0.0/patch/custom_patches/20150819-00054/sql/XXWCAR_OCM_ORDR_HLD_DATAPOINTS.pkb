CREATE OR REPLACE PACKAGE BODY APPS.xxwcar_ocm_ordr_hld_datapoints
AS
   /***************************************************************************
           *    script name: xxwcar_ocm_ordr_hld_datapoints_pkg
           *
           *    functional purpose: Used to Create additional data points in
           *                       Oracle credit management
           *    history:
           *
           *    version    date              author             description
           ********************************************************************
           *    1.0        31-May-2012  Steve Griffin     initial development.
           *    1.1        05-Jun-2012  Shankar Hariharan Bug fixes
           *    1.2        04-Jan-2013  Shankar Hariharan Bug fixes
           *    1.3        25-Mar-2013  Shankar Hariharan Additional functions per
           *                                              Steve's request
           *    1.4        20-May-2013 Shankar Hariharan  Additional data points per tms ticket
           *                                              for simplifying credit holds 
           *    1.5        06-Oct-2014  Maharajan Shunmugam TMS# 20141001-00158 Canada Multi-Org changes
           *    1.6        15-Oct-2015  Kishorebabu V        TMS# 20150819-00054 AR - Credit Holds causing issues with billing Rentals
           *******************************************************************/
   l_party_id                NUMBER (30) := NULL;
   -- party_id
   l_cust_account_id         NUMBER (30) := NULL;
   -- customer account id
   l_cust_site_use_id        NUMBER (30) := NULL;
   --cust account site id
   l_site_use_id             NUMBER (30) := NULL;
   -- site use id


   --Site Aging Variables
   l_total_ar_site           NUMBER (30) := NULL;
   --Site total AR
   l_current_site            NUMBER (30) := NULL;
   --Site current A/R
   l_1_to_30_site            NUMBER (30) := NULL;
   --Site 1-30 A/R
   l_31_to_60_site           NUMBER (30) := NULL;
   --Site 31-60 A/R
   l_61_to_90_site           NUMBER (30) := NULL;
   --Site 61 to 90 A/R
   l_91_to_180_site          NUMBER (30) := NULL;
   --Site 91 to 180 A/R
   l_181_to_360_site         NUMBER (30) := NULL;
   --Site 181 to 360 A/R
   l_360_plus_site           NUMBER (30) := NULL;
   --Site 360+ A/R

   --Account Aging Variables
   l_total_ar_acct           NUMBER (30) := NULL;
   --Account total AR
   l_current_acct            NUMBER (30) := NULL;
   --Account current A/R
   l_1_to_30_acct            NUMBER (30) := NULL;
   --Account 1-30 A/R
   l_31_to_60_acct           NUMBER (30) := NULL;
   --Account 31-60 A/R
   l_61_to_90_acct           NUMBER (30) := NULL;
   --Account 61 to 90 A/R
   l_91_to_180_acct          NUMBER (30) := NULL;
   --Account 91 to 180 A/R
   l_181_to_360_acct         NUMBER (30) := NULL;
   --Account 181 to 360 A/R
   l_360_plus_acct           NUMBER (30) := NULL;
   --Account 360+ A/R



   --Account level non-aging variables
   l_adp_acct                NUMBER (30) := NULL;
   --Account avg. days to pay
   l_cust_since_acct         DATE := NULL;
   --Account start date
   l_high_credit_acct        NUMBER (30) := NULL;
   --Account high credit
   l_last_pmt_date_acct      DATE := NULL;
   --Account last payment date
   l_margin_acct             NUMBER (30) := NULL;
   --Account margin
   l_AM_site                 VARCHAR2 (30) := NULL;
   --Account Manager on order header


   --Site level non-aging variables
   l_credit_limit_site       NUMBER (30) := NULL;
   --Site credit limit
   l_NTO_flag_site           VARCHAR2 (30) := NULL;
   --Site NTO flag
   l_NTO_num_site            VARCHAR2 (30) := NULL;
   --Site NTO Number
   l_NTO_job_info_ind_site   VARCHAR2 (30) := NULL;
   --Site NTO job info indicator
   l_NTO_job_total_site      NUMBER (30) := NULL;
   --Site NTO job total
   l_NTO_date_site           DATE := NULL;
   --Site NTO date
   vget_nto_data             get_nto_data_rec := NULL;

   -- credit data

   l_order_total             NUMBER := NULL; 

   /* Cursors to return site specific noticing datapoints */
   /*                                                */
   /*                                                  */
   CURSOR c_get_nto_data
   IS
        SELECT NVL (hcas.attribute_category, 'N')
             ,                                                      --NTO Flag
              NVL (hcas.attribute14, 'U')
             ,                                              --Job info on file
              NVL (hcas.attribute20, '00 / 00 / 0000')
             ,                                                      --NTO Date
              NVL (hcas.attribute19, 'N')
             ,                                                    --NTO Number
              NVL (hcas.attribute18, 'N')                      --NTO Job Total
          FROM hz_cust_acct_sites hcas
             , hz_cust_site_uses hcsu
         WHERE     1 = 1
               AND hcas.cust_acct_site_id = hcsu.cust_acct_site_id
               AND hcsu.site_use_id = l_cust_site_use_id
      ORDER BY hcas.creation_date DESC;


   /* Created by Steve Gribbin                         */
   /*                                                  */
   /* This function calls the c_get_nto_data cursor    */
   /* defined above, returning the NTO flag            */
   /*                                                  */
   /*          */

   FUNCTION xxocm_NTO_flag (x_resultout      OUT NOCOPY VARCHAR2
                          , x_errormsg       OUT NOCOPY VARCHAR2)
      RETURN VARCHAR2
   IS
      l_NTO_flag_site   VARCHAR2 (30) := NULL;
   BEGIN
      x_resultout := fnd_api.g_ret_sts_success;
      --l_site_use_id  := TO_NUMBER (ocm_add_data_points.pg_ocm_add_dp_param_rec.p_site_use_id);
      l_cust_site_use_id :=
         ocm_add_data_points.pg_ocm_add_dp_param_rec.p_site_use_id;

      BEGIN
         OPEN c_get_NTO_data;

         FETCH c_get_NTO_data INTO vget_NTO_data;

         CLOSE c_get_NTO_data;

         l_NTO_flag_site := NVL (vget_NTO_data.attribute_category, 'N');
      EXCEPTION
         WHEN NO_DATA_FOUND
         THEN
            l_NTO_flag_site := 'N';
         WHEN OTHERS
         THEN
            x_resultout := fnd_api.g_ret_sts_unexp_error;
            x_errormsg := SQLERRM;
      END;

      RETURN (l_NTO_flag_site);
   END xxocm_NTO_flag;



   /* Created by Steve Gribbin                         */
   /*                                                  */
   /* This function calls the c_get_nto_data cursor    */
   /* defined above, returning the Job info on file    */
   /* flag                                             */
   /*          */

   FUNCTION xxocm_job_info_flag (x_resultout      OUT NOCOPY VARCHAR2
                               , x_errormsg       OUT NOCOPY VARCHAR2)
      RETURN VARCHAR2
   IS
      l_NTO_job_info_ind_site   VARCHAR2 (30) := NULL;
   BEGIN
      x_resultout := fnd_api.g_ret_sts_success;
      --l_site_use_id  := TO_NUMBER (ocm_add_data_points.pg_ocm_add_dp_param_rec.p_site_use_id);
      l_cust_site_use_id :=
         ocm_add_data_points.pg_ocm_add_dp_param_rec.p_site_use_id;

      BEGIN
         OPEN c_get_NTO_data;

         FETCH c_get_NTO_data INTO vget_NTO_data;

         CLOSE c_get_NTO_data;

         l_NTO_job_info_ind_site := NVL (vget_NTO_data.attribute14, 'U');
      EXCEPTION
         WHEN NO_DATA_FOUND
         THEN
            l_NTO_job_info_ind_site := 'U';
         WHEN OTHERS
         THEN
            x_resultout := fnd_api.g_ret_sts_unexp_error;
            x_errormsg := SQLERRM;
      END;

      RETURN (l_NTO_job_info_ind_site);
   END xxocm_job_info_flag;



   /* Created by Steve Gribbin                         */
   /*                                                  */
   /* This function calls the c_get_nto_data cursor    */
   /* defined above, returning the NTO Date            */
   /*                                              */
   FUNCTION xxocm_NTO_date (x_resultout      OUT NOCOPY VARCHAR2
                          , x_errormsg       OUT NOCOPY VARCHAR2)
      RETURN VARCHAR2
   IS
      l_NTO_date_site   VARCHAR2 (30) := NULL;
   BEGIN
      x_resultout := fnd_api.g_ret_sts_success;
      --l_site_use_id  := TO_NUMBER (ocm_add_data_points.pg_ocm_add_dp_param_rec.p_site_use_id);
      l_cust_site_use_id :=
         ocm_add_data_points.pg_ocm_add_dp_param_rec.p_site_use_id;

      BEGIN
         OPEN c_get_NTO_data;

         FETCH c_get_NTO_data INTO vget_NTO_data;

         CLOSE c_get_NTO_data;

         l_NTO_date_site := NVL (vget_NTO_data.attribute20, '00/00/0000');
      EXCEPTION
         WHEN NO_DATA_FOUND
         THEN
            l_NTO_date_site := NULL;
         WHEN OTHERS
         THEN
            x_resultout := fnd_api.g_ret_sts_unexp_error;
            x_errormsg := SQLERRM;
      END;

      RETURN (l_NTO_date_site);
   END xxocm_NTO_date;



   /* Created by Steve Gribbin                         */
   /*                                                  */
   /* This function calls the c_get_nto_data cursor    */
   /* defined above, returning the NTO Number          */
   /*                                              */
   FUNCTION xxocm_NTO_number (x_resultout      OUT NOCOPY VARCHAR2
                            , x_errormsg       OUT NOCOPY VARCHAR2)
      RETURN VARCHAR2
   IS
      l_NTO_num_site   VARCHAR2 (30) := NULL;
   BEGIN
      x_resultout := fnd_api.g_ret_sts_success;
      --l_site_use_id  :=  TO_NUMBER (ocm_add_data_points.pg_ocm_add_dp_param_rec.p_site_use_id);
      l_cust_site_use_id :=
         ocm_add_data_points.pg_ocm_add_dp_param_rec.p_site_use_id;

      BEGIN
         OPEN c_get_NTO_data;

         FETCH c_get_NTO_data INTO vget_NTO_data;

         CLOSE c_get_NTO_data;

         l_NTO_num_site := NVL (vget_NTO_data.attribute19, 'N');
      EXCEPTION
         WHEN NO_DATA_FOUND
         THEN
            l_NTO_num_site := 'N';
         WHEN OTHERS
         THEN
            x_resultout := fnd_api.g_ret_sts_unexp_error;
            x_errormsg := SQLERRM;
      END;

      RETURN (l_NTO_num_site);
   END xxocm_NTO_number;



   /* Created by Steve Gribbin                         */
   /*                                                  */
   /* This function calls the c_get_nto_data cursor    */
   /* defined above, returning the NTO Job Total       */
   /*                                              */
   FUNCTION xxocm_NTO_job_total (x_resultout      OUT NOCOPY VARCHAR2
                               , x_errormsg       OUT NOCOPY VARCHAR2)
      RETURN VARCHAR2
   IS
      l_NTO_job_total_site   VARCHAR2 (30) := NULL;
   BEGIN
      x_resultout := fnd_api.g_ret_sts_success;
      --l_site_use_id  :=  TO_NUMBER (ocm_add_data_points.pg_ocm_add_dp_param_rec.p_site_use_id);
      l_cust_site_use_id :=
         ocm_add_data_points.pg_ocm_add_dp_param_rec.p_site_use_id;

      BEGIN
         OPEN c_get_NTO_data;

         FETCH c_get_NTO_data INTO vget_NTO_data;

         CLOSE c_get_NTO_data;

         l_NTO_job_total_site := NVL (vget_NTO_data.attribute18, '0');
      EXCEPTION
         WHEN NO_DATA_FOUND
         THEN
            l_NTO_job_total_site := 0;
         WHEN OTHERS
         THEN
            x_resultout := fnd_api.g_ret_sts_unexp_error;
            x_errormsg := SQLERRM;
      END;

      RETURN (l_NTO_job_total_site);
   END xxocm_NTO_job_total;



   /* Created by Steve Gribbin                         */
   /*                                                  */
   /* This function retrieves salesrep from the        */
   /* order site level                                 */


   FUNCTION xxocm_AM_site (x_resultout      OUT NOCOPY VARCHAR2
                         , x_errormsg       OUT NOCOPY VARCHAR2)
      RETURN VARCHAR2
   IS
      l_AM_site    VARCHAR2 (30) := NULL;
      l_result     VARCHAR2 (100) := NULL;
      l_errormsg   VARCHAR2 (100) := NULL;
   BEGIN
      x_resultout := fnd_api.g_ret_sts_success;
      --l_site_use_id := TO_NUMBER (ocm_add_data_points.pg_ocm_add_dp_param_rec.p_site_use_id);
      l_site_use_id :=
         ocm_add_data_points.pg_ocm_add_dp_param_rec.p_site_use_id;

      SELECT name
        INTO l_AM_site
        FROM ra_salesreps
       WHERE salesrep_id = (SELECT primary_salesrep_id
                              FROM hz_cust_site_uses
                             WHERE site_use_id = l_site_use_id);

      RETURN (l_AM_site);
   EXCEPTION
      WHEN NO_DATA_FOUND
      THEN
         l_AM_site := NULL;
         RETURN (l_AM_site);
      WHEN OTHERS
      THEN
         x_resultout := fnd_api.g_ret_sts_unexp_error;
         x_errormsg := SQLERRM;
         RETURN (l_AM_site);
   END xxocm_AM_site;



   /* Created by Steve Gribbin                         */
   /*                                                  */
   /* This function retrieves total A/R at the         */
   /* site level                                    */



   FUNCTION xxocm_total_ar_site (x_resultout      OUT NOCOPY VARCHAR2
                               , x_errormsg       OUT NOCOPY VARCHAR2)
      RETURN NUMBER
   IS
      l_total_ar_site   NUMBER (30) := NULL;
      l_result          VARCHAR2 (100) := NULL;
      l_errormsg        VARCHAR2 (100) := NULL;
   BEGIN
      x_resultout := fnd_api.g_ret_sts_success;

      l_site_use_id :=
         ocm_add_data_points.pg_ocm_add_dp_param_rec.p_site_use_id;

      SELECT ROUND (SUM (NVL (ps.acctd_amount_due_remaining, 0)), 2)
        INTO l_total_ar_site
        FROM ar_payment_schedules ps
       WHERE ps.customer_site_use_id = l_site_use_id;

      --GROUP BY ps.customer_site_use_id;
      RETURN (NVL (l_total_ar_site, 0));
   EXCEPTION
      WHEN NO_DATA_FOUND
      THEN
         l_total_ar_site := 0;
         RETURN (l_total_ar_site);
      WHEN OTHERS
      THEN
         x_resultout := fnd_api.g_ret_sts_unexp_error;
         x_errormsg := SQLERRM;
         RETURN (NVL (l_total_ar_site, 0));
   END xxocm_total_ar_site;


   /* Created by Steve Gribbin                         */
   /*                                                  */
   /* This function retrieves current A/R at the       */
   /* site level                                       */

   FUNCTION xxocm_current_site (x_resultout      OUT NOCOPY VARCHAR2
                              , x_errormsg       OUT NOCOPY VARCHAR2)
      RETURN NUMBER
   IS
      l_current_site   NUMBER (30) := NULL;
      l_result         VARCHAR2 (100) := NULL;
      l_errormsg       VARCHAR2 (100) := NULL;
   BEGIN
      x_resultout := fnd_api.g_ret_sts_success;

      l_site_use_id :=
         ocm_add_data_points.pg_ocm_add_dp_param_rec.p_site_use_id;

      SELECT ROUND (SUM (NVL (ps.acctd_amount_due_remaining, 0)), 2)
        INTO l_current_site
        FROM ar_payment_schedules ps
       WHERE ps.customer_site_use_id = l_site_use_id
             AND ps.due_date > (SYSDATE);

      RETURN (NVL (l_current_site, 0));
   EXCEPTION
      WHEN NO_DATA_FOUND
      THEN
         l_current_site := 0;
         RETURN (l_current_site);
      WHEN OTHERS
      THEN
         x_resultout := fnd_api.g_ret_sts_unexp_error;
         x_errormsg := SQLERRM;
         RETURN (NVL (l_current_site, 0));
   END xxocm_current_site;



   /* Created by Steve Gribbin                         */
   /*                                                  */
   /* This function retrieves 1-30 A/R at the          */
   /* site level                                       */

   FUNCTION xxocm_1_to_30_site (x_resultout      OUT NOCOPY VARCHAR2
                              , x_errormsg       OUT NOCOPY VARCHAR2)
      RETURN NUMBER
   IS
      l_1_to_30_site   NUMBER (30) := NULL;
      l_result         VARCHAR2 (100) := NULL;
      l_errormsg       VARCHAR2 (100) := NULL;
   BEGIN
      x_resultout := fnd_api.g_ret_sts_success;

      l_site_use_id :=
         ocm_add_data_points.pg_ocm_add_dp_param_rec.p_site_use_id;

      SELECT ROUND (SUM (NVL (ps.acctd_amount_due_remaining, 0)), 2)
        INTO l_1_to_30_site
        FROM ar_payment_schedules ps
       WHERE ps.customer_site_use_id = l_site_use_id
             AND ps.due_date BETWEEN SYSDATE - 30 AND SYSDATE;

      RETURN (NVL (l_1_to_30_site, 0));
   EXCEPTION
      WHEN NO_DATA_FOUND
      THEN
         l_1_to_30_site := 0;
         RETURN (l_1_to_30_site);
      WHEN OTHERS
      THEN
         x_resultout := fnd_api.g_ret_sts_unexp_error;
         x_errormsg := SQLERRM;
         RETURN (NVL (l_1_to_30_site, 0));
   END xxocm_1_to_30_site;

   /* Created by Steve Gribbin                         */
   /*                                                  */
   /* This function retrieves 31-60 A/R at the         */
   /* site level                                       */

   FUNCTION xxocm_31_to_60_site (x_resultout      OUT NOCOPY VARCHAR2
                               , x_errormsg       OUT NOCOPY VARCHAR2)
      RETURN NUMBER
   IS
      l_31_to_60_site   NUMBER (30) := NULL;
      l_result          VARCHAR2 (100) := NULL;
      l_errormsg        VARCHAR2 (100) := NULL;
   BEGIN
      x_resultout := fnd_api.g_ret_sts_success;

      l_site_use_id :=
         ocm_add_data_points.pg_ocm_add_dp_param_rec.p_site_use_id;

      SELECT ROUND (SUM (NVL (ps.acctd_amount_due_remaining, 0)), 2)
        INTO l_31_to_60_site
        FROM ar_payment_schedules ps
       WHERE ps.customer_site_use_id = l_site_use_id
             AND ps.due_date BETWEEN (SYSDATE - 60) AND (SYSDATE - 30);

      RETURN (NVL (l_31_to_60_site, 0));
   EXCEPTION
      WHEN NO_DATA_FOUND
      THEN
         l_31_to_60_site := 0;
         RETURN (l_31_to_60_site);
      WHEN OTHERS
      THEN
         x_resultout := fnd_api.g_ret_sts_unexp_error;
         x_errormsg := SQLERRM;
         RETURN (NVL (l_31_to_60_site, 0));
   END xxocm_31_to_60_site;


   /* Created by Steve Gribbin                         */
   /*                                                  */
   /* This function retrieves 61-90 A/R at the         */
   /* site level                                       */

   FUNCTION xxocm_61_to_90_site (x_resultout      OUT NOCOPY VARCHAR2
                               , x_errormsg       OUT NOCOPY VARCHAR2)
      RETURN NUMBER
   IS
      l_61_to_90_site   NUMBER (30) := NULL;
      l_result          VARCHAR2 (100) := NULL;
      l_errormsg        VARCHAR2 (100) := NULL;
   BEGIN
      x_resultout := fnd_api.g_ret_sts_success;

      l_site_use_id :=
         ocm_add_data_points.pg_ocm_add_dp_param_rec.p_site_use_id;

      SELECT ROUND (SUM (NVL (ps.acctd_amount_due_remaining, 0)), 2)
        INTO l_61_to_90_site
        FROM ar_payment_schedules ps
       WHERE ps.customer_site_use_id = l_site_use_id
             AND ps.due_date BETWEEN (SYSDATE - 90) AND (SYSDATE - 60);

      RETURN (NVL (l_61_to_90_site, 0));
   EXCEPTION
      WHEN NO_DATA_FOUND
      THEN
         l_61_to_90_site := 0;
         RETURN (l_61_to_90_site);
      WHEN OTHERS
      THEN
         x_resultout := fnd_api.g_ret_sts_unexp_error;
         x_errormsg := SQLERRM;
         RETURN (NVL (l_61_to_90_site, 0));
   END xxocm_61_to_90_site;


   /* Created by Steve Gribbin                         */
   /*                                                  */
   /* This function retrieves 91-180 A/R at the         */
   /* site level                                       */

   FUNCTION xxocm_91_to_180_site (x_resultout      OUT NOCOPY VARCHAR2
                                , x_errormsg       OUT NOCOPY VARCHAR2)
      RETURN NUMBER
   IS
      l_91_to_180_site   NUMBER (30) := NULL;
      l_result           VARCHAR2 (100) := NULL;
      l_errormsg         VARCHAR2 (100) := NULL;
   BEGIN
      x_resultout := fnd_api.g_ret_sts_success;

      l_site_use_id :=
         ocm_add_data_points.pg_ocm_add_dp_param_rec.p_site_use_id;

      SELECT ROUND (SUM (NVL (ps.acctd_amount_due_remaining, 0)), 2)
        INTO l_91_to_180_site
        FROM ar_payment_schedules ps
       WHERE ps.customer_site_use_id = l_site_use_id
             AND ps.due_date BETWEEN (SYSDATE - 180) AND (SYSDATE - 90);

      RETURN (NVL (l_91_to_180_site, 0));
   EXCEPTION
      WHEN NO_DATA_FOUND
      THEN
         l_91_to_180_site := 0;
         RETURN (l_91_to_180_site);
      WHEN OTHERS
      THEN
         x_resultout := fnd_api.g_ret_sts_unexp_error;
         x_errormsg := SQLERRM;
         RETURN (NVL (l_91_to_180_site, 0));
   END xxocm_91_to_180_site;



   /* Created by Steve Gribbin                         */
   /*                                                  */
   /* This function retrieves 181-360 A/R at the       */
   /* site level                                       */

   FUNCTION xxocm_181_to_360_site (x_resultout      OUT NOCOPY VARCHAR2
                                 , x_errormsg       OUT NOCOPY VARCHAR2)
      RETURN NUMBER
   IS
      l_181_to_360_site   NUMBER (30) := NULL;
      l_result            VARCHAR2 (100) := NULL;
      l_errormsg          VARCHAR2 (100) := NULL;
   BEGIN
      x_resultout := fnd_api.g_ret_sts_success;

      l_site_use_id :=
         ocm_add_data_points.pg_ocm_add_dp_param_rec.p_site_use_id;

      SELECT ROUND (SUM (NVL (ps.acctd_amount_due_remaining, 0)), 2)
        INTO l_181_to_360_site
        FROM ar_payment_schedules ps
       WHERE ps.customer_site_use_id = l_site_use_id
             AND ps.due_date BETWEEN (SYSDATE - 360) AND (SYSDATE - 180);

      RETURN (NVL (l_181_to_360_site, 0));
   EXCEPTION
      WHEN NO_DATA_FOUND
      THEN
         l_181_to_360_site := 0;
         RETURN (l_181_to_360_site);
      WHEN OTHERS
      THEN
         x_resultout := fnd_api.g_ret_sts_unexp_error;
         x_errormsg := SQLERRM;
         RETURN (NVL (l_181_to_360_site, 0));
   END xxocm_181_to_360_site;

   /* Created by Steve Gribbin                         */
   /*                                                  */
   /* This function retrieves 360 plus A/R at the      */
   /* site level                                       */

   FUNCTION xxocm_360_plus_site (x_resultout      OUT NOCOPY VARCHAR2
                               , x_errormsg       OUT NOCOPY VARCHAR2)
      RETURN NUMBER
   IS
      l_360_plus_site   NUMBER (30) := NULL;
      l_result          VARCHAR2 (100) := NULL;
      l_errormsg        VARCHAR2 (100) := NULL;
   BEGIN
      x_resultout := fnd_api.g_ret_sts_success;
      l_party_id := ocm_add_data_points.pg_ocm_add_dp_param_rec.p_party_id;
      l_cust_account_id :=
         ocm_add_data_points.pg_ocm_add_dp_param_rec.p_cust_account_id;
      l_site_use_id :=
         ocm_add_data_points.pg_ocm_add_dp_param_rec.p_site_use_id;

      SELECT ROUND (SUM (NVL (ps.acctd_amount_due_remaining, 0)), 2)
        INTO l_360_plus_site
        FROM ar_payment_schedules ps
       WHERE ps.customer_site_use_id = l_site_use_id
             AND ps.due_date < (SYSDATE - 360);

      --GROUP BY ps.customer_site_use_id;
      RETURN (NVL (l_360_plus_site, 0));
   EXCEPTION
      WHEN NO_DATA_FOUND
      THEN
         l_360_plus_site := 0;
         RETURN (l_360_plus_site);
      WHEN OTHERS
      THEN
         x_resultout := fnd_api.g_ret_sts_unexp_error;
         x_errormsg := SQLERRM;
         RETURN (NVL (l_360_plus_site, 0));
   END xxocm_360_plus_site;



   /* Created by Steve Gribbin                         */
   /*                                                  */
   /* This function retrieves total A/R at the         */
   /* accountlevel                                    */


   FUNCTION xxocm_total_ar_acct (x_resultout      OUT NOCOPY VARCHAR2
                               , x_errormsg       OUT NOCOPY VARCHAR2)
      RETURN NUMBER
   IS
      l_total_ar_acct   NUMBER (30) := NULL;
      l_result          VARCHAR2 (100) := NULL;
      l_errormsg        VARCHAR2 (100) := NULL;
   BEGIN
      x_resultout := fnd_api.g_ret_sts_success;

      l_cust_account_id :=
         ocm_add_data_points.pg_ocm_add_dp_param_rec.p_cust_account_id;

      SELECT ROUND (SUM (NVL (ps.acctd_amount_due_remaining, 0)), 2)
        INTO l_total_ar_acct
        FROM ar_payment_schedules ps
       WHERE CUSTOMER_ID = l_cust_account_id; -- AND customer_site_use_id IS NULL;

      RETURN (NVL (l_total_ar_acct, 0));
   EXCEPTION
      WHEN NO_DATA_FOUND
      THEN
         l_total_ar_acct := 0;
         RETURN (l_total_ar_acct);
      WHEN OTHERS
      THEN
         x_resultout := fnd_api.g_ret_sts_unexp_error;
         x_errormsg := SQLERRM;
         RETURN (NVL (l_total_ar_acct, 0));
   END xxocm_total_ar_acct;



   /* Created by Steve Gribbin                         */
   /*                                                  */
   /* This function retrieves current A/R at the       */
   /* accountlevel                                     */

   FUNCTION xxocm_current_acct (x_resultout      OUT NOCOPY VARCHAR2
                              , x_errormsg       OUT NOCOPY VARCHAR2)
      RETURN NUMBER
   IS
      l_current_acct   NUMBER (30) := NULL;
      l_result         VARCHAR2 (100) := NULL;
      l_errormsg       VARCHAR2 (100) := NULL;
   BEGIN
      x_resultout := fnd_api.g_ret_sts_success;

      l_cust_account_id :=
         ocm_add_data_points.pg_ocm_add_dp_param_rec.p_cust_account_id;

      SELECT ROUND (SUM (NVL (ps.acctd_amount_due_remaining, 0)), 2)
        INTO l_current_acct
        FROM ar_payment_schedules ps
       WHERE CUSTOMER_ID = l_cust_account_id --AND customer_site_use_id IS NULL
             AND ps.due_date > (SYSDATE);

      RETURN (NVL (l_current_acct, 0));
   EXCEPTION
      WHEN NO_DATA_FOUND
      THEN
         l_current_acct := 0;
         RETURN (l_current_acct);
      WHEN OTHERS
      THEN
         x_resultout := fnd_api.g_ret_sts_unexp_error;
         x_errormsg := SQLERRM;
         RETURN (NVL (l_current_acct, 0));
   END xxocm_current_acct;



   /* Created by Steve Gribbin                         */
   /*                                                  */
   /* This function retrieves 1-30 A/R at the         */
   /* accountlevel                                    */

   FUNCTION xxocm_1_to_30_acct (x_resultout      OUT NOCOPY VARCHAR2
                              , x_errormsg       OUT NOCOPY VARCHAR2)
      RETURN NUMBER
   IS
      l_1_to_30_acct   NUMBER (30) := NULL;
      l_result         VARCHAR2 (100) := NULL;
      l_errormsg       VARCHAR2 (100) := NULL;
   BEGIN
      x_resultout := fnd_api.g_ret_sts_success;

      l_cust_account_id :=
         ocm_add_data_points.pg_ocm_add_dp_param_rec.p_cust_account_id;

      SELECT ROUND (SUM (NVL (ps.acctd_amount_due_remaining, 0)), 2)
        INTO l_1_to_30_acct
        FROM ar_payment_schedules ps
       WHERE CUSTOMER_ID = l_cust_account_id --AND customer_site_use_id IS NULL
             AND ps.due_date > (SYSDATE - 30) AND ps.due_date <= (SYSDATE);

      RETURN (NVL (l_1_to_30_acct, 0));
   EXCEPTION
      WHEN NO_DATA_FOUND
      THEN
         l_1_to_30_acct := 0;
         RETURN (l_1_to_30_acct);
      WHEN OTHERS
      THEN
         x_resultout := fnd_api.g_ret_sts_unexp_error;
         x_errormsg := SQLERRM;
         RETURN (NVL (l_1_to_30_acct, 0));
   END xxocm_1_to_30_acct;



   /* Created by Steve Gribbin                         */
   /*                                                  */
   /* This function retrieves 31-60 A/R at the         */
   /* accountlevel                                    */

   FUNCTION xxocm_31_to_60_acct (x_resultout      OUT NOCOPY VARCHAR2
                               , x_errormsg       OUT NOCOPY VARCHAR2)
      RETURN NUMBER
   IS
      l_31_to_60_acct   NUMBER (30) := NULL;
      l_result          VARCHAR2 (100) := NULL;
      l_errormsg        VARCHAR2 (100) := NULL;
   BEGIN
      x_resultout := fnd_api.g_ret_sts_success;

      l_cust_account_id :=
         ocm_add_data_points.pg_ocm_add_dp_param_rec.p_cust_account_id;

      SELECT ROUND (SUM (NVL (ps.acctd_amount_due_remaining, 0)), 2)
        INTO l_31_to_60_acct
        FROM ar_payment_schedules ps
       WHERE     CUSTOMER_ID = l_cust_account_id
             --AND customer_site_use_id IS NULL
             AND ps.due_date > (SYSDATE - 60)
             AND ps.due_date <= (SYSDATE - 30);

      RETURN (NVL (l_31_to_60_acct, 0));
   EXCEPTION
      WHEN NO_DATA_FOUND
      THEN
         l_31_to_60_acct := 0;
         RETURN (l_31_to_60_acct);
      WHEN OTHERS
      THEN
         x_resultout := fnd_api.g_ret_sts_unexp_error;
         x_errormsg := SQLERRM;
         RETURN (NVL (l_31_to_60_acct, 0));
   END xxocm_31_to_60_acct;



   /* Created by Steve Gribbin                         */
   /*                                                  */
   /* This function retrieves 61-90 A/R at the         */
   /* accountlevel                                    */

   FUNCTION xxocm_61_to_90_acct (x_resultout      OUT NOCOPY VARCHAR2
                               , x_errormsg       OUT NOCOPY VARCHAR2)
      RETURN NUMBER
   IS
      l_61_to_90_acct   NUMBER (30) := NULL;
      l_result          VARCHAR2 (100) := NULL;
      l_errormsg        VARCHAR2 (100) := NULL;
   BEGIN
      x_resultout := fnd_api.g_ret_sts_success;

      l_cust_account_id :=
         ocm_add_data_points.pg_ocm_add_dp_param_rec.p_cust_account_id;

      SELECT ROUND (SUM (NVL (ps.acctd_amount_due_remaining, 0)), 2)
        INTO l_61_to_90_acct
        FROM ar_payment_schedules ps
       WHERE     CUSTOMER_ID = l_cust_account_id
             --AND customer_site_use_id IS NULL
             AND ps.due_date > (SYSDATE - 90)
             AND ps.due_date <= (SYSDATE - 60);

      RETURN (NVL (l_61_to_90_acct, 0));
   EXCEPTION
      WHEN NO_DATA_FOUND
      THEN
         l_61_to_90_acct := 0;
         RETURN (l_61_to_90_acct);
      WHEN OTHERS
      THEN
         x_resultout := fnd_api.g_ret_sts_unexp_error;
         x_errormsg := SQLERRM;
         RETURN (NVL (l_61_to_90_acct, 0));
   END xxocm_61_to_90_acct;

   /* Created by Steve Gribbin                         */
   /*                                                  */
   /* This function retrieves 91-180 A/R at the        */
   /* accountlevel                                     */

   FUNCTION xxocm_91_to_180_acct (x_resultout      OUT NOCOPY VARCHAR2
                                , x_errormsg       OUT NOCOPY VARCHAR2)
      RETURN NUMBER
   IS
      l_91_to_180_acct   NUMBER (30) := NULL;
      l_result           VARCHAR2 (100) := NULL;
      l_errormsg         VARCHAR2 (100) := NULL;
   BEGIN
      x_resultout := fnd_api.g_ret_sts_success;

      l_cust_account_id :=
         ocm_add_data_points.pg_ocm_add_dp_param_rec.p_cust_account_id;

      SELECT ROUND (SUM (NVL (ps.acctd_amount_due_remaining, 0)), 2)
        INTO l_91_to_180_acct
        FROM ar_payment_schedules ps
       WHERE     CUSTOMER_ID = l_cust_account_id
             --AND customer_site_use_id IS NULL
             AND ps.due_date > (SYSDATE - 180)
             AND ps.due_date <= (SYSDATE - 90);

      RETURN (NVL (l_91_to_180_acct, 0));
   EXCEPTION
      WHEN NO_DATA_FOUND
      THEN
         l_91_to_180_acct := 0;
         RETURN (l_91_to_180_acct);
      WHEN OTHERS
      THEN
         x_resultout := fnd_api.g_ret_sts_unexp_error;
         x_errormsg := SQLERRM;
         RETURN (NVL (l_91_to_180_acct, 0));
   END xxocm_91_to_180_acct;



   /* Created by Steve Gribbin                         */
   /*                                                  */
   /* This function retrieves 181-360 A/R at the       */
   /* accountlevel                                     */

   FUNCTION xxocm_181_to_360_acct (x_resultout      OUT NOCOPY VARCHAR2
                                 , x_errormsg       OUT NOCOPY VARCHAR2)
      RETURN NUMBER
   IS
      l_181_to_360_acct   NUMBER (30) := NULL;
      l_result            VARCHAR2 (100) := NULL;
      l_errormsg          VARCHAR2 (100) := NULL;
   BEGIN
      x_resultout := fnd_api.g_ret_sts_success;

      l_cust_account_id :=
         ocm_add_data_points.pg_ocm_add_dp_param_rec.p_cust_account_id;

      SELECT ROUND (SUM (NVL (ps.acctd_amount_due_remaining, 0)), 2)
        INTO l_181_to_360_acct
        FROM ar_payment_schedules ps
       WHERE     CUSTOMER_ID = l_cust_account_id
             --AND customer_site_use_id IS NULL
             AND ps.due_date > (SYSDATE - 360)
             AND ps.due_date <= (SYSDATE - 180);

      RETURN (NVL (l_181_to_360_acct, 0));
   EXCEPTION
      WHEN NO_DATA_FOUND
      THEN
         l_181_to_360_acct := NULL;
         RETURN (l_181_to_360_acct);
      WHEN OTHERS
      THEN
         x_resultout := fnd_api.g_ret_sts_unexp_error;
         x_errormsg := SQLERRM;
         RETURN (NVL (l_181_to_360_acct, 0));
   END xxocm_181_to_360_acct;



   /* Created by Steve Gribbin                         */
   /*                                                  */
   /* This function retrieves 360 plus A/R at the      */
   /* accountlevel                                     */

   FUNCTION xxocm_360_plus_acct (x_resultout      OUT NOCOPY VARCHAR2
                               , x_errormsg       OUT NOCOPY VARCHAR2)
      RETURN NUMBER
   IS
      l_360_plus_acct   NUMBER (30) := NULL;
      l_result          VARCHAR2 (100) := NULL;
      l_errormsg        VARCHAR2 (100) := NULL;
   BEGIN
      x_resultout := fnd_api.g_ret_sts_success;

      l_cust_account_id :=
         ocm_add_data_points.pg_ocm_add_dp_param_rec.p_cust_account_id;

      SELECT ROUND (SUM (NVL (ps.acctd_amount_due_remaining, 0)), 2)
        INTO l_360_plus_acct
        FROM ar_payment_schedules ps
       WHERE CUSTOMER_ID = l_cust_account_id --AND customer_site_use_id IS NULL
             AND ps.due_date <= (SYSDATE - 360);

      RETURN (NVL (l_360_plus_acct, 0));
   EXCEPTION
      WHEN NO_DATA_FOUND
      THEN
         l_360_plus_acct := 0;
         RETURN (l_360_plus_acct);
      WHEN OTHERS
      THEN
         x_resultout := fnd_api.g_ret_sts_unexp_error;
         x_errormsg := SQLERRM;
         RETURN (NVL (l_360_plus_acct, 0));
   END xxocm_360_plus_acct;



   /* Created by Steve Gribbin                         */
   /*                                                  */
   /* This function retrieves start date from        */
   /* account level                                 */


   FUNCTION xxocm_cust_since_acct (x_resultout      OUT NOCOPY VARCHAR2
                                 , x_errormsg       OUT NOCOPY VARCHAR2)
      RETURN VARCHAR2
   IS
      l_cust_since_acct   VARCHAR2 (30) := NULL;
      l_result            VARCHAR2 (100) := NULL;
      l_errormsg          VARCHAR2 (100) := NULL;
   BEGIN
      x_resultout := fnd_api.g_ret_sts_success;

      l_cust_account_id :=
         ocm_add_data_points.pg_ocm_add_dp_param_rec.p_cust_account_id;

      SELECT account_established_date
        INTO l_cust_since_acct
        FROM hz_cust_accounts
       WHERE cust_account_id = l_cust_account_id;

      RETURN (l_cust_since_acct);
   EXCEPTION
      WHEN NO_DATA_FOUND
      THEN
         l_cust_since_acct := NULL;
         RETURN (l_cust_since_acct);
      WHEN OTHERS
      THEN
         x_resultout := fnd_api.g_ret_sts_unexp_error;
         x_errormsg := SQLERRM;
         RETURN (l_cust_since_acct);
   END xxocm_cust_since_acct;

   /* Created by Shankar Hariharan                     */
   /*                                                  */
   /* This function retrieves location of credit folder*/

   FUNCTION xxocm_location (x_resultout      OUT NOCOPY VARCHAR2
                          , x_errormsg       OUT NOCOPY VARCHAR2)
      RETURN VARCHAR2
   IS
      --l_site_use_id   NUMBER (30) := NULL;
      l_location      VARCHAR2 (40);
      l_result        VARCHAR2 (100) := NULL;
      l_errormsg      VARCHAR2 (100) := NULL;
   BEGIN
      x_resultout := fnd_api.g_ret_sts_success;

      l_site_use_id :=
         ocm_add_data_points.pg_ocm_add_dp_param_rec.p_site_use_id;

      SELECT location
        INTO l_location
        FROM hz_cust_site_uses
       WHERE site_use_id = l_site_use_id;

      RETURN (l_location);
   EXCEPTION
      WHEN NO_DATA_FOUND
      THEN
         RETURN (l_location);
      WHEN OTHERS
      THEN
         x_resultout := fnd_api.g_ret_sts_unexp_error;
         x_errormsg := SQLERRM;
         RETURN (l_location);
   END xxocm_location;

   /* Created by Shankar Hariharan                     */
   /*                                                  */
   /* This function retrieves site number              */


   FUNCTION xxocm_site_number (x_resultout      OUT NOCOPY VARCHAR2
                             , x_errormsg       OUT NOCOPY VARCHAR2)
      RETURN VARCHAR2
   IS
      --l_site_use_id   NUMBER (30) := NULL;
      l_site_number   VARCHAR2 (40);
      l_result        VARCHAR2 (100) := NULL;
      l_errormsg      VARCHAR2 (100) := NULL;
   BEGIN
      x_resultout := fnd_api.g_ret_sts_success;

      l_site_use_id :=
         ocm_add_data_points.pg_ocm_add_dp_param_rec.p_site_use_id;

      SELECT party_site_number
        INTO l_site_number
        FROM hz_cust_site_uses a
           , hz_cust_acct_sites b
           , hz_party_sites c
       WHERE     a.site_use_id = l_site_use_id
             AND a.cust_acct_site_id = b.cust_acct_site_id
             AND b.party_site_id = c.party_site_id;

      RETURN (l_site_number);
   EXCEPTION
      WHEN NO_DATA_FOUND
      THEN
         RETURN (l_site_number);
      WHEN OTHERS
      THEN
         x_resultout := fnd_api.g_ret_sts_unexp_error;
         x_errormsg := SQLERRM;
         RETURN (l_site_number);
   END xxocm_site_number;

   /* Created by Shankar Hariharan                     */
   /*                                                  */
   /* This function retrieves requestor of credit folder*/


   FUNCTION xxocm_requestor (x_resultout      OUT NOCOPY VARCHAR2
                           , x_errormsg       OUT NOCOPY VARCHAR2)
      RETURN VARCHAR2
   IS
      l_requestor_id   NUMBER (30) := NULL;
      l_requestor      VARCHAR2 (100);
      l_result         VARCHAR2 (100) := NULL;
      l_errormsg       VARCHAR2 (100) := NULL;
   BEGIN
      x_resultout := fnd_api.g_ret_sts_success;

      l_requestor_id :=
         ocm_add_data_points.pg_ocm_add_dp_param_rec.p_requestor_id;

      SELECT SUBSTR (full_name, 1, 100)
        INTO l_requestor
        FROM per_people_f
       WHERE person_id = l_requestor_id AND ROWNUM = 1;

      RETURN (l_requestor);
   EXCEPTION
      WHEN NO_DATA_FOUND
      THEN
         RETURN (l_requestor);
      WHEN OTHERS
      THEN
         x_resultout := fnd_api.g_ret_sts_unexp_error;
         x_errormsg := SQLERRM;
         RETURN (l_requestor);
   END xxocm_requestor;

   /* Created by Shankar Hariharan                     */
   /*                                                  */
   /* This function retrieves last payment date  at    */
   /* customer site level                              */

   FUNCTION xxocm_last_pmt_date_site (x_resultout      OUT NOCOPY VARCHAR2
                                    , x_errormsg       OUT NOCOPY VARCHAR2)
      RETURN VARCHAR2
   IS
      l_pmt_date      VARCHAR2(30) ;
      l_result        VARCHAR2 (100) := NULL;
      l_errormsg      VARCHAR2 (100) := NULL;
   BEGIN
      x_resultout := fnd_api.g_ret_sts_success;

      l_site_use_id :=
         ocm_add_data_points.pg_ocm_add_dp_param_rec.p_site_use_id;
         
      l_cust_account_id :=
         ocm_add_data_points.pg_ocm_add_dp_param_rec.p_cust_account_id;
   

      SELECT to_char(last_payment_date,'DD-MON-YYYY')
        INTO l_pmt_date
        FROM ar_trx_bal_summary
       WHERE site_use_id = l_site_use_id
         AND cust_account_id=l_cust_account_id; --added by Shankar to prevent too many rows error 01/04/2013

      RETURN (l_pmt_date);
   EXCEPTION
      WHEN NO_DATA_FOUND
      THEN
         RETURN (l_pmt_date);
      WHEN OTHERS
      THEN
         x_resultout := fnd_api.g_ret_sts_unexp_error;
         x_errormsg := SQLERRM;
         RETURN (l_pmt_date);
   END xxocm_last_pmt_date_site;

   /* Created by Shankar Hariharan                     */
   /*                                                  */
   /* This function retrieves last payment amount  at  */
   /* customer site level                              */

   FUNCTION xxocm_last_pmt_amt_site (x_resultout      OUT NOCOPY VARCHAR2
                                   , x_errormsg       OUT NOCOPY VARCHAR2)
      RETURN NUMBER
   IS
      --l_site_use_id   NUMBER (30) := NULL;
      l_pmt_amount    NUMBER  := 0;
      l_result        VARCHAR2 (100) := NULL;
      l_errormsg      VARCHAR2 (100) := NULL;
   BEGIN
      x_resultout := fnd_api.g_ret_sts_success;

      l_site_use_id :=
         ocm_add_data_points.pg_ocm_add_dp_param_rec.p_site_use_id;
         
       l_cust_account_id :=
         ocm_add_data_points.pg_ocm_add_dp_param_rec.p_cust_account_id;
   

      SELECT abs(last_payment_amount)
        INTO l_pmt_amount
        FROM ar_trx_bal_summary
       WHERE site_use_id = l_site_use_id
         AND cust_account_id=l_cust_account_id; --added by Shankar to prevent too many rows error 01/04/2013

      RETURN (l_pmt_amount);
   EXCEPTION
      WHEN NO_DATA_FOUND
      THEN
         RETURN (l_pmt_amount);
      WHEN OTHERS
      THEN
         x_resultout := fnd_api.g_ret_sts_unexp_error;
         x_errormsg := SQLERRM;
         RETURN (l_pmt_amount);
   END xxocm_last_pmt_amt_site;

   /* Created by Shankar Hariharan                     */
   /*                                                  */
   /* This function retrieves last payment date  at    */
   /* customer level                                   */

   FUNCTION xxocm_last_pmt_date_acct (x_resultout      OUT NOCOPY VARCHAR2
                                    , x_errormsg       OUT NOCOPY VARCHAR2)
      RETURN varchar2
   IS
      l_pmt_date       VARCHAR2(30) ;
      l_result         VARCHAR2 (100) := NULL;
      l_errormsg       VARCHAR2 (100) := NULL;
   BEGIN
      x_resultout := fnd_api.g_ret_sts_success;

      l_cust_account_id :=
         ocm_add_data_points.pg_ocm_add_dp_param_rec.p_cust_account_id;


      --SELECT to_char(nvl(max(last_payment_date),to_date('01-01-2000','DD-MM-YYYY')),'DD-MON-YYYY')
      SELECT to_char(max(last_payment_date),'DD-MON-YYYY')
        INTO l_pmt_date
        FROM ar_trx_bal_summary
       WHERE cust_account_id = l_cust_account_id;

      RETURN (l_pmt_date);
   EXCEPTION
      WHEN NO_DATA_FOUND
      THEN
         RETURN (l_pmt_date);
      WHEN OTHERS
      THEN
         x_resultout := fnd_api.g_ret_sts_unexp_error;
         x_errormsg := SQLERRM;
         RETURN (l_pmt_date);
   END xxocm_last_pmt_date_acct;

   /* Created by Shankar Hariharan                     */
   /*                                                  */
   /* This function retrieves last payment amount  at  */
   /* customer level                                   */

   FUNCTION xxocm_last_pmt_amt_acct (x_resultout      OUT NOCOPY VARCHAR2
                                   , x_errormsg       OUT NOCOPY VARCHAR2)
      RETURN NUMBER
   IS
      --l_cust_acct_id   NUMBER (30) := NULL;
      l_pmt_amount     NUMBER :=0;
      l_result         VARCHAR2 (100) := NULL;
      l_errormsg       VARCHAR2 (100) := NULL;
   BEGIN
      x_resultout := fnd_api.g_ret_sts_success;

      l_cust_account_id :=
         ocm_add_data_points.pg_ocm_add_dp_param_rec.p_cust_account_id;

      SELECT abs(nvl(last_payment_amount,0))
        INTO l_pmt_amount
        FROM ar_trx_bal_summary
       WHERE cust_account_id = l_cust_account_id
         AND last_payment_date in ( SELECT nvl(max(last_payment_date),to_date('01-01-2000','DD-MM-YYYY'))
                                      FROM ar_trx_bal_summary
                                     WHERE cust_account_id = l_cust_account_id)
         AND rownum=1;

      RETURN (l_pmt_amount);
   EXCEPTION
      WHEN NO_DATA_FOUND
      THEN
         RETURN (l_pmt_amount);
      WHEN OTHERS
      THEN
         x_resultout := fnd_api.g_ret_sts_unexp_error;
         x_errormsg := SQLERRM;
         RETURN (l_pmt_amount);
   END xxocm_last_pmt_amt_acct;
   
   /* Created by Shankar Hariharan                     */
   /*                                                  */
   /* This function retrieves DSO  at                  */
   /* customer level                              */
   
   FUNCTION xxocm_dso_acct (x_resultout      OUT NOCOPY VARCHAR2
                                   , x_errormsg       OUT NOCOPY VARCHAR2)
      RETURN NUMBER
   IS
      --l_cust_acct_id   NUMBER (30) := NULL;
      l_ttm_amount     NUMBER;
      l_due_amount     NUMBER;
      l_dso            NUMBER :=0;
      l_result         VARCHAR2 (100) := NULL;
      l_errormsg       VARCHAR2 (100) := NULL;
   BEGIN
      x_resultout := fnd_api.g_ret_sts_success;

      l_cust_account_id :=
         ocm_add_data_points.pg_ocm_add_dp_param_rec.p_cust_account_id;

      SELECT nvl(sum(amount_due_original),0)
        INTO l_ttm_amount
        FROM ar_payment_schedules
       WHERE customer_id = l_cust_account_id
         AND trunc(sysdate-trx_date)<=90
         AND class<>'PMT';
         
      SELECT nvl(sum(amount_due_remaining),0)
        INTO l_due_amount
        FROM ar_payment_schedules
       WHERE customer_id = l_cust_account_id;
         
       IF l_ttm_amount <> 0 and l_due_amount <> 0 then
         --l_dso := round(365/(l_ttm_amount*4)*l_due_amount,0);
         l_dso := round(l_due_amount/(l_ttm_amount/90),1);
       END IF;

      RETURN (l_dso);
   EXCEPTION
      WHEN NO_DATA_FOUND
      THEN
         RETURN (l_dso);
      WHEN OTHERS
      THEN
         x_resultout := fnd_api.g_ret_sts_unexp_error;
         x_errormsg := SQLERRM;
         RETURN (l_dso);
   END xxocm_dso_acct;

   /* Created by Shankar Hariharan                     */
   /*                                                  */
   /* This function retrieves credit hold status at    */
   /* customer account level                           */
   

   FUNCTION xxocm_acct_credit_hold (x_resultout      OUT NOCOPY VARCHAR2
                                   , x_errormsg       OUT NOCOPY VARCHAR2)
      RETURN VARCHAR2
   IS   
   l_credit_hold varchar2(1);
   BEGIN
     x_resultout := fnd_api.g_ret_sts_success;
     l_cust_account_id := ocm_add_data_points.pg_ocm_add_dp_param_rec.p_cust_account_id;

      SELECT credit_hold
        INTO l_credit_hold
        FROM apps.hz_customer_profiles
       WHERE cust_account_id = l_cust_account_id
         AND site_use_id is null
         AND rownum=1;

      RETURN (l_credit_hold);
   EXCEPTION
      WHEN NO_DATA_FOUND
      THEN
         l_credit_hold := 'Y';
         RETURN (l_credit_hold);
      WHEN OTHERS
      THEN
         x_resultout := fnd_api.g_ret_sts_unexp_error;
         x_errormsg := SQLERRM;
         RETURN (l_credit_hold);
   END xxocm_acct_credit_hold;

   /* Created by Shankar Hariharan                     */
   /*                                                  */
   /* This function retrieves credit hold status at    */
   /* customer site level                              */

   FUNCTION xxocm_site_credit_hold (x_resultout      OUT NOCOPY VARCHAR2
                                   , x_errormsg       OUT NOCOPY VARCHAR2)
      RETURN VARCHAR2 
   IS   
   l_credit_hold varchar2(1);
   BEGIN
     x_resultout := fnd_api.g_ret_sts_success;
     l_site_use_id := ocm_add_data_points.pg_ocm_add_dp_param_rec.p_site_use_id;

      SELECT credit_hold
        INTO l_credit_hold
        FROM apps.hz_customer_profiles
       WHERE site_use_id = l_site_use_id
         AND rownum=1;

      RETURN (l_credit_hold);
   EXCEPTION
      WHEN NO_DATA_FOUND
      THEN
         l_credit_hold := 'Y';
         RETURN (l_credit_hold);
      WHEN OTHERS
      THEN
         x_resultout := fnd_api.g_ret_sts_unexp_error;
         x_errormsg := SQLERRM;
         RETURN (l_credit_hold);
   END xxocm_site_credit_hold;

   /* Created by Shankar Hariharan                     */
   /*                                                  */
   /* This function retrieves credit rating at         */
   /* customer account level  20-May-2013              */
   

   FUNCTION xxocm_credit_rating  (x_resultout      OUT NOCOPY VARCHAR2
                                   , x_errormsg       OUT NOCOPY VARCHAR2)
      RETURN VARCHAR2
   IS   
   l_credit_rating varchar2(30);
   BEGIN
     x_resultout := fnd_api.g_ret_sts_success;
     l_cust_account_id := ocm_add_data_points.pg_ocm_add_dp_param_rec.p_cust_account_id;

      SELECT nvl(credit_rating,'Y')
        INTO l_credit_rating
        FROM apps.hz_customer_profiles
       WHERE cust_account_id = l_cust_account_id
         AND site_use_id is null
         AND rownum=1;

      RETURN (l_credit_rating);
   EXCEPTION
      WHEN NO_DATA_FOUND
      THEN
         l_credit_rating := 'Y';
         RETURN (l_credit_rating);
      WHEN OTHERS
      THEN
         x_resultout := fnd_api.g_ret_sts_unexp_error;
         x_errormsg := SQLERRM;
         RETURN (nvl(l_credit_rating,'Y'));
   END xxocm_credit_rating;

   /* Created by Shankar Hariharan                     */
   /*                                                  */
   /* This function retrieves order total for the order*/
   /* that triggered the credit request  20-May-2013   */
   

   FUNCTION xxocm_order_amount      (x_resultout      OUT NOCOPY VARCHAR2
                                   , x_errormsg       OUT NOCOPY VARCHAR2)
      RETURN NUMBER
   IS   
   l_order_number number;
   l_header_id    number;
   BEGIN
     x_resultout := fnd_api.g_ret_sts_success;
     l_order_number := ocm_add_data_points.pg_ocm_add_dp_param_rec.p_source_column2;

     IF l_order_number is not null then
      SELECT header_id
        INTO l_header_id
        FROM oe_order_headers
       WHERE order_number=l_order_number;
    
      SELECT oe_oe_totals_summary.PRT_ORDER_TOTAL(l_header_id)
        INTO l_order_total
        from dual;

      RETURN (l_order_total);
     ELSE
      RETURN(0);
     END IF; 
   EXCEPTION
      WHEN NO_DATA_FOUND
      THEN
         l_order_total := 0;
         RETURN (l_order_total);
      WHEN OTHERS
      THEN
         x_resultout := fnd_api.g_ret_sts_unexp_error;
         x_errormsg := SQLERRM;
         RETURN (l_order_total);
   END xxocm_order_amount;

/* Ver#1.6  20150819-00054 --- AR - Credit Holds causing issues with billing Rentals*/ --Start
/* This function retrieves the order type for the order */
   FUNCTION xxocm_rental_order_type      (x_resultout    OUT NOCOPY VARCHAR2
                                         ,x_errormsg     OUT NOCOPY VARCHAR2)
     RETURN VARCHAR2
   IS
   l_order_number NUMBER;
   l_order_type   VARCHAR2 (40) := NULL;
   BEGIN
     x_resultout    := fnd_api.g_ret_sts_success;
     l_order_number := ocm_add_data_points.pg_ocm_add_dp_param_rec.p_source_column2;

	   SELECT ott.name
	   INTO   l_order_type
       FROM   oe_order_headers        ooh
       ,      oe_transaction_types_tl ott
	   WHERE  ooh.order_type_id       = ott.transaction_type_id
	   AND    UPPER(ott.name)         LIKE '%RENTAL%'
	   AND    LANGUAGE                = USERENV('LANG')
	   AND    ooh.order_number        = l_order_number;

       RETURN (l_order_type);

   EXCEPTION
      WHEN NO_DATA_FOUND
      THEN
         RETURN (l_order_type);
      WHEN OTHERS
      THEN
         x_resultout := fnd_api.g_ret_sts_unexp_error;
         x_errormsg := SQLERRM;
         RETURN (l_order_type);
   END xxocm_rental_order_type;

/* This function retrieves item name, If the Order have an item �PAYMENT ON ACCOUNT�  */

   FUNCTION xxocm_payment_on_account      (x_resultout    OUT NOCOPY VARCHAR2
                                          ,x_errormsg     OUT NOCOPY VARCHAR2)
      RETURN VARCHAR2
   IS
   l_order_number       NUMBER;
   l_payment_on_account VARCHAR2 (40) := NULL;
   BEGIN
     x_resultout    := fnd_api.g_ret_sts_success;
     l_order_number := ocm_add_data_points.pg_ocm_add_dp_param_rec.p_source_column2;

     SELECT ool.ordered_item
	 INTO   l_payment_on_account
     FROM   oe_order_headers        ooh
     ,      oe_order_lines          ool
     WHERE  ooh.header_id           = ool.header_id
     AND    UPPER(ool.ordered_item) = 'PAYMENT ON ACCOUNT'
	 AND    ooh.order_number        = l_order_number
     GROUP BY ool.ordered_item;

       RETURN (l_payment_on_account);

   EXCEPTION
      WHEN NO_DATA_FOUND
      THEN
         RETURN (l_payment_on_account);
      WHEN OTHERS
      THEN
         x_resultout := fnd_api.g_ret_sts_unexp_error;
         x_errormsg := SQLERRM;
         RETURN (l_payment_on_account);
   END xxocm_payment_on_account;
/* Ver#1.6  20150819-00054 --- AR - Credit Holds causing issues with billing Rentals */  --End
  
END XXWCAR_OCM_ORDR_HLD_DATAPOINTS;
/