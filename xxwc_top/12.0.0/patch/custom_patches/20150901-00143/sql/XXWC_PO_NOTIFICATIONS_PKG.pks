CREATE OR REPLACE
PACKAGE XXWC_PO_NOTIFICATIONS_PKG 
AS
/*************************************************************************
      $Header XXWC_PO_NOTIFICATIONS_PKG.PKG $
      Module Name: XXWC_PO_NOTIFICATIONS_PKG.PKG

      PURPOSE:   This package is used for sending the mail for unapproved PO's
	             and not received PO's

      REVISIONS:
      Ver        Date        Author                     Description
      ---------  ----------  ---------------         -------------------------
      1.0        05/05/2016  Pattabhi Avula          Initial Version TMS#20150901-00143
  ****************************************************************************/


  PROCEDURE unapproved_pos(errbuf OUT VARCHAR2, retcode OUT NUMBER,p_po_num VARCHAR2);
  
  PROCEDURE approved_and_not_received_pos(errbuf OUT VARCHAR2, retcode OUT NUMBER, p_po_num VARCHAR2);

END;
/