create or replace
PACKAGE BODY      XXWC_PO_NOTIFICATIONS_PKG
AS
/*************************************************************************
      $Header XXWC_PO_NOTIFICATIONS_PKG.PKG $
      Module Name: XXWC_PO_NOTIFICATIONS_PKG.PKG

      PURPOSE:   This package is used for sending the mail for unapproved PO's
                 and not received PO's

      REVISIONS:
      Ver        Date        Author                     Description
      ---------  ----------  ---------------         -------------------------
      1.0        05/05/2016  Pattabhi Avula          Initial Version TMS#20150901-00143
  ****************************************************************************/
  
      --------------------------------------------------------------
      -- Global Email Variables
      --------------------------------------------------------------
      g_host                     VARCHAR2 (256)
                                    := 'mailoutrelay.hdsupply.net';
      g_hostport                 VARCHAR2 (20) := '25';
      g_sender                   VARCHAR2 (100) := 'no-reply@whitecap.net';  
            
      --Email Defaults
     g_dflt_email   fnd_user.email_address%TYPE    := 'WC-ITDEVALERTS-U1@HDSupply.com';
      
  /*************************************************************************
      PROCEDURE Name: unapproved_pos

      PURPOSE:   To send the mail notification to buyes for older than 3 
                 days created PO's

      REVISIONS:
      Ver        Date        Author                     Description
      ---------  ----------  ---------------         -------------------------
      1.0        05/05/2015  Pattabhi Avula          Initial Version
   ****************************************************************************/
   
   PROCEDURE unapproved_pos(errbuf OUT VARCHAR2, retcode OUT NUMBER, p_po_num VARCHAR2)
   IS
   
      l_subject                  VARCHAR2 (32767) DEFAULT NULL;
      l_body                     VARCHAR2 (32767) DEFAULT NULL;
      l_body_header              VARCHAR2 (32767) DEFAULT NULL;
      l_body_detail              VARCHAR2 (32767) DEFAULT NULL;
      l_body_footer              VARCHAR2 (32767) DEFAULT NULL;
      l_sec                      VARCHAR2(300);
	  l_count                    NUMBER(10):=0;
	  l_url                      VARCHAR2(200);
      
    CURSOR c1
      IS
       SELECT pha.segment1,
       pha.type_lookup_code,
       aps.vendor_name,
       NVL (pha.authorization_status, 'incomplete') status,
       papf.email_address
  FROM po_headers_all pha,
       ap_suppliers aps,
       per_all_people_f papf
 WHERE  pha.vendor_id=aps.vendor_id
       AND pha.agent_id=papf.person_id
      AND NVL(pha.authorization_status, 'INCOMPLETE') IN ('INCOMPLETE',
                                                        'REJECTED',
                                                      'REQUIRES REAPPROVAL')
       AND pha.type_lookup_code ='STANDARD'
       AND NVL (pha.cancel_flag, 'N') = 'N'
       AND NVL (pha.closed_code, 'OPEN') <> 'FINALLY CLOSED'
       AND pha.org_id=162
       AND pha.segment1=NVL(p_po_num,pha.segment1)
       AND TRUNC(pha.creation_date)<=(TRUNC(SYSDATE) - (SELECT profile_option_value 
                                                        FROM   fnd_profile_option_values fpov,
                                                               fnd_profile_options fpo
                                                        WHERE  fpov.profile_option_id=fpo.profile_option_id
                                                        AND    fpo.profile_option_name='XXWC_UNAPPROVED_PO_PAST_DAYS'));
 
 BEGIN
 l_sec:='Before Loop started';
   BEGIN
    SELECT home_url 
	INTO   l_url
	FROM   icx_parameters;
  EXCEPTION
    WHEN NO_DATA_FOUND THEN
	l_sec:='Failed in get the URL';
	l_url:=NULL;
	WHEN others THEN
	 l_sec:=SUBSTR(SQLERRM,1,50);
	 l_url:=NULL;
  END;
  
   FOR i IN c1 
     LOOP
     
       l_sec := 'Send Email Notification';
                        ------------------------------------------
                        -- Send Email Notification
                        ------------------------------------------
                        l_subject :=
                           ' PO UNAPPROVED, PO# '|| i.segment1
                                                 || ' '
                                                 || i.vendor_name
                                                 ||' Status '
                                                 || i.status
                                                 ||' past 3 days ';
                        l_body_header :=
                           '<style type="text/css">.style1 {font-family: Arial, Helvetica, sans-serif;}</style><p class="style1">';
                        l_body_detail :=
                              ' <BR> Purchase Order# '
                           || i.segment1
                           || ' requires your attention '
                           || CHR (10)
                           || ' Please login to Oracle EBS instance '
                           || l_url
						   || CHR (10)
                           || 'and process the PO.       </BR>';
                        l_body_footer := NULL;
                           
                        l_body :=
                              l_body_header
                           || l_body_detail
                           || CHR (10)
                           || l_body_footer;

                        xxcus_misc_pkg.html_email (
                           p_to              => i.email_address,
                           p_from            => g_sender,
                           p_text            => 'UnApproved PO',
                           p_subject         => l_subject,
                           p_html            => l_body,
                           p_smtp_hostname   => g_host,
                           p_smtp_portnum    => g_hostport);
		l_count:=l_count+1;				   
      l_sec :=' Sending the mail to buyer for PO#: '||i.segment1||' is failed';
   END LOOP;
   fnd_file.put_line(FND_FILE.log,'Total count of mails sent is :'||l_count);
 EXCEPTION  
 WHEN OTHERS
      THEN
        xxcus_error_pkg.xxcus_error_main_api (
          p_called_from         => 'XXWC_PO_NOTIFICATIONS_PKG.unapproved_pos',
          p_calling             => l_sec,
          p_request_id          => fnd_global.conc_request_id,
          p_ora_error_msg       => SQLERRM,
          p_error_desc          => 'Error in unapproved purchase order procedure, When Others Exception',
          p_distribution_list   => g_dflt_email,
          p_module              => 'PO');
END;

/*************************************************************************
      PROCEDURE Name: approved_not_received

      PURPOSE:   To send the mail notification to buyes for Approved but not 
                 received PO's

      REVISIONS:
      Ver        Date        Author                     Description
      ---------  ----------  ---------------         -------------------------
      1.0        05/05/2015  Pattabhi Avula          Initial Version
   ****************************************************************************/
   
   PROCEDURE approved_and_not_received_pos(errbuf OUT VARCHAR2, retcode OUT NUMBER, p_po_num VARCHAR2)
   IS
      
      l_subject                  VARCHAR2 (32767) DEFAULT NULL;
      l_body                     VARCHAR2 (32767) DEFAULT NULL;
      l_body_header              VARCHAR2 (32767) DEFAULT NULL;
      l_body_detail              VARCHAR2 (32767) DEFAULT NULL;
      l_body_footer              VARCHAR2 (32767) DEFAULT NULL;
      l_sec                      VARCHAR2(300);
	  l_count                    NUMBER(10):=0;
	  l_url                      VARCHAR2(200);
      
    CURSOR c1
      IS
       SELECT pha.Segment1,
              NVL (pha.authorization_status, 'INCOMPLETE') status,
              TRUNC(TO_DATE(pha.attribute1,'YYYY/MM/DD HH24:MI:SS')) need_by_date,
              TRUNC(TO_DATE(pha.attribute6,'YYYY/MM/DD HH24:MI:SS')) promise_by_date,
              aps.vendor_name,
              papf.email_address
       FROM   po_headers_all pha,
              ap_suppliers aps,
              per_all_people_f papf
       WHERE  pha.vendor_id=aps.vendor_id
       AND    pha.agent_id=papf.person_id
       AND    TRUNC(SYSDATE) BETWEEN TRUNC(effective_start_date) AND TRUNC(effective_end_date)       
       AND    pha.authorization_status='APPROVED'
       AND    pha.type_lookup_code ='STANDARD'
       AND    NVL (pha.cancel_flag, 'N') = 'N'
       AND    NVL (pha.closed_code, 'OPEN') NOT IN ('FINALLY CLOSED','CLOSED')
       AND    pha.segment1=NVL(p_po_num,pha.segment1)	   
       AND    TRUNC(TO_DATE(NVL(pha.attribute6,pha.attribute1),'YYYY/MM/DD HH24:MI:SS')) <= (TRUNC(SYSDATE) - (SELECT  profile_option_value 
                                                                                                              FROM    fnd_profile_option_values fpov,
                                                                                                                      fnd_profile_options fpo
                                                                                                              WHERE   fpov.profile_option_id=fpo.profile_option_id
                                                                                                               AND    fpo.profile_option_name='XXWC_APPROVED_AND_NOT_RECD_PO_PAST_DAYS'))
       AND EXISTS (SELECT  1
                   FROM   po_headers_all phas,
                          po_lines_all pla, 
                          po_line_locations_all plla
                   WHERE  phas.po_header_id=pha.po_header_id
                   AND    phas.po_header_id=pla.po_header_id
                   AND    pla.po_line_id = plla.po_line_id
                   AND    NVL (pla.cancel_flag, 'N') = 'N'
                   AND    NVL (plla.cancel_flag, 'N') = 'N'
                   AND    NVL (pla.closed_code, 'OPEN') NOT IN ('FINALLY CLOSED','CLOSED')
                   AND    NVL (plla.closed_code, 'OPEN') NOT IN ('FINALLY CLOSED','CLOSED FOR RECEIVING','CLOSED')
                   AND    (NVL(plla.quantity,0)+NVL(plla.quantity_cancelled,0))-NVL(plla.quantity_received,0)>0
                   AND    ROWNUM=1);
 
 BEGIN
 l_sec:='Before Loop started';
 
  BEGIN
    SELECT home_url 
	INTO   l_url
	FROM   icx_parameters;
  EXCEPTION
    WHEN NO_DATA_FOUND THEN
	l_sec:='Failed in get the URL';
	l_url:=NULL;
	WHEN others THEN
	 l_sec:=SUBSTR(SQLERRM,1,50);
	 l_url:=NULL;
  END;
  
   FOR i IN c1 
     LOOP
     
       l_sec := 'Send Email Notification';
                        ------------------------------------------
                        -- Send Email Notification
                        ------------------------------------------
                        l_subject :=
                           ' PO NOT RECEIVED, PO# '|| i.segment1
                                                 || '. '
                                                 || i.vendor_name
                                                 ||', Promise date '
                                                 || nvl(i.promise_by_date,i.need_by_date);
                                                 
                        l_body_header :=
                           '<style type="text/css">.style1 {font-family: Arial, Helvetica, sans-serif;}</style><p class="style1">';
                        l_body_detail :=
                              ' <BR> Purchase Order# '
                           || i.segment1
                           || ' has been in Approved status that has exceed the promise date by 10 days. '
                           || ' </BR> PO Promise date:  '
                           || nvl(i.promise_by_date,i.need_by_date)
                           || CHR (10)
                           ||'</BR> If the PO is still valid, please consider updating the promise date.'
                           || CHR (10)
                           ||' </BR> If PO is no longer valid then please login to Oracle EBS '
                           || l_url
						   || CHR (10)
                           || ' and cancel the PO.       </BR>';
                        l_body_footer := NULL;
                           
                        l_body :=
                              l_body_header
                           || l_body_detail
                           || CHR (10)
                           || l_body_footer;

                        xxcus_misc_pkg.html_email (
                           p_to              => i.email_address,
                           p_from            => g_sender,
                           p_text            => 'Approved but not received PO',
                           p_subject         => l_subject,
                           p_html            => l_body,
                           p_smtp_hostname   => g_host,
                           p_smtp_portnum    => g_hostport);
	  l_count:=l_count+1;	
      l_sec :=' Sending the mail to buyer for approved_and_not_received_pos, PO#: '||i.segment1||' is failed';
   END LOOP;
   fnd_file.put_line(FND_FILE.log,'Total count of mails sent is :'||l_count);
 EXCEPTION
 WHEN OTHERS
      THEN
        xxcus_error_pkg.xxcus_error_main_api (
          p_called_from         => 'XXWC_PO_NOTIFICATIONS_PKG.approved_and_not_received_pos',
          p_calling             => l_sec,
          p_request_id          => fnd_global.conc_request_id,
          p_ora_error_msg       => SQLERRM,
          p_error_desc          => 'Error in approved_and_not_received_pos procedure, When Others Exception',
          p_distribution_list   => g_dflt_email,
          p_module              => 'PO');
 END;
END XXWC_PO_NOTIFICATIONS_PKG;
/