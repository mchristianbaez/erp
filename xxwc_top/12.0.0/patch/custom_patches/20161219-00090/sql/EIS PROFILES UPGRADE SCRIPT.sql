--//============================================================================
--//
--// Object Name         :: EIS PROFILES UPGRADE SCRIPT  
--//
--// Version Control
--//============================================================================
--// Version    Author           Date          Description
--//----------------------------------------------------------------------------
--//  1.0       20-Dec-2016      Siva   		TMS#20161219-00090  
--//============================================================================
--/* Script to update Spread sheet preference profile on user level */--
DECLARE
  CURSOR CUR_USERS
  IS
    SELECT LEVEL_VALUE,PROFILE_OPTION_VALUE
    FROM FND_PROFILE_OPTION_VALUES FPOV,
      FND_PROFILE_OPTIONS FPO
    WHERE FPO.PROFILE_OPTION_ID=FPOV.PROFILE_OPTION_ID
    AND FPO.PROFILE_OPTION_NAME='XXEIS_RSC_SPREAD_SHEET_PREFERENCE'
    AND LEVEL_ID               =10004
    AND PROFILE_OPTION_VALUE='Microsoft Office 2003';
  l_success boolean;
BEGIN
  FOR REC_USERS IN CUR_USERS
  LOOP
    l_success := FND_PROFILE.save ( X_NAME => 'XXEIS_RSC_SPREAD_SHEET_PREFERENCE' ,
          x_value => 'Microsoft Office 2007 / 2010' ,
          X_LEVEL_NAME => 'USER' , 
          x_level_value => REC_USERS.LEVEL_VALUE ,
          X_LEVEL_VALUE_APP_ID => NULL ) ;
  END LOOP;
  COMMIT;
END;
/
--/* Script to update Report specific Parameter List to public */--
BEGIN
UPDATE XXEIS.EIS_RS_PARAMETER_LISTS
SET SECURITY_FLAG         ='Y'
WHERE BASE_REPORT_ID     IS NOT NULL
AND NVL(SECURITY_FLAG,'N')='N';
COMMIT;
END;
/
-- /* Script to update Date List to Start Date and End Date */--
BEGIN
  UPDATE XXEIS.EIS_RS_REPORT_PARAMETERS ERRP
  SET errp.date_list ='Start Date'
  WHERE ( UPPER (errp.parameter_name) LIKE '%FROM%'
  OR UPPER (errp.parameter_name) LIKE '%LOW'
  OR UPPER (errp.parameter_name) LIKE '%START DATE')
  AND errp.date_list IS NULL
  AND ERRP.DATA_TYPE  ='DATE';  

  UPDATE XXEIS.EIS_RS_REPORT_PARAMETERS ERRP
  SET errp.date_list ='End Date'
  WHERE ( UPPER (errp.parameter_name) LIKE '%TO'
  OR UPPER (errp.parameter_name) LIKE '%HIGH'
  OR UPPER (errp.parameter_name) LIKE '%END DATE')
  AND ERRP.DATE_LIST IS NULL
  AND ERRP.DATA_TYPE  ='DATE';     

  UPDATE XXEIS.EIS_RS_REPORT_PARAMETERS ERRP
  SET errp.date_list ='Start Date'
  WHERE  ERRP.DATE_LIST IS NULL
  AND ERRP.DATA_TYPE  ='DATE';     
  COMMIT;
END;
/
