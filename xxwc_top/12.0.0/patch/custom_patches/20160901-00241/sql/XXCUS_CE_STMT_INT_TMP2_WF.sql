/*
 MODULE: CASH MANAGEMENT
 AUTHOR: BALAGURU SESHADRI
 DATE: 10/24/2016
 SCOPE: HDS TREASURY: PROCESS CURRENT DAY BANK BAI FILES
 TICKET: ESMS 322411 / TMS 20160901-00241
*/
--
CREATE TABLE XXCUS.XXCUS_CE_STMT_INT_TMP2_WF
(
  BANK_NAME           VARCHAR2(60 BYTE)         NOT NULL,
  FILE_NAME           VARCHAR2(60 BYTE)         NOT NULL,
  CREATION_DATE       DATE                      NOT NULL,
  FILE_IMPORTED_FLAG  VARCHAR2(1 BYTE)          NOT NULL,
  REQUEST_ID          NUMBER,
  STATUS_CODE         VARCHAR2(30 BYTE),
  REC_NO              NUMBER                    NOT NULL,
  REC_ID_NO           VARCHAR2(30 BYTE)         NOT NULL,
  COLUMN1             VARCHAR2(2000 BYTE),
  COLUMN2             VARCHAR2(255 BYTE),
  COLUMN3             VARCHAR2(255 BYTE),
  COLUMN4             VARCHAR2(255 BYTE),
  COLUMN5             VARCHAR2(255 BYTE),
  COLUMN6             VARCHAR2(255 BYTE),
  COLUMN7             VARCHAR2(255 BYTE),
  COLUMN8             VARCHAR2(255 BYTE),
  COLUMN9             VARCHAR2(255 BYTE),
  COLUMN10            VARCHAR2(255 BYTE),
  COLUMN11            VARCHAR2(255 BYTE),
  COLUMN12            VARCHAR2(255 BYTE),
  COLUMN13            VARCHAR2(255 BYTE),
  COLUMN14            VARCHAR2(255 BYTE),
  COLUMN15            VARCHAR2(255 BYTE),
  COLUMN16            VARCHAR2(255 BYTE),
  COLUMN17            VARCHAR2(255 BYTE),
  COLUMN18            VARCHAR2(255 BYTE),
  COLUMN19            VARCHAR2(255 BYTE),
  COLUMN20            VARCHAR2(255 BYTE),
  COLUMN21            VARCHAR2(255 BYTE),
  COLUMN22            VARCHAR2(255 BYTE),
  COLUMN23            VARCHAR2(255 BYTE),
  COLUMN24            VARCHAR2(255 BYTE),
  COLUMN25            VARCHAR2(255 BYTE),
  COLUMN26            VARCHAR2(255 BYTE),
  COLUMN27            VARCHAR2(255 BYTE),
  COLUMN28            VARCHAR2(255 BYTE),
  COLUMN29            VARCHAR2(255 BYTE),
  COLUMN30            VARCHAR2(255 BYTE),
  COLUMN31            VARCHAR2(255 BYTE),
  COLUMN32            VARCHAR2(255 BYTE),
  COLUMN33            VARCHAR2(255 BYTE),
  COLUMN34            VARCHAR2(255 BYTE),
  COLUMN35            VARCHAR2(255 BYTE)
)
;
--
COMMENT ON TABLE XXCUS.XXCUS_CE_STMT_INT_TMP2_WF IS 'ESMS 322411 / TMS 20160901-00241';
--

