/*
 MODULE: CASH MANAGEMENT
 AUTHOR: BALAGURU SESHADRI
 DATE: 10/24/2016
 SCOPE: HDS TREASURY: PROCESS CURRENT DAY BANK BAI FILES
 TICKET: ESMS 322411 / TMS 20160901-00241
*/
--
CREATE TABLE XXCUS.XXCUS_BANKS_BAI_LOOKUP
(
 BANK_NAME VARCHAR2(60)
,ACCT VARCHAR2(60)
,CODE VARCHAR2(3)
,REQUIRED_FLAG VARCHAR2(1)
);
--
COMMENT ON TABLE XXCUS.XXCUS_BANKS_BAI_LOOKUP IS 'ESMS 322411 / TMS 20160901-00241';
--