/*
 Ticket: ESMS 322411 / TMS 20160901-00241
 Date: 10/24/2016
 MODULE: CASH MANAGEMENT
 AUTHOR: BALAGURU SESHADRI 
 Notes:  HDS TREASURY: PROCESS CURRENT DAY BANK BAI FILES
*/
SET SERVEROUTPUT ON SIZE 1000000
DECLARE
 --
 n_loc number :=0;
 n_count number :=0;
 l_err_msg varchar2(2000) :=Null;
 b_go boolean;
 --
BEGIN --Main Processing...
   --
   b_go :=TRUE;
   --
   n_loc :=101;
   --
  if (b_go) then 
   --
   n_loc :=102;
   --
   begin
    --
    savepoint start1;
    --
	delete XXCUS.XXCUS_BAI_RECORD_TYPES;
	--
        Insert into XXCUS.XXCUS_BAI_RECORD_TYPES
           (RECORD_ID, RECORD_DESCRIPTION, CREATED_BY, CREATION_DATE)
         Values
           ('01', 'File Header', 15837, TO_DATE('09/15/2016 14:03:48', 'MM/DD/YYYY HH24:MI:SS'));
        Insert into XXCUS.XXCUS_BAI_RECORD_TYPES
           (RECORD_ID, RECORD_DESCRIPTION, CREATED_BY, CREATION_DATE)
         Values
           ('02', 'Group header', 15837, TO_DATE('09/15/2016 14:03:48', 'MM/DD/YYYY HH24:MI:SS'));
        Insert into XXCUS.XXCUS_BAI_RECORD_TYPES
           (RECORD_ID, RECORD_DESCRIPTION, CREATED_BY, CREATION_DATE)
         Values
           ('03', 'Account Identifier and Summary/Status', 15837, TO_DATE('09/15/2016 14:03:48', 'MM/DD/YYYY HH24:MI:SS'));
        Insert into XXCUS.XXCUS_BAI_RECORD_TYPES
           (RECORD_ID, RECORD_DESCRIPTION, CREATED_BY, CREATION_DATE)
         Values
           ('16', 'Account Transaction Detail', 15837, TO_DATE('09/15/2016 14:03:48', 'MM/DD/YYYY HH24:MI:SS'));
        Insert into XXCUS.XXCUS_BAI_RECORD_TYPES
           (RECORD_ID, RECORD_DESCRIPTION, CREATED_BY, CREATION_DATE)
         Values
           ('88', 'Continuation of Account Summary Record', 15837, TO_DATE('09/15/2016 14:03:48', 'MM/DD/YYYY HH24:MI:SS'));
        Insert into XXCUS.XXCUS_BAI_RECORD_TYPES
           (RECORD_ID, RECORD_DESCRIPTION, CREATED_BY, CREATION_DATE)
         Values
           ('49', 'Account Trailer', 15837, TO_DATE('09/15/2016 14:03:48', 'MM/DD/YYYY HH24:MI:SS'));
        Insert into XXCUS.XXCUS_BAI_RECORD_TYPES
           (RECORD_ID, RECORD_DESCRIPTION, CREATED_BY, CREATION_DATE)
         Values
           ('98', 'Group Trailer', 15837, TO_DATE('09/15/2016 14:03:48', 'MM/DD/YYYY HH24:MI:SS'));
        Insert into XXCUS.XXCUS_BAI_RECORD_TYPES
           (RECORD_ID, RECORD_DESCRIPTION, CREATED_BY, CREATION_DATE)
         Values
           ('99', 'File Trailer', 15837, TO_DATE('09/15/2016 14:03:48', 'MM/DD/YYYY HH24:MI:SS'));
    --
    n_loc :=104;
    --
    commit;
    --
   exception
    when others then
     --
     n_loc :=105;
     --
     dbms_output.put_line('@ n_loc ='||n_loc||', message ='||sqlerrm);
     --
     rollback to start1;
     --     
   end;
   --
  else
   --
   n_loc :=103;
   --
   dbms_output.put_line('@b_go =FALSE, n_loc ='||n_loc);
   --
  end if;  --  if (b_go) then 
 --
 commit;
 --
EXCEPTION
   WHEN OTHERS
   THEN
      ROLLBACK;
      DBMS_OUTPUT.put_line ('Outer Block..., Errors =' || SQLERRM);
END;
/