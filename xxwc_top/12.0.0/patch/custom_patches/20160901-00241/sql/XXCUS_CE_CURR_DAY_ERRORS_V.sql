/*
 MODULE: CASH MANAGEMENT
 AUTHOR: BALAGURU SESHADRI
 DATE: 10/24/2016
 SCOPE: HDS TREASURY: PROCESS CURRENT DAY BANK BAI FILES
 TICKET: ESMS 322411 / TMS 20160901-00241
*/
--
CREATE OR REPLACE FORCE VIEW APPS.XXCUS_CE_CURR_DAY_ERRORS_V 
AS
select 
          a."BANK_NAME",
          a."BANK_BRANCH_NAME",
          a."BANK_ACCOUNT_NAME",
          a."BANK_ACCOUNT_NUM",
          a."CURRENCY_CODE",
          a."CURRENT_DAY_FILE_NAME",
          a."BANK_FILE_CREATE_DATE",
          'Required BAI Codes missing [' 
          ||(
                 select    listagg(bai_code,',') within group (order by bai_code)
                 from     xxcus.xxcus_banks_bai_codes_mapping
                 where  1 =1
                       and bank_account_num =a.bank_account_num
                       and required_flag ='R'
               )
           ||']' BAI_CODE_CHECK
from   apps.xxcus_ce_curr_day_bank_accts_v a
where 1 =1
    and 
      ( --1
         not exists
           ( --2
           select '1'
           from  xxcus.xxcus_ce_stmt_hdr_int c
                      ,xxcus.xxcus_banks_bai_codes_mapping b
           where 1 =1
                 and c.bank_account_num =a.bank_account_num
                 and c.bank_name =a.bank_name
                 and c.bank_branch_name =a.bank_branch_name
                 and b.bank_account_num =a.bank_account_num
                 and b.bai_code =lpad (c.item_code, 3, '0') 
                 and b.required_flag ='R'
           ) --2
        OR
         ( --2
             exists
           ( --3
           select '1'
           from  xxcus.xxcus_ce_stmt_hdr_int c
                      ,xxcus.xxcus_banks_bai_codes_mapping b
           where 1 =1
                 and c.bank_account_num =a.bank_account_num
                 and c.bank_name =a.bank_name
                 and c.bank_branch_name =a.bank_branch_name
                 and b.bank_account_num =a.bank_account_num
                 and b.bai_code =lpad (c.item_code, 3, '0') 
                 and b.required_flag ='R'
                 and c.item_amount is null
           ) --3 
         ) --2          
      ) --1
           ;
--
COMMENT ON TABLE APPS.XXCUS_CE_CURR_DAY_ERRORS_V IS 'ESMS322411 /  TMS 20160901-00241';
-- 
