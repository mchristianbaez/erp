/*
 Ticket: ESMS 322411 / TMS 20160901-00241 
 Date: 10/24/2016
 MODULE: CASH MANAGEMENT
 AUTHOR: BALAGURU SESHADRI 
 Notes:  HDS TREASURY: PROCESS CURRENT DAY BANK BAI FILES
*/
SET SERVEROUTPUT ON SIZE 1000000
DECLARE
 --
 n_loc number :=0;
 n_count number :=0;
 l_err_msg varchar2(2000) :=Null;
 b_go boolean;
 --
BEGIN --Main Processing...
   --
   b_go :=TRUE;
   --
   n_loc :=101;
   --
  if (b_go) then 
   --
   n_loc :=102;
   --
   begin
    --
    savepoint start1;
    --
	delete XXCUS.XXCUS_BAI_CODE_MASTER;
	--
        Insert into XXCUS.XXCUS_BAI_CODE_MASTER
           (BAI_CODE, BAI_DESCRIPTION, ENABLED_FLAG, CREATED_BY, CREATION_DATE)
         Values
           ('010', 'Opening Ledger Balance', 'N', 15837, TO_DATE('09/15/2016 14:18:10', 'MM/DD/YYYY HH24:MI:SS'));
        Insert into XXCUS.XXCUS_BAI_CODE_MASTER
           (BAI_CODE, BAI_DESCRIPTION, ENABLED_FLAG, CREATED_BY, CREATION_DATE)
         Values
           ('015', 'Closing Ledger Balance', 'N', 15837, TO_DATE('09/15/2016 14:18:10', 'MM/DD/YYYY HH24:MI:SS'));
        Insert into XXCUS.XXCUS_BAI_CODE_MASTER
           (BAI_CODE, BAI_DESCRIPTION, ENABLED_FLAG, CREATED_BY, CREATION_DATE)
         Values
           ('020', 'MTD Average Ledger Balance', 'N', 15837, TO_DATE('09/15/2016 14:18:10', 'MM/DD/YYYY HH24:MI:SS'));
        Insert into XXCUS.XXCUS_BAI_CODE_MASTER
           (BAI_CODE, BAI_DESCRIPTION, ENABLED_FLAG, CREATED_BY, CREATION_DATE)
         Values
           ('025', 'YTD Average Ledger Balance', 'N', 15837, TO_DATE('09/15/2016 14:18:10', 'MM/DD/YYYY HH24:MI:SS'));
        Insert into XXCUS.XXCUS_BAI_CODE_MASTER
           (BAI_CODE, BAI_DESCRIPTION, ENABLED_FLAG, CREATED_BY, CREATION_DATE)
         Values
           ('030', 'Current Ledger Balance (Intraday)', 'Y', 15837, TO_DATE('09/15/2016 14:18:10', 'MM/DD/YYYY HH24:MI:SS'));
        Insert into XXCUS.XXCUS_BAI_CODE_MASTER
           (BAI_CODE, BAI_DESCRIPTION, ENABLED_FLAG, CREATED_BY, CREATION_DATE)
         Values
           ('040', 'Available Balance', 'N', 15837, TO_DATE('09/15/2016 14:18:11', 'MM/DD/YYYY HH24:MI:SS'));
        Insert into XXCUS.XXCUS_BAI_CODE_MASTER
           (BAI_CODE, BAI_DESCRIPTION, ENABLED_FLAG, CREATED_BY, CREATION_DATE)
         Values
           ('045', 'Collected Balance', 'N', 15837, TO_DATE('09/15/2016 14:18:11', 'MM/DD/YYYY HH24:MI:SS'));
        Insert into XXCUS.XXCUS_BAI_CODE_MASTER
           (BAI_CODE, BAI_DESCRIPTION, ENABLED_FLAG, CREATED_BY, CREATION_DATE)
         Values
           ('050', 'MTD Average Collected Balance', 'N', 15837, TO_DATE('09/15/2016 14:18:11', 'MM/DD/YYYY HH24:MI:SS'));
        Insert into XXCUS.XXCUS_BAI_CODE_MASTER
           (BAI_CODE, BAI_DESCRIPTION, ENABLED_FLAG, CREATED_BY, CREATION_DATE)
         Values
           ('057', 'Total Investment Position', 'N', 15837, TO_DATE('09/15/2016 14:18:11', 'MM/DD/YYYY HH24:MI:SS'));
        Insert into XXCUS.XXCUS_BAI_CODE_MASTER
           (BAI_CODE, BAI_DESCRIPTION, ENABLED_FLAG, CREATED_BY, CREATION_DATE)
         Values
           ('060', 'Current Available Balance (Intraday)', 'Y', 15837, TO_DATE('09/15/2016 14:18:11', 'MM/DD/YYYY HH24:MI:SS'));
        Insert into XXCUS.XXCUS_BAI_CODE_MASTER
           (BAI_CODE, BAI_DESCRIPTION, ENABLED_FLAG, CREATED_BY, CREATION_DATE)
         Values
           ('070', 'Zero Day Float', 'N', 15837, TO_DATE('09/15/2016 14:18:11', 'MM/DD/YYYY HH24:MI:SS'));
        Insert into XXCUS.XXCUS_BAI_CODE_MASTER
           (BAI_CODE, BAI_DESCRIPTION, ENABLED_FLAG, CREATED_BY, CREATION_DATE)
         Values
           ('072', 'One Day Float', 'N', 15837, TO_DATE('09/15/2016 14:18:11', 'MM/DD/YYYY HH24:MI:SS'));
        Insert into XXCUS.XXCUS_BAI_CODE_MASTER
           (BAI_CODE, BAI_DESCRIPTION, ENABLED_FLAG, CREATED_BY, CREATION_DATE)
         Values
           ('074', 'Two Day Float', 'N', 15837, TO_DATE('09/15/2016 14:18:11', 'MM/DD/YYYY HH24:MI:SS'));
        Insert into XXCUS.XXCUS_BAI_CODE_MASTER
           (BAI_CODE, BAI_DESCRIPTION, ENABLED_FLAG, CREATED_BY, CREATION_DATE)
         Values
           ('100', 'Total Credits', 'N', 15837, TO_DATE('09/15/2016 14:18:11', 'MM/DD/YYYY HH24:MI:SS'));
        Insert into XXCUS.XXCUS_BAI_CODE_MASTER
           (BAI_CODE, BAI_DESCRIPTION, ENABLED_FLAG, CREATED_BY, CREATION_DATE)
         Values
           ('110', 'Total Lockbox Credits', 'N', 15837, TO_DATE('09/15/2016 14:18:11', 'MM/DD/YYYY HH24:MI:SS'));
        Insert into XXCUS.XXCUS_BAI_CODE_MASTER
           (BAI_CODE, BAI_DESCRIPTION, ENABLED_FLAG, CREATED_BY, CREATION_DATE)
         Values
           ('140', 'Total Ach Credits', 'N', 15837, TO_DATE('09/15/2016 14:18:11', 'MM/DD/YYYY HH24:MI:SS'));
        Insert into XXCUS.XXCUS_BAI_CODE_MASTER
           (BAI_CODE, BAI_DESCRIPTION, ENABLED_FLAG, CREATED_BY, CREATION_DATE)
         Values
           ('170', 'Total Other Deposits', 'N', 15837, TO_DATE('09/15/2016 14:18:11', 'MM/DD/YYYY HH24:MI:SS'));
        Insert into XXCUS.XXCUS_BAI_CODE_MASTER
           (BAI_CODE, BAI_DESCRIPTION, ENABLED_FLAG, CREATED_BY, CREATION_DATE)
         Values
           ('182', 'Total Bank-Prepared Deposits', 'N', 15837, TO_DATE('09/15/2016 14:18:11', 'MM/DD/YYYY HH24:MI:SS'));
        Insert into XXCUS.XXCUS_BAI_CODE_MASTER
           (BAI_CODE, BAI_DESCRIPTION, ENABLED_FLAG, CREATED_BY, CREATION_DATE)
         Values
           ('186', 'Total Cash Letter Credits', 'N', 15837, TO_DATE('09/15/2016 14:18:11', 'MM/DD/YYYY HH24:MI:SS'));
        Insert into XXCUS.XXCUS_BAI_CODE_MASTER
           (BAI_CODE, BAI_DESCRIPTION, ENABLED_FLAG, CREATED_BY, CREATION_DATE)
         Values
           ('190', 'Total Incoming Money Transfers', 'N', 15837, TO_DATE('09/15/2016 14:18:11', 'MM/DD/YYYY HH24:MI:SS'));
        Insert into XXCUS.XXCUS_BAI_CODE_MASTER
           (BAI_CODE, BAI_DESCRIPTION, ENABLED_FLAG, CREATED_BY, CREATION_DATE)
         Values
           ('200', 'Total Automatic Transfer Credits', 'N', 15837, TO_DATE('09/15/2016 14:18:11', 'MM/DD/YYYY HH24:MI:SS'));
        Insert into XXCUS.XXCUS_BAI_CODE_MASTER
           (BAI_CODE, BAI_DESCRIPTION, ENABLED_FLAG, CREATED_BY, CREATION_DATE)
         Values
           ('205', 'Total Book Transfer Credits', 'N', 15837, TO_DATE('09/15/2016 14:18:11', 'MM/DD/YYYY HH24:MI:SS'));
        Insert into XXCUS.XXCUS_BAI_CODE_MASTER
           (BAI_CODE, BAI_DESCRIPTION, ENABLED_FLAG, CREATED_BY, CREATION_DATE)
         Values
           ('207', 'Total Intl Money Transfer Credits', 'N', 15837, TO_DATE('09/15/2016 14:18:11', 'MM/DD/YYYY HH24:MI:SS'));
        Insert into XXCUS.XXCUS_BAI_CODE_MASTER
           (BAI_CODE, BAI_DESCRIPTION, ENABLED_FLAG, CREATED_BY, CREATION_DATE)
         Values
           ('211', 'Total Foreign Exchange Credits', 'N', 15837, TO_DATE('09/15/2016 14:18:11', 'MM/DD/YYYY HH24:MI:SS'));
        Insert into XXCUS.XXCUS_BAI_CODE_MASTER
           (BAI_CODE, BAI_DESCRIPTION, ENABLED_FLAG, CREATED_BY, CREATION_DATE)
         Values
           ('215', 'Total Letters Of Credit', 'N', 15837, TO_DATE('09/15/2016 14:18:11', 'MM/DD/YYYY HH24:MI:SS'));
        Insert into XXCUS.XXCUS_BAI_CODE_MASTER
           (BAI_CODE, BAI_DESCRIPTION, ENABLED_FLAG, CREATED_BY, CREATION_DATE)
         Values
           ('256', 'Total Ach Return Items Credit', 'N', 15837, TO_DATE('09/15/2016 14:18:11', 'MM/DD/YYYY HH24:MI:SS'));
        Insert into XXCUS.XXCUS_BAI_CODE_MASTER
           (BAI_CODE, BAI_DESCRIPTION, ENABLED_FLAG, CREATED_BY, CREATION_DATE)
         Values
           ('260', 'Total Return Item Credits', 'N', 15837, TO_DATE('09/15/2016 14:18:11', 'MM/DD/YYYY HH24:MI:SS'));
        Insert into XXCUS.XXCUS_BAI_CODE_MASTER
           (BAI_CODE, BAI_DESCRIPTION, ENABLED_FLAG, CREATED_BY, CREATION_DATE)
         Values
           ('270', 'Total Zero Balance Credits', 'N', 15837, TO_DATE('09/15/2016 14:18:11', 'MM/DD/YYYY HH24:MI:SS'));
        Insert into XXCUS.XXCUS_BAI_CODE_MASTER
           (BAI_CODE, BAI_DESCRIPTION, ENABLED_FLAG, CREATED_BY, CREATION_DATE)
         Values
           ('294', 'Total Atm Credits', 'N', 15837, TO_DATE('09/15/2016 14:18:11', 'MM/DD/YYYY HH24:MI:SS'));
        Insert into XXCUS.XXCUS_BAI_CODE_MASTER
           (BAI_CODE, BAI_DESCRIPTION, ENABLED_FLAG, CREATED_BY, CREATION_DATE)
         Values
           ('310', 'Total Commercial Deposits', 'N', 15837, TO_DATE('09/15/2016 14:18:11', 'MM/DD/YYYY HH24:MI:SS'));
        Insert into XXCUS.XXCUS_BAI_CODE_MASTER
           (BAI_CODE, BAI_DESCRIPTION, ENABLED_FLAG, CREATED_BY, CREATION_DATE)
         Values
           ('356', 'Total Credit Adjustments', 'N', 15837, TO_DATE('09/15/2016 14:18:11', 'MM/DD/YYYY HH24:MI:SS'));
        Insert into XXCUS.XXCUS_BAI_CODE_MASTER
           (BAI_CODE, BAI_DESCRIPTION, ENABLED_FLAG, CREATED_BY, CREATION_DATE)
         Values
           ('400', 'Total Debits', 'N', 15837, TO_DATE('09/15/2016 14:18:11', 'MM/DD/YYYY HH24:MI:SS'));
        Insert into XXCUS.XXCUS_BAI_CODE_MASTER
           (BAI_CODE, BAI_DESCRIPTION, ENABLED_FLAG, CREATED_BY, CREATION_DATE)
         Values
           ('416', 'Total Lockbox Debits', 'N', 15837, TO_DATE('09/15/2016 14:18:11', 'MM/DD/YYYY HH24:MI:SS'));
        Insert into XXCUS.XXCUS_BAI_CODE_MASTER
           (BAI_CODE, BAI_DESCRIPTION, ENABLED_FLAG, CREATED_BY, CREATION_DATE)
         Values
           ('450', 'Total Ach Debits', 'N', 15837, TO_DATE('09/15/2016 14:18:11', 'MM/DD/YYYY HH24:MI:SS'));
        Insert into XXCUS.XXCUS_BAI_CODE_MASTER
           (BAI_CODE, BAI_DESCRIPTION, ENABLED_FLAG, CREATED_BY, CREATION_DATE)
         Values
           ('470', 'Total Checks Paid', 'N', 15837, TO_DATE('09/15/2016 14:18:11', 'MM/DD/YYYY HH24:MI:SS'));
        Insert into XXCUS.XXCUS_BAI_CODE_MASTER
           (BAI_CODE, BAI_DESCRIPTION, ENABLED_FLAG, CREATED_BY, CREATION_DATE)
         Values
           ('482', 'Total Bank Originated Debits', 'N', 15837, TO_DATE('09/15/2016 14:18:11', 'MM/DD/YYYY HH24:MI:SS'));
        Insert into XXCUS.XXCUS_BAI_CODE_MASTER
           (BAI_CODE, BAI_DESCRIPTION, ENABLED_FLAG, CREATED_BY, CREATION_DATE)
         Values
           ('490', 'Total Outgoing Money Transfers', 'N', 15837, TO_DATE('09/15/2016 14:18:11', 'MM/DD/YYYY HH24:MI:SS'));
        Insert into XXCUS.XXCUS_BAI_CODE_MASTER
           (BAI_CODE, BAI_DESCRIPTION, ENABLED_FLAG, CREATED_BY, CREATION_DATE)
         Values
           ('500', 'Total Automatic Transfer Debits', 'N', 15837, TO_DATE('09/15/2016 14:18:11', 'MM/DD/YYYY HH24:MI:SS'));
        Insert into XXCUS.XXCUS_BAI_CODE_MASTER
           (BAI_CODE, BAI_DESCRIPTION, ENABLED_FLAG, CREATED_BY, CREATION_DATE)
         Values
           ('505', 'Total Book Transfer Debits', 'N', 15837, TO_DATE('09/15/2016 14:18:12', 'MM/DD/YYYY HH24:MI:SS'));
        Insert into XXCUS.XXCUS_BAI_CODE_MASTER
           (BAI_CODE, BAI_DESCRIPTION, ENABLED_FLAG, CREATED_BY, CREATION_DATE)
         Values
           ('507', 'Total Intl Money Transfer Debits', 'N', 15837, TO_DATE('09/15/2016 14:18:12', 'MM/DD/YYYY HH24:MI:SS'));
        Insert into XXCUS.XXCUS_BAI_CODE_MASTER
           (BAI_CODE, BAI_DESCRIPTION, ENABLED_FLAG, CREATED_BY, CREATION_DATE)
         Values
           ('556', 'Total Ach Return Items', 'N', 15837, TO_DATE('09/15/2016 14:18:12', 'MM/DD/YYYY HH24:MI:SS'));
        Insert into XXCUS.XXCUS_BAI_CODE_MASTER
           (BAI_CODE, BAI_DESCRIPTION, ENABLED_FLAG, CREATED_BY, CREATION_DATE)
         Values
           ('570', 'Total Zero Balance Debits', 'N', 15837, TO_DATE('09/15/2016 14:18:12', 'MM/DD/YYYY HH24:MI:SS'));
        Insert into XXCUS.XXCUS_BAI_CODE_MASTER
           (BAI_CODE, BAI_DESCRIPTION, ENABLED_FLAG, CREATED_BY, CREATION_DATE)
         Values
           ('583', 'First Presentment', 'N', 15837, TO_DATE('09/15/2016 14:18:12', 'MM/DD/YYYY HH24:MI:SS'));
        Insert into XXCUS.XXCUS_BAI_CODE_MASTER
           (BAI_CODE, BAI_DESCRIPTION, ENABLED_FLAG, CREATED_BY, CREATION_DATE)
         Values
           ('584', 'Second Presentment ', 'N', 15837, TO_DATE('09/15/2016 14:18:12', 'MM/DD/YYYY HH24:MI:SS'));
        Insert into XXCUS.XXCUS_BAI_CODE_MASTER
           (BAI_CODE, BAI_DESCRIPTION, ENABLED_FLAG, CREATED_BY, CREATION_DATE)
         Values
           ('630', 'Total Debit Adjustments', 'N', 15837, TO_DATE('09/15/2016 14:18:12', 'MM/DD/YYYY HH24:MI:SS'));
        Insert into XXCUS.XXCUS_BAI_CODE_MASTER
           (BAI_CODE, BAI_DESCRIPTION, ENABLED_FLAG, CREATED_BY, CREATION_DATE)
         Values
           ('716', 'CDA ACH Debits - Total', 'N', 15837, TO_DATE('09/15/2016 14:18:12', 'MM/DD/YYYY HH24:MI:SS'));
        Insert into XXCUS.XXCUS_BAI_CODE_MASTER
           (BAI_CODE, BAI_DESCRIPTION, ENABLED_FLAG, CREATED_BY, CREATION_DATE)
         Values
           ('868', 'Deposit Transfer Debit', 'Y', 15837, TO_DATE('09/15/2016 14:18:12', 'MM/DD/YYYY HH24:MI:SS'));
    --
    n_loc :=104;
    --
    commit;
    --
   exception
    when others then
     --
     n_loc :=105;
     --
     dbms_output.put_line('@ n_loc ='||n_loc||', message ='||sqlerrm);
     --
     rollback to start1;
     --     
   end;
   --
  else
   --
   n_loc :=103;
   --
   dbms_output.put_line('@b_go =FALSE, n_loc ='||n_loc);
   --
  end if;  --  if (b_go) then 
 --
 commit;
 --
EXCEPTION
   WHEN OTHERS
   THEN
      ROLLBACK;
      DBMS_OUTPUT.put_line ('Outer Block..., Errors =' || SQLERRM);
END;
/