/*
 Ticket: ESMS 322411 / TMS 20160901-00241
 Date: 10/24/2016
 MODULE: CASH MANAGEMENT
 AUTHOR: BALAGURU SESHADRI 
 Notes:  HDS TREASURY: PROCESS CURRENT DAY BANK BAI FILES
*/
SET SERVEROUTPUT ON SIZE 1000000
DECLARE
 --
 n_loc number :=0;
 n_count number :=0;
 l_err_msg varchar2(2000) :=Null;
 b_go boolean;
 --
BEGIN --Main Processing...
   --
   b_go :=TRUE;
   --
   n_loc :=101;
   --
  if (b_go) then 
   --
   n_loc :=102;
   --
   begin
    --
    savepoint start1;
    --
	delete XXCUS.XXCUS_BANKS_BAI_LOOKUP;
	--
             Insert into XXCUS.XXCUS_BANKS_BAI_LOOKUP
               (BANK_NAME, ACCT, CODE, REQUIRED_FLAG)
             Values
               ('WELLSFARGO', '4100067578', '060', 'R');
            Insert into XXCUS.XXCUS_BANKS_BAI_LOOKUP
               (BANK_NAME, ACCT, CODE, REQUIRED_FLAG)
             Values
               ('WELLSFARGO', '4100067610', '060', 'R');
            Insert into XXCUS.XXCUS_BANKS_BAI_LOOKUP
               (BANK_NAME, ACCT, CODE, REQUIRED_FLAG)
             Values
               ('WELLSFARGO', '4121140263', '060', 'R');
            Insert into XXCUS.XXCUS_BANKS_BAI_LOOKUP
               (BANK_NAME, ACCT, CODE, REQUIRED_FLAG)
             Values
               ('WELLSFARGO', '4121714406', '060', 'R');
            Insert into XXCUS.XXCUS_BANKS_BAI_LOOKUP
               (BANK_NAME, ACCT, CODE, REQUIRED_FLAG)
             Values
               ('WELLSFARGO', '4496859257', '060', 'R');
            Insert into XXCUS.XXCUS_BANKS_BAI_LOOKUP
               (BANK_NAME, ACCT, CODE, REQUIRED_FLAG)
             Values
               ('WELLSFARGO', '4944458595', '060', 'R');
            Insert into XXCUS.XXCUS_BANKS_BAI_LOOKUP
               (BANK_NAME, ACCT, CODE, REQUIRED_FLAG)
             Values
               ('WELLSFARGO', '4944458603', '060', 'R');
            Insert into XXCUS.XXCUS_BANKS_BAI_LOOKUP
               (BANK_NAME, ACCT, CODE, REQUIRED_FLAG)
             Values
               ('WELLSFARGO', '4944458629', '060', 'R');
            Insert into XXCUS.XXCUS_BANKS_BAI_LOOKUP
               (BANK_NAME, ACCT, CODE, REQUIRED_FLAG)
             Values
               ('WELLSFARGO', '4944458637', '060', 'R');
            Insert into XXCUS.XXCUS_BANKS_BAI_LOOKUP
               (BANK_NAME, ACCT, CODE, REQUIRED_FLAG)
             Values
               ('WELLSFARGO', '4944458645', '060', 'R');
            Insert into XXCUS.XXCUS_BANKS_BAI_LOOKUP
               (BANK_NAME, ACCT, CODE, REQUIRED_FLAG)
             Values
               ('WELLSFARGO', '4944458652', '060', 'R');
            Insert into XXCUS.XXCUS_BANKS_BAI_LOOKUP
               (BANK_NAME, ACCT, CODE, REQUIRED_FLAG)
             Values
               ('WELLSFARGO', '4944458660', '060', 'R');
            Insert into XXCUS.XXCUS_BANKS_BAI_LOOKUP
               (BANK_NAME, ACCT, CODE, REQUIRED_FLAG)
             Values
               ('WELLSFARGO', '4944734268', '060', 'R');
            Insert into XXCUS.XXCUS_BANKS_BAI_LOOKUP
               (BANK_NAME, ACCT, CODE, REQUIRED_FLAG)
             Values
               ('WELLSFARGO', '4945414761', '060', 'R');
            Insert into XXCUS.XXCUS_BANKS_BAI_LOOKUP
               (BANK_NAME, ACCT, CODE, REQUIRED_FLAG)
             Values
               ('WELLSFARGO', '4945670370', '060', 'R');
            Insert into XXCUS.XXCUS_BANKS_BAI_LOOKUP
               (BANK_NAME, ACCT, CODE, REQUIRED_FLAG)
             Values
               ('WELLSFARGO', '4942159047', '060', 'R');
            Insert into XXCUS.XXCUS_BANKS_BAI_LOOKUP
               (BANK_NAME, ACCT, CODE, REQUIRED_FLAG)
             Values
               ('WELLSFARGO', '4220095624', '060', 'R');
            Insert into XXCUS.XXCUS_BANKS_BAI_LOOKUP
               (BANK_NAME, ACCT, CODE, REQUIRED_FLAG)
             Values
               ('WELLSFARGO', '4944802578', '060', 'R');
            Insert into XXCUS.XXCUS_BANKS_BAI_LOOKUP
               (BANK_NAME, ACCT, CODE, REQUIRED_FLAG)
             Values
               ('WELLSFARGO', '4126175413', '060', 'R');
            Insert into XXCUS.XXCUS_BANKS_BAI_LOOKUP
               (BANK_NAME, ACCT, CODE, REQUIRED_FLAG)
             Values
               ('WELLSFARGO', '2000036896419', '060', 'R');
            Insert into XXCUS.XXCUS_BANKS_BAI_LOOKUP
               (BANK_NAME, ACCT, CODE, REQUIRED_FLAG)
             Values
               ('BOA_US', '4426430988', '060', 'R');
            Insert into XXCUS.XXCUS_BANKS_BAI_LOOKUP
               (BANK_NAME, ACCT, CODE, REQUIRED_FLAG)
             Values
               ('BOA_US', '4426430988', '868', 'O');
            Insert into XXCUS.XXCUS_BANKS_BAI_LOOKUP
               (BANK_NAME, ACCT, CODE, REQUIRED_FLAG)
             Values
               ('BOA_US', '003299832594', '060', 'R');
            Insert into XXCUS.XXCUS_BANKS_BAI_LOOKUP
               (BANK_NAME, ACCT, CODE, REQUIRED_FLAG)
             Values
               ('BOA_US', '3359327197', '060', 'R');
            Insert into XXCUS.XXCUS_BANKS_BAI_LOOKUP
               (BANK_NAME, ACCT, CODE, REQUIRED_FLAG)
             Values
               ('BOA_US', '3299122491', '060', 'R');
            Insert into XXCUS.XXCUS_BANKS_BAI_LOOKUP
               (BANK_NAME, ACCT, CODE, REQUIRED_FLAG)
             Values
               ('BOA_US', '3751293655', '060', 'R');
            Insert into XXCUS.XXCUS_BANKS_BAI_LOOKUP
               (BANK_NAME, ACCT, CODE, REQUIRED_FLAG)
             Values
               ('BOA_CA', '711447973102', '030', 'R');
            Insert into XXCUS.XXCUS_BANKS_BAI_LOOKUP
               (BANK_NAME, ACCT, CODE, REQUIRED_FLAG)
             Values
               ('BOA_CA', '711447973110', '030', 'R');
            Insert into XXCUS.XXCUS_BANKS_BAI_LOOKUP
               (BANK_NAME, ACCT, CODE, REQUIRED_FLAG)
             Values
               ('BOA_CA', '711447973136', '030', 'R');
            Insert into XXCUS.XXCUS_BANKS_BAI_LOOKUP
               (BANK_NAME, ACCT, CODE, REQUIRED_FLAG)
             Values
               ('BOA_CA', '711447973144', '030', 'R');
            Insert into XXCUS.XXCUS_BANKS_BAI_LOOKUP
               (BANK_NAME, ACCT, CODE, REQUIRED_FLAG)
             Values
               ('BOA_CA', '711447973152', '030', 'R');
            Insert into XXCUS.XXCUS_BANKS_BAI_LOOKUP
               (BANK_NAME, ACCT, CODE, REQUIRED_FLAG)
             Values
               ('BOA_CA', '711447973178', '030', 'R');
            Insert into XXCUS.XXCUS_BANKS_BAI_LOOKUP
               (BANK_NAME, ACCT, CODE, REQUIRED_FLAG)
             Values
               ('BOA_CA', '711447973201', '030', 'R');
            Insert into XXCUS.XXCUS_BANKS_BAI_LOOKUP
               (BANK_NAME, ACCT, CODE, REQUIRED_FLAG)
             Values
               ('BOA_CA', '711447973219', '030', 'R');
            Insert into XXCUS.XXCUS_BANKS_BAI_LOOKUP
               (BANK_NAME, ACCT, CODE, REQUIRED_FLAG)
             Values
               ('BOA_CA', '711447973227', '030', 'R');
            Insert into XXCUS.XXCUS_BANKS_BAI_LOOKUP
               (BANK_NAME, ACCT, CODE, REQUIRED_FLAG)
             Values
               ('BOA_CA', '711447973235', '030', 'R');
            Insert into XXCUS.XXCUS_BANKS_BAI_LOOKUP
               (BANK_NAME, ACCT, CODE, REQUIRED_FLAG)
             Values
               ('BOA_CA', '711447973376', '030', 'R');
            Insert into XXCUS.XXCUS_BANKS_BAI_LOOKUP
               (BANK_NAME, ACCT, CODE, REQUIRED_FLAG)
             Values
               ('BOA_CA', '711447973384', '030', 'R');
            Insert into XXCUS.XXCUS_BANKS_BAI_LOOKUP
               (BANK_NAME, ACCT, CODE, REQUIRED_FLAG)
             Values
               ('BOA_CA', '711447973392', '030', 'R');
            Insert into XXCUS.XXCUS_BANKS_BAI_LOOKUP
               (BANK_NAME, ACCT, CODE, REQUIRED_FLAG)
             Values
               ('BOA_CA', '711447973409', '030', 'R');
            Insert into XXCUS.XXCUS_BANKS_BAI_LOOKUP
               (BANK_NAME, ACCT, CODE, REQUIRED_FLAG)
             Values
               ('BOA_CA', '711447973425', '030', 'R');
            Insert into XXCUS.XXCUS_BANKS_BAI_LOOKUP
               (BANK_NAME, ACCT, CODE, REQUIRED_FLAG)
             Values
               ('BOA_CA', '711447973433', '030', 'R');
    --
    n_loc :=104;
    --
    commit;
    --
   exception
    when others then
     --
     n_loc :=105;
     --
     dbms_output.put_line('@ n_loc ='||n_loc||', message ='||sqlerrm);
     --
     rollback to start1;
     --     
   end;
   --
  else
   --
   n_loc :=103;
   --
   dbms_output.put_line('@b_go =FALSE, n_loc ='||n_loc);
   --
  end if;  --  if (b_go) then 
 --
 commit;
 --
EXCEPTION
   WHEN OTHERS
   THEN
      ROLLBACK;
      DBMS_OUTPUT.put_line ('Outer Block..., Errors =' || SQLERRM);
END;
/