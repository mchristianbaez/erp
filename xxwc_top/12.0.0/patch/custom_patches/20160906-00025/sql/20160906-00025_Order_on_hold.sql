/*************************************************************************
  $Header TMS_20160906-00025_AWAITING_INVOICE_TO_CLOSE.sql $
  Module Name: TMS_20160906-00025  Data Fix script for I675908

  PURPOSE: Data fix script for I675908--No permanent fix in process (But patch available per oracle)

  REVISIONS:
  Ver        Date         Author                Description
  ---------  -----------  ------------------    --------------------------
  1.0        02-AUG-2016  Raghav Velichetti         TMS#20160906-00025 Order on hold

**************************************************************************/ 
SET SERVEROUTPUT ON SIZE 1000000;
DECLARE
BEGIN
   DBMS_OUTPUT.put_line ('TMS: 20160906-00025    , Before Update');

   update apps.oe_order_lines_all
   set FLOW_STATUS_CODE='CANCELLED',
   CANCELLED_FLAG='Y'
   where line_id = 78037759
   and header_id= 47660113;

   DBMS_OUTPUT.put_line (
         'TMS: 20160906-00025  Sales order lines updated (Expected:1): '
      || SQL%ROWCOUNT);

   COMMIT;

   DBMS_OUTPUT.put_line ('TMS: 20160906-00025    , End Update');
EXCEPTION
   WHEN OTHERS
   THEN
      ROLLBACK;
      DBMS_OUTPUT.put_line ('TMS: 20160906-00025 , Errors : ' || SQLERRM);
END;
/
