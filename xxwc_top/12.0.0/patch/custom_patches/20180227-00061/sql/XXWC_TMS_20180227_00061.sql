/*
 TMS:  20180227-00061
 Date: 02/27/2018
 Notes:  Delete incorrect budget interface records for Whitecap 2018 budget.
*/
SET SERVEROUTPUT ON SIZE 1000000
DECLARE
 --
 n_loc number :=0;
 l_sql varchar2(2000) :=Null;
 l_count number :=0;
 --
BEGIN --Main Processing...
   --
   n_loc :=101;
   --
    select count(1)
	into   l_count
	from   gl.gl_budget_interface
	where  1 =1
      and budget_name = 'WHITE CAP 2018'
	  and budget_entity_name = 'WC Budget - Org';
   --
   n_loc :=102;
   --   
   if l_count >0 then
    --
	execute immediate 'create table xxwc.gl_budget_intf_tms18022700061 as select * from gl.gl_budget_interface';
	--
    dbms_output.put_line('Backup table created, Table Name: xxwc.gl_budget_intf_tms18022700061');
    --
    n_loc :=103;
    --   	
	   begin
		--
		n_loc :=104;
		--        
		delete gl.gl_budget_interface
		where  1 =1
		  and budget_name = 'WHITE CAP 2018'
		  and budget_entity_name = 'WC Budget - Org'
		;
		--  
		   n_loc :=105;
        --   
		dbms_output.put_line('Rows deleted :'||sql%rowcount);
		--
		n_loc :=106;
		--        
	   exception
		when others then
		 --
		 n_loc :=107;
		 --
		 dbms_output.put_line('@ n_loc ='||n_loc||', message ='||sqlerrm);
		 --  
	   end;
	--
   else
    dbms_output.put_line('No records found for Budget: WHITE CAP 2018 and Entity Name: WC Budget - Org');
   end if;
   --
   commit;
   --
EXCEPTION
   WHEN OTHERS THEN
      ROLLBACK;
      DBMS_OUTPUT.put_line ('TMS: 20180227-00061, Errors =' || SQLERRM);
END;
/