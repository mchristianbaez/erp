CREATE TABLE XXWC.XXWC_MTL_DEMAND_HISTORIES_ARCH
/*************************************************************************
  $Header XXWC_MTL_DEMAND_HISTORIES_ARCH.sql $
  Module Name: XXWC_MTL_DEMAND_HISTORIES_ARCH

  PURPOSE: To Maintain the list transactions that are corrected 
            with their update statement and restore statements

  REVISIONS:
  Ver        Date         Author                Description
  ---------  -----------  ------------------    ----------------
  1.0        30-Apr-2015  Manjula Chellappan    Initial Version TMS # 20150302-00057

**************************************************************************/
(
  TRANSACTION_ID              NUMBER            NOT NULL,
  TRANSACTION_TYPE_CODE       VARCHAR2(30 BYTE) NOT NULL,
  TRANSACTION_TYPE            VARCHAR2(80 BYTE) NOT NULL,
  TRANSACTION_TYPE_ID         NUMBER            NOT NULL,
  TRANSACTION_SOURCE_TYPE_ID  NUMBER            NOT NULL,
  SOURCE_LINE_ID              NUMBER,
  TRX_SOURCE_LINE_ID          NUMBER,
  TRANSACTION_ACTION_ID       NUMBER            NOT NULL,
  INCLUDE_EXCLUDE_FLAG        VARCHAR2(150 BYTE),
  BUCKET_TO_UPDATE            VARCHAR2(150 BYTE),
  TRANSACTION_J_DATE          NUMBER,
  TRANSACTION_DATE            DATE              NOT NULL,
  INVENTORY_ITEM_ID           NUMBER            NOT NULL,
  ORGANIZATION_ID             NUMBER            NOT NULL,
  PRIMARY_QUANTITY            NUMBER            NOT NULL,
  PERIOD_START_DATE           DATE              NOT NULL,
  PERIOD_END_DATE             DATE,
  ROW_ID                      ROWID,
  STD_WIP_USAGE               NUMBER,
  SALES_ORDER_DEMAND          NUMBER,
  MISCELLANEOUS_ISSUE         NUMBER,
  INTERORG_ISSUE              NUMBER,
  DEMAND_RESTORE_STMT         VARCHAR2(1000 BYTE),
  DEMAND_UPDATE_STMT          VARCHAR2(1000 BYTE),
  UPDATE_DATE                 DATE
)
/
