CREATE OR REPLACE PACKAGE BODY APPS.XXWC_AR_OPEN_RECEIPTS_CONV_PKG
AS
   /*************************************************************************
   *  Copyright (c) 2011 Lucidity Consulting Group
   *  All rights reserved.
   **************************************************************************
   *   $Header XXWC_AR_OPEN_RECEIPTS_PKG.pks $
   *   Module Name: XXWC_AR_OPEN_RECEIPTS_PKG.pks
   *
   *   PURPOSE:   This package is called by the concurrent program XXWC AR Receipts Conversion
   *
   *   REVISIONS:
   *   Ver        Date        Author               Description
   *   ---------  ----------  ---------------      -------------------------
   *   1.0        10/11/2011  Vivek Lakaman        Initial Version
   *   1.1        03/04/2018  Ashwin Sridhar       Added for TMS#20180319-00242
   * ************************************************************************/

   /*************************************************************************
 *   Procedure : Write_Log
 *
 *   PURPOSE:   This procedure logs debug message in Concurrent Log file
 *   Parameter:
 *          IN
 *              p_debug_msg      -- Debug Message
 * ************************************************************************/


   -- ***************************************************************************************************************
   -- PROCEDURE Write_Log :  API to Log Error messages to Log file. Just to reduce the syntax in the file .

   --add message to concurrent output file
   PROCEDURE Write_Log (p_debug_msg IN VARCHAR2)
   IS
   BEGIN
      fnd_file.put_line (fnd_file.LOG, p_debug_msg);
   END Write_Log;

   /*************************************************************************
    *   Procedure : Write_output
    *
    *   PURPOSE:   This procedure logs message into concurrent output
    *   Parameter:
    *          IN
    *              p_debug_msg      -- Debug Message
    * ************************************************************************/
   PROCEDURE Write_output (p_debug_msg IN VARCHAR2)
   IS
   BEGIN
      fnd_file.put_line (fnd_file.output, p_debug_msg);
   END Write_output;

   -- ***************************************************************************************************************
   -- Function  GET_LAST_DAY_OF_PERIOD :  Initially this Function was called to determine GL Date. Now the value is derived from Profile .

   FUNCTION GET_LAST_DAY_OF_PERIOD
      RETURN DATE
   IS
      l_num_days_in_period   NUMBER;
      l_end_date             DATE;
   BEGIN
      SELECT end_date
        INTO l_end_date
        FROM GL_PERIODS_V
       WHERE ADD_MONTHS (SYSDATE, -1) BETWEEN start_date AND end_date;

      RETURN l_end_date;
   EXCEPTION
      WHEN OTHERS
      THEN
         RETURN NULL;
   END GET_LAST_DAY_OF_PERIOD;

   -- ***************************************************************************************************************
   -- Procedure SUBMIT : Main API to process AR Open Receipts from Staging table and populate them into Oracle Tables.

   PROCEDURE Submit (errbuf OUT VARCHAR2, retcode OUT VARCHAR2)
   IS
      l_error_msg           VARCHAR2 (1000);

      l_return_status       VARCHAR2 (1);
      l_msg_count           NUMBER;
      l_msg_data            VARCHAR2 (240);
      l_cash_receipt_id     NUMBER;
      p_count               NUMBER := 0;
      l_org_id              NUMBER;
      l_receipt_method_id   NUMBER;
      l_cust_account_id     NUMBER;
      l_receipt_count       NUMBER;
      l_conc_request_id     NUMBER;
      l_prev_pd_last_day    DATE;
      l_cust_party_id       NUMBER;
      l_cust_site_use_id    NUMBER;
      l_reference           VARCHAR2 (300);

      -- Define a Cursor that reads all the receipt records from staging table that are not processed
      CURSOR C_Open_Receipts (p_request_id IN NUMBER)
      IS
         SELECT ROWID row_id,
                conv.RECEIPT_NUMBER,
                conv.RECEIPT_METHOD,
                conv.RECEIPT_DATE,
                conv.RECEIPT_AMOUNT,
                conv.CURRENCY_CODE,
                conv.LEGACY_CUSTOMER_NUM,
                conv.LEGACY_CUSTOMER_SITE,
                conv.BANK_BRANCH_NAME,
                conv.BANK_ACCOUNT_NUMBER,
                conv.COMMENTS,
                conv.GL_DATE,
                conv.CREATION_DATE,
                conv.CREATED_BY,
                conv.LAST_UPDATED_BY,
                conv.LAST_UPDATE_DATE,
                conv.LAST_UPDATE_LOGIN,
                conv.PROCESS_STATUS,
                conv.ERROR_MESSAGE,
                conv.RECEIPT_REFERENCE,
                conv.DISPUTE_CODE,
                conv.ORG_ID
           FROM xxwc.xxwc_ar_receipts_conv conv
          WHERE process_status <> 'P' AND request_id = p_request_id;
         -- and receipt_number in ('1210100.115760.63','1348200.4369462-01');

      -- Define a Cursor to get Receipt COnversion Stats, count of the records by grouping on Process Status value.

      CURSOR C_Status_cnt (
         p_request_id   IN NUMBER)
      IS
           SELECT COUNT (*) sts_count,
                  DECODE (process_status,
                          'R', 'Ready to Process',
                          'E', 'Error',
                          'P', 'Processed Successfully',
                          'No Status')
                     status
             FROM xxwc_ar_receipts_conv conv
            WHERE process_status <> 'P' AND request_id = p_request_id
         GROUP BY DECODE (process_status,
                          'R', 'Ready to Process',
                          'E', 'Error',
                          'P', 'Processed Successfully',
                          'No Status');

      --Define user exception
      VALIDATION_ERROR      EXCEPTION;
      API_FAILURE           EXCEPTION;
   BEGIN
      Write_Log ('At Begin');
      Write_Log ('Initialize the Out Parameters...');

      --Initialize the Out Parameters
      errbuf := NULL;
      retcode := '0';

      l_org_id := mo_global.get_current_org_id ();
      l_org_id := 162; --Added by Ashwin.S on 02-APR-2018 for TMS#20180319-00242
      Write_Log ('l_org_id =>' || l_org_id);

      l_conc_request_id := fnd_global.conc_request_id; --Added by Ashwin.S on 02-APR-2018 for TMS#20180319-00242

      Write_Log ('l_conc_request_id =>' || l_conc_request_id);

      Write_Log ('Set the applications context');
      -- Set the applications context
      --mo_global.init ('AR');
      mo_global.set_policy_context ('S', l_org_id);
      fnd_global.apps_initialize (fnd_global.user_id,
                                  fnd_global.resp_id,
                                  fnd_global.resp_appl_id,
                                  0);

      --l_conc_request_id := fnd_global.conc_request_id;  --Commented by Ashwin.S on 02-APR-2018 for TMS#20180319-00242

      Write_Log ('Updating the Request Id');

      -- Update AR Receipts staging records with current request Id
      UPDATE xxwc.xxwc_ar_receipts_conv --Prefix xxwc by Ashwin.S on 02-APR-2018 for TMS#20180319-00242
         SET request_id = l_conc_request_id
       WHERE process_status <> 'P';

      Write_Log ('No of Records Updated =>' || SQL%ROWCOUNT);

      -- Loop through the AR Receipts from Staging table.
      FOR i IN C_Open_Receipts (l_conc_request_id)
      LOOP
         BEGIN
            SAVEPOINT PROCESS_RECEIPTS;
            Write_Log (
               '+------------------------------------------------------------+');
            Write_Log ('receipt_number      =>' || i.receipt_number);
            Write_Log ('legacy_customer_num =>' || i.legacy_customer_num);
            l_receipt_method_id := NULL;

            -- Validate Receipt Method. IF receipt method is invalid then raise error message.
            BEGIN
               SELECT receipt_method_id
                 INTO l_receipt_method_id
                 FROM ar_receipt_methods
                WHERE     NAME = i.receipt_method
                      AND SYSDATE BETWEEN start_date
                                      AND NVL (end_date, SYSDATE);
            EXCEPTION
               WHEN NO_DATA_FOUND
               THEN
                  l_error_msg :=
                        'Receipt method ('
                     || i.receipt_method
                     || ') does not exist';
                  RAISE VALIDATION_ERROR;
               WHEN OTHERS
               THEN
                  l_error_msg := SQLERRM;
                  RAISE VALIDATION_ERROR;
            END;

            Write_Log ('l_receipt_method_id =>' || l_receipt_method_id);

            l_cust_account_id := NULL;

            -- Check if Customer on Receipts exists in Oracle If not then raise error
            IF i.legacy_customer_num IS NOT NULL
            THEN
               -- Checking customer in cross over table or not
               BEGIN
                  SELECT b.cust_account_id, b.party_id
                    INTO l_cust_account_id, l_cust_party_id
                    FROM XXWC.XXWC_AHH_AR_CUST_CROSS_OVER_T A,
                         APPS.HZ_CUST_ACCOUNTS B
                   WHERE     A.CUST_NUM = i.legacy_customer_num
                         AND A.ORACLE_CUST_NUM = B.ACCOUNT_NUMBER;
               fnd_file.put_line(fnd_file.log,'Cross Over customer l_cust_account_id:'||l_cust_account_id||' l_cust_party_id:'||l_cust_party_id);          
               EXCEPTION
                  WHEN OTHERS
                  THEN
                     l_cust_account_id := 0;
                     l_cust_party_id := 0;
               END;

               -- Checking customer information available in oracle if customer is not in cross over.
               IF l_cust_account_id = 0
               THEN
                  BEGIN
                     SELECT a.cust_account_id, a.party_id
                       INTO l_cust_account_id, l_cust_party_id
                       FROM hz_cust_accounts a, hz_cust_acct_sites_all b
                      WHERE     a.cust_account_id = b.cust_account_id
                            AND a.ATTRIBUTE4 = 'AHH'
                            AND NVL (b.attribute17, 'ABCXYZ') =
                                   i.legacy_customer_num
                            AND b.bill_to_flag = 'P';
                    fnd_file.put_line(fnd_file.log,'Non-Cross Over customer l_cust_account_id:'||l_cust_account_id||' l_cust_party_id:'||l_cust_party_id);                            
                  EXCEPTION
                     WHEN NO_DATA_FOUND
                     THEN
                        l_error_msg :=
                              'Customer Number ('
                           || i.legacy_customer_num
                           || ' ) does not exist';
                        RAISE VALIDATION_ERROR;
                     WHEN OTHERS
                     THEN
                        l_error_msg := SQLERRM;
                        RAISE VALIDATION_ERROR;
                  END;
               END IF;

               Write_Log ('l_cust_account_id =>' || l_cust_account_id);
            END IF;


            Write_Log ('i.legacy_customer_site =>' || i.legacy_customer_site);

            -- Added  MC1, MC2, MC3, MC4 to the list : 28-FEB-2012  Satish Upadhyayula
            IF     i.legacy_customer_site IS NOT NULL
               AND i.legacy_customer_site != '0'
            THEN
               BEGIN
                  SELECT hcsu.SITE_USE_ID
                    INTO l_cust_site_use_id
                    FROM hz_party_sites hps,
                         hz_cust_acct_sites_all hcas,
                         hz_cust_site_uses_all hcsu
                   WHERE     hps.party_site_id = hcas.party_site_id
                         AND hcas.cust_acct_site_id = hcsu.cust_acct_site_id
                         AND hcsu.site_use_code = 'BILL_TO'
                         AND hps.party_id = l_cust_party_id
                         AND UPPER (NVL (hcas.attribute17, 'XYZ')) =
                                UPPER (
                                   TRIM (
                                         i.legacy_customer_num
                                      || '-'
                                      || i.LEGACY_CUSTOMER_SITE))
                         AND ROWNUM = 1;
                    fnd_file.put_line(fnd_file.log,'Bill to Site Useid:'||l_cust_site_use_id);                         
               EXCEPTION
                  WHEN NO_DATA_FOUND
                  THEN
                     l_error_msg :=
                           'Customer Site  ('
                        || i.legacy_customer_site
                        || ' ) does not exist';
                     RAISE VALIDATION_ERROR;
                  WHEN OTHERS
                  THEN
                     l_error_msg := SQLERRM;
                     RAISE VALIDATION_ERROR;
               END;
            ELSE
               l_cust_site_use_id := NULL;
            END IF;

            Write_Log ('l_cust_site_use_id =>' || l_cust_site_use_id);

            --Get the last day of the Previous Period
            l_prev_pd_last_day :=
               FND_PROFILE.VALUE ('XXWC_AR_CONVERSION_GL_DATE'); --GET_LAST_DAY_OF_PERIOD;

            IF l_prev_pd_last_day IS NULL
            THEN
               l_error_msg :=
                  'The GL Date is Null. Please set up the GL Date in the Profile Option - XXWC_AR_CONVERSION_GL_DATE';
               RAISE VALIDATION_ERROR;
            END IF;

            BEGIN
               SELECT i.dispute_code /*i.receipt_reference
                                              || DECODE (i.dispute_code, 0, NULL, i.dispute_code)*/
                                    INTO l_reference FROM DUAL;
            EXCEPTION
               WHEN OTHERS
               THEN
                  l_error_msg := SQLERRM;
                  RAISE VALIDATION_ERROR;
            END;

            Write_Log ('ar_receipt_api_pub.create_cash');

            FND_FILE.PUT_LINE (FND_FILE.LOG, 'Currency:' || i.currency_code);
            FND_FILE.PUT_LINE (FND_FILE.LOG, 'Rec amt:' || i.receipt_amount);
            FND_FILE.PUT_LINE (FND_FILE.LOG,
                               'receipt_number:' || i.receipt_number);
            FND_FILE.PUT_LINE (
               FND_FILE.LOG,
               'receipt_date:' || TO_CHAR (i.receipt_date, 'dd-mon-yyyy'));
            FND_FILE.PUT_LINE (FND_FILE.LOG, 'l_reference:' || l_reference);
            FND_FILE.PUT_LINE (FND_FILE.LOG,
                               'l_prev_pd_last_day:' || l_prev_pd_last_day);
            FND_FILE.PUT_LINE (FND_FILE.LOG,
                               'l_cust_account_id:' || l_cust_account_id);
            FND_FILE.PUT_LINE (FND_FILE.LOG,
                               'l_cust_site_use_id:' || l_cust_site_use_id);
            FND_FILE.PUT_LINE (FND_FILE.LOG,
                               'l_receipt_method_id:' || l_receipt_method_id);
            FND_FILE.PUT_LINE (FND_FILE.LOG,
                               'l_cash_receipt_id:' || l_cash_receipt_id);


            -- Call the API
            AR_RECEIPT_API_PUB.CREATE_CASH (
               p_api_version                  => 1.0,
               p_init_msg_list                => fnd_api.g_true,
               p_commit                       => fnd_api.g_false,
               p_validation_level             => fnd_api.g_valid_level_full,
               x_return_status                => l_return_status,
               x_msg_count                    => l_msg_count,
               x_msg_data                     => l_msg_data,
               p_currency_code                => i.currency_code,
               p_amount                       => i.receipt_amount,
               p_receipt_number               => i.receipt_number,
               p_receipt_date                 => i.receipt_date,
               p_customer_receipt_reference   => l_reference,
               p_comments                     => i.comments,
               p_gl_date                      => l_prev_pd_last_day,
               p_customer_id                  => l_cust_account_id,
               p_customer_site_use_id         => l_cust_site_use_id,
               p_receipt_method_id            => l_receipt_method_id,
               p_cr_id                        => l_cash_receipt_id);

            Write_Log ('Status        =>' || l_return_status);
            Write_Log ('Message count =>' || l_msg_count);

            IF l_return_status = 'S'
            THEN
               Write_Log (
                  'SUCCESS: Created Cash_Receipt_Id=>' || l_cash_receipt_id);
            ELSE
               IF l_msg_count = 1
               THEN
                  Write_Log ('FAILURE: l_msg_data ' || l_msg_data);
                  l_error_msg := l_msg_data;
               ELSIF l_msg_count > 1
               THEN
                  LOOP
                     p_count := p_count + 1;
                     l_msg_data :=
                        FND_MSG_PUB.Get (FND_MSG_PUB.G_NEXT, FND_API.G_FALSE);

                     IF l_msg_data IS NULL
                     THEN
                        EXIT;
                     END IF;

                     Write_Log (
                        'FAILURE: Message ' || p_count || '. ' || l_msg_data);
                  END LOOP;

                  l_error_msg := l_msg_data;
               END IF;

               RAISE API_FAILURE;
            END IF;

            UPDATE xxwc.xxwc_ar_receipts_conv --Prefix xxwc by Ashwin.S on 02-APR-2018 for TMS#20180319-00242
               SET process_status = 'P',
                   error_message = NULL,
                   last_updated_by = fnd_global.user_id,
                   last_update_date = SYSDATE,
                   last_update_login = fnd_global.login_id
             WHERE request_id = l_conc_request_id AND ROWID = i.row_id;

            COMMIT;
         EXCEPTION
            WHEN API_FAILURE
            THEN
               UPDATE xxwc.xxwc_ar_receipts_conv --Prefix xxwc by Ashwin.S on 02-APR-2018 for TMS#20180319-00242
                  SET process_status = 'E',
                      error_message = l_error_msg,
                      last_updated_by = fnd_global.user_id,
                      last_update_date = SYSDATE,
                      last_update_login = fnd_global.login_id
                WHERE request_id = l_conc_request_id AND ROWID = i.row_id;

               Write_Log ('FAILURE: error_msg1 =>' || l_error_msg);

               ROLLBACK TO PROCESS_RECEIPTS;
            --ROLLBACK TO PROCESS_RECEIPTS; --Commented by Ashwin.S on 03-APR-2018 for TMS#20180319-00242
            WHEN VALIDATION_ERROR
            THEN
               UPDATE xxwc.xxwc_ar_receipts_conv --Prefix xxwc by Ashwin.S on 02-APR-2018 for TMS#20180319-00242
                  SET process_status = 'E',
                      error_message = l_error_msg,
                      last_updated_by = fnd_global.user_id,
                      last_update_date = SYSDATE,
                      last_update_login = fnd_global.login_id
                WHERE request_id = l_conc_request_id AND ROWID = i.row_id;

               COMMIT; --Added by Ashwin.S on 03-APR-2018 for TMS#20180319-00242

               Write_Log ('FAILURE: error_msg =>' || l_error_msg);
         END;
      END LOOP;

      COMMIT;

      Write_output (' ');
      Write_output (' ');
      Write_output (
         '+--------------------------------------------------------------------------------+ ');
      Write_output (
         '***************************Count By Status*****************************************');
      Write_output (
         '+--------------------------------------------------------------------------------+ ');
      Write_output (
            RPAD ('Count By Status', 25, ' ')
         || RPAD ('Process Status', 50, ' '));

      FOR k IN C_Status_cnt (l_conc_request_id)
      LOOP
         Write_output (
            RPAD (k.sts_count, 25, '.') || RPAD (k.status, 50, ' '));
      END LOOP;

      Write_output (
         '+--------------------------------------------------------------------------------+ ');
   EXCEPTION
      WHEN VALIDATION_ERROR
      THEN
         errbuf := l_error_msg;
         retcode := '1';
      WHEN OTHERS
      THEN
         errbuf := SQLERRM;
         retcode := '2';
   END Submit;
END XXWC_AR_OPEN_RECEIPTS_CONV_PKG;
/