/***********************************************************************************************************************************************
   NAME:     TMS_20170419-00256_Datafix.sql
   PURPOSE:  Data fix

   REVISIONS:
   Ver        Date        Author           Description
   ---------  ----------  ---------------  ----------------------------------------------------------------------------------------------------
   1.0        04/21/2017  Rakesh Patel     TMS#20170419-00256-Data Fix to Update Notify Account Mgr flag for select AMs
************************************************************************************************************************************************/
SET SERVEROUTPUT ON SIZE 1000000

BEGIN
   DBMS_OUTPUT.put_line ('Before Update');

   UPDATE XXWC.XXWC_B2B_CONFIG_TBL
      SET notify_account_mgr = 'N'
         ,last_update_date   = SYSDATE
		 ,last_updated_by    = -1
    WHERE TRUNC(creation_date) BETWEEN TO_DATE('17-APR-17') AND TO_DATE('19-APR-17')
      AND notify_account_mgr = 'Y';


   DBMS_OUTPUT.put_line ('Records Updated -' || SQL%ROWCOUNT);

   COMMIT;

   EXCEPTION
   WHEN OTHERS
   THEN
      DBMS_OUTPUT.put_line ('Unable to update record ' || SQLERRM);
	  ROLLBACK;
END;
/