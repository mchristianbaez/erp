/*************************************************************************
  $Header TMS_20170524-00110_HEADER_CLOSE_ISSUE.sql $
  Module Name: 20170524-00110  Header closed issue

  PURPOSE: Data fix  

  REVISIONS:
  Ver        Date         Author                Description
  ---------  -----------  ------------------    --------------------------
  1.0        25-JUL-2017  Pattabhi Avula        TMS#20170524-00110 

**************************************************************************/ 
SET SERVEROUTPUT ON SIZE 1000000
DECLARE
BEGIN
   DBMS_OUTPUT.put_line ('TMS: 20170524-00110   , Before Update');

UPDATE OE_ORDER_HEADERS_ALL
   SET flow_status_code='BOOKED',
       Booked_Flag='Y'
 WHERE header_id=58749893;

   DBMS_OUTPUT.put_line (
         'TMS: 20170524-00110 Header updated and number of records (Expected:1): '
      || SQL%ROWCOUNT);
	  
   COMMIT;

   DBMS_OUTPUT.put_line ('TMS: 20170524-00110   , End Update');
EXCEPTION
   WHEN OTHERS
   THEN
      ROLLBACK;
      DBMS_OUTPUT.put_line ('TMS: 20170524-00110, Errors : ' || SQLERRM);
END;
/