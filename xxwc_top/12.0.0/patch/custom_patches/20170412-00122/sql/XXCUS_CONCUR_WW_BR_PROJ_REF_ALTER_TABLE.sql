/*
   Ticket#                               Date         Author             Notes
   -------------------------------------------------------------------------------------------------------------------------------------------------------
   TMS 20170412-00122 / ESMS 554216      02/17/2017   Balaguru Seshadri  Concur process  
*/
--
ALTER TABLE XXCUS.XXCUS_CONCUR_WW_BR_PROJ_REF ADD (
  CONSTRAINT XXCUS_CONCUR_WW_BR_PROJ_P1
  PRIMARY KEY
  (ID)
  USING INDEX XXCUS.XXCUS_CONCUR_WW_BR_PROJ_U2
  ENABLE VALIDATE)
;
