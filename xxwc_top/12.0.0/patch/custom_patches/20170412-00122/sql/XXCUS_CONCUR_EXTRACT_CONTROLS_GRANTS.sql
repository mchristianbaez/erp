/*
   Ticket#                               Date         Author             Notes
   -------------------------------------------------------------------------------------------------------------------------------------------------------
   TMS 20170412-00122 / ESMS 554216      02/17/2017   Balaguru Seshadri  Concur process related tables
*/
--
GRANT ALTER, DELETE, INDEX, INSERT, REFERENCES, SELECT, UPDATE, ON COMMIT REFRESH, QUERY REWRITE, DEBUG, FLASHBACK ON XXCUS.XXCUS_CONCUR_EXTRACT_CONTROLS TO EA_APEX WITH GRANT OPTION
;
--