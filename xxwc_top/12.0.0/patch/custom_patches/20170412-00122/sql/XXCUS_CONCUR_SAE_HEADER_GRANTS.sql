/*
   Ticket#                               Date         Author             Notes
   -------------------------------------------------------------------------------------------------------------------------------------------------------
   TMS 20170412-00122 / ESMS 554216      02/17/2017   Balaguru Seshadri  Concur process related tables
*/
--
GRANT DELETE, INSERT, SELECT ON XXCUS.XXCUS_CONCUR_SAE_HEADER TO INTERFACE_DSTAGE;
--