/*************************************************************************
  $Header TMS_20170807-00221_DELETE_LINE.sql $
  Module Name: TMS_20170807-00221 Data Fix for SETTING A DEFAULT CONTACT 

  PURPOSE: Data Fix SETTING A DEFAULT CONTACT

  REVISIONS:
  Ver        Date         Author                Description
  ---------  -----------  ------------------    --------------------------
  1.0        03-OCT-2017  Pattabhi Avula         TMS#20170807-00221

**************************************************************************/ 
SET SERVEROUTPUT ON SIZE 1000000;
DECLARE
BEGIN
   DBMS_OUTPUT.put_line ('TMS: 20170807-00221, Before Update');

DELETE FROM xxwc.xxwc_po_vendor_minimum 
 WHERE vendor_id=10197 
   AND organization_id=950 
   AND FREIGHT_MIN_DOLLAR=0 
   AND default_flag='Y' 
   AND VENDOR_MIN_DOLLAR=0;

-- Expected to delete one row

   DBMS_OUTPUT.put_line (
         'TMS: 20170807-00221 Lines deleted (Expected:1): '
      || SQL%ROWCOUNT);

   COMMIT;

   DBMS_OUTPUT.put_line ('TMS: 20170807-00221   , End delete');
EXCEPTION
   WHEN OTHERS
   THEN
      ROLLBACK;
      DBMS_OUTPUT.put_line ('TMS: 20170807-00221, Errors : ' || SQLERRM);
END;
/