  /*******************************************************************************
  Table: "XXWC"."XXWC_MSSQL_ATTACH_TBL"  
  Description: "XXWC"."XXWC_MSSQL_ATTACH_TBL" 
  HISTORY
  ===============================================================================
  VERSION DATE               AUTHOR(S)       DESCRIPTION
  ------- -----------------  --------------- -----------------------------------------
  1.0     11-JUL-2017        Pahwa Nancy   Task ID: 20170616-00237
  ********************************************************************************/
-- Create table
-- Create table
create table XXWC.XXWC_MSSQL_ATTACH_TBL
(
  file_id      NUMBER,
  document_url VARCHAR2(500),
  CREATION_DATE  date
);
/