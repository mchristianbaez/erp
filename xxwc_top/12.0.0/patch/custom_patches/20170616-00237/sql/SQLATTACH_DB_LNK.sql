--
-- "SQLATTACH"  (Database Link) 

create database link SQLATTACH.HSI.HUGHESSUPPLY.COM
  connect to INTERFACE_EBS identified by "get.data.9049"
  using '(DESCRIPTION=
    (ADDRESS=(PROTOCOL=tcp)(HOST=ghdosg01lps)(PORT=1521))
    (CONNECT_DATA=(SID=sqlimgp))
    (HS=OK))';