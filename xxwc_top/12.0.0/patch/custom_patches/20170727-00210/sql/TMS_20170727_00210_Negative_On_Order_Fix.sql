/******************************************************************************
  $Header TMS_20170727_00210_Negative_On_Order_Fix.sql $
  Module Name:Data Fix script for 20170727_00210

  PURPOSE: Data fix script for 20170801-00089 Order stuck in Close

  REVISIONS:
  Ver        Date         Author                Description
  ---------  -----------  ------------------    -------------------------------
  1.0        09-Aug-2017  Krishna Kumar         20170727_00210

*******************************************************************************/
ALTER SESSION SET CURRENT_SCHEMA=apps;
/

BEGIN
   mo_global.set_policy_context ('S', '162');
END;
/
DECLARE
   l_conc_req_id     NUMBER := fnd_global.conc_request_id;
   l_login_id        NUMBER := fnd_global.login_id;
   l_count           NUMBER;
   l_err_callfrom    VARCHAR2 (100);
   l_err_callpoint   VARCHAR2 (100);
   lv_hdr            VARCHAR2 (2000);
   l_err_msg         VARCHAR2 (2000);

   CURSOR c_req
   IS
      SELECT req_number,
             requisition_header_id,
             requisition_line_id,
             distribution_id,
             line_num,
             req_ln_qty,
             req_dist_qty,
             quantity_delivered,
             order_number,
             line_number,
             so_ln_qty
        FROM (  SELECT prha.segment1 req_number,
                       prha.requisition_header_id,
                       prla.requisition_line_id,
                       prda.distribution_id,
                       prla.line_num,
                       prla.quantity req_ln_qty,
                       prda.req_line_quantity req_dist_qty,
                       prla.quantity_delivered,
                       oha.order_number,
                       ola.line_number,
                       SUM (NVL (ola.shipped_quantity, ola.ordered_quantity))
                          so_ln_qty
                  FROM apps.po_requisition_headers prha,
                       apps.po_requisition_lines prla,
                       apps.po_req_distributions prda,
                       apps.oe_order_headers oha,
                       apps.oe_order_lines ola
                 WHERE     prha.requisition_header_id =
                              prla.requisition_header_id
                       AND prla.requisition_line_id = prda.requisition_line_id
                       AND oha.orig_sys_document_ref = prha.segment1
                       AND ola.orig_sys_document_ref = prha.segment1
                       AND ola.source_document_line_id =
                              prla.requisition_line_id
                       AND ola.inventory_item_id = prla.item_id
                       AND oha.header_id = ola.header_id
                       AND oha.order_type_id = 1011          -- Internal Order
                       AND ola.line_type_id = 1012            -- Internal line
                       --AND   ola.flow_status_code        <> 'CANCELLED'
                       AND NVL (prla.cancel_flag, 'N') = 'N'
                       AND NVL (prha.cancel_flag, 'N') = 'N'
                       AND prha.authorization_status = 'APPROVED'
                       AND prha.transferred_to_oe_flag = 'Y'
                       AND prla.transferred_to_oe_flag = 'Y'
                       AND prha.type_lookup_code = 'INTERNAL'
                       --AND   prla.quantity             = 0
                       --AND prha.segment1 = '13855537'
              -- AND TRUNC (ola.last_update_date) >= TRUNC (SYSDATE) - 5000
              GROUP BY prha.segment1,
                       prha.requisition_header_id,
                       prla.requisition_line_id,
                       prda.distribution_id,
                       prla.line_num,
                       prla.quantity,
                       prda.req_line_quantity,
                       prla.quantity_delivered,
                       oha.order_number,
                       ola.line_number)
       WHERE (   req_ln_qty < QUANTITY_DELIVERED
              OR req_dist_qty < QUANTITY_DELIVERED);
BEGIN
   DBMS_OUTPUT.put_line ('Process begin');

   l_count := 0;

   FOR i IN c_req
   LOOP
      l_count := l_count + 1;
      lv_hdr :=
            RPAD (i.req_number, 19, ' ')
         || ' '
         || RPAD (i.line_num, 21, '  ')
         || ' '
         || RPAD (i.requisition_line_id, 20, '  ')
         || ' '
         || RPAD (i.so_ln_qty, 6, ' ')
         || '  '
         || RPAD (i.req_ln_qty, 10, ' ')
         || '  '
         || RPAD (i.req_dist_qty, 13, ' ');

      DBMS_OUTPUT.put_line ('Record-' || lv_hdr);


      UPDATE apps.po_requisition_lines
         SET quantity = i.QUANTITY_DELIVERED
       WHERE     requisition_line_id = i.requisition_line_id
             AND requisition_header_id = i.requisition_header_id;

      UPDATE apps.po_req_distributions
         SET req_line_quantity = i.QUANTITY_DELIVERED
       WHERE     requisition_line_id = i.requisition_line_id
             AND distribution_id = i.distribution_id;
   END LOOP;

   COMMIT;

   DBMS_OUTPUT.put_line ('Number of Requisition lines updated = ' || l_count);
EXCEPTION
   WHEN OTHERS
   THEN
      l_err_msg := ' ERROR ' || SQLCODE || SUBSTR (SQLERRM, 1, 2000);
      DBMS_OUTPUT.put_line ('Error is-' || l_err_msg);
END;
/