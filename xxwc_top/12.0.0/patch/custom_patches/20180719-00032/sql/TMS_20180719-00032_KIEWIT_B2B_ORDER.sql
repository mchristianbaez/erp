/*************************************************************************
 $Header TMS_20180719-00032_KIEWIT_B2B_ORDER.sql $
 Module Name: TMS_20180719-00032_KIEWIT_B2B_ORDER.sql

 PURPOSE: To clean the Duplicate Orders Kiewit 4501183438 in b2b process

 REVISIONS:
 Ver        Date       Author           Description
 --------- ----------  ---------------  -------------------------
 1.0       07/19/2018  Pattabhi Avula   TMS#20180719-00032 - Correct Kiewit B2B Order
 **************************************************************************/
SET SERVEROUTPUT ON SIZE 1000000;
DECLARE
ln_count    NUMBER;
BEGIN
 DBMS_OUTPUT.put_line ('TMS: Datafix script , Before Update');
 
 -- Updating the b2b headers table
	UPDATE xxwc.xxwc_b2b_so_hdrs_stg_tbl
       SET process_flag = 'S'
     WHERE customer_po_number = '4501183438';

	COMMIT;
     ln_count:= SQL%ROWCOUNT;
DBMS_OUTPUT.put_line ('Records update-' ||ln_count);

	  DBMS_OUTPUT.put_line ('TMS: 20180719-00032  , End Update');
EXCEPTION
 WHEN OTHERS THEN
 DBMS_OUTPUT.put_line ('TMS: 20180719-00032 , Errors : ' || SQLERRM);
END;
/