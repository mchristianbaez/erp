--
-- XXWC_EBS_EDW_INTF_PKG  (Package) 
--
CREATE OR REPLACE PACKAGE APPS.xxwc_ebs_edw_intf_pkg
AS
/*************************************************************************
 Copyright (c) 2012 Lucidity Consulting Group
 All rights reserved.
**************************************************************************
  $Header xxwc_ebs_edw_intf_pkg $
  Module Name: xxwc_ebs_edw_intf_pkg.pks

  PURPOSE:

  REVISIONS:
  Ver        Date        Author                     Description
  ---------  ----------  ---------------         -------------------------
  1.0        04/23/2012  Consuelo Gonzalez      Initial Version
  2.0        10/03/2012  Consuelo Gonzalez      Corrected issue with default line size
                                                in the utl_file.open call to the max
                                                line size of 32676. Default calc based
                                                on OS sets it to 1024    
  3.0        01/22/2013  Consuelo Gonzalez      Changed request wait time from 3600 to
                                                9000 secs 
  3.1       7/19/2016   Neha Saini             TMS# 20160629-00017 EDW Extract was taking long time to run removing changes made for AIS to handle the performance of EDW.
                                               
**************************************************************************/
--ver3.1 starts
  /*************************************************************************
     $Header get_market_list_price $
     Module Name: get_market_list_price

     PURPOSE:   This function is used for the extracting the Market list Pricing

     REVISIONS:
     Ver        Date        Author                     Description
     ---------  ----------  ---------------         -------------------------
    10.1        07/18/2016   Neha Saini              TMS#20160629-00017 -- Initial Creation
   **************************************************************************/


   FUNCTION get_market_list_price (p_inventory_item_id   IN NUMBER,
                                   p_organization_id     IN NUMBER)
      RETURN NUMBER;
 --ver3.1 ends
 
   /*************************************************************************
     Procedure : gen_extract_file

     PURPOSE:   This procedure creates file for the open invoices to
                Bill Trust
     Parameter:

   ************************************************************************/
   PROCEDURE gen_extract_file (
      errbuf             OUT      VARCHAR2,
      retcode            OUT      VARCHAR2,
      p_operating_unit   IN       NUMBER,
      p_directory_name   IN       VARCHAR2,
      p_source_name      IN       VARCHAR2,
      p_from_date        IN       VARCHAR2,
      p_to_date          IN       VARCHAR2
   );

   TYPE xxwc_ebs_edw_rec_type IS RECORD (
      org_id           NUMBER,
      interface_date   DATE,
      rec_line         VARCHAR2 (32767)
   
   --rec_line2       VARCHAR2(32767)
   );

   TYPE xxwc_ebs_edw_rec_tbl_type IS TABLE OF xxwc_ebs_edw_rec_type
      INDEX BY BINARY_INTEGER;

    /*************************************************************************
   *   Procedure : submit_job
   *
   *   PURPOSE:   This procedure is called by UC4 to initiate EDW Interfaces
   *   Parameter:
   *          IN
   *              p_debug_msg      -- Debug Message
   * ************************************************************************/
   PROCEDURE submit_job (
      errbuf                  OUT      VARCHAR2,
      retcode                 OUT      VARCHAR2,
      p_user_name             IN       VARCHAR2,
      p_responsibility_name   IN       VARCHAR2,
      p_operating_unit_name   IN       VARCHAR2,
      p_ob_directory_name     IN       VARCHAR2,
      p_objects_source        IN       VARCHAR2,
      p_from_date             IN       VARCHAR2,
      p_to_date               IN       VARCHAR2
   );
END xxwc_ebs_edw_intf_pkg;
/


GRANT EXECUTE, DEBUG ON APPS.XXWC_EBS_EDW_INTF_PKG TO INTERFACE_PRISM;

GRANT EXECUTE, DEBUG ON APPS.XXWC_EBS_EDW_INTF_PKG TO INTERFACE_XXCUS;

