CREATE OR REPLACE PACKAGE BODY APPS.XXWC_EBS_EDW_INTF_PKG
AS
   /*************************************************************************
    Copyright (c) 2012 Lucidity Consulting Group
    All rights reserved.
   **************************************************************************
     $Header xxwc_ebs_edw_intf_pkg $
     Module Name: xxwc_ebs_edw_intf_pkg.pks

     PURPOSE:

     REVISIONS:
     Ver        Date        Author                     Description
     ---------  ----------  ---------------         -------------------------
     1.0        04/23/2012  Consuelo Gonzalez      Initial Version
     2.0        10/03/2012  Consuelo Gonzalez      Corrected issue with default line size
                                                   in the utl_file.open call to the max
                                                   line size of 32676. Default calc based
                                                   on OS sets it to 1024
     3.0        01/22/2013  Consuelo Gonzalez      Changed request wait time from 3600 to
                                                   9000 secs
     4.0        06/20/2013  Gopi Damuluri          Changed request wait time from 9000 to
                                                   10800 secs. TMS# 20130620-01197
     4.1        07/31/2013  Gopi Damuluri          Changed request wait time from 9000 to
                                                   12600 secs. TMS# 20130715-00548
     4.2        08/18/2013  Gopi Damuluri          Changed request wait time from 12600 to
                                                   16200 secs. TMS# 20130819-00511
     4.3        08/30/2013  Gopi Damuluri          Changed request wait time from 16200 to
                                                   21600 secs. ESMS# 220756
     4.4        09/09/2013  Gopi Damuluri          Changed request wait time from 21600 to
                                                   99999 secs. ESMS# 221693
     5.0        15/10/2013  Mahesh Kudle           Added script to populate temp table
                                                   TMS#20130626-01200
     6.0        15/11/2013  Gopi Damuluri          TMS# 20131108-00103
                                                   Performance tuning for PURCHASEDETAIL extract
     7.0        12/16/2014  Maharajan Shunmugam    TMS# 20141002-00066 - Canada Multi org Changes
     8.0        02/08/2015  Gopi Damuluri          TMS# 20141212-00194 Tuning of EComm Organization/Customer Extract
     9.0        17/08/2015  M Hari Prasad          TMS# 20150802-00015 - Extract - EDW MasterProduct extract failure
     10.0       1/7/2016    Neha Saini             TMS# 20151019-00172 Weekly Spend File - 201537 adding trunc while comparing dates and uncommenting code
     10.1       7/19/2016   Neha Saini             TMS# 20160629-00017 EDW Extract was taking long time to run removing changes made for AIS to handle the performance of EDW.
   **************************************************************************/
--ver10.1 starts
   G_MARKET_NTL_PRL_ID   NUMBER
      := fnd_profile.VALUE ('XXWC_QP_MARKET_NATIONAL_PRL');
   G_MARKET_CAT_PRL_ID   NUMBER
      := fnd_profile.VALUE ('XXWC_QP_MARKET_CATEGORY_PRL');
--ver10.1 ends

   /*************************************************************************
     Procedure : Write_Log

     PURPOSE:   This procedure logs debug message in Concurrent Log file
     Parameter:
            IN
                p_debug_msg      -- Debug Message
   ************************************************************************/
   PROCEDURE write_log (p_debug_msg IN VARCHAR2)
   IS
   BEGIN
      fnd_file.put_line (fnd_file.LOG, p_debug_msg);
      DBMS_OUTPUT.put_line (p_debug_msg);
   END write_log;

   /*************************************************************************
      Procedure : Write_Error

     PURPOSE:   This procedure logs error message
     Parameter:
            IN
                p_debug_msg      -- Debug Message
   ************************************************************************/

   --add message to concurrent output file
   PROCEDURE write_error (p_debug_msg IN VARCHAR2)
   IS
      l_req_id          NUMBER := fnd_global.conc_request_id;
      l_err_callfrom    VARCHAR2 (75) DEFAULT 'XXWC_EBS_EDW_INTF_PKG';
      l_err_callpoint   VARCHAR2 (75) DEFAULT 'START';
      l_distro_list     VARCHAR2 (75)
                           DEFAULT 'HDSOracleDevelopers@hdsupply.com';
   BEGIN
      xxcus_error_pkg.xxcus_error_main_api (
         p_called_from         => l_err_callfrom,
         p_calling             => l_err_callpoint,
         p_request_id          => l_req_id,
         p_ora_error_msg       => SUBSTR (p_debug_msg, 1, 2000),
         p_error_desc          => 'Error running XXWC_EBS_EDW_INTF_PKG',
         p_distribution_list   => l_distro_list,
         p_module              => 'FND');
   END write_error;

   --ver 10.1 Starts
   /*************************************************************************
     $Header get_market_list_price $
     Module Name: get_market_list_price

     PURPOSE:   This function is used for the extracting the Market list Pricing.
	            This is a temporary Fix . we created a TMS Task ID: 20160721-00148 to 
				take care of bug permanently in common package APPS.XXWC_QP_MARKET_PRICE_UTIL_PKG.GET_MARKET_LIST_PRICE.
				And remove it later.

     REVISIONS:
     Ver        Date        Author                     Description
     ---------  ----------  ---------------         -------------------------
    10.1        07/18/2016   Neha Saini              TMS#20160629-00017 -- Initial Creation
   **************************************************************************/

   FUNCTION get_market_list_price (p_inventory_item_id   IN NUMBER,
                                   p_organization_id     IN NUMBER)
      RETURN NUMBER
   IS
      l_list_price   NUMBER;
   BEGIN
      l_list_price := NULL;

      BEGIN
         SELECT ll.operand
           INTO l_list_price
           FROM apps.qp_list_headers_b lh,
                apps.qp_qualifiers qq,
                apps.qp_list_lines ll,
                apps.qp_pricing_attributes qpa
          WHERE     lh.list_type_code = 'PRL'
                AND lh.list_header_id NOT IN (G_MARKET_NTL_PRL_ID,
                                              G_MARKET_CAT_PRL_ID)
                AND lh.attribute10 = 'Market Price List'
                AND lh.list_header_id = qq.list_header_id
                AND qq.list_line_id = -1
                AND qq.qualifier_context = 'ORDER'
                AND qq.qualifier_attribute = 'QUALIFIER_ATTRIBUTE18'
                AND qq.qualifier_attr_value = TO_CHAR (p_organization_id)
                AND SYSDATE BETWEEN NVL (qq.start_date_active, SYSDATE - 1)
                                AND NVL (qq.end_date_active, SYSDATE + 1)
                AND lh.list_header_id = ll.list_header_id
                AND SYSDATE BETWEEN NVL (ll.start_date_active, SYSDATE - 1)
                                AND NVL (ll.end_date_active, SYSDATE + 1)
                AND ll.list_header_id = qpa.list_header_id
                AND ll.qualification_ind = qpa.qualification_ind
                AND ll.pricing_phase_id = qpa.pricing_phase_id
                AND ll.list_line_id = qpa.list_line_id
                AND qpa.product_attribute_context = 'ITEM'
                AND qpa.product_attribute = 'PRICING_ATTRIBUTE1'
                AND qpa.product_attribute_datatype = 'C'
                AND qpa.product_attr_value = TO_CHAR (p_inventory_item_id)
                AND ROWNUM = 1;
      EXCEPTION
         WHEN OTHERS
         THEN
            BEGIN
               SELECT ll.operand
                 INTO l_list_price
                 FROM apps.qp_list_headers_b lh,
                      apps.qp_list_lines ll,
                      apps.qp_pricing_attributes qpa
                WHERE     lh.list_header_id = G_MARKET_NTL_PRL_ID
                      AND lh.list_header_id = ll.list_header_id
                      AND SYSDATE BETWEEN NVL (ll.start_date_active,
                                               SYSDATE - 1)
                                      AND NVL (ll.end_date_active,
                                               SYSDATE + 1)
                      AND ll.list_header_id = qpa.list_header_id
                      AND ll.qualification_ind = qpa.qualification_ind
                      AND ll.pricing_phase_id = qpa.pricing_phase_id
                      AND ll.list_line_id = qpa.list_line_id
                      AND qpa.product_attribute_context = 'ITEM'
                      AND qpa.product_attribute = 'PRICING_ATTRIBUTE1'
                      AND qpa.product_attribute_datatype = 'C'
                      AND qpa.product_attr_value =
                             TO_CHAR (p_inventory_item_id)
                      AND ROWNUM = 1;
            EXCEPTION
               WHEN OTHERS
               THEN
                  BEGIN
                     SELECT ll.operand
                       INTO l_list_price
                       FROM apps.qp_list_headers_b lh,
                            apps.qp_list_lines ll,
                            apps.qp_pricing_attributes qpa
                      WHERE     lh.list_header_id = G_MARKET_CAT_PRL_ID
                            AND lh.list_header_id = ll.list_header_id
                            AND SYSDATE BETWEEN NVL (ll.start_date_active,
                                                     SYSDATE - 1)
                                            AND NVL (ll.end_date_active,
                                                     SYSDATE + 1)
                            AND ll.list_header_id = qpa.list_header_id
                            AND ll.qualification_ind = qpa.qualification_ind
                            AND ll.pricing_phase_id = qpa.pricing_phase_id
                            AND ll.list_line_id = qpa.list_line_id
                            AND qpa.product_attribute_context = 'ITEM'
                            AND qpa.product_attribute = 'PRICING_ATTRIBUTE1'
                            AND qpa.product_attribute_datatype = 'C'
                            AND qpa.product_attr_value =
                                   TO_CHAR (p_inventory_item_id)
                            AND ROWNUM = 1;
                  EXCEPTION
                     WHEN OTHERS
                     THEN
                        l_list_price := 20000;
                  END;
            END;
      END;

      RETURN NVL (l_list_price, 0);
   EXCEPTION
      WHEN OTHERS
      THEN
         RETURN 20000;
   END get_market_list_price;

   --version 10.1 ends
   /*************************************************************************
     Procedure : gen_extract_file

     PURPOSE:   This procedure creates files for the EBS to EDW interface
               based on the source view name
     Parameter:
     REVISIONS:
     Ver        Date        Author                     Description
     ---------  ----------  ---------------         -------------------------
     8.0        02/08/2015  Gopi Damuluri          TMS# 20141212-00194 Tuning of EComm Organization/Customer Extract
     9.0        17/08/2015  M Hari Prasad          TMS# 20150802-00015 - Extract - EDW MasterProduct extract failure
     10.0       1/7/2016    Neha Saini             TMS# 20151019-00172 Weekly Spend File - 201537 adding trunc while comparing dates and uncommenting code
   ************************************************************************/
   PROCEDURE gen_extract_file (errbuf                OUT VARCHAR2,
                               retcode               OUT VARCHAR2,
                               p_operating_unit   IN     NUMBER,
                               p_directory_name   IN     VARCHAR2,
                               p_source_name      IN     VARCHAR2,
                               p_from_date        IN     VARCHAR2,
                               p_to_date          IN     VARCHAR2)
   IS
      -- Intialize Variables
      l_err_msg                VARCHAR2 (2000);
      l_err_code               NUMBER;
      l_sec                    VARCHAR2 (150);
      l_procedure_name         VARCHAR2 (75)
                                  := 'xxwc_ebs_edw_intf_pkg.gen_extract_file';

      -- Reference Cursor Variables
      TYPE ref_cur IS REF CURSOR;

      view_cur                 ref_cur;
      view_rec                 xxwc_ebs_edw_intf_pkg.xxwc_ebs_edw_rec_type;
      --File Variables
      l_filehandle             UTL_FILE.file_type;
      l_filename               VARCHAR2 (240);
      l_base_filename          VARCHAR2 (240);
      l_view_name              VARCHAR2 (100);
      l_org_id                 NUMBER;
      l_org_name               VARCHAR2 (240);
      l_from_date              DATE;
      l_to_date                DATE;
      l_count                  NUMBER;
      l_query                  VARCHAR2 (2000);
      l_base_file_exists       BOOLEAN;
      l_base_file_length       NUMBER;
      l_base_file_block_size   BINARY_INTEGER;
      -- Concurrent Job Variables
      l_req_id                 NUMBER NULL;
      v_phase                  VARCHAR2 (50);
      v_status                 VARCHAR2 (50);
      v_dev_status             VARCHAR2 (50);
      v_dev_phase              VARCHAR2 (50);
      v_message                VARCHAR2 (250);
      v_error_message          VARCHAR2 (3000);
      l_statement              VARCHAR2 (9000);
      -- Exceptions
      no_base_view             EXCEPTION;
      mult_source_objects      EXCEPTION;
      other_base_err           EXCEPTION;
      no_dos_file_err          EXCEPTION;

      --l_err_rec              VARCHAR2(2000); -- ????? Commented for v 9.0
      l_err_rec                VARCHAR2 (32767);             --Added for v 9.0
   BEGIN
      write_log ('EBS to EDW Interface File Generation Process');
      write_log ('========================================================');
      write_log ('  ');
      write_log ('Parameters:');
      write_log ('Operating Unit ID: ' || p_operating_unit);
      write_log ('Directory: ' || p_directory_name);
      write_log ('View Source: ' || p_source_name);
      write_log ('From Date: ' || p_from_date);
      write_log ('To Date: ' || p_to_date);
      write_log ('========================================================');
      write_log ('  ');
      --Initialize the Out Parameter
      errbuf := '';
      retcode := '0';
      -- Finding Base View Name
      l_view_name := NULL;

      BEGIN
         SELECT ffvv.description
           INTO l_view_name
           FROM fnd_flex_value_sets ffvs, fnd_flex_values_vl ffvv
          WHERE     ffvs.flex_value_set_name = 'XXWC_EBS_INTERFACE_SOURCES'
                AND ffvs.flex_value_set_id = ffvv.flex_value_set_id
                AND ffvv.flex_value = p_source_name
                AND NVL (ffvv.enabled_flag, 'N') = 'Y';
      EXCEPTION
         WHEN NO_DATA_FOUND
         THEN
            l_view_name := NULL;
            RAISE no_base_view;
         WHEN TOO_MANY_ROWS
         THEN
            l_view_name := NULL;
            RAISE mult_source_objects;
         WHEN OTHERS
         THEN
            l_view_name := NULL;
            RAISE other_base_err;
      END;

      IF l_view_name IS NULL
      THEN
         RAISE no_base_view;
      END IF;

      -- Building file name
      l_filename :=
            'WHITECAP_'
         || p_source_name
         || '_'
         || TO_CHAR (SYSDATE, 'YYYYMMDD')
         || '.ld';
      l_base_filename := l_filename;
      l_filename := 'TMP_' || l_filename;
      -- Finding Operating Unit Name
      l_org_name := NULL;

      BEGIN
         SELECT NAME
           INTO l_org_name
           FROM hr_operating_units hou
          WHERE hou.organization_id = p_operating_unit;
      EXCEPTION
         WHEN OTHERS
         THEN
            l_org_name := NULL;
      END;

      write_log ('Operating Unit Name: ' || l_org_name);
      write_log ('View Name: ' || l_view_name);
      write_log ('File Name: ' || l_filename);
      write_log ('File Name Post DOS Conversion: ' || l_base_filename);
      write_log ('  ');
      write_log ('Opening the File handler');
      --Open the file handler
      -- 10/06/2012 CG: Changed to use the overloaded function to process lines longer than 1023 chars
      /*l_filehandle :=
         UTL_FILE.fopen (LOCATION       => p_directory_name,
                         filename       => l_filename,
                         open_mode      => 'w'
                        );*/

      l_filehandle :=
         UTL_FILE.fopen (LOCATION       => p_directory_name,
                         filename       => l_filename,
                         open_mode      => 'w',
                         max_linesize   => 32767);

      -- Building out query
      write_log ('  ');
      write_log ('Building Query');
      l_query := NULL;

      --Initialize org_id for ver# 5.0

      mo_global.set_policy_context ('S', p_operating_unit);

      -- Version# 8.0 > Start
      IF l_view_name IS NOT NULL AND p_source_name = 'CUSTOMER'
      THEN
         BEGIN
            --            UPDATE XXWC.XXWC_AR_CUSTOMERS_MV_TBL stg
            --            SET stg.firstsaledate = xxwc_mv_routines_pkg.get_first_sale_date (stg.cust_account_id)
            --              , stg.lastsaledate = xxwc_mv_routines_pkg.get_last_sale_date (stg.cust_account_id)
            --            WHERE 1 = 1;

            UPDATE XXWC.XXWC_AR_CUSTOMERS_MV_TBL stg
               SET (stg.firstsaledate, stg.lastsaledate) =
                      (SELECT MAX (ooha.ordered_date)
                              KEEP (DENSE_RANK FIRST ORDER BY ordered_date)
                                 first_sale_date,
                              MAX (ooha.ordered_date)
                                 KEEP (DENSE_RANK LAST ORDER BY ordered_date)
                                 last_sale_date
                         FROM oe_order_headers_all ooha
                        WHERE     ooha.sold_to_org_id = stg.cust_account_id
                              AND NVL (ooha.cancelled_flag, 'N') = 'N'
                              AND NVL (ooha.booked_flag, 'N') = 'Y')
             WHERE 1 = 1;
         EXCEPTION
            WHEN OTHERS
            THEN
               NULL;
         END;
      END IF;

      -- Version# 8.0 < End

      --***added by Mahesh to populate temp data for tuning product location extract TMS#20130626-01200
      IF l_view_name IS NOT NULL AND p_source_name = 'BRANCH_PROD'
      THEN
         BEGIN
            apps.xxwc_ebs_edw_prod_loc_pkg.populate_inv_prod_loc_temp (
               l_err_msg,
               l_err_code);
         END;
      END IF;

      --***end

      IF p_operating_unit IS NOT NULL
      THEN
         l_query :=
               'SELECT * FROM '
            || l_view_name
            || ' WHERE operating_unit_id = '
            || p_operating_unit;
      ELSE
         l_query := 'SELECT * FROM ' || l_view_name;
      END IF;

      write_log ('Section 1 built...');
      write_log ('l_query ' || l_query);
      l_from_date := NULL;
      l_to_date := NULL;

      IF    (p_from_date IS NOT NULL AND p_to_date IS NULL)
         OR (p_from_date IS NULL AND p_to_date IS NOT NULL)
      THEN
         write_log (
            'One date parameter provided, please provide from and to');
         write_log ('  ');
      ELSE
         l_from_date := TRUNC (TO_DATE (p_from_date, 'YYYY/MM/DD HH24:MI:SS'));
         l_to_date := TRUNC (TO_DATE (p_to_date, 'YYYY/MM/DD HH24:MI:SS'));
      END IF;

      --***added by Rasikha to populate temp data set for tuning invoice line extract
      IF     l_view_name IS NOT NULL
         AND p_source_name = 'INVOICELINE'
         AND p_from_date IS NOT NULL
         AND p_to_date IS NOT NULL
      THEN
         BEGIN
            xxwc_mv_routines_add_pkg.populate_xxwc_ar_inv_line_temp (
               TRUNC (TO_DATE (p_from_date, 'YYYY/MM/DD HH24:MI:SS')),
               TRUNC (TO_DATE (p_to_date, 'YYYY/MM/DD HH24:MI:SS')));
         END;
      END IF;

      --end --***added by Rasikha

      -- Version# 6.0 > Start
      IF     l_view_name IS NOT NULL
         AND p_source_name = 'PURCHASEDETAIL'
         AND p_from_date IS NOT NULL
         AND p_to_date IS NOT NULL
      THEN
         BEGIN
            EXECUTE IMMEDIATE 'truncate table xxwc.xxwc_mtl_material_trx_tbl';

            INSERT /*+ append */
                  INTO  xxwc.xxwc_mtl_material_trx_tbl
               SELECT *
                 FROM apps.mtl_material_transactions
                WHERE     1 = 1
                      AND transaction_type_id = 102                     -- RTV
                      AND TRUNC (creation_date) BETWEEN TRUNC (
                                                           TO_DATE (
                                                              p_from_date,
                                                              'YYYY/MM/DD HH24:MI:SS'))
                                                    AND TRUNC (
                                                           TO_DATE (
                                                              p_to_date,
                                                              'YYYY/MM/DD HH24:MI:SS')) --ver#10.0 added trunc
               UNION                              --Added for ver#10.0 <<Start
               SELECT *
                 FROM apps.mtl_material_transactions mmts
                WHERE     1 = 1
                      AND transaction_type_id = 41 --Account alias receipt (Inventory Type Adjustment In)
                      AND EXISTS
                             (SELECT 1
                                FROM apps.mtl_material_transactions mmt
                               WHERE     transaction_type_id = 102
                                     AND mmt.attribute_category =
                                            mmts.attribute_category
                                     AND mmt.attribute1 = mmts.attribute1
                                     AND mmt.attribute5 = mmts.attribute5
                                     AND mmt.attribute6 = mmts.attribute6)
                      AND TRUNC (creation_date) BETWEEN TRUNC (
                                                           TO_DATE (
                                                              p_from_date,
                                                              'YYYY/MM/DD HH24:MI:SS'))
                                                    AND TRUNC (
                                                           TO_DATE (
                                                              p_to_date,
                                                              'YYYY/MM/DD HH24:MI:SS'));

            --Added for ver#10.0 <<End
            COMMIT;
         EXCEPTION
            WHEN OTHERS
            THEN
               l_statement :=
                  'Error while inserting into temporary table - XXWC.XXWC_MTL_MATERIAL_TRX_TBL';
               write_log (l_statement);
               RAISE PROGRAM_ERROR;
         END;
      END IF;

      -- Version# 6.0 < End

      IF l_from_date IS NOT NULL
      THEN
         IF p_operating_unit IS NOT NULL
         THEN
            l_query :=
                  l_query
               || ' and trunc(interface_date) between trunc(to_date('
               || ''''
               || p_from_date
               || ''''
               || ','
               || ''''
               || 'YYYY/MM/DD HH24:MI:SS'
               || ''''
               || ')) and trunc(to_date('
               || ''''
               || p_to_date
               || ''''
               || ','
               || ''''
               || 'YYYY/MM/DD HH24:MI:SS'
               || ''''
               || '))';
         ELSE
            l_query :=
                  l_query
               || ' where trunc(interface_date) between trunc(to_date('
               || ''''
               || p_from_date
               || ''''
               || ','
               || ''''
               || 'YYYY/MM/DD HH24:MI:SS'
               || ''''
               || ')) and trunc(to_date('
               || ''''
               || p_to_date
               || ''''
               || ','
               || ''''
               || 'YYYY/MM/DD HH24:MI:SS'
               || ''''
               || '))';
         END IF;
      END IF;

      write_log ('Section 2 built...');
      write_log ('l_query ' || l_query);

      OPEN view_cur FOR l_query;

      write_log ('Writing to the file ... Opening Cursor');
      l_count := 0;

      LOOP
         FETCH view_cur INTO view_rec;

         EXIT WHEN view_cur%NOTFOUND;

         l_err_rec := view_rec.rec_line;                              -- ?????
         UTL_FILE.put_line (l_filehandle, UPPER (view_rec.rec_line));
         l_count := l_count + 1;
      END LOOP;

      write_log ('Wrote ' || l_count || ' records to file');
      write_log ('Closing File Handler');
      --Closing the file handler
      UTL_FILE.fclose (l_filehandle);
      write_log (' ');
      write_log (
         'Submitting program XXWC EBS to EDW File Conversion from Unix to DOS');
      -- Add call to concurrent job to convert the file to DOS format
      l_req_id :=
         fnd_request.submit_request (
            application   => 'XXWC',
            program       => 'XXWC_EBS_EDW_LINUX_DOS',
            description   => NULL,
            start_time    => SYSDATE,
            sub_request   => FALSE,
            argument1     => l_filename,
            argument2     => l_base_filename,
            argument3     => p_directory_name);
      COMMIT;
      write_log (
            'Request Submitted for XXWC EBS to EDW File Conversion from Unix to DOS. Request ID: '
         || l_req_id);

      IF (l_req_id != 0)
      THEN
         IF fnd_concurrent.wait_for_request (l_req_id,
                                             6,
                                             900            -- 15 min max wait
                                                ,
                                             v_phase,
                                             v_status,
                                             v_dev_phase,
                                             v_dev_status,
                                             v_message)
         THEN
            v_error_message :=
                  'ReqID:'
               || l_req_id
               || '-DPhase:'
               || v_dev_phase
               || '-DStatus:'
               || v_dev_status
               || CHR (10)
               || 'MSG:'
               || v_message;

            -- Error Returned
            IF v_dev_phase != 'COMPLETE' OR v_dev_status != 'NORMAL'
            THEN
               l_statement :=
                     'EBS Conc Prog Completed with problems '
                  || v_error_message
                  || '.';
               write_log (l_statement);
               RAISE PROGRAM_ERROR;
            ELSE
               -- Normal Completion Continuing processing
               -- Verifying if final file exists
               write_log ('Verifying if temp file converted to a DOS file');
               UTL_FILE.fgetattr (LOCATION      => p_directory_name,
                                  filename      => l_base_filename,
                                  fexists       => l_base_file_exists,
                                  file_length   => l_base_file_length,
                                  block_size    => l_base_file_block_size);

               IF l_base_file_exists
               THEN
                  -- Final File exists, removing original TMP file
                  write_log (
                        'File '
                     || l_base_filename
                     || ' exists compatible with DOS systems. Removing file '
                     || l_filename);
                  UTL_FILE.fremove (LOCATION   => p_directory_name,
                                    filename   => l_filename);
               ELSE
                  write_log (
                     'Final base file ' || l_base_filename || ' not found.');
                  RAISE no_dos_file_err;
               END IF;
            END IF;
         ELSE
            l_statement :=
               'EBS Conc Program Wait timed out for Conversion to DOS File';
            write_log (l_statement);
            RAISE PROGRAM_ERROR;
         END IF;
      ELSE
         l_statement :=
            'EBS Conc Program not initated for Conversion to DOS File';
         write_log (l_statement);
         RAISE PROGRAM_ERROR;
      END IF;

      COMMIT;
   EXCEPTION
      WHEN PROGRAM_ERROR
      THEN
         retcode := '2';
         errbuf :=
            SUBSTR ( (l_err_msg || ' ERROR ' || SUBSTR (SQLERRM, 1, 2000)),
                    1,
                    2000);
         write_error (errbuf);
         write_log (errbuf);
      WHEN UTL_FILE.invalid_path
      THEN
         UTL_FILE.fclose (l_filehandle);
         errbuf := 'File Path is invalid.';
         retcode := '2';
         write_error (errbuf);
         write_log (errbuf);
      WHEN UTL_FILE.invalid_mode
      THEN
         UTL_FILE.fclose (l_filehandle);
         errbuf := 'The open_mode parameter in FOPEN is invalid.';
         retcode := '2';
         write_error (errbuf);
         write_log (errbuf);
      WHEN UTL_FILE.invalid_filehandle
      THEN
         UTL_FILE.fclose (l_filehandle);
         errbuf := 'File handle is invalid..';
         retcode := '2';
         write_error (errbuf);
         write_log (errbuf);
      WHEN UTL_FILE.invalid_operation
      THEN
         UTL_FILE.fclose (l_filehandle);
         errbuf := 'File could not be opened or operated on as requested';
         retcode := '2';
         write_error (errbuf);
         write_log (errbuf);
      WHEN UTL_FILE.write_error
      THEN
         UTL_FILE.fclose (l_filehandle);
         errbuf :=
            'Operating system error occurred during the write operation';
         retcode := '2';
         write_error (errbuf);
         write_log (errbuf);
      WHEN UTL_FILE.internal_error
      THEN
         UTL_FILE.fclose (l_filehandle);
         write_log ('Unspecified PL/SQL error.');
      WHEN UTL_FILE.file_open
      THEN
         UTL_FILE.fclose (l_filehandle);
         errbuf := 'The requested operation failed because the file is open.';
         retcode := '2';
         write_error (errbuf);
         write_log (errbuf);
      WHEN UTL_FILE.invalid_filename
      THEN
         UTL_FILE.fclose (l_filehandle);
         errbuf := 'The filename parameter is invalid.';
         retcode := '2';
         write_error (errbuf);
         write_log (errbuf);
      WHEN UTL_FILE.access_denied
      THEN
         UTL_FILE.fclose (l_filehandle);
         errbuf := 'Permission to access to the file location is denied.';
         retcode := '2';
         write_error (errbuf);
         write_log (errbuf);
      WHEN no_base_view
      THEN
         errbuf := 'Base view not found for source';
         retcode := '2';
         write_error (errbuf);
         write_log (errbuf);
      WHEN mult_source_objects
      THEN
         errbuf := 'Multiple Base views not found for source';
         retcode := '2';
         write_error (errbuf);
         write_log (errbuf);
      WHEN other_base_err
      THEN
         errbuf := 'Error finding base view: ' || SQLERRM;
         retcode := '2';
         write_error (errbuf);
         write_log (errbuf);
      WHEN no_dos_file_err
      THEN
         errbuf :=
            'Final file compatible with DOS systems not generated please review TMP File';
         retcode := '2';
         write_error (errbuf);
         write_log (errbuf);
      WHEN OTHERS
      THEN
         UTL_FILE.fclose (l_filehandle);
         errbuf := 'Error Msg :' || SQLERRM;
         retcode := '2';
         write_error (errbuf);
         write_log (errbuf);
         write_log (l_err_rec);                                       -- ?????
   END gen_extract_file;

   /*************************************************************************
   *   Procedure : submit_job
   *
   *   PURPOSE:   This procedure is called by UC4 to initiate EDW Interfaces
   *   Parameter:
   *          IN
   *              p_debug_msg      -- Debug Message
   * ************************************************************************/
   PROCEDURE submit_job (errbuf                     OUT VARCHAR2,
                         retcode                    OUT VARCHAR2,
                         p_user_name             IN     VARCHAR2,
                         p_responsibility_name   IN     VARCHAR2,
                         p_operating_unit_name   IN     VARCHAR2,
                         p_ob_directory_name     IN     VARCHAR2,
                         p_objects_source        IN     VARCHAR2,
                         p_from_date             IN     VARCHAR2,
                         p_to_date               IN     VARCHAR2)
   IS
      -- Variable definitions
      l_package               VARCHAR2 (50) := 'XXWC_EBS_EDW_INTF_PKG';
      l_req_id                NUMBER NULL;
      v_phase                 VARCHAR2 (50);
      v_status                VARCHAR2 (50);
      v_dev_status            VARCHAR2 (50);
      v_dev_phase             VARCHAR2 (50);
      v_message               VARCHAR2 (250);
      v_error_message         VARCHAR2 (3000);
      l_err_msg               VARCHAR2 (3000);
      l_err_code              NUMBER;
      l_statement             VARCHAR2 (9000);
      l_user_id               NUMBER;
      l_responsibility_id     NUMBER;
      l_resp_application_id   NUMBER;
      l_org_id                NUMBER;
      l_err_callfrom          VARCHAR2 (75) DEFAULT 'XXWC_EBS_EDW_INTF_PKG';
      l_err_callpoint         VARCHAR2 (75) DEFAULT 'SUBMIT_JOB';
      l_distro_list           VARCHAR2 (75)
                                 DEFAULT 'HDSOracleDevelopers@hdsupply.com';
      l_argument1             VARCHAR2 (240);
      l_argument2             VARCHAR2 (240);
      l_argument3             VARCHAR2 (240);
      l_argument4             VARCHAR2 (240);
      l_argument5             VARCHAR2 (240);
      l_valid                 VARCHAR2 (1);
      -- 06/24/2012 CG Modified to use profile values to pull date ranges when needed
      l_profile_name          VARCHAR2 (240);
      l_profile_value         NUMBER;
   BEGIN
      DBMS_OUTPUT.put_line ('Entering submit_job...');
      DBMS_OUTPUT.put_line ('p_user_name: ' || p_user_name);
      DBMS_OUTPUT.put_line (
         'p_responsibility_name: ' || p_responsibility_name);
      DBMS_OUTPUT.put_line (
         'p_operating_unit_name: ' || p_operating_unit_name);
      DBMS_OUTPUT.put_line ('p_ob_directory_name: ' || p_ob_directory_name);
      DBMS_OUTPUT.put_line ('p_objects_source: ' || p_objects_source);
      DBMS_OUTPUT.put_line ('p_from_date: ' || p_from_date);
      DBMS_OUTPUT.put_line ('p_to_date: ' || p_to_date);
      -- Resetting verification variable
      l_user_id := NULL;
      l_responsibility_id := NULL;
      l_resp_application_id := NULL;
      l_org_id := NULL;
      l_valid := 'Y';

      -- Deriving Ids from initalization variables
      BEGIN
         SELECT user_id
           INTO l_user_id
           FROM fnd_user
          WHERE     user_name = UPPER (p_user_name)
                AND SYSDATE BETWEEN start_date
                                AND NVL (end_date, TRUNC (SYSDATE) + 1);
      EXCEPTION
         WHEN NO_DATA_FOUND
         THEN
            l_err_msg :=
               'UserName ' || p_user_name || ' not defined in Oracle';
            RAISE PROGRAM_ERROR;
         WHEN OTHERS
         THEN
            l_err_msg :=
               'Error deriving user_id for UserName - ' || p_user_name;
            RAISE PROGRAM_ERROR;
      END;

      BEGIN
         SELECT responsibility_id, application_id
           INTO l_responsibility_id, l_resp_application_id
           FROM fnd_responsibility_vl
          WHERE     responsibility_name = p_responsibility_name
                AND SYSDATE BETWEEN start_date
                                AND NVL (end_date, TRUNC (SYSDATE) + 1);
      EXCEPTION
         WHEN NO_DATA_FOUND
         THEN
            l_err_msg :=
                  'Responsibility '
               || p_responsibility_name
               || ' not defined in Oracle';
            RAISE PROGRAM_ERROR;
         WHEN OTHERS
         THEN
            l_err_msg :=
                  'Error deriving Responsibility_id for '
               || p_responsibility_name;
            RAISE PROGRAM_ERROR;
      END;

      -- Verifying Operating unit
      BEGIN
         SELECT organization_id
           INTO l_org_id
           FROM hr_operating_units
          WHERE NAME = p_operating_unit_name;
      EXCEPTION
         WHEN NO_DATA_FOUND
         THEN
            l_err_msg :=
                  'Operating unit '
               || p_operating_unit_name
               || ' is invalid in Oracle';
            RAISE PROGRAM_ERROR;
         WHEN OTHERS
         THEN
            l_err_msg :=
                  'Other Operating unit validation error for '
               || p_operating_unit_name
               || '. Error: '
               || SUBSTR (SQLERRM, 1, 250);
            RAISE PROGRAM_ERROR;
      END;

      -- Environment Initialization
      fnd_global.apps_initialize (l_user_id,
                                  l_responsibility_id,
                                  l_resp_application_id);
      mo_global.set_policy_context ('S', l_org_id);
      fnd_request.set_org_id (l_org_id);
      -- Verifying parameters
      l_argument1 := TO_CHAR (l_org_id);
      l_argument2 := p_ob_directory_name;

      BEGIN
         SELECT ffvv.flex_value valid_sources
           INTO l_argument3
           FROM fnd_flex_value_sets ffvs, fnd_flex_values_vl ffvv
          WHERE     ffvs.flex_value_set_name = 'XXWC_EBS_INTERFACE_SOURCES'
                AND ffvs.flex_value_set_id = ffvv.flex_value_set_id
                AND ffvv.flex_value = p_objects_source
                AND NVL (ffvv.enabled_flag, 'N') = 'Y';
      EXCEPTION
         WHEN NO_DATA_FOUND
         THEN
            l_argument3 := NULL;
            l_valid := 'N';
            l_err_msg :=
               'Source ' || p_objects_source || ' is invalid in Oracle';
            DBMS_OUTPUT.put_line (l_err_msg);
            RAISE PROGRAM_ERROR;
         WHEN OTHERS
         THEN
            l_argument3 := NULL;
            l_valid := 'N';
            l_err_msg :=
                  'Other error for source '
               || p_objects_source
               || '. Error: '
               || SQLERRM;
            DBMS_OUTPUT.put_line (l_err_msg);
            RAISE PROGRAM_ERROR;
      END;

      -- 06/24/2012 CG Modified to use profile values to pull date ranges when needed
      l_profile_name := NULL;
      l_profile_value := 0;

      IF l_argument3 IS NOT NULL
      THEN
         l_profile_name := 'XXWC_' || l_argument3 || '_SCHED';
         DBMS_OUTPUT.put_line (
            'Searching for value for profile ' || l_profile_name);

         BEGIN
            SELECT TO_NUMBER (fnd_profile.VALUE (l_profile_name))
              INTO l_profile_value
              FROM DUAL;
         EXCEPTION
            WHEN NO_DATA_FOUND
            THEN
               l_profile_value := 0;
               l_err_msg :=
                     'Profile '
                  || l_profile_name
                  || ' not found pulling full view';
               DBMS_OUTPUT.put_line (l_err_msg);
            WHEN TOO_MANY_ROWS
            THEN
               l_profile_value := 0;
               l_err_msg :=
                     'Multiple entries for  '
                  || l_profile_name
                  || ' found pulling full view';
               DBMS_OUTPUT.put_line (l_err_msg);
            WHEN OTHERS
            THEN
               l_profile_value := 0;
               l_err_msg :=
                     'Other error pulling profile '
                  || l_profile_name
                  || '. ERORR: '
                  || SQLERRM;
               DBMS_OUTPUT.put_line (l_err_msg);
         END;

         -- Date Format as a varchar should be YYYY/MM/DD HH24:MI:SS
         -- 06/24/2012 CG: Commented out passing of the date range from the UC4 call
         -- Finding date based on profile value
         -- l_argument4 := p_from_date;
         -- l_argument5  := p_to_date;
         IF l_profile_value > 0
         THEN
            l_argument4 :=
                  TO_CHAR (SYSDATE - l_profile_value, 'YYYY/MM/DD')
               || ' 00:00:00';
            l_argument5 := TO_CHAR (SYSDATE, 'YYYY/MM/DD') || ' 23:59:59';
         ELSE
            l_argument4 := NULL;
            l_argument5 := NULL;
         END IF;
      END IF;

      IF NVL (l_valid, 'N') = 'Y'
      THEN
         l_req_id :=
            fnd_request.submit_request (
               application   => 'XXWC',
               program       => 'XXWC_EBS_EDW_INTF_PKG',
               description   => NULL,
               start_time    => SYSDATE,
               sub_request   => FALSE,
               argument1     => l_argument1,
               argument2     => l_argument2,
               argument3     => l_argument3,
               argument4     => l_argument4,
               argument5     => l_argument5);
         COMMIT;
         DBMS_OUTPUT.put_line (
            'Concurrent Program Request Submitted. Request ID: ' || l_req_id);

         IF (l_req_id != 0)
         THEN
            IF fnd_concurrent.wait_for_request (l_req_id,
                                                6,
                                                99999           -- Version 4.4
                                                     ,
                                                v_phase,
                                                v_status,
                                                v_dev_phase,
                                                v_dev_status,
                                                v_message)
            THEN
               v_error_message :=
                     'ReqID:'
                  || l_req_id
                  || '-DPhase:'
                  || v_dev_phase
                  || '-DStatus:'
                  || v_dev_status
                  || CHR (10)
                  || 'MSG:'
                  || v_message;

               -- Error Returned
               IF v_dev_phase != 'COMPLETE' OR v_dev_status != 'NORMAL'
               THEN
                  l_err_msg :=
                        'EBS Conc Prog Completed with problems '
                     || v_error_message
                     || '.';
                  DBMS_OUTPUT.put_line (l_err_msg);
                  RAISE PROGRAM_ERROR;
               ELSE
                  retcode := 0;
               END IF;
            ELSE
               l_err_msg := 'EBS Conc Program Wait timed out';
               DBMS_OUTPUT.put_line (l_err_msg);
               RAISE PROGRAM_ERROR;
            END IF;
         ELSE
            l_err_msg := 'EBS Conc Program not initated';
            DBMS_OUTPUT.put_line (l_err_msg);
            RAISE PROGRAM_ERROR;
         END IF;
      ELSE
         l_err_msg := 'Invalid parameters for concurrent program';
         DBMS_OUTPUT.put_line (l_err_msg);
         RAISE PROGRAM_ERROR;
      END IF;

      DBMS_OUTPUT.put_line ('Exiting submit_job...');
   EXCEPTION
      WHEN PROGRAM_ERROR
      THEN
         ROLLBACK;
         l_err_code := 2;
         l_err_msg :=
            SUBSTR ( (l_err_msg || ' ERROR ' || SUBSTR (SQLERRM, 1, 2000)),
                    1,
                    2000);
         DBMS_OUTPUT.put_line (l_err_msg);
         xxcus_error_pkg.xxcus_error_main_api (
            p_called_from         => l_err_callfrom,
            p_calling             => l_err_callpoint,
            p_request_id          => l_req_id,
            p_ora_error_msg       => SUBSTR (SQLERRM, 1, 2000),
            p_error_desc          => SUBSTR (l_err_msg, 1, 2000),
            p_distribution_list   => l_distro_list,
            p_module              => 'FND');
         retcode := l_err_code;
         errbuf := l_err_msg;
      WHEN OTHERS
      THEN
         l_err_code := 2;
         l_err_msg :=
            SUBSTR ( (l_err_msg || ' ERROR ' || SUBSTR (SQLERRM, 1, 2000)),
                    1,
                    2000);
         DBMS_OUTPUT.put_line (l_err_msg);
         xxcus_error_pkg.xxcus_error_main_api (
            p_called_from         => l_err_callfrom,
            p_calling             => l_err_callpoint,
            p_request_id          => l_req_id,
            p_ora_error_msg       => SUBSTR (SQLERRM, 1, 2000),
            p_error_desc          => SUBSTR (l_err_msg, 1, 2000),
            p_distribution_list   => l_distro_list,
            p_module              => 'FND');
         retcode := l_err_code;
         errbuf := l_err_msg;
   END submit_job;
END xxwc_ebs_edw_intf_pkg;
/