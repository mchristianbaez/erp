------------------------------------------------------------------------------------
/******************************************************************************
   NAME       :  XXEIS.EIS_XXWC_INV_UPC_LISTING_EDI_V
   REVISIONS:
	Ver         Date            Author             Description
   ---------  ----------    ---------------  ------------------------------------
    1.0       26-Apr-2017       Siva	            Initial Version -- TMS#20160516-00225
******************************************************************************/
CREATE OR REPLACE VIEW XXEIS.EIS_XXWC_INV_UPC_LISTING_EDI_V (UPC_NUMBER, WHITE_CAP_ITEM_NUMBER, ITEM_DESCRIPTION, VENDOR_ITEM_NUMBER, VENDOR_LOCATION_NAME, CREATION_DATE, UPC_VALUE)
AS
  SELECT pol.upc_number,
    pol.white_cap_item_number,
    pol.item_description,
    pol.vendor_item_number,
    poh.vendor_location_name,
    TRUNC(poh.creation_date) creation_date,
    DECODE(pol.upc_number,NULL,'IS NULL','IS NOT NULL') upc_value
  FROM xxwc.xxwc_po_acceptance_headers poh,
    xxwc.xxwc_po_acceptance_lines pol
  WHERE POH.ACC_HEADER_ID = POL.ACC_HEADER_ID
/
