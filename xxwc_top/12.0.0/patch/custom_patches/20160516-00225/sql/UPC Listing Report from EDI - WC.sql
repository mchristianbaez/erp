--Report Name            : UPC Listing Report from EDI - WC
--Import Instructions    : Run this file in XXEIS user from SQL*Plus or any tool like Toad, SQL Navigator. 
prompt Creating Object Data EIS_XXWC_INV_UPC_LISTING_EDI_V
set scan off define off
DECLARE
mod_exist VARCHAR2(1);
BEGIN 
mod_exist := XXEIS.EIS_RSC_INSERT.check_if_module_exist(401);
IF mod_exist = 'Y' THEN 
--Inserting Object EIS_XXWC_INV_UPC_LISTING_EDI_V
xxeis.eis_rsc_ins.v( 'EIS_XXWC_INV_UPC_LISTING_EDI_V',401,'','','','','SA059956','XXEIS','Eis Xxwc Inv Upc Listing Edi V','EXIULEV','','','VIEW','US','','');
--Delete Object Columns for EIS_XXWC_INV_UPC_LISTING_EDI_V
xxeis.eis_rsc_utility.delete_view_rows('EIS_XXWC_INV_UPC_LISTING_EDI_V',401,FALSE);
--Inserting Object Columns for EIS_XXWC_INV_UPC_LISTING_EDI_V
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_INV_UPC_LISTING_EDI_V','WHITE_CAP_ITEM_NUMBER',401,'White Cap Item Number','WHITE_CAP_ITEM_NUMBER','','','','SA059956','VARCHAR2','','','White Cap Item Number','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_INV_UPC_LISTING_EDI_V','UPC_NUMBER',401,'Upc Number','UPC_NUMBER','','','','SA059956','VARCHAR2','','','Upc Number','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_INV_UPC_LISTING_EDI_V','VENDOR_LOCATION_NAME',401,'Vendor Location Name','VENDOR_LOCATION_NAME','','','','SA059956','VARCHAR2','','','Vendor Location Name','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_INV_UPC_LISTING_EDI_V','VENDOR_ITEM_NUMBER',401,'Vendor Item Number','VENDOR_ITEM_NUMBER','','','','SA059956','VARCHAR2','','','Vendor Item Number','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_INV_UPC_LISTING_EDI_V','ITEM_DESCRIPTION',401,'Item Description','ITEM_DESCRIPTION','','','','SA059956','VARCHAR2','','','Item Description','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_INV_UPC_LISTING_EDI_V','CREATION_DATE',401,'Creation Date','CREATION_DATE','','','','SA059956','DATE','','','Creation Date','','','','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_INV_UPC_LISTING_EDI_V','UPC_VALUE',401,'Upc Value','UPC_VALUE','','','','SA059956','VARCHAR2','','','Upc Value','','','','');
--Inserting Object Components for EIS_XXWC_INV_UPC_LISTING_EDI_V
--Inserting Object Component Joins for EIS_XXWC_INV_UPC_LISTING_EDI_V
ELSE
RAISE_APPLICATION_ERROR (-20001,'Module does not exist. Import/Create the module with id 401');
END IF;
END;
/
prompt Creating Report LOV Data for UPC Listing Report from EDI - WC
set scan off define off
DECLARE
mod_exist VARCHAR2(1);
BEGIN 
mod_exist := XXEIS.EIS_RSC_INSERT.check_if_module_exist(401);
IF mod_exist = 'Y' THEN 
--Inserting Report LOVs - UPC Listing Report from EDI - WC
xxeis.eis_rsc_ins.lov( 401,'SELECT DISTINCT pol.vendor_item_number
FROM xxwc.xxwc_po_acceptance_lines pol
WHERE pol.vendor_item_number IS NOT NULL
ORDER BY lpad(pol.vendor_item_number,10)','','XXWC Vendor Item Number LOV','XXWC Vendor Item Number LOV','SA059956',NULL,'N','','','N','N','','','US');
xxeis.eis_rsc_ins.lov( 401,'SELECT DISTINCT poh.vendor_location_name vendor_name
FROM xxwc.xxwc_po_acceptance_headers poh
WHERE poh.vendor_location_name IS NOT NULL
ORDER BY 1','','XXWC Vendor Location Name LOV','XXWC Vendor Location Name LOV','SA059956',NULL,'N','','','N','N','','','US');
xxeis.eis_rsc_ins.lov( 401,'SELECT DISTINCT pol.white_cap_item_number
FROM xxwc.xxwc_po_acceptance_lines pol
WHERE pol.white_cap_item_number IS NOT NULL
ORDER BY lpad(pol.white_cap_item_number,10)','','XXWC White Cap Item Number LOV','XXWC White Cap Item Number LOV','SA059956',NULL,'N','','','N','N','','','US');
xxeis.eis_rsc_ins.lov( 401,'select ''IS NULL'' upc_value
from dual
union all
select ''IS NOT NULL'' upc_value
from dual','','XXWC UPC Value LOV','','SA059956',NULL,'Y','','','N','N','','','US');
null;
ELSE
RAISE_APPLICATION_ERROR (-20001,'Module does not exist. Import/Create the module with id 401');
END IF;
END;
/
prompt Creating Report Data for UPC Listing Report from EDI - WC
set scan off define off
DECLARE
mod_exist varchar2(1);
BEGIN 
mod_exist := XXEIS.EIS_RSC_INSERT.check_if_module_exist(401);
IF mod_exist = 'Y' THEN 
--Deleting Report data - UPC Listing Report from EDI - WC
xxeis.eis_rsc_utility.delete_report_rows( 'UPC Listing Report from EDI - WC' );
--Inserting Report - UPC Listing Report from EDI - WC
xxeis.eis_rsc_ins.r( 401,'UPC Listing Report from EDI - WC','','UPC codes received from Vendors via EDI confirmations','','','','SA059956','EIS_XXWC_INV_UPC_LISTING_EDI_V','Y','','','SA059956','','Y','White Cap Reports','','CSV,EXCEL,','N','','','','','','N','','US','','','','');
--Inserting Report Columns - UPC Listing Report from EDI - WC
xxeis.eis_rsc_ins.rc( 'UPC Listing Report from EDI - WC',401,'ITEM_DESCRIPTION','Item Description','Item Description','','','default','','3','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_INV_UPC_LISTING_EDI_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'UPC Listing Report from EDI - WC',401,'UPC_NUMBER','UPC Number','Upc Number','','','default','','1','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_INV_UPC_LISTING_EDI_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'UPC Listing Report from EDI - WC',401,'VENDOR_ITEM_NUMBER','Vendor Item Number','Vendor Item Number','','','default','','4','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_INV_UPC_LISTING_EDI_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'UPC Listing Report from EDI - WC',401,'VENDOR_LOCATION_NAME','Vendor Name','Vendor Location Name','','','default','','5','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_INV_UPC_LISTING_EDI_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'UPC Listing Report from EDI - WC',401,'WHITE_CAP_ITEM_NUMBER','White Cap Item Number','White Cap Item Number','','','default','','2','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_INV_UPC_LISTING_EDI_V','','','','US','');
--Inserting Report Parameters - UPC Listing Report from EDI - WC
xxeis.eis_rsc_ins.rp( 'UPC Listing Report from EDI - WC',401,'Vendor Item Number','Vendor Item Number','VENDOR_ITEM_NUMBER','IN','XXWC Vendor Item Number LOV','','VARCHAR2','N','Y','3','Y','Y','CONSTANT','SA059956','Y','N','','','','EIS_XXWC_INV_UPC_LISTING_EDI_V','','','US','');
xxeis.eis_rsc_ins.rp( 'UPC Listing Report from EDI - WC',401,'Vendor Name','Vendor Name','VENDOR_LOCATION_NAME','IN','XXWC Vendor Location Name LOV','','VARCHAR2','N','Y','1','Y','Y','CONSTANT','SA059956','Y','N','','','','EIS_XXWC_INV_UPC_LISTING_EDI_V','','','US','');
xxeis.eis_rsc_ins.rp( 'UPC Listing Report from EDI - WC',401,'White Cap Item Number','White Cap Item Number','WHITE_CAP_ITEM_NUMBER','IN','XXWC White Cap Item Number LOV','','VARCHAR2','N','Y','2','Y','Y','CONSTANT','SA059956','Y','N','','','','EIS_XXWC_INV_UPC_LISTING_EDI_V','','','US','');
xxeis.eis_rsc_ins.rp( 'UPC Listing Report from EDI - WC',401,'Creation Date From','Creation Date From','CREATION_DATE','>=','','','DATE','N','Y','4','N','Y','CONSTANT','SA059956','Y','N','','Start Date','','EIS_XXWC_INV_UPC_LISTING_EDI_V','','','US','');
xxeis.eis_rsc_ins.rp( 'UPC Listing Report from EDI - WC',401,'Creation Date To','Creation Date To','CREATION_DATE','<=','','','DATE','N','Y','5','N','Y','CONSTANT','SA059956','Y','N','','End Date','','EIS_XXWC_INV_UPC_LISTING_EDI_V','','','US','');
xxeis.eis_rsc_ins.rp( 'UPC Listing Report from EDI - WC',401,'UPC Value','UPC Value','UPC_VALUE','IN','XXWC UPC Value LOV','''IS NOT NULL''','VARCHAR2','N','Y','6','N','Y','CONSTANT','SA059956','Y','N','','','','EIS_XXWC_INV_UPC_LISTING_EDI_V','','','US','');
--Inserting Dependent Parameters - UPC Listing Report from EDI - WC
--Inserting Report Conditions - UPC Listing Report from EDI - WC
xxeis.eis_rsc_ins.rcnh( 'UPC Listing Report from EDI - WC',401,'EXIULEV.VENDOR_ITEM_NUMBER IN Vendor Item Number','SIMPLE','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','VENDOR_ITEM_NUMBER','','Vendor Item Number','','','','','EIS_XXWC_INV_UPC_LISTING_EDI_V','','','','','','IN','Y','Y','','','','','1',401,'UPC Listing Report from EDI - WC','EXIULEV.VENDOR_ITEM_NUMBER IN Vendor Item Number');
xxeis.eis_rsc_ins.rcnh( 'UPC Listing Report from EDI - WC',401,'EXIULEV.VENDOR_LOCATION_NAME IN Vendor Location Name','SIMPLE','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','VENDOR_LOCATION_NAME','','Vendor Name','','','','','EIS_XXWC_INV_UPC_LISTING_EDI_V','','','','','','IN','Y','Y','','','','','1',401,'UPC Listing Report from EDI - WC','EXIULEV.VENDOR_LOCATION_NAME IN Vendor Location Name');
xxeis.eis_rsc_ins.rcnh( 'UPC Listing Report from EDI - WC',401,'EXIULEV.WHITE_CAP_ITEM_NUMBER IN White Cap Item Number','SIMPLE','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','WHITE_CAP_ITEM_NUMBER','','White Cap Item Number','','','','','EIS_XXWC_INV_UPC_LISTING_EDI_V','','','','','','IN','Y','Y','','','','','1',401,'UPC Listing Report from EDI - WC','EXIULEV.WHITE_CAP_ITEM_NUMBER IN White Cap Item Number');
xxeis.eis_rsc_ins.rcnh( 'UPC Listing Report from EDI - WC',401,'EXIULEV.CREATION_DATE >= Creation Date From','SIMPLE','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','CREATION_DATE','','Creation Date From','','','','','EIS_XXWC_INV_UPC_LISTING_EDI_V','','','','','','GREATER_THAN_EQUALS','Y','Y','','','','','1',401,'UPC Listing Report from EDI - WC','EXIULEV.CREATION_DATE >= Creation Date From');
xxeis.eis_rsc_ins.rcnh( 'UPC Listing Report from EDI - WC',401,'EXIULEV.CREATION_DATE <= Creation Date To','SIMPLE','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','CREATION_DATE','','Creation Date To','','','','','EIS_XXWC_INV_UPC_LISTING_EDI_V','','','','','','LESS_THAN_EQUALS','Y','Y','','','','','1',401,'UPC Listing Report from EDI - WC','EXIULEV.CREATION_DATE <= Creation Date To');
xxeis.eis_rsc_ins.rcnh( 'UPC Listing Report from EDI - WC',401,'EXIULEV.UPC_VALUE IN UPC Value','SIMPLE','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','UPC_VALUE','','UPC Value','','','','','EIS_XXWC_INV_UPC_LISTING_EDI_V','','','','','','IN','Y','Y','','','','','1',401,'UPC Listing Report from EDI - WC','EXIULEV.UPC_VALUE IN UPC Value');
--Inserting Report Sorts - UPC Listing Report from EDI - WC
xxeis.eis_rsc_ins.rs( 'UPC Listing Report from EDI - WC',401,'UPC_NUMBER','ASC','SA059956','1','');
--Inserting Report Triggers - UPC Listing Report from EDI - WC
--inserting report templates - UPC Listing Report from EDI - WC
--Inserting Report Portals - UPC Listing Report from EDI - WC
--inserting report dashboards - UPC Listing Report from EDI - WC
--Exporting report views table information -  
xxeis.eis_rsc_ins.rviews( 'UPC Listing Report from EDI - WC','401','EIS_XXWC_INV_UPC_LISTING_EDI_V','EIS_XXWC_INV_UPC_LISTING_EDI_V','N','');
--inserting report security - UPC Listing Report from EDI - WC
xxeis.eis_rsc_ins.rsec( 'UPC Listing Report from EDI - WC','401','','XXWC_DATA_MNGT_SC',401,'SA059956','','','');
--Inserting Report Pivots - UPC Listing Report from EDI - WC
--Inserting Report   Version details- UPC Listing Report from EDI - WC
xxeis.eis_rsc_ins.rv( 'UPC Listing Report from EDI - WC','','UPC Listing Report from EDI - WC','SA059956');
ELSE
RAISE_APPLICATION_ERROR (-20001,'Module does not exist. Import/Create the module with id 401');
END IF;
END;
/
