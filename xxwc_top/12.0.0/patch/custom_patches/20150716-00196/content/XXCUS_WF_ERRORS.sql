/* ************************************************************************
  $Header $XXCUS_TOP/sql/XXCUS_WF_ERRORS.sql $
  Module Name: Workflow -Script to check GSC I-Expense and other work flow errors

  REVISIONS:
  Ver        Date         Author                Description
  ---------  -----------  ------------------    ----------------
  1.0        06/30/2015   Ivelice De Jesus      TMS 20150716-00196 Initial Version
  1.1        07/23/2015   Ivelice De Jesus      Add set line size to 255
************************************************************************* */
set newpage 2
clear buffer
set serveroutput on
SET HEADING ON
SET LINESIZE 255 
REM SET LINE SIZE TO 255 in VER 1.1
set termout on 
col sysdate                    heading SYSTEM_DATE          format a15
select sysdate from dual
/ 


TTITLE LEFT 'Workflow Error Report'

col STATUS			heading STATUS		        format A10
col WORKFLOW_TYPE		heading WORKFLOW_TYPE		format A20
col ITEM_KEY			heading ITEM_KEY		format A10
col PROCESS_NAME		heading PROCESS_NAME		format A35
col OWNED_BY			heading OWNED_BY		format A25 
col BEGIN_DATE			heading STARTED			
col END_DATE			heading COMPLETED		
col ITEM_TYPE			heading ITEM_TYPE		format A10
col ROLE_EMAIL			heading ROLE_EMAIL		format A35 

SELECT *
  FROM (SELECT wf_fwkmon.getitemstatus(WorkflowItemEO.ITEM_TYPE,
                                       WorkflowItemEO.ITEM_KEY,
                                       WorkflowItemEO.END_DATE,
                                       WorkflowItemEO.ROOT_ACTIVITY,
                                       WorkflowItemEO.ROOT_ACTIVITY_VERSION) AS STATUS,
               WorkflowItemTypeEO.DISPLAY_NAME WORKFLOW_TYPE,
               WorkflowItemEO.ITEM_KEY,
               ActivityEO.DISPLAY_NAME PROCESS_NAME,
               wf_directory.getroledisplayname2(WorkflowItemEO.OWNER_ROLE) AS OWNED_BY,
               WorkflowItemEO.BEGIN_DATE STARTED,
               WorkflowItemEO.END_DATE COMPLETED,
               WorkflowItemEO.ITEM_TYPE,
               wf_fwkmon.getroleemailaddress(WorkflowItemEO.OWNER_ROLE) AS ROLE_EMAIL                  
          FROM WF_ITEMS         WorkflowItemEO,
               WF_ITEM_TYPES_VL WorkflowItemTypeEO,
               WF_ACTIVITIES_VL ActivityEO
         WHERE WorkflowItemEO.ITEM_TYPE = WorkflowItemTypeEO.NAME
           AND ActivityEO.ITEM_TYPE = WorkflowItemEO.ITEM_TYPE
           AND ActivityEO.NAME = WorkflowItemEO.ROOT_ACTIVITY
           AND ActivityEO.VERSION = WorkflowItemEO.ROOT_ACTIVITY_VERSION) QRSLT
 WHERE (COMPLETED is null and
       (ITEM_KEY, ITEM_TYPE) in
       (select wias.item_key, wias.item_type
           from wf_item_activity_statuses wias
          where wias.item_type = 'APEXP'
            and wias.activity_status = 'ERROR'
            and wias.end_date is null)) 
 ORDER BY STARTED DESC
/


set serveroutput off
set feedback off
TTITLE off 
set heading off
clear buffer 