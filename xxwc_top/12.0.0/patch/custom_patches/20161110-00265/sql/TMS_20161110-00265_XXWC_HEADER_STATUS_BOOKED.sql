/*************************************************************************
  $Header 20161110-00265_HEADER_STATUS_BOOKED.sql $
  Module Name: 20161110-00265  Data Fix script for 20161110-00265

  PURPOSE: Data fix script for 20161110-00265--No permanent fix in process.

  REVISIONS:
  Ver        Date         Author                Description
  ---------  -----------  ------------------    --------------------------
  1.0        17-JAN-2017  Ashwin Sridhar        TMS#20161110-00265 

**************************************************************************/ 
SET SERVEROUTPUT ON SIZE 1000000;
DECLARE
BEGIN
   
  DBMS_OUTPUT.PUT_LINE('TMS: 20161110-00265 , Before Update');

  UPDATE apps.oe_order_headers_all
  SET    flow_status_code='CLOSED'
  ,      open_flag       ='N'
  WHERE  header_id IN (21248193, 10481843);

  DBMS_OUTPUT.PUT_LINE(
         'TMS: 20161110-00265  Sales order header updated (Expected:2): '
      || SQL%ROWCOUNT);

  COMMIT;

  DBMS_OUTPUT.PUT_LINE('TMS: 20161110-00265 , End Update');
   
EXCEPTION
WHEN others THEN
     
  ROLLBACK;
     
  DBMS_OUTPUT.put_line ('TMS: 20161110-00265 , Errors : ' || SQLERRM);

END;
/