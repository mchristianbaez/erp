/*
 Ticket: TMS: 20170103-00293
 Date: 01/03/2017
 MODULE: CASH MANAGEMENT
 AUTHOR: BALAGURU SESHADRI 
 Notes:  HDS TREASURY: PROCESS CURRENT DAY BANK BAI FILES
*/
SET SERVEROUTPUT ON SIZE 1000000
DECLARE
 --
 n_loc number :=0;
 n_count number :=0;
 l_err_msg varchar2(2000) :=Null;
 b_go boolean;
 --
BEGIN --Main Processing...
   --
    --BEGIN  INSERT FOR ACCOUNT 4793211236   
             Insert into XXCUS.XXCUS_BANKS_BAI_LOOKUP
               (BANK_NAME, ACCT, CODE, REQUIRED_FLAG)
             Values
               ('WELLSFARGO', '4793211236', '060', 'R');
            Insert into XXCUS.XXCUS_BANKS_BAI_LOOKUP
               (BANK_NAME, ACCT, CODE, REQUIRED_FLAG)
             Values
               ('WELLSFARGO', '4793211236', '030', 'O');
            Insert into XXCUS.XXCUS_BANKS_BAI_LOOKUP
               (BANK_NAME, ACCT, CODE, REQUIRED_FLAG)
             Values
               ('WELLSFARGO', '4793211236', '100', 'O');
            Insert into XXCUS.XXCUS_BANKS_BAI_LOOKUP
               (BANK_NAME, ACCT, CODE, REQUIRED_FLAG)
             Values
               ('WELLSFARGO', '4793211236', '400', 'O');
    --END  INSERT FOR ACCOUNT 4793211236              
    --BEGIN  INSERT FOR ACCOUNT 4793211244
             Insert into XXCUS.XXCUS_BANKS_BAI_LOOKUP
               (BANK_NAME, ACCT, CODE, REQUIRED_FLAG)
             Values
               ('WELLSFARGO', '4793211244', '060', 'R');
            Insert into XXCUS.XXCUS_BANKS_BAI_LOOKUP
               (BANK_NAME, ACCT, CODE, REQUIRED_FLAG)
             Values
               ('WELLSFARGO', '4793211244', '030', 'O');
            Insert into XXCUS.XXCUS_BANKS_BAI_LOOKUP
               (BANK_NAME, ACCT, CODE, REQUIRED_FLAG)
             Values
               ('WELLSFARGO', '4793211244', '100', 'O');
            Insert into XXCUS.XXCUS_BANKS_BAI_LOOKUP
               (BANK_NAME, ACCT, CODE, REQUIRED_FLAG)
             Values
               ('WELLSFARGO', '4793211244', '400', 'O');    
    --END  INSERT FOR ACCOUNT 4793211244
    --BEGIN  INSERT FOR ACCOUNT 9679451691
             Insert into XXCUS.XXCUS_BANKS_BAI_LOOKUP
               (BANK_NAME, ACCT, CODE, REQUIRED_FLAG)
             Values
               ('WELLSFARGO', '9679451691', '060', 'R');
            Insert into XXCUS.XXCUS_BANKS_BAI_LOOKUP
               (BANK_NAME, ACCT, CODE, REQUIRED_FLAG)
             Values
               ('WELLSFARGO', '9679451691', '030', 'O');
            Insert into XXCUS.XXCUS_BANKS_BAI_LOOKUP
               (BANK_NAME, ACCT, CODE, REQUIRED_FLAG)
             Values
               ('WELLSFARGO', '9679451691', '100', 'O');
            Insert into XXCUS.XXCUS_BANKS_BAI_LOOKUP
               (BANK_NAME, ACCT, CODE, REQUIRED_FLAG)
             Values
               ('WELLSFARGO', '9679451691', '400', 'O');    
    --END  INSERT FOR ACCOUNT 9679451691
    -- 
	DBMS_OUTPUT.PUT_LINE('TOTAL NEW RECORDS INSERTED INTO XXCUS.XXCUS_BANKS_BAI_LOOKUP : '||SQL%ROWCOUNT);
--
commit;
--
INSERT INTO XXCUS.XXCUS_BANKS_BAI_CODES_MAPPING
(
SELECT
         D.BANK_ACCOUNT_ID
        ,D.BANK_ACCOUNT_NAME        
        ,D.BANK_ACCOUNT_NUM
        ,A.CODE BAI_CODE
        ,A.REQUIRED_FLAG --R -Required, N - No, O -Optional
        ,15837 CREATED_BY        
        ,SYSDATE CREATION_DATE
        ,15837 LAST_UPDATED_BY
        ,SYSDATE LAST_UPDATE_DATE        
FROM XXCUS.XXCUS_BANKS_BAI_LOOKUP A
         ,APPS.CE_BANK_ACCOUNTS  D
         ,APPS.CE_BANK_BRANCHES_V E
WHERE 1 =1
     AND A.BANK_NAME ='WELLSFARGO'
     AND A.ACCT IN ('4793211236', '4793211244', '9679451691')
     AND D.ATTRIBUTE_CATEGORY='Cash Positioning'
     AND D.ATTRIBUTE2 ='yes'     
     AND E.BANK_PARTY_ID =D.BANK_ID
     AND E.BRANCH_PARTY_ID =D.BANK_BRANCH_ID
     AND A.ACCT =D.BANK_ACCOUNT_NUM
);
--
DBMS_OUTPUT.PUT_LINE('TOTAL NEW RECORDS INSERTED INTO XXCUS.XXCUS_BANKS_BAI_CODES_MAPPING : '||SQL%ROWCOUNT);
--
COMMIT;
 --
EXCEPTION
   WHEN OTHERS
   THEN
      ROLLBACK;
      DBMS_OUTPUT.put_line ('Outer Block..., Errors =' || SQLERRM);
END;
/