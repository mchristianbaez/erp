/*************************************************************************
  $Header TMS_20160919-00034_CANCEL_ORDER_LINE.sql $
  Module Name: TMS_20160919-00034  Data Fix script for 18207419

  PURPOSE: Data fix script for 18207419--No permanent fix in process (But patch available per oracle)

  REVISIONS:
  Ver        Date         Author                Description
  ---------  -----------  ------------------    --------------------------
  1.0        10-OCT-2016  Niraj K Ranjan        TMS#20160919-00034 

**************************************************************************/ 
SET SERVEROUTPUT ON SIZE 1000000
DECLARE
BEGIN
   DBMS_OUTPUT.put_line ('TMS: 20160919-00034    , Before Update');

   update apps.oe_order_lines_all
   set FLOW_STATUS_CODE='CANCELLED',
   CANCELLED_FLAG='Y'
   where line_id = 55531342
   and header_id= 33715874;
   
   DBMS_OUTPUT.put_line (
         'TMS: 20160919-00034  Sales order lines updated (Expected:1): '
      || SQL%ROWCOUNT);

   COMMIT;

   DBMS_OUTPUT.put_line ('TMS: 20160919-00034    , End Update');
EXCEPTION
   WHEN OTHERS
   THEN
      ROLLBACK;
      DBMS_OUTPUT.put_line ('TMS: 20160919-00034 , Errors : ' || SQLERRM);
END;
/

