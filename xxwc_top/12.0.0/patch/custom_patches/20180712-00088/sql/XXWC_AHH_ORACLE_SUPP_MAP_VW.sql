/*
TMS#20180712-00088 - AH Harries AP Invoices Interface
Created by : P.Vamshidhar
*/
CREATE OR REPLACE FORCE VIEW APPS.XXWC_AHH_ORACLE_SUPP_MAP_VW
(AHH_VENDOR_NUM, ORACLE_VENDOR)
BEQUEATH DEFINER
AS
SELECT TO_CHAR (ahh_vendor_num) ahh_vendor_num,
       TO_CHAR (oracle_vendor) oracle_vendor
  FROM XXWC.XXWC_AP_AHH_VENDOR_SITES_REF
UNION
SELECT TO_CHAR (AHH_SUPPLIER_NUMBER) ahh_vendor_num,
       TO_CHAR (ORACLE_VENDOR_ID) oracle_vendor
  FROM XXWC.XXWC_AP_SUPPLIER_REF_TBL;
/
  