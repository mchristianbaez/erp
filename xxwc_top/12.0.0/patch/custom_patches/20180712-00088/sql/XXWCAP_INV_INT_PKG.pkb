CREATE OR REPLACE PACKAGE BODY APPS.XXWCAP_INV_INT_PKG
AS
   /********************************************************************************************************
   *    Package Name: XXWCAP_INV_INT_PKG
   *
   *    Functional purpose: Interface PRISM AP Invoices into Oracle
   *
   *    history:*
   *    version    date          author             description
   *********************************************************************************************************
   *    1.0        12/13/2011    G.Damuluri         initial development.
   *    1.1        07/10/2013    G.Damuluri         ESMS# 200785
   *                                                Distribution Account Combinations are
   *                                                populated in the interface tables rather than CCId.
   *    1.2        07/23/2013    G.Damuluri         TMS# 20130722-00696
   *                                                Resolve the issue with Location Mapping
   *    1.3        06/18/2018    P.Vamshidhar       Changed code for AP Invoices Interface.
   ********************************************************************************************************/

   g_currency_code            VARCHAR2 (50) := 'USD';
   g_invoice_source           VARCHAR2 (50) := 'AHH';        -- changed in 1.3
   g_user_id                  NUMBER := fnd_global.user_id;
   g_line_type_lookup_code    VARCHAR2 (100) := 'ITEM';
   g_gl_date                  DATE := SYSDATE;
   g_org_id                   NUMBER := fnd_profile.VALUE ('ORG_ID');
   g_exclusive_payment_flag   VARCHAR2 (1) := 'N';
   g_login_id                 NUMBER := fnd_profile.VALUE ('LOGIN_ID');
   g_error_message            VARCHAR2 (2000);
   g_error_code               NUMBER;
   g_batch_id                 NUMBER;
   g_group_name               VARCHAR2 (80)
      := 'AHH INVOICES - ' || TO_CHAR (SYSDATE, 'DD-MON-YYYY:HHMMRR'); -- changed in 1.3

   /********************************************************************************
   ProcedureName : LOAD_INTERFACE
   Purpose       : API to validate and process PRISM AP Invoice data from Oracle
                   Staging tables, load them into Oracle Interface tables.

   HISTORY
   ===============================================================================
   VERSION DATE          AUTHOR(S)       DESCRIPTION
   ------- -----------   --------------- -----------------------------------------
   1.0     12/13/2011    Gopi Damuluri    Initial version.
   1.1     07/10/2012    Gopi Damuluri    Check if Oracle Vendor# matches with
                                          PRISM Vendor# if Oracle DFF does fails to
                                          match
   1.3        06/18/2018    P.Vamshidhar  Changed code for AP Invoices Interface.
   ********************************************************************************/
   PROCEDURE load_interface
   AS
      l_vendor_id               NUMBER;
      l_vendor_site_id          NUMBER (15);
      l_terms_id                NUMBER (15);
      l_credit_terms_id         NUMBER (15);
      l_ccid                    NUMBER;
      l_dist_code_combination   VARCHAR2 (200);
      l_dist_location           VARCHAR2 (10);
      l_invoice_type            VARCHAR2 (20);
      l_payment_method_code     VARCHAR2 (30);
      l_rec_valid_flag          VARCHAR2 (1);
      l_error_code              NUMBER;
      l_error_message           VARCHAR2 (2000);
      l_invoice_num             VARCHAR2 (50);
      l_line_num                NUMBER;
      l_vendor_num              VARCHAR2 (40);

      TYPE invoivelinesrecord IS RECORD
      (
         status          VARCHAR2 (20),
         error_message   VARCHAR2 (30),
         r_id            VARCHAR2 (100)
      );

      TYPE invline_tbl IS TABLE OF invoivelinesrecord
         INDEX BY BINARY_INTEGER;

      invlinerecords            invline_tbl;
      dmy_invlinerecords        invline_tbl;

      CURSOR cur_hdr
      IS
           SELECT xaii.invoice_num,
                  xaii.vendor_num,
                  -- SUM (TO_NUMBER (xaii.invoice_amount)) invoice_amount, -- ?????
                  TO_NUMBER (xaii.invoice_amount) invoice_amount, -- ?????
                  SUM (TO_NUMBER (xaii.line_amount)) sum_line_amount, -- ?????
                  xaii.invoice_date,
                  xaii.attribute_category,
                  xaii.attribute1,
                  xaii.attribute2,
                  xaii.attribute6
             FROM xxwc.xxwcap_inv_int_stg_tbl xaii
            WHERE     NVL (status, 'XXXXX') <> 'PROCESSED'
                  AND invoice_amount <> 0
                 -- AND xaii.invoice_num = '1001132711'
         GROUP BY xaii.invoice_num,
                  xaii.vendor_num,
                  xaii.invoice_date,
                  xaii.attribute_category,
                  xaii.attribute1,
                  xaii.attribute2,
                  TO_NUMBER (xaii.invoice_amount), -- ?????
                  xaii.attribute6;

      --FOR UPDATE OF status, error_message

      CURSOR cur_detl (
         p_invoice_num    VARCHAR2,
         p_vendor_num     VARCHAR2)
      IS
             SELECT xaii.invoice_num,
                    TO_NUMBER (xaii.invoice_amount) invoice_amount,
                    TO_NUMBER (xaii.line_amount) line_amount, -- ?????
                    xaii.ROWID,
                    SUBSTR (xaii.gl_account,
                            INSTR (xaii.gl_account, '-', -1) + 1) -- Changed in 1.3
                       gl_account,
                    SUBSTR (xaii.gl_account,
                            1,
                            INSTR (xaii.gl_account, '-', 1) - 1) -- Changed in 1.3
                       branch,
                    xaii.attribute6
               FROM xxwc.xxwcap_inv_int_stg_tbl xaii
              WHERE     NVL (status, 'XXXXX') <> 'PROCESSED'
                    AND invoice_amount <> 0
                    AND invoice_num = p_invoice_num
                    AND vendor_num = p_vendor_num
         FOR UPDATE OF status, error_message;
   BEGIN
      fnd_file.put_line (
         fnd_file.LOG,
         '----------------------------------------------------------------------------------');
      fnd_file.put_line (fnd_file.LOG, 'Start of Procedure : LOAD_INTERFACE');
      fnd_file.put_line (
         fnd_file.LOG,
         '----------------------------------------------------------------------------------');

      fnd_file.put_line (fnd_file.LOG, 'Batch Name : ' || g_group_name);

      ----------------------------------------------------------------------------------
      -- Derive Payment Terms for Credit Memos
      ----------------------------------------------------------------------------------
      BEGIN
         SELECT term_id
           INTO l_credit_terms_id
           FROM ap_terms
          WHERE name = 'DUE UPON RECEIPT';
      EXCEPTION
         WHEN OTHERS
         THEN
            l_rec_valid_flag := 'N';
            l_error_message :=
                  l_error_message
               || ',Payment Term : "DUE UPON RECEIPT" is not defined in Oracle';
      END;

      FOR rec_hdr IN cur_hdr
      LOOP
         l_rec_valid_flag := 'Y';
         l_error_message := NULL;
         l_invoice_type := NULL;
         l_vendor_id := NULL;
         l_vendor_site_id := NULL;
         l_ccid := NULL;
         l_dist_code_combination := NULL;
         l_dist_location := NULL;
         l_payment_method_code := NULL;

         ----------------------------------------------------------------
         -- Deriving Vendor_id from reference table.
         ----------------------------------------------------------------

         -- Checking vendor exists in crossover table
         BEGIN
            SELECT aps.vendor_id
              INTO l_vendor_id
              FROM XXWC_AHH_ORACLE_SUPP_MAP_VW xaav, APPS.AP_SUPPLIERS APS
             WHERE     TRIM (xaav.AHH_VENDOR_NUM) =
                          TRIM (UPPER (rec_hdr.vendor_num))
                   AND TRIM (xaav.oracle_vendor) = aps.segment1
                   AND ROWNUM = 1;
         EXCEPTION
            WHEN OTHERS
            THEN
               l_vendor_id := NULL;
         END;

         -----------------------------------------------------------------
         -- Derive VENDOR_ID
         -----------------------------------------------------------------
         IF l_vendor_id IS NULL
         THEN
            BEGIN
               SELECT vendor_id
                 INTO l_vendor_id
                 FROM ap_suppliers
                WHERE     1 = 1
                      AND SYSDATE BETWEEN start_date_active
                                      AND NVL (end_date_active, SYSDATE + 1)
                      AND enabled_flag = 'Y'
                      AND TRIM (UPPER (attribute1)) =
                             LTRIM (UPPER (rec_hdr.vendor_num), '0'); -- ap_suppliers.attribute1 stores AHH Vendor#
            EXCEPTION
               WHEN OTHERS
               THEN
                  l_rec_valid_flag := 'N';
                  l_error_message := 'Vendor Number is Not Valid';
            END;
         END IF;

         -- ????? > Start
         -----------------------------------------------------------------
         -- Validate Invoice Amount
         -----------------------------------------------------------------
         IF rec_hdr.invoice_amount != rec_hdr.sum_line_amount THEN
            l_rec_valid_flag := 'N';
            l_error_message := 'Invoice Amount not equal to sum of Line Amount';
         END IF;
         -- ????? < End

         /*   commented in Rev 1.3
          IF l_vendor_id IS NULL THEN
            -----------------------------------------------------------------
            -- Derive VENDOR_ID where Supplier DFF does not match
            -----------------------------------------------------------------
            BEGIN
              SELECT vendor_id
                INTO l_vendor_id
                FROM ap_suppliers
               WHERE 1 = 1
                 AND SYSDATE BETWEEN start_date_active AND NVL(end_date_active, SYSDATE + 1)
                 AND enabled_flag = 'Y'
                 AND segment1 = LTRIM (UPPER (rec_hdr.vendor_num), '0');  -- ap_suppliers.attribute1 stores PRISM Vendor#
            EXCEPTION
              WHEN OTHERS
              THEN
                l_rec_valid_flag   := 'N';
                l_error_message    := 'Vendor Number is Not Valid';
            END;
          END IF;
           */
         IF l_vendor_id IS NOT NULL
         THEN
            -----------------------------------------------------------------
            -- Validate for Duplicate Vendors
            -----------------------------------------------------------------
            BEGIN
               SELECT vendor_id
                 INTO l_vendor_id
                 FROM ap_invoices_all
                WHERE     1 = 1
                      AND vendor_id = l_vendor_id
                      AND invoice_num = TRIM (rec_hdr.invoice_num)
                      AND invoice_amount = TO_NUMBER (rec_hdr.invoice_amount);

               l_rec_valid_flag := 'N';
               l_error_message := 'Duplicate Invoice';
            EXCEPTION
               WHEN OTHERS
               THEN
                  l_rec_valid_flag := 'Y';
            END;

            BEGIN
               SELECT vendor_id
                 INTO l_vendor_id
                 FROM ap_invoices_all
                WHERE     1 = 1
                      AND vendor_id = l_vendor_id
                      AND invoice_num = TRIM (rec_hdr.invoice_num)
                      AND invoice_amount !=
                             TO_NUMBER (rec_hdr.invoice_amount);

               l_rec_valid_flag := 'N';
               l_error_message := 'Duplicate Invoice';
            EXCEPTION
               WHEN OTHERS
               THEN
                  l_rec_valid_flag := 'Y';
            END;

            BEGIN
               SELECT vendor_id
                 INTO l_vendor_id
                 FROM ap_invoices_all
                WHERE     1 = 1
                      AND vendor_id != l_vendor_id
                      AND invoice_num = TRIM (rec_hdr.invoice_num)
                      AND invoice_amount = TO_NUMBER (rec_hdr.invoice_amount)
                      AND invoice_date = rec_hdr.invoice_date;

               l_rec_valid_flag := 'N';
               l_error_message := 'Duplicate Invoice';
            EXCEPTION
               WHEN OTHERS
               THEN
                  l_rec_valid_flag := 'Y';
            END;

            -----------------------------------------------------------------
            -- Derive VENDOR_SITE_ID
            -----------------------------------------------------------------
            BEGIN
               SELECT vendor_site_id, accts_pay_code_combination_id, terms_id
                 INTO l_vendor_site_id, l_ccid, l_terms_id
                 FROM ap_supplier_sites_all
                WHERE     1 = 1
                      AND vendor_id = l_vendor_id
                      AND NVL (inactive_date, SYSDATE + 1) > SYSDATE
                      AND org_id = g_org_id
                      AND pay_site_flag = 'Y';
            EXCEPTION
               WHEN OTHERS
               THEN
                  l_rec_valid_flag := 'N';
                  l_error_message :=
                        l_error_message
                     || ', Error deriving PaymentSite for Vendor';
            END;
         ELSE
            l_rec_valid_flag := 'N';
         END IF;

         ----------------------------------------------------------------
         -- Derive DEFAULT_PAYMENT_METHOD_CODE
         -----------------------------------------------------------------
         IF l_vendor_site_id IS NOT NULL
         THEN
            BEGIN
               SELECT ieppm.payment_method_code
                 INTO l_payment_method_code
                 FROM iby_external_payees_all iepa,
                      iby_ext_party_pmt_mthds ieppm
                WHERE     1 = 1
                      AND iepa.ext_payee_id = ieppm.ext_pmt_party_id
                      AND iepa.supplier_site_id = l_vendor_site_id
                      AND ieppm.primary_flag = 'Y'
                      AND NVL (ieppm.inactive_date, SYSDATE + 1) > SYSDATE;

               IF l_payment_method_code IS NULL
               THEN
                  l_rec_valid_flag := 'N';
                  l_error_message :=
                        l_error_message
                     || 'PaymentMethod is not defined on VendorPaySite';
               END IF;
            EXCEPTION
               WHEN OTHERS
               THEN
                  l_rec_valid_flag := 'N';
                  l_error_message :=
                        l_error_message
                     || 'Error deriving DEFAULT_PAYMENT_METHOD_CODE';
            END;
         ELSE
            l_rec_valid_flag := 'N';
         END IF;

         -----------------------------------------------------------------
         -- Derive INVOICE TYPE
         -----------------------------------------------------------------
         IF TO_NUMBER (rec_hdr.invoice_amount) < 0
         THEN
            l_invoice_type := 'CREDIT';
         ELSE
            l_invoice_type := 'STANDARD';
         END IF;

         -----------------------------------------------------------------
         -- Derive PAYMENT TERM
         -----------------------------------------------------------------
         IF l_invoice_type = 'CREDIT'
         THEN
            l_terms_id := l_credit_terms_id;
         END IF;

         IF l_terms_id IS NULL
         THEN
            l_rec_valid_flag := 'N';
            l_error_message :=
                  l_error_message
               || ',PaymentTerm is not defined on VendorPaySite';
         END IF;

         IF l_rec_valid_flag = 'Y'
         THEN
            BEGIN
               -----------------------------------------------------------------
               -- Load AP_INVOICES_INTERFACE table
               -----------------------------------------------------------------
               INSERT INTO ap_invoices_interface (invoice_id,
                                                  invoice_num,
                                                  invoice_type_lookup_code,
                                                  invoice_date,
                                                  vendor_id,
                                                  invoice_amount,
                                                  invoice_currency_code,
                                                  terms_id,
                                                  last_update_date,
                                                  last_updated_by,
                                                  creation_date,
                                                  created_by,
                                                  gl_date,
                                                  org_id,
                                                  source,
                                                  vendor_site_id,
                                                  GROUP_ID,
                                                  payment_currency_code,
                                                  exclusive_payment_flag,
                                                  terms_date,
                                                  payment_method_code,
                                                  description,
                                                  attribute_category,
                                                  attribute1,
                                                  attribute3)
                       VALUES (
                                 ap_invoices_interface_s.NEXTVAL,
                                 TRIM (rec_hdr.invoice_num),
                                 l_invoice_type,
                                 rec_hdr.invoice_date,
                                 l_vendor_id,
                                 TO_NUMBER (rec_hdr.invoice_amount),
                                 g_currency_code,
                                 l_terms_id,
                                 SYSDATE,
                                 g_user_id,
                                 SYSDATE,
                                 g_user_id,
                                 g_gl_date,
                                 g_org_id,
                                 g_invoice_source,
                                 l_vendor_site_id,
                                 g_group_name,
                                 g_currency_code,
                                 'N',
                                 rec_hdr.invoice_date,
                                 l_payment_method_code,
                                 rec_hdr.attribute6,                    -- PO#
                                 g_org_id,               -- attribute_category
                                 RTRIM (
                                    REPLACE (
                                       REPLACE (rec_hdr.attribute1,
                                                CHR (13),
                                                ' '),
                                       CHR (10),
                                       ' ')),             --rec_hdr.attribute1
                                 rec_hdr.attribute2);

               l_invoice_num := rec_hdr.invoice_num;
               l_vendor_num := rec_hdr.vendor_num;
               l_line_num := 0;

               FOR rec_detl IN cur_detl (l_invoice_num, l_vendor_num)
               LOOP
                  l_line_num := l_line_num + 1;

                  -----------------------------------------------------------------
                  -- Derive Distribution Location
                  -----------------------------------------------------------------

                  --Changed done in Rev 1.3
                  BEGIN
                     SELECT NVL(UPPER (TRIM (ORACLE_BW_NUMBER)),'BW092')
                       INTO l_dist_location
                       FROM XXWC.XXWC_AP_BRANCH_LIST_STG_TBL
                      WHERE UPPER (TRIM (AHH_BRANCH_NUMBER)) =
                               UPPER (TRIM (rec_detl.branch));

                     /*
                     -- Version # 1.1 > Start
                     SELECT loc_map.ENTRP_LOC
                       INTO l_dist_location
                       FROM xxcus.xxcus_location_code_tbl loc_map
                      WHERE     1 = 1
                            AND loc_map.lob_branch =
                                   SUBSTR (rec_detl.gl_account, -3)
                            AND NVL (loc_map.inactive, 'N') = 'N'
                            AND loc_map.system_cd = 'MAS 500-WC' -- Version # 1.2
                                                                ;
                     */
                     fnd_file.put_line (
                        fnd_file.LOG,
                           'CodeCombination for Invoice - '
                        || rec_hdr.invoice_num
                        || ' is - '
                        || l_dist_code_combination);
                  EXCEPTION
                     WHEN OTHERS
                     THEN
                        fnd_file.put_line (
                           fnd_file.LOG,
                              'Error deriving Distribution Location for Invoice - '
                           || rec_hdr.invoice_num);
                  l_dist_location := 'BW092';
                  END;

                  -----------------------------------------------------------------
                  -- Derive Distribution CodeCombination
                  -----------------------------------------------------------------
                  -- Changed below code in Rev 1.3
                  BEGIN
                     SELECT    '0W'
                            || '.'
                            || l_dist_location
                            || '.'
                            || '0000'
                            || '.'
                            || acct_map.oracle_gl_account
                            || '.'
                            || '00000'
                            || '.'
                            || '00000'
                            || '.'
                            || '00000'
                               cc
                       INTO l_dist_code_combination
                       FROM XXWC.XXWC_AP_GL_MAP_STG_TBL acct_map
                      WHERE TRIM (AHH_GL_ACCOUNT) =
                               TRIM (SUBSTR (rec_detl.gl_account, 1, 6));

                     /* commented below code in Rev 1.3
                      SELECT    '0W'
                             || '.'
                             || l_dist_location
                             || '.'
                             || '0000'
                             || '.'
                             || acct_map.z_new_value
                             || '.'
                             || '00000'
                             || '.'
                             || '00000'
                             || '.'
                             || '00000'
                                cc
                        INTO l_dist_code_combination
                        FROM xxwc.xxwc_et_mapping_gtt_tbl acct_map
                       WHERE     1 = 1
                             AND acct_map.z_old_value || acct_map.z_old_value2 =
                                    SUBSTR (rec_detl.gl_account, 1, 6)
                             AND acct_map.lob = 'WhiteCap'
                             AND acct_map.system_cd = 'MAS 500-WC'
                             AND acct_map.map_level = 'ACCOUNT'
                             AND acct_map.map_sublevel = 'INBOUND';
                      */
                     fnd_file.put_line (
                        fnd_file.LOG,
                           'CodeCombination for Invoice - '
                        || rec_hdr.invoice_num
                        || ' is - '
                        || l_dist_code_combination);
                  -- Version # 1.1 < End

                  /*
                               SELECT gcc.code_combination_id
                                 INTO l_ccid
                                 FROM xxwc.xxwc_et_mapping_gtt_tbl acct_map
                                    , xxcus.xxcus_location_code_tbl  loc_map
                                    , gl_code_combinations           gcc
                                WHERE 1 = 1
                                  AND gcc.enabled_flag          = 'Y'
                                  AND gcc.segment1              = '0W'
                                  AND acct_map.z_old_value||acct_map.z_old_value2||loc_map.lob_branch = rec_detl.gl_account
                                  AND acct_map.lob              = 'WhiteCap'
                                  AND acct_map.system_cd        = 'MAS 500-WC'
                                  AND acct_map.map_level        = 'ACCOUNT'
                                  AND acct_map.map_sublevel     = 'INBOUND'
                                  AND gcc.segment2              = loc_map.ENTRP_LOC
                                  AND NVL(loc_map.inactive,'N') = 'N'
                                  AND gcc.segment3              = '0000'
                                  AND gcc.segment4              = acct_map.z_new_value
                                  AND gcc.segment5              = '00000'
                                  AND gcc.segment6              = '00000'
                                  AND gcc.segment7              = '00000';
                  */

                  EXCEPTION
                     WHEN OTHERS
                     THEN
                        fnd_file.put_line (
                           fnd_file.LOG,
                              'Error deriving Distribution CodeCombination for Invoice - '
                           || rec_hdr.invoice_num);
                  END;


                  -----------------------------------------------------------------
                  -- Load AP_INVOICE_LINES_INTERFACE table
                  -----------------------------------------------------------------
                  INSERT
                    INTO ap_invoice_lines_interface (invoice_id,
                                                     invoice_line_id,
                                                     line_number,
                                                     line_type_lookup_code,
                                                     amount,
                                                     accounting_date,
                                                     -- dist_code_combination_id,
                                                     dist_code_concatenated, -- Version # 1.1
                                                     last_updated_by,
                                                     last_update_date,
                                                     created_by,
                                                     creation_date,
                                                     org_id,
                                                     description)
                  VALUES (ap_invoices_interface_s.CURRVAL,
                          ap_invoice_lines_interface_s.NEXTVAL,
                          l_line_num,
                          g_line_type_lookup_code,
                          -- TO_NUMBER (rec_detl.invoice_amount), -- ?????
                          TO_NUMBER (rec_detl.line_amount), -- ?????
                          g_gl_date,
                          -- l_ccid,
                          l_dist_code_combination,            -- Version # 1.1
                          g_user_id,
                          SYSDATE,
                          g_user_id,
                          SYSDATE,
                          g_org_id,
                          rec_detl.attribute6                           -- PO#
                                             );

                  -----------------------------------------------------------------
                  -- Updating the Status
                  -----------------------------------------------------------------
                  UPDATE xxwc.xxwcap_inv_int_stg_tbl
                     SET status = 'PROCESSED',
                         error_message = l_error_message
                   WHERE ROWID = rec_detl.ROWID;
               END LOOP;
            EXCEPTION
               WHEN OTHERS
               THEN
                  l_rec_valid_flag := 'N';
                  l_error_message :=
                        l_error_message
                     || 'Erorr Loading the interface tables. InvoiceNum: '
                     || rec_hdr.invoice_num;
            END;
         ELSE
            UPDATE xxwc.xxwcap_inv_int_stg_tbl
               SET status = 'REJECTED', error_message = l_error_message
             WHERE     invoice_num = rec_hdr.invoice_num
                   AND vendor_num = rec_hdr.vendor_num;
         END IF;
      END LOOP;

      COMMIT;

      fnd_file.put_line (
         fnd_file.LOG,
         '----------------------------------------------------------------------------------');
      fnd_file.put_line (fnd_file.LOG, 'End of Procedure : LOAD_INTERFACE');
      fnd_file.put_line (
         fnd_file.LOG,
         '----------------------------------------------------------------------------------');
   EXCEPTION
      WHEN OTHERS
      THEN
         l_error_code := SQLCODE;
         l_error_message := SUBSTR (SQLERRM, 1, 200);
         fnd_file.put_line (
            fnd_file.LOG,
            'Exception Block of Stage to Interface Import Procedure');
         fnd_file.put_line (fnd_file.LOG,
                            'The Error Code Traced is : ' || l_error_code);
         fnd_file.put_line (
            fnd_file.LOG,
            'The Error Message Traced is : ' || l_error_message);
   END load_interface;

   /********************************************************************************
   ProcedureName : SUBMIT_PAY_INTERFACE
   Purpose       : API to submit Payables Import Process.

   HISTORY
   ===============================================================================
   VERSION DATE          AUTHOR(S)       DESCRIPTION
   ------- -----------   --------------- -----------------------------------------
   1.0     12/13/2011    Gopi Damuluri    Initial version.
   ********************************************************************************/
   PROCEDURE submit_pay_interface
   AS
      l_request_id      NUMBER;
      v_phase           VARCHAR2 (100);
      v_status          VARCHAR2 (100);
      v_dev_phase       VARCHAR2 (100);
      v_dev_status      VARCHAR2 (100);
      v_message         VARCHAR2 (2000);
      v_wait_outcome    BOOLEAN;
      l_error_code      NUMBER;
      l_error_message   VARCHAR2 (2000);
   BEGIN
      fnd_file.put_line (
         fnd_file.LOG,
         '----------------------------------------------------------------------------------');
      fnd_file.put_line (fnd_file.LOG,
                         'Start of Procedure : SUBMIT_PAY_INTERFACE');
      fnd_file.put_line (
         fnd_file.LOG,
         '----------------------------------------------------------------------------------');

      l_request_id :=
         apps.fnd_request.submit_request (
            'SQLAP',                                 -- Applicaiton Short Name
            'APXIIMPT',                       -- Concurrent Program Short Name
            'Payables Open Interface Import',    -- Description of the request
            TO_CHAR (SYSDATE, 'DD-MON-RR HH24:MI:SS'), -- Request Start Running Time
            FALSE,                                              -- Sub Request
            g_org_id,
            g_invoice_source,                                        -- Source
            g_group_name,                                             -- Group
            g_group_name,                                        -- Batch Name
            NULL,                                                 -- Hold Name
            NULL,                                               -- Hold Reason
            NULL,                                                   -- GL Date
            'Y',                                                      -- Purge
            NULL,                                              -- Trace Switch
            NULL,                                              -- Debug Switch
            NULL,                                          -- Summarize Report
            NULL,                                         -- Commit Batch Size
            NULL,                                                   -- User ID
            NULL                                                   -- Login ID
                );
      COMMIT;

      v_wait_outcome :=
         fnd_concurrent.wait_for_request (request_id   => l_request_id,
                                          interval     => 10,
                                          max_wait     => 15000,
                                          phase        => v_phase,
                                          status       => v_status,
                                          dev_phase    => v_dev_phase,
                                          dev_status   => v_dev_status,
                                          MESSAGE      => v_message);

      ----------------------------------------------------------------------------------
      -- Call to Document attachment Procedure If Standard program completed Normal
      ----------------------------------------------------------------------------------
      /* -- Below code commented in Rev 1.3 (as per Tim no attachments for AHH AP invoices for now)
       IF UPPER (v_dev_phase) = 'COMPLETE' AND UPPER (v_dev_status) = 'NORMAL'
       THEN
          APPS.XXWCAP_INV_INT_PKG.attach_document (l_error_message,
                                                   l_error_code);
       END IF;
      */
      fnd_file.put_line (
         fnd_file.LOG,
         '----------------------------------------------------------------------------------');
      fnd_file.put_line (fnd_file.LOG,
                         'End of Procedure : SUBMIT_PAY_INTERFACE');
      fnd_file.put_line (
         fnd_file.LOG,
         '----------------------------------------------------------------------------------');
   EXCEPTION
      WHEN OTHERS
      THEN
         l_error_code := SQLCODE;
         l_error_message := SUBSTR (SQLERRM, 1, 200);
         g_error_code := l_error_code;
         g_error_message := l_error_message;
         fnd_file.put_line (
            fnd_file.LOG,
            'Exception Block of Standard Program Call Procedure');
         fnd_file.put_line (fnd_file.LOG,
                            'The Error Code Traced is : ' || l_error_code);
         fnd_file.put_line (
            fnd_file.LOG,
            'The Error Message Traced is : ' || l_error_message);
   END submit_pay_interface;

   /********************************************************************************
   ProcedureName : ATTACH_DOCUMENT
   Purpose       : API to attach documents to AP Invoices

   HISTORY
   ===============================================================================
   VERSION DATE          AUTHOR(S)       DESCRIPTION
   ------- -----------   --------------- -----------------------------------------
   1.0     12/13/2011    Gopi Damuluri    Initial version.
   1.1     06/18/2012    Gopi Damuluri    Procedure is modified to
                                          consider only PRISM Invoices
   ********************************************************************************/
   PROCEDURE attach_document (p_errbuf OUT VARCHAR2, p_retcode OUT NUMBER)
   AS
      l_error_code             NUMBER;
      l_error_message          VARCHAR2 (240);
      l_rowid                  ROWID;
      l_attached_document_id   NUMBER;
      l_document_id            NUMBER;
      l_media_id               NUMBER;
      l_category_id            NUMBER := 1;
      l_pk1_value              fnd_attached_documents.pk1_value%TYPE;
      l_description            fnd_documents_tl.description%TYPE
                                  := 'MAS-CONVERSION,Document Attach';
      l_seq_num                NUMBER;
      l_documentid             NUMBER;

      CURSOR cur_hdr
      IS
         SELECT *
           FROM ap_invoices_all
          WHERE     1 = 1
                AND org_id = g_org_id
                AND source = g_invoice_source
                AND attribute1 IS NOT NULL;
   BEGIN
      fnd_file.put_line (
         fnd_file.LOG,
         '----------------------------------------------------------------------------------');
      fnd_file.put_line (fnd_file.LOG,
                         'Start of Procedure : ATTACH_DOCUMENT');
      fnd_file.put_line (
         fnd_file.LOG,
         '----------------------------------------------------------------------------------');

      FOR rec_hdr IN cur_hdr
      LOOP
         l_pk1_value := rec_hdr.invoice_id;

         SELECT fnd_documents_s.NEXTVAL INTO l_document_id FROM DUAL;

         SELECT fnd_attached_documents_s.NEXTVAL
           INTO l_attached_document_id
           FROM DUAL;

         SELECT NVL (MAX (seq_num), 0) + 10
           INTO l_seq_num
           FROM fnd_attached_documents
          WHERE pk1_value = l_pk1_value AND entity_name = 'AP_INVOICES';

         fnd_documents_pkg.insert_row (
            x_rowid               => l_rowid,
            x_document_id         => l_document_id,
            x_creation_date       => SYSDATE,
            x_created_by          => g_user_id,
            x_last_update_date    => SYSDATE,
            x_last_updated_by     => g_user_id,
            x_last_update_login   => g_login_id,
            x_datatype_id         => 5,                            -- Web Page
            x_category_id         => l_category_id,
            x_security_type       => 2,
            x_publish_flag        => 'Y',
            x_usage_type          => 'O',
            x_language            => 'US',
            x_description         => l_description,
            x_media_id            => l_media_id,
            x_url                 => TRIM (rec_hdr.attribute1));

         fnd_documents_pkg.insert_tl_row (x_document_id         => l_document_id,
                                          x_creation_date       => SYSDATE,
                                          x_created_by          => g_user_id,
                                          x_last_update_date    => SYSDATE,
                                          x_last_updated_by     => g_user_id,
                                          x_last_update_login   => g_login_id,
                                          x_language            => 'US');

         fnd_attached_documents_pkg.insert_row (
            x_rowid                      => l_rowid,
            x_attached_document_id       => l_attached_document_id,
            x_document_id                => l_document_id,
            x_creation_date              => SYSDATE,
            x_created_by                 => g_user_id,
            x_last_update_date           => SYSDATE,
            x_last_updated_by            => g_user_id,
            x_last_update_login          => g_login_id,
            x_seq_num                    => l_seq_num,
            x_entity_name                => 'AP_INVOICES',
            x_column1                    => NULL,
            x_pk1_value                  => l_pk1_value,
            x_pk2_value                  => NULL,
            x_pk3_value                  => NULL,
            x_pk4_value                  => NULL,
            x_pk5_value                  => NULL,
            x_automatically_added_flag   => 'N',
            x_datatype_id                => 5,
            x_category_id                => l_category_id,
            x_security_type              => 2,
            x_publish_flag               => 'Y',
            x_language                   => 'US',
            x_description                => l_description,
            x_media_id                   => l_media_id,
            x_url                        => TRIM (rec_hdr.attribute1));
         COMMIT;

         BEGIN
              SELECT document_id
                INTO l_documentid
                FROM fnd_attached_docs_form_vl
               WHERE     function_name = DECODE (0, 1, NULL, 'APXINWKB')
                     AND function_type = DECODE (0, 1, NULL, 'O')
                     AND (   security_type = 4
                          OR publish_flag = 'Y'
                          OR (security_type = 2 AND security_id = 2042))
                     AND (    entity_name = 'AP_INVOICES'
                          AND pk1_value = l_pk1_value)
            ORDER BY user_entity_name, seq_num;

            UPDATE ap_invoices_all
               SET attribute1 = NULL
             WHERE invoice_id = rec_hdr.invoice_id;
         EXCEPTION
            WHEN OTHERS
            THEN
               NULL;
         END;
      END LOOP;

      fnd_file.put_line (
         fnd_file.LOG,
         '----------------------------------------------------------------------------------');
      fnd_file.put_line (fnd_file.LOG, 'End of Procedure : ATTACH_DOCUMENT');
      fnd_file.put_line (
         fnd_file.LOG,
         '----------------------------------------------------------------------------------');
   EXCEPTION
      WHEN OTHERS
      THEN
         l_error_code := SQLCODE;
         l_error_message := SUBSTR (SQLERRM, 1, 200);
         fnd_file.put_line (fnd_file.LOG,
                            'Exception Block of Document Attach Procedure');
         fnd_file.put_line (fnd_file.LOG,
                            'The Error Code Traced is : ' || l_error_code);
         fnd_file.put_line (
            fnd_file.LOG,
            'The Error Message Traced is : ' || l_error_message);
   END attach_document;

   /********************************************************************************
   ProcedureName : load_global_temp_tbl
   Purpose       : API to load Accounts CrossReference global temporary table
                   from Enterprise Transalator
   HISTORY
   ===============================================================================
   VERSION DATE          AUTHOR(S)       DESCRIPTION
   ------- -----------   --------------- -----------------------------------------
   1.0     12/13/2011    Gopi Damuluri    Initial version.
   ********************************************************************************/
   PROCEDURE load_global_temp_tbl
   IS
      l_error_code      NUMBER;
      l_error_message   VARCHAR2 (240);
   BEGIN
      INSERT INTO xxwc.xxwc_et_mapping_gtt_tbl
         SELECT *
           FROM SYSADM.PS_ZY_OF_VAL_TBL@XXCUS_ET_DBLINK.HSI.HUGHESSUPPLY.COM;
   EXCEPTION
      WHEN OTHERS
      THEN
         l_error_code := SQLCODE;
         l_error_message := SUBSTR (SQLERRM, 1, 200);
         fnd_file.put_line (
            fnd_file.LOG,
            'Exception Block of Load Global Temporary Table Procedure');
         fnd_file.put_line (fnd_file.LOG,
                            'The Error Code Traced is : ' || l_error_code);
         fnd_file.put_line (
            fnd_file.LOG,
            'The Error Message Traced is : ' || l_error_message);
   END load_global_temp_tbl;

   /********************************************************************************
   ProcedureName : main
   Purpose       : API which orchestrates the whole process.

   HISTORY
   ===============================================================================
   VERSION DATE          AUTHOR(S)       DESCRIPTION
   ------- -----------   --------------- -----------------------------------------
   1.0     12/13/2011    Gopi Damuluri    Initial version.
   ********************************************************************************/
   ----------------------------------------------------------------------------------
   -- MAIN PROCEDURE
   ----------------------------------------------------------------------------------
   PROCEDURE main (p_errbuf           OUT VARCHAR2,
                   p_retcode          OUT NUMBER,
                   p_submit_intf   IN     VARCHAR2)
   AS
      l_flag       VARCHAR2 (10) DEFAULT 'Y';
      l_batch_id   NUMBER;

      CURSOR stg_hdr
      IS
         SELECT c.ROWID r_id, c.*
           FROM xxwc.xxwcap_inv_int_stg_tbl c
          WHERE NVL (status, 'XXXXX') <> 'PROCESSED';
   BEGIN
      fnd_file.put_line (
         fnd_file.LOG,
         '**********************************************************************************');
      fnd_file.put_line (
         fnd_file.LOG,
         '----------------------------------------------------------------------------------');
      fnd_file.put_line (fnd_file.LOG, 'Start of Procedure : MAIN');
      fnd_file.put_line (
         fnd_file.LOG,
         '----------------------------------------------------------------------------------');
      fnd_file.put_line (fnd_file.LOG, 'Input Parameters     :');
      fnd_file.put_line (fnd_file.LOG,
                         'p_submit_intf        :' || p_submit_intf);
      fnd_file.put_line (
         fnd_file.LOG,
         '----------------------------------------------------------------------------------');

      ----------------------------------------------------------------------------------
      -- Load the Interface Tables
      ----------------------------------------------------------------------------------
      --APPS.XXWCAP_INV_INT_PKG.load_global_temp_tbl;  commented by Vamshi.

      ----------------------------------------------------------------------------------
      -- Load the Interface Tables
      ----------------------------------------------------------------------------------
      APPS.XXWCAP_INV_INT_PKG.load_interface;

      ----------------------------------------------------------------------------------
      -- Submit Interface Program
      ----------------------------------------------------------------------------------
      IF p_submit_intf = 'Y'
      THEN
         APPS.XXWCAP_INV_INT_PKG.submit_pay_interface;
      END IF;

      /*
            ----------------------------------------------------------------------------------
            -- Error Report printing Statememnts
            ----------------------------------------------------------------------------------
            fnd_file.put_line (fnd_file.output, '<HTML><BODY>');
            fnd_file.put_line (fnd_file.output, '<PRE>');
            fnd_file.new_line (fnd_file.output, 1);
            fnd_file.put_line
               (fnd_file.output,
                '********************************************************************************************<BR>'
               );
            fnd_file.put_line
               (fnd_file.output,
                '<H3>                Custom Payable Open Interface ERROR Report                      </H3>'
               );
            fnd_file.put_line
               (fnd_file.output,
                '********************************************************************************************<BR>'
               );
            fnd_file.put_line (fnd_file.output,
                                  '<H4> 1. Program Name           : '
                               || ' XXWC_AP_ PAYABLES_OPEN_INTERFACE_IMPORT '
                              );
            fnd_file.put_line (fnd_file.output,
                                  ' 2. Start Date             :  '
                               || TO_CHAR (SYSDATE, 'DD-MON-YYYY HH24:MI:SS')
                              );
            fnd_file.put_line (fnd_file.output, '</H4>');
            fnd_file.put_line
               (fnd_file.output,
                '********************************************************************************************<BR>'
               );
            fnd_file.put_line (fnd_file.output, '<BR>');
            fnd_file.put_line
                           (fnd_file.output,
                            '<b>The following records may causing this failure</b> '
                           );
            fnd_file.put_line (fnd_file.output, '<TABLE BORDER=1>');
            fnd_file.put_line
               (fnd_file.output,
                '<TR><TH>SupplierNumber # </TH><TH>SupplierName # </TH><TH>InvoiceNumber </TH><TH>InvoiceDate </TH><TH>InvoiceAmount # </TH><TH>Rejection Reason # </TH></TR>'
               );

            FOR rec_stg IN stg_hdr
            LOOP
               fnd_file.put_line (fnd_file.output,
                                     '<TR><TD>'
                                  || rec_stg.vendor_num
                                  || '</TD><TD>'
                                  || rec_stg.vendor_name
                                  || '</TD><TD>'
                                  || rec_stg.invoice_num
                                  || '</TD><TD>'
                                  || rec_stg.invoice_date
                                  || '</TD><TD>'
                                  || rec_stg.invoice_amount
                                  || '</TD><TD>'
                                  || rec_stg.error_message
                                  || '</TD></TR>'
                                 );
            END LOOP;

            fnd_file.put_line (fnd_file.output, '</TABLE>');
            fnd_file.put_line (fnd_file.output, '</BODY></HTML>');
      */
      fnd_file.put_line (
         fnd_file.LOG,
         '----------------------------------------------------------------------------------');
      fnd_file.put_line (fnd_file.LOG, 'End of Procedure : MAIN');
      fnd_file.put_line (
         fnd_file.LOG,
         '----------------------------------------------------------------------------------');
   EXCEPTION
      WHEN OTHERS
      THEN
         fnd_file.put_line (fnd_file.LOG,
                            'The Error Code Traced is : ' || g_error_code);
         fnd_file.put_line (
            fnd_file.LOG,
            'The Error Message Traced is : ' || g_error_message);
   END main;

   /********************************************************************************
   ProcedureName : uc4_call
   Purpose       : API which is called from UC4 which submits the Load Staging and
                   Process Interface Concurrent Programs in Oracle.
   HISTORY
   ===============================================================================
   VERSION DATE          AUTHOR(S)       DESCRIPTION
   ------- -----------   --------------- -----------------------------------------
   1.0     12/13/2011    Gopi Damuluri    Initial version.
   ********************************************************************************/
   PROCEDURE uc4_call (p_errbuf                   OUT VARCHAR2,
                       p_retcode                  OUT NUMBER,
                       p_conc_prg_name         IN     VARCHAR2,
                       p_conc_prg_arg1         IN     VARCHAR2,
                       p_user_name             IN     VARCHAR2,
                       p_responsibility_name   IN     VARCHAR2,
                       p_org_name              IN     VARCHAR2,
                       p_threshold_value       IN     NUMBER)
   IS
      --
      -- Package Variables
      --
      l_package               VARCHAR2 (50) := 'XXWCAP_INV_INT_PKG';
      l_dflt_email            VARCHAR2 (200) := 'HDSOracleDevelopers@hdsupply.com';
      l_email                 fnd_user.email_address%TYPE;

      l_req_id                NUMBER NULL;
      v_phase                 VARCHAR2 (50);
      v_status                VARCHAR2 (50);
      v_dev_status            VARCHAR2 (50);
      v_dev_phase             VARCHAR2 (50);
      v_message               VARCHAR2 (250);
      v_error_message         VARCHAR2 (3000);
      v_supplier_id           NUMBER;
      v_rec_cnt               NUMBER := 0;
      l_message               VARCHAR2 (150);
      l_errormessage          VARCHAR2 (3000);
      pl_errorstatus          NUMBER;
      l_can_submit_request    BOOLEAN := TRUE;
      l_globalset             VARCHAR2 (100);
      l_err_msg               VARCHAR2 (3000);
      l_err_code              NUMBER;
      l_sec                   VARCHAR2 (255);
      l_statement             VARCHAR2 (9000);
      l_user                  fnd_user.user_id%TYPE;      --REBTINTERFACE user
      l_user_id               NUMBER;
      l_responsibility_id     NUMBER;
      l_resp_application_id   NUMBER;
      l_rej_rec_cnt           NUMBER;
      l_totl_rec_cnt          NUMBER;
      l_rej_percent           NUMBER;
      l_conc_prg_arg1         VARCHAR2 (900);

      -- Error DEBUG
      l_err_callfrom          VARCHAR2 (75) DEFAULT 'xxcus_error_pkg';
      l_err_callpoint         VARCHAR2 (75) DEFAULT 'START';
      l_distro_list           VARCHAR2 (75)
                                 DEFAULT 'HDSOracleDevelopers@hdsupply.com';
   BEGIN
      --------------------------------------------------------------------------
      -- Deriving UserId
      --------------------------------------------------------------------------
      BEGIN
         SELECT user_id
           INTO l_user_id
           FROM fnd_user
          WHERE     1 = 1
                AND user_name = UPPER (p_user_name)
                AND SYSDATE BETWEEN start_date
                                AND NVL (end_date, TRUNC (SYSDATE) + 1);
      EXCEPTION
         WHEN NO_DATA_FOUND
         THEN
            l_err_msg :=
               'UserName - ' || p_user_name || ' not defined in Oracle';
            RAISE PROGRAM_ERROR;
         WHEN OTHERS
         THEN
            l_err_msg :=
               'Error deriving user_id for UserName - ' || p_user_name;
            RAISE PROGRAM_ERROR;
      END;

      --------------------------------------------------------------------------
      -- Deriving ResponsibilityId and ResponsibilityApplicationId
      --------------------------------------------------------------------------
      BEGIN
         SELECT responsibility_id, application_id
           INTO l_responsibility_id, l_resp_application_id
           FROM fnd_responsibility_vl
          WHERE     responsibility_name = p_responsibility_name
                AND SYSDATE BETWEEN start_date
                                AND NVL (end_date, TRUNC (SYSDATE) + 1);
      EXCEPTION
         WHEN NO_DATA_FOUND
         THEN
            l_err_msg :=
                  'Responsibility - '
               || p_responsibility_name
               || ' not defined in Oracle';
            RAISE PROGRAM_ERROR;
         WHEN OTHERS
         THEN
            l_err_msg :=
                  'Error deriving Responsibility_id for ResponsibilityName - '
               || p_responsibility_name;
            RAISE PROGRAM_ERROR;
      END;

      l_sec :=
         'UC4 call to run concurrent request - PRISM2EBS AP Invoice Interface';
      l_conc_prg_arg1 := p_conc_prg_arg1;

      --------------------------------------------------------------------------
      -- Apps Initialize
      --------------------------------------------------------------------------
      FND_GLOBAL.APPS_INITIALIZE (l_user_id,
                                  l_responsibility_id,
                                  l_resp_application_id);

      --------------------------------------------------------------------------
      -- Submit "XXWC Create Outbound File Common Program"
      --------------------------------------------------------------------------
      l_req_id :=
         fnd_request.submit_request (application   => 'XXWC',
                                     program       => p_conc_prg_name,
                                     description   => NULL,
                                     start_time    => SYSDATE,
                                     sub_request   => FALSE,
                                     argument1     => l_conc_prg_arg1);

      COMMIT;

      DBMS_OUTPUT.put_line ('After fnd_request');

      IF (l_req_id != 0)
      THEN
         IF fnd_concurrent.wait_for_request (l_req_id,
                                             6,
                                             15000,
                                             v_phase,
                                             v_status,
                                             v_dev_phase,
                                             v_dev_status,
                                             v_message)
         THEN
            v_error_message :=
                  CHR (10)
               || 'ReqID='
               || l_req_id
               || ' DPhase '
               || v_dev_phase
               || ' DStatus '
               || v_dev_status
               || CHR (10)
               || ' MSG - '
               || v_message;

            -- Error Returned
            IF v_dev_phase != 'COMPLETE' OR v_dev_status != 'NORMAL'
            THEN
               l_statement :=
                     'An error occured running the XXWCAP_INV_INT_PKG '
                  || v_error_message
                  || '.';
               fnd_file.put_line (fnd_file.LOG, l_statement);
               fnd_file.put_line (fnd_file.output, l_statement);
               RAISE PROGRAM_ERROR;
            END IF;
         -- Then Success!
         ELSE
            l_statement :=
                  'An error occured running the XXWCAP_INV_INT_PKG '
               || v_error_message
               || '.';
            fnd_file.put_line (fnd_file.LOG, l_statement);
            fnd_file.put_line (fnd_file.output, l_statement);
            RAISE PROGRAM_ERROR;
         END IF;
      ELSE
         l_statement := 'An error occured running the XXWCAP_INV_INT_PKG ';
         fnd_file.put_line (fnd_file.LOG, l_statement);
         fnd_file.put_line (fnd_file.output, l_statement);
         RAISE PROGRAM_ERROR;
      END IF;

      ----------------------------------------------------------------------------------
      -- Validate ERROR RECORD COUNT against THRESHOLD VALUE.
      ----------------------------------------------------------------------------------
      BEGIN
         SELECT COUNT (1)
           INTO l_totl_rec_cnt
           FROM xxwc.xxwcap_inv_int_stg_tbl xaist
          WHERE     1 = 1
                AND TRUNC (creation_date) IN (SELECT MAX (
                                                        TRUNC (creation_date))
                                                FROM xxwc.xxwcap_inv_int_stg_tbl);

         SELECT COUNT (1)
           INTO l_rej_rec_cnt
           FROM xxwc.xxwcap_inv_int_stg_tbl xaist
          WHERE     1 = 1
                AND NVL (status, 'XXXXX') = 'REJECTED'
                AND TRUNC (creation_date) IN (SELECT MAX (
                                                        TRUNC (creation_date))
                                                FROM xxwc.xxwcap_inv_int_stg_tbl);

         l_rej_percent := (l_rej_rec_cnt * 100) / l_totl_rec_cnt;

         IF l_rej_percent = 0
         THEN
            p_retcode := 0;
         ELSIF l_rej_percent > 0 AND l_rej_percent < p_threshold_value
         THEN
            p_retcode := 1;
         ELSIF l_rej_percent > p_threshold_value
         THEN
            p_retcode := 2;
         END IF;

         p_errbuf :=
               'TotalRecordCount = '
            || l_totl_rec_cnt
            || '      RejectedRecordCount = '
            || l_rej_rec_cnt;
      EXCEPTION
         WHEN OTHERS
         THEN
            p_retcode := 2;
            l_statement := 'Error while validating the Error Record Counts';
            fnd_file.put_line (
               fnd_file.LOG,
               'Error while validating the Error Record Counts - ' || SQLERRM);
      END;

      DBMS_OUTPUT.put_line (l_sec);
      DBMS_OUTPUT.put_line ('Request ID:  ' || l_req_id);
   EXCEPTION
      WHEN PROGRAM_ERROR
      THEN
         ROLLBACK;
         l_err_code := 2;
         l_err_msg := l_sec;
         l_err_msg := l_err_msg || ' ERROR ' || SUBSTR (SQLERRM, 1, 2000);
         DBMS_OUTPUT.put_line (l_err_msg);
         fnd_file.put_line (fnd_file.LOG, l_err_msg);
         fnd_file.put_line (fnd_file.output, l_err_msg);
         l_err_callpoint := l_message;

         xxcus_error_pkg.xxcus_error_main_api (
            p_called_from         => l_err_callfrom,
            p_calling             => l_err_callpoint,
            p_request_id          => l_req_id,
            p_ora_error_msg       => SQLERRM,
            p_error_desc          => 'Error running XXWCAP_INV_INT_PKG package with PROGRAM ERROR',
            p_distribution_list   => l_distro_list,
            p_module              => 'AP');

         fnd_file.put_line (fnd_file.output, 'Fix the error!');
      WHEN OTHERS
      THEN
         fnd_file.put_line (fnd_file.LOG, l_sec);
         fnd_file.put_line (fnd_file.output, l_sec);
         l_err_code := 2;
         l_err_msg := l_sec;
         l_err_msg :=
               l_sec
            || l_err_msg
            || ' ERROR '
            || SQLCODE
            || SUBSTR (SQLERRM, 1, 2000);
         DBMS_OUTPUT.put_line (l_err_msg);
         fnd_file.put_line (fnd_file.LOG, l_err_msg);
         fnd_file.put_line (fnd_file.output, l_err_msg);
         l_err_callpoint := l_message;

         xxcus_error_pkg.xxcus_error_main_api (
            p_called_from         => l_err_callfrom,
            p_calling             => l_err_callpoint,
            p_request_id          => l_req_id,
            p_ora_error_msg       => SUBSTR (l_err_msg, 1, 2000),
            p_error_desc          => 'Error running XXWCAP_INV_INT_PKG package with OTHERS Exception',
            p_distribution_list   => l_distro_list,
            p_module              => 'AP');
   END uc4_call;
END XXWCAP_INV_INT_PKG;
/