CREATE OR REPLACE PACKAGE XXWC_CLASS_GROUP_DATAFIX_PKG AS
  /********************************************************************************
  FILE NAME: XXWC_CLASS_GROUP_DATAFIX_PKG.pks
  
  PROGRAM TYPE: PL/SQL Package Specification.
  
  PURPOSE: Data fix script for Inserting the new ABC Class and ABC Group for all Inventory Orgs.
  
  HISTORY
  ===============================================================================
  VERSION DATE          AUTHOR(S)       DESCRIPTION
  ------- -----------   --------------- -----------------------------------------
  1.0     22/12/2017    Ashwin Sridhar  Initial creation 
  ********************************************************************************/
PROCEDURE insert_class_group_p(p_errbuf          OUT VARCHAR2
                              ,p_retcode         OUT NUMBER
							  ,p_organization_id IN  NUMBER
                              );
							  
FUNCTION check_class_f(p_organization_id IN NUMBER
                      ,p_class_name      IN VARCHAR2) RETURN VARCHAR2;
					  
FUNCTION check_group_class_f(p_assignment_group_id IN NUMBER
                            ,p_abc_class_id        IN NUMBER) RETURN VARCHAR2;
	
END XXWC_CLASS_GROUP_DATAFIX_PKG;
/