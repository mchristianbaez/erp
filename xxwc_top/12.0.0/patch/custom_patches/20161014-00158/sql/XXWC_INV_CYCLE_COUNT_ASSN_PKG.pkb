CREATE OR REPLACE PACKAGE BODY XXWC_INV_CYCLE_COUNT_ASSN_PKG AS
  /********************************************************************************
  FILE NAME: XXWC_INV_CYCLE_COUNT_ASSN_PKG.pkb
  
  PROGRAM TYPE: PL/SQL Package Body.
  
  PURPOSE: For Updating the ABC Class based on Sales Velocity.
  
  HISTORY
  ===============================================================================
  VERSION DATE          AUTHOR(S)       DESCRIPTION
  ------- -----------   --------------- -----------------------------------------
  1.0     20/12/2017    Ashwin Sridhar  Initial Creation-TMS#20161014-00158 
  ********************************************************************************/
  
pl_dflt_email fnd_user.email_address%TYPE := 'HDSOracleDevelopers@hdsupply.com';
  
PROCEDURE assign_class_codes(p_errbuf          OUT VARCHAR2
                            ,p_retcode         OUT NUMBER
                            ,p_organization_id IN  NUMBER) IS
            
--Cursor to Select All the Item Category Assignments for Sales Velocity...            
CURSOR cu_items IS
SELECT MAA.INVENTORY_ITEM_ID
,      MAAG.ASSIGNMENT_GROUP_ID
,      MAAG.ORGANIZATION_ID
,      MAA.ABC_CLASS_ID
,      MAA.ROWID row_id
,      MSIB.SEGMENT1  Item_Number
,      msib.description
,      msib.item_type
,      ood.organization_name
,      CAT_TBL.EX_FLAG
,      CAT_TBL.CAT_CLASS
,      CAT_TBL.cat_desc
,(SELECT NVL(flv.ATTRIBUTE2,'N')
  FROM   FND_LOOKUP_VALUES_VL flv
  WHERE  flv.LOOKUP_TYPE ='ITEM_TYPE'
  AND    NVL(flv.ATTRIBUTE_CATEGORY,'XXWC_ITEM_TYPE_RETIREMENT')='XXWC_ITEM_TYPE_RETIREMENT'
  AND    flv.lookup_code=msib.item_type) itm_type_chk
FROM   MTL_ABC_ASSIGNMENTS          MAA
,      MTL_ABC_ASSIGNMENT_GROUPS    MAAG
,      MTL_SYSTEM_ITEMS_B           msib
,      org_organization_definitions ood
,(SELECT NVL(MCV.ATTRIBUTE9,'N') EX_FLAG
 ,       MCV.CATEGORY_CONCAT_SEGS CAT_CLASS
 ,       MCV.DESCRIPTION  cat_desc
 ,       MIC.INVENTORY_ITEM_ID
 ,       MIC.ORGANIZATION_ID
  FROM   MTL_ITEM_CATEGORIES MIC
  ,      MTL_CATEGORIES_V    MCV
  WHERE  1=1
  AND    MIC.CATEGORY_ID       = MCV.CATEGORY_ID
  AND    MCV.STRUCTURE_NAME    ='Item Categories') cat_tbl
WHERE  MAA.ASSIGNMENT_GROUP_ID   = MAAG.ASSIGNMENT_GROUP_ID
AND    MSIB.INVENTORY_ITEM_ID    = MAA.INVENTORY_ITEM_ID
AND    MSIB.ORGANIZATION_ID      = MAAG.ORGANIZATION_ID
AND    MAAG.ORGANIZATION_ID      = OOD.ORGANIZATION_ID
AND    cat_tbl.INVENTORY_ITEM_ID = MAA.INVENTORY_ITEM_ID
AND    cat_tbl.ORGANIZATION_ID   = MAAG.ORGANIZATION_ID
AND    MAAG.ORGANIZATION_ID      = NVL(p_organization_id,MAAG.ORGANIZATION_ID);
  
--Declaring Local Variables...
lv_class          VARCHAR2(20);
ln_user_id        NUMBER:=fnd_profile.value('USER_ID');
ln_rec_count      NUMBER:=0;
ln_total_count    NUMBER:=0;
lv_procedure_name VARCHAR2(50) := 'XXWC_INV_CYCLE_COUNT_ASSN_PKG.assign_class_codes';
lv_sec            VARCHAR2(250);
lv_excl_cat_cnt   NUMBER:=0;
ln_request_id     NUMBER:=fnd_global.conc_request_id;
 
BEGIN

  fnd_file.put_line(fnd_file.log, 'Program Begin...');
  fnd_file.put_line(fnd_file.log, 'Starting the Loop...');
  
  lv_sec:='Program Begin...';
  lv_sec:=lv_sec||'Starting the Loop...';
  
  fnd_file.put_line(fnd_file.output, 'Excluded Items List:');
  fnd_file.put_line(fnd_file.output, '====================');
  fnd_file.put_line(fnd_file.output, RPAD('Item Number',30)||'|'||RPAD('Description',160)||'|'||RPAD('CatClass',15)||'|'||RPAD('Catclass Description',100)
                                                           ||'|'||RPAD('Item Type',20)||'|'||RPAD('Organization',50)); 

  FOR rec_items in cu_items LOOP  
    
    fnd_file.put_line(fnd_file.log, 'Inventory Item ID..'||rec_items.inventory_item_id);
    fnd_file.put_line(fnd_file.log, 'Organization ID..'||rec_items.organization_id);
    fnd_file.put_line(fnd_file.log, 'Category Exclude Check..'||rec_items.ex_flag);
    fnd_file.put_line(fnd_file.log, 'Item Type Exclude Check..'||rec_items.itm_type_chk);
  
    lv_class:=NULL;
    
    ln_rec_count  :=ln_rec_count+1;
    ln_total_count:=ln_total_count+1;
  
    BEGIN
  
      SELECT NVL((SELECT DESCRIPTION
                  FROM   FND_LOOKUP_VALUES_VL
                  WHERE  LOOKUP_TYPE='XXWC_CYCLE_COUNT_VELOCITY_LKP'
                  AND    LOOKUP_CODE=micv.category_concat_segs),'B') abc_class 
      INTO   lv_class    
      FROM   mtl_item_categories_v micv
      WHERE  1=1 
      AND    category_set_name = 'Sales Velocity'
      AND    organization_id   = rec_items.organization_id
      AND    inventory_item_id = rec_items.inventory_item_id;
      
    EXCEPTION
    WHEN others THEN
    
      lv_class:=NULL;
      
    END;
    
    IF lv_class IS NULL THEN
    
      lv_class:='B';
    
    END IF;
    
    IF lv_class IS NOT NULL AND rec_items.EX_FLAG!='Y' AND rec_items.itm_type_chk!='Y' THEN
     
      --Updating the Assignment table class id for every 'ROWID' based on ABC Class... 
      UPDATE mtl_abc_assignments
      SET    abc_class_id                    =    (SELECT ABC_CLASS_ID
                                                   FROM   MTL_ABC_CLASSES
                                                   WHERE  ABC_CLASS_NAME  =lv_class
                                                   AND    organization_id =rec_items.organization_id
                                                   AND    NVL(DISABLE_DATE,SYSDATE)>=SYSDATE)
      ,      last_update_date                =     SYSDATE
      ,      last_updated_by                 =     ln_user_id
      WHERE rowid = rec_items.row_id; 
      
    END IF;
         
    --Checking for Items to be Excluded from mtl_item_assignments...
    IF rec_items.EX_FLAG='Y' OR rec_items.itm_type_chk='Y' THEN
    
      fnd_file.put_line(fnd_file.log, 'Inside Excluded Item IF Condition...');
	  
	  --Updating the Assignment table class id for every 'ROWID' based on ABC Class... 
      UPDATE mtl_abc_assignments
      SET    abc_class_id                    =    (SELECT ABC_CLASS_ID
                                                   FROM   MTL_ABC_CLASSES
                                                   WHERE  ABC_CLASS_NAME  ='E'
                                                   AND    organization_id =rec_items.organization_id
                                                   AND    NVL(DISABLE_DATE,SYSDATE)>=SYSDATE)
      ,      last_update_date                =     SYSDATE
      ,      last_updated_by                 =     ln_user_id
      WHERE rowid = rec_items.row_id;       

      lv_excl_cat_cnt:=lv_excl_cat_cnt+1;
	  
      fnd_file.put_line(fnd_file.output, RPAD(rec_items.Item_Number,30)||'|'||RPAD(rec_items.Description,160)||'|'||RPAD(rec_items.Cat_Class,15)||'|'||
      RPAD(rec_items.cat_desc,100)||'|'||RPAD(rec_items.Item_Type,20)||'|'||RPAD(rec_items.organization_name,50));

    END IF;

    IF ln_rec_count=1000 THEN
    
      COMMIT;
      
      ln_rec_count:=0;
    
    END IF;

  END LOOP;

  COMMIT;
  
  p_errbuf:='ABC Item Class Updated Successfully for rows '||ln_total_count;
  lv_sec  :=lv_sec||p_errbuf;
  fnd_file.put_line(fnd_file.log, p_errbuf);
  fnd_file.put_line(fnd_file.log, 'Excluded Item Count...'||lv_excl_cat_cnt);
     
  fnd_file.put_line(fnd_file.log,'Excluded Items Count updates to E in ABC Assignments '||lv_excl_cat_cnt);
  
EXCEPTION
WHEN others THEN

  p_retcode:=2;
  p_errbuf:='Inside Exception '||SQLERRM;
  lv_sec:=lv_sec||'Inside Exception ';
  fnd_file.put_line(fnd_file.log, p_errbuf);
  
  xxcus_error_pkg.xxcus_error_main_api(p_called_from       => lv_procedure_name
                                      ,p_calling           => lv_sec
                                      ,p_request_id        => fnd_global.conc_request_id
                                      ,p_ora_error_msg     => p_errbuf
                                      ,p_error_desc        => 'Error running XXWC Inventory Cycle Count Assignments'
                                      ,p_distribution_list => pl_dflt_email
                                      ,p_module            => 'XXWC');

END assign_class_codes;
    
END XXWC_INV_CYCLE_COUNT_ASSN_PKG;
/