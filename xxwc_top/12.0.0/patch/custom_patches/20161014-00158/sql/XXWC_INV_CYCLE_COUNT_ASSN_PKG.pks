CREATE OR REPLACE PACKAGE XXWC_INV_CYCLE_COUNT_ASSN_PKG AS
  /********************************************************************************
  FILE NAME: XXWC_INV_CYCLE_COUNT_ASSN_PKG.pks
  
  PROGRAM TYPE: PL/SQL Package Specification.
  
  PURPOSE: For Updating the ABC Class based on Sales Velocity.
  
  HISTORY
  ===============================================================================
  VERSION DATE          AUTHOR(S)       DESCRIPTION
  ------- -----------   --------------- -----------------------------------------
  1.0     20/12/2017    Ashwin Sridhar  Initial creation 
  ********************************************************************************/
PROCEDURE assign_class_codes(p_errbuf          OUT VARCHAR2
                            ,p_retcode         OUT NUMBER
							,p_organization_id IN NUMBER);
	
END XXWC_INV_CYCLE_COUNT_ASSN_PKG;
/