CREATE OR REPLACE PACKAGE BODY APPS.XXWC_INV_CYCLE_COUNT_PKG
AS

g_retcode NUMBER;
   /*************************************************************************
   *  Copyright (c) 2011 Lucidity Consulting Group
   *  All rights reserved.
   **************************************************************************
   *   $Header XXWC_CYCLE_COUNT_PKG.pks $
   *   Module Name: XXWC_CYCLE_COUNT_PKG.pks
   *
   *   PURPOSE:   This package is called by the concurrent program xxwc cyclecount
   *
   *   REVISIONS:
   *   Ver        Date        Author                     Description
   *   ---------  ----------  ---------------         ----------------------------------------------------------------------------------------------------
   *   1.0        09/30/2011  Vivek Lakaman             Initial Version
   *   1.1        07/05/2013  Lee Spitzer               Added Procedure Process PI TMS Ticket - 20130214-01096 Implement Physical Inventory enhancements
   *                                                    @SC @FIN / Implement Physical Inventory enhancements ...
   *   1.2        10/24/2013  Lee Spitzer               Added Not Exists Clause to PI Procedure TMS Ticket # 20131016-00470 Exclude items assigned to Cat/Class SP.SPPP from PI�s
   *   1.3        12/18/2013  Lee Spitzer               Update insert_store_locations procedure to only insert active organizations TMS Ticket # 131115-00103
   *   1.4        12/19/2013  Lee Spitzer               Added Procedure for compiling ABC Compile for all orgs. TMS Ticket # 20131118-00080 Automate the ABC Compiling for cycle counts on a weekly basis @SC / Automate the ABC Compiling for cycle counts on a weekly basis ...
   *   1.5        22/04/2016  Niraj Ranjan              TMS#20151214-00044  Review PI Scheduling with Inactive Items
   *   1.6        06/17/2016  Pattabhi                  TMS#20160708-00169 - Batch improvements for XXWC Cycle Count Scheduler for All Orgs
   *   1.7        09/01/2018  Ashwin Sridhar            TMS#20161014-00158 - Cycle Count Optimization - back to Oracle standards.Removed the hard coding.
   * **************************************************************************************************************************************************/

   /*************************************************************************
 *   Procedure : Write_Log
 *
 *   PURPOSE:   This procedure logs error message
 *   Parameter:
 *          IN
 *              p_debug_msg      -- Debug Message
 * ************************************************************************/
   -- add debug message to log table and concurrent log file
   PROCEDURE Write_Log (p_debug_level   IN NUMBER,
                        p_mod_name      IN VARCHAR2,
                        p_debug_msg     IN VARCHAR2)
   IS
      PRAGMA AUTONOMOUS_TRANSACTION;
   BEGIN
      --IF (p_debug_level >= FND_LOG.G_CURRENT_RUNTIME_LEVEL)
      --THEN
      -- Call Oracle FND_LOG API to insert the debug message
      FND_LOG.STRING (p_debug_level, p_mod_name, P_DEBUG_MSG);
      -- END IF;
      fnd_file.put_line (fnd_file.LOG, P_DEBUG_MSG);
      DBMS_OUTPUT.put_line (P_DEBUG_MSG);
      COMMIT;
   END Write_Log;

   /*************************************************************************
 *   Procedure : Write_Error
 *
 *   PURPOSE:   This procedure logs error message
 *   Parameter:
 *          IN
 *              p_debug_msg      -- Debug Message
 * ************************************************************************/

   --add message to concurrent output file
   PROCEDURE Write_Error (p_debug_msg IN VARCHAR2)
   IS
      l_req_id          NUMBER := fnd_global.conc_request_id;
      l_err_callfrom    VARCHAR2 (75) DEFAULT 'xxwc_inv_cycle_count_pkg';
      l_err_callpoint   VARCHAR2 (75) DEFAULT 'START';
      l_distro_list     VARCHAR2 (75)
                           DEFAULT 'HDSOracleDevelopers@hdsupply.com';
   BEGIN
      xxcus_error_pkg.xxcus_error_main_api (
         p_called_from         => l_err_callfrom,
         p_calling             => l_err_callpoint,
         p_request_id          => l_req_id,
         p_ora_error_msg       => SUBSTR (p_debug_msg, 1, 2000),
         p_error_desc          => 'Error running XXWC_INV_CYCLE_COUNT_PKG with PROGRAM ERROR',
         p_distribution_list   => l_distro_list,
         p_module              => 'AR');
   END Write_Error;

   /*************************************************************************
    *   Procedure : Write_output
    *
    *   PURPOSE:   This procedure logs message into concurrent output
    *   Parameter:
    *          IN
    *              p_debug_msg      -- Debug Message
    * ************************************************************************/
   PROCEDURE Write_output (p_debug_msg IN VARCHAR2)
   IS
   BEGIN
      fnd_file.put_line (fnd_file.output, p_debug_msg);
   END Write_output;

   /*************************************************************************
 *   Function/Procedure : insert_store_locations
 *
 *   PURPOSE:   This procedure to insert store location
 *   Parameter:
 *          IN
 * ************************************************************************/

   PROCEDURE INSERT_STORE_LOCATIONS
   IS
      l_org_id   NUMBER := fnd_profile.VALUE ('ORG_ID');
   BEGIN
      --DELETE FROM   xxwc_cc_store_locations;

      INSERT INTO xxwc_cc_store_locations (inv_org_id,
                                           inv_org_name,
                                           select_trx)
         SELECT organization_id inv_org_id,
                organization_name inv_org_name,
                'N' select_trx
           FROM org_organization_definitions ood
          WHERE     organization_code = SUBSTR (organization_name, 1, 3)
                AND operating_unit = l_org_id
                AND NOT EXISTS
                       (SELECT '1'
                          FROM xxwc_cc_store_locations loc
                         WHERE ood.organization_id = loc.inv_org_id)
                ---TMS Ticket # 20131115-00103 Added 12/18/2013 to only include active organizations
                AND EXISTS
                       (SELECT *
                          FROM hr_all_organization_units haou
                         WHERE     haou.organization_Id = ood.organization_id
                               AND NVL (haou.date_to, SYSDATE + 1) > SYSDATE);


      COMMIT;
   END INSERT_STORE_LOCATIONS;

   /*************************************************************************
    *   Function/Procedure : Is_Day_Blockedout
    *
    *   PURPOSE:   This procedure to check if the day is blocked out due to holiday
    *   Parameter:
    *          IN
    *              p_inv_org_id      -- Inventory org
    *              p_schedule_date   -- Schedule Date
    * ************************************************************************/
   FUNCTION Is_Day_Blockedout (p_inv_org_id      IN NUMBER,
                               p_schedule_date   IN DATE)
      RETURN VARCHAR2
   IS
      l_count      NUMBER;
      l_yes_flag   VARCHAR2 (1) := 'Y';
      l_no_flag    VARCHAR2 (1) := 'N';
   BEGIN
      SELECT COUNT (*)
        INTO l_count
        FROM xxwc_inv_cc_blockout_dates
       WHERE     inv_org_id = p_inv_org_id
             --        AND p_schedule_date BETWEEN start_date AND end_date; removed 3/3/2013
             AND TRUNC (p_schedule_date) BETWEEN TRUNC (start_date)
                                             AND TRUNC (end_date); --added 3/3/2013;

      IF NVL (l_count, 0) > 0
      THEN
         RETURN l_yes_flag;
      ELSE
         RETURN l_no_flag;
      END IF;
   EXCEPTION
      WHEN OTHERS
      THEN
         RETURN l_no_flag;
   END Is_Day_Blockedout;

   /*************************************************************************
    *   Function/Procedure : Get_Process_Date
    *
    *   PURPOSE:   This procedure to get the process date
    *   Parameter:
    *          IN
    *              p_process_now_flag -- Process Now Flag
    *              p_schedule_date   -- Schedule Date
    *              p_inv_org_id      -- Inventory org
    * ************************************************************************/
   PROCEDURE Get_Process_Date (p_inv_org_id      IN     NUMBER,
                               px_process_date   IN OUT DATE,
                               px_days_to_add    IN OUT NUMBER)
   IS
      l_day_of_week   VARCHAR2 (100);
      l_mod_name      VARCHAR2 (100)
                         := 'xxwc_inv_cycle_count_pkg.get_process_date';
   BEGIN
      Write_Log (
         g_LEVEL_STATEMENT,
         l_mod_name,
            ' px_process_date= '
         || TO_CHAR (px_process_date, 'DD-MON-YYYY HH12:MI:SS AM'));
      Write_Log (g_LEVEL_STATEMENT,
                 l_mod_name,
                 ' p_inv_org_id= ' || p_inv_org_id);

      LOOP
         SELECT RTRIM (TO_CHAR (px_process_date, 'DAY'))
           INTO l_day_of_week
           FROM DUAL;

         Write_Log (g_LEVEL_STATEMENT,
                    l_mod_name,
                    ' l_day_of_week=' || l_day_of_week);
         Write_Log (g_LEVEL_STATEMENT,
                    l_mod_name,
                    ' length=' || LENGTH (l_day_of_week));
         Write_Log (
            g_LEVEL_STATEMENT,
            l_mod_name,
               ' Blocked Y/N ='
            || Is_Day_Blockedout (p_inv_org_id, px_process_date));

         EXIT WHEN     l_day_of_week IN ('MONDAY',
                                         'TUESDAY',
                                         'WEDNESDAY',
                                         'THURSDAY',
                                         'FRIDAY')
                   AND Is_Day_Blockedout (p_inv_org_id, px_process_date) =
                          'N';
         px_process_date := px_process_date + 1;
         px_days_to_add := px_days_to_add + 1;
      END LOOP;

      Write_Log (
         g_LEVEL_STATEMENT,
         l_mod_name,
            ' px_process_date= '
         || TO_CHAR (px_process_date, 'DD-MON-YYYY HH12:MI:SS AM'));
   EXCEPTION
      WHEN OTHERS
      THEN
         Write_Log (g_LEVEL_STATEMENT,
                    l_mod_name,
                    ' Error Msg = ' || SQLERRM);
   END Get_Process_Date;

   /*************************************************************************
    *   Function/Procedure : IS_ITEM_EXISTS
    *
    *   PURPOSE:   This procedure to get inventory Item Id for the given inv organization
    *   Parameter:
    *          IN
    *              p_item_number -- Item Number
    *              p_inv_org_id      -- Inventory org
    * ************************************************************************/

   FUNCTION IS_ITEM_EXISTS (p_item_number   IN VARCHAR2,
                            p_inv_org_id    IN NUMBER)
      RETURN NUMBER
   IS
      l_inv_item_id   NUMBER;
   BEGIN
      BEGIN
         SELECT inventory_item_id
           INTO l_inv_item_id
           FROM mtl_system_items
          WHERE segment1 = p_item_number AND organization_id = p_inv_org_id;
      EXCEPTION
         WHEN OTHERS
         THEN
            RETURN NULL;
      END;


      RETURN l_inv_item_id;
   END IS_ITEM_EXISTS;

   /*************************************************************************
    *   Function/Procedure : IS_Category_Exists
    *
    *   PURPOSE:   This procedure to get Category Id for the given category
    *   Parameter:
    *          IN
    *              p_Category -- category
    * ************************************************************************/
   FUNCTION IS_Category_Exists (p_Category IN VARCHAR2)
      RETURN NUMBER
   IS
      l_Category_id   NUMBER;
   BEGIN
      SELECT category_id
        INTO l_Category_id
        FROM mtl_categories_v
       WHERE category_concat_segs = p_Category;

      RETURN l_Category_id;
   EXCEPTION
      WHEN OTHERS
      THEN
         RETURN NULL;
   END IS_Category_Exists;


   /*************************************************************************
    *   Function/Procedure : Get_Category_list
    *
    *   PURPOSE:   This procedure to get the List of Category Id from Category String
    *   Parameter:
    *          IN
    *              p_store_loc_tbl -- List of Store Locations
    *              p_part_category -- Category String
    *          OUT
    *              x_category_tbl -- List of Category Id
    * ************************************************************************/


   PROCEDURE Get_Category_list (p_store_loc_tbl   IN     store_location_tbl,
                                p_part_category   IN     VARCHAR2,
                                x_category_tbl       OUT category_tbl)
   IS
      l_string           VARCHAR2 (32000);
      l_comma_pos        NUMBER;
      l_percent_pos      NUMBER;
      l_dash_pos         NUMBER;
      i                  NUMBER;
      lx_category_id     NUMBER;

      l_mod_name         VARCHAR2 (100)
                            := 'xxwc_inv_cycle_count_pkg.get_category_list';

      l_category         mtl_item_categories_v.category_concat_segs%TYPE;
      l_start_category   mtl_item_categories_v.category_concat_segs%TYPE;
      l_end_category     mtl_item_categories_v.category_concat_segs%TYPE;

      CURSOR C_dash
      IS
         SELECT category_concat_segs category, category_id
           FROM mtl_categories_v
          WHERE category_concat_segs BETWEEN l_start_category
                                         AND l_end_category;

      CURSOR C_percent
      IS
         SELECT category_concat_segs category, category_id
           FROM mtl_categories_v
          WHERE category_concat_segs LIKE p_part_category;
   BEGIN
      l_string := p_part_category;
      Write_Log (g_LEVEL_STATEMENT, l_mod_name, ' At Begin ');
      Write_Log (g_LEVEL_STATEMENT,
                 l_mod_name,
                 'p_store_loc_tbl.COUNT=' || p_store_loc_tbl.COUNT);

      SELECT INSTR (l_string, ',') INTO l_comma_pos FROM DUAL;

      SELECT INSTR (l_string, '%') INTO l_percent_pos FROM DUAL;

      SELECT INSTR (l_string, '-') INTO l_dash_pos FROM DUAL;

      Write_Log (g_LEVEL_STATEMENT,
                 l_mod_name,
                 'l_comma_pos=' || l_comma_pos);
      Write_Log (g_LEVEL_STATEMENT,
                 l_mod_name,
                 'l_percent_pos=' || l_percent_pos);
      Write_Log (g_LEVEL_STATEMENT, l_mod_name, ' l_dash_pos=' || l_dash_pos);

      i := 0;
      x_category_tbl.delete;

      IF p_store_loc_tbl.COUNT > 0
      THEN
         -- FOR org IN p_store_loc_tbl.FIRST .. p_store_loc_tbl.LAST
         -- LOOP
         IF l_comma_pos > 0
         THEN
            LOOP
               SELECT INSTR (l_string, ',') INTO l_comma_pos FROM DUAL;

               Write_Log (g_LEVEL_STATEMENT,
                          l_mod_name,
                          'in Loop l_string=' || l_string);
               Write_Log (g_LEVEL_STATEMENT,
                          l_mod_name,
                          'in Loop l_comma_pos=' || l_comma_pos);

               IF l_comma_pos = 0 AND l_string IS NOT NULL
               THEN
                  --Added 3/3/2013
                  FOR org IN p_store_loc_tbl.FIRST .. p_store_loc_tbl.LAST
                  LOOP
                     lx_category_id :=
                        IS_Category_Exists (p_Category => l_string);

                     Write_Log (g_LEVEL_STATEMENT,
                                l_mod_name,
                                'lx_category_id=' || lx_category_id);

                     IF lx_category_id IS NOT NULL
                     THEN
                        i := i + 1;
                        x_category_tbl (i).category := l_string;
                        x_category_tbl (i).category_id := lx_category_id;
                        x_category_tbl (i).inv_org_id :=
                           p_store_loc_tbl (org).inv_org_id;
                     END IF;
                  --Added 3/3/2013
                  END LOOP;
               END IF;

               EXIT WHEN (l_comma_pos = 0 OR l_string IS NULL);


               SELECT SUBSTR (l_string, 1, l_comma_pos - 1)
                 INTO l_category
                 FROM DUAL;

               Write_Log (g_LEVEL_STATEMENT,
                          l_mod_name,
                          'l_category=' || l_category);

               lx_category_id := IS_Category_Exists (p_Category => l_category);
               Write_Log (g_LEVEL_STATEMENT,
                          l_mod_name,
                          'lx_category_id=' || lx_category_id);

               --added 3/3/2013
               FOR org IN p_store_loc_tbl.FIRST .. p_store_loc_tbl.LAST
               LOOP
                  IF lx_category_id IS NOT NULL
                  THEN
                     i := i + 1;
                     x_category_tbl (i).category := l_category;
                     x_category_tbl (i).category_id := lx_category_id;
                     x_category_tbl (i).inv_org_id :=
                        p_store_loc_tbl (org).inv_org_id;
                  END IF;
               --Removed 3/3/2013
               END LOOP;

               SELECT SUBSTR (l_string, l_comma_pos + 1)
                 INTO l_string
                 FROM DUAL;
            END LOOP;
         ELSIF l_dash_pos > 4
         THEN
            SELECT SUBSTR (l_string, 1, l_dash_pos - 1)
              INTO l_start_category
              FROM DUAL;

            SELECT SUBSTR (l_string, l_dash_pos + 1)
              INTO l_end_category
              FROM DUAL;

            --Added 3/3/2013
            FOR org IN p_store_loc_tbl.FIRST .. p_store_loc_tbl.LAST
            LOOP
               FOR cat IN C_dash
               LOOP
                  i := i + 1;
                  x_category_tbl (i).category := cat.category;
                  x_category_tbl (i).category_id := cat.category_id;
                  x_category_tbl (i).inv_org_id :=
                     p_store_loc_tbl (org).inv_org_id;
               END LOOP;
            --Added 3/3/2013
            END LOOP;
         ELSIF l_percent_pos > 1
         THEN
            --Added 3/3/2013
            FOR org IN p_store_loc_tbl.FIRST .. p_store_loc_tbl.LAST
            LOOP
               FOR cat IN C_percent
               LOOP
                  i := i + 1;
                  x_category_tbl (i).category := cat.category;
                  x_category_tbl (i).category_id := cat.category_id;
                  x_category_tbl (i).inv_org_id :=
                     p_store_loc_tbl (org).inv_org_id;
               END LOOP;
            --added
            END LOOP;
         ELSE
            --added 3/3/2013
            FOR org IN p_store_loc_tbl.FIRST .. p_store_loc_tbl.LAST
            LOOP
               lx_category_id := IS_Category_Exists (p_Category => l_string);

               IF lx_category_id IS NOT NULL
               THEN
                  i := i + 1;
                  x_category_tbl (i).category := l_string;
                  x_category_tbl (i).category_id := lx_category_id;
                  x_category_tbl (i).inv_org_id :=
                     p_store_loc_tbl (org).inv_org_id;
               END IF;
            --Added 3/3/2013
            END LOOP;
         END IF;
      --Removed 3/3/2013
      --END LOOP;
      END IF;
   END Get_Category_list;

   /*************************************************************************
    *   Function/Procedure : Get_parts_list
    *
    *   PURPOSE:   This procedure to get the List of Item Id from the Item String
    *   Parameter:
    *          IN
    *              p_store_loc_tbl -- List of Store Locations
    *              p_part_category -- Item String
    *          OUT
    *              x_item_number_tbl -- List of ItemId
    * ************************************************************************/
   PROCEDURE Get_parts_list (p_store_loc_tbl     IN     store_location_tbl,
                             p_part_category     IN     VARCHAR2,
                             x_item_number_tbl      OUT item_number_tbl)
   IS
      l_string         VARCHAR2 (32000);
      l_comma_pos      NUMBER;
      l_item_num       mtl_system_items.segment1%TYPE;
      l_start_item     mtl_system_items.segment1%TYPE;
      l_end_item       mtl_system_items.segment1%TYPE;
      l_percent_pos    NUMBER;
      l_dash_pos       NUMBER;
      i                NUMBER;
      lx_inv_item_id   NUMBER;
      l_mod_name       VARCHAR2 (100)
                          := 'xxwc_inv_cycle_count_pkg.get_parts_list';

      CURSOR C1 (
         p_inv_org_id    NUMBER)
      IS
         SELECT inventory_item_id, segment1 item_number
           FROM mtl_system_items_fvl
          WHERE     segment1 BETWEEN l_start_item AND l_end_item
                AND cycle_count_enabled_flag = 'Y'
                --AND item_type NOT IN ('RENTAL', 'RE-RENTAL', 'REPAIR') --Commented by Ashwin.S on 09-Jan-2018 for TMS#20161014-00158
                AND inventory_item_status_code = 'Active'
                AND organization_id = p_inv_org_id
                --Added the EXIST condition by Ashwin.S on 09-Jan-2018 for TMS#20161014-00158
                AND NOT EXISTS
                (SELECT 1
                 FROM   FND_LOOKUP_VALUES_VL FLV
                WHERE  FLV.LOOKUP_TYPE ='ITEM_TYPE'
                AND    NVL(FLV.ATTRIBUTE_CATEGORY,'XXWC_ITEM_TYPE_RETIREMENT')='XXWC_ITEM_TYPE_RETIREMENT'
                AND    FLV.LOOKUP_CODE=ITEM_TYPE
                AND    NVL(flv.ATTRIBUTE2,'N')='Y');

      CURSOR C2 (
         p_inv_org_id    NUMBER)
      IS
         SELECT inventory_item_id, segment1 item_number
           FROM mtl_system_items_fvl
          WHERE     segment1 LIKE p_part_category
                AND cycle_count_enabled_flag = 'Y'
                --AND item_type NOT IN ('RENTAL', 'RE-RENTAL', 'REPAIR') --Commented by Ashwin.S on 09-Jan-2018 for TMS#20161014-00158
                AND inventory_item_status_code = 'Active'
                AND organization_id = p_inv_org_id
                --Added the EXIST condition by Ashwin.S on 09-Jan-2018 for TMS#20161014-00158
                AND NOT EXISTS
                (SELECT 1
                 FROM   FND_LOOKUP_VALUES_VL FLV
                WHERE  FLV.LOOKUP_TYPE ='ITEM_TYPE'
                AND    NVL(FLV.ATTRIBUTE_CATEGORY,'XXWC_ITEM_TYPE_RETIREMENT')='XXWC_ITEM_TYPE_RETIREMENT'
                AND    FLV.LOOKUP_CODE=ITEM_TYPE
                AND    NVL(flv.ATTRIBUTE2,'N')='Y');
   BEGIN
      l_string := p_part_category;

      SELECT INSTR (l_string, ',') INTO l_comma_pos FROM DUAL;

      SELECT INSTR (l_string, '%') INTO l_percent_pos FROM DUAL;

      SELECT INSTR (l_string, '-') INTO l_dash_pos FROM DUAL;

      /*
            write_Log (g_LEVEL_STATEMENT, l_mod_name, ' l_string=' || l_string);
            Write_Log (g_LEVEL_STATEMENT,
                       l_mod_name,
                       ' l_comma_pos=' || l_comma_pos);
            Write_Log (g_LEVEL_STATEMENT,
                       l_mod_name,
                       ' l_percent_pos=' || l_percent_pos);
            Write_Log (g_LEVEL_STATEMENT, l_mod_name, ' l_dash_pos=' || l_dash_pos);
      */

      i := 0;
      x_item_number_tbl.delete;

      Write_Log (g_LEVEL_STATEMENT,
                 l_mod_name,
                 'p_store_loc_tbl.COUNT=' || p_store_loc_tbl.COUNT);

      IF p_store_loc_tbl.COUNT > 0
      THEN
         --THEN
         Write_Log (g_LEVEL_STATEMENT,
                    l_mod_name,
                    ' --------------------------------');

         IF l_comma_pos > 0
         THEN
            LOOP
               SELECT INSTR (l_string, ',') INTO l_comma_pos FROM DUAL;

               Write_Log (g_LEVEL_STATEMENT,
                          l_mod_name,
                          'Comma Position Greater Than 0');

               Write_Log (g_LEVEL_STATEMENT,
                          l_mod_name,
                          'l_comma_pos=' || l_comma_pos);


               Write_Log (g_LEVEL_STATEMENT,
                          l_mod_name,
                          'l_string=' || l_string);

               IF l_comma_pos = 0 AND l_string IS NOT NULL
               THEN
                  Write_Log (
                     g_LEVEL_STATEMENT,
                     l_mod_name,
                     'Comma Position Greater Than 0 but loop comma pos = 0');

                  FOR org IN p_store_loc_tbl.FIRST .. p_store_loc_tbl.LAST
                  LOOP
                     lx_inv_item_id :=
                        is_item_exists (
                           p_item_number   => l_string,
                           p_inv_org_id    => p_store_loc_tbl (org).inv_org_id);
                     Write_Log (
                        g_LEVEL_STATEMENT,
                        l_mod_name,
                           'lx_org_id='
                        || p_store_loc_tbl (org).inv_org_id
                        || ' lx_inv_item_id='
                        || lx_inv_item_id);

                     IF lx_inv_item_id IS NOT NULL
                     THEN
                        i := i + 1;
                        x_item_number_tbl (i).item_number := l_string;
                        x_item_number_tbl (i).inv_item_id := lx_inv_item_id;
                        x_item_number_tbl (i).inv_org_id :=
                           p_store_loc_tbl (org).inv_org_id;
                     END IF;
                  END LOOP;
               END IF;

               Write_Log (
                  g_LEVEL_STATEMENT,
                  l_mod_name,
                  'Comma Position Greater Than 0 but loop comma pos > 0');

               SELECT SUBSTR (l_string, 1, l_comma_pos - 1)
                 INTO l_item_num
                 FROM DUAL;


               FOR org IN p_store_loc_tbl.FIRST .. p_store_loc_tbl.LAST
               LOOP
                  Write_Log (
                     g_LEVEL_STATEMENT,
                     l_mod_name,
                        'lx_org_id='
                     || p_store_loc_tbl (org).inv_org_id
                     || ' l_item_num='
                     || l_item_num);

                  lx_inv_item_id :=
                     is_item_exists (
                        p_item_number   => l_item_num,
                        p_inv_org_id    => p_store_loc_tbl (org).inv_org_id);

                  Write_Log (
                     g_LEVEL_STATEMENT,
                     l_mod_name,
                        'lx_org_id='
                     || p_store_loc_tbl (org).inv_org_id
                     || ' lx_inv_item_id='
                     || lx_inv_item_id);

                  IF lx_inv_item_id IS NOT NULL
                  THEN
                     i := i + 1;
                     x_item_number_tbl (i).item_number := l_item_num;
                     x_item_number_tbl (i).inv_item_id := lx_inv_item_id;
                     x_item_number_tbl (i).inv_org_id :=
                        p_store_loc_tbl (org).inv_org_id;
                  END IF;
               END LOOP;


               SELECT SUBSTR (l_string, l_comma_pos + 1)
                 INTO l_string
                 FROM DUAL;

               EXIT WHEN (l_comma_pos = 0 OR l_string IS NULL);
            END LOOP;
         ELSIF l_dash_pos > 4
         THEN
            SELECT SUBSTR (l_string, 1, l_dash_pos - 1)
              INTO l_start_item
              FROM DUAL;

            SELECT SUBSTR (l_string, l_dash_pos + 1)
              INTO l_end_item
              FROM DUAL;

            FOR org IN p_store_loc_tbl.FIRST .. p_store_loc_tbl.LAST
            LOOP
               FOR item IN C1 (p_store_loc_tbl (org).inv_org_id)
               LOOP
                  i := i + 1;
                  lx_inv_item_id :=
                     is_item_exists (
                        p_item_number   => item.item_number,
                        p_inv_org_id    => p_store_loc_tbl (org).inv_org_id);

                  IF lx_inv_item_id IS NOT NULL
                  THEN
                     x_item_number_tbl (i).item_number := item.item_number;
                     x_item_number_tbl (i).inv_item_id := lx_inv_item_id;
                     x_item_number_tbl (i).inv_org_id :=
                        p_store_loc_tbl (org).inv_org_id;
                  END IF;
               END LOOP;
            END LOOP;
         ELSIF l_percent_pos > 1
         THEN
            FOR org IN p_store_loc_tbl.FIRST .. p_store_loc_tbl.LAST
            LOOP
               FOR item IN C2 (p_store_loc_tbl (org).inv_org_id)
               LOOP
                  i := i + 1;
                  lx_inv_item_id :=
                     is_item_exists (
                        p_item_number   => item.item_number,
                        p_inv_org_id    => p_store_loc_tbl (org).inv_org_id);

                  IF lx_inv_item_id IS NOT NULL
                  THEN
                     x_item_number_tbl (i).item_number := item.item_number;
                     x_item_number_tbl (i).inv_item_id := lx_inv_item_id;
                     x_item_number_tbl (i).inv_org_id :=
                        p_store_loc_tbl (org).inv_org_id;
                  END IF;
               END LOOP;
            END LOOP;
         ELSE
            FOR org IN p_store_loc_tbl.FIRST .. p_store_loc_tbl.LAST
            LOOP
               Write_Log (g_LEVEL_STATEMENT,
                          l_mod_name,
                          'Comma Position = 0');
               i := i + 1;
               lx_inv_item_id :=
                  is_item_exists (
                     p_item_number   => l_string,
                     p_inv_org_id    => p_store_loc_tbl (org).inv_org_id);
               Write_Log (
                  g_LEVEL_STATEMENT,
                  l_mod_name,
                     'lx_org_id='
                  || p_store_loc_tbl (org).inv_org_id
                  || ' lx_inv_item_id='
                  || lx_inv_item_id);

               IF lx_inv_item_id IS NOT NULL
               THEN
                  x_item_number_tbl (i).item_number := l_string;
                  x_item_number_tbl (i).inv_item_id := lx_inv_item_id;
                  x_item_number_tbl (i).inv_org_id :=
                     p_store_loc_tbl (org).inv_org_id;
               END IF;
            END LOOP;
         END IF;
      --END LOOP;
      END IF;
   END Get_parts_List;

   /*************************************************************************
    *   Function/Procedure : Add_Parts_excl
    *
    *   PURPOSE:   This procedure to add the excluded parts from the excluded Item String
    *   Parameter:
    *          IN
    *              p_cyclecount_id -- Cycle Count Id
    *              p_inv_org_id -- Inv Org
    *              p_parts_excl -- Excluded Parts String
    *          OUT
    *              x_return_status -- Return Status
    *              x_error_message -- Error Message
    * ************************************************************************/

   PROCEDURE Add_Parts_excl (p_cyclecount_id          NUMBER,
                             p_inv_org_id      IN     NUMBER,
                             p_parts_excl      IN     VARCHAR2,
                             x_return_status      OUT VARCHAR2,
                             x_error_message      OUT VARCHAR2)
   IS
      l_string           VARCHAR2 (32000);
      l_excl_parts_tbl   excl_parts_tbl;
      l_item_num         VARCHAR2 (200);
      l_comma_pos        NUMBER;
      l_percent_pos      NUMBER;
      lx_inv_item_id     NUMBER;
      l_dash_pos         NUMBER;
      i                  NUMBER;
      l_start_item       mtl_system_items.segment1%TYPE;
      l_end_item         mtl_system_items.segment1%TYPE;

      l_mod_name         VARCHAR2 (100)
                            := 'xxwc_inv_cycle_count_pkg.add_parts_excl';


      CURSOR C1
      IS
         SELECT inventory_item_id, segment1 item_number
           FROM mtl_system_items
          WHERE segment1 LIKE p_parts_excl AND organization_id = p_inv_org_id;

      CURSOR C2
      IS
         SELECT inventory_item_id, segment1 item_number
           FROM mtl_system_items
          WHERE     segment1 BETWEEN l_start_item AND l_end_item
                AND organization_id = p_inv_org_id;
   BEGIN
      --define savepoint
      SAVEPOINT Add_Parts_excl;

      l_string := p_parts_excl;

      --assign out parameter
      x_return_status := 'S';
      x_error_message := '';


      SELECT INSTR (l_string, ',') INTO l_comma_pos FROM DUAL;

      SELECT INSTR (l_string, '%') INTO l_percent_pos FROM DUAL;

      SELECT INSTR (l_string, '-') INTO l_dash_pos FROM DUAL;

      --      Write_Log (g_LEVEL_STATEMENT,
      --                 l_mod_name,
      --                 'l_comma_pos=' || l_comma_pos);
      --      Write_Log (g_LEVEL_STATEMENT,
      --                 l_mod_name,
      --                 'l_percent_pos=' || l_percent_pos);
      --      Write_Log (g_LEVEL_STATEMENT, l_mod_name, ' l_dash_pos=' || l_dash_pos);

      IF l_comma_pos > 0
      THEN
         l_excl_parts_tbl.delete;
         i := 0;

         LOOP
            SELECT INSTR (l_string, ',') INTO l_comma_pos FROM DUAL;

            IF l_comma_pos = 0 AND l_string IS NOT NULL
            THEN
               lx_inv_item_id :=
                  is_item_exists (p_item_number   => l_string,
                                  p_inv_org_id    => p_inv_org_id);


               IF lx_inv_item_id IS NOT NULL
               THEN
                  i := i + 1;
                  l_excl_parts_tbl (i).item_number := l_string;
                  l_excl_parts_tbl (i).inv_item_id := lx_inv_item_id;
               END IF;
            END IF;

            EXIT WHEN l_comma_pos = 0 OR l_string IS NULL;

            SELECT SUBSTR (l_string, 1, l_comma_pos - 1)
              INTO l_item_num
              FROM DUAL;


            lx_inv_item_id :=
               is_item_exists (p_item_number   => l_item_num,
                               p_inv_org_id    => p_inv_org_id);



            IF lx_inv_item_id IS NOT NULL
            THEN
               i := i + 1;
               l_excl_parts_tbl (i).item_number := l_item_num;
               l_excl_parts_tbl (i).inv_item_id := lx_inv_item_id;
            END IF;

            SELECT SUBSTR (l_string, l_comma_pos + 1) INTO l_string FROM DUAL;
         END LOOP;

         FOR j IN l_excl_parts_tbl.FIRST .. l_excl_parts_tbl.LAST
         LOOP
            INSERT INTO xxwc_inv_cc_parts_excl (cyclecount_hdr_id,
                                                inv_org_id,
                                                inv_item_id,
                                                item_number,
                                                last_update_date,
                                                last_updated_by,
                                                creation_date,
                                                created_by,
                                                last_update_login,
                                                object_version_number)
                 VALUES (p_cyclecount_id,
                         p_inv_org_id,
                         l_excl_parts_tbl (j).inv_item_id,
                         l_excl_parts_tbl (j).item_number,
                         SYSDATE,
                         fnd_global.user_id,
                         SYSDATE,
                         fnd_global.user_id,
                         fnd_global.login_id,
                         1);
         END LOOP;
      ELSIF l_percent_pos > 0
      THEN
         FOR k IN C1
         LOOP
            INSERT INTO xxwc_inv_cc_parts_excl (cyclecount_hdr_id,
                                                inv_org_id,
                                                inv_item_id,
                                                item_number,
                                                last_update_date,
                                                last_updated_by,
                                                creation_date,
                                                created_by,
                                                last_update_login,
                                                object_version_number)
                 VALUES (p_cyclecount_id,
                         p_inv_org_id,
                         k.inventory_item_id,
                         k.item_number,
                         SYSDATE,
                         fnd_global.user_id,
                         SYSDATE,
                         fnd_global.user_id,
                         fnd_global.login_id,
                         1);
         END LOOP;
      ELSIF l_dash_pos > 4
      THEN
         --SELECT   INSTR (l_string, '-') INTO l_dash_pos FROM DUAL;

         SELECT SUBSTR (l_string, 1, l_dash_pos - 1)
           INTO l_start_item
           FROM DUAL;

         SELECT SUBSTR (l_string, l_dash_pos + 1) INTO l_end_item FROM DUAL;


         FOR k IN C2
         LOOP
            INSERT INTO xxwc_inv_cc_parts_excl (cyclecount_hdr_id,
                                                inv_org_id,
                                                inv_item_id,
                                                item_number,
                                                last_update_date,
                                                last_updated_by,
                                                creation_date,
                                                created_by,
                                                last_update_login,
                                                object_version_number)
                 VALUES (p_cyclecount_id,
                         p_inv_org_id,
                         k.inventory_item_id,
                         k.item_number,
                         SYSDATE,
                         fnd_global.user_id,
                         SYSDATE,
                         fnd_global.user_id,
                         fnd_global.login_id,
                         1);

            Write_Log (
               g_LEVEL_STATEMENT,
               l_mod_name,
                  'Num of rows inserted ( xxwc_inv_cc_parts_excl) ='
               || SQL%ROWCOUNT);
         END LOOP;
      ELSE
         lx_inv_item_id :=
            is_item_exists (p_item_number   => l_string,
                            p_inv_org_id    => p_inv_org_id);


         IF lx_inv_item_id IS NOT NULL
         THEN
            INSERT INTO xxwc_inv_cc_parts_excl (cyclecount_hdr_id,
                                                inv_org_id,
                                                inv_item_id,
                                                item_number,
                                                last_update_date,
                                                last_updated_by,
                                                creation_date,
                                                created_by,
                                                last_update_login,
                                                object_version_number)
                 VALUES (p_cyclecount_id,
                         p_inv_org_id,
                         lx_inv_item_id,
                         l_string,
                         SYSDATE,
                         fnd_global.user_id,
                         SYSDATE,
                         fnd_global.user_id,
                         fnd_global.login_id,
                         1);
         END IF;
      END IF;
   --COMMIT;
   EXCEPTION
      WHEN OTHERS
      THEN
         ROLLBACK TO Add_Parts_excl;
         x_return_status := 'E';
         x_error_message := SQLERRM;
   END Add_Parts_excl;

   /*************************************************************************
    *   Function/Procedure : Add_Bin_Locations_excl
    *
    *   PURPOSE:   This procedure to add the excluded Bin Locations from the excluded Bin String
    *   Parameter:
    *          IN
    *              p_cyclecount_id -- Cycle Count Id
    *              p_inv_org_id -- Inv Org
    *              p_bin_loc_excl -- Excluded Bin String
    *          OUT
    *              x_return_status -- Return Status
    *              x_error_message -- Error Message
    * ************************************************************************/

   PROCEDURE Add_Bin_Locations_excl (p_cyclecount_id          NUMBER,
                                     p_inv_org_id      IN     NUMBER,
                                     p_bin_loc_excl    IN     VARCHAR2,
                                     x_return_status      OUT VARCHAR2,
                                     x_error_message      OUT VARCHAR2)
   IS
      l_string         VARCHAR2 (32000);
      l_excl_bin_tbl   excl_bin_tbl;
      l_bin            VARCHAR2 (200);
      l_comma_pos      NUMBER;
      l_dash_pos       NUMBER;
      i                NUMBER;
      l_start_bin      mtl_item_locations.segment1%TYPE;
      l_end_bin        mtl_item_locations.segment1%TYPE;

      l_mod_name       VARCHAR2 (100)
         := 'xxwc_inv_cycle_count_pkg.add_bin_locations_excl';

      CURSOR C2
      IS
         SELECT DISTINCT SUBSTR (segment1, 1, 5) bin, subinventory_code
           FROM mtl_item_locations
          WHERE     organization_id = p_inv_org_id
                AND SUBSTR (segment1, 1, 5) BETWEEN l_start_bin AND l_end_bin;
   BEGIN
      --define savepoint
      SAVEPOINT Add_Bin_Locations_excl;
      Write_Log (g_LEVEL_STATEMENT, l_mod_name, 'At Begin');

      l_string := p_bin_loc_excl;

      --assign out parameter
      x_return_status := 'S';
      x_error_message := '';

      SELECT INSTR (l_string, ',') INTO l_comma_pos FROM DUAL;

      SELECT INSTR (l_string, '-', 3) INTO l_dash_pos FROM DUAL;

      Write_Log (g_LEVEL_STATEMENT, l_mod_name, 'l_dash_pos =' || l_dash_pos);

      l_excl_bin_tbl.delete;
      i := 0;

      IF l_comma_pos > 0
      THEN
         LOOP
            SELECT INSTR (l_string, ',') INTO l_comma_pos FROM DUAL;

            IF l_comma_pos = 0 AND l_string IS NOT NULL
            THEN
               i := i + 1;
               l_excl_bin_tbl (i).bin := l_string;
            END IF;

            EXIT WHEN l_comma_pos = 0 OR l_string IS NULL;

            SELECT SUBSTR (l_string, 1, l_comma_pos - 1) INTO l_bin FROM DUAL;

            i := i + 1;
            l_excl_bin_tbl (i).bin := l_bin;

            SELECT SUBSTR (l_string, l_comma_pos + 1) INTO l_string FROM DUAL;
         END LOOP;

         IF l_excl_bin_tbl.COUNT > 0
         THEN
            FOR j IN l_excl_bin_tbl.FIRST .. l_excl_bin_tbl.LAST
            LOOP
               Write_Log (g_LEVEL_STATEMENT,
                          l_mod_name,
                          'Insert xxwc_inv_cc_bin_excl (l_comma_pos )');

               INSERT INTO xxwc_inv_cc_bin_excl (cyclecount_hdr_id,
                                                 inv_org_id,
                                                 inv_location_id,
                                                 bin,
                                                 last_update_date,
                                                 last_updated_by,
                                                 creation_date,
                                                 created_by,
                                                 last_update_login,
                                                 object_version_number)
                    VALUES (p_cyclecount_id,
                            p_inv_org_id,
                            NULL,
                            l_excl_bin_tbl (j).bin,
                            SYSDATE,
                            fnd_global.user_id,
                            SYSDATE,
                            fnd_global.user_id,
                            fnd_global.login_id,
                            1);

               Write_Log (
                  g_LEVEL_STATEMENT,
                  l_mod_name,
                     'Num of rows inserted xxwc_inv_cc_bin_excl:'
                  || SQL%ROWCOUNT);
            END LOOP;
         END IF;
      ELSIF l_dash_pos > 4
      THEN
         --SELECT   INSTR (l_string, '-') INTO l_dash_pos FROM DUAL;

         SELECT SUBSTR (l_string, 1, l_dash_pos - 1)
           INTO l_start_bin
           FROM DUAL;

         SELECT SUBSTR (l_string, l_dash_pos + 1) INTO l_end_bin FROM DUAL;

         Write_Log (g_LEVEL_STATEMENT,
                    l_mod_name,
                    'l_start_bin =' || l_start_bin);
         Write_Log (g_LEVEL_STATEMENT,
                    l_mod_name,
                    'l_end_bin =' || l_end_bin);

         FOR k IN C2
         LOOP
            Write_Log (g_LEVEL_STATEMENT,
                       l_mod_name,
                       'Insert xxwc_inv_cc_bin_excl (l_dash_pos loop)');

            INSERT INTO xxwc_inv_cc_bin_excl (cyclecount_hdr_id,
                                              inv_org_id,
                                              inv_location_id,
                                              bin,
                                              last_update_date,
                                              last_updated_by,
                                              creation_date,
                                              created_by,
                                              last_update_login,
                                              object_version_number)
                 VALUES (p_cyclecount_id,
                         p_inv_org_id,
                         NULL,                      --k.inventory_location_id,
                         k.bin,
                         SYSDATE,
                         fnd_global.user_id,
                         SYSDATE,
                         fnd_global.user_id,
                         fnd_global.login_id,
                         1);

            Write_Log (
               g_LEVEL_STATEMENT,
               l_mod_name,
                  'Num of rows inserted ( xxwc_inv_cc_bin_excl):'
               || SQL%ROWCOUNT);
         END LOOP;
      ELSE
         Write_Log (g_LEVEL_STATEMENT,
                    l_mod_name,
                    'Insert xxwc_inv_cc_bin_excl(Else )');

         INSERT INTO xxwc_inv_cc_bin_excl (cyclecount_hdr_id,
                                           inv_org_id,
                                           inv_location_id,
                                           bin,
                                           last_update_date,
                                           last_updated_by,
                                           creation_date,
                                           created_by,
                                           last_update_login,
                                           object_version_number)
              VALUES (p_cyclecount_id,
                      p_inv_org_id,
                      NULL,
                      SUBSTR (l_string, 1, 5),
                      SYSDATE,
                      fnd_global.user_id,
                      SYSDATE,
                      fnd_global.user_id,
                      fnd_global.login_id,
                      1);

         Write_Log (
            g_LEVEL_STATEMENT,
            l_mod_name,
            'Num of rows inserted ( xxwc_inv_cc_bin_excl):' || SQL%ROWCOUNT);
      END IF;
   EXCEPTION
      WHEN OTHERS
      THEN
         ROLLBACK TO Add_Bin_Locations_excl;
         x_return_status := 'E';
         x_error_message := SQLERRM;
   END Add_Bin_Locations_excl;


   PROCEDURE DELETE_CC_RECORDS (p_cyclecount_hdr_id   IN     NUMBER,
                                x_return_status          OUT VARCHAR2,
                                x_error_message          OUT VARCHAR2)
   IS
      l_mod_name   VARCHAR2 (100)
                      := 'xxwc_inv_cycle_count_pkg.delete_cc_records';
   BEGIN
      SAVEPOINT DELETE_CC_RECORDS;

      x_return_status := 'S';
      x_error_message := '';

      DELETE FROM xxwc_inv_cyclecount_lines
            WHERE cyclecount_hdr_id = p_cyclecount_hdr_id;

      Write_Log (
         g_level_statement,
         l_mod_name,
         'Num of Rows Deleted (xxwc_inv_cyclecount_lines) =' || SQL%ROWCOUNT);

      DELETE FROM xxwc_inv_cyclecount_line_dtls
            WHERE cyclecount_hdr_id = p_cyclecount_hdr_id;

      Write_Log (
         g_level_statement,
         l_mod_name,
            'Num of Rows Deleted (xxwc_inv_cyclecount_line_dtls) ='
         || SQL%ROWCOUNT);

      DELETE FROM xxwc_inv_cyclecount_days a
            WHERE a.cyclecount_hdr_id = p_cyclecount_hdr_id;

      Write_Log (
         g_level_statement,
         l_mod_name,
         'Num of Rows Deleted (xxwc_inv_cyclecount_days) =' || SQL%ROWCOUNT);

      DELETE FROM xxwc_inv_cc_bin_excl
            WHERE cyclecount_hdr_id = p_cyclecount_hdr_id;

      Write_Log (
         g_level_statement,
         l_mod_name,
         'Num of Rows Deleted (xxwc_inv_cc_bin_excl) =' || SQL%ROWCOUNT);

      DELETE FROM xxwc_inv_cc_parts_excl
            WHERE cyclecount_hdr_id = p_cyclecount_hdr_id;

      Write_Log (
         g_level_statement,
         l_mod_name,
         'Num of Rows Deleted (xxwc_inv_cc_parts_excl) =' || SQL%ROWCOUNT);
   EXCEPTION
      WHEN OTHERS
      THEN
         ROLLBACK TO DELETE_CC_RECORDS;
         x_return_status := 'E';
         x_error_message := SQLERRM;
   END DELETE_CC_RECORDS;

   /*************************************************************************
    *   Function/Procedure : Calculate_days_to_count
    *
    *   PURPOSE:   This procedure to get the number of days to cycle count
    *   Parameter:
    *          IN
    *              p_cycle_count -- Cycle Count Name
    *              p_notes -- Notes
    *              p_bin_loc_excl -- Bin Location
    *              p_commit_flag -- Commit Flag
    *              p_inv_org_id -- Inv Org Id
    *              p_start_bin -- Start Bin
    *              p_end_bin -- End Bin
    *              p_schedule_date -- schedule date
    *              p_process_now_flag -- Process Now flag
    *          OUT
    *              x_num_of_days_to_count -- Number of Days to Count
    *              x_cyclecount_hdr_id -- Cycle Count Hdr Id
    *              x_return_status -- Return Status
    *              x_error_message -- Error Message
    * ************************************************************************/

   PROCEDURE Calculate_days_to_count (
      p_cycle_count            IN     VARCHAR2,
      p_notes                  IN     VARCHAR2,
      p_bin_loc_excl           IN     VARCHAR2,
      p_part_num_excl          IN     VARCHAR2,
      p_incl_parts_onhand      IN     VARCHAR2,
      p_mast_parts_excl_flag   IN     VARCHAR2,
      p_called_from            IN     VARCHAR2,
      p_inv_org_id             IN     NUMBER,
      p_start_bin              IN     VARCHAR2,
      p_end_bin                IN     VARCHAR2,
      p_schedule_date          IN     DATE,
      p_process_now_flag       IN     VARCHAR2,
      x_num_of_days_to_count      OUT NUMBER,
      x_cyclecount_hdr_id         OUT NUMBER,
      x_return_status             OUT VARCHAR2,
      x_error_message             OUT VARCHAR2)
   IS
      l_insert_header_flag        VARCHAR2 (1) := 'N';
      l_parts_per_day             NUMBER;
      l_onhand_qty                NUMBER;
      l_excl_count                NUMBER;
      l_cyclecount_line_id        NUMBER;
      l_days_counter              NUMBER;
      l_query_process_date_flag   VARCHAR2 (1);
      lx_days_to_add              NUMBER;
      l_process_date              DATE;
      l_system_date               DATE := SYSDATE;

      l_mod_name                  VARCHAR2 (100)
         := 'xxwc_inv_cycle_count_pkg.calculate_days_to_count';

      VALIDATION_FAILED           EXCEPTION;
      SKIP_RECORD                 EXCEPTION;

      CURSOR C1 (
         p_cyclecount_hdr_id   IN NUMBER)
      IS
           SELECT SUBSTR (mil.segment1, 1, 5) bin,
                  mil.subinventory_code,
                  COUNT (*) parts_per_day
             FROM mtl_system_items_fvl msi,
                  mtl_secondary_locators msl,
                  mtl_item_locations mil
            WHERE     msi.cycle_count_enabled_flag = 'Y'
                  AND msi.inventory_item_id = msl.inventory_item_id
                  AND msi.organization_id = msl.organization_id
                  AND mil.inventory_location_id = msl.secondary_locator
                  AND mil.organization_id = msl.organization_id
                  AND SUBSTR (mil.segment1, 1, 5) BETWEEN p_start_bin
                                                      AND p_end_bin
                  AND mil.organization_id = p_inv_org_id
                  --AND msi.item_type NOT IN ('RENTAL', 'RE-RENTAL', 'REPAIR') --Commented by Ashwin.S on 09-Jan-2018 for TMS#20161014-00158
                --Added the EXIST condition by Ashwin.S on 09-Jan-2018 for TMS#20161014-00158
                AND NOT EXISTS
                (SELECT 1
                 FROM   FND_LOOKUP_VALUES_VL FLV
                WHERE  FLV.LOOKUP_TYPE ='ITEM_TYPE'
                AND    NVL(FLV.ATTRIBUTE_CATEGORY,'XXWC_ITEM_TYPE_RETIREMENT')='XXWC_ITEM_TYPE_RETIREMENT'
                AND    FLV.LOOKUP_CODE=ITEM_TYPE
                AND    NVL(flv.ATTRIBUTE2,'N')='Y')
                  AND msi.inventory_item_status_code = 'Active'
                  AND NOT EXISTS
                         (SELECT '1'
                            FROM xxwc_inv_cc_bin_excl bin_excl
                           WHERE     bin_excl.cyclecount_hdr_id =
                                        p_cyclecount_hdr_id
                                 AND bin_excl.bin = SUBSTR (mil.segment1, 1, 5))
                  AND NOT EXISTS
                         (SELECT '1'
                            FROM xxwc_inv_cc_parts_excl pts_excl
                           WHERE     pts_excl.cyclecount_hdr_id =
                                        p_cyclecount_hdr_id
                                 AND pts_excl.inv_org_id = msi.organization_id
                                 AND pts_excl.inv_item_id =
                                        msi.inventory_item_id)
         GROUP BY SUBSTR (mil.segment1, 1, 5), mil.subinventory_code;

      CURSOR c2 (
         p_bin           IN VARCHAR2,
         p_inv_org_id    IN NUMBER,
         p_subinv_code   IN VARCHAR2)
      IS
         SELECT msl.inventory_item_id
           FROM mtl_secondary_locators msl, mtl_item_locations mil
          WHERE     mil.inventory_location_id = msl.secondary_locator
                AND mil.organization_id = msl.organization_id
                AND SUBSTR (mil.segment1, 1, 5) = p_bin
                AND msl.organization_id = p_inv_org_id
                AND msl.subinventory_code = p_subinv_code;
   BEGIN
      --Define savepoint
      SAVEPOINT Calculate_days_to_count;

      x_return_status := 'S';
      x_error_message := '';

      Write_Log (g_LEVEL_STATEMENT,
                 l_mod_name,
                 'Checking if the Cycle Count Header exists');

      BEGIN
         SELECT cyclecount_hdr_id
           INTO x_cyclecount_hdr_id
           FROM xxwc_inv_cyclecount_hdr
          WHERE cycle_count_name = p_cycle_count;

         --Set the Insert Flag to 'N'
         l_insert_header_flag := 'N';
         Write_Log (
            g_LEVEL_STATEMENT,
            l_mod_name,
               'SUCCESS: Found Cycle Count Header -p_cycle_count ='
            || p_cycle_count);
      EXCEPTION
         WHEN NO_DATA_FOUND
         THEN
            --Set the Insert Flag to 'Y'
            l_insert_header_flag := 'Y';
            Write_Log (
               g_LEVEL_STATEMENT,
               l_mod_name,
                  'EXCEPTION: No Cycle Count Header found for -p_cycle_count ='
               || p_cycle_count);
         WHEN OTHERS
         THEN
            x_error_message := SQLERRM;
            RAISE VALIDATION_FAILED;
      END;

      --If cycle count exists then skip it
      IF l_insert_header_flag = 'Y'
      THEN
         --Generate the cyclecount_hdr_id
         SELECT xxwc_inv_cyclecount_hdr_s.NEXTVAL
           INTO x_cyclecount_hdr_id
           FROM DUAL;

         --Insert cyclecount header
         INSERT INTO xxwc_inv_cyclecount_hdr (cyclecount_hdr_id,
                                              cycle_count_name,
                                              notes,
                                              process_type,
                                              process_date,
                                              process_now_flag,
                                              process_status,
                                              last_update_date,
                                              last_updated_by,
                                              creation_date,
                                              created_by,
                                              last_update_login,
                                              object_version_number)
              VALUES (x_cyclecount_hdr_id,
                      p_cycle_count,
                      p_notes,
                      'AUTOMATICBIN',
                      p_schedule_date,
                      p_process_now_flag,
                      'ACTIVE',
                      SYSDATE,
                      FND_GLOBAL.USER_ID,
                      SYSDATE,
                      fnd_global.user_id,
                      fnd_global.login_id,
                      1);

         Write_Log (
            g_level_statement,
            l_mod_name,
               'Num of Rows Inserted (xxwc_inv_cyclecount_hdr) ='
            || SQL%ROWCOUNT);
      ELSE
         UPDATE xxwc_inv_cyclecount_hdr
            SET notes = p_notes,
                process_date = p_schedule_date,
                last_update_date = SYSDATE,
                last_updated_by = fnd_global.user_id,
                last_update_login = fnd_global.login_id,
                object_version_number = object_version_number + 1
          WHERE cyclecount_hdr_id = x_cyclecount_hdr_id;

         Write_Log (
            g_level_statement,
            l_mod_name,
            'Num of Rows Updated (xxwc_inv_cyclecount_hdr) =' || SQL%ROWCOUNT);
      END IF;

      --Call the api to delete the records
      --for the automatic bin selection
      DELETE_CC_RECORDS (p_cyclecount_hdr_id   => x_cyclecount_hdr_id,
                         x_return_status       => x_return_status,
                         x_error_message       => x_error_message);

      Write_Log (g_level_statement,
                 l_mod_name,
                 'x_return_status (delete_cc_records) =' || x_return_status);

      IF x_return_status <> 'S'
      THEN
         RAISE VALIDATION_FAILED;
      END IF;

      IF p_bin_loc_excl IS NOT NULL
      THEN
         Write_Log (g_level_statement,
                    l_mod_name,
                    'Calling Add_Bin_Locations_excl');
         Add_Bin_Locations_excl (p_cyclecount_id   => x_cyclecount_hdr_id,
                                 p_inv_org_id      => p_inv_org_id,
                                 p_bin_loc_excl    => p_bin_loc_excl,
                                 x_return_status   => x_return_status,
                                 x_error_message   => x_error_message);
         Write_Log (
            g_level_statement,
            l_mod_name,
            'x_return_status (Add_Bin_Locations_excl) =' || x_return_status);

         IF x_return_status <> 'S'
         THEN
            RAISE VALIDATION_FAILED;
         END IF;
      END IF;

      --If parts excluded is not null then
      --populate the excluded parts
      IF p_part_num_excl IS NOT NULL
      THEN
         Write_Log (g_level_statement, l_mod_name, 'calling add_parts_excl');
         Add_parts_excl (p_cyclecount_id   => x_cyclecount_hdr_id,
                         p_inv_org_id      => p_inv_org_id,
                         p_parts_excl      => p_part_num_excl,
                         x_return_status   => x_return_status,
                         x_error_message   => x_error_message);

         Write_Log (g_level_statement,
                    l_mod_name,
                    'x_return_status (add_parts_excl)=' || x_return_status);

         IF x_return_status <> 'S'
         THEN
            RAISE VALIDATION_FAILED;
         END IF;
      END IF;

      IF p_called_from = 'API'
      THEN
         --Generate the cyclecount line id
         SELECT xxwc_inv_cyclecount_lines_s.NEXTVAL
           INTO l_cyclecount_line_id
           FROM DUAL;

         --Insert into lines table
         INSERT INTO xxwc_inv_cyclecount_lines (cyclecount_line_id,
                                                cyclecount_hdr_id,
                                                inv_org_id,
                                                process_status,
                                                last_update_date,
                                                last_updated_by,
                                                creation_date,
                                                created_by,
                                                last_update_login,
                                                object_version_number)
              VALUES (l_cyclecount_line_id,
                      x_cyclecount_hdr_id,
                      p_inv_org_id,
                      'ACTIVE',
                      SYSDATE,
                      FND_GLOBAL.USER_ID,
                      SYSDATE,
                      fnd_global.user_id,
                      fnd_global.login_id,
                      1);

         Write_Log (
            g_level_statement,
            l_mod_name,
               'num of rows inserted(xxwc_inv_cyclecount_lines)='
            || SQL%ROWCOUNT);
      END IF;

      x_num_of_days_to_count := 0;
      lx_days_to_add := 0;


      --Insert into the xxwc_inv_cyclecount_days
      --for each combination of subinventory,bin
      FOR i IN C1 (x_cyclecount_hdr_id)
      LOOP
         Write_Log (
            g_LEVEL_STATEMENT,
            l_mod_name,
               '******Bin Location ='
            || i.bin
            || ' ******Parts Per Day ='
            || i.parts_per_day
            || '****');
         --Set the flag to Y so that query for
         -- each bay bin location once instead of each item
         l_query_process_date_flag := 'Y';
         l_parts_per_day := 0;

         FOR k IN C2 (i.bin, p_inv_org_id, i.subinventory_code)
         LOOP
            BEGIN
               Write_Log (g_level_statement,
                          l_mod_name,
                          '---------------------------------------------');
               Write_Log (g_level_statement,
                          l_mod_name,
                          'k.inventory_item_id=' || k.inventory_item_id);

               --check for onhand qty and skip the record
               --if onhand is not available
               IF p_incl_parts_onhand = 'Y'
               THEN
                  SELECT SUM (NVL (transaction_quantity, 0))
                    INTO l_onhand_qty
                    FROM mtl_onhand_quantities
                   WHERE     organization_id = p_inv_org_id
                         AND subinventory_code = i.subinventory_code
                         AND inventory_item_id = k.inventory_item_id;

                  Write_Log (g_level_statement,
                             l_mod_name,
                             'l_onhand_qty=' || l_onhand_qty);

                  IF NVL (l_onhand_qty, 0) <= 0
                  THEN
                     RAISE SKIP_RECORD;
                  END IF;
               END IF;

               --check if the excl master part number is checked
               --then exclude those items
               IF p_mast_parts_excl_flag = 'Y'
               THEN
                  SELECT COUNT (*)
                    INTO l_excl_count
                    FROM xxwc_mstr_parts_excl_list
                   WHERE item_id = k.inventory_item_id;

                  Write_Log (g_level_statement,
                             l_mod_name,
                             'l_excl_count=' || l_excl_count);


                  IF NVL (l_excl_count, 0) > 0
                  THEN
                     RAISE SKIP_RECORD;
                  END IF;
               END IF;

               --Run this query only when it meets criteria to
               --insert the records
               IF l_query_process_date_flag = 'Y'
               THEN
                  Write_Log (g_level_statement,
                             l_mod_name,
                             'lx_days_to_add=' || lx_days_to_add);

                  IF p_process_now_flag = 'Y'
                  THEN
                     IF lx_days_to_add > 0
                     THEN
                        l_process_date :=
                             x_num_of_days_to_count
                           + l_system_date
                           + lx_days_to_add;
                     ELSE
                        l_process_date :=
                           x_num_of_days_to_count + l_system_date;
                     END IF;
                  ELSE
                     IF lx_days_to_add > 0
                     THEN
                        l_process_date :=
                             x_num_of_days_to_count
                           + p_schedule_date
                           + lx_days_to_add;
                     ELSE
                        l_process_date :=
                           x_num_of_days_to_count + p_schedule_date;
                     END IF;
                  END IF;

                  Write_Log (
                     g_level_statement,
                     l_mod_name,
                        'l_process_date(before block out date calculation)='
                     || TO_CHAR (l_process_date, 'DD-MON-YYYY HH12:MI:SS AM'));

                  --call the procedure to check if it weekend or
                  --blocked out for the store location
                  Get_Process_Date (p_inv_org_id      => p_inv_org_id,
                                    px_process_date   => l_process_date,
                                    px_days_to_add    => lx_days_to_add);

                  Write_Log (
                     g_level_statement,
                     l_mod_name,
                        'l_process_date(after block out date calculation)='
                     || TO_CHAR (l_process_date, 'DD-MON-YYYY HH12:MI:SS AM'));
               END IF;

               l_query_process_date_flag := 'N';

               IF p_called_from = 'API'
               THEN
                  -- Insert into details table
                  INSERT
                    INTO xxwc_inv_cyclecount_line_dtls (cyclecount_line_dtl_id,
                                                        cyclecount_line_id,
                                                        cyclecount_hdr_id,
                                                        inv_org_id,
                                                        subinventory_code,
                                                        inv_location_id,
                                                        inv_item_id,
                                                        category_id,
                                                        process_status,
                                                        process_date,
                                                        last_update_date,
                                                        last_updated_by,
                                                        creation_date,
                                                        created_by,
                                                        last_update_login,
                                                        object_version_number,
                                                        bin)
                  VALUES (xxwc_inv_cyclecount_line_dtl_s.NEXTVAL,
                          l_cyclecount_line_id,
                          x_cyclecount_hdr_id,
                          p_inv_org_id,
                          i.subinventory_code,
                          NULL,
                          k.inventory_item_id,
                          NULL,
                          'ACTIVE',
                          l_process_date,
                          SYSDATE,
                          FND_GLOBAL.USER_ID,
                          SYSDATE,
                          fnd_global.user_id,
                          fnd_global.login_id,
                          1,
                          i.bin);

                  Write_Log (
                     g_level_statement,
                     l_mod_name,
                        'num of rows inserted (xxwc_inv_cyclecount_line_dtls)='
                     || SQL%ROWCOUNT);
               END IF;

               --calculate the parts to count per day
               l_parts_per_day := l_parts_per_day + 1;
            EXCEPTION
               WHEN SKIP_RECORD
               THEN
                  Write_Log (g_level_statement,
                             l_mod_name,
                             'Skipping the records...');
            END;
         END LOOP;

         Write_Log (g_level_statement,
                    l_mod_name,
                    'l_parts_per_day =' || l_parts_per_day);

         IF NVL (l_parts_per_day, 0) > 0
         THEN
            --Increment only if there are parts qualify
            x_num_of_days_to_count := x_num_of_days_to_count + 1;

            INSERT INTO xxwc_inv_cyclecount_days (cyclecount_hdr_id,
                                                  inv_org_id,
                                                  inv_location_id,
                                                  bin,
                                                  subinventory_code,
                                                  day_sequence,
                                                  parts_per_day,
                                                  last_update_date,
                                                  last_updated_by,
                                                  creation_date,
                                                  created_by,
                                                  last_update_login,
                                                  object_version_number)
                 VALUES (x_cyclecount_hdr_id,
                         p_inv_org_id,
                         NULL,
                         i.bin,
                         i.subinventory_code,
                         x_num_of_days_to_count,
                         l_parts_per_day,
                         SYSDATE,
                         fnd_global.user_id,
                         SYSDATE,
                         fnd_global.user_id,
                         fnd_global.login_id,
                         1);
         END IF;

         Write_Log (
            g_LEVEL_STATEMENT,
            l_mod_name,
               'Num of records inserted (xxwc_inv_cyclecount_days)='
            || SQL%ROWCOUNT);
      END LOOP;

      IF p_called_from = 'FORM'
      THEN
         COMMIT;
      END IF;
   EXCEPTION
      WHEN VALIDATION_FAILED
      THEN
         ROLLBACK TO Calculate_days_to_count;
         x_return_status := 'E';
         Write_Log (g_LEVEL_STATEMENT,
                    l_mod_name,
                    'EXCEPTION(VALIDATION_FAILED): ' || x_error_message);
      WHEN OTHERS
      THEN
         ROLLBACK TO Calculate_days_to_count;
         x_return_status := 'E';
         x_error_message := SQLERRM;
         Write_Log (g_LEVEL_STATEMENT,
                    l_mod_name,
                    'EXCEPTION(OTHERS): ' || x_error_message);
   END Calculate_days_to_count;

   /*************************************************************************
    *   Function/Procedure : automatic_scheduler
    *
    *   PURPOSE:   This procedure to launch the scheduler for the automatic bin selection type
    *   Parameter:
    *          IN
    *              p_inv_org_id -- Inv Org
    *          OUT
    *              x_return_status -- Return Status
    *              x_error_message -- Error Message
    * ************************************************************************/
   PROCEDURE automatic_scheduler (p_inv_org_id      IN     NUMBER,
                                  x_return_status      OUT VARCHAR2,
                                  x_error_message      OUT VARCHAR2)
   IS
      l_cycle_count_schedule_id   NUMBER;
      l_cycle_count_header_id     NUMBER;
      l_req_id                    NUMBER;
      l_zero_count_flag           NUMBER;
      l_error_msg                 VARCHAR2 (1000);
      --      l_count                     NUMBER;
      l_cyclecount_tbl            cyclecount_tbl;
      l_auto_count                NUMBER;
      l_pending_count             NUMBER;

      l_mod_name                  VARCHAR2 (100)
         := 'xxwc_inv_cycle_count_pkg.automatic_scheduler';

      VALIDATION_FAILED           EXCEPTION;
      CONC_REQ_FAILED             EXCEPTION;

      CURSOR C_auto_org
      IS
         SELECT DISTINCT a.cyclecount_hdr_id,
                         a.cyclecount_line_id,
                         a.inv_org_id,
                         b.cycle_count_name
           FROM xxwc_inv_cyclecount_lines a, xxwc_inv_cyclecount_hdr b
          WHERE     a.cyclecount_hdr_id = b.cyclecount_hdr_id
                AND b.process_status = 'ACTIVE'
                AND a.process_status = 'ACTIVE'
                --AND trunc(a.process_date) <= trunc(SYSDATE)
                AND DECODE (p_inv_org_id, NULL, -999, a.inv_org_id) =
                       NVL (p_inv_org_id, -999);

      CURSOR C_auto_dtls (
         p_cyclecount_line_id   IN NUMBER,
         p_org_id               IN NUMBER)
      IS
         SELECT ROWID row_id, dtls.*
           FROM xxwc_inv_cyclecount_line_dtls dtls
          WHERE     cyclecount_line_id = p_cyclecount_line_id
                AND inv_org_id = p_org_id
                AND process_status = 'ACTIVE'
                AND SUBINVENTORY_CODE !='GeneralPCK'  --Added by Ashwin.S on 10-Jan-2018 for TMS#20161014-00158
                AND process_date <= SYSDATE;

      CURSOR C_bin (
         p_bin      IN VARCHAR2,
         p_org_id   IN NUMBER)
      IS
         SELECT *
           FROM mtl_item_locations mil
          WHERE     organization_id = p_org_id
                AND SUBSTR (segment1, 1, 5) = p_bin
                AND SUBINVENTORY_CODE !='GeneralPCK'; --Added by Ashwin.S on 10-Jan-2018 for TMS#20161014-00158
   BEGIN
      --assign out parameter
      x_return_status := 'S';
      x_error_message := '';

      Write_Log (g_LEVEL_STATEMENT,
                 l_mod_name,
                 '***********Automatic_Scheduler ******************');
      Write_Log (g_LEVEL_STATEMENT,
                 l_mod_name,
                 ' p_inv_org_id =' || p_inv_org_id);

      --Logic starts for automatic bin Type
      FOR i IN C_auto_org
      LOOP
         BEGIN
            --Define savepoint
            --SAVEPOINT automatic_scheduler;

            Write_Log (
               g_LEVEL_STATEMENT,
               l_mod_name,
               '***********Location FOR Loop (automatic bin Type) ************');
            Write_Log (g_LEVEL_STATEMENT,
                       l_mod_name,
                       ' i.inv_org_id =' || i.inv_org_id);

            BEGIN
               SELECT NVL (zero_count_flag, 2), cycle_count_header_id
                 INTO l_zero_count_flag, l_cycle_count_header_id
                 FROM mtl_cycle_count_headers
                WHERE     organization_id = i.inv_org_id
                      AND abc_assignment_group_id IS NOT NULL;
            EXCEPTION
               WHEN NO_DATA_FOUND
               THEN
                  l_error_msg :=
                        'No Cycle count defined for the inv Org :'
                     || i.inv_org_id;
                  RAISE VALIDATION_FAILED;
               WHEN OTHERS
               THEN
                  l_error_msg := 'Unknown Error Msg :' || SQLERRM;
                  RAISE VALIDATION_FAILED;
            END;

            Write_Log (g_LEVEL_STATEMENT,
                       l_mod_name,
                       'Zero Count Flag: ' || l_zero_count_flag);

            Write_Log (
               g_LEVEL_STATEMENT,
               l_mod_name,
               'l_cycle_count_header_id: ' || l_cycle_count_header_id);
            l_auto_count := 0;

            FOR j IN c_auto_dtls (i.cyclecount_line_id, i.inv_org_id)
            LOOP
               Write_Log (g_LEVEL_STATEMENT,
                          l_mod_name,
                          '********* j.bin =' || j.bin || '********');

               FOR k IN C_bin (j.bin, i.inv_org_id)
               LOOP
                  Write_Log (
                     g_LEVEL_STATEMENT,
                     l_mod_name,
                        '****** k.inventory_location_id ='
                     || k.inventory_location_id
                     || '*****');

                  -- Get a new cycle count schedule ID
                  SELECT mtl_cc_schedule_requests_s.NEXTVAL
                    INTO l_cycle_count_schedule_id
                    FROM DUAL;

                  INSERT
                    INTO mtl_cc_schedule_requests (cycle_count_schedule_id,
                                                   last_update_date,
                                                   last_updated_by,
                                                   creation_date,
                                                   created_by,
                                                   cycle_count_header_id,
                                                   request_source_type,
                                                   zero_count_flag,
                                                   schedule_date,
                                                   schedule_status,
                                                   subinventory,
                                                   locator_id,
                                                   inventory_item_id)
                  VALUES (l_cycle_count_schedule_id,
                          SYSDATE,
                          FND_GLOBAL.USER_ID,
                          SYSDATE,
                          FND_GLOBAL.USER_ID,
                          l_cycle_count_header_id,
                          2,
                          l_zero_count_flag,
                          j.process_date,
                          1,
                          k.subinventory_code,
                          k.inventory_location_id,
                          j.inv_item_id);

                  --Increment the autocount
                  l_auto_count := l_auto_count + 1;
               END LOOP;                                            --bin Loop

               UPDATE xxwc_inv_cyclecount_line_dtls
                  SET process_status = 'COMPLETE'
                WHERE cyclecount_line_dtl_id = j.cyclecount_line_dtl_id;
            END LOOP;                                          --dtls For Loop

            SELECT COUNT (*)
              INTO l_pending_count
              FROM xxwc_inv_cyclecount_line_dtls dtls
             WHERE     process_status != 'COMPLETE'
                   AND cyclecount_hdr_id = i.cyclecount_hdr_id;

            Write_Log (g_LEVEL_STATEMENT,
                       l_mod_name,
                       'l_pending_count =' || l_pending_count);

            IF NVL (l_pending_count, 0) = 0
            THEN
               UPDATE xxwc_inv_cyclecount_lines a
                  SET process_status = 'COMPLETE'
                WHERE cyclecount_hdr_id = i.cyclecount_hdr_id;

               UPDATE xxwc_inv_cyclecount_hdr b
                  SET process_status = 'COMPLETE'
                WHERE cyclecount_hdr_id = i.cyclecount_hdr_id;
            END IF;

            COMMIT;

            Write_Log (
               g_LEVEL_STATEMENT,
               l_mod_name,
                  'Num of Rows Inserted:(mtl_cc_schedule_requests) ='
               || l_auto_count);

            IF l_auto_count > 0
            THEN
               Write_Log (
                  g_LEVEL_STATEMENT,
                  l_mod_name,
                  'Calling Generate Cycle Count requests concurrent program');
               -- Call the concurrent program to generate count requests
               l_req_id :=
                  fnd_request.submit_request (
                     application   => 'INV',
                     program       => 'INCACG',
                     argument1     => '2',
                     argument2     => TO_CHAR (l_cycle_count_header_id),
                     argument3     => TO_CHAR (i.inv_org_id));
               Write_Log (g_LEVEL_STATEMENT,
                          l_mod_name,
                          ' l_req_id :' || l_req_id);

               -- Check for errors
               IF (l_req_id <= 0 OR l_req_id IS NULL)
               THEN
                  l_error_msg :=
                     'Error in calling the generate count requests program';
                  RAISE CONC_REQ_FAILED;
               ELSE
                  COMMIT;
               END IF;
            END IF;
         EXCEPTION
            WHEN CONC_REQ_FAILED
            THEN
               Write_Log (g_LEVEL_STATEMENT, l_mod_name, l_error_msg);
            WHEN VALIDATION_FAILED
            THEN
               Write_Log (g_LEVEL_STATEMENT, l_mod_name, l_error_msg);
         END;
      END LOOP;

      COMMIT;
   EXCEPTION
      WHEN OTHERS
      THEN
         x_error_message := ' Unknown error Msg :' || SQLERRM;
         Write_Log (g_LEVEL_STATEMENT, l_mod_name, x_error_message);
         x_return_status := 'E';
   END automatic_scheduler;

   /*************************************************************************
    *   Function/Procedure : Process_automatic
    *
    *   PURPOSE:   This procedure to process the automatic selection type
    *   Parameter:
    *          IN
    *              p_cycle_count -- Cycle Coutn Name
    *              p_notes -- Notes
    *              p_inv_org_id -- Inv Org/Locations
    *              p_start_bin -- Start Bin
    *              p_end_bin -- End Bin
    *              p_part_num_excl -- Excluded Part Num
    *              p_bin_loc_excl -- Excluded Bin Loc
    *              p_mast_parts_excl_flag -- Include Master parts Exclusion
    *              p_incl_parts_onhand -- Include On Hand items Only Flag
    *              p_schedule_date -- Schedule Date
    *              p_process_now_flag -- Process Now Flag
    *          OUT
    *              x_return_status -- Return Status
    *              x_error_message -- Error Message
    * ************************************************************************/
   PROCEDURE Process_automatic (p_cycle_count            IN     VARCHAR2,
                                p_notes                  IN     VARCHAR2,
                                p_inv_org_id             IN     NUMBER,
                                p_start_bin              IN     VARCHAR2,
                                p_end_bin                IN     VARCHAR2,
                                p_part_num_excl          IN     VARCHAR2,
                                p_bin_loc_excl           IN     VARCHAR2,
                                p_mast_parts_excl_flag   IN     VARCHAR2,
                                p_incl_parts_onhand      IN     VARCHAR2,
                                p_no_specials            IN     VARCHAR2,
                                p_schedule_date          IN     DATE,
                                p_process_now_flag       IN     VARCHAR2,
                                x_num_of_days_to_count      OUT NUMBER,
                                x_return_status             OUT VARCHAR2,
                                x_error_message             OUT VARCHAR2)
   IS
      l_cyclecount_hdr_id   NUMBER;
      l_mod_name            VARCHAR2 (100)
         := 'xxwc_inv_cycle_count_pkg.process_automatic';

      API_FAILURE           EXCEPTION;
   BEGIN
      --Define Savepoint
      SAVEPOINT Process_automatic;

      -- Initialize the Out parameter
      x_return_status := 'S';
      x_error_message := NULL;
      --Output the In Parameters
      Write_Log (g_level_statement,
                 l_mod_name,
                 '++++++++++++++In Parameters+++++++++++++');
      Write_Log (g_level_statement,
                 l_mod_name,
                 'p_cycle_count =' || p_cycle_count);
      Write_Log (g_level_statement, l_mod_name, 'p_notes =' || p_notes);
      Write_Log (g_level_statement,
                 l_mod_name,
                 'p_inv_org_id =' || p_inv_org_id);
      Write_Log (g_level_statement,
                 l_mod_name,
                 'p_start_bin =' || p_start_bin);
      Write_Log (g_level_statement, l_mod_name, 'p_end_bin =' || p_end_bin);
      Write_Log (g_level_statement,
                 l_mod_name,
                 'p_part_num_excl =' || p_part_num_excl);
      Write_Log (g_level_statement,
                 l_mod_name,
                 'p_bin_loc_excl =' || p_bin_loc_excl);
      Write_Log (g_level_statement,
                 l_mod_name,
                 'p_mast_parts_excl_flag =' || p_mast_parts_excl_flag);
      Write_Log (g_level_statement,
                 l_mod_name,
                 'p_incl_parts_onhand =' || p_incl_parts_onhand);
      Write_Log (g_level_statement,
                 l_mod_name,
                 'p_no_specials =' || p_no_specials);
      Write_Log (
         g_level_statement,
         l_mod_name,
            'p_schedule_date ='
         || TO_CHAR (p_schedule_date, 'DD-MON-YYYY HH12:MI:SS AM'));
      Write_Log (g_level_statement,
                 l_mod_name,
                 'p_process_now_flag =' || p_process_now_flag);
      Write_Log (g_level_statement,
                 l_mod_name,
                 '+++++++++++++++++++++++++++++++++++');

      Write_Log (g_level_statement,
                 l_mod_name,
                 'calling Calculate_days_to_count');
      -- Call the Calculate_days_to_count to process
      -- insert the records to staging table
      CALCULATE_DAYS_TO_COUNT (
         p_cycle_count            => p_cycle_count,
         p_notes                  => p_notes,
         p_bin_loc_excl           => p_bin_loc_excl,
         p_part_num_excl          => p_part_num_excl,
         p_incl_parts_onhand      => p_incl_parts_onhand,
         p_mast_parts_excl_flag   => p_mast_parts_excl_flag,
         p_called_from            => 'API',
         p_inv_org_id             => p_inv_org_id,
         p_start_bin              => p_start_bin,
         p_end_bin                => p_end_bin,
         p_schedule_date          => p_schedule_date,
         p_process_now_flag       => p_process_now_flag,
         x_num_of_days_to_count   => x_num_of_days_to_count,
         x_cyclecount_hdr_id      => l_cyclecount_hdr_id,
         x_return_status          => x_return_status,
         x_error_message          => x_error_message);

      Write_Log (
         g_level_statement,
         l_mod_name,
         'x_return_status (Calculate_days_to_count)=' || x_return_status);
      Write_Log (g_level_statement,
                 l_mod_name,
                 'l_cyclecount_hdr_id=' || l_cyclecount_hdr_id);

      Write_Log (g_level_statement,
                 l_mod_name,
                 'x_num_of_days_to_count=' || x_num_of_days_to_count);

      IF x_return_status <> 'S'
      THEN
         RAISE API_FAILURE;
      ELSE
         COMMIT;
      END IF;


      Write_Log (g_level_statement,
                 l_mod_name,
                 'p_process_now_flag=' || p_process_now_flag);

      IF p_process_now_flag = 'Y'
      THEN
         Write_Log (g_level_statement,
                    l_mod_name,
                    'calling automatic_scheduler');

         automatic_scheduler (p_inv_org_id      => p_inv_org_id,
                              x_return_status   => x_return_status,
                              x_error_message   => x_error_message);
         Write_Log (
            g_level_statement,
            l_mod_name,
            'x_return_status (automatic_scheduler)=' || x_return_status);
      END IF;

      COMMIT;
   EXCEPTION
      WHEN API_FAILURE
      THEN
         ROLLBACK TO Process_automatic;
         x_return_status := 'E';
         Write_Log (g_level_statement,
                    l_mod_name,
                    'error_msg )=' || x_error_message);
      WHEN OTHERS
      THEN
         ROLLBACK TO Process_automatic;
         x_return_status := 'E';
         x_error_message := SQLERRM;
         Write_Log (g_level_statement,
                    l_mod_name,
                    'error_msg )=' || x_error_message);
   END Process_automatic;

   /*************************************************************************
    *   Function/Procedure : specific_Scheduler
    *
    *   PURPOSE:   This procedure to launch the scheduler for the specific selection type
    *   Parameter:
    *          IN
    *              p_inv_org_id -- Inv Org
    *          OUT
    *              x_return_status -- Return Status
    *              x_error_message -- Error Message
    * ************************************************************************/

   PROCEDURE specific_scheduler (p_inv_org_id      IN     NUMBER,
                                 x_return_status      OUT VARCHAR2,
                                 x_error_message      OUT VARCHAR2)
   IS
      l_cycle_count_schedule_id   NUMBER;
      l_cycle_count_header_id     NUMBER;
      l_req_id                    NUMBER;
      l_zero_count_flag           NUMBER;
      l_error_msg                 VARCHAR2 (1000);
      l_count                     NUMBER;
      l_cyclecount_tbl            cyclecount_tbl;

      l_gen_subinv                VARCHAR2 (100)
         := fnd_profile.VALUE ('XXWC_INV_GENERAL_SUBINV');

      l_mod_name                  VARCHAR2 (100)
         := 'xxwc_inv_cycle_count_pkg.specific_scheduler';

      VALIDATION_FAILED           EXCEPTION;


      CURSOR c_inv_org
      IS
         SELECT DISTINCT
                lines.inv_org_id, hdr.cycle_count_name, hdr.cyclecount_hdr_id
           FROM xxwc_inv_cc_specific_lines lines, xxwc_inv_cyclecount_hdr hdr
          WHERE     lines.cyclecount_hdr_id = hdr.cyclecount_hdr_id
                AND lines.process_status != 'COMPLETE'
                AND lines.process_date <= SYSDATE
                AND DECODE (p_inv_org_id, NULL, -999, lines.inv_org_id) =
                       NVL (p_inv_org_id, -999);

      CURSOR c_item (
         p_cyclecount_hdr_id   IN NUMBER,
         p_organization_id     IN NUMBER)
      IS
         SELECT lines.*
           FROM xxwc_inv_cc_specific_lines lines, mtl_system_items_fvl msi
          WHERE     inv_item_id IS NOT NULL
                AND process_status != 'COMPLETE'
                AND lines.inv_item_id = msi.inventory_item_id
                AND lines.inv_org_id = msi.organization_id
                AND msi.cycle_count_enabled_flag = 'Y'
                --AND msi.item_type NOT IN ('RENTAL', 'RE-RENTAL', 'REPAIR') --Commented by Ashwin.S on 09-Jan-2018 for TMS#20161014-00158
                --Added the EXIST condition by Ashwin.S on 09-Jan-2018 for TMS#20161014-00158
                AND NOT EXISTS
                (SELECT 1
                 FROM   FND_LOOKUP_VALUES_VL FLV
                WHERE  FLV.LOOKUP_TYPE ='ITEM_TYPE'
                AND    NVL(FLV.ATTRIBUTE_CATEGORY,'XXWC_ITEM_TYPE_RETIREMENT')='XXWC_ITEM_TYPE_RETIREMENT'
                AND    FLV.LOOKUP_CODE=ITEM_TYPE
                AND    NVL(flv.ATTRIBUTE2,'N')='Y')
                AND msi.inventory_item_status_code = 'Active'
                AND lines.cyclecount_hdr_id = p_cyclecount_hdr_id
                AND lines.inv_org_id = p_organization_id;


      CURSOR c_category (
         p_cyclecount_hdr_id   IN NUMBER)
      IS
         SELECT *
           FROM xxwc_inv_cc_specific_lines lines
          WHERE     category_id IS NOT NULL
                AND process_status != 'COMPLETE'
                AND lines.cyclecount_hdr_id = p_cyclecount_hdr_id;

      CURSOR c_cat_items (
         p_category_id    NUMBER,
         p_inv_org_id     NUMBER)
      IS
         SELECT cat.inventory_item_id
           FROM mtl_item_categories_v cat, mtl_system_items_fvl msi
          WHERE     cat.category_set_name = 'Inventory Category'
                AND cat.organization_id = p_inv_org_id
                AND cat.inventory_item_id = msi.inventory_item_id
                AND cat.organization_id = msi.organization_id
                AND msi.cycle_count_enabled_flag = 'Y'
                --AND msi.item_type NOT IN ('RENTAL', 'RE-RENTAL', 'REPAIR') --Commented by Ashwin.S on 09-Jan-2018 for TMS#20161014-00158
                --Added the EXIST condition by Ashwin.S on 09-Jan-2018 for TMS#20161014-00158
                AND NOT EXISTS
                (SELECT 1
                 FROM   FND_LOOKUP_VALUES_VL FLV
                WHERE  FLV.LOOKUP_TYPE ='ITEM_TYPE'
                AND    NVL(FLV.ATTRIBUTE_CATEGORY,'XXWC_ITEM_TYPE_RETIREMENT')='XXWC_ITEM_TYPE_RETIREMENT'
                AND    FLV.LOOKUP_CODE=ITEM_TYPE
                AND    NVL(flv.ATTRIBUTE2,'N')='Y')
                AND msi.inventory_item_status_code = 'Active'
                AND category_id = p_category_id;

      CURSOR c_onhand (
         p_org_id     NUMBER,
         p_item_id    NUMBER)
      IS
           SELECT moq.inventory_item_id,
                  moq.organization_id,
                  moq.subinventory_code,
                  moq.locator_id,
                  SUM (moq.primary_transaction_quantity) quantity_onhand
             FROM mtl_onhand_quantities_detail moq,
                  mtl_secondary_inventories msi
            WHERE     moq.organization_id = p_org_id
                  AND moq.inventory_item_id = p_item_id
                  --AND moq.subinventory_code = l_gen_subinv
                  AND moq.subinventory_code = msi.secondary_inventory_name
                  AND msi.asset_inventory = 1
                  AND moq.subinventory_code != 'GeneralPCK' --added LHS 3/13/2013 for new shipping process
                  AND moq.organization_id = msi.organization_id --added LHS 3/13/2013 for new shipping process
         GROUP BY moq.inventory_item_id,
                  moq.organization_id,
                  moq.subinventory_code,
                  moq.locator_id
           HAVING SUM (moq.primary_transaction_quantity) > 0;

      l_organization_id           NUMBER;
      l_check                     NUMBER;
   BEGIN
      x_return_status := 'S';
      x_error_message := '';

      Write_Log (g_LEVEL_STATEMENT,
                 l_mod_name,
                 '****Scheduler starts for specific selection type*****');
      Write_Log (g_LEVEL_STATEMENT,
                 l_mod_name,
                 'p_inv_org_id =' || p_inv_org_id);

      --Initialize the apps context
      fnd_global.apps_initialize (user_id        => fnd_global.user_id,
                                  resp_id        => fnd_global.resp_id,
                                  resp_appl_id   => fnd_global.resp_appl_id);

      l_count := 0;


      --Logic starts for Specific Type
      FOR org IN c_inv_org
      LOOP
         BEGIN
            Write_Log (g_LEVEL_STATEMENT,
                       l_mod_name,
                       '*****Branch Location Loop********');
            Write_Log (g_LEVEL_STATEMENT,
                       l_mod_name,
                       ' org.inv_org_id =' || org.inv_org_id);

            BEGIN
               SELECT NVL (zero_count_flag, 2),
                      cycle_count_header_id,
                      organization_id
                 INTO l_zero_count_flag,
                      l_cycle_count_header_id,
                      l_organization_id
                 FROM mtl_cycle_count_headers
                WHERE     organization_id = org.inv_org_id
                      AND abc_assignment_group_id IS NOT NULL;
            EXCEPTION
               WHEN NO_DATA_FOUND
               THEN
                  l_error_msg :=
                        'No Cycle count defined for the inv Org :'
                     || org.inv_org_id;
                  RAISE VALIDATION_FAILED;
               WHEN OTHERS
               THEN
                  l_error_msg := 'Unknown Error Msg :' || SQLERRM;
                  RAISE VALIDATION_FAILED;
            END;

            Write_Log (g_LEVEL_STATEMENT,
                       l_mod_name,
                       'Zero Count Flag= ' || l_zero_count_flag);

            Write_Log (
               g_LEVEL_STATEMENT,
               l_mod_name,
               'l_cycle_count_header_id= ' || l_cycle_count_header_id);

            --Processing Logic for Parts Selection Type
            FOR item IN C_item (org.cyclecount_hdr_id, l_organization_id)
            LOOP
               Write_Log (g_LEVEL_STATEMENT,
                          l_mod_name,
                          '*****Item Loop*********');
               Write_Log (g_LEVEL_STATEMENT,
                          l_mod_name,
                          'item.inv_item_id: ' || item.inv_item_id);

               --FOR qty IN C_onhand (item.inv_org_id, item.inv_item_id)
               FOR qty IN C_onhand (l_organization_id, item.inv_item_id)
               LOOP
                  Write_Log (g_LEVEL_STATEMENT,
                             l_mod_name,
                             '*****Onhand qty Loop*******');

                  -- Get a new cycle count schedule ID
                  SELECT mtl_cc_schedule_requests_s.NEXTVAL
                    INTO l_cycle_count_schedule_id
                    FROM DUAL;

                  INSERT
                    INTO mtl_cc_schedule_requests (cycle_count_schedule_id,
                                                   last_update_date,
                                                   last_updated_by,
                                                   creation_date,
                                                   created_by,
                                                   cycle_count_header_id,
                                                   request_source_type,
                                                   zero_count_flag,
                                                   schedule_date,
                                                   schedule_status,
                                                   subinventory,
                                                   locator_id,
                                                   inventory_item_id)
                  VALUES (l_cycle_count_schedule_id,
                          SYSDATE,
                          FND_GLOBAL.USER_ID,
                          SYSDATE,
                          FND_GLOBAL.USER_ID,
                          l_cycle_count_header_id,
                          2,
                          l_zero_count_flag,                --zero_count_flag,
                          item.process_date,
                          1,
                          qty.subinventory_code,
                          qty.locator_id,
                          item.inv_item_id);

                  Write_Log (
                     g_LEVEL_STATEMENT,
                     l_mod_name,
                        'Num of Rows Inserted (mtl_cc_schedule_requests): '
                     || SQL%ROWCOUNT);
                  Write_Log (g_LEVEL_STATEMENT,
                             l_mod_name,
                             '******End Onhand qty Loop******');
               END LOOP;

               Write_Log (g_LEVEL_STATEMENT,
                          l_mod_name,
                          '*****End Item Loop******');
            END LOOP;

            --Processing Logic for Category Selection Type
            FOR cat IN C_category (org.cyclecount_hdr_id)
            LOOP
               Write_Log (g_LEVEL_STATEMENT,
                          l_mod_name,
                          '****category Loop*****');
               Write_Log (g_LEVEL_STATEMENT,
                          l_mod_name,
                          'cat.category_id: ' || cat.category_id);

               FOR cat_item IN C_cat_items (cat.category_id, org.inv_org_id)
               LOOP
                  Write_Log (g_LEVEL_STATEMENT,
                             l_mod_name,
                             '*****category_item Loop****');
                  Write_Log (
                     g_LEVEL_STATEMENT,
                     l_mod_name,
                        ' cat_item.inventory_item_id: '
                     || cat_item.inventory_item_id);

                  FOR qty
                     IN C_onhand (org.inv_org_id, cat_item.inventory_item_id)
                  LOOP
                     Write_Log (g_LEVEL_STATEMENT,
                                l_mod_name,
                                '*******onhand qty Loop*******');

                     -- Get a new cycle count schedule ID
                     SELECT mtl_cc_schedule_requests_s.NEXTVAL
                       INTO l_cycle_count_schedule_id
                       FROM DUAL;

                     INSERT
                       INTO mtl_cc_schedule_requests (cycle_count_schedule_id,
                                                      last_update_date,
                                                      last_updated_by,
                                                      creation_date,
                                                      created_by,
                                                      cycle_count_header_id,
                                                      request_source_type,
                                                      zero_count_flag,
                                                      schedule_date,
                                                      schedule_status,
                                                      subinventory,
                                                      locator_id,
                                                      inventory_item_id)
                     VALUES (l_cycle_count_schedule_id,
                             SYSDATE,
                             FND_GLOBAL.USER_ID,
                             SYSDATE,
                             FND_GLOBAL.USER_ID,
                             l_cycle_count_header_id,
                             2,
                             l_zero_count_flag,             --zero_count_flag,
                             cat.process_date,
                             1,
                             qty.subinventory_code,
                             qty.locator_id,
                             cat_item.inventory_item_id);

                     Write_Log (
                        g_LEVEL_STATEMENT,
                        l_mod_name,
                           'Num of Rows Inserted:(mtl_cc_schedule_requests) '
                        || SQL%ROWCOUNT);

                     Write_Log (g_LEVEL_STATEMENT,
                                l_mod_name,
                                '******End onhand qty Loop********');
                  END LOOP;

                  Write_Log (g_LEVEL_STATEMENT,
                             l_mod_name,
                             '*******End category_item Loop*****');
               END LOOP;

               Write_Log (g_LEVEL_STATEMENT,
                          l_mod_name,
                          '*****End category Loop*****');
            END LOOP;

            UPDATE xxwc_inv_cc_specific_lines
               SET process_status = 'COMPLETE'
             WHERE     cyclecount_hdr_id = org.cyclecount_hdr_id
                   AND inv_org_id = l_organization_id;

            Write_Log (
               g_LEVEL_STATEMENT,
               l_mod_name,
                  'Num of Rows Updated(xxwc_inv_cc_specific_lines): '
               || SQL%ROWCOUNT);

            --Check to see if all lines have processed and then update header
            BEGIN
               SELECT NVL (COUNT (cyclecount_hdr_id), 0)
                 INTO l_check
                 FROM xxwc_inv_cc_specific_lines
                WHERE     cyclecount_hdr_id = org.cyclecount_hdr_id
                      AND process_status != 'COMPLETE';
            EXCEPTION
               WHEN OTHERS
               THEN
                  l_check := 0;
            END;

            IF l_check = 0
            THEN
               UPDATE xxwc_inv_cyclecount_hdr
                  SET process_status = 'COMPLETE'
                WHERE cyclecount_hdr_id = org.cyclecount_hdr_id;

               Write_Log (
                  g_LEVEL_STATEMENT,
                  l_mod_name,
                     'Num of Rows Updated(xxwc_inv_cyclecount_hdr): '
                  || SQL%ROWCOUNT);
            END IF;

            l_count := l_count + 1;
            l_cyclecount_tbl (l_count).inv_org_id := org.inv_org_id;
            l_cyclecount_tbl (l_count).cycle_count_header_id :=
               l_cycle_count_header_id;

            Write_Log (g_LEVEL_STATEMENT,
                       l_mod_name,
                       '****End Branch Location Loop******');
         EXCEPTION
            WHEN VALIDATION_FAILED
            THEN
               Write_Log (g_LEVEL_STATEMENT, l_mod_name, l_error_msg);
            --RAISE VALIDATION_FAILED;
            WHEN OTHERS
            THEN
               l_error_msg := ' Unknown error Msg :' || SQLERRM;
               Write_Log (g_LEVEL_STATEMENT, l_mod_name, l_error_msg);
         --RAISE VALIDATION_FAILED;
         END;
      END LOOP;

      COMMIT;

      Write_Log (g_LEVEL_STATEMENT,
                 l_mod_name,
                 'l_cyclecount_tbl.count=' || l_cyclecount_tbl.COUNT);

      IF l_cyclecount_tbl.COUNT > 0
      THEN
         FOR req IN l_cyclecount_tbl.FIRST .. l_cyclecount_tbl.LAST
         LOOP
            Write_Log (
               g_LEVEL_STATEMENT,
               l_mod_name,
               'Submitting Generate count requests concurrent program');
            -- Call the concurrent program to generate count requests
            l_req_id :=
               fnd_request.submit_request (
                  application   => 'INV',
                  program       => 'INCACG',
                  argument1     => '2',
                  argument2     => TO_CHAR (
                                     l_cyclecount_tbl (req).cycle_count_header_id),
                  argument3     => TO_CHAR (l_cyclecount_tbl (req).inv_org_id));
            Write_Log (g_LEVEL_STATEMENT,
                       l_mod_name,
                       ' l_req_id :' || l_req_id);

            -- Check for errors
            IF (l_req_id <= 0 OR l_req_id IS NULL)
            THEN
               Write_Log (
                  g_LEVEL_STATEMENT,
                  l_mod_name,
                  'Error in calling the generate count requests program');
               RAISE FND_API.G_EXC_ERROR;
            ELSE
               COMMIT;
            END IF;
         END LOOP;
      END IF;

      COMMIT;
      Write_Log (g_LEVEL_STATEMENT,
                 l_mod_name,
                 '****End specific_Scheduler****');
   EXCEPTION
      WHEN FND_API.G_EXC_ERROR
      THEN
         --ROLLBACK TO submit_Scheduler;
         x_error_message :=
            'Error in calling the generate count requests program';
         x_return_status := 'E';
      WHEN VALIDATION_FAILED
      THEN
         --ROLLBACK TO submit_Scheduler;
         x_return_status := 'E';
         x_error_message := l_error_msg;
      WHEN OTHERS
      THEN
         --ROLLBACK TO submit_Scheduler;
         x_return_status := 'E';
         x_error_message := SQLERRM;
   END specific_Scheduler;

   /*************************************************************************
    *   Function/Procedure : Process_Specific
    *
    *   PURPOSE:   This procedure to process the Specific Parts/category selection type
    *   Parameter:
    *          IN
    *              p_cycle_count -- Cycle Coutn Name
    *              p_notes -- Notes
    *              p_store_loc_tbl -- List of Inv Org/Locations
    *              p_type -- Parts/category
    *              p_part_category -- parts/category string
    *              p_schedule_date -- Schedule Date
    *              p_process_now_flag -- Process Now Flag
    *          OUT
    *              x_return_status -- Return Status
    *              x_error_message -- Error Message
    * ************************************************************************/
   PROCEDURE Process_Specific (p_cycle_count        IN     VARCHAR2,
                               p_notes              IN     VARCHAR2,
                               p_store_loc_tbl      IN     store_location_tbl,
                               p_type               IN     VARCHAR2,
                               p_part_category      IN     VARCHAR2,
                               p_schedule_date      IN     DATE,
                               p_process_now_flag   IN     VARCHAR2,
                               x_return_status         OUT VARCHAR2,
                               x_error_message         OUT VARCHAR2)
   IS
      l_cyclecount_hdr_id    NUMBER;
      l_cyclecount_line_id   NUMBER;
      l_process_date         DATE;
      lx_days_to_add         NUMBER;
      l_system_date          DATE := SYSDATE;
      lx_item_number_tbl     item_number_tbl;
      lx_category_tbl        category_tbl;

      l_mod_name             VARCHAR2 (100)
         := 'xxwc_inv_cycle_count_pkg.process_specific';
   BEGIN
      --assign the out parameters
      x_return_status := 'S';
      x_error_message := NULL;

      Write_Log (g_level_statement,
                 l_mod_name,
                 '************process_specific****');
      Write_Log (g_level_statement,
                 l_mod_name,
                 'p_cycle_count =' || p_cycle_count);
      Write_Log (g_level_statement, l_mod_name, 'p_notes =' || p_notes);
      Write_Log (g_level_statement, l_mod_name, 'p_type =' || p_type);
      Write_Log (g_level_statement,
                 l_mod_name,
                 'p_part_category =' || p_part_category);
      Write_Log (g_level_statement,
                 l_mod_name,
                 'p_schedule_date =' || p_schedule_date);
      Write_Log (g_level_statement,
                 l_mod_name,
                 'p_process_now_flag =' || p_process_now_flag);

      --Define the savepoint
      --SAVEPOINT Process_Specific;

      --Generate the header id
      SELECT xxwc_inv_cyclecount_hdr_s.NEXTVAL
        INTO l_cyclecount_hdr_id
        FROM DUAL;

      --1  Automatic Bin Location Selection
      --2  Parts/Category Class Selection
      INSERT INTO xxwc_inv_cyclecount_hdr (cyclecount_hdr_id,
                                           cycle_count_name,
                                           notes,
                                           process_type,
                                           process_date,
                                           process_now_flag,
                                           process_status,
                                           last_update_date,
                                           last_updated_by,
                                           creation_date,
                                           created_by,
                                           last_update_login,
                                           object_version_number)
           VALUES (l_cyclecount_hdr_id,
                   p_cycle_count,
                   p_notes,
                   'SPECIFIC',
                   p_schedule_date,
                   p_process_now_flag,
                   'ACTIVE',
                   SYSDATE,
                   FND_GLOBAL.USER_ID,
                   SYSDATE,
                   fnd_global.user_id,
                   fnd_global.login_id,
                   1);

      Write_Log (g_level_statement,
                 l_mod_name,
                 'Num of rows (xxwc_inv_cyclecount_hdr) =' || SQL%ROWCOUNT);

      IF p_process_now_flag = 'Y'
      THEN
         l_process_date := l_system_date;
      ELSE
         l_process_date := p_schedule_date;
      END IF;

      Write_Log (
         g_level_statement,
         l_mod_name,
            'l_process_date ='
         || TO_CHAR (l_process_date, 'DD-MON-YYYY HH12:MI:SS AM'));

      Write_Log (g_level_statement, l_mod_name, 'p_type =' || p_type);

      --P Parts
      --C Category
      IF p_type = 'P'
      THEN
         Write_Log (g_level_statement, l_mod_name, 'calling get_parts_list ');
         Get_parts_list (p_store_loc_tbl     => p_store_loc_tbl,
                         p_part_category     => p_part_category,
                         x_item_number_tbl   => lx_item_number_tbl);

         Write_Log (g_level_statement,
                    l_mod_name,
                    'lx_item_number_tbl.count =' || lx_item_number_tbl.COUNT);

         IF lx_item_number_tbl.COUNT > 0
         THEN
            lx_days_to_add := 0;

            FOR k IN lx_item_number_tbl.FIRST .. lx_item_number_tbl.LAST
            LOOP
               Write_Log (
                  g_LEVEL_STATEMENT,
                  l_mod_name,
                     ' l_process_date before block out date calculation= '
                  || TO_CHAR (l_process_date, 'DD-MON-YYYY HH12:MI:SS AM'));

               /*
                            --Removed 3/3/2013 don't exclude weekends or black out dates
                              Get_Process_Date (
                                 p_inv_org_id      => lx_item_number_tbl (k).inv_org_id,
                                 px_process_date   => l_process_date,
                                 px_days_to_add    => lx_days_to_add
                              );

               */

               Write_Log (
                  g_level_statement,
                  l_mod_name,
                     'l_process_date after block out date calculation='
                  || TO_CHAR (l_process_date, 'DD-MON-YYYY HH12:MI:SS AM'));



               INSERT INTO xxwc_inv_cc_specific_lines (cc_specific_line_id,
                                                       cyclecount_hdr_id,
                                                       TYPE,
                                                       inv_org_id,
                                                       item_number,
                                                       inv_item_id,
                                                       category,
                                                       category_id,
                                                       process_date,
                                                       process_status,
                                                       last_update_date,
                                                       last_updated_by,
                                                       creation_date,
                                                       created_by,
                                                       last_update_login,
                                                       object_version_number)
                    VALUES (xxwc_inv_cc_specific_lines_s.NEXTVAL,
                            l_cyclecount_hdr_id,
                            p_type,
                            lx_item_number_tbl (k).inv_org_id,
                            lx_item_number_tbl (k).item_number,
                            lx_item_number_tbl (k).inv_item_id,
                            NULL,
                            NULL,
                            l_process_date,                     --process_date
                            'ACTIVE',
                            SYSDATE,
                            fnd_global.user_id,
                            SYSDATE,
                            fnd_global.user_id,
                            fnd_global.login_id,
                            1);

               Write_Log (
                  g_level_statement,
                  l_mod_name,
                     'Num of rows Inserted (xxwc_inv_cc_specific_lines) ='
                  || SQL%ROWCOUNT);
            END LOOP;
         END IF;
      ELSE
         write_log (g_level_statement, l_mod_name, 'Call Get_Category_list');
         Get_Category_list (p_store_loc_tbl   => p_store_loc_tbl,
                            p_part_category   => p_part_category,
                            x_category_tbl    => lx_category_tbl);
         write_log (g_level_statement,
                    l_mod_name,
                    'lx_category_tbl.count  =' || lx_category_tbl.COUNT);

         IF lx_category_tbl.COUNT > 0
         THEN
            lx_days_to_add := 0;

            FOR k IN lx_category_tbl.FIRST .. lx_category_tbl.LAST
            LOOP
               Write_Log (
                  g_LEVEL_STATEMENT,
                  l_mod_name,
                     ' l_process_date before block out date calculation= '
                  || TO_CHAR (l_process_date, 'DD-MON-YYYY HH12:MI:SS AM'));

               Write_Log (
                  g_LEVEL_STATEMENT,
                  l_mod_name,
                     ' lx_category_tbl (k).inv_org_id = '
                  || lx_category_tbl (k).inv_org_id);

               /*  --Removed 3/3/2013 don't exclude weekends or black out dates
                  Get_Process_Date (
                     p_inv_org_id      => lx_category_tbl(k).inv_org_id,
                     px_process_date   => l_process_date,
                     px_days_to_add    => lx_days_to_add
                  );
               */

               Write_Log (g_LEVEL_STATEMENT,
                          l_mod_name,
                          ' lx_days_to_add= ' || lx_days_to_add);
               Write_Log (
                  g_LEVEL_STATEMENT,
                  l_mod_name,
                     ' l_process_date after block out date calculation= '
                  || TO_CHAR (l_process_date, 'DD-MON-YYYY HH12:MI:SS AM'));

               INSERT INTO xxwc_inv_cc_specific_lines (cc_specific_line_id,
                                                       cyclecount_hdr_id,
                                                       TYPE,
                                                       inv_org_id,
                                                       item_number,
                                                       inv_item_id,
                                                       category,
                                                       category_id,
                                                       process_date,
                                                       process_status,
                                                       last_update_date,
                                                       last_updated_by,
                                                       creation_date,
                                                       created_by,
                                                       last_update_login,
                                                       object_version_number)
                    VALUES (xxwc_inv_cc_specific_lines_s.NEXTVAL,
                            l_cyclecount_hdr_id,
                            p_type,
                            lx_category_tbl (k).inv_org_id,
                            NULL,
                            NULL,
                            lx_category_tbl (k).category,
                            lx_category_tbl (k).category_id,
                            l_process_date,                     --process_date
                            'ACTIVE',
                            SYSDATE,
                            fnd_global.user_id,
                            SYSDATE,
                            fnd_global.user_id,
                            fnd_global.login_id,
                            1);

               Write_Log (
                  g_level_statement,
                  l_mod_name,
                     'Num of rows (xxwc_inv_cc_specific_lines) ='
                  || SQL%ROWCOUNT);
            END LOOP;                                   --lx_category_tbl Loop
         END IF;                                   --lx_category_tbl.COUNT > 0
      END IF;                                                --IF p_type = 'P'

      COMMIT;

      --Submit the scheduler if process now flag is Y
      IF p_process_now_flag = 'Y'
      THEN
         Write_Log (g_level_statement,
                    l_mod_name,
                    'Calling specific_scheduler =');
         specific_scheduler (p_inv_org_id      => NULL,
                             x_return_status   => x_return_status,
                             x_error_message   => x_error_message);
         COMMIT;
      END IF;

      Write_Log (g_level_statement,
                 l_mod_name,
                 '****End process_specific****');
   EXCEPTION
      WHEN OTHERS
      THEN
         --ROLLBACK TO Process_Specific;
         x_return_status := 'E';
         x_error_message := SQLERRM;
   END Process_Specific;

   /*************************************************************************
    *   Function/Procedure : Delete_auto_lines
    *
    *   PURPOSE:   This procedure to delete cycle count Lines and Details
    *   Parameter:
    *          IN
    *              p_cyclecount_line_id -- Cycle Count Line Id
    *          OUT
    *              x_return_status -- Return Status
    *              x_error_msg -- Error Message
    * ************************************************************************/
   PROCEDURE Delete_auto_lines (p_cyclecount_line_id   IN     NUMBER,
                                x_return_status           OUT VARCHAR2,
                                x_error_msg               OUT VARCHAR2)
   IS
      l_process_status   xxwc_inv_cyclecount_lines.process_status%TYPE;
      x_msg_count        NUMBER;
      l_mod_name         VARCHAR2 (100)
                            := 'xxwc_inv_cycle_count_pkg.update_line_dtls';
   BEGIN
      x_return_status := 'S';
      x_error_msg := NULL;
      Write_Log (g_LEVEL_STATEMENT,
                 l_mod_name,
                 ' p_cyclecount_line_id=' || p_cyclecount_line_id);

      BEGIN
         SELECT process_status
           INTO l_process_status
           FROM xxwc_inv_cyclecount_lines
          WHERE cyclecount_line_id = p_cyclecount_line_id;
      EXCEPTION
         WHEN OTHERS
         THEN
            FND_MESSAGE.SET_NAME ('XXWC', 'XXWC_INVALID_CC_LINE_ID');
            FND_MESSAGE.SET_TOKEN ('CC_LINE_ID', p_cyclecount_line_id);
            FND_MSG_PUB.Add;
            RAISE fnd_api.g_exc_error;
      END;

      IF l_process_status = 'COMPLETE'
      THEN
         Write_Log (
            g_LEVEL_STATEMENT,
            l_mod_name,
               ' The Line is already interfaced to Oracle -p_cyclecount_line_id='
            || p_cyclecount_line_id);
         FND_MESSAGE.SET_NAME ('XXWC', 'XXWC_DELETE_NOT_ALLOWED');
         FND_MESSAGE.SET_TOKEN ('CC_LINE_ID', p_cyclecount_line_id);
         FND_MSG_PUB.Add;
         RAISE fnd_api.g_exc_error;
      END IF;

      DELETE FROM xxwc_inv_cyclecount_line_dtls
            WHERE cyclecount_line_id = p_cyclecount_line_id;

      DELETE FROM xxwc_inv_cyclecount_lines
            WHERE cyclecount_line_id = p_cyclecount_line_id;
   EXCEPTION
      WHEN fnd_api.g_exc_error
      THEN
         x_return_status := 'E';
         fnd_msg_pub.count_and_get (p_count   => x_msg_count,
                                    p_data    => x_error_msg);
      WHEN OTHERS
      THEN
         x_return_status := 'E';
         x_error_msg := SQLERRM;
   END Delete_auto_lines;

   /*************************************************************************
    *   Function/Procedure : Delete_Line_Dtls
    *
    *   PURPOSE:   This procedure to delete cycle count Lines Details
    *   Parameter:
    *          IN
    *              p_cyclecount_line_dtl_id -- Cycle Count Line Detail Id
    *          OUT
    *              x_return_status -- Return Status
    *              x_error_msg -- Error Message
    * ************************************************************************/
   PROCEDURE Delete_Line_Dtls (p_cyclecount_line_dtl_id IN NUMBER)
   IS
      l_process_status   xxwc_inv_cyclecount_line_dtls.process_status%TYPE;
      x_error_msg        VARCHAR2 (100);
      x_msg_count        NUMBER;
      l_mod_name         VARCHAR2 (100)
                            := 'xxwc_inv_cycle_count_pkg.update_line_dtls';
   BEGIN
      Write_Log (g_LEVEL_STATEMENT,
                 l_mod_name,
                 ' p_cyclecount_line_dtl_id=' || p_cyclecount_line_dtl_id);

      BEGIN
         SELECT process_status
           INTO l_process_status
           FROM xxwc_inv_cyclecount_line_dtls
          WHERE cyclecount_line_dtl_id = p_cyclecount_line_dtl_id;
      EXCEPTION
         WHEN OTHERS
         THEN
            FND_MESSAGE.SET_NAME ('XXWC', 'XXWC_INVALID_CC_LINE_ID');
            FND_MESSAGE.SET_TOKEN ('CC_LINE_DTL_ID',
                                   p_cyclecount_line_dtl_id);
            FND_MSG_PUB.Add;
            RAISE fnd_api.g_exc_error;
      END;

      IF l_process_status = 'COMPLETE'
      THEN
         Write_Log (
            g_LEVEL_STATEMENT,
            l_mod_name,
               ' The Detail Line is already interfaced to Oracle - p_cyclecount_line_dtl_id='
            || p_cyclecount_line_dtl_id);
         FND_MESSAGE.SET_NAME ('XXWC', 'XXWC_DELETE_NOT_ALLOWED');
         FND_MESSAGE.SET_TOKEN ('CC_LINE_DTL_ID', p_cyclecount_line_dtl_id);
         FND_MSG_PUB.Add;
         RAISE fnd_api.g_exc_error;
      END IF;

      DELETE FROM xxwc_inv_cyclecount_line_dtls
            WHERE cyclecount_line_dtl_id = p_cyclecount_line_dtl_id;
   END Delete_Line_Dtls;

   /*************************************************************************
    *   Function/Procedure : Update_Line_Dtls
    *
    *   PURPOSE:   This procedure to Update  cycle count Lines Details
    *   Parameter:
    *          IN
    *              p_cyclecount_line_dtl_id -- Cycle Count Line Detail Id
    *              p_process_status -- Process Status
    *              p_process_date -- Process Date
    *              p_object_version_number -- Object Version Number
    * ************************************************************************/
   PROCEDURE Update_Line_Dtls (p_cyclecount_line_dtl_id    NUMBER,
                               p_process_status            VARCHAR2,
                               p_process_date              DATE,
                               p_object_version_number     NUMBER)
   IS
      l_mod_name         VARCHAR2 (100)
                            := 'xxwc_inv_cycle_count_pkg.update_line_dtls';

      l_process_status   xxwc_inv_cyclecount_line_dtls.process_status%TYPE;
      x_error_msg        VARCHAR2 (100);
      x_msg_count        NUMBER;
   BEGIN
      Write_Log (g_LEVEL_STATEMENT,
                 l_mod_name,
                 ' p_cyclecount_line_dtl_id=' || p_cyclecount_line_dtl_id);

      BEGIN
         SELECT process_status
           INTO l_process_status
           FROM xxwc_inv_cyclecount_line_dtls
          WHERE cyclecount_line_dtl_id = p_cyclecount_line_dtl_id;
      EXCEPTION
         WHEN OTHERS
         THEN
            FND_MESSAGE.SET_NAME ('XXWC', 'XXWC_INVALID_CC_LINE_ID');
            FND_MESSAGE.SET_TOKEN ('CC_LINE_DTL_ID',
                                   p_cyclecount_line_dtl_id);
            FND_MSG_PUB.Add;
            RAISE fnd_api.g_exc_error;
      END;

      IF l_process_status = 'COMPLETE'
      THEN
         Write_Log (
            g_LEVEL_STATEMENT,
            l_mod_name,
               ' The Line is interfaced to Oracle - p_cyclecount_line_dtl_id='
            || p_cyclecount_line_dtl_id);
         FND_MESSAGE.SET_NAME ('XXWC', 'XXWC_UPDATE_NOT_ALLOWED');
         FND_MESSAGE.SET_TOKEN ('CC_LINE_DTL_ID', p_cyclecount_line_dtl_id);
         FND_MSG_PUB.Add;
         RAISE fnd_api.g_exc_error;
      END IF;

      UPDATE xxwc_inv_cyclecount_line_dtls
         SET process_status =
                DECODE (p_process_status,
                        fnd_api.g_miss_char, process_status,
                        p_process_status),
             process_date =
                DECODE (p_process_date,
                        fnd_api.g_miss_date, process_date,
                        p_process_date),
             last_update_date = SYSDATE,
             last_updated_by = fnd_global.user_id,
             last_update_login = fnd_global.login_id,
             object_version_number =
                DECODE (p_object_version_number,
                        fnd_api.g_miss_num, object_version_number,
                        p_object_version_number)
       WHERE cyclecount_line_dtl_id = p_cyclecount_line_dtl_id;
   END Update_Line_Dtls;

   /*************************************************************************
    *   Function/Procedure : Delete_Specific_lines
    *
    *   PURPOSE:   This procedure to delete  cycle count specific Lines
    *   Parameter:
    *          IN
    *              p_cc_specific_line_id -- Cycle Count Specific Line Id
    * ************************************************************************/

   PROCEDURE Delete_Specific_lines (p_cc_specific_line_id IN NUMBER)
   IS
      l_process_status   xxwc_inv_cc_specific_lines.process_status%TYPE;
      l_mod_name         VARCHAR2 (100)
         := 'xxwc_inv_cycle_count_pkg.Delete_Specific_lines';
   BEGIN
      Write_Log (g_LEVEL_STATEMENT,
                 l_mod_name,
                 'p_cc_specific_line_id :' || p_cc_specific_line_id);

      BEGIN
         SELECT process_status
           INTO l_process_status
           FROM xxwc_inv_cc_specific_lines
          WHERE cc_specific_line_id = p_cc_specific_line_id;
      EXCEPTION
         WHEN OTHERS
         THEN
            FND_MESSAGE.SET_NAME ('XXWC', 'XXWC_INVALID_CC_LINE_ID');
            FND_MESSAGE.SET_TOKEN ('CC_SPECIFIC_LINE_ID',
                                   p_cc_specific_line_id);
            FND_MSG_PUB.Add;
            RAISE fnd_api.g_exc_error;
      END;

      IF l_process_status = 'COMPLETE'
      THEN
         Write_Log (
            g_LEVEL_STATEMENT,
            l_mod_name,
               'The Line is already interfaced to Oracle. It cannot be deleted - p_cc_specific_line_id :'
            || p_cc_specific_line_id);
         FND_MESSAGE.SET_NAME ('XXWC', 'XXWC_DELETE_NOT_ALLOWED');
         FND_MESSAGE.SET_TOKEN ('CC_SPECIFIC_LINE_ID', p_cc_specific_line_id);
         FND_MSG_PUB.Add;
         RAISE fnd_api.g_exc_error;
      END IF;

      DELETE FROM xxwc_inv_cc_specific_lines
            WHERE cc_specific_line_id = p_cc_specific_line_id;
   END Delete_Specific_lines;


   /*************************************************************************
    *   Function/Procedure : Submit_Scheduler
    *
    *   PURPOSE:   This procedure to kick off the scheduler
    *   Parameter:
    *          IN
    *              p_inv_org_id -- Inv Org (Optional)
    * ************************************************************************/
   PROCEDURE submit_scheduler (errbuf            OUT VARCHAR2,
                               retcode           OUT VARCHAR2,
                               p_inv_org_id   IN     NUMBER)
   IS
      l_mod_name         VARCHAR2 (100)
                            := 'xxwc_inv_cycle_count_pkg.submit_scheduler';
      lx_return_status   VARCHAR2 (10);
      lx_error_msg       VARCHAR2 (1000);

      API_FAILURE        EXCEPTION;
   BEGIN
      -- Assign out parameter
      errbuf := '';
      retcode := '0';

      Write_Log (g_LEVEL_STATEMENT,
                 l_mod_name,
                 '**********************submit_scheduler***************');

      Write_Log (g_LEVEL_STATEMENT,
                 l_mod_name,
                 'p_inv_org_id =' || p_inv_org_id);

      Write_Log (g_LEVEL_STATEMENT,
                 l_mod_name,
                 'calling automatic_scheduler');
      automatic_Scheduler (p_inv_org_id      => p_inv_org_id,
                           x_return_status   => lx_return_status,
                           x_error_message   => lx_error_msg);

      Write_Log (
         g_LEVEL_STATEMENT,
         l_mod_name,
         'lx_return_status(automatic_scheduler)=' || lx_return_status);

      IF lx_return_status <> 'S'
      THEN
         Write_Log (g_LEVEL_STATEMENT,
                    l_mod_name,
                    'lx_error_msg =' || lx_error_msg);
         RAISE API_FAILURE;
      END IF;

      Write_Log (g_LEVEL_STATEMENT, l_mod_name, 'calling specific_scheduler');
      specific_Scheduler (p_inv_org_id      => p_inv_org_id,
                          x_return_status   => lx_return_status,
                          x_error_message   => lx_error_msg);

      Write_Log (g_LEVEL_STATEMENT,
                 l_mod_name,
                 'lx_return_status(specific_scheduler)=' || lx_return_status);

      IF lx_return_status <> 'S'
      THEN
         Write_Log (g_LEVEL_STATEMENT,
                    l_mod_name,
                    'lx_error_msg =' || lx_error_msg);
         RAISE API_FAILURE;
      END IF;

      Write_Log (g_LEVEL_STATEMENT,
                 l_mod_name,
                 '*********************End************');
   EXCEPTION
      WHEN API_FAILURE
      THEN
         errbuf := lx_error_msg;
         retcode := '2';
      WHEN OTHERS
      THEN
         errbuf := SQLERRM;
         retcode := '2';
   END submit_scheduler;

   /*************************************************************************
 *   Function/Procedure : Update_Item_id
 *
 *   PURPOSE:   This procedure to update the item Id
 *   Parameter:
 *          OUT
 *              errbuf -- Error Buffer
 *              retcode -- Return Code
 * ************************************************************************/
   PROCEDURE Update_Item_id (errbuf OUT VARCHAR2, retcode OUT VARCHAR2)
   IS
      l_mod_name       VARCHAR2 (100)
                          := 'xxwc_inv_cycle_count_pkg.update_item_id';

      SKIP_RECORD      EXCEPTION;
      l_item_id        NUMBER;
      l_update_count   NUMBER;

      CURSOR C1
      IS
         SELECT ROWID row_id, item_id, item_number
           FROM xxwc_mstr_parts_excl_list
          WHERE item_number IS NOT NULL AND item_id IS NULL;
   BEGIN
      errbuf := '';
      retcode := '0';
      Write_Log (g_level_statement, l_mod_name, 'At Begin  ');

      l_update_count := 0;

      FOR i IN C1
      LOOP
         BEGIN
            Write_Log (g_level_statement,
                       l_mod_name,
                       'i.item_number =  ' || i.item_number);

            BEGIN
               SELECT inventory_item_id
                 INTO l_item_id
                 FROM mtl_system_items b
                WHERE b.segment1 = i.item_number AND ROWNUM = 1;
            EXCEPTION
               WHEN OTHERS
               THEN
                  RAISE SKIP_RECORD;
            END;

            Write_Log (g_level_statement,
                       l_mod_name,
                       'l_item_id =  ' || l_item_id);

            UPDATE xxwc_mstr_parts_excl_list a
               SET item_id = l_item_id
             WHERE ROWID = i.row_id;

            l_update_count := l_update_count + 1;
         EXCEPTION
            WHEN SKIP_RECORD
            THEN
               Write_Log (g_level_statement,
                          l_mod_name,
                          'Skipping Record ... ');
         END;
      END LOOP;

      Write_Log (
         g_level_statement,
         l_mod_name,
         'Number of Items Updated with Item Id =  ' || l_update_count);

      COMMIT;
   EXCEPTION
      WHEN OTHERS
      THEN
         errbuf := SQLERRM;
         retcode := '2';
   END Update_Item_id;

   /*
      PROCEDURE Submit (errbuf OUT VARCHAR2, retcode OUT VARCHAR2)
      IS
         --Define the variables
         l_error_message             VARCHAR2 (1000);
         l_dummy                     VARCHAR2 (1);
         l_inv_org_id                NUMBER;
         l_cycle_count_schedule_id   NUMBER;
         l_cycle_count_header_id     NUMBER;
         l_zero_count_flag           VARCHAR2 (1);
         l_inventory_item_id         NUMBER;
         l_gen_subinv VARCHAR2 (100)
               := fnd_profile.VALUE ('XXWC_INV_GENERAL_SUBINV') ;
         l_request_id                NUMBER := fnd_global.conc_request_id;
         l_mod_name VARCHAR2 (100)
               := 'xxwc_inv_cycle_count_pkg.Submit' ;


         --select all the unprocessed records
         CURSOR C1
         IS
                SELECT   xcc.ROWID row_id, xcc.*
                  FROM   xxwc_cyclecount xcc
                 WHERE   process_status <> 'P'
            FOR UPDATE   NOWAIT;


         --select all the unprocessed records
         CURSOR C2 (
            p_inv_org_id                 NUMBER,
            p_item_id      IN            NUMBER
         )
         IS
              SELECT   subinventory_code,
                       SUM (primary_transaction_quantity) quantity
                FROM   mtl_onhand_quantities_detail
               WHERE       subinventory_code <> l_gen_subinv
                       AND organization_id = p_inv_org_id
                       AND inventory_item_id = p_item_id
            GROUP BY   inventory_item_id, organization_id, subinventory_code
              HAVING   SUM (primary_transaction_quantity) > 0;


         --count by process status
         CURSOR C3
         IS
              SELECT   COUNT ( * ) count_by_status,
                       DECODE (process_status,
                               'R', 'Ready to Process',
                               'E', 'Error',
                               'P', 'Processed',
                               process_status)
                          process_status
                FROM   xxwc_cyclecount xcc
               WHERE   request_id = l_request_id
            GROUP BY   process_status;

         --count by process status
         CURSOR C4
         IS
            SELECT   item_number, error_message
              FROM   xxwc_cyclecount xcc
             WHERE   request_id = l_request_id AND process_status = 'E';

         --Define user exception
         VALIDATION_ERROR EXCEPTION;
      BEGIN
         errbuf := NULL;
         retcode := '0';

         --Initialize the apps context
         fnd_global.apps_initialize (user_id        => fnd_global.user_id,
                                     resp_id        => fnd_global.resp_id,
                                     resp_appl_id   => fnd_global.resp_appl_id);

         --Clear the existing session
         FND_MESSAGE.CLEAR;

         --update the request id for
         --the unprocessed records
         UPDATE   xxwc_cyclecount xcc
            SET   request_id = l_request_id
          WHERE   process_status <> 'P';

         FOR i IN C1
         LOOP
            BEGIN
               Write_Log (g_level_statement,
                          l_mod_name,
                          'item_number   => ' || i.item_number);
               Write_Log (g_level_statement,
                          l_mod_name,
                          'organization  => ' || i.organization);

               --Validate the Cycle Count
               BEGIN
                  SELECT   cycle_count_header_id
                    INTO   l_cycle_count_header_id
                    FROM   mtl_cycle_count_headers
                   WHERE   cycle_count_header_name = i.cycle_count_name;
               EXCEPTION
                  WHEN NO_DATA_FOUND
                  THEN
                     FND_MESSAGE.SET_NAME ('XXWC', 'XXWC_INV_CYCLE_CNT_MISSING');
                     FND_MESSAGE.SET_TOKEN ('XXWC_CYCLE_COUNT_NAME',
                                            i.cycle_count_name);
                     FND_MSG_PUB.ADD;
                     RAISE VALIDATION_ERROR;
                  WHEN OTHERS
                  THEN
                     l_error_message := SQLERRM;
                     RAISE VALIDATION_ERROR;
               END;

               Write_Log (
                  g_level_statement,
                  l_mod_name,
                  'l_cycle_count_header_id   => ' || l_cycle_count_header_id
               );

               --Validate the organization
               BEGIN
                  SELECT   organization_id
                    INTO   l_inv_org_id
                    FROM   mtl_parameters
                   WHERE   organization_code = i.organization;
               EXCEPTION
                  WHEN NO_DATA_FOUND
                  THEN
                     FND_MESSAGE.SET_NAME ('XXWC', 'XXWC_INV_ORG_INVALID');
                     FND_MESSAGE.SET_TOKEN ('XXWC_INV_ORG_ID', i.organization);
                     FND_MSG_PUB.ADD;
                     RAISE VALIDATION_ERROR;
                  WHEN OTHERS
                  THEN
                     l_error_message := SQLERRM;
                     RAISE VALIDATION_ERROR;
               END;

               Write_Log (g_level_statement,
                          l_mod_name,
                          'l_inv_org_id   => ' || l_inv_org_id);

               --Validate the inventory item
               BEGIN
                  SELECT   inventory_item_id
                    INTO   l_inventory_item_id
                    FROM   mtl_system_items
                   WHERE   organization_id = l_inv_org_id
                           AND segment1 = i.item_number;
               EXCEPTION
                  WHEN NO_DATA_FOUND
                  THEN
                     FND_MESSAGE.SET_NAME ('XXWC', 'XXWC_INV_ITEM_INVALID');
                     FND_MESSAGE.SET_TOKEN ('XXWC_INV_ITEM_NUMBER',
                                            i.item_number);
                     FND_MSG_PUB.ADD;
                     RAISE VALIDATION_ERROR;
                  WHEN OTHERS
                  THEN
                     l_error_message := SQLERRM;
                     RAISE VALIDATION_ERROR;
               END;

               Write_Log (g_level_statement,
                          l_mod_name,
                          'l_inventory_item_id   => ' || l_inventory_item_id);

               -- Get a new cycle count schedule ID
               SELECT   mtl_cc_schedule_requests_s.NEXTVAL
                 INTO   l_cycle_count_schedule_id
                 FROM   DUAL;

               Write_Log (
                  g_level_statement,
                  l_mod_name,
                  'l_cycle_count_schedule_id   => ' || l_cycle_count_schedule_id
               );

               SELECT   NVL (zero_count_flag, 2)
                 INTO   l_zero_count_flag
                 FROM   mtl_cycle_count_headers
                WHERE   cycle_count_header_id = l_cycle_count_header_id
                        AND organization_id = l_inv_org_id;

               Write_Log (g_level_statement,
                          l_mod_name,
                          'l_zero_count_flag   => ' || l_zero_count_flag);

               INSERT INTO mtl_cc_schedule_requests (cycle_count_schedule_id,
                                                     last_update_date,
                                                     last_updated_by,
                                                     creation_date,
                                                     created_by,
                                                     cycle_count_header_id,
                                                     request_source_type,
                                                     zero_count_flag,
                                                     schedule_date,
                                                     schedule_status,
                                                     subinventory,
                                                     inventory_item_id)
                 VALUES   (l_cycle_count_schedule_id,
                           SYSDATE,
                           FND_GLOBAL.USER_ID,
                           SYSDATE,
                           FND_GLOBAL.USER_ID,
                           l_cycle_count_header_id,
                           2,
                           l_zero_count_flag,
                           SYSDATE,
                           1,
                           l_gen_subinv,
                           l_inventory_item_id);

               Write_Log (
                  g_level_statement,
                  l_mod_name,
                  'No of rows inserted (mtl_cc_schedule_requests)  => '
                  || sql%ROWCOUNT
               );

               FOR k IN c2 (l_inv_org_id, l_inventory_item_id)
               LOOP
                  Write_Log (
                     g_level_statement,
                     l_mod_name,
                     'Inserting another record for the subinventory_code  => '
                     || k.subinventory_code
                  );

                  -- Get a new cycle count schedule ID
                  SELECT   mtl_cc_schedule_requests_s.NEXTVAL
                    INTO   l_cycle_count_schedule_id
                    FROM   DUAL;

                  Write_Log (
                     g_level_statement,
                     l_mod_name,
                     'l_cycle_count_schedule_id   => '
                     || l_cycle_count_schedule_id
                  );

                  --insert into mtl_cc_schedule_requests
                  INSERT INTO mtl_cc_schedule_requests (cycle_count_schedule_id,
                                                        last_update_date,
                                                        last_updated_by,
                                                        creation_date,
                                                        created_by,
                                                        cycle_count_header_id,
                                                        request_source_type,
                                                        zero_count_flag,
                                                        schedule_date,
                                                        schedule_status,
                                                        subinventory,
                                                        inventory_item_id)
                    VALUES   (l_cycle_count_schedule_id,
                              SYSDATE,
                              FND_GLOBAL.USER_ID,
                              SYSDATE,
                              FND_GLOBAL.USER_ID,
                              l_cycle_count_header_id,
                              2,
                              l_zero_count_flag,
                              SYSDATE,
                              1,
                              k.subinventory_code,
                              l_inventory_item_id);

                  Write_Log (
                     g_level_statement,
                     l_mod_name,
                     'No of rows inserted (mtl_cc_schedule_requests)  => '
                     || sql%ROWCOUNT
                  );
               END LOOP;

               Write_Log (g_level_statement,
                          l_mod_name,
                          'Updating xxwc_cyclecount to processed status');

               --update the status to processed
               UPDATE   xxwc_cyclecount xcc
                  SET   process_status = 'P',
                        error_message = NULL,
                        last_update_date = SYSDATE,
                        last_updated_by = fnd_global.user_id,
                        last_update_login = fnd_global.login_id,
                        object_version_number = object_version_number + 1
                WHERE   ROWID = i.row_id;

               Write_Log (
                  g_level_statement,
                  l_mod_name,
                  'No of rows updated to processed (xxwc_cyclecount)  => '
                  || sql%ROWCOUNT
               );
            EXCEPTION
               WHEN VALIDATION_ERROR
               THEN
                  l_error_message := fnd_message.get;
                  write_error (l_error_message);

                  --update the status to error
                  UPDATE   xxwc_cyclecount
                     SET   process_status = 'E',
                           error_message = l_error_message,
                           object_version_number = object_version_number + 1
                   WHERE   ROWID = i.row_id;
            END;
         END LOOP;

         Write_output('+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++');
         Write_output('+                  Count by Process Status                        +');
         Write_output('+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++');
         Write_output (RPAD ('Status  ', 15, ' ') || RPAD ('Count', 15, ' '));
         Write_output('+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++');

         FOR j IN C3
         LOOP
            Write_output(RPAD (j.process_status || ' =>', 15, ' ')
                         || RPAD (j.count_by_status, 15, ' '));
         END LOOP;

         Write_output('+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++');

         Write_output ('');
         Write_output ('');
         Write_output ('');

         Write_output('+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++');
         Write_output('+                  Error Report                                   +');
         Write_output('+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++');
         Write_output (
            RPAD ('Item Number', 15, ' ') || RPAD ('Error Message', 15, ' ')
         );
         Write_output('+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++');

         FOR m IN C4
         LOOP
            Write_output(RPAD (m.item_number || ' =>', 15, ' ')
                         || RPAD (m.error_message, 15, ' '));
         END LOOP;

         Write_output('+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++');

         COMMIT;
      EXCEPTION
         WHEN OTHERS
         THEN
            errbuf := SQLERRM;
            retcode := '2';
            write_error (errbuf);
      END Submit;
   */
   /*************************************************************************
    *   Function/Procedure : Process_PI
    *
    *   PURPOSE:   This procedure to delete  cycle count specific Lines
    *   Parameter:
    *          IN
    *              p_cc_specific_line_id -- Cycle Count Specific Line Id
    *   Ver        Date        Author                     Description
    *   ---------  ----------  ---------------         -------------------------
    *   1.0        09/30/2011  Vivek Lakaman             Initial Version
    *   1.5        22/04/2016  Niraj Ranjan                TMS#20151214-00044  Review PI Scheduling with Inactive Items
    * ************************************************************************/
   PROCEDURE Process_PI (p_cycle_count        IN     VARCHAR2,
                         p_notes              IN     VARCHAR2,
                         p_inv_org_id         IN     NUMBER,
                         p_subinventories     IN     store_sub_tbl,
                         p_schedule_date      IN     DATE,
                         p_process_now_flag   IN     VARCHAR2, --Added TMS Ticket 20130809-01349 - 9/6/2013
                         x_return_status         OUT VARCHAR2,
                         x_error_message         OUT VARCHAR2)
   IS
      l_cycle_count_header_id     NUMBER;
      l_zero_count_flag           NUMBER;
      l_mod_name                  VARCHAR2 (100)
                                     := 'xxwc_inv_cycle_count_pkg.process_pi';
      l_error_msg                 VARCHAR2 (1000);
      i                           NUMBER DEFAULT 0;
      l_cycle_count_schedule_id   NUMBER;

      CURSOR c_onhand (
         p_org_id          NUMBER,
         p_subinventory    VARCHAR2)
      IS
           SELECT moq.inventory_item_id,
                  moq.organization_id,
                  moq.subinventory_code,
                  moq.locator_id,
                  SUM (moq.primary_transaction_quantity) quantity_onhand
             FROM mtl_onhand_quantities_detail moq,
                  mtl_secondary_inventories msi
            WHERE     moq.organization_id = p_org_id
                  AND moq.subinventory_code = p_subinventory
                  AND moq.subinventory_code = msi.secondary_inventory_name
                  --AND msi.asset_inventory = 1
                  AND moq.organization_id = msi.organization_id --added LHS 3/13/2013 for new shipping process
                  --Added Not Exists Clause on 10/24/2014 for TMS Ticket # 20131016-00470 Exclude items assigned to Cat/Class SP.SPPP from PI�s
                  AND NOT EXISTS
                         (SELECT *
                            FROM mtl_category_sets mcs,
                                 mtl_categories_kfv mck,
                                 mtl_item_categories mic
                           WHERE     mcs.category_set_name =
                                        'Inventory Category'
                                 AND mcs.structure_id = mck.structure_id
                                 AND mck.concatenated_segments IN ('SP.SPPP')
                                 AND mck.category_id = mic.category_id
                                 AND mic.inventory_item_id =
                                        moq.inventory_item_id
                                 AND mic.organization_id = moq.organization_id)
         GROUP BY moq.inventory_item_id,
                  moq.organization_id,
                  moq.subinventory_code,
                  moq.locator_id
           HAVING SUM (moq.primary_transaction_quantity) > 0
         --TMS Ticket 20130809-01349 - 9/6/2013 - Added Union to get items assigned to locators that do not have a qty on-hand
         UNION
         SELECT DISTINCT msl.inventory_item_id,
                         msl.organization_id,
                         msl.subinventory_code,
                         NULL locator_id,                    --msl.locator_id,
                         0 quantity_onhand
           FROM mtl_secondary_locators msl
          WHERE     msl.organization_id = p_org_id
                AND msl.subinventory_code = p_subinventory
                AND NOT EXISTS
                       (SELECT *
                          FROM mtl_onhand_quantities_detail moqd
                         WHERE     moqd.inventory_item_id =
                                      msl.inventory_item_id
                               AND moqd.organization_id = msl.organization_id)
                --Added Not Exists Clause on 10/24/2014 for TMS Ticket # 20131016-00470 Exclude items assigned to Cat/Class SP.SPPP from PI�s
                AND NOT EXISTS
                       (SELECT *
                          FROM mtl_category_sets mcs,
                               mtl_categories_kfv mck,
                               mtl_item_categories mic
                         WHERE     mcs.category_set_name =
                                      'Inventory Category'
                               AND mcs.structure_id = mck.structure_id
                               AND mck.concatenated_segments IN ('SP.SPPP')
                               AND mck.category_id = mic.category_id
                               AND mic.inventory_item_id =
                                      msl.inventory_item_id
                               AND mic.organization_id = msl.organization_id);



      VALIDATION_FAILED           EXCEPTION;
      API_FAILURE                 EXCEPTION;

      --Added TMS Ticket 20130809-01349 - 9/6/2013 to exclude repair item types, l_schedule_date for process now, and l_req_id for request_id
      l_item_type                 VARCHAR2 (30);
      l_schedule_date             DATE;
      l_req_id                    NUMBER;
      l_item_transactable         VARCHAR2 (2);      --V1.5 TMS#20151214-00044
   BEGIN
      --Define Savepoint
      SAVEPOINT Process_pi;

      -- Initialize the Out parameter
      x_return_status := 'S';
      x_error_message := NULL;
      --Output the In Parameters
      Write_Log (g_level_statement,
                 l_mod_name,
                 '++++++++++++++In Parameters+++++++++++++');
      Write_Log (g_level_statement,
                 l_mod_name,
                 'p_cycle_count =' || p_cycle_count);
      Write_Log (g_level_statement, l_mod_name, 'p_notes =' || p_notes);
      Write_Log (g_level_statement,
                 l_mod_name,
                 'p_inv_org_id =' || p_inv_org_id);

      /*Write_Log (g_level_statement,
                 l_mod_name,
                 'p_subinventories =' || p_subinventories);
      */
      Write_Log (
         g_level_statement,
         l_mod_name,
            'p_schedule_date ='
         || TO_CHAR (p_schedule_date, 'DD-MON-YYYY HH12:MI:SS AM'));
      --Added TMS Ticket 20130809-01349 - 9/6/2013 P_PROCESS_NOW_FLAG
      Write_Log (g_level_statement,
                 l_mod_name,
                 'p_process_now_flag =' || p_process_now_flag);


      --Added TMS Ticket 20130809-01349 - 9/6/2013  for Process Now Logic for PI
      IF P_PROCESS_NOW_FLAG = 'Y'
      THEN
         l_schedule_date := SYSDATE;
      ELSE
         l_schedule_date := p_schedule_date;
      END IF;

      Write_Log (g_level_statement,
                 l_mod_name,
                 '+++++++++++++++++++++++++++++++++++');

      BEGIN
         SELECT -- NVL (zero_count_flag, 2), --TMS Ticket 20130809-01349 - 9/6/2013 Removed Zero Count Flag from query as we want 0 quantities on physicals
                cycle_count_header_id
           INTO --l_zero_count_flag, --TMS Ticket 20130809-01349 - 9/6/2013 Removed Zero Count Flag from query as we want 0 quantitities on physicals
               l_cycle_count_header_id
           FROM mtl_cycle_count_headers
          WHERE     organization_id = p_inv_org_id
                AND abc_assignment_group_id IS NOT NULL;
      EXCEPTION
         WHEN NO_DATA_FOUND
         THEN
            l_error_msg :=
               'No Cycle count defined for the inv Org :' || p_inv_org_id;
            RAISE VALIDATION_FAILED;
         WHEN OTHERS
         THEN
            l_error_msg := 'Unknown Error Msg :' || SQLERRM;
            RAISE VALIDATION_FAILED;
      END;


      Write_Log (g_level_statement,
                 l_mod_name,
                 'l_cycle_count_header_id=' || l_cycle_count_header_id);


      FOR org IN p_subinventories.FIRST .. p_subinventories.LAST
      LOOP
         i := i + 1;

         FOR r_onhand
            IN c_onhand (
                  p_org_id         => p_inv_org_id,
                  p_subinventory   => p_subinventories (i).subinventory)
         LOOP
            --Added TMS Ticket 20130809-01349 - 9/6/2013 to get Item Type
            BEGIN
               SELECT item_type, mtl_transactions_enabled_flag
                 INTO l_item_type, l_item_transactable --V1.5 TMS#20151214-00044
                 FROM mtl_system_items_b msib
                WHERE     r_onhand.inventory_item_id = msib.inventory_item_id
                      AND r_onhand.organization_id = msib.organization_id;
            EXCEPTION
               WHEN OTHERS
               THEN
                  l_item_type := NULL;
            END;


            IF l_item_type != 'REPAIR' --Added TMS Ticket 20130809-01349 - 9/6/2013 - if item type = REPAIR, then don't insert into cycle count schedule
                                      AND l_item_transactable = 'Y' --V1.5 TMS#20151214-00044
            THEN
               l_zero_count_flag := 1; ----TMS Ticket 20130809-01349 - 9/6/2013 Setting Zero Count flag to 1 for PI so records that don't have on-hand appear on PI count sheet

               Write_Log (g_LEVEL_STATEMENT,
                          l_mod_name,
                          '*******onhand qty Loop*******');

               -- Get a new cycle count schedule ID
               SELECT mtl_cc_schedule_requests_s.NEXTVAL
                 INTO l_cycle_count_schedule_id
                 FROM DUAL;

               INSERT INTO mtl_cc_schedule_requests (cycle_count_schedule_id,
                                                     last_update_date,
                                                     last_updated_by,
                                                     creation_date,
                                                     created_by,
                                                     cycle_count_header_id,
                                                     request_source_type,
                                                     zero_count_flag,
                                                     schedule_date,
                                                     schedule_status,
                                                     subinventory,
                                                     locator_id,
                                                     inventory_item_id)
                    VALUES (l_cycle_count_schedule_id,
                            SYSDATE,
                            FND_GLOBAL.USER_ID,
                            SYSDATE,
                            FND_GLOBAL.USER_ID,
                            l_cycle_count_header_id,
                            2,
                            l_zero_count_flag,              --zero_count_flag,
                            l_schedule_date,
                            1,
                            r_onhand.subinventory_code,
                            r_onhand.locator_id,
                            r_onhand.inventory_item_id);

               Write_Log (
                  g_LEVEL_STATEMENT,
                  l_mod_name,
                     'Num of Rows Inserted:(mtl_cc_schedule_requests) '
                  || SQL%ROWCOUNT);

               Write_Log (g_LEVEL_STATEMENT,
                          l_mod_name,
                          '******End onhand qty Loop********');

               NULL;
            END IF; --Added TMS Ticket 20130809-01349 - 9/6/2013  for ITEM TYPE REPAIR IF Clause
         END LOOP;

         NULL;
      END LOOP;

      IF x_return_status <> 'S'
      THEN
         RAISE API_FAILURE;
      ELSE
         COMMIT;
      END IF;

      COMMIT;

      --Added TMS Ticket 20130809-01349 - 9/6/2013 for Process Now - we need to submit the scheduler if process now flag is set to yes for the org selected

      IF p_process_now_flag = 'Y'
      THEN
         Write_Log (g_LEVEL_STATEMENT,
                    l_mod_name,
                    'Submitting Generate count requests concurrent program');
         -- Call the concurrent program to generate count requests
         l_req_id :=
            fnd_request.submit_request (
               application   => 'INV',
               program       => 'INCACG',
               argument1     => '2',
               argument2     => TO_CHAR (l_cycle_count_header_id),
               argument3     => TO_CHAR (p_inv_org_id));
         Write_Log (g_LEVEL_STATEMENT, l_mod_name, ' l_req_id :' || l_req_id);

         -- Check for errors
         IF (l_req_id <= 0 OR l_req_id IS NULL)
         THEN
            Write_Log (
               g_LEVEL_STATEMENT,
               l_mod_name,
               'Error in calling the generate count requests program');
            RAISE FND_API.G_EXC_ERROR;
         ELSE
            COMMIT;
         END IF;
      END IF;
   EXCEPTION
      WHEN VALIDATION_FAILED
      THEN
         Write_Log (g_LEVEL_STATEMENT, l_mod_name, l_error_msg);
      --RAISE VALIDATION_FAILED;

      WHEN API_FAILURE
      THEN
         ROLLBACK TO Process_automatic;
         x_return_status := 'E';
         Write_Log (g_level_statement,
                    l_mod_name,
                    'error_msg )=' || x_error_message);
      WHEN OTHERS
      THEN
         ROLLBACK TO Process_automatic;
         x_return_status := 'E';
         x_error_message := SQLERRM;
         Write_Log (g_level_statement,
                    l_mod_name,
                    'error_msg )=' || x_error_message);
   END PROCESS_PI;

   PROCEDURE INCACI_ALL_ORGS_CCR (retbuf        OUT VARCHAR2,
                                  retcode       OUT NUMBER,
                                  p_org_id   IN     NUMBER)
   IS
      CURSOR c_orgs
      IS
           SELECT organization_id, organization_code
             FROM org_organization_definitions ood
            WHERE     organization_code = SUBSTR (organization_name, 1, 3)
                  AND operating_unit = p_org_id
                  AND EXISTS
                         (SELECT *
                            FROM hr_all_organization_units haou
                           WHERE     haou.organization_Id = ood.organization_id
                                 AND NVL (haou.date_to, SYSDATE + 1) > SYSDATE)
         ORDER BY organization_code;

      l_cycle_count_header_id     NUMBER;
      l_abc_assignment_group_id   NUMBER;
      l_request_id                NUMBER;
      l_description               VARCHAR2 (100);
      l_abc_class_id              NUMBER;
      l_message                   VARCHAR2 (2000);
	  ln_count                    NUMBER;      --Added by Ashwin.S on 25-Jan-2018 for TMS#20161014-00158
	  lv_class                    VARCHAR2(5); --Added by Ashwin.S on 25-Jan-2018 for TMS#20161014-00158

      CURSOR c_missing_items (
         p_organization_id       IN NUMBER,
         p_assignment_group_id   IN NUMBER)
      IS
         SELECT inventory_item_id
           FROM mtl_system_items_b msib
          WHERE     msib.organization_id = p_organization_id
                AND NVL (msib.cycle_count_enabled_flag, 'N') = 'Y'
                AND NVL (msib.mtl_transactions_enabled_flag, 'N') = 'Y'
                AND NVL (msib.stock_enabled_flag, 'N') = 'Y'
                AND NOT EXISTS
                       (SELECT *
                          FROM mtl_abc_assignments maa
                         WHERE     maa.inventory_item_id =
                                      msib.inventory_item_id
                               AND maa.assignment_group_id =
                                      p_assignment_group_id);
   BEGIN
      FOR r_orgs IN c_orgs
      LOOP
         BEGIN
            SELECT cycle_count_header_id, abc_assignment_group_id
              INTO l_cycle_count_header_id, l_abc_assignment_group_id
              FROM mtl_cycle_count_headers
             WHERE     organization_id = r_orgs.organization_id
                   AND NVL (disable_date, SYSDATE + 1) > SYSDATE;


            l_description :=
                  'XXWC '
               || r_orgs.organization_code
               || ' Initialize cycle count items';
         EXCEPTION
            WHEN OTHERS
            THEN
               l_cycle_count_header_id := NULL;
               l_abc_assignment_group_id := NULL;
         END;

        /* Commented by Ashwin.S on 25-Jan-2018 for TMS#20161014-00158
         BEGIN
            SELECT abc_class_id
              INTO l_abc_class_id
              FROM mtl_abc_classes
             WHERE organization_id = r_orgs.organization_id;
         EXCEPTION
            WHEN OTHERS
            THEN
               l_abc_class_id := NULL;
         END;
        */

		--Added by Ashwin.S on 25-Jan-2018 for TMS#20161014-00158
		BEGIN
		
            SELECT COUNT(abc_class_id)
              INTO ln_count
              FROM mtl_abc_classes
             WHERE organization_id = r_orgs.organization_id;
			 
         EXCEPTION
         WHEN others THEN
		 
           ln_count := 0;
			   
         END;
		 
         --IF l_abc_class_id IS NOT NULL THEN Commented by Ashwin.S on 25-Jan-2018 for TMS#20161014-00158
		 
		 IF ln_count>0 THEN --Added by Ashwin.S on 25-Jan-2018 for TMS#20161014-00158
		 
            FOR r_missing_items
               IN c_missing_items (r_orgs.organization_id,
                                   l_abc_assignment_group_id)
            LOOP
			
	    --Added by Ashwin.S on 25-Jan-2018 for TMS#20161014-00158
	    BEGIN
  
          SELECT flv.DESCRIPTION
          INTO   lv_class	  
          FROM   MTL_ITEM_CATEGORIES_V MICV
          ,      FND_LOOKUP_VALUES_VL  flv
          WHERE  1=1 
          AND    CATEGORY_SET_NAME = 'Sales Velocity'
          AND    ORGANIZATION_ID   =  r_orgs.organization_id
          AND    INVENTORY_ITEM_ID =  r_missing_items.INVENTORY_ITEM_ID
          AND    FLV.LOOKUP_TYPE   ='XXWC_CYCLE_COUNT_VELOCITY_LKP'
          AND    flv.LOOKUP_CODE   = micv.category_concat_segs;
      
        EXCEPTION
        WHEN others THEN
        
          lv_class:='B';
          
        END;
		
		--Added by Ashwin.S on 25-Jan-2018 for TMS#20161014-00158
		BEGIN
		
		  SELECT abc_class_id
		  INTO   l_abc_class_id
          FROM   MTL_ABC_CLASSES 
          WHERE  ORGANIZATION_ID=r_orgs.organization_id
          AND    abc_class_name=lv_class;
		  
        EXCEPTION
        WHEN others THEN
        
          l_abc_class_id:=NULL;
		
		END;
		
               BEGIN
                  INSERT INTO mtl_abc_assignments (inventory_item_id,
                                                   assignment_group_id,
                                                   abc_class_id,
                                                   last_update_date,
                                                   last_updated_by,
                                                   creation_date,
                                                   created_by,
                                                   request_id)
                       VALUES (r_missing_items.inventory_item_id,
                               l_abc_assignment_group_id,
                               l_abc_class_id,
                               SYSDATE,
                               FND_GLOBAL.USER_ID,
                               SYSDATE,
                               FND_GLOBAL.USER_ID,
                               FND_GLOBAL.CONC_REQUEST_ID);
               EXCEPTION
                  WHEN OTHERS
                  THEN
                     l_message := SUBSTR (SQLCODE || SQLERRM, 1, 2000);
                     fnd_file.put_line (fnd_file.LOG, l_message);
                     ROLLBACK;
               END;
            END LOOP;

            COMMIT;
			
         END IF;

         IF     l_cycle_count_header_id IS NOT NULL
            AND l_abc_assignment_group_id IS NOT NULL
         THEN
            --Reinitialize
            l_request_id :=
               fnd_request.submit_request (
                  application   => 'INV',
                  PROGRAM       => 'INCACI',
                  description   => l_description,
                  start_time    => '',
                  sub_request   => FALSE,
                  argument1     => l_cycle_count_header_id --Cycle Count Header Id
                                                          ,
                  argument2     => l_abc_assignment_group_id -- ABC Assignment Group Id
                                                            ,
                  argument3     => 1,
                  argument4     => 0,
                  argument5     => 0);
         ELSE
            NULL;
         END IF;

         COMMIT;
      END LOOP;

      retcode := 0;
   END INCACI_ALL_ORGS_CCR;

   /**************************************************************************************************************************************
       *   Procedure : cycle_count_single_submit
       *
       *   PURPOSE:   This procedure used to run the inventory counts for all orgs or passed organization
       *   Parameter:
       *          IN
       *              p_inv_org_id -- Cycle Count Specific Line Id
       *   Ver        Date        Author                     Description
       *   ---------  ----------  ---------------         ------------------------------------------------------------------------------
       *   1.6        06/17/2016  Pattabhi Avula          TMS#20160708-00169 - Batch improvements for XXWC Cycle Count Scheduler for All Orgs
       * **************************************************************************************************************************************/

   PROCEDURE cycle_count_single_submit (errbuf            OUT VARCHAR2,
                                        retcode           OUT VARCHAR2,
                                        p_inv_org_id   IN     NUMBER)
   IS
      CURSOR C_all_org
      IS
         SELECT DISTINCT a.inv_org_id
           FROM xxwc_inv_cyclecount_lines a, xxwc_inv_cyclecount_hdr b
          WHERE     a.cyclecount_hdr_id = b.cyclecount_hdr_id
                AND b.process_status = 'ACTIVE'
                AND a.process_status = 'ACTIVE'
                AND a.inv_org_id = NVL (p_inv_org_id, a.inv_org_id)
         UNION
         SELECT DISTINCT lines.inv_org_id
           FROM xxwc_inv_cc_specific_lines lines, xxwc_inv_cyclecount_hdr hdr
          WHERE     lines.cyclecount_hdr_id = hdr.cyclecount_hdr_id
                AND lines.process_status != 'COMPLETE'
                AND lines.process_date <= SYSDATE
                AND lines.inv_org_id = NVL (p_inv_org_id, lines.inv_org_id);


      lx_ret_code     VARCHAR2 (10);
      lx_error_buff   VARCHAR2 (1000);
      l_mod_name      VARCHAR2 (100)
         := 'xxwc_inv_cycle_count_pkg.cycle_count_single_submit';
   BEGIN
      FOR I IN C_all_org
      LOOP
         XXWC_INV_CYCLE_COUNT_PKG.SUBMIT_SCHEDULER (lx_error_buff,
                                                    lx_ret_code,
                                                    i.inv_org_id);

         IF NVL (lx_ret_code, 0) = '1'
         THEN
            retcode := '1';
            g_retcode := '1';

            Write_Log (g_LEVEL_STATEMENT,
                       l_mod_name,
                       'Org_id =' || i.inv_org_id);

            Write_Log (g_LEVEL_STATEMENT,
                       l_mod_name,
                       'lx_error_msg =' || lx_error_buff);

            fnd_file.put_line (
               fnd_file.LOG,
               i.inv_org_id || ' Org Process completed With Warning Status');
         ELSIF NVL (lx_ret_code, 0) = '2'
         THEN
            retcode := '2';
            g_retcode := '2';

            Write_Log (g_LEVEL_STATEMENT,
                       l_mod_name,
                       'Org_id =' || i.inv_org_id);

            Write_Log (g_LEVEL_STATEMENT,
                       l_mod_name,
                       'lx_error_msg =' || lx_error_buff);

            fnd_file.put_line (
               fnd_file.LOG,
               i.inv_org_id || ' Org Process completed With Error Status');
         ELSE
            Write_Log (g_LEVEL_STATEMENT,
                       l_mod_name,
                       'Org_id =' || i.inv_org_id);
            fnd_file.put_line (
               fnd_file.LOG,
               i.inv_org_id || ' Org Process completed successfully');
         END IF;
      END LOOP;

      retcode := g_retcode;
      errbuf := lx_error_buff;
   EXCEPTION
      WHEN NO_DATA_FOUND
      THEN
         errbuf := lx_error_buff;
         retcode := '2';
      WHEN OTHERS
      THEN
         errbuf := SQLERRM;
         retcode := '2';
   END CYCLE_COUNT_SINGLE_SUBMIT;
END XXWC_INV_CYCLE_COUNT_PKG;
/