CREATE GLOBAL TEMPORARY TABLE XXWC.XXWCINV_INV_AGING_OH_TEMP
/***************************************************************************
    $Header XXWC.XXWCINV_INV_AGING_OH_TEMP $
    Module Name: XXWCINV_INV_AGING_OH_TEMP
    PURPOSE: Temp table used for Inventory Branch Aging Report
 
    REVISIONS:
    Ver    Date       	Author                Description
    ------ ---------  	------------------    ----------------
     1.0    16-DEC-2015    Lee Spitzer         TMS # 20150928-00193 Branch Aging Report
/***************************************************************************/

(inventory_item_id NUMBER
,organization_id NUMBER
,orig_date_received DATE
,primary_transaction_quantity NUMBER
,subinventory_code VARCHAR2(10)
,is_consigned NUMBER
,onhand_quantities_id NUMBER
,ORG_ID NUMBER DEFAULT NVL(TO_NUMBER(DECODE(SUBSTRB(USERENV('CLIENT_INFO'),1,1), ' ', NULL,SUBSTRB(USERENV('CLIENT_INFO'),1,10))),-99)
) ON COMMIT PRESERVE ROWS 
;