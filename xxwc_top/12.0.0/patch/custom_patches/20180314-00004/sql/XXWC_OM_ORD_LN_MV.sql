DROP MATERIALIZED VIEW APPS.XXWC_OM_ORD_LN_MV;

CREATE MATERIALIZED VIEW APPS.XXWC_OM_ORD_LN_MV
(
   ACTUAL_SHIPMENT_DATE,
   ATTRIBUTE5,
   ATTRIBUTE7,
   CUST_PO_NUMBER,
   HEADER_ID,
   INVENTORY_ITEM_ID,
   LAST_UPDATE_DATE,
   LINE_CATEGORY_CODE,
   LINE_ID,
   LINK_TO_LINE_ID,
   REFERENCE_LINE_ID,
   ORDERED_QUANTITY,
   PRICING_DATE,
   SHIP_FROM_ORG_ID,
   SHIPPING_METHOD_CODE,
   SOURCE_TYPE_CODE,
   UNIT_COST,
   UNIT_LIST_PRICE,
   HAS_DROPSHIP_CNT,
   ORDER_TYPE_ID,
   MSIB_ITEM_NUMBER,
   MSIB_ITEM_DESCRIPTION,
   MSIB_INVENTORY_ITEM_ID,
   UNIT_SELLING_PRICE,
   SOLD_TO_ORG_ID,
   SHIP_TO_ORG_ID,
   LINE_GM_PERCENT,
   ORDER_NUMBER,
   LINE_CREATION_DATE,
   FLOW_STATUS_CODE
)
   BUILD IMMEDIATE
   REFRESH
      COMPLETE
      ON DEMAND
      WITH PRIMARY KEY
AS
    /*
     $Header XXWC_OM_ORD_LN_MV $
     Module Name: XXWC_OM_ORD_LN_MV.sql

     PURPOSE:   Mobile Quote Form 0.2 Release

     REVISIONS:
     Ver        Date        Author                     Description
     ---------  ----------  ---------------         -------------------------
	 2.0        4/24/2017   Rakesh Patel            TMS#20170417-00208-Mobile Quote Form 0.2 Release - added get district by ntid function
	 3.0        5/17/2017   Rakesh Patel            TMS#20170510-00120-XXWC_OM_ORD_LN_MV refresh performance issue  
    */
   WITH order_lines
        AS
          (SELECT /*+ qb_name(ord_ln) materialize */
                 l1.actual_shipment_date,
                  l1.attribute5,
                  l1.attribute7,
                  l1.cust_po_number,
                  l1.header_id,
                  l1.inventory_item_id,
                  l1.last_update_date,
                  l1.line_category_code,
                  l1.line_id,
                  l1.link_to_line_id, ----TMS#20170510-00120-XXWC_OM_ORD_LN_MV refresh performance issue
                  l1.reference_line_id,
                  l1.ordered_quantity,
                  l1.org_id,
                  l1.pricing_date,
                  l1.ship_from_org_id,
                  l1.shipping_method_code,
                  l1.source_type_code,
                  l1.unit_cost,
                  l1.unit_list_price,
                  CASE
                     WHEN     NVL (l1.booked_flag, 'N') = 'Y' 
                          AND NVL (l1.cancelled_flag, 'N') = 'N'
                          AND l1.source_type_code = 'EXTERNAL'
                     THEN
                        'Y'
                     ELSE
                        'N'
                  END
                     has_dropship_cnt,
				  l1.unit_selling_price,
				  l1.flow_status_code,
				  TRUNC(l1.creation_date) line_creation_date
             FROM ont.oe_order_lines_all l1
  		    WHERE TRUNC(l1.creation_date) >= TRUNC(SYSDATE) -90 )--TMS#20170510-00120-XXWC_OM_ORD_LN_MV refresh performance issue
   SELECT l.actual_shipment_date,
          l.attribute5,
          l.attribute7,
          l.cust_po_number,
          l.header_id,
          l.inventory_item_id,
          l.last_update_date,
          l.line_category_code,
          l.line_id,
          l.link_to_line_id,
          l.reference_line_id,
          l.ordered_quantity,
          l.pricing_date,
          l.ship_from_org_id,
          l.shipping_method_code,
          l.source_type_code,
          l.unit_cost,
          l.unit_list_price,
		  l.has_dropship_cnt,
          h.order_type_id,
          msib.segment1 AS msib_item_number,
          msib.description AS msib_item_description,
          msib.inventory_item_id AS msib_inventory_item_id,
		  l.unit_selling_price,
		  h.sold_to_org_id,
          h.ship_to_org_id,
		  DECODE(l.unit_selling_price, 0, 0, (ROUND((l.unit_selling_price -  l.unit_cost)
                                                    / l.unit_selling_price*100
                                                 ,2)
                                 )
                 ) line_gm_percent,
          h.order_number,
          l.line_creation_date,
          l.flow_status_code
     FROM order_lines l
          INNER JOIN ont.oe_order_headers_all h
             ON l.header_id = h.header_id AND l.org_id = h.org_id
          LEFT OUTER JOIN order_lines rma_ln
             ON     l.link_to_line_id = rma_ln.line_id
                AND rma_ln.line_category_code = 'RETURN'
          LEFT OUTER JOIN order_lines shp_ln
             ON     rma_ln.link_to_line_id = shp_ln.line_id
                AND shp_ln.line_category_code = 'ORDER'
          LEFT OUTER JOIN inv.mtl_system_items_b msib
             ON     shp_ln.inventory_item_id = msib.inventory_item_id
                AND shp_ln.ship_from_org_id = msib.organization_id;


COMMENT ON MATERIALIZED VIEW APPS.XXWC_OM_ORD_LN_MV IS 'snapshot table for snapshot APPS.XXWC_OM_ORD_LN_MV';