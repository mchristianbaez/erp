/*************************************************************************
  $Header TMS_20170502-00018_Cancel_PO.sql $
  Module Name: TMS_20170502-00018   Data Script Fix to cancel Po#2135250

  PURPOSE: Data Script Fix to cancel Po#2135250

  REVISIONS:
  Ver        Date         Author                Description
  ---------  -----------  ------------------    --------------------------
  1.0        15-MAY-2017  Sundaramoorthy        TMS#20170502-00018

**************************************************************************/ 
SET SERVEROUTPUT ON
SET VERIFY OFF;

set serveroutput on size 100000;

create table xxwc.xxwc_pha_3_15117442641 as select * from po_headers_all where po_header_id = 3504206 and org_id = 162;
/
DECLARE
 x_progress varchar2(500);
 x_cont varchar2(10);
 x_active_wf_exists varchar2(1);
  
BEGIN


UPDATE po_Headers_All ph
SET ph.Revision_num =
(SELECT MAX (Revision_num)
FROM (SELECT NVL (MAX (pha.Revision_num), 0) + 1 Revision_num
FROM po_Headers_Archive_All pha
WHERE pha.po_Header_Id = 3504206
UNION
SELECT NVL (MAX (Pla.Revision_num), 0) + 1 Revision_num
FROM po_Lines_Archive_All Pla
WHERE Pla.po_Header_Id = 3504206
UNION
SELECT NVL (MAX (Plla.Revision_num), 0) + 1 Revision_num
FROM po_Line_Locations_Archive_All Plla
WHERE Plla.po_Header_Id = 3504206
UNION
SELECT NVL (MAX (pda.Revision_num), 0) + 1 Revision_num
FROM po_Distributions_Archive_All pda
WHERE pda.po_Header_Id = 3504206))
WHERE ph.po_Header_Id = 3504206 AND org_id = 162;

DBMS_OUTPUT.put_line ('TMS: TMS#20170502-00018  , Updated');

COMMIT;

EXCEPTION
WHEN OTHERS THEN
  dbms_output.put_line('some exception occured '||sqlerrm||' rolling back'||x_progress);
  rollback;
END;
/