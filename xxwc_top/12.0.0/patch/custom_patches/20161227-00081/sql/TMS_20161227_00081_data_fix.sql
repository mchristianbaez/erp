/*************************************************************************
    $Header TMS_20161227_00081_data_fix.sql $
     Module Name: TMS_20161227_00081_data_fix
   
     PURPOSE:   Data fix scrip to delete duplicate records from gl_interface.

     REVISIONS:
     Ver        Date        Author                             Description
     ---------  ----------  -------------------------------    -------------------------
     1.0        12/27/2016  Rakesh Patel                       TMS#20161227_00081- GSC S522846 Delete duplicate records in GL_Interface table for PeopleSoft source 
**************************************************************************/

SET SERVEROUTPUT ON SIZE 1000000
DECLARE
BEGIN
    apps.mo_global.set_policy_context('S',162); 

   	DBMS_OUTPUT.put_line ('TMS_20161227_00081 , Script 1 -Before Insert');
	
	DELETE FROM apps.gl_interface 
	WHERE date_created LIKE TO_DATE('12/27/2016','MM/DD/YYYY') 
	AND USER_JE_SOURCE_NAME = 'PeopleSoft';
	
	DBMS_OUTPUT.put_line ('TMS_20161227_00081, After Delete, records Deleted from gl_interface : '||sql%rowcount);
   
    COMMIT;
EXCEPTION
   WHEN OTHERS
   THEN
      ROLLBACK;
      DBMS_OUTPUT.put_line ('TMS_20161227_00081, Errors ='||SQLERRM);
END;
/