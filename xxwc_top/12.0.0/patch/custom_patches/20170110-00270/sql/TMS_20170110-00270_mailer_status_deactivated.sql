/*************************************************************************
  $Header TMS_20170110-00270_mailer_status_deactivated.sql $
  Module Name: TMS_20170110-00270   EM Event: Critical:EBSPRD - The value of Mailer_ Status is DEACTIVATED_SYSTEM

  PURPOSE: Data fix to update the mailer_status_deactivated

  REVISIONS:
  Ver        Date         Author                Description
  ---------  -----------  ------------------    --------------------------
  1.0        21-FEB-2017  Pattabhi Avula         TMS#20170110-00270 

**************************************************************************/ 


SET SERVEROUTPUT ON SIZE 100000;
SET VERIFY OFF;

DECLARE
BEGIN

DBMS_OUTPUT.put_line ('TMS: 20170110-00270    , Before Update');

    UPDATE WF_NOTIFICATIONS 
	   SET mail_status = 'SENT' 
     WHERE mail_status IN ('MAIL','INVALID')
       AND Status IN ('OPEN', 'CANCELED')
       AND begin_date < SYSDATE-10;



DBMS_OUTPUT.put_line (
         'TMS: 20170110-00270 - number of records updated: '
      || SQL%ROWCOUNT);


COMMIT;

   DBMS_OUTPUT.put_line ('TMS: 20170110-00270   , End Update');
   
END; 
/ 