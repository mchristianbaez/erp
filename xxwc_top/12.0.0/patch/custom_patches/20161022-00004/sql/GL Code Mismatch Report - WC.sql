--Report Name            : GL Code Mismatch Report - WC
--Import Instructions    : Run this file in XXEIS user from SQL*Plus or any tool like Toad, SQL Navigator. 
prompt Creating Object Data EIS_XXWC_INV_GL_MISMATCH_V
set scan off define off
DECLARE
mod_exist VARCHAR2(1);
BEGIN 
mod_exist := XXEIS.EIS_RSC_INSERT.check_if_module_exist(401);
IF mod_exist = 'Y' THEN 
--Inserting Object EIS_XXWC_INV_GL_MISMATCH_V
xxeis.eis_rsc_ins.v( 'EIS_XXWC_INV_GL_MISMATCH_V',401,'','','','','ANONYMOUS','XXEIS','Eis Xxwc Item Gl Mismatch V','EXIGMV','','','VIEW','US','','');
--Delete Object Columns for EIS_XXWC_INV_GL_MISMATCH_V
xxeis.eis_rsc_utility.delete_view_rows('EIS_XXWC_INV_GL_MISMATCH_V',401,FALSE);
--Inserting Object Columns for EIS_XXWC_INV_GL_MISMATCH_V
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_INV_GL_MISMATCH_V','EXPENSE_ACCOUNT',401,'Expense Account','EXPENSE_ACCOUNT','','','','ANONYMOUS','VARCHAR2','','','Expense Account','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_INV_GL_MISMATCH_V','SALES_DEFAULT',401,'Sales Default','SALES_DEFAULT','','','','ANONYMOUS','VARCHAR2','','','Sales Default','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_INV_GL_MISMATCH_V','EXPENSE_ACCOUNT_FULL',401,'Expense Account Full','EXPENSE_ACCOUNT_FULL','','','','ANONYMOUS','VARCHAR2','','','Expense Account Full','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_INV_GL_MISMATCH_V','SALES_ACCOUNT',401,'Sales Account','SALES_ACCOUNT','','','','ANONYMOUS','VARCHAR2','','','Sales Account','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_INV_GL_MISMATCH_V','SALES_ACCOUNT_FULL',401,'Sales Account Full','SALES_ACCOUNT_FULL','','','','ANONYMOUS','VARCHAR2','','','Sales Account Full','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_INV_GL_MISMATCH_V','COGS_DEFAULT',401,'Cogs Default','COGS_DEFAULT','','','','ANONYMOUS','VARCHAR2','','','Cogs Default','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_INV_GL_MISMATCH_V','COGS_ACCOUNT',401,'Cogs Account','COGS_ACCOUNT','','','','ANONYMOUS','VARCHAR2','','','Cogs Account','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_INV_GL_MISMATCH_V','COGS_ACCOUNT_FULL',401,'Cogs Account Full','COGS_ACCOUNT_FULL','','','','ANONYMOUS','VARCHAR2','','','Cogs Account Full','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_INV_GL_MISMATCH_V','ITEM_CREATED_BY',401,'Item Created By','ITEM_CREATED_BY','','','','ANONYMOUS','VARCHAR2','','','Item Created By','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_INV_GL_MISMATCH_V','CREATION_DATE',401,'Creation Date','CREATION_DATE','','','','ANONYMOUS','DATE','','','Creation Date','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_INV_GL_MISMATCH_V','CATEGORY_CLASS_DESC',401,'Category Class Desc','CATEGORY_CLASS_DESC','','','','ANONYMOUS','VARCHAR2','','','Category Class Desc','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_INV_GL_MISMATCH_V','CATEGORY_CLASS',401,'Category Class','CATEGORY_CLASS','','','','ANONYMOUS','VARCHAR2','','','Category Class','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_INV_GL_MISMATCH_V','ITEM_TYPE',401,'Item Type','ITEM_TYPE','','','','ANONYMOUS','VARCHAR2','','','Item Type','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_INV_GL_MISMATCH_V','ITEM_STATUS',401,'Item Status','ITEM_STATUS','','','','ANONYMOUS','VARCHAR2','','','Item Status','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_INV_GL_MISMATCH_V','ORG',401,'Org','ORG','','','','ANONYMOUS','VARCHAR2','','','Org','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_INV_GL_MISMATCH_V','DESCRIPTION',401,'Description','DESCRIPTION','','','','ANONYMOUS','VARCHAR2','','','Description','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_INV_GL_MISMATCH_V','ITEM_NUM',401,'Item Num','ITEM_NUM','','','','ANONYMOUS','VARCHAR2','','','Item Num','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_INV_GL_MISMATCH_V','LAST_UPDATE_DATE',401,'Last Update Date','LAST_UPDATE_DATE','','','','ANONYMOUS','DATE','','','Last Update Date','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_INV_GL_MISMATCH_V','ORG_ACT_Y_N',401,'Org Act Y N','ORG_ACT_Y_N','','','','ANONYMOUS','VARCHAR2','','','Org Act Y N','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_INV_GL_MISMATCH_V','EXPENSE_DEFAULT',401,'Expense Default','EXPENSE_DEFAULT','','','','ANONYMOUS','VARCHAR2','','','Expense Default','','','','US');
--Inserting Object Components for EIS_XXWC_INV_GL_MISMATCH_V
--Inserting Object Component Joins for EIS_XXWC_INV_GL_MISMATCH_V
ELSE
RAISE_APPLICATION_ERROR (-20001,'Module does not exist. Import/Create the module with id 401');
END IF;
END;
/
prompt Creating Report LOV Data for GL Code Mismatch Report - WC
set scan off define off
DECLARE
mod_exist VARCHAR2(1);
BEGIN 
mod_exist := XXEIS.EIS_RSC_INSERT.check_if_module_exist(401);
IF mod_exist = 'Y' THEN 
--Inserting Report LOVs - GL Code Mismatch Report - WC
xxeis.eis_rsc_ins.lov( 401,'SELECT DISTINCT concatenated_segments Item_Category,
                description
           FROM mtl_categories_kfv
           order by concatenated_segments','','EIS_INV_ITEM_CATEGORIES_LOV','Item Categories','XXEIS_RS_ADMIN',NULL,'','','','','','','','US');
xxeis.eis_rsc_ins.lov( 401,'SELECT DISTINCT concatenated_segments item, description FROM mtl_system_items_kfv msi,
                org_organization_definitions ood
          WHERE msi.organization_id = ood.organization_id
            AND ood.operating_unit = fnd_profile.VALUE (''ORG_ID'')
       ORDER BY concatenated_segments','','EIS_INV_ITEM_LOV','List of all inventory items.','XXEIS_RS_ADMIN',NULL,'N','','','','','','','US');
xxeis.eis_rsc_ins.lov( 401,'SELECT meaning
FROM mfg_lookups
WHERE lookup_type = ''SYS_YES_NO''','','EIS_INV_YES_NO_LOV','YES NO LOV','XXEIS_RS_ADMIN',NULL,'','','','','','','','US');
xxeis.eis_rsc_ins.lov( 401,'select list_name from XXWC_PARAM_LIST where list_type = ''Item'' and (PUBLIC_FLAG =''Y'' OR USER_ID = fnd_global.user_id)','','XXWC Item List','','ANONYMOUS',NULL,'N','','','','','','','US');
xxeis.eis_rsc_ins.lov( 401,'select distinct organization_code
from apps.mtl_parameters','','XXWC Organization Code','Organization Code from the mtl_parameters table','ID020048',NULL,'N','','','','','','','US');
xxeis.eis_rsc_ins.lov( 401,'SELECT COGS.segment4 cogs_account
FROM mtl_system_items_b MSIB ,
  apps.gl_code_combinations_kfv COGS
WHERE 1                              =1
AND MSIB.COST_OF_SALES_ACCOUNT       = cogs.code_combination_id
AND MSIB.INVENTORY_ITEM_STATUS_CODE <> ''Inactive''
GROUP BY COGS.segment4
ORDER BY 1','','XXWC INV COGS ACCOUNT','This  will provide list of cogs accounts','ANONYMOUS',NULL,'N','','','','','','','US');
xxeis.eis_rsc_ins.lov( 401,'SELECT SALES.segment4 sales_account
FROM mtl_system_items_b MSIB ,
  apps.gl_code_combinations_kfv SALES
WHERE 1                              =1
AND MSIB.sales_account               = sales.code_combination_id
AND MSIB.inventory_item_status_code <> ''Inactive''
GROUP BY SALES.segment4
ORDER BY 1','','XXWC INV SALES ACCOUNT','This will proved list of sales accounts','ANONYMOUS',NULL,'N','','','','','','','US');
xxeis.eis_rsc_ins.lov( 401,'SELECT EXPEN.segment4 expense_account
FROM mtl_system_items_b MSIB ,
  apps.gl_code_combinations_kfv EXPEN
WHERE 1                              =1
AND MSIB.expense_account             = EXPEN.code_combination_id
AND MSIB.inventory_item_status_code <> ''Inactive''
GROUP BY EXPEN.segment4
ORDER BY 1','','XXWC INV EXPENSE ACCOUNT','This will provide list of expense accounts','ANONYMOUS',NULL,'N','','','','','','','US');
null;
ELSE
RAISE_APPLICATION_ERROR (-20001,'Module does not exist. Import/Create the module with id 401');
END IF;
END;
/
prompt Creating Report Data for GL Code Mismatch Report - WC
set scan off define off
DECLARE
mod_exist varchar2(1);
BEGIN 
mod_exist := XXEIS.EIS_RSC_INSERT.check_if_module_exist(401);
IF mod_exist = 'Y' THEN 
--Deleting Report data - GL Code Mismatch Report - WC
xxeis.eis_rsc_utility.delete_report_rows( 'GL Code Mismatch Report - WC' );
--Inserting Report - GL Code Mismatch Report - WC
xxeis.eis_rsc_ins.r( 401,'GL Code Mismatch Report - WC','','To provide a GL audit report between MST and Child Orgs for data governance improvement.','','','','SA059956','EIS_XXWC_INV_GL_MISMATCH_V','Y','','','SA059956','','N','White Cap Reports','','CSV,EXCEL,','N','','','','','','N','','US','','','','');
--Inserting Report Columns - GL Code Mismatch Report - WC
xxeis.eis_rsc_ins.rc( 'GL Code Mismatch Report - WC',401,'CATEGORY_CLASS','Category Class','Category Class','','','default','','6','N','Y','','','','','','','SA059956','N','N','','EIS_XXWC_INV_GL_MISMATCH_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'GL Code Mismatch Report - WC',401,'CATEGORY_CLASS_DESC','Category Class Desc','Category Class Desc','','','default','','7','N','Y','','','','','','','SA059956','N','N','','EIS_XXWC_INV_GL_MISMATCH_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'GL Code Mismatch Report - WC',401,'COGS_ACCOUNT','Cogs Account','Cogs Account','','','default','','11','N','Y','','','','','','','SA059956','N','N','','EIS_XXWC_INV_GL_MISMATCH_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'GL Code Mismatch Report - WC',401,'COGS_ACCOUNT_FULL','Cogs Account Full','Cogs Account Full','','','default','','10','N','Y','','','','','','','SA059956','N','N','','EIS_XXWC_INV_GL_MISMATCH_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'GL Code Mismatch Report - WC',401,'COGS_DEFAULT','Cogs Default','Cogs Default','','','default','','12','N','Y','','','','','','','SA059956','N','N','','EIS_XXWC_INV_GL_MISMATCH_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'GL Code Mismatch Report - WC',401,'CREATION_DATE','Creation Date','Creation Date','','','default','','8','N','Y','','','','','','','SA059956','N','N','','EIS_XXWC_INV_GL_MISMATCH_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'GL Code Mismatch Report - WC',401,'DESCRIPTION','Description','Description','','','default','','2','N','Y','','','','','','','SA059956','N','N','','EIS_XXWC_INV_GL_MISMATCH_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'GL Code Mismatch Report - WC',401,'EXPENSE_ACCOUNT','Expense Account','Expense Account','','','default','','17','N','Y','','','','','','','SA059956','N','N','','EIS_XXWC_INV_GL_MISMATCH_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'GL Code Mismatch Report - WC',401,'EXPENSE_ACCOUNT_FULL','Expense Account Full','Expense Account Full','','','default','','16','N','Y','','','','','','','SA059956','N','N','','EIS_XXWC_INV_GL_MISMATCH_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'GL Code Mismatch Report - WC',401,'ITEM_CREATED_BY','Item Created By','Item Created By','','','default','','9','N','Y','','','','','','','SA059956','N','N','','EIS_XXWC_INV_GL_MISMATCH_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'GL Code Mismatch Report - WC',401,'ITEM_NUM','Item Number','Item Num','','','default','','1','N','Y','','','','','','','SA059956','N','N','','EIS_XXWC_INV_GL_MISMATCH_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'GL Code Mismatch Report - WC',401,'ITEM_STATUS','Item Status','Item Status','','','default','','4','N','Y','','','','','','','SA059956','N','N','','EIS_XXWC_INV_GL_MISMATCH_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'GL Code Mismatch Report - WC',401,'ITEM_TYPE','Item Type','Item Type','','','default','','5','N','Y','','','','','','','SA059956','N','N','','EIS_XXWC_INV_GL_MISMATCH_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'GL Code Mismatch Report - WC',401,'ORG','Organization','Org','','','default','','3','N','Y','','','','','','','SA059956','N','N','','EIS_XXWC_INV_GL_MISMATCH_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'GL Code Mismatch Report - WC',401,'SALES_ACCOUNT','Sales Account','Sales Account','','','default','','14','N','Y','','','','','','','SA059956','N','N','','EIS_XXWC_INV_GL_MISMATCH_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'GL Code Mismatch Report - WC',401,'SALES_ACCOUNT_FULL','Sales Account Full','Sales Account Full','','','default','','13','N','Y','','','','','','','SA059956','N','N','','EIS_XXWC_INV_GL_MISMATCH_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'GL Code Mismatch Report - WC',401,'SALES_DEFAULT','Sales Default','Sales Default','','','default','','15','N','Y','','','','','','','SA059956','N','N','','EIS_XXWC_INV_GL_MISMATCH_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'GL Code Mismatch Report - WC',401,'LAST_UPDATE_DATE','Last Update Date','Last Update Date','','','default','','19','N','Y','','','','','','','SA059956','N','N','','EIS_XXWC_INV_GL_MISMATCH_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'GL Code Mismatch Report - WC',401,'ORG_ACT_Y_N','Orgs Active Y/N','Org Act Y N','','','default','','20','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_INV_GL_MISMATCH_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'GL Code Mismatch Report - WC',401,'EXPENSE_DEFAULT','Expense Default','Expense Default','','','default','','18','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_INV_GL_MISMATCH_V','','','','US','');
--Inserting Report Parameters - GL Code Mismatch Report - WC
xxeis.eis_rsc_ins.rp( 'GL Code Mismatch Report - WC',401,'Category Class','Category Class','CATEGORY_CLASS','IN','EIS_INV_ITEM_CATEGORIES_LOV','','VARCHAR2','N','Y','3','Y','Y','CONSTANT','SA059956','Y','N','','','','EIS_XXWC_INV_GL_MISMATCH_V','','','US','');
xxeis.eis_rsc_ins.rp( 'GL Code Mismatch Report - WC',401,'COGS Account','COGS Account','COGS_ACCOUNT','IN','XXWC INV COGS ACCOUNT','','VARCHAR2','N','Y','4','Y','Y','CONSTANT','SA059956','Y','N','','','','EIS_XXWC_INV_GL_MISMATCH_V','','','US','');
xxeis.eis_rsc_ins.rp( 'GL Code Mismatch Report - WC',401,'Expense Account','Expense Account','EXPENSE_ACCOUNT','IN','XXWC INV EXPENSE ACCOUNT','','VARCHAR2','N','Y','6','Y','Y','CONSTANT','SA059956','Y','N','','','','EIS_XXWC_INV_GL_MISMATCH_V','','','US','');
xxeis.eis_rsc_ins.rp( 'GL Code Mismatch Report - WC',401,'Item Number','Item Number','ITEM_NUM','IN','EIS_INV_ITEM_LOV','','VARCHAR2','N','Y','1','Y','Y','CONSTANT','SA059956','Y','N','','','','EIS_XXWC_INV_GL_MISMATCH_V','','','US','');
xxeis.eis_rsc_ins.rp( 'GL Code Mismatch Report - WC',401,'Organization','Organization','ORG','IN','XXWC Organization Code','','VARCHAR2','N','Y','2','Y','Y','CONSTANT','SA059956','Y','N','','','','EIS_XXWC_INV_GL_MISMATCH_V','','','US','');
xxeis.eis_rsc_ins.rp( 'GL Code Mismatch Report - WC',401,'Sales Account','Sales Account','SALES_ACCOUNT','IN','XXWC INV SALES ACCOUNT','','VARCHAR2','N','Y','5','Y','Y','CONSTANT','SA059956','Y','N','','','','EIS_XXWC_INV_GL_MISMATCH_V','','','US','');
xxeis.eis_rsc_ins.rp( 'GL Code Mismatch Report - WC',401,'Item List','Item List','','IN','XXWC Item List','','VARCHAR2','N','Y','7','Y','N','CONSTANT','SA059956','Y','N','','','','EIS_XXWC_INV_GL_MISMATCH_V','','','US','');
xxeis.eis_rsc_ins.rp( 'GL Code Mismatch Report - WC',401,'Creation Date From','Creation Date From','CREATION_DATE','>=','','','DATE','N','Y','8','Y','Y','CONSTANT','SA059956','Y','N','','Start Date','','EIS_XXWC_INV_GL_MISMATCH_V','','','US','');
xxeis.eis_rsc_ins.rp( 'GL Code Mismatch Report - WC',401,'Last Update Date From','Last Update Date From','LAST_UPDATE_DATE','>=','','','DATE','N','Y','10','Y','Y','CONSTANT','SA059956','Y','N','','Start Date','','EIS_XXWC_INV_GL_MISMATCH_V','','','US','');
xxeis.eis_rsc_ins.rp( 'GL Code Mismatch Report - WC',401,'Creation Date To','Creation Date To','CREATION_DATE','<=','','','DATE','N','Y','9','Y','Y','CONSTANT','SA059956','Y','N','','End Date','','EIS_XXWC_INV_GL_MISMATCH_V','','','US','');
xxeis.eis_rsc_ins.rp( 'GL Code Mismatch Report - WC',401,'Last Update Date To','Last Update Date To','LAST_UPDATE_DATE','<=','','','DATE','N','Y','11','Y','Y','CONSTANT','SA059956','Y','N','','End Date','','EIS_XXWC_INV_GL_MISMATCH_V','','','US','');
xxeis.eis_rsc_ins.rp( 'GL Code Mismatch Report - WC',401,'Orgs Active Y/N','Orgs Active Y/N','ORG_ACT_Y_N','=','EIS_INV_YES_NO_LOV','','VARCHAR2','N','Y','12','Y','Y','CONSTANT','SA059956','Y','N','','','','EIS_XXWC_INV_GL_MISMATCH_V','','','US','');
--Inserting Dependent Parameters - GL Code Mismatch Report - WC
--Inserting Report Conditions - GL Code Mismatch Report - WC
xxeis.eis_rsc_ins.rcnh( 'GL Code Mismatch Report - WC',401,'Free Text ','FREE_TEXT','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','','','','','','','','','','','','','','','Y','','','','','AND (:Item List is null or EXISTS (SELECT 1  FROM
                         APPS.XXWC_PARAM_LIST
                         WHERE LIST_NAME = :Item List
                            AND LIST_TYPE =''Item''
                         AND DBMS_LOB.INSTR(LIST_VALUES,
                          EXIGMV.ITEM_NUM, 1, 1)<>0))
','1',401,'GL Code Mismatch Report - WC','Free Text ');
xxeis.eis_rsc_ins.rcnh( 'GL Code Mismatch Report - WC',401,'EXIGMV.ITEM_NUM IN Item Number','SIMPLE','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','ITEM_NUM','','Item Number','','','','','EIS_XXWC_INV_GL_MISMATCH_V','','','','','','IN','Y','Y','','','','','1',401,'GL Code Mismatch Report - WC','EXIGMV.ITEM_NUM IN Item Number');
xxeis.eis_rsc_ins.rcnh( 'GL Code Mismatch Report - WC',401,'EXIGMV.ORG IN Organization','SIMPLE','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','ORG','','Organization','','','','','EIS_XXWC_INV_GL_MISMATCH_V','','','','','','IN','Y','Y','','','','','1',401,'GL Code Mismatch Report - WC','EXIGMV.ORG IN Organization');
xxeis.eis_rsc_ins.rcnh( 'GL Code Mismatch Report - WC',401,'EXIGMV.CATEGORY_CLASS IN Category Class','SIMPLE','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','CATEGORY_CLASS','','Category Class','','','','','EIS_XXWC_INV_GL_MISMATCH_V','','','','','','IN','Y','Y','','','','','1',401,'GL Code Mismatch Report - WC','EXIGMV.CATEGORY_CLASS IN Category Class');
xxeis.eis_rsc_ins.rcnh( 'GL Code Mismatch Report - WC',401,'EXIGMV.COGS_ACCOUNT IN COGS Account','SIMPLE','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','COGS_ACCOUNT','','COGS Account','','','','','EIS_XXWC_INV_GL_MISMATCH_V','','','','','','IN','Y','Y','','','','','1',401,'GL Code Mismatch Report - WC','EXIGMV.COGS_ACCOUNT IN COGS Account');
xxeis.eis_rsc_ins.rcnh( 'GL Code Mismatch Report - WC',401,'EXIGMV.SALES_ACCOUNT IN Sales Account','SIMPLE','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','SALES_ACCOUNT','','Sales Account','','','','','EIS_XXWC_INV_GL_MISMATCH_V','','','','','','IN','Y','Y','','','','','1',401,'GL Code Mismatch Report - WC','EXIGMV.SALES_ACCOUNT IN Sales Account');
xxeis.eis_rsc_ins.rcnh( 'GL Code Mismatch Report - WC',401,'EXIGMV.EXPENSE_ACCOUNT IN Expense Account','SIMPLE','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','EXPENSE_ACCOUNT','','Expense Account','','','','','EIS_XXWC_INV_GL_MISMATCH_V','','','','','','IN','Y','Y','','','','','1',401,'GL Code Mismatch Report - WC','EXIGMV.EXPENSE_ACCOUNT IN Expense Account');
xxeis.eis_rsc_ins.rcnh( 'GL Code Mismatch Report - WC',401,'EXIGMV.CREATION_DATE >= Creation Date From','SIMPLE','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','CREATION_DATE','','Creation Date From','','','','','EIS_XXWC_INV_GL_MISMATCH_V','','','','','','GREATER_THAN_EQUALS','Y','Y','','','','','1',401,'GL Code Mismatch Report - WC','EXIGMV.CREATION_DATE >= Creation Date From');
xxeis.eis_rsc_ins.rcnh( 'GL Code Mismatch Report - WC',401,'EXIGMV.CREATION_DATE <= Creation Date To','SIMPLE','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','CREATION_DATE','','Creation Date To','','','','','EIS_XXWC_INV_GL_MISMATCH_V','','','','','','LESS_THAN_EQUALS','Y','Y','','','','','1',401,'GL Code Mismatch Report - WC','EXIGMV.CREATION_DATE <= Creation Date To');
xxeis.eis_rsc_ins.rcnh( 'GL Code Mismatch Report - WC',401,'EXIGMV.LAST_UPDATE_DATE >= Last Update Date From','SIMPLE','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','LAST_UPDATE_DATE','','Last Update Date From','','','','','EIS_XXWC_INV_GL_MISMATCH_V','','','','','','GREATER_THAN_EQUALS','Y','Y','','','','','1',401,'GL Code Mismatch Report - WC','EXIGMV.LAST_UPDATE_DATE >= Last Update Date From');
xxeis.eis_rsc_ins.rcnh( 'GL Code Mismatch Report - WC',401,'EXIGMV.LAST_UPDATE_DATE <= Last Update Date To','SIMPLE','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','LAST_UPDATE_DATE','','Last Update Date To','','','','','EIS_XXWC_INV_GL_MISMATCH_V','','','','','','LESS_THAN_EQUALS','Y','Y','','','','','1',401,'GL Code Mismatch Report - WC','EXIGMV.LAST_UPDATE_DATE <= Last Update Date To');
xxeis.eis_rsc_ins.rcnh( 'GL Code Mismatch Report - WC',401,'EXIGMV.ORG_ACT_Y_N = Orgs Active Y/N','SIMPLE','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','ORG_ACT_Y_N','','Orgs Active Y/N','','','','','EIS_XXWC_INV_GL_MISMATCH_V','','','','','','EQUALS','Y','Y','','','','','1',401,'GL Code Mismatch Report - WC','EXIGMV.ORG_ACT_Y_N = Orgs Active Y/N');
--Inserting Report Sorts - GL Code Mismatch Report - WC
xxeis.eis_rsc_ins.rs( 'GL Code Mismatch Report - WC',401,'ITEM_NUM','ASC','SA059956','1','');
xxeis.eis_rsc_ins.rs( 'GL Code Mismatch Report - WC',401,'ORG','ASC','SA059956','2','');
--Inserting Report Triggers - GL Code Mismatch Report - WC
--inserting report templates - GL Code Mismatch Report - WC
--Inserting Report Portals - GL Code Mismatch Report - WC
--inserting report dashboards - GL Code Mismatch Report - WC
--Exporting report views table information -  
xxeis.eis_rsc_ins.rviews( 'GL Code Mismatch Report - WC','401','EIS_XXWC_INV_GL_MISMATCH_V','EIS_XXWC_INV_GL_MISMATCH_V','N','');
--inserting report security - GL Code Mismatch Report - WC
xxeis.eis_rsc_ins.rsec( 'GL Code Mismatch Report - WC','401','','XXWC_DATA_MNGT_SC',401,'SA059956','','','');
--Inserting Report Pivots - GL Code Mismatch Report - WC
--Inserting Report   Version details- GL Code Mismatch Report - WC
xxeis.eis_rsc_ins.rv( 'GL Code Mismatch Report - WC','','GL Code Mismatch Report - WC','SA059956');
ELSE
RAISE_APPLICATION_ERROR (-20001,'Module does not exist. Import/Create the module with id 401');
END IF;
END;
/
