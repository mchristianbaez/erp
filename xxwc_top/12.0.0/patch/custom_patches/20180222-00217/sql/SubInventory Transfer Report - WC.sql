--Report Name            : SubInventory Transfer Report - WC
--Import Instructions    : Run this file in XXEIS user from SQL*Plus or any tool like Toad, SQL Navigator. 
prompt Creating Object Data EIS_XXWC_SUBINV_TRANSFER_V
set scan off define off
DECLARE
mod_exist VARCHAR2(1);
BEGIN 
mod_exist := XXEIS.EIS_RSC_INSERT.check_if_module_exist(401);
IF mod_exist = 'Y' THEN 
--Inserting Object EIS_XXWC_SUBINV_TRANSFER_V
xxeis.eis_rsc_ins.v( 'EIS_XXWC_SUBINV_TRANSFER_V',401,'','','','','PS066716','XXEIS','Eis Xxwc Subinv Transfer V','EXSTV','','','VIEW','US','','','');
--Delete Object Columns for EIS_XXWC_SUBINV_TRANSFER_V
xxeis.eis_rsc_utility.delete_view_rows('EIS_XXWC_SUBINV_TRANSFER_V',401,FALSE);
--Inserting Object Columns for EIS_XXWC_SUBINV_TRANSFER_V
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_SUBINV_TRANSFER_V','REGION',401,'Region','REGION','','','','PS066716','VARCHAR2','','','Region','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_SUBINV_TRANSFER_V','DISTRICT',401,'District','DISTRICT','','','','PS066716','VARCHAR2','','','District','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_SUBINV_TRANSFER_V','ITEM',401,'Item','ITEM','','','','PS066716','VARCHAR2','','','Item','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_SUBINV_TRANSFER_V','ITEM_DESCRIPTION',401,'Item Description','ITEM_DESCRIPTION','','','','PS066716','VARCHAR2','','','Item Description','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_SUBINV_TRANSFER_V','ON_HAND',401,'On Hand','ON_HAND','','','','PS066716','NUMBER','','','On Hand','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_SUBINV_TRANSFER_V','AVGCOST',401,'Avgcost','AVGCOST','','','','PS066716','NUMBER','','','Avgcost','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_SUBINV_TRANSFER_V','SUBINVENTORY',401,'Subinventory','SUBINVENTORY','','','','PS066716','VARCHAR2','','','Subinventory','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_SUBINV_TRANSFER_V','DATE_RECEIVED',401,'Date Received','DATE_RECEIVED','','','','PS066716','DATE','','','Date Received','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_SUBINV_TRANSFER_V','MOQD_INVENTORY_ITEM_ID',401,'Moqd Inventory Item Id','MOQD_INVENTORY_ITEM_ID','','','','PS066716','NUMBER','','','Moqd Inventory Item Id','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_SUBINV_TRANSFER_V','MOQD_ORGANIZATION_ID',401,'Moqd Organization Id','MOQD_ORGANIZATION_ID','','','','PS066716','NUMBER','','','Moqd Organization Id','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_SUBINV_TRANSFER_V','MP_ORGANIZATION_ID',401,'Mp Organization Id','MP_ORGANIZATION_ID','','','','PS066716','NUMBER','','','Mp Organization Id','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_SUBINV_TRANSFER_V','OOD_ORGANIZATION_ID',401,'Ood Organization Id','OOD_ORGANIZATION_ID','','','','PS066716','NUMBER','','','Ood Organization Id','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_SUBINV_TRANSFER_V','MSI_INVENTORY_ITEM_ID',401,'Msi Inventory Item Id','MSI_INVENTORY_ITEM_ID','','','','PS066716','NUMBER','','','Msi Inventory Item Id','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_SUBINV_TRANSFER_V','MSI_ORGANIZATION_ID',401,'Msi Organization Id','MSI_ORGANIZATION_ID','','','','PS066716','NUMBER','','','Msi Organization Id','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_SUBINV_TRANSFER_V','ORGANIZATION_CODE',401,'Organization Code','ORGANIZATION_CODE','','','','PS066716','VARCHAR2','','','Organization Code','','','','','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_SUBINV_TRANSFER_V','ORGANIZATION',401,'Organization','ORGANIZATION','','','','PS066716','VARCHAR2','','','Organization','','','','','');
--Inserting Object Components for EIS_XXWC_SUBINV_TRANSFER_V
--Inserting Object Component Joins for EIS_XXWC_SUBINV_TRANSFER_V
ELSE
RAISE_APPLICATION_ERROR (-20001,'Module does not exist. Import/Create the module with id 401');
END IF;
END;
/
prompt Creating Report LOV Data for SubInventory Transfer Report - WC
set scan off define off
DECLARE
mod_exist VARCHAR2(1);
BEGIN 
mod_exist := XXEIS.EIS_RSC_INSERT.check_if_module_exist(401);
IF mod_exist = 'Y' THEN 
--Inserting Report LOVs - SubInventory Transfer Report - WC
xxeis.eis_rsc_ins.lov( 401,'select SECONDARY_INVENTORY_NAME SUB_INVENTORY,HAOU.NAME ORGANIZATION_NAME
from MTL_SECONDARY_INVENTORIES MSI,
     HR_ALL_ORGANIZATION_UNITS HAOU
WHERE 1=1
AND MSI.ORGANIZATION_ID = HAOU.organization_id','','EIS_INV_SUBINVENTORY_LOV','List of All SubInventories.','XXEIS_RS_ADMIN',NULL,'N','','','','','','','US');
xxeis.eis_rsc_ins.lov( 401,'select distinct ATTRIBUTE9 Region from mtl_parameters','','Region Lov','','ANONYMOUS',NULL,'N','','','','','','','US');
xxeis.eis_rsc_ins.lov( 401,'select list_name from XXWC_PARAM_LIST where list_type = ''Item'' and (PUBLIC_FLAG =''Y'' OR USER_ID = fnd_global.user_id)','','XXWC Item List','','ANONYMOUS',NULL,'N','','','','','','','US');
xxeis.eis_rsc_ins.lov( '','SELECT ORGANIZATION_CODE WAREHOUSE,ORGANIZATION_NAME
FROM ORG_ORGANIZATION_DEFINITIONS OOD
WHERE SYSDATE < NVL(OOD.DISABLE_DATE,SYSDATE+1) AND EXISTS (SELECT 1 FROM XXEIS.EIS_ORG_ACCESS_V  WHERE organization_id = ood.organization_id )
UNION
SELECT ''All'', ''All Organizations'' FROM DUAL','','XXWC All ORG LIST','XXWC All ORG LIST','ANONYMOUS',NULL,'N','','','','','','','US');
null;
ELSE
RAISE_APPLICATION_ERROR (-20001,'Module does not exist. Import/Create the module with id 401');
END IF;
END;
/
prompt Creating Report Data for SubInventory Transfer Report - WC
set scan off define off
DECLARE
mod_exist varchar2(1);
BEGIN 
mod_exist := XXEIS.EIS_RSC_INSERT.check_if_module_exist(401);
IF mod_exist = 'Y' THEN 
--Deleting Report data - SubInventory Transfer Report - WC
xxeis.eis_rsc_utility.delete_report_rows( 'SubInventory Transfer Report - WC',401 );
--Inserting Report - SubInventory Transfer Report - WC
xxeis.eis_rsc_ins.r( 401,'SubInventory Transfer Report - WC','','To Pull subinventory onhand quantity detaisl with recieved date','','','','PS066716','EIS_XXWC_SUBINV_TRANSFER_V','Y','','','PS066716','','N','White Cap Reports','','CSV,EXCEL,','N','','','','','','N','','US','','','','','','','','','','','','','','','','');
--Inserting Report Columns - SubInventory Transfer Report - WC
xxeis.eis_rsc_ins.rc( 'SubInventory Transfer Report - WC',401,'AVGCOST','Avgcost','Avgcost','','$~,~.~2','default','','7','','Y','','','','','','','PS066716','N','N','','EIS_XXWC_SUBINV_TRANSFER_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'SubInventory Transfer Report - WC',401,'DATE_RECEIVED','Date Received','Date Received','','','default','','10','','Y','','','','','','','PS066716','N','N','','EIS_XXWC_SUBINV_TRANSFER_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'SubInventory Transfer Report - WC',401,'DISTRICT','District','District','','','default','','2','','Y','','','','','','','PS066716','N','N','','EIS_XXWC_SUBINV_TRANSFER_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'SubInventory Transfer Report - WC',401,'ITEM','Item','Item','','','default','','4','','Y','','','','','','','PS066716','N','N','','EIS_XXWC_SUBINV_TRANSFER_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'SubInventory Transfer Report - WC',401,'ITEM_DESCRIPTION','Item Description','Item Description','','','default','','5','','Y','','','','','','','PS066716','N','N','','EIS_XXWC_SUBINV_TRANSFER_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'SubInventory Transfer Report - WC',401,'ON_HAND','On Hand','On Hand','','~~~','default','','6','','Y','','','','','','','PS066716','N','N','','EIS_XXWC_SUBINV_TRANSFER_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'SubInventory Transfer Report - WC',401,'REGION','Region','Region','','','default','','1','','Y','','','','','','','PS066716','N','N','','EIS_XXWC_SUBINV_TRANSFER_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'SubInventory Transfer Report - WC',401,'SUBINVENTORY','Subinventory','Subinventory','','','default','','9','','Y','','','','','','','PS066716','N','N','','EIS_XXWC_SUBINV_TRANSFER_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'SubInventory Transfer Report - WC',401,'Extended_Cost','Extended Cost','','NUMBER','$~,~.~2','default','','8','Y','Y','','','','','','( EXSTV.AVGCOST* EXSTV.ON_HAND)','PS066716','N','N','','','','','','US','','');
xxeis.eis_rsc_ins.rc( 'SubInventory Transfer Report - WC',401,'ORGANIZATION','Organization','Organization','','','','','3','','Y','','','','','','','PS066716','N','N','','EIS_XXWC_SUBINV_TRANSFER_V','','','','US','','');
--Inserting Report Parameters - SubInventory Transfer Report - WC
xxeis.eis_rsc_ins.rp( 'SubInventory Transfer Report - WC',401,'Item','Item','ITEM','IN','XXWC Item List','','VARCHAR2','N','Y','4','Y','Y','CONSTANT','PS066716','Y','N','','','','EIS_XXWC_SUBINV_TRANSFER_V','','','US','');
xxeis.eis_rsc_ins.rp( 'SubInventory Transfer Report - WC',401,'Region','Region','REGION','IN','Region Lov','','VARCHAR2','N','Y','2','Y','Y','CONSTANT','PS066716','Y','N','','','','EIS_XXWC_SUBINV_TRANSFER_V','','','US','');
xxeis.eis_rsc_ins.rp( 'SubInventory Transfer Report - WC',401,'Subinventory','Subinventory','SUBINVENTORY','IN','EIS_INV_SUBINVENTORY_LOV','','VARCHAR2','N','Y','3','Y','Y','CONSTANT','PS066716','Y','N','','','','EIS_XXWC_SUBINV_TRANSFER_V','','','US','');
xxeis.eis_rsc_ins.rp( 'SubInventory Transfer Report - WC',401,'Organization','Organization','ORGANIZATION','IN','XXWC All ORG LIST','','VARCHAR2','Y','Y','1','Y','N','CONSTANT','PS066716','Y','N','','','','EIS_XXWC_SUBINV_TRANSFER_V','','','US','');
--Inserting Dependent Parameters - SubInventory Transfer Report - WC
--Inserting Report Conditions - SubInventory Transfer Report - WC
xxeis.eis_rsc_ins.rcnh( 'SubInventory Transfer Report - WC',401,'EXSTV.ITEM IN Item','SIMPLE','','','Y','','1');
xxeis.eis_rsc_ins.rcnd( '','','ITEM','','Item','','','','','EIS_XXWC_SUBINV_TRANSFER_V','','','','','','IN','Y','Y','','','','','1',401,'SubInventory Transfer Report - WC','EXSTV.ITEM IN Item');
xxeis.eis_rsc_ins.rcnh( 'SubInventory Transfer Report - WC',401,'EXSTV.REGION IN Region','SIMPLE','','','Y','','3');
xxeis.eis_rsc_ins.rcnd( '','','REGION','','Region','','','','','EIS_XXWC_SUBINV_TRANSFER_V','','','','','','IN','Y','Y','','','','','1',401,'SubInventory Transfer Report - WC','EXSTV.REGION IN Region');
xxeis.eis_rsc_ins.rcnh( 'SubInventory Transfer Report - WC',401,'EXSTV.SUBINVENTORY IN Subinventory','SIMPLE','','','Y','','4');
xxeis.eis_rsc_ins.rcnd( '','','SUBINVENTORY','','Subinventory','','','','','EIS_XXWC_SUBINV_TRANSFER_V','','','','','','IN','Y','Y','','','','','1',401,'SubInventory Transfer Report - WC','EXSTV.SUBINVENTORY IN Subinventory');
xxeis.eis_rsc_ins.rcnh( 'SubInventory Transfer Report - WC',401,'FreeText','FREE_TEXT','','','Y','','5');
xxeis.eis_rsc_ins.rcnd( '','','','','','','','','','','','','','','','','Y','Y','','','','and (''All'' IN (:Organization) OR EXSTV.ORGANIZATION_CODE in (:Organization))
and (Upper(EXSTV.SUBINVENTORY) not in (''GENERAL''))','1',401,'SubInventory Transfer Report - WC','FreeText');
--Inserting Report Sorts - SubInventory Transfer Report - WC
--Inserting Report Triggers - SubInventory Transfer Report - WC
--inserting report templates - SubInventory Transfer Report - WC
--Inserting Report Portals - SubInventory Transfer Report - WC
--inserting report dashboards - SubInventory Transfer Report - WC
--Exporting report views table information -  
xxeis.eis_rsc_ins.rviews( 'SubInventory Transfer Report - WC','401','EIS_XXWC_SUBINV_TRANSFER_V','EIS_XXWC_SUBINV_TRANSFER_V','N','');
--inserting report security - SubInventory Transfer Report - WC
xxeis.eis_rsc_ins.rsec( 'SubInventory Transfer Report - WC','401','','XXWC_ MOBILE_PHYSICAL_INV',401,'PS066716','','','');
xxeis.eis_rsc_ins.rsec( 'SubInventory Transfer Report - WC','401','','XXWC_MOV_INV_WC',401,'PS066716','','','');
xxeis.eis_rsc_ins.rsec( 'SubInventory Transfer Report - WC','401','','XXWC_INVENTORY_SUPER_USER',401,'PS066716','','','');
xxeis.eis_rsc_ins.rsec( 'SubInventory Transfer Report - WC','401','','XXWC_INVENTORY_SPEC_SCC',401,'PS066716','','','');
xxeis.eis_rsc_ins.rsec( 'SubInventory Transfer Report - WC','201','','XXWC_SALES_PURCHASING_MGR',401,'PS066716','','','');
xxeis.eis_rsc_ins.rsec( 'SubInventory Transfer Report - WC','201','','HDS_PRCHSNG_SPR_USR',401,'PS066716','','','');
xxeis.eis_rsc_ins.rsec( 'SubInventory Transfer Report - WC','201','','XXWC_PURCHASING_SR_MRG_WC',401,'PS066716','','','');
xxeis.eis_rsc_ins.rsec( 'SubInventory Transfer Report - WC','201','','XXWC_PURCHASING_MGR',401,'PS066716','','','');
xxeis.eis_rsc_ins.rsec( 'SubInventory Transfer Report - WC','201','','XXWC_PURCHASING_INQUIRY',401,'PS066716','','','');
xxeis.eis_rsc_ins.rsec( 'SubInventory Transfer Report - WC','201','','XXWC_PURCHASING_BUYER',401,'PS066716','','','');
xxeis.eis_rsc_ins.rsec( 'SubInventory Transfer Report - WC','401','','XXWC_INV_PLANNER',401,'PS066716','','','');
xxeis.eis_rsc_ins.rsec( 'SubInventory Transfer Report - WC','401','','XXWC_INVENTORY_CONTROL_INQUIRY',401,'PS066716','','','');
xxeis.eis_rsc_ins.rsec( 'SubInventory Transfer Report - WC','401','','XXWC_INVENTORY_CONTROL_SR_MGR',401,'PS066716','','','');
xxeis.eis_rsc_ins.rsec( 'SubInventory Transfer Report - WC','401','','XXWC_INV_ACCOUNTANT',401,'PS066716','','','');
xxeis.eis_rsc_ins.rsec( 'SubInventory Transfer Report - WC','401','','HDS_INVNTRY',401,'PS066716','','','');
xxeis.eis_rsc_ins.rsec( 'SubInventory Transfer Report - WC','401','','XXWC_EOM_INVENTORY',401,'PS066716','','','');
xxeis.eis_rsc_ins.rsec( 'SubInventory Transfer Report - WC','401','','XXWC_AO_INV_ADJ',401,'PS066716','','','');
xxeis.eis_rsc_ins.rsec( 'SubInventory Transfer Report - WC','401','','XXWC_AO_INV_ADJ_REC',401,'PS066716','','','');
xxeis.eis_rsc_ins.rsec( 'SubInventory Transfer Report - WC','401','','XXWC_AO_INV_ADJ_PO_RPT',401,'PS066716','','','');
xxeis.eis_rsc_ins.rsec( 'SubInventory Transfer Report - WC','401','','XXWC_AO_INV_ADJ_OEENTRY',401,'PS066716','','','');
xxeis.eis_rsc_ins.rsec( 'SubInventory Transfer Report - WC','707','','XXCUS_COST_MANAGEMENT_GSC',401,'PS066716','','','');
xxeis.eis_rsc_ins.rsec( 'SubInventory Transfer Report - WC','702','','XXWC_COST_MANAGEMENT',401,'PS066716','','','');
--Inserting Report Pivots - SubInventory Transfer Report - WC
--Inserting Report Distribution Details 
--Inserting Report Distribution Details 
--Inserting Report SubTotals Details 
--Inserting Report aggregations 
--Inserting Report   Version details- SubInventory Transfer Report - WC
xxeis.eis_rsc_ins.rv( 'SubInventory Transfer Report - WC','','SubInventory Transfer Report - WC','PS066716','09-MAR-2018');
ELSE
RAISE_APPLICATION_ERROR (-20001,'Module does not exist. Import/Create the module with id 401');
END IF;
END;
/
