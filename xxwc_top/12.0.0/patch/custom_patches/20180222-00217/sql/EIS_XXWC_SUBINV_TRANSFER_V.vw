-------------------------------------------------------------------------------------------
/*****************************************************************************************
  $Header XXEIS.EIS_XXWC_SUBINV_TRANSFER_V.vw $
  Module Name: Inventory
  PURPOSE: SubInventory Transfer Report - WC
  REVISIONS:
  Ver          Date        Author                Description
  ---------  ----------  ------------------    ----------------
  1.0		 3/06/2018      Prasanna			TMS#20180222-00217
******************************************************************************************/
---------------------------------------------------------------------------------------------
CREATE OR REPLACE VIEW XXEIS.EIS_XXWC_SUBINV_TRANSFER_V 
 AS 
  SELECT mp.attribute9 region,
  mp.attribute8 district,
  ood.organization_name organization,
  ood.organization_code,
  msi.segment1 item,
  msi.description item_description,
  primary_transaction_quantity on_hand,
  NVL ( apps.cst_cost_api.get_item_cost (1, MSI.inventory_item_id, MSI.organization_id), 0) avgcost,
  subinventory_code subinventory,
  MOQd.date_received,
  ---Primary Keys
  moqd.inventory_item_id moqd_inventory_item_id,
  moqd.organization_id moqd_organization_id,
  mp.organization_id mp_organization_id,
  ood.organization_id ood_organization_id,
  msi.inventory_item_id msi_inventory_item_id,
  msi.organization_id msi_organization_id
  --descr#flexfield#start
  --descr#flexfield#end
FROM MTL_ONHAND_QUANTITIES_DETAIL MOQD,
  mtl_parameters mp,
  org_organization_definitions ood,
  mtl_system_items_b msi
WHERE 1                   =1
AND moqd.organization_id  =mp.organization_id
AND mp.organization_id    =ood.organization_id
AND moqd.inventory_item_id=msi.inventory_item_id
AND moqd.organization_id  =msi.organization_id
/
  