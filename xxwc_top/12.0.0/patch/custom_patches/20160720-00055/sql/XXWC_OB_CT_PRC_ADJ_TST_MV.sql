DROP MATERIALIZED VIEW   APPS.XXWC_OB_CT_PRC_ADJ_TST_MV;

CREATE MATERIALIZED VIEW APPS.XXWC_OB_CT_PRC_ADJ_TST_MV (HEADER_ID,LINE_ID,ADJUSTMENT_NAME,COUNT_MANUAL_MODIFIERS,COUNT_ALL_MODIFIERS,COUNT_AUTO_LN_MODIFIERS,FIRST_MODIFIER,LAST_MODIFIER,ARITHMETIC_OPERATOR,OPERAND,LIST_LINE_TYPE_CODE,AUT_FLAG_FOR_SELLING_PRICE,CHANGE_REASON_CODE,PRICE_TYP_AUTOMATIC_FLAG,LH_ATTRIBUTE10,LH_NAME,PRICE_SRC_TYPE)
TABLESPACE APPS_TS_TX_DATA
NOCACHE
NOCOMPRESS
BUILD IMMEDIATE
REFRESH COMPLETE ON DEMAND
WITH PRIMARY KEY
AS 
/*************************************************************************
 Copyright (c) 2016 HD Supply
 All rights reserved.
**************************************************************************
   $Header XXWC_OB_CT_PRC_ADJ_TST_MV$
  Module Name: XXWC_OB_CT_PRC_ADJ_TST_MV

  PURPOSE:
 
     sub-MV used by APPS.XXWC_OB_CUST_TRX_INCR_TST_MV
       -- replacement for original OBI code calls to
       apps.xxwc_mv_routines_add_pkg
          FUNCTION get_original_modifier
          FUNCTION get_last_modifier
          FUNCTION get_overridden_sell_price
          FUNCTION get_price_type
     --
     -- This MV will be refreshed before the main query in
     -- XXWC_OBIEE_CUST_TRANS_INCR_MV
     --
     -- v2  -- correction for C59_SYSTEM_PRICE (overridden_sell_price)
         only records w automatic_flag='Y' should be returned
         else set the OPERAND to NULL. In such NULL cases the top level join should
        set the overridden_sell_price equal to oe_order_lines_all.unit_list_price
        v3 CG added aut_flag_for_selling_price
        v4 TMS 20151216-00037 Added new field PRICE_SRC_TYPE
     v5 TMS 20160323-00028 Added order by clause in ROWNUM PARTITION


  REVISIONS:
  
   VERSION  DATE        AUTHOR(S)        TMS TASK          DESCRIPTION
   -------  ----------  ---------------  ---------------   ----------------------------------------------
   1.0      2016-07-26  Steve Moffa      20160720-00055    Initially Created 
   
 
**************************************************************************/
SELECT 
      header_id
      ,line_id
      ,adjustment_name
      ,count_manual_modifiers
      ,count_all_modifiers
      ,count_auto_ln_modifiers
      ,first_modifier
      ,last_modifier
      ,arithmetic_operator
      ,operand
      ,list_line_type_code
      ,aut_flag_for_selling_price
      ,change_reason_code
      ,price_typ_automatic_flag
      ,lh_attribute10
      ,lh_name
      ,price_src_type -- v4 Added by Neha
  FROM (SELECT header_id
              ,line_id
              ,adjustment_name
              ,SUM (CASE
                      WHEN     (   (    (   line_id IS NOT NULL
                                         OR (    line_id IS NULL
                                             AND list_line_type_code <> 'CIE'))
                                    AND modifier_level_code = 'LINE')
                                OR (    line_id IS NULL
                                    AND modifier_level_code = 'ORDER'))
                           AND change_reason_code = 'MANUAL'
                      THEN
                        1
                      ELSE
                        0
                    END)
               OVER (PARTITION BY header_id, line_id)
                 AS count_manual_modifiers
              ,SUM (CASE
                      WHEN (   (    (   line_id IS NOT NULL
                                     OR (    line_id IS NULL
                                         AND list_line_type_code <> 'CIE'))
                                AND modifier_level_code = 'LINE')
                            OR (    line_id IS NULL
                                AND modifier_level_code = 'ORDER'))
                      THEN
                        1
                      ELSE
                        0
                    END)
               OVER (PARTITION BY header_id, line_id)
                 AS count_all_modifiers
              ,SUM (CASE
                      WHEN     (   (    (   line_id IS NOT NULL
                                         OR (    line_id IS NULL
                                             AND list_line_type_code <> 'CIE'))
                                    AND modifier_level_code = 'LINE')
                                OR (    line_id IS NULL
                                    AND modifier_level_code = 'ORDER'))
                           AND automatic_flag = 'Y'
                      THEN
                        1
                      ELSE
                        0
                    END)
               OVER (PARTITION BY header_id, line_id)
                 AS count_auto_ln_modifiers
              ,FIRST_VALUE (adjustment_name)
                 OVER (PARTITION BY header_id, line_id
                       ORDER BY
                         last_update_date -- v5 Added by Rakesh Patel
                        ,pricing_group_sequence
                        ,price_adjustment_id
                        ,creation_date
                        ,created_by
                        --  ,last_update_date -- v5 Commented by Rakesh Patel
                        ,last_updated_by
                        ,last_update_login
                        ,program_application_id
                        ,program_id
                        ,program_update_date
                       ROWS BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED FOLLOWING)
                 AS first_modifier
              ,LAST_VALUE (adjustment_name)
                 OVER (PARTITION BY header_id, line_id
                       ORDER BY
                         last_update_date -- v5 Added by Rakesh Patel
                        ,pricing_group_sequence
                        ,price_adjustment_id
                        ,creation_date
                        ,created_by
                        -- ,last_update_date -- v5 Commented by Rakesh Patel
                        ,last_updated_by
                        ,last_update_login
                        ,program_application_id
                        ,program_id
                        ,program_update_date
                       ROWS BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED FOLLOWING)
                 AS last_modifier
              ,LAST_VALUE (arithmetic_operator)
                 OVER (PARTITION BY header_id, line_id
                       ORDER BY
                         -- 10/06/14 CG: added
                         automatic_flag
                        ,pricing_group_sequence
                        ,price_adjustment_id
                        ,creation_date
                        ,created_by
                        ,last_update_date
                        ,last_updated_by
                        ,last_update_login
                        ,program_application_id
                        ,program_id
                        ,program_update_date
                       ROWS BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED FOLLOWING)
                 AS arithmetic_operator
              ,LAST_VALUE (operand)
                 OVER (PARTITION BY header_id, line_id
                       ORDER BY
                         -- 10/06/14 CG: added
                         automatic_flag
                        ,pricing_group_sequence
                        ,price_adjustment_id
                        ,creation_date
                        ,created_by
                        ,last_update_date
                        ,last_updated_by
                        ,last_update_login
                        ,program_application_id
                        ,program_id
                        ,program_update_date
                       ROWS BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED FOLLOWING)
                 AS operand
              ,LAST_VALUE (list_line_type_code)
                 OVER (PARTITION BY header_id, line_id
                       ORDER BY
                         -- 10/06/14 CG: added
                         automatic_flag
                        ,pricing_group_sequence
                        ,price_adjustment_id
                        ,creation_date
                        ,created_by
                        ,last_update_date
                        ,last_updated_by
                        ,last_update_login
                        ,program_application_id
                        ,program_id
                        ,program_update_date
                       ROWS BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED FOLLOWING)
                 AS list_line_type_code
              ,LAST_VALUE (automatic_flag)
                 OVER (PARTITION BY header_id, line_id
                       ORDER BY
                         -- 10/06/14 CG: added
                         automatic_flag
                        ,pricing_group_sequence
                        ,price_adjustment_id
                        ,creation_date
                        ,created_by
                        ,last_update_date
                        ,last_updated_by
                        ,last_update_login
                        ,program_application_id
                        ,program_id
                        ,program_update_date
                       ROWS BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED FOLLOWING)
                 AS aut_flag_for_selling_price
              ,change_reason_code
              ,LAST_VALUE (automatic_flag)
                 OVER (PARTITION BY header_id, line_id
                       ORDER BY
                         pricing_group_sequence
                        ,price_adjustment_id
                        ,creation_date
                        ,created_by
                        ,last_update_date
                        ,last_updated_by
                        ,last_update_login
                        ,program_application_id
                        ,program_id
                        ,program_update_date
                       ROWS BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED FOLLOWING)
                 AS price_typ_automatic_flag
              ,lh.attribute10 AS lh_attribute10
              ,lh.name AS lh_name
              ,ROW_NUMBER () OVER (PARTITION BY header_id, line_id ORDER BY last_update_date) AS rn 
              ,vs.description AS price_src_type 
          FROM apps.oe_price_adjustments_v
               LEFT OUTER JOIN (SELECT /* FUNCTION get_price_type */
                                      attribute10
                                      ,name
                                      ,list_header_id
                                  FROM apps.qp_secu_list_headers_vl
                                 WHERE     context = 162
                                       AND automatic_flag = 'Y') lh
                 ON oe_price_adjustments_v.list_header_id = lh.list_header_id
               LEFT OUTER JOIN 
                              (SELECT NVL (ffv.description, 'MKT') description
                                     ,flex_value
                                 FROM apps.fnd_flex_values_vl ffv
                                WHERE     flex_value_set_id = 1015252
                                      AND enabled_flag = 'Y') vs
                 ON vs.flex_value = lh.attribute10 
         WHERE     applied_flag = 'Y'
               AND list_line_type_code <> 'FREIGHT_CHARGE' 
               and header_id in (select header_id from APPS.XXWC_OB_CT_ORD_LN_TST_MV)
        )
 WHERE rn = 1;

COMMENT ON MATERIALIZED VIEW APPS.XXWC_OB_CT_PRC_ADJ_TST_MV IS 'snapshot table for snapshot APPS.XXWC_OB_CT_PRC_ADJ_TST_MV';

BEGIN
  SYS.DBMS_STATS.GATHER_TABLE_STATS (
     OwnName           => 'APPS'
    ,TabName           => 'XXWC_OB_CT_PRC_ADJ_TST_MV'
    ,Estimate_Percent  => 100
    ,Method_Opt        => 'FOR ALL COLUMNS SIZE SKEWONLY'
    ,Degree            => NULL
    ,Cascade           => TRUE
    ,No_Invalidate  => FALSE);
END;
/