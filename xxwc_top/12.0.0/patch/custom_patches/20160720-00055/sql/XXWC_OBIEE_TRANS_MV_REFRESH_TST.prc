CREATE OR REPLACE PROCEDURE APPS.xxwc_ob_trans_mv_refresh_tst ( errbuf  OUT VARCHAR2,
                                                               retcode OUT VARCHAR2) is
/*************************************************************************
 Copyright (c) 2016 HD Supply
 All rights reserved.
**************************************************************************
   $Header .xxwc_ob_trans_mv_refresh_tst$
  Module Name: .xxwc_ob_trans_mv_refresh_tst

  PURPOSE:
 
  refresh all the Customer Trx line Incremental MV's
      
  REVISIONS:
  
   VERSION  DATE        AUTHOR(S)        TMS TASK          DESCRIPTION
   -------  ----------  ---------------  ---------------   ----------------------------------------------
   1.0      2016-07-26  Steve Moffa      20160720-00055    Initially Created 
   
 
**************************************************************************/
    l_error_message2 clob;
    l_conc_req_id   number := fnd_global.conc_request_id;
begin

    fnd_file.put_line(fnd_file.log,'Starting OBIEE Customer Trx Incremental New MV Refresh');
    fnd_file.put_line(fnd_file.log,' ');
    
    fnd_file.put_line(fnd_file.log,'Altering Session for _delay_index_maintain attribute');
    execute immediate 'ALTER SESSION SET "_delay_index_maintain"=FALSE';
    
    fnd_file.put_line(fnd_file.log,' ');
    fnd_file.put_line(fnd_file.log,'Executing group of alter materialized view - COMPILE on ALL related SubMVs and IncMV');
    
    execute immediate 'ALTER MATERIALIZED VIEW APPS.XXWC_OB_CT_PO_PRICE_TST_MV      COMPILE';
    execute immediate 'ALTER MATERIALIZED VIEW APPS.XXWC_OB_CT_PRC_ADJ_TST_MV       COMPILE';
    execute immediate 'ALTER MATERIALIZED VIEW APPS.XXWC_OB_ORD_LN_COST_TST_MV      COMPILE';
    execute immediate 'ALTER MATERIALIZED VIEW APPS.XXWC_OB_CT_VNDR_QUOTE_TST_MV    COMPILE';
    execute immediate 'ALTER MATERIALIZED VIEW APPS.XXWC_OB_CT_ORD_LN_TST_MV        COMPILE';
    execute immediate 'ALTER MATERIALIZED VIEW APPS.XXWC_OB_CT_MTL_ITM_CAT_TST_MV   COMPILE';
    execute immediate 'ALTER MATERIALIZED VIEW APPS.XXWC_OB_CT_CTL_TST_MV           COMPILE';
    execute immediate 'ALTER MATERIALIZED VIEW APPS.XXWC_OB_CUST_TRX_INCR_TST_MV    COMPILE';
   
    fnd_file.put_line(fnd_file.log,' ');
    fnd_file.put_line(fnd_file.log,'Executing DBMS_SNAPSHOT.REFRESH for Group 1 MVs');
    fnd_file.put_line(fnd_file.log,'Group 1 Refresh Start Time '||to_char(sysdate, 'MM/DD/YYYY HH24:MI:SS'));
    
    DBMS_SNAPSHOT.REFRESH(LIST => 'APPS.XXWC_OB_CT_CTL_TST_MV'
                            , PUSH_DEFERRED_RPC => FALSE
                            , REFRESH_AFTER_ERRORS =>FALSE
                            , PURGE_OPTION=>0
                            , PARALLELISM =>0
                            , ATOMIC_REFRESH=>FALSE
                            , NESTED=> TRUE);
 
    fnd_file.put_line(fnd_file.log,'Group 1 Refresh End Time '||to_char(sysdate, 'MM/DD/YYYY HH24:MI:SS'));

    fnd_file.put_line(fnd_file.log,' ');
    fnd_file.put_line(fnd_file.log,'Executing DBMS_SNAPSHOT.REFRESH for Group 2 MVs');
    fnd_file.put_line(fnd_file.log,'Group 2 Refresh Start Time '||to_char(sysdate, 'MM/DD/YYYY HH24:MI:SS'));
    
    DBMS_SNAPSHOT.REFRESH(LIST => 'APPS.XXWC_OB_CT_ORD_LN_TST_MV'
                            , PUSH_DEFERRED_RPC => FALSE
                            , REFRESH_AFTER_ERRORS =>FALSE
                            , PURGE_OPTION=>0
                            , PARALLELISM =>0
                            , ATOMIC_REFRESH=>FALSE
                            , NESTED=> TRUE);
 
    fnd_file.put_line(fnd_file.log,'Group 2 Refresh End Time '||to_char(sysdate, 'MM/DD/YYYY HH24:MI:SS'));

    fnd_file.put_line(fnd_file.log,' ');
    fnd_file.put_line(fnd_file.log,'Executing DBMS_SNAPSHOT.REFRESH for Group 3 MVs');
    fnd_file.put_line(fnd_file.log,'Group 3 Refresh Start Time '||to_char(sysdate, 'MM/DD/YYYY HH24:MI:SS'));
    
    DBMS_SNAPSHOT.REFRESH(LIST => 'APPS.XXWC_OB_CT_PRC_ADJ_TST_MV
                                  ,APPS.XXWC_OB_CT_VNDR_QUOTE_TST_MV
                                  ,APPS.XXWC_OB_CT_MTL_ITM_CAT_TST_MV
                                  ,APPS.XXWC_OB_CT_PO_PRICE_TST_MV                                   
                                  ,APPS.XXWC_OB_ORD_LN_COST_TST_MV'
                            , PUSH_DEFERRED_RPC => FALSE
                            , REFRESH_AFTER_ERRORS =>FALSE
                            , PURGE_OPTION=>0
                            , PARALLELISM =>0
                            , ATOMIC_REFRESH=>FALSE
                            , NESTED=> TRUE);
    
    fnd_file.put_line(fnd_file.log,'Group 3 Refresh End Time '||to_char(sysdate, 'MM/DD/YYYY HH24:MI:SS'));

    
   fnd_file.put_line(fnd_file.log,' ');
    fnd_file.put_line(fnd_file.log,'Executing DBMS_SNAPSHOT.REFRESH for Group 4 MVs');
    fnd_file.put_line(fnd_file.log,'Group 4 Refresh Start Time '||to_char(sysdate, 'MM/DD/YYYY HH24:MI:SS'));
    
    DBMS_SNAPSHOT.REFRESH('XXWC_OB_CUST_TRX_INCR_TST_MV'
                            , PUSH_DEFERRED_RPC => FALSE
                            , REFRESH_AFTER_ERRORS =>FALSE
                            , PURGE_OPTION=>0
                            , PARALLELISM =>0
                            , ATOMIC_REFRESH=>FALSE
                            , NESTED=> TRUE);
                            
    fnd_file.put_line(fnd_file.log,'Group 4 Refresh End Time '||to_char(sysdate, 'MM/DD/YYYY HH24:MI:SS'));       
    
    
    fnd_file.put_line(fnd_file.log,'SubMV and MV Refresh Complete');
    
    commit;  
    
      
exception
when others then
    l_error_message2 := 'xxwc_bko_visibility_std_prc '
                || 'Error_Stack...'
                || DBMS_UTILITY.format_error_stack ()
                || ' Error_Backtrace...'
                || DBMS_UTILITY.format_error_backtrace ();

    xxcus_error_pkg.xxcus_error_main_api (
        p_called_from         => 'xxwc_obiee_trans_mv_refresh'
       ,p_calling             => 'xxwc_obiee_trans_mv_refresh'
       ,p_request_id          => l_conc_req_id
       ,p_ora_error_msg       => l_error_message2
       ,p_error_desc          => 'Error running XXWC OBIEE Customer Trx MV Refresh'
       ,p_distribution_list   => 'HDSOracleDevelopers@hdsupply.com'
       ,p_module              => 'OBIEE');

    dbms_output.put_line ('Error in main xxwc_obiee_trans_mv_refresh. Error: '||l_error_message2);  
    fnd_file.put_line(fnd_file.log, 'Error in main xxwc_obiee_trans_mv_refresh. Error: '||l_error_message2);
    
    errbuf := l_error_message2;
    retcode := '2';
end;
/