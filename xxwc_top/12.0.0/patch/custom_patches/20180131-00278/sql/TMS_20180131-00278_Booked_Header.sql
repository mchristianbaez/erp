  set serveroutput on;
 /*************************************************************************
      PURPOSE:  Booked header Lines not cancelled

      REVISIONS:
      Ver        Date          Author             Description
      ---------  ------------  ------------       ------------------
      1.0        06-FEB-2018  Sundaramoorthy     Initial Version - TMS #20180131-00278 
	************************************************************************/ 
 BEGIN
 dbms_output.put_line ('Start Update ');

 UPDATE oe_order_headers_all
  SET  flow_status_code ='CANCELLED'
  , open_flag ='N'
   WHERE header_id =67776178;  
  
  dbms_output.put_line ('Update count '||SQL%ROWCOUNT);
  COMMIT;
  
  EXCEPTION
  WHEN OTHERS THEN
    ROLLBACK;
	dbms_output.put_line ('Inside Exception '|| SUBSTR(SQLERRM,1,2000));	
END;
/	