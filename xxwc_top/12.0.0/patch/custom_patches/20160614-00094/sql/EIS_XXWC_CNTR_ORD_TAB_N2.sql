-----------------------------------------------------------------------------------------------------------------------------
/*************************************************************************************************************************
  $Header EIS_XXWC_CNTR_ORD_TAB_N2
  File Name: EIS_XXWC_CNTR_ORD_TAB_N2.sql
  PURPOSE:   
  REVISIONS:
     Ver        Date         Author         Description
     ---------  ----------   --------    -----------------------------------------------------------------------------------
     1.0        06-JUL-2016  SIVA        TMS#20160614-00094 Orders Exception Report - Program Running for longer time
****************************************************************************************************************************/
CREATE INDEX XXEIS.EIS_XXWC_CNTR_ORD_TAB_N2 ON XXEIS.EIS_XXWC_CNTR_ORD_TAB (HEADER_ID,LINE_ID,PROCESS_ID) TABLESPACE APPS_TS_TX_DATA
/
