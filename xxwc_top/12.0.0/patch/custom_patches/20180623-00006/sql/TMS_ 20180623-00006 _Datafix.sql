/***********************************************************************************************************************************************
   NAME:       TMS_ 20180623-00006_Datafix.sql
   PURPOSE:  Data fix

   REVISIONS:
   Ver        Date        Author           Description
   ---------  ----------  ---------------  ----------------------------------------------------------------------------------------------------
   1.0        06/23/2018  Rakesh Patel     TMS# 20180623-00006
************************************************************************************************************************************************/
SET SERVEROUTPUT ON SIZE 1000000

BEGIN
   DBMS_OUTPUT.put_line ('Before Update');

   UPDATE apps.wsh_delivery_details
      SET OE_INTERFACED_FLAG = 'Y'
    WHERE delivery_detail_id =29218379;

   DBMS_OUTPUT.put_line ('Records Update -' || SQL%ROWCOUNT);

   COMMIT;

   EXCEPTION
   WHEN OTHERS
   THEN
      DBMS_OUTPUT.put_line ('Unable to update record ' || SQLERRM);
	  ROLLBACK;
END;
/