CREATE OR REPLACE PACKAGE BODY APPS.XXCUS_OZF_YTD_PVT
-- Change History:
-- Ticket#                                  Author                       ESMS           Date                  Version       Comment
-- ===================    ================ ========= ============ ========= ===================================================================================
-- TMS  20160811-00317  Balaguru Seshadri  437126      08/11/2016     1.0              Add routine to extract zero dollar tier into a copy of the original table as well as for YTD Income
as
  --
  ln_request_id NUMBER :=0;
  --
  procedure print_log(p_message in varchar2) is
  begin
   if fnd_global.conc_request_id >0 then
    fnd_file.put_line(fnd_file.log, p_message);
   else
    dbms_output.put_line(p_message);
   end if;
  exception
   when others then
    fnd_file.put_line(fnd_file.log, 'Issue in print_log routine ='||sqlerrm);
  end print_log;
  --
  procedure extract_tierzero
  (
    retcode               out number
   ,errbuf                out varchar2
   ,p_period              in  varchar2
  ) is
  --
   v_sql VARCHAR2(4000) :=Null;
   N_Count NUMBER;
   b_proceed BOOLEAN;
   --
   v_del_sql varchar2(4000) :=Null;
   sql_my_trx  varchar2(20000);
   l_partition_for_delete_oper varchar2(10);
   l_partition_created boolean;
   --
   b_backup  BOOLEAN;
   v_loc     varchar2(3);
   l_del_partition_days_after number :=90;
   --
   l_period  varchar2(10) :=Null;
   l_count number :=0;
   --
   l_result boolean;
   v_merge_sql varchar2(4000) :=Null;
   --
   l_fiscal_month varchar2(3);
   l_fiscal_period varchar2(10);
   l_fiscal_partition varchar2(10);
   --
   l_created_from date;
   l_created_to date;
   l_chr13 varchar2(1) :='
';
   --
  --
  procedure setup_vol_tier_zero_partition
  (
    p_fiscal_period  in    varchar2
   ,p_result              out  boolean
  )  
  is
   --
   l_split_partition_sql  varchar2(4000) :=Null;
   l_drop_partition_sql varchar2(4000) :=Null;
   --
   l_request_id number :=fnd_global.conc_request_id;
   l_user_id number :=fnd_global.user_id;
   --
    cursor fiscal_periods  is
    select period_name                                         fiscal_period
               ,substr(upper(period_name), 1, 3) fiscal_month
               ,upper(replace(period_name, '-', '')) fiscal_partition
               ,start_date                                                 fiscal_start_date
               ,end_date                                                   fiscal_end_date
               ,(
                    select count(utp.partition_name)
                    from dba_tab_partitions  utp
                    where 1 =1
                      and utp.table_owner ='XXCUS'
                      and utp.table_name  ='XXCUS_OZF_TIER0_ACCRUALS_STG'
                      and utp.partition_name =upper(replace(p_fiscal_period, '-', ''))
                ) partition_count
    from gl_periods
    where 1 =1
    and period_set_name ='4-4-QTR'
    and period_name =p_fiscal_period
    and adjustment_period_flag ='N'; 
   --
  begin
     -- 
       --
           begin 
               --
               print_log(' ');
               print_log('Checking to see if we need to remove any partitions that are older than 90 days. If so we will drop them for good.');
               print_log(' ');
               --                                      
               for rec in (select * from xxcus.xxcus_ozf_tier0_partitions_stg where (trunc(sysdate) -trunc(last_update_date)) >= l_del_partition_days_after ) loop
                 --
                    select 'ALTER TABLE XXCUS.XXCUS_OZF_TIER0_ACCRUALS_STG DROP PARTITION '||rec.partition_name
                    into l_drop_partition_sql
                    from dual;
               --
                       print_log(' ');
                       print_log(l_drop_partition_sql);
                       print_log(' ');
                       print_log('...Over 90 days...before dropping partition '||rec.partition_name);
                       execute immediate  l_drop_partition_sql;
                       print_log('......Over 90 days...after dropping partition '||rec.partition_name);           
                 --                   
               end loop;
               l_drop_partition_sql :=Null;
                 --        
           exception
            when others then
             print_log ('Error in removal of partition over 90 days, message :'||sqlerrm);
           end;      
       --     
     --  
     for rec in fiscal_periods loop
      --
       l_partition_for_delete_oper :=rec.fiscal_partition;
       l_fiscal_month :=rec.fiscal_month;
       l_fiscal_period :=rec.fiscal_period;
       l_fiscal_partition :=rec.fiscal_partition;       
       l_created_from :=rec.fiscal_start_date;
       l_created_to :=rec.fiscal_end_date;
      --      
      if (rec.partition_count =0) then
       --
           begin 
               --
               l_split_partition_sql :=Null;
               --
                    select 'ALTER TABLE XXCUS.XXCUS_OZF_TIER0_ACCRUALS_STG '||'
                           '||'SPLIT PARTITION NA VALUES ('||''''||rec.fiscal_partition||''''||') '||'
                           '||' INTO '||'
                           '||' ( '||'
                           '||' PARTITION '||rec.fiscal_partition||','||'
                           '||' PARTITION NA '||'
                           '||')' 
                    into l_split_partition_sql
                    from dual;
               --
                   print_log(' ');
                   print_log(l_split_partition_sql);
                   print_log(' ');
                   print_log('...Before creating partition '||rec.fiscal_partition);
                   execute immediate  l_split_partition_sql;
                   print_log('......After creating partition '||rec.fiscal_partition);           
               --
               begin 
                --
                savepoint square1;
                -- 
                   insert into xxcus.xxcus_ozf_tier0_partitions_stg
                    (
                      partition_name
                     ,creation_date
                     ,created_by
                     ,request_id                 
                    )
                   values
                    (
                      rec.fiscal_partition
                     ,trunc(sysdate)
                     ,fnd_global.user_id
                     ,fnd_global.conc_request_id
                    );
                --
               exception
                when others then
                 rollback to square1;
                 print_log('Error in insert of table xxcus.xxcus_ozf_tier0_partitions_stg, message : '||sqlerrm);
               end;
               --
               l_partition_created :=TRUE;
               --               
               p_result :=TRUE;
               --
           exception
            when others then
             p_result :=FALSE;
           end;
       --
      else
           --Partition exists...no need to create...move on     
               begin 
                --
                savepoint square1;
                -- 
                   update xxcus.xxcus_ozf_tier0_partitions_stg
                    set last_update_date =sysdate
                          ,last_updated_by =fnd_global.user_id
                    where 1 =1
                         and partition_name =rec.fiscal_partition;
                --
               exception
                when others then
                 rollback to square1;
                 print_log('Error in insert of table xxcus.xxcus_ozf_tier0_partitions_stg, message : '||sqlerrm);
               end;
               --            
           print_log(' ');
           print_log('TM volume tier zero accrual table [xxcus.xxcus_ozf_tier0_accruals_stg] partition '||rec.fiscal_partition||' already exists.');
           print_log(' ');
           p_result :=TRUE;
           l_partition_created :=FALSE;
       --
      end if;
      --
     end loop;     
     --
  exception
   when others then
    print_log( 'Issue in setup_vol_tier_zero_partition routine ='||sqlerrm);
    p_result :=FALSE;
  end setup_vol_tier_zero_partition;  
  --  
  procedure pop_accruals_mvid_info_S is
  begin
   --
    v_merge_sql :='Merge /*+ PARALLEL (M1 4) */ into xxcus.xxcus_ozf_tier0_accruals_stg partition ('||l_fiscal_partition||') M1 '||'
    '||' using (
            select hzca.attribute2      attribute2
                  ,hzp.party_name       party_name
                  ,hzca.cust_account_id cust_account_id
            from   hz_cust_accounts hzca
                  ,hz_parties       hzp
            where  1 =1
              and  hzp.party_id =hzca.party_id
           ) s
    on (
          (M1.ofu_cust_account_id = s.cust_account_id)
       )
    when matched then update set M1.ofu_mvid =s.attribute2, M1.ofu_mvid_name =s.party_name';
    --
   print_log('M1 SQL: ');
   print_log('========');
   print_log(v_merge_sql);
   print_log(' ');
   --
    print_log ('Begin Merge 1: Time: '||to_char(sysdate, 'DD-MON-YYYY HH24:MI:SS'));
    execute immediate 'begin ' || v_merge_sql || '; :x := sql%rowcount; end;' using OUT l_count;
    print_log ('End Merge 1: Time: '||to_char(sysdate, 'DD-MON-YYYY HH24:MI:SS'));
    --
   print_log('Merge M1 complete:  total records :'||l_count);
   --
   commit;
   --
  exception
   when others then
    print_log ('@pop_accruals_mvid_info_S, message ='||sqlerrm);
  end pop_accruals_mvid_info_S;
  --
  procedure pop_branch_info_S is
  begin
   --
        v_merge_sql :='Merge /*+ PARALLEL (M4 8) */ into xxcus.xxcus_ozf_tier0_accruals_stg partition ('||l_fiscal_partition||') M4 '||'
        '||' using (
                        select hzca.attribute2      attribute2
                              ,hzp.party_name       party_name
                              ,hzca.account_number  account_number
                              ,hzca.cust_account_id cust_account_id
                        from   hz_cust_accounts hzca
                              ,hz_parties       hzp
                        where  1 =1
                          and  hzp.party_id =hzca.party_id
               ) s
        on (
              (M4.billto_cust_acct_id_br = s.cust_account_id)
           )
        when matched then update set M4.billto_cust_acct_br_name =s.party_name, M4.billto_cust_acct_br_acct_name =s.account_number';
        --
           print_log('M4 SQL: ');
           print_log('========');
           print_log(v_merge_sql);
           print_log(' ');
       --
        print_log ('Begin Merge 4: Time: '||to_char(sysdate, 'DD-MON-YYYY HH24:MI:SS'));
        execute immediate 'begin ' || v_merge_sql || '; :x := sql%rowcount; end;' using OUT l_count;
        print_log ('End Merge 4: Time: '||to_char(sysdate, 'DD-MON-YYYY HH24:MI:SS'));
        --
       print_log('Merge M4 complete:  total records :'||l_count);
       --
       commit;
       --
  exception
   when others then
    print_log ('@pop_branch_info_S, message ='||sqlerrm);
  end pop_branch_info_S;
  --
  procedure pop_posted_fin_loc_info_S is
  begin
   --
        v_merge_sql :='Merge /*+ PARALLEL (M7 8) */ into xxcus.xxcus_ozf_tier0_accruals_stg partition ('||l_fiscal_partition||') M7 '||'
        '||' using (
                             select * from xxcus.xxcus_ozf_override_fin_loc_b
                          ) s
        on (
                 (        
                    M7.resale_line_attribute2 =s.BU
                   --M7.resale_line_attribute11 =s.product
                   --and M7.resale_line_attribute12 =s.override_fin_loc
                   and M7.coop_yes_no             =''Y''
                   and s.override_fin_loc_y_n    =''Y''
                 )
           )
        when matched then update set M7.posted_fin_loc =s.override_fin_loc, M7.override_fin_loc =s.override_fin_loc_y_n';
        --
           print_log('M7 SQL: ');
           print_log('========');
           print_log(v_merge_sql);
           print_log(' ');
       --
       print_log ('Begin Merge 7: Time: '||to_char(sysdate, 'DD-MON-YYYY HH24:MI:SS'));
        execute immediate 'begin ' || v_merge_sql || '; :x := sql%rowcount; end;' using OUT l_count;
        print_log ('End Merge 7: Time: '||to_char(sysdate, 'DD-MON-YYYY HH24:MI:SS'));
        --
       print_log('Merge M7 complete:  total records :'||l_count);
       --
       commit;
       --
  exception
   when others then
    rollback;
    print_log ('@pop_posted_fin_loc_info_S, message ='||sqlerrm);
  end pop_posted_fin_loc_info_S;
  --
  procedure pop_posted_bu_branch_info_S is
  begin
        --
        v_merge_sql :='Merge /*+ PARALLEL (M10 8) */ into xxcus.xxcus_ozf_tier0_accruals_stg partition ('||l_fiscal_partition||') M10 '||'
        '||' using (
                                select /*+ RESULT_CACHE */
                                       xlcv.entrp_loc     entrp_loc
                                      ,xlcv.lob_branch    lob_branch
                                      ,xrbxt.rebates_fru  rebates_fru
                                from apps.xxcus_location_code_vw xlcv
                                    ,(
                                       select a.rebates_fru
                                       from   xxcus.xxcus_rebate_branch_xref_tbl a
                                       where  1 =1
                                         and  rowid =
                                                (
                                                  select min(rowid)
                                                  from   xxcus.xxcus_rebate_branch_xref_tbl b
                                                  where  1 =1
                                                    and  b.rebates_fru =a.rebates_fru
                                                )
                                     ) xrbxt
                                    ,fnd_lookup_values_vl lv
                                where 1 =1
                                  and xrbxt.rebates_fru = xlcv.fru
                                  and lv.lookup_type = ''XXCUS_BUSINESS_UNIT''
                                  and xlcv.business_unit = lv.lookup_code
                                order by 1, 2
                          ) s
        on (
                            M10.posted_fin_loc   =s.entrp_loc
                    and M10.coop_yes_no      =''Y''
                    and M10.override_fin_loc =''Y''
           )
        when matched then update set M10.posted_bu_branch =s.lob_branch';
        --
           print_log('M10 SQL: ');
           print_log('========');
           print_log(v_merge_sql);
           print_log(' ');
       --
       print_log ('Begin Merge 10: Time: '||to_char(sysdate, 'DD-MON-YYYY HH24:MI:SS'));
        execute immediate 'begin ' || v_merge_sql || '; :x := sql%rowcount; end;' using OUT l_count;
        print_log ('End Merge 10: Time: '||to_char(sysdate, 'DD-MON-YYYY HH24:MI:SS'));
        --
       print_log('Merge M10 complete:  total records :'||l_count);
       --
       commit;
       --
  exception
   when others then
    rollback;
    print_log ('@pop_posted_bu_branch_info_S, message ='||sqlerrm);
  end pop_posted_bu_branch_info_S;
  -- Main Processing begins
  begin
   --
    begin
        --
           print_log('Delete SQL: ');
           print_log('==========');
           print_log(' delete /*+ noparallel */  xxcus.xxcus_ozf_override_fin_loc_b');
           print_log(' ');
        delete /*+ noparallel */  xxcus.xxcus_ozf_override_fin_loc_b;
        --
        commit;
        --
        insert /*+ noparallel */ into  xxcus.xxcus_ozf_override_fin_loc_b
        (
         product
        ,override_fin_loc
        ,override_fin_loc_y_n
        ,bu 
        )
        (
        select distinct trim(attribute4) product, attribute5 override_fin_loc, nvl(attribute11, 'N') override_fin_loc_y_n
        ,party_name 
        from   ar.hz_parties p
        where  1 =1
          and  attribute1 = 'HDS_BU'
          and  (attribute4 is not null and attribute5 is not null)
        );
        --
        commit;
        --
    exception
     when program_error then
      print_log('PROGRAM ERROR EXCEPTION IN INSERT OF xxcus.xxcus_ozf_override_fin_loc_b, MSG = '||sqlerrm);
      raise;
     when others then
      print_log('OTHERS EXCEPTION IN INSERT OF xxcus.xxcus_ozf_override_fin_loc_b, MSG ='||sqlerrm);
      raise;
    end;
    --   
   --
   begin
    --
    setup_vol_tier_zero_partition (p_fiscal_period =>p_period, p_result =>l_result);
    --
    if (l_result) then
       b_proceed :=TRUE;
    else
       b_proceed :=FALSE;
    end if;
    --
    print_log('');
    --
    if (b_proceed) then
          --
          if (NOT l_partition_created) then --When the partition exists there is a good chance we've some data already populated, lets remove and extract again
           -- *****
              begin
                select 'DELETE /*+ PARALLEL (D1 2) */ XXCUS.XXCUS_OZF_TIER0_ACCRUALS_STG partition ('  
                     ||l_partition_for_delete_oper
                     ||') D1 '
                into v_del_sql
                from dual;
                --
                   print_log('Delete SQL: ');
                   print_log('===========');
                   print_log(v_del_sql);
                   print_log(' ');
                --
                print_log ('Begin Delete SQL: Time: '||to_char(sysdate, 'DD-MON-YYYY HH24:MI:SS'));
                print_log(' ');
                execute immediate 'begin ' || v_del_sql || '; :x := sql%rowcount; end;' using OUT l_count;
                print_log ('End Delete SQL: Time: '||to_char(sysdate, 'DD-MON-YYYY HH24:MI:SS'));
                --
                b_proceed :=TRUE;
                --
              exception
               when others then
                b_proceed :=FALSE;
              end;
              --
              print_log(' ');
              --
              if l_count =0 then
               print_log ('Action -Cleanup: No TM volume tier zero accrual entries deleted '||
                          'using partition '||l_partition_for_delete_oper||
                          ' from table XXCUS.XXCUS_OZF_TIER0_ACCRUALS_STG'
                         );
              else
               print_log ('Action -Cleanup: Successfully deleted '|| l_count ||
                          ' existing TM volume tier zero accrual entries ' ||
                          ' using partition '||l_partition_for_delete_oper||
                          ' from table XXCUS.XXCUS_OZF_TIER0_ACCRUALS_STG'
                         );
              end if;
              --
              COMMIT;
              --           
           -- *****
          else
           b_proceed  :=TRUE; --Delete was not run so lets keep the variable b_proceed true so we can proceed further
          end if;
    else
      b_proceed :=FALSE; -- leave b_proceed with FALSE value so further steps will fail to kick off
    end if;
    --
   exception
    --
    when others then
     --
     b_proceed :=FALSE;
     print_log('Error in delete of xxcus.xxcus_ozf_tier0_accruals_stg records for period ='||p_period||', msg ='||sqlerrm);
   end;
   --
   if (NOT b_proceed) then
    --
    print_log('Either partition setup process or delete of existing partition failed, extract process will quit now, please check the concurrent log for error messages');
    --
   else
    --
    -- begin insert from a specific partition based on fiscal period
    --
    begin
     --
     sql_my_trx :=
        'INSERT INTO /*+ APPEND PARALLEL 4 */ xxcus.xxcus_ozf_tier0_accruals_stg
        (
            SELECT /*+ PARALLEL(ofu 2) PARALLEL(rslines 2) PARALLEL(offers 2) PARALLEL(qph 2) */
                   to_number(null)                 xla_accrual_id
                  ,ofu.request_id                     ofu_request_id
                  ,ofu.utilization_type                   utilization_type
                  ,ofu.adjustment_type                adjustment_type
                  ,ofu.adjustment_type_id             adjustment_type_id
                  ,ofu.object_id                      object_id
                  ,ofu.plan_curr_amount               rebate_amount
                  ,ofu.acctd_amount                   util_acctd_amount
                  ,to_char(null)                              event_type_code
                  ,to_char(null)                    entity_code
                  ,to_number(null)                       event_id
                  ,ofu.utilization_id                 utilization_id
                  ,ofu.org_id                         oxa_org_id
                  ,trunc(ofu.gl_date)                 ofu_gl_date
                  ,hrou.set_of_books_id               ozf_ledger_id
                  ,case
                    when hrou.set_of_books_id =2021 then ''HDS Rebates USA''
                    when hrou.set_of_books_id =2023 then ''HDS Rebates CAN''
                    else Null
                   end                                ozf_ledger_name
                  ,to_number(null)                     xlae_entity_id
                  ,to_date(null)       xlae_trxn_date
                  ,to_date(null)              xlae_event_date
                  ,to_char(null)             xlae_event_status_code
                  ,to_char(null)           xlae_process_status_code
                  ,to_number(null)                  xlae_event_number
                  ,to_char(null)                  xlae_onhold_flag
                  ,to_number(null)                xla_application_id
                  ,to_number(null)                  xaeh_ae_header_id
                  ,to_date(null)               xaeh_accounting_date
                  ,to_char(null)       xaeh_gl_xfer_status_code
                  ,to_date(null)       xaeh_gl_xfer_date
                  ,to_char(null)              xaeh_je_category
                  ,'||''''||l_fiscal_month||''''||'           fiscal_month'||'
                  ,'||''''||l_fiscal_period||''''||'        xaeh_period_name'||'
                  ,to_number(null)            accounting_error_id
                  ,to_number(null)            accounting_batch_id
                  ,to_char(null)                   encoded_msg
                  ,to_number(null)                    err_ae_line_num
                  ,to_number(null)                 message_number
                  ,to_char(null)             error_source_code
                  ,offers.offer_id                    offer_id
                  ,offers.offer_code                  offer_code
                  ,qphtl.description                  offer_name
                  ,offers.offer_type                  offer_type
                  ,offers.autopay_party_id            autopay_party_id
                  ,offers.beneficiary_account_id      beneficiary_account_id
                  ,offers.status_code                 offer_status_code
                  ,offers.custom_setup_id             offer_custom_setup_id
                  ,offers.user_status_id              offer_user_status_id
                  ,offers.owner_id                    offer_owner_id
                  ,offers.activity_media_id           activity_media_id
                  ,ams.media_name                     activity_media_name
                  ,offers.autopay_flag                offer_autopay_flag
                  ,ofu.currency_code                  ofu_currency_code
                  ,rslines.currency_code              receipts_currency
                  ,qph.attribute1                     auto_renewal
                  ,qph.attribute7                     calendar_year
                  ,qph.attribute2                     until_year
                  ,qph.attribute5                     payment_method
                  ,qph.attribute6                     payment_frequency
                  ,qph.attribute9                     guaranteed_amount
                  ,ofu.plan_id                        qp_list_header_id
                  ,qph.global_flag                    qp_global_flag
                  ,qph.active_flag                    qp_active_flag
                  ,qph.orig_org_id                    qp_orig_org_id
                  ,ofu.year_id                        ofu_year_id
                  ,ofu.fund_id                        fund_id
                  ,(
                    select /*+ RESULT_CACHE */
                           short_name
                    from   ozf_funds_all_tl
                    where  1 =1
                      and  fund_id =ofu.fund_id
                   )                                  fund_name
                  ,ofu.plan_type                      plan_type
                  ,ofu.component_id                   component_id
                  ,ofu.component_type                 component_type
                  ,ofu.object_type                    object_type
                  ,ofu.reference_type                 reference_type
                  ,ofu.reference_id                   reference_id
                  ,ofu.ams_activity_budget_id         ams_activity_budget_id
                  ,ofu.product_level_type             product_level_type
                  ,ofu.product_id                     product_id
                  ,rslines.creation_date              resale_date_created
                  ,trunc(rslines.date_invoiced)       date_invoiced
                  ,rslines.po_number                  po_number
                  ,rslines.purchase_uom_code          purchase_uom_code
                  ,rslines.uom_code                   uom_code
                  ,rslines.purchase_price             purchase_price
                  ,ofu.cust_account_id                ofu_cust_account_id
                  ,Null                               ofu_mvid
                  ,Null                               ofu_mvid_name
                  ,rslines.bill_to_cust_account_id    billto_cust_acct_id_br
                  ,Null                               billto_cust_acct_br_name
                  ,Null                               billto_cust_acct_br_acct_name
                  ,rslines.bill_to_party_id           bill_to_party_id
                  ,rslines.bill_to_party_name         bill_to_party_name_lob
                  ,trunc(rslines.date_ordered)        date_ordered
                  ,rslines.selling_price              selling_price
                  ,rslines.quantity                   quantity
                  ,rslines.inventory_item_id          inventory_item_id
                  ,rslines.item_number                item_number
                  ,rslines.line_attribute1            resale_line_attribute1 --pay_to_vendor_code
                  ,rslines.line_attribute2            resale_line_attribute2 --bu_nm
                  ,rslines.line_attribute3            resale_line_attribute3 --src_sys_nm
                  ,rslines.line_attribute5            resale_line_attribute5 --receipt_number
                  ,rslines.line_attribute6            resale_line_attribute6 --ship_to_branch_code
                  ,rslines.line_attribute11           resale_line_attribute11 --product_segment
                  ,rslines.line_attribute12           resale_line_attribute12 --location_segment
                  ,rslines.line_attribute13           resale_line_attribute13 --freight_discount
                  ,rslines.line_attribute14           resale_line_attribute14 --payment_discount
                  ,ofu.price_adjustment_id            pricing_adj_id
                  ,trunc(ofu.exchange_rate_date)      ofu_exchange_rate_date
                  ,ofu.exchange_rate                  ofu_exchange_rate
                  ,ofu.billto_cust_account_id         ofu_billto_cust_account_id_br
                  ,ofu.product_id                     ofu_product_id
                  ,rslines.sold_from_cust_account_id  sold_from_cust_account_id_mvid
                  ,rslines.sold_from_site_id          sold_from_site_id_mvid
                  ,rslines.sold_from_party_name       sold_from_party_name_mvid
                  ,rslines.line_attribute12           posted_fin_loc
                  ,ams.coop_yes_no                    coop_yes_no
                  ,rslines.line_attribute6            posted_bu_branch
                  ,''N''                                override_fin_loc
                  ,ofu.attribute10                    ofu_attribute10
                  ,trunc(ofu.creation_date)           ofu_creation_date
                  ,'||''''||l_fiscal_partition||''''||'                                                   fiscal_partition '||' 
            '||' from   ozf_funds_utilized_all_b  ofu
                  ,hr_operating_units        hrou
                  ,ozf_offers                offers
                  ,qp_list_headers_b         qph
                  ,qp_list_headers_tl        qphtl
                  ,ozf_resale_lines_all      rslines
                  ,(
                    select amstl.media_name media_name
                          ,amstl.media_id
                          ,case
                            when amstl.description IN (''COOP'', ''COOP_MIN'') then ''Y''
                            else ''N''
                           end   coop_yes_no
                    from   ams_media_tl amstl
                          ,ams_media_b  ams
                    where  1 =1
                      and  ams.media_id =amstl.media_id
                   ) ams
            where  1 =1
                  and ofu.creation_date between '||''''||to_char(l_created_from, 'DD-MON-YY')||''''||' and '||''''||to_char(l_created_to, 'DD-MON-YY')||''''||'
                  and ofu.utilization_type =''ACCRUAL''
                  and ofu.plan_type =''OFFR'' 
                  and ofu.amount =0 --For volume agreements, when the first tier is not met...accruals are created with zero dollar.
                  and not exists
                   (
                        select 1
                        from   ozf_xla_accruals
                        where 1 =1
                        and utilization_id =ofu.utilization_id
                   )            
              and  hrou.organization_id    =ofu.org_id
              and  qphtl.list_header_id         =ofu.plan_id
              and  offers.qp_list_header_id     =qphtl.list_header_id
              and  qph.list_header_id           =qphtl.list_header_id
              and  rslines.resale_line_id(+)    =ofu.object_id
              and  ams.media_id(+)              =offers.activity_media_id
        )';
     --
     print_log('');
     --
     print_log('Insert SQL for Fiscal Period: '||p_period);
     print_log('======================================');
     --
     print_log('');
     --
     print_log(sql_my_trx);
     --
     print_log(' ');
     --
     begin
      --
      print_log('Call 1 - Before calling dynamic insert SQL for period: '||p_period);
      --
      execute immediate sql_my_trx;
      --
      print_log(' ');
      --
      print_log('Call 1 - After calling dynamic insert SQL for period: '||p_period);
      --
      print_log(' ');
      --
      print_log('Call 1 -Total records inserted in xxcus.xxcus_ozf_tier0_accruals_stg for fiscal period '||p_period||' : '||sql%rowcount);
      --
      commit;
      --
      print_log(' ');
      --
        -- begin scrubbing for tier zero records with issues in offer setup where there are two discount lines with zero $$ for all items with no lob
            sql_my_trx :=
            'delete xxcus.xxcus_ozf_tier0_accruals_stg partition ('||l_fiscal_partition||') tier0'||l_chr13
            ||' where 1 =1 '||l_chr13
            ||' and exists    
             (
                select 1
                from   apps.ozf_funds_utilized_all_b c
                where 1 =1
                and c.cust_account_id =tier0.ofu_cust_account_id
                and c.plan_id =tier0.qp_list_header_id
                and c.object_id =tier0.object_id  
                and c.utilization_type =tier0.utilization_type
                and c.plan_type =tier0.plan_type
                and c.acctd_amount <>0   
             )';
         --
         print_log(' ');
         --         
         print_log(sql_my_trx);
         --
         print_log(' ');
         --                         
        -- end scrubbing for tier zero records with issues in offer setup where there are two discount lines with zero $$ for all items with no lob      
          print_log('Call 2 - Begin Scrubbing in tier0 staging table relaeted to issues in offer setup');
          --
          execute immediate sql_my_trx;
          --
          print_log(' ');
          --
         print_log('Call 2 - After Scrubbing in tier0 staging table related to issues in offer setup');
          --
          print_log(' ');
          --
          print_log('Call 2 -Total records deleted in xxcus.xxcus_ozf_tier0_accruals_stg for fiscal period '||p_period||' : '||sql%rowcount);
          --
          commit;
          --
          print_log(' ');
          --        
      --
        begin
         --
         l_period :=p_period;
         --
         v_loc :='103';
         pop_accruals_mvid_info_S;
         Commit;
         print_log ('@ update of accrual attributes, Exit: pop_accruals_mvid_info_S');
         v_loc :='104';
         pop_branch_info_S;
         Commit;
         print_log ('@ update of accrual attributes, Exit: pop_branch_info_S');
         v_loc :='105';
         pop_posted_fin_loc_info_S;
         Commit;
         print_log ('@ update of accrual attributes, Exit: pop_posted_fin_loc_info_S');
         v_loc :='106';
         pop_posted_bu_branch_info_S;
         Commit;
         v_loc :='107';
         print_log ('@ update of accrual attributes, Exit: pop_posted_bu_branch_info_S');
         --
         Commit;
         --
        exception
         when others then
          print_log ('@Accruals attributes update single, last known location ='||v_loc||', Error =>'||sqlerrm);
        end;
       --      
      --
     exception
      --
      when others then
       --
       print_log('@ Period: '||p_period||' error in calling dynamic SQL, message ='||sqlerrm);
       --
     end;
     --
    exception
     when others then
      --
      print_log('@ Period: '||p_period||' error in generating dynamic SQL, message ='||sqlerrm);
      --
    end;
    --
    -- end insert from a specific partition using fiscal period
   end if;
   --
  exception
   when others then
    print_log ('Issue in extract_tierzero routine, message ='||sqlerrm);
    rollback;
  end extract_tierzero;
  -- 
  --;  
  procedure extract_ytdincome
  (
    retcode               out number
   ,errbuf                out varchar2
   ,p_period              in  varchar2
  ) is
  --
   v_sql VARCHAR2(4000) :=Null;
   N_Count NUMBER;
   b_proceed BOOLEAN;
   l_proceed BOOLEAN;
   --
   sql_my_trx  varchar2(20000);
   --
   b_backup  BOOLEAN;
   v_loc     varchar2(3);
   l_period  varchar2(10) :=Null;
   --
   l_valid_user varchar2(30) :=Null;
   lv_period varchar2(10) :=Null; 
   l_message varchar2(2000) :=Null;
   lv_errbuf varchar2(150) :=Null; 
   lv_retcode varchar2(150) :=Null;   
  --
  begin
       --
       -- Check the user running the procedure to see if they are part of the lookup HDS_REBATES_YTD_PRIVATE_USERS. If yes OK otherwise error the job.
       --
    begin 
     select description
     into     l_valid_user
     from  apps.fnd_lookup_values
     where 1 =1
          and lookup_code =fnd_global.user_name;
     --
     b_proceed :=TRUE;
     --
    exception
     when no_data_found then
      b_proceed :=FALSE;
      l_message :='ND: Not able to determine the list of users granted access to the job HDS Rebates: One off Extract YTD Income - Private. '|| 
             'Please check lookup type "HDS_REBATES_YTD_PRIVATE_USERS" under responsibility Application Developer \ Application \Lookups \Common';         
      l_proceed :=fnd_concurrent.set_completion_status('ERROR', l_message);       
     when program_error then
      b_proceed :=FALSE;     
     print_log
             ('PE: Not able to determine the list of users granted access to the job HDS Rebates: One off Extract YTD Income - Private. '|| 
             'Please check lookup type "HDS_REBATES_YTD_PRIVATE_USERS" under responsibility Application Developer \ Application \Lookups \Common'
             );     
      l_proceed :=fnd_concurrent.set_completion_status('ERROR', l_message);                 
     when others then
      b_proceed :=FALSE;     
      l_message :='OTH: Not able to determine the list of users granted access to the job HDS Rebates: One off Extract YTD Income - Private. '|| 
             'Please check lookup type "HDS_REBATES_YTD_PRIVATE_USERS" under responsibility Application Developer \ Application \Lookups \Common'; 
      print_log(l_message);                 
      l_proceed :=fnd_concurrent.set_completion_status('ERROR', l_message);     
    end;
    --
    if (b_proceed) then
       -- 
       begin
          --
          print_log('');
          --
        print_log('Begin tier zero extract process for fiscal period '||p_period);      
        --
         lv_period :=p_period;
         --
            extract_tierzero
              (
                retcode =>lv_retcode
               ,errbuf   =>lv_errbuf
               ,p_period  =>lv_period
              );
        --
        print_log('');
        --
        print_log('End tier zero extract process for fiscal period '||p_period);
        --
        print_log('');
        --
        b_proceed :=TRUE;
        --
       exception
         --  
         when others then
         --
         b_proceed :=FALSE;
         print_log('Error in extract of tier zero data for period ='||p_period||', msg ='||sqlerrm);
       end;   
       -- 
            if (b_proceed) then
                --
               begin
                --
                print_log('');
                --
                savepoint start_here;
                --
                 delete xxcus.xxcus_ytd_income_stg
                 where  1 =1
                   and  fiscal_period =p_period;
                --
                print_log('');
                --
                print_log('Deleted '||sql%rowcount||' records from xxcus.xxcus_ytd_income_stg for fiscal period '||p_period);
                --
                 delete xxcus.xxcus_ytd_income_stg
                 where  1 =1
                   and  fiscal_period ='NA';
                --
                print_log('');
                --
                print_log('Deleted '||sql%rowcount||' records from xxcus.xxcus_ytd_income_stg for fiscal period: NA');
                --
                b_proceed :=TRUE;
                --
                print_log('');
                --
               exception
                --
                when others then
                 --
                 b_proceed :=FALSE;
                 rollback to start_here;
                 print_log('Error in delete of xxcus.xxcus_ytd_income_stg records for period ='||p_period||', msg ='||sqlerrm);
               end;
               -- end delete    
            --   
           end if; -- 
       --
       if (NOT b_proceed) then
        --
        print_log('Delete routine failed, extract process will quit now, please check the concurrent log for error messages');
        --
       else
        --
        -- begin insert from a specific partition other than NA
        --
        begin
         --
         sql_my_trx :=
            'INSERT INTO /*+ APPEND */ xxcus.xxcus_ytd_income_stg
            (
            SELECT
                        XLA.XAEH_PERIOD_NAME                 FISCAL_PERIOD,
                        XLA.OFU_MVID                         MVID,
                        XLA.OFU_CUST_ACCOUNT_ID              OFU_CUST_ACCOUNT_ID,
                        (
                         SELECT /*+ RESULT_CACHE */
                                HZP.PARTY_NAME
                         FROM   HZ_PARTIES HZP,
                                HZ_CUST_ACCOUNTS HZCA
                         WHERE  1 =1
                           AND  HZCA.CUST_ACCOUNT_ID
                               =XLA.OFU_CUST_ACCOUNT_ID
                           AND  HZP.PARTY_ID
                               =HZCA.PARTY_ID
                        )                                    VENDOR_NAME,
                        XLA.BILL_TO_PARTY_NAME_LOB           LOB,
                        XLA.BILL_TO_PARTY_ID                 LOB_ID,
                        XLA.RESALE_LINE_ATTRIBUTE2           BU,
                        XLA.RESALE_LINE_ATTRIBUTE6           BRANCH,
                        XLA.BILLTO_CUST_ACCT_BR_ACCT_NAME    CUST_ACCT_BRANCH_NAME,
                        XLA.POSTED_FIN_LOC                   POSTED_FIN_LOC,
                        XLA.POSTED_BU_BRANCH                 POSTED_BU_BRANCH,
                        XLA.OFU_ATTRIBUTE10                  OFU_ATTRIBUTE10,
                        XLA.UTILIZATION_TYPE                 UTILIZATION_TYPE,
                        XLA.ADJUSTMENT_TYPE                  ADJUSTMENT_TYPE,
                        XLA.ADJUSTMENT_TYPE_ID               ADJUSTMENT_TYPE_ID,
                        XLA.OFU_CURRENCY_CODE                CURRENCY_CODE,
                        XLA.OZF_LEDGER_NAME                  LEDGER_NAME,
                        XLA.QP_LIST_HEADER_ID                PLAN_ID,
                        (
                         SELECT /*+ RESULT_CACHE */
                                QPLH.DESCRIPTION
                         FROM   QP_LIST_HEADERS_TL QPLH
                         WHERE  1 =1
                           AND  QPLH.LIST_HEADER_ID
                               =XLA.QP_LIST_HEADER_ID
                        )                                    OFFER_NAME,
                        OO.STATUS_CODE                       STATUS,
                        CASE
                         when XLA.ACTIVITY_MEDIA_NAME IN (''Coop'', ''Coop Minimum'') then ''COOP''
                         else ''REBATE''
                        END                                  REBATE_TYPE,
                        TO_NUMBER(XLA.CALENDAR_YEAR)         AGREEMENT_YEAR,
                        SUBSTR (T.month_id, 0, 4)            CALENDAR_YEAR,
                        T.ENT_YEAR_ID                        FISCAL_YEAR,
                        GLP.PERIOD_NUM                       FISCAL_MONTH_ID,
                        T.ENT_PERIOD_ID                      PERIOD_ID,
                        SUBSTR (T.MONTH_ID, 6, 2)            CALENDAR_MONTH_ID,
                        SUM(SELLING_PRICE*QUANTITY)          PURCHASES,
                        SUM (XLA.util_acctd_amount)          ACCRUAL_AMOUNT,
                        SYSDATE                              CREATION_DATE,
                        FND_GLOBAL.USER_ID                   CREATED_BY,
                        XLA.OXA_ORG_ID                       ORG_ID,
                        XLA.XLAE_PROCESS_STATUS_CODE         PROCESS_STATUS_CODE,
                        XLA.XLAE_EVENT_STATUS_CODE           EVENT_STATUS_CODE,
                        XLA.XAEH_GL_XFER_STATUS_CODE         GL_TRANSFER_STATUS_CODE
                   FROM XXCUS.XXCUS_OZF_XLA_ACCRUALS_B  PARTITION ('||UPPER(SUBSTR(p_period, 1, 3))||') XLA
                       ,APPS.OZF_TIME_DAY              T
                       ,GL_PERIODS                     GLP
                       ,OZF_OFFERS                     OO
                  WHERE 1 = 1
                    AND XLA.OFU_GL_DATE        =T.REPORT_DATE
                    AND XLA.XAEH_PERIOD_NAME   ='||''''||p_period||''''||'
                    AND GLP.PERIOD_SET_NAME    =''4-4-QTR''
                    AND glp.period_name =xla.xaeh_period_name
                    AND GLP.ADJUSTMENT_PERIOD_FLAG =''N''
                    AND T.REPORT_DATE BETWEEN GLP.START_DATE AND GLP.END_DATE
                    AND OO.OFFER_ID            =XLA.OFFER_ID
                    --AND OO.STATUS_CODE         <>''DRAFT''
               GROUP BY
                        XLA.XAEH_PERIOD_NAME,                    --FISCAL_PERIOD
                        XLA.OFU_MVID,                            --MVID,
                        XLA.OFU_CUST_ACCOUNT_ID,                 --CUST_ID,
                        XLA.BILL_TO_PARTY_NAME_LOB,              --LOB,
                        XLA.BILL_TO_PARTY_ID,                    --LOB_ID,
                        XLA.RESALE_LINE_ATTRIBUTE2,              --BU,
                        XLA.RESALE_LINE_ATTRIBUTE6,              --BRANCH
                        XLA.BILLTO_CUST_ACCT_BR_ACCT_NAME,       --CUST_ACCT_BRANCH_NAME
                        XLA.POSTED_FIN_LOC,                      --POSTED_FIN_LOCATION
                        XLA.POSTED_BU_BRANCH,                    --POSTED_BU_BRANCH
                        XLA.OFU_ATTRIBUTE10,                     --OFU_ATTRIBUTE10,
                        XLA.UTILIZATION_TYPE,                    --UTILIZATION_TYPE
                        XLA.ADJUSTMENT_TYPE,                     --ADJUSTMENT_TYPE
                        XLA.ADJUSTMENT_TYPE_ID,                  --ADJUSTMENT_TYPE_ID
                        XLA.OFU_CURRENCY_CODE,                   --CURRENCY_CODE
                        XLA.OZF_LEDGER_NAME,                     --LEDGER_NAME
                        XLA.QP_LIST_HEADER_ID,                   --PLAN_ID,
                        OO.STATUS_CODE,                          --STATUS,
                        CASE
                         when XLA.ACTIVITY_MEDIA_NAME IN (''Coop'', ''Coop Minimum'') then ''COOP''
                         else ''REBATE''
                        END,
                        TO_NUMBER(XLA.CALENDAR_YEAR),            --AGREEMENT_YEAR,
                        SUBSTR (T.month_id, 0, 4),               --CALENDAR_YEAR,
                        T.ENT_YEAR_ID,                           --FISCAL_YEAR,
                        T.ENT_PERIOD_ID,                         --PERIOD_ID
                        GLP.PERIOD_NUM,                          --FISCAL_MONTH_ID
                        SUBSTR (T.MONTH_ID, 6, 2),
                        XLA.OXA_ORG_ID,
                        XLA.XLAE_PROCESS_STATUS_CODE,
                        XLA.XLAE_EVENT_STATUS_CODE,
                        XLA.XAEH_GL_XFER_STATUS_CODE
                  UNION
            SELECT
                        XLA.XAEH_PERIOD_NAME                 FISCAL_PERIOD,
                        XLA.OFU_MVID                         MVID,
                        XLA.OFU_CUST_ACCOUNT_ID              OFU_CUST_ACCOUNT_ID,
                        (
                         SELECT /*+ RESULT_CACHE */
                                HZP.PARTY_NAME
                         FROM   HZ_PARTIES HZP,
                                HZ_CUST_ACCOUNTS HZCA
                         WHERE  1 =1
                           AND  HZCA.CUST_ACCOUNT_ID
                               =XLA.OFU_CUST_ACCOUNT_ID
                           AND  HZP.PARTY_ID
                               =HZCA.PARTY_ID
                        )                                    VENDOR_NAME,
                        XLA.BILL_TO_PARTY_NAME_LOB           LOB,
                        XLA.BILL_TO_PARTY_ID                 LOB_ID,
                        XLA.RESALE_LINE_ATTRIBUTE2           BU,
                        XLA.RESALE_LINE_ATTRIBUTE6           BRANCH,
                        XLA.BILLTO_CUST_ACCT_BR_ACCT_NAME    CUST_ACCT_BRANCH_NAME,
                        XLA.POSTED_FIN_LOC                   POSTED_FIN_LOC,
                        XLA.POSTED_BU_BRANCH                 POSTED_BU_BRANCH,
                        XLA.OFU_ATTRIBUTE10                  OFU_ATTRIBUTE10,
                        XLA.UTILIZATION_TYPE                 UTILIZATION_TYPE,
                        XLA.ADJUSTMENT_TYPE                  ADJUSTMENT_TYPE,
                        XLA.ADJUSTMENT_TYPE_ID               ADJUSTMENT_TYPE_ID,
                        XLA.OFU_CURRENCY_CODE                CURRENCY_CODE,
                        XLA.OZF_LEDGER_NAME                  LEDGER_NAME,
                        XLA.QP_LIST_HEADER_ID                PLAN_ID,
                        (
                         SELECT /*+ RESULT_CACHE */
                                QPLH.DESCRIPTION
                         FROM   QP_LIST_HEADERS_TL QPLH
                         WHERE  1 =1
                           AND  QPLH.LIST_HEADER_ID
                               =XLA.QP_LIST_HEADER_ID
                        )                                    OFFER_NAME,
                        OO.STATUS_CODE                       STATUS,
                        CASE
                         when XLA.ACTIVITY_MEDIA_NAME IN (''Coop'', ''Coop Minimum'') then ''COOP''
                         else ''REBATE''
                        END                                  REBATE_TYPE,
                        TO_NUMBER(XLA.CALENDAR_YEAR)         AGREEMENT_YEAR,
                        SUBSTR (T.month_id, 0, 4)            CALENDAR_YEAR,
                        T.ENT_YEAR_ID                        FISCAL_YEAR,
                        GLP.PERIOD_NUM                       FISCAL_MONTH_ID,
                        T.ENT_PERIOD_ID                      PERIOD_ID,
                        SUBSTR (T.MONTH_ID, 6, 2)            CALENDAR_MONTH_ID,
                        SUM(SELLING_PRICE*QUANTITY)          PURCHASES,
                        SUM (XLA.util_acctd_amount)          ACCRUAL_AMOUNT,
                        SYSDATE                              CREATION_DATE,
                        FND_GLOBAL.USER_ID                   CREATED_BY,
                        XLA.OXA_ORG_ID                       ORG_ID,
                        XLA.XLAE_PROCESS_STATUS_CODE         PROCESS_STATUS_CODE,
                        XLA.XLAE_EVENT_STATUS_CODE           EVENT_STATUS_CODE,
                        XLA.XAEH_GL_XFER_STATUS_CODE         GL_TRANSFER_STATUS_CODE
                   FROM XXCUS.XXCUS_OZF_TIER0_ACCRUALS_STG  PARTITION ('||upper(replace(p_period, '-', ''))||') XLA
                       ,APPS.OZF_TIME_DAY              T
                       ,GL_PERIODS                     GLP
                       ,OZF_OFFERS                     OO
                  WHERE 1 = 1
                    --AND XLA.OFU_GL_DATE        =T.REPORT_DATE
                    AND XLA.OFU_CREATION_DATE        =T.REPORT_DATE
                    AND XLA.XAEH_PERIOD_NAME   ='||''''||p_period||''''||'
                    AND GLP.PERIOD_SET_NAME    =''4-4-QTR''
                    AND glp.period_name =xla.xaeh_period_name
                    AND GLP.ADJUSTMENT_PERIOD_FLAG =''N''
                    AND T.REPORT_DATE BETWEEN GLP.START_DATE AND GLP.END_DATE
                    AND OO.OFFER_ID            =XLA.OFFER_ID
                    --AND OO.STATUS_CODE         <>''DRAFT''
               GROUP BY
                        XLA.XAEH_PERIOD_NAME,                    --FISCAL_PERIOD
                        XLA.OFU_MVID,                            --MVID,
                        XLA.OFU_CUST_ACCOUNT_ID,                 --CUST_ID,
                        XLA.BILL_TO_PARTY_NAME_LOB,              --LOB,
                        XLA.BILL_TO_PARTY_ID,                    --LOB_ID,
                        XLA.RESALE_LINE_ATTRIBUTE2,              --BU,
                        XLA.RESALE_LINE_ATTRIBUTE6,              --BRANCH
                        XLA.BILLTO_CUST_ACCT_BR_ACCT_NAME,       --CUST_ACCT_BRANCH_NAME
                        XLA.POSTED_FIN_LOC,                      --POSTED_FIN_LOCATION
                        XLA.POSTED_BU_BRANCH,                    --POSTED_BU_BRANCH
                        XLA.OFU_ATTRIBUTE10,                     --OFU_ATTRIBUTE10,
                        XLA.UTILIZATION_TYPE,                    --UTILIZATION_TYPE
                        XLA.ADJUSTMENT_TYPE,                     --ADJUSTMENT_TYPE
                        XLA.ADJUSTMENT_TYPE_ID,                  --ADJUSTMENT_TYPE_ID
                        XLA.OFU_CURRENCY_CODE,                   --CURRENCY_CODE
                        XLA.OZF_LEDGER_NAME,                     --LEDGER_NAME
                        XLA.QP_LIST_HEADER_ID,                   --PLAN_ID,
                        OO.STATUS_CODE,                          --STATUS,
                        CASE
                         when XLA.ACTIVITY_MEDIA_NAME IN (''Coop'', ''Coop Minimum'') then ''COOP''
                         else ''REBATE''
                        END,
                        TO_NUMBER(XLA.CALENDAR_YEAR),            --AGREEMENT_YEAR,
                        SUBSTR (T.month_id, 0, 4),               --CALENDAR_YEAR,
                        T.ENT_YEAR_ID,                           --FISCAL_YEAR,
                        T.ENT_PERIOD_ID,                         --PERIOD_ID
                        GLP.PERIOD_NUM,                          --FISCAL_MONTH_ID
                        SUBSTR (T.MONTH_ID, 6, 2),
                        XLA.OXA_ORG_ID,
                        XLA.XLAE_PROCESS_STATUS_CODE,
                        XLA.XLAE_EVENT_STATUS_CODE,
                        XLA.XAEH_GL_XFER_STATUS_CODE     
            )';
         --
         print_log('');
         --
         print_log('Insert SQL for Fiscal Period: '||p_period);
         print_log('=========================================');
         --
         print_log('');
         --
         print_log(sql_my_trx);
         --
         begin
          --
          savepoint init_here;
          --
          print_log('Before calling dynamic insert SQL for period: '||p_period);
          --
          execute immediate sql_my_trx;
          --
          print_log('After calling dynamic insert SQL for period: '||p_period);
          --
          print_log('Total records inserted for fiscal period '||p_period||' ='||sql%rowcount);
          --
         exception
          --
          when others then
           --
           print_log('@ Period: '||p_period||' error in calling dynamic SQL, message ='||sqlerrm);
           --
           rollback to init_here;
         end;
         --
        exception
         when others then
          --
          print_log('@ Period: '||p_period||' error in generating dynamic SQL, message ='||sqlerrm);
          --
        end;
        --
        -- end insert from a specific partition other than NA
        --
        --
        -- begin insert from partition NA
        --
        begin
         --
         sql_my_trx :=
            'INSERT INTO /*+ APPEND */ xxcus.xxcus_ytd_income_stg
            (
            SELECT
                        XLA.XAEH_PERIOD_NAME                 FISCAL_PERIOD,
                        XLA.OFU_MVID                         MVID,
                        XLA.OFU_CUST_ACCOUNT_ID              OFU_CUST_ACCOUNT_ID,
                        (
                         SELECT /*+ RESULT_CACHE */
                                HZP.PARTY_NAME
                         FROM   HZ_PARTIES HZP,
                                HZ_CUST_ACCOUNTS HZCA
                         WHERE  1 =1
                           AND  HZCA.CUST_ACCOUNT_ID
                               =XLA.OFU_CUST_ACCOUNT_ID
                           AND  HZP.PARTY_ID
                               =HZCA.PARTY_ID
                        )                                    VENDOR_NAME,
                        XLA.BILL_TO_PARTY_NAME_LOB           LOB,
                        XLA.BILL_TO_PARTY_ID                 LOB_ID,
                        XLA.RESALE_LINE_ATTRIBUTE2           BU,
                        XLA.RESALE_LINE_ATTRIBUTE6           BRANCH,
                        XLA.BILLTO_CUST_ACCT_BR_ACCT_NAME    CUST_ACCT_BRANCH_NAME,
                        XLA.POSTED_FIN_LOC                   POSTED_FIN_LOC,
                        XLA.POSTED_BU_BRANCH                 POSTED_BU_BRANCH,
                        XLA.OFU_ATTRIBUTE10                  OFU_ATTRIBUTE10,
                        XLA.UTILIZATION_TYPE                 UTILIZATION_TYPE,
                        XLA.ADJUSTMENT_TYPE                  ADJUSTMENT_TYPE,
                        XLA.ADJUSTMENT_TYPE_ID               ADJUSTMENT_TYPE_ID,
                        XLA.OFU_CURRENCY_CODE                CURRENCY_CODE,
                        XLA.OZF_LEDGER_NAME                  LEDGER_NAME,
                        XLA.QP_LIST_HEADER_ID                PLAN_ID,
                        (
                         SELECT /*+ RESULT_CACHE */
                                QPLH.DESCRIPTION
                         FROM   QP_LIST_HEADERS_TL QPLH
                         WHERE  1 =1
                           AND  QPLH.LIST_HEADER_ID
                               =XLA.QP_LIST_HEADER_ID
                        )                                    OFFER_NAME,
                        OO.STATUS_CODE                       STATUS,
                        CASE
                         when XLA.ACTIVITY_MEDIA_NAME IN (''Coop'', ''Coop Minimum'') then ''COOP''
                         else ''REBATE''
                        END                                  REBATE_TYPE,
                        TO_NUMBER(XLA.CALENDAR_YEAR)         AGREEMENT_YEAR,
                        SUBSTR (T.month_id, 0, 4)            CALENDAR_YEAR,
                        T.ENT_YEAR_ID                        FISCAL_YEAR,
                        GLP.PERIOD_NUM                       FISCAL_MONTH_ID,
                        T.ENT_PERIOD_ID                      PERIOD_ID,
                        SUBSTR (T.MONTH_ID, 6, 2)            CALENDAR_MONTH_ID,
                        SUM(SELLING_PRICE*QUANTITY)          PURCHASES,
                        SUM (XLA.util_acctd_amount)          ACCRUAL_AMOUNT,
                        SYSDATE                              CREATION_DATE,
                        FND_GLOBAL.USER_ID                   CREATED_BY,
                        XLA.OXA_ORG_ID                       ORG_ID,
                        XLA.XLAE_PROCESS_STATUS_CODE         PROCESS_STATUS_CODE,
                        XLA.XLAE_EVENT_STATUS_CODE           EVENT_STATUS_CODE,
                        XLA.XAEH_GL_XFER_STATUS_CODE         GL_TRANSFER_STATUS_CODE
                   FROM XXCUS.XXCUS_OZF_XLA_ACCRUALS_B  PARTITION (NA) XLA
                       ,APPS.OZF_TIME_DAY              T
                       ,GL_PERIODS                     GLP
                       ,OZF_OFFERS                     OO
                  WHERE 1 = 1
                    AND XLA.OFU_GL_DATE        =T.REPORT_DATE
                    AND XLA.XAEH_PERIOD_NAME   =''NA''
                    AND GLP.PERIOD_SET_NAME    =''4-4-QTR''
                    AND GLP.ADJUSTMENT_PERIOD_FLAG =''N''
                    AND T.REPORT_DATE BETWEEN GLP.START_DATE AND GLP.END_DATE
                    AND OO.OFFER_ID            =XLA.OFFER_ID
                    --AND OO.STATUS_CODE         <>''DRAFT''
               GROUP BY
                        XLA.XAEH_PERIOD_NAME,                     --FISCAL_PERIOD
                        XLA.OFU_MVID,                            --MVID,
                        XLA.OFU_CUST_ACCOUNT_ID,                 --CUST_ID,
                        XLA.BILL_TO_PARTY_NAME_LOB,              --LOB,
                        XLA.BILL_TO_PARTY_ID,                    --LOB_ID,
                        XLA.RESALE_LINE_ATTRIBUTE2,              --BU,
                        XLA.RESALE_LINE_ATTRIBUTE6,              --BRANCH
                        XLA.BILLTO_CUST_ACCT_BR_ACCT_NAME,       --CUST_ACCT_BRANCH_NAME
                        XLA.POSTED_FIN_LOC,                      --POSTED_FIN_LOCATION
                        XLA.POSTED_BU_BRANCH,                    --POSTED_BU_BRANCH
                        XLA.OFU_ATTRIBUTE10,                     --OFU_ATTRIBUTE10,
                        XLA.UTILIZATION_TYPE,                    --UTILIZATION_TYPE
                        XLA.ADJUSTMENT_TYPE,                     --ADJUSTMENT_TYPE
                        XLA.ADJUSTMENT_TYPE_ID,                  --ADJUSTMENT_TYPE_ID
                        XLA.OFU_CURRENCY_CODE,                   --CURRENCY_CODE
                        XLA.OZF_LEDGER_NAME,                     --LEDGER_NAME
                        XLA.QP_LIST_HEADER_ID,                   --PLAN_ID,
                        OO.STATUS_CODE,                          --STATUS,
                        CASE
                         when XLA.ACTIVITY_MEDIA_NAME IN (''Coop'', ''Coop Minimum'') then ''COOP''
                         else ''REBATE''
                        END,
                        TO_NUMBER(XLA.CALENDAR_YEAR),            --AGREEMENT_YEAR,
                        SUBSTR (T.month_id, 0, 4),               --CALENDAR_YEAR,
                        T.ENT_YEAR_ID,                           --FISCAL_YEAR,
                        T.ENT_PERIOD_ID,                         --PERIOD_ID
                        GLP.PERIOD_NUM,                          --FISCAL_MONTH_ID
                        SUBSTR (T.MONTH_ID, 6, 2),
                        XLA.OXA_ORG_ID,
                        XLA.XLAE_PROCESS_STATUS_CODE,
                        XLA.XLAE_EVENT_STATUS_CODE,
                        XLA.XAEH_GL_XFER_STATUS_CODE
            )';
         --
         print_log('');
         --
         print_log('Insert SQL for Period: NA');
         print_log('=========================');
         --
         print_log('');
         --
         print_log(sql_my_trx);
         --
         begin
          --
          savepoint init_here;
          --
          print_log('Before calling dynamic insert SQL @ Period: NA');
          --
          execute immediate sql_my_trx;
          --
          print_log('After calling dynamic insert SQL @ Period: NA');
          --
          print_log('Total records inserted for fiscal period NA ='||sql%rowcount);
          --
         exception
          --
          when others then
           --
           print_log('@ Period: NA, error in calling dynamic SQL, message ='||sqlerrm);
           --
           rollback to init_here;
         end;
         --
        exception
         when others then
          --
          print_log('@ Partition: NA, error in generating dynamic SQL, message ='||sqlerrm);
          --
        end;
        --
       end if;
       --    
    end if;
    --
  exception
   when others then
    print_log ('Issue in extract_ytdincome routine, message ='||sqlerrm);
    rollback;
  end extract_ytdincome;
  --;
  --
end XXCUS_OZF_YTD_PVT;
/