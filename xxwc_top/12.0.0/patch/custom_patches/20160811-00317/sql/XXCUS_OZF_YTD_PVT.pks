CREATE OR REPLACE PACKAGE APPS.XXCUS_OZF_YTD_PVT
-- Change History:
-- Ticket#                ESMS          Date         Version  Comment
-- ===================    ============  =========    ======== ============================================================================================
-- TMS  20160811-00317    ESMS 437126   08/11/2016   1.0      Add routine to extract zero dollar tier into a copy of the original table as well as for YTD Income
AS
  --
  procedure extract_tierzero
  (
    retcode               out number
   ,errbuf                out varchar2
   ,p_period              in  varchar2
  );  
  --
  procedure extract_ytdincome
  (
    retcode               out number
   ,errbuf                out varchar2
   ,p_period              in  varchar2
  );
--
end XXCUS_OZF_YTD_PVT;
/