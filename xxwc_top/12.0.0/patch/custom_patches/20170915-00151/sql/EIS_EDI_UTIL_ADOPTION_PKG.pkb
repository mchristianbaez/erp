CREATE OR REPLACE 
PACKAGE BODY XXEIS.EIS_EDI_UTIL_ADOPTION_PKG as
--//============================================================================
--//
--// Object Usage 				:: This Object Referred by "EDI Utilization and Adoption - WC"
--//
--// Object Name         		:: EIS_EDI_UTIL_ADOPTION_PKG
--//
--// Object Type         		:: Package Spec
--//
--// Object Description  		:: This Package will trigger in before report and insert the values into Table
--//
--// Version Control
--//============================================================================
--// Version    Author           Date          Description
--//----------------------------------------------------------------------------
--// 1.0        Siva  	       26-OCT-2017      Initial Build  --TMS#20170818-00013 
--//============================================================================


FUNCTION get_po_submitted_edi_func(
    P_REGION VARCHAR2,
    p_from_date   date,
    p_to_date date,
    p_type varchar2)
  RETURN number
IS
  --//============================================================================
  --// Object Usage         :: This Object Referred by "EDI Utilization and Adoption - WC"
  --//
  --// Object Name          :: get_po_submitted_edi_func
  --//
  --// Object Type          :: Function
  --//
  --// Object Description   :: This Function will populate the values in the view.
  --//
  --// Version Control
  --//============================================================================
  --// Version    Author           Date          		Description
  --//----------------------------------------------------------------------------
  --// 1.0        Siva  	       26-OCT-2017      Initial Build  --TMS#20170915-00151 
  --//============================================================================
L_HASH_INDEX varchar2(100);
l_sql varchar2(32000);
l_cond varchar2(10000);  
begin
g_po_submitted_edi := NULL;


if P_TYPE ='Yes' then
l_cond :='and aps.attribute3 in (''Tier1'',''Tier2'')';
elsif p_type ='No' THEN
l_cond:='and 1=1';
end if;

l_sql :='SELECT COUNT(*)
		from apps.po_headers_archive_all poh,
			 apps.hr_locations_all hrl,
			 apps.mtl_parameters orgs,
       ap_suppliers aps
		WHERE poh.type_lookup_code      = ''STANDARD''
			and poh.edi_processed_flag      = ''Y''
			AND poh.revision_num            = 0
      and poh.org_id=162
			AND POH.SHIP_TO_LOCATION_ID       = HRL.LOCATION_ID
			and hrl.inventory_organization_id = orgs.organization_id
      and poh.vendor_id = aps.vendor_id 
			and orgs.attribute9 = :1
			and trunc(poh.creation_date) >= :2
			AND trunc(poh.creation_date) <=:3
      '||l_cond||'
';
    BEGIN
        l_hash_index:=P_REGION||'-'||P_FROM_DATE||'-'||P_TO_DATE;
        g_po_submitted_edi := g_po_submitted_edi_vldn_tbl(L_HASH_INDEX);
 exception
    when no_data_found
    THEN
    begin      
    EXECUTE IMMEDIATE L_SQL INTO g_po_submitted_edi USING P_REGION,P_FROM_DATE,P_TO_DATE;
    EXCEPTION WHEN NO_DATA_FOUND THEN
    g_po_submitted_edi :=0;
    WHEN OTHERS THEN
    g_po_submitted_edi :=0;
    END;      
                      l_hash_index:=P_REGION||'-'||P_FROM_DATE||'-'||P_TO_DATE;
                       g_po_submitted_edi_vldn_tbl(L_HASH_INDEX) := g_po_submitted_edi;
    END;
     return  g_po_submitted_edi;
     EXCEPTION when OTHERS then
      g_po_submitted_edi:=0;
      RETURN  g_po_submitted_edi;

end get_po_submitted_edi_func;


FUNCTION GET_TOTAL_PO_EDI_SUP_FUNC(
    P_REGION VARCHAR2,
    p_from_date   date,
    p_to_date date,
     p_type varchar2)
  RETURN number
IS
  --//============================================================================
  --// Object Usage         :: This Object Referred by "EDI Utilization and Adoption - WC"
  --//
  --// Object Name          :: GET_TOTAL_PO_EDI_SUP_FUNC
  --//
  --// Object Type          :: Function
  --//
  --// Object Description   :: This Function will populate the values in the view.
  --//
  --// Version Control
  --//============================================================================
  --// Version    Author           Date          		Description
  --//----------------------------------------------------------------------------
  --// 1.0        Siva  	       26-OCT-2017      Initial Build  --TMS#20170915-00151 
  --//============================================================================
L_HASH_INDEX varchar2(100);
L_SQL varchar2(32000);
l_cond varchar2(10000);  
begin
g_total_po_edi_sup := NULL;

if P_TYPE ='Yes' then
l_cond :='and aps.attribute3 in (''Tier1'',''Tier2'')';
elsif p_type ='No' THEN
l_cond:='and 1=1';
end if;

l_sql :='SELECT COUNT(*)
FROM apps.po_headers_archive_all poh,
			 apps.hr_locations_all hrl,
			 apps.mtl_parameters orgs,
       ap_suppliers aps
where poh.type_lookup_code      = ''STANDARD''
AND poh.revision_num            = 0
and poh.org_id=162
AND POH.SHIP_TO_LOCATION_ID       = HRL.LOCATION_ID
and hrl.inventory_organization_id = orgs.organization_id
and poh.vendor_id = aps.vendor_id 
and orgs.attribute9 =nvl(:1,orgs.attribute9 )
AND trunc(poh.creation_date) >=:1
AND trunc(poh.creation_date) <=:2
AND EXISTS
  (SELECT 1
  FROM apps.ece_tp_headers tph,
    apps.ece_tp_details tpd,
    apps.ap_supplier_sites_all assl
  WHERE tph.tp_header_id    = tpd.tp_header_id
  AND assl.tp_header_id     = tph.tp_header_id
  AND assl.vendor_id        = poh.vendor_id
  AND tpd.document_id      IN ( ''POO'',''POCO'')
  AND tpd.edi_flag          = ''Y''
  and assl.vendor_site_code = ''0PURCHASING''
  )
  '||l_cond||'
';  

    BEGIN
        l_hash_index:=P_REGION||'-'||P_FROM_DATE||'-'||P_TO_DATE;
        g_total_po_edi_sup := g_total_po_edi_sup_vldn_tbl(L_HASH_INDEX);
 exception
    when no_data_found
    THEN
    BEGIN      
    EXECUTE IMMEDIATE L_SQL INTO g_total_po_edi_sup USING P_REGION,P_FROM_DATE,P_TO_DATE;
    EXCEPTION WHEN NO_DATA_FOUND THEN
    g_total_po_edi_sup :=0;
    WHEN OTHERS THEN
    g_total_po_edi_sup :=0;
    END;      
                      l_hash_index:= P_REGION||'-'||P_FROM_DATE||'-'||P_TO_DATE;
                       g_total_po_edi_sup_vldn_tbl(L_HASH_INDEX) := g_total_po_edi_sup;
    END;
     return  g_total_po_edi_sup;
     EXCEPTION when OTHERS then
      g_total_po_edi_sup:=0;
      RETURN  g_total_po_edi_sup;

end get_total_po_edi_sup_func;


FUNCTION get_po_confirmed_edi_func(
    P_REGION VARCHAR2,
    p_from_date   date,
    p_to_date date,
     p_type varchar2)
  RETURN number
IS
  --//============================================================================
  --// Object Usage         :: This Object Referred by "EDI Utilization and Adoption - WC"
  --//
  --// Object Name          :: get_po_confirmed_edi_func
  --//
  --// Object Type          :: Function
  --//
  --// Object Description   :: This Function will populate the values in the view.
  --//
  --// Version Control
  --//============================================================================
  --// Version    Author           Date          		Description
  --//----------------------------------------------------------------------------
  --// 1.0        Siva  	       26-OCT-2017      Initial Build  --TMS#20170915-00151 
  --//============================================================================
L_HASH_INDEX varchar2(100);
L_SQL varchar2(32000);
l_cond varchar2(10000);  
begin
g_po_confirmed_edi := NULL;

if P_TYPE ='Yes' then
l_cond :='and aps.attribute3 in (''Tier1'',''Tier2'')';
elsif p_type ='No' THEN
l_cond:='and 1=1';
end if;

l_sql :='SELECT COUNT(*)
FROM apps.po_headers_archive_all poh,
  apps.hr_locations_all hrl,
  apps.mtl_parameters orgs,
       ap_suppliers aps
WHERE poh.type_lookup_code        = ''STANDARD''
AND poh.edi_processed_flag        = ''Y''
AND poh.revision_num              = 0
and poh.org_id=162
AND POH.SHIP_TO_LOCATION_ID       = HRL.LOCATION_ID
AND hrl.inventory_organization_id = orgs.organization_id
and poh.vendor_id = aps.vendor_id 
AND orgs.attribute9               = :1
AND TRUNC(poh.creation_date)     >= :2
AND TRUNC(poh.creation_date)     <= :3
AND EXISTS
  (SELECT 1
  FROM apps.po_acceptances ah
  WHERE ah.po_header_id              = poh.po_header_id
  and ah.created_by=15985
  )
  '||l_cond||'
';   
    BEGIN
        l_hash_index:=P_REGION||'-'||P_FROM_DATE||'-'||P_TO_DATE;
        g_po_confirmed_edi := g_po_confirmed_edi_vldn_tbl(L_HASH_INDEX);
 exception
    when no_data_found
    THEN
    begin      
    EXECUTE IMMEDIATE L_SQL INTO g_po_confirmed_edi USING P_REGION,P_FROM_DATE,P_TO_DATE;
    EXCEPTION WHEN NO_DATA_FOUND THEN
    g_po_confirmed_edi :=0;
    WHEN OTHERS THEN
    g_po_confirmed_edi :=0;
    END;      
                      l_hash_index:=P_REGION||'-'||P_FROM_DATE||'-'||P_TO_DATE;
                       g_po_confirmed_edi_vldn_tbl(L_HASH_INDEX) := g_po_confirmed_edi;
    END;
     return  g_po_confirmed_edi;
     EXCEPTION when OTHERS then
      g_po_confirmed_edi:=0;
      RETURN  g_po_confirmed_edi;

end get_po_confirmed_edi_func;



FUNCTION get_po_unconfirmed_edi_func(
    P_FROM_DATE   DATE,
    P_TO_DATE DATE,
     p_type varchar2)
  RETURN number
IS
  --//============================================================================
  --// Object Usage         :: This Object Referred by "EDI Utilization and Adoption - WC"
  --//
  --// Object Name          :: get_po_unconfirmed_edi_func
  --//
  --// Object Type          :: Function
  --//
  --// Object Description   :: This Function will populate the values in the view.
  --//
  --// Version Control
  --//============================================================================
  --// Version    Author           Date          		Description
  --//----------------------------------------------------------------------------
  --// 1.0        Siva  	       26-OCT-2017      Initial Build  --TMS#20170915-00151 
  --//============================================================================
L_HASH_INDEX varchar2(100);
L_SQL varchar2(32000);
l_cond varchar2(10000);  
begin
g_po_unconfirmed_edi := NULL;

if P_TYPE ='Yes' then
l_cond :='and aps.attribute3 in (''Tier1'',''Tier2'')';
elsif p_type ='No' THEN
l_cond:='and 1=1';
end if;

l_sql :='SELECT COUNT(*)
FROM apps.po_headers_archive_all poh,
       ap_suppliers aps
WHERE poh.type_lookup_code        = ''STANDARD''
AND poh.edi_processed_flag        = ''Y''
AND poh.revision_num              = 0
and poh.vendor_id = aps.vendor_id 
and poh.org_id=162
AND TRUNC(poh.creation_date)     >= :2
AND TRUNC(poh.creation_date)     <= :3
AND NOT EXISTS
  (SELECT 1
  FROM apps.po_acceptances ah
  WHERE ah.po_header_id              = poh.po_header_id
  and ah.created_by=15985
  )
 '||l_cond||'
';
    BEGIN
        l_hash_index:=P_FROM_DATE||'-'||P_TO_DATE;
        g_po_unconfirmed_edi := g_po_unconfirmed_edi_vldn_tbl(L_HASH_INDEX);
 exception
    when no_data_found
    THEN
    begin      
    EXECUTE IMMEDIATE L_SQL INTO g_po_unconfirmed_edi USING P_FROM_DATE,P_TO_DATE;
    EXCEPTION WHEN NO_DATA_FOUND THEN
    g_po_unconfirmed_edi :=0;
    WHEN OTHERS THEN
    g_po_unconfirmed_edi :=0;
    END;      
                      l_hash_index:= P_FROM_DATE||'-'||P_TO_DATE;
                       g_po_unconfirmed_edi_vldn_tbl(L_HASH_INDEX) := g_po_unconfirmed_edi;
    END;
     return  g_po_unconfirmed_edi;
     EXCEPTION when OTHERS then
      g_po_unconfirmed_edi:=0;
      RETURN  g_po_unconfirmed_edi;

end get_po_unconfirmed_edi_func; 

FUNCTION get_po_non_purchase_edi_func(
   P_TYPE VARCHAR2,
   P_FROM_DATE  DATE,
   P_TO_DATE DATE,
   p_pref_sup VARCHAR2)
 RETURN NUMBER
as
  --//============================================================================
  --// Object Usage         :: This Object Referred by "EDI Utilization and Adoption - WC"
  --//
  --// Object Name          :: get_po_non_purchase_edi_func
  --//
  --// Object Type          :: Function
  --//
  --// Object Description   :: This Function will populate the values in the view.
  --//
  --// Version Control
  --//============================================================================
  --// Version    Author           Date          		Description
  --//----------------------------------------------------------------------------
  --// 1.0        Siva  	       26-OCT-2017      Initial Build  --TMS#20170915-00151 
  --//============================================================================

l_po_non_purchase_edi NUMBER;
BEGIN

IF p_pref_sup = 'No'
then

if P_TYPE= 'PURCHASE'
then
SELECT COUNT(*) into l_po_non_purchase_edi
FROM apps.po_headers_archive_all poh
WHERE poh.type_lookup_code    = 'STANDARD'
AND poh.edi_processed_flag    = 'Y'
AND POH.REVISION_NUM          = 0
AND poh.org_id                = 162
AND TRUNC(POH.CREATION_DATE) >= P_FROM_DATE
AND TRUNC(POH.CREATION_DATE) <= P_TO_DATE
and xxeis.eis_po_xxwc_open_pur_list_pkg.get_buyer_group(poh.agent_id) = 'Purchasing'
; 

elsif P_TYPE= 'NONPURCHASE'
then
SELECT COUNT(*) into l_po_non_purchase_edi
FROM apps.po_headers_archive_all poh
WHERE poh.type_lookup_code    = 'STANDARD'
AND poh.edi_processed_flag    = 'Y'
AND POH.REVISION_NUM          = 0
AND poh.org_id                = 162
AND TRUNC(POH.CREATION_DATE) >= P_FROM_DATE
AND TRUNC(POH.CREATION_DATE) <= P_TO_DATE
AND nvl(XXEIS.EIS_PO_XXWC_OPEN_PUR_LIST_PKG.GET_BUYER_GROUP(POH.AGENT_ID),1) != 'Purchasing'
; 

elsif P_TYPE='ALL'
THEN
SELECT COUNT(*) into l_po_non_purchase_edi
FROM apps.po_headers_archive_all poh
WHERE poh.type_lookup_code    = 'STANDARD'
AND poh.edi_processed_flag    = 'Y'
AND POH.REVISION_NUM          = 0
AND poh.org_id                = 162
AND TRUNC(POH.CREATION_DATE) >= P_FROM_DATE
AND TRUNC(POH.CREATION_DATE) <= P_TO_DATE
;
END IF;

elsif p_pref_sup = 'Yes'
then

if P_TYPE= 'PURCHASE'
then
SELECT COUNT(*) into l_po_non_purchase_edi
FROM apps.po_headers_archive_all poh,
	 ap_suppliers aps
WHERE poh.type_lookup_code    = 'STANDARD'
AND poh.edi_processed_flag    = 'Y'
AND POH.REVISION_NUM          = 0
AND poh.org_id                = 162
and poh.vendor_id = aps.vendor_id 
AND TRUNC(POH.CREATION_DATE) >= P_FROM_DATE
AND TRUNC(POH.CREATION_DATE) <= P_TO_DATE
and xxeis.eis_po_xxwc_open_pur_list_pkg.get_buyer_group(poh.agent_id) = 'Purchasing'
; 

elsif P_TYPE= 'NONPURCHASE'
then
SELECT COUNT(*) into l_po_non_purchase_edi
FROM apps.po_headers_archive_all poh,
	 ap_suppliers aps
WHERE poh.type_lookup_code    = 'STANDARD'
AND poh.edi_processed_flag    = 'Y'
AND POH.REVISION_NUM          = 0
AND poh.org_id                = 162
and poh.vendor_id = aps.vendor_id 
AND TRUNC(POH.CREATION_DATE) >= P_FROM_DATE
AND TRUNC(POH.CREATION_DATE) <= P_TO_DATE
AND nvl(XXEIS.EIS_PO_XXWC_OPEN_PUR_LIST_PKG.GET_BUYER_GROUP(POH.AGENT_ID),1) != 'Purchasing'
; 

elsif P_TYPE='ALL'
THEN
SELECT COUNT(*) into l_po_non_purchase_edi
FROM apps.po_headers_archive_all poh,
	 ap_suppliers aps
WHERE poh.type_lookup_code    = 'STANDARD'
AND poh.edi_processed_flag    = 'Y'
AND POH.REVISION_NUM          = 0
AND poh.org_id                = 162
and poh.vendor_id = aps.vendor_id 
AND TRUNC(POH.CREATION_DATE) >= P_FROM_DATE
AND TRUNC(POH.CREATION_DATE) <= P_TO_DATE
;
END IF;
end if;


return  l_po_non_purchase_edi;

exception when others
then return null;
END GET_PO_NON_PURCHASE_EDI_FUNC;


PROCEDURE get_edi_utilize_dtls_proc(
    p_region             IN VARCHAR2,
    p_fiscal_period      IN VARCHAR2,
    p_preferred_supplier IN VARCHAR2)
as                
--//============================================================================
--//
--// Object Name         :: get_edi_utilize_dtls_proc
--//
--// Object Usage 		 :: This Object Referred by "EDI Utilization and Adoption - WC"
--//
--// Object Type         :: Procedure
--//
--// Object Description  :: This  procedure is created based on the EIS_XXWC_EDI_UTIL_ADOPTION_V View.
--//
--// Version Control
--//============================================================================
--// Version    Author           Date          Description
--//----------------------------------------------------------------------------
--// 1.0        Siva  	       26-OCT-2017      Initial Build  --TMS#20170915-00151 
--//============================================================================
lc_region_sql 	varchar2(32000);
l_main_sql 		VARCHAR2(32000);
l_mon_start_date DATE;
l_mon_end_date DATE;
l_year_start_date DATE;
l_week1_start DATE;
l_week1_end DATE;
l_week2_start DATE;
l_week2_end DATE;
l_week3_start DATE;
l_week3_end DATE;
l_week4_start DATE;
l_week4_end DATE;
l_week5_start DATE;
l_week5_end DATE;

TYPE region_rec IS record
(
region varchar2(100)
);

 TYPE region_rec_tab IS TABLE OF region_rec INDEX BY binary_integer;
region_tab region_rec_tab;
 
type report_data_rec_tab is table of xxeis.EIS_XXWC_EDI_UTIL_ADOPTION_TBL%rowtype index by binary_integer ;
report_data_tab report_data_rec_tab;
 L_REF_CURSOR1          CURSOR_TYPE4;


 
BEGIN
region_tab.delete;
report_data_tab.delete;

lc_region_sql:='select distinct mp.attribute9 region from mtl_parameters mp where mp.attribute9 is not null';

IF p_region IS NOT NULL THEN
lc_region_sql:=lc_region_sql||''||' and mp.attribute9 in ('||xxeis.eis_rs_utility.get_param_values(p_region)||' )';
end if;


begin 
EXECUTE IMMEDIATE lc_region_sql  BULK COLLECT INTO region_tab;
exception when others then
NULL;
end;

SELECT gps.start_date,
  gps.end_date,
  gps.year_start_date,
  TRUNC(gps.start_date, 'Day')+1 week1_start,
  TRUNC(gps.start_date, 'Day')+5 week1_end,
  TRUNC(gps.start_date+7, 'Day')+1 week2_start,
  TRUNC(gps.start_date+7, 'Day')+5 week2_end,
  TRUNC(gps.start_date+14, 'Day')+1 week3_start,
  TRUNC(gps.start_date+14, 'Day')+5 week3_end,
  TRUNC(gps.start_date+21, 'Day')+1 week4_start,
  TRUNC(gps.start_date+21, 'Day')+5 week4_end,
  CASE WHEN  TRUNC(gps.start_date+28, 'Day')+1 > gps.end_date
  THEN NULL
  ELSE TRUNC(gps.start_date+28, 'Day')+1
  END week5_start,
  CASE WHEN  TRUNC(gps.start_date+28, 'Day')+1 > gps.end_date
  THEN NULL
  ELSE TRUNC(gps.start_date+28, 'Day')+5
  END week5_end
  INTO l_mon_start_date,
    l_mon_end_date,
    l_year_start_date,
    l_week1_start,
    l_week1_end,
    l_week2_start,
    l_week2_end,
    l_week3_start,
    l_week3_end,
    l_week4_start,
    l_week4_end,
    l_week5_start,
    l_week5_end
FROM apps.gl_periods gps
WHERE gps.period_set_name = '4-4-QTR'
AND gps.period_name = p_fiscal_period
AND ROWNUM=1 
; 

 Fnd_File.Put_Line(FND_FILE.log,'region_tab  '||region_tab.COUNT);
 Fnd_File.Put_Line(FND_FILE.log,'l_mon_start_date  '||l_mon_start_date);

 FOR I IN 1..region_tab.COUNT LOOP
 g_region := region_tab(i).region;
 
L_MAIN_SQL:='
select 
'''||g_region||''' region,
xxeis.EIS_EDI_UTIL_ADOPTION_PKG.get_po_submitted_edi_func('''||g_region||''','''||l_year_start_date||''','''||l_mon_end_date||''','''||p_preferred_supplier||''')po_submit_via_edi_yr,
xxeis.EIS_EDI_UTIL_ADOPTION_PKG.get_total_po_edi_sup_func('''||g_region||''','''||l_year_start_date||''','''||l_mon_end_date||''','''||p_preferred_supplier||''')total_po_submit_edi_sup_yr,
decode((xxeis.EIS_EDI_UTIL_ADOPTION_PKG.get_total_po_edi_sup_func('''||g_region||''','''||l_year_start_date||''','''||l_mon_end_date||''','''||p_preferred_supplier||''')),0,0,(xxeis.EIS_EDI_UTIL_ADOPTION_PKG.get_po_submitted_edi_func('''||g_region||''','''||l_year_start_date||''','''||l_mon_end_date||''','''||p_preferred_supplier||''')/xxeis.EIS_EDI_UTIL_ADOPTION_PKG.get_total_po_edi_sup_func('''||g_region||''','''||l_year_start_date||''','''||l_mon_end_date||''','''||p_preferred_supplier||'''))*100) adoption_percentage_yr,
xxeis.EIS_EDI_UTIL_ADOPTION_PKG.get_po_confirmed_edi_func('''||g_region||''','''||l_year_start_date||''','''||l_mon_end_date||''','''||p_preferred_supplier||''')po_confirmed_via_edi_yr,
decode((xxeis.EIS_EDI_UTIL_ADOPTION_PKG.get_total_po_edi_sup_func('''||g_region||''','''||l_year_start_date||''','''||l_mon_end_date||''','''||p_preferred_supplier||''')),0,0,(xxeis.EIS_EDI_UTIL_ADOPTION_PKG.get_po_confirmed_edi_func('''||g_region||''','''||l_year_start_date||''','''||l_mon_end_date||''','''||p_preferred_supplier||''')/xxeis.EIS_EDI_UTIL_ADOPTION_PKG.get_total_po_edi_sup_func('''||g_region||''','''||l_year_start_date||''','''||l_mon_end_date||''','''||p_preferred_supplier||'''))*100) compliance_percentage_yr,
xxeis.EIS_EDI_UTIL_ADOPTION_PKG.get_po_submitted_edi_func('''||g_region||''','''||l_mon_start_date||''','''||l_mon_end_date||''','''||p_preferred_supplier||''')po_submit_via_edi_mnth,
xxeis.EIS_EDI_UTIL_ADOPTION_PKG.get_total_po_edi_sup_func('''||g_region||''','''||l_mon_start_date||''','''||l_mon_end_date||''','''||p_preferred_supplier||''')total_po_submit_edi_sup_mnth,
decode((xxeis.EIS_EDI_UTIL_ADOPTION_PKG.get_total_po_edi_sup_func('''||g_region||''','''||l_mon_start_date||''','''||l_mon_end_date||''','''||p_preferred_supplier||''')),0,0,(xxeis.EIS_EDI_UTIL_ADOPTION_PKG.get_po_submitted_edi_func('''||g_region||''','''||l_mon_start_date||''','''||l_mon_end_date||''','''||p_preferred_supplier||''')/xxeis.EIS_EDI_UTIL_ADOPTION_PKG.get_total_po_edi_sup_func('''||g_region||''','''||l_mon_start_date||''','''||l_mon_end_date||''','''||p_preferred_supplier||'''))*100) adoption_percentage_mnth,
xxeis.EIS_EDI_UTIL_ADOPTION_PKG.get_po_confirmed_edi_func('''||g_region||''','''||l_mon_start_date||''','''||l_mon_end_date||''','''||p_preferred_supplier||''')po_confirmed_via_edi_mnth,
decode((xxeis.EIS_EDI_UTIL_ADOPTION_PKG.get_total_po_edi_sup_func('''||g_region||''','''||l_mon_start_date||''','''||l_mon_end_date||''','''||p_preferred_supplier||''')),0,0,(xxeis.EIS_EDI_UTIL_ADOPTION_PKG.get_po_confirmed_edi_func('''||g_region||''','''||l_mon_start_date||''','''||l_mon_end_date||''','''||p_preferred_supplier||''')/xxeis.EIS_EDI_UTIL_ADOPTION_PKG.get_total_po_edi_sup_func('''||g_region||''','''||l_mon_start_date||''','''||l_mon_end_date||''','''||p_preferred_supplier||'''))*100) compliance_percentage_mnth,
xxeis.EIS_EDI_UTIL_ADOPTION_PKG.get_po_submitted_edi_func('''||g_region||''','''||l_week1_start||''','''||l_week1_end||''','''||p_preferred_supplier||''')po_submit_via_edi_wk1,
xxeis.EIS_EDI_UTIL_ADOPTION_PKG.get_total_po_edi_sup_func('''||g_region||''','''||l_week1_start||''','''||l_week1_end||''','''||p_preferred_supplier||''')total_po_submit_edi_sup_wk1,
decode((xxeis.EIS_EDI_UTIL_ADOPTION_PKG.get_total_po_edi_sup_func('''||g_region||''','''||l_week1_start||''','''||l_week1_end||''','''||p_preferred_supplier||''')),0,0,(xxeis.EIS_EDI_UTIL_ADOPTION_PKG.get_po_submitted_edi_func('''||g_region||''','''||l_week1_start||''','''||l_week1_end||''','''||p_preferred_supplier||''')/xxeis.EIS_EDI_UTIL_ADOPTION_PKG.get_total_po_edi_sup_func('''||g_region||''','''||l_week1_start||''','''||l_week1_end||''','''||p_preferred_supplier||'''))*100) adoption_percentage_wk1,
xxeis.EIS_EDI_UTIL_ADOPTION_PKG.get_po_confirmed_edi_func('''||g_region||''','''||l_week1_start||''','''||l_week1_end||''','''||p_preferred_supplier||''')po_confirmed_via_edi_wk1,
decode((xxeis.EIS_EDI_UTIL_ADOPTION_PKG.get_total_po_edi_sup_func('''||g_region||''','''||l_week1_start||''','''||l_week1_end||''','''||p_preferred_supplier||''')),0,0,(xxeis.EIS_EDI_UTIL_ADOPTION_PKG.get_po_confirmed_edi_func('''||g_region||''','''||l_week1_start||''','''||l_week1_end||''','''||p_preferred_supplier||''')/xxeis.EIS_EDI_UTIL_ADOPTION_PKG.get_total_po_edi_sup_func('''||g_region||''','''||l_week1_start||''','''||l_week1_end||''','''||p_preferred_supplier||'''))*100) compliance_percentage_wk1,
xxeis.EIS_EDI_UTIL_ADOPTION_PKG.get_po_submitted_edi_func('''||g_region||''','''||l_week2_start||''','''||l_week2_end||''','''||p_preferred_supplier||''')po_submit_via_edi_wk2,
xxeis.EIS_EDI_UTIL_ADOPTION_PKG.get_total_po_edi_sup_func('''||g_region||''','''||l_week2_start||''','''||l_week2_end||''','''||p_preferred_supplier||''')total_po_submit_edi_sup_wk2,
decode((xxeis.EIS_EDI_UTIL_ADOPTION_PKG.get_total_po_edi_sup_func('''||g_region||''','''||l_week2_start||''','''||l_week2_end||''','''||p_preferred_supplier||''')),0,0,(xxeis.EIS_EDI_UTIL_ADOPTION_PKG.get_po_submitted_edi_func('''||g_region||''','''||l_week2_start||''','''||l_week2_end||''','''||p_preferred_supplier||''')/xxeis.EIS_EDI_UTIL_ADOPTION_PKG.get_total_po_edi_sup_func('''||g_region||''','''||l_week2_start||''','''||l_week2_end||''','''||p_preferred_supplier||'''))*100) adoption_percentage_wk2,
xxeis.EIS_EDI_UTIL_ADOPTION_PKG.get_po_confirmed_edi_func('''||g_region||''','''||l_week2_start||''','''||l_week2_end||''','''||p_preferred_supplier||''')po_confirmed_via_edi_wk2,
decode((xxeis.EIS_EDI_UTIL_ADOPTION_PKG.get_total_po_edi_sup_func('''||g_region||''','''||l_week2_start||''','''||l_week2_end||''','''||p_preferred_supplier||''')),0,0,(xxeis.EIS_EDI_UTIL_ADOPTION_PKG.get_po_confirmed_edi_func('''||g_region||''','''||l_week2_start||''','''||l_week2_end||''','''||p_preferred_supplier||''')/xxeis.EIS_EDI_UTIL_ADOPTION_PKG.get_total_po_edi_sup_func('''||g_region||''','''||l_week2_start||''','''||l_week2_end||''','''||p_preferred_supplier||'''))*100) compliance_percentage_wk2,
xxeis.EIS_EDI_UTIL_ADOPTION_PKG.get_po_submitted_edi_func('''||g_region||''','''||l_week3_start||''','''||l_week3_end||''','''||p_preferred_supplier||''')po_submit_via_edi_wk3,
xxeis.EIS_EDI_UTIL_ADOPTION_PKG.get_total_po_edi_sup_func('''||g_region||''','''||l_week3_start||''','''||l_week3_end||''','''||p_preferred_supplier||''')total_po_submit_edi_sup_wk3,
decode((xxeis.EIS_EDI_UTIL_ADOPTION_PKG.get_total_po_edi_sup_func('''||g_region||''','''||l_week3_start||''','''||l_week3_end||''','''||p_preferred_supplier||''')),0,0,(xxeis.EIS_EDI_UTIL_ADOPTION_PKG.get_po_submitted_edi_func('''||g_region||''','''||l_week3_start||''','''||l_week3_end||''','''||p_preferred_supplier||''')/xxeis.EIS_EDI_UTIL_ADOPTION_PKG.get_total_po_edi_sup_func('''||g_region||''','''||l_week3_start||''','''||l_week3_end||''','''||p_preferred_supplier||'''))*100) adoption_percentage_wk3,
xxeis.EIS_EDI_UTIL_ADOPTION_PKG.get_po_confirmed_edi_func('''||g_region||''','''||l_week3_start||''','''||l_week3_end||''','''||p_preferred_supplier||''')po_confirmed_via_edi_wk3,
decode((xxeis.EIS_EDI_UTIL_ADOPTION_PKG.get_total_po_edi_sup_func('''||g_region||''','''||l_week3_start||''','''||l_week3_end||''','''||p_preferred_supplier||''')),0,0,(xxeis.EIS_EDI_UTIL_ADOPTION_PKG.get_po_confirmed_edi_func('''||g_region||''','''||l_week3_start||''','''||l_week3_end||''','''||p_preferred_supplier||''')/xxeis.EIS_EDI_UTIL_ADOPTION_PKG.get_total_po_edi_sup_func('''||g_region||''','''||l_week3_start||''','''||l_week3_end||''','''||p_preferred_supplier||'''))*100) compliance_percentage_wk3,
xxeis.EIS_EDI_UTIL_ADOPTION_PKG.get_po_submitted_edi_func('''||g_region||''','''||l_week4_start||''','''||l_week4_end||''','''||p_preferred_supplier||''')po_submit_via_edi_wk4,
xxeis.EIS_EDI_UTIL_ADOPTION_PKG.get_total_po_edi_sup_func('''||g_region||''','''||l_week4_start||''','''||l_week4_end||''','''||p_preferred_supplier||''')total_po_submit_edi_sup_wk4,
decode((xxeis.EIS_EDI_UTIL_ADOPTION_PKG.get_total_po_edi_sup_func('''||g_region||''','''||l_week4_start||''','''||l_week4_end||''','''||p_preferred_supplier||''')),0,0,(xxeis.EIS_EDI_UTIL_ADOPTION_PKG.get_po_submitted_edi_func('''||g_region||''','''||l_week4_start||''','''||l_week4_end||''','''||p_preferred_supplier||''')/xxeis.EIS_EDI_UTIL_ADOPTION_PKG.get_total_po_edi_sup_func('''||g_region||''','''||l_week4_start||''','''||l_week4_end||''','''||p_preferred_supplier||'''))*100) adoption_percentage_wk4,
xxeis.EIS_EDI_UTIL_ADOPTION_PKG.get_po_confirmed_edi_func('''||g_region||''','''||l_week4_start||''','''||l_week4_end||''','''||p_preferred_supplier||''')po_confirmed_via_edi_wk4,
decode((xxeis.EIS_EDI_UTIL_ADOPTION_PKG.get_total_po_edi_sup_func('''||g_region||''','''||l_week4_start||''','''||l_week4_end||''','''||p_preferred_supplier||''')),0,0,(xxeis.EIS_EDI_UTIL_ADOPTION_PKG.get_po_confirmed_edi_func('''||g_region||''','''||l_week4_start||''','''||l_week4_end||''','''||p_preferred_supplier||''')/xxeis.EIS_EDI_UTIL_ADOPTION_PKG.get_total_po_edi_sup_func('''||g_region||''','''||l_week4_start||''','''||l_week4_end||''','''||p_preferred_supplier||'''))*100) compliance_percentage_wk4,
xxeis.EIS_EDI_UTIL_ADOPTION_PKG.get_po_submitted_edi_func('''||g_region||''','''||l_week5_start||''','''||l_week5_end||''','''||p_preferred_supplier||''')po_submit_via_edi_wk5,
xxeis.EIS_EDI_UTIL_ADOPTION_PKG.get_total_po_edi_sup_func('''||g_region||''','''||l_week5_start||''','''||l_week5_end||''','''||p_preferred_supplier||''')total_po_submit_edi_sup_wk5,
decode((xxeis.EIS_EDI_UTIL_ADOPTION_PKG.get_total_po_edi_sup_func('''||g_region||''','''||l_week5_start||''','''||l_week5_end||''','''||p_preferred_supplier||''')),0,0,(xxeis.EIS_EDI_UTIL_ADOPTION_PKG.get_po_submitted_edi_func('''||g_region||''','''||l_week5_start||''','''||l_week5_end||''','''||p_preferred_supplier||''')/xxeis.EIS_EDI_UTIL_ADOPTION_PKG.get_total_po_edi_sup_func('''||g_region||''','''||l_week5_start||''','''||l_week5_end||''','''||p_preferred_supplier||'''))*100) adoption_percentage_wk5,
xxeis.EIS_EDI_UTIL_ADOPTION_PKG.get_po_confirmed_edi_func('''||g_region||''','''||l_week5_start||''','''||l_week5_end||''','''||p_preferred_supplier||''')po_confirmed_via_edi_wk5,
decode((xxeis.EIS_EDI_UTIL_ADOPTION_PKG.get_total_po_edi_sup_func('''||g_region||''','''||l_week5_start||''','''||l_week5_end||''','''||p_preferred_supplier||''')),0,0,(xxeis.EIS_EDI_UTIL_ADOPTION_PKG.get_po_confirmed_edi_func('''||g_region||''','''||l_week5_start||''','''||l_week5_end||''','''||p_preferred_supplier||''')/xxeis.EIS_EDI_UTIL_ADOPTION_PKG.get_total_po_edi_sup_func('''||g_region||''','''||l_week5_start||''','''||l_week5_end||''','''||p_preferred_supplier||'''))*100) compliance_percentage_wk5,
to_char(to_date('''||l_week1_start||'''),''DD-Mon'') week1,
to_char(to_date('''||l_week2_start||'''),''DD-Mon'') week2,
to_char(to_date('''||l_week3_start||'''),''DD-Mon'') week3,
to_char(to_date('''||l_week4_start||'''),''DD-Mon'') week4,
decode(null,to_char(to_date('''||l_week5_start||'''),''DD-Mon''),to_char(0),to_char(to_date('''||l_week5_start||'''),''DD-Mon'')) week5,
decode(xxeis.EIS_EDI_UTIL_ADOPTION_PKG.get_po_non_purchase_edi_func(''ALL'','''||l_year_start_date||''','''||l_mon_end_date||''','''||p_preferred_supplier||'''),0,0,(xxeis.EIS_EDI_UTIL_ADOPTION_PKG.get_po_non_purchase_edi_func(''PURCHASE'','''||l_year_start_date||''','''||l_mon_end_date||''','''||p_preferred_supplier||''')/xxeis.EIS_EDI_UTIL_ADOPTION_PKG.get_po_non_purchase_edi_func(''ALL'','''||l_year_start_date||''','''||l_mon_end_date||''','''||p_preferred_supplier||'''))*100) purchasing_organization,
decode(xxeis.EIS_EDI_UTIL_ADOPTION_PKG.get_po_non_purchase_edi_func(''ALL'','''||l_year_start_date||''','''||l_mon_end_date||''','''||p_preferred_supplier||'''),0,0,(xxeis.EIS_EDI_UTIL_ADOPTION_PKG.get_po_non_purchase_edi_func(''NONPURCHASE'','''||l_year_start_date||''','''||l_mon_end_date||''','''||p_preferred_supplier||''')/xxeis.EIS_EDI_UTIL_ADOPTION_PKG.get_po_non_purchase_edi_func(''ALL'','''||l_year_start_date||''','''||l_mon_end_date||''','''||p_preferred_supplier||'''))*100) non_purchasing_organization,
decode((xxeis.EIS_EDI_UTIL_ADOPTION_PKG.get_total_po_edi_sup_func(null,'''||l_year_start_date||''','''||l_mon_end_date||''','''||p_preferred_supplier||''')),0,0,(xxeis.EIS_EDI_UTIL_ADOPTION_PKG.get_po_unconfirmed_edi_func('''||l_year_start_date||''','''||l_mon_end_date||''','''||p_preferred_supplier||''')/xxeis.EIS_EDI_UTIL_ADOPTION_PKG.get_total_po_edi_sup_func(null,'''||l_year_start_date||''','''||l_mon_end_date||''','''||p_preferred_supplier||'''))*100)  by_po_creator
from dual
';

 Fnd_File.Put_Line(FND_FILE.log,'l_main_sql  '||L_MAIN_SQL);
 
 OPEN L_REF_CURSOR1 FOR l_main_sql;  
  LOOP
    fetch L_REF_CURSOR1 bulk collect into report_data_tab limit 10;
  If report_data_tab.COUNT>0
  then
      FORALL J in 1..REPORT_DATA_TAB.COUNT
    	Insert Into XXEIS.EIS_XXWC_EDI_UTIL_ADOPTION_TBL Values report_data_tab(J);
    END IF;
    exit when L_REF_CURSOR1%notfound;
    END LOOP;
    g_region := null;
       commit;
    CLOSE L_REF_CURSOR1;
END loop;


  exception when others then
    Fnd_File.Put_Line(FND_FILE.log,'The Error is '||sqlcode||sqlerrm);
end;


END EIS_EDI_UTIL_ADOPTION_PKG;
/
