--Report Name            : EDI Utilization and Adoption - WC
--Import Instructions : 1. If RTF template needs to be uploaded to report automatically then move the RTF template to physical path of APPS_DATA_FILE_DIR oracle directory. Check dba_directories view to get the physical path of oracle directory. 
--                         (If you want to use any other oracle directory other than APPS_DATA_FILE_DIR then replace APPS_DATA_FILE_DIR with your oracle directory name below.)
--                      2. Make sure XXEIS schema has read/write permissions to above used oracle directory and give full permissions to the directory at OS level.
--                      3. Run this file in XXEIS user from SQL*Plus or any tool like Toad, SQL Navigator. 
exec xxeis.eis_rsc_ins.g_rtf_dir := 'APPS_DATA_FILE_DIR' ;
prompt Creating Object Data EIS_XXWC_EDI_UTIL_ADOPTION_V
set scan off define off
DECLARE
mod_exist VARCHAR2(1);
BEGIN 
mod_exist := XXEIS.EIS_RSC_INSERT.check_if_module_exist(200);
IF mod_exist = 'Y' THEN 
--Inserting Object EIS_XXWC_EDI_UTIL_ADOPTION_V
xxeis.eis_rsc_ins.v( 'EIS_XXWC_EDI_UTIL_ADOPTION_V',200,'','','','','SA059956','XXEIS','Eis Xxwc Edi Util Adoption V','EXEUAV','','','VIEW','US','','','');
--Delete Object Columns for EIS_XXWC_EDI_UTIL_ADOPTION_V
xxeis.eis_rsc_utility.delete_view_rows('EIS_XXWC_EDI_UTIL_ADOPTION_V',200,FALSE);
--Inserting Object Columns for EIS_XXWC_EDI_UTIL_ADOPTION_V
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_EDI_UTIL_ADOPTION_V','TOTAL_PO_SUBMIT_EDI_SUP_WK4',200,'Total Po Submit Edi Sup Wk4','TOTAL_PO_SUBMIT_EDI_SUP_WK4','','','','SA059956','NUMBER','','','Total Po Submit Edi Sup Wk4','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_EDI_UTIL_ADOPTION_V','PO_SUBMIT_VIA_EDI_WK4',200,'Po Submit Via Edi Wk4','PO_SUBMIT_VIA_EDI_WK4','','','','SA059956','NUMBER','','','Po Submit Via Edi Wk4','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_EDI_UTIL_ADOPTION_V','COMPLIANCE_PERCENTAGE_WK3',200,'Compliance Percentage Wk3','COMPLIANCE_PERCENTAGE_WK3','','','','SA059956','NUMBER','','','Compliance Percentage Wk3','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_EDI_UTIL_ADOPTION_V','PO_CONFIRMED_VIA_EDI_WK3',200,'Po Confirmed Via Edi Wk3','PO_CONFIRMED_VIA_EDI_WK3','','','','SA059956','NUMBER','','','Po Confirmed Via Edi Wk3','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_EDI_UTIL_ADOPTION_V','ADOPTION_PERCENTAGE_WK3',200,'Adoption Percentage Wk3','ADOPTION_PERCENTAGE_WK3','','','','SA059956','NUMBER','','','Adoption Percentage Wk3','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_EDI_UTIL_ADOPTION_V','TOTAL_PO_SUBMIT_EDI_SUP_WK3',200,'Total Po Submit Edi Sup Wk3','TOTAL_PO_SUBMIT_EDI_SUP_WK3','','','','SA059956','NUMBER','','','Total Po Submit Edi Sup Wk3','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_EDI_UTIL_ADOPTION_V','PO_SUBMIT_VIA_EDI_WK3',200,'Po Submit Via Edi Wk3','PO_SUBMIT_VIA_EDI_WK3','','','','SA059956','NUMBER','','','Po Submit Via Edi Wk3','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_EDI_UTIL_ADOPTION_V','COMPLIANCE_PERCENTAGE_WK2',200,'Compliance Percentage Wk2','COMPLIANCE_PERCENTAGE_WK2','','','','SA059956','NUMBER','','','Compliance Percentage Wk2','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_EDI_UTIL_ADOPTION_V','PO_CONFIRMED_VIA_EDI_WK2',200,'Po Confirmed Via Edi Wk2','PO_CONFIRMED_VIA_EDI_WK2','','','','SA059956','NUMBER','','','Po Confirmed Via Edi Wk2','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_EDI_UTIL_ADOPTION_V','ADOPTION_PERCENTAGE_WK2',200,'Adoption Percentage Wk2','ADOPTION_PERCENTAGE_WK2','','','','SA059956','NUMBER','','','Adoption Percentage Wk2','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_EDI_UTIL_ADOPTION_V','TOTAL_PO_SUBMIT_EDI_SUP_WK2',200,'Total Po Submit Edi Sup Wk2','TOTAL_PO_SUBMIT_EDI_SUP_WK2','','','','SA059956','NUMBER','','','Total Po Submit Edi Sup Wk2','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_EDI_UTIL_ADOPTION_V','PO_SUBMIT_VIA_EDI_WK2',200,'Po Submit Via Edi Wk2','PO_SUBMIT_VIA_EDI_WK2','','','','SA059956','NUMBER','','','Po Submit Via Edi Wk2','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_EDI_UTIL_ADOPTION_V','COMPLIANCE_PERCENTAGE_WK1',200,'Compliance Percentage Wk1','COMPLIANCE_PERCENTAGE_WK1','','','','SA059956','NUMBER','','','Compliance Percentage Wk1','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_EDI_UTIL_ADOPTION_V','PO_CONFIRMED_VIA_EDI_WK1',200,'Po Confirmed Via Edi Wk1','PO_CONFIRMED_VIA_EDI_WK1','','','','SA059956','NUMBER','','','Po Confirmed Via Edi Wk1','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_EDI_UTIL_ADOPTION_V','ADOPTION_PERCENTAGE_WK1',200,'Adoption Percentage Wk1','ADOPTION_PERCENTAGE_WK1','','','','SA059956','NUMBER','','','Adoption Percentage Wk1','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_EDI_UTIL_ADOPTION_V','TOTAL_PO_SUBMIT_EDI_SUP_WK1',200,'Total Po Submit Edi Sup Wk1','TOTAL_PO_SUBMIT_EDI_SUP_WK1','','','','SA059956','NUMBER','','','Total Po Submit Edi Sup Wk1','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_EDI_UTIL_ADOPTION_V','PO_SUBMIT_VIA_EDI_WK1',200,'Po Submit Via Edi Wk1','PO_SUBMIT_VIA_EDI_WK1','','','','SA059956','NUMBER','','','Po Submit Via Edi Wk1','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_EDI_UTIL_ADOPTION_V','COMPLIANCE_PERCENTAGE_MNTH',200,'Compliance Percentage Mnth','COMPLIANCE_PERCENTAGE_MNTH','','','','SA059956','NUMBER','','','Compliance Percentage Mnth','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_EDI_UTIL_ADOPTION_V','PO_CONFIRMED_VIA_EDI_MNTH',200,'Po Confirmed Via Edi Mnth','PO_CONFIRMED_VIA_EDI_MNTH','','','','SA059956','NUMBER','','','Po Confirmed Via Edi Mnth','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_EDI_UTIL_ADOPTION_V','ADOPTION_PERCENTAGE_MNTH',200,'Adoption Percentage Mnth','ADOPTION_PERCENTAGE_MNTH','','','','SA059956','NUMBER','','','Adoption Percentage Mnth','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_EDI_UTIL_ADOPTION_V','TOTAL_PO_SUBMIT_EDI_SUP_MNTH',200,'Total Po Submit Edi Sup Mnth','TOTAL_PO_SUBMIT_EDI_SUP_MNTH','','','','SA059956','NUMBER','','','Total Po Submit Edi Sup Mnth','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_EDI_UTIL_ADOPTION_V','PO_SUBMIT_VIA_EDI_MNTH',200,'Po Submit Via Edi Mnth','PO_SUBMIT_VIA_EDI_MNTH','','','','SA059956','NUMBER','','','Po Submit Via Edi Mnth','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_EDI_UTIL_ADOPTION_V','COMPLIANCE_PERCENTAGE_YR',200,'Compliance Percentage Yr','COMPLIANCE_PERCENTAGE_YR','','','','SA059956','NUMBER','','','Compliance Percentage Yr','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_EDI_UTIL_ADOPTION_V','PO_CONFIRMED_VIA_EDI_YR',200,'Po Confirmed Via Edi Yr','PO_CONFIRMED_VIA_EDI_YR','','','','SA059956','NUMBER','','','Po Confirmed Via Edi Yr','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_EDI_UTIL_ADOPTION_V','ADOPTION_PERCENTAGE_YR',200,'Adoption Percentage Yr','ADOPTION_PERCENTAGE_YR','','','','SA059956','NUMBER','','','Adoption Percentage Yr','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_EDI_UTIL_ADOPTION_V','TOTAL_PO_SUBMIT_EDI_SUP_YR',200,'Total Po Submit Edi Sup Yr','TOTAL_PO_SUBMIT_EDI_SUP_YR','','','','SA059956','NUMBER','','','Total Po Submit Edi Sup Yr','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_EDI_UTIL_ADOPTION_V','PO_SUBMIT_VIA_EDI_YR',200,'Po Submit Via Edi Yr','PO_SUBMIT_VIA_EDI_YR','','','','SA059956','NUMBER','','','Po Submit Via Edi Yr','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_EDI_UTIL_ADOPTION_V','REGION',200,'Region','REGION','','','','SA059956','VARCHAR2','','','Region','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_EDI_UTIL_ADOPTION_V','BY_PO_CREATOR',200,'By Po Creator','BY_PO_CREATOR','','','','SA059956','NUMBER','','','By Po Creator','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_EDI_UTIL_ADOPTION_V','NON_PURCHASING_ORGANIZATION',200,'Non Purchasing Organization','NON_PURCHASING_ORGANIZATION','','','','SA059956','NUMBER','','','Non Purchasing Organization','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_EDI_UTIL_ADOPTION_V','PURCHASING_ORGANIZATION',200,'Purchasing Organization','PURCHASING_ORGANIZATION','','','','SA059956','NUMBER','','','Purchasing Organization','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_EDI_UTIL_ADOPTION_V','COMPLIANCE_PERCENTAGE_WK5',200,'Compliance Percentage Wk5','COMPLIANCE_PERCENTAGE_WK5','','','','SA059956','NUMBER','','','Compliance Percentage Wk5','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_EDI_UTIL_ADOPTION_V','PO_CONFIRMED_VIA_EDI_WK5',200,'Po Confirmed Via Edi Wk5','PO_CONFIRMED_VIA_EDI_WK5','','','','SA059956','NUMBER','','','Po Confirmed Via Edi Wk5','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_EDI_UTIL_ADOPTION_V','ADOPTION_PERCENTAGE_WK5',200,'Adoption Percentage Wk5','ADOPTION_PERCENTAGE_WK5','','','','SA059956','NUMBER','','','Adoption Percentage Wk5','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_EDI_UTIL_ADOPTION_V','TOTAL_PO_SUBMIT_EDI_SUP_WK5',200,'Total Po Submit Edi Sup Wk5','TOTAL_PO_SUBMIT_EDI_SUP_WK5','','','','SA059956','NUMBER','','','Total Po Submit Edi Sup Wk5','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_EDI_UTIL_ADOPTION_V','PO_SUBMIT_VIA_EDI_WK5',200,'Po Submit Via Edi Wk5','PO_SUBMIT_VIA_EDI_WK5','','','','SA059956','NUMBER','','','Po Submit Via Edi Wk5','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_EDI_UTIL_ADOPTION_V','COMPLIANCE_PERCENTAGE_WK4',200,'Compliance Percentage Wk4','COMPLIANCE_PERCENTAGE_WK4','','','','SA059956','NUMBER','','','Compliance Percentage Wk4','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_EDI_UTIL_ADOPTION_V','PO_CONFIRMED_VIA_EDI_WK4',200,'Po Confirmed Via Edi Wk4','PO_CONFIRMED_VIA_EDI_WK4','','','','SA059956','NUMBER','','','Po Confirmed Via Edi Wk4','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_EDI_UTIL_ADOPTION_V','ADOPTION_PERCENTAGE_WK4',200,'Adoption Percentage Wk4','ADOPTION_PERCENTAGE_WK4','','','','SA059956','NUMBER','','','Adoption Percentage Wk4','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_EDI_UTIL_ADOPTION_V','WEEK1',200,'Week1','WEEK1','','','','SA059956','VARCHAR2','','','Week1','','','','','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_EDI_UTIL_ADOPTION_V','WEEK2',200,'Week2','WEEK2','','','','SA059956','VARCHAR2','','','Week2','','','','','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_EDI_UTIL_ADOPTION_V','WEEK3',200,'Week3','WEEK3','','','','SA059956','VARCHAR2','','','Week3','','','','','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_EDI_UTIL_ADOPTION_V','WEEK4',200,'Week4','WEEK4','','','','SA059956','VARCHAR2','','','Week4','','','','','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_EDI_UTIL_ADOPTION_V','WEEK5',200,'Week5','WEEK5','','','','SA059956','VARCHAR2','','','Week5','','','','','');
--Inserting Object Components for EIS_XXWC_EDI_UTIL_ADOPTION_V
--Inserting Object Component Joins for EIS_XXWC_EDI_UTIL_ADOPTION_V
ELSE
RAISE_APPLICATION_ERROR (-20001,'Module does not exist. Import/Create the module with id 200');
END IF;
END;
/
prompt Creating Report LOV Data for EDI Utilization and Adoption - WC
set scan off define off
DECLARE
mod_exist VARCHAR2(1);
BEGIN 
mod_exist := XXEIS.EIS_RSC_INSERT.check_if_module_exist(200);
IF mod_exist = 'Y' THEN 
--Inserting Report LOVs - EDI Utilization and Adoption - WC
xxeis.eis_rsc_ins.lov( 200,'select  per.period_name , led.name ledger_name, per.period_year, per.period_num, per.start_date, per.end_date
from    gl_periods per,gl_ledgers led
where  per.period_set_name = led.period_set_name
and     gl_security_pkg.validate_access(led.ledger_ID) = ''TRUE''
and     led.accounted_period_type = per.period_type','','AP GL Period Names LOV','This LOV lists all General Ledger Periods defined','XXEIS_RS_ADMIN',NULL,'','','','Y','','','','US');
xxeis.eis_rsc_ins.lov( '','SELECT DISTINCT mp.attribute9 region
FROM mtl_parameters mp
WHERE mp.attribute9 IS NOT NULL
ORDER BY 1','','EIS XXWC Region LOV','This LOV Lists all region details.','SA059956',NULL,'N','','','N','N','','','US');
xxeis.eis_rsc_ins.lov( 200,'SELECT Preferred_vendor,
  Preferred_vendor_category
FROM
  (SELECT ''No'' Preferred_vendor ,
    ''Tier1, Tier2, Tier3, Tier4'' Preferred_vendor_category
  FROM dual
  UNION ALL
  SELECT ''Yes'' Preferred_vendor ,
    ''Tier1, Tier2'' Preferred_vendor_category
  FROM dual
  )
ORDER BY 1 DESC','','EIS XXWC Preferred Vendor Type LOV','','SA059956',NULL,'N','','','N','N','','','US');
null;
ELSE
RAISE_APPLICATION_ERROR (-20001,'Module does not exist. Import/Create the module with id 200');
END IF;
END;
/
prompt Creating Report Data for EDI Utilization and Adoption - WC
set scan off define off
DECLARE
mod_exist varchar2(1);
BEGIN 
mod_exist := XXEIS.EIS_RSC_INSERT.check_if_module_exist(200);
IF mod_exist = 'Y' THEN 
--Deleting Report data - EDI Utilization and Adoption - WC
xxeis.eis_rsc_utility.delete_report_rows( 'EDI Utilization and Adoption - WC',200 );
--Inserting Report - EDI Utilization and Adoption - WC
xxeis.eis_rsc_ins.r( 200,'EDI Utilization and Adoption - WC','','WC & Supplier metrics by PO and Transaction type.  Adoption by PO Creator by Region','','','','SA059956','EIS_XXWC_EDI_UTIL_ADOPTION_V','Y','','','SA059956','','N','White Cap Reports','EXCEL,','','N','','','','','','N','','US','','','','','','','','','','','','','','','','');
--Inserting Report Columns - EDI Utilization and Adoption - WC
xxeis.eis_rsc_ins.rc( 'EDI Utilization and Adoption - WC',200,'ADOPTION_PERCENTAGE_MNTH','Adoption Percentage Mnth','Adoption Percentage Mnth','','~~~','default','','10','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_EDI_UTIL_ADOPTION_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'EDI Utilization and Adoption - WC',200,'ADOPTION_PERCENTAGE_WK1','Adoption Percentage Wk1','Adoption Percentage Wk1','','~~~','default','','14','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_EDI_UTIL_ADOPTION_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'EDI Utilization and Adoption - WC',200,'ADOPTION_PERCENTAGE_WK2','Adoption Percentage Wk2','Adoption Percentage Wk2','','~~~','default','','18','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_EDI_UTIL_ADOPTION_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'EDI Utilization and Adoption - WC',200,'ADOPTION_PERCENTAGE_WK3','Adoption Percentage Wk3','Adoption Percentage Wk3','','~~~','default','','22','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_EDI_UTIL_ADOPTION_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'EDI Utilization and Adoption - WC',200,'ADOPTION_PERCENTAGE_WK4','Adoption Percentage Wk4','Adoption Percentage Wk4','','~~~','default','','26','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_EDI_UTIL_ADOPTION_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'EDI Utilization and Adoption - WC',200,'ADOPTION_PERCENTAGE_WK5','Adoption Percentage Wk5','Adoption Percentage Wk5','','~~~','default','','30','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_EDI_UTIL_ADOPTION_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'EDI Utilization and Adoption - WC',200,'ADOPTION_PERCENTAGE_YR','Adoption Percentage Yr','Adoption Percentage Yr','','~~~','default','','6','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_EDI_UTIL_ADOPTION_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'EDI Utilization and Adoption - WC',200,'BY_PO_CREATOR','By Po Creator','By Po Creator','','~~~','default','','2','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_EDI_UTIL_ADOPTION_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'EDI Utilization and Adoption - WC',200,'COMPLIANCE_PERCENTAGE_MNTH','Compliance Percentage Mnth','Compliance Percentage Mnth','','~~~','default','','12','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_EDI_UTIL_ADOPTION_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'EDI Utilization and Adoption - WC',200,'COMPLIANCE_PERCENTAGE_WK1','Compliance Percentage Wk1','Compliance Percentage Wk1','','~~~','default','','16','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_EDI_UTIL_ADOPTION_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'EDI Utilization and Adoption - WC',200,'COMPLIANCE_PERCENTAGE_WK2','Compliance Percentage Wk2','Compliance Percentage Wk2','','~~~','default','','20','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_EDI_UTIL_ADOPTION_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'EDI Utilization and Adoption - WC',200,'COMPLIANCE_PERCENTAGE_WK3','Compliance Percentage Wk3','Compliance Percentage Wk3','','~~~','default','','24','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_EDI_UTIL_ADOPTION_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'EDI Utilization and Adoption - WC',200,'COMPLIANCE_PERCENTAGE_WK4','Compliance Percentage Wk4','Compliance Percentage Wk4','','~~~','default','','28','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_EDI_UTIL_ADOPTION_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'EDI Utilization and Adoption - WC',200,'COMPLIANCE_PERCENTAGE_WK5','Compliance Percentage Wk5','Compliance Percentage Wk5','','~~~','default','','32','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_EDI_UTIL_ADOPTION_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'EDI Utilization and Adoption - WC',200,'COMPLIANCE_PERCENTAGE_YR','Compliance Percentage Yr','Compliance Percentage Yr','','~~~','default','','8','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_EDI_UTIL_ADOPTION_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'EDI Utilization and Adoption - WC',200,'NON_PURCHASING_ORGANIZATION','Non Purchasing Organization','Non Purchasing Organization','','~~~','default','','3','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_EDI_UTIL_ADOPTION_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'EDI Utilization and Adoption - WC',200,'PO_CONFIRMED_VIA_EDI_MNTH','Po Confirmed Via Edi Mnth','Po Confirmed Via Edi Mnth','','~~~','default','','11','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_EDI_UTIL_ADOPTION_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'EDI Utilization and Adoption - WC',200,'PO_CONFIRMED_VIA_EDI_WK1','Po Confirmed Via Edi Wk1','Po Confirmed Via Edi Wk1','','~~~','default','','15','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_EDI_UTIL_ADOPTION_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'EDI Utilization and Adoption - WC',200,'PO_CONFIRMED_VIA_EDI_WK2','Po Confirmed Via Edi Wk2','Po Confirmed Via Edi Wk2','','~~~','default','','19','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_EDI_UTIL_ADOPTION_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'EDI Utilization and Adoption - WC',200,'PO_CONFIRMED_VIA_EDI_WK3','Po Confirmed Via Edi Wk3','Po Confirmed Via Edi Wk3','','~~~','default','','23','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_EDI_UTIL_ADOPTION_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'EDI Utilization and Adoption - WC',200,'PO_CONFIRMED_VIA_EDI_WK4','Po Confirmed Via Edi Wk4','Po Confirmed Via Edi Wk4','','~~~','default','','27','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_EDI_UTIL_ADOPTION_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'EDI Utilization and Adoption - WC',200,'PO_CONFIRMED_VIA_EDI_WK5','Po Confirmed Via Edi Wk5','Po Confirmed Via Edi Wk5','','~~~','default','','31','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_EDI_UTIL_ADOPTION_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'EDI Utilization and Adoption - WC',200,'PO_CONFIRMED_VIA_EDI_YR','Po Confirmed Via Edi Yr','Po Confirmed Via Edi Yr','','~~~','default','','7','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_EDI_UTIL_ADOPTION_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'EDI Utilization and Adoption - WC',200,'PO_SUBMIT_VIA_EDI_MNTH','Po Submit Via Edi Mnth','Po Submit Via Edi Mnth','','~~~','default','','9','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_EDI_UTIL_ADOPTION_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'EDI Utilization and Adoption - WC',200,'PO_SUBMIT_VIA_EDI_WK1','Po Submit Via Edi Wk1','Po Submit Via Edi Wk1','','~~~','default','','13','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_EDI_UTIL_ADOPTION_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'EDI Utilization and Adoption - WC',200,'PO_SUBMIT_VIA_EDI_WK2','Po Submit Via Edi Wk2','Po Submit Via Edi Wk2','','~~~','default','','17','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_EDI_UTIL_ADOPTION_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'EDI Utilization and Adoption - WC',200,'PO_SUBMIT_VIA_EDI_WK3','Po Submit Via Edi Wk3','Po Submit Via Edi Wk3','','~~~','default','','21','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_EDI_UTIL_ADOPTION_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'EDI Utilization and Adoption - WC',200,'PO_SUBMIT_VIA_EDI_WK4','Po Submit Via Edi Wk4','Po Submit Via Edi Wk4','','~~~','default','','25','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_EDI_UTIL_ADOPTION_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'EDI Utilization and Adoption - WC',200,'PO_SUBMIT_VIA_EDI_WK5','Po Submit Via Edi Wk5','Po Submit Via Edi Wk5','','~~~','default','','29','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_EDI_UTIL_ADOPTION_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'EDI Utilization and Adoption - WC',200,'PO_SUBMIT_VIA_EDI_YR','Po Submit Via Edi Yr','Po Submit Via Edi Yr','','~~~','default','','5','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_EDI_UTIL_ADOPTION_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'EDI Utilization and Adoption - WC',200,'PURCHASING_ORGANIZATION','Purchasing Organization','Purchasing Organization','','~~~','default','','1','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_EDI_UTIL_ADOPTION_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'EDI Utilization and Adoption - WC',200,'REGION','Region','Region','','','default','','4','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_EDI_UTIL_ADOPTION_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'EDI Utilization and Adoption - WC',200,'WEEK1','Week1','Week1','','','default','','33','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_EDI_UTIL_ADOPTION_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'EDI Utilization and Adoption - WC',200,'WEEK2','Week2','Week2','','','default','','34','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_EDI_UTIL_ADOPTION_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'EDI Utilization and Adoption - WC',200,'WEEK3','Week3','Week3','','','default','','35','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_EDI_UTIL_ADOPTION_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'EDI Utilization and Adoption - WC',200,'WEEK4','Week4','Week4','','','default','','36','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_EDI_UTIL_ADOPTION_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'EDI Utilization and Adoption - WC',200,'WEEK5','Week5','Week5','','','default','','37','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_EDI_UTIL_ADOPTION_V','','','','US','','');
--Inserting Report Parameters - EDI Utilization and Adoption - WC
xxeis.eis_rsc_ins.rp( 'EDI Utilization and Adoption - WC',200,'Region','Region','','','EIS XXWC Region LOV','','VARCHAR2','N','Y','1','Y','N','CONSTANT','SA059956','Y','N','','','','EIS_XXWC_EDI_UTIL_ADOPTION_V','','','US','');
xxeis.eis_rsc_ins.rp( 'EDI Utilization and Adoption - WC',200,'Fiscal Period','Fiscal Period','','','AP GL Period Names LOV','select period_name from GL_PERIODS GPS where GPS.PERIOD_SET_NAME = ''4-4-QTR'' and TRUNC(sysdate) between START_DATE and END_DATE and rownum=1','VARCHAR2','Y','Y','2','N','N','SQL','SA059956','Y','N','','','','EIS_XXWC_EDI_UTIL_ADOPTION_V','','','US','');
xxeis.eis_rsc_ins.rp( 'EDI Utilization and Adoption - WC',200,'Preferred Supplier','Preferred Supplier','','','EIS XXWC Preferred Vendor Type LOV','''Yes''','VARCHAR2','Y','Y','3','N','N','CONSTANT','SA059956','Y','N','','','','EIS_XXWC_EDI_UTIL_ADOPTION_V','','','US','');
--Inserting Dependent Parameters - EDI Utilization and Adoption - WC
--Inserting Report Conditions - EDI Utilization and Adoption - WC
--Inserting Report Sorts - EDI Utilization and Adoption - WC
xxeis.eis_rsc_ins.rs( 'EDI Utilization and Adoption - WC',200,'REGION','ASC','SA059956','1','');
--Inserting Report Triggers - EDI Utilization and Adoption - WC
xxeis.eis_rsc_ins.rt( 'EDI Utilization and Adoption - WC',200,'BEGIN  
XXEIS.EIS_EDI_UTIL_ADOPTION_PKG.GET_EDI_UTILIZE_DTLS_PROC(
    P_REGION 				=> :Region,
    P_FISCAL_PERIOD 		        => :Fiscal Period, 
    P_PREFERRED_SUPPLIER 	        => :Preferred Supplier
 );
END;','B','Y','SA059956','BQ');
--inserting report templates - EDI Utilization and Adoption - WC
xxeis.eis_rsc_ins.r_tem( 'EDI Utilization and Adoption - WC','EDI Utilization and Adoption - WC','EDI Utilization and Adoption - WC','','','','','','','','','','','EDI Utilization and Adoption - WC.rtf','SA059956','X','','','Y','Y','Y','N');
--Inserting Report Portals - EDI Utilization and Adoption - WC
--inserting report dashboards - EDI Utilization and Adoption - WC
--Exporting report views table information -  
xxeis.eis_rsc_ins.rviews( 'EDI Utilization and Adoption - WC','200','EIS_XXWC_EDI_UTIL_ADOPTION_V','EIS_XXWC_EDI_UTIL_ADOPTION_V','N','');
--inserting report security - EDI Utilization and Adoption - WC
xxeis.eis_rsc_ins.rsec( 'EDI Utilization and Adoption - WC','201','','XXWC_PUR_SUPER_USER',200,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'EDI Utilization and Adoption - WC','201','','HDS_PRCHSNG_SPR_USR',200,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'EDI Utilization and Adoption - WC','201','','XXWC_PURCHASING_SR_MRG_WC',200,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'EDI Utilization and Adoption - WC','201','','XXWC_PURCHASING_RPM',200,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'EDI Utilization and Adoption - WC','201','','XXWC_PURCHASING_MGR',200,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'EDI Utilization and Adoption - WC','201','','XXWC_PURCHASING_INQUIRY',200,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'EDI Utilization and Adoption - WC','201','','XXWC_PURCHASING_BUYER',200,'SA059956','','','');
--Inserting Report Pivots - EDI Utilization and Adoption - WC
--Inserting Report Distribution Details 
--Inserting Report Distribution Details 
--Inserting Report SubTotals Details 
--Inserting Report aggregations 
--Inserting Report   Version details- EDI Utilization and Adoption - WC
xxeis.eis_rsc_ins.rv( 'EDI Utilization and Adoption - WC','','EDI Utilization and Adoption - WC','SA059956','03-JAN-2018');
ELSE
RAISE_APPLICATION_ERROR (-20001,'Module does not exist. Import/Create the module with id 200');
END IF;
END;
/
