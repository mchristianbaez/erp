-- ******************************************************************************
-- *  Copyright (c) 2011 HDS Supply
-- *  All rights reserved.
-- ******************************************************************************
-- *   $Header XXWC_GL_AHH_JOURNALS_STG_TBL.ctl $
-- *   Module Name: XXWC_GL_AHH_JOURNALS_STG_TBL.ctl
-- *
-- *   PURPOSE:   This is the control file for loading the staging table XXWC.XXWC_GL_AHH_JOURNALS_STG_TBL
-- *
-- *   REVISIONS:
-- *   Ver Date AuthorDescription
-- *   ---------  ----------  ---------------  -------------------------
-- *   1.0 05/15/2018  P.Vamshidhar     TMS# 20180515-00049 - Initial Version
-- * ****************************************************************************

LOAD DATA
INFILE *
INTO TABLE XXWC.XXWC_GL_AHH_JOURNALS_STG_TBL
APPEND
FIELDS TERMINATED BY '|'
optionally enclosed by '"'
TRAILING NULLCOLS
(
STATUS char"NVL(:STATUS,'NEW')",
ACCOUNTING_DATE   date(8) "MM/DD/RR",
CURRENCY_CODE,
ACTUAL_FLAG,
PRODUCT,
LOCATION,
COST_CENTER,
ACCOUNT,
PROJECT_CODE,
FUTURE_USE,
FUTURE_USE2,
DEBIT_AMOUNT,
CREDIT_AMOUNT,
BATCH_NUMBER,
BATCH_DESCRIPTION,
JOURNAL_NAME,
JOURNAL_DESCRIPTION,
JOURNAL_LINE_DESC,
REFERENCE_FIELD1,
REFERENCE_FIELD2,
REFERENCE_FIELD3,
REFERENCE_FIELD4,
REFERENCE_FIELD5,
REFERENCE_FIELD6,
REFERENCE_FIELD7,
REFERENCE_FIELD8,
REFERENCE_FIELD9,
REFERENCE_FIELD10)