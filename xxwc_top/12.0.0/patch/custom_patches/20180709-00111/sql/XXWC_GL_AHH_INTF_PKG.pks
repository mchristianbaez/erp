CREATE OR REPLACE PACKAGE xxwc_gl_ahh_intf_pkg AS
  /***************************************************************************************************************************************
   $Header XXWC_GL_AHH_INTF_PKG $
   Module Name: XXWC_GL_AHH_INTF_PKG.pks
  
   PURPOSE:   This package will be used to import AHH journals
  
   REVISIONS:
   Ver        Date        Author                             Description
   ---------  ----------  -------------------------------    -------------------------
   1.0        17/05/2018  P.Vamshidhar                       TMS# 20180515-00049 - AH Harries Journals Conversion
                                                             Initial Version
  *************************************************************************************************************************************/

  PROCEDURE main(x_errbuf       OUT VARCHAR2
                ,x_retcode      OUT VARCHAR2
                ,p_gl_source    IN VARCHAR2
                ,p_gl_category  IN VARCHAR2
                ,p_default_acct IN VARCHAR2
                ,p_gl_import    IN VARCHAR2
                ,p_mode         IN VARCHAR2);

END xxwc_gl_ahh_intf_pkg;
/
