CREATE OR REPLACE FORCE VIEW XXEIS.EIS_XXWC_OM_OUTSTG_RTL_V
(ORDER_NUMBER
, EST_RENTAL_RETURN_DATE
, CREATED_BY
, LINE_NUMBER
, ORDER_TYPE
, CUSTOMER_NAME
, CUSTOMER_JOB_NAME
, SALES_PERSON_NAME
, ITEM_NUMBER
, ITEM_DESCRIPTION
, QUANTITY_ON_RENT
, RENTAL_START_DATE
, RENTAL_LOCATION
, DAYS_ON_RENT
, HEADER_ID
, LINE_ID
, PARTY_ID
, ORGANIZATION_ID
, INVENTORY_ITEM_ID
, MSI_ORGANIZATION_ID
, Pri_Bill_To_Ph
, bill_to_cust_Name
, bill_to_cust_ph_no)
/**************************************************************************************************************
$Header XXEIS.EIS_XXWC_OM_OUTSTG_RTL_V $
Module Name: Order Management
PURPOSE: View for EIS Report Outstanding Rental Report
TMS Task Id : NA
REVISIONS:
Ver        Date        Author                Description
---------  ----------  ------------------    ----------------
1.0        NA           NA   Initial Version
1.1        11/18/2015  Mahender Reddy         20151014-00004
1.2        12/21/2015  Mahender Reddy         20151215-00164
1.3		   7/29/2016   Siva 				  20160715-00164
**************************************************************************************************************/
AS 
SELECT oh.order_number ,
  DECODE (SUBSTR (ol1.attribute1, 5, 1) ,'/', TO_CHAR (TO_DATE (SUBSTR (ol1.attribute1, 1, 10), 'yyyy/mm/dd'), 'DD-MON-YYYY') ,ol1.attribute1) est_rental_return_date ,
  ppf.full_name created_by ,
  DECODE (ol.option_number ,'', (ol.line_number
  || '.'
  || ol.shipment_number) , (ol.line_number
  || '.'
  || ol.shipment_number
  || '.'
  || ol.option_number)) line_number ,
  ott.name order_type ,
  hzp.party_name customer_name ,
  hzcs_ship_to.location customer_job_name ,
  rep.name sales_person_name ,
  msi.segment1 item_number ,
  msi.description item_description ,
  ol.ordered_quantity quantity_on_rent ,
  xxeis.eis_rs_xxwc_com_util_pkg.get_rental_start_date (ol.link_to_line_id) rental_start_date ,
  mtp.organization_code rental_location ,
  (TRUNC (SYSDATE) - xxeis.eis_rs_xxwc_com_util_pkg.get_rental_start_date (ol.link_to_line_id)) days_on_rent , ---Primary Keys
  oh.header_id ,
  ol.line_id ,
  hzp.party_id ,
  mtp.organization_id ,
  msi.inventory_item_id ,
  msi.organization_id msi_organization_id ,
  (SELECT raw_phone_number
  FROM apps.hz_contact_points
  WHERE 1              = 1
  AND owner_table_id   = hzp.party_id
  AND owner_table_name = 'HZ_PARTIES'
  AND primary_flag     = 'Y'
  AND ROWNUM           =1
  ) Pri_Bill_To_Ph,                   --Added For Ver 1.1
  btc.party_name bill_to_cust_Name ,  --Added For Ver 1.1
  btc.phone_number bill_to_cust_ph_no --Added For Ver 1.1
  --descr#flexfield#start
  --descr#flexfield#end
  --gl#accountff#start
  --gl#accountff#end
FROM oe_order_lines ol ,
    oe_order_lines ol1 ,
    oe_order_headers oh ,
    oe_transaction_types_vl ott ,
    ra_salesreps rep ,
    hz_cust_accounts hca ,
    hz_parties hzp ,
    fnd_user fu ,
    per_people_f ppf ,
    mtl_system_items_kfv msi ,
    mtl_parameters mtp ,
    hz_cust_site_uses hzcs_ship_to ,
    hz_cust_acct_sites hcas_ship_to ,
    hz_party_sites hzps_ship_to ,
    hz_locations hzl_ship_to ,
    (SELECT party.party_name party_name ,
      DECODE(hcp.Phone_number,NULL,NULL, '('
      ||hcp.Phone_area_code
      ||') '
      ||hcp.Phone_number
      ||' EX '
      ||NVL(hcp.Phone_extension,'-')) phone_number,
      acct_role.cust_account_role_ID cust_account_role_ID
    FROM apps.hz_cust_account_roles acct_role,
      apps.hz_parties party,
      apps.hz_relationships rel,
      apps.hz_org_contacts org_cont ,
      APPS.hz_contact_points hcp
    WHERE 1                            = 1
    AND acct_role.party_id             = rel.party_id
    AND acct_role.role_type            = 'CONTACT'
    AND org_cont.party_relationship_id = rel.relationship_id
    AND rel.subject_table_name         = 'HZ_PARTIES'
    AND rel.object_table_name          = 'HZ_PARTIES'
    AND rel.directional_flag           = 'F'
    AND rel.subject_id                 = party.party_id
    AND HCP.OWNER_TABLE_ID(+)          = ACCT_ROLE.PARTY_ID
    AND HCP.OWNER_TABLE_NAME(+)        = 'HZ_PARTIES' -- Added for Ver 1.2
    AND hcp.contact_point_type(+)      = 'PHONE' -- Added for Ver 1.3
    ) btc --Added For Ver 1.1
  WHERE ol.flow_status_code IN ('AWAITING_RETURN')
  AND oh.header_id           = ol.header_id
  AND ol.link_to_line_id     = ol1.line_id
  AND OH.ORDER_TYPE_ID       = OTT.TRANSACTION_TYPE_ID
  AND oh.org_id              = OTT.org_id -- Added for Ver 1.3
  AND OH.SALESREP_ID         = REP.SALESREP_ID(+)
  AND oh.org_id              = REP.org_id(+)  -- Added for Ver 1.3
  AND oh.sold_to_org_id      = hca.cust_account_id(+)
  AND hca.party_id           = hzp.party_id(+)
    --AND fu.user_id = ol.created_by  --Commented by SowmyaKM on 5/5/2015 for 20150218-00108 to get created_by from order_header table than from order_lines
  AND fu.user_id     = oh.created_by --Added by SowmyaKM on 5/5/2015 for 20150218-00108 to get created_by from order_header table than from order_lines
  AND fu.employee_id = ppf.person_id(+)
    --  AND TRUNC (OL.CREATION_DATE) BETWEEN NVL (PPF.EFFECTIVE_START_DATE, TRUNC (OL.CREATION_DATE) ) AND NVL (PPF.EFFECTIVE_END_DATE, TRUNC (OL.CREATION_DATE) )
  AND TRUNC (SYSDATE) BETWEEN ppf.effective_start_date(+) AND ppf.effective_end_date(+) -- sjampala commented the above condition and added sysdate condition
  AND msi.organization_id            = ol.ship_from_org_id
  AND msi.inventory_item_id          = ol.inventory_item_id
  AND ol.ship_from_org_id            = mtp.organization_id(+)
  AND oh.ship_to_org_id              = hzcs_ship_to.site_use_id(+)
  AND hzcs_ship_to.cust_acct_site_id = hcas_ship_to.cust_acct_site_id(+)
  AND hcas_ship_to.party_site_id     = hzps_ship_to.party_site_id(+)
  AND hzl_ship_to.location_id(+)     = hzps_ship_to.location_id
  AND ott.name                      IN ('WC LONG TERM RENTAL', 'WC SHORT TERM RENTAL')
  and BTC.CUST_ACCOUNT_ROLE_ID(+)    = OH.INVOICE_TO_CONTACT_ID --Added For Ver 1.1
/
  