DECLARE
   CURSOR incorrect_mmt
   IS
      SELECT rsl.from_organization_id,
             rsl.to_organization_id,
             mmt.transfer_organization_id,
             mmt.organization_id,
             MMT.COST_GROUP_ID,
             mmt.TRANSFER_COST_GROUP_ID,
             mmt.XFR_OWNING_ORGANIZATION_ID,
             mmt.XFR_PLANNING_ORGANIZATION_ID,
             mmt.transaction_action_id,
             mmt.transaction_source_type_id,
             mmt.transaction_id
        FROM mtl_material_transactions mmt,
             rcv_shipment_lines rsl,
             rcv_transactions rt
       WHERE     mmt.TRANSACTION_TYPE_ID = 61
             AND mmt.TRANSACTION_ACTION_ID = 12
             AND mmt.TRANSACTION_SOURCE_TYPE_ID = 7
             AND mmt.FOB_POINT = 2
             AND mmt.costed_flag = 'E'
             AND mmt.ERROR_CODE LIKE '%CST_INVALID_INTERORG%'
             AND MMT.TRANSFER_ORGANIZATION_ID = MMT.organization_id
             AND mmt.rcv_transaction_id = rt.transaction_id
             AND rt.shipment_line_id = rsl.shipment_line_id;
BEGIN
   FOR i IN incorrect_mmt
   LOOP
      UPDATE mtl_material_transactions mmt
         SET ORGANIZATION_ID = i.TO_ORGANIZATION_ID,
             TRANSFER_ORGANIZATION_ID = i.FROM_ORGANIZATION_ID,
             XFR_OWNING_ORGANIZATION_ID = i.FROM_ORGANIZATION_ID,
             XFR_PLANNING_ORGANIZATION_ID = i.FROM_ORGANIZATION_ID,
             costed_flag = 'N',
             ERROR_CODE = NULL,
             error_explanation = NULL,
             transaction_group_id = NULL,
             transaction_set_id = NULL
       WHERE     transaction_id = i.transaction_id
             AND EXISTS              --cost group match the CG of receving org
                    (SELECT COST_GROUP_ID
                       FROM cst_cost_groups
                      WHERE     COST_GROUP_ID = i.COST_GROUP_ID
                            AND ORGANIZATION_ID = i.TO_ORGANIZATION_ID)
             AND EXISTS    -- transfer cost group match the CG of shipping org
                    (SELECT COST_GROUP_ID
                       FROM cst_cost_groups
                      WHERE     COST_GROUP_ID = i.TRANSFER_COST_GROUP_ID
                            AND ORGANIZATION_ID = i.FROM_ORGANIZATION_ID);
   END LOOP;

   COMMIT;
END;
/

UPDATE mtl_material_transactions
   SET costed_flag = 'N',
       transaction_group_id = NULL,
       ERROR_CODE = NULL,
       error_explanation = NULL
 WHERE costed_flag IN ('N', 'E');

COMMIT;
/

