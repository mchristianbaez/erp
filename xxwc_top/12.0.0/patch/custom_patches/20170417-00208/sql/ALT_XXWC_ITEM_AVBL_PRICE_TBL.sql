/*************************************************************************
  $Header ALT_XXWC_ITEM_AVBL_PRICE_TBL.sql $
  Module Name: ALT_XXWC_ITEM_AVBL_PRICE_TBL.sql

  PURPOSE:   To Add the column subinventory

  REVISIONS:
  Ver        Date        Author                     Description
  ---------  ----------  ---------------         -------------------------
  1.0        04/18/2017  Rakesh Patel            TMS#20170417-00208 Mobile Quote Form 0.2 Release - PL/SQL api changes
**************************************************************************/
ALTER TABLE XXWC.XXWC_ITEM_AVBL_PRICE_TBL ADD ( modifier         VARCHAR2(240 Byte),
                                                modifier_type    VARCHAR2 (240 Byte),   
											    gm_percent       NUMBER,
                                                part_description VARCHAR2(240 Byte)
											  );