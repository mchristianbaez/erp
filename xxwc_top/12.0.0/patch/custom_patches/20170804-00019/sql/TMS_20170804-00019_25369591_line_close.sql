/*************************************************************************
  $Header TMS_20170804-00019_25369591_line_close.sql $
  Module Name: TMS_20170804-00019  


  REVISIONS:
  Ver        Date         Author                Description
  ---------  -----------  ------------------    --------------------------
  1.0        29-AUG-2017  Pattabhi Avula         TMS#20170804-00019 

**************************************************************************/ 
SET SERVEROUTPUT ON SIZE 1000000
DECLARE
BEGIN
   DBMS_OUTPUT.put_line ('TMS: 20170804-00019, Before Update');

UPDATE apps.oe_order_lines_all
   SET flow_status_code='CLOSED',
       open_flag='N'
 WHERE line_id=102235740
   AND HEADER_ID =62017481;

   DBMS_OUTPUT.put_line (
         'TMS: 20170804-00019  Sales order lines updated (Expected:1): '
      || SQL%ROWCOUNT);

   COMMIT;

   DBMS_OUTPUT.put_line ('TMS: 20170804-00019    , End Update');
EXCEPTION
   WHEN OTHERS
   THEN
      ROLLBACK;
      DBMS_OUTPUT.put_line ('TMS: 20170804-00019 , Errors : ' || SQLERRM);
END;
/