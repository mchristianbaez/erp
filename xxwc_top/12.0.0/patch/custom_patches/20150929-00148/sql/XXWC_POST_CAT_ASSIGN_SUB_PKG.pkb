/* Formatted on 2014/08/08 13:25 (Formatter Plus v4.8.8) */
CREATE OR REPLACE PACKAGE BODY apps.xxwc_post_cat_assign_sub_pkg
AS
   /**************************************************************************
    File Name:XXWC_POST_CAT_ASSIGN_SUB_PKG
    PROGRAM TYPE: PL/SQL Package spec and body
    PURPOSE:
    HISTORY
    -- Description   : Called from the business event
    --                 oracle.apps.ego.item.postCatalogAssignmentChange
    --
    -- Dependencies Tables        :
    -- Dependencies Views         : None
    -- Dependencies Sequences     :
    -- Dependencies Procedures    :
    -- Dependencies Functions     : None
    -- Dependencies Packages      : None
    -- Dependencies Types         : None
    -- Dependencies Database Links: None
    ================================================================
           Last Update Date : 01/08/2014
    ================================================================
    ================================================================
    VERSION DATE          AUTHOR(S)       DESCRIPTION
    ------- -----------   --------------- ---------------------------------
    1.0     08-Jan-2014   Praveen Pawar    Initial creation of the package
    2.0     18-Mar-2014   Praveen Pawar    Modified to handle oracle.apps.ego.batch.postbatchprocess
                                           Business Event
                                           TMS# 20140319-00190
    2.1     25-Jul-2014   Praveen Pawar    Modifying PROCESS_EVENT function to handle processing of
                                           Inventory Category catalog and added private function process_cogs_sales
                                           and error handling TMS # 20140507-00218
	2.2     06-Oct-2015   Pattabhi Avula   TMS#20150929-00148 -- Error Alerts for categories missing Sales and COGS
	                                       exception skipped with count values count
   **************************************************************************/

   ---------------------------------------------------------------
-- Added private function process_cogs_sales w.r.t. ver 2.1 --
---------------------------------------------------------------
/**************************************************************************
*
* FUNCTION
*  process_cogs_sales
*
* DESCRIPTION
*  This function will sync the cogs and sales account based on new inventory catalog category value
*
* PARAMETERS
* ==========
* NAME                          TYPE     DESCRIPTION
* -----------------             -------- ---------------------------------------------
* p_inventory_item_id   IN       NUMBER    inventory_item_id
* p_organization_id     IN       NUMBER    organization_id
* p_new_cogs_segment    IN       VARCHAR2  COGS
* p_new_sales_segment   IN       VARCHAR2  New Sales Segment
* p_item_number         OUT      VARCHAR2  Item Number
* p_org_code            OUT      VARCHAR2  Org Code
* p_old_cogs_gl_code    OUT      VARCHAR2  Old COGS GL code
* p_new_cogs_gl_code    OUT      VARCHAR2  New COGS GL Code
* p_old_sales_gl_code   OUT      VARCHAR2  Old Sales GL COde
* p_new_sales_gl_code   OUT      VARCHAR2  New Sales GL Code
RETURN
BOOLEAN
 ================================================================
    VERSION DATE          AUTHOR(S)       DESCRIPTION
    ------- -----------   --------------- ---------------------------------
    2.1     25-Jul-2014   Praveen Pawar    Modifying PROCESS_EVENT function to handle processing of
                                           Inventory Category catalog and error handling TMS # 20140507-00218
*************************************************************************/
   FUNCTION process_cogs_sales (
      p_inventory_item_id   IN       NUMBER,
      p_organization_id     IN       NUMBER,
      p_new_cogs_segment    IN       VARCHAR2,
      p_new_sales_segment   IN       VARCHAR2,
      p_item_number         OUT      VARCHAR2,
      p_org_code            OUT      VARCHAR2,
      p_old_cogs_gl_code    OUT      VARCHAR2,
      p_new_cogs_gl_code    OUT      VARCHAR2,
      p_old_sales_gl_code   OUT      VARCHAR2,
      p_new_sales_gl_code   OUT      VARCHAR2
   )
      RETURN BOOLEAN
   IS
      PRAGMA AUTONOMOUS_TRANSACTION;
      cv_dist_list            VARCHAR2 (100)
                                        := 'HDSOracleDevelopers@hdsupply.com';
      l_missing_gl_dl         VARCHAR2 (400)
                         := apps.fnd_profile.VALUE ('XXWC_EGO_MISSING_GL_DL'); --Added functional DL as per Manny's comment on 19-Aug-14
      cv_inventory_category   mtl_category_sets_tl.category_set_name%TYPE
                                                      := 'Inventory Category';
      l_num_msg_cnt           NUMBER                                  := NULL;
      lv_new_cogs_ccid        NUMBER                                  := NULL;
      lv_new_sales_ccid       NUMBER                                  := NULL;
      lv_new_cogs             VARCHAR2 (181);
      lv_new_sales            VARCHAR2 (181);
      lv_error_message        VARCHAR2 (4000);
      l_chr_return_status     VARCHAR2 (100)                          := NULL;
      x_message_list          error_handler.error_tbl_type;
      x_tbl_item_table        ego_item_pub.item_tbl_type;
      l_tbl_item_table        ego_item_pub.item_tbl_type;
       l_calling               VARCHAR2 (4000)                         := NULL;

      CURSOR cur_item_account_details (
         p_inventory_item_id   IN   NUMBER,
         p_new_cogs_segment    IN   VARCHAR2,
         p_new_sales_segment   IN   VARCHAR2
      )
      IS
         SELECT item_number, organization_id, organization_code,
                cogs_account_id, existing_cogs, cogs_segment,
                sales_account_id, existing_sales, sales_segment, created_by
           FROM (SELECT msi.segment1 item_number, msi.organization_id,
                        mtp.organization_code,
                        msi.cost_of_sales_account cogs_account_id,
                        gck1.concatenated_segments existing_cogs,
                        gcc1.segment4 cogs_segment,
                        msi.sales_account sales_account_id,
                        gck2.concatenated_segments existing_sales,
                        gcc2.segment4 sales_segment, msi.created_by
                   FROM mtl_system_items_b msi,
                        gl_code_combinations gcc1,
                        gl_code_combinations gcc2,
                        gl_code_combinations_kfv gck1,
                        gl_code_combinations_kfv gck2,
                        mtl_parameters mtp
                  WHERE msi.inventory_item_id = p_inventory_item_id
                    AND msi.organization_id = p_organization_id
                    AND msi.organization_id = mtp.organization_id
                    AND gck1.code_combination_id = gcc1.code_combination_id
                    AND gck2.code_combination_id = gcc2.code_combination_id
                    AND (   gcc1.segment4 <>
                                      NVL (p_new_cogs_segment, gcc1.segment4)
                         OR gcc2.segment4 <>
                                     NVL (p_new_sales_segment, gcc2.segment4)
                        )
                    AND gcc1.code_combination_id = msi.cost_of_sales_account
                    AND gcc2.code_combination_id = msi.sales_account);
   BEGIN
      FOR rec_item_account_details IN
         cur_item_account_details (p_inventory_item_id,
                                   p_new_cogs_segment,
                                   p_new_sales_segment
                                  )
      LOOP
         IF p_new_cogs_segment IS NOT NULL
         THEN
            lv_new_cogs := NULL;
            lv_new_cogs_ccid := NULL;

            SELECT    segment1
                   || '.'
                   || segment2
                   || '.'
                   || segment3
                   || '.'
                   || p_new_cogs_segment
                   || '.'
                   || segment5
                   || '.'
                   || segment6
                   || '.'
                   || segment7 new_cogs_concat_segments
              INTO lv_new_cogs
              FROM gl_code_combinations_kfv
             WHERE code_combination_id =
                                      rec_item_account_details.cogs_account_id;

            BEGIN
               SELECT code_combination_id
                 INTO lv_new_cogs_ccid
                 FROM gl_code_combinations_kfv
                WHERE concatenated_segments = lv_new_cogs;
            EXCEPTION
               WHEN NO_DATA_FOUND
               THEN
                  p_item_number := rec_item_account_details.item_number;
                  p_org_code := rec_item_account_details.organization_code;
                  p_old_cogs_gl_code :=
                                       rec_item_account_details.existing_cogs;
                  p_new_cogs_gl_code := lv_new_cogs;
            END;
         END IF;

         IF p_new_sales_segment IS NOT NULL
         THEN
            lv_new_sales := NULL;
            lv_new_sales_ccid := NULL;

            SELECT    segment1
                   || '.'
                   || segment2
                   || '.'
                   || segment3
                   || '.'
                   || p_new_sales_segment
                   || '.'
                   || segment5
                   || '.'
                   || segment6
                   || '.'
                   || segment7 new_cogs_concat_segments
              INTO lv_new_sales
              FROM gl_code_combinations_kfv
             WHERE code_combination_id =
                                     rec_item_account_details.sales_account_id;

            BEGIN
               SELECT code_combination_id
                 INTO lv_new_sales_ccid
                 FROM gl_code_combinations_kfv
                WHERE concatenated_segments = lv_new_sales;
            EXCEPTION
               WHEN NO_DATA_FOUND
               THEN
                  p_item_number := rec_item_account_details.item_number;
                  p_org_code := rec_item_account_details.organization_code;
                  p_old_sales_gl_code :=
                                      rec_item_account_details.existing_sales;
                  p_new_sales_gl_code := lv_new_sales;
            END;
         END IF;

         IF lv_new_cogs_ccid IS NOT NULL OR lv_new_sales_ccid IS NOT NULL
         THEN
            fnd_global.apps_initialize (rec_item_account_details.created_by,
                                        fnd_global.resp_id,
                                        fnd_global.resp_appl_id
                                       );
            x_message_list.DELETE;
            error_handler.initialize;
            l_tbl_item_table (1).transaction_type := 'UPDATE';
            l_tbl_item_table (1).inventory_item_id := p_inventory_item_id;
            l_tbl_item_table (1).organization_id :=
                                      rec_item_account_details.organization_id;

            IF lv_new_cogs_ccid IS NOT NULL
            THEN
               l_tbl_item_table (1).cost_of_sales_account := lv_new_cogs_ccid;
            END IF;

            IF lv_new_sales_ccid IS NOT NULL
            THEN
               l_tbl_item_table (1).sales_account := lv_new_sales_ccid;
            END IF;

            ego_item_pub.process_items
                                      (p_api_version        => 1.0,
                                       p_init_msg_list      => fnd_api.g_true,
                                       p_commit             => fnd_api.g_true,
                                       p_item_tbl           => l_tbl_item_table,
                                       x_item_tbl           => x_tbl_item_table,
                                       x_return_status      => l_chr_return_status,
                                       x_msg_count          => l_num_msg_cnt
                                      );
            COMMIT;

            IF (l_chr_return_status <> fnd_api.g_ret_sts_success)
            THEN
               error_handler.get_message_list
                                            (x_message_list      => x_message_list);
               lv_error_message :=
                     'Error while updating COGS and Sales account for Item Id: '
                  || p_inventory_item_id
                  || ' and organization Id: '
                  || rec_item_account_details.organization_id
                  || '. Error Message: ';
                  l_calling := 'Updating Item COGS and Sales account information using EGO_ITEM_PUB.PROCESS_ITEMS API...';

               FOR i IN 1 .. x_message_list.COUNT
               LOOP
                  lv_error_message :=
                        lv_error_message
                     || i
                     || ')'
                     || x_message_list (i).MESSAGE_TEXT
                     || ' ';
               END LOOP;

               xxcus_error_pkg.xxcus_error_main_api
                  (p_called_from            => 'XXWC_POST_CAT_ASSIGN_SUB_PKG.PROCESS_EVENT',
                   p_calling                => l_calling,
                   p_request_id             => NULL,
                   p_ora_error_msg          => lv_error_message,
                   p_error_desc             => 'Standard API Error',
                   p_distribution_list      => l_missing_gl_dl,
                   p_module                 => 'XXWC'
                  );
                  -- changed the distribution list to Functional list based on Manny's review comment and after discussion with Perry, Manjulla on 19-Aug-14
              -- COMMIT;  --commented after code review with Manjulla on 12-Aug-14
            END IF;    --IF (l_chr_return_status <> FND_API.G_RET_STS_SUCCESS)
         END IF;
      --IF lv_new_cogs_ccid  IS NOT NULL OR lv_new_sales_ccid IS NOT NULL
      END LOOP;

      IF p_new_cogs_gl_code IS NOT NULL OR p_new_sales_gl_code IS NOT NULL
      THEN
         RETURN FALSE;
      ELSE
         RETURN TRUE;
      END IF;
   EXCEPTION
      WHEN OTHERS
      THEN
         lv_error_message := 'Error Message: ' || SQLERRM;
         l_calling := 'Processing COGS and SALES GL Codes...';
         ROLLBACK;
         xxcus_error_pkg.xxcus_error_main_api
            (p_called_from            => 'XXWC_POST_CAT_ASSIGN_SUB_PKG.PROCESS_COGS_SALES',
             p_calling                => l_calling,
             p_request_id             => NULL,
             p_ora_error_msg          => lv_error_message,
             p_error_desc             => 'User defined exception',
             p_distribution_list      => cv_dist_list,
             p_module                 => 'XXWC'
            );
       -- COMMIT;  --commented after code review with Manjulla on 12-Aug-14
         RETURN NULL;
   END process_cogs_sales;
------------------------------------------
-- End of additonal code w.r.t. ver 2.1 --
------------------------------------------

/**************************************************************************
*
* FUNCTION
*  process_event
*
* DESCRIPTION
*  This function will get executed when catelog category assignment or ego post batch process Business event is triggered
*
* PARAMETERS
* ==========
* NAME                          TYPE     DESCRIPTION
* -----------------             -------- ---------------------------------------------
* p_subscription_guid   IN          RAW           Subscription GUI ID
* p_event               IN OUT      wf_event_t    Event details
RETURN
VARCHAR2
 ================================================================
    VERSION DATE          AUTHOR(S)       DESCRIPTION
    ------- -----------   --------------- ---------------------------------
    1.0     08-Jan-2014   Praveen Pawar    Initial creation of the package
	2.2     06-Oct-2015   Pattabhi Avula   TMS#20150929-00148 -- Error Alerts 
	                                       for categories missing Sales and COGS
	                                       exception skipped with count values count
*************************************************************************/

   FUNCTION process_event (
      p_subscription_guid   IN              RAW,
      p_event               IN OUT NOCOPY   wf_event_t
   )
      RETURN VARCHAR2
   IS
      l_param_name            VARCHAR2 (240);
      l_param_value           VARCHAR2 (2000);
      l_event_name            VARCHAR2 (2000);
      l_err_text              VARCHAR2 (3000);
      l_param_list            wf_parameter_list_t;
      l_org_id                NUMBER;
      l_item_id               NUMBER;
      l_catalog_id            NUMBER;
      ln_request_id           NUMBER;
      ln_master_org           NUMBER;
      l_category_id           NUMBER;
      ln_category_set_id      NUMBER;
      ln_cat_intf_rec_cnt     NUMBER;
--------------------------
-- Added w.r.t. ver 2.1 --
--------------------------
      ln_inv_cat_set_id       NUMBER;
      ln_website_cat_set_id   NUMBER;
      ln_inv_cat_rec_cnt      NUMBER;
      lv_result               BOOLEAN;
      lb_send_email           BOOLEAN;
      lv_notif_hdr            VARCHAR2 (2000);
      lv_subject_line         VARCHAR2 (2000);
      l_del                   VARCHAR2 (1)                             := '|';
      l_file                  UTL_FILE.file_type;
      l_string                VARCHAR2 (3000)                         := NULL;
      l_file_name             VARCHAR2 (200)                          := NULL;
      l_instance_name         VARCHAR2 (200)                          := NULL;
      l_missing_gl_dl         VARCHAR2 (400)
                         := apps.fnd_profile.VALUE ('XXWC_EGO_MISSING_GL_DL');
      max_line_length         BINARY_INTEGER                         := 32767;
      l_result                VARCHAR2 (2000)                         := NULL;
      l_result_msg            VARCHAR2 (4000)                         := NULL;
      lv_host                 VARCHAR2 (256)   := 'mailoutrelay.hdsupply.net';
      lv_hostport             VARCHAR2 (20)                           := '25';
      lv_old_cogs_gl_code     gl_code_combinations_kfv.concatenated_segments%TYPE;
      lv_new_cogs_gl_code     gl_code_combinations_kfv.concatenated_segments%TYPE;
      lv_old_sales_gl_code    gl_code_combinations_kfv.concatenated_segments%TYPE;
      lv_new_sales_gl_code    gl_code_combinations_kfv.concatenated_segments%TYPE;
---------------------------------------------
-- End of code modification w.r.t. ver 2.1 --
---------------------------------------------
      lv_error_message        VARCHAR2 (4000);
      cv_dist_list            VARCHAR2 (100)
                                        := 'HDSOracleDevelopers@hdsupply.com';
      l_dml_type              VARCHAR2 (40);
      l_error                 VARCHAR2 (4000);
      lv_org_code             mtl_parameters.organization_code%TYPE   := NULL;
      lv_item_number          mtl_system_items_b.segment1%TYPE        := NULL;
      lv_category             mtl_categories_b.segment1%TYPE          := NULL;
      cv_website_item         mtl_category_sets_tl.category_set_name%TYPE
                                                            := 'Website Item';
      no_event_exception      EXCEPTION;
      cv_inventory_category   mtl_category_sets_tl.category_set_name%TYPE
                                                      := 'Inventory Category';
      -- Added w.r.t version 2.1 --
      l_num_msg_cnt           NUMBER                                  := NULL;
      lv_new_cogs_ccid        NUMBER                                  := NULL;
      lv_new_sales_ccid       NUMBER                                  := NULL;
      lv_new_cogs_segment     VARCHAR2 (25)                           := NULL;
      lv_new_sales_segment    VARCHAR2 (25)                           := NULL;
      l_chr_return_status     VARCHAR2 (100)                          := NULL;
      x_message_list          error_handler.error_tbl_type;
      x_tbl_item_table        ego_item_pub.item_tbl_type;
      l_tbl_item_table        ego_item_pub.item_tbl_type;
      l_calling               VARCHAR2 (4000)                         := NULL;
	  
	  -- Added w.r.t version 2.2 --
      l_count                 NUMBER                                  := NULL;
--------------------------------------------
-- Added w.r.t. ver 2.1 to declare cursor --
-- Cursor to fetch item and organizations --
-- from base table based on Item ID       --
--------------------------------------------
      CURSOR cur_item_details (pn_item_id IN NUMBER)
      IS
         SELECT   msi.inventory_item_id, msi.organization_id
             FROM mtl_system_items_b msi, mtl_parameters mtp
            WHERE msi.inventory_item_id = pn_item_id
              AND msi.organization_id = mtp.organization_id
         ORDER BY mtp.organization_code;

--------------------------------------------
-- Added w.r.t. ver 2.1 to declare cursor --
-- Cursor to fetch all the successfully   --
-- processed records for Inventory Category-
-- catalog from interface table           --
--------------------------------------------
      CURSOR cur_batch_item_details (
         pn_request_id   IN   NUMBER,
         pn_cat_set_id   IN   NUMBER,
         pn_org_id       IN   NUMBER
      )
      IS
         SELECT mic.inventory_item_id, mcb.attribute1 cogs_segment,
                mcb.attribute2 sales_segment
           FROM apps.mtl_item_categories_interface mic,
                apps.mtl_category_sets_b mcs,
                apps.mtl_categories_b mcb
          WHERE mic.request_id = pn_request_id
            AND mic.category_set_id = pn_cat_set_id
            AND mic.process_flag = 7
            AND mic.transaction_type IN ('CREATE', 'UPDATE')
            AND mic.organization_id = pn_org_id
            AND mcs.category_set_id = mic.category_set_id
            AND mcs.structure_id = mcb.structure_id
            AND mcb.category_id = mic.category_id;
   BEGIN
      IF (p_event IS NULL)
      THEN
         RAISE no_event_exception;
      END IF;

      l_event_name := p_event.geteventname ();
      l_param_list := p_event.getparameterlist;
      l_calling := 'Checking event name and fetching parameter value...';

      IF     l_param_list IS NOT NULL
         AND l_event_name = 'oracle.apps.ego.item.postCatalogAssignmentChange'
      THEN
         ln_master_org :=
                       TO_NUMBER (fnd_profile.VALUE ('XXWC_ITEM_MASTER_ORG'));

         FOR i IN l_param_list.FIRST .. l_param_list.LAST
         LOOP
            l_param_name := l_param_list (i).getname;
            l_param_value := l_param_list (i).getvalue;

            IF (l_param_name = 'DML_TYPE')
            THEN
               l_dml_type := l_param_value;
            ELSIF (l_param_name = 'INVENTORY_ITEM_ID')
            THEN
               l_item_id := l_param_value;
            ELSIF (l_param_name = 'ORGANIZATION_ID')
            THEN
               l_org_id := l_param_value;
            ELSIF (l_param_name = 'CATALOG_ID')
            THEN
               l_catalog_id := l_param_value;
            ELSIF (l_param_name = 'CATEGORY_ID')
            THEN
               l_category_id := l_param_value;
            END IF;
         END LOOP;

         COMMIT;

         IF ln_master_org = l_org_id
         THEN
            ln_category_set_id := NULL;
            l_calling := 'Fetching category set ID for Website item...';

            BEGIN
               SELECT category_set_id
                 INTO ln_category_set_id
                 FROM mtl_category_sets_tl
                WHERE category_set_name = cv_website_item;
            EXCEPTION
               WHEN OTHERS
               THEN
                  ln_category_set_id := NULL;
            END;

            IF ln_category_set_id = l_catalog_id
            THEN
               l_calling := 'Fetching category value...';

               BEGIN
                  SELECT mcb.segment1
                    INTO lv_category
                    FROM mtl_category_sets_b mcs, mtl_categories_b mcb
                   WHERE mcs.category_set_id = l_catalog_id
                     AND mcs.structure_id = mcb.structure_id
                     AND mcb.category_id = l_category_id;
               EXCEPTION
                  WHEN OTHERS
                  THEN
                     lv_category := NULL;
               END;

               IF    (l_dml_type = 'UPDATE' AND lv_category = 'N')
                  OR (l_dml_type = 'DELETE')
               THEN
                  l_calling :=
                              'Fetching item number and organization code...';

                  SELECT msi.segment1, mtp.organization_code
                    INTO lv_item_number, lv_org_code
                    FROM mtl_system_items_b msi, mtl_parameters mtp
                   WHERE msi.organization_id = mtp.organization_id
                     AND msi.inventory_item_id = l_item_id
                     AND mtp.organization_id = l_org_id;

                  INSERT INTO xxwc.xxwc_ego_catalog_category_log
                       VALUES (lv_item_number, lv_org_code, l_item_id,
                               l_org_id, cv_website_item, l_catalog_id,
                               lv_category, l_category_id, l_dml_type,
                               fnd_global.user_id, SYSDATE);

                  COMMIT;
               END IF;
-------------------------------------------------------------------------
-- Added w.r.t ver 2.1 to handle processing Inventory Category catalog --
-------------------------------------------------------------------------
            ELSE
               l_calling := 'Fetching instance name...';

               BEGIN
                  SELECT UPPER (instance_name)
                    INTO l_instance_name
                    FROM v$instance;
               EXCEPTION
                  WHEN OTHERS
                  THEN
                     NULL;
               END;

               lv_subject_line :=
                     'Error: Missing SALES or COGS GL Code - See Attached for details. Instance - '
                  || l_instance_name;
               lv_notif_hdr :=
                  'The Inventory Category classification (cat class) assignment for an Item has changed in the MST organization, but a corresponding GL account does not exist at one or more other organizations.  Please contact the finance department to create the missing GL account for the organization(s) listed in the attachment.';
               l_file_name :=
                     'Missing_GL_Code_'
                  || TO_CHAR (SYSDATE, 'DD-MON-YY HH.MI AM')
                  || '.csv';
               l_file :=
                  UTL_FILE.fopen ('XXWC_PDH_MISSING_GL_CODE',
                                  l_file_name,
                                  'W',
                                  max_line_length
                                 );
               l_string :=
                  'Item Number|Organization Code|Existing COGS GL Code Combination|New COGS GL Code Combination|Existing SALES GL Code Combination|New SALES GL Code Combination';
               UTL_FILE.put_line (l_file, l_string);
               ln_category_set_id := NULL;
               l_calling :=
                          'Fetching category set ID for Inventory category...';

               BEGIN
                  SELECT category_set_id
                    INTO ln_category_set_id
                    FROM apps.mtl_category_sets_tl
                   WHERE category_set_name = cv_inventory_category;
               EXCEPTION
                  WHEN OTHERS
                  THEN
                     ln_category_set_id := NULL;
               END;

               IF ln_category_set_id = l_catalog_id AND l_dml_type <> 'DELETE'
               THEN
                  lv_new_cogs_segment := NULL;
                  lv_new_sales_segment := NULL;
                  l_calling := 'Fetching COGS and Sales segment...';

                  BEGIN
                  l_calling := 'Fetching COGS and Sales account segment values for Inventory Category catalog. Category Id: '
                                                        || l_category_id;
                     SELECT mcb.attribute1, mcb.attribute2
                       INTO lv_new_cogs_segment, lv_new_sales_segment
                       FROM apps.mtl_category_sets_b mcs,
                            apps.mtl_categories_b mcb
                      WHERE mcs.category_set_id = l_catalog_id
                        AND mcs.structure_id = mcb.structure_id
                        AND mcb.category_id = l_category_id;
                  EXCEPTION
                     WHEN OTHERS
                     THEN
                        lv_error_message := 'Error Message: ' || SQLERRM;
                        ROLLBACK;
                        xxcus_error_pkg.xxcus_error_main_api
                           (p_called_from            => 'XXWC_POST_CAT_ASSIGN_SUB_PKG.PROCESS_EVENT',
                            p_calling                => l_calling,
                            p_request_id             => NULL,
                            p_ora_error_msg          => lv_error_message,
                            p_error_desc             => 'User defined exception',
                            p_distribution_list      => l_missing_gl_dl,
                            p_module                 => 'XXWC'
                           );
                            -- changed the distribution list to Functional list based on Manny's review comment and after discussion with Perry, Manjulla on 19-Aug-14
                     --   COMMIT; --commented after review with Manjulla on 12-Aug-14
                        RETURN l_error;
                  END;

                  IF    lv_new_cogs_segment IS NOT NULL
                     OR lv_new_sales_segment IS NOT NULL
                  THEN
                     FOR rec_item_details IN cur_item_details (l_item_id)
                     LOOP
                        lv_result := NULL;
                        lv_org_code := NULL;
                        lv_item_number := NULL;
                        lv_old_cogs_gl_code := NULL;
                        lv_new_cogs_gl_code := NULL;
                        lv_old_sales_gl_code := NULL;
                        lv_new_sales_gl_code := NULL;
                        lv_result :=
                           process_cogs_sales
                                         (rec_item_details.inventory_item_id,
                                          rec_item_details.organization_id,
                                          lv_new_cogs_segment,
                                          lv_new_sales_segment,
                                          lv_item_number,
                                          lv_org_code,
                                          lv_old_cogs_gl_code,
                                          lv_new_cogs_gl_code,
                                          lv_old_sales_gl_code,
                                          lv_new_sales_gl_code
                                         );

                        IF NOT lv_result
                        THEN
                           lb_send_email := TRUE;

                           IF     lv_old_cogs_gl_code IS NOT NULL
                              AND lv_old_sales_gl_code IS NOT NULL
                           THEN
                              l_string :=
                                    lv_item_number
                                 || l_del
                                 || lv_org_code
                                 || l_del
                                 || lv_old_cogs_gl_code
                                 || l_del
                                 || lv_new_cogs_gl_code
                                 || l_del
                                 || lv_old_sales_gl_code
                                 || l_del
                                 || lv_new_sales_gl_code;
                           ELSIF     lv_old_cogs_gl_code IS NOT NULL
                                 AND lv_old_sales_gl_code IS NULL
                           THEN
                              l_string :=
                                    lv_item_number
                                 || l_del
                                 || lv_org_code
                                 || l_del
                                 || lv_old_cogs_gl_code
                                 || l_del
                                 || lv_new_cogs_gl_code
                                 || l_del
                                 || ''
                                 || l_del
                                 || '';
                           ELSIF     lv_old_cogs_gl_code IS NULL
                                 AND lv_old_sales_gl_code IS NOT NULL
                           THEN
                              l_string :=
                                    lv_item_number
                                 || l_del
                                 || lv_org_code
                                 || l_del
                                 || ''
                                 || l_del
                                 || ''
                                 || l_del
                                 || lv_old_sales_gl_code
                                 || l_del
                                 || lv_new_sales_gl_code;
                           END IF;

                           UTL_FILE.put_line (l_file, l_string);
                        END IF;
                     END LOOP;
                  ELSE
				   --<Starts> Added for Ver#2.2 on 06-OCT-2015 for TMS#20150929-00148
				    BEGIN
					  SELECT COUNT(*)
                      INTO   l_count
                      FROM   fnd_flex_value_sets ffvs,
                             fnd_flex_values_vl ffvl
                      WHERE  ffvs.flex_value_set_id=ffvl.flex_value_set_id
                      AND    ffvs.flex_value_set_name='XXWC_INVENTORY_CATEGORY_VS'
                      AND    ffvl.enabled_flag        ='Y'
                      AND    ffvl.flex_value          =l_category_id;
					EXCEPTION
					  WHEN others THEN
					  l_count:=0;
					END;
					
					IF l_count=0 THEN
				   
                     xxcus_error_pkg.xxcus_error_main_api
                        (p_called_from            => 'XXWC_POST_CAT_ASSIGN_SUB_PKG.PROCESS_EVENT',
                         p_calling                =>    l_calling,
                         p_request_id             => NULL,
                         p_ora_error_msg          =>    'COGS and Sales DFFs are not configured for Inventory Category ID: '
                                                     || l_category_id,
                         p_error_desc             => 'User defined exception',
                         p_distribution_list      => l_missing_gl_dl,
                         p_module                 => 'XXWC'
                        );
				    END IF;	
                     --<Ends> Added for Ver#2.2 on 06-OCT-2015 for TMS#20150929-00148					
                         -- changed the distribution list to Functional list based on Manny's review comment and after discussion with Perry, Manjulla on 19-Aug-14
                     COMMIT;
                  END IF;

                  --lv_new_cogs_segment  IS NOT NULL OR lv_new_sales_segment IS NOT NULL
                  IF lb_send_email
                  THEN
                     UTL_FILE.fclose (l_file);
                     xxcus_misc_pkg.send_email_attachment
                                        (p_sender           => 'no-reply@whitecap.net',
                                         p_recipients       => l_missing_gl_dl,
                                         p_subject          => lv_subject_line,
                                         p_message          => lv_notif_hdr,
                                         p_attachments      => l_file_name,
                                         x_result           => l_result,
                                         x_result_msg       => l_result_msg
                                        );
                  END IF;

                  COMMIT;
               END IF;                  --IF ln_category_set_id = l_catalog_id
            END IF;
         END IF;
----------------------------------------------------------------------------------
-- Added as per version # 2.0 on 18-Mar-2014                                    --
-- Capture oracle.apps.ego.batch.postbatchprocess business event                --
-- And check if it got triggered for processing Website Item Catalog Categories --
----------------------------------------------------------------------------------
      ELSIF     l_param_list IS NOT NULL
            AND l_event_name = 'oracle.apps.ego.batch.postbatchprocess'
      THEN
         ln_master_org :=
                       TO_NUMBER (fnd_profile.VALUE ('XXWC_ITEM_MASTER_ORG'));

         FOR i IN l_param_list.FIRST .. l_param_list.LAST
         LOOP
            l_param_name := l_param_list (i).getname;
            l_param_value := l_param_list (i).getvalue;

            IF (l_param_name = 'REQUEST_ID')
            THEN
               ln_request_id := l_param_value;
            END IF;
         END LOOP;

--------------------------
-- Added w.r.t. ver 2.1 --
--------------------------
         l_calling := 'Fetching category set ID for website catalog...';

         BEGIN
            SELECT category_set_id
              INTO ln_website_cat_set_id
              FROM mtl_category_sets_tl
             WHERE category_set_name = cv_website_item;
         EXCEPTION
            WHEN OTHERS
            THEN
               ln_category_set_id := NULL;
         END;

---------------------------------------------
-- End of code modification w.r.t. ver 2.1 --
---------------------------------------------
         ln_cat_intf_rec_cnt := 0;

         SELECT COUNT (1)
           INTO ln_cat_intf_rec_cnt
           FROM mtl_item_categories_interface
          WHERE request_id = ln_request_id
            AND category_set_id = ln_website_cat_set_id;

         -- Added w.r.t. ver 2.1 --
         IF ln_cat_intf_rec_cnt > 0
         THEN
            BEGIN
               INSERT INTO xxwc.xxwc_ego_catalog_category_log
                  (SELECT item_number, organization_code, inventory_item_id,
                          organization_id, category_set_name,
                          category_set_id, category_name, category_id,
                          transaction_type, created_by, last_update_date
                     FROM inv.mtl_item_categories_interface
                    WHERE request_id = ln_request_id
                      AND process_flag = 7
                      AND category_set_name = cv_website_item
                      AND organization_id = ln_master_org
                      AND (       transaction_type = 'UPDATE'
                              AND category_name = 'N'
                           OR transaction_type = 'DELETE'
                          ));

               COMMIT;
            EXCEPTION
               WHEN OTHERS
               THEN
                  lv_error_message := 'Error Message: ' || SQLERRM;
                  l_calling := 'Capture SKUs which underwent change in Website Item Catalog Category value.';
                  ROLLBACK;
                  xxcus_error_pkg.xxcus_error_main_api
                     (p_called_from            => 'XXWC_POST_CAT_ASSIGN_SUB_PKG.PROCESS_EVENT',
                      p_calling                => l_calling,
                      p_request_id             => ln_request_id,
                      p_ora_error_msg          => lv_error_message,
                      p_error_desc             => 'User defined exception',
                      p_distribution_list      => cv_dist_list,
                      p_module                 => 'XXWC'
                     );
                   --   COMMIT; --commented after review with Manjulla on 12-Aug-14
            END;
         END IF;

         -- End of code modification w.r.t version # 2.0 --

         -------------------------
-- Added w.r.t ver 2.1 --
-------------------------
         BEGIN
            SELECT UPPER (instance_name)
              INTO l_instance_name
              FROM v$instance;
         EXCEPTION
            WHEN OTHERS
            THEN
               NULL;
         END;

         lv_subject_line :=
               'Error: Missing SALES or COGS GL Code - See Attached for details. Instance - '
            || l_instance_name;
         lv_notif_hdr :=
            'The Inventory Category classification (cat class) assignment for an Item has changed in the MST organization, but a corresponding GL account does not exist at one or more other organizations.  Please contact the finance department to create the missing GL account for the organization(s) listed in the attachment.';
         l_file_name :=
               'Missing_GL_Code_'
            || TO_CHAR (SYSDATE, 'DD-MON-YY HH.MI AM')
            || '.csv';
         l_file :=
            UTL_FILE.fopen ('XXWC_PDH_MISSING_GL_CODE',
                            l_file_name,
                            'W',
                            max_line_length
                           );
         l_string :=
            'Item Number|Organization Code|Existing COGS GL Code Combination|New COGS GL Code Combination|Existing SALES GL Code Combination|New SALES GL Code Combination';
         UTL_FILE.put_line (l_file, l_string);
         ln_inv_cat_set_id := NULL;

         BEGIN
            SELECT category_set_id
              INTO ln_inv_cat_set_id
              FROM apps.mtl_category_sets_tl
             WHERE category_set_name = cv_inventory_category;
         EXCEPTION
            WHEN OTHERS
            THEN
               ln_category_set_id := NULL;
         END;

         SELECT COUNT (1)
           INTO ln_inv_cat_rec_cnt
           FROM mtl_item_categories_interface
          WHERE request_id = ln_request_id
            AND category_set_id = ln_inv_cat_set_id;

         IF ln_inv_cat_rec_cnt > 0
         THEN
            FOR rec_batch_item_details IN
               cur_batch_item_details (ln_request_id,
                                       ln_inv_cat_set_id,
                                       ln_master_org
                                      )
            LOOP
               IF    rec_batch_item_details.cogs_segment IS NOT NULL
                  OR rec_batch_item_details.sales_segment IS NOT NULL
               THEN
                  FOR rec_item_details IN
                     cur_item_details
                                    (rec_batch_item_details.inventory_item_id)
                  LOOP
                     lv_result := NULL;
                     lv_org_code := NULL;
                     lv_item_number := NULL;
                     lv_old_cogs_gl_code := NULL;
                     lv_new_cogs_gl_code := NULL;
                     lv_old_sales_gl_code := NULL;
                     lv_new_sales_gl_code := NULL;
                     lv_result :=
                        process_cogs_sales
                                       (rec_item_details.inventory_item_id,
                                        rec_item_details.organization_id,
                                        rec_batch_item_details.cogs_segment,
                                        rec_batch_item_details.sales_segment,
                                        lv_item_number,
                                        lv_org_code,
                                        lv_old_cogs_gl_code,
                                        lv_new_cogs_gl_code,
                                        lv_old_sales_gl_code,
                                        lv_new_sales_gl_code
                                       );

                     IF NOT lv_result
                     THEN
                        lb_send_email := TRUE;

                        IF     lv_old_cogs_gl_code IS NOT NULL
                           AND lv_old_sales_gl_code IS NOT NULL
                        THEN
                           l_string :=
                                 lv_item_number
                              || l_del
                              || lv_org_code
                              || l_del
                              || lv_old_cogs_gl_code
                              || l_del
                              || lv_new_cogs_gl_code
                              || l_del
                              || lv_old_sales_gl_code
                              || l_del
                              || lv_new_sales_gl_code;
                        ELSIF     lv_old_cogs_gl_code IS NOT NULL
                              AND lv_old_sales_gl_code IS NULL
                        THEN
                           l_string :=
                                 lv_item_number
                              || l_del
                              || lv_org_code
                              || l_del
                              || lv_old_cogs_gl_code
                              || l_del
                              || lv_new_cogs_gl_code
                              || l_del
                              || ''
                              || l_del
                              || '';
                        ELSIF     lv_old_cogs_gl_code IS NULL
                              AND lv_old_sales_gl_code IS NOT NULL
                        THEN
                           l_string :=
                                 lv_item_number
                              || l_del
                              || lv_org_code
                              || l_del
                              || ''
                              || l_del
                              || ''
                              || l_del
                              || lv_old_sales_gl_code
                              || l_del
                              || lv_new_sales_gl_code;
                        END IF;

                        UTL_FILE.put_line (l_file, l_string);
                     END IF;
                  END LOOP;
               END IF;
            END LOOP;

            IF lb_send_email
            THEN
               UTL_FILE.fclose (l_file);
               xxcus_misc_pkg.send_email_attachment
                                        (p_sender           => 'no-reply@whitecap.net',
                                         p_recipients       => l_missing_gl_dl,
                                         p_subject          => lv_subject_line,
                                         p_message          => lv_notif_hdr,
                                         p_attachments      => l_file_name,
                                         x_result           => l_result,
                                         x_result_msg       => l_result_msg
                                        );
            END IF;

            COMMIT;
         END IF;
      END IF;

      RETURN 'SUCCESS';
   EXCEPTION
      WHEN no_event_exception
      THEN
         RETURN 'ERROR: WF_EVENT_MSG IS NULL';
      WHEN OTHERS
      THEN
         l_error := SQLERRM;
         l_err_text := 'Error : ' || TO_CHAR (SQLCODE) || '---' || l_error;
         ROLLBACK;
         xxcus_error_pkg.xxcus_error_main_api
            (p_called_from            => 'XXWC_POST_CAT_ASSIGN_SUB_PKG.PROCESS_EVENT',
             --p_calling                => 'Main function...',
             p_calling                => l_calling,
             p_request_id             => NULL,
             --p_ora_error_msg          => l_err_text,
             p_ora_error_msg          => SUBSTR
                                            (   'Error_Stack...'
                                             || DBMS_UTILITY.format_error_stack
                                                                           ()
                                             || 'Error_Backtrace...'
                                             || DBMS_UTILITY.format_error_backtrace
                                                                           (),
                                             1,
                                             2000
                                            ),
             --p_error_desc             => 'User defined exception',
             p_error_desc             => SUBSTR (l_err_text, 1, 240),
             p_distribution_list      => cv_dist_list,
             p_module                 => 'XXWC'
            );
        -- COMMIT; --commented after review with Manjulla on 12-Aug-14
         RETURN l_error;
   END process_event;
END xxwc_post_cat_assign_sub_pkg;
/