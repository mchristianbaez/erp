/*************************************************************************
     Module Name: XXWC_CAT_CLASS_ITEMS_V.sql

     PURPOSE:   Create view	
					   
     REVISIONS:
     Ver        Date        Author                     Description
     ---------  ----------  ---------------         -------------------------
     1.0        01/24/2018  Niraj K Ranjan          TMS#220170201-00276   CSP Form Should be modified to accommodate Matrix modifiers--Matrix Maintenance            
**************************************************************************/
  /*************************************************************************
     Module Name: XXWC_CAT_CLASS_ITEMS_V.sql

     PURPOSE:   Create view	
					   
     REVISIONS:
     Ver        Date        Author                     Description
     ---------  ----------  ---------------         -------------------------
     1.0        01/24/2018  Niraj K Ranjan          TMS#220170201-00276   CSP Form Should be modified to accommodate Matrix modifiers--Matrix Maintenance            
**************************************************************************/
  CREATE OR REPLACE VIEW "APPS"."XXWC_CAT_CLASS_ITEMS_V" ("ORGANIZATION_ID","CATEGORY_ID","ITEM_ID","CAT_CLASS","SKU", "SKU_DESCRIPTION", "UOM", "PRICE", "DISCOUNT_PRICE", "MODIFIER_NAME", "GM_PERCENT") AS 
  SELECT msi.ORGANIZATION_ID,
         mc.CATEGORY_ID,
         msi.inventory_item_id ITEM_ID,
         mc.segment1 || '.' || mc.segment2 CAT_CLASS,
         msi.segment1 SKU,
         msi.description SKU_DESCRIPTION,
         msi.PRIMARY_UNIT_OF_MEASURE UOM,
         NULL PRICE,
         NULL DISCOUNT_PRICE,
         NULL MODIFIER_NAME,
         NULL GM_PERCENT
    FROM mtl_system_items_b  msi,
         mtl_item_categories mic,
         mtl_categories_b    mc
    WHERE mic.category_set_id = 1100000062
    AND msi.ORGANIZATION_ID = mic.ORGANIZATION_ID
    AND msi.INVENTORY_ITEM_ID = mic.INVENTORY_ITEM_ID
    AND mic.CATEGORY_ID = mc.CATEGORY_ID
    AND mc.enabled_flag='Y';
