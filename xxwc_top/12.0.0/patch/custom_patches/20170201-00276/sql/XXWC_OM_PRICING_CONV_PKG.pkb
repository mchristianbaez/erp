CREATE OR REPLACE PACKAGE BODY XXWC_OM_PRICING_CONV_PKG AS
  /****************************************************************************************************************
      $Header XXXWC_OM_PRICING_CONV_PKG $
      Module Name: XXWC_OM_PRICING_CONV_PKG.pks
  
      PURPOSE:    Convert existing modifiers to new pricing form
  
      REVISIONS:
      Ver        Date        Author                     Description
      ---------  ----------  ---------------    -------------------------------------------------------------------
      1.0        01/15/2018  Nancy Pahwa                 Initial Version TMS # 20170201-00276
  
  *****************************************************************************************************************/
  procedure pricing_conv(errbuf      OUT VARCHAR2,
                         retcode     OUT NUMBER,
                         p_from_date varchar2,
                         p_to_date   varchar2) as
    l_QUALIFIER_CONTEXT       varchar2(30);
    l_qualifier_attribute     varchar2(30);
    l_price_type              varchar2(30);
    l_agreement_id            number;
    l_agreement_line_id       number;
    l_partnumber              varchar2(40);
    l_description             varchar2(240);
    l_product_attr_val        varchar2(240);
    l_mod_value               number;
    l_mod_value1              number;
    l_QUALIFIER_ATTR_VALUE_TO varchar2(240);
    l_product_attr_value      varchar2(4000);
    l_product_attribute       varchar2(30);
    l_organization_id         number;
    v_from_date               date;
    v_to_date                 date;
    l_count                   number;
    l_agreement_id1           number;
    l_count1                  number;
  
  begin
    errbuf      := NULL;
    retcode     := '0';
    v_from_date := nvl(to_date(p_from_date, 'YYYY/MM/DD HH24:MI:SS'),
                       to_date(sysdate, 'YYYY/MM/DD HH24:MI:SS'));
    -- fnd_file.put_line(fnd_file.log,'v_from_date  '||to_char(v_from_date,'dd-mon-yyyy hh24:mi:ss'));
    v_to_date := nvl(to_date(p_to_date, 'YYYY/MM/DD HH24:MI:SS'),
                     to_date(sysdate, 'YYYY/MM/DD HH24:MI:SS'));
    --read base table header data
    for i in (select /*row_id,*/
               context,
               list_header_id,
               creation_date,
               start_date_active,
               end_date_active,
               automatic_flag,
               created_by,
               last_update_date,
               last_updated_by,
               last_update_login,
               name,
               description,
               version_no,
               comments,
               active_flag,
               orig_system_header_ref,
               global_flag,
               orig_org_id,
               view_flag,
               update_flag,
               rowidtochar(rowid) testrowid
                from apps.qp_list_headers_all
               where attribute10 = 'Segmented Price'
                 and active_flag = 'Y'
                    --  and list_header_id = 3698992
                 and creation_date between v_from_date and v_to_date
              /* and end_date_active is null*/
              ) loop
      begin
      
        -- get the new sequence id for custom table
        select XXWC_OM_CONTRACT_PRICING_HDR_S.nextval
          into l_agreement_id
          from dual;
        --  dbms_output.put_line('l_agreement_id:' || l_agreement_id);
        -- get additional required parameters to fill in custom header table
        -- here we need to check the price type 
        select QUALIFIER_CONTEXT,
               QUALIFIER_ATTRIBUTE,
               QUALIFIER_ATTR_VALUE_TO
          into l_QUALIFIER_CONTEXT,
               l_qualifier_attribute,
               l_QUALIFIER_ATTR_VALUE_TO
          from apps.qp_qualifiers_v
         where list_header_id = i.list_header_id;
        -- dbms_output.put_line('l_QUALIFIER_CONTEXT:' || l_QUALIFIER_CONTEXT);
        -- dbms_output.put_line('l_qualifier_attribute:' ||
        --                    l_qualifier_attribute);
      
        if l_QUALIFIER_CONTEXT = 'CUSTOMER' and
           l_qualifier_attribute = 'QUALIFIER_ATTRIBUTE32' then
          l_price_type := 'MASTER';
        end if;
        --dbms_output.put_line('l_price_type:' || l_price_type);
      
        if l_QUALIFIER_CONTEXT = 'CUSTOMER' and
           l_qualifier_attribute = 'QUALIFIER_ATTRIBUTE11' then
          l_price_type := 'SHIP TO';
        end if;
      exception
        when no_data_found then
          l_price_type := 'MASTER';
      end;
      ---check the data if it already exist or not at header level
      begin
        select count(1)
          into l_count
          from xxwc.XXWC_OM_CONTRACT_PRICING_HDR
         where customer_id = l_QUALIFIER_ATTR_VALUE_TO
           and price_type = l_price_type
           and agreement_type = 'MTX';
        -- if it doesn't exist then insert header and lines
        if l_count = 0 then
          -- now insert the header level information in custom header table
          insert into xxwc.XXWC_OM_CONTRACT_PRICING_HDR
            (agreement_id,
             price_type,
             customer_id,
             customer_site_id,
             agreement_status,
             revision_number,
             organization_id,
             gross_margin,
             incompatability_group,
             attribute_category,
             attribute1,
             attribute2,
             attribute3,
             attribute4,
             attribute5,
             attribute6,
             attribute7,
             attribute8,
             attribute9,
             attribute10,
             attribute11,
             attribute12,
             attribute13,
             attribute14,
             attribute15,
             creation_date,
             created_by,
             last_update_date,
             last_updated_by,
             last_update_login,
             agreement_type,
             conversion_status)
          values
            (l_agreement_id,
             l_price_type,
             l_QUALIFIER_ATTR_VALUE_TO,
             null,
             'APPROVED',
             0,
             630,
             null,
             'Best Price Wins',
             null,
             null,
             null,
             null,
             null,
             null,
             null,
             null,
             null,
             null,
             null,
             null,
             null,
             null,
             null,
             null,
             i.creation_date,
             i.created_by,
             i.last_update_date,
             i.last_updated_by,
             i.last_update_login,
             'MTX',
             'Y');
          -- insert into archive tables
          insert into XXWC.XXWC_OM_CSP_HEADER_ARCHIVE
            (agreement_id,
             price_type,
             customer_id,
             customer_site_id,
             agreement_status,
             revision_number,
             organization_id,
             gross_margin,
             incompatability_group,
             attribute_category,
             attribute1,
             attribute2,
             attribute3,
             attribute4,
             attribute5,
             attribute6,
             attribute7,
             attribute8,
             attribute9,
             attribute10,
             attribute11,
             attribute12,
             attribute13,
             attribute14,
             attribute15,
             creation_date,
             created_by,
             last_update_date,
             last_updated_by,
             last_update_login,
             agreement_type,
             conversion_status)
          values
            (l_agreement_id,
             l_price_type,
             l_QUALIFIER_ATTR_VALUE_TO,
             null,
             'APPROVED',
             0,
             630,
             null,
             'Best Price Wins',
             null,
             null,
             null,
             null,
             null,
             null,
             null,
             null,
             null,
             null,
             null,
             null,
             null,
             null,
             null,
             null,
             i.creation_date,
             i.created_by,
             i.last_update_date,
             i.last_updated_by,
             i.last_update_login,
             'MTX',
             'Y');
          /* dbms_output.put_line('i.rowid:' || i.testrowid);
          dbms_output.put_line('agreement id check:' || l_agreement_id);*/
          -- now update the base header table with the id to create a relationship
          update qp_list_headers_all
             set attribute14 = to_char(l_agreement_id), attribute15 = '0'
           where list_header_id = i.list_header_id;
          /*       dbms_output.put_line('no of records updated for rowid' || i.testrowid || ' ' || l_agreement_id || ' ' || sql%rowcount);*/
          update qp_list_headers_all
             set VERSION_NO = l_agreement_id || '.0'
           where list_header_id = i.list_header_id;
          /*        dbms_output.put_line('no of records updated for rowid' || i.testrowid || ' ' || l_agreement_id || ' ' || sql%rowcount);*/
        
          -- commit;
          -- dbms_output.put_line('update complete to qp_list_headers_all');
          -- Pull the line level information from base table
          for a in (select list_line_id,
                           creation_date,
                           created_by,
                           last_update_date,
                           last_updated_by,
                           last_update_login,
                           program_application_id,
                           program_id,
                           program_update_date,
                           request_id,
                           list_header_id,
                           list_line_type_code,
                           start_date_active,
                           end_date_active,
                           automatic_flag,
                           modifier_level_code,
                           price_by_formula_id,
                           list_price,
                           list_price_uom_code,
                           primary_uom_flag,
                           inventory_item_id,
                           organization_id,
                           related_item_id,
                           relationship_type_id,
                           substitution_context,
                           substitution_attribute,
                           substitution_value,
                           revision,
                           revision_date,
                           revision_reason_code,
                           price_break_type_code,
                           percent_price,
                           number_effective_periods,
                           effective_period_uom,
                           arithmetic_operator,
                           operand,
                           override_flag,
                           print_on_invoice_flag,
                           rebate_transaction_type_code,
                           base_qty,
                           base_uom_code,
                           accrual_qty,
                           accrual_uom_code,
                           estim_accrual_rate,
                           comments,
                           generate_using_formula_id,
                           reprice_flag,
                           list_line_no,
                           estim_gl_value,
                           benefit_price_list_line_id,
                           expiration_period_start_date,
                           number_expiration_periods,
                           expiration_period_uom,
                           expiration_date,
                           accrual_flag,
                           pricing_phase_id,
                           pricing_group_sequence,
                           incompatibility_grp_code,
                           product_precedence,
                           proration_type_code,
                           accrual_conversion_rate,
                           benefit_qty,
                           benefit_uom_code,
                           recurring_flag,
                           benefit_limit,
                           charge_type_code,
                           charge_subtype_code,
                           context,
                           attribute1,
                           attribute2,
                           attribute3,
                           attribute4,
                           attribute5,
                           attribute6,
                           attribute7,
                           attribute8,
                           attribute9,
                           attribute10,
                           attribute11,
                           attribute12,
                           attribute13,
                           attribute14,
                           attribute15,
                           include_on_returns_flag,
                           qualification_ind,
                           limit_exists_flag,
                           group_count,
                           net_amount_flag,
                           recurring_value,
                           accum_context,
                           accum_attribute,
                           accum_attr_run_src_flag,
                           customer_item_id,
                           break_uom_code,
                           break_uom_context,
                           break_uom_attribute,
                           pattern_id,
                           product_uom_code,
                           pricing_attribute_count,
                           hash_key,
                           cache_key,
                           orig_sys_line_ref,
                           orig_sys_header_ref,
                           continuous_price_break_flag,
                           eq_flag,
                           currency_header_id,
                           null_other_oprt_count,
                           pte_code,
                           source_system_code,
                           service_duration,
                           service_period,
                           rowidtochar(rowid) testrowid
                      from qp_list_lines
                     where list_header_id = i.list_header_id
                    /*and end_date_active is null*/
                    ) loop
            begin
              -- get the seqeunce to go in custom line level table
              select XXWC_OM_CONTRACT_PRICING_LIN_S.nextval
                into l_agreement_line_id
                from dual;
              --  dbms_output.put_line('l_agreement_line_id:' ||
              --      l_agreement_line_id);
              -- get additional paramters to fill in custom line level table
              select product_attr_val, OPERAND, product_attr_value
                into l_product_attr_val, l_mod_value, l_product_attr_value
                from qp_modifier_summary_v
               where list_line_id = a.list_line_id;
              -- dbms_output.put_line('a.list_line_id:' || a.list_line_id);
              -- dbms_output.put_line('l_product_attr_val:' || l_product_attr_val);
              -- dbms_output.put_line('l_mod_value:' || l_mod_value);
              -- dbms_output.put_line('l_product_attr_value:' ||
              --             l_product_attr_value);
            
              l_mod_value1 := 100 - (l_mod_value * 100);
              --  dbms_output.put_line('l_mod_value1:' || l_mod_value1);
            
              begin
                select b.segment1, b.description
                  into l_partnumber, l_description
                  from mtl_system_items_b b
                 where b.inventory_item_id = l_product_attr_val
                   and ORGANIZATION_ID = 222;
              
                --  dbms_output.put_line('l_partnumber:' || l_partnumber);
                -- dbms_output.put_line('l_description:' || l_description);
              
                select b.organization_id
                  into l_organization_id
                  from mtl_system_items_b b
                 where b.segment1 = l_partnumber
                   and ORGANIZATION_ID not in 222
                   and rownum = 1;
              
                -- dbms_output.put_line('l_organization_id:' || l_organization_id);
              
                l_product_attribute := 'ITEM';
              exception
                when no_data_found then
                  begin
                    -- it will come here when its not sku level but cat class level 
                    select distinct /*item.INVENTORY_ITEM_ID,
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                    item.ORGANIZATION_ID,item.SEGMENT1,
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                   item.DESCRIPTION,item_c.CATEGORY_SET_ID,
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                               item_c.CATEGORY_ID, 
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                 m_c.CATEGORY_ID,*/
                                    m_c.SEGMENT1,
                                    m_c.DESCRIPTION
                      into l_partnumber, l_description
                    /*, m_c_s.CATEGORY_SET_ID,m_c_s.STRUCTURE_ID,
                    m_c_s.CATEGORY_SET_NAME,m_c_s.DESCRIPTION */
                      from mtl_system_items_b  item,
                           mtl_item_categories item_c,
                           mtl_categories      m_c /*,mtl_category_sets m_c_s */
                     where item.ORGANIZATION_ID = item_c.ORGANIZATION_ID
                       and item.INVENTORY_ITEM_ID =
                           item_c.INVENTORY_ITEM_ID
                       and item_c.CATEGORY_ID = m_c.CATEGORY_ID
                       and item.ORGANIZATION_ID = 222
                          --and item_c.CATEGORY_SET_ID=m_c_s.CATEGORY_SET_ID 
                       and item_c.category_id = l_product_attr_val;
                  
                    -- dbms_output.put_line('l_partnumber1:' || l_partnumber);
                    -- dbms_output.put_line('l_description1:' || l_description);
                  
                    select item.ORGANIZATION_ID
                      into l_organization_id
                      from mtl_system_items_b  item,
                           mtl_item_categories item_c,
                           mtl_categories      m_c /*,mtl_category_sets m_c_s */
                     where item.ORGANIZATION_ID = item_c.ORGANIZATION_ID
                       and item.INVENTORY_ITEM_ID =
                           item_c.INVENTORY_ITEM_ID
                       and item_c.CATEGORY_ID = m_c.CATEGORY_ID
                       and item.ORGANIZATION_ID not in 222
                       and m_c.segment1 = l_partnumber
                       and rownum = 1;
                    -- dbms_output.put_line('l_organization_id1:' ||
                    --      l_organization_id);
                    -- dbms_output.put_line('l_product_attr_value1:' ||
                    --      l_product_attr_value);
                    if l_product_attr_value is not null then
                      l_partnumber := l_product_attr_value;
                    end if;
                    l_product_attribute := 'CAT_CLASS';
                  exception
                    when no_data_found then
                      -- this is to handle exceptions
                      l_partnumber        := l_product_attr_val;
                      l_product_attribute := 'CAT_CLASS';
                      l_description       := l_product_attr_val;
                  end;
              end;
            end;
            insert into xxwc.XXWC_OM_CONTRACT_PRICING_LINES
              (agreement_line_id,
               agreement_id,
               agreement_type,
               vendor_quote_number,
               product_attribute,
               product_value,
               product_description,
               list_price,
               start_date,
               end_date,
               modifier_type,
               modifier_value,
               selling_price,
               special_cost,
               vendor_id,
               max_quantity_terms,
               average_cost,
               gross_margin,
               line_status,
               interfaced_flag,
               revision_number,
               latest_rec_flag,
               attribute_category,
               attribute1,
               attribute2,
               attribute3,
               attribute4,
               attribute5,
               attribute6,
               attribute7,
               attribute8,
               attribute9,
               attribute10,
               attribute11,
               attribute12,
               attribute13,
               attribute14,
               attribute15,
               creation_date,
               created_by,
               last_update_date,
               last_updated_by,
               last_update_login,
               calc_selling_price,
               list_header_id,
               conversion_status,
               CATEGORY_ID)
            values
              (l_agreement_line_id,
               l_agreement_id,
               'MTX',
               null,
               l_product_attribute,
               l_partnumber,
               l_description,
               null,
               a.start_date_active,
               a.end_date_active,
               'PERCENT',
               l_mod_value1,
               null,
               null,
               null,
               null,
               null,
               null,
               'APPROVED',
               'Y',
               0,
               'Y',
               null,
               null,
               null,
               null,
               null,
               null,
               null,
               null,
               null,
               null,
               null,
               null,
               null,
               null,
               null,
               null,
               a.creation_date,
               a.created_by,
               a.last_update_date,
               a.last_updated_by,
               a.last_update_login,
               null,
               i.list_header_id,
               'Y',
               l_partnumber);
            --inserting data into archive tables
            insert into XXWC.XXWC_OM_CSP_LINES_ARCHIVE
              (agreement_line_id,
               agreement_id,
               agreement_type,
               vendor_quote_number,
               product_attribute,
               product_value,
               product_description,
               list_price,
               start_date,
               end_date,
               modifier_type,
               modifier_value,
               selling_price,
               special_cost,
               vendor_id,
               max_quantity_terms,
               average_cost,
               gross_margin,
               line_status,
               interfaced_flag,
               revision_number,
               latest_rec_flag,
               attribute_category,
               attribute1,
               attribute2,
               attribute3,
               attribute4,
               attribute5,
               attribute6,
               attribute7,
               attribute8,
               attribute9,
               attribute10,
               attribute11,
               attribute12,
               attribute13,
               attribute14,
               attribute15,
               creation_date,
               created_by,
               last_update_date,
               last_updated_by,
               last_update_login,
               calc_selling_price,
               list_header_id,
               conversion_status,
               CATEGORY_ID)
            values
              (l_agreement_line_id,
               l_agreement_id,
               'MTX',
               null,
               l_product_attribute,
               l_partnumber,
               l_description,
               null,
               a.start_date_active,
               a.end_date_active,
               'PERCENT',
               l_mod_value1,
               null,
               null,
               null,
               null,
               null,
               null,
               'APPROVED',
               'Y',
               0,
               'Y',
               null,
               null,
               null,
               null,
               null,
               null,
               null,
               null,
               null,
               null,
               null,
               null,
               null,
               null,
               null,
               null,
               a.creation_date,
               a.created_by,
               a.last_update_date,
               a.last_updated_by,
               a.last_update_login,
               null,
               i.list_header_id,
               'Y',
               l_partnumber);
            --dbms_output.put_line('l_agreement_line_id1:' ||
            --             l_agreement_line_id);
            update qp_list_lines
               set attribute2 = l_agreement_line_id
             where rowidtochar(rowid) = a.testrowid;
            update xxwc.XXWC_OM_CONTRACT_PRICING_HDR
               set organization_id = l_organization_id
             where agreement_id = l_agreement_id;
            update xxwc.XXWC_OM_CSP_HEADER_ARCHIVE
               set organization_id = l_organization_id
             where agreement_id = l_agreement_id;
            commit;
          end loop;
          -- now if header exist so we have to check if lines exist or not if doesn't exist then insert
        else
          begin
            select agreement_id
              into l_agreement_id1
              from xxwc.XXWC_OM_CONTRACT_PRICING_HDR
             where customer_id = l_QUALIFIER_ATTR_VALUE_TO
               and price_type = l_price_type
               and agreement_type = 'MTX';
            select count(1)
              into l_count1
              from xxwc.XXWC_OM_CONTRACT_PRICING_LINES
             where agreement_id = l_agreement_id1
               and product_value = l_partnumber
               and product_attribute = l_product_attribute;
            if l_count1 = 0 then
              for a in (select list_line_id,
                               creation_date,
                               created_by,
                               last_update_date,
                               last_updated_by,
                               last_update_login,
                               program_application_id,
                               program_id,
                               program_update_date,
                               request_id,
                               list_header_id,
                               list_line_type_code,
                               start_date_active,
                               end_date_active,
                               automatic_flag,
                               modifier_level_code,
                               price_by_formula_id,
                               list_price,
                               list_price_uom_code,
                               primary_uom_flag,
                               inventory_item_id,
                               organization_id,
                               related_item_id,
                               relationship_type_id,
                               substitution_context,
                               substitution_attribute,
                               substitution_value,
                               revision,
                               revision_date,
                               revision_reason_code,
                               price_break_type_code,
                               percent_price,
                               number_effective_periods,
                               effective_period_uom,
                               arithmetic_operator,
                               operand,
                               override_flag,
                               print_on_invoice_flag,
                               rebate_transaction_type_code,
                               base_qty,
                               base_uom_code,
                               accrual_qty,
                               accrual_uom_code,
                               estim_accrual_rate,
                               comments,
                               generate_using_formula_id,
                               reprice_flag,
                               list_line_no,
                               estim_gl_value,
                               benefit_price_list_line_id,
                               expiration_period_start_date,
                               number_expiration_periods,
                               expiration_period_uom,
                               expiration_date,
                               accrual_flag,
                               pricing_phase_id,
                               pricing_group_sequence,
                               incompatibility_grp_code,
                               product_precedence,
                               proration_type_code,
                               accrual_conversion_rate,
                               benefit_qty,
                               benefit_uom_code,
                               recurring_flag,
                               benefit_limit,
                               charge_type_code,
                               charge_subtype_code,
                               context,
                               attribute1,
                               attribute2,
                               attribute3,
                               attribute4,
                               attribute5,
                               attribute6,
                               attribute7,
                               attribute8,
                               attribute9,
                               attribute10,
                               attribute11,
                               attribute12,
                               attribute13,
                               attribute14,
                               attribute15,
                               include_on_returns_flag,
                               qualification_ind,
                               limit_exists_flag,
                               group_count,
                               net_amount_flag,
                               recurring_value,
                               accum_context,
                               accum_attribute,
                               accum_attr_run_src_flag,
                               customer_item_id,
                               break_uom_code,
                               break_uom_context,
                               break_uom_attribute,
                               pattern_id,
                               product_uom_code,
                               pricing_attribute_count,
                               hash_key,
                               cache_key,
                               orig_sys_line_ref,
                               orig_sys_header_ref,
                               continuous_price_break_flag,
                               eq_flag,
                               currency_header_id,
                               null_other_oprt_count,
                               pte_code,
                               source_system_code,
                               service_duration,
                               service_period,
                               rowidtochar(rowid) testrowid
                          from qp_list_lines
                         where list_header_id = i.list_header_id
                        /*and end_date_active is null*/
                        ) loop
                begin
                  -- get the seqeunce to go in custom line level table
                  select XXWC_OM_CONTRACT_PRICING_LIN_S.nextval
                    into l_agreement_line_id
                    from dual;
                  --  dbms_output.put_line('l_agreement_line_id:' ||
                  --      l_agreement_line_id);
                  -- get additional paramters to fill in custom line level table
                  select product_attr_val, OPERAND, product_attr_value
                    into l_product_attr_val,
                         l_mod_value,
                         l_product_attr_value
                    from qp_modifier_summary_v
                   where list_line_id = a.list_line_id;
                  -- dbms_output.put_line('a.list_line_id:' || a.list_line_id);
                  -- dbms_output.put_line('l_product_attr_val:' || l_product_attr_val);
                  -- dbms_output.put_line('l_mod_value:' || l_mod_value);
                  -- dbms_output.put_line('l_product_attr_value:' ||
                  --             l_product_attr_value);
                
                  l_mod_value1 := 100 - (l_mod_value * 100);
                  --  dbms_output.put_line('l_mod_value1:' || l_mod_value1);
                
                  begin
                    select b.segment1, b.description
                      into l_partnumber, l_description
                      from mtl_system_items_b b
                     where b.inventory_item_id = l_product_attr_val
                       and ORGANIZATION_ID = 222;
                  
                    --  dbms_output.put_line('l_partnumber:' || l_partnumber);
                    -- dbms_output.put_line('l_description:' || l_description);
                  
                    select b.organization_id
                      into l_organization_id
                      from mtl_system_items_b b
                     where b.segment1 = l_partnumber
                       and ORGANIZATION_ID not in 222
                       and rownum = 1;
                  
                    -- dbms_output.put_line('l_organization_id:' || l_organization_id);
                  
                    l_product_attribute := 'ITEM';
                  exception
                    when no_data_found then
                      begin
                        -- it will come here when its not sku level but cat class level 
                        select distinct /*item.INVENTORY_ITEM_ID,
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                item.ORGANIZATION_ID,item.SEGMENT1,
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                               item.DESCRIPTION,item_c.CATEGORY_SET_ID,
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                           item_c.CATEGORY_ID, 
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                             m_c.CATEGORY_ID,*/
                                        m_c.SEGMENT1,
                                        m_c.DESCRIPTION
                          into l_partnumber, l_description
                        /*, m_c_s.CATEGORY_SET_ID,m_c_s.STRUCTURE_ID,
                        m_c_s.CATEGORY_SET_NAME,m_c_s.DESCRIPTION */
                          from mtl_system_items_b  item,
                               mtl_item_categories item_c,
                               mtl_categories      m_c /*,mtl_category_sets m_c_s */
                         where item.ORGANIZATION_ID =
                               item_c.ORGANIZATION_ID
                           and item.INVENTORY_ITEM_ID =
                               item_c.INVENTORY_ITEM_ID
                           and item_c.CATEGORY_ID = m_c.CATEGORY_ID
                           and item.ORGANIZATION_ID = 222
                              --and item_c.CATEGORY_SET_ID=m_c_s.CATEGORY_SET_ID 
                           and item_c.category_id = l_product_attr_val;
                      
                        -- dbms_output.put_line('l_partnumber1:' || l_partnumber);
                        -- dbms_output.put_line('l_description1:' || l_description);
                      
                        select item.ORGANIZATION_ID
                          into l_organization_id
                          from mtl_system_items_b  item,
                               mtl_item_categories item_c,
                               mtl_categories      m_c /*,mtl_category_sets m_c_s */
                         where item.ORGANIZATION_ID =
                               item_c.ORGANIZATION_ID
                           and item.INVENTORY_ITEM_ID =
                               item_c.INVENTORY_ITEM_ID
                           and item_c.CATEGORY_ID = m_c.CATEGORY_ID
                           and item.ORGANIZATION_ID not in 222
                           and m_c.segment1 = l_partnumber
                           and rownum = 1;
                        -- dbms_output.put_line('l_organization_id1:' ||
                        --      l_organization_id);
                        -- dbms_output.put_line('l_product_attr_value1:' ||
                        --      l_product_attr_value);
                        if l_product_attr_value is not null then
                          l_partnumber := l_product_attr_value;
                        end if;
                        l_product_attribute := 'CAT_CLASS';
                      exception
                        when no_data_found then
                          -- this is to handle exceptions
                          l_partnumber        := l_product_attr_val;
                          l_product_attribute := 'CAT_CLASS';
                          l_description       := l_product_attr_val;
                      end;
                  end;
                end;
                insert into xxwc.XXWC_OM_CONTRACT_PRICING_LINES
                  (agreement_line_id,
                   agreement_id,
                   agreement_type,
                   vendor_quote_number,
                   product_attribute,
                   product_value,
                   product_description,
                   list_price,
                   start_date,
                   end_date,
                   modifier_type,
                   modifier_value,
                   selling_price,
                   special_cost,
                   vendor_id,
                   max_quantity_terms,
                   average_cost,
                   gross_margin,
                   line_status,
                   interfaced_flag,
                   revision_number,
                   latest_rec_flag,
                   attribute_category,
                   attribute1,
                   attribute2,
                   attribute3,
                   attribute4,
                   attribute5,
                   attribute6,
                   attribute7,
                   attribute8,
                   attribute9,
                   attribute10,
                   attribute11,
                   attribute12,
                   attribute13,
                   attribute14,
                   attribute15,
                   creation_date,
                   created_by,
                   last_update_date,
                   last_updated_by,
                   last_update_login,
                   calc_selling_price,
                   list_header_id,
                   conversion_status,
                   CATEGORY_ID)
                values
                  (l_agreement_line_id,
                   l_agreement_id,
                   'MTX',
                   null,
                   l_product_attribute,
                   l_partnumber,
                   l_description,
                   null,
                   a.start_date_active,
                   a.end_date_active,
                   'PERCENT',
                   l_mod_value1,
                   null,
                   null,
                   null,
                   null,
                   null,
                   null,
                   'APPROVED',
                   'Y',
                   0,
                   'Y',
                   null,
                   null,
                   null,
                   null,
                   null,
                   null,
                   null,
                   null,
                   null,
                   null,
                   null,
                   null,
                   null,
                   null,
                   null,
                   null,
                   a.creation_date,
                   a.created_by,
                   a.last_update_date,
                   a.last_updated_by,
                   a.last_update_login,
                   null,
                   i.list_header_id,
                   'Y',
                   l_partnumber);
                --inserting data into archive tables
                insert into XXWC.XXWC_OM_CSP_LINES_ARCHIVE
                  (agreement_line_id,
                   agreement_id,
                   agreement_type,
                   vendor_quote_number,
                   product_attribute,
                   product_value,
                   product_description,
                   list_price,
                   start_date,
                   end_date,
                   modifier_type,
                   modifier_value,
                   selling_price,
                   special_cost,
                   vendor_id,
                   max_quantity_terms,
                   average_cost,
                   gross_margin,
                   line_status,
                   interfaced_flag,
                   revision_number,
                   latest_rec_flag,
                   attribute_category,
                   attribute1,
                   attribute2,
                   attribute3,
                   attribute4,
                   attribute5,
                   attribute6,
                   attribute7,
                   attribute8,
                   attribute9,
                   attribute10,
                   attribute11,
                   attribute12,
                   attribute13,
                   attribute14,
                   attribute15,
                   creation_date,
                   created_by,
                   last_update_date,
                   last_updated_by,
                   last_update_login,
                   calc_selling_price,
                   list_header_id,
                   conversion_status,
                   CATEGORY_ID)
                values
                  (l_agreement_line_id,
                   l_agreement_id,
                   'MTX',
                   null,
                   l_product_attribute,
                   l_partnumber,
                   l_description,
                   null,
                   a.start_date_active,
                   a.end_date_active,
                   'PERCENT',
                   l_mod_value1,
                   null,
                   null,
                   null,
                   null,
                   null,
                   null,
                   'APPROVED',
                   'Y',
                   0,
                   'Y',
                   null,
                   null,
                   null,
                   null,
                   null,
                   null,
                   null,
                   null,
                   null,
                   null,
                   null,
                   null,
                   null,
                   null,
                   null,
                   null,
                   a.creation_date,
                   a.created_by,
                   a.last_update_date,
                   a.last_updated_by,
                   a.last_update_login,
                   null,
                   i.list_header_id,
                   'Y',
                   l_partnumber);
                --dbms_output.put_line('l_agreement_line_id1:' ||
                --             l_agreement_line_id);
                update qp_list_lines
                   set attribute2 = l_agreement_line_id
                 where rowidtochar(rowid) = a.testrowid;
                update xxwc.XXWC_OM_CONTRACT_PRICING_HDR
                   set organization_id = l_organization_id
                 where agreement_id = l_agreement_id;
                update xxwc.XXWC_OM_CSP_HEADER_ARCHIVE
                   set organization_id = l_organization_id
                 where agreement_id = l_agreement_id;
                commit;
              end loop;
            end if;
          exception
            when others then
              null;
          end;
        end if;
      exception
        when others then
          null;
      end;
    end loop;
  
  exception
    when others then
      retcode := '1';
      errbuf  := sqlerrm;
  end;

end XXWC_OM_PRICING_CONV_PKG;
/