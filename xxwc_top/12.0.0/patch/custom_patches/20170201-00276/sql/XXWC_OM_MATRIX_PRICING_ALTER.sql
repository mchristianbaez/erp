/*************************************************************************
    *   SCRIPT Name: XXWC_OM_CONTRACT_PRICING_HDR_ALTER
    *
    *   PURPOSE:   Alter table XXWC_OM_CONTRACT_PRICING_HDR
	
	HISTORY
    ===============================================================================
    VERSION DATE          AUTHOR(S)              DESCRIPTION
    ------- -----------   --------------- -----------------------------------------
    1.0     01/16/2018     Niraj K ranjan   TMS#20170201-00276   CSP Form Should be modified to accommodate Matrix modifiers--Matrix Maintenance
*****************************************************************************/
alter table xxwc.xxwc_om_contract_pricing_hdr add  agreement_type varchar2(50);
alter table XXWC.XXWC_OM_CSP_HEADER_ARCHIVE add  agreement_type varchar2(50);
alter table xxwc.XXWC_OM_CONTRACT_PRICING_HDR add conversion_status varchar2(1) default 'N';
alter table xxwc.XXWC_OM_CSP_HEADER_ARCHIVE add conversion_status varchar2(1);
alter table xxwc.XXWC_OM_CONTRACT_PRICING_LINES add conversion_status varchar2(1) default 'N';
alter table XXWC.XXWC_OM_CSP_LINES_ARCHIVE add conversion_status varchar2(1); 
alter table xxwc.XXWC_OM_CONTRACT_PRICING_LINES add category_id number;
alter table XXWC.XXWC_OM_CSP_LINES_ARCHIVE add category_id number;