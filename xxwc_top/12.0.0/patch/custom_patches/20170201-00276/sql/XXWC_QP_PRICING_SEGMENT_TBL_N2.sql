/*************************************************************************
    *   SCRIPT Name: XXWC_QP_PRICING_SEGMENT_TBL
    *
    *   PURPOSE:   Indexes on XXWC_QP_PRICING_SEGMENT_TBL
	
	HISTORY
    ===============================================================================
    VERSION DATE          AUTHOR(S)              DESCRIPTION
    ------- -----------   --------------- -----------------------------------------
    1.0     01/16/2018     Nancy Pahwa   TMS#20170201-00276   CSP Form Should be modified to accommodate Matrix modifiers--Matrix Maintenance
*****************************************************************************/
CREATE INDEX "XXWC"."PRODUCT_IDX2" ON "XXWC"."XXWC_QP_PRICING_SEGMENT_TBL" ("PRODUCT_ID");